---
title: >-
    بخش ۱۰۹ - یاری نمودن تکش خان بر سام و جنگیدن بر فغفور چین
---
# بخش ۱۰۹ - یاری نمودن تکش خان بر سام و جنگیدن بر فغفور چین

<div class="b" id="bn1"><div class="m1"><p>شه چین چو از گفتش آگاه شد</p></div>
<div class="m2"><p>بدو روز گفتی که کوتاه شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز اندوه بر زد به سر هر دو دست</p></div>
<div class="m2"><p>همی گفت پشت امیدم شکست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همی گفت دستور انده چراست</p></div>
<div class="m2"><p>پری دخت خود نزد شاه ختاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همانا نیابد ازو سام کام</p></div>
<div class="m2"><p>که او با تمرتاش گردید رام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه شد گر تکش خان ز بت رخ بتافت</p></div>
<div class="m2"><p>به یاری سام نریمان شتافت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنون صدهزار است با تو سپاه</p></div>
<div class="m2"><p>همه رزمجوی و همه کینه‌خواه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو رخ بر سر کارزار آورند</p></div>
<div class="m2"><p>جهان را ز بدخواه تار آورند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز از غیر اندیشه‌ات پیشه نیست</p></div>
<div class="m2"><p>به سام و تکش خانت اندیشه نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز اندیشه بر تاب رخ سوی جنگ</p></div>
<div class="m2"><p>به بدخواه کن روز را تار و تنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سپه را برانگیز بر هر طرف</p></div>
<div class="m2"><p>ز انده مزن هر زمان کف به کف</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شه چنین چو بشنید برکرد بور</p></div>
<div class="m2"><p>خروشنده گشت و برآورد شور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برانگیخت لشکر درآمد به جنگ</p></div>
<div class="m2"><p>ز خون دشت چین شد همه لعل رنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تکش خان جنگی و سام دلیر</p></div>
<div class="m2"><p>نکردند اندیشه از تیغ و تیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نهادند رخ بر سوی قلبگاه</p></div>
<div class="m2"><p>گرفتند دور گرانمایه شاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ببستند ره را به جنگ آوران</p></div>
<div class="m2"><p>ز بس کشته شد گاو ماهی گران</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دو روز و دو شب همچنین جنگ بود</p></div>
<div class="m2"><p>جهان بر هژبرافکنان تنگ بود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به روز سیم شاه شد چیردست</p></div>
<div class="m2"><p>درآمد به سام و تکش خان شکست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو بیچاره شد سام لب برگشاد</p></div>
<div class="m2"><p>ز دادار دارنده می‌کرد یاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همی گفت کای آگه از هر چه هست</p></div>
<div class="m2"><p>مگردان درین رزم پشتم شکست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر چند دشمن کند چیرگی</p></div>
<div class="m2"><p>ممان تا کند اخترم تیرگی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>درین بد که از فر پروردگار</p></div>
<div class="m2"><p>یکی گرد شد ناگه از یک کنار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ختائی سواران زرینه چنگ</p></div>
<div class="m2"><p>همه شیرصولت ابا فر و هنگ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دلاور سپاهی همه نامور</p></div>
<div class="m2"><p>همه همچو شیران پرخاشخر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همه همچو نر اژدها روز کین</p></div>
<div class="m2"><p>همه دل نهاده به یکجا به چین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کجا پیشرو بود فرهنگ گرد</p></div>
<div class="m2"><p>که چون او نبد در گه دستبرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پس دیوزاده تمرتاش بود</p></div>
<div class="m2"><p>که او را به دل رای پرخاش بود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ابا او همه سروران سپاه</p></div>
<div class="m2"><p>برانگیخته باره بادپا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو آگاه گشتند از آن داوری</p></div>
<div class="m2"><p>براندند اسب از پی یاوری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پری‌دخت پوشیده ساز نبرد</p></div>
<div class="m2"><p>ز هودج به اسب اندر آمد چو گرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خروشنده شد ماهرو همچو میغ</p></div>
<div class="m2"><p>پی رزم و کینه برآهیخت تیغ</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دگر ره ز هر سوی پیوست جنگ</p></div>
<div class="m2"><p>ز بس کشته ره بر اجل گشته تنگ</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خبر شد همانگه به فغفور چین</p></div>
<div class="m2"><p>که گشت دگر گشت چرخ برین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پریوش نشد با تمرتاش رام</p></div>
<div class="m2"><p>ولی شد تمرتاش یل رام سام</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از آن رو بیامد چو نر اژدها</p></div>
<div class="m2"><p>به یاری سام از دیار ختا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مر این فتنه فرهنگ انگیخته است</p></div>
<div class="m2"><p>به حنظل درون توش آمیخته است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>پری‌دخت آن خنجر آبگون</p></div>
<div class="m2"><p>کنون ریزد از سروران جوی گون</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو بشنید فغفور شد پر ز خشم</p></div>
<div class="m2"><p>ببارید آب ندامت ز چشم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>عنان برگرائید از قلبگاه</p></div>
<div class="m2"><p>جهان گشت از گرد لشکر سیاه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گزید از دلیران خنجرگذار</p></div>
<div class="m2"><p>پی رزم و خون ریختن ده هزار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو از خشم رخ سوی پرخاش کرد</p></div>
<div class="m2"><p>گذر سوی جنگ تمرتاش کرد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بدیدش که بر باد داده عنان</p></div>
<div class="m2"><p>ربوده سران را ز نوک سنان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به پیکار او سرکشان را نداشت</p></div>
<div class="m2"><p>همه ویله از چرخ گرون گذاشت</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز ناگه نقاب افکن ارجمند</p></div>
<div class="m2"><p>پدیدار شد با کیانی کمند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>یکی تیغ کینه برآهیخته</p></div>
<div class="m2"><p>سمند سبک رو برانگیخته</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>همی زد چپ و راست تیغ ستیز</p></div>
<div class="m2"><p>وزو بود نام‌آوران را گریز</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بدو چون نظر کرد فغفور چین</p></div>
<div class="m2"><p>چو شیر دژآگاه در خشم و کین</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بدانست کان جنگجو مهوش است</p></div>
<div class="m2"><p>کز ایشان به پیکار چون تش است</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سوی رزم او رفت با صد هزار</p></div>
<div class="m2"><p>فرو بست بر وی ره کارزار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خروشید و گفتش که ای تیره‌بخت</p></div>
<div class="m2"><p>مرا شد نگون از تو این تاج و تخت</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>پری دخت چون روی شه را بدید</p></div>
<div class="m2"><p>رخش گشت از بیم چون شنبلید</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بدانست کافتاد دیگر به دام</p></div>
<div class="m2"><p>خداوند را آن زمان برد نام</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بینداخت بر شاه چین تیغ تیز</p></div>
<div class="m2"><p>گرفتش سر دست شاه از ستیز</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>برون کرد تیغ از کفش در زمان</p></div>
<div class="m2"><p>بزد چنگ از کین گرفتش میان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>پری دخت از خویش شد ناامید</p></div>
<div class="m2"><p>به لرزه درآمد چو از باد، بید</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>همانگه شه چین ربودش ز جا</p></div>
<div class="m2"><p>برانگیخت که پیکر بادپا</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>زلشکر برون برد دستش ببست</p></div>
<div class="m2"><p>دل نازکش را به انده بخست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>وز آن پس غلامان خود را بخواند</p></div>
<div class="m2"><p>پری‌دخت را بر تکاور نشاند</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>همه نوش مه روی را کرد زهر</p></div>
<div class="m2"><p>چو صرصر فرستاد بازش به شهر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>خود آمد به نزدیک دستور باز</p></div>
<div class="m2"><p>بدو گفت اکنون یکی چاره ساز</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>که این رزم و کینه شود اسپری</p></div>
<div class="m2"><p>برآساید از هر دو سو لشکری</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چو در بند آمد مرا دخترم</p></div>
<div class="m2"><p>همانا فروزنده شد اخترم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چو بشنید دستور شد شدمان</p></div>
<div class="m2"><p>بزد طبل آسایش اندر زمان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>غو طبل آرام چون شد بلند</p></div>
<div class="m2"><p>بتابید هر یک عنان سمند</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>سپه رخ ز پیکار برتافتند</p></div>
<div class="m2"><p>سراسر سوی شاه بشتافتند</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>شه چین سراپرده بر پای کرد</p></div>
<div class="m2"><p>بیامد به تخت مهی جای کرد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>دلیران به نزدش شدند انجمن</p></div>
<div class="m2"><p>همی از تمرتاش‌شان بد سخن</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>وزین سو درآمد به خرگاه سام</p></div>
<div class="m2"><p>نشستند نزدش بزرگان تمام</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>تمرتاش و فرهنگ زورآزمای</p></div>
<div class="m2"><p>رسیدند زی پهلو پاک رای</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>زبان برگشادند نام آوران</p></div>
<div class="m2"><p>به ما بر بد آمد ز جنگی سران</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>پری‌دخت از ایدر سوی جنگ شد</p></div>
<div class="m2"><p>همه روی گیتی به ما تنگ شد</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بدو بازخورده است فغفور چین</p></div>
<div class="m2"><p>به نیروش بر بوده از پشت زین</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ندانیم با او چه سازد دگر</p></div>
<div class="m2"><p>ازین کار گشتیم ما خون جگر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ز بهر دل شهریار ختا</p></div>
<div class="m2"><p>دژم رو نشد شیر رزم‌آزما</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>بگفتا کزین غم مدارید هیچ</p></div>
<div class="m2"><p>به شادی کنون برد باید بسیچ</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>دگر بر شه چین شکست آورم</p></div>
<div class="m2"><p>مر او را دگر ره به دست آورم</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چه کردی مرا یکسره بازگوی</p></div>
<div class="m2"><p>مپیچ از ره راستی هیچ روی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>سبک دیوزاده زبان برگشاد</p></div>
<div class="m2"><p>ز کردار خود یکسره کرد یاد</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>همان هم ز خوب تمرتاش گفت</p></div>
<div class="m2"><p>جهان پهلوان شد بسی در شگفت</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ورا با تکش خان نوازش نمود</p></div>
<div class="m2"><p>همی دم به دم پایگه برفزود</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چو یک بهره بگذشت از شب به راز</p></div>
<div class="m2"><p>به خواب و به آسایش آمد نیاز</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>تمرتاش چون شد به خرگاه خویش</p></div>
<div class="m2"><p>ز کار پریوش دلش بود ریش</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>مرا گفت ز اندیشه دل خون بود</p></div>
<div class="m2"><p>دل سام رزم‌آزما چون بود</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>یکی خواب دیدم شدم بیقرار</p></div>
<div class="m2"><p>جز ازغم ندارم کنون غمگسار</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>چه سازد درین تیره شب پهلوان</p></div>
<div class="m2"><p>ز دوری دلدار شیرین زبان</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>که با او فراوان به سر برده است</p></div>
<div class="m2"><p>به مهرش دل خویش بسپرده است</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>بدادم ز کف یار دلجوی او</p></div>
<div class="m2"><p>کنونم بود شرم از روی او</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>دلش چون به اندیشه‌ها گشت رام</p></div>
<div class="m2"><p>نهان ره سپر شد به خرگاه سام</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>سر پاسبان دید در خواب خوش</p></div>
<div class="m2"><p>تو گفتی که در سر ندارند هش</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>بر خرگه آمد شه تیزهوش</p></div>
<div class="m2"><p>یکی ناله زارش آمد به گوش</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>به خرگه درون از نهان بنگرید</p></div>
<div class="m2"><p>ز ناکام مر سام یل را بدید</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>که از هجر دلدار خون می‌گریست</p></div>
<div class="m2"><p>چه گویم کز اندوه چون می‌گریست</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>زمان تا زمان دست می‌زد به سر</p></div>
<div class="m2"><p>همی گفت کای مهوش سیمبر</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>ندانم ز هجرت چه چاره کنم</p></div>
<div class="m2"><p>چگونه ز رویت کناره کنم</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>ازین پس نتابم رخ از رزم و کین</p></div>
<div class="m2"><p>مگر سر ببرم ز سالار چین</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>تو را از نهانی به دست آورم</p></div>
<div class="m2"><p>سر اختر شوم پست آورم</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>از آن پس شب و روز بی غم شوم</p></div>
<div class="m2"><p>به بوی دو زلف تو خرم شوم</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>تمرتاش را چهره شد شنبلید</p></div>
<div class="m2"><p>چو گفتار سام دلاور شنید</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>بدانست کو غرق بحر غم است</p></div>
<div class="m2"><p>خیال رخ همدمش محرم است</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>همانگه ز دهلیز پرده‌سرای</p></div>
<div class="m2"><p>تمرتاش برجست و شد باز جای</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>به پیش اندر افکند ز اندوه سر</p></div>
<div class="m2"><p>ببارید از دیده خون جگر</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>یکی تیره او مرد جاسوس داشت</p></div>
<div class="m2"><p>که از تندیش باد افسوس داشت</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>به حیلت‌گری در جهان شهره بود</p></div>
<div class="m2"><p>ز مردانگی نیز بابهره بود</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>کمندش زحل را کشیدی به دام</p></div>
<div class="m2"><p>دژم دیو بودیش نیرنگ نام</p></div></div>