---
title: >-
    بخش ۹۰ - داستان فغفور چین با پری‌دخت و چگونگی آن
---
# بخش ۹۰ - داستان فغفور چین با پری‌دخت و چگونگی آن

<div class="b" id="bn1"><div class="m1"><p>سراینده داستان گزین</p></div>
<div class="m2"><p>چنین داستان زد ز فغفور چین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که چون سام یل را به نیرنگ و رنگ</p></div>
<div class="m2"><p>روان کرد با سروران سوی جنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درآمد به قصر پریوش دژم</p></div>
<div class="m2"><p>بدیدش فرو رفته در بحر غم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز اندیشه‌ها سر فکنده به پیش</p></div>
<div class="m2"><p>دو چشمش پر آب و دل از درد ریش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دژم روی بنشسته از بهر سام</p></div>
<div class="m2"><p>گل سرخ او یکسره زردفام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو دیدش بدین‌گونه سالار چین</p></div>
<div class="m2"><p>بَرو کرد پرچین و در شد به کین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زبان از پی سرزنش باز کرد</p></div>
<div class="m2"><p>همه گفته ناخوش آغاز کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همانا توئی ننگ و عار زنان</p></div>
<div class="m2"><p>که گردیده‌ای یار با دشمنان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ببستی به خون برادر کمر</p></div>
<div class="m2"><p>ورا دور گرداندی از تخت زر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه ساز شاهی و گنج طغان</p></div>
<div class="m2"><p>بدادی به سام و مر آن سروران</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه مایه مرا ساختی خوار و زار</p></div>
<div class="m2"><p>که شد سام چیره گه کارزار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو مایه شدم در جهان خار و پست</p></div>
<div class="m2"><p>که مر دیوزاده بتم را شکست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بگفت و بیامد چو آذر برش</p></div>
<div class="m2"><p>بزد تازیانه بسی بر سرش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برآشفت مه‌رو و بگشاد لب</p></div>
<div class="m2"><p>بدو گفت کای شاه والانصب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چرا سام تا بود در شهر چین</p></div>
<div class="m2"><p>نبودت جز از مهر من هم‌نشین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برو باز کردی در مکر و ریو</p></div>
<div class="m2"><p>فرستادیش سوی پیکار دیو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو او شد کمین سر برافراختی</p></div>
<div class="m2"><p>به من زین نشان داوری ساختی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همانگه که او چیره گردد به کین</p></div>
<div class="m2"><p>بیاید دگر ره سوی شهر چین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مکن کاری ای نامور شهریار</p></div>
<div class="m2"><p>که گردی پشیمان سرانجام کار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>برآشفت فغفور و آهیخت تیغ</p></div>
<div class="m2"><p>که خون ریزد از ماه رخ بی‌دریغ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گرفتش سر دست فغفور سخت</p></div>
<div class="m2"><p>بدو گفت کای شاه فیروزبخت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زمانی به خون ریختن رخ بتاب</p></div>
<div class="m2"><p>ز دل دور کن رای کین و شتاب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چه باید بدو اندر آویختن</p></div>
<div class="m2"><p>ز نازک تنش خون فرو ریختن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز سر دور کن کینه و ماجرا</p></div>
<div class="m2"><p>روان مر او را به راه خطا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تمرتاش را شادمان کن ازین</p></div>
<div class="m2"><p>چو او کام جوید تو شادی گزین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که مر دخت را شوی زیبد به دهر</p></div>
<div class="m2"><p>به جای بازمان ای شهنشاه قهر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>برون برد خنجر ز چنگال شاه</p></div>
<div class="m2"><p>شد از قصر و رو کرد در بارگاه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>برآمد به تخت و طلب کرد جام</p></div>
<div class="m2"><p>از آن پس گزین کرد پانصد غلام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دگر صد کنیزک همه چون پری</p></div>
<div class="m2"><p>همه چون مه هفته در دلبری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز اسب و سام و کلاه و کمر</p></div>
<div class="m2"><p>ز تیغ سرافشان و زرین سپر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ازینها فراوان طلب کرد شاه</p></div>
<div class="m2"><p>بیاورد دستور در بارگاه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>برآمد به تخت و طلب کرد جام</p></div>
<div class="m2"><p>وز آن پس گزین کرد از هر کدام</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بدو گفت سوی شبستان شتاب</p></div>
<div class="m2"><p>ز آزرم یکبارگی رخ بتاب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پری‌دخت را در محفه نشان</p></div>
<div class="m2"><p>گزین کن تنی چند از سرکشان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>غلامان و این خواسته سر به سر</p></div>
<div class="m2"><p>بر آن سرکشان یک به یک برشمر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>روان سازشان سوی راه ختا</p></div>
<div class="m2"><p>بدان تا شود اسپری ماجرا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>پذیرفت دستور از بارگاه</p></div>
<div class="m2"><p>درآمد شتابان به ایوان گاه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ورا دید ز اندوه پژمان شده</p></div>
<div class="m2"><p>از آن آتش تیز بریان شده</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>زمین را ببوسید و لب برگشاد</p></div>
<div class="m2"><p>همه گفت فغفور چین کرد یاد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>پریوش ز غم خون ز دیده براند</p></div>
<div class="m2"><p>می ارغوان را به گل برفشاند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>وز آن پس به اندیشه‌ها گشت جفت</p></div>
<div class="m2"><p>درآمد سوی هودج و رخ نهفت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ببستند هودج به پشت هیون</p></div>
<div class="m2"><p>پریوش همی ریخت از دیده خون</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>غلامان نشستند بر بادپا</p></div>
<div class="m2"><p>ز چین رخ نهادند سوی ختا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ازین عالم افروز آگاه شد</p></div>
<div class="m2"><p>برو روز اندوه کوتاه شد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز شادی برآمد به ابر بلند</p></div>
<div class="m2"><p>روان شد بر پهلو ارجمند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بدین تا بگوید به فرخنده سام</p></div>
<div class="m2"><p>که شد یار تو با قمرتاش رام</p></div></div>