---
title: >-
    بخش ۱۸۷ - به دار زدن شداد ناپاک و ناپدید شدن آن
---
# بخش ۱۸۷ - به دار زدن شداد ناپاک و ناپدید شدن آن

<div class="b" id="bn1"><div class="m1"><p>یکی دار کردند بر پا بلند</p></div>
<div class="m2"><p>رسیدند گردان تن ارجمند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به تیر و کمان همچو شیر آمدند</p></div>
<div class="m2"><p>ابا ترکش پر ز تیر آمدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ورا زنده کردند آنگه به دار</p></div>
<div class="m2"><p>تماشاکنان گرد سام سوار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که ناگاه ابری برآمد سیاه</p></div>
<div class="m2"><p>جهان کرد تاریک بر مهر و ماه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازو رعد با برق جستن گرفت</p></div>
<div class="m2"><p>دل مردمان زو شکستن گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غریوی برآمد از آن تیره ابر</p></div>
<div class="m2"><p>کزو آب گردید جان هژبر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که هان سام شوریده تیره بخت</p></div>
<div class="m2"><p>گرفتی همه گنج با تاج و تخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نرستی ز دیوان مغرب زمین</p></div>
<div class="m2"><p>که پیوسته بستی کمر بهر کین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسی غره گشتی تو بر خویشتن</p></div>
<div class="m2"><p>ندانی مگر نیروی اهرمن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تن خود به خیره به کشتن دهی</p></div>
<div class="m2"><p>میان بسته همواره بر بیرهی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیارم به جان تو یازید چنگ</p></div>
<div class="m2"><p>که عار است با تخم جمشید جنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگرنه تنت را ز سر کندمی</p></div>
<div class="m2"><p>به میدان تنت را بیفکندمی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بگفت و زمانه همه شد سیاه</p></div>
<div class="m2"><p>ز ماهی سیه گشت تا برج ماه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنان شد که کس چهره هم ندید</p></div>
<div class="m2"><p>جهان چادر قیر بر سر کشید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یکی تیره گرد از جهان اوفتاد</p></div>
<div class="m2"><p>تو گفتی مگر آسمان اوفتاد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یکی صاعقه جست با باد سخت</p></div>
<div class="m2"><p>که از ریشه برکند یکسر درخت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چنین تا سه ساعت جهان تیره گشت</p></div>
<div class="m2"><p>که چشم دلیران همه خیره گشت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پس آنگاه روشن شد آن جایگاه</p></div>
<div class="m2"><p>برون آمد از تیرگی مهر و ماه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نه شداد را دید آنگه نه دار</p></div>
<div class="m2"><p>ببد خیره زو گرد سام سوار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شگفتی فرو ماند سالار سام</p></div>
<div class="m2"><p>نه هرگز ندیدم بدین گونه دام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چه بود این سیاهی و شداد کو</p></div>
<div class="m2"><p>مر آن بدگهر دیو بیداد کو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کجا رفت و چون بود این گفتگو</p></div>
<div class="m2"><p>چنین گفت تسلیم کای رزمجو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>درین دشت آمد دگر ابرها</p></div>
<div class="m2"><p>کزو جان شداد آمد رها</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شگفتی بلائی است دیو دمان</p></div>
<div class="m2"><p>کزو چرخ گردنده جوید امان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نتابد کسی پیش او روز جنگ</p></div>
<div class="m2"><p>چو آید کند کار بر خویش تنگ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به یک دم جهان را نه پی بسپرد</p></div>
<div class="m2"><p>چو ابر دمان بر فلک بگذرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>برادر بود بارکیشان دیو</p></div>
<div class="m2"><p>به نیرو ز گیتی برآرد غریو</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو بشنید ازو این سخن پهلوان</p></div>
<div class="m2"><p>برآشفت سالار روشن روان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به دادار دارنده سوگند خورد</p></div>
<div class="m2"><p>به روز سفید و شب لاجورد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به یزدان که افراشته آسمان</p></div>
<div class="m2"><p>که هم جان دهد هم سر آرد زمان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به تخت و به تاج منوچهرشاه</p></div>
<div class="m2"><p>که از فر او برنهادم کلاه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>که هرگز نیاید چنان شهریار</p></div>
<div class="m2"><p>به داد و دهش گشته با عدل یار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به شمشیر تیز و به میدان جنگ</p></div>
<div class="m2"><p>به مردان با نام و ناموس و ننگ</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به جان پری‌دخت تابنده ماه</p></div>
<div class="m2"><p>بدان نرگس مست و زلف سیاه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به وصلش که جان را روان می‌دهد</p></div>
<div class="m2"><p>که سام از پی وصل جان می‌دهد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>که امشب بتازم به کوه فنا</p></div>
<div class="m2"><p>به پیکار آن دیو نر ابرها</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بیارم سرش را به این انجمن</p></div>
<div class="m2"><p>نشانی نمانم از آن اهرمن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همه لشکر عادیان یکسره</p></div>
<div class="m2"><p>کمربسته در پیش مرد سره</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همه تاج و از تخت و گنج و سپاه</p></div>
<div class="m2"><p>ببخشید یکسر ابا طنجه شاه</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نشاندش ابر تخت شداد عاد</p></div>
<div class="m2"><p>دل شاه طنجه از آن گشت شاد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بدو گفت این مرز یکسر ز تست</p></div>
<div class="m2"><p>که بیداردل باشی و تندرست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شب و روز همراه تسلیم شاه</p></div>
<div class="m2"><p>به سر بر نگهدار مغرب سپاه</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مبادا دگر عادی تیره روز</p></div>
<div class="m2"><p>که خوانند عوج آن یل کینه‌سوز</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به سوی زرنداب سازد کمین</p></div>
<div class="m2"><p>بگیرد شه طنجه را در کمین</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تو باید که باشی نگهدار او</p></div>
<div class="m2"><p>نباشی به دنبال آزار او</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>که رفتم من ایدر به پیکار دیو</p></div>
<div class="m2"><p>ببینم به فرمان کیهان خدیو</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چگونه به دست آورم ابرها</p></div>
<div class="m2"><p>پری‌دخت را سازم ایدر رها</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>مرا یاد دادار همره بس است</p></div>
<div class="m2"><p>که او بیکسان را به گیتی کس است</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>همه شب همی گفت سام سوار</p></div>
<div class="m2"><p>دل و جان نهاده در آن کارزار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چنین تا به هنگام بانگ خروس</p></div>
<div class="m2"><p>سپهبد خروشید مانند کوس</p></div></div>