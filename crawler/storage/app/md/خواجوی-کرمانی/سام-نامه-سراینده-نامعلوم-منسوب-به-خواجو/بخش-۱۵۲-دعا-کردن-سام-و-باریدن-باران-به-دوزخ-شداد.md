---
title: >-
    بخش ۱۵۲ - دعا کردن سام و باریدن باران به دوزخ شداد
---
# بخش ۱۵۲ - دعا کردن سام و باریدن باران به دوزخ شداد

<div class="b" id="bn1"><div class="m1"><p>به نزدیک دادار شد سرفراز</p></div>
<div class="m2"><p>زمین را ببوسید و بردش نماز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین گفت کای داور آب و خاک</p></div>
<div class="m2"><p>ز تو باد آتش پر از بیم و پاک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان سر به سر زیر فرمان تست</p></div>
<div class="m2"><p>وگر آب و آتش همه زان توست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا بخت و نیرو به شداد ده</p></div>
<div class="m2"><p>شکستی بر این لشکر عاد ده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی چاره از بهر آتش بساز</p></div>
<div class="m2"><p>که بیچارگان را توئی چاره‌ساز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که من دوزخ و جنت او خراب</p></div>
<div class="m2"><p>بسازم که شداد آید به تاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>براندازم این تخم ناپاک دیو</p></div>
<div class="m2"><p>که کردست نام خودش را خدیو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خدیو زمانه سراسر توئی</p></div>
<div class="m2"><p>به هرجای بر بنده یاور توئی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگفت و بمالید رخ بر زمین</p></div>
<div class="m2"><p>همان دم به فرمان جان آفرین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز گردون گردنده برخاست ابر</p></div>
<div class="m2"><p>خروشان بر آن کوه سر چون هژبر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکی برق و رعدی برآمد نهیب</p></div>
<div class="m2"><p>که سیلاب آمد ز بالا به شیب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از آن ابر بارید باران چنان</p></div>
<div class="m2"><p>که آن دره پر شد ز آب روان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه آتش دره را آب برد</p></div>
<div class="m2"><p>چو آب آمد از کوه، آتش فسرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همه آتش از فر مرد سره</p></div>
<div class="m2"><p>به فرمان یزدان بشست از دره</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر آن کس که برد او پناه بر خدا</p></div>
<div class="m2"><p>همیشه بود کارش از مدعا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همان دم رسیدند جنی سپاه</p></div>
<div class="m2"><p>به پیش اندر آن بود تسلیم شاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو آن آتش دره افسرده دید</p></div>
<div class="m2"><p>تن دیو از هر طرف مرده دید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شگفتی به سام دلاور بماند</p></div>
<div class="m2"><p>برو نام یزدان فراوان بخواند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بیامد ببوسید پای دلیر</p></div>
<div class="m2"><p>دعا کرد بر پهلو شیرگیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بر کوه آذر فرود آمدند</p></div>
<div class="m2"><p>به دل با می و بانگ و رود آمدند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کشیدند بر دشت پرده‌سرا</p></div>
<div class="m2"><p>خروشی برآمد ز پرده سرا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به هم ناز و شادی دگر ساز شد</p></div>
<div class="m2"><p>نی و چنگ مطرب هم آواز شد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به جلوه درآمد ز هر سو پری</p></div>
<div class="m2"><p>چو چشم خوش خود به ساقیگری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شده بزم مانند بزم افق</p></div>
<div class="m2"><p>کشیده ز می لعل نورش تتق</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پری‌پیکران چهره آراسته</p></div>
<div class="m2"><p>پی شادی سام برخاسته</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در آن بزم ناهید بد چنگ زن</p></div>
<div class="m2"><p>مه و مهر و برجیس بد چنگ زن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به یاد پری‌دخت خون می‌گریست</p></div>
<div class="m2"><p>چگویم که از درد چون می‌گریست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چنین تا که ساقی بزم فلک</p></div>
<div class="m2"><p>بپیمود ساغر به مُلک ملک</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ازین جام زرین می‌ نور داد</p></div>
<div class="m2"><p>همه باده از ساغر هور داد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سپیده سپهبد ز روی شتاب</p></div>
<div class="m2"><p>کمر بست و بر شد به پشت غراب</p></div></div>