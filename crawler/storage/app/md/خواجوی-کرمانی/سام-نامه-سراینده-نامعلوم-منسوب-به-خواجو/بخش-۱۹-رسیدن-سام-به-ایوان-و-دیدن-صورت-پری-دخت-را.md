---
title: >-
    بخش ۱۹ - رسیدن سام به ایوان و دیدن صورت پری‌دخت را
---
# بخش ۱۹ - رسیدن سام به ایوان و دیدن صورت پری‌دخت را

<div class="b" id="bn1"><div class="m1"><p>روان گشته با آن پریچهره ماه</p></div>
<div class="m2"><p>تماشاکنان اندر آن بارگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به ناگه به کاخی رسید از قضا</p></div>
<div class="m2"><p>چو بستان جنت خوش و دلگشا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نهاده در ایوانش تختی ز زر</p></div>
<div class="m2"><p>به کیوان برآورده ایوانش سر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز رفعت فلک مانده حیران او</p></div>
<div class="m2"><p>سرآورده بر چرخ ایوان او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی نیلگون پیکر زرنگار</p></div>
<div class="m2"><p>کشیده بر او پیکری چون نگار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به بالای آن نیلگون پرنیان</p></div>
<div class="m2"><p>نوشته‌ که‌ای سام روشن روان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درین کاخ فرخنده چون بغنوی</p></div>
<div class="m2"><p>نظر کن برین لعبت معنوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که نقشی بدین‌گونه از کفر و کین</p></div>
<div class="m2"><p>نبینی مگر دخت فغفور چین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گل‌اندام سروی پری‌دخت‌ نام</p></div>
<div class="m2"><p>رخش روز روشن نماید به شام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درین صورت از راه معنی ببین</p></div>
<div class="m2"><p>فرومانده صورت پرستان چین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه هر صورتی را توان داشت دوست</p></div>
<div class="m2"><p>درین نقش بین تا چه معنی دروست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ولی نقش خود گر نبینی نکوست</p></div>
<div class="m2"><p>چو از خود گذشتی رسیدن بدوست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به معنی دهد صورت دوست دست</p></div>
<div class="m2"><p>بخوان خویش و بنیان صورت پرست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز صورت ببر تا به معنی رسی</p></div>
<div class="m2"><p>در معنی از راه دانش بری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به نیرنگ ازین رنگ رنگی برآر</p></div>
<div class="m2"><p>که تا خود چه نقش آورد روزگار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو سام اندر آن نقش حیران بماند</p></div>
<div class="m2"><p>بدان صورت از دیده گوهر فشاند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پریزاد در طاق چون بنگرید</p></div>
<div class="m2"><p>مرآن پیکر ماه نو را بدید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بدانست کز سام بردند دل</p></div>
<div class="m2"><p>هم از عشق پایش فرو شد به گل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پی کار پرده همی چاره کرد</p></div>
<div class="m2"><p>چو بیچاره شد پرده را پاره کرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بدو بانگ بر زد گرانمایه سام</p></div>
<div class="m2"><p>که هرگز مباد اولت شادکام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چرا پرده را پاره کردی چنین</p></div>
<div class="m2"><p>سیه کردی این روزگارم ببین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پریزاد از وی برآشفت گفت</p></div>
<div class="m2"><p>که این پرده را دیو اندر نهفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز نیرنگ او چون شدم باخبر</p></div>
<div class="m2"><p>همه روز شادیش بردم به سر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جهانجوی بیدل زبان برگشاد</p></div>
<div class="m2"><p>دگر ره همی کرد از پرده یاد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چنان از می عشق سرمست شد</p></div>
<div class="m2"><p>که از پای افتاد و از دست شد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سهی سروش از پا درآمد چو باد</p></div>
<div class="m2"><p>چو خورشید بر خاک ره اوفتاد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پریزاد چون دید کان گونژاد</p></div>
<div class="m2"><p>به دام پری‌دخت اندرفتاد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بدانست کش تیره شد او به کام</p></div>
<div class="m2"><p>نگردد ورابخت فرخنده رام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>برون رفت از کاخ رخ شنبلید</p></div>
<div class="m2"><p>به من تا کجا باز آید پدید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو بیهوش شد سام فرخنده رای</p></div>
<div class="m2"><p>سهی نخل قدش درآمد ز پای</p></div></div>