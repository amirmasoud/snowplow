---
title: >-
    بخش ۱۵۸ - نامه نوشتن شدید و فرستادن پیش شداد و کیفیت آن
---
# بخش ۱۵۸ - نامه نوشتن شدید و فرستادن پیش شداد و کیفیت آن

<div class="b" id="bn1"><div class="m1"><p>از آن رو شدید پلید آن بدید</p></div>
<div class="m2"><p>ز کینه بجوشید بر خود شدید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی نامه بنوشت نزد پدر</p></div>
<div class="m2"><p>برو برنوشت این همه سر به سر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز مردی و پیکار سام سوار</p></div>
<div class="m2"><p>که چون ابر غرد گه کارزار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صد و سی تن از ما بدین بارگاه</p></div>
<div class="m2"><p>جهان در جهانبینشان شد سیاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی شیر دیدم به چنگال تیز</p></div>
<div class="m2"><p>که آرد به مغرب زمین رستخیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی کار باید تو را کرد زود</p></div>
<div class="m2"><p>که ما را نماندست گفت و شنود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طلب کرد باید ز کوه بلور</p></div>
<div class="m2"><p>همان عوج عادی بدان یار و زور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیاید مگر سام را برکند</p></div>
<div class="m2"><p>بداندیش ما را مگر سر کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وگرنه کسی نیست همتای سام</p></div>
<div class="m2"><p>نتابند گردان به یک پای سام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شگفتی بود فره سام شیر</p></div>
<div class="m2"><p>سر اژدها برکند شیرگیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو فردا سواره درآید به جنگ</p></div>
<div class="m2"><p>بکوشیم در رزم آن شیر چنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدان در برابر شوم در مصاف</p></div>
<div class="m2"><p>بود رزم سیمرغ با کوه قاف</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پس آنگه جهان زیر فرمان تست</p></div>
<div class="m2"><p>سر مهر و مه زیر پیمان تست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وگرنه همه بخت شد واژگون</p></div>
<div class="m2"><p>زرنداب گردد چو دریای خون</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به تنها تن خویش جوید نبرد</p></div>
<div class="m2"><p>نه لشکر نه کشور به گاه نبرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سخن گوید از رزم شداد عاد</p></div>
<div class="m2"><p>بجز رزم دیگر نیارد به یاد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تن کوه دارد دو چنگ پلنگ</p></div>
<div class="m2"><p>نتابد به پیکار او خاره سنگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ازین بیشتر جای گفتار نیست</p></div>
<div class="m2"><p>که صد داستان همچو دیدار نیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یکی مهر کردش شدید پلید</p></div>
<div class="m2"><p>فرستاده تیزرو برگزید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فرستاد نزدیک شداد عاد</p></div>
<div class="m2"><p>نوندش برون رفت مانند باد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به مرز زرنداب آمد ز راه</p></div>
<div class="m2"><p>بدو نامه بسپرد در پیشگاه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو شداد نامه سراسر بخواند</p></div>
<div class="m2"><p>دل از بیم اندیشه در خون نشاند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بترسید از رزم سام سوار</p></div>
<div class="m2"><p>که تنها به میدان کند کارزار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همه شب در اندیشه شد بدسگال</p></div>
<div class="m2"><p>که چون رزم سازد بدان بی‌همال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ازین روی فرخنده سالار سام</p></div>
<div class="m2"><p>به بزم اندر انداخت زرینه جام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پری صف زده پیش آن پهلوان</p></div>
<div class="m2"><p>به روی سپهدار روشن روان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که فرهنگ جنی درآمد به پیش</p></div>
<div class="m2"><p>زمین بوسه داده به آئین و کیش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خبر داد از عوج و چندین سیاه</p></div>
<div class="m2"><p>که خواهد رسید اندرین رزمگاه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همه داستان گفت با پهلوان</p></div>
<div class="m2"><p>که چون نامه بنوشت تیره روان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به شداد عاد این همه گفتگو</p></div>
<div class="m2"><p>کنون عوج را خواسته جنگجو</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو بشنید ازو سام خندید و گفت</p></div>
<div class="m2"><p>که پیکار ما را ندیدی شگفت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نه از عوج ترسم نه از ابرها</p></div>
<div class="m2"><p>نیابند از تیغ تیزم رها</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به فرمان یزدان به میدان جنگ</p></div>
<div class="m2"><p>زمانی نیارند پیشم درنگ</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تماشا کن بوستان باش و بس</p></div>
<div class="m2"><p>چو آتش بسوزم همه خار و خس</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همی دل ز قلواد آزرده‌ام</p></div>
<div class="m2"><p>در اندیشه‌اش سخت پژمرده‌ام</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کهناگه بد آید به آن رزمخواه</p></div>
<div class="m2"><p>پس آنگه چه گویم به زابل سپاه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>که گم شد به ناگه ورا رسم و اسم</p></div>
<div class="m2"><p>گرفتار گشته است اندر طلسم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ندانم که زنده است یا مرده است</p></div>
<div class="m2"><p>به چنگال جادوئی آزرده است</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به پاسخ بدو گفت فرهنگ هنگ</p></div>
<div class="m2"><p>که تا آگهی آورد بی‌درنگ</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بگویم تو را من که چونست کار</p></div>
<div class="m2"><p>دل خود در اندیشه چندین مدار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بگفت و برون رفت فرهنگ هنگ</p></div>
<div class="m2"><p>که تا آگهی آورد بی‌درنگ</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به سوی طلسم عناصر شتافت</p></div>
<div class="m2"><p>همه راز قلواد را می‌شکافت</p></div></div>