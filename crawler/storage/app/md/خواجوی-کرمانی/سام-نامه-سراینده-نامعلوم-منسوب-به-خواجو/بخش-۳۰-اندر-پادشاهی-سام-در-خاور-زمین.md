---
title: >-
    بخش ۳۰ - اندر پادشاهی سام در خاور زمین
---
# بخش ۳۰ - اندر پادشاهی سام در خاور زمین

<div class="b" id="bn1"><div class="m1"><p>سران سپاهش پذیره شدند</p></div>
<div class="m2"><p>ز دیدار او جمله خیره شدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسر بر نهادند تاج زرش</p></div>
<div class="m2"><p>فشاندند لعل و گهر بر سرش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درفش کیانی برافراختند</p></div>
<div class="m2"><p>به هر جا ز زر قبه‌ها ساختند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه رخ نهادند بر خاک راه</p></div>
<div class="m2"><p>پیاده روان تا بر اسب شاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تبیره زنان طبل بنواختند</p></div>
<div class="m2"><p>غو کوس در عالم انداختند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه ملک خاور به دیبای چین</p></div>
<div class="m2"><p>بیاراسته همچو خلد برین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به هر گوشه‌ای لعبتی می‌پرست</p></div>
<div class="m2"><p>به یاد جهانجوی ساغر به دست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به فال همایون و فر همای</p></div>
<div class="m2"><p>برافراخت چتر بزرگی به پای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به فرخ‌ترین روز فرخنده فال</p></div>
<div class="m2"><p>درآمد به شهر، آفتاب جلال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو از برج ماهی برون رفت ماه</p></div>
<div class="m2"><p>شه یوسف از چه برآمد به گاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نشست آن زمان شاه کشورگشای</p></div>
<div class="m2"><p>بدان تخت زرین به عقل و به رای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جهان داوران پیش تختش به پای</p></div>
<div class="m2"><p>شهان را شده درگهش بوسه جای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه برکشیده سر تاج‌ور</p></div>
<div class="m2"><p>نهاده ز مه تا به ماهیش سر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شده انجمش کمترین بنده‌ای</p></div>
<div class="m2"><p>سپهرش کمینه سرافکنده‌ای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در ایوانش کیوان غلامی و بس</p></div>
<div class="m2"><p>به میدانش مه تیزکامی و بس</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز رفعت به مه بگذرانید تاج</p></div>
<div class="m2"><p>به شوکت ز قیصر گرفته خراج</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نموده جهانش به جان بندگی</p></div>
<div class="m2"><p>همه سرورانش سرافکندگی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به خاقان که بودی به هنگام بار</p></div>
<div class="m2"><p>به درگاه او همچو خاقان هزار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز قیصر زبر جد علم برفراخت</p></div>
<div class="m2"><p>به آئین شاهان یکی بزم ساخت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو دل بر نظام ممالک نهاد</p></div>
<div class="m2"><p>وزارت به قلواد فرخنده داد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جهان عدل و دادش فراموش کرد</p></div>
<div class="m2"><p>چو آوازه عدل او گوش کرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سفیده دمان چون نسیم بهار</p></div>
<div class="m2"><p>خبر داد از کاروان تتار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خروس سحر در خروش آمدی</p></div>
<div class="m2"><p>دم صبح عنبر فروش آمدی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>روان پرور انفاس عنبرفشان</p></div>
<div class="m2"><p>ز گلزار فردوس دادی نشان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مر آن نغمه‌گر گلستان آمدی</p></div>
<div class="m2"><p>ازو نکهت دلستان آمدی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>صبا چون رسیدی ز راه تتار</p></div>
<div class="m2"><p>نشان دادی از زلف گیسوی یار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نشستی چو گر شب بر تخت زر</p></div>
<div class="m2"><p>برآورده از چرخ گردنده سر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به گرد درش صف زدندی گوان</p></div>
<div class="m2"><p>سرافکنده در خدمت پهلوان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جهان جوی قلواد فیروزبخت</p></div>
<div class="m2"><p>به خدمت کمر بسته در پای تخت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سر سرکشان سام گیتی‌گشای</p></div>
<div class="m2"><p>یل دانش‌افروز فرخنده‌رای</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نشستی ابر تخت طهمورثی</p></div>
<div class="m2"><p>به عقل و به دانش کیامورثی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ولی بی پری‌دخت سیمین عذار</p></div>
<div class="m2"><p>نبودیش پروای شهر و دیار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همی سوختی و همی ساختی</p></div>
<div class="m2"><p>به کار ممالک نپرداختی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز بس بار خاطر یل نامدار</p></div>
<div class="m2"><p>نکردی نظر سوی کس زان یار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مگر آنکه از سوی چین آمدی</p></div>
<div class="m2"><p>ز توران به خاور زمین آمدی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز شاهان نپرسیدی احوال کس</p></div>
<div class="m2"><p>مگر حال فغفور چین بود و بس</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دگر چون ملالش گرفتی ز تخت</p></div>
<div class="m2"><p>به خرگه شدی گرد فیروز بخت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همی بزم عشرت برآراستی</p></div>
<div class="m2"><p>ز ترکان چینی قدح خاستی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به یاد پری‌دخت سیمین‌بدن</p></div>
<div class="m2"><p>مه خوبرویان چین و ختن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>قدح نوش کردی و بگریستی</p></div>
<div class="m2"><p>که گر می نخوردی کجا زیستی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز شب‌ها مگر یک شبی همچو روز</p></div>
<div class="m2"><p>همی خورد می سام مجلس فروز</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نه شب بود از روشنی روز بود</p></div>
<div class="m2"><p>شبی خوش‌تر از روز نوروز بود</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هوا مشک بوی و صبا مشک‌بیز</p></div>
<div class="m2"><p>سر زلف مشکین شب مشک ریز</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هوا را مشام از هوا عنبرین</p></div>
<div class="m2"><p>شده ناف شب نافه مشک چین</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دُرافشان شده مه بدین سبز باغ</p></div>
<div class="m2"><p>چو در دست زنگی فروزان چراغ</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بر آواز مرغان شیرین سخن</p></div>
<div class="m2"><p>کهن سال چرخ فلک چرخ زن</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>خوش آواز بزم فلک نغمه‌ساز</p></div>
<div class="m2"><p>هم آوازش ناهید بربطنواز</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>شده همدم صبح خیزان نسیم</p></div>
<div class="m2"><p>صبوحی کشان راثریا ندیم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خوش الحان بزم، فلک در سماع</p></div>
<div class="m2"><p>جهان روز را کرده آن شب وداع</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>فرو بسته صبح از تحیر نفس</p></div>
<div class="m2"><p>به جنبش درآورده مرغان جرس</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>در آن شب که خلوتگه خاص بود</p></div>
<div class="m2"><p>به بزم افق زهره رقاص بود</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به زرین قدح باده لعل رنگ</p></div>
<div class="m2"><p>روان در کف ساقی شوخ و شنگ</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به جام بلورین می لعل ناب</p></div>
<div class="m2"><p>که جام آسمان بود و می آفتاب</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تو گفتی قدح جام جمشید بود</p></div>
<div class="m2"><p>و یا می‌ فروزنده خورشید بود</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>طرب نای در چنگ مستان زده</p></div>
<div class="m2"><p>مغنی به صد دست دستان زده</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>می چون عقیق اندر آن انجمن</p></div>
<div class="m2"><p>درخشنده همچون سهیل یمن</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ترنم سرایان پرده‌سرای</p></div>
<div class="m2"><p>به پرده سرا بسته پرده سرای</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>فروزنده رخ سام روشن ضمیر</p></div>
<div class="m2"><p>چو خورشید با لاجوردی سریر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چو بادام ترکان چین نیم مست</p></div>
<div class="m2"><p>هوا در سر و جام نوشین به دست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>کماندار چشمش به تیرافگنی</p></div>
<div class="m2"><p>دو هندوش در عین قلب افگنی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>هنوزش ازین گنبد لاجورد</p></div>
<div class="m2"><p>به گرد مه از مهر بنشسته گرد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ز مستی کله برده بر طرف گوش</p></div>
<div class="m2"><p>چو مستان برآورده از می خروش</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>گرانمایه قلواد کابل نژاد</p></div>
<div class="m2"><p>گهی نوش می‌خورد و گه نوش باد</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>پری چهره ترکان مجلس فروز</p></div>
<div class="m2"><p>به شب شام را بسته بر نیم روز</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>پلنگ افکنان شیرگیر آمده</p></div>
<div class="m2"><p>خرد جام می را اسیر آمده</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>حریفان ندیمان شیرین‌سخن</p></div>
<div class="m2"><p>ندیمان حریفان سیمین بدن</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چو بادام ساقی همه مست ناب</p></div>
<div class="m2"><p>ز جرعه شده جمله مست خراب</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>نواگر بتان بر گرفته سرود</p></div>
<div class="m2"><p>زده چنگ در زهره آواز رود</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>دل سام در ساغر آویخته</p></div>
<div class="m2"><p>ز نرگس می اندر قدح ریخته</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چو جم جام یاقوت برداشته</p></div>
<div class="m2"><p>یکی چتر ز آتش برافراشته</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>برافروخته زآتش می عذار</p></div>
<div class="m2"><p>برانگیخته ز آتش دل شرار</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>گل از دفتر حسن او یک ورق</p></div>
<div class="m2"><p>برآورده گلبرگش از می عرق</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>کمربسته قلواد زرین کلاه</p></div>
<div class="m2"><p>ستاده به یک گوشه بارگاه</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ز می شیرگیران شده شیرگیر</p></div>
<div class="m2"><p>برآورده بر شیر گردون نفیر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>گو شیردل مست و مدهوش بود</p></div>
<div class="m2"><p>دو آهوش درخواب خرگوش بود</p></div></div>