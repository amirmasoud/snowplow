---
title: >-
    بخش ۱۵۱ - جنگ کردن سام با سمندان و کشته شدن سمندان
---
# بخش ۱۵۱ - جنگ کردن سام با سمندان و کشته شدن سمندان

<div class="b" id="bn1"><div class="m1"><p>بگفت این و یک حمله آورد دیو</p></div>
<div class="m2"><p>سوی جاودان نعره بر زد غریو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که ای نره دیوان با خشم و تاب</p></div>
<div class="m2"><p>درآئید ای مالکان عذاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدان تا بگیرم من این بدنشان</p></div>
<div class="m2"><p>رسیدند هر سوی اهریمنان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هجوم آوریدند در پای کوه</p></div>
<div class="m2"><p>رسیدند هر سوی اهریمنا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه دیو آتش خور رزمساز</p></div>
<div class="m2"><p>همه آتش‌انداز با سوز و ساز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو سام آن چنان دید پیکار جنگ</p></div>
<div class="m2"><p>بجوئید از خشم جنگی پلنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشست از بر چرمه سخت سم</p></div>
<div class="m2"><p>که از گرد سمش فلک گشت گم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برآورد آن آتش آبدار</p></div>
<div class="m2"><p>یکی آتش انداخت در کارزار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به قلواد و شاپور فرزانه مرد</p></div>
<div class="m2"><p>یکی نعره زد شیر اندر نبرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کزین بد تنان دشت سازید پاک</p></div>
<div class="m2"><p>برآرید ازین تیره دیوان هلاک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مترسید از رزم دیو عذاب</p></div>
<div class="m2"><p>که سازم من این کوه آذرخراب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکی را نمایم ازین کوه سنگ</p></div>
<div class="m2"><p>به فرمان یزدان با فر و هنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کشم آتش دوزخ از خونشان</p></div>
<div class="m2"><p>که از من بماند در عالم نشان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به یزدان پناهید در رزمگاه</p></div>
<div class="m2"><p>که او می‌دهد بنده را دستگاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به حمله درآمد یل پهلوان</p></div>
<div class="m2"><p>همی نام برگفت روشن روان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به سوی سمندر درآمد دلیر</p></div>
<div class="m2"><p>به چنگال شمشیر چون نره شیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سمندر بدو هم یکی حمله کرد</p></div>
<div class="m2"><p>کزان دره برخاست از چرخ گرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عمودی درانداخت بر سام یل</p></div>
<div class="m2"><p>بتندید و بگشاد بر وی بغل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سپر بر سر آورد سام سوار</p></div>
<div class="m2"><p>سپرده دل و جان به پروردگار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سمندر فرو کوفت گرز گران</p></div>
<div class="m2"><p>که دوزخ شد از بیم گرزش نوان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که آواش پیچید بر بزر کوه</p></div>
<div class="m2"><p>همه کوه از آواز او شد ستوه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سر پهلوان را نشد زو خبر</p></div>
<div class="m2"><p>به فرمان دادار فیروزگر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سمندر دگر کوفت گرز گران</p></div>
<div class="m2"><p>به ماننده پتک آهنگران</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سه گرز پیاپی بزد نره دیو</p></div>
<div class="m2"><p>که از کوه و هامون برآمد غریو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بدو گفت جنگی سپهدار سام</p></div>
<div class="m2"><p>که ای دیو تیره دل تیره کام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عمود مرا هم یکی نوش کن</p></div>
<div class="m2"><p>همه رزم خود را فراموش کن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بخندید بر سام دیو پلید</p></div>
<div class="m2"><p>که گرز تو مویم نیارد خلید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به من داد شداد کوپال جنگ</p></div>
<div class="m2"><p>نتابد به چنگال من خاره سنگ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو من ابروان را درآرم به تاب</p></div>
<div class="m2"><p>تنت را دراندازم اندر عذاب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سپهبد چو بشنید برکرد اسب</p></div>
<div class="m2"><p>بیامد برش همچو آذرگشسب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عمود نریمان برآورد شیر</p></div>
<div class="m2"><p>فرو کوفت بر ترک دیو دلیر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>که مغز سر و کتف و یال و برش</p></div>
<div class="m2"><p>همه نرم گردید با پیکرش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو دیدند دیوان مر آن یال و برز</p></div>
<div class="m2"><p>چنان فره و رزم و آن دست و گرز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شگفتی بماندند از آن کامیاب</p></div>
<div class="m2"><p>کز آن گونه کشته است دیو عذاب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز سوی دگر گرد شاپور شیر</p></div>
<div class="m2"><p>سر دیو دیگر درآورد زیر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به هر سو خروشید چون پیل مست</p></div>
<div class="m2"><p>نموده به دیوان همان ضرب دست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همان گرد قلواد زرین کلاه</p></div>
<div class="m2"><p>به شمشیر می‌کشت در رزمگاه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بکشتند چندان ز دیوان عاد</p></div>
<div class="m2"><p>که از دره خون رو به دریا نهاد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گروه دگر دیو اندر دره</p></div>
<div class="m2"><p>در آتش فکندند خود یکسره</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شده نره دیوان دل گمرهان</p></div>
<div class="m2"><p>بسان سمندر در آتش نهان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دو روز و دو شب پهلو نامدار</p></div>
<div class="m2"><p>همی کشت دیو اندر آن کوهسار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سوم روز بر شد فراز دره</p></div>
<div class="m2"><p>جهان پر ز آتش شده یکسره</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همی شعله بر چرخ خور می‌کشید</p></div>
<div class="m2"><p>ابر خرمن ماه سر می‌کشید</p></div></div>