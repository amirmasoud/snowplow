---
title: >-
    بخش ۵۶ - مناظره کردن سام با پریدخت
---
# بخش ۵۶ - مناظره کردن سام با پریدخت

<div class="b" id="bn1"><div class="m1"><p>به زلف تو تا سر درآورده‌ام</p></div>
<div class="m2"><p>به آشفتگی سر برآورده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز موی میان تو موئی شدم</p></div>
<div class="m2"><p>ز مشک تو قانع به بوئی شدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ضعیفی که افگندیش در کمند</p></div>
<div class="m2"><p>گرش می‌کشی در به رویش مبند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غریبی که امیدش از خان تست</p></div>
<div class="m2"><p>درش باز کن زان که مهمان تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کریمان کسی را که مهمان کنند</p></div>
<div class="m2"><p>دلش را نشاید که رنجان کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترا گرچه نیروی سرپنجه است</p></div>
<div class="m2"><p>به خون ضعیفان میالای دست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به کویت ز روی نیاز آمدم</p></div>
<div class="m2"><p>به بویت ز راه دراز آمدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درم باز کن تا کشم در برت</p></div>
<div class="m2"><p>اگرنه بمیرم ز غم بر درت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلم باز کش تا جفایت کشم</p></div>
<div class="m2"><p>مکش سر که در زیر پایت کشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شکر لب لب درفشان برگشود</p></div>
<div class="m2"><p>به شیرین زبانی زبان برگشود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که شاها سرت سبز و رخ لعل باد</p></div>
<div class="m2"><p>سمند ترا ماه نو نعل باد</p></div></div>