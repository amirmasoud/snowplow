---
title: >-
    بخش ۲ - گفتار در ستایش خرد و عقل
---
# بخش ۲ - گفتار در ستایش خرد و عقل

<div class="b" id="bn1"><div class="m1"><p>خرد برتر از هر چه ایزدت داد</p></div>
<div class="m2"><p>ستایش خرد را به از روی داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خرد رهنما و خرد دلگشای</p></div>
<div class="m2"><p>خرد دست گیرد به هر دو سرای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازو شادمانی ازو مردمیست</p></div>
<div class="m2"><p>ازو بر فزونی و هم زو کمیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خرد تیره و مرد روشن‌روان</p></div>
<div class="m2"><p>نباشد همی شادمان یک زمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هشی‌وار و دیوانه خود ورا</p></div>
<div class="m2"><p>همان خویش و بیگانه خواند ورا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازوئی بهر دو سرا ارجمند</p></div>
<div class="m2"><p>گسسته خرد پای دارد به بند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خرد چشم جانست چون بنگری</p></div>
<div class="m2"><p>تو بی‌چشم روشن جهان نسپری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نخست آفرینش خرد را شناس</p></div>
<div class="m2"><p>نگهبان جان است و آن سه پاس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سپاس تو چشم است و گوش و زبان</p></div>
<div class="m2"><p>کزین سه رسد نیک و بد بی‌گمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خرد را و جان را که داند ستود</p></div>
<div class="m2"><p>وگر من ستایم که یارد شنود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حکیما چو کس نیست گفتن چه سود</p></div>
<div class="m2"><p>ازین پس بگو کافرینش چه بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حکیما چو کس نیست گفتن چه سود</p></div>
<div class="m2"><p>ازین پس بگو کافرینش چه بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>توئی کرده کردگار جهان</p></div>
<div class="m2"><p>شناسنده آشکار و نهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به یزدان‌گرای و بدو راه جوی</p></div>
<div class="m2"><p>به گیتی بپوی و به هر کس بگوی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز هر دانشی چون سخن بشنوی</p></div>
<div class="m2"><p>ز آموختن یک زمان نغنوی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو دیدار یابی به شاه سخن</p></div>
<div class="m2"><p>بدانی که دانش نیاید به بن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از آغاز باید که دانی درست</p></div>
<div class="m2"><p>سرمایه گوهران از نخست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که یزدان ز ناچیز چیز آفرید</p></div>
<div class="m2"><p>بدان تا توانائی آید پدید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وزو مایه گوهر آمد چهار</p></div>
<div class="m2"><p>برآورد بی‌رنج و بی‌روزگار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یکی آتشی برشده تابناک</p></div>
<div class="m2"><p>میان آب و باد از بر تیره خاک</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نخستین که آتش ز جنبش دمید</p></div>
<div class="m2"><p>ز گرمیش بس خشکی آمد پدید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وز آن پس از آرام سردی نمود</p></div>
<div class="m2"><p>ز سردی همان بازتری فزود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو این چارگوهر به جای آمدند</p></div>
<div class="m2"><p>ز بهر سپنجی سرای آمدند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گهرها یک اندر دگر ساختند</p></div>
<div class="m2"><p>ز هر گونه گردن برافراختند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پدید آمد این گنبد تیزرو</p></div>
<div class="m2"><p>شگفتی نماینده نو به نو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ابر ده و دو هفت شد کدخدای</p></div>
<div class="m2"><p>گرفتند هر یک سزاوار جای</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>فلکها یک اندر دگر بسته شد</p></div>
<div class="m2"><p>بجنبید چون کار پیوسته شد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زمین را بلندی نبد جایگاه</p></div>
<div class="m2"><p>یکی مرکز تیره بود و تباه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ستاره به سر بر شگفتی نمود</p></div>
<div class="m2"><p>به خاک اندرون روشنائی فزود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو دریا و چون دشت و چون باغ و راغ</p></div>
<div class="m2"><p>جهان شد به کردار روشن چراغ</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ببالید کوه آبها بردمید</p></div>
<div class="m2"><p>سر رستنی سوی بالا کشید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همی بر شد آتش فرو آمد آب</p></div>
<div class="m2"><p>همین گشت گرد زمین آفتاب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گیا رست با چند گونه درخت</p></div>
<div class="m2"><p>به زیر اندر آمد سرانشان ز تخت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ببالد ندارد جز این نیروئی</p></div>
<div class="m2"><p>نپوید چو پویندگان هر سوئی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>وز آن پس چو جنبیدن آمد پدید</p></div>
<div class="m2"><p>سر رستنی سوی بالا کشید</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سرش زیر آمد بسان درخت</p></div>
<div class="m2"><p>نگه کرد باید بدین کار سخت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خور و خواب و آرام جوید همی</p></div>
<div class="m2"><p>بدان زندگی کام جوید همی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نه گویا زبان و نه جویا خرد</p></div>
<div class="m2"><p>ز خار و ز خاشاک تن پرورد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نداند بد و نیک فرجام کار</p></div>
<div class="m2"><p>نخواهد ازو بندگی کردگار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو دانا تواند بدو دادگر</p></div>
<div class="m2"><p>ازیرا نکرد ایچ پنهان خبر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چنین است فرجام کار جهان</p></div>
<div class="m2"><p>نداند کسی آشکار و نهان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ترا دانش و دین رهاند درست</p></div>
<div class="m2"><p>در رستگاری ببایدت جست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اگر دل نخواهی که ماند نژند</p></div>
<div class="m2"><p>نخواهی که دایم بوی مستمند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به گفتار پیغمبرت راه جوی</p></div>
<div class="m2"><p>دل از تیرگیها بدین آب شوی</p></div></div>