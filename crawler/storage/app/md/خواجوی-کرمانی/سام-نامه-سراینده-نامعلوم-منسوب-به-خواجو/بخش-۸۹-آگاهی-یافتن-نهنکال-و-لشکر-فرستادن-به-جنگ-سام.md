---
title: >-
    بخش ۸۹ - آگاهی یافتن نهنکال و لشکر فرستادن به جنگ سام
---
# بخش ۸۹ - آگاهی یافتن نهنکال و لشکر فرستادن به جنگ سام

<div class="b" id="bn1"><div class="m1"><p>بره باز پس شد شهنشه به تخت</p></div>
<div class="m2"><p>وز آن پس بشد سام فیروزبخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به کوه و بیابان ره ایدر برید</p></div>
<div class="m2"><p>چنین تا به نزدیک دریا رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بفرمود تا چند کشتی در آب</p></div>
<div class="m2"><p>فکندند کشتی هم اندر شتاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هر کشتی‌ای کرد گردی‌پناه</p></div>
<div class="m2"><p>به کشتی روان کرد یکسر سپاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همی راند کشتی به دریا چو باد</p></div>
<div class="m2"><p>به ایشان مدد کرد باد مراد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز کارآگهان و ز جادوگران</p></div>
<div class="m2"><p>شنیدند چون از یل پهلوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ببردند سوی دیو جادو خبر</p></div>
<div class="m2"><p>ز احوال فغفور و یل سر به سر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو آن دیو آگاه شد زین خبر</p></div>
<div class="m2"><p>بخواندش ز جادوگران سر به سر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از ایشان چو احوال‌ها باز جست</p></div>
<div class="m2"><p>بگفتند شرح و بیان‌ها درست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که سام یل پهلو سرافراز</p></div>
<div class="m2"><p>ز مستی نداند سر از پای باز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز عشق پری دخت رفته ز دست</p></div>
<div class="m2"><p>نداند کسی حال آن خودپرست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز احوال قصر و ز باغ و ز بند</p></div>
<div class="m2"><p>بگفتند با نره‌دیو نژند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نهنکال ازین گفته آمد به جوش</p></div>
<div class="m2"><p>برآورد از دل فغان و خروش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که آیا کدام ابلهی بی‌خرد</p></div>
<div class="m2"><p>تواند که تا نام یارم برد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر بر پری‌دخت من بسته دل</p></div>
<div class="m2"><p>ز خونش کنم خاک ناورد گل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بگفتا و فرعین و عفریت خواند</p></div>
<div class="m2"><p>بر خویش ایشان به زانو نشاند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بگفتا ز دیوان گزین صدهزار</p></div>
<div class="m2"><p>به کشتی روان شو سوی کارزار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز دریا به کشتی نشین در زمان</p></div>
<div class="m2"><p>برو سوی سام یل پهلوان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دو دیو دلاور ابا صدهزار</p></div>
<div class="m2"><p>برو پیش رو باش در کارزار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دو پنجاه کشتی ز دریا روان</p></div>
<div class="m2"><p>برفتند دیوان بر پهلوان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>رسید از دو سو کشتیان درگذر</p></div>
<div class="m2"><p>به هم در رسیدند شیران نر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چنان شد که جویند از هم نبرد</p></div>
<div class="m2"><p>ز یکسوی دیو و به یک سوی مرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چنین گفت سام نریمان گرد</p></div>
<div class="m2"><p>که ای پهلوانان با دست برد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نمیرد کسی بی‌اجل در جهان</p></div>
<div class="m2"><p>نه زنده توان رفت به آسمان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نشد جانور رسته از چنگ مرگ</p></div>
<div class="m2"><p>درین باغ نی شاخ ماند نه برگ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پس آن به که نام نکو یادگار</p></div>
<div class="m2"><p>بماند ز ما تا بسی روزگار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بکوشید در جنگ دیوان نر</p></div>
<div class="m2"><p>به خون درکشید و ببرید سر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بگفت و بزد نعره‌ای در زمان</p></div>
<div class="m2"><p>که لرزید از وی زمین و زمان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اول بر زبان نام یزدان بخواند</p></div>
<div class="m2"><p>سوی جنگ عفریت کشتی براند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بفرمود تا تیرباران کنند</p></div>
<div class="m2"><p>به دیو سیه روز تازان کنند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز شصت دلیران در آن روی آب</p></div>
<div class="m2"><p>به بد کار دیوان جنگی خراب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خدنگ از کمان سواران جنگ</p></div>
<div class="m2"><p>چو رستی نشستی ابر دیو تنگ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تن دیو گفتی برآورد پر</p></div>
<div class="m2"><p>برهنه تن و نی کلاه و کمر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>یکی دشت بودی پر از خارپشت</p></div>
<div class="m2"><p>ز دیوان جنگی و تیر درشت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به کشتی نشستند دیوان ریو</p></div>
<div class="m2"><p>براندند زی سام سالار نیو</p></div></div>