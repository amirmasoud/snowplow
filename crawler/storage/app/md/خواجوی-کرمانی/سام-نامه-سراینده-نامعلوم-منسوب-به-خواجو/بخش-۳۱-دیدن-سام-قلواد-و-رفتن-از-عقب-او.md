---
title: >-
    بخش ۳۱ - دیدن سام، قلواد و رفتن از عقب او
---
# بخش ۳۱ - دیدن سام، قلواد و رفتن از عقب او

<div class="b" id="bn1"><div class="m1"><p>بدین سان چو پاسی ز شب درگذشت</p></div>
<div class="m2"><p>ز خون دل آتش ز سر درگذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نظر کرد آزاده قلواد را</p></div>
<div class="m2"><p>یلی راستی سرو آزاد را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشسته ندید اندر آن بارگاه</p></div>
<div class="m2"><p>برآورده بر چرخ گردنده آه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که آیا کجا رفت و حالش چه بود</p></div>
<div class="m2"><p>چه پیش آمد و در خیالش چه بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ملالش گر از باده بگرفته است</p></div>
<div class="m2"><p>و یا مست در گوشه خفته است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو قلواد را در شبستان ندید</p></div>
<div class="m2"><p>ز خرگه سراسیمه بیرون دوید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگردید در صحن بستان سرای</p></div>
<div class="m2"><p>بنالید چو مرغ دستان سرای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسی جست وجو کرد و او را ندید</p></div>
<div class="m2"><p>همی خواست از باغ بیرون دوید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز ناگه نظر کرد در پای سرو</p></div>
<div class="m2"><p>گرانمایه را دید همتای سرو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به خاک اندر افتاده چون پیل مست</p></div>
<div class="m2"><p>برون رفته هوش از سر و دل ز دست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سمن برگش از غم زریری شده</p></div>
<div class="m2"><p>رخ سرخش از هجر خیری شده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز پا اندر افتاده بر چشمه‌ای</p></div>
<div class="m2"><p>چو آزاده سروی به سرچشمه‌ای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ستاده به بالینش سرو بلند</p></div>
<div class="m2"><p>خم اندر خم افکنده مشکین کمند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دو زلفش دو گردن کش سرفراز</p></div>
<div class="m2"><p>دو چشمش دو آهوی روباه باز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شبش سایه‌بان بسته بر آفتاب</p></div>
<div class="m2"><p>سر زلفش افکنده بر ماه تاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رخش گلستان و لبش دلستان</p></div>
<div class="m2"><p>زده سنبلش حلقه بر گلستان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صد آشوب در بابل از جادویش</p></div>
<div class="m2"><p>شده ترک گردون ز جان هندویش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>قدش همچو نخلی ز گلزار جان</p></div>
<div class="m2"><p>چلیپای گیسوش ز نار جان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>میان موی و بر موش از مو کمر</p></div>
<div class="m2"><p>دهان تنگ شیرین چو تنگ شکر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دو گیسوش دلبند و رخ دلگشای</p></div>
<div class="m2"><p>وصالش روان‌بخش و لب جان فزای</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دل افروز خورشید شب زیورش</p></div>
<div class="m2"><p>روان‌بخش یاقوت جان پرورش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تواناش جادو ولی ناتوان</p></div>
<div class="m2"><p>دو آهوش هندو ولی دلستان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شهنشه چو آن زلف رخسار دید</p></div>
<div class="m2"><p>سرانگشت حیرت به دندان گزید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ندانست کان ماه یا روی اوست</p></div>
<div class="m2"><p>سواد شب ار زلف هندوی اوست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بدو گفت حوری بگو یا پری</p></div>
<div class="m2"><p>مه چرخ یا لعبت آذری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پری‌چهره خورشید شبگون نقاب</p></div>
<div class="m2"><p>چنین گفت کای گرد فرهنگ‌یاب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>منم مهرافروز آتش عذار</p></div>
<div class="m2"><p>رخم آتش و آب ازو شرمسار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چراغ چگل شمع توران زمین</p></div>
<div class="m2"><p>خور خاور و شاه خوبان چین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>فروزان رخم روز و شب زیور است</p></div>
<div class="m2"><p>کمین خادمم سنبلم عنبر است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بدو گفت سام ای بت خاوری</p></div>
<div class="m2"><p>ندانم چه کردی بدین یاوری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کز این گونه شیری شکار تو شد</p></div>
<div class="m2"><p>بدین خاک ره خاکسار تو شد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چه مرغی تو ای کبک طوطی خرام</p></div>
<div class="m2"><p>که افتادت این مرغ زیرک به دام</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>روان مهر افروز گفت ای جوان</p></div>
<div class="m2"><p>شنو حال ما را به روشن‌روان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو سلطان چشمم درآمد به صید</p></div>
<div class="m2"><p>درافتاد این صید لاغر به قید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خروشان پلنگی درآمد ز کوه</p></div>
<div class="m2"><p>شد از آهوی شیرگیرم ستوه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گوزنی مگر بر کمر می‌گذشت</p></div>
<div class="m2"><p>به هنگام نخجیر برطرف دشت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کماندار چشمم چو بگشود شست</p></div>
<div class="m2"><p>درافکندش از کوه چون پیل مست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مرنج ار زدم آهوئی را به تیر</p></div>
<div class="m2"><p>که او شیر نر بود و من شیرگیر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>من آن شاهبازم که بازان شاه</p></div>
<div class="m2"><p>نیاید به چشمم به نخجیرگاه</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به آهوی شیرافکن می‌پرست</p></div>
<div class="m2"><p>بسی کرده‌ام صید پیلان مست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بگفت این و دامن کشان برگذشت</p></div>
<div class="m2"><p>روان همچو سرو روان درگذشت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به ایوان فرو شد چو تابنده ماه</p></div>
<div class="m2"><p>بماند از پیش چشم فرخنده شاه</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو بگرفت قلواد را سام دست</p></div>
<div class="m2"><p>همان گاه قلواد بر پای جست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو سروی به پای یل اندر فتاد</p></div>
<div class="m2"><p>همه راز دل پیش او برگشاد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>که ای بر همه سرکشان نامدار</p></div>
<div class="m2"><p>مرا اندرین ورطه معذور دار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ترا عیب کردم به دیوانگی</p></div>
<div class="m2"><p>که مشهور بودم به فرزانگی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>کنون آن چنان گشته‌ام پای‌بند</p></div>
<div class="m2"><p>که هرگز نیایم برون از کمند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>غریقم به بحری که پایانش نیست</p></div>
<div class="m2"><p>امیرم به دردی که درمانش نیست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>دلم دانه‌ای دید و پر بر گشاد</p></div>
<div class="m2"><p>بدان دانه در دام غم اوفتاد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو چشمم به آن چشم بادام بود</p></div>
<div class="m2"><p>ندانست کان دانه یا دام بود</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>دلی داشتم پیش ازین برقرار</p></div>
<div class="m2"><p>خردمند و فرمان‌بر و هوشیار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ببرد آن دلم ناگهان دلبری</p></div>
<div class="m2"><p>زبون گشته در دست زورآوری</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>من آنم که دایم به عقل و به رای</p></div>
<div class="m2"><p>ترا بودمی در خرد رهنمای</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>در اقصای عزلت مکان داشتم</p></div>
<div class="m2"><p>به قاف خرد آشیان داشتم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چو باز سفید از سر دست شاه</p></div>
<div class="m2"><p>زدم بال بر قبه بارگاه</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به پرواز رفتم در ایوان عشق</p></div>
<div class="m2"><p>گرفتم هوا در گلستان عشق</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چو بلبل به باغ آشیان ساختم</p></div>
<div class="m2"><p>بدین دام خود را در انداختم</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>تو هم صید این دام و این دانه‌ای</p></div>
<div class="m2"><p>به شوریدگی چون من افسانه‌ای</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>مرا دل ده اکنون چو دل داده‌ام</p></div>
<div class="m2"><p>به دام محبت درافتاده‌ام</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>تو دانی مگر سوز آتش که چیست</p></div>
<div class="m2"><p>که هم شمع داند که پروانه کیست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>کسی آگه از درد پنهان بود</p></div>
<div class="m2"><p>که آشفته زلف جانان بود</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>طبیب ار به دردی گرفتار نیست</p></div>
<div class="m2"><p>مر او را غم از درد بیمار نیست</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>تو دانی که در ده شتر راندگان</p></div>
<div class="m2"><p>ندانند احوال واماندگان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ز سوز دل آنها خبر داده‌اند</p></div>
<div class="m2"><p>که از دل درین آتش افتاده‌اند</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ترا عیب می‌کردم اندر الم</p></div>
<div class="m2"><p>کنون غرقه گشتم به دریای غم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>دلم از می عاشقی مست شد</p></div>
<div class="m2"><p>مگر دستگیری که از دست شد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>که چشمم بدان چشم بادام بود</p></div>
<div class="m2"><p>ندانست کان دانه یا دام بود</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>از آن با تو می‌گویم این ماجرا</p></div>
<div class="m2"><p>که درد دلم را تو دانی دوا</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>روان سام نیرم ورا پند داد</p></div>
<div class="m2"><p>پس آنگه به پاسخ زبان برگشاد</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>که ای رفته از دیده پایت به گل</p></div>
<div class="m2"><p>خرد رفته از دست و از دست دل</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چنین صید تیرنظر گشته‌ای</p></div>
<div class="m2"><p>برو سر بنه زان که سرگشته‌ای</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>درین وادی اینها که ره رفته‌اند</p></div>
<div class="m2"><p>که جان داده دل را به در برده‌اند</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بر آن کس حرامست دعوی عشق</p></div>
<div class="m2"><p>که در خود نبیند تجلی عشق</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>حقیقت در آن چون بدین حی رسند</p></div>
<div class="m2"><p>چو از خود گذشتند در وی رسند</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ز جان درگذر تا به جانان رسی</p></div>
<div class="m2"><p>چو در درد میری به درمان رسی</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>تو در عشق اگر مرده‌ای، زنده‌ای</p></div>
<div class="m2"><p>چو در بند خویشی از آن بنده‌ای</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بسا کس که جان داد و جانان نیافت</p></div>
<div class="m2"><p>فرو رفت در درد و درمان نیافت</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ز میدان جانان کسی جان نبرد</p></div>
<div class="m2"><p>که خون خورد و بر خاک جانان نمرد</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>برو خون خور و خون خود کن سبیل</p></div>
<div class="m2"><p>که آتش، گلستان بود بر خلیل</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>در آتش بسوز ار دم از دل زنی</p></div>
<div class="m2"><p>کز آتش بود شمع را روشنی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>چو یک چند ازین داستان گفت سام</p></div>
<div class="m2"><p>به آرامگه شد یل نیک‌نام</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>به پیش اندر آن سام شد ره‌سپر</p></div>
<div class="m2"><p>که آید به شادی نشیند مگر</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>که ناگه برآمد ز روی هوا</p></div>
<div class="m2"><p>غریوی که شد سام از هش جدا</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>ز مهتاب بستان سرا روز بود</p></div>
<div class="m2"><p>هوا هم چو روی دلفروز بود</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>ز ناگه هوا یکسره تیره شد</p></div>
<div class="m2"><p>وز ان چشم قلواد یل خیره شد</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>زمانی چو شد چشم را کرد باز</p></div>
<div class="m2"><p>نشانی ندید از گو سرفراز</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>گریبان ز اندوه جان چاک کرد</p></div>
<div class="m2"><p>به سر بر زد و روی بر خاک کرد</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>بماندند ازو انجمن درشگفت</p></div>
<div class="m2"><p>جدا هر یکی راه بستان گرفت</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>که شاید نشانی ز فرخنده سام</p></div>
<div class="m2"><p>بیابند گردند دل شادکام</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>کنون رخ بتابان ازین جا دری</p></div>
<div class="m2"><p>سخن بشنو از شمسه خاوری</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>ملک ضیمران شاه خاورزمین</p></div>
<div class="m2"><p>یکی دخترش بود چون حور عین</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>به بالا خرامنده سرو بلند</p></div>
<div class="m2"><p>به گیسو برآشفته مشکین کمند</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>درخشان رخش چشمه آفتاب</p></div>
<div class="m2"><p>درافشان لبش چشمه نوشیاب</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>دو برگ گلش سوسن مشک پوش</p></div>
<div class="m2"><p>دو لعل لبش شهد و شکر فروش</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>شب دلستانش شبستان دل</p></div>
<div class="m2"><p>گل لاله رنگش گلستان دل</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>دو جادوی مخمورش از خواب مست</p></div>
<div class="m2"><p>دو هندوش در آب افگنده شست</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>لبش نوش داروی هر دردمند</p></div>
<div class="m2"><p>سر زلفش آشوب هر پای‌بند</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>سیه زلف در زلف مشکینش ماه</p></div>
<div class="m2"><p>زنج سیب و در سیب دلگیر جاه</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>سمن بوی نسرین بر و خوش‌خرام</p></div>
<div class="m2"><p>به رخ شمع طلعت بدش شمسه نام</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>مگر در گذر سام را دیده بود</p></div>
<div class="m2"><p>به رخسار او گرم گردیده بود</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>دلش رفته از دست و پایش به گل</p></div>
<div class="m2"><p>مهش رفته از چشم و صبرش ز دل</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>شده آهوی چشم صیدافکنش</p></div>
<div class="m2"><p>شکسته دل از زلف قیدافکنش</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>چو بلبل شده فتنه برگلشنی</p></div>
<div class="m2"><p>چو آهو شده صید شیرافکنی</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>برآشفته چون چین گیسوی خویش</p></div>
<div class="m2"><p>دو تا گشته چون طاق ابروی خویش</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>چو بادام میگون شده نیم مست</p></div>
<div class="m2"><p>برون رفته چون زلف مشکین زدست</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>دلش دست در دامن جان زده</p></div>
<div class="m2"><p>غمش چنگ در زلف جانان زده</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>ولیکن کس از خویش و اخوان او</p></div>
<div class="m2"><p>نبود آگه از حال و سامان او</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>بجز اشک گرمش که همراز بود</p></div>
<div class="m2"><p>و یا آه سردش که دم ساز بود</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>چو دید آن چنان مهر افروز را</p></div>
<div class="m2"><p>بت یاسمن بوی فیروز را</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>برآشفت و گفت ای برآشفته موی</p></div>
<div class="m2"><p>کجا بوده تیره شب بازگوی</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>پراکنده زلف از کجا می‌رسی</p></div>
<div class="m2"><p>ز بستان چو باد صبا می‌رسی</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>به بوی که در باغ گردیده‌ای</p></div>
<div class="m2"><p>به روی که چون غنچه خندیده‌ای</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>چو سرو از چمن می‌رسی راستی</p></div>
<div class="m2"><p>مگر فتنه بودی که برخاستی</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>ز برگ سمن آب گل برده‌ای</p></div>
<div class="m2"><p>دل لاله از غصه خون کرده‌ای</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>مگر با صنوبر سری داشتی</p></div>
<div class="m2"><p>که در بوستان قد برافراشتی</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>به بالا بلا بوده تا بود?</p></div>
<div class="m2"><p>بگو راستی تا کجا بود?</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>دو هندوت آیا بر آتش چراست</p></div>
<div class="m2"><p>کمان‌دار چشمت کمان‌کش چراست</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>سمن بر چو گل زان سخن برشکفت</p></div>
<div class="m2"><p>خم آورد در سرو سیمین و گفت</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>که ای آفتاب سپهر جمال</p></div>
<div class="m2"><p>ندیده سپهرت به خوبی مثال</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>به برج شرف شمسه دلبری</p></div>
<div class="m2"><p>قمر مهر روی ترا مشتری</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>جهان ملاحت سراسر تراست</p></div>
<div class="m2"><p>بگویم چو آزادسروی و راست</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>دلم همچو پسته دهان تنگ بود</p></div>
<div class="m2"><p>زمانی به بستانش آهنگ بود</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>دگر چون شنیدم که فرخنده سام</p></div>
<div class="m2"><p>قدح نوش می‌کرد و با فر و کام</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>مرا در دل آمد که در گوش?</p></div>
<div class="m2"><p>بچینم ز باغ رخش خوش?</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>نهم گوش بر قول مطرب دمی</p></div>
<div class="m2"><p>به مرغ چمن بازگویم غمی</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>ولی هندویم چون که بنمود دست</p></div>
<div class="m2"><p>درافتاد ماهی چو ماهی به شست</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>خدنگ افکن شیرگیرم چو تیر</p></div>
<div class="m2"><p>گوزنی بزد بر لب آب‌گیر</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>ولیکن چو تیرم برون شد به شست</p></div>
<div class="m2"><p>خطا کرده در شاه‌بازی نشست</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>چو آن شاهباز از هوا در رسید</p></div>
<div class="m2"><p>همان لحظه سام از قفا در رسید</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>برآمد ز مرغان بلبل نوا</p></div>
<div class="m2"><p>به یک ره خروشی که ای بینوا</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>چه مرغی که سیمرغت آید به دام</p></div>
<div class="m2"><p>چه برجی که خورشیدت آمد به نام</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>توتیهو و طاووس شد صید تو</p></div>
<div class="m2"><p>همان ایرج و سام در قید تو</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>چو صبح امیدم دمیدن گرفت</p></div>
<div class="m2"><p>دو چشم نشاطم پریدن گرفت</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>یلی دیدم از شهر شاهنشهی</p></div>
<div class="m2"><p>به قد راست مانند سرو سهی</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>خرامنده سروی به طالع چو ماه</p></div>
<div class="m2"><p>چو گل رفته در ارغوانی کلاه</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>چو خورشید بد سام گیتی گشای</p></div>
<div class="m2"><p>چو جمشید با جام گیتی‌نمای</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>خط سبزش افکنده دفتر در آب</p></div>
<div class="m2"><p>سر زلفش افکنده چنبر به خواب</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>بیفکنده طوطیش پر بر شکر</p></div>
<div class="m2"><p>فکنده لبش شوری اندر شکر</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>ولی شمسه چون گفته می‌کرد گوش</p></div>
<div class="m2"><p>درو خیره می‌گشت و می‌شد ز هوش</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>چو باز آمدی گفتی ای ماه روی</p></div>
<div class="m2"><p>چو دیدی بیا یک به یک بازگوی</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>بدانست مهوش که آن راز چیست</p></div>
<div class="m2"><p>دل شمسه در بند سودای کیست</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>به لعل بدخشان زمین بوسه داد</p></div>
<div class="m2"><p>پس آنگه لب درفشانی گشاد</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>به صد لابه گفت ای بت دل گسل</p></div>
<div class="m2"><p>نگار ختن شمع چین و چگل</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>چو دانی که در هر دمت همدمم</p></div>
<div class="m2"><p>به هر حال در خدمتت محرمم</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>اگر زان که گشتی گرفتار دل</p></div>
<div class="m2"><p>چه پنهان کنی از من اسرار دل</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>کسی را که دردی بود از حبیب</p></div>
<div class="m2"><p>نشاید که پنهان کند از طبیب</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>پری‌وار در پرده رانی سخن</p></div>
<div class="m2"><p>بیا پرده از کار خود برفکن</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>بت بربری لعبت آذری</p></div>
<div class="m2"><p>کجا شمسه آن بانوی خاوری</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>به خنده سر درج در برگرفت</p></div>
<div class="m2"><p>لب درفشان را به در درگرفت</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>که خاموش کن گفته ناگفتنت</p></div>
<div class="m2"><p>وزین گونه دُردانه ناسفتنت</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>شدم صید شیرافگنی در شکار</p></div>
<div class="m2"><p>چو خورشید بر شیر گردون سوار</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>مگر سام بر مسند ناز بود</p></div>
<div class="m2"><p>مرا چشم بر روی او باز بود</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>گرفتم هوا هم چو باز سفید</p></div>
<div class="m2"><p>هوا در سر و چشم و دل در امید</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>که باشد که چون بر هوایش پرم</p></div>
<div class="m2"><p>مگر سایه افکند بر سرم</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>همم بال بشکست و هم پر بریخت</p></div>
<div class="m2"><p>ز تیغ قضا چون توانم گریخت</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>دلم مبتلایست و جان پرغمست</p></div>
<div class="m2"><p>ز سوز درون دیده‌ام پر نمست</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>ز هر چه نشاید توان گفت باز</p></div>
<div class="m2"><p>که بسیار چیزست با سوز و ساز</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>نگار پری چهره آن دم به سوز</p></div>
<div class="m2"><p>دلش باز می‌داد کای دلفروز</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>مخور غم که غمخور همه خون خورد</p></div>
<div class="m2"><p>چو آتش همه آب مردم برد</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>مبادا گلت زعفرانی شود</p></div>
<div class="m2"><p>به خون نرگست ارغوانی شود</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>پری‌وش نگاری که دلخواه تست</p></div>
<div class="m2"><p>به تیره شبان طلعتش ماه تست</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>مخور غم که او نیز غمخواره‌ایست</p></div>
<div class="m2"><p>دلش فتنه روی مه پاره‌ایست</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>طبیب ار به دردی نشد پای بند</p></div>
<div class="m2"><p>چه داند دوای دل دردمند</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>شود سام یل گر سپهر آشیان</p></div>
<div class="m2"><p>و یا همچو عنقا شود بی‌نشان</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>میندیش کو هم درآید به دام</p></div>
<div class="m2"><p>شبی همچو روزت برآید به بام</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>بیا تا به شادی بنوشیم می</p></div>
<div class="m2"><p>روان شاد سازیم از بانگ نی</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>چو غم جان بکاهد خرد کم شود</p></div>
<div class="m2"><p>چه گفتم دو دیده پر از نم شود</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>سراینده از سام فرخ نژاد</p></div>
<div class="m2"><p>شنیدم که زینسان سخن کرد یاد</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>که با آذرافروز مهر و سخن</p></div>
<div class="m2"><p>بدید و سوی قصر شد ز انجمن</p></div></div>