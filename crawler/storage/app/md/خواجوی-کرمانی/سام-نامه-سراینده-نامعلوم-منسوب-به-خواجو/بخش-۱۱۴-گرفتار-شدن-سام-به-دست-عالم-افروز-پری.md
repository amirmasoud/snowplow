---
title: >-
    بخش ۱۱۴ - گرفتار شدن سام به دست عالم افروز پری
---
# بخش ۱۱۴ - گرفتار شدن سام به دست عالم افروز پری

<div class="b" id="bn1"><div class="m1"><p>وز آن سو جهان پهل شیر نر</p></div>
<div class="m2"><p>سوی لشکر خویش شد ره‌سپر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تکش خان و قلواد شمشیرزن</p></div>
<div class="m2"><p>تمرتاش و قلوش صف شکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برانگیخته ابرش تیزگام</p></div>
<div class="m2"><p>روان گشته یکسر به دنبال سام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر چشمه‌ای از قضا در رسید</p></div>
<div class="m2"><p>یکی آهوی پر خط و خال دید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه آهو عروسیست گفتی به جای</p></div>
<div class="m2"><p>به بندش جهان‌پهلوان کرد رای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برانگیخت از جا غراب نوند</p></div>
<div class="m2"><p>ز فتراک بگشاد پیچان کمند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رمیده شد آن آهوی شیرفر</p></div>
<div class="m2"><p>سوی دشت شد چون صبا ره‌سپر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پسش بارگی راند سام دلیر</p></div>
<div class="m2"><p>به دستش کمندی بد از چرم شیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو آمد به نزدیک آهو غراب</p></div>
<div class="m2"><p>همان دم جهان‌پهلوان کامیاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر آهو بینداختش خم خام</p></div>
<div class="m2"><p>چو صرصر برون جست آهو ز دام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کمان را به زه کرد مرد دلیر</p></div>
<div class="m2"><p>ز ترکش برون کرد یک چوبه تیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بینداخت بر وی نیامد صواب</p></div>
<div class="m2"><p>براند از پی او دمادم غراب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همی رفت آهو و سام از پیش</p></div>
<div class="m2"><p>به همراه نبود از دلیران کسش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر آن دشت تا هفت فرسنگ راند</p></div>
<div class="m2"><p>بسی نام یزدان بر آن راه خواند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دگر ره چو نزدیک آهو رسید</p></div>
<div class="m2"><p>حصاری به ناگاه آنجا بدید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کجا بود ز آهن درون حصار</p></div>
<div class="m2"><p>خروشنده مانند شیر شکار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که ای صاحب قلعه بگشای در</p></div>
<div class="m2"><p>و یا از سر باره بر من نگر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از آن قلعه کس پاسخ او نداد</p></div>
<div class="m2"><p>فرود آمد از باره مانند باد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بزد بر زمین نیزه آهنین</p></div>
<div class="m2"><p>بدان سان که لرزید روی زمین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو بنشست بر خاک تیره سنان</p></div>
<div class="m2"><p>برو بست مر بارگی را عنان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وز آن پس به تندی همی کوفت در</p></div>
<div class="m2"><p>ز گشت سپهری نبد باخبر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پس در ز ناگه صدائی شنید</p></div>
<div class="m2"><p>جهان‌پهلوان چون به پس بنگرید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نه نیزه بدید و نه بر جا غراب</p></div>
<div class="m2"><p>شد از بس شگفتی دلش پر ز تاب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همی گفت کاین جای اهریمن است</p></div>
<div class="m2"><p>که او آدمی‌زاده را دشمن است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در اندیشه بد آن گو سرفراز</p></div>
<div class="m2"><p>به ناگه در حصن گردید باز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یکی باغ خرم پدیدار شد</p></div>
<div class="m2"><p>کزان سام را رخ چو گلنار شد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دلش گرچه از بارگی داشت داغ</p></div>
<div class="m2"><p>ولیکن ز دشت اندر آمد به باغ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو گفتی بهشتی است آراسته</p></div>
<div class="m2"><p>مهیا درو هر چه دل خواسته</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به هر سو گلستان بد آن مرغزار</p></div>
<div class="m2"><p>درختان ز هر گونه‌ای میوه دار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به نزد چمن در یکی رود آب</p></div>
<div class="m2"><p>درخشان‌تر از چشمه آفتاب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بیامد بر رود مرد دلیر</p></div>
<div class="m2"><p>نشستنگهی دید بر آبگیر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز بس خستگی خواست تا نوشد آب</p></div>
<div class="m2"><p>چو کرد از پی آب خوردن شتاب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خروشی برآمد همانگه چو دود</p></div>
<div class="m2"><p>که ای سام بختت همانا غنود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نبینی دگر روی یار و دیار</p></div>
<div class="m2"><p>جز ازغم نیابی دگر غمگسار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چه سانت رساندم درین سبز باغ</p></div>
<div class="m2"><p>وزین ساختم پر ز خونت دماغ</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>که دیگر ره رفتنت نیست هیچ</p></div>
<div class="m2"><p>فتادی درین عقده پیچ‌پیچ</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اگر خود ندانی که من کیستم</p></div>
<div class="m2"><p>سخن گوش کن از پی چیستم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>منم عالم افروز برگشته روز</p></div>
<div class="m2"><p>که با من نسازد شه نیمروز</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گر امروز کامم برآری رواست</p></div>
<div class="m2"><p>وگر نه کنم هر چه بر تو سزاست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>که گشتم ز چنگال حرمان زبون</p></div>
<div class="m2"><p>مرا کرده یکبار هجران زبون</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چنین چند خون گریم از دوریت</p></div>
<div class="m2"><p>بی‌آرام باشم ز محجوریت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تو یار پری‌دخت و من یار غم</p></div>
<div class="m2"><p>چنین چند باشم گرفتار غم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>وگر با من امروز جوئی فراغ</p></div>
<div class="m2"><p>همانا روی سوی لشکر ز باغ</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نبینم اگر از تو خود رای و کام</p></div>
<div class="m2"><p>نیابی ازین دژ رهائی ز دام</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>جهانجو چو بشنید در شد به غم</p></div>
<div class="m2"><p>شد از جور او دیده‌اش پر ز نم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بدانست کز وی نیامد رها</p></div>
<div class="m2"><p>که او بندد از جادوئی اژدها</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چنین پاسخ آراستش جنگجو</p></div>
<div class="m2"><p>که پنهان مشو هیچ و بنمای روی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>درین گفتگو پهلو کامیاب</p></div>
<div class="m2"><p>که شخصی درآمد سلح پوش ز آب</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نقابی به رو اندر آورده تنگ</p></div>
<div class="m2"><p>گرفته سنانی ز آهن به چنگ</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نشسته چو کوهی به پشت غراب</p></div>
<div class="m2"><p>پی رزم و کین داشت گویا شتاب</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چنین گفت کای سام رزم‌آزما</p></div>
<div class="m2"><p>از آن برنشستم به این بادپا</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>که گر گشن جوئی و گر بدخوئی</p></div>
<div class="m2"><p>بجویم به تو رزم پی جادوئی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>جهانجو به دشنام لب برگشاد</p></div>
<div class="m2"><p>بگفتا که چون تو پری رو مباد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چه خواهی زمن در جهان بازگوی</p></div>
<div class="m2"><p>که هر دم بیاری مرا بد به روی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>پری‌رو بدو خواهش آورد باز</p></div>
<div class="m2"><p>که لختی درین باغ با من بساز</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>وز آن پس بر آن باره که سرین</p></div>
<div class="m2"><p>ازین باغ خرم برو سوی چین</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بدو سام یل گفت کز جادوئی</p></div>
<div class="m2"><p>همانا نیابی ز من نیکوئی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ز افسون‌گری و ز آئین شیر</p></div>
<div class="m2"><p>اگر رخ بتابی شوی دل پذیر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>به ناکام‌یابی ز من کام خویش</p></div>
<div class="m2"><p>اگر یابمت زین نشان رام خویش</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>پریزاد از گفت او شد دژم</p></div>
<div class="m2"><p>ز انده به خشم اندر آورد نم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بدو گفت کای مرد بی‌هوش و رای</p></div>
<div class="m2"><p>مرا گوئی از شیر سر برگرای</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ز افسونگری هم میاور به یاد</p></div>
<div class="m2"><p>مرا تا شوی از پی وصل شاد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چو از تو نباشد مرا خرمی</p></div>
<div class="m2"><p>به نرمی چرا لب گشایم همی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>همان به که گیرم ره جور و کین</p></div>
<div class="m2"><p>ز افسون به تو تنگ سازم زمین</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بگفت این و مهمیز زد بر غراب</p></div>
<div class="m2"><p>چنان چون درآمد نهان شه در آب</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چو او شد نهان پهلو نامدار</p></div>
<div class="m2"><p>بیامند که بیرون رود از حصار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>فراوان بگردید و راهی ندید</p></div>
<div class="m2"><p>رخش گشت ماننده شنبلید</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>دگر ره چو صرصر درآمد به باغ</p></div>
<div class="m2"><p>سری پر زکینه دلی پر ز داغ</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>همانگه بر رود آمد فراز</p></div>
<div class="m2"><p>ز بس تشنگی آمد آبش نیاز</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>یکی دست ناگه درآمد ز آب</p></div>
<div class="m2"><p>بدو ناخنان همچون چنگ عقاب</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>کمرگاه سام دلاور گرفت</p></div>
<div class="m2"><p>جهان پهلوان شد ازو در شگفت</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ز نیرو کشیدش به آب اندرون</p></div>
<div class="m2"><p>ازو اسپری کرد صبر و سکون</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بپوشید مژگان گو رزمساز</p></div>
<div class="m2"><p>زمانی چو شد دیده را کرد باز</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>نه آن باغ دید و نه آن رود آب</p></div>
<div class="m2"><p>نه آرامگاه و نه ماوای خواب</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>شه قلعه ریگش آرامگاه</p></div>
<div class="m2"><p>جز از تابش خور نبودش پناه</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ز هر سوش تا چشم می‌کرد کار</p></div>
<div class="m2"><p>همی آتش تیز می‌زد شرار</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>تو گفتی مگر وی به دوزخ در است</p></div>
<div class="m2"><p>که زیر آتش است و به سر بر خور است</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چنان تشنه شد پهلو نیکنام</p></div>
<div class="m2"><p>که شد روز روشن برو تیره شام</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بنالید بر داور بی‌نیاز</p></div>
<div class="m2"><p>که ای بر همه بندگان کارساز</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>توئی آفریننده مور و مار</p></div>
<div class="m2"><p>نداریم غیر از تو پروردگار</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بگفت و به خاک سیه رو نهاد</p></div>
<div class="m2"><p>ز دادار دارنده می‌کرد یاد</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>ز گرمی چنان از زمین برفروخت</p></div>
<div class="m2"><p>که رخساره و دست و پایش بسوخت</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>ز بیچارگی اندر آمد ز پای</p></div>
<div class="m2"><p>نه هش دید با خود نه مردی و رای</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>چو از پا درآمد صدائی شنید</p></div>
<div class="m2"><p>بدان سان بلرزید بر خود چو بید</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>که ای سام اگر خواهی از هر بلا</p></div>
<div class="m2"><p>به نیک‌اختری باز یابی رها</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>همان به که تابی رخ از دین خویش</p></div>
<div class="m2"><p>نیاری دگر یاد از آئین خویش</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>پرستش کنی شیر را همچو من</p></div>
<div class="m2"><p>که گردی از آن سرور انجمن</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>دگر از پری‌دخت بردار دل</p></div>
<div class="m2"><p>به نیکی سوی مهر من دار دل</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>چو این گفته‌ها سر به سر بشنوی</p></div>
<div class="m2"><p>همانا بر خرمی بدروی</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>تو را شاه ایران و توران کنم</p></div>
<div class="m2"><p>شهان را همه کاخ ویران کنم</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>به دشنام بگشاد لب سام و گفت</p></div>
<div class="m2"><p>که با تو همه تیرگی باد جفت</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>مبادا مرا هرگز این هوش و را</p></div>
<div class="m2"><p>که رخ بازتابم به دین خدا</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>ددی را پرستنده گردم به دهر</p></div>
<div class="m2"><p>که او نوش را می‌نداند ز زهر</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>دگر با پریوش درین چند روز</p></div>
<div class="m2"><p>قسم خورده‌ام ای دد کینه‌توز</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>که جز وی نباشد مرا یار کس</p></div>
<div class="m2"><p>همان خود نیابد به کس دسترس</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>کنون با وی از مهر همخانه‌ام</p></div>
<div class="m2"><p>ز کیش تو و از تو بیگانه‌ام</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>چنین پاسخش داد افسون نما</p></div>
<div class="m2"><p>که ایدون رخ آور به دین خدا</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>برندت تو را گر سوی آسمان</p></div>
<div class="m2"><p>نیابی ازین جادوئی‌ها امان</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>ز گفتش دل سام آمد به درد</p></div>
<div class="m2"><p>رخ خود به دادار دارنده کرد</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>همی خواست آزادی از هر بلا</p></div>
<div class="m2"><p>که تا بازیابد ز جادو رها</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>به ناگه یکی آتش تیز و تاب</p></div>
<div class="m2"><p>درآمد ز هر سو چو تیر شهاب</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>همه دور آن خانه آتش گرفت</p></div>
<div class="m2"><p>همه ریگ تفتیده تابش گرفت</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>چو شد بر جهانجو جهان همچو دود</p></div>
<div class="m2"><p>یکی دستش از ناگهان در ربود</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>ببردش به روی هوا در زمان</p></div>
<div class="m2"><p>چنان برخروشید و برزد فغان</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>که ای سام گفتار من گوش کن</p></div>
<div class="m2"><p>می از ساغر بخردی نوش کن</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>اگر نه به آتش دراندازمت</p></div>
<div class="m2"><p>به گیتی ازین پس نهان سازمت</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>نپذرفت گفتار او پیل مست</p></div>
<div class="m2"><p>فسون‌ساز از وی رها کرد دست</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>به دریای بی بن رها ساختش</p></div>
<div class="m2"><p>به یکباره از هش جدا ساختش</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>به آب اندرون شد جهان پهلوان</p></div>
<div class="m2"><p>نهنگ دمنده شد از وی رمان</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>چو از پشت آمد به افراز آب</p></div>
<div class="m2"><p>فسونگر گرفتش هم اندر شتاب</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>ز بحرش به سوی هوا برد باز</p></div>
<div class="m2"><p>دگر چنگ افسونگری کرد ساز</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>نمی‌دید جز دست را هیچ سام</p></div>
<div class="m2"><p>همی خواست خنجر کشد از نیام</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>نبد خنجرش نیز اندر میان</p></div>
<div class="m2"><p>شگفتی فرو ماند ازو پهلوان</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>بنالید و برزد یکی تیزدم</p></div>
<div class="m2"><p>ز چشمش درآمد ز اندوه نم</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>همانگه پریزاد افسون پژوه</p></div>
<div class="m2"><p>نشستنگهی جست بالای کوه</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>به هر سو نظر کرد سام دلیر</p></div>
<div class="m2"><p>همه گل بد و سبزه و آبگیر</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>چو بر چشمه اندر زمان ره سپرد</p></div>
<div class="m2"><p>خدا را ثنا گفت و آبی بخورد</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>یکی نیروئی یافت با خویشتن</p></div>
<div class="m2"><p>چمان شد چو شاخ گل اندر چمن</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>بیامد بر آبگیری فراز</p></div>
<div class="m2"><p>نشست اندر آنجا زمانی دراز</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>ز بس غم به خواب اندر آمد سرش</p></div>
<div class="m2"><p>برآسود آرام جان در برش</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>چو بیدار شد نامبردار سام</p></div>
<div class="m2"><p>به پیش اندرش بود خان طعام</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>بیازید دست و چو شیران بخورد</p></div>
<div class="m2"><p>همی شکر دادار دارنده کرد</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>به ناگه یکی پیکری شد پدید</p></div>
<div class="m2"><p>جهانجو سراپای او بنگرید</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>ندانستاو را چگونه است روی</p></div>
<div class="m2"><p>شگفتی فروماند لختی بدوی</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>همان گاه آن پیکر آواز داد</p></div>
<div class="m2"><p>چنین گفت و چنگ سخن ساز داد</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>که ای سام از گفت من سر متاب</p></div>
<div class="m2"><p>بدان تا ز شاهان شوی کامیاب</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>تو را هر چه گویم پذیرنده شو</p></div>
<div class="m2"><p>مرا کام ده شیر را بنده شو</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>ربودش دگر باره افسون نما</p></div>
<div class="m2"><p>از آن پس به کوی دگر کرد جا</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>ز پولاد سیلی بر افراز کوه</p></div>
<div class="m2"><p>بدید آن جهان‌پهلوان شکوه</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>میانش ز افسونگری بد تهی</p></div>
<div class="m2"><p>وز آن نامور را نبد آگهی</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>درو بود سوراخها چون قفس</p></div>
<div class="m2"><p>ندیده شگفتی بدین گونه کس</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>همانگه پریزاد افسون نما</p></div>
<div class="m2"><p>جهان پهلوان را درآن کرد جا</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>بدو گفت کاین جای زندان تست</p></div>
<div class="m2"><p>همان گوهر بد نگهبان تست</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>رهائی نیابی ازین که دگر</p></div>
<div class="m2"><p>مگر آنکه تابی تو از مهر سر</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>بگفت این و پنهان شد اندر زمان</p></div>
<div class="m2"><p>جهان پهلو شد ز کارش نوان</p></div></div>