---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>ای داده روی خوب تو خورشید را نظام</p></div>
<div class="m2"><p>ای‌گشته عالمی به سر زلف تو غلام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر ماه لاله داری و بر لاله سلسله</p></div>
<div class="m2"><p>هرگز که دید سلسله بر مه ز عود خام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در زیر سایهٔ سر زلفین عارضت</p></div>
<div class="m2"><p>کالْبَدْر فی‌الرّیٰاحین والشّمس فی‌الغَمام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای روی تو چو لاله و قد تو همچو سرو</p></div>
<div class="m2"><p>وی خال تو چو دانه و زلف تو همچو دام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی از رهی نتابی و در بنده ننگری</p></div>
<div class="m2"><p>ای بی‌وفای کم‌خرد آخر کم از سلام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خونم حرام دانی و بوسه حرام چیست</p></div>
<div class="m2"><p>می ننگری که بوسه حلال است و خون حرام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر باد صبحدم به تو آرد پیام من</p></div>
<div class="m2"><p>زنهار تا نگیری آزار از آن پیام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرگز بود که باز خرامی به سوی من</p></div>
<div class="m2"><p>بر کف‌ گرفته ساغر و بر لب نهاده جام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو باده نوش کرده و من گفته مر تورا</p></div>
<div class="m2"><p>یا ایّها الغَزال تَنشا لَکَ المدام</p></div></div>