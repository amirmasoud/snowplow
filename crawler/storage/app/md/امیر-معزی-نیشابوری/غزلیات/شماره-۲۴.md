---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>آن شب که مرا بودی وصل تو به‌ کف بر</p></div>
<div class="m2"><p>با دوست نشستم به سرکوی لَطَف بر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ابروش کمان بود و هدف ساختم از دل</p></div>
<div class="m2"><p>تا غمزهٔ او تیر همی زد به هدف بر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پر دُر صدفی داشت عقیقین و همان شب</p></div>
<div class="m2"><p>غواص صدف یافته بودم به صدف بر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتی خط مشکینش بر عارض سیمین</p></div>
<div class="m2"><p>طغرای جمال است به منشور شرف بر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خلد به نظارهٔ طغرای جمالش</p></div>
<div class="m2"><p>گرد آمده حوران بهشتی به غُرَف بر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتی‌که مگر هست زپیراهن‌کُحلی</p></div>
<div class="m2"><p>پیدا شده دستی‌که زند نقره به دف بر</p></div></div>