---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>کی نهم روی دگرباره بر آن روی چو ماه</p></div>
<div class="m2"><p>کی زنم دست دگرباره در آن زلف سیاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بروم روی بر آن روی نهم کامد وقت</p></div>
<div class="m2"><p>بشوم دست بدان زلف زنم کامد گاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای پسر چند کنم بی‌لب خندان تو صبر</p></div>
<div class="m2"><p>وی صنم چندکشم در غم هجران تو آه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند دارم ز پی وعده تو گوش به در</p></div>
<div class="m2"><p>چند دارم زپی رقعهٔ تو چشم به‌راه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هست پیوسته تورا خواب در آن چشم دژم</p></div>
<div class="m2"><p>هست همواره تورا تاب در آن زلف دو تاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواب در چشم به‌من درنگری روزبروز</p></div>
<div class="m2"><p>تاب در زلف به‌من درگذری ماه به‌ماه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اشک من لؤلؤ و یاقوت شود چون تو به من</p></div>
<div class="m2"><p>با کلاه و کمر از دورکنی ژرف نگاه</p></div></div>