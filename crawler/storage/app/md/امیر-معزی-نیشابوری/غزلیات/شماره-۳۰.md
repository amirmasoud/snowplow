---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>بس‌که من دل را به‌دام عشق خوبان بسته‌ام</p></div>
<div class="m2"><p>از نشاط روی ایشان توبه‌ها بشکسته‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جسته‌ام او را که او را دیده تیر انداخته است</p></div>
<div class="m2"><p>تا دل و جان را به تیر غمزهٔ او خسته‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکجا سوزنده‌ای را دیده‌ام چون خویشتن</p></div>
<div class="m2"><p>دوستی را دامن اندر دامن او بسته‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوستانم بر سرکارند در بازار عشق</p></div>
<div class="m2"><p>من چو معزولان چرا درگوشهٔ بنشسته‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر به ظاهر بنگری درکار من‌گویی مگر</p></div>
<div class="m2"><p>با سلامت همنشین و از خصومت رسته‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این سلامت راکه من دارم ملامت در قفاست</p></div>
<div class="m2"><p>تا نپنداری که از دام ملامت جسته‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوک خار هجر این یاران مشکین موی را</p></div>
<div class="m2"><p>از جفای دوستان در دیدگان بشکسته‌ام</p></div></div>