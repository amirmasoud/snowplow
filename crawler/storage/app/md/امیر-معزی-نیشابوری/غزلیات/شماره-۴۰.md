---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>تا دل بود ای دلبر تا جان بود ای جانان</p></div>
<div class="m2"><p>با مهر تو دارم دل با عشق تو دارم جان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر دل ببری شاید زیرا که تویی دلبر</p></div>
<div class="m2"><p>ور جان ببری زیبد زیرا که تویی جانان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هوش از همه بستانی چون غمزه‌ کنی ناوک</p></div>
<div class="m2"><p>گوی از همه بربایی چون زلف‌ کنی چوگان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرچندکه سلطانم آخر به تو محتاجم</p></div>
<div class="m2"><p>چون عشق پدید آید محتاج شود سلطان</p></div></div>