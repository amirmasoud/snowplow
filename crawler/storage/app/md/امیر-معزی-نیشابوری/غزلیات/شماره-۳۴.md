---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>دلم را یاری از یاری ندیدم</p></div>
<div class="m2"><p>غمم را هیچ غمخواری ندیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به قاف عشق بر سیمرغ شادی</p></div>
<div class="m2"><p>اگر دیدی تو من باری ندیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امید راحتی اندر که بندم</p></div>
<div class="m2"><p>کزو در حال آزاری ندیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم را با دهانت کاری افتاد</p></div>
<div class="m2"><p>کز آن در بسته‌تر کاری ندیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هر بادی شود زلف تو از جای</p></div>
<div class="m2"><p>به سان او سبکباری ندیدم</p></div></div>