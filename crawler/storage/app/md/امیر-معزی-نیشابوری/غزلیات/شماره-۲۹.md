---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>خبرت هست که در آرزوی روی توام</p></div>
<div class="m2"><p>وز غم و فرقت تو تافته چون موی توام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خسته هجر تو و سوخته عشق توام</p></div>
<div class="m2"><p>عاشق موی تو و شیفتهٔ روی توام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوی تو باد سحرگه به من آرد صنما</p></div>
<div class="m2"><p>بندهٔ باد سحرگه ز پی بوی توام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به سر تو که برم عهد وفای تو به سر</p></div>
<div class="m2"><p>تا بدانی که هواخواه و هواجوی توام</p></div></div>