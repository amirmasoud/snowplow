---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>عمری گذاشتم صنما در وفای تو</p></div>
<div class="m2"><p>وز صد هزارگونه‌ کشیدم جفای تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن چیست از جفا که نکردی به جای من</p></div>
<div class="m2"><p>وان چیست از وفا که نکردم به جای تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مسکین دلم‌گر از تو کشیدست صد جفا</p></div>
<div class="m2"><p>یک دم زدن سُتُه نشدست از وفای تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویند مردمان که بود ذره در هوا</p></div>
<div class="m2"><p>من لاجرم چو ذره شدم در هوای تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در عشق تو بنالم از چشم خویشتن</p></div>
<div class="m2"><p>کاین چشم من فکند مرا در بلای تو</p></div></div>