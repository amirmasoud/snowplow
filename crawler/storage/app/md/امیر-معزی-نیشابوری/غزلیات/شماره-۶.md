---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>حلقه‌های زلف جانان تا سراندر سرزده است</p></div>
<div class="m2"><p>دل ز من بگریخته است و زیر زلف او شده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر شب تاریک خواب آرد همی در چشم من</p></div>
<div class="m2"><p>زلف شبرنگش چرا خواب از دو چشمم بستداست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر ز اصل جادویی و شعبده خواهی نشان</p></div>
<div class="m2"><p>چشم او بنگر که اصل جادویی و شعبده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تاکه او را دو رده است از در مکنون و عقیق</p></div>
<div class="m2"><p>از سرشک و لعل او بر چهرهٔ من صد رده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بود آتشکده آرامگاه موبدان</p></div>
<div class="m2"><p>عشق او چون موبدست و جان من آتشکده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پارسا چون باشم از عشق وی و توبه ‌کنم</p></div>
<div class="m2"><p>کان بت عیار تیر غمزه بر جانم زده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با چنان غمزه‌ که او دارد مرا و جز مرا</p></div>
<div class="m2"><p>پارسایی باطل است و توبه ‌کردن بیهده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دارد آن خورشید لشکر صورت فردوسیان</p></div>
<div class="m2"><p>گویی از فردوس پیش تخت سلطان آمده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسرو گیتی ملکشاه آن‌ که اندر شرق و غرب</p></div>
<div class="m2"><p>نه بود هرگز چنو سلطان و نه هرگز بُده است</p></div></div>