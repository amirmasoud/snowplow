---
title: >-
    شمارهٔ ۵۸
---
# شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>آن صنم‌ کاندر دو لب تنگ شکر دارد همی</p></div>
<div class="m2"><p>بر سر سرو روان شمس و قمر دارد همی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حلقه‌های زلف او عمدا کند زیر و زبر</p></div>
<div class="m2"><p>تا دل و جان‌مرا زیرو زیردارد همی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تلخ‌ گفتار است و شیرین لب نگارین روی من</p></div>
<div class="m2"><p>وین عجب بنگر که زهر اندر شکر دارد همی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آیت و اللیل بر خواند همی شمس الضحاش</p></div>
<div class="m2"><p>تا نقاب از آیت وَالْفَجر بردارد همی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آتش عشقش ببرده است آب رویم تا مرا</p></div>
<div class="m2"><p>لب از آتش خشک‌ و چشم از آب تر دارد همی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نخواهد تا غنی‌ گردد ز سیم و زر چرا</p></div>
<div class="m2"><p>اشک من چون سیم ‌و رخسارم چو زر دارد همی</p></div></div>