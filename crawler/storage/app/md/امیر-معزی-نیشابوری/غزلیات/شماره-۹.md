---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>گر تو پنداری که رازم بی‌تو پیدا نیست هست</p></div>
<div class="m2"><p>یا دلم مشتاق آن رخسار زیبا نیست هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا ز عشق لولو و یاقوت شَکَّر بار تو</p></div>
<div class="m2"><p>چشم ‌گوهر بار من هر شب چو دریا نیست هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور تو را صورت همی بندد که از چشم و دلم</p></div>
<div class="m2"><p>آب و آتش تا ثَری و تا ثُریّا نیست هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر تو پنداری که بی‌وصل تو جان اندر تنم</p></div>
<div class="m2"><p>مستمند و دردمند و ناشکیبا نیست هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ور تو پنداری که از جور و جفای روزگار</p></div>
<div class="m2"><p>در دِماغ و طبع من سودا و صفرا نیست هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر گمان تو چنان است ای صنم ‌کز عشق تو</p></div>
<div class="m2"><p>این بلاها بر من بیچاره تنها نیست هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این همه زشتی مکن کامروز را فردا بود</p></div>
<div class="m2"><p>ور تو گویی از پس امروز فردا نیست هست</p></div></div>