---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>امروز بتم تیغ جفا آخته دارد</p></div>
<div class="m2"><p>خون دلم از دیده برون تاخته دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او را دلم آرامگه است و عجب این است</p></div>
<div class="m2"><p>کارامگه خویش برانداخته دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد مشعله از عشق برافروخته دارم</p></div>
<div class="m2"><p>تا صد علم از حسن برافراخته دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جانم ببرد گر ز پی نرد بتازد</p></div>
<div class="m2"><p>زیرا که از آغاز تو را باخته دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صد سلسله دارد ز ‌شبه ساخته برسیم</p></div>
<div class="m2"><p>وان سلسله گویی که مرا ساخته دارد</p></div></div>