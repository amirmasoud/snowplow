---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>آن زلف نگر بر آن بر و دوش</p></div>
<div class="m2"><p>وان خط سیه بر آن بناگوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دو شده پیش ماه و خورشید</p></div>
<div class="m2"><p>مانندهٔ حاجبان سیه‌پوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی‌گرمی و بی‌فروغ آتش</p></div>
<div class="m2"><p>چون عنبر و مشک‌دوش بر دوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن داده به عاشقان غم و درد</p></div>
<div class="m2"><p>وین برده زعاقلان دل و هوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سنبل خط و لاله رخ نگاری است</p></div>
<div class="m2"><p>آن ماه سمنبر گل آغوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سنبل اوست نوش من زهر</p></div>
<div class="m2"><p>وز لالهٔ اوست زهر من نوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گویند که یادکن مر او را</p></div>
<div class="m2"><p>واندر غم او مباش خاموش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گویم‌که به حیله چون‌کنم یاد</p></div>
<div class="m2"><p>آن را که نکرده‌ام فراموش</p></div></div>