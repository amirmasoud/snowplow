---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>شب نماید در صفت زلفین آن بت روی را</p></div>
<div class="m2"><p>مه نماید در صفت رخسار آن دلجوی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب ‌کجا جوشن بود کافور دیبا رنگ را</p></div>
<div class="m2"><p>مه‌ کجا مَفرَش بود زنجیر عنبر بوی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر زمین هر کس خبر دارد که ماه و آفتاب</p></div>
<div class="m2"><p>سجده بردند از فلک دیدار آن بت روی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر گذشت آن ماه پیکر گرد باغ و بوستان</p></div>
<div class="m2"><p>گرد رو اندر به عَمد‌ا تاب داده موی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موی و روی او به‌ باغ و بوستان تشویر داد</p></div>
<div class="m2"><p>سنبل و شمشاد را و لالهٔ خود روی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زلف و خالش را شناسد هر کسی چوگان و گوی</p></div>
<div class="m2"><p>درخور آمد گوی‌ چوگان را و چوگان‌‌ گوی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر کجا باشد رخ و خطش نباشد بس‌ عجب</p></div>
<div class="m2"><p>گر ندارد شوی زن را طاعت و زن شوی را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چونکه اندر خانهٔ وصل آمد از کوی فراق</p></div>
<div class="m2"><p>در گشاد این خانه را و در ببست آن‌ کوی را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>او و من هر دو به مهر و دوستی یکتا دلیم</p></div>
<div class="m2"><p>نیست راه اندر میانه حاسد و بدگوی را</p></div></div>