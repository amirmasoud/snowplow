---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>امروز بت من سر پیکار ندارد</p></div>
<div class="m2"><p>جز دوستی و عذر و لَطَف‌ کار ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بشکفت رخم چون‌ گل بی‌خار ز شادی</p></div>
<div class="m2"><p>زیرا که ‌گل صحبت او خار ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با گریه شد این چرخ ‌گهربار که آن بت</p></div>
<div class="m2"><p>بی‌خنده همی لعل شکربار ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلفش همه مشک است و چنان مشک دلاویز</p></div>
<div class="m2"><p>کم جوی ز عطار که عطار ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بِربود دلم زلفش و بیم است‌ که آن زلف</p></div>
<div class="m2"><p>زنهار خورد با من و زنهار ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در شهر دلی نیست وگر هست‌ کدام است</p></div>
<div class="m2"><p>کاو در شکن زلف گرفتار ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ماهی است‌ که مشک تبت و لالهٔ خود روی</p></div>
<div class="m2"><p>با زلف و رخش قیمت و مقدار ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون غمز‌ه کند نرگس او هیج مُشَعبد</p></div>
<div class="m2"><p>با نرگس او رونق بازار ندارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من بنده ی آن ماه‌ که در جان و دل خویش</p></div>
<div class="m2"><p>جز بندگی شاه جهاندار ندارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سلطان جهانگیر ملکشاه جوان‌بخت</p></div>
<div class="m2"><p>شاهی که به شاهی و هنر یار ندارد</p></div></div>