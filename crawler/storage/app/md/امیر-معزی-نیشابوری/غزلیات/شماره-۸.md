---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>خطّی است‌ که بر عارض آن ماه تنیدست</p></div>
<div class="m2"><p>یا دست فلک غالیه بر ماه‌ کشیدست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا رهگذر مورچگان است به ‌گلبرک</p></div>
<div class="m2"><p>یا بر سمن تازه بنفشه بدمیدست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در جمله یکی خط بدیع است‌که آن خط</p></div>
<div class="m2"><p>صد توبه شکسته است و دو صد پرده دریدست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من عاشق آن تُرک پریزاد که او را</p></div>
<div class="m2"><p>هم جعد پریشیده و هم زلف خمیدست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صورتگر چین از حسد صورت خویش</p></div>
<div class="m2"><p>هم خامه شکسته است و هم انگشت‌ گزیدست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من از همه املاک دلی دارم و جانی</p></div>
<div class="m2"><p>و اندر دل و جانم گل شادی شکفیدست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل دوستی یار دلارام‌ گرفته است</p></div>
<div class="m2"><p>جان بندگی شاه جهاندار گزیدست</p></div></div>