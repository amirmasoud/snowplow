---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>بر ماه لاله ‌داری و بر لاله مشک ناب</p></div>
<div class="m2"><p>در مشک حلقه‌داری و در حلقه بند و تاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میگون لب است و مغزم از آن می پر از خُمار</p></div>
<div class="m2"><p>گلگون رخ است و چشمم از آن‌ گل پر از گلاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خصم من است زلفش وگر نیست پس چرا</p></div>
<div class="m2"><p>دارد حلال خونم و دارد حرام خواب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آب روی اوست همه آتش دلم</p></div>
<div class="m2"><p>کس دیده آتشی‌که بود قوتش به آب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>او ساکن دل است و خراب است مسکنش</p></div>
<div class="m2"><p>آسوده ساکنی است‌ که شد مسکنش خراب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جستم ز راه عشق و ببستم ره خطا</p></div>
<div class="m2"><p>جستم مدیح میر وگشادم در صواب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میر بزرگوار عبیدالله آن که هست</p></div>
<div class="m2"><p>در ملک شه موید و در دین حق شهاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>و‌حی است آفرینش و آن وحی را مدام</p></div>
<div class="m2"><p>هفت اخترند کاتب و هفت آسمان کتاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اندر حریم دولت او جای ساخته است</p></div>
<div class="m2"><p>در چشم خویش بچهٔ ‌گنجشک را عقاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر بر غراب همت او سایه‌گسترد</p></div>
<div class="m2"><p>طاوس‌وار جلوه دهد خویش را غُراب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ایمان و کفر گشت‌ گل مهر و کین او</p></div>
<div class="m2"><p>کان اصل راحت آمد و این مایهٔ عذاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زو مهر او طلب‌که به‌دنیا و آخرت</p></div>
<div class="m2"><p>دولت دهد مرادت و ایزد دهد ثواب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>انگشت او زبان شد و جود اندرو سخن</p></div>
<div class="m2"><p>زر هست از او سوال و همه خلق را جواب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جامی ز سیم پاک فرستاد نزد من</p></div>
<div class="m2"><p>پر عود خام و عنبر سارا و مشک ناب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یک شکل او مدور و یک شکل او دراز</p></div>
<div class="m2"><p>چون پاره پاره ابر سیه پیش آفتاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفتی که هست عارض سیمین دلبرم</p></div>
<div class="m2"><p>در زیر زلف بسته از او بر قصب نقاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پیروزه روی موم بیاراسته به مهر</p></div>
<div class="m2"><p>مهری شریف تر ز دعاهای مستجاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کردم بدین کرامت بر جود او ثنا</p></div>
<div class="m2"><p>خوردم بدین لطافت بر یاد او شراب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>روشن شد از مدایح او طبع من چنانک</p></div>
<div class="m2"><p>چرخ از ستاره و صدف از لؤلؤ خوشاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>توفیق خواستم ز خداوند تا کنم</p></div>
<div class="m2"><p>در ‌شیب‌ شکر نعمت و احسا‌نش چون شباب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا قبله سازم آنجا کاورا بود عنان</p></div>
<div class="m2"><p>تا کعبه سازم آنجا کاو را بود رکاب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در حلم و جود باد درنگ و شتاب او</p></div>
<div class="m2"><p>تا خاک را درنگ بود باد را شتاب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از همتش به اهل معالی رسیده باد</p></div>
<div class="m2"><p>اِنعام بی‌نهایت و اِحسان بی‌حساب</p></div></div>