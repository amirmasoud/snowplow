---
title: >-
    شمارهٔ ۱۰۸
---
# شمارهٔ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>مَلِک سنجر جهانداری به میراث از پدر دارد</p></div>
<div class="m2"><p>پدر شادست در فردوس تا چون او پسر دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز فرّ و رسم و آیینش بیاراید همی‌گیتی</p></div>
<div class="m2"><p>که فَرّ عمّ و رسم جدّ و آیین پدر دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدو نازد همی دولت که با دولت خرد دارد</p></div>
<div class="m2"><p>بدو زیبد همی شاهی‌ که با شاهی هنر دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زآفریدون و ذوالقرنین و اسکندر فزون است او</p></div>
<div class="m2"><p>که ملک و نعمت و لشکر ز هر سه بیشتر دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خداوند بزر‌گ است او که اسباب بزرگی را</p></div>
<div class="m2"><p>هم از عقل و هنر دارد هم از اصل و گهر دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ضمیر روشن او هر چه خواهد بود بشناسد</p></div>
<div class="m2"><p>ز سر غیب پنداری ضمیر او خبر دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ملک در زیر پر دارد خجسته تاج و تختش را</p></div>
<div class="m2"><p>که بال عدل و انصافش جهان در زیر پر دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به باغ دولت اندر، بر لب جوی شهنشاهی</p></div>
<div class="m2"><p>یکی سروست شخص او که از اقبال بر دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر آنچ از سیم و زر شاهان پیشین را به‌ دست آمد</p></div>
<div class="m2"><p>به‌خاک اندر نهادستند تا او جمله بردارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قضای ایزدی کردست گیتی را به حکم او</p></div>
<div class="m2"><p>به سان خانه‌ای کز طاعت و عصیان دو در دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همیشه اهل طاعت را دری سوی جنان دارد</p></div>
<div class="m2"><p>همیشه اهل عصیان را دری سوی سَقَر دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کند عزم سفر جان از تن خصمان و بدخواهان</p></div>
<div class="m2"><p>چو شد نامه به هفت اقلیم کاو عزم سفر دارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هنوز از فتح اقلیمی نیاسودست شمشیرش</p></div>
<div class="m2"><p>به پیروزی نشاط فتح اقلیمی دگر دارد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گهی چون ماه قصد از باختر دارد سوی خاور</p></div>
<div class="m2"><p>گهی چون خور ز خاور قصد سوی باختر دارد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز ماه و خور همه ساله‌ گر احکام است در عالم</p></div>
<div class="m2"><p>پس این احکام ازو باید که سیر ماه و خور دارد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز تیغ و کلک او خیزد بد و نیک جهان‌ گویی</p></div>
<div class="m2"><p>به تیغ اندر قضا دارد به‌کلک اندر قدر دارد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زمین رزم او گویی هزاران جوی خون دارد</p></div>
<div class="m2"><p>زمین بزم او گویی هزاران‌ کان زر دارد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو ساغرگیرد اندر چشم دیدار طرب دارد</p></div>
<div class="m2"><p>چو خنجرگیرد اندرگوش لبیک ظفر دارد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر زیر قبا اندر جهانی باکمال است او</p></div>
<div class="m2"><p>چرا زیر نگین اندر جهانی مختصر دارد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مه‌ گردون ز بهر آن که تا باشد سلاح او</p></div>
<div class="m2"><p>گهی شکل‌کمان دارد گهی شکل سپر دارد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چرا دشمن به رزم اندر فروزد آتش‌ کینش</p></div>
<div class="m2"><p>کز آن آتش دل و دیده پر از دود و شرر دارد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مگر مغز و جگر جوید ز شخص دشمنان تیغش</p></div>
<div class="m2"><p>که جای خویش‌ گه در مغز و گاهی در جگر دارد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بسا کس کز نهیب او میانی چون‌ کمان دارد</p></div>
<div class="m2"><p>به خدمت پیش تخت او کمانی برکمر دارد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ایا فرخ قدم شاهی‌ که دولت برنگیرد سر</p></div>
<div class="m2"><p>ز خط حکم آن سرورکه بر حکم تو سر دارد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زلطف طبع تو جسم هنرمندی روان دارد</p></div>
<div class="m2"><p>ز نور و رای تو چشم خردمندی بصر دارد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مگر حج است دیدارت که هرکس‌کاو تورا بیند</p></div>
<div class="m2"><p>بساطت چون حَرَم دارد رکابت چون حَجَر دارد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>معزی از ثنا و شکر تو هرگز نیاساید</p></div>
<div class="m2"><p>ز بهر آن که چون الحمد مدح تو زبر دارد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دل‌ و جانش‌ گه خدمت مقیم است اندر آن حضرت</p></div>
<div class="m2"><p>اگرچه شخص او اکنون مقام اندر حضر دارد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همیشه تاکه دارد شمس حکم سال دهقانان</p></div>
<div class="m2"><p>جهان چون حکم سال تازیان سیر قمر دارد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به داد خویش خرم دار ملک دین باقی را</p></div>
<div class="m2"><p>چنان‌کاندر مه نیسان‌ گلستان از مطر دارد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به بزم‌اندر شرابی خور چنان یاقوت رُمّانی</p></div>
<div class="m2"><p>ز دست آن‌ که در یاقوت رُمّانی شَکَر دارد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تویی شایستهٔ شاهی تویی بایستهٔ شادی</p></div>
<div class="m2"><p>بمان و بگذران‌ گیتی اگرگیتی‌ گذر دارد</p></div></div>