---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>هزار شکر کنم دولت مؤیّد را</p></div>
<div class="m2"><p>که داد باز به من دلبر سَهی قد را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آتش دل مشتاق و از بلای فراق</p></div>
<div class="m2"><p>فرو گذاشته بودم وُثاق و مرقد را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو ماه روی من آمد کنون بحمدالله</p></div>
<div class="m2"><p>به نور وصل بَدَل ‌کرده نار مُوقَد را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بتی که چنبر خورشید کرد عنبر و ند</p></div>
<div class="m2"><p>که ‌کرد چنبر خورشید عنبر و نَدْ را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وگر به عُقْده رأس اندرون گرفت قمر</p></div>
<div class="m2"><p>مر ا‌َهْرمن به حمایت گرفت فَرْ‌قَد را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وگر به ناصیت روز شب مُعَقّد شد</p></div>
<div class="m2"><p>اسیر گشت دل من شب مُعَقّد را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وگر به ساحت‌ گلزار یافت مورچه راه</p></div>
<div class="m2"><p>گرفت مورچه دامن گل مُورّد را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وگر ز مشک سرشته نوشت دست جمال</p></div>
<div class="m2"><p>به‌گرد تختهٔ سیمین حروف ابجد را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وگر طراز مدیح من است بر دفتر</p></div>
<div class="m2"><p>محلّ مَجْد و عُلوّ مِهتَر مؤیّد را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کمال دولت عالی سَرِ فضایل و جود</p></div>
<div class="m2"><p>ابوالرضا فضل‌اللهِ محمد را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مُقَدَّمی که جهانی است فرد از حکمت</p></div>
<div class="m2"><p>مطیع‌ گشته جهان این جهان مفرد را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مدام خدمت او سنت مؤکد شد</p></div>
<div class="m2"><p>قضا فریفته شد سُنّت مؤکّد را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز بُورضاست جهان را همیشه نور و نوا</p></div>
<div class="m2"><p>چنانکه زینت و زیب از رضاست مَشْهد را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که را خبر بود از سد جاه و حشمت او</p></div>
<div class="m2"><p>هبا و هَزْل شِمارَد سِکندری سد را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ایا ستارهٔ احسان و آفتاب سَخا</p></div>
<div class="m2"><p>که آسمان تو بینم سریر و مسند را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو آن‌کسی که چو مهد ملوک مهدی‌وار</p></div>
<div class="m2"><p>همی نظام دهی عالم مُمَهّد را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به‌ دین پاک تو جایی بلند یافته‌ای</p></div>
<div class="m2"><p>به جِدّ و جهد أب و جَدّ نیافتی جد را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>حروف ابجد اگر تا ابد کنی تضعیف</p></div>
<div class="m2"><p>فزون از آن به تو فخرست مر أب و جَدّ را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو آن کسی که ز نام تو یافت استحقاق</p></div>
<div class="m2"><p>کمال دولت عالی بقای سرمد را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به روزگار تو گر نصرِ احمد آید باز</p></div>
<div class="m2"><p>خجل ‌کنند عبید تو نصرِ احمد را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو از عدم به وجود آمدی، عدم کردی</p></div>
<div class="m2"><p>به جود خویش وجود نهایت و حد را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به زیر پای تو خاک زمین شود عَسْجُد</p></div>
<div class="m2"><p>از آنکه دست تو چون خاک‌ کرد عَسجَد را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کجا بود شَبَه خامهٔ تو چون شب قدر</p></div>
<div class="m2"><p>چه قدر باشد مر لؤلؤ و زَبَرجَد را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو مَدّ کلک به روز و به شب مسلسل کرد</p></div>
<div class="m2"><p>زمانه لعبت دو دیده کرد آن مد را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به مد کلک تو بر شرق و غرب محتاج است</p></div>
<div class="m2"><p>هر آن‌ که هست سزاوار کوس و مِطرَد را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خدای خواست به اقبال و سروری مدام</p></div>
<div class="m2"><p>که بر تو وقف‌ کند سروری و سَؤدد را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مجرّدست دل تو ز رنج مخلوقات</p></div>
<div class="m2"><p>مجرّبست سخا آن دل مجرّد را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز خدمت تو شناسد سداد و سؤدَد خویش</p></div>
<div class="m2"><p>هر ان‌کسی‌که شناسد ز ابیض‌، اَ‌سوَد را</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هر آن یدی‌ که نویسد به مدح تو یک حرف</p></div>
<div class="m2"><p>دهان دهر ببوسد بنان آن ید را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همشه غاشیه ی بخت آن‌ کشد فرقد</p></div>
<div class="m2"><p>که از زیارت بخت تو برکشد قد را</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>من آن معزّی برهانیم که نشر کنم</p></div>
<div class="m2"><p>به فرّ دولت تو شعرهای بی‌رد را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به اهتمام تو سال دگر تمام کنم</p></div>
<div class="m2"><p>نگاشته به مدیح تو صد مُجَلّد را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز خدمت تو به حدّی رسم‌ که‌ گویی تو</p></div>
<div class="m2"><p>ایا غلام بیار آن امیر اوحَد را</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به حق آن‌که تو را تربیت خدای‌کند</p></div>
<div class="m2"><p>که تربیت‌ کنی این بندهٔ مؤیّد را</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همیشه تا که بود منبع بُخار بِحار</p></div>
<div class="m2"><p>چو مرکزست اثیر آتش مُصَعَّد را</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مباد سور و سرور از سرای تو زایل</p></div>
<div class="m2"><p>نشانه باد سریر تو بُرو مورد را</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مساعد تو سعادت‌، موافق تو فلک</p></div>
<div class="m2"><p>ضمیر و رای تو مطلوب فال اسعد را</p></div></div>