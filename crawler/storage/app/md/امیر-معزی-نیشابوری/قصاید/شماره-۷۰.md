---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>شاه جهان که خسرو فرخنده اخترست</p></div>
<div class="m2"><p>بر شرق و غرب‌ پادشه عدل‌ گسترست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عزمش فرو برندهٔ ملک مخالف است</p></div>
<div class="m2"><p>رأیش برآورندهٔ دین پیمبرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سَهمَش به خاورست و نهیبش به باختر</p></div>
<div class="m2"><p>وز باختر ولایت او تا به خاورست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با آفتاب رای منیرش مقابل است</p></div>
<div class="m2"><p>با نوبهار بزم شریفش برابرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کز نور رای او همه گیتی مزیّن است</p></div>
<div class="m2"><p>وز بوی بزم او همه عالم معطرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنجا که تیغ اوست شجاعت مُرکّب است</p></div>
<div class="m2"><p>وانجا که دست اوست سخاوت مصوّرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تیغش نه تیغ صاعقهٔ دشمن افکن است</p></div>
<div class="m2"><p>دستش نه دست معجزهٔ روح پرورست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از نعل مرکبان سپاهش به شرق و غرب</p></div>
<div class="m2"><p>چندانکه هست روی زمین ماه پیکرست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر بنگری ز خدمت تیغش به روم و شام</p></div>
<div class="m2"><p>رخها مُزَعفرست و زمینها مُعَصفَرست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آنجا که بود نعرهٔ ناقوس رومیان</p></div>
<div class="m2"><p>اکنون خروش و نالهٔ الله اکبرست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاها تو در فتوح فزون از سکندری</p></div>
<div class="m2"><p>وان تیغ جان ربای تو سدّ سکندرست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خطی که گرد مملکت اندر کشیده‌ای</p></div>
<div class="m2"><p>چون دایره است و نقطهٔ او هفت کشورست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از پیش همت تو چو خاک است لاجرم</p></div>
<div class="m2"><p>اندرکف رعیت تو خاک چون زرست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر روز بهترست خصال تو بی‌خلاف</p></div>
<div class="m2"><p>تا عالم است روز تو از روز بهترست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آورده‌اند سیصد و هفتاد در شمار</p></div>
<div class="m2"><p>آن خسروان که لشکری خسرو ایدرست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یک رایت تو سیصد و هفتاد رایت است</p></div>
<div class="m2"><p>یک لشکر تو سیصد و هفتاد لشکرست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دشمن نماند در همه عالم تو را کسی</p></div>
<div class="m2"><p>پس‌ گر کسی بماند مطیع و مُسَخََّرست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در خاک رفت هرکه همی با تو سرکشید</p></div>
<div class="m2"><p>او خاک بر سرست و تورا تاج برسرست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آویزد آنکه‌ گر بگریزد ز مهر تو</p></div>
<div class="m2"><p>گرچه رسن دراز سرش هم به چنبرست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وقت سَماع و باده و آرام و رامِش است</p></div>
<div class="m2"><p>نه وقت جوشن و زره و خَود و مِغفُرست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در هر وطن که پای برون آری از رکاب</p></div>
<div class="m2"><p>تو هدیه‌ای بدیع و یکی بزم دیگرست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر روز نوبت است یکی بزم ساختن</p></div>
<div class="m2"><p>و امروز نوبت ملک فرّخ اخترست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آن میر شیربجه و آن شاه شاه زاد</p></div>
<div class="m2"><p>کز اصل پاک خسرو سلجوق گوهرست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو همچو آفتابی و او هست همچو ماه</p></div>
<div class="m2"><p>وز هر دو دین و دولت و دنیا منوّرست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>این بزم جنت است و تو رضوان جنتی</p></div>
<div class="m2"><p>بخت بلند و جام تو طوبی و کوثرست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>می خور ز دست نوش لبی خَلُّخی نژاد</p></div>
<div class="m2"><p>کش مشتری برادر و خورشید مادرست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زان می‌ که چون به جام بلور اندر افکنند</p></div>
<div class="m2"><p>گویی در آبِ روشن‌ رخشنده آذرست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا کوس و لشکر و علم و تخت و مرکب است</p></div>
<div class="m2"><p>تا جام و خاتم و قلم و تیغ و افسرست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>این جمله را به حق ملک و پادشاه باش</p></div>
<div class="m2"><p>زیرا که حق همیشه سزاوار حقورست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>عدل تو باد یاور و دارندهٔ جهان</p></div>
<div class="m2"><p>کایزد تو را همیشه نگهبان و یاورست</p></div></div>