---
title: >-
    شمارهٔ ۱۰۷
---
# شمارهٔ ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>ترکی‌که دو لب شیرین چون شهد و شکر دارد</p></div>
<div class="m2"><p>دو دایرهٔ مشکین بر طرف قمر دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خط بر رخ اوگویی بر ماه زره دارد</p></div>
<div class="m2"><p>دل در بر اوگویی در سیم حَجَر دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیّ و دوگهر بینم در تنک دهان او</p></div>
<div class="m2"><p>بر روی‌گهرگویی دو تَنک شَکر دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوهر گهر ار دارد گیرد ز شعاع خور</p></div>
<div class="m2"><p>خور نور بدان روشن سی و دوگهر دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باریک تنم دایم چون موی و میان دارد</p></div>
<div class="m2"><p>خمیده قدم دایم چون زلف و کمر دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در جستن او هر کس اندر غم هجرانش</p></div>
<div class="m2"><p>دو پای به ره دارد دو دست به سر دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من نامهٔ نام او پیوسته ز بر دارم</p></div>
<div class="m2"><p>و او نام کسی دیگر پیوسته ز بر دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>امروز به شهر اندر هر مهتر و هر کهتر</p></div>
<div class="m2"><p>زین حال نشان دارد زین کار خبر دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای دلبر سیمین بر هستی توکمر زرین</p></div>
<div class="m2"><p>عاشق ز غم هجرت رخسار چو زر دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیداد کنی بر من دادم ندهی هرگز</p></div>
<div class="m2"><p>بیداد تو بر جانم هر روز حشر دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خصم تو منم جانا رفتم به‌سوی داور</p></div>
<div class="m2"><p>تا از دل و جان من بیداد تو بردارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یابد به در داور پیروزی و بهروزی</p></div>
<div class="m2"><p>هر خصم‌که او پشتی از میر عمر دارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فرزند قوام‌الدین داماد شه قزوین</p></div>
<div class="m2"><p>میری که سپاه خویش از فتح و ظفر دارد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قزوین به همه وقتی با نور و نوا بودست</p></div>
<div class="m2"><p>هر ذره به جاه اندر صد نور و صُوَر دارد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هست از خردش دولت هست از پدرش حشمت</p></div>
<div class="m2"><p>هم فرّ خرد دارد هم جاهِ پدر دارد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>با او به همه عالم دارات نیارد کس</p></div>
<div class="m2"><p>کاقبال قوام‌الدین در پیش سپر دارد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در حضرت او تازد جسمی که روان دارد</p></div>
<div class="m2"><p>وز طلعت او نازد چشمی‌ که بصر دارد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هرگز نبود گویی گردش به سپهر اندر</p></div>
<div class="m2"><p>بی‌آنکه به ایامش سَعدین نظر دارد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بُران و روان بینم تیغ و قلمش گویی</p></div>
<div class="m2"><p>در تیغ قضا دارد در کلک قدر دارد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فرض است رضا دائم و ایزد به رضا او را</p></div>
<div class="m2"><p>در روضه ملک دین پاینده شجر دارد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون عرش بود گلشن باشد شجرش تازه</p></div>
<div class="m2"><p>وان تازه شجر لابد اینگونه ثمر دارد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>صدری که شرف دارد زو بیت شرفشاهی</p></div>
<div class="m2"><p>خاطر ز مدیح او اقبال و خطر دارد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>او هست ملک صورت زاثار دل و سیرت</p></div>
<div class="m2"><p>هرگز ملکی دیدی کآثار بشر دارد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یک خلعت او ناید در فکرت هر زایر</p></div>
<div class="m2"><p>مقدار سخاگویی بیرون ز فَکَر دارد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هر ‌فِکر که محکمتر هر گنج‌ که عالی تر</p></div>
<div class="m2"><p>فرمانش هبا دارد اِمعانش هدر دارد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دارد صور دولت معنی همه از رسمش</p></div>
<div class="m2"><p>گویی که به رسم اندر تعیین صور دارد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای آنکه امیران را تقدیر خداوندی</p></div>
<div class="m2"><p>از قهر تو هر مهتر تا حَشر نفر دارد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا جوهر ناری را مرکز ز اثیر آید</p></div>
<div class="m2"><p>گویی به اثیر اندر چشم تو اثر دارد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هرچند حذر دارد هرکس ز تف آتش</p></div>
<div class="m2"><p>آتش ز تف خشمت حقا که حذر دارد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا زیر مدار اندر هست این مدر تیره</p></div>
<div class="m2"><p>بدخواه تو را گردون در زیر مدر دارد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>برهانی سلطانی از فخر تو فخر آرد</p></div>
<div class="m2"><p>گر عزم حضر دارد ور قصد سفر دارد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چون خواند پدر خدمت دارد پسرش نوبت</p></div>
<div class="m2"><p>نوبت ز پدر هر وقت آن به که پسر دارد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از من نبود میرا مخلص‌تر و خادم‌تر</p></div>
<div class="m2"><p>هرکس که بر این عالی درگاه گذر دارد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تا دهر فنا دارد تا مهر ضیا دارد</p></div>
<div class="m2"><p>تا بحر گهر دارد تا ابر مَطَر دارد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خواهم که تو را یزدان با عز و شرف دارد</p></div>
<div class="m2"><p>خواهم‌که تو را دولت با فتح و ظفر دارد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در مجلس تو اجرام از سعد گذر دارد</p></div>
<div class="m2"><p>بر درگه تو ایام از فتح خبر دارد</p></div></div>