---
title: >-
    شمارهٔ ۴۱۰
---
# شمارهٔ ۴۱۰

<div class="b" id="bn1"><div class="m1"><p>خدایگان زمان است و شهریار زمین</p></div>
<div class="m2"><p>سپاهدار جهان است و پهلوان ‌گزین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو پادشاه چنین باشد و سپهسالار</p></div>
<div class="m2"><p>سزای هر دو بباید یکی وزیر چنین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به حق شدست ملک را وزیر فخرالملک</p></div>
<div class="m2"><p>چنانکه بود ملک شاه را قوام‌الدین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>موافق است پسر با پسر در این ‌گیتی</p></div>
<div class="m2"><p>مساعد است پدر با پدر به خلد برین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سزد که خواجه بود وارث دوات و قلم</p></div>
<div class="m2"><p>چنانکه هست ملک وارث حسام و نگین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وزیر زادهٔ دنیا سزد مُدبّر ملک</p></div>
<div class="m2"><p>چو شاهزادهٔ دنیاست پادشاه زمین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگرچه هست چو باغی شکفته مُلک‌ مَلِک</p></div>
<div class="m2"><p>پر از درخت بلند و پر از گل و نسرین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکفته‌تر شود اکنون ز همت دستور</p></div>
<div class="m2"><p>که هست همت دستور باد فروردین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگرچه شاه و همه لشکرش گرفتستند</p></div>
<div class="m2"><p>عجم به دولت پیروز و تیغ زهرآگین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو باز صدر جهان ‌گشت یار دولت و تیغ</p></div>
<div class="m2"><p>ز سومنات بگیرند تا به ‌قسطنطین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روان شاه ملک شاه و جان خواجه نظام</p></div>
<div class="m2"><p>گزیده‌اند به ملک ملک ز علیین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز بهر هدیه فرستند یا ز بهر نثار</p></div>
<div class="m2"><p>به دست رضوان پیرایه‌های حورالعین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر آنچه خسرو مشرق بگوید و بکند</p></div>
<div class="m2"><p>به حق بود که خدایش همی کند تلقین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جهان به سیرت و آیین او همی نازد</p></div>
<div class="m2"><p>که نایب پدر است او ، به سیرت و آیین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خدایگانا، هرچ از خدای خواست دلت</p></div>
<div class="m2"><p>بیافتی و نهادی بر اسب دولت زین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ببرد عدل تو از پشت پادشاهی خم</p></div>
<div class="m2"><p>فکند تیغ تو بر روی بدسگالان چین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ضمیر روشن تو هست عقل را مسکن</p></div>
<div class="m2"><p>رکاب فرخ تو بخت را بود بالین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز بیم خنجر تو ولوله است در توران</p></div>
<div class="m2"><p>ز سهم لشکر تو زلزله است در غزنین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو از سنان تو تابد ظفر به روز مصاف</p></div>
<div class="m2"><p>چو از کمان تو پَرّد اجل به وقت‌ کمین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز نعل مرکب و از خون‌ کشتهٔ تو رسد</p></div>
<div class="m2"><p>به روی ماه غبار و به پشت ماهی طین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گهی‌ که هست سپاه تو بر لب جیحون</p></div>
<div class="m2"><p>شوند خسته و بسته سپاه خان و تکین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گهی به بیشهٔ مازندران سوارانت</p></div>
<div class="m2"><p>عصا کنند به دست سپهبدان زوبین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سپه‌کشی که ز توران به کین تو بشتافت</p></div>
<div class="m2"><p>خبر نداشت کزو تیغ تو بتوزد کین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نهاد روی به اقبال چون کشید مصاف</p></div>
<div class="m2"><p>گرفت دامن ادبار وکشته شد در حین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به‌ یک زمان سپهش منهزم شدند چنانک</p></div>
<div class="m2"><p>هزیمت از لب جیحون رسید تا در چین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مخالفی که به مازندران خلاف تو جست</p></div>
<div class="m2"><p>پناه ساخت زبیشه چنانکه شیر عرین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زپهلوان سپاهت به عاقبت بگریخت</p></div>
<div class="m2"><p>بر آن صفت که کبوتر گریزد از شاهین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بدان عدد که بود بر مجره کوکب خُرد</p></div>
<div class="m2"><p>ز بیشه با او رفتند لشکرش همگین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شدند عاقبت کار در میانهٔ راه</p></div>
<div class="m2"><p>ستارگان مَجرّه کواکب پروین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو ره نمود سعادت بر تو ایشان را</p></div>
<div class="m2"><p>رسید بهرهٔ ایشان جلالت و تمکین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همه به قبضهٔ فرمان تو شدند رهی</p></div>
<div class="m2"><p>همه به منت احسان تو شدند رهین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خلاف و طاعت تو هست اگر قیاس کنند</p></div>
<div class="m2"><p>یکی چو آذر برزین یکی چو ماء معین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خرد کجا بود آن را که او ز خیره سری</p></div>
<div class="m2"><p>شود ز ماء معین اندر آذر برزین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چه آن که باد خلاف تو دارد اندر سر</p></div>
<div class="m2"><p>چه آن‌که هست به صد ساز زیر خاک دفین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به نیک‌بختی تو هرکه دل ندارد شاد</p></div>
<div class="m2"><p>بنالد از غم و بر بخت بدکند نفرین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مگر خدای زجان آفرید عهد تورا</p></div>
<div class="m2"><p>که هست عهد تو در هر دلی چو جان شیرین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مگر قرین و همال از فرشته است تورا</p></div>
<div class="m2"><p>کز آدمی نشناسم تورا همال و قرین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو دید مجلس عالیت شاعر پدری</p></div>
<div class="m2"><p>بهشت دید به دنیا به ‌چشم روشن بین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ضمیر و خاطرش از مدح تو گرفت شرف</p></div>
<div class="m2"><p>چو آسمان زنجوم و صدف ز درّ ثمین</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همیشه تا که بود حفظ و عصمت یزدان</p></div>
<div class="m2"><p>جهانیان را حِصن‌ حصین و حَبل‌ مَتین</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نظام دین هُدی باد و عِزِّ دینِ هدی</p></div>
<div class="m2"><p>تورا وزیر و سپهدار تا به یوم‌الدین</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چنانکه ناصر دین و معین خلق تویی</p></div>
<div class="m2"><p>خدای عزوجل ناصر تو باد و معین</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>سپاه و مملکت و عمر و روزگار تورا</p></div>
<div class="m2"><p>دعا زدولت و آمین زجبرئیل امین</p></div></div>