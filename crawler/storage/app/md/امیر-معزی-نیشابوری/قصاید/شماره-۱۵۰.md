---
title: >-
    شمارهٔ ۱۵۰
---
# شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>کس ندید و کس نخواهد دید تا محشر دگر</p></div>
<div class="m2"><p>چون ملکشاه محمد پادشاه دادگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سایهٔ یزدان جلال دولت و صدر ملوک</p></div>
<div class="m2"><p>خسرو کیهان جلال ملت و فخر بشر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن جهانداری که جاویدست از او دین رسول</p></div>
<div class="m2"><p>وان شهنشاهی که خشنودست ازو جان پدر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شهریاران را به شرق و تاجداران را به غرب</p></div>
<div class="m2"><p>سیرت و کردار او تاریخ فتح است و ظفر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>او به تخت پادشاهی بر نشسته در عجم</p></div>
<div class="m2"><p>در عرب جنگ‌آوران از بیم تیغش در حذر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چه ز اقبال و هنر باید ز ایزد یافته است</p></div>
<div class="m2"><p>چیست آن کایزد ندادستش ز اقبال و هنر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>او به‌ مشرق شاد و خرم با مراد و کام دل</p></div>
<div class="m2"><p>بندگان او به مغرب جنگ را بسته کمر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>. از شجاعت وز سخاوت وز سیاست وز خرد</p></div>
<div class="m2"><p>از ولایت وز کفایت وز هدایت وز نظر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از همایون همت و تدبیر با فرهنگ و هنگ</p></div>
<div class="m2"><p>از مبارک طلعت و دیدار با تأیید و فر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از سپاه بی‌قیاس و نعمت بیرون ز حد</p></div>
<div class="m2"><p>از فتوح بیشمار و نصرت بیرون ز مَرّ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از وزیر عادل و وز چاکران نامدار</p></div>
<div class="m2"><p>از ندیم عاقل و وز بندگان نامور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هرکجا ساید رکاب و هرکجا راند سپاه</p></div>
<div class="m2"><p>منفعت یابد ز عدلش ملک‌ گیتی سر به سر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>راست‌گویی آفتاب است آن‌که از رفتار خویش</p></div>
<div class="m2"><p>صدهزاران منفعت پیدا کند در یک سفر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ایزد او را هر زمانی نصرت دیگر دهد</p></div>
<div class="m2"><p>تا تن و جان‌مخالف راکند زیروزبر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خسروا شاها خداوندا تویی کز عدل توست</p></div>
<div class="m2"><p>هم به شرق اندر نشان و هم به غرب اندر خبر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در جهانی تو ولیکن قدر تو بیش از جهان</p></div>
<div class="m2"><p>کاین جهان همچون صدف‌ گشت است و تو همچون‌ گهر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هرکه او را نیست از جاه تو در عالم پناه</p></div>
<div class="m2"><p>هرکه او را نیست از عدل تو درگیتی سپر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هست در اِدبار و محنت همچو جسمی بی‌حیات</p></div>
<div class="m2"><p>هست در تیمار و حسرت همچو چشمی بی‌بصر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هرکه او شغلی سِگالد بی‌رضا و مهر تو</p></div>
<div class="m2"><p>عمر او آید به سر آن شغل نابرده به سر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای عجب گویی رضا و مهر تو آب و هواست</p></div>
<div class="m2"><p>زانکه بی‌هر دو همی زنده نماند جانور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هرکه را یک ره زکین تو بجوشد خون دل</p></div>
<div class="m2"><p>بخت شوم او را به سنگ اندر بکوبد مغز سر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پیش درگاه تو آرد روزگار او را به قهر</p></div>
<div class="m2"><p>روی زرد واشک سرخ ومغز خشک‌و چشم تر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پای برگردن فکنده دست بسته باز پس</p></div>
<div class="m2"><p>چاوشان تو بیندازندَش از کوه و کمر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>این بود آری سزای آن که از تو چون شهی</p></div>
<div class="m2"><p>کینه دارد بر دل و پیکار دارد بر جگر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زین چنین عبرت برآرد چون بیندیشد همی</p></div>
<div class="m2"><p>دل تهی سازد ز شور و سهر تهی سازد ز شر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از حصارش آمده و آورده را چون بشمرند</p></div>
<div class="m2"><p>بیش از آن باشدکه او دارد ز اوباش و حشر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بهترین و مهترین لشکر او ایدرند</p></div>
<div class="m2"><p>با قبول و با خطر قومی و قومی با خطر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بر خطر آن است‌ کاو را دستگیر آورده‌اند</p></div>
<div class="m2"><p>باز آنکس‌ کاو خود آید با قبول است و خطر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خلق را معلوم شد کز گوهر آلب ارسلان</p></div>
<div class="m2"><p>چون تو درگیتی نخواهد بود سلطان دگر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در خرد واجب نباشد ملک جستن بر محال</p></div>
<div class="m2"><p>یک تن آمد پادشا از یک نژاد و یک‌ گهر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گرچه یعقوب پیمبر داشت فرزندان بسی</p></div>
<div class="m2"><p>پادشاه مصر یوسف شد سخن شد مختصر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هر چه اندیشه در آن بندی گشاید بی‌خلاف</p></div>
<div class="m2"><p>عاقبت نیکو تر آمد چون‌ گشاید دیرتر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>صد اثر پیدا شدست ای شاه کز مقصود خویش</p></div>
<div class="m2"><p>صد دلیل و صد نشان بینی همی در هر اثر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بخت چون عالی بود بنماید از آغاز کار</p></div>
<div class="m2"><p>روز روشن روشنی پیدا کند وقت سحر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تا همی از دور گردون بر گذر باشد جهان</p></div>
<div class="m2"><p>شاد و خرم باش و بگذر زین جهان برگذر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بخت عالی یار توست و فتح و نصرت‌ کار توست</p></div>
<div class="m2"><p>روزگارت چاکرست وکردگارت راهبر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در شجاعت رزم ساز و در سیاست خصم‌ گیر</p></div>
<div class="m2"><p>در سعادت بزم ساز و در سلامت نوش خور</p></div></div>