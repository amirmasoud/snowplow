---
title: >-
    شمارهٔ ۱۴۰
---
# شمارهٔ ۱۴۰

<div class="b" id="bn1"><div class="m1"><p>تا مبارک رایت شاه جهان آمد پدید</p></div>
<div class="m2"><p>در خراسان نقش رَوضات الجَنان آمد پدید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیر بود از برف و از سرما خراسان مدتی</p></div>
<div class="m2"><p>شد جوان تا طلعت شاه جوان آمد پدید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر زمین از ابر لؤلؤبار و بادِ مشک‌بیز</p></div>
<div class="m2"><p>فرشهایی چون مُنَقّش پرنیان آمد پدید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تودهٔ کافور اگر پنهان شد اندر کوهسار</p></div>
<div class="m2"><p>سوسن کافورگون در بوستان آمد پدید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دُرّ و مینا از نهال یاسمین آمد برون</p></div>
<div class="m2"><p>لعل و بُسّد از درخت ارغوان آمد پدید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گلستان گر نیست چون ار تنگ مانی پس چرا</p></div>
<div class="m2"><p>نقشهای مانوی در گلستان آمد پدید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این همه رنگ و نگار گونه‌گون در باغ و راغ</p></div>
<div class="m2"><p>از نشاط رایت شاه جهان آمد پدید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاه دریادل ملکشاه آن‌که از طبع و دلش</p></div>
<div class="m2"><p>گوهر و فرهنگ را دریا و کان آمد پدید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسروی‌ کز حلم‌ و طبع‌ و خشم و جودش در جهان</p></div>
<div class="m2"><p>خاک و باد و آتش و آب روان آمد پدید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن جهانداری که از اِنعام او در شرق و غرب</p></div>
<div class="m2"><p>بندگان را تا قیامت نام و نان آمد پدید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پیش از آن کایزد بساط پادشاهی گسترید</p></div>
<div class="m2"><p>نور او از گوهر آلب ارسلان آمد پدید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر زمین و بر زمان تا عدل او گسترده گشت</p></div>
<div class="m2"><p>امن و نعمت در زمین و در زمان آمد پدید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>داد او بیداد پنهان‌ کرد در زیر زمین</p></div>
<div class="m2"><p>داد پیغمبر نشان و آن نشان آمد پدید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت در عالم پدید آید شه صاحبقِران</p></div>
<div class="m2"><p>اینک اکنون آن شه صاحبقران آمد پدید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا پدید آید مبارک ذات او بر تخت ملک</p></div>
<div class="m2"><p>آفریده صورتی از عقل و جان آمد پدید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا پدید آید حُسام آبدار اندر کَفَش</p></div>
<div class="m2"><p>در دهان دولت و نصرت زبان آمد پدید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آنچه پیدا گشت در تاریخ او پیداتر است</p></div>
<div class="m2"><p>زانچه در تاریخهای باستان آمد پدید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ازکتاب فتح او در دست خلق روزگار</p></div>
<div class="m2"><p>صد هزاران سرگذشت و داستان آمد پدید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رایت او ایدر است و از شرار تیغ او</p></div>
<div class="m2"><p>خانیان را صاعقه در خانمان آمد پدید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تیغ او نیلوفرست و بر رخ اعدای ملک</p></div>
<div class="m2"><p>از غم نیلوفر او زعفران آمد پدید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سروران کشور توران شدند آسیمه‌سر</p></div>
<div class="m2"><p>تا شه‌کشوردِه‌ کشورسِتان آمد پدید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدسگالان مضطرب گشتند چون کاه سبک</p></div>
<div class="m2"><p>تا سپاه شاه چون‌ کوه گران آمد پدید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آسمان اکنون همی‌گوید که ای جیحون مجوش</p></div>
<div class="m2"><p>زانکه جوش و جَیش بحر بی‌کران آمد پدید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>روزگار اکنون همی‌گوید که ای روبه متاز</p></div>
<div class="m2"><p>زانکه سَهم و هیبت شیر ژیان آمد پدید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای شهنشاهی که سر و دولت و ملک تو را</p></div>
<div class="m2"><p>بیخ و شاخ از باختر تا خاوران آمد پدید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کمترین سالار بنماید همی در لشکرت</p></div>
<div class="m2"><p>آن هنر کز روستم در هفتخوان آمد پدید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کمترین مَنْجوق بنماید همی در موکبت</p></div>
<div class="m2"><p>آن ظفرها کز درفش کاویان آمد پدید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>درکف موسی اگر ثُعبان جنبان شد عصا</p></div>
<div class="m2"><p>مر تو را ثُعبان پَرّان در کمان آمد پدید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ور سلیمان داشت باد نامُجَسَّم زیر تخت</p></div>
<div class="m2"><p>مر تو را باد مُجَسَّم زیر ران آمد پدید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا شعاع رایت تو بر نشابور اوفتاد</p></div>
<div class="m2"><p>از پس جور و بلا عدل و امان آمد پدید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شهر خرم‌ گشت وز بهر نثار خدمتت</p></div>
<div class="m2"><p>گل‌فشان و زرفشان و جان فشان آمد پدید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا پدید آید بسی نَعت جوانی از بهار</p></div>
<div class="m2"><p>همچنان چون وصف پیری از خزان آمد پدید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از جمالت باد دایم چشم ملت را بصر</p></div>
<div class="m2"><p>کز جلالت جسم دولت را روان آمد پدید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شکر کن شاها که هرچ از ملک و دولت خواستی</p></div>
<div class="m2"><p>از قضا و حکم‌ ایزد همچنان آمد پدید</p></div></div>