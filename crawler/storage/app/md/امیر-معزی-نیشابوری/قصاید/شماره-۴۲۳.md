---
title: >-
    شمارهٔ ۴۲۳
---
# شمارهٔ ۴۲۳

<div class="b" id="bn1"><div class="m1"><p>آمد رسول عید و مه روزه نام او</p></div>
<div class="m2"><p>فرخنده باد بر شه‌گیتی سلام او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سلطان جلال دولت باقی معز دین</p></div>
<div class="m2"><p>شاهی‌ که هست دولت و دین زیر نام او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فال جهان خجسته شد و کار دین تمام</p></div>
<div class="m2"><p>از همت خجسته و عدل تمام او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ایزد مقام دولت او ساخت از فلک</p></div>
<div class="m2"><p>جز وهم آدمی نرسد بر مقام او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر ذوالفقار در کف حیدر ندیده‌ای</p></div>
<div class="m2"><p>در دست ‌شهریار نگه کن حسام او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر خسروان کشند عدو را به زهر و دام</p></div>
<div class="m2"><p>تیغ است زهر خسرو و تیرست دام او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شمشیر تیز او چو برون آید از نیام</p></div>
<div class="m2"><p>باشد دل و دو دیدهٔ شیران نیام او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اسب بلند او چو بساید زمین به نعل</p></div>
<div class="m2"><p>سَعدِ سپهر بوسه دهد بر لگام او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جوید همی‌ کلاه غلامش امیر شاه</p></div>
<div class="m2"><p>تا افسری کند زکلاه غلام او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آورده ماه روزه به‌ سلطان پیام عید</p></div>
<div class="m2"><p>سلطان به خیر داد جواب پیام او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر شب که جام آب به‌ کف بر نهد ملک</p></div>
<div class="m2"><p>خورشید و ماه را حسد آید ز جام او</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گویی‌که از بهشت فرستند هر شبی</p></div>
<div class="m2"><p>بر دست جبرئیل شراب و طعام او</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کان است کام بنده معزی به مدح شاه</p></div>
<div class="m2"><p>گوهر همی برند حکیمان زکام او</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا هر که مُنعِم است بر او واجب است حج</p></div>
<div class="m2"><p>جز کعبه نیست قبله و بیت‌الحرام او</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بادا مدام عدل شهنشاه روزگار</p></div>
<div class="m2"><p>و آسوده باد ملک ز عدل مُدام او</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روزه بر او مبارک و روزش همه چو عید</p></div>
<div class="m2"><p>کارش به‌کام دولت و دولت به کام او</p></div></div>