---
title: >-
    شمارهٔ ۱۰۵
---
# شمارهٔ ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>تا نگار من ز سنبل بر سمن پر چین نهاد</p></div>
<div class="m2"><p>داغ حسرت بر دل صورتگران چین نهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلف او برگل ز عود خام خَم در خَم فکند</p></div>
<div class="m2"><p>جَعد او بر مه ز مشک ناب چین بر چین نهاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه در یاقوت مشک آگین او شکّر سرشت</p></div>
<div class="m2"><p>قُوت عشّاق اندر آن یاقوت مشک آگین نهاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر دلی‌کز سرکشی ننهاد سر برهیچ خط</p></div>
<div class="m2"><p>زیر زلف او کنون سر بر خط مشکین نهاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من غلام آن خط مشکین‌ ک ه‌گویی مورچه</p></div>
<div class="m2"><p>پای مشک آلود بر برگ ‌گل نسرین نهاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا نبوسیدم لب شیرین او نشناختم</p></div>
<div class="m2"><p>کایزد آب زندگانی در لب شیرین نهاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>موبد آذرپرستان را دل من قبله‌گشت</p></div>
<div class="m2"><p>زانکه عشقش در دل من آذر برزین نهاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که از رنج من و از کار او آگاه شد</p></div>
<div class="m2"><p>نام من فرهاد کرد و نام او شیرین نهاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حال خویش اندر بلای او دل مسکین من</p></div>
<div class="m2"><p>خود تبه کرد و گنه بر چشم روشن بین نهاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواب خویش اندر غم او چشم روشن‌بین من</p></div>
<div class="m2"><p>دوش ‌گم کرد و بهانه بر دل مسکین نهاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چشم و ‌دل را من ملامت چون کنم از عشق خویش</p></div>
<div class="m2"><p>بند بر جان من آن پروردهٔ تکسین نهاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیکبخت آن‌کس‌که دل در بند عشق او نبست</p></div>
<div class="m2"><p>پر گرفت از عشق او بر مدح شمس‌الدین نهاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن امامی کاو شهاب ثاقب است اسلام را</p></div>
<div class="m2"><p>وان شهابی‌ کاو قَدَم بر تارک پروین نهاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عبد رزّاق آن که اندر سایهٔ اقبال او</p></div>
<div class="m2"><p>بخت عالی رای بر بالای علیین نهاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>روز اول کاو سواری کرد در میدان علم</p></div>
<div class="m2"><p>روزگار از بهر او بر اسب دولت زین نهاد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کرد در خلد برین روح‌الامین او را دعا</p></div>
<div class="m2"><p>حور آمین کر و رضوان گوش بر آمین نهاد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اختران با بخت او شطرنج رفعت باختند</p></div>
<div class="m2"><p>بخت او هر هفت را اسب و رخ و فرزین نهاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کرد دولت آستان دولتش بالین خویش</p></div>
<div class="m2"><p>جفت دولت شد کسی کاو سر بر آن بالین نهاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مشتری سعدست چون کین توزد از اعدای خویش</p></div>
<div class="m2"><p>آفتاب او را لقب مریخ‌ کیوان کین نهاد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر که در گیتی بنای کین او آغاز کرد</p></div>
<div class="m2"><p>آن بنا را قاعده بر لعنت و نفرین نهاد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فخرکردی‌گر نشستی ساعتی بر خوان او</p></div>
<div class="m2"><p>آن که او بر خوان سیمین کاسه زرین نهاد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سَجده‌ کردی گر بدیدی تخت و باروزین او</p></div>
<div class="m2"><p>آن‌که از آغاز رسم تخت و بار و زین نهاد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گرچه اکنون در عجم هستند ارادان‌ا بی‌قیاس</p></div>
<div class="m2"><p>روز رادی بخشش بی‌منت او آیین نهاد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ورچه در عهد نبی بودند شیران بی‌شمار</p></div>
<div class="m2"><p>روز مردی پای در صف صاحب صِفّین نهاد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای برار زادهٔ صدری که دولت را اساس</p></div>
<div class="m2"><p>از زمین‌ کاشغر تا بحر ‌قُسطَنطین نهاد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>او به عقبی رفت و اندر قلعهٔ اقبال خویش</p></div>
<div class="m2"><p>از علوم تو ذخیره حشمت و تمکین نهاد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ساخت او از گوهر شکر تو تاجی قیمتی</p></div>
<div class="m2"><p>در بهشت‌ آن تاج ‌را بر فرقِ عِلّیین نهاد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آسمان کز باد فروردین زمین را زنده کرد</p></div>
<div class="m2"><p>لطف و طبع تو مگر در باد فروردین نهاد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نار پیش طین سجود از رشک نور تو نکرد</p></div>
<div class="m2"><p>کاو همی دانست کایزد نور تو در طین نهاد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آرزو آمد فلک را تا بسنجد عقل تو</p></div>
<div class="m2"><p>کردگار اندرکف او کفهٔ شاهین نهاد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا که در دست تو پیدا شد کلید گنج علم</p></div>
<div class="m2"><p>دشمنت قفل جهالت بر دل غمگین نهاد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا همی دم زد بداندیش تو اندر سِجن بود</p></div>
<div class="m2"><p>چون دمش بگسست با از سِجن در سِجّین نهاد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تیزی خنجر نهاد اندر سر اقلام تو</p></div>
<div class="m2"><p>آن‌که اندر چوب موسی هیئت تِنّین نهاد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>واندر اقلام تو از بهر هلاک دشمنان</p></div>
<div class="m2"><p>قد تیر و شکل رُمح و پیکر زوبین نهاد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مهترا از ضربت گردون دل من خسته شد</p></div>
<div class="m2"><p>لطف تو بر خستهٔ من مرهم و تسکین نهاد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تا ضمیرم یافت در مدح تو تلقین از خرد</p></div>
<div class="m2"><p>دام فکرت بر ره معنی بر آن تلقین نهاد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همت عالیت چون بشنید گفتار مرا</p></div>
<div class="m2"><p>کرد تحسین و ز احسان مهر بر تحسین نهاد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چون دلت را آن پسند آمد عروس‌ شعر من</p></div>
<div class="m2"><p>جودت او را مبلغی هر سال برکابین نهاد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>عاجز آید زین سخن‌ گر زنده‌ گردد آن که او</p></div>
<div class="m2"><p>داستان بیژن و افسانهٔ‌گرگین نهاد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تا که باشد در نبی نام و نشان آن نبی</p></div>
<div class="m2"><p>کاو پسر را گاه قربان بر گلو سِکّین نهاد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بر تو فرخ باد عید آن نبی‌ کاندر جهان</p></div>
<div class="m2"><p>دین تازی را شریعت تا به یوم‌الدین نهاد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کرد ایزد هم صلات و هم صیام از تو قبول</p></div>
<div class="m2"><p>وز تو راضی جان آن‌کاندر شریعت این نهاد</p></div></div>