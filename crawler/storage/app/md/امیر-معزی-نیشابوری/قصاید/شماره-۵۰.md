---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>جشن عید اندر شریعت سنت پیغمبرست</p></div>
<div class="m2"><p>قدر او از قدر دیگر جشنها افزونترست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست این جشن جهان افروز در سالی دو بار</p></div>
<div class="m2"><p>ملک را فر ملک هر روز جشنی دیگرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عُدّت مُستَظهر و فخر ملوک روزگار</p></div>
<div class="m2"><p>بازوی دولت که تاج ملت پیغمبرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سایهٔ یزدان و خورشید همه سلجوقیان</p></div>
<div class="m2"><p>ناصر دین خسرو مشرق که نامش سنجرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شهریاری کز خطاب و نام او نازد همی</p></div>
<div class="m2"><p>هرکجا درکشور ایران خطیب و منبرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در سرای پادشاهی بر سریر خسروی</p></div>
<div class="m2"><p>چون ملک سلطان و چون الب‌ارسلان نیک اخترست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوهر سلجوق را زین و جمال از فر اوست</p></div>
<div class="m2"><p>همچنان چون عِقد را زین و جمال از گوهر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در بقای او جهان را رامش و آرامش است</p></div>
<div class="m2"><p>همچو تن را جان بقای او جهان را درخور است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رای ملک افروز او را ماه تابان خادم است</p></div>
<div class="m2"><p>دولت پیروز او را چرخ ‌گردون چاکر است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن درختی‌ کایزد اندر باغ اقبالش نشاند</p></div>
<div class="m2"><p>بیخ و شاخ آن درخت از باختر تا خاورست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اندر آن وقتی که حاضر باشی اندر حضرتش</p></div>
<div class="m2"><p>منظرش چون بنگری زیباتر از هر منظر است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>و اندر آن وقتی که غایب باشی از درگاه او</p></div>
<div class="m2"><p>مخبرش چون بشنوی مخبرتر از هر مخبرست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همچنان کاندر چهارم آسمان است آفتاب</p></div>
<div class="m2"><p>طلعت میمون او اندر چهارم کشور است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر به باغ اندر بیفزاید ز عرعر خرمی</p></div>
<div class="m2"><p>رایت او باغ نصرت را به‌جای عرعرست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ور سر شاهان بیفزاید به افسر روز بار</p></div>
<div class="m2"><p>نامهٔ او بر سر شاهان به جای افسرست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زانکه اندر خنجر او هست ز‌هر جانگزای</p></div>
<div class="m2"><p>جان‌گزاید دشمنان را تا به‌دستش خنجرست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زانکه اندر ساغر او هست نوش جانفزای</p></div>
<div class="m2"><p>جان فزاید دوستان را تا به دستش ساغرست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باید اندرخدمتش پشت بزرگان چنبری</p></div>
<div class="m2"><p>پشت‌گردون زین‌قبل درخدمتش چون‌چنبرست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یادکرد او بزرگان را ثبات دولت است</p></div>
<div class="m2"><p>آفرین او حکیمان را طراز دفترست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کوه بینی زیر دریا هرکجا باشد سوار</p></div>
<div class="m2"><p>زانکه او دریا دل است و اسب او که‌ پیکرست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آب بینی جفت آذر چون زند د‌ر رزم تیغ</p></div>
<div class="m2"><p>زانکه تیغ او به‌رنگ آب و جفت آذرست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اندر آن صحرا که او با دشمنان ‌کرده است حرب</p></div>
<div class="m2"><p>تا گه محشر در آن صحرا نهیب محشرست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر امید پادشاهی هر کسی دستی بزد</p></div>
<div class="m2"><p>منت ایزد را که اکنون حق به دست حقورست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>برد کیفر هر که از پیمانش بیرون برد سر</p></div>
<div class="m2"><p>آنچه پیش آمد قدر خان را نشان‌ کیفرست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بر خلاف دولت او سر ‌دهد ناگه به ‌باد</p></div>
<div class="m2"><p>هرکه را از کینه و پرخاش بادی در سرست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خانهٔ اقبال او دارد ز پیروزی دری</p></div>
<div class="m2"><p>بدسگال ملک او چون حلقه بیرون از در است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پیش او خصمان همه اعجاز نخل اند از قیاس</p></div>
<div class="m2"><p>حملهٔ او از پس خصمان چو باد صرصرست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گه به‌سوی رایت رای است فرخ رای او</p></div>
<div class="m2"><p>گه زبهر غَزْوْ قصد او به قصر قیصرست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر بفرماید بگیرد ملک هند و ملک روم</p></div>
<div class="m2"><p>صاحب الجیش‌ آن‌که او را پهلوان لشکرست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا نه بس مدت حصار غور بگشاید به ‌زور</p></div>
<div class="m2"><p>گر حصار سومنات و قلعهٔ کالنجرست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>رخنه ‌گرداند به‌ اقبال ملک حِصن عدو</p></div>
<div class="m2"><p>گر چوکوه بیستون و بارهٔ اسکندرست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هست ‌کار ناصرالدین نصرت و فتح و ظفر</p></div>
<div class="m2"><p>تا وزیرش مهربان و عا‌دل و دین‌پرورست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>با ملک‌ سلطان قوام‌الدین به جنت هست شاد</p></div>
<div class="m2"><p>با ملک سنجر نظام‌الدین به‌شادی ایدرست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هست خدمتگر در آن ‌گیتی پدر پیش پدر</p></div>
<div class="m2"><p>واندرین گیتی پسر پیش پسر خدمتگرست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ای‌ خداوند‌ی که بزم توست فردوس برین</p></div>
<div class="m2"><p>واندرو ساقی چو حورالعین و می چون کوثرست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>می ستان هر لحظه از دست نگار آزری</p></div>
<div class="m2"><p>د‌ر چنین جشنی‌ که آیین خلیل آزرست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>عشرتت اندر زیادت باد اندر روز عید</p></div>
<div class="m2"><p>زانکه طبعت عشرت افزای است و شادی گسترست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>باد زیر سایهٔ عدلت جهان بی‌داوری</p></div>
<div class="m2"><p>زانکه عدل تو همه خلق جهان را داورست</p></div></div>