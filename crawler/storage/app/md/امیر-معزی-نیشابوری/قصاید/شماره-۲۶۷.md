---
title: >-
    شمارهٔ ۲۶۷
---
# شمارهٔ ۲۶۷

<div class="b" id="bn1"><div class="m1"><p>ای شاه همه عالم و فخرگهر خویش</p></div>
<div class="m2"><p>وی در همه آفاق نموده اثر خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از چین و ختا تا به فلسطین که رسانید</p></div>
<div class="m2"><p>جز تو به جوانمردی و مردی خبر خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خصمان تورا چون تن و جان در خطر افتاد</p></div>
<div class="m2"><p>از کین تو جستند یکایک خطر خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خیره سری رغبت پیکار توکردند</p></div>
<div class="m2"><p>تا در سر پیکار تو کردند سر خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کشور توران و به غزنین و عراقین</p></div>
<div class="m2"><p>چون خواستی آوازهٔ فتح و ظفر خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر سه بگرفتی و سپردی به سه خسرو</p></div>
<div class="m2"><p>در جود و شجاعت بنمودی هنر خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرگز پدر و جد تو این کار نکردند</p></div>
<div class="m2"><p>پیشی تو بدین کار ز جد و پدر خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زیبد که فرستی سوی حوران بهشتی</p></div>
<div class="m2"><p>فهرست عجایب ز کتاب سِیَر خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا هدیه فرستند به درگاه تو از خلد</p></div>
<div class="m2"><p>تاج و کمر و یاره و دُرّ و گهر خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بس دیر نماندست که از بهر تو گردون</p></div>
<div class="m2"><p>سازد کمر و کیش ز شمس و قمر خویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاهان جهان چون کمر و کیش تو بینند</p></div>
<div class="m2"><p>شاید که ننازند به‌ کیش و کمر خویش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هرگز ز بر خویش سعادت نکند دور</p></div>
<div class="m2"><p>آن را که تو یک بار بخوانی به بر خویش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر تخت شهنشاهی جاوید همی ساز</p></div>
<div class="m2"><p>کار همه آفاق به عدل و نظر خویش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شخص تو امان یافته از تیر حوادث</p></div>
<div class="m2"><p>تو ساخته از عصمت یزدان سپر خویش</p></div></div>