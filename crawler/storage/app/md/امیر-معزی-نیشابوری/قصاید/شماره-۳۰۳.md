---
title: >-
    شمارهٔ ۳۰۳
---
# شمارهٔ ۳۰۳

<div class="b" id="bn1"><div class="m1"><p>جاوید ز یادِ خسروِ عالم</p></div>
<div class="m2"><p>سلطان جهان شهنشهِ اعظم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاهی‌ که نشاط عیش او باقی</p></div>
<div class="m2"><p>شاهی‌ که صبوح بزم او خرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاهی که ز خسروان و سلطانان</p></div>
<div class="m2"><p>نازنده به اوست گوهر آدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای خسرو نیکبخت نیک‌اختر</p></div>
<div class="m2"><p>سلطان جهان و داور عالم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عزّ ولی تو هر زمانی بیش</p></div>
<div class="m2"><p>عمر عدوی تو هر زمانی کم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>افاق مسخرست حُکمَت را</p></div>
<div class="m2"><p>گویی که به دست توست جام جم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر بخت نهد موافق تو رَخت</p></div>
<div class="m2"><p>در دام زند مخالف تو دم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا هست جهان شه جهان بادی</p></div>
<div class="m2"><p>تو شاد و مخالف تو جفت غم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در خانهٔ دوستان تو شادی</p></div>
<div class="m2"><p>در خانهٔ دشمنان تو ماتم</p></div></div>