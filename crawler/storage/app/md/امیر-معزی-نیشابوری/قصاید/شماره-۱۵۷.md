---
title: >-
    شمارهٔ ۱۵۷
---
# شمارهٔ ۱۵۷

<div class="b" id="bn1"><div class="m1"><p>همیشه پرشکن است آن دو زلف حلقه‌ پذیر</p></div>
<div class="m2"><p>شکن‌شکن چو زره حلقه‌حلقه چون زنجیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رسد ز حلقه بدو هر زمان هزار نفر</p></div>
<div class="m2"><p>وز آن نفر چو دل من هزار تن به‌نفیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز تیرگیش همی روشنی دهد بیرون</p></div>
<div class="m2"><p>بود هر آینه از شب دمیدن شبگیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنانکه شیر بود پرورندهٔ اطفال</p></div>
<div class="m2"><p>شکنج و حلقهٔ او هست پرورندهٔ سیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز مشک بر مه روشن همی‌کشد پرگار</p></div>
<div class="m2"><p>ز قیر بر گل و سوسن همی‌کند تصویر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به مشک ماند اگر گل نگار باشد مشک</p></div>
<div class="m2"><p>به قیر ماند اگر مه پرست باشد قیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عبیر و غالیه گر رنگ و بوی آن دارند</p></div>
<div class="m2"><p>به عشق در ا‌نظرا من زغالیه است و عبیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به فعل و شکل به‌ دام و کمند ماند راست</p></div>
<div class="m2"><p>کمند جادو بندست و دام عاشق‌ گیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلی‌ که بسته و غمگین شده است در گرهش</p></div>
<div class="m2"><p>گشاده گردد و خیره شود به مدح امیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جمال آل حسن فخر گوهر اسحاق</p></div>
<div class="m2"><p>که هست بر فلک دولت آفتاب منبر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بزرگوار جهان مَخلَص‌ِ خلیفهٔ حق</p></div>
<div class="m2"><p>بشیر هر بشر و فخر دودمان وزیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مظفر آن که کَفَش رایت کفایت را</p></div>
<div class="m2"><p>همی درست‌کند پیش کافیان تفسیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر آن چه هست مقدر ز حسن مخلوقات</p></div>
<div class="m2"><p>یقین بدان که همه دون اوست جز تقدیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دل منور او هست عقل را عنصر</p></div>
<div class="m2"><p>ید مؤید او هست جود را اکسیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>امکبر اندا بر نام او تخلص و مدح</p></div>
<div class="m2"><p>که مدح همچو نمازست و نام او تکبیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به‌ آفرین خدای آن ‌که بود در شب و روز</p></div>
<div class="m2"><p>محرری که کند آفرین او تحریر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کسی‌ که بر تن و بر جان او سگالد غدر</p></div>
<div class="m2"><p>ز غد‌ر دهر شود چشمهای او چو غدیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اگر چه قدرت حق بیشتر ز قدرت جم</p></div>
<div class="m2"><p>به وقت قدرت تدبیر هست آصف پیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگرچه در همه چیزی مؤثرست فلک</p></div>
<div class="m2"><p>بلند همت او در فلک‌ کند تاثیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز بهر مصلحت ملک باشدش فکرت</p></div>
<div class="m2"><p>ز بهر منفعت خلق باشدش تدبیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همه لطافت نور از اثیر بگریزد</p></div>
<div class="m2"><p>اگر رسد اثر خشم او به چرخ اثیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به روز بزم قدم درکف موافق او</p></div>
<div class="m2"><p>شعاع نور دهد همچو آفتاب منیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به روز رزم زره بر تن مخالف او</p></div>
<div class="m2"><p>ز هم ‌گسسته شود همچو تارهای حریر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ایا همیشه دل پاک تو به فخر مشار</p></div>
<div class="m2"><p>ویا همیشه ‌کف راد تو به خیر مشیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز کارهای پسندیده کار توست سرور</p></div>
<div class="m2"><p>ز جایهای گرانمایه جای توست سریر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فقیر بود جهان بی‌تو ازکفایت و فخر</p></div>
<div class="m2"><p>تو آمدیّ و غنی شد به تو جهانِ فقیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زمانه شیفتهٔ دل بود و تیرهٔ چشم کنون</p></div>
<div class="m2"><p>به تن‌ گرفت قرار و به‌ چشم گشت قریر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز دست و طبع تو گیتی همه شکفته شود</p></div>
<div class="m2"><p>به طبع باد صبایی به دست ابر مطیر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نظیر گفت نیارم تو را به هیچ صفت</p></div>
<div class="m2"><p>از آن قِبَل که خدایت نیافرید نظیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دل و ضمیر تو ماند همی به لؤلؤ تر</p></div>
<div class="m2"><p>میان لؤلؤ لالا توراست بحر غزیر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زریر و خون به رخ و چشم دشمن تو درست</p></div>
<div class="m2"><p>مگر زمانه بر او وقف کرد خون و زریر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زحل به تیر نحوست مخالفان تو را</p></div>
<div class="m2"><p>همی خَلَد جگر آری خَلَنده باشد تیر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز رنج و سختی چون زیر و زار ناله شدست</p></div>
<div class="m2"><p>تن عدؤت‌ بلی زار ناله باشد و زیر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>قضای خالق عرش است وعدهٔ تو مگر</p></div>
<div class="m2"><p>که هر دو را نبود نیم دم زدن تأخیر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مگر مدیح تو شد چشم عقل را قوت</p></div>
<div class="m2"><p>که چشم عقل بود بی‌مدایح تو ضریر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مگر لقای تو اصل بصیرت است و بصر</p></div>
<div class="m2"><p>که چشمهای سر و تن بدو شدست بصیر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به نامه‌ای چو نویسد دبیر نام تو را</p></div>
<div class="m2"><p>دهانِ دهر دهد بوسه بر دهان دبیر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اگر خیال تو بیند بداختر اندر خواب</p></div>
<div class="m2"><p>مُعَبِرّش همه نیک‌اختری کند تعبیر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>وگر دهی تو اسیر زمانه را قوت</p></div>
<div class="m2"><p>شود زمانه به دست اسیر خویش اسیر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو حال بندگی من تو را خداوندا</p></div>
<div class="m2"><p>مُقرّرست مرا نیست طاقت تقریر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>جوان و پیر سزد آفرین‌گر تو چو من</p></div>
<div class="m2"><p>به سال و ماه جوان و به فضل و دانش پیر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مهذّب است به تو حکمتم زهی تهذیب</p></div>
<div class="m2"><p>موّقرست به تو نعمتم زهی توقیر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به جای هر نفسی‌ گر ستایشی کنمت</p></div>
<div class="m2"><p>به جان تو که شناسم ز خویشتن تقصیر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>وگر کنم به همه عمر شکر نعمت تو</p></div>
<div class="m2"><p>به نعمت تو که از خویشتن خورم تشویر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>همیشه تا که به خیر و به شر میان بشر</p></div>
<div class="m2"><p>همی رسول و بشیر آید از صغیر و کبیر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>زمانه باد به شر مخالف تو رسول</p></div>
<div class="m2"><p>ستاره باد به خیر موافق تو بشیر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تو خوش نشسته و پیش تو ایستاده به پای</p></div>
<div class="m2"><p>بتان نوش لب دوست جوی دشمن‌ گیر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بلند قامت ایشان چو سرو در کِشمَر</p></div>
<div class="m2"><p>بدیع صورت ایشان جو نقش در کشمیر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تو جفت طاعت و گردون تو را همیشه مطیع</p></div>
<div class="m2"><p>تو یار نصرت و یزدان تورا همیسه نصیر</p></div></div>