---
title: >-
    شمارهٔ ۱۰۳
---
# شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>شهی که گوهر و دینار رایگانی داد</p></div>
<div class="m2"><p>هرآنچه داد خدایش خدایگانی داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عزیزکرد بدو دین و داد و بگزیدش</p></div>
<div class="m2"><p>به دین و دادش ده چیز و رایگانی داد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگین و افسر و شمشیر و تخت و تاج وکمر</p></div>
<div class="m2"><p>سپاه و دولت و پیروزی و جوانی داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یقین بدان که دهد آن جهانیش فردا</p></div>
<div class="m2"><p>فزون از آن که به او ملک این جهانی داد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر آن شهی‌که ظفر یافت بر مخالف خویش</p></div>
<div class="m2"><p>زآسمان و زسیارگان نشانی داد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بخت خویش نشان داد شاه روز ظفر</p></div>
<div class="m2"><p>نه از ستاره و دوران آسمانی داد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لطیف طبع ملک داد باد را سَبُکی</p></div>
<div class="m2"><p>چنانکه حلم ملک خاک را گرانی داد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حصار دولت از آن آمد استوار و بلند</p></div>
<div class="m2"><p>که تیغ را مَلِک شرق پاسبانی داد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسا مخالف دولت‌که شاه مالِشِ او</p></div>
<div class="m2"><p>به تیر و ناوک آن تیغ هندوانی داد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شهان به زیر زمین گنج را نهان کردند</p></div>
<div class="m2"><p>خدایگان به عطا گنج شایگانی داد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نداد هیچ کسی خاک رایگان به مثل</p></div>
<div class="m2"><p>چنانکه شاه جهان زر به رایگانی داد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به آفرین ملک من رهی همی نازم</p></div>
<div class="m2"><p>که آفرینش لفظ مرا معانی داد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به بزم خویش مرا پیش خاصگان بنشاند</p></div>
<div class="m2"><p>به دست خویش به من بنده دوستگانی داد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به زندگانی خضرم که شهریار جهان</p></div>
<div class="m2"><p>ز جام خویش مرا آب زندگانی داد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز عدل باد دلش شادمان و برخوردار</p></div>
<div class="m2"><p>که عدل او به همه خلق شادمانی داد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همیشه شاه جهان باد کامران و عزیز</p></div>
<div class="m2"><p>که بخت را هنرش عِزّ و کامرانی داد</p></div></div>