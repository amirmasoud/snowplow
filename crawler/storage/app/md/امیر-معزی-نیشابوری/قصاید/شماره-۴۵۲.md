---
title: >-
    شمارهٔ ۴۵۲
---
# شمارهٔ ۴۵۲

<div class="b" id="bn1"><div class="m1"><p>ایا شاهی‌که عالم را همی زیر عَلَم داری</p></div>
<div class="m2"><p>به دولت فرق جباران همی زیر قدم داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عرب را با عجم فخرست تا محشر به اقبالت</p></div>
<div class="m2"><p>که هم ملک عرب داری و هم ملک عجم داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر رشک آید از تو شهریاران را عجب نبود</p></div>
<div class="m2"><p>که شاهی و جوانی و جوان‌بختی به هم داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به‌ شمشیر و قلم نازند رزم‌ و بزم از این‌ معنی</p></div>
<div class="m2"><p>که تو درکف به رزم ‌و بزم‌ شمشیر و قلم داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>علمهای تو گویی دفتر اشکال عالم شد</p></div>
<div class="m2"><p>که عالم سر به سر پیموده در زیر علم داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کجا ملک است تا ملک است در توران و در ایران</p></div>
<div class="m2"><p>به نام خویش تا محشر بدین داغ و رقم داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه باید بیش از این برهان‌ که نام خویش جاویدان</p></div>
<div class="m2"><p>جمال خطبه و منشور و دینار و درم داری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مجسم تیغ‌ گوهر دار از فتح و ظفر داری</p></div>
<div class="m2"><p>مصور دست‌ گوهر بار از جود و کرم داری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز نعمت نیکخواهان را حریفان طرب داری</p></div>
<div class="m2"><p>زمحنت بدسگالان را ندیمان نَدَم داری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز مهر تو ضیا خیزد ز کین تو ظُلَم زاید</p></div>
<div class="m2"><p>ولی را در ضیا داری عدو را در ظلم داری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بداندیشان ملت را حریف اندر بلا داری</p></div>
<div class="m2"><p>نکوخواهان دولت را غریق اندر نعم داری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به بهروزی روی هر جا و بازآیی به پیروزی</p></div>
<div class="m2"><p>که بهروزی سپه داری و پیروزی حشم داری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خدنگ مرغ پرنده است و اسبت باد پوینده</p></div>
<div class="m2"><p>مطیعت‌ گشت مرغ و بادگویی مهر جم داری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حدیث و قصهٔ اسفندیار و روستم تا کی</p></div>
<div class="m2"><p>تو در لشکر هزار اسفندیار و روستم داری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو را نتوان برابرکرد با ایشان معاذالله</p></div>
<div class="m2"><p>که چون ایشان به درگه بر غلامان و خدم داری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو آن شاهی‌که روز رزم‌ گردون داری اندر یم</p></div>
<div class="m2"><p>که تیغی همچو گردون و یمینی همچو یم داری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو آن شاهی که طاعتگاه قِسّیسان و رُهبانان</p></div>
<div class="m2"><p>به شمشیر صنم‌ وارت‌ چه خالی از صنم داری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صد و هشتاد فتح از خویش داری بیش درگیتی</p></div>
<div class="m2"><p>وگرچه سال خویش از نیمهٔ هشتاد کم داری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زجود خویش بر عالم همی قسمت‌ کنی روزی</p></div>
<div class="m2"><p>به قسمت کردن روزی تو پنداری قسم داری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هم اندر کار دینی و هم اندر کار دنیایی</p></div>
<div class="m2"><p>از آن قاطع بود حکمت‌ که دانش را حکم داری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عزیز و محتشم آنکس بود در دین و در دنیا</p></div>
<div class="m2"><p>که او را تو به اقبالش عزیز و محتشم داری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اگر باغِ ارم جایی است کز وی دل شود خرّم</p></div>
<div class="m2"><p>تو ‌گیتی را به فرّ خویش چون باغ ‌ارم داری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وگر بیت حرم حِصنی است‌ کآنجا ایمنی باشد</p></div>
<div class="m2"><p>تو عالم را به عِلْم خویش چون بیت حرم داری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به انصاف و به‌ عدل اندر چنانی تو که‌ گر خواهی</p></div>
<div class="m2"><p>میان بیشه ضیغم را نگهبان غَنَم داری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به تو معدوم شدکفر و به تو موجود شد ایمان</p></div>
<div class="m2"><p>سزد گر تا جهان باشد وجود بی‌عدم داری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به‌کام دل نشاط افزای و شادی‌ کن‌ که دلها را</p></div>
<div class="m2"><p>به شادی و نشاط خویش بی‌تیمار و غم داری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همیشه سایهٔ عدل تو بادا بر سر عالم</p></div>
<div class="m2"><p>که عالم را به عدل خویش خالی از ستم داری</p></div></div>