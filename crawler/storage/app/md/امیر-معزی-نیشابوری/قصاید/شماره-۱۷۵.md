---
title: >-
    شمارهٔ ۱۷۵
---
# شمارهٔ ۱۷۵

<div class="b" id="bn1"><div class="m1"><p>آن شمع چه شمع است که برنامه و دفتر</p></div>
<div class="m2"><p>دودش همه مشک است و فروغش همه‌ گوهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وان ابر چه ابرست که بر سوسن و نسرین</p></div>
<div class="m2"><p>بارد همه یاقوت و فشاند همه عنبر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وان باز چه بازست که باشد گه پرواز</p></div>
<div class="m2"><p>گیرندهٔ بی‌چنگل و پرَّندهٔ بی پر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وان شاخ چه شاخ است که در باغ کفایت</p></div>
<div class="m2"><p>توفیق بود برگش و توقیر بود بر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وان تیر چه تیرست که بالای عدو را</p></div>
<div class="m2"><p>دارد چو کمان چفته به پیکان مقشر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وان مار چه مارست که چون معجز موسی</p></div>
<div class="m2"><p>ناچیز کند تنبل خصمان فسونگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وان مارهٔ زر چیست که مردان جهان را</p></div>
<div class="m2"><p>بر فضل و هنرمندی بر سنگ زند زر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وان ماهی بر خشک چه چیزست‌ که دریا</p></div>
<div class="m2"><p>هر دم زدن از قیر کند تارک او تر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گویی که شهابی است مقارن شده با ماه</p></div>
<div class="m2"><p>واندر کف خورشید ز شب ساخته اختر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یا طرفه چراغی است که از نور و دُخانش</p></div>
<div class="m2"><p>ایام مزین شده اسلام منور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یا هست به خوارزم ز تقدیر وکیلی</p></div>
<div class="m2"><p>تا فخر معالی کند ارزاق مقدر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صدری ز محل زین ملوک همه‌ گیتی</p></div>
<div class="m2"><p>بدری ز شرف شمس‌کفات همه کشور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن خواجه‌ که سعدست و ا‌امینا است و ظهیرست</p></div>
<div class="m2"><p>در دولت و ملک ملک و دین پیمبر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بو سعد که تا طلعت او گشت پدیدار</p></div>
<div class="m2"><p>مسعود شد از طلعت او طالع و اختر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خوب است همه سیرت او درخور صورت</p></div>
<div class="m2"><p>زیباست همه مخبر او درخور منظر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آباد همه ساله بر آن صورت و سیرت</p></div>
<div class="m2"><p>آباد همه ساله بر آن منظر و مخبر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن روز که ایام ندا کرد که هرگز</p></div>
<div class="m2"><p>کس را نشود چنبر افلاک مسخر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پیروزی او دست برآورد و درآورد</p></div>
<div class="m2"><p>ناگاه سر چنبر افلاک به چنبر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>با ناوک تدبیرش و با نیزهٔ عزمش</p></div>
<div class="m2"><p>چون خانهٔ زنبور شود سد سکندر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خوارزم شد اکنون چو یکی دفتر کامل</p></div>
<div class="m2"><p>خیرات و صلاتش طرف و نکتهٔ دفتر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر زان طرف و نکته یکی نقطه بکاهد</p></div>
<div class="m2"><p>آن دفتر کامل شود اجزای مبتر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در ملک عجم کار دگر کارگزاران</p></div>
<div class="m2"><p>اندوختن نعمت و مال است سراسر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>او کارگزاری است که کارش ز کریمی</p></div>
<div class="m2"><p>پروردن بنده است و نوازیدن چاکر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون بنگرد اندر سیرَش مرد خردمند</p></div>
<div class="m2"><p>عنوان شرف بیند و پیرایهٔ مفخر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خلقش به صبا بوی دهد در مه نوروز</p></div>
<div class="m2"><p>از خار بدان بوی برآید گل احمر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>وز همت او سایه برافتد به درختان</p></div>
<div class="m2"><p>گیرند درختان صفت ‌گنبد اخضر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اندر کنف دولت او خسته نگردد</p></div>
<div class="m2"><p>آهو به ره از ناخن و دندان غضنفر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ور سوی‌کبوتر نگرد بخت بلندش</p></div>
<div class="m2"><p>شاهین به عنایت نگرد سوی‌ کبوتر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای بار خدایی که همی شکر توگویند</p></div>
<div class="m2"><p>شاهنشه و خوارزمشه و خواجه و لشکر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آن شهر عقیم است‌ کزو خاسته‌ای تو</p></div>
<div class="m2"><p>زیرا که از آن شهر نخیزد چو تو دیگر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از روی تو و رای تو اجرام سماوی</p></div>
<div class="m2"><p>گیرند همه فال و پذیرند همه فر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خدمتگر نفس تو و نقش قلم توست</p></div>
<div class="m2"><p>برجیس به ماهی و عطارد به دو پیکر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر خسته شد از خنجر رستم دل سهراب</p></div>
<div class="m2"><p>ورکنده شد از بازوی حیدر در خیبر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کلک تو گه ‌خشم عدو را بخلد دل</p></div>
<div class="m2"><p>عزم تو گه عدل ستم را بکند در</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ای‌ کلک تو در قدرت چون خنجر رستم</p></div>
<div class="m2"><p>وی عزم تو در قوت چون بازوی حیدر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جودی تو اگر جود توان دید مجسم</p></div>
<div class="m2"><p>عقلی تو اگر عقل توان دید مصور</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>زان است که خورشید تغیر نپذیرد</p></div>
<div class="m2"><p>کاو هست به هیات چو دوات تو مدور</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زان است ‌که آفت نرسد قطب سما را</p></div>
<div class="m2"><p>کاو بر صفت کلک تو دارد خط محور</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از کلک گهر گستر تو خلق جهان را</p></div>
<div class="m2"><p>شکر است چنان ‌کز نی خوزستان شکر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مشکورتر از تو به جهان کیست ‌که هستند</p></div>
<div class="m2"><p>هم خلق ز تو شاکر و هم خالق اکبر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از فر تو خوارزم چو فردوس برین است</p></div>
<div class="m2"><p>جیحون روان هست در آن چشمهٔ کوثر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گر کوثر و فردوس بر این نسیه به عقبی است</p></div>
<div class="m2"><p>این هر دو ز عدل و نظرت نقد شد ایدر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اقبال سپهرست در الفاظ تو مدغم</p></div>
<div class="m2"><p>ارزاق جهان است در اقلام تو مضمر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>درویش که بیند به شب اقبال تو در خواب</p></div>
<div class="m2"><p>او را کند احسان تو شبگیر توانگر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>در مجلس تذکیر امامان سخنگوی</p></div>
<div class="m2"><p>در خطبهٔ تحمید خطیبان سخنور</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>خواهند ثنای تو همی بر سر کرسی</p></div>
<div class="m2"><p>گویند دعای تو همی بر سر منبر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بر مادح تو مدح تو چون حرز براهیم</p></div>
<div class="m2"><p>از آتش سوزنده‌ کند سوسن و عبهر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>کر کنگرهٔ خُلد همی حور بهشتی</p></div>
<div class="m2"><p>مداح تو را هدیه دهد جامه و زیور</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>امروز ثناخر تویی از اهل معالی</p></div>
<div class="m2"><p>وز اهل معانی منم امروز ثناگر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>آنجا که بود جمع معالی و معانی</p></div>
<div class="m2"><p>مداح ثناگر به و ممدوح ثنا خر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تا اختر سیار بدین گنبد دوار</p></div>
<div class="m2"><p>هر شب کند از باختر آهنگ به خاور</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>در خاور و در باختر اقبال و قبولت</p></div>
<div class="m2"><p>تابنده و پاینده همی باد چو اختر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نازنده همی باد به تو دین محمد</p></div>
<div class="m2"><p>تا ملک محمد بود و دولت سنجر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>فرخ‌تر و فرخنده‌تر امروز تو از دی</p></div>
<div class="m2"><p>و امسال تو از پار همایون‌تر و خوشتر</p></div></div>