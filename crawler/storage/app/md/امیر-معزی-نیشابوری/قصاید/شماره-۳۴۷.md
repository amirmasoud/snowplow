---
title: >-
    شمارهٔ ۳۴۷
---
# شمارهٔ ۳۴۷

<div class="b" id="bn1"><div class="m1"><p>جهان پیر دیگرباره تازه گشت و جوان</p></div>
<div class="m2"><p>به تازگی و جوانی چو بخت شاه جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه باک از آن‌که جهان‌گه جوان وگه پیرست</p></div>
<div class="m2"><p>همیشه شاه جوان است و بخت شاه جوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر ملوک، ملک شاه دادگر ملکی</p></div>
<div class="m2"><p>که شهریار زمین است و پادشاه زمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زکین او به دل اندر فسرده گردد خون</p></div>
<div class="m2"><p>ز مهر او به تن اندر شکفته‌ گردد جان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نثار خدمت او واجب است و زین معنی</p></div>
<div class="m2"><p>قضاگشاده زبان است و بخت بسته میان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنانکه بسته میان است بخت در خدمت</p></div>
<div class="m2"><p>همیشه هست قضا بر ثناگشاده زبان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مبارزان عرب چون عجم شدند امسال</p></div>
<div class="m2"><p>رعیت مَلِک مُلک بخش مُلک ستان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زخسروان عجم کوشش است وزو بخشش</p></div>
<div class="m2"><p>زسروران عرب طاعت است وزو فرمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دوگوشه دارد کیهان زمشرق و مغرب</p></div>
<div class="m2"><p>نبرد هیچ کس از خلق روزگار گمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که شاه کیهان با صد هزار عالم جنگ</p></div>
<div class="m2"><p>علم زند بدو مه بر دو گوشهٔ‌ کیهان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز ملک روم به نزدیک مردمان عجم</p></div>
<div class="m2"><p>نوشته‌اند به تعجیل چند بازرگان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که چون به جانب موصل رسید شاهنشه</p></div>
<div class="m2"><p>به‌روم در ز نهیبش خروش بود و فغان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرفت قیصر روم و سپاه او از بیم</p></div>
<div class="m2"><p>ره‌گریز و هزیمت به‌آشکار و نهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همه‌کبود لب و زردروی و سرخ سرشک</p></div>
<div class="m2"><p>همه شکسته‌دل و تیره‌چشم و خشک‌دهان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زبیم آنکه شهنشاه بر سبیل شکار</p></div>
<div class="m2"><p>ز حدِّ شام بتابد به حدِّ روم عنان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر به غرب در از فتح شاه بود خبر</p></div>
<div class="m2"><p>کنون به شرق در از تیغ شاه هست نشان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رسید رایت مه پیکرش به جانب غرب</p></div>
<div class="m2"><p>زهیبتش نه اَمَل ماند خصم را نه امان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به تُرک تارَک فَغفورگشت خاک آلود</p></div>
<div class="m2"><p>به هند دیدهٔ چیپال گشت خون‌افشان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هزار ولوله و مشغله درافتادست</p></div>
<div class="m2"><p>ز تیغ شاه به هندوستان و ترکستان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شهی‌ که هیبت او را چنین بود تأثیر</p></div>
<div class="m2"><p>شهی‌ که دولت او را چنین بود بُرهان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مجاز باشد با او شکستن پیمان</p></div>
<div class="m2"><p>محال باشد با او نمودن عصیان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ایا شهی‌ که ز مریخ رنگ شمشیرت</p></div>
<div class="m2"><p>ز شرق و غرب رسیدست گرد بر کیوان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سپهر پرخطر از تیر توست بر صحرا</p></div>
<div class="m2"><p>ستاره برحذر از گوی توست در میدان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سپاه خصم توگر جاودان فرعونند</p></div>
<div class="m2"><p>تویی به دولت وتأیید موسی عمران</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کجا برهنه شود تیر تو برابر خصم</p></div>
<div class="m2"><p>فرو خورد همه نیرنگ خصم چون ثُعبان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو شادباش به مُلک اندرون که دشمن تو</p></div>
<div class="m2"><p>زبیم تو به جهان اندرون شدست جهان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زبهر سود به جز راه سرکشی نسپرد</p></div>
<div class="m2"><p>نکرد سود بر آن سرکشی وکرد زیان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زیادتیش به‌ملک اندرون همی بایست</p></div>
<div class="m2"><p>به آرزوی زیادت فتاد در نقصان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کنون زخوی بد خویشتن گرانبارست</p></div>
<div class="m2"><p>مثل زنندکه خوی بدست بار گران</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خدایگانا برخور زملک و دولت خویش</p></div>
<div class="m2"><p>به صد هزار قرون و به صد هزار قِران</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز شادمانی زن فال و شادمانه بزی</p></div>
<div class="m2"><p>زجاودانی کن یاد و جاودانه بمان</p></div></div>