---
title: >-
    شمارهٔ ۱۶۱
---
# شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>به فرخی و خوشی بر خدایگان بشر</p></div>
<div class="m2"><p>خجسته باد چنین عید و صدهزار دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جلال دولت و دولت بدو فزوده شرف</p></div>
<div class="m2"><p>جمال ملت و ملت بدو نموده هنر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهی‌ که بر همه روی زمین همی تابد</p></div>
<div class="m2"><p>ز ماه رایت او آفتاب فتح و ظفر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نبود تا که جهان است و هم نخواهد بود</p></div>
<div class="m2"><p>خدایگانی بر خلق از او مبارکتر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همی دهد قلم و تیغ او به بزم و به رزم</p></div>
<div class="m2"><p>نشان نعمت فردوس و هیبت محشر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به رزمگاه چو مریخ‌وار گیرد زور</p></div>
<div class="m2"><p>به بزمگاه چو خورشیدوار گیرد فر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زمین معصفر گردد ز بسکه راند خون</p></div>
<div class="m2"><p>هوا مُزَعْفر گردد ز بسکه بخشد زر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حسام شاه چو نیلوفر است و چهرهٔ خصم</p></div>
<div class="m2"><p>چو شَنْبلید شدست از نهیب نیلوفر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سرش ز چنبر فرمان شاه بیرون است</p></div>
<div class="m2"><p>قدش ز هیبت شاهی است چفته چون چنبر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر تنش به مثل سربه سر همه جگر است</p></div>
<div class="m2"><p>سرشک‌وار ببارد ز دیده خون جگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وگر همی نشناسد که وهم شاه جهان</p></div>
<div class="m2"><p>مؤثرست در آفاق چون قضا و قدر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همی به‌ کوه و کمر نازد و نه آگاه است</p></div>
<div class="m2"><p>که وهم شاه فرود آردش ز کوه و کمر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خدایگانا آ‌ن کس که نعمتش دادی</p></div>
<div class="m2"><p>به شرط خدمت یک‌ چند بسته بود کمر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو شد مخالف و بر نعمت تو شکر نکرد</p></div>
<div class="m2"><p>به نعمت تو که بدبخت‌ گشت و شومْ‌ اختر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شکسته کرد و پراکنده یک سیاست تو</p></div>
<div class="m2"><p>سپاه او را چون قوم عاد را صَرصَر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز فعل خویش بنازد همی و در مثل است</p></div>
<div class="m2"><p>کسی‌ که بد کند از بد هم او برد کیفر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مباد آن که خلاف تو دارد اندر دل</p></div>
<div class="m2"><p>که سوخته کندش خشم تو دل اندر بر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مخالفانی کاندر حصار خصم تواند</p></div>
<div class="m2"><p>خلاف و کین‌توزیشان ببرد سمع و بصر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز ترس خشم تو گشته است چشم ایشان ‌کور</p></div>
<div class="m2"><p>ز بیم کوس تو گشته است‌ گوش ایشان کر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بر آن حصار که ایشان مقام ساخته‌اند</p></div>
<div class="m2"><p>ز آب و خاک ندارند هیچگونه خبر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مگرکه صاعقه بارید چرخ بر سرشان</p></div>
<div class="m2"><p>که آب ایشان خون‌ گشت و خاک خاکستر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شدست خنجر برنده عقلشان در دل</p></div>
<div class="m2"><p>شدست آتش سوزنده مغزشان در سر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو حال ایشان در زیستن بر این جمله است</p></div>
<div class="m2"><p>چنان شناس‌ که ‌گشت آن حصار زیر و زبر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شهنشها ،ملکا، همچو آفتاب فلک</p></div>
<div class="m2"><p>به شرق و غرب تو را هست سال و ماه سفر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جهان شدست منور ز فر طلعت تو</p></div>
<div class="m2"><p>ز آفتاب منور شود جهان یکسر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به وقت راه سپردن همی وفا نکند</p></div>
<div class="m2"><p>به سیر مرکب تو بر سپهر سیر قمر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>حکایت و سَمَر امروز جمله باطل‌ گشت</p></div>
<div class="m2"><p>که رسمهای تو بیش از حکایت است و سمر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر قیاس کنم من ز دجله تا جیحون</p></div>
<div class="m2"><p>ز لشکر تو همی‌ نگسلد نفر ز نفر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خدایگان چو تو باید همی‌ که روز نبرد</p></div>
<div class="m2"><p>ز دجله تا لب جیحون ‌کشد صف لشکر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همیشه تا که همی بشکفد ز باد صبا</p></div>
<div class="m2"><p>به باغ در سمن تازه و بنفشهٔ تر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>یکی چو عارض خوبان سپید و روشن و پاک</p></div>
<div class="m2"><p>یکی چو زلف بتان برشکسته یک به دگر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شکفته باد ز عدل تو باغ شاهی و ملک</p></div>
<div class="m2"><p>چو بوستان ز نسیم بهار و قَطْرِ مَطَر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تو را زمانه غلام و ملوک خدمتکار</p></div>
<div class="m2"><p>تو را ستاره مطیع و سپهر فرمانبر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خجسته عید تو و پیش تو عدو قربان</p></div>
<div class="m2"><p>شب تو از شب و روزت ز روز خرم‌تر</p></div></div>