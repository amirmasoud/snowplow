---
title: >-
    شمارهٔ ۲۶۰
---
# شمارهٔ ۲۶۰

<div class="b" id="bn1"><div class="m1"><p>پیام دادم نزدیک آن بت کشمیر</p></div>
<div class="m2"><p>که زیر حلقهٔ زلفت دلم چراست اسیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جواب داد که دیوانه شد دل تو ز عشق</p></div>
<div class="m2"><p>به ره نیارد دیوانه را مگر زنجیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیام دادم کز بهر چیست گرد رخت</p></div>
<div class="m2"><p>ز مشک و غالیه خطی کشیده حلقه پذیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جواب داد که خط من آیتی عجب است</p></div>
<div class="m2"><p>که هیچکس به جهان در نداندش تفسیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیام دادم کان عارض چو شیر سپید</p></div>
<div class="m2"><p>رها مکن‌که شود سربه سر سیاه چو قیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جواب داد که‌ گر شیر من چو قیر شود</p></div>
<div class="m2"><p>روا بود چو همه قیر تو شدست چو شیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیام دادم کز روی زرد و نالهٔ زار</p></div>
<div class="m2"><p>به زر و زیر همی مانم ای بت‌کشمیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جواب داد که از زیر و زر بود شاهی</p></div>
<div class="m2"><p>چرا غم است توراگر چو زر شدی و چو زیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیام دادم کز عشق تو رخ و تن من</p></div>
<div class="m2"><p>چرا زریر وکمان شدکه بود لاله و تیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جواب داد که در عشق چون تو بسیارند</p></div>
<div class="m2"><p>ز تیرکرده کمان و ز لاله کرده زریر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پیام دادم کامد به دست تو دل من</p></div>
<div class="m2"><p>به دل بسنده کن و جان من شکار مگیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جواب داد که جان و دلت به دست من است</p></div>
<div class="m2"><p>چو شرق و غرب به فرمان شاه و حکم وزیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پیام دادم کاو را غیاث ملت خوان</p></div>
<div class="m2"><p>که عدل اوست بشر را بزرگوار بشیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جواب داد که او را وزیر عادل گوی</p></div>
<div class="m2"><p>که چشم دولت عالی بدو شدست بصیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پیام دادم کاندر جهان نظیرش کیست</p></div>
<div class="m2"><p>به ‌دین و دولت و فرهنگ و دانش و تدبیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جواب داد که او را نظیر نشناسم</p></div>
<div class="m2"><p>ز بهر آنکه خدایش نیافرید نظیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پیام دادم کز قدر او به حکم قیاس</p></div>
<div class="m2"><p>چه پایه فرق کنم تا به آفتاب منیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جواب داد که بسیار فرق باید کرد</p></div>
<div class="m2"><p>که قدر خواجه عظیم‌است و آفتاب حقیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پیام دادم کز دولتش عجب دارم</p></div>
<div class="m2"><p>که قادرست به تأثیر همچو چرخِ اثیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جواب داد که این دولت جهان‌آرای</p></div>
<div class="m2"><p>زیادت است ز چرخ اثیر در تأثیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پیام دادم کز منتش گرانبارند</p></div>
<div class="m2"><p>رعیت و سپهِ شهریار‌ِ کشور گیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جواب داد که در زیر بار منت او</p></div>
<div class="m2"><p>هزار خواجه فزون است و صد هزار امیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پیام دادم کز دست و طبع او خیزد</p></div>
<div class="m2"><p>نسیم باد صبا و سرشک ابر مَطیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جواب داد که ابر مطیر و باد صبا</p></div>
<div class="m2"><p>همی خورند زدست و زطبع او تشویر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پیام دادم کز عدل اوست ناپیدا</p></div>
<div class="m2"><p>چو آب حیوان در دهر فتنه و تزویر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جواب داد که از جود اوست ناموجود</p></div>
<div class="m2"><p>چو کیمیا و چو سیمرغ در زمانه فقیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پیام دادم کازادگان دنیا را</p></div>
<div class="m2"><p>به جای روزی توقیع کلک اوست مشیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جواب داد که هرچ آن مسیح کردی دم</p></div>
<div class="m2"><p>همی کند گه توقیع کلک او به ضریر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پیام دادم کز دشمان دولت او</p></div>
<div class="m2"><p>شدست بخت نفور و همی‌ کنند نفیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>جواب داد که بر روزنامهٔ ملکان</p></div>
<div class="m2"><p>نبشت‌ گردون ما‌یَملِکون مِن‌ ‌قِطمیر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پیام دادم کاندر ضمیر و فکرت او</p></div>
<div class="m2"><p>جواهر خِرَدست و نوادرِ تقدیر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>جواب داد که معلوم کرد عالم را</p></div>
<div class="m2"><p>ملک به فکرت و تقدیر ایزدی به ضمیر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پیام دادم کز بخشش خدای کریم</p></div>
<div class="m2"><p>نرفت و هم نرود در رضای او تأ‌خیر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جواب داد که از گردش سپهر بلند</p></div>
<div class="m2"><p>نرفت و هم نرود در مراد او تقصیر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پیام دادم کاقبال بی‌پرستش او</p></div>
<div class="m2"><p>بود به نزد خردمند خواب بی‌تعبیر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جواب داد که اشعار بی‌ستایش او</p></div>
<div class="m2"><p>بود به نزد سخندان نماز بی‌تکبیر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>پیام دادم کز طبع من‌ گهر خیزد</p></div>
<div class="m2"><p>کجا کند قلم من مدیح او تحریر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>جواب داد که از طبع تو گهر نه عجب</p></div>
<div class="m2"><p>که هست طبع تو درمدح او چو بحر غزیر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>پیام دادم کز خدمتش قرار دل است</p></div>
<div class="m2"><p>همیشه باد بدو چشم روزگار قریر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>جواب داد که تا سعد و نصرت از فلک است</p></div>
<div class="m2"><p>فلک مساعد او باد و روزگار نصیر</p></div></div>