---
title: >-
    شمارهٔ ۱۸۶
---
# شمارهٔ ۱۸۶

<div class="b" id="bn1"><div class="m1"><p>چون شمردم در سفر یک نیمه از ماه صفر</p></div>
<div class="m2"><p>ساختم ساز رحیل و توشهٔ راه سفر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکجا دولت نهد راه سفر در پیش من</p></div>
<div class="m2"><p>کی دهد چندان زمان تا بگذرد ماه صفر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون سپهر از ماه تابان کرد زرین آیتی</p></div>
<div class="m2"><p>راه من معلوم‌کرد آن ماه روی سیمبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یافت آگاهی که من ساز سفر سازم همی</p></div>
<div class="m2"><p>دشت و بر یک چند بگزینم همی بر شهر و در</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آمد از خانه دوان چون آهوی خسته نوان</p></div>
<div class="m2"><p>لب ز خشکی چون بیابان دیدگان همچون شمر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کوه غم بر دل نهاده جون‌کمرکرده میان</p></div>
<div class="m2"><p>تا چرا من رنج خواهم دید در کوه و کمر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فندق او بر شقایق‌کرده سنبل ساخ شاخ</p></div>
<div class="m2"><p>نرگس او بر سمن باریده مروارید تر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از تاسف چنبری‌کرده قد چون سرو راست</p></div>
<div class="m2"><p>وز تپانچه نیلگون ‌کرده رخ چون معصفر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت بشکستی دلم تا عزم را کردی درست</p></div>
<div class="m2"><p>با صبا پیوستن و منزل بریدن چون قمر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جامه و پیرایه ساز از بهر من ‌گر عاشقی</p></div>
<div class="m2"><p>زین و پالان را ا‌فرو هل‌ا از پی‌ اسب و ستر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جند بازی بر بساط آرزو نرد امید</p></div>
<div class="m2"><p>چند کاری در زمین ‌کاشکی تخم اگر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>موی سیمین چون‌ کند مرد حکیم از بهر سیم</p></div>
<div class="m2"><p>روی زرین چون‌ کند شخص عزیز از بهر زر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گه چو میشان باشی از دندان ‌گرگان با نهیب</p></div>
<div class="m2"><p>گه چو گوران باشی از چنگال شیران با خطر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گوش بر هر سو نهاده تا چه پیش آرد قضا</p></div>
<div class="m2"><p>چشم بر هر ره نهاده تا چه فرماید قدر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفتم ای ماه شکرلب آب چشم تو مرا</p></div>
<div class="m2"><p>کرد در حسرت گدازان چون به آب اندر شکر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا ستوران را نسازم در سفر پالان و زین</p></div>
<div class="m2"><p>جامه و پیرایه چون سازم بتان را در حضر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر به شست اندر شوم چون ماهیان بی‌هوش و هنگ</p></div>
<div class="m2"><p>ور به دام اندر شوم جون آهوان بی‌خواب و خور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از پی سیمم نباشد هیچ سودا در دماغ</p></div>
<div class="m2"><p>وز غم‌ زرم نباشد هیچ صفرا در جگر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از خطرکردن بزرگی و خطر جویم همی</p></div>
<div class="m2"><p>این مثل نشنید‌ه‌‌ای کاندر خطر باشد خطر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کم بها باشد به بحر خویش دُرّ شاهوار</p></div>
<div class="m2"><p>کم خطر باشد به شهر خویش مرد نامور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در مکان وکان خویش از خواری و بی‌قیمتی</p></div>
<div class="m2"><p>عود باشد چون حطب یاقوت باشد چون حجر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دولتم گوید همی کز شهر بیرون شو به راه</p></div>
<div class="m2"><p>اخترم ‌گوید همی‌ کز خانه بیرون شو به ‌در</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هرکه رنجی بیند آخر کار او گردد به کام</p></div>
<div class="m2"><p>هر که تخمی ‌کارد آخر کشت او آید به بر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بر مده خرمن به باد و آتش اندر من مزن</p></div>
<div class="m2"><p>خاک بر تارک مبیز و آب روی من مبر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دوستان و عاشقان را از پس هجرانِ صَعب‌</p></div>
<div class="m2"><p>چرخ ‌گردان شاد گرداند ز وصل یکدگر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تنگم اندر برگرفت و در دل من برفروخت</p></div>
<div class="m2"><p>آتشی‌ کز شب دخانش بود و از پروین شرر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تیره شب دیدم چو شاه زنگ با خیل و حشم</p></div>
<div class="m2"><p>افسری بر سر ز مینا و اندر آن افسر درر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اختران دیدم چو سیماب و فلک چون آینه</p></div>
<div class="m2"><p>سر برآورده دو دیو از خاور و از باختر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آن یکی بر راندن سیماب بگشاده دو دست</p></div>
<div class="m2"><p>و آن دگر بر خوردن سیماب بگشاده زفر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خیره ماندم زان ‌دو دیو مشرقی و مغربی</p></div>
<div class="m2"><p>مشرقی سیماب زای و مغربی سیماب خور</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پیش من راهی دراز آهنگ و منزلهای دور</p></div>
<div class="m2"><p>سنگ او بیرون ز حد فرسنگ او بیرون ز مرّ</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خامهٔ ریگ اندرو جون موج جیحون و فرات</p></div>
<div class="m2"><p>تودهٔ سنگ اندرو چون ا‌بحرا الان و خزر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در میان بیشهٔ او خوابگاه اژدها</p></div>
<div class="m2"><p>در کنار چشمهٔ او آهوان را آبخور</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چون چَهِ دوزخ به تاریکی نشیبش را امسیرا</p></div>
<div class="m2"><p>چون پل محشر ز تاریکی فرازش را ممر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گاه بودم در نشیبش همبر ماهی و گاو</p></div>
<div class="m2"><p>گاه بودم بر فرازش همنشین ماه و خور</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همبرم چون موج دریا مرکب هامون‌ گذار</p></div>
<div class="m2"><p>رهبرم چون سیل وادی بارهٔ وادی سپر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تیزچشمی‌ کز غبارش چشم کیوان گشت کور</p></div>
<div class="m2"><p>خردگوشی کز صُهیلش گوش گردون گشت کر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>طبع او بشناخت مقصد را همی پیش از ضمیر</p></div>
<div class="m2"><p>گام او دریافت منزل را همی پیش از بصر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چون سبک پا و زمین پیما شود در زیر ران</p></div>
<div class="m2"><p>آن ‌شبه ‌رنک‌ تگاور هم‌ محجّل هم ‌اَغر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>راست‌ گفتی شب مجسم گشت و جان دادش خدای</p></div>
<div class="m2"><p>بر جبین و دست و پای او پدید آمد سحر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ماه و ابر و برق و مرغ و باد و تیر و وهم را</p></div>
<div class="m2"><p>گر ز مخلوقات دیگر زودتر باشد گذر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>او بدین هر هفت در رفتن همی پیشی‌ گرفت</p></div>
<div class="m2"><p>از نشاط و حرص درگاه وزیر دادگر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>صاحب عادل قوام‌الملک صدرالدین که هست</p></div>
<div class="m2"><p>صد جهان کامل اندر یک جهان مختصر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>آن‌ که هست او را حیا و علم عثمان و علی</p></div>
<div class="m2"><p>آن که هست او را وقار و عدل بوبکر و عمر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>صبح اقبالش دمیدست از ختن تا قیروان</p></div>
<div class="m2"><p>باد افضالش رسیدست از یمن تا کاشغر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مدغم اندر مهر و کینش اتفاق سعد و نحس</p></div>
<div class="m2"><p>مضمر اندر صلح و جنگش اتصال خیر و شر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>زیر منشور قبول و رد او ناز و نیاز</p></div>
<div class="m2"><p>زیر توقیع رضا و خشم او نفع و ضرر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سر بر آر و چشم بگشا ای نظام دین حق</p></div>
<div class="m2"><p>تا بینی در خراسان حشمت و جاه پسر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>حشمت و جاهش نه تنها در خراسان است و بس</p></div>
<div class="m2"><p>بل‌که مشهور است در اطراف عالم سربه‌سر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>آسمان خاطر او ازگهر تابد نجوم</p></div>
<div class="m2"><p>بحر جود او ز دینار و درم بارد مطر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گر شگفت است اینکه دینار و درم بارد ز ابر</p></div>
<div class="m2"><p>این شگفتی تر بسی‌ کز آسمان تابد گهر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>خنجر ترکان او را بر ظفر باشد مدار</p></div>
<div class="m2"><p>تا مدار اختر و افلاک باشد بر مدر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گر عجب نبود که‌گردد چنبری گرد نجوم</p></div>
<div class="m2"><p>کی عجب باشد که‌ گردد خنجری‌ گرد ظفر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>فرق اعدا بسپرد چون زیر پا آرد رکاب</p></div>
<div class="m2"><p>پشت خصمان بشکند چون پیش رو آرد سپر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>معجز نصرت نماید هرکجا پوشد زره</p></div>
<div class="m2"><p>مشکل دولت گشاید هرکجا بندد کمر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بر هوا از حشمت آن شاه مرغان شد عقاب</p></div>
<div class="m2"><p>بر زمین از فخر این شاه ددان شد شیر نر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>این یکی افکند ناخن تاکند تعویذ ا‌خصم</p></div>
<div class="m2"><p>وان یکی بگرفت‌ گیسو بر مثال زال زر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>آن به‌گاه بردباری چیره بر صلح و صواب</p></div>
<div class="m2"><p>وین به روز کامکاری آگه از حزم و حذر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>صورت عفو تو دارد روی رضوان در جنان</p></div>
<div class="m2"><p>پیکر شخص تو دارد شخص مالک در سقر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>هرکه او معبود را بر عرش ‌گوید مستوی</p></div>
<div class="m2"><p>مذهب او ارا به برهان سربه‌سر یکسر بخر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>شد به تایید تو عالی نسل اسحاق و علی</p></div>
<div class="m2"><p>چون به‌تایید پیمبر نسل عدنان و مُضَر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>هست فضل و حق تو بر دودمان فخر ملک</p></div>
<div class="m2"><p>همچو فضل روستم بر دودمان زال زر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>کدخدایی بر تو زیبد پادشاهی بر ملک</p></div>
<div class="m2"><p>کین دو منصب هر دو را بودست در اصل و گهر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ذکر اوصاف شما و حسن الطاف شما</p></div>
<div class="m2"><p>گشت باقی تا قیامت در تواریخ و سیر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>آفرین بر کلک لؤلؤ بار مشک افشان تو</p></div>
<div class="m2"><p>کز خرد دارد نشان و از هنر دارد اثر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>نکته‌های او همه نورست در چشم خرد</p></div>
<div class="m2"><p>لفظهای او همه خال است بر روی هنر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>شکل هر حرفی‌ که بنگارد بدیع و نادرست</p></div>
<div class="m2"><p>چون طراز و نقش بر دیبای روم و شوشتر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>هست بر مد صریر او مدار ملک شاه</p></div>
<div class="m2"><p>چون مدار شرع پیغمبر بر اخبار و سور</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>اندر آن ساعت ‌که ارواح از صور گردد جدا</p></div>
<div class="m2"><p>از صریر او به ارواح اندر آویزد صور</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ای ز تو جَدّ و پدر خشنود چون دانی روا</p></div>
<div class="m2"><p>کز تو ناخشنود باشد مادح جد و پدر</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>آن‌که ‌در میدان نظم او را چنین باشد مجال</p></div>
<div class="m2"><p>کی‌کند واجب که در بیغوله‌ای سازد مقر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>اندرین یک سال نگشادی به نام او زبان</p></div>
<div class="m2"><p>در حدیث او نبستی یک زمان وهم و فکر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>گر قلم در دست تو حرفی نوشتی سوی او</p></div>
<div class="m2"><p>از نشاط آن قلم همچون قلم رفتی به‌ سر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>گشت عریان باز او چون ‌در حضر برگی نداشت</p></div>
<div class="m2"><p>حاضر آمد پیش از آن کز برگ عریان شد شجر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>تا ز باران قبول و آفتاب رای تو</p></div>
<div class="m2"><p>بر درخت دولت و عمرش پدید آید ثمر</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>از تو باید یک نظر تا با فلک‌ گوید سخن</p></div>
<div class="m2"><p>وز تو باید یک سخن تا از فلک یابد نظر</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>گه در درج درربگشاید اندرمدح شاه</p></div>
<div class="m2"><p>گاه در شکر تو بگشاید در درج غرر</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بازگردد شادمان از شهر مرو شا‌هجان</p></div>
<div class="m2"><p>مدح شه‌ گفته به جان‌ و شکر او کرده ز بر</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>تا شناسد حال هفت اقلیم ا‌راا بر درگهت</p></div>
<div class="m2"><p>با پرستش صدگروه و با ستایش صد نفر</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>باش تو پیوسته با فر خداوند جهان</p></div>
<div class="m2"><p>در جهانداری تو آصف رای و او جمبثبیذ فر</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>هر دو را دایم خطاب ازگنبد فیروزه‌گون</p></div>
<div class="m2"><p>صاحب پیروزبخت و خسرو پیروزگر</p></div></div>