---
title: >-
    شمارهٔ ۲۵۹
---
# شمارهٔ ۲۵۹

<div class="b" id="bn1"><div class="m1"><p>کنون‌ که خور به ترازو رسید و آمد تیر</p></div>
<div class="m2"><p>شدند راست شب و روز چون ترازو و تیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به‌ کوه سونش سیم و به باغ آبی و سیب</p></div>
<div class="m2"><p>مگر که سیمگر و زرگرند لشکر تیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر که باد خزان صیقل است کز عملش</p></div>
<div class="m2"><p>چو روی آینه روشن شدست روی غدیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر که عاشقِ زارند لعبتان چمن</p></div>
<div class="m2"><p>که پشتشان چو کمان است و رویشان چو زریر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز فربهی شد و زینت به سان رَبع وطَلَل</p></div>
<div class="m2"><p>هر آن صنم که در آن خانه بود چون تصویر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گمان برم که گلستان گناه آدم کرد</p></div>
<div class="m2"><p>که شد برهنه چو آدم ز جامه‌های حریر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به تاکهای رزان بر ببین که دست خزان</p></div>
<div class="m2"><p>هزار خوشهٔ لؤلؤ فزوده است به قیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد از سفیدی و سرخی بدیع گونهٔ سیب</p></div>
<div class="m2"><p>چو رنگ و روی بتی کز جفا خورد تشویر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به‌صورت و صفت آبی چو گوی زرّین است</p></div>
<div class="m2"><p>برو نشسته ز میدان شاه‌ گرد عبیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کفیده نار و در او دانه‌های سرخ پدید</p></div>
<div class="m2"><p>چو روز رزم دهان مخالفان وزیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قوام دین رضی مقتدی اتابک شاه</p></div>
<div class="m2"><p>نظام ملک حسن سید صغیر و کبیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بزرگوار وزیری که از سلامت و امن</p></div>
<div class="m2"><p>غنی شدست به تدبیر او جهان فقیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>میان غیب و میان ضمیر روشن او</p></div>
<div class="m2"><p>ستاره واسطه گشته است و آفتاب سفیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو گردش فلک است امر او که عالم را</p></div>
<div class="m2"><p>دهد جوانی و پیری و خود نگردد پیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مسیح اگر به دعا جان رفته باز آورد</p></div>
<div class="m2"><p>همان کند گه توقیع کلک او به صریر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نه راستی قلم او به تیر ماند راست</p></div>
<div class="m2"><p>همی به چرخ بر از تیر او ببارد تیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رسی ز قلت ‌شکر ازکَفَش به‌کثرت مال</p></div>
<div class="m2"><p>که او دهدت به شُکر قلیل مال کثیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کسی‌که پشت‌کند پیش تیر او چوکمان</p></div>
<div class="m2"><p>سعادت ابد از تیر چرخ یابد تیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نه سنگ زر کند اقبال او چرا نکنند</p></div>
<div class="m2"><p>ز خاک درگه او کیمیاگران اکسیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنان نماید بحر عریض پیش دلش</p></div>
<div class="m2"><p>که آبگیر نماید به پیش بحر غزیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>موافقش ز سعیر ایمن است و این نه عجب</p></div>
<div class="m2"><p>ز بهر آنکه حرام است بر سعید سعیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو نام او نبود ناتمام باشد مدح</p></div>
<div class="m2"><p>که مدح همچو نمازست و نام او تکبیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چرا به قول منجم مؤثرست سپهر</p></div>
<div class="m2"><p>که در سپهر کند دولتش همی تأثیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زمین ز دولت او دید صد هزار اثر</p></div>
<div class="m2"><p>به‌ زیر هر اثری صد هزار چرخ اثیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز بهر مژدهٔ فتح و بشارت ظفرش</p></div>
<div class="m2"><p>همیشه رنجه بود پای پیک و دست دبیر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گهی ز شرق فرستد به سوی غرب رسول</p></div>
<div class="m2"><p>گهی ز غرب فرستد به سوی شرق‌ سفیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو هست نصرت سنت مراد او شب و روز</p></div>
<div class="m2"><p>خدای هست مر او را بهر مراد نصیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ایا علوم تو اسباب عقل را معنی</p></div>
<div class="m2"><p>و یا رسوم تو آیات عدل را تفسیر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز اعتقاد توگر نسختی برند به چین</p></div>
<div class="m2"><p>شوند مانویان دین‌ پرست و شرع‌ پذیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>وگر پیام تو در خواب بشنود قیصر</p></div>
<div class="m2"><p>ز جاثلیق جز اسلام نشنود تعبیر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مرادهای تو گویی به برج تقدیرست</p></div>
<div class="m2"><p>کز آن بروج درآید کواکب تقدیر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هرآنچه رای تو بگزیندش گزیده بود</p></div>
<div class="m2"><p>که رای پاک تو در ملک ناقدی است بصیر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مخالف تو چو زیرست و زیر زخم قضا</p></div>
<div class="m2"><p>عجب نباشد اگر زیر زخم باشد زیر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همی سبق برد از روزگار مدت تو</p></div>
<div class="m2"><p>که مدت تو طویل است و روزگار قصیر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به فر بخت تو دراج زیر چنگل باز</p></div>
<div class="m2"><p>برون‌کند ز نشیمن عقاب را به صفیر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>وگر بود به‌کف شیر بچهٔ روباه</p></div>
<div class="m2"><p>جو بوی عدل تو بیند ز شیر خواهد شیر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همیشه خلق جهان را تویی به عجز مشار</p></div>
<div class="m2"><p>چنانکه شاه جهان را تویی به خیر مشیر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز بهر آنکه مکان محبت تو دل است</p></div>
<div class="m2"><p>ز عضوهای دگر بر تن او شدست امیر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چنان ندید جهان در جلالت و تعظیم</p></div>
<div class="m2"><p>چنین نزاد فلک در کفایت و تدبیر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ضمیر و وهم شما را چگونه وصف‌کنند</p></div>
<div class="m2"><p>که برگذشت ثنای شما ز وهم و ضمیر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شرف گرفت به تو نامه و دوات و قلم</p></div>
<div class="m2"><p>چنان‌ کجا به شهنشه حُسام و تاج و سریر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>حسام درکف شاه و قلم به دست تو در</p></div>
<div class="m2"><p>دو معجزند ولایت‌گشای وکشورگیر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>درست شد که ز اهل حسام و اهل قلم</p></div>
<div class="m2"><p>تورا و او را ایزد نیافرید نظیر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ثناگران نتوانند کرد وصف شما</p></div>
<div class="m2"><p>اگر به طبع فرزدق بوند و لفظ جریر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نکرد بنده معزی و هم نخواهد کرد</p></div>
<div class="m2"><p>به شکر هر دو خداوند یک زمان تقصیر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ولیکن ار همهٔ عمر شکر هر دو کند</p></div>
<div class="m2"><p>چو بشمرند بود صد یکی ز عُشْرِ عشیر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>همیشه تا که همی لحظه‌ای نیاساید</p></div>
<div class="m2"><p>هم اسمان زمدار و هم اختران ز مسیر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ضمیر و خاطر و رای جهان‌فروز تو باد</p></div>
<div class="m2"><p>بر آسمان وزارت چو اختران منیر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>دل زمانه به فرمان توگرفته قرار</p></div>
<div class="m2"><p>دو چشم ملک به پیروزی تو گشته قریر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تو صدر عالم و در صدر دین و دولت و داد</p></div>
<div class="m2"><p>فزوده قدر تو تقدیر روزگار قدیر</p></div></div>