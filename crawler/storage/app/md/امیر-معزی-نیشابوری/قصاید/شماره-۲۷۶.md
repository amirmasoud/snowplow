---
title: >-
    شمارهٔ ۲۷۶
---
# شمارهٔ ۲۷۶

<div class="b" id="bn1"><div class="m1"><p>خدایگان جهانی و شاه با فرهنگ</p></div>
<div class="m2"><p>به عدل چون عمری و به هوش چون هوشنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه‌ای بهار و بهاری چو کرد خواهی بزم</p></div>
<div class="m2"><p>نه‌ای هژیر و هژیری چو کرد خواهی چنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خدنگ فخرکند بر درخت صندل و عود</p></div>
<div class="m2"><p>از آن قبل ‌که بود تیر تو ز چوب خدنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پلنگ، کِبر کند سال و ماه بر دد و دام</p></div>
<div class="m2"><p>از آن قبل ‌که جناغت بود ز چرم پلنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حسام تو ز تن دشمنان رباید جان</p></div>
<div class="m2"><p>پیام تو ز دل دوستان زداید زنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرآنگهی‌که تو آهنگ تیغ تیزکنی</p></div>
<div class="m2"><p>اجل به جان بداندیش تو کند آهنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شهنشها ملکا خسروا خداوندا</p></div>
<div class="m2"><p>تویی نتیجهٔ اقبال و مایهٔ فرهنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درخت و باغ تو گردد میان مجلس تو</p></div>
<div class="m2"><p>جو نوبهار به بوی و چو آفتاب به رنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز بس بدایع نقش و نگار گوناگون</p></div>
<div class="m2"><p>بهار خانهٔ چین است و صورت ارژنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدین درخت و بدین باغ شادمانی کن</p></div>
<div class="m2"><p>همی شنو به‌سعادت خروش بربط و چنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فرشتگان خدا از فلک همی‌گویند</p></div>
<div class="m2"><p>خجسته باد تو را میهمانی سرهنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همیشه باد تو را در سرور بزم شتاب</p></div>
<div class="m2"><p>همیبثبه باد تو را بر سریر ملک درنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چنین و بهتر از این باش با هزاران سال</p></div>
<div class="m2"><p>جهان‌ گشاده به تیغ و قدح‌ گرفته به‌ چنگ</p></div></div>