---
title: >-
    شمارهٔ ۳۶۹
---
# شمارهٔ ۳۶۹

<div class="b" id="bn1"><div class="m1"><p>خطّ است ‌گرد عارض آن ماه دلستان</p></div>
<div class="m2"><p>یا سنبل است ریخته بر طرف ‌گلستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا عنبرست حلّ شده بربرگ نسترن</p></div>
<div class="m2"><p>یا مورچه است صف‌زده برگرد ارغوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از کوچکی ‌که هست مر آن ماه را دهن</p></div>
<div class="m2"><p>از لاغری‌که هست مر آن ماه را میان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون در میان و در دهن او نگه‌ کنی</p></div>
<div class="m2"><p>گوئی میان او کمرست و کمر دهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در پرنیانش آهن و در مشک آتش است</p></div>
<div class="m2"><p>این هر چهار سخت بدیع است و دلستان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرگز بدین صفت نشنیدم مُشَعبَدی</p></div>
<div class="m2"><p>کاهن به مشک پوشد و آتش به‌پرنیان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من دارم از عقیق به جَزع اندرون اثر</p></div>
<div class="m2"><p>واو دارد از شکر به عقیق اندرون نشان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا دور گشت قامت چون تیر او زمن</p></div>
<div class="m2"><p>پشتم شدست در طلبش چفته چون‌ کمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جز دیدهٔ من و لب او در جهان‌ که دید</p></div>
<div class="m2"><p>جزع عقیق بار و عقیق‌ گهرفشان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرگز کمان روان ز پس تیر کس ندید</p></div>
<div class="m2"><p>چون شدکمان من ز پس تیر او روان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر شب‌ که دست در علم وصل او زنم</p></div>
<div class="m2"><p>خورشید برکشد عَلَم صبح در زمان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وان شب که قصه‌های فراقش کنم به‌ شعر</p></div>
<div class="m2"><p>گمره شوند فَرْ‌قد و شِعْر‌ی به آسمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تاکی نهم به دل بر از اندوه عشق داغ</p></div>
<div class="m2"><p>تاکی‌کنم ز هجر بتان ناله و فغان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جان پرورم به‌دوستی و مدح صاحبی</p></div>
<div class="m2"><p>کاو هست یرورندهٔ ملک خدایگان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فرخنده مجد ملک و پسندیده شمس دین</p></div>
<div class="m2"><p>تاج تبار و واسطهٔ عقد دودمان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بوالفضل کز فضایل و اقبال نام او</p></div>
<div class="m2"><p>منسوخ‌ کرد نام بزرگان باستان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدری که شد به طلعتش افروخته زمین</p></div>
<div class="m2"><p>صدری‌ که شد به همتش آراسته زمان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جان است شکر او که بود آشنای عقل</p></div>
<div class="m2"><p>عقل است مهر او که بود رهنمای جان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گویی‌ کفایت و هنرش وهم و فکر توست</p></div>
<div class="m2"><p>کان هست بی‌نهایت و این هست بی‌کران</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گویی مناقبش صفت ذ‌ات صانع است</p></div>
<div class="m2"><p>کاندر چگونگیش همی گم شود گمان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تخمی شناس خدمت او در زمین بخت</p></div>
<div class="m2"><p>کان تخم بردهد به همه وقت نام و نان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سود و زیان و سَعْد و نحوست به هم دهند</p></div>
<div class="m2"><p>افلاک در تحرک و اجرام در قران</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آهنگ سوی خدمت او کن که خدمتش</p></div>
<div class="m2"><p>سعدی است بی‌نحوست و سودی است بی‌زیان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دارد به زیر کلک در از عقل و از هنر</p></div>
<div class="m2"><p>گنج و سپاه و مملکت صاحب القران</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بی‌ عقل کامل و هنر وافر ای عجب</p></div>
<div class="m2"><p>بر کام خویشتن نتوان گشت کامران</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نه شغل روزگار توان ساخت بر گزاف</p></div>
<div class="m2"><p>نه کلک شهریار توان داست رایگان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای‌ گوش غایبان ز کمال تو پر خبر</p></div>
<div class="m2"><p>وی چشم حاضران زجمال تو پر عیان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>امر تو چون قضاست رسیده بهر مکین</p></div>
<div class="m2"><p>نام تو چون هواست رسیده بهر مکان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کوهی‌گران زعزم تو کاهی شود سبک</p></div>
<div class="m2"><p>کاهی سبک ز حزم تو کوهی شود گران</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>فضل کفات را به قلم نقد کرده‌ای</p></div>
<div class="m2"><p>وارزاق خلق را به قلم کرده‌ای ضمان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>یک تن که دید ناقد فضل همه‌ کفات</p></div>
<div class="m2"><p>یک تن که دید رازق رزق همه جهان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از قُوَّت عبارت و تهذیب لفظ توست</p></div>
<div class="m2"><p>اندر لغت فصاحت و اندر نُکَت بیان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اعجاز و سِحْر وصف بیان و بنان توست</p></div>
<div class="m2"><p>هم معجزالبیانی و هم ساحرالبنان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کلک مُشَعْبد تو چراغی است نوربخش</p></div>
<div class="m2"><p>ازکاشغر زبانه زده تا به قیروان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ملک از دخان او همه ساله مُنَقّش است</p></div>
<div class="m2"><p>و او خود منقش است همه ساله از دخان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ماند به خیزران و به قدرت چو خنجرست</p></div>
<div class="m2"><p>هرگز که دید قدرت خنجر ز خیزران</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بینای اَ‌کْمَه است و سخنگوی اَ‌بْکَم است</p></div>
<div class="m2"><p>دانای بی‌دل است و توانای ناتوان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مرغی است اوکه درّو شبه پرکشد به هم</p></div>
<div class="m2"><p>چون سوی صحن باغ‌ گراید ز آشیان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در زیر هر صفیرش درّی است شاهوار</p></div>
<div class="m2"><p>در زیر هر صریرش گنجی است شایگان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>او هست درکف تو و تاثیر نقش اوست</p></div>
<div class="m2"><p>در قصرهای قیصر و در خانه‌های خان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ای آشکار پیش دلت هر چه کردگار</p></div>
<div class="m2"><p>آرد همی به پردهٔ غیب اندرون نهان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دست تورا به ابر که داند قیاس کرد</p></div>
<div class="m2"><p>تا بدره بدره این دهد و قطره قطره آن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>در حشر اگر به دست تو باشد شمار خلق</p></div>
<div class="m2"><p>بر هیج خلق بسته نماند در جنان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هرچند پادشاه تن آدمی است دل</p></div>
<div class="m2"><p>از بهر آفرین تو شد بندهٔ زبان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گویی‌که مدح تو سبب عز و شادی است</p></div>
<div class="m2"><p>زیرا که مادح تو عزیزست و شادمان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گرگنجهای مدح تو مخزون‌ کند قضا</p></div>
<div class="m2"><p>گردونش قلعه باید و خورشید پاسبان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>رسته است از امتحان فلک طبع من رهی</p></div>
<div class="m2"><p>تا کرده‌ام به طبع مدیح تو امتحان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>خواهم همی ز بهر ثنای و لقای تو</p></div>
<div class="m2"><p>در دیده روشنایی و درکالبد روان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>در مدح تو به وصف‌کمال است شعر من</p></div>
<div class="m2"><p>خاصه به رسم تهنیت ‌جشن مهرگان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>جشنی عجب‌ که در چمن و بوستان همی</p></div>
<div class="m2"><p>بر لشکر بهار زند لشکر خزان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گویی مگر درخت یکی مرد راهب است</p></div>
<div class="m2"><p>بر دوش او فکنده یهودانه طیلسان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>زنگارگون لباس درختان جویبار</p></div>
<div class="m2"><p>گویی فرو زدند به‌زنگار زعفران</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بیماری است و عشق رخ زرد را سبب</p></div>
<div class="m2"><p>بیمار و عاشق اندر مگر باغ و بوستان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>گر طبع باغ پیر و کهن‌ گشت باک نیست</p></div>
<div class="m2"><p>طبع تو تازه باد و تن و بخت نوجوان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تا در میان دشمن و اندر میان دوست</p></div>
<div class="m2"><p>ازکین بود حکایت و از مهر داستان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بر دشمنانت نحس زحل باد کینه‌ورز</p></div>
<div class="m2"><p>بر دوستانت سعد فلک باد مهربان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>گنج طرب همیشه تو را باد زیردست</p></div>
<div class="m2"><p>اسب ظفر همیشه تو را باد زیرران</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>روشن به طلعت شه افاق چشم تو</p></div>
<div class="m2"><p>روشن به نور طلعت تو چشم خاندان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>جاه و قبول و حشمت تو هر سه پایدار</p></div>
<div class="m2"><p>عز و بقا و دولت تو هر سه جاودان</p></div></div>