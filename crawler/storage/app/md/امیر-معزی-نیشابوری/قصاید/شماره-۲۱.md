---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>تا رای بود نصرت دین ناصر دین را</p></div>
<div class="m2"><p>در نصرت او رای بود روح امین را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا پادشه روی زمین باشد سنجر</p></div>
<div class="m2"><p>بر هفت فلک فخر بود روی زمین را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاهی که به ماهی به سپاهی بگشاید</p></div>
<div class="m2"><p>صد شهرگرانمایه و صدحص‌ه حصین را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با خصم برابر زند اندر صف پیکار</p></div>
<div class="m2"><p>بی‌ آنکه کند چاره شبیخون و کمین را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون نیزه زند نرم‌کند پیل دمان را</p></div>
<div class="m2"><p>چون تیغ زند رام‌ کند شیر عَرین را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرگز ظفر از عزم متینش نبود دور</p></div>
<div class="m2"><p>گویی‌که ظفر بنده شد آن عزم متین را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرگز خرد از رای رزینش نکشد سر</p></div>
<div class="m2"><p>گویی‌که خرد سخره شد آن رآی رزین را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای شاه فلک خاتم و خورشید نگینت</p></div>
<div class="m2"><p>پیروزی و اقبال تو مُهرست نگین را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در دایرهٔ ملک تویی نقطهٔ مفرد</p></div>
<div class="m2"><p>ره نیست در این دایره همتا و قرین را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از طین چو تویی آمد و چون احمد مُرسَل</p></div>
<div class="m2"><p>بر مرکز نورست شرف جوهر طین را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هنگام سواری ز سواران مبارز</p></div>
<div class="m2"><p>شایسته‌ تر از تو نبود مرکب و زین را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در معرکه برهان مبین تیغ تو بیند</p></div>
<div class="m2"><p>چون چشم نهد خصم تو برهان مبین را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدخواه لعین را بود از هیبت نامت</p></div>
<div class="m2"><p>قهری‌که ز لا حول بود دیو لعین را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سهم‌ است ز پیکان تو در بتکدهٔ هند</p></div>
<div class="m2"><p>بیم است ز تُرکان تو بتخانهٔ چین را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گرد سپه وگوهر شمشیر تو در رزم</p></div>
<div class="m2"><p>گویی که دخان شر رست آتش کین را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در خاک بسی‌گنج دفین است نهاده</p></div>
<div class="m2"><p>شاهان هنرمند و امیران گزین را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو گنج همی از قبل بخشش خواهی</p></div>
<div class="m2"><p>در خاک چه تاثیر بود گنج دفین را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هرگز نبود چون تو ملک تا به قیامت</p></div>
<div class="m2"><p>افروختن دولت و پروردن دین را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دیندار و جوانمرد و جهانگیر و دلیری</p></div>
<div class="m2"><p>وین است علامت ملک باز پسین را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>امروز در این بزم که چون خلد برین است</p></div>
<div class="m2"><p>ماند می صافی ز خوشی ماء معین را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دیدار همایو‌نت‌ فزایندهٔ جان است</p></div>
<div class="m2"><p>هم خواجهٔ نیکودل و هم خواجهٔ مُعین را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وز فر تو امروز فزون زانکه همه روز</p></div>
<div class="m2"><p>شادی و نشاط است هم آن را و هم این را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هرگه ‌که نهد بنده جبین پیش تو بر خاک</p></div>
<div class="m2"><p>تفضیل نهد بر همه اندام جبین را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همواره دلش مدح تو را هست مهیا</p></div>
<div class="m2"><p>چونانکه مهیاست صدف دُرّ ثمین را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا در دل مخلوق‌گمان است و یقین است</p></div>
<div class="m2"><p>شکر تو مدد باد گمان را و یقین را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا نام مکان است و مکین است در آفاق</p></div>
<div class="m2"><p>عدل تو سبب باد مکان را و مکین را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون چرخ برین از تو زمین باد مُزَیّن</p></div>
<div class="m2"><p>تا دور بود گرد زمین‌،چرخ برین را</p></div></div>