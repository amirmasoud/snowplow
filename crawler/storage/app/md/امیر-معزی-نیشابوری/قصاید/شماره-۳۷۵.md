---
title: >-
    شمارهٔ ۳۷۵
---
# شمارهٔ ۳۷۵

<div class="b" id="bn1"><div class="m1"><p>لاغری یار من است از همه خوبان جهان</p></div>
<div class="m2"><p>که بتی موی میان است و مهی تنگ دهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهم آن را که بود چون دل من تنگ‌دهن</p></div>
<div class="m2"><p>جو‌یم آن را که بود چون تن من موی میان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یار لاغر به همه ‌حال ز فربه بهتر</p></div>
<div class="m2"><p>ور ندانی ز من آگاه شو و نیک بدان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوشتر از شاخ سپیدار بود شاخ سمن</p></div>
<div class="m2"><p>بهتر از نارون و مشک بود سرو روان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ماه چون نو شود از لاغری و باریکی</p></div>
<div class="m2"><p>بنمایند به انگشت همه خلق جهان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ستونی بود از سیم، نگیری در دست</p></div>
<div class="m2"><p>باز سوی دهن آری چو خلالی بود آن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوست لاغر را برگیری و یک فربه را</p></div>
<div class="m2"><p>به دو صد حیله در آغوش گرفتن نتوان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فربهان را نتوان داشت نهان در هر جای</p></div>
<div class="m2"><p>لاغران را به همه جای توان داشت نهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سبکی شادی جان است و گرانی غم دل</p></div>
<div class="m2"><p>بفروشم غم دل، بازخرم شادی جان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جان سبک باشد و لاغر نبود جز که سبک</p></div>
<div class="m2"><p>تن ‌گران باشد و فربه نبود جز که ‌گران</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>منم آن عاشق آشفته‌دل لاغردوست</p></div>
<div class="m2"><p>جز بدین نام مرا پیش همه خلق مخوان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لاغری دارم و با او دل من سخت خوش است</p></div>
<div class="m2"><p>صبر نتوانم از او یک نفس و نیم زمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همچنین یار دلارام و چنین دلبر خوش</p></div>
<div class="m2"><p>آفرین شرف الملک مرا داد نشان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قبلهٔ دولت ابوسعد خداوند سعود</p></div>
<div class="m2"><p>دادگر محتشم داد ده داد ستان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن که از بخشش او فخرکند ملک زمین</p></div>
<div class="m2"><p>آن که از دانش او شاد بود شاه زمان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کف او جود عیان است و کف خلق خبر</p></div>
<div class="m2"><p>دل او علم یقین است و دل خلق‌ گمان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر گمان تکیه ندارند کجا هست یقین</p></div>
<div class="m2"><p>وز خبر یاد ندارند کجا هست عیان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای خداوند کریمان و دلیل دولت</p></div>
<div class="m2"><p>ای سرافراز بزرگان و امین سلطان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پیش از این ‌گاه کفایت، پس از این ‌گاه خرد</p></div>
<div class="m2"><p>ننشست و ننشیند چو تو اندر دیوان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مشتری سعد فشاند به سر کلک تو بر</p></div>
<div class="m2"><p>چون شود کلک تو بر سیم و سمن مشک‌فشان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>استوار از قلم و دست تو بینم همه سال</p></div>
<div class="m2"><p>ملک سلطان معظم ز کران تا به ‌کران</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گوی تدبیر وکفایت زبزرگان بردی</p></div>
<div class="m2"><p>پس از این کس نزند گوی و نبازد چوگان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>باز نشناسد اگر نوشرَوان زنده شود</p></div>
<div class="m2"><p>قلعهٔ درگهت از سلسلهٔ نوشروان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>قدم همّت تو تارک کیوان سپرد</p></div>
<div class="m2"><p>زان قبل راه نیابد به تو نحس کیوان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تن بدخواه و بداندیش تو چون نالهٔ طفل</p></div>
<div class="m2"><p>کند اندر شکم مادر فریاد و فغان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مهر و کین تو دهد سود و زیان همه‌کس</p></div>
<div class="m2"><p>مهرتو سود پدید آرد وکین تو زیان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دل بدخواه تو چون خسته کند دست اجل</p></div>
<div class="m2"><p>باشد از تیروکمان فلکش تیر وکمان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به فلک برشده مریخ و زحل زان دارد</p></div>
<div class="m2"><p>تا غلامان تو را سازد پیکان و سنان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>حور خواهد که ‌کند صورت او نقش بساط</p></div>
<div class="m2"><p>چون نهی پای برین سدره و این شادروان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تخت تو جایگهی دارد بر عرش بلند</p></div>
<div class="m2"><p>گر به حیلت رسد اندیشهٔ مخلوق بدان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در بقای ازلی بخت یکی نامه نوشت</p></div>
<div class="m2"><p>تا بر آن نامه مگر نام تو باشد عنوان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ننمودست جنان را مَلِک‌العَرش به‌کس</p></div>
<div class="m2"><p>وافریدست سرای تو نمودار جنان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گوهر سرخ‌ بروید ز همه روی زمین</p></div>
<div class="m2"><p>گر دهد جود تو یک بار زمین را باران</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ای‌ کریمی‌ که عنا را به عنایت ببری</p></div>
<div class="m2"><p>برنتابم ز در و درگه تو نیز عنان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کردمی هوش و روان بر سر مدح تو نثار</p></div>
<div class="m2"><p>گر مرا دست رسیدی به‌سوی هوش و روان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گر ثناهای تو گفتن نتواند دل من</p></div>
<div class="m2"><p>ترجمان دل من بنده بود کلک و زبان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>درّ و یاقوت من از همت وجود تو سزد</p></div>
<div class="m2"><p>زان‌ کجا همت وجود تو چو بحرست و چوگان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا بود راغ چو زنگار به هنگام بهار</p></div>
<div class="m2"><p>تا بود باغ چو دینار به هنگام خزان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تا مکان است و درو خُزْد و بزرگی است مکین</p></div>
<div class="m2"><p>تا به زیر قدم خرد و بزرگ است مکان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شاد بادند به درگاه تو هم خرد و بزرگ</p></div>
<div class="m2"><p>تازه بادند به ایوان تو هم پیر و جوان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تن و جان و دل تو خرم و شادان و درست</p></div>
<div class="m2"><p>برخور از جان و تن و کام دل خویش بران</p></div></div>