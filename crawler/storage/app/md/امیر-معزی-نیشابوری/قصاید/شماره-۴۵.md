---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>ایام نشاط است که عید است و بهار است</p></div>
<div class="m2"><p>گیتی همه پربوی‌ گل و رنگ و نگار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هر وطنی خرمی از موکب عیدست</p></div>
<div class="m2"><p>در هر چمنی تازگی از باد بهارست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا باد بهاری به سوی باغ گذر کرد</p></div>
<div class="m2"><p>بر شاخ درختان‌ گل و نسرین که به‌ بار است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر طرف چمن شاخ درختان ز شکوفه</p></div>
<div class="m2"><p>مانند بت سیمبر مُشک عِذارست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گشته است بنفشه چو یکی عاشق مهجور</p></div>
<div class="m2"><p>کز هجر سرافکنده و از عشق نزار است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نرگس قدح باده نهادست به ‌کف بر</p></div>
<div class="m2"><p>زان است که در دیدهٔ او خواب و خمارست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سبزه و لاله به لب جوی و سر کوه</p></div>
<div class="m2"><p>از مرغ جغانه است و ز نخجیر قطار است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرد آمدن مرغ و به هم رفتن نخجیر</p></div>
<div class="m2"><p>از بهرشکار مَلِک شیر شکار ست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سنجر که به خنجر دل بدخواه بسوزد</p></div>
<div class="m2"><p>زیرا که تف خنجر او صاعقه بار است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن شاه جهانگیر که از تاختن او</p></div>
<div class="m2"><p>بر قیصر و فغفور جهان همچو حصار است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از موکب او تا به در هند نهیب است</p></div>
<div class="m2"><p>و ز لشکر او تا به حد روم غبار است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر اسب ‌گه رزم همه مردی و زورست</p></div>
<div class="m2"><p>بر تخت‌گه بار همه حلم و وقارست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بحری است‌ گهربخش‌ که بر تخت نشسته است</p></div>
<div class="m2"><p>شیری است عدو سوز که بر اسب سوار است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در خدمت او شخص ادب راست مزاج است</p></div>
<div class="m2"><p>در مد‌حت او زرّ سخن پاک عیار است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تاجند تبارش ز شرف بر سر شاهان</p></div>
<div class="m2"><p>او از هنر و روزبهی تاج تبارست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا کرد عیان دولت او صورت دولت</p></div>
<div class="m2"><p>‌چرخ آینه گون است و قضا آینه‌دارست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همواره بود تعبیهٔ دولت او راست</p></div>
<div class="m2"><p>کان تعبیه را قاعده تا روز شمارست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای شاه ز تو تخت همی شکرگزارد</p></div>
<div class="m2"><p>هرچند کزو هر ملکی شکرگزارست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر نام تو از تاجوران خطبه و سکه است</p></div>
<div class="m2"><p>هرجا که در اسلام بلادست و دیارست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر خیل ز ترکان تو چون سیل جبال است</p></div>
<div class="m2"><p>هر فوج ز گردان تو چون موج بحارست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از روضهٔ عدل تو در آفاق نسیم است</p></div>
<div class="m2"><p>وز آتش خشم تو در اقطار شرارست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مانندهٔ آب است حُسام تو ولیکن</p></div>
<div class="m2"><p>این است که از خون اعادیش بخارست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز اندیشهٔ روزی ننهد بار به دل بر</p></div>
<div class="m2"><p>هر بنده‌که او را سوی درگاه تو بارست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آن کس که برفت از در تو بیهده روزی</p></div>
<div class="m2"><p>تا از در تو دور شدست از دردارست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای ناصر دین نبی و یار شریعت</p></div>
<div class="m2"><p>ایزد به همه کار تو را ناصر و یار است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اَجرام فلک را به هوای تو مسیر است</p></div>
<div class="m2"><p>زیرا که فلک را به مدار تو مدارست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>با حد وکنار است همه چیز به‌ گیتی</p></div>
<div class="m2"><p>فیروزی و اقبال تو بی‌حدّ و کنارست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا نصرت و یمن است تو را سوی یمین است</p></div>
<div class="m2"><p>تا راحت و یسر است تو را سوی یسار است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دمساز موالیت می و نالهٔ زیرست</p></div>
<div class="m2"><p>همراه مَعادیت غم و نالهٔ زار است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>عید آمد و بگذشت همه روز به شادی</p></div>
<div class="m2"><p>وقت طرب و خرمی و جام عقار است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا یازده مه اهل طرب را به سعادت</p></div>
<div class="m2"><p>در مجلس تو با می و مطرب سر و کارست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اقرار دهد هر که حریف است در این کار</p></div>
<div class="m2"><p>کز مجلس عا‌لیت بر این جمله قرار است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا روز رونده است و شب اندر پی روزست</p></div>
<div class="m2"><p>تا خار خَلنده است وگل اندر بی خارست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از دولت تو جان ولی تازه چو گل باد</p></div>
<div class="m2"><p>کز هیبت تو روز عدو چون شب تارست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خوش باد همه روز تو چون عید و چو نوروز</p></div>
<div class="m2"><p>کز فرّ تو هر روز گل بزم به بارست</p></div></div>