---
title: >-
    شمارهٔ ۲۰۴
---
# شمارهٔ ۲۰۴

<div class="b" id="bn1"><div class="m1"><p>آهن و نی جون پدید آمد ز صنع‌کردگار</p></div>
<div class="m2"><p>در میان‌کلک و تیغ افتاد جنگ و کارزار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیغ‌گفتا فخر من ز آن است‌کاندر شاء‌ن من</p></div>
<div class="m2"><p>گاه وحی آمد «‌واَنزَلنَا الْحَدید» از کردگار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کلک‌گفتا آمد اندر شان من «‌ن والقلم‌»</p></div>
<div class="m2"><p>هم‌ برین‌ معنی‌مرا فخرست‌ تاروز شمار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تیغ‌گفتا لون من لون سپهر آمد درست</p></div>
<div class="m2"><p>هست از این معنی مرا برگردن مردان گذار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کلک‌گفتا شکل من شکل شهاب آمد درست</p></div>
<div class="m2"><p>مردم شیطان‌پرست از من نیابد زینهار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیغ‌ گفتا هستم ان مکار کز مکر من است</p></div>
<div class="m2"><p>کارگیتی مستقیم و بند شاهی استوار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کلک‌گفتا هستم آن نقاش کز نقش من است</p></div>
<div class="m2"><p>خوب و زشت و نیک و بد در دین و دنیا آشکار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تیغ‌گفتا قوت مریخ دارد جرم من</p></div>
<div class="m2"><p>در مصاف و جنگ باشد جرم من مریخ‌وار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کلک‌ گفتا از عطارد بهره دارد فعل‌ من</p></div>
<div class="m2"><p>در حساب و درکتابت هستم او را اختیار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تیغ‌گفتا من درختی ام که در باغ ظفر</p></div>
<div class="m2"><p>دارم از بیجاده برگ و دارم از یاقوت بار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کلک‌ گفتا من سحابی ام که باران من است</p></div>
<div class="m2"><p>عنبر و مشک و منم عنبر فشان و مشکبار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تیغ‌ گفتا من یکی شیرم‌ که دارم روز رزم</p></div>
<div class="m2"><p>مغز بدخواهان سلطان معظم مرغزار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کلک گفتا من یکی مرغم که بر سیم سپید</p></div>
<div class="m2"><p>رازها پیدا کنم چون‌ بارم از منقار قار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تیغ‌ گفتا پادشاهان را به من فخرست از آنک</p></div>
<div class="m2"><p>چند گه بودم من اندر دست حیدر ذوالفقار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کلک‌گفتا در جهان از قول و از فعل من است</p></div>
<div class="m2"><p>قصهٔ شاهان و اخبار بزرگان یادگار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر دو زین معنی بسی‌ گفتند و آخر یافتند</p></div>
<div class="m2"><p>قیمت و مقدار خویش از دست شاه روزگار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سایهٔ یزدان ملکشاه آفتاب خسروان</p></div>
<div class="m2"><p>شهریارِ کامران و پادشاهِ کامکار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن شهنشاهی‌که هست اندر عرب و اندر عجم</p></div>
<div class="m2"><p>از مبارک دست او تیغ و قلم را افتخار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اندر آن وقتی‌که ایزد شخص آدم آفرید</p></div>
<div class="m2"><p>این جهان فرمان عدلش را همی‌ کرد انتظار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هم به مشرق هم به مغرب خسروان جستند ملک</p></div>
<div class="m2"><p>جز براو نگرفت ملک مشرق و مغرب قرار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هست بر دفتر نگار مدح دیگر خسروان</p></div>
<div class="m2"><p>مدح سلطان هست بر جان خردمندان نگار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دوستان و دشمنانش را بلندی داد چرخ</p></div>
<div class="m2"><p>دوستانش را ز تخت و دشمنانش را ز دار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کمتر از یک ذره و یک قطره باشد از قیاس</p></div>
<div class="m2"><p>پیش ظلم او جبال و پیش جود او بحار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هرکجا مِغْفَر بود شمشیر او مِغْفَر شکاف</p></div>
<div class="m2"><p>هرکجا جوشن بود شمشیر او جوشن‌ گذار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در نشاط آرد جهان را همت او روز بزم</p></div>
<div class="m2"><p>در سجود آرد شهان را هیبت او روز بار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هست عدلش در جهان خورشید ناپیدا زوال</p></div>
<div class="m2"><p>هست ملکش بر زمین‌ گردون ناپیدا کنار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هست در چشم عدو دیدار او بی نار نور</p></div>
<div class="m2"><p>هست در مغز عدو شمشیر او بی نور نار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پادشاها از تو فرخ تر نباشد پادشاه</p></div>
<div class="m2"><p>شهریارا از تو عادل‌تر نباشد شهریار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مرکب شاهی و دولت را عنان در دست توست</p></div>
<div class="m2"><p>جز تو درگیتی نمی‌زیبد بر آن مرکب سوار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چون نشستی تو براسب دولت‌آن ساعت نشست</p></div>
<div class="m2"><p>از سم اسب تو بر روی بداندیشان غبار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نام آنکس‌کاو تورا بنده نباشد هست ننگ</p></div>
<div class="m2"><p>فخر آنکس‌ کاو تورا چاکر نباشد هست عار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هرکه را در سر خمارست از شراب کین تو</p></div>
<div class="m2"><p>ضربت‌ تیغ‌ تو او را بشکند درسرخمار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دولت و بخت تو شاها سازگارست و جوان</p></div>
<div class="m2"><p>دولت‌و بخت عدو پیر آمد و ناسازگار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>با چنین بخت و چنین دولت‌ کجا ماند عدو</p></div>
<div class="m2"><p>با چنان بخت و چنان دولت‌ کجا ماند حصار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تیر تو باکین تودارد مگر پیوستگی</p></div>
<div class="m2"><p>زان کجا هر دو به صید اندر یکی دارند کار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تیر تو گیرد شکار اندر میان دام و دَد</p></div>
<div class="m2"><p>کین توگرد جهان دشمن همی‌گیرد شکار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تا چمن دینارگون گردد به هنگام خزان</p></div>
<div class="m2"><p>تا زمین زنگارگون‌ گردد به هنگام بهار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همچنان بادی که هستی کامکار و کامران</p></div>
<div class="m2"><p>همچنان بادی که هستی شادکام و شادخوار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>روزگار و دولت و بخت تو هر سه بر مراد</p></div>
<div class="m2"><p>روزگارت بنده و دولت ندیم و بخت یار</p></div></div>