---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>گوهری گویا کزو شد دیده پرگوهر مرا</p></div>
<div class="m2"><p>کرد مشکین چنبر او پشت چون چنبر مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق او سیمین و زرّین ‌کرد روی و موی من</p></div>
<div class="m2"><p>او همی خواهد که بِفریبد به سیم و زر مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا مرا دل آس شد در آسیای عشق او</p></div>
<div class="m2"><p>هست پنداری غبار آسیا بر سر مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیدهٔ چون عَبهَرش بسته همه خون در تنم</p></div>
<div class="m2"><p>تا همه تن زرد شد چون دیدهٔ عبهر مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از سرشک و از تپانچه چهرهٔ من شد چُنا‌نک</p></div>
<div class="m2"><p>گر ببیند باز نشناسد ز نیلوفر مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زاب چشم و آذر دل هر شبی تا بامداد</p></div>
<div class="m2"><p>قطره و شعله است در بالین و در بستر مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیش داور بردم او را فتنه شد داور بر او</p></div>
<div class="m2"><p>تا ز رشکش داوری افتاد با داور مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون ز درد دل بنالیدم مرا باور نداشت</p></div>
<div class="m2"><p>کاشکی دیدی دلم تا داشتی باور مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر طبیبان ازگل و شَکّر علاج دل کنند</p></div>
<div class="m2"><p>او چرا درد دل آورد از گل و شَکّر مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر زمان آرد به عَمد‌ا زلف را نزدیک لب</p></div>
<div class="m2"><p>تا نماید دود دوزخ بر لب کوثر مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تاکه او از شب همی زنجیر سازدگرد روز</p></div>
<div class="m2"><p>عشق او چون حلقه دارد روز و شب بر در مرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر ز عشقش فتنهٔ یأجوج خیزد در دلم</p></div>
<div class="m2"><p>بس بود جاه سَدیدی سَدّ اسکندر مرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جاه آن صاحب که او شمس‌الشّرف دارد لقب</p></div>
<div class="m2"><p>وز شرف‌کردست با شمس و فلک همسر مرا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آنکه ایزد همچنانک او را به صُنع لطف خویش</p></div>
<div class="m2"><p>کُنیت صِدّیق داد و نام پیغمبر مرا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جان پاک میر ابوحاتم همی‌گوید به خُلد</p></div>
<div class="m2"><p>کز نشاط اوست ناز و شادی و مَفخَر مرا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر چه بی‌پیکر مرا در روضهٔ رضوان خوش است</p></div>
<div class="m2"><p>آرزو آید همی از بهر او پیکر مرا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر سر او باشدی هر ساعتی پرواز من</p></div>
<div class="m2"><p>گر دهندی بر مثال مرغ بال و پر مرا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفت‌گیتی از مرادش برنگردم تا بود</p></div>
<div class="m2"><p>آب و خاک و باد و آتش عنصر و گوهر مرا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گفت آتش گرچه من رخشنده و سوزنده‌ام</p></div>
<div class="m2"><p>نار خشم او کند انگشت و خاکستر مرا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بادگفت از خلق او من بهره‌ای کردم نهان</p></div>
<div class="m2"><p>تا لقب باشد جهان آرای جان پرور مرا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آب‌گفتا گر مرا بودی صفای طبع او</p></div>
<div class="m2"><p>کس ندیدی تیره در دریا و در فَرغَر مرا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خاک‌گفتا گر مرا بودی ز حلم او نصیب</p></div>
<div class="m2"><p>بر هوا هرگز نبردی جنبش صَرصَر مرا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خسرو مشرق همی‌گوید که از تدبیر اوست</p></div>
<div class="m2"><p>در جهانداری مُسلّم خاتم و افسر مرا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا سپهدار مرا باشد چو او فرمانبری</p></div>
<div class="m2"><p>شهریاران جهان باشند فرمانبر مرا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تاکه شد مخدوم من در لشکر من پهلوان</p></div>
<div class="m2"><p>جرخ لشکرگَه شد و سَیّارگان لشکر مرا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از نهیب تیغ مخدومم به هند و ترک و روم</p></div>
<div class="m2"><p>طاعت است از رأی و از فَغفُور و از قیصر مرا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یاور من فتح و نصرت باشد اندرکارزار</p></div>
<div class="m2"><p>تا بود در فتح و نصرت تیغ او یاور مرا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>صاحب عادل همی‌گوید که از دیدار اوست</p></div>
<div class="m2"><p>روشنایی هر زمان افزون به چشم اندر مرا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا همه خلق جهان را زندگانی درخورست</p></div>
<div class="m2"><p>هست همچون زندگانی مهر او درخور مرا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>عِزّ دین‌گوید همی تا او وزیر من شدست</p></div>
<div class="m2"><p>عز و پیروزی است ازگردون و از اختر مرا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از مبارک رای او هرمه که بر من بگذرد</p></div>
<div class="m2"><p>نصرتی دیگر بود بر دشمن دیگر مرا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گه بود بر نیزه پیش من سرگردنگشان</p></div>
<div class="m2"><p>گه بود بر حنجر گردنکشان خنجر مرا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از نهیب تیغ من چون زرکند رخسار زرد</p></div>
<div class="m2"><p>گر به رزم اندر ببیند پورِ زالِ زر مرا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آنحه بشنیدم به ایام ملوک روزگار</p></div>
<div class="m2"><p>هست صد چندان به ایام ملک سنجر مرا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تا مرا دستور بوبکرست باشد کی عجب</p></div>
<div class="m2"><p>گر بود عزم عُمَر با صولت حیدر مرا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>رونق کار من از تدبیر دستور من است</p></div>
<div class="m2"><p>هیچ دستوری نباشد زین مبارکتر مرا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>باد فروردین همی گوید که از اقبال اوست</p></div>
<div class="m2"><p>باشد اندر زینت آفاق یاریگر مرا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چون مزین کرد آفاق از نگار و رنگ من</p></div>
<div class="m2"><p>ایمنی باشد ز باد مهر و شهریور مرا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ابرگفتا ازکفش با ناله و با شورشم</p></div>
<div class="m2"><p>گرگوا خواهی ببین با برق و با تندر مرا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کو‌ه گفت از شرم حلمش عاشقم بر ماه دی</p></div>
<div class="m2"><p>زانکه ابر از ماه دی بر سرکشد چادَر مرا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بلبل شیدا همی‌گویدکه اندر باغ او</p></div>
<div class="m2"><p>پیشه شد رامشگری بر سرو و بر عرعر مرا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گفت نرگس می‌بهٔاد او خورم زیراکه او</p></div>
<div class="m2"><p>برکف سیمین نهد برکف همی ساغر مرا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گفت لاله من دل خصمان او سوزم همی</p></div>
<div class="m2"><p>زانکه خشمش بهره داد از دود و از اخگر مرا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>کشتی دولت همی‌ گوید که در دریای مُلْک</p></div>
<div class="m2"><p>رای او بس بادبان و حلم او لنگر مرا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بخت‌ گوید گر به ‌نامش خطبه خوانم بر فلک</p></div>
<div class="m2"><p>عرش و کرسی بس نباشد کرسی و منبر مرا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>جود او گوید که من بخشنده بحرم زانکه هست</p></div>
<div class="m2"><p>از کف او کشتی و از حِلم او لنگر مرا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>اسب اوگویدکه تا بر من سوارست آفتاب</p></div>
<div class="m2"><p>گاه جولان هست دور گنبد اَخضَر مرا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ای همه ساله سخاگسترکف میمون تو</p></div>
<div class="m2"><p>وقف شد بر مدح تو طبع سخن‌ گستر مرا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گر ز یحیی و ز جعفر هست در بخشش مثل</p></div>
<div class="m2"><p>یاد ناید با تو از یحیی و از جعفر مرا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>در باک و مشک ناب از بهر سکر و مدح تو</p></div>
<div class="m2"><p>در ضمیر و در قَلَم شد مُدغَم و مُضمَر مرا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>من مقیمی چون توانم بود در خدمت‌که نیست</p></div>
<div class="m2"><p>خیمه و خرگاه و اسب و اشتر و استر مرا</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>در هر آن دولت که من بودم به اقبال کمال</p></div>
<div class="m2"><p>بوده‌ایی مُنکر همه معروف سرتاسر مرا</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>وندران حضرت زنیرنگ و دروغ این و آن</p></div>
<div class="m2"><p>هست بی‌معروف سرتاسر همه منکر مرا</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>پیش تو در دوستداری محضری آورده‌ام</p></div>
<div class="m2"><p>تا چو خوانی محضرم خوانی نکو محضر مرا</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>مقبلی تو دست بر دامان تو مقبل زدم</p></div>
<div class="m2"><p>این وصیت هم پدر کردست هم مادر مرا</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>تا بود در شعر من ‌نَعتِ نگار آزری</p></div>
<div class="m2"><p>باد نامت حِرز ابراهیم بِن‌ آزر مرا</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تا مدایح باشد اندر دفتر و دیوان من</p></div>
<div class="m2"><p>باد پُر مدح تو هم دیوان و هم دفتر مرا</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>از سعادت باد تا محشر بقای جسم تو</p></div>
<div class="m2"><p>در ثنای تو زبان پُر مدح تا محشر مرا</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چون قَهستانی تو را چاکر بسی در مهتری</p></div>
<div class="m2"><p>پیش تو چون فرّخی در شاعری چاکر مرا</p></div></div>