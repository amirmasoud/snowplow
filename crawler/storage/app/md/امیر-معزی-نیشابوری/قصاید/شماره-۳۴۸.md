---
title: >-
    شمارهٔ ۳۴۸
---
# شمارهٔ ۳۴۸

<div class="b" id="bn1"><div class="m1"><p>از هیبت و نهیب تو ای خسرو جهان</p></div>
<div class="m2"><p>گشتند دشمنان بی‌جان‌ تو و بی‌روان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رُمحِ همه قلم شد و فَرقِ همه قَدَم</p></div>
<div class="m2"><p>روی همه قفا شد و سود همه زیان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر پایشان چو کُندهٔ پولاد شد رکاب</p></div>
<div class="m2"><p>بر دستشان چو حلقهٔ زنجیر شد عنان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمشیر در نهاده چو خصمان بهٔکدگر</p></div>
<div class="m2"><p>آن بدسگال این شده این بدسگال آن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زین سان وزین نهاد گریزند سربه‌سر</p></div>
<div class="m2"><p>آسیمه در ولایت و آشفته در جهان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گه‌ گوید این‌ که شعلهٔ تیغ آمد الحذر</p></div>
<div class="m2"><p>گه ‌گوید آن‌که نامهٔ عفو آمد اَ‌لْاَمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل باید و خزانه و تیغ و سپاه و تخت</p></div>
<div class="m2"><p>تا بر مراد خویش بود مرد کامران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یعقوب را چو زین همه عُدَّت یکی نبود</p></div>
<div class="m2"><p>بیهوده قصد ملک چرا کرد رایگان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از پیش لاف زدکه منم مردکارزار</p></div>
<div class="m2"><p>چون وقت حمله بود شد از بیم تو نهان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بس کس‌که‌گاه حمله چو میشی بودضعیف</p></div>
<div class="m2"><p>هرچند گاه لاف چو شیری بود ژیان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگریخت زین ولایت و شد باز جای خویش</p></div>
<div class="m2"><p>چون یافت از علامت و مَنجوق تو نشان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آری چو بانگ جُلجُل باز آید از هوا</p></div>
<div class="m2"><p>دُرّاج زود بازگریزد در آشیان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کاسان و اوزگندو سمرقند پیش ازین</p></div>
<div class="m2"><p>بودست گنج‌خانهٔ چندین تکین و خان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بی‌آنکه در نبرد فروزنده شد حُسام</p></div>
<div class="m2"><p>بی‌آنکه در مصاف درخشنده شد سنان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بی‌آنکه شد کشیده یکی خنجر از نیام</p></div>
<div class="m2"><p>بی‌آنکه شدگشاده یکی ناوک از کمان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بگشادی این سه قلعه‌ که هر قلعه را سزد</p></div>
<div class="m2"><p>کس مهرکوتوال بود ماه پاسبان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از اوزگند تا به فَرَب بستدی ز خصم</p></div>
<div class="m2"><p>بستی میان و فتنه برون‌کردی از میان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هرگز که یافته است چنین طالعی قوی</p></div>
<div class="m2"><p>هرگز که داشته است چنین دولتی جوان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از مُعتَصَم‌ گذشته کرا بود جز تو را</p></div>
<div class="m2"><p>این ملک و این خزانه و این لشکرگران</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از ترک و دیلم و عرب و روم عالمی</p></div>
<div class="m2"><p>جز تو به اوزگَند که آورد زاصفهان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جز تو حصار و خانهٔ خاقانیان که کرد</p></div>
<div class="m2"><p>جای امیر و حاجب و سالار و پهلوان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اخبار و قصهٔ تو ز بس گونه‌گون شگفت</p></div>
<div class="m2"><p>منسوخ کرد قصه و اخبار باستان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آنچ از تو دیده‌ایم و بخواهیم نیز دید</p></div>
<div class="m2"><p>نشنیده‌ایم در کتب از هیچ داستان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از دولت تو هر چه‌ گمان بود شد یقین</p></div>
<div class="m2"><p>وز دشمن تو هر چه یقین بود شدگمان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آن‌ کیست ‌کاو به ملک‌ کند با تو همسری</p></div>
<div class="m2"><p>از روم تا به هند و ز چین تا به قیروان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو ایدری و از فَزَعِ جنگیان توست</p></div>
<div class="m2"><p>درکاشغر مصیبت و اندر خُتن فغان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سیماب شد تن چِگِلی ازنهیب سر</p></div>
<div class="m2"><p>طَبطاب شد دل خُتَنی از نهیبِ جان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نیل است و زعفران‌ حسد تو که حاسدت</p></div>
<div class="m2"><p>در دیده نیل دارد و بر چهره ارغوان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خون در رگ از نهیب تو چون ژاله بفسرد</p></div>
<div class="m2"><p>و اخگر شود ز بیم تو مغز اندر استخوان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از رشک روی توست زبان حاسدِ بَصَر</p></div>
<div class="m2"><p>وز رشک نام توست بصر دشمن زبان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همواره آسمان و زمین تابع تواند</p></div>
<div class="m2"><p>تا یار تو خدای زمین است و آسمان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ای شاه کار خویش به ایزد سپار و بس</p></div>
<div class="m2"><p>کایزد چنانکه باید سازد همی چنان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تو شاکری زخالق و خلق از تو شاکرند</p></div>
<div class="m2"><p>تو شادمان و دولت و ملک از تو شادمان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زودا که باز گردی زایدر سوی عراق</p></div>
<div class="m2"><p>با بندگان بُراق سعادت به زیر ران</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دشمن به دام و کار به ‌کام و فلک غلام</p></div>
<div class="m2"><p>دولت نگاهدار و سعادت نگاهبان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در کاشغر ز حضرت تو شحنه و عمید</p></div>
<div class="m2"><p>واندر ختن ز دست تو والی و مرزبان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از فرّ تو رسیده سعادت بهر وطن</p></div>
<div class="m2"><p>وز فتح تو رسیده سلامت بهر مکان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>افتاده دشمنان تو درکندهٔ سقر</p></div>
<div class="m2"><p>وآسوده دوستان تو در روضهٔ جنان</p></div></div>