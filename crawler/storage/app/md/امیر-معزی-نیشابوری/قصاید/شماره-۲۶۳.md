---
title: >-
    شمارهٔ ۲۶۳
---
# شمارهٔ ۲۶۳

<div class="b" id="bn1"><div class="m1"><p>چه پیکرست ز تیر سپهر یافته تیر</p></div>
<div class="m2"><p>به شکل تیر و بدو ملک راست گشته چو تیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کجا بگرید در کالبد بخندد جان</p></div>
<div class="m2"><p>کجا ببارد بر آسمان بتازد تیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز نادرات جواهر نشان دهد به سرشک</p></div>
<div class="m2"><p>ز مشکلات ضمایر خبر دهد به صریر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر آنچه طبع براندیشد او کند تأ‌لیف</p></div>
<div class="m2"><p>هر آنچه وهم فراز آرد او کند تفسیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>محررست ز حکم خدای و امر رسول</p></div>
<div class="m2"><p>چه در اَنامل مفتی چه در بنان دبیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدو محرر و کاتب همیشه محتاجند</p></div>
<div class="m2"><p>که آیتی است ز بهرکتابت و تحریر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بساط و خوابگه او بود ز سیم و اَدیم</p></div>
<div class="m2"><p>کلاه و پیرهن او بود ز مشک و عبیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همی ندانم تا عاشق است یا معشوق</p></div>
<div class="m2"><p>که گه به‌گونهٔ لاله است و گه به رنگ زریر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گهی به چشمه چو ماهی گل سیاه خورد</p></div>
<div class="m2"><p>گهی چو مرغ‌ زند برگل سفید صفیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به‌کودکی همه با شیر باشدش صحبت</p></div>
<div class="m2"><p>از آن پرستش پیران‌ کند چو گردد پیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به شیر خویش بپرورده است و این عجب است</p></div>
<div class="m2"><p>که او به شیر هم‌اکنون همی فشاند قیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر نه تارک او شد شکنج زلف بتان</p></div>
<div class="m2"><p>چرا ز قیر همی‌ نقشها کند بر شیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز حلق خویش زبان ساخته است‌گاه سخن</p></div>
<div class="m2"><p>ز فرق خویش قدم ساخته است ‌گاه مسیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به ‌جسم هست مریض و به‌عقل هست صحیح</p></div>
<div class="m2"><p>به چشم هست ضریر و به‌ فهم هست بصیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ندیده‌ام به ‌جهان پیکری عجب‌تر از او</p></div>
<div class="m2"><p>که هم صحیح مریض است و هم بصیر ضریر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به خیزران و صدف ماند او به‌دست کفات</p></div>
<div class="m2"><p>اگر بود صدف و خیزران به‌بحر و غدیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به ذات خویش مر او را شرف نبود و خطر</p></div>
<div class="m2"><p>به خدمت شرف‌الدین شریف‌گشت و خطیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وجیه ملک جمال‌کفات بو طاهر</p></div>
<div class="m2"><p>که یافت در نظر از عین جوهر تطهیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ستوده سعد علی مهتری‌که سعد و علو</p></div>
<div class="m2"><p>نصیب دولت او کرد کردگار نصیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بزرگوار جهان است و پیش همت او</p></div>
<div class="m2"><p>بود هزار جهان بزرگوار حقیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>صفای صورت او را طراز قدرت کرد</p></div>
<div class="m2"><p>کجاکشید به‌مقدور بر خط تقدیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بود به‌قدرت او ذات قادری به‌کمال</p></div>
<div class="m2"><p>مصوری که مر او را چنین بود تصویر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به‌حَلّ و عَقد چو بگشاد دست قدرت خویش</p></div>
<div class="m2"><p>عدو به‌قدرت او شد به‌دست عجز اسیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به بدر ماند لیکن منازلش عجب است</p></div>
<div class="m2"><p>که گاه زین بود و گاه صدر و گاه سریر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اگرچه بدر منیر اختری درفشان است</p></div>
<div class="m2"><p>چنین منازل هرگز ندید بدر منیر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ایا گرفته به ‌کلک تو کار ملک قرار</p></div>
<div class="m2"><p>وز آن قرار شده چشم روزگار قریر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نه هیچ میر در اسلام یافت چون تو قرین</p></div>
<div class="m2"><p>نه هیچ شاه در آفاق یافت چون تو مشیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خدایگان عجم را و صدر عالم را</p></div>
<div class="m2"><p>به فرخی و سعادت لقای توست مشیر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز شه سه فایده محتوم حاصل است تو را</p></div>
<div class="m2"><p>رضای مجلس خاتون و شکر شاه و وزیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گهی نثار فرستد سوی ضمیر تو چرخ</p></div>
<div class="m2"><p>گهی تو هدیه دهی چرخ را ضیاء ضمیر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مگر فریشته‌ای از فرشتگان خدای</p></div>
<div class="m2"><p>میان چرخ و ضمیر تو واسطه است و سفیر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>طرازِ دفترِ تاریخ ساختی سیَرَت</p></div>
<div class="m2"><p>اگر به‌ عصر تو بودی محمد بن جریر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اگرچه دست اجل را طویل کرد خدای</p></div>
<div class="m2"><p>اجل ز دامن تو دست خویش‌ کرد قصیر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>فضایل تو نگردد به وهم ما معدود</p></div>
<div class="m2"><p>که وهم ماست قلیل و فضایل تو کثیر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همی دلیل‌کند با عنایت وکرمت</p></div>
<div class="m2"><p>که در زمانه نه غمناک ماند و نه فقیر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هر آن زمین‌که بر آن مرکب تو پای نهد</p></div>
<div class="m2"><p>در آن زمین نکند شیر دست زی نخجیر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اگر زمانه به قِنطار در نهد پیشت</p></div>
<div class="m2"><p>بود به چشم تو قِنطار کمتر از قطمیر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اگر سعیر به فکرت‌ کنی‌ گشاده شود</p></div>
<div class="m2"><p>دری ز رحمت فردوس بر عذاب سعیر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>وگر کنی ز دمن وز طَلَل به همت‌ تو</p></div>
<div class="m2"><p>دمن شود چو خُورَنق طَلَل‌شود چو سدیر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>جماعتی‌ که ز امر تو سرکشی کردند</p></div>
<div class="m2"><p>شدند سخرهٔ مامور تو صغیر و کبیر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز بهر خواستن حاجت آن جماعت را</p></div>
<div class="m2"><p>به بارگاه تو امروز حاجت است و مسیر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بود به مدح تو افزون مدیح را رونق</p></div>
<div class="m2"><p>بود به عید زیادت نماز را تکبیر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اگرچه بر دل مردم خرد امیر شدست</p></div>
<div class="m2"><p>ضمیر روشن تو بر خرد شدست امیر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>منم‌که آرزوی من همیشه خدمت توست</p></div>
<div class="m2"><p>چنانکه آرزوی کیمیاگران اکسیر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هر آن شبی‌که خیال تو بینم اندر خواب</p></div>
<div class="m2"><p>هزار نیکویی آن خواب را کنم تعبیر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>همه رکاب تو بوسد که در دماغ من است</p></div>
<div class="m2"><p>غبار اسب تو خوشبوی‌تر ز بوی عبیر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نیامدست ز من در وجود هیچ گناه</p></div>
<div class="m2"><p>کز آن‌گناه همی خورد بایدم تشویر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز خویشتن نشناسم همی جز آن گنهی</p></div>
<div class="m2"><p>که در پرستش و مدح توکرده‌ام تقصیر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مرا بپرور و بِپذیر عذر من ز کرم</p></div>
<div class="m2"><p>که تو کریم رهی پروری و عذر پذیر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>همیشه تاکه خلایق هنرکنند به‌جهد</p></div>
<div class="m2"><p>ز بهر مرتبه و فایده شب و شبگیر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هنر ز رای رفیع تو باد مرتبه جوی</p></div>
<div class="m2"><p>خرد ز لفظ شریف تو باد فایده‌گیر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو نوبهار به‌هر بقعتی تورا اثار</p></div>
<div class="m2"><p>چو آفتاب به‌ هر کشوری تو را تاثیر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>عدو و خصم تو و حاسد و مخالف تو</p></div>
<div class="m2"><p>عدیل رنج و عَنا و بدیل گرم و زحیر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>یکی ز ناله چو نای و یک ز مویه چو موی</p></div>
<div class="m2"><p>یکی‌ به زردی زر و یکی به‌زاری زیر</p></div></div>