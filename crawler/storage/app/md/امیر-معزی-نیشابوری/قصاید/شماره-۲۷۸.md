---
title: >-
    شمارهٔ ۲۷۸
---
# شمارهٔ ۲۷۸

<div class="b" id="bn1"><div class="m1"><p>آمد آن ماه دو هفته با قبای هفت رنگ</p></div>
<div class="m2"><p>زلف پربند و شکنج و چشم پرنیرنگ و رنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لؤلؤ اندر لاله پنهان داشت چون رویم بدید</p></div>
<div class="m2"><p>چنگ را بر لاله زد لؤلؤ و برهم سود چنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت مهر از من‌ گسستی با تو جای جنگ هست</p></div>
<div class="m2"><p>لیکن اندر مهرگان با دوست نتوان کرد جنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرو اگر در باغ باشد دارد او بر سرو باغ</p></div>
<div class="m2"><p>سیم اگر در سنگ باشد دارد او در سیم سنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون دلم بی‌قُوََّت و جان و تنم بی‌قُوت دید</p></div>
<div class="m2"><p>داد قوت و قُوََّتم زان شَکّرِ یاقوت رنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تنگم اندر برگرفت و زلف مشکین برفشاند</p></div>
<div class="m2"><p>مشک و عنبر برگرفتند از سرای من به تنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گاه دلبر بود و گه چنگش همه شب درکنار</p></div>
<div class="m2"><p>یک زمان بنواخت یار و یک زمان بنواخت چنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتمش کز من چه خواهی مهرگانی یادگار</p></div>
<div class="m2"><p>تا جهان بر من نسازی چون دهان خویش تنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت خواهم شُکرِ اِنعام خداوندی که او</p></div>
<div class="m2"><p>اندر انعام و فتوت نام نعمان‌ کرد ننگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ملک یزدان را مؤیّد دین یزدان را شهاب</p></div>
<div class="m2"><p>آفتاب عقل و علم و مایهٔ فرهنگ و هنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن خداوندی که گردون بخت او را مرکب است</p></div>
<div class="m2"><p>مرکبی‌ کش‌ ماه نو زین است و جوزا پالهنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون نهادند اختران از قوت تأثیر خویش</p></div>
<div class="m2"><p>هم به نار اندر شتاب و هم به خاک اندر درنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>باد را از طبع او پاکیزگی دادند و لطف</p></div>
<div class="m2"><p>خاک را از حلم او آهستگی دادند و سنگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تیزی آموزد همی از حکم او شمشیر تیز</p></div>
<div class="m2"><p>راستی‌گیرد همی ازکلک او تیر خدنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در زمستان فرش او را از پلنگ آرند پوست</p></div>
<div class="m2"><p>بر سباع‌کوه و صحراکبر از آن دارد پلنگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از ‌دم خصمش‌ به آتش در سمندر بِفسُرَد</p></div>
<div class="m2"><p>وز تف خشمش بسوزد زیر آب اندر نهنگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بیش او خلق از مروف لاف نتواند زدن</p></div>
<div class="m2"><p>بیش رهواران به رهواری نداند رفت لنگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در پناه امر او نشگفت اگر کوته شود</p></div>
<div class="m2"><p>پنجهٔ شیر از گراز و چنگل باز از کلنگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر ز مهر او فتد یک ذره در دریای چین</p></div>
<div class="m2"><p>ور زجود او چکد یک قطره در دریای زنگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نه به چین اندر بماند هیچ رخ در زیر چین</p></div>
<div class="m2"><p>نه به زنگ اندر بماند هیچ دل در زیر زنگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای سرفرازی که از تاج شهان زیبد همی</p></div>
<div class="m2"><p>بر میان بندگان تو گهر هنگام جنگ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ماه مهر آمد زیادت‌کرد باید مهر ماه</p></div>
<div class="m2"><p>آب شد چون زنگ برکف باده‌ها باید چو زنگ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از کف تُرک دلارامی‌ که از دیدار اوست</p></div>
<div class="m2"><p>حسرت صورتگران چین و نقاشان‌گنگ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شیر زوری‌کاو به نیزه زور بستاند ز شیر</p></div>
<div class="m2"><p>رنگ چشمی‌کاو به غمزه چشم برباید ز رنگ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تاکه سیسنبر ندارد رنگ و بوی شنبلید</p></div>
<div class="m2"><p>تاکه آذرگون ندارد بوی و رنگ بادرنگ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خار در دست نکوخواه تو بادا چون سمن</p></div>
<div class="m2"><p>شهد درکام بداندیش تو بادا چون شرنگ</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مهرگان بر تو همایون باد از گشت سپهر</p></div>
<div class="m2"><p>جاه تو بی‌عیب باد و عمر تو بی آذرنگ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>روز و شب بر درگه عالیت دست روزگار</p></div>
<div class="m2"><p>مرکب اقبال و دولت راکشیده تنگ تنگ</p></div></div>