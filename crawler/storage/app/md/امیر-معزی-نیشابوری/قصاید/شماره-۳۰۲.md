---
title: >-
    شمارهٔ ۳۰۲
---
# شمارهٔ ۳۰۲

<div class="b" id="bn1"><div class="m1"><p>از مشک اگر ندیدی بر پرنیان علم</p></div>
<div class="m2"><p>وز قیر اگر ندیدی بر ارغوان رقم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر پرنیان ز مشک علم دارد آن نگار</p></div>
<div class="m2"><p>بر ارغوان ز قیر رقم دارد آن صنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلف سیاه بر رخ او هست سایبان</p></div>
<div class="m2"><p>بر طرف نور طرفه بود سایبان ظُلَم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با روی او بهشت به دنیا شد آشکار</p></div>
<div class="m2"><p>وز شرم روی او ز جهان شد نهان ارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رویبثن همی نهفته نباید زجشم من</p></div>
<div class="m2"><p>گر تازه و شکفته شود گلستان ز نم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از چفتگی چو چنگ شدم در فراق او</p></div>
<div class="m2"><p>از ناله همچو زیر شدم از فغان چو بم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در وصل او کنم جگر گرم را علاج</p></div>
<div class="m2"><p>گر یابم از لبش شکر و ناردان به هم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بینند روز وصل چو رخ بر رخم نهد</p></div>
<div class="m2"><p>بر شَنبَلید لاله و بر زَعفران بقم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای دلبری که قد تو چون تیر راست است</p></div>
<div class="m2"><p>وز عشق توست قامت من چون‌ کمان به خم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر من ستم مکن‌ که به انصاف و عدل خویش</p></div>
<div class="m2"><p>برداشته‌ است شاه جهان از جهان ستم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سنجر خدایگان جهان کز فتوح او</p></div>
<div class="m2"><p>گشته است پر عجایب و پر داستان عجم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شاهی که دارد او چو فریدون و سام یل</p></div>
<div class="m2"><p>صد تاجدار بنده و صد پهلوان خدم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از خیلِ چاکران و غلامانِ خاص اوست</p></div>
<div class="m2"><p>در قندهار لشکر و در قیروان‌ حشم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سدّی است در زمانه و سعدی است در جهان</p></div>
<div class="m2"><p>اندر یمین حسامش و اندر بنان قلم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر بام قصر او ز بلندی عجب مدار</p></div>
<div class="m2"><p>گر بر سر ستاره نهد پاسبان قدم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گرگ است دهر و ما غَنَم و عدل او شبان</p></div>
<div class="m2"><p>از گرگ بی‌گزند بود با شبان غنم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از اوزْگَند تا فَرَبْ از دست اوست خان</p></div>
<div class="m2"><p>وز جود اوست خان را در خانمان نِعَم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شدکارا‌های خُردا به اقبال او بزرگ</p></div>
<div class="m2"><p>چون‌ گفت در مصالح احوال خان نَعَم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>باطل زحق جدا شد وکَژّی زراستی</p></div>
<div class="m2"><p>چون‌ گشت حکم قاطع او در میان حکم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یک چند کرد بر لب جیحون شکار شیر</p></div>
<div class="m2"><p>پرداخت شاهوار ز شیر ژیان اَجَم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر بر شکار پیل شدی عزم او درست</p></div>
<div class="m2"><p>بودی ز بلخ تا به در مولتان خیم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تیغش نهنگ‌وار کشیدی به جای پیل</p></div>
<div class="m2"><p>چیپال را ز بیشهٔ هندوستان به دم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای‌ گشته داستان تو تاریخ ملک و دین</p></div>
<div class="m2"><p>گشته به داستان تو همداستان امم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون همت بزرگ تو هرگز نداشتند</p></div>
<div class="m2"><p>کیخسرو و سکندر و نوشیروان همم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گاه هنر نبود ملوک‌ گذشته را</p></div>
<div class="m2"><p>چون شِیمَتِ حمید تو در باستانِ شیَم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مشتاق شد به سیرت و رسم تو روزگار</p></div>
<div class="m2"><p>چون مملکت رسید ز الب‌ارسلان به عم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بهروزی تو کرد و به پیروزی تو خورد</p></div>
<div class="m2"><p>گردون پیر قسمت و بخت جوان قَسَم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>عدل تو برگرفت ز بلغار تا عَدَن‌</p></div>
<div class="m2"><p>از قافله عوارض و از کاروان رقم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>واندر ولایت تو ز تاثیر عدلِ تو</p></div>
<div class="m2"><p>دینار گشت در کف بازارگان دِرَم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>درویش را کف تو توانگر کند همی</p></div>
<div class="m2"><p>کز جودداری آن کف گوهرفشان چویم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بر دوستان درم کرم تو کند نثار</p></div>
<div class="m2"><p>چون ابر نوبهاری بر بوستان دیم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سَم با محبت تو شود در گلو چو نوش</p></div>
<div class="m2"><p>نوش از عداوت تو شود در دهان چو سَم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هر چیز راکه آن به کم ارزد بها بود</p></div>
<div class="m2"><p>ارزد همی مخالف تو رایگان به کم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بر خاک رزمگاه تو هر کس که بگذرد</p></div>
<div class="m2"><p>یابد خبر ز ناله و بیند نشان ز دم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>قومی که از هوای تو برتافتند سر</p></div>
<div class="m2"><p>کشته شدند سر به‌ سر اندر هَوان به غم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ازکشتگان هنوز طیور و سِباع را</p></div>
<div class="m2"><p>پر گوشت است ژاغر و پر استخوان شکم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ای خسروی که با کف رادِ تو گاه مَدح</p></div>
<div class="m2"><p>هرگز نشد ندیم دل مدح خوان نَدَم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بی‌آفرین و شکر تو هرگز به نظم و نثر</p></div>
<div class="m2"><p>مرد حکیم را نرود بر زبان حکم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چون بنده در پرستش تو دل چو تیر داشت</p></div>
<div class="m2"><p>از زخم تیر تو نرسیدش به جان الم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گر بنده را سعادت تو درنیافتی</p></div>
<div class="m2"><p>گشتی وجود بنده هم اندر زمان عدم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>فَرِّ تو دفع کرد و قبول تو سَهل‌ کرد</p></div>
<div class="m2"><p>از مستمند محنت و بر ناتوان سِقَم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خواند همی ملک ملک مهربان تو را</p></div>
<div class="m2"><p>نشگفت اگر کند ملک مهربان کرم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تا باغ را بود به مه فرودین شباب</p></div>
<div class="m2"><p>تا راغ را بود به مه مهرگان هَرَم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>جای نشاط باد بساطت چنانکه هست</p></div>
<div class="m2"><p>دارالسلام جنت و دارالامان حرم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تو مقبل و مظفر و منصور و سرفراز</p></div>
<div class="m2"><p>بر تخت پادشاهی تا جاودان چو جم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>وز بخت نیکخواه تو و بدسگال تو</p></div>
<div class="m2"><p>چون اردشیر خرّم و چون اردوان دژم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بر دودمان خصم تو مریخ تاخته</p></div>
<div class="m2"><p>کیوان پیر توخته زان دودمان نِقَم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بوسیده بخت پایهٔ تخت تو بر زمین</p></div>
<div class="m2"><p>اقبال تو فراخته بر آسمان علم</p></div></div>