---
title: >-
    شمارهٔ ۲۸۷
---
# شمارهٔ ۲۸۷

<div class="b" id="bn1"><div class="m1"><p>مرا خیال تو هر شب دهد امید وصال</p></div>
<div class="m2"><p>خوشا پیام وصال تو بر زبان خیال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میان بیم و امید اندرم‌ که هست مرا</p></div>
<div class="m2"><p>به روز بیم فراق و به شب امید وصال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امید هست ولیکن وفا همی نشود</p></div>
<div class="m2"><p>که هست باغ وصال تو بی‌درخت و نهال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا زباغ وصالت نه بوی ماند و نه رنگ</p></div>
<div class="m2"><p>مرا زداغ فراقت نه هوش ماند و نه هال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وصال آب زلال است پس چراست حرام</p></div>
<div class="m2"><p>فراق بادهٔ تلخ است پس چراست حلال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر به رخصت دهر گزاف کار شدست</p></div>
<div class="m2"><p>حلال بادهٔ تلخ و حرام آب زلال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو را گرامی چون دیده داشتم همه روز</p></div>
<div class="m2"><p>کنار من وطن خویش داشتی همه سال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کنون کنار مرا کرد حادثات فلک</p></div>
<div class="m2"><p>ز دیده خالی و از آب دیده مالامال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تن چو کوه من از ماه توست‌ کاه‌ صفت</p></div>
<div class="m2"><p>قدِ چو ناژِ من از سروِ توست نال‌ْ مثال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که دید هرگز کوهی زماه گشته چو کاه</p></div>
<div class="m2"><p>که دید هرگز ناری زسرو گشته چونال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر این مقام‌ که با من وفا و صحبت را</p></div>
<div class="m2"><p>به حد صدق رسانید و بر مقام مقال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ملازمت‌ کنمی گر نترسمی ز مَلام</p></div>
<div class="m2"><p>مواظبت کنمی کر نترسمی ز ملال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو راه یافت به‌ خورشید صحبت تو کسوف</p></div>
<div class="m2"><p>زوال‌ کرد زمن تا شدم به شکل هلال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کنون شکایت خورشید با زوال و کسوف</p></div>
<div class="m2"><p>کنم به مجلس خورشید بی‌کسوف و زوال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یگانه فخر خراسان بهاء دین هدی</p></div>
<div class="m2"><p>که زین ملک و ملوک است و قبلهٔ اقبال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ولی دولت عالی ابوعلی ختنی</p></div>
<div class="m2"><p>که هست شمس معالی بر آسمان جلال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جهان و خلق جهان را لقا و خدمت او</p></div>
<div class="m2"><p>چو سعد اکبر و اصغر مبارک است به فال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>درخت طوبی‌ گیرد به زیر سایهٔ خویش</p></div>
<div class="m2"><p>اگر گشاده کند باز دولتش پر و بال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر مشابه مردان کفایت و هنرست</p></div>
<div class="m2"><p>بدین دو چیز مر او را ز خلق نیست همال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کفایت و هنرش در همه جهان سمرست</p></div>
<div class="m2"><p>چو حسن یوسف یعقوب و رسم رستم زال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اگر محامد او را قضا شود وزان</p></div>
<div class="m2"><p>وگر مکارم او را قدر شود کیال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هزاران‌ گردون آنرا نه بس بود میزان</p></div>
<div class="m2"><p>هزار دریا این را نه بس بود مکیال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ایا ستوده تو را دولت و فزوده تو را</p></div>
<div class="m2"><p>خدای عرش جلال و خدایگان‌ اجلال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زآدمی تو ولیکن بر او شرف داری</p></div>
<div class="m2"><p>که تو ز نور لطیفی و آدم از صلصال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز مشکلات هنر گر خرد سوال کند</p></div>
<div class="m2"><p>به جز تو کس ندهد در جهان جواب سوال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به زیر پای تو زیبد که شیر شادروان</p></div>
<div class="m2"><p>ز کبر بر سر شیر فلک زند دنبال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زهمت تو همی روزگار رشک برد</p></div>
<div class="m2"><p>که همت تو معیل است و روزگار عیال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز بهر آنکه به حلم تو نسبتی دارد</p></div>
<div class="m2"><p>مکان منفعت و کان گوهرست جبال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اگر ز حلم تو باشد جبال را مددی</p></div>
<div class="m2"><p>بود زمین همه اوقات ایمن از زلزال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کسی که باد خلاف تو دارد اندر سر</p></div>
<div class="m2"><p>رسد به خانهٔ آن ژاژ باد استیصال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تویی خلیفهٔ بغداد را یمین و معین</p></div>
<div class="m2"><p>که دین و داد تورا هست بر یمین و شمال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از آن قبل به لقای تو آرزومندست</p></div>
<div class="m2"><p>که از لقای تو خیزد سعادت و اقبال</p></div></div>