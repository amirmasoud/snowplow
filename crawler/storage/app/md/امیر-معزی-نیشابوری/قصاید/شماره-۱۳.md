---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>یافتی بر خوان اگر جویی رضای مرتضا</p></div>
<div class="m2"><p>لا فَتی اِلّا علی بر خوا‌نْد هر دم مُصطفا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور همی خواهی ‌که گردی ایمن از هَلْ‌ منْ‌ مَزید</p></div>
<div class="m2"><p>شرح یُوفون و یُخافون یاد کن از هَل أتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن‌که داماد نبی بود و وصی بود و ولی</p></div>
<div class="m2"><p>در موالاتش وصیت نیست شرط اولیا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر علی بعد از سنین بنشست او را زان چه نقص</p></div>
<div class="m2"><p>هیچ نقصان نامد‌ش بعد از سنین اندر سنا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرتضی را چه زیان ‌گر بود بَعْدَ‌الاختیار</p></div>
<div class="m2"><p>مصطفی را چه زیان‌ گر بود بعد‌الانبیا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حب یاران پیمبر فرض باشد بی‌خلاف</p></div>
<div class="m2"><p>لیکن از بهر قرابت هست حیدر مقتدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بود با زهرا و حیدر حجت پیغمبری</p></div>
<div class="m2"><p>لاجرم بنشاند پیغمبر سزایی با سزا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن که چون آمد به دستش ذوالفقار جانشکار</p></div>
<div class="m2"><p>گشت معجز درکفش چون درکف موسی عصا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آمد آواز منادی لافتی الا علی</p></div>
<div class="m2"><p>وانگهی لا سیف الا ذوالفقار آمد ندا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وان دو فرزند عزیزش چون ‌حسین و چون حسن</p></div>
<div class="m2"><p>هر دو اندر کعبهٔ جود و کرم رکن و صفا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن یکی کشته به زهر بددلان در اختفاء</p></div>
<div class="m2"><p>وان دگر گشته پی دفع البَلایا در بلا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن یکی را جان ز تن‌ گشته جدا اندر حجاز</p></div>
<div class="m2"><p>وان دگر را سر جداگشته ز تن درکربلا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن ‌که دادی بوسه بر روی و قَفای او رسول</p></div>
<div class="m2"><p>گرد بر رویش نشست و شمر ملعون در قفا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وانکه حیدر گیسوان او نهادی بر دو چشم</p></div>
<div class="m2"><p>چشم او در آب غرق وگیسوان اندر دما</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>روز محشر داد بستاند خدا از قاتلانش</p></div>
<div class="m2"><p>تو بده داد و مباش از حب مقتولان جدا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خدمت آن کن که فخر عِتْر‌َت پیغمبرست</p></div>
<div class="m2"><p>سید سادات ذواه‌لفخرین و تاج اه‌لاه‌صفیا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قبلهٔ اقبال بوطاهر مُطَهّر بِن‌ علی</p></div>
<div class="m2"><p>الامام بن الامام المرتضی بن المرتضا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هست هرکس در سیاست مُفْتَخَر واو مفتخر</p></div>
<div class="m2"><p>هست هر کس در ریاست مُقْتَدی واو مقتدا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>طالعش را هر زمان اقبال گوید السلام</p></div>
<div class="m2"><p>طلعتش را هر زمان خورشید گوید مرحبا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نیست اندر سیرت و رای و رسوم او خَلَل</p></div>
<div class="m2"><p>نیست اندر خاطر و خط و خطاب او خطا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در همایون روزگار او رعایا ایمنند</p></div>
<div class="m2"><p>روز و شب از حادثات روزگار پر جفا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پیش حِلمش ذرّهٔ صغری بود میخ زمین</p></div>
<div class="m2"><p>پیش رویش عالم سُفلی بود قُطبِ سَما</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فضل او بی‌غایت است و سِرِّ او بی‌غایله</p></div>
<div class="m2"><p>حال او بی‌منت است و جود او بی‌منتها</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سائلان را بی‌تغافل زود فرماید جواب</p></div>
<div class="m2"><p>شاعران را بی‌نسیه‌ نقد فرماید عطا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بخشش مال است‌ کار سید عالم همی</p></div>
<div class="m2"><p>کوشش خعرست شغل مهتر فرمانروا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مال او را نصرت دین است در دنیا بدل</p></div>
<div class="m2"><p>خیر او را جنت عَدن است در عُقبی جزا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کردگار او را دهد فردا ثواب بی‌حساب</p></div>
<div class="m2"><p>تا که امروز او همی بخشد عطای بی‌ریا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای متابع‌ گشته فرمان تو را حکم قدر</p></div>
<div class="m2"><p>ای موافق گشته تدبیر تورا امر قضا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مهتری چون‌گوهرست ورای تو او را چو رنگ</p></div>
<div class="m2"><p>گوهری کان را نباشد رنگ باشد بی‌بها</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کبریای محض بی‌کبر و ریا دادت خدای</p></div>
<div class="m2"><p>هست مستغنی زکِبر آن کس که دارد کِبریا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اختیار خاندان دین تویی وقت هنر</p></div>
<div class="m2"><p>افتخار دودمان دولتی وقت سَخا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پادشاه دل به هر تدبیر اگر باشد خرد</p></div>
<div class="m2"><p>مر تو را زیبد اگر شاهی کنی بر پادشا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ای همیشه الفت تو دفع آفت را اساس</p></div>
<div class="m2"><p>ای همیشه همّت تو درد و مِحنت را دوا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>طالع میمون بود پیش صلات تو صلات</p></div>
<div class="m2"><p>نعمت قارون بود نزد هَبات تو هبا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هرکه بر جاهت‌کمین سازد ز تن سازدکمان</p></div>
<div class="m2"><p>هرکه در پیشت رهی باشد ز غم باشد رها</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>روز و شب خوان نکوخواه تو باشد خرمی</p></div>
<div class="m2"><p>سال و مه بر خون بدخواه توگردد آسیا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بر فلک کردست دولت صُفّهٔ آن سرفراز</p></div>
<div class="m2"><p>بر زحل کردست گردون گردن این گردنا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>درگه تو هست بنیان شرف را قاعده</p></div>
<div class="m2"><p>مجلس تو هست حَملان‌ کَرَم را کیمیا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خار باغ توست در دست حکیمان سرخ‌گل</p></div>
<div class="m2"><p>خاک پای توست بر چشم کریمان‌، توتیا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مهتراگر عارضی بر عَرض تو سایه فکند</p></div>
<div class="m2"><p>بدر را گه‌گه پدید آید خُسوف اندر ضیا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>عارض از عرض تو زایل‌ گشت چون شد متصل</p></div>
<div class="m2"><p>از خدای ما اجابت وز مسلمانان دعا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خدمت تو مخلصانه کرد برهانی به دل</p></div>
<div class="m2"><p>یافت از اقبال تو هم ملتجا هم مرتجا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کرد خواهم خدمت تو مخلصانه چون پدر</p></div>
<div class="m2"><p>تا به اقبال توگردم مقبل اندر مبتدا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خاطر من چون هوا و مدح تو چون آتش است</p></div>
<div class="m2"><p>گر بود آتش مصعد سال و ماه اندر هوا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تا شود برگ درختان کَهرُبا رنگ از خزان</p></div>
<div class="m2"><p>تا شود شاخ درختان مشتری سان از صبا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>طلعت مداح تو بادا به فر مشتری</p></div>
<div class="m2"><p>جهرهٔ بدخواه تو بادا به رنگ کهربا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>در سرای دین و دولت دایمی بادت درنگ</p></div>
<div class="m2"><p>بر سریر سود و سُؤدَد، سرمدی بادت بقا</p></div></div>