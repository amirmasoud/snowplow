---
title: >-
    شمارهٔ ۴۲۷
---
# شمارهٔ ۴۲۷

<div class="b" id="bn1"><div class="m1"><p>ای تخت و گاه پادشهی جایگاه تو</p></div>
<div class="m2"><p>آراسته است مملکت از تخت و گاه تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هستی ندیم شاهی و دولت ندیم تو</p></div>
<div class="m2"><p>هستی پناه عالم و ایزد پناه تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فخر همه شهانی و کس نیست فخر تو</p></div>
<div class="m2"><p>شاه همه جهانی و کس نیست شاه تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چاه است‌ کین تو که همه زهر دارد آب</p></div>
<div class="m2"><p>وافتاده دشمنان تو در قعر چاه تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ماهی که زیر لشکر او سایه‌ای بود</p></div>
<div class="m2"><p>بنگرکه بر سر عَلَم توست ماه تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آفتاب باز نداند تو را کسی</p></div>
<div class="m2"><p>گر دارد آفتاب قبا و کلاه تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر گه‌ که در شکار و سفر باشی ای ملک</p></div>
<div class="m2"><p>آب رونده‌ گَرد بشوید ز راه تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ور آب کم بود سپه و لشکر تو را</p></div>
<div class="m2"><p>ابر اید و نثارکند بر سپاه تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا بخت جاودان به تو دادست فرّ و جاه</p></div>
<div class="m2"><p>ا‌این هر دوان‌ا بهشت‌ کند فرّ و جاه تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از دوستی که بخت تو دارد تو را همی</p></div>
<div class="m2"><p>خواهد که در بهشت بود جایگاه تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاها دل تو هست به هروقت نیکخواه</p></div>
<div class="m2"><p>جاوید شاد باد دل نیکخواه تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا سال و ماه و روز و شب است اندرین جهان</p></div>
<div class="m2"><p>فرخنده باد روز و شب و سال و ماه تو</p></div></div>