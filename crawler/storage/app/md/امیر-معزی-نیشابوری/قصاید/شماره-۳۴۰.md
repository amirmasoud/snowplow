---
title: >-
    شمارهٔ ۳۴۰
---
# شمارهٔ ۳۴۰

<div class="b" id="bn1"><div class="m1"><p>چون برآرم به زبان نام خداوند جهان</p></div>
<div class="m2"><p>تن من جمله شود گوش و دلم جمله زبان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچه در دهر زبان است مرا بایستی</p></div>
<div class="m2"><p>تا ثنا گفتمی از بهر خداوند جهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاه آفاق ملک شاه که در طاعت او</p></div>
<div class="m2"><p>ملکان حمل‌پذیرند و شهان بسته میان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شهریاری که به‌ روزی همه کس را ز خدای</p></div>
<div class="m2"><p>خاطر پاک و دل روشن اوکرد ضَمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ایزد اندر دل او دفتر تقدیر نهاد</p></div>
<div class="m2"><p>هرچه خواهد بود از رفتن تقدیر چنان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در جهانداری و سلطانی از اوگشت یقین</p></div>
<div class="m2"><p>آن هنرها که نبردست کس از خلق‌گمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چندگویند ز شهنامه سخن‌های دروغ</p></div>
<div class="m2"><p>چند خوانند هنرهای فلان و بهمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سیرت شاه عیان است و دگر جمله خبر</p></div>
<div class="m2"><p>از خبر یاد نیارند کجا هست عیان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اندر آفاق کرا بود زشاهان قدیم</p></div>
<div class="m2"><p>این چنین دولت پیروز و چنین بخت جوان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که‌گرفت از ملکان با ظفر و نصرت و فتح</p></div>
<div class="m2"><p>شرق تا غرب زمین را ز کران تا به‌ کران</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>راه شش ماهه به‌ یک ماه ز شاهان که‌ گذاشت</p></div>
<div class="m2"><p>با هزاران سپهِ تیغْ زنِ قلعهْ‌ستان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همه‌کیوانْ دل و مهْ طلعت و بهرامْ حُسام</p></div>
<div class="m2"><p>صاعقهْ تیر و فلکْ مرکب و سیّارهْ سنان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه زین تخت و از این‌گاه بیفراخته سر</p></div>
<div class="m2"><p>همه زین بخت و از این شاه بیفروخته جان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کس ندیدست چنین تخت و چنین گاه به‌خواب</p></div>
<div class="m2"><p>کس ندادست چنین بخت و چنین شاه نشان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هرکجا شاه جوانبخت روان کرد سپاه</p></div>
<div class="m2"><p>از تن دشمن بدبخت روان گشت روان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بود در مشرق و در مغرب از او بود خروش</p></div>
<div class="m2"><p>هست در مشرق و در مغرب از او هست فغان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شادباش ای به حقیقت ملک روی زمین</p></div>
<div class="m2"><p>دیر زی ای به سزا پادشه ملک زمان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هرکه او بر طمع سود کند با تو خلاف</p></div>
<div class="m2"><p>آخرالْاَمر کند جان و تن خویش زیان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن‌که با تیر وکمان‌کرد همی قصد نبرد</p></div>
<div class="m2"><p>قد چون تیر وی از بیم توگشتست کمان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خستهٔ بار گران است ز خوی بد خو‌یش</p></div>
<div class="m2"><p>نشنیدست مگر خوی بد و بارگران</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خصم تو هست چو فرعون و تویی چون موسی</p></div>
<div class="m2"><p>رای تو چون ید بیضا و حُسامت ثُعبان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>قلعه بر خصم تو مانندهٔ زندان‌ گشته است</p></div>
<div class="m2"><p>چه خطر باشد آن را که بود در زندان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر بداندیش تویی دانش و بی‌سنگ و درنگ</p></div>
<div class="m2"><p>دست در سنگ زد و روی ز توکرد نهان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حکم و فرمان تو مانند قضا و قَدَرست</p></div>
<div class="m2"><p>زقضا و ز قَدَر روی نهفتن نتوان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دشمنانَتْ همه رفتند و بماندست یکی</p></div>
<div class="m2"><p>وان یکی نیز چنان دان‌که شود چون دگران</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ملکا تیغ تو و جام تو دارند دو خون</p></div>
<div class="m2"><p>به‌ گه بزم تو این است و گه رزم تو آن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هست بر تیغ تو چون رزم‌کنی خون عدو</p></div>
<div class="m2"><p>هست درجام تو چون بزم کنی خون‌رزان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بدسگالان را تیغ تو چو زهر افعی است</p></div>
<div class="m2"><p>نیک‌خواهان را مهر تو چو آب حیوان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>داشت نوشِرْ‌وان بر درگه خود سلسله‌ای</p></div>
<div class="m2"><p>تا دلیلی بود از عدل و نشانی ز امان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بر جهان وقت امان دادن و گستردن عدل</p></div>
<div class="m2"><p>هست یک حکم تو صد سلسلهٔ نوشر‌وان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا شود راغ چو زنگار به هنگام بهار</p></div>
<div class="m2"><p>تا شود باغ چو دینار به هنگام خزان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>باد اقبال تو بیوسته و بخت تو بلند</p></div>
<div class="m2"><p>باد فرمان تو پاینده و حکم تو روان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا همی راند کار همه‌کس حکم ازل</p></div>
<div class="m2"><p>همچنین نوش‌خور وکام دل خویش بران</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در دل افروزی و در شادی و جان‌افروزی</p></div>
<div class="m2"><p>در جهانداری و در شادی جاوید بمان</p></div></div>