---
title: >-
    شمارهٔ ۱۷۴
---
# شمارهٔ ۱۷۴

<div class="b" id="bn1"><div class="m1"><p>سوگند خورده‌ام به سر زلف آن پسر</p></div>
<div class="m2"><p>کز مهر او نتابم و عهدش برم بسر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوگند من شکسته نشد گرچه روزگار</p></div>
<div class="m2"><p>برهم شکست خرد سر زلف آن پسر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرگز ندیده‌اند و نبینند در جهان</p></div>
<div class="m2"><p>از قد و زلف و جشم و لب او بدیع‌تر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیباسلب صنوبر و خورشید مشک‌پوش</p></div>
<div class="m2"><p>بادام‌ شکل نرگس و بیجاده گون شکر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زلفش مشعبدی است‌ که پیش قمر همی</p></div>
<div class="m2"><p>بندد ز ابر پرده و سازد ز شب سپر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرچند پردهٔ قمر از ابر دیده ام</p></div>
<div class="m2"><p>نشنیده‌ام سپر ز شب تیره بر قمر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مویم چو سیم و روی چو زر شد ز عشق آن</p></div>
<div class="m2"><p>کز سیم و زر ناب میان دارد و کمر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا زر او بدیدم شد موی من چو سیم</p></div>
<div class="m2"><p>تا سیم او بدیدم شد موی من چو زر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای دلبری که از پی شور و بلا توراست</p></div>
<div class="m2"><p>بر ارغوان بنفشه و در پرنیان حجر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هم ترک حور زادی و هم حور سرو قد</p></div>
<div class="m2"><p>هم سرو ماهرویی و هم ماه سیمبر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا در دل تو آتش بیداد برفروخت</p></div>
<div class="m2"><p>از تف او شدست مرا تافته جگر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدادگر مباش‌ که فردا کنم نفیر</p></div>
<div class="m2"><p>از دست تو به مجلس دستور دادگر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زین ملوک و صدرِ وزیران قوامِ دین</p></div>
<div class="m2"><p>بوالقاسم آفتاب کرم قبلهٔ هنر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صدری که نام اوست رسیده به شرق و غرب</p></div>
<div class="m2"><p>بدری که نور اوست رسیده به بحر و بر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر ذات عقل را زلطافت بود بدن</p></div>
<div class="m2"><p>ور باغ فضل را زکفایت بود شجر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>باشد در آن بدن ز مقامات او روان</p></div>
<div class="m2"><p>باشد بر آن شجر ز مقالات او ثمر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در شیب تازیانه و در نوک‌ کلک او</p></div>
<div class="m2"><p>هر ساعتی به چشم تعجب همی نگر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کاندر نفاذ و دفع ستم هر دو نایبند</p></div>
<div class="m2"><p>از ذوالفقار حیدر و از دِرّهٔ عمر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ماند به امن و عافیت اخلاص و مهر او</p></div>
<div class="m2"><p>زیرا که زین دو چیز مهیاست خواب و خَور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ماند به چرخ اول و رابع دل و کَفَش</p></div>
<div class="m2"><p>کاندر میان هر دو مهیاست کام و گر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر کارها روان ز قضا و قدر بود</p></div>
<div class="m2"><p>دو شاخ کلک او به قضا ماند و قدر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر خصم ازو حذر نتواند شگفت نیست</p></div>
<div class="m2"><p>بیچاره از قضا و قدر چون کند سر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هرچند شاه و خسرو مرغان بود عقاب</p></div>
<div class="m2"><p>سیمرغ را گرفت نیارد به زیر پر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای از کرم چو برمکیان در عرب مثل</p></div>
<div class="m2"><p>وی از هنر چو بلعمیان در عجم سمر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جز تو در آن گروه که هستند در عراق</p></div>
<div class="m2"><p>هرگز که کرد سوی خراسان چنین سفر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بر تو سفر مبارک و خوش بود چون جنان</p></div>
<div class="m2"><p>هر چند گفته اند سفر هست چون سقر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>امروز در عراق و خراسان دو خسروند</p></div>
<div class="m2"><p>آن شهریار خاور و این شاه باختر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از رای و از کفایت تو هر دو شاکرند</p></div>
<div class="m2"><p>آن خواندت برادر و این خواندت پدر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مقصود اگر موافقت عهد بود و مهد</p></div>
<div class="m2"><p>محمود شاه را ز شهنشاه دادگر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>امروز عهد و مهد به جهد تو حاصل است</p></div>
<div class="m2"><p>چون هر دو حاصل است چه باید همی دگر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زین عهد محکم است به هر کشوری نشان</p></div>
<div class="m2"><p>زین مهد فرّخ است به هر بقعتی اثر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>این عهد و مهد را به سعادت بود نثار</p></div>
<div class="m2"><p>از چرخها ستاره و از بحرها گهر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا کار مهد و شغل ولی‌عهد پادشاه</p></div>
<div class="m2"><p>از جاه تو گرفت جمال و جلال و فر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نام‌آوران به درگهت از بهر تهنیت</p></div>
<div class="m2"><p>دایم همی رسند نفر از پی نفر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>فردا که در عراق نشینی به کام دل</p></div>
<div class="m2"><p>بر بالش وزارت با حشمت و خطر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از تیغ شاه تیره دلان را بود نهیب</p></div>
<div class="m2"><p>وز خامهٔ تو خیره‌سران را بود خطر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بسیار دل به امن تو صافی شود ز شور</p></div>
<div class="m2"><p>بسیار سر به امر تو خالی شود ز شر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>باغ مراد را بود اقبال تو درخت</p></div>
<div class="m2"><p>کشت امید را بود احسان تو مطر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در نامه‌ها نوشته شود آیت فتوح</p></div>
<div class="m2"><p>در شهرها کشیده شود رایت ظفر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ای گفته شُکر تو همه آزادگان به جان</p></div>
<div class="m2"><p>ای کرده مدح تو همه فرزانگان زبر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>طبع مرا ز نظم مدیح تو چاره نیست</p></div>
<div class="m2"><p>چونانکه دیده را نبود چاره از بصر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>در روح من ز دوستی توست تازگی</p></div>
<div class="m2"><p>چونانکه تازگی بود از روح در صور</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تشریف پادشاه تو حاصل شود مرا</p></div>
<div class="m2"><p>گر تو به چشم سعی به کارم کنی نظر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ور در عنایت تو بود غایت کمال</p></div>
<div class="m2"><p>کامل بود عطا و سخن گشت مختصر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تا درج را غرَر بود از نکته های خوب</p></div>
<div class="m2"><p>تا د‌ُرج را ز قطرهٔ باران بود دُرر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>در درج محمدت درر ار سی ث تح باد</p></div>
<div class="m2"><p>در دَ‌رج مدح باد ز اوصاف تو غُرَر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>فرخنده هفت چیز تو دایم گشاده باد</p></div>
<div class="m2"><p>طبع و دل و زبان و رخ و دست و کار و در</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>راضی ز مهربانی تو شاه نامدار</p></div>
<div class="m2"><p>شاکر ز نیک عهدی تو مهد نامور</p></div></div>