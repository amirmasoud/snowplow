---
title: >-
    شمارهٔ ۴۱۷
---
# شمارهٔ ۴۱۷

<div class="b" id="bn1"><div class="m1"><p>باد میمون و مبارک بر شه روی زمین</p></div>
<div class="m2"><p>عید و دیدار امام‌الحق امیرالمومنین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر شریعت راستی بفز‌ود از این معنی‌ که بود</p></div>
<div class="m2"><p>با امام راستان دیدار شاه راستین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اتفاق هر دو عالی‌ کرد قدر تاج و تخت</p></div>
<div class="m2"><p>اتصال هر دو روشن‌کرد چشم ملک و دین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین مبارک اتفاق و زین همایون اتصال</p></div>
<div class="m2"><p>قائم و الب ارسلان شادند در خلد برین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از شهنشه ملک باقی ویژه شد تا روز ‌حشر</p></div>
<div class="m2"><p>وز خلیفه دین تازی تازه شد تا روز دین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ملک و دولت را به هر دو کرد باید تهنیت</p></div>
<div class="m2"><p>دین و دنیا را به هر دو گفت باید آفرین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا جهان باقی بود پاینده و عالی بود</p></div>
<div class="m2"><p>دودهٔ عباس از آن و گوهر سلجوق از این</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای شهنشاهی‌ که رای و رایت و روی تو را</p></div>
<div class="m2"><p>آفرین و رحمت است از ایزد خلد آفرین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ملک عالم را تو داری یک به یک زیر علم</p></div>
<div class="m2"><p>گنج‌ گیتی را تو داری سر به سر زیر نگین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شکرکردی گر به ‌هنگام تو بودی معتصم</p></div>
<div class="m2"><p>فخرکردی ‌گر در ایام تو بودی مستعین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا به شرع اندر یمین ا‌لمقتدی بالله تویی</p></div>
<div class="m2"><p>یسر داری بر یسار و یمن داری بر یمین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دولت عباسیان بودی به هفتم آسمان</p></div>
<div class="m2"><p>تخت و بخت رومیان بردی فرو هفتم زمین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لاجرم در دین پیغمبر تو را حاصل شده است</p></div>
<div class="m2"><p>آنچه ‌گفت ایزد تعالی ‌نِعم اَجرَ العاملین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو شریعت را پناهی و تو را دولت پناه</p></div>
<div class="m2"><p>تو خلیفت‌ را معینی و تو را ایزد معین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با شجاعت هم نژادی و با سخاوت هم نسب</p></div>
<div class="m2"><p>با جلالت هم تباری با سعادت هم نشین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از هنرهای تو تاریخ فتوح و نصرت است</p></div>
<div class="m2"><p>هم به ‌روم و هم به ‌مصر و هم به هند و هم به چین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا بود گیتی به عدل تو سریر ملک را</p></div>
<div class="m2"><p>تهنیت‌گویند هر روزی کرام‌الکاتبین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تاکه زین و تخت شاهی در جهان آمد بدید</p></div>
<div class="m2"><p>هیچ کس ننشست راحت‌تر چو تو بر تخت وزین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا نجوم اندر بروج است و بروج اندر فلک</p></div>
<div class="m2"><p>تا فصول اندر شهور است و شهور اندر سنین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر مکان و هر مکین در خطهٔ فرمانت باد</p></div>
<div class="m2"><p>تو همیشه در مکان شاهی و شادی مکین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>روشن از رای تو گیتی همچو چرخ از آفتاب</p></div>
<div class="m2"><p>خرم از عدل تو عالم همچو باغ از فرودین</p></div></div>