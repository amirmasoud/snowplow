---
title: >-
    شمارهٔ ۷۶
---
# شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>شاهی که عدل و جود همه روزگار اوست</p></div>
<div class="m2"><p>تاریخ نصرت و ظفر از روزگار اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قفل غم و کلید طرب روز بزم اوست</p></div>
<div class="m2"><p>اثبات عدل و نفی ستم روز بار اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>والی به حد شام یکی پهلوان اوست</p></div>
<div class="m2"><p>عامل به حد روم یکی کاردار اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>احسان او نگار گر ملک شد مگر</p></div>
<div class="m2"><p>زیرا که شرق و غرب همه پرنگار اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نندیشد از پناه و حِصار مخالفان</p></div>
<div class="m2"><p>تا عصمت خدای پناه و حصار اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از کار زار او اجل اندر رسد به خصم</p></div>
<div class="m2"><p>گویی اَجَل مقدمهٔ کار زار اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شمشیر آبدارش شیری است از قیاس</p></div>
<div class="m2"><p>شیری که مغز اَهْرِمنان مرغزار اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هست او به شاهی از همه آفاق اختیار</p></div>
<div class="m2"><p>تا قهر کفر و نصرت دین اختیار اوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آموزگار خلق هنرهای او بس است</p></div>
<div class="m2"><p>زیرا که در هنر خرد آموزگار اوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر شاه راکه بخت بلندست و کامکار</p></div>
<div class="m2"><p>از دولت بلند و دل کامکار اوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرگنج و خواسته که نهادست در زمین</p></div>
<div class="m2"><p>از بهر تَفْرَ‌قه همه در انتظار اوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر یک مکان مخالف او را قرار نیست</p></div>
<div class="m2"><p>تا بر سریر ملک ولایت قرار اوست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بغداد دار ملک شد و بزم نوبهار</p></div>
<div class="m2"><p>آرایش و شکفتن باغ از بهار اوست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اندر خورش نثار چه آرند بر زمین</p></div>
<div class="m2"><p>کز آسمان سعادت کلی نثار اوست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بشکفت نو درختی از باغ دولتش</p></div>
<div class="m2"><p>فرخ یکی درخت که اقبال یار اوست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ملک و شعار دولت او پایدار باد</p></div>
<div class="m2"><p>کاشعار شاعران جهان در شعار اوست</p></div></div>