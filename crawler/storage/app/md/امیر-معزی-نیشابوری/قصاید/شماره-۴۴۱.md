---
title: >-
    شمارهٔ ۴۴۱
---
# شمارهٔ ۴۴۱

<div class="b" id="bn1"><div class="m1"><p>آن بت مجلس فروز امروز اگر با ماستی</p></div>
<div class="m2"><p>مجلس ما خُرّمَستی کار ما زیباستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خفته و مست است و پنداری که از ما فارغ است</p></div>
<div class="m2"><p>عیش ما خوش نیست بی او کاشکی با ماستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه می خوردست و از مستی به خواب اندر شدست</p></div>
<div class="m2"><p>هم توانستی بر ما آمدن‌ گر خواستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بدو پیداستی از مهربانی یک نشان</p></div>
<div class="m2"><p>صد نشان از خرمی بر روی ما پیداستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نکردستی دل ما دی به وصل او نشاط</p></div>
<div class="m2"><p>در غم هجران او امروز ناپرواستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دی ازو در وصل ما را وعدهٔ امروز بود</p></div>
<div class="m2"><p>کاشکی امروز ما را وعدهٔ فرداستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نمودستی فروغ جِبهت و رخسار خویش</p></div>
<div class="m2"><p>بزم ما بر مستری و زهرهٔ زهراستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وز خم زلف و گهرهای کلاهش روز و شب</p></div>
<div class="m2"><p>مشتری در عقربستی زهره در جوزاستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وصف او هستی به معنی راست چون وصف پری</p></div>
<div class="m2"><p>کر پری راگِرد سوسن عنبر ساراستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نَعت او هستی به برهان راست چون نعت صنم</p></div>
<div class="m2"><p>گر صنم راگرد مرجان لولو لالاستی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بی‌رقیبی آفتاب اندر فلک تنها رود</p></div>
<div class="m2"><p>آفتابی دیگرستی کاشکی تنهاستی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر فلک نتواندی تنها گذشتن آفتاب</p></div>
<div class="m2"><p>گر نه زیر سایهٔ تخت شه دنیاستی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>افسر شاهان ملک‌سنجر سرِ سلجوقیان</p></div>
<div class="m2"><p>آن‌گهر بخشی‌که‌گویی دست او دریاستی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر نه آنستی که جوید هرکس از دریا گهر</p></div>
<div class="m2"><p>بوسه‌دادن دست او هرگز کرا یاراستی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر به نام بخت منشوری فرستادی خدای</p></div>
<div class="m2"><p>برسر منشور او نام ملک طغراستی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ماه اگر هستی برابر با مه منجوق شاه</p></div>
<div class="m2"><p>چرخ‌کیوان زیر و چرخ ماه بربالاستی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وز فزونستی سپهر از وصف و وهم فیلسوف</p></div>
<div class="m2"><p>قدر او همتای قدر شاه بی‌همتاستی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دولت عالیش اگر هستی درختی سرفراز</p></div>
<div class="m2"><p>بیخ و شاخ او به جابلسا و جابلقاستی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>باز و شاهین گر ز دست او شوندی سوی صید</p></div>
<div class="m2"><p>مخلب‌ و منقارشان در قبهٔ خضراستی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ور سر اعدای او در خاک پنهان نیستی</p></div>
<div class="m2"><p>در خم چوگان او گوی از سر اعداستی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پیکر پیل است اسبش را ولیکن ‌گاه جنگ</p></div>
<div class="m2"><p>پیل ازو بگریزدی‌گر در صف هیجاستی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون عرق گیرد تو گویی سیل دروادیستی</p></div>
<div class="m2"><p>چون سَبَق جوید تو گویی باد در صحراستی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جون نشنید شاه برپشتش توگویی بر نهنگ</p></div>
<div class="m2"><p>چیره شد شیری که اندر چنگش اژدرهاستی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای جهانداری که گر خورشید عقلت نیستی</p></div>
<div class="m2"><p>روز خلق از تیرگی همچون شب یلداستی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ور به اطراف ممالک نیستی فرمان تو</p></div>
<div class="m2"><p>هر طرف را آفتی از غارت و غوغاستی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>امن و آرام جهان از جنبش شمشیر توست</p></div>
<div class="m2"><p>ور جز اینستی جهان آشفته و شیداستی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در جهان گر نیستی سهم سر شمشیر تو</p></div>
<div class="m2"><p>ای بسا سر کاندرو بیهوده و سوداستی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>روزکین چون نرد نصرت باختی با دشمنان</p></div>
<div class="m2"><p>صد نَدَب بردی که‌ گفتی هر ندب عذراستی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر به رجعت بازگشتندی ملوک باستان</p></div>
<div class="m2"><p>خواندی اختر مدیحت‌گر بلند آراستی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گیردی گردون رکابت گر قوی بازوستی</p></div>
<div class="m2"><p>خواندی اختر مدیحت‌گر بلند آواستی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عقل توکل است وگر پیداستی اجزای او</p></div>
<div class="m2"><p>آسمان هفتمین یک جزو از آن اجزاستی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گر نبودی از تف خشم تو آتش را نهیب</p></div>
<div class="m2"><p>مسکن آتش نه اندر آهن و خاراستی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ور ز باران نوالت بهره‌مندستی زمین</p></div>
<div class="m2"><p>حنظل او شکرستی خار او خرماستی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خَلُّخ و یغما تو داری ور تو را هستی مراد</p></div>
<div class="m2"><p>قیروان و روم همچون خلخ و یغماستی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از سر پیکان تو وز خنجر ترکان تو</p></div>
<div class="m2"><p>قیروان بی‌مشرکستی روم بی‌ترساستی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گر ز دست تو به چین والاستی فغفور چین</p></div>
<div class="m2"><p>در قبول دین تازی دولتش والاستی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ور فتادستی فروغ تیغ تو بر رآی هند</p></div>
<div class="m2"><p>جشم سرش در هدی چون چشم سر بیناستی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خسروا معلوم رای تو شدستی تاکنون</p></div>
<div class="m2"><p>گر به عالم چون معزی شاعر داناستی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گر نبودی ناتوان بودی به عالی مجلست</p></div>
<div class="m2"><p>وز قبول مجلس تو کار خویش آراستی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شد معزی در فراق خدمتت پیر و کهن</p></div>
<div class="m2"><p>گر مقیمستی به خدمت تازه و برناستی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ور به صهبا مایل استی طبع او از دیرباز</p></div>
<div class="m2"><p>بیش تخت تو به دستش ساغر صهباستی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>حاضر آمد تا نماید خاطرش در پیش تو</p></div>
<div class="m2"><p>شعرهای نوکه‌گویی حله و دیباستی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تا مثال اختران بر آسمان‌ گویی مگر</p></div>
<div class="m2"><p>در مکنون ریخته بر تختهٔ میناستی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>آسمان مختار بادا تا تو را مولی شود</p></div>
<div class="m2"><p>زانکه ‌گر مختار بودی خود تو را مولاستی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>باد چون ثعبان موسی تیغ تو هنگام رزم</p></div>
<div class="m2"><p>رای تو روشن‌ که پنداری ید بیضاستی</p></div></div>