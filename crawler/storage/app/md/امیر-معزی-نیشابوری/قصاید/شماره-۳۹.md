---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>خدای عرش گواه و زمانه آگاه است</p></div>
<div class="m2"><p>که دین عزیز به سلطان دین ملکشاه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شهی که خاطر پاک و ضمیر روشن او</p></div>
<div class="m2"><p>ز هر هنرکه خدا آفریده آگاه است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر به افسر و گاه است فخر هر ملکی</p></div>
<div class="m2"><p>به فرّ طلعت او فخرِ افسر و گاه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ملوک روی سوی درگهش نهادستند</p></div>
<div class="m2"><p>که قبله‌گاه ملوک خجسته درگاه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فتوح او به عدد هست اگر حساب کنند</p></div>
<div class="m2"><p>فزون از آن‌که حروف سخن در افّواه است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ایا شهی‌که تورا در صفات پادشهی</p></div>
<div class="m2"><p>کمال صد مَلِک است و جمال صد شاه است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز خدمت تو شهان را سعادت و شرف است</p></div>
<div class="m2"><p>ز طاعت تو جهان را جلالت و جاه است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زگَرد موکب تو روی ماه پرخاک است</p></div>
<div class="m2"><p>ز نعل مرکب تو روی خاک پُرماه است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر ستاره پرستش کند تورا وقت است</p></div>
<div class="m2"><p>اگر زمانه ستایش کند تو را گاه است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رضا و خشم تو مانند مشتری و زُحَل</p></div>
<div class="m2"><p>همیشه سعد نکوخواه و نحس بدخواست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به‌خدمت تو دو تا هست خدمت ملکان</p></div>
<div class="m2"><p>از آنکه با تو دل روزگار یکتا هست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چرا نهد عدوی تو خلاف را سرو بن</p></div>
<div class="m2"><p>که جای او سردارست یا بن چاه است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مخالفان تو با آه و آهنند ندیم</p></div>
<div class="m2"><p>سر و زبان همه زیر آهن وآه است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر آن عدو که سپاهش گران‌تر از کوه است</p></div>
<div class="m2"><p>چو پیش تیغ تو آید سبکتر از کاه است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بسا کسا که همی‌گفت شیر شرزه منم</p></div>
<div class="m2"><p>کنون ز بیم تو بیچاره‌تر ز روباه است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز تو جدا نشود دولت تو یکساعت</p></div>
<div class="m2"><p>که با تو دولت تو همنشین و همراه است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دلیل توست بهر جای عِصمت یزدان</p></div>
<div class="m2"><p>برین دلیل دلیل اِعتَصَمتَ بِالله است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خجسته باد شب و روز و ماه و هفتهٔ تو</p></div>
<div class="m2"><p>همیشه تاکه شب و روز و هفته و ماه است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به‌دولت اندر عمری دراز باد تو را</p></div>
<div class="m2"><p>که دست بد ز تو و دولت تو کوتاه است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شمار ملک تو صد بار صد زیادت باد</p></div>
<div class="m2"><p>که حدّ عمر تو پنجاه بار پنجاه است</p></div></div>