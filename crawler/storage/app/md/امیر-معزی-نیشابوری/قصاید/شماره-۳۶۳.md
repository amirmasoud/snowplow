---
title: >-
    شمارهٔ ۳۶۳
---
# شمارهٔ ۳۶۳

<div class="b" id="bn1"><div class="m1"><p>صنع خدای و عدل وزیر خدایگان</p></div>
<div class="m2"><p>هستند پرورنده و دارندهٔ جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معلوم عالم است که بر خلق واجب است</p></div>
<div class="m2"><p>شکر خدا و مدح وزیر خدایگان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صدر اجل رضیّ خلیفه قوام دین</p></div>
<div class="m2"><p>دستور کامکار و خداوند کامران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیک‌اختری که سیرت و کردارهای او</p></div>
<div class="m2"><p>در شرق و غرب هست ز نیک‌ اختری نشان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آراسته شدست به توقیع او زمین</p></div>
<div class="m2"><p>و افروخته شدست به تدبیر او زمان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در عدل جز بدو نکند عالم افتخار</p></div>
<div class="m2"><p>در جود جز بدو نزند ملک داستان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اندر کفایت آنچه از او دید چشم خلق</p></div>
<div class="m2"><p>نشنید گوش خلق ز تاریخ باستان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گویی ز رآی پاک و ز بخت بلند اوست</p></div>
<div class="m2"><p>پاکی در آفتاب و بلندی در آسمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گویی سپهر مرکب اقبال او شدست</p></div>
<div class="m2"><p>کش ماه و آفتاب رکاب آمد و عنان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قفل و کلید گشت دو دستش که جور و عدل</p></div>
<div class="m2"><p>بسته شدست ازین وگشاده شدست از آن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روزی بَنان او دهد آفاق را مگر</p></div>
<div class="m2"><p>دارد بنای روزی آفاق را بنان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گیتی سرای و خلق همه میهمان شدند</p></div>
<div class="m2"><p>تا گشت همت و کرم خواجه میزبان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جایی‌ که میزبان ‌کرم و همتش بود</p></div>
<div class="m2"><p>گیتی سزد سزای و همه خلق میهمان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بنگر به ‌دست و خامهٔ او گر ندیده‌ای</p></div>
<div class="m2"><p>بحر گهر هبا کن و ابر گهر فشان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ور آرزوی دولت و بخت آیدت همی</p></div>
<div class="m2"><p>آثار او نگه کن و توقیع او بخوان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اندر وفای او نَبُوَد جسم را وفات</p></div>
<div class="m2"><p>و اندر هوای او نَبُوَد چرخ را هَوان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر کس‌ که هست چاکر او بهره یافته است</p></div>
<div class="m2"><p>از مایهٔ سعادت و از سایهٔ امان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>افزون کند موافقتش نیکخواه‌ را</p></div>
<div class="m2"><p>در دل قوام دانش و در تن نظام جان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بیرون کند مخالفتش بدسگال را</p></div>
<div class="m2"><p>از دیده روشنایی و از کالبد روان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای دادگستری که به ‌تدبیر ورای خویش</p></div>
<div class="m2"><p>کردی جهان مسخر شاه جهان ستان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تدبیر تو نشاند و سر کِلک تو گماشت</p></div>
<div class="m2"><p>در روم کاردارش و در شام پهلوان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از تیغ و کلک تو حاصل همی شود</p></div>
<div class="m2"><p>هم‌ گنج بی‌نهایت و هم ملک بی‌کران</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>امسال روم و شام بهر دو گشاده شد</p></div>
<div class="m2"><p>سال دگر گشاده شود مصر و قیروان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای دور روزگار به ‌تو سفتهٔ نشان</p></div>
<div class="m2"><p>گردد دل مخالف تو سفتهٔ سنان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بی‌طلعت مبارک و بی‌آفرین تو</p></div>
<div class="m2"><p>بر آدمی حرام شود دیده و دهان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از بهر آنکه دست تو بوسد زبان و لب</p></div>
<div class="m2"><p>گوش و دو دیده رشک برد بر لب و زبان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بر آسمان قضا و قدر متفق شدند</p></div>
<div class="m2"><p>کردند عمر و بخت تو از یک دگر ضمان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بخت تو همچو عمر تو گردید پایدار</p></div>
<div class="m2"><p>عمر تو همچو بخت تو گردید جاودان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>من بنده روزگار تو را وصف چون کنم</p></div>
<div class="m2"><p>پیمودن فلک به کف دست‌ چون توان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>عین‌الکمال عالم ارواح خوانمت</p></div>
<div class="m2"><p>کاندر مناقب تو همی‌گم شودگمان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در خدمت تو رنج برم ‌گنج بر دهم</p></div>
<div class="m2"><p>بر رنج خدمت تو نکردست‌ کس زیان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>روزی که نفخ صور برانگیزدم ز خاک</p></div>
<div class="m2"><p>جانم همه ثنای تو خواند ز استخوان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا باغ را شکفته ‌کند رایت بهار</p></div>
<div class="m2"><p>تا راغ را کآشفته کند لشکر خزان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از عدل تو شکفته همی باد دین و ملک</p></div>
<div class="m2"><p>خصم تو را کَشَفته همی باد خان و مان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تابنده باد فر تو بر خُرد و بر بزرگ</p></div>
<div class="m2"><p>پاینده باد عدل تو بر پیر و بر جوان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>فرخنده باد مهر تو جاوید و من رهی</p></div>
<div class="m2"><p>هر سال‌ گفته تهنیت جشن مهرگان</p></div></div>