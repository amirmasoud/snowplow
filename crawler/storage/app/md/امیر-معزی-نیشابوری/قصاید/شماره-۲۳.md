---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>سال چون نو گشت فرزند نو آمد شاه را</p></div>
<div class="m2"><p>شاه نیکو روی نیکوعهد نیکوخواه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواست یزدان تا ز نسل شاه بنماید به ‌خلق</p></div>
<div class="m2"><p>چون ملکشاه و چو طغرلشاه و سلطانشاه ‌را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواست دولت تا بود چون آفتاب و مشتری</p></div>
<div class="m2"><p>آسمانی نو به برج پادشاهی ماه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین طرب نشگفت اگر زینت فزاید در جهان</p></div>
<div class="m2"><p>رایت و تیغ و نگین و تاج و تخت و گاه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای جهانداری که ایوان تو و میدان تو</p></div>
<div class="m2"><p>قبله و محراب شد عز و جلال و جاه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در هنر پیشی ز اسکندر که هنگام هنر</p></div>
<div class="m2"><p>سجده باید کرد پیش تو چون او پنجاه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عدل و انصاف تو اندر بیشهٔ ایران زمین</p></div>
<div class="m2"><p>آشتی داده است با شیر ژیان روباه را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رسم تو رونق دهد رسم بزرگان را همی</p></div>
<div class="m2"><p>همچو یاقوتی که او قیمت دهد اشباه را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شیرمردان بینم اندر خدمت درگاه تو</p></div>
<div class="m2"><p>طوق در گردن فکنده طوع بی‌اکراه را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دوستان و دشمنانت در جهان مستوجب اند</p></div>
<div class="m2"><p>شادی پاداشن و تیمار بادافراه را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حلق و فرق بدسگالت جای آه و آهن است</p></div>
<div class="m2"><p>درخور آمد فرقش آهن را و حلقش آه را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کامکاری کی بود در پیش تیغت خصم را</p></div>
<div class="m2"><p>پایداری کی بود در پیش صرصر کاه را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هرکه جوید کین تو، کوتاه ‌گردد مد‌تش</p></div>
<div class="m2"><p>کین تو گویی سبب شد مدت ‌کوتاه را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دشمن تو در نهان شش چیز دارد روز و شب</p></div>
<div class="m2"><p>تیر و تیغ و نیزه و زندان و بند و چاه را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر هر آن صحرا که لشکرگه زند شاه جهان</p></div>
<div class="m2"><p>ابر سقائی کند هر روز لشکرگاه را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>‌عنبرین بینم همی ا‌فواه خلق از مدح تو</p></div>
<div class="m2"><p>بوی عنبر داد گویی مدح تو اَفواه را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بنده از راه حوادث با سلامت بگذرد</p></div>
<div class="m2"><p>چون ز مدح و آفرینت توشه سازد راه را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سیرت و رسم تو را بر هر هنر تقدیم باد</p></div>
<div class="m2"><p>تا بود بر هر سخن تقدیم بسم‌الله را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سال و ماه تو همیشه فرخ و فرخنده باد</p></div>
<div class="m2"><p>تا که در تقویم تاریخ است سال و ماه را</p></div></div>