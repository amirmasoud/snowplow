---
title: >-
    شمارهٔ ۲۹۷
---
# شمارهٔ ۲۹۷

<div class="b" id="bn1"><div class="m1"><p>هفت چیز از خسرو عالم همی نازد به هم</p></div>
<div class="m2"><p>دین و ملک و تاج و تخت و رایت و تیغ و قلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن خداوندی که مغرب دارد او زیر نگین</p></div>
<div class="m2"><p>وان شهنشاهی که مشرق دارد او زیر علم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سایهٔ یزدان ملک شاه آن‌که اندر ملک خویش</p></div>
<div class="m2"><p>بندگان دارد چو افریدون و ذوالقرنین و جم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا که او گیتی‌ گشاد و بست بر شاهی‌ کمر</p></div>
<div class="m2"><p>قیمت شاهی فزود و کاست از گیتی ستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچنان کارایش سیارگان است آفتاب</p></div>
<div class="m2"><p>نام او آرایش خطبه است و دینار و درم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آورد جودش ولی را از عدم سوی وجود</p></div>
<div class="m2"><p>افکند تیغش عدو را از وجود اندر عدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>موسی عمران مگر بگرفت تیغش را به کف</p></div>
<div class="m2"><p>عیسی مریم مگر پرورد جودش را به دم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حاجت پیغمبران و حجت پیغمبری</p></div>
<div class="m2"><p>گر ندیدی شو نگه‌ کن دین و عدلش را به هم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عدل او از حاجت پیغمبران دارد نشان</p></div>
<div class="m2"><p>دین او از حجت پیغمبران دارد رقم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا نه بس مدت به دولت کرد خواهد شهریار</p></div>
<div class="m2"><p>رومیان را همچو حاج و روم را همچون حرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در چلیپاخانهٔ قیصر بسی مدت نماند</p></div>
<div class="m2"><p>تا نهد سی‌پاره قرآن را و بردارد صنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از شجاعت وز سخا نازند میران عرب</p></div>
<div class="m2"><p>وز فتوت وز کرم نازند شاهان عجم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گو بیایید و بیاموزید از این فرخنده شاه</p></div>
<div class="m2"><p>هم شجاعت هم سخاوت هم فتوت هم‌ کرم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا بود درچرخ دور و تابود در مهر نور</p></div>
<div class="m2"><p>تا بود در بحر موج و تا بود در ابر نم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هرکجا شادی است با شه باد و غم با دشمنان</p></div>
<div class="m2"><p>جفت شادی پادشاه و دشمنانش جفت غم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ملک چون افزون بود بدخواه کم باشد بلی</p></div>
<div class="m2"><p>ملک او هر ساعت افزون باد و بدخواها‌نش کم</p></div></div>