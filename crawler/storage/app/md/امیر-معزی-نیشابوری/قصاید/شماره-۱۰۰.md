---
title: >-
    شمارهٔ ۱۰۰
---
# شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>عید و آدینه بهم بر پادشا فرخنده باد</p></div>
<div class="m2"><p>طالعش سعد و دلش شاد و لبش پرخنده باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عید اَضحی فرخ و فرخنده آمد در جهان</p></div>
<div class="m2"><p>روز او چون عید اَضْحیٰ فرخ و فرخنده باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا بتابد آفتاب از چرخ‌ گردان بر زمین</p></div>
<div class="m2"><p>آفتاب دولت او بر جهان تابنده باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر همه عالم رخ رخشندهٔ او فرخ است</p></div>
<div class="m2"><p>رحمت ایزد بر آن فرخ رخ رخشنده باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سیرت و آیین او بخشیدن و بخشودن است</p></div>
<div class="m2"><p>آفرین بر شاه بخشایندهٔ بخشنده باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خانه‌های بدسگالانش تهی باد از طرب</p></div>
<div class="m2"><p>خانه‌های نیکخواهانش به‌مال آکنده باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای در این‌ گیتی به‌ تو نازنده جان مصطفی</p></div>
<div class="m2"><p>اندر آن‌گیتی به‌تو جان پدر نازنده باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هشت شاه از گوهر سلجوق گیتی داشتند</p></div>
<div class="m2"><p>نام آن هر هشت تا محشر به عدلت زنده باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی‌غبار و ابر چون خورشید بِدرَخشد ز اوج</p></div>
<div class="m2"><p>جام و دیهیم و نگین تو بدو تابنده باد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرکه در باغ بلا کارد درخت‌ کین تو</p></div>
<div class="m2"><p>در سر میدان تو در پای پیل افکنده باد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وانکه‌ کوشد تا بگرداند سر از فرمان تو</p></div>
<div class="m2"><p>عمر او با آن درخت‌ از بیخ و بن برکنده باد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شهریار هند پیش چاکر تو چاکرست</p></div>
<div class="m2"><p>پادشاه روم پیش بندهٔ تو بنده باد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا بود پرّنده و برنده در صورت یکی</p></div>
<div class="m2"><p>تیر تو پرّنده باد و تیغ تو بُرّنده باد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون درفش باز پیکر برگشایی روز غزو</p></div>
<div class="m2"><p>باز نصرت‌گرد عالی مرکبت پرنده باد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همچنان کز باز ترسد کبک و از شاهین تَذَرو</p></div>
<div class="m2"><p>قیصر ترسا ز تیر تیز تو ترسنده باد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا زبان خواننده وگوینده باشد در جهان</p></div>
<div class="m2"><p>فتح تو خواننده باد و مدح تو گوینده باد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تاکه ابر اندر بهاران بر زمین بارد سِرشک</p></div>
<div class="m2"><p>ابر نعمت بر زمین ملک تو بارنده باد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا همی پوید صبا بر هفت‌ کشور سال و ماه</p></div>
<div class="m2"><p>اسب تو در هفت‌کشور چون صبا پوینده باد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا ز بخت و ملک و عمر اندر جهان باشد اثر</p></div>
<div class="m2"><p>بخت و عمر و ملک تو هر سه به هم پاینده باد</p></div></div>