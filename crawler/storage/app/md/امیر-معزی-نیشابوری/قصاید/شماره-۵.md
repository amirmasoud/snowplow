---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>آفتاب اندر شرف شد بر جهان فرمانروا</p></div>
<div class="m2"><p>کرد دیگرگون زمین و کرد دیگرسان هوا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داد فرمان تا کند در باغ نقاشی سحاب</p></div>
<div class="m2"><p>کرد یاری تا کند در راغ عَطّاری صبا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گلبن از یاقوت رمّانی نهد بر سر کلاه</p></div>
<div class="m2"><p>یاسمین از پرنیان سبز بر بندد قبا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرکجا باشد بیابانی ز بی‌آبی چو تیه</p></div>
<div class="m2"><p>ابر نوروزی زند بر سنگ چون موسی عصا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کنند از مرکبان در موج فوجی تاختن</p></div>
<div class="m2"><p>تا کنند از آهوان در سیل خیلی آشنا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هست در عالم خلایق راکنون وقت‌نظر</p></div>
<div class="m2"><p>هست در صحرا بهایم را کنون جای چرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرخ شد منقار کبک و سبز شد سم ‌گوزن</p></div>
<div class="m2"><p>تا توانگر گشت کوه از لاله و دشت از گیا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شنبلید و لالهٔ نعمان به روی سبزه بر</p></div>
<div class="m2"><p>هست پنداری به مینا در، عقیق و کهربا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خصم سوسن‌ گشت نرگس‌ چشم او زان شد دُژم</p></div>
<div class="m2"><p>عاشق‌ گل شد بنفشه‌ پشت او زان شد دو تا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بلبلان وقت سحر گویی همی دستان زنند</p></div>
<div class="m2"><p>پیش تخت ناصرالدین مطربان خوش نوا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قمریان‌ گویی همی‌گویند شاه شرق را</p></div>
<div class="m2"><p>روز آدینه خطیبان بر سر منبر دعا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شاه روزافروز ابوا‌لحارث ملک ‌سنجر که هست</p></div>
<div class="m2"><p>پادشا گوهر خداوندی‌، عجم را پادشا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن جهانگیری که هست او بر سریر مملکت</p></div>
<div class="m2"><p>آفتاب خسروی بر آسمان کبریا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بازوی دولت خطاب و افسر ملت لقب</p></div>
<div class="m2"><p>از ملوک عالم او دارد که هست او را سزا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بازوی نصرت به این بازو همی گردد قوی</p></div>
<div class="m2"><p>افسر ملت به این افسر همی‌گیرد بها</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بخت عالی چون به درگاهش رسد هر بامداد</p></div>
<div class="m2"><p>خاک درگاهش به ‌چشم اندر کشد چون توتیا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شکر او گویند در خُلد برین با یکدگر</p></div>
<div class="m2"><p>هر زمان جان ملک ‌سلطان و جان مصطفا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن همی‌گوید که صافی شد به‌ عدلش ملک من</p></div>
<div class="m2"><p>وین همی‌گوید که باقی شد به تیغش دین ما</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>او سلیمان است و تیغ تیز او انگشتری</p></div>
<div class="m2"><p>وین مبارک پی وزیر‌ش آصف‌ بن‌ برخیا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پهلوانان سپاهش روز بزم و روز رزم</p></div>
<div class="m2"><p>چون پری و دیو در فرمان او فرمانروا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>رای هر یک عالم آراید همی چون آفتاب</p></div>
<div class="m2"><p>خشم هر یک دشمن او بارد همی چون اژدها</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عز دین جان معزالدین بیفروزد همی</p></div>
<div class="m2"><p>زان ‌کجا کردست با فرزند او عهد و وفا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تیغ تیز نصرت‌الدین نایب است از ذوالفقار</p></div>
<div class="m2"><p>زانکه او در نصرت دین نایب است از مرتضا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از لطافت آسمان تفضیل دارد بر زمین</p></div>
<div class="m2"><p>هست با هر دو به تایید و سعادت آشنا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر دلیلی باید این را طالع او بس دلیل</p></div>
<div class="m2"><p>ور گواهی باید آن را طینت او بس ‌گوا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شد ز رای این وزیر و دانش این دو امیر</p></div>
<div class="m2"><p>کار این خسرو عجب چون معجزات انبیا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای فزوده گوهر سلجوق را عز و شرف</p></div>
<div class="m2"><p>داده ملک و دولت مُوروث را نور و نوا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رنجِ قارون است حاسد را ز تو روز نبرد</p></div>
<div class="m2"><p>گنج قارون است سائل را ز تو روز عطا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>با عنا باشد کسی ‌کز حکم تو تابد عنان</p></div>
<div class="m2"><p>بی‌هوی باشد کسی کش سوی تو باشد هوا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بادِ عدل تو بگرداند بلا از دوستان</p></div>
<div class="m2"><p>آتش شمشیر تو بر دشمنان بارد بلا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>درگه میمون توکعبه است و دستت ز‌مزم است</p></div>
<div class="m2"><p>پایهٔ تخت تو رکن است و رکاب تو صفا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گر به خواب اندر ببیند رایت تو رآی هند</p></div>
<div class="m2"><p>ور نبرد لشکر تو بشنود خان ختا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از فَزَع شوریده‌ گردد رآی را تدبیر و رآی</p></div>
<div class="m2"><p>وز نهیب اندیشهٔ خان ختا گردد خطا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بر سریرِ خسروی بادت بقای سَرمدی</p></div>
<div class="m2"><p>تا بود خاک و هوا و آب و آتش را بقا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دشمنت را باد همچون آسیا پر آب چشم</p></div>
<div class="m2"><p>تا همی‌گردد سپهر آبگون چون آسیا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تهنیت ‌کرده تو را میران به صد جشن چنین</p></div>
<div class="m2"><p>شاعران ‌گفته به هر جشنی تو را مدح و ثنا</p></div></div>