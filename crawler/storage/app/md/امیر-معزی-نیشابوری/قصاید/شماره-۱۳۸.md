---
title: >-
    شمارهٔ ۱۳۸
---
# شمارهٔ ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>جشن خزان به‌خدمت شاه جهان رسید</p></div>
<div class="m2"><p>رایت ز کوهسار به صحرا درون کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از عکس رایت وی و از نور آفتاب</p></div>
<div class="m2"><p>وز جام می سه صبح بهٔک جای بردمید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شرط است اگر کنند به جشنی چنین نشاط</p></div>
<div class="m2"><p>وقت است اگر خورند به وقتی چنین نَبید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاصه‌ که شاه ما ز سمرقند بر مراد</p></div>
<div class="m2"><p>با شادکامی آمد و با فرخی رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاهی به آفرین‌ که زبس رحمت و کرم</p></div>
<div class="m2"><p>گویی خداش از کرم و رحمت آفرید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندر جهان گرفتن و در ملک داشتن</p></div>
<div class="m2"><p>گردون چنو نزاد و زمانه چنو ندید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>او سایهٔ خدای به قول پیمبرست</p></div>
<div class="m2"><p>کز عدل بر شریعت او سایه‌ گسترید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشتاق عدل او شد و محتاج عفو او</p></div>
<div class="m2"><p>هرکس‌که در جهان خبر و نام او شنید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جان صلاح در تن دولت قرار یافت</p></div>
<div class="m2"><p>تا او به تیغِ دادْ گلوی ستم برید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>او را گزید بخت ز شاهان روزگار</p></div>
<div class="m2"><p>فرّخ‌ کسی که خدمت درگاه او گزید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آب حیات گشت قبولش که خِضروار</p></div>
<div class="m2"><p>باقی بماند هر که ازو شربتی چشید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گوهر بود عزیز ولیکن به زر خرند</p></div>
<div class="m2"><p>عهدش عزیزتر که همه ‌کس به جان خرید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هرکار کز حوادث ایام بسته بود</p></div>
<div class="m2"><p>وافکنده بود چرخ بر او قفل بی‌کلید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن‌ کار شد گشاده ز تأیید بخت او</p></div>
<div class="m2"><p>وامد کلید قفل زاقبال او پدید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تَنبُل نداشت سود کرا عزم او شکست</p></div>
<div class="m2"><p>افسون نداشت سود کراکین اوگزید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یک دست او قضاست دگر دست او قدر</p></div>
<div class="m2"><p>بیهوده با قضا و قدر کی توان چَخید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای خسروی که راست نهادی همه جهان</p></div>
<div class="m2"><p>در خدمت تو پشت همه خسروان خمید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مانَد فتوح تو ز عجایب به معجزات</p></div>
<div class="m2"><p>هرکس‌ که معجزات تو بشنید بگروید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دولت جهنده بود ز هر کس به روزگار</p></div>
<div class="m2"><p>جون دید روزگار تو با تو بیارمید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همچون قلم به‌دست دبیران اوستاد</p></div>
<div class="m2"><p>از حرص خدمت تو به‌تارک همی دوید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>معلوم خلق‌گشت که ایزد بدان سبب</p></div>
<div class="m2"><p>عالم تورا سپرد که عالم تورا سزید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جاوید باد عمر تو کز باد عدل تو</p></div>
<div class="m2"><p>در باغ مملکت‌ گل اقبال بشکفید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر دست تو نهاد شرابی چون ارغوان</p></div>
<div class="m2"><p>وز بیم تو شده رخ دشمن چو شَنبَلید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>روی تو پرورندهٔ دولت که ملک را</p></div>
<div class="m2"><p>دولت ز دیر باز برای تو پرورید</p></div></div>