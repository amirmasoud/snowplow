---
title: >-
    شمارهٔ ۱۰۶
---
# شمارهٔ ۱۰۶

<div class="b" id="bn1"><div class="m1"><p>بتی کاو نسبت از نوشاد دارد</p></div>
<div class="m2"><p>دلم هر ساعت از نو، شاد دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به روی خویش کوی و برزن من</p></div>
<div class="m2"><p>چو لعبت‌ خانهٔ نوشاد دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به صورت هست نیکوتر ز ‌شیرین</p></div>
<div class="m2"><p>مرا عاشق‌تر از فرهاد دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر آن بت هست مادر زاد عشقم</p></div>
<div class="m2"><p>که این بت حسن مادر زاد دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رخ او هست چون بغداد و چشمم</p></div>
<div class="m2"><p>نشان دجلهٔ بغداد دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به‌ سرخی چهرهٔ او ارغوان است</p></div>
<div class="m2"><p>به گرد ارغوان شمشاد دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به نرمی سینهٔ او پرنیان است</p></div>
<div class="m2"><p>به زیر پرنیان پولاد دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر آن عاشق نیارد کرد بیداد</p></div>
<div class="m2"><p>که او مهر امیر راد دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>معین دولت سلطان عادل</p></div>
<div class="m2"><p>که طبع پاک و دست راد دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سپهداری که اندر لشکر خویش</p></div>
<div class="m2"><p>هزاران گرد چون کشواد دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به رای پاک خویش آزادگان را</p></div>
<div class="m2"><p>ز بند روزگار آزاد دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به رزم اندر ز سهم هیبت خویش</p></div>
<div class="m2"><p>همه فولاد دشمن لاد دارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدان ماند که تیغ آب رنگش</p></div>
<div class="m2"><p>فروغ آزر خرّاد دارد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو کفّار از تف دوزخ مخالف</p></div>
<div class="m2"><p>ز تَفّ تیغ او فریاد دارد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زمانه سال عمر هر تنی را</p></div>
<div class="m2"><p>اگر هفتاد اگر هشتاد دارد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ولیکن سال عمر دادبک را</p></div>
<div class="m2"><p>صد و هفتاد ره هفتاد دارد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>امیرا خانهٔ مجد و مروت</p></div>
<div class="m2"><p>ز عقل و عدل تو بنیاد دارد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کسی کاو دین و داد و دانش آموخت</p></div>
<div class="m2"><p>دل و طبع تو را استاد دارد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زعدل و جود نوشِروان و حاتم</p></div>
<div class="m2"><p>هر آنکس کاو حدیثی یاد دارد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کجا عدل تو و جود تو بیند</p></div>
<div class="m2"><p>حدیث هر دو یکسر باد دارد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همه ساله معزی در مدیحت</p></div>
<div class="m2"><p>قلم چون حکم تو نَفّاذ دارد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز شعر آورد نزد تو عروسی</p></div>
<div class="m2"><p>که از اقبال تو داماد دارد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همیشه تا هوا سردی و گرمی</p></div>
<div class="m2"><p>ز ماه بهمن و خرداد دارد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بنای عمر تو آباد داراد</p></div>
<div class="m2"><p>که عمرت دین و ملک آباد دارد</p></div></div>