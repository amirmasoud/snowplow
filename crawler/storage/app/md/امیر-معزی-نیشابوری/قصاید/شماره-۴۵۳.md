---
title: >-
    شمارهٔ ۴۵۳
---
# شمارهٔ ۴۵۳

<div class="b" id="bn1"><div class="m1"><p>ای بر سمن از مشک به عمدا زده خالی</p></div>
<div class="m2"><p>مسکین دلم از خال تو گشته است به حالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حالی به جهان زارتر از حال دلم نیست</p></div>
<div class="m2"><p>تا نیست د‌ل آشوب‌تر از خال تو خالی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدّ و دهن و زلف تو و جعد تو دیدم</p></div>
<div class="m2"><p>هر یک ز یکی حرف پذیرفته مثالی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سیم الف دیدم از بسد میمی</p></div>
<div class="m2"><p>از مشک سیه جیمی و از غالیه دالی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم که تو خورشیدی و این بود حقیقت</p></div>
<div class="m2"><p>گفتم که تو چون ماهی و این بود محالی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مه بدر نماید چو زخورشید شود دور</p></div>
<div class="m2"><p>من کز تو شوم دور نمایم چو هلالی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در خواب خیال تو به نزدیک من آید</p></div>
<div class="m2"><p>گویم‌که مگر هست مرا با تو وصالی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیدار شوم چون نه تو باشی نه خیالت</p></div>
<div class="m2"><p>عشق تو مرا باز نداند ز خیالی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای از بر من دور همانا خبرت نیست</p></div>
<div class="m2"><p>کز مویه چو مویی شدم از ناله چو نالی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یک روز به سالی نکنی یاد کسی را</p></div>
<div class="m2"><p>کز هجر تو روزیش گذشته است به سالی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روزی بود آخر که دل و جان بفروزم</p></div>
<div class="m2"><p>زان روی که شهری بفروزد به جمالی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از قبضهٔ هجر تو شود رسته دل من</p></div>
<div class="m2"><p>وز روضه ی وصل تو شود رُسته نهالی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فرخنده بود روز هر آن کس‌ که به شبگیر</p></div>
<div class="m2"><p>از روی تو و رای ملک‌ گیرد فالی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شاه همه شاهان ملک ارغو که ندارد</p></div>
<div class="m2"><p>در مردی و فرهنگ نظیری و همالی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن شهرگشایی‌ که ملک بر فلک او را</p></div>
<div class="m2"><p>هر روز دهد مژده به عزّی و جلالی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در معرکه بستاند و در بزم ببخشد</p></div>
<div class="m2"><p>ملکی به سواری و جهانی به سوالی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عادلتر و عالمتر از او هیچ ملک نیست</p></div>
<div class="m2"><p>الا مَلَکُ العَرش تبارک و تعالی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کیوانْ سخطی‌، مهرْ اثری چرخ‌ْ محلی</p></div>
<div class="m2"><p>باران حَشَمی ابر کفی بحر نوالی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای دهر گرفته ز تو فری و بهایی</p></div>
<div class="m2"><p>وی ملک فزوده ز تو جاهی و جمالی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شاها چو شود لفظ متین یاور طبعم</p></div>
<div class="m2"><p>گویی‌ که جهد بیرون از سنگ زلالی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در جلوه عروسان ضمیرم چو درآیند</p></div>
<div class="m2"><p>بنمایدم این آینه‌گون حقه مثالی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جان دادن خفاش بدم کار مسیح است</p></div>
<div class="m2"><p>ور نه نکند از گل صد مرغ‌ کلالی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا در چمن و باغ نهالی به بر آید</p></div>
<div class="m2"><p>از تربیت اختر و تأثیر شمالی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ایزد شب و روز مه و سالیت معین باد</p></div>
<div class="m2"><p>تا روز و شبی هست و به عالم مه و سالی</p></div></div>