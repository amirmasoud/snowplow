---
title: >-
    شمارهٔ ۳۹۵
---
# شمارهٔ ۳۹۵

<div class="b" id="bn1"><div class="m1"><p>نگار من خط مشکین کشید بر نسرین</p></div>
<div class="m2"><p>خطاکشید به نسرین بر آن خط مشکین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زبهر آنکه چو مشکین خطش پدید آید</p></div>
<div class="m2"><p>اسیر آن خط مشکین شد این دل مسکین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخش ‌گل است و لبش لاله از لطافت و نور</p></div>
<div class="m2"><p>خطش چو سنبل و زلفش بنفشه از خم و چین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زمانه خواست مگر کز بنفشه و سنبل</p></div>
<div class="m2"><p>به‌ گرد لاله و گرد گلش بود پرچین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کجا نهان شود از من رخ چو پروینش</p></div>
<div class="m2"><p>کنم خروش و دلم گیرد آذر برزین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زمن بدیع نباشد خروش آذر دل</p></div>
<div class="m2"><p>که رعد و برق بود چون نهان شود پروین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر من از دل غمگین همی زنم دم سرد</p></div>
<div class="m2"><p>شگفت نیست دَمِ سرد از این دل غمگین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بت من از لب شیرین جواب تلخ دهد</p></div>
<div class="m2"><p>جواب تلخ شگفت است از آن لب شیرین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به عشق و حسن‌کنون داستان و قصهٔ ما</p></div>
<div class="m2"><p>سمر شدست چو اخبار خسرو و شیرین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وگرچه بر رخ شیرین و در دل خسرو</p></div>
<div class="m2"><p>نه حسن بود چنان و نه عشق بود چنین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در آن غزال غزل‌گفتم و لطیف آمد</p></div>
<div class="m2"><p>که عشق کرد غزلهای او مرا تلقین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر به گاه غزل شعر از او لطافت یافت</p></div>
<div class="m2"><p>به‌گاه مدح علو یافت از علاء‌الدین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بلند همت خاقان پادشا گوهر</p></div>
<div class="m2"><p>ستوده تاج سلاطین جمال مشرق و چین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سپهر فتح ابوالفتح قبلهٔ اقبال</p></div>
<div class="m2"><p>محمد آیت احماد و مایهٔ تمکین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شهی‌ که از شرف نام فخر و کنیت او</p></div>
<div class="m2"><p>کشید محمدت و فتح سر به علیین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زگاه دولت افراسیاب دودهٔ او</p></div>
<div class="m2"><p>امیر و شاه و ملک بوده‌اند و خان و تَکین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بزرگواری چون خاتم است و گوهر او</p></div>
<div class="m2"><p>نگین خاتم و فرهنگ او چو نقش نگین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مزین است خراسان ز فر او امروز</p></div>
<div class="m2"><p>چنانکه بود مزین ز رای او غزنین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زبهر آنکه ز نور خجسته طلعت او</p></div>
<div class="m2"><p>شدست روی زمین همچو آسمان برین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>محل و پایهٔ او از زمین رسانیدست</p></div>
<div class="m2"><p>بر آسمان برین پادشاه روی زمین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همان عمل‌ کند اندر مصاف خنجر او</p></div>
<div class="m2"><p>که ذوالفقار علی کرد در صف صفین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شود شکسته به عزمش مصافهای عظیم</p></div>
<div class="m2"><p>شود گشاده به حزمش حصارهای حصین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به زخم تیر کند سفته یَشک پیل دمان</p></div>
<div class="m2"><p>به زخم تیر کند پاره یال شیر عرین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز نیست هست‌ کند چو به بزم ورزد مهر</p></div>
<div class="m2"><p>ز هست نیست کند چون به رزم توزد کین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اگر شکار به شاهین او کند دولت</p></div>
<div class="m2"><p>کند ز سینهٔ سیمرغ طعمهٔ شاهین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>وگر عطاش به میزان چرخ برسنجند</p></div>
<div class="m2"><p>شکسته‌ گردد میزان چرخ را شاهین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عجب زبارهٔ شبرنگ او که گر خواهد</p></div>
<div class="m2"><p>به نیمشب ز فلسطین رود به قسطنطین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو از نشیب رود بر فراز باشد ابر</p></div>
<div class="m2"><p>چو از فراز رود در نشیب باشد هین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به حمله جان برد از جادوان چو گوید هان</p></div>
<div class="m2"><p>به پویه بگذرد از آهوان چو گوید هین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به طور ماند چون با فسار باشد و جل</p></div>
<div class="m2"><p>به طیر ماند چون با لگام باشد و زین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عجب ز هندی تیغش که چون برهنه شود</p></div>
<div class="m2"><p>بود چو لولو پیروزه رنگ دُر آگین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز بهر آنکه به شکل زبان تنین است</p></div>
<div class="m2"><p>بود به عقل گزاینده چون دم تِنّین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>روان خصم رباید و گرچه خصم بزرگ</p></div>
<div class="m2"><p>بود به شوخی ‌گرگ و به چارهٔ گرگین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو لعل فام شود همچو ابر در نیسان</p></div>
<div class="m2"><p>رخ حسود کند همچو برگ در تِشرین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ایا نجوم سخا با کف تو کرده قرار</p></div>
<div class="m2"><p>و یا رسوم ادب با دل تو گشته قرین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مگر صدف بگشاید ز مِدحَتِ تو زبان</p></div>
<div class="m2"><p>که دست ابر نهد در دهانش در ثمین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مگر سجود بر طین وقار و حلم تو را</p></div>
<div class="m2"><p>که دست باد کند پرشکوفه دامن طین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اگر ز جود تو باشد سرشک ابر بهار</p></div>
<div class="m2"><p>چهار فصل بود همچو ماه فروردین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز بس معانی نیکو که در مدایح توست</p></div>
<div class="m2"><p>همه زحسن صفات تو درخور تحسین</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بدان نیاز نباشد مدیح‌گوی تو را</p></div>
<div class="m2"><p>که در مدیح تو شعری دگر کند تضمین</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز حاجیان تو بر درگه تو خواهد بار</p></div>
<div class="m2"><p>چو اعتصام بود بنده را به حبل متین</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زساقیان تو در مجلس تو خواهد می</p></div>
<div class="m2"><p>چو اشتیاق بود بنده را به ماء مَعین</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خدای عرش مدام از فرشتگان دو رقیب</p></div>
<div class="m2"><p>به نزد بنده نشاندست بر شمال و یمین</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>اگر ثنای تو گوید یکی زند ا‌حسنت‌</p></div>
<div class="m2"><p>وگر دعای توگوید یکی‌کند آمین</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>همیشه تاکه زمهرست رحمت و انصاف</p></div>
<div class="m2"><p>همیشه تاکه ز روح است راحت و تسکین</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ضمان‌کنندهٔ مهر تو باد مهر منیر</p></div>
<div class="m2"><p>مدددهندهٔ روح توباد روح امین</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>اگر قرار نگیرد همی سنین و شهور</p></div>
<div class="m2"><p>قرار گیر تو تا حشر در شهور و سنین</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به روز عید همایون و روزگار بهار</p></div>
<div class="m2"><p>مغنیان بنشان و به خرمی بنشین</p></div></div>