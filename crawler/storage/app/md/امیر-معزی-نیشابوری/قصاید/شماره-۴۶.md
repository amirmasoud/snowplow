---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>عید ا‌َضْحیٰ رسم و آیین خلیل آزرست</p></div>
<div class="m2"><p>عیدفطر اندر شریعت سنت پیغمبرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دو عید ملت است و زینت است اسلام را</p></div>
<div class="m2"><p>عید دولت طلعت میمون سلطان سنجرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عید ملت خلق را باشد به ‌سال اندر دو روز</p></div>
<div class="m2"><p>طلعت او خلق را هر روز عیدی د‌یگرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن جهانگیری ‌که آرام جهان از تیغ اوست</p></div>
<div class="m2"><p>وان جهانداری که داد او جهان را ‌داورست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن‌ که شاهان را به ایرانشهر سر بر نام اوست</p></div>
<div class="m2"><p>وان که خاقان را به توران نامهٔ او بر سرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاه والا همت است و شاه نیکو سیرت است</p></div>
<div class="m2"><p>شاه عالی رُتبَت است و شاه پبروز اخترست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوهر سلطان ملک تاج سر شاهنشه است</p></div>
<div class="m2"><p>دولت او گوهر تاج است و تاج‌گوهرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حشمت اسلاف او از نام او تا آدم است</p></div>
<div class="m2"><p>دولت ا‌عقاب او از فر او تا محشرست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در جهان یا زیر ‌دست اوست یا از دست اوست</p></div>
<div class="m2"><p>هرکه در شاهی سزای ملک و تاج و افسرست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خطبه را هست از خطاب نام او عز و شرف</p></div>
<div class="m2"><p>هرکجا ‌در مشرق و مغرب خطیب و منبرست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنچه بگرفت از جهان و از پدر میراث ‌یافت</p></div>
<div class="m2"><p>هر ‌دو حقی واجب‌است و حق به‌دست حقورست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خسروی را نیست د‌رخور هر که عهدش بشکند</p></div>
<div class="m2"><p>وانکه در عهدش بماند خسروی را درخورست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>او به ایران است و عزمش بر در انطاکیه</p></div>
<div class="m2"><p>او به مروست و نهیبش بر درکالنجرست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از شمار لشکر او وهم مردم عاجزست</p></div>
<div class="m2"><p>وهم مردم کی بود چندانکه او را لشکرست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر شگفتیهای رزم او سراسر بشمری</p></div>
<div class="m2"><p>بیش باشد زان سگفتیها که در هر دفترست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>قصهٔ اسکندر از دفتر چرا خوانی همی</p></div>
<div class="m2"><p>با فتوح او چه جای قصهٔ اسکندرست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آسمان آراسته است از رایت مه پیکرش</p></div>
<div class="m2"><p>از سم اسبان او روی زمین مه پیکرست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هست مهر وکین او چون نوش و زهر از بهرآنک</p></div>
<div class="m2"><p>کینه او جانگزای و مهر او جان پرورست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هیچ کس را در جهان از آب و آذر چاره نیست</p></div>
<div class="m2"><p>زانکه تیغ او به‌رنگ‌آب و فعل آذرست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون ببالاید به ‌خون بدسگالان تیغ او</p></div>
<div class="m2"><p>ارغوان و لاله‌ گویی رسته‌ از نیلوفرست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ابر نیسان را سر اندر چنبر فرمان اوست</p></div>
<div class="m2"><p>در هوا قوس قزح‌ از بهر آن چون چنبر است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر به ‌دریا در سکون ‌کشتی از لنگر بود</p></div>
<div class="m2"><p>این جهان دریا و او کشتی و عدلش لنگر است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای خداوندی ‌که عالی رایت رای تو را</p></div>
<div class="m2"><p>خسرو سیارگان چون بندگان خدمتگرست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از دل و جان هر که پنهان نیست در فرمان تو</p></div>
<div class="m2"><p>آشکارا از بن دندان تو را فرمانبرست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هر کجا سازی مقام آنجا بود شادی مقیم</p></div>
<div class="m2"><p>چون به‌شادی ایدری اسباب شادی ایدرست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>باده باید خواست از دست بتان آزری</p></div>
<div class="m2"><p>اندرین موسم‌ که آئین خلیل آزر است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خاصه در فصلی که بر اطراف جوی از باد سرد</p></div>
<div class="m2"><p>پاره پاره ‌سیم و پولاد و بلور ومرمر است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در میان خانه‌ها ازگوهر مجلس فروز</p></div>
<div class="m2"><p>توده توده بُسّد و یاقوت و لعل اَحمَرست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گوهری کاورا برادر ماه و خواهر مشتری است</p></div>
<div class="m2"><p>گوهری کاورا پدر سنگ است و آهن مادرست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از فروغش خانه همچون بوستان خرم است</p></div>
<div class="m2"><p>قد او در بوستان مانند زرین عرعرست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گرچه‌ رخشان‌ است بیش‌ رای او همچون رهی‌ است</p></div>
<div class="m2"><p>ورچه سوزان است پیش چشم او خاکستر است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا که جای تیر و خنجر هست رزم جنگیان</p></div>
<div class="m2"><p>تاکه بزم باده‌خواران جای جام و ساغرست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا قیامت فخر جام و ساغر از دست تو باد</p></div>
<div class="m2"><p>همچنان‌ کز بازوی تو فخر تیر و خنجرست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>باد عدلت‌ کار ساز و یاور خلق جهان</p></div>
<div class="m2"><p>کایزدت در هر مقامی کارساز و یاورست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>عهد تو خوش باد و خرم ‌کز مدار آسمان</p></div>
<div class="m2"><p>روزت از روز و شب از شب خوشتر و خرمترست</p></div></div>