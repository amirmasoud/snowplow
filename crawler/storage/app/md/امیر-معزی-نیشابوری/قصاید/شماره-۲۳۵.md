---
title: >-
    شمارهٔ ۲۳۵
---
# شمارهٔ ۲۳۵

<div class="b" id="bn1"><div class="m1"><p>تا طَیْلسان سبز برافکند جویبار</p></div>
<div class="m2"><p>دیبای هفت رنگ بپوشید کوهسار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن همچو گنج خانهٔ قارون شد از گهر</p></div>
<div class="m2"><p>وین همچو نقش نامهٔ مانی شد از نگار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از ژاله لاله را همه دُرَّست در دهن</p></div>
<div class="m2"><p>وز لاله سبزه را همه لعل است در کنار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون در کنار سبزه بود لعل قیمتی</p></div>
<div class="m2"><p>اندر دهان لاله سزد درّ شاهوار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرخی ستاره بار شدست از نسیم باد</p></div>
<div class="m2"><p>در هر چمن که هست درختی شکوفه‌دار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشگفت اگر ز غلغل بلبل قیامت است</p></div>
<div class="m2"><p>باشد به هم قیامت و چرخ ستاره بار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خورشید شد بلند و ز دریا به فعل خویش</p></div>
<div class="m2"><p>هر ساعتی همی ز هوا برکشد بخار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گاهی از آن بخار فلک را کند حجاب</p></div>
<div class="m2"><p>گاهی از آن حجاب زمین را کند نثار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر سال در جهان دو بهارست خلق را</p></div>
<div class="m2"><p>طبعی بهار اول و عقلی دگر بهار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طبعی بود لطایف یزدان دادگر</p></div>
<div class="m2"><p>عقلی بود مدایح دستور شهریار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عادل نظام ملک اتابک قوام دین</p></div>
<div class="m2"><p>شمس کفات و سیّد سادات روزگار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صدر اجل رضی خلیفه حسن‌که هست</p></div>
<div class="m2"><p>از حُسن خلق حجت احسان کردگار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در همتش همی نرسد گردش فلک</p></div>
<div class="m2"><p>گویی فلک پیاده شد و همتش سوار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رایش ز آفتاب همی زر کند به خاک</p></div>
<div class="m2"><p>مهرش چو نوبهار همی گل کند ز خار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>طبعش به جود و عفو کند میل و زین سبب</p></div>
<div class="m2"><p>بخشیدن است شغلش و بخشودن است کار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شد متفق مدار فلک با مراد او</p></div>
<div class="m2"><p>جز بر مراد او نکند ساعتی مدار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ناممکن است دیدن یار و نظیر او</p></div>
<div class="m2"><p>ایزد نیافرید مر او را نظیر و یار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هرکس که در حمایت او زینهار یافت</p></div>
<div class="m2"><p>از دهر یافت تا ابدالدهر زینهار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر آهویی که سایهٔ عدلش فتد بر او</p></div>
<div class="m2"><p>باشد حرام پنجهٔ شیران مرغزار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ماند به نار خشمش و ماند به خاک حلم</p></div>
<div class="m2"><p>اندر یکی تحرک و اندر یکی قرار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا ملک شاه را قلمش خط استواست</p></div>
<div class="m2"><p>زان استواست قاعدهٔ ملک استوار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر مه که نو شود متواتر همی رسد</p></div>
<div class="m2"><p>حملش ز قیروان و خراجش ز قندهار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بستند بر میان کمر عهد و طاعتش</p></div>
<div class="m2"><p>خانان کامران و تکینان کامکار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون آب و موم گردد گر در خلاف او</p></div>
<div class="m2"><p>زاتش بود مخالف و زاهن بود حصار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خشم و رضای تو سبب عجز و قدرت است</p></div>
<div class="m2"><p>از عجز جبر خیزد و از قدرت اختیار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>باطل هر آن‌که از خط مهر تو سرکشید</p></div>
<div class="m2"><p>حق است آن‌که عهد تو را هست خواستار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>باطل شد از میانه و حق پیش تو بماند</p></div>
<div class="m2"><p>حق پایدار باشد و باطل نه پایدار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای خامهٔ تو شاخی کش ساحری است بر</p></div>
<div class="m2"><p>وی نامهٔ تو باغی‌ کش نیکویی است بار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زان خامه سِحر بابلیان هست مُستَرِق</p></div>
<div class="m2"><p>زان نامه نقش مانویان هست مُسْتعار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو مهر جوی شاه و فلک بر تو مهربان</p></div>
<div class="m2"><p>توکار ساز خلق و جهان با تو سازگار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اندازهٔ شمار ممالک به دست تو</p></div>
<div class="m2"><p>عمر تو درگذشته ز اندازهٔ شمار</p></div></div>