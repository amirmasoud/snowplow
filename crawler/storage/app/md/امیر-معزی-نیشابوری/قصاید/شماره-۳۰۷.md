---
title: >-
    شمارهٔ ۳۰۷
---
# شمارهٔ ۳۰۷

<div class="b" id="bn1"><div class="m1"><p>منت خدای را که برون آمد از غمام</p></div>
<div class="m2"><p>بدری که هست پیشرو دودهٔ نظام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صدری که هست خادم پایش سر کفات</p></div>
<div class="m2"><p>میری‌ که هست عاشق دستش لب‌ کرام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شایسته زین ملت وبایسته فخر مُلک</p></div>
<div class="m2"><p>فرخنده نصر دولت ابوالفتح بن نظام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دستور زاده‌ای که به اقبال و مکرمت</p></div>
<div class="m2"><p>چون واسطه ز عقده همی تابد از انام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ظاهرترست از آنکه کسی گویدش کجاست</p></div>
<div class="m2"><p>پیداترست زانکه کسی گویدش کدام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل در ستایش هنرش هست بی‌ملال</p></div>
<div class="m2"><p>جان در پرستش خردش هست بی‌ملام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سرّ غیب خاطر او هست مُطّلع</p></div>
<div class="m2"><p>بی‌آنکه جبرئیل گزارد بدو پیام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>او را سلام کن که سلامت بود تو را</p></div>
<div class="m2"><p>او را بود سلامت‌ کاو را کند سلام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با مهر و ماه دولت او متصل شدست</p></div>
<div class="m2"><p>وین اتصال خواهد بودن علی‌الدوام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گویی نهاد دولت او را خدای عرش</p></div>
<div class="m2"><p>بر سر ز مهر افسر و بر کف ز ماه جام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای سیرت بدیع تو فهرست افتخار</p></div>
<div class="m2"><p>ای همت رفیع تو قانون احتشام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر نام‌گیرد از ظفر و مدح هر امیر</p></div>
<div class="m2"><p>اینک تو را ز فتح و ظفر کنیت است و نام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرچه توراست عالم جسمانیان وطن</p></div>
<div class="m2"><p>رای توراست عالم روحانیان مقام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همچون پدر به جود بشر را تویی بشیر</p></div>
<div class="m2"><p>همچون پدر به عدل امم را تویی امام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر جان خلق خازن مهر تو نیستی</p></div>
<div class="m2"><p>حقا که آمدی همه مهر تو از مسام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دلهای خاص و عام به فر تو شد درست</p></div>
<div class="m2"><p>زان پس‌ که بود کوفته دلهای خاص و عام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا شد نسیم وصل تو بر جسم ما حلال</p></div>
<div class="m2"><p>شد آتش فراق تو بر جان ما حرام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رفته است سیدالوزراء و تو مانده‌ای</p></div>
<div class="m2"><p>از رفته‌ایم غمگین وز مانده شادکام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آمد بسی به دام اجل صیدگونه‌گون</p></div>
<div class="m2"><p>صیدی چو سَیّدالوزرا نامدش به‌دام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اندر جهان نظام زعمر نظام بود</p></div>
<div class="m2"><p>رفت از جهان نظام و ببرد از جهان نظام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کار حسام کرد همی درکفش قلم</p></div>
<div class="m2"><p>واکنون شدست بی‌قلمش ملک بی‌حُسام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا مست کرد خمر وفاتش زمانه را</p></div>
<div class="m2"><p>گویی زمانه همچو هیونی است بی‌زمام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چرخ از نیام فتنه یکی تیغ برکشید</p></div>
<div class="m2"><p>تا صد هزار تیغ برون آمد از نیام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا سیب تازیانهٔ رایض گسسته گشت</p></div>
<div class="m2"><p>آشفته گشت و گشت جهانی که بود رام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شیری شد آن که بود گرازنده چون گوزن</p></div>
<div class="m2"><p>بازی شد آن‌ که بود گریزنده چون حمام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شد تیره‌فام روز گروهی کز ابتدا</p></div>
<div class="m2"><p>خنجر به خون ناحق کردند لعل فام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از دست روزگار ببردند مدتی</p></div>
<div class="m2"><p>دیدند دست برد مکافات و انتقام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از وصف این عجایب و از شرح این عِبَر</p></div>
<div class="m2"><p>عاجز بود عبارت و قاصر بود کلام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>این حالها که رفت به بیداری ای عجب</p></div>
<div class="m2"><p>گویی چو نومهای محال است در منام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ای نیکخواه مهتر و نیکوسخن‌کریم</p></div>
<div class="m2"><p>فرخ‌لقا امیر و همایون نسب همام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در عصمت خدای بدین جانب آمدی</p></div>
<div class="m2"><p>تا بندگان ‌کنند به حبل تو اعتصام</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا شرع را کنی به هدی صافی از ضلال</p></div>
<div class="m2"><p>تا ملک را کنی به ضیا خالی از ظلام</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا همت تو خوب ‌کند فعلهای زشت</p></div>
<div class="m2"><p>تا دولت تو پخته کند کارهای خام</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گیرد به دولت تو همه شغلها نسق</p></div>
<div class="m2"><p>گردد به همت تو همه کارها تمام</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ا‌رجو که همچنین بود و بیش از این بود</p></div>
<div class="m2"><p>تا دوستت رهی شود و دشمنت غلام</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>من بنده گر چه هول قیامت کشیده‌ام</p></div>
<div class="m2"><p>پیوسته کرده‌ام به ثناهای تو قیام</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گه خوانده‌ام مدیح تو از شام تا به صبح</p></div>
<div class="m2"><p>گه‌ گفته‌ام ثنای تو از صبح تا به شام</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گه بوده است یاد تو و آفرین تو</p></div>
<div class="m2"><p>تکبیر در صلوتم و تسبیح در صیام</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تا طبع آب تر بود و طبع خاک خشک</p></div>
<div class="m2"><p>واندر جهان مزاج بود هر دو را مدام</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>از آب و خاک باد همه دشمنانت را</p></div>
<div class="m2"><p>تری‌ نصیب دیده و خشکی نصیب کام</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>آنجا که هست بخت تو دولت کشیده رخت</p></div>
<div class="m2"><p>وانجا که هست کام تو نصرت نهاده گام</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از شاعران بنا و ز توبر و مکرمت</p></div>
<div class="m2"><p>از عالمان دعا و زتو سعی و اهتمام</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تا مدتی قریب نهاده شه ملوک</p></div>
<div class="m2"><p>در دست تو زمانهٔ آشفته را لگام</p></div></div>