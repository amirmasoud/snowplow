---
title: >-
    شمارهٔ ۲۱۰
---
# شمارهٔ ۲۱۰

<div class="b" id="bn1"><div class="m1"><p>ای جهان را از قوام‌الدین مبارک یادگار</p></div>
<div class="m2"><p>روز نو بر تو مبارک‌باد و جشن نو بهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در چنین روزی سزد دست تو با جام شراب</p></div>
<div class="m2"><p>در چنین جشنی سزد چشم تو بر روی نگار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساعتی‌گویی به ساقی جام فرعونی بده</p></div>
<div class="m2"><p>لحظه‌ای گویی به مطرب صوت موسیقی بیار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوستان از ابر لؤلؤ بار و باد مشک بیز</p></div>
<div class="m2"><p>کرد پر مشک آستین وکرد بر لولوکنار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تاکند در جشن نوروز از کنار و آستین</p></div>
<div class="m2"><p>مشک ناب و لؤلؤ مکنون بدین مجلس نثار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نرگس آنگه جام زرین بر کف سیمین نهاد</p></div>
<div class="m2"><p>تا خورد یاد تو اندر پیش تخت شهریار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر بنفشه سوگ خصم تو نخواهد داشتن</p></div>
<div class="m2"><p>از چه معنی در لباس نیلگون شد سوگوار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از پی صید غلامانت‌کنون بر دشت وکوه</p></div>
<div class="m2"><p>باشد از مرغان و نخجیران قطار اندر قطار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فاخته بر سر‌و بن هر شب دعاگوید تورا</p></div>
<div class="m2"><p>وافرین‌ گوید تو را هر روز قمری بر چنار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر زمان از پَرِّ رنگین پیش تو طاوس نر</p></div>
<div class="m2"><p>چتر بو‌قلمون نماید پر کواکب جویبار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>او نه آگاه است کز بهر تو گر سازند چتر</p></div>
<div class="m2"><p>ماه زیبد چتر تو عَیوق زیبد چتر دار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر به هفت اختر نماید دولت تو جای خویش</p></div>
<div class="m2"><p>فخر او جویند وز هفت آسمان دارند عار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر نیی بر چرخ و هستی بر زمین نشگفت از آنک‌</p></div>
<div class="m2"><p>باشد اندر قعر د‌ریا جای دُرّ ِشاهوار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اشتقاق‌ کنیت و نامت ز فتح است و ظفر</p></div>
<div class="m2"><p>لاجرم عمر تو بر فتح و ظفر دارد مدار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر نظام‌الدین و فخرالملک خوانندت سزاست</p></div>
<div class="m2"><p>کز هنر هستی نظام‌الملک را فخر تبار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خواستار شغل شاهان نیستی لیکن تو را</p></div>
<div class="m2"><p>از پی امن جهان هستند شاهان خواستار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آمد از غزنین و بغداد اندرین مجلس گواه</p></div>
<div class="m2"><p>کز قوام‌الدین تویی ملک جهان را یادگار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بهره‌ور ما نی از آن مرکب‌که اندر باغ ملک</p></div>
<div class="m2"><p>سی‌ و شش‌ سال‌ است تا هستی بر آن مرکب سوار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر ز عدل کار فرمایی جهان را چاره نیست</p></div>
<div class="m2"><p>کار د‌ر دست تو نیکوتر که هستی مرد کار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جغد نتواند نمودن صنعتِ بازِ سفید</p></div>
<div class="m2"><p>غرم نتواند گرفتن جای شیر مرغزار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کلی و جزوی همی سرمایه باید چند چیز</p></div>
<div class="m2"><p>تا به استحقاق شغلی بر کسی گیرد قرار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بخت باید بی‌زوال و عقل باید بی‌مجاز</p></div>
<div class="m2"><p>جاه باید بی‌قیاس و مال باید بی‌شمار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا نباشد بخت دل در بر نباشد شادمان</p></div>
<div class="m2"><p>تا نباشد عقل جان در تن نباشد شادْخوار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا نباشد جاه در دلها کجا باشد شکوه</p></div>
<div class="m2"><p>تا نباشد ما‌ل دلها چون توان کردن شکار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هست کلی بخت و عقل و هست جزوی جاه و مال</p></div>
<div class="m2"><p>فخر مردم زین چهارست و تو داری هر چهار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در جوانمردان بسی بودند با شمشیر و تیر</p></div>
<div class="m2"><p>«‌لا فتی الّا علی لا سیف الّا ذوا‌لفقار»</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دیگران کوشند تا بر دشمنان توزند کین</p></div>
<div class="m2"><p>تو نکوشی زانکه داری نایبی چون روزگار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آید از صبر و سکون و از وقار و حلم تو</p></div>
<div class="m2"><p>خلق‌ گیتی را شگفتی و تعجب چند بار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون نگه کردند عجز خصم و اعجاز تو بود</p></div>
<div class="m2"><p>اندر آن صبر و سکون‌گر بود حیف و بردبار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هرکه یک جام شراب از کین تو برکف نهد</p></div>
<div class="m2"><p>زود گردد مست لیکن دیر گردد در خمار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تخم‌ کین کشتند و تیر دشمنی انداختند</p></div>
<div class="m2"><p>هست گفتی با تو هر یک را مصاف و کارزار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تیرشان نامَد صواب و تخمشان نامد ببر</p></div>
<div class="m2"><p>عمرشان زیر و زبر شد خان و ما‌نشان تیر و تار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر هنرمندان بسی هستند باتدبیر و رای</p></div>
<div class="m2"><p>نیست‌ کس را این خداوندی و جاه و اقتدار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جمله بگذشتند و گیتی را به تو بگذاشتند</p></div>
<div class="m2"><p>تو زگیتی مگذر وگیتی به‌شادی میگذار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دو ملک یزدان موکل کرد بر هر آدمی</p></div>
<div class="m2"><p>هر زمان‌ گویند هر یک بر یمین و بر یسار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ای حسود فخر ملک ا‌لاحتراز الاحتراز</p></div>
<div class="m2"><p>وی عدوی فخر ملک الاعتبار الا‌عتبار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گردتو باری‌حصاری ساخته‌است ازحفظ خویش</p></div>
<div class="m2"><p>باره و دیوار او چون قطب گردون استوار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کس نیاردگشت گرد باره و دیوار آن</p></div>
<div class="m2"><p>هر کجا باری بود باقی چنین باشد حصار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>انتظار و مهلت از مقصود تو دورست از آنک‌</p></div>
<div class="m2"><p>چرخ با تو یکدل است و بخت با تو سازگار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چرخ نگذارد که در مقصود تو مهلت رود</p></div>
<div class="m2"><p>بخت نپسندد که باشی مدتی در انتظار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گر ز بهر لذت دنیا شوی رامش فزای</p></div>
<div class="m2"><p>گر ز بهر نعمت عقبی شوی پرهیزکار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گر گماری لشکری بر کوهسار از جود خویش</p></div>
<div class="m2"><p>ابر نتواند که از صحرا برانگیزد غبار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>زانکه توقیع تو هست از دُرِّ مکنون پاکتر</p></div>
<div class="m2"><p>از سر کلک تو رشک آید صدف را در بحار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تیغ‌ گوهردار تو بی جنگ دارد فعل شیر</p></div>
<div class="m2"><p>کلک عنبر بار تو بی‌زهر دارد شکل مار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>خیر یزدان سنگ و آهن را ز حرمت نار داد</p></div>
<div class="m2"><p>تا میان سنگ و آهن نور پیدا شد ز نار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>وز پی آرایش بزم تو اندر کان خویش</p></div>
<div class="m2"><p>منعقد گشتند سیم نقره وَ زرِّ عیار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ور برآرند از پی‌ کین تو خصمان تو سر</p></div>
<div class="m2"><p>شیر و مار تو در آرند از سر خصمان دمار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ای چو نور شمس تابان نور تو قایم به ذات</p></div>
<div class="m2"><p>وز تو چون نور قمر جاه خلایق مستعار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>اختیار خلق‌ گیتی خدمت درگاه توست</p></div>
<div class="m2"><p>زان که خالق را تویی از خلق‌ گیتی اختیار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>با چو تو صدری که از خلق اختیار خالقی</p></div>
<div class="m2"><p>حال من بنده چرا باید به ضعف و اضطر‌ار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چون به نور حشمت توست این دیار افروخته</p></div>
<div class="m2"><p>زشت باشد جای دیگر رحلت من زین دیار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چند ره گفتی که کار او بباید ساختن</p></div>
<div class="m2"><p>تا بود د‌ر مجلس ما روز و شب خدمتگزار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چون به هشیاری نگفتی آنچه‌ گفتی در سراب</p></div>
<div class="m2"><p>با خرد گفتم کلامُ اللیلِ یَمْحُوهُ النّهار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بنگر این ریحان‌ که از نعت تو دارد رنگ و بوی</p></div>
<div class="m2"><p>بنگر این دیبا که از وصف تو دارد پود و تار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>مدحهای خویش بین چون کودکان جلوگی</p></div>
<div class="m2"><p>در لباس قیمتی در یاره و در گوشوار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>هر یکی را همت تو داده کابین‌ گزاف</p></div>
<div class="m2"><p>گاه د‌ر جشن خزان و گاه در جشن بهار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>یک هزار است آن و گر تاخیر باشد در اجل</p></div>
<div class="m2"><p>تا نه بس مدت به اقبال تو باشد صد هزار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>تا چو آید آفتاب از حوت در برج حمل</p></div>
<div class="m2"><p>روی ‌در کاهش نهد لیل و بیفزاید نهار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چون نهار اندر زیادت باد بخت عمر تو</p></div>
<div class="m2"><p>بخت عمر دشمنت چون لیل باد اندر بهار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>دولت اندر هر مکانی همنشینت باد و جفت</p></div>
<div class="m2"><p>ایزد اندر هر مقامی رهنمایت باد و یار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>افتخار عالم از اسحاقیان تا نفخ‌ صور</p></div>
<div class="m2"><p>وز تو تا روز شمار اسحاقیان را افتخار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>در دلت نور نشاط و بر سرت تاج شرف</p></div>
<div class="m2"><p>در برت ماه طراز و بر کفت جام عُقار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>جشن نوروزت همایون‌ بخت پیروزت ندیم</p></div>
<div class="m2"><p>خوشتر امروزت زدی و بهتر امسالت ز پار</p></div></div>