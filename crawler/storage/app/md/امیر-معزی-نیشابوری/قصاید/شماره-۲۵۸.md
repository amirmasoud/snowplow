---
title: >-
    شمارهٔ ۲۵۸
---
# شمارهٔ ۲۵۸

<div class="b" id="bn1"><div class="m1"><p>همی بنازد تیغ و نگین و تاج و سریر</p></div>
<div class="m2"><p>به شهریار ولایت گشای کشورگیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شه ملوک ملکشاه کز شمایل او</p></div>
<div class="m2"><p>فزود قیمت تیغ و نگین و تاج و سریر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز پادشاهی او روشن است دیدهٔ مهر</p></div>
<div class="m2"><p>چنان‌ کجا ز بصر روشن است چشم بصیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به‌ هر چه رای کند همسرش بود توفیق</p></div>
<div class="m2"><p>به هر چه روی نهد همرهش بود تقدیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به‌گرد رایت او آیتی نوشت قضا</p></div>
<div class="m2"><p>که روزگار همی نصرتش کند تفسیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دو جانب است ز شرق و ز غرب عالم را</p></div>
<div class="m2"><p>زهر دو جانب درگاه اوست مژده‌پذیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گهی ز جانب غربی رسد به حمل رسول</p></div>
<div class="m2"><p>گهی ز جانب شرقی رسد به فتح بشیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ظفر بخندد کز دست او بتابد تیغ</p></div>
<div class="m2"><p>اجل بگریدکز شست او بپرد تیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رود زَخمّ کمانش خدنگ جان اوبار</p></div>
<div class="m2"><p>چنانکه زخم شیاطین رود زچرخ اثیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حُسام او جگر حاسدان همی سوزد</p></div>
<div class="m2"><p>نه آتش است وچو آتش همی‌کند تأثیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نهال بندگی او امیری آرد بار</p></div>
<div class="m2"><p>که بندگانش سراسر همی شوند امیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>درخت دشمنی او اسیری آرد بار</p></div>
<div class="m2"><p>که دشمنانش یکایک همی شوند اسیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آیا شهی‌ که به جود تو نسبتی دارد</p></div>
<div class="m2"><p>نسیم باد صبا و سرشک ابر مطیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ملوک گنج به چنگ آورند و نشناسند</p></div>
<div class="m2"><p>که هست گنج همه پیش همت تو حقیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو شیری و همه شیران به پیش تو چو شغال</p></div>
<div class="m2"><p>تو بحری و همه شاهان به پیش تو چو غدیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سَخی شود به رضا جستن تو طبع بخیل</p></div>
<div class="m2"><p>غنی شود به ثناگفتن تو مرد فقیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>محبت تو دلیل است از ثواب بهشت</p></div>
<div class="m2"><p>عداوت تو نشان است از عذاب سعیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خیال دولت تو هرکه بیند اندر خواب</p></div>
<div class="m2"><p>مُعَبرّش همه نیک‌اختری کند تعبیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نکرد رای تو تقصیر در مصالح ملک</p></div>
<div class="m2"><p>سپهر هم نکند در هوای تو تقصیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نکرد عدل تو تأخیر در منافع خلق</p></div>
<div class="m2"><p>خدای هم نکند در مراد تو تا‌خیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دو معجزه که صلاح زمانه بپسندید</p></div>
<div class="m2"><p>حسام درکف توست و قلم به دست وزیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>درست شد که ز اهل حسام و اهل قلم</p></div>
<div class="m2"><p>تو را و او را ایزد نیافرید نظیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو تو ندید فلک در جلالت و تعظیم</p></div>
<div class="m2"><p>چو او نزاد فلک در کفایت و تدبیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز فرّ بخت تو در پیش تخت تو امروز</p></div>
<div class="m2"><p>جوان شدست دگرباره این مبارک پیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تو آفتابی و او پیش تو نشسته چو بَدر</p></div>
<div class="m2"><p>بود شگفت به هم آفتاب و بدر منیر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ضمیر و وهم شما را ثنا چگونه کنم</p></div>
<div class="m2"><p>که برگذشت ثنای شما ز وهم و ضمیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اگر بود به مثل رودکی در این ایام</p></div>
<div class="m2"><p>ز مدح هر دو شود عاجز و خورد تشویر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همیشه تا بنگاری چو مهر باشد مهر</p></div>
<div class="m2"><p>همیشه تا بنویسی چو شیر باشد شیر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تو مهرباش و همه بندگانت چون کوکب</p></div>
<div class="m2"><p>تو شیر باش و همه دشمنانت چون نخجیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دل زمانه به فرمان توگرفته قرار</p></div>
<div class="m2"><p>دو چشم ملک ز پیروزی تو گشته قریر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به دوستان تو از جود تو رسیده نفر</p></div>
<div class="m2"><p>به دشمنان تو از تیغ تو رسیده نفیر</p></div></div>