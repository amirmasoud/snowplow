---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>روی آن تُرک جهان آرای ماه روشن است</p></div>
<div class="m2"><p>زلف او در تیره شب بر ماه روشن جوشن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تاکه او را جوشن است از تیره شب بر طرف ماه</p></div>
<div class="m2"><p>راز من در عشق او پیدا چو روز روشن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا گلی نو بشکفد هر ساعتی بر روی او</p></div>
<div class="m2"><p>کوی ازو حون‌گلبببتان و خانه زو چون‌کلسن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو فَرخارست مجلس تا که او در مجلس است</p></div>
<div class="m2"><p>همچو کشمیرست برزن تا که او در برزن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون ببینی چشم او گویی شکفته نرگس است</p></div>
<div class="m2"><p>چون ببینی روی او گویی دمیده سوسن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سوسنی دیدی که گردش شاخه‌های سنبل است</p></div>
<div class="m2"><p>نرگسی دیدی که گردش نوکهای سوزن است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سنگ بر دل بندم اندر عشق آن زرین‌کمر</p></div>
<div class="m2"><p>زانکه همواره به زیر سنگ او دست من است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>او ز من منت ندارد گرچه او را شاهوار</p></div>
<div class="m2"><p>طوق زرین هر شبی از د‌ست من در گردن است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر کسی را رشک باشد بر بت معشو‌ق خویش</p></div>
<div class="m2"><p>رشک من دایم بر آن سنگین دل سیمین تن است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دشمنی جویم همی با آنکه او را هست دوست</p></div>
<div class="m2"><p>دوستی‌ گیرم همی با آنکه او را دشمن است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حور دیدارست لیکن بر سر دیدار او</p></div>
<div class="m2"><p>سِحر هاروت است و کَید و فتنهٔ اهریمن است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تیر مژگانش همی ناگاه بر د‌ل بگذرد</p></div>
<div class="m2"><p>راست‌ گویی نیزهٔ اسپهبد شیر اوژن است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در جهان اسپهبد شیر اوژن از روی هنر</p></div>
<div class="m2"><p>خسرو عالی اعلی شهریار قارن است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رکن اسلام و علاء دولت و شمس ملوک</p></div>
<div class="m2"><p>آن ‌که د‌ر مردی فزون از رستم و از بیژن است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نامور قطب‌المعالی آن که اندر صلح و جنگ</p></div>
<div class="m2"><p>مهربان چون اردشیر وکینه ‌‌کش چون بهمن است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گاه بخشش هر که بیند شخص او گوید مگر</p></div>
<div class="m2"><p>آفتاب اندر قبا و بحر ‌در پیراهن است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قاصد فتح و ظفر را موکب او مقصد است</p></div>
<div class="m2"><p>گوهر عز و شرف را مجلس او معدن است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نرم شد با او فلک گر با همه ‌کس سرکش است</p></div>
<div class="m2"><p>رام شد با او جهان‌گر با همه‌کس توسن است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سیرت او گردن ایام را چون زیور است</p></div>
<div class="m2"><p>همت او تارک اَجرام را چون ‌گرزن است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کرد نتوانم برابر کوه را با حلم او</p></div>
<div class="m2"><p>زانکه در میزان حلمش ‌کوه سنگ یک من است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در جحیم از بهر بدخواهان او بادافره است</p></div>
<div class="m2"><p>د‌ر بهشت از بهر مداحان او پاداشن است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون‌ کمان گیرد اجل با تیر او در معرکه است</p></div>
<div class="m2"><p>چون‌ کمین سازد ظفر با تیغ او در مکمن است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جوشن‌گردان چو غربال است از زوبین او</p></div>
<div class="m2"><p>مِغْفَر جنگ‌آوران با گرز او چون هاون است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پیش زوبین سپاه او سر اعدای او</p></div>
<div class="m2"><p>راست‌ گویی پیش مرغان دانه‌های ارزن است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اسب او را باد خوان وکوه دان از بهر آنک</p></div>
<div class="m2"><p>کوه شکلی بادپای و بادپایی‌ کُه‌کَن است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فتح زاید روز رزم از تیغ‌ گوهر دار او</p></div>
<div class="m2"><p>تیغ ‌گوهردار او گویی به‌ فتح آبستن است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هست ‌در آهن به ‌صنع ایزدی باس شدید</p></div>
<div class="m2"><p>زآنکه زوبین و رکاب و تیغ او از آهن است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای بلند اختر سپهداری که پیش شهریار</p></div>
<div class="m2"><p>نام تو اسپهبد شیر اوژن وگرد افگن است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دشمنت را چون توانم ‌گفت خرمن سوخته</p></div>
<div class="m2"><p>کز نیاز و تنگدستی دشمنت بی‌خرمن است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از وفاقت در وثاق نیکخواهان هست سور</p></div>
<div class="m2"><p>وز خلافت در سرای بدسگالان شیون است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در سر خصم تو مغفر معجرست از بهر آنک</p></div>
<div class="m2"><p>در صفت مردست لیکن در هنر همچون زن است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هست واجب مدح تو کز جملهٔ اسپهبدان</p></div>
<div class="m2"><p>سیرت تو مُستَفاد و رسم تو ‌مُستَحسَن است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>روشنی باشد همی از مدح تو روح مرا</p></div>
<div class="m2"><p>روح من ‌گویی چراغ و مدح تو چون روغن است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>عیب نتوان کرد بر مدحی‌ که من ‌گویم تو را</p></div>
<div class="m2"><p>کانچه در مدح تو گویم خالی از زرق و فن است</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>می ستان از ماه دیبا روی‌ کاندر بوستان</p></div>
<div class="m2"><p>نه چمن دیبا لباس و نه هوا دیبا تن است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گر زمین و کوه پرکافور شد نشگفت از آنک</p></div>
<div class="m2"><p>برف چون ‌کافور سوده ابر چون پرویزن است</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نیست اکنون وقت صحرا و شکار و تاختن</p></div>
<div class="m2"><p>موسم کاشانه و آسودن و می خوردن است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آتشی ‌باید که پوشاند هوا را عکس او</p></div>
<div class="m2"><p>جامه‌ای کان را گریبان ماه و ماهی دامن است</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>آن‌که شاه‌ گوهران است و جزای کافران</p></div>
<div class="m2"><p>تحفهٔ ماه دی و ریحان ماه بهمن است</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>لون او گه زرد و همچون زعفران و شنبلید</p></div>
<div class="m2"><p>رنگ او گه سرخ و همچون ارغوان و روین است</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اخگر او زیر خاکستر تو گویی از قیاس</p></div>
<div class="m2"><p>مبرم و شعر خلوقی زیر شعر ادکن است</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تا همی هر شب چو بینی لون و شکل اختران</p></div>
<div class="m2"><p>چرخ را گویی به روز اندر هزاران روزن است</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تا همیشه اهرمن را هست ماوا در سقر</p></div>
<div class="m2"><p>تا که در خُلد برین روح‌الامین را مسکن است</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>اهرمن روز و شب از پیرامن تو دور باد</p></div>
<div class="m2"><p>زانکه سال و مه تو را روح‌الامین پیرامن است</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>از نوای چنگ و بربط بزم تو خالی مباد</p></div>
<div class="m2"><p>تا خروش چنگ ساز و غلغل بربط زن است</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>باعث‌ کار صبوحت باد وقت صبحدم</p></div>
<div class="m2"><p>بانگ آن مرغی که او میخوارگان را مُؤذن است</p></div></div>