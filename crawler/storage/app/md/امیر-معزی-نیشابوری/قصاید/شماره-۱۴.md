---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>باغ شد از ابر پر ز لؤلؤ لالا</p></div>
<div class="m2"><p>راغ شد از باد پر ز عنبر سارا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد به هوا درگسسته رشتهٔ گوهر</p></div>
<div class="m2"><p>شد به زمین برگشاده اَزهَر دیبا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوه ز لاله‌گرفت سرخی بُسََّد</p></div>
<div class="m2"><p>دشت ز سبزه گرفت سبزی مینا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طَرف چمن هست پرهلال و دو پیکر</p></div>
<div class="m2"><p>شاخ ‌سَمَن‌ هست پر سُهیل و ثُریّا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در شده با شنبلید لاله به عشرت</p></div>
<div class="m2"><p>درشده با خیری ارغوان به تماشا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون رخ مجنون نهاده بر رخ لیلی</p></div>
<div class="m2"><p>چون لب وامق نهاده بر لب عَذْرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گلبن تا تازه شد چو لعبت دلبر</p></div>
<div class="m2"><p>بلبل بیچاره شد چو عاشق شیدا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سیل ز بالا نهاد روی به پستی</p></div>
<div class="m2"><p>مهر ز پستی نهاد روی به بالا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برق درخشان چو تیغ خسرو عالم</p></div>
<div class="m2"><p>ابر درا‌فشان چو دست مهتر والا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عارض دنیا جمال دین ثقه‌الملک</p></div>
<div class="m2"><p>بار خدای نژاد آدم و حوا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خواجه ابوجعفر آن که از هنر او</p></div>
<div class="m2"><p>دهر مزین شده است و ملک مُهنّا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از صفت مهتری هر آنجه بباید</p></div>
<div class="m2"><p>دارد و در مهتری ندارد همتا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پست بود پیش او بلندی گردون</p></div>
<div class="m2"><p>خُرد بود پیش او بزرگی دنیا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>با نظرش بر دمد بنفشه ز آهن</p></div>
<div class="m2"><p>با کرمش بشکفد شکوفه ز خارا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زیر رکابش ز روم تا به سمرقند</p></div>
<div class="m2"><p>زیر حُسامش ز شام تا به بخارا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بحرْ بَرِ جود او چو قطره به جیحون</p></div>
<div class="m2"><p>کوه بَرِ حلم او چو صخره به صحرا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پیش مثالش کند ستاره تواضع</p></div>
<div class="m2"><p>پیش مرادش کند زمانه مدارا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هست مقدس عطای او ز توقف</p></div>
<div class="m2"><p>هست منزّه سَخای او ز تقاضا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای همه ناراستی ز کِلکِ تو پنهان</p></div>
<div class="m2"><p>وی همه آزادگی ز طبع تو پیدا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تاجوران را به مهر توست تَقرب</p></div>
<div class="m2"><p>ناموران را به شُکر توست تولّا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چاکری توست آز را شده مَقطَع</p></div>
<div class="m2"><p>بندگی توست ناز را شده مَبد‌ا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از دلِ تو نور یافت چشمه خورشید</p></div>
<div class="m2"><p>وز سر تو سبز گشت گنبد خَضر‌ا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هست دو چشم هنر به رسم تو روشن</p></div>
<div class="m2"><p>هست زبان خرد به مدح تو گویا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مُعتَکِف درگهت چه بخت و چه دولت</p></div>
<div class="m2"><p>مُعتَرفِ دا‌نشت چه پیر و چه برنا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همت تو هست همچو رای تو عالی</p></div>
<div class="m2"><p>دولت تو هست همچو بخت تو والا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کِلک تو در مَر‌تَبِت‌ چو مهر سلیمان</p></div>
<div class="m2"><p>دست تو در معجزه چو باد مسیحا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خاک قدمهای توست عَنبر اَ‌شهَب‌</p></div>
<div class="m2"><p>نقش‌ قلم‌های توست لؤلؤ لالا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>قاهر اعداست دولت تو وزین روی</p></div>
<div class="m2"><p>فکرت و سودا رسید قسمت اعدا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>قهر بر آن گمرهان قضای خدایی‌است</p></div>
<div class="m2"><p>باز نگردد قضا به فکرت و سودا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ایزد دادار مهر و کین تو گویی</p></div>
<div class="m2"><p>از شب قدر آفرید و از شب یلدا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زانکه به مهرت بود تقرّب مؤمن</p></div>
<div class="m2"><p>زانکه به کینت بود تفاخر ترسا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بار خدا ز فرّ جود تو شعرم</p></div>
<div class="m2"><p>هست‌ ‌کشیده عَلَم به عالم اعلا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>لفظ من از اِهتمام توست مُهَذّب</p></div>
<div class="m2"><p>طبع من از اهتزاز توست مُصّفا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>قامت من پیش تو دوتاست ولیکن</p></div>
<div class="m2"><p>هست دلم در وفا و مهر تو یکتا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خاطر من جَنّت است و مهر تو رضوان</p></div>
<div class="m2"><p>شکر تو طوبی و آفرین تو حورا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تا که همی بُرج زهره باشد میزان</p></div>
<div class="m2"><p>تا که همی برج مهر باشد جَوزا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چاکر بخت تو باد مهر مُنَوّر</p></div>
<div class="m2"><p>بندهٔ رای تو باد زُهرهٔ زهرا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بزم تو افروخته به شمسهٔ خَلُّخ</p></div>
<div class="m2"><p>رزم تو آراسته به دلبر یغما</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بر سر تو تاج بخت و افسر دولت</p></div>
<div class="m2"><p>درکف تو زلف یار و ساغر صهبا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>رسم تو زیبا و روزگار تو خرم</p></div>
<div class="m2"><p>بر تو خجسته بهار خرم و زیبا</p></div></div>