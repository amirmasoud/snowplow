---
title: >-
    شمارهٔ ۳۲۹
---
# شمارهٔ ۳۲۹

<div class="b" id="bn1"><div class="m1"><p>طبع‌ گیتی سرد گشت از باد فصل مهرگان</p></div>
<div class="m2"><p>چون دم دلدادگان از هجر یار مهربان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هجر یار مهربان‌ گر چهر را زردی دهد</p></div>
<div class="m2"><p>بوستان را داد زردی وصل باد مهرگان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در هوا و در چمن پوشید سنجأب و نسیج</p></div>
<div class="m2"><p>کوه دیباپوش را داد از مُشَجّر طَیلَسان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شَنبلیدی‌ گشت ز آشوبش ثِیابِ مرغزار</p></div>
<div class="m2"><p>زعفرانی‌ گشت ز آسیبش درخت بوستان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باد در آشوب او بِنهُفت‌ گویی شنبلید</p></div>
<div class="m2"><p>ابر در آسیب او بِسرِشت‌ گویی زعفران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نگشت از زرّ پالوده چمن سرمایه‌دار</p></div>
<div class="m2"><p>ور نگشت از درّ ناسفته هوا بازارگان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از چه معنی‌ گشت باد اندر چمن دینار بار</p></div>
<div class="m2"><p>وز چه معنی‌ گشت ابر اندر چمن لؤلؤفشان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرد و پژمرده شدست اکنون چمن چون طبع پیر</p></div>
<div class="m2"><p>چند گه‌ گر بود گرم و تازه چون طبع جوان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر جهان پژمرده شد هرگز نباشد هیچ باک</p></div>
<div class="m2"><p>تا جوان و تازه باشد دولت شاه جهان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شاه شاهان سایهٔ یزدان ملک سلطان که هست</p></div>
<div class="m2"><p>طلعتش چون آفتاب و حضرتش چون آسمان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پادشاهی کز جلالش هست رفعت پایدار</p></div>
<div class="m2"><p>شهریاری کز جمالش هست دولت جاودان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دین به عدل وجود او تازه است همچون دل به دین</p></div>
<div class="m2"><p>جان به مهر و مدح او زنده است همچون تن به جان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر به مغرب بگذری از عدل او یابی اثر</p></div>
<div class="m2"><p>ور به مشرق بنگری از جود او یابی نشان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یک روان از مهر او خالی نبینی در بدن</p></div>
<div class="m2"><p>یک زبان از مدح او فارغ نبینی در دهان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>طاعتِ یزدان اگر در عقل و دانش واجب است</p></div>
<div class="m2"><p>خدمتِ سلطان عالم هست واجب همچنان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر که او در طاعت یزدان همی بندد کمر</p></div>
<div class="m2"><p>همچنان در خدمت سلطان همی بندد میان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شهریارا بر فلک جِرم زُحَل در بُرج قَوس</p></div>
<div class="m2"><p>لرزه‌گیرد چون تو را بیند به‌کف تیر و کمان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همچنان کز چشمهٔ خورشید عالم روشن است</p></div>
<div class="m2"><p>روشن است از دولت تو گوهر الب‌ارسلان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر ز بخت و چرخ باشد پادشاهی مستقیم</p></div>
<div class="m2"><p>بخت با تو یکدل است و چرخ با تو یکزبان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آن یکی‌ گوید زه ای خورشید ناپیدا زوال</p></div>
<div class="m2"><p>وین دگر گوید زه‌ای دریای ناپیدا کران</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر کند تقدیر از عدل تو روزی اِقتِراح</p></div>
<div class="m2"><p>ورکند توفیق از جود تو وقتی امتحان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بی‌بزرگی کس خداوندی نیابد بر مجاز</p></div>
<div class="m2"><p>بی‌هنر صاحبقرانی‌ کس نیابد رایگان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون تورا دادست یزدان هم بزرگی هم هنر</p></div>
<div class="m2"><p>ملک و دین را هم خداوندی و هم صاحبقران</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تاکه هر نفسی ز تدبیر هنر باشد عزیز</p></div>
<div class="m2"><p>تا که هر جسمی ز تأثیر روان باشد روان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در ستایش بیش تو بادا همه ساله خرد</p></div>
<div class="m2"><p>در پرستش باد پیش تو همه ساله روان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>رای ملک افروز تو بر هر چه باشد کامکار</p></div>
<div class="m2"><p>دولت پیروز تو بر هر که خواهد کامران</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عالم از تو چون بهار خرم و فصل بهار</p></div>
<div class="m2"><p>بر تو فرخنده خزان فرخ و جشن خزان</p></div></div>