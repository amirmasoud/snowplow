---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>ایا ستارهٔ خوبان خَلُّخ و یغما</p></div>
<div class="m2"><p>به دلبری دل ما را همی زنی یغما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو تو نگار دل افروز نیست ‌در خَلُّخ</p></div>
<div class="m2"><p>چو تو سوار سرافراز نیست در یغما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غنوده همچو دل تنگ ماست دیدهٔ تو</p></div>
<div class="m2"><p>خمیده همچو سر زلف توست قامت ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکنجِ زلف تو شب را همی دهد سیهی</p></div>
<div class="m2"><p>فروغ روی تو مه را همی دهد سیما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همی حسد برد از صورت تو حور بهشت</p></div>
<div class="m2"><p>همی خِجِل شود از طلعت تو ماه سَما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز مُشک سلسله داری نهاده بر خورشید</p></div>
<div class="m2"><p>ز شیر دایره داری کشیده بر دیبا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به ارغوان تو بَر هست سنبل خوشبوی</p></div>
<div class="m2"><p>به پرنیان تو بر هست عنبر سارا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرفته‌ای تو به یاقوت لُؤلُؤ مَکنون</p></div>
<div class="m2"><p>نهفته‌ای تو به هاروت زُهرهٔ زهرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تویی به حسن چو لیلی منم تو را مجنون</p></div>
<div class="m2"><p>منم به عشق چو وامق تویی مرا عَذ‌را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سر مرا همه ساله ز عشق توست خمار</p></div>
<div class="m2"><p>دل مرا همه روزه به روی توست هوا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خمار تو به سر اندر بود به جای خرد</p></div>
<div class="m2"><p>هوای تو به دل اندر بود به جای وفا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سخن به وصف تو گردد همی بزرگ خطر</p></div>
<div class="m2"><p>غزل به ‌نَعت تو گردد همی تمام بها</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر آن غزل‌که تو را گویم ای غزال لطیف</p></div>
<div class="m2"><p>بود مقدمهٔ مدح سَیّد الرّوءسا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مُعین مُلکِ مَلک‌بوالمحاسن مُحسن</p></div>
<div class="m2"><p>کریم خوب سیر مهتر خجسته لقا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بزرگواری‌، آزاده‌ای‌، خداوندی</p></div>
<div class="m2"><p>که از کفایت او چشم عقل شد بینا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از آن قِبل‌ که صبا را ز دست او اثرست</p></div>
<div class="m2"><p>جهان گشاده و خرم شود ز دست صبا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رجا و خوف خلایق بود ز همّت او</p></div>
<div class="m2"><p>بود به همّت او بازگشت خوف و رجا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به رأی پاک هنر را همی‌کند یاری</p></div>
<div class="m2"><p>به رسم خوب خرد را همی دهد یارا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نه دولت است و چو دولت ندانمش مانند</p></div>
<div class="m2"><p>نه ایزدست و جو ایزد نبنمش همتا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ایا مُتابعِ فرمان تو همیشه قَدَر</p></div>
<div class="m2"><p>و یا موافق تدبیر تو همیشه قضا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به مهر توست یمین خلیفه خورده یمین</p></div>
<div class="m2"><p>به وصل توست رضی الامام داده رضا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بزرگی و کرم از تو گرفت رونق و فر</p></div>
<div class="m2"><p>چو تو کریم کدام و چو تو بزرگ کجا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خدا به شخص تو از کبریا نهاد سرشت</p></div>
<div class="m2"><p>که در نهاد و سرشت تو نیست کبر ‌و ریا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بود ز ملک تو طَغرای شاه را زینت</p></div>
<div class="m2"><p>بود زمهر تو اجرام چرخ را بالا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به‌ خامهٔ تو شود حُجّت فتوح‌، روان</p></div>
<div class="m2"><p>به نامهٔ تو شود حاجت ملوک‌، روا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>قمر ز قبضهٔ شمشیر توست ناایمن</p></div>
<div class="m2"><p>زُحَل ز پیکر پیکان توست ناپروا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سزد خدنگ تو را پَر زبانهٔ مرّیخ</p></div>
<div class="m2"><p>سزد کمان تو را ز‌ه قلادهٔ جوزا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز نوکِ نیزهٔ تو کافران همی ترسند</p></div>
<div class="m2"><p>از آن قِبَل لقبِ کافران بود تَرسا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدان زمانه که موسی نمود معجز خویش</p></div>
<div class="m2"><p>شکست جادویی جادوان به‌ دست عصا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به تیغ و کِلک دل دشمنان تو بشکستی</p></div>
<div class="m2"><p>نه چوب جانورت بود و نه یدِ بیضا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ایا چو دست تو دریا بزرگ و بابخشش</p></div>
<div class="m2"><p>و یا چو رای تو گردون بلند و با پهنا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز نور روی تو اختر بتابد از گردون</p></div>
<div class="m2"><p>ز مهر دست تو گوهر برآید از دریا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو شاعر از تو نعم بشنود رسد به نعم</p></div>
<div class="m2"><p>چو زایر از تو بلی بشنود رهد ز بلا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شریف حضرت تو کعبهٔ بزرگان است</p></div>
<div class="m2"><p>دل تو چشمهٔ زمزم ‌کف تو رکن و صفا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اگر ز حاتم طی شاعران سخن رانند</p></div>
<div class="m2"><p>دهند جمله ‌گواهی بر او به جود و سخا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تو را به دست ‌گهربار بر ده انگشت است</p></div>
<div class="m2"><p>که بر سخاوت ‌وجود تو گشته‌اند گوا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بلند بختا در مدح تو قصاید من</p></div>
<div class="m2"><p>مرصع است به یاقوت و لولو لالا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ستوده دارم عقل و گزیده دارم بخت</p></div>
<div class="m2"><p>که عقل من‌ نه عِقاب است و خط من نه خطا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هر آن گهی که ثنای تو پرورد طبعم</p></div>
<div class="m2"><p>کند ثنای تو بر طبع من به ‌طبع‌ ثنا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>امارت شعرا با هزار خلعت خوب</p></div>
<div class="m2"><p>به اهتمام تو دادست شهریار مرا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>که یافته است مگر من به ‌فر دولت تو</p></div>
<div class="m2"><p>هزار خلعت شاه و اَمارت شعرا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>همیشه تا که بود دهر را صلاح و فساد</p></div>
<div class="m2"><p>همیشه تا که بود خلق را بقا و فنا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>صلا‌ح کار معادیت باد جمله فساد</p></div>
<div class="m2"><p>فنای عمر موا‌لیت‌ باد جمله بقا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سه چیز باد تو را جاودان و بی‌پایان</p></div>
<div class="m2"><p>تن درست و دل شاد و دولت برنا</p></div></div>