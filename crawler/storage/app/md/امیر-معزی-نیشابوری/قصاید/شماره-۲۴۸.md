---
title: >-
    شمارهٔ ۲۴۸
---
# شمارهٔ ۲۴۸

<div class="b" id="bn1"><div class="m1"><p>ترک نزاید چنو به کاشغر اندر</p></div>
<div class="m2"><p>سرو نروید چنو به عاتفر اندر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوب تر از عارضش ندید و نبیند</p></div>
<div class="m2"><p>هیج کسی پرنیان به شوشتر اندر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هست دو زلفش همیشه پرشکن و بند</p></div>
<div class="m2"><p>بند و شکن هر یکی بهٔک‌دگر اندر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عمداگویی کسی ز عنبر سارا</p></div>
<div class="m2"><p>سلسله بسته است گرد مُعصَفَر اندر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون قمرستش رخان و هست شب و روز</p></div>
<div class="m2"><p>روشنی چشم من بدان قمر اندر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون شکرستش لبان و هست مه و سال</p></div>
<div class="m2"><p>آرزوی طبع من بدان شکر اندر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشق آن دلبرم که هر شب و هر روز</p></div>
<div class="m2"><p>طعنه زند روی او به ماه و خور اندر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از دل بی‌رحمتش نهاد خداوند</p></div>
<div class="m2"><p>غایت سختی به آهن و حجر اندر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر بشناسد که آب دارم و آتش</p></div>
<div class="m2"><p>از غم عشقش به دیده و فکر اندر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز آتش و آبم بترسد و نگذارد</p></div>
<div class="m2"><p>تا دهمش بوس و گیرمش به ‌بر اندر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ماهِ تمام است در جمال و ملاحت</p></div>
<div class="m2"><p>نیست نظیرش به عالم صور اندر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از غم آن ماه بی‌نظیر بنالم</p></div>
<div class="m2"><p>پیش خداوند مشتری نظر اندر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سید آزادگان که از شرف اوست</p></div>
<div class="m2"><p>جان به تن ملک شاه دادگر اندر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کُنیت و نامش حساب سَعد و محامِد</p></div>
<div class="m2"><p>کرد فَذلک به دفتر هنر اندر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هست کریمی که شد به نامش منسوخ</p></div>
<div class="m2"><p>نام ‌کریمان به قصه و سمر اندر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون گهرست او و روزگار چو بحرست</p></div>
<div class="m2"><p>بحر چه داند به قیمت ‌گهر اندر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هست چو خورشید در میانهٔ اجرام</p></div>
<div class="m2"><p>مرتبت او به نسبت پدر اندر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پیر و جوان را ز طلعتش بفزاید</p></div>
<div class="m2"><p>نور طبیعی به قوت بصر اندر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>طلعت او روشن است و هست همانا</p></div>
<div class="m2"><p>روشنی او به کوکب سحر اندر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای به سزا مهتری که از هنر و عقل</p></div>
<div class="m2"><p>نیست مثالت میانهٔ بشر اندر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هر که ز مهر تو آب روی نجوید</p></div>
<div class="m2"><p>سوخته‌گردد به آتش سقر اندر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در نظر دوستان خویش نگه کن</p></div>
<div class="m2"><p>شاد و سراسر به نصرت و ظفر اندر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>راست تو گویی که کردگار نهادست</p></div>
<div class="m2"><p>مهر سعادت به دست آن نظر اندر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در حَشَر دشمنان خویش نگه کن</p></div>
<div class="m2"><p>راست یکایک به محنت و ضرر اندر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>راست تو گویی که کردگار کشیدست</p></div>
<div class="m2"><p>خط سقاوت به‌گرد آن حشر اندر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر که ‌کمر بست بر وفاق تو بستیش</p></div>
<div class="m2"><p>عز مُخّلد به بند آن کمر اندر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کین و خلاف تو آتشی است ‌که دارد</p></div>
<div class="m2"><p>مرگ مخالف به شعله و شرر اندر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کلک تو ابری است‌ کش مطر همه درّ است</p></div>
<div class="m2"><p>مشک سرشته به‌ قطرهٔ مطر اندر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون شود اندر بنان تو درر افشان</p></div>
<div class="m2"><p>خیره کند عقل را بدان درر اندر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>باز چو از نقش مشک سازد بر سیم</p></div>
<div class="m2"><p>گیرد روی مخالفان به‌ زر اندر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>او شجر دولت است و تو به‌ کفایت</p></div>
<div class="m2"><p>دست زده استی به اصل آن شجر اندر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نقش حساب تو تا بود ثمر او</p></div>
<div class="m2"><p>سجده‌کند پیش تخت آن ثمر اندر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بار خدایا چو من مدیح تو خوانم</p></div>
<div class="m2"><p>پیش جهان دیدگان نامور اندر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از شرف نام تو کشند چو سرمه</p></div>
<div class="m2"><p>خاک کف پای تو به چشم سر اندر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>من رهی انعام تو ز بهر تفاخر</p></div>
<div class="m2"><p>فاتحه کردم به نامه و سیر اندر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>وز قبل مدح تو جواهر معنی</p></div>
<div class="m2"><p>مرسله‌کردم به خاطر و فکر اندر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تا ظفر و حشمت است اهل خرد را</p></div>
<div class="m2"><p>باش تن آسان به حشمت و ظفر اندر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گشته زیادت ز عمر و جاه تو هر روز</p></div>
<div class="m2"><p>قربت تو پیش شاه تاجور اندر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تو ز بر بخت بر مدار سعادت</p></div>
<div class="m2"><p>دشمن تو زیر تخته و مدر اندر</p></div></div>