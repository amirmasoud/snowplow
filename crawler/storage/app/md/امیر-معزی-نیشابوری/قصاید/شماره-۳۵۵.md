---
title: >-
    شمارهٔ ۳۵۵
---
# شمارهٔ ۳۵۵

<div class="b" id="bn1"><div class="m1"><p>تازه و نو شد ز فر باد فروردین جهان</p></div>
<div class="m2"><p>خرم و خوش‌ گشت کوه و دشت و باغ و بوستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرد پنداری زمین را آسمان چون خویشتن</p></div>
<div class="m2"><p>کزگل و سبزه زمین دارد نهاد آسمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زند خواند هر زمان بلبل به باغ اندر همی</p></div>
<div class="m2"><p>زند باف است او به لفظ پارسی پاز‌ندخوان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوه شد چون پرنیان و لاله شد همچون علم</p></div>
<div class="m2"><p>سرخ نیکوتر علم چون سبز باشد پرنیان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیلگون آمد بنفشه زعفران‌گون‌ شنبلید</p></div>
<div class="m2"><p>آن همانا نیل بودست این همانا زعفران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لعل‌گویی از بدخشان نقب بر زد بر زمین</p></div>
<div class="m2"><p>وز زمین بر رفت پنداری به شاخ ارغوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برق در ابر و سیاهی در میان لاله هست</p></div>
<div class="m2"><p>همچو آتش در دخان و همچو در آتش دخان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جون برآید ابر پرسیمرغ‌گردد روی چرخ</p></div>
<div class="m2"><p>پرِّ هر سیمرغ بر روی زمین گوهرفشان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از هوا هر ساعتی بر ابر بدرخشد درخش</p></div>
<div class="m2"><p>چون زگرد معرکه تیغ شه‌گیتی ستان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ناصر دین خسرو مشرق ملک‌سنجر که ملک</p></div>
<div class="m2"><p>یافت میراث از ملک سلطانُ از الب ارسلان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن جهانداری که هست اندر خراسان جیش او</p></div>
<div class="m2"><p>جوش او در ماوراء‌النهر و در زابلستان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پادشاهان پنج چیز او را مسلم کرده‌اند</p></div>
<div class="m2"><p>خاتم و شمشیر و تاج و تخت وگنج شایگان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از بنات‌النعش قصر بخت او را کنگره است</p></div>
<div class="m2"><p>وز ثریا درگه اقبال او را آستان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چرخ باشد زیر پایش هر کجا ساید رکاب</p></div>
<div class="m2"><p>دهر باشد زیر دستش هر کجا تابد عنان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر بود رایش که دریابد زمان رفته را</p></div>
<div class="m2"><p>چون ستاره گاه رجعت باز پس‌ گردد زمان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر بلرزد نیزه اندر دست او روز نبرد</p></div>
<div class="m2"><p>مرد در جوشن بلرزد اسب در بَرْگُستوان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هم بر آن‎سان کز زُمرُّد چشم افعی بِتَرکَد</p></div>
<div class="m2"><p>چشم دشمن بترکد چون او بگرداند سنان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر سعادتهای او گردون گردان داد خط</p></div>
<div class="m2"><p>زهره شد بر خط‌ گواه و مشتری شد در ضمان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تاکه هامون پست باشد رای او باشد بلند</p></div>
<div class="m2"><p>تا که‌ گردون پیر باشد بخت او باشد جوان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای جهان‌آرای شاهی‌ کز مبارک رای توست</p></div>
<div class="m2"><p>دولت و دین را ز بیداد و بدی امن و امان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اختیارست از جهان اقلیم رابع خلق را</p></div>
<div class="m2"><p>در خط فرمان توست آنچ اختیارست از جهان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون خرد شکر تو گوید، جان‌ کُند شکر خرد</p></div>
<div class="m2"><p>چون زبان مدح تو گوید دل‌ کند مدح زبان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ملک و دین را از تو نگزیرد چو نگزیرد همی</p></div>
<div class="m2"><p>دیده را از روشنایی کالبد را از روان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سر فرازد آسمان‌ گر بر میان خویشتن</p></div>
<div class="m2"><p>آن کمر بیند که دربان تو دارد برمیان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نیزه و شمشیر و تیر لشکرت روز مصاف</p></div>
<div class="m2"><p>کرد در صحرای تِرْمَذ دام و دد را میهمان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>میهمان نشگفت اگر باشد به صحرا دام و دد</p></div>
<div class="m2"><p>هرکجا شمشیر و تیر و نیزه باشد میزبان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هر امیر از لشکرت بر لشکری شد کامکار</p></div>
<div class="m2"><p>هر غلام از مرکبت بر مرکبی شد کامران</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از امیران و غلامان تو رشک آید همی</p></div>
<div class="m2"><p>مهر ومه را بر سبهر و حورعین را بر جنان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر خلاف تو قدرخان‌ کرد پیدا بر زمین</p></div>
<div class="m2"><p>حشمت و قَدر قَدَرخان در زمین کردی نهان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آن غرور اندر سر او دشمنی دیگر نهاد</p></div>
<div class="m2"><p>کز میان چون جست از تیغ تو چون تیر ازکمان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آن‌ یکی‌ از بیم تیرت چون‌ کمان خم داد پشت</p></div>
<div class="m2"><p>وین دگر جست از سر تیغ تو چون تیر ازکمان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هر دو را بارگران از خوی بد درگردن است</p></div>
<div class="m2"><p>هست معروف این مثل‌: خوی بدو بارگران</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گرچه بود اندر گمان خصم پیروزی و فتح</p></div>
<div class="m2"><p>ایزدش روزی نکرد از هر چه بود اندر گمان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>رزم تو دریای جوشان‌ گشت و تیغت چون نهنگ</p></div>
<div class="m2"><p>سر ز دریا برزد و ناگه کشیدش در دهان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آنچه با او شاه ماضی کرده بود از نیکویی</p></div>
<div class="m2"><p>شد فراموش از دلش تاکرد جان و تن زبان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هرکه بدکاری کند ناگه نهد بر خاک‌ سر</p></div>
<div class="m2"><p>هرکه بدعهدی کند ناگه دهد بر باد جان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چون اجل را برکران عمر او افتاد چشم</p></div>
<div class="m2"><p>آمد از توران به جیحون با سپاهی بی‌کران</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مار و مرغ آری چو سنگ و دام را درخور شوند</p></div>
<div class="m2"><p>مار بیرون آید از سوراخ و مرغ از آشیان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هرکه با تو سرکشد تا پیش تو لشکر کشد</p></div>
<div class="m2"><p>باشد انجامش چنین و باشد آغازش چنان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نصرت تو روز دهرافروز را ماند همی</p></div>
<div class="m2"><p>روز دهر افروز را انکارکردن کی توان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>آنکه عصیان کرد ملک از دست او ناگه برفت</p></div>
<div class="m2"><p>وانکه فرمان برد ملک آمد به‌دستش رایگان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چاکر فرمان توست و بندهٔ احسان توست</p></div>
<div class="m2"><p>آنکه اکنون در دیار ماورالنهرست خان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نام سلجوق از جهان هرگز نگردد منقطع</p></div>
<div class="m2"><p>تا چو تو شاهی بود سلجوق را در دودمان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>یک‌ گهر باشد کزو قیمت فزاید عقل را</p></div>
<div class="m2"><p>یک پسر باشد کزو باقی بماند خاندان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هرکجا لشکرکشی اقبال باشد پیشرو</p></div>
<div class="m2"><p>تا بود اینانج بک در لشکر تو پهلوان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>با دلیری‌های او منسوخ گشت اندر عجم</p></div>
<div class="m2"><p>آن دلیری‌ها که رستم کرد در مازندران</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>لشکر افروزند و ملک‌آرای پیش تخت تو</p></div>
<div class="m2"><p>این سپهسالار عادل وان وزیر مهربان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>زین مبارکتر سپهداری و دستوری ندید</p></div>
<div class="m2"><p>د‌ولت افراسیاب و حضرت نوشین روان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تا بود در عالمِ سِفلی طبایع را مزاج</p></div>
<div class="m2"><p>تا بود در عالم عِلوی کواکب را قران</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>باکواکب باد پیمانت در این عالم درست</p></div>
<div class="m2"><p>بر طبایع باد فرمانت در این عالم روان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تارک دشمن ز تیغ آبدارت خاکسار</p></div>
<div class="m2"><p>باد انصاف تو اندر مملکت آتش نشان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دولت از تو سرفراز و تو ز دولت سرفراز</p></div>
<div class="m2"><p>لشکر از تو شادمان و تو زلشکر شادمان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>اختیار تو همه پیروزی و نیک‌اختری</p></div>
<div class="m2"><p>روزگار تو همه نوروز و عید و مهرگان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>شاکر و راضی به تو جان معزّالدین به خلد</p></div>
<div class="m2"><p>پیش تخت تو معزّی شعر گوی و مَدح خوان </p></div></div>