---
title: >-
    شمارهٔ ۳۹۶
---
# شمارهٔ ۳۹۶

<div class="b" id="bn1"><div class="m1"><p>شد خراسان به‌سان خلد برین</p></div>
<div class="m2"><p>در راحت گشاد روحِ امین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا رسید از عراق خرم و شاد</p></div>
<div class="m2"><p>سیف دولت امیر شمس‌الدین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن امیری که رای روشن او</p></div>
<div class="m2"><p>خاتمِ مُلک را شدست نگین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اجل آنجاست کاو کشید کمان</p></div>
<div class="m2"><p>نصرت آنجاست کاو گشاد کمین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش سلطان ملک‌که بود چو او</p></div>
<div class="m2"><p>به قبول و به حشمت و تمکین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در عزیزی چو او کرا دارد</p></div>
<div class="m2"><p>برکیارق که هست شاه زمین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر بگردی ز روم تا حد هند</p></div>
<div class="m2"><p>ور بپویی ز مصر تا در چین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تازه‌تر زو نیایی اندر صدر</p></div>
<div class="m2"><p>چیره‌تر زو نیابی اندر زین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دو سپه را سپاه سالارست</p></div>
<div class="m2"><p>که روانها به مهر اوست رهین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ظفر و فتح را به روز نبرد</p></div>
<div class="m2"><p>عَلَم او علامتی است مُبین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای امیری که از تو آموزند</p></div>
<div class="m2"><p>اُ‌مَرا رسم و سیرت و آیین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همه عقل است با دل تو ندیم</p></div>
<div class="m2"><p>همه جود است با کف تو قرین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هست در رزم تیغ تو ابری</p></div>
<div class="m2"><p>که از او خون صِرْف خیزد هین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>روی لشکر تویی به صلح و به جنگ</p></div>
<div class="m2"><p>پشت لشکر تویی به مهر و به‌ کین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بِدَرَد زهره‌ها چو گویی هان</p></div>
<div class="m2"><p>بِدَمد جانها چوگویی هین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا برآورد رایت عالیت</p></div>
<div class="m2"><p>در نشابور سر به علیّین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زنده گشتند امّتی بی‌جان</p></div>
<div class="m2"><p>شاد گشتند لشکری غمگین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هرکجا عزم و همت تو بود</p></div>
<div class="m2"><p>جرخ یار تو باد و بختْ مُعین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وز تو خشنود باد تا محشر</p></div>
<div class="m2"><p>جان سلطان ملک‌ به خلد برین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آمدی تا به تو سلامت یافت</p></div>
<div class="m2"><p>پای‌گوران ز دست شیر عرین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کرد اقبال و فر این سلطان</p></div>
<div class="m2"><p>بر تو فرخنده جشن فروردین</p></div></div>