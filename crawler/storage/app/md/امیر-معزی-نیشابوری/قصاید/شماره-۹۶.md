---
title: >-
    شمارهٔ ۹۶
---
# شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>بقای شهریار تاجور باد</p></div>
<div class="m2"><p>پناهش کردگار دادگر باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلیلش دولت و بخت جوان باد</p></div>
<div class="m2"><p>ندیمش نصرت و فتح و ظفر باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز رزم شاه در مشرق نشان باد</p></div>
<div class="m2"><p>ز بزم شاه در مغرب خبر باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خدنگ شاه را هنگام رفتن</p></div>
<div class="m2"><p>ز مرگ دشمنان پیکان و پر باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کجا بندد کمر بر کینِ دشمن</p></div>
<div class="m2"><p>میان دشمنش همچون کمر باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر آن‌ گونه که شارستان لوط است</p></div>
<div class="m2"><p>حصار خصم او زیر و زبر باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به نعمت مختصر شد خصم سلطان</p></div>
<div class="m2"><p>کنون زین پس به‌ دولت مختصر باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز خشم شاه چشم خصم‌ کورست</p></div>
<div class="m2"><p>ز کوس شاه گوش خصم کر باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شه آفاق هست اندر خراسان</p></div>
<div class="m2"><p>نهیب و بیم او در کاشْغر باد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر آن کوکب کزو باشد سعادت</p></div>
<div class="m2"><p>به‌سوی طالع شاهش نظر باد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شعار دیدهٔ شاهیّ و شادی</p></div>
<div class="m2"><p>مبارک رای تو همچون بصر باد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو آبی کاندرو آتش فروزد</p></div>
<div class="m2"><p>ز خون دشمنان تیغ تو تَر باد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جهانداران و شاهان جهان را</p></div>
<div class="m2"><p>کجا پای تو باشد فرق سر باد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تن اقبال را جود تو جان باد</p></div>
<div class="m2"><p>درخت ملک را جُود تو بر باد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز اقبال تو طبع بنده دریاست</p></div>
<div class="m2"><p>در آن دریا ز مدح تو گهر باد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر روزه تو را فرخنده بودست</p></div>
<div class="m2"><p>ز روز عید تو فرخنده‌تر باد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تویی سازندهٔ کار همه خلق</p></div>
<div class="m2"><p>خدایت کارساز و راهبر باد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تورا نصرت برادر باد جاوید</p></div>
<div class="m2"><p>تورا دولت همه ساله پدر باد</p></div></div>