---
title: >-
    شمارهٔ ۱۱۳
---
# شمارهٔ ۱۱۳

<div class="b" id="bn1"><div class="m1"><p>آن خداوند که آفاق به یک فرمان کرد</p></div>
<div class="m2"><p>ملک آفاق به فرمان ملک سلطان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ازل کرد قضا از قبل دولت او</p></div>
<div class="m2"><p>تا به پیروزی و اقبال فلک دوران کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه عالم چو یکی نامه به معنی بنگاشت</p></div>
<div class="m2"><p>کنیت و نام شهنشاه برو عنوان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حکم سلطانی و دعوی شهنشاهی را</p></div>
<div class="m2"><p>خنجر و بازوی او معجزه و برهان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خانهٔ همت او را ز هنر قاعده کرد</p></div>
<div class="m2"><p>مرکب دولت او را ز ظفر میدان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سایهٔ عدلش بر روی زمین پیدا کرد</p></div>
<div class="m2"><p>پیکر خصمش در زیر زمین پنهان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساخت از دانش و از بخشش کلّی گهری</p></div>
<div class="m2"><p>وان‌ گهر را کف راد و دل پاکش کان کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سر شاهان همه در چنبر فرمانش کشید</p></div>
<div class="m2"><p>چنبر چرخ به کام دل او دوران کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نتوان گفت به صد سال به تفصیل و به شرح</p></div>
<div class="m2"><p>آنچه با شاه ز احسان و کرم یزدان کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کرد احسان و کرم با همه کس شاه جهان</p></div>
<div class="m2"><p>لاجرم یزدان با او کرم و احسان کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آفرین باد بر آن شاه که فرماندهٔ خلق</p></div>
<div class="m2"><p>زیر فرمانش همه مملکت ایران ‌کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آفتاب کرم و سایهٔ عدل و نظرش</p></div>
<div class="m2"><p>دهر ویران شده را خرم و آبادان کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون کمر بست به پیروزی عالم بگشاد</p></div>
<div class="m2"><p>همه دشوار جهان دولت او آسان کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آنچه کردند به صد لشکر ازین پیش ملوک</p></div>
<div class="m2"><p>او به یک حاجب و یک نامه و یک فرمان کرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هرکه با دولت او بست به پیکار کمر</p></div>
<div class="m2"><p>تیغ تن پیکر او پیکر او بی‌جان کرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گمرهانی که کشیدند سر از طاعت او</p></div>
<div class="m2"><p>سر تیغش همه را بی‌سر و بی‌سامان کرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای بسا خصم که با شیر همی زد دندان</p></div>
<div class="m2"><p>خدمت او به ضرورت ز بن دندان کرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یاد کن آنچه به شمشیر در این پانزده سال</p></div>
<div class="m2"><p>با فلان‌کرد شه عالم و با بهمان کرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عبرتی بود جهان را چه به‌شرق و چه به‌غرب</p></div>
<div class="m2"><p>آنجه از پیش همه با ملک کرمان کرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هم ز اقبال نشان بود و ز اعجاز دلیل</p></div>
<div class="m2"><p>آنچه با ترمذ و خیل چِگِل و خَتلان‌کرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بازکار عجب و تهنیتی بود بزرگ</p></div>
<div class="m2"><p>آنچه درگنجه و ارمینیه وارّان کرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>باز هم نادره بود آنچه به‌فرمان قضا</p></div>
<div class="m2"><p>دم او با سر بلکاوتن عثمان کرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یا ز برهان کرم بود و خداوندی و عفو</p></div>
<div class="m2"><p>آنچه با مسلم و نجاری و با جرتان‌کرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>باز در دولت و دین بود عجایب به سه سال</p></div>
<div class="m2"><p>آنچه با جعبر و انطاکیه و حرّان‌کرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>باز اصل ظفر و مایه ی پیروزی بود</p></div>
<div class="m2"><p>آنچه با خانه و ملک و سپه خاقان‌کرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نتوان گفت فنون و هنر شاه تمام</p></div>
<div class="m2"><p>هرچه‌گوییم شناسیم که صد چندان کرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آنکه او شرح ظفرنامهٔ افریدون گفت</p></div>
<div class="m2"><p>وانکه او وصف هنرنامهٔ نوشروان کرد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>متفق‌ گشت کزین بیش ظفر نتوان کرد</p></div>
<div class="m2"><p>معترف گشت کزین بیش هنر نتوان کرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای بلند اختر شاهی که تو را بار خدای</p></div>
<div class="m2"><p>شاه ایران و خداوند همه توران کرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نیست بر تیغ تو تاوان ظفر و نصرت و فتح</p></div>
<div class="m2"><p>هرچه کرد این عجبی تیغ تو بی‌تاوان کرد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بارگاهت را دولت ز بقا کرد سریر</p></div>
<div class="m2"><p>وز معالیّ و شرف کنگرهٔ ایران کرد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مرد وصّاف نپیوسته گهرهای سخن</p></div>
<div class="m2"><p>چون هنرها و ظفرهای تورا دیوان کرد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر بدیع است که عیسی به دعا افسون کرد</p></div>
<div class="m2"><p>ور شگفت است‌که موسی به عصا ثعبان کرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تو به شمشیرکنی آنچه به ثعبان این‌کرد</p></div>
<div class="m2"><p>تو به اقبال کنی آنچه به افسون آن‌ کرد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شاه عالم تویی از عالم و از هر چه در اوست</p></div>
<div class="m2"><p>داد بستان که جهان داد تو چون بستان کرد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آن‌ گهر خواه کجا خاطر شاعر صفتش</p></div>
<div class="m2"><p>به بدخشی و عقیق یمن و مرجان کرد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از کفِ طرفه نگاری که نگارین‌ رخ او</p></div>
<div class="m2"><p>مجلس بزم تو را همچو نگارستان کرد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زَنَخ و زلفش گویی‌که یکی جادوی نغز</p></div>
<div class="m2"><p>از سمن‌ گوی وز شمشاد و شَبَه‌ چوگان کرد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>آن‌کند با دل عشاق همین غمزهٔ او</p></div>
<div class="m2"><p>که سر تیغ تو با جان بداندیشان کرد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>جاودان همت میمون تو زرافشان باد</p></div>
<div class="m2"><p>که فلک خدمت آن همت زرافشان کرد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پایهٔ خسروی از پای تو آراسته باد</p></div>
<div class="m2"><p>که قضا پایهٔ اقبال تو بی‌پایان کرد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>عمر تو چون مه نو باد که شد عمر عَدوت‌</p></div>
<div class="m2"><p>چون مه پانزده و روی سوی نقصان کرد</p></div></div>