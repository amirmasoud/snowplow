---
title: >-
    شمارهٔ ۱۳۹
---
# شمارهٔ ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>برمعین دین پیغمبر مبارک باد عید</p></div>
<div class="m2"><p>چشم بد باد از جمال و ازکمال او بعید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صاحب دنیا ابونصر احمد آن کز طلعتش</p></div>
<div class="m2"><p>هست فال و طالع آزادگان سعد و سعید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالم آرای و مبارک رای دستوری که هست</p></div>
<div class="m2"><p>امر او اثبات عدل و نهی او نفی و وعید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش شاهنشه مَحَلّ و جاه او افزونترست</p></div>
<div class="m2"><p>از محلّ و جاه مأمون پیش هارون‌الرشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون دگر اصحاب دیوان پیش او خدمت کنند</p></div>
<div class="m2"><p>گر شوند امروز راجع صاحب و اِبن العَمید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در جهان چون پایهٔ او را بلندی و شرف</p></div>
<div class="m2"><p>می ندانم پایه‌ای جز بایهٔ عرش مجید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همت او در بزرگی در دوگیتی نَنْگرد</p></div>
<div class="m2"><p>گر دو گیتی پیش چشم او بود بر من‌ یزید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ور ببخشد هر چه از دور فلک پیدا شود</p></div>
<div class="m2"><p>همت عالیْش‌ْ گوید ای فلک هل مِن مزید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون براند کلک فخر آرد به‌کلک او قَصَب</p></div>
<div class="m2"><p>چون بگیرد تیغ فخر آرد به ‌تیغ او حدید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آمد اندر شأن‌کلک و تیغ اوگویی مگر</p></div>
<div class="m2"><p>سورهٔ نون و القلم یا آیه بأساً شدید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عمر خلق از مَدِّکلک او همی یابد مدد</p></div>
<div class="m2"><p>هرکه بیند مدّ کلکش عمر او گردد مدید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خاک پایش هست نافعتر ز باران و درخت</p></div>
<div class="m2"><p>کایزد آن را گفت در قرآن لها طلع نضید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدسِگال و نیکخواه او دو مسکن یافتند</p></div>
<div class="m2"><p>آن یکی ‌بئر معطّل و آن دگر قصر مشید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از هنرمندان عصر او را کسی مانند نیست</p></div>
<div class="m2"><p>زانکه هست او از هنرمندی به عصر اندر وحید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وز جوانمردی هِمال او نبیند چشم دهر</p></div>
<div class="m2"><p>تخت او زیبد مراد و آسمان باید مرید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تاکه‌گردون پیر باشد بخت او باشد جوان</p></div>
<div class="m2"><p>تاکهن باشد جهان اقبال او باشد جدید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شاکرند از همت او مادحان روزگار</p></div>
<div class="m2"><p>نیست ممدوحی که دارد ماد‌حان را مستزید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر بتابد دولت او بر دل مرد بخیل</p></div>
<div class="m2"><p>ور برافتد سایهٔ او بر سر مرد بلید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن بخیل اندر سخاوت حاتم و نُعمان شود</p></div>
<div class="m2"><p>وین بلید اندر فصاحت‌ گردد ا‌عشی و لبید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر بود باران مفید اندر بهاران‌کشت را</p></div>
<div class="m2"><p>هست ‌کشت ملک را کلک تو چون باران مفید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای موکد درکف احباب تو حبل‌ا‌لمتین</p></div>
<div class="m2"><p>ای معطل در تن اعدای تو حبل‌الورید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا دلیل قوت است و تا نشان قدرت است</p></div>
<div class="m2"><p>یفعل الله مایشاء ‌یحکم الله ما یرید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر زمین بادند امرت را طبایع چون خدم</p></div>
<div class="m2"><p>بر فلک بادند حکمت ‌را کواکب چون عبید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بر تو فرخ باد روز عید از اقبال شاه</p></div>
<div class="m2"><p>روزگارت باد سرتاسر همه چون ‌روز عید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خرم و شاد از تو در انشاء و استیفاء و عرض</p></div>
<div class="m2"><p>روز و شب هم فخر ملک وهم نصیر و هم سدید</p></div></div>