---
title: >-
    شمارهٔ ۳۲۰
---
# شمارهٔ ۳۲۰

<div class="b" id="bn1"><div class="m1"><p>ای ماه لاله روی من ای سرو سیم‌تن</p></div>
<div class="m2"><p>از دل تو را فلک‌ کنم از جان تو را چمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیرا که دل سزد فلک ماه روی را</p></div>
<div class="m2"><p>زیرا که جان سزد چمن سرو سیم‌تن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلف تو توده تودهٔ مشک است بر قمر</p></div>
<div class="m2"><p>جعد تو حلقه حلقهٔ ابرست بر سمن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان توده توده است به شهر اندرون بلا</p></div>
<div class="m2"><p>زان حلقه حلقه است به دهر اندرون فتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لب چون عقیق‌ کردی و رخساره چون سهیل</p></div>
<div class="m2"><p>وین هر دو ساختی به هزاران فسون و فن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا در عجم بود لب و رخسار تو بدیع</p></div>
<div class="m2"><p>چونان‌ کجا سهیل و عقیق است در یمن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل بر دلم نه ای صنم ششتری قبای</p></div>
<div class="m2"><p>لب بر لبم نِه ای پسر مشتری ذقن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا موم نرم بینی در زیر سنگ سخت</p></div>
<div class="m2"><p>تا شَنبلید بینی در زیر نسترن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون تیر برکمان نهی و بشکنی سپاه</p></div>
<div class="m2"><p>صد توبه بشکنی به‌سر زلف پر شکن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درکار تو شگفت فرو مانده‌ام بتا</p></div>
<div class="m2"><p>توبه‌شکن نهم لقبت یا سپه‌شکن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا تو به وقتِ خشم و به وقتِ لَطَف مرا</p></div>
<div class="m2"><p>آتش نموده‌ای ز رخ و لؤلؤ از دهن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هجران تو بر آتش و لؤلؤ همی کند</p></div>
<div class="m2"><p>همچون رخ و دهانت لب و دیدگان من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ایدون‌ گمان بری که مگر ماه انجم است</p></div>
<div class="m2"><p>چون بنگری به چهره و دندان خویشتن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خواهان دیدن تو شود گر خبر رسد</p></div>
<div class="m2"><p>از ماه و انجم تو به خورشید انجمن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>میر اجل مؤید ملک و شهاب دین</p></div>
<div class="m2"><p>فرخ ظهیر دولت‌ ابونصر ‌بن حسن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فرخنده اختری که خجسته خصال او</p></div>
<div class="m2"><p>آسایش زمین شده و آرایش ز من</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرد خرد سپهر شناسد بساط او</p></div>
<div class="m2"><p>آری سپهر باشد خورشید را وطن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کینش به کار دشمن دولت دهد فساد</p></div>
<div class="m2"><p>خشمش به چشم دشمن ملت نهد وسن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تایید او چو پیرهن یوسف است و خلق</p></div>
<div class="m2"><p>یعقوب‌وار در طلب بوی پیرهن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>درگاه اوست ملتزم خلق و ملتجا</p></div>
<div class="m2"><p>تدبیر اوست معتمد ملک و مؤتمن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در رسمهاش گنج معالی است مُدَّخَر</p></div>
<div class="m2"><p>در لفظهاش گنج معانی است مُخْتَزن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر بر زند به سنگ نکوخواه از حسام</p></div>
<div class="m2"><p>ور بر زند به‌خاک نگون‌خواه او مجِن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از سنگ و خاک قسمت ایشان رسد دو چیز</p></div>
<div class="m2"><p>آن را رسد جواهر و این را رسدکفن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای نفی کفر باطل و اثبات دین حق</p></div>
<div class="m2"><p>ای نصرت فرشته و ای قهر اهرمن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دُرّ است دولت تو و آفاق چون صدف</p></div>
<div class="m2"><p>جان است همت تو و افلاک چون بدن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هرکس ز معن‌ زائده ‌گوید همی خبر</p></div>
<div class="m2"><p>هرکس ز سیف ذُویزن آرد همی سخن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یک چاکر تو صاحب صد ‌معن زائده است</p></div>
<div class="m2"><p>یک‌ کِهتر تو مهتر صد سیف ذویزن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از آتش سیاست و خشم تو در سزد</p></div>
<div class="m2"><p>‌مِغفَر شود چون مَعجر و مردان شوند زن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بر پای و بر دو دست تو عاشق شده است ماه</p></div>
<div class="m2"><p>زین روی ‌گه چو نعل بودگاه چون لگن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آسوده نیست دست تو از جود ساعتی</p></div>
<div class="m2"><p>گویی شدست دست تو بر جود مفتتن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر چاهکن شدست ز بهر تو دشمنت</p></div>
<div class="m2"><p>ناگاه دراوفتد به ته چاه چاهکن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>وانگاه دست بر رسن مدبری زند</p></div>
<div class="m2"><p>از چَه درآید و به ‌گلو در کند رسن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر بر عَدَن خیال جمال تو بگذرد</p></div>
<div class="m2"><p>همچون بهشت عَدْ‌ن شود تُربتِ عَدَن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر باد احتشام تو بر نار بن وزد</p></div>
<div class="m2"><p>آن ناربن شود به بلندی چو نارون</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ور سایهٔ قبول تو بر روبه اوفتد</p></div>
<div class="m2"><p>شیران دهند بچهٔ روباه را لبن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تیر فلک شمن شود وکلک من صنم</p></div>
<div class="m2"><p>چون طبع تو صنم شود و طبع من شَمَن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هر مدحتی‌که نام تو باشد تخلصش</p></div>
<div class="m2"><p>گردو‌نْشْ مشتری سزد و مشتری ثمن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا از نِعَم همیشه بود خلق را طرب</p></div>
<div class="m2"><p>تا از مِحَن همیشه بود خلق را حَزَن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بادند دوستان تو در روضهٔ نعم</p></div>
<div class="m2"><p>بادند دشمنان تو در قبضهٔ محن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>افروخته وثاق تو از شَمسهٔ چِگِل</p></div>
<div class="m2"><p>آراسته سرای تو از لُعبت خُتن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>وان گوهر لطیف که پروردش آفتاب</p></div>
<div class="m2"><p>یاقوت‌وار آمده در جام تو ز دَن</p></div></div>