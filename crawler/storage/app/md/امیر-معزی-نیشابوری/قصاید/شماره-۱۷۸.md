---
title: >-
    شمارهٔ ۱۷۸
---
# شمارهٔ ۱۷۸

<div class="b" id="bn1"><div class="m1"><p>ای رفته مدتی به سعادت سوی سفر</p></div>
<div class="m2"><p>باز آمده به نصرت و فیروزی و ظفر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در صد سفر ملوک گذشته ندیده‌اند</p></div>
<div class="m2"><p>آن فتح و آن ظفر که تو دیدی به یک سفر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با فتح نامه‌ها و ظفرنامه‌های تو</p></div>
<div class="m2"><p>مدروس شد حکایت و منسوخ شد سمر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیش آید از شمار فتوح‌ گذشتگان</p></div>
<div class="m2"><p>هر نامه‌ کز فتوح تو خوانند مختصر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کردار تو معاینه بیند همی خرد</p></div>
<div class="m2"><p>ممکن ‌کجا شود که ‌کند تکیه بر خبر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک جنبش تو هست ز جیحون سوی فرات</p></div>
<div class="m2"><p>یک نهضت تو هست ز خاور به باختر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پست است دهر و همت عالیت سرفراز</p></div>
<div class="m2"><p>زیرست چرخ و دولت عالیت بر زبر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر نیست بی‌قضا و قدر نیکی و بدی</p></div>
<div class="m2"><p>فرمان تو قضا شد و شمشیر تو قدر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دو چیز در دو چیز زآفت منزهند</p></div>
<div class="m2"><p>در آسمان ستاره و در طبع تو هنر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آراسته است رای تو عالم به‌دین و داد</p></div>
<div class="m2"><p>پرداخته است تیغ تو گیتی ز شور و شر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دو چیز در دو چیز یکی اند در صفت</p></div>
<div class="m2"><p>در آفتاب ذره و در تیغ توگهر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از مهر وکین توست در ایام نیک و بد</p></div>
<div class="m2"><p>وز عفو و خشم توست در آفاق نفع و ضر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر روی دوستان تو و دشمنان توست</p></div>
<div class="m2"><p>اقبال را علامت و اِدْبار را اثر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در ملک شام و روی به یک عزم تو شدند</p></div>
<div class="m2"><p>صد شاه و شهر بسته میان و گشاده در</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ازگرد لشکر تو به شام اندرون هنوز</p></div>
<div class="m2"><p>سرخ است خاک همچو طَبرخون و مُعْصَفَر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از آتش جگر لب بدخواه توست خشک</p></div>
<div class="m2"><p>وز آب دیده گونهٔ بدگوی توست تر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آری هر آن کجا که خلاف تو بگذرد</p></div>
<div class="m2"><p>هم آب دیده باشد و هم آتش جگر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای دادگر شهی که تو را خوانَدَن رواست</p></div>
<div class="m2"><p>سلطان شرق و غرب و شهنشاه بحر و بر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون لؤلؤ از جواهر و خورشید ز اختران</p></div>
<div class="m2"><p>مشهوری از خلایق و مختاری از بشر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از بهر خدمت تو سزد گر خدای عرش</p></div>
<div class="m2"><p>ارواح رفته باز رساند سوی صور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>صید کمند توست به دور اندرون سپهر</p></div>
<div class="m2"><p>نعل سمند توست به سیر اندرون قمر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دشمن به دام توست و زمانه به‌کام توست</p></div>
<div class="m2"><p>دولت غلام توست چه باید همی دگر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر رفتنت ز شهر سپاهان خجسته بود</p></div>
<div class="m2"><p>باز آمدنْتْ هست ز رفتن خجسته‌تر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یک چند در سفر ظفر انگیختی به تیغ</p></div>
<div class="m2"><p>اکنون به جام می طرب‌انگیز در حضر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ساغر ستان ز دست نگاری که زلف او</p></div>
<div class="m2"><p>گه پیش‌ گل سپر بود و گاه گل سپر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گه جعد او به قصد خم اندر شود به خم</p></div>
<div class="m2"><p>گه زلف او به طبع سر اندر زند به سر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نوش است در لب وی و نوش است در قدح</p></div>
<div class="m2"><p>بستان قدح بر آن ‌لب چون نوش و نوش خور</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نیکی توراست عمر به نیکی همی‌گذار</p></div>
<div class="m2"><p>شادی توراست روز به شادی همی شمر</p></div></div>