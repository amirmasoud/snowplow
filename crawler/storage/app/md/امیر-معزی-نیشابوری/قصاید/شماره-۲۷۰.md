---
title: >-
    شمارهٔ ۲۷۰
---
# شمارهٔ ۲۷۰

<div class="b" id="bn1"><div class="m1"><p>روزی همی گذشتم جُزوی غزل به کف</p></div>
<div class="m2"><p>دیدم یکی غزالِ خرامان میان صف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با همرهان خویش به نخّاس خانه رفت</p></div>
<div class="m2"><p>نخاس باز کرد یکایک در غُرَف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاعر میان شارع و طرفه به غرفه بر</p></div>
<div class="m2"><p>او تافته ز خوبی و من تافته ز تف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>او در میان حُلّه و من در میان خاک</p></div>
<div class="m2"><p>من برگرفته دفتر و او برگرفته دف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قالت‌ اِذا جَلَست‌َ وابصَرت‌ فَاَنصَرِف</p></div>
<div class="m2"><p>مَن‌ لَم‌ یَکُن لَهُ ثَمَنی مرَّ وَاِنصَرف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک ساعت ایستادم و کردم بدو نگاه</p></div>
<div class="m2"><p>فَالجسم قَد تَرَّحل و القلب قَد وَقَف‌</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون وصف آن وصیفت زیبا نگاشتم</p></div>
<div class="m2"><p>لَم‌ یَبق‌َ فی‌ القَطیعَهِٔ وَصف‌ الّذی وَصَف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باز آمدم به خانه تنم‌ گشته چون کمان</p></div>
<div class="m2"><p>تیر فراق را شده جان و تنم هدف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یعقوب‌ گفت یا اَسَفی از غم فراق</p></div>
<div class="m2"><p>من نیز از فراق همی‌ گفتم اَلاَسَف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا کی من از بلاد خراسان بلا کشم</p></div>
<div class="m2"><p>این آمد از بلاد خراسان مرا به‌ کف</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در خدمت رکاب تو آیم سوی عراق</p></div>
<div class="m2"><p>یا سیدالعراقین ای ملک را شرف</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یا مَفخَرَ الکُفاهِٔ اَبا سَعدِ اَلّذی</p></div>
<div class="m2"><p>مدح الموحدین له لیس یختلف</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آمد عبید شاه جهان جوهر عبید</p></div>
<div class="m2"><p>آمد خلف پیمبربا جوهر خلف</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا محشر از تو تازه بود جاه هر عقب</p></div>
<div class="m2"><p>تا آدم از تو شاد بود جان هر سلف</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رایت همه‌ کرامت و راهت همه‌ کرم</p></div>
<div class="m2"><p>وصفت همه لطافت و وصلت همه لطف</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گیرند عالمان ز مقاماتِ تو سبق</p></div>
<div class="m2"><p>خوانند فاضلان ز مقالات تو نتف</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از جود توست نامهٔ ارزاق را نُکَت</p></div>
<div class="m2"><p>وز خُلق توست دفتر اخلاق را طُرَف</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صافی بود طریقت عدل تو از فساد</p></div>
<div class="m2"><p>خالی بود حقیقت جاه تو از صَلَف</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای مهتری که از رخ زنگی شب سیاه</p></div>
<div class="m2"><p>نوک سنان تو برباید همی‌ کَلَف</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جان عدو به وهم برون آوری ز تن</p></div>
<div class="m2"><p>چون بچه را ز بیضه برون آورد کشف</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جان شرف به خدمت تو پوید از علوّ</p></div>
<div class="m2"><p>گر باد همت تو جهد بر تن شرف</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>غوّاص دولت است و سعادت چو گوهرست</p></div>
<div class="m2"><p>دست تو بحر و ماهی زرین در او صدف</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سوگند مرد چون به همه مملکت بود</p></div>
<div class="m2"><p>آن مرد را به مدح تو واجب‌کند حَلَف</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دریا که موج و کف زند اندر جهان تویی</p></div>
<div class="m2"><p>رادیت هست موج و بزرگیت هست‌کف</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آنجا که جود توست چه باشد سخای بحر</p></div>
<div class="m2"><p>فرقی بود ز رفرف و فردوس تا زرف</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>با رای‌ تو ستاره و با بخت تو سپهر</p></div>
<div class="m2"><p>چون لعل با شبه است و چو فیروزه باخزف</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هرچند ز آسمان شرف عرش برترست</p></div>
<div class="m2"><p>بگذشته رای و همت و بخت تو زان شرف</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هر چند نیست طبع تو بر خلق مُستَخِف</p></div>
<div class="m2"><p>شد دهر مُسْتَخِف و حسود تو مُسْتَخَف</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عزل عدوت دائم و عزِّ ولی مدام</p></div>
<div class="m2"><p>آن دیده حال خوفت و این دیده حال خف</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در نامهٔ عدوت‌ نوشتند لَن‌ تَنال</p></div>
<div class="m2"><p>بر خاتم ولیت نوشتند لا تخف</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کفران نعمت تو خداوند کافری است</p></div>
<div class="m2"><p>نعمت حرام‌تر ز رباگردد و سلف</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>من شکر نعمت تو کنم یا وحید عصر</p></div>
<div class="m2"><p>تا نعمتم مصون بود و جاه معترف</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>برهانی از شمار قدم بود پیش تو</p></div>
<div class="m2"><p>مشهور بود نام و نشانش بهر طرف</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>او غایب است و نایب و فرزند او منم</p></div>
<div class="m2"><p>و آورده‌ام زخاطر خویش احسن‌الطّرف</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>وان طرفه هم به دولت و اقبال و جاه توست</p></div>
<div class="m2"><p>بالبدر یهتدی و من البحر یغترف</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>فی خِدمَهٔ اَلتّی قَصدَت‌ فی زمانِنا</p></div>
<div class="m2"><p>قَد قَصّر البعیدُ وبالذنبِ اِعترف</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>عذرم قبول کن که دل و جان من رهی</p></div>
<div class="m2"><p>هست از ثنا و شکر و مدیح تو مؤتلف</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>باید مرا قبول تو تا محتشم شوم</p></div>
<div class="m2"><p>خواهم ز تو لَطَف‌ که نیم طالب علف</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تا جسم را ز روح بود طبع معتدل</p></div>
<div class="m2"><p>تا ماه را ز مهر بود نور مختطف</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هرگز مباد مادح تو جز که در نجات</p></div>
<div class="m2"><p>هرگز مباد حاسد تو جز که در تلف</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>فضل خدا و رحمت او داشته تو را</p></div>
<div class="m2"><p>معصوم در حمایت و محفوظ در کنف</p></div></div>