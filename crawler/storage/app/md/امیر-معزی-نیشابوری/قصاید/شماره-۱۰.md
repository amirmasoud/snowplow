---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>برآمد ساج‌گون ابری ز روی ساج‌گون دریا</p></div>
<div class="m2"><p>بخار مرکز خاکی نقاب قبّهٔ خضرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو پیوندد به هم‌ گویی‌ که در دشت است سیمابی</p></div>
<div class="m2"><p>چو از هم بگسلد گویی مگر کشتی است در دریا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گهی چون‌ خرمن‌ مشک‌ است بر پیروزه‌ گون مَفرَش</p></div>
<div class="m2"><p>گهی چون تودهٔ ریگ است بر زنگارگون صحرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گهی چون شاخ نیلوفر میان باغ پُر نرگس</p></div>
<div class="m2"><p>گهی چون تلّ خاکستر فراز کوه پر مینا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گهی کافور بار آید چه بر کوه و چه بر هامون</p></div>
<div class="m2"><p>گهی لؤلؤ فشان آید چه بر خار و چه بر خارا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گه لؤلؤ پراگندن بود چون عاملی حایر</p></div>
<div class="m2"><p>گه ‌کافور پاشیدن بود چون عاقلی شیدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ازو هر ساعتی جیحون شود پر تختهٔ نقره</p></div>
<div class="m2"><p>وزو هر ساعتی دریا شود پر لُؤلُؤ لالا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو بگراید سوی بالا برآرد گوهر از پستی</p></div>
<div class="m2"><p>چو باز آید سوی پستی فشاند گوهر از بالا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گهی با خاک در بیعت‌ گهی با باد در کشتی</p></div>
<div class="m2"><p>گهی با آب در صحبت، گهی با آتش ا‌َنْدَروا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کجا خورشید رخشان را بپوشد زیر دامن در</p></div>
<div class="m2"><p>بدان ماندکه اهریمن همی پوشد ید بیضا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو نور چشمهٔ خورشید زیر او برون تابد</p></div>
<div class="m2"><p>تو گویی نور قندیل است زیر جامهٔ ترسا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نماید تیره و‌ گریان ز بیم باد نوروزی</p></div>
<div class="m2"><p>چو چشمِ دشمنِ دولت ز بیم خواجه والا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عمید دولت عالیّ و جمشید قوی دولت</p></div>
<div class="m2"><p>هنرمندی کزو نازند اصل آدم و حوا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سخا بر دست او مفتون چو بر لیلی بود مجنون</p></div>
<div class="m2"><p>سخن بر لفظِ او عاشق چو بر وامق بود عَذرا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه‌گردون ‌است ‌و در رفعت چو گردون ‌است بی‌آفت</p></div>
<div class="m2"><p>نه یزدان است و در قدرت چو یزدان است بی همتا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چنو اسرار او روشن چنو آثار او نیکو</p></div>
<div class="m2"><p>چنو گفتارِ او شیرین چنو کردارِ او زیبا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نهد تدبیر او ‌دایم قدم بر تارک ‌کیوان</p></div>
<div class="m2"><p>نهد فرمانِ او دایم عَلَم بر عالمِ بالا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به ‌جنب دست او دریا نماید خاک بی‌بخشش</p></div>
<div class="m2"><p>به جنبِ رای او گردون نماید تنگ بی پهنا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز اعیان برگزید او را به جود و همت عالی</p></div>
<div class="m2"><p>خداوند همه شاهان معزّالدّین و الدّنیا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>صفاهان گشت ازو خرّم چو باغ از فرّ فروردین</p></div>
<div class="m2"><p>کنون خرمای بی‌خارست و باشد خار با خرما</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نه هر والی چو او باشد، به ‌کف کافی، به دل عادل</p></div>
<div class="m2"><p>نه هر بیتی بود کعبه نه هر طوری بود سینا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جوانمردا جوانبختا به تدبیر و خردمندی</p></div>
<div class="m2"><p>همان کردی تو با دشمن که ذُوالقَرنَین با دارا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تویی شایستهٔ دولت چو سر را روح نفسانی</p></div>
<div class="m2"><p>تویی بایستهٔ ملت چو دل را نقطه سودا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مراد جزئی و کلی تویی در سیرت و سامان</p></div>
<div class="m2"><p>وجود عِلوی و سِفلی تویی در صورت و سیما</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بداندیشان تو هستند از چنگ قضا خسته</p></div>
<div class="m2"><p>همه پالوده و حیران به بیغوله درون رسوا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به چشم اندر همه سوزن، به‌حلق اندر همه نشتر</p></div>
<div class="m2"><p>به مغز اندر همه ‌گرمی، به قلب اندر همه سرما</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>الا یا مهتر مقبل عمید مشتری طالع</p></div>
<div class="m2"><p>عطارد پیش تو خواهد که بنشیند به ‌استیفا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>حکیم حضرت سلطان چو پیش تو شود حاضر</p></div>
<div class="m2"><p>جواز بخت او باشد فراز فرقد و جوزا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو ‌دیدار تو را بیند معزی پور برهانی</p></div>
<div class="m2"><p>ز بهر دیدنت خواهد همیشه دیده بینا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مدیح و آفرین تو همی تابد ز دیوانش</p></div>
<div class="m2"><p>چنان کز گنبد گردون بتابد زهرهٔ زهرا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همیشه تا که سیسنبر بود در ساحت بستان</p></div>
<div class="m2"><p>همیشه تاکه ‌کاهر‌با بود در صخرهٔ صمّا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سر مداح تو بادا به سبزی همچو سیسنبر</p></div>
<div class="m2"><p>رخ بدخواه تو بادا به زردی همچو کاهر‌با</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز یزدان عمر تو باقی ز سلطان بخت تو عالی</p></div>
<div class="m2"><p>ز دی امروز تو خوشتر ز امروز توبه فردا</p></div></div>