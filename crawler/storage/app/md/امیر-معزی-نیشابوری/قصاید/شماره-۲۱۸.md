---
title: >-
    شمارهٔ ۲۱۸
---
# شمارهٔ ۲۱۸

<div class="b" id="bn1"><div class="m1"><p>ای تاج دین و دنیا ای فخر روزگار</p></div>
<div class="m2"><p>بر تو خجسته باد چنین عید صدهزار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای از وَرَعْ چو مادر عیسی بلند قدر</p></div>
<div class="m2"><p>وی از شرف چو دختر احمد بزرگوار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای مادر دو شاه چو سلطان و چون ملک</p></div>
<div class="m2"><p>هر دو خدایگان و خداوند و شهریار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از یکدگر به دولت تو هر دو شادمان</p></div>
<div class="m2"><p>با یکدگر به حشمت تو هر دو سازگار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کار دختر و پسر هر دو پادشاه</p></div>
<div class="m2"><p>بردی ز خاص خویش بسی مالها به‌کار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن ساختی زهدیه که هرگز نساختند</p></div>
<div class="m2"><p>شاهان باستان و بزرگان روزگار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرگز به دولت تو نبودست هیچ زن</p></div>
<div class="m2"><p>دانا و دوربین و خردمند و هوشیار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در زهد و پارسایی با حشمت و جلال</p></div>
<div class="m2"><p>در ملک و پادشاهی با عصمت و وقار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گویی همه سعادت بودست بر فلک</p></div>
<div class="m2"><p>روزی که آفرید تو را آفریدگار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خیری که تو به مرو و نشابور کرده‌ای</p></div>
<div class="m2"><p>خیری است در شریعت و اسلام پایدار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دیوار آن چو چرخ بلندست بی‌گزند</p></div>
<div class="m2"><p>بنیاد آن چو کوه گران است استوار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گشته ز بهر درس امامان در او مقیم</p></div>
<div class="m2"><p>کرده ز بهر علم فقیهان در او قرار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>امروز هست شُکْر و ثنای تو بی‌قیاس</p></div>
<div class="m2"><p>فردا بود ثواب و جزای تو بی‌شمار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از اعتقاد توست که اندر جهان نماند</p></div>
<div class="m2"><p>یک دشمن سبکسر و یک خصم خاکسار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وز سرّ پاک توست که سلطان دادگر</p></div>
<div class="m2"><p>بگشاد در عراق به شمشیر صد حصار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وز فر بخت توست‌ که آمد بر ملک</p></div>
<div class="m2"><p>شهزاده‌ای بزرگ ز غزنین به زینهار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بنهان و آشکار تو با خلق چون یکی است</p></div>
<div class="m2"><p>خالق معین توست چه پنهان چه آشکار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از بس‌که هست در دل تو رحمت وکرم</p></div>
<div class="m2"><p>بر خلق مهربانی و از خلق بردبار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر درخور تو بخت نثاری فرستدی</p></div>
<div class="m2"><p>گردون ستارگان کندی بر سرت نثار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دنیا و دین تو داری قدرش همی شناس</p></div>
<div class="m2"><p>هر دو خدای دادت شکرش همی گزار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آن بندگان که پیش تو خدمت همی‌کنند</p></div>
<div class="m2"><p>روز همه زخدمت تو هست چون بهار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وانان که نعمت تو به ایشان همی رسد</p></div>
<div class="m2"><p>کار همه ز نعمت تو هست چون نگار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دیری است تا معزی خدمتگر شماست</p></div>
<div class="m2"><p>او را سزد به خدمت دیرینه افتخار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>درخورد خلعت است که امسال شعر او</p></div>
<div class="m2"><p>زان شعر بهترست‌که پیرارگفت و پار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای آنکه روز و شب ز سه فرزند خرمی</p></div>
<div class="m2"><p>هر سه زمانه را ز ملک شاه یادگار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بادی تو در سعادت با هر سه کامران</p></div>
<div class="m2"><p>بادی تو در سعادت با هر سه ‌کامکار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون دولت تو یار و نگهدار عالم است</p></div>
<div class="m2"><p>ایزد تورا همیشه نگهدار باد و یار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>فرخنده باد بر تو به شادی هزار عید</p></div>
<div class="m2"><p>طبع تو شاد باد به روزی هزار بار</p></div></div>