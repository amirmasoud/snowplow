---
title: >-
    شمارهٔ ۲۱۹
---
# شمارهٔ ۲۱۹

<div class="b" id="bn1"><div class="m1"><p>بی‌روح پیکری است‌ گه جنگ جان شکار</p></div>
<div class="m2"><p>بی‌دود آتشی است گه رزم پرشرار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر پرشرار آتش بی‌دود نادرست</p></div>
<div class="m2"><p>نادرترست پیکر بی‌روح جان شکار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیکر بود شگفت به پاکیزگی چو جان</p></div>
<div class="m2"><p>آتش بود بدیع‌تر ار باشد آبدار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رخشنده چون ستاره و چون آسمان کبود</p></div>
<div class="m2"><p>وز آسمان ستاره شود بر تنش نثار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هنگام کینه بر تنش از فرق تا قدم</p></div>
<div class="m2"><p>دندانهاش تیزتر از شعله‌های نار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گویی‌که هست بر سر دندان‌های او</p></div>
<div class="m2"><p>زهری که هست در بن دندانهای مار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ابری است لاله‌ بار و درختی است لاله‌ بر</p></div>
<div class="m2"><p>دیدی درخت لاله‌بر و ابر لاله بار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باریدنش همیشه به صحرای معرکه</p></div>
<div class="m2"><p>یازیدنش همیشه به میدان کارزار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آبی مروق است فسرده که روز رزم</p></div>
<div class="m2"><p>دشمن در او خیال اجل بیند آشکار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر دشمنی که دید خیال اجل در او</p></div>
<div class="m2"><p>خالی شد از خیال و روان شد خیال‌وار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لوحی است نیلگون که قلم در خلاف او</p></div>
<div class="m2"><p>از رشک زرد روی شد و لاغر و نزار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با لوح‌ گر قلم به ازل سازگار بود</p></div>
<div class="m2"><p>با این زدوده لوح کبودی است سازگار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کان لوح از او نگار پذیرفت در ازل</p></div>
<div class="m2"><p>وین لوح از این همی بپذیرد کنون نگار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بشکست پشت مهرهٔ کفار در عرب</p></div>
<div class="m2"><p>تا نام او به دست علی کشت ذوالفقار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آدینه چون خطیب به منبر بر آردش</p></div>
<div class="m2"><p>تازه شود به تیزی او دین کردگار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هست او به روز رزم سلیحی‌که آن سلیح</p></div>
<div class="m2"><p>آید گه اجل ملک‌الموت را به کار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شخصی که زینهار نباید ز چنگ او</p></div>
<div class="m2"><p>جانش نیابد از ملک‌الموت زینهار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا از میان سنگ درنگش‌گسسته شد</p></div>
<div class="m2"><p>سنگ و درنگ برد ز خصمان خاکسار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در سنگ بود عاجز و امروز معجزست</p></div>
<div class="m2"><p>در دست پهلوان خداوند روزگار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>والاعماد دولت و دنیا جمال دین</p></div>
<div class="m2"><p>خوارزمشاه میر هنرمند کامگار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شاهی‌که حق و باطل از او شد بلند و پست</p></div>
<div class="m2"><p>میری‌ که دین و کفر از او شد بلند و خوار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نامش محمدست و به عدلش مُخَلّدست</p></div>
<div class="m2"><p>هم ملت محمد و هم ملک شهریار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آهو ز شیر شیر خورد در ولایتش</p></div>
<div class="m2"><p>و زبَرکند ستایش او طفل شیر خوار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نشگفت اگر به‌خدمت جودش بر آسمان</p></div>
<div class="m2"><p>بندد کمر ز قوسِ قزح ابر نو بهار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کان گل فروز باشد و این هست دل‌فروز</p></div>
<div class="m2"><p>وان قطره بار شد و این هست بدره بار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بازی است تیر اوکه شود چون تذرو پر</p></div>
<div class="m2"><p>هرگه‌ که خصم را چو کبوتر کند شکار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همرنگ خاک جرم قمر زان سبب بود</p></div>
<div class="m2"><p>کز نعل اسب او به قمر پر شود غبار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تاریک فام روی زحل زان قبل بود</p></div>
<div class="m2"><p>کز خون رزم او به زحل بر شود بخار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دارد هر آن هنرکه به‌کارست خلق را</p></div>
<div class="m2"><p>واندر هنر ز خلق ندارد نظیر و یار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در حق بود طریقت او صدق را دلیل</p></div>
<div class="m2"><p>در دین بود عقیدت او شرع را شعار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از اعتقاد پاک بود در دلش دو چیز</p></div>
<div class="m2"><p>تحقیق مرد خندق و تصدیق یار غار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>وانجا که رای باید و تدبیر مملکت</p></div>
<div class="m2"><p>پیرایهٔ خرد بود و مایهٔ وقار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گفتار او بود به همه خیرها مشیر</p></div>
<div class="m2"><p>تدبیر او بود به همه فخرها مسار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>رای صواب او ز بلندی و روشنی</p></div>
<div class="m2"><p>از چرخ ننگ دارد و از آفتاب عار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>وآنجا که عدل باید و انصاف و راستی</p></div>
<div class="m2"><p>تیغش برآورد ز سر ظالمان دمار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تنها روند قافله از امن و عدل او</p></div>
<div class="m2"><p>بی‌رهنمای و بدرقه در کوه و در قفار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ازگرگ، میش یاد نیارد به ساده دشت</p></div>
<div class="m2"><p>وز باز کبک باک ندارد به کوهسار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>وانجاکه جود باید و احسان و مکرمت</p></div>
<div class="m2"><p>ابری بودکه موج زند در دلش بخار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به‌زان دهد صِلَتْ‌ که کند مادح آرزو</p></div>
<div class="m2"><p>مه‌ زان دهد عطا که کند زایر انتظار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>باشد دو چیز مختلف از جود او به‌هم</p></div>
<div class="m2"><p>آسایش افاضل و رنج خزینه‌دار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>وانجا که حلم‌ باید و بخشایش گناه</p></div>
<div class="m2"><p>عَفْوش خبر دهد که رحیم‌ است و بردبار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>جرم‌ گناهکار کند عفو بیش از آنک</p></div>
<div class="m2"><p>در پیش او زبان بگشاید به اعتذار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هنگام عفو و رحمت او در مناظره</p></div>
<div class="m2"><p>از بیگناه چیره‌تر آید گناهکار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>وانجا که رزم باید و پیکار و تاختن</p></div>
<div class="m2"><p>بر مرکب شجاعت و مردی بود سوار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گاهی کند حصار چو صحرا زعزم خویش‌</p></div>
<div class="m2"><p>گاهی به جزم خویش ز صحرا کند حصار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>تیغش ز دور عرضه کند صورت اجل</p></div>
<div class="m2"><p>بر پیل کارزاری و بر شیر مرغزار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دندان شیر در دهن از خون چنان کند</p></div>
<div class="m2"><p>کاندر کفیده نار بود دانه‌های نار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>زیبد کزان جهان به نظاره به این جهان</p></div>
<div class="m2"><p>آیند جان رستم و جان سفندیار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تا چون بگیرد او به سنانی هزار خصم</p></div>
<div class="m2"><p>بوسند دست او به زمانی هزار بار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شخصی به‌این صفت‌ که شنیدست در جهان</p></div>
<div class="m2"><p>اندر شمار یک تن و اندر هنر هزار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گویی نگاشته است یکی صورت از هنر</p></div>
<div class="m2"><p>هنگام آفرینش او آفریدگار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ای روز بار تو دل زوّار پر نشاط</p></div>
<div class="m2"><p>وی روز رزم تو سر حَسّاد پر خمار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>میدان تو مگر عرصات است روز رزم</p></div>
<div class="m2"><p>ایوان تو مگر عرفات است روز بار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>زان اختیاری از امرا شاه و خواجه را</p></div>
<div class="m2"><p>کان از ملوک و این ز وزیران شد اختیار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چون هر دو را به‌ دیدن روی تو بود رای</p></div>
<div class="m2"><p>روی از دیار خویش نهادی برین دیار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>مردانه‌وار ریگ بیابان گذاشتی</p></div>
<div class="m2"><p>با لشکری چو ریگ بیابان‌، گه شمار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>از فرّ پادشاه تو را یمن بر یمین</p></div>
<div class="m2"><p>وز دولت وزیر تو را یُسر بر یسار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>دریای بی‌کنار وزیرست و پادشاه</p></div>
<div class="m2"><p>عالم ز موج هر دو پر از درّ شاهوار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>تا تو ز رود بار به پیروزی آمدی</p></div>
<div class="m2"><p>چون کوه آهنین سوی دریای بی‌کنار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>گویی جهان به خواب همی بیند ای عجب</p></div>
<div class="m2"><p>کوه آمده ز جانب دریا به رود بار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ای حق‌گزار خواجه و خدمتگزار شاه</p></div>
<div class="m2"><p>خدمتگزار چون تو که دیده است و حق گذار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>من بنده مدح تاجوران‌ گفته‌ام بسی</p></div>
<div class="m2"><p>وان مدح در زمانه زمن هست یادگار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>دانند خدمت من و دارند حرمتم</p></div>
<div class="m2"><p>شاه بلند بخت و وزیر بزرگوار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>کردم تو را پرستش و کردم ستایشت</p></div>
<div class="m2"><p>تا یابم از ستایش تو عزّ و افتخار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>روزم شود خجسته چو گویی مرا بیا</p></div>
<div class="m2"><p>طبعم شود گشاده چو گویی مرا بیار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>تا با رضا و شکر بقا و عُلوّ بُوَد</p></div>
<div class="m2"><p>بادت مدام ساخته اسباب هر چهار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>راضی ز تو شهنشه و شاکر ز تو وزیر</p></div>
<div class="m2"><p>باقی به تو عشیرت و عالی به تو تبار</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>تا قار قیر باشد در لفظ فارسی</p></div>
<div class="m2"><p>چونانکه در عبارت ترکی است برف قار</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بادا چنانکه قار به ترکی سر عدوت</p></div>
<div class="m2"><p>مویت چنانکه در لغت بارسی است قار</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>تا باشد از دو جامه شب و روز را سَلَب</p></div>
<div class="m2"><p>کان هر دو را زظلمت و نورست پود و تار</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>روشن چو روز باد همیشه شب ولیت</p></div>
<div class="m2"><p>چون شب همیشه روز معادیت باد تار</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بر دشمنان دولت شاه زمانه باد</p></div>
<div class="m2"><p>از رزم و کارزار تو همواره کار زار</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>از تو بساط مملکتش را رسیده زود</p></div>
<div class="m2"><p>یک سر به قیروان و دگر سر به قندهار</p></div></div>