---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>آفتابی را همی ماند رخش عنبر نقاب</p></div>
<div class="m2"><p>هیچکس دیدست عنبر را نقاب از آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نقاب آفتاب و آسمان شاید ز ابر</p></div>
<div class="m2"><p>آفتاب دلبران را شاید از عنبر نقاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساحر و عطار شد زلفش که هر چون بنگرم</p></div>
<div class="m2"><p>پیشه دارد سِحر صِرف و مایه دارد مشک ناب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زآنکه خَمّ جَعْد او پشت مرا دارد به خَم</p></div>
<div class="m2"><p>زانکه تاب زلف او جان مرا دارد به تاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ظلم کردست آنکه اندر جعدش آوردست خم</p></div>
<div class="m2"><p>جور کردست آنکه اندر زلفش افکندست تاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آب رویش هر زمان اندر دلم آتش زند</p></div>
<div class="m2"><p>تا دلم بر آتش هجران او گردد کباب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من چو خواهم کرد فریاد آب از آتش برکشم</p></div>
<div class="m2"><p>او چو خواهد خورد تشویر آذر افروزد ز آب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کار صبر من شد از تیمار زلف او ضعیف</p></div>
<div class="m2"><p>جای خواب من شد از وسواس چشم او خراب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صبر من بشکست آری بشکند تیمار صبر</p></div>
<div class="m2"><p>خواب من بگسست‌ آری بگسلد وسواس خواب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زان نهفته در شکر بار تو در یاقوت سرخ</p></div>
<div class="m2"><p>چشم ‌من ‌همچون ‌سحاب ‌و لعل‌ باران چون سحاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر به چشم اندر سرشکم لعل‌گون شد باک نیست</p></div>
<div class="m2"><p>در دلم مدح خداوند است چون درّ خوشاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نصر میر مومنین پروردگار ملک و دین</p></div>
<div class="m2"><p>ملک سلطان را مؤیّد دین یزدان را شهاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن خداوندی که بر آرامگاه دولتش</p></div>
<div class="m2"><p>ره به دستوری همی یابد دعای مستجاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مَرْکَب اقبال او را در چراگاه بقا</p></div>
<div class="m2"><p>عِقد و خَلخال همه حوران عِنان است و رکاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رای او را هست‌گویی از بلندی و ضیاء</p></div>
<div class="m2"><p>هم به گردون اتصال و هم به‌خورشید انتساب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حلم‌ او ‌دادست گویی خاک هامون را درنگ</p></div>
<div class="m2"><p>جود او دادست گویی دور گردون را شتاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اختر فرزانگی را با دلش هست اقتران</p></div>
<div class="m2"><p>لشکر آزادگی را با کَفَش هست اِقتراب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>حضرت او تا بود اعیان ملت را مآل</p></div>
<div class="m2"><p>مجلس او تا بود ارکان دولت را مآب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ملت پیغمبری هرگز نیابد اِنقطاع</p></div>
<div class="m2"><p>دولت شاهنشهی هرگز نبید انقلاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آفتاب از آسمان در بُرج پیروزی رسید</p></div>
<div class="m2"><p>سجده برد ایوا‌نش را حتی توارَت بِا‌لحجاب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پیش کیکاووس اگر بودی چو تو یک محتشم</p></div>
<div class="m2"><p>هرگز از توران به ایران نامدی افراسیاب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای مؤثر در همه کس همچو اَ‌جرام سپهر</p></div>
<div class="m2"><p>ای ‌گرامی بر همه‌ کس همچو ایّام شباب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>حق‌گزاری همچو آب و کامکاری همچو باد</p></div>
<div class="m2"><p>سرفرازی همچو آتش بردباری چون تُراب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ذوالفقار بوتراب از آسمان آمد به‌زیر</p></div>
<div class="m2"><p>هست گویی کلک تو چون ذوالفقار بوتُراب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از توکافی تر نبیند هیچکس در هیچ فن</p></div>
<div class="m2"><p>وز تو عاقل تر نیابد هیچکس در هیچ باب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مرد اگرچه فضل دارد عاجز آید با تو هم</p></div>
<div class="m2"><p>باز اگر چه صید گیرد عاجز آید با عُقاب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>درگناه و در نیاز از توست هرکس را سؤال</p></div>
<div class="m2"><p>زانکه جز بخشایش و بخشش نفرمایی جواب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آهن دولت تو را نرم است و هستی زین سبب</p></div>
<div class="m2"><p>همچو داود پیمبر صاحب فَصلُ‌ا‌لخِطاب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در حساب عمر تو گردون تفاریقی نبشت</p></div>
<div class="m2"><p>کان تفاریقش فذلک دارد از یوم‌الحساب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا مرا مهر تو همچون خون به‌ رگ‌ها شد درون</p></div>
<div class="m2"><p>از مشام من به ‌جای خوی همی آید گلاب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هست و خواهد بود از مدح و ثنای تو مرا</p></div>
<div class="m2"><p>اندرین گیتی بزرگی و اندران گیتی ثواب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا مصیب‌است آنکه بر فرقش همی پرد همای</p></div>
<div class="m2"><p>تا مصاب است آن‌که بر مرگش همی غُرّد غُراب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نیکخواهت باد بر نعمت مهنا و مصیب</p></div>
<div class="m2"><p>بدسگالت باد در مِحنت مُعزّا و مصاب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در دو دست تو دو چیز دلگشای جانفزا</p></div>
<div class="m2"><p>در یکی زلف بتان و در یکی جام شراب</p></div></div>