---
title: >-
    شمارهٔ ۲۰۰
---
# شمارهٔ ۲۰۰

<div class="b" id="bn1"><div class="m1"><p>تا باغ زرد روی شد از گشت روزگار</p></div>
<div class="m2"><p>بر سر نهاد تودهٔ کافور کوهسار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از برف شد بدایع کهسار در حجاب</p></div>
<div class="m2"><p>وز ابر شد صنایع خورشید در حصار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هامون برهنه گشت ز دیبای هفت رنگ</p></div>
<div class="m2"><p>گردون نهفته گشت بَه سنجاب سیل بار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باد صبا به باغ بسوزد همی بخور</p></div>
<div class="m2"><p>باد خزان به چرخ برآرد همی بخار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاغ سیاه یافت به میراث بوستان</p></div>
<div class="m2"><p>اماغ‌ا سپید داد به تاراج لاله زار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قمری کنون همی نسراید به گلستان</p></div>
<div class="m2"><p>بلبل‌ کنون همی نگراید به مرغزار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اذر به جای لالهٔ کوهی است با فروغ</p></div>
<div class="m2"><p>آذر به جای سوسن جویی است آبدار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هست آبگیر را به رخام اندرون مقام</p></div>
<div class="m2"><p>هست آفتاب را به کمان اندرون قرار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر دوش دشت هست زکافور طَیْلسان</p></div>
<div class="m2"><p>در گوش باغ هست ز دینار گوشوار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر روز بر درخت بپوشند جامه‌ای</p></div>
<div class="m2"><p>کش زرّ پخته پود بود سیم اخام‌ا تار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یک چند نوبهار بیاراست روی خویش</p></div>
<div class="m2"><p>آمد خزان و کرد نهان روی نوبهار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زودا که نوبهار برآرد سر از زمین</p></div>
<div class="m2"><p>گردد به دولت ثقه‌الملک آشکار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صدر عراقیان و خداوند رازیان</p></div>
<div class="m2"><p>بومسلم ستوده رئیس بزرگوار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نسل سروشیار پراکنده در جهان</p></div>
<div class="m2"><p>بومسلم است سید نسل سروشیار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گرگاه کودکی پدر از وی کناره شد</p></div>
<div class="m2"><p>بختش به عزّ و ناز بپرورد در کنار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شد بدسگال دولت او پیشکار خلق</p></div>
<div class="m2"><p>و او را همیشه بخت بلندست پیشکار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>او روز و شب ز خالق هفت آسمان به شکر</p></div>
<div class="m2"><p>دشمنش دام خدمت مخلوق را شکار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای درگه بلند تو تا‌لیف احتشام</p></div>
<div class="m2"><p>وی حضرت شریف تو تاریخ افتخار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در حق شناختن ز تو به نیست حق شناس</p></div>
<div class="m2"><p>در حق‌گزاردن ز تو ا‌مه‌ا نیست حق‌گزار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>روز درنگ تو نبود خاک را سکون</p></div>
<div class="m2"><p>روز شتاب تو نبود چرخ را مدار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کار هنر به همت تو گیرد استواء</p></div>
<div class="m2"><p>بند خرد به دولت تو گردد استوار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گفتار توست حجت تقدیر لم‌یزل</p></div>
<div class="m2"><p>کردار توست صورت توفیق کردگار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جاه تو وصف را ندهد پیش خویش راه</p></div>
<div class="m2"><p>بخت تو وهم را ندهد پیش خویش بار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سرگشته شد ز جود تو گردون به زیر عرش</p></div>
<div class="m2"><p>فرسوده شد ز حلم تو ماهی به زیر بار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ارکان دین ز جاه تو جویند ایمنی</p></div>
<div class="m2"><p>اعیان ری ز رای تو خواهند زینهار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از عزم خویش بر دل مردان زنی رقم</p></div>
<div class="m2"><p>وز حزم خویش بر سر شیران ‌کنی فسار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آسایش قضا و قدر زیر دست توست</p></div>
<div class="m2"><p>با خامهٔ تو هر دو رفیقند و سازگار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آن ساختی به خامه که هرگز نساختند</p></div>
<div class="m2"><p>موسی به چوب زنده و حیدر به ذوالفقار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا کی ز جود صاحب عَبّاد و همتش</p></div>
<div class="m2"><p>در خدمت تو هست به همت چنو هزار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نبتی‌ که بر دمد به سپاهان ز خاک او</p></div>
<div class="m2"><p>هر ساعتی ثنای تو گوید هزار بار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ای بخت تو فراشته بر آسمان علم</p></div>
<div class="m2"><p>وی نام تو نگاشته بر مشتری نگار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>من ا‌کهترا آمدم ز نشابور سوی ری</p></div>
<div class="m2"><p>وز بهر خدمت تو گذشتم بدین دیار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در مجلس تو بود یکی شاعر عزیز</p></div>
<div class="m2"><p>زان شاعر عزیز معزی است یادگار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از شهریار خلعت و منشور یافتم</p></div>
<div class="m2"><p>مقبل شدم به خلعت و منشور شهریار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دانم که اختیار پدر خدمت تو بود</p></div>
<div class="m2"><p>من نیز چون پدرکنم این خدمت اختیار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ده روز مدح‌گوی بوم بر بساط تو</p></div>
<div class="m2"><p>زان بس شوم به خدمت سلطان روزگار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دریاست خاطر من و گوهر در او سخن</p></div>
<div class="m2"><p>در مجلس شریف توگوهر کنم نثار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شعری که خاطرم به معانی بپرورد</p></div>
<div class="m2"><p>باشد یکی طویله پر از در شاهوار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در نقد و در شناختن شعرهای خویش</p></div>
<div class="m2"><p>بر همّت و کفایت تو کردم اختصار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تا هست در زمانهٔ فانی بلند و پست</p></div>
<div class="m2"><p>تا هست در میانهٔ‌ گیتی عزیز و خوار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بادی بلند و دشمن تو پست و سرنگون</p></div>
<div class="m2"><p>بادی عزیز و حاسد تو خوار و خاکسار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>اقبال همنشین تو بالصّیف والشّتاء</p></div>
<div class="m2"><p>توفیق رهنمای تو باللیل والنهار</p></div></div>