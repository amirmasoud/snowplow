---
title: >-
    شمارهٔ ۲۲۳
---
# شمارهٔ ۲۲۳

<div class="b" id="bn1"><div class="m1"><p>چون ز سلطانان گیتی شهریاراست اختیار</p></div>
<div class="m2"><p>فَرِّخ آن صاحب‌ که باشد اختیار شهریار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صاحبی باید که باشد کاردان و ‌دوربین</p></div>
<div class="m2"><p>درخور صاحبقرانی کامران و کامکار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صاحب دنیا به صدر اندر نظام‌الدین سزد</p></div>
<div class="m2"><p>چون معزالدین بود صاحبقران روزگار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخت تلقین کرد و تأیید الهی ره نمود</p></div>
<div class="m2"><p>تا معزالدین معزالدوله را کرد اختیار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشرق و مغرب مهیا شد چو سلطان جهان</p></div>
<div class="m2"><p>داد کار مشرق و مغرب به دست مرد کار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صاحبی بنشست در دیوان که از انصاف او</p></div>
<div class="m2"><p>خیر و راحت کرد روزی بندگان را کردگار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صدر نیک اختر محمد بن‌ سلیمان آنکه هست</p></div>
<div class="m2"><p>چون محمد دین‌پرست و چون سلیمان ملک‌دار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از نظام رسم او شد شغل‌گیتی بر نظام</p></div>
<div class="m2"><p>وز نگار کلک او شد کار عالم چون نگار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باغ ملت را ز رسم او پدید آمد درخت</p></div>
<div class="m2"><p>سال دولت را ز عدل او پدید آمد بهار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بوی خلق او معطرکرد صحن بوستان</p></div>
<div class="m2"><p>سرو عقل او مزین‌کرد طر‌ف جویبار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گلبنی بنشاند انصافش به ‌ملک اندر که هست</p></div>
<div class="m2"><p>شاخش اندر قیروان و بیخش اندر قندهار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رای او امروز ما را کرد خرم‌تر ز دی</p></div>
<div class="m2"><p>فر او امسال ما را کرد فرخ‌تر ز پار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شد ز نور طلعت او دیدهٔ ملت قریر</p></div>
<div class="m2"><p>یافت از تدبیر و رای او دل دولت قرار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ملک را با سیرت او هست جای تهنیت</p></div>
<div class="m2"><p>خلق را در خدمت او هست جای افتخار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رام شد چون مرکبی در زیر بختش آسمان</p></div>
<div class="m2"><p>آسمان مرکب سزد چون بخت او باشد سوار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روی هامون را ز بهر جود او زرین کند</p></div>
<div class="m2"><p>چون برآید بامدادان آفتاب از کوهسار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>توتیا سازد سپهر از بهر چشم اختران</p></div>
<div class="m2"><p>چون ز نعل مرکب او از زمین خیزد غبار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بی هوای او نباشد مهر تابان را مسیر</p></div>
<div class="m2"><p>بی مراد او نباشد چرخ ‌گردان را مدار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شمس بودی عقل او گر شمس بودی بی‌زوال</p></div>
<div class="m2"><p>بحر بودی جود او گر بحر بودی بی‌کنار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مهتران را از حوادث هست توقیعش پناه</p></div>
<div class="m2"><p>کهتران را از نوایب هست درگاهش حصار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آب را مانَد تو گویی طبع او گاه لَطَف</p></div>
<div class="m2"><p>خاک را ماند تو گویی حلم او گاه وقار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چیست آن آبی‌کزو یابد موافق آبروی</p></div>
<div class="m2"><p>چیست آن خاکی کزو گردد مخالف خاکسار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا به‌ بار آمد گل اقبال او در باغ ملک</p></div>
<div class="m2"><p>هست بدخواهان او را زان گل اندر دیده خار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در صدف دریا به نور رای او سازد همی</p></div>
<div class="m2"><p>از سرشک ابر مروارید و در شاهوار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از پی آن تا تواند کرد قهر دشمنان</p></div>
<div class="m2"><p>مرد را گر زور و قوت باید اندر کارزار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>روز قهر دشمنان در پیش عزم و حزم او</p></div>
<div class="m2"><p>سیل‌ها را در جبال و موج‌ها را در بحار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون قلم گیرد بود روح‌الامینش بر یمین</p></div>
<div class="m2"><p>چون عنان‌گیرد بود بخت بلندش بر یسار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>حشمت او هست اصل و کار دیوان هست فرع</p></div>
<div class="m2"><p>فرع باشد بی‌خلل چون اصل باشد استوار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ماه و خورشید از محاق و از کسوف ایمن شوند</p></div>
<div class="m2"><p>گر زعالی رای او خواهند هر دو زینهار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سائلی کز جود او یابد نِعَم هنگام بر</p></div>
<div class="m2"><p>زایری‌کز عدل او یابد نظر هنگام بار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آن شود همچون خلیل از باد او آتش‌نشین</p></div>
<div class="m2"><p>وین شود همچون کلیم از فر او دریاگذار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از شرار نار دوزخ عفو او سازد سرشک</p></div>
<div class="m2"><p>وز سرشک آب حیوان خشم او سازد شرار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>فرق بر فَرْقَد رسد گر جاه او یابد کلاه</p></div>
<div class="m2"><p>شِعر بر شَعْری رسد گر نام او یابد شعار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر بماند یادگار از هر کسی اندر جهان</p></div>
<div class="m2"><p>من چنان خواهم که او ماند به‌جای یادگار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ای تبار تو ز حشمت سید و فخر کِرام</p></div>
<div class="m2"><p>ای تو از جاه و جلالت سید و فخر تبار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>رای تو خورشید را ماند که چون پیدا شود</p></div>
<div class="m2"><p>روشنایی گستراند بر بلاد و بر دیار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>پیش ازین هرچ از مکارم بود در گیتی نهان</p></div>
<div class="m2"><p>کرد در عصر تو اکنون دورگردون آشکار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مشک‌خوار و گوهرافشان است کلک اندر کفت</p></div>
<div class="m2"><p>چون بود گوهرفشان کلکی که باشد مشک‌خوار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گاه مهر و گاه کین از مد و نقش او شوند</p></div>
<div class="m2"><p>دوستان شاد وگرامی دشمنان غمگین و خوار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مرغ بی‌پَرَّست و زو نامه همی پرد چو مرغ</p></div>
<div class="m2"><p>مار بی‌بیچ است وزو دشمن همی پیچد چو مار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>جز به دست چون تویی معجز نباشد آن قلم</p></div>
<div class="m2"><p>جز به دست مرتضی معجز نباشد ذوالفقار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شاه‌گیتی را کنون بر توست جای اعتماد</p></div>
<div class="m2"><p>ور چه گیتی از عجایب هست جای اعتبار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>آدمی اسباب دنیا از تو جوید بر دوام</p></div>
<div class="m2"><p>ور چه هست اسباب دنیا آدمی را مستعار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>از شراب خدمت تو هرکه مست و خرم است</p></div>
<div class="m2"><p>ایمن است از آفت بی‌عقلی و رنج خمار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>وانکه از اقبال تو امید دارد یک نظر</p></div>
<div class="m2"><p>گردد امیدش وفا بی‌وعده و بی‌انتظار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>هیچکس دُرّ ثنا را چون تو نگزارد بها</p></div>
<div class="m2"><p>هیچ‌کس زَرِّ سخن را چون تو نشناسد عیار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چون ز مدح تو بیارایم عروس طبع را</p></div>
<div class="m2"><p>بر مثال لعبتی سیمین‌بر و مشکین‌عِذار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>مشتری زیبدکه باشد خاتم او را نگین</p></div>
<div class="m2"><p>ماه نو شاید که باشد ساعد او را سوار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>هرکه پیش تو نثاری آرد از زر و گهر</p></div>
<div class="m2"><p>از ره معنی نثار او نباشد پایدار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>من تو را اکنون نثاری پایدار آورده ام</p></div>
<div class="m2"><p>بر بساط چون تو دَستوری چنین باید نثار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تا ز بهر بی‌نیازی و ز بهر بی‌غمی</p></div>
<div class="m2"><p>کیمیای نعمت و شادی عٍقارست و عُقار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>آب دست و خاک پایت باد دایم خلق را</p></div>
<div class="m2"><p>مایهٔ شادی و نعمت چون عُقار و چون عِقار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ماهرویان طراز و مشک مویان ختن</p></div>
<div class="m2"><p>پیش تو هنگام خدمت صف کشیده در قطار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>کودکانی کرده از خوبی دل مردم اسیر</p></div>
<div class="m2"><p>آهوانی کرده از شوخی دل شیران شکار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>خط ایشان مشکبوی و خال ایشان مشک‌رنگ</p></div>
<div class="m2"><p>جعد ایشان مشک بیز و زلف ایشان مشکبار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>روز رزم و روز بزم از سهم و جشن هر یکی</p></div>
<div class="m2"><p>رزمگه دشت قیامت‌، بزمگه دارالقرار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تا هزار اندر عدد بیش از یکی باشد همی</p></div>
<div class="m2"><p>تا یکی باشد همی اصل عدد اندر شمار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>باد فرمان تو اندر مشرق و مغرب یکی</p></div>
<div class="m2"><p>زیر فرمان تو باد اقبال و دولت صدهزار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>هر کجا رای تو باشد چرخ بر تو مهربان</p></div>
<div class="m2"><p>هرکجا عزم تو باشد دهر با تو سازگار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>اندر احکام شریعت عصمت یزدانت پشت</p></div>
<div class="m2"><p>واندر اسباب وزارت دولت سلطانْتْ یار</p></div></div>