---
title: >-
    شمارهٔ ۲۹۸
---
# شمارهٔ ۲۹۸

<div class="b" id="bn1"><div class="m1"><p>موسم عید و لب دجله و بغداد خُرَم</p></div>
<div class="m2"><p>بوی ریحان و فروغ قدح و لاله به هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه جمع اند و به یک جای مهیا ‌شده‌اند</p></div>
<div class="m2"><p>از پی عشرت شاه عرب و شاه عجم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رکن اسلام ملک شاه جهانگیر شهی</p></div>
<div class="m2"><p>که امام ملکان است و خداوند اُ‌مم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اندر آن وقت ‌که بر لوح قلم رفت همی</p></div>
<div class="m2"><p>فخر کردند به پیروزی او لوح و قلم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>علمش دفتر اشکال اقالیم شدست</p></div>
<div class="m2"><p>زانکه صد باره بپیمود جهان زیر علم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لب شیران همه آنجاست ‌که او راست رکاب</p></div>
<div class="m2"><p>سرشاهان همه آنجاست که او راست قدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از حد مشرق و چین تا به‌ حد مغرب و روم</p></div>
<div class="m2"><p>عدل اوکرد تهی از بد و خالی زستم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر هنرمند که از خدمت او جوید نام</p></div>
<div class="m2"><p>شجر همتش از دولت او یابد نم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خلق را نیست به از درگه او هیچ پناه</p></div>
<div class="m2"><p>صید را هیچ حصاری نبود به ز حرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در میان خرد و حکمت اگر حکم‌ کند</p></div>
<div class="m2"><p>بهتر از رای صوابش نبود هیچ حکم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بخشش یم به بر بخشش او باشد خرد</p></div>
<div class="m2"><p>دانش چرخ بر دانش او باشد کم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنچه او داند در ملک کجا داند چرخ</p></div>
<div class="m2"><p>وانچه او بخشد در جود کجا بخشد یم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای فلک را به علمهای رفیع تو شرف</p></div>
<div class="m2"><p>وی ملک را به قدمهای عزیز تو قسم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جز برای عدوی تو ننهد گردون دام</p></div>
<div class="m2"><p>جز به‌کام ولی تو نزند گیتی دم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خالق عرش سه چیز سه پیمبر به تو داد</p></div>
<div class="m2"><p>معجز عیسی و عمر خضر و شاهی جم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کنیت و نام و خطاب تو در اسلام بس است</p></div>
<div class="m2"><p>تا قیامت شرف خطبه و دینار و درم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نیمی از بتکدهٔ هند برانداخته‌ای</p></div>
<div class="m2"><p>وان دگر نیمه به شمشیر براندازی هم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرکبان تو به سم خرد بخواهند شکست</p></div>
<div class="m2"><p>هرچه در خانهٔ‌ کفار صلیب است و صنم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هرکه از چشمهٔ مهر تو کند آب حیات</p></div>
<div class="m2"><p>بارد از ابر سخای تو بر او قطر نعم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وانکه باکین تو خواهد که شود جفت و ندیم</p></div>
<div class="m2"><p>نشود جان ز تنش تا نشود جفت ندم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بس امیرا که مر او را نه حشم بود و نه خیل</p></div>
<div class="m2"><p>گشت در خدمت درگاه تو با خیل و حشم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بس دلیرا که ز سیاره خدم خواست همی</p></div>
<div class="m2"><p>چون تورا دید ببوسید زمین همچو خدم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر گنه کار چو قادر شوی از کردهٔ او</p></div>
<div class="m2"><p>نکنی یاد و کنی عفو و همین است‌ کرم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یک دل اندر همه‌ گیتی نشناسم که بر او</p></div>
<div class="m2"><p>نیست از منت تو داغ و ز شکر تو رقم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا ز باغ ارم از خوشی و خوبی مثل است</p></div>
<div class="m2"><p>باد بزمت به خوشی خوب‌تر از باغ ارم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو جهان بخش و جهانگیر نشسته شب و روز</p></div>
<div class="m2"><p>نیکخواه تو به شادی و بداندیش به غم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دل دینداران در عهد تو چون تیر توراست</p></div>
<div class="m2"><p>پشت بدخواهان مانند کمان تو به خم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بر تو میمون و براولاد و عبید و خَدَمَت</p></div>
<div class="m2"><p>عید فرخنده و بغداد و لب دجله به هم</p></div></div>