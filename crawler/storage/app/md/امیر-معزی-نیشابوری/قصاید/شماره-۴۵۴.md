---
title: >-
    شمارهٔ ۴۵۴
---
# شمارهٔ ۴۵۴

<div class="b" id="bn1"><div class="m1"><p>بر هوا ابر بهاری سیم پالاید همی</p></div>
<div class="m2"><p>بر زمین باد شمالی مشک پیماید همی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گُلْستان نقاش گشت و نقشها سازد همی</p></div>
<div class="m2"><p>بوستان عطار گشت و عطرها ساید همی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر درختی در چمن چون دختر رز حامله است</p></div>
<div class="m2"><p>بَچّگانِ رنگ‌رنگ و گونه‌گون زاید همی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارد از کافور کهسار افسری بر فرق خویش</p></div>
<div class="m2"><p>نور خورشید افسر از کُهسار بِرباید همی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از سوی بالا به پستی سیل بشتابد همی</p></div>
<div class="m2"><p>وز سوی پستی به بالا ابر بگراید همی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شب همی کاهد چو عمر دشمنان شهریار</p></div>
<div class="m2"><p>روز همچون دولت و ملکش بیفزاید همی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسرو دنیا ملکشاه آن‌که اندر باغ ملک</p></div>
<div class="m2"><p>دست عدلش سرو دولت را بپیراید همی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سیرت او دفتری هر ماه بنگارد همی</p></div>
<div class="m2"><p>دولت او کشوری هر سال بگشاید همی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرچه آزادی بهر حالی بِه‌ است از بندگی</p></div>
<div class="m2"><p>خسروان را بنده بودن پیش او شاید همی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کار عالی همتس بخشیدن و بخشودن است</p></div>
<div class="m2"><p>زان‌ که دایم یا ببخشد یا ببخشاید همی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عادت او روز و شب‌ گرد جهان برگشتن است</p></div>
<div class="m2"><p>آفتاب است او که ازگردش نیاساید همی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شرق تا غرب جهان اندر خط پیمان اوست</p></div>
<div class="m2"><p>پادشاهی و خداوندی چنین باید همی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هست بی‌پیغمبری چون معجز پیغمبران</p></div>
<div class="m2"><p>هر چه اندر روزگار او پدید آید همی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زهر باید هر ملک را تا نهان دشمن‌ کشد</p></div>
<div class="m2"><p>کین او زهری است کاندر حال بگزاید همی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وان زدوده تیغ‌ گوهردار او چون صیقل است</p></div>
<div class="m2"><p>کز دل کفار زنگ کر بزداید همی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خسروان دعوی بی‌برهان فراوان کرده‌اند</p></div>
<div class="m2"><p>شاه چون دعوی کند برهانش بنماید همی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بیش ازین برهان چه باید کاو هنوز اندر عراق</p></div>
<div class="m2"><p>در بخارا خطبه از نامش بیاراید همی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای جوان دولت جهانداری که دست روزگار</p></div>
<div class="m2"><p>جامهٔ عمر تو را هرگز نفرساید همی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در جهانداری تو را ایام بپسندد همی</p></div>
<div class="m2"><p>مهتران را کهتری حکم تو فرماید همی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هم برین سیرت بمان و هم برین عادت بپای</p></div>
<div class="m2"><p>تا فلک ماند همی و تا زمین پاید همی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>باد ،‌عمرت جاودان تا در بهار و در خزان</p></div>
<div class="m2"><p>باده پیمایی که دشمن باد پیماید همی</p></div></div>