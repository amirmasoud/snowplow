---
title: >-
    شمارهٔ ۴۴۸
---
# شمارهٔ ۴۴۸

<div class="b" id="bn1"><div class="m1"><p>تیره شد ماه خرد بر آسمان مهتری</p></div>
<div class="m2"><p>خشک شد سرو هنر در بوستان سروری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راست پنداری که جنبان شد زمین از زلزله</p></div>
<div class="m2"><p>پاره شد از جنبش او بارهٔ اسکندری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کس ندید این حادثه کز روز یکساعت شده</p></div>
<div class="m2"><p>در نشیب افتد زبالا آفتاب خاوری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بامداد از دولت و نیک‌اختری ثانی نداشت</p></div>
<div class="m2"><p>چاشتگه فانی شد اندر محنت و شوم اختری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مردمان‌گفتند سعد آید زگردون قسم خلق</p></div>
<div class="m2"><p>چون قران سازد به چرخ اندر زحل با مشتری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قسم فخرالملک نحس آمدکنون پیش از قران</p></div>
<div class="m2"><p>سعد بیرون رفت‌گویی از سپهر چنبری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باغ‌ دولت را مظفر بود و سعد سرفراز</p></div>
<div class="m2"><p>هرکه دیدی غرهٔ دیدار اوگفتی فری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا که او پژمرده شد بسیار گل پژمرده شد</p></div>
<div class="m2"><p>سرو چون پژمرده گردد گل کجا ماند تری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرچه وافر بود مالش‌ گشت عصرش مختصر</p></div>
<div class="m2"><p>ورچه کامل بود عقلش گشت عمرش سرسری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون نگین در حلقهٔ انگشتری شایسته بود</p></div>
<div class="m2"><p>ازچه شدگیتی بر او چون حلقهٔ انگشتری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای زمین اندر کنارت گوهری با قیمت است</p></div>
<div class="m2"><p>قدر آن‌گوهر بدان با او مکن بدگوهری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زینهار از طبع او نورکریمی نگسلی</p></div>
<div class="m2"><p>زینهار از روی او نقش بزرگی نَستری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر به پای خویش بسپاری سپردست او تورا</p></div>
<div class="m2"><p>تومگر او را به پای خویش هرگز نسپری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این سخن با تومحال و بیهده است از بهرآنک</p></div>
<div class="m2"><p>تو به‌گوهر نفس فرسایی و صورت پروری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای سرایی‌کز وجود و از عدم داری دو در</p></div>
<div class="m2"><p>هرچه از یک دردرآری از دگر بیرون بری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر تو ما را دوستی با آفت است این دوستی</p></div>
<div class="m2"><p>ور تو ما را مادری بی‌راحت است این مادری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای سپهر بی‌وفا بازی‌گری دانی مگر</p></div>
<div class="m2"><p>کز شگفتی هر زمانی بر مثال دیگری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عالمی را از ثریّا در ثَرَی انداختی</p></div>
<div class="m2"><p>کس نکردست ای عجب زین طرفه‌تر بازیگری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای نظام‌الدین همی خواهم‌که یک بار دگر</p></div>
<div class="m2"><p>چشم بگشایی و درکار خلایق بنگری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای شهید بن شهید از درد تو ناچیز شد</p></div>
<div class="m2"><p>بی‌نهایت امتی از شهریار و لشکری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بر دریغ تو خروشان است وگریان باب تو</p></div>
<div class="m2"><p>گاه آن آمد که گویم کم‌خروش وکم‌گری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بندگان خویش را وَیْحَک نبخشایی همی</p></div>
<div class="m2"><p>از تپانچه‌کرده روی لاله‌گون نیلوفری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گشت سرو اندر فراق تو خمیده چون کمان</p></div>
<div class="m2"><p>خوشهٔ سنبل برید‌ه برَ ‌بَرِ مه مشتری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نیستی پیدا ندانم تا کجا داری نشست</p></div>
<div class="m2"><p>ایمن و ساکن همانا خفته اندر بستری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یا به حاجت با نماز و روزه پیش ایزدی</p></div>
<div class="m2"><p>یا به خدمت پیش تخت شاه مشرق سنجری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یا به دیوان با بزرگان شغلها سازی همی</p></div>
<div class="m2"><p>یا به ایوان با ندیمان جفت جام و ساغری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>این سعادت باد یارت‌ کز قضای ایزدی</p></div>
<div class="m2"><p>گشته با حضرت چو مظلومان به تابوت اندری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو ملک بودی و دیوی شخص تومجروح‌کرد</p></div>
<div class="m2"><p>تا ز چشم آدمی پنهان شدی همچون پری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زان سپس کز دست شیران جهان خوردی شراب</p></div>
<div class="m2"><p>کی‌گمان بردم‌که از دست سگی شربت خوری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>روز عاشورا به زاری‌ کشته گشتی چون حسین</p></div>
<div class="m2"><p>زان سعادت با حسین اندر شهادت همسری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>صدر عالم بودی و هرگز ندانستم که تو</p></div>
<div class="m2"><p>صدر بگذاری و از عالم به زودی بگذری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شاه مشرق را پدر بودی کجا رفتی کنون</p></div>
<div class="m2"><p>کز پسر گشتی جدا وز لشکرش‌ گشتی بری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گرچه از سوز تو روز ما چو روز محشرست</p></div>
<div class="m2"><p>شاه مشرق را شفاعت خواه روز محشری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر هوا از بوی خلقت بود مشکین شصت سال</p></div>
<div class="m2"><p>شد زمین از بوی خلقت تا قیامت عنبری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ازکنار صفهٔ زرین اگر غایب شدی</p></div>
<div class="m2"><p>با پدر در خلد رضوان بر کنار کوثری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تا منور مرقدت پرنور و پر ریحان بود</p></div>
<div class="m2"><p>من نپندارم‌ که با هول نکیر و منکری</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سیرت والا ز تو در هفت کشور ظاهرست</p></div>
<div class="m2"><p>گر تو پنهان‌ گشته اندر خاک چارم ‌کشوری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خالقی‌کاندر فراقت کرد گریان چشم ما</p></div>
<div class="m2"><p>داور حق است و با او نیست ما را داوری</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>صبر بادا در فراق تو شه آفاق را</p></div>
<div class="m2"><p>تا بیابد در صبوری پایهٔ پیغمبری</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نیک‌بختی باد و نیکوسیرتی نسل تورا</p></div>
<div class="m2"><p>زانکه کارت نیک‌عهدی بود و نیکومحضری</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تا فراق تو دل و جان معزی خسته‌کرد</p></div>
<div class="m2"><p>از دریغ و حسرت تو توبه‌کرد از شاعری</p></div></div>