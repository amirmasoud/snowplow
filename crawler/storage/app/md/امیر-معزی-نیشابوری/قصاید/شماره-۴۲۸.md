---
title: >-
    شمارهٔ ۴۲۸
---
# شمارهٔ ۴۲۸

<div class="b" id="bn1"><div class="m1"><p>ای چرخ پیر بندهٔ تدبیر و رای تو</p></div>
<div class="m2"><p>ای اختران چرخ همه خاک پای تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچند روشن‌اند و بلند آفتاب و ماه</p></div>
<div class="m2"><p>دارند روشنی و بلندی ز رای تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز کردگار عالم و سلطان روزگار</p></div>
<div class="m2"><p>موجود نیست در همه عالم ورای تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مستَظهری به حشمت موروث و مکتسب</p></div>
<div class="m2"><p>اصل است و نفس پاک دلیل وگوای تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لیکن تو را همیشه تفاخر بود به نفس</p></div>
<div class="m2"><p>کز نفس خاست دانش و عقل و ذکای تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دیگران بدین سه فضیلت زیادت است</p></div>
<div class="m2"><p>عز وجلال ومرتبه وکبریای تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نحس زُحَل همی رود و سعد مشتری</p></div>
<div class="m2"><p>در آسمان برابر خشم و رضای تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بحری است موج‌زن صدفی درفشان در او</p></div>
<div class="m2"><p>دست جواد و خامهٔ معجز نمای تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آزادگان شوند تو را بنده بی‌بها</p></div>
<div class="m2"><p>هرگه‌که بنگرند به فروبهای تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خوشتر ز مژدهٔ ظفر و وعدهٔ وصال</p></div>
<div class="m2"><p>درگوش بندگان سخن دل گشای تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در مجلس تو ساقی و می حور و کوثرست</p></div>
<div class="m2"><p>ماند به خلد مجلس راحت فزای تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ایزد جزای بنده به عقبی دهد همی</p></div>
<div class="m2"><p>تو شکرکن که داد به‌دنیا جزای تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پرواز دولت است و طواف فریشته</p></div>
<div class="m2"><p>گرد سرای پرده وگرد سرای تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>معلوم رای توست‌ که هستم ز دیرباز</p></div>
<div class="m2"><p>من بنده در سرای تو مدحت سرای تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تحسین کند زمانه چو خوانم مدیح تو</p></div>
<div class="m2"><p>آمین کند ستاره چو گویم دعای تو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هرچند قادرست زبانم به نظم و نثر</p></div>
<div class="m2"><p>امروز عاجزست ز شکر عطای تو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر من زبان خلق ستانم به عاریت</p></div>
<div class="m2"><p>شکر عطای تو نگزارم سزای تو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون در کف از عطای تو دارم هزارگان</p></div>
<div class="m2"><p>خواهم هزار جان‌ که سگالم ثنای تو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آری هزار جانی زیبد تو را ثنا</p></div>
<div class="m2"><p>چون زر هزارکانی بخشد سخای تو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا مهر بر سپهر بتابد خجسته باد</p></div>
<div class="m2"><p>روز جهانیان ز بقا و لقای تو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تابنده باد دایم پاینده در جهان</p></div>
<div class="m2"><p>چون مهر و چون سپهر لقا و بقای تو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هرگز برون مباد سر چرخ چنبری</p></div>
<div class="m2"><p>یک دم زدن زچنبر عهد و وفای تو</p></div></div>