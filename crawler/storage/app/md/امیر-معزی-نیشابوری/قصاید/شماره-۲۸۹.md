---
title: >-
    شمارهٔ ۲۸۹
---
# شمارهٔ ۲۸۹

<div class="b" id="bn1"><div class="m1"><p>عاشق بدری شدم کز عشق او گشتم هلال</p></div>
<div class="m2"><p>فتنهٔ سروی شدم‌ کز هجر او گشتم خلال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کیست چون من در جهان ‌کز عشق بدر و هجر سرو</p></div>
<div class="m2"><p>شخص دارد چون خلال و پشت دارد چون هلال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بینی آن دلبر که دارد قامت او شکل مد</p></div>
<div class="m2"><p>وز دو حرف مد دهان و زلف او ‌دارد مثال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مد اگر میم است و دال آنک دهان و زلف او</p></div>
<div class="m2"><p>آن یکی مانند میم و آن دگر مانند دال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساعتی عاشق فریبد خال او در زیر زلف</p></div>
<div class="m2"><p>ساعتی حیلت سگالد زلف او در پیش خال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز بر آن روی جهان‌آرای هرگز دیده‌ای</p></div>
<div class="m2"><p>عنبر عاشق فریب و سنبل حیلت سگال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زلف او شوریده دیدم حال من شوریده‌ گشت</p></div>
<div class="m2"><p>کردم از شوریده‌حالی رخ چو نیل و تن چو نال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر نداری باورم بنگر ا‌چسان نامیده‌اند ا</p></div>
<div class="m2"><p>نام او شوریده زلف و نام من شوریده‌حال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صبرم از دل دور گشت و خوابم از دیده نفور</p></div>
<div class="m2"><p>من چنین بی‌خواب و صبر و آرزومند وصال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر وصال از صبر آید من ‌کجا یابم مراد</p></div>
<div class="m2"><p>ور خیال از خواب خیزد من کجا یابم خیال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خواستم که آواز وصل او به ‌گوش آید مرا</p></div>
<div class="m2"><p>گوش من بر وصل بود از هجر دیدم گوشمال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هجر او گر نیست درویشی و پیری پس چرا</p></div>
<div class="m2"><p>جان شیرین بر من از هجرش همی‌گردد وبال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جز به ‌فرمان شریعت در حلال و در حرام</p></div>
<div class="m2"><p>نیست اندر هر مقامی اهل سنت را مقال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عشق نشناسد شریعت پس چراکرد ای عجب</p></div>
<div class="m2"><p>وصل او بر من حرام و هجر او بر من حلال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آفتاب وصل او را گر زوال آمد چه شد</p></div>
<div class="m2"><p>هست دیدار خداوند آفتاب بی‌زوال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صاحب اسرار سلطان سید احرار دهر</p></div>
<div class="m2"><p>آن‌که او هم تاج‌ملکت هست وهم دین را جمال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بوالغنایم مرزبان کز خدمتش هر مرزبان</p></div>
<div class="m2"><p>بر فلک دارد کشیده رایت عز و جلال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شعرکویان را کمان معنی اندر لفظ اوست</p></div>
<div class="m2"><p>تا نگویی مدح از معنی ‌کجا گیرد کمال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در دل بیدار و طبع روشن او حاصل است</p></div>
<div class="m2"><p>هرچه از فرهنگ باشد در دل و طبع رجال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کار او در ملک سلطان بخشش و بخشایش است</p></div>
<div class="m2"><p>نه ز بخشش‌ گیرد او را نه ز بخشایش ملال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جان مهیاکن به مهرش تا بمانی در هدی</p></div>
<div class="m2"><p>دل مصفا کن ز کینش تا نمانی در ضلال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پهلوانان بر رکاب او همی بوسه دهند</p></div>
<div class="m2"><p>تا به سعی او بیابند از شهنشه جاه و مال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پور زال اکنون ز عقبی‌ گر به دنیا آمدی</p></div>
<div class="m2"><p>همچو ایشان بر رکابش بوسه دادی پور زال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خواستی‌ گردون که بودی یک هلالی از چهار</p></div>
<div class="m2"><p>تا ز بهر دست و پای مرکبش بودی نعال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خواستی پروین ‌که بودی جِرم او شکل دراز</p></div>
<div class="m2"><p>تا میانش را کمر بودی رکابش را دوال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تاج از آن دادش لقب سلطان که دارد در قلم</p></div>
<div class="m2"><p>گوهری کش نیست همتا در بحار و در جبال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر به‌نزد پادشاهان عز تاج از گوهر است</p></div>
<div class="m2"><p>عز این‌گوهر به تاج است ا‌ینت‌ تاجی بی‌همال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گوهر تاج شهان در پیش این‌گوهر بود</p></div>
<div class="m2"><p>پیش یاقوت آبگینه پیش پیروزه سفال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای ز نعمای تو شاکر لشکر دنیا و دین</p></div>
<div class="m2"><p>ای ز آلای تو خشنود احمد مختار و آل</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>روبهی‌ کز بیم جان هرگز نگردد گرد شیر</p></div>
<div class="m2"><p>گر عنایت یابد از تو بشکند بر شیر یال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>باز اگر بر پای‌کبکان بسته بیند خط تو</p></div>
<div class="m2"><p>بر سرکبکان به خدمت‌ گستراند پر و بال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>حَلّ و عَقد ملک و دخل و خرج نعمتهای خویش</p></div>
<div class="m2"><p>زیر اقلام تو دارد خسرو نیکو خصال</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تاگرفتی ملک هفت اقلیم در زیر قلم</p></div>
<div class="m2"><p>دولت عالیت را هستند هفت اختر عیال</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چون قلم بگرفته بر منشور شه طغراکشی</p></div>
<div class="m2"><p>شاخ طوبی را بود با نقش مانی اتصال</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>طوبی آنکس راکه باشد خادم درگاه تو</p></div>
<div class="m2"><p>شاخ طوبی بر یمین و نقش مانی بر شمال</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بر زمین هر کس که با حکم تو باشد در جدل</p></div>
<div class="m2"><p>آسمان با حکم او همواره باشد در جدال</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ره نیابد روح او بر عالم‌ کبری به‌ جهد</p></div>
<div class="m2"><p>تا نگیرد جسم او در عالم صغری کمال</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>قطب‌گردون بسپرد خشم تو وقت انتقام</p></div>
<div class="m2"><p>پشت ماهی بشکند حلم تو وقت احتمال</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کَرِّ مادرزاد باشد هرکه‌ گوید بد تو را</p></div>
<div class="m2"><p>چون بود در آفرینش کرِّ مادر زاد لال</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بخشش هر کس بود با قیل و قال اندر جهان</p></div>
<div class="m2"><p>در جهان بخشندهٔ مطلق تویی بی‌قیل و قال</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>با سؤال آیند و با فکرت ‌بَرِ تو زایران</p></div>
<div class="m2"><p>بر تو یابند بیش از فکرت و بیش از سوال</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شکر بوبکر قهستانی بگوید چند جای</p></div>
<div class="m2"><p>فرخی در شعرهای خوشتر از آبِ زلال</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>من نخوانم خویشتن را فرخی لیکن تو را</p></div>
<div class="m2"><p>صد چو بوبکر قهستانی شناسم در نوال</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خویشتن را زان نخوانم فرخی ‌کز روی عقل</p></div>
<div class="m2"><p>در مدیح تو هم از من مدح من باشد محال</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تا سوارم بر معانی مرکب طبع مرا</p></div>
<div class="m2"><p>هست در میدان مدح تو همه ساله مجال</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>خاصه وقتی کز نسیم باد فروردین به‌ باغ</p></div>
<div class="m2"><p>حُلّه دارد هر چمن پیرایه دارد هر نهال</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دشت را از سبزه زنگاری همی بیند گوزن</p></div>
<div class="m2"><p>کوه را از لاله شنگرفی همی بیند غزال</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گلبنان درگلستان با یار و خلخال و عقد</p></div>
<div class="m2"><p>راست پنداری عروسانند با غَنْج و دَلال</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>قمریان چون عاشقان از عشقشان رفته زهوش</p></div>
<div class="m2"><p>بلبلان چون بیدلان از مهرشان رفته زحال</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>لشکر نوروز با خیل دی اندر بوستان</p></div>
<div class="m2"><p>گر نه آهنگ خصومت دارد و عزم قتال</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>پس چرا در بوستان از شاخ بید و برگ بید</p></div>
<div class="m2"><p>کرد بُسَّدْگون سهام و کرد میناگون نِصال</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گر به باغ از ارغوان و لاله و نسرین وگل</p></div>
<div class="m2"><p>حله‌های گونه‌گون بافد همی باد شمال</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>حله‌های کلک تو از حله‌ها نیکوترست</p></div>
<div class="m2"><p>کز خرد دارد طراز و از ادب دارد صقال</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تا ز ممدوحان دانا مادحان یابند جاه</p></div>
<div class="m2"><p>تا ز معشوقان زیبا عاشقان‌گیرند فال</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>عمر تو ممدوح باد و مادحانت روز و شب</p></div>
<div class="m2"><p>بخت تو معشوق باد و عاشقانت ماه و سال</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>خانهٔ عمر تو را گردونِ گردان پاسبان</p></div>
<div class="m2"><p>قلعهٔ بخت تورا خورشیدِ تابان کُوْ‌توال</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>شیرمردان را ز تشریفات تو عز و شرف</p></div>
<div class="m2"><p>رادمردن را ز توقیعات تو رفق و منال</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بر تو میمون اعتدال روز و شب وز عدل تو</p></div>
<div class="m2"><p>شغلها را استقامت‌، طبعها را اعتدال</p></div></div>