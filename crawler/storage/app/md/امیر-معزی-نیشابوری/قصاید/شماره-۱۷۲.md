---
title: >-
    شمارهٔ ۱۷۲
---
# شمارهٔ ۱۷۲

<div class="b" id="bn1"><div class="m1"><p>گر نکشی سر ز برم ای پسر</p></div>
<div class="m2"><p>عمر برم با تو به شادی بسر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور ببری پای خود از دام من</p></div>
<div class="m2"><p>دست من و دامن تو ای پسر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر سمن از مورچه داری نشان</p></div>
<div class="m2"><p>بر قمر از غالیه داری اثر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مورچه را چند نهی بر سمن</p></div>
<div class="m2"><p>غالیه را چند کشی بر قمر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر رخت از زنگ سپاه آورند</p></div>
<div class="m2"><p>سر به سر افسونگر و افسانه بر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روز و شب از بهر فسون و فسوس</p></div>
<div class="m2"><p>کرده زبان در دهن یکدگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مردم درویش توانگر شود</p></div>
<div class="m2"><p>چون رسدش دست به سیم و به زر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گشت به زرین‌رخ و سیمین‌سرشک</p></div>
<div class="m2"><p>عاشق درویش تو درویش تر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای جگرم خسته به تیر مژه</p></div>
<div class="m2"><p>کرده خم زلف دلم را سپر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر نبدی در خم زلفت دلم</p></div>
<div class="m2"><p>کوفته و خسته شدی خون جگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من چو بگریم‌ گهر آرم ز چشم</p></div>
<div class="m2"><p>تو چو بخندی ز لب آری شکر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هست تو را یک شکر از من دریغ</p></div>
<div class="m2"><p>نیست دریغ از تو مرا صد گهر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خون دل از دیده گشادی مرا</p></div>
<div class="m2"><p>تاکه به بیداد ببستی کمر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>داد من از تو نستاند به حق</p></div>
<div class="m2"><p>جز شرف‌الملک شه دادگر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خواجه ابوسعد محمد که هست</p></div>
<div class="m2"><p>صدر فلک‌ همت خورشید فر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بار خدایی که از او شاکرند</p></div>
<div class="m2"><p>بار خدایان جهان سر به سر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هست سرشته دل و جان و تنش</p></div>
<div class="m2"><p>از کرم و از خرد و از هنر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در همه علمیش نیابی نظیر</p></div>
<div class="m2"><p>گر کنی اندر همه عالم نظر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از قِبَل خدمت درگاه او</p></div>
<div class="m2"><p>رشک برد هر نفسی پا به سر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وز قِبَل دیدن دیدار او</p></div>
<div class="m2"><p>گوش و زبان را حسد است از بصر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای کَرَمت‌ بحری زرّین بخار</p></div>
<div class="m2"><p>ای قلمت ابری مشکین مطر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>لفظ تو دُرّست و معانی صدف</p></div>
<div class="m2"><p>رای تو جان است و معالی صور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>باغ ادب را سخن توست بار</p></div>
<div class="m2"><p>تخم سخا را کرم توست بر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>روشنی از سِرّ تو دارد ملک</p></div>
<div class="m2"><p>زیرکی از بِرّ تو دارد بشر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هر چه تو ‌نپسند‌ی باشد هبا</p></div>
<div class="m2"><p>هر چه تو نپذ‌یری باشد هدر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دُر ثمینی تو که هر سروری</p></div>
<div class="m2"><p>پیش تو باشد ز قیاس حجر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بحر محیطی تو که هر مهتری</p></div>
<div class="m2"><p>پیش تو باشد ز شمار شَمَر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دیو گر از مهر تو جوید نشان</p></div>
<div class="m2"><p>حور گر ازکین تو یابد خطر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آن ز سقر آید سوی جنان</p></div>
<div class="m2"><p>این ز جنان آید سوی سقر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ای شرف ملک شهی کاو گرفت</p></div>
<div class="m2"><p>ملک ز انطاکیه تا کاشغر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گرد برآورد بدو تاختن</p></div>
<div class="m2"><p>دولتش از خاور و از باختر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در کف او تیغ‌ کلید قضا است</p></div>
<div class="m2"><p>درکف تو کلک‌ْ کلید قدر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کلک تو مرغی است شگفت و بدیع</p></div>
<div class="m2"><p>از شَبَه منقارش و از سیم پر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گفتن او مشکل و رفتن نگون</p></div>
<div class="m2"><p>خوردن او عنبر و زادن دُرَر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زرد و بدو روضهٔ سیراب سبز</p></div>
<div class="m2"><p>خشک و ازو گلبن اقبال تر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از سخن آگاه و نداند سخن</p></div>
<div class="m2"><p>وز فَکَر آگاه و نداند فَکَر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>جنبش او ساکنی شرق و غرب</p></div>
<div class="m2"><p>شورش او ایمنی بحر و بر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بسته میان است ولیکن ز خیر</p></div>
<div class="m2"><p>بر همه آفاق گشادست در</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بار خدایا به ره شاعری</p></div>
<div class="m2"><p>هست مرا دولت تو راهبر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>خاطر من پر سخن مدح توست</p></div>
<div class="m2"><p>نکته بر و برگ و معانی ثمر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بر شجر خاطرم ار بشمری</p></div>
<div class="m2"><p>مدح تو بیش است ز برگ شجر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دفترم از مدح تو آکنده شد</p></div>
<div class="m2"><p>کیسه تهی‌ گشت ز خرج‌ سفر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از طربت باد مدد بر مدد</p></div>
<div class="m2"><p>وز ظفرت باد نفر بر نفر</p></div></div>