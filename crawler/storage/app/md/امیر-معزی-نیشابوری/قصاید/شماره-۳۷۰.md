---
title: >-
    شمارهٔ ۳۷۰
---
# شمارهٔ ۳۷۰

<div class="b" id="bn1"><div class="m1"><p>آنچه من بر چهره دارم یار دارد در میان</p></div>
<div class="m2"><p>وآنچه من در دیده دارم دوست دارد در دهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چهرهٔ من با میانش‌ گشت پنداری قرین</p></div>
<div class="m2"><p>دیدهٔ من با دهانش کرد پنداری قران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر تو را باور نیاید کاو دهان دارد چنین</p></div>
<div class="m2"><p>ور تورا صورت نبندد کاو میان بندد چنان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنگر اینک تا ببینی در پاکش در دهن</p></div>
<div class="m2"><p>بنگرآنک تا بیابی زر نابش در میان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بینی آن قد بلندش همچو تیر از راستی</p></div>
<div class="m2"><p>وان دل بی‌مهرش از ناراستی همچون کمان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دل من در قد او هست پنداری اثر</p></div>
<div class="m2"><p>وز قد من در دل او هست پنداری نشان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هست هجر او به‌وصل اندر چو بیم اندر امید</p></div>
<div class="m2"><p>هست وصل او به هجر اندر چو سود اندر زیان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قصد او آن است کز من دل رباید بی‌گزاف</p></div>
<div class="m2"><p>رای او آن است کز من جان ستاند رایگان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با چنو دلبر لئیمی‌ کرد نتوانم به‌دل</p></div>
<div class="m2"><p>با چنو جانان بخیلی کرد نتوانم به‌جان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درغم عشقش به‌ زرین چهره‌ و سیمین سرشک</p></div>
<div class="m2"><p>بر امید سود یک چندی شدم بازارگان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سود کردم عشق لیکن با زیان‌ گشتم زصبر</p></div>
<div class="m2"><p>اوفتد بازارگان را گاه سود و گه زیان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شادمانی چون کنم کز صبر مفلس گشته‌ام</p></div>
<div class="m2"><p>کی تواند بود هرگز مرد مفلس شادمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر ز صبرم مفلس از شادی‌ کند قارون مرا</p></div>
<div class="m2"><p>ناصح ملک و صفی حضرت شاه جهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پشت دین بوطاهر اسماعیل کاو را آفرید</p></div>
<div class="m2"><p>همچو اسماعیل طاهر کردگار غیب دان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در صفاهان چشمهٔ نعمت‌ گشاد از دست این</p></div>
<div class="m2"><p>گر به مکه چشمهٔ زمزم‌ گشاد از پای آن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دید روز و شب زمان را سخره و منقاد خویش</p></div>
<div class="m2"><p>داد در دستش زمام خویش پنداری زمان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زان سپس‌ کاو در خرد کافی ترست از هر مکین</p></div>
<div class="m2"><p>همت او در خرد عالی‌ترست از هر مکان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هست آرام روان را مهر او گویی سبب</p></div>
<div class="m2"><p>زانکه بی‌مهرش همی در تن نیارامد روان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زانکه بیند چشم و بستاید زبان او را همی</p></div>
<div class="m2"><p>گاه جان بر جسم رشک آرد گهی دل بر زبان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جاه هر سرور گمان‌ گشته است و جاه او یقین</p></div>
<div class="m2"><p>جود هر مهتر خبر گشته است و جود او عیان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا عیان باشد نباید دل نهادن بر خبر</p></div>
<div class="m2"><p>تا یقین باشد نباید تکیه‌کردن برگمان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای هنرمندی که پیش خاطرت هست آشکار</p></div>
<div class="m2"><p>هرچه اندر پرده دارد گنبد گردون نهان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گرچه هست اندر سمر فرزانگان را سرگذشت</p></div>
<div class="m2"><p>ورچه هست اندر کتب آزادگان را داستان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هیچ فرزانه نبودست از تو مه‌ در روزگار</p></div>
<div class="m2"><p>هیچ آزاده نبودست از تو به در باستان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هست بازار تو در پیش معزالدین روا</p></div>
<div class="m2"><p>هست فرمان تو در پیش قوام‌الدین روان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از سر اعلام توگردد زبون خصم دلیر</p></div>
<div class="m2"><p>وز سر اقلام تو گردد سبک شغل‌ گران</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در جلالت با اثیرت کرد پیوند آفتاب</p></div>
<div class="m2"><p>در سعادت با ضمیرت خورد سوگند آسمان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در کف او آتش خنجر نشان بینم همی</p></div>
<div class="m2"><p>باز بینم درکف تو خنجر آتش فشان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون زهم بگشایی اوراق جراید روز عرض</p></div>
<div class="m2"><p>وان همایون کلک گوهروار گیری در بنان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خاطر بیننده پندارد که بگذاری همی</p></div>
<div class="m2"><p>جوشن سیمین به‌نوک نیزهٔ مشکین سنان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کان یاقوت و زبرجد گرچه نشناسد کسی</p></div>
<div class="m2"><p>هست یاقوت و زبرجد را سرکلک توکان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آن چراغ است آن‌ که شد ملک از دخانش پرنگار</p></div>
<div class="m2"><p>ور ببینی پیکرش را پرنگارست از دخان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در عرب مشتاق تصنیفات او خرد و بزرگ</p></div>
<div class="m2"><p>در عجم محتاج توقیعات او پیر و جوان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دست‌ او بحرست و او چون خیزران‌ است و صدف</p></div>
<div class="m2"><p>بی‌شک اندر بحر باشد هم صدف هم خیزران</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ای به آیین مهتری کاندر کمال مهتری</p></div>
<div class="m2"><p>از تو آموزد همی هر مهتری آیین و سان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گرچه من‌ کهتر نبودم مدتی در مجلست</p></div>
<div class="m2"><p>کهتری بودم به ‌واجب شکرگوی و مدح‌خوان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از طراز شکر تو غایب نبودم یک نفس</p></div>
<div class="m2"><p>وز نگار مدح تو فارغ نبودم یک زمان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بعد از این غایب نگردم تا نگردد طبع من</p></div>
<div class="m2"><p>بستهٔ بند هوی و خستهٔ تیر هَوان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>با تو باشم تا ز فر بخت تو گردد مرا</p></div>
<div class="m2"><p>گنج حکمت زیر دست و اسب دولت زیر ران</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تا که از باد بهاری تازه‌ گردد لاله‌زار</p></div>
<div class="m2"><p>تا که از باد خزانی تیره ‌گردد گلستان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>باد طبع مادحت چون لاله‌زار اندر بهار</p></div>
<div class="m2"><p>باد روی حاسدت چون ‌گلستان اندر خزان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بزم تو چون باغ‌ و رامشگر در او چون عندلیب</p></div>
<div class="m2"><p>ساقیان چون لاله و نسرین و می چون ارغوان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بر سپهر نیکبختی شمس عقلت بی‌زوال</p></div>
<div class="m2"><p>بر زمین رادمردی بحر جودت بی‌کران</p></div></div>