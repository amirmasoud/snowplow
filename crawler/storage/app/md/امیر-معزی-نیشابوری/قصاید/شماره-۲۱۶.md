---
title: >-
    شمارهٔ ۲۱۶
---
# شمارهٔ ۲۱۶

<div class="b" id="bn1"><div class="m1"><p>امسال در آفاق دو عید است به یک بار</p></div>
<div class="m2"><p>بر ملت و دولت اثر هر دو پدیدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک عید ز ماه شب شوال و دگر عید</p></div>
<div class="m2"><p>از عافیت شاه جهانگیر جهاندار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تاج ملکان ناصر دین خسرو اسلام</p></div>
<div class="m2"><p>در نصرت دین نایب پیغمبر مختار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سنجرکه به خنجر سر بدخواه ببرد</p></div>
<div class="m2"><p>چونانکه سر خیبریان حیدر کرﹼار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاهی که شرف یافت در اسلام ز نامش</p></div>
<div class="m2"><p>هم نامه و هم خطبه و هم سکه و دینار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>او همچو درختی است برومند که هرگز</p></div>
<div class="m2"><p>زایل نشود سایهٔ او از سر احرار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از دین و خرد بیخش و از جود و کرم شاخ</p></div>
<div class="m2"><p>از عدل و هنر برگش و از فتح و ظفر بار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در مجلس او نعمت خلدست‌ گه بزم</p></div>
<div class="m2"><p>بر درگه او رحمت حشرست‌ گه بار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در ملک همی دولت او زرکند از خاک</p></div>
<div class="m2"><p>زانسان که همی باد صبا گل‌کند از خار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از خاک به جز دولت سنجر نکند زر</p></div>
<div class="m2"><p>از چوب به جز موسی عمران نکند مار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون او به دلیری و به ‌شمشیر و به ‌دولت</p></div>
<div class="m2"><p>هم ناصر دین آمد و هم قاهر کفار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>او را علم خویش فرستاد خلیفه</p></div>
<div class="m2"><p>با یاره و طوق و کمر و جبه و دستار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای درخور تو شاهی و تو درخور شاهی</p></div>
<div class="m2"><p>ایزد به سزاوار سپردست سزاوار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تأیید چو پرگار و ضمیر تو چو نقطه‌ است</p></div>
<div class="m2"><p>بر نقطه بود راستی‌ گردش پرگار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن بخت جوان است‌ که بر باد روان است</p></div>
<div class="m2"><p>یا شخص همایون تو بر مرکب رهوار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن چرخ بسیط است‌ که در بحر محیط است</p></div>
<div class="m2"><p>یا تیغ‌ گهر دار تو در دست گهربار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن دُرﹼ معالی است‌ که در دُرج معانی است</p></div>
<div class="m2"><p>یا شرح هنرهای تو در دفتر اشعار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یک عزم تو در رزم و یک آهنگ تو در جنگ</p></div>
<div class="m2"><p>بهتر بود از حملهٔ صد لشکر جرار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گشتند گریزنده چو از باز تذروان</p></div>
<div class="m2"><p>از گرز گران سنگ تو خصمان سبکسار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شیران همه‌ کردند ز شمشیر تو پرهیز</p></div>
<div class="m2"><p>شاهان همه دادند به اقبال تو اقرار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در صحت شخص تو صلاح است جهان را</p></div>
<div class="m2"><p>آن روز مبادا که بود شخص تو بیمار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کز صحت بیماری شخص تو بدیدم</p></div>
<div class="m2"><p>بخشایش جبار پس از قدرت جبار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای شاه پدید آمد شاهین طرب را</p></div>
<div class="m2"><p>دوش از فلک آینه‌گون زرین منقار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا از دل میخواره به‌ منقار کند صید</p></div>
<div class="m2"><p>گرد آمده سی روزه ی درد و غم و تیمار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دانی‌که پسندیده نباشد به چنین وقت</p></div>
<div class="m2"><p>میخواره به اندیشه و میخانه به مسمار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نه ساز و نه زخمه به‌ کف مطرب خاموش</p></div>
<div class="m2"><p>نه جام و نه ساغر به‌ کف ساقی بیکار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>فاسد شده تدبیر ندیمان معاشر</p></div>
<div class="m2"><p>کاسد شده بازار حریفان کم‌آزار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر حکم تو و رای دل آرای تو باشد</p></div>
<div class="m2"><p>آن شغل چو زر گردد و این‌ کار چو طیار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا دور کند گنبد دوار همی باد</p></div>
<div class="m2"><p>زیر قدم همت تو گنبد دوار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا سیر کند کوکب سیار همی باد</p></div>
<div class="m2"><p>زیر علم نصرت تو کوکب سیار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>یزدان ز تو راضی و خلیفه ز تو شاکر</p></div>
<div class="m2"><p>سلطان زتو دل شاد و تو را دولت او یار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>عید تو همایون و همه روز تو چون عید</p></div>
<div class="m2"><p>امروز تو از دی به و امسال تو از پار</p></div></div>