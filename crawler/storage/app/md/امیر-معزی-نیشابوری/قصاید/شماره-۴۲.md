---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>اگرچه ناموران را تفاخر از هنرست</p></div>
<div class="m2"><p>تفاخر هنر از شهریار نامورست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جلال دولت عالی جمال ملت حق</p></div>
<div class="m2"><p>که پادشاه جهان است و خسرو بشرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر زمانه بنازد ز عدل او نه شگفت</p></div>
<div class="m2"><p>که عدل او ز حوادث زمانه را سپرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به گِرد رایت او گَرد گر ظفر خواهی</p></div>
<div class="m2"><p>که ‌گرد رایت عالیش آیت ظفرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همیشه روشنی از رای اوست عالم را</p></div>
<div class="m2"><p>مگر که عالم چشم است ورای او بصر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خجسته دولت او آفتاب را ماند</p></div>
<div class="m2"><p>که هم به خاور از او نور و هم به باختر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر خرد ز دل آید دلش همه خردست</p></div>
<div class="m2"><p>و گر هنر ز تن آید تنش همه هنرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه بی‌ستایش او بر زبان کس سخن است</p></div>
<div class="m2"><p>نه بی‌ پرستش او بر میان کس کمر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از آن بود نظر مشتری خجسته به فال</p></div>
<div class="m2"><p>که بخت فرخ او را به مشتری نظرست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مناز خیره به قومی که پیشتر بودند</p></div>
<div class="m2"><p>به شاه ناز کز ایشان به ملک بیشتر است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پدرش بود به دولت زیاده از دگران</p></div>
<div class="m2"><p>به دین و دا‌نش و داد او زیاده از پدرست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خدایگانا فتح تو از میان فتوح</p></div>
<div class="m2"><p>به قدر و جاه چو سَبْع‌ُالمَثانی از سُوَرست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو آن شهی که هوای تو داد بی‌ستم است</p></div>
<div class="m2"><p>تو آن شهی‌ که رضای تو نفع بی‌ضررست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز روی عقل جهان چون تن است کان تن را</p></div>
<div class="m2"><p>مراد تو چو سر ورای تو چو چشم سرست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خدای عرش به حکم تو کرد گنج ملوک</p></div>
<div class="m2"><p>اگر چه بیش تو گنج ملوک بی‌خطرست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مگر مراد تو جزوی است از قضا و قدر</p></div>
<div class="m2"><p>که حَلّ و عَقدِ جهان از قضا و از قَدَرست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زمانه را دو درست از بدی و از نیکی</p></div>
<div class="m2"><p>حُسام و کِلک تو قفل و کلید آن دو درست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به شرق و غرب ز احسان وجود تو صفت است</p></div>
<div class="m2"><p>به بر و بحر ز انصاف و عدل تو سمرست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بسا کسا که چو آتش به کینهٔ تو شتافت</p></div>
<div class="m2"><p>کنون دو دیده پر از دود و دل پر از شررست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مگر عداوت تو آتش جگرسوز است</p></div>
<div class="m2"><p>که سال و ماه عدوی تو سوخته جگرست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شریف حضرت تو هست‌ کعبهٔ شاهان</p></div>
<div class="m2"><p>سریر تو چو مقام و رکاب تو حَجَرست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به مدح توست سزاوار هر کجا نُکَت است</p></div>
<div class="m2"><p>به تاج توست سزاوار هرکجا گهرست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مدایح تو همه مدح ما بیفروزد</p></div>
<div class="m2"><p>که طبع ما صدف است و مدیح تو دررست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به جز خدای تعالی هرآنچه هست دگر</p></div>
<div class="m2"><p>همه سراسر زیرست و بخت تو زبرست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تو را ز بخت و جهان را ز عدل تو هر روز</p></div>
<div class="m2"><p>بشارتی دگرست و سعادتی دگرست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همیشه تا که زمانه نتیجهٔ فلک است</p></div>
<div class="m2"><p>همیشه تا که مُحرّم مُقدّم صفرست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جهان تو گیر و ولایت تو بخش و شاه تو باش</p></div>
<div class="m2"><p>ز دهر مگذر اگرچه که دهر در گذرست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>برو به کام دل خو‌یش هر کجا خواهی</p></div>
<div class="m2"><p>که کردگار تو را یار و بخت راهبرست</p></div></div>