---
title: >-
    شمارهٔ ۳۱۷
---
# شمارهٔ ۳۱۷

<div class="b" id="bn1"><div class="m1"><p>نرگس ز نشاط ماه فروردین</p></div>
<div class="m2"><p>بر دست نهاد ساغر زرین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ابر آمد و کرد ساغرش پر می</p></div>
<div class="m2"><p>تا نوش کند به یاد فروردین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی‌آنکه شکسته گشت و پیچیده</p></div>
<div class="m2"><p>شد زلف بنفشه پرخم و پرچین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دستی که به زلف او درآویزد</p></div>
<div class="m2"><p>بی‌مشک شود چو نافه مشک‌آگین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تاکرد دم صباگلستان را</p></div>
<div class="m2"><p>از خوشی و خرمی بهشت آیین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گلبن به بهشت در همی نازد</p></div>
<div class="m2"><p>با جامهٔ سبز همچو حورالعین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر پروین شد در آسمان پنهان</p></div>
<div class="m2"><p>پروین صفت است در زمین نسرین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گویی که ز بهر خدمت خسرو</p></div>
<div class="m2"><p>آمد به زمین ز آسمان پروین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون فاخته باغ را دعا گوید</p></div>
<div class="m2"><p>طاووس دعاش را کند آمین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از بهر دعا ثنا کند بلبل</p></div>
<div class="m2"><p>بر ناصر دین بن معزالدین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سنجر که ز رای دولت آرایش</p></div>
<div class="m2"><p>دین را شرف است و ملک را تزیین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>والا ملکی که در صف هیجا</p></div>
<div class="m2"><p>دارد دل و زور صاحب صفین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ایزد چو ولایت خراسان را</p></div>
<div class="m2"><p>آراست به عدل او سنهٔ تِسعین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دادند به او سعادت کلی</p></div>
<div class="m2"><p>از برج شرف ستارگان همگین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در طالع او همی توان دیدن</p></div>
<div class="m2"><p>کز روم بود ولایتش تا چین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنجا که امید عدل او باشد</p></div>
<div class="m2"><p>بی‌بیم بود کبوتر از شاهین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وانجا که نهیب تیغ او باشد</p></div>
<div class="m2"><p>اندر غم جان بود تن تِنّبن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر مژدهٔ فتح او به هرکشور</p></div>
<div class="m2"><p>بندند و زنند کله و آذین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گردد ز نثار نامهٔ فتحش</p></div>
<div class="m2"><p>پرگوهر سرخ دست‌گوهر چین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر رای کند به آمل و ساری</p></div>
<div class="m2"><p>ور روی نهد به‌ کابل و غزنین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از بیم به دست هندو و دیلم</p></div>
<div class="m2"><p>بی‌بیم شود کَتاره و زوبین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بس دیر نماند تا نهد عزمش</p></div>
<div class="m2"><p>بر اسب غزای کافرستان زین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در روم کند رکاب سالارش</p></div>
<div class="m2"><p>زین را ز صلیب رومیان خرزین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یک حملهٔ سنجری زند برهم</p></div>
<div class="m2"><p>بتخانهٔ قیصری به قسطنطین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر افشین کرد فتنهٔ بابک</p></div>
<div class="m2"><p>در دولت و ملک معتصم تسکین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در لشکر خویشتن ملک‌سنجر</p></div>
<div class="m2"><p>دارد دو هزار بنده چون افشین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر بیژن گیو در هنر بودی</p></div>
<div class="m2"><p>چون حاجب او به روز بزم و کین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هنگام شکارکی روا گشتی</p></div>
<div class="m2"><p>بر بیژن گیو چاره‌گر گرین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای شاد به تو خلیفه و سلطان</p></div>
<div class="m2"><p>وز شادی هر دو دشمنان غمگین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از نصرت تو همی ببالد آن</p></div>
<div class="m2"><p>وز دولت تو همی بنازد این</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دو بیت شنیده‌ام دقیقی را</p></div>
<div class="m2"><p>در مدح تو هر دو کرده‌ام تضمین‌:</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>«استاد شهید زنده بایستی</p></div>
<div class="m2"><p>و آن شاعر تیره‌ چشم روشن بین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا شاه مرا مدیح‌گفتندی</p></div>
<div class="m2"><p>معنیش درست و لفظها شیرین‌»</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در شان تو آمدست پنداری</p></div>
<div class="m2"><p>واندر شأن حسود با نفرین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هفتم آیت ز سورهٔ یوسف</p></div>
<div class="m2"><p>پنجم آیت ز سورهٔ یاسین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تا با دل دشمنان به رزم اندر</p></div>
<div class="m2"><p>کین تو کند صناعت سِکین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هرکس‌ که ز کین تو خطر جوید</p></div>
<div class="m2"><p>سر در سر آن خطر کند مسکین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آباد بر آن‌ کُمیت میمونت</p></div>
<div class="m2"><p>کاو تیزتر است زآذر برزین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کو هست درنگ را چو گویی هان</p></div>
<div class="m2"><p>با دست شتاب را چو گویی هین</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هر گه که به پستی آید از بالا</p></div>
<div class="m2"><p>گویی به نشیب روی دارد هین</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>فرهاد نکرد نقش از آن بهتر</p></div>
<div class="m2"><p>شبدیز به جنب خسرو و شیرین</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تا پای تو در رکاب او باشد</p></div>
<div class="m2"><p>نعلش سر ماه را بود بالین</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شاها به بهار و موسم نیسان</p></div>
<div class="m2"><p>بر تخت شهی به‌کام دل بنشین</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نیک است و بد است مردم‌ گیتی</p></div>
<div class="m2"><p>بد را بگزای و نیک را بگزین</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>خوارزم شه آمد از لب جیحون</p></div>
<div class="m2"><p>زی درگه تو به حشمت و تمکین</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>تا رایت و رای او درین خدمت</p></div>
<div class="m2"><p>عالی شود از تو همچو علیین</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تا دانش و داد و دین او هر سه</p></div>
<div class="m2"><p>باقی شود از تو تا به یوم‌الدین</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>با دولت و فر تو بهر کشور</p></div>
<div class="m2"><p>کو قصد کند بگیرد اندر حین</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>از جانب غرب تا حد مکه</p></div>
<div class="m2"><p>از جانب شرق تا در ماچین</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بادا ز چهار چیز سازنده</p></div>
<div class="m2"><p>قسم تو چهار چیز با تحسین</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تا هست چهار طبع‌ گیتی را</p></div>
<div class="m2"><p>از آتش و از هوا و آب و طین</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>از چرخ عنایت از قضا یاری</p></div>
<div class="m2"><p>از بخت هدایت از خرد تلقین</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تشرین تو باد خوش‌تر از نیسان</p></div>
<div class="m2"><p>نیسان تو باد بهتر از تشرین</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ازگرد ولیت رفته برگردون</p></div>
<div class="m2"><p>وز سجن‌ عدوت رفته در سجین</p></div></div>