---
title: >-
    شمارهٔ ۱۸۵
---
# شمارهٔ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>گر چه آمد داستان خسرو شیرین به سر</p></div>
<div class="m2"><p>خسرو دیگر منم شیرینم آن شیرین پسر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من بسی در پیش آن شیرین پسر خدمت‌ کنم</p></div>
<div class="m2"><p>همچنان چون کرد خسرو خدمت شیرین به سر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا که دارد در جهان چون چشمهٔ آب حیات</p></div>
<div class="m2"><p>دارم از تیار او چون آذر برزین جگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از فروغ آتش دل وز سرشک آب چشم</p></div>
<div class="m2"><p>هر شبی در بسترم برق است و بر بالین مطر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر شود در عقدهٔ تنّین قمر هر چندگاه</p></div>
<div class="m2"><p>دارد آن بت سال و مه در عقدهٔ تنین قمر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بینی آن خطش که گویی مورچه بر دست و روی</p></div>
<div class="m2"><p>مشک و قیر اندوده عمدا کرد بر نسرین ‌گذر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در مه تشرین اگر رخساره بنماید به‌باغ</p></div>
<div class="m2"><p>آید اندر باغ ‌نسرین در مه ‌تشرین به‌ بر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هست بت‌گر بت بود نوشین به لب مشکین به زلف</p></div>
<div class="m2"><p>هست مه‌ گر مه بود سنگین به دل سیمین به بر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زلف او بر گل ز سنبل بست بر چینی عجب</p></div>
<div class="m2"><p>کس نبندد بر گل از سنبل چنان پرچین دگر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن ‌که در شطرنج تضعیفات بتواند شمرد</p></div>
<div class="m2"><p>گو بیا در زلف‌ او بند و شکنج ‌و چین ‌شُمَر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا فرستادند سوی چین ز رویش نسختی</p></div>
<div class="m2"><p>هیچ صورتگر دگر ننگارد اندر چین صور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حور عین است او و مجلس هست از او همچون بهشت</p></div>
<div class="m2"><p>اندرین‌گیتی بهشت و روی حورالعین نگر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آسمان او را ز جوزا گر کمر سازد رواست</p></div>
<div class="m2"><p>تا به خدمت بندد او پیش سدیدالدین کمر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صدر عثمان حلم بوبکر آن محمد کز شرف</p></div>
<div class="m2"><p>هست در امکان علی و هست در تمکین عمر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عالم آرایی‌ که با روح‌الامین هر ساعتی</p></div>
<div class="m2"><p>شکر عالی رای او گوید به علییّن پدر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از هنر آل ظهیری تا ابد مستظهرند</p></div>
<div class="m2"><p>کاو کند آل ظهیری را همی تلقین هنر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عاشق آیین او اندر فلک جان ملک</p></div>
<div class="m2"><p>کس نبیند درزمین هرگز بدین آیین بشر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون ببیند چشم‌ گیتی بین مبارک طلعتش</p></div>
<div class="m2"><p>فر او بفزاید اندر چشم‌ گیتی بین بصر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از حَجَر تا‌ثیر اقبالش گهر سازد همی</p></div>
<div class="m2"><p>هم بر آن‌گونه که سازد آفتاب از طین حجر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>قطره‌ای از آب دستش گر به آهن برچکد</p></div>
<div class="m2"><p>زاهن و پولاد بیرون آید اندر حین خضر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بر هر آن بقعت‌کجا خورشید عدلش تافته است</p></div>
<div class="m2"><p>سایه آرد بر سرکبکان همی شاهین به‌پر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر ز عزم او یکی معیار سازد روزگار</p></div>
<div class="m2"><p>کَفّتینش فتح و نصرت باشد و شاهین ظفر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ور به جنگ اعدای او سازند زوبین از شهاب</p></div>
<div class="m2"><p>سازد از ما دو هفته پیش آن زوبین سپر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای جوان بختی‌که از بهر دوام دولتت</p></div>
<div class="m2"><p>گه دعا گوید قضا و گه ‌کند آمین قدر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ماه شبه نعل و زین‌ گیرد به ماهی در دو بار</p></div>
<div class="m2"><p>تا از آن اسب تو را سازند نعل و زین کمر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در خراسان چون کند کلک همایونت صریر</p></div>
<div class="m2"><p>از صریر او بود در حضرت غزنین اثر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>محشری بینند پیش از مرگ بدخواهان تو</p></div>
<div class="m2"><p>چون تو برخیزی و انگیزی به روز کین حشر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خشم تو بر دشمنت حکم قضای ایزدست</p></div>
<div class="m2"><p>از قضا و حکم ایزد چون کند مسکین حذر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شرح این معنی چه‌ گویم من‌ که اینک پرشدست</p></div>
<div class="m2"><p>چشم دولت زین عجایب چشم ملت زین عِبَر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آفرین‌گویم تو را وَ اعدات را نفرین کنم</p></div>
<div class="m2"><p>زافرین بهتر ندانم چیز وز نفرین بتر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شاعری هست اندرین مجلس که اهل روزگار</p></div>
<div class="m2"><p>کرده‌اند اشعار او چون سوره ی یاسین ز بر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شعر او را من به نیکویی برابر کرده‌ام</p></div>
<div class="m2"><p>با عروس جلوگی کاو را بود کابین گهر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>او شکر خواند ز شیرینی همی شعر مرا</p></div>
<div class="m2"><p>گویی اندر لفظ و معنی‌کرده‌ام تضمین شکر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>یکدگر را هر دو در احسان تو تحسین‌کنیم</p></div>
<div class="m2"><p>نیست این احسان هَبا و نیست این تحسین هَدر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>این خبر باید که مداحان عالم بشنوند</p></div>
<div class="m2"><p>تا به شرق و غرب عالم بازگویند این خبر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تاکه درافشان و مشک‌افشان شود در بوستان</p></div>
<div class="m2"><p>هر بهاری از نسیم باد فروردین شجر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در هوای دولت تو باد دُرافشان سحاب</p></div>
<div class="m2"><p>بر درخت عزت تو باد مشک‌آگین ثمر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دوستان دولتت را باد در جنت مقام</p></div>
<div class="m2"><p>دشمنان عزتت را باد در سِجّین مقر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از نهیب تیغ تو وز موکب ترکان تو</p></div>
<div class="m2"><p>هم‌به‌ تانیسر نفیر و هم به‌ قسطنطین نفر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بر تو فرخ عید آن پیغمبری ‌کایزد بخواست</p></div>
<div class="m2"><p>بر تن فرزند او از ضربت سکین ضرر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از بلای چرخ‌گردان وز جفای روزگار</p></div>
<div class="m2"><p>مجلس تو اهل دین را تا به یوم‌الدین مفر</p></div></div>