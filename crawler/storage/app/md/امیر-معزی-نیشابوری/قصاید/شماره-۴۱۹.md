---
title: >-
    شمارهٔ ۴۱۹
---
# شمارهٔ ۴۱۹

<div class="b" id="bn1"><div class="m1"><p>نگاه کرد خدای اندر آسمان و زمین</p></div>
<div class="m2"><p>رقم‌ کشید ز قدرت بر آسمان و زمین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کیشدن رقم قدرتش پدید آورد</p></div>
<div class="m2"><p>هزارگونه عجایب در آسمان و زمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو یافتند عُلّو و سکون ز قدرت او</p></div>
<div class="m2"><p>چه در سرشت و چه در جوهر آسمان و زمین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه علو و سکون سر به سر رها کردند</p></div>
<div class="m2"><p>به ‌کدخدای ملک سنجر آسمان و زمین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نظام دین ‌که همی بر سرسش کنند نثار</p></div>
<div class="m2"><p>همه نجوم و همه گوهر آسمان و زمین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مظفر آنکه چو مهر و وفای او بینند</p></div>
<div class="m2"><p>خلاف و کین بنهند از سر آسمان و زمین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز نیک‌بختی او ملک و دین همی نازد</p></div>
<div class="m2"><p>چو از نبوت بیغمبر آسمان و زمین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کجا ثنا کند او را خطیب بر منبر</p></div>
<div class="m2"><p>ثنا کنند بر آن منبر آسمان و زمین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر بخواهد تا بر بدن بود زینت</p></div>
<div class="m2"><p>شوند پر زر و پر اختر آسمان و زمین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز بهر زینت ایوان بزم او شده‌اند</p></div>
<div class="m2"><p>مکان اختر و کان زر آسمان و زمین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز فرّ طلعت میمون و سعد طالع او</p></div>
<div class="m2"><p>شوند حاسد یکدیگر آسمان و زمین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیاورند صلاح و نظام عالم را</p></div>
<div class="m2"><p>چو آفتاب چنین داور آسمان و زمین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بپرورند به عدلش همی درختان را</p></div>
<div class="m2"><p>ز مهر چون پدر و مادر آسمان و زمین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز بهر عشرت او باغ را بیارایند</p></div>
<div class="m2"><p>به ‌گونه ‌گونه سلب زیور آسمان و زمین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر به صورت مرغی شود سعادت او</p></div>
<div class="m2"><p>چو دانه گیرد در ژاغر آسمان و زمین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وگر زهمت او چرخ چنبری سازد</p></div>
<div class="m2"><p>برون شوند بدان چنبر آسمان و زمین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ایا فریشتگان کاتب مدایح تو</p></div>
<div class="m2"><p>ستارگان قلم و دفتر آسمان و زمین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کسی که‌ گوید با قدر و حلم تو ز قیاس</p></div>
<div class="m2"><p>بوند همسر و همبر بر آسمان و زمین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>محال گیرد و چون ژرف بنگرد بیند</p></div>
<div class="m2"><p>به قدر و حلم تو در همسر آسمان و زمین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو در وفای تو تا حشر دهر محضر بست</p></div>
<div class="m2"><p>گوا شدند بر آن محضر آسمان و زمین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به روزگار تو از خلق باز داشته‌اند</p></div>
<div class="m2"><p>بلای صاعقه و صرصر آسمان و زمین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز دولت تو به ‌آذار و فرودین شده‌اند</p></div>
<div class="m2"><p>سرشک بار و شجرپرور آسمان و زمین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نقاب کُحلی و ادکن بر او فرو بندند</p></div>
<div class="m2"><p>ز دود تیره و خاکستر آسمان و زمین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کند سیاست تو روز رزم مرد تهی</p></div>
<div class="m2"><p>هم از روان و هم از پیکر آسمان و زمین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به‌ گاه خشم تو گر روی عفو ننمودی</p></div>
<div class="m2"><p>بسوختی ز تف آذر آسمان و زمین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کجا بسوزد خشم تو بدسگالان را</p></div>
<div class="m2"><p>شوند پرشرر و اخگر آسمان و زمین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شکافته است و گسسته سرای و کاخ عدوت</p></div>
<div class="m2"><p>بر آن صفت‌ که ‌گه محشر آسمان و زمین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو سوگواران هر شب به سوگ دشمن تو</p></div>
<div class="m2"><p>شوند در شبه‌گون چادر آسمان و زمین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>فرشته است مگر لشکر تو روز مصاف</p></div>
<div class="m2"><p>مدد کنندهٔ آن لشکر آسمان و زمین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به قهر خصم تو کردند کارهای عجیب</p></div>
<div class="m2"><p>چو مهره باز و چو بازیگر آسمان و زمین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به کشوری که بود دشمنت خراب کنند</p></div>
<div class="m2"><p>خم و ثبات در آن‌ کشور آسمان و زمین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چه مهره بود و چه لعبت ‌که داشتند آن روز</p></div>
<div class="m2"><p>گرفته در دهن و در بر آسمان و زمین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همیشه لشکر کین تو نیلگون دارد</p></div>
<div class="m2"><p>چو لون خویش و چو نیلوفر آسمان و زمین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر آسمان و زمین برزند مخالف تو</p></div>
<div class="m2"><p>زنند بر سر او خنجر آسمان و زمین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همیشه تا که ز نور و ظَلَم زنند علم</p></div>
<div class="m2"><p>به حد باختر و خاور آسمان و زمین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همیشه تا که نمایند از فراز و نشیب</p></div>
<div class="m2"><p>چو بحر اَخضر و چون لنکر آسمان و زمین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تو باش صد‌ر و خداوند جاه قدر تو را</p></div>
<div class="m2"><p>مطیع چون رهی و چاکر آسمان و زمین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بر آسمان و زمین رای و رایت تو بلند</p></div>
<div class="m2"><p>تو را مُسَخّر و فرمان بر آسمان و زمین</p></div></div>