---
title: >-
    شمارهٔ ۱۶۳
---
# شمارهٔ ۱۶۳

<div class="b" id="bn1"><div class="m1"><p>فرخنده باد و میمون این مجلس منور</p></div>
<div class="m2"><p>بر شهریار گیتی شاهنشه مظفر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاهی کجا رسیدست از همت بلندش</p></div>
<div class="m2"><p>تختش به هفت گردون عدلش به هفت کشور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اَسْلاف را به عدلش جاه است تا به آدم</p></div>
<div class="m2"><p>اَعقاب را به جاهش فخرست تا به محشر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ابرست دست رادش بحرست طبع پاکش</p></div>
<div class="m2"><p>زان ابر قطره بدره زان بحر موج‌ گوهر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خسروی و شاهی مانند او که باشد</p></div>
<div class="m2"><p>هر خانه نیست‌ کعبه هر چشمه نیست‌ کوثر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دو برون نبینم شاهان و خسروان را</p></div>
<div class="m2"><p>یا سر به نام او بر یا نامهاش بر سر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاه است و ملک و لشکر هر سه به‌هم موافق</p></div>
<div class="m2"><p>مقهور شد مخالف زین شاه و ملک و لشکر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میران نامدارند این بندگان سلطان</p></div>
<div class="m2"><p>هریک چو حاتم طَیّ هریک چو رستم زر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یک بنده‌گاه بخشش با همّت فریدون</p></div>
<div class="m2"><p>یک بنده‌گاه کوشش با نصرت سکندر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وان میزبان زیبا در پیش تخت خسرو</p></div>
<div class="m2"><p>بسته میان به خدمت چون بندگان‌ دیگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>امروز بر شهنشه رحمت همی فشاند</p></div>
<div class="m2"><p>هم از بهشت رضوان هم از سپهر اختر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شاید که میر قیصر سر بر فلک فرازد</p></div>
<div class="m2"><p>زیرا که هست سنجر مهمانِ میرِ قیصر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زین شاه بنده‌ پرور شادی است بندگان‌ را</p></div>
<div class="m2"><p>تا جاودان بماناد این شاه بنده پرور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گردون به جهد و طاعت پیمانش را متابع</p></div>
<div class="m2"><p>گیتی به طُوع و رغبت فرمانْشْ را مسخر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تختش قرین شاهی بختش ندیم شادی</p></div>
<div class="m2"><p>سالش ز سال بهتر روزش ز روز خوشتر</p></div></div>