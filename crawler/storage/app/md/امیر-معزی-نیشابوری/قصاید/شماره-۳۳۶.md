---
title: >-
    شمارهٔ ۳۳۶
---
# شمارهٔ ۳۳۶

<div class="b" id="bn1"><div class="m1"><p>دو گوهرند سزاوار مجلس و میدان</p></div>
<div class="m2"><p>که فخر مجلس و میدان بود به این و به ان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی به آب لطیف آمده پدید از خاک</p></div>
<div class="m2"><p>یکی به آتش تیز آمده برون از کان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی رسیده به‌ شربت ز زخم و ز چرخشت</p></div>
<div class="m2"><p>یکی رسیده به ضربت ز سنگ و زسوهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی نه عقل و همه میل او بود سوی عقل</p></div>
<div class="m2"><p>یکی نه جان و همه قصد او بود سوی جان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی نشاط جوانی دهد به مردم پیر</p></div>
<div class="m2"><p>یکی هزیمت بیری دهد به مرد جوان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی ترازوی عقل است و کیمیای نظر</p></div>
<div class="m2"><p>یکی طلایه مرگ است و اژدهای روان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی دهد به‌گه رامش‌ از صبوح خبر</p></div>
<div class="m2"><p>یکی دهد به‌گه کوشش از فتوح نشان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی به جام بلور اندر از لطافت و نور</p></div>
<div class="m2"><p>چو آتشی است به آب اندرون گرفته مکان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی زگوهر رخشان و لون و پیکر خویش</p></div>
<div class="m2"><p>چو آینه است و درو عکس کوکب رخشان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکی به غایت سرخی فروخته ز قدح</p></div>
<div class="m2"><p>چنان‌کجا زسمن برگ لالهٔ نعمان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکی کبود و نماینده گوهر از تن خویش</p></div>
<div class="m2"><p>چو بر بنفشه پراکنده قطرهٔ باران</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به روز بزم یکی مایه گیرد از ناهید</p></div>
<div class="m2"><p>به روز رزم یکی توشه خواهد ازکیوان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سزد کزین دو گهر بزم و رزم فخر کنند</p></div>
<div class="m2"><p>که قدر هر دو بیفزود دست شاه جهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جلال ملک ملکشاه‌ کز جلالت او</p></div>
<div class="m2"><p>شرف‌ گرفت زمین و خطر گرفت زمان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یقین شدست همه خلق را به شرق و به غرب</p></div>
<div class="m2"><p>که آفتاب ملوک است و سایهٔ یزدان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گریختن نتواند کسی ز طاعت او</p></div>
<div class="m2"><p>زآفتاب وز سایه‌گریختن نتوان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگرکسی ز خلافش زیادتی طلبد</p></div>
<div class="m2"><p>همه زیادت آن‌ کس خدا کند نقصان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زمانه را بدو قوت همی نگه دارد</p></div>
<div class="m2"><p>به قوت سر شمشیر و قوت فرمان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به جنب عزمشْ عزم ملوک سست شود</p></div>
<div class="m2"><p>چنانکه سست شود سِحْر در بر قرآن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>صف سپاه و تَفِ خنجرش پدید آرند</p></div>
<div class="m2"><p>به زمهریر و دی اندر سموم و تابستان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو در حَضَر بود او پرطرب بود گیتی</p></div>
<div class="m2"><p>چو در سفر بود او پرظفر بود کیهان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سریر او به حَضَر با طرب کند بیعت</p></div>
<div class="m2"><p>حسام او به سفر با ظفر کند پیمان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ببین رکاب و عنانش چو کرد عزم سفر</p></div>
<div class="m2"><p>اگر هوای سبک‌ خواهی و زمین گران</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که پای دارد با او چو پای زد به رکاب</p></div>
<div class="m2"><p>که دست ساید با او چو دست زد به عنان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به شام رفت وز تیغش به روم بود خروش</p></div>
<div class="m2"><p>به روم رفت و ز سهمش به مصر بود فغان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو روم و شام‌ کند هند و ترک سال دگر</p></div>
<div class="m2"><p>اگر شود سوی هندوستان و ترکستان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ایا شهی‌ که به عدل تو بس عجب نبود</p></div>
<div class="m2"><p>اگر به آبخور آیند غُرْم و شیر ژیان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شه زمانی و سلطان روزگاری تو</p></div>
<div class="m2"><p>که خوار شد به تو کفر و عزیز شد ایمان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به‌هیچ معنی واجب نگردد استغفار</p></div>
<div class="m2"><p>بر آن‌ کسی‌ که تو را شاه خواند و سلطان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو آن شهی‌که همیشه تو را به داد و دهش</p></div>
<div class="m2"><p>همی درود فرستد روان نوشِرْوان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تو آن شهی‌که فلک تا تورا همی بیند</p></div>
<div class="m2"><p>نگردد و نکند بی‌مراد تو دوران</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تو آن شهی‌که به یک برج در دو کوکب را</p></div>
<div class="m2"><p>نبود و هم نبود بی‌سعادت تو قران</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بس است نامه و نام تو ملک را حجت</p></div>
<div class="m2"><p>بس است رایت و رای تو فتح را برهان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به آب جود تو از خاره بردمد نسرین</p></div>
<div class="m2"><p>به باد عدل تو از شوره بشکفد ریحان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شود زکین تو بسیار دشمنان اندک</p></div>
<div class="m2"><p>شود ز مهر تو دشوار دوستان آسان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به شرق و غرب اگر دشمنی بود به مثل</p></div>
<div class="m2"><p>که در عداوت تو تیر برنهد به کمان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>عجب نباشد اگر باز پس رود تیرش</p></div>
<div class="m2"><p>کند زمانه ز سوفار تیر او پیکان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خدایگانا در شکر و در پرستش تو</p></div>
<div class="m2"><p>قضاگشاده زبان است و بخت بسته میان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>توراست هرچه در اسلام هست باقیمت</p></div>
<div class="m2"><p>توراست هرچه در آفاق هست آبادان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>زتیغ تیز تویی خصم‌ ‌بند و شهرگشای</p></div>
<div class="m2"><p>به دست راد تویی مالْ‌بخش و شهرْ‌ستان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز خون دشمن کردی عقیق رنگ حُسام</p></div>
<div class="m2"><p>عقیق رنگ کن اکنون قدح زخون زران</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو رزم راندی برکام خویشتن یک چند</p></div>
<div class="m2"><p>کنون به بزم دمی چند کام خویش بران</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به شادکامی و نیک‌اختری و بهروزی</p></div>
<div class="m2"><p>چنانکه خواهی و چندانکه آرزوست بمان</p></div></div>