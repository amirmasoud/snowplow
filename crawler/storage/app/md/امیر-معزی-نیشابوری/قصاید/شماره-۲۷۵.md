---
title: >-
    شمارهٔ ۲۷۵
---
# شمارهٔ ۲۷۵

<div class="b" id="bn1"><div class="m1"><p>نشاط باد همه روزگار فخرالملک</p></div>
<div class="m2"><p>بهار باد همه روزگار فخرالملک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان چنانکه ز خورشد بشکفد بشکفت</p></div>
<div class="m2"><p>ز فر طلعت خورشیدوار فخرالملک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز چرخ تا که نبرد شمار هندسیان</p></div>
<div class="m2"><p>ز بخت و عمر نبرد شمار فخرالملک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر این جهان همه ایزد بدو دهد شاید</p></div>
<div class="m2"><p>که هست برتر ازین انتظار فخرالملک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به‌سان ذرّه نماید به وقت قدرت و قدر</p></div>
<div class="m2"><p>سپهر پیش دل کامکار فخرالملک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رخ مخالف دولت به رنگ دینارست</p></div>
<div class="m2"><p>ز غیرت کف دینا‌رْ بار فخرالملک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به مهر و کین زحل و مشتری همی سازند</p></div>
<div class="m2"><p>به رزم و بزم همه ساله کار فخرالملک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو مشتری به شرفخانه در رسد خواهد</p></div>
<div class="m2"><p>که اوفتد ز فلک در کنار فخرالملک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنانکه طبع بشر هست خواستار ملوک</p></div>
<div class="m2"><p>همیشه هست خرد خواستار فخرالملک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنانکه هست هنر اختیار دولت و دین</p></div>
<div class="m2"><p>شدست دولت و دین اختیار فخرالملک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>امید خلق جهان هست در بزرگی و جاه</p></div>
<div class="m2"><p>به قدر و مرتبه و افتخار فخرالملک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خدای جَلّ جَلاله نهاد پنداری</p></div>
<div class="m2"><p>قرار خلق جهان در قرار فخرالملک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ضمیر خلق همی داند ای عجب‌ گویی</p></div>
<div class="m2"><p>نهان غیب شده است آشکار فخرالملک</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به زینهار خدای اندرون بود شب و روز</p></div>
<div class="m2"><p>کسی که باشد در زینهار فخرالملک</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شعار دانش و معنی درست کرد همی</p></div>
<div class="m2"><p>که خواند شعر من اندر شعار فخرالملک</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به حکم بندگی از دیرباز هست دلم</p></div>
<div class="m2"><p>به دام شکر و ثنا در شکار فخرالملک</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به حکم دوستی امروز اگر بسنده بود</p></div>
<div class="m2"><p>رضا دهم که کنم جان‌ نثار فخرالملک</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همیشه ‌تا که جهان یادگار آدمی است</p></div>
<div class="m2"><p>بباد ملک جهان یادگار فخرالملک</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عنایت ازلی بود جفت فخرالملک</p></div>
<div class="m2"><p>سعادت ابدی باد یار فخرالملک</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بهار و عید بهم حاضرند و فرخ باد</p></div>
<div class="m2"><p>به شادمانی عید و بهار فخرالملک</p></div></div>