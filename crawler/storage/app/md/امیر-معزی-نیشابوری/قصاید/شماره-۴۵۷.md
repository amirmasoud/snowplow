---
title: >-
    شمارهٔ ۴۵۷
---
# شمارهٔ ۴۵۷

<div class="b" id="bn1"><div class="m1"><p>مخوان فسانهٔ افراسیاب تورانی</p></div>
<div class="m2"><p>مگوی قصهٔ اسفندیار ایرانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخن زخسرو و سلطان هفت کشورگوی</p></div>
<div class="m2"><p>که ختم‌گشت بدو خسروی و سلطانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معز دین خدای و خدایگان جهان</p></div>
<div class="m2"><p>که تا جهان بود او را سزد جهانبانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ستوده سنجر سلطان نشان که هست او را</p></div>
<div class="m2"><p>دل سکندری و دولتِ سلیمانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شهی‌که بر در غزنین به یک زمان بگرفت</p></div>
<div class="m2"><p>همه ولایت شاهان زاولستانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شهی‌کز او شه غزنین و خان ترکستان</p></div>
<div class="m2"><p>نشسته‌اند به سلطانی و به خاقانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به هر چه رای‌کند رایتش بود منصور</p></div>
<div class="m2"><p>زهی سعادت و تأیید و فرّ یزدانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غبار موکب او را همی برند نماز</p></div>
<div class="m2"><p>برآسمان بلند اختران نورانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خدای عزوجل چون بر آسمان و زمین</p></div>
<div class="m2"><p>بیافرید چه روحانی و چه جسمانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هِمال او دگری درکمال عقل و هنر</p></div>
<div class="m2"><p>نیافرید نه جسمانی و نه روحانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کجا سعادت و اقبال او پدید آید</p></div>
<div class="m2"><p>شود جلالت و فرّ ملوک پنهانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو آفتاب درخشان شود زچرخ بلند</p></div>
<div class="m2"><p>مه چهارده را کی بود دُرافشانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لقای اوست نشاطِ دلِ مسلمانان</p></div>
<div class="m2"><p>که روشن است به او دیدهٔ مسلمانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر آن زمین‌که جهد باد عدل و انصافش</p></div>
<div class="m2"><p>ز شیر شیر خورد آهوی بیابانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نسیم دولت او چشم ملک روشن‌کرد</p></div>
<div class="m2"><p>چو بوی یوسف چشم رسول کنعانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سپرد زیر قدم تخت وگاه محمودی</p></div>
<div class="m2"><p>گرفت زیر علم مال و ملک سامانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عراق را فزع است از نهیب و هیبت آنک</p></div>
<div class="m2"><p>سوی عراق کشد لشکرِ خراسانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نماند دیر که فغفور چین و قیصر روم</p></div>
<div class="m2"><p>کنند بر در او حاجبی و دربانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ایا مدیح تو سرمایهٔ سخندانان</p></div>
<div class="m2"><p>و یا فتوح تو پیرایهٔ سخندانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کدام اشاه سر از خط‌کشیدا و کین تو جست</p></div>
<div class="m2"><p>که خیره سر نشد از عاجزی و حیرانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نهال کین تو در هر دلی‌ که کِشته شود</p></div>
<div class="m2"><p>به عاقبت ندهد بار جز پشیمانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دلیل نصر تو بس بر شکستن سه مصاف</p></div>
<div class="m2"><p>امیر دادی و عرنیجی و قدرخانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سنان نیزهٔ تو روز رزم کرد روان</p></div>
<div class="m2"><p>زخون چشم بداندیش چشمه و خانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر آن نفرکه تورا بنده و رهی نشدند</p></div>
<div class="m2"><p>به‌زیر بند تو بندی شدند و زندانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به تیغ و بازو یک نیمه بستدی زجهان</p></div>
<div class="m2"><p>به عز بخت دگر نیمه نیز بستانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جهان سیاه کنی بر عدو چوکان شبه</p></div>
<div class="m2"><p>بدان تکاور شبرنگ صبح پیشانی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هر آن‌کسی‌که سر از حکم تو بگرداند</p></div>
<div class="m2"><p>بر آب دیدهٔ او آسیا بگردانی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شکار کردن و رزم است و بزم کار شهان</p></div>
<div class="m2"><p>تو آن شهی‌که به یک روز هر سه بتوانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زملک پادشهی را سبک برانگیزی</p></div>
<div class="m2"><p>به جای او دگری را به ملک بنشانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اگر یکی را ثانی بود ز مخلوقات</p></div>
<div class="m2"><p>تویی یکی که تو را نیست در جهان ثانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نشاط کن که ز بهر نشاط کردن تو</p></div>
<div class="m2"><p>به سان عالم باقی است عالم فانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چنانکه بود زکینت‌ گرفته جانِ عدو</p></div>
<div class="m2"><p>کآشفته بود چمنها زباد آبانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کنون چنانکه زمهرت شکفته‌جان ولی</p></div>
<div class="m2"><p>شکفته گبببت گلستان زباد نیسانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو آسمان به زمین جامهٔ بهاری داد</p></div>
<div class="m2"><p>هوا ازو بِسَتَد جامه ی زمستانی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جمال خویش چمن را به عاریت دادند</p></div>
<div class="m2"><p>بتانِ خَلُّخی و لعبتان کاشانی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>زنند نعره همی‌کبک و فاخته همه شب</p></div>
<div class="m2"><p>ز عشق لالهٔ کوهی و سرو بستانی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دهان لاله چو از ژاله پر شود گویی</p></div>
<div class="m2"><p>که در عقیق یمانی است درّ عمانی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همی شود چمن باغ پرگل و ریحان</p></div>
<div class="m2"><p>بخواه بر گل و ریحان شراب ریحانی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>زحدگذشته همی ابرگوهر افشاند</p></div>
<div class="m2"><p>مگر ز جود تو آموخت گوهر افشانی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>زبهر جود تو در آب و سنگ صنع خدای</p></div>
<div class="m2"><p>نهاد لؤلؤ بحری و گوهر کانی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>قرین هرکرمت نعمتی است قارونی</p></div>
<div class="m2"><p>به زیر هر سخنت حکمتی است لقمانی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به خاک پای حکیمان تو سرافرازد</p></div>
<div class="m2"><p>اگر ز خاک برآید حکیم یونانی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چنانکه بنده معزّی به جان ثناگر توست</p></div>
<div class="m2"><p>دعاگرست تورا جان بنده برهانی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>همی ز طبع و دل بنده خوشتر آید شعر</p></div>
<div class="m2"><p>به آن صفت که گلاب ازگل سپاهانی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو در فتوح تو دیوان او رسید به چرخ</p></div>
<div class="m2"><p>چرا بدو نرسیدست مال دیوانی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>همی ز فتح تو سازد یکی بنای سخن</p></div>
<div class="m2"><p>که در زمانه نسازد چنو بنا بانی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>اگر بناها ویران شود ز ابر بهار</p></div>
<div class="m2"><p>بنای فتح تو ایمن بود ز ویرانی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>اگر بماند تا جاودان کسی به جهان</p></div>
<div class="m2"><p>تو را سزاست که تا جاودان همی مانی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به بزم جامهٔ لهو و طرب همی پوشی</p></div>
<div class="m2"><p>به رزم نامهٔ فتح و ظفر همی خوانی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>سپه همی‌کشی و مملکت همی‌گیری</p></div>
<div class="m2"><p>جهان همی خوری و کام دل همی رانی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چهار چیز به‌ گیتی نصیب عمر تو باد</p></div>
<div class="m2"><p>خوشی و خرمی و شادی و تن‌آسانی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ز ملک و دولت و شاهی تو باش برخوردار</p></div>
<div class="m2"><p>که هر سه از همه شاهان توراست ارزانی</p></div></div>