---
title: >-
    شمارهٔ ۳۴۱
---
# شمارهٔ ۳۴۱

<div class="b" id="bn1"><div class="m1"><p>هر آن عاقل‌که او بندد دل اندر طاعت یزدان</p></div>
<div class="m2"><p>نشایدکاو نپیوندد دل اندر خدمت سلطان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سلامت باد آنکس ‌کاو بهم پیوندد و بندد</p></div>
<div class="m2"><p>تن اندر خدمت سلطان دل اندر طاعت یزدان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه شاهان همی نیک‌اختری جویند از این خدمت</p></div>
<div class="m2"><p>چه ‌در مشرق چه درمغرب چه در توران چه در ایران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنین سلطان نبودست و نخواهد بود در عالم</p></div>
<div class="m2"><p>جمال دودهٔ جُغری پناه دودهٔ خاقان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همی فرمان برند او را دو فرمانبر به‌دوکشور</p></div>
<div class="m2"><p>یکی فرمانده غزنین یکی فرمانده توران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی‌را برمراد او سر اندر چنبر طاعت</p></div>
<div class="m2"><p>یکی را بر وفای او دل اندر عهدهٔ پیمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنین فرمان ز سلطانان کرا بودست در عالم</p></div>
<div class="m2"><p>چنین دولت ز جباران کرا بودست درکیهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر آن‌کس کاو ازین فرمان و این دولت خبر داد</p></div>
<div class="m2"><p>تعجب را همی‌گوید زهی دولت زهی فرمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کجا خشمش رسد تیمارگیرد خانه‌ها یک سر</p></div>
<div class="m2"><p>کجا سهمش رسد دشوارگردد کارها یکسان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ولیکن چون کند رحمت زمهر و عفو اوگردد</p></div>
<div class="m2"><p>همه تیمارها شادی همه دشوارها آسان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه‌بیگانگان را عفو و احسان‌است از این خسرو</p></div>
<div class="m2"><p>برادر باشد اولی‌تر به این عفو و به‌ این احسان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو راضی گشت از او سلطان و راضی شد به‌ این خدمت</p></div>
<div class="m2"><p>سلامت یافت تا محشر سعادت یافت جاویدان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جه بهتر زانکه نزدیک چنین شاهی چنین میری</p></div>
<div class="m2"><p>نماید مدتی شاهی و باشد مدتی مهمان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چه خوش‌تر زانکه از یک اصل باشد تازه و خرم</p></div>
<div class="m2"><p>دو گلبن در یکی باغ و دو سرو اندر یکی بستان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز شادُروان این حضرت یکی بوی بهشت آید</p></div>
<div class="m2"><p>کسی باید که بنشیند بر این فرخنده شادروان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کرا باشد شکیبایی ز دیدار چنین شاهی</p></div>
<div class="m2"><p>که از دیدار او در تن همی شادی فزاید جان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چنین‌ گفته است پیغمبر که دیدار شه عالم</p></div>
<div class="m2"><p>بود طاعت وزان طاعت فزاید قُوَّت ایمان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به ملک اندر چنین باید شه عادل که از عدلش</p></div>
<div class="m2"><p>همی در عالم عِلْوی بنازد جان نوشِرْوان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به هر روزی که نو گردد ببخشد کشور دیگر</p></div>
<div class="m2"><p>نه جودش را بود غایت نه ملکش را بود پایان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دراین معنی‌که من‌گفتم چو خورشیدست پنداری</p></div>
<div class="m2"><p>که بخشد نور و هرگز ناید اندر نور او نقصان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دو ابرند ای عجب‌گویی‌کف و تیغ جهانگیرش</p></div>
<div class="m2"><p>یکی در بزم زرافشان یکی در رزم خون‌افشان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یکی اندر سبب چون معجز عیسیّ‌بِنْ مریم</p></div>
<div class="m2"><p>یکی اندر صفت چون معجز موسی بن‌ عمران</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شهنشاها جهاندارا به‌ میدان فتوح اندر</p></div>
<div class="m2"><p>تو داری گوی پیروزی گرفته در خم چوگان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر آن چیزی‌که از اقبال موجودست ‌در عالم</p></div>
<div class="m2"><p>به معنی چون یکی نامه است و نام تو بر او عنوان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همیشه تاز تقدیر و قضای خالق‌اکبر</p></div>
<div class="m2"><p>همی باشد زمین ساکن همی باشد فلک‌گردان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زمین را بی‌رضای تو مبادا ساعتی تسکین</p></div>
<div class="m2"><p>فلک را بی‌مراد تو مبادا لحظه‌ای دَ‌وْران</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نظام دین تازی را همیشه عزم تو حجت</p></div>
<div class="m2"><p>صلاح ملک باقی را همیشه تیغ تو برهان</p></div></div>