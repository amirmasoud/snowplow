---
title: >-
    شمارهٔ ۳۱۲
---
# شمارهٔ ۳۱۲

<div class="b" id="bn1"><div class="m1"><p>دوش با سیمین صنوبر در نهان سر داشتم</p></div>
<div class="m2"><p>ای خوش آن عیشی که با سیمین صنوبر داشتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در برمن بود تا روز آن نگار نوش لب</p></div>
<div class="m2"><p>داد خود تا روز از آن نوشین لبان برداشتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زیبد ار با ماه تابان برزنم زیرا که دوش</p></div>
<div class="m2"><p>ماه و مشک و سرو سیمین هر سه در بر داشتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این تن مسکین ز جان خویشتن برداشت دل</p></div>
<div class="m2"><p>چون من اندر بر چنان مه روی دلبر داشتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یارگفتا داشتی چون من نگاری‌گفمش</p></div>
<div class="m2"><p>کافرم‌ گر من به جز تو یار دیگر داشتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صابری فرمودم آن دلدار اندر عشق خود</p></div>
<div class="m2"><p>صابری بر باد دادم سر به‌سر گر داشتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون ز عنبر چنبرش دیدم به‌گرد آفتاب</p></div>
<div class="m2"><p>تا سحر در گردنش دو دست چنبر داشتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نرگس وگلنار و سرو و ماه و یار سیمگون</p></div>
<div class="m2"><p>تا سحر هر پنج بر بالین و بستر داشتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوهر آگین شَکِّر خندان چو بگشادی به‌ناز</p></div>
<div class="m2"><p>دامنی پرشکر خندان و گوهر داشتم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شَکَّرش خواری فزود و گوهرش راحت نمود</p></div>
<div class="m2"><p>کردم از گوهر گله گر شُکر شَکَّر داشتم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ماه بر گردون بود سرو سهی در بوستان</p></div>
<div class="m2"><p>من به یک دم هر دو اندر زیر چادر داشتم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گاه بنشستی و دادی ساغر و گه بوس و من</p></div>
<div class="m2"><p>گاه اندر دست زلفش گاه ساغر داشتم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هیچکس در حوض ‌کوثر شَکَّر و پروین نداشت</p></div>
<div class="m2"><p>شکر و پروین من اندر حوض کوثر داشتم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر بر من بر نهاد آن لُعبتِ شیرین زبان</p></div>
<div class="m2"><p>من ز شیرینی ورا با جان برابر داشتم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گه مکابروار بوسه‌ گاه بوس و گه کنار</p></div>
<div class="m2"><p>گه بدو آهنگ از این معنی مکابر داشتم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ماه پیکر سیم ساقی بود ساقی دوش و من</p></div>
<div class="m2"><p>چشم سوی سیم ساق و ماه پیکر داشتم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر زمان از ماه خندان چون مرا دادی شراب</p></div>
<div class="m2"><p>پیکر از شادیش ‌گفتی بر دو پیکر داشتم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفتم ای مه در برم تا بامداد آرام‌گیر</p></div>
<div class="m2"><p>گوش سوی پاسخ یار ستمگر داشتم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گفت هستم تاگه الله اکبر در برت</p></div>
<div class="m2"><p>کردم انکار و چنان کردار منکر داشتم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون مؤذن برکشید الله‌اکبر ناگهان</p></div>
<div class="m2"><p>دست بر گردون از آن الله‌اکبر داشتم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون شنید آهنگ رفتن کرد از آن‌گفتار خام</p></div>
<div class="m2"><p>من از آن الله‌اکبر مرگ خوشتر داشتم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ناگهان بربست مَعجَر گرد ماه دلفریب</p></div>
<div class="m2"><p>ماه برگردون بُد و من زیر مَعجَر داشتم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شوخ‌وار آن کافر از پیشم برون شدگفتمش</p></div>
<div class="m2"><p>شوخ‌چشمی و نه این چشم از تو کافر داشتم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سر بگردانید و پای از حجره چون بیرون نهاد</p></div>
<div class="m2"><p>پای او را بوسه دادم دست بر سر داشتم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گفتم ای دلبر چو بودم زر نکردی با من این</p></div>
<div class="m2"><p>ای دریغا کار چون زر بود چون زر داشتم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اندر آنجا داشتم من زر ز بهرروی تو</p></div>
<div class="m2"><p>گرچه آنجا شغل شاه دادگستر داشتم</p></div></div>