---
title: >-
    شمارهٔ ۴۴۰
---
# شمارهٔ ۴۴۰

<div class="b" id="bn1"><div class="m1"><p>شهریارا بر سر دولت نثاری کرده‌ای</p></div>
<div class="m2"><p>در بهار از شادی و رامش بهاری کرده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما شنیدیم از بزرگان قصهٔ هر روزگار</p></div>
<div class="m2"><p>روزگار ما به از هر روزگاری کرده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جسته‌ای شکر خدای و کرده‌ای دین را عزیز</p></div>
<div class="m2"><p>نیک‌نامی جسته‌ای شایسته کاری کرده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پادشاهان پیش ازین گر رسم نیکو داشتند</p></div>
<div class="m2"><p>تو ز رسم پادشاهان اختیاری کرده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در جهانداری حصار از سنگ و آهن ساختند</p></div>
<div class="m2"><p>تو ز بخت و عدل و دینداری حصاری‌ کرده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا تو را دادست یزدان هیبت و فرّ علی</p></div>
<div class="m2"><p>تیغ گوهردار را چون ذوالفقاری کرده‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کی توان خواندن تو را چون رستم و اسفندیار</p></div>
<div class="m2"><p>تا تو بر تخت شهنشاهی قراری کرده‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بینم اندر لشکر تو صدهزاران شیر نر</p></div>
<div class="m2"><p>هر یکی را رستم و اسفندیاری کرده‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در همه‌ کاری تو را میمون و فرخنده است فال</p></div>
<div class="m2"><p>لاجرم فرخنده و میمون شکاری کرده‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون سپهری کرده‌ای خاک زمین از نعل اسب</p></div>
<div class="m2"><p>وز سپاهت دشت را چون کوهساری کرده‌ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من چنان دانم همی‌ کز خون نخجیر حلال</p></div>
<div class="m2"><p>کوهسار و دشت را چون لاله‌زاری کرده‌ای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای خداوندی که بخت توست بر گردون سوار</p></div>
<div class="m2"><p>بنده را بر مرکب دولت سواری کرده‌ای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گوش دولت بشنود چون من ثنا گویم تو را</p></div>
<div class="m2"><p>کز هنر درگوش دولت گوشواری کرده‌ای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا جهان باشد بمان در زینهار کردگار</p></div>
<div class="m2"><p>زانکه در گیتی به شاهی زینهاری کرده‌ای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با سعادت باشیا هرجا که باشی نیکبخت</p></div>
<div class="m2"><p>کز سعادت بخت را آموزگاری کرده‌ای</p></div></div>