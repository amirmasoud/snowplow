---
title: >-
    شمارهٔ ۱۸۸
---
# شمارهٔ ۱۸۸

<div class="b" id="bn1"><div class="m1"><p>صبوح مرا خوش کن ای خوش پسر</p></div>
<div class="m2"><p>به میخوارگان ده قدح تا به سر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که چون سر برآرد ز کوه آفتاب</p></div>
<div class="m2"><p>قدح تا به سر خوشتر ای خوش پسر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه از آب رز شد زمین بهره‌مند</p></div>
<div class="m2"><p>تو از آب رز کن مرا بهره‌ور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو را چشم بادام و لب شکر است</p></div>
<div class="m2"><p>مرا نقل بادام ده با شکر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کمندی‌ که با سیم داری به مشک</p></div>
<div class="m2"><p>کمان ‌کرد پشت من ای سیمبر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز خوبان گیتی تو را معجزه است</p></div>
<div class="m2"><p>دهان و سخن با میان و کمر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمر ظاهرست و میان ناپدید</p></div>
<div class="m2"><p>سخن‌ کامل است و دهان مختصر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو تیر افکند چشمت از ساحری</p></div>
<div class="m2"><p>بود بر دلم تیر او کارگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیاساید از ساحری تیر اوی</p></div>
<div class="m2"><p>چو تیغ شجاع الملوک الظفر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>معینی کزو یافت دولت شرف</p></div>
<div class="m2"><p>نصیری کزو یافت ملت خطر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حُسامی که در ملت ایران زمین</p></div>
<div class="m2"><p>بدو تازه شد دین خیرالبشر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سماعیل‌ بن گیلکی کز شرف</p></div>
<div class="m2"><p>کفش زمزم است و رکابش حجر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به اندیشه نتوان به قدرش رسید</p></div>
<div class="m2"><p>که اندیشه زیر است و قدرش زبر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قضا و قدر حاصل آید ازو</p></div>
<div class="m2"><p>هرآنچ اندر آرد به فهم و فکر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر چه ندارد روا دین اوی</p></div>
<div class="m2"><p>که باشد ثناهای او چون سور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ثناهای او اهل دین کرده‌اند</p></div>
<div class="m2"><p>چو الحمد و چون قل هو الله ز بر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بیابان چنان کرد شمشیر او</p></div>
<div class="m2"><p>که چون آهوی ماده شد شیر نر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به راه بیابان پیاپی شدست</p></div>
<div class="m2"><p>ز بازارگانان نفر بر نفر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کنندش دعای سحر هر شبی</p></div>
<div class="m2"><p>که عدلش شب فتنه را شد سحر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همه راد مردان از او شاکرند</p></div>
<div class="m2"><p>به شرق و به غرب و به بحر و به بر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نزاد و نزاید به سیصد قِران</p></div>
<div class="m2"><p>ز مادر چو او رادمردی دگر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ایا پهلوانی که از قدر و جاه</p></div>
<div class="m2"><p>تویی خسروان را به جای پدر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اگر چندگرم است آتش به طبع</p></div>
<div class="m2"><p>ز نزدیک سوزد همی خشک و تر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از اوگرم تر آتش تیغ توست</p></div>
<div class="m2"><p>که از دور سوزد عدو را جگر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عجب دارم اندر سقر زمهریر</p></div>
<div class="m2"><p>وگر چند هست این سخن در خبر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دم سرد باشد عدوی تو را</p></div>
<div class="m2"><p>مگر زان بود زمهریر سقر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هر آن خصم‌کز پیش تو روز رزم</p></div>
<div class="m2"><p>هزیمت پذیرد ز بیم خطر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زمانه به دستش دهد نامه‌ای</p></div>
<div class="m2"><p>نبشته در آن نامه این‌ المفر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اگر مرغ بلقیس را نامه برد</p></div>
<div class="m2"><p>ز نزد سلیمان پیغامبر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>برد تیر پران تو نزد خصم</p></div>
<div class="m2"><p>اجل‌نامهٔ خصم در زیر پر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>امیرا اگر چون تو باشد سحاب</p></div>
<div class="m2"><p>همه دُرّ فَشاند به جای مطر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به صُنعت از آتش برآید نسیم</p></div>
<div class="m2"><p>به فعلت از آهن بروید خضر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو من بر مدیحت گشایم زبان</p></div>
<div class="m2"><p>زبان را فضیلت نهم بر بصر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به باغ مدیح تو هر ساعتی</p></div>
<div class="m2"><p>ز شاخ ضمیرم‌ برآید ثمر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر از حدگذشته است تقصیر من</p></div>
<div class="m2"><p>به تقصیر منگر به عذرم نگر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو در شاعری من ندارم نظیر</p></div>
<div class="m2"><p>زتو جسم دارم قبول و نظر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>الا تا که امروز و فردا و دی</p></div>
<div class="m2"><p>پدید آید از رفتن ماه و خَور</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز دی خوبتر باد امروز تو</p></div>
<div class="m2"><p>وز امروز فردای تو خوبتر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سر رایتت باد خورشید سای</p></div>
<div class="m2"><p>سم مرکبت بادگیتی سپر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به‌ هر کار یزدان تو را رهنمای</p></div>
<div class="m2"><p>به‌هر راه دولت تورا راهبر</p></div></div>