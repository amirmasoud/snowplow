---
title: >-
    شمارهٔ ۳۴۴
---
# شمارهٔ ۳۴۴

<div class="b" id="bn1"><div class="m1"><p>ای جهانداری که از تو تازه باشد جاودان</p></div>
<div class="m2"><p>گوهر طغرل‌بک و جغری‌بک و الب‌ارسلان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا جلال دولتی دولت بماند پایدار</p></div>
<div class="m2"><p>تا جمال ملتی ملت بماند جاودان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست جز تو خلق عالم را یکی فریادرس</p></div>
<div class="m2"><p>نیست جزتو ملک‌ گیتی راکسی صاحبقران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آسمان گر یک شرف دارد ز پاکی بر زمین</p></div>
<div class="m2"><p>از تو بسیاری شرف دارد زمین بر آسمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نشان نیکبختی هرکس از دولت دهد</p></div>
<div class="m2"><p>نیکبختی را همی از تو دهد دولت نشان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای خداوند ملوک ای خسرو پیروزبخت</p></div>
<div class="m2"><p>ای شه بنده‌نواز ای داورگیتی ستان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا که از عدل تو آسایش همی یابد زمین</p></div>
<div class="m2"><p>تا که از رای تو آرایش همی یابد زمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عالم آبادست و آفاق ایمن و ملک استوار</p></div>
<div class="m2"><p>دین عزیز و نعمت ارزان و رعیت شادمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این اثر هرگز که دید از خسروان روزگار</p></div>
<div class="m2"><p>وین خبر هرگز که داد از سروران باستان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چند گویم قصهٔ افراسیاب کامکار</p></div>
<div class="m2"><p>چند خوانم نامهٔ نوشین‌ روان کامران</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چاوُشان داری بسی غالب‌تر از افراسیاب</p></div>
<div class="m2"><p>حاجبان داری بسی عادل‌تر از نوشیروان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای بسا میرا که اندر خدمت درگاه تو</p></div>
<div class="m2"><p>بر میان دارد کمر یا چون کمر دارد میان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر مثال قلعه‌ای بینم جهان را سربه‌سر</p></div>
<div class="m2"><p>واندرو شمشیر تو چون کوتوال و باسبان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زهر باید هر مَلِک را تا نهان دشمن‌ کند</p></div>
<div class="m2"><p>بند باید هر ملک را تا جهان‌ گیرد بدان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زهر تو اقبال توست وزان همی میرد عدو</p></div>
<div class="m2"><p>بند تو شمشیر توست وزان همی‌ گیری جهان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون کمان صدمنی در دست توگردد بلند</p></div>
<div class="m2"><p>چون خدنگ‌ دیده دوز از شست تو گردد روان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بس کمان‌افراز و تیرانداز کاندر پیش تو</p></div>
<div class="m2"><p>سر نهد بر خاک و از بازو بیندازد کمان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تیغ تو هنگام ضرب و اسب تو هنگام حرب</p></div>
<div class="m2"><p>آتس اندر جوشن است و باد در برگستوان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زآتش فرزند آزرگر همی نرگس برست</p></div>
<div class="m2"><p>زاتش تیغ تو روز جنگ روید ارغوان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ور همی راند سلیمان باد را در زیر تخت</p></div>
<div class="m2"><p>تو همی رانی به دولت باد را در زیر ران</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آفرین تو به دریای خرد در گوهرست</p></div>
<div class="m2"><p>بندهٔ مخلص معزی بیش تو گوهر فشان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر به‌کارش نامدی در خدمت تو جان و دل</p></div>
<div class="m2"><p>برفشاندی بر بساط تو دل و بر جام‌جان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا که زیرگنبد گردون هوا باشد سبک</p></div>
<div class="m2"><p>تا که در زیر هوا خاک زمین باشد گران</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بوستان عدل تو شه جاودان بشکفته باد</p></div>
<div class="m2"><p>وین جهان از عدل تو همچون شکفته بوستان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هم شهنشاه زمانی هم جهاندار زمین</p></div>
<div class="m2"><p>بر شهنشاهی بپای و در جهانداری بمان</p></div></div>