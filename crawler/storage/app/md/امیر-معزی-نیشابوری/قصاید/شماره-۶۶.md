---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>سدید ملک ملک عارض خراسان است</p></div>
<div class="m2"><p>صفی دولت و مخدوم اهل دیوان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پناه دین خدای و معین شرع رسول</p></div>
<div class="m2"><p>عمر که همچو علیّ و صدیق و عثمان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لقب سدید و صفی یافته است زانکه دلش</p></div>
<div class="m2"><p>قرارگاه سداد و صفای ایمان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گزیده عادت او چشم عقل را بصرست</p></div>
<div class="m2"><p>ستوده سیرت او جسم فضل را جان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هنر چو نقطه وکردار او چو پرگارست</p></div>
<div class="m2"><p>ادب چو نامه و گفتار او چو عنوان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر آسمان معالی به برج عز و شرف</p></div>
<div class="m2"><p>همه کفایت او بی خُسوف و نقصان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگر که کیوان جای بلند همت اوست</p></div>
<div class="m2"><p>که برتر از همه اجرام جای کیوان است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سخا توقع زو کن که او سخا ورزست</p></div>
<div class="m2"><p>سخن به مجلس او بر که او سخندان است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهر مقام همی بارد و همی تابد</p></div>
<div class="m2"><p>که ابر مَکرمت و آفتاب احسان است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ایا خجسته همامی‌ که با تو در همه کار</p></div>
<div class="m2"><p>ز چرخ بیعت و از روزگار پیمان است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عبارت تو ظُرَف را علامت است و نشان</p></div>
<div class="m2"><p>براعت تو نُکَت را دلیل و برهان است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هوای عمر تو صافی است از بخار و غبار</p></div>
<div class="m2"><p>عقیدهٔ تو چو ماه دو هفته تابان است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به ‌هرچه کردی و گفتی میان اهل خرد</p></div>
<div class="m2"><p>نه بر خصال تو عیب و نه بر تو تاوان است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صحیفه‌های تو قانون دولت ملک است</p></div>
<div class="m2"><p>جریده‌های تو دستور ملک سلطان است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عراقیان بستایند خط و لفظ تو را</p></div>
<div class="m2"><p>که خط و لفظ تو پیرایهٔ خراسان است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شگفت نادره مرغی است کلک در کف او</p></div>
<div class="m2"><p>که طعمه او را همواره قیر و قَطران است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر بود همه قطران و قیر طعمهٔ او</p></div>
<div class="m2"><p>لعاب او ز چه معنی چو درّ و مرجان است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به شمع ماند و او را چو عنبر است دُخان</p></div>
<div class="m2"><p>به‌ابر ماند و او را ز مشک باران است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>غذای او شبه رنگ است و این غریب ترست</p></div>
<div class="m2"><p>که در بنان تو بر سیم گوهر افشان است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بلند بختا با تو به یک دو بیت مرا</p></div>
<div class="m2"><p>حدیث حادثهٔ تیر شاه ایران است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اگرچه بر تنم آثار عافیت پیداست</p></div>
<div class="m2"><p>هنوز پیکان در کنج سینه پنهان است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خزانهٔ سخن است از قیاس سینهٔ من</p></div>
<div class="m2"><p>که اندرو گهر قیمتی فراوان است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همی برند به هر جا ازین خزینه گهر</p></div>
<div class="m2"><p>چنین خزینه دریغا که جای پیکان است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>من از لطافت تو شاکرم که درد مرا</p></div>
<div class="m2"><p>لطافت تو به جای علاج و درمان است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خدای عرش نگهبان بخت و عمر تو باد</p></div>
<div class="m2"><p>که دست و خامهٔ تو ملک را نگهبان است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز هیچ‌کار پشیمان مبادیا هرگز</p></div>
<div class="m2"><p>که دشمنت ز همه ‌کارها پشیمان است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>علاج سینهٔ من ‌گرچه صَعب و دشوارست</p></div>
<div class="m2"><p>بر آن که خالق خلق است سهل و آسان است</p></div></div>