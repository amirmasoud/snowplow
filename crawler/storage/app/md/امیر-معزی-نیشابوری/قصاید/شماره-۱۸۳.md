---
title: >-
    شمارهٔ ۱۸۳
---
# شمارهٔ ۱۸۳

<div class="b" id="bn1"><div class="m1"><p>ای ز دارٍُالملک رفته مدتی سوی سفر</p></div>
<div class="m2"><p>بازگشته سوی دارالملک با فتح و ظفر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نرد ملک و نرد دولت باخته در یک ندب</p></div>
<div class="m2"><p>کار دین و کار دنیا ساخته در یک سفر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برده در شام و بلاد روم بی‌رهبر سپاه</p></div>
<div class="m2"><p>کرده بر آب فرات و دجله بی‌کشتی‌ گذر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ملکهای شام را ترتیب داده یک به یک</p></div>
<div class="m2"><p>مالهای روم را تقریرکرده سر به سر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از صف لشکر فکنده جنبش اندر دشت وکوه</p></div>
<div class="m2"><p>وز تف خنجر فکنده جوش اندر بحر و بر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در هوای عالم ازگرد سوارانت نشان</p></div>
<div class="m2"><p>بر جبین عالم از نعل‌ سوارانت اثر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رادمردان را به طاعت درکف پیمانت دل</p></div>
<div class="m2"><p>شیرمردان را به خدمت بر خط فرمانت سر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر جهان را از جهاندارای و شاهی چاره نیست</p></div>
<div class="m2"><p>چون تو باید در جهانداری شه پیروزگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درخور پیشی و بیشی از همه شاهان تویی</p></div>
<div class="m2"><p>پیشتر رفتی و بگرفتی ز عالم بیشتر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن ظفرهایی‌که در یک سال شد حاصل تورا</p></div>
<div class="m2"><p>ده مجلد بیش باشد گر بگویم مختصر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در فتوح شام و روم امسال دیوان ساختم</p></div>
<div class="m2"><p>ساخت باید در فتوح هند و چین‌ سال دگر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چشم ما در روزگارت بیشتر بیند همی</p></div>
<div class="m2"><p>زانچه‌ گوش ما شنیدست از حکایت‌ وز سیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فتحها یکسر خبرگشته است و فتح تو عیان</p></div>
<div class="m2"><p>مرد دانا تا عیان یابد کجا جوید خبر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ایزد اندر شخص تو چندان هنر موجود کرد</p></div>
<div class="m2"><p>کز شمار آن همی عاجزشود وهم بشر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر هنرمندی که درگیتی هنر ورزد همی</p></div>
<div class="m2"><p>هست واجب‌ کاو بیاید وز تو آموزد هنر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای بسا شاها که بر سر داشت تاج خسروی</p></div>
<div class="m2"><p>تاج‌ بنهاد و به خدمت بست پیش تو کمر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای بسا حِصنا که از کین تو شور و شر نمود</p></div>
<div class="m2"><p>بازگشت آخر به ملک و دولت او شور و شر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کین تو چون زلزله است و هرکجا قوت گرفت</p></div>
<div class="m2"><p>خانمان و خان بدخواهان کند زیر و زبر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تیغ تو در باغ پیروزی درخت نصرت است</p></div>
<div class="m2"><p>بیخ او در خاور است و شاخ او در باختر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هست بر عالم همایون همت تو چون همای</p></div>
<div class="m2"><p>شرق دارد زیر بال و غرب دارد زیر پر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هست‌ گویی تیغ تو در طبع چون سودای خون</p></div>
<div class="m2"><p>زان‌ کجا آهنگ او سوی دِماغ است و جگر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کعبهٔ شاهان آفاق است عالی مجلست</p></div>
<div class="m2"><p>پایهٔ تخت و رکاب تو مقام است و حجر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خلق را از مهر دیدارت بیفزاید همی</p></div>
<div class="m2"><p>هم به جسم اندر حیات و هم به چشم اندر بصر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بندگان پروردگان دولت و بخت تواند</p></div>
<div class="m2"><p>خاصه آن شیر دلیر و میر بی‌همتا و نر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کرد بر راه و رکاب تو نثار سیم و زر</p></div>
<div class="m2"><p>خواستی‌ کش جان و دل بودی به جای سیم و زر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یافته است از خدمت‌ و مهر تو عالی دولتی</p></div>
<div class="m2"><p>تا پسر خوانی تو او را دولتش ناید بسر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خسروا شاها هر آنچ از دین و دنیا خواستی</p></div>
<div class="m2"><p>بی‌توقف یافتی از کردگار د‌ادگر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مدتی در هر زمینی در گشادی رزم را</p></div>
<div class="m2"><p>مدتی اندر سپاهان بزم را بگشای در</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در قدح زان گوهر پاکیزه دوشیزه خواه</p></div>
<div class="m2"><p>کافتابش دایه و مادر بدانگورش پدر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هرچه ‌هست اندر جهان از نیکی و شادی تو را است</p></div>
<div class="m2"><p>عمر در نیکی‌ گذار و روز در شادی شُمَر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا که هفت اقلیم و چار عنصر بود اندر جهان</p></div>
<div class="m2"><p>باد شش چیز از دو چیز تو عزیز و نامور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از بقای تو همیشه دولت و دنیا و دین</p></div>
<div class="m2"><p>وز لقای تو همیشه شادی و تایید و فر</p></div></div>