---
title: >-
    شمارهٔ ۳۶۱
---
# شمارهٔ ۳۶۱

<div class="b" id="bn1"><div class="m1"><p>شد ز تاثیر سپهر سرکش نامهربان</p></div>
<div class="m2"><p>هجر یار مهربان چون وصل باد مهرگان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لاجرم‌گیتی و من هر دو موافق‌گشته‌ایم</p></div>
<div class="m2"><p>او ز باد مهرگان و من ز یار مهربان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>او همی دارد هوا را سرد بی‌دیدار این</p></div>
<div class="m2"><p>من همی‌دارم‌نفس را سردبی‌دیدار آن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>او همی ریزد بعمدا بر زمردکهربا</p></div>
<div class="m2"><p>من همی سایم بعمدا برشقایق زعفران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من بخار عشق دارم در بصر بیجاده بار</p></div>
<div class="m2"><p>او بخارآب دارد برهوا لولو فشان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من همی پنهان‌کنم در طبع راز خویشتن</p></div>
<div class="m2"><p>او همی پنهان‌ کند در خاک نقش بوستان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>او همی بر خاک خشک آتش برافروزد زچوب</p></div>
<div class="m2"><p>من همی بر طبع سرد آتش برانگیزم زجان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>او همی پژمرده گردد بی‌بهار دل‌گشای</p></div>
<div class="m2"><p>من همی فرسوده کردم بی‌نگار دلستان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن نگاری کز وصال و هجر او پیدا شود</p></div>
<div class="m2"><p>در خزان من بهار و در بهار من خزان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قامت او سرو را قیمت دهد در جویبار</p></div>
<div class="m2"><p>طلعت او ماه را روشن‌کند در آسمان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عارضش در زیر خط دندانْشْ اندر زیر لب</p></div>
<div class="m2"><p>چشمش اندر زیر مژگان و دلش در بر نهان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سوسن اندر جوشن است و لؤلؤ اندر لاله برگ</p></div>
<div class="m2"><p>نرگس اندر سوزن است و آهن اندر پرنیان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نسبتی دارد همانا زلف او با طبع من</p></div>
<div class="m2"><p>کان به یک صنعت چنین است او به یک صنعت چنان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زلف او بر دامن خورشید دارد مشک ناب</p></div>
<div class="m2"><p>طبع من بار دگر در مدح خورشید جهان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مجد دولت افتخار ملت صاحب‌کتاب</p></div>
<div class="m2"><p>بوالمحاسن آفتاب دولت صاحبقران</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن‌که اندر طاعتش گردون همی‌کوشد به جهد</p></div>
<div class="m2"><p>وان‌که اندر خدمتش دولت همی بندد میان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن خداوندی که بر ایوان و درگاهش سزد</p></div>
<div class="m2"><p>اوج‌ گردون کوتْوال و برج‌کیوان پاسبان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کوه را با حلم او گر بنگری باشد سبک</p></div>
<div class="m2"><p>باد را با طبع او گر بنگری باشدگران</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در مصاف دشمنان با نیزه و شمشیر او</p></div>
<div class="m2"><p>چیست ضایع‌تر ز درع و جوشن و برگستوان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تیغش اندر دست دیدم همچو بیم اندر امید</p></div>
<div class="m2"><p>عفوش اندر خشم دیدم همچو سود اندر زیان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زر و مشک از خدمتش خیزد همی زیراکه هست</p></div>
<div class="m2"><p>زایرش زرین کنار و مادحش مشکین دهان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خانهٔ کعبه است کویش خان ابراهیم بزم</p></div>
<div class="m2"><p>کاین زطاعت خواه خالی نیست وان از میهمان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>باد را هرگز نباشد با فلک پیوستگی</p></div>
<div class="m2"><p>او فلک قدر است و دارد باد را در زیر ران</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا ندیدم اسب او را من ندانستم که هست</p></div>
<div class="m2"><p>در جهان باد مصور با رکاب و با عنان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای همه تأثیر گردون را به‌خوبی رهنمای</p></div>
<div class="m2"><p>وی همه‌ تقدیر ایزد را به نیکی‌ ترجمان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گاه شدت بس نباشد دشمنانت را سقر</p></div>
<div class="m2"><p>روز نعمت بس نباشد دوستانت را جنان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در سخا بدخواه مالی دروغا بدخواه مال</p></div>
<div class="m2"><p>در شتاب آتش‌فشانی در درنگ آتش‌نشان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو به عدل اندر چو خورشیدی ولیکن بی‌زوال</p></div>
<div class="m2"><p>تو به جود اندر چو دریایی ولیکن بی‌کران</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تو چو موسی و سلیمان و فریدون مقبلی</p></div>
<div class="m2"><p>بی عصا و بی نگین و بی درفش کاویان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اختیار ایزدی تا عقل داری اختیار</p></div>
<div class="m2"><p>قهرمان دولتی تا قهر داری قهرمان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دودهٔ نسل‌ کمالی از تو ماند تا به حشر</p></div>
<div class="m2"><p>همچو از سلطان عالم دودهٔ الب ارسلان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نوبت بخت جوان دشمنانت درگذشت</p></div>
<div class="m2"><p>نوبت تو در رسید از دولت شاه جهان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آتش خشم تو هر ساعت‌ که بنماید سرشک</p></div>
<div class="m2"><p>دشمنانت را برآرد دود مرگ از دودمان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ای خداوندی که از اقبالْ سوی درگهت</p></div>
<div class="m2"><p>قافله د‌ر قافله است و کاروان در کاروان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از هر آن دشمن که خشم تو برآرد دودوگرد</p></div>
<div class="m2"><p>آتش سوزنده گردد مغزش اندر استخوان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مهر تو جویم به دل تا در دلم باشد خِرَد</p></div>
<div class="m2"><p>پیش تو باشم به تن تا در تنم باشد روان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از ثنا و مدح تو فارغ نباشم یک نفس</p></div>
<div class="m2"><p>وز سرای و کوی تو غایب نباشم یک زمان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چون ز سلطان بگذرم مقصود هر معنی تویی</p></div>
<div class="m2"><p>زانچه بنویسم به دست و زانچه رانم بر زبان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تا نباشد سوگوار اندر طرب چون شادخوار</p></div>
<div class="m2"><p>تا نباشد ناتوان اندر ظفر چون کامران</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>باده‌خوار اندر طرب بادی و خصمت سوگوار</p></div>
<div class="m2"><p>کامران اندر ظفر بادی و خصمت ناتوان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>روزگار و بخت و اقبال تو هر سه پایدار</p></div>
<div class="m2"><p>مهرگان و عید نوروز تو هر سه جاودان</p></div></div>