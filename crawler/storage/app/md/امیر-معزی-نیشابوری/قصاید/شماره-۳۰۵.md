---
title: >-
    شمارهٔ ۳۰۵
---
# شمارهٔ ۳۰۵

<div class="b" id="bn1"><div class="m1"><p>رسید عید و ز قندیل نار داد به جام</p></div>
<div class="m2"><p>ز جام نور به قندیل داد ماه تمام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هلال عید کلید همان دَرَست مگر</p></div>
<div class="m2"><p>که قفل‌ گشت بر آن در هلال ماه تمام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دَرِ بساط دگر باره چرخ بازگشاد</p></div>
<div class="m2"><p>شکست شیشهٔ خاص و درید پردهٔ عام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنون به جام غم انجام می‌کند آغاز</p></div>
<div class="m2"><p>که عید را آغازست و روزه را انجام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کنون به میکده باشد ز شام تا گه صبح</p></div>
<div class="m2"><p>هر آن‌که بود به مسجد زصبح تاگه شام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنون به رود و سرود اقتدا کند هر دو</p></div>
<div class="m2"><p>هر آن که‌کرد همی هر شب اقتدا به امام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من آن‌کسم‌که به‌کنجی نشستم وکردم</p></div>
<div class="m2"><p>مهی تمام صبوری ز روی ماه تمام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زبهر حرمت و تعظیم شرع دانستم</p></div>
<div class="m2"><p>نماز و روزه حلال و کنار و بوسه حرام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گشاده بود زبانم به‌نام و ذکر خدای</p></div>
<div class="m2"><p>اگرچه بسته دهان بودم از شراب و طعام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وگرچه بود کف من تهی زآب کروم</p></div>
<div class="m2"><p>تهی نبود دل من ز مدح صدر کرام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سدید دین سَرِاشراف دهر مُشْرِف ملک</p></div>
<div class="m2"><p>وجیه دولتْ شمس شرفْ جمالِ انام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پناه و پیشرو دودهٔ ظهیر که هست</p></div>
<div class="m2"><p>چو یار غار و چو خیرالبشر به ‌کنیت و نام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سر سپهر برین در لگام دولت اوست</p></div>
<div class="m2"><p>سپهر توسن از این روی نرم باشد و رام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مُنّزه است‌گه جود طبع او ز ملال</p></div>
<div class="m2"><p>مقدس است‌ گه شکر عقل او ز مَلام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هزار حادثه زایل‌ کند به‌ یک تدبیر</p></div>
<div class="m2"><p>هزار فایده حاصل کند بهٔک پیغام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گه رضا و سَخَط‌ گر کند مبالغتی</p></div>
<div class="m2"><p>ظلام نور شود در جهان و نور ظلام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو برنهد گه بخشش قلم به خط و دوات</p></div>
<div class="m2"><p>چو بر کشد گه کوشش حُسام را ز نیام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سر نیاز کند پست همچو قَد قلم</p></div>
<div class="m2"><p>لب حسود کند نیلگون چو روی حُسام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر مَجَسَّم‌ گردد ضیای همت او</p></div>
<div class="m2"><p>به‌پای او نرسد فَیْلَسوف را اوهام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هوای اوست همیشه به ‌همت عالی</p></div>
<div class="m2"><p>بود به‌همت عالی هوای مرد همام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>برو به بلخ و سرایش ببین اگر خواهی</p></div>
<div class="m2"><p>نشان قبهٔ کسری به قُبَّهٔ‌الاسلام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به صحن او بگذر کز بهشت دارد بوم</p></div>
<div class="m2"><p>به سقف او بنگر کز بهشت دارد بام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سپهر بیند هر که اندر او گمارد جسم</p></div>
<div class="m2"><p>بهشت یابد هر کاندرو گذارد گام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر آن ‌که هست به بلخ و هر آن ‌که هست ایدر</p></div>
<div class="m2"><p>خجسته بادش و میمون و فرخ و پدرام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آیا ز گوهر پیغمبری‌ که تا محشر</p></div>
<div class="m2"><p>به‌کعبه از شرف او گرفت قدر مقام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جو ایزد از گهر او نمود نور تو را</p></div>
<div class="m2"><p>سلیم‌ گشت بر او نار و برد گشت سلام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ندای بخت تو گر بشنوند زیر زمین</p></div>
<div class="m2"><p>گذشتگان کهن گشته از اولوالاحکام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>برآورند سر از خاک از هر اقلیمی</p></div>
<div class="m2"><p>به‌سر دوند سوی خدمت تو چون اقلام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زکین و حقد تو ماند به تیر زهرآلود</p></div>
<div class="m2"><p>تن عدوی تو را در میان مغز عظام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به‌جای مغز چو اندر عظام دارد تیر</p></div>
<div class="m2"><p>به‌جای خوی همه خون آیدش همی ز مسام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مخالفان تو را از چهار گوهر هست</p></div>
<div class="m2"><p>چهار طبع مقیم و چهار چیز مدام</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز نار گرمی مغز و ز باد سردی دم</p></div>
<div class="m2"><p>ز آب ترّی چشم و زخاک خشکی‌ کام</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اگر همیشه زمام زمان به‌ دست قضاست</p></div>
<div class="m2"><p>قضا تویی‌ که زمان را به دست توست زمام</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تویی ‌که اهل زمان را به دست و شکر تو هست</p></div>
<div class="m2"><p>هم ابتدای کتاب و هم افتتاح کلام</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شمار مدت عمر تو تا به ‌روز شمار</p></div>
<div class="m2"><p>درست شد ز نجوم و فراست و اعلام</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اگرچه رای قوام از جهان شدست برون</p></div>
<div class="m2"><p>ز رای توست همه کارها گرفته قوام</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>وگرچه هست کنون بی‌نظام کار عراق</p></div>
<div class="m2"><p>گرفت کار خراسان به همت تو نظام</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خجسته همت تو آفتاب را ماند</p></div>
<div class="m2"><p>که روزگار همی نور ازو ستاند وام</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اگر تو پرسی از روزگار نشناسند</p></div>
<div class="m2"><p>که آفتاب کدام است و رایت تو کدام</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مگر ستاره ی سَعدست کلک در کف تو</p></div>
<div class="m2"><p>که هست در حرکاتش زمانه را آرام</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>سزاست درکف راد تو کلک درافشان</p></div>
<div class="m2"><p>چنانکه در کف میر تو تیغ خون‌آشام</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دل امیر تو در دام شکر توست شکار</p></div>
<div class="m2"><p>شکار دل بود آری چو شکر باشد دام</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>رسید عید همایون و رایت میمون</p></div>
<div class="m2"><p>رسید فتح یمین‌الملوک را هنگام</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گشاده شد علم عید وگشت عزالدین</p></div>
<div class="m2"><p>علامت ظفر و فتح بر سر اعلام</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>مظفرند ازین جنگ زانکه در سفرش</p></div>
<div class="m2"><p>قوام ملت و شرع است تا به روز قیام</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ایا ستوده کریمی که از سیاست تو</p></div>
<div class="m2"><p>موشح است به‌در دانه گردن ایام</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به خدمت تو رسیدن فریضه دانم من</p></div>
<div class="m2"><p>ز بهر آنکه رسیدم به خدمت تو به‌کام</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سزدکه آیم وآرم مدیح تو هرروز</p></div>
<div class="m2"><p>از آن نیایم و نارم که ترسم از ابرام</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>اگرچه هست خطاب من از ملوک امیر</p></div>
<div class="m2"><p>تورا به‌طوع رهی گشتم و به طبع غلام</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>حروف مدح تو گوهر شدست در دهنم</p></div>
<div class="m2"><p>بدان صفت که شود در صدف سرشک‌ غمام</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو راست کردهٔ انعام توست مرسومم</p></div>
<div class="m2"><p>روامدار که نقصان رود در آن انعام</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>رضا مده که سود خام کار پختهٔ من</p></div>
<div class="m2"><p>که هرچه پخته شود زان سپس نگردد خام</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>همیشه تا به فلک بر قِران اجرام است</p></div>
<div class="m2"><p>چنان‌کجا به زمین بر تولد اجسام</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>فتاده باد بر اجسام سایهٔ کرمت</p></div>
<div class="m2"><p>نهاده همت تو پای بر سر اجرام</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>لباس عمر تو نو باد در جهان کهن</p></div>
<div class="m2"><p>طراز او ز بقا باد و نقش او ز دوام</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>تورا همیشه به‌سوی چهار چیز دودست</p></div>
<div class="m2"><p>به دفتر و قلم و جام و زلف غالیه فام</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تو خوش نشسته و پیش تو ایستاده بتی</p></div>
<div class="m2"><p>به چشم و چهره چو بادام و گلشن بادام</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>خجسته عید تو و روزهٔ توکرده قبول</p></div>
<div class="m2"><p>خدای عزوجل ذوالجلال والاکرام</p></div></div>