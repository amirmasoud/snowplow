---
title: >-
    شمارهٔ ۲۸۲
---
# شمارهٔ ۲۸۲

<div class="b" id="bn1"><div class="m1"><p>عید را با مهرگان هست اتفاق و اتصال</p></div>
<div class="m2"><p>هر دو را دارند اهل دولت و ملت به فال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اتفاق و اتصال هر دو بر ما خرم است</p></div>
<div class="m2"><p>مرحبا زین اتفاق و حبذا زین اتصال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عید آیینی است کز وی هست ملت را شرف</p></div>
<div class="m2"><p>مهرگان رسمی است کزوی هست دولت را جمال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن یکی دارد بدین اندر ز پیغمبر نشان</p></div>
<div class="m2"><p>وین دگر دارد به ملک اندر ز افریدون مثال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر دو منشور نشاط و خرمی آورده‌اند</p></div>
<div class="m2"><p>بیش تخت خسرو نیک اختر نیکو خصال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آفتاب نسل سلجوق ارسلان ارغو که هست</p></div>
<div class="m2"><p>تا قیامت بی‌غروب و بی‌کسوف و بی‌زوال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن جهانداری‌که یک‌ تن نیست اندر شرق و غرب</p></div>
<div class="m2"><p>یا به میری یا به دولت یا به ملک او را همال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باز عدلش گر چه اکنون شرق دارد زیر پر</p></div>
<div class="m2"><p>چون به پرواز اندر آید غرب‌گیرد زیر بال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در خلاف او قدم برداشتن باشد حرام</p></div>
<div class="m2"><p>کز پدر ملک جهان دارد به میراث حلال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دولت او هست چون تقدیر ایزد لَم‌ یَزَل</p></div>
<div class="m2"><p>هرچه باشد لم یزل ناچار باشد لایزال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرکه بیرون شد ز عدلش زین جهان بیرون نشد</p></div>
<div class="m2"><p>تا ندید از دولت او دستبرد وگوشمال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا کی از شهنامه و تاریخ شاهان‌ کهن</p></div>
<div class="m2"><p>تاکی از دیو سفید و رستم و سیمرغ‌ و زال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کس ندید از قاف تا قاف جهان سیمرغ و دیو</p></div>
<div class="m2"><p>قیل و قال است این چرا باید شنیدن قیل و قال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قصه‌ای باید شنید و دفتری باید نوشت</p></div>
<div class="m2"><p>کاندرو باشد عجایب و آن عجایب حسب حال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حسب حال با عجایب فتح شاه مشرق است</p></div>
<div class="m2"><p>وآن شجاعتها که او بنمود هنگام قتال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنچه در سی سال نتواند نمودن هیچ شاه</p></div>
<div class="m2"><p>او ز مردی و هنرمندی نمود اندر سه سال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هیبت او دست مکاران و محتالان ببست</p></div>
<div class="m2"><p>کس نیارد گشت اکنون گرد مکر و احتیال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ور کسی خواهد که‌ گردد گو بیا بنگر نخست</p></div>
<div class="m2"><p>قصهٔ تیر دو شاخ و قصهٔ چاه و جوال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر که با تیغ جهانگیرش نماید سرکشی</p></div>
<div class="m2"><p>گر بماند زنده جان و تن بر او باشد وبال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رنگ‌ آب و فعل‌ آتش هر دو اندر تیغ اوست</p></div>
<div class="m2"><p>سرکشی با آب و آتش در خرد باشد محال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مرکبش را هر کجا باشد مجال تاختن</p></div>
<div class="m2"><p>وهم مردم را نباشد گِرد گَرد او مجال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آفرین بر مرکبش‌ کاو را سزد پروین لگام</p></div>
<div class="m2"><p>مشتری زین و مجره تنگ و ماه نو نعال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نه نهنگ و با نهنگان آب خورده در بحار</p></div>
<div class="m2"><p>نه پلنگ و با پلنگان خواب کرده در جبال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پاک دندان تیز چشم آهخته‌ گردن خُرد گوش</p></div>
<div class="m2"><p>سخت سم محکم قوایم پهن‌پشت آگنده یال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نه ز سیر او را قرار و نه ز دور او را شکیب</p></div>
<div class="m2"><p>نه‌ ز رزم او را نهیب‌ و نه‌ ز صید او را ملال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در دو پای او تو پنداری دبورست و صبا</p></div>
<div class="m2"><p>در دو دست او تو پنداری جنوب است و شمال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>این چنین مرکب‌ نشاید جز ملک را تا بر او</p></div>
<div class="m2"><p>گه به سوی صید تازد گه به‌ جنگ بدسگال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای ز صد گردون قوی تر تیغ تو روز نبرد</p></div>
<div class="m2"><p>وی ز صد دریا سخی‌تر دست تو روزنوال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از تو هنگام فضیلت فرق باشد تا ملوک</p></div>
<div class="m2"><p>همچنان کز شیر شرزه فرق باشد تا شغال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>قلعهٔ بخت تو را برگنبد فیروزه رنگ</p></div>
<div class="m2"><p>ماه زیبد پاسبان و مهر شاید کوتوال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خواسته تا خواسته بخشی همی‌گویی مگر</p></div>
<div class="m2"><p>از جهان برداشتی یک بارگی رسم سؤال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کار عالم را همی جود تو سازد سربه‌سر</p></div>
<div class="m2"><p>جود توگویی معیل است و همه عالم عیال</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آمد آن فصلی‌که از تاثیر او در بوستان</p></div>
<div class="m2"><p>دیبهٔ زربفت پوشیدست پنداری نهال</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>باغ‌ هست اکنون ز برگ زرد پر زرّ درست</p></div>
<div class="m2"><p>وز شکوفه بود در نوروز پرسیم حلال</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زاغ‌ گویی محتسب شد کز نهیب زخم او</p></div>
<div class="m2"><p>بلبل رامشگر اندر بوستان ماندست لال</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نار شد بازارگان وز لعل دامن کرد پر</p></div>
<div class="m2"><p>سیب دلبرگشت وز شنگرف زد بر روی خال</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>آب‌ گویی در شمر حراقهٔ چینی شدست</p></div>
<div class="m2"><p>کاندرو چشم جهان بین از صور بند خیال</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در چنین فصلی سزد گر گوهری گیری به دست</p></div>
<div class="m2"><p>گوهری کاو را وطن در آبگینه است و سفال</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هست فرزند رزان لیکن ز عکس و روشنی</p></div>
<div class="m2"><p>آفتابش هست عَمّ و ماهتابش هست خال</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هرکس اندر مهرگان پیش تو آرد خدمتی</p></div>
<div class="m2"><p>خدمت مداح تو شعری است چون‌ آب زلال</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همچنان شعری که در محمود گوید عنصری‌:</p></div>
<div class="m2"><p>«‌مهرگان آمدگرفته فالش از نیکی مثال‌»</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>باد با تیغ تو نصرت را به رزم اندر قران</p></div>
<div class="m2"><p>باد با جام تو عشرت را به بزم اندر وصال</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چشم پیروزی همیشه بر مه منجوق تو</p></div>
<div class="m2"><p>چون شب شوال چشم روزه‌داران بر هلال</p></div></div>