---
title: >-
    شمارهٔ ۲۳۳
---
# شمارهٔ ۲۳۳

<div class="b" id="bn1"><div class="m1"><p>هست شکر بار یاقوت تو ای عیار یار</p></div>
<div class="m2"><p>نیست‌کس را نزد آن یاقوت شکر بار بار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سال سرتاسر چو گلزارست خرم عارضت</p></div>
<div class="m2"><p>چون دل من صد دل اندر عشق آن‌گلزار زار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیمهٔ دینار را ماند دهان تنگ تو</p></div>
<div class="m2"><p>در دل تنگم فکند آن نیمهٔ دینار نار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای بت شیرین لبان تا چند از این ‌گفتار تلخ</p></div>
<div class="m2"><p>روز من چون شب مدار از تلخی‌گفتار تار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوستی و مهربانی کار تو پنداشتم</p></div>
<div class="m2"><p>‌کی گمان بردم که ا‌داریا‌ کینه و پیکار کار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشقی جستم نگارا تا دلم غمخوار گشت</p></div>
<div class="m2"><p>گشتم اندر عهدهٔ عشق از دل غمخوار خوار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکه از یاران وفا جوید نبیند جز جفا</p></div>
<div class="m2"><p>آفرین فخر فَتْیان بهتر از بسیار یار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قاسِمُ الارزاق بُوالقاسم که اندر حَلّ و عَقْد</p></div>
<div class="m2"><p>هست عزم او میان نیک و بد دیوار وار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن‌که اندر مهتری آثار خوبش ظاهرست</p></div>
<div class="m2"><p>وز بداندیشان همی خواهد بر آن آثار ثار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هست فرخنده درختی باغ اصل خویش را</p></div>
<div class="m2"><p>کافرید از آفرینش ایزد جبار بار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرد گردون همت میمون او هنجار زد</p></div>
<div class="m2"><p>لاجرم‌ گشته است‌ گردون را از آن هنجار جار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر بپیوندد زه سوفار تیر خویش را</p></div>
<div class="m2"><p>شیر نر گردد ز بیم و ترس آن سوفار فار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شار غرجستان اگر یابد نسیم همتش</p></div>
<div class="m2"><p>خاک آن بقعت‌ کند چون زر مشت‌افشار شار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کلک او در دست او مرغی است زرین ای عجب</p></div>
<div class="m2"><p>هر زمان بر لوح سیمین بارد از منقار قار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نور رای او اگر بر کوه بلغار اوفتد</p></div>
<div class="m2"><p>معدن یاقون گردد در کُه بلغار غار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای هنرمندی که گر بر مار بگذاری ضمیر</p></div>
<div class="m2"><p>زار گردد همچو مور از حسرت تیمار مار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تحفه‌ای زیبا فرستادم تو را از طبع خویش</p></div>
<div class="m2"><p>تحفهٔ طبع مرا با قیمت و مقدار دار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>من یقین دانم که تو فخر آوری زاشعار من</p></div>
<div class="m2"><p>حاش لله ‌گر تو داری از چنین اشعار عار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا که بشناسد ز چوگان مرد حِکمت گوی گوی</p></div>
<div class="m2"><p>تا که بشناسد ز بلبل مرد زیرک سار سار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شاخ شادی و طرب بنشان به نام دوستان</p></div>
<div class="m2"><p>تخم درد و غم به نام دشمن مکار کار</p></div></div>