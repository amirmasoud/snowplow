---
title: >-
    شمارهٔ ۳۱۰
---
# شمارهٔ ۳۱۰

<div class="b" id="bn1"><div class="m1"><p>کی توان‌ گفتن که شد ملک شهنشه بی‌نظام</p></div>
<div class="m2"><p>کی توان ‌گفتن که شد دین پیمبر بی‌قوام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی توان‌ گفتن که شد صدر زمان زیر زمین</p></div>
<div class="m2"><p>کی توان ‌گفتن که شد بَدر زمین اندر غَمام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قهر یزدان نرم‌ کرد آن را که بودش دهر نرم</p></div>
<div class="m2"><p>چرخ‌ گردان رام کرد آن را که بودش بخت رام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عالمی در یک زمان معدوم شد در یک مکان</p></div>
<div class="m2"><p>امّتی در یک نفس مَدروس شد در یک مقام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد شکار عالم آن ‌کاو کرد عالم را شکار</p></div>
<div class="m2"><p>شد به‌کام دشمن آن‌کاو دید دشمن را به‌کام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در ره بغداد صیّاد اجل دامی نهاد</p></div>
<div class="m2"><p>بس شگرف و محتشم صیدی درافتادش به دام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن‌ که بودی روزگارش با صیام و با صلوت</p></div>
<div class="m2"><p>روزگارش منقطع شد در صلوت و در صیام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن‌که بودی چون حُسام اندر بنان او قلم</p></div>
<div class="m2"><p>خون همی‌گرید قلم در فرقت او چون حسام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن ‌که خصمان در پیام او همی عاجز شدند</p></div>
<div class="m2"><p>گشت عاجز چون به جان او ز مرگ آمد پیام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای جهان بی‌وفا رنج بصر کردی حلال</p></div>
<div class="m2"><p>تا فروغ طلعت او بر بصرکردی حرام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن که تیغ عدل کرد اندر نیام دولتش</p></div>
<div class="m2"><p>تیغ‌ کین اندر هلاکش برکشیدی از نیام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن‌که بود اندر وزارت بی‌ملام و بی‌ملال</p></div>
<div class="m2"><p>در ملال عمر اوگشتی سزاوار ملام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در حیاتش جان خاص و عام سخت آسوده بود</p></div>
<div class="m2"><p>در وفاتش سخت شوریدست شغل خاص و عام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بود حلمش خاک وجودش آب و هست اندر غمش</p></div>
<div class="m2"><p>خاک بر فرق‌ کفات و آب در چشم‌ کرام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>راست پنداری خلایق در منامند از قیاس</p></div>
<div class="m2"><p>وین شگفتی ها همی بینند گویی در منام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای وزیر شاه عالم بردی از عالم علم</p></div>
<div class="m2"><p>وی قوام دین شدی در پرده تا روز قیام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای به امر و نهی‌ کرده بر سر گیتی فسار</p></div>
<div class="m2"><p>کرد عزرائیل ناگه بر سر عمرت لگام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شد وزارت بر تو گریان بر بساط تعزیت</p></div>
<div class="m2"><p>شد کفایت بی‌ تو گریان در لباس احتشام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نه ببالد چون تو در باغ ظفر سروی بلند</p></div>
<div class="m2"><p>نه بتابد چون تو در چرخ هنر ماهی تمام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مرگ تو پرگار شیون گرد ملک اندر کشید</p></div>
<div class="m2"><p>هم اَنام است اندرین پرگار و هم شاه انام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آن‌که پیوسته به مدح تو زبان برداشتی</p></div>
<div class="m2"><p>خشک دارد بر مصیبت زآتش هجر تو کام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>با دریغ و حسرت تو در غریو افتاده‌اند</p></div>
<div class="m2"><p>بی‌نهایت خلق از فرزند و پیوند و غلام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زعفران و نیل سُوْ دَسْتند گویی کز صفت</p></div>
<div class="m2"><p>رویشان مر زعفران‌گون است و لبها نیل فام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر نبود اندازهٔ عمرت مدام اندر جهان</p></div>
<div class="m2"><p>شکر آثار تو خواهد بود تا محشر مدام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>باد شخصت را نثار از حامل عرش مجید</p></div>
<div class="m2"><p>باد روحت را سلام از خازن دارالسلام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دست حسرت جامهٔ صبر معزی چاک‌ کرد</p></div>
<div class="m2"><p>تا جهانی را مُعَزّا کرد حَیّ لایَنام</p></div></div>