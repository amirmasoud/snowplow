---
title: >-
    شمارهٔ ۲۳۲
---
# شمارهٔ ۲۳۲

<div class="b" id="bn1"><div class="m1"><p>چون عقیق آبدار است و کمند تابدار</p></div>
<div class="m2"><p>آن لب جان پرور و زلف جهان آشوب یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آب دارم در دو چشم و تاب دارم در جگر</p></div>
<div class="m2"><p>زان عقیق آبدار و زان ‌کمند تابدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلف او گرد رخش پروانه‌وار است ای عجب</p></div>
<div class="m2"><p>این دل من هست در سودای او دیوانه‌وار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست یک ساعت قرار این هر دو را بر جای خویش</p></div>
<div class="m2"><p>کی بود پروانه و دیوانه را هرگز قرار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نبیند هیچکس پیوسته با خورشید سرو</p></div>
<div class="m2"><p>کان یکی بر آسمان است این دگر بر جویبار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی چون خورشید او بر سرو مسکن چون گرفت</p></div>
<div class="m2"><p>قامت چون سرو را خورشید چون آورد بار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من ز دل ‌گیرم قیاس ا‌حال زارا خویشتن</p></div>
<div class="m2"><p>او ز من‌ گیرد قیاس این دل نابردبار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>او چو در من بنگرد داند که ‌گرم افتاد دل</p></div>
<div class="m2"><p>من چو د‌ر او بنگرم دانم ‌که صعب افتاد کار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در دو زلفش هست عطر و در دل من آتش است</p></div>
<div class="m2"><p>عطر در آتش نهد چون گیرم او را در کنار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواهد آن دلبر که من وصف جمال او کنم</p></div>
<div class="m2"><p>بوی عطر آید ز من در پیش تخت شهریار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاه مشرق ارسلان ارغو خداوند ملوک</p></div>
<div class="m2"><p>ملک را از ارسلان سلطان مبارک یادگار</p></div></div>