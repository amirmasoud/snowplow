---
title: >-
    شمارهٔ ۱۲۶
---
# شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>ایا شهی‌ که چو تو کس ندید و کس نشنود</p></div>
<div class="m2"><p>چو تو نباشد در عالم و نه هست و نه بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کدام شاه تو را دید در میانهٔ صدر</p></div>
<div class="m2"><p>که بر بساط تو بوسه نداد و چهره نسود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر آنکه تافت بر او آفتاب دولت تو</p></div>
<div class="m2"><p>به زیر سایهٔ اقبال تو فرو آسود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تویی که تیغ تو آن گوهر ستاره‌نمای</p></div>
<div class="m2"><p>هزار روز به بدخواه تو ستاره نمود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تویی که دیده ز چشم عدو برون آری</p></div>
<div class="m2"><p>به نوک نیزهٔ سندان گداز زهرآلود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میان خنجر تو آتشی است کان آتش</p></div>
<div class="m2"><p>همی زدودهٔ اعدای تو برآرد دود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تن حسود تو را باد در ربود چو کاه</p></div>
<div class="m2"><p>ز بس که بر سر خود باد را همی پیمود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خدایگانا بخشایش آر بر تن من</p></div>
<div class="m2"><p>از آنکه رای تو بر صد هزار تن بخشود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو زیر خاک نهان شد خلیل حضرت تو</p></div>
<div class="m2"><p>میان جان من افروخت آتش نمرود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز درد آنکه بپالود زیر خاک تنش</p></div>
<div class="m2"><p>دلم چو خون شد و از دیدگان فرو پالود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رسید نوبت سرما و فصل‌گرما رفت</p></div>
<div class="m2"><p>بکاست صبر من و سردی هوا بفزود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غنود دیدهٔ بلبل ز باد شهریور</p></div>
<div class="m2"><p>ز شهریار چرا چشم من شبی نغنود؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ربود حسرت و تیمار زاغ باد خزان</p></div>
<div class="m2"><p>نسیم جود تو تیمار من چرا نربود؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دروده‌ گشت زمین هرکجا و همت تو</p></div>
<div class="m2"><p>نهال حسرت و بیماریم چرا ندرود؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زدوده رنگ درختان سپهر آینه‌ گون</p></div>
<div class="m2"><p>کمال عدل تو زنگ دلم چرا نزدود؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>غریب شهر توام بشنو از من این قصه</p></div>
<div class="m2"><p>که مصطفی به‌کرم قصهٔ غریب شنود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>روم که گر نروم باشم اندرین هفته</p></div>
<div class="m2"><p>به خون جملهٔ خویشان خویشتن ماخوذ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رضای مادر و خشنودی پدر جویم</p></div>
<div class="m2"><p>که درکتاب خود ایزد مرا چنین فرمود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر بزودی یابم ز شاه دستوری</p></div>
<div class="m2"><p>شوند جمله ز من مادر و پدر خشنود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نگاه دار شها حق خدمت پدرم</p></div>
<div class="m2"><p>که از کمال ارادت تو را به جان بستود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به حق خدمت برهانی و ارادت او</p></div>
<div class="m2"><p>که شغل بنده بدین هفته برگذاری زود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همیشه تا که بود اختری جهان‌فرسا</p></div>
<div class="m2"><p>همیشه تا که بود صورتی زمین فرسود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مخالفان تو را باد بی‌طرف همه غم</p></div>
<div class="m2"><p>موافقان تو را باد بی‌زیان همه سود</p></div></div>