---
title: >-
    شمارهٔ ۴۲۶
---
# شمارهٔ ۴۲۶

<div class="b" id="bn1"><div class="m1"><p>ای آسمان مُسَخّر حکمِ روانِ تو</p></div>
<div class="m2"><p>کیوانِ پیر بندهٔ بخت جوان تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید عالمی که به‌ هنگام بزم و رزم</p></div>
<div class="m2"><p>گه زین و گاه تخت بود آسمان تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر در زمان مهدی ایمن شود جهان</p></div>
<div class="m2"><p>امروز ایمن است جهان در زمان تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر روز بامداد همی دولت بلند</p></div>
<div class="m2"><p>بندد به دست خویش کمر بر میان تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چند وحی نیست پس از عهد مصطفی</p></div>
<div class="m2"><p>وحی است هر سخن که رود بر زبان تو!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از بهر آنکه هست‌ گمان تو چون یقین</p></div>
<div class="m2"><p>هرگز مرا غلط نرود در گمان تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ایزد به آشکار و نهان یار توست از آنک</p></div>
<div class="m2"><p>با آشکار توست برابر نهان تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پشت ولایت است و پناه شریعت است</p></div>
<div class="m2"><p>شمشیر تیز و بازوی کشورستان تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جایی نماند در همه عالم به شرق و غرب</p></div>
<div class="m2"><p>کانجا نشد به مردی نام و نشان تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خاک است و باد بر سر و برکف عَدوت را</p></div>
<div class="m2"><p>زان آب رنگ خنجر آتش فشان تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گویی خلیفهٔ دم عیسی مریم است</p></div>
<div class="m2"><p>در بارگاه و مجلس کلک و بنان تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گویی ز حَربهٔ ملک الموت نایب است</p></div>
<div class="m2"><p>در کارزار و معرکه تیغ و سنان تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سَعدست هرکجا که‌ گران شد رکاب تو</p></div>
<div class="m2"><p>فتح است هر کجا که سبک شد عنان تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بس دشمن سبک سر با لشکر گران</p></div>
<div class="m2"><p>کاخر سبک شکست زگرزِگران تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جان پرورد کسی‌ که بنوشد شراب تو</p></div>
<div class="m2"><p>فخر آورد کسی‌که نشیند به‌خوان تو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وان را که هست بر سر خوان مدح‌خوان هزار</p></div>
<div class="m2"><p>خواهد که روز رزم بود مدح خوان تو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هستند امتی همه از اعتقاد دل</p></div>
<div class="m2"><p>بعد از خدای عزوجل در ضمان تو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون حاجتی بود ز تو خواهیم و از خدای</p></div>
<div class="m2"><p>زیرا که بندگان خداییم و آن تو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>با طبع جود پرور تو سازگار باد</p></div>
<div class="m2"><p>هرمی‌ که از پیاله شود در دهان تو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پیوسته باد گنج طرب‌ زیر مهر تو</p></div>
<div class="m2"><p>همواره باد اسب ظفر زیر ران تو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بادا طراز دولت و رخسار خسروان</p></div>
<div class="m2"><p>دایم بر آستین تو و آستان تو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از روزگار باد تو را صد هزار شکر</p></div>
<div class="m2"><p>ای صد هزار جان همه پیوند جان تو</p></div></div>