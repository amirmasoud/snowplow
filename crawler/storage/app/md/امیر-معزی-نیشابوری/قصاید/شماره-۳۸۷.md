---
title: >-
    شمارهٔ ۳۸۷
---
# شمارهٔ ۳۸۷

<div class="b" id="bn1"><div class="m1"><p>جشنی‌ است بس مبارک عیدی‌ است بس همایون</p></div>
<div class="m2"><p>بر شهریار گیتی فرخنده باد و میمون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاهی که طلعت او هر روز بندگان را</p></div>
<div class="m2"><p>عیدی بود مبارک جشنی بود همایون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنجا که هست ‌کامش باکام اوست دولت</p></div>
<div class="m2"><p>وانجا که هست رایش بارای اوست گردون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا آخته است خنجر پرداخته است ‌گیتی</p></div>
<div class="m2"><p>از دشمنان مُفْسِد وز حاسدان ملعون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر سال ایزد او را ملکی دهد دگرسان</p></div>
<div class="m2"><p>هر ماه دولت او را فتحی دهد دگرگون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گه ‌گرد لشکر او خیزد ز آبِ دجله</p></div>
<div class="m2"><p>گه ماه رایت او تابد زآب جیحون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بِفْزود دانش او بر دانش سکندر</p></div>
<div class="m2"><p>بگذشت بخشش او از بخشش فریدون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با عدل او نماند جور و فساد و آفت</p></div>
<div class="m2"><p>با تیغ او نپاید بند و طلسم و افسون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در دولتش چه‌ گویم کز وصف هست برتر</p></div>
<div class="m2"><p>در همتش چه‌ گویم کز وهم هست بیرون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بحری است دست رادش بحری‌ که موج او در</p></div>
<div class="m2"><p>ابری است تیغ تیزش ابری که قطر او خون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از رنگ وز نمایش نیلوفرست تیغش</p></div>
<div class="m2"><p>نیلوفری ندیدم کز وی دمد طَبَرْ‌خون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای خسروی که رادی بر دست توست عاشق</p></div>
<div class="m2"><p>ای عادلی که شادی بر طبع توست مفتون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ملت به‌توست قایم همچون عَرَض به جوهر</p></div>
<div class="m2"><p>دولت به توست باقی همچون رَصَد به قانون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در خاک همچو قارون رفتند دشمنانت</p></div>
<div class="m2"><p>نشگفت اگر بر آری از خاک‌ گنج قارون</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گشتست عید فرخ با ماه دی موافق</p></div>
<div class="m2"><p>در بزمگاه عالی باید دو آتش اکنون</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از گونهٔ یک آتش پرلاله ‌گشت ساغر</p></div>
<div class="m2"><p>از قوت یک آتش پر لعل‌ گشت‌ کانون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شاها به این دو آتش بِفزْ‌ای شادکامی</p></div>
<div class="m2"><p>وز عکس هر دو آتش بِفْروز روی هامون</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دایم شنیده باداگوشت سماعِ مُطرب</p></div>
<div class="m2"><p>دایم گرفته بادا دستت شراب گلگون</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در جشن دین سِماطت چون جشن دین مبارک</p></div>
<div class="m2"><p>بر ماه نو نشاطت چون ماه نو در افزون</p></div></div>