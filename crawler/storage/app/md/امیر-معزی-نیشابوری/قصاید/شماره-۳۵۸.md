---
title: >-
    شمارهٔ ۳۵۸
---
# شمارهٔ ۳۵۸

<div class="b" id="bn1"><div class="m1"><p>گفتم مرا بوسه ده ای ماهِ دلستان</p></div>
<div class="m2"><p>گفتا که ماه بوسه که را داد در جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم فروغ روی تو افزون بود به شب</p></div>
<div class="m2"><p>گفتا به شب فروغ دهد ماه آسمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم به یک مکا‌نت نبینم به یک قرار</p></div>
<div class="m2"><p>گفتا که مه قرار نگیرد به ‌یک مکان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم‌ که از خط تو فغان است خلق را</p></div>
<div class="m2"><p>گفتا خسوف ماه بود خلق را فغان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم نشان آبله بر روی تو چراست</p></div>
<div class="m2"><p>گفتا بود هر آینه بر روی مه نشان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم چرا گشاده نداری دهان و لب</p></div>
<div class="m2"><p>گفتا که مه‌ گشاده ندارد لب و دهان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم که‌گلستان شگفت است بر رخت</p></div>
<div class="m2"><p>گفتا شگفت باشد بر ماه ‌گلستان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم رخ تو راه قلندر به من نمود</p></div>
<div class="m2"><p>گفتا که ماه راه نماید به کاروان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتم ز چهرهٔ تو تنم را زیان رسید</p></div>
<div class="m2"><p>گفتا ز ماه تار قصب‌ را بود زیان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتم عجب بود که در آغوش ‌گیرمت</p></div>
<div class="m2"><p>گفتا که بس عجب نبود ماه درکمان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتم ‌که بر کف تو ستاره است جام می</p></div>
<div class="m2"><p>گفتا که با ستاره بود ماه را قرآن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفتم قِرانِ ماه و ستاره به هم ‌کجا است</p></div>
<div class="m2"><p>گفتا به بزمگاه وزیر خدایگان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفتم نظام دین عرب، داور عجم</p></div>
<div class="m2"><p>گفتا که فخر ملک زمین صاحب زمان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفتم که سَیّدالوزرا صدر روزگار</p></div>
<div class="m2"><p>گفتا مظفربن حسن فخر دودمان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفتم مظفری به همه وقت کامکار</p></div>
<div class="m2"><p>گفتا موفقی به همه ‌کار کامران</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفتم ز خاندان پدر کس چو او نخاست</p></div>
<div class="m2"><p>گفتا که اوست واسطهٔ عِقْد خاندان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفتم جهان ستاند و داد جهان دهد</p></div>
<div class="m2"><p>گفتا وزیر دادْ دِه است و جهانْ ستان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفتم گمانِ کس نرسد در مناقبش</p></div>
<div class="m2"><p>گفتا که در مناقب او گم شود گمان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گفتم به عقل و جود و هنر یافت مَنزِلت</p></div>
<div class="m2"><p>گفتا که منزلت نتوان یافت رایگان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گفتم که مملکت نبود تازه جز بدو</p></div>
<div class="m2"><p>گفتا که کالبد نبود زنده جز به جان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفتم که چاره نیست ز عدلش زمانه را</p></div>
<div class="m2"><p>گفتا که جسم را نبود چاره از روان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گفتم که عدل او ز کجا تا کجا رسد</p></div>
<div class="m2"><p>گفتا ز قندهار رسد تا به قیروان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گفتم ستاره‌وار زند روز رزم رای</p></div>
<div class="m2"><p>گفتا مَجّره‌وار نهد روز بزم خوان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گفتم به هند برحذر از رای اوست رای</p></div>
<div class="m2"><p>گفتا به تُرک با حسد از خوان اوست خان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گفتم‌ کند به حزم ز سنجاب سنگ سخت</p></div>
<div class="m2"><p>گفتا کند به عزم ز پولاد پرنیان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گفتم اجل به رزمگهش گوید الحذر</p></div>
<div class="m2"><p>گفتا اَمل به بزمگهش گوید الامان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گفتم که بر عدوش قضا هست‌ کینه‌ور</p></div>
<div class="m2"><p>گفتا که بر ولیش قدر هست مهربان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفتم خلاف او به دل اندر چو آتش است</p></div>
<div class="m2"><p>گفت آتشی که مغز بسوزد در استخوان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گفتم بر آن زمین که خلافش گذر کند</p></div>
<div class="m2"><p>گفتا خراب و پست شود شهر و خاندان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گفتم ز بیمش شیر بغرد به مرغزار</p></div>
<div class="m2"><p>گفتا ز تیرش مرغ نپرد ز آشیان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گفتم که چیست اشک و لب و روی دشمنش</p></div>
<div class="m2"><p>گفتاکه آب معصفر و نیل و زعفران</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گفتم ‌که چیست خون عدو بر حُسام او</p></div>
<div class="m2"><p>گفتا که بر بنفشه پراکنده ارغوان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گفتم چه ‌کرد دور فلک با مخالفش</p></div>
<div class="m2"><p>گفتا همان که باد خزان کرد با رزان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گفتم که چون شود عدوی او به عاقبت</p></div>
<div class="m2"><p>گفتا شود هلاک چو بهمان و چون فلان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گفتم چه وقت غاشیهٔ او کشد ظفر</p></div>
<div class="m2"><p>گفتا چو اسب بادتک آرد به زیر ران</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گفتم شود به سعد عنانش همی سبک</p></div>
<div class="m2"><p>گفتا شود به فتح رکابش همی‌گران</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گفتم همه به فتح‌ کند پای در رکاب</p></div>
<div class="m2"><p>گفتا همه به سعد زند دست در عنان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گفتم چه‌ کرد کلک چو بشنیند نام او</p></div>
<div class="m2"><p>گفتا که بنده‌وار کمر بست بر میان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گفتم بنان او گه توقیع ساحر است</p></div>
<div class="m2"><p>گفتا مگر ز سحر بنا کرد بر بنان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گفتم ز امتحان کف او هست بی‌نیاز</p></div>
<div class="m2"><p>گفتا که بی‌نیاز بود بحر زامتحان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گفتم که هست‌ کلکش چون خیزران به بحر</p></div>
<div class="m2"><p>گفتا بلی به بحر بود جای خیزران</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گفتم که از جنان همه شادی خبر دهند</p></div>
<div class="m2"><p>گفتا که کرد مجلس او آن خبر عیان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گفتم که باد برکف او هست سلسبیل</p></div>
<div class="m2"><p>گفتا که سلسبیل عجب نیست در جنان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گفتم که جای جود و سخا دست و طبع اوست</p></div>
<div class="m2"><p>گفتا که جای زر و گهر معدن است و کان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گفتم بود به ‌بخشش او ابر در بهار</p></div>
<div class="m2"><p>گفتا نباشد ابر گهربار و درفشان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گفتم ندیم مجلس او هست بی نَدَم</p></div>
<div class="m2"><p>گفتا هوای خدمت او هست بی‌هَوان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گفتم به جودکرد سپه را رهین شکر</p></div>
<div class="m2"><p>گفتا چنین کنند بزرگان کاردان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گفتم‌که تافت همت او بر جوان و پیر</p></div>
<div class="m2"><p>گفتا که مهر تابد بر پیر و بر جوان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گفتم بتافت بر سر من نور آفتاب</p></div>
<div class="m2"><p>گفتا که بر سر تو قضا بود سایبان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گفتم زمدح اوست مرا پرگهر ضمیر</p></div>
<div class="m2"><p>گفتا ز شُکر اوست مرا پُر شَکَر زبان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گفتم که مدح‌گوی و ثناخوان او بسی است</p></div>
<div class="m2"><p>گفتا که چون تو نیست ثناگوی و مدح‌خوان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گفتم چنین قصیده کس از شاعران نگفت</p></div>
<div class="m2"><p>گفتا که گفت عنصری استاد شاعران</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گفتم که آن قصیده بدیع است و نادرست</p></div>
<div class="m2"><p>گفتا که این قصیده بسی بهترست از آن</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>گفتم به مدح خواجه روان است شعر من</p></div>
<div class="m2"><p>گفتا سزد که دارد مرسوم تو روان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گفتم سخاش داد مرا وعده در بهار</p></div>
<div class="m2"><p>گفتا کَفَش وفا کند آن وعده در خزان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گفتم‌که تا زشمس بود بر فلک اثر</p></div>
<div class="m2"><p>گفتا که تا ز بحر بود بر زمین نشان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>گفتم مباد شمس معالیش را زوال</p></div>
<div class="m2"><p>گفتا مباد بحر معانیش را کران</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>گفتم که شادمانی او باد پایدار</p></div>
<div class="m2"><p>گفتا که زندگانی او باد جاودان</p></div></div>