---
title: >-
    شمارهٔ ۳۷۹
---
# شمارهٔ ۳۷۹

<div class="b" id="bn1"><div class="m1"><p>یک امشب زبهر من ای ساربان</p></div>
<div class="m2"><p>زدروازه بیرون مَبَر کاروان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درنگی بکن تا من از جان و دل</p></div>
<div class="m2"><p>ز جانان و دلبر بپرسم نشان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که تیمار دلبر ز من برد دل</p></div>
<div class="m2"><p>که اندوه جانان ز من برد جان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زمن جان و دل چونکه بیرون شدست</p></div>
<div class="m2"><p>به دروازه بیرون شدن چون توان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر امشب درنگی نباشد تورا</p></div>
<div class="m2"><p>زچشمم رسد همرهان را زیان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به کشتی بود کاروان را نیاز</p></div>
<div class="m2"><p>که دریا شدست از دو چشمم روان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من امشب همی بر سر کوی یار</p></div>
<div class="m2"><p>زبهر دل و جان بر آرم فغان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگر بر من عاشق مستمند</p></div>
<div class="m2"><p>شود مهربانْ یارِ نامهربان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر یابم از یار مقصود خویش</p></div>
<div class="m2"><p>به مقصد رسم با تو ای ساربان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میان چون کمرکرده از عشق یار</p></div>
<div class="m2"><p>همه راه بندم‌ کمر بر میان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نهادست بار گران بر دلم</p></div>
<div class="m2"><p>ز تیمار خویش آن بت دلستان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هَیونی سبک پای باید مرا</p></div>
<div class="m2"><p>که رنجه نگردد ز بار گران</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هیونی‌ که‌ گویی بر اعضای او</p></div>
<div class="m2"><p>مفاصل بپیوست بی‌استخوان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هیونی چو دیوانه دیوی زبند</p></div>
<div class="m2"><p>برون خسته مانند تیر از کمان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چوگیرند شیران مهارش به‌دست</p></div>
<div class="m2"><p>خرد را چنان باشد اندر گمان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که ثُعبان همی طور سینا کند</p></div>
<div class="m2"><p>به اعجاز دستِ کلیمِ شبان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو ازگام او بر ره‌ کوفته</p></div>
<div class="m2"><p>شود شکلهای مدوّر عیان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو گویی بنایی عیان کرد چرخ</p></div>
<div class="m2"><p>هزاران قمر بر ره و کهکشان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو تابد ستاره زگردون پیر</p></div>
<div class="m2"><p>بدو گویم ای بیسُراک جوان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چراگاه تو تازه و سبز باد</p></div>
<div class="m2"><p>همه خار او چون‌ گل و ارغوان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دَرای تو از زرّ و یاقوت باد</p></div>
<div class="m2"><p>هُوَید تو از حُلّه و پرنیان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تویی تیزرو مرکبِ بی‌رکاب</p></div>
<div class="m2"><p>تویی زود دو بارهٔ بی‌عنان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همی‌ گوش دارم نفر تا نفر</p></div>
<div class="m2"><p>همی چشم دارم زمان تا زمان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که تو با سلامت رسانی مرا</p></div>
<div class="m2"><p>به درگاه دستور شاه جهان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پناه عجم صدر دین عرب</p></div>
<div class="m2"><p>دل دولت و دیدهٔ دودمان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>محمد روان تن مَحْمِدَت</p></div>
<div class="m2"><p>کز او شاد دارد محمد روان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>وزیری‌ که بشکفت از او روی ملک</p></div>
<div class="m2"><p>چو از فرّ باد صبا بوستان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رهی شد جهان پیش توقیع او</p></div>
<div class="m2"><p>رها کرد توقیع نوشیروان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>رسد مرد بر سِدرَهُٔ المُنتهی</p></div>
<div class="m2"><p>اگر سازد از رای او نردبان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سخارا به خورشید و دریا و ابر</p></div>
<div class="m2"><p>همی زد خرد پیش از این داستان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کنون تا دل و دست و طبعش بدید</p></div>
<div class="m2"><p>بدان داستان نیست همداستان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به تدبیر او کرد صافی ملک</p></div>
<div class="m2"><p>از اهل ستم خانه و ملک خان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به باغ مرادش درخت ظفر</p></div>
<div class="m2"><p>ز چین سایه گسترد تا قیروان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خطی داد گردون به‌ اقبال او</p></div>
<div class="m2"><p>که هستند سیارگان در ضَمان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اگر زهره و مشتری را دهد</p></div>
<div class="m2"><p>جهان آفرین دست و نطق و زبان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به بزمش کند زهره رامشگری</p></div>
<div class="m2"><p>به خوانش شود مشتری مدح خوان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سزد میهمان شهریار زمین</p></div>
<div class="m2"><p>کجا همت او بود میزبان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اگر میزبان دولت او بود</p></div>
<div class="m2"><p>سزد آفتاب فلک میهمان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چنین منصبی را که او یافته است</p></div>
<div class="m2"><p>ز پروردگار و ز صاحب قران</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کمال حَسَبْ باید از نفس پاک</p></div>
<div class="m2"><p>جمال نسب باید از خاندان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اثر باید از جنبش روزگار</p></div>
<div class="m2"><p>مدد باید ازگردش آسمان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بزرگی نیابد کسی بر گزاف</p></div>
<div class="m2"><p>وزارت نیابد کسی رایگان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ایا کامگاری که از رای توست</p></div>
<div class="m2"><p>ملک بر ملوک عجم کامران</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به پیکار خصم تو غرد همی</p></div>
<div class="m2"><p>کجا هست در بیشه شیر ژیان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز منقار باز تو ترسد همی</p></div>
<div class="m2"><p>اگر هست سیمرغ در آشیان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سعادت در آن خانه گیرد کمین</p></div>
<div class="m2"><p>که عرضت در آن خانه گیرد مکان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بود روز و شب بر در و بام او</p></div>
<div class="m2"><p>قضا و قدر حاجب و پاسبان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>زمانه ز بهر تو آرد پدید</p></div>
<div class="m2"><p>همی زر وگوهر ز دریا و کان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به دریا و کان هر دو را مدّتی</p></div>
<div class="m2"><p>ز اِسْراف جُود تو دارد نهان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به نام تو یک بیت تضمین‌ کنم</p></div>
<div class="m2"><p>که منصورگفته است در باستان‌:</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>«‌درم از کف تو به نزع آمدست</p></div>
<div class="m2"><p>شهادت نهندش همی در دهان‌)‌)</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>زمین و زمان از تو نازد همی</p></div>
<div class="m2"><p>که سعد زمینی و صدر زمان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بمان جاودان در جهان همچنین</p></div>
<div class="m2"><p>وگرچه نماند جهان جاودان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نبودست مشکین دُخانِ چراغ</p></div>
<div class="m2"><p>چراغی است کلک تو مشکین دخان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>میان ضمیر و خرد واسطه است</p></div>
<div class="m2"><p>میان زبان و خرد ترجمان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>اگر حاجب و حاکم ملک نیست</p></div>
<div class="m2"><p>چرا با کمر باشد و طیلسان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>مقیم است چون خیزران و صدف</p></div>
<div class="m2"><p>به بحری که هرگز ندارد کران</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به بحری که دارد مکین یافته است</p></div>
<div class="m2"><p>دهان از صدف قامت از خیزران</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بلند اخترا تا بدیدم تو را</p></div>
<div class="m2"><p>مرا داد اختر ز حرمان امان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>به نوروز رفتم ز درگاه تو</p></div>
<div class="m2"><p>به درگاه باز آمدم مهرگان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>کنون هست با تو خزانم بهار</p></div>
<div class="m2"><p>اگر بود بی‌تو بهارم خزان</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ز طبع من اقبال در مدح تو</p></div>
<div class="m2"><p>قبول تو این شعرکرد امتحان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>معانی همه صافی از مستعار</p></div>
<div class="m2"><p>قوافی همه خالی از شایگان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>مسلم کسی را بود شاعری</p></div>
<div class="m2"><p>که دارد چنین ساحری در بیان</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>اگر چه معزّی لقب یافتم</p></div>
<div class="m2"><p>زسلطان ملکشاه آلب ارسلان</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چو در مدح تو شعر من معجزست</p></div>
<div class="m2"><p>مرا معجزی خوان معزی مخوان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>همی تا هوی باهوا همبرست</p></div>
<div class="m2"><p>در اختر چنین است آیین و سان</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>جهان را به مهر و بقای تو باد</p></div>
<div class="m2"><p>هوایی که هرگز نبیند هَوان</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>زتدبیر تو ملک را جاه و آب</p></div>
<div class="m2"><p>ز توقیع تو خلق را نام و نان</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ملک شادمان از تو و رای تو</p></div>
<div class="m2"><p>تو از دولت و رأی او شادمان</p></div></div>