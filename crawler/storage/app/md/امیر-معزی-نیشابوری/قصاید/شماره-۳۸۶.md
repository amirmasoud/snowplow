---
title: >-
    شمارهٔ ۳۸۶
---
# شمارهٔ ۳۸۶

<div class="b" id="bn1"><div class="m1"><p>خدایا دور کن چشم بد از این دولت میمون</p></div>
<div class="m2"><p>وزین شاه مبارک رای ملک آرای روزافزون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شه‌شرق ارسلان ارغو که هست اندر جهانداری</p></div>
<div class="m2"><p>به بهروزی چو اسکندر به بیروزی چو افریدون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هر ماهی که نوگردد نصیب او ز هفت اختر</p></div>
<div class="m2"><p>یکی ملک است دیگر سان یکی فتح است دیگرگون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خردمندان دولت را به تاریخ فتوح اندر</p></div>
<div class="m2"><p>شگفتیها زیادت گشت ازین معنی که رفت اکنون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ملک چون سوی مرو آمد سپه را داد دستوری</p></div>
<div class="m2"><p>به قهر و غارتِ گردن‌کشانِ مفسد و ملعون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سپاه او همه بودند شیران و جهانگیران</p></div>
<div class="m2"><p>ظفر بر تیغشان عاشق هنر بر طبعشان مفتون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زدوده تیغها اندر کف ایشان چو نیلوفر</p></div>
<div class="m2"><p>شده نیلوفر از خون بداندیشان چو آذ‌ریون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زمین از عکس خنجرشان شده مانند بیجاده</p></div>
<div class="m2"><p>هوا از رنگ مِطرَدشان شده مانند بوقلمون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غریو و نعرهٔ ایشان به سوی مه شد از ماهی</p></div>
<div class="m2"><p>تبه شد بدسگالان را طلسم و تَنبُل و افسون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکی شد مرده در بیشه یکی شد کشته در وادی</p></div>
<div class="m2"><p>یکی شد خسته بر بالا یکی شد بسته بر هامون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکی را باد در حنجر ز تلخی‌ گشت چون حَنظل</p></div>
<div class="m2"><p>یکی را مغز در تارک زسردی گشت چون افیون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکی را شد به طبع اندر ز فکرت شادمانی غم</p></div>
<div class="m2"><p>یکی را شد به چشم اندر زحیرت روشنایی خون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برفتند از میانشان چندکس آشفته و حیران</p></div>
<div class="m2"><p>بجستند از میانشان چند تن بیچاره و محزون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زهی رای سر شاهان زهی عزم شه ایران</p></div>
<div class="m2"><p>مدار دولت عالی دلیل طالع میمون</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دلیران را به‌کردار زبونان خسته جان و تن</p></div>
<div class="m2"><p>امیران را به کردار اسیران کرده خوار و دون</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر لشکرکشد زایدر شه مشرق به ترکستان</p></div>
<div class="m2"><p>خطر جفت ختاگردد بلا جفت بلاساغون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سپاهش در خراسان است و سَهمش بر لب دجله</p></div>
<div class="m2"><p>رکابش در نشابورست و بیمش بر لبِ جیحون</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تف تیغ و دم خشمش همیشه بر بد اندیشان</p></div>
<div class="m2"><p>به‌سان دعوت موسی است بر هامان و بر قارون</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یکی را تیغ او در آب با هامان کند همبر</p></div>
<div class="m2"><p>یکی را خشم او در خاک با قارون ‌کند مقرون</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همیشه روزگارِ خسروِ مشرق چنین خواهم</p></div>
<div class="m2"><p>سعادت را شده تاریخ و دولت را شده قانون</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خدایش ناصر و رهبر سپهرش بنده و چاکر</p></div>
<div class="m2"><p>حسودش هر زمان‌کمتر فتوحش هر زمان افزون</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ولی در حفظ فرمانش عزیز از طالع فرخ</p></div>
<div class="m2"><p>عدو در بند و زندانش ذلیل از اختر وارون</p></div></div>