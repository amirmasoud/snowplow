---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>از زین‌ دین عراق و خراسان مزین است</p></div>
<div class="m2"><p>این را دلیل ظاهر و حجت مبرهن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حاجت نیایدش به‌ دلیلی و حاجتی</p></div>
<div class="m2"><p>کاقبال او شناخته چون روز روشن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر قدّ بخت او سَلَبی دوخته است چرخ</p></div>
<div class="m2"><p>کاورا ز مهر و ماه‌ گریبان و دامن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با دوستان گنبد دوار هست دوست</p></div>
<div class="m2"><p>با دشمنانش کوکب سَیّار دشمن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از خصم ایمن است‌ که پشت و پناه او</p></div>
<div class="m2"><p>شاهنشه سپه‌شکنِ دشمن افکن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آتش ز بیم آنکه بسوزد ز خشم او</p></div>
<div class="m2"><p>ترسیده و گریخته در سنگ و آهن است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از بهر مهر و خدمت او کهترانش را</p></div>
<div class="m2"><p>تن عاشق روان و روان عاشق تن است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شعری که من به حضرت او عرضه کرده‌ام</p></div>
<div class="m2"><p>در شرق و غرب تا به‌ قیامت مدون است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من‌ گفتم آنکه بر دل او بود گاه مدح</p></div>
<div class="m2"><p>او نیز آن کند که در اندیشهٔ من است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کارش به کام باد و جهانش مدام باد</p></div>
<div class="m2"><p> زیرا که عالمی به جما‌لش مزین است</p></div></div>