---
title: >-
    شمارهٔ ۳۰۹
---
# شمارهٔ ۳۰۹

<div class="b" id="bn1"><div class="m1"><p>ای به توفیق و هدایت دین یزدان را قوام</p></div>
<div class="m2"><p>وی به تدبیر و کفایت ملک سلطان را نظام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخت تو عالی و مقدار تو عالیتر ز بخت</p></div>
<div class="m2"><p>نام تو نیکو و کردار تو نیکوتر ز نام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر زمین آزادگان در خدمت تو بی‌ملال</p></div>
<div class="m2"><p>بر فلک سیارگان در بیعت تو بی‌ملام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فطرت تو دست مکاران درآورده به‌ بند</p></div>
<div class="m2"><p>قدرت تو پای جباران درآورده به‌ دام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن وزیری تو که هست اندر صلاح مملکت</p></div>
<div class="m2"><p>چرخ سرکش با تو نرم و دهر توسن با تو رام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو یاقوت از جواهر اختیاری از بشر</p></div>
<div class="m2"><p>همچو خورشید از کواکب نامداری از انام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرد شادُرْوان تو نورست در چشم کفات</p></div>
<div class="m2"><p>نعل رهواران تو تاج است بر فرق‌ کرام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صاحب صد لشکرست از حضرت تو یک رسول</p></div>
<div class="m2"><p>نایب صد خنجرست از مجلس تو یک پیام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یاد تو نوشد همی‌ گردون که هستش روز و شب</p></div>
<div class="m2"><p>فیض یزدانی شراب و چشمهٔ خورشید جام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بخت بیدار تو را گر صورتی پیدا شود</p></div>
<div class="m2"><p>نقش آن صورت بود تفسیر حی لا ینام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اندر احیا بس بود دست جواد تو جواب</p></div>
<div class="m2"><p>ملحدانی راکجاگویند من یحیی‌العظام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گَر مدد یابد ز جود دست تو بحر محیط</p></div>
<div class="m2"><p>تا قیامت زو همی زرین مَطَر خیزد غمام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از قلم در دست تو فعل حُسام آمد پدید</p></div>
<div class="m2"><p>دیده‌کس مصری قلم را قدرت زرین حسام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا روان باشد علمهای تو را فتح و ظفر</p></div>
<div class="m2"><p>تیغهای جنگیان آسوده باشد در نیام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رایت عالی به ترکستان کشیدی از عراق</p></div>
<div class="m2"><p>تا تکینان را رهی‌کردی و خانان را غلام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا هوا چون بوستان کردی ز گوناگون علم</p></div>
<div class="m2"><p>تا زمین چون آسمان کردی ز گوناگون خیام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا گشادی قلعه‌هایی را که مر هر قلعه را</p></div>
<div class="m2"><p>پشت ماهی هست بوم و برج ماهی هست بام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون ‌کشیدی زیر فرمان از حلب تا کاشغر</p></div>
<div class="m2"><p>مصلحت جستی و در یک جای فرمودی مقام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر نه ابر رحمت‌ تو آب بر آتش زدی</p></div>
<div class="m2"><p>خاک ترکستان ز تیغ شاه‌گشتی لعل فام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>حاسدان را از شکم بر پشت بگذشتی رَماح</p></div>
<div class="m2"><p>دشمنان را از قضا بر حلق بگذشتی سَهام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از شرار تیغ بودی پادشاهان را شراب</p></div>
<div class="m2"><p>وز طِعان رُمْح بودی خاکساران را طَعام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پنجهٔ شیران بخستی‌ گردن و پشت‌ گوزن</p></div>
<div class="m2"><p>چنگل بازان بکندی سینه و چشم حمام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اینت‌ عفو بی‌نهایت و ینت علم بی‌قیاس</p></div>
<div class="m2"><p>اینت فضل بر تواتر وینت شکر بر دوام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای خداوندی که اندر ملک توران کرده‌ای</p></div>
<div class="m2"><p>هم بدین سان‌ کرده‌ای در ملک روم و ملک شام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خشم‌ تو شامی‌ است از محنت‌ که آن را نیست صبح</p></div>
<div class="m2"><p>عفو تو صبحی است از نعمت‌که آن را نیست شام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر نبودی شکر تو پیوند جان ایدر بدن</p></div>
<div class="m2"><p>خلق عالم را همه شکر تو رفتی از مسام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هر که بیند مر تو را داند که صدر عالمی</p></div>
<div class="m2"><p>جهل باشد گر کسی خورشید را گوید کدام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>وهم تو نیرنگ محتالان بیوبارد همی</p></div>
<div class="m2"><p>همچنان چون آتش سوزان بیوبارد ثغام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مهر پیروزی نگارد بر نگین عمر خویش</p></div>
<div class="m2"><p>هرکه یک شب صورت عمر تو بیند در منام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو همامی و قلم در دست تو همچون همای</p></div>
<div class="m2"><p>هست روز ما همایون از همای و از همام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا همی بارد قلم در دست تو سحر حلال</p></div>
<div class="m2"><p>برخلاف تو قدم بر داشتن باشد حرام</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا به جاه تو همه اسلامیان را حاجت است</p></div>
<div class="m2"><p>بر سلامت حجتی باشد تو را کردن سلام</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هرکه او از چنبر حکم تو سر بیرون‌کشد</p></div>
<div class="m2"><p>از شریف و دون و از نیک و بد و از خاص و عام</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کردگارش کرد مخذول و تو مستغنی ز جنگ</p></div>
<div class="m2"><p>روزگارش کرد مقهور و تو فارغ ز انتقام</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>وانکه او جان و دل اندر عُهدهٔ عَهد تو کرد</p></div>
<div class="m2"><p>یافت از یزدان و از سلطان قبول و احتشام</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کردی اندر پیش یزدان کار او را تربیت</p></div>
<div class="m2"><p>داشتی در پیش سلطان شغل او را اهتمام</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>رای نیک و رسم خوب توست در دنیا و دین</p></div>
<div class="m2"><p>نیک‌بختی را ثواب و رادمردی را قوام</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ای ثناهای تو چون تعویذ کرده هر حکیم</p></div>
<div class="m2"><p>وی دعاهای تو چون تسبیح کرده هر امام</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تا به میدان سخن بر مدح توگشتم سوار</p></div>
<div class="m2"><p>مرکب شعر من از شعر‌ی همی خواهد لگام</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>از گهرهای مدیح تو قلم در دست من</p></div>
<div class="m2"><p>گردن ایام را عِقْدی همی سازد مدام</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کلک گوهربار تو پر گوهرم ‌کردست طبع</p></div>
<div class="m2"><p>لفظ شکربار تو پرشکرم کرده است کام</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تا فساد و کون باشد فلسفی را در مثال</p></div>
<div class="m2"><p>تا ظلام و نور باشد مانوی را در کلام</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>در جلالت باد کون دولت تو بی‌فساد</p></div>
<div class="m2"><p>در وزارت باد نور حشمت تو بی‌ظلام</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تو به شکر از کردگار و کردگار از تو به شکر</p></div>
<div class="m2"><p>تو به‌ کام از شهریار و شهریار از تو به ‌کام</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سرو عمر تو همیشه خرم و سبز و بلند</p></div>
<div class="m2"><p>ماه بخت تو همیشه روشن و بدر تمام</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>از تو اندر ملک سلطان هم صلاح و هم صواب</p></div>
<div class="m2"><p>وزتو اندر دین یزدان هم صلوت و هم‌صیام</p></div></div>