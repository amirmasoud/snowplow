---
title: >-
    شمارهٔ ۲۵۲
---
# شمارهٔ ۲۵۲

<div class="b" id="bn1"><div class="m1"><p>ای امیر مظفر منصور</p></div>
<div class="m2"><p>ای چو خورشید در جهان مبثبهور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تاج دینی و دین ز دولت تو</p></div>
<div class="m2"><p>هست روشن چنانکه چشم از نور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هست بوالفضل کنیت تو به حق</p></div>
<div class="m2"><p>که به فضل و فضایلی مذکور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نصر نام تو نیز هست سزا</p></div>
<div class="m2"><p>که تویی بر مخالفان منصور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رایت پادسا به تیغ تو تیز</p></div>
<div class="m2"><p>هست منصور تا دمیدن صور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر تو تازی ز نیمروز به چین</p></div>
<div class="m2"><p>بگریزد به نیم شب فَغْفُور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ور نهی روی سوی کشور روم</p></div>
<div class="m2"><p>قیصران را بیاوری ز قصور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ور به هندوستان کَشی سپهی</p></div>
<div class="m2"><p>کنی از عقل و رای رآی نفور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بستانی همه ولایت رآی</p></div>
<div class="m2"><p>چون سکندر همه ولایت فور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون شود تیغ با کفت موصول</p></div>
<div class="m2"><p>تن دشمن ز جان شود مهجور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تیغ تو هست قاهری که‌ کند</p></div>
<div class="m2"><p>صد سپه را به یک زمان مقهور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نایب است از قضا که درگهِ رزم</p></div>
<div class="m2"><p>خصم مختار را کند مجبور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر زمین آورد دزی که بود</p></div>
<div class="m2"><p>بحر وکوهش به جای خندق و سور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حکم تو خاتم سلیمان است</p></div>
<div class="m2"><p>مرکب توست چون صبا و دبور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همچو دیو و پری مطیع تو اند</p></div>
<div class="m2"><p>بر زمین و هوا وحوش و طیور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در پناه تو چیرگی نکند</p></div>
<div class="m2"><p>باز بر کبک و باشه بر عصفور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پیش لطف تو باد نیست لطیف</p></div>
<div class="m2"><p>پیش صبر تو کوه نیست صبور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زیر قدر تو آفرید خدای</p></div>
<div class="m2"><p>هر بلندی که هست در مقدور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>راست‌گویی ز مهر وکین تو خاست</p></div>
<div class="m2"><p>نوش و نیش از سر و بن زنبور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فلک رابع است لشکرگاه</p></div>
<div class="m2"><p>خیمهٔ توست خانهٔ معمور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جز به تو مرتبت نگیرد خاک</p></div>
<div class="m2"><p>جز به موسی شرف نگیرد طور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دست تاریخ دولت تو نهاد</p></div>
<div class="m2"><p>افسری بر سر سنین و شهور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو به اصل و به‌ نفس محتشمی</p></div>
<div class="m2"><p>نه به توقیع و نامه و منشور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از حضور تو فر و زینت یافت</p></div>
<div class="m2"><p>حضرت شاه و مجلس دستور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عالمی خرم از حضور تواند</p></div>
<div class="m2"><p>اینت‌ فرخنده و خجسته حضور</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر صدورند در جهان بسیار</p></div>
<div class="m2"><p>جاه تو پیشتر ز جاه صدور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>فضل عاشور اگرچه بسیارست</p></div>
<div class="m2"><p>روزه فاضلتر آمد از عاشور</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خلق دنیا کنند در عقبی</p></div>
<div class="m2"><p>مکرمات تو نشر روز نشور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هرکجا صدق بخشش تو بود</p></div>
<div class="m2"><p>بخشش ابر و بحر باشد زور</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بحر شاید دل تو را شاگرد</p></div>
<div class="m2"><p>ابر زیبد کف تورا مزدور</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بوی مهر تو سازگار کند</p></div>
<div class="m2"><p>مشک را با طبیعت مَحر‌ور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ور ز طبعت برد بخور بخار</p></div>
<div class="m2"><p>بوی خُلد آید از بُخار بخور</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ای به فضل و کرم ز خالق و خلق</p></div>
<div class="m2"><p>به همه‌ وقت شاکر و مشکور</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در بهشت برین اگر داود</p></div>
<div class="m2"><p>خوانَدَی مدح تو به جای زَبور</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بر سر او فَشانَدَی رضوان</p></div>
<div class="m2"><p>حُلّه‌های بهشت و زیور حور</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>عاجز و ا‌قاصرم‌ا ز خدمت تو</p></div>
<div class="m2"><p>هست بر من نشان عجز و قصور</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سرو من شد خمیده چون چنبر</p></div>
<div class="m2"><p>مشک من شد سپید چون ‌کافور</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کاشکی نیستی تنم بیمار</p></div>
<div class="m2"><p>کاشکی نیستی دلم رنجور</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تا ز دریای طبع هر روزی</p></div>
<div class="m2"><p>ا‌باردی‌ا بر تو لؤلؤ منثور</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>با چنین حال اگر کنم تقصیر</p></div>
<div class="m2"><p>چشم دارم‌ که داریم معذور</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تا سریر و سرور جمع بود</p></div>
<div class="m2"><p>در سرایی که جشن باشد و سور</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از سرایت جدا مباد سریر</p></div>
<div class="m2"><p>وز سریرت جدا مباد سرور</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بخت تو مالک و فلک مملوک</p></div>
<div class="m2"><p>رای تو آمر و جهان مأمور</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تا به کیوان شده ز ایوانت</p></div>
<div class="m2"><p>نعرهٔ چنگ و نالهٔ تنبور</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>در دلت نور جشمهٔ خورشید</p></div>
<div class="m2"><p>بر کفت آب خوشهٔ انگور</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ساقی تو بتی‌که سرمهٔ سحر</p></div>
<div class="m2"><p>دارد اندر دو نرگس مخمور</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>آنکه با غمزهٔ فسوس برش</p></div>
<div class="m2"><p>مرد ناباخته شود مقهور</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>زلف او داده روز روشن را</p></div>
<div class="m2"><p>زره و جوشن از شب دیجور</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>جعد او نقش حسن را نقاش</p></div>
<div class="m2"><p>جسم او گنج فتنه را گَنجور</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بزم تو خُلد و او چو حورالعین</p></div>
<div class="m2"><p>تو چو رضوان و می شراب طَهُور</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تو به حسن و جمال او خرم</p></div>
<div class="m2"><p>او به جاه و جلال تو مسرور</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>همه نیکی به عمر تو نزدیک</p></div>
<div class="m2"><p>دست و چشم بدی ز عمر تو دور</p></div></div>