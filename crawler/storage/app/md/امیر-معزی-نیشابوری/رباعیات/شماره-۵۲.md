---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>هرگز دل تو به هیچکس شاد مباد</p></div>
<div class="m2"><p>وز بند تو بندهٔ تو آزاد مباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا عشق تو را دلم عمارت نکند</p></div>
<div class="m2"><p>ویران شدهٔ عشق تو آباد مباد</p></div></div>