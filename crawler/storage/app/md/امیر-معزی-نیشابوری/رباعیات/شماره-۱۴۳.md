---
title: >-
    شمارهٔ ۱۴۳
---
# شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>ای شاه زتو تخت همی نازد و زین</p></div>
<div class="m2"><p>وز دولت تو داد همی یابد دین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا روی زمین گرفته‌ای زیر نگین</p></div>
<div class="m2"><p>خصمان تو رفته‌اند در زیر زمین</p></div></div>