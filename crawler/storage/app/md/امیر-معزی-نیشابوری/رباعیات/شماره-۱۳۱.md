---
title: >-
    شمارهٔ ۱۳۱
---
# شمارهٔ ۱۳۱

<div class="b" id="bn1"><div class="m1"><p>گه سرو بلند حله بوشت خوانم</p></div>
<div class="m2"><p>گه ماه تمام باده نوشت خوانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ارزان بخری و رایگان بفروشی</p></div>
<div class="m2"><p>ارزان خر و رایگان فروشت خوانم</p></div></div>