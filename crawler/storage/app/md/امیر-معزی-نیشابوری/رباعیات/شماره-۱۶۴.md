---
title: >-
    شمارهٔ ۱۶۴
---
# شمارهٔ ۱۶۴

<div class="b" id="bn1"><div class="m1"><p>از بهر جمال چهرهٔ همچو پری</p></div>
<div class="m2"><p>دستت به سوی آینه تا چند بری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بس‌ که همی به آینه درنگری</p></div>
<div class="m2"><p>بر چهرهٔ خویشتن ز من فتنه‌تری</p></div></div>