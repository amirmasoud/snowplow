---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>ای صدر جهان هرکه می‌کین تو خورد</p></div>
<div class="m2"><p>از رنج خمارگشت با ناله و درد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر شیر سیه بود به هنگام نبرد</p></div>
<div class="m2"><p>شد عاقبت کار به حال سگ زرد</p></div></div>