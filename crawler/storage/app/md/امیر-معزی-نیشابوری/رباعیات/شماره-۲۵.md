---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>شاها اثر صبوح کاری عجب است</p></div>
<div class="m2"><p>نازد به صبوح هرکه شادی طلب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باده به همه وقت طرب را سبب است</p></div>
<div class="m2"><p>لیکن به صبوح‌ کیمیای طرب است</p></div></div>