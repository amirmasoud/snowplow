---
title: >-
    شمارهٔ ۹۹
---
# شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>چون آتش خاطر مرا شاه بدید</p></div>
<div class="m2"><p>از خاک مرا بر زبر ماه کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون آب یکی ترانه از من بشنید</p></div>
<div class="m2"><p>چون باد یکی مرکب خاصم بخشید</p></div></div>