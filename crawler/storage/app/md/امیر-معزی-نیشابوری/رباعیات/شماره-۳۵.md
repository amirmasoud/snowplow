---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>نور ملک ای ملک به نام تو درست</p></div>
<div class="m2"><p>دور فلک ای ملک به دام تو درست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کان ظفر ای ملک به‌ کام تو درست</p></div>
<div class="m2"><p>جان طرب ای ملک به جام تو درست</p></div></div>