---
title: >-
    شمارهٔ ۱۰۶
---
# شمارهٔ ۱۰۶

<div class="b" id="bn1"><div class="m1"><p>شاها خردت هست به می خوردن یار</p></div>
<div class="m2"><p>شاید که شب و روز همین داری کار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می خوردن تو فلک چو بیند هر بار</p></div>
<div class="m2"><p>خواهد که کند ستارگان بر تو نثار</p></div></div>