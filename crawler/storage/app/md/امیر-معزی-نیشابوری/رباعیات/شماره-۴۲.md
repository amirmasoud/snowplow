---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>در عشق توام امید بهروزی نیست</p></div>
<div class="m2"><p>وز عهد شب وصال تو روزی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آتش تو دلم چرا می‌سوزد</p></div>
<div class="m2"><p>چون هیچ تو را عادت دلسوزی نیست</p></div></div>