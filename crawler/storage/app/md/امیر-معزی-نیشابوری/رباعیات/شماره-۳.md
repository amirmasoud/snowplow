---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>ای تاخته از جهان جهانبانان را</p></div>
<div class="m2"><p>برهم زده ملک و خانهٔ خانان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای وارث نامدار سلطانان را</p></div>
<div class="m2"><p>فخرست به تو جمله مسلمانان را</p></div></div>