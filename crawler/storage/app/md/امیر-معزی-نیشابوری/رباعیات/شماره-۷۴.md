---
title: >-
    شمارهٔ ۷۴
---
# شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>چون شاه سوی تخت سرافراز آمد</p></div>
<div class="m2"><p>پیرامن او بخت به‌پرواز آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن روز ز تخت شاهی آواز آمد</p></div>
<div class="m2"><p>کامروز مسیح از آسمان باز آمد</p></div></div>