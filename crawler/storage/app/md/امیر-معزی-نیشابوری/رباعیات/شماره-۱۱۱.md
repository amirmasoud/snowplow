---
title: >-
    شمارهٔ ۱۱۱
---
# شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>گر بخت بلند خواهی و عمر دراز</p></div>
<div class="m2"><p>از دولت برکیارقی سر بفراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر کالبد ملوک جان یابد باز</p></div>
<div class="m2"><p>از وی همه برکیارق آید آواز</p></div></div>