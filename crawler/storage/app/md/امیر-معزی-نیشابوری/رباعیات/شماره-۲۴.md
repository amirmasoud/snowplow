---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>شاها همه آشوب ز بدخواه تو خاست</p></div>
<div class="m2"><p>دادار جز آن خواست که بدخواه تو خواست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیغمبری ملوک بی‌وحی تو راست</p></div>
<div class="m2"><p>کارت همه معجزات را ماند راست</p></div></div>