---
title: >-
    شمارهٔ ۱۶۷
---
# شمارهٔ ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>هر سال به‌کار ملک بیدار تری</p></div>
<div class="m2"><p>چون‌ گوی زنی شها و چوگان بازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر روز سخی تری و دیندارتری</p></div>
<div class="m2"><p>چوگان ز خمیده قد ایشان سازی</p></div></div>