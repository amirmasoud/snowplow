---
title: >-
    شمارهٔ ۱۱۵
---
# شمارهٔ ۱۱۵

<div class="b" id="bn1"><div class="m1"><p>شاهی که به رزم کاویان داشت درفش</p></div>
<div class="m2"><p>گر زنده شود پیش تو بردارد کفش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای کرده دل خصم خلاف تو بنفش</p></div>
<div class="m2"><p>بشت است دل خصم و خلاف تو درفش</p></div></div>