---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>ای از همه خسروان چو افریدون فرد</p></div>
<div class="m2"><p>وز دولت تو رسیده بر گردون گَرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای ‌گشته به دولت تو روزافزون مرد</p></div>
<div class="m2"><p>ایزد به تو این جهان ا‌همه‌ا میمون‌ کرد</p></div></div>