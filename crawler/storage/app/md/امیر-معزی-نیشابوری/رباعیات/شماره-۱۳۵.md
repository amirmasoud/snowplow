---
title: >-
    شمارهٔ ۱۳۵
---
# شمارهٔ ۱۳۵

<div class="b" id="bn1"><div class="m1"><p>تیغ ملک شرق خداوند جهان</p></div>
<div class="m2"><p>شیری است تنش به جمله چنگال و دهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون ناخن او باز شود با دندان</p></div>
<div class="m2"><p>در ناخن سرگیرد و در دندان جان</p></div></div>