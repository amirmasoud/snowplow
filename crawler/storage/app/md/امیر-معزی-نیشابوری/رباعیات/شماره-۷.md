---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>مهرست ظفر نگین فرمان تو را</p></div>
<div class="m2"><p>صید است بهشت دام احسان تو را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک است ستاره صحن میدان تو را</p></div>
<div class="m2"><p>گوی است زمانه خَمٌ جوگان تورا</p></div></div>