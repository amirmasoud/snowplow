---
title: >-
    شمارهٔ ۱۰۹
---
# شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>ای شاه کجا گرم کنی بارهٔ بور</p></div>
<div class="m2"><p>صدگور بیفکنی به یک بار به زور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بهمن و بهرام برآیند ازگور</p></div>
<div class="m2"><p>گویند که کار توست افکندن گور</p></div></div>