---
title: >-
    شمارهٔ ۱۰۰
---
# شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>حوران سپاهت ای شه شیر شکر</p></div>
<div class="m2"><p>در آب روان همی نمایند صُوَر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن است مرادشان که باشند مگر</p></div>
<div class="m2"><p>در خدمت مجلس تو اِستاده کمر</p></div></div>