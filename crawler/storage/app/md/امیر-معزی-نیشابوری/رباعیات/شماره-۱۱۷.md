---
title: >-
    شمارهٔ ۱۱۷
---
# شمارهٔ ۱۱۷

<div class="b" id="bn1"><div class="m1"><p>دستور و شهنشه از جهان رایت خوش</p></div>
<div class="m2"><p>بردند و مصیبتی نیامد زین بیش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس دل‌ که شدی ز مرگ شاهنشه ریش</p></div>
<div class="m2"><p>گر کشتن دستور نبودی از پیش</p></div></div>