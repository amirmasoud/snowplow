---
title: >-
    شمارهٔ ۱۳۰
---
# شمارهٔ ۱۳۰

<div class="b" id="bn1"><div class="m1"><p>هر شب ز غم تو دل ز جان برگیرم</p></div>
<div class="m2"><p>پندی که خرد دهد من آن نپذیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا روز نهد خیال تو در پیشم</p></div>
<div class="m2"><p>صد چشمه و من ز تشنگی می‌میرم</p></div></div>