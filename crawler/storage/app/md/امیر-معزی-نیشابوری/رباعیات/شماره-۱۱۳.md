---
title: >-
    شمارهٔ ۱۱۳
---
# شمارهٔ ۱۱۳

<div class="b" id="bn1"><div class="m1"><p>چون چشم تو بر دلم شود تیر انداز</p></div>
<div class="m2"><p>زلف تو شود پیش دلم جوشن ساز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کوته نکنم دو دست از آن زلف دراز</p></div>
<div class="m2"><p>کاو تیر به جوشن از دلم دارد باز</p></div></div>