---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>چون شاه سرای پرده بر هامون زد</p></div>
<div class="m2"><p>از دجله و نیل خیمه تا جیحون زد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بارهٔ تازی از میان بیرون زد</p></div>
<div class="m2"><p>کیمخت زمین ز گَرد بر گردون زد</p></div></div>