---
title: >-
    شمارهٔ ۱۷۵
---
# شمارهٔ ۱۷۵

<div class="b" id="bn1"><div class="m1"><p>ای ماه کمان شهریاری گویی</p></div>
<div class="m2"><p>یا ابروی آن طرفه نگاری گویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نعلی زده از زرّ عیاری گویی</p></div>
<div class="m2"><p>درگوش سپهر گوشواری گویی</p></div></div>