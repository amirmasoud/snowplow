---
title: >-
    شمارهٔ ۱۳۷
---
# شمارهٔ ۱۳۷

<div class="b" id="bn1"><div class="m1"><p>نه با منی ار چه همنشینی با من</p></div>
<div class="m2"><p>ای بس دوری‌ که از تو بینم تا من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در من نرسی تا نشوم یک پا من</p></div>
<div class="m2"><p>کاندر ره عشق یا تو لنگی یا من</p></div></div>