---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>کوه از صف ترکان ملک ‌گردد پست</p></div>
<div class="m2"><p>ز ایشان به هنر یکی و از خصمان شصت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مجلس و میدان شه حورپرست</p></div>
<div class="m2"><p>دارند نهاده جام و جان برکفِ دست</p></div></div>