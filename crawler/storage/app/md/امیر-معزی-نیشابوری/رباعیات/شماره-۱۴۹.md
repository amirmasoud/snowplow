---
title: >-
    شمارهٔ ۱۴۹
---
# شمارهٔ ۱۴۹

<div class="b" id="bn1"><div class="m1"><p>سروی‌که بنفشه برگل آمد بر او</p></div>
<div class="m2"><p>اقبال رسانید به گردون سر او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماییم به مهر و دوستی در خور او</p></div>
<div class="m2"><p>فرماندهٔ عالمیم و فرمانبر او</p></div></div>