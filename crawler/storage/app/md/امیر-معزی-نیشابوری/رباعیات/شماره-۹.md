---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>خورشید فلک سجده برد رأی تو را</p></div>
<div class="m2"><p>ور سجده برد روی دلارای تورا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من خود چه‌ کسم که جان کنم جای تو را</p></div>
<div class="m2"><p>جان در تن من خاک سزد پای تو را</p></div></div>