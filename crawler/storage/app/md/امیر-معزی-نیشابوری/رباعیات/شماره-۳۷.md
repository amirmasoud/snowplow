---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>ای شاه دل روشن تو جوشن توست</p></div>
<div class="m2"><p>عالم شده روشن از دل روشن توست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پریدن جبریل به پیرامن توست</p></div>
<div class="m2"><p>صید ملک‌الموت سر دشمن توست</p></div></div>