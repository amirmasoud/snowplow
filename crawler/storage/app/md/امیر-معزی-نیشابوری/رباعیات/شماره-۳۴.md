---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>دولت‌ که همه جهان به‌سنجر دادست</p></div>
<div class="m2"><p>داند به‌ یقین که خوب و درخور دادست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سنجر که وزارت به مظفر دادست</p></div>
<div class="m2"><p>شک نیست که حق به‌ دست حقور دادست</p></div></div>