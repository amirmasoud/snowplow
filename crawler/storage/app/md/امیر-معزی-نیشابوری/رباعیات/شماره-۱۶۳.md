---
title: >-
    شمارهٔ ۱۶۳
---
# شمارهٔ ۱۶۳

<div class="b" id="bn1"><div class="m1"><p>ای عشق تو عمرم به کران آوردی</p></div>
<div class="m2"><p>ای هجر تنم را به فغان آوردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دل تو مرا کار به جان آوردی</p></div>
<div class="m2"><p>ای دیده دلم را به زبان آوردی</p></div></div>