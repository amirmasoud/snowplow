---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>هر کار که هست جز به‌ کام تو مباد</p></div>
<div class="m2"><p>هر خصم‌ که هست جز به دام تو مباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر شاه که هست جز غلام تو مباد</p></div>
<div class="m2"><p>هر خطبه که هست جز به نام تو مباد</p></div></div>