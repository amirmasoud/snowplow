---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>تا از برم آن یار پسندیده برفت</p></div>
<div class="m2"><p>آرام و قرار از دل شوریده برفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون دلم از دیده رواست از آنک</p></div>
<div class="m2"><p>از دل برود هر آنچه از دیده برفت</p></div></div>