---
title: >-
    شمارهٔ ۸۳
---
# شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>چون باز خیال تو پر و بال زند</p></div>
<div class="m2"><p>در جان رهی عشق تو چنگال زند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن کس‌ که نه از وصال تو نال زند</p></div>
<div class="m2"><p>شاید که ز چشم خویش قیقال زند</p></div></div>