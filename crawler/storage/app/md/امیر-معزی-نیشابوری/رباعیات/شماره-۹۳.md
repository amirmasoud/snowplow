---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>هر بزم که کردی همه بهروزی بود</p></div>
<div class="m2"><p>کار تو نشاط و مجلس افروزی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر رزم که کردی همه پیروزی بود</p></div>
<div class="m2"><p>هرچ آن دگری خواست تو را روزی بود</p></div></div>