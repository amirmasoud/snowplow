---
title: >-
    شمارهٔ ۱۵۹
---
# شمارهٔ ۱۵۹

<div class="b" id="bn1"><div class="m1"><p>افروخته دولت شه عالم رای</p></div>
<div class="m2"><p>ملک‌افزای است و عدل گستر همه‌جای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین دولت عدل گستر ملک‌افزای</p></div>
<div class="m2"><p>جشم بد خلق دور داراد خدای</p></div></div>