---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>یازنده‌تر از روزشماری ای شب</p></div>
<div class="m2"><p>تاریک‌تر از زلف نگاری ای شب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از روز همی یاد نداری ای شب</p></div>
<div class="m2"><p>گویی که سپیده‌دم نداری ای شب</p></div></div>