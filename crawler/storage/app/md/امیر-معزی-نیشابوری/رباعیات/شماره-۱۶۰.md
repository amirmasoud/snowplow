---
title: >-
    شمارهٔ ۱۶۰
---
# شمارهٔ ۱۶۰

<div class="b" id="bn1"><div class="m1"><p>ای رایت و رای تو همایون چو همای</p></div>
<div class="m2"><p>وای نامه و ا‌نام‌ا تو رسیده همه جای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیتی چو سرایی به تو دادست خدای</p></div>
<div class="m2"><p>شاهان جهان تورا غلامان سرای</p></div></div>