---
title: >-
    شمارهٔ ۱۴۶
---
# شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>ای برده به شمشیر همه ملک تگین</p></div>
<div class="m2"><p>آورده همه ملک تگین زیر نگین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیروزی و نصرت تو بر روی زمین</p></div>
<div class="m2"><p>آرایش دولت است و افزایش دین</p></div></div>