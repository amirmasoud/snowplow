---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>از تیغ چو آب تو به رزم آتش زاد</p></div>
<div class="m2"><p>تا خصم ز باد حمله در خاک افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بیم دلش پرآتش و سر بر باد</p></div>
<div class="m2"><p>دو دیده پرآب روی بر خاک نهاد</p></div></div>