---
title: >-
    شمارهٔ ۱۵۶
---
# شمارهٔ ۱۵۶

<div class="b" id="bn1"><div class="m1"><p>خصمان تو رسم بد نهادند همه</p></div>
<div class="m2"><p>بر ملک در فتنه‌ گشادند همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون عهد تو را به باد دادند همه</p></div>
<div class="m2"><p>از چرخ به خاک درفتادند همه</p></div></div>