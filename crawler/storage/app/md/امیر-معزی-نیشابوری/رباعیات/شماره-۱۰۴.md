---
title: >-
    شمارهٔ ۱۰۴
---
# شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>چون نرگس اگر نهیم در خاکستر</p></div>
<div class="m2"><p>ور داریم اندر آب چون نیلوفر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور بسپریم به پای همچون‌ گل تر</p></div>
<div class="m2"><p>از شرم تو چون بنفشه بر نارم سر</p></div></div>