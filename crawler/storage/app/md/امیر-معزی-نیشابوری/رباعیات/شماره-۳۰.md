---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>علم تو زعلم خضر و آصف بیش است</p></div>
<div class="m2"><p>حلم تو زحلم مَعن‌ وَاحنَف بیش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از صاعقه شمشیر تو را تف بیش است</p></div>
<div class="m2"><p>وز مورچه ترکان تو را صف بیش است</p></div></div>