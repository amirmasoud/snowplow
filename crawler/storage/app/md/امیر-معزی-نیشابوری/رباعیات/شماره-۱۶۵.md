---
title: >-
    شمارهٔ ۱۶۵
---
# شمارهٔ ۱۶۵

<div class="b" id="bn1"><div class="m1"><p>در بر ملکا دل توانگر داری</p></div>
<div class="m2"><p>دریای محیط است‌که در بر داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا برکف جام و بر سر افسر داری</p></div>
<div class="m2"><p>مه بر کف و آفتاب بر سر داری</p></div></div>