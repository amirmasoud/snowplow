---
title: >-
    شمارهٔ ۱۲۲
---
# شمارهٔ ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>ای کرده همه جهان ز ناپاکان پاک</p></div>
<div class="m2"><p>هرگز نبود تو را ز ناپاکان باک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای خسرو پاک پیکر ازگوهر خاک</p></div>
<div class="m2"><p>ای‌گوهر پاک احسن الله جزاک‌</p></div></div>