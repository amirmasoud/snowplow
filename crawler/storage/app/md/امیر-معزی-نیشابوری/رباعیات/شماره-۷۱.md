---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>هر شب که وصال یار دلبر باشد</p></div>
<div class="m2"><p>شب زورق و ماه باد صرصر باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وان شب که فراق آن سمن بر باشد</p></div>
<div class="m2"><p>شب کشتی و آفتاب لنگر باشد</p></div></div>