---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>در زلف تو آویخته دلبندی ها</p></div>
<div class="m2"><p>پیش خردت خیره خردمندی ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دل دارم که بندگیهات کنم</p></div>
<div class="m2"><p>تا خود چه کنی تو از خداوندی ها</p></div></div>