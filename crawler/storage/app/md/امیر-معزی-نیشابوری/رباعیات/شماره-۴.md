---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>شاها ادبی کن فلک بد خو را</p></div>
<div class="m2"><p>گر چشم رسانید رخ نیکو را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرگوی خطا کرد به چوگانش زن</p></div>
<div class="m2"><p>ور اسب خطا کرد به‌ من بخش او را</p></div></div>