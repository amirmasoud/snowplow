---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>شریف خاطر مسعود سعد سلمان را</p></div>
<div class="m2"><p>مسخرست سخن چون پری سلیمان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نسیج وحده که نو حُلّه‌ای دهد هر روز</p></div>
<div class="m2"><p>زکارگاه سخن بارگاه سلطان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز شادی ادب و عقل او به دار سلام</p></div>
<div class="m2"><p>همه سلامت و سعدست سعدسلمان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر دلیل بزرگی است فضل پس نه عجب</p></div>
<div class="m2"><p>که او دلیل بزرگی است فضل یزدان را</p></div></div>