---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>کردم اندر فتح غزنین ساحری در شاعری</p></div>
<div class="m2"><p>کرد پرگوهر دهانم پادشاه گوهری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست رادش در دهانم درّ دریایی نهاد</p></div>
<div class="m2"><p>چون ببارید از زبانم پیش او درّ دری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پادشا بخشد به شاعر زرّ و دیبا و قصب</p></div>
<div class="m2"><p>او مرا این هر سه بخشید و جواهر بر سری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درکنارم درّ و فیروزه است و لعل از جود او</p></div>
<div class="m2"><p>در وثاقم جامهٔ رومی و زرّ جعفری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگز از محمود غازی این عطا کی یافتند</p></div>
<div class="m2"><p>زینبی و عسجدی و فرخی و عنصری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر زند از جود محمودی به گیتی داستان</p></div>
<div class="m2"><p>گشت باطل جود محمودی ز جود سنجری</p></div></div>