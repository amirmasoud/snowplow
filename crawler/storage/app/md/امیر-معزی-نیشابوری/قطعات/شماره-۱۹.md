---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>صدر دین را ملک‌العرش گزید از وزرا</p></div>
<div class="m2"><p>همچنان چون وزرا از همهٔ خلق جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وزرا از همگان چون رمضان اندر سال</p></div>
<div class="m2"><p>صدر دین از وزرا چون شب قدر از رمضان</p></div></div>