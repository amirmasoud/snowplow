---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>چو بنوشت بر لوح نام تو را</p></div>
<div class="m2"><p>فرو ایستاد از نوشتن قلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همی گفت زین پس ندانم نوشت</p></div>
<div class="m2"><p>چو جزوی و کلی نوشتم به هم</p></div></div>