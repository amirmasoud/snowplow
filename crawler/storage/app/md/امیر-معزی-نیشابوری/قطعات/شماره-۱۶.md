---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>عزیز کرد مرا در محل عز و قبول</p></div>
<div class="m2"><p>ظهیر دولت شاه و شهاب دین رسول</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان شنید زمن شعر، کاحمد مختار</p></div>
<div class="m2"><p>شنید وحی ز روح‌الامین به وقت نزول</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو در ستایش او لفظ من مکرر شد</p></div>
<div class="m2"><p>لَطَف نمود و ز تکرار من نگشت ملول</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کجا ملول شود صاحبی که گاه سخن</p></div>
<div class="m2"><p>بود ز خاطر او نفع را فروع و اصول</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ایا ستوده‌کریمی که فضل‌گویان را</p></div>
<div class="m2"><p>ز شکر مکرمت توست فصل‌ها ز فصول</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کمال فضل تو داری و من به مجلس تو</p></div>
<div class="m2"><p>چو فضل خویش نمایم بود کمال فضول</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر هزار زبانم بود به جای یکی</p></div>
<div class="m2"><p>ستایش تو یکی از هزار کیف اقول</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو کرد طبع لطیفت قبول شعر مرا</p></div>
<div class="m2"><p>سزد که رای شریفت دهد نشان قبول</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو بشت آل بتولی و هست نایب من</p></div>
<div class="m2"><p>به مجلس تو خداوند شمع آل بتول</p></div></div>