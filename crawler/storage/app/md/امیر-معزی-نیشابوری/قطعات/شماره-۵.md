---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>جهاندار شد صدر دین در وزارت</p></div>
<div class="m2"><p>سپهدار شد شمس دین در امارت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز جد و پدر یادگار اند هر دو</p></div>
<div class="m2"><p>یکی در امارت یکی در وزارت</p></div></div>