---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>ای بار خدایی که خداوند جهانی</p></div>
<div class="m2"><p>لشکر شکن و ملک ده و ملک ستانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دریادل و مه‌طلعت و خورشید ضمیری</p></div>
<div class="m2"><p>باران سپه و ابر کف و برق سنانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فخرست به‌ سلطانی تو پیر و جوان را</p></div>
<div class="m2"><p>تا با خرد پیری و با بخت جوانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون مهر و سپهری و نه آنی و نه اینی</p></div>
<div class="m2"><p>چون ابر و هژیری و نه اینی ونه آنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چندین سخن نغز که دارد که تو داری</p></div>
<div class="m2"><p>چندین سخن نغز که داند که تو دانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاهان جهان را به‌ گه کین و گه مهر</p></div>
<div class="m2"><p>از تخت برانگیزی و بر تخت نشانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با تیغ به یک ساعت ملکی بگشایی</p></div>
<div class="m2"><p>با جام به یک لحظه‌ گنجی بفشانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرگز نبود شادی و هرگز نبود غم</p></div>
<div class="m2"><p>آن را که برانی تو و آن را که بخوانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در جام تو می بر صفت آب حیات است</p></div>
<div class="m2"><p>چون خضر امیدست که جاوید بمانی</p></div></div>