---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>چون مشک سیه بود مرا هر دو بنا گوش</p></div>
<div class="m2"><p>کافور ‌شد از پیری مشک سیه من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچندکه بسیارگنه دارم یا رب</p></div>
<div class="m2"><p>آمرزش تو بیشترست از گنه من</p></div></div>