---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>زان خط تو که همی بردمد از عارض تو</p></div>
<div class="m2"><p>کس نگوید که جمال تو دگر خواهد شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عارض نازک تو بر صفت گل تازه است</p></div>
<div class="m2"><p>زینت تازه گلت سُنبلِ تَر خواهد شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر دلم بر رخ تو شیفته و فتنه شدست</p></div>
<div class="m2"><p>بر خطت فتنه‌تر و شیفته‌تر خواهد شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای پسر گر خط مشکینت چنین خواهد بود</p></div>
<div class="m2"><p>نه بر آنم ‌که مرا با تو به سر خواهد شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به سر کار تو هر چند که در می‌نگرم</p></div>
<div class="m2"><p>دل و دینم به سر و کار تو در خواهد شد</p></div></div>