---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>فخر کردی که نسب داری از آباء کرام</p></div>
<div class="m2"><p>همه مشهور به‌ جود و کرم و آزادی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راست گفتی پدرانت همه نیکان بودند</p></div>
<div class="m2"><p>بد تو بودی به‌ حقیقت که از ایشان زادی</p></div></div>