---
title: >-
    شمارهٔ ۸ - قصیده در مدح شروانشاه منوچهر بن فریدون
---
# شمارهٔ ۸ - قصیده در مدح شروانشاه منوچهر بن فریدون

<div class="b" id="bn1"><div class="m1"><p>روز طرب رخ نمود روزه به پایان رسید</p></div>
<div class="m2"><p>رایت سلطان عید بر سر میدان رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خسرو شب سجده برد بر در سلطان روز</p></div>
<div class="m2"><p>دوش ز درگاه او پشت به خم زآن رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود به میدان عید پیکر خورشید گوی</p></div>
<div class="m2"><p>زآن به شب عید ماه چون سر چوگان رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حلقه سیمین نمود چرخ ز مه چون شهاب</p></div>
<div class="m2"><p>نیزه زرین به دست از پی جولان رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عید به شادی چو زد آینه بر پشت پیل</p></div>
<div class="m2"><p>آینه چرخ را گرد فراوان رسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مدت سی روز دید تاب تنور اثیر</p></div>
<div class="m2"><p>ز اول آن اجتماع کاخر شعبان رسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا چو بعید عرب شاه عجم خوان فکند</p></div>
<div class="m2"><p>خوان ورا ز آفتاب آهوی بریان رسید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گردون فراش وار کرد خلال از هلال</p></div>
<div class="m2"><p>گفت شهنشاه را عید به مهمان رسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>داشت چو خورشید و ماه تخت فلک تاج و طوق</p></div>
<div class="m2"><p>دوش ز تشریف بخت هر سه به خاقان رسید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نام خزان بر نبشت چرخ به منشور ملک</p></div>
<div class="m2"><p>نامه عزل بهار سوی گلستان رسید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خیل خزان تا گرفت مملکت نو بهار</p></div>
<div class="m2"><p>مهد شه مهرگان در صف بستان رسید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دیده ابر آب ریخت چهره آبان بشست</p></div>
<div class="m2"><p>تاب مه آب رفت تری آبان رسید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سیب کش آسیب زد نار بنار هوا</p></div>
<div class="m2"><p>خون دل از دیدگان تا به زنخدان رسید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باد که بی کیمیا خاک زمین کرد زر</p></div>
<div class="m2"><p>گفت مرا دستگاه از شه شروان رسید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وارث ملک زمین داور خلق جهان</p></div>
<div class="m2"><p>کش لقب از آسمان شاه جهانبان رسید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنکه ز بختش ببخش جاه سکندر فتاد</p></div>
<div class="m2"><p>وانکه ز دهرش به بهر ملک سلیمان رسید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از حشم حشمتش خصم به حیرت گرفت</p></div>
<div class="m2"><p>وز حرم حرمتش ظلم به پایان رسید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زلزله رخش او در (سد خزران) فتاد</p></div>
<div class="m2"><p>ولوله خنگ او تا حد (ختلان) رسید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از همه خصمانش کس مرده و زنده نرست</p></div>
<div class="m2"><p>مرده به دوزخ فتاد زنده به زندان رسید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر که بخیل و حشم خشم تو آسان شمرد</p></div>
<div class="m2"><p>آن حشم و خیل را خشم بدیسان رسید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هر که ز خاک درت دیده بینا بتافت</p></div>
<div class="m2"><p>زود به خاک درت کور و پشیمان رسید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>رفعت ایوان تو هست به جائی کزو</p></div>
<div class="m2"><p>هندوی پاس تو را دست به کیهان رسید</p></div></div>