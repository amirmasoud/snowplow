---
title: >-
    شمارهٔ ۱۳ - قصیده
---
# شمارهٔ ۱۳ - قصیده

<div class="b" id="bn1"><div class="m1"><p>چون نقطه نور سپهر آید ز حوت اندر حمل</p></div>
<div class="m2"><p>پوشد چو جنت باغ را حالی و حلی و حلل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچون ز نقض ار تنگ چین، گردد پر از سبزه زمین</p></div>
<div class="m2"><p>همچون بهشت از حور عین، گردد پر از لاله جبل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلبل برآرد غلغلی، چون بشکفد از گل گلی</p></div>
<div class="m2"><p>وز رشگ گل هر صلصلی، با بلبل آید در جدل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گردد شخ پر شاخ و سنگ، از سبزه چون پشت پلنگ</p></div>
<div class="m2"><p>آهو کند سم سیم رنگ، از یاسمین بر کوه و تل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیلوفر زاهد لباس از زر نهد بر دست کاس</p></div>
<div class="m2"><p>ابر از هوا در بیقیاس، افشانده در کاسش زطل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بستان ز گل یابد خطر بر گل کند بلبل نظر</p></div>
<div class="m2"><p>گل را دهد قطر مطر، در دلبری زور بطل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گویند مرغان در ربیع، ابیات و اشعار بدیع</p></div>
<div class="m2"><p>این گفته از بحر سریع، آن گفته از بحر رمل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون بلبلی ناله کند، دیده پر از ژاله کند</p></div>
<div class="m2"><p>کبک از پی ناله کند، بر بانگ او رقص از قلل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با گل کند لاله قران، مل بابنفشه همچنان</p></div>
<div class="m2"><p>زین هر سه بینی بوستان، پر آتش و دود و شعل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لاله برغم ماه دی، بر کف نهاده جام می</p></div>
<div class="m2"><p>بر جای می در جام وی، بیند نشان درد خل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گل چون طبیب دستکار، آراسته بر جویبار</p></div>
<div class="m2"><p>آید که نرگس را ز تار، از دیده بردارد سبل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا باد نوروزی بزان، شد در چمن ها در وزان</p></div>
<div class="m2"><p>گم گشت آثار خزان و افزود در عالم امل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زین پیش در دی ماه دون، از برف که شد سیمگون</p></div>
<div class="m2"><p>وز فر فروردین کنون شد سیم چون سیماب حل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای چون تو خوبی در جهان، ندهد به خوبی کس نشان</p></div>
<div class="m2"><p>لاغر چو موران از میان، فربه چو گوران از کفل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن خال تو بر طرف لب، در سایه زلف چو شب</p></div>
<div class="m2"><p>گوئی قران کرد ای عجب، با زهره در عقرب زحل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون جام بر دستم دهی، باید که بوسم در دهی</p></div>
<div class="m2"><p>تا من کنم ساغر تهی، بر یاد شاهنشاه یل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فرمانده روی زمین خاقان اکبر فخر دین</p></div>
<div class="m2"><p>خسرو منوچهر گزین دارنده دین و دول</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شاهی که بر درگاه او، از قدر و صدر گاه او</p></div>
<div class="m2"><p>دور فلک با جاه او از بندگان کمتر محل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در امن و عدل و ملک و دین ساکن چو اندر بسم سین</p></div>
<div class="m2"><p>بر لطف و خشمش مهر و کین، بینی چو ها را لام هل</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گرچه پلنگان را گلو، بفشرد، چرخ شیر خو</p></div>
<div class="m2"><p>پیش سگ درگاه او، گربه بیفکند از بغل</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>احکام او را چون عباد، آورده افلاک انقیاد</p></div>
<div class="m2"><p>از عالم کون و فساد، آثار او برده خلل</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر عکس تیغش اندکی، بر انجام افتد بی شکی</p></div>
<div class="m2"><p>گردد ز نور هر یکی، افلاک بر سوز دفل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>با عدلش اندر ناحیت، ظالم نماند و بد نیت</p></div>
<div class="m2"><p>آری به حکم خاصیت، بگریزد از نافه جعل</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>افلاح شاخ و بیخ او، در تیغ چون مریخ او</p></div>
<div class="m2"><p>ایام را تاریخ او، از عهد اسکندر بدل</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا خصم او غمناک شد، زهر دلی تریاک شد</p></div>
<div class="m2"><p>شروان ز فتنه پاک شد، چون کعبه از لات و هبل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای فضل و عدلت بی غرض، طبع و مزاجت بی مرض</p></div>
<div class="m2"><p>دوری چو روح از هر عوض، پاکی چو عقل از هر زلل</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا هست انجم را قران، تا خیزد از آتش دخان</p></div>
<div class="m2"><p>تا باد باشد اصل جان، تا زآب و خاک آید وحل</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>باد انجم از قدرت نشان، چون آتش آثار عیان</p></div>
<div class="m2"><p>بر آب و خاک امرت روان، چون باد در صحرا وتل</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بر دست توگاه ظفر چون گوهر از نصرت حجر</p></div>
<div class="m2"><p>در کام خصم خیره سر، چون حنظل از محنت عسل</p></div></div>