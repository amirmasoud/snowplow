---
title: >-
    شمارهٔ ۱۶ - قصیده
---
# شمارهٔ ۱۶ - قصیده

<div class="b" id="bn1"><div class="m1"><p>دادگرا ملک را هم فلک و هم قوام</p></div>
<div class="m2"><p>تاجورا بخت را هم شرف و هم نظام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاجز قهرش قضا چاکر قدرش قدر</p></div>
<div class="m2"><p>ساکن طبعش کرم شاکر جودش کرام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسته ببندش سپهر اشهب زرین جناح</p></div>
<div class="m2"><p>کرده به داغش جهان ادهم سیمین ستام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاک درش را ز عرض سجده گه شرق و غرب</p></div>
<div class="m2"><p>بارگهش ماه بار، شاه ره خاص و عام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در اثر لفظ او مایه کشف الصدور</p></div>
<div class="m2"><p>در نظر رای او معجز یحیی العظام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از رقم رای او هیأت افلاک را</p></div>
<div class="m2"><p>بر کرهای عظیم دایره های عظام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای کمر آسمان بسته به خم کمند</p></div>
<div class="m2"><p>وی جگر اختران خسته به زخم سهام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نعل ستور تو را تاج شه شام شوم</p></div>
<div class="m2"><p>پای سریر تو را فرق شه روم رام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حکم تو را زیر دست چارحد و شش جهت</p></div>
<div class="m2"><p>فر تو را زیر پای هشت در و هفت بام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هور به نصف النهار بر درج اوج خویش</p></div>
<div class="m2"><p>قامت رای تو را تا حد نصف القیام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر به تناسخ شوند زنده به دوران تو</p></div>
<div class="m2"><p>سام و جم اندر جهان با حشم و احتشام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حال کند جان نثار بر سر جام تو جم</p></div>
<div class="m2"><p>زود کند سر فدا بر سم اسب تو سام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خصم به شطرنج کین با تو ببسته گرو</p></div>
<div class="m2"><p>داده به هر سو برش دولت تو شاه فام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر دم افعی جذاب دفع کند چون فکند</p></div>
<div class="m2"><p>رمح چو افعی تو بر تن خصمان جذام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گرد رکاب تراست خدمت حبل المتین</p></div>
<div class="m2"><p>خاک حریم تراست حرمت بین الحرام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نیست عجب گر کند خلق به طوع و به طبع</p></div>
<div class="m2"><p>گاه بدین اجتهاد گاه بدان اعتصام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چرخ ز ایوان تو در طمع ارتفاع</p></div>
<div class="m2"><p>دهر ز دیوان تو در طلب اهتمام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از پی فرخندگی بر سر دنیا و دین</p></div>
<div class="m2"><p>فر همایون تو گشته همای همام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چرخ چو زیر تو دید قله فربه چو برق</p></div>
<div class="m2"><p>گفت جهان را متاز ابلق لاغر جمام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دایره گردی که داد نقطه موهوم را</p></div>
<div class="m2"><p>همچو خط و سطح و جسم گردش اوالتسام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تنگ بر هنگ او پهنه سهل الامم</p></div>
<div class="m2"><p>پست بر دست او نسبت صعب المرام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هیکل او در مصاف کشتی دریا رکاب</p></div>
<div class="m2"><p>پیکر او در نبرد صرصر آتش لگام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در تک او روز جنگ هر دو جهان یک وجب</p></div>
<div class="m2"><p>با سم او گاه سیر هفت زمین نیم گام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گرد سم او ببست چشم زحل را سبل</p></div>
<div class="m2"><p>باد تک او گشاد مغز فلک را زکام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هست اثیر و محیط پیکر او، کآورد</p></div>
<div class="m2"><p>گه تف آتش ز سم گه کف دریا ز گام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای رقم کین تو آتش و حاسد حطب</p></div>
<div class="m2"><p>وی اثر خشم تو دوزخ و دشمن حطام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>موسم نوروز تو یافت به آثار سور</p></div>
<div class="m2"><p>خاصیت پرغموم نزد خواص و عوام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>باد همایون به تو سال نو و روز نو</p></div>
<div class="m2"><p>عمر تو زان بر مزید عز تو زین بر دوام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>داد ز طبع چو آب خاطر چون آتشم</p></div>
<div class="m2"><p>این سخن گرم را زین غزل تر قوام</p></div></div>