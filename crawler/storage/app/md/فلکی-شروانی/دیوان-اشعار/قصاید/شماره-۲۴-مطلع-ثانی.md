---
title: >-
    شمارهٔ ۲۴ - مطلع ثانی
---
# شمارهٔ ۲۴ - مطلع ثانی

<div class="b" id="bn1"><div class="m1"><p>سرو قدی شکر لبی گلرخ غالیه کله</p></div>
<div class="m2"><p>جان مرا به صد زبان زآن رخ و غالیه گله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نرگس مستش آسمان سفته به تیر غمزگان</p></div>
<div class="m2"><p>سنبل هندویش به جان رفته به سایه گله</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن ز میان انس و جان برده هزار کاروان</p></div>
<div class="m2"><p>وین ز بساط انس و جان رفته هزار قافله</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هست طراز یاسمین لاله لؤلؤ آفرین</p></div>
<div class="m2"><p>کرده لبش چو انگبین تعبیه در شکرلله</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از سر زلف خود بفن وز گهر سرشگ من</p></div>
<div class="m2"><p>بافته جیب و پیرهن ساخته گوی و انگله</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من ز غمش چو بی هشان بر دو رخ از جفا نشان</p></div>
<div class="m2"><p>تن ز دو چشم خونفشان غرقه در آب و آبله</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>او چو پری به دلبری کرده مرا ز دل بری</p></div>
<div class="m2"><p>خسته دل من آن پری بسته به بند و سلسله</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای بت خلخ و چکل از تو بت تبت خجل</p></div>
<div class="m2"><p>نزد تو وزن جان و دل یکجو و نیم خردله</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مشعله برفروختی رخت فلک بسوختی</p></div>
<div class="m2"><p>بر (فلکی) فروختی شهر بسوز و مشعله</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کرده به عالم روان حسن تو کاروان دوان</p></div>
<div class="m2"><p>وز در شاه خسروان یافته زاد و راحله</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مالک ملک باستان بارگهش در آسمان</p></div>
<div class="m2"><p>بام ورا ز نردبان چرخ فروترین پله</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بس که کند به چشم و سر بر در درگه تو بر</p></div>
<div class="m2"><p>صاحب چاچ و کاشغر خدمت کفش و چاچله</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای گه کین درخش تو خنجر نوربخش تو</p></div>
<div class="m2"><p>گشته به کام رخش تو هفت زمین دو مرحله</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ملک بقا گشاده خوان کرم نهاده</p></div>
<div class="m2"><p>طعم طمع تو داده بیش ز قدر حوصله</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا به مشام ذوق جان ندهد و ناورد جهان</p></div>
<div class="m2"><p>نکهت گل زانگدان لذت مل ز آمله</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>طبع تو باد شاد خور مل به کفت ز جام زر</p></div>
<div class="m2"><p>دلبر گلرخت ببر بی غم و رنج و غایله</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چار ملک ز شش کران هفت شه از نه آسمان</p></div>
<div class="m2"><p>حکم تو را نهاده جان بر دو کف و ده انمله</p></div></div>