---
title: >-
    شمارهٔ ۲۱ - وله
---
# شمارهٔ ۲۱ - وله

<div class="b" id="bn1"><div class="m1"><p>صورت گردون صفت جسم زمین را جان از او</p></div>
<div class="m2"><p>ماده جان لطف او لیکن خرد حیران از او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آینه رنگی که در یک لحظه گردد پیش چشم</p></div>
<div class="m2"><p>نقش ششدر هشت اشکال فلک عریان از او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن یمانی تیغ بین اندر یمین او که هست</p></div>
<div class="m2"><p>کفر در ناایمنی اندر امان ایمان از او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاده ایمان که گر بر آسمان عکس افکند</p></div>
<div class="m2"><p>نور صد خورشید بخشد در زمان کیوان از او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر کشد مالک رقاب او را به شروان از قراب</p></div>
<div class="m2"><p>خاک ترکستان شود در قیروان قربان از او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ز فولاد فلک دشمن سپر سازد شود</p></div>
<div class="m2"><p>ریزه ریزه روز کین چون سونش سوهان از او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>او به سر سامان نیا آمد به نسبت لاجرم</p></div>
<div class="m2"><p>دشمن این دودمان شد یسر و سامان از او</p></div></div>