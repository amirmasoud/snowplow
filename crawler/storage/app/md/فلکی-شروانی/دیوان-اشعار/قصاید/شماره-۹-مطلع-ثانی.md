---
title: >-
    شمارهٔ ۹ - مطلع ثانی
---
# شمارهٔ ۹ - مطلع ثانی

<div class="b" id="bn1"><div class="m1"><p>تا به دل و جان مرا آفت جانان رسید</p></div>
<div class="m2"><p>بس که ز جانان به من رنج دل و جان رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک ره از چشم من چشمه خوناب گشت</p></div>
<div class="m2"><p>تا به من از باد غم آتش هجران رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا لب من دور ماند از لب و دندان او</p></div>
<div class="m2"><p>دل شد و جانم به لب از بن دندان رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هست به باغ بهار چون گل خندان رخش</p></div>
<div class="m2"><p>در مه مهر از رخش مهر به سرطان رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>او چو بهار و بهشت وز رخ رخشان او</p></div>
<div class="m2"><p>فتنه به فصل خزان با گل و ریحان رسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چهره او آفتاب چشمه حیوان لبش</p></div>
<div class="m2"><p>چشم مرا زآن دو شکل آفت طوفان رسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه ز ظلمت رسید خضر به آب حیات</p></div>
<div class="m2"><p>دوش به من ز آفتاب چشمه حیوان رسید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با رخ رخشان او گشت به شروان خجل</p></div>
<div class="m2"><p>پرتو آن آفتاب کو ز خراسان رسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ماه رخش چون بتافت از بن دندان او</p></div>
<div class="m2"><p>بحر دو چشم مرا لؤلؤ و مرجان رسید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دوش خیالش به خواب کرد گذر بر دلم</p></div>
<div class="m2"><p>عقل ولایت سپرد گفت که سلطان رسید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بود رسیده به جان درد دل ریش من</p></div>
<div class="m2"><p>ریش به مرهم فتاد درد به درمان رسید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفتمش ای از لبت لعل بدخشان خجل</p></div>
<div class="m2"><p>بی لبت از چشم من خون به بدخشان رسید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شد بر دندان تو لؤلؤ عمان زآب</p></div>
<div class="m2"><p>وز غم تو اشک من ز آنسوی عمان رسید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای شده از هست من در طلب تو مرا</p></div>
<div class="m2"><p>بس که بباید دوید تا به تو بتوان رسید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون فلکی در جفا با (فلکی) طرفه نیست</p></div>
<div class="m2"><p>گر (فلکی) را ز درد بر فلک افغان رسید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنکه به ام ملوک نامه شاهی نوشت</p></div>
<div class="m2"><p>نام تو سر نامه کرد چونکه بعنوان رسید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا ز جهان در جهان خلق حکایت کنند</p></div>
<div class="m2"><p>کز پس عهد فلان ملک به بهمان رسید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وارث اعمار خلق ذات مکین تو باد</p></div>
<div class="m2"><p>کز تو به تمکین حق غایت امکان رسید</p></div></div>