---
title: >-
    شمارهٔ ۲۳ - در مدح منوچهر شروانشاه
---
# شمارهٔ ۲۳ - در مدح منوچهر شروانشاه

<div class="b" id="bn1"><div class="m1"><p>دوش چو کرد آسمان افسر زر ز سر یله</p></div>
<div class="m2"><p>ساخت ز ماه و اختران یاره و عقد و مرسله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکل فلک خراس شد مهر چو دانه آس شد</p></div>
<div class="m2"><p>عقده راس داس شد از پی کشت سنبله</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طرف جبین نموده ماه از طرف بساط شاه</p></div>
<div class="m2"><p>آمده با قبول و جاه از قبل مقابله</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از پی تیر آسمان ساخته ماه نو کمان</p></div>
<div class="m2"><p>تا ز کمان به بدگمان همچو یلان کند یله</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زهره چو شیر شرزه برده ز دهر بهره</p></div>
<div class="m2"><p>آخته شهره دهره داده صقال و مصقله</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاه فلک ز بارگه کرده نشاط خوابگه</p></div>
<div class="m2"><p>بر در بارگه سپه ساخته شور و مشغله</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیر سپهر پنجمین شیر سپهر کرده زین</p></div>
<div class="m2"><p>خیره چو شیر تا به کین با که کند مجادله</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از پی فال مشتری انجم سعد مشتری</p></div>
<div class="m2"><p>او ز شراع ششتری با همه در معامله</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همچو مهندسان ز حل مشکل چرخ کرده حل</p></div>
<div class="m2"><p>پایه برترین محل از درجات سافله</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دهر چو زنگی عجب کرده کلاه بوالعجب</p></div>
<div class="m2"><p>بر کله از پی طرب بسته هزار زنگله</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شب ز سپهر و اختران ساخته بحر و گوهران</p></div>
<div class="m2"><p>نعش ز بهر دختران کرده سفینه راحله</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از پی انجم و فلک دوخته کسوتی ملک</p></div>
<div class="m2"><p>پشت ز قاقم و فنک روی ز قند زودله</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خسرو اختران به کین کرده همانگه از کمین</p></div>
<div class="m2"><p>خنجر شاه پاک دین بر سپه معطله</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چرخ و نجوم و مهر و مه بر در بارگاه شه</p></div>
<div class="m2"><p>بوسه زنان به خاک ره چون رهیان یکدله</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خسرو آرشی نسب آرش دیگر از حسب</p></div>
<div class="m2"><p>ناصر سنت عرب قاهر بدعت حله</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جام و حسامش از شرف برده بگاه بارولف</p></div>
<div class="m2"><p>از رخ دوستان کلف وز سر دشمنان کله</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای ملکان چو بندگان بر تو ثنا کنندگان</p></div>
<div class="m2"><p>مادر جان زندگان از قبل تو حامله</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وارث جود و جاه جم، شیر اجم شه عجم</p></div>
<div class="m2"><p>مالک ملت و امم صاحب صولت و صله</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عز ولیش را ازل، گربه فکنده از بغل</p></div>
<div class="m2"><p>عمر عدوش را اجل، گرگ فکنده در گله</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عقل به فضل شاملش جان خجل از فضایلش</p></div>
<div class="m2"><p>فضله رای فاضلش نکته و رمز و فاضله</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای به وجودت از زمین یافته عقل دوربین</p></div>
<div class="m2"><p>گنج روان ز پارگین در ثمین ز مزبله</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر که رخ از تو تافته قبله جان شکافته</p></div>
<div class="m2"><p>طفل کرم نیافته به ز کف تو قابله</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>قدر تو را فراز خوان، قرصه آفتاب نان</p></div>
<div class="m2"><p>زیر و زبر به زور تو، بوم عدو به زلزله</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>باد برت جبابره، قصه گرت قیاصره</p></div>
<div class="m2"><p>کاسه کشت اکاسره، قلبه چشت هراقله</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هر که بدید در دغا گاه مقاتله تو را</p></div>
<div class="m2"><p>معجز روی مصطفی دیده گه مباهله</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر تو کنی به امتحان چتر به هندوچین روان</p></div>
<div class="m2"><p>رایت رای و خوان خان زود رود به ولوله</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای بر تو بهر نسق خلق جهان همه خلق</p></div>
<div class="m2"><p>عالم علم را به حق علم تو گشته عاقله</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رایت عید شد عیان موکب روزه شد نهان</p></div>
<div class="m2"><p>سنت عید قرض دان فرض صیام نافله</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر چه به صحن گلستان از پی نزهت روان</p></div>
<div class="m2"><p>نیست صفیر بلبلان هست صفیر بلبله</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>عید و خزان و مهرگان هر سه شدند هم قران</p></div>
<div class="m2"><p>گشت میان هر سه شان بندگی تو واصله</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هر سه به شکل صوفیان خرقه نهاده در میان</p></div>
<div class="m2"><p>پیر توئی بکن بیان مشکل این مشاکله</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بزم فکن طرب گزین فتنه نشان و خوش نشین</p></div>
<div class="m2"><p>عدت عید کن قرین عادت روزه کن یله</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>وز پی مدح خود شنو این غزل لطیف تو</p></div>
<div class="m2"><p>لطف کمال او گرو برده ز روح کامله</p></div></div>