---
title: >-
    شمارهٔ ۱۷ - مطلع ثانی
---
# شمارهٔ ۱۷ - مطلع ثانی

<div class="b" id="bn1"><div class="m1"><p>ای رخ و قد تو را دل رهی و جان غلام</p></div>
<div class="m2"><p>قد تو سرو سهی روی تو ماه تمام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در فلک چشم من ماه تو گشته مقیم</p></div>
<div class="m2"><p>در چمن جان من سرو تو کرده مقام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درد توام در دلست زخم توام بر جگر</p></div>
<div class="m2"><p>داروی دردم کجا مرهم زخمم کدام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند ز رویت به من ماه فرستد درود</p></div>
<div class="m2"><p>چند به بویت به من باد رساند پیام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخت نخواهد گرفت دست من مستمند</p></div>
<div class="m2"><p>چرخ نخواهد شنید مست من مستهام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چه ز اسباب عیش بود مرا در غمت</p></div>
<div class="m2"><p>اغرق ماء البکا، احرق نارالغرام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا ز جمال خودم روی تو محروم کرد</p></div>
<div class="m2"><p>خون دلم شد حلال خواب خوشم شد حرام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از دل من چاشت خورد غمزه تو روز هجر</p></div>
<div class="m2"><p>تا نخورد از لبت دل به شب وصل شام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر (فلکی) بیش ازین جور مکن چون فلک</p></div>
<div class="m2"><p>تا چو لقای ملک مهر تو جوید مدام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ضامن ارزاق خلق نایب فرمان حق</p></div>
<div class="m2"><p>اختر گردون لطف گوهر دریای کام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن که به پیش لقاش گشت ستاره سپهر</p></div>
<div class="m2"><p>وآنکه به دست رضاش داد زمانه زمام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای شرف بی وبال یافته در طالعت</p></div>
<div class="m2"><p>هم به تواضع نجوم هم به مواضع سهام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا بود اندر عجم نوبت جشن ملوک</p></div>
<div class="m2"><p>تا بود اندر عرب عادت عید صیام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کف تو بحر و در او گوهر تیغ بنفش</p></div>
<div class="m2"><p>دست تو چرخ و بر او اختر جام مدام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ملکت تو مستقیم رایت تو مستوی</p></div>
<div class="m2"><p>دولت تو مستزاد نعمت تو مستدام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون بنشینی به ناز با می نوشین نشین</p></div>
<div class="m2"><p>چون بخرامی به کام با دل خرم خرام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا به سلامت بود طبع سلیم از جهان</p></div>
<div class="m2"><p>باد مسلم تو را ملک جهان والسلام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باد معمر به تو ملک عجم تا ابد</p></div>
<div class="m2"><p>باد مشرف به تو دین عرب تا قیام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بسته میان خسران پیش تو چون لام الف</p></div>
<div class="m2"><p>ساخته در خدمتت دل چو الف قد چو لام</p></div></div>