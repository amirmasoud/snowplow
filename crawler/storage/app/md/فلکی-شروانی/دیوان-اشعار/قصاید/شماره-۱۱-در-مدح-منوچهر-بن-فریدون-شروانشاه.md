---
title: >-
    شمارهٔ ۱۱ - در مدح منوچهر بن فریدون شروانشاه
---
# شمارهٔ ۱۱ - در مدح منوچهر بن فریدون شروانشاه

<div class="b" id="bn1"><div class="m1"><p>آن رخ رخشان و زلف عنبر افشانش نگر</p></div>
<div class="m2"><p>و آن لب و دندان چون لؤلؤ و مرجانش نگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرد با من شرط و پیمان در وفا و دوستی</p></div>
<div class="m2"><p>ذره ننمود از آن شرط و پیمانش نگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در میان جان من درد فراقش دیده ای</p></div>
<div class="m2"><p>بر دل بیچاره من داغ هجرانش نگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلبری کز دور دیداری ز ما دارد دریغ</p></div>
<div class="m2"><p>با خسان و ناکسان در بوسه احسانش نگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جرم های بی خطا و جورهای بی سبب</p></div>
<div class="m2"><p>بر مسلمانان ز چشم نامسلمانش نگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ندیدی یار کو عاشق کند قربان بعید</p></div>
<div class="m2"><p>کیش و قربان بسته و در عید قربانش نگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای بتی کز دل چو پرسم کو قرارت گویدم</p></div>
<div class="m2"><p>بسته اندر طره جعد پریشانش نگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر زمان ما را چنین اندر غم و خواری مگیر</p></div>
<div class="m2"><p>وین چنین بازار عشق خویش بر جانش نگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ور همه در دوستی زو عار داری روز عید</p></div>
<div class="m2"><p>مدح خوانان پیش تخت شاه شروانش نگر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فخر دین خاقان اکبر کآسمان چون بیندش</p></div>
<div class="m2"><p>گوید آن جاه و جلال و امر و فرمانش نگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خسرو ایران منوچهر آنکه در شأنش خرد</p></div>
<div class="m2"><p>گفت سبحان الله آن رای جهانبانش نگر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هست خاقان بزرگ او را قلب لیکن به قدر</p></div>
<div class="m2"><p>بندگان بهتر از فغفور و خاقانش نگر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>منگر آن کز کینه دشمن پار، زی او کرد قصد</p></div>
<div class="m2"><p>روز کنون امسال خان و مان ویرانش نگر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر همی خواهی که بتوانی نشان دادن ز عرش</p></div>
<div class="m2"><p>یک ره آن ایوان عالیتر ز کیوانش نگر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سوی خصمان از برای بردن پیغام مرگ</p></div>
<div class="m2"><p>هر زمانی رفتن پیکان پیکانش نگر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روز کین رایات او را بین به پیروزی روان</p></div>
<div class="m2"><p>وآمده آیات فتح از چرخ در شانش نگر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر ندیدی سیل باران در بهاران روز جنگ</p></div>
<div class="m2"><p>بر کمانداران دشمن تیر بارانش نگر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زیر پای مرکبان سرهای بدخواهان ملک</p></div>
<div class="m2"><p>همچو گوی افتاده اندر صحن میدانش نگر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آنکه عصیان جست و دست از دامن مهرش بداشت</p></div>
<div class="m2"><p>هم به تیغ او ز خون زه بر گریبانش نگر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از برای آنکه عالم را نگهبان تیغ اوست</p></div>
<div class="m2"><p>کردگار خلق عالم را نگهبانش نگر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دارد اندر دولت و دانش ملک حد کمال</p></div>
<div class="m2"><p>این کمال ایمن از آسیب نقصانش نگر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر چه دوران فلک فرخنده گشت از فر عید</p></div>
<div class="m2"><p>فر عیدی فرخ از فرخنده دورانش نگر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مدح او را نیست پایانی و انجامی پدید</p></div>
<div class="m2"><p>این همایون مدح بی انجام و پایانش نگر</p></div></div>