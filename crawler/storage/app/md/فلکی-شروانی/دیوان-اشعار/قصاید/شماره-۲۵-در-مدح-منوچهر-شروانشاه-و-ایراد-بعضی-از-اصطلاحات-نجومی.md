---
title: >-
    شمارهٔ ۲۵ - در مدح منوچهر شروانشاه و ایراد بعضی از اصطلاحات نجومی
---
# شمارهٔ ۲۵ - در مدح منوچهر شروانشاه و ایراد بعضی از اصطلاحات نجومی

<div class="b" id="bn1"><div class="m1"><p>شاه گردون را نگر شکل دگرگون ساخته</p></div>
<div class="m2"><p>بر شمایل پیکران عزم شبیخون ساخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داده فرمان لشگر سقلاب را بر ملک زنگ</p></div>
<div class="m2"><p>هر یکی را آلت و برگی دگرگون ساخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حمة العقرب چشیده وز پی کسب شرف</p></div>
<div class="m2"><p>خود ز بطن الحوت خلوت جای ذوالنون ساخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از پی طفلان آب و گل صبا فراش وار</p></div>
<div class="m2"><p>بالش از بغدادی و بستر ز پرنون ساخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم کنون بینی عروسان بهاری را به باغ</p></div>
<div class="m2"><p>قرطه از مقراضی و کسوت ز اکسون ساخته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در جهانگیری صبا و در جهانداری سحاب</p></div>
<div class="m2"><p>روز جنگ قارن و شب گنج قارون ساخته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کاس لعل ارغوان و طاس یاقوتین چرخ</p></div>
<div class="m2"><p>در چمن کو بدخش و کان طیسون ساخته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باد چون گنجور از درگاه خاقان آمده</p></div>
<div class="m2"><p>باغ را از حله چون خرگاه خاتون ساخته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خرم و خندان درخش تندر اندر ابر و ابر</p></div>
<div class="m2"><p>خیره خود را از میان گریان و محزون ساخته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باغ چون فردوس و صلصل همچو رضوان و صبا</p></div>
<div class="m2"><p>شکلها چون آدم از صلصال مسنون ساخته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دشت هر کافور کاندر ماه کانون گرد کرد</p></div>
<div class="m2"><p>در مه نیسان به دست مهر مرهون ساخته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باد نوروزی به تأثیر اثیر اندر هوا</p></div>
<div class="m2"><p>بر مساعد کردن کافور کانون ساخته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کرد مینوچهر گردون چهره باغ و در او</p></div>
<div class="m2"><p>بزم شاهنشه منوچهر فریدون ساخته</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا زند هر صبحدم پیراهن ملکش به آب</p></div>
<div class="m2"><p>آسمان از قرصه خود قرص صابون ساخته</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بخت بالای نود درج ارتفاع و آسمان</p></div>
<div class="m2"><p>رفعت او را درج تسعا و تسعون ساخته</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حجله سعدان گردون طالع مسعود او</p></div>
<div class="m2"><p>از فضای کرزمان و دست سعدون ساخته</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون ز نوک نیزها گردد پر اوراق اجل</p></div>
<div class="m2"><p>فضل های فتنه را فهرست قانون ساخته</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر جگر خواران جهان بفروخته گردون به جان</p></div>
<div class="m2"><p>عقل از آن بازار خود را سخت مغبون ساخته</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آسمان از نیزه گردان و خون گردنان</p></div>
<div class="m2"><p>بیشه هندوستان بر رود جیحون ساخته</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سیل سیل از خون روان بر روی خاک و رنگ او</p></div>
<div class="m2"><p>میل میل از خاک همرنگ طبرخون ساخته</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کرده خصمان را جگر بریان و از بهر ددان</p></div>
<div class="m2"><p>خوانی از صد هفتخوان برگ وی افزون ساخته</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای ظفر با رایت منصور تو در دین و ملک</p></div>
<div class="m2"><p>همچو با اعجاز موسی رای هارون ساخته</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وی بدی با حاسد و بدخواه تو در کفر و شرک</p></div>
<div class="m2"><p>همچو هامان خیره با فرعون ملعون ساخته</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کرده شروان را چنان معمور کز بس فر و زیب</p></div>
<div class="m2"><p>خلق را دیدار او بی فتنه مفتون ساخته</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا طرازند ابلق ایام را از بهر تو</p></div>
<div class="m2"><p>مه پلاس و سایه خورشید بر گون ساخته</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تاخته بر آسمان بخت تو چون عیسی و خصم</p></div>
<div class="m2"><p>همچو قارون در زمین با بخت واژون ساخته</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ماده لفظ بدیعت با عروس بکر غریب</p></div>
<div class="m2"><p>چون دل گشتاسب با مهر کتایون ساخته</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای ز خاک پای تو دولت به اعجاز و به علم</p></div>
<div class="m2"><p>کیمیای جان ادریس و فلاطون ساخته</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مهر تو در حلق ملک از نیش نوش انگیخته</p></div>
<div class="m2"><p>کین تو در کام خصم از طعمه طاعون ساخته</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خشم تو از چشم دشمن بر گشاده باسلیق</p></div>
<div class="m2"><p>چشم چرخ از خاک پایت باسلیقون ساخته</p></div></div>