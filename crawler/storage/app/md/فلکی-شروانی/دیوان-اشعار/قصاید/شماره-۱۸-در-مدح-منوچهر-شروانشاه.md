---
title: >-
    شمارهٔ ۱۸ - در مدح منوچهر شروانشاه
---
# شمارهٔ ۱۸ - در مدح منوچهر شروانشاه

<div class="b" id="bn1"><div class="m1"><p>کی کشم در چشم و کی بوسم به کام</p></div>
<div class="m2"><p>خاک درگاه شهنشاه انام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی بود گوئی که بینم بر مراد</p></div>
<div class="m2"><p>شاه را دلشاد و گردم شاد کام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از قبول شاه کی باشد مرا</p></div>
<div class="m2"><p>سعی استظهار و حسن اهتمام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کار بختم را که رفت از قاعده</p></div>
<div class="m2"><p>رحمت خسرو کی آرد با نظام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ماه بختم کی برون آید ز میغ</p></div>
<div class="m2"><p>صید بختم کی رها گردد ز دام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از لهیب آن گنه بر جان من</p></div>
<div class="m2"><p>روز روشن گشت چون شام ظلام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرو عیشم خفته گشت از باد برد</p></div>
<div class="m2"><p>ماه امیدم بماند اندر غمام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اختر کامم فتاد اندر هبوط</p></div>
<div class="m2"><p>واختر بد کرد در حالم مقام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیش کز تف دل و سوز جگر</p></div>
<div class="m2"><p>شد طعامم طعم آتش چون نعام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر بدی کردم کشید از جان من</p></div>
<div class="m2"><p>اتفاق طالع بد انتقام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طبع پیری عکس طبع هر کسی</p></div>
<div class="m2"><p>با خرد ناجنس و با جانها قهام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>راه غم سوی دلم سهل الالم</p></div>
<div class="m2"><p>راه من سوی طرب صعب المرام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر مرا خوان مانده بودی در عروق</p></div>
<div class="m2"><p>چون عرق خونم گشادی از مسام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای عجب گردون به عزم کشتنم</p></div>
<div class="m2"><p>زود صعب آهیخت شمشیر از نیام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چرخ چون بر کشتنم بفشرد پای</p></div>
<div class="m2"><p>مهربان بخت از برم برداشت گام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آری ار گل بوی بدهد بی خلاف</p></div>
<div class="m2"><p>صاحب سرسام را گیرد زکام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مقصد امید بس دور است و هست</p></div>
<div class="m2"><p>مرکب اقبال من لاغر جمام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرده بودم وز همه اعضای من</p></div>
<div class="m2"><p>استخوانها بود پیدا همچو لام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>لطف شروانشاه جانم بازد داد</p></div>
<div class="m2"><p>رغم آن کو گفت من یحیی العظام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر مکافاتم به حق کردی فلک</p></div>
<div class="m2"><p>صبح عمرم متصل گشتی به شام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بر تنم گشتی عقوبت مستزاد</p></div>
<div class="m2"><p>در دلم ماندی ندامت مستدام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون توانم گفت شکر لطف شاه</p></div>
<div class="m2"><p>کانتظام عمر بادش بر دوام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هم نه در خورد خطا آمد خطاب</p></div>
<div class="m2"><p>هم نه بر حسب ملال آمد ملام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خسرو غازی ملک تاج الملوک</p></div>
<div class="m2"><p>شاه خورشید افسر کیوان حسام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شه منوچهر فریدون کز شرف</p></div>
<div class="m2"><p>شد سپهرش چاکر و گردون غلام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آن جهانداری که این توسن جهان</p></div>
<div class="m2"><p>از ریاضت کردن او گشت رام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بر سریر چرخ و هفت اختر بقدر</p></div>
<div class="m2"><p>پنج نوبت کوفت از شش حرف نام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چرخ توسن چون رمیدن ساز کرد</p></div>
<div class="m2"><p>گشت اقبالش ورا بر سر لگام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عاید از رأیش رسوم افتخار</p></div>
<div class="m2"><p>حاصل از جودش وجود احتشام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ای به اعجاز تو دین را اعتماد</p></div>
<div class="m2"><p>وی به اقبال تو جان را اعتصام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر رزم و بزم دیدندی تو را</p></div>
<div class="m2"><p>سام با شمشیر و جم با رصل و جام</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>جان فشاندی بر سر رطل تو جم</p></div>
<div class="m2"><p>بوسه دادی بر سم اسب تو سام</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از قدر صد قاصد از تو یک رسول</p></div>
<div class="m2"><p>از قضا صد نامه از تو یک پیام</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هر کجا گیرد معسکر دولتت</p></div>
<div class="m2"><p>باشد از نصرت خیام اندر خیام</p></div></div>