---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>با من چو بخندید خوش آن در خوشاب</p></div>
<div class="m2"><p>بر خنده ز شرم دست را کرد نقاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لعل لب او ز پشت دست پرتاب</p></div>
<div class="m2"><p>می تافت چو از جام بلورین مهتاب</p></div></div>