---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>تا گشت رخت روشنی انداز از روز</p></div>
<div class="m2"><p>شب شد روزم، شبم بیفروز از روز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو خوبتری مه به مه و سال و به سال</p></div>
<div class="m2"><p>من زارترم شب ز شب و روز از روز</p></div></div>