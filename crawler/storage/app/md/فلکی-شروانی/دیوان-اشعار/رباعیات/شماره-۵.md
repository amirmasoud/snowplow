---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>دیدار تو اصل نیک پیوندیهاست</p></div>
<div class="m2"><p>طبع تو سرشته از خردمندیهاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با بنده خود موافقت کردی دوش</p></div>
<div class="m2"><p>این خود چه کرم ها و خداوندیهاست</p></div></div>