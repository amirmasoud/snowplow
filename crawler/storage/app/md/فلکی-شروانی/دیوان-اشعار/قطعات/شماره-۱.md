---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>جان را هوای روی تو بر جای جان نشست</p></div>
<div class="m2"><p>مهر توام درون دل مهربان نشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گنج روان توئی و بهر تار موی تو</p></div>
<div class="m2"><p>مار شکنج بر سر گنج روان نشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر دل که از کنار تو برخاست یک زمان</p></div>
<div class="m2"><p>جان داد و از میان جهان بر کران نشست</p></div></div>