---
title: >-
    شمارهٔ ۱ - ترکیب بند
---
# شمارهٔ ۱ - ترکیب بند

<div class="b" id="bn1"><div class="m1"><p>سوری که حور در وی پیرایه برگشاید</p></div>
<div class="m2"><p>رضوان که نثارش عقد گهر گشاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر عزم خدمت او حورا میان ببندد</p></div>
<div class="m2"><p>وز شرم زینت او جوزا کمر گشاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رضوان اگر جنان را هر هشت در ببندد</p></div>
<div class="m2"><p>زین بزم دست دولت هشتاد در گشاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر صبح نزهت از جان زنگی دگر زداید</p></div>
<div class="m2"><p>هر شام عشرت از دل بندی دگر گشاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گه نافهای تبت باد صبا شکافد</p></div>
<div class="m2"><p>گه عقدهای بحرین ابر سحر گشاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مطرب به وزن زیبا نقش نشاط بندد</p></div>
<div class="m2"><p>شاعر به نظم شیرین تنگ شکر گشاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از عکس روی هامون اندر هوای صافی</p></div>
<div class="m2"><p>هر صبحدم تو گوئی سیمرغ پر گشاید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از جرم سنگ خارا تأثیر لطف خسرو</p></div>
<div class="m2"><p>هر ساعتی چو کوثر صد چشمه برگشاید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دوزخ شود بهشتی هر گه زمانه در وی</p></div>
<div class="m2"><p>بگذر اگر ز بزمی این دادگر گشاید</p></div></div>
<div class="b2" id="bn10"><p>شاهیکه درگهش را، چرخ آستانه زیبد</p>
<p>عقد جلال او را گردون میانه زیبد</p></div>
<div class="b" id="bn11"><div class="m1"><p>دست جهانی اکنون با رطل و جام بینی</p></div>
<div class="m2"><p>در دار ملک خسرو دارالسلام بینی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اسباب ملک و ملت بر اتفاق یابی</p></div>
<div class="m2"><p>احوال دین و دولت بر انتظام بینی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از بس که روی نیکو بینی به هر کناری</p></div>
<div class="m2"><p>عاجز شوی ندانی کاول کدام بینی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در پیش هر که اکنون نرد نشاط بازد</p></div>
<div class="m2"><p>بر رقعه تماشا داد تمام بینی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر روز خال نزهت بر روی صبح یابی</p></div>
<div class="m2"><p>هر شب شکنج شادی بر روی شام بینی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن را که بود دایم دعوی پارسائی</p></div>
<div class="m2"><p>اکنون مدام بر کف جام مدام بینی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یعنی که بزم خسرو خلدست بی خلافی</p></div>
<div class="m2"><p>در خلد هر چه یابی بر حسب کام بینی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای زاهد مزور از خود حلال داری</p></div>
<div class="m2"><p>کاندر چنین بهشتی می را حرام بینی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر روز بنده مانا بسته کمر شهان را</p></div>
<div class="m2"><p>در پیش تخت خسرو کسری غلام بینی</p></div></div>
<div class="b2" id="bn20"><p>خاقان دین منوچهر کز یاری سپهرش</p>
<p>در صدر مهر مسند مه پایگانه زیبد</p></div>
<div class="b" id="bn21"><div class="m1"><p>اکنون زمین به خوبی چون آسمان نماید</p></div>
<div class="m2"><p>عالم به وقت پیری خود را جوان نماید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر صورت بهشت است ناممکنست بنگر</p></div>
<div class="m2"><p>کاکنون همی ز خوبی صحرا جنان نماید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تأثیر چرخ و انجم فرق چنین جهان را</p></div>
<div class="m2"><p>بنماید ار نماید در فر آن نماید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از بس که دست خسرو در و گهر فشاند</p></div>
<div class="m2"><p>اطراف بارگاهش چونه بحر و کان نماید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>صحرا ز حله های الوان بهر کناری</p></div>
<div class="m2"><p>طراده از گل و مل از ارغوان نماید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در آفتاب دود عنبر فروغ مجمر</p></div>
<div class="m2"><p>رایت گه از پرند و گه پرنیان نماید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>حکمی که راند گردون در امن ملک شروان</p></div>
<div class="m2"><p>اینک همی به خوبی آن را بیان نماید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هامون اگر ز گردون جوید ز حسن پیشی</p></div>
<div class="m2"><p>برهان آن ز مجلس شاه جهان نماید</p></div></div>
<div class="b2" id="bn29"><p>فرخنده شهریاری دیندار و دادگستر</p>
<p>گو تا زمانه باشد شاه زمانه زیبد</p></div>
<div class="b" id="bn30"><div class="m1"><p>ای بزم شاه شروان چندان جمال داری</p></div>
<div class="m2"><p>کاندر جمال باقی حد کمال داری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شاید که بی خلافت مثل بهشت خوانم</p></div>
<div class="m2"><p>که حسن بی نهایت با خود مثال داری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>فتوی که دادت آخر کایدون میان مردم</p></div>
<div class="m2"><p>بازی مباح کردی باده حلال داری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بر چرخ کامکاری بدری شب طرب را</p></div>
<div class="m2"><p>فارغ ز وصل و هجران نقص و کمال داری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گه انجم لطف را سوی شرف رسانی</p></div>
<div class="m2"><p>گه اختر وبا را اندر وبال داری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بر اتفاق گردون نقص کمال یابی</p></div>
<div class="m2"><p>در اختلاف گیتی بیم زوال داری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یزدان به مذهب من شبه و نظیر دارد</p></div>
<div class="m2"><p>گر تو به هیچ مذهب شبه و همال داری</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همچون مه دو هفته تائی به حسن لیکن</p></div>
<div class="m2"><p>همچون هلال طلعت فرخنده فال داری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شاید که جمله انجم گردون کند نثارت</p></div>
<div class="m2"><p>چون تو ز بزم خسرو زیب و جمال داری</p></div></div>
<div class="b2" id="bn39"><p>آن آفتاب شاهان کاندر محل شاهی</p>
<p>شاهین قدر او را چرخ آشیانه زیبد</p></div>
<div class="b" id="bn40"><div class="m1"><p>شاهی که پای شاهان فرسوده شد ز بندش</p></div>
<div class="m2"><p>آزرده حلق شیران از حلقه کمندش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شد توتیای دولت خاک در سرایش</p></div>
<div class="m2"><p>شد گوشواره گردون نعل سم سمندش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خورشید تیره ماند با خاطر منیرش</p></div>
<div class="m2"><p>افلاک پست باشد با همت بلندش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بنگر به بارگاهش تا همچو مستمندان</p></div>
<div class="m2"><p>شاهان نازنین را بینی نیازمندش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نام خدای بادا وآن فرشتگان هم</p></div>
<div class="m2"><p>بر دست شیر گیرش بازوی دیو بندش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زینسان که دولت او آراست مملکت را</p></div>
<div class="m2"><p>از حادثات گردون کمتر رسد گزندش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ایمن شدی ز دوزخ گر سجده بردی او را</p></div>
<div class="m2"><p>آنک از بهشت باقی یزدان برون فکندش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بزمی نهاد و خوانی کز بهر چشم بد را</p></div>
<div class="m2"><p>سازد سپهر و سوزد گه خز و گه سپندش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>محبوس بین ز یزدان کز بهر چشم بد را</p></div>
<div class="m2"><p>مشمول بین به شادی شهری به دست بندش</p></div></div>
<div class="b2" id="bn49"><p>نواب بارگاهش میری گزیده شاید</p>
<p>فراش پیشگاهش شاهی یگانه زیبد</p></div>
<div class="b" id="bn50"><div class="m1"><p>شاها همیشه دستت با جام باده بارا</p></div>
<div class="m2"><p>وین بزم و خوان همیدون دایم نهاده بادا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>فرزند پنج داری پنجاه باد و آنگه</p></div>
<div class="m2"><p>از هر یکیت پانصد فرزند زاده بادا</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چون تو سوار گردی بر مرکب مبارک</p></div>
<div class="m2"><p>در خدمت رکابت گردون پیاده بادا</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چون تو نشسته باشی بر تخت و تاج بر سر</p></div>
<div class="m2"><p>چون بندگان به پیشت بخت ایستاده بادا</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>در هر چه رای داری وز هر چه کام یابی</p></div>
<div class="m2"><p>از گشت چرخ و انجما داد تو داده بادا</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>هست آفت فلک را ره بسته زی در تو</p></div>
<div class="m2"><p>بر خلق عالم این در دایم گشاده بادا</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>سرهای سرکشانی کز تو کشند کینه</p></div>
<div class="m2"><p>در پای مرکبانت پست اوفتاده بادا</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>درگاهت از بلندی با چرخ باد همسر</p></div>
<div class="m2"><p>بام سرای خصمت با بوم ساده بادا</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>از رشک این که داری پرباده جام بر کف</p></div>
<div class="m2"><p>پر خون دل حسودت چون جام باده بادا</p></div></div>
<div class="b2" id="bn59"><p>بعد از ثنات شاها گویم یکی غزل خوش</p>
<p>کز قول بوالفتوحش قول ترانه زیبد</p></div>
<div class="b" id="bn60"><div class="m1"><p>ای زیر زلف پرچین ار تنگ چین نهاده</p></div>
<div class="m2"><p>مه زآسمان به پیشت رخ بر زمین نهاده</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>یا آنکه نیست بر تو کس مهربان تر از من</p></div>
<div class="m2"><p>با جان من فراقت بنیاد کین نهاده</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>در خلد کرد رضوان شکرانه بر جمالت</p></div>
<div class="m2"><p>در حسن صد غرامت بر حور عین نهاده</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>در طاق ابروی تو نرگس کمان کشیده</p></div>
<div class="m2"><p>داند ز خم کمانت جادو کمین نهاده</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>خوبان پاک دامن مانده بر آستانت</p></div>
<div class="m2"><p>هر یک ز جان نثاری در آستین نهاده</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چرخ آنقدر حلاوت از لعل نوشخندت</p></div>
<div class="m2"><p>اندر شکر سرشته در انگبین نهاده</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>آن خال بر رخ تو گوئی که هست عمدا</p></div>
<div class="m2"><p>از مشک نقطه نور بر یاسمین نهاده</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>الحق که شکر باشد با چون تو دلنوازی</p></div>
<div class="m2"><p>بر مرکب تماشا ز اقبال زین نهاده</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>شاهی چنین نشسته وقتی چنین رسیده</p></div>
<div class="m2"><p>بزمی چنین فکنده خوانی چنین نهاده</p></div></div>
<div class="b2" id="bn69"><p>ای یادگار شاهان در ملک جاودان مان</p>
<p>کآن مملکت تو داری گو جاودانه زیبد</p></div>