---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>گر پخته نصیب پختگان است</p></div>
<div class="m2"><p>ما سوخته ایم جام در ده</p></div></div>