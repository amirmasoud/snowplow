---
title: >-
    شمارهٔ ۵ - ترجیع بند در مدح و میلاد قطب عالم امکان امام زمان عجل الله تعالی فرجه الشریف
---
# شمارهٔ ۵ - ترجیع بند در مدح و میلاد قطب عالم امکان امام زمان عجل الله تعالی فرجه الشریف

<div class="b" id="bn1"><div class="m1"><p>وقت صبوحست ای به جسم جهان روح</p></div>
<div class="m2"><p>خیز که شد باز، باب میکده مفتوح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبحک الله باکرامة والخیر</p></div>
<div class="m2"><p>راح مروق بسیار و رایحه روح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مؤذن میخوارگان خروس خوش الحان</p></div>
<div class="m2"><p>آیه قدوس خواند از پی سبوح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناله شب زنده دار و آه سحرخیز</p></div>
<div class="m2"><p>از دل خونین خوش است و سینه مجروح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نی نی خاک درتو حله جودیست</p></div>
<div class="m2"><p>فایده حاصله مثلث بدوح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای بت سرمست وی نگار قوی دست</p></div>
<div class="m2"><p>ای دو جهان مادح و تو بر همه ممدوح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در شب میلاد شاه حاضر غائب</p></div>
<div class="m2"><p>انجمن قدس راست شأن تو مطروح</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ذکر جهان و جهانیان شده یکسر</p></div>
<div class="m2"><p>این سخن جان فزا مفصل و مشروح</p></div></div>
<div class="b2" id="bn9"><p>کعبه مقصود گشت قبله مسجود</p>
<p>مهد زمین از ظهور مهدی موعود</p></div>
<div class="b" id="bn10"><div class="m1"><p>زد علم عدل یار باز به عالم</p></div>
<div class="m2"><p>بر همه عالم فکنده پرتو پرچم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سر نهان آشکار گشت به دنیا</p></div>
<div class="m2"><p>شاهد معنی ظهور کرد در این دم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>محتسب عدل کرد شحنه انصاف</p></div>
<div class="m2"><p>هفت قلم را به یک نظام منظم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از دم روح القدس دوباره عیان شد</p></div>
<div class="m2"><p>طلعت عیسی به مهد دامن مریم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تافت ز نو، بر فلک ستاره موسی</p></div>
<div class="m2"><p>نخوت فرعون شد ز تابش او کم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زآتش نمرود و قهر آتش شداد</p></div>
<div class="m2"><p>رست رسول خدا خلیل مکرم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کشتی نوح است در برابر جودی</p></div>
<div class="m2"><p>ماهی یونس پدید شد زدل یم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نی نی نوبت زن زمانه فرو کوفت</p></div>
<div class="m2"><p>نوبت دولت به نام شاه معظم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مهدی هادی امام حاضر غائب</p></div>
<div class="m2"><p>قائم دائم وصی آدم و خاتم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شد متولد زمام آن ولدی کش</p></div>
<div class="m2"><p>قابله حوا و شوی قابله آدم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ابر هدایت چنان به کعبه ببارید</p></div>
<div class="m2"><p>کاب زمیزاب ریخت در چه زمزم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای تو بقوت به ممکنات مسلط</p></div>
<div class="m2"><p>وی تو بقدرت به کائنات مسلم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در شب میلاد حضرت تو، به ایران</p></div>
<div class="m2"><p>انجمنی کرده بزم قدس فراهم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>انجمن قدسیان به مجلس قدس است</p></div>
<div class="m2"><p>کش پی تعظیم گشته پشت فلک خم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از لب روحانیان عالم بالا</p></div>
<div class="m2"><p>این سخن آید به گوش هوش دمادم</p></div></div>
<div class="b2" id="bn25"><p>کعبه مقصود گشت قبله مسجود</p>
<p>مهد زمین از ظهور مهدی موعود</p></div>
<div class="b" id="bn26"><div class="m1"><p>ساقی روحانیان عالم بالا</p></div>
<div class="m2"><p>ریخت به ساغر شراب ناب تولا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از خم وحدت کشید ساغر اول</p></div>
<div class="m2"><p>کرد ظهور دومت به کثرت اشیا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>صبح به میخوارگان صلای صبوحی</p></div>
<div class="m2"><p>داد، بغمز و برمز عشوه و ایما</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اقترب الساعة الصبوح فقومو</p></div>
<div class="m2"><p>کز دم روح الله است راح مصفا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>صبح ازل گشت شام، شام ابد صبح</p></div>
<div class="m2"><p>صبح دوم شد پدید از شب یلدا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>صبح سعادت طلوع کرد زمشرق</p></div>
<div class="m2"><p>شمس حقیقت نمود طلعت زیبا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>صبح چنین ای ندیم باز چه خسبی</p></div>
<div class="m2"><p>نوبت بیداری است و وقت تماشا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خیز که شمعی وجود یافت به عالم</p></div>
<div class="m2"><p>خیز که شد ساعت ولادت مولا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خیز که نوبت زن زمانه فرو کوفت</p></div>
<div class="m2"><p>نوبت دولت به نام داور دنیا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>قائم موعود شهریار مظفر</p></div>
<div class="m2"><p>مهدی هادی امام حی توانا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>انکه بدین شعر دلکشش بسرایند</p></div>
<div class="m2"><p>مجمع کروبیان عالم بالا</p></div></div>
<div class="b2" id="bn37"><p>کعبه مقصود گشت قبله مسجود</p>
<p>مهد زمین از ظهور مهدی موعود</p></div>
<div class="b" id="bn38"><div class="m1"><p>عرشه عرش است یا که روضه رضوان</p></div>
<div class="m2"><p>خلوت قدس است یا مدینه ایمان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سینه سیناست یا که وادی ایمن</p></div>
<div class="m2"><p>صحنه صنعاست یا که ساحت کنعان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بیت حرام است یا که بیت مقدس</p></div>
<div class="m2"><p>مسجد اقصی است یا که معبد رهبان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>محضر شیث است یا که مدرس ادریس</p></div>
<div class="m2"><p>خیمه داود یا بساط سلیمان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خانقه عدل یا که مصطبه عشق</p></div>
<div class="m2"><p>منزل جان است یاکه محفل جانان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بزم حقیقت و یا مقام طریقت</p></div>
<div class="m2"><p>عالم معنی است یا قلمرو عرفان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>باغ ارم یا فزای خلد مخلد</p></div>
<div class="m2"><p>جنت شداد یا حدیقه رحمان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تبت تاتار یا که خلخ فرخار</p></div>
<div class="m2"><p>کشور چین است یا طراز بدخشان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کاخ خورنق و یا که صرح ممرد</p></div>
<div class="m2"><p>جرگه بهرام یا که خرگه نعمان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>قطعه شعر است یا که آیه منزل</p></div>
<div class="m2"><p>صفحه نصر است یا که سوره فرقان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>صفحه ارژنگ یا که کبک دساتیر</p></div>
<div class="m2"><p>نامه زید است یا نصایح لقمان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سوره نور است یا تلاوت تورات</p></div>
<div class="m2"><p>نغمه انجیل یا قرائت قرآن</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>مصرع هر بیت تیر دیده دجال</p></div>
<div class="m2"><p>مطلع هر شعر تیغ تارک شیطان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>آن شب قدری که قر اوست معظم</p></div>
<div class="m2"><p>هست شب نیمه مبارک شعبان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>انجمنی کرده ماه انجمنی را</p></div>
<div class="m2"><p>انجمنی چون فلک زانجم تابان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>انجمنی این چنین تمام سخن سنج</p></div>
<div class="m2"><p>انجمنی این چنین تمام سخندان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>انجم این انجمن تمام غزل گو</p></div>
<div class="m2"><p>انجم این انجمن تمام غزل خوان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>انجم این انجمن مسلم آفاق</p></div>
<div class="m2"><p>انجم این انجمن مشخص دوران</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>انجم این انجمن خلاصه کونین</p></div>
<div class="m2"><p>انجم این انجمن سرآمد کیهان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>انجم این انجمن مدیر زمانه</p></div>
<div class="m2"><p>انجم این انجمن مؤید ایران</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>انجم این انجمن تمام سرایند</p></div>
<div class="m2"><p>این سخن جان فزا چو بلبل دستان</p></div></div>
<div class="b2" id="bn59"><p>کعبه مقصود گشت قبله مسجود</p>
<p>مهد زمین از ظهور مهدی موعود</p></div>
<div class="b" id="bn60"><div class="m1"><p>شمس حقیقت امام حاضر غائب</p></div>
<div class="m2"><p>در خور حمد و ثنا و مدح و مناقب</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>خدمت اوراست جبرئیل مواظب</p></div>
<div class="m2"><p>درگه اوراست روح قدس مراقب</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>رتبت درگاه او سپهر مطبق</p></div>
<div class="m2"><p>قبه خرگاه او نجوم ثواقب</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>آدم و نوح و خلیل عیسی و موسی</p></div>
<div class="m2"><p>راجل و راکب ورا دوان به مواکب</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>در شب میلاد با سعادت او شد</p></div>
<div class="m2"><p>اختر دجال شوم ساقط و خائب</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>اوست چه شهباز و خلق جمله چه عصفور</p></div>
<div class="m2"><p>اوست چه ضرغام و جیش خصم ثعالب</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ای همه مقهور و تو بر همه قاهر</p></div>
<div class="m2"><p>وی همه مغلوب و تو بر همه غالب</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>مدح سرای تو عالمند ولیکن</p></div>
<div class="m2"><p>گوی سعادت ربوده از همه «حاجب »</p></div></div>
<div class="b2" id="bn68"><p>کعبه مقصود گشت قبله مسجود</p>
<p>مهد زمین از ظهور مهدی موعود</p></div>
<div class="b" id="bn69"><div class="m1"><p>ای زده در عرش کبریا علم کوس</p></div>
<div class="m2"><p>وی ز تو بانگ اذان و نغمه ناقوس</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>بنده فرمان تست دادگر روم</p></div>
<div class="m2"><p>چاکر دربان تست پادشه روس</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>دشمن جاه تو مبتلاست شب و روز</p></div>
<div class="m2"><p>بر، سل و دق بضرع سکته و کابوس</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>کوی تو خلد مخلد است به تحقیق</p></div>
<div class="m2"><p>دشمن تو مارملک چون پر طاووس</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ای خلف ارشد ستوده آدم</p></div>
<div class="m2"><p>سنگ قصاصش بزن به سینه ناموس</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>عصر ظهور تو گشت بر همه عالم</p></div>
<div class="m2"><p>روز قیام تو گشت بر همه محسوس</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ای شه آفاق گیر عصر ظهور است</p></div>
<div class="m2"><p>چند نهی ملک و دین به دشمن منحوس</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>آی و به یادآور آن زمان که شه دین</p></div>
<div class="m2"><p>جد گرامت شدی ز یاران مأیوس</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>یکه و تنها میان لشگر کفار</p></div>
<div class="m2"><p>نه پسر و نه پدر نه یار و نه مأنوس</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>تشنه لب و داغدار زخم فراوان</p></div>
<div class="m2"><p>مرهم او تیغ تیز و غلغله کوس</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>تشنگیش زد شرر به عالم هستی</p></div>
<div class="m2"><p>آبش باران تیر شد به صد افسوس</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>ذکر ملائک شد است این سخن من</p></div>
<div class="m2"><p>هر که بخواند شود ز حادثه محروس</p></div></div>
<div class="b2" id="bn81"><p>کعبه مقصود گشت قبله مسجود</p>
<p>مهد زمین از ظهور مهدی موعود</p></div>