---
title: >-
    شمارهٔ ۳ - درد بی دوا
---
# شمارهٔ ۳ - درد بی دوا

<div class="b" id="bn1"><div class="m1"><p>گفتم به یار غمزه چشم تو دلرباست</p></div>
<div class="m2"><p>لعل تو شکر است کلام تو جان فزاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلف تو عنبر است ز کوتش به ما رواست</p></div>
<div class="m2"><p>ای نازنین صنم دل تو مایل جفاست</p></div></div>
<div class="b2" id="bn3"><p>عمر عزیز ماست چه حاصل که بی بقاست</p></div>
<div class="b" id="bn4"><div class="m1"><p>یکدم ز دام عشق تو جانا رها شدم</p></div>
<div class="m2"><p>بنگر چسان به درد و الم مبتلا شدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بهر تو ز قوم و ز خویشان جدا شدم</p></div>
<div class="m2"><p>«تنها نه من به خال لبت مبتلا شدم »</p></div></div>
<div class="b2" id="bn6"><p>«بر هر که بنگری به همین درد مبتلاست»</p></div>
<div class="b" id="bn7"><div class="m1"><p>از بهر تو مدام اسیرم به درد و غم</p></div>
<div class="m2"><p>جور و جفا بس است رها کن مرا زغم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زلف سیاه تست چه پرپیچ و پر زخم</p></div>
<div class="m2"><p>چشمت گر اندکی به سیاهی زند رقم</p></div></div>
<div class="b2" id="bn9"><p>فیروزه را نظر چه کنی دیده را جلاست</p></div>
<div class="b" id="bn10"><div class="m1"><p>تکذیب غیر، نقص کمالت نمی‌شود</p></div>
<div class="m2"><p>ماه بلند همچو هلالت نمی‌شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ابروی زرد نقص جمالت نمی‌شود</p></div>
<div class="m2"><p>... الت نمی‌شود</p></div></div>
<div class="b2" id="bn12"><p>سر سوره کلام خدا اکثرش طلاست</p></div>
<div class="b" id="bn13"><div class="m1"><p>ترسم از آنکه مرگ هوای دلم کند</p></div>
<div class="m2"><p>تب عارضم گرفته اجل غافلم کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>غسال شوید و بر دو در گلم کند</p></div>
<div class="m2"><p>رفتم بر طبیب علاج دلم کند</p></div></div>
<div class="b2" id="bn15"><p>آهی کشید و گفت که این درد بی دواست</p></div>
<div class="b" id="bn16"><div class="m1"><p>چشم طمع بپوش تو ایدل از این جهان</p></div>
<div class="m2"><p>پیمانه پر کنیم چه از پیر و از جوان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>روزی به عزم سیر به صحرا شدم روان</p></div>
<div class="m2"><p>صیاد می دوید و اجل از پیش دوان</p></div></div>
<div class="b2" id="bn18"><p>گفتم مرو مرو که تو را مرگ در قفاست</p></div>
<div class="b" id="bn19"><div class="m1"><p>ساقی ز جای خیز و میم در پیاله کن</p></div>
<div class="m2"><p>بر رغم مدعی ز غمم آه و ناله کن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ما را به خاندان مروت حواله کن</p></div>
<div class="m2"><p>«حاجب » به خم باده وحدت غساله کن</p></div></div>
<div class="b2" id="bn21"><p>گر زین میانه جام ببرم شاهدم خداست</p></div>