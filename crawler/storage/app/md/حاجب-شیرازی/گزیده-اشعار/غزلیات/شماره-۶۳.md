---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>دادن جان گرچه بر ابناء عالم شاق بود</p></div>
<div class="m2"><p>لیک عاشق این هنر را، از ازل مشتاق بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طاق ابروی تو را نازم که افتاده است طاق</p></div>
<div class="m2"><p>به به از طاقی که جفتش در حقیقت طاق بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نوش وصل و نیش هجرانت به میزان درست</p></div>
<div class="m2"><p>جان گزا، چون زهر و روح افزای چون تریاق بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه بیرون ز، انفس و آفاق جویا بودمش</p></div>
<div class="m2"><p>چون بگردیدم عیان در انفس و آفاق بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مصحف ما را چنان شیرازه زد صحاف عقل</p></div>
<div class="m2"><p>کاسمانی نامه ها پیشش همه اوراق بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حادث مطلق وجودم محترم را بازگو</p></div>
<div class="m2"><p>تا که بد ذات قدم درگردش این نه طاق بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنکه سرو کاشمر را تیشه زد بر ریشه گفت</p></div>
<div class="m2"><p>این حسود قامت آن سرو سیمین ساق بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با کفت تشبیه می کردم به همت بحر و کان</p></div>
<div class="m2"><p>بحر اگر رزاق بود و کان اگر خلاق بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در بساط قرب جانان با تو هر عهدی که بست</p></div>
<div class="m2"><p>خود تو بشکستی درست او بر سر میثاق بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از حجاز آن ترک شهر آشوب چون شد در عراق</p></div>
<div class="m2"><p>یافت هر، شور و نوا، در پرده عشاق بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حرف حق زد، دوش «حاجب » صوفئی از خانقاه</p></div>
<div class="m2"><p>پاسخش تا صبحدم با زهره و مطراق بود</p></div></div>