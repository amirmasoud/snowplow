---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>هر که زد بر آب و آتش حرق و غرقش باک نیست</p></div>
<div class="m2"><p>تنبل و منبل براه عاشقی چالاک نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکه را در عاشقی از سر رود سودای وصل</p></div>
<div class="m2"><p>سر بزیر است ار سر او نیزه و فتراک نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه در شبهای هجران ریخت سیل خون ز چشم</p></div>
<div class="m2"><p>زیر تیغ از دادن جان دیده اش نمناک نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پاکبازی در قمار دوستی مردانگی است</p></div>
<div class="m2"><p>بدقماری در حقیقت کار مرد پاک نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرو، و بید از بار آزادند با آن اعتبار</p></div>
<div class="m2"><p>باغ را پست و بلندی به ز نخل و تاک نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن بود سر، یا کدوی خشک کز سودا تهیست؟</p></div>
<div class="m2"><p>سینه یا سنگ است آن کز تیر عشقی چاک نیست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم پر آز و دل پر آرزو را در جهان</p></div>
<div class="m2"><p>بهره ای جز باد حسرت توشه ای جز خاک نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پاک مردان جهان تریاکی حسن تواند</p></div>
<div class="m2"><p>هیچ تریاقی به عالم به از این تریاک نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مدرک ار حیوان به کلیات نبود عیب نیست</p></div>
<div class="m2"><p>کم ز حیوان آدمی زادیست کش ادراک نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر در افلاک است چون خورشید یا مه آیتی</p></div>
<div class="m2"><p>کوکبی «حاجب » به کف دارد که در افلاک نیست</p></div></div>