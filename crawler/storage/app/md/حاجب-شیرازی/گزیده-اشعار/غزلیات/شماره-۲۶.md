---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>پرد، در عرش مرغ جانم امشب</p></div>
<div class="m2"><p>که آید در برم جانانم امشب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>الا صیاد دنیا ای ستمگر</p></div>
<div class="m2"><p>از این دام و قفس برهانم امشب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وصال یار را خواهانم امروز</p></div>
<div class="m2"><p>بساط قدس را مهمانم امشب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود روح الامین فراشم امروز</p></div>
<div class="m2"><p>شود روح القدس دربانم امشب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جنت حور و غلمان کرده زینت</p></div>
<div class="m2"><p>که یک یک بگذرند از سانم امشب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فلک ثور و حمل را کرده حاضر</p></div>
<div class="m2"><p>که آن دو را کند قربانم امشب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ملایک چون صف کروبیان باز</p></div>
<div class="m2"><p>ستاده اند بر ایوانم امشب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زمن عمر ابد دارد طمع خضر</p></div>
<div class="m2"><p>به ظلمت چشمه حیوانم امشب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بود بر کف پی تشریف اصحاب</p></div>
<div class="m2"><p>کلید روضه رضوانم امشب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هزاران لیله اسری شب قدر</p></div>
<div class="m2"><p>بود تا صبحگه حیرانم امشب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شب وصل است «حاجب » تا سحرگه</p></div>
<div class="m2"><p>ز جانان کام دل بستانم امشب</p></div></div>