---
title: >-
    شمارهٔ ۱۴۶
---
# شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>خیزید شمع وحدت روشن کنید روشن</p></div>
<div class="m2"><p>بر تن ز نور معنی جوشن کنید جوشن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سرو قد موزون وز، رنگ روی گلگون</p></div>
<div class="m2"><p>امروز بزم ما را روشن کنید روشن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بذری است صلح نافع، نخلی است عدل مثمر</p></div>
<div class="m2"><p>درکشت زار خاطر کشتن کنید کشتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بانگ اناالحق آمد یا حق مطلق آمد</p></div>
<div class="m2"><p>چون مرغ حق تن از دار آون کنید آون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دربند چرخه دوک اعدا چه پیر زالند</p></div>
<div class="m2"><p>کو پنبه گردد آخر رشتن کنید رشتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشتیم بذر معنی دادیم آبش از خون</p></div>
<div class="m2"><p>وقت درو، رسیده خرمن کنید خرمن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نفس اژدری است غژمان این خصم خانگی را</p></div>
<div class="m2"><p>یاران حواله تیغ گردن کنید گردن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای کودکان دنیا مجنون و منکم عقل</p></div>
<div class="m2"><p>از سنگ طعنه خالی دامن کنید دامن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در قتلگاه مردم در زیر تیغ قاتل</p></div>
<div class="m2"><p>گر عاشقید و جان باز مردن کنید مردن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نی همچو گوسفندان در زیر تیغ قصاب</p></div>
<div class="m2"><p>بیچاره وار و ناچار مردن کنید مردن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دوران صلح و عدل است هنگام عفو و جود است</p></div>
<div class="m2"><p>هر جنگ و هر جدالیست با من کنید با من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تیغ زبان «حاجب » بشکست جیش دجال</p></div>
<div class="m2"><p>ای لشگر عزازیل شیون کنید شیون</p></div></div>