---
title: >-
    شمارهٔ ۷۶
---
# شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>دلم چو صعوه به زلف تو آشیان دارد</p></div>
<div class="m2"><p>که آشیانه سیمرغ زیر، ران دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امید دانه خالت به دام زلف کشید</p></div>
<div class="m2"><p>خوش است دام که این دانه در میان دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دهان تنگ تو چون غنچه هر زمان بشکفت</p></div>
<div class="m2"><p>به عارضت گل و مل رنگ و بو از آن دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی حقیقت حسن تو را نکرد انکار</p></div>
<div class="m2"><p>یکی یقین نبود دیگری گمان دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گذشت مژه ات از، ابروان و چشمت گفت</p></div>
<div class="m2"><p>خوش آنکه زخمی از این تیر و آن کمان دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به درگه تو، کرا، دست و لب رسد حاشا</p></div>
<div class="m2"><p>که آسمان سر خدمت در آستان دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکفته شد گل و مغز جهان معطر شد</p></div>
<div class="m2"><p>ز شوق بلبل دل ناله و فغان دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ببین به دولت اردیبهشت و وضع بهشت</p></div>
<div class="m2"><p>کجا بهشت چنین لاله ارغوان دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گلی به سایه نیلوفر است، یا که رخت</p></div>
<div class="m2"><p>همه ز طره شبرنگ سایه بان دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز صلح گشت دی از راستی خجسته بهار</p></div>
<div class="m2"><p>کز اعتدال جهان باده بوی جان دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به نظم و نثر تو «حاجب » هر آنکه خورده گرفت</p></div>
<div class="m2"><p>سر عناد و عداوت به یک جهان دارد</p></div></div>