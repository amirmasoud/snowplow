---
title: >-
    شمارهٔ ۱۱۸
---
# شمارهٔ ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>دلدار دوست ترک سفر کرد ساز باز</p></div>
<div class="m2"><p>یا رب بوصل او سبب خیر ساز باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساز سفر به شهر صفر کرد باز یار</p></div>
<div class="m2"><p>ای کاش باز از سفر آید صحیح و ساز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوته چو روز وصل بود سال و ماه عمر</p></div>
<div class="m2"><p>شرح غم تو و شب هجران بود دراز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقی تو باز کن سر مینا که بازگشت</p></div>
<div class="m2"><p>ابواب عدل باز و ره جنگ و کین فراز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کعبه ایم و مرحله پیمای کوی تو</p></div>
<div class="m2"><p>مستغرق حقیقت و آلوده مجاز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم سیاه مست تو یغمای دین کند</p></div>
<div class="m2"><p>آری به ترک می سزد آئین ترکتاز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محراب ابروان بنما پیش عاشقان</p></div>
<div class="m2"><p>تا کس به قبقبه نبرد بعد از این نماز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منسوخ شد جراز و محن در زمان تو</p></div>
<div class="m2"><p>ای عارض تو همچو محن ابروان جراز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رخ برفروخت لاله، تو رخ نیز برفروز</p></div>
<div class="m2"><p>قد برفراخت سرو تو قد باز برفراز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>«حاجب » نیازمند تو را کبر و ناز نیست</p></div>
<div class="m2"><p>نازت کشم از آنکه توئی قبله نیاز</p></div></div>