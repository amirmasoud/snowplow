---
title: >-
    شمارهٔ ۸۰
---
# شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>آمد به فرخندگی، روز نجات ابد</p></div>
<div class="m2"><p>روز نجات ابد، آمد حیات ابد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کی قیام و قعود، تا کی رکوع و سجود</p></div>
<div class="m2"><p>کز فیض رب و دود، آمد صلات ابد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زد موج بحر ولا، بارید ابر بلا</p></div>
<div class="m2"><p>ای تشنگان الصلا سوی فرات ابد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در جمع افتادگان در بزم دلدادگان</p></div>
<div class="m2"><p>از دست آزادگان بستان برات ابد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از حق براتی طلب وز حق نجاتی طلب</p></div>
<div class="m2"><p>از حق حیاتی طلب روز ممات ابد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان برخی یار کن دل محو دلدار کن</p></div>
<div class="m2"><p>رو جلوه بر، دار کن اینت حیات ابد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>«حاجب » پی صلح کل پیوسته کوبد دهل</p></div>
<div class="m2"><p>ای سالکان سبل اینک صفات ابد</p></div></div>