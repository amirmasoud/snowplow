---
title: >-
    شمارهٔ ۱۵۱
---
# شمارهٔ ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>سنجند اگر قدرت حسنت به ترازو</p></div>
<div class="m2"><p>سنگینی عالم بودت وزن یکی مو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمان و دو ابروت دو شیرند و دو شمشیر</p></div>
<div class="m2"><p>من شیر ندیدم که بود در صفت آهو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیمین ذقنت گوی بود زلف تو چوگان</p></div>
<div class="m2"><p>پشت همه خم گشته چو چوگان بر آن گو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خون همه عشاق روان کرد و هدر داد</p></div>
<div class="m2"><p>چشمان سیه مست تو و نرگس جادو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در علم بود فخر نه در، ریش مطول</p></div>
<div class="m2"><p>گیرم گذرد ریش عوام از سر زانو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ریش بری از دانش و دست تهی از جود</p></div>
<div class="m2"><p>آن ریش چو جارو بود آن دست چو پارو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دنیا نبود یک سر مو قابل تمجید</p></div>
<div class="m2"><p>بگذر تو از این خیره سر ناکس جادو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شب مرغ حقم روز نمایند کبوتر</p></div>
<div class="m2"><p>پر کرده جهان را همه از حق حق و هوهو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کوکو، به لب دجله بغداد همی گفت</p></div>
<div class="m2"><p>کو نغمه چنگیزی و کو عزم هلاکو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>«حاجب » خبر صلح دهد در وسط جنگ</p></div>
<div class="m2"><p>با نغمه موزیک و دف و چنگ و پیانو</p></div></div>