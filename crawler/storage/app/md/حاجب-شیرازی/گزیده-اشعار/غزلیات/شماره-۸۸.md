---
title: >-
    شمارهٔ ۸۸
---
# شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>پرده وهم برانداز بتا، تات ببینند</p></div>
<div class="m2"><p>تا که سیرت عرب و کرد و لر وفات ببینند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مات مائیم چو آئینه به رخسار دلارات</p></div>
<div class="m2"><p>کاش در ظاهر و باطن هم چون مات ببینند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به سر و پای تو سوگند که بی پا و سران را</p></div>
<div class="m2"><p>سر آن است که پیوسته سراپات ببینند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکرافشان، که همه لعل شکرخات ببوسند</p></div>
<div class="m2"><p>روی بنما که همه طلعت زیبات ببینند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای به بالا همه والا و به قامت چو قیامت</p></div>
<div class="m2"><p>قد برافراز که در عالم بالات ببینند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پایداری کن و پا بر سر این دار فنا، نه</p></div>
<div class="m2"><p>تا که بردار حواری چو مسیحات ببینند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>«حاجبا» از حجب وهم برون آی خدا را</p></div>
<div class="m2"><p>تا که بین همه با دیده بینات ببینند</p></div></div>