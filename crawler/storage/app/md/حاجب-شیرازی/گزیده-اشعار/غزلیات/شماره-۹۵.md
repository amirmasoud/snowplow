---
title: >-
    شمارهٔ ۹۵
---
# شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>ای آستانت خلد مخلد</p></div>
<div class="m2"><p>وی پاسبانت عقل مجرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر جنابت جیش معظم</p></div>
<div class="m2"><p>وندر رکابت جند مجند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مبنای عزمت به از سکندر</p></div>
<div class="m2"><p>کز دست بنهاد سدی مسدد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا شبنم فیض باری به رویش</p></div>
<div class="m2"><p>بشکفت در باغ ورد مورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از قید و بندم دشمن مترسان</p></div>
<div class="m2"><p>شیر است در بند باز است مردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>متروک کردی زایمای ابرو</p></div>
<div class="m2"><p>با، بی کمانی تیغ مهند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از صلح پیچید هر جنگجو، سر</p></div>
<div class="m2"><p>در شرع عشقش، باید زدن حد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فرقی که ساید بر مرقد تو</p></div>
<div class="m2"><p>فرقی ندارد از فرق مرقد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فخر از تو دارند جد و اب و ام</p></div>
<div class="m2"><p>مجد از تو یابند ام و اب و جد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شق حجب کرد انگشت «حاجب »</p></div>
<div class="m2"><p>شق القمر کرد گردست احمد</p></div></div>