---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>ماه در، ابر بماند چو رخت جلوه نماید</p></div>
<div class="m2"><p>غیر خورشید کسی حسن تو جانا نستاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یار اگر غیر گشاید در تزویر ببندد</p></div>
<div class="m2"><p>ور ببندد، ره انصاف بقدرت بگشاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جر اثقال بود حسن تو، بی شبهه و دل را</p></div>
<div class="m2"><p>گر بود کوه گران سنگ چو کاهی برباید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس عجب دارم از آن کس که به دوران تو میرد</p></div>
<div class="m2"><p>زانکه دیدار، تو جان پرورد و عمر فزاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صلح کل نزع و صلاح از همم خالی خود کن</p></div>
<div class="m2"><p>کاین چنین همت مردانه ز شخص تو برآید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عارضت آینه مهر و مه ار نیست لبت چیست</p></div>
<div class="m2"><p>زنگ ز آئینه دلها بنگاهی بزداید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگس نحل ز گل گو بپرد، باز پس آید</p></div>
<div class="m2"><p>هر که با، پا رود از کوی تو بی شک به سر آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای گل واحد گلزار حقیقت که به عالم</p></div>
<div class="m2"><p>بلبلی نیست که جز حمد تو مدحی بسر آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنکه در پرده پندار نهان بود چو «حاجب »</p></div>
<div class="m2"><p>برقع از روی براندازد و خود را بنماید</p></div></div>