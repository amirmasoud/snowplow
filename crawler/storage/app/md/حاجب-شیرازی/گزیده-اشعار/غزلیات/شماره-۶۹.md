---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>یار آنقدر، به حسن بنازید و ناز کرد</p></div>
<div class="m2"><p>کش روزگار هستی خود را نیاز کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با حقه بازیش فلک حقه باز باخت</p></div>
<div class="m2"><p>باید قمار با فلک حقه باز کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زاهد ز کعبه رخت به دیر مغان کشید</p></div>
<div class="m2"><p>تبدیل بر حقیقت مطلق مجاز کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر مسجد است میکده و قبله خم می</p></div>
<div class="m2"><p>باید به هفت وقت به مستی نماز کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاهد بیا، به مسجد و میخوارگان ببین</p></div>
<div class="m2"><p>کاین خانه را خدا ز، ریا بی نیاز کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا این سرای را، در رحمت نمود باز</p></div>
<div class="m2"><p>درهای کذب و نکبت و زحمت فراز کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دانست عشق من ذهب خالصم به عهد</p></div>
<div class="m2"><p>چون کوره گشت بوته خود شکل گاز کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کوتاه پای ظلم و دراز است دست عدل</p></div>
<div class="m2"><p>باید نشست راحت و پائی دراز کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دهر مشعبد فلک حقه باز، را</p></div>
<div class="m2"><p>حق دست بست و بسته ارباب راز کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جز آرزو، چه صرفه و سود، از جهان برد</p></div>
<div class="m2"><p>آن کس که عمر صرف طمع وقت آز کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>«حاجب » بگوی مردم آزاده را خدای</p></div>
<div class="m2"><p>ممتاز کرد و قابل هر امتیاز کرد</p></div></div>