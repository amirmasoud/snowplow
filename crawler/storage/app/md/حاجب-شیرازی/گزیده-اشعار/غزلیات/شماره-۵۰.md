---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>دوش در برداشتم خورشید، ماه آمد گذشت</p></div>
<div class="m2"><p>ماه را تصویر می کردم که شاه آمد گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من جهاد و جنگ را تغییر می دادم به صلح</p></div>
<div class="m2"><p>کز من آن ترک سپاهی با سپاه آمد گذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش تیغش جان سپر کردم ولیک آن جنگجو</p></div>
<div class="m2"><p>با کمان ابرو و تیر نگاه آمد گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده با دل گفت روز وصل یار آمد پدید</p></div>
<div class="m2"><p>دل به جان می گفت شام هجر آه آمد گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوش دیدار مه نو، مشتبه شد بر همه</p></div>
<div class="m2"><p>و آن هلال ابرو، به دفع اشتباه آمد گذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر گنه غیر ازجفا و ظلم و کین بخشیدنی است</p></div>
<div class="m2"><p>آنکه از یک آه بخشد صد گناه آمد گذشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من پناه و پشت خوبانم به معنی در جهان</p></div>
<div class="m2"><p>تا نپندارد کسی پشت و پناه آمد گذشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با حریف عشق نر دیدم سر بر تافت عقل</p></div>
<div class="m2"><p>دعوی جان باختن کردم گواه آمد گذشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رفت صبح روشن وصل و شب تاریک هجر</p></div>
<div class="m2"><p>آن سپیداندام با زلف سیاه آمد گذشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یوسفان مصر معنی را بشیر، از ما بگو</p></div>
<div class="m2"><p>آنکه بخشد جاه و عزت قعر چاه آمد گذشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>داد مظلومان بخواهد «حاجب » از ظالم بحکم</p></div>
<div class="m2"><p>تا بگویند اهل صورت دادخواه آمد گذشت</p></div></div>