---
title: >-
    شمارهٔ ۹۸
---
# شمارهٔ ۹۸

<div class="b" id="bn1"><div class="m1"><p>جور اغیار و غم فرقت یار آخر شد</p></div>
<div class="m2"><p>عهد ناکامی عشاق فکار آخر شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خم بجوش آمد و برخواست ز میخانه خروش</p></div>
<div class="m2"><p>ابشرو، باده کشان دور خمار آخر شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقیا صبحک الله بده جام صبوح</p></div>
<div class="m2"><p>تا بدانند حریفان شب تار آخر شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو بمان تازه بهارا که جهان زنده شود</p></div>
<div class="m2"><p>چه غم ار فصل خزان رفت و بهار آخر شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داشت هر سحر و فسونی فلک شعبده باز</p></div>
<div class="m2"><p>همه از معجز لعل لب یار آخر شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بلبلان مژده که بشکفت گل تازه به باغ</p></div>
<div class="m2"><p>عهد گلچین بسرآمد غم خار آخر شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>«حاجبا در دل مردان خدا منزل تست</p></div>
<div class="m2"><p>چون دلت آینه روی نگار آخر شد</p></div></div>