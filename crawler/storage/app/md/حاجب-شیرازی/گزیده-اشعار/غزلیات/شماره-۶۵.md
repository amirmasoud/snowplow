---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>صبحدم مغ بچه گان صف در میخانه زدند</p></div>
<div class="m2"><p>بنگاهی ره هر عاقل و دیوانه زدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صوفیان باده صافی ز ره صدق و صفا</p></div>
<div class="m2"><p>از کف پیرمغان یک دو سه پیمانه زدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در گدایان خرابات به نخوت منگر</p></div>
<div class="m2"><p>که در این میکده بس ساغر شاهانه زدند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عرش را فرش شمردند در آن از سر شوق</p></div>
<div class="m2"><p>باده با خانه خدا سر خوش و مستانه زدند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>محرمان حرم دل ز سر صلح و صلاح</p></div>
<div class="m2"><p>راه هفتاد و دو ملت به یک افسانه زدند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشقانی که ز جان در ره جانان گذرند</p></div>
<div class="m2"><p>باده وصل همه از کف جانانه زدند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نفس باد بهار از چه بود مشک افشان</p></div>
<div class="m2"><p>حوریان سنبل زلف تو، به یک شانه زدند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بلبل از، گل به فغان و من و پروانه خموش</p></div>
<div class="m2"><p>شمع رویان شرری بر من و پروانه زدند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طبع عشاق چو غواص به دریای وجود</p></div>
<div class="m2"><p>غوطه ها بهر تو ای گوهر یکدانه زدند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عشق و فرزانگی از پنبه و آتش بترند</p></div>
<div class="m2"><p>آتش عشق تو چون بر من فرزانه زدند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرغ دل لانه به شاخ سحر افکند ز غیب</p></div>
<div class="m2"><p>آتش عشق به طور دل ویرانه زدند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>داشتم کلبه و کاشانه ای از عالم عقل</p></div>
<div class="m2"><p>عشق و ذوقم بهم آن کلبه و کاشانه زدند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دوش بر، یاد خم ابروی «حاجب » تا صبح</p></div>
<div class="m2"><p>پاک مردان جهان می همه مردانه زدند</p></div></div>