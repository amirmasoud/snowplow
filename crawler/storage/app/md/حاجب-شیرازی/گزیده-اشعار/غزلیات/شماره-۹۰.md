---
title: >-
    شمارهٔ ۹۰
---
# شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>گرچه بر پیر و جوان دادن جان شاق آمد</p></div>
<div class="m2"><p>لیک عاشق به چنین مرحله مشتاق آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو برون ز انفس و آفاقی ای نفس نفیس</p></div>
<div class="m2"><p>کی دگر مثل تو در انفس و آفاق آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زهر در دست تو چون آب حیات ابد است</p></div>
<div class="m2"><p>زهرنوشان تو را عار ز تریاق آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از لب لعل و دهان تو شود حوصله تنگ</p></div>
<div class="m2"><p>طاقت از طاق دو ابرو و لبت طاق آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه اغراق تو بس واجب و فرض آسان شد</p></div>
<div class="m2"><p>وصف حسن تو صنم خالی از اغراق آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نتوان از سر کوی تو جفا جوی گذشت</p></div>
<div class="m2"><p>خون عشاق به حدی است که تا ساق آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اشک خونین چو، شراب است و دل خسته کباب</p></div>
<div class="m2"><p>رزق عشاق تو اینگونه ز، رزاق آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عهد و میثاق شکستن نبود شرط وفا</p></div>
<div class="m2"><p>یار باید همه جا بر سر میثاق آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>«حاجبا» علم و کمال و هنر و فضل و ادب</p></div>
<div class="m2"><p>هر یکی را قلم دست تو مصداق آمد</p></div></div>