---
title: >-
    شمارهٔ ۱۱۱
---
# شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>شاهدان کار تموچین کرده‌اند</p></div>
<div class="m2"><p>ره به تبت رخنه در چین کرده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چین و ژاپن تبت و تاتار، را</p></div>
<div class="m2"><p>مشک با راز، زلف پرچین کرده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کرد گلزار عذار از خط سبز</p></div>
<div class="m2"><p>چشم بد، را خوب برچین کرده‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لادن و عود و عبیر و مشک‌بان</p></div>
<div class="m2"><p>در میان هر عرقچین کرده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زلف را، در دست بهر صید دل</p></div>
<div class="m2"><p>چون کمند رستمی چین کرده‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهر قربانی ره مشروطه را</p></div>
<div class="m2"><p>در صف عشاق گلچین کرده‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دین‌فروشان دکه باطل چیده‌اند</p></div>
<div class="m2"><p>وحی حقْشان حکم برچین کرده‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یوسفان صدق و عصمت «حاجبا»</p></div>
<div class="m2"><p>حسن را عطف کمر، چین کرده‌اند</p></div></div>