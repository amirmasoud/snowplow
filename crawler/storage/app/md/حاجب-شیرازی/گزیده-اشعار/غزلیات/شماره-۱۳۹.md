---
title: >-
    شمارهٔ ۱۳۹
---
# شمارهٔ ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>ای سرو جهان بازآ، به چمن</p></div>
<div class="m2"><p>تا سرو، رود در سجده چو من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با قد، و رخت مانند خجل</p></div>
<div class="m2"><p>هم سرو سهی هم تازه سمن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشتاق تواند با عجز و نیاز</p></div>
<div class="m2"><p>هر تازه جوان هر پیر کهن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در مصر جمال دارد مه من</p></div>
<div class="m2"><p>بس یوسف مصر در چاره ذقن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تابنده رخت در شرق نتافت</p></div>
<div class="m2"><p>در عالم جان در ملک بدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرهون لب و دندان تواند</p></div>
<div class="m2"><p>هر لعل بدخش هر در عدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی بهره نماند از حسن تو کس</p></div>
<div class="m2"><p>نی شیخ و نه شاب نی مرد و نه زن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مقتول تو باد محبوب جهان</p></div>
<div class="m2"><p>نامت چو نوشت با خون به کفن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای جوهر فرد در شیشه جان</p></div>
<div class="m2"><p>وی زر عیار در بوته تن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر معجزه ایست از علم و بیان</p></div>
<div class="m2"><p>ریزی چو درر از درج دهن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آزادی گل از طینت تست</p></div>
<div class="m2"><p>ای ماه زمین وی شاه زمن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شد کشو دل تسخیر، تو را</p></div>
<div class="m2"><p>ناشسته لبت مادر زلبن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سحریست ببین در خامه تو</p></div>
<div class="m2"><p>منسوخ گر احکام و سنن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>استاد بود «حاجب »ز قدم</p></div>
<div class="m2"><p>بر اهل کمال در باب سخن</p></div></div>