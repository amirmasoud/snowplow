---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>ای ز سر، تا به قدم ناز و عطیب</p></div>
<div class="m2"><p>عضو عضو تو همه طیب طیب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این سخن با که توان گفت که ما</p></div>
<div class="m2"><p>جز تو محبوب ندیدیم و حبیب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند تازی فرس ای گرم عنان</p></div>
<div class="m2"><p>خون عشاق گذشته ز، رکیب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کلک تو شافی و کافیست بلی</p></div>
<div class="m2"><p>در حذاقت تو حکیمی و طبیب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شرف از علم بود یا ز ادب</p></div>
<div class="m2"><p>بی ادب را نتوان خواند ادیب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا، به ده علم معلم نشوی</p></div>
<div class="m2"><p>نه ادیبی نه اریبی نه لبیب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روزوصل آمد و زد، بر شب هجر</p></div>
<div class="m2"><p>پشت درماند به یک عمر رقیب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صدق و عصمت اگرت نیست درست</p></div>
<div class="m2"><p>نه نبیلی نه اصیلی نه نجیب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>«حاجب » از پرده پندار درآی</p></div>
<div class="m2"><p>چند محجوبی و تنها و غریب</p></div></div>