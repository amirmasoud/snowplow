---
title: >-
    شمارهٔ ۱۰۷
---
# شمارهٔ ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>آمد آن کس که جهان را همه ارشاد کند</p></div>
<div class="m2"><p>آخرین نامه حق را، زنو انشاد کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آمد آن شاهسواری که به میدان جهان</p></div>
<div class="m2"><p>دل ز ابدال برد حکم بر، اوتاد کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آمد آن قادر قیوم که از خامه صنع</p></div>
<div class="m2"><p>عالم و آدم دیگر زنو ایجاد کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آمد آن کس که سمیع است و علیم است و حکیم</p></div>
<div class="m2"><p>تا، به تأثیر بیان در ره اضداد کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه اولاد، ویند، از، زن و مرد اهل جهان</p></div>
<div class="m2"><p>پدر، آمد که مگر رحم به اولاد کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آمد آن مجمع احسان و مکارم که ز لطف</p></div>
<div class="m2"><p>مرحمت در حق هر فرد، از افراد کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جبت نمرود، در او لطمه به فرعون زند</p></div>
<div class="m2"><p>خاک غم در، دهن و دیده شداد کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جام جم تا بخط بصره و بغداد کشد</p></div>
<div class="m2"><p>شطی از، داد، روان در شط بغداد کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حیدر، از بیشه توحید درآمد که ز عدل</p></div>
<div class="m2"><p>بر، به روبه صفتان حمله چو اجداد کند</p></div></div>