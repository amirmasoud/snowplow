---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>تا پریشان به رخ آن زلف سمن ساست ترا</p></div>
<div class="m2"><p>جمع اسباب پریشانی دلهاست ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست بردی به رخ از شرم حریفان دانند</p></div>
<div class="m2"><p>که تو موسائی و عزم ید بیضاست ترا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون در آئی بسخن زنده کنی عظم رمیم</p></div>
<div class="m2"><p>ای صنم خود مگر اعجاز مسیحاست ترا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که بوسید لبت یافت حیات ابدی</p></div>
<div class="m2"><p>چشمه خضر مگر در لب گویاست ترا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو ترسابچگان عود و صلیب افکندی</p></div>
<div class="m2"><p>یا حمایل به دو سو، زلف چلیپاست ترا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر کوی تو بود محشر خونین کفنان</p></div>
<div class="m2"><p>خود ببام آی اگر میل تماشاست ترا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به تولای تو «حاجب » به دو عالم زده پا</p></div>
<div class="m2"><p>با چنین شوخ بگو از چه تبراست ترا</p></div></div>