---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>چشم تو آهوی ختن پرورد</p></div>
<div class="m2"><p>لعل لبت در عدن پرورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عکس لب و روی تو لعل و عقیق</p></div>
<div class="m2"><p>آن به بدخش این به یمن پرورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرو جوانا، به چمن چم چو من</p></div>
<div class="m2"><p>تا که قدت سرو چمن پرورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در صدف سینه و دریای دل</p></div>
<div class="m2"><p>علم بیان در سخن پرورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ثابت و سیار فلک را، ز مهر</p></div>
<div class="m2"><p>از سر قدرت مه من پرورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی خبر از جان وز جانانه کیست</p></div>
<div class="m2"><p>پر خور و خوابست که تن پرورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر جدو آب علم ادب درس ده</p></div>
<div class="m2"><p>نخل جوان نخل کهن پرورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیشه ببران سر شیران بود</p></div>
<div class="m2"><p>گرچه زبانیست که زن پرورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>«حاجب » درویش به اندرز و پند</p></div>
<div class="m2"><p>روز و شب ابناء وطن پرورد</p></div></div>