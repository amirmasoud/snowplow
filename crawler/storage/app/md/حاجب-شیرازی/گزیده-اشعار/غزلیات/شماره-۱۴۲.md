---
title: >-
    شمارهٔ ۱۴۲
---
# شمارهٔ ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>نسیم صبح به عالم زمن سلام رسان</p></div>
<div class="m2"><p>پس از سلام تو این بهترین پیام رسان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بزم مردم دانا چو بگذری زنهار</p></div>
<div class="m2"><p>تو این پیام به خوبان زخاص و عام رسان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگو که فصل بهار است و جشن جمشید است</p></div>
<div class="m2"><p>بگوش جمله پیامم به احترام رسان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر که مفتی شهرت خم و سبو بشکست</p></div>
<div class="m2"><p>بحکم شهنه عدلش به انتقام رسان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مقام فقر بسی برتر از مقامات است</p></div>
<div class="m2"><p>بسعی و جهد تو خود را در این مقام رسان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شهان به قدرت و حشمت به خویش می بالند</p></div>
<div class="m2"><p>تو خود زفقر و قناعت به احتشام رسان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیا نسیم سحر چون تو روح قدس منی</p></div>
<div class="m2"><p>به طاق ابروی مردان ز من سلام رسان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به کوه و دشت چو خود مشکبار می گذری</p></div>
<div class="m2"><p>تواین غزل به غزالان خوش خرام رسان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رسد بیان و فصاحت به اهتمام ولی</p></div>
<div class="m2"><p>کنون تو نامه خود رابه اختتام رسان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اثر به صحبت خامان و ناتمامان نیست</p></div>
<div class="m2"><p>تو این کلام بهر پخته و تمام رسان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سلام معنی خیر و سلامت است بلی</p></div>
<div class="m2"><p>زمن سلام به هر سالم همام رسان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زماه عارض او شامها به صبح رسید</p></div>
<div class="m2"><p>ززلف تیره خود صبحها به شام رسان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>امام رسته ز، هر قید و تارک دنیاست</p></div>
<div class="m2"><p>تو این حدیث به مأموم و بر امام رسان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز «حاجب » این غزل نغز و این عطیه خاص</p></div>
<div class="m2"><p>بگوش اهل حقیقت به اهتمام رسان</p></div></div>