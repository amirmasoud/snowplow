---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>این جهان جای آرمیدن نیست</p></div>
<div class="m2"><p>هیچ از او چاره جز رمیدن نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخت دامی است مرغ دلها را</p></div>
<div class="m2"><p>که از آن دامشان پریدن نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر سرم هست سر بریدن خویش</p></div>
<div class="m2"><p>وز تو هیچم سر بریدن نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر عشق آشکار باید گفت</p></div>
<div class="m2"><p>گرچه کس قادر شنیدن نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست، درویش کن در این گلزار</p></div>
<div class="m2"><p>زانکه این گل برای چیدن نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کام شیرین مرا از آن حلواست</p></div>
<div class="m2"><p>که کسش قابل چشیدن نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کاهی از خرمن ریاضت ما</p></div>
<div class="m2"><p>کوه را طاقت کشیدن نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برقع وهم بر جمال افکن</p></div>
<div class="m2"><p>که بهر دیده تاب دیدن نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خسروی نامه ایست (حاجب) را</p></div>
<div class="m2"><p>کش فلک قادر دریدن نیست</p></div></div>