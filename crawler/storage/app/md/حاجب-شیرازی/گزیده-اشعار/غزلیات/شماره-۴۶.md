---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>آنجا که هست بود تو باد بهار چیست؟</p></div>
<div class="m2"><p>وآنجا که هست موی تو مشک تتار چیست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلف و رخ تو معنی لیل و نهار ماست</p></div>
<div class="m2"><p>با بودن تو گردش لیل و نهار چیست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باز آ و پا به دیده خونبار ما گذار</p></div>
<div class="m2"><p>تا گویمت که سرو، چه و جویبار چیست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما اختیار در کف جانان نهاده ایم</p></div>
<div class="m2"><p>مختار چونکه یار بود اختیار چیست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد بی حجاب شاهد ما روبرو بما</p></div>
<div class="m2"><p>باز این بلای هجر و غم انتظار چیست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواهی اگر ز حال دل ما شوی خبر</p></div>
<div class="m2"><p>از لاله پرس کاین جگر داغدار چیست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما روزگار را به غم یار طی کنیم</p></div>
<div class="m2"><p>تا در دل این غم است غم روزگار چیست؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما در وصال بی خبریم از غم فراق</p></div>
<div class="m2"><p>تا باده در سبوست بلای خمار چیست؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>«حاجب » چو وصل یار میسر بود ترا</p></div>
<div class="m2"><p>این آه و ناله و فزع انکسار چیست؟</p></div></div>