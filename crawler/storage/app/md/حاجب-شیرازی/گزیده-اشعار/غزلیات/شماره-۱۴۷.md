---
title: >-
    شمارهٔ ۱۴۷
---
# شمارهٔ ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>جام جهان بین جم به طالع میمون</p></div>
<div class="m2"><p>پر می وحدت کنم ز خم فلاطون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خم فلاطون و جام جم به چه ارزد</p></div>
<div class="m2"><p>هر چه بود جام دل ز جوهر مکنون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل به چه ارزد چو باشد آن رخ زیبا</p></div>
<div class="m2"><p>سرو چه باشد چو بالد آن قد موزون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرو بنالد به باغ و گل نشود باز</p></div>
<div class="m2"><p>با قد موزون و آن دو گونه گلگون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسته عشق تو را به سلسله غم نیست</p></div>
<div class="m2"><p>طره لیلی است عقد گردن مجنون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نام نکو مایه تجارت دنیاست</p></div>
<div class="m2"><p>کس نشود از چنین معامله مغبون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چونکه تجارت به من ز تیغ متاع است</p></div>
<div class="m2"><p>عقل مبیع است و جان به حسن تو مرهون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مهر تو با آب و خاک پاک عجین است</p></div>
<div class="m2"><p>ای همه تریاکیت ز نشئه معجون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شهرت سیل سرشکم از غم عشقت</p></div>
<div class="m2"><p>ساحل سیحون گرفت و ساحل جیحون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تشنگی عالمی به آب زلال است</p></div>
<div class="m2"><p>تشنگی اهل دل به شربت قانون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لجه مواج طبع «حاجب » واجب</p></div>
<div class="m2"><p>ریخت همی بر کنار لؤلؤ مخزون</p></div></div>