---
title: >-
    شمارهٔ ۱۱۶
---
# شمارهٔ ۱۱۶

<div class="b" id="bn1"><div class="m1"><p>به می کشان صبحدم داد صلا پیر دیر</p></div>
<div class="m2"><p>که ای صبوحی کشان صبح سعادت بخیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باده وحدت کشید تا که بخود پی برید</p></div>
<div class="m2"><p>چون ز، دوئی بگذرید نیست در این دیر غیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیر در آفاق کن تا که به انفس رسی</p></div>
<div class="m2"><p>نفس نفیسی بجو کامل از او ساز سیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلقت سیمرغ کرد همت والای ما</p></div>
<div class="m2"><p>زاده مریم بساخت اربشب تیره طیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زنده جاوید باش کز عقب موت تن</p></div>
<div class="m2"><p>زحمت مردن دوبار تا نکشی چون عزیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رو، به پناه کسی تا که تو هم کس شوی</p></div>
<div class="m2"><p>ورنه که بودی بلال یا ز کجا بد بریر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای علی الله یا باش تو درویش کیش</p></div>
<div class="m2"><p>چونکه ز نصرانی است ملت و کیش نصیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنکه بود یار ما کی شود اغیار ما</p></div>
<div class="m2"><p>فی المثل اغیار ما طلحه بود یا زبیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با همه در صلح کل دست ده و دست گیر</p></div>
<div class="m2"><p>تا که شود از تو نو عادت این کهنه دیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>«حاجب » درویش را کی شود آن کس حریف</p></div>
<div class="m2"><p>کو نشناسد به عمر لفظ حمار از حمیر</p></div></div>