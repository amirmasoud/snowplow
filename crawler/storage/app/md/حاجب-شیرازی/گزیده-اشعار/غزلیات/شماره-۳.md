---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>ای که بس بی خبری عالم انسانی را</p></div>
<div class="m2"><p>کرده بر خویش روا خصلت حیوانی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کیست آن کس که به حیوان کند اثبات شرف</p></div>
<div class="m2"><p>آنکه دارد صفت خاصه رحمانی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حسن و قبح همه کس بنگر و خاموش نشین</p></div>
<div class="m2"><p>رو، ز آئینه بیاموز تو حیرانی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر امروز ز درویش وز شاهت خبر است</p></div>
<div class="m2"><p>بگدائی بخری حشمت سلطانی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نفسی نفس دغا پیشه اگر رام کنی</p></div>
<div class="m2"><p>عین رحمانی و بندی دم شیطانی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مدعی نقص کمالات مرا گفت چه باک</p></div>
<div class="m2"><p>اهرمن خیره شود صنعت یزدانی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای که در عالم جسمی به حقیقت پابست</p></div>
<div class="m2"><p>جسم حائل نشود باطن نورانی را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیمه شب با رخ چون روز چو روشن گذری</p></div>
<div class="m2"><p>نور باران کنی از رخ شب ظلمانی را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل در آن شور که من در قفس سینه تنگ</p></div>
<div class="m2"><p>شرمگین کرده ز رخ یوسف کنعانی را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زاهد این حیله و طامات و خرافات بنه</p></div>
<div class="m2"><p>مرد دانا نخرد سکه نادانی را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر نهی پا بسر چرخ بدین حسن و جمال</p></div>
<div class="m2"><p>چرخ ثور و حمل آرد صف قربانی را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صلح کل باش و دل از وسوسه جنگ بشوی</p></div>
<div class="m2"><p>تا چو دیوان نکنی خدمت دیوانی را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از پی نزع صلاح آمر کل فرمان داد</p></div>
<div class="m2"><p>کیست گردن ننهد حکم جهان بانی را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>داغ را پخته بسوزم جگر ای پخته خام</p></div>
<div class="m2"><p>تا تو زینت نکنی صفحه پیشانی را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>«حاجب » از پرده اوهام برون مینه گام</p></div>
<div class="m2"><p>تا بداند همه کس معنی روحانی را</p></div></div>