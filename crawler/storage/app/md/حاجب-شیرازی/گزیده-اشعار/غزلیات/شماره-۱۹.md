---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>در آن مجلس که حرمت نیست می را</p></div>
<div class="m2"><p>صلا ده ای پسر خوبان ری را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می و مطرب در این مجلس مباح است</p></div>
<div class="m2"><p>خبر کن ساقی فرخنده پی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به ری خوبان دریغ از ما نکردند</p></div>
<div class="m2"><p>می و چنگ و رباب و عود و نی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان دارند یارانم که دارند</p></div>
<div class="m2"><p>بنات النعش اطراف جدی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه سر در باده و جام است زاهد</p></div>
<div class="m2"><p>که عالم راغب هستند این دو شی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به یک جام آن چنانم مست گرداند</p></div>
<div class="m2"><p>که بخشم تخت و تاج جم و کی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو از عکس رخ و زلف آفریدی</p></div>
<div class="m2"><p>به حکمت نور و ظلمت شمس و فی را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>معلم شد به علم و عشق جانان</p></div>
<div class="m2"><p>که این علم و هنر آموخت وی را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهار عمر را، هست از قفا، دی</p></div>
<div class="m2"><p>تو کردی نوبهاری فصل دی را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بساط حاتم طی، طی نگردد</p></div>
<div class="m2"><p>اگر دست قضا طی کرد طی را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کریمان را نگیرد، موت دامن</p></div>
<div class="m2"><p>که «حاجب » کرد طی این طرفه حی را</p></div></div>