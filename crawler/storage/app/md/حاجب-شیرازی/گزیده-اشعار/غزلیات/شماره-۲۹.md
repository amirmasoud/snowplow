---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>ز چشم ساقی و تاب شراب و بانگ رباب</p></div>
<div class="m2"><p>به روز و شب همه افتاده ایم مست و خراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفیق صادق و یار موافق ار نبود</p></div>
<div class="m2"><p>رفیق رطل شرابست و یار غار کباب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر رقیب گریزد ز کلک ما چه عجب</p></div>
<div class="m2"><p>پرد چگونه عزازیل پیش تیر شهاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سراب، آب نماید ز دور، و میدانیم</p></div>
<div class="m2"><p>که از سراب نشد هیچ تشنه ای سیراب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز ضعف پیری و جهل جهول و شدت فقر</p></div>
<div class="m2"><p>دگر، نه حال سؤال است و نی مجال جواب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنون که روز حساب است و حشر و نشر امم</p></div>
<div class="m2"><p>نمیروند چرا مرد و زن به راه حساب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>متاز، توسن ناز ای سوار گرم عنان</p></div>
<div class="m2"><p>که در گذشت تو را خون عاشقان زرکاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشو به سنگدلی غره و به خویش مبال</p></div>
<div class="m2"><p>که هست شیشه مر از هوا به شکل حباب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به کنج عزلت و گنج قناعتم خورسند</p></div>
<div class="m2"><p>به پیری است مرا، کر و فر عهد شباب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حجاب وهم برانداز از میان «حاجب »</p></div>
<div class="m2"><p>که وهم عقل جهان را بود بزرگ حجاب</p></div></div>