---
title: >-
    شمارهٔ ۱۴۸
---
# شمارهٔ ۱۴۸

<div class="b" id="bn1"><div class="m1"><p>ای قبله مه رویان برقع به عذار افکن</p></div>
<div class="m2"><p>بر آئینه خورشید عکس شب تارافکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوب است بپوشی رو، از چشم بداندیشان</p></div>
<div class="m2"><p>برقرص قمر گردی از مشک تتار افکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نی نی که همه نوری تابنده بهر جوری</p></div>
<div class="m2"><p>از پرده درآ، جانا برقع به کنار افکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بشگفت گل رویت مستانه به گلشن آی</p></div>
<div class="m2"><p>منت به گلستان نه رونق به بهار افکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دشمن به کمین ما چون هند جگرخوار است</p></div>
<div class="m2"><p>لختی ز جگر ایدل پیش سگ هار افکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای مقصد درویشان بگذر ز بداندیشان</p></div>
<div class="m2"><p>خورشید صفت پرتو بر هر خس و خارافکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>«حاجب » ز سبو در جام هان باده وحدت ریز</p></div>
<div class="m2"><p>وانگه نظری از لطف بر باده گسار افکن</p></div></div>