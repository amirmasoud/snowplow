---
title: >-
    شمارهٔ ۱۵۰
---
# شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>از پرتو عکس رخت، افتاده بر طرف چمن</p></div>
<div class="m2"><p>یکجا صبا یکجا خزان یکجا گل ویکجا سمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برقع ز عارض برگشا تا عالمی شیدا کنی</p></div>
<div class="m2"><p>جمعی ز مو برخی زرو، خلق از لب و من از دهن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون در تبسم می روی از فرقتت گم می کند</p></div>
<div class="m2"><p>سوسن زبان قمری فغان بلبل نوا طوطی سخن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر گه که بر خیزی بپا بر گرد سر می گرددا</p></div>
<div class="m2"><p>شمع از زمین مه از سما روح از تن و جان از بدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>افتاده بر طرف چمن از خرمی های رخت</p></div>
<div class="m2"><p>آب از روش باد از طپش ریگ از گل و حالت زمن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دانم زمن رنجیده ای ای نازنین خونم بریز</p></div>
<div class="m2"><p>این خنجر و این حنجرم این هم من و این هم کفن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از وصف آن شیرین زبان پرسید «حاجب » گفتمش</p></div>
<div class="m2"><p>رخساره مه ابرو کمان زلفان سیه چشمان ختن</p></div></div>