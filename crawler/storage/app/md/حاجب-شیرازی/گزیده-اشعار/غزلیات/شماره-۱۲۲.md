---
title: >-
    شمارهٔ ۱۲۲
---
# شمارهٔ ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>ساقی شب زنده دار وقت صبوح است خیز</p></div>
<div class="m2"><p>باده همرنگ صبح جم شو، در جام ریز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معنی یوسف توئی، از چه زندان در آی</p></div>
<div class="m2"><p>ای مه کنعان حسن در همه مصری عزیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کیستی ای ذات غیب کت زقدم بوده است</p></div>
<div class="m2"><p>موسی و عیسی غلام مریم و هاجر کنیز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در ره عشقت مرا نیست غم جان و دل</p></div>
<div class="m2"><p>گر شود این ریش ریش ور شود آن ریز ریز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در پی دنیا مرو غافل و احمق مشو</p></div>
<div class="m2"><p>کز جلو آمال تست وز عقب افعال نیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز صنع طباخ طبع ببین به سطح زمین</p></div>
<div class="m2"><p>چیده چنان میزبان ظلمت ایوان به میز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان پی دنیا مکن قانع و درویش باش</p></div>
<div class="m2"><p>که گنج قارون بود پیش تو کم از پشیز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روی چو «حاجب » ز ما ای بت یکتا مپوش</p></div>
<div class="m2"><p>خاک سیه بیش از این بر سر یاران مریز</p></div></div>