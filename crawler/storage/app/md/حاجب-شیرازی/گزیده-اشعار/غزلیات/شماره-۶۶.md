---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>ماه من گر طرف کاکل بشکند</p></div>
<div class="m2"><p>رونق بازار سنبل بشکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نقاب وهم از، رخ افکند</p></div>
<div class="m2"><p>قدر ماه و قیمت گل بشکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در تغنی آن نگار گل عذار</p></div>
<div class="m2"><p>نغمه را، در نای بلبل بشکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با حقیقت بگذر، از پل نی مجاز</p></div>
<div class="m2"><p>می برد آبت اگر پل بشکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کوک کن مطرب تو، بربط را چو فور</p></div>
<div class="m2"><p>تا خماری را، بطی مل بشکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در کمند آرد مگس را عنکبوت</p></div>
<div class="m2"><p>وقت حمله شیر نر، غل بشکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روز میدان شیر چون جولان کند</p></div>
<div class="m2"><p>جیش گرگان بی تأمل بشکند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جزء و کل گر، بر خلافش صف کشند</p></div>
<div class="m2"><p>صف بدرد جزء با کل بشکند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست «حاجب » را توسل با کسی</p></div>
<div class="m2"><p>چون توسل را، توکل بشکند</p></div></div>