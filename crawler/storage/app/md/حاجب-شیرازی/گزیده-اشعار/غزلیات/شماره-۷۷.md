---
title: >-
    شمارهٔ ۷۷
---
# شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>هر آنکه جامه جان در محبتی بدرید</p></div>
<div class="m2"><p>به قد و قامت خود خلعت شرف ببرید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسود ما همه خر مهره داشت ما همه در</p></div>
<div class="m2"><p>ازو خرید به خروار و کس ز ما نخرید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چمان چو من بگذر، زان چمن که گرگ در اوست</p></div>
<div class="m2"><p>که آهوی دل ما اندر این چمن بچرید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گه رحیل ز خواب ار تنی نشد بیدار</p></div>
<div class="m2"><p>چو رحل ماند به مرحل به کاروان نرسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کجا دگر، به گلستان پرد، دمی آزاد</p></div>
<div class="m2"><p>اگر که مرغ گرفتار از قفس بپرید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کجا شود، سر، سرکردگان کسی که به شوق</p></div>
<div class="m2"><p>به جان و سر، بسر کوی سروری نرسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کسی که فصل جوانی نکرد خدمت پیر</p></div>
<div class="m2"><p>نبرد راه به مقصد به مسلکی نرسید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چگونه می شنود صوت مرغ گلشن راز</p></div>
<div class="m2"><p>هر آنکه پنبه غفلت ز گوش جان نکشید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیا چو «حاجب » درویش صلح کل میباش</p></div>
<div class="m2"><p>کز اهل حرب کسی بوی مردمی نشنید</p></div></div>