---
title: >-
    شمارهٔ ۱۳۰
---
# شمارهٔ ۱۳۰

<div class="b" id="bn1"><div class="m1"><p>امشب بخواب ناز مگر رفته این خروس؟</p></div>
<div class="m2"><p>تا کی به درد و غم کنم امشب کنار وبوس؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نوبت زن زمانه بخواب است یا خمار؟</p></div>
<div class="m2"><p>یا نای برشکسته و یا بر دریده کوس؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا سندروس بر شبه افشاند چرخ ریخت</p></div>
<div class="m2"><p>بیچاره اشکم از دو رخ همچو سندروس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای صبح صادقاز افق غیب کن طلوع</p></div>
<div class="m2"><p>تا نگذرد خدنگ تهمتن بر اشکبوس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امروز، بر، دریچه صبح است پیک صلح</p></div>
<div class="m2"><p>در، روم و هندوچین و فرنگ و پروس و روس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبح است صبح، ساقی شب زنده دار خیز</p></div>
<div class="m2"><p>می ده مخواه عمر گرانمایه برفسوس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کام کسی نداد عروس جهان و ما</p></div>
<div class="m2"><p>برداشتیم مهر بکارت از این عروس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما ملک جم به یک تن تنها گرفته ایم</p></div>
<div class="m2"><p>بی سعی زال و رستم و گودزر و گیو، و طوس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رو، قدر وقت دان و غنیمت شمار عمر</p></div>
<div class="m2"><p>بگذر ز چرخ سفله و دوران چاپلوس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>«حاجب » بر آن سرم که به چوگان راستی</p></div>
<div class="m2"><p>بس گوی عاج گیرم از این چرخ آبنوس</p></div></div>