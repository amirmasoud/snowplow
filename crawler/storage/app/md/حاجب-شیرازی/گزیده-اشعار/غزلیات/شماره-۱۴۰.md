---
title: >-
    شمارهٔ ۱۴۰
---
# شمارهٔ ۱۴۰

<div class="b" id="bn1"><div class="m1"><p>نسیم صبح توئی جبرئیل نام از من</p></div>
<div class="m2"><p>به طاق ابروی مردان رسان سلام از من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر که از من گمنام نام پرسد یار</p></div>
<div class="m2"><p>ز بعد سجده شکرانه بر تو، نام از من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر ز قتل من آن دوست را به دل هوس است</p></div>
<div class="m2"><p>میسر است در این کار اهتمام از من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عیال و مال و کسان شد حرام و باده حلال</p></div>
<div class="m2"><p>گر، از حلال بپرسید یا حرام از من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غنا و ثروت کل منعما تمام از تو</p></div>
<div class="m2"><p>ملال و ذلت و فقر و فنا تمام از من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر که نعمت وصل تو قدر نشناسم</p></div>
<div class="m2"><p>کشد زمانه به هجر تو انتقام از من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از آن زمان که چو گل آمدی به دست مرا</p></div>
<div class="m2"><p>همیشه می طلبد بوی تو مشام از من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غلام ناطق حقم قلم گواه من است</p></div>
<div class="m2"><p>برد ملک به فلک روز وشب پیام از من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر قیامت موعود را توانی دید</p></div>
<div class="m2"><p>بگو به خلق قیامت بود قیام از من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>منم که محرم خلوتسرای قدسم و بس</p></div>
<div class="m2"><p>بگو به کعبه نگهدار احترام از من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا که غیر دل آشنا مقامی نیست</p></div>
<div class="m2"><p>زمانه از چه طلب می کند مقام از من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو ای نسیم صبا تاز سوی ما گذری</p></div>
<div class="m2"><p>رسان به دوست سلام از من و پیام از من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز غیر من مطلب التیام و خرق جهان</p></div>
<div class="m2"><p>تو نیز خرق زمن خواه و التیام از من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>منی نماند بجا هر چه هست «حاجب » اوست</p></div>
<div class="m2"><p>که دوست می طلبد من علی الدوام از من</p></div></div>