---
title: >-
    شمارهٔ ۱۲۶
---
# شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>تو هر چه ناز کنی ما اگر کنیم نیاز</p></div>
<div class="m2"><p>نیازمند تو هستیم ناز میکن ناز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز کعبه راه به کوی تو می توان بردن</p></div>
<div class="m2"><p>از آنکه قنطره ای بر حقیقت است مجاز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حدیث عشق بر پیر عقل بردم دوش</p></div>
<div class="m2"><p>چنان بخویش فرو رفت کش ندیدم باز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو، باز حسن پراندی و من کبوتر دل</p></div>
<div class="m2"><p>کبوتری که رود سوی باز ناید باز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو گر، به حسن و جمالی ز جمع خوبان فرد</p></div>
<div class="m2"><p>منم ز فضل و معانی زعاشقان ممتاز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شب فراق ز زلف تو شکوه خواهم کرد</p></div>
<div class="m2"><p>که روز وصل بسی کوته است و قصه دراز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو ای عشق نبدر است کوزند آن ترک</p></div>
<div class="m2"><p>هزار شور برانگیزد از عراق و حجاز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گذشت ناوک نازش مرا ز جوشن جان</p></div>
<div class="m2"><p>فغان ز دست کمان ابروان تیرانداز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نبات «زند» به مازندران شده است شکر</p></div>
<div class="m2"><p>ز شهد شعر شکرریز «حاجب » شیراز</p></div></div>