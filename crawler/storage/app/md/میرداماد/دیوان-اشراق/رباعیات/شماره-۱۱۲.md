---
title: >-
    شمارهٔ ۱۱۲
---
# شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>از لعل لب تو باده تاب چکد</p></div>
<div class="m2"><p>وز دست غم تو خون احباب چکد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز در رخ صبر سوز مهر تو که دید</p></div>
<div class="m2"><p>آتش که بیفشرند ازو آب چکد</p></div></div>