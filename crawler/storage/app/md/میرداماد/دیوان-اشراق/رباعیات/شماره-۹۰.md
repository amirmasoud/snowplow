---
title: >-
    شمارهٔ ۹۰
---
# شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>آن ماه که حسن را روان می بخشد</p></div>
<div class="m2"><p>یادش به بدن خواص جان می بخشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاد لبش ار آنکه در آید به ضمیر</p></div>
<div class="m2"><p>با فکر مزاج بهره مان می بخشد</p></div></div>