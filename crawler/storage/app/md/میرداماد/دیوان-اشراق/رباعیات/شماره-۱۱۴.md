---
title: >-
    شمارهٔ ۱۱۴
---
# شمارهٔ ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>با تیغ تو جنبش از جهان بر خیزد</p></div>
<div class="m2"><p>گردون بنشیند و زمان برخیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز تیغ نماندکس دراین ملک دو روی</p></div>
<div class="m2"><p>وقت است که او هم زمیان برخیزد</p></div></div>