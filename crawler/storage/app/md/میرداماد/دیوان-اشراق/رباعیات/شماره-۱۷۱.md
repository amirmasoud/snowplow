---
title: >-
    شمارهٔ ۱۷۱
---
# شمارهٔ ۱۷۱

<div class="b" id="bn1"><div class="m1"><p>جز یاد تو در سینه ما یاد مباد</p></div>
<div class="m2"><p>جز نام توام بر لب فریاد مباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی حلقه زنجیر تو آزاد مباد</p></div>
<div class="m2"><p>بی درد غم رخت دلی شاد مباد</p></div></div>