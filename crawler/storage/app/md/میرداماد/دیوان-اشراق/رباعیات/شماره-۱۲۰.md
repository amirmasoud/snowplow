---
title: >-
    شمارهٔ ۱۲۰
---
# شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>بی عشق سرشکم آتش تیز نبود</p></div>
<div class="m2"><p>بی درد رگم نشتر خونریز نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کوس طرب از دولت غم کوفت دلم</p></div>
<div class="m2"><p>ورنه دل من خسروپرویز نبود</p></div></div>