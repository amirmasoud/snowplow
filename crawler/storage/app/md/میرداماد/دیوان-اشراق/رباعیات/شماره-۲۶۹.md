---
title: >-
    شمارهٔ ۲۶۹
---
# شمارهٔ ۲۶۹

<div class="b" id="bn1"><div class="m1"><p>ای وصل تو اختر مراد دل من</p></div>
<div class="m2"><p>در راه غم تو شعله زاد دل من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان دامن زلف تو ز کف نگذارد</p></div>
<div class="m2"><p>از هجر تو ناگرفته داد دل من</p></div></div>