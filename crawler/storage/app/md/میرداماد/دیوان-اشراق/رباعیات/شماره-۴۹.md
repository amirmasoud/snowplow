---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>چوگان شده قامتم سرم گوی خوش است</p></div>
<div class="m2"><p>خوناب دل مرا جهان جوی خوش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد شعله مرا بر سر هر موی خوش است</p></div>
<div class="m2"><p>میدان بلامرا ز هر سوی خوش است</p></div></div>