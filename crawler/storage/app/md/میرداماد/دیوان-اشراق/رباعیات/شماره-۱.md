---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>ای دل دو سه روزی بود این بستان‌ها</p></div>
<div class="m2"><p>در باغ هنر چو بلبلان خوش بسرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با گردن ظلم خصم داند چکند</p></div>
<div class="m2"><p>بازوی عدالت علی اعلا</p></div></div>