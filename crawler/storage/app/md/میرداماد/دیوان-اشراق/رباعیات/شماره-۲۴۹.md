---
title: >-
    شمارهٔ ۲۴۹
---
# شمارهٔ ۲۴۹

<div class="b" id="bn1"><div class="m1"><p>رهبان کلیسیای دوران شده ام</p></div>
<div class="m2"><p>ناقوس نواز دیر حرمان شده ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه معصیتی نه طاعتی وای به من</p></div>
<div class="m2"><p>شرمنده کافر و مسلمان شده ام</p></div></div>