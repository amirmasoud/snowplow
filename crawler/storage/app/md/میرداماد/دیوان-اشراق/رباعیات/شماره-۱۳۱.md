---
title: >-
    شمارهٔ ۱۳۱
---
# شمارهٔ ۱۳۱

<div class="b" id="bn1"><div class="m1"><p>خواهم فلکت دگر مساعد گردد</p></div>
<div class="m2"><p>بختت طرف یاره ساعد گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو کوکب اقبالی و کوکب که هبوط</p></div>
<div class="m2"><p>گیرد غرض آن بود که صاعد گردد</p></div></div>