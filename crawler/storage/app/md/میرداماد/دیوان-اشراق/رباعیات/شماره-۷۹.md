---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>ویرانه خاطرم که حکمت کده است</p></div>
<div class="m2"><p>بر درگهش از عقول قوسی زده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرجوهر حکمت که ره دل زده است</p></div>
<div class="m2"><p>از خازن طبع من یکی کم شده است</p></div></div>