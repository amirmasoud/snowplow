---
title: >-
    شمارهٔ ۲۵۱
---
# شمارهٔ ۲۵۱

<div class="b" id="bn1"><div class="m1"><p>کی باد ز شر تن اخسا برهیم</p></div>
<div class="m2"><p>از وحشت این قفس چو عنقا برهیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان در چمن قدس فشانم به رقص</p></div>
<div class="m2"><p>روزی که ز ظلمت هیولا برهیم</p></div></div>