---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>برسید کاینات بی حد صلوات</p></div>
<div class="m2"><p>بر زوج بتول و نفس احمد صلوات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر هر سه خلیفه . . . . . .</p></div>
<div class="m2"><p>بر هر دو نبیره محمد صلوات</p></div></div>