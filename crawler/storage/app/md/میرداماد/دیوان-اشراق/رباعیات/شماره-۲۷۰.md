---
title: >-
    شمارهٔ ۲۷۰
---
# شمارهٔ ۲۷۰

<div class="b" id="bn1"><div class="m1"><p>اکنون که شدی مه خرد برده من</p></div>
<div class="m2"><p>از وصل تو زنده شد دل مرده من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیمرغ غمت کشد به منقار ستم</p></div>
<div class="m2"><p>خاشاک بلا به جان افسرده من</p></div></div>