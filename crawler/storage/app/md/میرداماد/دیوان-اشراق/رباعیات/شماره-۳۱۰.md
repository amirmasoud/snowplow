---
title: >-
    شمارهٔ ۳۱۰
---
# شمارهٔ ۳۱۰

<div class="b" id="bn1"><div class="m1"><p>در بزم محبت امشب از دولت تو</p></div>
<div class="m2"><p>از باده درد و ساغر محنت تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن جام که میخواستم از دست تو من</p></div>
<div class="m2"><p>دست قدرم بداد بی منت تو</p></div></div>