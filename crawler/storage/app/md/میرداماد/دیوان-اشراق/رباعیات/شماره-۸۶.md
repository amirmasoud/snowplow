---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>دنیا همه هیچ و کار دنیا همه هیچ</p></div>
<div class="m2"><p>اندیشه و اعتبار دنیا همه هیچ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای جان به دیار هیچ دل هیچ مبند</p></div>
<div class="m2"><p>زیراکه بود دیار دنیا همه هیچ</p></div></div>