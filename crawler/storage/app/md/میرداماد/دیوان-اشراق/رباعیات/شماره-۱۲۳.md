---
title: >-
    شمارهٔ ۱۲۳
---
# شمارهٔ ۱۲۳

<div class="b" id="bn1"><div class="m1"><p>جام می اگر چه غارت هوش کند</p></div>
<div class="m2"><p>زان پس کند اما که کسی نوش کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نام تو چه باده ئیست یارب که چنین</p></div>
<div class="m2"><p>تاراج خردها زره گوش کند</p></div></div>