---
title: >-
    شمارهٔ ۳۱۳
---
# شمارهٔ ۳۱۳

<div class="b" id="bn1"><div class="m1"><p>هرتن که سرشت بد بود محضر او</p></div>
<div class="m2"><p>ناچار همی بدی بکوبد در او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنمای کسی را که ز اندیشه بد</p></div>
<div class="m2"><p>سر دل او نشد قضای سر او</p></div></div>