---
title: >-
    شمارهٔ ۹۶
---
# شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>امشب که بلا بدین ستمکش بارد</p></div>
<div class="m2"><p>از چشم ترم باده بیغش بارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من گریه ندیده ام بدین بوالعجبی</p></div>
<div class="m2"><p>کز دیده بجای آب آتش بارد</p></div></div>