---
title: >-
    شمارهٔ ۳۳۴
---
# شمارهٔ ۳۳۴

<div class="b" id="bn1"><div class="m1"><p>تاراج متاع اختیارم کردی</p></div>
<div class="m2"><p>در رهگذر بلا غبارم کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم بشکیبم از تو آتش رسنی</p></div>
<div class="m2"><p>در گردن جان بیقرارم کردی</p></div></div>