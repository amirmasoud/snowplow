---
title: >-
    شمارهٔ ۱۵۸
---
# شمارهٔ ۱۵۸

<div class="b" id="bn1"><div class="m1"><p>کفتم که چو بخت هجر در خواب شود</p></div>
<div class="m2"><p>خون جگرم مگر می ناب شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلف شب من به صبح گردن ننهد</p></div>
<div class="m2"><p>خورشید ز نور اگر رسن تاب شود</p></div></div>