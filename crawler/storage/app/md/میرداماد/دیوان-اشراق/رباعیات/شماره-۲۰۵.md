---
title: >-
    شمارهٔ ۲۰۵
---
# شمارهٔ ۲۰۵

<div class="b" id="bn1"><div class="m1"><p>از حسن تو ای فارس میدان جمال</p></div>
<div class="m2"><p>آشوب و بلا فتاده در دست خیال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش باش که آفاق وجود ما شد</p></div>
<div class="m2"><p>در راه تو از خون جگر مالامال</p></div></div>