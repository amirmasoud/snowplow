---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>ای ختم رسل دو کون پیرایه تست</p></div>
<div class="m2"><p>افلاک یکی منبر نه پایه تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر شخصی ترا سایه نباشد چه عجب</p></div>
<div class="m2"><p>تو نوری و آفتاب در سایه تست</p></div></div>