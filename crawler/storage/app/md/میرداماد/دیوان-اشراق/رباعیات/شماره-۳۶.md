---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>بتخانه نمک زان لب چون نوش گرفت</p></div>
<div class="m2"><p>بت کام از آن سرو قباپوش گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهم که تمام عمر در برگیرم</p></div>
<div class="m2"><p>آن بت که شبی ترا در آغوش گرفت</p></div></div>