---
title: >-
    شمارهٔ ۸۹
---
# شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>آن آفت خرمن سکون می آید</p></div>
<div class="m2"><p>ای دیده بیا ببین که چون می آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دل برو و خانه جان پاک بروب</p></div>
<div class="m2"><p>کآن خانه خدا ز در درون می آید</p></div></div>