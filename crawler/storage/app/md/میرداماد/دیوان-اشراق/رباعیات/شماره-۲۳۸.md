---
title: >-
    شمارهٔ ۲۳۸
---
# شمارهٔ ۲۳۸

<div class="b" id="bn1"><div class="m1"><p>تانیت صحبت وصالت بستم</p></div>
<div class="m2"><p>قفل در کاخ نقد جان بشکستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچون معنی اساس و اسباب وجود</p></div>
<div class="m2"><p>در خانه دل نهادم و بنشستم</p></div></div>