---
title: >-
    شمارهٔ ۲۴۵
---
# شمارهٔ ۲۴۵

<div class="b" id="bn1"><div class="m1"><p>در خانه عشق تا که من بنشستم</p></div>
<div class="m2"><p>از هر فکری بجز خیالت رستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسوده شدم که برد سلطان غمت</p></div>
<div class="m2"><p>در کاخ وجود هر چه بود از دستم</p></div></div>