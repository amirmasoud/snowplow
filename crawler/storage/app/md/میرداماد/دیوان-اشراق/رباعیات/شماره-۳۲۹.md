---
title: >-
    شمارهٔ ۳۲۹
---
# شمارهٔ ۳۲۹

<div class="b" id="bn1"><div class="m1"><p>ای دیده ز اشک بی متاعم نکنی</p></div>
<div class="m2"><p>در بیعگه غم ابتیاعم نکنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون من سخن از وداع دلدار کنم</p></div>
<div class="m2"><p>ای جان عجب است اگر وداعم نکنی</p></div></div>