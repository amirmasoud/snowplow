---
title: >-
    شمارهٔ ۱۴۹
---
# شمارهٔ ۱۴۹

<div class="b" id="bn1"><div class="m1"><p>زین باده که دل زجام غیرت نوشید</p></div>
<div class="m2"><p>خون درتن من چو باده در خم جوشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که به صبر از تو نهان دارم لیک</p></div>
<div class="m2"><p>آتش به گیاه خشک نتوان پوشید</p></div></div>