---
title: >-
    شمارهٔ ۲۱۶
---
# شمارهٔ ۲۱۶

<div class="b" id="bn1"><div class="m1"><p>از صحبت خلق دیده بر دوخته ام</p></div>
<div class="m2"><p>وز لاله طریق صحبت آموخته ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عیبم مکن ار به ظاهر افروخته ام</p></div>
<div class="m2"><p>در باطن من نگر که چون سوخته ام</p></div></div>