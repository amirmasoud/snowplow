---
title: >-
    شمارهٔ ۱۱۰
---
# شمارهٔ ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>ای درد تو خوشتر از حیات جاوید</p></div>
<div class="m2"><p>در عهد غمت کساد بازار نوید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در نیمه ره گلشن وصلت مانده ست</p></div>
<div class="m2"><p>از بسکه خلیده خار در پای امید</p></div></div>