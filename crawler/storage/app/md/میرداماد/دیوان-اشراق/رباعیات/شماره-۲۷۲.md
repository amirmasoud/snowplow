---
title: >-
    شمارهٔ ۲۷۲
---
# شمارهٔ ۲۷۲

<div class="b" id="bn1"><div class="m1"><p>ای یاد تو سرمایه بیهوشی من</p></div>
<div class="m2"><p>هیچت نشد از یاد فراموشی من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر نه ز عشق تست بر بستر غم</p></div>
<div class="m2"><p>با خار و خس بلا هم آغوشی من</p></div></div>