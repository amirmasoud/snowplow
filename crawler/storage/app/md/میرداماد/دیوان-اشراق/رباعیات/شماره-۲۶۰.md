---
title: >-
    شمارهٔ ۲۶۰
---
# شمارهٔ ۲۶۰

<div class="b" id="bn1"><div class="m1"><p>مائیم که با فقر و فنا ساخته ایم</p></div>
<div class="m2"><p>در ملک عدم مرکب جان تاخته ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دولت سودای تو بی منت مرگ</p></div>
<div class="m2"><p>خود را ز خودی خویش پرداخته ایم</p></div></div>