---
title: >-
    شمارهٔ ۱۹۸
---
# شمارهٔ ۱۹۸

<div class="b" id="bn1"><div class="m1"><p>ای از تو سرای دل پر افغان و خروش</p></div>
<div class="m2"><p>از آتش تو دیگ وجودم پر جوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کس که برد نام تو همچون مطلوب</p></div>
<div class="m2"><p>جانم ز بدن برون برد از ره گوش</p></div></div>