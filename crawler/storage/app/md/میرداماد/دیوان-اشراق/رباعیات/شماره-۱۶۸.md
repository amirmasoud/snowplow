---
title: >-
    شمارهٔ ۱۶۸
---
# شمارهٔ ۱۶۸

<div class="b" id="bn1"><div class="m1"><p>یا در چمن قدس وطن باید کرد</p></div>
<div class="m2"><p>یا همبری زاغ و زغن باید کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاترک جهان پر فتن باید کرد</p></div>
<div class="m2"><p>یاجوهر جان رهین تن باید کرد</p></div></div>