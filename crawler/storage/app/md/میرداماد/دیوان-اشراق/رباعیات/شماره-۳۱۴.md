---
title: >-
    شمارهٔ ۳۱۴
---
# شمارهٔ ۳۱۴

<div class="b" id="bn1"><div class="m1"><p>مستیم و خراب کنج میخانه تو</p></div>
<div class="m2"><p>سرخوش همه از شراب پیمانه تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیرند اگر چه عاقلان خرده به ما</p></div>
<div class="m2"><p>شادیم که خود شدیم دیوانه تو</p></div></div>