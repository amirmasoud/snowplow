---
title: >-
    شمارهٔ ۱۸۶
---
# شمارهٔ ۱۸۶

<div class="b" id="bn1"><div class="m1"><p>چون خواست مرا دور جهان شعبده باز</p></div>
<div class="m2"><p>تاریک چو شب خانه بخت ناساز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آیا ز چه رو به مفت شماع فلک</p></div>
<div class="m2"><p>شمع هنرم داد همی از آغاز</p></div></div>