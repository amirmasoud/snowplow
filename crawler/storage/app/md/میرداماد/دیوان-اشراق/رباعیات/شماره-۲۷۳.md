---
title: >-
    شمارهٔ ۲۷۳
---
# شمارهٔ ۲۷۳

<div class="b" id="bn1"><div class="m1"><p>ای خون ز تو باده روان دل من</p></div>
<div class="m2"><p>آتش ز تو در خرمن جان دل من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم به تو زین شعله حدیثی گویم</p></div>
<div class="m2"><p>تا دامن لب سوخت زبان دل من</p></div></div>