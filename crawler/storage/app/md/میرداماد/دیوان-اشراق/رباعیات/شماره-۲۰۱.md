---
title: >-
    شمارهٔ ۲۰۱
---
# شمارهٔ ۲۰۱

<div class="b" id="bn1"><div class="m1"><p>خورشید همی هراسد از روزن عشق</p></div>
<div class="m2"><p>چاک جگری ندوزد این سوزن عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این خانه ز آتش بلا سوخته ام</p></div>
<div class="m2"><p>کم خانه ز آتش است در برزن عشق</p></div></div>