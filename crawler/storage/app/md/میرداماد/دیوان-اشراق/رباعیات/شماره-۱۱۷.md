---
title: >-
    شمارهٔ ۱۱۷
---
# شمارهٔ ۱۱۷

<div class="b" id="bn1"><div class="m1"><p>بوی تو ره قافله هوش زند</p></div>
<div class="m2"><p>وز شوق تو خون در دل جان جوش زند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هوش از سر دل رقص کنان برخیزد</p></div>
<div class="m2"><p>چون نام تو حلقه بر در گوش زند</p></div></div>