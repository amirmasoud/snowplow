---
title: >-
    شمارهٔ ۲۱۲
---
# شمارهٔ ۲۱۲

<div class="b" id="bn1"><div class="m1"><p>گردید الف قامت ما آخر دال</p></div>
<div class="m2"><p>بر دوش نبردیم مگر وزر و وبال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگذشته ما بود سراسر غفلت</p></div>
<div class="m2"><p>تا چون شود آینده که خوابیم الحال</p></div></div>