---
title: >-
    شمارهٔ ۱۰۳
---
# شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>ای خاک در تو آب حیوان وجود</p></div>
<div class="m2"><p>ای داغ تو بر سرین یکران وجود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ابر ار زکف تو قطره گیرد فکند</p></div>
<div class="m2"><p>در بطن صدف درر ز باران وجود</p></div></div>