---
title: >-
    شمارهٔ ۲۸۹
---
# شمارهٔ ۲۸۹

<div class="b" id="bn1"><div class="m1"><p>تا دور شدی ز چشم غمدیده من</p></div>
<div class="m2"><p>افعی بلا شد مژه در دیده من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود سوختم و همی نمی دارد دست</p></div>
<div class="m2"><p>آتش ز گیاه جان تفتیده من</p></div></div>