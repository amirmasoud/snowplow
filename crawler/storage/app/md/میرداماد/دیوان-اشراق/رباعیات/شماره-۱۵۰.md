---
title: >-
    شمارهٔ ۱۵۰
---
# شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>زین شعله که دل چو جانش در بر گیرد</p></div>
<div class="m2"><p>بهر شرفش چو تاج بر سر گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مجلس دهر اگر کسی نام برد</p></div>
<div class="m2"><p>در جان فلک آتش دل درگیرد</p></div></div>