---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>ای شهد لبت دوای بیماری‌ها</p></div>
<div class="m2"><p>وی دیده دل از زلف تو دلداری‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسان تو کنی مگر که در راه غمت</p></div>
<div class="m2"><p>افتاده دلم به دام دشواری‌ها</p></div></div>