---
title: >-
    شمارهٔ ۲۴۱
---
# شمارهٔ ۲۴۱

<div class="b" id="bn1"><div class="m1"><p>خاکستر گلخن عدم باد دلم</p></div>
<div class="m2"><p>تا نفخه صور غم دژم باد دلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کوی تو دل رهین غم کرد مرا</p></div>
<div class="m2"><p>زینسان که منم رهین غم باد دلم</p></div></div>