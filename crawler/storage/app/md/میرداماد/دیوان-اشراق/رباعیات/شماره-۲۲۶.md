---
title: >-
    شمارهٔ ۲۲۶
---
# شمارهٔ ۲۲۶

<div class="b" id="bn1"><div class="m1"><p>ای جان من از عشق تو پیمانه غم</p></div>
<div class="m2"><p>دل جغد بلا و سینه و یرانه غم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو اختر شادئیی چگویم بر تو</p></div>
<div class="m2"><p>حال دل تیره یعنی افسانه غم</p></div></div>