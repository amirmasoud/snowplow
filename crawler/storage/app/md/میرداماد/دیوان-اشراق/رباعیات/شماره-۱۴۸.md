---
title: >-
    شمارهٔ ۱۴۸
---
# شمارهٔ ۱۴۸

<div class="b" id="bn1"><div class="m1"><p>زان پیش که جان ز نور بی مایه شود</p></div>
<div class="m2"><p>دل تیره ز دور چرخ نه پایه شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ساغر ما یکی از آن باده بریز</p></div>
<div class="m2"><p>کز پرتوش آفتاب چون سایه شود</p></div></div>