---
title: >-
    شمارهٔ ۱۸۴
---
# شمارهٔ ۱۸۴

<div class="b" id="bn1"><div class="m1"><p>در ساغر دل خون ز شراب اولیتر</p></div>
<div class="m2"><p>در سینه سنان بجای خواب اولیتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ویرانه تن نه جای آبادانی ست</p></div>
<div class="m2"><p>این دیر بلا همان خراب اولیتر</p></div></div>