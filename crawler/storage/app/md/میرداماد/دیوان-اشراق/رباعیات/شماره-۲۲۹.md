---
title: >-
    شمارهٔ ۲۲۹
---
# شمارهٔ ۲۲۹

<div class="b" id="bn1"><div class="m1"><p>با آتش دل به عشق همخوابه شدم</p></div>
<div class="m2"><p>وندر ره غم روان چو خونابه شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر شب شود اندرونم از آتش پر</p></div>
<div class="m2"><p>گوئی که مگر گلخن گرمابه شدم</p></div></div>