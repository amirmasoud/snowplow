---
title: >-
    شمارهٔ ۱۶۵
---
# شمارهٔ ۱۶۵

<div class="b" id="bn1"><div class="m1"><p>وقت آمده کاین جان و تنم پاک رود</p></div>
<div class="m2"><p>وز عالم خاکی سوی افلاک رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنهار مکن این ستم ای بخت که دل</p></div>
<div class="m2"><p>نادیده وصال دوست در خاک رود</p></div></div>