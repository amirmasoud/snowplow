---
title: >-
    شمارهٔ ۱۴۷
---
# شمارهٔ ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>زان پیش که خاک ما فلک کوزه کند</p></div>
<div class="m2"><p>بازیچه دور چرخ فیروزه کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر مرقد ما خرام تا روح قدس</p></div>
<div class="m2"><p>از تربت ما حیات دریوزه کند</p></div></div>