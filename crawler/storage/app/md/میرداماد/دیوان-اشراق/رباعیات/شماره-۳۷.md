---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>بوقلمونی و بایزیدی کفر است</p></div>
<div class="m2"><p>وین دلسیهی و مو سفیدی کفر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانم که نیم لایق رحمت لیکن</p></div>
<div class="m2"><p>از درگه دوست ناامیدی کفر است</p></div></div>