---
title: >-
    شمارهٔ ۱۹۲
---
# شمارهٔ ۱۹۲

<div class="b" id="bn1"><div class="m1"><p>در وصف تو خلق دو جهان از آغاز</p></div>
<div class="m2"><p>گفتند ونگفتند یک از صدها راز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای خالق چاره ساز و ای بنده نواز</p></div>
<div class="m2"><p>جز درگه تو بر که نهم روی نیاز</p></div></div>