---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>از دست تو در ساغر جانم خونهاست</p></div>
<div class="m2"><p>وز عشق تو دل چو پرده قانونهاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویند که این باده افیون زهر است</p></div>
<div class="m2"><p>زهر است و لیک حسرت افسونهاست</p></div></div>