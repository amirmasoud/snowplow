---
title: >-
    شمارهٔ ۱۱۶
---
# شمارهٔ ۱۱۶

<div class="b" id="bn1"><div class="m1"><p>با خون جگر خاک دل آمیخته باد</p></div>
<div class="m2"><p>جان من و خاک غم به هم بیخته باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل خون مرا بریخت کز تیغ غمت</p></div>
<div class="m2"><p>بر خاک عدم خون دلم ریخته باد</p></div></div>