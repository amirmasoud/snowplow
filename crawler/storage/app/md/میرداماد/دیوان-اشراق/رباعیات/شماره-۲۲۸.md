---
title: >-
    شمارهٔ ۲۲۸
---
# شمارهٔ ۲۲۸

<div class="b" id="bn1"><div class="m1"><p>ای در ته دریای گناهان شده گم</p></div>
<div class="m2"><p>تیره شده بر طالع شومت انجم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندانکه بکردند زراعت مردم</p></div>
<div class="m2"><p>هرگز دیدی که جو بیارد گندم؟</p></div></div>