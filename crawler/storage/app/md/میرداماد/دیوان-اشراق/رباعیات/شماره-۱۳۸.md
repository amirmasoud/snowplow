---
title: >-
    شمارهٔ ۱۳۸
---
# شمارهٔ ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>در جام دل آن باده که دی دلبر کرد</p></div>
<div class="m2"><p>کز هوش روان و خردش ساغر کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یارب ز چه باده بود کاندیشه آن</p></div>
<div class="m2"><p>فکرت به ضمیر عقل خاکستر کرد</p></div></div>