---
title: >-
    شمارهٔ ۳۲۴
---
# شمارهٔ ۳۲۴

<div class="b" id="bn1"><div class="m1"><p>ای تیره شب فراق آخر به سر آی</p></div>
<div class="m2"><p>وی صبح امید از در مهر درآی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر عمر منی ای شب هجران بگذر</p></div>
<div class="m2"><p>ور جان منی ای نفس صبح برآی</p></div></div>