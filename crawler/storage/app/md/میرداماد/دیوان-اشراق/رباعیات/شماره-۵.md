---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>اندر صدوبیست دوره چرخش‌ها</p></div>
<div class="m2"><p>کز درگه جدت شه اقلیم رضا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دورم نکشیدم آن ستم کز دو سه روز</p></div>
<div class="m2"><p>از دوری خدمت تو دیدم ز قضا</p></div></div>