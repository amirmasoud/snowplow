---
title: >-
    شمارهٔ ۳۲۷
---
# شمارهٔ ۳۲۷

<div class="b" id="bn1"><div class="m1"><p>ای دل تا کی به هرزه تدبیر کنی</p></div>
<div class="m2"><p>وز خون جگر به جوی غم شیر کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تار تو کز آن دام مگس نتوان کرد</p></div>
<div class="m2"><p>خواهی که به آن همای نخجیر کنی</p></div></div>