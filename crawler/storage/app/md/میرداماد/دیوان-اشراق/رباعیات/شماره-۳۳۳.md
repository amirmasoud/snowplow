---
title: >-
    شمارهٔ ۳۳۳
---
# شمارهٔ ۳۳۳

<div class="b" id="bn1"><div class="m1"><p>تا روز رخت شد چو شراب عنبی</p></div>
<div class="m2"><p>روزم همه تیره شد ببین بوالعجبی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید فلک ندید هم روز بخواب</p></div>
<div class="m2"><p>زان شب که شب از زلف تو آموخت شبی</p></div></div>