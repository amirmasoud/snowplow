---
title: >-
    شمارهٔ ۸۱
---
# شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>یک نامه به من رسید از حضرت دوست</p></div>
<div class="m2"><p>کاین شعله جانم همه از آتش اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن نامه نگاری که به یک جلوه ناز</p></div>
<div class="m2"><p>نه جان به بدن گذاشت نه مغز به پوست</p></div></div>