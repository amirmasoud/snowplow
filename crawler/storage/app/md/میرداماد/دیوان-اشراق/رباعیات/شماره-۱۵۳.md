---
title: >-
    شمارهٔ ۱۵۳
---
# شمارهٔ ۱۵۳

<div class="b" id="bn1"><div class="m1"><p>شد تن همه دل دلستان می آید</p></div>
<div class="m2"><p>دل خود همه تن شد که روان می آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای جان مجازی از جسد بیرون رو</p></div>
<div class="m2"><p>کآن یار حقیقتی چو جان می آید</p></div></div>