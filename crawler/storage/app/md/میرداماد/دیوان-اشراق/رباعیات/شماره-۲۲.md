---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>اسبم ز فراق جو چو نالی مانده ست</p></div>
<div class="m2"><p>از پیکر اصلیش مثالی مانده ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیچاره ز خرمن خیالیست جوش</p></div>
<div class="m2"><p>ز آنروست کزو همی خیالی مانده ست</p></div></div>