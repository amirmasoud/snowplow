---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>آتش ز تو در هستی نابود من است</p></div>
<div class="m2"><p>وین جرم شفق اخگر و شب دود من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن شعله آتشم که خورشید فلک</p></div>
<div class="m2"><p>گرم از تف آه آتش آلود من است</p></div></div>