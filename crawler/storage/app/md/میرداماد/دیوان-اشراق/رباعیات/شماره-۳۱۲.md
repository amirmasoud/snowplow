---
title: >-
    شمارهٔ ۳۱۲
---
# شمارهٔ ۳۱۲

<div class="b" id="bn1"><div class="m1"><p>کو عمر که داد عیش بستانم ازو</p></div>
<div class="m2"><p>کو وصل که درد هجر بنشانم ازو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کو یار که گر پای خیالش بمثل</p></div>
<div class="m2"><p>بر دیده نهد دیده نکو دانم ازو</p></div></div>