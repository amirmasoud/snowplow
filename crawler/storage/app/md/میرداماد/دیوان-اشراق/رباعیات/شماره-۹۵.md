---
title: >-
    شمارهٔ ۹۵
---
# شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>از دوریت ای تازه گل باغ مراد</p></div>
<div class="m2"><p>چون غنچه چیده خنده ام رفته زیاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گریان چو پیاله پرم در کف دست</p></div>
<div class="m2"><p>نالان چوسبوی خالیم در ره باد</p></div></div>