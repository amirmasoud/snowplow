---
title: >-
    شمارهٔ ۱۰۴
---
# شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>از دوری خدمتت که جان افزاید</p></div>
<div class="m2"><p>سهل است اگر دلم ز غم فرساید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لیکن ز قدمهای خیالت خجلم</p></div>
<div class="m2"><p>کز روی کرم اینهمه ره می آید</p></div></div>