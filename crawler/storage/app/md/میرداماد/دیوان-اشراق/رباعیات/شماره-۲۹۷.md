---
title: >-
    شمارهٔ ۲۹۷
---
# شمارهٔ ۲۹۷

<div class="b" id="bn1"><div class="m1"><p>می آمد دی بسان اقبال شهان</p></div>
<div class="m2"><p>خندان چو لب بهار در خوزستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من از غم او چو پیر زاهد گریان</p></div>
<div class="m2"><p>او همچو شباب بر غم من خندان</p></div></div>