---
title: >-
    شمارهٔ ۲۰۳
---
# شمارهٔ ۲۰۳

<div class="b" id="bn1"><div class="m1"><p>این کاخ هزار در که باشد صد رنگ</p></div>
<div class="m2"><p>نه ملک بقا باشد و نه جای درنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیهوده میندوز زر و سیم وگهر</p></div>
<div class="m2"><p>کز بهر نهادن چه تفاوت زر و سنگ</p></div></div>