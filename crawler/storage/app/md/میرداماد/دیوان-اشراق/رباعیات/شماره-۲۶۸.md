---
title: >-
    شمارهٔ ۲۶۸
---
# شمارهٔ ۲۶۸

<div class="b" id="bn1"><div class="m1"><p>ای کهتر فضل تو هزار افلاطون</p></div>
<div class="m2"><p>شد صبر من از حوصله شوق افزون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر وعده خویش را وفا خواهی کرد</p></div>
<div class="m2"><p>ای مفخر اهل علم وقت است اکنون</p></div></div>