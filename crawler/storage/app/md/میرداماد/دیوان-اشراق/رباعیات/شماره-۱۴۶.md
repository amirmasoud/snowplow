---
title: >-
    شمارهٔ ۱۴۶
---
# شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>روزی دلم از غم تو بگسسته شود</p></div>
<div class="m2"><p>گز لوح وجود نام من شسته شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بسکه خیال تو هجوم آورده ست</p></div>
<div class="m2"><p>ترسم که همی راه نفس بسته شود</p></div></div>