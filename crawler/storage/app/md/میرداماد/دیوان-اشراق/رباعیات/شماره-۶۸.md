---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>صد شکر که کوچه عدم جای من است</p></div>
<div class="m2"><p>بازار فنا گرم ز سودای من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سینه چو فکر یار بیرون نرود</p></div>
<div class="m2"><p>گوئی که درون سینه دنیای من است</p></div></div>