---
title: >-
    شمارهٔ ۳۰۰
---
# شمارهٔ ۳۰۰

<div class="b" id="bn1"><div class="m1"><p>یارب که بکوی خویش پابستم کن</p></div>
<div class="m2"><p>وز باده جام نیستی هستم کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلگیرم ازین نهاد افسرده خویش</p></div>
<div class="m2"><p>یک جرعه می عشق ده و مستم کن</p></div></div>