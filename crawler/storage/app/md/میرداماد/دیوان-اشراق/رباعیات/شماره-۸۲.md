---
title: >-
    شمارهٔ ۸۲
---
# شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>دیوانه عشق پیرو عاقل نیست</p></div>
<div class="m2"><p>آن دل که بجز تو یار خواهد دل نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با غیر تو همعنان شدن مشکل ماست</p></div>
<div class="m2"><p>در راه غمت دادن جان مشکل نیست</p></div></div>