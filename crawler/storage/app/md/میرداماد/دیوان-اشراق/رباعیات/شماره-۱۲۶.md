---
title: >-
    شمارهٔ ۱۲۶
---
# شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>جان غاشیه غم تو بردوشش باد</p></div>
<div class="m2"><p>درد تو روان من در آغوشش باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز وصل تو آرزو گر اندیشه کند</p></div>
<div class="m2"><p>رسم و ره اندیشه فراموشش باد</p></div></div>