---
title: >-
    شمارهٔ ۱۱۹
---
# شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>بی تو همه عیش ها و بالم باشد</p></div>
<div class="m2"><p>با تو همه دردها زلالم باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک پرتو خورشید جمالت خواهم</p></div>
<div class="m2"><p>تا زینت خانه خیالم باشد</p></div></div>