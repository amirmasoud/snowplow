---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>دستی که گرفتی سر آن زلف چو شست</p></div>
<div class="m2"><p>پائی که ره وصل به سر می پیوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان دست کنون در غم دل دادم پای</p></div>
<div class="m2"><p>زان پای کنون بر سر دل دادم دست</p></div></div>