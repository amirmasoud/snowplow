---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>ای دوست بیا که بی تو بودن عار است</p></div>
<div class="m2"><p>جان بی تو ز من چو سایه دیوار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبح همه بی باده وصلت شام است</p></div>
<div class="m2"><p>نور همه بی نور جمالت تار است</p></div></div>