---
title: >-
    حکیم رکنا و میرداماد
---
# حکیم رکنا و میرداماد

<div class="n" id="bn1"><p>از حکیم رکنا به میرداماد:</p></div>
<div class="b" id="bn2"><div class="m1"><p>در طرز سخن تورا بیانی دگر است</p></div>
<div class="m2"><p>القصه زبان تو زبانی دیگر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از قلزم دانش تو ای بحر عمیق</p></div>
<div class="m2"><p>هر قطره هیولای جهانی دگر است</p></div></div>
<div class="n" id="bn4"><p>پاسخ میرداماد:</p></div>
<div class="b" id="bn5"><div class="m1"><p>در قالب نظم از تو جانی دگر است</p></div>
<div class="m2"><p>در تن ز خیال تو روانی دگر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در محور آسمان استعدادست</p></div>
<div class="m2"><p>هر نقطه محیط آسمانی دگر است</p></div></div>