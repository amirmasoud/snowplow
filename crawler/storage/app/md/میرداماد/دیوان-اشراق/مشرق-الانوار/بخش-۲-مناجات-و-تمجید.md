---
title: >-
    بخش ۲ - مناجات و تمجید
---
# بخش ۲ - مناجات و تمجید

<div class="b" id="bn1"><div class="m1"><p>ای خرد از حلقه به گوشان تو</p></div>
<div class="m2"><p>خلق خوش از عطر فروشان تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای ز تو این گوی گریبان چرخ</p></div>
<div class="m2"><p>گوی شده پیش تو چوگان چرخ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای ز تو نه طاق فلک پر شروق</p></div>
<div class="m2"><p>ای ز تو آراسته این چار سوق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داغ تو بر جبهه روح القدس</p></div>
<div class="m2"><p>خاک درت آب چهار استطقس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حلقه تعلیم تو در گوش عقل</p></div>
<div class="m2"><p>غاشیه حکم تو بر دوش عقل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زنگ غمت صیقل مرآت دل</p></div>
<div class="m2"><p>یاد تو تعمیر خرابات دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ذات تو مصداق وجود صفات</p></div>
<div class="m2"><p>لیک صفات تو همه عین ذات</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گردن ما سخره طوق فنا</p></div>
<div class="m2"><p>ملک قدم خاص و مسلم ترا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قد ابد پیش بقای تو پست</p></div>
<div class="m2"><p>قامت معنی ر ثنای تو پست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از تو ضمیر خرد آراسته</p></div>
<div class="m2"><p>فیض تو پهلوی عدم کاسته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از تو جهان مرکز و هستی مدار</p></div>
<div class="m2"><p>از تو فلک پخته زمین خام کار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گردش چرخ از تو به انجام شد</p></div>
<div class="m2"><p>کار عدم از تو چنین خام شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خور ز تو چون باده افق همچو جام</p></div>
<div class="m2"><p>کار فلک از تو چنین با نظام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تخم کواکب تو پراکنده ای</p></div>
<div class="m2"><p>ناف شب از مشگ تو آکنده ای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تاج خرد از تو مکلل شده</p></div>
<div class="m2"><p>زیج وجود از تو مجدول شده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روی زمین روز تورخشان کنی</p></div>
<div class="m2"><p>زلف فلک شب تو پریشان کنی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بی تو روان ره نبرد سوی تن</p></div>
<div class="m2"><p>جان نرهد بی تو ز جادوی تن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>قالب جنبنده تو بی جان کنی</p></div>
<div class="m2"><p>باز رگ مرده تو شریان کنی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چهره خورشید درخشان ز تست</p></div>
<div class="m2"><p>گردش نه چرخ به سامان زتوست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از تو جهان هستی جاوید یافت</p></div>
<div class="m2"><p>مار شب و مهره خورشید یافت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یافت ز تو جوف سپهر برین</p></div>
<div class="m2"><p>زهره دریا و سپرز زمین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>طفل سخن دامن لب رادهی</p></div>
<div class="m2"><p>مهره صبح افعی شب را دهی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یاد تو شد صحت جان سقیم</p></div>
<div class="m2"><p>بوی تو شد قوت دماغ نسیم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>منطقه چرخ شتاب از تو یافت</p></div>
<div class="m2"><p>ملکت ایجاد کتاب از تو یافت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کنه تو اندیشه تصور نکرد</p></div>
<div class="m2"><p>جام تصور ز تو کس پر نکرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عقل به تأئید دلیل و قیاس</p></div>
<div class="m2"><p>گفت نهد معرفتت را اساس</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>برق تو خود خرمن ادارک سوخت</p></div>
<div class="m2"><p>بال و پر مرغ خرد پاک سوخت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای گهر ما صدف نعمتت</p></div>
<div class="m2"><p>وی گنه ما علف رحمتت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خاک درت سرمه اشراق شد</p></div>
<div class="m2"><p>زین شرف اندر دو جهان طاق شد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ضمت جانش به تو بسپرده ام</p></div>
<div class="m2"><p>وقف غلامی تواش کرده ام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هر که غلامی ترا در خور است</p></div>
<div class="m2"><p>از گهر عقل گرامی تراست</p></div></div>