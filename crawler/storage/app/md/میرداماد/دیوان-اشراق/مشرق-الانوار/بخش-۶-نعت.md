---
title: >-
    بخش ۶ - نعت
---
# بخش ۶ - نعت

<div class="b" id="bn1"><div class="m1"><p>ای شرف مسند پیغمبری</p></div>
<div class="m2"><p>نه فلکت نایب انگشتری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرخ نهم سفره دربان تو</p></div>
<div class="m2"><p>عقل دهم ریزه خور خوان تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عهد تو چون موسم باران عزیز</p></div>
<div class="m2"><p>شرع تو چون صحبت یاران عزیز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طوف کن کوی تو ایوان چرخ</p></div>
<div class="m2"><p>ناز کش گوی تو چوگان چرخ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهر فلک آینه رای تو</p></div>
<div class="m2"><p>قلزم هستی کف دریای تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در حرم بندگی تو قوا</p></div>
<div class="m2"><p>جمع چو در قوت بنطاسیا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جمله قوا عالیه و سافله</p></div>
<div class="m2"><p>در ره اخلاص تو همقافله</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گوش فلک حلقه کش بندگیت</p></div>
<div class="m2"><p>خنده صبح ار لب فرخنده گیت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>موسی و عیسی همه محتاج تو</p></div>
<div class="m2"><p>هفت سماسلم معراج تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گشته بلند از سر تو سروری</p></div>
<div class="m2"><p>هندوی تو جای زحل مشتری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آب رخ نه فلک از جوی تست</p></div>
<div class="m2"><p>ملک شرف رهن سر کوی تست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عرش اگردعوی رفعت نمود</p></div>
<div class="m2"><p>چرخ ز درگاه تو برهان شنود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نافه به خلق تو فرستاده باج</p></div>
<div class="m2"><p>یاد تو ز اندیشه گرفته خراج</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر شده تعلیم تو استاد وهم</p></div>
<div class="m2"><p>کرد همای خرد از خاد فهم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لطف تو کرده نظری بر زبان</p></div>
<div class="m2"><p>دامنش از سود زده بر میان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کرده اگر تیر قبولت هدف</p></div>
<div class="m2"><p>گشته بدن غیرت روح از شرف</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نافه چین داغ کش بوی تست</p></div>
<div class="m2"><p>جزیه ده غالیه موی تست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رای تو مهر فلک خانه زاد</p></div>
<div class="m2"><p>جود سحاب از کف تو مستفاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یافته چون روح بخاری جنین</p></div>
<div class="m2"><p>قالب شک از تو روان یقین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خلق تو از نافه جنایت گرفت</p></div>
<div class="m2"><p>کوی تو از کعبه ولایت گرفت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حکمت حق قاعده دین تو</p></div>
<div class="m2"><p>ملت روح القدس آئین تو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پیش شبانی تو عالم رمه</p></div>
<div class="m2"><p>سایه نداری که تو نوری همه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فذلکه هستی و خاتم توئی</p></div>
<div class="m2"><p>غایت ایجاد دو عالم توئی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اینهمه پاکی که به زینت گرفت</p></div>
<div class="m2"><p>دامن عقل از تو ودیعت گرفت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آب خضر چون سر من چاکرت</p></div>
<div class="m2"><p>خواسته دریوزه ز خاک درت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چونکه نسیم تو حمایتگر است</p></div>
<div class="m2"><p>شعله ز بستان ارم خوشتراست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>روضه دین تو چو باغ ارم</p></div>
<div class="m2"><p>از تف باحور معاصی چه غم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ذمت اشراق رهین تو شد</p></div>
<div class="m2"><p>خاک نشین در دین تو شد</p></div></div>