---
title: >-
    بخش ۷ - منقبت علی (ع)
---
# بخش ۷ - منقبت علی (ع)

<div class="b" id="bn1"><div class="m1"><p>نفس نبی باب مدینه ی علوم</p></div>
<div class="m2"><p>در کف او آهن مریخ موم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سید ابرار و شه اتقیا</p></div>
<div class="m2"><p>سرور و سرخیل همه اصفیا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خازن سبحانی تنزیل وحی</p></div>
<div class="m2"><p>عالم ربانی تأویل وحی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داغ کش نافه او مشگ ناب</p></div>
<div class="m2"><p>جزیه ده سایه او آفتاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فذلکه عالم و باب وجود</p></div>
<div class="m2"><p>سوره توحید و کتاب وجود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حامل دین غیبته علم خدا</p></div>
<div class="m2"><p>عقل دهم کرده بر او اقتدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاک درش تاج سر سروران</p></div>
<div class="m2"><p>آب کفش کوثر دین پروران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>راست به بازوش همی پشت دین</p></div>
<div class="m2"><p>لاغر ازو پهلوی کفر اینچنین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اوست که در ظلمت سمت جهات</p></div>
<div class="m2"><p>کعبه نور است و سفینه ی نجات</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کفر بر آویخته دینش ز دار</p></div>
<div class="m2"><p>بر در او شرک همی سنگسار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گردن او گوش نه در بیعت است</p></div>
<div class="m2"><p>عروه کفر وعلم شقوت است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جبهه او گوش نه خاک ره است</p></div>
<div class="m2"><p>تیه ضلالیست که در لهله است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نسل نبی زایچه صلب اوست</p></div>
<div class="m2"><p>خیل سعادت همه در طلب اوست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا که شده کنیت او بوتراب</p></div>
<div class="m2"><p>نه فلک از جوی زمین خورده آب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صورت اشراق چو از خاک اوست</p></div>
<div class="m2"><p>در ره معنی سگ چالاک اوست</p></div></div>