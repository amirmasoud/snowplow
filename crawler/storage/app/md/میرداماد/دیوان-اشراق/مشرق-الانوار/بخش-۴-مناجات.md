---
title: >-
    بخش ۴ - مناجات
---
# بخش ۴ - مناجات

<div class="b" id="bn1"><div class="m1"><p>ای کرمت مایه امید من</p></div>
<div class="m2"><p>سرو جوان از تو کهن بید من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاد توام قوت تن و جان و دل</p></div>
<div class="m2"><p>درد توام مایه درمان دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرگ ز تو هستی جاوید من</p></div>
<div class="m2"><p>سایه دیوار تو خورشید من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده من خاک درت راست باج</p></div>
<div class="m2"><p>داغ ترا ناصیه من خراج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قافله سالار نویدم توئی</p></div>
<div class="m2"><p>آبده کشت امیدم توئی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش تو داروی مداوای من</p></div>
<div class="m2"><p>مایه سود از تو زیانهای من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر بنوازی تو اگر بفکنی</p></div>
<div class="m2"><p>من نتوانم ز تو بودن غنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر دهی ام خواری اگر عزتی</p></div>
<div class="m2"><p>نیست مرا بر در تو حجتی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوش من و حلقه افکندگی</p></div>
<div class="m2"><p>دوش من و غاشیه بندگی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جز تو ندارم کس و یار دگر</p></div>
<div class="m2"><p>کیست کنون از من کس دارتر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جز تو کسی کس بود آن خواری است</p></div>
<div class="m2"><p>چون تو کسی اینهمه کس داری است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آه که در حکم تو عاصی شدم</p></div>
<div class="m2"><p>تاجر بازار معاصی شدم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روی دلم در عرق معصیت</p></div>
<div class="m2"><p>خون تنم از شفق معصیت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>داغ دوصد معصیتم بر جبین</p></div>
<div class="m2"><p>پیش تو چون جبهه نهم بر زمین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دیده دل نایب جیحون کنم</p></div>
<div class="m2"><p>دامن دل دجله ای از خون کنم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز ابر دو چشم آنقدر اندر سجود</p></div>
<div class="m2"><p>قطره بریزم به کنار وجود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کش به خیال آنکه درآرد دلیر</p></div>
<div class="m2"><p>رویدش اقسام گیاه از ضمیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر در جود تو بیارم شفیع</p></div>
<div class="m2"><p>از در اشک اینهمه طفل رضیع</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نالش لز آئین بدیع آورم</p></div>
<div class="m2"><p>خواجه کونین شفیع آورم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا مگر آنجا که کرمهای تست</p></div>
<div class="m2"><p>لطف تو سازد غلط ما درست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در حرم عفو تو تقصیرها</p></div>
<div class="m2"><p>خورده ز غفران تو تشویرها</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چشم دلم بر کنف عفو تست</p></div>
<div class="m2"><p>جرم دو عالم علف عفو تست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>قطره ای از عفو تو موج بحار</p></div>
<div class="m2"><p>ترسم از الایش مشتی غبار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گنج دل او که به تائید تست</p></div>
<div class="m2"><p>مهر رسول تو و توحید تست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خواجه کونین شفیعی چنین</p></div>
<div class="m2"><p>سهل بود بخشش یک کف زمین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دولت اشراق که در طینتش</p></div>
<div class="m2"><p>خاک رسول تو بد و عترتش</p></div></div>