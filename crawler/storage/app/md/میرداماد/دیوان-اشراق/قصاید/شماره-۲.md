---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>شه ملک دانشم من به جنود آسمانی</p></div>
<div class="m2"><p>که بود ز فضل دیهیم سریرم از معانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز مداد من سوادی در چشم آفرینش</p></div>
<div class="m2"><p>ز ظلال من کلاهی بر تارک معانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ارتقای فکرم خط استوا ز دانش</p></div>
<div class="m2"><p>نطق میان نطقم افق درر فشانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صدف محیط طبعم کنف در حقایق</p></div>
<div class="m2"><p>محک نقود طرزم فلک رسوم دانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقط سواد خطم همه جیب قوس گردون</p></div>
<div class="m2"><p>وترس قسی فضلم همه قطر آسمانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شوس صواب بینم مجس مصاب دانی</p></div>
<div class="m2"><p>رقم قضا نشانم حکم قدر بیانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه اختران طبعم فلک آورد به تحفه</p></div>
<div class="m2"><p>همه دختران غیبم خرد آرد ارمغانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز ردای من کناغی بر دوش سعد اکبر</p></div>
<div class="m2"><p>ز ثنای من کناغی در لوح عقل ثانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لقب من است جز من به کسسی سزا نباشد</p></div>
<div class="m2"><p>چه ممهد حقایق چه مشید مبانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل مرده را بجز من نکند کسی مسیحی</p></div>
<div class="m2"><p>تن حکمه رابجز من ندهد کسی روانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پرن سمای عقلم مه چرخ نامجوئی</p></div>
<div class="m2"><p>سقط نهاد پاکم نمط خرد فشانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سر کوی دانش من عرفات راز گردون</p></div>
<div class="m2"><p>حرم حریم فرکرم در کعبه معانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز شفای من ارسطو شده بهره مند دانش</p></div>
<div class="m2"><p>ز رموز من فلاطون رده گام در معانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سخن از حدوث بی من به نوائب غرامت</p></div>
<div class="m2"><p>خرد از وجود بی من به مضیق ایرمانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خردم به عذر خواهی ز تقدم زمانه</p></div>
<div class="m2"><p>فلکم به عفو جوئی ز تصدر مکانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز کمان فکر هر گه بکشم خدنگ برهان</p></div>
<div class="m2"><p>چو فلک ز قامت خود خردم کند کمانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>(فکنم به جوی باغ سخن آبی از طراوت</p></div>
<div class="m2"><p>که نمی ازان نیابی بر دجله جوانی)</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز ستاک باغ طبعم به غرامت است طوبی</p></div>
<div class="m2"><p>ز نگار نقش فکرم به خجالت است مانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در من چو کعبه سازد که خلیل فضل و دانش</p></div>
<div class="m2"><p>دل من مطاف سازد که سروش آسمانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز هنر خراج گیرم ز خرد حمایت اما</p></div>
<div class="m2"><p>به وفور فضل و دانش نه به زور حکم رانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>رسع شک آورم من ز دو پلک چشم بیرون</p></div>
<div class="m2"><p>دعی خطا کنم من ز معراج فکر فانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ببرم چو بر نشینم به تکاور فصاحت</p></div>
<div class="m2"><p>وقراز صماخ جذر اصم از سبک عنانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز خرد به شرع دانش همه ساله جزیه گیرم</p></div>
<div class="m2"><p>(چو نبی به زور دین از که زکیش باستانی)</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز دلم به فقر گنجور به خزاین معادن</p></div>
<div class="m2"><p>ز ضمیر من به فاقه کف گنج شایگانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز پی حساب و دانش کنم آسمان چو دفتر</p></div>
<div class="m2"><p>سزدم ز تیر کلکی و ز مشتری بنانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چمن حریم دل را کنم از نفس صبائی</p></div>
<div class="m2"><p>روض ریاض جان را کنم از دل (ایلوانی)</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زقمر برم به دعوت کلف سیاه روئی</p></div>
<div class="m2"><p>ز درر برم به حکمت برص سفید رانی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سوی شکل اول از من رود ار نخست اجازت</p></div>
<div class="m2"><p>پس از آن نتیجه ریزد ز قیاس اقترانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دل خسته را پس از من سخنم کند طبیبی</p></div>
<div class="m2"><p>تن خاک را پس از من جسدم کند روانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زجبین خاک تیره به نظر برم کریهی</p></div>
<div class="m2"><p>ز روان کوه تهلان به نفس برم گرانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دل من چو کان ولیکن نه به سقط نطفه خورده</p></div>
<div class="m2"><p>همه همچو مریم آرد گهری بدان روانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>صدفی نیم که جایز بودم به دین همت</p></div>
<div class="m2"><p>ز سخای ابرنیسان ز حموت تر دهانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شده ام چو آب حیوان به نهاد پاک داری</p></div>
<div class="m2"><p>شده ام چو کان گوهر به نژاد دودمانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نپذیردم تصور شبهی زکینه شاید</p></div>
<div class="m2"><p>گرم آب جوی فرهنگ و ضمیر عقل خوانی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سمرم چو آب باران به کتاب جرم شوئی</p></div>
<div class="m2"><p>مثلم چو پیر دانش به حساب عفورانی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هنرم بر اوج گردون و منم چو خاک سفلی</p></div>
<div class="m2"><p>سخنم خنیده چون هور و منم چو سر نهانی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>پس از این به دوش دعوی فکنم ردای دانش</p></div>
<div class="m2"><p>که خرد کند اطاعت به ردای وردخوانی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به فراق یار ای دل ز تو یک عطیه خواهم</p></div>
<div class="m2"><p>که زابر دیده بر ما همه خون دل برانی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نه چنانکه دهر جانی بزید به حیله کردن</p></div>
<div class="m2"><p>فلک نهم معلم فکند به بادبانی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو اثیریم به طوف سر کوی اسطقسی</p></div>
<div class="m2"><p>چو زمانیم به گرد در معرفت چو آنی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>سخن من ار نباشد چکند خرد دبیری</p></div>
<div class="m2"><p>خرد من ار نباشد چکند جهان جهانی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>منم آنکه در خموشی سزدم زبان چو موئی</p></div>
<div class="m2"><p>که چو موی برتن من همه تن کند زبانی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به حساب آفرینش چو به مرکزی نشایم</p></div>
<div class="m2"><p>چه سفه بود که لافم ز محیط آسمانی</p></div></div>