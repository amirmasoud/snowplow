---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>آفت تقوی ما جلوه کنان می آید</p></div>
<div class="m2"><p>خوش شراری به سر خرمن جان می آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمرها رفت پس از سوختن ما و هنوز</p></div>
<div class="m2"><p>بوی مهر تو ز خاکسترمان می آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزی از دیده گذشتی تو و خون از مژه ام</p></div>
<div class="m2"><p>آمد و عمر به سر رفت و همان می آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر مژگان تو گردم که به یادش همه شب</p></div>
<div class="m2"><p>مژه در دیده من نوک سنان می آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گریه زینسان نبود تلخ همانا امشب</p></div>
<div class="m2"><p>جای خوناب دل از دیده روان می آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل به عشق تو سپردم به امانت لیکن</p></div>
<div class="m2"><p>باورم نیست که دیگر به میان می آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آب این بحر همه آتش سوزان کشتی</p></div>
<div class="m2"><p>کی به افسون معلم به کران می آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چند گوئی مکش از جور من اشراق نفس</p></div>
<div class="m2"><p>شعله چون در کسی افتد به فغان می آید</p></div></div>