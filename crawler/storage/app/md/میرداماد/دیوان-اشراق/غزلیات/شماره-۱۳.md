---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>اشکم ز سوز سینه چو عمان آتش است</p></div>
<div class="m2"><p>دریای شعله مایه باران آتش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دم به جانبی ز دلم شعله سر زند</p></div>
<div class="m2"><p>یاران در این خرابه مگر کان آتش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب هر نفس که بی تو کشیدم چنان نمود</p></div>
<div class="m2"><p>کز سینه تا به لب همه پیکان آتش است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو شب به ناز خفته و من خسته تا به روز</p></div>
<div class="m2"><p>چون خار وخس که بر سر طوفان آتش است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهمان تست جان ستمدیده روز وصل</p></div>
<div class="m2"><p>مانند خشک همیه که مهمان آتش است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دل مپرس سینه در سوز خفته را</p></div>
<div class="m2"><p>این کوی را هزار خیابان آتش است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم که جان خسته اشراق و درد عشق</p></div>
<div class="m2"><p>گفتا گیاه خشک و بیابان آتش است</p></div></div>