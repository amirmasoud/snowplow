---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>ای که گویی ما به زهد از خود حجاب افکنده‌ایم</p></div>
<div class="m2"><p>رو که ما سجاده تقوی بر آب افکنده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردمان دیده را ما در شب آسودگی</p></div>
<div class="m2"><p>بسترِ خارِ مغیلان وقت خواب افکنده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهر خونِ ما خدا را دل مرنجان غمزه را</p></div>
<div class="m2"><p>ما کتان زندگی در ماهتاب افکنده‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساغر تو باد پر می ما به جام آفتاب</p></div>
<div class="m2"><p>جای می از شیشه دل خون ناب افکنده‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آتش دوزخ نگردد خشک و ما از سادگی</p></div>
<div class="m2"><p>رخت خون‌آلود خود در آفتاب افکنده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این خسیسان محرم عشاق صافی‌دل نیند</p></div>
<div class="m2"><p>درپذیر اشراق اگر بر رخ نقاب افکنده‌ایم</p></div></div>