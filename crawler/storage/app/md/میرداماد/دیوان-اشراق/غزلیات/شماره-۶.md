---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>شعله ها در جان زدی این سینه غمناک را</p></div>
<div class="m2"><p>خرمنی ز آتش چه حاجت بود یک خاشاک را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بکی در سینه تنگم نهان دارم چو راز</p></div>
<div class="m2"><p>آتشی کز شعله خاکستر کند افلاک را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ره عشق تو عمری شد که حیران مانده ام</p></div>
<div class="m2"><p>من که اندر کوی دانش رهبرم ادراک را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچ صیادی به صید خسته در صحرا نتاخت</p></div>
<div class="m2"><p>تیر مژگان تا به کی این سینه صد چاک را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر دو عالم خار شد در چشم اشراق از غمت</p></div>
<div class="m2"><p>هرکه آب خضر دارد خوار دارد خاک را</p></div></div>