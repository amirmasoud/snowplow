---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>ای به درگاه تو از قدس روان قافله‌ها</p></div>
<div class="m2"><p>پیش طوف سر کوی تو خجل نافله‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکجا شاکله فضل تو در ذکر آمد</p></div>
<div class="m2"><p>غیر تشویر نشد شاکله شاکله‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشکل آید همی اسناد تولد به تو زانک</p></div>
<div class="m2"><p>زادن مثل تو نشنید کس از حامله‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مجد ذات تو به حدی که محال آید از آنک</p></div>
<div class="m2"><p>مدرک کنه کمال تو شود عاقله‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکه بی‌یار و مطیع است همین اشراق است</p></div>
<div class="m2"><p>دیگران هرکه شنیدیم بود راحله‌ها</p></div></div>