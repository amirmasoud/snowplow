---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>چنان ز آتش دل سینه مشتعل کردم</p></div>
<div class="m2"><p>که جان آتش سوزنده را خجل کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا که بی تو دعایم به آسمان نرسد</p></div>
<div class="m2"><p>ز بسکه راه فلک ز آب دیده گل کردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مکن تأمل و در خانه دل آتش زن</p></div>
<div class="m2"><p>به چند ارزد ویرانه جا بهل کردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان ز درد دل امشب به دوست نالیدم</p></div>
<div class="m2"><p>که درد را ز دل خویش منفعل کردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزار خرمن آتش فکندم اندر دل</p></div>
<div class="m2"><p>بیا ببین که چه با روزگار دل کردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلی که مایه دکان عیش بود اشراق</p></div>
<div class="m2"><p>منش نثار یکی شوخ دل گسل کردم</p></div></div>