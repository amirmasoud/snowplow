---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>مپرس از من که خون دل شبت از دیده چون آید</p></div>
<div class="m2"><p>چه خون دل همه شب ریشه جانم برون آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدوز آخر به پیکان دیده ام تا کی توان دیدن</p></div>
<div class="m2"><p>که هر سب صد بلا زین رخنه محنت برون آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عزیز من شکر خواب صبوحی کرده کی داند</p></div>
<div class="m2"><p>که بر بیدار غم پاسی شب از سالی فزون آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به سر سودای خام ای دل که باور میکند کاکنون</p></div>
<div class="m2"><p>به دام عنکبوت بخت ما عنقا درون آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در این شبهای بیداری چنان نازک دلم از غم</p></div>
<div class="m2"><p>که کاهی بر دل من همچو کوه بیستون آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا تا آتش اندر خرمن سحر و فسون افتد</p></div>
<div class="m2"><p>چو چشمت بهر جانم بر سر کار فسون آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو باران بارد از چشم همه شب شعله آتش</p></div>
<div class="m2"><p>در این آتش بگو تا کی زمن صبر و سکون آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غلط کردم ره کوی تو مهمان بلا گشتم</p></div>
<div class="m2"><p>مبادا بخت بد یارب کسی رارهنمون آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بجای اشک چشمم ریزه الماس می بارد</p></div>
<div class="m2"><p>که آن پیکان مباد از دیده ام روزی برون آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مگر اشراق را در کار این سودا زبون دیدم</p></div>
<div class="m2"><p>زبون باشد بلی کاری که از بخت زبون آید</p></div></div>