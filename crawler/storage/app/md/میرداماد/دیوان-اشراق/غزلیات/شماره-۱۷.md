---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>گر زمهر بتی دل به قصد کین من است</p></div>
<div class="m2"><p>سپاه فتنه دگر باره در کمین من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلا بگو دگر این گرد راه جلوه کیست</p></div>
<div class="m2"><p>که همچو نور فروزنده در جبین من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به شرع عشق مسلمان نیم،تف دوزخ</p></div>
<div class="m2"><p>اگر نه عاریت از آه آتشین من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غمی که شادی عالم بدو خراج دهد</p></div>
<div class="m2"><p>سریر سلطنتش خاطر حزین من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رهین آه خودم کز فروغ شعله او</p></div>
<div class="m2"><p>هزار دوزخ افروخته رهین من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنون به دست تو باری زمام دل دادم</p></div>
<div class="m2"><p>اگر چه خون به دل عقل پیش بین من است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا به داغ غلامی نشانه کن هر چند</p></div>
<div class="m2"><p>که داغ تو نه به اندازه جبین من است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو از نصیحت من رنج خود مده اشراق</p></div>
<div class="m2"><p>که در حقه دانش در آستین من است</p></div></div>