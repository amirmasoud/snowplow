---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>آنکه در آتش غم سوخت دل خام من است</p></div>
<div class="m2"><p>وآنکه او را غم کس نیست دلارام من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>توکه ته جرعه جام تو بود کوثر عشق</p></div>
<div class="m2"><p>چه خبر داری ازین شعله که آشام من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرمی آتش دوزخ خوی خجلت ریزد</p></div>
<div class="m2"><p>بی تو پیش تف این جرعه که در جام من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چهره افروخته بادت که خوش افروخته ای</p></div>
<div class="m2"><p>شعله ها در بن هر مو که بر اندام من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای که گوئی ز چه دل کعبه غم ساخته ای</p></div>
<div class="m2"><p>چکنم طوف بلا گرد در و بام من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به فسون لب پر شهد تو شیرین نشود</p></div>
<div class="m2"><p>تلخی زهر جفای تو که در کام من است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خبر از عیش کسم نیست همی میدانم</p></div>
<div class="m2"><p>کآتش دل می و غم نقل و بلا جام من است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوست شد دشمن و این بوالعجبی نیست ز دوست</p></div>
<div class="m2"><p>بوالعجب بخت بد تیره سرانجام من است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عنکبوت غمم اشراق خیال رخ اوست</p></div>
<div class="m2"><p>هیچ دانی تو چه عنقاست که در دام من است</p></div></div>