---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>آتش که شعله عاریت از جان ما گرفت</p></div>
<div class="m2"><p>چون برق عشق بود که در آشنا گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای بس که در فراق تو از بخت واژگون</p></div>
<div class="m2"><p>نفرین خویش کردم و گردون دعا گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر جا که جان خسته به بیماری ئی فتاد</p></div>
<div class="m2"><p>عشق تو رفت و بیعت درد از دوا گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این دل که عنکبوت زوایای محنت است</p></div>
<div class="m2"><p>یارب چسان به دام حیل این هماگرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای بیوفا خیال تو چندان به روز هجر</p></div>
<div class="m2"><p>پهلوی ما نشست که بوی وفا گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روزی کتاب هستی ما می نوشت چرخ</p></div>
<div class="m2"><p>تقدیر رفت نسخه اصل از بلا گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شرمنده خیال توام کم قبول کرد</p></div>
<div class="m2"><p>من خاک بودم او ز کرم توتیا گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اشراق چون دو چشم تو در خشکسال هجر</p></div>
<div class="m2"><p>چندان گریستم که کنارم گیا گرفت</p></div></div>