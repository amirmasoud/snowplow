---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>امشب این دل سوز عشقش بر سر جان کرده بود</p></div>
<div class="m2"><p>دوزخی در یک گیاه خشک پنهان کرده بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماجرای شب چه می پرسی نصیب کس مباد</p></div>
<div class="m2"><p>آنچه با جان من امشب روز هجران کرده بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواست غم کز خانه جانم رود نگذاشتم</p></div>
<div class="m2"><p>گرچه این ویرانه رابا خاک یکسان کرده بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یاد آن آتش فروز دل که از بس سوختش</p></div>
<div class="m2"><p>سینه ما را چو آتشگاه یزدان کرده بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جانم آسود ار چه تیرش تارسیدن بر دلم</p></div>
<div class="m2"><p>هر سر موی مرا صد نوک پیکان کرده بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قطره آبی روا برکشت امیدم نداشت</p></div>
<div class="m2"><p>آنکه از اشکم کنار دیده عمان کرده بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی تو با کشتی چشمم موج دریای بلا</p></div>
<div class="m2"><p>کرد آن کاری که با خاشاک طوفان کرده بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پرده اشراق مسکین را مدرکز اضطراب</p></div>
<div class="m2"><p>شعله زیر خار و خس بیچاره پنهان کرده بود</p></div></div>