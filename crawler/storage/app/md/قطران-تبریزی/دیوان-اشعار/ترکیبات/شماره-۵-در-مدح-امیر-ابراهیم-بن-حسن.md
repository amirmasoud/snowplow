---
title: >-
    شمارهٔ ۵ - در مدح امیر ابراهیم بن حسن
---
# شمارهٔ ۵ - در مدح امیر ابراهیم بن حسن

<div class="b" id="bn1"><div class="m1"><p>آن دلبری که خوبی بسیار یار اوست</p></div>
<div class="m2"><p>دردا که در دلم همه پیکار کار اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرد سرای وصل نگشته است یک نفس</p></div>
<div class="m2"><p>پیش در فراق بصد بار بار اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در نار هجر روی چو آبی شدم از آنک</p></div>
<div class="m2"><p>دارنده عاشقان را در نار نار اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر عاشق دو تای ز مشگین او منم</p></div>
<div class="m2"><p>سست و نوان و زار چو بیمار مار اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خون شد دلم ز عشقش و گشتم نحیف و زار</p></div>
<div class="m2"><p>دورم از آن دو غمزه خونخوار خوار اوست</p></div></div>
<div class="b2" id="bn6"><p>از وی همیشه قالب خون خوار خوار به</p>
<p>وانکو ز زخم هست در آزار زار به</p></div>
<div class="b" id="bn7"><div class="m1"><p>تا جان غلام آن بت آزاد زاد شد</p></div>
<div class="m2"><p>دل را مدام صورت فریاد یاد شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اشکم بموج گشت ز بیداد او چنانک</p></div>
<div class="m2"><p>دریا به پیش دجله بغداد داد شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غمگین چرا کند دلم آن دلبری کزو</p></div>
<div class="m2"><p>هنگام دلبری دل نوشاد شاد شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حسنش هزار سینه بیکدم خراب کرد</p></div>
<div class="m2"><p>نزدش حدیث هر دل آباد باد شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرگز کجا شود دلم آزاد از غمش</p></div>
<div class="m2"><p>چون جان غلام آن بت آزاد زاد شد</p></div></div>
<div class="b2" id="bn12"><p>شغل لبش ببوسه اگر داد داد باز</p>
<p>مارش همیشه سنت او زاد زاد باز</p></div>
<div class="b" id="bn13"><div class="m1"><p>ای برده آب از گل خودروی روی او</p></div>
<div class="m2"><p>خوشتر ز قندهار وز مشگوی کوی او</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر بوی این بباغ بخفتم هزار شب</p></div>
<div class="m2"><p>تا بو که یابم از گل شب بوی بوی او</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از چشم او همیشه بلاجوی خلق زد</p></div>
<div class="m2"><p>وز جان شدم همان ز بلاجوی جوی او</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شخصم چو موی گشت و عجب تر نگر که کرد</p></div>
<div class="m2"><p>اشگم چو چشم چشمه آموی موی او</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نالم ببارگاه شه مشرق از غمش</p></div>
<div class="m2"><p>بر من زمانه تنگ تر از روی روی او</p></div></div>
<div class="b2" id="bn18"><p>آن خسروی که همچو سخن گوی گوی او</p>
<p>راند چنان که سیل بهر سوی سوی او</p></div>
<div class="b" id="bn19"><div class="m1"><p>شاهی که روی او چو به مهتاب تاب داد</p></div>
<div class="m2"><p>در گنبد تن از اسباب باب داد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سیماب فضل او چو بشخص عدو رسید</p></div>
<div class="m2"><p>دشمن ز خون سینه بسیماب آب داد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هر روز رزم خنجر او بی کران بود</p></div>
<div class="m2"><p>بی جام او ببزم چو عناب ناب دارد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از عدل چون پدر ز یتیمان روزگار</p></div>
<div class="m2"><p>گوئی مگر بفضل زهرباب باب داد</p></div></div>
<div class="b2" id="bn23"><p>ماند بچنگ دشمن پرتاب تاب او</p>
<p>باشد گشاده بر همه ارباب باب او</p></div>
<div class="b" id="bn24"><div class="m1"><p>بر شخص سهم تیرش بی رنگ رنگ شد</p></div>
<div class="m2"><p>صحرا ز خون صید بنیرنگ رنگ شد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شاه فراخ دل بگه کینه حمله کرد</p></div>
<div class="m2"><p>بر خصم دهر چون سپر تنگ تنگ شد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بر جام تیغ مرگ بداندیش ملک او</p></div>
<div class="m2"><p>از بیم تیغ او می چون رنگ رنگ شد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سهمش بسوی چرخ گذر کرد یک شبی</p></div>
<div class="m2"><p>تا روز حشر بین شباهنگ هنگ شد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شاه ستاره جاه براهیم بن حسن</p></div>
<div class="m2"><p>کاندر نبرد خصم چو هوشنگ شنگ شد</p></div></div>
<div class="b2" id="bn29"><p>او را سزد اگر کند آونگ ونگ را</p>
<p>چون سهم او گداخت بفرسنگ سنگ را</p></div>
<div class="b" id="bn30"><div class="m1"><p>شاها حسام تو همه ناورد ورد کرد</p></div>
<div class="m2"><p>جان عدو چو باد جهان گرد گرد کرد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از خونش کرد سرخ تن خاک تیره را</p></div>
<div class="m2"><p>خون خواست روی آنکه نیازرد زرد کرد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بر جان خویش هرکه نخورده است زینهار</p></div>
<div class="m2"><p>بنگر که جانش تیغ تو در خورد خورد کرد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از چرخ پیر آنکه فرو ماند چون زمان</p></div>
<div class="m2"><p>او را سخاوت تو جوانمرد مرد کرد</p></div></div>
<div class="b2" id="bn34"><p>روی غرور نفس بناورد ورد شد</p>
<p>خویش از نهیب او چو بیفسرد سرد شد</p></div>