---
title: >-
    شمارهٔ ۵۶ - در مدح ابوالفتح علی
---
# شمارهٔ ۵۶ - در مدح ابوالفتح علی

<div class="b" id="bn1"><div class="m1"><p>اگر بتگر چنو داند نگاریدن یکی پیکر</p></div>
<div class="m2"><p>روا باشد اگر دعوی خلاقی کند بتگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه چون او پیکری آید نه حورالعین چنو زاید</p></div>
<div class="m2"><p>نه گر باشد پری شاید چنو هرگز پری پیکر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدو رخ چون شکفته گل بدو لب چون فشرده مل</p></div>
<div class="m2"><p>یکی بندیست بر سنبل یکی مهریست بر گوهر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگل بر تافته زلفش بهم بربافته زلفش</p></div>
<div class="m2"><p>بعنبر یافته زلفش بشم و زیب و رنگ و فر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پری خوبی ستاند زو و مه خیره بماند زو</p></div>
<div class="m2"><p>همی فریاد خواند زو روان مؤمن و کافر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدل ماننده آهن زو شی کرده پیراهن</p></div>
<div class="m2"><p>بپای اندر کشان دامن همی آید بر چاکر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قبای زرد پوشیده برخ بر ماه جوشیده</p></div>
<div class="m2"><p>خمار و خواب کوشیده هم اندر دل هم اندر سر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دو چشم از خواب شبگیران بسان چشم نخجیران</p></div>
<div class="m2"><p>دو رو چون شعله نیران شکسته زلف چون چنبر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نگار مجلس افروزی دلارای روان سوزی</p></div>
<div class="m2"><p>همی دارد مرا روزی ز غم سالی برنج اندر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرآنگه کم بیاد آید همه تدبیر باد آید</p></div>
<div class="m2"><p>از او بی داد و داد آید بدین و داد من ایدر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شرنگ آمیز شد کامم ز کام خویش ناکامم</p></div>
<div class="m2"><p>که شاید بر دهد کامم جدا گشته ز خواب و خور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بتا هم ناز هم نوشی بلاجوئی بلاکوشی</p></div>
<div class="m2"><p>ندارد سود خاموشی کنون از عشق تو دیگر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بخوبی شمع بازاری ز تو بازار بازاری</p></div>
<div class="m2"><p>نه بگذاری نه باز آری دل بی یار و بی یاور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو خورشیدی و من ماهم تو افزونی و من کاهم</p></div>
<div class="m2"><p>برخ ماننده کاهم گشاده بر رخ از غم در</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بدان با دام شیرافکن سپاه صبر من بشکن</p></div>
<div class="m2"><p>چو صف لشگر دشمن سنان خسرو خاور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سرگردان ابوالفتح آنکه روز رزم زو گردان</p></div>
<div class="m2"><p>بوند اندر زمین گردان بخون اندر نهاده سر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>علی کز همت عالی جهان کرد از بدی خالی</p></div>
<div class="m2"><p>بپیروزی و برنائی شده بر خسروان سروان سرور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جهان را پای پیش او مهان را جای پیش او</p></div>
<div class="m2"><p>ندارد پای پیش او بروز رزم شیر نر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>می آراید ایران را همی مالد دلیران را</p></div>
<div class="m2"><p>چو روبه کرد شیران را بنوک نیزه و خنجر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بدشمن تاختن خواهد ازو کین آختن خواهد</p></div>
<div class="m2"><p>جهان پرداختن خواهد بشمشیر از بلا و شر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همه جود است گفتارش همه جنگست کردارش</p></div>
<div class="m2"><p>کسی کو دید دیدارش نخواهد زینت و زیور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ولی و بد سگال او همی یابند مال او</p></div>
<div class="m2"><p>فزونتر باد سال او ز قطر بحر و ریک بر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو بر بالای میمون او برزم اندر نهد یون او</p></div>
<div class="m2"><p>بود فرخ فریدون او عدو ضحاک بد اختر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو او در کارزار آید عدو را کارزار آید</p></div>
<div class="m2"><p>درخت کین ببار آید چو او مغفر نهد بر سر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بداندیش از کمند او نبیند تنگ بند او</p></div>
<div class="m2"><p>ز بیم جان بجنگ او زمین اندر زند مغفر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو او تیر و تبر گیرد قضا راه قدر گیرد</p></div>
<div class="m2"><p>زمانه زو حذر گیرد چو او بیرون کشد خنجر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از او رادی پراگنده وز او زفتی سرافکنده</p></div>
<div class="m2"><p>سعادت پیش او بنده سیاست پیش او چاکر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ایا دارنده کیهان که هم دردی و هم در مان</p></div>
<div class="m2"><p>کند دولت همی پیمان که از تو برنتابد سر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عدو اندر دریغ از تو سر از بدخواه و تیغ از تو</p></div>
<div class="m2"><p>ندیده کس گریغ از تو بروز رزم در لشگر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سعادت باد یار تو سر دشمن شکار تو</p></div>
<div class="m2"><p>بناز اندر قرار تو بهر جائی و هر محضر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مرا تا بنده خواندی تو به پیش اندر نشاندی تو</p></div>
<div class="m2"><p>بهر دولت رساندی تو سرم را تا بماه و خور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همی نازم بفر تو همی نازم بزر تو</p></div>
<div class="m2"><p>رسیدم زیر پر تو بنام و عز و کام و فر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ایا چون تندرستی خوش بکردار جوانی کش</p></div>
<div class="m2"><p>شه دشمن کش و کین کش گشاده کف گشاده در</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>الا تا در بهاران خوش نیاید در جهان آتش</p></div>
<div class="m2"><p>الا تا آب و تا آتش بیکجا ناید اندر خور</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بباغ اندر نگاه گل پدید آید سپاه گل</p></div>
<div class="m2"><p>بنفشه در پناه گل چو زلف اندر رخ دلبر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به پیروزی بقا بادت همه کامی روا بادت</p></div>
<div class="m2"><p>از انده جان جدا بادت بتو پیوسته و فخر و فر</p></div></div>