---
title: >-
    شمارهٔ ۱۱۳ - در زلزلهٔ تبریز و مدح ابونصر مملان
---
# شمارهٔ ۱۱۳ - در زلزلهٔ تبریز و مدح ابونصر مملان

<div class="b" id="bn1"><div class="m1"><p>بود محال مرا داشتن امید محال</p></div>
<div class="m2"><p>به عالمی که نباشد همیشه بر یک حال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن زمان که جهان بود حال زینسان بود</p></div>
<div class="m2"><p>جهان بگردد لیکن نگرددش احوال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دگر شوی تو و لیکن همان بود شب و روز</p></div>
<div class="m2"><p>دگر شوی تو و لیکن همان بود مه و سال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محال باشد فال و محال باشد زجر</p></div>
<div class="m2"><p>مدار بیهده مشغول دل به زجر و به فال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگوی خیره که چون رسته شد فلان اعوان</p></div>
<div class="m2"><p>مگوی خیره که چون برده شد فلان ابدال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو بنده‌ای سخن بندگانت باید گفت</p></div>
<div class="m2"><p>که کس نداند تقدیر ایزد متعال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همیشه ایزد بیدار و خلق یافته خواب</p></div>
<div class="m2"><p>همیشه گردون گردان و خلق یافته هال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل تو بستهٔ تدبیر و نالد از تقدیر</p></div>
<div class="m2"><p>تن تو سخرهٔ آمال و غافل از آجال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عذاب یاد نیاری به روزگار نشاط</p></div>
<div class="m2"><p>فراق یاد نیاری به روزگار وصال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نبود شهر در آفاق خوش‌تر از تبریز</p></div>
<div class="m2"><p>به ایمنی و به مال و به نیکوئی و جمال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز ناز و نوش همه خلق بود نوشانوش</p></div>
<div class="m2"><p>ز خلق و مال همه شهر بود مالامال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در او به کام دل خویش هر کسی مشغول</p></div>
<div class="m2"><p>امیر و بنده و سالار و فاضل و مفضال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یکی به خدمت ایزد یکی به خدمت خلق</p></div>
<div class="m2"><p>یکی به جستن نام و یکی به جستن مال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یکی به خواستن جام بر سماع غزل</p></div>
<div class="m2"><p>یکی به تاختن یوز بر شکار غزال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به روز بودن با مطربان شیرین گوی</p></div>
<div class="m2"><p>به شب غنودن با نیکوان مشگین خال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به کار خویش همی کرد هر کسی تدبیر</p></div>
<div class="m2"><p>به مال خویش همی داشت هر کسی آمال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به نیم چندان کز دل کسی برآرد قیل</p></div>
<div class="m2"><p>به نیم چندان کز لب تنی برآرد قال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خدا به مردم تبریز بر فکند فنا</p></div>
<div class="m2"><p>فلک به نعمت تبریز برگماشت زوال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>فراز گشت نشیب و نشیب گشت فراز</p></div>
<div class="m2"><p>رمال گشت جبال و جبال گشت رمال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دریده گشت زمین و خمیده گشت نبات</p></div>
<div class="m2"><p>دمنده گشت بحار و رونده گشت جبال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بسا سرای که بامش همی بسود فلک</p></div>
<div class="m2"><p>بسا درخت که شاخش همی بسود هلال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کز آن درخت نمانده کنون مگر آثار</p></div>
<div class="m2"><p>وز آن سرای نمانده کنون مگر اطلال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کسی که رسته شد از مویه گشته بود چو مو</p></div>
<div class="m2"><p>کسی که جسته شد از ناله گشته بود چو نال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یکی نبود که گوید به دیگری که مموی</p></div>
<div class="m2"><p>یکی نبود که گوید به دیگری که منال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همی به دیده بدیدم چو روز رستاخیز</p></div>
<div class="m2"><p>ز پیش رایت مهدی و فتنهٔ دجال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کمال دور کناد ایزد از جمال جهان</p></div>
<div class="m2"><p>کمی رسد به جمالی کجا گرفت کمال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چنان که باید بگذاشتم همی شب و روز</p></div>
<div class="m2"><p>به ناز و باده و رود و سرود و غنج و دلال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به مهر بود دل من ربوده چند نگار</p></div>
<div class="m2"><p>به فضل بود دل من سپرده چند همال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدان همال همی دادمی به علم جواب</p></div>
<div class="m2"><p>وزان نگار همی کردمی به بوسه سؤال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>یکی گروه به زیر اندر آمدند ز مرگ</p></div>
<div class="m2"><p>یکی گروه پریشان شدند از آن اهوال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز رفتگان نشنیدم کنون یکی پیغام</p></div>
<div class="m2"><p>ز ماندگان بنبینم کنون بها و جمال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گذشت خواری لیک این از آن بود بدتر</p></div>
<div class="m2"><p>که هر زمان به زمین اندر او فتد زلزال</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زمین نگشتی لرزان اگر نکردی پشت</p></div>
<div class="m2"><p>بحکم شاه ستوده دل و ستوده خصال</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چراغ شاهان مملان که پیش تیغ و کفش</p></div>
<div class="m2"><p>یکیست شیر و شگال و یکیست سیم و سفال</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز گال گردد با مهر او برنگ عقیق</p></div>
<div class="m2"><p>عقیق گردد با کین او برنگ ز گال</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بگاه رادی رادان ازو زنند مثل</p></div>
<div class="m2"><p>بگاه مردی مردان ازو برند مثال</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بروز بزم بود کفش آفتاب نما</p></div>
<div class="m2"><p>بروز رزم بود تیغش آسمان تمثال</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>جهان نباشد با جود او یکی ذره</p></div>
<div class="m2"><p>زمین نه سنجد با حلم او یکی مثقال</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بلای جان معادی توئی به روز نبرد</p></div>
<div class="m2"><p>حیات جان موالی توئی بروز نوال</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سزد که شاهان گاه ترا نماز برند</p></div>
<div class="m2"><p>که سجده گاه سعود است و قبله اقبال</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خدای تیغ ترا از ازل بزال نمود</p></div>
<div class="m2"><p>ز بیم تیغ تو نازاده خشک شد سر زال</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>اگر تو خشم کنی بر هژبر گور افکن</p></div>
<div class="m2"><p>وگر تو کینه کشی از پلنگ آهو مال</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>یکی بچنگال از خشم برکند دندان</p></div>
<div class="m2"><p>یکی بدندان از دست بفکند چنگال</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نهال نیک نروید مگر ز نیک درخت</p></div>
<div class="m2"><p>درخت نیک نخیزد مگر ز نیک نهال</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>جمال و حسن پدر داری و عجب نبود</p></div>
<div class="m2"><p>پدرت هم ز پدر یافته است حسن و جمال</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>اگرچه خیل بود روز جنگ پشت ملوک</p></div>
<div class="m2"><p>تو پشت خیلی در روز جنگ و گاه جدال</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بدست و تیغ تو آراسته است مردی و ملک</p></div>
<div class="m2"><p>چو دست و پای عروسان بباره و خلخال</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>خدایگانا کار جهان چنین آمد</p></div>
<div class="m2"><p>گهی نشاط و سرور و گهی بلا و ملال</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>از آن غمی که گذشته است بر تو یاد مکن</p></div>
<div class="m2"><p>وزان بدی که نیاید بسوی تو مسگال</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>غم گذشته کشیدن بود محال مجاز</p></div>
<div class="m2"><p>غم نیامده بردن بود مجاز محال</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بخواه باده بر آوای مطربان جمیل</p></div>
<div class="m2"><p>بگیر ساغر بر یاد مهتران جمال</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>همیشه تا نبود سرو را ز لاله طراز</p></div>
<div class="m2"><p>همیشه تا نبود ماه را ز مشک شکال</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بسان ماه بتاب و بسان مشگ ببوی</p></div>
<div class="m2"><p>بسان لاله بخند و بسان سرو ببال</p></div></div>