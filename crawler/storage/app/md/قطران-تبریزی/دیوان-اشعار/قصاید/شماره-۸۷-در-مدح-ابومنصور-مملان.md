---
title: >-
    شمارهٔ ۸۷ - در مدح ابومنصور مملان
---
# شمارهٔ ۸۷ - در مدح ابومنصور مملان

<div class="b" id="bn1"><div class="m1"><p>مه نیسان برون آورد بر صحرا یکی لشگر</p></div>
<div class="m2"><p>که با فیروزه گون در عند با بیجاده گون مغفر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شبیخون برده بر خر خیز و نازش برده بر ششتر</p></div>
<div class="m2"><p>شده پر مشک و پر دیبا از ایشان دشت و کوه و در</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بخندد بوستان زیر و بگرید آسمان از بر</p></div>
<div class="m2"><p>یکی چون دیده عاشق یکی چون چهره دلبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بوی باد نوروزی جوان گشت این جهان از سر</p></div>
<div class="m2"><p>بنفشه زلف و نرگس چشم و لاله روی سیمین بر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر گردون همی خواهی یکی در بوستان بنگر</p></div>
<div class="m2"><p>و گر جنت همی خواهی یکی در گلستان بگذر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لباس گلستان خضرا و فرش بوستان عبقر</p></div>
<div class="m2"><p>شکفته هر سویی لاله دمیده هر سویی عبهر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی چون عقد یاقوتین و پنهان اندر آن عنبر</p></div>
<div class="m2"><p>یکی چون مجمر سیمین و رخشان اندر آن آذر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درختان گل اندر باغ هر یک چون بت آذر</p></div>
<div class="m2"><p>همه با چادر اخضر همه با معجر احمر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرفته هر یکی بر سر پر از سیکی یکی ساغر</p></div>
<div class="m2"><p>چو اندر بزم بت رویان گرفته می ز یکدیگر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرازان گور بر صحرا نواخوان مرغ بر عرعر</p></div>
<div class="m2"><p>شقایق رسته از یکسو ز یکسو رسته سیسنبر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دهان لاله پر لؤلؤ کنار گل پر از گوهر</p></div>
<div class="m2"><p>ز مرجان کرده این بالین ز مینا کرده آن بستر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ببستان اندرون بلبل نماید مدح گل از بر</p></div>
<div class="m2"><p>چو اندر مجلس صاحب کشیده بانک خنیاگر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ابومنصور مملان کو بنوک خامه و خنجر</p></div>
<div class="m2"><p>کندخار موافق گل کند خیر مخالف شر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بروز بزم چون حاتم بروز رزم چون حیدر</p></div>
<div class="m2"><p>یکی بیمش بمشرق در یکی جودش بخاور در</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زمانه کهترانشرا همیشه هست چون کهتر</p></div>
<div class="m2"><p>ستاره چاکرانش را همیشه هست چون چاکر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بصد تیشه همی آید برون مثقالی از کان زر</p></div>
<div class="m2"><p>ز یک مدحت برون آید ز کف او دو صد گوهر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ندانم هیچ کانی را ز کف راد او بهتر</p></div>
<div class="m2"><p>ندانم هیچ بحری را ز بحر مدح او برتر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شجاعت چون سرایی گشت و تیغ تیزش او را در</p></div>
<div class="m2"><p>سخاوت همچو جسمی گشت و کف راد او پیکر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز دولت داد بستاند کسی کو باشدش داور</p></div>
<div class="m2"><p>نگردد یار درد و غم کسی کو گرددش یاور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ایا آرایش مجلس و یا آرامش لشگر</p></div>
<div class="m2"><p>ببزم اندر چو افریدون برزم اندر چو اسکندر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز کف تو پدید آید ز سنگ خاره گوهر بر</p></div>
<div class="m2"><p>ز خوی تو پدید آید ز خاک سوده عنبر بر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز کفت راحت مؤمن ز تیغت آفت کافر</p></div>
<div class="m2"><p>یکی دائم ز تو خرم یکی دائم ز تو غمخور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>الا تا رنگ دارد گل الا تا نور دارد خور</p></div>
<div class="m2"><p>از این خرم بود بستان وز آن روشن شود کشور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مبادا دست تو خالی ز زلف یار و از ساغر</p></div>
<div class="m2"><p>بسان باده بادت رخ بسان مورد بادت سر</p></div></div>