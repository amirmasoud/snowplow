---
title: >-
    شمارهٔ ۱۵۷ - فی المدیحه
---
# شمارهٔ ۱۵۷ - فی المدیحه

<div class="b" id="bn1"><div class="m1"><p>چه سرو است این میان بزم نازان</p></div>
<div class="m2"><p>چه مشگست این بگرد ماه تابان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی خورده است گوئی آب وصلت</p></div>
<div class="m2"><p>یکی دیده است گوئی درد هجران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلای دل رخ و زلفین دلبر</p></div>
<div class="m2"><p>شفای جان لب و دندان جانان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی آبست گوئی زیر آتش</p></div>
<div class="m2"><p>یکی کفر است گوئی روی ایمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فری آن سنبلی کش بار عنبر</p></div>
<div class="m2"><p>فری آن نرگسی کش برک پیکان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی کوشد همی بر بستن دل</p></div>
<div class="m2"><p>یکی کوشد همی بر بردن جان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رخ روشنش روزم کرد تاریک</p></div>
<div class="m2"><p>لب خندانش چشمم کرد گریان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی نوش است وزیر نوش لؤلؤ</p></div>
<div class="m2"><p>یکی سیم است وزیر سیم سندان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز جعد او سرای من چو تبت</p></div>
<div class="m2"><p>ز چشم من سرای او چو عمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکی مشگ است افکنده بر آذر</p></div>
<div class="m2"><p>یکی جزعست افکنده بمرجان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز سنبل دارد او بر لاله پرچین</p></div>
<div class="m2"><p>ز عنبر دارد او بر ماه چوگان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکی را سرو شاخ دو ماه بالین</p></div>
<div class="m2"><p>یکی را سیب گوی و عاج میدان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دلم بیچاره کرد و چشم بی خواب</p></div>
<div class="m2"><p>بدان چشم و لب پر بند و دستان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یکی دائم بود پیروزه را گنج</p></div>
<div class="m2"><p>یکی دائم بود بیجاده را کان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همی بندد تن هر کس بزلفین</p></div>
<div class="m2"><p>همی درد دل هر کس بمژگان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یکی همچون کمند رستم زال</p></div>
<div class="m2"><p>یکی همچون سنان شاه اران</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>علی پیرایه شاهان عالم</p></div>
<div class="m2"><p>که رای و همت علایش هزمان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یکی منظرش بگذارد ز گردون</p></div>
<div class="m2"><p>یکی ایوانش بگذارد ز کیوان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو تیغ تیز بنماید در آورد</p></div>
<div class="m2"><p>چو کف راد بگشاید در ایوان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یکی را خشک باشد پیش دریا</p></div>
<div class="m2"><p>یکی را نرم باشد پیش سندان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بروز بخشش آن کف گهربار</p></div>
<div class="m2"><p>بروز کوشش آن تیغ سرافشان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یکی دارد زمین را معدن در</p></div>
<div class="m2"><p>یکی دارد هوا را معدن جان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو او دیگر نپرورده است گیتی</p></div>
<div class="m2"><p>چو او دیگر نیاورده است یزدان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یکی بادا سپاهش را نگه دار</p></div>
<div class="m2"><p>یکی بادا کلاهش را نگهبان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اگر بد شاعری خواندیش مدحت</p></div>
<div class="m2"><p>وگر بد زائری کردیش احسان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یکی بیشی کند بر گنج قارون</p></div>
<div class="m2"><p>یکی بیشی کند بر شعر حسان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سنان نیزه و پیکان تیرش</p></div>
<div class="m2"><p>چون او باشد بر آن شبرنگ پویان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یکی دارد اجل را تیز چنگال</p></div>
<div class="m2"><p>یکی دارد قضا را تیز دندان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز نوک کلک او شد رای خرم</p></div>
<div class="m2"><p>ز نوک خشت او شد روح پژمان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>یکی رخشان و زو جان گشته تاری</p></div>
<div class="m2"><p>یکی تاری وزو جان گشته رخشان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز تیغ او معادی گشته غمگین</p></div>
<div class="m2"><p>ز کف او موالی گشته شادان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>یکی ریحان پدید آرد ز آتش</p></div>
<div class="m2"><p>یکی آتش پدید آرد ز ریحان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ایا کف تو مهری روز بخشش</p></div>
<div class="m2"><p>و یا تیغ تو ابری روز جولان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>یکی را راحت زوار تابش</p></div>
<div class="m2"><p>یکی را محنت بدخواه باران</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>الا تا ابر نیسانی بگردون</p></div>
<div class="m2"><p>الا تا لاله نعمان به نیسان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یکی گریان بود چون چشم عاشق</p></div>
<div class="m2"><p>یکی خندان بود چون لعل جانان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>زمانه باد با تو وعده کرده</p></div>
<div class="m2"><p>ستاره باد با تو کرده پیمان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یکی بر بردن از جان ولی غم</p></div>
<div class="m2"><p>یکی بر بردن از جسم عدو جان</p></div></div>