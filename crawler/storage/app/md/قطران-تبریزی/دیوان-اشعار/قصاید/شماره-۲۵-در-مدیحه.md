---
title: >-
    شمارهٔ ۲۵ - در مدیحه
---
# شمارهٔ ۲۵ - در مدیحه

<div class="b" id="bn1"><div class="m1"><p>خدایگانا جان منا بجان و سرت</p></div>
<div class="m2"><p>که جان بشد ز برم تا جدا شدم ز برت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو موی گشت تنم تا خبر شنیدن تو</p></div>
<div class="m2"><p>چگونه باشم آندم که نشنوم خبرت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگرچه خواب و خور من چو زهر گشت رواست</p></div>
<div class="m2"><p>بهر کجا که توئی نوش باده خواب و خورت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خورد و خواب ندارد خبر تنم شب و روز</p></div>
<div class="m2"><p>ز هجر طلعت فرخنده چو ماه و خورت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر توانم بودی براه رفتن در</p></div>
<div class="m2"><p>بسر بیامدمی همچو ناله بر اثرت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی که با تو بود در سفر بود به بهشت</p></div>
<div class="m2"><p>چو دوزخ است بمن بر ز دوری حضرت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان نبینم ازین بیشتر ز گریه بچشم</p></div>
<div class="m2"><p>اگر بچشم نبینم ز عید پیشترت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه حال بود ترا ره گذر بخوزستان</p></div>
<div class="m2"><p>چرا بدیده من بر نبود رهگذرت؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خطر ندارد زی خلق بنده بی سالار</p></div>
<div class="m2"><p>کنون بجان و دل آگاه گشتم از خطرت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در این سفر چو سکندر بکام خود برسی</p></div>
<div class="m2"><p>ز بهر آنکه چنو بس دراز شد سفرت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بسی کشیدی درد و بسی کشیدی غم</p></div>
<div class="m2"><p>دهاد گیتی ازین بیش کردکار برت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیافرید بمردی و مردمی جفتت</p></div>
<div class="m2"><p>نپرورید برادی و راستی دگرت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هزار طبع شود تازه از یکی سخنت</p></div>
<div class="m2"><p>هزار دیده شود روشن از یکی نظرت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گهر بر تو سفالست و زر به پیش تو سنگ</p></div>
<div class="m2"><p>بدان که بیشتر است از همه شهان گهرت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هزار گنج بود یک عطای ماحضرت</p></div>
<div class="m2"><p>هزار نکته بود یک حدیث مختصرت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هنرت گوئی هست از هنر فزون خردت</p></div>
<div class="m2"><p>خردت گوئی هست از خرد فزون هنرت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بسی نمانده که تا کردگار هر دو جهان</p></div>
<div class="m2"><p>دهد ز هر دو فزون بر جهانیان ظفرت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بود ستاره بجنگ مخالفان سپهت</p></div>
<div class="m2"><p>بود زمانه به پیکار آسمان سپرت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرا بباید رفتن بر پدر دشوار</p></div>
<div class="m2"><p>اگر نه بینم شادان بخانه پدرت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگرچه هست حذر عاجز از قضای خدا</p></div>
<div class="m2"><p>همیشه باد قضا گشته عاجز از حذرت</p></div></div>