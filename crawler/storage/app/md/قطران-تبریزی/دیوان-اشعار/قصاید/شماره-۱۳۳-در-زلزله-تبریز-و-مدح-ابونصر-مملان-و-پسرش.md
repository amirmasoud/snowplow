---
title: >-
    شمارهٔ ۱۳۳ - در زلزله تبریز و مدح ابونصر مملان و پسرش
---
# شمارهٔ ۱۳۳ - در زلزله تبریز و مدح ابونصر مملان و پسرش

<div class="b" id="bn1"><div class="m1"><p>آن غیرت یزدان نگر و قدرت یزدان</p></div>
<div class="m2"><p>از قدرت یزدان چه عجب غیرت چندان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز نرسد کس بسر قدرت ایزد</p></div>
<div class="m2"><p>هرگز نرسد کس بسر غیرت یزدان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گه کوه و بیابان کند از باغ و بساتین</p></div>
<div class="m2"><p>گه باغ و بساتین کند از کوه و بیابان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاید که فرو مانی زین فکرت و غیرت</p></div>
<div class="m2"><p>شاید که فرو مانی زین غیرت حیران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواهی که بدانی همه را یکسر معنی</p></div>
<div class="m2"><p>خواهی که ببینی همه را یکسر برهان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رو قصه تبریز همیخوان و همی بین</p></div>
<div class="m2"><p>شو ساحت تبریز همی بین و همیخوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شهری بدو صد سال برآورده بگردون</p></div>
<div class="m2"><p>خلقی بدو صد سال در او ساخته ایوان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مردمش همه دست کشید از بر پروین</p></div>
<div class="m2"><p>با روش همه بار کشید از سر کیوان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن خلق همه گشت به یک ساعت مرده</p></div>
<div class="m2"><p>و آن شهر همه گشت به یک ساعت ویران</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بس صورت آراسته همچون بت کشمیر</p></div>
<div class="m2"><p>بس خانه افراخته چون روضه رضوان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در بوم شد آنصورت آراسته مدفون</p></div>
<div class="m2"><p>در خاک شد آن خانه افراخته پنهان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنانکه پر از نعمت شان بد همه خانه</p></div>
<div class="m2"><p>آنانکه پر از خواسته شان بد همه دکان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>امروز همی تن بفروشند بیکدانگ</p></div>
<div class="m2"><p>وامروز همی جان بفروشند بیک نان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شهری همه پر نان و در او خلق گرسنه</p></div>
<div class="m2"><p>جائی همه پر آب و در او مردم عطشان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مردم بگه سختی داند محل مال</p></div>
<div class="m2"><p>مردم بگه مرگ شناسد خطر جان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنانکه برفتند ز تیمار برستند</p></div>
<div class="m2"><p>وآنانکه بماندند بماندند در احزان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کس رسته نشد وانکه شد از تخمه اولاد</p></div>
<div class="m2"><p>کس جسته نشد وانکه شد از غصه اخوان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از درد همه روی بکندند بچنگال</p></div>
<div class="m2"><p>وز درد همه دست گزیدند بدندان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن شهر بدینگونه بیاشفت که گفتم</p></div>
<div class="m2"><p>وانشب که بلا داد بر این خلق نگهبان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ما در زفزع یاد نیاورد ز فرزند</p></div>
<div class="m2"><p>عاشق ز جزع یاد نیاورد ز جانان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون روز جزا آن نه همی خورد غم این</p></div>
<div class="m2"><p>چون روز پسین این نه همی خورد غم آن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وز انده بی نانی و بی جامگی امروز</p></div>
<div class="m2"><p>آجال چو آمالش نمانده شده انسان؟</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زانگه که پدید آمده عالم را بنیاد</p></div>
<div class="m2"><p>زآنگه که پدید آمده گیتی را بنیان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>این زلزله نشنیند کس اندر همه گیتی</p></div>
<div class="m2"><p>وین ولوله نادیده کس اندر همه کیهان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از کرده ما رفت همه آفت بر ما</p></div>
<div class="m2"><p>وز کرده خود هیچ نگشتیم پشیمان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آرامش اینانرا کز مرگ رهیدند</p></div>
<div class="m2"><p>او رسته شد و پور دل افروزش مملان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از دیدن آن با دل شادی همه ساله</p></div>
<div class="m2"><p>در مملکت این با دل شادان همسالان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا میر اجل با پسرش باقی باشد</p></div>
<div class="m2"><p>هرگز نرسد بد بتن هیچ مسلمان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>این هست چو مهری که زوالش نبود هیچ</p></div>
<div class="m2"><p>وان هست چو ماهی که در او ناید نقصان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از دولت ایشان شود این شهر دگر بار</p></div>
<div class="m2"><p>بهتر ز عراقین و نکوتر ز خراسان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دیگر نبود ناصح این دولت غمگین</p></div>
<div class="m2"><p>دیگر نبود حاسد این ملکت شادان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>صد دل بود آن را که ترا دارد دلبر</p></div>
<div class="m2"><p>صد جان بود آن را که ترا داند جانان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شیرین تری از مال و نوآئین تری از ملک</p></div>
<div class="m2"><p>چون چشم بجان و بخرد ملک بسامان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>میر عضد آن تاج ملوک همه عالم</p></div>
<div class="m2"><p>بو نصر مکان ظفر و نصرت امکان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بار ایت یارانش رود رایت نصرت</p></div>
<div class="m2"><p>با لشگر خصمانش بود لشگر خذلان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بر دشمن او شهد بود زهر هلاهل</p></div>
<div class="m2"><p>بر حاسد او لاله شود خار مغیلان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از بسکه برد قیمت زرینه گه بذل</p></div>
<div class="m2"><p>از بسکه برد قیمت سیمینه گه خوان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خواهد که دگر باره بر خاک رود این</p></div>
<div class="m2"><p>خواهد که دگر باره سوی سنگ رود آن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سوزنده تر از آتش و سازنده تر از آب</p></div>
<div class="m2"><p>تیغ و کف کافیش بایوان و بمیدان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>زائر کند از جود کفش زرین زیور</p></div>
<div class="m2"><p>تیغش کند از خون عدو میدان الوان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>زی وی نبود خواسته زندانی یک شب</p></div>
<div class="m2"><p>زیرا که شناسد که نه خوش باشد زندان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مهمان بر او باشد چون جان گرامی</p></div>
<div class="m2"><p>داند که بجسم اندر جان هست چو مهمان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از سوی خراسان مه رخشنده برآید</p></div>
<div class="m2"><p>هرگز نرود سوی خراسان مه رخشان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>با طلعت او تیره شود چشمه خورشید</p></div>
<div class="m2"><p>با همت او پست بود گنبد گردان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>طاعت نبرد طبعش و گیتیش به طاعت</p></div>
<div class="m2"><p>فرمان نبرد دستش و گیتیش بفرمان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>باشند پشیمان همه بر نعمت داده</p></div>
<div class="m2"><p>او باشد بر نعمت ناداده پشیمان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>از راستی و رادی پیوسته بتوحید</p></div>
<div class="m2"><p>از مردمی و مردی پیوسته بایمان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>در وعده تو نیست نه تأخیر و نه تأویل</p></div>
<div class="m2"><p>در عادت تو نیست نه تبدیل و نه نسیان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>کهتر ز تو مهتر شود و بسته گشاده</p></div>
<div class="m2"><p>مفلس ز تو قارون شود و غمگین شادان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نام تو چو روز است بهر جای رسیده</p></div>
<div class="m2"><p>جود تو چو روزی است بهر جای فراوان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>در خشک بیابان ز کفت دریا خیزد</p></div>
<div class="m2"><p>دریا شود از تیغت چون خشک بیابان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>از طاقت و امکان نتوان کرد چنو هیچ</p></div>
<div class="m2"><p>وز بنده بهر کار همیخواهد یزدان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>رادیت گه بزم فزون است ز طاقت</p></div>
<div class="m2"><p>مردیت گه رزم فزون است ز امکان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>با دولت تو سندان چون موم شود نرم</p></div>
<div class="m2"><p>بر دشمن تو موم شود سخت چو سندان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تا لؤلؤ عمان نشود خوار چو خارا</p></div>
<div class="m2"><p>تا خار نگیرد محل لاله نعمان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چون لاله نعمان کن در باغ ولی خار</p></div>
<div class="m2"><p>بر دست عدو خار کن از لؤلؤ عمان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بگذار چنین عید هزاران و تو مگذر</p></div>
<div class="m2"><p>مگذر تو و بگذار چنین عید هزاران</p></div></div>