---
title: >-
    شمارهٔ ۴۰ - در مدح ابومنصور وهسودان
---
# شمارهٔ ۴۰ - در مدح ابومنصور وهسودان

<div class="b" id="bn1"><div class="m1"><p>دل بدو دادم که جان از روی او شادان شود</p></div>
<div class="m2"><p>جان من هست او سزد گر دل فدای جان شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بر او نازد دل و جان نیست طرفه زان کجا</p></div>
<div class="m2"><p>دل سوی دلبر گراید جان سوی جانان شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون گل خندانش رخ چون لاله نعمانش لب</p></div>
<div class="m2"><p>گریه بر خلق افتد از عشقش اگر خندان شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لاله نعمان توان چید از رخش در ماه دی</p></div>
<div class="m2"><p>ور بخندد بزم از او پر لؤلؤ عمان شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ور فروغ دو رخش بر لؤلؤ عمان فتد</p></div>
<div class="m2"><p>لؤلؤ عمان برنگ لاله نعمان شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قطره باران شود لؤلؤ و هم لؤلؤ ز شرم</p></div>
<div class="m2"><p>گر بدندانش نمائی قطره باران شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روز رخشان گردد از زلف سیاهش تیره شب</p></div>
<div class="m2"><p>وز رخش چون روز رخشان تیره شب رخشان شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ابر درافشان شود گر بگذرد بر چشم من</p></div>
<div class="m2"><p>ور بزلف او برآید باد مشگ افشان شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ماه میدانی نگارا آفتاب مجلسی</p></div>
<div class="m2"><p>فتنه بر تو جان دل در هر زمانی زآن شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دوست آن باید که او را دوستدار خویشتن</p></div>
<div class="m2"><p>گاه در مجلس خرامد گاه در میدان شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرکه با تو بسته شد زو بگسلد غم چون ز رنج</p></div>
<div class="m2"><p>دور ماند هرکه او نزدیک وهسودان شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنگه بی فرمان او در عهد ایزد جاودان</p></div>
<div class="m2"><p>همچو اندر عهد او یکروز بی فرمان شود؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از کریمی هرچه از پیمان بگردد دشمنش</p></div>
<div class="m2"><p>چون ظفر یابد بر او هم بر سر پیمان شود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کفر گردد کین او کر در دل مؤمن نهی</p></div>
<div class="m2"><p>مهر او کرد در دل کافر نهی ایمان شود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دشمنان ملک او گر چند روز افزون بوند</p></div>
<div class="m2"><p>چون خلاف او کنند افزونشان نقصان شود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>لاله هاشان خار گردد درشان خارا شود</p></div>
<div class="m2"><p>نقدهاشان نسیه گردد حفظشان نسیان شود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سوی او با تیغ و تیر آیند اندر دستشان</p></div>
<div class="m2"><p>تیغ گردد دستها سوفارها پیکان شود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر زمینی را که در وی با عدو جنگ آورد</p></div>
<div class="m2"><p>خاک و خار و سنگ و ریگ او بدگر سان شود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خاک او شنگرف گردد خار او زوبین شود</p></div>
<div class="m2"><p>سنگ او یاقوت گردد ریک او مرجان شود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پیش تیر او شود سندان بسان موم نرم</p></div>
<div class="m2"><p>پیش تیر دشمنانش موم چون سندان شود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای خداوندی که هرکو خفت جفت کین تو</p></div>
<div class="m2"><p>گر فرشته باشد اندر خواب جاویدان شود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از پی آن تا تو روزی گوی در چوگان نهی</p></div>
<div class="m2"><p>گاه مه چون گوی گردد گاه چون چو گان شود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر گهی نکبت رسد ملک ترا چون عادتست</p></div>
<div class="m2"><p>سینه بفروزد ز غم زین دشمنت شادان شود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خسروان را دل نباید خست و رخستی بدانکه</p></div>
<div class="m2"><p>شیر بی چنگال نبود گرچه بی دندان شود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون کنی آهنک او زیر و ز بر گردد جهانش</p></div>
<div class="m2"><p>از پشیمانی و غم با خویشتن پیچان شود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هرچه اندر طالع تو نکبتی بود آن گذشت</p></div>
<div class="m2"><p>زین سپس ملک تو بیش از ملک نوشروان شود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بس نیاید تا تو در روی زمین سلطان شوی</p></div>
<div class="m2"><p>وز همه کس چاکر تو زودتر سلطان شود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هم پشیمان گشت خصم از دیدن دیدار تو</p></div>
<div class="m2"><p>زین پشیمانی و غم هر دم دلش بریان شود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وان کجا ترسد که حجتهای تو نادان گرفت</p></div>
<div class="m2"><p>گرچه دانا مرد چون ترسان شود نادان شود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خسرو امیران کجا یارند دیدن روی تو</p></div>
<div class="m2"><p>گرچه ایمن باشد آنکو با تو در ایمان شود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گرچه رو به بند و دستان بیشتر داند ز شیر</p></div>
<div class="m2"><p>چون ببیند شیر را بی بند و بی دستان شود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ورچه از شاهین کبوتر تیزتر باشد بپر</p></div>
<div class="m2"><p>چون ببیند روی شاهین خیره و لرزان شود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ورچه انجم صدهزار است و یکی هست آفتاب</p></div>
<div class="m2"><p>چون برآید آفتاب انجم همه پنهان شود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وز خرد چون بنگری تو مهتری او کهتر است</p></div>
<div class="m2"><p>عز دارد کهتری کز مهتری ترسان شود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تا جهان باشد مباد از وصل تو خالی جهان</p></div>
<div class="m2"><p>زانکه پیش از رستخیز از هجر تو ویران شود</p></div></div>