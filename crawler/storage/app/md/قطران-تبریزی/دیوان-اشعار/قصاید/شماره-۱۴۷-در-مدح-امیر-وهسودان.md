---
title: >-
    شمارهٔ ۱۴۷ - در مدح امیر وهسودان
---
# شمارهٔ ۱۴۷ - در مدح امیر وهسودان

<div class="b" id="bn1"><div class="m1"><p>بتی که لاله چند از رخانش لاله ستان</p></div>
<div class="m2"><p>چه لاله ای که ندیده است خلق لاله چنان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلای دین و دل آمد بسنبل و بادام</p></div>
<div class="m2"><p>شفای جان و تن آمد بلاله و مرجان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همی رباید دلرا بناز در وصلت</p></div>
<div class="m2"><p>همی ستاند جانرا بناز در هجران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چرا نباشم در هجر او نوان و نوند</p></div>
<div class="m2"><p>چرا نباشم از هجر او نوند و نوان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسیکه دلبر باشد نباشدش غم دل</p></div>
<div class="m2"><p>کسیکه جانان باشد نباشدش غم جان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا بتر که نه دل با من است و نه دلبر</p></div>
<div class="m2"><p>مرا بتر که نه جان با منست و نه جانان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو یادم آید زان نرگس عذاب انگیز</p></div>
<div class="m2"><p>چو یادم آید زان شکر عذاب نشان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنان شوم که ندارم ز هیچ چیز خبر</p></div>
<div class="m2"><p>چنان شوم که ندارم ز هیچ چیز نشان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لبش چو مرجان لیکن بزیر او لؤلؤ</p></div>
<div class="m2"><p>برش چو وشی ولیکن بزیر او سندان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو روی او ندهد نور ماه و هور فروغ</p></div>
<div class="m2"><p>چو قد او نبود شاخ سرو در بستان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مراست تاوان در هجر آن نگار بسی</p></div>
<div class="m2"><p>که هیچ روی نیاید بر او گهی تاوان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کسیکه کار وی از فعل او تباه شود</p></div>
<div class="m2"><p>بود پشیمان چون خصم میر وهسودان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خدایگان زمین و زمان امیر اجل</p></div>
<div class="m2"><p>که هست زیر نگینش همه زمین و زمان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نه بی رضاش کسی شاد باشد از نصرت</p></div>
<div class="m2"><p>نه با رضاش کسی باک دارد از خذلان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هزار بهتان در مدح او بگوید خلق</p></div>
<div class="m2"><p>چو بنگری همه بوده است راست آن بهتان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بجز فتح نشد تیغ او جدا ز نیام</p></div>
<div class="m2"><p>بجز بسعد نشد تیر او جدا ز کمان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ایا مظفر کشورگشای و دشمن بند</p></div>
<div class="m2"><p>ایا موید دینار بخش و شهرستان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بناز یار شد آن کز پی تو جست نیاز</p></div>
<div class="m2"><p>ز سود دور شد آن کز پی تو جست زیان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سخاوت تو گسسته زو عده و تقصیر</p></div>
<div class="m2"><p>شجاعت تو بریده ز تنبل و دستان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بروز جود تو بی نام حاتم طائی</p></div>
<div class="m2"><p>بروز حرب تو بینام رستم دستان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کسی نبیند خان تو خالی از زائر</p></div>
<div class="m2"><p>کسی نبیند خوان تو خالی از مهمان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کدام دشمن کز بیم تو نشد غمگین</p></div>
<div class="m2"><p>کدام حاسد کز هول تو نشد ترسان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>که بود کو ببدی با تو پیشدستی کرد</p></div>
<div class="m2"><p>که نه بپای بلاش اندرون فکند زمان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کدام شاه که یکروز با تو دندان سود</p></div>
<div class="m2"><p>که بنده تو نگشت آخر از بن دندان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اگر گهی حد ثانی فتاد ملک ترا</p></div>
<div class="m2"><p>چه بود پس نبود ملک خالی از حدثان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کنون نگر که ز بخت جوان و دولت پیر</p></div>
<div class="m2"><p>همه شهان هم از آن تواند پیر و جوان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بغم گذاشت همه عمر و آخر از غم مرد</p></div>
<div class="m2"><p>هر آنکسیکه بغمناکی تو شد شادان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رضای یزدان جستی بهر چه کردی تو</p></div>
<div class="m2"><p>بهر چه میکنی از تو رضا شود یزدان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ترا ز خلق جهان کردگار بگزیده است</p></div>
<div class="m2"><p>کسی که خصم تو شد خصم کردگارش دان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اگرچه شاهان گه گه ترا خلاف کنند</p></div>
<div class="m2"><p>بدرگه تو بود بازگشتن ایشان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بود همیشه گذرگاه حبل بر چنبر</p></div>
<div class="m2"><p>بود همیشه گذرگاه گوی بر چوگان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بدولت تو همه کار ملک نیکو کرد</p></div>
<div class="m2"><p>نشاط جانت فرزند مهترت مملان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پسر چنین بود آن را که تو پدر باشی</p></div>
<div class="m2"><p>گهر نخیزد نیکو مگر ز نیکوکان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مرا چنانکه تو دانی نداند ایچ کسی</p></div>
<div class="m2"><p>هم آنچنانم دار و هم آنچنانم دان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بچشم تو که مرا از پی تو باید چشم</p></div>
<div class="m2"><p>بجان تو که مرا از پی تو باید جان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دلم بمدح تو رخشنده چون روان بخرد</p></div>
<div class="m2"><p>تنم بمدح تو پاینده همچو تن بروان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بمن حقوق تو بسیار حبذا آن حق</p></div>
<div class="m2"><p>که خون منت حلالست گر کنی قربان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بهیچ وجه ندارد بطبع ظلم و لیک</p></div>
<div class="m2"><p>بدان و بدمنشان را بریده باد زبان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چنانکه رأی تو باشد هزار سال بزی</p></div>
<div class="m2"><p>چنانکه کام تو باشد هزار سال بمان</p></div></div>