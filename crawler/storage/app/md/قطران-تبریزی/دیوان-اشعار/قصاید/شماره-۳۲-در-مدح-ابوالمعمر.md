---
title: >-
    شمارهٔ ۳۲ - در مدح ابوالمعمر
---
# شمارهٔ ۳۲ - در مدح ابوالمعمر

<div class="b" id="bn1"><div class="m1"><p>آمد نوروز و گشت مشگ فشان باد</p></div>
<div class="m2"><p>ساحت باغ از نسیم باد شد آباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دل تیمار دیده برگ بنفشه</p></div>
<div class="m2"><p>چون زره زنگ خورده خوشه شمشاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون برخ دوست بر فتاده سر زلف</p></div>
<div class="m2"><p>برگ بنفشه ببرد لاله بر افتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دشت بخندد همی ز لاله سیراب</p></div>
<div class="m2"><p>باغ بنازد همی بسوسن آزاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دشت بخندد همی چو چهره شیرین</p></div>
<div class="m2"><p>ابر بگرید همی چو دیده فرهاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کوه چو خر خیز گشت و دشت چو تبت</p></div>
<div class="m2"><p>باغ چو فر خار گشت و راغ چو نوشاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرخ بکهسار هدیه کرد ستاره</p></div>
<div class="m2"><p>دریا گوهر بباغ تحفه فرستاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دشت شد از باد پر ظرائف عمان</p></div>
<div class="m2"><p>باغ شد از ابر پر طرائف بغداد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لاله بصحرا شکفته چون قدح می</p></div>
<div class="m2"><p>کبک چو مطرب نهاده دست بفریاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جز قدح می منه بوقت چنین پیش</p></div>
<div class="m2"><p>جز طرب دل مکن بروز چنین یاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر طرف جوی رسته تازه بنفشه</p></div>
<div class="m2"><p>پیش در افکنده سر چو دشمن استاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شمع بزرگان ابوالمعمر کو کرد</p></div>
<div class="m2"><p>جان و دل ما ز بند درد و غم آزاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پولاد آنجا که عزم اوست چو وشی</p></div>
<div class="m2"><p>وشی آنجا که حزم اوست چو پولاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رادان باشند با سخاوت او زفت</p></div>
<div class="m2"><p>ز فتان گردند با سیاست اوراد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>روزی در وهم او نگردد ناحق</p></div>
<div class="m2"><p>گاهی در طبع او نگنجد بیداد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر کس بیداد خویشتن نپسندد</p></div>
<div class="m2"><p>کس ز تن خویشتن چنو ندهد داد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ایدل مردم بچشم عقل گشاده</p></div>
<div class="m2"><p>چشم کریمی ز دست راد تو بگشاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>علم همیشه ز نوک کلک تو زاید</p></div>
<div class="m2"><p>گوئی علم جهان سراسر از او زاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صاحب میزان فضل و عقل بتو ماند</p></div>
<div class="m2"><p>حاتم نام سخا و جود بتو داد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رادی وشادی ز طبع پاک تو خیزد</p></div>
<div class="m2"><p>شاد مباد آن کجا بتو نبود شاد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا نبود لاد پایدار بر برق</p></div>
<div class="m2"><p>تا نبود کاه پایدار بر باد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هیبت تو باد باد و دشمن تو کاه</p></div>
<div class="m2"><p>خشم تو چون برق باد و خصم تو چون لاد</p></div></div>