---
title: >-
    شمارهٔ ۱۳۲ - در مدح ابوالمعمر
---
# شمارهٔ ۱۳۲ - در مدح ابوالمعمر

<div class="b" id="bn1"><div class="m1"><p>آنچه هست اندر دل من نیست کسرا در دل آن</p></div>
<div class="m2"><p>از جفا و جور این نامهربان سنگین دلان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکه من با او بسازم گردد او ناسازگار</p></div>
<div class="m2"><p>آنکه من زو مهر جویم گردد او نامهربان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکسی کز من بود نازش مرا خواهد نیاز</p></div>
<div class="m2"><p>وآنکسی کز من بود سودش مرا خواهد زیان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کرد تیر من بمانند کمان ترکی که هست</p></div>
<div class="m2"><p>تیر با بالای او گوئی بمانند کمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بپیچد جعد او بر سیم غلطد سنبله</p></div>
<div class="m2"><p>ور بتابد زلف او بر لاله گردد صولجان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر گمان جفت یقین خواهی نگه کن آن دهن</p></div>
<div class="m2"><p>ور نهان جفت عیان خواهی نگه کن آنمیان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن یکی هست آن گمان کو را سخن دارد یقین</p></div>
<div class="m2"><p>وین یکی هست آن نهان کو را کمر دارد عیان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر ندانی ناردان با نار سوزان ساخته</p></div>
<div class="m2"><p>دو رخش را ناردان و دو لبشرا ناردان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هستم از طبع وفا دائم برنج اندر جفا</p></div>
<div class="m2"><p>هستم از طبع هوا دائم نوان اندر هوان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روی زرد و اشک سرخ و رنج بیش و کار کم</p></div>
<div class="m2"><p>چشم تر و کام خشک و صبر پیر و غم جوان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر من و بلبل رسید از گردش گردون ستم</p></div>
<div class="m2"><p>او ز مهر گل نژند و من ز مهر وی نوان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من بتیمار نگارم او بتیمار بهار</p></div>
<div class="m2"><p>من باندوه فراقم او باندوه خزان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شد نگار یاسمن بو از من و زو یاسمن</p></div>
<div class="m2"><p>شد بهار ارغوان رو از من و زو ارغوان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من بجای خویش بینم ناسزا را یادگار</p></div>
<div class="m2"><p>او بجای خویش بیند زاغرا در بوستان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>من ز جور مهر او اندوه مند و هم نژند</p></div>
<div class="m2"><p>او ز جور مهر و آذر مستمند و ناتوان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>من بفریاد و فغان اندوه بگسارم همی</p></div>
<div class="m2"><p>او ندارد تاب آن کآمد بفریاد و فغان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا سپاه اندر جهان آورد آذر ماه ازو</p></div>
<div class="m2"><p>دیگر آئین شد هوا و دیگر آئین شد جهان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کاروان نوبهار از باغ و بستان دور گشت</p></div>
<div class="m2"><p>تا خزان آورد سوی باغ و بستان کاروان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آسمان اکنون بدان رنگست کاکنون آبگیر</p></div>
<div class="m2"><p>آبگیر اکنون بدان نوع است کانگاه آسمان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فرشهای خسروی بربود باد کوهسار</p></div>
<div class="m2"><p>نقشهای مانوی بسترد ابر از گلستان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر نیاید آتش از بالا سوی پستی بطبع</p></div>
<div class="m2"><p>ور بطبع آهن نیاید بر سر آب روان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون همی افتد ز گردون شمعها بر کوهسار</p></div>
<div class="m2"><p>چون همی دارد ز ره بر سر فکنده ناودان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از هوا کافور بارد بر چمن ابر بلند</p></div>
<div class="m2"><p>از چمن دینار بارد بر هوا باد بزان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نار بگرفته است جای ارغوان لعل پوش</p></div>
<div class="m2"><p>زاغ بگرفته است جای بلبلان زند خوان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شاخ زرین گشته از رنگ و فروغ باد رنگ</p></div>
<div class="m2"><p>مرز مشگین کشته از بوی و نسیم ضمیران</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نرگس اندر باغ بر نارنگ بسته چشم ژرف</p></div>
<div class="m2"><p>کرده برنا رنگ باغ او را همانا پاسبان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>این چو زرین جام او را سیم پخته بر کنار</p></div>
<div class="m2"><p>وآن چو زر پخته او را سیم خام اندر میان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رخ ز باده سرخ کن گر زرد شد روی زمین</p></div>
<div class="m2"><p>خانه ز آتش گرم کن گر سرد شد طبع زمان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>این ربوده عکس آن و آن ربوده رنگ این</p></div>
<div class="m2"><p>رنگ این در جان نشان و عکس آن از جان نشان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>این ترا از معجز موسی دهد دائم خبر</p></div>
<div class="m2"><p>و آن ترا از حجت عیسی دهد دائم نشان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بام گردد در دو دیده همچو شام از رنگ این</p></div>
<div class="m2"><p>شام گردد بر دو دیده همچو بام از عکس آن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مر هوا را بوی آن دارد بمشگ اندر عجین</p></div>
<div class="m2"><p>مر زمین را عکس این دارد بزر اندر نهان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>این ببالا بر شود بشتاب همچون لاله برگ</p></div>
<div class="m2"><p>آن بکام اندر شود بد رنگ همچون زعفران</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>این بنورانی چو چشم اوستاد کامگار</p></div>
<div class="m2"><p>وآن بنیکوئی چو خوی او ستاد کامران</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بوالمعمر کآسمان این ملک بر وی وقف کرد</p></div>
<div class="m2"><p>با نشاط بی قیاس و با بقای بیکران</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از پی جاهش همی باید فلک را انس انس</p></div>
<div class="m2"><p>از پی جانش همی باید جهانرا جان جان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گر کند نسبت بطبع او زمین گردد سبک</p></div>
<div class="m2"><p>ور کند نسبت بحلم او هوا گردد گران</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آتش بیداد بنشاند آتش شمشیر او</p></div>
<div class="m2"><p>آتشی دیدی تو هرگز کو بود آتش نشان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از پی زائر گشاده دارد او پیوسته گنج</p></div>
<div class="m2"><p>وز پی مهمان نهاده دارد او همواره خوان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بدسگالشرا بود خون دل اندر جایگاه</p></div>
<div class="m2"><p>دشمنانشرا بود درد و غم اندر دودمان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تیغ او دارد بکوشش دشمنانرا سوگوار</p></div>
<div class="m2"><p>کف او دارد ببخشش دوستانرا شادمان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گر بگویم داستان فضل او از صد یکی</p></div>
<div class="m2"><p>بر پذیرفتن نباشد عقل کس همداستان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هر که او با دولت میمون او گردد قرین</p></div>
<div class="m2"><p>آسمان با دولت و تایید او دارد قرآن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گردد از کینش جنان بر مؤمنان همچون سقر</p></div>
<div class="m2"><p>گردد از مهرش سقر بر کافران همچون جنان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>وانکه نتوان بیزبان گفتن ثنا و مدح او</p></div>
<div class="m2"><p>مرد هان مردمان را چاره نبود از زبان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>تا بیارایند دفتر از ثنا و مدح او</p></div>
<div class="m2"><p>مرزبان مردمان را خامه باشد ترجمان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>خفتنش بر شاخ سرو و رفتنش بر عاج سیم</p></div>
<div class="m2"><p>نقش او زرد و زریر و خوردن او مشک و بان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>روش روشن همچو آتش سرش تیره همچو دود</p></div>
<div class="m2"><p>شخص او در دست جود و علم او بر دل قران؟</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خاکی و آبی است او چون بنگری رنگین سخن</p></div>
<div class="m2"><p>رفتن و رنگش دهد از آب و از آتش نشان؟</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>هرچه بندیشی بوهم اندر بداند بی خبر</p></div>
<div class="m2"><p>هرچه زو خواهی براز اندر بگوید بیدهان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>خار با مهرت پرند و شهد با کینت کبست</p></div>
<div class="m2"><p>بوم با فرت همای و گرگ با عدلت شبان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ای بپیشت میهمان چون زی دگر کس خواسته</p></div>
<div class="m2"><p>زی تو باشد خواسته چون زی دگرکس میهمان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>روز کوشیدن بتیغ تیز هستی کان کین</p></div>
<div class="m2"><p>روز بخشیدن بکف راد هستی کین کان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>گر بخواب اندر ببیند نیزه تو شیر نر</p></div>
<div class="m2"><p>چون شود بیدار در چشمش بود نوک سنان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>از نهیب خنجر زهر آبدارت روز جنگ</p></div>
<div class="m2"><p>زهر گردد مغز دشمن در میان استخوان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ای بکف راد راه مکرمترا رهنمون</p></div>
<div class="m2"><p>وی بنوک کلک فضل فضلها را ترجمان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>من رهیرا هست هر جا نام کاینجا هست نام</p></div>
<div class="m2"><p>من رهیرا هست هر جا نان کاینجا هست نان</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>سوی آذربایگان خواهم شدن کز هر کسی</p></div>
<div class="m2"><p>بنده را بهتر نوازد شاه آذربایگان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>تا نپوید یوز با آهو بهم در مرغزار</p></div>
<div class="m2"><p>تا نپاید باز با تیهو بهم در آشیان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>برنگردد هرگز از تو دولت فرخنده فر</p></div>
<div class="m2"><p>بر نتابد هرگز از تو نعمت باقی عنان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>تا خرد نازد بناز و تا شجر بالد ببال</p></div>
<div class="m2"><p>تا فلک پاید بپای و تا زمین ماند بمان</p></div></div>