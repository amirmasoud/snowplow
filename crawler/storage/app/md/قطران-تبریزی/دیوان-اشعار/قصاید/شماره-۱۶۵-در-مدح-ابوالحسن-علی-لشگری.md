---
title: >-
    شمارهٔ ۱۶۵ - در مدح ابوالحسن علی لشگری
---
# شمارهٔ ۱۶۵ - در مدح ابوالحسن علی لشگری

<div class="b" id="bn1"><div class="m1"><p>غالیه دارد کشیده بر شکفته ارغوان</p></div>
<div class="m2"><p>ارغوان دارد شکفته بر منقش پرنیان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ارغوان هر روزه تازه تر بزیر غالیه</p></div>
<div class="m2"><p>غالیه هر روز خوشبوتر بگرد ارغوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از جفاجوئی که هست آندلبر ناسازگار</p></div>
<div class="m2"><p>از ستمکاری که هست آندلبر نامهربان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر دلم باشد گمان هجر او همچون یقین</p></div>
<div class="m2"><p>بر دلم باشد یقین وصل او همچون گمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من برنگ زعفران و او برنگ لاله برگ</p></div>
<div class="m2"><p>من بنرخ لاله برگ و او بنرخ زعفران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی او چون گلستان و موی او چون سنبلست</p></div>
<div class="m2"><p>طرفه رسته سنبل او در میان گلستان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نه از دل خاست عشقش چون درآویزد بدل</p></div>
<div class="m2"><p>ور نه از جان خاست مهرش چون درآمیزد بجان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از خیال روی من باشد خزان اندر بهار</p></div>
<div class="m2"><p>وز خیال روی او باشد بهار اندر خزان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گیسویش گوئی که خسرو بافته دارد کمند</p></div>
<div class="m2"><p>ابرویش گوئی که خسرو آخته دارد کمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آفتاب لشگر ایران و اران لشگری</p></div>
<div class="m2"><p>کشته زو ایران و اران خرمی را بوستان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرکجا باشد گران از طبع او باشد سبک</p></div>
<div class="m2"><p>هرکجا باشد سبک از حلم او باشد گران</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مشتری را طالع او گفت روزی مرحبا</p></div>
<div class="m2"><p>آسمان را همت او گفت روزی گرم ران</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آسمان هر ساعتی فخر آورد بر روزگار</p></div>
<div class="m2"><p>مشتری هر ساعتی فخر آورد بر آسمان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شاعران گنج و درم بود زو در هر زمین</p></div>
<div class="m2"><p>زائران کان گهر دارند زو در هر مکان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن کجا گنج درم بود از ثنا بنهاد گنج</p></div>
<div class="m2"><p>آن کجا کان گهر بود از سخن بنهاد کان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر ببینی ابر و کفش زان نیاری یاد از این</p></div>
<div class="m2"><p>ور ببینی بحر و طبعش زین نیاری یاد از آن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زان که آن گه گه سرشک افشاند این دائم گهر</p></div>
<div class="m2"><p>زان که آن گه گه بخار آهیخت این دائم روان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هرکه ناز از کین او جوید شود جفت نیاز</p></div>
<div class="m2"><p>هرکه سود از جنگ او جوید شود جفت زیان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هست کوکب را شمار و نیست فضلش را شمار</p></div>
<div class="m2"><p>هست گردون را کران و نیست مدحش را کران</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همت پست موالی زو همی گردد بلند</p></div>
<div class="m2"><p>دولت پیر موافق زو همی گردد جوان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای همیشه نام تو بر نامداران نامدار</p></div>
<div class="m2"><p>وی همیشه کام تو بر کامکاران کامران</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تیغ تو شیریست کو را مغز باشد مرغزار</p></div>
<div class="m2"><p>تیر تو مرغیست کو را دیده باشد آشیان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هم برامش بختیاری هم بمردی کامکار</p></div>
<div class="m2"><p>هم بدانش نامداری هم برادی داستان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر سنان گیرد عدو گردد بر او همچون زره</p></div>
<div class="m2"><p>ور زره پوشد عدو گردد بر او همچون سنان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خدمت تو راه نیکی را همیشه رهنما</p></div>
<div class="m2"><p>مدحت تو لفظ دولت را همیشه ترجمان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر عدو باشد عیان از تیغ تو گردد خبر</p></div>
<div class="m2"><p>ور ولی باشد خبر از کف تو گردد عیان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>طبع شادی داری و رامش ولیکن لاجرم</p></div>
<div class="m2"><p>شادی و رامش همیشه پیش گیری هر زمان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا بود شادی همیشه جفت با شادی بپای</p></div>
<div class="m2"><p>تا بود رامش همیشه یار با رامش بمان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در همه شهری بدست خویش بنشان شهریار</p></div>
<div class="m2"><p>در همه مرزی ز دست خویش بنشان مرزبان</p></div></div>