---
title: >-
    شمارهٔ ۱۲۷ - در مدح ابونصر مملان
---
# شمارهٔ ۱۲۷ - در مدح ابونصر مملان

<div class="b" id="bn1"><div class="m1"><p>اگر باران نباشد در بهاران</p></div>
<div class="m2"><p>سرشگ و آه من بس باد و باران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان را بس بود نالیدن من</p></div>
<div class="m2"><p>اگر بلبل ننالد در بهاران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سحرگه بانگ من بشنو ز مطرب</p></div>
<div class="m2"><p>بجای بانگ کبک کوهساران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسم من سوگوار از عشق اگر چرخ</p></div>
<div class="m2"><p>نپوشد جامه های سوگواران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وگر در بوستان پیدا نیاید</p></div>
<div class="m2"><p>چو دیگر سالها نقش و نگاران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه نقش نگاران داده آن بت</p></div>
<div class="m2"><p>مه خوبان و خورشید نگاران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پری روئی که چون رویش نگارند</p></div>
<div class="m2"><p>میان باغ لاله لاله کاران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بجای نرگس و شمشاد و سنبل</p></div>
<div class="m2"><p>بجای لاله اندر مرغزاران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دو چشم و دو رخ و دو زلف و قدش</p></div>
<div class="m2"><p>بسی نیکوترند از هر چهاران</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لب و دندان او بنگر چو خواهی</p></div>
<div class="m2"><p>پس از سنبل ببستان لاله زاران</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز هر دو بزمگاه شاه خوشتر</p></div>
<div class="m2"><p>کفش بهتر ز شاخ در باران</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خداوند جهان بونصر مملان</p></div>
<div class="m2"><p>سر شاهان و تاج شهریاران</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بکبکان بر چنان برافکند باز</p></div>
<div class="m2"><p>کجا اسب افکند وی بر سواران</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بجای دشمنان از کینه توزان</p></div>
<div class="m2"><p>بجای دوستان از حق گزاران</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جهانداران ز خشم او شکوهند</p></div>
<div class="m2"><p>چو غمازان شکوهند از عیاران</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ببانگ سائلان چونان شود شاد</p></div>
<div class="m2"><p>چو فرزندان همی ز آواز ماران</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ایاگردن نهاده خدمتت را</p></div>
<div class="m2"><p>همه گردنکشان و تاج داران</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بود کین جستن تو آفت جان</p></div>
<div class="m2"><p>نجویند آفت جان هوشیاران</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>قرار این جهان از دولت تست</p></div>
<div class="m2"><p>بداندیش تو بادا بیقراران</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ترا شاهان ز جمع خاک بوسان</p></div>
<div class="m2"><p>ترا خصمان ز خیل خاکساران</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زمانی مهر بر موران بیفکن</p></div>
<div class="m2"><p>زمانی کین بماران بر گماران</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شوند از کین تو ماران چو موران</p></div>
<div class="m2"><p>شوند از مهر تو موران چو ماران</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همه بر کامگاران بردبارند</p></div>
<div class="m2"><p>تو هستی کامران بر کامگاران</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو با این کامگاری بردباری</p></div>
<div class="m2"><p>هزاران آفرین بر بردباران</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تو هستی پیشکار خسروان لیک</p></div>
<div class="m2"><p>ترا چرخ از شمار پیشگاران</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بعالم در ندانم هیچ فضلی</p></div>
<div class="m2"><p>که نسپرده است در تو کردگار آن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شوند از گنج تو غاران چو کوهان</p></div>
<div class="m2"><p>شوند از خیل تو کوهان چو غاران</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مرا مردم همه چاکر شمارند</p></div>
<div class="m2"><p>قدیمی تر ز من چاکر شماران</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ترا بودم ز گاه مشگ ساری</p></div>
<div class="m2"><p>کنون برگشتم از کافور ساران</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گنه کارم تو شاه آمرزگاری</p></div>
<div class="m2"><p>مرا بخشای چون آمرزگاران</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گنه کردم تو فرمودیم کردن</p></div>
<div class="m2"><p>بفضل خود ز من اندر گذاران</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خجسته باد نوروز و بهارت</p></div>
<div class="m2"><p>چنین نوروز بگذاران هزاران</p></div></div>