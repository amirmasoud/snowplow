---
title: >-
    شمارهٔ ۵۳ - در مدح ابوالخلیل جعفر
---
# شمارهٔ ۵۳ - در مدح ابوالخلیل جعفر

<div class="b" id="bn1"><div class="m1"><p>گه بهار همه خلق جفت یار بود</p></div>
<div class="m2"><p>مر از یار جدائی گه بهار بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا چگونه بود در فراق یار قرار</p></div>
<div class="m2"><p>که در وصال کنون باز بی قرار بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کنون که خلق همه در کنار دارد یار</p></div>
<div class="m2"><p>بجای یار مرا اشک در کنار بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سزد ز دوری آن در شاهوار نگار</p></div>
<div class="m2"><p>که جزع من صدف در شاهوار بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بوقت آنکه گل کامکار بوی دهد</p></div>
<div class="m2"><p>ز وصل یار دد و دام کامکار بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز نو بهار گل کامکار بهره من</p></div>
<div class="m2"><p>بدیده و بدل اندر خلیده خار بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا ز یار گسستن بوقت لاله و گل</p></div>
<div class="m2"><p>باختیار بود کی گر اختیار بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنفشه وار دل من نژند و زار بود</p></div>
<div class="m2"><p>کنون که خوردن می در بنفشه زار بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز نوحه کردن و زاری تنم نزاری یافت</p></div>
<div class="m2"><p>تنی که زار بود شاید ار نزار بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز پیش آنکه تن من بکار زار شود</p></div>
<div class="m2"><p>دلم ز هجرت با تن بکارزار بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رهی دراز و دلی زار و درد و هجر دراز</p></div>
<div class="m2"><p>همه جهان را فریاد از این چهار بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دو کس همیشه گرفتار درد و غم باشند</p></div>
<div class="m2"><p>ز درد وغم دل و جانشان نژند و زار بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یکی کسی که ز دلبند خویش دور شود</p></div>
<div class="m2"><p>دوم کسی که بداندیش شهریار بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ابوالخلیل ملک جعفربن عزالدین</p></div>
<div class="m2"><p>که بی رضاش همه فخر خلق عار بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یکی زمان ندهد زینهار خواسته را</p></div>
<div class="m2"><p>جهانش دائم در زیر زینهار بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>موافقان را زو بهره تاج و تخت بود</p></div>
<div class="m2"><p>خلاف او نکند هرکه بختیار بود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کسی که خورد می کین او بجام خلاف</p></div>
<div class="m2"><p>بهوشیاری دائم ورا خمار بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کسی که دم بزند بی هوای او یکبار</p></div>
<div class="m2"><p>همیشه تا بزید در دم دمار بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کند سوار بنانش که را پیاده بود</p></div>
<div class="m2"><p>کند پیاده سنانس که را سوار بود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کسی که پایه او جست جان بداد و نیافت</p></div>
<div class="m2"><p>نجست پایه اش آنکس که هوشیار بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بکام خویش نبیند چنین معادی را</p></div>
<div class="m2"><p>که را خدای معین و زمانه یار بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ندیده شاهی تا این دیار بود چو او</p></div>
<div class="m2"><p>نه نیز بیند تا صد چنین دیار بود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بپیش قدرش گردون چو چشم مور بود</p></div>
<div class="m2"><p>بپیش دستش دریا چو پای مار بود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز هفت گردون بگذشت قدرش از پی آن</p></div>
<div class="m2"><p>بهفت گردون بر یک عطاش بار بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یکی ولی را بخشد هزار بخشش او</p></div>
<div class="m2"><p>کند نگون سخطش گر عدو هزار بود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ترا شها ملکا روزگار هست بسی</p></div>
<div class="m2"><p>همه مراد برآید چو روزگار بود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اگر چه کام دل خویش دیرتر یابی</p></div>
<div class="m2"><p>چو یافته بود این کام پایدار بود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بگاه دشمن تو هست مستعارشها</p></div>
<div class="m2"><p>نه پایدار بود هرکه مستعار بود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شکار زائر و سائل بود خزینه تو</p></div>
<div class="m2"><p>ولایت ملکان مر ترا شکار بود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بمان بفر و بملک اندرون تو چندانی</p></div>
<div class="m2"><p>کجا به نیک و به بد چرخ را مدار بود</p></div></div>