---
title: >-
    شمارهٔ ۱۴ - در مدح ابومنصور
---
# شمارهٔ ۱۴ - در مدح ابومنصور

<div class="b" id="bn1"><div class="m1"><p>دارد آن وشی رخ و وشی برو وشی سلب</p></div>
<div class="m2"><p>رادگاه غمزه چشم و زفت گاه بوسه لب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لؤلؤ لالا شرا از لاله نعمان صدف</p></div>
<div class="m2"><p>لاله نعمانش را از عنبر سارا سلب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم او مخمور و من خوردم بجام مهر می</p></div>
<div class="m2"><p>زلف او لرزان و من دارم ز داغ هجر تب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلف شبرنگش مرا ناهید بنماید بروز</p></div>
<div class="m2"><p>روی رخشانش مرا خورشید بنماید بشب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهر من بروی همه زان نرگسان مهره باز</p></div>
<div class="m2"><p>عجب او بر من همه زان کژدمان بوالعجب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دل و چشمم همی خیزند جیحون و جحیم</p></div>
<div class="m2"><p>وز لب و زلفش همی خیزند عناب و عنب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بس مرا از عاشقی کز عاشقی خیزد بلا</p></div>
<div class="m2"><p>بس مرا از نیکوان کز نیکوان آید شغب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تاکنون کردم طلب پیوند مهر نیکوان</p></div>
<div class="m2"><p>تا توانم خدمت صاحب کنم زین پس طلب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آفتاب مهتران دهر ابومنصور کاوست</p></div>
<div class="m2"><p>از کریمان اختیار و از سواران منتخب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر نسب دارد عرب را فخر دارد بر عجم</p></div>
<div class="m2"><p>گر سخن گوید عجم را فخر باشد بر عرب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روز کوشش آسمان از تیغ او دارد شگفت</p></div>
<div class="m2"><p>روز بخشش آفتاب از دست او دارد عجب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن یکی بارد بجان دشمنان اندر بلا</p></div>
<div class="m2"><p>واین یکی کآرد بطبع دوستان اندر طرب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پادشاهی را نظام و پادشاهی را قوام</p></div>
<div class="m2"><p>نیک نامی را دلیل و شادکامی را سبب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای که مهرت ابر فروردین و احبابت چمن</p></div>
<div class="m2"><p>ای که خشمت آتش سوزنده و اعدا حطب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دست تو چون آفتاب است و موالی چون نبات</p></div>
<div class="m2"><p>تیغ تو چون ماهتابست و معادی چون قصب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زان که زیر سایه کلک تو خلق ایمن زیند</p></div>
<div class="m2"><p>ایمنی را شیر دارد جایگاه اندر قصب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آنکه بر دارد تعب در خدمت تو یک زمان</p></div>
<div class="m2"><p>جاودانه رسته باشد جانش از رنج و تعب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جان دشمن دائم از تیغ تو باشد پرخروش</p></div>
<div class="m2"><p>گنج گوهر دائم از دست تو باشد پر شعب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آنچه تو کردی بجان من ز جود و مردمی</p></div>
<div class="m2"><p>نیست هرگز کرده با من خال و عم و ام و اب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا پدید آرد فلک سنگ و عقیق از یک زمین</p></div>
<div class="m2"><p>تا پدید آرد جهان خار و رطب از یک خشب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بهره دشمنت بأدا سنگ و آن تو عقیق</p></div>
<div class="m2"><p>قسمت دشمنت بادا خار و آن تو رطب</p></div></div>