---
title: >-
    شمارهٔ ۵۰ - در مدح شاه ابوالخلیل جعفر
---
# شمارهٔ ۵۰ - در مدح شاه ابوالخلیل جعفر

<div class="b" id="bn1"><div class="m1"><p>آن پری نشگفت اگر از خوبرویان سر بود</p></div>
<div class="m2"><p>گر بنفشه پر گر و از سنبلش افسر بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکر لؤلؤ نمایست آن لب رامش فزای</p></div>
<div class="m2"><p>گر میان شکر اندر چشمه کوثر بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اندر آن بالا و روی او پدید آید همی</p></div>
<div class="m2"><p>آنکه در کشمیر باشد و آنکه در کشمر بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر ببوئی آن دو زلف و گر ببوسی آن دو لب</p></div>
<div class="m2"><p>جاودان در کام عمرت عنبر و شکر بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خور آمد چون روان دیدار او وان حیرتست</p></div>
<div class="m2"><p>گر بدلجوئی گران کان چون روان در خور بود کذا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی او مهر است پنداری و من ما هم که راست</p></div>
<div class="m2"><p>کاملش چندان بیابم کو مرا همبر بود کذا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن بآئین سنگ دل باشد دل آیینه سنگ</p></div>
<div class="m2"><p>از چه آن بی آذر این همواره پرآذر بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنبری شد پشت من زان زلف کو بر برک گل</p></div>
<div class="m2"><p>گاه چون زنجیر باشد گاه چون چنبر بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون بمجلس در بود پیرایش مجلس بود</p></div>
<div class="m2"><p>چون بلشگر در بود آرایش لشگر بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بنگر آن چشم سیه وان غمرکان دلگداز</p></div>
<div class="m2"><p>گر ندیدی نرکسی کش برگها خنجر بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرش بیند هر زمانی خون رود از دیده هاش</p></div>
<div class="m2"><p>آن کسی کش آرزوی آن پری پیکر بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا بود بیجاده بی دلبند آن گوهرنمای؟</p></div>
<div class="m2"><p>جزع من دایم ز بهران گهر گستر بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در دو چشمش خار باشد چون لبش دار و بود</p></div>
<div class="m2"><p>جور و زلفش سهل باشد چون رخش داور بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از دو چشم من همیشه ابر پر لؤلؤ بود</p></div>
<div class="m2"><p>از دو زلف او همیشه باد پر عنبر بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مرد با جان آن زمان باشد که با جانان بود</p></div>
<div class="m2"><p>مرد با دل آن زمان باشد که با دلبر بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دل ربودی ای پسر زنهار طمع جان مکن</p></div>
<div class="m2"><p>زآنکه جان دیگر نباشد گرچه دل دیگر بود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گرچه ترسانی مرا بر بردن جان زان دو چشم</p></div>
<div class="m2"><p>کاین دل من زو همیشه معدن اخکر بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر مرا بی جان کنی در تن بجای جان مرا</p></div>
<div class="m2"><p>مهر جان افزای خورشید جهان جعفر بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن خداوند خداوندان و تاج سروران</p></div>
<div class="m2"><p>آنکه نعل پاره او تاج هر سرور بود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مرد نیک اختر شود در خدمت او هیچکس</p></div>
<div class="m2"><p>سوی او ناید بخدمت تانه نیک اختر بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر عیان گردد سراسر بر تو پنهان فلک</p></div>
<div class="m2"><p>همتش از جمله برتر بر تو پیداتر بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زانکه شاه از کشتن زن ننگ دارد روز جنگ</p></div>
<div class="m2"><p>آنکه در جوشن بود خواهد که در چادر شود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از پسر زادن بر ایشان شادییی بد پیش از این</p></div>
<div class="m2"><p>شادمانیشان کنون از زادن دختر بود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر بمیرد مؤمنی بی مهر او پیش خدای</p></div>
<div class="m2"><p>روز محشر سر فکنده تر ز هر کافر بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای خداوندی که پیش خیل تو خیل عدو</p></div>
<div class="m2"><p>همچو پیش باد تندی تل خاکستر بود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>این جهان مانند اندامست و تو او را سری</p></div>
<div class="m2"><p>باشد آن اندام بی اندام کو بی سر بود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چاکرت را زین سپس چاکر به از خاقان بود</p></div>
<div class="m2"><p>کهترت را زین سپس کهتر به از قیصر بود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چون تو کشور گیر در گیتی نبوده است و نه هست</p></div>
<div class="m2"><p>هم نخواهد بود وز پشت تو باشد گر بود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بیم در هند است همواره اگر تو ایدری</p></div>
<div class="m2"><p>گرچه تو در هند باشی امر تو ایدر بود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آنکه بستائی مرا هر گاه دارم دوستر</p></div>
<div class="m2"><p>زآنکه نام در میان خطبه و منبر بود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در میان دیگر انبازان مرا این فخر بس</p></div>
<div class="m2"><p>کم چنان چون تو خداوندی ستایش گر بود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مردمان بی خرد گویند قطران کودک است</p></div>
<div class="m2"><p>وانکه او را سال کمتر دانشش کمتر بود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مصطفی را شصت و سه بود اهرمن را صد هزار</p></div>
<div class="m2"><p>وان کجا گوید جز این دیگر حدیثی خر بود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بابت و مجلس بزی تو تابت و مجلس بود</p></div>
<div class="m2"><p>با می و ساغر بمان تو تا می و ساغر بود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تا بباشد روزگار و تا بگردد آسمان</p></div>
<div class="m2"><p>روزگارت بنده باشد آسمان چاکر بود</p></div></div>