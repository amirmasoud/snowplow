---
title: >-
    شمارهٔ ۱۴۶ - در مدح شاه ابوالحسن و شاه ابومنصور
---
# شمارهٔ ۱۴۶ - در مدح شاه ابوالحسن و شاه ابومنصور

<div class="b" id="bn1"><div class="m1"><p>بتی که سجده برد پیش او مه گردون</p></div>
<div class="m2"><p>به نیکوئی بر او نیکوان دیگر دون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدان دو لاله مصقول دل کند مشغول</p></div>
<div class="m2"><p>بدان دو سنبل مفتول دل کند مفتون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر نوان و نگونست زلف او چه عجب</p></div>
<div class="m2"><p>که صد هزار دلست اندر او نوان و نگون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ایا بروی چو گلنار خیز و باده بیار</p></div>
<div class="m2"><p>چو باده ساز رخ خود ز باده گلگون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر نبید بهر جای و هر زمین نهی است</p></div>
<div class="m2"><p>بگنجه نیست بر من نبید نهی اکنون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آنکه گنجه کنون خلدعون را ماند</p></div>
<div class="m2"><p>نبید نهی نباشد بخلدعدن درون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زمین بدیبه و زر اندرون شد پنهان</p></div>
<div class="m2"><p>هوا بعنبر ومشگ اندرون شده معجون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همان وصال پدیدار گشت در هجران</p></div>
<div class="m2"><p>همان بهار پدیدار گشت در کانون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز بس نثار که کردند بر زمین گوئی</p></div>
<div class="m2"><p>برون فکنده زمین گنج خانه قارون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کسی نماند از این وصل در جهان درویش</p></div>
<div class="m2"><p>دلی نماند از این راز در جهان محزون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر بخانه شیر آمده است شیدرواست</p></div>
<div class="m2"><p>بدآنکه خانه شیداست شیر بر گردون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کنون که گشت دو خسرو بیکدگر موصول</p></div>
<div class="m2"><p>کنون که گشت دو کوکب بیکدگر مقرون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دو شهریار قدیم و دو جایگاه قدیم</p></div>
<div class="m2"><p>همان دو خسرو منصور و سید میمون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>امیرابوالحسن و شهریار ابومنصور</p></div>
<div class="m2"><p>که نصرت آید و احسان از آن و این بیرون</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یکی ز گوهر شداد و زو بگوهر بیش</p></div>
<div class="m2"><p>یکی ز تخمه دارا و زو بملک افزون</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ببخت این کند آن خیل دشمنان مخذول</p></div>
<div class="m2"><p>بخیل آن کند این بخت دشمنان وارون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدولت این بود آن را همیشه راهنمای</p></div>
<div class="m2"><p>بنعمت آن بود این را همیشه راهنمون</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یکی بگیرد چندان که داشتی مملان</p></div>
<div class="m2"><p>یکی بگیرد چندانکه داشتی فضلون</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ایا شهی که ز خون عدوت در میدان</p></div>
<div class="m2"><p>بروز جنگ بگردد بگونه گون طاحون</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نه هیچ شهر گشاده است چون تو اسکندر</p></div>
<div class="m2"><p>نه هیچ دشمن بسته است چون تو افریدون</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>قضات هست زبون و اجلت هست مطیع</p></div>
<div class="m2"><p>جهانت هست مسخر زمانه هست زبون</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زمانه را نرسد بر شجاعت تو فسوس</p></div>
<div class="m2"><p>ستاره را نرود در سیاست تو فسون</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو بی قرین و عدیلی بگاه مردی و جود</p></div>
<div class="m2"><p>چنانکه هست خداوند بی چگونه و چون</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بعمرها بمرادی رسد همه کس باز</p></div>
<div class="m2"><p>بهر مراد که خواهی رسی بکن فیکون</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هر آنکه کین تو جوید بجان بود مظلوم</p></div>
<div class="m2"><p>هرآنکه جنگ تو جوید بتن بود مغبون</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همیشه تا خبر طور باشد و موسی</p></div>
<div class="m2"><p>همیشه تا سخن نون باشد و ذوالنون</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ولیت باد چو موسی بناز در که طور</p></div>
<div class="m2"><p>عدوت باد چو ذوالنون برنج اندر نون</p></div></div>