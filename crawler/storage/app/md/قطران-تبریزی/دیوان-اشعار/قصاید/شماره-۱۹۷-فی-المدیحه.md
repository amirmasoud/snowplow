---
title: >-
    شمارهٔ ۱۹۷ - فی المدیحه
---
# شمارهٔ ۱۹۷ - فی المدیحه

<div class="b" id="bn1"><div class="m1"><p>بار خدایا بسی عذاب کشیدی</p></div>
<div class="m2"><p>انده و تیمار گونه گون بچشیدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از قبل مردمان نه از قبل خویش</p></div>
<div class="m2"><p>شادی بفروختی و غم بخریدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نرسد خلق را گزند و بد ترک</p></div>
<div class="m2"><p>خود بگزیدی گزند و لب نگزیدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا که توئی هرگزت گزند نباشد</p></div>
<div class="m2"><p>گز پی مردم گزند خویش گزیدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رنج کشد خلق بهر مال و تو ما را</p></div>
<div class="m2"><p>رنج کشیدی و مالها بخشیدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با همه سختی بخانه غم و تیمار</p></div>
<div class="m2"><p>پرده جان عنکبوت وار تنیدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از شدن جان خویش ترک نکردی</p></div>
<div class="m2"><p>از شدن خانه پدر ترسیدی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا نرسد خم به پشت مملکت اندر</p></div>
<div class="m2"><p>پیش کهان و مهان دهر خمیدی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاهان خواهند خلق را ز پی خویش</p></div>
<div class="m2"><p>تو ز پی خلق خویش را بخشیدی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زانکه برفتی بروم با سپه و گنج</p></div>
<div class="m2"><p>زانکه بسی رنج و ننک بند کشیدی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ما بسلامت بجای خویش بماندیم</p></div>
<div class="m2"><p>تو بسعادت بجای خویش رسیدی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رفتی با مردمی و جستی مردی</p></div>
<div class="m2"><p>مردی کردی و مردمی ورزیدی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خلقت بسیار گفته اند که بگریز</p></div>
<div class="m2"><p>چونت بگفتند در زمان نشنیدی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تات نشستن صواب بود نشستی</p></div>
<div class="m2"><p>چونت رمیدن صواب بود رمیدی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شیر نه ای لیک شیروار بجستی</p></div>
<div class="m2"><p>باز نه ای لیک باز وار پریدی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صف سواران بسی دریدی لیکن</p></div>
<div class="m2"><p>هیچ صفی زین عظیم تر ندریدی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بودی بهر جهان چمیده بمردی</p></div>
<div class="m2"><p>اکنون اندر همه جهان بچمیدی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ایزد دانا امیدهات وفا کرد</p></div>
<div class="m2"><p>زانکه زمانی امید ازو نه بریدی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کس نخریده است بیش ازانکه خریده است</p></div>
<div class="m2"><p>تو بخریدی فزون از آنکه خریدی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ملک خری جاودان بفر پدر تو</p></div>
<div class="m2"><p>کز پی ملک پدر بسی بچمیدی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نیز برای تو خواهد او همه گیتی</p></div>
<div class="m2"><p>پس بنیابت بعمر خویش گزیدی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو نه سزائی شها بیافتن غم</p></div>
<div class="m2"><p>هرچه که آن یافتی همان بسزیدی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بل بستم تن فدای مردم کردی</p></div>
<div class="m2"><p>بل بستم در میان رنج خزیدی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خوردی بسیار غم نبیند خور اکنون</p></div>
<div class="m2"><p>تو نه سزای غمی سزای نبیدی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بنشین با حور چهرگان و مخور غم</p></div>
<div class="m2"><p>بسکه میان هزار دین رسیدی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شاد زی و بر مراد دل بغنو خوش</p></div>
<div class="m2"><p>زانکه بسی بی مراد دل بغنیدی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا تو بجستی شمال وار ز بدخواه</p></div>
<div class="m2"><p>بر دل بدخواه چون سموم وزیدی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از دل بدخواه تو دمار بر آید</p></div>
<div class="m2"><p>باز تو چون لاله در بهار دمیدی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چشم بداندیش تو چو نار کفیده است</p></div>
<div class="m2"><p>تو چو گل کامکار نو شکفیدی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ای عدوی شهریار زاهن و روئی</p></div>
<div class="m2"><p>کامدن شه شنیدی و نکفیدی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر نکفیدی رواست باری از غم</p></div>
<div class="m2"><p>همچو در آتش فکنده مار طپیدی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>صید نکردی اگر چه دام نهادی</p></div>
<div class="m2"><p>سود نکردی اگر چه دیر دویدی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بار خدا خدایگانا شاها</p></div>
<div class="m2"><p>با تو بدی کرد مردمی که بدیدی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>اکنون دانند مردمان که تو خسرو</p></div>
<div class="m2"><p>جان جهانی همه جهان ارزیدی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خلق سراسر بمهر تو گرویدند</p></div>
<div class="m2"><p>چون تو بدادار آسمان گرویدی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شیران با ناچخ قضا نچخیدند</p></div>
<div class="m2"><p>جز تو که با ناچخ قضا بچخیدی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>یوسف روئی و همچو یوسف چاهی</p></div>
<div class="m2"><p>چاه کشیدی ببارگاه رسیدی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>جان و تن دوستان بناز سپردی</p></div>
<div class="m2"><p>چشم و دل دشمنان برنج خلیدی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>قفل غمان بر گرفتی از دل مردم</p></div>
<div class="m2"><p>قفل غمان را بروی خوب کلیدی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مردم چون خوید تشنه اند و تو باران</p></div>
<div class="m2"><p>تازه تو چون بر گل سعادت خویدی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چون تو برفتی همه شدند خماری</p></div>
<div class="m2"><p>زامدن تو همه شدند نبیدی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گاه لب جام می گهی لب جانان</p></div>
<div class="m2"><p>رغم عدو را بمز چنانکه مزیدی</p></div></div>