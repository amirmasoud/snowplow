---
title: >-
    شمارهٔ ۹۱ - در مدح ابوالخلیل جعفر
---
# شمارهٔ ۹۱ - در مدح ابوالخلیل جعفر

<div class="b" id="bn1"><div class="m1"><p>همیشه بد بود اندوه و درد فرقت یار</p></div>
<div class="m2"><p>بتر بوقت گل و صبح روزگار بهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کنونکه باد بهاری کنار پر گل کرد</p></div>
<div class="m2"><p>تهی شده است مرا از گل و بنفشه کنار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهار رویش بر من حصار کرد فراق</p></div>
<div class="m2"><p>کنون که لاله و گل سر برون کند ز حصار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز درد فرقت آن چون چنار قامت دوست</p></div>
<div class="m2"><p>همی بنالم چون فاخته بشاخ چنار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز هجر سی و دو لؤلؤ همی عقیقی گشت</p></div>
<div class="m2"><p>مرا دو جزع چو دو شنبلید لؤلؤ بار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل وصال دلم شاد کام داشت و کنون</p></div>
<div class="m2"><p>همی خلد دل ریشم غم فراق بخار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وصال سالی نرزد بیکشبان فراق</p></div>
<div class="m2"><p>چنانکه مستی نرزد به نیمروز خمار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چگونه باشد از این خسته تر بگیتی دل</p></div>
<div class="m2"><p>چگونه باشد از این بسته تر بگیتی کار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز دوست فرد شدن با غمانش گشتن جفت</p></div>
<div class="m2"><p>ز یار دور شدن با بلاش گشتن یار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شدن ز یار جدا درد یار باشد صعب</p></div>
<div class="m2"><p>چگونه باشد گشتن جدا ز یار و دیار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غم فراق تو دینار کرد گلنارم</p></div>
<div class="m2"><p>فراق داند دینار کردن از گلنار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بوصلش اندر بسیار خرمی دیدم</p></div>
<div class="m2"><p>بهجرش اندر خواهم گریستن بسیار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگرچه هست تنم خسته از جدائی دوست</p></div>
<div class="m2"><p>وگرچه هست دلم تافته ز فرقت یار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فراق یار فرامش کنم چو یاد آرم</p></div>
<div class="m2"><p>ز رفتن ملک شهر بخش گیتی دار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ابوالخلیل خداوند خسروان جعفر</p></div>
<div class="m2"><p>که نام جعفر بسترد دستش از دینار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چه سنک باشد در دست او چو سیم حلال</p></div>
<div class="m2"><p>چه خاک باشد در دست او چو زر عیار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همیشه ترسد از او خصم ملک و دشمن دین</p></div>
<div class="m2"><p>چننکه مردم غماز ترسد از عیار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کسی کجاش بود بی رضای او گفتن</p></div>
<div class="m2"><p>کسی کجاش بود بی هوای او دیدار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بکامش اندر دندان شوند چون سوزن</p></div>
<div class="m2"><p>بچشمش اندر مژگان شوند چون مسمار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر جهان بستاند همی نیارد فخر</p></div>
<div class="m2"><p>وگر ببخشد سیصد خزانه دارد عار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از آنکه نیست جهان را بنزد او قیمت</p></div>
<div class="m2"><p>از آنکه نیست درم را بنزد او مقدار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز زر و گوهر زی او ثناگری خوشتر</p></div>
<div class="m2"><p>سئوال خوشتر نزدیک او ز موسیقار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بکام حاسدش اندر چو قار گردد شیر</p></div>
<div class="m2"><p>بجام ناصحش اندر چو شیر گردد قار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بسا که روز شمار ایستاده باید ماند</p></div>
<div class="m2"><p>اگر کنند شمار عطاش روز شمار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یکی ز لشگر شاهی چو تو ندید فلک</p></div>
<div class="m2"><p>یکی ز لشگر شاهی چو تو ندید سوار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز سنگ روید از آن بر پی ولیش سمن</p></div>
<div class="m2"><p>ز آب خیزد از این بر لب عدویش خار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ایا بمسطر تدبیر کرده ملکت راست</p></div>
<div class="m2"><p>جهان بر اعدا کرده چون نقطه پرگار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بکف راد فزائی بجانور روزی</p></div>
<div class="m2"><p>بلفظ خوب زدائی ز طبعها زنگار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز آب ابر سخای تو قلزمست سرشگ</p></div>
<div class="m2"><p>ز تف آتش تیغ تو دوزخ است شرار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بخواب نوشین اندر شدند خلق بدان</p></div>
<div class="m2"><p>که هست رأی تو بیدار و بخت تو بیدار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بشادمانی هستند خلق مست که هست</p></div>
<div class="m2"><p>دلت به مستی و هشیاری اندران هشیار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ایا بدیدن تو چشم خلق گشته قریر</p></div>
<div class="m2"><p>ایا بدولت تو یافته زمانه قرار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همی روی بسعادت بدرگه سلطان</p></div>
<div class="m2"><p>جهان روشن بر بنده کرد خواهی تار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بهار من چو تو آنجا بوی بود چو خزان</p></div>
<div class="m2"><p>خزان من چو تو اینجا بوی بود چو بهار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اگرچه بر من دوزخ شود ز فرقت تو</p></div>
<div class="m2"><p>شود سپاهان از مقدم تو جنت وار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اگرچه مارا تیمار بی نشاط رسد</p></div>
<div class="m2"><p>رسد بسلطان از تو نشاط بی تیمار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از آن عزیزتر اندر جهان ندارم روز</p></div>
<div class="m2"><p>که باز گردی تو شادمان و خصمان خوار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بجای زر بنهم روی پیش تو بر خاک</p></div>
<div class="m2"><p>بجای در کنم دیده بر سر تو نثار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چنان نثار کنم در مدیح تو دل و جان</p></div>
<div class="m2"><p>که تا جهان بود از نام تو بود آثار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همیشه تا بر دزدان ز دار یابد رنج</p></div>
<div class="m2"><p>همیشه تا ملکان را ز تخت باشد دار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تن موافق تو باد دائم از بر تخت</p></div>
<div class="m2"><p>بر مخالف تو باد دائم از بر دار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کسی که قدح تو گوید ز بخت برنخورد</p></div>
<div class="m2"><p>همیشه باش تو از ملک خویش برخوردار</p></div></div>