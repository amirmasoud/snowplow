---
title: >-
    شمارهٔ ۱۶۷ - در مدح ابومنصور وهسودان
---
# شمارهٔ ۱۶۷ - در مدح ابومنصور وهسودان

<div class="b" id="bn1"><div class="m1"><p>کسی کش دل برد دلبر کسی کش جان برد جانان</p></div>
<div class="m2"><p>که جانان دارد و دلبر سبک دارد دل و جان آن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا برگو که جان و دل بجانان دادم و دلبر</p></div>
<div class="m2"><p>همم دل رفت و هم دلبر همم جان رفت و هم جانان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر باز آیدم دلبر نیندیشم بتیر از دل</p></div>
<div class="m2"><p>وگر باز آیدم جانان نیندیشم بتیغ از جان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه از طمع سلامت خلق عالم دوستی دارد</p></div>
<div class="m2"><p>من از طمع وصال دوست بر دل خوش کنم هجران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نهیب هجر او دارد مرا در وصل او غمگین</p></div>
<div class="m2"><p>امید وصل او دارد مرا در هجر او شادان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فکار مهر و کین دل بدو بادام و دو سنبل</p></div>
<div class="m2"><p>بهار رنج و بار جان بدو نسرین و دو مرجان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر آنگاهی که روی او نبیند چشم بی خوابم</p></div>
<div class="m2"><p>بآب اندر نهان گردد ز تاب آن رخ تابان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رخان دوست چون ماهست چشم من چو نیلوفر</p></div>
<div class="m2"><p>ز نور ماه نیلوفر بآب اندر بود پنهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>الا ای تاخته بر من یکی تیغ آخته بر من</p></div>
<div class="m2"><p>یکی همچون بمن تازی یکی تازی بترکستان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکی حمله ببر بردن یکل حمله سرش بشکن</p></div>
<div class="m2"><p>بجام اندر فکن خونش بیاور سوی من تازان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مگر لختی بیفزاید ز خون او تنم را خون</p></div>
<div class="m2"><p>که خون را من بپالودم ز راه دیده گریان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از آن چون قبله دهقان بسوزانی و تابانی</p></div>
<div class="m2"><p>چو فرزند گرامی را بنازش پرورد دهقان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرا بر یاد افریدون و نوشیروان مئی در ده</p></div>
<div class="m2"><p>کز افریدون خبر دارد نشان دارد ز نوشیروان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو جعد دلبران لرزان چو زلف دلبران بویا</p></div>
<div class="m2"><p>چو اشگ عاشقان روشن چو آه عاشقان سوزان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بزردی چون رخ غمگین وزو غمگین شود خرم</p></div>
<div class="m2"><p>بپاکی چون دل دانا وزو دانا شود نادان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بطعم زهر و زو باشد همیشه عیش چون شکر</p></div>
<div class="m2"><p>برنگ زر و زو باشد همیشه روی چون مرجان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو در جام است زو رخشان نماید دیده تاری</p></div>
<div class="m2"><p>چو در جان رفت زو تاری نماید دیده رخشان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زیان دارد همیشه آب خواب از دیده مردم</p></div>
<div class="m2"><p>چو آبست آن و لیکن هست خواب رفته را درمان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر چه خوردنش دائم حرام و تلخ و خوار آمد</p></div>
<div class="m2"><p>حلال و خوشگوار آمد بیاد خسرو اران</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پناه گر گر و گر زن ستون تخمه و لشگر</p></div>
<div class="m2"><p>چراغ گوهر و کشور ابومنصور وهسودان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اگر خواهی که خدمتکار و مدحت خوان بود چرخت</p></div>
<div class="m2"><p>همیشه خدمت او کن همیشه مدحت او خوان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز بخت دوستان او نگردد یک زمان نصرت</p></div>
<div class="m2"><p>ز روز دشمنان او نگردد یک زمان خذلان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اگر زاهد در این گیتی کند با کین او بیعت</p></div>
<div class="m2"><p>وگر رهبان بدین عالم کند با مهر او پیمان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو رهبان اندر آن عالم بدوزخ در شود زاهد</p></div>
<div class="m2"><p>چو زاهد اندر آن عالم بجنت در شود رهبان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ورا ایزد همی دارد قوی بخت و بلند اختر</p></div>
<div class="m2"><p>کسی او را بود دشمن که باشد دشمن یزدان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اگر بر گنبد گردان بگرداند زمانی دل</p></div>
<div class="m2"><p>ز بیم او فرو ماند زمانی گنبد گردان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو را خیل و رهی ای شاه بسیارند و من دائم</p></div>
<div class="m2"><p>رهی را کی کم از قلاش و خیلی کمتر از ترکان؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بجنگ آهنگ او کردند با پیکان بسا سرکش</p></div>
<div class="m2"><p>بمردی باز گردانید بر اندامشان پیکان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کنون تا از سر ایشان تو سایه بر گرفتستی</p></div>
<div class="m2"><p>نگه کن تا چه آورده است گردون بر سر ایشان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همیشه عزم ایشان بود بر تاراج و بر کشتن</p></div>
<div class="m2"><p>چو باشد عزمشان آنگونه باشد حالشان اینسان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هلاک آنگه شود عاصی که بالا گیردش قوت</p></div>
<div class="m2"><p>چنان چون مور کو گردد هلاک آنگه که شد پران</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خداوندا من این چندان بیک لفظ تو بنوشتم</p></div>
<div class="m2"><p>ز بهر مهر تو کردم همه دشوارها آسان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اگر شایم ترا چاکر پدید آرم یکی نیکی</p></div>
<div class="m2"><p>و گرنه چون ترا باشد پدیداری کنم فرمان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>فزون از طاقت امکان نگیرد بنده را ایزد</p></div>
<div class="m2"><p>ندارم من بدشواری فزون زین طاقت امکان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>الا تا در مه کانون نروید سبزه در صحرا</p></div>
<div class="m2"><p>الا تا در مه نیسان بروید لاله نعمان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همیشه باد خصم تو چو سبزه در مه کانون</p></div>
<div class="m2"><p>همیشه باد یار تو بسان لاله در نیسان</p></div></div>