---
title: >-
    شمارهٔ ۳ - در مدح ابوالخلیل جعفر
---
# شمارهٔ ۳ - در مدح ابوالخلیل جعفر

<div class="b" id="bn1"><div class="m1"><p>تا داد باغ را سمن و گل بنونوا</p></div>
<div class="m2"><p>بلبل همی سراید بر گل بنونوا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رود و سرود ساخته بر سرو فاخته</p></div>
<div class="m2"><p>چون عاشقی که باشد معشوق او نوا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشک و عبیر بارد بر گلستان شمال</p></div>
<div class="m2"><p>در و عقیق کارد در بوستان هوا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر نیلگون بنفشه فشاند شکوفه باد</p></div>
<div class="m2"><p>همچون ستارگان زبر نیلگون سما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش از همه گلی گل رعنا نمود روی</p></div>
<div class="m2"><p>یکروش از نشاط و دگر روش از عنا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روئی چو روی عاشق و روئی چو روی دوست</p></div>
<div class="m2"><p>این برده رنگ بد و آن لون کهربا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سرخ لاله باد دریده نقاب سبز</p></div>
<div class="m2"><p>ابرش کنار کرده پر از در پر بها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون طفل هندوان نگران اندر آئینه</p></div>
<div class="m2"><p>ماغان همی کنند بحوض اندر آشنا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خیری چو روی عاشق بیچاره از فراق</p></div>
<div class="m2"><p>لاله چو روی دلبر میخواره از حیا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هامون ز سبزه و گل پر طوطی و تذرو</p></div>
<div class="m2"><p>گردون ز میغ دارد پیرایه قطا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تابان چو ناردانه سرخ از بر پرند</p></div>
<div class="m2"><p>بیجا ده رنگ لاله ز پیروزه گون گیا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اکنون که شد هوا خوش و باغ ایستادء کش</p></div>
<div class="m2"><p>دارد هوای هجر مرا زار در هوا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اکنون مرا که خلق خورد بر شقاق می</p></div>
<div class="m2"><p>باید بجام هجران خوردن می شقا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اکنون که جفت در بهائی شود درخت</p></div>
<div class="m2"><p>خواهیم گشت فرد ز یاقوت پربها</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بیگانه گشت خواهم از آن چشم نرگسین</p></div>
<div class="m2"><p>اکنون که باغ گردد با نرگس آشنا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اکنون که نوبهار جهان را نوا دهد</p></div>
<div class="m2"><p>من گشته خواهم از دل و دلبند بینوا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اکنون که هرکسی ز جدائی جدا شود</p></div>
<div class="m2"><p>از کام دل بمانم بی کام دل جدا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زان چون گل و بنفشه رخ و زلف بگسلم</p></div>
<div class="m2"><p>چون از گل و بنفشه نسیم آورد صبا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هنگام سنبل و سمن و گل بری شوم</p></div>
<div class="m2"><p>زان گلرخان سنبل زلف و سمن لقا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اکنون که شد درخت دو تا از وصال گل</p></div>
<div class="m2"><p>گردد تنم ز هجر گل روی تو دو تا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>روز وصال عشق بلا باشد ای عجب</p></div>
<div class="m2"><p>اندر فراق عشق بتر باشد آن بلا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دوری ز دوست روی نهادن براه دور</p></div>
<div class="m2"><p>از درد و غم چگونه شود جان من رها</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>این راه جز بکشتی نتوان گذشت از آنک</p></div>
<div class="m2"><p>طوفان همی نماید چشم من از بکا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ترسم کز آب چشم من اندر فراق یار</p></div>
<div class="m2"><p>بانگ آید از سپهر علی الجودی استوا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>طوفان لجه کم نتوان کرد از زمین</p></div>
<div class="m2"><p>الا بتف تیغ جهان سوز پادشا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جعفر که زر جعفری از دست وی کساد</p></div>
<div class="m2"><p>چونان که عدل گستری از تیغ او روا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از مردمی ندارد کالای کس حلال</p></div>
<div class="m2"><p>وز راستی ندارد رنج عدو روا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از دست او شکوه برد نیل و هیرمند</p></div>
<div class="m2"><p>وز تیغ او ستوه شود پیل و اژدها</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دریا ستاند از کف بخشان او سلف</p></div>
<div class="m2"><p>خورشید خواهد از رخ رخشان او بها</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خالی شود ز خیر کسی کش کند خلاف</p></div>
<div class="m2"><p>راضی شود ز بخت کسی کش دهد رضا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بریان شود به نیل در از تیغ او نهنگ</p></div>
<div class="m2"><p>گردان شود ببادیه از دستش آسیا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زرین شود ز خدمت او خوان خادمان</p></div>
<div class="m2"><p>بهتر ز خدمتش مشناس آنچه کیمیا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بر ز ایران چو عاشق بر دوست شیفته</p></div>
<div class="m2"><p>بر سائلان چو مفلس بر مال مبتلا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بس یاد کف رادش بر راست دل دلیل</p></div>
<div class="m2"><p>بس باد روی خویش بر خوی خوش گوا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>روی موافقان شود از مهرش ارغوان</p></div>
<div class="m2"><p>مروای حاسدان شود از کینش مرغوا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گردد هزار شاه رهی زو بیک خلاف</p></div>
<div class="m2"><p>گردد هزار گنج تهی زو بیک عطا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>داناست بی معلم و داهی است بی دسیس</p></div>
<div class="m2"><p>راد است بی سپاس و کریمست بی ریا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یابد عطا فزونش کسی کش فزون هنر</p></div>
<div class="m2"><p>بیند عفو فزونش کسی کش فزون خطا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>یک نقطه نیست بر دل او خالی از کرم</p></div>
<div class="m2"><p>یک موی نیست بر تن او فارغ از صفا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز آزار او حذر کن و آزرم او بجوی</p></div>
<div class="m2"><p>کآزار او فنا بود آزرم او بقا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ای فخر آل آدم و شاهنشه عجم</p></div>
<div class="m2"><p>چون جان مصطفی دلت آئینه صفا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از سیرت تو تازه شد آئین کیقباد</p></div>
<div class="m2"><p>وز داد تو نواخته شد رسم مصطفا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هم مشتری بطالع و هم مشتری بفال</p></div>
<div class="m2"><p>هم مشتری سعادت و هم مشتری لقا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>جوینده ای به در صدف در آفرین</p></div>
<div class="m2"><p>خرنده ای بگوهر کان گوهر ثنا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ایزد کناد ملک ترا ایمن از زوال</p></div>
<div class="m2"><p>یزدان کناد عمر تو را ایمن از فنا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>آن را که بر خلاف تو گردن کشی کند</p></div>
<div class="m2"><p>گردون کشد بر آتش تیمار گردنا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بر دشمنان ضیا شود از تیغ تو ظلم</p></div>
<div class="m2"><p>بر دوستان ظلم شود از تیغ تو ضیا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ایزد تو را ز جمع ملوک اختیار کرد</p></div>
<div class="m2"><p>چونانکه مصطفی را از جمع انبیا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>دشمن چگونه گردد چون تو بزر عزیز</p></div>
<div class="m2"><p>اعمی چگونه گردد بینا ز توتیا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ناهید پیش همت توپست چون زمین</p></div>
<div class="m2"><p>خورشید پیش طلعت تو خرد چون سها</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هر رنج را امیدی و هر ناز را سبب</p></div>
<div class="m2"><p>هر بند را کلیدی و هر درد را دوا</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>در مدحت تو موی موالی شود زبان</p></div>
<div class="m2"><p>از هیبت تو روی معادی شود قفا</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>از بسکه داد چرخ ز هر دانشیت بهر</p></div>
<div class="m2"><p>بخری تو نزیبد فرزند برخیا</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ترسان اجل ز تو چو امل ترسد از اجل</p></div>
<div class="m2"><p>لرزان قضا ز تو چو قدر لرزد از قضا</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تا وصف غرقه گشتن فرعونیان بود</p></div>
<div class="m2"><p>تانعت کربلا بود و آن همه بلا</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بادند دشمنانت چو فرعونیان غریق</p></div>
<div class="m2"><p>خصمانت گشته مرده چو کفار کربلا</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>نوروز بر تو فرخ و گلگون ز باده رخ</p></div>
<div class="m2"><p>امر تو گشته نافذ بر خلخ و ختا</p></div></div>