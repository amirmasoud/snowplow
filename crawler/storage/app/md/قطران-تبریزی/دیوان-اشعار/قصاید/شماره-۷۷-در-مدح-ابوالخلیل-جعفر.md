---
title: >-
    شمارهٔ ۷۷ - در مدح ابوالخلیل جعفر
---
# شمارهٔ ۷۷ - در مدح ابوالخلیل جعفر

<div class="b" id="bn1"><div class="m1"><p>ز روزنامه شاهان چنین دهند خبر</p></div>
<div class="m2"><p>چنین کنند بزرگان چیره دست هنر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که شهریار زمین کرد و پادشاه زمان</p></div>
<div class="m2"><p>امیر و سید و خورشید خسروان جعفر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگرچه دیر همی داد داد او گردون</p></div>
<div class="m2"><p>و گرچه دیر همی جست کام او اختر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنون که دادش این داد و جست کامش آن</p></div>
<div class="m2"><p>از او نتابد تأیید روی تا محشر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بهر خدمتش آورد شهریار اران</p></div>
<div class="m2"><p>سپاه خویش برای نبرد بسته کمر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی بتیر فکندن بسان ارش نیو</p></div>
<div class="m2"><p>یکی بدرع دریدن بسان رستم زر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بجای جامه بتنشان همیشه بر جوشن</p></div>
<div class="m2"><p>بجای تاج بسرشان همیشه بر مغفر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسال و ماه بود طرف زینشان بالین</p></div>
<div class="m2"><p>بسال و ماه بود پشت اسبشان بستر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیاید از دهن آواز سوی گوش چنانک</p></div>
<div class="m2"><p>کجا رود ز کمان تیرشان بسوی بصر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بتیغ مغز شکاف و بنیزه دیده گذار</p></div>
<div class="m2"><p>بتیر شیر شکار و بگرز شاه شکر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بتن چو کوه ولیکن بتاب کوهستان</p></div>
<div class="m2"><p>بتک چو باد و لیکن بسم باد سپر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پناه ایشان در بیشه ای که بود همه</p></div>
<div class="m2"><p>چو زلف خوبان کاندر شده بیکدیگر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بچاره کردی باد اندر او همیشه گذار</p></div>
<div class="m2"><p>بباره کردی دیو اندر او همیشه گذر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بماه آذر از برق تیغ لشگر شاه</p></div>
<div class="m2"><p>بغز و ایشان اندر فروختند آذر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بدان سپاه نبود او نیازمند ولیک</p></div>
<div class="m2"><p>بدان سپاه شهان خواند تا بهر کشور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خبر دهند که چون او رود بحرب عدو</p></div>
<div class="m2"><p>بود بلشگرش اندر شه اران و خزر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همی بفخر بخوانند جنگ بیژن  گیو</p></div>
<div class="m2"><p>که او میان گرازی بزد بیک خنجر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بیک خدنگ ملک لشگری شکست کجا</p></div>
<div class="m2"><p>گراز بود همیشه غذای آن لشگر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بتن موافق پیکار کین شاه جهان</p></div>
<div class="m2"><p>بدل موافق گفتار دین پیغمبر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سپاهشان را کردند تار و مار همه</p></div>
<div class="m2"><p>زمینشان را کردند پاک زیر و زبر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فراز نیزه اینان جگر بجای سنان</p></div>
<div class="m2"><p>میان سینه آنان سنان بجای جگر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از آن زمینها چندان غنیمت آوردند</p></div>
<div class="m2"><p>که از شنیدن و دیدنش عاجز است بشر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همی نداند کردن مهندس او را حد</p></div>
<div class="m2"><p>همی نیارد کردن محاسب او را مر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عدو در اول آذر بجست کینه شاه</p></div>
<div class="m2"><p>کشید کینه از او هم در اول آذر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همان عدوی خدا و خدایگان جهان</p></div>
<div class="m2"><p>که گفت نیست کسی در جهان مرا همسر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همیشه افسر شاهی مرا سزد که منم</p></div>
<div class="m2"><p>بخسروان و بشاهان دهر چون افسر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خدای داد بدست خدایگانش چنان</p></div>
<div class="m2"><p>بجای افسر بر سر همی کند معجر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زهی مؤید و کشورگشای و دشمن بند</p></div>
<div class="m2"><p>زهی مظفر و فیروز بخت و نیک اختر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از این ظفر که تو کردی بترک رفت نشان</p></div>
<div class="m2"><p>از این هنر که تو جستی بروم رفت خبر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شگفت نیست گرت بندگی کند خاقان</p></div>
<div class="m2"><p>عجیب نیست گرت چاکری کند قیصر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سر مخالف در زیر چنبر ادب است</p></div>
<div class="m2"><p>اگر ز چنبر پیمانت کرد بیرون سر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اگر نه جست رضای تو زود کیفر برد</p></div>
<div class="m2"><p>وگر رضات نجوید دگر برد کیفر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ایا فزوده ز تو نام لشگر اسلام</p></div>
<div class="m2"><p>و یا شکسته ز تو فر لشگر کافر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سنان تو اجل است و سپاه خصم امل</p></div>
<div class="m2"><p>سپاه تو قدر است و حصار خصم حذر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ایا ز بخشش تو خیل آز کشته هبا</p></div>
<div class="m2"><p>و یا ز رامش تو خون شرم گشته هدر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یکی فرو شود از هیبت تو تا ماهی</p></div>
<div class="m2"><p>یکی فرا رود از نعمت تو تا محور</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بشعرهای دگر مر ترا همی گفتم</p></div>
<div class="m2"><p>که ملک دشمن خواهد شدن ترا یکسر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ببود هرچه بگفتم من و دگر باشد</p></div>
<div class="m2"><p>پدید کشت نشان اندر این نخست سفر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همه کسان سخن من بفال نیک شمرد</p></div>
<div class="m2"><p>تو نیز هم سخن من بفال نیک شمر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همیشه نازش چاکر بود بخدمت تو</p></div>
<div class="m2"><p>اگر زمانه شود چاکر ترا چاکر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همیشه مهر تو جوید اگر چه نیست آنجا</p></div>
<div class="m2"><p>همیشه شکر تو گوید اگرچه هست ایدر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هزار یک نتواند ز فضلهای تو گفت</p></div>
<div class="m2"><p>اگر ز مدحت تو میکند دو صد دفتر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همیشه تا نبود هیچ شکری چون زهر</p></div>
<div class="m2"><p>همیشه تا نبود هیچ آهنی چون زر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بدست ناصحت اندر چو زر بود آهن</p></div>
<div class="m2"><p>بکام حاسدت اندر چو زهر باد شکر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هزار شهر بگیر و هزار تاج ببخش</p></div>
<div class="m2"><p>هزار شیر ببند و هزار صف بر در</p></div></div>