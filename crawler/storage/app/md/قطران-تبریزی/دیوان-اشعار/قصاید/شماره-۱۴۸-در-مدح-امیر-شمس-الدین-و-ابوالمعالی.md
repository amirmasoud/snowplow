---
title: >-
    شمارهٔ ۱۴۸ - در مدح امیر شمس الدین و ابوالمعالی
---
# شمارهٔ ۱۴۸ - در مدح امیر شمس الدین و ابوالمعالی

<div class="b" id="bn1"><div class="m1"><p>بزلف غالیه رنگی بروی آینه گون</p></div>
<div class="m2"><p>ز عشق هر دو مرا روی زرد و رای نگون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برنگ آب گل و می شده است دیده من</p></div>
<div class="m2"><p>ز مهر آن لب می رنگ و چهره گلگون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه سرو نازد چون قامت تو در بستان</p></div>
<div class="m2"><p>نه ماه تابد چون عارض تو بر گردون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زمانه تا برخت چشم بد همی نرسد</p></div>
<div class="m2"><p>همی نویسد گردش ز غالیه افسون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر کمر بندی زی میانت راهنمای</p></div>
<div class="m2"><p>وگر سخن گوئی زی دهانت راهنمون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کس از میانت نگفتی خبر که مدحش چیست</p></div>
<div class="m2"><p>کس از دهانت نداری نشان که وصفش چون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از آن فزاید هر روز بر تو مهر مرا</p></div>
<div class="m2"><p>که نیکوئیت فزونست و مردمی افزون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بباغ پر گل ماند رخ تو مالامال</p></div>
<div class="m2"><p>زمانه بسته بشمشاد گرد او پر هون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لب تو خسته مژگانت را دهد مرهم</p></div>
<div class="m2"><p>دل من از پی این شد بمهر تو مرهون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو موم شد دل سنگ من از هوای رخت</p></div>
<div class="m2"><p>چو شد ز بهر ملک نرم روزگار حرون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جهان ستان چو ملوکان باستان جستان</p></div>
<div class="m2"><p>که هست خانه فرهنگ را بفضل ستون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بشهریاری شکاری بسان اسکندر</p></div>
<div class="m2"><p>بروزگار شناسی بسان افریدون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه هیچ مرد بود بی نوا بدرگه او</p></div>
<div class="m2"><p>نه هیچ خلق بود تشنه بر لب جیحون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کفش چو بحری موج گهر بخارش جود</p></div>
<div class="m2"><p>سنانش ابری با رانش سیل و سیلش خون</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بتیغ تیز دمار صناعت داود</p></div>
<div class="m2"><p>بکف راد هلاک فکنده قارون</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هزار یک بنیاید برون دریا آب</p></div>
<div class="m2"><p>که در و دینار آید ز دست او بیرون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بگاه خشم بود دور طبع او ز شتاب</p></div>
<div class="m2"><p>بگاه جود بود دور طبع او ز شتاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جفا بگوید و پیش آورد همی تأخیر</p></div>
<div class="m2"><p>سخا بگوید و پیدا کند بکن فیکون</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گه مجالسه خلقش چو عنبر سار است</p></div>
<div class="m2"><p>گه مذاکره لفظش چو لؤلؤ مکنون</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ایا بدانش چون مهتر ارسطالیس</p></div>
<div class="m2"><p>بدین و دولت چون اوستاد افلاطون</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همه ببدره دهی جعفری و منصوری</p></div>
<div class="m2"><p>همه برزمه دهی ششتری و سقلاطون</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بروز رامش و رادی زبون دست و دلی</p></div>
<div class="m2"><p>بروز کوشش و فرمان ترا زمانه زبون</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ترا عدو نبود مرد طالع مسعود</p></div>
<div class="m2"><p>ترا ولی نبود مرد اختر وارون</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگرچه عالم مامور بود مامون را</p></div>
<div class="m2"><p>تراست بر در مامور مهتر از مامون</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نکو خصال و نکوحال امیر شمس الدین</p></div>
<div class="m2"><p>که کمترینش عطا هست بار صد گردون</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>باوالمعالی عالم نمای و عالم رای</p></div>
<div class="m2"><p>که هست همت عالیش برتر از گردون</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بسا مغاک کز او راست گشته با پشته</p></div>
<div class="m2"><p>بسا حصار گز او راست گشته با هامون</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هر آن هنر که ز رستم همی دهند خبر</p></div>
<div class="m2"><p>از او همی بعیان یافتن توان اکنون</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بروز بخشش قارون از او شود درویش</p></div>
<div class="m2"><p>بروز رامش شادان از او شود محزون</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز بانک سائل شادان شود روانش چنانکه</p></div>
<div class="m2"><p>ز بانگ لیلی خرم شود دل مجنون</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نداد و هم ندهد هیچ خلق را تیمار</p></div>
<div class="m2"><p>نکرد و هم نکند هیچ خلق را معجون</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بروز بزم چو یوسف بود فراز سریر</p></div>
<div class="m2"><p>بروز رزم چو رستم بود فراز هیون</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زمین ز جود کف او میان زر پنهان</p></div>
<div class="m2"><p>هوا ز خلق خوش او بغالیه معجون</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شود چو افیون بر دشمنان او شکر</p></div>
<div class="m2"><p>شود چو شکر بر دوستان او افیون</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همیشه تا نکند با فنا بقا پیوند</p></div>
<div class="m2"><p>همیشه تا نبود فتنه با خرد مقرون</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بقا و دولت با هر دو میر مقرون باد</p></div>
<div class="m2"><p>بر این سعادت عاشق بر آن ظفر مفتون</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>فزون طربشان هر روز و بختشان فیروز</p></div>
<div class="m2"><p>خجسته عید بر ایشان خجسته و میمون</p></div></div>