---
title: >-
    شمارهٔ ۹۲ - در مدح ابونصر مملان
---
# شمارهٔ ۹۲ - در مدح ابونصر مملان

<div class="b" id="bn1"><div class="m1"><p>یاقوت سرخ شد ز می از ابر در بار</p></div>
<div class="m2"><p>شاخ درخت دارد یاقوت و در بار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بربط نواخته و چنگ ساخته</p></div>
<div class="m2"><p>قمری و فاخته بخروشند بر چنار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل بر زمین بخندد مانند روی دوست</p></div>
<div class="m2"><p>ابر از هوا بگرید چون چشم من بزار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوئی مشاطه گشت بباغ اندرون صبا</p></div>
<div class="m2"><p>کز فعل او شدند درختان عروس وار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این را حریر پیرهن و حله روی بند</p></div>
<div class="m2"><p>آن را عقیق مخنقه و زر گوشوار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون ابر جای جای بمانده بر آسمان</p></div>
<div class="m2"><p>برفست جای جای بر اطراف کوهسار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گردون چو چادریست مهش تار و میغ پود</p></div>
<div class="m2"><p>هامون چو مطردیست گلش پود و سبزه تار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لاله شکفته سرخ و سیاهیش در میان</p></div>
<div class="m2"><p>نرگس شکفته زرد و سپیدیش در کنار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این چون درون ساغر سیمین نبید زرد</p></div>
<div class="m2"><p>آن چون میان آتش رخشنده دود تار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بلبل گهی بگرید و گه ناله سر کند</p></div>
<div class="m2"><p>این از نشاط گل کند آن از فراق یار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سیمین شد از شکوفه همه باغ و بوستان</p></div>
<div class="m2"><p>مشگین شد از بنفشه همه جوی و جویبار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زیر درخت پیش فکنده بنفشه سر</p></div>
<div class="m2"><p>چون پیش داور اندر مرد گناه کار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون در ریخته ز بر پرنیان سبز</p></div>
<div class="m2"><p>بر سبزه اوفتاده شکوفه ز میوه دار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وان صدهزار لاله شکفته میان کشت</p></div>
<div class="m2"><p>گوئی میان دریا شمع است صد هزار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر برگ لاله قطره باران نگاه کن</p></div>
<div class="m2"><p>چون بر عقیق ریخته لؤلؤی شاهوار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون از بر تذروان پرواز کرده باز</p></div>
<div class="m2"><p>ابر ایستاده از بر گلزار و لاله زار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بیرون رو از حصار و بصحرا فرو نشین</p></div>
<div class="m2"><p>می خور سحر که لاله برون آمد از حصار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بین بر زمین گروه غزال از پی گروه</p></div>
<div class="m2"><p>بین بر هوا قطار کلنک از پس قطار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>این را ز بیم یوز بسبزه درون مقام</p></div>
<div class="m2"><p>وان را ز هول باز بآب اندرون قرار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ما را شتاب کرده دل از آرزوی صید</p></div>
<div class="m2"><p>جای قرار کرده از بویه نگار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خر خیز وار گشته که از گونه گونه رنگ</p></div>
<div class="m2"><p>فرخار وار شد چمن از گونه گون نگار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آن شنبلید کفته چو رخسار دردمند</p></div>
<div class="m2"><p>آن ارغوان شکفته چو رخسار شادخوار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آن پیش سرو بید خمیده بروز باد</p></div>
<div class="m2"><p>چون پیش شهریار بزرگان روزگار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>میر بزرگوار ابونصر کز ملوک</p></div>
<div class="m2"><p>چون او نیافریده خدای بزرگوار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>رادی و راستیش برآورده زیر دست</p></div>
<div class="m2"><p>مردی و مردمیش بپرورده بر کنار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا یک عدو بود نبود جز دغاش فعل</p></div>
<div class="m2"><p>تا یکدرم بود نبود جز سخاش کار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گردون بود بنزد دل او چو پای مور</p></div>
<div class="m2"><p>دریا بود بنزد کف او چو چشم مار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هرگز ز چار طبع نیاید چنو پدید</p></div>
<div class="m2"><p>چونان که هیچ طبع نیفزاید از چهار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گوئی صراط مال جهان کف راد اوست</p></div>
<div class="m2"><p>کش سوی هیچکس نبود جز بدو گذار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گردون بدور او نکند هیچ بند و رنگ</p></div>
<div class="m2"><p>گیتی بدور او نکند هیچ مکر و چار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>او هست سرفراز و همه خلق پی سپر</p></div>
<div class="m2"><p>او هست پیشگاه و همه خلق پیشکار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گر برگ بخت خواهی کار و فاش کن</p></div>
<div class="m2"><p>ور بار سعد خواهی تخم هواش کار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از بهر خواستار کند گرد خواسته</p></div>
<div class="m2"><p>وز بهر آن شدند بزرگانش خواستار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بی عیب و بی عوار بود جاودان چو او</p></div>
<div class="m2"><p>آن زر فضل را که سعادت نکرد عار؟</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر آب را جدا کند ازیم کسی بطبع</p></div>
<div class="m2"><p>یا هیچ کس بر آب پدید آورد نگار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گردد جدا ز رادی آن میر تاجور</p></div>
<div class="m2"><p>آید پدید آرزوی میر نامدار؟</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ای آنکه چون تو در همه گیتی سوار نیست</p></div>
<div class="m2"><p>هم بر سخا سواری و هم بر سخن سوار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چون تو جوان ندیدم با طبع و باهنر</p></div>
<div class="m2"><p>چون تو سخی ندیدم بی کبر و بردبار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>فخر آورد بطالع مولود تو فلک</p></div>
<div class="m2"><p>کسر آورد بوعده عمر تو روزگار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>زائر نماند جود تو نادیده چند ره</p></div>
<div class="m2"><p>دشمن نماند هول تو ناخورده چند بار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ای با ولی برادی سازنده تر ز آب</p></div>
<div class="m2"><p>ای با عدو بمردی سوزنده تر ز نار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هم دوستدار خویش بود دوستدار تو</p></div>
<div class="m2"><p>کز دوستیت راست شود کار دوستدار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تا من بدوستیت بیاراستم روان</p></div>
<div class="m2"><p>بر من بفر دولت تو راست گشت کار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>اینم همی درم دهد و آن کند محل</p></div>
<div class="m2"><p>آنم همی گهر دهد و این کند وقار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>آنجا که هیچگونه ندارد دلم امید</p></div>
<div class="m2"><p>چون نیکوئی ببخت تو گردد امیدوار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>تا نار کفته باشد بر شاخ در خزان</p></div>
<div class="m2"><p>تا گل شگفته باشد در باغ در بهار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>خندان لب تو باد بسان شگفته گل</p></div>
<div class="m2"><p>چشم عدوت باد بسان کفیده نار</p></div></div>