---
title: >-
    شمارهٔ ۱۶۹ - در مدح امیر ابوالقاسم عبدالله بن وهسودان
---
# شمارهٔ ۱۶۹ - در مدح امیر ابوالقاسم عبدالله بن وهسودان

<div class="b" id="bn1"><div class="m1"><p>گل چو بشکفت زمین گشت پر از آب روان</p></div>
<div class="m2"><p>بگل و آب روان تازه بود جان جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکجا چشم زنی هست زمین نرگس زار</p></div>
<div class="m2"><p>هرکجا پای نهی هست زمین لاله ستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سبزه را باد پر از عنبر کرده است کنار</p></div>
<div class="m2"><p>لاله را ابر پر از لؤلؤ کرده است دهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از هوا در فکند سوی زمین ابر بلند</p></div>
<div class="m2"><p>از زمین مشگ برد سوی هوا باد وزان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا زره پوش شد از باد وزان آب شمر</p></div>
<div class="m2"><p>گل نشکفته چو گوی آمد و گلبن چوگان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا زمین گنج گل و کان سمن کرد پدید</p></div>
<div class="m2"><p>فاخته مست شد و راز دلش کرد عیان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه رازی که نهان بود پدیدار شده است</p></div>
<div class="m2"><p>سزد ار عاشق مسگین نکند راز نهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گل صد برگ بخنده بگشاده است دهن</p></div>
<div class="m2"><p>بلبل مست بناله بگشاده است زبان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جان میخواران شادی کند از خنده این</p></div>
<div class="m2"><p>جان غمخواران شادی کند از گریه آن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از هوا ابر همی خواند فریاد و نفیر</p></div>
<div class="m2"><p>در زمین کبک همی دارد فریاد و فغان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باغ رنگین شده گوئی که بر او کرده گذر</p></div>
<div class="m2"><p>میر ابوالقاسم عبداله بن و هسودان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن جوانی که بدو بخت معادی شده پیر</p></div>
<div class="m2"><p>تیز هوشی که بدو بخت ولی گشت جوان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آنکه رادی را بسته است همه ساله کمر</p></div>
<div class="m2"><p>وآنکه مردی را بسته است همه ساله میان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن همه روز گشاده ز پی زائر دست</p></div>
<div class="m2"><p>آن همه وقت نهاده ز پی مهمان خوان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آنکه بگشاید بر جان معادیش کمین</p></div>
<div class="m2"><p>چون گه جنگ خدنگی بگشاید ز کمان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دل یاران و عدیلان بگشاید بسخا</p></div>
<div class="m2"><p>دل میران و بزرگان بگشاید بزبان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای ز رادیت شده خیره کریمان زمین</p></div>
<div class="m2"><p>وی ز مردیت شده طیره سواران زمان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون یکی ساعت در بزم گرفتی تو مقام</p></div>
<div class="m2"><p>چون یکی ساعت در رزم گرفتی تو مکان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>درم از دست تو فریاد کند اندر گنج</p></div>
<div class="m2"><p>آهن از تیغ تو فریاد کند اندر کان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دشمنان تو همه پاک نوانند و نژند</p></div>
<div class="m2"><p>حاسدان تو همه پاک نژندند و نوان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شود آسوده ز تیمار بگفتار تو دل</p></div>
<div class="m2"><p>شود آزاد ز اندیشه بدیدار تو جان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بهمه گیتی چون تو نبود نیکو دین</p></div>
<div class="m2"><p>بهمه عالم چون تو نبود نیکودان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تن بدخواه تو همواره بود جفت گزند</p></div>
<div class="m2"><p>دل بدگوی تو پیوسته بود جفت زیان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>با کرمهای تو هرگز نبود جای مگر</p></div>
<div class="m2"><p>با عطاهای تو هرگز نبود جای گمان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نوبهار آمد و نوروز نو آورد نشاط</p></div>
<div class="m2"><p>ز مهی چون بت نوشاد می سرخ ستان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا بجایست زمین با طرب و شادی زی</p></div>
<div class="m2"><p>تا بپایست فلک با خوشی و رامش مان</p></div></div>