---
title: >-
    شمارهٔ ۱۲۵ - در مدح ابوالیسر
---
# شمارهٔ ۱۲۵ - در مدح ابوالیسر

<div class="b" id="bn1"><div class="m1"><p>ربود جان و دل من بزلف غالیه فام</p></div>
<div class="m2"><p>بتی که بوی دهد زلف او بغالیه وام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همی رباید صبرم بزلف غالیه بوی</p></div>
<div class="m2"><p>همی فزاید عشقم بجعد غالیه فام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی نه شست و گل سرخ را گرفته بشست</p></div>
<div class="m2"><p>یکی نه دام و مه تام را گرفته بدام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که دید توده شود مشگناب بر گل سرخ</p></div>
<div class="m2"><p>که دید حلقه شود عود خام بر مه تام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آن دو کژدم بر دل نهاده دائم سر</p></div>
<div class="m2"><p>از آن دوزنگی بر کف گرفته دائم جام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندیده زهر نژندم چو زهر دیده سقیم</p></div>
<div class="m2"><p>نخورده باده نوانم چو باده خورده مدام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدان دو مشگ سیه دام کرده سیم سپید</p></div>
<div class="m2"><p>دلم ببست بدام آن نگار سیم اندام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلم همیشه ز بادام او فتاده برنج</p></div>
<div class="m2"><p>لبم همیشه ز یاقوت او رسیده بکام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روان من همه ساله بشادی از یاقوت</p></div>
<div class="m2"><p>زبان من همه ساله بزاری از بادام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو آفتاب درخشان شود ز گوشه چرخ</p></div>
<div class="m2"><p>بعام گوید خاص و بخاص گوید عام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که آن نگار کجا رفت و آفتاب کجا</p></div>
<div class="m2"><p>کدام بود رخ او و آفتاب کدام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گرفت جان و دل من غمام حسرت و غم</p></div>
<div class="m2"><p>از آن ز مشگ سیه کرده آفتاب غمام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>غمام غم ز دل و جان من جدا نکند</p></div>
<div class="m2"><p>جز آفتاب عطاهای آفتاب کرام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پناه دانش و بیناد دین ابوالیسر آن</p></div>
<div class="m2"><p>که اختیار کرام است و اختیار انام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ثبات ملک و بد و بخت ما گرفته ثبات</p></div>
<div class="m2"><p>قوام خیل و بدو بخت ما گرفته قوام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همیشه خنجر او را ز خون شیر شراب</p></div>
<div class="m2"><p>همیشه نیزه او را ز مغز ببر طعام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کسیکه روزی بر وی کند سلام بطبع</p></div>
<div class="m2"><p>سلامت دو جهانش دهد جواب سلام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اگر سعادت خواهی که با تو بنشیند</p></div>
<div class="m2"><p>بمجلسش بنشین و بدرگهش بخرام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همیشه پیشه او خوردن است و بخشیدن</p></div>
<div class="m2"><p>بود گشاده دل و دست او بهر هنگام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همه ببخشد امروز و ننگرد فردا</p></div>
<div class="m2"><p>مگر نداند کآغاز را بود انجام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همه متابع مالند و او متابع فضل</p></div>
<div class="m2"><p>همه حریض بنانند و او حریص بنام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ایا همیشه سخا را بکف راد مکان</p></div>
<div class="m2"><p>ایا همیشه وغا را بتیغ تیز مقام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کسیکه یافته باشد بروز رزم تو رنج</p></div>
<div class="m2"><p>کسیکه یافته باشد بروز بزم تو کام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از آن جدا نشود رنج تا بروز فنا</p></div>
<div class="m2"><p>وزین بری نشود کام تا بروز قیام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اگرچه حکم زمانه رواست بر همه خلق</p></div>
<div class="m2"><p>رواست بر همه احکام او ترا احکام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز ما سئوال بود نزد تو همیشه رسول</p></div>
<div class="m2"><p>ز ما مدیح بود نزد تو همیشه پیام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>رسول تو بر ما رزمه باشد و بدره</p></div>
<div class="m2"><p>پیام تو بر ما اسب باشد و استام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز فضل بر در تو سال و ماه باشد حشر</p></div>
<div class="m2"><p>ز جود بر در گنج تو ماه و سال خیام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کسیکه تیغ تو او را دهد بحر نوید</p></div>
<div class="m2"><p>قضا بیاید و او را دهد بمرگ پیام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ایا کشیده بتایید تو سپهر سپاه</p></div>
<div class="m2"><p>و یا سپرده بفرمان تو زمانه زمام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همی گشاده کنی کار کهتران بسخا</p></div>
<div class="m2"><p>همی ز دوده کنی رای مهتران بکلام</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همیشه نیست بیکحال گردش گردون</p></div>
<div class="m2"><p>همیشه نیست بیک روی گردش ایام</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گهی ز غار بخاره کند ز خاره بغار</p></div>
<div class="m2"><p>گهی ز بام بخانه گهی ز خانه ببام</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز گشت بخت جهان حال من شده است تباه</p></div>
<div class="m2"><p>ز نحس دور فلک روز من شده است چو شام</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سخای تو کند امروز کار من بنوا</p></div>
<div class="m2"><p>عطای تو کند امروز شغل من بنظام</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همیشه تا نبرد کس ز شام شام بمصر</p></div>
<div class="m2"><p>همیشه تا نبرد کس ز مصر چاشت بشام</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز شام رنج مماناد ناصح تو بچاشت</p></div>
<div class="m2"><p>ز چاشت باز مماناد حاسد تو بشام</p></div></div>