---
title: >-
    شمارهٔ ۱۴۵ - در مدح ابومنصور وهسودان
---
# شمارهٔ ۱۴۵ - در مدح ابومنصور وهسودان

<div class="b" id="bn1"><div class="m1"><p>بتی چون رامش اندر می مهی چون دانش اندر جان</p></div>
<div class="m2"><p>بلای دل بدو سنبل شفای جان بدو مرجان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز عنبر بر مهش چنبر ز سنبل بر گلش چوگان</p></div>
<div class="m2"><p>دلش چون قبله تازی رخش چون قبله دهقان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو چشمش مایه درد است و دو لب مایه درمان</p></div>
<div class="m2"><p>دو زلفش مایه کفر است و دو رخ مایه ایمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر با من بخندیدی نبودی چشم من گریان</p></div>
<div class="m2"><p>ور از من رخ نپوشیدی نبودی راز من عریان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترا دو زلف مشک افشان بر آن دو عارض رخشان</p></div>
<div class="m2"><p>مرا بر دو رخ زرین دو دیده هست درافشان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لب و دندانش چون مرجان چکیده بر گل خندان</p></div>
<div class="m2"><p>بدندان مانده انگشتم ز عشق آن لب و دندان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ببالا سرو بستانی شکفته بر سرش بستان</p></div>
<div class="m2"><p>اگر دائم بقا خواهی از آن بستان گلی بستان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ببین دو زلف پرتابش بران دو عارض تابان</p></div>
<div class="m2"><p>بکردار کف موسی بدو پیچیده بر ثعبان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ایا نقشی که چون رویت نباشد نقش بر ایوان</p></div>
<div class="m2"><p>بدو رخ چشمه مهری بدو لب چشمه حیوان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل از گفتار تو غمگین تن از رفتار تو بیجان</p></div>
<div class="m2"><p>خیال روی و مویت را شمن گردد بت کاشان کذا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز عشقت بس زیان دارم ولیکن بس مرا سود آن</p></div>
<div class="m2"><p>که دیدم روی شاهنشه ابومنصور وهسودان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خدای ما که پیدا کرد از ناچیز انس و جان</p></div>
<div class="m2"><p>ز بهر انس و جان او را پدید آورد انس جان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چنان گردن کشی گردون برون نارد بصد دوران</p></div>
<div class="m2"><p>نه از روم و نه از تازی نه از ایران نه از توران</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگرچه نیک و بد باشد ز گشت کنبد گردان</p></div>
<div class="m2"><p>تو خیر و شر و نیک و بد ز کلک و خنجر اودان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر شیطان کند مدحش شود همچون ملک شیطان</p></div>
<div class="m2"><p>عدو زو پست تا ماهی ولی زور است تا سرطان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو خورشید است جود او به بر و بحر بی پایان</p></div>
<div class="m2"><p>که باشد بر که و بر مه فروغ روی او تابان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بزیر رانش اندر اسب چون باد وزان پران</p></div>
<div class="m2"><p>بجز او هیچ کس را بوده هرگز باد زیر ران</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>میان مدح نام او بسان سجده در فرقان</p></div>
<div class="m2"><p>بیاد او ولی تازه عدو از غم بود پژمان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بروز بزم چون دارا بروز رزم چون ماکان</p></div>
<div class="m2"><p>بمهر او ولی باقی ز کین او عدو ماکان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز تیغ و کف او خیزد ز خون و خواسته طوفان</p></div>
<div class="m2"><p>موافقرا دهد بار این مخالف را دهد باران</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اگر یک شاعری یابد ز کافی کف او احسان</p></div>
<div class="m2"><p>چنان گردد که از اقبال برتر باشد از احسان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همه دشوارهای چرخ نزدیک ولیش آسان</p></div>
<div class="m2"><p>سپهر از تیغ او خائف جهان از تیر او ترسان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بروز خشم چون دوزخ بروز مهر چون ریحان</p></div>
<div class="m2"><p>بدین ریحان کند آتش بدان آتش کند ریحان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدی را خنجر وی گنج (کذا) و نیکی را کف او کان</p></div>
<div class="m2"><p>ولی را بهره زین گوهر عدو را بهره زان نیران</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سخای او گه مجلس وغای او گه جولان</p></div>
<div class="m2"><p>موالیرا دهد نصرت معادی را دهد خذلان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بگاه رزم چون رستم بگاه بزم چون دستان</p></div>
<div class="m2"><p>جدا گفتارش از تنبل بری کردارش از دستان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ایا دعوی رادی را دو کف راد تو برهان</p></div>
<div class="m2"><p>سخا از کف تو پیدا و جور از عدل تو پنهان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر هنگام کوشیدن به پیش آید جهانی جان</p></div>
<div class="m2"><p>بدشت اندر جهانی جان نهی مر کرکسانرا خوان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز گردون برترت منظر ز کیوان برترت ایوان</p></div>
<div class="m2"><p>به منظر نایدت گردون بایوان نرسدت کیوان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>برزم اندر چو نعمانی ببزم اندر چو نوشیروان</p></div>
<div class="m2"><p>که را دربان تو باشد سزد دربان او خاقان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>الا تا از مه تابان بفرساید همی کتان</p></div>
<div class="m2"><p>الا تا از مه آبان بیفزاید همی بستان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بداندیش تو کتان باد و تیغ تو مه تابان</p></div>
<div class="m2"><p>نکوخواه تو بستان باد و دست تو مه آبان</p></div></div>