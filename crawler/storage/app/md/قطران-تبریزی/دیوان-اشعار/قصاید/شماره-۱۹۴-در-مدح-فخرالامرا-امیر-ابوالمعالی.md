---
title: >-
    شمارهٔ ۱۹۴ - در مدح فخرالامراء امیر ابوالمعالی
---
# شمارهٔ ۱۹۴ - در مدح فخرالامراء امیر ابوالمعالی

<div class="b" id="bn1"><div class="m1"><p>ای ماه شبه زلف مشک خالی</p></div>
<div class="m2"><p>خالی نشود جانم از تو حالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کندن نتوان نقش مهرت از دل</p></div>
<div class="m2"><p>گوئی که بدل بر نشان خالی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناهید بدت خال و مشتری عم</p></div>
<div class="m2"><p>لیکن حسد عم و رشک خالی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل را بدو زلفین مدام دامی</p></div>
<div class="m2"><p>جان را بدو خال سیاه حالی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از عنبر جعد و زلف مشکین</p></div>
<div class="m2"><p>بر ماه دو هفته غالیه چو ماهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ماهی و جز اندر روان نتابی</p></div>
<div class="m2"><p>سروی و جز اندر روان نبالی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نالی کند از ناله قد سروی</p></div>
<div class="m2"><p>زان قد چو سرو میان نالی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زلفانت بکردار دال کرده</p></div>
<div class="m2"><p>ابدال ز عشق تو پشت دالی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای نرگس تو جایگاه نیرنگ</p></div>
<div class="m2"><p>ای لاله تو معدن لئالی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل را بیکی روز و شب نشاطی</p></div>
<div class="m2"><p>جان را بیکی سال و مه و بالی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مانی کف و تیغ تاج ملکت را</p></div>
<div class="m2"><p>فخرالامراء و میر ابوالمعالی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای تیغ تو فرسایش معادی</p></div>
<div class="m2"><p>ای دست تو آسایش موالی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جز همت عالی بند علی را</p></div>
<div class="m2"><p>خلقی بوی اندر شدند غالی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>غالی شدن اندر تو بیش باید</p></div>
<div class="m2"><p>کت همت عالی است دست عالی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون ایزد با قدرت و محلی</p></div>
<div class="m2"><p>چون پیغمبر بی کبر و بی همالی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شاهان چو نهالند و تو بیخی</p></div>
<div class="m2"><p>سالاران بر گند و تو نهالی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جان را بسخاوت نشاط و نازی</p></div>
<div class="m2"><p>دل را بنوازش قرار و هالی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>با تیغ بمیدان هلاک خصمی</p></div>
<div class="m2"><p>با جام بمجلس دمار مالی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در مجلس و میدان بوی تو دائم</p></div>
<div class="m2"><p>از بسکه دهی مال و خصم مالی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ماران جهان پیش تو چون موری</p></div>
<div class="m2"><p>شیران جهان پیش تو شگالی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جان و تن تاریک را چراغی</p></div>
<div class="m2"><p>جان و دل پر زنگ را صقالی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در رای و مردی تو بی نظیری</p></div>
<div class="m2"><p>در جود و سخاوت بی همالی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شیری بگه رزم بر جنیبی</p></div>
<div class="m2"><p>باری بگه بزم بر نهالی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ماننده خورشید بی عدیلی</p></div>
<div class="m2"><p>ماننده جمیشد با جمالی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آن را که نباشد ترا ستودن</p></div>
<div class="m2"><p>گویا باشد زبان لالی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از مهر تو یابند نیک بختی</p></div>
<div class="m2"><p>وز مدح تو یابند نیک فالی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>امید ملکتی و پشت خلقی</p></div>
<div class="m2"><p>خورشید تباری و ماه آلی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو یار همال و وفا و جودی</p></div>
<div class="m2"><p>وز همتای یار بی همالی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خوانند مرا بیکران امیران</p></div>
<div class="m2"><p>با رای بلند و نکو نوالی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نام تو کشیدم بدین نواحی</p></div>
<div class="m2"><p>جود تو فکندم بدین حوالی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا بد نفزاید ز نیکنامی</p></div>
<div class="m2"><p>تا نیک نیاید ز بدسگالی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همواره ترا فعل نیک بادا</p></div>
<div class="m2"><p>بادات عدو جفت بد فعالی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پیوسته بزی بکام دل شاها</p></div>
<div class="m2"><p>ایمن به پناه ذوالجلالی</p></div></div>