---
title: >-
    شمارهٔ ۷۱ - در مدح شاه ابومنصور
---
# شمارهٔ ۷۱ - در مدح شاه ابومنصور

<div class="b" id="bn1"><div class="m1"><p>چون رخ معشوق خندان شد بصحرا لاله زار</p></div>
<div class="m2"><p>ابر نیسانی همی گرید ز عشق لاله زار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از نسیم باد خارستان همه شد گلستان</p></div>
<div class="m2"><p>وز سرشگ ابر شورستان همه شد لاله زار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باغ شد خرخیز بوی و راغ شد فر خار نقش</p></div>
<div class="m2"><p>کوه شد گردون نهاد و دشت شد فردوس وار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو چشم نیکوان نرگس نماید در چمن</p></div>
<div class="m2"><p>همچو جسم عاشقان شد خیزران زار و نزار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باز نشناسی سحرگه کوهسار از آسمان</p></div>
<div class="m2"><p>باز نشناسی شبانگه آسمان از کوهسار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لاله چون ناری که باشد دودش اندر زیر نور</p></div>
<div class="m2"><p>گرچه باشد زیر دود اندر همیشه نور نار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بانگ بلبل چون عتاب بیدلان اندر نبید</p></div>
<div class="m2"><p>گونه گل چون رخان دلبران اندر خمار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باد بگشاید ز روزی نرگس و نسرین نقاب</p></div>
<div class="m2"><p>ابر بزداید ز روی سوسن و خیری غبار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بلبل و صلصل سرایان سرکش آئین در چمن</p></div>
<div class="m2"><p>سار و قمری باربد کردار نالان بر چنار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دشتها زنگارگون و کوهها شنگرف رنگ</p></div>
<div class="m2"><p>مرزها پیروزه پوش و شاخها بیجاده بار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صابری گردد نهار و عاشقی گردد فزون</p></div>
<div class="m2"><p>تا نهار اندر فزونی رفت و لیل اندر نهار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دوست یاد آرد ز بانک فاخته آوای دوست</p></div>
<div class="m2"><p>یار یاد آرد ز بانگ ارغنون آواز یار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون بساط خسروانست از طرائف بوستان</p></div>
<div class="m2"><p>چون درفش کاویان است از جواهر میوه دار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سیم پوش روز بار است از شکوفه باغها</p></div>
<div class="m2"><p>مشگبوی و مشگ رنگ است از بنفشه جویبار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از نسیم باد پر مشگست خاک غار و کوه</p></div>
<div class="m2"><p>از فروغ لاله پر خونست سنگ کوه و غار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>این چو مجلسگاه صاحب روز جشن و خرمی</p></div>
<div class="m2"><p>و آن چو لشکرگاه صاحب روز جنگ و کارزار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آفتاب جود ابومنصور کو دارد جهان</p></div>
<div class="m2"><p>بر موالی چون بهشت و بر معادی چون حصار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نیست جودش را شمار و نیست لطفش را کران</p></div>
<div class="m2"><p>ملک بادش بیکران و عمر بادش بیشمار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مهر و ناز او ز ماه رنج بزداید خسوف</p></div>
<div class="m2"><p>آب جود او ز دشت آز بنشاند غبار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کردگارش ناصر است و روزگارش یاور است</p></div>
<div class="m2"><p>آسمانش چاکر است و آفتابش پیشکار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کار مردی جز بطبع او نگیرد انتظام</p></div>
<div class="m2"><p>بند رادی جز بدست او نگردد استوار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شاعران از هر زمینی نزد او گشته گروه</p></div>
<div class="m2"><p>زائران از هر دیاری نزد او بسته قطار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گمرهان جهل را دائم دل پاکش دلیل</p></div>
<div class="m2"><p>بستگان آز را دائم کف رادش زوار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کردگار او را بنور خود پدید آورد باز</p></div>
<div class="m2"><p>کرد دین و دانش و جود و وفا بر وی نثار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خانه بخشیدنش را عقل بوم و فضل بام</p></div>
<div class="m2"><p>جامه پوشیدنش را خیر پود و فخر تار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در هزاران وعد او هرگز نبینی یک خلاف</p></div>
<div class="m2"><p>در هزاران جود او یکره نبینی انتظار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای شکار زائران پیوسته زر و سیم تو</p></div>
<div class="m2"><p>وی روان دشمنان همواره تیغتر اشکار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چاره بیچارگان و یاور درماندگان</p></div>
<div class="m2"><p>سائلان را دست گیر و غمکنان را غمگسار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هرکجا گیری قرار آنجا سخا گیرد مقام</p></div>
<div class="m2"><p>هرکجا گیری مقام آنجا وفا گیرد قرار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ای بدانش رهنمایان سخن را رهنما</p></div>
<div class="m2"><p>وی زرادی خواستاران درم را خواستار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هرکجا من بوده ام مدح توام بوده است شغل</p></div>
<div class="m2"><p>هرکجا من بوده ام شکر توام بوده است کار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سال و مه از حسرت نادیدن دیدار تو</p></div>
<div class="m2"><p>بود جان من نژند و بود جسم من فکار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>صد هزاران شکر بادا کردگار عرشرا</p></div>
<div class="m2"><p>چون بمن بنمود چهر تو بشادی کردگار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تا نگردد مور مار از گشت سعد آسمان</p></div>
<div class="m2"><p>تا نگردد مار مور از گشت نحس روزگار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بر تن خصمان تو بادا بسان مار مور</p></div>
<div class="m2"><p>بر دل یاران تو بادا بسان مور مار</p></div></div>