---
title: >-
    شمارهٔ ۱۱۸ - در مدح ابوالیسر
---
# شمارهٔ ۱۱۸ - در مدح ابوالیسر

<div class="b" id="bn1"><div class="m1"><p>خیال شام فراق بتان بروز وصال</p></div>
<div class="m2"><p>مرا گداخته دارد ز غم بسان هلال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن نهیب نماند بچشمم اندر خواب</p></div>
<div class="m2"><p>وزین عذاب نماند بجسمم اندر هال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فروغ ماه نبینم همی ز بیم خسوف</p></div>
<div class="m2"><p>شعاع مهر نیابم همی ز بیم زوال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حلال کردم بر خویشتن فراق حرام</p></div>
<div class="m2"><p>حرام کردم بر خویشتن وصال حلال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که در وصال بود انده از نهیب فراق</p></div>
<div class="m2"><p>که در فراق بود شادی از امید وصال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بسکه مویم گشتم بسان تافته موی</p></div>
<div class="m2"><p>ز بسکه نالم گشتم بسان سوخته نال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا همه کس گویند خیر خیر مموی</p></div>
<div class="m2"><p>مرا همه کس گویند خیر خیر منال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه آگهند که من چون همیگذارم روز</p></div>
<div class="m2"><p>نه آگهند که من چون همی گذارم سال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رفیق رفته و دل با هواش گشته رفیق</p></div>
<div class="m2"><p>همال رفته و تن با بلاش گشته همال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه روی اینجا بودن نه پای رفتن بر</p></div>
<div class="m2"><p>نه رای بر یکروی و نه کار بر یک حال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برفتن اندر دلرا نهیب دوری دوست</p></div>
<div class="m2"><p>به بودن اندر تنرا عذاب تنگی بال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدوست باشد دل را همیشه صبر و شکیب</p></div>
<div class="m2"><p>بمال باشد تن را همیشه جاه و جلال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر آنزمان که من آهنگ راه خواهم کرد</p></div>
<div class="m2"><p>بسوی من دود آن ماه روی مشگین خال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گشاده شکر شنگرف رنگرا بعتاب</p></div>
<div class="m2"><p>نهاده نرگس نیرنگ ساز را بجدال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گهیش لاله عیان کرده در میان عقیق</p></div>
<div class="m2"><p>گهی عقیق نهان کرده در میان لآل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ستاره پوش مه از سیل قیرگون بادام</p></div>
<div class="m2"><p>بنفشه رنگ گل از زخم سیمگون چنگال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرا بخوشی گوید که تا کی این رفتار</p></div>
<div class="m2"><p>مرا بکشی گوید که تا کی این احوال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دلت خلاف زبان و زبان خلاف دلت</p></div>
<div class="m2"><p>بدان امید پذیر و بدین فریب سگال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>روا بود ز پس دوستی و نزدیکی</p></div>
<div class="m2"><p>ز دوستان و رفیقان ترا گرفته ملال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگرچه آب زلالست زندگانی خلق</p></div>
<div class="m2"><p>بسی چو ماند چون زهر گردد آب زلال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وگر ز تنگی مالست رفتن تو مرو</p></div>
<div class="m2"><p>که من ترا برسانم بگونه گون اموال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همت بچهره توانگر کنم بزر عیار</p></div>
<div class="m2"><p>همت بدیده توانگر کنم بسیم حلال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دلم بسوزد و گویم بآن بهشتی روی</p></div>
<div class="m2"><p>که در نگار تذرو است و در خرام غزال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که شاد کن دل خرسند و خوار و زار مکن</p></div>
<div class="m2"><p>بر این نهادم گوش و از آن کشیدم یال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مرا بکار نه مال آید و نه سیم و نه زر</p></div>
<div class="m2"><p>بد آنکه هست فزون زر و سیم وافر مال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گمان بری تو که بی مال باشد آنکه کند</p></div>
<div class="m2"><p>همیشه خدمت استاد راد اعداد مال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چراغ دانش خورشید دین ابوالیسر آنکه</p></div>
<div class="m2"><p>بدست هست درافشان بکلک در اقبال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر کنند بصدر اندرش سئوال بعلم</p></div>
<div class="m2"><p>وگر کنند ببزم اندرش سئوال بمال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دهد بسائل پرسنده ز آن هزار جواب</p></div>
<div class="m2"><p>دهد بسائل خواهنده زین هزار جوال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بنوک تیر فرود آورد ز کوه پلنگ</p></div>
<div class="m2"><p>بنوک نیزه برون آورد ز دریا بال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز بسکه خواسته ناخواسته همی بخشد</p></div>
<div class="m2"><p>کسی نبیند اندر زبان خلق سئوال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اگر علی بگه جنگ همچو او بوده است</p></div>
<div class="m2"><p>بهیچ روی نکوهیده نیست مذهب غال</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دو کف اوست گه بزم مایه امید</p></div>
<div class="m2"><p>سرای اوست گه بار قبله اقبال</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ببحر مردی در تیغ او فشانده گهر</p></div>
<div class="m2"><p>بباغ رادی در کف او نشانده نهال</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سنان روشن او در دل سیاه عدو</p></div>
<div class="m2"><p>بود چو آتش افروخته میان زگال</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ایا سخای تو داده بمهر فضل فروغ</p></div>
<div class="m2"><p>ایا عطای تو داده بتیغ علم صقال</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اگر بدیدی حاتم ترا بروز سخا</p></div>
<div class="m2"><p>وگر بدیدی رستم ترا بروز قتال</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز جود نام نبردی هگر ز حاتم طی</p></div>
<div class="m2"><p>ز حرب نام نجستی هگر ز رستم زال</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اگر بدست تو آید چو مال آب بحار</p></div>
<div class="m2"><p>وگر بروی تو آید چو خصم سنگ جبال</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نه زین بماند با بخشش تو یکقطره</p></div>
<div class="m2"><p>نه ز آن بماند با کوشش تو یک مثقال</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بود ثنای تو گفتن نشان فرخ روز</p></div>
<div class="m2"><p>بود رضای تو جستن نشان فرخ فال</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>همیشه بادت ملک و همیشه بادت عز</p></div>
<div class="m2"><p>دلت عدیل نشاط و کفت قرین نوال</p></div></div>