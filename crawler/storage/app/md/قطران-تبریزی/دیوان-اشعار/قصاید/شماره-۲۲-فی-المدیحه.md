---
title: >-
    شمارهٔ ۲۲ - فی المدیحه
---
# شمارهٔ ۲۲ - فی المدیحه

<div class="b" id="bn1"><div class="m1"><p>ای میر جهانگیر چو تو دادگری نیست</p></div>
<div class="m2"><p>چون تو بگه کوشش و بخشش دگری نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناداده ترا گردش گردون شرفی نیست</p></div>
<div class="m2"><p>نسپرده ترا طایر میمون هنری نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر روی زمین رزمگهی نیست که تا حشر</p></div>
<div class="m2"><p>از سم سمند تو بر او بر اثری نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناداده کف راد تو صد بار بمردم</p></div>
<div class="m2"><p>در کنج ملوکان زمانه گهری نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی رخنه گرز تو بحصنی بدنی نه</p></div>
<div class="m2"><p>ناسفته ز تیر تو بحصنی سپری نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی شکر تو در دهر گشاده دهنی نه</p></div>
<div class="m2"><p>بی امر تو در گیتی بسته کمری نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مانند تو در مجلس دینار دهی نیست</p></div>
<div class="m2"><p>برسان تو در میدان لشگر شکری نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از جمع امیران جهان چون تو ندیدم</p></div>
<div class="m2"><p>وز جمله شاهان چو تو اندر خبری نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی مدح و ثنای تو گزیده سخنی نیست</p></div>
<div class="m2"><p>بی تیغ و سنان تو ستوده ظفری نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نزدیک تو کس رنج نبرده است بخدمت</p></div>
<div class="m2"><p>کز دولت گنج تو بر او تازه تری نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دانا و توانا بسفر گردد مردم</p></div>
<div class="m2"><p>از قصد بدرگاه تو بهتر سفری نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هرچند بدرگاه تو من قصد نکردم</p></div>
<div class="m2"><p>چون من بجهان نیز تو را مدح گری نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وقفیست ز دو میر دهی خرد بمن بر</p></div>
<div class="m2"><p>در ده بجز از جفت من و برزگری نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یک روز مرا باشد و یک روز مرا نه</p></div>
<div class="m2"><p>زیرا که در این نعمت پیوسته سری نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر کار گذاری که بدین ناحیت آید</p></div>
<div class="m2"><p>گوید که مرا برده تو بر گذری نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون راست شود کارش و ایمن بنشیند</p></div>
<div class="m2"><p>گوید که مرا جز بده تو نظری نیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در قسم نشد گویم در قسم شده گیر</p></div>
<div class="m2"><p>در نیمه من کسرا آن داد وری نیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هرچند بگویم سخن من ننیوشد</p></div>
<div class="m2"><p>گوید که در این معنی ما را نظری نیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>غم نیست بگیتی که غمی نیست فزون زان</p></div>
<div class="m2"><p>بد نیست در آفاق که زان بد بتری نیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>من باز نمودم بتو ای میر همه حال</p></div>
<div class="m2"><p>کز گفته من هیچکسی را ضرری نیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آن را خطری نیست بر تو به جهان را؟</p></div>
<div class="m2"><p>کان را ببر من که رهی ام خطری نیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خالی نکناد ایزد گوشت ز بشارت</p></div>
<div class="m2"><p>زیرا که بجود تو بگیتی بشری نیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>باد از تو و یاران تو بیداد فلک دور</p></div>
<div class="m2"><p>کاندر همه آفاق چو تو دادگری نیست</p></div></div>