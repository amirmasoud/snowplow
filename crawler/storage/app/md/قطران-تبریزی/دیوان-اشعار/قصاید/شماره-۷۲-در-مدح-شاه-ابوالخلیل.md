---
title: >-
    شمارهٔ ۷۲ - در مدح شاه ابوالخلیل
---
# شمارهٔ ۷۲ - در مدح شاه ابوالخلیل

<div class="b" id="bn1"><div class="m1"><p>چون روز بر کشید سر از قیرگون حریر</p></div>
<div class="m2"><p>بر کوهسار زر بگسترد چون زریر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون زردگون حریر شد از عکس او بلون</p></div>
<div class="m2"><p>یاقوت زرد ریخته بر زرگون حریر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون شنبلید زار میان بنفشه زار</p></div>
<div class="m2"><p>از گوشه سپهر روان مهر دلپذیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا چون غدیر بود پر از آب نیلگون</p></div>
<div class="m2"><p>از زر زورقی زبر آب آن غدیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوئی نشسته خسرو چین بر سریر زر</p></div>
<div class="m2"><p>زرین سپر بداشته در پیش آن سریر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کوه از فروغ آن شده پر توده های زر</p></div>
<div class="m2"><p>دشت از شعاع این شده پر چشمه های شیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از ماه تا بماهی اگرچه تفاوت است</p></div>
<div class="m2"><p>بگرفته است از اوزثری نور تا اثیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اندرا سد ندیدم چونینش تافته</p></div>
<div class="m2"><p>وندر حمل نیافتم ایدونش مستنیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گسترده بد ز گاه سحر تا گه زوال</p></div>
<div class="m2"><p>سوزنده در زمستان چون در تنور تیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در پیش تافتنش نه کاریست بیهده</p></div>
<div class="m2"><p>وز نور دادنش نه حدیثی است خیر خیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از تف او جدا ز تن مفلسان ضرر</p></div>
<div class="m2"><p>وز نور او بخواندی نقش نگین ضریر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مانا بسعد خسروی و فال مشتری</p></div>
<div class="m2"><p>در وی نشاط زهره و تدبیر رای تیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ایزد بکاست دیده ز بهر خزینه بخش؟</p></div>
<div class="m2"><p>یزدان فزود عمر شهنشاه شیر گیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون مهر چهر خویش نهان کرد در زمین</p></div>
<div class="m2"><p>از گوشه سپهر برآمد مه منیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نزدیک زی میانش دو صد تیر تابناک</p></div>
<div class="m2"><p>چون در کمان زرین سیمین نهاده تیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اندر میان جوزا تابنده ماه نو</p></div>
<div class="m2"><p>چون در کمر نهاده نگون تاج اردشیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون موی بند حورا چون یاره پری</p></div>
<div class="m2"><p>چون ناخن بریده چو ابروی مرد پیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون نیم طوق فاخته از زر ساخته</p></div>
<div class="m2"><p>یا در کنار ماه درخشان درفش میر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>قطب و قرار ملک جهان میر ابوالخلیل</p></div>
<div class="m2"><p>کز روی اوست چشم ملوک جهان قریر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از تف تیغ شاه شراریست آفتاب</p></div>
<div class="m2"><p>وز آه دشمنانش بخاریست زمهریر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شاه سریر اگر بکشد سر ز طاعتش</p></div>
<div class="m2"><p>گردد سریر بند گران بر شه سریر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>قیصر ز قصر مملکت ار قصد او کند</p></div>
<div class="m2"><p>دستش ز قصر ملک کند تیغ او قصیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ور کین او سگالد سالار قیروان</p></div>
<div class="m2"><p>قیران روزگار کند روز او چو قیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>فارغ مباد جان عدوش از عذاب عصر</p></div>
<div class="m2"><p>خالی مباد دست وی از ساغر عصیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جستن خطای او خطر جان و تن بود</p></div>
<div class="m2"><p>دائم کند حذر ز خطر مردم خطیر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>حاسد فتد بدام چو اسبش کند صهیل</p></div>
<div class="m2"><p>ناصح رسد بکام چو کلکش کشد صریر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از جان دوستان غم و ناله کند نفور</p></div>
<div class="m2"><p>وز جان دشمنان بکشد ناله و نفیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کلک و بنان اوست همه روزی بشر</p></div>
<div class="m2"><p>تیغ و سنان اوست بفتح بشر بشیر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خصمانش را ز دهر بود بهره زهر مار</p></div>
<div class="m2"><p>اعداش را ز چرخ بود بهره تیغ و تیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بر دشمنانش چو تیر کند خشم او بهار</p></div>
<div class="m2"><p>بر دوستان خویش کند چون بهار تیر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چون خاک و نار و آب و هوایش درست خوش</p></div>
<div class="m2"><p>دیدنش ناگزای و گزیدنش ناگزیر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بر حاسدان جهان شد از هول او حصار</p></div>
<div class="m2"><p>بر جانشان خلافش چون نار بر حصیر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در مغز بدسگال کند تیغ او مقر</p></div>
<div class="m2"><p>وز رأی روزگار بود رأی او خبیر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خواهد بفخر مهر که بر گیردش بمهر</p></div>
<div class="m2"><p>خواهد بطبع تیر که پیشش بود دبیر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آن روز بد نبیند کو باشدش معین</p></div>
<div class="m2"><p>وان راه بد نگیرد کو باشدش مشیر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ای روزگار چون تو نیاورده شهریار</p></div>
<div class="m2"><p>شاهان ترا شکار و امیران ترا اسیر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گاه سلام و سهم چو کاوس و کیقباد</p></div>
<div class="m2"><p>گاه کلام و فهم چو قابوس وشمگیر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هنگام رزم پیلی و هنگام بزم نیل</p></div>
<div class="m2"><p>در نثر چون خلیلی و در نظم چون جریر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دارد چهار گوهر در طبع تو سرشت</p></div>
<div class="m2"><p>هستی ز چار گوهر بی مثل و بی نظیر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>عزم درست داری و رأی صواب و راست</p></div>
<div class="m2"><p>عقل تمام داری و کردارها خبیر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گردون ترا مسخر و انجم ترا مطیع</p></div>
<div class="m2"><p>گیتی ترا مساعد و یزدان ترا نصیر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از مهر تو سعیر شود بر ولی بهشت</p></div>
<div class="m2"><p>از کین تو بهشت شود برعد و سعیر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خصمت سلیم باد و غم و رنج او سلیم</p></div>
<div class="m2"><p>بدگوی تو ضریر و تو در کارها بصیر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ای لفظ تو بخوبی ماننده زبور</p></div>
<div class="m2"><p>کرده مدیح تو همه خلق هان زبیر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>اندر مدیح شاه جهان ظن برم که نیست</p></div>
<div class="m2"><p>کس را بداده قدرت من ایزد قدیر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>آن شاعری کند به جهان نقض شعر من</p></div>
<div class="m2"><p>کو شعر و وزن شعر بنشناسد از شعیر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نظمم بمدح شاه بود گوهر نظیم</p></div>
<div class="m2"><p>نثرم بذکر میر بود لؤلؤ نثیر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تا بانگ زیر باشد در بزم گاه شاه</p></div>
<div class="m2"><p>تا گنج زر میر فراز آورد بزیر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تا هست نام مرده عدوی تو مرده باد</p></div>
<div class="m2"><p>تا هست نام میری شاهی کن و ممیر</p></div></div>