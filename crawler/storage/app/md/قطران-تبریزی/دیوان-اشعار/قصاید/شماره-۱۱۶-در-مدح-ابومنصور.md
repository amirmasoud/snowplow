---
title: >-
    شمارهٔ ۱۱۶ - در مدح ابومنصور
---
# شمارهٔ ۱۱۶ - در مدح ابومنصور

<div class="b" id="bn1"><div class="m1"><p>تنم به گونه نال و دلم به گونه نیل</p></div>
<div class="m2"><p>جهان ز نیلم نال و روان ز نالم نیل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو نیل چشم منست از گریستن شب و روز</p></div>
<div class="m2"><p>چراست جای نهنگ اندر آن دو چشم کحیل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رفیق رنجم تا عشق با منست رفیق</p></div>
<div class="m2"><p>عدیل در دم تا هجر با منست عدیل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم بسان هوا آمد از هوای حبیب</p></div>
<div class="m2"><p>تنم بسان خیال آمد از خیال خلیل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بتی که قدش چون قول عاشق آمد راست</p></div>
<div class="m2"><p>مهی که قولش چون پشت عاشق آمد کیل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بروی خلد و بلب سلسبیل و من کردم</p></div>
<div class="m2"><p>دل و تن از پی آن خلد و سلسبیل سبیل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسان خضر پیمبر همیشه زنده بوم</p></div>
<div class="m2"><p>اگر بیابم بر سلسبیل دوست سبیل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا بس است بدین درد روی زرد گواه</p></div>
<div class="m2"><p>مرا بس است برین انده آب دیده دلیل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همی گریزد صبرم ز عشق آن بت روی</p></div>
<div class="m2"><p>چنانکه خیل گریزد ز جنگ میر جلیل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جمال و جاه جهان شهریار ابومنصور</p></div>
<div class="m2"><p>که روزگار بدیدار او شده است جمیل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بروز بخشش او و بروز کوشش او</p></div>
<div class="m2"><p>چو قطره باشد نیل و چو پشه باشد پیل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بتیغ جان بستاند بدست باز دهد</p></div>
<div class="m2"><p>بدین بعیسی ماند بدان بعزرائیل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رضای او بدل اندر برابر توحید</p></div>
<div class="m2"><p>خلاف او بتن اندر برابر تعطیل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ایا زمانه تن و دولت تواش زیور</p></div>
<div class="m2"><p>ایا سپهر سر و همت تواش اکلیل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بگاه جود ندانی که چون بود تاخیر</p></div>
<div class="m2"><p>بگاه حلم ندانی که چون بود تعجیل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هزار زائر بر درگهت نزول کند</p></div>
<div class="m2"><p>نکرده زائری از درگهت هنوز رحیل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر نبارد ابرو نبات نارد بر</p></div>
<div class="m2"><p>برزق خلق پس آن کف کافی تو کفیل</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بنزد ایزد مدح تو همچنان تسبیح</p></div>
<div class="m2"><p>بنزد باری شکرت برابر تهلیل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر عدوت خورد نوش و وز تو یاد کند</p></div>
<div class="m2"><p>بماند آن نوش اندر کلوش چون نشپیل</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بهیچ دانش گردون نبوده با تو خسیس</p></div>
<div class="m2"><p>بهیچ فضل ستاره نبوده با تو بخیل</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز دست و طبع و دل هرکسی سخاوت و فضل</p></div>
<div class="m2"><p>بکرد سوی دل و دست طبع تو تحویل</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز بهر این همگان سائلند و تو معطی</p></div>
<div class="m2"><p>همه کسیرا نقص آید و ترا تفضیل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بفضل و دانش پیری به رای و بخت جوان</p></div>
<div class="m2"><p>بجود و فضل کثیری بسال و ماه قلیل</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نهفته مال همه خسروان برافشاندی</p></div>
<div class="m2"><p>درست گوئی بودند خسروانت و کیل</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زمانه بر تو نیابد بهیچ باب عوض</p></div>
<div class="m2"><p>ستاره با تو نیارد بهیچ روی بدیل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خدایگانا از آرزوی صورت تو</p></div>
<div class="m2"><p>تنم شده است نحیف و دلم شده است علیل</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همیشه مهر تو ورزم چو مؤبدان آتش</p></div>
<div class="m2"><p>همیشه مدح تو خوانم چو راهبان انجیل</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر بخدمت نایم بر تو معذورم</p></div>
<div class="m2"><p>که مر مرا نگذارند از این زمین یکمیل</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اگر فقیر مقصر شدم بخدمت تو</p></div>
<div class="m2"><p>همیشه هست زبانم بمدحت تو طویل</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همیشه تا خبر زهره باشد و هاروت</p></div>
<div class="m2"><p>چنانکه قصه قابیل باشد و هابیل</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عدوت باد چو هاروت و دوست چون زهره</p></div>
<div class="m2"><p>ولیت باد چو هابیل و خصم چون قابیل</p></div></div>