---
title: >-
    شمارهٔ ۶۷ - در مدح ابوالخلیل جعفر
---
# شمارهٔ ۶۷ - در مدح ابوالخلیل جعفر

<div class="b" id="bn1"><div class="m1"><p>بفرخ فال و خرم بخت و میمون روز و نیک اختر</p></div>
<div class="m2"><p>بدارالملک باز آمد شه نیک اختر از لشگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکسته لشکر جنگی بسان خیل افریدون</p></div>
<div class="m2"><p>گشاده قلعه محکم بسان سد اسگندر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنین زی لشگر ترکان و پیکار بداندیشان</p></div>
<div class="m2"><p>برفت و قلعه ای بگرفت در دم اژدها پیکر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان چون اژدهای هند پیچان بر لب دریا</p></div>
<div class="m2"><p>رسانده زی ثری دنبال و برده بر ثریا سر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندیدند ایچ میغی را که بارید از بر او نم</p></div>
<div class="m2"><p>ندیدند ایج مرغی را که بگشود از سر او پر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه گیتی همی گفتند جنگ و شغل آن دژ را</p></div>
<div class="m2"><p>نباید تاختن آنجا بباید ساختن ایدر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ملک نشنید قول کس به رأی خویش بیرون شد</p></div>
<div class="m2"><p>که فر خداوند است و با تأیید پیغمبر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همانا غیب ها داند که هرچه گوید آن باشد</p></div>
<div class="m2"><p>ز ناز و رنج و مهر و کین و صلح و جنگ و خیر و شر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه هر کاری خدایی را ز مردم مشورت باید</p></div>
<div class="m2"><p>نه هرگز هیچ پیغمبر کسی را گشت فرمان بر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کسی کو را بود یزدان مساعد عالم او را دان</p></div>
<div class="m2"><p>چه انس و جان و گنج و کان چه کوه و در چه بحر و بر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدین زودی ظفر کو یافت بر محکم دزی چونین</p></div>
<div class="m2"><p>نه رستم یافت بر گنگ نه حیدر یافت بر خیبر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جهانگیری چنو هرگز نبوده است و نباشد هم</p></div>
<div class="m2"><p>از آدم باز تا اکنون وز اکنون تا گه محشر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بچشم دوستان اندر خیالش همچو خواب خوش</p></div>
<div class="m2"><p>بچشم دشمنان اندر سنانش چون سر نشتر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدست حاسدانش گل شود چون شعله آتش</p></div>
<div class="m2"><p>به نزد ناصحانش آتش شود چون دیبه ششتر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از او راضی شده سلطان از او عاجز شده دشمن</p></div>
<div class="m2"><p>خدای آسمان او راگشاد از ناز و نیکی در</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جهان از فر او خالی نباشد جاودان زیرا</p></div>
<div class="m2"><p>که داد از فر خویش او را خدای جاودانی فر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همیشه هست کارش راست زان کور است دارد دل</p></div>
<div class="m2"><p>چه با دوست و چه با دشمن چه با مؤمن چه با کافر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اگر رادیش را گویی چو حاتم شایدش خادم</p></div>
<div class="m2"><p>و گر مردیش را گویی چو رستم بایدش چاکر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همیشه تا فلک گردان و خور تابان بود باشد</p></div>
<div class="m2"><p>خداوند فلک یزدان خداوند زمین جعفر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نبرده بوالخلیل آن کو بنوک نیزه و زوبین</p></div>
<div class="m2"><p>ظفر جوید ز پیل مست و ببر تند و شیر نر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سپرده خدمتش را جان امیران جهان یکسان</p></div>
<div class="m2"><p>نهاده طاعتش را سر بزرگان جهان یکسر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>امیرا از تو بدخواهان غلط کردند یکسر ظن</p></div>
<div class="m2"><p>ندانستند کت دانش مشیر است و خرد رهبر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نباشد هیچ روزی نو که فتح تو نیابی تو</p></div>
<div class="m2"><p>نباشد هفته ای دیگر که نستانی دژ دیگر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همه نام از هنر جویی همه داد از خردخواهی</p></div>
<div class="m2"><p>ز هرکس داد بستاند کسی کو را خرد داور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همه کردارهای تو مهان و خسروان شاها</p></div>
<div class="m2"><p>همی بینند در عمری نباشد شان همی باور</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بروز بزم در مجلس سخا باشد تو را مونس</p></div>
<div class="m2"><p>بروز رزم در میدان فلک باشد ترا یاور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بکف راد روز مهر و تیغ تیز روز کین</p></div>
<div class="m2"><p>بدین سازنده چون آبی بدان سوزنده چون اخگر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بروز رزم تو خصمان دهند اندر هزیمت گه</p></div>
<div class="m2"><p>دو صد مغفر بیک معجر دو صد جوشن بیک چادر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ازانگه کآفرید ایزد جهان اندر جهان نامد</p></div>
<div class="m2"><p>جز از تو تخت را زیبا جز از تو تاج را در خور</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو چون جمشید دانائی چو افریدون توانائی</p></div>
<div class="m2"><p>بدانش همچو بهرامی بمردی همچو زال زر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ببانگ سائلان جانت بیفروزد چونا ناگاهان</p></div>
<div class="m2"><p>ببانگ گم شده فرزند بفروزد دل مادر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چنان گشته است زر و سیم خوار از کف راد تو</p></div>
<div class="m2"><p>که دارد سنگ ننگ از سیم و دارد خاک عار از زر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همت دین است و هم دانش همت رای است و هم رامش</p></div>
<div class="m2"><p>همت بخشش همت کوشش همت منظر همت مخبر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>امیرا بنده معذور است گر نامد بره با تو</p></div>
<div class="m2"><p>که پشتش بود چون چوگان و قدش بود چون چنبر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر از سر پای دانستی کسی کردن بدانائی</p></div>
<div class="m2"><p>بجان پاک تو شاها که کردی بنده پا از سر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>الا تا آرزو نکند کسی سوزن به سو سنبر</p></div>
<div class="m2"><p>الا تا کس گزین نکند همی حنظل شبکر بر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بزیر و پشت بدگویانت سوسن باد چون سوزن</p></div>
<div class="m2"><p>میان کام دلجویانت حنظل باد چون شکر</p></div></div>