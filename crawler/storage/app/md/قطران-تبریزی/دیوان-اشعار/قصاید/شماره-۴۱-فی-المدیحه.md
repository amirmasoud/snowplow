---
title: >-
    شمارهٔ ۴۱ - فی المدیحه
---
# شمارهٔ ۴۱ - فی المدیحه

<div class="b" id="bn1"><div class="m1"><p>دیر آمدن شاه برآورد ز من دود</p></div>
<div class="m2"><p>گر دیرتر آید برود جان و تنم زود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بسکه همی دارم در سینه غم شاه</p></div>
<div class="m2"><p>خون دل ریشم زره دیده بپالود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با پشت خم آگینم و با کام سم آگین</p></div>
<div class="m2"><p>با چشم دم آلودم و با جان غم آلود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون لاله رخم زردتر از چهره زر گشت</p></div>
<div class="m2"><p>چون کوه تنم زارتر از کاه بفرسود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگه که حدیثی بشنیدم ز اراجیف</p></div>
<div class="m2"><p>از درد تو بر جانم صد درد بیفزود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رنجورم و معذورم کز پادشهم دور</p></div>
<div class="m2"><p>بی او فلکم رامش و آرامش پیمود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنکو نبود مرده ولی نعمت خود را</p></div>
<div class="m2"><p>جز ناله و فریاد بدو عقل نفرمود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بودند بریده ز من امید همه کس</p></div>
<div class="m2"><p>ایزد بکرم بر من بیچاره ببخشود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرچند بلا دیدم خشنودم از ایزد</p></div>
<div class="m2"><p>بخشایش ایزد همه را دارد خشنود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرکو بگه درد بایزد نزند دست</p></div>
<div class="m2"><p>بر هرچه رود بر سر او باشد مأخوذ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این خلق ز دیر آمدن شاه چنانند</p></div>
<div class="m2"><p>کز سبزه تهی بستان وز آب جدا رود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دلها همه پر درد و دهانها همه پر گل</p></div>
<div class="m2"><p>رخها همه پر گرد و زبانها همه پردود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رنجور شد و بیم زده خلق زیانکار</p></div>
<div class="m2"><p>تا شاه جهان آن ره دشخوار بپیمود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باز آمد و هزمان بشود ز آمدنش باز</p></div>
<div class="m2"><p>این رنج همه راحت و این بیم زیان سود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با آفت بدگوی چنان باشد جانش</p></div>
<div class="m2"><p>چون حال خلیل الله با آفت نمرود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از خصم کی آید بهمه حال که بهتر</p></div>
<div class="m2"><p>کرد ار زر آگند ز گفتار زر اندود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پاینده همی باد بملک اندر چندان</p></div>
<div class="m2"><p>کاین چرخ فلک باشد و این دور فلک بود</p></div></div>