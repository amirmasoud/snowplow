---
title: >-
    شمارهٔ ۱۶۲ - در مدح ابوالهیجا منوچهر بن وهسودان
---
# شمارهٔ ۱۶۲ - در مدح ابوالهیجا منوچهر بن وهسودان

<div class="b" id="bn1"><div class="m1"><p>ز ابرو باد آزاری بشد آراسته بستان</p></div>
<div class="m2"><p>کنون داد از می و جانان ببستان اندرون بستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پدید آمد نهفته گل بخور می برشکفته گل</p></div>
<div class="m2"><p>بمرجان در گرفته گل همه باغ و همه بستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو گشتی ابر تندر بر پر از لؤلؤ بچرخ اندر</p></div>
<div class="m2"><p>هوا پر ناله تندر چمن پر غلغل مستان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فراز سوسن و سنبل فکنده سایه شاخ گل</p></div>
<div class="m2"><p>فراز شاخ گل بلبل زنان چون مطربان دستان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنفشه زیر گل رسته از آب نیل رخ شسته</p></div>
<div class="m2"><p>چو مهجوران دلخسته خمیده پشت چون چوگان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بکوه آهو گرازنده سر از کشی فرازنده</p></div>
<div class="m2"><p>گهی بر لاله تازنده گهی بر نسترن غلطان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سمن لؤلؤ نماینده سرشگ از گل گراینده</p></div>
<div class="m2"><p>بباغ اندر سراینده هزار آوا هزار افسان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که و صحرا پر از لاله ز مرغان باغ پر ناله</p></div>
<div class="m2"><p>میان لاله در ژاله چو دندان و لب جانان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کنون هستی یکی روزه به از سی روز هر روزه؟</p></div>
<div class="m2"><p>ز می همرنگ پیروزه بر او گل رسته چون مرجان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چمن چون دیبه چینی شکوفه گشته پروینی</p></div>
<div class="m2"><p>زمین و آسمان بینی نه بینی باز اینرازان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شکفته شنبلید اندر چو زرین ساغر از گوهر</p></div>
<div class="m2"><p>دمیده گرد او عبهر چو پروین زهره تابان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بتابد برق ز ابر آنجا چو تیغ اندر صف هیجا</p></div>
<div class="m2"><p>ز دست میر ابوالهیجا منوچهربن وهسودان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خداوندی شهی میری گهربخشی جهانگیری</p></div>
<div class="m2"><p>اگر خواهد بهر تیری بدوزد سینه کیوان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز نور آمد تنش نزگل وز او هر مشکلی حاصل</p></div>
<div class="m2"><p>ز رادی هست یکسر دل ز مردی هست یکسر جان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گه جنگ و گه رادی دلش ناری کفش بادی</p></div>
<div class="m2"><p>از این خواهنده را شادی وزان بدخواه را احزان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چنو میری بگیتی در نه صفدار است نه صفدر</p></div>
<div class="m2"><p>گشاده دل گشاده در نهاده خو نهاده خوان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تنش همچو روان روشن روانشرا خرد گلشن</p></div>
<div class="m2"><p>کند گر روی در گلخن بکانون در کند نیسان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نهاده گردنش گردون فلک با همت او دون</p></div>
<div class="m2"><p>زمین از دیدنش میمون هوا از بوش مشگ افشان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بدان گفتار در آگین کند شادان دل غمگین</p></div>
<div class="m2"><p>سنانش هست کان کین بنانش هست کین کان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جهان از کین او عاجز چنو نارد فلک هرگز</p></div>
<div class="m2"><p>همه گفتار او معجز همه کردار او برهان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کسی کو مهر او جوید گل بخت بقا بوید</p></div>
<div class="m2"><p>کسی کو مدح وی گوید شود ز احسانش چون حسان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز دستش جود شد قائم ز بختش جور شد نائم</p></div>
<div class="m2"><p>جهان گوید همی دائم ز من طاعت از او فرمان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سخاوت دارد او پیشه شجاعت دارد اندیشه</p></div>
<div class="m2"><p>ز هولش شیر در بیشه بود بر خویشتن پیچان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو دولت طلعتش فرخ سپرده عالم او را رخ</p></div>
<div class="m2"><p>نهد هزمان بطاعت رخ بزیر پای او خاقان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اگر گیتی ستر گیرا بیارد پیش گر گیرا</p></div>
<div class="m2"><p>بدو بخشد بزرگی را فزایش برکشد از اقران</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر او بودی بملک اندر نبودی کس بسلک اندر</p></div>
<div class="m2"><p>گرفته زیر کلک اندر بدانائی جهان یکسان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شدی میر اجل زنده عدو بودی سرافکنده</p></div>
<div class="m2"><p>ندیدی او پراکنده یکی پرورده ایشان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ولیکن عالم و کانا بدل دارد چنین مانا</p></div>
<div class="m2"><p>کز او غمگین بود دانا وز او نادان بود شادان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نگیرد همچنان روزی شود غم زو نهان روزی</p></div>
<div class="m2"><p>خورندانده شهان روزی بکام و نام جاویدان؟</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>که هر کو را خرد گوید که باید میری او جوید</p></div>
<div class="m2"><p>کز او میری همی بوید چو مشگ از عنبر آگین بان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ایا پیرایه میری تو داری پایه میری</p></div>
<div class="m2"><p>بدانش مایه میری همی پیوسته با میران</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بدست و تیغ در داری وفا و مرگ پنداری</p></div>
<div class="m2"><p>شرف بی مهر تو خواری سخن بی مدح تو بهتان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بر تو زر زندانی نباشد یکزمان خانی</p></div>
<div class="m2"><p>بداندیشت ز نادانی همیشه باد در زندان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بود بی آب چون بیدا به پیش دست تو دریا</p></div>
<div class="m2"><p>بود بی تاب چون دیبا بپیش خشت تو سندان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>معادیرا به بیدادی پدید آری غم از شادی</p></div>
<div class="m2"><p>موالیرا گه رادی کنی دشوارها آسان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>الا تا خوردن زوبین کند جان و روان غمگین</p></div>
<div class="m2"><p>الا تا دیدن نسرین کند جان و روان شادان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ترا آماده پیوسته ز نسرین و ز گل دسته</p></div>
<div class="m2"><p>مخالف را جگر خسته گه از زوبین گه از پیکان</p></div></div>