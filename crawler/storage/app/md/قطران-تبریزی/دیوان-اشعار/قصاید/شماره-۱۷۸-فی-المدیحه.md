---
title: >-
    شمارهٔ ۱۷۸ - فی المدیحه
---
# شمارهٔ ۱۷۸ - فی المدیحه

<div class="b" id="bn1"><div class="m1"><p>هر آنچه هست نهان از منجمان جهان</p></div>
<div class="m2"><p>ز رای روشن شاه زمانه نیست نهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر آنچه خواهد بودن در آیدش بضمیر</p></div>
<div class="m2"><p>هر آنچه خواهد رفتن در آیدش بزیان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ببیند او بعیان هر چه نزد عقل خبر</p></div>
<div class="m2"><p>در آیدش بیقین هر چه نزد خلق گمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سپه برون برد از رود ژرف بی کشتی</p></div>
<div class="m2"><p>گهر برآورد از سنگ خاره بی کهکان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چه شاه جوانست بخت او پیر است</p></div>
<div class="m2"><p>ز عقل پیرش بختش همیشه هست جوان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو او ز گنجه بفال بهی برون آمد</p></div>
<div class="m2"><p>یکیش گفتی این و یکیش گفتی آن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که بی سپاه گران خصم را مدار سبک</p></div>
<div class="m2"><p>بجنگ خصم منه روی بی سپاه گران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نبرد کس را فرمان و خیمه بیرون زد</p></div>
<div class="m2"><p>جز آن نکرد کجا آید از خرد فرمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز درد زود رها گردد آنکسی که کند</p></div>
<div class="m2"><p>باتفاق خرد درد خویش را درمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو بدسگال ز کردار شاه شد آگاه</p></div>
<div class="m2"><p>دلش نژند شد از بیم و تن ز هول نوان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو دم بخواهش نگشاد آنکه رفتش پیش</p></div>
<div class="m2"><p>بجنگ جستن شاه جهان ببست میان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بمال و ملک سپاهی بهم فرا آورد</p></div>
<div class="m2"><p>فزون ز برگ درختان و قطره باران</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سوارشان همه گردان ارمن و ابخاز</p></div>
<div class="m2"><p>پیاده شان همه شیران لگزی و شروان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همه بتیغ چو گیو و بنیزه چون بیژن</p></div>
<div class="m2"><p>همه بحمله چو رستم بحیله چون دستان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برابر شه آران شدند بر کوهی</p></div>
<div class="m2"><p>که بی دلیل نداند در آن شدن شیطان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پناه خویش گرفتند بیشه بر سر کوه</p></div>
<div class="m2"><p>چنانکه سرش همی گفت راز با سرطان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چور ایت شه گیتی بدشت پیدا شد</p></div>
<div class="m2"><p>نهان شدند سپه در درون یکان و دوگان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ملک بیامد آنجا بناز و فیروزی</p></div>
<div class="m2"><p>گشاده روی و گشاده دل و گشاده عنان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دو روز خرم و خندان بگرد آن بنشست</p></div>
<div class="m2"><p>شده بدیدن او خلق خرم و خندان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>برفت وی که بسوزد زمین دشمن دین</p></div>
<div class="m2"><p>مگر شود جگر دشمنان بدان سوزان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سران لشگر ایشان رسید بر کوهی</p></div>
<div class="m2"><p>که هیچ خلق بدان سرکشی نداد نشان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سپاه شاه کشیدندشان ز کوه بدشت</p></div>
<div class="m2"><p>بیامدند ز دوده دل و ز دو ده سنان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز نیره ها همه صحرا چو نیستان شده بود</p></div>
<div class="m2"><p>همه چو شیران در نیستان گرفته مکان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بسان طوفان از که برآمدند و لیک</p></div>
<div class="m2"><p>بخاست بر ز می از خون حلقشان طوفان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بحمله سپه شاه خیل ایشان را</p></div>
<div class="m2"><p>بتیغ کرد دریده دل و رمیده روان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بساعتی تنشان شد نشانه زوبین</p></div>
<div class="m2"><p>بساعتی دلشان شد نشانه پیکان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز هول تیر سواران تیر قد عدو</p></div>
<div class="m2"><p>شدند گوژ و نوان اندران بسان کمان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هوا برنگ شبه شد زمین برنگ عقیق</p></div>
<div class="m2"><p>یکی ز تیر روان و یکی ز خون روان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بجان ز شاه نرسته از آن سپاه دو بهر</p></div>
<div class="m2"><p>بتن نرست و بمال آن کجا برست بجان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سپاهشان را کشته سپاه شاه زمین</p></div>
<div class="m2"><p>امیرشان را کرده اسیر شاه زمان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>امیر همچو شبان باشد و سپه چو رمه</p></div>
<div class="m2"><p>شود رمیده رمه چون شود گرفته شبان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نه مهتر است و نه کهتر بدین سپاه اندر</p></div>
<div class="m2"><p>که نیست مهتری از کافرانش وز دزدان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اگر نبودی تایید شاه شیر شکار</p></div>
<div class="m2"><p>وگر نبودی اقبال میر شهرستان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بکار زاری از پیش لشگری چندین</p></div>
<div class="m2"><p>چگونه گشتی آواره لشگری چندان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همیشه نازش گردنکشان از من و روم</p></div>
<div class="m2"><p>بفتح ار کون بود و فتح ارزنگان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ولیکن ایشان ز انبوه خیل نازیدند</p></div>
<div class="m2"><p>ملک ننازد الا بفره یزدان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بآفتاب بر آورد افسر اسلام</p></div>
<div class="m2"><p>بزیر خاک فرو برد رایت کفران</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خدایگان بزمانی ز کافران بستد</p></div>
<div class="m2"><p>بتیغ کینه فضلون و کینه مملان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کنون هر آنچه تو خواهی بگردنش برنه</p></div>
<div class="m2"><p>کنون هرانچه تو خواهی ز نعمتش بستان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کنون اگر پسرشرا بدو فروشی بر</p></div>
<div class="m2"><p>بس است قلعه نشواد و ایدرش ارزان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تو بر نشاطی و هر روز خصم بر تیمار</p></div>
<div class="m2"><p>تو بر فزونی و هر روز خصم بر نقصان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تودی برون شده بودی بشهر خصم اندر</p></div>
<div class="m2"><p>که تا بر آتش بوم و برش کنی ویران</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چنانکه موسی عمران بکوه آتش جست</p></div>
<div class="m2"><p>پیمبری یافت از کوه موسی عمران</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>یکی سپاه شکستی دلیر و شاه شکن</p></div>
<div class="m2"><p>شهی گرفتی لشگر فروز و گردافشان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>همیشه تا که بود در جهان هوان و هوا</p></div>
<div class="m2"><p>همیشه تا که بود در جهان بهار و خزان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>خزان ناصح تو سال و ماه باد بهار</p></div>
<div class="m2"><p>هوای حاسد تو سال و ماه باد هوان</p></div></div>