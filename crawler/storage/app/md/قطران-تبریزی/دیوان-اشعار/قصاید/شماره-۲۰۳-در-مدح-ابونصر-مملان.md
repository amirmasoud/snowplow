---
title: >-
    شمارهٔ ۲۰۳ - در مدح ابونصر مملان
---
# شمارهٔ ۲۰۳ - در مدح ابونصر مملان

<div class="b" id="bn1"><div class="m1"><p>دهد روی آن سرو سیمین نشانی</p></div>
<div class="m2"><p>ز ماهی که بر سرو سیمین نشانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دخانی پدید آید اندر دو چشمم</p></div>
<div class="m2"><p>از آن روی ناری و زلف دخانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو بر سحر آن ترک خانی بگویم</p></div>
<div class="m2"><p>شود چشم من خانه و خانه خانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل رایگانی که بد مهر پرور</p></div>
<div class="m2"><p>بدادم بدست کسان رایگانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا جسم چون شاخه خیزران شد</p></div>
<div class="m2"><p>ز هجران آن قامت خیزرانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ایا عاشق از عشق چون موی گردی</p></div>
<div class="m2"><p>گر آنی که کوه از تو گیرد کرانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بتن چون هوا از هوان هوائی</p></div>
<div class="m2"><p>بدل با فریب از فریب فغانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز هم داستانی که هستم بر آنم</p></div>
<div class="m2"><p>که هرجا که هستی زنم داستانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر زندگانی بهر حال باشد</p></div>
<div class="m2"><p>تو از مردگانی نه از زندگانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ایا گشته پیر از جوانیت مانده</p></div>
<div class="m2"><p>هوسهات با عارض ارغوانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ایا قبله دلبران زمانه</p></div>
<div class="m2"><p>گر آنی که خون دلم را برانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ندام چه کانی بلا را که چندین</p></div>
<div class="m2"><p>تو از دیده عاشقان خون چکانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه سائی سر زلف بر چهره گل</p></div>
<div class="m2"><p>دلم را می مهر تا کی چشانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بزلف دو تا مبتلا را بلائی</p></div>
<div class="m2"><p>بچشم سیه آهوان را هوانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بسرو چمانت کند وصف هرکس</p></div>
<div class="m2"><p>چه مانی تو سرو چمان را چه مانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هنوز از نوانی ندانی به از بد</p></div>
<div class="m2"><p>بدان را نوازی بهان را نوانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو با کس نمانی که با من نماندی</p></div>
<div class="m2"><p>گهی نزد اینی گهی نزد آنی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همیشه جهانی بگرد جهان در</p></div>
<div class="m2"><p>مگر دشمن شهریار جهانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شهنشاه گیتی ابونصر مملان</p></div>
<div class="m2"><p>که او را بود فر خسرو نشانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خدیوی کجا نام شمشیر تیزش</p></div>
<div class="m2"><p>که برنده است آن شرار یمانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اگر دوزخی بر زبان آرد آنرا</p></div>
<div class="m2"><p>زبانی کند دوزخی را زبانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ایا کی دل و بر دل خصم چون کی</p></div>
<div class="m2"><p>پدید است بر تو نشان کیانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همه فر و فال کیانیست با تو</p></div>
<div class="m2"><p>اگر نز کیانی بگو از کیانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همه فر و فال کیانیست با تو</p></div>
<div class="m2"><p>اگر نز کیانی بگو از کیانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تو زان خاندانی که گردون بنازد</p></div>
<div class="m2"><p>گرش خادم و خاک آن خاندانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو بدخواه مالی و بد خواه مالی</p></div>
<div class="m2"><p>تو آتش نشانی و آتش نشانی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یکی را تو سودی یکی را تو سودی</p></div>
<div class="m2"><p>یکی را زیانی یکی را زیانی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر با زمانه بتازی زمانی</p></div>
<div class="m2"><p>نه زو باز گردی نه زو باز مانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گه حلم گوئی درنگ زمینی</p></div>
<div class="m2"><p>گه خشم گوئی شتاب زمانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو آنی که هفت آسمان را بروزی</p></div>
<div class="m2"><p>توانی بهم بر زدن بی توانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سخا را مکانی بدان کف کافی</p></div>
<div class="m2"><p>بشمشیر خون معادی چکانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مه و مهر از آن مهربانند با تو</p></div>
<div class="m2"><p>که بر مهر آزادگی مهربانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بروز و شبان مر جهان را تو مانی</p></div>
<div class="m2"><p>جهان چون رمه گشت و تو چون شبانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مکان عطائی بدان طبع صافی</p></div>
<div class="m2"><p>یمین و غائی به تیغ یمانی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گمانی برم شهریارا که ناید</p></div>
<div class="m2"><p>تو را در سخن دانی من گمانی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نکو داردم هرکه نیکوم داند</p></div>
<div class="m2"><p>تو نیکو نداری و نیکوم دانی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نخواهم شدن بر کسی بار گردن</p></div>
<div class="m2"><p>توانی مگر بیش از این ناتوانی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>الا تا به آذر جهان پیر گردد</p></div>
<div class="m2"><p>الا تا به آزار یابد جوانی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در این ملکت باستانی بزی تو</p></div>
<div class="m2"><p>بشادی دو رخ چون گل بوستانی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>طرب کن به آواز چنگ مغنی</p></div>
<div class="m2"><p>طلب کن ز خوبان نبیذ معانی</p></div></div>