---
title: >-
    شمارهٔ ۸۸ - در مدح ابوالیسر
---
# شمارهٔ ۸۸ - در مدح ابوالیسر

<div class="b" id="bn1"><div class="m1"><p>نگار کرد رخ من بخون دیده نگار</p></div>
<div class="m2"><p>کنار کرد بیکبارگی مرا ز کنار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من از جدائی آن دلبر فریشته خوی</p></div>
<div class="m2"><p>ز خورد و خواب جدا مانده ام فریشته وار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بسکه هجر همی کاهدم چنان شده ام</p></div>
<div class="m2"><p>که مهر بر سر دیوار و کاه بر دیوار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسان نار کفیده شده است دیده من</p></div>
<div class="m2"><p>وز او سرشگ رونده بسان دانه نار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرفت از آن لب چون باده جان من مستی</p></div>
<div class="m2"><p>گرفت از آن دل چون روی رای من زنگار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همیشه رنج مرا و دو زلف او پیچان</p></div>
<div class="m2"><p>همیشه درد مرا و دو چشم او بیمار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشاط من بربود و بخصم داد نشاط</p></div>
<div class="m2"><p>قرار من بکسست و بهجر داد قرار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر شرنگ بداری بر ابر لب دوست</p></div>
<div class="m2"><p>وگر زریر بداری مقابل رخ یار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بساعت اندر گردد شرنگ همچو شکر</p></div>
<div class="m2"><p>بساعت اندر گردد زریر چون گلنار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ایا مهی که ز تو خوار شد مه گردون</p></div>
<div class="m2"><p>ایا بتی که ز تو خوار شد بت فرخار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بقد سروی گر سر و ماه دارد بر</p></div>
<div class="m2"><p>بروی ماهی گر ماه مشگ آرد بار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز رنگ و روی تو من بی نیازم از بزاز</p></div>
<div class="m2"><p>ز بوی زلف تو من بینیازم از عطار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که پیش روی تو مانند قار باشد قیر</p></div>
<div class="m2"><p>که پیش زلف تو مانند قیر باشد قار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر ببویم زلفین تو کنی پرخاش</p></div>
<div class="m2"><p>وگر ببوسم رخسار تو کنی پیکار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سپاسدار نه ای کآن ببویدت زلفین</p></div>
<div class="m2"><p>پسند کار نه ای کان ببوسدت رخسار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که دیده باشد و بوسیده صد هزاران ره</p></div>
<div class="m2"><p>رکاب عالی و مجلسگه سهپسالار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چراغ ناموران جهان ابوالیسر آن</p></div>
<div class="m2"><p>که یمن و یسرش هستند بر یمین و یسار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بجود بر سر گردون همی زند افسر</p></div>
<div class="m2"><p>بجنگ بر سر شیران همی کند افسار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ولی همیشه فرازان بدو و دشمن پست</p></div>
<div class="m2"><p>عدو همیشه فروزان بدو و خواسته خوار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز بار انده تا رستخیز رسته شود</p></div>
<div class="m2"><p>هر آن کسیکه بدیدار او بیابد بار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بطبع ناید چندان بصد قرون مردم</p></div>
<div class="m2"><p>ز کان نخیزد چندان بصد قران دینار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که او بکشت گه کینه آختن یکراه</p></div>
<div class="m2"><p>که او بداد گه بزم ساختن یکبار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز بسکه کشت تهی کرد عالم از اعدا</p></div>
<div class="m2"><p>ز بسکه داد تهی کرد عالم از دینار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدستش اندر شادی بتیغش اندر غم</p></div>
<div class="m2"><p>بمهرش اندر منبر بکینش اندر دار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز کین او ببهار اندرون همیشه خزان</p></div>
<div class="m2"><p>ز مهر او به خزان اندرون همیشه بهار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ستاره گشت به فرهنگ و فضل او خوشنود</p></div>
<div class="m2"><p>زمانه داد به تدبیر و رای او اقرار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ایا نشانده بباران جود گرد نیاز</p></div>
<div class="m2"><p>و یا نموده بخورشید فضل روز وقار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سخا ز دست تو پیدا چو ذره از خورشید</p></div>
<div class="m2"><p>وغا بتیغ تو پیدا چو نقطه از پرگار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تو چون میانه ای و دیگران همه چو در</p></div>
<div class="m2"><p>تو چون فذالکی و دیگران همه چو شمار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز بخشش تو نمانده است ذره ای خواهش</p></div>
<div class="m2"><p>ز رامش تو نمانده است نقطه ای بیمار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همیشه تا نتوان کرد خار فرد از ورد</p></div>
<div class="m2"><p>همیشه تا نتوان کرد نور دور از نار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز نار باد ابر جان دوستان تو نور</p></div>
<div class="m2"><p>ز ورد باد ابر چشم دشمنان تو خار</p></div></div>