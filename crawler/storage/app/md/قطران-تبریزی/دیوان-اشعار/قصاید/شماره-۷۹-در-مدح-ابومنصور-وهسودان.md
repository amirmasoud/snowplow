---
title: >-
    شمارهٔ ۷۹ - در مدح ابومنصور وهسودان
---
# شمارهٔ ۷۹ - در مدح ابومنصور وهسودان

<div class="b" id="bn1"><div class="m1"><p>شد ز فر ماه فروردین جهان فردوس وار</p></div>
<div class="m2"><p>باغها دیبا سلب شد شاخها مرجان نگار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد هزاران فرش رنگینست در هر بوستان</p></div>
<div class="m2"><p>صد هزاران شمع رخشانست در کوهسار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بهاری باد گیتی گشت چون خلد برین</p></div>
<div class="m2"><p>گوئی از خلد برین آید همی باد بهار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سرشگ ابر لاله کرده پر لؤلؤ دهان</p></div>
<div class="m2"><p>وز نسیم باد سوسن کرد پر عنبر کنار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بنفشه مرزها گسترده دیبای بنفش</p></div>
<div class="m2"><p>وز شکوفه شاخها بر بسته در شاهوار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم بگشاده است نرگس همچو چشم نیکوان</p></div>
<div class="m2"><p>از شجر بیرون شود مانند یاقوت از حجار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زیر شاخ سرخ لاله زرد شاخ شنبلید</p></div>
<div class="m2"><p>این بروی دوست مانند آن بروی دوستار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پای برده برگ نسرین زیر شاخ شنبلید</p></div>
<div class="m2"><p>قطره شب بر شنبلید افتاده ز ابر تندبار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن یکی زر عیار است از بر سیم حلال</p></div>
<div class="m2"><p>این یکی سیم حلالست از بر زر عیار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با نگار خویشتن رفتم بباغ خویشتن</p></div>
<div class="m2"><p>باغ را دیدم بسان جنت پروردگار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با هوای اوست گویی هرچه در گیتی نسیم</p></div>
<div class="m2"><p>بر زمین اوست گوئی هرچه در عالم بهار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن درختان اندر او مانند حوران بهشت</p></div>
<div class="m2"><p>از زمرد جامه وز یاقوت و مرجان گوشوار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از میان جوی آن آبی روان همچون گلاب</p></div>
<div class="m2"><p>شاخهای گل شکفته بر کنار جویبار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بود هرجا بهر نزهتگاه بان و نقل و می</p></div>
<div class="m2"><p>گلستان در گلستان و میوه اندر میوه زار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یار من گفتا بهشت است ای شگفت ای باغ نیست</p></div>
<div class="m2"><p>گفتمش باغیست خرم چون بهشت کردگار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>این بهشتی بر زمینست آن بهشتی بر سپهر</p></div>
<div class="m2"><p>این بنقد است آن به نسیه آن نهان این آشکار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن مکافات نماز است این مکافات مدیح</p></div>
<div class="m2"><p>آن عطای کردگار است این عطای شهریار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اختیار دهر ابومنصور وهسودان که هست</p></div>
<div class="m2"><p>بندگانش را بمیران جهان بر افتخار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دست و تیغش آب و آتش مهر و کینش خیر و شر</p></div>
<div class="m2"><p>امن و بیمش دار و منبر مهر و خشمش فخر و عار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نیک خواهانش بلند و بدسگالانش بلند</p></div>
<div class="m2"><p>نیکخواهانش بتخت و بدسگالانش بدار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عالمش زیر رکاب است و فلک زیر نگین</p></div>
<div class="m2"><p>آفتابش زیر دست است و زمانه پیشکار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>روزگار خلق پاک از روزگار وی خوش است</p></div>
<div class="m2"><p>تا جهان باشد بماناد این خجسته روزگار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>انتظار او براه سائلان باشد مدام</p></div>
<div class="m2"><p>سائلان با جود او هرگز ندارند انتظار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اختیار روزگار و افتخار عالم است</p></div>
<div class="m2"><p>از همه عالم وفا و جود کرده اختیار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پیش یزدان خلق را بسیار باید ایستاد</p></div>
<div class="m2"><p>گر کند یزدان شمار جود او روز شمار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دوستانش را برون آید ز سنگ خاره گل</p></div>
<div class="m2"><p>دشمنانش را برون آید ز برگ لاله خار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>روز کوشیدن زمین از دست او گردد تهی</p></div>
<div class="m2"><p>روز بخشیدن زمان از دست او خواهد فرار کذا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خلد بنماید موالی را بروز بزم و لهو</p></div>
<div class="m2"><p>حشر بنماید معادی را بروز کارزار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای امیر نامدار شکر جوی و مدح جوی</p></div>
<div class="m2"><p>ای خداوند کریم و حق شناس و حق گذار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چون ز شهر خویش رفتم شد عقار از من جدا</p></div>
<div class="m2"><p>هرکسی گفتا که رفت از تو عقار و هم وقار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر عقار از من بشد دارم خداوندی چو تو</p></div>
<div class="m2"><p>کم ببخشیدی ببیتی شعر ده چندان عقار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دوستانم را تو کردی شادمان و تندرست</p></div>
<div class="m2"><p>دشمنانم را تو کردی دردمند و سوگوار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر هزارانم دهان در هر یکی سیصد زبان</p></div>
<div class="m2"><p>شکر نیکیهات نتوانم یکی گفت از هزار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تا بهنگام بهار آرد درختی تازه ورد</p></div>
<div class="m2"><p>تا بهنگام خزان آرد درخت نار بار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>روی خویشان تو باد از می بسان تازه ورد</p></div>
<div class="m2"><p>روی خصمان تو بادا ز غم بسان کفته نار</p></div></div>