---
title: >-
    شمارهٔ ۱۷۴ - در مدح ابونصر مملان
---
# شمارهٔ ۱۷۴ - در مدح ابونصر مملان

<div class="b" id="bn1"><div class="m1"><p>منم غلام خداوند زلف غالیه گون</p></div>
<div class="m2"><p>که هست چون دل من زلف او نوان و نگون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز خون و تف همه روزه دو دیده و دل من</p></div>
<div class="m2"><p>یکی به آذر ماند یکی بآذریون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز تاب ماند جانم بآذر برزین</p></div>
<div class="m2"><p>ز آب ماند چشمم برود آبسکون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چگونه یابد جان من اندر آتش هال</p></div>
<div class="m2"><p>چگونه یابد جسمم در آب دیده سکون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همی ندانم در هجر چند باشم چند</p></div>
<div class="m2"><p>همی ندانم کز دوست چون شکیبم چون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هواش دارد جان مرا قرین هوا</p></div>
<div class="m2"><p>جفاش دارد جان مرا قرین جنون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بس کزین دل پرسوز من برآید دود</p></div>
<div class="m2"><p>ز بس دو دیده بیخواب من ببارد خون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز خون دیده من رست لاله در صحرا</p></div>
<div class="m2"><p>ز تف دود دلم خاست ابر بر گردون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فروغ لاله چو عذرا بجلوه وامق</p></div>
<div class="m2"><p>خروش ابر چو لیلی بگریه مجنون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز خاک شوره برآورد بوی باد شمال</p></div>
<div class="m2"><p>ز سنگ خاره عیان کرد اشگ ابر عیون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سمن بلرزد همچون پری گرفته ز ماه</p></div>
<div class="m2"><p>بدو کند چو پری سای عندلیب افسون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شقاق غالیه گونست و نیست غالیه بوی</p></div>
<div class="m2"><p>شکوفه غالیه بویست و نیست غالیه گون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز باد خاک معنبر بعنبر سارا</p></div>
<div class="m2"><p>ز ابر شاخ مکلل بلؤلؤ مکنون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز سنگ خارا پیدا همی شود مینا</p></div>
<div class="m2"><p>ز روی مینا مرجان همی دمد بیرون</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شکوفه ریخته از باد در بنفشه ستان</p></div>
<div class="m2"><p>چنانکه تافته لؤلؤی از براکسون</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر آنچه بست میان ارم بهم شداد</p></div>
<div class="m2"><p>هر آنچه کرد بزیر زمین نهان قارون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سرشگ ابر پراکنده کرد در بستان</p></div>
<div class="m2"><p>نسیم باد پدیدار کرد در هامون</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همی بلرزد شاخ رزان ز باد بهار</p></div>
<div class="m2"><p>چو جسم خصم ز تیغ امیر روزافزون</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مکان نصرت و اقبال میر ابونصر آن</p></div>
<div class="m2"><p>که هست طالع او جفت طالع میمون</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زبان مهتر و کهتر بمدح او گویا</p></div>
<div class="m2"><p>روان عاقل و جاهل بمهر او مرهون</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بطبع ز انسان بر خواستار مفتونست</p></div>
<div class="m2"><p>که سفله باشد بر گنج خواسته مفتون</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عدوش دائم مسجون بود بدرد و بلا</p></div>
<div class="m2"><p>درم نباشد روزی بنزد او مسجون</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یکی عطاش همه گنجهای اسکندر</p></div>
<div class="m2"><p>یکی سخنش همه علمهای افلاطون</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز دست او شده لؤلؤ ببحر متواری</p></div>
<div class="m2"><p>ز تیغ او شده آهن بسنگ در مدفون</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ستون دانش و دینی و از نهیب تو هست</p></div>
<div class="m2"><p>همیشه زیر ز نخ دست دشمنانت ستون</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر آنچه قارون می کرد زیر خاک اندر</p></div>
<div class="m2"><p>بسان خاک همی بر پراکنی تو کنون</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بود روان عدوی تو با عذاب عدیل</p></div>
<div class="m2"><p>بود روان ولی تو با طرب مقرون</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نکرد هیچکس اندر جهان ترا دستان</p></div>
<div class="m2"><p>نکرد هیچکس اندر جهان ترا مفتون</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اگر ببادیه از دست تو کنند حدیث</p></div>
<div class="m2"><p>و گر ز تیغ تو افتد خیال در جیحون</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بسان گردون آنجا روان شود کشتی</p></div>
<div class="m2"><p>بسان کشتی آنجا روان شود گردون</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دهان بمدح تو گردد بگو هر آگنده</p></div>
<div class="m2"><p>زبان بمدح تو گردد بغالیه معجون</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همیشه تا مه نیسان به آید از تشرین</p></div>
<div class="m2"><p>همیشه تا مه تشرین خوش آید از کانون</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خجسته بادت نوروز و روزه و هموار</p></div>
<div class="m2"><p>هزار روزه و نوروز بگذران ایدون</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>یکی بتوبه و طاعت بعهد پیغمبر</p></div>
<div class="m2"><p>یکی برامش و رادی برسم افریدون</p></div></div>