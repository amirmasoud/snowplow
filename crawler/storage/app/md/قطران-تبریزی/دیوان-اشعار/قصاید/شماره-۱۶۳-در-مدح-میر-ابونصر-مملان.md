---
title: >-
    شمارهٔ ۱۶۳ - در مدح میر ابونصر مملان
---
# شمارهٔ ۱۶۳ - در مدح میر ابونصر مملان

<div class="b" id="bn1"><div class="m1"><p>شد برگ رزان زرد ز آذر مه و آبان</p></div>
<div class="m2"><p>شد آب رزان سرخ چو بیجاده تابان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیدار رزان زرد شد و آب رزان سرخ</p></div>
<div class="m2"><p>حکمی که خداوند کند هست صواب آن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر آب ببرد از گل و گلزار مه مهر</p></div>
<div class="m2"><p>پائیز بیاراست بآئین رز آبان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا زاغ بیابانی در باغ وطن ساخت</p></div>
<div class="m2"><p>شد بلبل خوشبانگ سوی کوه و بیابان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیدار شده نرگس و نارنگ ولیکن</p></div>
<div class="m2"><p>در خواب گران رفته گل و لاله خندان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن هر دو بدیدار چو اشگ و رخ عاشق</p></div>
<div class="m2"><p>وین هر دو بدیدار چو روی و لب جانان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا سیب بکردار زنخدان بتان شد</p></div>
<div class="m2"><p>بفزود مرا مهر بت سیم زنخدان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا ابر بکافور بپوشید سر کوه</p></div>
<div class="m2"><p>از باد بدینار بیاراست گلستان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن حور زره پوش و بت سیم بناگوش</p></div>
<div class="m2"><p>آن سرو خرامنده و خورشید درخشان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از مشگ فرو هشته بخورشید دو زنجیر</p></div>
<div class="m2"><p>وز غالیه پیوسته بگلنار دو چوگان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نقش لب و دندانش بچین گر بنگارند</p></div>
<div class="m2"><p>گردد چو دلم خون لب فغفور بدندان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ترسم که همی بکسلد از ایمان ز دل من</p></div>
<div class="m2"><p>تا بر رخ او کفر ظفر یافت بایمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>او را بخریدم بتن و هست به از دل</p></div>
<div class="m2"><p>او را بگزیدم بدل و هست به از جان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جان و دل من هست سزاوار بدان بت</p></div>
<div class="m2"><p>چون ملک جهان هست سزاوار بمملان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خورشید همه میران بونصر که بسپرد</p></div>
<div class="m2"><p>یزدان بوی و دشمن وی نصرت و خذلان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر نعمت نعمان بیکی زائر بخشد</p></div>
<div class="m2"><p>بر وی ننهد منت یک لاله نعمان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از هیبت او سندان بگدازد چون موم</p></div>
<div class="m2"><p>با دولت او گل شکفد بر سر سندان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فارغ نشود درگهش از سائل و زائر</p></div>
<div class="m2"><p>خالی نبود مجلسش از مطرب و مهمان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از بهر همه پاک گشاده است دل و دوست</p></div>
<div class="m2"><p>وز بهر همه پاک نهاده است می و خوان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آنکس که یکی روز بداندیش تو باشد</p></div>
<div class="m2"><p>از کرده خود باشد تا حشر پشیمان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کز هول تو بی درد دلش باشد بیمار</p></div>
<div class="m2"><p>وز بیم تو بی بند بود تنش بزندان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پیمانه آنکس بیقین پر شده باشد</p></div>
<div class="m2"><p>کو با تو نیارد بسر وعده و پیمان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>روی تو بدل بس بود امروز جهان را</p></div>
<div class="m2"><p>شاید که مه و مهر نتابد ز خراسان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>روز و شب از آنست نگهبان وی ایزد</p></div>
<div class="m2"><p>کوهست جهانرا بشب و روز نگهبان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا زرد کند باد خزان برگ رزان را</p></div>
<div class="m2"><p>تا سرخ کند گل را باران ببهاران</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون برگ رزان خصم تو از باد خزان زرد</p></div>
<div class="m2"><p>روی تو چو گل باد ز می سرخ بباران</p></div></div>