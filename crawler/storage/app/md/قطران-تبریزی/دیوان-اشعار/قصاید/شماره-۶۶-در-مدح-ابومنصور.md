---
title: >-
    شمارهٔ ۶۶ - در مدح ابومنصور
---
# شمارهٔ ۶۶ - در مدح ابومنصور

<div class="b" id="bn1"><div class="m1"><p>بلای غربت و تیمار عشق و فرقت یار</p></div>
<div class="m2"><p>شدند با من دلخسته این سه آفت یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همیشه بود نشاط دلم ز دیدن دوست</p></div>
<div class="m2"><p>همیشه بود قرار تنم بصحبت یار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برفت یار و مرا غم گرفت جای نشاط</p></div>
<div class="m2"><p>برفت یار و مراتب گرفت جای قرار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پری ندیدم و همچون پری گرفته شدم</p></div>
<div class="m2"><p>ز درد فرقت آن لعبت پری دیدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بشب ز حسرت آن روی چون ستاره روز</p></div>
<div class="m2"><p>ستاره بار دو چشمم بود ستاره شمار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا بزاری گوید چکارت آمد پیش</p></div>
<div class="m2"><p>هر آن کسیکه ببیند که من بگریم زار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز دوست دورم ازین زارتر چه باشد حال</p></div>
<div class="m2"><p>ز یار فردم ازین صعبتر چه باشد کار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میان آتش و آب اندرون گرفتارم</p></div>
<div class="m2"><p>که جانم آتش کانست و دیده دریابار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز بهر آن رخ رنگین چو نقش بر دیبا</p></div>
<div class="m2"><p>بمانده ام متحیر چو نقش بر دیوار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گمان بری که دو رخسار او نیافته باز</p></div>
<div class="m2"><p>سرشگ دیده همی باز کردم از رخسار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز آب دیده ندیدم کنار خویش تهی</p></div>
<div class="m2"><p>از آن گهی که ز من آن بتم گرفت کنار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همی ندانم چاره فراق و نیست عجب</p></div>
<div class="m2"><p>که هیچ عاقل خود کرده را نداند چار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یکی زمان ز دلم عاشقی جدا نشود</p></div>
<div class="m2"><p>چنانکه مردمی از طبع شاه گیتی دار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خدایگان جهان شهریار ابومنصور</p></div>
<div class="m2"><p>که اختیار ملوکست و افتخار تبار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برزم شهرگشا و بفکر دشمن بند</p></div>
<div class="m2"><p>بتیغ ملک ستان و بدست ملک سپار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بروز رزم بگرید ز دست او شمشیر</p></div>
<div class="m2"><p>بروز بزم بگرید ز دست او دینار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شمر خلق و شمار زمین اگر داند</p></div>
<div class="m2"><p>بروز خواسته دادن نداند ایچ شمار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همه سخاوت بینیش بر یسار و یمین</p></div>
<div class="m2"><p>همه شجاعت بینیش بر یمین و یسار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شب مخالف او را نکرده گردون روز</p></div>
<div class="m2"><p>گل موافق او را ندیده گیتی خار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نه اسب و جامه بنزدیک او گرفت درنگ</p></div>
<div class="m2"><p>نه زر و سیم بنزدیک او گرفت قرار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>درست گوئی کز اسب و جامه دارد ننگ</p></div>
<div class="m2"><p>درست گوئی کز زر و سیم دارد عار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>موافقانش بلندند لیک بر سر تخت</p></div>
<div class="m2"><p>مخالفانش بلندند لیک بر سر دار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بروی جور بر آورد عدل او شمشیر</p></div>
<div class="m2"><p>بچشم بخل فرو کرد جود او مسمار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ایا حسام تو هنگام صید شیر شکر</p></div>
<div class="m2"><p>ایا سنان تو هنگام رزم شیر شکار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گهی شکار طرازی گهی مصاف افروز</p></div>
<div class="m2"><p>مگر ز بهر تو کرد آسمان مصاف و شکار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نه دشمنان را با تیغ تو بود امید</p></div>
<div class="m2"><p>نه آهوانرا با یوز تو بود زنهار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همیشه تا بود اندر میان نار شعاع</p></div>
<div class="m2"><p>همیشه تا بود اندر میان خاک غبار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>غبار باد نصیب مخالفانت ز خاک</p></div>
<div class="m2"><p>شعاع باد نصیب موافقانت ز نار</p></div></div>