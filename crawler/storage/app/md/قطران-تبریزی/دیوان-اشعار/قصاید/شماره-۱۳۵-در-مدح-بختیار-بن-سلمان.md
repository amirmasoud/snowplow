---
title: >-
    شمارهٔ ۱۳۵ - در مدح بختیار بن سلمان
---
# شمارهٔ ۱۳۵ - در مدح بختیار بن سلمان

<div class="b" id="bn1"><div class="m1"><p>ای ببالا بلای آزادان</p></div>
<div class="m2"><p>آرزوی دلی و رنج روان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تنم از عشق تو نوان و نزار</p></div>
<div class="m2"><p>دلم از رنج تو نژند و نوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آرزوی جوان و پیری تو</p></div>
<div class="m2"><p>وز تو دائم بدرد پیر و جوان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین بتنبل همی ستانی دل</p></div>
<div class="m2"><p>زان بدستان همی ستانی جان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نکند بر تو کس روا تنبل</p></div>
<div class="m2"><p>نکند بر تو کس روا دستان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل من خسته زان نهفته دهن</p></div>
<div class="m2"><p>تن من زار زان نزار میان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر آن زلف کینه خواه و سیاه</p></div>
<div class="m2"><p>هر زمان اندر آوری بدهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن یکیرا بسان غالیه دان</p></div>
<div class="m2"><p>وین یکیرا بسان غالیه دان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر نه عاشق تر از منست چرا</p></div>
<div class="m2"><p>بر دهان تو هست بوسه زنان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تن من هست اسیر آن زنجیر</p></div>
<div class="m2"><p>دل من هست گوی آن چوگان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در نوشته بساط صحبت من</p></div>
<div class="m2"><p>چون زمستان بساط تابستان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا زمستان بساط گستر شد</p></div>
<div class="m2"><p>شد زمین و زمان بدیگر سان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون رخ من شده است رنگ زمین</p></div>
<div class="m2"><p>چون دم من شده است طبع زمان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باغ برکند پرنیان و پرند</p></div>
<div class="m2"><p>کوه پوشید توزی و کتان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گشت صحرا تهی ز لشگر روم</p></div>
<div class="m2"><p>گشت پر لشگر حبش بستان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دشت پوشیده چادر ترسا</p></div>
<div class="m2"><p>چرخ پوشیده جامه رهبان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا سردشت و کوه سیمین گشت</p></div>
<div class="m2"><p>باد دیماه گشت چون سوهان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>لاجرم در میان سونش سیم</p></div>
<div class="m2"><p>دامن کوهسار گشت نهان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بوستان پر سیاه پوشان گشت</p></div>
<div class="m2"><p>تا بر او گشت ماه دی سلطان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای بدل همچو قبله تازی</p></div>
<div class="m2"><p>خیز و بفروز قبله دهقان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>باده پیش آر و پیش من بنشین</p></div>
<div class="m2"><p>شاخ بیجاده پیش من بنشان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون جنان خانه زان و آن چو سقر</p></div>
<div class="m2"><p>چون سقر طبع از این و آن چو جنان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای پدید آرد از ترنج عقیق</p></div>
<div class="m2"><p>وآن برون آرد از شجر مرجان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آن یکی آب رنگ و خواب فزای</p></div>
<div class="m2"><p>این یکی زر خام و سیم فشان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سر دیوانه زان شود هشیار</p></div>
<div class="m2"><p>دل غمناک از این شود شادان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آن بسرخی دهد ز یار خبر</p></div>
<div class="m2"><p>این بزردی دهد ز رنج نشان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آن یکی نار وصف و رنج شکن</p></div>
<div class="m2"><p>این یکی رنج تف و نارنشان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>این دماند ز روی سندان گل</p></div>
<div class="m2"><p>آن گدازد ز تف خود سندان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هرکه این خورد پرورید روان</p></div>
<div class="m2"><p>پرورانیده را همه خورد آن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آن یکی یادگار افریدون</p></div>
<div class="m2"><p>وین یکی دستگاه نوشیروان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گشته مشگین ز بوی آن مجلس</p></div>
<div class="m2"><p>گشته رنگین ز رنگ این ایوان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آن چو خوی پناه ملک امیر</p></div>
<div class="m2"><p>این چو دست امیر خلق جهان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>صاحب نیکبخت عالی تخت</p></div>
<div class="m2"><p>بوالعلی بختیاربن سلمان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آن وفا را تن و سخا را دل</p></div>
<div class="m2"><p>آن خرد را مکان و تنرا جان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خازنش نیست خالی از بخشش</p></div>
<div class="m2"><p>مجلسش نیست خالی از مهمان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سیم دائم ز کف او بگله</p></div>
<div class="m2"><p>زر دائم ز دست او بفغان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>زائر از کف او شود خوشنود</p></div>
<div class="m2"><p>شاعر از کلک او شود خندان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هرکه زو یک حدیث نیک شنید</p></div>
<div class="m2"><p>جاودان گشت رسته از حدثان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گنج شادی از او شده گنجه</p></div>
<div class="m2"><p>شمع شادی از او شده شروان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>فضل گرد دلش کند پرواز</p></div>
<div class="m2"><p>جود گرد کفش کند طیران</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>آن گران سنگ و حلمهاش سبک</p></div>
<div class="m2"><p>وین سبک سنگ و حملهاش گران</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>قسمت نیکخواه از او نصرت</p></div>
<div class="m2"><p>بهره بدسگال از او خذلان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مرگ بر دشمنش گشاده کمین</p></div>
<div class="m2"><p>چرخ بر حاسدش کشیده کمان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>فلک فضل را دلش خورشید</p></div>
<div class="m2"><p>نامه جود را کفش عنوان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کلک او را قضا برد طاعت</p></div>
<div class="m2"><p>تیغ او را اجل کشد فرمان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>آن یکی نار فعل و آب صفت</p></div>
<div class="m2"><p>وین یکی آب طبع و نارنشان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گوهر این چو ذره خورشید</p></div>
<div class="m2"><p>صورت آن چو عشق در هجران</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>اول اینرا ز خاک بد بالین</p></div>
<div class="m2"><p>اول آن را ز سنگ بدبنیان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>آن یکی دست جود را انگشت</p></div>
<div class="m2"><p>وین یکی کام حرب را دندان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>آن یکی رزق را همیشه مقام</p></div>
<div class="m2"><p>وین یکی مرگ را همیشه مکان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ای ز کلک تو فضل را شادی</p></div>
<div class="m2"><p>وی ز تیغ تو خصم را احزان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دیده فضل را توئی دیدار</p></div>
<div class="m2"><p>خانه جود را توئی بنیان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ناز دشمن کنی بوهم نیاز</p></div>
<div class="m2"><p>سود حاسد کنی بعزم زیان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تو برادی همی دهی روزی</p></div>
<div class="m2"><p>هرکه را جان همی دهد یزدان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>جان خلقی که نان خلق ز تست</p></div>
<div class="m2"><p>جان نباشد کرا نباشد نان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>دست ادبار از آن شده بسته</p></div>
<div class="m2"><p>که بمدح تو برگشاده زبان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>گر کنی با عدو بعزم بدی</p></div>
<div class="m2"><p>گر کنی با ولی بوهم احسان</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>سنگ در دست این شود یاقوت</p></div>
<div class="m2"><p>مژه در چشم آن شود پیکان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>زآتش و آب و باد و خاک کند</p></div>
<div class="m2"><p>چرخ طوفان پدید در کیهان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>تا ندیدم ترا ندانستم</p></div>
<div class="m2"><p>که ز زر و درم بود طوفان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>تا به اران توئی مدار عجب</p></div>
<div class="m2"><p>که به اران حسد برد ایران</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>تا نباشد گمان بسان یقین</p></div>
<div class="m2"><p>تا نباشد خبر بسان عیان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>تو عیان باش و دشمن تو خبر</p></div>
<div class="m2"><p>تو یقین باش و حاسد تو گمان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>دوستان ترا بود شادی</p></div>
<div class="m2"><p>دشمنان ترا بود خذلان</p></div></div>