---
title: >-
    شمارهٔ ۲۱۰ - در مدح ابونصر مملان
---
# شمارهٔ ۲۱۰ - در مدح ابونصر مملان

<div class="b" id="bn1"><div class="m1"><p>ندانی درد هجر ای گل مرا زان زار گردانی</p></div>
<div class="m2"><p>دگر زارم نگردانی بداغ هجر گردانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر یکره چو من بیدل بعشق اندر فرو مانی</p></div>
<div class="m2"><p>ز خون عاشقان خوردن بسی یابی پشیمانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه رنج دل و جسمی همه درد تن و جانی</p></div>
<div class="m2"><p>بسوزانی و گریانی و رنجانی و پیچانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آن چون زر شده رویم که تو سیمین زنخدانی</p></div>
<div class="m2"><p>از آن چون لعل شد اشگم که مروارید دندانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو ماهی سرو را مانی تو سروی ماه را مانی</p></div>
<div class="m2"><p>که ماه سرو بالائی و سرو ماه پیشانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بمهر آن لبم کردی سرشک دیده مرجانی</p></div>
<div class="m2"><p>بروشن روی روز من شب تاریک گردانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا رخسار زرین کرد تف نار هجرانی</p></div>
<div class="m2"><p>که سیمین کرد هامون را دم تیغ زمستانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شده کهسار کافوری و آب رود سندانی</p></div>
<div class="m2"><p>در آب از بند دیماه است ماهی گشته زندانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دمنده خلق در خانه فسرده چشمه چون خانی</p></div>
<div class="m2"><p>بسان سونش سیم است برف از باد سوهانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیابانها گرفته بلبل خوش بانگ بستانی</p></div>
<div class="m2"><p>ببستان اندر آمد باز آن زاغ بیابانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو بر تو برف بارد باد بر تن باده بارانی</p></div>
<div class="m2"><p>که باران زمستان را چو باده نیست بارانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز زر خام پیش خویش گوئی بر فروزانی</p></div>
<div class="m2"><p>چو بر بالا دل عاشق بسوزانی و لرزانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از آن ایوان بیاراید چو مجمرهای گردانی</p></div>
<div class="m2"><p>وزین گردون بیفروزد چو گوهرهای عمانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گهی زو رودها بینی پر از یاقوت رمانی</p></div>
<div class="m2"><p>گهی زو کوهها بینی پر از لعل بدخشانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شود باز آسمان یکسر پر از دیبای کاشانی</p></div>
<div class="m2"><p>همه دینارها گردد درمهای سپاهانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ایا ابر زمستانی نه چون ابر بهارانی</p></div>
<div class="m2"><p>مکن چندین میان کوه و باغ و راغ ویرانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که نه آثار طوفانی و نه بنیاد سیلانی</p></div>
<div class="m2"><p>نه موج بحر عمانی نه کف میر مملانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ابو نصر آنکه یزدانش بنصرت داد ارزانی</p></div>
<div class="m2"><p>از او مدحت گرانی یافت وزوی گوهر ارزانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>فکنده فر یزدانی بر او دیدار سلطانی</p></div>
<div class="m2"><p>فری دیدار سلطانی که دارد فر یزدانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ایا میری که از رادی سر میران ارانی</p></div>
<div class="m2"><p>دلیل سعد گردونی نشان وعد قرآنی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ولی را سعد برجیسی عدو را نحس کیوانی</p></div>
<div class="m2"><p>بمیدان شیر میدانی در ایوان ماه ایوانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو درد آز و سختی را بکف راد درمانی</p></div>
<div class="m2"><p>بفرمان تو شد عالم گه یزدان را بفرمانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اگر شیطان شود یارت دهد یزدانش رضوانی</p></div>
<div class="m2"><p>و گر رضوان شود خصمت دهد یزدانش شیطانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بقول آسایش جسمی بعقل آرایش جانی</p></div>
<div class="m2"><p>کهان را از تو آرایش مهان را از تو آسانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اگر نه موج دریایی و گرنه سیل نیسانی</p></div>
<div class="m2"><p>چرا با دوست و با دشمن بگاه جود یکسانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ایا پوشیده از هر عیب از هر عیب عریانی</p></div>
<div class="m2"><p>چو در مجلس شوی خندان دو صد کان را بگریانی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مگر پیغمبر روزی ز هرکس داد بستانی</p></div>
<div class="m2"><p>که یک سر مظهر تأیید و فر فضل یزدانی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یکی دهقان بدم شاها شدم شاعر بنادانی</p></div>
<div class="m2"><p>مرا از شاعری کردن تو گرداندی بدهقانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بجای تو که با هر شاه هم صنفی و همخوانی</p></div>
<div class="m2"><p>بسا کس مهترم خوانند تا تو کهترم خوانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>حسودانم فراوانند و بدگویان ز نادانی</p></div>
<div class="m2"><p>ز بس کم خواسته پاشی ز بس کم پیش بنشانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>فراوان دادیم نعمت حسودان شد فراوانی</p></div>
<div class="m2"><p>تو کردی بر من این بیدادگر نه از چه سان دانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>الا تا هست اندر عالم افزونی و نقصانی</p></div>
<div class="m2"><p>الا تا هست شادانی و غمگینی و پژمانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ترا باد ابر افزونی ترا دل باد شادانی</p></div>
<div class="m2"><p>عدو را باد غمگینی و جان و تن به نقصانی</p></div></div>