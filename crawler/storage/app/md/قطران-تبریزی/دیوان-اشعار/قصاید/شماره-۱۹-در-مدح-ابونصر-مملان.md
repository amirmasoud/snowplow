---
title: >-
    شمارهٔ ۱۹ - در مدح ابونصر مملان
---
# شمارهٔ ۱۹ - در مدح ابونصر مملان

<div class="b" id="bn1"><div class="m1"><p>ز پی آفت هر چیز پدید است سبب</p></div>
<div class="m2"><p>سبب آفت من فرقت آن سیم غبب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر سوی دیده من خواب نیاید نه شگفت</p></div>
<div class="m2"><p>ور طرب سوی دل من نگراید نه عجب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کاندر آن بستد اندوه و غمش معدن خواب</p></div>
<div class="m2"><p>وندرین در برگرفت انده او جای طرب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل من غافل بی آنگه مرا گیرد خواب</p></div>
<div class="m2"><p>تن من لرزان بی آنکه مرا گیرد تب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من ز نادیدن آن ماه بر آن کوبم سر</p></div>
<div class="m2"><p>من در اندیشه آن حور بدان خایم لب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که یکی بار دل او طلب من نکند</p></div>
<div class="m2"><p>که دلم باشد صد بار ورا کرده طلب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای بناکام من و خویشتن از من شده دور</p></div>
<div class="m2"><p>خویشتن را و مرا کرده گرفتار تعب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سبب شادی و غم چشم و لب تست چو هست</p></div>
<div class="m2"><p>مرگ و روزیرا شمشیر و کف میر سبب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میر ابو نصر محمد که خداوند جهان</p></div>
<div class="m2"><p>بگزیدش ز جهان هم بحسب هم بنسب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نسبش از عجم و قدوه شاهان عجم</p></div>
<div class="m2"><p>حسبش از عرب و قبله میران عرب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل او بحری موجش همه دینار و درم</p></div>
<div class="m2"><p>کف او ابری سیلش همه دیبا و قصب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آفت و راحت خلق است به شمشیر و قلم</p></div>
<div class="m2"><p>که ز پولاد بود آفت و راحت ز قصب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کف او ابر گهر بار بود وقت سخا</p></div>
<div class="m2"><p>تیغ او شیر روان خوار بود گاه غضب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از پی آنکه ذهب خوار بود بر دل او</p></div>
<div class="m2"><p>روی خصمانش بود زرد همیشه چو ذهب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مهر او مهر درخشنده و خواهنده نهال</p></div>
<div class="m2"><p>کین او آتش سوزنده و بدخواه حطب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای بشیرینی چون جان و بخوشی چو جهان</p></div>
<div class="m2"><p>وی پسندیده چو تدبیر و ستوده چو ادب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر کند بولهب از مهر تو در دوزخ یاد</p></div>
<div class="m2"><p>برهد جان و تن بولهب از نار لهب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ور بکوه اندر عاصی شود اندر تو پلنگ</p></div>
<div class="m2"><p>میش با فر تو بیرون برد از تنش عصب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>لقب و نام یکی دارد هر میر و ترا</p></div>
<div class="m2"><p>بکریمی و وفا هست دو صد نام و لقب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خلق در امن تو همواره تو در امن خدا</p></div>
<div class="m2"><p>خلق در طاعت تو پاک و تو در طاعت رب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بنشین خرم و خندان و مهان را بنشان</p></div>
<div class="m2"><p>بستان از کف عناب لبان آب عنب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا شب و روز همی آید پیدا ز فلک</p></div>
<div class="m2"><p>تا گل و خار همی آید پیدا ز خشب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>باد بر ناصح تو خار ببویائی گل</p></div>
<div class="m2"><p>باد بر حاسد تو روز بتاریکی شب</p></div></div>