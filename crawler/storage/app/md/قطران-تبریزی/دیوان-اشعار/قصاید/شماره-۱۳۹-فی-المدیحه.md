---
title: >-
    شمارهٔ ۱۳۹ - فی المدیحه
---
# شمارهٔ ۱۳۹ - فی المدیحه

<div class="b" id="bn1"><div class="m1"><p>ایکام دل دوست و بلای دل دشمن</p></div>
<div class="m2"><p>روزه شد و دیمه شد و عید آمد و بهمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رسم اندر پیغمبر و بهمن تو بجای آر</p></div>
<div class="m2"><p>هم سیرت پیغمبر و هم سیرت بهمن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر سیرت آن هستی و بر کرده او نیز</p></div>
<div class="m2"><p>بر سیرت این باش و بر آن کرده همی تن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خصم میندیش و درافکن بقدح می</p></div>
<div class="m2"><p>وز می برخان رنگ گلسرخ بیفکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از شادی و از سور مپرداز بکاری</p></div>
<div class="m2"><p>تا آنکه بپردازد بدخواه بشیون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گردون ز زمین دور کند گردن آن پست</p></div>
<div class="m2"><p>کز کام و هوای تو بگرداند گردن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنکس که ز دل خرمن تو سوخته خواهد</p></div>
<div class="m2"><p>هم سوخته دل گردد و هم سوخته خرمن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی کام تو یک مرد خراسان بقضا شد</p></div>
<div class="m2"><p>یکره نتوانست گشاد از همه ارمن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با کام تو صد مرد خراسانی هر سال</p></div>
<div class="m2"><p>دراعه بکردند پی فتح ملون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدخواه تو فن دارد و تو فر خداوند</p></div>
<div class="m2"><p>با فر خداوند فنا زاید از آن فن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خواهد که عدو از تو برد سود بچاره</p></div>
<div class="m2"><p>کی کوه هماون بتوان سود بهاون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نتوان ستد از شیر بروباه نیستان</p></div>
<div class="m2"><p>نتوان ستد از باز بدراج نشیمن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شاها بمثل دولت تو زرین جام است</p></div>
<div class="m2"><p>جامی است بلورین بمثل دولت دشمن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون بشکند آن زرگر از آن به کندش باز</p></div>
<div class="m2"><p>چون بشکند این دیوش نتواند بستن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دولت بتو آرام کند ملک بدولت</p></div>
<div class="m2"><p>همچون بخرد جان کند آرام و بجان تن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چونانکه ز گلشن تو سوی میدان آئی</p></div>
<div class="m2"><p>از میدان دشمنت نیاید سوی گلشن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر خصم تو آن چهره رخشانت ببیند</p></div>
<div class="m2"><p>بر دیده او تیره شود عالم روشن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>با مهر تو گردد بمثل ارزن چون کوه</p></div>
<div class="m2"><p>با کین تو گردد بمثل کوه چو ارزن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نز خصم تو فتح آید و نز حاسد تو سعد</p></div>
<div class="m2"><p>نی مرده شود زنده و نی مرد شود زن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چندانت بقا باد بشاهی و بشادی</p></div>
<div class="m2"><p>کاتش نشود آب و نگردد شبه آهن</p></div></div>