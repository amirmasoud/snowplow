---
title: >-
    شمارهٔ ۱۵ - در مدح ابوالمظفر سرخاب
---
# شمارهٔ ۱۵ - در مدح ابوالمظفر سرخاب

<div class="b" id="bn1"><div class="m1"><p>شده است بلبل داود و شاخ گل محراب</p></div>
<div class="m2"><p>فکنده فاخته بر رود و ساخته مضراب؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی سرود سراینده از ستاک سمن</p></div>
<div class="m2"><p>یکی زبور روایت کننده از محراب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگر که پردر گردید آبگیر بدانکه</p></div>
<div class="m2"><p>شکن شکن شده آب از شمال چون مضراب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکوفه ریخته از شاخ نار زیر درخت</p></div>
<div class="m2"><p>چنان نبشته درم پیش ریخته ضراب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صبا بساط حواصل ز بوستان بنوشت</p></div>
<div class="m2"><p>چو مشگ بید بپوشید بر سمن سنجاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکفته سرخ و سیه لاله چون رخ و دل و دوست</p></div>
<div class="m2"><p>بنفشه رسته چو زلفین او ببوی و بتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقیق و مرجان با رنگ آن ندارد پای</p></div>
<div class="m2"><p>عبیر و عنبر با بوی این ندارد تاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ببوی و گونه گل هست خاک روی چمن</p></div>
<div class="m2"><p>ببوی عنبر ناب و بگونه گل ناب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکفته گشت بباغ اندرون بنفشه و گل</p></div>
<div class="m2"><p>ببوستان شده آب غدیر همچو گلاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز ژاله لاله چو لؤلؤ شده رفیق عقیق</p></div>
<div class="m2"><p>نوای صلصل و بلبل چو چنک و تار و رباب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خروش رعد بابر اندرون چو ناله دعد</p></div>
<div class="m2"><p>فروغ لاله بجوی اندرون چو روی رباب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز ابر گشته بکردار روی و شی خاک</p></div>
<div class="m2"><p>ز باد گشته بکردار موی زنگی آب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>میان بستان نرگس بیاد میر خطیر</p></div>
<div class="m2"><p>بجام سیمین اندر فکنده زرد شراب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ابوالمظفر سرخاب کو بتیغ کبود</p></div>
<div class="m2"><p>دل سیاه بد اندیش کرده پر ز ذباب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گناه دشمن بگذارد از کرم بعفو</p></div>
<div class="m2"><p>خطای دوست بپوشد ز مرد می بصواب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دهنده سائل خواهنده را هزار عطا</p></div>
<div class="m2"><p>دهنده سائل پرسنده را هزار جواب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بجای قدرش گردون بود بجای سریر</p></div>
<div class="m2"><p>بجای دستش دریا بود بجای سراب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مخالفان را بر تن بود همیشه جرب</p></div>
<div class="m2"><p>موافقان را پر زر از او همیشه جراب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بگیتی اندر داد آنچنان بگسترده است</p></div>
<div class="m2"><p>گه کرده یزدان ایمن روان او ز عقاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنانکه میش کند بچه در نشیمن شیر</p></div>
<div class="m2"><p>چنانکه کبک نهد خایه در کنام عقاب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بداد کرد همه شهرهای شاه آباد</p></div>
<div class="m2"><p>بجود کرد همه گنجهای خویش خراب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چه سنگ باشد با تیغ تیز او چه پرند</p></div>
<div class="m2"><p>چه زر باشد با دست راد او چه تراب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کسی که راست زند دست در کتاب ثناش</p></div>
<div class="m2"><p>بروز حشر دهندش بدست راست کتاب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خمار باشد بهر عدوی او ز نبیذ</p></div>
<div class="m2"><p>چو دود باشد بهر حسود او ز کباب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز تاب تیغش جان عدو بفرساید</p></div>
<div class="m2"><p>چنانکه کتان فرسوده گردد از مهتاب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خدای گوئی از دوستان او برداشت</p></div>
<div class="m2"><p>بدان سرای عذاب و بدین سرای حساب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز طبع حاسد او رتفه از نهیب شکیب</p></div>
<div class="m2"><p>تن مخالف او گشته از عذاب مذاب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همیشه باد دل و طبع این رفیق نهیب</p></div>
<div class="m2"><p>همیشه باد تن و جان آن عدیل عذاب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کسی که طالع گیرد نبرد خصمان را</p></div>
<div class="m2"><p>کند ز روی چو روی مخالفان صلا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بمگر که سوختن آتش ز تیغ او آموخت</p></div>
<div class="m2"><p>چنانکه رادی آموخت از دو کفش آب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چنانکه خاک بحلمش نسب کند بدرنک</p></div>
<div class="m2"><p>چنانکه باد بطبعش نسب کند بشتاب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بکاه ماند بدخواه و خشم او بشمال</p></div>
<div class="m2"><p>بدیو ماند بدساز و خشت او بشهاب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بروی خوبتر است از مه دو هفته بشب</p></div>
<div class="m2"><p>بلفظ نوشتر است از شراب وقت شباب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گه بهار ز خلقش برد نسیم صبا</p></div>
<div class="m2"><p>گه خزان ز نوالش برد سرشک سحاب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چنان خوش آید آواز سائلانش بگوش</p></div>
<div class="m2"><p>که گوش عاشق میخواره را خروش رباب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خراب گردد پیش سنان او خاره</p></div>
<div class="m2"><p>سراب باشد پیش بنان او دریاب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سرای پرده جاه و جلال او را کرد</p></div>
<div class="m2"><p>همی ز مدت گیتی هماره چرخ طناب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همیشه تا تن سیماب و چشمه خورشید</p></div>
<div class="m2"><p>یکی بلرزد و دیگر فروغ دارد و تاب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دل موالی رخشان چو تافته خورشید</p></div>
<div class="m2"><p>تن حسودش لرزان ز بیم چون سیماب</p></div></div>