---
title: >-
    شمارهٔ ۱۵۵ - در مدح امیر ابونصر و تهنیت عید فطر
---
# شمارهٔ ۱۵۵ - در مدح امیر ابونصر و تهنیت عید فطر

<div class="b" id="bn1"><div class="m1"><p>چه دید تشرین گوئی ز نرگس و نسرین</p></div>
<div class="m2"><p>که باغ و بستان بستد زهر دوان تشرین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنار کفته سپرده است معدن نرگس</p></div>
<div class="m2"><p>بسیب رنگین داده است مسکن نسرین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نبرده رنج یکی هست چون دل فرهاد</p></div>
<div class="m2"><p>ندیده ناز یکی هست چون رخ شیرین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بد از بنفشه لب جوی چون نگین کبود</p></div>
<div class="m2"><p>وز او بمشگ همه جویبار بود عجین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کنار جوی تهی ماند از نگین کبود</p></div>
<div class="m2"><p>میان جوی شد از آب چون کبود نگین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو کوهسار بتو زی بداد دیبه روم</p></div>
<div class="m2"><p>چمن بششتری زرد داد دیبه چین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز ناف معشوق آبی گرفته بوی و مثال</p></div>
<div class="m2"><p>ز روی عاشق برده ترنج زردی و چین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درست گوئی کز نار دیده سیب آسیب</p></div>
<div class="m2"><p>درست گوئی با سیب نار دارد کین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز زخم نار رخ سیب گشته خون آلود</p></div>
<div class="m2"><p>ز کین سیب دل نار گشته خون آگین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسیب زرد بر آن نقطه های سرخ نگر</p></div>
<div class="m2"><p>چو اشگ خونین بر روی عاشق غمگین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو زر و نیل شده باغ زرد و آب کبود</p></div>
<div class="m2"><p>چو سیم و سرب شده که سپید و دشت چنین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بسان زرین قندیل بر درخت ترنج</p></div>
<div class="m2"><p>میانش کرده نهان بر فتیله سیمین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فکنده روشنی خویشتن برابر هوا</p></div>
<div class="m2"><p>سپرده تیرگی خویشتن برابر زمین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بکاست روز چو رنج از تن عمیدالملک</p></div>
<div class="m2"><p>فزود شب چو نشاط دل عمادالدین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>امین جان ملوک جهان ابونصر آن</p></div>
<div class="m2"><p>که یمن و یسرش جفتند بر یسار و یمین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نه روز بخشش او دارد ایچ گنج قرار</p></div>
<div class="m2"><p>نه روز کوشش او ماند ایچ حصن حصین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هزار شاه بود روز بزم در یک تخت</p></div>
<div class="m2"><p>هزار شیر بود روز رزم در یک زین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>موافقان را کلکش بسان آب حیات</p></div>
<div class="m2"><p>مخالفان را تیغش چو آذر برزین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نه با سخاوت او هیچ دوست رنجور است</p></div>
<div class="m2"><p>نه با سعادت او هیچ بنده هست حزین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو رسم او بستائی شوی ستوده ستای</p></div>
<div class="m2"><p>چو مهر او بگزینی شوی ستوده گزین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از ابر و دریا دست و دلش گذشته بجود</p></div>
<div class="m2"><p>قیاس هر دو بکن تا یقین بدانی این</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از آن دو خلق بموج و بهین غریق شوند</p></div>
<div class="m2"><p>وزین دو خلق توانگر شود بمدح و بهین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بمدحتش تن آزادگان همیشه دهان</p></div>
<div class="m2"><p>بخدمتش دل فرزانگان همیشه رهین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هواش در دل دانا چو سکه بر دینار</p></div>
<div class="m2"><p>روان نادان کینش خلیده چون سکین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ستاره را همه رادی دهد کفش تعلیم</p></div>
<div class="m2"><p>زمانه را همه شادی کند دلش تلقین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خردش مونس جانست و فضل مونس دل</p></div>
<div class="m2"><p>وفاش همبر عمر است وجود همبر دین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز سجده ملکان پیش تخمش اندر هست</p></div>
<div class="m2"><p>همه بساط پر از شکل روی و نقش جبین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پلنگ و شیر چو نام خدنگ او شنوند</p></div>
<div class="m2"><p>پلنگ لنگ بماند بجای شیر عرین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز فضل کرد خداوند طبع او نه ز گل</p></div>
<div class="m2"><p>ز جود کرد خداوند دست او نه ز طین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از او تهور باشد ز خصم و حاسد جان</p></div>
<div class="m2"><p>ز شیر دندان باشد ز غرم و رنگ سرین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بجای طلعت او تیره آفتاب بلند</p></div>
<div class="m2"><p>به پیش همت او پست آسمان برین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تن مخالف او کرده آسمان کمان</p></div>
<div class="m2"><p>بجان دشمن او بر جهان گشاده کمین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بدوستان بر از او مر غوا شود مروا</p></div>
<div class="m2"><p>بدشمنان براز او آفرین شود نفرین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سخای خواجه عیانست وان خلق خبر</p></div>
<div class="m2"><p>عطای خلق گمانست وان خواجه یقین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ایا بمردی با اژدها و شیر عدیل</p></div>
<div class="m2"><p>و یا برادی با آفتاب و ابر قرین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بقا ندارد پیش بنان تو دریا</p></div>
<div class="m2"><p>پدید ناید پیش سنان تو تنین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بگاه نظم زبان تو بحر در یتیم</p></div>
<div class="m2"><p>بگاه نثر بیان تو ابر در ثمین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اگرچه یاسین هست از شرف سورتها</p></div>
<div class="m2"><p>بنام تو شرف آرد مدیح بر یاسین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>رهی بطمع شرف کرد قصد مجلس تو</p></div>
<div class="m2"><p>که خلق را شرفی و زمانه را تزیین</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شریف مجلس تو دید و خوب طلعت تو</p></div>
<div class="m2"><p>شریف گشت بنزد جهانیان و مکین</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به مجلس تو بیاراست جان تن پرور</p></div>
<div class="m2"><p>بطلعت تو بیفروخت چشم گیتی بین</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بیامده است که فرمان دهیش تا برود</p></div>
<div class="m2"><p>که هست مهر تواش دین و مدح تو آیین</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همیشه تا نفروشد بتلخ شیرین کس</p></div>
<div class="m2"><p>همیشه تا نفروشد بخار کس نسرین</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو خار بادا نسرین بچشم دشمن تو</p></div>
<div class="m2"><p>مدام عیش عدو تلخ و آن تو شیرین</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>خجسته بادت فرخنده عید روزه گشای</p></div>
<div class="m2"><p>بخرمی بگذاری هزار عید چنین</p></div></div>