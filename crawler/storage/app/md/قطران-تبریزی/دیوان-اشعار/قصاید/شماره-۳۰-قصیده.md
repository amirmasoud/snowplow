---
title: >-
    شمارهٔ ۳۰ - قصیده
---
# شمارهٔ ۳۰ - قصیده

<div class="b" id="bn1"><div class="m1"><p>ایا به تیغ و قلم رنج خصم و دشمن گنج</p></div>
<div class="m2"><p>تن عدوی ترا داده روزگار شکنج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بناز دست ولی کرده یار با بگماز</p></div>
<div class="m2"><p>برنج روی عدو کرده جفت با آرنج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز دیده خون دل افتاده بر رخان عدوت</p></div>
<div class="m2"><p>چو نار دانه نشانده بقصد در نارنج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسان موسی عمران ز دست فرعونان</p></div>
<div class="m2"><p>همه جهان بگرفتی به تیغ تو بی رنج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر آنچه زان نیاکانت بود بگرفتی</p></div>
<div class="m2"><p>وز آنچه بود طمعشان خدای دادت خنج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بروز بخشش نوک قلمت جان پرورد</p></div>
<div class="m2"><p>بروز کوشش نوک سنانت جان آهنج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مخالفان ترا قول هست و نیست عمل</p></div>
<div class="m2"><p>چنانکه خورد نشان تا خلنج کاسه خلنج کذا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسا کسا که بر کس به نیم ذره نجست</p></div>
<div class="m2"><p>شد از عطای تو دینار پاش و گوهرسنج</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنانکه تازی سوی و غا بروز مصاف</p></div>
<div class="m2"><p>بروز صید نتازد عقاب زی سارنج</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز نیکی آید نیکی چنانکه عادت تست</p></div>
<div class="m2"><p>همیشه نیک سکال و همیشه نیک الفنج</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خدا یکانا گنجور تو چه دیده ز من</p></div>
<div class="m2"><p>که تو بگویی پنجاه ده نیارد پنج</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بمن برنج دل و جان رسید رنج سخن</p></div>
<div class="m2"><p>چو گنج مال بگنجور تو رسیده بگنج</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ترا جواهر گنج سخن فرستم من</p></div>
<div class="m2"><p>مرا فرستد گنجور تو سوانح رنج</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بقات بادا چندانکه کام تست بکام</p></div>
<div class="m2"><p>که چون تو کس ندهد داد زین سرای سپنج</p></div></div>