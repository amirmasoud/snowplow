---
title: >-
    شمارهٔ ۱۹۰ - در مدح ابونصر مملان
---
# شمارهٔ ۱۹۰ - در مدح ابونصر مملان

<div class="b" id="bn1"><div class="m1"><p>ایا سروی که سوسن را ز سنبل سایبان کردی</p></div>
<div class="m2"><p>ز بوی سوسن و سنبل جهان پر مشگ و بان کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فکندی بر گل از عنبر هزاران حلقه و چنبر</p></div>
<div class="m2"><p>بزیر هر یک از عمدا یکی جادوستان کردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی را دل شکن کردی یکی را دل گران کردی</p></div>
<div class="m2"><p>یکی را دل سپر کردی یکی را جان ستان کردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشیدی غالیه بر کل فکندی بر سمن سنبل</p></div>
<div class="m2"><p>یکی را دام دل کردی یکی را بند جان کردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه مشگت سوزد از آتش نه آتش میرد از باران</p></div>
<div class="m2"><p>نه آن را زین بیازردی نه این را زان زیان کردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگل گویند نتوان کرد پنهان ماه تابان را</p></div>
<div class="m2"><p>تو اندر غالیه خورشید تابان را نهان کردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسان سرو سیمینی میان باغ نیکوئی</p></div>
<div class="m2"><p>مرا در بوستان غم چو زرین خیزران کردی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو همچون نار داری روی و همچون ناردان دو لب</p></div>
<div class="m2"><p>بدان هر دو دل و چشمم چو نار و ناردان کردی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میان باغ بنشینی و گرد راغ برگشتی</p></div>
<div class="m2"><p>یکی را بوستان کردی یکی را گلستان کردی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه آفت دیدی از عاشق چه راحت دیدی از گیتی</p></div>
<div class="m2"><p>که کردی پیر عاشق را و گیتی را جوان کردی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سریر مرغ در بستان زمرد کردی و مرجان</p></div>
<div class="m2"><p>بساط گور در صحرا پرند و پرنیان کردی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چرا توریة خوان کردی میان باغ بلبل را</p></div>
<div class="m2"><p>که چون موسی درختان را بباغ اندر نوان کردی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مگر گنجور نعمانی و یا دریای عمانی</p></div>
<div class="m2"><p>و یا روزی گذر از دست شاه کامران کردی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سر شاهان ابونصر بن مسعود بن مملان آن</p></div>
<div class="m2"><p>که چون جستی رضای او دل از سختی جهان کردی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ایا خسرو تو آن شاهی که کردت قصد بد خواهی</p></div>
<div class="m2"><p>که چون تیرش جهان کردی و پشتش چون کمان کردی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فلک نکند چنین کاری که مردم را چنان باید</p></div>
<div class="m2"><p>تو هر کاری که مردم را چنان باید چنان کردی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سبب دست تو می دانم روزیهای مردم را</p></div>
<div class="m2"><p>همانا دست را ز ایزد بروزیها ضمان کردی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز دشمن ملک خالی شد چو دل را کان کین کردی</p></div>
<div class="m2"><p>ز گوهر گنج خالی شد چو کف را کین کان کردی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کسی کاندر روان او روا نشد کین تو روزی</p></div>
<div class="m2"><p>روانش را گرفتار بلای جاودان کردی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بکان زعفران ماند بروز رزم تیغ تو</p></div>
<div class="m2"><p>بسا چون ارغوان رویان کزان چون زعفران کردی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بسا جستند کین تو سنانها برده بر گردون</p></div>
<div class="m2"><p>که جسم چشم ایشان را بساعت بر سنان کردی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کفت چون ابر نوروزی گهر بارد شبان روزی</p></div>
<div class="m2"><p>برای زائران از زر چو باغ اندر خزان کردی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز مردی اصل ببریدی بمیدان گرگ مردم را</p></div>
<div class="m2"><p>بدین و داد گرگان را امینان شبان کردی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دلم چون بوستان کردی ز بس شادی خداوندا</p></div>
<div class="m2"><p>مرا جفت ضیاع و ملک و باغ و بوستان کردی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز جود تو من از گیتی بنعمت داستان بردم</p></div>
<div class="m2"><p>بنعمت مر مرا همچو سخایت داستان کردی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بسان کاوه من بودم نژند از دست ضحاکان</p></div>
<div class="m2"><p>تو افریدون مرا همچون درفش کاویان کردی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مرا در آسمان بردی بجای خانه پستم</p></div>
<div class="m2"><p>کنون چون همت خویشم مکان در آسمان کردی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ببامش چون گذر کردی و می خوردی بنامش بر</p></div>
<div class="m2"><p>یکی را چون سما کردی یکی را چون جهانکردی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شدی زی خانه میران و در حشمت سر ایشان</p></div>
<div class="m2"><p>فراز آسمان بردی و جفت اختران کردی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اگر من کهترم ز ایشان چو ایشان کردیم زیرا</p></div>
<div class="m2"><p>کجا با من همان کردی که با ایشان همان کردی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بدین امید میران را سراسر مدح گو کردی</p></div>
<div class="m2"><p>بدین امید شاهان را یکایک مداح خوان کردی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تو هستی سایه یزدان نشاید گفت یزدان را</p></div>
<div class="m2"><p>چرا این را سبک کردی چرا آن را گران کردی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تو مهتاب زمانی و مرا شمع زمین کردی</p></div>
<div class="m2"><p>تو خورشید زمینی و مرا ماه زمان کردی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بقا بادت به پیروزی و هر روزی بقا بادت</p></div>
<div class="m2"><p>که خصمان را و خویشان را بدیدن شادمان کردی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز گشت عالم فانی خدایت پاسبان بادا</p></div>
<div class="m2"><p>که دست و تیغ را بر خلق عالم پاسبان کردی</p></div></div>