---
title: >-
    شمارهٔ ۲۳ - در مدح شاه ابوالخلیل جعفر
---
# شمارهٔ ۲۳ - در مدح شاه ابوالخلیل جعفر

<div class="b" id="bn1"><div class="m1"><p>تا دیده سوی دوست دلم را دلیل گشت</p></div>
<div class="m2"><p>بی خواب گشت و جای خیال خلیل گشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده بخفت ز اول از آن کاو دلیل بود</p></div>
<div class="m2"><p>راهی که گم شد اول ثانی دلیل گشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هاروت وار گشتم از آن زهره رخ کجا</p></div>
<div class="m2"><p>بادام او بسرمه بابل کحیل گشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مژگان چو نیش نحل و میان چو نمیان نحل</p></div>
<div class="m2"><p>بی او تنم ز نوحه و زاری نحیل گشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا آن بنفشه زلف جمال جمیل یافت</p></div>
<div class="m2"><p>جانم اسیر عشق چو جان جمیل گشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از درد عشق نارون من چو نال شد</p></div>
<div class="m2"><p>وز داغ عشق قطره دل همچو نیل گشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از هول آن دو چشم بد آهنگ چون نهنگ</p></div>
<div class="m2"><p>از خون دل دو چشمم چون رود نیل گشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با او دلم چو قدش بی بند راست شد</p></div>
<div class="m2"><p>بی او تنم چو زلفش بی هال و هیل گشت؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرچند نیکوئیش رخش را رفیق شد</p></div>
<div class="m2"><p>هرچند عاشقیش دل مرا عدیل گشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شاید که نازد آن بت و کشی کند بدان</p></div>
<div class="m2"><p>کز نیکوان بدل ستدن بی بدیل گشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه هرکه خوب گشت چنو دلربای شد</p></div>
<div class="m2"><p>نه هرکه پادشا شد چون بوالخلیل گشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آشوب جان خصمان آرام جان دوست</p></div>
<div class="m2"><p>جعفر که زر جعفری از وی ذلیل گشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>امید خلق هست برزق از کفش مگر</p></div>
<div class="m2"><p>کفش برزق خلق ز یزدان کفیل گشت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از بس بکف زر و گهر داد خلق را</p></div>
<div class="m2"><p>زر و گهر ذلیل چو ریک سبیل گشت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آنکو پسندا وز ملک تخت و ملک بود</p></div>
<div class="m2"><p>از بیم او بتخت پدر بر دخیل گشت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای آنکه هر گه علت ملک تو دید اگر</p></div>
<div class="m2"><p>مانند خضر بود فنا را عدیل گشت؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هرچند رخ یابد گاهی ز پشه پیل</p></div>
<div class="m2"><p>نه پیل پشه گردد و نه پشه پیل گشت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تشنه سراب دیده ز مهر تو یاد کرد</p></div>
<div class="m2"><p>بروی سراب یکسر چون سلسبیل گشت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن کو بهشت کین تو از دل بر او بهشت</p></div>
<div class="m2"><p>چونانکه بر پیامبر ما سبیل گشت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وانکو بدیت کفت و بچشم بدت بدید</p></div>
<div class="m2"><p>دست قصیر بر سوی جانش طویل گشت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دندان بکامش اندر چون کفته خشت شد</p></div>
<div class="m2"><p>مژگان بچشمش اندر چون تفته میل گشت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بس میر کو ببزم تو اندر ندیم شد</p></div>
<div class="m2"><p>بس شاه کو بشهر تو اندر وکیل گشت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گیتی بفضل واصل تو را بایدی و لیک</p></div>
<div class="m2"><p>گردون عدو فاضل و خصم اصیل گشت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آن کش نزول مهر تو در دل طریق شد</p></div>
<div class="m2"><p>روز نزول او همه روز رحیل گشت؟</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بر تو صهیل اسب بود چون صفیر مرغ</p></div>
<div class="m2"><p>وز بیم تو صفیر بر اعدا صهیل گشت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آنکو ره سلامت در سایه تو جست</p></div>
<div class="m2"><p>بر دوستان پیامش سیف سلیل گشت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بس کهتری غمی که بجای تو رنج برد</p></div>
<div class="m2"><p>از جاه و دولت تو امیر جلیل گشت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بس خسروی جلیل که با تو ببست فصل</p></div>
<div class="m2"><p>بسیار خوارتر ز سگان فصیل گشت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ناصح که مهر جوی تو باشد بروز و شب</p></div>
<div class="m2"><p>با فر و بر زو زور تن جبرئیل گشت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آن کو بنفس دون و بهمت حقیر بود</p></div>
<div class="m2"><p>چون خدمت تو کردش او را دو جلیل گشت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از مدح تو بشعری شاعر رساند سر</p></div>
<div class="m2"><p>با فر قد از عطای تو فرقش عدیل گشت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>رنجی قلیل را ز تو گنجی کثیر یافت</p></div>
<div class="m2"><p>وین رنج و گنج زی تو کثیرش قلیل گشت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>با دانش تو حکمت لقمان فتاده شد</p></div>
<div class="m2"><p>با لفظ تو کلام عرب قال و قیل گشت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تا وصف در مسیل کنند و حدیث نوح</p></div>
<div class="m2"><p>کز معجزات نوح بآخر قبیل گشت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بادت بقای نوح که بدخواه ملک تو</p></div>
<div class="m2"><p>در بند رنج و محنت چون در مسیل گشت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>عید خلیل خرم بگذار با خلیل</p></div>
<div class="m2"><p>کز بس خلیل عدو و عدوی خلیل گشت</p></div></div>