---
title: >-
    شمارهٔ ۹۴ - در مدح ابوالیسر
---
# شمارهٔ ۹۴ - در مدح ابوالیسر

<div class="b" id="bn1"><div class="m1"><p>بهشت وار شد از نوبهار گیتی باز</p></div>
<div class="m2"><p>در بهشت بر او کرد چرخ گوئی باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درم درم شده روی زمین چو پشت پلنگ</p></div>
<div class="m2"><p>شکن شکن شده آب شمر چو سینه باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرشگ ابر کند هر فراز را چو نشیب</p></div>
<div class="m2"><p>نسیم باد کند هر نشیب را چو فراز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر نگشت هوا جای آهوان ختن</p></div>
<div class="m2"><p>وگر نگشت زمین جای بتگران طراز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو آهوان ختن آن چراست مشگ فشان</p></div>
<div class="m2"><p>چو بتگران طراز این چراست نقش طراز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز نافه باد تهی کرد طبله عطار</p></div>
<div class="m2"><p>ز حله ابر تهی کرد کلبه بزاز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سحاب گرد که اندر همی کشد پرده</p></div>
<div class="m2"><p>شمال گرد گل اندر همی کند پرواز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کنون که سرخ گل از روی پرده باز گرفت</p></div>
<div class="m2"><p>بتا گل رخت از من چرا گرفتی باز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همی ببندی خوابم بزلف عاشق بند</p></div>
<div class="m2"><p>همی بتازی صبرم بچشم جا دو تاز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نژند کردی جانم بدان دو چشم نژند</p></div>
<div class="m2"><p>دراز کردی عشقم بدان دو زلف دراز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو خند خند همه سال و من گری گری</p></div>
<div class="m2"><p>تو ناز ناز همه روز و من گداز گداز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا همی ننوازی مگر ندانی تو</p></div>
<div class="m2"><p>که هست مهتر من اوستاد بنده نواز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سپهر دانش و دریای جود ابوالیسر آن</p></div>
<div class="m2"><p>که جود و دانش یابند باز از او تک و تاز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بخشم جان آشوب و بمهر جان آرام</p></div>
<div class="m2"><p>بتیغ جنگ انجام و بتیر جنگ آغاز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بطمع سود بداندیش او اسیر زیان</p></div>
<div class="m2"><p>به طمع ناز بداندیش او اسیر نیاز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بساط او ز لب مهتران گرفته نگار</p></div>
<div class="m2"><p>رکاب او ز رخ سرکشان گرفته طراز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز عدل او بجهان اندرون نماند جور</p></div>
<div class="m2"><p>ز جود او بجهان اندرون نماند آز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ایا همیشه ولی را بکف جان پرور</p></div>
<div class="m2"><p>ایا همیشه عدو را بتیغ جان پرداز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>روان شود بهوای تو با خرد همراه</p></div>
<div class="m2"><p>خرد شود بمدح تو با هنر انباز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سپهر هست سرای تو و زمینش بساط</p></div>
<div class="m2"><p>زمانه هست عروس تو و جهانش جهاز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ندیده هیچ حصاری چو تو حصار گشای</p></div>
<div class="m2"><p>ندیده هیچ سپاهی چو تو سپاه طراز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>برزم رزم گشائی ببزم بزم آرای</p></div>
<div class="m2"><p>بتیغ تیغ گذاری به تیر تیرانداز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ستاره پیش تو اندر برد بطوع سجود</p></div>
<div class="m2"><p>سپهر پیش تو اندر برد بطبع نماز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>موالیان تو همواره با نشاط و سرور</p></div>
<div class="m2"><p>معادیان تو همواره با گزند و گداز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تو ایدری و نهیب تو هست در بلغار</p></div>
<div class="m2"><p>تو ایدری ونهیب تو هست در ابخاز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اگر نبوده بدانی شگفت نیست بدانک</p></div>
<div class="m2"><p>زمانه از دل و از رأی تو نپوشد راز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همیشه تا ز پی هر گزند باشد سود</p></div>
<div class="m2"><p>همیشه تا ز پی هر نیاز باشد ناز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو جفت سود و بداندیش تو عدیل گزند</p></div>
<div class="m2"><p>تو جفت ناز و بداندیش تو عدیل نیاز</p></div></div>