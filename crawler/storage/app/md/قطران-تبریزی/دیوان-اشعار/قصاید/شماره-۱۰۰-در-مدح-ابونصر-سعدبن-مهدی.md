---
title: >-
    شمارهٔ ۱۰۰ - در مدح ابونصر سعدبن مهدی
---
# شمارهٔ ۱۰۰ - در مدح ابونصر سعدبن مهدی

<div class="b" id="bn1"><div class="m1"><p>تا مهر بر فروخت ببرج حمل چراغ</p></div>
<div class="m2"><p>پر شمع و پر چراغ شد از لاله باغ و راغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیو است زاغ گوئی مقری است عندلیب</p></div>
<div class="m2"><p>کز بانک او ز باغ هزیمت گرفت زاغ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بوستان کلاغ هزیمت گرفت راست</p></div>
<div class="m2"><p>کز بادریسه کشت سر کوه چون کلاغ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از باد شد غدیر بکردار صدر باز</p></div>
<div class="m2"><p>وز میغ گشت چرخ بکردار پشت ماغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نرگس بیاد سوسن و شمشاد در فکند</p></div>
<div class="m2"><p>دینار گون نبیذ به کافور گون ایاغ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در باغ بگذری ز فروغ و نسیم گل</p></div>
<div class="m2"><p>رنگین شود دو دیده و مشگین شود دماغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوئی بباغ حور فرود آمد از بهشت</p></div>
<div class="m2"><p>یا دهخدای شه بگذشته است پیش باغ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بو نصر سعد مهدی کز نصرت است و سعد</p></div>
<div class="m2"><p>بر خاتمش نگینه و بر مرکبش جناغ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از مهر او کناغ فرازنده چون چنار</p></div>
<div class="m2"><p>وز کین او چنار گدازنده چون کناغ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از خوی او برند گل و نسترن نسیم</p></div>
<div class="m2"><p>وز روی او برند مه و مشتری فراغ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آموختن توان ز یکی خوش صد ادب</p></div>
<div class="m2"><p>وافروختن توان ز یکی شمع صد چراغ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آبست جود او و دل دوست چون خوید</p></div>
<div class="m2"><p>ناراست خشم او و تن خصم خشک تاغ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در رزم برق تیغش اندر میان گرد</p></div>
<div class="m2"><p>تابان ز چرخ باشد چون پیش دوده داغ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از مهر جود نیست بچیزد گرش میل</p></div>
<div class="m2"><p>وز شغل ملک نیست بچیز دگر فراغ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در باغ و راغ میر چمان باد جاودان</p></div>
<div class="m2"><p>تا جای سرو باغ بود جای رنگ راغ</p></div></div>