---
title: >-
    شمارهٔ ۱۰۱ - در مدح ابونصر مملان
---
# شمارهٔ ۱۰۱ - در مدح ابونصر مملان

<div class="b" id="bn1"><div class="m1"><p>تا خزان آورد روی خویش سوی باغ و راغ</p></div>
<div class="m2"><p>ابر یک ساعت نجست از تعبیه کردن فراغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از لب دریا برآمد بامدادان خیل ابر</p></div>
<div class="m2"><p>و آسمان از وی شود پر خیل گردو دود و داغ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرخ شد در کوه از پس لاله چید منقار کبک</p></div>
<div class="m2"><p>سم آهو سبز شد از بس گرازان شد براغ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از فروغ لاله و کل می شود رنگین دو چشم</p></div>
<div class="m2"><p>از شمیم بان و سنبل می شود مشکین دماغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا سحرگه بشکند در بوستان نرکس خمار</p></div>
<div class="m2"><p>لاله از ژاله بود چونان که پر از می ایاغ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برق هر ساعت بتابد همچو داغ تافته</p></div>
<div class="m2"><p>آب ریزد از سحاب اندر میان دشت و باغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا حواصل عرض کرده طوطی و طاووس کوه</p></div>
<div class="m2"><p>کهربا کرده بعرض بسد و پیروزه باغ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بلبل از بستان گریخت از گلستان گلبرگ ریخت</p></div>
<div class="m2"><p>جای این نارنگ بستد جای آن بگرفت زاغ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طرف بستان گشت پر قندیل زرین از ترنج</p></div>
<div class="m2"><p>گر ز نرگس بود بر روی زمین سیمین چراغ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا نثار زر بشاخ سرو سر زی زاغ کرد؟</p></div>
<div class="m2"><p>چون برآمد ماه روی رایت خسرو ز باغ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خسرو پیروزگر بو نصر مملان آنکه نیست</p></div>
<div class="m2"><p>از سخا و جود او را از دگر شغلی فراغ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دست و جودش ابر و باران است و آز خلق خوید</p></div>
<div class="m2"><p>تیغ و تیزش نار سوزان است و جسم خصم تاغ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هست دولت خاتم جاه وجلالش را نکین</p></div>
<div class="m2"><p>هست نصرت مرکب قدر و کمالش را جناغ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شیر را ماند بروز جنگ و خصم اوست میش</p></div>
<div class="m2"><p>باز را ماند بگاه رزم و دشمن چون کلاغ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مر ولی را قامت از مهرش فرازان چون چنار</p></div>
<div class="m2"><p>مر عدو را سینه از کینش گدازان چون کناغ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روشن از برق حسامش چرخ همچون صدر باز</p></div>
<div class="m2"><p>دشت از گرد سپاهش تیره همچون پشت ماغ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مهتری با بذل و جود او نیامد در جهان</p></div>
<div class="m2"><p>خسروی با عدل و داد او ندارد کس سراغ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سروری گر سرو قامت پیش بنده اش خم نکرد</p></div>
<div class="m2"><p>پیچد اندر گرد اندامش اجل همچون فشاغ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا بدشت اندر بروید لاله با داغ درون</p></div>
<div class="m2"><p>همچو لاله دشمنش را باد دل پردرد و داغ</p></div></div>