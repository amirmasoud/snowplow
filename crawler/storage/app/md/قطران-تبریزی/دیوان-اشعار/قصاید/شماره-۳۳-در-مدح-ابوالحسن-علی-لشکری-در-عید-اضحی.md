---
title: >-
    شمارهٔ ۳۳ - در مدح ابوالحسن علی لشکری در عید اضحی
---
# شمارهٔ ۳۳ - در مدح ابوالحسن علی لشکری در عید اضحی

<div class="b" id="bn1"><div class="m1"><p>ای نگار خند خندان یک زمان با من بخند</p></div>
<div class="m2"><p>تا کی این خشم تو تا کی چند از این ناز تو چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرم بردار از میان و جام می بر دست گیر</p></div>
<div class="m2"><p>بند بگشا از میان و لب ز خندیدن مبند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر مرا بی بند خواهی بند بگشا از میان</p></div>
<div class="m2"><p>ور مرا بی گریه خواهی شاد بنشین و بخند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرخ می مانا بجام زر همیدادی مرا</p></div>
<div class="m2"><p>آن لب و می مر مرا اندیشه ای در دل فکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کاین چرا آمد برون زو لفظهای همچو زهر</p></div>
<div class="m2"><p>وان چرا چون زهر کرده حرفهای همچو قند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حرف چندین در جهان یکشب نشد آن غمگسار</p></div>
<div class="m2"><p>فرق چندین در میان یک شب نشد آن دلپسند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر این خواهم لب جام و لب جانان بهم</p></div>
<div class="m2"><p>تا بود گردد دلم دائم ز شادی دستبند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای عدیل نرگس پرکین تو مشکین کمان</p></div>
<div class="m2"><p>وی رفیق لاله رنگین تو پروین کمند کذا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مار کردار است زلفت زان قبل شد پیچ پیچ</p></div>
<div class="m2"><p>کژدم آئین است جعدت زین سبب شد بند بند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل زبون دارم ز مهر رویت ای ماه آنچنانک</p></div>
<div class="m2"><p>دشمنان دارند جان از بیم شاه شیر بند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خسرو توران و ایران میر میران بوالحسن</p></div>
<div class="m2"><p>آن چو خسرو بر سریر و آن چو بهمن بر سمند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تیره باشد پیش روشن رای او روز سپید</p></div>
<div class="m2"><p>پست باشد پیش عالی قدر او چرخ بلند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کیقباد ار مانده بودی مهر او جستی بجان</p></div>
<div class="m2"><p>زرد هشت ارزنده بودی مدح او خواندی بزند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کافران زو پند نشنیدند بسپردند جان</p></div>
<div class="m2"><p>برگزید از بیم او کافرستان امروز پند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گاه بخشیدن ندارد رأی او روی و ریا</p></div>
<div class="m2"><p>گاه کوشیدن ندارد طبع او دستان و فند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>لشگری را کشت کو را مرگ نتوانست کشت</p></div>
<div class="m2"><p>قلعه ای را کند کو را چرخ نتوانست کند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز آتش شمشیر او دارند جان در تن چنانک</p></div>
<div class="m2"><p>هست نالان و طپان مانند بر آتش سپند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>لشگر فضلون همانجا شد فکنده کز قضا</p></div>
<div class="m2"><p>شاه خصمان را فکند و خصم یاران را فکند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بد رسد گویند شاهان را ز دستوران بد</p></div>
<div class="m2"><p>جز کنون این داستان را کس نیابد دلپسند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای جهانت پیشکار ای روزگارت زیر دست</p></div>
<div class="m2"><p>ای سپهرت رهنما ای کردگارت یارمند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>باد هر روزیت عید و فتح بادت زین سپس</p></div>
<div class="m2"><p>سوی کس بی نامه های فتح نفرستی نوند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گوسفند و گاو کشتن فرض هست این عید را</p></div>
<div class="m2"><p>کاندرین آمد رضای ایزد بیچون و چند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ایزد از هر عید هست این عید راضی تر ز تو</p></div>
<div class="m2"><p>زانکه کافر کشته بر جای گاو و گوسفند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا بود کرم از گزند و تا بود رامش ز سود</p></div>
<div class="m2"><p>تا بوند از سور خرم همچو از ماتم نژند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بد سگالت جفت ماتم نیکخواهت جفت سور</p></div>
<div class="m2"><p>دوستت انباز سور و دشمنت جفت گزند</p></div></div>