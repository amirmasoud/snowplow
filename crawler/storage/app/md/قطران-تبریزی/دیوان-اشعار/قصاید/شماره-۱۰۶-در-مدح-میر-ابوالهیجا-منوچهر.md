---
title: >-
    شمارهٔ ۱۰۶ - در مدح میر ابوالهیجا منوچهر
---
# شمارهٔ ۱۰۶ - در مدح میر ابوالهیجا منوچهر

<div class="b" id="bn1"><div class="m1"><p>گشت کوه و باغ در زیر گل بیجاده رنگ</p></div>
<div class="m2"><p>ساق و سم از گل چریدن کرد چون بیجاده رنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ارغوان آمد بجای شنبلید زرد گون</p></div>
<div class="m2"><p>لاله های باده رنگ آمد بجای باد رنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش بود خوردن کنون با دوستان در بوستان</p></div>
<div class="m2"><p>باده های لاله گون در لاله های باده رنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا بدید از باد نیسان خاک گلزاری بود</p></div>
<div class="m2"><p>ز آب آذرگون کند دل مرد دانا آذرنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از نسیم گل شده چون عنبر و کافور خاک</p></div>
<div class="m2"><p>وز فروغ گل شده چون بسد و یاقوت سنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گشت زابر قیرگون و لاله بیجاده فام</p></div>
<div class="m2"><p>دشت چون منقار طوطی چرخ چون پشت پلنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بانگ بلبل هر شبان روزی بسان بانگ نای</p></div>
<div class="m2"><p>بانگ صلصل هر سحرگاهی بسان بانگ چنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پشت و بانگ من چو پشت و بانگ چنگ آمد درست</p></div>
<div class="m2"><p>تا من آن خورشید خوبان را رها کردم ز چنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا بدست خویش تنگ اسب هجران سخت کرد</p></div>
<div class="m2"><p>شد بمن چون حلقه تنکش جهان تاریک و تنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر بنزدیک من آید بی درنگ آن ماه روی</p></div>
<div class="m2"><p>میر ابوالهیجا نیابد جای چندان بی درنگ کذا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مشتری چهر و فلک همت منوچهر آنکه او</p></div>
<div class="m2"><p>چون فریدون فر و چون هوشنگ دارد هوش و هنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدره ها گریند چون بادوستان باشد بصلح</p></div>
<div class="m2"><p>کرکسان خندند چون با دشمنان باشد بجنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زانکه گه گه باشد از چرم پلنگ او را جناغ</p></div>
<div class="m2"><p>از همه ددها تکبر بیشتر دارد پلنگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بادل و دست و سنان و تیغ او در رزم و بزم</p></div>
<div class="m2"><p>برق سرد و مرگ راحت بحر خشک و چرخ تنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گرش بودی ملک در خور اسب او را آمدی</p></div>
<div class="m2"><p>ز افسر خان نعل و میخ از موی خاتون حل و تنگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روز بخشیدن نشاید خادمش سالار طی</p></div>
<div class="m2"><p>گاه کوشیدن نشاید چاکرش پور پشنگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چین انده گیرد از هولش رخان خان چین</p></div>
<div class="m2"><p>رشک حسرت آید از بیمش روان شاه زنگ؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر هواخواهان کند چون روز شبهای چو قیر</p></div>
<div class="m2"><p>بر براندیشان کند چون زهر صهبای چو رنگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بانگ تندر پیش بانگ او بروز کارزار</p></div>
<div class="m2"><p>همچنان باشد که پیش بانگ تندر بانگ چنگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مدح گویان را ببزم اندر گهر بخشد بمشت</p></div>
<div class="m2"><p>مهرجویان را بصف اندر درم بخشد بسنگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مهر او و کین او چون رود نیل آمد درست</p></div>
<div class="m2"><p>دوستان را زو شراب و دشمنان را زو شرنگ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دوستان را همچو یوسف می سپارد ملک مصر</p></div>
<div class="m2"><p>دشمنان را همچو فرعون افکند کام نهنگ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آنکه در میدان کینش طوق باشد یافته</p></div>
<div class="m2"><p>او بجای طوق سر گردنش بندد پالهنگ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر سخن گوید بود گویای یونان همچو گنگ</p></div>
<div class="m2"><p>گر عطا بخشد بود دریای عمان همچو گنگ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پیش او چون میش و مور و پشه باشد پیش پیل</p></div>
<div class="m2"><p>خصم روز جنگ او باشد اگر پور پشنگ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا بود بالا خدنگ آئین ز شادی و سرور</p></div>
<div class="m2"><p>تا شود قامت کمان آسا ز اندوه و غرنگ</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>باد بالا دشمنانش را ز انده چون کمان</p></div>
<div class="m2"><p>باد قامت دوستانش را ز شادی چون خدنگ</p></div></div>