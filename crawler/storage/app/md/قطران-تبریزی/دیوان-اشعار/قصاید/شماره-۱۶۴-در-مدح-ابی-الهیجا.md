---
title: >-
    شمارهٔ ۱۶۴ - در مدح ابی الهیجا
---
# شمارهٔ ۱۶۴ - در مدح ابی الهیجا

<div class="b" id="bn1"><div class="m1"><p>شگفتهای جهانرا پدید نیست کران</p></div>
<div class="m2"><p>هران شگفت که بینی بود شگفت بران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر شگفتی میبایدت بپوی زمین</p></div>
<div class="m2"><p>وگر عجائب میبایدت بجوی زمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر آن گمان که بری در سفر شودت یقین</p></div>
<div class="m2"><p>هر آن خبر که بود در سفر شودت عیان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو یک عیان نبود در جهان هزار خبر</p></div>
<div class="m2"><p>چو یک یقین نبود در جهان هزار گمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخن گزاف روانست و عقل میزانست</p></div>
<div class="m2"><p>گزاف راست نیاید مگر که با میزان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که هر سخن بزبان در توان گرفت و لیک</p></div>
<div class="m2"><p>درست کردن بر عقل هر سخن نتوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بوند بر سر بهتان زبان و گوش بجنگ</p></div>
<div class="m2"><p>هو او عقل نکنجند بر سر بهتان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه از سرودن گوینده یابد ایچ گزند</p></div>
<div class="m2"><p>نه از شنودن پرسنده یابد ایچ زیان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هزار ره صفت هفتخوان و روئین دژ</p></div>
<div class="m2"><p>فزون شنیدم و خواندم من از هزار افسان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه عقل کرد همی باو راز شگفتی این</p></div>
<div class="m2"><p>نه رای دید همی در خور از عجیبی آن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شد استوار بر من آنچه بود ضعیف</p></div>
<div class="m2"><p>شد آشکار بر من هر آنچه بود نهان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدانکه دیده همی دید و هرچه گوش شنید</p></div>
<div class="m2"><p>بدانکه عقل پذیرفت هر آنچه گفت زبان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز قلعه ای که مرا کس چنان نگفت خبر</p></div>
<div class="m2"><p>ز باره ای که مرا کس چنان نداد نشان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنان بلند کجا رنجه گشت و فرسوده</p></div>
<div class="m2"><p>ز بیخ و سرش دل ماهی و سر سرطان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بزیر سایه او در هزار چرخ سبک</p></div>
<div class="m2"><p>بزیر پایه او در هزار جرم گران</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ببامش اندر بی پایه ننگرد گردون</p></div>
<div class="m2"><p>بزیرش اندر بی باره نگذرد کیوان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در او گزند نیارد فلک بصد نیرنگ</p></div>
<div class="m2"><p>بر او گذار نیابد پری بصد دستان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>میان او نتواند خزید دیو نژند</p></div>
<div class="m2"><p>فراز او نتواند وزید باد بزان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بمحکمی چو کف مرد زفت بی فرهنگ</p></div>
<div class="m2"><p>بتیرگی چو دل مرد غمر بی ایمان؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بر او ز گنبد گردان چنان توان نگری</p></div>
<div class="m2"><p>که از زمین نگری سوی گنبد گردان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ببام او بر نادان شود ستاره شمر</p></div>
<div class="m2"><p>شود ستاره شمر زیرش اندرون نادان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هزار کاخ بدو در یکی هزار سرای</p></div>
<div class="m2"><p>هزار برج بدو در یکی هزار ایوان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بنش چو دشمن خسرو گذشته از ماهی</p></div>
<div class="m2"><p>سرش چو همت خسرو گذشته از کیوان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سر زمان و زمین شهریار ابوالهیجا</p></div>
<div class="m2"><p>که اختیار زمین است وافتخار زمان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زدوده رای و زدوده دل و زدوده روان</p></div>
<div class="m2"><p>گشاده دست و گشاده دل و گشاده عنان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بدرع دشمن او بر قدر بود حلقه</p></div>
<div class="m2"><p>بتیر لشگر او بر قضا بود پیکان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز پروریدن او نازش آورد گردون</p></div>
<div class="m2"><p>بآفریدن او مفخر آورد یزدان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>قضا نسازد با تیغ او همی چنگال</p></div>
<div class="m2"><p>اجل نساید با تیر او همی دندان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز تیغ او شود آرام هر کجا آشوب</p></div>
<div class="m2"><p>ز کف او شود آباد هر کجا ویران</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اگر بهمتش اندر خورنده بودی جای</p></div>
<div class="m2"><p>جهانش مجلس بودی سپهر شادروان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عطا گرفتن و بستن دو کف بر او دشوار</p></div>
<div class="m2"><p>جهان گشادن و دادن گهر بر او آسان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بزائران همه دیبا برزمه بخشد و تخت</p></div>
<div class="m2"><p>بشاعران همه گوهر بگنج بخشد و کان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سنان او بدل اندر شود بسان خرد</p></div>
<div class="m2"><p>حسام او بتن اندر شود بسان روان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ایا گشاده زبان آسمان بمدحت تو</p></div>
<div class="m2"><p>و یا بخدمت تو بسته روزگار میان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>برنده بر همه احکامها ترا احکام</p></div>
<div class="m2"><p>رونده بر همه فرمانها ترا فرمان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بنزد همت تو هست پست چرخ بلند</p></div>
<div class="m2"><p>به پیش دولت تو هست پیر بخت جوان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همیشه تا ز هوی خلق را بود شادی</p></div>
<div class="m2"><p>همیشه تا ز هوان خلق را بود احزان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>موافقان تو بادند پاک جفت هوی</p></div>
<div class="m2"><p>مخالفان تو بادند پاک جفت هوان</p></div></div>