---
title: >-
    شمارهٔ ۱۴۴ - در مدح ابونصر جستان
---
# شمارهٔ ۱۴۴ - در مدح ابونصر جستان

<div class="b" id="bn1"><div class="m1"><p>بت پیمان شکن دائم شکسته زلف چون پیمان</p></div>
<div class="m2"><p>رخش ایمان دلش از کفر زلفش کفر بر ایمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بچین زلف دام دل برنک روی کام جان</p></div>
<div class="m2"><p>ز پیوندش روان نازان و از دوریش دل لرزان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز عشق آن رخ رخشان ز مهر آن لب جانان</p></div>
<div class="m2"><p>برنج اندر مرا دائم رخ از ناخن لب از دندان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو زلف و دو رخش شمشاد باغ و نو گل بستان</p></div>
<div class="m2"><p>ز رنج و از هوای آن دو دل افسرده و حیران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عبیر و مشگ ارزان زان دو زلف و طره لرزان</p></div>
<div class="m2"><p>ز آب چشم و رنگ روی من دنیا رو در ارزان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدو بادام و دو شکر هم او درد و هم او درمان</p></div>
<div class="m2"><p>ز دل رفتن و ز او گفتن ز جان طاعت وز او فرمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ببالا سرو میدانی بعارض نسترن میدان</p></div>
<div class="m2"><p>ازین افروخته مجلس از آن آراسته میدان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو جانان جام میدارد بیفزاید مرا زان جان</p></div>
<div class="m2"><p>ز لب هرگز نبرم من لب جام و لب جانان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلم چون زلف او بی جان تنم چون جعد او بی جان</p></div>
<div class="m2"><p>لبش چون اشگ من رنگین رخش چون رای من تابان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر آن دردی که از دوریش در من بود شد درمان</p></div>
<div class="m2"><p>بدیدار ملک بونصر تاج خسروان جستان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>امیر مشتری طینت بهمت برتر از کیوان</p></div>
<div class="m2"><p>ز فرش جانور گردد نگار و نقش در ایوان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خداوند جهانداران و خورشید خداوندان</p></div>
<div class="m2"><p>تنش پاکیزه از هر عیب چون رای خردمندان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بگاه دانش اسکندر بگاه داد نوشیروان</p></div>
<div class="m2"><p>غلام کهترش قیصر گدای حاجبش خاقان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدو شادند آزادان و خرم آرزومندان</p></div>
<div class="m2"><p>چه پیش صاعقه سوسن چه پیش تیر او سندان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گشاده دل گشاده در نهاده خو نهاده خوان</p></div>
<div class="m2"><p>گر از زر بدره ها خواهی همیشه مدحت او خوان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نه خلای شهرش از سائل نه خالی بومش از مهمان</p></div>
<div class="m2"><p>همه شاهان همی گویند کو باد از جهان مه مان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدو کردن بدی دشوار و بخشد خواسته آسان</p></div>
<div class="m2"><p>ز داد او نمیبیند کسی اندر جهان نقصان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یکی بخشیدنش باشد فزون از دخل صد عمان</p></div>
<div class="m2"><p>یکی کهترش را زیبد هزاران ملکت نعمان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ایا گشته دراز امید از تو کوته امیدان</p></div>
<div class="m2"><p>تو بادی شاد با شاهی تو بادی با شهی شادان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مباد ایران ز تو خالی که هستی قبله ایران</p></div>
<div class="m2"><p>که ایران بی وجود تو بیک ساعت شود ویران</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>توئی شیرین بدانائی بسان مهر دلبندان</p></div>
<div class="m2"><p>هر آن مدحی که من گویم ترا هستی دو صد چندان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نکو خلق و نکو خلقی و هستی راحت انسان</p></div>
<div class="m2"><p>کسی کو مدح تو خواند نباید خواندنش قران</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز تو قارون شود مفلس ز تو دانا شود نادان</p></div>
<div class="m2"><p>توئی پاینده گیتی ترا پاینده بادا جان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ندارد پای با دست تو زر و گوهر اندر کان</p></div>
<div class="m2"><p>وفا و جود را کانی و داد فضل را ارکان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>توئی فخر همه رادان توئی فخر همه گردان</p></div>
<div class="m2"><p>ندیده است و نبیند چون تو رادی کنبد گردان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عدو از دیدنت گریان ولی از دیدنت خندان</p></div>
<div class="m2"><p>بر اینان خانه چون جنت بر آنان خانه چون زندان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خدایت زود باز آورد و از ما دور کرد احزان</p></div>
<div class="m2"><p>کنون هستیم زین شادان اگر بودیم غمگین زان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>الا تا قطره باران شود در دریم عمان</p></div>
<div class="m2"><p>بخوشی باش با خویشان بشادی باش با یاران</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همیشه با معالی زی همیشه بوالمعالی دان</p></div>
<div class="m2"><p>بدو آراسته بادت سپاه و ملک و خان و مان</p></div></div>