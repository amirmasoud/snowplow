---
title: >-
    شمارهٔ ۱۳۰ - بهاریه و خطاب به ابر
---
# شمارهٔ ۱۳۰ - بهاریه و خطاب به ابر

<div class="b" id="bn1"><div class="m1"><p>الا ای پرده تاری به پیش چشمه روشن</p></div>
<div class="m2"><p>زمانی کوه را تَرگی زمانی چرخ را جوشن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دژم روئی و گیتی را کند آثار تو خرم</p></div>
<div class="m2"><p>سیه فامی و عالم را کند دیدار تو روشن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گهی بر گوشه گردون نهاده مر ترا گوشه</p></div>
<div class="m2"><p>گهی بر دامن خورشید بسته مر ترا دامن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گهی بادت بود مرکب گهی چرخت بود میدان</p></div>
<div class="m2"><p>گهی برت بود مکمن گهی بحرت بود معدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گهی چون پشت شاهینی و گه چون سینه آهو</p></div>
<div class="m2"><p>گهی چون سیمگون خزی گهی چون نیلگون ادکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو بربستی درختان را هم از بیجاده پیرایه</p></div>
<div class="m2"><p>تو پوشیدی چمنها را هم از فیروزه پیراهن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زمین را رنگ تو دارد برنگ صدر حورالعین</p></div>
<div class="m2"><p>هوا را لون تو دارد بلون جان اهریمن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میان هر زمینی هست گوئی صد نگارستان</p></div>
<div class="m2"><p>میان هر درختی هست گوئی صد چغانه زن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شمال اندر میان باغ و بوستان هست زرافشان</p></div>
<div class="m2"><p>سحاب اندر میان دشت و وادی گشته دیباتن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بهر کاخی که بنشینی ازو بینی دو صد خرگه</p></div>
<div class="m2"><p>بهر دشتی که پیمائی در او بینی دو صد خرمن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز مرجان ارغوان را کرد نیسان یاره در ساعد</p></div>
<div class="m2"><p>ز لؤلؤ یاسمن را بست گردون عقد بر گردن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نهفته باغ در لؤلؤ نهفته شاخ در دیبا</p></div>
<div class="m2"><p>سرشته شاخ در کافور و سوده آب در چندن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>میان بوستان خیره بماند نرگس اندر گل</p></div>
<div class="m2"><p>چنان کاندر رخ معشوق ماند خیره چشم من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نگاری کز فراق او بدرد اندر همی پیچم</p></div>
<div class="m2"><p>گهی زاری کنم با دل گهی خواری کنم با تن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو در برزن بود باشد ز رویش رنگ در خانه</p></div>
<div class="m2"><p>چو در خانه بود باشد ز مویش بوی در برزن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دل من خانه عشق است و خورشید است عشق او</p></div>
<div class="m2"><p>که گر من در ببندم او همی در تابد از روزن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر سوسن همی خواهی یکی دیدار او بنگر</p></div>
<div class="m2"><p>وگر سنبل همی خواهی یکی زلفین او بشکن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کنار و دامن از چشمم بود همواره پر گوهر</p></div>
<div class="m2"><p>روان من ز چشم او بود پیوسته پر سوزن</p></div></div>