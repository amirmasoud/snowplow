---
title: >-
    شمارهٔ ۱۰۳ - در مدح ابوالخلیل جعفر
---
# شمارهٔ ۱۰۳ - در مدح ابوالخلیل جعفر

<div class="b" id="bn1"><div class="m1"><p>ای رخ رخشانت چون آئینه نادیده زنگ</p></div>
<div class="m2"><p>زنگ بزدا از دل عاشق ببکمازی چو زنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکه رومی آرزو کرده عطایش چون عرب</p></div>
<div class="m2"><p>آنکه ترکی آرزو کرده بساطش همچو زنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مادرش بوده است همچون زنگی زنگارگون</p></div>
<div class="m2"><p>او بسان رومیان بر تن ندارد هیچ زنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در میان جام زرین چون گل اندر شنبلید</p></div>
<div class="m2"><p>بر سرش کف ایستاده همچو سیم هفت رنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>او برنگ و بوی همچون بهرمان و غالیه است</p></div>
<div class="m2"><p>رنگ و بوی او ز دلها دور دارد بند و رنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهرمان دیدی که همچون غالیه باشد ببوی</p></div>
<div class="m2"><p>غالیه دیدی که همچون بهرمان باشد برنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنکه کبک از بوی او گردد به نیروی عقاب</p></div>
<div class="m2"><p>آنکه رنگ از زور او گردد بآهنگ پلنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر بآذرمه چکانی قطره ای بر سنگ ازو</p></div>
<div class="m2"><p>در مه دی آهوان سنبل چرند از روی سنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر خورد زو زفت همچون میر گردد روز جود</p></div>
<div class="m2"><p>ور خورد کم زهره زو چون شاه گردد روز جنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بوالخلیل آن چون خلیل اندر گه جود و سخا</p></div>
<div class="m2"><p>جعفر آن ماننده هوشنگ گاه هوش و هنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر پلنگان کین او ورزند و رنگان مهر او</p></div>
<div class="m2"><p>کمترین رنگی برون آرد پلنگیر از سنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای خداوند سخا کاندر جهان آئین تست</p></div>
<div class="m2"><p>جامه بخشیدن بتخت و سیم بخشیدن بسنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مال گرد آورده هرکس تو گم کردی بدست</p></div>
<div class="m2"><p>نعمت گم کرده هرکس تو آوردی بچنگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون بجنبانی عنان باره از خیل عدو</p></div>
<div class="m2"><p>کس نداند زین ز پالان پاردم از پالهنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همچو تو دارند میران نام و نی شبه تواند</p></div>
<div class="m2"><p>هم بمردم ماند و مردم نباشد استرنگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روی خویشان تو باشد زین سپس چون ارغوان</p></div>
<div class="m2"><p>روی خصمان تو باشد زین سپس چون با درنگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>غایبی از دوستان و حاضری زی دشمنان</p></div>
<div class="m2"><p>دشمنان را آذری و دوستان را آذرنگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دشت گشت از هول تو بر دشمنان همچون مزار</p></div>
<div class="m2"><p>نوششان گشت از تو زهر و نامشان گشت از تو ننگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گشتشان از گرد لشگر گشتشان از بانگ کوس</p></div>
<div class="m2"><p>گشتشان از زخم زوبین گشتشان از ضرب سنگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چشمها همواره کور و گوشها پیوسته کر</p></div>
<div class="m2"><p>دستها پیوسته شل و پایها همواره لنگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بس نماند تا تو باز آئی بدارالملک خویش</p></div>
<div class="m2"><p>ملک بدخواهان دین آورده یکسر زیر چنگ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آوری دلخسته بطریقان روم و رو سرا</p></div>
<div class="m2"><p>پای جفت پای بند و سر رفیق پا لهنگ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای هوا بر دشمنان از هیبت تو گشته تار</p></div>
<div class="m2"><p>وی زمین بر دوستان از فرقت تو گشته تنگ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا به پیروزی برفتی دوستداران ترا</p></div>
<div class="m2"><p>یکزمان خالی نباشد از غریو و از غرنگ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ساختی با تو خداوندا سفر چاکر بسی</p></div>
<div class="m2"><p>گر بدانستی که سازی در سفر چندین درنگ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فتح آذربایجان امسال اینجا خوانده ام</p></div>
<div class="m2"><p>فتح ترکستان و چین خوانم دگر سالت فرنگ</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا نباشد خلق را هرگز غرنگ اندر نشاط</p></div>
<div class="m2"><p>از شرنگ دهر بادا دشمنانت را غرنگ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا بود گردنده گردون بزم تو خالی مباد</p></div>
<div class="m2"><p>از بتان شنگ و شوخ و ساقیان شوخ و شنگ</p></div></div>