---
title: >-
    شمارهٔ ۸۹ - در مدح ابوالمعمر
---
# شمارهٔ ۸۹ - در مدح ابوالمعمر

<div class="b" id="bn1"><div class="m1"><p>نگه کن روی آن دلبر چو نقش لعبت بربر</p></div>
<div class="m2"><p>دو گلنارش ببین پر مار و دو مارش ببین پرپر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لبش ماننده مرجان برش ماننده مرمر</p></div>
<div class="m2"><p>رخش پیرایه کشمیر و قدش فتنه کشمر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لبانش برده رنگ از می رخانش برده نور از خور</p></div>
<div class="m2"><p>بشب بر دو رخش خور بین بروز از دو لبش میخور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بچین زلف چون سنبل بتاب جعد چون عنبر</p></div>
<div class="m2"><p>چو چوگان بسته در چوگان چو چنبر بسته در چنبر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگرد بسدش لؤلؤ بگرد نرگسش نشتر</p></div>
<div class="m2"><p>ز پیکان زخم این بهتر ز شکر طعم آن خوشتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل من گشت چون نیلی بسان برگ نیلوفر</p></div>
<div class="m2"><p>چو من سوی هوا پویم شود پایم بسان پر؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ایا از جان گرامی تر ز بخت نیک فرخ تر</p></div>
<div class="m2"><p>مرا دائم ز عشق تو دو لب خشک و دو دیده تر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من ار لب زمهریر آرم ز چشم آب و ز جان آذر</p></div>
<div class="m2"><p>وی از دو رخ گل آزار و از دو لب می آذر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من از عبهر همی بارم برخ هرگونه کون گوهر</p></div>
<div class="m2"><p>تو بر من گونه گون پیکان همی اندازی از عبهر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز گل بر سوسنت پرده ز سنبل بر گلت معجر</p></div>
<div class="m2"><p>خم زلفانت چون چوگان سر مژگانت چون خنجر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زبانت مهربان با من روانت باز کین آور</p></div>
<div class="m2"><p>یکی بیدادگر میر است و دیگر دادگر داور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جگر سوزی بدو نرگس دل افروزی بدو رخ بر</p></div>
<div class="m2"><p>چو کلک و نیزه استاد در ایوان و در لشگر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نبرده بوالمعمر کوست جمله خلق را یاور</p></div>
<div class="m2"><p>مهیا گشت زو ملک و معمر گشت زو کشور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بگاه رزم چون رستم بگاه بزم چون نوذر</p></div>
<div class="m2"><p>گه تدبیر چون سلمان گه پرهیز چون بوذر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بدان تیغ روان او بار از آن دست گهر گستر</p></div>
<div class="m2"><p>عدو را گل کند بالین ولیرا گل کند بستر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کمالش ملک را پرگار و کلکش فضل را مسطر</p></div>
<div class="m2"><p>ولی را خانه زو خرم عدو را کارازا و مضطر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز روی او بیفروزد سر او مجلس و محضر</p></div>
<div class="m2"><p>بفر او بماه دی شود خاک سیاه اخضر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فلک با همتش هامون و دریا با کفش فرغر</p></div>
<div class="m2"><p>ز یک جودش ملا گردد عقاب چر خر اژاغر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بدان خشت چو الماس و بدان شمشیر چون آذر</p></div>
<div class="m2"><p>همان خود و همان معجر همان درع و همان چادر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شود بر درگهش ظاهر همه نیک و بد مضمر</p></div>
<div class="m2"><p>وی آتش گشت و مردم عود و عالی درگهش مجمر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بمنظر بهتر از مخبر سخنجر بهتر از منظر</p></div>
<div class="m2"><p>ز کیوان در برش جوشن ز گردون بر سرش مغفر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز حلم او شود درکه ز خشم او شود که در</p></div>
<div class="m2"><p>سرای مهر و کینرا هست شمشیر و کف او در</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ولی را ناز ازو فربه عدو را ناز از او لاغر</p></div>
<div class="m2"><p>یکی را بهره زو زوبین یکی را بهره زو ساغر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ایا اعدای تو بردار و احباب تو بر منبر</p></div>
<div class="m2"><p>که آتش با رضای تو نسوزد برگ سیسنبر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اگرچه زاد تو اینجا و گرچه جای تو ایدر</p></div>
<div class="m2"><p>بتو ترساند اندر سند و چین فرزند را مادر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بجونش دارد و مغفرنکه تن مؤمن و کافر</p></div>
<div class="m2"><p>نگه دارد بروز کین تن تو جوشن و مغفر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>الا تا هر عرض قائم بود در اصل بر جوهر</p></div>
<div class="m2"><p>الا تا گوهری مردم ستوده باشد از گوهر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جهان بادا بتو قائم چو از جوهر عرض اندر</p></div>
<div class="m2"><p>کفت گوهرفشان بادا مدام و دل گهرپرور</p></div></div>