---
title: >-
    شمارهٔ ۱۲۰ - قصیده
---
# شمارهٔ ۱۲۰ - قصیده

<div class="b" id="bn1"><div class="m1"><p>ای آنکه ترا بوده بر اندام جهان دام</p></div>
<div class="m2"><p>چون بست ترا دست جهان دام بر اندام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آن پس که همی گام بکام تو زدی چرخ</p></div>
<div class="m2"><p>چون داد به ناکام ترا چرخ زدن گام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ایام همه عالم از ایام تو خوش بود</p></div>
<div class="m2"><p>ایام تو چون تلخ شد از گردش ایام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای خوبتر از یوسف یعقوب ترا روی</p></div>
<div class="m2"><p>چون بود مر او را بودت خوب سرانجام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زین دام بیابی تو بدل ناحیت روم</p></div>
<div class="m2"><p>چون یافت وی از بند بدل ناحیت شام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو زود خوری شام بدان شوم بداندیش</p></div>
<div class="m2"><p>کاو خورد بدست دگران بر تو ملک شام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خود شد چو تو شاپور بروم اندر زی بند</p></div>
<div class="m2"><p>خود شد چو تو بهرام بهند اندرزی دام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از روم بکام دل باز آمد شاپور</p></div>
<div class="m2"><p>وز هند بناز دل باز آمد بهرام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون راست رود دولت مادام نپاید</p></div>
<div class="m2"><p>افکنده و خیزنده بود دولت مادام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باید که بود مرد گهی شاد و گهی زار</p></div>
<div class="m2"><p>نیکی ببدی در شده و کام بناکام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زود از پی آرام پدید آید آشوب</p></div>
<div class="m2"><p>زود از پی آشوب پدید آید آرام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سلطان ببناریک شنیدی که چه کرده است</p></div>
<div class="m2"><p>کاو را بمصاف اندر بگرفته بصمصام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>او عاصی و بد اصل تو با اصل و اطاعت</p></div>
<div class="m2"><p>او دشمن و تو دوست وی از کفر و تو ز اسلام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>انصاف کسی خواهد کردن که بگویند</p></div>
<div class="m2"><p>چندانکه جهانست ز سلطان بودت نام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ما گوش سوی نامه و پیغام تو داریم</p></div>
<div class="m2"><p>ار چه که تویی مان بدل نامه و پیغام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چشم همه خون بارد هنگام گرستن</p></div>
<div class="m2"><p>تا می نزند بی تو ملک چشم بهنگام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چرخت برساناد سوی ملک و سوی پور</p></div>
<div class="m2"><p>دهرت برساناد بر باب و بر مام</p></div></div>