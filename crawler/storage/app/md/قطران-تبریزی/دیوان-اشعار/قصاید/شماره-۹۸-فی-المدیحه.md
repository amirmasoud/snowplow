---
title: >-
    شمارهٔ ۹۸ - فی المدیحه
---
# شمارهٔ ۹۸ - فی المدیحه

<div class="b" id="bn1"><div class="m1"><p>نهاد روی بما دولت و سعادت باز</p></div>
<div class="m2"><p>ز رنج و درد بدل دادمان سلامت و ناز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرفت سعد فراز و گرفت نحس نشیب</p></div>
<div class="m2"><p>گرفت رنج نشیب و گرفت ناز فراز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نهفته سود در آمد ز خواب و خفت زیان</p></div>
<div class="m2"><p>حقیقت آمد و اندر نوشت کار مجاز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برست تن ز نهار و برست دل ز نهیب</p></div>
<div class="m2"><p>برست سر ز گزند و برست جان ز گداز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دو بهره مانده ز روز خجسته آمد عید</p></div>
<div class="m2"><p>بدیمه اندر نوروز بخت کرد آغاز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسا کسا که فرو برده بود سر بگریز</p></div>
<div class="m2"><p>بسا کسا که جگر خسته بدبگرم و گداز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرفته بود گهی چند میش موطن شیر</p></div>
<div class="m2"><p>گرفته بود گهی چند زاغ مسکن باز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کنون بجایگه خویش شیر باز آمد</p></div>
<div class="m2"><p>کنون بجایگه خویش باز بر شد باز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از آنکه شمس ملوک و از آنکه شمس الملک</p></div>
<div class="m2"><p>بدار مملکت خویشتن رسید فراز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بآفتاب برآمد سر سعادت میر</p></div>
<div class="m2"><p>سر عدوش فروشد بچاه محنت باز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مخالفانش همه سرنگون و بخت نگون</p></div>
<div class="m2"><p>موافقانش همه سرفراز و سینه فراز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگرچه رنج دراز آزمود بی او خلق</p></div>
<div class="m2"><p>کنون بدیدن او شد بخواب رنج دراز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کناد وقف بر این خلق جای میر خدای</p></div>
<div class="m2"><p>که خلق میر پرستند و میر خلق نواز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ایا فزوده جهان را بطاعت تو ولی</p></div>
<div class="m2"><p>ز روم تا بیمن و ز عراق تا بطراز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هرآنکه را که خلاف تو افتد اندر دل</p></div>
<div class="m2"><p>نهد هماره تن و جان خویش بر سر آز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو عقد را بمیانه چو تیغ را بگهر</p></div>
<div class="m2"><p>چو حلقه را بنگین و چو جامه را بطراز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بزهره راست چو شیری بزور راست چو پیل</p></div>
<div class="m2"><p>بزخم همچو پلنگی بحمله همچو گراز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چنانکه سهم تو افتد سوی نشان عدو</p></div>
<div class="m2"><p>نشانه را نزند سهم هیچ تیر انداز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر شهنشه اهواز با تو کین سازد</p></div>
<div class="m2"><p>شودش موی بتن بر چو کژدم اهواز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سزد که مردم از این پس ترا برند سجود</p></div>
<div class="m2"><p>سزد که مردم از این پس ترا برند نماز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بقدر خویشتن انباز کرد چرخ ترا</p></div>
<div class="m2"><p>گمان مبر که کند چرخ غدر با انباز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چنان شدند ز روی تو شادمان که بحشر</p></div>
<div class="m2"><p>گناهکاران یابند زی بهشت جواز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فراز گشت در بخت خلق تا که کنند</p></div>
<div class="m2"><p>ترا ثنا که تو کردی در سعادت باز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نبود طاقت ایشان که بر زنند نفس</p></div>
<div class="m2"><p>کنون بطاق فلک بر همی زنند آواز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همیشه تا که بتابد مه و ببالد سرو</p></div>
<div class="m2"><p>بسان ماه بتاب و بسان سر و بناز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دریده باد دل کور دشمنانت بخشت</p></div>
<div class="m2"><p>بریده باد سر شوم دشمنانت بگاز</p></div></div>