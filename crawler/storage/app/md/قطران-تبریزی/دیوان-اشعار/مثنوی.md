---
title: >-
    مثنوی
---
# مثنوی

<div class="b" id="bn1"><div class="m1"><p>ز نزدیک این کهتر کهتران</p></div>
<div class="m2"><p>بنزدیک آن مهتر مهتران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپهدار دوران ابوالیسر کوست</p></div>
<div class="m2"><p>جگر سوز دشمن دل افروز دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بجسم اندر از روح بایسته تر</p></div>
<div class="m2"><p>بجان اندر از عقل شایسته تر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برادی چو ابرو بمردی چو ببر</p></div>
<div class="m2"><p>ز تیغ و کفش رنج بر ببر و ابر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز دریا گه جود بخشنده تر</p></div>
<div class="m2"><p>ز آتش عدو را گدازنده تر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برزم اندرون مرگ بارد چو میغ</p></div>
<div class="m2"><p>ببزم اندرون جان دهد بیدریغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روان شاد گردد بدیدار او</p></div>
<div class="m2"><p>خرد تازه گردد بگفتار او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر بنگرد دشمن از چهر او</p></div>
<div class="m2"><p>دل و جان بیاراید از مهر او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ببخشیدن زر از آن شادتر</p></div>
<div class="m2"><p>که درویش جوینده باشد بزر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه بی نکته نغز گوید سخن</p></div>
<div class="m2"><p>نه جز نکته هرگز بجوید سخن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ایا آفتاب مهان جهان</p></div>
<div class="m2"><p>پناه بزرگان و پشت کهان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو دانی که من نیکخواه توام</p></div>
<div class="m2"><p>همه ساله اندر پناه توام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو آنی که من با تو یاران بدم</p></div>
<div class="m2"><p>بشادی و غم با تو هم زان بدم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بشهر اندرون از تو نامی شدم</p></div>
<div class="m2"><p>بنزدیک خسرو گرامی شدم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یکی روز بی من نخوردی نبید</p></div>
<div class="m2"><p>که بی من کسی نیز خوانت ندید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بنزدیک خسرو نشاندی مرا</p></div>
<div class="m2"><p>بگردون هفتم رساندی مرا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بجاه توام هر کسی چیز داد</p></div>
<div class="m2"><p>ز بهر تو میرم بسی چیز داد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بخدمت همی خواند شاهم فزون</p></div>
<div class="m2"><p>همی کرد هر روز جاهم فزون</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرا بویه شهر تبریز خاست</p></div>
<div class="m2"><p>بجان اندرم آتش تیز خاست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو من عزم تبریز کردم همی</p></div>
<div class="m2"><p>بدل باد تبریز خوردم همی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بسی نیکوئیها پذیروفتم</p></div>
<div class="m2"><p>بشیرین زبانی همی گوفتم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که نزدیک من باش و زائر مرو</p></div>
<div class="m2"><p>که نیکی کنم با تو هر روز نو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هم از میر خرم بوی هم ز من</p></div>
<div class="m2"><p>نیاید ترا خواسته کم ز من</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همت نام هست و همت کام هست</p></div>
<div class="m2"><p>همت با چو ما مردم آرام هست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تو آنجا نه فرزند داری نه زن</p></div>
<div class="m2"><p>هم اینجا بهر چیز با من بزن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چه خواهی که را جوئی اندر جهان</p></div>
<div class="m2"><p>بخیره چرا پوئی اندر جهان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو بشنیدم این دست برداشتم</p></div>
<div class="m2"><p>ترا بر سر خویش بگماشتم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بسی خلعت و خواسته دادیم</p></div>
<div class="m2"><p>بکام دل آنجا فرستادیم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو من رخت بر بستم از تخت تو</p></div>
<div class="m2"><p>رسیدم بکام اندر از بخت تو</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شدند این بزرگان خریدار من</p></div>
<div class="m2"><p>بود خرمی شان بدیدار من</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بود خوش دل من بدیدارشان</p></div>
<div class="m2"><p>روانم ز گیتی خریدارشان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو آن نیکوئیهات یاد آورم</p></div>
<div class="m2"><p>ز دود جگر خیره گردد سرم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو یاد آیدم روی فرزند تو</p></div>
<div class="m2"><p>نشاط دل خویش و پیوند تو</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بکردار تندر بنالد دلم</p></div>
<div class="m2"><p>بشادی و غم زو سکالد دلم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>که گر بیکران بر دلم غم بدی</p></div>
<div class="m2"><p>بدیدار او از دلم کم بدی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بماناد جان تو با آن من</p></div>
<div class="m2"><p>فدای تو بادا تن و جان من</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مرا گفته کان بخت آید بروی</p></div>
<div class="m2"><p>ز اندوه و شادی مرا باز گوی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بدین پایه اندر کنون هر چه بود</p></div>
<div class="m2"><p>ترا در بدر باز خواهم نمود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نخست از کرمهای میر اجل</p></div>
<div class="m2"><p>که دستش ز رزقست و تیغ از اجل</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همی آن کند با من از نیکوئی</p></div>
<div class="m2"><p>که گر باز گویم ترا نگروی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز هم پیشه گان پیش دارد مرا</p></div>
<div class="m2"><p>ز گردون همی بر گذارد مرا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نه او هرگز این کرد با هیچ تن</p></div>
<div class="m2"><p>نه از هیچ تن هستم این دیده من</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دگر میر فرخ که فرزند اوست</p></div>
<div class="m2"><p>که گیتی گشایست و دلبند اوست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ابونصر مملان که هر ساعتی</p></div>
<div class="m2"><p>فرستد بنزدیک من خلعتی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نه یک ساعت از پیش بگذاردم</p></div>
<div class="m2"><p>چنان چون بباید همی داردم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بر آنم کز این پس عقارم دهد</p></div>
<div class="m2"><p>بجام سعادت عقارم دهد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دگر میر عبداله از بهر من</p></div>
<div class="m2"><p>زبان بر گشاید بهر انجمن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>از او هرچه خواهی ندارد گران</p></div>
<div class="m2"><p>همان خلعتم خواهد از دیگران</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>کز او بگذری بر خدای بزرگ</p></div>
<div class="m2"><p>که دادش بزرگی خدای سترک</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>جوان مرد شیر اوژن پیرمرد</p></div>
<div class="m2"><p>ز نیکی ندانی که با من چه کرد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گهی استر را هوارم دهد</p></div>
<div class="m2"><p>گهی نیفه شاهوارم دهد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بخروار هامی فرستد مرا</p></div>
<div class="m2"><p>وز ایندر پیاپی فرستد مرا</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ز حسان مساوی بشادی درم</p></div>
<div class="m2"><p>بشادی ز حسان مساوی درم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>مرا دارد از جان و تن دوستر</p></div>
<div class="m2"><p>کسی را ندارد ز من دوستر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بتن جانم از دولت خسرو است</p></div>
<div class="m2"><p>که هنگام رادی چو کیخسرو است</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>دو سو دستم از وی که باید بتن</p></div>
<div class="m2"><p>زمانی سخا و زمانی سخن</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>مرا مطیعانند ازین بیشتر</p></div>
<div class="m2"><p>من این قوم را داشتم پیشتر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>که میرند و ز میر نامی ترند</p></div>
<div class="m2"><p>ز جان بر تن من گرامی ترند</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>اگر چه من آنجا بگنج اندرم</p></div>
<div class="m2"><p>ز نادیدن تو برنج اندرم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>مرا دیدن روی تو بایدی</p></div>
<div class="m2"><p>و گر نان نبودی مرا شایدی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بدیدار تو شاد بودی دلم</p></div>
<div class="m2"><p>وز اندیشه آزاد بودی دلم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>من از بهر شاه جهان لشگری</p></div>
<div class="m2"><p>فروزنده شهر و هم لشگری</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>یکی شعر گفتم برنج روان</p></div>
<div class="m2"><p>بمعنی نغز و بلفظ روان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>اگر نیک رائی بجای آوری</p></div>
<div class="m2"><p>بدین چاکر خویش رای آوری</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بفرمان این شعر خواندن بدو</p></div>
<div class="m2"><p>همان رسم چاکر بماندن بدو</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>اگر خلعت او بیابد رهی</p></div>
<div class="m2"><p>چو ماه دو هفته بتابد رهی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بر مهتران جاهش افزون شود</p></div>
<div class="m2"><p>دل حاسدانش پر از خون شود</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چو استاد بوالمعمر آید بشهر</p></div>
<div class="m2"><p>در خرمی برگشاید بشهر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>دعا کن ز بهر من او را بسی</p></div>
<div class="m2"><p>که چون او نباشد بگیتی کسی</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>دگر حاجب راد و فرزانه را</p></div>
<div class="m2"><p>چراغ دل خویش و بیگانه را</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بسی آفرینش ستایش نمای</p></div>
<div class="m2"><p>بسی در ثنایش نیایش نمای</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>همان آسمان سخا بوالفرج</p></div>
<div class="m2"><p>که هرگز مبادش ز شادی هرج</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>فراوان ز چاکر درودش رسان</p></div>
<div class="m2"><p>که بادش فدا جان و چیز کسان</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>هم استاد ما بوعلی را که هست</p></div>
<div class="m2"><p>برادی گشاده همه ساله دست</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بسی آفرین کن ز بهر رهی</p></div>
<div class="m2"><p>که هرگز مباد این جهان زو تهی</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>رسان سوی مملان دعاهای من</p></div>
<div class="m2"><p>که گوئی مکن آن تقاضای من</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>هم اندر سرای تو دادم ردی</p></div>
<div class="m2"><p>سزد گر من او را کنم جان فدی</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>علی را همی بوس هر روز بیش</p></div>
<div class="m2"><p>هم از بهر چاکر هم از بهر خویش</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>هزار آفرین کن تن خویش را</p></div>
<div class="m2"><p>بنفرین بزن دشمن خویش را</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>به خطی مرا هر زمان یاد کن</p></div>
<div class="m2"><p>بدان خط جان مرا شاد کن</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>که گر نیست راهم بدیدار تو</p></div>
<div class="m2"><p>شوم شاد باری بگفتار تو</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>ز خط تو من دیده روشن کنم</p></div>
<div class="m2"><p>دل از گفته های تو گلشن کنم</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>کنم شاد از آن خاطر خویش را</p></div>
<div class="m2"><p>نهم مرهمی این دل ریش را</p></div></div>