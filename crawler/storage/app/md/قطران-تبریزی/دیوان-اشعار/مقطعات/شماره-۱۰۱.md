---
title: >-
    شمارهٔ ۱۰۱
---
# شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>آرامش جان من آرام روان من</p></div>
<div class="m2"><p>روشن بتو چشم من شادان بتو جان من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بالات چو تیر من مژگان چو سنان من</p></div>
<div class="m2"><p>گیسو چو کمند من ابرو چو کمان من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیهوده چرا داری ای سرو روان من</p></div>
<div class="m2"><p>نومید چرا داری از وصل روان من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بس که همی جویی از هجر زیان من</p></div>
<div class="m2"><p>ریش است نهان من زار است عیان من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دانیم بهر حالی ای جان جهان من</p></div>
<div class="m2"><p>من راز نهان تو تو راز نهان من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هستم ز دل آن تو هستی ز دل آن من</p></div>
<div class="m2"><p>بل نیست زبان تو شیرین چو زبان من</p></div></div>