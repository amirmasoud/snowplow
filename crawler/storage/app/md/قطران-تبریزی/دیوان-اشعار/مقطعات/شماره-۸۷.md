---
title: >-
    شمارهٔ ۸۷
---
# شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>زان زرد شد از داغ و درد رویم</p></div>
<div class="m2"><p>زیرا که بوصل تو نیست رویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من راه نیابم سوی تو دانی</p></div>
<div class="m2"><p>هرچند بسوی تو راه جویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان پس که همه روز با تو بودم</p></div>
<div class="m2"><p>پوشیده نبود از تو روی و مویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو نان نگسستی ز من که روزی</p></div>
<div class="m2"><p>آرد بر تو باد تند بویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا روی بشوراب چشم شوئی</p></div>
<div class="m2"><p>من روی به آب دیده شویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون بر گذری نام تو بگویم</p></div>
<div class="m2"><p>از دور کند بر خروش رویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در دیده شود سرشگ رویم</p></div>
<div class="m2"><p>چون دور کند پیک تو بگویم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با کس نتوانم حدیث گفتن</p></div>
<div class="m2"><p>گه گاه بخلوت غم مویم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رازم بجهان کس نگه ندارد</p></div>
<div class="m2"><p>من راز تو جز با تو با که گویم</p></div></div>