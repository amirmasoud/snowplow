---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>نبود صعبتر از هجر بتان هیچ عذاب</p></div>
<div class="m2"><p>که شب و روز جدا دارد از من خور و خواب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندرین گیتی کس یاد نکردی ز گنه</p></div>
<div class="m2"><p>گر بدان گیتی چون هجر بدی هیچ عذاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا غم فرقت آن ماه بمن باز نخورد</p></div>
<div class="m2"><p>ظن نبردم که ببد خلق چنین دارد تاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد خمیده قدم از فرقت آن زلف بخم</p></div>
<div class="m2"><p>تافته شد دلم از حسرت آن جعد بتاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای سفر کرده و برده ز من آرام و قرار</p></div>
<div class="m2"><p>ز آتش و آب دل و دیده مرا کرده کباب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اشک من سرختر از روی تو و پر تذرو</p></div>
<div class="m2"><p>بخت من تیره تر از موی تو و پر غراب</p></div></div>