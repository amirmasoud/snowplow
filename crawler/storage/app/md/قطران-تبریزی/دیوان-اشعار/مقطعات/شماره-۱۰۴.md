---
title: >-
    شمارهٔ ۱۰۴
---
# شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>ای دل ترا بگفتم کز عاشقی حذر کن</p></div>
<div class="m2"><p>بگذار نیکوان را وز مهرشان گذر کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون روی خوب بینی دیده فراز هم نه</p></div>
<div class="m2"><p>چون تیر عشق بارد شرم و خرد سپر کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرمان من نبردی فرجام خود نجستی</p></div>
<div class="m2"><p>پنداشتی که گویم هر ساعتی بتر کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر گام عاشقی را صدگونه درد و رنج است</p></div>
<div class="m2"><p>گر ایمنیت باد از عاشقی حذر کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناکام من برفتی در دام عشق ماندی</p></div>
<div class="m2"><p>چونست روزگارت ما را یکی خبر کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اکنون به صبر کردن ناید مراد حاصل</p></div>
<div class="m2"><p>زین چاره بازمانی، رو چاره دگر کن</p></div></div>