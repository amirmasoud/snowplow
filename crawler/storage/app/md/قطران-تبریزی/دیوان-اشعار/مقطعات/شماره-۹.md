---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>ز اول اردی بهشت گشت جهان چون بهشت</p></div>
<div class="m2"><p>از قبل آنکه درد شاه جهان را بهشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای ملک خوب کار خوب خوی و خصم زشت</p></div>
<div class="m2"><p>بادت چندان بقا کاب و گل و شاخ و کشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نامه عمر تو چرخ تا بقیامت نوشت</p></div>
<div class="m2"><p>جامه فر تو دهر با کف اقبال رشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یزدان از نور خویش جان و تن تو سرشت</p></div>
<div class="m2"><p>تخم نشاط آسمان خود ز برای تو کشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باد دل دشمنانت جایگه تیر و خشت</p></div>
<div class="m2"><p>بستر ایشان ز خاک بالش ایشان ز خشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با تو زینک وز بد با همه خلقی کنشت</p></div>
<div class="m2"><p>خانقه دشمنت باد چو ویران گشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هست جهان خدای از تو بشادی بهشت</p></div>
<div class="m2"><p>بگذر و بگذار شاد هزار اردی بهشت</p></div></div>