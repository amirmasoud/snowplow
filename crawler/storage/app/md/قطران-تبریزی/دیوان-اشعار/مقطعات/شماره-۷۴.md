---
title: >-
    شمارهٔ ۷۴
---
# شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>ای افسر زمانه و ای شاه روزگار</p></div>
<div class="m2"><p>از دولت آفرید ترا آفریدگار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رادی وراستی است ترا روز و شب سخن</p></div>
<div class="m2"><p>مردی و مردمی است ترا سال و ماه کار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باشد شکار شاهان دائم تذرو و کبک</p></div>
<div class="m2"><p>شاهان ار منند همیشه ترا شکار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اندر میان لشگر تو کی عیان بود</p></div>
<div class="m2"><p>گر کم شود هزار ور افزون شود هزار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بادات روز خرم و فرخنده مال و سال</p></div>
<div class="m2"><p>مولات خاک بوس و معادیت خاکسار</p></div></div>