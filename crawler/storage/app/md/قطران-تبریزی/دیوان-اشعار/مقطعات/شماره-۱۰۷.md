---
title: >-
    شمارهٔ ۱۰۷
---
# شمارهٔ ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>ای آفتاب روشن تابان میان گلشن</p></div>
<div class="m2"><p>آرام شهر مائی نام تو شیر اوژن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از فخر نام داری و ز نام نیک جوشن</p></div>
<div class="m2"><p>فرخنده باد بر تو نوروز و سیر گلشن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گیتی بتست خرم گردون بتست روشن</p></div>
<div class="m2"><p>ناز روان مائی رنج روان دشمن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در زیر دشمنانت سوسن شود چو سوزن</p></div>
<div class="m2"><p>در دست دوستانت سوزن شود چو سوسن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بستند بخت و دولت با دامن تو دامن</p></div>
<div class="m2"><p>وندر زدند آتش دشمنت را بخرمن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با دوستان همی زی با دشمنان همی زن</p></div>
<div class="m2"><p>با دوستان بساغر با دشمنان به آهن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هستی بکف کافی مر جود را تو معدن</p></div>
<div class="m2"><p>هستی برای روشن مر فضل را تو مسکن</p></div></div>