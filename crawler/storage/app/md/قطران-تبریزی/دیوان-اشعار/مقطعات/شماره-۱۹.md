---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>گوشی که در حلقه او بود لفظ تو</p></div>
<div class="m2"><p>مالیده سفاهت هر بدگهر شده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمی که خاک درگه تو سرمه داشتی</p></div>
<div class="m2"><p>راه ذهاب چشمه خون جگر شده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بودی نیام تیغ فصاحت زبان من</p></div>
<div class="m2"><p>واکنون ببین که ترکش تیر سحر شده است</p></div></div>