---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>خدایگانا چرخ برین زمین تو باد</p></div>
<div class="m2"><p>بروز کوشش روح الامین امین تو باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همیشه دولت و اقبال در یسار تو باد</p></div>
<div class="m2"><p>همیشه فره و فرهنگ در یمین تو باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهمت فلکی و به آفرین ملک</p></div>
<div class="m2"><p>زبان هرکس گردان بآفرین تو باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همیشه پیشه تو در جهان وفا و سخاست</p></div>
<div class="m2"><p>وفا رفیق تو باد و سخا قرین تو باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه نشستن مه همنشین قدر تو باد</p></div>
<div class="m2"><p>همه گذشتن خورشید بر نگین تو باد</p></div></div>