---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>خسروا گر دون بتاج و تخت تو محتاج باد</p></div>
<div class="m2"><p>کار تو با ناز جفت تخت و جام و تاج باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز بدخواهان تو چون روز رستاخیز باد</p></div>
<div class="m2"><p>نیک خواهان ترا شب چون شب معراج باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خنجر تو شیر باد و دشمن تو گور باد</p></div>
<div class="m2"><p>نیزه تو باز باد و خصم تو دراج باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا جهان باشد ترا هرگز بکس حاجت مباد</p></div>
<div class="m2"><p>وین جهان دائم به کهتر چاکرت محتاج باد</p></div></div>