---
title: >-
    شمارهٔ ۱۱۶
---
# شمارهٔ ۱۱۶

<div class="b" id="bn1"><div class="m1"><p>شد از بهار خجسته سپاه برد شکسته</p></div>
<div class="m2"><p>بر اوستاد موفق بهار باد خجسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همیشه بادا دستش بدست ساغر و دسته</p></div>
<div class="m2"><p>رخ ولیش شکفته دل عدوش شکسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همیشه باد دل آن کفیده چون دل پسته</p></div>
<div class="m2"><p>که دل ندارد با او بدوستاری بسته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همیشه همچو فرشته ز مرگ بادا رسته</p></div>
<div class="m2"><p>که فعلش آن فرشته است و رسمش آن فرشته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رونده بادا دولت بدو چو باز بمسته</p></div>
<div class="m2"><p>زمانه پیشش بر پای و او بتخت نشسته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ببزمش اندر بازار خوبرویان بسته</p></div>
<div class="m2"><p>بباغ دولتش اندر درخت شادی رسته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز تنش رنج رمیده ز جانش انده جسته</p></div>
<div class="m2"><p>زمانه یارش با دو ستاره بادش جسته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به آب دولت بادا مدام رویش شسته</p></div>
<div class="m2"><p>سر محبش سبز و تن عدویش خسته</p></div></div>