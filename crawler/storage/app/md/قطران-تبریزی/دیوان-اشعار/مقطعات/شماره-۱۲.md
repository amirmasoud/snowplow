---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>ای آنکه مر ترا بجهان در نظیر نیست</p></div>
<div class="m2"><p>آنکو خطر نیافت ز فیضت خطیر نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای یادگار آنکه نبودش نظیر کس</p></div>
<div class="m2"><p>ای دلفروز آنکه کس او را نظیر نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو ماه روزگاری و او میر روزگار</p></div>
<div class="m2"><p>چون او و چون تو بر بزمین ماه و میر نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چندان که داشت تاج و سریر ولوا جهان</p></div>
<div class="m2"><p>چون تو سزای تاج ولوا و سریر نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست مخالفان تو هست از دهان قصیر</p></div>
<div class="m2"><p>دست موافقانت ز گردون قصیر نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون روی دوستان تو گلنار و لاله نیست</p></div>
<div class="m2"><p>چون روی دشمنان تو زرد و زریر نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن کز تو نیست گشته جلیل او جلیل نیست</p></div>
<div class="m2"><p>وان کز تو نیست گشته حقیر او حقیر نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون دست تو برادی ابر بهار نیست</p></div>
<div class="m2"><p>چون لفظ تو بپاکی در منیر نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شادان بزی بشاهی با میر جاودان</p></div>
<div class="m2"><p>کز هر دو چون ز روزی کس را گزیر نیست</p></div></div>