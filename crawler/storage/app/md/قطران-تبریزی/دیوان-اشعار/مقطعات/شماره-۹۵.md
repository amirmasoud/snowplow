---
title: >-
    شمارهٔ ۹۵
---
# شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>فراز و نشیب است روی زمین</p></div>
<div class="m2"><p>متاز ای برادر گشاده عنان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخن نیک بر سنج و از دل بگوی</p></div>
<div class="m2"><p>ره راست بشناس و بی غم بران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برنج ار بکاهم ننالم ز غم</p></div>
<div class="m2"><p>ز چرخ ار بمیرم نخواهم امان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو کورست گیتی چه خیر از هنر</p></div>
<div class="m2"><p>چو کرست گردون چه سود از فغان</p></div></div>