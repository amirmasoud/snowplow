---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>بار خدایا ز مرگ منت چه آید</p></div>
<div class="m2"><p>بی گنهان را ز غم مکش که نشاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خامشی خویش خوار داری لیکن</p></div>
<div class="m2"><p>خامشی من ترا همی نکزاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا تو یکی ره بسوی من نگرائی</p></div>
<div class="m2"><p>هیچ سعادت بسوی من نگراید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بر تو دست من رسد عجبی نیست</p></div>
<div class="m2"><p>آنکس کو می خورد تنش برباید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آمده نوروز و شعر باید نیکو</p></div>
<div class="m2"><p>چو نان کز طبع زنگ غم بزداید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طبعی باید گشاده شعر نکو را</p></div>
<div class="m2"><p>جز سخنان تو طبع می نگشاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرت بکار است شعر نیک سخنگوی</p></div>
<div class="m2"><p>خامش باش ارت شعر نیک نیاید؟</p></div></div>