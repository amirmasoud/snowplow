---
title: >-
    شمارهٔ ۸۴
---
# شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>به او رمز دو مه تیر ای امید کرام</p></div>
<div class="m2"><p>نبید خوردن بر خویشتن مدار حرام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگرچه داری امروز روزه فردا باز</p></div>
<div class="m2"><p>نماز کرده ز مزگت بکاخ بنده خرام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برسم و شیوه بهرام جام می بستان</p></div>
<div class="m2"><p>که هست بنده تو صد چو بهمن و بهرام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مخالفان را از تیغ و تیر تست آشوب</p></div>
<div class="m2"><p>موافقان را از کف راد تست آرام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>توئی بقای زمین و توئی بقای زمان</p></div>
<div class="m2"><p>توئی پناه انام و توئی امید کرام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همیشه روز تو نوروز و بخت تو پیروز</p></div>
<div class="m2"><p>مخالفانت بی آرام و کار تو پدرام</p></div></div>