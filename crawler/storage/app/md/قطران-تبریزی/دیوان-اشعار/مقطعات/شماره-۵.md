---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>ای مایه شده دیدن تو روزبهی را</p></div>
<div class="m2"><p>بایسته و شایسته بهی را و شهی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از زر و درم کرده تهی گنج ملا را</p></div>
<div class="m2"><p>وز مدح و ثنا گرده ملا گنج تهی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمشیر تو و کلک تو در مجلس و میدان</p></div>
<div class="m2"><p>نفع و ضرر آورد بدی را و بهی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مهر تو کند سرو سهی نال نوان را</p></div>
<div class="m2"><p>کین تو کند نال نوان سرو سهی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مدح تو نکرده است فراموش رهی هیچ</p></div>
<div class="m2"><p>از بهر چه کردی تو فراموش رهی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا بوی بود چون خط معشوق سمن را</p></div>
<div class="m2"><p>تا رنگ بود چون رخ عشاق بهی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دینار و درم بخش کهان را و مهان را</p></div>
<div class="m2"><p>پیدا و نهان دان تو کهی را مهی را</p></div></div>