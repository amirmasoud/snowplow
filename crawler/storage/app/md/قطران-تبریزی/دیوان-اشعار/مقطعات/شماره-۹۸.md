---
title: >-
    شمارهٔ ۹۸
---
# شمارهٔ ۹۸

<div class="b" id="bn1"><div class="m1"><p>ای زدوده دل و زدوده سخن</p></div>
<div class="m2"><p>تازه گشت از تو روزگار کهن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زائران سر نهاده اند بتو</p></div>
<div class="m2"><p>مال تو زین قبل نگیرد تن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگشائی دل یکی به سخا</p></div>
<div class="m2"><p>بزدائی دل یکی بسخن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی تو رادی چو دیده بی دیدار</p></div>
<div class="m2"><p>بی تو شادی چو دست بی ناخن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وعده کردی مرا بزیر نخو</p></div>
<div class="m2"><p>وعده خویش را خلاف مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آی آفت شهر و فتنه برزن</p></div>
<div class="m2"><p>از روی تو خیره ماند مرد و زن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ماهی و که دید ماه سنگین دل</p></div>
<div class="m2"><p>سروی و که دید سر و سیمین تن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای من رهی آن دو چشم زوبین دار</p></div>
<div class="m2"><p>ای من رهی آن دو دست زوبین زن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زان دستان بسته دل شده عاشق</p></div>
<div class="m2"><p>زین زوبین خسته تن شده دشمن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر من ز غم بمیرم سزد تا تو</p></div>
<div class="m2"><p>با میر همی روی سوی ار من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون جوشن پوشیدی گه رفتن</p></div>
<div class="m2"><p>شد تیر علم را دلم بند جوشن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پیراهن آهن آن دلت بس باد</p></div>
<div class="m2"><p>ز آهن چکنی تو نیز پیراهن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه در خور جنگ و در خور رزمی</p></div>
<div class="m2"><p>ای در خور بزم و در خور گلشن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یارب تو بگردان نیت خسرو</p></div>
<div class="m2"><p>زین عزم درست کردن و رفتن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر میر مرا رها کند زنده</p></div>
<div class="m2"><p>به آید مرا زین غزا کردن</p></div></div>