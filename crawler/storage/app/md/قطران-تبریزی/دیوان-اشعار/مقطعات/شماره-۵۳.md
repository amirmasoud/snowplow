---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>دولت شاه جهان پاینده چون خورشید باد</p></div>
<div class="m2"><p>ملک وعمر او چو عمر و ملکت جمشید باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناصحش را رخ چو پیش مهر لاله برگ باد</p></div>
<div class="m2"><p>حاسدش را تن چو پیش باد برگ بید باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز بیزدانش مباد امید ازین گیتی بکس</p></div>
<div class="m2"><p>شهریاران را بدست و تیغ او امید باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روز او فرخنده باد و تخت او پاینده باد</p></div>
<div class="m2"><p>کام او پیوسته باد و عمر او جاوید باد</p></div></div>