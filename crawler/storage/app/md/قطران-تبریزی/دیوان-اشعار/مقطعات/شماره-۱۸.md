---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>این جهان را سایه تو باد بر سر جاودان</p></div>
<div class="m2"><p>زانکه چندانی که هستی این جهان جان پرور است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرد سم اسب تو در چشم شاهان توتیا است</p></div>
<div class="m2"><p>نعل سم اسب تو بر فرق میران افسر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر کسی صد سال یزدان را پرستد روز و شب</p></div>
<div class="m2"><p>چون مهی کین تو جوید جاودانه کافر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست و چشمت جفت ساغر باد و جفت روی دوست</p></div>
<div class="m2"><p>تا بگیتی نام دلدار است و نام ساغر است</p></div></div>