---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>اسب طرب و عیش تو ای شاه بزین باد</p></div>
<div class="m2"><p>جان و تن خصمان تو پیوسته بزین باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید زمینی و خداوند زمانی</p></div>
<div class="m2"><p>از جور زمان دشمن تو زیر زمین باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از هیبت تو پشت مخالف چو کمانست</p></div>
<div class="m2"><p>بر جان بداندیش تو از مرگ کمین باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چندانکه زمینست ترا زیر رکابست</p></div>
<div class="m2"><p>چندانکه سپهر است ترا زیر نگین باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از سجده میران و بزرگان همه ساله</p></div>
<div class="m2"><p>در گاه وصال تو پر از شکل جبین باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رادیست ترا پیشه و شادیست ترا کار</p></div>
<div class="m2"><p>تا روز قضا پیشه و کار تو همین باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با راستی و رادی طبع تو قرینست</p></div>
<div class="m2"><p>با رامش و آرامش طبع تو قرین باد</p></div></div>