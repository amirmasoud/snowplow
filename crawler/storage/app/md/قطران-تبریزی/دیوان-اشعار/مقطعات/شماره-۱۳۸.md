---
title: >-
    شمارهٔ ۱۳۸
---
# شمارهٔ ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>ای پیشه تو رامش و پیروزی و بهی</p></div>
<div class="m2"><p>گل رفت و لاله رفت و ترنج آمد و بهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دست لاله رویان گل بوی و می ستان</p></div>
<div class="m2"><p>بفزای بر ترنج و بهی رامش و بهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داد تو روزگار ز دولت همی دهد</p></div>
<div class="m2"><p>تو داد روزگار بخوشی همی دهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کردی ز نام نیک همه شهرها ملا</p></div>
<div class="m2"><p>کردی ز زر و سیم همه گنجها تهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راه نشاط گیرد و از غم رها شود</p></div>
<div class="m2"><p>آنکس که کمترین رهیت را شود رهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن از میان آهن و پولاد سر نهد</p></div>
<div class="m2"><p>گر دست خود به آهن پولاد بر نهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کارت همیشه بخشش و بخشایش است از آنک</p></div>
<div class="m2"><p>از راز روزگار فرومایه آگهی</p></div></div>