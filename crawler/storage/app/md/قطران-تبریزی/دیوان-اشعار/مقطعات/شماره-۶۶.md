---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>مرا رنج بسیار و کم روزگار</p></div>
<div class="m2"><p>بشادی کسم نیست آموزگار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه هستند یاران من مهربان</p></div>
<div class="m2"><p>نه هستند خویشان من سازگار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زمانی همی نالم از یار بد</p></div>
<div class="m2"><p>زمنی ز رنج و بد روزگار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شود نیک روز بد از یار نیک</p></div>
<div class="m2"><p>مرا بدتر از روزگار است یار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا دل فکار است دائم ز دوست</p></div>
<div class="m2"><p>ز دشمن بود هر کسی دلفکار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر دشمن و دوست یکسان بو</p></div>
<div class="m2"><p>چه دوست و چه دشمن چه خرما چه خار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از آن دشمنی کو بود دور دست</p></div>
<div class="m2"><p>تواند رهاندن بدستان و چار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چگونه تواند ز دشمن گریخت</p></div>
<div class="m2"><p>کسی کش بود دشمن اندرکنار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ایا عاشقی بی دل و بی روان</p></div>
<div class="m2"><p>گهی گرم خوار و گهی سوگوار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه نالی چو رعد و چه گریی چو ابر</p></div>
<div class="m2"><p>چه گوئی تو چندین چه پیچی چو مار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همیشه ز دل نالی و دیده هیچ</p></div>
<div class="m2"><p>که دارند جان گرامیت خوار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که دید این رخ یار پیمان شکن</p></div>
<div class="m2"><p>برفت از پی یار بی زینهار</p></div></div>