---
title: >-
    شمارهٔ ۱۳۱
---
# شمارهٔ ۱۳۱

<div class="b" id="bn1"><div class="m1"><p>ای شاه جهانگیر جهاندار جهانجوی</p></div>
<div class="m2"><p>عید است و لب یار و لب جام و لب جوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از درد و غمان جان و روان تورها یافت</p></div>
<div class="m2"><p>تو ز انده و اندیشه بیاسای و روان جوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در شدت همی باد بهار آید گه گاه</p></div>
<div class="m2"><p>بر باد بهاری بستان باده خوشبوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گه گوی همی باز و گهی صید همی کن</p></div>
<div class="m2"><p>ای تیغ تو چوگان و سر دشمن تو گوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شمشیر تو چون روی و دل خصم بسوزد</p></div>
<div class="m2"><p>گر خصم تو باشد بمثل زاهن و از روی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چندانت بقا باد بشاهی که تو خواهی</p></div>
<div class="m2"><p>بدخواه تو از بیم تو بگدازد چون موی</p></div></div>