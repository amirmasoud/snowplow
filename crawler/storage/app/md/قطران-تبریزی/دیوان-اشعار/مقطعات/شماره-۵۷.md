---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>آن را که همی جست و دلم بخت بمن داد</p></div>
<div class="m2"><p>اکنون بستاند دلم از ناز و طرب داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بخت بد آزادم و از یار بشادی</p></div>
<div class="m2"><p>شاد است بمن همچو من آن ماه پریزاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر شادم از آن یار وفادار عجب نیست</p></div>
<div class="m2"><p>گز یار وفادار همه خلق بود شاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر لاله و شمشاد نروید بجهان نیز</p></div>
<div class="m2"><p>بس باد مرا زلف و رخش لاله و شمشاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا من بزیم قصه نوشاد نخوانم</p></div>
<div class="m2"><p>کز دیدن او مجلس من گشت چو نوشاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون آهن و پولاد قوی بود جدائی</p></div>
<div class="m2"><p>از تف دلم نرم شد آن آهن و پولاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی یار نباشد تن من نیز بزاری</p></div>
<div class="m2"><p>بیمن نبود نیز دل خسته بفریاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیگر نکشد بنده ای آزار ز من نیز</p></div>
<div class="m2"><p>اکنون که من از بند جدائی شدم آزاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غم خوردم بل عاقبت کارنکو گشت</p></div>
<div class="m2"><p>همواره مرا عاقبت کار چنین باد</p></div></div>