---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>بر سبزه همی آب روان آب دواند</p></div>
<div class="m2"><p>وز شاخ همی باد خزان برگ فشاند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این هیچ کس از آئینه چین نشناسد</p></div>
<div class="m2"><p>وان هیچ کس از زر ورق باز نداند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچون تن دل رفته ز تیمار جدائی</p></div>
<div class="m2"><p>باد سحری شاخ سمنرا بنواند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بسکه ببارد شب و روز ابر خزانی</p></div>
<div class="m2"><p>از کوه سوی دشت همی سیل براند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گه شد که بماند بکف میر و لیکن</p></div>
<div class="m2"><p>گر گوهر بارد بکف میر بماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاه ملکان لشگری آن شاه دلیران</p></div>
<div class="m2"><p>کو ملک جهان همچو سکندر بستاند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چندانش بقا باد بشادی و بشاهی</p></div>
<div class="m2"><p>گز مهر تو مهره خوی و خون بچکاند</p></div></div>