---
title: >-
    شمارهٔ ۱۳۲
---
# شمارهٔ ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>خدایا بر جهانم کام و فرمان روان دادی</p></div>
<div class="m2"><p>بمدح و آفرین من زبان خلق بگشادی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دشمن کین من جستی ز دولت داد من دادی</p></div>
<div class="m2"><p>بدان کز من نبیند کس بلا و رنج آزادی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بمستی و بهشیاری بخواندن دل مرا دادی</p></div>
<div class="m2"><p>منم فریاد از او آن مرادانم تو فریادی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همم تدبیر و هم رایست هم مردی و هم رادی</p></div>
<div class="m2"><p>همم نامست و هم کامست و هم مستی و استادا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بختم هست خشنودی ز دولت هستم آزادی</p></div>
<div class="m2"><p>الا ای دولتت محکم همیشه هم چنین بادی</p></div></div>