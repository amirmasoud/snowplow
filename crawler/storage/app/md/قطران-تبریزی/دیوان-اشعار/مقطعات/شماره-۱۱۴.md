---
title: >-
    شمارهٔ ۱۱۴
---
# شمارهٔ ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>شد سرشگم ز آرزوی روی تو چون روی تو</p></div>
<div class="m2"><p>وز فراق روی تو بگداختم چون موی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان من پاینده اندر تن ز مشگین موی تست</p></div>
<div class="m2"><p>دل بود روشن ز روی فرخ دلجوی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از نهیب تیر مژگان و کمان ابرویت</p></div>
<div class="m2"><p>جز دل من هیچ دل دیدن نیارد روی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا فکندی گوی نیکویی تو در میدان مهر</p></div>
<div class="m2"><p>هیچ چوگان‌زن ندید از من سبک‌تر گوی تو</p></div></div>