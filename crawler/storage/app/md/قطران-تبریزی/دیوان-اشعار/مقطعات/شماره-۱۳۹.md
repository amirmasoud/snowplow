---
title: >-
    شمارهٔ ۱۳۹
---
# شمارهٔ ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>ای طبع تو سرشته ز رادی و راستی</p></div>
<div class="m2"><p>دور از روان تو کژی و نارواستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میرا خدای راست سوی راستان بود</p></div>
<div class="m2"><p>زانست راست کار تو دائم که راستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چیزی که خواست بر تو ندید است خصم تو</p></div>
<div class="m2"><p>بر هرکسی بدیدی چیزی که خواستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از روزگار شاه فریدون تاکنون</p></div>
<div class="m2"><p>چون او بدین و دانش و دولت تو خاستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنانکه از کژی که بحیلت بخاستند</p></div>
<div class="m2"><p>از راستی و داد فروشان نشاستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندر جهان فکندی هولی که هیچ شاه</p></div>
<div class="m2"><p>از بیم تو برون نکند دست از آستی</p></div></div>