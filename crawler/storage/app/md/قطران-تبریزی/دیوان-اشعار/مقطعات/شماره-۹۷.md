---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>آن کس که بیک چشم زدن برد دل من</p></div>
<div class="m2"><p>زد آتش افروخته در آب و گل من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز نکند سوی من خسته نگاهی</p></div>
<div class="m2"><p>از آنکه نخواهد که شود شاد دل من</p></div></div>