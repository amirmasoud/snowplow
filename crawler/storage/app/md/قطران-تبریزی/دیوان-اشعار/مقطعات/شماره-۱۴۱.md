---
title: >-
    شمارهٔ ۱۴۱
---
# شمارهٔ ۱۴۱

<div class="b" id="bn1"><div class="m1"><p>همی گذشت بکوی اندرون بت مشکوی</p></div>
<div class="m2"><p>ز روی او شده جای نشاط و رامش کوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جفا نمود و بمن روی باز کرد بخشم</p></div>
<div class="m2"><p>ز خشم چون گل صد برگ برفروخته روی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برفت و ماند مرا دلفکار و زار بجای</p></div>
<div class="m2"><p>ز دیده بر دو رخ از جوی خون گشاد آموی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رخم بزردی زر است و تن بزاری زار</p></div>
<div class="m2"><p>دلم ز ناله چو نال است تن ز مویه چو موی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بعشق خوبان گر با تو دانش است مو رز</p></div>
<div class="m2"><p>بگرد خوبان گر با تو مردمی است مپوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازین نداد خداوند مهر خوبان را</p></div>
<div class="m2"><p>ز مردم آنکه خداوندشان نداده مجوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرآنکو گوی زنخدان نیکوان جوید</p></div>
<div class="m2"><p>دلش همیشه بود همچو پیش چوگان گوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر درست کند بخت نام و کنیت من</p></div>
<div class="m2"><p>ببوسه داد دل خویشتن بخواهم از اوی</p></div></div>