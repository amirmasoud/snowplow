---
title: >-
    شمارهٔ ۱۱۲
---
# شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>ای خسرو جوان و جوانبخت گاه نو</p></div>
<div class="m2"><p>در خور چو اول شب شوال ماه نو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرخنده باد بر تو و بر دوستان نو</p></div>
<div class="m2"><p>این مجلس پرآئین وین بزمگاه نو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هستی تو یادگار فریدون و همچو او</p></div>
<div class="m2"><p>آئین نو نمائی هر روز و راه نو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر ساعتیت باد یکی شهر و گنج نو</p></div>
<div class="m2"><p>هر ساعتیت باد یکی تاج و گاه نو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کینت مخالفان ترا کرد بند نو</p></div>
<div class="m2"><p>مهرت موافقان ترا کرد جاه نو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر روز دشمنان ترا داد رنج نو</p></div>
<div class="m2"><p>هر ماه دوستان ترا داد ماه نو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر ماه نو بدست تو بادایسر جهان</p></div>
<div class="m2"><p>هم بسته هم گشاده نگین و کلاه نو</p></div></div>