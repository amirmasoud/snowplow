---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>مرغ وفا برون ز جهان آشیان گرفت</p></div>
<div class="m2"><p>عنقا صفت ز عالم وحدت کران گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خون دل کنار زمین موج زد چنانکه</p></div>
<div class="m2"><p>ز آسیب موج دامن مغرب نشان گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طوفان درد کشتی دل را ز راه برد</p></div>
<div class="m2"><p>سیلاب غم خرابه جان در میان گرفت</p></div></div>