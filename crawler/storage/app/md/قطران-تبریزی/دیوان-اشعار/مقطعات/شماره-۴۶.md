---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>از بلای دهر جان شاه گیتی فرد باد</p></div>
<div class="m2"><p>جان بدخواهان او جفت بلا و درد باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رنگ گرد از روی رادی آب جود او بشست</p></div>
<div class="m2"><p>جان او بی رنگ باد و روی او بی گرد باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روز و شب در مجلس او بانک نوشانوش باد</p></div>
<div class="m2"><p>سال و مه بر درگه او بانگ برد ابردباد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوستانش را ز شادی روی دائم سرخ باد</p></div>
<div class="m2"><p>دشمنانش را ز زاری روی دائم زرد باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با سلامت جفت باد و با سعادت یار باد</p></div>
<div class="m2"><p>از ملامت دور باد و از ندامت فرد باد</p></div></div>