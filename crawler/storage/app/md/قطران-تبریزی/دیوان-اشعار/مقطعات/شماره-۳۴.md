---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>خدایگانا چشم دلت فروخته باد</p></div>
<div class="m2"><p>بآتش غم جان عدوت سوخته باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزر و گوهر شادی خری که دشمن تو</p></div>
<div class="m2"><p>خریده باد غم و خرمی فروخته باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپاه محنت بر دشمن تو تاخته باد</p></div>
<div class="m2"><p>و زو برنج و بلا کینه تو توخته باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بروز پاک جهان بر عدوت باد سیاه</p></div>
<div class="m2"><p>هوا بتیره شبان پیش تو فروخته باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مخالفان ترا سر بگرز کوفته باد</p></div>
<div class="m2"><p>منافقان ترا دل بتیر دوخته باد</p></div></div>