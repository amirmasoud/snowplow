---
title: >-
    شمارهٔ ۸۳
---
# شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>ایا چراغ شهان جهان امیر اجل</p></div>
<div class="m2"><p>بدست مایه پیروزی و بتیغ اجل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بابر و دریا ماند بنانت گاه سخا</p></div>
<div class="m2"><p>ببرق و صاعقه ماند سنانت گاه جدل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدانش ودهش و جود و داد و دولت و دین</p></div>
<div class="m2"><p>نیافرید عدیلت خدای عز و جل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلیل دولت و بختست و جای شکر و سپاس</p></div>
<div class="m2"><p>کنون بجای نیا یادگار میر اجل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر بشد پسری ده دهد خدای عوض</p></div>
<div class="m2"><p>و گر بشد خلقی صد دهد خدای بدل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو چرخ دولتی و هم بر تو ناز و نشاط</p></div>
<div class="m2"><p>مباد دور ز نزدت نشاط و ناز و دول</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه چرخ را بود از جستن شهاب زیان</p></div>
<div class="m2"><p>نه شاخ را رسد از رفتن شکوفه خلل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همیشه تا بفلک بر زحل بقا دارد</p></div>
<div class="m2"><p>همیشه تا بزمین بر بود ثبات جبل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ثبات ملکت تو چون جبل بود محکم</p></div>
<div class="m2"><p>بقای دولت تو بر دوام همچو زحل</p></div></div>