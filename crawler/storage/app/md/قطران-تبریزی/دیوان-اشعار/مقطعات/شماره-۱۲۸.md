---
title: >-
    شمارهٔ ۱۲۸
---
# شمارهٔ ۱۲۸

<div class="b" id="bn1"><div class="m1"><p>ای درم یافته از دست تو ارزانی</p></div>
<div class="m2"><p>ای کرم داشته ایزد بتو ارزانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو بگیتی در فاشی بگهر پاشی</p></div>
<div class="m2"><p>تو چو لقمانی هنگام سخندانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو توانائی داری بهمه چیزی</p></div>
<div class="m2"><p>لیکن خویشتن دیدن نتوانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنچه بستانی از خصم بدشواری</p></div>
<div class="m2"><p>به یکی بخشی در بزم به آسانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه میرانت همواره بفرمانند</p></div>
<div class="m2"><p>بهمه کاری بر جمله سلیمانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زی تو مهمانی دائم بود و گوهر</p></div>
<div class="m2"><p>نبود یک شب نزد تو به مهمانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه گفتار سخاوت را معنائی</p></div>
<div class="m2"><p>همه دعوی شجاعت را برهانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روبهان باشند در پیش تو بیچاره</p></div>
<div class="m2"><p>بمصاف اندر شیران بیابانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو از ان زندان آخر بمراد دل</p></div>
<div class="m2"><p>برهی روزی چون یوسف زندانی</p></div></div>