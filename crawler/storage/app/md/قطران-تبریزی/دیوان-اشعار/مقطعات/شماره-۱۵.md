---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>تا نگارین من سرائی گشت</p></div>
<div class="m2"><p>پیشه من غزل سرائی گشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا جدا گشتم از دو زلف دو تاش</p></div>
<div class="m2"><p>پشتم از باد غم دو تائی گشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم بی خواب من به آب اندر</p></div>
<div class="m2"><p>غرقه چون مرد آشنائی گشت</p></div></div>