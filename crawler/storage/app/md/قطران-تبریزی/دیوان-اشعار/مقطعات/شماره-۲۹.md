---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>خدایگان جهان را طبیب دارو داد</p></div>
<div class="m2"><p>موافق آمد از بهر آنکه نیکو داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر چه روزی موئی بکاست از تن شاه</p></div>
<div class="m2"><p>هزار سال بجسم و روانش نیرو داد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان و جان و دل و تنش هر سه باد فدا</p></div>
<div class="m2"><p>که هر سه چار مرا چون نگه کنم او داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ایا خدای ترا داده صد هزار هنر</p></div>
<div class="m2"><p>هم او بدشمن تو صد هزار آهو داد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مباد خسته یکی روز پشت و پهلوی تو</p></div>
<div class="m2"><p>که بخت خصم ترا درد پشت و پهلو داد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز نور خویشتن ایزد بیافرید ترا</p></div>
<div class="m2"><p>پس انگهت بسزا دست و تیغ و بازو داد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترا زمانه زبانی بداد گوهر بار</p></div>
<div class="m2"><p>ولی بچشم عدوی تو باز لولو داد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ایا مبارک داروی تو مبارک باد</p></div>
<div class="m2"><p>که دشمنان ترا بخت مرگ دارو داد</p></div></div>