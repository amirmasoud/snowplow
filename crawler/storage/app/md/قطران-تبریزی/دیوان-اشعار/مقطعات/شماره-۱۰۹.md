---
title: >-
    شمارهٔ ۱۰۹
---
# شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>خداوندا به پیروزی همه گیتی گشادی تو</p></div>
<div class="m2"><p>ز بخت و دولت پیروز ماه و سال شادی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آنگه باز کز مادر بپیروزی بزادی تو</p></div>
<div class="m2"><p>بهر جائی که می باشی بپیروزی نهادی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر داد و نشاط و جود چون بهرام دادی تو</p></div>
<div class="m2"><p>بدیدار سیاوشی و فر کیقبادی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بهر آن که بر گردون گردان اوفتادی تو</p></div>
<div class="m2"><p>بکار رادی و شادی شب و روز ایستادی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برادی بر دل قطران در شادی گشادی تو</p></div>
<div class="m2"><p>چنان کوهست شاد از تو ز دولت شاد بادی تو</p></div></div>