---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>شد اسیر آن بت کو دلبر و جنگ آور بود</p></div>
<div class="m2"><p>شمع صد مجلس و پیرایه صد لشگر بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگه بزم دلفروزتر از یوسف بود</p></div>
<div class="m2"><p>بگه رزم عدو سوزتر از حیدر بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرفرا کرد و یکی بار بیامد بر من</p></div>
<div class="m2"><p>آنکه خوبان همه هستند تن اوشان سر بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من بر او دیگر نگزینم یاری که مرا</p></div>
<div class="m2"><p>نه چنو دیگر باشد نه چنو دیگر بود</p></div></div>