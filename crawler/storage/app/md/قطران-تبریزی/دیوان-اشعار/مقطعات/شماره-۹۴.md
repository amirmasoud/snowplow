---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>سرنگون مانده است جانم زان دو زلف سرنگون</p></div>
<div class="m2"><p>لاله گون گشته است چشمم زان دو لعل لاله گون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بناگوشش ندیدم مه ندیدم بارور</p></div>
<div class="m2"><p>تا زنخدانش ندیدم چه ندیدم سرنگون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دهانش خیره ماندم من که چون گوید سخن</p></div>
<div class="m2"><p>از میانش خیره ماندم من که چون ناید برون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روزگرا از چشم بد دارد نگه او را که هست</p></div>
<div class="m2"><p>گرد رخسارش بخط جادوئی عمد افسون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای بزم را چو بهرام وی جنگ را چو بهمن</p></div>
<div class="m2"><p>فرخنده باد بر تو فرخنده ماه بهمن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی تو مباد روزی تا روز حشر گیتی</p></div>
<div class="m2"><p>دائم رسد بگوشت آواز مرگ دشمن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گیتی ترا پرستد شادی ترا فرستد</p></div>
<div class="m2"><p>تو چون بتی و گیتی ماننده برهمن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از طلعت تو اقبال فرخنده باد طلعت</p></div>
<div class="m2"><p>با دولت تو دولت پیوسته باد دامن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن کس که سوخته خواست از بخت خرمن تو</p></div>
<div class="m2"><p>چرخش ببرد دولت بختش بسوخت خرمن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ابری بروز بخشش ببری بروز کوشش</p></div>
<div class="m2"><p>میدان ترا سپهر است مجلس ترا نشیمن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جان و تن و دل من هر سه ز تست نازان</p></div>
<div class="m2"><p>باد فدای جانت جان و تن و دل من</p></div></div>