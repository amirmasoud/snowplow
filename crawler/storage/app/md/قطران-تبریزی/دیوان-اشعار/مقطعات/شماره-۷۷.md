---
title: >-
    شمارهٔ ۷۷
---
# شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>ای توئی بیچارگان را چاره و فریادرس</p></div>
<div class="m2"><p>ایزد از هر دو بند و سختی مر ترا فریادرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکه را رنجی بدو آمد تو برداری ازو</p></div>
<div class="m2"><p>بر ندارد رنج تو جز کردکار پاک و بس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز بکردار نشاط و ناز نگذاری قدم</p></div>
<div class="m2"><p>جز بگفتار وفا و جود نگشائی نفس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ایزدت فریادرس بادا بهر کاری کجا</p></div>
<div class="m2"><p>خلق عالم را بهر کاری توئی فریادرس</p></div></div>