---
title: >-
    شمارهٔ ۱۳۶
---
# شمارهٔ ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>تا جامه مدح تو بپوشید رهی</p></div>
<div class="m2"><p>با چرخ ستمکاره بکوشید رهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا از تو حدیث خوش نیوشید رهی</p></div>
<div class="m2"><p>از شیر ژیان بدوشید رهی</p></div></div>