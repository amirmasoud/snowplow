---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>آن بت که بهین لفظ بود دشنامش</p></div>
<div class="m2"><p>از حسن و لطافتست هفت اندامش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن بد که نمود بنده را بادامش</p></div>
<div class="m2"><p>بنمود بجنگ هفتخوان هم نامش</p></div></div>