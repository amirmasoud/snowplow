---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>نیمی ز تنم برنج و نیمی بشکنج</p></div>
<div class="m2"><p>کاری که کنی نخست با عقل بسنج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن کو بخورد درد و غم و رنج بگنج</p></div>
<div class="m2"><p>گنجش بورد بدو بماند همه رنج</p></div></div>