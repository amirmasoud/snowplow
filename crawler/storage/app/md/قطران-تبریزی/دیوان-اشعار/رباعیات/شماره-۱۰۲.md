---
title: >-
    شمارهٔ ۱۰۲
---
# شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>ای دوست بیا تا ره دیگر گیریم</p></div>
<div class="m2"><p>وازار و جفاها ز میان برگیریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مر یکدیگر را خود ببر اندر گیریم</p></div>
<div class="m2"><p>کینه بنهیم و صحبت از سر گیریم</p></div></div>