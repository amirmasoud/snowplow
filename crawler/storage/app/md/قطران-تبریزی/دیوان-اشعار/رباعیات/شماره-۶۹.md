---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>گر خیزد هیچ دیبه از هیچ طراز</p></div>
<div class="m2"><p>تارش گل و پودش ز می و مشک طراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن دیبه روی تست ای شمع طراز</p></div>
<div class="m2"><p>در وصل تو نیست هیچ زی شعر طراز</p></div></div>