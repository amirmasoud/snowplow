---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>هنگام سخا و جامه و جام تراست</p></div>
<div class="m2"><p>فرمان شهان و نامه و نام تراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اصل بد و نیک اندر ایام تراست</p></div>
<div class="m2"><p>تقدیر و مراد و بخت و هنگام تراست</p></div></div>