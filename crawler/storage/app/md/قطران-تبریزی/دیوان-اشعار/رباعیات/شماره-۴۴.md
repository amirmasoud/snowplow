---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>بر شاخ گل دولت تو خار نماند</p></div>
<div class="m2"><p>جز بخت تو هیچ بخت بیدار نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردانش را جز از تو بازار نماند</p></div>
<div class="m2"><p>جز داشتن ملک ترا کار نماند</p></div></div>