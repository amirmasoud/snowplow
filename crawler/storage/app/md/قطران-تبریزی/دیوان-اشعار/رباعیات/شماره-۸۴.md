---
title: >-
    شمارهٔ ۸۴
---
# شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>هم هست مرا هوای آن زلف بخم</p></div>
<div class="m2"><p>لیکن تب عشق از نخستین شده کم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده که نبیند و فرو بارد نم</p></div>
<div class="m2"><p>گر شادی و گریه بر تند هم خور غم</p></div></div>