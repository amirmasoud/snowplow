---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>نیمی ز تنم کمان شد و نیمی تیر</p></div>
<div class="m2"><p>نیمی ز دلم جوان شد و نیمی پیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسیار عنا خوردم بر چشمه شیر</p></div>
<div class="m2"><p>از چشمه شیر من برون آمد قیر</p></div></div>