---
title: >-
    شمارهٔ ۱۱۰
---
# شمارهٔ ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>هر گه که ترا بطبع پاک انگارم</p></div>
<div class="m2"><p>بهتر ز جهان و جان پاکت دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر تو بحدیث کس مرا نگذاری</p></div>
<div class="m2"><p>بالله که ترا بجان خود بگذارم</p></div></div>