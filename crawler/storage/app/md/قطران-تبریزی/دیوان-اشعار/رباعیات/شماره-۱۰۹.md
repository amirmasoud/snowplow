---
title: >-
    شمارهٔ ۱۰۹
---
# شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>بیمارم و ناردان لبت پندارم</p></div>
<div class="m2"><p>در بویه آبی تنت بیمارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر آبی و ناردان مرا بسپاری</p></div>
<div class="m2"><p>جان و تن خویشتن بتو بسپارم</p></div></div>