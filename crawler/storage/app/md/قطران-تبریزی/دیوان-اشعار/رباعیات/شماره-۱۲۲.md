---
title: >-
    شمارهٔ ۱۲۲
---
# شمارهٔ ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>پیوسته چو شمع در گدازم بی تو</p></div>
<div class="m2"><p>شب تا بسحر بسوز و سازم بی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه سوی شراب دست یازم بی تو</p></div>
<div class="m2"><p>نه سوی نشاط قد فرازم بی تو</p></div></div>