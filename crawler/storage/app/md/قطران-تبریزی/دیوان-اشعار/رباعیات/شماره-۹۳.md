---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>کوشم که بپوشم انده و نخروشم</p></div>
<div class="m2"><p>پیدا کند این دو دیده تا کی پوشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون گفته بخردان همی بنیوشم</p></div>
<div class="m2"><p>با زخم زمانه هرچه یکسر کوشم؟</p></div></div>