---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>ماننده شیر نر شمس الدین است</p></div>
<div class="m2"><p>بر خلق فکنده فر شمس الدین است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیک و بدو خیر و شر شمس الدین است</p></div>
<div class="m2"><p>شاهان سر بند و زر شمس الدین است</p></div></div>