---
title: >-
    شمارهٔ ۸۹
---
# شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>ما نامه عزل مهر تو بنوشتیم</p></div>
<div class="m2"><p>گسترده وصال چهر تو بنوشتیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکبار بدل ز مهر تو برگشتیم</p></div>
<div class="m2"><p>مهرت در ویدیم و صبوری کشتیم</p></div></div>