---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>چشمم ز غمت بهر عقیقی که بسفت</p></div>
<div class="m2"><p>بر چهره هزار گل ز رازم بشکفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رازی که دلم ز جان همی داشت نهفت</p></div>
<div class="m2"><p>اشکم بزبان حال با خلق بگفت</p></div></div>