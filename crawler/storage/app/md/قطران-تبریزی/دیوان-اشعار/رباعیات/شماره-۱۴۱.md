---
title: >-
    شمارهٔ ۱۴۱
---
# شمارهٔ ۱۴۱

<div class="b" id="bn1"><div class="m1"><p>صد بوسه بدادمش بزیر کف پای</p></div>
<div class="m2"><p>صد خواهش کردم که روی بر بنده نمای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشنود که بود رأی او دیگر جای</p></div>
<div class="m2"><p>خوبان همه صد روی بوند و یکرای</p></div></div>