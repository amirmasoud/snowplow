---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>ایزد چو بزرگ شهریاری نکند</p></div>
<div class="m2"><p>بر روی بدان نگاهداری نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بهر جهان گشادنت داشت نگاه</p></div>
<div class="m2"><p>ایزد بگزاف هیچ کاری نکند</p></div></div>