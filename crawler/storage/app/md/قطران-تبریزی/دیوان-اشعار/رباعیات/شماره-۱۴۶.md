---
title: >-
    شمارهٔ ۱۴۶
---
# شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>عناب لبا چو برگ عناب شدی</p></div>
<div class="m2"><p>بدرنگ بیامدی و بشتاب شدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نادیده منت تمام نایاب شدی</p></div>
<div class="m2"><p>چون رنگ بیامدی و چون آب شدی</p></div></div>