---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>از دوستی تو جز ندامت ناید</p></div>
<div class="m2"><p>بر تو ز بهی همی علامت ناید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از داروی تو بیمار سلامت ناید</p></div>
<div class="m2"><p>از تو ببرم که جز ملامت ناید</p></div></div>