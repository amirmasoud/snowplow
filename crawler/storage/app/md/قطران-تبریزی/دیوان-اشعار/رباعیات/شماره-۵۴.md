---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>تا مهر فکند بر من آن سرو بلند</p></div>
<div class="m2"><p>مهر همه عالم از دل من برکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون مهر ز چرخ بر زمی نور افکند</p></div>
<div class="m2"><p>مه را چه خطر باشد و که را چه گزند</p></div></div>