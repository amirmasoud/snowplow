---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>با من ز قضای بد برآشفت دیار</p></div>
<div class="m2"><p>آرام دلم یکی و خصمان بسیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درمانده تر از من اندر آفاق بیار</p></div>
<div class="m2"><p>مظلوم ز روزگار و مهجور زیار</p></div></div>