---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>تابنده چو خورشید ملک مملانست</p></div>
<div class="m2"><p>ماننده جمشید ملک مملانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرخنده چو افرید ملک مملانست</p></div>
<div class="m2"><p>چون ایزد جاوید ملک مملانست</p></div></div>