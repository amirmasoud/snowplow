---
title: >-
    شمارهٔ ۱۰۴
---
# شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>ما دل ز وفا و مهر تو برداریم</p></div>
<div class="m2"><p>بر آب نگار بی هده ننگاریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما دل بهوای آن کسی نسپاریم</p></div>
<div class="m2"><p>کز صحبت او بچشم مردم خواریم</p></div></div>