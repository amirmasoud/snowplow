---
title: >-
    شمارهٔ ۹۱
---
# شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>یکباره دل از هوای تو بگسستیم</p></div>
<div class="m2"><p>با آنکه بما وفا کند پیوستیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما سنگ شکیبائی بر دل بستیم</p></div>
<div class="m2"><p>از دام بجستیم و چه نیکو جستیم</p></div></div>