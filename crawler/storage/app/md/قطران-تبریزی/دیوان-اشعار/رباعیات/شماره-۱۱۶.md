---
title: >-
    شمارهٔ ۱۱۶
---
# شمارهٔ ۱۱۶

<div class="b" id="bn1"><div class="m1"><p>در کار جهانیان گشاد است از تو</p></div>
<div class="m2"><p>رنجات گنجست و جور داد است از تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر دل هرکه هست یاد است از تو</p></div>
<div class="m2"><p>شادیش مباد آنکه نه شاد است از تو</p></div></div>