---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>تا غایب شد بت از کنار شمنش</p></div>
<div class="m2"><p>می خون گردد بتن ز غایت شدنش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر مژده دهد کسی ز باز آمدنش</p></div>
<div class="m2"><p>پر در بکنم کنار همچون دهنش</p></div></div>