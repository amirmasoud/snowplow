---
title: >-
    شمارهٔ ۱۱۷
---
# شمارهٔ ۱۱۷

<div class="b" id="bn1"><div class="m1"><p>ای دوست مرا بدشمنان دادی تو</p></div>
<div class="m2"><p>وز مهر و هوای دشمنان شادی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر زینت بتخانه نوشادی تو</p></div>
<div class="m2"><p>یکباره ز چشم من بیفتادی تو</p></div></div>