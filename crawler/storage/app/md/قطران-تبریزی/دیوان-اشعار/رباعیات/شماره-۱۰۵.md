---
title: >-
    شمارهٔ ۱۰۵
---
# شمارهٔ ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>تا همبر من نشسته خاموشم</p></div>
<div class="m2"><p>چون یاد آرم فراق تو بخروشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از من نرهی که هست چندان هوشم</p></div>
<div class="m2"><p>کان را که بدل خرم بجان نفروشم</p></div></div>