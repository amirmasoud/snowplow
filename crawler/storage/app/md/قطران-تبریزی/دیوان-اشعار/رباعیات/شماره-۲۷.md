---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>بر ملک فکنده برخ شمس الامراست</p></div>
<div class="m2"><p>وز ملک ربوده نرخ شمس الامراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهتر ز ملوک کرخ شمس الامراست</p></div>
<div class="m2"><p>میران ز می اندو چرخ شمس الامراست</p></div></div>