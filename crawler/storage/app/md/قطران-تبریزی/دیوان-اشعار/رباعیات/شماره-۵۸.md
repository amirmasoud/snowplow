---
title: >-
    شمارهٔ ۵۸
---
# شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>تا تو نکنی بدشمن و دوست نظر</p></div>
<div class="m2"><p>نه نفع رسد بدشمن و دوست نه ضر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاد است موافق تو با گنج ظفر</p></div>
<div class="m2"><p>زار است مخالف تو با رنج خطر</p></div></div>