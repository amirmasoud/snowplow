---
title: >-
    شمارهٔ ۹۶
---
# شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>گویند مرا ز عشق آن تازه صنم</p></div>
<div class="m2"><p>نه خندی و نه ز دیدگان باری نم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم متحیرم نه شادم نه دژم</p></div>
<div class="m2"><p>کنم نیست دل و ز دل بود شادی و غم</p></div></div>