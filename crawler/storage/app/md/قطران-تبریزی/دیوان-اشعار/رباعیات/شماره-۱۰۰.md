---
title: >-
    شمارهٔ ۱۰۰
---
# شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>خواهم که همیشه با تو پیوسته بوم</p></div>
<div class="m2"><p>دل با دل تو بدوستی بسته بوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بی تو بوم ز درد غم خسته بوم</p></div>
<div class="m2"><p>چون با تو بوم ز دردها رسته بوم</p></div></div>