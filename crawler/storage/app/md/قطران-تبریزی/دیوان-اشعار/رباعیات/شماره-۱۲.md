---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>روی تو بشبهای سیه روز منست</p></div>
<div class="m2"><p>عشقت بخزان بهار ونوروز منست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قد تو دلارا و دل افروز منست</p></div>
<div class="m2"><p>گیتی بمراد بخت پیروز منست</p></div></div>