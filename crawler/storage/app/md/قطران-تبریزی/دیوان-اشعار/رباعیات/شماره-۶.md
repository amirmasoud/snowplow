---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>مادر چو بزاد آن بدعا خواسته را</p></div>
<div class="m2"><p>کرده است نشان آن مه پیراسته را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خالیست میان آن مه ناکاسته را</p></div>
<div class="m2"><p>گر مهر نهد کسی چنین خواسته را</p></div></div>