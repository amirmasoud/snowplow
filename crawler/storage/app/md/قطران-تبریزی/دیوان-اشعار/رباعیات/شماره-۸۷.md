---
title: >-
    شمارهٔ ۸۷
---
# شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>تا کی ز فراق دوست فریاد کنم</p></div>
<div class="m2"><p>از آه درون رخنه بپولاد کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیداد کنی بر من و من داد کنم</p></div>
<div class="m2"><p>بر یاد رخ خوب تو دل شاد کنم</p></div></div>