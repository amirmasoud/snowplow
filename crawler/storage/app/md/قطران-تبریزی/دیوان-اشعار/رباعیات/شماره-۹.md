---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>یک نیم دلم کلبچه یک نیم کباب</p></div>
<div class="m2"><p>یک نیمه در آتش و دگر نیمه در آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مسکین دل من خراب کردی بعذاب</p></div>
<div class="m2"><p>اکنون تو همی خراج خواهی ز خراب</p></div></div>