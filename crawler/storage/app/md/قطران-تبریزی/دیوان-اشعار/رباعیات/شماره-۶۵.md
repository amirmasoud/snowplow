---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>تب خاله مرا نمود معشوقه ز ناز</p></div>
<div class="m2"><p>هر دم بلبان سرخش انگشت فراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون کودک شیرخواره از حرص وز آز</p></div>
<div class="m2"><p>انگشت همی مزم به شبهای دراز</p></div></div>