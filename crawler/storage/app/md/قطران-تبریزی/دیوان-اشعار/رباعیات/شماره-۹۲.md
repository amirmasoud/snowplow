---
title: >-
    شمارهٔ ۹۲
---
# شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>هندو بچه ای ببرد از راه دلم</p></div>
<div class="m2"><p>در چاه بلا فکند ناگاه دلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بر نارد ببوسه از چاه دلم</p></div>
<div class="m2"><p>یکباره برآید از دلم آه دلم</p></div></div>