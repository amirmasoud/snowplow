---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>تا من بودم بود مرا دولت جفت</p></div>
<div class="m2"><p>وین دولت بیدارم یک روز نخفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بد گوی مرا طعنه چه بتواند گفت</p></div>
<div class="m2"><p>الماس بابریشم که بتواند سفت</p></div></div>