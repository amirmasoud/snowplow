---
title: >-
    شمارهٔ ۱۲۹
---
# شمارهٔ ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>گر سوی هوا دلم همی جوید راه</p></div>
<div class="m2"><p>پیوند مرا سوی زمین آمد ماه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سنبل و بر سمن بیفزاید جاه</p></div>
<div class="m2"><p>کز سنبل و از سمن شود ماه تباه؟</p></div></div>