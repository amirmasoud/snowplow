---
title: >-
    شمارهٔ ۷۶
---
# شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>سه چیز ترا سه چیز داده است جمال</p></div>
<div class="m2"><p>خد را خط و زلف را گل و عارض خال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سه چیز من از سه چیز برده است مثال</p></div>
<div class="m2"><p>دل ز آتش و چشم از آب و دیده از خال</p></div></div>