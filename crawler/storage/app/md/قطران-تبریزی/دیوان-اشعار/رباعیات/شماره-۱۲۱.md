---
title: >-
    شمارهٔ ۱۲۱
---
# شمارهٔ ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>از دیده میان رود خونم بی تو</p></div>
<div class="m2"><p>گوئی که به آتش اندرونم بی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از فکرت خویشتن برونم بی تو</p></div>
<div class="m2"><p>ای دوست بیا ببین که چونم بی تو</p></div></div>