---
title: >-
    شمارهٔ  ۵۶ - در غزل است
---
# شمارهٔ  ۵۶ - در غزل است

<div class="b" id="bn1"><div class="m1"><p>بنگر که چه روزگار دارم</p></div>
<div class="m2"><p>کان چهره چون نگار دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دل غم و غم سگال سوزم</p></div>
<div class="m2"><p>در بر می و می گسار دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشگفت که باغ وار خندم</p></div>
<div class="m2"><p>تا وصل چو تو بهار دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورشید به صیدگاه مشرق</p></div>
<div class="m2"><p>زان زلف خوشت شکار دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای عشق تو دم شمار عاشق</p></div>
<div class="m2"><p>غمها ز تو بیشمار دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منگر تو به باغ وصل امسال</p></div>
<div class="m2"><p>کز هجر تو داغ پار دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوگند مخور به نیک عهدی</p></div>
<div class="m2"><p>کز جور تو دل فگار دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر کوفه خوری به جای سوگند</p></div>
<div class="m2"><p>حاشا گرت استوار دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از بهر یکی سخن دو سه ماه</p></div>
<div class="m2"><p>چون دیده برانتظار دارم؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آغاز کنم حدیث گوئی</p></div>
<div class="m2"><p>بسیار مگو که کار دارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای جان قوامی به دل اندر</p></div>
<div class="m2"><p>غمهات قیامه وار دارم</p></div></div>