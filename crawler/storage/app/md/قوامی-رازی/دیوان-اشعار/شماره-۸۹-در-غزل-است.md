---
title: >-
    شمارهٔ  ۸۹ - در غزل است
---
# شمارهٔ  ۸۹ - در غزل است

<div class="b" id="bn1"><div class="m1"><p>هر کو چو تو دلستان ندارد</p></div>
<div class="m2"><p>خورشید شکر فشان ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دست غم عشق تو جانا</p></div>
<div class="m2"><p>آن جان ببرد که جان ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشکی که ز شب پدید گردد</p></div>
<div class="m2"><p>جز زلف تو ترجمان ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرغی که ز آفتاب زاید</p></div>
<div class="m2"><p>جز حسن تو آشیان ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خورشید چو روی تو از آن نیست</p></div>
<div class="m2"><p>کز زلف تو سایه بان ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دادی به غلامی تو اقرار</p></div>
<div class="m2"><p>مسکین چه کند زبان ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر چند چو سیمرغ وصالت</p></div>
<div class="m2"><p>نامیست که خود نشان ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک کنج نماندست که دروی</p></div>
<div class="m2"><p>صد بنده به رایگان ندارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بستان رخت بر چمن لهو</p></div>
<div class="m2"><p>جز عارضت ارغوان ندارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باران غمم ز بام اندوه</p></div>
<div class="m2"><p>الا مژه ناودان ندارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در عشقم گوشمال دادی</p></div>
<div class="m2"><p>شاید، که ادب زیان ندارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کم دار قوام سیم باری</p></div>
<div class="m2"><p>دانی که قوامی آن ندارد</p></div></div>