---
title: >-
    شمارهٔ  ۱۱۴ - در توحید و پند و زهد گوید
---
# شمارهٔ  ۱۱۴ - در توحید و پند و زهد گوید

<div class="b" id="bn1"><div class="m1"><p>ای از خدا عزوجل بر تو آفرین</p></div>
<div class="m2"><p>تا آفریده چون تو دگر گیتی آفرین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن را کن آفرین که جهان را بیافرید</p></div>
<div class="m2"><p>تاباشد از ملائکه بر جانت آفرین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن پادشا که حلقه درگاه ملک او</p></div>
<div class="m2"><p>هفصد هزار بار به از چرخ هفتمین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شایسته عبادت و معبود مملکت</p></div>
<div class="m2"><p>دارنده مکان و نگارنده مکین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان پروری که بی قلم و دست و آلتی</p></div>
<div class="m2"><p>اندر رحم نگار کند صورت جنین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خردتر مگس بدهد نوش خوشگوار</p></div>
<div class="m2"><p>در کمترینه کرم نهد گوهر ثمین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوز به قهر برگ درختان به مهرگان</p></div>
<div class="m2"><p>سازد ز لطف نغمه مرغان به فرودین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برق از عتاب او شده چون تیغ در مصاف</p></div>
<div class="m2"><p>رعد از نهیب او شده چون شیر در عرین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با ابر و باد گفته که در باغ و بوستان</p></div>
<div class="m2"><p>نقاشی آن چنان کن و فراشی این چنین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون صورت پری گل ازو شد به نوبهار</p></div>
<div class="m2"><p>زلف بنفشه چون خم چوگان حور عین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از شاخ گل به قدرت او بانگ عندلیب</p></div>
<div class="m2"><p>همچون نوای بهربد از چنگ رامتین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کبکان به کوهسار خرامان به قدرتش</p></div>
<div class="m2"><p>چون لعبتان چین شده خندان و لاله چین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زاغان فراز برف زمستان ز صنعتش</p></div>
<div class="m2"><p>چون لشکری رسیده زهندوستان به چین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گردون پرستاره زصنع بدیع او</p></div>
<div class="m2"><p>چون بوستان پر گل و نسرین و یاسمین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ازروز و شب بساخت جهان را دو پیرهن</p></div>
<div class="m2"><p>خورشید و مه درو به تکاپوی هان و هین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>می برکنند سر ز گریبان آسمان</p></div>
<div class="m2"><p>دامن کشان ز ظلمت و از نور در زمین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پای جهان ز دامن شب چون نهان کند</p></div>
<div class="m2"><p>گوید به صبح دست برون کن ز آستین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در صنعهاش عاجز و حیران بمانده اند</p></div>
<div class="m2"><p>مردان تیز فهم وبزرگان دوربین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای خلق را عبادت تو نصرت و فتوح</p></div>
<div class="m2"><p>ما را توئی به فضل و کرم ناصر و معین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>انگشت کاینات به تقدیر چون توئی</p></div>
<div class="m2"><p>انگشتری فلک کند و مشتری نگین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تسبیح و شکر و یاد تو خوشتر بود مرا</p></div>
<div class="m2"><p>که اندر بهشت جوی می و شیر و انگبین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>با پادشاهی تو چه خیزد ز من گدا</p></div>
<div class="m2"><p>بیچاره ذلیلم و درمانده مهین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>من کیستم که پیش تو سر بر زمین نهم</p></div>
<div class="m2"><p>یا باشدم به درگه تو ناله حزین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خورشید را که هست کله گوشه برفلک</p></div>
<div class="m2"><p>هرشب ز پیش تو به زمین برنهد جبین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای از دم هوی و هوس روز و شب دوان</p></div>
<div class="m2"><p>در بند آن که تا تن لاغر کنی سمین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از حرص و شهوت است دلت را به هم رهی</p></div>
<div class="m2"><p>کش غول بر یسار بود دیو بر یمین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در طاعت خدای دو تا باش چون کمان</p></div>
<div class="m2"><p>کاندر ره تو دیو لعین است در کمین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آنجا سوار باش که میدان طاعت است</p></div>
<div class="m2"><p>تا آسمانت اسب شود آفتاب زین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ایزدپرست شو چو بدوت استعانت است</p></div>
<div class="m2"><p>«ایاک نعبد» است پس «ایاک نستعین »</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر خود فرشته ایست مقرب ز ساق عرش</p></div>
<div class="m2"><p>چون نگردد به ایزد دیوی بود لعین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نتوان شدن به پای غلط در ره خدای</p></div>
<div class="m2"><p>نگرفت کس به دست گمان دامن یقین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بر راه جهل چند نشینی اسیروار</p></div>
<div class="m2"><p>چون در ره خرد نشوی شهسوار دین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خود دانی این قدر که بهم راست نیستند</p></div>
<div class="m2"><p>میران شه نشان و گدایان ره نشین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ابلیس وار ناکس و نامعتمد مباش</p></div>
<div class="m2"><p>گر همچو جبرئیل امین نیستی امین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آخر تو را که کرد نصیحت مرا بگوی</p></div>
<div class="m2"><p>کز رنج نفس باش به جان بلا قرین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>رحمت مخواه وز در رحمن همی گریز</p></div>
<div class="m2"><p>لعنت پسند و خدمت شیطان همی گزین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>می برکشی ز جانور پوست تا تو را</p></div>
<div class="m2"><p>در پوستین بود تن و اندام نازنین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چون پوستین ز قاقم و سنجاب ساختی</p></div>
<div class="m2"><p>آن کن که مرتو را ندرد خلق پوستین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از بهر دین تو را چو نصیحت کند کسی</p></div>
<div class="m2"><p>در دل مگیر کین و در ابرو میار چین</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کین دار دین ندارد پیش از تو گفته شد</p></div>
<div class="m2"><p>آن راست تاج دین که برو نیست داغ کین</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از هول دوزخ و خطر راه رستخیز</p></div>
<div class="m2"><p>آگه نه ای که دل به هوی کرده ای رهین</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تدبیر کن که پیش تو در راه دوزخ است</p></div>
<div class="m2"><p>دریای آتشین ز پس کوه آهنین</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بیدار جز به مرگ نخواهی همی شدن</p></div>
<div class="m2"><p>صبر آر تا درآئی از این خواب سهمگین</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نزدیک کژ روان نتوان یافتن خبر</p></div>
<div class="m2"><p>از جای راستان و ز مردان راستین</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گاو است و شیر در ره تو باش تا رسد</p></div>
<div class="m2"><p>زخم سروت بر سر و چنگال برسرین</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چون نعمت خدای خوری شکر او گزار</p></div>
<div class="m2"><p>گر نه ز کبر و خشم و حسد گشته ای عجین</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>او را چه از شکایت و شکر جهانیان</p></div>
<div class="m2"><p>مستغنی و غنی است ز نفرین و آفرین</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ناشاکران چون تو خداوند را بسی است</p></div>
<div class="m2"><p>گرد جهان دوان چو سگان گرد پارگین</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ای گشته سر جریده پیران شوخ چشم</p></div>
<div class="m2"><p>بشنو نصیحتی ز جوانان شرمگین</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>برگی بکن که لشکر عمر تو کوچ کرد</p></div>
<div class="m2"><p>گردی نشست بر سرت از گردش سنین</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>توحید و زهد کارقوامی است و آن منم</p></div>
<div class="m2"><p>کز غایت سخن شده ام آیتی مبین</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>امروز پادشاه سخن در جهان منم</p></div>
<div class="m2"><p>گنج قناعت است مرا در خرد دفین</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>توحید و زهد هست سپاهی گران مرا</p></div>
<div class="m2"><p>توفیق ایزد است حصار«ی» مرا حصین</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بر درگه سرای سخن پادشاهوار</p></div>
<div class="m2"><p>در دین زنند نوبت من تا به یوم دین</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>آن نانبا منم به سخن پادشا شده</p></div>
<div class="m2"><p>دکان گرفته بر زبر گنبد برین</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گندم مرا ز مزرعه کاف و هی بود</p></div>
<div class="m2"><p>پرورده کشتهاش ز کاریز یی و سین</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>از چرخ خاطر است مرا آسیای عقل</p></div>
<div class="m2"><p>از چشم فکرت است مرا چشمه معین</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>در ناوه ضمیر خمیر لطبف من</p></div>
<div class="m2"><p>به سرشت آنکه آدم را او سرشت طین</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نانی که من ز آتش چون ارغوان پزم</p></div>
<div class="m2"><p>آرد چو زعفران طرب اندر دل حزین</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>زین نان وین سخن نتوانند گفت خلق</p></div>
<div class="m2"><p>نانت نه گندمین سخنانت نه مردمین</p></div></div>