---
title: >-
    شمارهٔ  ۸۶ - در غزل واشارت بمدح حسن ابوالعمید است
---
# شمارهٔ  ۸۶ - در غزل واشارت بمدح حسن ابوالعمید است

<div class="b" id="bn1"><div class="m1"><p>جانا دل من تو را مرید است</p></div>
<div class="m2"><p>روی تو مرا هزار عید است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن را که تو شاهدی نگارا</p></div>
<div class="m2"><p>بی شک دانم که او شهید است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در هجر و وصالت غم است و شادی</p></div>
<div class="m2"><p>کی بی کی مگر وعد و وعید است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ماوصل تو عن قریب یابیم</p></div>
<div class="m2"><p>زیرا که فراق تو بعید است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما طاقت هجر تو نداریم</p></div>
<div class="m2"><p>زیرا که عذاب او شدید است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتی که بسنده کن بدیدار</p></div>
<div class="m2"><p>کت در ره عشق دل برید است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای دوست نیم از این مرا بس</p></div>
<div class="m2"><p>بر قول تو خود کرا مزید است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روی تو به نیکوئی بعینه</p></div>
<div class="m2"><p>چون خوی حسن ابوالمعید است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن خواجه خواجه زاده کز جود</p></div>
<div class="m2"><p>از جمله همسران فرید است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شاد است روان آن پدر زو</p></div>
<div class="m2"><p>زیرا پسری خوب و رشید است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرگز نبود چنو حسودش</p></div>
<div class="m2"><p>زیراکه شقی نه چون سعید است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در شهر خرد رئیس بادا</p></div>
<div class="m2"><p>تا شحنه نه همنام عمید است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در خدمت اوباش قوامی</p></div>
<div class="m2"><p>ممدوح مدانش که مرید است</p></div></div>