---
title: >-
    شمارهٔ  ۲۷ - در مدح بزرگی و تقاضای صلتی از او
---
# شمارهٔ  ۲۷ - در مدح بزرگی و تقاضای صلتی از او

<div class="b" id="bn1"><div class="m1"><p>مها چو گاه سخا دست تو دلیر بود</p></div>
<div class="m2"><p>به مدحت تو زبان زمانه چیر بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو خواجگی طلبی همتی چو شیر بیار</p></div>
<div class="m2"><p>که از سخاست که هر دد غلام شیر بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مدام خدمت من به اشتاب خواهی و زود</p></div>
<div class="m2"><p>اگر چه از تو صلت با درنگ و دیر بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حسود زیر و زبر کی بود؟ در آن ساعت</p></div>
<div class="m2"><p>که دست تو زبر و دست بنده زیر بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه هر که مدح تو گوید چو من تواند بود</p></div>
<div class="m2"><p>نه نره شیر بود هر که در کویر بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زجود خویش پرم کن که بس تهی دستم</p></div>
<div class="m2"><p>که گرسنه شکم آن نیکتر که سیر بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پس ار دلیر عتابی کنم شگفت مدار</p></div>
<div class="m2"><p>که هست بنده تهی و تهی دلیر بود</p></div></div>