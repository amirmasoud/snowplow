---
title: >-
    شمارهٔ  ۱۹ - در شکایت از عدم وصول حوالتی که برای او شده است
---
# شمارهٔ  ۱۹ - در شکایت از عدم وصول حوالتی که برای او شده است

<div class="b" id="bn1"><div class="m1"><p>ای خواجه تا کی از تو بیداد و ظلم بر من</p></div>
<div class="m2"><p>تا چندمان دوانی « . . . » به گرد خرمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کردی مرا حواله با گنده ای و پیری</p></div>
<div class="m2"><p>این عار شهر و روستا آن ننگ کوی و برزن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از عشوه ها نگفتم چون پسته کنندم</p></div>
<div class="m2"><p>در دستشان می فکن در پای سیم افکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن گویدم بدین شو وین گویدم بدان شو</p></div>
<div class="m2"><p>من در میانه حیران تو بر کناره برزن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون گویمش که دستار آن گویدم که اینک</p></div>
<div class="m2"><p>بردار و ریشه می کن یعنی که ریش می کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای صدر دین که بادی با خیر و خرمیها</p></div>
<div class="m2"><p>اندر میانه ما منداز شر و شیون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما را حواله ای کن زین به که نیست پیدا</p></div>
<div class="m2"><p>آن زن به مزد مسکین در هیچ جا و مسکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا کی دهم صداعت در دم این پلیدان</p></div>
<div class="m2"><p>که آخر بهشت نتوان برساختن ز گلخن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گندم بدول بادا با تو مرا همیشه</p></div>
<div class="m2"><p>شادانه باد جوجو مرجوی ارزن ارزن</p></div></div>