---
title: >-
    شمارهٔ  ۷۸ - در غزل است
---
# شمارهٔ  ۷۸ - در غزل است

<div class="b" id="bn1"><div class="m1"><p>ای عشق غم شد از تو مرا شادی</p></div>
<div class="m2"><p>چون از همه جهان به من افتادی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنده شد از تو مردم آزاده</p></div>
<div class="m2"><p>روی از تو نکرد کس آزادی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرمایه بخش روی چو گلبرگی</p></div>
<div class="m2"><p>پیرایه دار زلف چو شمشادی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گه پاسبان لاله سیرابی</p></div>
<div class="m2"><p>گه پرده دار سوسن آزادی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عقل سخن شناس جهان دیده</p></div>
<div class="m2"><p>شاگرد تو است با همه استادی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیرینه یار غار توم گفتی</p></div>
<div class="m2"><p>بیزارم از تو گر همه همزادی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از رفتن تو شادی مرا زاید</p></div>
<div class="m2"><p>کز زادن تو مردمرا شادی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در عاشقی ز عشوه گلبرگی</p></div>
<div class="m2"><p>ما را چه خار بود که ننهادی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتی قوامیا چه زیان کردی</p></div>
<div class="m2"><p>تا دست را به صحبت ما دادی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عشقا من از تو داغ بسی دارم</p></div>
<div class="m2"><p>هیچ اندهت مباد چو من بادی</p></div></div>