---
title: >-
    شمارهٔ  ۸۸ - در غزل واشارت باسم خواجه اوحد است
---
# شمارهٔ  ۸۸ - در غزل واشارت باسم خواجه اوحد است

<div class="b" id="bn1"><div class="m1"><p>ز عشق تو کشیدم گرد دل سد</p></div>
<div class="m2"><p>نهادم در میان سینه مسند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به مسند در نشین فرمان همی ران</p></div>
<div class="m2"><p>که دارد حسن تو ملک مؤید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشان خال تو بر روی رنگین</p></div>
<div class="m2"><p>بدان ماند که بر آتش نهی ند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فراز عارض تو خط مشکین</p></div>
<div class="m2"><p>چو بر لوح نگارین شکل ابجد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بده چون بوسه ای خواهم که دانی</p></div>
<div class="m2"><p>نشاید کرد قول دوستان رد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو را بهتر که کم باشد رقیبت</p></div>
<div class="m2"><p>بلی بهتر بود زر مجرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنفشه گرد گل بشکفت مندیش</p></div>
<div class="m2"><p>خط آور خوبتر باشد ز امرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز بهر بوسه جان و دل و مال</p></div>
<div class="m2"><p>ربودی ای نگار نارون قد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به جان و مال و دل یک بوسه ندهی</p></div>
<div class="m2"><p>نگارینا مبر یک باره از حد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مکن بیگانگی جانا که هستیم</p></div>
<div class="m2"><p>من آن تو، تو آن خواجه اوحد</p></div></div>