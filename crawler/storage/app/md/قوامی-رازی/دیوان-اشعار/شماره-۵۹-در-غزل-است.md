---
title: >-
    شمارهٔ  ۵۹ - در غزل است
---
# شمارهٔ  ۵۹ - در غزل است

<div class="b" id="bn1"><div class="m1"><p>لشکر کشید عشق و مرا در میان گرفت</p></div>
<div class="m2"><p>خواهند مردمانم از این در زبان گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر زبان خلق فتادم ز دست عشق</p></div>
<div class="m2"><p>تابایدم بلا به دراین و آن گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جانا غلام عشق تو گشتم به رایگان</p></div>
<div class="m2"><p>می بایدت مرا به عنایت عنان گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آزاد و پادشاه تن خویشم ای نگار</p></div>
<div class="m2"><p>آخر مرا به بنده همی بر توان گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نالنده گشت بلبل عشقم که مر تو را</p></div>
<div class="m2"><p>طاوس حسن بر سر سرو آشیان گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با آفتاب و ماه و ستاره است آسمان</p></div>
<div class="m2"><p>گوئی که نسخت رخ تو آسمان گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون خط دمید گر درخت عشق نعره زد</p></div>
<div class="m2"><p>کامد سپاه زاغ وصف بوستان گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برکند عشق خیمه و از لشکر جمال</p></div>
<div class="m2"><p>ترکان گریختند که هندو جهان گرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ایمن نشسته بودم در کنج عافیت</p></div>
<div class="m2"><p>آمد بلای عشق و مرا ناگهان گرفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از گوشه ای برآمد از این شوخ دلبری</p></div>
<div class="m2"><p>بربود دل زدستم و پای از میان گرفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باز شکار جوی قوامی ندیده ای؟</p></div>
<div class="m2"><p>شاهین عشق کبک دلت را چنان گرفت</p></div></div>