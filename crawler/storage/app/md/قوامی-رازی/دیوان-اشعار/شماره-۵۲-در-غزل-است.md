---
title: >-
    شمارهٔ  ۵۲ - در غزل است
---
# شمارهٔ  ۵۲ - در غزل است

<div class="b" id="bn1"><div class="m1"><p>ای در دل عاشقان تو درد</p></div>
<div class="m2"><p>و ای چهره دوستان تو زرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از جمله عاشقان منم طاق</p></div>
<div class="m2"><p>وز باره نیکوان توئی فرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر برزند از غم تو هر شب</p></div>
<div class="m2"><p>از برج دلم ستاره درد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا چند ره تو بایدم رفت</p></div>
<div class="m2"><p>تاچند غم تو بایدم خورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک روز برم نیائی آخر</p></div>
<div class="m2"><p>ای عشوه فروش ناجوانمرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وقت تو عزیز باشد ای جان</p></div>
<div class="m2"><p>خلوت نتوان ز تو طلب کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در حجره عاشق آی و منشین</p></div>
<div class="m2"><p>بر پای سلام کن و برگرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر چند که کودکی و نادان</p></div>
<div class="m2"><p>چون طیره دهی مرا زهی مرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بد مهر کسی و بی محابا</p></div>
<div class="m2"><p>کز مرد همی برآوری گرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فرزند که بودی از که زادی</p></div>
<div class="m2"><p>گوئیت کدام دایه پرورد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درد او جفا تو از قوامی</p></div>
<div class="m2"><p>بی مهره به مهر برده ای نرد</p></div></div>