---
title: >-
    شمارهٔ  ۳ - ترجیع بندیست در مدح معمار الحرمین منتجب الدین حسین بن ابی سعد ورامینی رحمة الله
---
# شمارهٔ  ۳ - ترجیع بندیست در مدح معمار الحرمین منتجب الدین حسین بن ابی سعد ورامینی رحمة الله

<div class="b" id="bn1"><div class="m1"><p>آتش عشق آفتی عجب است</p></div>
<div class="m2"><p>عشق را اولین نظر سبب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل عاشق به زیر حقه عشق</p></div>
<div class="m2"><p>همچو مهره به دست بوالعجب است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روز و شب آرزوی معشوقان</p></div>
<div class="m2"><p>ازپس یکدگر چو روز و شب است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنچه خاص منست لاتسأل</p></div>
<div class="m2"><p>که از آن سرو قدنوش لب است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلبری خوش لبی نگارینی</p></div>
<div class="m2"><p>که آدمی خلقت و پری نسب است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زلف او را که طبع مشتاق است</p></div>
<div class="m2"><p>خط او را که عشق در طلب است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن نه زلف است رایت حسن است</p></div>
<div class="m2"><p>وان نه خط است آیت طرب است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر بر دیگری شوم گوید</p></div>
<div class="m2"><p>خیش چون دارد آنکه را قصب است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ور ازو بوسه بایدم گوید</p></div>
<div class="m2"><p>انگبین چون خوری تو را که تب است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>او نداند مگر قوامی را</p></div>
<div class="m2"><p>کز کسان امیر منتجب است</p></div></div>
<div class="b2" id="bn11"><p>تاج آزادگان امیر حسین</p>
<p>که ندارد نظیر در کونین</p></div>
<div class="b" id="bn12"><div class="m1"><p>زلف معشوق مشک پاش منست</p></div>
<div class="m2"><p>غمزه دوست دور باش منست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هرشب از یاد روی او تا روز</p></div>
<div class="m2"><p>از گل و نسترن فراش منست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سال و مه بارگیر انده عشق</p></div>
<div class="m2"><p>لاشه جسم و جان لاش منست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لرزه بر من فتد ز دیدن دوست</p></div>
<div class="m2"><p>حسن او گوئی ارتعاش منست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>غزلی چون شکر همی گویم</p></div>
<div class="m2"><p>زان دو لب این قدر تراش منست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عنبر لاله پوش پرشکنش</p></div>
<div class="m2"><p>نافه عشق مشک پاش منست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در جهان شاهنامه دیگر</p></div>
<div class="m2"><p>خلق را سرگذشت فاش منست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای قوامی سرای عقل؛ ترا</p></div>
<div class="m2"><p>حجره عشق پرقماش منست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عقل ده روزه گر اتابک توست</p></div>
<div class="m2"><p>عشق دیرینه خواجه تاش منست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دل من تا بود مفتش عشق</p></div>
<div class="m2"><p>مدحت میر افتتاش منست</p></div></div>
<div class="b2" id="bn22"><p>تاج آزادگان امیرحسین</p>
<p>که ندارد نظیر در کونین</p></div>
<div class="b" id="bn23"><div class="m1"><p>داده ام دل به دست نادانی</p></div>
<div class="m2"><p>شده زین کار چون پشیمانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پای را در رهی نهاد ستم</p></div>
<div class="m2"><p>که نیرزد درو سری نانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای دل از غم مجه که نگزیرد</p></div>
<div class="m2"><p>یوسفی را ز چاه و زندانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هیچ دردی به عالم اندر نیست</p></div>
<div class="m2"><p>کش نیاید به دست درمانی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جامه روز را همی دوزد</p></div>
<div class="m2"><p>هر سپیده دمی گریبانی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>عشق او خونبهای این دل من</p></div>
<div class="m2"><p>از دلی نیکتر بود جانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای قوامی به یک تن تنها</p></div>
<div class="m2"><p>منه از عشق میل و بالانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>رو که ایدر نداند آوردن</p></div>
<div class="m2"><p>به کلاغی کسی زمستانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هر چه در عشق گم کنی بدهد</p></div>
<div class="m2"><p>هر یکی را امیر تاوانی</p></div></div>
<div class="b2" id="bn32"><p>تاج آزادگان امیرحسین</p>
<p>که ندارد نظیر در کونین</p></div>
<div class="b" id="bn33"><div class="m1"><p>گوئی از دست عشق کی برهم</p></div>
<div class="m2"><p>تا روم سر به تخت باز نهم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چه کنم زلف یار چون زره است</p></div>
<div class="m2"><p>زره او همی برد ز رهم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ای دل از من به شاه خوبان شو</p></div>
<div class="m2"><p>تا نیارد فراق او سپهم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>عشق را گو مزن که بی زورم</p></div>
<div class="m2"><p>دوست را گو مکش که بی گنهم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هم ز مکر و سپید کاری اوست</p></div>
<div class="m2"><p>کاین چنین من ز عشق دل سیهم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چون مرا آن نگار بی آزرم</p></div>
<div class="m2"><p>گفت جز جان و مال و دل نخواهم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خویشتن را و یار بد خود را</p></div>
<div class="m2"><p>چه دهم رنج «و» بفکنم به رهم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ای قوامی در آرزوی وصال</p></div>
<div class="m2"><p>چون تو در هجر دلبران تبهم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ترک خوبان کنم کجا برم آن</p></div>
<div class="m2"><p>تشت زرین که جان درو بدهم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گر مرا سر برهنه دارد بخت</p></div>
<div class="m2"><p>حشمت میر بس بود کلهم</p></div></div>
<div class="b2" id="bn43"><p>تاج آزادگان امیرحسین</p>
<p>که ندارد نظیر در کونین</p></div>
<div class="b" id="bn44"><div class="m1"><p>شاه و سالار دلبران بودست</p></div>
<div class="m2"><p>تاج فرق سمنبران بودست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>از نکوروئی و خوشی گوئی</p></div>
<div class="m2"><p>از در بزم مهتران بودست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>از کرشمه به نوک غمزه تیز</p></div>
<div class="m2"><p>همچو تیغ دلاوران بودست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گاه عشرت میان خوبان در</p></div>
<div class="m2"><p>همچو خورشید از اختران بودست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بر عمارت سرای حسن امروز</p></div>
<div class="m2"><p>کار فرمای دلبران بودست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>از لطافت به روزگار وصال</p></div>
<div class="m2"><p>راحت روح پروران بودست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>روز هجران عاشق مظلوم</p></div>
<div class="m2"><p>مایه ظلم گستران بودست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نشود بارگیر درویشان</p></div>
<div class="m2"><p>زانکه یار توانگران بودست</p></div></div>
<div class="b2" id="bn52"><p>تاج آزادگان امیرحسین</p>
<p>که ندارد نظیر در کونین</p></div>
<div class="b" id="bn53"><div class="m1"><p>ای دل از عشق دست و پای مزن</p></div>
<div class="m2"><p>روی نیکوترست و رای مزن</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تکیه بر عقل تن گداز مکن</p></div>
<div class="m2"><p>بانگ بر عشق جان فزای مزن</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ابروئی پر ز خشم؛ عشق مباز</p></div>
<div class="m2"><p>دهنی پر ز پست؛ نای مزن</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>عشق بر دلبر جفاجوی آر</p></div>
<div class="m2"><p>لاف یار وفا نمای مزن</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تیغ بر روی پادشاهان کش</p></div>
<div class="m2"><p>تیر بر چشم هر گدای مزن</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>تا توانی مباش با اوباش</p></div>
<div class="m2"><p>گام بی یار دلربای مزن</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>تا بود عرصه بهشت خدای</p></div>
<div class="m2"><p>خیمه بر دشت دهخدای مزن</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ای قوامی چو بسته کردت عشق</p></div>
<div class="m2"><p>جز در صبر درگشای مزن</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>سرندانی تو روی عشق مبین</p></div>
<div class="m2"><p>سر نداری تو پشت پای مزن</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>عشق را باش وجز به نزد امیر</p></div>
<div class="m2"><p>نفس شکر هیچ جای مزن</p></div></div>
<div class="b2" id="bn63"><p>تاج آزادگان امیرحسین</p>
<p>که ندارد نظیر در کونین</p></div>
<div class="b" id="bn64"><div class="m1"><p>آن امیر لطیف آزاده</p></div>
<div class="m2"><p>محترم نفس و محتشم زاده</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>صدر نیکوخصال گردون قدر</p></div>
<div class="m2"><p>بدر خورشید زاد آزاده</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>شکر گویان ز جود چون مستان</p></div>
<div class="m2"><p>بردر او به هم در افتاده</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>مهتران همچو صورت اندر آب</p></div>
<div class="m2"><p>بسر از پیش قدرش استاده</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>در بهاران به شادی عدلش</p></div>
<div class="m2"><p>داده باد صبا به گل باده</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>سال و مه شکل کاغذ و خطش</p></div>
<div class="m2"><p>ملکت روز را به شب داده</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>سجده ها برده از سیاست او</p></div>
<div class="m2"><p>شیر نر پیش آهوی ماده</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>از پی شاعران به راه و به در</p></div>
<div class="m2"><p>چشم بگشاده گوش بنهاده</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>وز پی زایران به روز و به شب</p></div>
<div class="m2"><p>خوان نهادست و دست بگشاده</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بورامین ز بهر خدمت او</p></div>
<div class="m2"><p>دولت از ری مرا فرستاده</p></div></div>
<div class="b2" id="bn74"><p>تاج آزادگان امیرحسین</p>
<p>که ندارد نظیر در کونین</p></div>
<div class="b" id="bn75"><div class="m1"><p>ای بگوهر ز نسل آدم فرد</p></div>
<div class="m2"><p>ملک را حشمت تو اندر خورد</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بر فلک در رکاب دولت تو</p></div>
<div class="m2"><p>اختران می زنند بردا برد</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>روزگار از برای دوستیت</p></div>
<div class="m2"><p>کرد با دشمن آنچه باید کرد</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>هست بر چرخ فضل خامه تو</p></div>
<div class="m2"><p>کوکب شب نمای روز نورد</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>پیش روی تو بانگ او گوئی</p></div>
<div class="m2"><p>می کند عندلیب خطبه ورد</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>دولت تو کجا کند خامی</p></div>
<div class="m2"><p>طبع آتش چگونه باشد سرد</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>تیره شد روز دشمن از جاهت</p></div>
<div class="m2"><p>چون بجنبد سپه بخیزد گرد</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>چه شناسد عدو لطافت و خشم</p></div>
<div class="m2"><p>چه خبر مرده را ز راحت و درد</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>جفت شادی شود همی دل من</p></div>
<div class="m2"><p>بهر این بیت همچو گوهر فرد</p></div></div>
<div class="b2" id="bn84"><p>تاج آزادگان امیرحسین</p>
<p>که ندارد نظیر در کونین</p></div>
<div class="b" id="bn85"><div class="m1"><p>ای سرای تو آسمان کردار</p></div>
<div class="m2"><p>پیشکاران تو کواکب وار</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>بر سرت اختران سعد نمای</p></div>
<div class="m2"><p>بر درت مهتران دولت یار</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>تا قوامی به خدمت تو رسید</p></div>
<div class="m2"><p>از یکی شد به صد هزار هزار</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>سایه تو فتاد بر سر من</p></div>
<div class="m2"><p>تا مرا کرد آفتاب تبار</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>مر تورا شاعر و ندیم بسیست</p></div>
<div class="m2"><p>جلد درفضل و چابک اندر کار</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>لیک زیشان همه به حضرت تو</p></div>
<div class="m2"><p>من و با احمدیم خدمتکار</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>دهخدائی اجل رشیدالملک</p></div>
<div class="m2"><p>بوده با ما به حکمت اندر غار</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>من در آن بند نیستم که مرا</p></div>
<div class="m2"><p>خلعت امسال به بود یا پار</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>خلعت تو مرا نه امروزست</p></div>
<div class="m2"><p>کز پی خدمت تو ایزد بار</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>روز اول که آفرید مرا</p></div>
<div class="m2"><p>تن من جبه کرد و سردستار</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>هست بیتی خوش اندرین ترجیع</p></div>
<div class="m2"><p>باز گویم چو بشنوی ز هزار</p></div></div>
<div class="b2" id="bn96"><p>تاج آزادگان امیرحسین</p>
<p>که ندارد نظیر در کونین</p></div>
<div class="b" id="bn97"><div class="m1"><p>نانبائی که شاعرست منم</p></div>
<div class="m2"><p>شاعری نانبای خوش سخنم</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>گندم ارتفاع حصه عقل</p></div>
<div class="m2"><p>بر در آسیای دل فکنم</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>برم از آسیا به دوکانی</p></div>
<div class="m2"><p>که بود چار حد او بدنم</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>نانم ار در تنور دیده نه ای</p></div>
<div class="m2"><p>بنگر اندر زبان و در دهنم</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>در ترازوی طبع و خاطر سنگ</p></div>
<div class="m2"><p>وهم و فهم است یک من و دومنم</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>مشتری ز آسمان فرود آید</p></div>
<div class="m2"><p>مشتری را گهی که بانگ زنم</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>ای که بازر همچو گلبرگی</p></div>
<div class="m2"><p>سغبه نانهای چون سمنم</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>نان شعر از من و تو شاید پخت</p></div>
<div class="m2"><p>که ترازو توئی و سنگ منم</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>راتب مدح میر خواهم داد</p></div>
<div class="m2"><p>تابه دوکان شعر خویشتنم</p></div></div>
<div class="b2" id="bn106"><p>تاج آزادگان امیرحسین</p>
<p>که ندارد نظیر در کونین</p></div>
<div class="b" id="bn107"><div class="m1"><p>حظ عمر تو نیک نامی باد</p></div>
<div class="m2"><p>قسم طبع تو شادکامی باد</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>کار دولت بر آستانه تو</p></div>
<div class="m2"><p>چو دگر خواجگان غلامی باد</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>زیر ران تو اسب ناز و نیاز</p></div>
<div class="m2"><p>خوش لگامی و تیز گامی باد</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>بهره از روزگار، بدگورا</p></div>
<div class="m2"><p>خام طبعی و ناتمامی باد</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>صفت رای و روی دشمن تو</p></div>
<div class="m2"><p>تنگ خوئی و زردفامی باد</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>هم ره شخص و همنشین دلت</p></div>
<div class="m2"><p>نیک عهدی و نیکنامی باد</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>هر که زرین کند ز مهر تو روی</p></div>
<div class="m2"><p>در جهان همچو زر گرامی باد</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>عقل را پختگیست در سر تو</p></div>
<div class="m2"><p>باده را در کف تو خامی باد</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>تا بود خاص و عام در عالم</p></div>
<div class="m2"><p>خاصتر کس برتو عامی باد</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>کار خصم تو ناقوامی شد</p></div>
<div class="m2"><p>شاعر خاص تو قوامی باد</p></div></div>