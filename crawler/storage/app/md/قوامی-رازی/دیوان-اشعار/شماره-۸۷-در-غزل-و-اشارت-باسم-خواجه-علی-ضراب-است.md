---
title: >-
    شمارهٔ  ۸۷ - در غزل و اشارت باسم خواجه علی ضراب است
---
# شمارهٔ  ۸۷ - در غزل و اشارت باسم خواجه علی ضراب است

<div class="b" id="bn1"><div class="m1"><p>بپیچ سنبل اندر تاب داری</p></div>
<div class="m2"><p>میان نرگس اندر خواب داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل از عشق تو چون قندیل دارم</p></div>
<div class="m2"><p>که روی از حسن چون محراب داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم بیتاب گشت و دیده پرآب</p></div>
<div class="m2"><p>که زلف و رخ بتاب و آب داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شفا از بوسه تو یافت جانم</p></div>
<div class="m2"><p>همانا در دو لب جلاب داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شدستی شرمگین تا از بنفشه</p></div>
<div class="m2"><p>طراز لاله سیراب داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو را منشور حسن اکنون درست است</p></div>
<div class="m2"><p>که طغرا«ئی» ز مشک ناب داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر تو ماهی ای دلبر چرا پس</p></div>
<div class="m2"><p>مرا رخساره چون مهتاب داری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قوامی خاکپای تو است اگر تو</p></div>
<div class="m2"><p>سر خواجه علی ضراب داری</p></div></div>