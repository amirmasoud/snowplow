---
title: >-
    شمارهٔ  ۱۰۶ - در منقبت امیرالمؤمنین علی علیه السلام و عدل خدای تعالی و مدح سید فخرالدین و پدر او سید شمس الدین که هر دو رئیس شیعه در ری بوده اند گوید
---
# شمارهٔ  ۱۰۶ - در منقبت امیرالمؤمنین علی علیه السلام و عدل خدای تعالی و مدح سید فخرالدین و پدر او سید شمس الدین که هر دو رئیس شیعه در ری بوده اند گوید

<div class="b" id="bn1"><div class="m1"><p>مرتضی باید که بعد از مصطفی فرمان دهد</p></div>
<div class="m2"><p>تابدین در علم دارووار او درمان دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکه منبر جز علی را سازد او باشد چو آن</p></div>
<div class="m2"><p>کس که مصحف را به دست کودک نادان دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکه دین آور بود در حق به حق پرور شود</p></div>
<div class="m2"><p>هرکه دریا بر شود کشتی به کشتیبان دهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای که اندر دین علی را باز پس داری همی</p></div>
<div class="m2"><p>این چنین رخصت بدستت دیو پردستان دهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من نگویم مرتضی را تو نمی دانی امام</p></div>
<div class="m2"><p>که این زیادت بر تو بستن علم را نقصان دهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرتضی کز پیش بوبکر و عمر باشد به علم</p></div>
<div class="m2"><p>کی روا داری که فرمان از پس «عقمان» دهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یار اهل البیت حق باش و بدان غره مشو</p></div>
<div class="m2"><p>گر به باطل یاری امروزت همی سلطان دهد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یاری سلطان یک روزه ندارد قیمتی</p></div>
<div class="m2"><p>یاری آن یاری است کان سلطان جاویدان دهد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر همی دانی که نوشروان ز عدل ایوان فراشت</p></div>
<div class="m2"><p>عدل کن تا در بهشت ایزد تو را ایوان دهد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ظلم بر یزدان همی بندی روا داری چرا</p></div>
<div class="m2"><p>ظلم تو یزدان کند عدل تو نوشروان دهد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خیره بدها کردن و آنگاه بستن بر خدای</p></div>
<div class="m2"><p>اینت نازیبا غروری کز هوی شیطان دهد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کمترازکبری چه باید بودکو گوید همی</p></div>
<div class="m2"><p>راهها شیطان زند توفیقها یزدان دهد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر به سامانی در ایمان پس مدان ایمان عطا</p></div>
<div class="m2"><p>زانکه عشوه خویشتن را مرد بی سامان دهد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خود به قول تو نشاید خواند مؤمن بنده را</p></div>
<div class="m2"><p>چون بود مؤمن کسی کورا خدای ایمان دهد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بنده را گوئی عطای داده بستاند خدای</p></div>
<div class="m2"><p>تاش در دوزخ شراب درد بی پایان دهد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مدخلی باشد که داده باز بستاند عطا</p></div>
<div class="m2"><p>حق به ما حاشا و کلا گر عطا زین سان دهد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>طاعت و عصیان بنده کی کند سود و زیان</p></div>
<div class="m2"><p>چون نداند کش ملک توفیق یا خذلان دهد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دل همی سوزد به زاهد بر درین مذهب مرا</p></div>
<div class="m2"><p>تاچرا در صومعه بیهوده مسکین جان دهد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای برادر زین تعصب دور شو تا کردگار</p></div>
<div class="m2"><p>روز حق ز ابر کرم کشت تو را باران دهد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تاکی اندر بغض حیدر هر زمان شیطان جهل</p></div>
<div class="m2"><p>در سرای دل به ایوان تو شاد روان دهد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>غصه جان است نادان را علی که اندر مثل</p></div>
<div class="m2"><p>«درد جاهل علم باشد رنج خر پالان دهد»</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هرچه اندازی به دنیا بازیابی روز حشر</p></div>
<div class="m2"><p>هرچه کاری در زمستان بربه تابستان دهد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هرزمان گوئی به سخره مهدیت را گو بیای</p></div>
<div class="m2"><p>تا جهان را گاه عدل ارایش بستان دهد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>معتقد باید که حال مهدیش باور کند</p></div>
<div class="m2"><p>مرد چون یعقوب کوتایوسفش هجران دهد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هرکه دارد حب مهدی را ند در مهدی رسید</p></div>
<div class="m2"><p>مالش فرعونیان هم موسی عمران دهد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نرم گردن باش حق را تا میان ما و تو</p></div>
<div class="m2"><p>شاخ ایمان سایه بخشد باغ دین ریحان دهد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در سؤال من ترش روئی مکن که اندر بهار</p></div>
<div class="m2"><p>گل جواب عندلیب از باغها خندان دهد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفت سلمان مصطفی راکه ایزد اندر عهد تو</p></div>
<div class="m2"><p>مرتضی را چون رسولان معجزالوان دهد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گه چو آدم در بهشت حضرتت معصوم وار</p></div>
<div class="m2"><p>مالش ابلیس کفر از عیبها عریان دهد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گه چو نوح از کشتی عصمت به تیغ آب رنگ</p></div>
<div class="m2"><p>دشمنان را هم ز خون دشمنان طوفان دهد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گه چو ابراهیم فرزند هوی را پیش عقل</p></div>
<div class="m2"><p>از برای قرب ایزد فتوی قربان دهد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گه چو خضر از ظلمت دنیا به اهل شرع و دین</p></div>
<div class="m2"><p>علمهای سودمندش چشمه حیوان دهد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گاه چون موسی به صف جنگ فرعونان کفر</p></div>
<div class="m2"><p>مر نهنگ آهنین را قوت ثعبان دهد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گه چو عیسی در دیار روم رهبانان جهل</p></div>
<div class="m2"><p>مردگان شبهه را جان از دم برهان دهد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گه سلیمان وار بر مرغان و دیوان حسد</p></div>
<div class="m2"><p>در صفا از خاتم عهد و وفا فرمان دهد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گه چو یوسف در چه گیتی ز رغم گرگ حرص</p></div>
<div class="m2"><p>ازپی اسلام تن در خدمت اخوان دهد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گه چو یونس از عبادتها ز پیش کردگار</p></div>
<div class="m2"><p>روح را در بطن حوت بحر تن زندان دهد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همچو سلمان گو فضیلتهای میرمؤمنین</p></div>
<div class="m2"><p>تا جهاندارت درج چون بوذر و سلمان دهد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>آن امام نص معصوم آنکه زیرساق عرش</p></div>
<div class="m2"><p>بوسه بر تعلین قدر او همی کیوان دهد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>حامل تنزیل قرآن حافظ شرع رسول</p></div>
<div class="m2"><p>کش به فضل اندر گوائی آیت قرآن دهد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>درسخا و فضل و فرهنگ و شجاعت چون علی</p></div>
<div class="m2"><p>کو سواری کاسب جدو جهد را جولان دهد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>علم گوید، زهد ورزد، سرپذیرد، سرنهد</p></div>
<div class="m2"><p>تیغ بخشد درقه پاشد جان فشاند، نان دهد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ای قوامی شعر رنگین از بهار طبع تو است</p></div>
<div class="m2"><p>نقش زیبا راچنین چشم از نگارستان دهد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>آفرین برطبع خوبت کز صدفهای خرد</p></div>
<div class="m2"><p>همچو بحر علم فخرالدین گهر آسان دهد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>صدر عالی قدر فخرالدین که گاه مرتبه</p></div>
<div class="m2"><p>رشوت جاه رفیعش گنبد گردان دهد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مفخر سادات هفت اقلیم شمس الدین که شمس</p></div>
<div class="m2"><p>نامه اقبال او رابوسه برعنوان دهد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>یافت تشریفی ز همنامیش در تدویر شمس</p></div>
<div class="m2"><p>جمله اجرام فلک را روشنائی زان دهد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سیدی صدری بزرگی سروری نیک اختری</p></div>
<div class="m2"><p>کو زباد نعل اسب افلاک را دوران دهد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>دولتش را بی وفائی حیلت حاسد کند</p></div>
<div class="m2"><p>بوستان را بینوائی باد مهریگان دهد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>هرکه غمگین خواهدش بی شک هم او غمگین شود</p></div>
<div class="m2"><p>هرکه کاری بد کند لابد هم او تاوان دهد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هست حضرتها بسی لیکن شرف اینجا بود</p></div>
<div class="m2"><p>هست دریاها بسی لیکن گهر عمان دهد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ای که چوگان مرادت را زمین گوئی شود</p></div>
<div class="m2"><p>وای که گوی دولتت را آسمان چوگان دهد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>در خم چوگان جاهت گرچه گوی است آسمان</p></div>
<div class="m2"><p>باش تا چون گوی و چوگان دولتت میدان دهد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چرخ را گیتی ز پیش تیر عزمت روز و شب</p></div>
<div class="m2"><p>لاژوردین جوشن و پیروزه گون خفتان دهد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>میغ را گردون به جنگ خصمت از باران وبرف</p></div>
<div class="m2"><p>تیغ زراندود و تیر سیمگون پیکان دهد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>از غرائب زان بیان مدح تو شاعرکند</p></div>
<div class="m2"><p>کز عجائب خودنشان بحر بازرگان دهد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>کشت دشمن را جهان زان توبری ء الساحه ای</p></div>
<div class="m2"><p>شربتش را رنج تن گر چرخ و گر ارکان دهد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>رنجه دل کردت عدو تا جان او رنجور شد</p></div>
<div class="m2"><p>این قدر داند که چون مهمان خورد مهمان دهد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نعمت بدخواه ناپاینده زان شد کایدری است</p></div>
<div class="m2"><p>بی مدد باشد بلی نرگس که نرگسدان دهد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>دولتی داری خدائی همچنین پاینده باد</p></div>
<div class="m2"><p>دون ازآن که اینجا فلان را از هوس بهمان دهد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بخشش مخلوق کی چون خلعت خالق بود</p></div>
<div class="m2"><p>همچنان بی حس که او تنگان بباتنگان دهد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>هرکه این خدمت به دیگر خدمتی بدهد بود</p></div>
<div class="m2"><p>همچنان بی حس که او تنگان بباتنگان دهد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>صدر چون تو مادر ملت نه از بطن آورد</p></div>
<div class="m2"><p>شیر مقبل دایه دولت نه از پستان دهد</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>مصطفی خلقی به خلقت مرتضی واری به شکل</p></div>
<div class="m2"><p>چون صدف یک رنگ باشد در همه یکسان دهد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>هم رئیس شیعتی هم سید سادات عصر</p></div>
<div class="m2"><p>دولت از جاهت همی سرمایه اعیان دهد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>هم سیادت هم ریاست هم سیاست زیبدت</p></div>
<div class="m2"><p>این چنین فضل و شرف حنان دهد منان دهد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ای که اندر ری صبای سایه اقبال تو</p></div>
<div class="m2"><p>رازیان را چون درختان خلعت نیسان دهد</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>تو محمد نسبتی آمد قوامی تا ز خود</p></div>
<div class="m2"><p>دایه احسانت او را پایه حسان دهد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>شاعران آیند هر وقتی قوامی گه گهی</p></div>
<div class="m2"><p>دردسر گر کم دهد چاکر نه از عصیان دهد</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>آنکه شیرآرد ز بیشه پای او سنگی بود</p></div>
<div class="m2"><p>آن سبک دستی کند کو گربه ازانبان دهد</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>شاعر نان پز به جز من کیست کو ممدوح را</p></div>
<div class="m2"><p>از معانی گندم آرد و ز عبارت نان دهد</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>حکمتش سرمایه باشد دولتش یاری کند</p></div>
<div class="m2"><p>فکرتش مزدور بخشد خاطرش دو کان دهد</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ماه چون سنگی شود مهتاب از اوویزان چو آرد</p></div>
<div class="m2"><p>که آسیابان سپهر از عقربش قبان دهد</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>آفتایش در تنور افروختن آتش برد</p></div>
<div class="m2"><p>آسمانش در ترازو داشتن میزان دهد</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>طبعش ازوهم و خیال و حفظ و فکرت نان نظم</p></div>
<div class="m2"><p>نیک ورزد، پاک دارد، خوش پزد، ارزان دهد</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>نه چنان ارزان به یک باره که باشد رایگان</p></div>
<div class="m2"><p>کان که نان شعرخواهد گندم احسان دهد</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>تا ز روی عقل باشد جاهل و بیدادگر</p></div>
<div class="m2"><p>آنکه داد شهر آباد از ده ویران دهد</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بدسگالت در دهی ویران به حالی زار باد</p></div>
<div class="m2"><p>تا تو را دولت هزاران شهر آبادان دهد</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>هر سعادت که آسمان خویشانت را داد از درج</p></div>
<div class="m2"><p>دان که فرزند عزیزت رابه صد چندان دهد</p></div></div>