---
title: >-
    شمارهٔ  ۸۲ - در غزل است
---
# شمارهٔ  ۸۲ - در غزل است

<div class="b" id="bn1"><div class="m1"><p>ای که در روزه بال و پر زده ای</p></div>
<div class="m2"><p>عید زن دست و پای اگر زده ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کوه اندوه را درآر از پای</p></div>
<div class="m2"><p>ای بسا سر که بر کمر زده ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روح را آب عید برلب زن</p></div>
<div class="m2"><p>که آتش روزه در جگر زده ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با بتی چون شکر زن اکنون دم</p></div>
<div class="m2"><p>که ازو بر سر شکر زده ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون لب او نچشته ای حلوا</p></div>
<div class="m2"><p>گر چه لوزینهای تر زده ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه ماهی گرفته ای یک ماه</p></div>
<div class="m2"><p>شست در جوی مختصر زده ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دست در شست زلف زن چه بسی</p></div>
<div class="m2"><p>دست در سر ز درد سرزده ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای سلیمان ما که با داود</p></div>
<div class="m2"><p>به نواهای خوب بر زده ای</p></div></div>