---
title: >-
    شمارهٔ  ۶۱ - در غزل است
---
# شمارهٔ  ۶۱ - در غزل است

<div class="b" id="bn1"><div class="m1"><p>حساب عشق تو ای دوست سخت بار نجست</p></div>
<div class="m2"><p>حساب عشق تو گوئی حساب شطرنجست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لب ملیح کم آوازت ارچه روح افزاست</p></div>
<div class="m2"><p>ز مکر چشم دغل باز تو دل آهن جست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو گنج حسنی و زلف تو مار خم در خم</p></div>
<div class="m2"><p>بدیع نیست اگر مار بر سر گنجست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لبت بهای دل و جان دهد به گاه سخن</p></div>
<div class="m2"><p>کز آن دو کفه یاقوت گون گهر سنج است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه آب و آتش و خاک و هوائی از خوبی</p></div>
<div class="m2"><p>اگر طبایع چار است زان تو پنج است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا پیاده عشق تو چون به دیوان خواند</p></div>
<div class="m2"><p>دلم به رنج گرو کرد گفت بی رنج است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر قوامی با توست هیچ عیب مدار</p></div>
<div class="m2"><p>ترنج دانی جانا که جفت نارنج است</p></div></div>