---
title: >-
    شمارهٔ  ۶۰ - در غزل است
---
# شمارهٔ  ۶۰ - در غزل است

<div class="b" id="bn1"><div class="m1"><p>ای زلف بند کرده ز ابرو گره گشای</p></div>
<div class="m2"><p>با دوستان بخند و سخن گوی و خوش درآی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میدان مهر جوی و درو اسب وصل تاز</p></div>
<div class="m2"><p>چوگان عشق گیر و درو گوی دل ربای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>انده فزای شد دل شادی پرست من</p></div>
<div class="m2"><p>از من مکن کنار میان تو و خدای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا کی کنی فراخ روز با کلاه کبر</p></div>
<div class="m2"><p>ترسم به سر نیاید و تنگ آیدت قبای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای بس که در زمانه بدان چشم دلفریب</p></div>
<div class="m2"><p>از جای برده ای دل مردان دل به جای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن را که سرزنش کند از عشق گو هلا</p></div>
<div class="m2"><p>پائی به راه درنه و دستی برآزمای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با خویستن به عشق تو گویم قوامیا</p></div>
<div class="m2"><p>بر جان نگار مهر نگاران جانفزای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر خویشتن همه در شادی فرو مبند</p></div>
<div class="m2"><p>صبر آر تا خدای کند بر تو درگشای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اندیشه دور کن مبر اندوه «و» خوش بزی</p></div>
<div class="m2"><p>بیچاره ای چه شد که بمردی به دست و پای</p></div></div>