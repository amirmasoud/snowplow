---
title: >-
    شمارهٔ  ۷۰ - در غزل است
---
# شمارهٔ  ۷۰ - در غزل است

<div class="b" id="bn1"><div class="m1"><p>در عشقم آرزوست که دل را طرب دهم</p></div>
<div class="m2"><p>تا صد هزار بوسه بر آن چشم و لب دهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از رشگ این دو دیده و از رشگ آن دو لب</p></div>
<div class="m2"><p>هم نقل طرفه سازم و هم می عجب دهم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل را که سغبه تو کنم بی غرض کنم</p></div>
<div class="m2"><p>جان را که مالش تو دهم بی سبب دهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منشور آفتاب ز درگاه آسمان</p></div>
<div class="m2"><p>آنکه رسد که حسن تو را من لقب دهم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون شاخ رز به فصل خزان زرد و لاغرم</p></div>
<div class="m2"><p>تاگفته ای که از خم زلفت عنب دهم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل بی ادب شدست تقاضا همی کند</p></div>
<div class="m2"><p>تا من ز باده لبت او را طرب دهم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گیرم که مستئی ندهی دلبرا مرا</p></div>
<div class="m2"><p>چندان بده که در خور این بی ادب دهم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتی قوامیا چه دوی گرد من به روز</p></div>
<div class="m2"><p>من بار عاشقان جفاکش به شب دهم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای یار مهربان پس اگر عاشقان تو</p></div>
<div class="m2"><p>ور زند مهر دل نه که من مهر لب دهم</p></div></div>