---
title: >-
    شمارهٔ  ۱۰۹ - در عدل خدای تعالی و امامت ائمه اثنی عشر علیهم السلام و موعظت و نصیحت و مدح نقیب النقباء ری شرف الدین مرتضی گوید
---
# شمارهٔ  ۱۰۹ - در عدل خدای تعالی و امامت ائمه اثنی عشر علیهم السلام و موعظت و نصیحت و مدح نقیب النقباء ری شرف الدین مرتضی گوید

<div class="b" id="bn1"><div class="m1"><p>چهار دار امام ای پسر ولی سه چهار</p></div>
<div class="m2"><p>کزین دوازده یابی بهشت جنت بار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من از دوازده نازم تو از چهار ای دوست</p></div>
<div class="m2"><p>کنون بباید هان ساختن به هم ناچار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به چار فصل نگه کن که هست در سالی</p></div>
<div class="m2"><p>چگونه ساخته گشت است با دوازده چار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من و تو هر دو بدو مذهبیم در یک دین</p></div>
<div class="m2"><p>چنانکه روز و شب از یک جهان بدو هنجار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو از میان من و خویشتن بده انصاف</p></div>
<div class="m2"><p>نه مهر ورز به یکبارگی نه کینه گذار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه فرد باش ز ظلمت نه دور باش از نور</p></div>
<div class="m2"><p>چو صبح تکیه زن اندر میان لیل و نهار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من و تو را نگزیرد ز یکدگر در دین</p></div>
<div class="m2"><p>چنانکه احمد را از مهاجر و انصار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر دوازده گویم علی است اول دور</p></div>
<div class="m2"><p>وگربه چار بگوئی علی است آخر کار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چهار یار تمام از دوازده باشد</p></div>
<div class="m2"><p>چو ز اهل بیت نباشد یکی سه باشد یار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>و گر بیاران گفتی به اهل بیت بگوی</p></div>
<div class="m2"><p>رحب یاران با اهل بیت بغض مدار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو مهر یاران با اهل بیت دار به هم</p></div>
<div class="m2"><p>که بوده اند نبی و عتیق در یک غار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>طریق عدل نگهدار در ره توحید</p></div>
<div class="m2"><p>بگرد جبر مگرد ای عزیز من زنهار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدان جهان چه شوی هم بدین جهان اندر</p></div>
<div class="m2"><p>یکی به جور خزان بنگر و به عدل بهار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر از مدائن خلد آرزو است ایوانت</p></div>
<div class="m2"><p>ز عدل سر نتوانی کشید کسری وار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بدان خدای که کرد از شرف مدینه و در</p></div>
<div class="m2"><p>ز علم احمد مختار و حیدر کرار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که گر تو مهر دراز عشق مهر دل نکنی</p></div>
<div class="m2"><p>درین مدینه نیابی چو حلقه بر دربار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز بغض ایزد اگر قفل نیست بر در تو</p></div>
<div class="m2"><p>به راه دوزخ بر هفت در زنی مسمار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز نار و جنت به الله که رنج و راحت نیست</p></div>
<div class="m2"><p>مگر ز کینه و مهر و قسیم جنت و نار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز مصطفی تو شوی زرد روی چون آبی</p></div>
<div class="m2"><p>ز مرتضی چو دل آگنده ای چو دانه نار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو می زنی ز عمر لاف دوستی در دین</p></div>
<div class="m2"><p>به گرد جبر چه گردی بیا و عدل بیار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو عمر و عمر تو بر باد داده شد زیرا</p></div>
<div class="m2"><p>که عمر و واری در کار نه عمر کردار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نثار مؤمن فرمود مصطفی را حق</p></div>
<div class="m2"><p>که در حدیث به جز در ز ابر وحی مبار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز خون کافر گفت است مرتضی را هم</p></div>
<div class="m2"><p>ز ذوالفقار به جز لاله بر بنفشه مکار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مبر تو تهمت شتم صحابه بر شیعت</p></div>
<div class="m2"><p>مگوی چیزی کت واجب آید استغفار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مدار باور آن را که این سخن گوید</p></div>
<div class="m2"><p>که هست عثمان مهتر ز حیدر کرار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تعصبی که کنون هست در میانه ما</p></div>
<div class="m2"><p>نبود هرگز در عهد احمد مختار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زمانه اول چون آخرالزمان کی بود</p></div>
<div class="m2"><p>چگونه باشد روز سفید چون شب تار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هر آنکه آمد در دین رسول گفت او را</p></div>
<div class="m2"><p>به مژدگانی دین در نثار کن دینار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به مرتضی که نه دینار خواست نه دنیا</p></div>
<div class="m2"><p>چه گفت گفت فلان را ز کفر در دین آر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به علم همچو علی کس نبود در اسلام</p></div>
<div class="m2"><p>که بود مطلع سر ز عالم الاسرار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کمال علم الهی چو جهل خلقان نیست</p></div>
<div class="m2"><p>جمال غنچه گل کی بود چو غمزه خار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سرای شرع نبی را علی ستون بناست</p></div>
<div class="m2"><p>دگر چه آید و خیزد همی ز رنگ و نگار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به علم و عصمت و مردی سؤالها کردیم</p></div>
<div class="m2"><p>«علی » جواب همی آمد از در و دیوار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>قصیده های قوامی قیامت سخن است</p></div>
<div class="m2"><p>که طیر بهشت است جعفر طیار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>لطیفه ایت بگویم برمز غمز مکن</p></div>
<div class="m2"><p>ورت به هوش نیارم بگوش در مگذار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به باغ باقی درچند گونه مرغانند</p></div>
<div class="m2"><p>ز یک بده شده از ده به صد ز صد به هزار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>حروف حکمت سیر مغ لم یزل در غیب</p></div>
<div class="m2"><p>ز لوح کردن طاوس سدره را تکرار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همای شرع بگسترد سایه در عالم</p></div>
<div class="m2"><p>ز عندلیب رسالت گشاده شد منقار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو باز عصمت در صیدگاه دین آمد</p></div>
<div class="m2"><p>به باغ یازده طوطی شدند در گفتار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>که تا ز باغ حسد دشمنان زاغ صفت</p></div>
<div class="m2"><p>فرو برند ز تیمار سر چو بوتیمار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>قوامیا تو سراینده دار همچون گل</p></div>
<div class="m2"><p>فراز گلبن ارواح بلبل اشعار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بر انتظار خروش خروس مهدی باش</p></div>
<div class="m2"><p>به عهد سید شاهین دل عقاب شکار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>جهان عزشرف الدین که هر دمی زشرف</p></div>
<div class="m2"><p>سر فلک به لگد می زند هزاران بار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سپهر حمد محمد که در مصاف سخا</p></div>
<div class="m2"><p>به تیغ جود بر آرد ز خیل آز دمار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>خدایگان گهری مصطفی نسب صدری</p></div>
<div class="m2"><p>که آفتاب جلال است وسایه دادار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نقیب آل محمد سلاله نبوی</p></div>
<div class="m2"><p>جمال گوهر سلجوق و فخر آل و تبار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>خدای رحمت و خسرو دل و سپهبد سهم</p></div>
<div class="m2"><p>فرشته شکل و پیمبرفش و امام شعار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>همی دهند ز دیوان رای روشن اوی</p></div>
<div class="m2"><p>منوران فلک را معیشت و ادرار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>اگرچو همت او موجها زند دریا</p></div>
<div class="m2"><p>گهربرند ز دریا کنارها به کنار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به نزد همت او کیست آسمان و زمین</p></div>
<div class="m2"><p>به دست دایره کش چیست نقطه و پرگار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ایا سیاست تو همچو دیو بی آزرم</p></div>
<div class="m2"><p>ایا لطافت تو چون فرشته بی آزار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>اگرچه هست تو را آب لطف دوست نواز</p></div>
<div class="m2"><p>تو راست آتش تهدید خشم دشمن خوار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>در آب پنهان ناخن برای خرچنگ است</p></div>
<div class="m2"><p>ز پیش صورت آب ارچه هست آینه دار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ز بیم خشم تو در حلم تو گریزد مرگ</p></div>
<div class="m2"><p>از آن کجا سپر تیغ برق شد کهسار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ز عطر خلق تویک ذره کم نخواهد شد</p></div>
<div class="m2"><p>اگر شوند مؤید همه جهان عطار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ز عدل خوشه گوهر برآری ار گوئی</p></div>
<div class="m2"><p>زبان آب روان در دهان آتش کار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>شگفتم آید چون بینم از قلم خطت</p></div>
<div class="m2"><p>که دید مورچه هرگز روان ز دیده مار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>خزینه های علوم تو را نه بس باشد</p></div>
<div class="m2"><p>گر آفتاب شود کوتوال و چرخ حصار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>توآن سکندر دینی که هست حجت تو</p></div>
<div class="m2"><p>ز پیش رخنه یأجوج شبهه شه دیوار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ز بهر کسب سعادت فلک کند کارت</p></div>
<div class="m2"><p>درین دوازده دوکان به هفت دست افزار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>هزار کنگره دارد حصار دولت تو</p></div>
<div class="m2"><p>کهینه کنگره مهتر ز گنبد دوار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بر آستانه تو رخ همی نهد دولت</p></div>
<div class="m2"><p>ز بهرآنکه برین خاک به چنان رخسار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>هر آینه علم ازجرم آفتاب سزد</p></div>
<div class="m2"><p>هر آنکه رابود ازدورآسمان دستار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>تو از نژاد امامان و پادشاهانی</p></div>
<div class="m2"><p>کراست این درج و رتبت ازصغار و کبار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>به جز تو کیست ز سادات در همه دنیا</p></div>
<div class="m2"><p>که او ائمه نژاد آمد و ملوک تبار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ترا نقابت سلطان از آن جهت فرمود</p></div>
<div class="m2"><p>که تا به خدمت آن تیز ترکنی بازار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ولیکن از شرف و حشمتی که هست تو را</p></div>
<div class="m2"><p>برین سپاه تو زیبی همی سپهسالار</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>صداع شغل نقابت ز حرمتش بیش است</p></div>
<div class="m2"><p>نشاط باده نیرزد همی به رنج خمار</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>نه سوی راحت ورنج است مرتو را این شغل</p></div>
<div class="m2"><p>نه بهر گرمی و سردیست مرد را شلوار</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>به عمر دشمن تو ماند در جهان زیرا</p></div>
<div class="m2"><p>به کعبتین شب و روز باخت است قمار</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چو صدر شرع شدی کبریا شده دشمن</p></div>
<div class="m2"><p>چو چرخ آینه شد ابر باشدش زنگار</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>دلم ز فر تو معنی فشان شد است آری</p></div>
<div class="m2"><p>هوا ز طلعت خورشید ذره کردنثار</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ایا ز فضل شده در میانه فضلا</p></div>
<div class="m2"><p>همان چنانکه بر خفتگان بود بیدار</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>منم قوامی کان میده های شعرپزم</p></div>
<div class="m2"><p>که لاغران معانی کنند ازو پروار</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ز کشت حکمت در کاروانسرای سخن</p></div>
<div class="m2"><p>همی نهم به نهان خانه دماغ انبار</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>در آسیای تفکر چو گندم آرد کنم</p></div>
<div class="m2"><p>بپشت گاو سپهر آورم به دکان بار</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بدان تنور بود دست پخت خاطر من</p></div>
<div class="m2"><p>به کام فایده در دیر خای و زودگوار</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>به حرص مشتریانم ز تیربازاری</p></div>
<div class="m2"><p>نهند گرده امسال در ترازوی پار</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>همیشه تا که بود اسم یاری و یاور</p></div>
<div class="m2"><p>همیشه تا که بود نام اندک و بسیار</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>تو را ز اندک و بسیار بهره نیکی باد</p></div>
<div class="m2"><p>که بدر یاور دینی و صدر دولت یار</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>پدر ز دیدن تو شاد و تو هم از او «شاد»</p></div>
<div class="m2"><p>زمانه از تو و تو از زمانه برخوردار</p></div></div>