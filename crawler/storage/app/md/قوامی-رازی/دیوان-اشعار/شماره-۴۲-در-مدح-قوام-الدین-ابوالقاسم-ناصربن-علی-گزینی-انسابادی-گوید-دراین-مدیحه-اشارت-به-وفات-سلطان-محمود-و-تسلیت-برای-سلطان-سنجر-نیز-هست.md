---
title: >-
    شمارهٔ  ۴۲ - در مدح قوام الدین ابوالقاسم ناصربن علی گزینی انسابادی گوید دراین مدیحه اشارت به وفات سلطان محمود و تسلیت برای سلطان سنجر نیز هست
---
# شمارهٔ  ۴۲ - در مدح قوام الدین ابوالقاسم ناصربن علی گزینی انسابادی گوید دراین مدیحه اشارت به وفات سلطان محمود و تسلیت برای سلطان سنجر نیز هست

<div class="b" id="bn1"><div class="m1"><p>ایا در حکم تو کرده جهانبان</p></div>
<div class="m2"><p>جهان چندان که هست آباد و ویران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وزیر شرق و غرب و صدر اسلام</p></div>
<div class="m2"><p>پناه ملک و دین و پشت ایمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو را آن پیرهن پوشید دولت</p></div>
<div class="m2"><p>که دارد هفت چرخ اندر گریبان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه خلق جهانت چاکرانند</p></div>
<div class="m2"><p>فلک بنده ستاره آفرین خوان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به نزدیک دل تو صعب تنگ است</p></div>
<div class="m2"><p>ز روی عقل طول و عرض دو جهان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرائی را که سازد دولت او را</p></div>
<div class="m2"><p>فلک در باشدش خورشید دربان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دویت از قرصه خورشید زیبد</p></div>
<div class="m2"><p>سزای خامه تو روز دیوان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کز آن زرین شهاب آسا قلمهات</p></div>
<div class="m2"><p>مجره هست چون سیمین قلمدان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر چه رای تو بر هفت کشور</p></div>
<div class="m2"><p>همی تابد چو خورشید درفشان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیمبروار خواهد کرد از این پس</p></div>
<div class="m2"><p>براق بخت تو بر عرش جولان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز جرم ماه داری طوق مرکب</p></div>
<div class="m2"><p>زخم چرخ سازی طاق ایوان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدان کز جود تو پوشیده گردد</p></div>
<div class="m2"><p>همی زاید ز مادر خلق عریان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر با کین تو کودک خورد شیر</p></div>
<div class="m2"><p>ببندد دایه را رگهای پستان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خراسان و عراق ارچه بزرگ است</p></div>
<div class="m2"><p>بود با همت تو خوار و آسان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دو سائل گر به نزدیک تو آیند</p></div>
<div class="m2"><p>عراق آن را دهی این را خراسان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به گردون بر یکی برج کمان است</p></div>
<div class="m2"><p>که تیر او شکافد سنگ و سندان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که تا زو ناوک انداز حوادث</p></div>
<div class="m2"><p>بدوزد بدسگالت را دل و جان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو در باران اقبالی و خصمت</p></div>
<div class="m2"><p>به باران در ولی در تیر باران</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز بانگ و نام تو بدخواه مدبر</p></div>
<div class="m2"><p>نه نان دارد نه جان نه خان و نه مان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بهشت از بهر آن آراست ایزد</p></div>
<div class="m2"><p>که خواند دوستانت را به مهمان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جهنم زان سبب را تافت جبار</p></div>
<div class="m2"><p>که دارد دشمنانت را به زندان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دم بدخواه تو چون باد دی گشت</p></div>
<div class="m2"><p>که مهرت شد چو باغ پر ز ریحان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از آن اندر دلش مهر تو خوش نیست</p></div>
<div class="m2"><p>که بستان خوش نباشد در زمستان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وزیرانی که بودستند از این پیش</p></div>
<div class="m2"><p>کفایت ورز و هشیار و سخندان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز تو معلوم گشت امروز که آنگاه</p></div>
<div class="m2"><p>وزارت بر همه بود است تاوان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو را در کاروانهای کفایت</p></div>
<div class="m2"><p>سزد چون صاحب کافی شتربان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز بالای وزارت پایه ای نیست</p></div>
<div class="m2"><p>ولیکن زو تو را بستود نتوان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بر آن درگه که آب دولت توست</p></div>
<div class="m2"><p>وزارت هست خاک پای دربان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جهان در حکم سلطان جهان است</p></div>
<div class="m2"><p>ولیکن هست در حکم تو سلطان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>قوام الدین تو را زیبد که خوانند</p></div>
<div class="m2"><p>به حق نعمت دارای دوران</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>که در دینی نظام ملک عالم</p></div>
<div class="m2"><p>که در ملکی قوام دین یزدان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>روانت را مبادا هیچ اندوه</p></div>
<div class="m2"><p>به ملک اندر روانت باد فرمان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در آن فترت که شد سلطان ماضی</p></div>
<div class="m2"><p>ز لشکرگه بدارالملک رضوان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>طناب خیمه عمرش گسستند</p></div>
<div class="m2"><p>از این آفت سرای تنگ میدان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>برفت آثار او از دین و دولت</p></div>
<div class="m2"><p>چو فر نو بهار از باغ و بستان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز کیهان شاه کیهان بس جوان رفت</p></div>
<div class="m2"><p>که لعنت باد بر کردار کیهان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بقای شاه اعظم باد با تو</p></div>
<div class="m2"><p>که هست از عمرتان آفاق عمران</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اگر نه رای تو بودی در آن وقت</p></div>
<div class="m2"><p>ز خون دریاصفت گشتی بیابان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سپاهی بر مثال کوه آهن</p></div>
<div class="m2"><p>به زور زال و سهم پور دستان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>حمایل کرده زهرآلود شمشیر</p></div>
<div class="m2"><p>زره را داده خون آلود خفتان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چتو تیغ سیاست برکشیدی</p></div>
<div class="m2"><p>بگشت از هول رنگ چرخ گردان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کمان بر قلب تن بشکست قبضه</p></div>
<div class="m2"><p>خدنگ از فرق سر بنداخت پیکان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به بحر کین نهنگان بلا را</p></div>
<div class="m2"><p>ز سهم تو فرو ریزید دندان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>یکی طوفان نشاندستی به تدبیر</p></div>
<div class="m2"><p>که بگسستی ز هم گردون و ارکان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>اگر طوفان به عهد نوح برخاست</p></div>
<div class="m2"><p>تو آن نوحی که کردی دفع طوفان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>خداوندا قوامی بس قدیم است</p></div>
<div class="m2"><p>که دارد حق خدمتها فراوان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>رسیده پیش از این در دولت تو</p></div>
<div class="m2"><p>ز خدمتها به نعمتهای الوان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>کنون گر دورم از خدمت عجب نیست</p></div>
<div class="m2"><p>که باغ وصل دارد داغ هجران</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مرا گر فر تو بر سر نبودی</p></div>
<div class="m2"><p>نه دل بودی نه جان بودی نه جانان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به حمدالله نیم در خدمت تو</p></div>
<div class="m2"><p>ازین مشتی هوس پیمای نادان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ز باد طبعم از دریای خاطر</p></div>
<div class="m2"><p>بخیزد موجهای گوهرافشان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به ایوان تو در چون شعر خوانم</p></div>
<div class="m2"><p>فرود آید ز کیوان آب حیوان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گلستان سعادت درگه تو است</p></div>
<div class="m2"><p>قوامی بلبل آهسته دستان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به فضل خویشتن نزدیک دارش</p></div>
<div class="m2"><p>که بلبل دور ماند است از گلستان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>سلیمان واری اندر ملک و چاکر</p></div>
<div class="m2"><p>طلب کرد است هدهد را سلیمان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>الا تا از خدای اندر زمانه</p></div>
<div class="m2"><p>بود دولت دلیل و بخت برهان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>خدایت یار باد و بخت هم پشت</p></div>
<div class="m2"><p>زمانه یاور و دولت نگهبان</p></div></div>