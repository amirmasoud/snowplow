---
title: >-
    شمارهٔ  ۲۸ - در شکایت و گله و طلب شفاعت و صله
---
# شمارهٔ  ۲۸ - در شکایت و گله و طلب شفاعت و صله

<div class="b" id="bn1"><div class="m1"><p>ای خواجه ای که با تو کسی را قرین کنند</p></div>
<div class="m2"><p>کو را حکایت از بر چرخ برین کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بشنو ز فضل خویشتن از بنده یک سخن</p></div>
<div class="m2"><p>تا خلق شکر تو بر جان آفرین کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه شد است همت وجودت بر آسمان</p></div>
<div class="m2"><p>بیم است از آنکه هر دو مرا در زمین کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آحاد گشت ألوف امیدم ز هر دوان</p></div>
<div class="m2"><p>من طمع کرده تا عشراتم مائین کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این شاعران که پای برین آستان نهند</p></div>
<div class="m2"><p>دانی که شعر من علم آستین کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ایشان همه به شکر و رهی باشکایت است</p></div>
<div class="m2"><p>آری مگر به شهر شما در چنین کنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر روز ناامید چو برگردم از درت</p></div>
<div class="m2"><p>قومی ز به هر خور ز رهی دل حزین کنند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از خویشتن به من نگر ای صدر روزگار</p></div>
<div class="m2"><p>تا اهل روزگار تو را آفرین کنند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو«ن» در رکیب همت تو دست دل زدم</p></div>
<div class="m2"><p>گفتم مرا ز درگه تو اسب زین کنند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آخر عنایت تو به نزدیک میر چیست</p></div>
<div class="m2"><p>این سرکه را ز بهر چه در انگبین کنند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو می روی به درگه سلطان امیدوار</p></div>
<div class="m2"><p>تا خواجگان به مهر تو دل را رهین کنند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بنگر که بر دل تو چه آید از آن گروه</p></div>
<div class="m2"><p>گرو العیاذبالله با تو همین کنند</p></div></div>