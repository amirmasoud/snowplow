---
title: >-
    شمارهٔ  ۱۵ - در مدح و استعطاء است
---
# شمارهٔ  ۱۵ - در مدح و استعطاء است

<div class="b" id="bn1"><div class="m1"><p>ای جهان را بزرگیت معلوم</p></div>
<div class="m2"><p>وای خرد را کفایتت مفهوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخنت باد بر دل وزرا</p></div>
<div class="m2"><p>گرم چون انگبین و نرم چو موم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ولیت بر کنار آب حیات</p></div>
<div class="m2"><p>عدوت در میان باد سموم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن درختی که چون تو میوه دهد</p></div>
<div class="m2"><p>آفرین باد بر چنان بر و بوم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک دو هفته گذشت که این خادم</p></div>
<div class="m2"><p>کمتر آمد به پیش آن مخدوم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خشم کم گیر چون به مهراندر</p></div>
<div class="m2"><p>کرده ای اعتقاد من معلوم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از عقوبت به سم بود که ز بخت</p></div>
<div class="m2"><p>مانده باشم ز خدمتت محروم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من که باید که با تو بنشینم</p></div>
<div class="m2"><p>کز تو خیزد معیشت و مرسوم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه نشینم به تنگ دستی در</p></div>
<div class="m2"><p>پیش مشتی فراخ کون زن شوم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آری آری به طبع بنشیند</p></div>
<div class="m2"><p>مرغ می شوم بر درخت ز قوم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رنج نان دادن است و زن گادن</p></div>
<div class="m2"><p>که مرا جان برآرد از حلقوم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آدمی را دو محنت سنگی است</p></div>
<div class="m2"><p>رنج حلقوم و آفت خرطوم</p></div></div>