---
title: >-
    شمارهٔ  ۳۱ - در مدح و شکایت و گله و طلب عنایت و شفاعت وصله
---
# شمارهٔ  ۳۱ - در مدح و شکایت و گله و طلب عنایت و شفاعت وصله

<div class="b" id="bn1"><div class="m1"><p>ای خواجه ای که نیستت از خواجگان نظیر</p></div>
<div class="m2"><p>بر کهتران عزیزی و بر مهتران خطیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هستی تو آن رشید که از رشد دولتت</p></div>
<div class="m2"><p>بر چرخ بخت ماه سعادت بود منیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باهمت بلند تو بهتر زمین پست</p></div>
<div class="m2"><p>با دولت جوان تو خوش تر جهان پیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوهی ز بخشش تو گرانی بود سبک</p></div>
<div class="m2"><p>طفلی ز خانه تو صغیری بود کبیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خواجگی به فخر پذیرفته ام تو را</p></div>
<div class="m2"><p>در بندگی به فضل رهی را فرا پذیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زین کارمان برآر چو موی از میان ماست</p></div>
<div class="m2"><p>با ما چرا شدستی چون کارد با پنیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از تو شکایتی نکنم پیش کردگار</p></div>
<div class="m2"><p>گر تو عنایتیم کنی در بر امیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در بندمان مدار و نشانهای کژ مده</p></div>
<div class="m2"><p>ما را به پا مگیر و با دست راست گیر</p></div></div>