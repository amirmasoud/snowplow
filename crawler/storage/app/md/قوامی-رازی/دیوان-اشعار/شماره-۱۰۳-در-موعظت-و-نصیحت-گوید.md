---
title: >-
    شمارهٔ  ۱۰۳ - در موعظت و نصیحت گوید
---
# شمارهٔ  ۱۰۳ - در موعظت و نصیحت گوید

<div class="b" id="bn1"><div class="m1"><p>مکن دین در سر دنیا ز خودرائی و نادانی</p></div>
<div class="m2"><p>که این اقطاع شیطانی است و آن املاک یزدانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین ویرانه دیوان بی فرمان فرو منشین</p></div>
<div class="m2"><p>که تا خود را در آن ایوان سلیمان وار بنشانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کمین سازان شهوانی ترا در راه و توزیشان</p></div>
<div class="m2"><p>به عقل از جان نه آگاهی به شخص از جامه عریانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چراغ عقل روشن کن که اندر جستن مقصد</p></div>
<div class="m2"><p>رهی تاریک داری پیش و درتاریک ویرانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از این خون خوار محبس سالخورده رخت بیرون بر</p></div>
<div class="m2"><p>که برنامد به گیتی در به نیکی نام زندانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برین زندانی چو زندانی منه امروز باری دل</p></div>
<div class="m2"><p>که تا فردا از آن خوش بوستانی دادبستانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز عشق خان و مان کردن شدستی واله ای مسکین</p></div>
<div class="m2"><p>تو را تا خان و مان این است بی خانی و بی مانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چلیپا کرده شهوت را و زنار هوی بسته</p></div>
<div class="m2"><p>چو رهبانان درین دیرینه دیر تیز دورانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز هول مرگ نندیشی که من خود مرد سلطانم</p></div>
<div class="m2"><p>شود سلطان جانت مرگ اگر خود حال سلطانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اجل چون کوس بنوازد کجا فریادرس باشد</p></div>
<div class="m2"><p>سپهبد را سپهداری جهانبان را جهانبانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو ناوکهای ربانی روان گردد چه برخیزد</p></div>
<div class="m2"><p>ز جوشنهای سلطانی و خفتانهای خاقانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هزیمت رفتگان لشکر مرگند از این عالم</p></div>
<div class="m2"><p>جهانداران و جباران تورانی و ایرانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به رسم و فعل فرعونان چرا گشتستی ای نادان</p></div>
<div class="m2"><p>اگر در لشکر ایمان کنی موسی عمرانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کنی موسی عمرانی به لشکرگاه ایمان در</p></div>
<div class="m2"><p>ولی همراه موسی نیستی همکار ثعبانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به ظاهر آیت خیری به باطن آلت شری</p></div>
<div class="m2"><p>به رخ موسی و هارونی به دل فرعون و هامانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو را تا سرسپید آمد سیه دل گشتی از غفلت</p></div>
<div class="m2"><p>بسان دیو ظلمانی شدی ای پیر نورانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نماند مرد شادان دل به روز آفت پیری</p></div>
<div class="m2"><p>نباشد باغ آبادان به وقت باد ابانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نفیر از دست ما پیران که مردم را بریم از ره</p></div>
<div class="m2"><p>رفیق جمع گمراهان دلیل راه نادانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به ره گم کردن مردم همی چون نیک بندیشم</p></div>
<div class="m2"><p>چه این پیران نورانی چه غولان بیابانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز بهر تیرگی دادند ما را خلعت پیری</p></div>
<div class="m2"><p>به دست زاغ بفرستند منشور زمستانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز راه ریشخند و روی استهزا سخن گوئی</p></div>
<div class="m2"><p>اگر بینی یکی زین سهل جانب مرد سلمانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مسلمانی مسلم نیستت زیرا که در دنیا</p></div>
<div class="m2"><p>که با پرهیز سلمانی که با ملک سلیمانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به امر دیو آز اندر مجو ملک سلیمان را</p></div>
<div class="m2"><p>اگر مرد سلیمانی چرا پس دیو فرمانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مسلمانی ز تو رنجور کی گشتی به گیتی در</p></div>
<div class="m2"><p>ولیکن خواجه را رنجه نمی دارد مسلمانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز هر مردم فزون دارد هنرها مردم دینی</p></div>
<div class="m2"><p>ز هر یاقوت به گیرد بها یاقوت رمانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>قناعت چون جهانبانیست او را حرص چون درمان</p></div>
<div class="m2"><p>مده گر نیستی نادان جهانبانی به دربانی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نیفشانند بر تو جان به عقبی در نکورویان</p></div>
<div class="m2"><p>اگر تو دست چون مردان به دنیا برنیفشانی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز نادانی خورد اندوه دنیا مرد دنیائی</p></div>
<div class="m2"><p>ز بدبختی کشد پالان محنت اسب پالانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به بدسختن ترازووار داری خاطر و دل را</p></div>
<div class="m2"><p>چرا میزان طاعت نیستی معیار عصیانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سرای حرص و شهوت کردی آبادان عجب خلقی</p></div>
<div class="m2"><p>که هم معیار عصیانی و هم معمار شیطانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نکردی شرع را فرمان و دیوان را سیه کردی</p></div>
<div class="m2"><p>از آن کار گزاف توست نه شرعی نه دیوانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به حشر اندر سر از تاج سرافرازی برافرازد</p></div>
<div class="m2"><p>کرا تابان بود داغ پشیمانی ز پیشانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اگر خواهی که کم بینی خمار درد جاویدان</p></div>
<div class="m2"><p>مخور در خوردن سیکی به نقل الا پشیمانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به طاعت کردن یزدان ز دوزخ بازخر خود را</p></div>
<div class="m2"><p>درین معنی تأمل کن حقیقت دان که ارزانی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اگر تو قیمت طاعت ندانی بس عجب ناید</p></div>
<div class="m2"><p>چه دانی قیمت طاعت که قدر خود نمی دانی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به طاعت رنج بر خود نه گرت ناز ابد باید</p></div>
<div class="m2"><p>میسر کی شود هرگز تن آسانی به آسانی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شدستی بر گنه چیره که جبارم بیامرزد</p></div>
<div class="m2"><p>بکن درمان از این بهتر که فردا صعب درمانی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مکن با ایزد بی چون چنین یکباره گستاخی</p></div>
<div class="m2"><p>که آنگاهی عتابش را تحمل کرد نتوانی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گرت جنت همی باید زکوة از مال بیرون کن</p></div>
<div class="m2"><p>توانی خواستن مهمان ندانی کرد مهمانی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ربا دادن زنا کردن چه معنی دارد ای ویحک</p></div>
<div class="m2"><p>به سامانتر بزی باری نه تو مردی به سامانی؟</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ربا دادن چرا باید ندانی در جهان کاری</p></div>
<div class="m2"><p>که ایزد را بیازاری و خلقان را برنجانی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به آزار خدای و خلق و رنج نفس و درد دل</p></div>
<div class="m2"><p>بسی گرد آری از هر سو فذلک رایگان مانی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نه پیغمبر نه اهل البیت نه اصحاب کردند این</p></div>
<div class="m2"><p>عجب کاری است کار تو ندانم تا کرا مانی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به هنگام گنه کردن چو خورشید به پیدائی</p></div>
<div class="m2"><p>به وقت توبه آوردن چو سیمرغی به پنهانی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نداند مفسد بدبخت چون مصلح نکو گوئی</p></div>
<div class="m2"><p>ندانی باشه بدخوی چون بلبل خوش الحانی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ندانی علمها خواندن توانی عیبها جستن</p></div>
<div class="m2"><p>نه از مردان برهانی ز نامردان بهتانی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به سال و ماه در هرگز نخوانی سبعی از قرآن</p></div>
<div class="m2"><p>ولیکن هر شب و هر روز نقش مردمان خوانی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به سیرت غدر و تلبیسی به خصلت فسق و تزویری</p></div>
<div class="m2"><p>به فکرت حیلت و زرقی به خاطر مکر و دستانی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ریاورز و نفاق اندوز و پرتزویر و بی حاصل</p></div>
<div class="m2"><p>رباخوار و خدای آزار ومؤمن سوز و کسلانی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>اگر ایمان قوی داری میندیش از گنه کاری</p></div>
<div class="m2"><p>چه گر نااهل کرداری هم آخر ز اهل ایمانی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>همی ترس از گنه لیکن امید از فضل او مگسل</p></div>
<div class="m2"><p>که بس باشد تو را شحنه درین ره فضل یزدانی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دلی گرترسد ازدوزخ به دوزخ کی شود خسته</p></div>
<div class="m2"><p>کسی گر بد نیندیشد به بد کی باشد ارزانی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>قوامی قوت دین را به زهد اندر قیامت کن</p></div>
<div class="m2"><p>که تا روز قیامت جان ز قوم نار برهانی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تو را آراست است امروز مهمانخانه خاطر</p></div>
<div class="m2"><p>که بر خوان عبارتها به طبع خوش نمکدانی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>در اصل از نانبا زادی ولیکن زاد نام از تو</p></div>
<div class="m2"><p>به حکمت پایه نامی به نسبت مایه نانی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>شگفتا نانبائی را که هست اندر ضمیر تو</p></div>
<div class="m2"><p>ز استادان گردونی و مزدوران ارکانی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ز طبع آبی فرو ریزی خمیر از دل برانگیزی</p></div>
<div class="m2"><p>به فکرت آرد دربیزی به خاطر گنده گردانی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو آرد اجرام گردون را ببیز اندر ضمیر دل</p></div>
<div class="m2"><p>که با پرویزن پروین برین پیروزه دوکانی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ز بیاعان اندیشه همی خر گندم معنی</p></div>
<div class="m2"><p>درین دوکان جسمانی همی پز نان روحانی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>مگردان چون سنائی رخ ز گفتار بداندیشان</p></div>
<div class="m2"><p>که ایشان جمله ناجنس اند و تو نز جنس ایشانی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>خراسان و عراق امروز اقطاع دو شاعر شد</p></div>
<div class="m2"><p>قوامی را عراقی دان سنائی را خراسانی</p></div></div>