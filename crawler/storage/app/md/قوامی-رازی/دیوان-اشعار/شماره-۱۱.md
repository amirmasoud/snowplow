---
title: >-
    شمارهٔ  ۱۱
---
# شمارهٔ  ۱۱

<div class="b" id="bn1"><div class="m1"><p>مرکب اقبال را زین کرده‌ای</p></div>
<div class="m2"><p>نصرت ملک از پی دین کرده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسمان ملک را از روی و لفظ</p></div>
<div class="m2"><p>تاوگاه ماه و پروین کرده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در جهان چون پادشاهان کریم</p></div>
<div class="m2"><p>بخشش و بخشایش آیین کرده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از کرم این در خورد کز روی مهر</p></div>
<div class="m2"><p>چون کریمان پشت برکین کرده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاه عدل اندر ولایت گرگ را</p></div>
<div class="m2"><p>از کنار میش بالین کرده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان شبه سر کهر با اندام کلک</p></div>
<div class="m2"><p>عقل را دست گهرچین کرده‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زان سیه‌رویان خط بر نامه‌ها</p></div>
<div class="m2"><p>شهر بند هندوان چین کرده‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اختیار کعبه کرده سخت نیک</p></div>
<div class="m2"><p>به اختیاری که اختیار این کرده‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون توانی شد به کعبه کز سخا</p></div>
<div class="m2"><p>کعبه عالم ورامین کرده‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شعر من جان سنایی تازه کرد</p></div>
<div class="m2"><p>تا مرا از فضل تلقین کرده‌ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با قوامی هرچه اندر ری کنی</p></div>
<div class="m2"><p>با سنائی آن به غزنین کرده‌ای</p></div></div>