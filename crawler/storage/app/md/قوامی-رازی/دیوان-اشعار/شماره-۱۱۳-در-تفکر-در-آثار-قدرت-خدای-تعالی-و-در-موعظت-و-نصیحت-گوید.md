---
title: >-
    شمارهٔ  ۱۱۳ - در تفکر در آثار قدرت خدای تعالی و در موعظت و نصیحت گوید
---
# شمارهٔ  ۱۱۳ - در تفکر در آثار قدرت خدای تعالی و در موعظت و نصیحت گوید

<div class="b" id="bn1"><div class="m1"><p>خداوندی است عالم را جهان آرای و گیتی بان</p></div>
<div class="m2"><p>که عالم را همی دارد نگاری چون نگارستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگه دارنده خلقان؛ پدید آرنده گیتی</p></div>
<div class="m2"><p>که رحمتهاش بی حد است و نعمتهاش بی پایان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهانداری که می دارد ؛ به ترتیب و نسق عالم</p></div>
<div class="m2"><p>بهر فصلی به دیگر شکل و هر وقتی به دیگر سان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گهی روز آورد گه شب ؛ گهی خورشید و گاهی مه</p></div>
<div class="m2"><p>گهی سرما گهی گرما؛ گهی افزون گهی نقصان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بفصل نوبهار اندر؛ بهشت آئین کند گیتی</p></div>
<div class="m2"><p>عروسان بهاری را؛ ببندد کله در بستان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز جرم ابر هر ساعت زند بر مه سراپرده</p></div>
<div class="m2"><p>بدست باد هر لحظه کند در باغ شادروان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رخ بستان کند تازه ؛ دل مرغان کند خرم</p></div>
<div class="m2"><p>نهان گل کند پیدا ؛دهان غنچه را خندان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز نرگس تاج زر سازد ؛ ز گلبن تخت پیروزه</p></div>
<div class="m2"><p>ز برگ لاله ها گلشن ؛ ز شاخ سروها ایوان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز صنعش عندلیب ازشاخ سوگند دهد گل را</p></div>
<div class="m2"><p>به چشم نرگس سیراب و روی لاله نعمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گهی ازقطره باران به شاخ ارغوان سازد</p></div>
<div class="m2"><p>کنار لاله پرلؤلؤ میان باغ پرمرجان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو او بستان بیاراید به گلها راست پنداری</p></div>
<div class="m2"><p>بحورالعین همی بخشد ز جنت حله ها رضوان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو از گل صورت انگیزد بجنبد باد نوروزی</p></div>
<div class="m2"><p>چو رنگ گل برآمیزد بیاید بوی تابستان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز تقدیرش به تابستان چنان گرما شود غالب</p></div>
<div class="m2"><p>که گردد ماهی اندر آب و مرغ اندر هوا بریان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو خورشید درخشنده کله گوشه برافرازد</p></div>
<div class="m2"><p>قبای آتشین گردد به تن برتوزی و کتان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز گرما و عرق مردم غرق در آب و در آتش</p></div>
<div class="m2"><p>تو گوئی کز تنور نوح برخیزد همی طوفان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کند برگ درختان را به ماه مهرگان زرین</p></div>
<div class="m2"><p>چو گردد ابر گوهر بار بر کهسار سیم افشان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز صنعش خوشه انگور زیر برگ پنداری</p></div>
<div class="m2"><p>چو شاخ زلف معشوق است در زیر کله پنهان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>درون نار خون آلود کرده چون دل عاشق</p></div>
<div class="m2"><p>برون سیب رنگی ساخته چون عارض جانان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز شاخ آویخته در باغ شکل سیب سیمائی</p></div>
<div class="m2"><p>تو گوئی گوی سیمین است بر پیروزه گون چوگان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یکی چون گوهر اندر زر یکی چون لعل در مینا</p></div>
<div class="m2"><p>یکی چون ماه در عقرب یکی چون زهره در میزان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>میان آن چنان گرما کمال قدرت ایزد</p></div>
<div class="m2"><p>یکی سرما پدید آرد که گرما را کند ریحان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جهان آهنگری گردد که خورشیدش بود کوره</p></div>
<div class="m2"><p>به دستش باده چون سوهان به پیشش آب چون سندان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>درآید زاغ چون ابلیس در بستان چون جنت</p></div>
<div class="m2"><p>درخت از حله نوروز چون آدم شود عریان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بیاراید به دیباهای چینی باغ و بستان را</p></div>
<div class="m2"><p>ز قطر برف چون کافور و پر زاغ و چون قطران</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به امرش آبها در جو فسرده خونها در تن</p></div>
<div class="m2"><p>نفس در حلقها بی جان و جان در شخصها رنجان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جهان را دم فرو رفته فلک را روی بگرفته</p></div>
<div class="m2"><p>هوا چون بیدلی گریان زمین چون مرده بی جان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که را باشد چنین قدرت که داند این همه حکمت</p></div>
<div class="m2"><p>به جز دارنده دانای پیدا بین پنهان دان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جز او از شمس نورانی به میزان و حمل هرگز</p></div>
<div class="m2"><p>که دارد روز و شب را راست بی معیار و بی میزان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همی نوروز مه روزی تماشا را برون رفتم</p></div>
<div class="m2"><p>که تا از دیده عبرت ببینم قدرت یزدان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز بس گلهای گوناگون جهان دیدم چو طاوسی</p></div>
<div class="m2"><p>که اندر جلوه خوبی خرامد گرد سروستان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به عینه سروها در باغ و گلها در چمن گوئی</p></div>
<div class="m2"><p>نگارانند در ایوان سوارانند در میدان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به داغ عشق هر نوگل نوای مرغکی دیدم</p></div>
<div class="m2"><p>به گرد باغها تازان زنان بر شاخها دستان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سرایان پیش گل بلبل ز سوز عشق و درد دل</p></div>
<div class="m2"><p>تو در خوبی سرافرازی و من در عشق سرگردان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو دستم سوی گل یازید بلبل گفت از طیره</p></div>
<div class="m2"><p>دریغ آید مرا گوهر به دست مردم نادان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خرد با من به سر گفتا به چشم سرسری منگر</p></div>
<div class="m2"><p>درین گلهای رنگارنگ وین مرغان نغزالحان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چرا بیت و غزل خوانی همی آواز مرغان را</p></div>
<div class="m2"><p>مگو از خویشتن چیزی که معلومت نباشد آن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>زبان حال مرغان را به گوش هوش و جان بشنو</p></div>
<div class="m2"><p>که در تسبیح میگویند:«یا حنان و یا منان »</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>من مسکین بیچاره به اندیشه فرو رفته</p></div>
<div class="m2"><p>ز صنع آفریننده به مانده عاجز و حیران</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>برآورد از گریبان بحرا بر آستین پر در</p></div>
<div class="m2"><p>کشان از ماه تا ماهی بهیبت دامن خفتان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دلش بر چشمه خورشید و تن بر تارک گردون</p></div>
<div class="m2"><p>سرش بر خوشه پروین و پی بر گوشه عمان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به اسب باد بر تازان کمان قوس قزح کرده</p></div>
<div class="m2"><p>نهیبش رعد و تیغش برق و تیرش قطره باران</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>همه عالم درو حیران که عالم کی زند برهم</p></div>
<div class="m2"><p>بمانده او درآن عاجز که ایزد کی دهد فرمان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>سپاس آن پادشاهی را کزین گونه بیاراید</p></div>
<div class="m2"><p>نگارستان گیتی را به رنگ و صورت الوان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بعلم محض بی حیلت به صنع پاک بی آلت</p></div>
<div class="m2"><p>کواکب تاخت بر گردون و گردون ساخت بر ارکان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>مکان و کان چه داند کرد عالم عالمی داند</p></div>
<div class="m2"><p>که کان پردازد اندر کوه و گوهر سازد اندر کان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نگهبانی است عالم را که این ترتیبها داند</p></div>
<div class="m2"><p>چه داند تابش اختر چه باشد قوت دوران</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>همه کس داند این معنی که آخر آفریننده</p></div>
<div class="m2"><p>نه ناهید است و نه خورشید و نه بهرام و نه کیوان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>خدای پاک بی همتاست دور از وهم و از خاطر</p></div>
<div class="m2"><p>که یارش نیست اندر ملک و مثلش نیست در ارکان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>شهان بردرگهش بنده جهان از نعمتش خرم</p></div>
<div class="m2"><p>زمین از قدرتش ساکن سپهر از هیبتش لرزان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بزرگ و خرد و نیک و بد به فرمانش کمربسته</p></div>
<div class="m2"><p>که او را بنده فرمانند هم سلطان و هم دربان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چن و فریاد رس باشد بهر وقتی که درمانی</p></div>
<div class="m2"><p>پس او را باش و او را خواه و او را خوان و او را دان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>الا ای بنده گمره به ره باز آی و طاعت کن</p></div>
<div class="m2"><p>مکن چندین گنه تا کی سیه داری دل و دیوان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>برین درگه همی باید به جان و دل کمربستن</p></div>
<div class="m2"><p>کزین خدمت به دست آید بقای ملک جاویدان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به خدمت کردن سلطان ز طاعت باز می مانی</p></div>
<div class="m2"><p>اگر گیرد ایزد حمایت کی کند سلطان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بترس از آفت دنیا طلب کن راحت عقبی</p></div>
<div class="m2"><p>که درمانی است این بی درد و آن دردی است بی درمان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به مهر و خلق و خوشخوئی چرا خندان نداری لب</p></div>
<div class="m2"><p>چه گیری کینه اندر دل چه داری در شکم دندان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>نباشد مردم دیندار کین دار و جفاپیشه</p></div>
<div class="m2"><p>نپاید نور با ظلمت نسازد کفر با ایمان</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>تو مرد جبرئیل و مرد میکائیل کی باشی</p></div>
<div class="m2"><p>که با ابلیس هم عهدی و بالاقیس هم پیمان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>تو را باکار خیر و کار طاعت نیست کار ای دل</p></div>
<div class="m2"><p>به دنبال هوس می تاز و عیش خویشتن میزان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>کسی کو مرد نباشد به دینش کی بود رغبت</p></div>
<div class="m2"><p>که آنجا ماتم و گریه است و اینجا مطرب و مهمان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>اگر خواهی که در جنت عزیز مملکت باشی</p></div>
<div class="m2"><p>به رنج نفس در دنیا چو یوسف باش در زندان</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ظفر برتو نیابد دیو اگر قرآن سپرسازی</p></div>
<div class="m2"><p>نپاید آیت دیوان ز پیش آیت قرآن</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>به خدمت کردن سلطان تن اندر داده هرزه</p></div>
<div class="m2"><p>اگرایزد کند قهری حمایت کی کند سلطان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بنای انبان همی مانی ازین گفتار بیهوده</p></div>
<div class="m2"><p>که از راه گلو نائی و از طبل شکم انبان</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>اگرتو راستی ورزی حقیقت تاجور گردی</p></div>
<div class="m2"><p>که تیر از راستی دارد به سر بر تاج از پیکان</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>وگر انصاف ده باشی بماند نام تو برجا</p></div>
<div class="m2"><p>که چون سلطان بود عادل بماند عالم آبادان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چو یابند ازتو انصافی بماند نام نیک از تو</p></div>
<div class="m2"><p>به نیکی در جهان ماند است نام عدل نوشروان</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>به نیکی کوش کز نیکی نجات آخرت باشد</p></div>
<div class="m2"><p>به دارای دوست روزی چند دست از حیله و دستان</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>همه نیکی بدین اندرز موسی بود و از هارون</p></div>
<div class="m2"><p>همه بدنامی دنیا ز فرعون است و ز هامان</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>زکاتت می بباید داد و تو رشوت همی گیری</p></div>
<div class="m2"><p>به واجب چون همی ندهی بنا واجب ز کس مستان</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>خردمندی به جای آور، می نگیز آتش فتنه</p></div>
<div class="m2"><p>وگر ننشیند این آتش به آب عافیت بنشان</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>همه ساله همی گوئی که مفسد رایتی برکش</p></div>
<div class="m2"><p>نگوئی هیچ یک روزی که مقری آیتی برخوان</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>به شبها در عبادتها طلب کن جنت باقی</p></div>
<div class="m2"><p>که اندر موضع تاریک باشد چشمه حیوان</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>به آسانی نشاید یافت ملک جاودانی را</p></div>
<div class="m2"><p>که در دریا غرق گردد به طمع سود بازرگان</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>نماز و روزه و خیرات و احکام است دینت را</p></div>
<div class="m2"><p>که گرد قلعه در باشد درو در بند شهرستان</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>تو را دانم شگفت آید اگر شه نامه نشنیدی</p></div>
<div class="m2"><p>حدیث مردی سرخاب و زور رستم دستان</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>اگرتو هفت خوان دل به تیغ عقل بگشائی</p></div>
<div class="m2"><p>نباشد پهلوان چون تو نه در توران و نه در ایران</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>جهان چون مادری پیر است و تو چون کودکی خردی</p></div>
<div class="m2"><p>درو آویخته از عشق همچون بچه در پستان</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>به چشم عشق تو مانند معشوقی است تازنده</p></div>
<div class="m2"><p>چو دیدی راحت وصلش ببینی آفت هجران</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>تومانی گوسفندی را که باشد در چراگاهی</p></div>
<div class="m2"><p>نمی دانی که خواهی بود عید مرگ را قربان</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>عمارتها مکن نچندین سرای و باغ دنیا را</p></div>
<div class="m2"><p>که می بینی که از مرگ است خان و مانها ویران</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>نهنگ مرگ بسیاری بیفکند است مردان را</p></div>
<div class="m2"><p>که ازهر شیرمردی شیرتر بودند در جولان</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>بسا معشوق زیبا رخ بمانده زیر خاک اندر</p></div>
<div class="m2"><p>که دلداران چین بودند و مه رویان ترکستان</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>ترا هم باز باید خورد یک روزی همین شربت</p></div>
<div class="m2"><p>اگر تلخ است و گر شیرین گردشوار و گر آسان</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>اگر مردی و گر نامرد اگر زشتی و گر نیکو</p></div>
<div class="m2"><p>اگر پیری و گر برنا و گر دانا و گر نادان</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>اجل همچون پلی باشد به راه آن جهان اندر</p></div>
<div class="m2"><p>که آنجا رهگذر دارد همه کس کائنا من کان</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>غم روزی مخور چندین که ازپیش تو روزی ده</p></div>
<div class="m2"><p>تو را چندانکه می باید فرستاده است صد چندان</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>غم تو کی توان خوردن کز آنها نیستی آخر</p></div>
<div class="m2"><p>کشیدن باز نتوانی همی یک مرده نان از خوان</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>چو آمد نوبت پیری ز دنیا دست کوته کن</p></div>
<div class="m2"><p>بیاور اندکی طاعت که خوش ناید همه عصیان</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>شد آن دولت که بودی تازه همچون شاخ نوروزی</p></div>
<div class="m2"><p>کنون پژمرده و زردی چو باغ ازباد مهریجان</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>جوانی و جمالت رفت و تو اینک بخواهی شد</p></div>
<div class="m2"><p>چه داری گوش طاعت کن چه باشی کاهل و کسلان</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>خداوند جهان با تو کرمها کرده چندینی</p></div>
<div class="m2"><p>دلیری کرد با ایزد چنین یکبارگی نتوان</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>نخواهد کرد برتو کاراگر زین بیشتر گویم</p></div>
<div class="m2"><p>مگر درتو فکند ایزد ز خشم خویشتن خذلان</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>نصیحتها ز من بشنو اگر چت سخت میآید</p></div>
<div class="m2"><p>چو کری ناقوامیها قوامی را مکن تاوان</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>قوامی نانبائی شد از بازار توحیدش</p></div>
<div class="m2"><p>به شرق و غرب در نان است و بر هفت آسمان دکان</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>بدان ده در؛ همی کاندر گندمهای پاکش را</p></div>
<div class="m2"><p>که ملک سرمدی ملک است و فضل ایزدی دهقان</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>جهان او را جوالی گشت و گردون آسیائی شد</p></div>
<div class="m2"><p>که آنجا آرد مهتابست و اینجا برجها قبان</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>زرش در کیسه اقبال و نان بر خوانچه دولت</p></div>
<div class="m2"><p>ستاره گفت بستان هین فلک گفتا بیاور هان</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>شب و روزش دو مزدورند کز خورشید و ماه او را</p></div>
<div class="m2"><p>تنور افروخت در مغرب که از مشرق برآید نان</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>خردمند از این نان خور که تا جانت بیفزاید</p></div>
<div class="m2"><p>که باشد میده ما را خمیر از عقل و آب از جان</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>تو را گر نان ما باید به جان و دل بخر ورنه</p></div>
<div class="m2"><p>از این دکان فراتر شو که اینجا نیست نان ارزان</p></div></div>