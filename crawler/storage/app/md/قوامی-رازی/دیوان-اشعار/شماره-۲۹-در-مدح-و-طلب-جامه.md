---
title: >-
    شمارهٔ  ۲۹ - در مدح و طلب جامه
---
# شمارهٔ  ۲۹ - در مدح و طلب جامه

<div class="b" id="bn1"><div class="m1"><p>ای از کف تو هر گرسنه سیر</p></div>
<div class="m2"><p>وای هر زبر از همت تو زیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر کف آزم همان چنان</p></div>
<div class="m2"><p>که آهو بچه ای در دهان شیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بد دلم و نیم سیر آز</p></div>
<div class="m2"><p>شاید که تو سیری و «هم» دلیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک جامه دبیقی به من فرست</p></div>
<div class="m2"><p>که عقل زبونست و آز چیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ور زانکه ندانی که از چه رنگ</p></div>
<div class="m2"><p>همچون من «و» تو سیر و نیم سیر</p></div></div>