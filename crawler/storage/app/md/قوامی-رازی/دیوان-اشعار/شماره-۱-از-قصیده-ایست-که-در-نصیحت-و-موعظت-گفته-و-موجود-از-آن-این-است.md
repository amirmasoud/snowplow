---
title: >-
    شمارهٔ  ۱ - از قصیده‌ایست که در نصیحت و موعظت گفته و موجود از آن این است
---
# شمارهٔ  ۱ - از قصیده‌ایست که در نصیحت و موعظت گفته و موجود از آن این است

<div class="b" id="bn1"><div class="m1"><p>ساکن شو و تو طاعت ایزد کن اختیار</p></div>
<div class="m2"><p>کز مرد بختیار جزین اختیار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرهیزگار باش و چه سودست پند من</p></div>
<div class="m2"><p>که امروز روز مردم پرهیزگار نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرد خدای شو که خدای است دستگیر</p></div>
<div class="m2"><p>دل بر کن از جهان که جهان پایدار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان زلزله که بود گه یحیی بن معاذ</p></div>
<div class="m2"><p>ری شد خراب اگر چه تو را اعتبار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی جان شدند سیصدو پنجه هزار خلق</p></div>
<div class="m2"><p>معلوم کن چو قول منت استوار نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دارنده زمانه تواناست همچنان</p></div>
<div class="m2"><p>در کارهاش هیچ کس آموزگار نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرو العیاذ بالله با ما همان کند</p></div>
<div class="m2"><p>این روزگار بهتر از آن روزگار نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این قوم زان گروه بسی باز پس ترند</p></div>
<div class="m2"><p>ری را اگر چه آن درج و کارو بار نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناقد چنانکه بود بصیر است همچنان</p></div>
<div class="m2"><p>زر سرای ضرب عمل را عیار نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا زلزله ست چیره تری بر گناهها</p></div>
<div class="m2"><p>گوئی تو را هدایت پروردگار نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از هیبت خدای نترسی ز ابلهی</p></div>
<div class="m2"><p>دیوانگی و ابلهی از افتخار نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آگه نه که از طرف بیشه قضا</p></div>
<div class="m2"><p>شیر عذاب را ز تو بهتر شکار نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا تو بدی کنی به دل آید ز پیش تو</p></div>
<div class="m2"><p>زیرا که باده شهوت خمر و خمار نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای قوم از ین عذاب بترسید زینهار</p></div>
<div class="m2"><p>که این کار جز علامت اصحاب نار نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بنگر که ما چه لشکر ظلمیم کز خدای</p></div>
<div class="m2"><p>بر فرق ما جز آتش دوزخ نثار نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زین تندبادها که به هم بر زند جهان</p></div>
<div class="m2"><p>در دیده ها به جای بصر جز غبار نیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از کردگار باد عذابست خاک پاش</p></div>
<div class="m2"><p>وز بحر رحمت ابر کرم قطر«ه» بار نیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر بنده خاکسار شد از باد، شکرهاست</p></div>
<div class="m2"><p>کز خشم پادشاه جهان سنگسار نیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نزدیک خاطر و دلت ای مرد خاکسار</p></div>
<div class="m2"><p>روز شمار و هیبت او در شمار نیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در زیر خاک زلزله خواهد تو را شکست</p></div>
<div class="m2"><p>جز خاک تیره مالش تو خاکسار نیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در ملک پادشا چه که عالم شود خراب</p></div>
<div class="m2"><p>از مرگ زنگئی خلل زنگبار نیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو خواه باش و خواه نه در عالم خدای</p></div>
<div class="m2"><p>بر هردو گام چون تو کم از صدهزار نیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زنهار خواستی چو در افتاد زلزله</p></div>
<div class="m2"><p>ای ظالمی که از تو به جهان زینهار نیست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ایزد تو را به فضل و کرم زینهار داد</p></div>
<div class="m2"><p>از بهر آن که او چو تو زنهار خوار نیست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مردی مبر به درگه ایزد نیاز بر</p></div>
<div class="m2"><p>کان صدر عزتست و صف کارزار نیست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آنجاست سجده گاه ضعیفان و عاجزان</p></div>
<div class="m2"><p>ناوردگاه رستم و اسفندیار نیست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بر درگه خدای جهان عاجزی نمای</p></div>
<div class="m2"><p>کان جایگاه جز به در عجز بار نیست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>امروز تو زدی به خصومت قویتری</p></div>
<div class="m2"><p>و امسال قوت تو چو پیرار و پار نیست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>داننده که گردش لیل و نهار ساخت</p></div>
<div class="m2"><p>داند که خیری از تو به لیل و نهار نیست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>لیل و نهار بر تو به غفلت بسی گذشت</p></div>
<div class="m2"><p>و اندر تو جز جحود نهارا جهار نیست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اسب مراد تو به ره دین نمی رود</p></div>
<div class="m2"><p>ره را چه عیب مرکب تو راهوار نیست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گرچه پیاده ای به ره عقل و عافیت</p></div>
<div class="m2"><p>میدان فتنه را چو تو چابک سوار نیست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اینجا مکن قرار که جائی ست بی قرار</p></div>
<div class="m2"><p>جای تو جز به منزل دارالقرار نیست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از بهر لفظ فحش ندارد لب تو مهر</p></div>
<div class="m2"><p>وز راه مهر دین شترت را مهار نیست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بی شک تن هیزم دوزخ کند خدای</p></div>
<div class="m2"><p>زیرا که شاخ خیر تو را برگ و بار نیست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کس دیده نیست چون تو نکوروی زشت خوی</p></div>
<div class="m2"><p>چون خلقت تو صورت طاوس و مار نیست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>زهر کشنده مار ندارد چو خوی تو</p></div>
<div class="m2"><p>طاوس را چو روی تو رنگ و نگار نیست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نتوان از ین همه کرم و فضل کردگار</p></div>
<div class="m2"><p>گفتن که پادشاه جهان بردبار نیست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در بندگیش بسته میان باش کز نهیب</p></div>
<div class="m2"><p>دریای آتش غضبش را کنار نیست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>از آتش جهنم و «ا»ز خشم او بترس</p></div>
<div class="m2"><p>ای بی خبر ترا مگر از نار عار نیست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>با نفس خویش به شو و خیرات پیش گیر</p></div>
<div class="m2"><p>عذری بخواه اگر چه دلت خواستار نیست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آن را که با تو این همه نعمت همی کند</p></div>
<div class="m2"><p>در طاعتش چرا دلت اومیدوار نیست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ما ناکسیم اگر نه کریمست پادشا</p></div>
<div class="m2"><p>تقصیر بنده جرم خداوندگار نیست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بشنو قوامیا ز خرد پند و کار بند</p></div>
<div class="m2"><p>هرکو نه اهل پند بود هوشیار نیست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>«تو» پادشاه گنج قناعت شدی رواست</p></div>
<div class="m2"><p>گر تخت زر وافسر گوهر نگار نیست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>«بر تخت» عافیت شو و«ا»ز شرم پرده دار</p></div>
<div class="m2"><p>گربر در تو قاعده پرده دار نیست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ترک جهانیان کن و بر تخت عقل گوی</p></div>
<div class="m2"><p>ای پرده دار پرده فروهل که بار نیست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>با همگنان بگوی که دیوان شعر من</p></div>
<div class="m2"><p>باغی است که اندر و همه گل هست و خار نیست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>آن نانبامنم که چو دوکان خاطرم</p></div>
<div class="m2"><p>ایوان ملک و بارگه شهریار نیست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چون دانه های گندم پاکم به روشنی</p></div>
<div class="m2"><p>اندر خزینه ها گهر شاهوار نیست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>آن را که نیست گندم انبار دل چنین</p></div>
<div class="m2"><p>از آسیای فضل الاهیش بار نیست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>در حلق زیرکان جهان همچو نان من</p></div>
<div class="m2"><p>حلوای تر شهد و شکر خوشگوار نیست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نام نکوست حاصل نان سپید من</p></div>
<div class="m2"><p>وز مرد به ز نام نکو یادگار نیست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>هرکس که نیست درکف او قرص نان من</p></div>
<div class="m2"><p>از چرخش آفتاب و مه اندر کنارنیست</p></div></div>