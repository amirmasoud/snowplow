---
title: >-
    شمارهٔ  ۹۶ - در مدح عمادالدین ابومحمد حسن بن محمد بن احمداسترآبادی قاضی ری گوید
---
# شمارهٔ  ۹۶ - در مدح عمادالدین ابومحمد حسن بن محمد بن احمداسترآبادی قاضی ری گوید

<div class="b" id="bn1"><div class="m1"><p>ای که در دنیا همه جدی و در دین سرسری</p></div>
<div class="m2"><p>چون شود دنیا میسر دین نباشد بر سری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اهل دنیا ز اهل دین دورند و این اولیتر است</p></div>
<div class="m2"><p>باکسان هرگز مبادا ناکسان را داوری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرد دین پرور نداند ساخت با دنیاپرست</p></div>
<div class="m2"><p>وین تعجب نیست خود با دیو کی سازد پری!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای ز غفلت دور گشته شرم دار از خویشتن</p></div>
<div class="m2"><p>هیچ می دانی که از عصیان به گرداب اندری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر جوان اومید پیری را همی حجت کند</p></div>
<div class="m2"><p>پس تو ای پیر ضغیف آخر چه حجت آوری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل سیه گشتی ز غفلت تا شدستی سرسپید</p></div>
<div class="m2"><p>روز کافوری به آید چون شود شب عنبری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تیزدندانی ز خشم و هیچ نندیشی ز مرگ</p></div>
<div class="m2"><p>کندکن دندان بنه گردن مکن گنداوری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرگ زلزالت بس ار قاف بقا را قوتی</p></div>
<div class="m2"><p>مرگ تحسیرت بس ار سیمرغ دولت را پری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شیرمردان کز حمیت گرد زن گشته نیند</p></div>
<div class="m2"><p>دختران عمرشان را مرگ بستد دختری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر تو را در نرد محشر مهره های شبهت است</p></div>
<div class="m2"><p>کعبتین مرگ چون مالی کزین در ششدری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای شده اختر طلب بگذر ز اختر حق طلب</p></div>
<div class="m2"><p>زانکه از حق یافته بداختران نیک اختری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زافرینشها اگر چه چرخ و اختر برترند</p></div>
<div class="m2"><p>گر بدانی قدر خود دانی کز ایشان برتری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر به طالع درخداوندی دهد اخترشناس</p></div>
<div class="m2"><p>اختری را کو ز نور شمس خواهد یاوری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کی خداوندی کند در طالع اختر مر تو را</p></div>
<div class="m2"><p>چون کند در طبخ کردن آفتابت چاکری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مشتری نزد تو سعد است ارنه نزدیک خرد</p></div>
<div class="m2"><p>چه چراغی زابگینه چه ز گردون مشتری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای نهاده مشتری را نام سعد این فتنه چیست</p></div>
<div class="m2"><p>کت خدای آسمان چون خود نهاده مشتری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کشتی دل را ز ایمان بادبانی برفراز</p></div>
<div class="m2"><p>تا درین دریای زرین موج مسکین لنگری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از ره انصاف در دنیا به حق نزدیک باش</p></div>
<div class="m2"><p>تا نبینی در جهنم دور باش از آذری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گرتو دانا بودئی بودی تو همچو خارخسگ</p></div>
<div class="m2"><p>هم ز نادانی است رخسارت چو گلبرگ طری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عاقلان گریان ز عقل و جاهلان خندان ز جهل</p></div>
<div class="m2"><p>عاشقان را بی دلی به نیکوان را دلبری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هرکجا باشی خداوند جهان را بنده باش</p></div>
<div class="m2"><p>خواه رومی باش و خوه چینی و خواهی خاوری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آفریننده دو عالم را یکی باشد ولیک</p></div>
<div class="m2"><p>آتش الله خواند و این ایزد و آن تنگری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گفتم اندر جستن آب حیات معرفت</p></div>
<div class="m2"><p>در میان ظلمت اندیشه خضری دیگری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون تفحص کردم احوال تو را از حرص و حقد</p></div>
<div class="m2"><p>عالمی یأجوج دل در صورت اسکندری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عوج شهوت را غذائی عادتن را لذتی</p></div>
<div class="m2"><p>عید دل را انبه عود هوی را مجمری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون توانی تاخت اسب عقل در میدان که تو</p></div>
<div class="m2"><p>زیر مهد شهوت اندر بسته همچون استری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در معاصی همچو مردی در نشاط عشرتی</p></div>
<div class="m2"><p>در عبادت چون زنی رنجورتن بر بستری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از برون با نوش قندی وز درون با زهرنی</p></div>
<div class="m2"><p>ای به خلقت با شکونه بوالعجب نی شکری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آنگهی گوئی که جلاب وفا را شکرم</p></div>
<div class="m2"><p>نیستی آگه که فصاد جفا را نشتری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو مسلمانی به اسم و نامسلمانی به فعل</p></div>
<div class="m2"><p>گر مسلمانی چنین باشد عفاالله کافری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر مسلمان نیستی گبری مورز آئین شرع</p></div>
<div class="m2"><p>ور سلیمان نیستی دیوی مدار انگشتری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از ریا گریان و نالان چون تو بگزاری نماز</p></div>
<div class="m2"><p>در اجابت گوید ایزد زار نال و خون گری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>با دلی کژ همچو چنگی با دمی نالان چو نای</p></div>
<div class="m2"><p>پس به محراب اندرون زاهد نه ای خنیاگری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در یکی ماتم سرا بنشسته ای خندان و خوش</p></div>
<div class="m2"><p>همچنین سر در نهاده عاقبت را ننگری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در فلک بنگر که تا چون در قبای نیلئی</p></div>
<div class="m2"><p>در زمین بنگر که تا چون بر سر خاکستری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گوئی از دعوی که در مردی به از شیر نرم</p></div>
<div class="m2"><p>هیچ مردی را به مرد از دست و بازو نشمری</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نفس تو آبستن است از گونه گونه آرزو</p></div>
<div class="m2"><p>پس مرا برگوی آبستن چرائی گر نری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آدمی روی اژدهائی زانکه از آز و نیاز</p></div>
<div class="m2"><p>هفت سرداری ب فعل ارچه به خلقت یک سری</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هر زمان گوئی که اندر کسب کان گوهرم</p></div>
<div class="m2"><p>همچنین است ای پسر کانی ولی بدگوهری</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کهرباروی و عقیقین اشک از آنی کز هوس</p></div>
<div class="m2"><p>با بلورین دست و سیمین ساق و زرین ساغری</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همچو زن در انده زن سست رای و کژروی</p></div>
<div class="m2"><p>همچو زر ز اندیشه زر زرد روی و لاغری</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سغبه گیرد روزگارت چون گرفتی رنگ او</p></div>
<div class="m2"><p>پای بند گاو را گوساله سازد سامری</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نیست باکت زان صراطی کز برش چون پی نهی</p></div>
<div class="m2"><p>از قدم گوئی مگر بر نوک بران خنجری</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>آن گنهکاری مخروان کنون نفت سپید</p></div>
<div class="m2"><p>بر پل آتش ندانم روز حق چون بگذری</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هرکه روشن دل بود یک باره ایزد را بود</p></div>
<div class="m2"><p>کی تواند بود کار کاردانان سرسری</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بازکن دیده موحد وار در عالم نگر</p></div>
<div class="m2"><p>کت کند در راه صنع ایزد تعالی رهبری</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بامداد از هودج زرین چو بگشاید عروس</p></div>
<div class="m2"><p>روی بند لاجورد از روی چرخ چنبری</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>شامگاه آرند نخاسان گردونی به عرض</p></div>
<div class="m2"><p>صد هزاران گلرخ اندر جامه نیلوفری</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>عالمی آراسته حکمش به انواع حکم</p></div>
<div class="m2"><p>همچو رنگین نقشها بر جامه های ششتری</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>این همه صنع الهی بر تو همچون محضری است</p></div>
<div class="m2"><p>محضرش برخوان مکن با محضرش بدمحضری</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>کار شهرستان آبادان جاویدان بساز</p></div>
<div class="m2"><p>تا درین ربع خراب سهمناک منکری</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ناجوانمردا بهشتی را به ایمانی بخر</p></div>
<div class="m2"><p>تا چو زینجا رخت بربندی برانی یک سری</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>جاه بخشی ملک داری سرکشی فرمان دهی</p></div>
<div class="m2"><p>راز گوئی ناز جوئی خوش خوری جان پروری</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>این همه نعمت که الله راست در دارالثواب</p></div>
<div class="m2"><p>از برای توست اگر اینجا خری آنجا خوری</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>آن سخن نشنیده ای کان مرد حلواساز را</p></div>
<div class="m2"><p>مشتری گفتا که حلواها خورم گفت ار خری</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ای قوامی چون معانی شد عماد لفظ تو</p></div>
<div class="m2"><p>جهد آن کن تا به نزدیک عمادالدین بری</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>آن عمادالدین حق أقضی القضاة شرق و غرب</p></div>
<div class="m2"><p>کش رسد بر مهتران دین و دولت مهتری</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>پادشاه شرع و ملت خواجه درگاه و دین</p></div>
<div class="m2"><p>کاسمان را نیست در پهلوی او پهناوری</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>اوست بر جای رسول و هر که ورزد خدمتش</p></div>
<div class="m2"><p>در ره اسلام سلمانی کند یا بوذری</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>کی کند بدخواه با تأیید بختش همدمی</p></div>
<div class="m2"><p>چون کند عصفور با عنقای مغرب همبری</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>فضل حکم کس بود با فضل او گاه عیار</p></div>
<div class="m2"><p>چون درمهای بد و دینارهای جعفری</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>شمس را گوید زحل بر کوه حلمش لاله ای</p></div>
<div class="m2"><p>زهره را گوید قمر در باغ علمش عبهری</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ای که در ملت سپاه دین حق را خسروی</p></div>
<div class="m2"><p>وی که در دولت سرهفتم فلک را افسری</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>همچو خاک از حلم جان جاودانی را تنی</p></div>
<div class="m2"><p>همچو آب از علم شاخ زندگانی رابری</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>روز و شب در کار دینی سال و مه در شغل شرع</p></div>
<div class="m2"><p>از زبان و از قلم جان هزاران جانوری</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>دفتر فتوی نویسی خامه حجت زنی</p></div>
<div class="m2"><p>جامه اسلام دوزی پرده بدعت دری</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>از فصاحتهات پنداری که در تذکیر تو</p></div>
<div class="m2"><p>جبرئیلت مقرئی کرد است و عرشت منبری</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>مفتی دین خدای و داور خلق جهان</p></div>
<div class="m2"><p>حجت سلطان وقت و نایب پیغمبری</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چون قلم برداشتی پیدا شد اندر عهد تو</p></div>
<div class="m2"><p>از سرکلک حسن برهان تیغ حیدری</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>نعمتی داری بقائی دستگیری دولتی</p></div>
<div class="m2"><p>کارپرداز«ی» الهی پای مردی سنجری</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>عندلیبان فصاحت را به باغ آن محبرت</p></div>
<div class="m2"><p>همچو طاوسان خرد دادست نیکو منظری</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>خصم را با تو به یک جا کی بود هم صحبتی</p></div>
<div class="m2"><p>روز و شب را کی بود در خانه هم خواهری</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>هفت کشور هشت گشت است و همه عالم رهی</p></div>
<div class="m2"><p>فاضلان یکباره اذناب و تو در ده ده سری</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>خسرو «هر» هفت چرخ است آفتاب نوربخش</p></div>
<div class="m2"><p>گرچه بر چرخ چهارم باشدش رامشگری</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>مفتی هر هفت کشور پس تو باشی بر زمین</p></div>
<div class="m2"><p>از برای آنکه در خط چهارم کشوری</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>شکرایزد را که فرزندان تو همچون تواند</p></div>
<div class="m2"><p>زانکه ایشان در پاکند و تو بحر اخضری</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>سرور دین شد نظام الدین به همزادی تو را</p></div>
<div class="m2"><p>هرکه همزاد سران شد زیبد او را سروری</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>از نظام و از ظهیر و شمس و بدر و نجم و نور</p></div>
<div class="m2"><p>ساخت شش جوهر خرد تاباشی آن را جوهری</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>نه نه شش جوهر نه اند ایشان که شش سیاره اند</p></div>
<div class="m2"><p>آسمانی را که از رفعت تو شمس انوری</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>بر سپهر دین تو بادی مهر و ایشان مر تو را</p></div>
<div class="m2"><p>تیر و ناهید و زحل بهرام و ماه و مشتری</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>گرچه شمس الدین از این عقد سلامت غائب است</p></div>
<div class="m2"><p>خواهمش کردن نثار این در الفاظ دری</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>در خراسان شمس دین از بهر چه چندین بماند</p></div>
<div class="m2"><p>آری آری شمس آنجائی بود نه ایدری</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>شمس تو گر لشگری شد غم مخور که این طرفه نیست</p></div>
<div class="m2"><p>تا کواکب هست لشکر شمس باشد لشکری</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>تا نه بس مدت به کام دوستان اومید دار</p></div>
<div class="m2"><p>از خراسانت خور آید ای که غمخوار خوری</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>ای که در دارالکتب برآسمان علم محض</p></div>
<div class="m2"><p>در بر لوح و قلم روحانیان را حق تری</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>سالها بگذشت تا نامد قوامی پیش تو</p></div>
<div class="m2"><p>کوه دولت کرده بودم خالی از کبک دری</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>یا نفس حقا کزین خدمت دلم نستد برات</p></div>
<div class="m2"><p>عاقلان دانند کز اسلام نتوان شد بری</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>خاطر من بیش از این شایسته خدمت نبود</p></div>
<div class="m2"><p>بود در پرده عروس شعرم از بی زیوری</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>روزگاری در شود ناچار که آموزد به طبع</p></div>
<div class="m2"><p>خاطر نقاش دست از چین دل صورتگری</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>شادمان باش ای قوامی کز همه عالم توئی</p></div>
<div class="m2"><p>نانبائی کو ز نان جوید همی نام آوری</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>دست فکر تو به بازار دل از دکان طبع</p></div>
<div class="m2"><p>پخت نان شاعری را در تنور ساحری</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>گندم و نان تو و نان آژن نظم تراست</p></div>
<div class="m2"><p>زهره کرداری و مه جرمی و پروین پیکری</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>آمدی از نان به حکمت رفتی از حکمت بنان</p></div>
<div class="m2"><p>رو که پختی شاعری از نان و نان از شاعری</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>تا تو را هر مشتری باشند محمودی دگر</p></div>
<div class="m2"><p>گنده گرداند به دوکان تو اندر عنصری</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>تا ز روی عقل باشد در شمار بحر و بر</p></div>
<div class="m2"><p>در جهان چندانکه مقدور است خشکی و تری</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>نعمت دنیا شما را باد روزی خشک و تر</p></div>
<div class="m2"><p>تا دعا گویانتان باشند بحری و بری</p></div></div>