---
title: >-
    شمارهٔ  ۴۵ - در تغزل است
---
# شمارهٔ  ۴۵ - در تغزل است

<div class="b" id="bn1"><div class="m1"><p>ای پسر با ما بباید ساختن</p></div>
<div class="m2"><p>اسب را بر ما نباید تاختن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما غلام روی تو خواهیم گشت</p></div>
<div class="m2"><p>با تو سیم و خواجگی درباختن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش بود با زلف چون چوگان تو</p></div>
<div class="m2"><p>هر درنگی گوی و چوگان باختن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیک باشد با قد چون تیر تو</p></div>
<div class="m2"><p>هر زمان رفتن به تیر انداختن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از کرشمه دیده چون برهم زنی</p></div>
<div class="m2"><p>کیسه ها باید بدو پرداختن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون به وقت خنده بگشائی دو لب</p></div>
<div class="m2"><p>جانها باید درو انداختن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواهی از ابرو و چشم و جعد و زلف</p></div>
<div class="m2"><p>گردن از ترکان خوب افراختن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا کی ای شکر لب بادام چشم</p></div>
<div class="m2"><p>سوختن ما را و بی ما ساختن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو مرا بگداختی در عشق و من</p></div>
<div class="m2"><p>خواهمت هر ساعتی بنواختن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواهد از پیشت قوامی هر زمان</p></div>
<div class="m2"><p>تن چو چاکر پیشگان بگداختن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حق ما بشناس و خود واجب کند</p></div>
<div class="m2"><p>حق چاکر پیشگان بشناختن</p></div></div>