---
title: >-
    شمارهٔ  ۱۳ - قوامی در حق عمادی گوید
---
# شمارهٔ  ۱۳ - قوامی در حق عمادی گوید

<div class="b" id="bn1"><div class="m1"><p>«ای قوامی هر که چون تو نانباست</p></div>
<div class="m2"><p>تا قیام الساعه فخر شهر ماست »</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>«گندم فضل خدای از بهر تو</p></div>
<div class="m2"><p>کشته اندر دستگرد کبریاست »</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>«تخمش از تقدیس عرش ایزدیست</p></div>
<div class="m2"><p>آبش از کاریز وحی انبیاست »</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>«آسیابان آفتاب نوربخش</p></div>
<div class="m2"><p>آسمان تیز گردت آسیاست »</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>«آسیاهای تو را از بهر آرد</p></div>
<div class="m2"><p>زیردلو صدق در؛سنگ صفاست »</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>«رکن دوکان تو در شهر خرد</p></div>
<div class="m2"><p>بر سر بازار سدرالمنتهی است »</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>«از خمیر لطف دل قرص سخن</p></div>
<div class="m2"><p>وز تنور نور جان نور و ضیاست »</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>«نیز بهر طعمه جسمانیان</p></div>
<div class="m2"><p>کاسه و خوان را تریدد و غباست »</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>«کز برای واجب روحانیان</p></div>
<div class="m2"><p>لقمه تسبیح در حلق دعاست »</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>«نان موزون توای طباخ روح</p></div>
<div class="m2"><p>ناقدان سختند نفزود و نکاست »</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>«آتش طبع توشد معیار عقل</p></div>
<div class="m2"><p>زان تنورت با ترازو گشت راست »</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آفتاب ملک و دین رای تو باد</p></div>
<div class="m2"><p>آسمان عقل و جان جای تو باد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دست تو بر هشت جنت مطلق است</p></div>
<div class="m2"><p>بر سر هفتم فلک پای تو باد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نوبهار بوستان مملکت</p></div>
<div class="m2"><p>فر عدل عالم آرای تو باد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سایه خورشید فضل کردگار</p></div>
<div class="m2"><p>تاج فرق آسمان سای تو باد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر موافق گیسو«ی» حور بهشت</p></div>
<div class="m2"><p>بوی خلق شادی افزای تو باد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مار زرین خلقت مشگین سخن</p></div>
<div class="m2"><p>شکل کلک فلک پیمای تو باد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مور عنبر صورت کافور پوش</p></div>
<div class="m2"><p>خط روزآرای شب زای تو باد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا دل ابر بهاری در دهد</p></div>
<div class="m2"><p>مهر بر گردون زر اندای تو باد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا دم باد خزان زرگر شود</p></div>
<div class="m2"><p>کان به که در سیم پالای تو باد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ابر و برق و آسمان و آفتاب</p></div>
<div class="m2"><p>دست و کلک و همت و رای تو باد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بخت بر منشور زد توقیع ما</p></div>
<div class="m2"><p>تا عمادی وار شد ترجیع ما</p></div></div>