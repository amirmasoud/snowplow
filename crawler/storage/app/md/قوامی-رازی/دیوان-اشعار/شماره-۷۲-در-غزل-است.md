---
title: >-
    شمارهٔ  ۷۲ - در غزل است
---
# شمارهٔ  ۷۲ - در غزل است

<div class="b" id="bn1"><div class="m1"><p>چون تو که عزیز باشد ای جان</p></div>
<div class="m2"><p>آن را که تمیز باشد ای جان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عشق فدای کرد «جا»ن چیست</p></div>
<div class="m2"><p>جان کمتر چیز باشد ای جان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن پیماید طریق وصلت</p></div>
<div class="m2"><p>کش دل بقفیر باشد ای جان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق شود از وصل مبارز</p></div>
<div class="m2"><p>هر چند که حیز باشد ای جان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کودک به نشاط باشد آن جان</p></div>
<div class="m2"><p>که آنجای مویز باشد ای جان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود است قوامی ازتو خرم</p></div>
<div class="m2"><p>نشگفت که نیز باشد ای جان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در تو نتوان . . . حت که دانم</p></div>
<div class="m2"><p>وقت تو عزیز باشد ای جان</p></div></div>