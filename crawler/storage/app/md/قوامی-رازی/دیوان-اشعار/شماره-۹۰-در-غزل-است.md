---
title: >-
    شمارهٔ  ۹۰ - در غزل است
---
# شمارهٔ  ۹۰ - در غزل است

<div class="b" id="bn1"><div class="m1"><p>تا کرده ام ای دوست به عشق تو تولا</p></div>
<div class="m2"><p>شد رنج و بلای دلم آن قامت و بالا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شایند تو را جان و روان بنده و چاکر</p></div>
<div class="m2"><p>زیبند تو را حور و پری خادم و مولا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با طلعت میگونی و با دولت میمون</p></div>
<div class="m2"><p>با صورت حورائی و با دیده شهلا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حقا که چو جان دارمت و بلکه به از جان</p></div>
<div class="m2"><p>هست از دل «من» آگه الله تعالی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جانا بنه این خوی بد از سر که پس آنگه</p></div>
<div class="m2"><p>یک باره تبرا شود آن جمله تولا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در هیچ مرادی ز تو آری نشنیدیم</p></div>
<div class="m2"><p>آخر نعمی بایدمان تا کی ازاین لا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما را ز تو جان و دل و سیم و زر و کالا</p></div>
<div class="m2"><p>هر چند نباشد به همه وقت دریغا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با جور و جفاکردن تو فایده ای نیست</p></div>
<div class="m2"><p>چندان که همی گویمت البته و اصلا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تاروز قیامت ز مقالات قوامی</p></div>
<div class="m2"><p>گویند غزلهای تو دربزم اجلا</p></div></div>