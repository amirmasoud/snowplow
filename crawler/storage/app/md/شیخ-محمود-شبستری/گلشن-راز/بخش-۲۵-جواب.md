---
title: >-
    بخش ۲۵ - جواب
---
# بخش ۲۵ - جواب

<div class="b" id="bn1"><div class="m1"><p>کسی بر سر وحدت گشت واقف</p></div>
<div class="m2"><p>که او واقف نشد اندر مواقف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل عارف شناسای وجود است</p></div>
<div class="m2"><p>وجود مطلق او را در شهود است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به جز هست حقیقی هست نشناخت</p></div>
<div class="m2"><p>از آن رو هستی خود پاک در باخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وجود تو همه خار است و خاشاک</p></div>
<div class="m2"><p>برون انداز از خود جمله را پاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برو تو خانهٔ دل را فرو روب</p></div>
<div class="m2"><p>مهیا کن مقام و جای محبوب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو تو بیرون شدی او اندر آید</p></div>
<div class="m2"><p>به تو بی تو جمال خود نماید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کس کو از نوافل گشت محبوب</p></div>
<div class="m2"><p>به لای نفی کرد او خانه جاروب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درون جان محبوب او مکان یافت</p></div>
<div class="m2"><p>ز «بی یسمع و بی یبصر» نشان یافت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز هستی تا بود باقی بر او شین</p></div>
<div class="m2"><p>نیابد علم عارف صورت عین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>موانع تا نگردانی ز خود دور</p></div>
<div class="m2"><p>درون خانهٔ دل نایدت نور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>موانع چون در این عالم چهار است</p></div>
<div class="m2"><p>طهارت کردن از وی هم چهار است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نخستین پاکی از احداث و انجاس</p></div>
<div class="m2"><p>دوم از معصیت وز شر وسواس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سوم پاکی ز اخلاق ذمیمه است</p></div>
<div class="m2"><p>که با وی آدمی همچون بهیمه است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چهارم پاکی سر است از غیر</p></div>
<div class="m2"><p>که اینجا منتهی می‌گرددش سیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر آن کو کرد حاصل این طهارات</p></div>
<div class="m2"><p>شود بی شک سزاوار مناجات</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو تا خود را بکلی در نبازی</p></div>
<div class="m2"><p>نمازت کی شود هرگز نمازی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو ذاتت پاک گردد از همه شین</p></div>
<div class="m2"><p>نمازت گردد آنگه قرةالعین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نماند در میانه هیچ تمییز</p></div>
<div class="m2"><p>شود معروف و عارف جمله یک چیز</p></div></div>