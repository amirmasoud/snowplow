---
title: >-
    بخش ۱۳ - قاعده در تفکر در انفس
---
# بخش ۱۳ - قاعده در تفکر در انفس

<div class="b" id="bn1"><div class="m1"><p>به اصل خویش یک ره نیک بنگر</p></div>
<div class="m2"><p>که مادر را پدر شد باز و مادر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان را سر به سر در خویش می‌بین</p></div>
<div class="m2"><p>هر آنچ آمد به آخر پیش می‌بین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آخر گشت پیدا نفس آدم</p></div>
<div class="m2"><p>طفیل ذات او شد هر دو عالم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه آخر علت غایی در آخر</p></div>
<div class="m2"><p>همی گردد به ذات خویش ظاهر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ظلومی و جهولی ضد نورند</p></div>
<div class="m2"><p>ولیکن مظهر عین ظهورند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو پشت آینه باشد مکدر</p></div>
<div class="m2"><p>نماید روی شخص از روی دیگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شعاع آفتاب از چارم افلاک</p></div>
<div class="m2"><p>نگردد منعکس جز بر سر خاک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو بودی عکس معبود ملایک</p></div>
<div class="m2"><p>از آن گشتی تو مسجود ملایک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بود از هر تنی پیش تو جانی</p></div>
<div class="m2"><p>وز او در بسته با تو ریسمانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از آن گشتند امرت را مسخر</p></div>
<div class="m2"><p>که جان هر یکی در توست مضمر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو مغز عالمی زان در میانی</p></div>
<div class="m2"><p>بدان خود را که تو جان جهانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو را ربع شمالی گشت مسکن</p></div>
<div class="m2"><p>که دل در جانب چپ باشد از تن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جهان عقل و جان سرمایهٔ توست</p></div>
<div class="m2"><p>زمین و آسمان پیرایهٔ توست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ببین آن نیستی کو عین هستی است</p></div>
<div class="m2"><p>بلندی را نگر کو ذات پستی است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>طبیعی قوت تو ده هزار است</p></div>
<div class="m2"><p>ارادی برتر از حصر و شمار است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وز آن هر یک شده موقوف آلات</p></div>
<div class="m2"><p>ز اعضا و جوارح وز رباطات</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پزشکان اندر آن گشتند حیران</p></div>
<div class="m2"><p>فرو ماندند در تشریح انسان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نبرده هیچکس ره سوی این کار</p></div>
<div class="m2"><p>به عجز خویش هر یک کرده اقرار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز حق با هر یکی حظی و قسمی است</p></div>
<div class="m2"><p>معاد و مبدا هر یک به اسمی است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از آن اسمند موجودات قائم</p></div>
<div class="m2"><p>بدان اسمند در تسبیح دائم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به مبدا هر یکی زان مصدری شد</p></div>
<div class="m2"><p>به وقت بازگشتن چون دری شد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از آن در کامد اول هم بدر شد</p></div>
<div class="m2"><p>اگرچه در معاش از در به در شد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از آن دانسته‌ای تو جمله اسما</p></div>
<div class="m2"><p>که هستی صورت عکس مسما</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ظهور قدرت و علم و ارادت</p></div>
<div class="m2"><p>به توست ای بندهٔ صاحب سعادت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سمیعی و بصیری، حی و گویا</p></div>
<div class="m2"><p>بقا داری نه از خود لیک از آنجا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زهی اول که عین آخر آمد</p></div>
<div class="m2"><p>زهی باطن که عین ظاهر آمد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو از خود روز و شب اندر گمانی</p></div>
<div class="m2"><p>همان بهتر که خود را می‌ندانی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو انجام تفکر شد تحیر</p></div>
<div class="m2"><p>در اینجا ختم شد بحث تفکر</p></div></div>