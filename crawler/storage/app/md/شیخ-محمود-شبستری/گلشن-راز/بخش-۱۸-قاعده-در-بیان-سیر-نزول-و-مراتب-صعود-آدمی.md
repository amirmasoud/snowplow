---
title: >-
    بخش ۱۸ - قاعده در بیان سیر نزول و مراتب صعود آدمی
---
# بخش ۱۸ - قاعده در بیان سیر نزول و مراتب صعود آدمی

<div class="b" id="bn1"><div class="m1"><p>بدان اول که تا چون گشت موجود</p></div>
<div class="m2"><p>کز او انسان کامل گشت مولود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در اطوار جمادی بود پیدا</p></div>
<div class="m2"><p>پس از روح اضافی گشت دانا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس آنگه جنبشی کرد او ز قدرت</p></div>
<div class="m2"><p>پس از وی شد ز حق صاحب ارادت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به طفلی کرد باز احساس عالم</p></div>
<div class="m2"><p>در او بالفعل شد وسواس عالم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو جزویات شد بر وی مرتب</p></div>
<div class="m2"><p>به کلیات ره برد از مرکب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غضب شد اندر او پیدا و شهوت</p></div>
<div class="m2"><p>وز ایشان خاست بخل و حرص و نخوت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به فعل آمد صفتهای ذمیمه</p></div>
<div class="m2"><p>بتر شد از دد و دیو و بهیمه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تنزل را بود این نقطه اسفل</p></div>
<div class="m2"><p>که شد با نقطهٔ وحدت مقابل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد از افعال کثرت بی‌نهایت</p></div>
<div class="m2"><p>مقابل گشت از این رو با بدایت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر گردد مقید اندر این دام</p></div>
<div class="m2"><p>به گمراهی بود کمتر ز انعام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وگر نوری رسد از عالم جان</p></div>
<div class="m2"><p>ز فیض جذبه یا از عکس برهان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دلش با لطف حق همراز گردد</p></div>
<div class="m2"><p>از آن راهی که آمد باز گردد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز جذبه یا ز برهان حقیقی</p></div>
<div class="m2"><p>رهی یابد به ایمان حقیقی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کند یک رجعت از سجین فجار</p></div>
<div class="m2"><p>رخ آرد سوی علیین ابرار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به توبه متصف گردد در آن دم</p></div>
<div class="m2"><p>شود در اصطفی ز اولاد آدم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز افعال نکوهیده شود پاک</p></div>
<div class="m2"><p>چو ادریس نبی آید بر افلاک</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو یابد از صفات بد نجاتی</p></div>
<div class="m2"><p>شود چون نوح از آن صاحب ثباتی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نماند قدرت جزویش در کل</p></div>
<div class="m2"><p>خلیل آسا شود صاحب توکل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ارادت با رضای حق شود ضم</p></div>
<div class="m2"><p>رود چون موسی اندر باب اعظم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز علم خویشتن یابد رهائی</p></div>
<div class="m2"><p>چو عیسای نبی گردد سمائی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دهد یکباره هستی را به تاراج</p></div>
<div class="m2"><p>درآید از پی احمد به معراج</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>رسد چون نقطهٔ آخر به اول</p></div>
<div class="m2"><p>در آنجا نه ملک گنجد نه مرسل</p></div></div>