---
title: >-
    بخش ۳۰ - جواب
---
# بخش ۳۰ - جواب

<div class="b" id="bn1"><div class="m1"><p>انا الحق کشف اسرار است مطلق</p></div>
<div class="m2"><p>جز از حق کیست تا گوید انا الحق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه ذرات عالم همچو منصور</p></div>
<div class="m2"><p>تو خواهی مست گیر و خواه مخمور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در این تسبیح و تهلیلند دائم</p></div>
<div class="m2"><p>بدین معنی همی‌باشند قائم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر خواهی که گردد بر تو آسان</p></div>
<div class="m2"><p>«و ان من شیء» را یک ره فرو خوان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو کردی خویشتن را پنبه‌کاری</p></div>
<div class="m2"><p>تو هم حلاج‌وار این دم برآری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برآور پنبهٔ پندارت از گوش</p></div>
<div class="m2"><p>ندای «واحد القهار» بنیوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ندا می‌آید از حق بر دوامت</p></div>
<div class="m2"><p>چرا گشتی تو موقوف قیامت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درآ در وادی ایمن که ناگاه</p></div>
<div class="m2"><p>درختی گویدت «انی انا الله»</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روا باشد انا الحق از درختی</p></div>
<div class="m2"><p>چرا نبود روا از نیک‌بختی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر آن کس را که اندر دل شکی نیست</p></div>
<div class="m2"><p>یقین داند که هستی جز یکی نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>انانیت بود حق را سزاوار</p></div>
<div class="m2"><p>که هو غیب است و غایب وهم و پندار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جناب حضرت حق را دویی نیست</p></div>
<div class="m2"><p>در آن حضرت من و ما و تویی نیست</p></div></div>