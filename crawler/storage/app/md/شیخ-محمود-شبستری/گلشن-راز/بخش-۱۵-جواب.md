---
title: >-
    بخش ۱۵ - جواب
---
# بخش ۱۵ - جواب

<div class="b" id="bn1"><div class="m1"><p>دگر کردی سؤال از من که من چیست</p></div>
<div class="m2"><p>مرا از من خبر کن تا که من کیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو هست مطلق آید در اشارت</p></div>
<div class="m2"><p>به لفظ من کنند از وی عبارت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حقیقت کز تعین شد معین</p></div>
<div class="m2"><p>تو او را در عبارت گفته‌ای من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من و تو عارض ذات وجودیم</p></div>
<div class="m2"><p>مشبکهای مشکات وجودیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه یک نور دان اشباح و ارواح</p></div>
<div class="m2"><p>گه از آیینه پیدا گه ز مصباح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو گویی لفظ من در هر عبارت</p></div>
<div class="m2"><p>به سوی روح می‌باشد اشارت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو کردی پیشوای خود خرد را</p></div>
<div class="m2"><p>نمی‌دانی ز جزو خویش خود را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برو ای خواجه خود را نیک بشناس</p></div>
<div class="m2"><p>که نبود فربهی مانند آماس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من تو برتر از جان و تن آمد</p></div>
<div class="m2"><p>که این هر دو ز اجزای من آمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به لفظ من نه انسان است مخصوص</p></div>
<div class="m2"><p>که تا گویی بدان جان است مخصوص</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکی ره برتر از کون و مکان شو</p></div>
<div class="m2"><p>جهان بگذار و خود در خود جهان شو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز خط وهمی‌ های هویت</p></div>
<div class="m2"><p>دو چشمی می‌شود در وقت ریت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نماند در میانه رهرو راه</p></div>
<div class="m2"><p>چو های هو شود ملحق به الله</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بود هستی بهشت امکان چو دوزخ</p></div>
<div class="m2"><p>من و تو در میان مانند برزخ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو برخیزد تو را این پرده از پیش</p></div>
<div class="m2"><p>نماند نیز حکم مذهب و کیش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همه حکم شریعت از من توست</p></div>
<div class="m2"><p>که این بربستهٔ جان و تن توست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>من تو چون نماند در میانه</p></div>
<div class="m2"><p>چه کعبه چه کنشت چه دیرخانه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تعین نقطهٔ وهمی است بر عین</p></div>
<div class="m2"><p>چو صافی گشت غین تو شود عین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دو خطوه بیش نبود راه سالک</p></div>
<div class="m2"><p>اگر چه دارد آن چندین مهالک</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یک از های هویت در گذشتن</p></div>
<div class="m2"><p>دوم صحرای هستی در نوشتن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در این مشهد یکی شد جمع و افراد</p></div>
<div class="m2"><p>چو واحد ساری اندر عین اعداد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو آن جمعی که عین وحدت آمد</p></div>
<div class="m2"><p>تو آن واحد که عین کثرت آمد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کسی این راه داند کو گذر کرد</p></div>
<div class="m2"><p>ز جز وی سوی کلی یک سفر کرد</p></div></div>