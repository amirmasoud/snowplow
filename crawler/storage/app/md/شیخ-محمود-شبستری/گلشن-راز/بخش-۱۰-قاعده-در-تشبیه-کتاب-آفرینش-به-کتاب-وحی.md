---
title: >-
    بخش ۱۰ - قاعده در تشبیه کتاب آفرینش به کتاب وحی
---
# بخش ۱۰ - قاعده در تشبیه کتاب آفرینش به کتاب وحی

<div class="b" id="bn1"><div class="m1"><p>به نزد آنکه جانش در تجلی است</p></div>
<div class="m2"><p>همه عالم کتاب حق تعالی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عرض اعراب و جوهر چون حروف است</p></div>
<div class="m2"><p>مراتب همچو آیات وقوف است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از او هر عالمی چون سوره‌ای خاص</p></div>
<div class="m2"><p>یکی زان فاتحه و آن دیگر اخلاص</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نخستین آیتش عقل کل آمد</p></div>
<div class="m2"><p>که در وی همچو باء بسمل آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوم نفس کل آمد آیت نور</p></div>
<div class="m2"><p>که چون مصباح شد از غایت نور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیم آیت در او شد عرش رحمان</p></div>
<div class="m2"><p>چهارم «آیت الکرسی» همی دان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پس از وی جرمهای آسمانی است</p></div>
<div class="m2"><p>که در وی سورهٔ سبع المثانی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نظر کن باز در جرم عناصر</p></div>
<div class="m2"><p>که هر یک آیتی هستند باهر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پس از عنصر بود جرم سه مولود</p></div>
<div class="m2"><p>که نتوان کرد این آیات محدود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به آخر گشت نازل نفس انسان</p></div>
<div class="m2"><p>که بر ناس آمد آخر ختم قرآن</p></div></div>