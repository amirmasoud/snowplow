---
title: >-
    بخش ۲۳ - تمثیل در بیان سیر مراتب نبوت و ولایت
---
# بخش ۲۳ - تمثیل در بیان سیر مراتب نبوت و ولایت

<div class="b" id="bn1"><div class="m1"><p>چه نور آفتاب از شب جدا شد</p></div>
<div class="m2"><p>تو را صبح و طلوع و استوا شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دگر باره ز دور چرخ دوار</p></div>
<div class="m2"><p>زوال و عصر و مغرب شد پدیدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود نور نبی خورشید اعظم</p></div>
<div class="m2"><p>گه از موسی پدید و گه ز آدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر تاریخ عالم را بخوانی</p></div>
<div class="m2"><p>مراتب را یکایک باز دانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز خور هر دم ظهور سایه‌ای شد</p></div>
<div class="m2"><p>که آن معراج دین را پایه‌ای شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زمان خواجه وقت استوا بود</p></div>
<div class="m2"><p>که از هر ظل و ظلمت مصطفا بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به خط استوا بر قامت راست</p></div>
<div class="m2"><p>ندارد سایه پیش و پس چپ و راست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو کرد او بر صراط حق اقامت</p></div>
<div class="m2"><p>به امر «فاستقم» می‌داشت قامت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نبودش سایه کان دارد سیاهی</p></div>
<div class="m2"><p>زهی نور خدا ظل الهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ورا قبله میان غرب و شرق است</p></div>
<div class="m2"><p>ازیرا در میان نور غرق است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به دست او چو شیطان شد مسلمان</p></div>
<div class="m2"><p>به زیر پای او شد سایه پنهان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مراتب جمله زیر پایهٔ اوست</p></div>
<div class="m2"><p>وجود خاکیان از سایهٔ اوست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز نورش شد ولایت سایه گستر</p></div>
<div class="m2"><p>مشارق با مغارب شد برابر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز هر سایه که اول گشت حاصل</p></div>
<div class="m2"><p>در آخر شد یکی دیگر مقابل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کنون هر عالمی باشد ز امت</p></div>
<div class="m2"><p>رسولی را مقابل در نبوت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نبی چون در نبوت بود اکمل</p></div>
<div class="m2"><p>بود از هر ولی ناچار افضل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ولایت شد به خاتم جمله ظاهر</p></div>
<div class="m2"><p>بر اول نقطه هم ختم آمد آخر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از او عالم شود پر امن و ایمان</p></div>
<div class="m2"><p>جماد و جانور یابد از او جان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نماند در جهان یک نفس کافر</p></div>
<div class="m2"><p>شود عدل حقیقی جمله ظاهر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بود از سر وحدت واقف حق</p></div>
<div class="m2"><p>در او پیدا نماید وجه مطلق</p></div></div>