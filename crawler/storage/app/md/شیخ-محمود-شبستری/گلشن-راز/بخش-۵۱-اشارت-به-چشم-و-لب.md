---
title: >-
    بخش ۵۱ - اشارت به چشم و لب
---
# بخش ۵۱ - اشارت به چشم و لب

<div class="b" id="bn1"><div class="m1"><p>نگر کز چشم شاهد چیست پیدا</p></div>
<div class="m2"><p>رعایت کن لوازم را بدینجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز چشمش خاست بیماری و مستی</p></div>
<div class="m2"><p>ز لعلش گشت پیدا عین هستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز چشم اوست دلها مست و مخمور</p></div>
<div class="m2"><p>ز لعل اوست جانها جمله مستور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز چشم او همه دلها جگرخوار</p></div>
<div class="m2"><p>لب لعلش شفای جان بیمار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به چشمش گرچه عالم در نیاید</p></div>
<div class="m2"><p>لبش هر ساعتی لطفی نماید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دمی از مردمی دلها نوازد</p></div>
<div class="m2"><p>دمی بیچارگان را چاره سازد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به شوخی جان دمد در آب و در خاک</p></div>
<div class="m2"><p>به دم دادن زند آتش بر افلاک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از او هر غمزه دام و دانه‌ای شد</p></div>
<div class="m2"><p>وز او هر گوشه‌ای میخانه‌ای شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز غمزه می‌دهد هستی به غارت</p></div>
<div class="m2"><p>به بوسه می‌کند بازش عمارت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز چشمش خون ما در جوش دائم</p></div>
<div class="m2"><p>ز لعلش جان ما مدهوش دائم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به غمزه چشم او دل می‌رباید</p></div>
<div class="m2"><p>به عشوه لعل او جان می‌فزاید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو از چشم و لبش جویی کناری</p></div>
<div class="m2"><p>مر این گوید که نه آن گوید آری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز غمزه عالمی را کار سازد</p></div>
<div class="m2"><p>به بوسه هر زمان جان می‌نوازد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از او یک غمزه و جان دادن از ما</p></div>
<div class="m2"><p>وز او یک بوسه و استادن از ما</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز «لمح بالبصر» شد حشر عالم</p></div>
<div class="m2"><p>ز نفخ روح پیدا گشت آدم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو از چشم و لبش اندیشه کردند</p></div>
<div class="m2"><p>جهانی می‌پرستی پیشه کردند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نیاید در دو چشمش جمله هستی</p></div>
<div class="m2"><p>در او چون آید آخر خواب و مستی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وجود ما همه مستی است یا خواب</p></div>
<div class="m2"><p>چه نسبت خاک را با رب ارباب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خرد دارد از این صد گونه اشگفت</p></div>
<div class="m2"><p>که «ولتصنع علی عینی» چرا گفت</p></div></div>