---
title: >-
    بخش ۴۶ - قاعده در بیان معنی حشر
---
# بخش ۴۶ - قاعده در بیان معنی حشر

<div class="b" id="bn1"><div class="m1"><p>ز تو هر فعل که اول گشت صادر</p></div>
<div class="m2"><p>بر آن گردی به باری چند قادر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هر باری اگر نفع است اگر ضر</p></div>
<div class="m2"><p>شود در نفس تو چیزی مدخر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به عادت حالها با خوی گردد</p></div>
<div class="m2"><p>به مدت میوه‌ها خوش بوی گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آن آموخت انسان پیشه‌ها را</p></div>
<div class="m2"><p>وز آن ترکیب کرد اندیشه‌ها را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه افعال و اقوال مدخر</p></div>
<div class="m2"><p>هویدا گردد اندر روز محشر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو عریان گردی از پیراهن تن</p></div>
<div class="m2"><p>شود عیب و هنر یکباره روشن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تنت باشد ولیکن بی‌کدورت</p></div>
<div class="m2"><p>که بنماید از او چون آب صورت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه پیدا شود آنجا ضمایر</p></div>
<div class="m2"><p>فرو خوان آیت «تبلی السرائر»</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دگر باره به وفق عالم خاص</p></div>
<div class="m2"><p>شود اخلاق تو اجسام و اشخاص</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنان کز قوت عنصر در اینجا</p></div>
<div class="m2"><p>موالید سه گانه گشت پیدا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه اخلاق تو در عالم جان</p></div>
<div class="m2"><p>گهی انوار گردد گاه نیران</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تعین مرتفع گردد ز هستی</p></div>
<div class="m2"><p>نماند در نظر بالا و پستی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نماند مرگت اندر دار حیوان</p></div>
<div class="m2"><p>به یک رنگی برآید قالب و جان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بود پا و سر و چشم تو چون دل</p></div>
<div class="m2"><p>شود صافی ز ظلمت صورت گل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کند انوار حق بر تو تجلی</p></div>
<div class="m2"><p>ببینی بی‌جهت حق را تعالی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دو عالم را همه بر هم زنی تو</p></div>
<div class="m2"><p>ندانم تا چه مستی‌ها کنی تو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>«سقاهم ربهم» چبود بیندیش</p></div>
<div class="m2"><p>«طهورا» چیست صافی گشتن از خویش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زهی شربت زهی لذت زهی ذوق</p></div>
<div class="m2"><p>زهی حیرت زهی دولت زهی شوق</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خوشا آن دم که ما بی‌خویش باشیم</p></div>
<div class="m2"><p>غنی مطلق و درویش باشیم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نه دین نه عقل نه تقوی نه ادراک</p></div>
<div class="m2"><p>فتاده مست و حیران بر سر خاک</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بهشت و حور و خلد آنجا چه سنجد</p></div>
<div class="m2"><p>که بیگانه در آن خلوت نگنجد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو رویت دیدم و خوردم از آن می</p></div>
<div class="m2"><p>ندانم تا چه خواهد شد پس از وی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پی هر مستیی باشد خماری</p></div>
<div class="m2"><p>از این اندیشه دل خون گشت باری</p></div></div>