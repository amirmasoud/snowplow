---
title: >-
    بخش ۱ - دیباچه
---
# بخش ۱ - دیباچه

<div class="b" id="bn1"><div class="m1"><p>به نام آن که جان را فکرت آموخت</p></div>
<div class="m2"><p>چراغ دل به نور جان برافروخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز فضلش هر دو عالم گشت روشن</p></div>
<div class="m2"><p>ز فیضش خاک آدم گشت گلشن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>توانایی که در یک طرفةالعین</p></div>
<div class="m2"><p>ز کاف و نون پدید آورد کونین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو قاف قدرتش دم بر قلم زد</p></div>
<div class="m2"><p>هزاران نقش بر لوح عدم زد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آن دم گشت پیدا هر دو عالم</p></div>
<div class="m2"><p>وز آن دم شد هویدا جان آدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آدم شد پدید این عقل و تمییز</p></div>
<div class="m2"><p>که تا دانست از آن اصل همه چیز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو خود را دید یک شخص معین</p></div>
<div class="m2"><p>تفکر کرد تا خود چیستم من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز جزوی سوی کلی یک سفر کرد</p></div>
<div class="m2"><p>وز آنجا باز بر عالم گذر کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جهان را دید امر اعتباری</p></div>
<div class="m2"><p>چو واحد گشته در اعداد ساری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جهان خلق و امر از یک نفس شد</p></div>
<div class="m2"><p>که هم آن دم که آمد باز پس شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ولی آن جایگه آمد شدن نیست</p></div>
<div class="m2"><p>شدن چون بنگری جز آمدن نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به اصل خویش راجع گشت اشیا</p></div>
<div class="m2"><p>همه یک چیز شد پنهان و پیدا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تعالی الله قدیمی کو به یک دم</p></div>
<div class="m2"><p>کند آغاز و انجام دو عالم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جهان خلق و امر اینجا یکی شد</p></div>
<div class="m2"><p>یکی بسیار و بسیار اندکی شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همه از وهم توست این صورت غیر</p></div>
<div class="m2"><p>که نقطه دایره است از سرعت سیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یکی خط است از اول تا به آخر</p></div>
<div class="m2"><p>بر او خلق جهان گشته مسافر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در این ره انبیا چون ساربانند</p></div>
<div class="m2"><p>دلیل و رهنمای کاروانند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وز ایشان سید ما گشته سالار</p></div>
<div class="m2"><p>هم او اول هم او آخر در این کار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>احد در میم احمد گشت ظاهر</p></div>
<div class="m2"><p>در این دور اول آمد عین آخر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز احمد تا احد یک میم فرق است</p></div>
<div class="m2"><p>جهانی اندر آن یک میم غرق است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بر او ختم آمده پایان این راه</p></div>
<div class="m2"><p>در او منزل شده «ادعوا الی الله»</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مقام دلگشایش جمع جمع است</p></div>
<div class="m2"><p>جمال جانفزایش شمع جمع است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شده او پیش و دلها جمله از پی</p></div>
<div class="m2"><p>گرفته دست دلها دامن وی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در این ره اولیا باز از پس و پیش</p></div>
<div class="m2"><p>نشانی داده‌اند از منزل خویش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به حد خویش چون گشتند واقف</p></div>
<div class="m2"><p>سخن گفتند در معروف و عارف</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یکی از بحر وحدت گفت انا الحق</p></div>
<div class="m2"><p>یکی از قرب و بعد و سیر زورق</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یکی را علم ظاهر بود حاصل</p></div>
<div class="m2"><p>نشانی داد از خشکی ساحل</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یکی گوهر برآورد و هدف شد</p></div>
<div class="m2"><p>یکی بگذاشت آن نزد صدف شد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یکی در جزو و کل گفت این سخن باز</p></div>
<div class="m2"><p>یکی کرد از قدیم و محدث آغاز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>یکی از زلف و خال و خط بیان کرد</p></div>
<div class="m2"><p>شراب و شمع و شاهد را عیان کرد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>یکی از هستی خود گفت و پندار</p></div>
<div class="m2"><p>یکی مستغرق بت گشت و زنار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سخنها چون به وفق منزل افتاد</p></div>
<div class="m2"><p>در افهام خلایق مشکل افتاد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کسی را کاندر این معنی است حیران</p></div>
<div class="m2"><p>ضرورت می‌شود دانستن آن</p></div></div>