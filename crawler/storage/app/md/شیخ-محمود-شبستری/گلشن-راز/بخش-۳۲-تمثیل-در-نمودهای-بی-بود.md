---
title: >-
    بخش ۳۲ - تمثیل در نمودهای بی‌بود
---
# بخش ۳۲ - تمثیل در نمودهای بی‌بود

<div class="b" id="bn1"><div class="m1"><p>بنه آیینه‌ای اندر برابر</p></div>
<div class="m2"><p>در او بنگر ببین آن شخص دیگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی ره باز بین تا چیست آن عکس</p></div>
<div class="m2"><p>نه این است و نه آن پس کیست آن عکس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو من هستم به ذات خود معین</p></div>
<div class="m2"><p>ندانم تا چه باشد سایهٔ من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عدم با هستی آخر چون شود ضم</p></div>
<div class="m2"><p>نباشد نور و ظلمت هر دو با هم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو ماضی نیست مستقبل مه و سال</p></div>
<div class="m2"><p>چه باشد غیر از آن یک نقطهٔ حال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی نقطه است وهمی گشته ساری</p></div>
<div class="m2"><p>تو آن را نام کرده نهر جاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز از من اندر این صحرا دگر کیست</p></div>
<div class="m2"><p>بگو با من که تا صوت و صدا چیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عرض فانی است جوهر زو مرکب</p></div>
<div class="m2"><p>بگو کی بود یا خود کو مرکب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز طول و عرض و از عمق است اجسام</p></div>
<div class="m2"><p>وجودی چون پدید آمد ز اعدام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از این جنس است اصل جمله عالم</p></div>
<div class="m2"><p>چو دانستی بیار ایمان و فالزم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جز از حق نیست دیگر هستی الحق</p></div>
<div class="m2"><p>هوالحق گو و گر خواهی انا الحق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نمود وهمی از هستی جدا کن</p></div>
<div class="m2"><p>نه ای بیگانه خود را آشنا کن</p></div></div>