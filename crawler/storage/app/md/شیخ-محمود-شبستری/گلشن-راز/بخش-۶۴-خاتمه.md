---
title: >-
    بخش ۶۴ - خاتمه
---
# بخش ۶۴ - خاتمه

<div class="b" id="bn1"><div class="m1"><p>از آن گلشن گرفتم شمه‌ای باز</p></div>
<div class="m2"><p>نهادم نام او را گلشن راز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در او راز دل گلها شکفته است</p></div>
<div class="m2"><p>که تا اکنون کسی دیگر نگفته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زبان سوسن او جمله گویاست</p></div>
<div class="m2"><p>عیون نرگس او جمله بیناست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تامل کن به چشم دل یکایک</p></div>
<div class="m2"><p>که تا برخیزد از پیش تو این شک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببین منقول و معقول و حقایق</p></div>
<div class="m2"><p>مصفا کرده در علم دقایق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به چشم منکری منگر در او خوار</p></div>
<div class="m2"><p>که گلها گردد اندر چشم تو خار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشان ناشناسی ناسپاسی است</p></div>
<div class="m2"><p>شناسایی حق در حق شناسی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غرض زین جمله آن کز ما کند یاد</p></div>
<div class="m2"><p>عزیزی گویدم رحمت بر او باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به نام خویش کردم ختم و پایان</p></div>
<div class="m2"><p>الهی عاقبت محمود گردان</p></div></div>