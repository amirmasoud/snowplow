---
title: >-
    بخش ۲۰ - جواب به سال دوم
---
# بخش ۲۰ - جواب به سال دوم

<div class="b" id="bn1"><div class="m1"><p>کسی مرد تمام است کز تمامی</p></div>
<div class="m2"><p>کند با خواجگی کار غلامی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس آنگاهی که ببرید او مسافت</p></div>
<div class="m2"><p>نهد حق بر سرش تاج خلافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بقایی یابد او بعد از فنا باز</p></div>
<div class="m2"><p>رود ز انجام ره دیگر به آغاز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شریعت را شعار خویش سازد</p></div>
<div class="m2"><p>طریقت را دثار خویش سازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حقیقت خود مقام ذات او دان</p></div>
<div class="m2"><p>شده جامع میان کفر و ایمان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به اخلاق حمیده گشته موصوف</p></div>
<div class="m2"><p>به علم و زهد و تقوی بوده معروف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه با او ولی او از همه دور</p></div>
<div class="m2"><p>به زیر قبه‌های ستر مستور</p></div></div>