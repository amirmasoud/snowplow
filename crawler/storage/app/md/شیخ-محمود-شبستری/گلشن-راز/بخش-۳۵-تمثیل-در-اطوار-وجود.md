---
title: >-
    بخش ۳۵ - تمثیل در اطوار وجود
---
# بخش ۳۵ - تمثیل در اطوار وجود

<div class="b" id="bn1"><div class="m1"><p>بخاری مرتفع گردد ز دریا</p></div>
<div class="m2"><p>به امر حق فرو بارد به صحرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شعاع آفتاب از چرخ چارم</p></div>
<div class="m2"><p>بر او افتد شود ترکیب با هم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کند گرمی دگر ره عزم بالا</p></div>
<div class="m2"><p>در آویزد بدو آن آب دریا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو با ایشان شود خاک و هوا ضم</p></div>
<div class="m2"><p>برون آید نبات سبز و خرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غذای جانور گردد ز تبدیل</p></div>
<div class="m2"><p>خورد انسان و یابد باز تحلیل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شود یک نطفه و گردد در اطوار</p></div>
<div class="m2"><p>وز او انسان شود پیدا دگر بار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو نور نفس گویا بر تن آید</p></div>
<div class="m2"><p>یکی جسم لطیف و روشن آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شود طفل و جوان و کهل و کمپیر</p></div>
<div class="m2"><p>بیابد علم و رای و فهم و تدبیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رسد آنگه اجل از حضرت پاک</p></div>
<div class="m2"><p>رود پاکی به پاکی خاک با خاک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هم اجزای عالم چون نباتند</p></div>
<div class="m2"><p>که یک قطره ز دریای حیاتند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زمان چو بگذرد بر وی شود باز</p></div>
<div class="m2"><p>همه انجام ایشان همچو آغاز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رود هر یک از ایشان سوی مرکز</p></div>
<div class="m2"><p>که نگذارد طبیعت خوی مرکز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو دریایی است وحدت لیک پر خون</p></div>
<div class="m2"><p>کز او خیزد هزاران موج مجنون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نگر تا قطرهٔ باران ز دریا</p></div>
<div class="m2"><p>چگونه یافت چندین شکل و اسما</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بخار و ابر و باران و نم و گل</p></div>
<div class="m2"><p>نبات و جانور انسان کامل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همه یک قطره بود آخر در اول</p></div>
<div class="m2"><p>کز او شد این همه اشیا ممثل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جهان از عقل و نفس و چرخ و اجرام</p></div>
<div class="m2"><p>چو آن یک قطره دان ز آغاز و انجام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اجل چون در رسد در چرخ و انجم</p></div>
<div class="m2"><p>شود هستی همه در نیستی گم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو موجی بر زند گردد جهان طمس</p></div>
<div class="m2"><p>یقین گردد «کان لم تغن بالامس»</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خیال از پیش برخیزد به یک بار</p></div>
<div class="m2"><p>نماند غیر حق در دار دیار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو را قربی شود آن لحظه حاصل</p></div>
<div class="m2"><p>شوی تو بی تویی با دوست واصل</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وصال این جایگه رفع خیال است</p></div>
<div class="m2"><p>چو غیر از پیش برخیزد وصال است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مگو ممکن ز حد خویش بگذشت</p></div>
<div class="m2"><p>نه او واجب شد و نه واجب او گشت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر آن کو در معانی گشت فایق</p></div>
<div class="m2"><p>نگوید کین بود قلب حقایق</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هزاران نشاه داری خواجه در پیش</p></div>
<div class="m2"><p>برو آمد شد خود را بیندیش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز بحث جزو و کل نشئات انسان</p></div>
<div class="m2"><p>بگویم یک به یک پیدا و پنهان</p></div></div>