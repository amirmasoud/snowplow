---
title: >-
    بخش ۲۲ - قاعده در حکمت وجود اولیا
---
# بخش ۲۲ - قاعده در حکمت وجود اولیا

<div class="b" id="bn1"><div class="m1"><p>نبوت را ظهور از آدم آمد</p></div>
<div class="m2"><p>کمالش در وجود خاتم آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ولایت بود باقی تا سفر کرد</p></div>
<div class="m2"><p>چو نقطه در جهان دوری دگر کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ظهور کل او باشد به خاتم</p></div>
<div class="m2"><p>بدو گردد تمامی دور عالم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وجود اولیا او را چو عضوند</p></div>
<div class="m2"><p>که او کل است و ایشان همچو جزوند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو او از خواجه یابد نسبت تام</p></div>
<div class="m2"><p>از او با ظاهر آید رحمت عام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شود او مقتدای هر دو عالم</p></div>
<div class="m2"><p>خلیفه گردد از اولاد آدم</p></div></div>