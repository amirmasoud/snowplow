---
title: >-
    بخش ۳۱ - قاعده در بطلان حلول و اتحاد
---
# بخش ۳۱ - قاعده در بطلان حلول و اتحاد

<div class="b" id="bn1"><div class="m1"><p>من و ما و تو و او هست یک چیز</p></div>
<div class="m2"><p>که در وحدت نباشد هیچ تمییز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر آن کو خالی از خود چون خلا شد</p></div>
<div class="m2"><p>انا الحق اندر او صوت و صدا شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شود با وجه باقی غیر هالک</p></div>
<div class="m2"><p>یکی گردد سلوک و سیر و سالک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حلول و اتحاد از غیر خیزد</p></div>
<div class="m2"><p>ولی وحدت همه از سیر خیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تعین بود کز هستی جدا شد</p></div>
<div class="m2"><p>نه حق شد بنده نه بنده خدا شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حلول و اتحاد اینجا محال است</p></div>
<div class="m2"><p>که در وحدت دویی عین ضلال است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وجود خلق و کثرت در نمود است</p></div>
<div class="m2"><p>نه هرچ آن می‌نماید عین بود است</p></div></div>