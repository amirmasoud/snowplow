---
title: >-
    بخش ۴۱ - قاعده در بیان اقسام فضیلت
---
# بخش ۴۱ - قاعده در بیان اقسام فضیلت

<div class="b" id="bn1"><div class="m1"><p>اصول خلق نیک آمد عدالت</p></div>
<div class="m2"><p>پس از وی حکمت و عفت شجاعت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حکیمی راست گفتار است و کردار</p></div>
<div class="m2"><p>کسی کو متصف گردد بدین چار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به حکمت باشدش جان و دل آگه</p></div>
<div class="m2"><p>نه گربز باشد و نه نیز ابله</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به عفت شهوت خود کرده مستور</p></div>
<div class="m2"><p>شره همچون خمود از وی شده دور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شجاع و صافی از ذل و تکبر</p></div>
<div class="m2"><p>مبرا ذاتش از جبن و تهور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عدالت چون شعار ذات او شد</p></div>
<div class="m2"><p>ندارد ظلم از آن خلقش نکو شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه اخلاق نیکو در میانه است</p></div>
<div class="m2"><p>که از افراط و تفریطش کرانه است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میانه چون صراط مستقیم است</p></div>
<div class="m2"><p>ز هر دو جانبش قعر جحیم است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به باریکی و تیزی موی و شمشیر</p></div>
<div class="m2"><p>نه روی گشتن و بودن بر او دیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عدالت چون یکی دارد ز اضداد</p></div>
<div class="m2"><p>همی هفت آمد این اضداد ز اعداد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به زیر هر عدد سری نهفت است</p></div>
<div class="m2"><p>از آن درهای دوزخ نیز هفت است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنان کز ظلم شد دوزخ مهیا</p></div>
<div class="m2"><p>بهشت آمد همیشه عدل را جا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جزای عدل، نور و رحمت آمد</p></div>
<div class="m2"><p>سزای ظلم، لعن و ظلمت آمد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ظهور نیکویی در اعتدال است</p></div>
<div class="m2"><p>عدالت جسم را اقصی کمال است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مرکب چون شود مانند یک چیز</p></div>
<div class="m2"><p>ز اجزا دور گردد فعل و تمییز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بسیط الذات را مانند گردد</p></div>
<div class="m2"><p>میان این و آن پیوند گردد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نه پیوندی که از ترکیب اجزاست</p></div>
<div class="m2"><p>که روح از وصف جسمیت مبراست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو آب و گل شود یکباره صافی</p></div>
<div class="m2"><p>رسد از حق بدو روح اضافی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو یابد تسویت اجزای ارکان</p></div>
<div class="m2"><p>در او گیرد فروغ عالم جان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شعاع جان سوی تن وقت تعدیل</p></div>
<div class="m2"><p>چو خورشید و زمین آمد به تمثیل</p></div></div>