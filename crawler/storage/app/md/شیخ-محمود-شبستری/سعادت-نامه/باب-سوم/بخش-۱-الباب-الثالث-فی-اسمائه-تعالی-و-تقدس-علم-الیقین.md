---
title: >-
    بخش ۱ - الباب الثّالث فی اسمائه تعالی و تقدّس  علم الیقین
---
# بخش ۱ - الباب الثّالث فی اسمائه تعالی و تقدّس  علم الیقین

<div class="b" id="bn1"><div class="m1"><p>نصّ قرآن به نام خالق ما</p></div>
<div class="m2"><p>وارد است قوله «له الاسما»</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حضرت حق گشوده عزّ و علاه</p></div>
<div class="m2"><p>در قرآن به لفظ «بسم اللّه»</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اسمش این گنج را کلید آمد</p></div>
<div class="m2"><p>هرچه هستی است زو پدید آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لفظ اللّه اسم حضرت ذات</p></div>
<div class="m2"><p>غیر از آن جمله اسمهای صفات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اسم اعظم بدو سزاوار است</p></div>
<div class="m2"><p>وین چنین در صحیح و اخبار است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرنه او نام خویشتن گفتی</p></div>
<div class="m2"><p>کیست آنکس که این سخن گفتی</p></div></div>