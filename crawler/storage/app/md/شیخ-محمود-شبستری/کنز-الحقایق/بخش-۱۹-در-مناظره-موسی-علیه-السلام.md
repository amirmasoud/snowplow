---
title: >-
    بخش ۱۹ - در مناظرهٔ موسی علیه‌السلام
---
# بخش ۱۹ - در مناظرهٔ موسی علیه‌السلام

<div class="b" id="bn1"><div class="m1"><p>چو موسی باز می‌گردید از طور</p></div>
<div class="m2"><p>در آن وادی سیاهی دید از دور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو نزدیکش رسید او بود شیطان</p></div>
<div class="m2"><p>که می‌نالید او از دوری و عصیان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو موسی دید او را رحمش آمد</p></div>
<div class="m2"><p>کمانش شد که آن دم خشمش آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به شیطان گفت موسی ای گنهکار</p></div>
<div class="m2"><p>چرا سجده نکردی تا شوی خوار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگفتا زان سبب سجده نکردم</p></div>
<div class="m2"><p>که ترسیدم مبادا چون تو گردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگفتا من چه گشتم در نبوت</p></div>
<div class="m2"><p>بگفتا اوفتادی از فتوت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگفتا چون فتادم هین بیان کن</p></div>
<div class="m2"><p>عیانم نیست این بر من عیان کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگفتا خواستی از دوست دیدار</p></div>
<div class="m2"><p>چرا کردی نظر آنجا به کهسار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو روی از وی بگرداندی ندانی</p></div>
<div class="m2"><p>شوی خسته ز قول لن ترانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو بودم من به عشق او یگانه</p></div>
<div class="m2"><p>مرا می‌آزمود این بُد بهانه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز عصیان بس چها آمد به رویم</p></div>
<div class="m2"><p>نجستم غیر او و هم نجویم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز عشقش سجدهٔ آدم نکردم</p></div>
<div class="m2"><p>چو حق باشد سوی آدم نگردم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به غیر حق دگر چیزی ندانم</p></div>
<div class="m2"><p>اگر نزدیک یا دورم همانم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بگفت این‌ها و از موسی جدا شد</p></div>
<div class="m2"><p>ندانستش چه افتاد و کجا شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یقین دان عشق کار سرسری نیست</p></div>
<div class="m2"><p>حقیقت مرد عاشق هر دری نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کسی کش عشق شخصی هست در پوست</p></div>
<div class="m2"><p>نخواهد غیر او هرچند نیکوست</p></div></div>