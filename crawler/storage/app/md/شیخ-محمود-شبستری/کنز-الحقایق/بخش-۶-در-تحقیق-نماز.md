---
title: >-
    بخش ۶ - در تحقیق نماز
---
# بخش ۶ - در تحقیق نماز

<div class="b" id="bn1"><div class="m1"><p>خشوع مومنان جان نماز است</p></div>
<div class="m2"><p>از آن معنی که با او دوست راز است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر از جان و دل با حق به رازی</p></div>
<div class="m2"><p>یقین می‌دان که دایم در نمازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر چه افضل طاعت نماز است</p></div>
<div class="m2"><p>فضیلت بیشتر اندر نیاز است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مصلی را فلاح اندر خشوع‌ست</p></div>
<div class="m2"><p>خشوع دوستان در عین جوع‌ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازیرا جوع باشد قوت مردان</p></div>
<div class="m2"><p>شکم چون پر شود می‌دان ز شیطان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ببُر از خلق و با حق گیر پیوند</p></div>
<div class="m2"><p>به کلّی اقتدا کن رکعتی چند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز اول بفکن این دنیا پس سر</p></div>
<div class="m2"><p>پس آن گاهی بگو الله اکبر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز خاطر دور کن افعال مَنهی</p></div>
<div class="m2"><p>به قول و فعل گو وجّهت وجهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو استادی دوانستی که جای‌ست</p></div>
<div class="m2"><p>به هر سویی که روی آری خدای‌ست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو میدانی که نیت چیست باری</p></div>
<div class="m2"><p>به نیت کن چو خواهی کرد کاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قرائت چیست با حق راز گفتن</p></div>
<div class="m2"><p>نیاز خویش با حق باز گفتن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حضور قلب می‌باید در این کار</p></div>
<div class="m2"><p>اگر داری وگرنه رو به دست آر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر چه ما نمازی می‌گزاریم</p></div>
<div class="m2"><p>ولی در وی حضور دل نداریم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خداوندا حضوری بخش ما را</p></div>
<div class="m2"><p>اجابت کن به فضلت این دعا را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو را تا بود تو اندر وجود است</p></div>
<div class="m2"><p>تنت فارق چو شیطان از سجود است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ولی چون بود خود را ترک کردی</p></div>
<div class="m2"><p>برو کن سجده اکنون زانکه مردی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>برو نیت نکو کن پس قدم نه</p></div>
<div class="m2"><p>که نیت مومنان را از عمل به</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نماز پنج وقتی سهل باشد</p></div>
<div class="m2"><p>توان کردن چون این کس اهل باشد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نماز مومنان این بُد که گفتم</p></div>
<div class="m2"><p>به یک معنی و دیگرها نهفتم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نماز مخلصان برتر از این‌ست</p></div>
<div class="m2"><p>ندانستی‌ش پنداری همین است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نمازی کان به حق دین را ستون‌ست</p></div>
<div class="m2"><p>یقین می‌دان صلوه دائمون‌ست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بسی خفتی کنون برخیز از خواب</p></div>
<div class="m2"><p>جماعت فوت خواهد گشت دریاب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نخست از فاتحه بشناس خود را</p></div>
<div class="m2"><p>بخوان پس قل هو الله احد را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نماز معنوی زان علی بود</p></div>
<div class="m2"><p>که جان او ز نور حق جلی بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که پیکان برکشیدش مرد از پای</p></div>
<div class="m2"><p>نجنبید از حضور خویش از جای</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو هم گر عابدی بگذار عادت</p></div>
<div class="m2"><p>که این‌ست ای اخی سر عبادت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نمازی کز سر صدق و صفا نیست</p></div>
<div class="m2"><p>اگر در کعبه بگذاری روا نیست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مکن سهو نمازت در ره دین</p></div>
<div class="m2"><p>بخوان یک راه ویل للمصلین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نمازی کان به سهو آری تمامت</p></div>
<div class="m2"><p>جزایش ویل باشد در قیامت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>برو جان پد بشنو ز محمود</p></div>
<div class="m2"><p>کز اینش جز نصیحت نیست مقصود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نماز از صدق کن کان‌ست اولی</p></div>
<div class="m2"><p>که تا یابی جزایش قرب مولی</p></div></div>