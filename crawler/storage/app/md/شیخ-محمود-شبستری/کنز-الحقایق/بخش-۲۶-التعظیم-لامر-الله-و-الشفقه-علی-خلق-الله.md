---
title: >-
    بخش ۲۶ - التعظیم لامر الله و الشفقه علی خلق الله
---
# بخش ۲۶ - التعظیم لامر الله و الشفقه علی خلق الله

<div class="b" id="bn1"><div class="m1"><p>به جان تعظیم امر حق به جا آر</p></div>
<div class="m2"><p>بدل بر خلق شفقت نیز می‌دار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شریعت ورز کان تعظیم امر است</p></div>
<div class="m2"><p>که شفقت نیکوی با زید و عمرو است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برای خود به امرش کار می‌کن</p></div>
<div class="m2"><p>مسلمانی خود اظهار می‌کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر آنچت گفت حق کلی به جا آر</p></div>
<div class="m2"><p>نه بهر نفس خود بهر خدا آر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که تعظیم آن بود کان را کنی کار</p></div>
<div class="m2"><p>نداری کار او بر جان خود بار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رها کن بوالفضولی و هوس را</p></div>
<div class="m2"><p>مرنج از کس مرنجان نیز کس را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به ناخوانده مشو مهمان مردم</p></div>
<div class="m2"><p>منه باری ز خود بر جان مردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وگر آیند مهمانان به ناگاه</p></div>
<div class="m2"><p>گرامی دا و عذرش نیز می‌خواه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وگر داری تو وامی باز ده زود</p></div>
<div class="m2"><p>کز این رو شد خدا و خلق خشنود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مده وام ار دهی دیگر مخواهش</p></div>
<div class="m2"><p>به سختی تقاضا جان مکاهش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مگو درد دلت با خلق بسیار</p></div>
<div class="m2"><p>وگر گویند با تو گوش می‌دار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز کس چیزی مخواه و گر بخواهند</p></div>
<div class="m2"><p>بده گر باشدت هرچند کاهند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مگو غیب کسان ور زانکه ایشان</p></div>
<div class="m2"><p>کنندت عیب کمتر شو پریشان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مکن غیبت وگر گویند مشنو</p></div>
<div class="m2"><p>چو می‌دانی تفاوت نیست یک جو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>منه بهتان که آن جرمی عظیمست</p></div>
<div class="m2"><p>مزن بر روی کودک چون یتیم است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یتیمان را پدر باش و برادر</p></div>
<div class="m2"><p>بدان حق پدر با حق مادر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ضعیفان را فرو مگذار در راه</p></div>
<div class="m2"><p>که افتد اینچنین بسیار در راه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مکن با دیگری کاری که آنت</p></div>
<div class="m2"><p>اگر با تو کنند باشد گرانت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مکن بهر طمع با کس نکوئی</p></div>
<div class="m2"><p>که رنجور است مطمع از دو روئی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بدان تعظیمت افشای سلام است</p></div>
<div class="m2"><p>یقین دان شفقتت بذل طعام است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو کردی با کسی احسان نعمت</p></div>
<div class="m2"><p>بر او منت منه زو دار منت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دگر گر با کسی کردی نکوئی</p></div>
<div class="m2"><p>نباشد نیکوئی چون باز گوئی</p></div></div>