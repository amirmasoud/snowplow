---
title: >-
    بخش ۲۷ - در تحقیق عیسی و دجال
---
# بخش ۲۷ - در تحقیق عیسی و دجال

<div class="b" id="bn1"><div class="m1"><p>دو چشمست آدمی را بی ضرورت</p></div>
<div class="m2"><p>برای دیدن معنی و صورت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی چشم چپ است و آن دگر راست</p></div>
<div class="m2"><p>ز هر دو بیند آن مردی که بیناست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو چشمت در حقسقت هست یعنی</p></div>
<div class="m2"><p>یکی در صورت و دیگر به معنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نبیند چشم صورت جز هوا را</p></div>
<div class="m2"><p>به چشم معنوی بیند خدا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدان کاین چشم صورت چشم دنیاست</p></div>
<div class="m2"><p>ولیکن چشم معنی چشم عقبی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی کز چشم معنی هست اعور</p></div>
<div class="m2"><p>نبیند آخرت را ای برادر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که چشم چپ جز از دنیا نبیند</p></div>
<div class="m2"><p>چو دنیا دید هم دنیا گزیند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی کش میل جمله سوی دنیی است</p></div>
<div class="m2"><p>به چشم راست چون دجال اعمی است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مثال تن خر و عیسی است جانت</p></div>
<div class="m2"><p>اگر عیسی صفت باشد روانت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو جان نادان بود دجال باشد</p></div>
<div class="m2"><p>چو دانا گشت عیسی حال باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز نطق عیسوی گیرد نشان جان</p></div>
<div class="m2"><p>شود از علم زنده جان نادان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خر و دجال و عیسی جز یکی نیست</p></div>
<div class="m2"><p>یقین دان اندر این معنی شکی نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر دانا بود عیسی است بر خر</p></div>
<div class="m2"><p>که نادان خر بود وز خر فروتر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو دانستی یقین عیسی و دجال</p></div>
<div class="m2"><p>ز راه علم معلومت شد این حال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ببین تا زین دو یک بر خر کدامست</p></div>
<div class="m2"><p>بدان نامش بخوان اینت تمام است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به چشم راست چون دجال اعمی است</p></div>
<div class="m2"><p>به چشم چپ نظر او را به دنیا است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کسی کش سوی دنیا میل باشد</p></div>
<div class="m2"><p>یقین دجال را در خیل باشد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو دانستی خروج خیل دجال</p></div>
<div class="m2"><p>چو بینی خیل او بگریز در حال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ببُر زین خیل دجالی به مردی</p></div>
<div class="m2"><p>مرو پیشان که زیشان نگردی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نبی گفته مرو آخر زمانه</p></div>
<div class="m2"><p>به دنبال دف و چنگ و چغانه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مبادا کز پیش دجال باشد</p></div>
<div class="m2"><p>که در مانی که او نیکال باشد</p></div></div>