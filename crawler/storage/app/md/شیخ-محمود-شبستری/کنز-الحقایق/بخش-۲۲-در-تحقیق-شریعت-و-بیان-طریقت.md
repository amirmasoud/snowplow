---
title: >-
    بخش ۲۲ - در تحقیق شریعت و بیان طریقت
---
# بخش ۲۲ - در تحقیق شریعت و بیان طریقت

<div class="b" id="bn1"><div class="m1"><p>سوالی چند کردم از حکیمی</p></div>
<div class="m2"><p>سوالی نیک هست از علم نیمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شریعت چیست گفتم گفت بسیار</p></div>
<div class="m2"><p>برای خود به امر حق کنی کار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگفتم چیست مقصود از شریعت</p></div>
<div class="m2"><p>بگفتا آن که دریابی طریقت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدو گفتم طریقت چیست گفتن</p></div>
<div class="m2"><p>بگفتا رو به سوی دوست رفتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگفتم چیست پایان طریقت</p></div>
<div class="m2"><p>بگفتا هست پایانش حقیقت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگفتم از حقیقت چیست حاصل</p></div>
<div class="m2"><p>بگفتا آن که گردد جانت واصل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدو گفتم چه باشد وصل با جان</p></div>
<div class="m2"><p>بگفتا وصل جان را قرب حق دان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگفتم قرب حق از چیست امروز</p></div>
<div class="m2"><p>بگفت از بُعد نفس و آه دلسوز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگفتم بُعد نفس از چه بدانم</p></div>
<div class="m2"><p>بگفتا از تصوف ای چو جانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگفتم چیست تحقیق تصوف</p></div>
<div class="m2"><p>بگفتا ای اخی ترک تکلف</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگفتم ترک آن معنی چه سانست</p></div>
<div class="m2"><p>بگفتا آن که درویشی همان است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگفتم چیست درویشی و درویش</p></div>
<div class="m2"><p>بگفتا آنچه داری آوری پیش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدو گفتم نشانم ده که چونست</p></div>
<div class="m2"><p>بگفتا آن نشان از ما برونست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگرچه مختصر گفتم بیانش</p></div>
<div class="m2"><p>ولیکن معتبر می‌دان عیانش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نشانی بی نشان دارند ایشان</p></div>
<div class="m2"><p>که جز حق در نظر نارند ایشان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خوشا وقت کسی کان حال دارد</p></div>
<div class="m2"><p>ز حال خویش ترک قال دارد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر آن کس را که این معنی نشان است</p></div>
<div class="m2"><p>نمی‌گوید چو من کل اللسان است</p></div></div>