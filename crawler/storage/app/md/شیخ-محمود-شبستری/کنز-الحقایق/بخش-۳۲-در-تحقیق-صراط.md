---
title: >-
    بخش ۳۲ - در تحقیق صراط
---
# بخش ۳۲ - در تحقیق صراط

<div class="b" id="bn1"><div class="m1"><p>صراط اندر حقیقت چیست راهست</p></div>
<div class="m2"><p>چو بگذشتی از آن جان را پناه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صراطت گرچه ره سوی بهشتست</p></div>
<div class="m2"><p>ولی دوزخ به زیرش سخت زشتست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به نادانی بر او نتوان گذشتن</p></div>
<div class="m2"><p>به دانائی توان آسان گذشتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بکن نفست برای حق ضحیه</p></div>
<div class="m2"><p>که باشد بر صراطت آن مطیه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیفتی گر نباشد مرکبت زیر</p></div>
<div class="m2"><p>که هست آن تیزتر از تیر و شمشیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی کاینجا دل از توحید آراست</p></div>
<div class="m2"><p>به عقبی بر صراطش راه شد راست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وگر افتاد در تشبیه و تعطیل</p></div>
<div class="m2"><p>در آن شد سوی دوزخ میل در میل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برو دنیا رباط منزلی دان</p></div>
<div class="m2"><p>به دانائی از آن بگذر چو مردان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو دانستی که دنیا چون رباط است</p></div>
<div class="m2"><p>وجودت اندر او همچون صراط است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر جانت سلامت زو گذر کرد</p></div>
<div class="m2"><p>برستی از عذاب دوزخ ای مرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وگر از وی در افتادی به پستی</p></div>
<div class="m2"><p>ندانم کی رسی آنجا که هستی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ازین صورت اگر بیرون نرفتی</p></div>
<div class="m2"><p>بدان همواره در دوزخ بخفتی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر راهت صراط المستقیم است</p></div>
<div class="m2"><p>روانت در بهشت حق مقیم است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برو جان پدر پندار بگذار</p></div>
<div class="m2"><p>چو نادانان مشو مغرور در پندار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نکوئی می‌کن و ترک تبه گیر</p></div>
<div class="m2"><p>پی مردان و دانایان ره گیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به جان بشنو ز من تحقیق این کار</p></div>
<div class="m2"><p>به اقرار و مکن زین بیش انکار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کسی کز جان و دل با حق مقیم است</p></div>
<div class="m2"><p>حقیقت بر صراط مستقیم است</p></div></div>