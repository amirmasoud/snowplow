---
title: >-
    بخش ۲۴ - در تحقیق بهشت و دوزخ
---
# بخش ۲۴ - در تحقیق بهشت و دوزخ

<div class="b" id="bn1"><div class="m1"><p>اگر تو خوی خوش داری به هر کار</p></div>
<div class="m2"><p>از آن خویت بهشت آید پدیدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وگر خوی بدت اندر رباید</p></div>
<div class="m2"><p>از آن جز دوزخت خیری نیاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو دانستی که خوی خوش بهشت است</p></div>
<div class="m2"><p>همانا خوی بد کفر است و زشت است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهشت و دوزخ زیر زبان دان</p></div>
<div class="m2"><p>بهشتست سود و دوزخ را زیان دان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهشت خود به خود می‌ساز اینجا</p></div>
<div class="m2"><p>که چون رفتی نیائی باز اینجا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا بشنو حدیث نیکمردان</p></div>
<div class="m2"><p>به دانش خوی بد را نیک گردان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهشت صورت ار چه دلپذیر است</p></div>
<div class="m2"><p>به نسبت با حقیقت زمهریر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشو شاد از بهشت و نعمت او</p></div>
<div class="m2"><p>مترس از دوزخ و از نغمت او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو صدیقان ز هر دو گرد آزاد</p></div>
<div class="m2"><p>بجز در بندگی حق مشو شاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو حق را بندگی چون بندگان کن</p></div>
<div class="m2"><p>چو استحقاق آن داری به جان کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بلا را همچو مردان شو خریدار</p></div>
<div class="m2"><p>که قوت اولیا را نیست هموار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بهشت اندر مثل چون مطبخی دان</p></div>
<div class="m2"><p>که باشد اندر او مرغان بریان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بهشت پر طعام از بهر عام است</p></div>
<div class="m2"><p>تو عامی میل تو سوی طعام است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به دنیا خوردن و در آخرت هم</p></div>
<div class="m2"><p>چو بی خوردن نخواهی عمر یک دم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بجز خوردن اگر چیزی نخواهی</p></div>
<div class="m2"><p>مرنج از من اگر گویم تباهی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که حیوانی نه انسانی به مقدار</p></div>
<div class="m2"><p>که میلت نیست جز سوی علفزار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر طاعت برای آن کنی تو</p></div>
<div class="m2"><p>که یابی مرغ خود بریان کنی تو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بهشت خاص بی ذوق و طعام است</p></div>
<div class="m2"><p>که ذوق جان به حق ما لا کلام است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر طاعت تو را بهر نعیم است</p></div>
<div class="m2"><p>نه بهر حق جزای آن جحیم است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر آنکس را که باشد عقل همراه</p></div>
<div class="m2"><p>نجوید مطبخ الا حضرت شاه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بدان خوبی جمال دوست حاضر</p></div>
<div class="m2"><p>به هر حالی که هستی بر تو ناظر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نشان ابلهی چیزی دگر نیست</p></div>
<div class="m2"><p>که مطبخ جوئی و زانت خبر نیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به نزد تو اگر خوردن بهشت است</p></div>
<div class="m2"><p>پس این دنیا بهشت است و چه زشتست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو را با آخرت کاری نباشد</p></div>
<div class="m2"><p>وزان بر جان تو باری نباشد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بهشت و دوزخت خود هست موجود</p></div>
<div class="m2"><p>که بشناسی به معنی گفت محمود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به صورت چون تنت خورد این جهان را</p></div>
<div class="m2"><p>به دست آوری به معنی ذوق جان را</p></div></div>