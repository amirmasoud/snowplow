---
title: >-
    بخش ۳۷ - در حسب حال و ختم کتاب گوید
---
# بخش ۳۷ - در حسب حال و ختم کتاب گوید

<div class="b" id="bn1"><div class="m1"><p>سخن در سینه زین بسیار دارم</p></div>
<div class="m2"><p>نمی‌گویم که عمری کار دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو در کار است با گفتار کردار</p></div>
<div class="m2"><p>پی کردار گَرد و ترک گفت آر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه گفتار است تنها جمله کارم</p></div>
<div class="m2"><p>که از کردار دارم هرچه دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برای غیر این گفتار بوده است</p></div>
<div class="m2"><p>وگرنه کار من کردار بوده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگویم چیست کارم ترک دنیا</p></div>
<div class="m2"><p>بگویم چیست بارم میل عقبی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرفتم ترک دنیا کارم این بود</p></div>
<div class="m2"><p>ببستم بار عقبی بارم این بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز دنیا و ز عقبی گشتم آزاد</p></div>
<div class="m2"><p>سر و کارم کنون با دوست افتاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ازین پس کار من یکتاست با دوست</p></div>
<div class="m2"><p>یکی باید دوئی آنجا نه نیکوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر این معنی سخن را ختم کردم</p></div>
<div class="m2"><p>که ختم است این سخن بر من که مردم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنان مردی که الحق مرد باشد</p></div>
<div class="m2"><p>که از عقبی و دنیا فرد باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو هم گر وارهی همدرد گردی</p></div>
<div class="m2"><p>یقین دانم به معنی مرد گردی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر ار خلق بُرّی با خود آئی</p></div>
<div class="m2"><p>ز خود بًرّی از آن پس با خدائی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برای دوست کم کن دشمنی را</p></div>
<div class="m2"><p>به حق دوستی بفکن منی را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مجو از بهر نفس خود مرادش</p></div>
<div class="m2"><p>که گردد از مراد افزون عنادش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ولی چون کم شود از دل مرادت</p></div>
<div class="m2"><p>کند در نامرادی دوست شادت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مریدی هست میلش سوی دنیا</p></div>
<div class="m2"><p>مریدی هست میلش سوی عقبی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ارادت بعد از این بر سوی حق کن</p></div>
<div class="m2"><p>مریدی گر کنی بر این نسق کن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اگر مردی ز خود گردی تو بیزار</p></div>
<div class="m2"><p>به مردی روی خود سوی خدا آر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چه می‌خواهی بری در معرفت گوی</p></div>
<div class="m2"><p>خدا بین و خدا دان و خدا گوی</p></div></div>