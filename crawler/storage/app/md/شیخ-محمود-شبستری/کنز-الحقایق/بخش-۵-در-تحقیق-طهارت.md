---
title: >-
    بخش ۵ - در تحقیق طهارت
---
# بخش ۵ - در تحقیق طهارت

<div class="b" id="bn1"><div class="m1"><p>ز دنیا و ز دنیادار شو دور</p></div>
<div class="m2"><p>مباش از بهر دنیا بیش رنجور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که دنیا چون رباط است اندر این راه</p></div>
<div class="m2"><p>بباید رفتنت زین جای ناگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز نیک و بد هر آنچت خلق گویند</p></div>
<div class="m2"><p>تو منت دار زانکت جامه شویند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی کت جامهٔ تن پاک شوید</p></div>
<div class="m2"><p>یقین می‌دان که از تو سیم جوید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدین معنی کسی کت جامهٔ جان</p></div>
<div class="m2"><p>بشوید دار ازو منت فراوان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تن تو جامهٔ جان‌ست ای دوست</p></div>
<div class="m2"><p>ولی وقتی که پاکیزه است نیکوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غبار جامهٔ تن شوخ و چرک‌ست</p></div>
<div class="m2"><p>ولیکن شوخ جان از کفر و شرک‌ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لباس تن به آب جوی می‌شوی</p></div>
<div class="m2"><p>طهور جان ز آب علم و دین جوی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز خشم و کینه و از شهوت و آز</p></div>
<div class="m2"><p>درونت پاک کن آنگه وضو ساز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از یراکر نباشد جان نمازی</p></div>
<div class="m2"><p>اگر چه در نمازی بی‌نمازی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حدث دنیاست زیرا هست مردار</p></div>
<div class="m2"><p>به ترک تا حدث از پیش بردار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو دنیا را پیمبر خوانده جیفه</p></div>
<div class="m2"><p>مکش جیفه دگر در بند نیفه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به توبه کوش تا یابی انابت</p></div>
<div class="m2"><p>حقیقت این بود غسل جنابت</p></div></div>