---
title: >-
    بخش ۲۵ - در تحقیق رزق
---
# بخش ۲۵ - در تحقیق رزق

<div class="b" id="bn1"><div class="m1"><p>یقین دان رزق جان از علم و دین است</p></div>
<div class="m2"><p>نباشد جز به سعی انسان یقین است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تن از دنیا و رزق او طعام است</p></div>
<div class="m2"><p>دل از عقبی طعام او کلام است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به رزق تن شدی عمری گرفتار</p></div>
<div class="m2"><p>چو خواهی عمر رزق جان به دست آر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به جان از طالبان علم و دین شو</p></div>
<div class="m2"><p>اگر اینجا نیست سوی شهر چنین شو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان کاین رزق تن از نان و آبست</p></div>
<div class="m2"><p>حقیقت رزق جان از علم کتابست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به حکمت گر بخوانی آن کتابت</p></div>
<div class="m2"><p>یقین آن بس بود روز حسابت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>و ما من دابه فی الارض گفته است</p></div>
<div class="m2"><p>نه بر من هست رزق فرض گفتست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگر باور نمی‌داری ز حق آن</p></div>
<div class="m2"><p>که می‌سوزی به جان از بهر یک نان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مکن از بهر خوردن خلق‌سوزی</p></div>
<div class="m2"><p>که با روز تو خواهد بود روزی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرو چندان به دنبال نواله</p></div>
<div class="m2"><p>طلب آن کن که با تو شد حواله</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو را گفته است رزق جان به دست آر</p></div>
<div class="m2"><p>که رزق تن آید به خروار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو را چون پادشا بر خوان نشاند</p></div>
<div class="m2"><p>گرسنه بر در اسیت نماند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>توکل بر خدا باید همیشه</p></div>
<div class="m2"><p>نه بر تیر و کمان و کار و پیشه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به صنعت هر کسی دارد امیدی</p></div>
<div class="m2"><p>نباشد بر خداشان اعتمیدی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به خلاقیش جمله خلق دانند</p></div>
<div class="m2"><p>ولی رزاقیش را در گمانند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر آن کس را که باشد عقل داند</p></div>
<div class="m2"><p>چو جانت داد بی روزی نماند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو جانت داد بر تن کرد فیروز</p></div>
<div class="m2"><p>معین کرد زرق تن همان روز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به تن می‌خور همیشه آب و نان را</p></div>
<div class="m2"><p>به جان می‌جوی علم خاص جان را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو بشناسی که‌ای و از کجائی</p></div>
<div class="m2"><p>بدانی عاقبت سر خدائی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مرا زان شمه معلوم گشته است</p></div>
<div class="m2"><p>سوی الله این دمم مفهوم گشته است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به هر جائی که هستم در شب و روز</p></div>
<div class="m2"><p>همی گویم به آه گرم و دلسوز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خداوندا دلی بخش این گدا را</p></div>
<div class="m2"><p>که اندر وی نبیند جز خدا را</p></div></div>