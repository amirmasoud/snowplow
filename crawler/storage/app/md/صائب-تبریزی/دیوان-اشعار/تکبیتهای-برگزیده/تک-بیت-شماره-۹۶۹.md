---
title: >-
    تک‌بیت شمارهٔ ۹۶۹
---
# تک‌بیت شمارهٔ ۹۶۹

<div class="b" id="bn1"><div class="m1"><p>سنگ و گوهر، دیده حیران میزان را یکی است</p></div>
<div class="m2"><p>امتیاز کفر و ایمان از من مجنون مپرس</p></div></div>