---
title: >-
    تک‌بیت شمارهٔ ۱۲۰۴
---
# تک‌بیت شمارهٔ ۱۲۰۴

<div class="b" id="bn1"><div class="m1"><p>فریب مهربانی خوردم از گردون، ندانستم</p></div>
<div class="m2"><p>که در دل بشکند خاری که بیرون آرد از پایم</p></div></div>