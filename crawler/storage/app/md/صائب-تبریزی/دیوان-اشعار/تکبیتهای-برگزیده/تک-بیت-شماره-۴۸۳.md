---
title: >-
    تک‌بیت شمارهٔ ۴۸۳
---
# تک‌بیت شمارهٔ ۴۸۳

<div class="b" id="bn1"><div class="m1"><p>کیفیت طاعت مطلب از سر هشیار</p></div>
<div class="m2"><p>مینای تهی بی خبر از ذوق سجودست</p></div></div>