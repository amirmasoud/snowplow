---
title: >-
    تک‌بیت شمارهٔ ۱۲۶۶
---
# تک‌بیت شمارهٔ ۱۲۶۶

<div class="b" id="bn1"><div class="m1"><p>چیده‌ایم از دو جهان دامن الفت چون سرو</p></div>
<div class="m2"><p>هر که از ما گذرد آب روان می‌دانیم</p></div></div>