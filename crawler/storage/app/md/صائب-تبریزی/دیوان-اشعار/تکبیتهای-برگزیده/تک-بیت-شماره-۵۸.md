---
title: >-
    تک‌بیت شمارهٔ ۵۸
---
# تک‌بیت شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>راه خوابیده رسانید به منزل خود را</p></div>
<div class="m2"><p>نرساندی تو گرانجان به در دل خود را</p></div></div>