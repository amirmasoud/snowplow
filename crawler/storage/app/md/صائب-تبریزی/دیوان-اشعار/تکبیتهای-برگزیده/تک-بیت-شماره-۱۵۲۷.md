---
title: >-
    تک‌بیت شمارهٔ ۱۵۲۷
---
# تک‌بیت شمارهٔ ۱۵۲۷

<div class="b" id="bn1"><div class="m1"><p>زمین، سرای مصیبت بود، تو می‌خواهی</p></div>
<div class="m2"><p>که مشت خاکی ازین خاکدان به سر نکنی؟</p></div></div>