---
title: >-
    تک‌بیت شمارهٔ ۱۲۴۱
---
# تک‌بیت شمارهٔ ۱۲۴۱

<div class="b" id="bn1"><div class="m1"><p>هر کسی تخمی به خاک افشاند و ما دیوانگان</p></div>
<div class="m2"><p>دانه زنجیر در دامان صحرا کاشتیم</p></div></div>