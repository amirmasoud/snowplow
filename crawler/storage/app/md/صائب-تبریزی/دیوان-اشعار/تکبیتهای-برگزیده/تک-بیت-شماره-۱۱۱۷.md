---
title: >-
    تک‌بیت شمارهٔ ۱۱۱۷
---
# تک‌بیت شمارهٔ ۱۱۱۷

<div class="b" id="bn1"><div class="m1"><p>اول ز رشک محرمیم سرمه داغ بود</p></div>
<div class="m2"><p>چون خواب، رفته رفته به چشمش گران شدم</p></div></div>