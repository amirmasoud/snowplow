---
title: >-
    تک‌بیت شمارهٔ ۸۱۴
---
# تک‌بیت شمارهٔ ۸۱۴

<div class="b" id="bn1"><div class="m1"><p>دل دیوانهٔ من قابل زنجیر نبود</p></div>
<div class="m2"><p>ورنه کوتاهی ازان زلف گرهگیر نبود</p></div></div>