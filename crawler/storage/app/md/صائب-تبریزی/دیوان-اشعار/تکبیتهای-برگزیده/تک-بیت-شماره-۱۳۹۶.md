---
title: >-
    تک‌بیت شمارهٔ ۱۳۹۶
---
# تک‌بیت شمارهٔ ۱۳۹۶

<div class="b" id="bn1"><div class="m1"><p>پردهٔ عصمت ندارد تاب دست انداز شوق</p></div>
<div class="m2"><p>رو به کنعان کرد از دست زلیخا پیرهن</p></div></div>