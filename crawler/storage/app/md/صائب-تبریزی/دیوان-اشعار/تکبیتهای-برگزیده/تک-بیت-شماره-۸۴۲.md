---
title: >-
    تک‌بیت شمارهٔ ۸۴۲
---
# تک‌بیت شمارهٔ ۸۴۲

<div class="b" id="bn1"><div class="m1"><p>گل بی خار درین غمکده کم سبز شود</p></div>
<div class="m2"><p>دست در گردن هم، شادی و غم سبز شود</p></div></div>