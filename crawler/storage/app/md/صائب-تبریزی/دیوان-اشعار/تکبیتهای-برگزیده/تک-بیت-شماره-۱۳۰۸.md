---
title: >-
    تک‌بیت شمارهٔ ۱۳۰۸
---
# تک‌بیت شمارهٔ ۱۳۰۸

<div class="b" id="bn1"><div class="m1"><p>قسمت خود بین نمی‌گردد زلال زندگی</p></div>
<div class="m2"><p>ای سکندر، سنگ بر آیینه می‌باید زدن</p></div></div>