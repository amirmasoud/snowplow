---
title: >-
    تک‌بیت شمارهٔ ۵۴۸
---
# تک‌بیت شمارهٔ ۵۴۸

<div class="b" id="bn1"><div class="m1"><p>یک دل آسوده نتوان یافت در زیر فلک</p></div>
<div class="m2"><p>در بساط آسیا یک دانهٔ نشکسته نیست</p></div></div>