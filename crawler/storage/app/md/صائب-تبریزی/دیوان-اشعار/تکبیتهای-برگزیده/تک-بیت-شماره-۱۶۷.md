---
title: >-
    تک‌بیت شمارهٔ ۱۶۷
---
# تک‌بیت شمارهٔ ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>در بیابانی که از نقش قدم بیش است چاه</p></div>
<div class="m2"><p>با دو چشم بسته می‌باید سفر کردن مرا</p></div></div>