---
title: >-
    تک‌بیت شمارهٔ ۲۱۳
---
# تک‌بیت شمارهٔ ۲۱۳

<div class="b" id="bn1"><div class="m1"><p>شد ره خوابیده بیدار و همان آسوده‌اند</p></div>
<div class="m2"><p>برده گویا خواب مرگ این همرهان خفته را</p></div></div>