---
title: >-
    تک‌بیت شمارهٔ ۱۳۶۰
---
# تک‌بیت شمارهٔ ۱۳۶۰

<div class="b" id="bn1"><div class="m1"><p>اندیشه از شکست ندارم، که همچو موج</p></div>
<div class="m2"><p>افزوده می‌شود ز شکستن سپاه من</p></div></div>