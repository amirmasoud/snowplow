---
title: >-
    تک‌بیت شمارهٔ ۱۵۱۵
---
# تک‌بیت شمارهٔ ۱۵۱۵

<div class="b" id="bn1"><div class="m1"><p>زینهار از لاله رخساران به دیدن صلح کن</p></div>
<div class="m2"><p>کز نچیدن می‌توان یک عمر گل چید از گلی</p></div></div>