---
title: >-
    تک‌بیت شمارهٔ ۸۳۸
---
# تک‌بیت شمارهٔ ۸۳۸

<div class="b" id="bn1"><div class="m1"><p>عشق فکر دل افگار ز من دارد بیش</p></div>
<div class="m2"><p>دایه پرهیز کند طفل چو بیمار شود</p></div></div>