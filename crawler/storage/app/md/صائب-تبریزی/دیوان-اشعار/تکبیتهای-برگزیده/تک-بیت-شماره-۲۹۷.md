---
title: >-
    تک‌بیت شمارهٔ ۲۹۷
---
# تک‌بیت شمارهٔ ۲۹۷

<div class="b" id="bn1"><div class="m1"><p>نیست صائب ملک تنگ بی‌غمی جای دو شاه</p></div>
<div class="m2"><p>زین سبب طفلان جدل دارند با دیوانه‌ها</p></div></div>