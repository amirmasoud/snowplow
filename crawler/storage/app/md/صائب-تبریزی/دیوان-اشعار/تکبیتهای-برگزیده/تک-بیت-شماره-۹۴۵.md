---
title: >-
    تک‌بیت شمارهٔ ۹۴۵
---
# تک‌بیت شمارهٔ ۹۴۵

<div class="b" id="bn1"><div class="m1"><p>صبح آگاهی شود گفتم مرا موی سفید</p></div>
<div class="m2"><p>چشم بی شرم مرا شد پردهٔ خواب دگر</p></div></div>