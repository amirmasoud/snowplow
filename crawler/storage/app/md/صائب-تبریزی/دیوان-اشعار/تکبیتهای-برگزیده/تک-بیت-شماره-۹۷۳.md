---
title: >-
    تک‌بیت شمارهٔ ۹۷۳
---
# تک‌بیت شمارهٔ ۹۷۳

<div class="b" id="bn1"><div class="m1"><p>در جبههٔ گشادهٔ گلها نگاه کن</p></div>
<div class="m2"><p>دلگیر از گرفتگی باغبان مباش</p></div></div>