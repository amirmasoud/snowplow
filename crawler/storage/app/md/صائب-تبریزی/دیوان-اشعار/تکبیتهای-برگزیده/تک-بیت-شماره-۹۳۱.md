---
title: >-
    تک‌بیت شمارهٔ ۹۳۱
---
# تک‌بیت شمارهٔ ۹۳۱

<div class="b" id="bn1"><div class="m1"><p>عقل پیری ز من ایام جوانی مطلب</p></div>
<div class="m2"><p>که در ایام خزان صاف شود آب بهار</p></div></div>