---
title: >-
    تک‌بیت شمارهٔ ۵۷۸
---
# تک‌بیت شمارهٔ ۵۷۸

<div class="b" id="bn1"><div class="m1"><p>تا نهادم پای در وحشت سرای روزگار</p></div>
<div class="m2"><p>عمر من در فکر آزادی چو زندانی گذشت</p></div></div>