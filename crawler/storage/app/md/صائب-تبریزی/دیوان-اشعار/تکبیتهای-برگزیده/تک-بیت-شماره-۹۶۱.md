---
title: >-
    تک‌بیت شمارهٔ ۹۶۱
---
# تک‌بیت شمارهٔ ۹۶۱

<div class="b" id="bn1"><div class="m1"><p>روزی که آه من به هواداری تو خاست</p></div>
<div class="m2"><p>در خواب ناز بود نسیم سحر هنوز</p></div></div>