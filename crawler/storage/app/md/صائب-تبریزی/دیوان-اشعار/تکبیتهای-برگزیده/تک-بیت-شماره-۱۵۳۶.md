---
title: >-
    تک‌بیت شمارهٔ ۱۵۳۶
---
# تک‌بیت شمارهٔ ۱۵۳۶

<div class="b" id="bn1"><div class="m1"><p>کمند زلف در گردن گذشتی روزی از صحرا</p></div>
<div class="m2"><p>هنوز از دور گردن می‌کشد آهوی صحرایی</p></div></div>