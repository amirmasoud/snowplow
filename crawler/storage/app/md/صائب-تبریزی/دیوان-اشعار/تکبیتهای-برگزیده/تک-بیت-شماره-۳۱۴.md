---
title: >-
    تک‌بیت شمارهٔ ۳۱۴
---
# تک‌بیت شمارهٔ ۳۱۴

<div class="b" id="bn1"><div class="m1"><p>آبرو در پیش ساغر ریختن دون‌همتی است</p></div>
<div class="m2"><p>گردنی کج می‌کنی، باری می از مینا طلب</p></div></div>