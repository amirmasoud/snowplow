---
title: >-
    تک‌بیت شمارهٔ ۲۰۹
---
# تک‌بیت شمارهٔ ۲۰۹

<div class="b" id="bn1"><div class="m1"><p>در دیار عشق، کس را دل نمی‌سوزد به کس</p></div>
<div class="m2"><p>از تب گرم است این‌جا شمع بالین خسته را</p></div></div>