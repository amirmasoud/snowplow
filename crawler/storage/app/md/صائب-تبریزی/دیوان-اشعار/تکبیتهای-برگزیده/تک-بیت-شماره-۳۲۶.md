---
title: >-
    تک‌بیت شمارهٔ ۳۲۶
---
# تک‌بیت شمارهٔ ۳۲۶

<div class="b" id="bn1"><div class="m1"><p>چون کوه، بزرگان جهان آنچه به سائل</p></div>
<div class="m2"><p>بی منت و بی فاصله بخشند، جواب است !</p></div></div>