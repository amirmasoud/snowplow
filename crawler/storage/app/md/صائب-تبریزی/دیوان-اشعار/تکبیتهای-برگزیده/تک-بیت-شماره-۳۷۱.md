---
title: >-
    تک‌بیت شمارهٔ ۳۷۱
---
# تک‌بیت شمارهٔ ۳۷۱

<div class="b" id="bn1"><div class="m1"><p>در ظاهر اگر شهپر پرواز نداریم</p></div>
<div class="m2"><p>افشاندن دست از دو جهان، بال و پر ماست</p></div></div>