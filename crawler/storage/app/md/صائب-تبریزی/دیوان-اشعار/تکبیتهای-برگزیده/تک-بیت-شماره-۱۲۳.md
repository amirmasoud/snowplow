---
title: >-
    تک‌بیت شمارهٔ ۱۲۳
---
# تک‌بیت شمارهٔ ۱۲۳

<div class="b" id="bn1"><div class="m1"><p>می‌کنم در جرعهٔ اول سبکبارش ز غم</p></div>
<div class="m2"><p>چون سبو هر کس که بار دوش می‌سازد مرا</p></div></div>