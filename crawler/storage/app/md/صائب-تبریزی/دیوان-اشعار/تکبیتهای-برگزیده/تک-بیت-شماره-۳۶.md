---
title: >-
    تک‌بیت شمارهٔ ۳۶
---
# تک‌بیت شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>ای گل که موج خنده‌ات از سرگذشته است</p></div>
<div class="m2"><p>آماده باش گریهٔ تلخ گلاب را</p></div></div>