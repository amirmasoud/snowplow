---
title: >-
    تک‌بیت شمارهٔ ۲۱۷
---
# تک‌بیت شمارهٔ ۲۱۷

<div class="b" id="bn1"><div class="m1"><p>ساحلی نیست به جز دامن صحرای عدم</p></div>
<div class="m2"><p>خس و خاشاک به دریای وجود آمده را</p></div></div>