---
title: >-
    تک‌بیت شمارهٔ ۴۲۲
---
# تک‌بیت شمارهٔ ۴۲۲

<div class="b" id="bn1"><div class="m1"><p>تا گذشتی گرم چون خورشید از ویرانه‌ام</p></div>
<div class="m2"><p>از گرستن گل به چشم روزنم افتاده است</p></div></div>