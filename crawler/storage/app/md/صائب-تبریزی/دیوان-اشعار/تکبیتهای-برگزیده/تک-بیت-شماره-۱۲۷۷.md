---
title: >-
    تک‌بیت شمارهٔ ۱۲۷۷
---
# تک‌بیت شمارهٔ ۱۲۷۷

<div class="b" id="bn1"><div class="m1"><p>آن طفل یتیمم که شکسته است سبویم</p></div>
<div class="m2"><p>از آب، همین گریهٔ تلخی است به جویم</p></div></div>