---
title: >-
    تک‌بیت شمارهٔ ۸۳۲
---
# تک‌بیت شمارهٔ ۸۳۲

<div class="b" id="bn1"><div class="m1"><p>هیچ کس عقده‌ای از کار جهان باز نکرد</p></div>
<div class="m2"><p>هر که آمد گرهی چند برین کار افزود</p></div></div>