---
title: >-
    تک‌بیت شمارهٔ ۱۱۶۱
---
# تک‌بیت شمارهٔ ۱۱۶۱

<div class="b" id="bn1"><div class="m1"><p>بیداری دولت به سبکروحی من نیست</p></div>
<div class="m2"><p>هرچند که در چشم تو چون خواب گرانم</p></div></div>