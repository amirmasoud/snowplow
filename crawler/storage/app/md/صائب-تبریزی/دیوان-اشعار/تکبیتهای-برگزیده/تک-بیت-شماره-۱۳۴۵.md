---
title: >-
    تک‌بیت شمارهٔ ۱۳۴۵
---
# تک‌بیت شمارهٔ ۱۳۴۵

<div class="b" id="bn1"><div class="m1"><p>می‌رود فیض صبوح از دست، تا دم می‌زنی</p></div>
<div class="m2"><p>پیش این دریای رحمت، دست را پیمانه کن</p></div></div>