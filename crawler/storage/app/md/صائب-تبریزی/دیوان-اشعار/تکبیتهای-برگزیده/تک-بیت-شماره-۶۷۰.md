---
title: >-
    تک‌بیت شمارهٔ ۶۷۰
---
# تک‌بیت شمارهٔ ۶۷۰

<div class="b" id="bn1"><div class="m1"><p>مرا به حال خود ای عشق بیش ازین مگذار</p></div>
<div class="m2"><p>که بی غمی یکی از اهل روزگارم کرد!</p></div></div>