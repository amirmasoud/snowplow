---
title: >-
    تک‌بیت شمارهٔ ۱۱۰۵
---
# تک‌بیت شمارهٔ ۱۱۰۵

<div class="b" id="bn1"><div class="m1"><p>هنوزم از دهان چون صبح بوی شیر می‌آمد</p></div>
<div class="m2"><p>که چون خورشید، مطلعهای عالمگیر می‌گفتم!</p></div></div>