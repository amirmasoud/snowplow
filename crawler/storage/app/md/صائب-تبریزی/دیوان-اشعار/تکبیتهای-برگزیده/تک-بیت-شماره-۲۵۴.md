---
title: >-
    تک‌بیت شمارهٔ ۲۵۴
---
# تک‌بیت شمارهٔ ۲۵۴

<div class="b" id="bn1"><div class="m1"><p>هر چند از بلای خدا می‌رمند خلق</p></div>
<div class="m2"><p>دل را به آن بلای خدا داده‌ایم ما</p></div></div>