---
title: >-
    تک‌بیت شمارهٔ ۱۳۵۸
---
# تک‌بیت شمارهٔ ۱۳۵۸

<div class="b" id="bn1"><div class="m1"><p>به خاک افتم ز تخت سلطنت چون در خمار افتم</p></div>
<div class="m2"><p>چو آید گردن مینا به کف، مالک رقابم من</p></div></div>