---
title: >-
    غزل شمارهٔ ۸
---
# غزل شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>چیخارتدی خط و منم زلف مبتلاسی هنوز</p></div>
<div class="m2"><p>دوگون ساقالدی و باشیمده دور قراسی هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خطین غباری قویاشی اگر چه یاشوردی</p></div>
<div class="m2"><p>گوزیمی خیره قیلور یوزینین صفاسی هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهار ایدنده نه قانلار که تو کدی گوزلردن</p></div>
<div class="m2"><p>خزان اولاندا نلر ایلسون حناسی هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سو واردیلار قلیجیندان مگر گلستانی؟</p></div>
<div class="m2"><p>که بیر بیرینه قاوشماز گلین یاراسی هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حیات سویینه بیر داغ قویدی رشک لبین</p></div>
<div class="m2"><p>که گیچدی عمر ابد توشمدی قراسی هنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محیط عشق آرا، من اول حباب بی باکم</p></div>
<div class="m2"><p>که گیتدی باشیم و باشیمده دورهواسی هنوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وصال امیدینه عمرین گچیتدی کویینده</p></div>
<div class="m2"><p>یتیشمز ایشلرینه صائبین دعاسی هنوز</p></div></div>