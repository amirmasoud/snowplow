---
title: >-
    غزل شمارهٔ ۱۳
---
# غزل شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>عمر گیچدی، سفر اسبابینی آماده قیلین</p></div>
<div class="m2"><p>هرنه سیزدن کسه تیغ اجل اوندان کسیلین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قلزم عشقدن ای آیری دوشن شبنملر</p></div>
<div class="m2"><p>دوشمه میشکن یولا سیلاب بهاری یغیلین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا سیزی دور زمان ایلمیوپدور پامال</p></div>
<div class="m2"><p>گون ساری شبنم گل تک بوچمندن چکیلین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اولمایان چرخ آرا پرواز رواندن غافل</p></div>
<div class="m2"><p>بوکول ایلن قرالان گوزگولری صاف قیلین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوگ فضاسی نه مقام پرو بال آچماقدور</p></div>
<div class="m2"><p>دانه نارکیمی بیر بیرینزه قیسیلین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر اومارسیز که جوانبخت اولاسیز آخر عمر</p></div>
<div class="m2"><p>قوجالار قدرینی زنهار ایگیدلیکده بیلین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آرتورور گرد معاصینی قورو استغفار</p></div>
<div class="m2"><p>بو زمین گیر توزینی اشک ندامتله سیلین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یاش قیلور خم قوجالار قامتینی طاعت سیز</p></div>
<div class="m2"><p>اگمه میشکن سیزی بو چرخ مقوس اگیلین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دوتا گر غنچه کیمی چوره نزی نشتر خار</p></div>
<div class="m2"><p>ویرمیین گل کیمی الدن قدح می ایچیلین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کیم که ساغر کیمی آغزین آچا سیز میناتک</p></div>
<div class="m2"><p>گوله گوله کرم ایلن اونی معمور قیلین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باده نین جوشی اونون سوز کلامیندن دور</p></div>
<div class="m2"><p>صائبین قدرینی ای اهل خرابات، بیلین؟</p></div></div>