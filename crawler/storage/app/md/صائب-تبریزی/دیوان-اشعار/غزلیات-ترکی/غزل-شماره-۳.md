---
title: >-
    غزل شمارهٔ ۳
---
# غزل شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>الدن چیخارام زلف پریشانینی گورجک</p></div>
<div class="m2"><p>ایشدن گیدرم سرو خرامانینی گورجک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوسیزلارا گر جان آخیدور چشمه حیوان</p></div>
<div class="m2"><p>من جان ویریرم چشمه حیوانینی گورجک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر باغلاماسون ایل گوزینی شرم عذارین</p></div>
<div class="m2"><p>جاندان کسیلور خنجر مژگانینی گورجک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ریحان که نزاکت توکولوردی قلمیندن</p></div>
<div class="m2"><p>خط تیره چکر زلف پریشانینی گورجک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلبل که گلون لعل لبیندن سوزآلیردی</p></div>
<div class="m2"><p>دیلی دولاشور غنچه خندانینی گورجک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رضوان که بهشتین یمیشی گوزینه گلمز</p></div>
<div class="m2"><p>دیشلر الینی سیب زنخدانینی گورجک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تردن خط ریحان ورقین پاک سیلیپدور</p></div>
<div class="m2"><p>نقاش گلستان خط ریحانینی گورجک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گلرنگ اولور صبح اتکی قانلو تریندن</p></div>
<div class="m2"><p>خورشید عذار عرق افشانینی گورجک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مژگانی اولوب قانلو یاشیندان رگ یاقوت</p></div>
<div class="m2"><p>صائب لب لعل گهر افشانینی گورجک</p></div></div>