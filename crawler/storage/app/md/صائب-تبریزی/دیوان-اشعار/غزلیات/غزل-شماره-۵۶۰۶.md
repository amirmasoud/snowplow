---
title: >-
    غزل شمارهٔ ۵۶۰۶
---
# غزل شمارهٔ ۵۶۰۶

<div class="b" id="bn1"><div class="m1"><p>غوطه در بحر گهر ز آبله پا زده‌ام</p></div>
<div class="m2"><p>در دل خاک قدم بر سر دریا زده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سود من از سفر خاک، که چشمش مرساد</p></div>
<div class="m2"><p>مشت خاکی است که در دیده دنیا زده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا در فیض گشوده است به رویم توفیق</p></div>
<div class="m2"><p>حلقه چون داغ بسی بر در دل‌ها زده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خراش جگر خویش نظر داشته‌ام</p></div>
<div class="m2"><p>تیشه در ظاهر اگر بر دل خارا زده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه کند سیل گران‌سنگ به همواری دشت؟</p></div>
<div class="m2"><p>خاک در دیده دشمن به مدارا زده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غوطه در خون زده چون پنجه مرجان دستم</p></div>
<div class="m2"><p>بس که کف بر سر شوریده چو دریا زده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دست چون در کمر موج تهیدست زنم؟</p></div>
<div class="m2"><p>من که چون رشته مکرر به گهر پا زده‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این زمان در سفر قطره به جان می‌لرزم</p></div>
<div class="m2"><p>من که صد مرتبه چون سیل به دریا زده‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست بیکار درین مرحله یک نشتر خار</p></div>
<div class="m2"><p>همه را بر محک دیده بینا زده‌ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عاجزم در گره خویش گشودن صائب</p></div>
<div class="m2"><p>من که نقب از مژه در سینه خارا زده‌ام</p></div></div>