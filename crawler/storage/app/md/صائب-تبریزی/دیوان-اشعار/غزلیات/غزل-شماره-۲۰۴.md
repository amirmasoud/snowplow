---
title: >-
    غزل شمارهٔ ۲۰۴
---
# غزل شمارهٔ ۲۰۴

<div class="b" id="bn1"><div class="m1"><p>پوست زندان است بر تن زاهد افسرده را</p></div>
<div class="m2"><p>حاجت زندان دیگر نیست خون مرده را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر جراحت بخیه نتواند ره خوناب بست</p></div>
<div class="m2"><p>سود ندهد مهر خاموشی دل آزرده را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خضر در سرچشمه تیغش نمازی می کند</p></div>
<div class="m2"><p>عمر اگر باشد، دهان آب حیوان خورده را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقد جان را چون شرر بر آتشین رویی فشان</p></div>
<div class="m2"><p>در گره تا کی توان چون غنچه بست این خرده را؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آب را استادگی آیینه روشن کند</p></div>
<div class="m2"><p>صاف می سازد تحمل، طبع بر هم خورده را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می کند باد مخالف شور دریا را زیاد</p></div>
<div class="m2"><p>کی نصیحت می دهد تسکین، دل آزرده را؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر چه رفت از کف، به دست آوردن او مشکل است</p></div>
<div class="m2"><p>چون کند گردآوری گل، بوی غارت برده را؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این جواب آن که وقتی حالتی فرموده است</p></div>
<div class="m2"><p>از نصیحت می دهم تسکین، دل آزرده را</p></div></div>