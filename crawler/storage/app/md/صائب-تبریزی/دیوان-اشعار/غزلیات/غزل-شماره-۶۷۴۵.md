---
title: >-
    غزل شمارهٔ ۶۷۴۵
---
# غزل شمارهٔ ۶۷۴۵

<div class="b" id="bn1"><div class="m1"><p>به ظاهر نیست عشق را اگر بر دست و پا بندی</p></div>
<div class="m2"><p>به هر مو دارد از پاس وفاداری جدا بندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان دلبستگی دارم به اسباب گرفتاری</p></div>
<div class="m2"><p>که من آزاد گردم هر که بگشاید ز پابندی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در جنت به رویش بی تکلف واکند رضوان</p></div>
<div class="m2"><p>گشاید هر که آن گل پیرهن را از قبا بندی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مدان آزادگان را غافل از حال گرفتاران</p></div>
<div class="m2"><p>که از هر طوق قمری سرو را باشد جدابندی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دورانداز از رطل گرانسنگی مرا ساقی</p></div>
<div class="m2"><p>که بی آبی بود بر دست و پای آسیابندی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز شیرینی شدم قانع به شکرخواب درویشی</p></div>
<div class="m2"><p>به ظاهر گر نبستم بر شکر چون بوریا بندی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مده از کف عنان جور بی باکانه ای ظالم</p></div>
<div class="m2"><p>که مظلومان نمی دارند بر دست دعا بندی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه سازد مهر خاموشی به سوز سینه عاشق؟</p></div>
<div class="m2"><p>نگیرد پیش این سیلاب بی زنهار را بندی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز کار عشق هیهات است آرد عقل سر بیرون</p></div>
<div class="m2"><p>که هر موجی ازین دریا بود بر ناخدا بندی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همان از ناله صائب می کنم آزاد دلها را</p></div>
<div class="m2"><p>به هر بندم گذارد عشق اگر چون نی جدا بندی</p></div></div>