---
title: >-
    غزل شمارهٔ ۱۵۵۰
---
# غزل شمارهٔ ۱۵۵۰

<div class="b" id="bn1"><div class="m1"><p>از دل خم می گلرنگ به جام آمده است</p></div>
<div class="m2"><p>آفتاب عجبی بر لب بام آمده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باده در سلسله تاک ندارد آرام</p></div>
<div class="m2"><p>لب میگون تو تا بر لب جام آمده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرو چون سبزه خوابیده زمین گیر شده است</p></div>
<div class="m2"><p>قد رعنای که دیگر به خرام آمده است؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اشک حسرت شده در ساغر خضر آب حیات</p></div>
<div class="m2"><p>تا دگر تیغ که بیرون ز نیام آمده است؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هاله از غیرت من حلقه ماتم شده است</p></div>
<div class="m2"><p>تا به دلجوییم آن ماه تمام آمده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سیاهی چه خیال است برآید داغش</p></div>
<div class="m2"><p>هر عقیقی که گرفتار به نام آمده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای بسا خام که بسیار به از پخته بود</p></div>
<div class="m2"><p>عیب عنبر نتوان کرد که خام آمده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می کند جوش گل و ناله بلبل فریاد</p></div>
<div class="m2"><p>که ز می توبه درین فصل حرام آمده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیری از حرص مدارید توقع زنهار</p></div>
<div class="m2"><p>که تهی چشم تر از حلقه دام آمده است</p></div></div>