---
title: >-
    غزل شمارهٔ ۱۹۰
---
# غزل شمارهٔ ۱۹۰

<div class="b" id="bn1"><div class="m1"><p>جامه آزادگی چالاک باشد سرو را</p></div>
<div class="m2"><p>جیب و دامن فارغ از خاشاک باشد سرو را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخت زنگاری بهار بی خزان دیگرست</p></div>
<div class="m2"><p>دل چو از زنگ کدورت پاک باشد سرو را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی بری دارالامان مردم آزاده است</p></div>
<div class="m2"><p>کی دل از بی حاصلی غمناک باشد سرو را؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می توان بر سرکشان غالب شد از آزادگی</p></div>
<div class="m2"><p>آب با آن منزلت در خاک باشد سرو را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرد مهری نوبهار مردم آزاده است</p></div>
<div class="m2"><p>در خزان سرسبزی افلاک باشد سرو را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از رعونت صاحب معراج می گردد جمال</p></div>
<div class="m2"><p>همچو گل چندین گریبان چاک باشد سرو را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همت از خاکی نهادان جو که با آن سرکشی</p></div>
<div class="m2"><p>قوت نشو و نما از خاک باشد سرو را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از علایق خط آزادی ندارد هیچ کس</p></div>
<div class="m2"><p>دام ها از ریشه زیر خاک باشد سرو را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بست طوق بندگی راه نفس بر قمریان</p></div>
<div class="m2"><p>دست تا کی در بغل ز امساک باشد سرو را؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دار و گیر حسن از عشق است در هر جا که هست</p></div>
<div class="m2"><p>طوق قمری حلقه فتراک باشد سرو را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زخم شمشیر حوادث موج آب زندگی است</p></div>
<div class="m2"><p>تازه رویی از دل صد چاک باشد سرو را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باد با آن سرکشی، یک عاشق سر در هوا</p></div>
<div class="m2"><p>آب یک دیوانه بی باک باشد سرو را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دامن برچیده صائب دور باش آفت است</p></div>
<div class="m2"><p>از خس و خاشاک، دامن پاک باشد سرو را</p></div></div>