---
title: >-
    غزل شمارهٔ ۵۶۳۴
---
# غزل شمارهٔ ۵۶۳۴

<div class="b" id="bn1"><div class="m1"><p>چند خود را زخیال تو به خواب اندازم ؟</p></div>
<div class="m2"><p>چند از تشنه لبی سنگ در آب اندازم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در نهانخانه محوست عبادتگاهم</p></div>
<div class="m2"><p>نیستم موج که سجاده بر آب اندازم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لاله ای نیست صباحت که مرا گرم کند</p></div>
<div class="m2"><p>چه بر این آتش افسرده کباب اندازم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند در پرده توان مشق نظر بازی کرد؟</p></div>
<div class="m2"><p>طرح نظاره به آن روی نقاب اندازم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من که بر چشم خود از نور شرر می لرزم</p></div>
<div class="m2"><p>به چه جرأت ز جمال تو نقاب اندازم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منت آب خضر سوخت مرا، نزدیک است</p></div>
<div class="m2"><p>که نفس سوخته خود را به سراب اندازم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تلخیی نیست که بر خود نتوان شیرین کرد</p></div>
<div class="m2"><p>به که مهر لب او را به شراب اندازم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چند در شعر کنم عمر گرامی را صرف؟</p></div>
<div class="m2"><p>چند ازین گوهر نایاب در آب اندازم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به که کوتاه کنم زلف سخن را صائب</p></div>
<div class="m2"><p>رگ جان را چه ضرورست به تاب اندازم؟</p></div></div>