---
title: >-
    غزل شمارهٔ ۶۵۰۷
---
# غزل شمارهٔ ۶۵۰۷

<div class="b" id="bn1"><div class="m1"><p>کشت بی خوشه خجالت کشد از روی درو</p></div>
<div class="m2"><p>مفکن ای تیغ اجل بر من بیدل پرتو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گردش چرخ بدو نیک ز هم نشناسد</p></div>
<div class="m2"><p>آسیا تفرقه از هم نکند گندم و جو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با لب خشک سکندر ز سیاهی برگشت</p></div>
<div class="m2"><p>یک دم آب به قسمت نفزاید تک و دو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در شکستن حذر از شیشه فزون باید کرد</p></div>
<div class="m2"><p>لشکری را که شکسته است به دنبال مرو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق از حال پریشان شدگان غافل نیست</p></div>
<div class="m2"><p>همه جا دیده خورشید بود با پرتو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برق از تندی خود زود فنا می گردد</p></div>
<div class="m2"><p>نیست ممکن که نبازد سر خود تیز جلو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سبحه از دست بینداز که بر دل بارست</p></div>
<div class="m2"><p>میفروش آنچه ز مستان نستاند به گرو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه بود دولت دنیا که به آن فخر کنند؟</p></div>
<div class="m2"><p>گشت در غار ازین شرم نهان کیخسرو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تازه عاشق نتواند که نگرید صائب</p></div>
<div class="m2"><p>بیشتر آب تراوش کند از کوزه نو</p></div></div>