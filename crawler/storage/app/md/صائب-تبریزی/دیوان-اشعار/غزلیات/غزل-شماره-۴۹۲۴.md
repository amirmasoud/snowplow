---
title: >-
    غزل شمارهٔ ۴۹۲۴
---
# غزل شمارهٔ ۴۹۲۴

<div class="b" id="bn1"><div class="m1"><p>ز سوز سینه پروانه من آب شد آتش</p></div>
<div class="m2"><p>زسیر و دور من سرگشته چون گرداب شد آتش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به داغ ازروی آتشناک او خوش می کنم دل را</p></div>
<div class="m2"><p>شرر تسبیح باشد هرکه را محراب شد آتش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بس خون گریه کرد از رشک روی آتشین او</p></div>
<div class="m2"><p>چو رنگ لعل پنهان در دل خوناب شد آتش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز برق می کف خاکستری شد زهد خشک من</p></div>
<div class="m2"><p>کتان بیجگر را پرتو مهتاب شد آتش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چه بود از روشن روانی شمع محفلها</p></div>
<div class="m2"><p>ز آب دیده من گوهر نایاب شد آتش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من عاجز عنان عمر مستعجل چسان گیرم</p></div>
<div class="m2"><p>که از بس گرمی رفتار،این سیلاب شد آتش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز فیض کیمیای عشق آتش آب می گردد</p></div>
<div class="m2"><p>گوارا بر سمندر چون شراب ناب شدآتش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به همواری توان مغلوب کردن خصم سرکش را</p></div>
<div class="m2"><p>که با چندین زبان، خامش به مشتی آب شد آتش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر از اسباب افزود آب روی دیگران صائب</p></div>
<div class="m2"><p>دل آزاده را جمعیت اسباب شد آتش</p></div></div>