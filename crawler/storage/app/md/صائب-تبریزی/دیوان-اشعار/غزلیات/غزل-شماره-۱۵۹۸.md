---
title: >-
    غزل شمارهٔ ۱۵۹۸
---
# غزل شمارهٔ ۱۵۹۸

<div class="b" id="bn1"><div class="m1"><p>در قناعت لب خشک و مژه پر نم نیست</p></div>
<div class="m2"><p>عالمی هست درین گوشه که در عالم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دل هر که رضا رنگ اقامت ریزد</p></div>
<div class="m2"><p>چشم شوخ و سخن تلخ، کم از زمزم نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از جهان شادی بی غم چه توقع دارید؟</p></div>
<div class="m2"><p>لوح پیشانی گل بی گره شبنم نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که سوهان حوادث نکند هموارش</p></div>
<div class="m2"><p>می توان گفت که از سلسله آدم نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باخبر باش دلی از خم زلفت نبرد</p></div>
<div class="m2"><p>در گوش تو یتیمی است که در عالم نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچ کس روی دل از حلقه آن زلف ندید</p></div>
<div class="m2"><p>نقش امید همانا که درین خاتم نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همت آن است کز آوازه احسان گذرند</p></div>
<div class="m2"><p>هر که این بادیه را طی نکند حاتم نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لب فرو بستن غواص گهر می گوید</p></div>
<div class="m2"><p>که درین قلزم خونخوار، نفس محرم نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نفس سوخته لاله خطی آورده است</p></div>
<div class="m2"><p>از دل خاک، که آرام در آنجا هم نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همچو صائب به سیه روزی خود ساخته ایم</p></div>
<div class="m2"><p>داغ ما را نظر مرحمت از مرهم نیست</p></div></div>