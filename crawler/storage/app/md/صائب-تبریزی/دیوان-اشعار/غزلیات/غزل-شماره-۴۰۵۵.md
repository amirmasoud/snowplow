---
title: >-
    غزل شمارهٔ ۴۰۵۵
---
# غزل شمارهٔ ۴۰۵۵

<div class="b" id="bn1"><div class="m1"><p>از چشم ودل کی آن گل سیراب بگذرد</p></div>
<div class="m2"><p>خودبین کجا ز آینه وآب بگذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سینه های صاف نگیرد قرار دل</p></div>
<div class="m2"><p>زود از بساط آینه سیماب بگذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون آب شور کام جهان تشنگی فزاست</p></div>
<div class="m2"><p>سیراب تشنه ای که ازین آب بگذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در جوی شیر کاسه به خون جگر زند</p></div>
<div class="m2"><p>از می کسی که شب مهتاب بگذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ظلم است زندگانی روشندلان چو شمع</p></div>
<div class="m2"><p>جایی بغیر گوشه محراب بگذرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر قرب دل مبند که با ربط آفتاب</p></div>
<div class="m2"><p>در کان مدار لعل به خوناب بگذرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیری به صد شتاب جوانی ز من گذشت</p></div>
<div class="m2"><p>پل را ندیده ام که ز سیلاب بگذرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گیرنده است پنجه خونهای بیگناه</p></div>
<div class="m2"><p>چون ناوک تو از دل بیتاب بگذرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو موسم شباب دم صبح شیب را</p></div>
<div class="m2"><p>صائب روا مدار که در خواب بگذرد</p></div></div>