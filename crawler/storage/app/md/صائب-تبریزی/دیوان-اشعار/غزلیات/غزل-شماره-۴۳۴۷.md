---
title: >-
    غزل شمارهٔ ۴۳۴۷
---
# غزل شمارهٔ ۴۳۴۷

<div class="b" id="bn1"><div class="m1"><p>جویای تو با کعبه گل کار ندارد</p></div>
<div class="m2"><p>آیینه ما روی به دیوار ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حلقه این زهر فروشان نتوان یافت</p></div>
<div class="m2"><p>یک سبحه که شیرازه زنار ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر لحظه به رنگ دگر از پرده برآیی</p></div>
<div class="m2"><p>دل بردن ما این همه در کار ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دور سفر سنگ فلاخن به سر آمد</p></div>
<div class="m2"><p>سرگشتگی ماست که پرگار ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک داغ جگر سوز درین لاله ستان نیست</p></div>
<div class="m2"><p>این میکده یک ساغر سرشار ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دیدن رویت دل آیینه فرو ریخت</p></div>
<div class="m2"><p>هر شیشه دلی طاقت دیدار ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در هر شکن زلف گرهگیر تو دامی است</p></div>
<div class="m2"><p>این سلسله یک حلقه بیکار ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از گرد کسادی گهرم مهره گل شد</p></div>
<div class="m2"><p>رحم است به جنسی که خریدار ندارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما گوشه نشینان چمن آرای خیالیم</p></div>
<div class="m2"><p>در خلوت ما نکهت گل بار ندارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بلبل ز نظر بازی شبنم گله مند است</p></div>
<div class="m2"><p>مسکین خبر از رخنه دیوار ندارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در ملک رضا زخم زبان سایه بیدست</p></div>
<div class="m2"><p>سرتاسر این بادیه یک خار ندارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پیش ره آتش ننهد چوب خس وخار</p></div>
<div class="m2"><p>صائب حذر از کثرت اغیار ندارد</p></div></div>