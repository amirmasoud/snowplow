---
title: >-
    غزل شمارهٔ ۴۱۵۸
---
# غزل شمارهٔ ۴۱۵۸

<div class="b" id="bn1"><div class="m1"><p>آزادگان کجا غم دستار می خورند</p></div>
<div class="m2"><p>این پر دلان قسم به سر دارمی خورند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حیرانیان عشق چو شبنم در این چمن</p></div>
<div class="m2"><p>روزی ز راه دیده بیدار می خورند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنان که ره به نقطه توحیدبرده اند</p></div>
<div class="m2"><p>از دل همیشه دانه چو پرگار می خورند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نشاه شراب صبوحی چه غافلند</p></div>
<div class="m2"><p>جمعی که باده را به شب تار می خورند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر لوح دل چو مصرع رنگین کنند نقش</p></div>
<div class="m2"><p>زخمی که رهروان تو از خار می خورند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقل شراب سنگ ملامتگران کنند</p></div>
<div class="m2"><p>رندان که باده بر سر بازار می خورند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طوطی به زهر غوطه زد از حرف شکرین</p></div>
<div class="m2"><p>مردم همین فریب ز گفتار می خورند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با آسمان بساز که آیینه خاطران</p></div>
<div class="m2"><p>پیمانه های زهر ز زنگار می خورند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مگذر ز خون من که طبیبان مهربان</p></div>
<div class="m2"><p>گاهی ز لطف شربت بیمار می خورند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از شکر می کنند لب خویش شکرین</p></div>
<div class="m2"><p>گر جام زهر مردم هشیار می خورند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب هزار بار به از آب زندگی است</p></div>
<div class="m2"><p>خونی که عاشقان به شب تار می خورند</p></div></div>