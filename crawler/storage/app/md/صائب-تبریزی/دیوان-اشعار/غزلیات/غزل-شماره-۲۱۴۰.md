---
title: >-
    غزل شمارهٔ ۲۱۴۰
---
# غزل شمارهٔ ۲۱۴۰

<div class="b" id="bn1"><div class="m1"><p>در وصل، دل از هجر فزون دل نگران است</p></div>
<div class="m2"><p>آوارگی تیر در آغوش کمان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیهوده پس سبحه و زنار دویدیم</p></div>
<div class="m2"><p>شیرازه اوراق دل آن موی میان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این با که توان گفت، که با آن همه نعمت</p></div>
<div class="m2"><p>دل خوردن ما بر فلک سفله گران است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر باد به فرمان سلیمان زمان بود</p></div>
<div class="m2"><p>مجنون ترا ریگ روان تخت روان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روشن گهران از هنر خویش نگویند</p></div>
<div class="m2"><p>اینجاست که جوهر علف تیغ زبان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مردان حق از دایره چرخ برونند</p></div>
<div class="m2"><p>از چرخ شکایت روش پیرزنان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیرون شد ازین دایره بی زخم محال است</p></div>
<div class="m2"><p>ما سست عنانیم و قضا سخت کمان است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ذرات جهان ریزه خور خوان سپهرند</p></div>
<div class="m2"><p>هر چند که بر سفره او یک ته نان است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب دم گرمی که برآرد ز جهان دود</p></div>
<div class="m2"><p>در حلقه ما سوختگان باد خزان است</p></div></div>