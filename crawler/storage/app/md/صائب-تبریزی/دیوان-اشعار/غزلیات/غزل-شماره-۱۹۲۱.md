---
title: >-
    غزل شمارهٔ ۱۹۲۱
---
# غزل شمارهٔ ۱۹۲۱

<div class="b" id="bn1"><div class="m1"><p>آیینه را توجه خاطر به گلخن است</p></div>
<div class="m2"><p>هر جا صفای قلب دهد روی، گلشن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دور ما که سنگ به سایل نمی دهند</p></div>
<div class="m2"><p>دست و دل گشاده نصیب فلاخن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی جبهه گشاده، سخن رو نمی دهد</p></div>
<div class="m2"><p>این ماجرا ز طوطی و آیینه روشن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیچیده است خنده و شیون به یکدگر</p></div>
<div class="m2"><p>این نکته از صدای شکفتن مبرهن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همت به بی نیازی من ناز می کند</p></div>
<div class="m2"><p>یک سرو در سراسر این سبز گلشن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با سرگذشتگان چه کند موج حادثات؟</p></div>
<div class="m2"><p>شمع خموش را چه غم از باد دامن است؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیچیده است اگر چه چو جوهر زبان ما</p></div>
<div class="m2"><p>احوال ما به تیغ تو چون آب روشن است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نتوان به روی دختر رز چشم غیر دید</p></div>
<div class="m2"><p>در خانه ای شراب ننوشم که روزن است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب کسی که عشق بود اوستاد او</p></div>
<div class="m2"><p>در هر فنی که نام توان برد، یک فن است</p></div></div>