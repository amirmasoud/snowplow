---
title: >-
    غزل شمارهٔ ۱۲۱۴
---
# غزل شمارهٔ ۱۲۱۴

<div class="b" id="bn1"><div class="m1"><p>بهر قتل ما کمر آن حسن بی اندازه بست</p></div>
<div class="m2"><p>دفتر گل را خس و خاشاک ما شیرازه بست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی دماغان جنون را رام کردن مشکل است</p></div>
<div class="m2"><p>سوخت لیلی، محمل خود تا بر این جمازه بست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوخت چون خال از فروغ عارض گلگون او</p></div>
<div class="m2"><p>از شفق آن کس که بر خورشید تابان غازه بست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آب شد از انفعال پیچ و تاب زلف او</p></div>
<div class="m2"><p>موج بر آب روان چندان که نقش تازه بست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جمع نتوانست کردن این دل صد پاره را</p></div>
<div class="m2"><p>آن که اوراق خزان را بارها شیرازه بست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه همین صائب بلند آوازه گشت از حرف عشق</p></div>
<div class="m2"><p>صاحب گلبانگ شد هر کس که این آوازه بست</p></div></div>