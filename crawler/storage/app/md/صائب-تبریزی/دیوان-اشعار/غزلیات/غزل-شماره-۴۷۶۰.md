---
title: >-
    غزل شمارهٔ ۴۷۶۰
---
# غزل شمارهٔ ۴۷۶۰

<div class="b" id="bn1"><div class="m1"><p>شد جدا از زخم من آن خنجر سیراب سبز</p></div>
<div class="m2"><p>چون بماند ازروانی، زود گردد آب سبز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آب بی یاران مخور کز خجلت تنها خوری</p></div>
<div class="m2"><p>خضر نتواند شدن درحلقه احباب سبز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوشوار از شرم آن صبح بنا گوش آب شد</p></div>
<div class="m2"><p>شمع نتواند شد از خجلت درین مهتاب سبز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست امید رهایی زین سپهر آبگون</p></div>
<div class="m2"><p>کشتی ما می شود آخر درین گرداب سبز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شست از دل آرزوی عمر جاویدان مرا</p></div>
<div class="m2"><p>تاشد از استادگی درجوی خضر این آب سبز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر قدر کز دیده افشاندم سرشک لاله گون</p></div>
<div class="m2"><p>تخم مهر من نشد در سینه احباب سبز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیش او طاعت ندارد آبرویی، ورنه شد</p></div>
<div class="m2"><p>از سرشکم دانه تسبیح در محراب سبز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ازدم سرد خزان صائب نگردد زرد رو</p></div>
<div class="m2"><p>بخت هر کس شد چو مینا از شراب ناب سبز</p></div></div>