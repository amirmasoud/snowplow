---
title: >-
    غزل شمارهٔ ۴۳۴۱
---
# غزل شمارهٔ ۴۳۴۱

<div class="b" id="bn1"><div class="m1"><p>سیری ز تپیدن دل بیتاب ندارد</p></div>
<div class="m2"><p>آسودگی این قطره سیماب ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر صاف ضمیران سخن سخت گران نیست</p></div>
<div class="m2"><p>پروای شکست آینه آب ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبنم چه طراوت دهد این لاله ستان را</p></div>
<div class="m2"><p>سیری جگر سوخته از آب ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیدار نگردد دل غافل به نصیحت</p></div>
<div class="m2"><p>ازخار حذرپای گرانخواب ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با جبهه وا کرده چه سازد غم عالم</p></div>
<div class="m2"><p>ساغر خطر از زور می ناب ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خال نگردید فروغ رخ او کم</p></div>
<div class="m2"><p>از داغ حذر لاله سیراب ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون موج رود آن که درین بحر سراسر</p></div>
<div class="m2"><p>مسکین خبراز عقده گرداب ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ماهی چو زند برلب خود مهرخموشی</p></div>
<div class="m2"><p>اندیشه ز گیرایی قلاب ندارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از نقش ونگارست دل حق طلبان پاک</p></div>
<div class="m2"><p>دیوار حرم صورت محراب ندارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همصحبتی ساده دلان صیقل روح است</p></div>
<div class="m2"><p>بر در زن ازان خانه که مهتاب ندارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گشته است زبس محو مسبب نظر او</p></div>
<div class="m2"><p>صائب خبر از عالم اسباب ندارد</p></div></div>