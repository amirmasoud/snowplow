---
title: >-
    غزل شمارهٔ ۳۸۲۰
---
# غزل شمارهٔ ۳۸۲۰

<div class="b" id="bn1"><div class="m1"><p>همیشه از دل من آه سرد می خیزد</p></div>
<div class="m2"><p>ازین خرابه شب و روز گرد می خیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلیر بر صف افتادگان عشق متاز</p></div>
<div class="m2"><p>که جای گرد ازین خاک مرد می خیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ستاره سوختگان فیض صبح دریابند</p></div>
<div class="m2"><p>ز سینه ای که ازو آه سرد می خیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل رمیده من می دود ز سینه برون</p></div>
<div class="m2"><p>ز ملک هستی هرکس که گرد می خیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز تخم سوخته نشو و نما نمی آید</p></div>
<div class="m2"><p>کجا ز سینه من داغ و درد می خیزد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زمین بادیه عشق شورشی دارد</p></div>
<div class="m2"><p>که هرکه خیزد ازو هرزه گرد می خیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آن حریم که باشد زبان شمع خموش</p></div>
<div class="m2"><p>ز مصحف پر پروانه گرد می خیزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سماع اهل دل از روی شادمانی نیست</p></div>
<div class="m2"><p>سپند از سر آتش ز درد می خیزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به روی خاک کشد تیغ خود چو سایه بید</p></div>
<div class="m2"><p>به من کسی که به قصد نبرد می خیزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کجا مقید همراه می شود صائب؟</p></div>
<div class="m2"><p>سبکروی که چو خورشید فرد می خیزد</p></div></div>