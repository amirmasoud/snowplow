---
title: >-
    غزل شمارهٔ ۵۷۱۱
---
# غزل شمارهٔ ۵۷۱۱

<div class="b" id="bn1"><div class="m1"><p>جمال یوسف ازین تیره خاکدان دیدم</p></div>
<div class="m2"><p>عبیر پیرهن از گرد کاروان دیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ربود خواب ترا در کنارم از مستی</p></div>
<div class="m2"><p>ترا چنان که دلم خواست آنچنان دیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز این که آب شدم دست شستم از هستی</p></div>
<div class="m2"><p>دگر چه بهره چو شبنم زگلستان دیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز شست صاف نشد تیر راست را روزی</p></div>
<div class="m2"><p>گشایشی که من از خانه چون کمان دیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل گرفته من چون ز باغ بگشاید</p></div>
<div class="m2"><p>که در گشودن در روی باغبان دیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برابرست به عیش تمام روی زمین</p></div>
<div class="m2"><p>که روی خویش برآن خاک آستان دیدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از آن گذشت به خمیازه عمر من چو کمان</p></div>
<div class="m2"><p>که من ز دور گردی از نشان دیدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میانه وطن وغربت است بادیه ها</p></div>
<div class="m2"><p>منم که داغ غریبی در آشیان دیدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو گردباد نفس می کشم غبارآلود</p></div>
<div class="m2"><p>ز بس که کلفت ازین تیره خاکدان دیدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جواب آن غزل حاذق است این صائب</p></div>
<div class="m2"><p>بهار دیدم و گل دیدم و خزان دیدم</p></div></div>