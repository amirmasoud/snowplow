---
title: >-
    غزل شمارهٔ ۵۰۰۸
---
# غزل شمارهٔ ۵۰۰۸

<div class="b" id="bn1"><div class="m1"><p>که می رهد زخم طره گرهگیرش؟</p></div>
<div class="m2"><p>که چشم بررخ یوسف گشوده زنجیرش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار زخم نمایان ز غمزه ای دارم</p></div>
<div class="m2"><p>که برنیامده ازخانه کمان تیرش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هنوز شیوه چین جبین نمی داند</p></div>
<div class="m2"><p>هنوز نو خط جوهر نگشته شمشیرش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خوان حسن تو یعقوب اگر شود مهمان</p></div>
<div class="m2"><p>کنی ز نعمت دیدار،چشم و دل سیرش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گرد تربت مجنون که می تواند گشت؟</p></div>
<div class="m2"><p>هنوز شمع مزارست دیده شیرش</p></div></div>