---
title: >-
    غزل شمارهٔ ۴۱۹۸
---
# غزل شمارهٔ ۴۱۹۸

<div class="b" id="bn1"><div class="m1"><p>از کف عنان گذاشته منزل چه می کند</p></div>
<div class="m2"><p>موج رمیده دامن ساحل چه می کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست ز کار رفته چه محتاج دامن است</p></div>
<div class="m2"><p>شمع گدازیافته محفل چه می کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از پیچ وتاب دل خبری نیست جسم را</p></div>
<div class="m2"><p>با پای خفته دوری منزل چه می کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک دل هواس جمع مرا تار ومار کرد</p></div>
<div class="m2"><p>زلف شکسته تو به صد دل چه می کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مجنون نمی کند گله از سنگ کودکان</p></div>
<div class="m2"><p>داند اگر شعور به عاقل چه می کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنجاکه هست بیخبری می چه حاجت است</p></div>
<div class="m2"><p>پای به خواب رفته سلاسل چه می کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرجلوه ای ازو رقم قتل عالمی است</p></div>
<div class="m2"><p>آن مست ناز تیغ حمایل چه می کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای بحر از حباب نظر باز کن ببین</p></div>
<div class="m2"><p>کاین موج بیقرار به ساحل چه می کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواهی نفس گداخته آمدبه خانه ام</p></div>
<div class="m2"><p>گر بشنوی فراق تو با دل چه می کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لب تلخ از سوال نکردی چه غافلی</p></div>
<div class="m2"><p>کاین زهر جانگداز به سایل چه می کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب ز اشک تلخ دلم لاله زار شد</p></div>
<div class="m2"><p>با خاک نرم دانه قابل چه می کند</p></div></div>