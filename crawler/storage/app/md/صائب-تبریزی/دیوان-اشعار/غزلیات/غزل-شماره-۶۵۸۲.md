---
title: >-
    غزل شمارهٔ ۶۵۸۲
---
# غزل شمارهٔ ۶۵۸۲

<div class="b" id="bn1"><div class="m1"><p>صباحت آب در گلزارش از جوی گهربسته</p></div>
<div class="m2"><p>نزاکت رشته جان را بر آن موی کمر بسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سری از کوچه هر رگ برآورده است مژگانش</p></div>
<div class="m2"><p>ز شوخی تهمت خون بر زبان نیشتر بسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پریشانان همه جمعند و آن نازک میان حاضر</p></div>
<div class="m2"><p>که غیر از زلف، دیگر طرف ازان طرف کمربسته؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگردد چون کف افسوس هر برگ نهال من؟</p></div>
<div class="m2"><p>که چون بادام آوردند در باغم نظربسته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برآورده است از دل جوش چندین عقده مشکل</p></div>
<div class="m2"><p>گمان ساده‌لوحان این که (ما کمر بسته)</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نفس از سینه مجروح چون زخمی برون آید</p></div>
<div class="m2"><p>که آب چشمه پیکان سپهرم در جگر بسته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همانا دل شکست از من درین دریا حبابی را</p></div>
<div class="m2"><p>که چندین صف کمر در کشتنم موج خطر بسته</p></div></div>