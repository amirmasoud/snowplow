---
title: >-
    غزل شمارهٔ ۳۸۱۲
---
# غزل شمارهٔ ۳۸۱۲

<div class="b" id="bn1"><div class="m1"><p>مرا ز باده گلگون دماغ می سوزد</p></div>
<div class="m2"><p>چو لاله باده من در ایاغ می سوزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز می چراغ دگرها اگر شود روشن</p></div>
<div class="m2"><p>مرا ز باده روشن دماغ می سوزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به عشق لاله عذاران علاقه ای است مرا</p></div>
<div class="m2"><p>که من کباب شوم هر که داغ می سوزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بیکسی به جز از داغ ناامیدی نیست</p></div>
<div class="m2"><p>مرا کسی که به بالین چراغ می سوزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برد به خرمن مقصود ره سبکسیری</p></div>
<div class="m2"><p>که همچو برق نفس در سراغ می سوزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دگر کدام گل آتشین شکفته شده است؟</p></div>
<div class="m2"><p>که عندلیب ز بیرون باغ می سوزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیاهی از شب عاشق نمی برد زحمت</p></div>
<div class="m2"><p>اگرچه شب همه شب چون چراغ می سوزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا ز نشو و نما نیست بهره، ابر بهار</p></div>
<div class="m2"><p>عبث به تربیت من دماغ می سوزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بود ملال به مقدار مال هرکس را</p></div>
<div class="m2"><p>به قدر روغن خود هر چراغ می سوزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خیال روی که صائب مراست در دل گرم؟</p></div>
<div class="m2"><p>که اشک چون گهر شبچراغ می سوزد</p></div></div>