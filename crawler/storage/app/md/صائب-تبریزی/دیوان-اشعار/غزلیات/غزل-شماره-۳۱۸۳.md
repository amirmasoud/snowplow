---
title: >-
    غزل شمارهٔ ۳۱۸۳
---
# غزل شمارهٔ ۳۱۸۳

<div class="b" id="bn1"><div class="m1"><p>به بی برگی قناعت می کنم تا نوبهار آید</p></div>
<div class="m2"><p>به زخم خار دارم صبر تا گل در کنار آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلی نشکفت بر رخسارم از میخانه پردازی</p></div>
<div class="m2"><p>مگر در خون خود غلطم که رنگم برقرار آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرشک تلخ من آن روز نقل انجمن گردد</p></div>
<div class="m2"><p>که یارم با لبی شیرین تر از خواب بهار آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به فرصت می توان خصم سبکسر را ادب کردن</p></div>
<div class="m2"><p>مدارا می کنم با عقل تا فصل بهار آید؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به راه عشق اگر خاری مرا در دامن آویزد</p></div>
<div class="m2"><p>چنان گریم به درد دل که خون از چشم خار آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر اشک پشیمانی به فریادم رسد، ورنه</p></div>
<div class="m2"><p>چه دارم در بساط زندگی تا در شمار آید؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمی آید به کاری صائب اوراق پریشانم</p></div>
<div class="m2"><p>مگر آن رخنه دیوار را روزی به کار آید</p></div></div>