---
title: >-
    غزل شمارهٔ ۴۵۵۶
---
# غزل شمارهٔ ۴۵۵۶

<div class="b" id="bn1"><div class="m1"><p>بیشتر گردد دل نازک ز غمخواران فگار</p></div>
<div class="m2"><p>وای بر چشمی که از دستش بود بیماردار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر تهی مغزی ندارد جوهر میدان فقر</p></div>
<div class="m2"><p>کز تهیدستی زند درجان خود آتش چنار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنچه می آید به کار از شعر، می ماند به جا</p></div>
<div class="m2"><p>سوده گردد از جواهر آنچه ننشیند به کار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سست در گفتار مانند گنهکاران مباش</p></div>
<div class="m2"><p>سعی کن چون بیگناهان بر سخن باشی سوار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پله ای کز عشق و رسوایی مرا قسمت شده است</p></div>
<div class="m2"><p>هست طفل نی سوارم در نظر منصورودار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باشد از نقص جنون پهلو تهی کردن ز سنگ</p></div>
<div class="m2"><p>کز محک پروانمی دارد زرکامل عیار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حسن رابا خال باشد گوشه چشم دگر</p></div>
<div class="m2"><p>مهر کوچک را بود از مهرها بیش اعتبار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وحشتی دارم که چون حرف بیابان بگذرد</p></div>
<div class="m2"><p>می دود از سینه من دل برون دیوانه وار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر شهیدان پرتو منت گرانی می کند</p></div>
<div class="m2"><p>لاله خونین کفن دارد ز خود شمع مزار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در دویدن خواب نتوان کرد بر پشت سمند</p></div>
<div class="m2"><p>اهل دولت را به غفلت چون سرآمد روزگار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سرو از بی حاصلی بر یک قرار استاده است</p></div>
<div class="m2"><p>از تزلزل نیست ایمن هیچ نخل میوه دار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با تزلزل چشم نگشایند از خواب غرور</p></div>
<div class="m2"><p>وای اگر می بود دولتهای دنیا پایدار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شد فزون ناز وغرورحسن او صائب ز خط</p></div>
<div class="m2"><p>می شود خواب سبک ،سنگین درایام بهار</p></div></div>