---
title: >-
    غزل شمارهٔ ۳۳۹۴
---
# غزل شمارهٔ ۳۳۹۴

<div class="b" id="bn1"><div class="m1"><p>سینه را تیره هوا و هوسی می سازد</p></div>
<div class="m2"><p>وقت آیینه مکدر نفسی می سازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل معشوق اگر بیضه فولاد بود</p></div>
<div class="m2"><p>ناله سینه شکافم جرسی می سازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راستی پیشه خود کن خیانت کردن</p></div>
<div class="m2"><p>در و دیوار جهان را عسسی می سازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون گل از پوست برون خنده زنان می آید</p></div>
<div class="m2"><p>هر که چون غنچه به صاحب نفسی می سازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه شود گر به شکر خنده مرا شاد کنی؟</p></div>
<div class="m2"><p>شهد با آنهمه شان با مگسی می سازد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست در کار، شتاب اینهمه در سوختنم</p></div>
<div class="m2"><p>با سپند آتش سوزان نفسی می سازد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل ارباب هوس هر نفسی در جایی است</p></div>
<div class="m2"><p>کی سگ هرزه مرس با مرسی می سازد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در پس پرده تزویر و ریا زاهد خشک</p></div>
<div class="m2"><p>عنکبوتی است که دام مگسی می سازد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر دمی کز سر صدق است اثرها دارد</p></div>
<div class="m2"><p>صبح صد شمع خموشی از نفسی می سازد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بودم از ناکسی خویش خجل، زین غافل</p></div>
<div class="m2"><p>که ازین خاک سیه عشق کسی می سازد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روح در جسم محال است بماند صائب</p></div>
<div class="m2"><p>طایر قدس کجا با قفسی می سازد؟</p></div></div>