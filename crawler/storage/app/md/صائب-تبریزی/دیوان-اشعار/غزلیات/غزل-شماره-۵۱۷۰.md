---
title: >-
    غزل شمارهٔ ۵۱۷۰
---
# غزل شمارهٔ ۵۱۷۰

<div class="b" id="bn1"><div class="m1"><p>می نماید صد گره را یک گره زنار عشق</p></div>
<div class="m2"><p>سبحه داران چون برون آیند ازبازار عشق؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوی این می آسمانها را به دور انداخته است</p></div>
<div class="m2"><p>کیست تابرلب گذارد ساغر سرشار عشق؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تخم راز عشق را در خاک کردن مشکل است</p></div>
<div class="m2"><p>چون شرر از سنگ بیرون می جهد اسرار عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در سر هرذره ای اینجا هوای دیگرست</p></div>
<div class="m2"><p>اختر ثابت ندارد چرخ خوش پرگار عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق ظاهر ساختن معشوق را کامل کند</p></div>
<div class="m2"><p>ورنه عاشق رانباشد صرفه دراظهار عشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لاله خورشید بی تقریب می سوزد نفس</p></div>
<div class="m2"><p>سر فرو نارد به هر گل گوشه دستار عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می خورد ازسایه بال هما طبل گریز</p></div>
<div class="m2"><p>برسر هرکس که افتد سایه دیوار عشق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون توانم ازگل بی خار او دفتر گشود؟</p></div>
<div class="m2"><p>می زند پهلو به مژگان غزالان خار عشق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شوق موسی نخل ایمن رابه حرف آورده بود</p></div>
<div class="m2"><p>خامه صائب چرا بندد لب ازگفتار عشق؟</p></div></div>