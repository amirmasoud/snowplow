---
title: >-
    غزل شمارهٔ ۳۷۴۴
---
# غزل شمارهٔ ۳۷۴۴

<div class="b" id="bn1"><div class="m1"><p>شکفتگی ز می ناب تازگی دارد</p></div>
<div class="m2"><p>نشاط در ره سیلاب تازگی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین زمانه که خون خوردن است بیدردی</p></div>
<div class="m2"><p>شراب خوردن احباب تازگی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درین بساط که آیینه خانه بر دوش است</p></div>
<div class="m2"><p>گران رکابی سیماب تازگی دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به زخم من که ز الماس رو نمی تابد</p></div>
<div class="m2"><p>نمک فشانی مهتاب تازگی دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تغافل تو به یک زخم کار عالم ساخت</p></div>
<div class="m2"><p>ترحم از دل قصاب تازگی دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نظر به صبح بناگوش اوست موج سراب</p></div>
<div class="m2"><p>اگرچه پرتو مهتاب تازگی دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میان تیره دلان دشمنی است رسم قدیم</p></div>
<div class="m2"><p>نزاع آینه و آب تازگی دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز پیچ و تاب من آن چشم شوخ دلگیرست</p></div>
<div class="m2"><p>ز موج، شکوه گرداب تازگی دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غریب نیست ز سیل ایستادگی صائب</p></div>
<div class="m2"><p>شکیب عاشق بیتاب تازگی دارد</p></div></div>