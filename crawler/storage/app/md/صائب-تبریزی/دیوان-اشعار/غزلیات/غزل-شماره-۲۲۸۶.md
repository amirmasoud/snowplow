---
title: >-
    غزل شمارهٔ ۲۲۸۶
---
# غزل شمارهٔ ۲۲۸۶

<div class="b" id="bn1"><div class="m1"><p>می زند موج پریزاد، صنمخانه صبح</p></div>
<div class="m2"><p>فیض موجی است سبکسیر ز پیمانه صبح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می شود زود چو خورشید چراغش روشن</p></div>
<div class="m2"><p>هر که جایی نرود غیر در خانه صبح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تخم اشکی بفشان، خوشه آهی برچین</p></div>
<div class="m2"><p>مگذر بیخبر از مزرع بی دانه صبح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در محیطی که منم کشتی دریایی او</p></div>
<div class="m2"><p>کف خشکی است نصیب لب دیوانه صبح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل ما میکده خون جگر بد، که زدند</p></div>
<div class="m2"><p>از شفق پنجه خونین به در خانه صبح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست در سینه ما هیچ به جز داغ جنون</p></div>
<div class="m2"><p>جام خورشید زند دور به میخانه صبح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرو از ره به سخن سازی هر سرد نفس</p></div>
<div class="m2"><p>که شکرخواب بود حاصل افسانه صبح</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرو از راه چو اطفال به شیرینی خواب</p></div>
<div class="m2"><p>دیده ای آب ده از گریه مستانه صبح</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دام خورشید جهانتاب شود زنارش</p></div>
<div class="m2"><p>هر که از صدق کند خدمت بتخانه صبح</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای که از دل سیهی تلختر از شب شده ای</p></div>
<div class="m2"><p>می توان شد شکرستان به دو پیمانه صبح</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سینه صاف، دل گرم مهیا دارد</p></div>
<div class="m2"><p>مهر خورشید بود لازم پروانه صبح</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خنده رو باش درین بزم که ذرات جهان</p></div>
<div class="m2"><p>شیر مستند تمام از می پیمانه صبح</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شسته رویان به (دو) صد خون جگر رام شوند</p></div>
<div class="m2"><p>رام هر کس نشود معنی بیگانه صبح</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هست در سینه ترا گر دل روشن صائب</p></div>
<div class="m2"><p>می توان راست گذشت از در کاشانه صبح</p></div></div>