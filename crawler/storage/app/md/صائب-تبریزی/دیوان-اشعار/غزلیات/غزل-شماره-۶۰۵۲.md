---
title: >-
    غزل شمارهٔ ۶۰۵۲
---
# غزل شمارهٔ ۶۰۵۲

<div class="b" id="bn1"><div class="m1"><p>با هوسناکان چنین گر آشنا خواهی شدن</p></div>
<div class="m2"><p>بی مروت، بی حقیقت، بی وفا خواهی شدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانفشانی های ما را ای پریشان اختلاط</p></div>
<div class="m2"><p>یاد خواهی کرد چون از ما جدا خواهی شدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من گرفتم ساختی دامن ز چنگ من رها</p></div>
<div class="m2"><p>از کمند جذبه من چون رها خواهی شدن؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می روی دامن کشان صد چشم حسرت در قفا</p></div>
<div class="m2"><p>کیست آن کس کز تو پرسد تا کجا خواهی شدن؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عالمی سر در هوا از انتظارت گشته اند</p></div>
<div class="m2"><p>سایه گستر تا کجا همچون هما خواهی شدن؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر چنین با خود کنی بیگانگان را آشنا</p></div>
<div class="m2"><p>زود محتاج نگاه آشنا خواهی شدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در زمان سادگی گشتی به پرکاری تمام</p></div>
<div class="m2"><p>تا در این ایام خط مشکین چها خواهی شدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر به این سامان حسن آیینه پیش رو نهی</p></div>
<div class="m2"><p>پیش خود چون ما به صد دل مبتلا خواهی شدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر لب بام آفتابت از غبار خط رسید</p></div>
<div class="m2"><p>کی به صائب مهربان ای بی وفا خواهی شدن؟</p></div></div>