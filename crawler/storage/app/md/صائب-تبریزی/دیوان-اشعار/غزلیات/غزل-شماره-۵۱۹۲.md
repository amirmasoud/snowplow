---
title: >-
    غزل شمارهٔ ۵۱۹۲
---
# غزل شمارهٔ ۵۱۹۲

<div class="b" id="bn1"><div class="m1"><p>در دل خلد چو تیر قضا هر ادای خلق</p></div>
<div class="m2"><p>رحم است بر کسی که شود آشنای خلق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبح قیامت است جبین گشاده شان</p></div>
<div class="m2"><p>برق فناست خنده دندان نمای خلق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در شوره زار ریختن آب زندگی است</p></div>
<div class="m2"><p>از عمر آنچه صرف کنی دررضای خلق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در چار موجه لنگر کشتی است بادبان</p></div>
<div class="m2"><p>آسودگی طمع مکن ازآشنای خلق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرغی است کز گسستن دام است دلگران</p></div>
<div class="m2"><p>آن ساده دل که شکوه کند از جفای خلق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درآب زیر کاه خطر بیشتر بود</p></div>
<div class="m2"><p>از ره مرو به ظاهر صلح وصفای خلق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکس که برتو پشت کند مغتنم شمار</p></div>
<div class="m2"><p>کز روی کار خلق بود به، قفای خلق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تارو به خلق داری، پشتت به قبله است</p></div>
<div class="m2"><p>بر خلق پشت کن که شوی مقتدای خلق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیری ز حرف پوچ ندارندمردمان</p></div>
<div class="m2"><p>بی دانه سیر و دور کند آسیای خلق</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از پنبه ناز مرهم کافور می کشد</p></div>
<div class="m2"><p>گوشی که شد گزیده زآواز پای خلق</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دلسوزیش به اشک ندامت سرشته بود</p></div>
<div class="m2"><p>بستیم چشم یکقلم از توتیای خلق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا وحشتم به وادی تنها روی فکند</p></div>
<div class="m2"><p>برمن دهان شیر بود نقش پای خلق</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در دیده ها سبک نشوی تا چو برگ کاه</p></div>
<div class="m2"><p>ازجا مرو به جاذبه کهربای خلق</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صائب به درد خویش ز درمان کن اختصار</p></div>
<div class="m2"><p>کز درد بی دواست گرانتر دوای خلق</p></div></div>