---
title: >-
    غزل شمارهٔ ۲۷
---
# غزل شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>می کند گلگل نگه رخسار خندان ترا</p></div>
<div class="m2"><p>گل ز چیدن بیش می گردد گلستان ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آب نتواند به گرد دیده گشت از حیرتش</p></div>
<div class="m2"><p>نیست با خورشید نسبت روی تابان ترا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باغبان در بستن در سعی بی جا می کند</p></div>
<div class="m2"><p>چوب منع از جوش گل باشد گلستان ترا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تشنگی در خواب ممکن نیست کم گردد ز آب</p></div>
<div class="m2"><p>نیست صبر از خون عاشق چشم فتان ترا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پای خود چون کوه پیچیده است در دامن ز شرم</p></div>
<div class="m2"><p>دیده تا کبک دری سرو خرامان ترا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با قیامت نسبت آن قد موزون چون کنم؟</p></div>
<div class="m2"><p>شور محشر گرد دامانی است جولان ترا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه ناز و نعمت حسن تو بیش است از شمار</p></div>
<div class="m2"><p>روزیی جز خوردن دل نیست مهمان ترا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طوطیان دیگر اینجا سبزه بیگانه اند</p></div>
<div class="m2"><p>از خط سبزست طوطی شکرستان ترا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خون رحم چشم خونخوار تو می آمد به جوش</p></div>
<div class="m2"><p>خون اگر می کرد رنگین تیغ مژگان ترا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مانع از جولان نمی گردد شفق خورشید را</p></div>
<div class="m2"><p>نیست پروایی ز خون خلق دامان ترا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دارد از تمکین پا بر جای خود در پیچ و تاب</p></div>
<div class="m2"><p>گوی سیمین ذقن زلف چو چوگان ترا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می نماید برق عالمسوز در ابر سیاه</p></div>
<div class="m2"><p>خط کند پوشیده چون رخسار خندان ترا؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همچو مژگان تیر یک ترکش بود افکار تو</p></div>
<div class="m2"><p>مصرع بی رتبه صائب نیست دیوان ترا</p></div></div>