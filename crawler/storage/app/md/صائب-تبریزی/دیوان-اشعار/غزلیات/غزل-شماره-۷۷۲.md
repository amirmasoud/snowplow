---
title: >-
    غزل شمارهٔ ۷۷۲
---
# غزل شمارهٔ ۷۷۲

<div class="b" id="bn1"><div class="m1"><p>دست فلک کبود شد از گوشمال ما</p></div>
<div class="m2"><p>شوخی ز سر نهشت دل خردسال ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندین هزار جامه بدل کرد روزگار</p></div>
<div class="m2"><p>غفلت نگر که رنگ نگرداند حال ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با آن که آفتاب قیامت بلند شد</p></div>
<div class="m2"><p>بیرون نداد نم، عرق انفعال ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون آفتاب سرکشی ما زیاده شد</p></div>
<div class="m2"><p>چندان که بیش داد فلک خاکمال ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عمر آنچنان گذشت که رو باز پس نکرد</p></div>
<div class="m2"><p>دنبال خود ندید ز وحشت غزال ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>افکند روزگار به یکبار صد کمند</p></div>
<div class="m2"><p>از شش جهت به گردن وحشی غزال ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از سیلی خزان که ز رخ رنگ می برد</p></div>
<div class="m2"><p>نگذاشت باد سرکشی از سر نهال ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خال شب از صحیفه ایام محو شد</p></div>
<div class="m2"><p>از شبروی به تنگ نیامد خیال ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب هزار حیف که در مزرع جهان</p></div>
<div class="m2"><p>شد صرف شوره زار معاصی، زلال ما</p></div></div>