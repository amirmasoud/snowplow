---
title: >-
    غزل شمارهٔ ۵۴۱۴
---
# غزل شمارهٔ ۵۴۱۴

<div class="b" id="bn1"><div class="m1"><p>جسم خاکی را زغفلت چند معماری کنم</p></div>
<div class="m2"><p>چند اوقات گرامی صرف گلکاری کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قامت خم گشته را نتوان به حکمت راست کرد</p></div>
<div class="m2"><p>چند این دیوار مایل را نگهداری کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد زپیری ناتوان هر عضوی از اعضای من</p></div>
<div class="m2"><p>یک جهان بیمار را من چون پرستاری کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساده لوحی بین که می خواهم به دست رعشه دار</p></div>
<div class="m2"><p>توسن عمر سبکرو را عنانداری کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آفتاب تیغ زن اینجا سپر انداخته است</p></div>
<div class="m2"><p>من درین میدان چه اظهار جگر داری کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دبستان جهان تا چند با موی سفید</p></div>
<div class="m2"><p>صرف مد عمر خود را در سیه کاری کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از درو دیوار این غمخانه می بارد ملال</p></div>
<div class="m2"><p>من که را بااین غم بسیار غمخواری کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هست در خرمن مرامور و ملخ از دانه بیش</p></div>
<div class="m2"><p>خوشه چینان را به احسان چون هواداری کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون ز غفلت صرف مستی شد مرا سر جوش عمر</p></div>
<div class="m2"><p>به که این ته جرعه را در کار هشیاری کنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من که نیش پشه ای در خاک و خونم می کشد</p></div>
<div class="m2"><p>چون دم تیغ حوادث را سپر داری کنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون ز طوف کعبه مقصود گردم کامیاب</p></div>
<div class="m2"><p>من که در هر گام منزل از گرانباری کنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من که می دانم عزیزی می دهد خواری ثمر</p></div>
<div class="m2"><p>چون مه کنعان چرا اندیشه از خواری کنم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من که نتوانم گلیم خود بر آوردن ز آب</p></div>
<div class="m2"><p>دیگران را باکدامین دست و دل یاری کنم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>می کند سیل حوادث کوه را صائب ز جا</p></div>
<div class="m2"><p>من که از خار و خسم کمتر، چه خودداری کنم</p></div></div>