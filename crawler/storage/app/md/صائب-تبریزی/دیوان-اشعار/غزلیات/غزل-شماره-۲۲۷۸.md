---
title: >-
    غزل شمارهٔ ۲۲۷۸
---
# غزل شمارهٔ ۲۲۷۸

<div class="b" id="bn1"><div class="m1"><p>در جبین کس نمی بینیم انوار صلاح</p></div>
<div class="m2"><p>ریش و دستاری بجا مانده است ز آثار صلاح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای بسا میت که خواهد بی کفن رفتن به خاک</p></div>
<div class="m2"><p>گر چنین خواهد بزرگی یافت دستار صلاح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نوبت پاکی ز دلها با لباس افتاده است</p></div>
<div class="m2"><p>در گره افتاده از عمامه ها کار صلاح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جبهه پرهیزکاران نامه واکرده ای است</p></div>
<div class="m2"><p>اهل دل باشند مستغنی ز گفتار صلاح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهر زن بر لب ز اظهار صلاحیت، که نیست</p></div>
<div class="m2"><p>شاهدی بر فسق گویاتر ز اظهار صلاح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سعی کن چون عارفان در پاکی باطن، که نیست</p></div>
<div class="m2"><p>پاکی ظاهر متاع روی بازار صلاح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب آن جمعی که آگاهند ز آفات ریا</p></div>
<div class="m2"><p>از نظر پوشیده می دارند آثار صلاح</p></div></div>