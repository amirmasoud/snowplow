---
title: >-
    غزل شمارهٔ ۵۲۲۱
---
# غزل شمارهٔ ۵۲۲۱

<div class="b" id="bn1"><div class="m1"><p>ندامت بود بار مطلوب خشک</p></div>
<div class="m2"><p>مپیوند زنهار با چوب خشک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به نخل ثمردار پیوند کن</p></div>
<div class="m2"><p>مده دل چو قمری به محبوب خشک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مزن با خط سبز چین بر جبین</p></div>
<div class="m2"><p>که سوهان روح است مکتوب خشک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر بوی یوسف به کنعان گذشت؟</p></div>
<div class="m2"><p>که چون سرو شد تازه یعقوب خشک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به اهل خرابات مفروش زهد</p></div>
<div class="m2"><p>که آتش فتد زود در چوب خشک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود زاهد ژاژخا را کلام</p></div>
<div class="m2"><p>چو گردی که خیزد ز جاروب خشک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز آیینه سیراب نتوان شدن</p></div>
<div class="m2"><p>گذشتیم صائب ز مطلوب خشک</p></div></div>