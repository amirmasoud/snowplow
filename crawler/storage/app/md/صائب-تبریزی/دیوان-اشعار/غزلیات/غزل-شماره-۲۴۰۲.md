---
title: >-
    غزل شمارهٔ ۲۴۰۲
---
# غزل شمارهٔ ۲۴۰۲

<div class="b" id="bn1"><div class="m1"><p>غیر را در بزم خاص آن سیم‌تن می‌پرورد</p></div>
<div class="m2"><p>یوسف ما گرگ را در پیرهن می‌پرورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون چو گردد مشک هیهات است ماند در وطن</p></div>
<div class="m2"><p>نافه را بیهوده آهوی ختن می‌پرورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن حریف خار زخمم من که صحرای جنون</p></div>
<div class="m2"><p>هرکجا خاری است بهر پای من می‌پرورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوشه را هرگز نمی‌باشد دو سر، بگسل طمع</p></div>
<div class="m2"><p>می‌گدازد جان خود را هرکه تن می‌پرورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل‌رخان را می‌دهد تعلیم عاشق‌پروری</p></div>
<div class="m2"><p>گل که بلبل را در آغوش چمن می‌پرورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی‌تأمل دم مزن، کز لب گهر می‌ریزدش</p></div>
<div class="m2"><p>چون صدف هرکس سخن را در دهن می‌پرورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرده‌ای بر روی کار از جوی شیر افکنده است</p></div>
<div class="m2"><p>عشق، شیرین را به خون کوهکن می‌پرورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این غزل را هرکه گوید صائب از اهل سخن</p></div>
<div class="m2"><p>«می‌گدازد جان شیرین و سخن می‌پرورد»</p></div></div>