---
title: >-
    غزل شمارهٔ ۵۶۸۹
---
# غزل شمارهٔ ۵۶۸۹

<div class="b" id="bn1"><div class="m1"><p>ما نه زان بیخبرانیم که هشیار شویم</p></div>
<div class="m2"><p>یا به بانگ جرس قافله بیدار شویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما در آن صبح بنا گوش صبوحی زده ایم</p></div>
<div class="m2"><p>در قیامت چه خیال است که هشیار شویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فتح بابی نشد آیینه ما را ز جلا</p></div>
<div class="m2"><p>نیست بی صورت اگر در ته زنگار شویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مغز ما را نه چنان عشق پریشان کرده است</p></div>
<div class="m2"><p>که مقید به پریشانی دستار شویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما که از پشت ورق روی ورق می خوانیم</p></div>
<div class="m2"><p>به که قانع به نقاب از رخ دلدار شویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بحر و کان در نظرش چشم ترست و لب خشک</p></div>
<div class="m2"><p>حسن او را به چه سرمایه خریدار شویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما که قانع زبهاریم به نظاره خشک</p></div>
<div class="m2"><p>ادب این است که خار سر دیوار شویم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرما در قدم دار فنا افتاده است</p></div>
<div class="m2"><p>ما نه آنیم که بر دوش کسی بار شویم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عقل کرده است زمین گیر چون مرکز مرا را</p></div>
<div class="m2"><p>مگر از گردش آن چشم به پرگار شویم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می شود از نفس سوخته عالم تاریک</p></div>
<div class="m2"><p>ما به این شوق اگر قافله سالار شویم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا به کی صرف به گفتار شود نقد حیات</p></div>
<div class="m2"><p>صائب آن به که دگر بر سر کردار شویم</p></div></div>