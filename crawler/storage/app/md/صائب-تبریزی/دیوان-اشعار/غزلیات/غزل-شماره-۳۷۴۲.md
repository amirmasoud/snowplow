---
title: >-
    غزل شمارهٔ ۳۷۴۲
---
# غزل شمارهٔ ۳۷۴۲

<div class="b" id="bn1"><div class="m1"><p>شراب روز دل لاله را سیه دارد</p></div>
<div class="m2"><p>ازین سخن مگذر سرسری که ته دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فروغ مشعل خورشید کرم شب تاب است</p></div>
<div class="m2"><p>چنین که زلف تو روز مرا سیه دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه فیضها صدف از پرتو خموشی یافت</p></div>
<div class="m2"><p>گهر شود به کفش آب، هرکه ته دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چگونه بدر نگردد هلال غبغب او؟</p></div>
<div class="m2"><p>ز ناز بالش خورشید تکیه گه دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عنان گسسته چو سیلاب می روم، بفرست</p></div>
<div class="m2"><p>توجهی که عنان مرا نگه دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چسان برون ندهم شعله شکایت را؟</p></div>
<div class="m2"><p>ازان دلی که چو مجمر هزار ره دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گشود بند قبا بی حجاب، آه کجاست</p></div>
<div class="m2"><p>که چشم روزن این خانه را نگه دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درازدستی در کاروان احسان نیست</p></div>
<div class="m2"><p>وگرنه چندین یوسف هنر به چه دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کسی که فکر سر خود نمی کند صائب</p></div>
<div class="m2"><p>همیشه باد به کف، خاک در کله دارد</p></div></div>