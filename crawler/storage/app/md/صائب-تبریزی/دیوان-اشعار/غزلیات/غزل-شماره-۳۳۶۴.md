---
title: >-
    غزل شمارهٔ ۳۳۶۴
---
# غزل شمارهٔ ۳۳۶۴

<div class="b" id="bn1"><div class="m1"><p>از دو عالم دل اگر رو به سویدا می‌کرد</p></div>
<div class="m2"><p>سیر پرگار درین نقطه تماشا می‌کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساده‌لوحی که به دنبال دوا می‌گردد</p></div>
<div class="m2"><p>کاش دریوزه درد از در دل‌ها می‌کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر چراغ نفسش دست حمایت می‌شد</p></div>
<div class="m2"><p>برق اگر با خس و خاشاک مدارا می‌کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تازه گرداندن احرام سفر مطلب بود</p></div>
<div class="m2"><p>روی در ساحل اگر موج ز دریا می‌کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر ز افتادگی این راه نمی‌شد کوتاه</p></div>
<div class="m2"><p>دوری کعبه مقصود چه با ما می‌کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آب حیوان که سکندر ز سیاهی می‌جست</p></div>
<div class="m2"><p>بود آماده اگر رو به سویدا می‌کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سالک از دوری این راه خبر گر می‌یافت</p></div>
<div class="m2"><p>توشه در گام نخستین ز کمر وا می‌کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خبر از سینه پرآبله خویش نداشت</p></div>
<div class="m2"><p>آن که گوهر طلب از سینه دریا می‌کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آب می‌گشت به چشم دل پرآبله‌ام</p></div>
<div class="m2"><p>هرکه از کار دل خود گرهی وا می‌کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زاهد خشک ز درد طلب آگاه نبود</p></div>
<div class="m2"><p>ور نه تسبیح خود از آبله پا می‌کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>منت جان مکش از خلق که در شب خفاش</p></div>
<div class="m2"><p>جلوه از خجلت جان‌بخشی عیسی می‌کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>داشت از شاه سخن‌سنج امید تحسین</p></div>
<div class="m2"><p>صائب آن روز که این خوش غزل انشا می‌کرد</p></div></div>