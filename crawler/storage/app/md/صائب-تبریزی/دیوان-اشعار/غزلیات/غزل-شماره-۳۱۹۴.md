---
title: >-
    غزل شمارهٔ ۳۱۹۴
---
# غزل شمارهٔ ۳۱۹۴

<div class="b" id="bn1"><div class="m1"><p>به گوشم ناله اغیار دردآلود می آید</p></div>
<div class="m2"><p>درین محفل زچوب بید بوی عود می آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر بال و پر همت به فریادم رسد، ورنه</p></div>
<div class="m2"><p>چه قطع راه شوق از پای خواب آلود می آید؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زتمکین تو صبح محشر از جا برنمی خیزد</p></div>
<div class="m2"><p>تو گر قامت بر افرازی قیامت زود می آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نبیند مرگ تلخ عاشقان را هیچ سنگین دل!</p></div>
<div class="m2"><p>هنوز از بیستون فریاد دردآلود می آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که خود را بر دل ما خاکساران می زند یارب؟</p></div>
<div class="m2"><p>که آه از سینه چون زنبور خاک آلود می آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گهر بر صفحه آیینه خود را چون نگه دارد؟</p></div>
<div class="m2"><p>عرق از چهره صافش به دامن زود می آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر دل را سبک خواهی، به لب مهر خموشی زن</p></div>
<div class="m2"><p>که دود دل برون زین روزن مسدود می آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی کاینجا نشوید چهره از اشک پشیمانی</p></div>
<div class="m2"><p>به صحرای جزا با روی گردآلود می آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زحیرانی همان در وادی سرگشتگی محوم</p></div>
<div class="m2"><p>اگر پیشانیم بر کعبه مقصود می آید</p></div></div>