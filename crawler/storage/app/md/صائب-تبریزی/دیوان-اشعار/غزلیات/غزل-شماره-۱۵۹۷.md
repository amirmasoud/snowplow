---
title: >-
    غزل شمارهٔ ۱۵۹۷
---
# غزل شمارهٔ ۱۵۹۷

<div class="b" id="bn1"><div class="m1"><p>شادی هر که زیادست ز غم، کامل نیست</p></div>
<div class="m2"><p>هر که را خرج ز دخل است فزون، عاقل نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل گردون متأثر نشد از گریه ما</p></div>
<div class="m2"><p>گنه تخم چه باشد چو زمین قابل نیست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشق آن است که سر بر قدم دار نهد</p></div>
<div class="m2"><p>میوه تا در گرو شاخ بود کامل نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طالع حلقه زلف تو کبابم دارد</p></div>
<div class="m2"><p>کز تماشای تو یک چشم زدن غافل نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رشته نسبت بی پا و سران همتاب است</p></div>
<div class="m2"><p>گرهی نیست به زلفش که مرا در دل نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیل ویرانه ام، آرام نمی دانم چیست</p></div>
<div class="m2"><p>هیچ سنگی به ره من بتر از منزل نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جوش عشق است که در ظرف نگنجد، ورنه</p></div>
<div class="m2"><p>ساغر بحر زیاد از دهن ساحل نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خطر قلزم هستی، گل خودکامیهاست</p></div>
<div class="m2"><p>نیست یک موج که در بحر رضا ساحل نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرد هستی اگر از پیش نظر برخیزد</p></div>
<div class="m2"><p>رهروی نیست درین راه که در منزل نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چند صائب جگر خود خوری از فکر سخن؟</p></div>
<div class="m2"><p>جز دل چاک، قلم را ز سخن حاصل نیست</p></div></div>