---
title: >-
    غزل شمارهٔ ۵۱۹۵
---
# غزل شمارهٔ ۵۱۹۵

<div class="b" id="bn1"><div class="m1"><p>از ملامتگر ندارد یوسف بی جرم، باک</p></div>
<div class="m2"><p>گرد تهمت پاک می سازد ز رخ دامان پاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عیب می گردد هنر در دیده های پاک بین</p></div>
<div class="m2"><p>نور ماه ناقص از روزن تمام افتد به خاک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از سر تقصیر ما ای محتسب گر نگذری</p></div>
<div class="m2"><p>مرحمت کن حد ماباری بزن با چوب تاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چرب نرمی سد راه سیل آفت می شود</p></div>
<div class="m2"><p>باده زورین نمی سازد کدو را سینه چاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هست ماه عید، صیقل درنظر آیینه را</p></div>
<div class="m2"><p>عاشق پر دل نمی اندیشد از تیغ هلاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاکساری سرکشان رابر سر رحم آورد</p></div>
<div class="m2"><p>ورنه تیر آن کمان ابرو نمی افتد به خاک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گلعذاری کز تراش خط صفا دارد طمع</p></div>
<div class="m2"><p>زنگ را با دامن تر می کند ز آیینه پاک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیدن وضع جهان بارست بر روشندلان</p></div>
<div class="m2"><p>نیست صائب شکوه ای ما را ز چشم خوابناک</p></div></div>