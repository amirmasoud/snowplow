---
title: >-
    غزل شمارهٔ ۵۷۶۰
---
# غزل شمارهٔ ۵۷۶۰

<div class="b" id="bn1"><div class="m1"><p>چراغ طور نسوزد اگر کلیم شوم</p></div>
<div class="m2"><p>شکفتگی نکند گل اگر نسیم شوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس است جوهر ذاتی مرا، نه آن گهرم</p></div>
<div class="m2"><p>که گر صدف برود از سرم یتیم شوم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دم مسیح درین گلستان گرانجان است</p></div>
<div class="m2"><p>به اعتماد کدام آبرو نسیم شوم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فلک مراد کریمان نمی دهد صائب</p></div>
<div class="m2"><p>به مصلحت دو سه روزی مگر لئیم شوم</p></div></div>