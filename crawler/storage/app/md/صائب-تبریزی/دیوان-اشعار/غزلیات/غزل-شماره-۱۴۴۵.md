---
title: >-
    غزل شمارهٔ ۱۴۴۵
---
# غزل شمارهٔ ۱۴۴۵

<div class="b" id="bn1"><div class="m1"><p>هر که از درد طلب شکوه کند نامردست</p></div>
<div class="m2"><p>عشق دردی است که درمان هزاران دردست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کثرت خلق به وحدت نرساند نقصان</p></div>
<div class="m2"><p>که علم غوطه به لشکر زده است و فردست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مهر و مه نور دهد تا نظر ما بیناست</p></div>
<div class="m2"><p>چرخ در گرد بود تا سر ما در گردست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوچه گردان جنون موج سرایی دارند</p></div>
<div class="m2"><p>عشرت روی زمین رزق بیابانگردست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جرم ابنای زمان را ز فلک می دانیم</p></div>
<div class="m2"><p>هر چه شب دزد نماید گنه شبگردست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مس طلا می شود از نور عبادت صائب</p></div>
<div class="m2"><p>روی شبخیز چو خورشید ازان رو زردست</p></div></div>