---
title: >-
    غزل شمارهٔ ۲۹۱۳
---
# غزل شمارهٔ ۲۹۱۳

<div class="b" id="bn1"><div class="m1"><p>سر عاشق زتن کی هر می کم زور بردارد؟</p></div>
<div class="m2"><p>که این خشت از سرخم باده منصور بردارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر برق تجلی گوشه ابرو نجنباند</p></div>
<div class="m2"><p>که از راه کلیم الله سنگ طور بردارد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس از عمری به دستش تخته ای افتاده زین دریا</p></div>
<div class="m2"><p>به زودی چون دل از دار فنا منصور بردارد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو مجنونی که بوی نوبهارش بر مشام آید</p></div>
<div class="m2"><p>مرا از دور چون بیند بیابان شور بردارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من آن لب تشنه ام کز سادگی بیرون روم از خود</p></div>
<div class="m2"><p>اگر موج سرابی دست خود از دور بردارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تواند هر که لب بر لب نهادن جویباران را</p></div>
<div class="m2"><p>زهی غفلت که ناز چینی فغفور بردارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به خون گرم هر کس داغ خود چون لاله به سازد</p></div>
<div class="m2"><p>چرا ناز خنک از مرهم کافور بردارد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تواند هر که بر خود کرد شیرین تلخی عالم</p></div>
<div class="m2"><p>چه افتاده است شهد از خانه زنبور بردارد؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مشو در عین قدرت از ضعیفان جهان غافل</p></div>
<div class="m2"><p>که از دوش سلیمان بار اینجا مور بردارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زنور حسن مژگان موی آتش دیده می گردد</p></div>
<div class="m2"><p>چه نقش از روی شیرین خامه شاپور بردارد؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سبکروحی و تمکین لازم افتاده است پیری را</p></div>
<div class="m2"><p>که طفل از جا کمانی را به صدمن زور بردارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نهد در دامن ناز دگر از سرگرانیها</p></div>
<div class="m2"><p>سری کز خواب ناز آن نرگس مخمور بردارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وصال پاکدامانان به پاکان می رسد صائب</p></div>
<div class="m2"><p>نسیم صبح مهر از غنچه مستور بردارد</p></div></div>