---
title: >-
    غزل شمارهٔ ۵۳۰۴
---
# غزل شمارهٔ ۵۳۰۴

<div class="b" id="bn1"><div class="m1"><p>سالها گرد زمین چون آسمان گردیده ام</p></div>
<div class="m2"><p>تا چنین صافی دل و روشن روان گردیده ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سبز گردیده است چون طوطی پروبالم ز زهر</p></div>
<div class="m2"><p>تا درین عبرت سرا شیرین زبان گردیده ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>استخوانم را هما تعویذ بازو می کند</p></div>
<div class="m2"><p>تا نشان تیر آن ابرو کمان گردیده ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر گلی داغی و هر خاری زبان شکوه ای است</p></div>
<div class="m2"><p>گرد این گلزار چون آب روان گردیده ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زندگی در چار دیوار عناصر چون کنم</p></div>
<div class="m2"><p>من که در دامان دشت لامکان گردیده ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سایه من گر چه می بخشد سعادت خلق را</p></div>
<div class="m2"><p>از جهان قانع به مشتی استخوان گردیده ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست آبی غیر آب تیغ با من سازگار</p></div>
<div class="m2"><p>من که از زخم نمایان گلستان گردیده ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ذره ام اما ز فیض داغ عالمسوز عشق</p></div>
<div class="m2"><p>روشنی بخش زمین و آسمان گردیده ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی دماغی صائب از عالم مرا بیگانه کرد</p></div>
<div class="m2"><p>با که سازم من که از خود دلگران گردیده ام</p></div></div>