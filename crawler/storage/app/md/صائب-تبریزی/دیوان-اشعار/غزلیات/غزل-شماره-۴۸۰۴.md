---
title: >-
    غزل شمارهٔ ۴۸۰۴
---
# غزل شمارهٔ ۴۸۰۴

<div class="b" id="bn1"><div class="m1"><p>به اختیار ز نزهت سرای جان برخیز</p></div>
<div class="m2"><p>گران نگشته ازین خاک آستان برخیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گره مشو به دل خاک تیره چون قارون</p></div>
<div class="m2"><p>چوعیسی از سر این تیره خاکدان برخیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو تخم اشک ممان از فسردگی در خاک</p></div>
<div class="m2"><p>چو آه گرم شو از سینه جهان برخیز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اجل نیامده جان را به طاق نسیان نه</p></div>
<div class="m2"><p>روان نگشته قضا از سر روان برخیز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دمادم است که در خرمن تو افتاده است</p></div>
<div class="m2"><p>ز زیر تیغ شرربار کهکشان برخیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز گریه دل شب، روی شمع نورانی است</p></div>
<div class="m2"><p>تو نیز شب به دو چشم شررفشان برخیز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مده ز دست گریبان غنچه خسبی را</p></div>
<div class="m2"><p>گل صباح، گل از بستر گران برخیز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تلاش عالم بالای خاکساری کن</p></div>
<div class="m2"><p>به صدر اگر بنشینی، ز آستان برخیز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نفس شمرده زدن صبح را جوان دارد</p></div>
<div class="m2"><p>توهم شمرده نفس خرج کن، جوان برخیز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پل شکسته به سیلاب برنمی آید</p></div>
<div class="m2"><p>ز راه اشک من ای طاق کهکشان برخیز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>محک چه صرفه برد از زر تمام عیار؟</p></div>
<div class="m2"><p>ز پیش راه من ای سنگ امتحان برخیز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>منه به دوش عصا بار ناتوانی خویش</p></div>
<div class="m2"><p>شراب کهنه به دست آور و جوان برخیز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زبان طرز نظیری است صائب این مصرع</p></div>
<div class="m2"><p>که پیش ازان که نگردیده ای گران برخیز</p></div></div>