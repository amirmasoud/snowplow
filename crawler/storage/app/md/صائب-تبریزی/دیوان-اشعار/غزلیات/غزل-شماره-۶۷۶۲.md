---
title: >-
    غزل شمارهٔ ۶۷۶۲
---
# غزل شمارهٔ ۶۷۶۲

<div class="b" id="bn1"><div class="m1"><p>مجو چون غافلان از عالم اسباب بیداری</p></div>
<div class="m2"><p>که پیدا کم شود در پرده های خواب بیداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشو از سجده آن طاق ابرو یک نفس غافل</p></div>
<div class="m2"><p>که می خواهد به جای شمع این محراب بیداری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نصیحت بی ثمر باشد زمین گیران غفلت را</p></div>
<div class="m2"><p>نینگیزد ره خوابیده را از خواب بیداری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به عنوانی که سوزد شمع روشن پرده شب را</p></div>
<div class="m2"><p>فزاید زنده دل را بستر سنجاب بیداری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل آگاه تا دارد نفس از پای ننشیند</p></div>
<div class="m2"><p>نگیرد هیچ جا آرام چون سیماب بیداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز ماه آسمان گردد درو بام نظر روشن</p></div>
<div class="m2"><p>درون خانه دل را بود مهتاب بیداری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگردد سینه پاک از آرزوها با گرانخوابی</p></div>
<div class="m2"><p>بود این خار و خس را آتشین سیلاب بیداری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز روی شبنم افشان خواب ناز او گرانتر شد</p></div>
<div class="m2"><p>اگر چه هست خواب آلود را از آب بیداری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مده در گوش خود ره گفتگوی اهل غفلت را</p></div>
<div class="m2"><p>که می گردد ازین افسانه مست خواب بیداری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل روشن بود از دیده بی خواب مستغنی</p></div>
<div class="m2"><p>چراغ روز باشد در شب مهتاب بیداری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شد از افسانه حسن تو از بس خوابها شیرین</p></div>
<div class="m2"><p>نهان شد از نظر چون گوهر نایاب بیداری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز یک بیدار دل صدمرده دل بیدار می گردد</p></div>
<div class="m2"><p>که عالم را دهد خورشید عالمتاب بیداری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مشو غافل ز پیچ و تاب اگر دل زنده ای صائب</p></div>
<div class="m2"><p>که جوهردار می گردد ز پیچ و تاب بیداری</p></div></div>