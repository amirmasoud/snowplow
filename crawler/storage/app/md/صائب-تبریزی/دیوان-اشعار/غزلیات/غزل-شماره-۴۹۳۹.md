---
title: >-
    غزل شمارهٔ ۴۹۳۹
---
# غزل شمارهٔ ۴۹۳۹

<div class="b" id="bn1"><div class="m1"><p>چه می پرسی ز احوال شرار ما و پروازش</p></div>
<div class="m2"><p>که در یک نقطه طی شد جلوه انجام و آغازش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازان چون مهر تابان است حسنش از زوال ایمن</p></div>
<div class="m2"><p>که لغزد پای خط ازچهره آیینه پردازش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به جای سبزه گر صبح قیامت از زمین روید</p></div>
<div class="m2"><p>ز تمکین زیرپای خود نبیند حسن طنازش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو مژگان هر دو عالم رابه هم افکند از شوخی</p></div>
<div class="m2"><p>همان ناخن زند بر یکدگر چشم سخنسازش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پریشان گر شود اجزای مجلس جمع کن دل را</p></div>
<div class="m2"><p>که مطرب می کند شیرازه باز از رشته سازش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی باشد خط آزادی و پروانه کشتن</p></div>
<div class="m2"><p>قفس افتاده مرغی راکه رفت از یاد پروازش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه گل چیند کنار ما زشمع نازک اندامی</p></div>
<div class="m2"><p>که از بال و پر پروانه باشد زحمت گازش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درین یک قطره خون راز عشقش رانگه دارم؟</p></div>
<div class="m2"><p>که تنگی می کند این نه صدف بر گوهر رازش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مگر درخواب بیند وصل گل، کوتاه پروازی</p></div>
<div class="m2"><p>که هم در آشیان خود بود چون چشم، پروازش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لبش راه سخن بسته است بر عاشق، ولی دارد</p></div>
<div class="m2"><p>ز هر مژگان زبانی در دهن چشم سخنسازش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه خواهد شد سرانجام دل مومین من صائب؟</p></div>
<div class="m2"><p>که خارا سینه کبک است پیش چنگل بازش</p></div></div>