---
title: >-
    غزل شمارهٔ ۱۵۵۸
---
# غزل شمارهٔ ۱۵۵۸

<div class="b" id="bn1"><div class="m1"><p>دوزخ اهل نظر، پاس نگه داشتن است</p></div>
<div class="m2"><p>چه بهشتی است که معشوقه ما بازاری است!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راه عشق از خودی توست چنین پست و بلند</p></div>
<div class="m2"><p>اگر از خویش برآیی، همه جا همواری است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا درین دایره ای، خون خور و خاموش نشین</p></div>
<div class="m2"><p>که در آغوش رحم، کار جنین خونخواری است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عافیت می طلبی، پای خم از دست مده</p></div>
<div class="m2"><p>که بلاها همه در زیر سر هشیاری است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نسبت فقر به هر بی سر و پا نتوان کرد</p></div>
<div class="m2"><p>شال پیچیدن این قوم ز بی دستاری است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در کمین است که صیدی نجهد از دامش</p></div>
<div class="m2"><p>غنچه خسبیدن زهاد نه از دینداری است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>(کار ما نیست سر زلف سخن شانه زدن</p></div>
<div class="m2"><p>اینقدر هست که یک پرده به از بیکاری است!)</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نسبتش با همه جا و همه کس یکسان است</p></div>
<div class="m2"><p>هر که چون صائب از آیین تکلف عاری است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مستی چشم تو در مرتبه هشیاری است</p></div>
<div class="m2"><p>خواب آهونگهان شوختر از بیداری است</p></div></div>