---
title: >-
    غزل شمارهٔ ۱۲۵۰
---
# غزل شمارهٔ ۱۲۵۰

<div class="b" id="bn1"><div class="m1"><p>سنگ راهی شوق را چون چشم سنگین خواب نیست</p></div>
<div class="m2"><p>راه پیما را براقی چون دل بیتاب نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از عزیزیهای غربت دل نمی گیرد قرار</p></div>
<div class="m2"><p>آب در صلب گهر بی رعشه سیماب نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برگ از آزادگی بیرون نیارد سرو را</p></div>
<div class="m2"><p>بر دل عارف گران جمعیت اسباب نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشکل است از عالم آب آمدن آسان برون</p></div>
<div class="m2"><p>موج این دریا به گیرایی کم از قلاب نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از خودآرایان، دل روشن طمع کردن خطاست</p></div>
<div class="m2"><p>اخگر دل زنده در خاکستر سنجاب نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخت روشنگر شود ز آیینه تاریک سبز</p></div>
<div class="m2"><p>بحر را بر دل غبار از ظلمت سیلاب نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرده پوش پای خواب آلود، طرف دامن است</p></div>
<div class="m2"><p>زاهد دلمرده را جایی به از محراب نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آشنایانند یکسر پرده بیگانگی</p></div>
<div class="m2"><p>فیض در جمعیت احباب چون اسباب نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می کشد موج می از دل ریشه غم را برون</p></div>
<div class="m2"><p>این نهنگ جان ستان را غیر ازین قلاب نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از دل روشن شود نزدیک، منزلهای دور</p></div>
<div class="m2"><p>شبروان را بال پروازی به از مهتاب نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پشت ما گرم است از خورشید عالمتاب عشق</p></div>
<div class="m2"><p>دیده ما بر سمور و قاقم و سنجاب نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خواب مخمل پرده چشم غلط بینان شده است</p></div>
<div class="m2"><p>ورنه در نی بوریا را غیر شکر خواب نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آه صائب کز لب میگون آن بیدادگر</p></div>
<div class="m2"><p>عشقبازان را به جز خمیازه فتح الباب نیست</p></div></div>