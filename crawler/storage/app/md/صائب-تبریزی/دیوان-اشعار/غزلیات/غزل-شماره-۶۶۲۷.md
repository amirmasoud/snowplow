---
title: >-
    غزل شمارهٔ ۶۶۲۷
---
# غزل شمارهٔ ۶۶۲۷

<div class="b" id="bn1"><div class="m1"><p>ای راز نه فلک ز جبینت عیان همه</p></div>
<div class="m2"><p>در دامن تو حاصل دریا و کان همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اسرار چار دفتر و مضمون نه کتاب</p></div>
<div class="m2"><p>در نقطه تو ساخته ایزد نهان همه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدوسیان به حکم خداوند امر و نهی</p></div>
<div class="m2"><p>پیش تو سرگذاشته بر آستان همه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روحانیان برای تماشای جلوه ات</p></div>
<div class="m2"><p>چون کودکان برآمده بر آسمان همه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کردی جدا به تیغ زبان اسم هر چه هست</p></div>
<div class="m2"><p>نام از تو یافت چرخ و زمین و زمان همه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در عرض حال بسته زبانان عرش و فرش</p></div>
<div class="m2"><p>یکسر نموده اند ترا ترجمان همه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از قطره تا به قلزم و از ذره تا به مهر</p></div>
<div class="m2"><p>پیش تو کرده راز دل خود عیان همه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از بهر خدمت تو فلکها چو بندگان</p></div>
<div class="m2"><p>ز اخلاص بسته اند کمر بر میان همه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در کار توست چرخ بلند و زمین پست</p></div>
<div class="m2"><p>از بهر رزق توست نعیم جهان همه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غیر از تو هر که هست درین میهمانسرا</p></div>
<div class="m2"><p>نان تو می خورند بر این گرد خوان همه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>افلاک پیش قامت همچون خدنگ تو</p></div>
<div class="m2"><p>خم کرده اند پشت ادب چون کمان همه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غیر از تو نیست شعله دیگر درین بساط</p></div>
<div class="m2"><p>افلاک و انجمند شرار و دخان همه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جستند از فروغ دل زنده ات چو صبح</p></div>
<div class="m2"><p>دلمردگان خاک ز خواب گران همه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>غیر از تو نیست مردمکی چشم چرخ را</p></div>
<div class="m2"><p>روشن به توست چشم زمین و زمان همه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شیران ببر صولت و فیلان جنگجوی</p></div>
<div class="m2"><p>دادند عاجزانه به دستت عنان همه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در خدمت تو تازه نهالان بوستان</p></div>
<div class="m2"><p>استاده اند بر سر پا چون سنان همه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پیش تو سر به خاک مذلت نهاده اند</p></div>
<div class="m2"><p>با آن علو مرتبه روحانیان همه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در گوش کرده حلقه فرمان پذیریت</p></div>
<div class="m2"><p>خاک و هوا و آتش و آب روان همه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نه آسمان ز شوق لب درفشان تو</p></div>
<div class="m2"><p>واکرده اند همچو صدفها دهان همه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پاس نفس بدار و قدم را شمرده زن</p></div>
<div class="m2"><p>دارند چشم بر تو درین کاروان همه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>این آن غزل که اوحدی خوش کلام گفت</p></div>
<div class="m2"><p>ای روشن از رخ تو زمین و زمان همه</p></div></div>