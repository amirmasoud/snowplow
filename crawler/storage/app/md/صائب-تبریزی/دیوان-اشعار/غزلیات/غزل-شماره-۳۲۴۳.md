---
title: >-
    غزل شمارهٔ ۳۲۴۳
---
# غزل شمارهٔ ۳۲۴۳

<div class="b" id="bn1"><div class="m1"><p>عشق تا هست عنان را به هوس نتوان داد</p></div>
<div class="m2"><p>چون قلم نبض به دست همه کس نتوان داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناله ای کز سر در دست شنیدن دارد</p></div>
<div class="m2"><p>دل به بیهوده درایان جرس نتوان داد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست هر گوش به اسرار حقیقت لایق</p></div>
<div class="m2"><p>طوق زرین به سگ هرزه مرس نتوان داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دم باد صبا غنچه پریشان گردید</p></div>
<div class="m2"><p>دل به افسانه هر سرد نفس نتوان داد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه کند یوسف اگر تن ندهد در زندان؟</p></div>
<div class="m2"><p>تن به آغوش زلیخای هوس نتوان داد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عقل از دایره بیخبران بیرون است</p></div>
<div class="m2"><p>به خرابات مغان راه عسس نتوان داد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساقی میکده قسمت حق مختارست</p></div>
<div class="m2"><p>جام اگر صاف و اگر درد به پس نتوان داد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا توان فکر گلوسوز شنیدن صائب</p></div>
<div class="m2"><p>هوش خود را به شکر همچو مگس نتوان داد</p></div></div>