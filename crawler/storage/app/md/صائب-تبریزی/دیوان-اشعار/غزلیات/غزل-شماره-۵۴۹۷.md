---
title: >-
    غزل شمارهٔ ۵۴۹۷
---
# غزل شمارهٔ ۵۴۹۷

<div class="b" id="bn1"><div class="m1"><p>نخوردم نیش خاری تا وداع رنگ و بو کردم</p></div>
<div class="m2"><p>ز شر ایمن شدم تا خیرباد آرزو کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برو ای خرقه تقوی هوایی گیر از دوشم</p></div>
<div class="m2"><p>که من دوش و بر خود وقف مینا و سبو کردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مبادا بخیه هشیاریم بر روی کار افتد</p></div>
<div class="m2"><p>گریبان چاکی خمیازه را از می رفو کردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمی دانم چه خواهم کرد با دشنام تلخ او</p></div>
<div class="m2"><p>برآمد خون ز چشمم تا به زهر چشم خو کردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کجا افتادی ای دردانه مقصود از دستم</p></div>
<div class="m2"><p>که من با سیل خون این خاکدان را ریگ شو کردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو سرمه پرده پرده بر سواد چشم او گشتم</p></div>
<div class="m2"><p>چو شانه در سر زلفش تصرف موبمو کردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو مرجان سرخ رو از آبروی همت خویشم</p></div>
<div class="m2"><p>نه چون گوهر به آب چشمه نیسان وضو کردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درین گلشن نکردم در رعایت هیچ تقصیری</p></div>
<div class="m2"><p>ز اشک تاک دایم آب در پای کدو کردم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سراسر حاصل جنت به یک جو برنمی گیرم</p></div>
<div class="m2"><p>نگه را دانه خور از روی گندم گون او کردم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز بخت سبز خود در زیر بار منتم صائب</p></div>
<div class="m2"><p>چو طوطی از سخن تسخیر آن آیینه رو کردم</p></div></div>