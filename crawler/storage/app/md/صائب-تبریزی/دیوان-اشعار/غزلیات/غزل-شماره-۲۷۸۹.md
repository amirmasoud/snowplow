---
title: >-
    غزل شمارهٔ ۲۷۸۹
---
# غزل شمارهٔ ۲۷۸۹

<div class="b" id="bn1"><div class="m1"><p>غنی فیض از دل شب چون فقیران در نمی یابد</p></div>
<div class="m2"><p>زظلمت آنچه یابد خضر، اسکندر نمی یابد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برآ از قید خودبینی که در زندان آب و گل</p></div>
<div class="m2"><p>کسی کز خود نپوشد چشم راه در نمی یابد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود زیر نگین ملک سلیمان تنگدستی را</p></div>
<div class="m2"><p>که غیر از گوشه دل، گوشه دیگر نمی یابد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گهی در حلقه تسبیح و گه در قید زنارم</p></div>
<div class="m2"><p>کسی از رشته سر در گم من سر نمی یابد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سپند من مسلم چون تو اندجست ازین آتش؟</p></div>
<div class="m2"><p>که دود من ره بیرون شد از مجمر نمی یابد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زسایل می گشاید غنچه امید همت را</p></div>
<div class="m2"><p>نخندد شیشه سربسته تا ساغر نمی یابد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گشاد از بستگیهاجو، که تا غواص در دریا</p></div>
<div class="m2"><p>نمی سازد نفس در دل گره گوهر نمی یابد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مجو سر رشته آسایش از دنیای پروحشت</p></div>
<div class="m2"><p>که موج آسودگی در بحر بی لنگر نمی یابد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نباشد در مقام خویشتن قدری هنرور را</p></div>
<div class="m2"><p>که در دریا کسی بوی خوش از عنبر نمی یابد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به بی شرمی توان شد کامیاب از چرخ مینایی</p></div>
<div class="m2"><p>زدریا جز حباب شوخ چشم افسر نمی یابد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به شعر خشک صائب رام نتوان کرد خوبان را</p></div>
<div class="m2"><p>که گوهر راه در گوش بتان بی زر نمی یابد</p></div></div>