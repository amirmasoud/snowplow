---
title: >-
    غزل شمارهٔ ۲۶۲۰
---
# غزل شمارهٔ ۲۶۲۰

<div class="b" id="bn1"><div class="m1"><p>ذوق خاموشی مرا روزی که دامنگیر بود</p></div>
<div class="m2"><p>گرد را هم سرمه سای ناله زنجیر بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این زمان منزل پرستم، ورنه چندی پیش ازین</p></div>
<div class="m2"><p>نقش پای خضر در چشمم دهان شیر بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست معمار فلک را کوتهی پیچیده داشت</p></div>
<div class="m2"><p>تا دل ویرانه من قابل تعمیر بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهر چشم عشق هر پیمانه خونم که داد</p></div>
<div class="m2"><p>چون به رغبت نوش کردم کاسه پر شیر بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق آتشدست تا در پیکر من خانه داشت</p></div>
<div class="m2"><p>سینه من گرمتر از خوابگاه شیر بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تخته مشق حوادث نیست صائب این زمان</p></div>
<div class="m2"><p>سینه او چون هدف دایم نشان تیر بود</p></div></div>