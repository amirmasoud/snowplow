---
title: >-
    غزل شمارهٔ ۴۲۵۶
---
# غزل شمارهٔ ۴۲۵۶

<div class="b" id="bn1"><div class="m1"><p>چینی اگر ز سنبل زلف تو وا شود</p></div>
<div class="m2"><p>خون از دماغ مشک روان درختا شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد پیرهن عرق کند از شرم، ماه مصر</p></div>
<div class="m2"><p>یک عقده گر ز بند قبای تو وا شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این شیوه ها که من زمیان تو دیده ام</p></div>
<div class="m2"><p>مشکل به صد عبارت نازک ادا شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگشای لب که آب شود گوهر از حجاب</p></div>
<div class="m2"><p>بنمای رخ که آینه محو صفا شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آب گهر به چشم صدف اشک حسرت است</p></div>
<div class="m2"><p>آنجا که لعل او به شکر خنده وا شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خط شکسته است مرا خط سرنوشت</p></div>
<div class="m2"><p>بال هما به طالع من بوریا شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محراب صبح، گوشه ابرو بلند کرد</p></div>
<div class="m2"><p>ساقی مهل نماز صراحی قضا شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر کس به ذوق معنی بیگانه آشناست</p></div>
<div class="m2"><p>صائب به طرز تازه ما آشناست</p></div></div>