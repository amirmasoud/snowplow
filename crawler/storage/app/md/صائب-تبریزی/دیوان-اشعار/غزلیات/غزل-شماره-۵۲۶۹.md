---
title: >-
    غزل شمارهٔ ۵۲۶۹
---
# غزل شمارهٔ ۵۲۶۹

<div class="b" id="bn1"><div class="m1"><p>از سرکشی و ناز ندارد سر ما گل</p></div>
<div class="m2"><p>سرپیش فکنده است به تقریب حیا گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کو فرصت دلجویی مرغان گرفتار</p></div>
<div class="m2"><p>خاری نتوانست برآورد ز پا گل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکرنگی عشق است که از خاک برآید</p></div>
<div class="m2"><p>با جامه خونین به طریق شهدا گل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غافل مشو از شبنم این باغ که چیده است</p></div>
<div class="m2"><p>زان روعرق شرم به دامان نشو قبا گل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از زخم زبان است نشاط دل افگار</p></div>
<div class="m2"><p>در دامن خاشاک کند نشو و نما گل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسن از نظر پاک محابا ننماید</p></div>
<div class="m2"><p>ازدیده شبنم نکند شرم و حیا گل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگشا به شکر خنده لب خویش که باشد</p></div>
<div class="m2"><p>درمرتبه غنچگی انگشت نما گل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشم نگران است سراپای ز شبنم</p></div>
<div class="m2"><p>تا زان رخ گلرنگ کند کسب صفا گل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رنگین سخنان درسخن خویش نهادنند</p></div>
<div class="m2"><p>از نکهت خود نیست به هر حال جدا گل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دلتنگی جاوید نگهبانی عمرست</p></div>
<div class="m2"><p>از خنده خود رفت به تاراج فنا گل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از پاکی عشق است که در پرده شبها</p></div>
<div class="m2"><p>در خواب رود مست به زیر پرما گل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با نیک و بد خلق بود لطف تو یکسان</p></div>
<div class="m2"><p>خندد به یک آیین به رخ شاه و گدا گل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صائب ز نواسنجی ما غنچه شد آن شوخ</p></div>
<div class="m2"><p>هر چند که خندان شود از باد صبا گل</p></div></div>