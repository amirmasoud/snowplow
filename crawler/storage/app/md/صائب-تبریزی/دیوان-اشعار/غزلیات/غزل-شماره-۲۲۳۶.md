---
title: >-
    غزل شمارهٔ ۲۲۳۶
---
# غزل شمارهٔ ۲۲۳۶

<div class="b" id="bn1"><div class="m1"><p>شراب نامرادی بی خمارست</p></div>
<div class="m2"><p>به قدر تلخی این می خوشگوارست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جواب خشک ازان لبهای سیراب</p></div>
<div class="m2"><p>به کشت عاشقان ابر بهارست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازان چشم تو رنجورست دایم</p></div>
<div class="m2"><p>که هم بیمار و هم بیماردارست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز چشم یار قانع شو به دیدن</p></div>
<div class="m2"><p>که پرسش بر دل بیمار بارست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمی خیزد سپند از جا ز حیرت</p></div>
<div class="m2"><p>در آن محفل که آن آتش عذارست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبا را منفعل دارد ز جولان</p></div>
<div class="m2"><p>اگر چه بوی گل دامن سوارست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بود لازم غضب را دل سیاهی</p></div>
<div class="m2"><p>پلنگ از خشم، دایم داغدارست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وصال آفتاب عالم افروز</p></div>
<div class="m2"><p>نصیب شبنم شب زنده دارست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به نرمی کن زبان خصم کوتاه</p></div>
<div class="m2"><p>که عاجز از نمد، دندان مارست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گذشتن مشکل است از سینه صافان</p></div>
<div class="m2"><p>که در گل پای سرو از جویبارست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>محک را از سیه رویی برآرد</p></div>
<div class="m2"><p>زر سرخی که کامل در عیارست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رخ مقصود بی پرده است صائب</p></div>
<div class="m2"><p>اگر آیینه دل بی غبارست</p></div></div>