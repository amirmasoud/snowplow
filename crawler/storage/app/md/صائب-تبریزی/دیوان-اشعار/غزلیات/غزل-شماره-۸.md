---
title: >-
    غزل شمارهٔ ۸
---
# غزل شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>گر چه باشند آن دو زلف مشکبار از هم جدا</p></div>
<div class="m2"><p>نیستند اما به وقت گیر و دار از هم جدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مستی و مخموری از هم گر چه دور افتاده اند</p></div>
<div class="m2"><p>نیست در چشم تو مستی و خمار از هم جدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لرزد از بیم جدایی استخوانم بند بند</p></div>
<div class="m2"><p>هر کجا بینم فلک سازد دو یار از هم جدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشأه و می را نماید با کمال اتحاد</p></div>
<div class="m2"><p>از نگاهی چشم شور روزگار از هم جدا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک دل صد پاره آید عارفان را در نظر</p></div>
<div class="m2"><p>گر چه باشد برگ برگ لاله زار از هم جدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر به یک جا می گذارد این دو راه مختلف</p></div>
<div class="m2"><p>می نماید گر به صورت زلف یار از هم جدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>متحد گردند با هم، چشم چون بر هم نهند</p></div>
<div class="m2"><p>هست اگر جان های روشن چون شرار از هم جدا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از دل روشن، علایق را شود پیوند سست</p></div>
<div class="m2"><p>ماه می سازد کتان را پود و تار از هم جدا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چند باشیم از حجاب عشق و استغنای حسن</p></div>
<div class="m2"><p>در ته یک پیرهن، ما و نگار از هم جدا؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آشنایی های ظاهر، پرده بیگانگی است</p></div>
<div class="m2"><p>آب و روغن هست در یک جویبار از هم جدا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غافلی از پشت و روی کار صائب، ور نه نیست</p></div>
<div class="m2"><p>چون گل رعنا، خزان و نوبهار از هم جدا</p></div></div>