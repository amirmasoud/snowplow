---
title: >-
    غزل شمارهٔ ۱۶۹۱
---
# غزل شمارهٔ ۱۶۹۱

<div class="b" id="bn1"><div class="m1"><p>منم که دام بلایم رهایی قفس است</p></div>
<div class="m2"><p>وداع زندگیم در جدایی قفس است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمی توان به زر گل مرا به دام آورد</p></div>
<div class="m2"><p>ز بیضه مرغ دل مرا هوایی قفس است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هنوز در گره غنچه است نکهت گل</p></div>
<div class="m2"><p>چه وقت چاک گریبان گشایی قفس است؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مقیدان همه از تنگی قفس نالند</p></div>
<div class="m2"><p>منم که ناله ام از دلگشایی قفس است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز چوب خشک مگویید گل نمی روید</p></div>
<div class="m2"><p>شکست بال، گل آشنایی قفس است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو کعبه گرد قفس طوف می کند شب و روز</p></div>
<div class="m2"><p>دگر چه چون دل صائب فدایی قفس است؟</p></div></div>