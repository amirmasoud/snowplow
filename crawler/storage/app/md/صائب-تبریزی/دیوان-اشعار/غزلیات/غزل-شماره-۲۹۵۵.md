---
title: >-
    غزل شمارهٔ ۲۹۵۵
---
# غزل شمارهٔ ۲۹۵۵

<div class="b" id="bn1"><div class="m1"><p>دل بیمار من ناز مداوا برنمی دارد</p></div>
<div class="m2"><p>گرانی از دم جان بخش عیسی بر نمی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نماند از خون دل چندان که مژگانی کنم رنگین</p></div>
<div class="m2"><p>همان دست از دل آن مژگان گیرا برنمی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر با خار دیوارش نظر بازی کنم، ورنه</p></div>
<div class="m2"><p>گل این بوستان بار تماشا برنمی دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مبادا هیچ کافر را الهی خصم کم فرصت!</p></div>
<div class="m2"><p>به ترک سرفلک دست از سرما برنمی دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به می اصلاح سودا می کنم هر چند می دانم</p></div>
<div class="m2"><p>که خامی را زعنبر جوش دریا برنمی دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز نامردان به مردان زال دنیا بیشتر پیچد</p></div>
<div class="m2"><p>که دست از دامن یوسف زلیخا بر نمی دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدان هرگز نمی گردند خوب از صحبت نیکان</p></div>
<div class="m2"><p>ز سوزن تنگ چشمی قرب عیسی بر نمی دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وطن هر چند دلگیرست بر غربت شرف دارد</p></div>
<div class="m2"><p>به آهن، دل شرار از سنگ خارا بر نمی دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو می اندیشی از خار ملامت، ورنه صاحبدل</p></div>
<div class="m2"><p>نیارد در نظر تا خار را پا بر نمی دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگرچه دامن ما بر فلک چون ابر می ساید</p></div>
<div class="m2"><p>همان خار علایق دست از ما برنمی دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به هر نقش و نگاری کی مقید می شود صائب؟</p></div>
<div class="m2"><p>دلی کز سرکشی عبرت ز دنیا برنمی دارد</p></div></div>