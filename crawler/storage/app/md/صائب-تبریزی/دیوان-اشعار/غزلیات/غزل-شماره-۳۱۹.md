---
title: >-
    غزل شمارهٔ ۳۱۹
---
# غزل شمارهٔ ۳۱۹

<div class="b" id="bn1"><div class="m1"><p>دگر با نوخطی دارد دل من در میان سودا</p></div>
<div class="m2"><p>که دارد در گره هر موی خطش یک جهان سودا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غبار استخوانم سرمه چشم غزالان شد</p></div>
<div class="m2"><p>نمی پیچد سر از سنگ ملامت همچنان سودا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که جز دیوانه من، سایه بید سلامت را</p></div>
<div class="m2"><p>به رغبت می کند با زخم شمشیر زبان سودا؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی صد شد ز سرو خوش خرام او جنون من</p></div>
<div class="m2"><p>چه حرف است این که کم می گردد از آب روان سودا؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غزالان را به مجنون مهربان دیدم، یقینم شد</p></div>
<div class="m2"><p>که وحشت می برد بیرون ز طبع وحشیان سودا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر باید به دشمن رایگان دادن متاع خود</p></div>
<div class="m2"><p>مکن زنهار تا ممکن بود با دوستان سودا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>متاع شیشه‌جانان را دلی از سنگ می‌باید</p></div>
<div class="m2"><p>از آن دیوانه دایم می‌کند با کودکان سودا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز سوز تشنگی هر چند دارد رنگ خاکستر</p></div>
<div class="m2"><p>درون پرده دارد چشمه حیوان نهان سودا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهار خرمی در پوست دارد نخل بی برگش</p></div>
<div class="m2"><p>به ظاهر گر چه افسرده است در فصل خزان سودا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر می داشت مغزی دولت دنیای بی حاصل</p></div>
<div class="m2"><p>همامی کرد هرگز سایه را با استخوان سودا؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مکش منت ز دست چرب این سنگین دلان صائب</p></div>
<div class="m2"><p>که روغن می کشد از دانه ریگ روان سودا</p></div></div>