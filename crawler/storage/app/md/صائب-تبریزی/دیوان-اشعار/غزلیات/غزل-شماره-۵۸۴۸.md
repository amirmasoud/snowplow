---
title: >-
    غزل شمارهٔ ۵۸۴۸
---
# غزل شمارهٔ ۵۸۴۸

<div class="b" id="bn1"><div class="m1"><p>ما توبه را به طاعت پیمانه برده ایم</p></div>
<div class="m2"><p>محراب را به سجده بتخانه برده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ابروی قبله در گره سبحه گم شده است</p></div>
<div class="m2"><p>تا رخت خود ز کعبه به بتخانه برده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آیینه شکسته تجلی پذیر نیست</p></div>
<div class="m2"><p>دل را عبث برابر جانانه برده ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خمها چو فیل مست سر خود گرفته اند</p></div>
<div class="m2"><p>از بس که دردسر سوی میخانه برده ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان خرمنی که خوشه پروین در او گم است</p></div>
<div class="m2"><p>روزی مور باد اگر دانه برده ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صائب به زور بازوی طبع بلند خویش</p></div>
<div class="m2"><p>گوی سخن ز عرصه دلیرانه برده ایم</p></div></div>