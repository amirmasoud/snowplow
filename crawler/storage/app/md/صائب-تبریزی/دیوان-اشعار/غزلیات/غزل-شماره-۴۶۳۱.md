---
title: >-
    غزل شمارهٔ ۴۶۳۱
---
# غزل شمارهٔ ۴۶۳۱

<div class="b" id="bn1"><div class="m1"><p>می شود طی در ورق گرداندنی دیوان عمر</p></div>
<div class="m2"><p>داغ دارد شعله جواله را دوران عمر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از نسیمی می شود زیر وزبر شیرازه اش</p></div>
<div class="m2"><p>چون قلم گردیده ام صدبار بر دیوان عمر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هست بر چرخ مقوس جلوه تیر شهاب</p></div>
<div class="m2"><p>در زمین طینت ما خاکیان جولان عمر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقطه آغاز باشد چون شرر انجام او</p></div>
<div class="m2"><p>دل منه چون غافلان بر چهره خندان عمر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چه از قسمت بود صدسال بر فرض محال</p></div>
<div class="m2"><p>طی به یک تسبیح گرداندن شود دوران عمر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مانع است از سبز گردیدن روانی آب را</p></div>
<div class="m2"><p>ترمکن چون خضر لب از چشمه حیوان عمر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>موجه ریگ روان را نعل در آتش بود</p></div>
<div class="m2"><p>ساده لوح آن کس که می پیچد به کف دامان عمر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در جوانی خاکساری پیشه کن آسوده شو</p></div>
<div class="m2"><p>برزمین چون نقش خواهی بست درپایان عمر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست جز خون روزی اطفال صائب در رحم</p></div>
<div class="m2"><p>می توان بردن به پایان راه از عنوان عمر</p></div></div>