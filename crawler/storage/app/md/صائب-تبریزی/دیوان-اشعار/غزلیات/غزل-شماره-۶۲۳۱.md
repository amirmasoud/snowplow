---
title: >-
    غزل شمارهٔ ۶۲۳۱
---
# غزل شمارهٔ ۶۲۳۱

<div class="b" id="bn1"><div class="m1"><p>سبک جولانتر از برق است حسن لاله زار من</p></div>
<div class="m2"><p>به یک خمیازه گل می شود آخر بهار من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر شبها خبر یابی ز درد انتظار من</p></div>
<div class="m2"><p>ز خواب ناز رو ناشسته آیی در کنار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندارد حسن و عشق از هم جدایی، سخت می ترسم</p></div>
<div class="m2"><p>که در پیراهن گل خار ریزد خار خار من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه در دست است گیرایی، نه در آغوش گنجایی</p></div>
<div class="m2"><p>عبث پهلو تهی می سازد آن سرو از کنار من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آب چشم شبنم دامن گلها نمازی شد</p></div>
<div class="m2"><p>مگردان روی زنهار از دو چشم اشکبار من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمی پیچم سر از سنگ ملامت، عاشقم عاشق</p></div>
<div class="m2"><p>محک را سرخ رو دارد زر کامل عیار من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ندارم هیچ پروا گر ببازم هر دو عالم را</p></div>
<div class="m2"><p>پشیمانی بود خصل نخستین قمار من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر لنگر نیندازد به خاکم سایه قاتل</p></div>
<div class="m2"><p>تپیدن در فلاخن می نهد سنگ مزار من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دوانیده است از بس ریشه خشکی در گلستانم</p></div>
<div class="m2"><p>به جای سرو خیزد گردباد از جویبار من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا افسرده دارد سردی این خاکدان، ورنه</p></div>
<div class="m2"><p>ز شوخی بیستون را می کند از جا شرار من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ندارد همچو من دیوانه ای دامان این صحرا</p></div>
<div class="m2"><p>غزالان می جهند از خواب از ذوق شکار من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به آب از اشک شادی می رسانم خانه زین را</p></div>
<div class="m2"><p>اگر افتد به دست من عنان شهسوار من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من آن رنگین نوامرغم درین بستانسرا صائب</p></div>
<div class="m2"><p>که چشم شبنم گل می پرد از انتظار من</p></div></div>