---
title: >-
    غزل شمارهٔ ۶۰۷۶
---
# غزل شمارهٔ ۶۰۷۶

<div class="b" id="bn1"><div class="m1"><p>ساقیا صبح است می از شیشه در پیمانه کن</p></div>
<div class="m2"><p>حشر خواب آلودگان از نعره مستانه کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجلس از دود چراغ کشته ماتمخانه ای است</p></div>
<div class="m2"><p>این مصیبت خانه را از باده عشرتخانه کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بت پندار زناری است هر مو بر تنم</p></div>
<div class="m2"><p>تیشه مردانه ای در کار این بتخانه کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرمه سایی می کند در مغزها دود خمار</p></div>
<div class="m2"><p>این جهان تیره را روشن به یک پیمانه کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چهره گلگون برافروز از شراب آتشین</p></div>
<div class="m2"><p>برگ برگ این چمن را بلبل و پروانه کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساغری لبریز کن از باده اندیشه سوز</p></div>
<div class="m2"><p>هر که دعوای خردمندی کند دیوانه کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می رود فیض صبوح از دست تا دم می زنی</p></div>
<div class="m2"><p>پیش این دریای رحمت دست را پیمانه کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در جهان بیخودی هوش و خرد بیگانه است</p></div>
<div class="m2"><p>صاف ملک خویش را از لشکر بیگانه کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کلک صائب پرده از کار جهان برداشته است</p></div>
<div class="m2"><p>ساغر مردافکنی در کار این دیوانه کن</p></div></div>