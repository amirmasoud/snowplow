---
title: >-
    غزل شمارهٔ ۳۴۵۸
---
# غزل شمارهٔ ۳۴۵۸

<div class="b" id="bn1"><div class="m1"><p>کی دل سالک سرگشته بجا می باشد؟</p></div>
<div class="m2"><p>حرکت لازمه قبله نما می باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده عالمی از خواب، دم صبح گشود</p></div>
<div class="m2"><p>نفس صافدلان عقده گشا می باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر غمی هست به انجام رسد در دو سه روز</p></div>
<div class="m2"><p>غم روزی است که سی روز به جا می باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرده فقر دریدن گل بیحوصلگی است</p></div>
<div class="m2"><p>خرقه ما چو زره زیر قبا می باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشقان را نبود در جگر سوخته آه</p></div>
<div class="m2"><p>دانه سوخته بی نشو و نما می باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکه خود را به تمامی شکند شهره شود</p></div>
<div class="m2"><p>مه چو باریک شد انگشت نما می باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیروان را نگرانی نبود از دنبال</p></div>
<div class="m2"><p>دیده راهنمایان به قفا می باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از خودی در ته سنگ است ترا پا، ورنه</p></div>
<div class="m2"><p>هرکه از خویش برآمد همه جا می باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خون کند در جگر تیغ حوادث صائب</p></div>
<div class="m2"><p>عاجزی را که سپر دست دعا می باشد</p></div></div>