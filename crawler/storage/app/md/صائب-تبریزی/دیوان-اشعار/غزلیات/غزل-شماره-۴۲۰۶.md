---
title: >-
    غزل شمارهٔ ۴۲۰۶
---
# غزل شمارهٔ ۴۲۰۶

<div class="b" id="bn1"><div class="m1"><p>زاهد هوای عالم بالا نمی کند</p></div>
<div class="m2"><p>این رود خشک روی به دریا نمی کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسوده است زاهد خشک از فشار عشق</p></div>
<div class="m2"><p>شهباز قصد سینه صحرا نمی کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در رستخیز رو به قفا حشر می شود</p></div>
<div class="m2"><p>اینجا کسی که پشت به دنیا نمی کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نتوان به کوه غم دل ما را شکست داد</p></div>
<div class="m2"><p>از فیل مست کعبه محابا نمی کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اینجا اگر به دانه نبندی دهان مور</p></div>
<div class="m2"><p>در زیر خاک با تو مدارا نمی کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیهوده دست بر دل ما می نهد طبیب</p></div>
<div class="m2"><p>لنگر علاج شورش دریا نمی کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درد سخن همان به سخن می شود علاج</p></div>
<div class="m2"><p>این درد را مسیح مداوا نمی کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مریم به رشته ای که بتابد ز مهر خویش</p></div>
<div class="m2"><p>از آسمان شکار مسیحا نمی کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در سایه حمایت عشق است جان ما</p></div>
<div class="m2"><p>ما را زبون خود غم دنیا نمی کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جز ناخن شکسته و آه جگرخراش</p></div>
<div class="m2"><p>از کار ما گره دگری وا نمی کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب غبار سینه مشکل پسند ماست</p></div>
<div class="m2"><p>داغی که کار دیده بینا نمی کند</p></div></div>