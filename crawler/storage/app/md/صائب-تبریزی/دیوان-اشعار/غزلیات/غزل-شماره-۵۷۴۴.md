---
title: >-
    غزل شمارهٔ ۵۷۴۴
---
# غزل شمارهٔ ۵۷۴۴

<div class="b" id="bn1"><div class="m1"><p>اگر چه با گل دمساز می شود شبنم</p></div>
<div class="m2"><p>چو صبح شد به فلک بار می شود شبنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین حدیقه زنگار گون نمی ماند</p></div>
<div class="m2"><p>به وصل مهر سرافراز می شود شبنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر ز دامن گل تکیه گاه سازندش</p></div>
<div class="m2"><p>چو بوی گل به هوا باز می شود شبنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درون دیده خورشید جای خود دیده است</p></div>
<div class="m2"><p>که زود خانه برانداز می شود شبنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صفای دل به کف آور کز این ره روشن</p></div>
<div class="m2"><p>سبک به عالم آغاز می شود شبنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز جمع کردن دامن بود ز هر خس و خار</p></div>
<div class="m2"><p>که بر فلک به یک انداز می شود شبنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سحر به سیر چمن رو که چون هوا شد گرم</p></div>
<div class="m2"><p>نهفته چون گهر راز می شود شبنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز پرده خیرگی عشق چون برون آید</p></div>
<div class="m2"><p>به آفتاب نظرباز می شود شبنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه پابه دامن غفلت کشیده ای صائب</p></div>
<div class="m2"><p>قرین مهر به پرواز می شود شبنم</p></div></div>