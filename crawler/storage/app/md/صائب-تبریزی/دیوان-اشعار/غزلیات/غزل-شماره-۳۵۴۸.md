---
title: >-
    غزل شمارهٔ ۳۵۴۸
---
# غزل شمارهٔ ۳۵۴۸

<div class="b" id="bn1"><div class="m1"><p>هرچه دریافت کلیم از نظر بینا بود</p></div>
<div class="m2"><p>کف این بحر گهرخیز ید بیضا بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نرسیدیم به جایی که ز پا بنشینیم</p></div>
<div class="m2"><p>ساحل خار و خس ما کف این دریا بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در فضایی که دل از تنگی جا می نالید</p></div>
<div class="m2"><p>آسمان یک گره خاطر آن صحرا بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیشتر نوسفران طالع شهرت دارند</p></div>
<div class="m2"><p>ورنه آوارگی ما، چه کم از عنقا بود؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک سر تیر ز ما سایه جدا می گردید</p></div>
<div class="m2"><p>روزگاری که دل وحشی ما با ما بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش ازان دم که رسد بحر به شیرازه موج</p></div>
<div class="m2"><p>صف مژگان تو در دیده خونپالا بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در غم این شادی ناآمده را می دیدیم</p></div>
<div class="m2"><p>چهره صبح ز زلف شب ما پیدا بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حسن یک جلوه مستانه درین بزم نکرد</p></div>
<div class="m2"><p>تنگی حوصله ها مهر لب مینا بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نرسیدیم به پروانه راحت صائب</p></div>
<div class="m2"><p>خط آزادی ما نقش پر عنقا بود</p></div></div>