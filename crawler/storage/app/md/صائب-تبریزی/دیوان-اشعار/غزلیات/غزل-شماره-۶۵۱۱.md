---
title: >-
    غزل شمارهٔ ۶۵۱۱
---
# غزل شمارهٔ ۶۵۱۱

<div class="b" id="bn1"><div class="m1"><p>کی دوبین می شود از سایه تماشایی سرو؟</p></div>
<div class="m2"><p>هر سیه رو نشود پرده یکتایی سرو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشود دیده حق بین دودل از کثرت خلق</p></div>
<div class="m2"><p>چه خلل می رسد از برگ به یکتایی سرو؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حسن گلهای چمن پا به رکاب است تمام</p></div>
<div class="m2"><p>پای برجاست مخلد چمن آرایی سرو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سرافرازی حسن است نه از کوتاهی</p></div>
<div class="m2"><p>به زمین گر نکشد دامن رعنایی سرو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دو سه روزی است نظربازی بلبل با گل</p></div>
<div class="m2"><p>در خزان سبز بود بخت تماشایی سرو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سفر عالم بالا به قدم نتوان کرد</p></div>
<div class="m2"><p>مانع نشو و نما نیست گران پایی سرو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زان مبدل نکند جامه خود را هرگز</p></div>
<div class="m2"><p>کز تن خویش بود جامه زیبایی سرو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دست آزاده و دریوزه حاجت، هیهات</p></div>
<div class="m2"><p>برنیاید ز بغل پنجه گیرایی سرو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می شود دیده ور از فیض نظربازان حسن</p></div>
<div class="m2"><p>قمری از طوق بود دیده بینایی سرو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل آزاده نگردد ز گرفتاران باز</p></div>
<div class="m2"><p>کم ز قمری نشود وحشت تنهایی سرو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>راستی پیشه خود کن که بود سبز مدام</p></div>
<div class="m2"><p>مجلس افروزی شمع و چمن آرایی سرو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نفس سرد خزان باد بهارست او را</p></div>
<div class="m2"><p>نیست در باغ نهالی به شکیبایی سرو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر زند با قد او لاف رعونت، از آه</p></div>
<div class="m2"><p>می کنم فاخته ای جامه مینایی سرو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آسمان در نظر همت مردان پست است</p></div>
<div class="m2"><p>نرسد سبزه خوابیده به رعنایی سرو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کاهلان سنگ ره گرمروان می گردند</p></div>
<div class="m2"><p>که زمین گیر شود آب ز همپایی سرو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صائب از عالم بالا به تو فیضی نرسد</p></div>
<div class="m2"><p>تا چو قمری نشوی واله و شیدایی سرو</p></div></div>