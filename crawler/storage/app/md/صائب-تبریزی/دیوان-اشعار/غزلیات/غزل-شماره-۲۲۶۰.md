---
title: >-
    غزل شمارهٔ ۲۲۶۰
---
# غزل شمارهٔ ۲۲۶۰

<div class="b" id="bn1"><div class="m1"><p>مرهم کافور خلق پرده صد نشترست</p></div>
<div class="m2"><p>صندل این ناکسان گرده دردسرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست جدایی ز هم حلقه زنجیر را</p></div>
<div class="m2"><p>حادثه روزگار از پی یکدیگرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرم عنانان شوق زیر فلک نیستند</p></div>
<div class="m2"><p>اخگر افسرده را خاک سیه بر سرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی نظر اعتبار پرده خواب است چشم</p></div>
<div class="m2"><p>بی سخن حق نفس رشته بی گوهرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غنچه امید را، قفل دل تنگ را</p></div>
<div class="m2"><p>هست کلیدی اگر، در بغل محشرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم و در سیر را، نیست به نعمت نیاز</p></div>
<div class="m2"><p>کاسه ما فربه است، کیسه اگر لاغرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست به می احتیاج حسن گلوسوز را</p></div>
<div class="m2"><p>آینه بی غبار دشمن روشنگرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میکده باغ بهشت، کوثر او جام می</p></div>
<div class="m2"><p>ساقی شمشاد قد، سرو لب کوثرست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل ز هوس پاک کن، فیض گشایش ببین</p></div>
<div class="m2"><p>هر چه درون دل است، قفل برون درست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تن به حوادث گذار صائب اگر پخته ای</p></div>
<div class="m2"><p>کآبله چون پخته شد روزی او نشترست</p></div></div>