---
title: >-
    غزل شمارهٔ ۳۷۵۶
---
# غزل شمارهٔ ۳۷۵۶

<div class="b" id="bn1"><div class="m1"><p>به گرد تربت روشندلان دلیر مگرد</p></div>
<div class="m2"><p>که ابر، سینه خورشید را نسازد سرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جریده شو که رسد پیشتر به صید مراد</p></div>
<div class="m2"><p>شود چو تیر ز همصحبتان ترکش فرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خوردن دل خود از نصیب قانع شو</p></div>
<div class="m2"><p>که آب و نان جهان مرد را کند نامرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خار راه پر و بال می دهد سامان</p></div>
<div class="m2"><p>چو گردباد شود رهروی که تنهاگرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جای خون ز رگ و ریشه اش برآید دود</p></div>
<div class="m2"><p>اگر چنین دل پرخون من فشارد درد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه حاجت است به شمشیر، تیزدستان را؟</p></div>
<div class="m2"><p>که هست در کف دشمن مرا سلاح نبرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز اهل درد مس من طلای خالص شد</p></div>
<div class="m2"><p>که کیمیای وجودست دیدن رخ زرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به سرکشی مشو از خصم خاکسار ایمن</p></div>
<div class="m2"><p>که خط برآورد از روی همچو آتش گرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر چه دیر به جوش آمدم به این شادم</p></div>
<div class="m2"><p>که هرچه دیر شود گرم، دیر گردد سرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز ماه چهره آفاق گشت مهتابی</p></div>
<div class="m2"><p>که از طمع نشود رنگ هیچ کافر زرد!</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عجب که رخنه کند عیش در دل صائب</p></div>
<div class="m2"><p>که داغ بر سر داغ است و درد بر سر درد</p></div></div>