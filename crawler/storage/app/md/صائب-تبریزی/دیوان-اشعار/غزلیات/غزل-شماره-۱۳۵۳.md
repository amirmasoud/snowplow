---
title: >-
    غزل شمارهٔ ۱۳۵۳
---
# غزل شمارهٔ ۱۳۵۳

<div class="b" id="bn1"><div class="m1"><p>در دلم هرگاه زلف آن پری پیکر گذشت</p></div>
<div class="m2"><p>از سر دریای چشم موجه عنبر گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر مجنون اگر کردند مرغان آشیان</p></div>
<div class="m2"><p>مرغ نتواند ز سوز دل مرا بر سر گذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از در دل می توان کام دو عالم یافتن</p></div>
<div class="m2"><p>در به در افتاد هر کس بی خبر زین در گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوهر سیراب در گنجینه اقبال نیست</p></div>
<div class="m2"><p>با دهان خشک ازین غمخانه اسکندر گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خشک مغزی لازم زندان گردون است و بس</p></div>
<div class="m2"><p>می شود ریحان تر، دودی کز این مجمر گذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کم نگردد برگ عیش از خانه اش در برگریز</p></div>
<div class="m2"><p>هر که ایام بهارش زیر بال و پر گذشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم از حال دل پر خون کنم حرفی رقم</p></div>
<div class="m2"><p>تا قلم برداشتم یک نیزه خون از سر گذشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر نباشد از علایق بال همت زیر سنگ</p></div>
<div class="m2"><p>می توان چون موج ازین دریای بی لنگر گذشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از تکلف نفس قانع تلخکامی می کشد</p></div>
<div class="m2"><p>شکرستان شد زمین تا مور از شکر گذشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ترک افسر با وجود فقر چندان کار نیست</p></div>
<div class="m2"><p>از حباب آسان توان در بحر پر گوهر گذشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خوشه دادن در عوض خرمن گرفتن سهل نیست</p></div>
<div class="m2"><p>وقع شمعی خوش که پیش آفتاب از سر گذشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آرزو چون سوخت در دل حرص را عاجز کند</p></div>
<div class="m2"><p>مور هیهات است بتواند ز خاکستر گذشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در خرابات جهان چون آفتاب بی زوال</p></div>
<div class="m2"><p>روزگار خوشدلی ما را به یک ساغر گذشت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بستگی بعد از گشایش نیست بر خاطر گران</p></div>
<div class="m2"><p>از خدا خواهد گره، چون رشته از گوهر گذشت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نیست صائب هیچ گردی بر دل روشن مرا</p></div>
<div class="m2"><p>گر چه عمر اخگر من زیر خاکستر گذشت</p></div></div>