---
title: >-
    غزل شمارهٔ ۲۱۴۷
---
# غزل شمارهٔ ۲۱۴۷

<div class="b" id="bn1"><div class="m1"><p>آن خانه برانداز که در خانه زین است</p></div>
<div class="m2"><p>معمار تمنای من خاک نشین است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از شوخی حسن است که آن سرو خرامان</p></div>
<div class="m2"><p>بر روی زمین است و نه بر روی زمین است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اوراق گل از خنده بیجاست پریشان</p></div>
<div class="m2"><p>شیرازه مجموعه دل چین جبین است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسیار شود مرکز سرگشتگی خلق</p></div>
<div class="m2"><p>خالی که در آن کنج دهن گوشه نشین است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون خامه صیاد، متاعش همه مکرست</p></div>
<div class="m2"><p>هر بوته خاری که درین شوره زمین است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سوختگان نیست تهی کوی خرابات</p></div>
<div class="m2"><p>دایم سر این چشمه، سیه خانه نشین است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی مرگ نخوابد قدم سعی حریصان</p></div>
<div class="m2"><p>آسایش این طایفه در زیر زمین است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در انجمن وصل، شکایت مزه دارد</p></div>
<div class="m2"><p>در دامن گل گریه شبنم نمکین است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما قدرت دریوزه دیدار نداریم</p></div>
<div class="m2"><p>این سلسله جنبانی ازان چین جبین است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دارد سر ویرانی من پشته سواری</p></div>
<div class="m2"><p>کز شوخی او زلزله در خانه زین است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب چه سر از چاک گریبان بدر آرد؟</p></div>
<div class="m2"><p>امنیت اگر هست درین حصن حصین است</p></div></div>