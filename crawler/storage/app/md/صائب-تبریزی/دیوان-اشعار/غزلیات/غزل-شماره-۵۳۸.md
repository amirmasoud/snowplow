---
title: >-
    غزل شمارهٔ ۵۳۸
---
# غزل شمارهٔ ۵۳۸

<div class="b" id="bn1"><div class="m1"><p>دل سیه شد ز سیه خانه افلاک مرا</p></div>
<div class="m2"><p>کی بود آینه زین زنگ شود پاک مرا؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گره آن روز شود باز چو شبنم ز دلم</p></div>
<div class="m2"><p>که به خورشید رساند نظر پاک مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقده تاک فزون می شود از گریه تاک</p></div>
<div class="m2"><p>مگر از آه گشاید دل غمناک مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون جرس گر چه نیاسود زبانم ز فغان</p></div>
<div class="m2"><p>نشد از ناله تهی، سینه صد چاک مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا سرم گرم ز میخانه وحدت شده است</p></div>
<div class="m2"><p>گردش جام بود گردش افلاک مرا</p></div></div>