---
title: >-
    غزل شمارهٔ ۸۰۵
---
# غزل شمارهٔ ۸۰۵

<div class="b" id="bn1"><div class="m1"><p>از بدگهری می شکند گوهر رز را</p></div>
<div class="m2"><p>در دل چه گره هاست ز زاهد بر رز را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حاشا که گذارد کرم ساقی کوثر</p></div>
<div class="m2"><p>در گلشن فردوس ملامتگر رز را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک دانه انگور به زاهد مچشانید</p></div>
<div class="m2"><p>حیف است فکندن به وبال اختر رز را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای شیشه می چند دهن بسته نشینی؟</p></div>
<div class="m2"><p>با جام بکن عقد روان دختر رز را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صائب اگر از نشأه می چشم دهی آب</p></div>
<div class="m2"><p>از آب گهر سبز نمایی سر رز را</p></div></div>