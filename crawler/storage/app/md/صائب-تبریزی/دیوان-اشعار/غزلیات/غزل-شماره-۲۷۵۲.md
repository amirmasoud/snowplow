---
title: >-
    غزل شمارهٔ ۲۷۵۲
---
# غزل شمارهٔ ۲۷۵۲

<div class="b" id="bn1"><div class="m1"><p>وقت مجنون خوش که پا در دامن صحرا کشید</p></div>
<div class="m2"><p>در سواد اعظم چشم غزالان واکشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگذر از دریوزه دلها که از ارباب فقر</p></div>
<div class="m2"><p>آن توانگر شد که هویی بر در دلها کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سد راه عجز، ترک شیوه عاجزکشی است</p></div>
<div class="m2"><p>کور شد هرکس عصا از دست نابینا کشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ابر ما بر آب گوهر می فشاند آستین</p></div>
<div class="m2"><p>پرده تلخی چرا بر روی خود دریا کشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاتم از شوق تو اینجا می کند قالب تهی</p></div>
<div class="m2"><p>تا به کی ای لعل خواهی سختی ازخارا کشید؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>(چون نشوید باغبان از باغ دست تربیت؟</p></div>
<div class="m2"><p>آب شد سرو چمن چون سرو او بالا کشید)</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سنگ گردیده است از فولاد جوهردارتر</p></div>
<div class="m2"><p>تیشه من بس که ناخن بر رخ خارا کشید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کشتنی ارباب غیرت را بتر از عفو نیست</p></div>
<div class="m2"><p>دشمن از کوتاه بینی انتقام از ما کشید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از سواد خاک، صائب نقد آسایش مجوی</p></div>
<div class="m2"><p>این رقم دست قضا بر شهپر عنقا کشید</p></div></div>