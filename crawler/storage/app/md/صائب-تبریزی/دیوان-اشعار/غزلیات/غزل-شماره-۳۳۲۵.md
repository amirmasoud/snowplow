---
title: >-
    غزل شمارهٔ ۳۳۲۵
---
# غزل شمارهٔ ۳۳۲۵

<div class="b" id="bn1"><div class="m1"><p>پاس اندوه دل تنگ نگه می دارد</p></div>
<div class="m2"><p>شیشه ما طرف سنگ نگه می دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در زمان دل دیوانه من هر طفلی</p></div>
<div class="m2"><p>چون فلاخن به بغل سنگ نگه می دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلبل از سیر مقامات ندارد خبری</p></div>
<div class="m2"><p>عشق کی پرده آهنگ نگه می دارد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون گل از خنده پریشان شود اوراق حواس</p></div>
<div class="m2"><p>غنچه را جمع دل تنگ نگه می دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سنگ کم نیست کسی را که به میزان نظر</p></div>
<div class="m2"><p>چون گهر عزت هر سنگ نگه می دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غرض این است که غیری نکند در دل جای</p></div>
<div class="m2"><p>آن که ما را به دل تنگ نگه می دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیشه را از خطر سنگ حوادث صائب</p></div>
<div class="m2"><p>بیشتر باده گلرنگ نگه می دارد</p></div></div>