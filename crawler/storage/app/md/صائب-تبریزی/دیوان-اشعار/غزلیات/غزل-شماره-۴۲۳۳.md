---
title: >-
    غزل شمارهٔ ۴۲۳۳
---
# غزل شمارهٔ ۴۲۳۳

<div class="b" id="bn1"><div class="m1"><p>توفیق درد وداغ به هر دل نمی دهند</p></div>
<div class="m2"><p>این فیض را به هر دل غافل نمی دهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلبل گلوی خویش عبث پاره می کند</p></div>
<div class="m2"><p>این شوخ دیدگان به سخن دل نمی دهند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آزادگان تلاش شهادت نمی کنند</p></div>
<div class="m2"><p>تا خونبهای خویش به قاتل نمی دهند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از تلخی سوال گروهی که واقفند</p></div>
<div class="m2"><p>فرصت به لب گشودن سایل نمی دهند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیوانگی است قفل در رزق راکلید</p></div>
<div class="m2"><p>عاقل مشو که سنگ به عاقل نمی دهند</p></div></div>