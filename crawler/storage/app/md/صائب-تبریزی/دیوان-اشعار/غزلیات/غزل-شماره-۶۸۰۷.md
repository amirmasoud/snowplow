---
title: >-
    غزل شمارهٔ ۶۸۰۷
---
# غزل شمارهٔ ۶۸۰۷

<div class="b" id="bn1"><div class="m1"><p>نیست چون صبح ترا جز نفس معدودی</p></div>
<div class="m2"><p>چه کنی چون دل شب تیره اش از هر دودی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست سرمایه عمر تو به جز یک دو سه دم</p></div>
<div class="m2"><p>چه کنی صرف به دودی که ندارد سودی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دود اگر زلف ایازست ببر پیوندش</p></div>
<div class="m2"><p>حیف باشد که به زنجیر بود محمودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون سیاووش گذشتند ز آتش مردان</p></div>
<div class="m2"><p>ما به همت نتوانیم گذشت از دودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عیش خود تلخ مکن صائب ازین دود کثیف</p></div>
<div class="m2"><p>گر به آتش نگذاری به تکلف عودی</p></div></div>