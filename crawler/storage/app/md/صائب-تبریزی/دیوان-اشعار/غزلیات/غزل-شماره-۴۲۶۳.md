---
title: >-
    غزل شمارهٔ ۴۲۶۳
---
# غزل شمارهٔ ۴۲۶۳

<div class="b" id="bn1"><div class="m1"><p>از نور وحدت آن که دلش بهره ور شود</p></div>
<div class="m2"><p>کی از هجوم ذره پریشان نظر شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جایی که هفت پرده حجاب نظر نشد</p></div>
<div class="m2"><p>کی آسمان حجاب دل دیده ور شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رنگش ز آفتاب قیامت نمی پرد</p></div>
<div class="m2"><p>رخسار هر که لعل به خون جگر شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ته جرعه ای ز جسم گرانجان او بجاست</p></div>
<div class="m2"><p>آن را که از محیط کف پای تر شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طالع نگر که دیده من در حریم وصل</p></div>
<div class="m2"><p>از شرم عشق حلقه بیرون در شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر خار بی گلی گل بی خار می شود</p></div>
<div class="m2"><p>در راه سالکی که ز خود بیخبرشود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر برگ سبز دامن پر سنگ می شود</p></div>
<div class="m2"><p>روزی که نخل طالع ما بارور شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چندان که خط زیاده دهد گوشمال حسن</p></div>
<div class="m2"><p>صائب امیدواری من بیشتر شود</p></div></div>