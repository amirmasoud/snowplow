---
title: >-
    غزل شمارهٔ ۴۵۲۱
---
# غزل شمارهٔ ۴۵۲۱

<div class="b" id="bn1"><div class="m1"><p>از غم گم کرده راهان فارغ است</p></div>
<div class="m2"><p>هر که صائب شد به منزل ناپدید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شش جهت راه است ومنزل ناپدید</p></div>
<div class="m2"><p>دشت هموارست ومحمل ناپدید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز اشتیاق آب دریا ماهیان</p></div>
<div class="m2"><p>خاک می لیسند وساحل ناپدید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برگ برگ این گلستان همچوگل</p></div>
<div class="m2"><p>جمله تن گوشند وقایل ناپدید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد ز خون بی دریغ کشتگان</p></div>
<div class="m2"><p>خاک اطلس پوش وقاتل ناپدید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاقلان چون عهد دیوانگان</p></div>
<div class="m2"><p>جمله در بند وسلاسل ناپدید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در دل هر ذره آن خورشید هست</p></div>
<div class="m2"><p>شد ترا آیینه در گل ناپدید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رست چون پرگار از سرگشتگی</p></div>
<div class="m2"><p>هر که شد در نقطه دل ناپدید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می شمارند این گروه ساده لوح</p></div>
<div class="m2"><p>خویش را صاحبدل ودل ناپدید</p></div></div>