---
title: >-
    غزل شمارهٔ ۳۴۱۷
---
# غزل شمارهٔ ۳۴۱۷

<div class="b" id="bn1"><div class="m1"><p>کوهکن کیست به گرد من شیدا برسد؟</p></div>
<div class="m2"><p>جنبش قاف محال است به عنقا برسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جگر تشنه صحرای علایق ترسم</p></div>
<div class="m2"><p>سیل ما را نگذارد که به دریا برسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالمی همچو صدف چشم و دهن وا کرده است</p></div>
<div class="m2"><p>به که تا گوهر عبرت ز تماشا برسد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می گذراند کم نعمت باقی فردا</p></div>
<div class="m2"><p>هر چه اینجا به تو از نعمت دنیا برسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگز از کبر نکردی نگهی در ته پا</p></div>
<div class="m2"><p>به تو چون مایده فیض ز بالا برسد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناقص از تربیت چرخ نگردد کامل</p></div>
<div class="m2"><p>باده خام محال است به مینا برسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حیف و صد حیف که در دایره امکان نیست</p></div>
<div class="m2"><p>اهل دردی که به درد سخن ما برسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شهپر دامن عصمت به فلک می ساید</p></div>
<div class="m2"><p>پی یوسف چه خیال است زلیخا برسد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از کمندش نجهد هیچ شکاری صائب</p></div>
<div class="m2"><p>دست هرکس که به آن زلف چلیپا برسد</p></div></div>