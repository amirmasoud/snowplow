---
title: >-
    غزل شمارهٔ ۸۰۰
---
# غزل شمارهٔ ۸۰۰

<div class="b" id="bn1"><div class="m1"><p>از دست و تیغ عشق فگارند لاله‌ها</p></div>
<div class="m2"><p>در خاک و خون نشسته یارند لاله‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دیده بصیرت پروانه‌طینتان</p></div>
<div class="m2"><p>فانوس شمع چهره یارند لاله‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باشند همچو شعله جواله بی‌قرار</p></div>
<div class="m2"><p>بر خار و خس اگرچه سوارند لاله‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا سر کشیده‌اند، به پایان رسیده‌اند</p></div>
<div class="m2"><p>کم‌عمرتر ز شعله خارند لاله‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک نصف خون تازه و یک نصف مشک تر</p></div>
<div class="m2"><p>چون نافه غزال تتارند لاله‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آتشند و خنده مستانه می‌زنند</p></div>
<div class="m2"><p>با داغ دل، گشاده‌عذارند لاله‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در شیشه حسن باده لعلی عیان شود</p></div>
<div class="m2"><p>آیینه‌دار روی بهارند لاله‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در خون دهند غوطه تمنای بوسه را</p></div>
<div class="m2"><p>دست نگاربسته یارند لاله‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زان هرگز از خمار نگردند زردروی</p></div>
<div class="m2"><p>کز خون خویش باده‌گسارند لاله‌ها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کردند خون خود به تماشاییان حلال</p></div>
<div class="m2"><p>از سرگذشتگان بهارند لاله‌ها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با چهره شکفته آن آتشین‌عذار</p></div>
<div class="m2"><p>دل‌مرده‌تر ز شمع مزارند لاله‌ها</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با نور آفتاب چه باشد فروغ شمع؟</p></div>
<div class="m2"><p>با روی یار، در چه شمارند لاله‌ها</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صائب ز خون خود می گلرنگ می‌خورند</p></div>
<div class="m2"><p>زان ایمن از گزند خمارند لاله‌ها</p></div></div>