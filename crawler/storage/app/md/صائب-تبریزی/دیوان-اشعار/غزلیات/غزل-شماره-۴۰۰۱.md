---
title: >-
    غزل شمارهٔ ۴۰۰۱
---
# غزل شمارهٔ ۴۰۰۱

<div class="b" id="bn1"><div class="m1"><p>ز راه صلح مهیای جنگ می آید</p></div>
<div class="m2"><p>ز مومیایی او کار سنک می آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امید رحم بود کفر ازان خدا ناترس</p></div>
<div class="m2"><p>که گر به کعبه رود از فرنگ می آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز شیشه بال پریزاد اگر شکسته شود</p></div>
<div class="m2"><p>خیال یار هم از دل به تنگ می آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غبار آه ز دل می شود بلند مرا</p></div>
<div class="m2"><p>به شیشه دل هر کس که سنگ می آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به چار بالش خاراست چون شرر جایم</p></div>
<div class="m2"><p>ز بس که بر من از اطراف سنگ می آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قد خمیده مرا شد به راه راست دلیل</p></div>
<div class="m2"><p>به صیقل آینه بیرون ز رنگ می آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنان به عهد تو شد عام دردمندیها</p></div>
<div class="m2"><p>که بوی درد ز داغ پلنگ می آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خیال روی تو هم می رود ز دل بیرون</p></div>
<div class="m2"><p>برون ز گوهر اگر آب ورنگ می آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز آسمان مقوس ز بس کجی دیدم</p></div>
<div class="m2"><p>کمان به دیده من چون خدنگ می آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مگر که هست امید اجابتی صائب</p></div>
<div class="m2"><p>که آه بر لب من بی درنگ می آید</p></div></div>