---
title: >-
    غزل شمارهٔ ۶۵۰۲
---
# غزل شمارهٔ ۶۵۰۲

<div class="b" id="bn1"><div class="m1"><p>من آن بخت از کجا دارم که پیچیم بر میان تو</p></div>
<div class="m2"><p>بگردم چون خط شبرنگ بر گرد دهان تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من و اندیشه بر گرد سرگشتن، معاذالله</p></div>
<div class="m2"><p>که شادی مرگ می گردم چو بوسم آستان تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیال موشکافان سربرون ناورد از جایی</p></div>
<div class="m2"><p>مگر خط آورد بیرون سر از راز دهان تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>متاع یوسفی کز دیدنش شد چشم ها روشن</p></div>
<div class="m2"><p>به گرد بی نیازی می رود در کاروان تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سری دارد به ره گم کردگان وادی حیرت</p></div>
<div class="m2"><p>نمی آید عبث بیرون ز کنج لب زبان تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زلال خضر از گرد کسادی خاک می لیسد</p></div>
<div class="m2"><p>که سیر از آب حیوان کرد عالم را دهان تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به زیر بال بلبل می شود گل از حیا پنهان</p></div>
<div class="m2"><p>در آن گلشن که باشد چهره چون ارغوان تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پریشان گرد زلفم، گوشه گیری نیست کار من</p></div>
<div class="m2"><p>وگرنه هیچ کنجی نست چون کنج دهان تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکوه حسن عالمسوز ازین افزون نمی باشد</p></div>
<div class="m2"><p>که در خواب بهاران است دایم پاسبان تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مگر خود ساقی خود بوده ای ای شاخ گل امشب؟</p></div>
<div class="m2"><p>که آتش می زند در خار مژگان ارغوان تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز آغوش لحد چون گل بغل واکرده می خیزد</p></div>
<div class="m2"><p>به خاک هر که مایل می شود سرو روان تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا همچون شکار جرگه دایم در میان دارد</p></div>
<div class="m2"><p>بناگوش و خط و خال و رخ و زلف و دهان تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نفس در سینه باد صبا مستانه می رقصد</p></div>
<div class="m2"><p>همانا غنچه ای واکرده است از بوستان تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نباشد جای حیرت گر نقاب از چهره نشناسم</p></div>
<div class="m2"><p>که دارد یک فروغ آیینه و آیینه دان تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مگر در خلوت آیینه تنها یافتی خود را؟</p></div>
<div class="m2"><p>که از نقش حیا ساده است مهر بوسه دان تو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ترا بس در میان سروقدان این سرافرازی</p></div>
<div class="m2"><p>که باشد همچو صائب بلبلی در بوستان تو</p></div></div>