---
title: >-
    غزل شمارهٔ ۲۱۰۰
---
# غزل شمارهٔ ۲۱۰۰

<div class="b" id="bn1"><div class="m1"><p>چون گوشه کلاه به پروانه نشکنم؟</p></div>
<div class="m2"><p>داغ از میان سوختگان دست من گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از چاک پیرهن چه قدر وا شود دلش؟</p></div>
<div class="m2"><p>دستی که فال عیش ز چاک کفن گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در نار باغ سینه حلاوت نمانده است</p></div>
<div class="m2"><p>امروز دست ازوست که سیب ذقن گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در سنگلاخ دهر چه پاسخت کرده ای؟</p></div>
<div class="m2"><p>آیینه روشنی ز جلای وطن گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صائب همین بس است که در سلک شاعران</p></div>
<div class="m2"><p>طالب نمی کند به سخن های من گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کلکم به یک صریر سواد سخن گرفت</p></div>
<div class="m2"><p>بلبل به زور ناله سراسر چمن گرفت</p></div></div>