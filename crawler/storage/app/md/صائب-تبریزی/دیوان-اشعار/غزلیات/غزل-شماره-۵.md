---
title: >-
    غزل شمارهٔ ۵
---
# غزل شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>شد به دشواری دل از لعل لب دلبر جدا</p></div>
<div class="m2"><p>این کباب تر به خون دل شد از اخگر جدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقش هستی را به آسانی ز دل نتوان زدود</p></div>
<div class="m2"><p>بی گداز از سکه هیهات است گردد زر جدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آگه است از حال زخم من جدا از تیغ او</p></div>
<div class="m2"><p>با دهان خشک شد هر کس که از کوثر جدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کار هر بی ظرف نبود دل ز جان برداشتن</p></div>
<div class="m2"><p>زان لب میگون به تلخی می شود ساغر جدا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر درآمیزد به گلها بوی آن گل‌پیرهن</p></div>
<div class="m2"><p>من به چشم بسته می سازم ز یکدیگر جدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در گذر از قرب شاهان عمر اگر خواهی، که خضر</p></div>
<div class="m2"><p>یافت عمر جاودان تا شد ز اسکندر جدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی سرشک تلخ، افتاد از نظر مژگان مرا</p></div>
<div class="m2"><p>رشته می گردد سبک چون گردد از گوهر جدا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون نسوزد خواب در چشمم، که شبهای فراق</p></div>
<div class="m2"><p>اخگری در پیرهن دارم ز هر اختر جدا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست چون صائب قراری نقش را بر روی آب</p></div>
<div class="m2"><p>چون خیال او نمی گردد ز چشم تر جدا؟</p></div></div>