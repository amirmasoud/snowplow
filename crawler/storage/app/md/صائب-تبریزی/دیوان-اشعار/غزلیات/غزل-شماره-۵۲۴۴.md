---
title: >-
    غزل شمارهٔ ۵۲۴۴
---
# غزل شمارهٔ ۵۲۴۴

<div class="b" id="bn1"><div class="m1"><p>من که هرپاره دلم هست به صد جا مشغول</p></div>
<div class="m2"><p>بادل جمع شوم چون به تو تنها مشغول</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خدمت دور به نزدیک نمی فرمایند</p></div>
<div class="m2"><p>اهل دل را نکند عشق به دنیا مشغول</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماند از جلوه بی قیمت یوسف محروم</p></div>
<div class="m2"><p>هرکه درقافله گردید به سودا مشغول</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ماند چون آینه در دایره حیرانی</p></div>
<div class="m2"><p>هرکه از ساده دلی شد به تماشا مشغول</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قسمت دیده ز هر عضو جدا می گیرم</p></div>
<div class="m2"><p>به تماشای توام بس که سراپا مشغول</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر نفس عشق دو صد نقش بدیع انگیزد</p></div>
<div class="m2"><p>تا نگردد به خود آن آینه سیما مشغول</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می شود صائب از اندیشه دنیا فارغ</p></div>
<div class="m2"><p>شد دل هر که به اندیشه عقبی مشغول</p></div></div>