---
title: >-
    غزل شمارهٔ ۴۸۵۶
---
# غزل شمارهٔ ۴۸۵۶

<div class="b" id="bn1"><div class="m1"><p>هیچ نوشی نیست بی نیش ای پسر هشیار باش</p></div>
<div class="m2"><p>خواب شیرین پشه دارد درکمین بیدار باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قرب آتش طلعتان تردامنی می آورد</p></div>
<div class="m2"><p>آب پای گل مشو، خارسردیوار باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشأه زندانی بود در شیشه های سر به مهر</p></div>
<div class="m2"><p>گرسری داری به شور عشق،بی دستار باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز سرانگشت ندامت نیست رزق کاهلان</p></div>
<div class="m2"><p>مزد می خواهی، چو مردان روزو شب در کار باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهر خاموشی صدف راکرد معمور از گهر</p></div>
<div class="m2"><p>لب ببند از گفتگو، گنجینه اسرار باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آب را استادگی آیینه گلزار کرد</p></div>
<div class="m2"><p>پا به دامن کش درین بستانسرا سیار باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خار بی گل را گل بی خار سازد احتیاط</p></div>
<div class="m2"><p>جمع کن دامان خود فارغ ز زخم خارباش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صحبت پل می کند سیلاب را پا در رکاب</p></div>
<div class="m2"><p>چون دوتا گردید قد صائب سبکرفتار باش</p></div></div>