---
title: >-
    غزل شمارهٔ ۴۸۰
---
# غزل شمارهٔ ۴۸۰

<div class="b" id="bn1"><div class="m1"><p>مستی و بی خبری رتبه عام است اینجا</p></div>
<div class="m2"><p>ابجد تازه سوادان خط جام است اینجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سفر کردن ظاهر، نشود کار تمام</p></div>
<div class="m2"><p>هر که در خویش سفر کرد تمام است اینجا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشود جمع، زبان آوری و سوختگی</p></div>
<div class="m2"><p>سخن از شمع مگویید که خام است اینجا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سخن عشق چو آید به میان خامش باش</p></div>
<div class="m2"><p>لب گشودن به تکلم لب بام است اینجا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عارفان تلخ لب خود به شکایت نکنند</p></div>
<div class="m2"><p>کجروی های فلک گردش جام است اینجا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تلخکامی نبود در شکرستان وصال</p></div>
<div class="m2"><p>نامه آور نگه و بوسه پیام است اینجا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صید خود گوشه نشینان به توجه گیرند</p></div>
<div class="m2"><p>دیده منتظران حلقه دام است اینجا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به غم این یک دو نفس را گذراندن ستم است</p></div>
<div class="m2"><p>خنده صبح به دلگیری شام است اینجا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ذره تا مهر ندارند درین بزم قرار</p></div>
<div class="m2"><p>بنما خاطر آسوده کدام است اینجا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در غم آباد فلک رخنه آزادی نیست</p></div>
<div class="m2"><p>چشم تا کار کند حلقه دام است اینجا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پای در خلوت ما از در عادت مگذار</p></div>
<div class="m2"><p>در دل باز چو شد وقت سلام است اینجا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زلف را شانه زد، ای بال فشانان چمن</p></div>
<div class="m2"><p>زود خود را برسانید که دام است اینجا!</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نیست مقبول دل عشق، پسندیده عقل</p></div>
<div class="m2"><p>هر که آدم بود آنجا، دد و دام است اینجا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا در آتشکده دل نگدازی صائب</p></div>
<div class="m2"><p>دعوی پختگی اندیشه خام است اینجا</p></div></div>