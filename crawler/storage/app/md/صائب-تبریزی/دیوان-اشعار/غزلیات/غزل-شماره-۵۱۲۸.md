---
title: >-
    غزل شمارهٔ ۵۱۲۸
---
# غزل شمارهٔ ۵۱۲۸

<div class="b" id="bn1"><div class="m1"><p>هرکه گردید ز عبرت به تماشا قانع</p></div>
<div class="m2"><p>به کف پوچ شد از گوهر دریا قانع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زود عاجز شود از دیدن یوسف، چشمی</p></div>
<div class="m2"><p>که به دیدار نگردد چو زلیخا قانع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نتوان کرد به دیوار هوس را خرسند</p></div>
<div class="m2"><p>طفل از باغ نگردد به تماشا قانع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاک در کاسه چشمی که ز کوته نظری</p></div>
<div class="m2"><p>به نظر بازی آهوست زلیلی قانع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر زمان روی سخن در دگری نتوان کرد</p></div>
<div class="m2"><p>طوطی ماست به یک آینه سیما قانع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با کلام شکرین شکوه مکن ازتلخی</p></div>
<div class="m2"><p>کز شکر شد به سخن طوطی گویاقانع</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر سحر سرزند ازمشرق دیگر خورشید</p></div>
<div class="m2"><p>چون به یک سینه شود داغ تو تنها قانع؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرکه با وسعت مشرب طرف زهد گرفت</p></div>
<div class="m2"><p>به کف خاک شداز دامن صحرا قانع</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جای رحم است برآن فاخته کوته بین</p></div>
<div class="m2"><p>که به یک سرو شد از عالم بالاقانع</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی نیاز ازدر ابنای زمان شد صائب</p></div>
<div class="m2"><p>شد فقیری که به دریوزه دلها قانع</p></div></div>