---
title: >-
    غزل شمارهٔ ۶۸۵۸
---
# غزل شمارهٔ ۶۸۵۸

<div class="b" id="bn1"><div class="m1"><p>درین حدیقه پر میوه تا جگر نخوری</p></div>
<div class="m2"><p>ز نخل زندگی خویشتن ثمر نخوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترا به چشمه حیوان گذار خواهد بود</p></div>
<div class="m2"><p>جگر ز رفتن این عمر مختصر نخوری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود به قدر هنر داغهای محرومی</p></div>
<div class="m2"><p>فریب شهرت بی حاصل هنر نخوری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به ناروایی خود صبر اگر توانی کرد</p></div>
<div class="m2"><p>ز سکه زخم به رخسار همچو زر نخوری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترا که چیدن گل در خیال می گردد</p></div>
<div class="m2"><p>نمی شود که ز هر خار نیشتر نخوری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>توان به همت عالی ز عشق گل چیدن</p></div>
<div class="m2"><p>به دست کوته ازین بوستان ثمر نخوری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گناه مایده بی دریغ رحمت چیست؟</p></div>
<div class="m2"><p>اگر تو جز دل خود روزی دگر نخوری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهان سفله چه دارد کز او طمع داری؟</p></div>
<div class="m2"><p>ز سرو حاصل و از چوب بید برنخوری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو مغزپسته ترا صبح در شکر گیرد</p></div>
<div class="m2"><p>فریب چاشنی خواب اگر سحر نخوری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هزار لقمه ندارد زیان در آگاهی</p></div>
<div class="m2"><p>بهوش باش که یک لقمه بی خبر نخوری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر گزیر نداری ز آشنایی خلق</p></div>
<div class="m2"><p>بیازما و بپیوند تا جگر نخوری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قضا به دست تو زان داده است لنگر عقل</p></div>
<div class="m2"><p>که روی دست ز هر موجه خطر نخوری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به هیچ دل نزنی همچو ماه نو ناخن</p></div>
<div class="m2"><p>اگر دو هفته دل خویش چون قمر نخوری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کمر مبند به آزار هیچ کس صائب</p></div>
<div class="m2"><p>که زخم تیغ مکافات بر کمر نخوری</p></div></div>