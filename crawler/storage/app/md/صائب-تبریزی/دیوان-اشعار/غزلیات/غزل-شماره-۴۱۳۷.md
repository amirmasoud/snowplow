---
title: >-
    غزل شمارهٔ ۴۱۳۷
---
# غزل شمارهٔ ۴۱۳۷

<div class="b" id="bn1"><div class="m1"><p>عیش جهان به رند می آشام داده اند</p></div>
<div class="m2"><p>خط مسلمی به لب جام داده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بر گریز حادثه ریزند گل به جیب</p></div>
<div class="m2"><p>آزادگان که ترک سرانجام داده اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آسودگی ز خاطر نام آوران مجو</p></div>
<div class="m2"><p>کاین منزلت به مردم گمنام داده اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جمعی که حلقه بر در ابرام می زنند</p></div>
<div class="m2"><p>با خود قرار تلخی دشنام داده اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از جستجوی رزق چه آسوده خاطرست</p></div>
<div class="m2"><p>آن را که دانه از گره دام داده اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مالیده اند بر لب خود خاک عاشقان</p></div>
<div class="m2"><p>از دور بوسه گر به لب بام داده اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترسم ز بوسه لب ساقی کنند کم</p></div>
<div class="m2"><p>بوسی که میکشان به لب جام داده اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرگز نصیب بوسه ربایان نگشته است</p></div>
<div class="m2"><p>ما را حلاوتی که ز پیغام داده اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عذرم بجاست پخته اگر دیر می شوم</p></div>
<div class="m2"><p>شیر مرا ز صبح ازل خام داده اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نقصان نکرده است کسی از ملایمت</p></div>
<div class="m2"><p>قند از زبان چرب به بادام داده اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تیغ فسان کشیده میدان جراتند</p></div>
<div class="m2"><p>آنها که تن به سختی ایام داده اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از خود گسستگان ز جان دست شسته را</p></div>
<div class="m2"><p>در راه سیل لنگر آرام داده اند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جمعی که دیده اند سرانجام بیخودی</p></div>
<div class="m2"><p>نقد حیات خویش به یک جام داده اند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پر سر کشی مکن که ز آغوش فاخته است</p></div>
<div class="m2"><p>هر سرو را که در چمن اندام داده اند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از شوق کعبه چشم تو کرده اند اگر سفید</p></div>
<div class="m2"><p>منشین ز پا که جامه احرام داده اند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صائب چه فارغند ز اندیشه حساب</p></div>
<div class="m2"><p>جمعی که کار آخرت انجام داده اند</p></div></div>