---
title: >-
    غزل شمارهٔ ۴۸۰۸
---
# غزل شمارهٔ ۴۸۰۸

<div class="b" id="bn1"><div class="m1"><p>اشکم ز دل به چهره دویدن گرفت باز</p></div>
<div class="m2"><p>این خانه شکسته چکیدن گرفت باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آه ضعیف من که به روزن نمی رسید</p></div>
<div class="m2"><p>بر روی چرخ، نیل کشیدن گرفت باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد تازه داغ کهنه ام از روی گرم عشق</p></div>
<div class="m2"><p>این میوه های خام رسیدن گرفت باز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیش شکستگی به سویدای دل رسید</p></div>
<div class="m2"><p>این خون نیم مرده چکیدن گرفت باز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر دانه اشک را که فرو خورده بود دل</p></div>
<div class="m2"><p>از تنگنای حوصله چیدن گرفت باز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باد مراد آه دلم را ز جا ربود</p></div>
<div class="m2"><p>این بحر آرمیده تپیدن گرفت باز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از ترکتاز غم دل صد پاره هر طرف</p></div>
<div class="m2"><p>چون لشکر شکسته دویدن گرفت باز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آهی علم کشید ز هر ذره خاک من</p></div>
<div class="m2"><p>مور ضعیف بال پریدن گرفت باز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نبضی که بود از رگ خواب آرمیده تر</p></div>
<div class="m2"><p>از شوق دست یار جهیدن گرفت باز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از لاله دشت دام تماشا به خاک کرد</p></div>
<div class="m2"><p>دل از سواد شهر رمیدن گرفت باز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دیگر دل ضعیف من از رشته های آه</p></div>
<div class="m2"><p>برخود چو کرم پیله تنیدن گرفت باز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر دانه امید که در زیر خاک بود</p></div>
<div class="m2"><p>از نوبهار وصل، دمیدن گرفت باز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر مویم از حلاوت آن لعل آبدار</p></div>
<div class="m2"><p>انگشت خود چو شمع مکیدن گرفت باز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چشمی که با خیال تو در خواب ناز بود</p></div>
<div class="m2"><p>از مژده وصال، پریدن گرفت باز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>این آن غزل که مولوی روم گفته است</p></div>
<div class="m2"><p>سیمرغ کوه قاف، رسیدن گرفت باز</p></div></div>