---
title: >-
    غزل شمارهٔ ۳۲۲۵
---
# غزل شمارهٔ ۳۲۲۵

<div class="b" id="bn1"><div class="m1"><p>ز سوز عشق داغی بر دل افگار می‌باید</p></div>
<div class="m2"><p>چراغی بر سر بالین این بیمار می‌باید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز لعل آبدار او تمنایی که من دارم</p></div>
<div class="m2"><p>مرا در دست صد انگشتر زنهار می‌باید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پریشان دارد از صد رهگذر تسبیح، احوالم</p></div>
<div class="m2"><p>مرا شیرازه‌ای از رشتهٔ زنّار می‌باید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به زهر چشم تنها پاس نتوان داشت خوبی را</p></div>
<div class="m2"><p>گل بی‌خار را شبنم دل بیدار می‌باید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زلیخا دامن امید را بیهوده نگشاید</p></div>
<div class="m2"><p>عبیر پیرهن را چشم چون دستار می‌باید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز چشم مست دارد عذرخواهی گر ننوشد می</p></div>
<div class="m2"><p>همین سابقی میان می‌کشان هشیار می‌باید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه سود از کارفرمایان ظاهر بی‌دماغان را؟</p></div>
<div class="m2"><p>که در دل کارفرمایی ز ذوق کار می‌باید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>متاع یوسفی حیف است باشد فرش در زندان</p></div>
<div class="m2"><p>تکلف بر طرف، دیوانه در بازار می‌باید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به از اشک ندامت نیست صائب هیچ تسبیحی</p></div>
<div class="m2"><p>ترا گر سبحه‌ای از بهر استغفار می‌باید</p></div></div>