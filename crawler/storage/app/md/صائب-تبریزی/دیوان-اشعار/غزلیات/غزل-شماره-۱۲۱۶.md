---
title: >-
    غزل شمارهٔ ۱۲۱۶
---
# غزل شمارهٔ ۱۲۱۶

<div class="b" id="bn1"><div class="m1"><p>تا فشاندم دست بر دنیا جهان آمد به دست</p></div>
<div class="m2"><p>از سبکدستی مرا رطل گران آمد به دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یافتم در سینه گرم آن بهشتی روی را</p></div>
<div class="m2"><p>در دل دوزخ بهشت جاودان آمد به دست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم پوشیدن ز دنیا چشم دل را باز کرد</p></div>
<div class="m2"><p>دولت بیدار ازین خواب گران آمد به دست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون هما مغز من از اندیشه روزی گداخت</p></div>
<div class="m2"><p>تا مرا از خوان قسمت استخوان آمد به دست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دامن زلفش به دستم در سیه مستی فتاد</p></div>
<div class="m2"><p>رفته بود از کار دستم چون عنان آمد به دست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سالها گردن کشیدم چون هدف در انتظار</p></div>
<div class="m2"><p>تا مرا تیری ازان ابرو کمان آمد به دست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صحبت یاران یکرنگ است دل را نوبهار</p></div>
<div class="m2"><p>برگ عیش من در ایام خزان آمد به دست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سایه بال هما بر استخوان من فتاد</p></div>
<div class="m2"><p>در کهنسالی مرا بخت جوان آمد به دست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همچو لال، از گفتگوی ظاهر اهل جهان</p></div>
<div class="m2"><p>تا زبان بستم مرا چندین زبان آمد به دست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در کمند پیچ و تاب افتاد از آزادگی</p></div>
<div class="m2"><p>هر که را سررشته کار جهان آمد به دست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قامت خم عذر ایام جوانی را نخواست</p></div>
<div class="m2"><p>رفت تیر از شست بیرون چون کمان آمد به دست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زین جهان آب و گل را هم به دل صائب فتاد</p></div>
<div class="m2"><p>یوسفی آخر مرا زین کاروان آمد به دست</p></div></div>