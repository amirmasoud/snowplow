---
title: >-
    غزل شمارهٔ ۶۰۸۱
---
# غزل شمارهٔ ۶۰۸۱

<div class="b" id="bn1"><div class="m1"><p>سرمه را هم محرم چشم سیاه خود مکن</p></div>
<div class="m2"><p>گر توانی آشنایی با نگاه خود مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رنگ بر رخساره عصمت مبادا بشکند</p></div>
<div class="m2"><p>دستبازی با سر زلف سیاه خود مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قبله من، عکس در شرع حیا نامحرم است</p></div>
<div class="m2"><p>خلوت آیینه را هم جلوه گاه خودمکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاطر رنگ حیا از برگ گل نازکترست</p></div>
<div class="m2"><p>شاخ گل را زینت طرف کلاه خود مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لشکر غارتگر خط می رسد از گرد راه</p></div>
<div class="m2"><p>تکیه بر جمعیت زلف سیاه خود مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پند صائب را در گوش غرور حسن ساز</p></div>
<div class="m2"><p>بیش ازین آزار جان بی گناه خود مکن</p></div></div>