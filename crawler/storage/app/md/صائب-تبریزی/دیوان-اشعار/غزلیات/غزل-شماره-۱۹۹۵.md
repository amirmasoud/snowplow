---
title: >-
    غزل شمارهٔ ۱۹۹۵
---
# غزل شمارهٔ ۱۹۹۵

<div class="b" id="bn1"><div class="m1"><p>تا در ترددست نفس، جان روانه است</p></div>
<div class="m2"><p>بر باد پای عمر، نفس تازیانه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشق کجا به فکر سرانجام خانه است؟</p></div>
<div class="m2"><p>مرغ ملول را ته بال آشیانه است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشتیم پیر از غم دنیا و آخرت</p></div>
<div class="m2"><p>پشت کمان خمیده ز فکر دو خانه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آوازه رحیل کز او خوابهاست تلخ</p></div>
<div class="m2"><p>پای به خواب رفته ما را فسانه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کوتاه دیدگی است نفس راست ساختن</p></div>
<div class="m2"><p>بر توسنی که موج نفس تازیانه است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حیرت امان نمی دهدم تا نفس کشم</p></div>
<div class="m2"><p>بیچاره طوطیی که در آیینه خانه است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین سرکشان که گردن دعوی کشیده اند</p></div>
<div class="m2"><p>از هر که عشق گرد برآرد نشانه است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روی شکفته خرده جان را دهد به باد</p></div>
<div class="m2"><p>کم عمری گل از نفس بیغمانه است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل می برد به چین جبین دلربای من</p></div>
<div class="m2"><p>این صید پیشه را گره دام دانه است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پروانه ها فسرده، خموشند شمعها</p></div>
<div class="m2"><p>در محفلی که پای ادب در میانه است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روشندلان ز هر دو جهانند بی نیاز</p></div>
<div class="m2"><p>خورشید را ز چهره زرین خزانه است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>روی زمین ز شکوه گردون لبالب است</p></div>
<div class="m2"><p>هر کس که هست زخمی ازین شیشه خانه است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آبی که زندگانی جاوید می دهد</p></div>
<div class="m2"><p>دارد اگر وجود، شراب شبانه است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آغوش بحر بی گهر شاهوار نیست</p></div>
<div class="m2"><p>دل چون دو نیم شد صدف آن یگانه است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تسلیم می کند به ستم ظلم را دلیر</p></div>
<div class="m2"><p>جرم زمانه ساز فزون از زمانه است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر کس به قدر هوش خود آزار می کشد</p></div>
<div class="m2"><p>در بحر پر کنار، خطر بیکرانه است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صائب ز کوی عشق به جایی نمی روم</p></div>
<div class="m2"><p>چون کعبه قتلگاه من این آستانه است</p></div></div>