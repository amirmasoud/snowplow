---
title: >-
    غزل شمارهٔ ۳۹۵۵
---
# غزل شمارهٔ ۳۹۵۵

<div class="b" id="bn1"><div class="m1"><p>خطی که گرد رخ او ز مشک ناب بود</p></div>
<div class="m2"><p>یکی ز حلقه بگوشانش آفتاب بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خواب نازندیده است دولت بیدار</p></div>
<div class="m2"><p>گشایشی که در آن چشم نیمخواب بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنین که سنگدل افتاده کوه تمکینت</p></div>
<div class="m2"><p>عجب که ناله عشاق را جواب بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شراب تلخ ز شیرین بود گواراتر</p></div>
<div class="m2"><p>ز لطف بیش مرا چشم برعتاب بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز علم رسم دل خویش ساده که کتاب</p></div>
<div class="m2"><p>به چشم زنده دلان پرده های خواب بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز روشنایی دل ظلمت است قسمت نفس</p></div>
<div class="m2"><p>سیاه روزی خفاش از آفتاب بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فریب جلوه دنیا مخور ز بی بصری</p></div>
<div class="m2"><p>که غول تشنه لبان موجه سراب بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به سعی خویش بودغره از سیاه دلی</p></div>
<div class="m2"><p>اگر چه بال و پرسایه ز آفتاب بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شمرده نه قدم خویش تا رسی به مراد</p></div>
<div class="m2"><p>که دوری ره نزدیک از شتاب بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز روشنایی دل خواب شد به چشمم تلخ</p></div>
<div class="m2"><p>نمک به دیده روزن ز ماهتاب بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز برگ عیش دگرها شوند اگر خوشوقت</p></div>
<div class="m2"><p>حضور صائب از اندیشه صواب بود</p></div></div>