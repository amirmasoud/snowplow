---
title: >-
    غزل شمارهٔ ۷۸۰
---
# غزل شمارهٔ ۷۸۰

<div class="b" id="bn1"><div class="m1"><p>دل را ز قید جسم رها می کنیم ما</p></div>
<div class="m2"><p>این دانه را ز کاه جدا می کنیم ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمر دوباره در گره روزگار نیست</p></div>
<div class="m2"><p>جان را به زلف یار فدا می کنیم ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ظرف بحر رحمت حق آب و خون یکی است</p></div>
<div class="m2"><p>اندیشه صواب و خطا می کنیم ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آه این چنین اگر شکند آستین سعی</p></div>
<div class="m2"><p>پیراهن سپهر، قبا می کنیم ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>افتد غزال دولت اگر در کمند ما</p></div>
<div class="m2"><p>از همت بلند رها می کنیم ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می می کشیم و خنده مستانه می زنیم</p></div>
<div class="m2"><p>با این دو روزه عمر چها می کنیم ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مهمان مرگ بر در دل حلقه می زند</p></div>
<div class="m2"><p>تا فکر آشیان و سرا می کنیم ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در قلزمی که نیست سر نوح در حساب</p></div>
<div class="m2"><p>همچون حباب، کسب هوا می کنیم ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با شوخ دیدگان نتوان هم نواله شد</p></div>
<div class="m2"><p>زین خوان نصیب خویش جدا می کنیم ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نگشود صائب از مدد خلق هیچ کار</p></div>
<div class="m2"><p>از خلق روی دل به خدا می کنیم ما</p></div></div>