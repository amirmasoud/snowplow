---
title: >-
    غزل شمارهٔ ۲۹۱۶
---
# غزل شمارهٔ ۲۹۱۶

<div class="b" id="bn1"><div class="m1"><p>دل پرخون کجا از جسم پا در گل خبر دارد؟</p></div>
<div class="m2"><p>کجا این دل به دریا کرده از ساحل خبر دارد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سیر عالم بالا نگردد تن حجاب جان</p></div>
<div class="m2"><p>که از نشو و نما این سرو پا در گل خبر دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از احوال نظربازان مدان معشوق را غافل</p></div>
<div class="m2"><p>که از هر ذره ای خورشید روشندل خبر دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نبیند زیرپا چشمی که افتاده است بر منزل</p></div>
<div class="m2"><p>دل حق جو کجا از عالم باطل خبر دارد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زدست و پای بیتابی زدن آسوده می گردد</p></div>
<div class="m2"><p>هر آن موجی کز این دریای بی ساحل خبر دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل گم گشته خود را سراغ از زلف جانان کن</p></div>
<div class="m2"><p>که هر تاری از و چون سبحه از صددل خبر دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه می پرسی شمار منزل از سیل سبک جولان؟</p></div>
<div class="m2"><p>که هر کس کاهل افتاده است از منزل خبر دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زما بی حاصلان از حاصل دنیا چه می پرسی؟</p></div>
<div class="m2"><p>که هر کس تخمی افشانده است از حاصل خبر دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز ابراهیم ادهم پرس قدر ملک درویشی</p></div>
<div class="m2"><p>که طوفان دیده از آسایش ساحل خبر دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به شکر خنده شیرین می کند صائب دهانش را</p></div>
<div class="m2"><p>کسی کز تلخی محرومی سایل خبر دارد</p></div></div>