---
title: >-
    غزل شمارهٔ ۷۵۳
---
# غزل شمارهٔ ۷۵۳

<div class="b" id="bn1"><div class="m1"><p>چون شعله سر مکش ز دل سینه تاب ما</p></div>
<div class="m2"><p>کز سوز عشق، اشک ندارد کباب ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آفتاب تجربه گشتیم خامتر</p></div>
<div class="m2"><p>نارس برآمد از سفر خم شراب ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیچ است هر چه هست به جز همت بلند</p></div>
<div class="m2"><p>این مصرع است از دو جهان انتخاب ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این راه دور زود به انجام می رسد</p></div>
<div class="m2"><p>کوتاهیی اگر نکند پیچ و تاب ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منزل بلند و همت شبگیر کوته است</p></div>
<div class="m2"><p>فرصت سبک عنان و گران است خواب ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچیم اگر چه صائب و از هیچ کمتریم</p></div>
<div class="m2"><p>دام فریب خلق ندارد سراب ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پاک است همچو صبح به عالم حساب ما</p></div>
<div class="m2"><p>در خون شبنمی نرود آفتاب ما</p></div></div>