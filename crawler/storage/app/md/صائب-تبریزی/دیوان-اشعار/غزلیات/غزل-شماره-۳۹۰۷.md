---
title: >-
    غزل شمارهٔ ۳۹۰۷
---
# غزل شمارهٔ ۳۹۰۷

<div class="b" id="bn1"><div class="m1"><p>سبکروان که طلبکار یار می گردند</p></div>
<div class="m2"><p>غبار رهگذر انتظار می گردند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برآز قید علایق که خانه بردوشان</p></div>
<div class="m2"><p>ز سیل حادثه کم بیقرار می گردند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز پشت مرکب چوبین دار بی برگان</p></div>
<div class="m2"><p>به دوش چرخ چو عیسی سوار می گردند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جماعتی که ز تلخی زنند جوش نشاط</p></div>
<div class="m2"><p>به هر مذاق چو می خوشگوار می گردند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به نقد وقت گروهی که دل نمی بندند</p></div>
<div class="m2"><p>همیشه خرج ره انتظار می گردند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز خوش عنانی عمر آن کسان که آگاهند</p></div>
<div class="m2"><p>گرهگشا چو نسیم بهار می گردند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز خود برون شدگان همچو قطره بیخبرند</p></div>
<div class="m2"><p>که عاقبت گهر شاهوار می گردند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به آب خضر ندارند کار موزونان</p></div>
<div class="m2"><p>سخنوران به سخن پایدار می گردند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حذر کنند ز خلوت فزون ز دیده خلق</p></div>
<div class="m2"><p>جماعتی که ز خود شرمسار می گردند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز سایه پروبال هما سبک مغزان</p></div>
<div class="m2"><p>شکار دولت ناپایدار می گردند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز تاج وتخت زلیخا به خاک راه افتاد</p></div>
<div class="m2"><p>عزیز خوارکنان زود خوار می گردند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو نافه مردم خونین جگر نمی دانند</p></div>
<div class="m2"><p>که صاحب نفس مشکبار می گردند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چنین که مست غرورند دلبران صائب</p></div>
<div class="m2"><p>کجا ز سیلی خط هوشیار می گردند</p></div></div>