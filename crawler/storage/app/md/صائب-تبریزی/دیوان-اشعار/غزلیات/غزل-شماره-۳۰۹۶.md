---
title: >-
    غزل شمارهٔ ۳۰۹۶
---
# غزل شمارهٔ ۳۰۹۶

<div class="b" id="bn1"><div class="m1"><p>اگر فتح جگرداران به تیغ افراختن باشد</p></div>
<div class="m2"><p>مرا امید نصرت از سپر انداختن باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگردد شمع خرج گاز چون خاموش می گردد</p></div>
<div class="m2"><p>گل خیر زبان آتشین سر باختن باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل خود می خورد دیوانه دور از حلقه طفلان</p></div>
<div class="m2"><p>که غمگین ساز سیر آهنگ از ننواختن باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر از روشندلانی از گداز تن مشو غافل</p></div>
<div class="m2"><p>که کار شمع در دلهای شب بگداختن باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شود تیغ زبان خار، خرج آتش از تندی</p></div>
<div class="m2"><p>ز زخم خار، گل امن از سپر انداختن باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مقامات محبت را به زهد خشک طی کردن</p></div>
<div class="m2"><p>به صحرای پر آتش اسب چوبین تاختن باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نثار تیغ سیراب شهادت نقد جان کردن</p></div>
<div class="m2"><p>نفس در زیر آب زندگانی باختن باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تهیدستی ندارد جز خجالت حاصل دیگر</p></div>
<div class="m2"><p>که بار بید مجنون سر به زیر انداختن باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درین در گاه صائب طاعت خاصی است هر کس را</p></div>
<div class="m2"><p>صلاح اهل دولت، کار مردم ساختن باشد</p></div></div>