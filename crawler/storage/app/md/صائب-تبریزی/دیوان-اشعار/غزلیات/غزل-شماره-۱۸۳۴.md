---
title: >-
    غزل شمارهٔ ۱۸۳۴
---
# غزل شمارهٔ ۱۸۳۴

<div class="b" id="bn1"><div class="m1"><p>ره سخن به رخش خط عنبرافشان یافت</p></div>
<div class="m2"><p>فغان که طوطی از آیینه باز میدان یافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز شبنمش جگر سنگ می شود سوراخ</p></div>
<div class="m2"><p>گلی که پرورش از اشک عندلیبان یافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هر که هر چه سزاوار بود بخشیدند</p></div>
<div class="m2"><p>سکندر آینه و خضر آب حیوان یافت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگیر از سر زانوی فکر سر زنهار</p></div>
<div class="m2"><p>که غنچه هر چه طلب کرد در گریبان یافت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز کاوش جگر فکر ناامید مباش</p></div>
<div class="m2"><p>که ذره در دل خود آفتاب تابان یافت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز کاوش جگر فکر ناامید مباش</p></div>
<div class="m2"><p>که ذره در دل خود آفتاب تابان یافت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کلید گنج سعادت زبان خاموش است</p></div>
<div class="m2"><p>صدف به مزد خموشی گهر ز نیسان یافت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من آن زمان ز دل چاک چاک شستم دست</p></div>
<div class="m2"><p>که شانه راه در آن زلف عنبرافشان یافت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هزار سختی نادیده در کمین دارد</p></div>
<div class="m2"><p>کسی که کام دل از روزگار آسان یافت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حجاب مانع روزی است خاکساران را</p></div>
<div class="m2"><p>تنور از نفس آتشین خود نان یافت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فغان که کوهکن ساده دل نمی داند</p></div>
<div class="m2"><p>که راه در دل خوبان به زور نتوان یافت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مکن شتاب به هر ورطه ای که افتادی</p></div>
<div class="m2"><p>که ماه مصر برآمد ز چاه، زندان یافت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>(لب خموش سخن های دلنشین دارد</p></div>
<div class="m2"><p>ضمیر نامه ما می توان ز عنوان یافت)</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز فکر، قامت هر کس که حلقه شد صائب</p></div>
<div class="m2"><p>به دست همت خود خاتم سلیمان یافت</p></div></div>