---
title: >-
    غزل شمارهٔ ۲۱۵
---
# غزل شمارهٔ ۲۱۵

<div class="b" id="bn1"><div class="m1"><p>طی به ماهی سازد از کندی، ره یک روزه را</p></div>
<div class="m2"><p>رشته بیرون آمده است از پای، ماه روزه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مه شوال، دست از باده روشن مدار</p></div>
<div class="m2"><p>صیقل سی روزه باید، ظلمت سی روزه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خسیسان عیب ظاهر گردد اسباب طمع</p></div>
<div class="m2"><p>می کند کوری مثنی، کاسه دریوزه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل ز دنیا زودتر گردد جوانان را خنک</p></div>
<div class="m2"><p>کهنگی از سردی آب است مانع کوزه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در غریبی زود میرد ناز پرورد وطن</p></div>
<div class="m2"><p>شد نگین دان چار دیوار لحد فیروزه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخت رویی با ملایم طینتان زیبنده نیست</p></div>
<div class="m2"><p>در زمین نرم بیرون آور از پا موزه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیده عاشق نگردد صائب از دیدار سیر</p></div>
<div class="m2"><p>کز طمع سیری نباشد کاسه دریوزه را</p></div></div>