---
title: >-
    غزل شمارهٔ ۳۸۲۲
---
# غزل شمارهٔ ۳۸۲۲

<div class="b" id="bn1"><div class="m1"><p>ز جوش نشأه ز مغزم بهار می خیزد</p></div>
<div class="m2"><p>ز فیض اشک گلم از کنار می خیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین که گوشه ابروی صیقل است بلند</p></div>
<div class="m2"><p>کجا ز آینه ما غبار می خیزد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هر چمن که چو طاوس جلوه گر گردید</p></div>
<div class="m2"><p>تذرو رنگ ز شاخ بهار می خیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به داغ سینه صائب به چشم کم منگر</p></div>
<div class="m2"><p>جنون ز دامن این لاله زار می خیزد</p></div></div>