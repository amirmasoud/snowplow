---
title: >-
    غزل شمارهٔ ۵۹۱۷
---
# غزل شمارهٔ ۵۹۱۷

<div class="b" id="bn1"><div class="m1"><p>از یار ز ناسازی اغیار گذشتیم</p></div>
<div class="m2"><p>از کثرت خار از گل بی خار گذشتیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این باده زیاد از دهن ساغر ما بود</p></div>
<div class="m2"><p>مخمور ز لعل لب دلدار گذشتیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جایی که سخن سبز نگردد نتوان گفت</p></div>
<div class="m2"><p>چون طوطی ازان آینه رخسار گذشتیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کردیم ز گل صلح به نظاره خشکی</p></div>
<div class="m2"><p>چون آب تهیدست ز گلزار گذشتیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سهل است نظر بسته ز فردوس گذشتن</p></div>
<div class="m2"><p>ما کز سر کیفیت دیدار گذشتیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کوتاه نمودیم ز دل دست علایق</p></div>
<div class="m2"><p>چون برق بر این وادی خونخوار گذشتیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بستیم به سر رشته وحدت کمر خویش</p></div>
<div class="m2"><p>از کشمکش سبحه و زنارگذشتیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قطع نظر از راحت این نشأه نمودیم</p></div>
<div class="m2"><p>زین خواب گران از دل بیدار گذاشتیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سنگ ره ما سختی این راه نگردید</p></div>
<div class="m2"><p>چون سیل سبکسیر ز کهسار گذشتیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خاری نشد آزرده به زیر قدم ما</p></div>
<div class="m2"><p>چون سایه ابراز سر گلزار گذشتیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از خرقه تزویر نچیدیم دکانی</p></div>
<div class="m2"><p>مردانه ازین پرده پندار گذشتیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شد دست دعا خار به زیر قدم ما</p></div>
<div class="m2"><p>از بس که ازین مرحله هموار گذشتیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صائب چو گران بود به رنجور عیادت</p></div>
<div class="m2"><p>از دیدن آن نرگس بیمار گذشتیم</p></div></div>