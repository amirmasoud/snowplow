---
title: >-
    غزل شمارهٔ ۵۴۸۲
---
# غزل شمارهٔ ۵۴۸۲

<div class="b" id="bn1"><div class="m1"><p>ز کوشش حاصلی غیر از غبار دل نمی یابم</p></div>
<div class="m2"><p>به از افتادگی این راه را منزل نمی یابم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که گردانید از عالم ندانم روی دلها را</p></div>
<div class="m2"><p>که از صاحبدلان عهد روی دل نمی یابم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه ساعت بود بند از پای من برداشت بیتابی</p></div>
<div class="m2"><p>که چون ریگ روان می گردم و منزل نمی یابم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ظهور حق ز باطل چشم من بسته است ای خود بین</p></div>
<div class="m2"><p>تو لیلی را نمی یابی و من محمل نمی یابم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به احسان می توان جان برد ازین دریای پرشورش</p></div>
<div class="m2"><p>کنار این بحر را جز دامن سایل نمی یابم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که از گرداب افکند این گره در کار دریارا</p></div>
<div class="m2"><p>که چندانی که می گردم در او ساحل نمی یابم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درون سینه خرمنها ز تخم دوستی دارم</p></div>
<div class="m2"><p>زمین سینه احباب را قابل نمی یابم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز آب و گل ترا گر حاصلی باشد غنیمت دان</p></div>
<div class="m2"><p>که من جز مایه لغزش در آب و گل نمی یابم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنان از موج رحمت دامن این بحر خالی شد</p></div>
<div class="m2"><p>که جوهر در جبین خنجر قاتل نمی بابم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز بار طوق چون قمری چرا گردن سیه سازم</p></div>
<div class="m2"><p>که من چون سرو ازین گردنکشان حاصل نمی یابم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ترا گر هست ازین دریا گهر در کف غنیمت دان</p></div>
<div class="m2"><p>که من گوهر بغیر از عقده مشکل نمی یابم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ندارد فکر رحلت راه در جسم گرانجانم</p></div>
<div class="m2"><p>به افتادن من این دیوار را مایل نمی یابم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به بار دل بساز از خلوت آن شمع بی پروا</p></div>
<div class="m2"><p>که با پروانگی من بار در محفل نمی یابم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مجو صائب نوای دلپذیر از عندلیب من</p></div>
<div class="m2"><p>که در عالم نشان از هیچ صاحبدل نمی یابم</p></div></div>