---
title: >-
    غزل شمارهٔ ۴۷۵۸
---
# غزل شمارهٔ ۴۷۵۸

<div class="b" id="bn1"><div class="m1"><p>از شراب ارغوانی چهره را گلرنگ ساز</p></div>
<div class="m2"><p>بر نسیم از جوش گل جای نفس راتنگ ساز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می رسد روزی که بر بالینت آید آفتاب</p></div>
<div class="m2"><p>همچو شبنم سعی کن آیینه را بی زنگ ساز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از تماشای تو دلهای اسیران آب شد</p></div>
<div class="m2"><p>بعد ازین آیینه خود از دل چون سنگ ساز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون میسرنیست قانون فلک را گوشمال</p></div>
<div class="m2"><p>این نوای تلخ را از پنبه سیر آهنگ ساز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پاکدامانی میسر نیست بی خون جگر</p></div>
<div class="m2"><p>تا به بیرنگی رسی یک چند با نیرنگ ساز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یوسف از زندان قدم بر مسند عزت گذاشت</p></div>
<div class="m2"><p>چند روزی مصلحت رابا جهان تنگ ساز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نداری ظرف خون خوردن درین بستان چو گل</p></div>
<div class="m2"><p>زین شراب لعل دست و دامنی گلرنگ ساز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا چو شبنم از دامان گلها برخوری</p></div>
<div class="m2"><p>گریه خود را درین بستانسرا بیرنگ ساز</p></div></div>