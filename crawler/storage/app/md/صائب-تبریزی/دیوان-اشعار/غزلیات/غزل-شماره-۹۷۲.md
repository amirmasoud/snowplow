---
title: >-
    غزل شمارهٔ ۹۷۲
---
# غزل شمارهٔ ۹۷۲

<div class="b" id="bn1"><div class="m1"><p>دل چنین زار و نزار از اختر بد گوهرست</p></div>
<div class="m2"><p>شعله لاغر این چنین از چشم تنگ مجمرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست غافل گلشن از احوال بیرون ماندگان</p></div>
<div class="m2"><p>رخنه دیوار بهر مرغ بی بال و پرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من که دارم تکیه بر شمشیر چون سازم، که چرخ</p></div>
<div class="m2"><p>غوطه در خون می دهد آن را که از گل بسترست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس که رم خورده است از معموره عالم دلم</p></div>
<div class="m2"><p>دیده روزن به چشم من دهان اژدرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می خورم چون موج حسرت غوطه در بحر سراب</p></div>
<div class="m2"><p>آب حیوان از تغافل های خشک من ترست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما که از دل خارخار جاه بیرون کرده ایم</p></div>
<div class="m2"><p>بوته خاری به فرق ما به از صد افسرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سبزه زنگار چار انگشت بر آیینه ام</p></div>
<div class="m2"><p>بهتر از کوه گران منت روشنگرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کرده ام قطع نظر از گرم و سرد روزگار</p></div>
<div class="m2"><p>بی نیاز آیینه ام از صیقل و خاکسترست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آفتاب هر کسی از مشرقی آید برون</p></div>
<div class="m2"><p>می پرستان را دهان شیشه می خاورست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون نگردد هر سر مو مشرق آهی مرا؟</p></div>
<div class="m2"><p>شعله جواله خونین دل مرا در مجمرست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون لباس ارغوان رنگش نباشد داغ داغ؟</p></div>
<div class="m2"><p>لاله را در پیرهن از رشک رویت اخگرست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می چکد شهد حلاوت صائب از گفتار تو</p></div>
<div class="m2"><p>طوطی کلک ترا منقار گویا شکرست</p></div></div>