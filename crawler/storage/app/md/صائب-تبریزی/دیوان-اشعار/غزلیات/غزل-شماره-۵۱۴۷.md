---
title: >-
    غزل شمارهٔ ۵۱۴۷
---
# غزل شمارهٔ ۵۱۴۷

<div class="b" id="bn1"><div class="m1"><p>می کند از مهربانی حفظ طفل نوسوار</p></div>
<div class="m2"><p>آن که می دارد عنان اختیار از من دریغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آب می بندد به روی تشنگان کربلا</p></div>
<div class="m2"><p>هرکه دارد جام می را در خمار از من دریغ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از وجود خاکی من سرمه واری مانده است</p></div>
<div class="m2"><p>گوشه چشم مروت را مدار از من دریغ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست گل چیدن ندارد دیده حیران من</p></div>
<div class="m2"><p>وصل خود دارد چرا آن گلعذار از من دریغ؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قطره اش را چون صدف تشریف گوهر می دهم</p></div>
<div class="m2"><p>فیض خود دارد چرا ابر بهار از من دریغ؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون حنا هر چند خون من ندارد باز خواست</p></div>
<div class="m2"><p>پای بوس خویش دارد آن نگار از من دریغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می فشاند سنبل و ریحان به دامن شانه را</p></div>
<div class="m2"><p>آن که دارد بوی زلف مشکبار از من دریغ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کی دهد صائب مرا در بزم خاص خویش بار؟</p></div>
<div class="m2"><p>آن که دارد خاک راه انتظار از من دریغ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرمی گلگون ندارد روزگار از من دریغ</p></div>
<div class="m2"><p>سهل باشد فیض اگر دارد بهار از من دریغ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست آن بیرحم آگاه از دل سوزان من</p></div>
<div class="m2"><p>ورنه کی می دانست لعل آبدار از من دریغ؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گلستان را که پروردم به آب چشم خویش</p></div>
<div class="m2"><p>نکهت خود داشت در فصل بهار از من دریغ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر ندارد لطف پنهان با من آن امید گاه</p></div>
<div class="m2"><p>چون نمی دارد دل امیدوار از من دریغ؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آرزوی وصل چون گردد به گرد خاطرم ؟</p></div>
<div class="m2"><p>کان گل بی خار دارد خارخار از من دریغ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کی چراغ خلوت و شمع مزار من شود ؟</p></div>
<div class="m2"><p>آتشین رویی که می دارد شرار از من دریغ</p></div></div>