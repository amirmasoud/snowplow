---
title: >-
    غزل شمارهٔ ۵۷۲۰
---
# غزل شمارهٔ ۵۷۲۰

<div class="b" id="bn1"><div class="m1"><p>کنون که با تو مکان در یک انجمن دارم</p></div>
<div class="m2"><p>هزار مرحله ره تا به خویشتن دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مدار رزق به اقبال قسمت است که من</p></div>
<div class="m2"><p>در آستین شکر و زهر در دهن دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز صبح مهر تمنا کنم چه ساده دلم</p></div>
<div class="m2"><p>که چشم بخیه ز سر رشته کفن دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو گردباد غبار دل است جامه من</p></div>
<div class="m2"><p>به مرگ و زیست بس است این قبا که من دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سپر ز شبنم گل کرده ام ز ساده دلی</p></div>
<div class="m2"><p>سر مجادله با مهر تیغ زن دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمی شود سر خود در سخن نکنم</p></div>
<div class="m2"><p>چو خامه زخم نمایانی از سخن دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سپر فکندن من گرد من حصار بس است</p></div>
<div class="m2"><p>به این سلاح چه پروای تیغ زن دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا به جوشن داودی احتیاجی نیست</p></div>
<div class="m2"><p>ز جان سخت زره زیر پیرهن دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو شمع صبح بپا افتاده ام صائب</p></div>
<div class="m2"><p>سر وداع حریفان انجمن دارم</p></div></div>