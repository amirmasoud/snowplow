---
title: >-
    غزل شمارهٔ ۲۹۵۳
---
# غزل شمارهٔ ۲۹۵۳

<div class="b" id="bn1"><div class="m1"><p>ز آه عاشقان اندیشه‌ای اختر نمی‌دارد</p></div>
<div class="m2"><p>ز دود تلخ پروا دیده مجمر نمی‌دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به تلخی صبر کن تا معدن گوهر توانی شد</p></div>
<div class="m2"><p>که آب بحر چون شیرین شود گوهر نمی‌دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز آسیب شکستن نیست شاخ پرثمر ایمن</p></div>
<div class="m2"><p>غم فربه شدن صید مرا لاغر نمی‌دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه سازم بر جگر دندان نومیدی نیفشارم؟</p></div>
<div class="m2"><p>جراحت‌های پنهان بخیه دیگر نمی‌دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درین گلزار زیبنده است تاج زر به بینایی</p></div>
<div class="m2"><p>که چشم از پشت پای خود چو نرگس برنمی‌دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندارد حاصلی جز ناله پیوند تهی‌چشمان</p></div>
<div class="m2"><p>نیی کز چاه می‌آید برون شکر نمی‌دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خرد دارد غم دنیا، غرور عشق را نازم</p></div>
<div class="m2"><p>که گر افتد ز دستش هردو عالم، برنمی‌دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غنیمت دان درین عالم وصال سبزخطان را</p></div>
<div class="m2"><p>که باغ خُلد این ریحان جان‌پرور نمی‌دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز بخت تیرهٔ ما شد غبارآلود خط لعلش</p></div>
<div class="m2"><p>وگرنه آتش یاقوت خاکستر نمی‌دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به لوح ساده از روشن‌ضمیران صلح کن صائب</p></div>
<div class="m2"><p>که چون آیینه گردد صیقلی جوهر نمی‌دارد</p></div></div>