---
title: >-
    غزل شمارهٔ ۳۲۷۵
---
# غزل شمارهٔ ۳۲۷۵

<div class="b" id="bn1"><div class="m1"><p>از نظر بازی من چشم سخنگو گردد</p></div>
<div class="m2"><p>پرده خواب ز شوخی رم آهو گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون حنا کز سفر هند شود غالیه رنگ</p></div>
<div class="m2"><p>خون دل مشک در آن حلقه گیسو گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می شود تیره ز یاد گنه آیینه دل</p></div>
<div class="m2"><p>کز نمی سبزه زنگار به نیرو گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کیست هم پله شود با تو که از شرم، گهر</p></div>
<div class="m2"><p>می شود آب که در چشم ترازو گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طی شود در نفسی زندگیش همچو حباب</p></div>
<div class="m2"><p>سر هرکس که درین بحر هوا جو گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دخل بیجاست گران بر دل ارباب سخن</p></div>
<div class="m2"><p>که دماغ قلم آشفته به یک مو گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کاهلی غوطه به زنگار دهد جانها را</p></div>
<div class="m2"><p>بیشتر آب روان سبز درین جو گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صیقلی ساز دل تیره خود را صائب</p></div>
<div class="m2"><p>که دورو چون شود این آینه یکرو گردد</p></div></div>