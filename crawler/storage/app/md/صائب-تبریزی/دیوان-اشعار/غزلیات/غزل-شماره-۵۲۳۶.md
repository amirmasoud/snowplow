---
title: >-
    غزل شمارهٔ ۵۲۳۶
---
# غزل شمارهٔ ۵۲۳۶

<div class="b" id="bn1"><div class="m1"><p>نیست امروزی چو شبنم عشق من باروی گل</p></div>
<div class="m2"><p>در حریم بیضه خلوت داشتم با بوی گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آب چشم بلبلان آیینه داری می کند</p></div>
<div class="m2"><p>می نهد شبنم عبث آیینه بر زانوی گل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در گلستانی که رخسار تو گردد بی نقاب</p></div>
<div class="m2"><p>رنگ نتواند گرفتن خویش را بر روی گل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق در مستی عنان شرم می دارد نگاه</p></div>
<div class="m2"><p>ناله بلبل نپیچد از ادب با بوی گل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلبلان چون سر ز زیر بال بیرون آورند</p></div>
<div class="m2"><p>در گلستان که باشد خار همزانوی گل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فارغم از دور باش خار و منع باغبان</p></div>
<div class="m2"><p>من که از گل قانعم صائب به گفت و گوی گل</p></div></div>