---
title: >-
    غزل شمارهٔ ۲۲۰۱
---
# غزل شمارهٔ ۲۲۰۱

<div class="b" id="bn1"><div class="m1"><p>قد تو کجا و قد رعنای قیامت</p></div>
<div class="m2"><p>این جامه بلندست به بالای قیامت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای از مژه شوخ صف آرای قیامت</p></div>
<div class="m2"><p>وز زلف دلاویز دو بالای قیامت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دامن کهسار کم از خنده کبک است</p></div>
<div class="m2"><p>در پله تمکین تو غوغای قیامت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم جنتی از چهره و هم دوزخی از خوی</p></div>
<div class="m2"><p>نقدست در ایام تو سودای قیامت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خورشید تو چون از افق زلف برآید</p></div>
<div class="m2"><p>ریزد عرق شرم ز سیمای قیامت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از داغ بود گرمی هنگامه دلها</p></div>
<div class="m2"><p>خورشید بود انجمن آرای قیامت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در سینه ما سوختگان نم نتوان یافت</p></div>
<div class="m2"><p>بی آب بود دامن صحرای قیامت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از شرم گنه بس که کشیدم به زمین خط</p></div>
<div class="m2"><p>مسطر زده شد دامن صحرای قیامت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در سایه کوه گنه ما ز بلندی</p></div>
<div class="m2"><p>آسوده بود خلق ز گرمای قیامت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از سینه آتش نفسان دود برآید</p></div>
<div class="m2"><p>چون خامه صائب کند انشای قیامت</p></div></div>