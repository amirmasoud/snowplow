---
title: >-
    غزل شمارهٔ ۶۹۴۲
---
# غزل شمارهٔ ۶۹۴۲

<div class="b" id="bn1"><div class="m1"><p>ای غافلی که در پی دینار می روی</p></div>
<div class="m2"><p>آخر ز سکه در دهن مار می روی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسن مجاز را به حقیقت گزیده ای</p></div>
<div class="m2"><p>غافل مشو که روی به دیوار می روی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از غفلت تو پیر مغان در کشاکش است</p></div>
<div class="m2"><p>می در پیاله داری و هشیار می روی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاری است خار غصه که در پا نمی خلد</p></div>
<div class="m2"><p>تا پا برهنه بر سر این خار می روی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آفتاب دیده بد نور می برد</p></div>
<div class="m2"><p>ای ماه خانگی چه به بازار می روی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در قلزمی که کام نهنگ است هر صدف</p></div>
<div class="m2"><p>غواص نیستی و نگونسار می روی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشمت به نور شمسه ایوان عقل نیست</p></div>
<div class="m2"><p>از ره به رزق طره دستار می روی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آب حیات آتش افسرده، دامن است</p></div>
<div class="m2"><p>چندین ز حرف سرد چه از کار می روی؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در آستان خانه خود خاک می شوی</p></div>
<div class="m2"><p>از خود برون چنین که گرانبار می روی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صائب چه نشائه بود که چون چشم دلبران</p></div>
<div class="m2"><p>مست آمدی به عالم و بیمار می روی</p></div></div>