---
title: >-
    غزل شمارهٔ ۴۱۷۷
---
# غزل شمارهٔ ۴۱۷۷

<div class="b" id="bn1"><div class="m1"><p>مشکل دل رمیده هوای وطن کند</p></div>
<div class="m2"><p>شبنم چنان نرفت که یاد چمن کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنها که دید یوسف از اخوان سنگدل</p></div>
<div class="m2"><p>خونش به گردن است که یاد وطن کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل می کند به سینه ما بیدلان رجوع</p></div>
<div class="m2"><p>گر نافه بازگشت به ناف ختن کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلهای جمع را کند آشفته یاد من</p></div>
<div class="m2"><p>رازی نمی شوم که کسی یاد من کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی پرده نقش صورت شیرین نگاشته است</p></div>
<div class="m2"><p>تا انتقام عشق چه با کوهکن کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسیار رو مده دل عشاق را مباد</p></div>
<div class="m2"><p>زلف تورا گرانی دل بی شکن کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بال ملک چو برگ خزان دیده ریخته است</p></div>
<div class="m2"><p>پروانه را که یاد در آن انجمن کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب مرا ز درد سخن خورد وخواب نیست</p></div>
<div class="m2"><p>کو عیسیی که چاره درد سخن کند</p></div></div>