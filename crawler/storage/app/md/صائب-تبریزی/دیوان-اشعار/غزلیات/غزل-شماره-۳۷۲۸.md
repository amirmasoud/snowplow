---
title: >-
    غزل شمارهٔ ۳۷۲۸
---
# غزل شمارهٔ ۳۷۲۸

<div class="b" id="bn1"><div class="m1"><p>ز چهره تو بهشت آب و تاب بردارد</p></div>
<div class="m2"><p>ز جلوه تو قیامت حساب بردارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نصیب سوختگان می رسد ز پرده غیب</p></div>
<div class="m2"><p>همیشه آبله آب از سراب بردارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنین که خوی تو کرده است عام ناسازی</p></div>
<div class="m2"><p>عجب که آتش سوزان کباب بردارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان عقیق تو از خون خلق شد سیراب</p></div>
<div class="m2"><p>که از مشاهده اش زخم آب بردارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بغیر لخت دل و پاره جگر صائب</p></div>
<div class="m2"><p>چه توشه کس ز جهان خراب بردارد؟</p></div></div>