---
title: >-
    غزل شمارهٔ ۳۱۹۶
---
# غزل شمارهٔ ۳۱۹۶

<div class="b" id="bn1"><div class="m1"><p>دل از مژگان خواب‌آلود در زنهار می‌آید</p></div>
<div class="m2"><p>بلای جان بود تیغی که لنگردار می‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میانجی نیست حاجت نقطه و پرگار وحدت را</p></div>
<div class="m2"><p>سر همت بلندان خود به پای دار می‌آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندارد جنگ با هم شیوه مستوری و مستی</p></div>
<div class="m2"><p>ز جوش می به گوشم بانگ استغفار می‌آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز قید صد گره در یک گره می‌افکند خود را</p></div>
<div class="m2"><p>کسی کز حلقه تسبیح در زنار می‌آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو چون طفلان ز وصل گل به دیدن نیستی قانع</p></div>
<div class="m2"><p>وگرنه کار در از رخنه دیوار می‌آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خلاصی از ملامت نیست سرگرم محبت را</p></div>
<div class="m2"><p>سر خورشید هرجا رفت بر دیوار می‌آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محال است این که داغ لاله‌رویان در جگر ماند</p></div>
<div class="m2"><p>گل رنگین به سیر گوشه دستار می‌آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نواسنجی که در دل زخم خاری دارد از غیرت</p></div>
<div class="m2"><p>به جای ناله خون گرمش از منقار می‌آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سخن را صاف خواهی، لوح دل را صاف کن صائب</p></div>
<div class="m2"><p>که از آیینه طوطی بر سر گفتار می‌آید</p></div></div>