---
title: >-
    غزل شمارهٔ ۵۱۱۷
---
# غزل شمارهٔ ۵۱۱۷

<div class="b" id="bn1"><div class="m1"><p>زلف تو نرم شانه شد از گوشمال خط</p></div>
<div class="m2"><p>هر مویی ازتو شد شب عید از هلال خط</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گردد دعا به دامن شب بیش مستجاب</p></div>
<div class="m2"><p>نومید نیستیم ز حسن مآل خط</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دریای رحمتی است که موجش زعنبرست</p></div>
<div class="m2"><p>روی عرق فشان تو در زیر بال خط</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سودای زلف،حلقه بیرون در شود</p></div>
<div class="m2"><p>در هر دلی که ریشه دواند خیال خط</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قدر گهر ز گرد یتیمی شود زیاد</p></div>
<div class="m2"><p>در دل مده غبار ره از خاکمال خط</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناقص ز پیچ و تاب شود گر چه رشته ها</p></div>
<div class="m2"><p>گردد ز پیچ و تاب یکی صد کمال خط</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکر نزول آیه رحمت شکفتگی است</p></div>
<div class="m2"><p>پنهان مساز روی خود از انفعال خط</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از پیچ و تاب حلقه کند نام آفتاب</p></div>
<div class="m2"><p>اصلاح دست اگر نزند بر جمال خط</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از آب تیغ سبزه خط می شود بلند</p></div>
<div class="m2"><p>سعی از تراش چند کنی در زوال خط؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر از بهار سبز شود تخم سوخته</p></div>
<div class="m2"><p>آید برون ستاره خال از وبال خط</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مار از هجوم مور دل از گنج بر گرفت</p></div>
<div class="m2"><p>زلف تو کرد ترک جمال از جلال خط</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آوازه اش اگر چه جهانگیر گشته بود</p></div>
<div class="m2"><p>گلبانگ خوبی تو فزود از بلال خط</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هرچند بود زلف تو پردلی علم</p></div>
<div class="m2"><p>پس خم زد از غبار سپاه جلال خط</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نعلش در آتش است ز هر حلقه ای جدا</p></div>
<div class="m2"><p>نازش مکن به حسن سریع الزوال خط</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شد ملک حسن از ستم بی حساب تو</p></div>
<div class="m2"><p>زیر و زبر ز لشکر بی اعتدال خط</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صائب شد از دمیدن او سبز حرف من</p></div>
<div class="m2"><p>چون آب چشم خویش نسازم حلال خط؟</p></div></div>