---
title: >-
    غزل شمارهٔ ۲۷۴۶
---
# غزل شمارهٔ ۲۷۴۶

<div class="b" id="bn1"><div class="m1"><p>برگرفتی پرده از رخ گلستان آمد پدید</p></div>
<div class="m2"><p>آستین ناز افشاندی خزان آمد پدید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاکدان دهر مفلس بود از نقد مراد</p></div>
<div class="m2"><p>دستها بر هم زدی دریا و کان آمد پدید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا شعوری داشتم می کرد وصل از من کنار</p></div>
<div class="m2"><p>من چو رفتم از میان آن خوش میان آمد پدید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشمه خورشید در گرد کدورت غوطه زد</p></div>
<div class="m2"><p>تا غبار خط ز روی دلستان آمد پدید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم را خواباند، چندین فتنه را بیدار کرد</p></div>
<div class="m2"><p>زلف را افشاند، عمر جاودان آمد پدید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در حریم نیستی بالا وپایینی نبود</p></div>
<div class="m2"><p>من چو گشتم خاک، خاک آستان آمد پدید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کلک گوهربار صائب تا سخن پرداز شد</p></div>
<div class="m2"><p>زنده رود تازه ای در اصفهان آمد پدید</p></div></div>