---
title: >-
    غزل شمارهٔ ۲۸۷
---
# غزل شمارهٔ ۲۸۷

<div class="b" id="bn1"><div class="m1"><p>زیر شمشیر حوادث پای بر جاییم ما</p></div>
<div class="m2"><p>رو نمی تابیم از سیلاب، دریاییم ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرده غفلت نمی گردد بصیرت را حجاب</p></div>
<div class="m2"><p>گر چه از پوشیده چشمانیم، بیناییم ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مطلب ما گوهر عبرت به دست آوردن است</p></div>
<div class="m2"><p>گر به ظاهر همچو طفلان در تماشاییم ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شبنم ما را ز گل آتش بود در زیر پا</p></div>
<div class="m2"><p>کز نظربازان آن خورشید سیماییم ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وحشت ما کم نگردد ز اجتماع دوستان</p></div>
<div class="m2"><p>چون الف با هر چه پیوندیم، تنهاییم ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست خواب غفلت ما را به بیداری امید</p></div>
<div class="m2"><p>چون ره خوابیده در دامان صحراییم ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کرده ایم از خودحسابی نقد بر خود حشر را</p></div>
<div class="m2"><p>فارغ از اندیشه دیوان فرداییم ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با رفیقان موافق، بند و زندان گلشن است</p></div>
<div class="m2"><p>هر که شد دیوانه، چون زنجیر همپاییم ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیل نتواند غبار ما ز کوی یار برد</p></div>
<div class="m2"><p>کز نظربندان آن مژگان گیراییم ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیش پا دیدن ز ما صائب نمی آید چو شمع</p></div>
<div class="m2"><p>بس که محو جلوه آن قد رعناییم ما</p></div></div>