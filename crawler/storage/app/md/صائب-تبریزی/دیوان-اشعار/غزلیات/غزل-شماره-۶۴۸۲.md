---
title: >-
    غزل شمارهٔ ۶۴۸۲
---
# غزل شمارهٔ ۶۴۸۲

<div class="b" id="bn1"><div class="m1"><p>عقده ای نگشود آزادی ز کارم همچو سرو</p></div>
<div class="m2"><p>زیر بار دل سرآمد روزگارم همچو سرو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه ز اسباب جهان یک جامه دارم در بساط</p></div>
<div class="m2"><p>زیر بار منت چندین بهارم همچو سرو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>محو نتوان ساختن از صفحه خاطر مرا</p></div>
<div class="m2"><p>مصرع برجسته باغ و بهارم همچو سرو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاطر آزاده من فارغ است از انقلاب</p></div>
<div class="m2"><p>در بهار و در خزان بر یک قرارم همچو سرو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه برگشتن ندارد جویبار زندگی</p></div>
<div class="m2"><p>بر سر یک پا همان در انتظارم همچو سرو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از رعونت نقش هستی در بساطم زنگ بست</p></div>
<div class="m2"><p>آب روشن گر چه بود آیینه دارم همچو سرو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا به زانو پایم از گرد کدورت در گل است</p></div>
<div class="m2"><p>گر چه دایم در کنار جویبارم همچو سرو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طوق قمری در بساطم چشم حیرت می شود</p></div>
<div class="m2"><p>بس که سرگرم تماشای بهارم همچو سرو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سایه من میکشان را دامگاه عشرت است</p></div>
<div class="m2"><p>میوه ای هر چند در ظاهر ندارم همچو سرو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باغ را بی برگ در فصل خزان نگذاشتم</p></div>
<div class="m2"><p>کام تلخی گر نشد شیرین ز بارم همچو سرو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سر برون از یک گریبان کرده ام با راستی</p></div>
<div class="m2"><p>نیست فرقی در نهان و آشکارم همچو سرو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نشکند چون پشت شاخ میوه دار از غیرتم؟</p></div>
<div class="m2"><p>با تهیدستی رخ خود تازه دارم همچو سرو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سرفرازی نیست از نشو و نما مطلب مرا</p></div>
<div class="m2"><p>خواهم از گل ریشه خود را برآرم همچو سرو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیست بر تحسین بلبل گوش من چون شاخ گل</p></div>
<div class="m2"><p>زین گلستان با خود افتاده است کارم همچو سرو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سبزه بختم درین بستانسرا پامال شد</p></div>
<div class="m2"><p>پنجه ای رنگین نگردید از نگارم همچو سرو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن کهن گبرم که از طوق گلوی قمریان</p></div>
<div class="m2"><p>بر میان صد حلقه زنار دارم همچو سرو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فرصت خاریدن سر نیست از حیرت مرا</p></div>
<div class="m2"><p>دست خود را در بغل پیوسته دارم همچو سرو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یک سر مو نیست از تیغ زبان اندیشه ام</p></div>
<div class="m2"><p>می کند پیرایش افزون اعتبارم همچو سرو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خجلت روی زمین از سنگ طفلان می کشم</p></div>
<div class="m2"><p>بس که از بی حاصلی ها شرمسارم همچو سرو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شمع سبز من به کوری سوخت در بزم وجود</p></div>
<div class="m2"><p>آتشین بالی نشد هرگز دچارم همچو سرو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گرچه برگ و بار من غیر از کف افسوس نیست</p></div>
<div class="m2"><p>از برومندی همان امیدوارم همچو سرو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>میوه من جز گزیدن های پشت دست نیست</p></div>
<div class="m2"><p>منفعل از التفات نوبهارم همچو سرو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>برگ عیش نوبهاران است روی تازه ام</p></div>
<div class="m2"><p>درخزان از نوبهاران یادگارم همچو سرو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زنگ ذاتی را به خاکستر ز دل نتوان زدود</p></div>
<div class="m2"><p>دست پیش قمریان تا چند دارم همچو سرو؟</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بار من آزادگی و برگ من دست دعاست</p></div>
<div class="m2"><p>حرز جان باغ و تعویذ بهارم همچو سرو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کوه را از پا درآرد تنگدستی ها و من</p></div>
<div class="m2"><p>سالها شد خویش را بر پای دارم همچو سرو</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر چه گل بر هیچ کس دست دراز من نزد</p></div>
<div class="m2"><p>شد کبود از سیلی دوران عذارم همچو سرو</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نارسایی داردم از سنگ طفلان بی نصیب</p></div>
<div class="m2"><p>ورنه از دل شیشه ها در بار دارم همچو سرو</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بس که خوردم زهر غم، چون ریزد از هم پیکرم</p></div>
<div class="m2"><p>سبزپوش از خاک برخیزد غبارم همچو سرو</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در چنین فصلی که گل از پوست می آید برون</p></div>
<div class="m2"><p>دست را تا کی به روی هم گذرم همچو سرو؟</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>با هزاران دست، دایم بود در دست نسیم</p></div>
<div class="m2"><p>صائب از حیرت عنان اختیارم همچو سرو</p></div></div>