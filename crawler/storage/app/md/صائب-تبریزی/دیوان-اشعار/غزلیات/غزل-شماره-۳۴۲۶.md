---
title: >-
    غزل شمارهٔ ۳۴۲۶
---
# غزل شمارهٔ ۳۴۲۶

<div class="b" id="bn1"><div class="m1"><p>هر که ایام خط از سیمبران غافل شد</p></div>
<div class="m2"><p>از شب قدر به ماه رمضان غافل شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیخودی می کند اوضاع جهان را هموار</p></div>
<div class="m2"><p>وای بر آن که ازین خواب گران غافل شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزی بسته دهانان رسد از غیب، چرا</p></div>
<div class="m2"><p>از دل تنگ من آن غنچه دهان غافل شد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با قدم خم شده خوش نیست پریشان نظری</p></div>
<div class="m2"><p>در کمانخانه نباید ز نشان غافل شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به فلک می رسد این دود کبابی که مراست</p></div>
<div class="m2"><p>از دل سوخته من نتوان غافل شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که از پشت ورق روی ورق را خوانده است</p></div>
<div class="m2"><p>در بهاران نتواند ز خزان غافل شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رتبه جاذبه عشق بلند افتاده است</p></div>
<div class="m2"><p>بر فلک مه نتواند ز کتان غافل شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یوسف از سیلی اخوان به غریبی افتاد</p></div>
<div class="m2"><p>چون تواند کس از ابنای زمان غافل شد؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یاد آن جان جهان است حیاتی گر هست</p></div>
<div class="m2"><p>مرده دل آن که ازان جان جهان غافل شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دامن بخت جوان رفت ز دستش بیرون</p></div>
<div class="m2"><p>صائب آن روز که از پیر مغان غافل شد</p></div></div>