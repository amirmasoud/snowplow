---
title: >-
    غزل شمارهٔ ۲۲۷۳
---
# غزل شمارهٔ ۲۲۷۳

<div class="b" id="bn1"><div class="m1"><p>دور کن از دل هوس در پیرهن اخگر مپیچ</p></div>
<div class="m2"><p>بگسل از طول امل، چون مار در بستر مپیچ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کار خود چون کوهکن با تیشه خود کن تمام</p></div>
<div class="m2"><p>بیش ازین در انتظار تیغ چون جوهر مپیچ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل چو روشن شد به باد نیستی ده جسم را</p></div>
<div class="m2"><p>خط پاکی چون به دست افتاد در دفتر مپیچ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با فلک چندان مدارا کن که دل صافی شود</p></div>
<div class="m2"><p>چون شود آیینه ات روشن، به خاکستر مپیچ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ره دوری که نقش بال وپر باشد وبال</p></div>
<div class="m2"><p>رشته دام علایق را به بال وپر مپیچ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دردمندان را به قدر زخم باشد فتح باب</p></div>
<div class="m2"><p>از حوادث، تیغ اگر بارد به فرقت، سرمپیچ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا توان پیچید در ساقی به شبهای دراز</p></div>
<div class="m2"><p>کوته اندیشی مکن در شیشه و ساغر مپیچ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رنج باریک آورد آمیزش سیمین بران</p></div>
<div class="m2"><p>اینقدر ای رشته باریک بر گوهر مپیچ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با کمند عنکبوتان صید عنقا مشکل است</p></div>
<div class="m2"><p>بیش ازین صائب به فکر آن پری پیکر مپیچ</p></div></div>