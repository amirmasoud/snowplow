---
title: >-
    غزل شمارهٔ ۲۵۴
---
# غزل شمارهٔ ۲۵۴

<div class="b" id="bn1"><div class="m1"><p>گر نظربازی به بال خود کند طاوس ما</p></div>
<div class="m2"><p>جوید از بهر رهایی روزنی محبوس ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غربت ما دردمندان، پله آزادگی است</p></div>
<div class="m2"><p>نیست جز دام و قفس جای دگر مأنوس ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پنجه با زور جنون کردن نه کار هر کس است</p></div>
<div class="m2"><p>سنگ می لرزد به خود از شیشه ناموس ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست خود را چون صدف بر روی هم نگذاشتیم</p></div>
<div class="m2"><p>تا نشد گنجینه گوهر کف افسوس ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چه یار از حال ما هرگز نمی گیرد خبر</p></div>
<div class="m2"><p>خلوت آیینه خالی نیست از جاسوس ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تازه گردد در دل پرشور ما داغ کهن</p></div>
<div class="m2"><p>می شود روشن چراغ کشته در فانوس ما</p></div></div>