---
title: >-
    غزل شمارهٔ ۵۹۱۸
---
# غزل شمارهٔ ۵۹۱۸

<div class="b" id="bn1"><div class="m1"><p>در پله آغاز ز انجام گذشتیم</p></div>
<div class="m2"><p>از مصر برون نامده از شام گذشتیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون برق فتادیم به خاشاک تعلق</p></div>
<div class="m2"><p>زین خاک جلوگیر به یک گام گذشتیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ابر سفید و لب خاموش خطرهاست</p></div>
<div class="m2"><p>از گردن مینا و لب جام گذشتیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی نقطه شب یک الف روز ندیدیم</p></div>
<div class="m2"><p>هر چند که بر صفحه ایام گذشتیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در طالع ما نیست گرفتاری، اگرنه</p></div>
<div class="m2"><p>صد بار فزون از نظر دام گذشتیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>الماس ندامت دل ما را نخراشید</p></div>
<div class="m2"><p>تا همچو عقیق از هوس نام گذشتیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از سود و زیان سفر عشق مپرسید</p></div>
<div class="m2"><p>یک دانه نچیدیم و به صد دام گذشتیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در سایه شمشیر شهادت نتپیدیم</p></div>
<div class="m2"><p>زین قلزم خونخوار به آرام گذشتیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در گلشن بیرنگ جهان چون گل خورشید</p></div>
<div class="m2"><p>گر صبح شکفتیم سر شام گذشتیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در باغ جهان چون ثمر نخل تمنا</p></div>
<div class="m2"><p>صد حیف که خام آمده و خام گذشتیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این آن غزل میر فصیحی است که فرمود</p></div>
<div class="m2"><p>از پشته صبح و دره شام گذشتیم</p></div></div>