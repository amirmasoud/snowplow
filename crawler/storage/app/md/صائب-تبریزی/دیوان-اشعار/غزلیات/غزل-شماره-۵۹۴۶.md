---
title: >-
    غزل شمارهٔ ۵۹۴۶
---
# غزل شمارهٔ ۵۹۴۶

<div class="b" id="bn1"><div class="m1"><p>چشم گشایش از خلق نبود به هیچ بابم</p></div>
<div class="m2"><p>در بزم بیسوادان لب بسته چون کتابم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ملک بی نشانی ازمن چه جرم سر زد؟</p></div>
<div class="m2"><p>کز شش جهت فکندند در پنجه عقابم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چند کشتی من بر خشک بسته گردون</p></div>
<div class="m2"><p>نومید بر نگشته است یک تشنه از سرابم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محو محیط وحدت مستغرق وصال است</p></div>
<div class="m2"><p>من از ره تعین سرگشته چون حبابم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در زهد خشک باشد پوشیده مشرب من</p></div>
<div class="m2"><p>آب حیاتم اما خس پوش از سرابم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون ماه نو تواضع با خاکیان نمایم</p></div>
<div class="m2"><p>با آن شکوه گردون گیرد اگر رکابم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرگز دلم نبوده است بی داغ عشق صائب</p></div>
<div class="m2"><p>چسبیده است دایم بر اخگری کبابم</p></div></div>