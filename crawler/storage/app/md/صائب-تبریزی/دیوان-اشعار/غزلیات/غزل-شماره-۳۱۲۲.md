---
title: >-
    غزل شمارهٔ ۳۱۲۲
---
# غزل شمارهٔ ۳۱۲۲

<div class="b" id="bn1"><div class="m1"><p>بهشتی بی دماغان را به از خلوت نمی باشد</p></div>
<div class="m2"><p>گلابی بهتر از پاشیدن صحبت نمی باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجو از گفتگوی زاهدان خشک کیفیت</p></div>
<div class="m2"><p>که جز ریگ روان در شیشه ساعت نمی باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نصیب سرو از استادگی شد خط آزادی</p></div>
<div class="m2"><p>به آزادی رسد چون بنده کم خدمت نمی باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به فکر عذرپردازی مکن اوقات را ضایع</p></div>
<div class="m2"><p>که عصیان را شفیعی بهتر از خجلت نمی باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندارم شکوه ای از تیره بختی با دل روشن</p></div>
<div class="m2"><p>که آب زندگی بی پرده ظلمت نمی باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر از روشندلانی با رمیدن رام کن دل را</p></div>
<div class="m2"><p>که آسایش درین صحرای پروحشت نمی باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زدست خود سلیمان داد پای تخت موران را</p></div>
<div class="m2"><p>تواضع با فقیران نقص در دولت نمی باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نمی ریزیم غافل بر سر دشمن چو نامردان</p></div>
<div class="m2"><p>که می گردد مظفر هر که کم فرصت نمی باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو دیدم بر سر تاج زر خود شمع را لرزان</p></div>
<div class="m2"><p>یقینم شد که خواب امن با دولت نمی باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز انگشت اشارت در گریبان خارها دارم</p></div>
<div class="m2"><p>بلایی آدمی را بدتر از شهرت نمی باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به گردون برد همت شبنم افتاده را آخر</p></div>
<div class="m2"><p>به جایی می رسد هر کس که دون همت نمی باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چه می لرزی چو شاهین بر سر بیش و کم روزی؟</p></div>
<div class="m2"><p>به میزان عدالت میل در قسمت نمی باشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به غربت از وطن چون ماه کنعان صلح کن صائب</p></div>
<div class="m2"><p>که جز یاد وطن مکروه در غربت نمی باشد</p></div></div>