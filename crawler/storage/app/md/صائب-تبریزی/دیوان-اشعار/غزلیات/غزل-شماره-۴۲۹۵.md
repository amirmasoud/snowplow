---
title: >-
    غزل شمارهٔ ۴۲۹۵
---
# غزل شمارهٔ ۴۲۹۵

<div class="b" id="bn1"><div class="m1"><p>هر رهروی دچار به منزل نمی‌شود</p></div>
<div class="m2"><p>این راه قطع بی‌کشش دل نمی‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنجیر موج مانع شور محیط نیست</p></div>
<div class="m2"><p>مجنون ما به سلسله عاقل نمی‌شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گلگونه خجالت روح است روز حشر</p></div>
<div class="m2"><p>خونی که زیب دامن قاتل نمی‌شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نتوان به ماه نو گره آسمان گشود</p></div>
<div class="m2"><p>ناخن حریف آبله دل نمی‌شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در عشق شو چو سرو و صنوبر تمام دل</p></div>
<div class="m2"><p>کاین کار دلخوری است، به یک دل نمی‌شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حاصل شد از لب شکرین تو کام مور</p></div>
<div class="m2"><p>کام دل من است که حاصل نمی‌شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ابر تنک نهان نکند آفتاب را</p></div>
<div class="m2"><p>لیلی نهان به پرده محمل نمی‌شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک ساعت است جلوه عاشق درین جهان</p></div>
<div class="m2"><p>پروانه بال خاطر محفل نمی‌شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چندان که می‌رود به مقامی نمی‌رسد</p></div>
<div class="m2"><p>آواره‌ای که همسفر دل نمی‌شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از باد نخوت است که خاکش به فرق باد</p></div>
<div class="m2"><p>با بحر هر حباب که واصل نمی‌شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عارف ز موج حادثه بر هم نمی‌خورد</p></div>
<div class="m2"><p>از شور بحر، آب گهر گل نمی‌شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دستی که از دو کون نشویند همچو موج</p></div>
<div class="m2"><p>در گردن محیط حمایل نمی‌شود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خال سفیدی از نظر دوربین ما</p></div>
<div class="m2"><p>بی‌سرمه سیاهی منزل نمی‌شود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون قبله‌گاه حاجت عالم همین درست</p></div>
<div class="m2"><p>صائب چرا گدای در دل نمی‌شود</p></div></div>