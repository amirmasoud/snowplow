---
title: >-
    غزل شمارهٔ ۱۰۳۲
---
# غزل شمارهٔ ۱۰۳۲

<div class="b" id="bn1"><div class="m1"><p>داستان شوق را تحریر کردن مشکل است</p></div>
<div class="m2"><p>بحر را از موج در زنجیر کردن مشکل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بند پیش سیل بی زنهار نتواند گرفت</p></div>
<div class="m2"><p>بی قرار شوق را زنجیر کردن مشکل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با تهی چشمان چه سازد نعمت روی زمین؟</p></div>
<div class="m2"><p>چشم روزن را ز پرتو سیر کردن مشکل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می توان ز افسانه کردن چشم آهو را به خواب</p></div>
<div class="m2"><p>چشم عیار ترا تسخیر کردن مشکل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دستگیری نیست پیری را به جز افتادگی</p></div>
<div class="m2"><p>این کهن دیوار را تعمیر کردن مشکل است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواب زاهد تلخ گردیده است از یاد بهشت</p></div>
<div class="m2"><p>کودکان را ترک جوی شیر کردن مشکل است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتگوی اهل غفلت قابل تأویل نیست</p></div>
<div class="m2"><p>خواب پای خفته را تعبیر کردن مشکل است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>معنی پیچیده می پیچد زبان تقریر را</p></div>
<div class="m2"><p>آیه آن زلف را تفسیر کردن مشکل است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هست زیر آسمان امنیت خاطر محال</p></div>
<div class="m2"><p>خواب راحت در دهان شیر کردن مشکل است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با صف مژگان نظربازی نه کار هر کس است</p></div>
<div class="m2"><p>دیده را آماجگاه تیر کردن مشکل است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خط غباری نیست کز وی دل توان برداشتن</p></div>
<div class="m2"><p>چاره این خاک دامنگیر کردن مشکل است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تشنگی نتوان به شبنم بردن از ریگ روان</p></div>
<div class="m2"><p>دیده نادیدگان را سیر کردن مشکل است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با خیال خشک تا کی سر به یک بالین نهم؟</p></div>
<div class="m2"><p>دست در آغوش با تصویر کردن مشکل است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیست جز تسلیم صائب هیچ درمان عشق را</p></div>
<div class="m2"><p>پنجه در سر پنجه تقدیر کردن مشکل است</p></div></div>