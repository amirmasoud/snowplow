---
title: >-
    غزل شمارهٔ ۳۸۷۱
---
# غزل شمارهٔ ۳۸۷۱

<div class="b" id="bn1"><div class="m1"><p>خوشا کسی که ز خود باخبر نمی باشد</p></div>
<div class="m2"><p>که آه بی اثران بی اثر نمی باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمی شود دل بیتاب از خدا غافل</p></div>
<div class="m2"><p>ز قبله قبله نما بیخبر نمی باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه حاجت است به ارشاد عزم صادق را</p></div>
<div class="m2"><p>دلیل قافله را راهبر نمی باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز مغز نیست سخنهای پوچ ما خالی</p></div>
<div class="m2"><p>حباب قلزم ما بی گهر نمی باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گردنی که زبند لباس شد آزاد</p></div>
<div class="m2"><p>دو شاخه ای ز گریبان بتر نمی باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دهان تلخ برون می برم ز گلزاری</p></div>
<div class="m2"><p>که سرو وبید در او بی ثمر نمی باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صفای دل ز جهان بی نیاز کرد مرا</p></div>
<div class="m2"><p>که روی آینه محتاج زر نمی باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به خون دل ز می ناب صلح کن صائب</p></div>
<div class="m2"><p>که غیر خون می بی دردسرنمی باشد</p></div></div>