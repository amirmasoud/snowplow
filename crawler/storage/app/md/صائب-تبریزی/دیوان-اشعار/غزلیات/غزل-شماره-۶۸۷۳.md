---
title: >-
    غزل شمارهٔ ۶۸۷۳
---
# غزل شمارهٔ ۶۸۷۳

<div class="b" id="bn1"><div class="m1"><p>زبان شکوه اگر همچو خار داشتمی</p></div>
<div class="m2"><p>همیشه خرمن گل در کنار داشتمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار خانه چو زنبور کردمی پر شهد</p></div>
<div class="m2"><p>اگر گزیدن مردم شعار داشتمی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز دست راست ندانستمی اگر چپ را</p></div>
<div class="m2"><p>چه گنج ها به یمین و یسار داشتمی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به ابر اگر دهن خود گشودمی چو صدف</p></div>
<div class="m2"><p>هزار عقد گهر در کنار داشتمی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گرد شمع تو پروانه وار می گشتم</p></div>
<div class="m2"><p>اگر به گردش خود اختیار داشتمی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به درد عشق اگر مبتلا نمی گشتم</p></div>
<div class="m2"><p>چه دلخوشی من ازین روزگار داشتمی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خزان فسرده نمی کرد روزگار مرا</p></div>
<div class="m2"><p>اگر امید جنون از بهار داشتمی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر غبار تعلق فشاندمی از خویش</p></div>
<div class="m2"><p>دل سبک چو نسیم بهار داشتمی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر غبار دل خود نشستمی به سرشک</p></div>
<div class="m2"><p>هزار قافله در زیر بار داشتمی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قفس به دوش سفر کردمی ازین گلشن</p></div>
<div class="m2"><p>اگر ز درد طلب خارخار داشتمی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر به عالم بیرنگیم فتادی چشم</p></div>
<div class="m2"><p>کجا نظر به خزان و بهار داشتمی؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز آه کشتی دل بادبان اگر می داشت</p></div>
<div class="m2"><p>ازین محیط امید کنار داشتمی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گذشته بودی اگر دل ز پرده اسباب</p></div>
<div class="m2"><p>کجا ز چرخ به خاطر غبار داشتمی؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به عیب خویش اگر راه بردمی صائب</p></div>
<div class="m2"><p>به عیبجوئی مردم چه کار داشتمی؟</p></div></div>