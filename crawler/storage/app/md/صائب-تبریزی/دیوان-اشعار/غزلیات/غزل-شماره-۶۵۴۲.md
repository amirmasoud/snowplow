---
title: >-
    غزل شمارهٔ ۶۵۴۲
---
# غزل شمارهٔ ۶۵۴۲

<div class="b" id="bn1"><div class="m1"><p>مپسند پر ز داغ کنم از جفای تو</p></div>
<div class="m2"><p>آن کیسه ها که دوخته ام بر وفای تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در جبهه ستاره من این فروغ نیست</p></div>
<div class="m2"><p>یارب به طالع که شدم مبتلای تو؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طومار شکوه را نکنم طی به حرف و صوت</p></div>
<div class="m2"><p>تا همچو زلف سرنگذارم به پای تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیمانه ای که دست تو باشد در آن میان</p></div>
<div class="m2"><p>گر زهر قاتل است بنوشم برای تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چند می کشد ز درازی به روی خاک</p></div>
<div class="m2"><p>دست که می رسد به دو زلف رسای تو؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آب خضر ز چشمه سوزن روان شود</p></div>
<div class="m2"><p>آید چو در حدیث لب جانفزای تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شرم تو گفتم از خط شبرنگ کم شود</p></div>
<div class="m2"><p>یک پرده هم فزود ز خط بر حیای تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیگانه پروری چو تو در کاینات نیست</p></div>
<div class="m2"><p>بیچاره عاشقی که شود آشنای تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شادم به مرگ خود که هلاک تو می شوم</p></div>
<div class="m2"><p>با زندگی خوشم که بمیرم برای تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دایم به روی دست دعا جلوه می کنی</p></div>
<div class="m2"><p>هرگز ندیده است کسی نقش پای تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرگز ز ناز اگر چه به دنبال ننگری</p></div>
<div class="m2"><p>افتاده اند هر دو جهان در قفای تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بسیار در لطافت دل سعی می کنی</p></div>
<div class="m2"><p>از پرده دل است همانا قبای تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بیگانه وار می نگری در مثال خویش</p></div>
<div class="m2"><p>من چون کنم به این دل دیرآشنای تو؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فارغ بود ز جلوه رنگین نوبهار</p></div>
<div class="m2"><p>هر کس که چید گل ز خزان حنای تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خط هم دمید و گوش نکردی به حرف من</p></div>
<div class="m2"><p>داد مرا مگر ز تو گیرد خدای تو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر بشنوی ازو دو سه حرفی چه می شود؟</p></div>
<div class="m2"><p>صائب چها شنید ز مردم برای تو</p></div></div>