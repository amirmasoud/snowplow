---
title: >-
    غزل شمارهٔ ۶۷۶۱
---
# غزل شمارهٔ ۶۷۶۱

<div class="b" id="bn1"><div class="m1"><p>ز زلف پرشکن بتخانه چین است پنداری</p></div>
<div class="m2"><p>ز خال مشکبو آهوی مشکین است پنداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان شد از شراب لعل رنگین چشم مخمورش</p></div>
<div class="m2"><p>که هر مژگان شوخش تیغ خونین است پنداری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رسا افتاده است از بس کمند زلف مشکینش</p></div>
<div class="m2"><p>همیشه پشت پای او نگارین است پنداری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگه دارد خدا از چشم بد، رعناییی دارد</p></div>
<div class="m2"><p>که بر روی زمین در خانه زین است پنداری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازان رخسار عالمسوز در دل آتشی دارم</p></div>
<div class="m2"><p>که هر مو بر سر من شمع بالین است پنداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نپردازد به خار پای خود از بی دماغی ها</p></div>
<div class="m2"><p>ز شبنم چشم گل در راه گلچین است پنداری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکوهی در نظر جا کرده از حسن گرانسنگش</p></div>
<div class="m2"><p>که با شوخی سراپا کوه تمکین است پنداری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهاران رفت و بلبل مهر از لب برنمی دارد</p></div>
<div class="m2"><p>گل این بوستان گوش سخن چین است پنداری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز بس نازک شده است از گریه صائب پرده چشمم</p></div>
<div class="m2"><p>نگه در دیده من خواب سنگین است پنداری</p></div></div>