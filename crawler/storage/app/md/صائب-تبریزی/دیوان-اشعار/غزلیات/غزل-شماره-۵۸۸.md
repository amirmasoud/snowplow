---
title: >-
    غزل شمارهٔ ۵۸۸
---
# غزل شمارهٔ ۵۸۸

<div class="b" id="bn1"><div class="m1"><p>نداد عشق گریبان به دست کس ما را</p></div>
<div class="m2"><p>گرفت این می پر زور، چون عسس ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر چه سگ به مرس می کشند صیادان</p></div>
<div class="m2"><p>کشیده است سگ نفس در مرس ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تمام روز ازان همچو شمع، خاموشیم</p></div>
<div class="m2"><p>که خرج آه سحر می شود نفس ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فغان که منزل دور و دراز وادی عشق</p></div>
<div class="m2"><p>نکرد دل تهی از ناله چون جرس ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خراب حالی ما لشکری نمی خواهد</p></div>
<div class="m2"><p>بس است آمدن و رفتن نفس ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترا که پای گلی هست، می به ساغر کن</p></div>
<div class="m2"><p>که زهد خشک کشیده است در قفس ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به گرد خاطر ما آرزو نمی گردید</p></div>
<div class="m2"><p>لب تو ریخت به دل رنگ صد هوس ما را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر چه در قفس افتاده ایم از گلزار</p></div>
<div class="m2"><p>ز گل نمی گسلد رشته نفس ما را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکسته بال و پرانیم، جای آن دارد</p></div>
<div class="m2"><p>که باغبان کند از چوب گل قفس ما را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غریب گشت چنان فکرهای ما صائب</p></div>
<div class="m2"><p>که نیست چشم به تحسین هیچ کس ما را</p></div></div>