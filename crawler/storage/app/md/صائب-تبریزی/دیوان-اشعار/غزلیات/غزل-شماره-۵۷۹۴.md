---
title: >-
    غزل شمارهٔ ۵۷۹۴
---
# غزل شمارهٔ ۵۷۹۴

<div class="b" id="bn1"><div class="m1"><p>چون زلف دست بر کمر یار یافتم</p></div>
<div class="m2"><p>سر رشته نزاکت ز نار یافتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون شبنم به چهره گل جای می دهند</p></div>
<div class="m2"><p>این منزلت ز دیده بیدار یافتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آخر چو شبنم از اثر صاف طینتی</p></div>
<div class="m2"><p>در پیشگاه خاطر گل بار یافتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن دولتی که بال هما صفحه ای ازوست</p></div>
<div class="m2"><p>در زیر چتر سایه دیوار یافتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میراب زندگی ز حیات ابد نیافت</p></div>
<div class="m2"><p>فیضی که من ز چشم گهربار یافتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طاق پل مجاز، اساس حقیقت است</p></div>
<div class="m2"><p>من ساق عرش را ز سر دار یافتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از عشق ره به مرتبه حسن برده ام</p></div>
<div class="m2"><p>آب گهر ز رنگ خریدار یافتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا چشم همچو غنچه گشودم ز یکدگر</p></div>
<div class="m2"><p>خود را چو خار بر سر دیوار یافتم</p></div></div>