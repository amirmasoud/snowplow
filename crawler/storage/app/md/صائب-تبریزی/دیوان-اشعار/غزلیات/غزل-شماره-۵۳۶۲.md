---
title: >-
    غزل شمارهٔ ۵۳۶۲
---
# غزل شمارهٔ ۵۳۶۲

<div class="b" id="bn1"><div class="m1"><p>رفت آن عهدی که من بی یار ساغر می زدم</p></div>
<div class="m2"><p>بر کمر دارم کنون دستی که بر سر می زدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود طرق قمریان انگشتر پا سرو را</p></div>
<div class="m2"><p>در گلستانی که من با او سراسر می زدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می دواندم ریشه در دل قاتل بیرحم را</p></div>
<div class="m2"><p>زیر تیغش پیچ وتابی گر چو جوهر می زدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با کمال تنگدستی از شکست آرزو</p></div>
<div class="m2"><p>خارو خس در دیده تنگ توانگر می زدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جوشن داودیی از عشق اگر می داشتم</p></div>
<div class="m2"><p>خویش را بر قلب آتش چون سمندر می زدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زنگ کلفت را اگر می شد برون دادن زدل</p></div>
<div class="m2"><p>خیمه با گردون زنگاری برابر می زدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بوریا بر خاک پشت دست خود را می گذاشت</p></div>
<div class="m2"><p>هر کجا من تکیه باپهلوی لاغر می زدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این که کردم حلقه درها دو چشم خویش را</p></div>
<div class="m2"><p>کاش دل را حلقه امید بر در می زدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جلوه لشکر تن تنها کند در دیده ام</p></div>
<div class="m2"><p>من که تنها چون علم بر قلب لشکر می زدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می کشم اکنون الف چون سوزن از بهر خزف</p></div>
<div class="m2"><p>من که همچون رشته پشت پا به گوهر می زدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نی به ناخن می کند دوران تلخم این زمان</p></div>
<div class="m2"><p>زان تغافلها که من بر تنگ شکر می زدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می کنم آب حیات خویش صرف شوره زار</p></div>
<div class="m2"><p>من که از نخوت سیاهی بر سکندر می زدم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>می شدم خامش اگر چون شانه باچندین زبان</p></div>
<div class="m2"><p>دست در دامان آن زلف معنبر می زدم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صائب اکنون بادهان خشک و چشم تو خوشم</p></div>
<div class="m2"><p>من که استغنا به خلد و آب کوثر می زدم</p></div></div>