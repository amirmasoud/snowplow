---
title: >-
    غزل شمارهٔ ۱۷۷۰
---
# غزل شمارهٔ ۱۷۷۰

<div class="b" id="bn1"><div class="m1"><p>چراغ صبح و دم مستعار هر دو یکی است</p></div>
<div class="m2"><p>بقای خرده جان و شرار هر دو یکی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز لطف و قهر نمی بالم و نمی نالم</p></div>
<div class="m2"><p>به خار خشک، خزان و بهار هر دو یکی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان ربوده این باغ و بوستان شده ام</p></div>
<div class="m2"><p>که نوشخند گل و نیش خار هر دو یکی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فسردگی و کدورت شده است عالمگیر</p></div>
<div class="m2"><p>جوان و پیر درین روزگار هر دو یکی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان گزیده دنیای بد گهر شده ام</p></div>
<div class="m2"><p>که پیش دیده من گنج و مار هر دو یکی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مکن به بدگهران مردمی که آتش را</p></div>
<div class="m2"><p>چه گل به جیب فشانی چه خار هر دو یکی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه لازم است شب و روز خون دل خوردن؟</p></div>
<div class="m2"><p>چو سنگ و لعل درین روزگار هر دو یکی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>توان به زنده دلی شد ز مردگان ممتاز</p></div>
<div class="m2"><p>وگرنه سینه و لوح مزار هر دو یکی است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر دو بین ز دو رنگی نگشته ای صائب</p></div>
<div class="m2"><p>شب جدایی و روز شمار هر دو یکی است</p></div></div>