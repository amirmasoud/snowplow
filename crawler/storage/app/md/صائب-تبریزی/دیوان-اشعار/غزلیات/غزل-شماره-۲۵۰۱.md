---
title: >-
    غزل شمارهٔ ۲۵۰۱
---
# غزل شمارهٔ ۲۵۰۱

<div class="b" id="bn1"><div class="m1"><p>نه همین بر قلب ایمان یا دل ما می‌زند</p></div>
<div class="m2"><p>دزد خال او شبی خود را به صد جا می‌زند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جام چون خالی شود سر می‌نهد در پای خُم</p></div>
<div class="m2"><p>ابر چون بی‌آب شد بر قلب دریا می‌زند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اینقدرها شوربختی را اثر می بوده است؟</p></div>
<div class="m2"><p>می‌شود هشیار هرکس باده با ما می‌زند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان مشتاقان نمی‌سازد به زندان بدن</p></div>
<div class="m2"><p>وحشی ما زود بر دامان صحرا می‌زند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشتن ما نیست مطلب از شکست آستین</p></div>
<div class="m2"><p>دامنی بر آتش بی‌تابی ما می‌زند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرچه هر بندم ز بار درد کوه آهنی است</p></div>
<div class="m2"><p>باز عشق بدگمانم بند بر پا می‌زند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مدتی شد خط او فرمان عزل آورده است</p></div>
<div class="m2"><p>همچنان خال لب او مهر بالا می‌زند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می‌تواند گل ز روی دولت بیدار چید</p></div>
<div class="m2"><p>هرکه چون صائب می روشن به شب‌ها می‌زند</p></div></div>