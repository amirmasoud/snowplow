---
title: >-
    غزل شمارهٔ ۲۱۲۵
---
# غزل شمارهٔ ۲۱۲۵

<div class="b" id="bn1"><div class="m1"><p>این هستی باطل چو شرر محض نمودست</p></div>
<div class="m2"><p>یک چشم زدن ره ز عدم تا به وجودست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کیفیت طاعت مطلب از سر هشیار</p></div>
<div class="m2"><p>مینای تهی بیخبر از ذوق سجودست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خامی است امید ثمر از نخل تمنا</p></div>
<div class="m2"><p>بگذار که این هیزم تر مایه دودست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زخمی که نه ناسور بود رخنه مرگ است</p></div>
<div class="m2"><p>داغی که ندارد نمکی چشم حسودست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بید جز افتادگی و عجز مجویید</p></div>
<div class="m2"><p>مجنون خدا را همه دم کار سجودست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>افسردگی عشق ز افسردگی ماست</p></div>
<div class="m2"><p>هنگامه مجمر خنک از خامی عودست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مردان خدا فارغ از اندیشه چرخند</p></div>
<div class="m2"><p>رخسار زنان لایق این خال کبودست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب ثمر عشق من از آینه رویان</p></div>
<div class="m2"><p>چون طوطی از آیینه همین گفت و شنودست</p></div></div>