---
title: >-
    غزل شمارهٔ ۳۹۸۱
---
# غزل شمارهٔ ۳۹۸۱

<div class="b" id="bn1"><div class="m1"><p>به چشم شوخ رگ خواب تازیانه شود</p></div>
<div class="m2"><p>که خاروخس چوبه آتش رسد زبانه شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهار رنگ خزان برکند در آخر حسن</p></div>
<div class="m2"><p>به تیغ غمزه خط سبز موریانه شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به نیکوان سر طومار شکوه باز مکن</p></div>
<div class="m2"><p>که خواب حسن گرانسنگ ازین فسانه شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به بلبلی که حیاتش ز بوی گل باشد</p></div>
<div class="m2"><p>قفس چگونه گوارا ز آب ودانه شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز باده نرم نشد لعل او که هیهات است</p></div>
<div class="m2"><p>کز آب آتش یاقوت بی زبانه شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنین که گل شده از برگ گوش سرتاپا</p></div>
<div class="m2"><p>چگونه بلبل ماسیر از ترانه شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مریز اشک به تحصیل رزف چون طفلان</p></div>
<div class="m2"><p>که در گلو چوگره گشت گریه دانه شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به هیچ جا نرسد هرکه همتش پست است</p></div>
<div class="m2"><p>پرشکسته خس وخار آشیانه شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شود ز وسعت مشرب بهشت خارستان</p></div>
<div class="m2"><p>که جوش خلق به عارف شرابخانه شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گناه کجروی توست ناامیدی تو</p></div>
<div class="m2"><p>که تیر راست خطا کمتر از نشانه شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنان که غنچه شود نیمشب شکفته به باغ</p></div>
<div class="m2"><p>شکفته بیش دل از باده شبانه شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دهد به راهروان بال وپر سبکباری</p></div>
<div class="m2"><p>پیاده پیشتر از کاروان روانه شود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نشست از دل من گریه گرد کلفت را</p></div>
<div class="m2"><p>کجا ز موج زدن بحر بیکرانه شود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز بردباری من سرکشی شود پامال</p></div>
<div class="m2"><p>ز خاکساری من صدر آستانه شود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گشاده رویی گل عندلیب را صائب</p></div>
<div class="m2"><p>درین شکفته چمن باعث ترانه شود</p></div></div>