---
title: >-
    غزل شمارهٔ ۳۹۵۳
---
# غزل شمارهٔ ۳۹۵۳

<div class="b" id="bn1"><div class="m1"><p>به هر فسرده لب خشک وچشم تر ندهند</p></div>
<div class="m2"><p>قبول داغ محبت به هر جگر ندهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گوشمال ستم سر ز حکم عشق مپیچ</p></div>
<div class="m2"><p>که هیچ رشته بی تاب را گهر ندهند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فراغبالی در تنگنای چرخ مخواه</p></div>
<div class="m2"><p>همان به است که در بیضه بال وپر ندهند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بریز بار تعلق که شاخه های درخت</p></div>
<div class="m2"><p>نمی شوند سبکبار تا ثمر ندهند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز روی تلخ مکافات زهر می بارد</p></div>
<div class="m2"><p>چه نعمتی است که کام مرا شکر ندهند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترم ز طعنه این زاهدان خشک ای کاش</p></div>
<div class="m2"><p>چو صندلی نفرستند دردسرندهند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنان چکیده بخلند این گرانجانان</p></div>
<div class="m2"><p>که نیم قطره به ابرام نیشتر ندهند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز دوش دار سرش تکیه گه نخواهد یافت</p></div>
<div class="m2"><p>اگر دو دور به منصور بیشتر ندهند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه شکوه می کنی از اشک تلخ خود صائب</p></div>
<div class="m2"><p>ترا شرابی ازین خوشگوارتر ندهند</p></div></div>