---
title: >-
    غزل شمارهٔ ۳۱۶۵
---
# غزل شمارهٔ ۳۱۶۵

<div class="b" id="bn1"><div class="m1"><p>نصیب خویش هر کس یافت در دنیا نمی ماند</p></div>
<div class="m2"><p>گهر سیراب چون گردید در دریا نمی ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زخودبینی برآور کشتی بی لنگر خود را</p></div>
<div class="m2"><p>که در موج خطر آیینه از دریا نمی ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمی گیرند در دل خاکساران کینه انجم</p></div>
<div class="m2"><p>زداغ لاله جا در سینه صحرا نمی ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غم روزی نیفشارد دل اهل توکل را</p></div>
<div class="m2"><p>کسی در پای خم بی نشأه صهبا نمی ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترا چشم قیامت بین ندارد نور آگاهی</p></div>
<div class="m2"><p>وگرنه شورش امروز از فردا نمی ماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فراغت دارد از بیتابی ما چرخ سنگین دل</p></div>
<div class="m2"><p>اثر از نقش پای مور در خارا نمی ماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به روی عشق طاقت پرده می پوشد، نمی داند</p></div>
<div class="m2"><p>که کوه قاف زیر شهپر عنقا نمی ماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>محبت وحشیان را آشنا رو می کند صائب</p></div>
<div class="m2"><p>اگر مجنون به صحرا می رود تنها نمی ماند</p></div></div>