---
title: >-
    غزل شمارهٔ ۵۹۴۳
---
# غزل شمارهٔ ۵۹۴۳

<div class="b" id="bn1"><div class="m1"><p>ازان گشاده جبین جام پر شراب گرفتم</p></div>
<div class="m2"><p>عجب هلال تمامی ز آفتاب گرفتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خواب دامن آن زلف بی حجاب گرفتم</p></div>
<div class="m2"><p>عنان دولت بیدار را به خواب گرفتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به چشم بندی شرم و حجاب عشق چه سازم؟</p></div>
<div class="m2"><p>گرفتم این که ز رخسار او نقاب گرفتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشد ز دولت بیدار رزق اهل سعادت</p></div>
<div class="m2"><p>تمتعی که ازان چشم نیمخواب گرفتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به من چگونه رسد پیچ و تاب موی در آتش؟</p></div>
<div class="m2"><p>که من ز موی میان مشق پیچ و تاب گرفتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز گریه عاقبت کار گل فتاد به چشمم</p></div>
<div class="m2"><p>ز گل گلاب کشیدم، گل از گلاب گرفتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به نبض چشمه حیوان رسید دست امیدم</p></div>
<div class="m2"><p>اگر ز صدق طلب دامن سراب گرفتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نشد چو تیر خطا، آفرین نصیب غزالم</p></div>
<div class="m2"><p>زنافه گر چه زمین را به مشک ناب گرفتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به روی من در امید هر که بست ز مردم</p></div>
<div class="m2"><p>من از گشایش توفیق فتح باب گرفتم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گزیدن لب افسوس بود نقل شرابم</p></div>
<div class="m2"><p>ز دست هر که درین انجمن شراب گرفتم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به نارسایی من رهرو این بساط ندارد</p></div>
<div class="m2"><p>فتاد پیش زمن، هر که را به خواب گرفتم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هزار غوطه زدم چون صدف به بحر خجالت</p></div>
<div class="m2"><p>به یک دو قطره که من صائب از سحاب گرفتم</p></div></div>