---
title: >-
    غزل شمارهٔ ۶۵۷۵
---
# غزل شمارهٔ ۶۵۷۵

<div class="b" id="bn1"><div class="m1"><p>در دل از نادان فزون صاحب هنر دارد گره</p></div>
<div class="m2"><p>سرو موزون از درختان بیشتر دارد گره</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گلستان جهان هر لاله رخساری که هست</p></div>
<div class="m2"><p>از غم عشق تو آهی در جگر دارد گره</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در گرفتاری حلاوت های عالم مضمرست</p></div>
<div class="m2"><p>نی به هر بندی جدا تنگ شکر دارد گره</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس که می پیچم دل شبها به یاد زلف او</p></div>
<div class="m2"><p>هر رگم از رشته تب بیشتر دارد گره</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دو ناخن گر گره وا می شود، چون از صدف</p></div>
<div class="m2"><p>بر جبین خویشتن دایم گهر دارد گره؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آه سردی از لب هر کس که می گردد بلند</p></div>
<div class="m2"><p>آفتابی در ته دل چون سحر دارد گره</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رشته نگسسته باشد بی گره، چون اشک من</p></div>
<div class="m2"><p>نگسلد هر چند از هم بیشتر دارد گره؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست جای پرفشانی تنگنای آسمان</p></div>
<div class="m2"><p>ورنه دل در سینه چندین بال و پر دارد گره</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا شدم از غنچه خسبان، شد پر از گل دامنم</p></div>
<div class="m2"><p>در گشاد کارها دست دگر دارد گره</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون گشاید کار من زان در که دربانش ز منع</p></div>
<div class="m2"><p>از دم عقرب بر ابرو بیشتر دارد گره</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از سبک مغزی به فرقش تیغ می بارد مدام</p></div>
<div class="m2"><p>بر جبین خویش هر کس چون سپر دارد گره</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یک گره افزون نباشد رشته زنار را</p></div>
<div class="m2"><p>سبحه تزویر از صد رهگذر دارد گره</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ریخت چون برگ خزان از عقده دل ناخنم</p></div>
<div class="m2"><p>حرف پوچ است این که از ناخن خطر دارد گره</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در تلاش رشته کار من بی دست و پا</p></div>
<div class="m2"><p>با همه بی دست و پایی بال و پر دارد گره</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>قرب حق در قبض بیش از بسط عارف را بود</p></div>
<div class="m2"><p>با گهر در رشته پیوند دگر دارد گره</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عقده زود از جبهه اهل کرم وا می شود</p></div>
<div class="m2"><p>از حباب پوچ دریای گهر دارد گره</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نیست ممکن سربرآرد از گریبان گهر</p></div>
<div class="m2"><p>رشته از کوتاه بینی تا به سر دارد گره</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نیست صائب دلخراشی کار اشک صافدل</p></div>
<div class="m2"><p>ورنه در هر قطره ای صد نیشتر دارد گره</p></div></div>