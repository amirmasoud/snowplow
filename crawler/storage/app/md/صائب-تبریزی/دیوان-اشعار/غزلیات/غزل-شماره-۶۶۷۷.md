---
title: >-
    غزل شمارهٔ ۶۶۷۷
---
# غزل شمارهٔ ۶۶۷۷

<div class="b" id="bn1"><div class="m1"><p>از فنای پیکر خاکی چرا خون می خوری؟</p></div>
<div class="m2"><p>از شکست خم چرا غم ای فلاطون می خوری؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در قفس روزی ز بیرون می خورد مرغ قفس</p></div>
<div class="m2"><p>غم ز بی برگی چرا در زیر گردون می خوری؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای که می سازی ز می رخسار خود را لاله گون</p></div>
<div class="m2"><p>غافلی کز دل سیاهی غوطه در خون می خوری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حفظ کن اندازه را در می که گردد ناگوار</p></div>
<div class="m2"><p>گر ز آب زندگی یک جرعه افزون می خوری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کاهش و افزایش این نشأه با یکدیگرست</p></div>
<div class="m2"><p>می خورد افیون ترا چندان که افیون می خوری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می رسد در سنگ صائب رزق لعل از آفتاب</p></div>
<div class="m2"><p>اینقدر ز اندیشه روزی چرا خون می خوری؟</p></div></div>