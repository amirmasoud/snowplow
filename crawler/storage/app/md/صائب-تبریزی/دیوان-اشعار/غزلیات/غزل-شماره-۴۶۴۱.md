---
title: >-
    غزل شمارهٔ ۴۶۴۱
---
# غزل شمارهٔ ۴۶۴۱

<div class="b" id="bn1"><div class="m1"><p>برگ عیش خویش را چون گل زهم پاشیده گیر</p></div>
<div class="m2"><p>این دکانی را که برخود چیده ای برچیده گیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می کند عریان چومرگ از کسوت هستی ترا</p></div>
<div class="m2"><p>چند روزی این لباس عاریت پوشیده گیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمر جاویدان این عالم همین روزوشبی است</p></div>
<div class="m2"><p>پشت وروی این ورق را تاقیامت دیده گیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون زهر برگی به چندین چشم می باید گریست</p></div>
<div class="m2"><p>یک دهن چون گل درین بستانسراخندیده گیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم می باید چون پوشیدن زدنیا عاقبت</p></div>
<div class="m2"><p>دیده را نادیده و نادیده هارادیده گیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون فزایش را نباشد غیر کاهش حاصلی</p></div>
<div class="m2"><p>چند روزی خویش را چون ماه نو بالیده گیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ازخط و زلف نکویان دیده رغبت بپوش</p></div>
<div class="m2"><p>از دل بیدار، این خواب پریشان دیده گیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر نخواهی بست چون حلاج لب از حرف راست</p></div>
<div class="m2"><p>ریسمان بهر گلوی خویشتن تابیده گیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون گرانبارست از خواب گران این کاروان</p></div>
<div class="m2"><p>بادل صد چاک چندی چون جرس نالیده گیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گوش سنگین، سنگ دندان سبک مغزان بود</p></div>
<div class="m2"><p>هر چه در حق تو گوید مدعی نشنیده گیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون به ارباب بصیرت عرض دادی جنس خویش</p></div>
<div class="m2"><p>در ترازوی قیامت خویش راسنجیده گیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیست صائب حاصلی این عالم پرشور را</p></div>
<div class="m2"><p>در زمین شور تخم خویش را پاشیده گیر</p></div></div>