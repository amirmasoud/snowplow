---
title: >-
    غزل شمارهٔ ۶۲۹۴
---
# غزل شمارهٔ ۶۲۹۴

<div class="b" id="bn1"><div class="m1"><p>ز آستین دست تو گر یک سحر آید بیرون</p></div>
<div class="m2"><p>چون گل از دست تو بی خواست زر آید بیرون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کف خاکستر از سوختگان پیدا نیست</p></div>
<div class="m2"><p>به چه امید ز خارا شر آید بیرون؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز دم از بی خبری جوش حلاوت، غافل</p></div>
<div class="m2"><p>که نی از ناخن من چون شکر آید بیرون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل محال است که از فکر تو فارغ گردد</p></div>
<div class="m2"><p>این سری نیست که از زیر پر آید بیرون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچنان دست چو گل پیش کسان می داری</p></div>
<div class="m2"><p>اگر از جیب تو چون غنچه زر آید بیرون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو پیکان که به تن نیست قرارش یک جا</p></div>
<div class="m2"><p>هر زمان دل ز مقام دگر آید بیرون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر از سیل حوادث متزلزل نشوی</p></div>
<div class="m2"><p>تیغ چون کوه ترا از کمر آید بیرون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رگ جانی که در او پیچ و خم غیرت هست</p></div>
<div class="m2"><p>خشک چون رشته ز آب گهر آید بیرون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه طباشیر هم از سوخته نی می خیزد؟</p></div>
<div class="m2"><p>از شب ما چه عجب گر سحر آید بیرون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در زمین دل اگر دانه امیدی هست</p></div>
<div class="m2"><p>به هواداری مژگان تر آید بیرون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از حضور ابدی کیست که دل بردارد؟</p></div>
<div class="m2"><p>از بیابان فنا چون خبر آید بیرون؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خضر صائب خبرش را نتواند دریافت</p></div>
<div class="m2"><p>رهنوردی که ز خود بی خبر آید بیرون</p></div></div>