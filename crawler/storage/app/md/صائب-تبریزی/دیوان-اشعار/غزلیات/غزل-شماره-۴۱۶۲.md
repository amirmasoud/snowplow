---
title: >-
    غزل شمارهٔ ۴۱۶۲
---
# غزل شمارهٔ ۴۱۶۲

<div class="b" id="bn1"><div class="m1"><p>گلزار جوش حسن خداداد می‌زند</p></div>
<div class="m2"><p>باغ از شکوفه موج پریزاد می‌زند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون لاله لاله می‌چکد از تیغ کوهسار</p></div>
<div class="m2"><p>طاوس سر ز بیضه فولاد می‌زند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون عندلیب هرکه قدم در چمن گذاشت</p></div>
<div class="m2"><p>بی‌اختیار بر در فریاد می‌زند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر لاله‌ای که سر زند از کوه بیستون</p></div>
<div class="m2"><p>ساغر به طاق ابروی فرهاد می‌زند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشق به هیچ وجه تسلی نمی‌شود</p></div>
<div class="m2"><p>در وصل عندلیب همان داد می‌زند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صائب به روی خود در غم باز می‌کند</p></div>
<div class="m2"><p>هرکس که خنده بر من ناشاد می‌زند</p></div></div>