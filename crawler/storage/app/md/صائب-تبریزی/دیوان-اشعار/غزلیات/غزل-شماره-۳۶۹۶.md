---
title: >-
    غزل شمارهٔ ۳۶۹۶
---
# غزل شمارهٔ ۳۶۹۶

<div class="b" id="bn1"><div class="m1"><p>ز باده چشم تو ظالم رحیم می گردد</p></div>
<div class="m2"><p>اگر بخیل به مستی کریم می گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به جد و جهد اگر گرگ گوسفند شود</p></div>
<div class="m2"><p>شریر هم به ریاضت سلیم می گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بیکسی به دل صاف من غباری نیست</p></div>
<div class="m2"><p>گهر عزیز شود چون یتیم می گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سپند ریشه دوانید در دل آتش</p></div>
<div class="m2"><p>چه وقت اختر ما مستقیم می گردد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آن ریاض غمینم که غنچه پیکان</p></div>
<div class="m2"><p>شکفته از دم سرد نسیم می گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل از گشودن لب می شود تهی از درد</p></div>
<div class="m2"><p>ز رخنه ملک اگر مستقیم می گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به چشم کم منگر در گناه اندک خویش</p></div>
<div class="m2"><p>که هرچه خرد شماری عظیم می گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مده به خلوت دل راه خنده را صائب</p></div>
<div class="m2"><p>که پسته را دل ازین ره دونیم می گردد</p></div></div>