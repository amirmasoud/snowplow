---
title: >-
    غزل شمارهٔ ۴۲۱۱
---
# غزل شمارهٔ ۴۲۱۱

<div class="b" id="bn1"><div class="m1"><p>مهر لب مرا می منصور نشکند</p></div>
<div class="m2"><p>زنجیر موج باده پر زور نشکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از درد عشق چون دل رنجور نشکند</p></div>
<div class="m2"><p>چون زیر کوه قاف پر مور نشکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از کاسه سر نگون دگران فیض می برند</p></div>
<div class="m2"><p>هرگز خمار نرگس مخمور نشکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پا چون شراب بر سر مستان نمی نهد</p></div>
<div class="m2"><p>در زیر پا سری که چو انگور نشکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاجز نواز باش که در دیده هاشکر</p></div>
<div class="m2"><p>شیرین ازان بود که دل مور نشکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرهم چه می کند به دل داغدار ما</p></div>
<div class="m2"><p>این تب ز سردمهری کافور نشکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آتش ز چوب خشک سرافراز می شود</p></div>
<div class="m2"><p>از دار پشت رایت منصور نشکند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی مرکزست دایره عیش ناتمام</p></div>
<div class="m2"><p>شان عسل حقارت زنبور نشکند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون تر شود ز آب گره سختترشود</p></div>
<div class="m2"><p>مهر حجاب را می پر زور نشکند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لاف بزرگی از تو پسندیده است اگر</p></div>
<div class="m2"><p>بر یکدگر ترا دهن گور نشکند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چرخ نشاط کاسه سایل نمی زند</p></div>
<div class="m2"><p>تا سنگ فتنه کاسه فغفور نشکند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آزاده آن رونده که با کوههای درد</p></div>
<div class="m2"><p>در زیر پای او کمر مور نشکند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ما می ز کاسه سر منصور خورده ایم</p></div>
<div class="m2"><p>صائب خمار ما می انگور نشکند</p></div></div>