---
title: >-
    غزل شمارهٔ ۵۱۸۶
---
# غزل شمارهٔ ۵۱۸۶

<div class="b" id="bn1"><div class="m1"><p>جان تازه می شود ز نسیم بهار عشق</p></div>
<div class="m2"><p>از یک سرست جوش گل وخار خار عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در شوره زار عقل به درمان گیاه نیست</p></div>
<div class="m2"><p>پیوسته سرخ روی بود لاله زار عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاری است خار عشق که در پای چون خلید</p></div>
<div class="m2"><p>نتوان کشید پا دگر از رهگذار عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از جان مگو که در گرو نقش اول است</p></div>
<div class="m2"><p>سرمایه دوکون به دارالقمار عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رحمی به بال کاغذی خود کن ای خرد</p></div>
<div class="m2"><p>خود را مزن برآتش بی زینهار عشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشقی که بی شمار نباشد بلای او</p></div>
<div class="m2"><p>پیش بلاکشان نبود در شمار عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دایم به زیر دار فنا ایستاده ایم</p></div>
<div class="m2"><p>بیرون نمی رویم ز دارالقرار عشق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اینجا مدار کارگزاری به همت است</p></div>
<div class="m2"><p>از بحر آتشین گذرد نی سوار عشق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تکلیف بار عشق دوتا کردچرخ را</p></div>
<div class="m2"><p>من کیستم که خم نشوم زیر بار عشق؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صائب هزار مرتبه کردیم امتحان</p></div>
<div class="m2"><p>با هیچ کار جمع نگردید کار عشق</p></div></div>