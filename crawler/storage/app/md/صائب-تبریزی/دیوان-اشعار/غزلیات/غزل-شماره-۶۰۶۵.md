---
title: >-
    غزل شمارهٔ ۶۰۶۵
---
# غزل شمارهٔ ۶۰۶۵

<div class="b" id="bn1"><div class="m1"><p>چون سکندر خانه عمر از اثر آباد کن</p></div>
<div class="m2"><p>این بنای سست پی را آهنین بنیاد کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می شود وقتی که فریادت شود فریادرس</p></div>
<div class="m2"><p>تا نفس در سینه داری ناله و فریاد کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرو را تشریف آزادی به رعنایی فکند</p></div>
<div class="m2"><p>بنده خود کن، ز رعنایی مرا آزاد کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روزگار کامرانی را زکاتی لازم است</p></div>
<div class="m2"><p>در حریم شعله ما را ای سمندر یاد کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست غیر از عشق خضری در بیابان وجود</p></div>
<div class="m2"><p>هر کجا گم گشته ای یابی، به عشق ارشاد کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند ای گل جلوه در کار تماشایی کنی؟</p></div>
<div class="m2"><p>بینوایان قفس را هم به برگی یاد کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می رساند موج کشتی را به ساحل بی خطر</p></div>
<div class="m2"><p>صبر بر جور ادیب و سیلی استاد کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر دو صد تیغ زبان باشد ترا در عرض حال</p></div>
<div class="m2"><p>در نیام خامشی چون سوسن آزاد کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از کمند پیچ و تاب عشق صائب سر مپیچ</p></div>
<div class="m2"><p>همچو جوهر ریشه محکم در دل فولاد کن</p></div></div>