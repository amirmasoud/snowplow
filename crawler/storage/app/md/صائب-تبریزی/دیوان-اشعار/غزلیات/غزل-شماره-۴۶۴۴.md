---
title: >-
    غزل شمارهٔ ۴۶۴۴
---
# غزل شمارهٔ ۴۶۴۴

<div class="b" id="bn1"><div class="m1"><p>به نادانی کند اقرارهرکس هست داناتر</p></div>
<div class="m2"><p>زحیرت پرده خواب است هر چشمی که بیناتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهفتم دردل صد پاره رازعشق ازین غافل</p></div>
<div class="m2"><p>که بوی گل زبرگ گل شود صد پرده رسواتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به پیری گفتم ازدامان دنیا دست بردارم</p></div>
<div class="m2"><p>ندانستم که درخشکی شود این خار گیراتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زسنگینی شودکم لنگر تمکین فلاخن را</p></div>
<div class="m2"><p>دل دیوانه از بند گران گرددسبکپاتر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مکن فکر اقامت درجهان گربینشی داری</p></div>
<div class="m2"><p>که از ریگ روان کوه است اینجا دشت پیماتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رعونت از شکست آرزو شد نفس راافزون</p></div>
<div class="m2"><p>که سازد آتش افسرده را خاشاک رعناتر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی صد شد زحرف تلخ،شور آن لب میگون</p></div>
<div class="m2"><p>که از تلخی می گلرنگ می گردد گواراتر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ریاض حسن او آب وهوای سرکشی دارد</p></div>
<div class="m2"><p>که باشد سبزه خوابیده اش از سرو رعناتر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نبندد حجت ناطق زبان منکران ،ورنه</p></div>
<div class="m2"><p>زعیسی روی شرم آلود مریم بود گویاتر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سفیدیهای مو غماز گردد رو سیاهی را</p></div>
<div class="m2"><p>که باشد در میان شیر خالص موی رسواتر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زبال افشانی پروانه روشن می شود صائب</p></div>
<div class="m2"><p>که عاشق درفراق از وصل می باشد شکیباتر</p></div></div>