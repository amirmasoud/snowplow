---
title: >-
    غزل شمارهٔ ۴۰۶۷
---
# غزل شمارهٔ ۴۰۶۷

<div class="b" id="bn1"><div class="m1"><p>روزی که خط سر از لب دلبر برآورد</p></div>
<div class="m2"><p>از موج بال چشمه کوثر برآورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با عشق حسن در ته یک پیرهن بود</p></div>
<div class="m2"><p>آتش ز بال خویش سمندر برآورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از سینه های گرم مجو آرزوی خام</p></div>
<div class="m2"><p>از خاک تخم سوخته کی سر برآورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگذاشت خط در آن لب شیرین حلاوتی</p></div>
<div class="m2"><p>مور حریص گرد ز شکر برآورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل را مکن کباب که هرقطره اشک او</p></div>
<div class="m2"><p>شور قیامت از دل اخگر برآورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رنگین سخن ز بخشش خلق است بی نیاز</p></div>
<div class="m2"><p>این غنچه بی طلب زدهن زر برآورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از روی آتشین تو انگشت زینهار</p></div>
<div class="m2"><p>برق تجلی از مژه تر برآورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تابرخورد ازان لب میگون به کام دل</p></div>
<div class="m2"><p>ساغر ز خویش باده احمر برآورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مژگان اشکبار شود رشته گهر</p></div>
<div class="m2"><p>چون زان دهان تنگ سخن سربرآورد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در جلوه گاه حسن تو انگشت زینهار</p></div>
<div class="m2"><p>از قامت علم صف محشربرآورد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شبها ز بیقراری پهلوی خشک من</p></div>
<div class="m2"><p>بالش پر از تزلزل بسر برآورد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آسوده تر ز دیده قربانیان شود</p></div>
<div class="m2"><p>بر روی آرزو دل اگر در برآورد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از گرمخونی دل مشتاق زخم من</p></div>
<div class="m2"><p>در بیضه تیغ بال ز جوهر برآورد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پا در رکاب برق بود فصل نوبهار</p></div>
<div class="m2"><p>صائب ز زیر بال چرا سر برآورد</p></div></div>