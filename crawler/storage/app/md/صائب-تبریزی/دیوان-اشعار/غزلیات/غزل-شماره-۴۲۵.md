---
title: >-
    غزل شمارهٔ ۴۲۵
---
# غزل شمارهٔ ۴۲۵

<div class="b" id="bn1"><div class="m1"><p>شکوه حسن لیلی آنچنان پر کرد هامون را</p></div>
<div class="m2"><p>که از جمعیت آهو، حصاری ساخت مجنون را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان باشند وحشت دیدگان جویای یکدیگر</p></div>
<div class="m2"><p>که می آید رم آهو به استقبال مجنون را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر دست از دهان آه آتشبار بردارم</p></div>
<div class="m2"><p>مشبک همچو مجمر می توانم ساخت گردون را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمیرد هر که با معشوق در یک پیرهن باشد</p></div>
<div class="m2"><p>وصال گنج دارد زنده زیر خاک قارون را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگردد منقطع فیض بزرگان در تهیدستی</p></div>
<div class="m2"><p>که خم چون شد تهی، دارالامانی شد فلاطون را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر از خجلت ز زیر بال قمری برنمی آرد</p></div>
<div class="m2"><p>مگر دیده است سرو بوستان آن قد موزون را؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز کاوش بیش آب چشمه ها افزون شود صائب</p></div>
<div class="m2"><p>تهی ازگریه نتوان ساخت دل های پر از خون را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر چه نیست قدر خاک، شعر تازه را صائب</p></div>
<div class="m2"><p>همان ارباب نظم از یکدگر دزدند مضمون را!</p></div></div>