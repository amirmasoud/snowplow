---
title: >-
    غزل شمارهٔ ۲۰۱۱
---
# غزل شمارهٔ ۲۰۱۱

<div class="b" id="bn1"><div class="m1"><p>از لعل آبدار تو طرفی نظر نبست</p></div>
<div class="m2"><p>از شور بحر در صدف ما گهر نبست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمی که شد به روی سخن باز چون قلم</p></div>
<div class="m2"><p>یک قطره آب خویش به جوی دگر نبست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان دم که لعل او به شکر خنده باز شد</p></div>
<div class="m2"><p>در نیشکر ز رعشه غیرت شکر نبست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آتشم ز آینه کز شوق دیدنت</p></div>
<div class="m2"><p>تا باز کرد دیده خود را دگر نبست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از برگ عیش ماند تهی جیب و دامنش</p></div>
<div class="m2"><p>چون لاله هر که داغ ترا بر جگر نبست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی زمین گذر که سیل حوادث است</p></div>
<div class="m2"><p>هر کس میان گشود در اینجا، کمر نبست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر برگ سبز او کف افسوس دیگرست</p></div>
<div class="m2"><p>نخلی که در شکوفه پیری ثمر نبست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب نشد عزیز به چشم جهانیان</p></div>
<div class="m2"><p>تا آبروی خود به گره چون گهر نبست</p></div></div>