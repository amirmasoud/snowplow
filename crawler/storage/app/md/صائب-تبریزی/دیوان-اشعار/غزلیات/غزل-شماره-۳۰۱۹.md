---
title: >-
    غزل شمارهٔ ۳۰۱۹
---
# غزل شمارهٔ ۳۰۱۹

<div class="b" id="bn1"><div class="m1"><p>مکن کاری که از جورت دل اندوهگین لرزد</p></div>
<div class="m2"><p>که از لرزیدن من آسمانها چون زمین لرزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زچشم بد خطر افزون بود رنگین لباسان را</p></div>
<div class="m2"><p>زصحرا بیش در فانوس شمع دوربین لرزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فروغ لعل و یاقوتم که بر کوه است پشت من</p></div>
<div class="m2"><p>نیم شمعی که بر پرتو زباد آستین لرزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به آه سرد چون زحمت دهم آن نازپرور را؟</p></div>
<div class="m2"><p>که از سرمای گل چون برگ بید آن نازنین لرزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندارد یاد چون من بیقراری صفحه دوران</p></div>
<div class="m2"><p>که نامم همچو دست رعشه داران در نگین لرزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به شمع صبحدم پروانه را چندان نلرزد دل</p></div>
<div class="m2"><p>که وقت خط به رخسار تو زلف عنبرین لرزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل از جان بر گرفتن نیست کار هر تنک ظرفی</p></div>
<div class="m2"><p>عجب نبود عرق بر چهره آن مه جبین لرزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به قدر حاصل از دنیا بود غم قسمت هر کس</p></div>
<div class="m2"><p>به خرمت صاحب خرمن فزون از خوشه چین لرزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زحرف سرد ناصح عاشق صادق نیندیشد</p></div>
<div class="m2"><p>کی از باد خزان بر خویش سرو راستین لرزد؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زبان در کام کش صائب اگر آسودگی خواهی</p></div>
<div class="m2"><p>که دایم شمع بر جان از زبان آتشین لرزد</p></div></div>