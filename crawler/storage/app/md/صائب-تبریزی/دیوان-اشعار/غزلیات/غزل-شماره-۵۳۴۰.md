---
title: >-
    غزل شمارهٔ ۵۳۴۰
---
# غزل شمارهٔ ۵۳۴۰

<div class="b" id="bn1"><div class="m1"><p>یاد ایامی که رو برروی جانان داشتم</p></div>
<div class="m2"><p>آبرویی همچو شبنم در گلستان داشتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باغبان بی رخصت من گل نمی چید از چمن</p></div>
<div class="m2"><p>امتیازی در میان عندلیبان داشتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاخ گل یک آب خوردن غافل از حالم نبود</p></div>
<div class="m2"><p>برگ بخت سبز برسر در گلستان داشتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر سحر کز خار خار عشق می جستم زجا</p></div>
<div class="m2"><p>همچو گل بر سینه صد زخم نمایان داشتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این زمان آمد سرم بر سنگ ورنه پیش ازین</p></div>
<div class="m2"><p>بالش آسایش از زانوی جانان داشتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بوی گل بیرون نمی برد از چمن دزد نسیم</p></div>
<div class="m2"><p>پاسبانی در بن هر خار پنهان داشتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرمه را دست خموشی بر دهان من نبود</p></div>
<div class="m2"><p>راه حرفی پیش آن چشم سخندان داشتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر غباری کز سرکوی تو می رفتم به چشم</p></div>
<div class="m2"><p>منت روی زمین بر دوش مژگان داشتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب آن روزی که می خندیدم از وصلش چو صبح</p></div>
<div class="m2"><p>کی خبر از روزگار شام هجران داشتم</p></div></div>