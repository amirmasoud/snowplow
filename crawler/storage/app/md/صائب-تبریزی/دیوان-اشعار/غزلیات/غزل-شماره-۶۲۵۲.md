---
title: >-
    غزل شمارهٔ ۶۲۵۲
---
# غزل شمارهٔ ۶۲۵۲

<div class="b" id="bn1"><div class="m1"><p>نیم غمگین که مرگ آرد مرا از زندگی بیرون</p></div>
<div class="m2"><p>ازین داغم که می آرد ز شغل بندگی بیرون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین کز قطع راه زندگانی مانده گردیدم</p></div>
<div class="m2"><p>مگر خواب اجل آرد مرا از ماندگی بیرون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تهیدستی است بر اهل کرم از کوه سنگین تر</p></div>
<div class="m2"><p>نیارد از گرانی ابر را بارندگی بیرون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کند همصحبت بد در نظرها خوار نیکان را</p></div>
<div class="m2"><p>پر طاوس را پا آرد از زیبندگی بیرون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تواضع می فزاید رتبه ارباب دولت را</p></div>
<div class="m2"><p>ز غلطانی نیاید گوهر از ارزندگی بیرون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز پیری می کشد از ظلم دست خویش هم ظالم</p></div>
<div class="m2"><p>خمیدن تیغ را آرد گر از برندگی بیرون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برآورد آن که از دوزخ من آلوده دامان را</p></div>
<div class="m2"><p>مرا ای کاش می آورد از شرمندگی بیرون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رگ گردن فزود از طوق قمری سرو را صائب</p></div>
<div class="m2"><p>ز رعنایی نیارد سرکشان را بندگی بیرون</p></div></div>