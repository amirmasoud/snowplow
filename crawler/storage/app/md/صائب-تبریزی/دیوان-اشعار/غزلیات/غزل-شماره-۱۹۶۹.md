---
title: >-
    غزل شمارهٔ ۱۹۶۹
---
# غزل شمارهٔ ۱۹۶۹

<div class="b" id="bn1"><div class="m1"><p>تا چشم من به گوشه عزلت فتاده است</p></div>
<div class="m2"><p>از دیده ام سراسر جنت فتاده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داند که روح در تن خاکی چه می کشد</p></div>
<div class="m2"><p>هر نازپروری که به غربت فتاده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون شمع آه می کشم از بهر خامشی</p></div>
<div class="m2"><p>تا کار من به دست حمایت فتاده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیوار اگر فتد به سرش چتر دولت است</p></div>
<div class="m2"><p>بر فرق هر که سایه منت فتاده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داند که خار و خس چه ز گرداب می کشد</p></div>
<div class="m2"><p>آن را که ره به حلقه صحبت فتاده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آسیای چرخ نشد نرم دانه ای</p></div>
<div class="m2"><p>دنبال من چه سنگ ملامت فتاده است؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اکنون که رعشه از کف من برده اختیار</p></div>
<div class="m2"><p>دستم به فکر دامن فرصت فتاده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل را ز درد و داغ محبت شکیب نیست</p></div>
<div class="m2"><p>در بحر بیکنار حقیقت فتاده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با دیده ای که می شود از نور ذره آب</p></div>
<div class="m2"><p>کارم به آفتاب قیامت فتاده است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون از کنار دست نشویم که کشتیم</p></div>
<div class="m2"><p>صائب به چارموجه کثرت فتاده است</p></div></div>