---
title: >-
    غزل شمارهٔ ۱۳۸۳
---
# غزل شمارهٔ ۱۳۸۳

<div class="b" id="bn1"><div class="m1"><p>جای جام باده را تریاک نتواند گرفت</p></div>
<div class="m2"><p>خاک جای آب آتشناک نتواند گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کار مژگان نیست حفظ گریه بی اختیار</p></div>
<div class="m2"><p>پیش این سیلاب را خاشاک نتواند گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخنه چون در ملک افزون شد گرفتن مشکل است</p></div>
<div class="m2"><p>عافیت جا در دل صد چاک نتواند گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رز به اندک روزگاری بر سر آمد از چنار</p></div>
<div class="m2"><p>هر سبکدستی عنان تاک نتواند گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کمان سخت نتوان حفظ کردن تیر را</p></div>
<div class="m2"><p>آه جا در خاطر غمناک نتواند گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می شود در ناف آهو مشک هر خونی که خورد</p></div>
<div class="m2"><p>دل کسی ان طره پیچاک نتواند گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می برد خورشید تابان گرمی بیجا به کار</p></div>
<div class="m2"><p>دل ز ماهرروی آتشناک نتواند گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طعمه بیرون از دهان شیر کردن مشکل است</p></div>
<div class="m2"><p>خون خود را کس ازان فتراک نتواند گرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غیر آه ما که از دامان مطلب کوته است</p></div>
<div class="m2"><p>هیچ دستی دامن افلاک نتواند گرفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می توان از پنجه شاهین گرفتن کبک را</p></div>
<div class="m2"><p>دل کسی صائب ازان بی باک نتواند گرفت</p></div></div>