---
title: >-
    غزل شمارهٔ ۵۵۱۰
---
# غزل شمارهٔ ۵۵۱۰

<div class="b" id="bn1"><div class="m1"><p>نیم نومید از عقبی گر از دنیا گره خوردم</p></div>
<div class="m2"><p>در آنجا باز خواهم شد اگر اینجا گره خوردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشد کوته ز دامان اجابت دست امیدم</p></div>
<div class="m2"><p>چو تار اشک چندانی که سر تا پا گره خوردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز فیض راستی از جیب منزل سر برآوردم</p></div>
<div class="m2"><p>چو راه از نقش پا هر چند چندین جا گره خوردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشد چشم پریشان گرد من سیر از هوا جویی</p></div>
<div class="m2"><p>مکرر گر چه زین دریا حباب آسا گره خوردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشو در منزل مقصود از سر گشتگی ایمن</p></div>
<div class="m2"><p>که چون گرداب من در عین این دریا گره خوردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نصیب شانه زان سنبلشان چندین گشایش شد</p></div>
<div class="m2"><p>زدام زلف جای دانه من تنها گره خوردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شدم مشهور عالم گر چه از عزلت گزینی ها</p></div>
<div class="m2"><p>به بال و پر ز کوه قاف چون عنقا گره خوردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز من حرف طلب نتوان شنیدن با تهیدستی</p></div>
<div class="m2"><p>که از تبخال غیرت بر لب گویا گره خوردم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بحمدالله سر از فرمان ذکر حق نپیچیدم</p></div>
<div class="m2"><p>چو تار سبحه از دوران اگر صد جا گره خوردم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز سیر و دور گردون موبمو آگه شدم صائب</p></div>
<div class="m2"><p>درین پرگار تا چون نقطه سودا گره خوردم</p></div></div>