---
title: >-
    غزل شمارهٔ ۱۱۰۷
---
# غزل شمارهٔ ۱۱۰۷

<div class="b" id="bn1"><div class="m1"><p>هر که بست از گفتگو لب جنت دربسته است</p></div>
<div class="m2"><p>می زند جوش بهاران غنچه تا سر بسته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی سخن روشندلان بهتر به مضمون می رسند</p></div>
<div class="m2"><p>نامه وا کرده اینجا نامه سربسته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عندلیب خوش نوایی را دهن پر زر نکرد</p></div>
<div class="m2"><p>غنچه از بهر چه یارب در گره زر بسته است؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرده عصمت بود زندان حسن شوخ چشم</p></div>
<div class="m2"><p>شمع در فانوس چون پروانه پر بسته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کوه را موج حوادث در فلاخن می نهد</p></div>
<div class="m2"><p>این صدف از ساده لوحی دل به گوهر بسته است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسن عالمسوز را پروای آه سرد نیست</p></div>
<div class="m2"><p>بارها این شمع ره بر باد صرصر بسته است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن که بی شیرازه دارد کهنه اوراق مرا</p></div>
<div class="m2"><p>بارها شیرازه دیوان محشر بسته است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سبزه خط زان لب جانبخش دل را مانع است</p></div>
<div class="m2"><p>خضر آب زندگی را بر سکندر بسته است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دولت دنیا سبک جولانتر از بال هماست</p></div>
<div class="m2"><p>ساده لوح آن کس که دل بر تخت و افسر بسته است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن که ابروی هلال عید را طاق آفرید</p></div>
<div class="m2"><p>طاق ابروی ترا بسیار بهتر بسته است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیست صائب در پر پرواز کوتاهی مرا</p></div>
<div class="m2"><p>دور باش باغبان مرغ مرا پر بسته است</p></div></div>