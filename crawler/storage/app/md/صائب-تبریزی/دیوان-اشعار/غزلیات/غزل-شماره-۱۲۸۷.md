---
title: >-
    غزل شمارهٔ ۱۲۸۷
---
# غزل شمارهٔ ۱۲۸۷

<div class="b" id="bn1"><div class="m1"><p>درگذر زین خاکدان، گرد سپاهی بیش نیست</p></div>
<div class="m2"><p>برشکن افلاک را، طرف کلاهی بیش نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تشنه چشم افتاده است آیینه اسکندری</p></div>
<div class="m2"><p>ورنه آب زندگانی دل سیاهی بیش نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رهنوردان طریق کعبه مقصود را</p></div>
<div class="m2"><p>سایه دیوار امکان خوابگاهی بیش نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر ز کوه قاف باشد گفتگو سنجیده تر</p></div>
<div class="m2"><p>پیش تمکین خموشی برگ کاهی بیش نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوشه دل از عمارت کرد مستغنی مرا</p></div>
<div class="m2"><p>مطلب صیاد از عالم، پناهی بیش نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دل روشن سراسر می رود یاد بهشت</p></div>
<div class="m2"><p>چشمه خورشید را زرین گیاهی بیش نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما به داغ لاله صلح از لاله رویان کرده ایم</p></div>
<div class="m2"><p>از جهان منظور ما چشم سیاهی بیش نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طی نمی گردد به شبگیر حیات جاودان</p></div>
<div class="m2"><p>گرچه زلف او به ظاهر کوچه راهی بیش نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در غریبی می نماید خویش را حسن غریب</p></div>
<div class="m2"><p>قسمت یوسف ز کنعان قعر چاهی بیش نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون قلم هر چند دست از ماست، بر لوح وجود</p></div>
<div class="m2"><p>حاصل ما از تردد مد آهی بیش نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با هزاران چشم روشن، چرخ نشناسد مرا</p></div>
<div class="m2"><p>بهره مجمر ز عنبر دود آهی بیش نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حاصل پرواز ما چون چشم ازین چرخ خسیس</p></div>
<div class="m2"><p>به همه روشن روانی برگ کاهی بیش نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون تواند ماه پیش عارض او شد سفید؟</p></div>
<div class="m2"><p>آفتاب اینجا چراغ صبحگاهی بیش نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>می رسد صائب به زهرآلوده، آن هم گاه گاه</p></div>
<div class="m2"><p>روزی ما گر چه از خوبان نگاهی بیش نیست</p></div></div>