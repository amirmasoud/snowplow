---
title: >-
    غزل شمارهٔ ۵۴۳۶
---
# غزل شمارهٔ ۵۴۳۶

<div class="b" id="bn1"><div class="m1"><p>ما رگ جان را به آن زلف پریشان بسته ایم</p></div>
<div class="m2"><p>پیچ و تاب زلف او را بر رگ جان بسته ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دل پرخون که قربان شهادت می رود</p></div>
<div class="m2"><p>لاله داغی به تابوت شهیدان بسته ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبنمیم اما ز فیض شوخ چشمیهای عشق</p></div>
<div class="m2"><p>با گل خورشید، مژگان را به مژگان بسته ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوری ما یک سر تیرست ازان ابرو کمان</p></div>
<div class="m2"><p>بر خدنگ راست کیشش دل چو پیکان بسته ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست دریا زیر بار گریه خونین ماست</p></div>
<div class="m2"><p>ما حنای رنگ بست دست مرجان بسته ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی رویم از جا به سنگ کودکان شوچ چشم؟</p></div>
<div class="m2"><p>ما و صحرای جنون دامان به دامان بسته ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر زبان افتاده راز بوسه دزدیهای ما</p></div>
<div class="m2"><p>این نمک را ما به چشم پاسبانان بسته ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پر بر آورده است چون مرغ نگاه از اشتیاق</p></div>
<div class="m2"><p>نامه خود را اگر بر بال مژگان بسته ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون نسوزیم از ندامت، چون نمیریم از خمار؟</p></div>
<div class="m2"><p>ما به زخم خود در فیض نمکدان بسته ایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چشم حسرت از گل روی وطن پوشیده ایم</p></div>
<div class="m2"><p>دل به زلف سرکش شام غریبان بسته ایم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا به کی ناخن زنی ای شانه دستت خشک باد!</p></div>
<div class="m2"><p>دل به امیدی در آن زلف پریشان بسته ایم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کعبه از باب السلام آغوش وا کرده است و ما</p></div>
<div class="m2"><p>دامن محمل به مژگان مغیلان بسته ایم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>محمل ما همچو شبنم هست بر دوش وداع</p></div>
<div class="m2"><p>ما نه همچون غنچه صائب دل به بستان بسته ایم</p></div></div>