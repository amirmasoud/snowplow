---
title: >-
    غزل شمارهٔ ۳۳۸۶
---
# غزل شمارهٔ ۳۳۸۶

<div class="b" id="bn1"><div class="m1"><p>نیست ممکن دل آشفته به سر پردازد</p></div>
<div class="m2"><p>بید مجنون به سرانجام ثمر پردازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست در خاطر سودازدگان فکر وصال</p></div>
<div class="m2"><p>به چه دل غرقه دریا به گهر پردازد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم بیمار تو درمانده تدبیر خودست</p></div>
<div class="m2"><p>فرصتی کو به من خسته جگر پردازد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق پاک نظر اوست که در خلوت وصل</p></div>
<div class="m2"><p>چشم پوشد ز تماشا، به خبر پردازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صفحه روی تو اکنون که خط آورد برون</p></div>
<div class="m2"><p>عجبی نیست به ارباب نظر پردازد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محو در پرتو خورشید جهانتاب شود</p></div>
<div class="m2"><p>هر که چون شبنم گل دیده به زر پردازد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نامه اش پاکتر از صبح قیامت باشد</p></div>
<div class="m2"><p>هر که آیینه دل وقت سحر پردازد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عیش شیرین نشود با نفس گیرا جمع</p></div>
<div class="m2"><p>بی نوا ماند اگر نی به شکر پردازد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که در تربیت جوهر بینش باشد</p></div>
<div class="m2"><p>به جگر سوختگان همچو شرر پردازد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زان عقیق لب سیراب چه کم می گردد؟</p></div>
<div class="m2"><p>یک نفس گر به من تشنه جگر پردازد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دایم از تنگدلی سر به گریبان باشد</p></div>
<div class="m2"><p>هر که چون غنچه درین باغ به زر پردازد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گرد ما فرد روان را نتواند دریافت</p></div>
<div class="m2"><p>رهنوردی که به اسباب سفر پردازد</p></div></div>