---
title: >-
    غزل شمارهٔ ۱۰۶۰
---
# غزل شمارهٔ ۱۰۶۰

<div class="b" id="bn1"><div class="m1"><p>در بیابانی که خارش تشنه خون خوردن است</p></div>
<div class="m2"><p>پای در دامن کشیدن گل به دامن کردن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رزق ما چون شبنم از رنگین عذاران چمن</p></div>
<div class="m2"><p>با کمال قرب، دندان بر جگر افشردن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون صدف دامن گره کردن به دامان گهر</p></div>
<div class="m2"><p>در گریبان دشمن خونخوار را پروردن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خو به عزلت کن که در بحر پر آشوب جهان</p></div>
<div class="m2"><p>گوشه گیری کشتی خود را به ساحل بردن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>معنی نازک به آسانی نمی آید به دست</p></div>
<div class="m2"><p>پیچ و تاب جوهر هر شمشیر از خون خوردن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عمر در تمهید اسباب سفر ضایع مکن</p></div>
<div class="m2"><p>توشه ای گر هست راه عشق را، دل خوردن است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست راهی از دل و دین باختن نزدیکتر</p></div>
<div class="m2"><p>در قمار عشق هر کس را که میل بردن است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سر به جیب خامشی بردن درین آشوبگاه</p></div>
<div class="m2"><p>از خم چوگان گردون گوی بیرون بردن است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از تأمل پایه معنی به گردون می رسد</p></div>
<div class="m2"><p>سرفرازی نخل را صائب ز پا افشردن است</p></div></div>