---
title: >-
    غزل شمارهٔ ۳۸۳۲
---
# غزل شمارهٔ ۳۸۳۲

<div class="b" id="bn1"><div class="m1"><p>به گرد گریه من ابر درفشان نرسد</p></div>
<div class="m2"><p>به آه حسرت من برق خوش عنان نرسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجردان چو مسیح از علایق آزادند</p></div>
<div class="m2"><p>کمند رشته مریم به آسمان نرسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا ز خرمن گردون چه چشمداشت بود؟</p></div>
<div class="m2"><p>که برگ کاه به چشمم ز کهکشان نرسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به زهد خشک به معراج قرب نتوان رفت</p></div>
<div class="m2"><p>که کس به عالم بالا به نردبان نرسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز سطحیان مطلب غور نکته های دقیق</p></div>
<div class="m2"><p>هما به چاشنی مغز استخوان نرسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گشایش از دم پیران بود جوانان را</p></div>
<div class="m2"><p>به خاکبوس هدف تیر بی کمان نرسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به خواری وطن از عیش غربتم قانع</p></div>
<div class="m2"><p>که هیچ گل به خس و خار آشیان نرسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کمند آه ستمدیدگان عنان تاب است</p></div>
<div class="m2"><p>نمی شود به سر چاه، کاروان نرسد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حضور همنفسان مغتنم شمر صائب</p></div>
<div class="m2"><p>که لذتی به ملاقات دوستان نرسد</p></div></div>