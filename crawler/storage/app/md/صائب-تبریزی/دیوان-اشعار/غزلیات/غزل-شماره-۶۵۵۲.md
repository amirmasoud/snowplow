---
title: >-
    غزل شمارهٔ ۶۵۵۲
---
# غزل شمارهٔ ۶۵۵۲

<div class="b" id="bn1"><div class="m1"><p>چون شبنم روشن گهر با خار و گل یکرنگ شو</p></div>
<div class="m2"><p>بگذار رعنایی ز سر بیزار از نیرنگ شو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکرنگی ظاهر بود دارالامان عافیت</p></div>
<div class="m2"><p>در حلقه دیوانگان زنهار بی فرهنگ شو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل زود می گردد سیه زین طارم زنگارگون</p></div>
<div class="m2"><p>بگذر ازین ماتم سرا آیینه بی زنگ شو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زنهار در دار فنا انگور خود ضایع مکن</p></div>
<div class="m2"><p>گر باده نتوانی شدن منصوروار آونگ شو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز دل نمی باشد مکان آن لامکان پرواز را</p></div>
<div class="m2"><p>خواهی به بر تنگش کشی دلتنگ شو دلتنگ شو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خالی نمی ماند ز زر دستی که احسان می کند</p></div>
<div class="m2"><p>تقصیر در ریزش مکن خورشید زرین چنگ شو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راه از زمین گیری بود در دامن منزل سرش</p></div>
<div class="m2"><p>بشکن به دامن پای خود چون راه پیشاهنگ شو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خصم درونی از برون بارست بر دل بیشتر</p></div>
<div class="m2"><p>با دشمنان کن آشتی با خویشتن در جنگ شو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون آسمان از گوشمال آهنگ می سازد ترا</p></div>
<div class="m2"><p>بی گوشمال آسمان آهنگ شو آهنگ شو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر چند خون باشد ترا روزی ازین وحشت سرا</p></div>
<div class="m2"><p>چون لعل از چشم بدان پنهان درون سنگ شو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از می پرستی گل بود پیوسته صائب سرخ رو</p></div>
<div class="m2"><p>پیمانه را از کف مده گلرنگ شو گلرنگ شو</p></div></div>