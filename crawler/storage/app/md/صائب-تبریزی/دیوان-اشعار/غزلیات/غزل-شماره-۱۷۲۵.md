---
title: >-
    غزل شمارهٔ ۱۷۲۵
---
# غزل شمارهٔ ۱۷۲۵

<div class="b" id="bn1"><div class="m1"><p>به فکر چاره فتادن جگر گداختن است</p></div>
<div class="m2"><p>علاج درد چو مردان به درد ساختن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مدان ز عشق جگرسوز حسن را غافل</p></div>
<div class="m2"><p>که شمع بیش ز پروانه در گداختن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>توان به خانه خرابی ز گنج شد معمور</p></div>
<div class="m2"><p>ترا مدار چو طفلان به خانه ساختن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو از رعونت خود می کشی ز خلق آزار</p></div>
<div class="m2"><p>هدف نشانه ناوک ز قد فراختن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز زخم نیست مرا طالعی چو صید حرم</p></div>
<div class="m2"><p>وگرنه شیوه خورشید تیغ آختن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخن ز دست نوازش زند به دل ناخن</p></div>
<div class="m2"><p>که ساز باعث خوشوقتی از نواختن است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صفای آینه دل درین جهان، موقوف</p></div>
<div class="m2"><p>به نقش نیک و بد و خوب و زشت ساختن است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به آه گرم دل سخن نرم گرداندن</p></div>
<div class="m2"><p>ز سنگ خاره به تدبیر شیشه ساختن است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فغان که نرم شد جان سخت ما صائب</p></div>
<div class="m2"><p>به بوته ای که در او سنگ در گداختن است</p></div></div>