---
title: >-
    غزل شمارهٔ ۲۴۷
---
# غزل شمارهٔ ۲۴۷

<div class="b" id="bn1"><div class="m1"><p>از ته دل نیست در میخانه استغفار ما</p></div>
<div class="m2"><p>خوابها در پرده دارد دیده بیدار ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حوادث طاقت ما را شکیب دیگرست</p></div>
<div class="m2"><p>می کند پهلو تهی سیلاب از دیوار ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گریه مستانه زنگ کلفت از دل می برد</p></div>
<div class="m2"><p>آب گوهر می نشاند گرد در بازار ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای سلیمان اینقدر استادگی در کار نیست</p></div>
<div class="m2"><p>می گشاید ناخن موری گره از کار ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خون ما را پیری از گردون سنگین دل خرید</p></div>
<div class="m2"><p>قامت خم گشته شد انگشتر زنهار ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از قماش دل چه می پرسی، نظر بگشا ببین</p></div>
<div class="m2"><p>ماه کنعان یک خریدار است در بازار ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برنتابد منت تعمیر، دیوار خراب</p></div>
<div class="m2"><p>خضر وقتی کو که بی منت شود معمار ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آفتاب رحمت حق بر دل ما تافته است</p></div>
<div class="m2"><p>اشک شادی چشمه تلخی است در کهسار ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غنچه تصویر وا شد، عقده دل وا نشد</p></div>
<div class="m2"><p>در چه ساعت کرد پیوند این گره در تار ما؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این جواب آن غزل صائب که ملا گفته است</p></div>
<div class="m2"><p>پرده دیگر مزن جز پرده دلدار ما</p></div></div>