---
title: >-
    غزل شمارهٔ ۶۱
---
# غزل شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>نیست از سنگ ملامت غم سر پر شور را</p></div>
<div class="m2"><p>کس نترسانده است از رطل گران مخمور را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما به داغ خود خوشیم ای صبح دست از ما بدار</p></div>
<div class="m2"><p>صرف داغ مهر کن این مرهم کافور را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چرخ عاجز کش چرا در خاک و خونم می کشد؟</p></div>
<div class="m2"><p>پای من دست حمایت بود دایم مور را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قهرمان عشق هر جا مجلس آرایی کند</p></div>
<div class="m2"><p>چینی مودار می داند سر فغفور را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نفس را بدخو به ناز و نعمت دنیا مکن</p></div>
<div class="m2"><p>آب و نان سیر، کاهل می کند مزدور را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسن اگر این است و عالمسوزی رخسار این</p></div>
<div class="m2"><p>می کشد بی تابی غیرت چراغ طور را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رتبه افکار صائب را چه می داند حسود؟</p></div>
<div class="m2"><p>بهره ای از حسن یوسف نیست چشم کور را</p></div></div>