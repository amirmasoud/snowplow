---
title: >-
    غزل شمارهٔ ۱۴۷۲
---
# غزل شمارهٔ ۱۴۷۲

<div class="b" id="bn1"><div class="m1"><p>خواب و بیداری آن نرگس مخمور خوش است</p></div>
<div class="m2"><p>این سرایی است که در بسته و معمور خوش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه همین روی زمین از تو شکر می خندد</p></div>
<div class="m2"><p>کز شکرخند تو در زیر زمین مور خوش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کبابی که بود شور، نمی باشد خوش</p></div>
<div class="m2"><p>دل کبابی است که هر چند بود شور خوش است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاکساری ز بزرگان جهان زیبنده است</p></div>
<div class="m2"><p>این سفالی است که در مجلس فغفور خوش است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در نگین خانه نگین جلوه دیگر دارد</p></div>
<div class="m2"><p>بر سر دار فنا مسند منصور خوش است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خون مرده است به چشم تو شب از مرده دلی</p></div>
<div class="m2"><p>ورنه بیداردلان را شب دیجور خوش است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چند در پرده کسی راز خود اظهار کند؟</p></div>
<div class="m2"><p>ارنی گفتن موسی به سر طور خوش است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوزخ بی هنران صحبت بینایان است</p></div>
<div class="m2"><p>خانه هر چند که تاریک بود عور خوش است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست باز آمدن از فکر و خیال تو مرا</p></div>
<div class="m2"><p>با رفیقان موافق سفر دور خوش است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می زند بر جگر تشنه لبان آب، عقیق</p></div>
<div class="m2"><p>با خیال تو دل صائب مهجور خوش است</p></div></div>