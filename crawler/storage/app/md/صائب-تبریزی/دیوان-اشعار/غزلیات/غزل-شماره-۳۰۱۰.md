---
title: >-
    غزل شمارهٔ ۳۰۱۰
---
# غزل شمارهٔ ۳۰۱۰

<div class="b" id="bn1"><div class="m1"><p>غم من عالم بیدرد را غمخواره می سازد</p></div>
<div class="m2"><p>مسیحا را علاج درد من بیچاره می سازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همین بس شاهد یکرنگی معشوق با عاشق</p></div>
<div class="m2"><p>که بلبل عاشق است و گل گریبان پاره می سازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چرا بر کوه پشت خویش چون فرهاد نگذارد؟</p></div>
<div class="m2"><p>سبکدستی که صد شیرین زسنگ خاره می سازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهر کس نامه ای آید، زند چون شاخ گل بر سر</p></div>
<div class="m2"><p>همین آن سنگدل مکتوب ما را پاره می سازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غزال وحشی من رو به صحرای دگر دارد</p></div>
<div class="m2"><p>مرا هویی ازین وحشت سرا آواره می سازد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تکلف بر طرف، ختم است بر آیینه خودداری</p></div>
<div class="m2"><p>که از خوبان سیمین بر به یک نظاره می سازد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دو عالم گر شود پروانه، شمع از پای ننشیند</p></div>
<div class="m2"><p>به یک عاشق کجا آن آتشین رخساره می سازد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نسوزد دل اگر صائب سرشک ناامیدی را</p></div>
<div class="m2"><p>که از بهر یتیمان مهره گهواره می سازد؟</p></div></div>