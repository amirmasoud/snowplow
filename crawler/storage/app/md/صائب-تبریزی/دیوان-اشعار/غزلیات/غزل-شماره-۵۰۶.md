---
title: >-
    غزل شمارهٔ ۵۰۶
---
# غزل شمارهٔ ۵۰۶

<div class="b" id="bn1"><div class="m1"><p>نیست اندیشه ای از زخم زبان سرکش را</p></div>
<div class="m2"><p>خار و خس بستر سنجاب بود آتش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهره از عمر بود تیره روانان را بیش</p></div>
<div class="m2"><p>زودتر جذب کند خاک می بی غش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوابش از بستر بیگانه پریشان نشود</p></div>
<div class="m2"><p>هر که در زندگی از خاک کند مفرش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم بر شست تو دارند غزالان حرم</p></div>
<div class="m2"><p>خالی از تیر به بازیچه مکن ترکش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جز از جاذبه مهر و محبت صائب</p></div>
<div class="m2"><p>کیست از خانه برون آورد آن سرکش را؟</p></div></div>