---
title: >-
    غزل شمارهٔ ۵۷۱۳
---
# غزل شمارهٔ ۵۷۱۳

<div class="b" id="bn1"><div class="m1"><p>به جرم این که متاع هنر بود بارم</p></div>
<div class="m2"><p>یکی ز گرد کسادی خوران بازارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گهر شود به نهانخانه صدف پنهان</p></div>
<div class="m2"><p>ز غیرت گهر آبدار گفتارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه عرض گوهر خوش آب و رنگ خویش دهم</p></div>
<div class="m2"><p>که مرده خون به رگ رغبت خریدارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر فلک ز شفق دست در حنا دارد</p></div>
<div class="m2"><p>که عقده ای نگشاید ز رشته کارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من بلند نوا را درین چمن مپسند</p></div>
<div class="m2"><p>که غنچه باشد در زیر بال منقارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نرفته است ز دل بر زبان دروغ مرا</p></div>
<div class="m2"><p>کجی گذار ندارد به راست بازارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بده به دست من اکسیر رنگ را ساقی</p></div>
<div class="m2"><p>که همچو برگ خزان دیده است رخسارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غرض زدوری چون من نگاهبانی چیست</p></div>
<div class="m2"><p>به گرد گلشنت انگار خار دیوارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چگونه جان برم از جور آسمان صائب</p></div>
<div class="m2"><p>اگر نه لطف ظفرخان شود هوادارم</p></div></div>