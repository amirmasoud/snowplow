---
title: >-
    غزل شمارهٔ ۲۰۹۳
---
# غزل شمارهٔ ۲۰۹۳

<div class="b" id="bn1"><div class="m1"><p>نتوان به دستگیری اخوان ز راه رفت</p></div>
<div class="m2"><p>یوسف به ریسمان برادر به چاه رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من بودم و دلی که مرا غمگسار بود</p></div>
<div class="m2"><p>آن نیز رفته رفته به خرج نگاه رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رویت ز آفتاب کشید انتقام ما</p></div>
<div class="m2"><p>گر ز آفتاب رنگ ز رخسار ماه رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از جوش مشتری به دلارام من رسید</p></div>
<div class="m2"><p>بر ماه مصر آنچه ز زندان و چاه رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگذر ز عزم هند که بهر زر سفید</p></div>
<div class="m2"><p>نتوان به پای خود به زمین سیاه رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از حرف سرد بر من آتش زبان گذشت</p></div>
<div class="m2"><p>بر شمع آنچه از نفس صبحگاه رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از ظلمت گنه به دل پاک من رسید</p></div>
<div class="m2"><p>بر چشم روشن آنچه ز آب سیاه رفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در محفل وجود مرا زندگی چو شمع</p></div>
<div class="m2"><p>گاهی به اشک صائب و گاهی به آه رفت</p></div></div>