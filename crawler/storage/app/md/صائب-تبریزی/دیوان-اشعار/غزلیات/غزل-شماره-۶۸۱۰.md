---
title: >-
    غزل شمارهٔ ۶۸۱۰
---
# غزل شمارهٔ ۶۸۱۰

<div class="b" id="bn1"><div class="m1"><p>پرده بردار ز رخسار که دیدن داری</p></div>
<div class="m2"><p>سربرآور ز گریبان که دمیدن داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منت خشک چرا می کشی از آب حیات؟</p></div>
<div class="m2"><p>تو که قدرت به لب خویش مکیدن داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم بد دور ز مژگان شکار اندازت</p></div>
<div class="m2"><p>که بر آهوی حرم حق تپیدن داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می چکد گر چه طراوت ز تو چون سروبهشت</p></div>
<div class="m2"><p>قامتی تشنه آغوش کشیدن داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فکر تسخیر تو چون در دل عاشق گذرد؟</p></div>
<div class="m2"><p>که در آیینه ز خود فکر رمیدن داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می کنم رحم به دلسوختگان ای لب یار</p></div>
<div class="m2"><p>گر بدانی که چه مقدار مکیدن داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب این پنبه آسودگی از گوش برآر</p></div>
<div class="m2"><p>اگر از ما هوس ناله شنیدن داری</p></div></div>