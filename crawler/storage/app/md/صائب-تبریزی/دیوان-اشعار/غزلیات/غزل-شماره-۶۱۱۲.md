---
title: >-
    غزل شمارهٔ ۶۱۱۲
---
# غزل شمارهٔ ۶۱۱۲

<div class="b" id="bn1"><div class="m1"><p>آه مظلوم است در بالا دوی ادراک من</p></div>
<div class="m2"><p>از زبردستی به ساق عرش پیچد تاک من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کیست دیگر تا تواند دست با من کوفتن؟</p></div>
<div class="m2"><p>کآسمان باآن زبردستی بود در خاک من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست چین نارسایی در کمند فکرتم</p></div>
<div class="m2"><p>هست گیراتر ز چشم آهوان فتراک من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون پر پروانه سوزد پرده افلاک را</p></div>
<div class="m2"><p>گر نفس در دل ندزدد شعله ادراک من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اشک نیسان چون صدف گوهر شود در سینه ام</p></div>
<div class="m2"><p>وقت تخمی خوش که افتد در زمین پاک من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جوهر ذاتی نمی گرداند از شمشیر روی</p></div>
<div class="m2"><p>می زند سرپنجه با دریا خس و خاشاک من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شمع عالمسوز را انگشت زنهاری کند</p></div>
<div class="m2"><p>چون به محفل رو نهد پروانه بی باک من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سیر چشمان را نظر بر جامه پوشیده نیست</p></div>
<div class="m2"><p>ورنه بوی پیرهن باشد گریبان چاک من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می شود صائب ز سوز سینه ام عالم فروز</p></div>
<div class="m2"><p>گر چراغ کشته ای آرد کسی بر خاک من</p></div></div>