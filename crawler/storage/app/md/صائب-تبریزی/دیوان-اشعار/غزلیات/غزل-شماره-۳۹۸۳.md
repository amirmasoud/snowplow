---
title: >-
    غزل شمارهٔ ۳۹۸۳
---
# غزل شمارهٔ ۳۹۸۳

<div class="b" id="bn1"><div class="m1"><p>ز وصل شوق دل داغدار کم نشود</p></div>
<div class="m2"><p>گرسنه چشمی دام از شکار کم نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز داغ لاله سیاهی نمی برد شبنم</p></div>
<div class="m2"><p>ملال من ز می خوشگوار کم نشود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به آه وناله نفس سوختم ندانستم</p></div>
<div class="m2"><p>که تلخکامی بحر از بخار کم نشود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزار قاصد اگر ناامید برگردد</p></div>
<div class="m2"><p>تردد دل امیدوار کم نشود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هر چه رونکنی روی در تو می آرد</p></div>
<div class="m2"><p>ز پشت آینه نقش ونگار کم نشود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کمند موج ز دریا چه می تواند برد</p></div>
<div class="m2"><p>ز خط طراوت آن گلعذار کم نشود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صفای وقت میسر نمی شود صائب</p></div>
<div class="m2"><p>ز آبگینه دل تا غبار کم نشود</p></div></div>