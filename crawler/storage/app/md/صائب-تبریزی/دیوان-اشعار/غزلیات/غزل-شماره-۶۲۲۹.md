---
title: >-
    غزل شمارهٔ ۶۲۲۹
---
# غزل شمارهٔ ۶۲۲۹

<div class="b" id="bn1"><div class="m1"><p>به خاموشی بدل شد نغمه های دلفریب من</p></div>
<div class="m2"><p>به چشم سرمه دار آمد نوای عندلیب من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بس چین جبین بی نیازی کرده در کارش</p></div>
<div class="m2"><p>صبا را دل گرفت از غنچه حسرت نصیب من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به شاخ ارغوان نبض من گر آشنا گردد</p></div>
<div class="m2"><p>شود شاخ گل تبخاله انگشت طبیب من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نباشم چون ز همزانویی آیینه در آتش؟</p></div>
<div class="m2"><p>که می آید برون از سنگ و از آهن رقیب من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان در عشق رسوایم که خال چهره لاله</p></div>
<div class="m2"><p>به خون رشک می غلطد ز داغ سینه زیب من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز چندین مصرع رنگین یکی صائب خوشم آید</p></div>
<div class="m2"><p>به هر شاخ گلی سر درنیارد عندلیب من</p></div></div>