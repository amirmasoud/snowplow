---
title: >-
    غزل شمارهٔ ۱۴۴۶
---
# غزل شمارهٔ ۱۴۴۶

<div class="b" id="bn1"><div class="m1"><p>دل ازان نخل به امید ثمر خرسندست</p></div>
<div class="m2"><p>گره جبهه خوبان، گره پیوندست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرده خواب گران است سبک مغزان را</p></div>
<div class="m2"><p>سایه بال هما گر چه سعادتمندست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرو را نیست ز پیوند به خاطر گرهی</p></div>
<div class="m2"><p>دل آزاده ما را چه غم فرزندست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دردمندی است پر و بال اثر افغان را</p></div>
<div class="m2"><p>ناخن ناله نی سینه خراش از بندست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که ما را کند آزاد ز خود، قبله ماست</p></div>
<div class="m2"><p>عاشقان را به سر دار فنا سوگندست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باش خرسند به قسمت که درین وحشتگاه</p></div>
<div class="m2"><p>هست اگر جنت دربسته، دل خرسندست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عارفان را گله از وحشت تنهایی نیست</p></div>
<div class="m2"><p>نخل چون خوش ثمر افتد غنی از پیوندست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب از تنگی دل شکوه ز کوته نظری است</p></div>
<div class="m2"><p>که دل غنچه گل چاک ز شکرخندست</p></div></div>