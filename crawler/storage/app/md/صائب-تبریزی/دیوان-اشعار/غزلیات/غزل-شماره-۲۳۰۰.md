---
title: >-
    غزل شمارهٔ ۲۳۰۰
---
# غزل شمارهٔ ۲۳۰۰

<div class="b" id="bn1"><div class="m1"><p>زان پیش کآفتاب بگیرد گلوی صبح</p></div>
<div class="m2"><p>روی خود از می شفقی کن چو روی صبح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان پیش کز غبار نفس بی صفا شود</p></div>
<div class="m2"><p>لبریز کن سبوی خود از آب جوی صبح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خورشید چشم آب دهد از نظاره اش</p></div>
<div class="m2"><p>چون شبنم آن که چشم گشاید به روی صبح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در چشم منکران قیامت نمونه ای است</p></div>
<div class="m2"><p>از جوی شیر گلشن فردوس، جوی صبح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو خفته ای و می شکند خار آتشین</p></div>
<div class="m2"><p>در پای آفتاب ز بس جستجوی صبح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون شمع اگر چه مرگ من از نوشخند اوست</p></div>
<div class="m2"><p>صد پیرهن گداختم از آرزوی صبح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای دل سیاه، عزت پیران نگاه دار</p></div>
<div class="m2"><p>در خون مکش ز باده گلرنگ موی صبح</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواهی که سرخ روی شوی در بسیط خاک</p></div>
<div class="m2"><p>چون گل به آب دیده خود کن وضوی صبح</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون آفتاب خامه صائب علم کشید</p></div>
<div class="m2"><p>پر نور کرد عالمی از گفتگوی صبح</p></div></div>