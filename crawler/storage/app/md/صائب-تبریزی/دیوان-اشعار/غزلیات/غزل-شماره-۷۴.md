---
title: >-
    غزل شمارهٔ ۷۴
---
# غزل شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>نیست از راز نهان من خبر جاسوس را</p></div>
<div class="m2"><p>نبض من بند زبان گردید جالینوس را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی ندامت نیست هر حرفی که از لب سر زند</p></div>
<div class="m2"><p>بخیه زن از خامشی این رخنه افسوس را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناله دل کرد رسوا عشق پنهان مرا</p></div>
<div class="m2"><p>نیست ممکن در بغل کردن نهان ناقوس را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صاحبان کشف بی قدرند در درگاه حق</p></div>
<div class="m2"><p>نیست در دیوان شاهان رتبه ای جاسوس را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست مانع از تماشا جامه فانوس شمع</p></div>
<div class="m2"><p>وای بر شمعی که از پرتو کند فانوس را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون پر و بالی نباشد، راه آزادی است بند</p></div>
<div class="m2"><p>روزن زندان کند دلگیرتر محبوس را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق در هر دل که افروزد چراغ دوستی</p></div>
<div class="m2"><p>چون پر پروانه سوزد پرده ناموس را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عالم معقول بر هر کس که صائب جلوه کرد</p></div>
<div class="m2"><p>نشمرد موج سراب این عالم محسوس را</p></div></div>