---
title: >-
    غزل شمارهٔ ۲۰۴۳
---
# غزل شمارهٔ ۲۰۴۳

<div class="b" id="bn1"><div class="m1"><p>آفاق روشن و مه تابان پدید نیست</p></div>
<div class="m2"><p>پر شور عالمی و نمکدان پدید نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از مهر تا به ذره و از قطره تا محیط</p></div>
<div class="m2"><p>چون گوی در تردد و چوگان پدید نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در موج خیز گل چمن آرا نهان شده است</p></div>
<div class="m2"><p>آب از هجوم سنبل و ریحان پدید نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پوشیده است سبزه بیگانه باغ را</p></div>
<div class="m2"><p>جز بوی خوش اثر ز گلستان پدید نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر برگ سبز، طوطی شیرین تکلمی است</p></div>
<div class="m2"><p>گردی اگر چه از شکرستان پدید نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چندین هزار صید درین دشت پر فریب</p></div>
<div class="m2"><p>در خاک و خون تپیده و پیکان پدید نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در جوش و ذره، چشمه خورشید گم شده است</p></div>
<div class="m2"><p>از موج تشنه، چشمه حیوان پدید نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل واله نظاره و دلدار در حجاب</p></div>
<div class="m2"><p>آیینه محو و چهره جانان پدید نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از انتظار آب گهر خلق چون صدف</p></div>
<div class="m2"><p>یکسر دهن گشاده و نیسان پدید نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مصر از هجوم مشتریان تنگ گشته است</p></div>
<div class="m2"><p>هر چند جلوه مه کنعان پدید نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می خون و شمع آه جگرسوز و دل کباب</p></div>
<div class="m2"><p>بزم نشاط چیده و مهمان پدید نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این جلوه گاه کیست که تا می کنی نگاه</p></div>
<div class="m2"><p>چیزی بغیر دیده حیران پدید نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آورده است چشم جهان بین من غبار؟</p></div>
<div class="m2"><p>یا از غبار خط رخ جانان پدید نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا پا کشند بیجگران از طریق عشق</p></div>
<div class="m2"><p>از کعبه غیر خار مغیلان پدید نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دل در میان داغ جگرسوز گم شده است</p></div>
<div class="m2"><p>از جوش لعل، کوه بدخشان پدید نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بیرون بر از سپهر مرا، روشنی ببین</p></div>
<div class="m2"><p>نور چراغ در ته دامان پدید نیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بند خموشی از دهن من گرفته اند</p></div>
<div class="m2"><p>در عالمی که هیچ زبان دان پدید نیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صائب به شهرهای دگر رو مرا ببین</p></div>
<div class="m2"><p>این سرمه در سواد صفاهان پدید نیست</p></div></div>