---
title: >-
    غزل شمارهٔ ۱۲۲۵
---
# غزل شمارهٔ ۱۲۲۵

<div class="b" id="bn1"><div class="m1"><p>تا به طرف سر کلاه آن شوخ بی پروا شکست</p></div>
<div class="m2"><p>سرکشان را زین شکست افتاد بر دلها شکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این قدر استادگی ای سنگدل در کار نیست</p></div>
<div class="m2"><p>می توان از گردش چشمی خمار ما شکست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خور احسان به سایل ظرف می بخشد کریم</p></div>
<div class="m2"><p>سهل باشد گر سبوی ما درین دریا شکست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بحر چون برجاست مشکل نیست ایجاد حباب</p></div>
<div class="m2"><p>دولت خم پای بر جا باد اگر مینا شکست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فتح باب آسمان در گوشه گیری بسته است</p></div>
<div class="m2"><p>رفت ازین زندان برون هر کس به دامن پا شکست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر قلم بر مردم مجنون نمی باشد، چرا</p></div>
<div class="m2"><p>در بن هر ناخنم نی خشکی سودا شکست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا قیامت پایش از شادی نیاید بر زمین</p></div>
<div class="m2"><p>هر که را خاری ز صحرای طلب در پا شکست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد دل سنگین او سنگ فسان ناله ام</p></div>
<div class="m2"><p>کوهکن را تیشه گر از سختی خارا شکست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جستجوی خار نایابی که در پای من است</p></div>
<div class="m2"><p>خار عالم را به چشم سوزن عیسی شکست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می شوم صد پیرهن از مومیایی نرمتر</p></div>
<div class="m2"><p>سنگ طفلان گر چنین خواهد مرا اعضا شکست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شد چو آتش شعله بینایی من شعله ور</p></div>
<div class="m2"><p>خصم اگر خاری مرا در دیده بینا شکست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شد مرا سنگ ملامت صائب از مردم حجاب</p></div>
<div class="m2"><p>پای در دامان کوه قاف اگر عنقا شکست</p></div></div>