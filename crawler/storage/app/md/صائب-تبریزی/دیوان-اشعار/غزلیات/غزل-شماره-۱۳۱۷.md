---
title: >-
    غزل شمارهٔ ۱۳۱۷
---
# غزل شمارهٔ ۱۳۱۷

<div class="b" id="bn1"><div class="m1"><p>غیر حسرت رزق من زان حسن بی اندازه نیست</p></div>
<div class="m2"><p>فتح باب من ازین میخانه جز خمیازه نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میکشان را روز باران می کند گردآوری</p></div>
<div class="m2"><p>جز رگ ابر بهاران جمع را شیرازه نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باغ جنت در صفا هر چند باشد بی نظیر</p></div>
<div class="m2"><p>پیش ارباب بصیرت همچو روی تازه نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست هر بیهوده نالی را خبر از سوز عشق</p></div>
<div class="m2"><p>مطلب بلبل ز عشق گل به جز آوازه نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تیر تخشی هست هر کس را ازان ابرو کمان</p></div>
<div class="m2"><p>قسمت ما چون کمان از دور جز خمیازه نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لاله در کوه بدخشان خون خود را می خورد</p></div>
<div class="m2"><p>چهره گلرنگ او را احتیاج غازه نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مستمع را صائب از گفتار ما بهره است بیش</p></div>
<div class="m2"><p>چون کمان ما را نصیب از صید جز خمیازه نیست</p></div></div>