---
title: >-
    غزل شمارهٔ ۵۶۰۵
---
# غزل شمارهٔ ۵۶۰۵

<div class="b" id="bn1"><div class="m1"><p>دست بر زلف پریشان سخن یافته ام</p></div>
<div class="m2"><p>چشم بد دور، رگ جان سخن یافته ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شسته ام روی به خوناب جگر لعل صفت</p></div>
<div class="m2"><p>تا رگ کان بدخشان سخن یافته ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نکنم چشمم به عمر ابد خضر سیاه</p></div>
<div class="m2"><p>عمر جاوید ز احسان سخن یافته ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون شکر رفته ام از گرمی فکرت به گداز</p></div>
<div class="m2"><p>تا نصیب از شکرستان سخن یافته ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مورم اما ز شکر ریزی گفتار بلند</p></div>
<div class="m2"><p>مسند از دست سلیمان سخن یافته ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه گهرهای گرانسنگ که در جیب امید</p></div>
<div class="m2"><p>از هواداری نیسان سخن یافته ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جوی شیری که سفیدست ازو روی بهشت</p></div>
<div class="m2"><p>خورده ام خون و ز پستان سخن یافته ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به ستم دست از آن زلف ندارم صائب</p></div>
<div class="m2"><p>چه کنم، سلسله جنبان سخن یافته ام</p></div></div>