---
title: >-
    غزل شمارهٔ ۵۷۳۴
---
# غزل شمارهٔ ۵۷۳۴

<div class="b" id="bn1"><div class="m1"><p>ز بوی باده گلرنگ می پرد رنگم</p></div>
<div class="m2"><p>ز برق شیشه می آب می شود سنگم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرا دلیر نباشد غنیم در جنگم</p></div>
<div class="m2"><p>که تا به شیشه رسد آب می شود سنگم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به آب گوهر من غوطه می خورد خورشید</p></div>
<div class="m2"><p>پیاله از جگر لعل می زند رنگم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غبار حادثه در عین سرمه ساییهاست</p></div>
<div class="m2"><p>نفس چگونه کشد بلبل خوش آهنگم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گلم ولی جگر شیر داده اند مرا</p></div>
<div class="m2"><p>ز آفتاب تجلی نمی پرد رنگم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به یک دو جرعه دیگر خراب می گردم</p></div>
<div class="m2"><p>به یک دو موجه دیگر به بحر یکرنگم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوای من دل عشاق را به جوش آرد</p></div>
<div class="m2"><p>به گوش مردم بیدرد خارج آهنگم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنان ز سردی عالم فسرده دل شده ام</p></div>
<div class="m2"><p>که زخم تیشه شرر بر نیارد از سنگم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه شد که سینه موری نمی توانم خست</p></div>
<div class="m2"><p>که در خراش دل خویش آهنین چنگم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شکسته پایی من شوق را ز پا انداخت</p></div>
<div class="m2"><p>گره به کار فلاخن فتاد از سنگم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو تار چنگ دل خویش را گداخته ام</p></div>
<div class="m2"><p>که آمده است سر زلف فکر در چنگم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مگر فلاخن توفیق دست من گیرد</p></div>
<div class="m2"><p>که پا شکسته چو سنگ نشان فرسنگم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو کز سپهر برون رفته ای به خویش ببال</p></div>
<div class="m2"><p>که من به زیر فلک سبزه ته سنگم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر چه تلخ جبینم چو نیشکر صائب</p></div>
<div class="m2"><p>شکر به تنگ فتاده است در دل تنگم</p></div></div>