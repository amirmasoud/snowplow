---
title: >-
    غزل شمارهٔ ۵۰۰۹
---
# غزل شمارهٔ ۵۰۰۹

<div class="b" id="bn1"><div class="m1"><p>مگر ز موج شراب است رشته سازش؟</p></div>
<div class="m2"><p>که می دود به رگ و پی چو روح آوازش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کباب مطرب رنگین نوای عشق شوم</p></div>
<div class="m2"><p>که ساخت گوشه نشین عندلیب راسازش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو شیشه هرکه به آوازه می کند احسان</p></div>
<div class="m2"><p>گران به گوش خورد بانگ مغز پردازش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کریم اوست که صد جام اگر دهد چون خم</p></div>
<div class="m2"><p>چنان دهد که نگردد بلند آوازش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر جدال به صیاد پیشه ای دارم</p></div>
<div class="m2"><p>که سنگ، سینه کبک است پیش شهبازش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زخون کبک، بدخشان شده است سینه کوه</p></div>
<div class="m2"><p>هنوز تشنه خون است چنگل بازش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کلاه گوشه به خورشید و ماه می شکند</p></div>
<div class="m2"><p>کسی که خامه صائب کند سرافرازش</p></div></div>