---
title: >-
    غزل شمارهٔ ۶۷۵۰
---
# غزل شمارهٔ ۶۷۵۰

<div class="b" id="bn1"><div class="m1"><p>به ظاهر گر دریغ از نامرادان روی خود داری</p></div>
<div class="m2"><p>روانشان تازه از مد رسای بوی خودداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیایی گر برون از خانه آیینه معذوری</p></div>
<div class="m2"><p>که باغ دلگشایی در نظر چون روی خودداری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کجا فکر نظربازان به گرد خاطرت گردد؟</p></div>
<div class="m2"><p>که صد دام تماشا در نظر از موی خودداری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز روی آتشینت چشمه سیماب می گردد</p></div>
<div class="m2"><p>اگر آیینه فولاد پیش روی خودداری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نریزد رنگ یوسف طلعتان چون از تماشایت؟</p></div>
<div class="m2"><p>که بوی پیرهن را سینه چاک از بوی خودداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر چه می نمایی رام در ظاهر، ز پرکاری</p></div>
<div class="m2"><p>پلنگ خشمگین را داغدار از خوی خودداری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه حد دارند نگاه خیره گردد گرد رخسارت؟</p></div>
<div class="m2"><p>که چین زلف را پیوسته در ابروی خودداری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو چون از بس لطافت نیست ممکن در نظر آیی</p></div>
<div class="m2"><p>چه ذرات جهان را گرم جست و جوی خودداری؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز نکهت عذرخواهی می کند زلف رسای تو</p></div>
<div class="m2"><p>به ظاهر گر دریغ از دورگردان روی خودداری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مکن در پیش این سنگین دلان چون تیغ گردن کج</p></div>
<div class="m2"><p>ز قسمت آب باریکی اگر در جوی خودداری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز عکس خود کنی همچون پلنگ خشمگین وحشت</p></div>
<div class="m2"><p>اگر در وقت خشم آیینه پیش روی خودداری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گلابش کن ز آه آتشین پیش از خزان صائب</p></div>
<div class="m2"><p>اگر دلبستگی چون گل به رنگ و بوی خودداری</p></div></div>