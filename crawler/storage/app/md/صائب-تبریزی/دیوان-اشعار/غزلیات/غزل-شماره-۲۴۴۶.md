---
title: >-
    غزل شمارهٔ ۲۴۴۶
---
# غزل شمارهٔ ۲۴۴۶

<div class="b" id="bn1"><div class="m1"><p>غافلی کز دل نفس بی‌یاد یزدان می‌کشد</p></div>
<div class="m2"><p>دلوی خود خالی برون از چاه کنعان می‌کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بیابانی که ما سرگشتگان افتاده‌ایم</p></div>
<div class="m2"><p>پای حیرت گردباد آنجا به دامان می‌کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر به ظاهر زاهد از دنیا کند پهلو تهی</p></div>
<div class="m2"><p>از فریب او مشو غافل که میدان می‌کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از تزلزل قدر آسایش شود ظاهر که خاک</p></div>
<div class="m2"><p>توتیا گردد به چشم هرکه طوفان می‌کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیده مغرور ما مشکل‌پسند افتاده است</p></div>
<div class="m2"><p>ورنه مجنون ناز لیلی از غزالان می‌کشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آتش یاقوت را دست تظلم کوته است</p></div>
<div class="m2"><p>از چه قاتل دامن از خاک شهیدان می‌کشد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا نگردد غافل از حال گرفتاران خویش</p></div>
<div class="m2"><p>عشق چندی ماه کنعان را به زندان می‌کشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رو نمی‌گرداند از چین جبین نفس خسیس</p></div>
<div class="m2"><p>این گدای خیره چوب از دست دربان می‌کشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نخل بارآور ز زیر بار می‌آید برون</p></div>
<div class="m2"><p>جذبه دیوانه سنگ از دست طفلان می‌کشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می‌رسد آزار بدگوهر به روشن‌گوهران</p></div>
<div class="m2"><p>پنجهٔ خونین به روی بحر مرجان می‌کشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا صف مژگان آهو چشم ما را دیده است</p></div>
<div class="m2"><p>خط ز مژگان بر زمین خورشید تابان می‌کشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ناتوانان بر زبردستان عالم غالبند</p></div>
<div class="m2"><p>آب خود از زهره شیر این نیستان می‌کشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هرکه صائب همچو مجنون ذوق رسوایی چشید</p></div>
<div class="m2"><p>منت رطل گران از سنگ طفلان می‌کشد</p></div></div>