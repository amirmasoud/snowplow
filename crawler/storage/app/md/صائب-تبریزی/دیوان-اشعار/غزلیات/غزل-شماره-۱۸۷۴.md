---
title: >-
    غزل شمارهٔ ۱۸۷۴
---
# غزل شمارهٔ ۱۸۷۴

<div class="b" id="bn1"><div class="m1"><p>خاطر چو خرم است به صهبا چه حاجت است؟</p></div>
<div class="m2"><p>دل چون گشاده است به صحرا چه حاجت است؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیر چمن بود پی تحصیل وقت خوش</p></div>
<div class="m2"><p>با وقت خوش به سیر و تماشا چه حاجت است؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیچ است گنج عالم اگر نیست دل غنی</p></div>
<div class="m2"><p>دل چون توانگرست به دنیا چه حاجت است؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست کریم آینه سیماب گوهرست</p></div>
<div class="m2"><p>ابر بهار را به تقاضا چه حاجت است؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما چون کلید خانه به دست تو داده ایم</p></div>
<div class="m2"><p>دیگر درازدستی یغما چه حاجت است؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم از برای روی عزیزان بود به کار</p></div>
<div class="m2"><p>یعقوب را به دیده بینا چه حاجت است؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محتاج بادبان نبود کشتی سپهر</p></div>
<div class="m2"><p>عشاق را به همت والا چه حاجت است؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فردا چو غم زیاده ز امروز می رسد</p></div>
<div class="m2"><p>امروز خوردن غم فردا چه حاجت است؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>موی سفید و روی سیه عیب مشک نیست</p></div>
<div class="m2"><p>با خلق خوش به صورت زیبا چه حاجت است؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>راز دو کون در گره نقطه بسته است</p></div>
<div class="m2"><p>گشتن به هر کتاب سراپا چه حاجت است؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از شمع بی نیاز بود خاک کشتگان</p></div>
<div class="m2"><p>در کوه لعل لاله حمرا چه حاجت است؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>احوال ما به تیغ تو چون آب روشن است</p></div>
<div class="m2"><p>عرض نیاز تشنه به دریا چه حاجت است؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خصمی چو کجروی همه جا در رکاب اوست</p></div>
<div class="m2"><p>افلاک را به دشمنی ما چه حاجت است؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از راه حرف وصوت رسیدن به کنه خلق</p></div>
<div class="m2"><p>با نامه گشاده سیما چه حاجت است؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سرگرمی محبت خوبان مرا بس است</p></div>
<div class="m2"><p>صائب مرا به نشأه صهبا چه حاجت است؟</p></div></div>