---
title: >-
    غزل شمارهٔ ۳۷۳۹
---
# غزل شمارهٔ ۳۷۳۹

<div class="b" id="bn1"><div class="m1"><p>گلی که بلبل ما برگ عیش ازو دارد</p></div>
<div class="m2"><p>هزار مرحله افزون به رنگ و بو دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خبر کسی که ازان حسن عالم آرا یافت</p></div>
<div class="m2"><p>به هر طرف که کند روی، رو به او دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به آبرو ز حیات ابد قناعت کن</p></div>
<div class="m2"><p>که خضر وقت بود هرکه آبرو دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به فکر پا سر آزادگان نمی افتد</p></div>
<div class="m2"><p>که سرو، پای به گل در کنار جو دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دو هفته گرمی هنگامه اش نباشد بیش</p></div>
<div class="m2"><p>علاقه هر که چو بلبل به رنگ و بو دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میان خوف و رجا حالتی است عارف را</p></div>
<div class="m2"><p>که خنده در دهن و گریه در گلو دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز حرف حالت بی مغز را توان دریافت</p></div>
<div class="m2"><p>که در پیاله بود هرچه در کدو دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به سرو سرکشی افتاده است کار مرا</p></div>
<div class="m2"><p>که رفتن دل من حکم آب جو دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز سیر عالم بالا نمی شود غافل</p></div>
<div class="m2"><p>چه شد که سرو به گل پای جستجو دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نخورده کرد سیه مست عندلیبان را</p></div>
<div class="m2"><p>چه باده غنچه این باغ در سبو دارد؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به چاره ساز ز بیچارگی توان پیوست</p></div>
<div class="m2"><p>ترحم است بر آن کس که چاره جو دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فغان که آب نگردیده دل چو شبنم گل</p></div>
<div class="m2"><p>کشش توقع ازان آفتاب رو دارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>امید لطف ز خورشید طلعتی است مرا</p></div>
<div class="m2"><p>که آب زندگی آتش ز خوی او دارد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگرچه سر به هوا اوفتاده آن خم زلف</p></div>
<div class="m2"><p>خبر ز پیچش عشاق موبمو دارد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به هیچ رشته جان نیست تن پرستان را</p></div>
<div class="m2"><p>علاقه ای که دل من به زلف او دارد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بجز سپند کز آتش نمی کند پروا</p></div>
<div class="m2"><p>که ره به محفل آن ترک تندخو دارد؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به هیچ چیز تسلی نمی شود صائب</p></div>
<div class="m2"><p>که حرص عادت طفل بهانه جو دارد</p></div></div>