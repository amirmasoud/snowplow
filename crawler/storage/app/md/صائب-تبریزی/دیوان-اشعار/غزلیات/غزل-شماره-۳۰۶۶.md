---
title: >-
    غزل شمارهٔ ۳۰۶۶
---
# غزل شمارهٔ ۳۰۶۶

<div class="b" id="bn1"><div class="m1"><p>اگر ناقص به روشن گوهری واصل تواند شد</p></div>
<div class="m2"><p>چو ماه نو به اندک فرصتی کامل تواند شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کجا واصل به این بی دست و پایی دل تواند شد؟</p></div>
<div class="m2"><p>چه قطع ره به بال افشانی بسمل تواند شد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندارد گرچه راه کعبه مقصود پایانی</p></div>
<div class="m2"><p>کند هر کس سفر در خویشتن منزل تواند شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر از منقار بلبل بر ندارد مهر خاموشی</p></div>
<div class="m2"><p>درین بستانسرا با غنچه ها یکدل تواند شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امید سرخ رویی روز محشر صورتی دارد</p></div>
<div class="m2"><p>کف خون تو گر گلگونه قاتل تواند شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به فکر کاروان و توشه و مرکب چرا افتد؟</p></div>
<div class="m2"><p>به گردون هر که چون عیسی به یک منزل تواند شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو کز سنگین رکابی لنگری، سامان کشتی کن</p></div>
<div class="m2"><p>که بر خار و خس ما هر کفی ساحل تواند شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نمی گردد زوحشت آشنا مژگان به مژگانم</p></div>
<div class="m2"><p>خوشا صیدی که از صیاد خود غافل تواند شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زوصل شمع گل آن عاشق گستاخ می چیند</p></div>
<div class="m2"><p>که چون پروانه بی تکلیف در محفل تواند شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میسر نیست از رندان خوش مشرب شود زاهد</p></div>
<div class="m2"><p>زمین شور کی از تربیت قابل تواند شد؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کسی را از کریمان نیک محضر می توان گفتن</p></div>
<div class="m2"><p>که در دستش درم مهر لب سایل تواند شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زدامی مرغ زیرک چون جهد دیگر نمی افتد</p></div>
<div class="m2"><p>محال است این که مجنون هر که شد عاقل تواند شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خط شبرنگ در افسون نفس بیهوده می سوزد</p></div>
<div class="m2"><p>محال است این که سحر چشم او باطل تواند شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بنای عشق محکم گردد از معشوق پا بر جا</p></div>
<div class="m2"><p>کجا قمری خلاص از سرو پا در گل تواند شد؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نظرپرداز شو گر نقد می خواهی قیامت را</p></div>
<div class="m2"><p>که چشم دوربین آیینه منزل تواند شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دل سرگشته از حق نیست غافل، هر کجا گردد</p></div>
<div class="m2"><p>زمرکز گردش پرگار کی غافل تواند شد؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر آتش به خار و خس گذارد سرکشی از سر</p></div>
<div class="m2"><p>به چوب گل دل دیوانه هم عاقل تواند شد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سلیمان را به از خاتم بود دلجویی موران</p></div>
<div class="m2"><p>که هر کس دل به دست آورد صاحبدل تواند شد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر آن آزاده باشد چون صنوبر ختم رعنایی</p></div>
<div class="m2"><p>که با دست تهی شیرازه صددل تواند شد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به آزادی سزاوارست اگر تقصیر می ورزد</p></div>
<div class="m2"><p>کسی کز حسن خدمت بنده مقبل تواند شد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گرانقدران نیامیزند صائب با سبک مغزان</p></div>
<div class="m2"><p>به برگ کاه کی آهن ربا مایل تواند شد؟</p></div></div>