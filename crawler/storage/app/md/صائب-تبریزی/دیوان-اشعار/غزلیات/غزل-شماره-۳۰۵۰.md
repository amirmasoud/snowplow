---
title: >-
    غزل شمارهٔ ۳۰۵۰
---
# غزل شمارهٔ ۳۰۵۰

<div class="b" id="bn1"><div class="m1"><p>سیاهی از دل چون گلخن ما برنمی خیزد</p></div>
<div class="m2"><p>چو داغ لاله دود از روزن ما برنمی خیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که با ما می تواند دعوی افتادگی کردن؟</p></div>
<div class="m2"><p>که از افتادگی مو بر تن ما برنمی خیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نسازد دستبرد ابر هرگز خشک دریا را</p></div>
<div class="m2"><p>به افشردن تری از دامن ما برنمی خیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشد از شرم عصیان آب گردد این دل سنگین</p></div>
<div class="m2"><p>به آتش بستگی از آهن ما برنمی خیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زما نتوان شنیدن شکوه ای از سینه صافیها</p></div>
<div class="m2"><p>غباری از زمین روشن ما برنمی خیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زگوهر دور نتوان ساختن گرد یتیمی را</p></div>
<div class="m2"><p>به افشاندن غبار از دامن ما بر نمی خیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نفس از سینه مجروح ما بیرون نمی آید</p></div>
<div class="m2"><p>زدلچسبی نسیم از گلشن ما برنمی خیزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مجو آه از دل خرسند، ما آیینه طبعان را</p></div>
<div class="m2"><p>که دود از خانه بی روزن ما برنمی خیزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به هم چون خوشه پیوسته است صائب دانه دلها</p></div>
<div class="m2"><p>که می سوزد که دود از خرمن ما برنمی خیزد؟</p></div></div>