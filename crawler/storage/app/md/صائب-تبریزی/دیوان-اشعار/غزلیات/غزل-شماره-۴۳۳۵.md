---
title: >-
    غزل شمارهٔ ۴۳۳۵
---
# غزل شمارهٔ ۴۳۳۵

<div class="b" id="bn1"><div class="m1"><p>هر لحظه ترا حسن به صد رنگ برآرد</p></div>
<div class="m2"><p>از دست تو دل کس به چه نیرنگ برآرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محتاج به می نیست رخ لاله عذاران</p></div>
<div class="m2"><p>این جام ز خود باده گلرنگ برآرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون غنچه به دامن دهدش برگ شکرخند</p></div>
<div class="m2"><p>آن را که بهاران به دل تنگ برآرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از ناله بی پرده ما داغ و کباب است</p></div>
<div class="m2"><p>هر کس نفس از سینه به آهنگ برآرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وقت است درین انجمن از تنگدلیها</p></div>
<div class="m2"><p>چون پسته زبان در دهنم زنگ برآرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از چوب و گل و سایه بیدست بهارش</p></div>
<div class="m2"><p>عشق تو کسی را که ز فرهنگ برآرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نومید مباشید که با جاذبه عشق</p></div>
<div class="m2"><p>معشوقه خود کوهکن از سنگ برآرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>محتاج به کاوش نشود چشمه عمرش</p></div>
<div class="m2"><p>هر کس به خراش دل ما چنگ برآرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسیار شکفته است هوای چمن امروز</p></div>
<div class="m2"><p>ترسیم که ما را ز دل تنگ برآرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از دامن تر روی زمین یک گل ابرست</p></div>
<div class="m2"><p>آیینه دل چون کسی از زنگ برآرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب شود آن روز ترا آینه بی رنگ</p></div>
<div class="m2"><p>کان چهره روشن خط شبرنگ برآرد</p></div></div>