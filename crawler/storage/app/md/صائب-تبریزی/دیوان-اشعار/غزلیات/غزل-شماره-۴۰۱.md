---
title: >-
    غزل شمارهٔ ۴۰۱
---
# غزل شمارهٔ ۴۰۱

<div class="b" id="bn1"><div class="m1"><p>به هم پیچد خط مشکین بساط حسن خوبان را</p></div>
<div class="m2"><p>غبار خط لب بام است این خورشید تابان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آن فرصت که نقش خاتم اقبال برگردد</p></div>
<div class="m2"><p>هجوم مور سازد بر سلیمان تنگ میدان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مکن در مد احسان کوتهی در روزگار خط</p></div>
<div class="m2"><p>که نشتر می کند خشکی رگ ابر بهاران را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غبار خط او گفتم شود خاک مراد من</p></div>
<div class="m2"><p>چه دانستم زمین پنهان کند رخسار جانان را؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به این دستور اگر دل می رباید آن خط مشکین</p></div>
<div class="m2"><p>به یک دل می کند محتاج، زلف عنبرافشان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قلم در پنجه یاقوت شد انگشت حیرانی</p></div>
<div class="m2"><p>به دور لعل او تا دید آن خط چو ریحان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا چون روز، روشن بود از جوش خریداران</p></div>
<div class="m2"><p>که خواهد تخته کرد از خط مشکین حسن دکان را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لب جان بخش او را نیست پروا از غبار خط</p></div>
<div class="m2"><p>که ظلمت نیل چشم زخم باشد آب حیوان را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل و دینی به کس نگذاشت زنار سر زلفش</p></div>
<div class="m2"><p>مگر خط بر سر رحم آورد آن نامسلمان را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز طوق قمریان چون دود از روزن هوا گیرد</p></div>
<div class="m2"><p>اگر سرو گلستان بیند آن سرو خرامان را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مگر دارد هوای سیر باغ آن شاخ گل صائب؟</p></div>
<div class="m2"><p>که گل چون غنچه سازد بهر رفتن جمع دامان را</p></div></div>