---
title: >-
    غزل شمارهٔ ۴۹۳۱
---
# غزل شمارهٔ ۴۹۳۱

<div class="b" id="bn1"><div class="m1"><p>عرق رامی کند بی دست و پا لغزنده رخسارش</p></div>
<div class="m2"><p>دهد از دور شبنم آب، چشم خود ز گلزارش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دست رعشه داران ساغر سرشار می ریزد</p></div>
<div class="m2"><p>تراوش می کند خون خوردن از مژگان خونخوارش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هر گلشن که آن شمشاد قامت درخرام آید</p></div>
<div class="m2"><p>خیابان می کشد چون سروقد ازشوق رفتارش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سفید از گریه شد چون قند بادام سیه چشمان</p></div>
<div class="m2"><p>زخط سبز تاشد پسته ای لعل شکربارش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امان برخیزد از شهری که دزدش باعسس سازد</p></div>
<div class="m2"><p>بلایی شد به خط همدست تا شد خال طرارش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز روی آتشین خون سمندر را به جوش آرد</p></div>
<div class="m2"><p>ز شکر طوطیان رامی کند دلسرد گفتارش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نظر چون از گل بی خار این گلزار بردارم؟</p></div>
<div class="m2"><p>که چون مژگان دواند ریشه در دل خار دیوارش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شود جای نفس بر شمع تنگ از جوش پروانه</p></div>
<div class="m2"><p>کند گر اقتباس روشنی صائب ز رخسارش</p></div></div>