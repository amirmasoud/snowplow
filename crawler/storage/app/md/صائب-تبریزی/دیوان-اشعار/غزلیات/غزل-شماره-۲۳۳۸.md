---
title: >-
    غزل شمارهٔ ۲۳۳۸
---
# غزل شمارهٔ ۲۳۳۸

<div class="b" id="bn1"><div class="m1"><p>بر جهان هر کس که از روی تأمل بگذرد</p></div>
<div class="m2"><p>از بساط خار با دامان پر گل بگذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جنگ دارد با توکل، بر توکل اعتماد</p></div>
<div class="m2"><p>آن توکل دارد اینجا کز توکل بگذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رنگ و بو روشن ضمیران را نگردد سنگ راه</p></div>
<div class="m2"><p>چون شود خورشید طالع شبنم از گل بگذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می شود باد خزان شیرازه جمعیتش</p></div>
<div class="m2"><p>عمر هر کس در پریشانی چون سنبل بگذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نکته ها درج است در هر صفحه رخسار گل</p></div>
<div class="m2"><p>چون بر این مجموعه در یک هفته بلبل بگذرد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در بهار از باده گلگون گذشتن مشکل است</p></div>
<div class="m2"><p>واعظ از ما بگذران تا موسم گل بگذرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از تواضع می توان مغلوب کردن خصم را</p></div>
<div class="m2"><p>می شود باریک چون سیلاب از پل بگذرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نغمه سنجانی که صائب از مقامات آگهند</p></div>
<div class="m2"><p>گوش می گیرند هر جا حرف بلبل بگذرد</p></div></div>