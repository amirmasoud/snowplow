---
title: >-
    غزل شمارهٔ ۷۵۵
---
# غزل شمارهٔ ۷۵۵

<div class="b" id="bn1"><div class="m1"><p>از نان و آب نیست بقا و ثبات ما</p></div>
<div class="m2"><p>باشد ز درد و داغ محبت حیات ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یارب نصیب سوخته جانان عشق کن</p></div>
<div class="m2"><p>ته جرعه ای که مانده ز آب حیات ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از سعی، راه عشق به پایان نمی رسد</p></div>
<div class="m2"><p>در ترک کوشش است طریق نجات ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دل هزار عقده ز افلاک داشتیم</p></div>
<div class="m2"><p>حل شد به یک پیاله می مشکلات ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قد راست تا قیام قیامت نمی کند</p></div>
<div class="m2"><p>افتاد هر که از نظر التفات ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>افتاده است چاشنی عشق ما بلند</p></div>
<div class="m2"><p>در شیشه سپهر نگنجد نبات ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیدیم تا یگانگی ذات با صفات</p></div>
<div class="m2"><p>شد محو در تصور ذاتش صفات ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>محراب ماست روی به هر جانب آوریم</p></div>
<div class="m2"><p>زان بی جهت، شده است یکی تا جهات ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب سیاهکاری ما را حساب نیست</p></div>
<div class="m2"><p>روی زمین سیاه شد از سیئات ما</p></div></div>