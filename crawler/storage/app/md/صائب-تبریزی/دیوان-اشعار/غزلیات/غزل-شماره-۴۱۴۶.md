---
title: >-
    غزل شمارهٔ ۴۱۴۶
---
# غزل شمارهٔ ۴۱۴۶

<div class="b" id="bn1"><div class="m1"><p>جمعی که افسر از خرد خام کرده اند</p></div>
<div class="m2"><p>از بحر اختصار به یک جام کرده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بند غم منال که مرغان دوربین</p></div>
<div class="m2"><p>سیر چمن ز روزنه دام کرده اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مستان ز قید شنبه وآدینه فارغند</p></div>
<div class="m2"><p>رو در پیاله پشت به ایام کرده اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در علم آشنایی آن چشم عاجزند</p></div>
<div class="m2"><p>آنان که وحش را به فسون رام کرده اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صد بر گریز ناخن تدبیر دیده است</p></div>
<div class="m2"><p>این غنچه گره که دلش نام کرده اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صائب ز آگهی است که دریاکشان عشق</p></div>
<div class="m2"><p>عادت به خامشی چو لب جام کرده اند</p></div></div>