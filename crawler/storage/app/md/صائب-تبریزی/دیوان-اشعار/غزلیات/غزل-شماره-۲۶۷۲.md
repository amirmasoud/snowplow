---
title: >-
    غزل شمارهٔ ۲۶۷۲
---
# غزل شمارهٔ ۲۶۷۲

<div class="b" id="bn1"><div class="m1"><p>حاش لله از ملامت شوق جانان کم شود</p></div>
<div class="m2"><p>خارخار کعبه از خار مغیلان کم شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست در پیکان سرایت خنده سوفار را</p></div>
<div class="m2"><p>تنگی دلها کی از لبهای خندان کم شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون به خون شستن ندارد جزندامت حاصلی</p></div>
<div class="m2"><p>عشق دردی نیست کز سیر گلستان کم شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وسعت مشرب کند هموار وضع چرخ را</p></div>
<div class="m2"><p>شور سیلاب بهاران در بیابان کم شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا بود پیوسته با لعل لب سیراب یار</p></div>
<div class="m2"><p>آب هیهات است از آن چاه زنخدان کم شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر به بلبل واگذار دیده بانی باغبان</p></div>
<div class="m2"><p>نیست ممکن برگ سبزی از گلستان کم شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می کند خم باده کم جوش را پرزورتر</p></div>
<div class="m2"><p>خوبی یوسف کجا از چاه و زندان کم شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حسن کامل می کند کوتاه دست زلف را</p></div>
<div class="m2"><p>در بلندی سایه خورشید تابان کم شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیده شور سکندر تا بود در چاشنی</p></div>
<div class="m2"><p>خضر را کی تشنگی از آب حیوان کم شود؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عرض نعمت دستگاه حرص را سازد زیاد</p></div>
<div class="m2"><p>نیست ممکن حرص مور از شکرستان کم شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر کند صد ماه نو را هر زمان ماه تمام</p></div>
<div class="m2"><p>نیست ممکن ذره ای از مهر تابان کم شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لنگر بیتابی دریا نمی گردد گهر</p></div>
<div class="m2"><p>کی جنون دیوانه را از سنگ طفلان کم شود؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>باده نتوانست صائب زنگ غم از دل زدود</p></div>
<div class="m2"><p>از گهر گرد یتیمی کی به طوفان کم شود؟</p></div></div>