---
title: >-
    غزل شمارهٔ ۴۱۰۸
---
# غزل شمارهٔ ۴۱۰۸

<div class="b" id="bn1"><div class="m1"><p>اشکم به خاک چهره سیلاب می کشد</p></div>
<div class="m2"><p>در گوش بحر حلقه گرداب میکشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گردن به هر شکار زبون کج نمی کنم</p></div>
<div class="m2"><p>صیاد من نهنگ به قلاب میکشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دارد مگر امید اجابت دعای من</p></div>
<div class="m2"><p>کامروز دل به گوشه محراب میکشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نتوان حریف پاک بران زمانه شد</p></div>
<div class="m2"><p>پرهیز ازان که دست ودهن آب میکشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از عشق هر که را دل گرمی نداده اند</p></div>
<div class="m2"><p>ناز سمور و قاقم و سنجاب میکشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن بسملم که از نگه عجز چشم من</p></div>
<div class="m2"><p>خنجر زدست جرات قصاب میکشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زاهد زآه ساخته خود تمام شب</p></div>
<div class="m2"><p>داغ حبش به چهره محراب میکشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غفلت بود نتیجه گفتارهای پوچ</p></div>
<div class="m2"><p>افسانه عاقبت به شکر خواب می کشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>داغم که بیقراری این درد جانگداز</p></div>
<div class="m2"><p>حرف شکایت از دل بیتاب می کشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زین خاک سرمه خیز دل من سیاه شد</p></div>
<div class="m2"><p>خاطر به سیر دامن سرخاب می کشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب به بیقراری من گر نظر کند</p></div>
<div class="m2"><p>حیرت عنان لرزش سیماب می کشد</p></div></div>