---
title: >-
    غزل شمارهٔ ۶۰۰۹
---
# غزل شمارهٔ ۶۰۰۹

<div class="b" id="bn1"><div class="m1"><p>کار هر بی ظرف نبود عشق پنهان داشتن</p></div>
<div class="m2"><p>سهل کاری نیست اخگر در گریبان داشتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیستون از صبر بالا دست من دارد به یاد</p></div>
<div class="m2"><p>بر سر خود تیغ خوردن، پا به دامان داشتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بخیه تسبیح زاهد عاقبت بر رو فتاد</p></div>
<div class="m2"><p>چند بتوان دام را در خاک پنهان داشتن؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاک بر لب مال اینجا، تا توانی چون مسیح</p></div>
<div class="m2"><p>دست در یک کاسه با خورشید تابان داشتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشتی امید در دریای خون افکندن است</p></div>
<div class="m2"><p>از تنور نوح امید لب نان داشتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ضبط معشوق پریشان گرد کردن مشکل است</p></div>
<div class="m2"><p>چون نگردد خون دلم از پاس پیکان داشتن؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از من آزاده دارد یاد (سرو) بی ثمر</p></div>
<div class="m2"><p>روی خود را تازه با اهل گلستان داشتن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون معلم را نگیرد دود آه کودکان؟</p></div>
<div class="m2"><p>نیست آسان بی گناهان را به زندان داشتن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می زنم امروز و فردا بر جنون از دست عقل</p></div>
<div class="m2"><p>چند صائب پاس ننگ و نام بتوان داشتن؟</p></div></div>