---
title: >-
    غزل شمارهٔ ۳۴۲۳
---
# غزل شمارهٔ ۳۴۲۳

<div class="b" id="bn1"><div class="m1"><p>پیش مژگان درازت که هدف خواهد شد؟</p></div>
<div class="m2"><p>چون تو بر یک طرف افتی که طرف خواهد شد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن ترنج ذقنی را که به آن می نازی</p></div>
<div class="m2"><p>از خط سبز، چو نارنج هدف خواهد شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خرق عادت اگر از خرقه تنها خیزد</p></div>
<div class="m2"><p>صاحب کشف و کرامات، کشف خواهد شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی بازار اگر این است که من می بینم</p></div>
<div class="m2"><p>گوهر از پرده نشینان صدف خواهد شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صائب از هند جگرخوار برون می آیم</p></div>
<div class="m2"><p>دستگیر من اگر شاه نجف خواهد شد</p></div></div>