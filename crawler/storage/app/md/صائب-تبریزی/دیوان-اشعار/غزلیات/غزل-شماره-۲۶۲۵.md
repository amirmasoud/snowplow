---
title: >-
    غزل شمارهٔ ۲۶۲۵
---
# غزل شمارهٔ ۲۶۲۵

<div class="b" id="bn1"><div class="m1"><p>تا عنان اختیار ناقصم در چنگ بود</p></div>
<div class="m2"><p>تا به زانو پایم از خواب گران در سنگ بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاجزان را رحمت حق پرده داری می کند</p></div>
<div class="m2"><p>بودم از صیاد ایمن تا شکارم لنگ بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرمه خواب گران در چشم پر خون داشتم</p></div>
<div class="m2"><p>بستر و بالین من چون لاله تا از سنگ بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از صفای سینه در چشمم جهان تاریک شد</p></div>
<div class="m2"><p>دیو یوسف بود تا آیینه ام در زنگ بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عدل ایزد بر گرفت از من عذاب قبر را</p></div>
<div class="m2"><p>بس که بر من چار دیوار عناصر تنگ بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود در قید محبت تا دلم خود را شناخت</p></div>
<div class="m2"><p>از حلاوت این شکر دایم اسیر تنگ بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آهنم روزی که منزل داشت در دل سنگ را</p></div>
<div class="m2"><p>چون جرس آوازه ام فرسنگ در فرسنگ بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست صائب همچو طوطی قالبی گفتار ما</p></div>
<div class="m2"><p>بلبل ما در حریم بیضه سیر آهنگ بود</p></div></div>