---
title: >-
    غزل شمارهٔ ۲۹۲۳
---
# غزل شمارهٔ ۲۹۲۳

<div class="b" id="bn1"><div class="m1"><p>مرا از لاف نه عجز سخن کوته زبان دارد</p></div>
<div class="m2"><p>زجوهر تیغ من بند خموشی بر زبان دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه از منزل خبر دارم، نه از فرسنگ آگاهی</p></div>
<div class="m2"><p>سرزنجیر مجنون مرا ریگ روان دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکستم قدر خود از جستن درمان، ندانستم</p></div>
<div class="m2"><p>که اینجا مومیایی نیز درد استخوان دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آن صحرا که مرغ من زغفلت دانه می چیند</p></div>
<div class="m2"><p>زمین از تار و پود دام در بر پرنیان دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه بیدردست بلبل در میان نغمه پردازان</p></div>
<div class="m2"><p>که با شغل گرفتاری دماغ گلستان دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پناهی نیست در روی زمین خوشتر زبی برگی</p></div>
<div class="m2"><p>کجا خار سر دیوار پروای خزان دارد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کدامین گرمرو یارب ازین صحرا مسافر شد؟</p></div>
<div class="m2"><p>که هر ریگی درین وادی عقیقی در دهان دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به دست خود سلیمان مور را از خاک می گیرد</p></div>
<div class="m2"><p>که می گوید سبکروحی بزرگی را زیان دارد؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به جرم این که چون گل خنده رو افتاده ام صائب</p></div>
<div class="m2"><p>به قصد جان من هر خار تیری در کمان دارد</p></div></div>