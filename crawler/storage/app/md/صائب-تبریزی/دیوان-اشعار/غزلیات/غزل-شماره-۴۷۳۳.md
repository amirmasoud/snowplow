---
title: >-
    غزل شمارهٔ ۴۷۳۳
---
# غزل شمارهٔ ۴۷۳۳

<div class="b" id="bn1"><div class="m1"><p>ای هر دل از خیال تو میخانه دگر</p></div>
<div class="m2"><p>هر گردشی ز چشم تو پیمانه دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرمرغ پرشکسته ز فکر و خیال تو</p></div>
<div class="m2"><p>دارد بزیر بال پریخانه دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از چشم نیم مست تو هر گوشه گیر را</p></div>
<div class="m2"><p>در کنج فقر گوشه میخانه دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از راه عقل برده برون سرو قامتت</p></div>
<div class="m2"><p>هر فرقه رابه جلوه مستانه دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر رشته ای زشمع جهان سوز عارضت</p></div>
<div class="m2"><p>دامی کشیده در ره پروانه دگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از اشتیاق ذکر تو در دیده ها شده است</p></div>
<div class="m2"><p>هر تار اشگ سبحه صد دانه دگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بوستان ز جلوه مستانه ات شده است</p></div>
<div class="m2"><p>هر طوق قمریی خط پیمانه دگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هردم ز سایه طره کافر نهاد تو</p></div>
<div class="m2"><p>در کعبه رنگ ریخته بتخانه دگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غیر از دل شکسته معمار عقل نیست</p></div>
<div class="m2"><p>در شهربند عشق تو ویرانه دگر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدار هرکه راکه درین بزم یافته است</p></div>
<div class="m2"><p>چشمت به خواب کرده ز افسانه دگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زلف تراست از دل صد چاک عاشقان</p></div>
<div class="m2"><p>در هرخم و شکنج و نهان شانه دگر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در خاک و خون تپیده تیغ ترا بود</p></div>
<div class="m2"><p>هر رخنه ای ز دل در میخانه دگر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرغی که دانه خور شده زان خال دلفریب</p></div>
<div class="m2"><p>چشمش نمی پرد ز پی دانه دگر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صائب مرا ز نشأه سرشار عشق او</p></div>
<div class="m2"><p>هر داغ آتشین شده پیمانه دگر</p></div></div>