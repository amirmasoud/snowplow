---
title: >-
    غزل شمارهٔ ۱۵۸۶
---
# غزل شمارهٔ ۱۵۸۶

<div class="b" id="bn1"><div class="m1"><p>تیغ ابروی ترا جوهر چین می بایست</p></div>
<div class="m2"><p>رقم ناز بر آن لوح جبین می بایست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از گلستان تو هر خار چرا گل چیند؟</p></div>
<div class="m2"><p>شعله خوی تو رعناتر ازین می بایست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند گستاخ رکاب تو ببوسند اغیار؟</p></div>
<div class="m2"><p>قفل و بندی به در خانه زین می بایست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا هوس دست نیابد به شکر دزدیدن</p></div>
<div class="m2"><p>گرد لعل تو حصاری چو نگین می بایست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در بغل جای دهد سرو صفت فاخته را</p></div>
<div class="m2"><p>قد رعنای تو سرکش تر ازین می بایست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا دم خط که دم بازپسین حسن است</p></div>
<div class="m2"><p>غنچه باغ حیا، چین به جبین می بایست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم بر سرمه سیه کردی و رفت آب حیا</p></div>
<div class="m2"><p>نرگس شوخ ترا داغ چنین می بایست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه اسباب جمال تو به جای خویش است</p></div>
<div class="m2"><p>بوسه در کنج لبت گوشه نشین می بایست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بوالهوس کرد وطن بر سر کویش آخر</p></div>
<div class="m2"><p>صائب از بهر جلای تو همین می بایست</p></div></div>