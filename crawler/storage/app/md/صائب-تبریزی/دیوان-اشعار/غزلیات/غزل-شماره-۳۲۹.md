---
title: >-
    غزل شمارهٔ ۳۲۹
---
# غزل شمارهٔ ۳۲۹

<div class="b" id="bn1"><div class="m1"><p>ز سیما می شود روشندلان را مهر و کین پیدا</p></div>
<div class="m2"><p>که در دل هر چه پوشیده است، گردد از جبین پیدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نسازد پرده شب، گوهر شب تاب را پنهان</p></div>
<div class="m2"><p>دل سوزان من باشد ز زلف عنبرین پیدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنین گر چاک سازد سینه ها را زلف مشکینش</p></div>
<div class="m2"><p>نگردد نافه سربسته در صحرای چین پیدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی صد شد ز خط، حسن لب یاقوت فام او</p></div>
<div class="m2"><p>که گردد در نگین دان بیشتر حسن نگین پیدا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخن سنجیده گفتن نیست کار هر تنک ظرفی</p></div>
<div class="m2"><p>نمی گردد ز هر آب تنک، در ثمین پیدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به وا کردن ندارد حاجت این مکتوب سر بسته</p></div>
<div class="m2"><p>که گردد تنگدستی بی سخن از آستین پیدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جگرگاه زمین می شد ز خواب آلودگان خالی</p></div>
<div class="m2"><p>اگر آسودگی می بود در روی زمین پیدا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سیه رویی ندارد راستی در پی، نظر واکن</p></div>
<div class="m2"><p>که این معنی ز نقش راست باشد در نگین پیدا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نبود از درد دین، زین پیش خالی هیچ دل صائب</p></div>
<div class="m2"><p>به درمان در زمان (ما) نگردد درد دین پیدا</p></div></div>