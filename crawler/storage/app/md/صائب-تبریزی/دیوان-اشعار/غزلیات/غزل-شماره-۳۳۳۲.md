---
title: >-
    غزل شمارهٔ ۳۳۳۲
---
# غزل شمارهٔ ۳۳۳۲

<div class="b" id="bn1"><div class="m1"><p>هر که چون غنچه سر خود به گریبان نبرد</p></div>
<div class="m2"><p>وقت رفتن ز گلستان لب خندان نبرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از جهان قسمت ارباب نظر حیرانی است</p></div>
<div class="m2"><p>نرگس از باغ به جز دیده حیران نبرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم ما شور بود، ورنه کدامین ذره است</p></div>
<div class="m2"><p>کز تماشای تو خورشید به دامان نبرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خط گستاخ چها با گل روی تو نکرد</p></div>
<div class="m2"><p>مور اینجاست که فرمان سلیمان نبرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل سودازده عمری است هوایی شده است</p></div>
<div class="m2"><p>آه اگر راه به آن زلف پریشان نبرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترک سر کن که درین دایره بی سر و پا</p></div>
<div class="m2"><p>تا کسی سر ندهد گوی ز میدان نبرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زلفش از حلقه سراپای ازان چشم شده است</p></div>
<div class="m2"><p>که کسی دست به آن سیب زنخدان نبرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاکیان را چه بود غیر گنه راه آورد؟</p></div>
<div class="m2"><p>سیل غیر از خس و خاشاک به عمان نبرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می رسانند به آب اهل طمع، خانه جود</p></div>
<div class="m2"><p>خانه را حاتم طی چون به بیابان نبرد؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در و دیوار به محرومی من می گریند</p></div>
<div class="m2"><p>هیچ کس دامن خالی ز گلستان نبرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو که در مکر و حیل دست ز شیطان بردی</p></div>
<div class="m2"><p>چه خیال است که ایمان ز تو شیطان نبرد؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بازوی همت ما سست عنان افتاده است</p></div>
<div class="m2"><p>ورنه گردون نه کمانی است که فرمان نبرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صائب از بس که خریدار سخن نایاب است</p></div>
<div class="m2"><p>هیچ کس زاهل سخن شعر به دیوان نبرد</p></div></div>