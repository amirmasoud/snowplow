---
title: >-
    غزل شمارهٔ ۶۲۶۷
---
# غزل شمارهٔ ۶۲۶۷

<div class="b" id="bn1"><div class="m1"><p>آه ما با دل جانان چه تواند کردن؟</p></div>
<div class="m2"><p>باد با تخت سلیمان چه تواند کردن؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده شور ازان کان ملاحت داغ است</p></div>
<div class="m2"><p>با نمکزار، نمکدان چه تواند کردن؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می برد تیرگی از شام شکرخنده صبح</p></div>
<div class="m2"><p>خط به آن چهره خندان چه تواند کردن؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عسل سبز شد از خط لب شیرین سخنش</p></div>
<div class="m2"><p>مور با این شکرستان چه تواند کردن؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه کند سختی ایام به دلهای دو نیم؟</p></div>
<div class="m2"><p>سنگ با پسته خندان چه تواند کردن؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخت رویی سپر تیغ حوادث نشود</p></div>
<div class="m2"><p>سینه با آن صف مژگان چه تواند کردن؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه کند زخم زبان با دل خوش مشرب ما؟</p></div>
<div class="m2"><p>شور مجنون به بیابان چه تواند کردن؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شیشه را باده پر زور به هم می شکند</p></div>
<div class="m2"><p>چرخ با باده پرستان چه تواند کردن؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کوه طاقت نشود سد ره شورش عشق</p></div>
<div class="m2"><p>کف بی مغز به طوفان چه تواند کردن؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>موج از چشمه زاینده نمی گردد کم</p></div>
<div class="m2"><p>دیده با خواب پریشان چه تواند کردن؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کمر دشمنی حسن، عبث خط بسته است</p></div>
<div class="m2"><p>مور با ملک سلیمان چه تواند کردن؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زیر گردون چه کند دل که نگردد ساکن؟</p></div>
<div class="m2"><p>در صدف گوهر غلطان چه تواند کردن؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دل عارف نرود از سخن سرد از جای</p></div>
<div class="m2"><p>باد با تخت سلیمان چه تواند کردن؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عقل با عشق محال است برآید صائب</p></div>
<div class="m2"><p>زال با رستم دستان چه تواند کردن؟</p></div></div>