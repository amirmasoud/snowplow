---
title: >-
    غزل شمارهٔ ۱۵۷۰
---
# غزل شمارهٔ ۱۵۷۰

<div class="b" id="bn1"><div class="m1"><p>رتبه عشق و هوس پیش بتان هر دو یکی است</p></div>
<div class="m2"><p>خار خشک و مژه اشک فشان هر دو یکی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل بی خار در آنجاست به خرمن، ورنه</p></div>
<div class="m2"><p>تار گلدسته و آن موی میان هر دو یکی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به نسیمی ز گلستان سفری می گردد</p></div>
<div class="m2"><p>برگ عیش من و اوراق خزان هر دو یکی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشأه لطف دهد خشک و عتابی که تر است</p></div>
<div class="m2"><p>سخن سخت تو و رطل گران هر دو یکی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش آن کس که مرا سر به بیابان داده است</p></div>
<div class="m2"><p>خرده جان من و ریگ روان هر دو یکی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سری آن رشته به همتاب ندارد، ورنه</p></div>
<div class="m2"><p>پیچ و تاب من و آن موی میان هر دو یکی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از بصیرت خبری نیست تهی چشمان را</p></div>
<div class="m2"><p>سنگ و گوهر به ترازوی جهان هر دو یکی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه ضرورت کنی راست به آتش خود را؟</p></div>
<div class="m2"><p>پیش این کج نظران تیر و کمان هر دو یکی است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه خیال است در آیینه مصور گردد؟</p></div>
<div class="m2"><p>عکس رخسار تو و صورت جان هر دو یکی است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در خزان سرو چو ایام بهاران تازه است</p></div>
<div class="m2"><p>دل چو آزاد شود سود و زیان هر دو یکی است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سخن ماست یکی گر چه دل ماست دو نیم</p></div>
<div class="m2"><p>خامه یکدل ما را دو زبان هر دو یکی است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پیش سروی که به گل رفته مرا پا صائب</p></div>
<div class="m2"><p>اشک خونین من و آب روان هر دو یکی است</p></div></div>