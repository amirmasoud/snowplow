---
title: >-
    غزل شمارهٔ ۴۴۱۵
---
# غزل شمارهٔ ۴۴۱۵

<div class="b" id="bn1"><div class="m1"><p>غفلت زدگان دیده بیدار ندانند</p></div>
<div class="m2"><p>از مرده دلی قدر شب تار ندانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رحم است بر آن قوم که بیداری شب را</p></div>
<div class="m2"><p>صد پرده به از دولت بیدار ندانند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دارند در ایام خزان جوش بهاران</p></div>
<div class="m2"><p>حیرت زدگانی که گل از خار ندانند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جمعی که ز سر پای نمودند چو پرگار</p></div>
<div class="m2"><p>یک نقطه درین دایره بیکار ندانند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مغرور کند جوش خریدار گهر را</p></div>
<div class="m2"><p>خوب است که خوبان ره بازار ندانند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا آینه از دست نکویان نگذارند</p></div>
<div class="m2"><p>بیطاقتی تشنه دیدار ندانند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زان خلق دلیرند به گفتار که از جهل</p></div>
<div class="m2"><p>گفتار خود از جمله کردار ندانند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب طمع نامه فضولی است ز خوبان</p></div>
<div class="m2"><p>ما را به پیامی چو سزاوار ندانند</p></div></div>