---
title: >-
    غزل شمارهٔ ۵۸۱
---
# غزل شمارهٔ ۵۸۱

<div class="b" id="bn1"><div class="m1"><p>عیار حسن ز صاحب نظر شود پیدا</p></div>
<div class="m2"><p>که قیمت گهر از دیده ور شود پیدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دهد ثمر ز رگ و ریشه درخت خبر</p></div>
<div class="m2"><p>نهفته های پدر از پسر شود پیدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به رنگ زرد قناعت کن از ریاض جهان</p></div>
<div class="m2"><p>که رنگ سرخ به خون جگر شود پیدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزار نامه عنقا ز کوه قاف رسید</p></div>
<div class="m2"><p>نشد ز گمشده ما خبر شود پیدا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشو به مهر خموشی ز بی زبانان امن</p></div>
<div class="m2"><p>که برق تیغ ز ابر سپر شود پیدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشو به موی سفید از فریب غفلت امن</p></div>
<div class="m2"><p>که خواب های گران در سحر شود پیدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر به صدق قدم در طریق عشق نهی</p></div>
<div class="m2"><p>ترا ز نقش قدم راهبر شود پیدا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو شیشه دل، ندهی تن به سختی ایام</p></div>
<div class="m2"><p>وگرنه لعل ز کوه و کمر شود پیدا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درین زمانه که جوهرشناس نایاب است</p></div>
<div class="m2"><p>چه قدر مردم روشن گهر شود پیدا؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز حرص دانه درین کشتزار نزدیک است</p></div>
<div class="m2"><p>که همچو مور ترا بال و پر شود پیدا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مجو ز هر دل افسرده معنی روشن</p></div>
<div class="m2"><p>که دل چو آب شود این گهر شود پیدا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز همرهان ره دورست عمر جاویدان</p></div>
<div class="m2"><p>سفر خوش است اگر همسفر شود پیدا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عیار فکر ز همفکر می شود ظاهر</p></div>
<div class="m2"><p>که روز معرکه صاحب جگر شود پیدا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>توان ز ساده دلی یافت رازهای مرا</p></div>
<div class="m2"><p>چو رشته ای که ز مغز گهر شود پیدا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر تو چون کف دریا سبک کنی خود را</p></div>
<div class="m2"><p>ترا سفینه ز موج خطر شود پیدا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زمین قابل اگر بهر فکر می طلبی</p></div>
<div class="m2"><p>ز پیش مصرع ما بیشتر شود پیدا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به سیم قلب نگیرند صائب از اخوان</p></div>
<div class="m2"><p>درین زمانه عزیزی اگر شود پیدا</p></div></div>