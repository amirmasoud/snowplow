---
title: >-
    غزل شمارهٔ ۵۸۴۷
---
# غزل شمارهٔ ۵۸۴۷

<div class="b" id="bn1"><div class="m1"><p>ما نام خود ز صفحه دلها سترده ایم</p></div>
<div class="m2"><p>در دفتر جهان ورق باد برده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون سرو تازه روی درین بوستانسرا</p></div>
<div class="m2"><p>در راه گرم و سرد جهان پا فشرده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رقص فلک ز جوش نشاط درون ماست</p></div>
<div class="m2"><p>چون خون مرده گرچه به ظاهر فسرده ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نزدیکتر به پرده چشم است از نگاه</p></div>
<div class="m2"><p>راهی که ما به کعبه مقصود برده ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از صبح پرده سوز خدایا نگاه دار</p></div>
<div class="m2"><p>این رازها که ما به دل شب سپرده ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر خاک ره شویم فرامش نمی کنیم</p></div>
<div class="m2"><p>از چشمه سار تیغ تو آبی که خورده ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از یک نگاه گرم شویم آتش و سپند</p></div>
<div class="m2"><p>هر چند تخم سوخته در خاک مرده ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از آرزوی میوه فردوس فارغیم</p></div>
<div class="m2"><p>دندان صبر بر جگر خود فشرده ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مجنون به ریگ بادیه غمهای خود شمرد</p></div>
<div class="m2"><p>با عقده های دل غم خود ما شمرده ایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگذر ز دستگیری ما ای سبوی خام</p></div>
<div class="m2"><p>ما التجا به پای خم می نبرده ایم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر نقش نیک و بد که چو آیینه دیده ایم</p></div>
<div class="m2"><p>صائب ز لوح خاطر روشن سترده ایم</p></div></div>