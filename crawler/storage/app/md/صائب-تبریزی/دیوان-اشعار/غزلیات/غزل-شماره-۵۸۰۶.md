---
title: >-
    غزل شمارهٔ ۵۸۰۶
---
# غزل شمارهٔ ۵۸۰۶

<div class="b" id="bn1"><div class="m1"><p>چشمی به گریه تر نشد از دود آتشم</p></div>
<div class="m2"><p>یارب چه بود مصلحت از بود آتشم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پروانه مرا به چراغ احتیاج نیست</p></div>
<div class="m2"><p>چون کرم شبچراغ زراندود آتشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آسوده اند سوختگان از گداز عشق</p></div>
<div class="m2"><p>از خامیی که هست مرا، عود آتشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پای چراغ سوختگان است سینه ام</p></div>
<div class="m2"><p>از داغ عشق، کعبه مقصود آتشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوزی که هست در جگر من مرا بس است</p></div>
<div class="m2"><p>خامی نمی کند هوس آلود آتشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیگر عنان گریه نیارد نگاه داشت</p></div>
<div class="m2"><p>در دیده ای که سرمه کشد دود آتشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه مستحق عشقم و نه در خور هوس</p></div>
<div class="m2"><p>بیگانه بهشتم و مردود آتشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاکسترست حاصل نشو و نمای من</p></div>
<div class="m2"><p>بی عاقبت چو خرمن نابود آتشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از آه کم نشد پرکاهی غم از دلم</p></div>
<div class="m2"><p>شد چهره کهربایی ازین دود آتشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون گوهر گرامی آدم درین بساط</p></div>
<div class="m2"><p>مسجود آفرینش و مردود آتشم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب نگشت نرم دل آهنین من</p></div>
<div class="m2"><p>عمری است گر چه در کف داود آتشم</p></div></div>