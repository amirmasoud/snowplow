---
title: >-
    غزل شمارهٔ ۶۱۰۹
---
# غزل شمارهٔ ۶۱۰۹

<div class="b" id="bn1"><div class="m1"><p>بلبلم اما رسد بر لاله و گل ناز من</p></div>
<div class="m2"><p>دست گلچین می رود از کار از آواز من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رشته ذوق گرفتاری به بالم بسته اند</p></div>
<div class="m2"><p>نگذرد از گوشه بام قفس پرواز من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جوهرم را تا به سنگ امتحان زد کوهکن</p></div>
<div class="m2"><p>تیشه فولاد خود را کرد پای انداز من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سهل باشد از فغانم گر قفس مجمر شود</p></div>
<div class="m2"><p>بیضه چون فانوس بود از شعله آواز من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو شاخ پر ثمر وقت است پشتش بشکند</p></div>
<div class="m2"><p>از هجوم عندلیبان گوشه های ساز من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا به دارالامن صلح کل رسیدم، کبک مست</p></div>
<div class="m2"><p>خواب راحت می زند در چنگل شهباز من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صبحم، اما چون شبم در پرده پوشی ها مثل</p></div>
<div class="m2"><p>مشرق لب را نداند آفتاب راز من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سر فرو نارد به شاخ پست طوبی فطرتم</p></div>
<div class="m2"><p>می زند پر در فضای لامکان، انداز من</p></div></div>