---
title: >-
    غزل شمارهٔ ۶۱۵۴
---
# غزل شمارهٔ ۶۱۵۴

<div class="b" id="bn1"><div class="m1"><p>کی سخن خام از لب فرزانه می آید برون؟</p></div>
<div class="m2"><p>باده چون شد پخته از میخانه می آید برون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از زبان خامه من لفظ های آشنا</p></div>
<div class="m2"><p>در لباس معنی بیگانه می آید برون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دانه دل را تو پامال علایق کرده ای</p></div>
<div class="m2"><p>ورنه خرمن ها ازین یک دانه می آید برون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناله ناقوس دارد هر سر مو بر تنم</p></div>
<div class="m2"><p>این سزای آن که از بتخانه می آید برون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در شبستان که بوده است و کجا می خورده است؟</p></div>
<div class="m2"><p>آفتاب امروز خوش مستانه می آید برون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عالمی از داغ عالمسوز ما در آتشند</p></div>
<div class="m2"><p>دود شمع ما ز صد کاشانه می آید برون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کعبه گر آید به استقبال من پر دور نیست</p></div>
<div class="m2"><p>دود شمع ما ز صد کاشانه می آید برون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می تند گرد دهانش همچو خط عنبرین</p></div>
<div class="m2"><p>هر حدیثی کز لب جانانه می آید برون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرد هستی در حریم پاکبازان توتیاست</p></div>
<div class="m2"><p>دست خالی سیل ازین ویرانه می آید برون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جامه فانوس می گردد ز غیرت شمع را</p></div>
<div class="m2"><p>لاله ای کز تربت پروانه می آید برون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در سواد خامه من گفتگوی سهل نیست</p></div>
<div class="m2"><p>زین نیستان نعره شیرانه می آید برون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر کسی در عالم خود شهریار عالم است</p></div>
<div class="m2"><p>وای بر جغدی که از ویرانه می آید برون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نفس را مگذار پا از حد خود بیرون نهد</p></div>
<div class="m2"><p>می شود گم طفل چون از خانه می آید برون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>می شود صائب ز بی تابی دل غواص آب</p></div>
<div class="m2"><p>از صدف تا گوهر یکدانه می آید برون</p></div></div>