---
title: >-
    غزل شمارهٔ ۶۴۰۷
---
# غزل شمارهٔ ۶۴۰۷

<div class="b" id="bn1"><div class="m1"><p>گریان ز کوی او دل ما می رود برون</p></div>
<div class="m2"><p>زین باغ، آب رو به قفا می رود برون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفتی و رفت روشنی از چشم و دل مرا</p></div>
<div class="m2"><p>با میهمان ز خانه صفا می رود برون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر درد استخوان رود از مغز بوریا</p></div>
<div class="m2"><p>درد از دل شکسته ما می رود برون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر رنگ و بوی عالم امکان مبند دل</p></div>
<div class="m2"><p>کز دست همچو رنگ حنا می رود برون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلبل چگونه بال گشاید درین چمن</p></div>
<div class="m2"><p>کز جوش گل نسیم صبا می رود برون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل را نجات داد ز زندان جسم، عشق</p></div>
<div class="m2"><p>خون مشک چو شود ز ختا می رود برون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک ساعت است گرمی هنگامه هوس</p></div>
<div class="m2"><p>زود از سر حباب هوا می رود برون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از سختی زمانه شود چرب نرم دل</p></div>
<div class="m2"><p>نخوت به استخوان ز هما می رود برون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این رعشه ای که در تن ما پی فشرده است</p></div>
<div class="m2"><p>زود این کمان ز قبضه ما می رود برون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آرام نیست موی بر آتش فکنده را</p></div>
<div class="m2"><p>از زلف پیچ و تاب کجا می رود برون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از رهروان عشق مجویید کجروی</p></div>
<div class="m2"><p>کی راست ز تیر قضا می رود برون؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سهل است اگر به باد رود نقد جان ما</p></div>
<div class="m2"><p>قارون ز کوی عشق گدا می رود برون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صائب ز هر طرف که صدایی شود بلند</p></div>
<div class="m2"><p>از خود دل رمیده دل می رود برون</p></div></div>