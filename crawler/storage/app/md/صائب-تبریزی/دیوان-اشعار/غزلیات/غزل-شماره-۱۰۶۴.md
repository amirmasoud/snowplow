---
title: >-
    غزل شمارهٔ ۱۰۶۴
---
# غزل شمارهٔ ۱۰۶۴

<div class="b" id="bn1"><div class="m1"><p>جان نثار یار کردن خاک را زر کردن است</p></div>
<div class="m2"><p>قطره ناچیز را دریای گوهر کردن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوابگاه مرگ را هموار بر خود ساختن</p></div>
<div class="m2"><p>در زمان زندگی از خاک بستر کردن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در جهان آب و گل رنگ اقامت ریختن</p></div>
<div class="m2"><p>در گذار سیل بی زنهار لنگر کردن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو ماهی فلس کردن جمع در بحر وجود</p></div>
<div class="m2"><p>در هلاک خویشتن انشای محضر کردن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کعبه را بتخانه کردن پیش ما آزادگان</p></div>
<div class="m2"><p>از تمناخانه دل را مصور کردن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عقل را با عشق عالمسوز گردیدن طرف</p></div>
<div class="m2"><p>موم را سر پنجه با خورشید انور کردن است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاکساری را بدل با سرفرازی ساختن</p></div>
<div class="m2"><p>پشت بر محراب طاعت بهر منبر کردن است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عافیت کردن طلب در عالم پرشور و شر</p></div>
<div class="m2"><p>جستجوی سایه در صحرای محشر کردن است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زهد را بر وسعت مشرب نمودن اختیار</p></div>
<div class="m2"><p>بهر شیر دایه ترک شیر مادر کردن است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همچو بیدردان ز خون دل به می قانع شدن</p></div>
<div class="m2"><p>با کف بی مغز صلح از بحر گوهر کردن است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هست در روی زمین هر دانه ای را حاصلی</p></div>
<div class="m2"><p>حاصل کوچکدلی دلها مسخر کردن است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عرض مطلب پیش خوی آتشین گلرخان</p></div>
<div class="m2"><p>عودهای خام را در کار مجمر کردن است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تنگ خلقی بر خود و بر خلق سازد کار تنگ</p></div>
<div class="m2"><p>خلق خوش خود را و عالم را معطر کردن است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیک بختان نیستند ایمن ز چشم شور چرخ</p></div>
<div class="m2"><p>شوربختی ها نمک در چشم اختر کردن است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خرد مشمر جرم را کز زخم نیش پشگان</p></div>
<div class="m2"><p>کار فیل کوه پیکر خاک بر سر کردن است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از زمین گیری برآرد ترک دنیا روح را</p></div>
<div class="m2"><p>سکه رایج در جهان از پشت بر زر کردن است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با نگاه خشک قانع زان بهشتی رو شدن</p></div>
<div class="m2"><p>صبر بر لب تشنگی با آب کوثر کردن است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جوهر چین جبهه وا کرده را در کار نیست</p></div>
<div class="m2"><p>صفحه آیینه مستغنی ز مسطر کردن است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر مآل کار خود چون می لرزد دلم</p></div>
<div class="m2"><p>گر چه کار بحر رحمت موم عنبر کردن است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گلرخان را جلوه در آیینه کردن بی حجاب</p></div>
<div class="m2"><p>شمع روشن بر سر خاک سکندر کردن است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مهر خاموشی زدن بر لب درین وحشت سرا</p></div>
<div class="m2"><p>کام تلخ خویش صائب تنگ شکر کردن است</p></div></div>