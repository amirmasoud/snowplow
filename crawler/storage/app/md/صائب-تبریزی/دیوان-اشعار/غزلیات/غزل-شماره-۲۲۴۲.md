---
title: >-
    غزل شمارهٔ ۲۲۴۲
---
# غزل شمارهٔ ۲۲۴۲

<div class="b" id="bn1"><div class="m1"><p>در خودآرایی خطرها مضمرست</p></div>
<div class="m2"><p>حلقه فتراک طاوس از پرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی سبکروحی و تمکین آدمی</p></div>
<div class="m2"><p>کشتی بی بادبان و لنگرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قرب خوبان رنج باریک آورد</p></div>
<div class="m2"><p>رشته را کاهش نصیب از گوهرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق می بخشد تمامی حسن را</p></div>
<div class="m2"><p>شمع بی پروانه تیر بی سرپرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خلق نیکو عیب را سازد هنر</p></div>
<div class="m2"><p>خامی عنبر کمال عنبرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی طلب سیراب می گردد ز می</p></div>
<div class="m2"><p>چون سبو دستی که در زیر سرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرتو منت کند دل را سیاه</p></div>
<div class="m2"><p>زنگ این آیینه از روشنگرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در سخن لعلش قیامت می کند</p></div>
<div class="m2"><p>این نمکدان پر ز شور محشرست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر عذار لیلی آن خال کبود</p></div>
<div class="m2"><p>چشمه خورشید را نیلوفرست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آتش درویشانه خود ای پسر</p></div>
<div class="m2"><p>از طعام میهمانی بهترست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شیر بیگانه است آش دیگران</p></div>
<div class="m2"><p>شوربای خویش شیر مادرست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صائب از شیرینی گفتار خود</p></div>
<div class="m2"><p>طوطی ما بی نیاز از شکرست</p></div></div>