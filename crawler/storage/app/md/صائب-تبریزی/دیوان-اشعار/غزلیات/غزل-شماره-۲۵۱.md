---
title: >-
    غزل شمارهٔ ۲۵۱
---
# غزل شمارهٔ ۲۵۱

<div class="b" id="bn1"><div class="m1"><p>تن به بیماری دهد چشمش پی تسخیر ما</p></div>
<div class="m2"><p>لنگ سازد خویش را آهوی آهوگیر ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خانه ما در ره سیلاب اشک افتاده است</p></div>
<div class="m2"><p>حیف از اوقاتی که گردد صرف در تعمیر ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راه زلف او به طی کردن نمی آید به سر</p></div>
<div class="m2"><p>ورنه کوتاهی ندارد طره شبگیر ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آب می گردیم اگر بر روی ما آری گناه</p></div>
<div class="m2"><p>بگذر ای پیر مغان دانسته از تقصیر ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاک راه انگار و درد جرعه ای بر ما بریز</p></div>
<div class="m2"><p>گرد خجلت را بشو از چهره تقصیر ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو زخم تازه خون گردد روان از جوی شیر</p></div>
<div class="m2"><p>بیستون را بر کمر آید اگر شمشیر ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بس که صائب شد خطا از صید و بر خارا نشست</p></div>
<div class="m2"><p>خنده دندان نما زد اره بر شمشیر ما</p></div></div>