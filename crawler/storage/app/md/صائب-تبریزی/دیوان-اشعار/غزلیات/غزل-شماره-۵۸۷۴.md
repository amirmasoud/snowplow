---
title: >-
    غزل شمارهٔ ۵۸۷۴
---
# غزل شمارهٔ ۵۸۷۴

<div class="b" id="bn1"><div class="m1"><p>نقش مراد اگر چه نشد دستگیر ما</p></div>
<div class="m2"><p>بیرون به زور همت ازین ششدر آمدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردم همان ز سایه ما فیض می برند</p></div>
<div class="m2"><p>مانند سرو و بید اگر بی بر آمدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از زهر سبز شد قلم استخوان ما</p></div>
<div class="m2"><p>تا در مذاق اهل جهان شکر آمدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای عمر برق سیر، شتاب اینقدر چرا</p></div>
<div class="m2"><p>آخر به این جهان نه پی اخگر آمدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صائب فتاد اطلس گردون به پای ما</p></div>
<div class="m2"><p>روزی که از لباس تعلق بر آمدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گشتیم خاک تا ز فلک برتر آمدیم</p></div>
<div class="m2"><p>مردیم تا ز بحر فنا بر سر آمدیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چندین هزار بار فشاندیم خویش را</p></div>
<div class="m2"><p>تا همچو آب در نظر گوهر آمدیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون باده آب شد ز لگد استخوان ما</p></div>
<div class="m2"><p>تا از حریم خم به لب ساغر آمدیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوشوقت شد دماغ پریشان روزگار</p></div>
<div class="m2"><p>روزی که ما چو عود به این مجمر آمدیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما را به چشم شور، حسودان گداختند</p></div>
<div class="m2"><p>هر چند تشنه لب ز لب کوثر آمدیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون کاروان آینه از زنگبار چرخ</p></div>
<div class="m2"><p>در گلخن جهان پی خاکستر آمدیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آیینه را به دامن تر تا به کی نهیم؟</p></div>
<div class="m2"><p>آخر به این جهان پی روشنگر آمدیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای قلزم کرم بفشان گرد راه ما</p></div>
<div class="m2"><p>چون سیل اگر چه بی ادب و خودسر آمدیم</p></div></div>