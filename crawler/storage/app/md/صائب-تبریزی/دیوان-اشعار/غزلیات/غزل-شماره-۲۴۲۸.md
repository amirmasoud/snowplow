---
title: >-
    غزل شمارهٔ ۲۴۲۸
---
# غزل شمارهٔ ۲۴۲۸

<div class="b" id="bn1"><div class="m1"><p>بس که از سرگرمی فکرم نفس تفسیده شد</p></div>
<div class="m2"><p>جوهر تیغ زبانم موی آتش دیده شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سبک سنگی چو کف با موج بودم همعنان</p></div>
<div class="m2"><p>لنگر دریا شدم تا گوهرم سنجیده شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نرم نتوانست کردن آن دل چون سنگ را</p></div>
<div class="m2"><p>گرچه خط سرنوشتم محو از آب دیده شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شوق تا نعل مرا در آتش سوزان گذاشت</p></div>
<div class="m2"><p>خار صحرای ملامت موی آتش دیده شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا غبار خط به روی آتشین او نشست</p></div>
<div class="m2"><p>چشمه خورشید تابان از نظر پوشیده شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هست بر نقص بصیرت رغبت دنیا دلیل</p></div>
<div class="m2"><p>چشم می پوشد زدنیا هر که صاحب دیده شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می فزاید دستگاه طاعت از درماندگی</p></div>
<div class="m2"><p>چار جانب قبله گردد قبله چون پوشیده شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آه کز آتش عنانی مد عمرم چون شهاب</p></div>
<div class="m2"><p>تا نفس را راست کردم چیده و برچیده شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>داشت چون سی پاره بی شیرازه جمعیت مرا</p></div>
<div class="m2"><p>شد حواسم جمع تا صحبت زهم پاشیده شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رفت جمعیت زدلها تا بریدی زلف را</p></div>
<div class="m2"><p>می شود لشکر پریشان چون علم خوابیده شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روی دل درماندگان را بیشتر باشد به حق</p></div>
<div class="m2"><p>شش جهت محراب گردد قبله چون پوشیده شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از غبار خط بجا آمد دل بیتاب من</p></div>
<div class="m2"><p>خاک صائب توتیای چشم طوفان دیده شد</p></div></div>