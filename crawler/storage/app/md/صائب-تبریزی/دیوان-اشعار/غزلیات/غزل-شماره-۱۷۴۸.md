---
title: >-
    غزل شمارهٔ ۱۷۴۸
---
# غزل شمارهٔ ۱۷۴۸

<div class="b" id="bn1"><div class="m1"><p>ز بس به کشتن من تیغ مایل افتاده است</p></div>
<div class="m2"><p>هزار مرتبه در پای قاتل افتاده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو گردباد به گرد سر زمین گردم</p></div>
<div class="m2"><p>که به افتادگی من مقابل افتاده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بال همت گردون نورد من بنگر</p></div>
<div class="m2"><p>به این مبین که مرا رخت در گل افتاده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزار مرحله از کعبه است تا در دل</p></div>
<div class="m2"><p>دلت خوش است که بارت به منزل افتاده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غرض ز صحبت دریا کشاکش است چو موج</p></div>
<div class="m2"><p>وگرنه گوهر تمکین به ساحل افتاده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زبان شمع مگر مصرعی ز صائب خواند؟</p></div>
<div class="m2"><p>که باز شور قیامت به محفل افتاده است</p></div></div>