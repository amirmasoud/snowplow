---
title: >-
    غزل شمارهٔ ۵۵۱۲
---
# غزل شمارهٔ ۵۵۱۲

<div class="b" id="bn1"><div class="m1"><p>دل صد پاره خود را به زلف یار می بندم</p></div>
<div class="m2"><p>من این اوراق را شیرازه از زنار می بندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به چشم خیره رسوا نگاهان برنمی آیم</p></div>
<div class="m2"><p>به افسون گر چه چشم رخنه دیوار می بندم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دم سرد خریداران اگر این چاشنی دارد</p></div>
<div class="m2"><p>شوم گر آب گوهر یخ درین بازار می بندم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زبان در کام چون پیکانم از خشکی نمی گردد</p></div>
<div class="m2"><p>لب خشک از تکلم چون لب سوفار می بندم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز چشمم روی می تابد ز حرفم گوش می گیرد</p></div>
<div class="m2"><p>نگه در چشم می دزدم لب از گفتار می بندم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز تسخیر مزاح سرکش او عاجزم ور نه</p></div>
<div class="m2"><p>به تردستی شعله را با خار می بندم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمر در خون من صد عندلیب مست می بندد</p></div>
<div class="m2"><p>گل داغی اگر بر گوشه دستار می بندم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فرستم نامه چون صائب به آن سنگین دل کافر</p></div>
<div class="m2"><p>به بال نامه بر با رشته زنار می بندم</p></div></div>