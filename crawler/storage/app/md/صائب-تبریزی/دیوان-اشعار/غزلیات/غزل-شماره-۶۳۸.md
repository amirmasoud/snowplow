---
title: >-
    غزل شمارهٔ ۶۳۸
---
# غزل شمارهٔ ۶۳۸

<div class="b" id="bn1"><div class="m1"><p>گرفت خط تو دلهای بی قراران را</p></div>
<div class="m2"><p>غبار، جامه فتح است خاکساران را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز خوان عالم بالاست رزق خاموشان</p></div>
<div class="m2"><p>سحاب آب دهد تیغ کوهساران را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لب تو پرده راز مرا تنک کرده است</p></div>
<div class="m2"><p>شراب دشمن جان است رازداران را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همین نه پشت من از بار دل، شکسته شده است</p></div>
<div class="m2"><p>شکست خامی این میوه شاخساران را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه طرف بست می از صحبت نمک، زنهار</p></div>
<div class="m2"><p>مده به مجلس می راه، هوشیاران را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حضور دایمی از هجر دایمی بترست</p></div>
<div class="m2"><p>ز وصل گل چه تمتع بود هزاران را؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز ماجرای خط و زلف یار دانستم</p></div>
<div class="m2"><p>که رفته رفته خورد مور مغز ماران را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گران چو ابر شب جمعه است بر خاطر</p></div>
<div class="m2"><p>وجود محتسب شهر، میگساران را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ازان ز داغ نهان پرده بر نمی دارم</p></div>
<div class="m2"><p>که دست و دل نشود سرد، لاله کاران را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همای عالم توحید، دانه پرور نیست</p></div>
<div class="m2"><p>ز ما دعا برسانید سبحه داران را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرفته نیست دل صائب از گرفت حسود</p></div>
<div class="m2"><p>محک بلند کند رتبه، خوش عیاران را</p></div></div>