---
title: >-
    غزل شمارهٔ ۱۹۴۶
---
# غزل شمارهٔ ۱۹۴۶

<div class="b" id="bn1"><div class="m1"><p>پوچ است هر سری که نه در وی هوای توست</p></div>
<div class="m2"><p>سهوست سجده ای که نه بر خاک پای توست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طبل رحیل هوش من آواز پای توست</p></div>
<div class="m2"><p>حسرت نصیب دیده من از لقای توست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در پرده های چشم شکر خواب صبح نیست</p></div>
<div class="m2"><p>شیرینیی که در دو لب جانفزای توست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خون می کند عرق ز شفق هر صباح و شام</p></div>
<div class="m2"><p>از بس که آفتاب خجل از لقای توست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در باز کردن در باغ بهشت نیست</p></div>
<div class="m2"><p>فیضی که در گشودن بند قبای توست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ظرف وصال نیست من تنگ ظرف را</p></div>
<div class="m2"><p>طبل رحیل هوش من آواز پای توست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خودداری سپند در آتش بود محال</p></div>
<div class="m2"><p>خالی است جای من به حریمی که جای توست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر شاخ گلی که دست کند در چمن بلند</p></div>
<div class="m2"><p>از روی صدق ورد زبانش دعای توست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر دل رمیده ای که بساط زمانه داشت</p></div>
<div class="m2"><p>امروز در کمند دو زلف رسای توست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ره نیست در حریم تو هر خودپرست را</p></div>
<div class="m2"><p>بیگانه هر که گشت ز خود آشنای توست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون ترک دلبری ننمایند دلبران ؟</p></div>
<div class="m2"><p>چون هر کجا دلی که بود مبتلای توست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>استادگی چگونه کند در نثار جان؟</p></div>
<div class="m2"><p>صائب که مرگ و زندگیش از برای توست</p></div></div>