---
title: >-
    غزل شمارهٔ ۵۷۱۰
---
# غزل شمارهٔ ۵۷۱۰

<div class="b" id="bn1"><div class="m1"><p>به حرف تلخ ز لبهای یار خرسندم</p></div>
<div class="m2"><p>چو طوطیان نبود چشم بر شکر خندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا مکن ز سر کوی خود به خواری دور</p></div>
<div class="m2"><p>که من به یک نگه دور از تو خرسندم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر علاقه به مجنون من ندارد عشق</p></div>
<div class="m2"><p>چرا از چشم غزالان کند نظربندم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو سرو و بید ز بی حاصلی کفایت من</p></div>
<div class="m2"><p>همین بس است که آسوده دل ز پیوندم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به خون بیگنهان نیست تشنه غمزه تو</p></div>
<div class="m2"><p>چنین که من به وصال تو آرزومندم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رسیده است به جایی جنون من صائب</p></div>
<div class="m2"><p>که هیچ کس ز عزیزان نمیدهد پندم</p></div></div>