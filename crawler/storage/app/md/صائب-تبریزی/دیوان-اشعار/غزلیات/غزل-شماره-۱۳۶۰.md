---
title: >-
    غزل شمارهٔ ۱۳۶۰
---
# غزل شمارهٔ ۱۳۶۰

<div class="b" id="bn1"><div class="m1"><p>مد عمر من چو نی در ناله و زاری گذشت</p></div>
<div class="m2"><p>از تهی مغزی حیاتم در سبکساری گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواب غفلت فرصت وا کردن چشمی نداد</p></div>
<div class="m2"><p>روز من در پرده شب از سیه کاری گذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در شبستان عدم شد شمع کافوری مرا</p></div>
<div class="m2"><p>آنچه از شبهای تار من به بیداری گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می توانم بی تأمل سینه زد بر تیغ کوه</p></div>
<div class="m2"><p>لیک نتوانم به آسانی ز همواری گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر دل خوش مشربم چون سایه ابر بهار</p></div>
<div class="m2"><p>سختی دوران سنگین دل به همواری گذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سجده گاه بیخودان را احترامی لازم است</p></div>
<div class="m2"><p>از در میخانه می باید به هشیاری گذشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ظالمان را آیه رحمت بود فرمان غزل</p></div>
<div class="m2"><p>چشم او در دور خط از مردم آزاری گذشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این جواب آن غزل صائب که خسرو گفته است</p></div>
<div class="m2"><p>ضایع آن روزی که مستان را به هشیاری گذشت</p></div></div>