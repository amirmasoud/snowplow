---
title: >-
    غزل شمارهٔ ۵۰۳۲
---
# غزل شمارهٔ ۵۰۳۲

<div class="b" id="bn1"><div class="m1"><p>به هر سیاه درون مشنوان ترانه خویش</p></div>
<div class="m2"><p>زمین پاک طلب کن برای دانه خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زبان خویش به دیوار تا توان مالید</p></div>
<div class="m2"><p>قدم برون مگذار از درون خانه خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گناه زشتی خود را بر آبگینه منه</p></div>
<div class="m2"><p>مکن چو تنگدلان شکوه از زمانه خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل خراب ز خاک مراد کمتر نیست</p></div>
<div class="m2"><p>بخواه حاجت خود را ز آستانه خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درین دو هفته که گل میهمان این چمن است</p></div>
<div class="m2"><p>مباش درپی تعمیر آشیانه خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو زلف ماتمیان در هم است کارجهان</p></div>
<div class="m2"><p>ازین بلای سیه دور دار شانه خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمند گوهر مقصود رشته اشک است</p></div>
<div class="m2"><p>مکن چو شمع قضا گریه شبانه خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به نیم جو نخرد خرمن فلک صائب</p></div>
<div class="m2"><p>ز عقده دل خود هر که ساخت دانه خویش</p></div></div>