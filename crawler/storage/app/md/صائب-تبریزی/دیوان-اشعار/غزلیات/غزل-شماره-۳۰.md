---
title: >-
    غزل شمارهٔ ۳۰
---
# غزل شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>جنت در بسته سازد مهر خاموشی ترا</p></div>
<div class="m2"><p>چهره زرین می کند چون به، نمدپوشی ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حلقه ذکر خدا گردد لب خاموش تو</p></div>
<div class="m2"><p>گر شود توفیق از مردم فراموشی ترا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خانه داری، در گذار سیل لنگر کردن است</p></div>
<div class="m2"><p>می شود حصن سلامت، خانه بر دوشی ترا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هوشمندی می برد بیرون ترا از آب و گل</p></div>
<div class="m2"><p>می نماید صورت دیوار، بیهوشی ترا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوش اگر داری درین بستانسرا هر غنچه ای</p></div>
<div class="m2"><p>می کند با صد زبان تلقین خاموشی ترا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غافلی چون رشته کز سیمین بران روزگار</p></div>
<div class="m2"><p>رنج باریک است حاصل از هم آغوشی ترا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خنده چون مینای می کم کن که چون خالی شدی</p></div>
<div class="m2"><p>می گذارد چرخ بر طاق فراموشی ترا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنچنان کز خار آتش را فزاید سرکشی</p></div>
<div class="m2"><p>بیش شد رعنایی نفس از خشن پوشی ترا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هوشیاری زنگ غفلت می برد صائب ز دل</p></div>
<div class="m2"><p>دل سیه چون لاله می گردد ز می نوشی ترا</p></div></div>