---
title: >-
    غزل شمارهٔ ۷۹۴
---
# غزل شمارهٔ ۷۹۴

<div class="b" id="bn1"><div class="m1"><p>افکنده اند در جگر سنگ رخنه‌ها</p></div>
<div class="m2"><p>از موج تازیانه حکم تو آب‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مجلس شراب تو از شوق می‌زنند</p></div>
<div class="m2"><p>پروانه‌وار سینه بر آتش کباب‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شادم ز پیچ و تاب محبت که می‌رسد</p></div>
<div class="m2"><p>آخر به زلف، سلسله پیچ و تاب‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آه ما در انجمن حسن می‌پرد</p></div>
<div class="m2"><p>چون نامه‌های روز قیامت نقاب‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیدار شو که در شب یلدای نیستی</p></div>
<div class="m2"><p>در پرده است چشم ترا طرفه خواب‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیداری حیات شود منتهی به مرگ</p></div>
<div class="m2"><p>آرامش است عاقبت اضطراب‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تسلیم شو، وگرنه برای سبکسران</p></div>
<div class="m2"><p>تابیده‌اند از رگ گردن طناب‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب به این خوشم که مرا آزموده‌اند</p></div>
<div class="m2"><p>شیرین‌لبان به باده تلخ عتاب‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای حسن پرده‌سوز تو برق نقاب‌ها</p></div>
<div class="m2"><p>روی عرق‌فشان تو سیل حجاب‌ها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از نقطه‌های خال تو در هر نظاره‌ای</p></div>
<div class="m2"><p>بیرون نوشته حرف‌شناسان کتاب‌ها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از انفعال روی تو گل‌های شوخ‌چشم</p></div>
<div class="m2"><p>بر پیرهن فشانده مکرر گلاب‌ها</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در رشته می‌کشند گهرهای آبدار</p></div>
<div class="m2"><p>در موج خیز حسن تو دام سراب‌ها</p></div></div>