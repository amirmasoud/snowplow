---
title: >-
    غزل شمارهٔ ۲۷۲۰
---
# غزل شمارهٔ ۲۷۲۰

<div class="b" id="bn1"><div class="m1"><p>از نظربازان کمال حسن افزون می‌شود</p></div>
<div class="m2"><p>از فشار طوق قمری سرو موزون می‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشکند هرگز خمار آتش از اشک کباب</p></div>
<div class="m2"><p>عشق کی سیراب از دل‌های پرخون می‌شود؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست ممکن یافتن مضمون خط یار را</p></div>
<div class="m2"><p>خوبی خط پرده رخسار مضمون می‌شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برنمی‌آید به ناز بی‌نیازی‌های عشق</p></div>
<div class="m2"><p>ورنه لیلی همچو آهو رام مجنون می‌شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از خمار زندگی هرگز نگردد روی زرد</p></div>
<div class="m2"><p>خون هرکس رزق آن لب‌های میگون می‌شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طوق احسان برنتابد خاطر آزدگان</p></div>
<div class="m2"><p>پاک گوهر از بخیلان بیش ممنون می‌شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست در میخانه تحصیل کمال از راه درس</p></div>
<div class="m2"><p>هرکه چون خُم خالی از خود شد فلاطون می‌شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می‌فزاید رغبت صیاد را دام و کمند</p></div>
<div class="m2"><p>از وفور مال، حرص جاه افزون می‌شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از دل پرخون شکایت صائب از انصاف نیست</p></div>
<div class="m2"><p>می‌شود دریای رحمت دل چو پرخون می‌شود</p></div></div>