---
title: >-
    غزل شمارهٔ ۱۸۱۷
---
# غزل شمارهٔ ۱۸۱۷

<div class="b" id="bn1"><div class="m1"><p>ستاره سوخته عشق را پناهی نیست</p></div>
<div class="m2"><p>در آفتاب قیامت گریزگاهی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به داغ کهنه و نو، روز و شب شود معلوم</p></div>
<div class="m2"><p>به عالمی که منم آفتاب و ماهی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل رمیده من وحشی بیابانی است</p></div>
<div class="m2"><p>که جز زبان ملامت در او گیاهی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر چه آه ندارند در جگر عشاق</p></div>
<div class="m2"><p>نگاه حسرت این قوم کم ز آهی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فغان که در نظر اعتبار لاله رخان</p></div>
<div class="m2"><p>شکسته رنگی عاشق به برگ کاهی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکفته باش که قصر وجود انسان را</p></div>
<div class="m2"><p>به از گشادگی جبهه پیشگاهی نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چگونه بال فشانم به کهکشان صائب؟</p></div>
<div class="m2"><p>مرا که قوت پرواز برگ کاهی نیست</p></div></div>