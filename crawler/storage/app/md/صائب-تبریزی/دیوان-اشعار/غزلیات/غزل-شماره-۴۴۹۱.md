---
title: >-
    غزل شمارهٔ ۴۴۹۱
---
# غزل شمارهٔ ۴۴۹۱

<div class="b" id="bn1"><div class="m1"><p>مباد روی تو از پرده حجاب بر آید</p></div>
<div class="m2"><p>قیامت است چو از مغرب آفتاب بر آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من آن زمان به فراغت بر آورم نفس از دل</p></div>
<div class="m2"><p>که بوی سوختگی از دل کباب بر آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر سخن ز کسادی نشد به خاک برابر</p></div>
<div class="m2"><p>چرا بهم چو زنی گرد از کتاب بر آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نسیم زلف ترا گر گذاربر ختن افتد</p></div>
<div class="m2"><p>نفس گداخته از پوست مشک ناب بر آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سیاهی از دل سالک رود به گوشه نشینی</p></div>
<div class="m2"><p>ستاره از ته این ابر آفتاب بر آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر برند به دوزخ مرا سوال نکرده</p></div>
<div class="m2"><p>وگر نه کیست که از عهده جواب بر آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که می تواند ازان روی دلفریب گذشتن</p></div>
<div class="m2"><p>که از نظاره او عمر از شتاب بر آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گشود پرده ز رخسار حشر صرصر آهم</p></div>
<div class="m2"><p>نشد که روی تو بیرحم از نقاب بر آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به جد وجهد توان راه عشق برد به پایان</p></div>
<div class="m2"><p>اگر ز زلف به شبگیر پیچ وتاب بر آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر فتد به غلط راه جغد در دل تنگم</p></div>
<div class="m2"><p>نفس گداخته صائب ازین خراب بر آید</p></div></div>