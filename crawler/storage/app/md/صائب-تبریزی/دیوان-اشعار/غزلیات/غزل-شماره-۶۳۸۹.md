---
title: >-
    غزل شمارهٔ ۶۳۸۹
---
# غزل شمارهٔ ۶۳۸۹

<div class="b" id="bn1"><div class="m1"><p>در کاسه سپهر کند خاک گرد من</p></div>
<div class="m2"><p>رحم است بر کسی که شود هم نبرد من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در شهربند عافیت از خاکساریم</p></div>
<div class="m2"><p>دیوار می کشد به ره سیل گرد من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی اختیار آب روان می کند ز چشم</p></div>
<div class="m2"><p>چون آفتاب دیدن رخسار زرد من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک نقطه است اشک در مجموعه غمم</p></div>
<div class="m2"><p>یک مصرع است آه ز دیوان درد من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرد یتیمی از گهرم گرچه می چکد</p></div>
<div class="m2"><p>بر هیچ خاطری ننشسته است گرد من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قسمت چو ابر گرد جهان می دواندم</p></div>
<div class="m2"><p>تا از کدام بحر بود آبخورد من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر چند پایه تو بلند اوفتاده است</p></div>
<div class="m2"><p>غافل مشو ز ناله گردون نورد من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نقصان نمی کند کسی از دستگیریم</p></div>
<div class="m2"><p>پایش فرو به گنج رود پایمرد من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب ز می مرا نتوان لاله رنگ ساخت</p></div>
<div class="m2"><p>چون شعله رنگ بست بود روی زرد من</p></div></div>