---
title: >-
    غزل شمارهٔ ۴۷۹
---
# غزل شمارهٔ ۴۷۹

<div class="b" id="bn1"><div class="m1"><p>سخن از صلح مگو، عالم جنگ است اینجا</p></div>
<div class="m2"><p>صحبت شیر و شکر، شیشه و سنگ است اینجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حاصل دلشکنی غیر پشیمانی نیست</p></div>
<div class="m2"><p>مومیایی، عرق خجلت سنگ است اینجا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه کند کوچه و بازار به دیوانه ما؟</p></div>
<div class="m2"><p>دامن دشت جنون سینه تنگ است اینجا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق در هر چه زند دست به جز دامن یار</p></div>
<div class="m2"><p>گر چه تسبیح بود، قید فرنگ است اینجا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خشم خونخوار تو از لطف رباینده ترست</p></div>
<div class="m2"><p>چشم آهو خجل از داغ پلنگ است اینجا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسن مستور به عاشق نتواند پرداخت</p></div>
<div class="m2"><p>عکس طوطی به دل آینه زنگ است اینجا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قدر اگر می طلبی بر در بیرنگی زن</p></div>
<div class="m2"><p>که گهر، خوار به اندازه رنگ است اینجا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کام ما بی سخن تلخ نگردد شیرین</p></div>
<div class="m2"><p>گر همه شیره جان است، شرنگ است اینجا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عجز این نشأه، توانایی آن نشأه بود</p></div>
<div class="m2"><p>از صراط آن گذر در است، که لنگ است اینجا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خطر قلزم عشق است به مقدار شعور</p></div>
<div class="m2"><p>زورق بیخبران کام نهنگ است اینجا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کیست صائب سبک از دشت علایق گذرد؟</p></div>
<div class="m2"><p>دامن ریگ روان در ته سنگ است اینجا</p></div></div>