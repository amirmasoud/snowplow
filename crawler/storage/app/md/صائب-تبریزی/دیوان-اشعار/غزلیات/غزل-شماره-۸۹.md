---
title: >-
    غزل شمارهٔ ۸۹
---
# غزل شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>ساختم از قتل نادم دلربای خویش را</p></div>
<div class="m2"><p>عاقبت زان لب گرفتم خونبهای خویش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فکر دلهای پریشان کی پریشانش کند؟</p></div>
<div class="m2"><p>آن که در پا افکند زلف دوتای خویش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبنم بیگانه ای این غنچه را در کار نیست</p></div>
<div class="m2"><p>تر مکن از باده لعل جانفزای خویش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آه و دودش سنبل و ریحان جنت می شود</p></div>
<div class="m2"><p>در دل هر کس که سازی گرم جای خویش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از خزان هرگز نگردد نوبهارش روی زرد</p></div>
<div class="m2"><p>گر خمیر از اشک من سازی حنای خویش را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر به این سامان خوبی روی در مصر آوری</p></div>
<div class="m2"><p>ماه کنعان رو نما سازد بهای خویش را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گل نخواهی زد، چه جای سنگ، بر دیوانگان</p></div>
<div class="m2"><p>گر بدانی لذت جور و جفای خویش را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یوسف سیمین بدن را تاب این زنجیر نیست</p></div>
<div class="m2"><p>باز کن ای سنگدل بند قبای خویش را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بعد ازین آیینه را بر طاق نسیان می نهی</p></div>
<div class="m2"><p>گر ببینی در دل پاکم صفای خویش را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر چه می دانم شکایت را در او تأثیر نیست</p></div>
<div class="m2"><p>می کنم خالی دل درد آشنای خویش را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ناله ام تا بود کم، صائب اثر بسیار داشت</p></div>
<div class="m2"><p>بی اثر کردم ز بسیاری، نوای خویش را</p></div></div>