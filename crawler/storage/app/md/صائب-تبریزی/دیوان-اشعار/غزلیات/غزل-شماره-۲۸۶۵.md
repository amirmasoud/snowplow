---
title: >-
    غزل شمارهٔ ۲۸۶۵
---
# غزل شمارهٔ ۲۸۶۵

<div class="b" id="bn1"><div class="m1"><p>ز بالیدن ترا هردم لباسی تازه می‌گردد</p></div>
<div class="m2"><p>نگنجد در قبا حسنی که بی‌اندازه می‌گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که را ای غنچه‌لب این لعل میگون است از خوبان؟</p></div>
<div class="m2"><p>که صد برگ از تماشایش گل خمیازه می‌گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نباشد لاله‌ای حاجت جگرگاه بدخشان را</p></div>
<div class="m2"><p>کجا رخسار او منت‌پذیر از غازه می‌گردد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خط هر چند شد زیر و زبر مجموعه حسنت</p></div>
<div class="m2"><p>همان از طاق ابروی تو ایمان تازه می‌گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دعوی لب گشودن می‌دهد یاد از تهی‌مغزی</p></div>
<div class="m2"><p>که چون خُم خالی از می شد بلندآوازه می‌گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عزیزی هرکه را در مصر هستی از سفر آید</p></div>
<div class="m2"><p>مرا داغ دل گم‌گشته از نو تازه می‌گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا گر خنده‌ای چون غنچه در سالی شود روزی</p></div>
<div class="m2"><p>به لب تا از ته دل می‌رسد خمیازه می‌گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز عاشق حسن صائب می‌شود مشهور در خوبی</p></div>
<div class="m2"><p>گلستانی ز یک بلبل بلند آوازه می‌گردد</p></div></div>