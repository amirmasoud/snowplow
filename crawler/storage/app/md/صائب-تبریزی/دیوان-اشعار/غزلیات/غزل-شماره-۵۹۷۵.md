---
title: >-
    غزل شمارهٔ ۵۹۷۵
---
# غزل شمارهٔ ۵۹۷۵

<div class="b" id="bn1"><div class="m1"><p>ای فدای چشم مخمور تو خواب عاشقان</p></div>
<div class="m2"><p>وی بلاگردان زلفت پیچ و تاب عاشقان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر به بیداری غرور حسن مانع می شود</p></div>
<div class="m2"><p>می توان دلهای شب آمد به خواب عاشقان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش ازان دست گل شبنم فرو ریزد به خاک</p></div>
<div class="m2"><p>سر برآر از جیب صبح ای آفتاب عاشقان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شست خورشید قیامت دامن از خون شفق</p></div>
<div class="m2"><p>همچنان خونابه می ریزد کباب عاشقان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گردن ما در کمند پیچ و تاب عقل نیست</p></div>
<div class="m2"><p>زلف معشوقان بود مالک رقاب عاشقان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسن لیلی در رخ مجنون تماشاکردنی است</p></div>
<div class="m2"><p>مگذر از سیر رخ چون ماهتاب عاشقان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از حجاب غنچه بلبل سر به زیر پر کشید</p></div>
<div class="m2"><p>نیست کم از شرم معشوقان حجاب عاشقان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سبحه ریگ روان سررشته را گم کرده است</p></div>
<div class="m2"><p>از شمار درد و داغ بی حساب عاشقان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اعتمادی نیست بر جمعیت برگ خزان</p></div>
<div class="m2"><p>زود می پاشد ز یکدیگر کتاب عاشقان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تیغ یار از خون ما زنجیر جوهر پاره کرد</p></div>
<div class="m2"><p>نشأه دیوانه ای دارد شراب عاشقان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر هوای سیر گردون هست در خاطر ترا</p></div>
<div class="m2"><p>همتی صائب طلب کن از جناب عاشقان</p></div></div>