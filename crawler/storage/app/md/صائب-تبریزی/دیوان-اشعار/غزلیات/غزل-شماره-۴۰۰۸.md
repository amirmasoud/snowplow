---
title: >-
    غزل شمارهٔ ۴۰۰۸
---
# غزل شمارهٔ ۴۰۰۸

<div class="b" id="bn1"><div class="m1"><p>گل از عذار تو چیدن ز من نمی آید</p></div>
<div class="m2"><p>چه جای چیدن دیدن ز من نمی آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو سطحیان به کف از بحر گوهر قانع</p></div>
<div class="m2"><p>به غور حسن رسیدن ز من نمی آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر ز بی پروبالی به خاک بندم نقش</p></div>
<div class="m2"><p>به بال غیر پریدن ز من نمی آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم سیه چو دل شب ازان بود که چو صبح</p></div>
<div class="m2"><p>نفس شمرده کشیدن ز من نمی آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر به تیغ مرا بندبند پاره کنند</p></div>
<div class="m2"><p>ز یار و دوست بریدن ز من نمی آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آتشم که چو آب گهر ز سنگدلی</p></div>
<div class="m2"><p>به کام تشنه چکیدن ز من نمی آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من آن شکسته پر و بال طایرم چون چشم</p></div>
<div class="m2"><p>کز آشیانه پریدن ز من نمی آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نظر به صبح ندارد سیاه بختی من</p></div>
<div class="m2"><p>الف به سینه کشیدن ز من نمی آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برای صید مگس در خرابه دنیا</p></div>
<div class="m2"><p>چو عنکبوت تنیدن ز من نمی آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیم ز دل سیها کز قلم خورم روزی</p></div>
<div class="m2"><p>زبان مار مکیدن ز من نمی آید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ازان ز کام جهان آستین فشان گذرم</p></div>
<div class="m2"><p>که پشت دست گزیدن ز من نمی آید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عطیه ای است که چون خار بر سر دیوار</p></div>
<div class="m2"><p>به پای خلق خلیدن ز من نمی آید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به استقامت من شاخ میوه داری نیست</p></div>
<div class="m2"><p>به زیر بار خمیدن ز من نمی آید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>غبار خاطر آب حیات نتوان شد</p></div>
<div class="m2"><p>به زیر تیغ تپیدن ز من نمی آید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مگر رسد به سرم یار بیخبر ورنه</p></div>
<div class="m2"><p>چو پای خفته دویدن ز من نمی آید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر چه تخم مرا برق ناامیدی سوخت</p></div>
<div class="m2"><p>به این خوشم که دمیدن ز من نمی آید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو سیل تا نکشم بحر را به بر صائب</p></div>
<div class="m2"><p>عنان شوق کشیدن ز من نمی آید</p></div></div>