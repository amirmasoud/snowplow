---
title: >-
    غزل شمارهٔ ۴۸۲۲
---
# غزل شمارهٔ ۴۸۲۲

<div class="b" id="bn1"><div class="m1"><p>ای چشم تو پرده دار اعجاز</p></div>
<div class="m2"><p>مژگان تو سایه پرور ناز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از قافله شکایت ما</p></div>
<div class="m2"><p>چون ریگ روان نخیزد آواز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیشانی صبح و آفتاب اوست</p></div>
<div class="m2"><p>درسینه پاک گوهران راز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر زنده قبای خاک مپسند</p></div>
<div class="m2"><p>دل را ز غبار کین بپرداز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در پرده خون دل حصاری است</p></div>
<div class="m2"><p>از لعل لب تو آب اعجاز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گردانده ز خلق روی امید</p></div>
<div class="m2"><p>کار صائب بود خداساز</p></div></div>