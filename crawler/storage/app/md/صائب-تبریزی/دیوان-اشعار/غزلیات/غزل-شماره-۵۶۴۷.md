---
title: >-
    غزل شمارهٔ ۵۶۴۷
---
# غزل شمارهٔ ۵۶۴۷

<div class="b" id="bn1"><div class="m1"><p>من نه آنم که چو گلچین در گلزار زنم</p></div>
<div class="m2"><p>دست در دامن معشوق خس و خار زنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مدت آمدن و رفتن ایام بهار</p></div>
<div class="m2"><p>آنقدر نیست که گل بر سر دستار زنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من که آزار به ارباب هوس نپسندم</p></div>
<div class="m2"><p>گل چرا بر قفس مرغ گرفتار زنم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هدف چشم بد و نیک شدن دشوارست</p></div>
<div class="m2"><p>به از آن نیست که آیینه به زنگار زنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل تسبیح ز بی قیدی من سوراخ است</p></div>
<div class="m2"><p>دست چون در کمر رشته زنار زنم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی توقف به ته خاک رود چون قارون</p></div>
<div class="m2"><p>من به این درد اگر تکیه به کهسار زنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به خموشی بگذارید دل زار مرا</p></div>
<div class="m2"><p>خون علم گردد اگر زخمه برا ین تار زنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می روم صائب ازین عالم افسرده برون</p></div>
<div class="m2"><p>نان خود چند چو خورشید به دیوار زنم؟</p></div></div>