---
title: >-
    غزل شمارهٔ ۴۶۸۲
---
# غزل شمارهٔ ۴۶۸۲

<div class="b" id="bn1"><div class="m1"><p>چو غنچه نکهت خود از صبا دریغ مدار</p></div>
<div class="m2"><p>ز آشنا سخن آشنا دریغ مدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکستگان جهان را خوش است دل دادن</p></div>
<div class="m2"><p>دل شکسته ز زلف دوتا دریغ مدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مکن مضایقه باآن نگار در کف خون</p></div>
<div class="m2"><p>ز دست وپای بلورین حنا دریغ مدار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به شکر این که ترا خون چو نافه مشک شده است</p></div>
<div class="m2"><p>نفس ز سینه مجروح ما دریغ مدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز رهروی که به دنبال کاروان ماند</p></div>
<div class="m2"><p>نوای خویش چو بانگ درا دریغ مدار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز تلخکام، شکر بازداشتن ستم است</p></div>
<div class="m2"><p>ز هیچ تلخ زبانی دعا دریغ مدار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درین بساط کمالی چو عیب پوشی نیست</p></div>
<div class="m2"><p>ز دوستان لباسی، قبا دریغ مدار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز تنگدستی اگر خرده ای نیفشانی</p></div>
<div class="m2"><p>گشاده رویی خود از گدا دریغ مدار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مباش کم ز نی خشک در جوانمردی</p></div>
<div class="m2"><p>اگر شکر نفشانی، نوا دریغ مدار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به میوه کام جهان چون نمی کنی شیرین</p></div>
<div class="m2"><p>چو سرو سایه ز هر بینوا دریغ مدار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زکات راستی از کجروان مگردان راه</p></div>
<div class="m2"><p>ز هیچ کور درین ره عصا دریغ مدار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شود حلاوت شکر دو مغز از بادام</p></div>
<div class="m2"><p>شکر ز طوطی شیرین نوا دریغ مدار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یکی هزار شود قطره چون به بحر رسد</p></div>
<div class="m2"><p>ز صاحبان نظر توتیا دریغ مدار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>درین ریاض چو ابر بهار شو صائب</p></div>
<div class="m2"><p>ز خار قوت نشو و نما دریغ مدار</p></div></div>