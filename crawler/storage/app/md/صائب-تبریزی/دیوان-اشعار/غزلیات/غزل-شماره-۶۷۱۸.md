---
title: >-
    غزل شمارهٔ ۶۷۱۸
---
# غزل شمارهٔ ۶۷۱۸

<div class="b" id="bn1"><div class="m1"><p>جامه زرین نگردد جمع با سیمین تنی</p></div>
<div class="m2"><p>یوسف از چه برنمی آید ز بی پیراهنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبر چون بادام کن بر خشک مغزی های پوست</p></div>
<div class="m2"><p>جنگ دارد با زبان چرب نان روغنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوشه چشمی ز غمخواران چو نبود غم بلاست</p></div>
<div class="m2"><p>اژدهایی می شود هر خار در بی سوزنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی دهانی تیره دارد مشرب عیش مرا</p></div>
<div class="m2"><p>دود پیچیده است در این خانه از بی روزنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رونگردانند از شمشیر صاحب جوهران</p></div>
<div class="m2"><p>می کند موج خطر بر پشت دریا جوشنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از حنا بستن نگردد پای رفتارش گران</p></div>
<div class="m2"><p>هر که چون برگ خزان شد از گلستان رفتنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آدمی را چشم عبرت بین اگر باشد بس است</p></div>
<div class="m2"><p>آنچه آمد بر سر ابلیس از ما و منی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق اگر داری جهان گو سر به سر زنجیر باش</p></div>
<div class="m2"><p>صاحب سوهان نیندیشد ز بند آهنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از سیه کاران حدیث تو به جرم دیگرست</p></div>
<div class="m2"><p>جامه خود را همان بهتر نشوید گلخنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اشک را در دیده روشندلان آرام نیست</p></div>
<div class="m2"><p>ذره می رقصد در آن روزن که باشد روشنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همت پیران گشاید کارهای سخت را</p></div>
<div class="m2"><p>رخنه در خارا کند تیر کمان صد منی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بی لباسی دارد از زخم زبان ایمن مرا</p></div>
<div class="m2"><p>فارغم از داروگیر خار از بی دامنی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برنمی دارم نظر از پشت پای خویشتن</p></div>
<div class="m2"><p>بس که دیدم صائب از نادیدگان نادیدنی</p></div></div>