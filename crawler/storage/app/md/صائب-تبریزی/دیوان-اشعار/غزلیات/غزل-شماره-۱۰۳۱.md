---
title: >-
    غزل شمارهٔ ۱۰۳۱
---
# غزل شمارهٔ ۱۰۳۱

<div class="b" id="bn1"><div class="m1"><p>وقت خط پهلو تهی از یار کردن مشکل است</p></div>
<div class="m2"><p>در بهاران پشت بر گلزار کردن مشکل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می توان کردن به تلقین زنده خون مرده را</p></div>
<div class="m2"><p>بخت خواب آلود را بیدار کردن مشکل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می گریزند اهل دل از صحبت زهاد خشک</p></div>
<div class="m2"><p>رو به روی صورت دیوار کردن مشکل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می رسد از ذوق هر کاری به معراج کمال</p></div>
<div class="m2"><p>بر امید کارفرما کار کردن مشکل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بحر از باد مخالف می شود شوریده تر</p></div>
<div class="m2"><p>از نصیحت مست را هشیار کردن مشکل است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اختیاری نیست فریاد من از وضع جهان</p></div>
<div class="m2"><p>سیل را خاموش در کهسار کردن مشکل است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می توان بر خود گوارا کرد مرگ تلخ را</p></div>
<div class="m2"><p>زندگانی را به خود هموار کردن مشکل است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هست در آمیزش تردامنان مرگ شرار</p></div>
<div class="m2"><p>پیش خامان سوز خود اظهار کردن مشکل است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در گذر صائب ز دل، افتاد چون در قید زلف</p></div>
<div class="m2"><p>مهره بیرون از دهان مار کردن مشکل است</p></div></div>