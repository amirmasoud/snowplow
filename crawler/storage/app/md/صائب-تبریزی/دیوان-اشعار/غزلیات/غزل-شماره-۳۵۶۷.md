---
title: >-
    غزل شمارهٔ ۳۵۶۷
---
# غزل شمارهٔ ۳۵۶۷

<div class="b" id="bn1"><div class="m1"><p>شب که روی تو ز می در عرق افشانی بود</p></div>
<div class="m2"><p>دل سراسیمه تر از کشتی طوفانی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خار در پیرهنم جوهر ذاتی می ریخت</p></div>
<div class="m2"><p>بس که چون تیغ مرا ذوق ز عریانی بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده شوخ به گرداب غم انداخت مرا</p></div>
<div class="m2"><p>یاد آن روز که در عالم حیرانی بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شیشه و سنگ بغل گیری هم می کردند</p></div>
<div class="m2"><p>چه صفا بود که در عالم روحانی بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شوق روزی که به گرد تو مرا می گرداند</p></div>
<div class="m2"><p>آسمان صورت دیوار گرانجانی بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شهری از حسن غریب تو بیابانی شد</p></div>
<div class="m2"><p>عاشق لیلی اگر یک دو بیابانی بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از سر کوی تو روزی که به جنت رفتم</p></div>
<div class="m2"><p>توشه راه من از اشک پشیمانی بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون قلم تا کمر هستی ناقص بستم</p></div>
<div class="m2"><p>تیغ دایم به سرم از خط پیشانی بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا برآورد سر از حلقه مستی صائب</p></div>
<div class="m2"><p>دل ما شانه کش زلف پریشانی بود</p></div></div>