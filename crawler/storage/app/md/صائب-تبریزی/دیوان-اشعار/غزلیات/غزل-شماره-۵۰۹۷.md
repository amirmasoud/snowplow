---
title: >-
    غزل شمارهٔ ۵۰۹۷
---
# غزل شمارهٔ ۵۰۹۷

<div class="b" id="bn1"><div class="m1"><p>هرکه بیند به چشم بیمارش</p></div>
<div class="m2"><p>می شود درزمان پرستارش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>توبه را می کند خراباتی</p></div>
<div class="m2"><p>لب میگون و چشم خمارش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زندگانی به خضر بخشیده است</p></div>
<div class="m2"><p>آب حیوان ز شرم گفتارش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبح عیدست در دل شب قدر</p></div>
<div class="m2"><p>درشبستان زلف، رخسارش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مغز دراستخوان شود شیرین</p></div>
<div class="m2"><p>چون بخندد لب شکر بارش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سنگ برسینه می زند از کوه</p></div>
<div class="m2"><p>کبک درروزگار رفتارش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صلح داده است آب و آتش را</p></div>
<div class="m2"><p>آتش آبدار رخسارش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تاب نگذاشتند در دلها</p></div>
<div class="m2"><p>خط مشکین و زلف طرارش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خون به دلهای عاشقان کردن</p></div>
<div class="m2"><p>می چکد چون عرق ز رخسارش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خار دیوار می شود مژه اش</p></div>
<div class="m2"><p>هرکه آید به سیر گلزارش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در ترازو به جای سنگ نهد</p></div>
<div class="m2"><p>یوسف مصر را خریدارش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لرزش زلف یار بیجا نیست</p></div>
<div class="m2"><p>شیشه صد دل است دربارش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قامت اوست سر خط صائب</p></div>
<div class="m2"><p>چون نگردد بلند گفتارش ؟</p></div></div>