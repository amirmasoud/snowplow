---
title: >-
    غزل شمارهٔ ۲۶۰۳
---
# غزل شمارهٔ ۲۶۰۳

<div class="b" id="bn1"><div class="m1"><p>کی به کوشش عاقلان را نشأه سودا دهند؟</p></div>
<div class="m2"><p>عشق تشریفی بود کز عالم بالا دهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عارفان چون دل به آن یکتای بی همتا دهند</p></div>
<div class="m2"><p>هر دو عالم را طلاق اول به پشت پا دهند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست در دامان همت زن که گوهر می شود</p></div>
<div class="m2"><p>قطره آبی اگر از عالم بالا دهند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که چون پیکان زبان او بود با دل یکی</p></div>
<div class="m2"><p>راست کیشان چون خدنگش بر سر خود جا دهند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آتش دوزخ زننگ ما نهان در سنگ شد</p></div>
<div class="m2"><p>نامه ما را مگر فردا به دست ما دهند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پایه عزت بلندی گیرد از افتادگی</p></div>
<div class="m2"><p>از قلم چون حرفی افتد در کنارش جا دهند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مستی غفلت عنان صائب زدست ما ربود</p></div>
<div class="m2"><p>چون عنان اختیار ما به دست ما دهند؟</p></div></div>