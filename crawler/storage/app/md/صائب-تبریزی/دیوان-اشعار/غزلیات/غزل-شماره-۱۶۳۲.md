---
title: >-
    غزل شمارهٔ ۱۶۳۲
---
# غزل شمارهٔ ۱۶۳۲

<div class="b" id="bn1"><div class="m1"><p>این چه حرف است که در عالم بالاست بهشت؟</p></div>
<div class="m2"><p>هر کجا وقت خوشی رو دهد آنجاست بهشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باده هر جا که بود چشمه کوثر نقدست</p></div>
<div class="m2"><p>هر کجا سرو قدی هست دو بالاست بهشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل رم کرده ندارد گله از تنهایی</p></div>
<div class="m2"><p>که به وحشت زدگان دامن صحراست بهشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از درون سیه توست جهان چون دوزخ</p></div>
<div class="m2"><p>دل اگر تیره نباشد همه دنیاست بهشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دارد از خلد ترا بی بصریها محجوب</p></div>
<div class="m2"><p>ورنه در چشم و دل پاک مهیاست بهشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هست در پرده آتش رخ گلزار خلیل</p></div>
<div class="m2"><p>در دل سوختگان انجمن آراست بهشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عمر زاهد به سر آمد به تمنای بهشت</p></div>
<div class="m2"><p>نشد آگاه که در ترک تمناست بهشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب از روی بهشتی صفتان چشم مپوش</p></div>
<div class="m2"><p>که درین آینه بی پرده هویداست بهشت</p></div></div>