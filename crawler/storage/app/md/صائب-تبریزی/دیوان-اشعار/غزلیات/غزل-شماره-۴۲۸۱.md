---
title: >-
    غزل شمارهٔ ۴۲۸۱
---
# غزل شمارهٔ ۴۲۸۱

<div class="b" id="bn1"><div class="m1"><p>دل بی غبار از لب خاموش می شود</p></div>
<div class="m2"><p>از جوهر آب آینه خس پوش می شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی مغز را کند دهن بسته مغزدار</p></div>
<div class="m2"><p>خوان تهی نهفته به سرپوش می شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در هر کجا فسانه چشم تو سر کنند</p></div>
<div class="m2"><p>چشم غزال، خواب فراموش می شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از صبح اگر خموش شود شمع دیگران</p></div>
<div class="m2"><p>روشن دلم ز صبح بناگوش می شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گردش ز جا نخیزد اگر خاک ره شود</p></div>
<div class="m2"><p>هر کس که از خرام تو مدهوش می شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بار سلاح عاریه مردان نمی کشند</p></div>
<div class="m2"><p>دریا ز موج خویش زره پوش می شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تلخی که نوش جان کنی آن را، شود شکر</p></div>
<div class="m2"><p>نیشی که در جگر شکنی نوش می شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می حسن را ز پرده شرم آورد برون</p></div>
<div class="m2"><p>گل در شکفتگی همه آغوش می شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون آب ازانفعال فرو می رودبه خاک</p></div>
<div class="m2"><p>سروی که با نهال تو همدوش می شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صائب خموش باش که در مجلس شراب</p></div>
<div class="m2"><p>می عاجز از پیاله خاموش می شود</p></div></div>