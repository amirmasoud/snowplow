---
title: >-
    غزل شمارهٔ ۳۱۸۷
---
# غزل شمارهٔ ۳۱۸۷

<div class="b" id="bn1"><div class="m1"><p>به مهر و مه کجا از مغز ما سودا برون آید؟</p></div>
<div class="m2"><p>می روشن مگر از مشرق مینا برون آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به چشم تنگ، سوزن رشته را هموار می سازد</p></div>
<div class="m2"><p>سخن باریک گردد تا از ان لبها برون آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چسان دزدیده بینم روی او، کز شوق دیدارش</p></div>
<div class="m2"><p>شرر ازخانه دربسته خارا برون آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زغمخواران مگر غم دست بردارد زدل، ورنه</p></div>
<div class="m2"><p>به پای خویش هیهات است خار از پا برون آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو از زنگ علایق سینه خود را مصفا کن</p></div>
<div class="m2"><p>که چون شد صبح، خورشید جهان آرا برون آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غباری نیست بر خاطر زغربت جان روشن را</p></div>
<div class="m2"><p>که بینا می شود گوهر چو از دریا برون آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمی باشد ملالت جغد را از خانه ویران</p></div>
<div class="m2"><p>حریصان را کجا از دل غم دنیا برون آید؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ندارد حاصلی جز تیره روزی پرتو منت</p></div>
<div class="m2"><p>که ماه از شرم نور عاریت شبها برون آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لب میگون او هم می شود شیرین سخن صائب</p></div>
<div class="m2"><p>رگ تلخی اگر از گوهر صهبا برون آید</p></div></div>