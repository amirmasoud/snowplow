---
title: >-
    غزل شمارهٔ ۲۴۱۳
---
# غزل شمارهٔ ۲۴۱۳

<div class="b" id="bn1"><div class="m1"><p>از گرفتاری دلم فارغ زپیچ و تاب شد</p></div>
<div class="m2"><p>ناله زنجیر، خوابم را صدای آب شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کوته است از حرف خاموشان زبان اعتراض</p></div>
<div class="m2"><p>ایمن از تیغ است هر خونی که مشک ناب شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهل دارد همچنان خم در خم عصیان مرا</p></div>
<div class="m2"><p>گر به ظاهر قامت خم گشته ام محراب شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با فقیران دست در یک کاسه کردن عیب نیست</p></div>
<div class="m2"><p>بحر با آن منزلت همکاسه گرداب شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون رگ سنگ است اهل درد را بر دل گران</p></div>
<div class="m2"><p>در میان زخمها زخمی که بی خوناب شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شرم هیهات است خوبان را سپرداری کند</p></div>
<div class="m2"><p>هاله نتواند حجاب پرتو مهتاب شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر برآرد می زخود پیمانه ما دور نیست</p></div>
<div class="m2"><p>پنجه مرجان نگارین در میان آب شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فکر من صائب جهان خاک را دل زنده کرد</p></div>
<div class="m2"><p>این سفال خشک از ریحان من سیراب شد</p></div></div>