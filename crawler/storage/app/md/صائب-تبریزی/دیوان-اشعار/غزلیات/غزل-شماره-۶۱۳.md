---
title: >-
    غزل شمارهٔ ۶۱۳
---
# غزل شمارهٔ ۶۱۳

<div class="b" id="bn1"><div class="m1"><p>به اعتبار جهان هیچ کار نیست مرا</p></div>
<div class="m2"><p>دماغ دشمنی روزگار نیست مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو تخم سوخته خاکسترست حاصل من</p></div>
<div class="m2"><p>امید تربیت از نوبهار نیست مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بر و بحر چو گوهر یکی است نسبت من</p></div>
<div class="m2"><p>گشایشی ز میان و کنار نیست مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر به خاک برابر کند مرا گردون</p></div>
<div class="m2"><p>به دل غباری ازین رهگذار نیست مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به پاسبانی اوقات خویش مشغولم</p></div>
<div class="m2"><p>به هیچ کار جهان، هیچ کار نیست مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی است مطلب من چون موحدان جهان</p></div>
<div class="m2"><p>دو چشم در ره مطلب چهار نیست مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز فکر نعمت الوان به خون نمی غلطم</p></div>
<div class="m2"><p>به سینه داغی ازین لاله زار نیست مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز خارخار تمنا بریده ام پیوند</p></div>
<div class="m2"><p>دل از تردد خاطر فگار نیست مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به دست و دوش نسیم است رهنوردی من</p></div>
<div class="m2"><p>وگرنه برگ سفر چون غبار نیست مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو آب آینه ام برقرار خود دایم</p></div>
<div class="m2"><p>ز موج حادثه دل بی قرار نیست مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکی است در نظر من بلند و پست جهان</p></div>
<div class="m2"><p>ز هیچ مرتبه ای فخر و عار نیست مرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در امید برآورده ام به گل صائب</p></div>
<div class="m2"><p>دو چشم در گرو انتظار نیست مرا</p></div></div>