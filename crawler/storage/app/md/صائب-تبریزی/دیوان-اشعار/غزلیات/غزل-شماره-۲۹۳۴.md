---
title: >-
    غزل شمارهٔ ۲۹۳۴
---
# غزل شمارهٔ ۲۹۳۴

<div class="b" id="bn1"><div class="m1"><p>دل رنگین لباسان تیرگی را در کمین دارد</p></div>
<div class="m2"><p>حنای دست زنگی هند را در آستین دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشو گستاخ کان لب خنده های شکرین دارد</p></div>
<div class="m2"><p>که زهر از گفتگوی تلخ هم زیر نگین دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زشرم ابروی او پیوسته چینی بر جبین دارد</p></div>
<div class="m2"><p>وگرنه خنده گل غنچه اش در آستین دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زرنگ می بود دلهای غافل را سیه مستی</p></div>
<div class="m2"><p>حنای دست زنگی هند را در آستین دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گرد او رسیدن نیست کار هر سبک جولان</p></div>
<div class="m2"><p>زتوسنها که عذر لنگ او در زیر زین دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غم و شادی درین میخانه می جوشد به یکدیگر</p></div>
<div class="m2"><p>صراحی خنده را با گریه در یک آستین دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زرنگ آمیزی دولت شود غافل سیه دلتر</p></div>
<div class="m2"><p>حنای دست زنگی هند را در آستین دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چرا ترسد زچشم بد، که روی آتشین او</p></div>
<div class="m2"><p>سپند خانه زاد از خالهای عنبرین دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مشو ایمن به نرمی از زبان خصم بدگوهر</p></div>
<div class="m2"><p>که تیر شمع از موم است و پیکان آتشین دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غباری نیست بر خاطر زعزلت پاک گوهر را</p></div>
<div class="m2"><p>که گنج آسایش روی زمین زیرزمین دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به ریزش دست را سرپنجه خورشید تابان کن</p></div>
<div class="m2"><p>کز احسان چون تهی شد دست حکم آستین دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نشد چون نرم از گفتار من آن سنگدل صائب</p></div>
<div class="m2"><p>چه حاصل زین که کلک من زبان آتشین دارد؟</p></div></div>