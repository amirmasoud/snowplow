---
title: >-
    غزل شمارهٔ ۶۱۱۵
---
# غزل شمارهٔ ۶۱۱۵

<div class="b" id="bn1"><div class="m1"><p>سر نمی پیچد ز اشک لاله گون مژگان من</p></div>
<div class="m2"><p>پنجه با دریای آتش می زند مرجان من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سینه ای چون صبح می خواهد قبول داغ عشق</p></div>
<div class="m2"><p>در زمین پاک ریزد تخم را دهقان من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا شدم قانع ز نعمت ها به درد و داغ عشق</p></div>
<div class="m2"><p>گرم چون خورشید تابان است دایم نان من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می شود هر روز بند غفلت من بیشتر</p></div>
<div class="m2"><p>دانه زنجیر در خاک است در زندان من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چه از لب تشنگی یک مشت خاکستر شدم</p></div>
<div class="m2"><p>تازه رو دارد سفال خاک را ریحان من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می دهد از سنبلستان ریاض خلد یاد</p></div>
<div class="m2"><p>از سیه مستان معنی صفحه دیوان من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تازه رو بر می خورم با هر که خونم می خورد</p></div>
<div class="m2"><p>نیشتر را گل به دامان می کند شریان من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اختیار گریه بی اختیارم داده اند</p></div>
<div class="m2"><p>غیر مژگان یک سر مو نیست در فرمان من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حلقه بیرون در کام از نظربازی گرفت</p></div>
<div class="m2"><p>تا به کی محروم باشد دیده حیران من؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این جواب آن غزل صائب که گوید مولوی</p></div>
<div class="m2"><p>چون بنالم عطر گیرد عالم از ریحان من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بس که ترسیده است چشمم صائب از رخسار او</p></div>
<div class="m2"><p>برنمی آید نگه از سایه مژگان من</p></div></div>