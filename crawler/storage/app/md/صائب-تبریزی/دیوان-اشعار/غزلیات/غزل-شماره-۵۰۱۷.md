---
title: >-
    غزل شمارهٔ ۵۰۱۷
---
# غزل شمارهٔ ۵۰۱۷

<div class="b" id="bn1"><div class="m1"><p>رسیده است به جایی لطافت بدنش</p></div>
<div class="m2"><p>که از نسیم شود داغدار یاسمنش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر ز نکهت گل پیرهن کند در بر</p></div>
<div class="m2"><p>شگفت نیست که نیلوفری شود سمنش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سخن چو بال و پر طوطیان شود سرسبز</p></div>
<div class="m2"><p>ز آبداری لعل لب شکرشکنش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکوه حسن ازین بیشتر نمی‌باشد</p></div>
<div class="m2"><p>که از سپند نخیزد صدا در انجمنش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز اشک شمع توان نقل در گریبان ریخت</p></div>
<div class="m2"><p>به محفلی که بخندد لب شکرشکنش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به این فروغ ندارد یمن عقیقی یاد</p></div>
<div class="m2"><p>سهیل برگ خزان دیده‌ای است از چمنش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حلاوت لب ازین بیشتر نمی‌باشد</p></div>
<div class="m2"><p>که همچو نامه سربسته است هر سخنش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عبیر پیرهن چشم می‌کند یوسف</p></div>
<div class="m2"><p>اگر به مصر برد باد بوی پیرهنش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه لذتی است شنیدن نوای جان‌پرور</p></div>
<div class="m2"><p>ز مطربی که توان بوسه داد بر دهنش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز دام موج، نجات حباب ممکن نیست</p></div>
<div class="m2"><p>چگونه دل به در آید ز زلف پرشکنش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نشد گشایشی از راه گفتگو صائب</p></div>
<div class="m2"><p>مگر به خال توان یافت نقطه دهنش</p></div></div>