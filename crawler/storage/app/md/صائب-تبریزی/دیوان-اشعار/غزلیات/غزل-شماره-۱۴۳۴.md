---
title: >-
    غزل شمارهٔ ۱۴۳۴
---
# غزل شمارهٔ ۱۴۳۴

<div class="b" id="bn1"><div class="m1"><p>عشرت روی زمین در دل ویرانه ماست</p></div>
<div class="m2"><p>خلوت سینه پر آه، پریخانه ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشتی چرخ اگر باد مرادی دارد</p></div>
<div class="m2"><p>ناله بیخودی و نعره مستانه ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چه جز جذبه عشق است درین دامن دشت</p></div>
<div class="m2"><p>گر همه خضر بود، سبزه بیگانه ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دل سوخته ما به حقارت منگر</p></div>
<div class="m2"><p>که سویدای دل خاک، سیه خانه ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سیل وحشت کند از کلبه ما بی برگان</p></div>
<div class="m2"><p>جای رحم است به جغدی که به ویرانه ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روز محشر چه کند با دل پر شکوه ما؟</p></div>
<div class="m2"><p>که شب زلف تو کوتاه به افسانه ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نقش بال و پر ما، دام ره ما شده است</p></div>
<div class="m2"><p>هر کجا ریخت پروبال، پریخانه ماست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حسن در هیچ زمان این همه شاداب نبود</p></div>
<div class="m2"><p>گریه شادی این شمع ز پروانه ماست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کار چون در گره افتد ز خدا یاد کنیم</p></div>
<div class="m2"><p>عقده مشکل ما سبحه صد دانه ماست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر چه از سوختگانیم به ظاهر صائب</p></div>
<div class="m2"><p>مزرع سبز فلک در گره دانه ماست</p></div></div>