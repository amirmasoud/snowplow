---
title: >-
    غزل شمارهٔ ۲۰۰۵
---
# غزل شمارهٔ ۲۰۰۵

<div class="b" id="bn1"><div class="m1"><p>موج شراب و موجه آب بقا یکی است</p></div>
<div class="m2"><p>هر چند پرده هاست مخالف، نوا یکی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر موج ازین محیط اناالبحر می زند</p></div>
<div class="m2"><p>گر صد هزار دست برآید دعا یکی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواهی به کعبه رو کن و خواهی به سومنات</p></div>
<div class="m2"><p>از اختلاف راه چه غم، رهنما یکی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این ما و من نتیجه بیگانگی بود</p></div>
<div class="m2"><p>صد دل به یکدگر چو شود آشنا، یکی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کام هر که محو شود در رضای دوست</p></div>
<div class="m2"><p>با نیشکر حلاوت تیر قضا یکی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در چشم پاک بین نبود رسم امتیاز</p></div>
<div class="m2"><p>در آفتاب سایه شاه و گدا یکی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پروای سرد و گرم خزان و بهار نیست</p></div>
<div class="m2"><p>آن را که همچو سرو و صنوبر قبا یکی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی ساقی و شراب غم از دل نمی رود</p></div>
<div class="m2"><p>این درد را طبیب یکی و دوا یکی است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر چند نقش ما یک و از دیگران شش است</p></div>
<div class="m2"><p>نومید نیستیم ز بردن، خدا یکی است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دانند عاقلان که ظفر در رکاب کیست</p></div>
<div class="m2"><p>هر چند دانه بیعدد و آسیا یکی است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از حرف خود به تیغ نگردیم چون قلم</p></div>
<div class="m2"><p>هر چند دل دو نیم بود حرف ما یکی است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صائب شکایت از ستم یار چون کند؟</p></div>
<div class="m2"><p>هر جا که عشق هست جفا و وفا یکی است</p></div></div>