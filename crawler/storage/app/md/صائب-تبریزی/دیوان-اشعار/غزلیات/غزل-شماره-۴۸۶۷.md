---
title: >-
    غزل شمارهٔ ۴۸۶۷
---
# غزل شمارهٔ ۴۸۶۷

<div class="b" id="bn1"><div class="m1"><p>پیش میخواران سبک چون پنبه مینا مباش</p></div>
<div class="m2"><p>از سبکساری چو کف سیلی خور دریا مباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دختر زرکیست تا مردان زبون او شوند؟</p></div>
<div class="m2"><p>بیش ازین مغلوب این معشوقه رسوا مباش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از محیط می برون آور گلیم خویش را</p></div>
<div class="m2"><p>بیش ازین چون موج بی لنگر درین دریا مباش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طاق نسیان انتظار شیشه می می کشد</p></div>
<div class="m2"><p>بیش ازین شیدایی این شوخی نارعنا مباش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از رگ گردن سر مینا به خون غلطیده است</p></div>
<div class="m2"><p>چون قدح سربر خط تسلیم نه، مینا مباش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خون دل از انتظارت خون خود را می خورد</p></div>
<div class="m2"><p>بیش ازین آلوده کیفیت صهبا مباش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مغز گفتارست خاموشی، ازان روبی صداست</p></div>
<div class="m2"><p>نیستی طبل تهی، آبستن غوغا مباش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیده روشندلان از انتظارت شد سفید</p></div>
<div class="m2"><p>چون شرر زین بیشتر در سینه خارا مباش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا نظر گردانده ای، گلها ورق گردانده اند</p></div>
<div class="m2"><p>زینهار ایمن ز نیرنگ چمن پیرا مباش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیده از روی عرقناک سمن رویان بپوش</p></div>
<div class="m2"><p>بیش ازین در رهگذار سیل بی پروا مباش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فیض خورشید بلند اختر به عریانان رسد</p></div>
<div class="m2"><p>در حجاب رخت صوف و جامه دیبا مباش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حاصل بیهوده گردیها غبار خاطرست</p></div>
<div class="m2"><p>از تردد گردباد دامن صحرا مباش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خاک از همراهی سیلاب شد دریا نشین</p></div>
<div class="m2"><p>در دل این خاکدان بی چشم خونپالا مباش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سایبانی بهر خورشید قیامت فکر من</p></div>
<div class="m2"><p>غافل از بی سایگان درموسم گرما مباش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>باده صافی به مغز هوشمندان می دود</p></div>
<div class="m2"><p>در خرابات مغان درد ته مینا مباش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فکر امروز ترا نوعی که باید کرده اند</p></div>
<div class="m2"><p>ای ستمگر غافل از اندیشه فردا مباش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نیستی مرد مصاف تیرباران سؤال</p></div>
<div class="m2"><p>تابه نادانی توان گشتن علم، دانا مباش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تقویت کن چون حکیمان عقل دور اندیش را</p></div>
<div class="m2"><p>دشمن هوش و خرد چون نشأه صهبا مباش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همدمی چون ذکر حق درپرده دل حاضرست</p></div>
<div class="m2"><p>خلوتی چون رو دهد از مردمان، تنها مباش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گوشه عزلت ترا با شمع می جوید ز خلق</p></div>
<div class="m2"><p>بیش ازین صائب درین هنگامه غوغا مباش</p></div></div>