---
title: >-
    غزل شمارهٔ ۳۴۱۱
---
# غزل شمارهٔ ۳۴۱۱

<div class="b" id="bn1"><div class="m1"><p>عشق در پای گلی رنگ وفا می‌ریزد</p></div>
<div class="m2"><p>فرصتش باد که بسیار به جا می‌ریزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان سفر کرده بستان خبری هست که گل</p></div>
<div class="m2"><p>زر خود را همه در پای صبا می‌ریزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می چنان دشمن شرم است که گر سایه تاک</p></div>
<div class="m2"><p>بر سر حسن فتد، رنگ حیا می‌ریزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر کف پای تو تا تهمت خونریزی بست</p></div>
<div class="m2"><p>هر که را دست دهد خون حنا می‌ریزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صائب از دیده خونبار کرم دارد یاد</p></div>
<div class="m2"><p>کآنچه دارد همه در پای گدا می‌ریزد</p></div></div>