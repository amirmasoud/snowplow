---
title: >-
    غزل شمارهٔ ۲۵۶۵
---
# غزل شمارهٔ ۲۵۶۵

<div class="b" id="bn1"><div class="m1"><p>خط غزال چشم را آهوی مشکین می‌کند</p></div>
<div class="m2"><p>چهره‌های ساده را بتخانه چین می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گلستانی که چشم بلبلان بیدار نیست</p></div>
<div class="m2"><p>پای خواب‌آلود کار دست گلچین می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست یک ساعت هوس را تاب خودداری فزون</p></div>
<div class="m2"><p>این ستمگر آفرین را زود نفرین می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر کند در دادن تشریف، شیرین کوتهی</p></div>
<div class="m2"><p>تیشه را از خون خود فرهاد رنگین می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌توان دیدن ز کشتی اضطراب بحر را</p></div>
<div class="m2"><p>حسن طوفان بیشتر در خانه زین می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکوه کردن از حیات تلخ، کافر نعمتی است</p></div>
<div class="m2"><p>خواب را شیرینی افسانه سنگین می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سینه شیرین کلامان در غبار غم خوش است</p></div>
<div class="m2"><p>طوطیان را صافی آیینه خودبین می‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می‌کشد در خاکدان جسم، خواری جان پاک</p></div>
<div class="m2"><p>باده تا در خم بود از خشت بالین می‌کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این نگاه آشنارویی که من دیدم از او</p></div>
<div class="m2"><p>زود صائب خلق را بیگانه از دین می‌کند</p></div></div>