---
title: >-
    غزل شمارهٔ ۲۹۵۷
---
# غزل شمارهٔ ۲۹۵۷

<div class="b" id="bn1"><div class="m1"><p>نظر عاشق به خط زان روی انور برنمی دارد</p></div>
<div class="m2"><p>به دود تلخ از آتش دل سمندر برنمی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بشو از صبر و طاقت دست اگر از عشقبازانی</p></div>
<div class="m2"><p>که بحر بیکران عشق لنگر برنمی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه گل چیند زگلریزان انجم کوته اندیشی</p></div>
<div class="m2"><p>که پهلو از گرانخوابی زبستر برنمی دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سفر کن از وطن گر آرزوی پختگی داری</p></div>
<div class="m2"><p>که جوش بحر خامی را زعنبر برنمی دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نهند از تنگدستی خاکیان سر بر خط فرمان</p></div>
<div class="m2"><p>سبو چون شد تهی از پای خم سر برنمی دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نگردد جمع با رنگین لباسی زیرپا دیدن</p></div>
<div class="m2"><p>که طاوس خودآرا چشم از پر برنمی دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل خود را به صد امید کردم چاک، ازین غافل</p></div>
<div class="m2"><p>که بار شانه آن زلف معنبر برنمی دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا در پیچ و تاب رشک دارد طوطیی صائب</p></div>
<div class="m2"><p>که از شیرین کلامی ناز شکر برنمی دارد</p></div></div>