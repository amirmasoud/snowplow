---
title: >-
    غزل شمارهٔ ۵۷۴
---
# غزل شمارهٔ ۵۷۴

<div class="b" id="bn1"><div class="m1"><p>کمال حسن کجا، دیده پر آب کجا؟</p></div>
<div class="m2"><p>شکوه بحر کجا، خیمه حباب کجا؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا که جلوه هر ذره است رطل گران</p></div>
<div class="m2"><p>کجاست حوصله جام آفتاب، کجا؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمانده است ز دل جز غبار افسوسی</p></div>
<div class="m2"><p>به این خرابه فتد نور ماهتاب کجا؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گذشته است ترا ز آفتاب پایه حسن</p></div>
<div class="m2"><p>هلال عید شود با تو همرکاب کجا؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جستجوی تو گرد از جهان برآوردم</p></div>
<div class="m2"><p>دگر کجا روم ای خانمان خراب، کجا؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا که نعره مستانه بی قرار نکرد</p></div>
<div class="m2"><p>رسد به داد دلم نغمه رباب کجا؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرفتم این که رسد نوبت سؤال به من</p></div>
<div class="m2"><p>دماغ حرف کجا، قدرت جواب کجا؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز بس که گرم تماشای گلرخان گشتم</p></div>
<div class="m2"><p>نیافتم که کجا شد دل من آب کجا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز برگ، نکهت گل بیش می شود رسوا</p></div>
<div class="m2"><p>ترا نهفته کند پرده حجاب کجا؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میان سوخته و خام فرق بسیارست</p></div>
<div class="m2"><p>سرشک تاک کجا، گریه کباب کجا؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرفته است جهان را غبار بی دردی</p></div>
<div class="m2"><p>کجا رویم ازین عالم خراب، کجا؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنین که آب برآورده است خانه چشم</p></div>
<div class="m2"><p>بساط خود فکند پرده های خواب کجا؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فروغ حسن جهانگیر او کجاست که نیست؟</p></div>
<div class="m2"><p>ز خویش می روی ای دل به این شتاب کجا؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نظر به چشمه حیوان نمی کنم صائب</p></div>
<div class="m2"><p>مرا ز راه برد جلوه سراب کجا؟</p></div></div>