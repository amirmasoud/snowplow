---
title: >-
    غزل شمارهٔ ۱۳۲۶
---
# غزل شمارهٔ ۱۳۲۶

<div class="b" id="bn1"><div class="m1"><p>شورش سودای ما افلاک را معمور داشت</p></div>
<div class="m2"><p>پر نمک بود این نمکدان تا سر ما شور داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در شکست من ندارد چرخ سنگین دل گناه</p></div>
<div class="m2"><p>در بغل مینای من سنگ از می پر زور داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بار منت بر دل نازک گرانی می کند</p></div>
<div class="m2"><p>زخم خود را گل ز بوی خویشتن ناسور داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برنیاید از لبم در فقر، آواز سؤال</p></div>
<div class="m2"><p>کاسه چوبینم شکوه افسر فغفور داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می تواند داشت طوفان را مقید در تنور</p></div>
<div class="m2"><p>سینه هر کس که راز عشق را مستور داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خال دیگر بر جمال پادشاهی می فزود</p></div>
<div class="m2"><p>گر سلیمان گوشه چشمی به حال مور داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست جای لاف و دعوی راه باریک ادب</p></div>
<div class="m2"><p>عشق چوب دار ازان پیش ره منصور داشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از فلک هرگز غباری بر دل صافم نبود</p></div>
<div class="m2"><p>زنگ، طوطی بود تا آیینه من نور داشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دور گردی لذتی دارد که دل در بزم وصل</p></div>
<div class="m2"><p>با کمال قرب، حسرت بر نگاه دور داشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هیچ کس می باید از صائب نباشد پیشتر</p></div>
<div class="m2"><p>خدمت دیرینه را خواهی اگر منظور داشت</p></div></div>