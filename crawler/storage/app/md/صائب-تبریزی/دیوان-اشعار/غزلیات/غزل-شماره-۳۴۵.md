---
title: >-
    غزل شمارهٔ ۳۴۵
---
# غزل شمارهٔ ۳۴۵

<div class="b" id="bn1"><div class="m1"><p>دهن بستن ز آفت‌ها نگهبان است دل‌ها را</p></div>
<div class="m2"><p>لب خاموش دیوار گلستان است دل‌ها را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به ظاهر گر ز داغ آتشین دارند دوزخ‌ها</p></div>
<div class="m2"><p>بهشت جاودان در پرده پنهان است دل‌ها را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قناعت کن به لوح ساده چون طفلان ازین مکتب</p></div>
<div class="m2"><p>که نقش یوسفی خواب پریشان است دل‌ها را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خودداری درون دیده مورند زندانی</p></div>
<div class="m2"><p>جهان بی‌خودی ملک سلیمان است دل‌ها را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مکن اندیشه از زخم زبان گر بینشی داری</p></div>
<div class="m2"><p>که هر زخم نمایان مد احسان است دل‌ها را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمی‌دانند از کودک‌مزاجی کوته‌اندیشان</p></div>
<div class="m2"><p>که تلخی‌های عالم شکرستان است دل‌ها را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به زور عشق چون گل چاک کن پیراهن تن را</p></div>
<div class="m2"><p>که صبح عید از چاک گریبان است دل‌ها را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نباشد در دل مرغ قفس جز فکر آزادی</p></div>
<div class="m2"><p>کجا اندیشه آب و غم نان است دل‌ها را؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر چون غنچه نشکفته دلگیرند درظاهر</p></div>
<div class="m2"><p>چو گل در پرده چندین روی خندان است دل‌ها را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز بی‌تابی دل سیماب شد آسوده چون مرکز</p></div>
<div class="m2"><p>همان بی‌طاقتی گهواره‌جنبان است دل‌ها را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نمی‌دانم کدامین غنچه‌لب در پرده می‌خندد</p></div>
<div class="m2"><p>که شور صد قیامت در نمکدان است دل‌ها را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سر زلف که یارب آستین افشاند بر عالم؟</p></div>
<div class="m2"><p>که اسباب پریشانی به سامان است دل‌ها را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کدامین تیر را دیدی که باشد از دو سر خندان؟</p></div>
<div class="m2"><p>لب خندان گواه چشم گریان است دل‌ها را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز خواری شکوه‌ها دارند صائب کوته‌اندیشان</p></div>
<div class="m2"><p>نمی‌دانند عزت چاه و زندان است دل‌ها را</p></div></div>