---
title: >-
    غزل شمارهٔ ۱۹۹۴
---
# غزل شمارهٔ ۱۹۹۴

<div class="b" id="bn1"><div class="m1"><p>ما را ز عشق درد و غم بیکرانه است</p></div>
<div class="m2"><p>دریای بیکنار سراسر میانه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غفلت نگشت مانع تعجیل عمر را</p></div>
<div class="m2"><p>در خواب نیز قافله ما روانه است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غافل مشو ز پاس نفس تا حیات هست</p></div>
<div class="m2"><p>کاین شمع در کمین نسیم بهانه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد سنگ آب و سختی دل همچنان بجاست</p></div>
<div class="m2"><p>با آن که سالهاست درین شیشه خانه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چند روزگار کند شور بیشتر</p></div>
<div class="m2"><p>خواب گران غفلت ما را فسانه است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از حرف سخن، روی نتابند مبرمان</p></div>
<div class="m2"><p>مرغ حریص را گره دام دانه است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر توسن سبکرو پا در رکاب عمر</p></div>
<div class="m2"><p>موی سفید گشته ما تازیانه است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از دلبران طلب خبر دل رمیدگان</p></div>
<div class="m2"><p>چون تیر در کمان نبود بر نشانه است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در گوشه قفس مگر از دل برآورم</p></div>
<div class="m2"><p>این خارها که در دلم از آشیانه است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گردید از نظاره ما حس شوخ چشم</p></div>
<div class="m2"><p>بر آهوی رمیده، نگه تازیانه است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در خاکساری آن که چو صائب تمام شد</p></div>
<div class="m2"><p>بر صدر اگر قرار کند آستانه است</p></div></div>