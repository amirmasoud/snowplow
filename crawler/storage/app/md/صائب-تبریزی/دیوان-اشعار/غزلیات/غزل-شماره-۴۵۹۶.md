---
title: >-
    غزل شمارهٔ ۴۵۹۶
---
# غزل شمارهٔ ۴۵۹۶

<div class="b" id="bn1"><div class="m1"><p>بوسه ای در کار من کن زان لب همچون شکر</p></div>
<div class="m2"><p>تا به چشم شاه شیرین باشی ای صوفی پسر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پوست برتن ناتوانان را گرانی می کند</p></div>
<div class="m2"><p>بهله راکوتاه کن دست تعدی زان کمر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کاملان را ابجد بیرحمیی درکار نیست</p></div>
<div class="m2"><p>صید عاشق کن، مرو درخون چندین جانور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طره دستار خوبان کار شاهین می کند</p></div>
<div class="m2"><p>نیست حاجت دلربایان رابه شاهین دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوستان رااز نظر انداختن انصاف نیست</p></div>
<div class="m2"><p>ترک می باید که دشمن رانیارد درنظر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون غبارآلودبرگردی زصحرای شکار</p></div>
<div class="m2"><p>آب گرددهرکه اندازد به رخسارت نظر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه ازموی میان اودلی دارم دونیم</p></div>
<div class="m2"><p>دارم ازتیغش هلال عید قربان درنظر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گربه این تمکین گذاری پای برچشم رکاب</p></div>
<div class="m2"><p>خانه زین راتزلزل می کند زیروزبر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر ضعیفان ظلم کردن می کند دل راسیاه</p></div>
<div class="m2"><p>باز کن یک لحظه آن شمشیر کج رااز کمر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می رباید حلقه های دیده عشاق را</p></div>
<div class="m2"><p>چون سنان هر جا شود با قد رعناجلوه گر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بوی خون بیدار سازد فتنه خوابیده را</p></div>
<div class="m2"><p>چشم او از باده شد درخون عاشق گرمتر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از خرامی می کند زیروزبر آفاق را</p></div>
<div class="m2"><p>از نگاهی لشکری را می زند بریکدگر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گربه ظاهر سربه پیش افکنده است از شرم حسن</p></div>
<div class="m2"><p>تیغها دارد ز پرکاری نهان زیر سپر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رو به هر جانب که آرد، دلفتد بر روی دل</p></div>
<div class="m2"><p>هر طرف تازد،به جان گرد خیزد الحذر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر درین میخانه می خواهی شراب بی خمار</p></div>
<div class="m2"><p>نیست غیر از خون عاشق باده بی دردسر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نیست ممکن ترک من برفارسی دندان نهد</p></div>
<div class="m2"><p>گر ز قند فارسی سازم جهان راپرشکر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رحم کن ای سنگدل بر صائب شیرین سخن</p></div>
<div class="m2"><p>ورنه خواهد شکوه کردن پیش شاه دادگر</p></div></div>