---
title: >-
    غزل شمارهٔ ۵۴۰۹
---
# غزل شمارهٔ ۵۴۰۹

<div class="b" id="bn1"><div class="m1"><p>رو به هر صحرا که بااین شور چون مجنون کنم</p></div>
<div class="m2"><p>پایکوبان کوه را در دامن هامون کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاکساری دست من کوتاه دارد ورنه من</p></div>
<div class="m2"><p>می توانم خاکها د رکاسه گردون کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از ازل آورده با خود پختگی صهبای من</p></div>
<div class="m2"><p>نیستم نارس که جادر خم چو افلاطون کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خشکی سودای من ابر بهار عالم است</p></div>
<div class="m2"><p>هرکه زین دامان صحرا بگذرد مجنون کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلبلان را چون توانم مست در گلزار دید</p></div>
<div class="m2"><p>من که خواهم باغبان را از چمن بیرون کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من که می دانم سبکروحان عالم را ثقیل</p></div>
<div class="m2"><p>یک جهان بد هضم را بر خود گوارا چون کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من که نتوانم بر آوردن ز پا خاررهش</p></div>
<div class="m2"><p>خارخار عشق او را چون زدل بیرون کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از چه ناز سرو نارعنا کشم چون قمریان</p></div>
<div class="m2"><p>من که صائب می توانم مصرعی موزون کنم</p></div></div>