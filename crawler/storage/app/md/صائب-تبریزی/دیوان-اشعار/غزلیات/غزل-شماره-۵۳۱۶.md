---
title: >-
    غزل شمارهٔ ۵۳۱۶
---
# غزل شمارهٔ ۵۳۱۶

<div class="b" id="bn1"><div class="m1"><p>مومنی را می‌کند آزاد از قید فرنگ</p></div>
<div class="m2"><p>هرکه می‌سازد درین محفل ز خود بیگانه‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا به کی در خوردن دل روزگارم بگذرد</p></div>
<div class="m2"><p>چند چون پرگار باشد مرکز خود دانه‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از کتان صد پیرهن بنیاد من نازک‌تر است</p></div>
<div class="m2"><p>می‌کند مهتاب کار سیل در ویرانه‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در سر شوریده من عقل سودا می‌شود</p></div>
<div class="m2"><p>می‌کند گرد یتیمی درد را پیمانه‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کوه غم رطل گران طبع خرسند من است</p></div>
<div class="m2"><p>چون گهر در سنگ سیراب است دایم دانه‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق او کرد این چنین شوریده مغزم ورنه بود</p></div>
<div class="m2"><p>سرنوشت آسمان‌ها ابجد طفلانه‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خشکسال زهد نم درجوی من نگذاشته است</p></div>
<div class="m2"><p>تشنه یک های‌های گریه مستانه‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمع نازک‌دل غبارآلود غیرت می‌شود</p></div>
<div class="m2"><p>ورنه برمی‌آورد آتش ز خود پروانه‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر چراغی صائب از جا درنمی‌آرد مرا</p></div>
<div class="m2"><p>سینه بر شمع تجلی می‌زند پروانه‌ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کس نگردد از جنون گرد دل دیوانه‌ام</p></div>
<div class="m2"><p>چون کمان از زور خود دارد نگهبان خانه‌ام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شیر می‌بازد جگر از شورش سودای من</p></div>
<div class="m2"><p>حلقه از داغ جنون دارد در غمخانه‌ام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کیست مجنون تا نتواند هم ترازو شد به من</p></div>
<div class="m2"><p>می‌شمارد سنگ طفلان کوه را دیوانه‌ام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بارها از افسر خورشید سر دزدیده‌ام</p></div>
<div class="m2"><p>داغ دارد آسمان را همت مردانه‌ام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خانه پردازی مرا پیوسته در دل ساکن است</p></div>
<div class="m2"><p>سیل مار گنج گردیده است در ویرانه‌ام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در بنای صبر من غم رخنه نتواند فکند</p></div>
<div class="m2"><p>من نه آن تیغم که هر سنگی کند دندانه‌ام</p></div></div>