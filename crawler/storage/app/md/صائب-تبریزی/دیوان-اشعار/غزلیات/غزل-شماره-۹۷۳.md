---
title: >-
    غزل شمارهٔ ۹۷۳
---
# غزل شمارهٔ ۹۷۳

<div class="b" id="bn1"><div class="m1"><p>عشرت روی زمین در چرب نرمی مضمرست</p></div>
<div class="m2"><p>رشته هموار را بالین و بستر گوهرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می روند از جا سبک مغزان ز دنیای خسیس</p></div>
<div class="m2"><p>برگ کاهی کهربای حرص را بال و پرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نسوزد آرزو در دل نگردد سینه صاف</p></div>
<div class="m2"><p>سرمه بینایی آیینه از خاکسترست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زینت ظاهر کند محضر به خون خود درست</p></div>
<div class="m2"><p>حلقه فتراک طاوس خودآرا از پرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهر بر لب زن که چون منصور با این باطلان</p></div>
<div class="m2"><p>هر که گوید حرف حق بی پرده، دارش منبرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می خلد در دیده ها دستی که از ریزش تهی است</p></div>
<div class="m2"><p>خشک چون گردد رگ ابر بهاران نشترست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می گشاید هر که چون ناخن گره از کار خلق</p></div>
<div class="m2"><p>می کند نشو و نما هر چند تیغش بر سرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر چراغ ما که می میرد برای خامشی</p></div>
<div class="m2"><p>سایه دست حمایت آستین صرصرست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی خموشی در حریم قرب نتوان بار یافت</p></div>
<div class="m2"><p>حلقه را از هرزه نالی جای بیرون درست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیده بیدار، صائب می برد فیض از جهان</p></div>
<div class="m2"><p>هر چه مینا جمع می سازد برای ساغرست</p></div></div>