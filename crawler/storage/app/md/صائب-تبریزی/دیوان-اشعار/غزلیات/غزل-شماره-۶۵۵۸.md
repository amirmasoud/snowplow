---
title: >-
    غزل شمارهٔ ۶۵۵۸
---
# غزل شمارهٔ ۶۵۵۸

<div class="b" id="bn1"><div class="m1"><p>می کند دل را سیه چندان که خواب صبحگاه</p></div>
<div class="m2"><p>می نماید آنقدر روشن شراب صبحگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقد انجم را به یک جام صبوحی می دهد</p></div>
<div class="m2"><p>خوب می داند فلک قدر شراب صبحگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چهره خورشید اگر طالع نگردد گو مگرد</p></div>
<div class="m2"><p>لذت دیدار می بخشد نقاب صبحگاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چهره ای می باید از خورشید تابان شسته تر</p></div>
<div class="m2"><p>جای هر ناشسته رو نبود جناب صبحگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر دمی کز روی صدق از مشرق دل سرزند</p></div>
<div class="m2"><p>هست پیش قدردانان در حساب صبحگاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غفلت پیران جاهل را سبب در کار نیست</p></div>
<div class="m2"><p>فارغ است از منت افسانه خواب صبحگاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیش ازان کز چشم خواب آلودگان نورش رود</p></div>
<div class="m2"><p>آب ده چشم از رخ چون آفتاب صبحگاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از شفق تا خون نگردیده است شیر صاف او</p></div>
<div class="m2"><p>شیر مست فیض شو از فتح باب صبحگاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل سیاهان می کشند از سینه صافان انفعال</p></div>
<div class="m2"><p>چهره شب شد عرق ریز از حجاب صبحگاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرچه می گویند باران نیست در ابر سفید</p></div>
<div class="m2"><p>فیض می بارد ز سیمای سحاب صبحگاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می شود چون چهره خورشید زرین چهره اش</p></div>
<div class="m2"><p>هر که رو از صدق مالد بر رکاب صبحگاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می دهد یاد از سبک جولانی دوران عیش</p></div>
<div class="m2"><p>عارفان را خنده پا در رکاب صبحگاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تیغ سیراب تو در آغوش زخم عاشقان</p></div>
<div class="m2"><p>هست در کام خمارآلود، آب صبحگاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر بیاض گردن مینای می افتد به دست</p></div>
<div class="m2"><p>می توان شد در دل شب کامیاب صبحگاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چشم اگر داری که چون خورشید روشندل شوی</p></div>
<div class="m2"><p>همتی صائب طلب کن از جناب صبحگاه</p></div></div>