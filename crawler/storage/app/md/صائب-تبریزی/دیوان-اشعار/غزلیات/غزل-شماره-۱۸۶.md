---
title: >-
    غزل شمارهٔ ۱۸۶
---
# غزل شمارهٔ ۱۸۶

<div class="b" id="bn1"><div class="m1"><p>چشم مستش از نگاهی کرد سودایی مرا</p></div>
<div class="m2"><p>کشتی از یک قطره می، گردید دریایی مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم باز از پیش پا دیدن حجابم گشته است</p></div>
<div class="m2"><p>از نظر بستن یکی صد گشت بینایی مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نرمتر صد پیرهن از خواب مخمل گشته است</p></div>
<div class="m2"><p>خار صحرای ملامت از سبکپایی مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خانه داری داشت بر من دستگاه عیش تنگ</p></div>
<div class="m2"><p>مالک روی زمین گرداند بی جایی مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آه حسرت می کشم چون سرو بهر بندگی</p></div>
<div class="m2"><p>تا فکند آزادگی در قید رعنایی مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محنت پیری نمی بود این قدر ناخوشگوار</p></div>
<div class="m2"><p>محو اگر می شد ز خاطر یاد برنایی مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرغ بی بال و پری را می کند بی آشیان</p></div>
<div class="m2"><p>هر که می آرد برون از کنج تنهایی مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برندارم چون قلم صائب سر از پای سخن</p></div>
<div class="m2"><p>گر چه مد عمر کوته شد ز گویایی مرا</p></div></div>