---
title: >-
    غزل شمارهٔ ۲۵۵
---
# غزل شمارهٔ ۲۵۵

<div class="b" id="bn1"><div class="m1"><p>تا خرام قامت او برد از سر هوش ما</p></div>
<div class="m2"><p>پشت بر دیوار چون محراب ماند آغوش ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آمدی ای عشق و آتش در صلاح ما زدی</p></div>
<div class="m2"><p>خوب کردی، پینه ای بود این ردا بر دوش ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جوهر ما را می لعلی نمایان می کند</p></div>
<div class="m2"><p>می شود از باده افزون آب و رنگ هوش ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جام ما در پرده دارد نغمه های جانگداز</p></div>
<div class="m2"><p>دست خود کوتاه دارید از لب خاموش ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نعره ما می کند مهر خموشی را سپند</p></div>
<div class="m2"><p>خشت خم را در فلاخن می گذارد جوش ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پشتبانی چون سبو داریم در دیر مغان</p></div>
<div class="m2"><p>گو مزن دست نوازش آسمان بر دوش ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیستی صائب حریف داغ های سینه سوز</p></div>
<div class="m2"><p>دست خود کوتاه دار از سینه پرجوش ما</p></div></div>