---
title: >-
    غزل شمارهٔ ۶۲۰۰
---
# غزل شمارهٔ ۶۲۰۰

<div class="b" id="bn1"><div class="m1"><p>ز دل مجموعه ای هر روز املا می توان کردن</p></div>
<div class="m2"><p>ازین یک قطره خون صد نامه انشا می توان کردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر روی دلی از کارفرما در میان باشد</p></div>
<div class="m2"><p>به ناخن سنگ را آیینه سیما می توان کردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگردد لنگر تمکین حریف ناله عاشق</p></div>
<div class="m2"><p>به هویی بیستون را دشت پیما می توان کردن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گریزد لشکر خواب گران از قطره آبی</p></div>
<div class="m2"><p>به یک پیمانه از سر عقل را وا می توان کردن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگیری گر به مرهم رخنه غمخانه دل را</p></div>
<div class="m2"><p>ازین روزن دو عالم را تماشا می توان کردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر دریوزه همت کنی از شوق بی پروا</p></div>
<div class="m2"><p>سفر در آب و آتش بی محابا می توان کردن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خط پاکی ز سیلاب فنا دارد وجود ما</p></div>
<div class="m2"><p>چه از ما می توان بردن، چه با ما می توان کردن؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر بر دل گذاری همچو کشتی بار مردم را</p></div>
<div class="m2"><p>به آسانی سفر بر روی دریا می توان کردن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر چه مزد کار خود نمی دانم دو عالم را</p></div>
<div class="m2"><p>به انصافی مرا از خود تسلی می توان کردن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در آن وادی که من طرح شکار افکنده ام صائب</p></div>
<div class="m2"><p>به دام عنکبوتان صید عنقا می توان کردن</p></div></div>