---
title: >-
    غزل شمارهٔ ۵۸۳۲
---
# غزل شمارهٔ ۵۸۳۲

<div class="b" id="bn1"><div class="m1"><p>دست طمع ز مایده چرخ شسته ایم</p></div>
<div class="m2"><p>از جان سخت خود به شکم سنگ بسته ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دامان بادبان توکل گرفته ایم</p></div>
<div class="m2"><p>در زورق حباب به لنگر نشسته ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برگ خزان رسیده گلزار عالمیم</p></div>
<div class="m2"><p>پیوند شاخسار اقامت گسسته ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>موقوف ترکتاز نسیمی است گرد ما</p></div>
<div class="m2"><p>بر روی برگ گل به امانت نشسته ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون قطره سر به دامن دریا نهاده ایم</p></div>
<div class="m2"><p>وز موجه تردد خاطر نرسته ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در بند یک اشاره موج است این طلسم</p></div>
<div class="m2"><p>دل چون حباب بر نفس خود نبسته ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کیفیت از عبادت ما می چکد به خاک</p></div>
<div class="m2"><p>در آب تلخ دامن سجاده شسته ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فردا به روی مصحف دل چون نگه کنیم؟</p></div>
<div class="m2"><p>شیرازه اش به رشته زنار بسته ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مردم چرا به خرمن ما اوفتاده اند ؟</p></div>
<div class="m2"><p>هرگز به سهو خاطر موری نخسته ایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مکتوب خویش از الف آه کرده ایم</p></div>
<div class="m2"><p>کاغذ دریده ایم و قلم را شکسته ایم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب به عیب خویش فتاده است کار ما</p></div>
<div class="m2"><p>زان رو زبان زنیک و بد خلق بسته ایم</p></div></div>