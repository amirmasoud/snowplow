---
title: >-
    غزل شمارهٔ ۱۷۹۴
---
# غزل شمارهٔ ۱۷۹۴

<div class="b" id="bn1"><div class="m1"><p>ز زلف او دل عشاق را محابا نیست</p></div>
<div class="m2"><p>کبوتران حرم را ز دام پروا نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکن سپند مرا دور از حریم وصال</p></div>
<div class="m2"><p>که بیقراری من خالی از تماشا نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر ز اهل دلی ذره را حقیر مدان</p></div>
<div class="m2"><p>که هیچ نقطه سهوی کم از سویدا نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خود جداشدگان پرس درد تنهایی</p></div>
<div class="m2"><p>که هر که دور ز مردم فتاده تنها نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لب سؤال صدف بی حجاب می گوید</p></div>
<div class="m2"><p>که هیچ آب مروت به چشم دریا نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمی توان به زبان حرف وا کشید از من</p></div>
<div class="m2"><p>که روی حرف مرا جز به چشم گویا نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>معاشران سبکروح بوی پیرهنند</p></div>
<div class="m2"><p>به دوش چرخ گران هیکل مسیحا نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سپر فکند فلک پیش آه من صائب</p></div>
<div class="m2"><p>علاج خصم زبردست جز مدارا نیست</p></div></div>