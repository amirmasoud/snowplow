---
title: >-
    غزل شمارهٔ ۶۶۹۲
---
# غزل شمارهٔ ۶۶۹۲

<div class="b" id="bn1"><div class="m1"><p>تابش برق و حیات مختصر باشد یکی</p></div>
<div class="m2"><p>جلوه آغاز و انجام شرر باشد یکی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تلخی و شیرینی عالم به هم آمیخته است</p></div>
<div class="m2"><p>نوش و نیش این جهان مختصر باشد یکی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در قناعت می شود هر ناگواری خوشگوار</p></div>
<div class="m2"><p>خاک پیش مور قانع با شکر باشد یکی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دولت بیدار کوته دیدگان روزگار</p></div>
<div class="m2"><p>با گرانخوابی به میزان نظر باشد یکی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاکیان بی بصیرت بر زمین چسبیده اند</p></div>
<div class="m2"><p>پیش طفلان مهره گل با گهر باشد یکی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با توکل فکر زاد راه کافر نعمتی است</p></div>
<div class="m2"><p>توشه و زنار ما را بر کمر باشد یکی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر چه از اندازه بیرون رفت دل را می گزد</p></div>
<div class="m2"><p>خون فاسد در بدن با نیشتر باشد یکی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از گرانباری است کشتی را ز هر موجی خطر</p></div>
<div class="m2"><p>ورنه بر کف ساحل و موج خطر باشد یکی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست از نازک خیالی قسمتم جز پیچ و تاب</p></div>
<div class="m2"><p>رشته جان من و موی کمر باشد یکی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آخرت را جمع نتوان کرد با دنیای دون</p></div>
<div class="m2"><p>خوشه را نشنیده ای صائب که سر باشد یکی؟</p></div></div>