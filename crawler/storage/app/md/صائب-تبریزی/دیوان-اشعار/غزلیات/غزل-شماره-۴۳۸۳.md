---
title: >-
    غزل شمارهٔ ۴۳۸۳
---
# غزل شمارهٔ ۴۳۸۳

<div class="b" id="bn1"><div class="m1"><p>سرگرم تو با کشمکش دار نسازد</p></div>
<div class="m2"><p>این نقطه سرگشته به پرگارنسازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرده است دل زاهد دمسرد ز تزویر</p></div>
<div class="m2"><p>چون برسرخودگنبددستارنسازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زنهار مکن خوارعزیزان جهان را</p></div>
<div class="m2"><p>تا چرخ عزیزان ترا خوارنسازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آیینه اقبال به کس رو ننماید</p></div>
<div class="m2"><p>تا سرمه ز خاکستر ادبارنسازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کج می نگرد عشق به بال وپرجبریل</p></div>
<div class="m2"><p>این شعله سرکش به خس وخارنسازد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طوطی شو وآنگاه ببین روی دل از ما</p></div>
<div class="m2"><p>آیینه به هر بیهده گفتار نسازد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر رشته پیوند بودتاب موافق</p></div>
<div class="m2"><p>چون موی میان تو به زنارنسازد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با سینه پرداغ نسازد دل صائب</p></div>
<div class="m2"><p>رسم است که پروانه به گلزارنسازد</p></div></div>