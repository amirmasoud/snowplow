---
title: >-
    غزل شمارهٔ ۴۸۴۴
---
# غزل شمارهٔ ۴۸۴۴

<div class="b" id="bn1"><div class="m1"><p>داشت امروز رخ یار حجابی که مپرس</p></div>
<div class="m2"><p>زد به روی دل مدهوش گلابی که مپرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر از شرم و حیا بوددوچشمش مخمور</p></div>
<div class="m2"><p>از عرق داشت رخش عالم آبی که مپرس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خنده می کرد، ولی داشت ز پر کاری حسن</p></div>
<div class="m2"><p>درشکرخنده نهان زهر عتابی که مپرس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه می زدنگه شوخ به بازی در صلح</p></div>
<div class="m2"><p>داشت بابوسه دهانش شکرابی که مپرس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داشت از سنگدلی هر مژه خونخوارش</p></div>
<div class="m2"><p>پیش دست از دل صدپاره کبابی که مپرس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر سؤالی که ازو خیرگی شوق نمود</p></div>
<div class="m2"><p>داد در زیر لب خویش جوابی که مپرس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه بی پرده برون آمده بود ازخلوت</p></div>
<div class="m2"><p>داشت از حیرت دیدار نقابی که مپرس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناز هر چند به دامان نگه می آویخت</p></div>
<div class="m2"><p>می دوید از پی دلها به شتابی که مپرس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با خط و زلف خود از رهگذر دلها داشت</p></div>
<div class="m2"><p>مو شکافانه حسابی و کتابی که مپرس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از خیال لب میگون خراباتی خود</p></div>
<div class="m2"><p>داشت در ساغر اندیشه شرابی که مپرس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با خیالش دل سودایی صائب همه شب</p></div>
<div class="m2"><p>بود مشغول سؤالی و جوابی که مپرس</p></div></div>