---
title: >-
    غزل شمارهٔ ۶۴۲۷
---
# غزل شمارهٔ ۶۴۲۷

<div class="b" id="bn1"><div class="m1"><p>از زلف پر از پیچ و خم یار حذر کن</p></div>
<div class="m2"><p>هر چند فسونسازی ازین مار حذر کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حلقه گرداب ز دریاست خطر بیش</p></div>
<div class="m2"><p>از مردم کم حوصله زنهار حذر کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر سنگ زن این آینه عیب نما را</p></div>
<div class="m2"><p>در بزم می از مردم هشیار حذر کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سهل است اگر از سنگ محابا ننمایی</p></div>
<div class="m2"><p>از هم گهر ای گوهر شهوار حذر کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آلوده مگردان به زنا دامن عصمت</p></div>
<div class="m2"><p>از صحبت بی فایده زنهار حذر کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سنگ ره توفیق بود پای گرانخواب</p></div>
<div class="m2"><p>صائب ز رفیقان گرانبار حذر کن</p></div></div>