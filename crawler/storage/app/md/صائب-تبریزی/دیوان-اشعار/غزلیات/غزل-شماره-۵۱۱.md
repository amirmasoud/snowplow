---
title: >-
    غزل شمارهٔ ۵۱۱
---
# غزل شمارهٔ ۵۱۱

<div class="b" id="bn1"><div class="m1"><p>عشق سازد ز هوس پاک دل آدم را</p></div>
<div class="m2"><p>دزد چون شحنه شود امن کند عالم را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آب جان را چو گهر در گره تن مگذار</p></div>
<div class="m2"><p>چون گل و لاله به خورشید رسان شبنم را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در وصالیم و همان خون جگر می نوشیم</p></div>
<div class="m2"><p>تلخی از دل نبرد قرب حرم زمزم را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عالم از جای به تعظیم کلامش خیزد</p></div>
<div class="m2"><p>هر که چون صبح برآرد به تأمل دم را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رم آهوی حرم پای گرانخواب شود</p></div>
<div class="m2"><p>چون به دوش افکنی آن زلف خم اندر خم را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قفس شیر نگشته است نیستان هرگز</p></div>
<div class="m2"><p>عشق آن نیست که بر هم نزند عالم را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شور و غوغا نبود در سفر اهل نظر</p></div>
<div class="m2"><p>نیست آواز درا قافله شبنم را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زینت مردم آزاده بود بی برگی</p></div>
<div class="m2"><p>محضر جود بود دست تهی حاتم را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه خبر از دل آواره ما خواهد داشت؟</p></div>
<div class="m2"><p>مست نازی که ندارد خبر عالم را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صائب از شعله آه تو، که روشن بادا!</p></div>
<div class="m2"><p>می توان خواند شب تار خط درهم را</p></div></div>