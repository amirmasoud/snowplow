---
title: >-
    غزل شمارهٔ ۳۹۳۳
---
# غزل شمارهٔ ۳۹۳۳

<div class="b" id="bn1"><div class="m1"><p>اجل چه کار به جانهای با کمال کند</p></div>
<div class="m2"><p>چرا ملاحظه خورشید از زوال کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز گل برید چو شبنم به آفتاب رسید</p></div>
<div class="m2"><p>دگر چرا کسی اندیشه مآل کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز این که رخنه آزادیش فروبندد</p></div>
<div class="m2"><p>قفس چه رحم به مرغ شکسته بال کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شده است عام چنان حرص در غنی وفقیر</p></div>
<div class="m2"><p>که بحر با همه گوهر به کف سؤال کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چهار فصل بهارست عندلیبی را</p></div>
<div class="m2"><p>که برگ عیش سرانجام زیر بال کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه حاصل است ز عمر دراز نادان را</p></div>
<div class="m2"><p>سیاستی است که کرکس هزار سال کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گلی که مست درآید به باغ می باید</p></div>
<div class="m2"><p>که خون خود به تماشاییان حلال کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکایت از فلک بی وجود مردی نیست</p></div>
<div class="m2"><p>چرا به سایه خود آدمی جدال کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ظهور دوزخ ازان شد که عاصی بی شرم</p></div>
<div class="m2"><p>تهیه عرقی بحر انفعال کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز پرده پوش کند التماس پرده دری</p></div>
<div class="m2"><p>کسی که کشف توقع ز اهل حال کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نشد ز عشق شود چرب نرم زاهد خشک</p></div>
<div class="m2"><p>شراب لعل چه تأثیر در سفال کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تأمل آینه پرداز فکر ناصاف است</p></div>
<div class="m2"><p>ستادن آب گل آلود را زلال کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خوشا کسی که چو صائب ز صاحبان سخن</p></div>
<div class="m2"><p>تتبع سخن میرزا جلال کند</p></div></div>