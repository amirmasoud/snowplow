---
title: >-
    غزل شمارهٔ ۵۹۳۲
---
# غزل شمارهٔ ۵۹۳۲

<div class="b" id="bn1"><div class="m1"><p>ما طالع جمعیت اسباب نداریم</p></div>
<div class="m2"><p>روزی که هوا هست می ناب نداریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی دل ما در حرم کعبه بود فرش</p></div>
<div class="m2"><p>در ظاهر اگر روی به محراب نداریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فریاد که از گردش بیهوده درین بحر</p></div>
<div class="m2"><p>جز خار و خسی چند چو گرداب نداریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آه قدر انداز به فرمان دل ماست</p></div>
<div class="m2"><p>در قبضه اگر تیغ سیه تاب نداریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قانع به هواداری دریا چو حبابیم</p></div>
<div class="m2"><p>ما حوصله گوهر سیراب نداریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون ماهی لب بسته درین بحر پر آشوب</p></div>
<div class="m2"><p>اندیشه ز گیرایی قلاب نداریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کفران بود از فتنه خوابیده شکایت</p></div>
<div class="m2"><p>ما شکوه ای از بخت گرانخواب نداریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن بلبل مستیم درین باغچه صائب</p></div>
<div class="m2"><p>کز شور محبت خبر از خواب نداریم</p></div></div>