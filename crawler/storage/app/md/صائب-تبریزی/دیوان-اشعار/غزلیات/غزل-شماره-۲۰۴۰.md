---
title: >-
    غزل شمارهٔ ۲۰۴۰
---
# غزل شمارهٔ ۲۰۴۰

<div class="b" id="bn1"><div class="m1"><p>دلهای غم ندیده پذیرای پند نیست</p></div>
<div class="m2"><p>آنجا که درد نیست، سخن سودمند نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسیار چاره هست که از درد بدترست</p></div>
<div class="m2"><p>صد چشم بد، برابر دود سپند نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما را به بخت شور خود ای دوست واگذار</p></div>
<div class="m2"><p>بادام تلخ در خور آغوش قند نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نتوان گرفت دامن معنی به دست ناز</p></div>
<div class="m2"><p>جز پیچ و تاب، صید سخن را کمند نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگرفت پیش اشک مرا منع آستین</p></div>
<div class="m2"><p>سیلاب را ملاحظه از کوچه بند نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لب بسته همچو غنچه تصویر زاده ایم</p></div>
<div class="m2"><p>ما را خبر ز چاشنی نوشخند نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صد دل چو تار سبحه به یک رشته می کشد</p></div>
<div class="m2"><p>کوتاهیی در آن مژه های بلند نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>امروز عیسیی که به درد سخن رسد</p></div>
<div class="m2"><p>صائب درین زمانه نادردمند نیست</p></div></div>