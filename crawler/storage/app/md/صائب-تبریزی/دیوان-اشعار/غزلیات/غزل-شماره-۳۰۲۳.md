---
title: >-
    غزل شمارهٔ ۳۰۲۳
---
# غزل شمارهٔ ۳۰۲۳

<div class="b" id="bn1"><div class="m1"><p>دل ما بر سیه روزان فقر از خود فزون سوزد</p></div>
<div class="m2"><p>چراغ خانه ما در برون بیش از درون سوزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به چشم روشنم از اشک خواهد شد سیه عالم</p></div>
<div class="m2"><p>به این عنوان اگر در دل مرا چون لاله خون سوزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندارد رنگی از بال سمندر آتش سوزان</p></div>
<div class="m2"><p>کجا آن پرده شرم از شراب لاله گون سوزد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود دست حمایت عشق حسن آتشین خو را</p></div>
<div class="m2"><p>که لرزد شمع بر خود بیشتر پروانه چون سوزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر نعلش در آتش نیست از خورشید رخساری</p></div>
<div class="m2"><p>چرا هر شب ز انجم داغ چرخ نیلگون سوزد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برآرد سر چو دود از خیمه گستاخانه لیلی را</p></div>
<div class="m2"><p>نفس چون گردباد آن را که در دشت جنون سوزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ندارم شکوه ای از طالع وارون، به این شادم</p></div>
<div class="m2"><p>که می آید به پایان زود چون شمعی نگون سوزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نشوید خواب اگر از چشم شیران گریه مجنون</p></div>
<div class="m2"><p>که در شبها چراغی بر سر اهل جنون سوزد؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برآید روز حشر از بوته صائب چون زر خالص</p></div>
<div class="m2"><p>به درد و داغ عشق آن کس که در اینجا فزون سوزد</p></div></div>