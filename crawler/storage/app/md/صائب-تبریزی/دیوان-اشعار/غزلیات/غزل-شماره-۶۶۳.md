---
title: >-
    غزل شمارهٔ ۶۶۳
---
# غزل شمارهٔ ۶۶۳

<div class="b" id="bn1"><div class="m1"><p>شکست رنگ می از ترک میگساری ما</p></div>
<div class="m2"><p>نمک به چشم قدح ریخت هوشیاری ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کم است اشک برای سیاهکاری ما</p></div>
<div class="m2"><p>مگر کند عرق انفعال یاری ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نماند در دل خم نم ز میگساری ما</p></div>
<div class="m2"><p>سفید شد لب ساغر ز بوسه کاری ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به چشم جوهریان گوهر بصیرت نیست</p></div>
<div class="m2"><p>وگرنه گرد یتیمی است خاکساری ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان کز آیه رحمت امید خلق افزود</p></div>
<div class="m2"><p>یکی هزار شد از خط امیدواری ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شده است حلقه گرداب، چشم قربانی</p></div>
<div class="m2"><p>ز چارموجه طوفان بی قراری ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رسید خیرگی چشم ما به معراجی</p></div>
<div class="m2"><p>که ماه بر فلک از هاله شد حصاری ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کلاه گوشه همت بلند کرده ماست</p></div>
<div class="m2"><p>چو تیغ کوه ز ابرست آبداری ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنان گذشت ز تقصیر ما عنایت دوست</p></div>
<div class="m2"><p>که از گناه نکرده است شرمساری ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به زیر تیغ فشردیم پای خود چندان</p></div>
<div class="m2"><p>که کوه بست کمر پیش بردباری ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز تار و پود جهان آگهیم با طفلی</p></div>
<div class="m2"><p>دویده است به هر کوچه نی سواری ما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زبان ما اگر از شکر تیغ خاموش است</p></div>
<div class="m2"><p>دهان شکرگزاری است زخم کاری ما</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ازان دوید به آفاق نام ما صائب</p></div>
<div class="m2"><p>که روشن است جهان از نفس شماری ما</p></div></div>