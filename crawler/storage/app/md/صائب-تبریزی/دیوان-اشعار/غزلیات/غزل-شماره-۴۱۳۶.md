---
title: >-
    غزل شمارهٔ ۴۱۳۶
---
# غزل شمارهٔ ۴۱۳۶

<div class="b" id="bn1"><div class="m1"><p>آنان که دل به عقل خدا دور داده اند</p></div>
<div class="m2"><p>مغز سر همای به عصفور داده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را چه اختیار که ضبط نگه کنیم</p></div>
<div class="m2"><p>سر رشته نظاره به منظور داده اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان باده ای که میکده پرداز آن منم</p></div>
<div class="m2"><p>ته جرعه ای به انجمن طور داده اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دشت عشق ملک سلیمان عقل را</p></div>
<div class="m2"><p>رندان باد دست به یک مور داده اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با چرخ پرستاره چه سازم کجا روم</p></div>
<div class="m2"><p>عریان سرم به خانه زنبور داده اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در کعبه یقین نرسیده است هیچ کس</p></div>
<div class="m2"><p>هر کس نشان آتشی از دور داده اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با خون دل بساز که در خاکدان دهر</p></div>
<div class="m2"><p>خط مسلمی به لب گور داده اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشم ترا ز میکده قسمت ازل</p></div>
<div class="m2"><p>نزدیکی دل ونگه دور داده اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این کارخانه را دل ما می برد به راه</p></div>
<div class="m2"><p>زنجیر فیل را به کف مور داده اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نتوان به اوج فکر رسیدن به بال سعی</p></div>
<div class="m2"><p>این منزلت به صائب پر شور داده اند</p></div></div>