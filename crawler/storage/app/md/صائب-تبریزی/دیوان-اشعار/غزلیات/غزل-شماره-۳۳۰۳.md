---
title: >-
    غزل شمارهٔ ۳۳۰۳
---
# غزل شمارهٔ ۳۳۰۳

<div class="b" id="bn1"><div class="m1"><p>عشق صد لخت جگر بر مژه تر دارد</p></div>
<div class="m2"><p>گره افزون خورد آن رشته که گوهر دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشق آن است که پا بر سر افلاک نهد</p></div>
<div class="m2"><p>باده آن است که خشت از سر خم بردارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خط سبز چه پرواست لب لعل ترا؟</p></div>
<div class="m2"><p>چه زیان موج به سرچشمه کوثر دارد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رشک بر کوکب اقبال حباب است مرا</p></div>
<div class="m2"><p>که به هر چشم زدن عالم دیگر دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گریه و آه، گل و سبزه باغ هنرست</p></div>
<div class="m2"><p>تیغ در آتش و آب است که جوهر دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غنچه در جامه خود چاک زدن عاجز نیست</p></div>
<div class="m2"><p>دل عاشق چه غم از طارم اخضر دارد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مدعی کیست که هنگامه ما سرد کند؟</p></div>
<div class="m2"><p>آتش ما مدد از دامن محشر دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر قدر مرتبه عشق بلند افتاده است</p></div>
<div class="m2"><p>سخن صائب ما رتبه دیگر دارد</p></div></div>