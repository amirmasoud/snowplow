---
title: >-
    غزل شمارهٔ ۴۰۴۶
---
# غزل شمارهٔ ۴۰۴۶

<div class="b" id="bn1"><div class="m1"><p>عشاق را خرام تو از خویش می برد</p></div>
<div class="m2"><p>سیل بهار هر چه کند پیش می برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کس که بی رفیق موافق سفر کند</p></div>
<div class="m2"><p>با خود هزار قافله تشویش می برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بوته گداز زر پاک را چه نقص</p></div>
<div class="m2"><p>از نیکوان چه صرفه بد اندیش می برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست از کرم مدار که از خوان پرنعیم</p></div>
<div class="m2"><p>رزق تو لقمه ای است که درویش می برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از زخم تیغ غوطه به خون بیشتر زند</p></div>
<div class="m2"><p>هر کس ستمگرست ستم بیش می برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن را که تازیانه ز رگهای گردن است</p></div>
<div class="m2"><p>هر دعوی غلط که کند پیش می برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگذر ز جمع مال که زنبور بی نصیب</p></div>
<div class="m2"><p>با خویشتن ز شان عسل نیش می برد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کج نیز راست می شود از قرب راستان</p></div>
<div class="m2"><p>صائب اگر ز تیر کجی کیش می برد</p></div></div>