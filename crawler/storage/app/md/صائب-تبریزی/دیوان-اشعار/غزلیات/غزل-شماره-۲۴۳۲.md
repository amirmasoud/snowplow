---
title: >-
    غزل شمارهٔ ۲۴۳۲
---
# غزل شمارهٔ ۲۴۳۲

<div class="b" id="bn1"><div class="m1"><p>آشنای حق شد آن کس کز جهان بیگانه شد</p></div>
<div class="m2"><p>هر که زین دریا برآمد گوهر یکدانه شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه از زنجیر هر دیوانه ای عاقل شود</p></div>
<div class="m2"><p>دید تا زنجیر زلف او دلم دیوانه شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کاش برمی داشت از خاکش در ایام حیات</p></div>
<div class="m2"><p>این که آخر شمع نخل ماتم پروانه شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با کدامین آبرو در کعبه آرم روی خویش؟</p></div>
<div class="m2"><p>من که سرجوش حیاتم صرف در بتخانه شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کار مردم جز فضولی نیست در زیر فلک</p></div>
<div class="m2"><p>هر که شد مهمان درین غمخانه، صاحبخانه شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکقلم بیگانه گردد ز آشنایان دگر</p></div>
<div class="m2"><p>هر که صائب آشنا با معنی بیگانه شد</p></div></div>