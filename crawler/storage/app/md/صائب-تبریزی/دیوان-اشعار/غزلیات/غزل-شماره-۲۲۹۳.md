---
title: >-
    غزل شمارهٔ ۲۲۹۳
---
# غزل شمارهٔ ۲۲۹۳

<div class="b" id="bn1"><div class="m1"><p>زان پیشتر که تیغ کشد آفتاب صبح</p></div>
<div class="m2"><p>رطلی به گردش آر گرانتر ز خواب صبح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرصت غنیمت است، به دست دعا بشوی</p></div>
<div class="m2"><p>داغ سیه گلیمی خود را به آب صبح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر عشر این کلام مبین است آفتاب</p></div>
<div class="m2"><p>زنهار بر مدار نظر از کتاب صبح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از باغ صبح خنده خشکی شنیده ای</p></div>
<div class="m2"><p>چون شیشه غافلی ز شمیم گلاب صبح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر عیش دل مبند که کم عمری نشاط</p></div>
<div class="m2"><p>روشن بود ز خنده پا در رکاب صبح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آسوده است عاشق صادق ز بیم حشر</p></div>
<div class="m2"><p>پاک است از غبار خیانت حساب صبح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صافی رسیده است به جایی که می کند</p></div>
<div class="m2"><p>مهر از بیاض سینه من انتخاب صبح</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از بوی گل اگر چه سبکروح تر شدم</p></div>
<div class="m2"><p>در چشم روزگار گرانم چو خواب صبح</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب سری برآر و تماشای فیض کن</p></div>
<div class="m2"><p>سگ نیستی، چه مرده ای از بهر خواب صبح؟</p></div></div>