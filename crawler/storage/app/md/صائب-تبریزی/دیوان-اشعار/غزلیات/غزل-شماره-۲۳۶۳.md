---
title: >-
    غزل شمارهٔ ۲۳۶۳
---
# غزل شمارهٔ ۲۳۶۳

<div class="b" id="bn1"><div class="m1"><p>گرچه ماه مصر را دامن زلیخا چاک کرد</p></div>
<div class="m2"><p>گرد تهمت چاک پیراهن زرویش پاک کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد ز آب تیغ گرد خط از آن عارض بلند</p></div>
<div class="m2"><p>چون توان آیینه را با دامن تر پاک کرد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده بد دور باد از روی آتشناک او</p></div>
<div class="m2"><p>کز خس و خار تمنا سینه ام را پاک کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آه کز گردنکشی چون تیغ زهرآلود، سرو</p></div>
<div class="m2"><p>طوق را بر قمری من حلقه فتراک کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عزم صادق رخنه در سد سکندر می کند</p></div>
<div class="m2"><p>صبح از آهی گریبان فلک را چاک کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کاش از غیبت دهان خویش را می کرد پاک</p></div>
<div class="m2"><p>آن که چندین پاک دندان خود از مسواک کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بحر رحمت را چرا باید غبارآلود ساخت؟</p></div>
<div class="m2"><p>تا توان زاشک ندامت دامن خود پاک کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر بیاض گردن مینای می آید به دست</p></div>
<div class="m2"><p>در دل شب می توان فیض سحر ادراک کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر نتابد تنگ ظرفی لقمه بیش از دهن</p></div>
<div class="m2"><p>تشنه ما را دل پر خون گریبان چاک کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر کند ساقی مسلسل دور جام باده را</p></div>
<div class="m2"><p>چند روزی می توان خون در دل افلاک کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیست غیر از عقده دل حاصلی پیوند را</p></div>
<div class="m2"><p>در بریدنها دلی از گریه خالی تاک کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می توان از ذکر حق تا کرد پر گوهر دهان</p></div>
<div class="m2"><p>حیف باشد از حدیث پوچ پرخاشاک کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آسیای سنگدل با دانه گندم نکرد</p></div>
<div class="m2"><p>آنچه با خاکی نهادان گردش افلاک کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گرچه صائب می چکد آب حیات از خامه ام</p></div>
<div class="m2"><p>دام بتوان از غبار خاطرم در خاک کرد</p></div></div>