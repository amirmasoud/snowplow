---
title: >-
    غزل شمارهٔ ۳۴۸۶
---
# غزل شمارهٔ ۳۴۸۶

<div class="b" id="bn1"><div class="m1"><p>عاشقان زان لب شیرین به سخن ساخته اند</p></div>
<div class="m2"><p>بوشناسان به نسیمی ز چمن ساخته اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون قلم، موی شکافان دبستان وجود</p></div>
<div class="m2"><p>از دو عالم به سر زلف سخن ساخته اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی به آن گلشن بیرنگ توانند رسید؟</p></div>
<div class="m2"><p>بلبلانی که به این سبز چمن ساخته اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوشه ای گیر ازین بزم که صحبت سازان</p></div>
<div class="m2"><p>بارها از لب پیمانه سخن ساخته اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خون به جای عرق افشانده ام از جبهه سعی</p></div>
<div class="m2"><p>تا کف خون مرا مشک ختن ساخته اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زود باشد که زبان در قفس کام کشند</p></div>
<div class="m2"><p>صائب آنان که به گلهای چمن ساخته اند</p></div></div>