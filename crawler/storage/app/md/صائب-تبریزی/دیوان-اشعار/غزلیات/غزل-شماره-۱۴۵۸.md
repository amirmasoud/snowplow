---
title: >-
    غزل شمارهٔ ۱۴۵۸
---
# غزل شمارهٔ ۱۴۵۸

<div class="b" id="bn1"><div class="m1"><p>سنگ در دیده ارباب بصیرت گهرست</p></div>
<div class="m2"><p>خاک در پله میزان قناعت شکرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسن را نشو و نما از نظر پاک بود</p></div>
<div class="m2"><p>آبروی چمن از شبنم روشن گهرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده بد به تو ای ترک ختایی مرساد!</p></div>
<div class="m2"><p>که بدخشان ز لب لعل تو خونین جگرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشتی از باد مخالف متزلزل گردد</p></div>
<div class="m2"><p>دل به جا نیست کسی را که پریشان نظرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از فضولی است ترا دست تصرف کوتاه</p></div>
<div class="m2"><p>بهله قالب چو تهی کرد مقامش کمرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنچه مانده است ز ته جرعه عمرم باقی</p></div>
<div class="m2"><p>خوردنش خون دل و ماندن او دردسرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می کند قطع به سر، راه طلب را صائب</p></div>
<div class="m2"><p>هر که چون سوزن فولاد حدیدالبصرست</p></div></div>