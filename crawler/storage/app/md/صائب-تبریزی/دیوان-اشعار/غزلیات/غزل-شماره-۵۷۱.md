---
title: >-
    غزل شمارهٔ ۵۷۱
---
# غزل شمارهٔ ۵۷۱

<div class="b" id="bn1"><div class="m1"><p>از بساط فلک آن سوی بود بازی ما</p></div>
<div class="m2"><p>شش جهت کیست به ششدر فکند بازی ما؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما حریفان کهنسال جهان ازلیم</p></div>
<div class="m2"><p>طفل شش روزه عالم ندهد بازی ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تخته نقش مرادست دل ساده دلان</p></div>
<div class="m2"><p>بازی خود دهد آن کس که دهد بازی ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قوت بازوی اقبال، رسا افتاده است</p></div>
<div class="m2"><p>نیست محتاج به تعلیم و مدد بازی ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خانه پرداختگانیم درین بازیگاه</p></div>
<div class="m2"><p>دل ز بازیچه گردون نخورد بازی ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیری و طفل مزاجی به هم آمیخته ایم</p></div>
<div class="m2"><p>تا شب مرگ به آخر نرسد بازی ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روزگاری است به گردون دغا هم نردیم</p></div>
<div class="m2"><p>عجبی نیست اگر پخته بود بازی ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون زر قلب نداریم به خود امیدی</p></div>
<div class="m2"><p>در شب تار جهان تا که خورد بازی ما؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ظاهر و باطن ما آینه یکدگرند</p></div>
<div class="m2"><p>خاک در چشم حریفی که دهد بازی ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بود ما محض نمودست، سرابیم سراب</p></div>
<div class="m2"><p>جای رحم است بر آن کس که خورد بازی ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جامه را پرده درویشی خود ساخته ایم</p></div>
<div class="m2"><p>ندهد فقر به تشریف نمد، بازی ما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چه خیال است که از پای نشیند صائب</p></div>
<div class="m2"><p>تا به هر کوچه چو طفلان ندود بازی ما</p></div></div>