---
title: >-
    غزل شمارهٔ ۱۱۱۷
---
# غزل شمارهٔ ۱۱۱۷

<div class="b" id="bn1"><div class="m1"><p>نامه از قاصد دل مغرور ما نگرفته است</p></div>
<div class="m2"><p>غیرت ما بوی یوسف از صبا نگرفته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرکشی از ترکتاز عشق بر ما تهمت است</p></div>
<div class="m2"><p>گرد ما افتادگان هرگز هوا نگرفته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با دل روشن زمین و آسمان غمخانه ای است</p></div>
<div class="m2"><p>صورتی دارد جهان تا دل جلا نگرفته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می رسد آخر به جایی گریه خونین ما</p></div>
<div class="m2"><p>خون ناحق را کسی پا در حنا نگرفته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز ما را گر سیه کردند این مه طلعتان</p></div>
<div class="m2"><p>دامن شب را کسی از دست ما نگرفته است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چه هر کس یافته است از دامن شب یافته است</p></div>
<div class="m2"><p>دل عبث دامان آن زلف دو تا نگرفته است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آه را در سینه سوزان من آرام نیست</p></div>
<div class="m2"><p>دود از آتش این چنین صائب هوا نگرفته است</p></div></div>