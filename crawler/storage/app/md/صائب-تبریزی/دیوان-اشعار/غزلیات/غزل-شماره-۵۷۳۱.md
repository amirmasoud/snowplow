---
title: >-
    غزل شمارهٔ ۵۷۳۱
---
# غزل شمارهٔ ۵۷۳۱

<div class="b" id="bn1"><div class="m1"><p>ز خال گوشه ابروی یار می ترسم</p></div>
<div class="m2"><p>ازین ستاره دنباله دار می ترسم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو مهره در دهن مار می توانم رفت</p></div>
<div class="m2"><p>از آن دو سلسله تابدار می ترسم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز رنگ و بوی جهان قانعم به بی برگی</p></div>
<div class="m2"><p>خزان گزیده ام از نوبهار می ترسم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز نیش مار به نرمی نمی توان شد امن</p></div>
<div class="m2"><p>من از ملایمت روزگار می ترسم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکست دشمن عاجز نه از جوانمردی است</p></div>
<div class="m2"><p>ترا گمان که من از نیش خار می ترسم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به تنگ حوصلگان بر نمی توان آمد</p></div>
<div class="m2"><p>از بحر بیش من از چشمه سار می ترسم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا ز آتش دوزخ نمی توان ترساند</p></div>
<div class="m2"><p>ز شرمساری روز شمار می ترسم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز سیل حادثه از جا نمی روم صائب</p></div>
<div class="m2"><p>ز شبنم رخ آن گلعذار می ترسم</p></div></div>