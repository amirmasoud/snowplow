---
title: >-
    غزل شمارهٔ ۲۳۸۲
---
# غزل شمارهٔ ۲۳۸۲

<div class="b" id="bn1"><div class="m1"><p>کوه را چون ابر، حکم او به رفتار آورد</p></div>
<div class="m2"><p>ریگ را چون سبحه، ذکر او به گفتار آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جنبش ما ناتوانان است از اقبال عشق</p></div>
<div class="m2"><p>ذره را خورشید تابان بر سر کار آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرده پوشی می کند دریای جوشان را به کف</p></div>
<div class="m2"><p>آن که می خواهد مرا دربند دستار آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دل بی حاصلم هر جا حدیثی بگذرد</p></div>
<div class="m2"><p>بید مجنون سر به پیش انداختن بار آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد زبیکاری سیه عالم به چشم من، کجاست</p></div>
<div class="m2"><p>چشم پرکاری که ما را بر سر کار آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مهر عالمتاب در هر جا دچار او شود</p></div>
<div class="m2"><p>با کمال شوخ چشمی رو به دیوار آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر نهالی را که آبش از گداچشمان بود</p></div>
<div class="m2"><p>شاخسارش برگ سبز سایلان بار آورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می تواند با تو در پیری هم آغوشم کند</p></div>
<div class="m2"><p>آن که چندین گل برون از پرده خار آورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون ید بیضا فروغش نور می سوزد به چشم</p></div>
<div class="m2"><p>در نظر چون گوهر ما را خریدار آورد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دفتر گل را دهد بلبل به باد از آه سرد</p></div>
<div class="m2"><p>فردی از دیوان اگر صائب به گلزار آورد</p></div></div>