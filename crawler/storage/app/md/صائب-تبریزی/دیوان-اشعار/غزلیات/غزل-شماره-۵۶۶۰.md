---
title: >-
    غزل شمارهٔ ۵۶۶۰
---
# غزل شمارهٔ ۵۶۶۰

<div class="b" id="bn1"><div class="m1"><p>چه ضرورست که آلوده تعمیر شوم؟</p></div>
<div class="m2"><p>در ره سیل چه افتاده زمین گیر شوم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک در دیده همت نتوان زد، ورنه</p></div>
<div class="m2"><p>می توانم که چو خورشید جهانگیر شوم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه گذارم ز ریاضیت جگر خود، که مرا</p></div>
<div class="m2"><p>به زر قلب نگیرند گر اکسیر شوم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منت مهد امان می کشم از طالع خویش</p></div>
<div class="m2"><p>اگر از زخم زبان در دهن شیر شوم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش دریا چه ضرورست کنم گردن کج؟</p></div>
<div class="m2"><p>من که قانع به دمی آب چو شمشیر شوم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون کمان گوشه گر از خلق کنم معذورم</p></div>
<div class="m2"><p>چند از انگشت اشارت هدف تیر شوم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو به صد آینه از دیدن خود سیرنه ای</p></div>
<div class="m2"><p>من به یک چشم ز دیدار تو چون سیر شوم؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می کند عشق جوانمرد تلافی صائب</p></div>
<div class="m2"><p>چون زلیخا ز غم عشق اگر پیر شوم</p></div></div>