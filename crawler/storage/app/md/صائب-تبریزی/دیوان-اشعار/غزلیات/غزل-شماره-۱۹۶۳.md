---
title: >-
    غزل شمارهٔ ۱۹۶۳
---
# غزل شمارهٔ ۱۹۶۳

<div class="b" id="bn1"><div class="m1"><p>بر طفل اشک خون جگر دست یافته است</p></div>
<div class="m2"><p>در آب، رنگ چون به گهر دست یافته است؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بتوان به حرف نرم دل سنگ آب کرد</p></div>
<div class="m2"><p>شیر از ملایمت به شکر دست یافته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین طفل مشربان ز مکتب گریخته</p></div>
<div class="m2"><p>آفت ز شش جهت به ثمر دست یافته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیری ز آب تیغ ندارد شهید ما</p></div>
<div class="m2"><p>بر آب خضر تشنه جگر دست یافته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>افتادگی چرا نکند کس شعار خویش؟</p></div>
<div class="m2"><p>زلف از فتادگی به کمر دست یافته است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خود را چسان به بوسه تسلی کنم ازو؟</p></div>
<div class="m2"><p>موری به تنگهای شکر دست یافته است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در هم نریخته است اگر مهره نجوم</p></div>
<div class="m2"><p>چون بدگهر به پاک گهر دست یافته است؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>امروز نیست دست جفای فلک دراز</p></div>
<div class="m2"><p>دیری است تا بر اهل هنر دست یافته است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی گریه ای مباش که شبنم به طرف باغ</p></div>
<div class="m2"><p>بر گل ز فیض دیده تر دست یافته است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نبود عجب که خنده نو کیسگی زند</p></div>
<div class="m2"><p>گل یک دو روز شد که به زر دست یافته است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فرهاد هم به کوه و کمر برده است راه</p></div>
<div class="m2"><p>خسرو اگر به گنج گهر دست یافته است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>(خواهد شدن چو لاله بناگوش میکشان</p></div>
<div class="m2"><p>از نوبهار تاک شرر دست یافته است)</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برگشته است همچو صدا بی اثر ز کوه</p></div>
<div class="m2"><p>فریاد ما کجا به اثر دست یافته است؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون آب، موج می زند از جبهه صدف</p></div>
<div class="m2"><p>کز پاک طینتی به گهر دست یافته است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بی سیلی و تپانچه کسی از دست می دهد؟</p></div>
<div class="m2"><p>بر طفل شوخ چشم، پدر دست یافته است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صائب شکر به تنگ بود در کلام تو</p></div>
<div class="m2"><p>کلک تو بر کدام شکر دست یافته است؟</p></div></div>