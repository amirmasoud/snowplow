---
title: >-
    غزل شمارهٔ ۲۰۴۴
---
# غزل شمارهٔ ۲۰۴۴

<div class="b" id="bn1"><div class="m1"><p>ما را دماغ جنگ و سر کارزار نیست</p></div>
<div class="m2"><p>ورنه دل دو نیم کم از ذوالفقار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیوانه ای که می رمد از سنگ کودکان</p></div>
<div class="m2"><p>بیرون کنش ز شهر که کامل عیار نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خواب درگذر که سپهر وجود را</p></div>
<div class="m2"><p>انجم بغیر دیده شب زنده دار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون موجه سراب اسیر کشاکش است</p></div>
<div class="m2"><p>پایی که در مقام رضا استوار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیداست چیست لنگر مشت غبار ما</p></div>
<div class="m2"><p>در عالمی که کوه گران پایدار نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با زاهدان خشک مکن گفتگوی عشق</p></div>
<div class="m2"><p>شمشیر چوب را جگر کارزار نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از دل برون نمی رود امید بخت سبز</p></div>
<div class="m2"><p>هر چند تخم سوخته را نوبهار نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون وا نمی کند گره از کار هیچ کس؟</p></div>
<div class="m2"><p>دست فلک اگر ز شفق در نگار نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از هیزم است آتش سوزنده را حیات</p></div>
<div class="m2"><p>منصور را ملاحظه از چوب دار نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون ماهی ضعیف که افتد در آب تند</p></div>
<div class="m2"><p>در اختیار خویش مرا اختیار نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از حال هم ز مرده دلی خلق غافلند</p></div>
<div class="m2"><p>ورنه کدام سینه که لوح مزار نیست؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خمیازه را به خنده غلط کرده اند خلق</p></div>
<div class="m2"><p>ورنه گل شکفت درین خارزار نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>(با حکمم ایزدی چه بود گیر و دار خلق؟</p></div>
<div class="m2"><p>خاشاک را در آب روان اختیار نیست)</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>(در هیچ سینه نیست که نشکسته ناخنی</p></div>
<div class="m2"><p>یک داغ سر به مهر درین لاله زار نیست)</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ریحان زلف اگر چه ز دل زنگ می برد</p></div>
<div class="m2"><p>صائب به دلنشینی خط غبار نیست</p></div></div>