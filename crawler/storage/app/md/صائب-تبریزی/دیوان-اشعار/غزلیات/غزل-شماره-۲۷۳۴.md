---
title: >-
    غزل شمارهٔ ۲۷۳۴
---
# غزل شمارهٔ ۲۷۳۴

<div class="b" id="bn1"><div class="m1"><p>کی به عاشق بوسه آن لعل لب میگون دهد؟</p></div>
<div class="m2"><p>نیست ممکن گوهر شاداب نم بیرون دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکوه از دل کی تراود تا نگردد دل دو نیم؟</p></div>
<div class="m2"><p>چون زبان خامه شق گردد سخن بیرون دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که آب از چشمه سار بی نیازی خورده است</p></div>
<div class="m2"><p>آب گوهر در مذاقش تلخی افیون دهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش چرخ بی مروت آبروی خود مریز</p></div>
<div class="m2"><p>این سبوی کهنه هیهات است نم بیرون دهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر نیارد سرمه دان دریاکشان را از خمار</p></div>
<div class="m2"><p>دیده آهو چه تسکین دل مجنون دهد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خلق مجنون را نسازد تنگ، جوش دام و دد</p></div>
<div class="m2"><p>کوه را دیوانگی پیشانی هامون دهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست بوی گل دماغ آشفتگان را سازگار</p></div>
<div class="m2"><p>ما و دامان بیابانی که بوی خون دهد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عالم امکان، کف بحر پر آشوب فناست</p></div>
<div class="m2"><p>پشت بر دیوار آسایش کس اینجا چون دهد؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که دریابد نشاط باده تلخ فنا</p></div>
<div class="m2"><p>بوسه بر لبهای خنجر چون لب میگون دهد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لقمه چرب از برای خاک سامان می کند</p></div>
<div class="m2"><p>هر که را گردون دون، جمعیت قارون دهد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتم از زرکار من چون زر شود، غافل که چرخ</p></div>
<div class="m2"><p>چون گل رعنا مرا از کاسه زر خون دهد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حکمت اندوزی که شد گوهرشناس وقت صاف</p></div>
<div class="m2"><p>بوسه ها بر پای خم مانند افلاطون دهد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زان خوشم صائب به نان جو که بر خوان جهان</p></div>
<div class="m2"><p>نعمت الوان ثمر غمهای گوناگون دهد</p></div></div>