---
title: >-
    غزل شمارهٔ ۳۸۴۸
---
# غزل شمارهٔ ۳۸۴۸

<div class="b" id="bn1"><div class="m1"><p>خوشم که خرده جان صرف یار جانی شد</p></div>
<div class="m2"><p>دو روزه هستی من عمر جاودانی شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به عمر خویش تلافی نمی توان کردن</p></div>
<div class="m2"><p>زفرصت آنچه مرا فوت در جوانی شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فغان که لعل لب آبدار او از خط</p></div>
<div class="m2"><p>سیاه کاسه تر از آب زندگانی شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خنده باز مکن لب که عمر گل کوتاه</p></div>
<div class="m2"><p>درین ریاض ز تأثیر شادمانی شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آن جهان گل رعنای باغ فردوس است</p></div>
<div class="m2"><p>ز اشک چهره زردی که ارغوانی شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زنخلها که رساندم درین ریاض مرا</p></div>
<div class="m2"><p>کف پرآبله قسمت ز باغبانی شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوشم که مایه اشکی بهم رسید مرا</p></div>
<div class="m2"><p>اگر چه خون دلم از حسرت جوانی شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا ز گریه شب روح تازه شد صائب</p></div>
<div class="m2"><p>نصیب خضر اگر آب زندگانی شد</p></div></div>