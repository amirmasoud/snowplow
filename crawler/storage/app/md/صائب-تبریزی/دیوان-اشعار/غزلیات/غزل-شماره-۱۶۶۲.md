---
title: >-
    غزل شمارهٔ ۱۶۶۲
---
# غزل شمارهٔ ۱۶۶۲

<div class="b" id="bn1"><div class="m1"><p>غبار هستی ما پرده دار سیلاب است</p></div>
<div class="m2"><p>کتان طاقت ما شیر مست مهتاب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دهان شیر بود خوابگاه وادی عشق</p></div>
<div class="m2"><p>حصار عافیت این محیط، گرداب است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان ز سیر چمن خاطرم گزیده شده است</p></div>
<div class="m2"><p>که شاخ گل به نظر آستین قصاب است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عیار آتش روی ترا چه می داند؟</p></div>
<div class="m2"><p>هنوز دیده آیینه در شکرخواب است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر ز غیبت ما در حضور می افتند</p></div>
<div class="m2"><p>حضور خاطر ما در حضور احباب است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترا چه بهره ز رنگینی کلام بود؟</p></div>
<div class="m2"><p>که همچو طفلان چشمت به سرخی باب است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دور زلف تو کفر آنچنان رواج گرفت</p></div>
<div class="m2"><p>که طاق نسیان امروز طاق محراب است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سر مشاهده عیب خود اگر داری</p></div>
<div class="m2"><p>کدام آینه بهتر ز عالم آب است؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چرا خموش نگردند طوطیان صائب؟</p></div>
<div class="m2"><p>سخن شناس درین روزگار نایاب است</p></div></div>