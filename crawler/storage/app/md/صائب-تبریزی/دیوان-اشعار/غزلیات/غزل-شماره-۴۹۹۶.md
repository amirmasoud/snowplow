---
title: >-
    غزل شمارهٔ ۴۹۹۶
---
# غزل شمارهٔ ۴۹۹۶

<div class="b" id="bn1"><div class="m1"><p>کرد بیهوش مرا نعره مستانه خویش</p></div>
<div class="m2"><p>خواب من گشت گرانسنگ ز افسانه خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بحر و کان را کف افسوس کند بی برگی</p></div>
<div class="m2"><p>گر به بازار برم گوهر یکدانه خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از شبیخون نسیم سحر ایمن می بود</p></div>
<div class="m2"><p>شمع می کرد اگر رحم به پروانه خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وقت آن خوش که درین دایره همچون پرگار</p></div>
<div class="m2"><p>ازسویدای دل خویش کند دانه خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عمر چون باد به تعجیل ازان می گذرد</p></div>
<div class="m2"><p>که تو غافل کنی از کاه جدا دانه خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باعث غفلت من شد سخن من صائب</p></div>
<div class="m2"><p>خواب من گشت گرانسنگ ز افسانه خویش</p></div></div>