---
title: >-
    غزل شمارهٔ ۲۵۷
---
# غزل شمارهٔ ۲۵۷

<div class="b" id="bn1"><div class="m1"><p>آهوان را در کمند آورد چشم پاک ما</p></div>
<div class="m2"><p>شد چو مجنون دیده ما حلقه فتراک ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همت آه رسای ما بلند افتاده است</p></div>
<div class="m2"><p>از زبردستی به ساق عرش پیچد تاک ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون صدف از سینه صافی قطره را گوهر کنیم</p></div>
<div class="m2"><p>وقت تخمی خوش که افتد در زمین پاک ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر زمین هر چند نقش از خاکساری بسته ایم</p></div>
<div class="m2"><p>باکمال سرکشی گردون بود در خاک ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناتوانان را زبان شکوه می باشد خموش</p></div>
<div class="m2"><p>برنمی خیزد به آتش دود از خاشاک ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم بی یوسف گشودن، از نظربازان خطاست</p></div>
<div class="m2"><p>ورنه بوی پیرهن باشد گریبان چاک ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در ضمیر نقطه ما صد سواد اعظم است</p></div>
<div class="m2"><p>چشم کوته بین مردم چون کند ادراک ما؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمع می لرزد چو برگ بید با آن سرکشی</p></div>
<div class="m2"><p>چون به محفل رو نهد پروانه بی باک ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاک دامنگیر، بند دست و پای رهروست</p></div>
<div class="m2"><p>نیست ممکن غم برآید از دل غمناک ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شبنم ما گر چه صائب در نمی آید به چشم</p></div>
<div class="m2"><p>تازه دارد گلستان را دیده نمناک ما</p></div></div>