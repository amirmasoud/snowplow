---
title: >-
    غزل شمارهٔ ۵۴۵۱
---
# غزل شمارهٔ ۵۴۵۱

<div class="b" id="bn1"><div class="m1"><p>ما که سطر کهکشان از لوح گردون خوانده ایم</p></div>
<div class="m2"><p>در خط دیوانی زنجیر، عاجز مانده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سر بازار محشر دست ما خواهد گرفت</p></div>
<div class="m2"><p>در مصاف آرزو دستی که بر دل مانده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رنجش بیجا گل خودروی باغ دوستی است</p></div>
<div class="m2"><p>ورنه ما کی دشمن خود را ز خود رنجانده ایم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عقده می افتد به کار غنچه گل از نسیم</p></div>
<div class="m2"><p>در گلستانی که ما سر در گریبان مانده ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم کوته بین تمنای قیامت می کند</p></div>
<div class="m2"><p>ما ز پشت این نامه را نوعی که باید خوانده ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این زمان در ضبط اشک خویش صائب عاجزیم</p></div>
<div class="m2"><p>ما که از دریا عنان سیل را پیچانده ایم</p></div></div>