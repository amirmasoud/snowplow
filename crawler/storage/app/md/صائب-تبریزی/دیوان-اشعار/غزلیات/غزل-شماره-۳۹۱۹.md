---
title: >-
    غزل شمارهٔ ۳۹۱۹
---
# غزل شمارهٔ ۳۹۱۹

<div class="b" id="bn1"><div class="m1"><p>چرا به خلدبرین از خدا شوی خرسند</p></div>
<div class="m2"><p>به جوی شیر چو طفلان چرا شوی خرسند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز ماه مصر به زندان چاه ساخته ای</p></div>
<div class="m2"><p>اگر به هر دو جهان از خدا شوی خرسند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مباد همچو سکندر درین تماشاگاه</p></div>
<div class="m2"><p>به آبگینه ز آب بقا شوی خرسند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سعادت ازلی بی حجاب می تابد</p></div>
<div class="m2"><p>چرا به سایه بال هما شوی خرسند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهشت نسیه خود نقد می توانی کرد</p></div>
<div class="m2"><p>ز خلد اگر به مقام رضا شوی خرسند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز هر شکست ترا شهپری دهند چوموج</p></div>
<div class="m2"><p>اگر به حکم روان قضا شوی خرسند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به آشنایی بیگانگان برآمده ای</p></div>
<div class="m2"><p>تو آن نه ای که به یک آشنا شوی خرسند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بلنددار نظر را مباد چون نرگس</p></div>
<div class="m2"><p>ز چشم خود به همین پیش پا شوی خرسند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز شش جهت در روزی ترا گشاده شود</p></div>
<div class="m2"><p>اگرزعشق به درد وبلا شوی خرسند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به خواب ناز روی همچو چشم قربانی</p></div>
<div class="m2"><p>اگر به خاطر بی مدعا شوی خرسند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>علم شوی به طراوت چو نرگس بیمار</p></div>
<div class="m2"><p>به درد خویش اگر از دوا شوی خرسند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز فکر رزق پریشان نمی شوی صائب</p></div>
<div class="m2"><p>اگر به پاره دل از غذاشوی خرسند</p></div></div>