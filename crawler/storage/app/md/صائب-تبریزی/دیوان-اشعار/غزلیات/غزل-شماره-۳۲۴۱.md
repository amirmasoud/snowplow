---
title: >-
    غزل شمارهٔ ۳۲۴۱
---
# غزل شمارهٔ ۳۲۴۱

<div class="b" id="bn1"><div class="m1"><p>دل سودازده در طره دلدار افتاد</p></div>
<div class="m2"><p>گل بچینید که دیوانه به بازار افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو مفلس که فتد راه به گنجش ناگاه</p></div>
<div class="m2"><p>بوسه را راه به کنج دهن یار افتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر تنک حوصله ره یافت در آن خلوت خاص</p></div>
<div class="m2"><p>شیشه ماست که از طاق دل یار افتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر دل خونشده دندان نگذارد، چه کند؟</p></div>
<div class="m2"><p>کار گوهر چو به انصاف خریدار افتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست ممکن نشود نقل مجالس اشکش</p></div>
<div class="m2"><p>دیده هر که بر آن لعل شکر بار افتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از جبین گوهر جان را چو عرق ریخت به خاک</p></div>
<div class="m2"><p>راه هرکس که به این وادی خونخوار افتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سنگ طفلان شمرد کوه گران را صائب</p></div>
<div class="m2"><p>عاشقی را که چو فرهاد به سرکار افتاد</p></div></div>