---
title: >-
    غزل شمارهٔ ۵۲۵۹
---
# غزل شمارهٔ ۵۲۵۹

<div class="b" id="bn1"><div class="m1"><p>رخسار همچو ماه تو از عنبرین هلال</p></div>
<div class="m2"><p>درگوش آفتاب کشد حلقه زوال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فارغ زرشک آینه وآب کرده است</p></div>
<div class="m2"><p>عشاق را نظاره آن حسن بی مثال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر لعل او عقیق کند آب خود سبیل</p></div>
<div class="m2"><p>بر سیب او سهیل کند خون خود حلال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لب نیست رخنه ای توان بست چون گشود</p></div>
<div class="m2"><p>چندان که ممکن است بپرهیز از سوال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صائب دلش فسرده نگردد ز برگریز</p></div>
<div class="m2"><p>مرغی که بهار کشد سر به زیر بال</p></div></div>