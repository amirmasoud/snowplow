---
title: >-
    غزل شمارهٔ ۳۷۶۰
---
# غزل شمارهٔ ۳۷۶۰

<div class="b" id="bn1"><div class="m1"><p>تو آن نه ای که ره از خود بدر توانی برد</p></div>
<div class="m2"><p>نمرده از سر خود دردسر توانی برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کمر نبسته به قصد هلاک خود چون شمع</p></div>
<div class="m2"><p>کجا ز بزم جهان تاج زر توانی برد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز شاخ خشک تو آن روز گل توانی چید</p></div>
<div class="m2"><p>که در بهار سری زیر پر توانی برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترا ستیزه به گردون خوش است در وقتی</p></div>
<div class="m2"><p>که التجا به سپهر دگر توانی برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به داغ عشق اگر آشنا شوی امروز</p></div>
<div class="m2"><p>در آفتاب قیامت بسر توانی برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گره نکرده نفس را به سینه چون غواص</p></div>
<div class="m2"><p>کجا ز بحر حقیقت گهر توانی برد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه آنچنان ز خود افتاده ای تو غافل دور</p></div>
<div class="m2"><p>که ره به منزل اصلی دگر توانی برد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر به خشک لبی چون صدف شوی قانع</p></div>
<div class="m2"><p>به خانه نهر ز آب گهر توانی برد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو کز صفای دل خویش عاجزی صائب</p></div>
<div class="m2"><p>کلف چگونه ز روی قمر توانی برد؟</p></div></div>