---
title: >-
    غزل شمارهٔ ۴۵۳
---
# غزل شمارهٔ ۴۵۳

<div class="b" id="bn1"><div class="m1"><p>ندارد حاجت مشاطه روی گلعذار ما</p></div>
<div class="m2"><p>پر طاوس مستغنی است از نقش و نگار ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمین از سایه ما گر شود نیلی، عجب نبود</p></div>
<div class="m2"><p>که کوه قاف می بازد کمر در زیر بار ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز طوف ما دل بی درد صاحب درد می گردد</p></div>
<div class="m2"><p>چراغ کشته در می گیرد از خاک مزار ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکوه خاکساری خصم را بی دست و پا سازد</p></div>
<div class="m2"><p>شود باریک، دریا چون رسد در جویبار ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بال افشانی جان این چنین معلوم می گردد</p></div>
<div class="m2"><p>که چشم دام زلفی می پرد در انتظار ما</p></div></div>