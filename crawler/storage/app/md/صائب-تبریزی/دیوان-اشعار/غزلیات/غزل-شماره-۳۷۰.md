---
title: >-
    غزل شمارهٔ ۳۷۰
---
# غزل شمارهٔ ۳۷۰

<div class="b" id="bn1"><div class="m1"><p>بلند آوازه سازد شور عاشق عشق سرکش را</p></div>
<div class="m2"><p>به فریاد آورد مشتی نمک دریای آتش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو بالا می شود شور جنون در دامن صحرا</p></div>
<div class="m2"><p>که گردد بال و پر میدان خالی اسب سرکش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز سیر و دور مجنون عشق عالمسوز کامل شد</p></div>
<div class="m2"><p>که سازد شعله جواله خوش پرگار آتش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تراوش می کند خون دل از لبهای خشک من</p></div>
<div class="m2"><p>سفال تشنه گر بیرون دهد صهبای بی غش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به احسان دولت دنیای فانی می شود باقی</p></div>
<div class="m2"><p>عنانداری به دست باز کن این اسب سرکش را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کجا آگاه گردی از دل صاف خشن پوشان؟</p></div>
<div class="m2"><p>که از آیینه داری در نظر پشت منقش را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فضای آسمان تنگ است بر جولان شوخ او</p></div>
<div class="m2"><p>به افسون چون توان در شیشه کردن آن پریوش را؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به چشم خود چو مژگان جا دهد صید حرم تیرت</p></div>
<div class="m2"><p>مکن خالی به هر صید زبون زنهار ترکش را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قناعت با نگاه دور کن ز آمیزش خوبان</p></div>
<div class="m2"><p>که آب زندگی هم می کند خاموش آتش را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گذارد عشق هر کس را که نعل شوق در آتش</p></div>
<div class="m2"><p>به اسب چوب صائب طی کند صحرای آتش را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>میفشان تخم قابل در زمین شور بی حاصل</p></div>
<div class="m2"><p>به بی دردان مخوان زنهار صائب شعر دلکش را</p></div></div>