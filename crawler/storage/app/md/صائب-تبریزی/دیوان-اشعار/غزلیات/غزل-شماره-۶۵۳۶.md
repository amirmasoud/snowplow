---
title: >-
    غزل شمارهٔ ۶۵۳۶
---
# غزل شمارهٔ ۶۵۳۶

<div class="b" id="bn1"><div class="m1"><p>نتوان در آب و آینه دیدن مثال تو</p></div>
<div class="m2"><p>چون مد آه، سایه ندارد نهال تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صید حرم نداشت ز تیغ تو جان دریغ</p></div>
<div class="m2"><p>من خون خویش را نکنم چون حلال تو؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از من نمانده عشق بجا غیر درد و داغ</p></div>
<div class="m2"><p>آن به که نگذرم به دل بی ملال تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مور حریص دانه به منزل نمی برد</p></div>
<div class="m2"><p>زینسان که می برد دل عشاق خال تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون بوی گل که می شود از برگ بیشتر</p></div>
<div class="m2"><p>در پرده بیش فیض رساند جمال تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وصل مدام به بود از وصل گاه گاه</p></div>
<div class="m2"><p>مستغنی از وصال توام با خیال تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد قامتت نهال ز آب دو چشم من</p></div>
<div class="m2"><p>انصاف نیست برنخورم از نهال تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گلگونه عذار شود آفتاب را</p></div>
<div class="m2"><p>شد خون هر که همچو شفق پایمال تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عاشق ترا به گریه چسان رام خود کند؟</p></div>
<div class="m2"><p>کز بوی خون خویش کند رم غزال تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون با رخ تو ماه برآید، که آفتاب</p></div>
<div class="m2"><p>خون از شفق عرق کند از انفعال تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب شده است تنگ شکرگوش عالمی</p></div>
<div class="m2"><p>از گفتگوی طوطی شیرین مقال تو</p></div></div>