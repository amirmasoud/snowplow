---
title: >-
    غزل شمارهٔ ۱۴۲۳
---
# غزل شمارهٔ ۱۴۲۳

<div class="b" id="bn1"><div class="m1"><p>شور در دل فکند لعل خموشی که تراست</p></div>
<div class="m2"><p>خواب را تلخ کند چشمه نوشی که تراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از لطافت، سخنی چند که در دل داری</p></div>
<div class="m2"><p>می توان خواند ز لبهای خموشی که تراست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواب را شوخی چشم تو رم آهو کرد</p></div>
<div class="m2"><p>چه کند باده گلرنگ به هوشی که تراست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صرف خمیازه آغوش شود اوقاتش</p></div>
<div class="m2"><p>هر که را چشم فتد بر بر و دوشی که تراست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای بسا روز عزیزان که سیه خواهد کرد</p></div>
<div class="m2"><p>از خط و زلف، رخ غالیه پوشی که تراست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سبزه تربتش از آب گهر سبز شود</p></div>
<div class="m2"><p>هر که چشم آب دهد از در گوشی که تراست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه بهشتی است که ایمان به گرو می گیرد</p></div>
<div class="m2"><p>از فقیران، نگه باده فروشی که تراست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طرف دعوی صائب مشو ای بلبل مست</p></div>
<div class="m2"><p>که دو هفته است همین جوش و خروشی که تراست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست ممکن که ترا پخته نسازد صائب</p></div>
<div class="m2"><p>چون می تلخ درین میکده جوشی که تراست</p></div></div>