---
title: >-
    غزل شمارهٔ ۵۶۷۳
---
# غزل شمارهٔ ۵۶۷۳

<div class="b" id="bn1"><div class="m1"><p>صبح در خواب عدم بود که بیدار شدیم</p></div>
<div class="m2"><p>شب سیه مست فنا بود که هشیار شدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پای ما نقطه صفت در گرو دامن بود</p></div>
<div class="m2"><p>به تماشای تو سرگشته چو پرگار شدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به شکار آمده بودیم ز معموره قدس</p></div>
<div class="m2"><p>دانه خال تو دیدیم گرفتار شدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در کف عقل کم از قطره شبنم بودیم</p></div>
<div class="m2"><p>کاوشی کرد جنون قلزم زخار شدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پای زنگار بر آیینه ما می لغزد</p></div>
<div class="m2"><p>صیقلی بس که از آن آینه رخسار شدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نرود دیده شبنم به شکر خواب بهار</p></div>
<div class="m2"><p>عبث افسانه طراز دل بیدار شدیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خانه پردازتر از سیل بهاران بودیم</p></div>
<div class="m2"><p>لنگر انداخت خرد، خانه نگهدار شدیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون مؤذن سر تسبیح شماران بودیم</p></div>
<div class="m2"><p>گردشی کرد فلک، رشته ز تار شدیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جان به تاراج دهد خدمت سی روزه عشق</p></div>
<div class="m2"><p>قوت طالع ما بود که بیمار شدیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عالم بیخبری طرفه بهشتی بوده است</p></div>
<div class="m2"><p>حیف و صید حیف که ما دیر خبردار شدیم!</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب از کاسه دریوزه ما ریزد نور</p></div>
<div class="m2"><p>تا گدای در شه قاسم انوار شدیم</p></div></div>