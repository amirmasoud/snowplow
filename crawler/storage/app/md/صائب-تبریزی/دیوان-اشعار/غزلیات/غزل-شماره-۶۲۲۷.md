---
title: >-
    غزل شمارهٔ ۶۲۲۷
---
# غزل شمارهٔ ۶۲۲۷

<div class="b" id="bn1"><div class="m1"><p>برای کام دنیا دامن دل بر میان مشکن</p></div>
<div class="m2"><p>پر و بال هما را در هوای استخوان مشکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فلک در زیر پای توست چون از خود برون آیی</p></div>
<div class="m2"><p>برای این ره نزدیک دامن بر میان مشکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به یوسف می توان بخشید جرم کاروانی را</p></div>
<div class="m2"><p>خدا را در میان بین، خاطر خلق جهان مشکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به بال توست در این خاکدان چون تیر پروازم</p></div>
<div class="m2"><p>به جرم کجروی بال من ای ابرو کمان مشکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غرور حسن ای مجنون شراکت برنمی تابد</p></div>
<div class="m2"><p>خمار چشم لیلی را به چشم آهوان مشکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکست بی گناهان سنگ را در ناله می آرد</p></div>
<div class="m2"><p>دل چون شیشه ما را به سنگ امتحان مشکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حریف کاسه زهر پشیمانی نخواهد شد</p></div>
<div class="m2"><p>به حرف دشمنان زنهار قلب دوستان مشکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به کار چشم زخم باغ می آید وجود من</p></div>
<div class="m2"><p>به جرم بی بری شاخ مرا ای باغبان مشکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به گوش جان نگه دار این نصیحت را ز من صائب</p></div>
<div class="m2"><p>اگر خواهی که قدرت نشکند نان خسان مشکن</p></div></div>