---
title: >-
    غزل شمارهٔ ۵۱۹۸
---
# غزل شمارهٔ ۵۱۹۸

<div class="b" id="bn1"><div class="m1"><p>می‌توان با تازه‌رویان شد قرین از چشم پاک</p></div>
<div class="m2"><p>در گلستان است شبنم خوش‌نشین از چشم پاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برندارد شاخ نرگس از حجاب حسن او</p></div>
<div class="m2"><p>با کمال شوخ‌چشمی آستین از چشم پاک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جویبار از صافی سرچشمه می‌گیرد صفا</p></div>
<div class="m2"><p>می‌شود حسن نکویان شرمگین از چشم پاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترک شوخی کن که در بزم بهشت آیین گل</p></div>
<div class="m2"><p>شبنم افتاده شد بالانشین از چشم پاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌توان از پاک‌چشمی حسن را تسخیر کرد</p></div>
<div class="m2"><p>شد صدف گهواره در ثمین از چشم پاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تلخ شد بر شورچشمان خواب، تا بادام کرد</p></div>
<div class="m2"><p>بستر و بالین خود را شکرین از چشم پاک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌کند آب گهر را تلخ در کام صدف</p></div>
<div class="m2"><p>قطره اشکی که افتد بر زمین از چشم پاک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دایم از گرد یتیمی روزگارش تیره است</p></div>
<div class="m2"><p>نیست حسنی را که ابری در کمین از چشم پاک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می‌کند دندانه تیغ آتشین برق را</p></div>
<div class="m2"><p>خرمن حسنی که دارد خوشه‌چین از چشم پاک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شبنم از تردامنی می‌گیرد از گل بوسه‌ها</p></div>
<div class="m2"><p>بلبل بیچاره می‌بوسد زمین از چشم پاک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می‌نشیند زود نقش ساده‌لوحان بر مراد</p></div>
<div class="m2"><p>بوسه‌ها بر دست خوبان زد نگین از چشم پاک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هرکه چون آیینه صائب شست دست از آرزو</p></div>
<div class="m2"><p>در حریم حسن شد زانونشین از چشم پاک</p></div></div>