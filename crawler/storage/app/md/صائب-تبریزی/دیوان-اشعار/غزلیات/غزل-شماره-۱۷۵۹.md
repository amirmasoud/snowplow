---
title: >-
    غزل شمارهٔ ۱۷۵۹
---
# غزل شمارهٔ ۱۷۵۹

<div class="b" id="bn1"><div class="m1"><p>کدام زهره جبین بی نقاب گردیده است؟</p></div>
<div class="m2"><p>که آتش از عرق شرم، آب گردیده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نفس ز سینه مجروح ما دریغ مدار</p></div>
<div class="m2"><p>ترا که خون به جگر مشک ناب گردیده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر ز دل نکشم آه، نیست بیدردی</p></div>
<div class="m2"><p>که رشته ام گره از پیچ و تاب گردیده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز قرب، دیده من از وصال محروم است</p></div>
<div class="m2"><p>محیط، پرده چشم حباب گردیده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر ز اهل دلی، باش در سفر دایم</p></div>
<div class="m2"><p>که نقطه از حرکت صد کتاب گردیده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زبان شکر بود سبزه دل جویش</p></div>
<div class="m2"><p>دلی که از نگه گرم، آب گردیده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز سیر خانه آیینه چون به بزم آید</p></div>
<div class="m2"><p>گمان برند که در آفتاب گردیده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نفس ز سینه من زنگ بسته می آید</p></div>
<div class="m2"><p>ز بس که در دل من شکوه آب گردیده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه هاله است به دور قمر، که خوبی ماه</p></div>
<div class="m2"><p>به دور حسن تو پا در رکاب گردیده است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به ساقیی است سر و کار من که از رویش</p></div>
<div class="m2"><p>بط شراب، مکرر کباب گردیده است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز تخم سوخته ما نظر دریغ مدار</p></div>
<div class="m2"><p>ترا که آینه در دست، آب گردیده است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به پای خم چه ضرورست دردسر بردن؟</p></div>
<div class="m2"><p>مرا که آب ز تلخی شراب گردیده است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز ترکتاز حوادث مسلمی مطلب</p></div>
<div class="m2"><p>ز سیل، کعبه مکرر خراب گردیده است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کسی ز سوز دل ماست با خبر صائب</p></div>
<div class="m2"><p>کز آفتاب قیامت کباب گردیده است</p></div></div>