---
title: >-
    غزل شمارهٔ ۲۱۱۱
---
# غزل شمارهٔ ۲۱۱۱

<div class="b" id="bn1"><div class="m1"><p>جمعیت اسباب، حجاب نظر ماست</p></div>
<div class="m2"><p>هر کس که شود رهزن ما، راهبر ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ظاهر اگر شهپر پرواز نداریم</p></div>
<div class="m2"><p>افشاندن دست از دو جهان بال و پر ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با همت مردانه گذشتن ز دو عالم</p></div>
<div class="m2"><p>یک منزل کوتاه دل نوسفر ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر جا که شود چاشنی عشق پدیدار</p></div>
<div class="m2"><p>گردیده مورست، که تنگ شکر ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی نگه ماست به صد راه چو مژگان</p></div>
<div class="m2"><p>هر چند که آن پاک گهر در نظر ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرمایه عیشی که به آن فخر توان کرد</p></div>
<div class="m2"><p>خشتی است که از کوی تو در زیر سر ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر بر جگر کوه گذارند شود آب</p></div>
<div class="m2"><p>داغی که ز عشق تو نهان در جگر ماست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روشن شود از ریختن اشک، دل ما</p></div>
<div class="m2"><p>ابریم که روشنگر ما در جگر ماست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب کند از جلوه دل اهل نظر خون</p></div>
<div class="m2"><p>بر چهره هر لاله که داغ نظر ماست</p></div></div>