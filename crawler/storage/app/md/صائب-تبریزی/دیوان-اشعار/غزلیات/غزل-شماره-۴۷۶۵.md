---
title: >-
    غزل شمارهٔ ۴۷۶۵
---
# غزل شمارهٔ ۴۷۶۵

<div class="b" id="bn1"><div class="m1"><p>چشم حیران را حجابش دام می داند هنوز</p></div>
<div class="m2"><p>عاشق ناکام را خود کام می داند هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کیست حرف بوسه بررویش تواند فاش گفت ؟</p></div>
<div class="m2"><p>دیدن دزدیده راابرام می داندهنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوی شیر خام میآید ز تنگ شکرش</p></div>
<div class="m2"><p>بوسه را شیرین تراز دشنام می داند هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده قربانیان را چشم آن وحشی غزال</p></div>
<div class="m2"><p>در ره خود حلقه های دام می داند هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عالم از شوخی نگردیده است درچشمش سیاه</p></div>
<div class="m2"><p>وقت آزادی زمکتب شام می داندهنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چه می خواند زشوخی هافرامش می کند</p></div>
<div class="m2"><p>کی زبان نامه وپیغام میداندهنوز؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناله گرمی نه پیچیده است گوشش راچوگل</p></div>
<div class="m2"><p>عشق را بازیچه ایتام می داندهنوز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ساده لوح وطفل و بازیگوش وشوخ سرکش است</p></div>
<div class="m2"><p>قدرعاشق را کی آن خودکام می داند هنوز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پختگان را گرچه افکنده است آتش درجگر</p></div>
<div class="m2"><p>طبع صائب فکر خود را خام می داندهنوز</p></div></div>