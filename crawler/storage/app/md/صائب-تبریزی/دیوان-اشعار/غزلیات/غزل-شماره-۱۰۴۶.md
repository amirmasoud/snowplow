---
title: >-
    غزل شمارهٔ ۱۰۴۶
---
# غزل شمارهٔ ۱۰۴۶

<div class="b" id="bn1"><div class="m1"><p>زهر در ساغر مرا از سیر ماه و انجم است</p></div>
<div class="m2"><p>آسمان پر کواکب شیشه پر کژدم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرخ معذورست در افشردن دلهای خلق</p></div>
<div class="m2"><p>نخل ماتم تازه رو از آب چشم مردم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کار نادان می شود مشکل تر از تدبیر خویش</p></div>
<div class="m2"><p>از لگد محکم شود خاری که در زیر دم است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از علایق رشته ای تا هست، جان آزاد نیست</p></div>
<div class="m2"><p>تا رگ خامی بود در باده، محبوس خم است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خرد مشمر جرم را هر چند باشد اندکی</p></div>
<div class="m2"><p>کز بهشت آواره آدم از برای گندم است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوری ظاهر حجاب تشنه دیدار نیست</p></div>
<div class="m2"><p>قطره در هر جا که باشد متحد با قلزم است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از صفای سینه مستورست صائب داغ من</p></div>
<div class="m2"><p>پرتو خورشید تابان پرده دار انجم است</p></div></div>