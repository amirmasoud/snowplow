---
title: >-
    غزل شمارهٔ ۶۹۸۷
---
# غزل شمارهٔ ۶۹۸۷

<div class="b" id="bn1"><div class="m1"><p>خاک سیه روز را شمع شبستان تویی</p></div>
<div class="m2"><p>نه صدف چرخ را گوهر رخشان تویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جن و ملک وحش و طیر همه چه درین عرصه اند</p></div>
<div class="m2"><p>جمله تماشایی اند صاحب میدان تویی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چه بز زیر فلک هست طفیلی توست</p></div>
<div class="m2"><p>مایده عشق را نادره مهمان تویی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قد فلک ها چو دال از پی تعظیم توست</p></div>
<div class="m2"><p>با قد همچون الف بر سر جولان تویی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست به ملک وجود از تو گرامی تری</p></div>
<div class="m2"><p>آن که گرفته است جان از دم رحمان تویی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آینه رویان چرخ واله حسن تواند</p></div>
<div class="m2"><p>قافله مصر را یوسف کنعان تویی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درد جهان را علاج در کف تدبیر توست</p></div>
<div class="m2"><p>از نفس روح بخش عیسی دوران تویی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تاج کرامت تراست از همه عالم به فرق</p></div>
<div class="m2"><p>تختگه خاک را صاحب فرمان تویی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست به غیر از تو راه عالم توحید را</p></div>
<div class="m2"><p>در همه روی زمین شارع عرفان تویی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از تو به حق می رسند راهنوردان خاک</p></div>
<div class="m2"><p>راه نماینده جامد و حیوان تویی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غیر تو معمور نیست هیچ رباط دگر</p></div>
<div class="m2"><p>توشه رساننده اهل بیابان تویی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عالم خاکی ز توست صاحب حس و شعور</p></div>
<div class="m2"><p>ظلمت آفاق را چشمه حیوان تویی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نقش تو دل می برد از همه روحانیان</p></div>
<div class="m2"><p>چهره ابداع را زلف پریشان تویی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر چه درین چارسو هست دکان بی شمار</p></div>
<div class="m2"><p>جمله تهی مایه اند صاحب سامان تویی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از پی روزی توست گردش نه آسیا</p></div>
<div class="m2"><p>سلسله چرخ را سلسله جنبان تویی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر دم و مکری که هست جز دم جان بخش تو</p></div>
<div class="m2"><p>موج سراب فناست ابر درافشان تویی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر چه درین نه بساط رنگ پذیرد ز جان</p></div>
<div class="m2"><p>جمله خزف ریزه اند گوهر این کان تویی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نیست به مصر وجود جز تو عزیز دگر</p></div>
<div class="m2"><p>بی گنه و بی خطا بسته زندان تویی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دفتر ایجاد را جانوران دگر</p></div>
<div class="m2"><p>جمله غزل پرکنند بیت نمایان تویی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در قدح توست خون بر جگر توست داغ</p></div>
<div class="m2"><p>دامن این دشت را لاله نعمان تویی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شور تو در پرده است از نظر قاصران</p></div>
<div class="m2"><p>سفره افلاک را ورنه نمکدان تویی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چرخ به سر می رود این ره باریک را</p></div>
<div class="m2"><p>آن که به پا می رود در ره یزدان تویی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از خط فرمان شرع گر ننهی پا برون</p></div>
<div class="m2"><p>در نظر اهل دید صائب، انسان تویی</p></div></div>