---
title: >-
    غزل شمارهٔ ۱۵۹۵
---
# غزل شمارهٔ ۱۵۹۵

<div class="b" id="bn1"><div class="m1"><p>آه من مد رسایی است که پایانش نیست</p></div>
<div class="m2"><p>بخت من ابر سیاهی است که بارانش نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ابروی او مه عیدی است که دایم پیداست</p></div>
<div class="m2"><p>کاکل او شب قدری است که پایانش نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همت از مهر فراگیر که با یک ته نان</p></div>
<div class="m2"><p>ذره ای نیست که شرمنده احسانش نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم شبنم به شکر خواب بهاران رفته است</p></div>
<div class="m2"><p>خبر از پرتو خورشید درخشانش نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی گرم آن که ندارد ز بزرگان جهان</p></div>
<div class="m2"><p>آسمانی است که خورشید درخشانش نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از چه از کاهکشان طوق به گردن دارد؟</p></div>
<div class="m2"><p>چرخ اگر فاخته سرو خرامانش نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه برگ گلش از غنچه نمایان نشده است</p></div>
<div class="m2"><p>شبنمی را نتوان یافت که حیرانش نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لب لعل تو چها می کند از شیرینی</p></div>
<div class="m2"><p>وای بر شهدفروشی که مگس رانش نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرد آن کعبه مغرور که صد قافله دل</p></div>
<div class="m2"><p>خونبهای سر خاری ز مغیلانش نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه خبر از دل آشفته ما خواهد داشت؟</p></div>
<div class="m2"><p>آن که پروای سر زلف پریشانش نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چهره هر چند به رنگ ورق گل باشد</p></div>
<div class="m2"><p>بی خط سبز، سفالی است که ریحانش نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چشم شبنم ز هواداری گل روشن شد</p></div>
<div class="m2"><p>یوسف ماست که پروای عزیزانش نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رشته عمر ابد روی به کوتاهی کرد</p></div>
<div class="m2"><p>راه خوابیده زلف است که پایانش نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دست گستاخی من جرأت دیگر دارد</p></div>
<div class="m2"><p>گل ازان باغ نچینم که نگهبانش نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر چه بیماری ازان چشم سیه می بارد</p></div>
<div class="m2"><p>شیر را طاقت سر پنجه مژگانش نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چه قدر جلوه کند در دل تنگم صائب؟</p></div>
<div class="m2"><p>آن که میدان فلک در خور جولانش نیست</p></div></div>