---
title: >-
    غزل شمارهٔ ۵۴۳۴
---
# غزل شمارهٔ ۵۴۳۴

<div class="b" id="bn1"><div class="m1"><p>ما ز حرف پوچ مانند صدف لب بسته ایم</p></div>
<div class="m2"><p>چون گهر در خلوت روشندلی بنشسته ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تنگ نتواند زمین و آسمان بر ما گرفت</p></div>
<div class="m2"><p>چون شرار از تنگنای سنگ و آهن جسته ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اهل مجلس در شکست ما چه یکدل گشته اند؟</p></div>
<div class="m2"><p>ما نه مینای تهی، نه توبه نشکسته ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تاج اقبال سکندر این چنین لعلی نداشت</p></div>
<div class="m2"><p>پیش یأجوج سخن سد خموشی بسته ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در محیط عشق، خون نوح در جوش است و ما</p></div>
<div class="m2"><p>چون حباب از سادگی بر موج محمل بسته ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم ما از بس که ترسیده است از پیوند خلق</p></div>
<div class="m2"><p>ابروی پیوسته را از لوح خاطر شسته ایم!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یاد ما از خاطر احباب صائب چون رود؟</p></div>
<div class="m2"><p>در بیاض آفرینش مصرح برجسته ایم</p></div></div>