---
title: >-
    غزل شمارهٔ ۳۴۴۳
---
# غزل شمارهٔ ۳۴۴۳

<div class="b" id="bn1"><div class="m1"><p>دل ازان دورتر افتاده که واصل باشد</p></div>
<div class="m2"><p>یار وحشی تر ازان است که در دل باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چهره لیلی اگر پرده شرمی دارد</p></div>
<div class="m2"><p>چه ضرورست که زندانی محمل باشد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق در وصل همان پرده نشین ادب است</p></div>
<div class="m2"><p>موج در بحر مقید به سلاسل باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاقبت خال لب لعل تو از خط شد سبز</p></div>
<div class="m2"><p>ریشه در سنگ کند تخم چو قابل باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خون بیدرد شود قسمت خاک آخر کار</p></div>
<div class="m2"><p>خون ما نیست که گلگونه قاتل باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در مقامی که کماندار بود هوش ربا</p></div>
<div class="m2"><p>جای رحم است بر آن صید که غافل باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوری راه تو از خواب گران است، ارنه</p></div>
<div class="m2"><p>چشم بیدار دل آیینه منزل باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل دریایی ما تشنه طوفان بلاست</p></div>
<div class="m2"><p>لنگر کشتی ما دوری ساحل باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرگ سیماب سبکسیر بود آسایش</p></div>
<div class="m2"><p>دوزخ راهروان راحت منزل باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حرف باطل ز دل آن به که نیاید به زبان</p></div>
<div class="m2"><p>سحر خوب است نهان در چه بابل باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>داده ابر بود هر چه ز دریا یابی</p></div>
<div class="m2"><p>جود اهل کرم از کیسه سایل باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>استقامت بود از خاک نهادان مطلوب</p></div>
<div class="m2"><p>عیب دیوار در آن است که مایل باشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طمع جود ازین حبه ربایان غلط است</p></div>
<div class="m2"><p>خرج این طایفه از کیسه سایل باشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کاهلان خود گره کار پریشان خودند</p></div>
<div class="m2"><p>ورنه این راه نه راهی است که مشکل باشد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نیست ممکن رود پیچ و خم دوری ازو</p></div>
<div class="m2"><p>راه هرچند که پیوسته به منزل باشد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در نگشاده بود گوش بر آواز سؤال</p></div>
<div class="m2"><p>دست ارباب کرم بر لب سایل باشد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به ادب باش که در دفتر ایجاد جهان</p></div>
<div class="m2"><p>هیچ فردی نتوان یافت که باطل باشد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خود فروشان ز خریدار توانگر نشوند</p></div>
<div class="m2"><p>شمع را نعل در آتش پی محفل باشد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>می رسد روزی ناقص به تمامی صائب</p></div>
<div class="m2"><p>می خورد ماه دل خویش چو کامل باشد</p></div></div>