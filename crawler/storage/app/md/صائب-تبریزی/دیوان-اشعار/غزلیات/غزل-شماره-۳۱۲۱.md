---
title: >-
    غزل شمارهٔ ۳۱۲۱
---
# غزل شمارهٔ ۳۱۲۱

<div class="b" id="bn1"><div class="m1"><p>دل یکرنگ در غمخانه دنیا نمی‌باشد</p></div>
<div class="m2"><p>درین بستان گلی غیر از گل رعنا نمی‌باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمی‌اندیشد از زخم زبان هرکس که مجنون شد</p></div>
<div class="m2"><p>ز تیغ کوه کبک مست را پروا نمی‌باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز خود بیگانگان را لازم افتاده است تنهایی</p></div>
<div class="m2"><p>به خود هرکس که گردید آشنا تنها نمی‌باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز صید خود نگردد دام در زیر زمین غافل</p></div>
<div class="m2"><p>که آب و گل حجاب دیده بینا نمی‌باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لب ما خامش است از حرف خواهش چون لب ساغر</p></div>
<div class="m2"><p>وگرنه بخل در سرچشمه مینا نمی‌باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فروغ عاریت گاهی نهان، گه می‌شود پیدا</p></div>
<div class="m2"><p>من و نوری که نه پنهان و نه پیدا نمی‌باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به حفظ راز عاشق کوه طاقت برنمی‌آید</p></div>
<div class="m2"><p>شرار شوخ را آرام و در خارا نمی‌باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درین بستان‌سرا زان کاسه خود سرنگون دارم</p></div>
<div class="m2"><p>که جام سرنگون لاله بی‌صهبا نمی‌باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ملایم‌طینتان آسوده‌اند از سردی دوران</p></div>
<div class="m2"><p>که نخل موم را اندیشه از سرما نمی‌باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گوارا می‌شوند از وسعت مشرب گران‌جانان</p></div>
<div class="m2"><p>که کشتی‌های سنگین، بار بر دریا نمی‌باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ندارد انتهایی همچو مجنون سیر و دور ما</p></div>
<div class="m2"><p>که بی‌پرگار هرگز نقطه سودا نمی‌باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز سختی‌های دوران نیست پروا گوشه‌گیران را</p></div>
<div class="m2"><p>ز کوه قاف باری بر دل عنقا نمی‌باشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به چشم کم مبین زنهار آثار بزرگان را</p></div>
<div class="m2"><p>که پیرو را دلیلی به ز نقش پا نمی‌باشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز دامان وسایل دستگیری گر طمع داری</p></div>
<div class="m2"><p>درین وحشت‌سرا جز دامن شب‌ها نمی‌باشد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به ظاهر سرو را هرچند پا در گل بود صائب</p></div>
<div class="m2"><p>همان غافل ز سیر عالم بالا نمی‌باشد</p></div></div>