---
title: >-
    غزل شمارهٔ ۱۴۵۳
---
# غزل شمارهٔ ۱۴۵۳

<div class="b" id="bn1"><div class="m1"><p>نیست آرام در آن دل که هوس بسیارست</p></div>
<div class="m2"><p>شررآمیز بود شعله چو خس بسیارست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل بی وسوسه از گوشه نشینان مطلب</p></div>
<div class="m2"><p>که هوس در دل مرغان قفس بسیارست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر قدم اری و هر خار زبان ماری است</p></div>
<div class="m2"><p>آفت دامن صحرای هوس بسیارست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر تهیدستی ما خنده زدن بیدردی است</p></div>
<div class="m2"><p>به کنار آمدن از بحر ز خس بسیارست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باعث رنجش ما یک سخن سرد بس است</p></div>
<div class="m2"><p>دل چون آیینه را نیم نفس بسیارست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناقه و محمل و لیلی همه بی آرامند</p></div>
<div class="m2"><p>اثر شعله آواز جرس بسیارست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از تماشای گهر نعل در آتش دارد</p></div>
<div class="m2"><p>ورنه در سینه غواص نفس بسیارست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر جگرسوختگانی که درین انجمنند</p></div>
<div class="m2"><p>سینه گرم مرا حق نفس بسیارست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از بدان فیض محال است به نیکان نرسد</p></div>
<div class="m2"><p>حق بیداری دزدان به عسس بسیارست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در پی قافله ز افسانه غفلت صائب</p></div>
<div class="m2"><p>نتوان خفت که آواز جرس بسیارست</p></div></div>