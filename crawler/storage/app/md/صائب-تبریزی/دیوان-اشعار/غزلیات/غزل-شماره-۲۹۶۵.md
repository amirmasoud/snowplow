---
title: >-
    غزل شمارهٔ ۲۹۶۵
---
# غزل شمارهٔ ۲۹۶۵

<div class="b" id="bn1"><div class="m1"><p>بدن را در زمین هرگز روان پاک نگذارد</p></div>
<div class="m2"><p>که دام خویش را صیاد زیر خاک نگذارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی صد شد زفیض صبح تأثیر سرشک من</p></div>
<div class="m2"><p>که حق دانه پیش خود زمین پاک نگذارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهادت غافل از پاس ادب جان را نمی سازد</p></div>
<div class="m2"><p>سر عاشق قدم بر دیده فتراک نگذارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کجا خواهد لب گستاخ را راه سخن دادن؟</p></div>
<div class="m2"><p>شکر خندی که دلها را گریبان چاک نگذارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زصبح آفرینش بر نیاید آتشین رویی</p></div>
<div class="m2"><p>که در کوی تو چون خورشید سر بر خاک نگذارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به ور باده نتواند برآمد هر زبردستی</p></div>
<div class="m2"><p>که را دیدی که پشت دست پیش تاک نگذارد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گزیدم با هزاران آرزو عشقش، ندانستم</p></div>
<div class="m2"><p>که در دل آرزو آن شعله بیباک نگذارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا چون آب بود از جلوه مستانه اش روشن</p></div>
<div class="m2"><p>که قمری را به سرو آن قامت چالاک نگذارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طلسم شیشه نتواند برآمد با می زورین</p></div>
<div class="m2"><p>عبث سر در سر پرشور من افلاک نگذارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرفتم چون شرر در سینه خارا نهان گشتم</p></div>
<div class="m2"><p>مرا در پرده نور شعله ادراک نگذارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نماند از چشم تر در سینه صائب خرده رازم</p></div>
<div class="m2"><p>که ابر نوبهاران دانه ای در خاک نگذارد</p></div></div>