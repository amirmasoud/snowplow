---
title: >-
    غزل شمارهٔ ۳۱۹۰
---
# غزل شمارهٔ ۳۱۹۰

<div class="b" id="bn1"><div class="m1"><p>زقید جسم جانهای عزیز آسان برون آید</p></div>
<div class="m2"><p>به خوابی یوسف بی جرم از زندان برون آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگیرد رنگ دنیا هر که دارد جوهر مردی</p></div>
<div class="m2"><p>که تیغ تیز از دریای خون عریان برون آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خط شبرنگ می آید برون از لعل سیرابش</p></div>
<div class="m2"><p>به آیینی که خضر از چشمه حیوان برون آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیه گردید از عشق لباسی روزگار من</p></div>
<div class="m2"><p>خوشا روزی که این شمع از ته دامان برون آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بکش تا می توانی خشم عالمسوز را در دل</p></div>
<div class="m2"><p>کز این آتش به همواری گل و ریحان برون آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لب گورست از بی برگی قسمت لب نانش</p></div>
<div class="m2"><p>دهانی را که در صد سالگی دندان برون آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترا کز خاک برگ خرمی روید غنیمت دان</p></div>
<div class="m2"><p>که برگ عیش ما از غنچه پیکان برون آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نمی گردد به تلخ و شور رنگ جوهر ذاتی</p></div>
<div class="m2"><p>که از دریا نگارین پنجه مرجان برون آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر این است انصاف و مروت کاروانی را</p></div>
<div class="m2"><p>چه افتاده است یوسف از چه کنعان برون آید؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنان دستی است در مهمان نوازیها مرا صائب</p></div>
<div class="m2"><p>که چون سوفار، پیکان از دلم خندان برون آید</p></div></div>