---
title: >-
    غزل شمارهٔ ۴۸۸
---
# غزل شمارهٔ ۴۸۸

<div class="b" id="bn1"><div class="m1"><p>دست شستن ز بقا آب حیات است ترا</p></div>
<div class="m2"><p>خط کشیدن به جهان خط نجات است ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برگ از خویش بیفشان، ز ثمر دست بشوی</p></div>
<div class="m2"><p>ای که چون بید تمنای نبات است ترا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در جوانی به طواف حرم کعبه شدن</p></div>
<div class="m2"><p>شحنه باقی ایام حیات است ترا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر چه از خوشه پروین گذرد خرمن تو</p></div>
<div class="m2"><p>همچنان از دگران چشم زکات است ترا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا به منزل نرسی، بر تو نگردد روشن</p></div>
<div class="m2"><p>برکت ها که نهان در حرکات است ترا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لنگر از قافله ریگ روان می جویی</p></div>
<div class="m2"><p>ای که از زندگی امید ثبات است ترا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از ره یک جهتی روی مگردان صائب</p></div>
<div class="m2"><p>اگر امید رهایی ز جهات است ترا</p></div></div>