---
title: >-
    غزل شمارهٔ ۴۵۴۵
---
# غزل شمارهٔ ۴۵۴۵

<div class="b" id="bn1"><div class="m1"><p>فلک از ناله ما نرم نشد</p></div>
<div class="m2"><p>کجروان سخت کمان می باشند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با قلم راز ترا چون گویم</p></div>
<div class="m2"><p>دل سیاهان دو زبان می باشند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا دم باز پسین اهل نظر</p></div>
<div class="m2"><p>چون شرر دل نگران می باشند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیشتر خوش نفسان چون نافه</p></div>
<div class="m2"><p>در ته خرقه نهان می باشند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نافه دامن صحرای وجود</p></div>
<div class="m2"><p>ژنده پوشان جهان می باشند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شعله ها پیش لب ما صائب</p></div>
<div class="m2"><p>خامش و بسته زبان می باشند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صوفیان صاف روان می باشند</p></div>
<div class="m2"><p>چون صدف پاک دهان می باشند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در ته سبزه شمشیر بلا</p></div>
<div class="m2"><p>صاف چون آب خزان می باشند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روز آسوده و شبهای دراز</p></div>
<div class="m2"><p>همچو انجم نگران می باشند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گنج زیر قدم و بر در خلق</p></div>
<div class="m2"><p>شی ء لله زنان می باشند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گل خامی است سخن پردازی</p></div>
<div class="m2"><p>پختگان بسته زبان می باشند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بلبلانی که چمن مشتاقند</p></div>
<div class="m2"><p>در قفس بال زنان می باشند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا نسیم سحری جلوه گراست</p></div>
<div class="m2"><p>برگها بال فشان می باشند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون کف بحر سبک جولانان</p></div>
<div class="m2"><p>بر سر آب روان می باشند</p></div></div>