---
title: >-
    غزل شمارهٔ ۶۴۷۳
---
# غزل شمارهٔ ۶۴۷۳

<div class="b" id="bn1"><div class="m1"><p>ای بهار آفرینش گرده سیمای تو</p></div>
<div class="m2"><p>رشته جانها خس و خاشاک از دریای تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوی خون از دیده خورشید می سازد روان</p></div>
<div class="m2"><p>چهره خاک از فروغ لاله حمرای تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک تا گردن میان آب پنهان گشته است</p></div>
<div class="m2"><p>بس که می سوزد دلش از آتش سودای تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دامن بی طاقتان را خاک نتواند گرفت</p></div>
<div class="m2"><p>چون شرر از سنگ می آید برون جویای تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوهر دلها ز گلزار تو عقد شبنمی است</p></div>
<div class="m2"><p>رشته جانها رگ ابری است از دریای تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاک شد بیدار از خواب گران نیستی</p></div>
<div class="m2"><p>از عرق افشانی رخسار جان افزای تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تیغ اگر بارد به فرقش، همچنان آسوده است</p></div>
<div class="m2"><p>بس که حیران است صائب در رخ زیبای تو</p></div></div>