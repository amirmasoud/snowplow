---
title: >-
    غزل شمارهٔ ۲۱۳۹
---
# غزل شمارهٔ ۲۱۳۹

<div class="b" id="bn1"><div class="m1"><p>درد تو به دلهای سبکروح گران است</p></div>
<div class="m2"><p>تبخال بر آن لب گره رشته جان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در وصل دل از هجر فزون دل نگران است</p></div>
<div class="m2"><p>آوارگی تیر در آغوش کمان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر خاطر آزاده من دست گهربار</p></div>
<div class="m2"><p>چون دست تهی بر دل محتاج گران است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دل نبرد شوق وطن عزت غربت</p></div>
<div class="m2"><p>در صلب گهر آب همان قطره زنان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ایمن نتوان گشت ز برگشتگی بخت</p></div>
<div class="m2"><p>پیوست هدف را خطر از پشت کمان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در قافله ریگ روان پیش و پسی نیست</p></div>
<div class="m2"><p>پس مانده این مرحله از پیشروان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حیرت زدگان را نبود بهره ای از وصل</p></div>
<div class="m2"><p>در دامن گل دیده شبنم نگران است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در بال و پر عزم، مرا کوتهیی نیست</p></div>
<div class="m2"><p>سنگ ره من کاهلی همسفران است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیتابی ذرات جهان در طلب حق</p></div>
<div class="m2"><p>در شیشه ساعت سفر ریگ روان است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صائب نگه گرم در آن چشم سیه مست</p></div>
<div class="m2"><p>برقی است جهانسوز که در ابر نهان است</p></div></div>