---
title: >-
    غزل شمارهٔ ۶۵۸
---
# غزل شمارهٔ ۶۵۸

<div class="b" id="bn1"><div class="m1"><p>ز عمر باج ستاند می دو ساله ما</p></div>
<div class="m2"><p>به آفتاب شبیخون زند پیاله ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بی قراری ما دردسر کشد بالین</p></div>
<div class="m2"><p>شبی که دختر رز نیست در حباله ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز زیر بال ازان سر برون نمی آریم</p></div>
<div class="m2"><p>که رنگ گل نپرد از نسیم ناله ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشسته تا به کمر در میان خاکستر</p></div>
<div class="m2"><p>هنوز تشنه داغ است برگ لاله ما</p></div></div>