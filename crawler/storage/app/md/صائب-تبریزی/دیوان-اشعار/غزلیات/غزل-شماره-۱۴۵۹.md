---
title: >-
    غزل شمارهٔ ۱۴۵۹
---
# غزل شمارهٔ ۱۴۵۹

<div class="b" id="bn1"><div class="m1"><p>راز من نقل مجالس ز صفای گهرست</p></div>
<div class="m2"><p>همچو آیینه مرا هر چه بود در نظرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین چه حاصل که رخ یار مرا در نظرست؟</p></div>
<div class="m2"><p>چشم حیرت زدگان حلقه بیرون درست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>توشه برداشتن آیینه سبکباران نیست</p></div>
<div class="m2"><p>جگر خویش خورد هر که به ما همسفرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خموشی چمن آرا لب مرغان را بست</p></div>
<div class="m2"><p>سنگ دندان پریشان سخنان گوش کرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تکیه بر دوستی ساخته خلق مکن</p></div>
<div class="m2"><p>کاین بنایی است که ناساخته زیر و زبرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پنبه بر داغ دل هر که گذاری امروز</p></div>
<div class="m2"><p>تیغ خورشید قیامت چو برآید، سپرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که در چشمه سوزن سفر دریا کرد</p></div>
<div class="m2"><p>سفرش باد مبارک که حدیدالبصرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکرابی که ازان عیش رقیبان تلخ است</p></div>
<div class="m2"><p>به مذاق من دلسوخته شیر و شکرست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خار را تشنه جگر سر به بیابان ندهد</p></div>
<div class="m2"><p>هر که چون آبله در راه طلب دیده ورست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر چه موی کمر و رشته جان باریک است</p></div>
<div class="m2"><p>جاده حسن سلوک از همه باریکترست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب این آن غزل حضرت سعدی است که گفت</p></div>
<div class="m2"><p>عشقبازی دگر و نفس پرستی دگرست</p></div></div>