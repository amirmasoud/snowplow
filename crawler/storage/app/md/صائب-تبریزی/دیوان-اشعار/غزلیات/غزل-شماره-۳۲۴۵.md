---
title: >-
    غزل شمارهٔ ۳۲۴۵
---
# غزل شمارهٔ ۳۲۴۵

<div class="b" id="bn1"><div class="m1"><p>هرکه از گریه بیدرد اثر می طلبد</p></div>
<div class="m2"><p>همت از مردم کوتاه نظر می طلبد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>علم فتح بلند از سپر انداختن است</p></div>
<div class="m2"><p>ساده لوح آن که ز شمشیر ظفر می طلبد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست هر رشته سزاوار به گلدسته ما</p></div>
<div class="m2"><p>دل صد پاره ما موی کمر می طلبد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر شود هر سر مو پای طلب کافی نیست</p></div>
<div class="m2"><p>قطع این بادیه سامان دگر می طلبد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طمع از خوان بخیلان نکند قطع امید</p></div>
<div class="m2"><p>مور حرص از نی بی مغز شکر می طلبد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرچه صد بار گره شد به گلویش این آب</p></div>
<div class="m2"><p>صدف خام همان آب گهر می طلبد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یوسف آنجا که به زندان فراموشان است</p></div>
<div class="m2"><p>از دل گمشده ما که خبر می طلبد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ماه از هاله عبث می شود آغوش طراز</p></div>
<div class="m2"><p>سرو سیمین تو آغوش دگر می طلبد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاک صحرای قناعت جگرش سوخته است</p></div>
<div class="m2"><p>نه ز حرص است اگر مور شکر می طلبد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی شکستن به مقامی نرسد عزم درست</p></div>
<div class="m2"><p>کشتی ما مدد از موج خطر می طلبد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حاجت خود نکند عرض به هرکس صائب</p></div>
<div class="m2"><p>هرچه می بایدش از آه سحر می طلبد</p></div></div>