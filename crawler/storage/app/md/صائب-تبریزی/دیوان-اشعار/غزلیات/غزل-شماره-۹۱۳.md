---
title: >-
    غزل شمارهٔ ۹۱۳
---
# غزل شمارهٔ ۹۱۳

<div class="b" id="bn1"><div class="m1"><p>از اشک بلبل است رگ تلخی گلاب</p></div>
<div class="m2"><p>نادان کند حواله ز غفلت به آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از روی آتشین تو دل آب می شود</p></div>
<div class="m2"><p>از روی آفتاب شود چشم اگر پر آب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نتوان به هیچ وجه عنانش نگاه داشت</p></div>
<div class="m2"><p>حسنی که شد ز حلقه خط پای در رکاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نازکی به موی میانش نمی رسد</p></div>
<div class="m2"><p>هر چند زلف بیش کند مشق پیچ و تاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ابر از آفتاب توان فیض بیش برد</p></div>
<div class="m2"><p>ما می بریم لذت دیدار از نقاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از موجه سراب شود بیش تشنگی</p></div>
<div class="m2"><p>پروانه را خنک نشود دل ز ماهتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اشک ندامت است سیه کار را فزون</p></div>
<div class="m2"><p>در تیرگی زیاده بود ریزش سحاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>موی سفید ریشه طول امل بود</p></div>
<div class="m2"><p>در شوره زار بیش بود موجه سراب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آرام نیست آبله پایان شوق را</p></div>
<div class="m2"><p>مانع نگردد از حرکت آب را حباب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همت عطای خویش نگیرد ز سایلان</p></div>
<div class="m2"><p>یاقوت و لعل رنگ نبازد ز آفتاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در رد سایلند بزرگان زبان دراز</p></div>
<div class="m2"><p>باشد دلیر کوه گرانسنگ در جواب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر نیست نشأه سخن افزون ز می، چرا</p></div>
<div class="m2"><p>مستی شود زیاده ز گفتار در شراب؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در روی آفتاب توان بی حجاب دید</p></div>
<div class="m2"><p>نتوان دلیر روی ترا دید از حجاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بی مهری سپهر سیه دل به نیکوان</p></div>
<div class="m2"><p>روشن شد از گرفتگی ماه و آفتاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کامل عیار نیست به میزان دوستی</p></div>
<div class="m2"><p>هر کس که هم خمار نگردد به هم شراب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مویش به روزگار جوانی شود سفید</p></div>
<div class="m2"><p>چون نافه خون خویش کند هر که مشک ناب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>این روی شرمناک که من دیده ام ز یار</p></div>
<div class="m2"><p>صائب ز خط عجب که برون آید از حجاب</p></div></div>