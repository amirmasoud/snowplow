---
title: >-
    غزل شمارهٔ ۲۴۵۳
---
# غزل شمارهٔ ۲۴۵۳

<div class="b" id="bn1"><div class="m1"><p>آن رخ گلرنگ می باید زصهبا بشکفد</p></div>
<div class="m2"><p>سهل باشد غنچه گل بشکفد یا نشکفد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر به ظاهر سرکش افتاده است، اما در لباس</p></div>
<div class="m2"><p>یوسف مغرور بر روی زلیخا بشکفد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عیش این گلشن به خون دل چو گل آمیخته است</p></div>
<div class="m2"><p>غوطه در خون می زند هر کس که اینجا بشکفد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می ربایندش زدست یکدگر گلهای باغ</p></div>
<div class="m2"><p>چون نسیم صبحدم از هر که دلها بشکفد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حرص نوش از نیش گردیده است چشمش را حجاب</p></div>
<div class="m2"><p>کوته اندیشی که از لذات دنیا بشکفد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خنده های بی تأمل را ندامت در قفاست</p></div>
<div class="m2"><p>زود بی گل گردد آن گلبن که یکجا بشکفد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عیش چون شد عام، گردد پرده چشم حسود</p></div>
<div class="m2"><p>وای بر آن گل که در گلزار تنها بشکفد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر به خاکم بگذری ای نوبهار زندگی</p></div>
<div class="m2"><p>استخوانم همچو شاخ گل سراپا بشکفد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوشه گیرانند باغ دلگشا صائب مرا</p></div>
<div class="m2"><p>غنچه من از نسیم بال عنقا بشکفد</p></div></div>