---
title: >-
    غزل شمارهٔ ۵۵۳۷
---
# غزل شمارهٔ ۵۵۳۷

<div class="b" id="bn1"><div class="m1"><p>چه سازد گرد کلفت با دل شادی که من دارم</p></div>
<div class="m2"><p>ندارد پای در گل سروآزادی که من دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گریبان چاک سازد پرده گوش فلکها را</p></div>
<div class="m2"><p>از آن بیداد گر در سینه فریادی که من دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز حیرت چشم آهو را به آهو دام می سازد</p></div>
<div class="m2"><p>نمی خواهد کمند و دام صیادی که من دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرانی می کند چون تیشه بر من هر پر کاهی</p></div>
<div class="m2"><p>ز بس فرسوده گردیده است بنیادی که من دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به تردستی ز خارا نفش شیرین محو می سازد</p></div>
<div class="m2"><p>اگر تن در دهد در کار فرهادی که من دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلیمان را ز تاج سلطنت دلسرد می سازد</p></div>
<div class="m2"><p>ز سودا بر سر این چتر پریزادی که من دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به کوه قاف دارم از توکل پشت چون عنقا</p></div>
<div class="m2"><p>ندارد هیچ رهرو بر کمر زادی که من دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به من کفرست در شرع محبت تهمت نسیان</p></div>
<div class="m2"><p>که ذکر خیر احباب است اورادی که من دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که می گوید پری در دیده مردم نمی آید</p></div>
<div class="m2"><p>که دایم در نظر باشد پریزادی که من دارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به خون می شست از آب زندگانی خضر دست خود</p></div>
<div class="m2"><p>اگر می دید دست و تیغ جلادی که من دارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز وحشت می رود چون دود جغد از روزنم بیرون</p></div>
<div class="m2"><p>خراب افتاده است از بس غم آبادی که من دارم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از آن در غورگیها مویز انگور من صائب</p></div>
<div class="m2"><p>که بر نگرفت از من چشم استادی که من دارم</p></div></div>