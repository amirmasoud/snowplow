---
title: >-
    غزل شمارهٔ ۵۹۵۳
---
# غزل شمارهٔ ۵۹۵۳

<div class="b" id="bn1"><div class="m1"><p>حوصله وصل آن نگار ندارم</p></div>
<div class="m2"><p>دام به اندازه شکار ندارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوهر دریای بیکرانه عشقم</p></div>
<div class="m2"><p>در صدف آسمان قرار ندارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صحبت من در مذاق عشق گواراست</p></div>
<div class="m2"><p>باده روحانیم خمار ندارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکوه تراوش نمی کند ز زبانم</p></div>
<div class="m2"><p>آتش حل کرده ام شرار ندارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موجه بی دست و پای قلزم شوقم</p></div>
<div class="m2"><p>در سفر خویش اختیار ندارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست و دم سرد گشته است ز عالم</p></div>
<div class="m2"><p>کار چو صائب به هیچ کار ندارم</p></div></div>