---
title: >-
    غزل شمارهٔ ۱۰۱۸
---
# غزل شمارهٔ ۱۰۱۸

<div class="b" id="bn1"><div class="m1"><p>شاخ گل را از سراپا چهره تنها نازک است</p></div>
<div class="m2"><p>نازک اندامی که من دارم سراپا نازک است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آرزوی بوسه در دل خون شود عشاق را</p></div>
<div class="m2"><p>گر بگویم چهره او تا کجاها نازک است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بیاض گردنش پیداست خون عاشقان</p></div>
<div class="m2"><p>می شود بی پرده می، چندان که مینا نازک است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می توان صد رنگ گل در هر نگاهی دسته بست</p></div>
<div class="m2"><p>بس که رنگ چهره آن ماه سیما نازک است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جلوه پا در رکاب خط دو روزی بیش نیست</p></div>
<div class="m2"><p>غافل از فرصت مشو، وقت تماشا نازک است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می توانستم به خون خود لبش در خون کشید</p></div>
<div class="m2"><p>وقت تنگ است و حیا مهر لب و جا نازک است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخت می لرزم بر این زنجیر ازین دیوانه ها</p></div>
<div class="m2"><p>رشته زلف تو نازک، خوی دلها نازک است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در دل سنگین شیرین رخنه کردن مشکل است</p></div>
<div class="m2"><p>ورنه پیش تیشه فرهاد، خارا نازک است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون به دست خود نریزد خون خود را کوهکن؟</p></div>
<div class="m2"><p>کار دشوار است و طبع کارفرما نازک است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در گذر ای عقل از همراهی دیوانگان</p></div>
<div class="m2"><p>خار این صحرا است الماس و تو را پا نازک است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دامن پر سنگ می داند حباب باده را</p></div>
<div class="m2"><p>بس که از روشن روانی شیشه ما نازک است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رو به صحرا کرد اگر مجنون ز حی عذرش بجاست</p></div>
<div class="m2"><p>سایه لیلی گران و طبع سودا نازک است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>موشکافان را سراسر موی آتش دیده کرد</p></div>
<div class="m2"><p>گوشه ابروی او را بس که ایما نازک است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر نمی دارد دو رنگی مشرب یکرنگ عشق</p></div>
<div class="m2"><p>چون حباب از آب کشتی کن که دریا نازک است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نیست صائب موشکافی در بساط روزگار</p></div>
<div class="m2"><p>ورنه چون موی کمر اندیشه ما نازک است</p></div></div>