---
title: >-
    غزل شمارهٔ ۵۵۹۷
---
# غزل شمارهٔ ۵۵۹۷

<div class="b" id="bn1"><div class="m1"><p>به اشک از اطلس افلاک داغ شام می شویم</p></div>
<div class="m2"><p>به نور دل، سیاهی از رخ ایام می شویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز خاموشی بهاری در دل خود چون صدف دارم</p></div>
<div class="m2"><p>که در دریای تلخ از آب شیرین کام می شویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لباس کعبه شد از داغ عصیان پرده های دل</p></div>
<div class="m2"><p>من از غفلت به ظاهر جامه احرام می شویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به ابر نو بهاران نسبت من نیست بینایی</p></div>
<div class="m2"><p>که من از گریه مستانه خط جام می شویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هلاک من بود در جلوه مستانه ساقی</p></div>
<div class="m2"><p>به آب خضر دست از جان بی آرام می شویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز پیغام وصالش نیست بیجا گریه تلخم</p></div>
<div class="m2"><p>که قاصد را ز لب شیرینی پیغام می شویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به درد آرد دل صیاد را از لاغری صیدم</p></div>
<div class="m2"><p>غبار بال و پر از آب چشم دام می شویم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همان از طاعت من بوی کیفیت نمی آید</p></div>
<div class="m2"><p>اگر سجاده خود در می گلفام می شویم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ندارد مو شکافی حاصلی غیر از پریشانی</p></div>
<div class="m2"><p>ازین خواب پریشان، دیده خود کام می شویم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همان قدمی کشد چون سبزه از آب روان صائب</p></div>
<div class="m2"><p>ز دل چندان که نقش آرزوی خام می شویم</p></div></div>