---
title: >-
    غزل شمارهٔ ۵۲
---
# غزل شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>از عذار او بپوشان دیده امید را</p></div>
<div class="m2"><p>تکمه از شبنم مکن پیراهن خورشید را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بهشت عافیت افتادم از بی حاصلی</p></div>
<div class="m2"><p>شد حصاری بی بری از سنگ طفلان بید را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نور معنی می درخشد از جبین لفظ من</p></div>
<div class="m2"><p>بال خفاشی چه ستاری کند خورشید را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جام را بهر تنک ظرفان به دور انداخته است</p></div>
<div class="m2"><p>هیچ حقی نیست بر دریاکشان جمشید را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون دل شب می زنم صائب بر آهنگ فغان</p></div>
<div class="m2"><p>می کشانم بر زمین از آسمان ناهید را</p></div></div>