---
title: >-
    غزل شمارهٔ ۳۸۰۲
---
# غزل شمارهٔ ۳۸۰۲

<div class="b" id="bn1"><div class="m1"><p>چو تیغ او به جبین چین جوهر اندازد</p></div>
<div class="m2"><p>به نیم چشم زدن قحطی سر اندازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش آن که گربه سرش تیغ همچو موج زنند</p></div>
<div class="m2"><p>حباب وار کلاه از طرب براندازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بس که تشنه سرگشتگی است کشتی من</p></div>
<div class="m2"><p>همیشه در دل گرداب لنگر اندازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا مسوز که خواهی کباب شد ای چرخ</p></div>
<div class="m2"><p>سپند شوخ من آتش به مجمر اندازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نماند آینه ای بی غبار در عالم</p></div>
<div class="m2"><p>غبار خاطر من پرده گر بر اندازد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو شیر گیر شود می پرست، جا دارد</p></div>
<div class="m2"><p>اگر به دختر رز مهر مادر اندازد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لب پیاله شود غنچه از نهایت شوق</p></div>
<div class="m2"><p>اگر دهان تو عکسی به ساغر اندازد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بغیر خامه گوهرفشان من صائب</p></div>
<div class="m2"><p>که دیده مرغ ز منقار گوهر اندازد؟</p></div></div>