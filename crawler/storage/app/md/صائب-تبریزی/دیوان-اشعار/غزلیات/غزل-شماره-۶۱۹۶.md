---
title: >-
    غزل شمارهٔ ۶۱۹۶
---
# غزل شمارهٔ ۶۱۹۶

<div class="b" id="bn1"><div class="m1"><p>عنان مصلحت در عشق می باید رها کردن</p></div>
<div class="m2"><p>ندارد حاصلی در بحر بی ساحل شنا کردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندارد حلقه ای جز نعل وارون محمل لیلی</p></div>
<div class="m2"><p>نباید گوش ای مجنون به آواز درا کردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کمان کن قامت چون تیر را در قبضه طاعت</p></div>
<div class="m2"><p>کز این صیقل توان آیینه دل را جلا کردن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هم پیوسته گردد چون شرر آغاز و انجامت</p></div>
<div class="m2"><p>توانی خرده جان را به رغبت گر فدا کردن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کمان شکوه چون حلاج چند از دار زه سازی؟</p></div>
<div class="m2"><p>به حرف حق نمی بایست خود را آشنا کردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز شکر خواب گردد تنگ شکر جامه خوابت</p></div>
<div class="m2"><p>توانی بستر خود را اگر از بوریا کردن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز دستت بی طلب دادن به سایل چون نمی آید</p></div>
<div class="m2"><p>نباید روی خود را تلخ از ابرام گدا کردن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشو غافل ز پاس وقت اگر از دور بینایی</p></div>
<div class="m2"><p>که چون شد فوت، نتوان این عبادت را قضا کردن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نصیحت بشنو ای زاهد، فرود آ از سر منبر</p></div>
<div class="m2"><p>برای روی مردم پشت نتوان بر خدا کردن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به منبر بهر تسخیر خلایق حرف حق گفتن</p></div>
<div class="m2"><p>بود رفتن به بام کعبه در کسب هوا کردن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرو از ره برون صائب به حرف پوچ شیادان</p></div>
<div class="m2"><p>که بی مغزی است از هر چوب بی مغزی عصا کردن</p></div></div>