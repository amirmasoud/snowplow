---
title: >-
    غزل شمارهٔ ۴۲۸۶
---
# غزل شمارهٔ ۴۲۸۶

<div class="b" id="bn1"><div class="m1"><p>از خط نگاه پردگی دیده می شود</p></div>
<div class="m2"><p>مژگان شوخ، سبزه خوابیده می شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نتوان به پای سعی به کنه جهان رسید</p></div>
<div class="m2"><p>در خویش هر که گشت جهان دیده می شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا راست می کنی نفس خود، بساط عمر</p></div>
<div class="m2"><p>چون گردباد چیده وبرچیده می شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در پله کسی که نبیند به چشم کم</p></div>
<div class="m2"><p>سنگ سفال، گوهر سنجیده می شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم بدست لازم آرایش جهان</p></div>
<div class="m2"><p>طاوس خرج مردم نادیده می شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کس شود ز سنگ ملامت حریربیز</p></div>
<div class="m2"><p>چون سرمه روشنایی هر دیده می شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب جهان خاک مقام قرار نیست</p></div>
<div class="m2"><p>منشین برآن بساط که برچیده می شود</p></div></div>