---
title: >-
    غزل شمارهٔ ۲۸۶۲
---
# غزل شمارهٔ ۲۸۶۲

<div class="b" id="bn1"><div class="m1"><p>دل یاقوت را خون می کند لعل سخنگویت</p></div>
<div class="m2"><p>قلمها سینه چاک از خط ریحان تو می گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه اندام لطیف است این که گل با آن سبکروحی</p></div>
<div class="m2"><p>نفس دزدیده در چاک گریبان تو می گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تعجب نیست گر پروانه در بیرون در سوزد</p></div>
<div class="m2"><p>که شمع کشته روشن در شبستان تو می گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگرچه نیست ناز و نعمت حسن ترا پایان</p></div>
<div class="m2"><p>دل خود می خورد هر کس که مهمان تو می گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو کز هر جلوه ای بر هم زنی ملک دو عالم را</p></div>
<div class="m2"><p>کجا ویرانی ما گرد دامان تو می گردد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سواد چشمها از سرمه می گردید اگر روشن</p></div>
<div class="m2"><p>سخنگو سرمه از چشم سخندان تو می گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به فریاد آورد خونابه اش دریای آتش را</p></div>
<div class="m2"><p>چنین گر دل نمکسود از نمکدان تو می گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سلیمان وار اگر سازی هوا را زیردست خود</p></div>
<div class="m2"><p>فلک چون حلقه خاتم به فرمان تو می گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سخنهای تو صائب از حقیقت بهره ای دارد</p></div>
<div class="m2"><p>که عارف می شود هر کس به دیوان تو می گردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نگردد اشک در چشمی که حیران تو می گردد</p></div>
<div class="m2"><p>که آب استاده از سرو خرامان تو می گردد</p></div></div>