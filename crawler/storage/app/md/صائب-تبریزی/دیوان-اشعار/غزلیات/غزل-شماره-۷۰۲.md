---
title: >-
    غزل شمارهٔ ۷۰۲
---
# غزل شمارهٔ ۷۰۲

<div class="b" id="bn1"><div class="m1"><p>دریاب صبح فیض نسیم بهار را</p></div>
<div class="m2"><p>در دیده جا ده این نفس بی غبار را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با درد خود گذار من خاکسار را</p></div>
<div class="m2"><p>از روی گردباد میفشان غبار را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سهل است اگر به خواب شب قدر بگذرد</p></div>
<div class="m2"><p>در خواب مگذران دم صبح بهار را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از چشم او به یک نگه خشک قانعیم</p></div>
<div class="m2"><p>طی کرده ایم خواهش بوس و کنار را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیرون شدن ز عالم تکلیف، نعمتی است</p></div>
<div class="m2"><p>دیوانه از خدا طلبد نوبهار را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی اختیار خون ز لب زخم می چکد</p></div>
<div class="m2"><p>نتوان ز شکوه بست دهان خمار را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با روی سخت، خرده ز ممسک توان گرفت</p></div>
<div class="m2"><p>آهن ز صلب سنگ برآرد شرار را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گم می شوند خلق به زیر فلک تمام</p></div>
<div class="m2"><p>گوهر نبندد این صدف بی قرار را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دنبال صید، قطره بی جا نمی زنم</p></div>
<div class="m2"><p>من رام می کنم به رمیدن شکار را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شب زنده دار باش که شب روز روشن است</p></div>
<div class="m2"><p>در چشم باز، دیده شب زنده دار را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تاب شکنجه ات نبود گر ز چشم شور</p></div>
<div class="m2"><p>صائب نهفته دار دل داغدار را</p></div></div>