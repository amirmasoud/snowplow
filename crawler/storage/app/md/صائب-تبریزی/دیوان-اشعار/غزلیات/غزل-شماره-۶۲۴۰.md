---
title: >-
    غزل شمارهٔ ۶۲۴۰
---
# غزل شمارهٔ ۶۲۴۰

<div class="b" id="bn1"><div class="m1"><p>نه امروزست گرم از داغ سودای تو نان من</p></div>
<div class="m2"><p>نمک پرورده عشق است مغز استخوان من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمین تنگ میدان نیست جای گرم جولانان</p></div>
<div class="m2"><p>وگرنه توسن گردون بود در زیر ران من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به کوری خرج شد اشکی که پروردم به خون دل</p></div>
<div class="m2"><p>گلویی تر نشد چون شمع از آب روان من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برآمد بس که بی حاصل نهال من، عجب دارم</p></div>
<div class="m2"><p>که سر بالا کند چون بید مجنون باغبان من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز خواهش های الوان در ره سیل خطر بودم</p></div>
<div class="m2"><p>دل بی مدعا زین سیل شد دارالامان من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تواضع با فرودستان بود خوش از زبردستان</p></div>
<div class="m2"><p>وگرنه دور باش از زور خود دارد کمان من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرفتم گوشه بر امید گمنامی، ندانستم</p></div>
<div class="m2"><p>که کوه قاف چون عنقا شود سنگ نشان من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرانجانی نباشد پیشه من با خریداران</p></div>
<div class="m2"><p>به سیم قلب یوسف می خرند از کاروان من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز هزل و هجو دادم تو به صائب شوخ طبعان را</p></div>
<div class="m2"><p>دهان عالمی شد چون صدف پاک از دهان من</p></div></div>