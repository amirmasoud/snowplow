---
title: >-
    غزل شمارهٔ ۱۲۱۳
---
# غزل شمارهٔ ۱۲۱۳

<div class="b" id="bn1"><div class="m1"><p>محتسب از عاجزی دست سبوی باده بست</p></div>
<div class="m2"><p>بشکند دستی که دست مردم افتاده بست!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عکس خود را دید در می زاهد کوتاه بین</p></div>
<div class="m2"><p>تهمت آلوده دامانی به جام باده بست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آب خضر و باده روشن ز یک سرچشمه اند</p></div>
<div class="m2"><p>چشم بست از زندگی هرکس که چشم از باده بست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرو را خم کرد بار آشیان قمریان</p></div>
<div class="m2"><p>بار خود نتوان به دوش مردم آزاده بست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ذوق رسوایی گرفت اوجی که زهد مرده دل</p></div>
<div class="m2"><p>سنگ طفلان را به جای مهر در سجاده بست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همت از افتادگی بستان که حسن خیره چشم</p></div>
<div class="m2"><p>دست عالم را به زلف پیش پا افتاده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وصل لیلی از ره آوارگی نزدیک بود</p></div>
<div class="m2"><p>دشت در گمراهی مجنون کمر از جاده بست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از صراط المستقیم عشق پا بیرون منه</p></div>
<div class="m2"><p>شد بیابان مرگ صائب هر که چشم از جاده بست</p></div></div>