---
title: >-
    غزل شمارهٔ ۵۶۸۳
---
# غزل شمارهٔ ۵۶۸۳

<div class="b" id="bn1"><div class="m1"><p>دل به این عمر سبکسیر چرا شاد کنیم</p></div>
<div class="m2"><p>برسر ریگ روان خانه چه بنیاد کنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مهره گل پی بازیچه اطفال خوش است</p></div>
<div class="m2"><p>دل به بازیچه تعمیر چرا شاد کنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دشمن خانگی آدم خاکی است زمین</p></div>
<div class="m2"><p>خانه دشمن خود را ز چه آباد کنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نظر تربیت از عشق حقیقی داریم</p></div>
<div class="m2"><p>خوبی ساخته را حسن خداداد کنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل سخت تو و امید ترحم، هیهات</p></div>
<div class="m2"><p>طمع مرغ چه از بیضه فولاد کنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صید ما را نبود دغدغه آزادی</p></div>
<div class="m2"><p>خواب در کنج قفس روی به صیاد کنیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از ادب نیست به گرد سر زلفش گشتن</p></div>
<div class="m2"><p>جان فدا در قدم شانه شمشاد کنیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای خوش آن روز که از شهر صفاهان صائب</p></div>
<div class="m2"><p>دست توفیق به کف، روی به بغداد کنیم</p></div></div>