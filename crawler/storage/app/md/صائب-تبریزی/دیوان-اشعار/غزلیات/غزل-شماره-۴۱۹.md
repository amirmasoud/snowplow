---
title: >-
    غزل شمارهٔ ۴۱۹
---
# غزل شمارهٔ ۴۱۹

<div class="b" id="bn1"><div class="m1"><p>متاب از کشتن ما ای غزال شوخ گردن را</p></div>
<div class="m2"><p>که خون عاشقان باشد شفق این صبح روشن را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا از صافی مشرب ز خود دانند هر قومی</p></div>
<div class="m2"><p>که هر ظرفی به رنگ خود برآرد آب روشن را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نهادی چون قدم در راه از دلبستگی بگذر</p></div>
<div class="m2"><p>که می گردد گره در رشته سنگ راه، سوزن را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیفشان دانه احسان، ز برق فتنه ایمن شو</p></div>
<div class="m2"><p>که جز نقش پی موران حصاری نیست خرمن را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به زور عشق ازین زندان ظلمانی توان رستن</p></div>
<div class="m2"><p>که جز رستم برون می آورد از چاه بیژن را؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمی گردد حریف نفس سرکش عقل دریا دل</p></div>
<div class="m2"><p>چگونه زیر دست خویش سازد آب، روغن را؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مکن از دور گردون شکوه، ای جویای آزادی</p></div>
<div class="m2"><p>گشایش نیست بی سرگشتگی سنگ فلاخن را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به دشمن می گریزم از نفاق دوستان صائب</p></div>
<div class="m2"><p>که خار پا گوارا کرد بر من زخم سوزن را</p></div></div>