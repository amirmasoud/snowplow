---
title: >-
    غزل شمارهٔ ۱۴۱۴
---
# غزل شمارهٔ ۱۴۱۴

<div class="b" id="bn1"><div class="m1"><p>هیچ جوینده ندانست که جای تو کجاست</p></div>
<div class="m2"><p>آخر ای خانه برانداز سرای تو کجاست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزنی نیست که چون ذره نجستیم ترا</p></div>
<div class="m2"><p>هیچ روشن نشد ای شمع که جای تو کجاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر وفای تو فزون است ز اندازه ما</p></div>
<div class="m2"><p>آخر ای دلبر بیرحم، جفای تو کجاست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جنگ و بدخویی و بیرحمی و بی پروایی</p></div>
<div class="m2"><p>همه هستند به جا، صلح و صفای تو کجاست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای نسیم سحر، ای غنچه گشاینده دل</p></div>
<div class="m2"><p>وقت یاری است، دم عقده گشای تو کجاست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بوسه ای از لب شیرین تو ای تنگ شکر</p></div>
<div class="m2"><p>ما گرفتیم نخواهیم، عطای تو کجاست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب از گرد خجالت شده در خاک نهان</p></div>
<div class="m2"><p>موجه رحمت دریای عطای تو کجاست</p></div></div>