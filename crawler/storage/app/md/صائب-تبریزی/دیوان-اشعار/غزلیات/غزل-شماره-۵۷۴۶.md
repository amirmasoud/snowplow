---
title: >-
    غزل شمارهٔ ۵۷۴۶
---
# غزل شمارهٔ ۵۷۴۶

<div class="b" id="bn1"><div class="m1"><p>چگونه توبه ز می موسوم بهار کنم</p></div>
<div class="m2"><p>من آن نیم که پشیمانی اختیار کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش آن که روز شب خود ز روی یار کنم</p></div>
<div class="m2"><p>شبی به روز در آن زلف مشکبار کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مروت است که بوسد لب تو ساغر و من</p></div>
<div class="m2"><p>ز دور بوسه بر آن لعل آبدار کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شبی چو روز قیامت دراز می خواهم</p></div>
<div class="m2"><p>که بیحساب ترا یک به یک شمار کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خمار آن لب میگون به می نمی شکند</p></div>
<div class="m2"><p>چه لازم است که خون در دل خمار کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز آفتاب به چشم من آب می گردد</p></div>
<div class="m2"><p>چگونه خیره نظر بر جمال یار کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حجاب جوهر آیینه می شود صیقل</p></div>
<div class="m2"><p>چسان مطالعه آن خط غبار کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به بوسه تلخی هجران نمی رود ز مذاق</p></div>
<div class="m2"><p>به حرف چون ز لب یار اختصار کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طراوت تو ز آب مستغنی است</p></div>
<div class="m2"><p>دو چشم خود به چه امید اشکبار کنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فغان که نیست مرا عمر جاودان چون خضر</p></div>
<div class="m2"><p>که حلقه حلقه آن زلف را سرشمار کنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا غرض ز عنانداری حیات این است</p></div>
<div class="m2"><p>که در رکاب تو این نیم جان نثار کنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنان ربوده ز من شوق دیدن تو قرار</p></div>
<div class="m2"><p>که چشم بسته ز خلد برین گذار کنم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حذر ز صبح قیامت ندارد آن کافر</p></div>
<div class="m2"><p>سفید چشم چه در راه انتظار کنم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یسر نیامده ایام عمر، می خواهم</p></div>
<div class="m2"><p>شبی به روز در آن زلف مشکبار کنم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز چشم او به نگاهی ز دور خرسندم</p></div>
<div class="m2"><p>من آن نیم که غزال حرم شکار کنم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز اشک و آه نگردید مهربان صائب</p></div>
<div class="m2"><p>دگر چه با دل سنگین آن نگار کنم</p></div></div>