---
title: >-
    غزل شمارهٔ ۱۲۵۹
---
# غزل شمارهٔ ۱۲۵۹

<div class="b" id="bn1"><div class="m1"><p>توبه همصحبتان بر خاطر ما بار نیست</p></div>
<div class="m2"><p>راه امن بیخودی را کاروان در کار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاسه منصور خالی بود پرآوازه شد</p></div>
<div class="m2"><p>ورنه در میخانه وحدت کسی هشیار نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در پس دیوار محرومی گریبان می درم</p></div>
<div class="m2"><p>گر چه محرمتر ز من کس در حریم یار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که پیراهن به بدنامی درید آسوده شد</p></div>
<div class="m2"><p>بر زلیخا طعن ارباب ملامت بار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کهربا نتواند از دیوار جذب کاه کرد</p></div>
<div class="m2"><p>جذبه توفیق را با تن پرستان کار نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر نیاید صبر با مژگان خواب آلود او</p></div>
<div class="m2"><p>هیچ جوش مانع این تیغ لنگردار نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون زر بی سکه مردودست در بازار حشر</p></div>
<div class="m2"><p>هر دلی کز کاوش مژگان او افگار نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می توان از پرنیان ابر دیدن ماه را</p></div>
<div class="m2"><p>هر دو عالم روی او را مانع دیدار نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل عبث از سبحه و زنار منت می کشد</p></div>
<div class="m2"><p>این کهن اوراق را شیرازه ای در کار نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در خرابات مغان از عدل پیر می فروش</p></div>
<div class="m2"><p>گوشه ویرانه ای غیر از دل معمار نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گوهر خود را به خار و خس فشاندن مشکل است</p></div>
<div class="m2"><p>می کند خون گریه هر ابری که در گلزار نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پیش ما صائب که رطل خسروانی می زنیم</p></div>
<div class="m2"><p>گنج باد آورد غیر از ابر گوهر بار نیست</p></div></div>