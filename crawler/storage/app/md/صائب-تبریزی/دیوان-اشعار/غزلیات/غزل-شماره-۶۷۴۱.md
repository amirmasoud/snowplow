---
title: >-
    غزل شمارهٔ ۶۷۴۱
---
# غزل شمارهٔ ۶۷۴۱

<div class="b" id="bn1"><div class="m1"><p>مرا از عشق داغی بر دل افگار بایستی</p></div>
<div class="m2"><p>چراغی بر سر بالین این بیمار بایستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمی شد فرصت خاریدن سر باددستان را</p></div>
<div class="m2"><p>به مقدار خرابی گر مرا معمار بایستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غلط کردم نیفتادم به فکر ظاهرآرایی</p></div>
<div class="m2"><p>به جای عقل در سر طره دستار بایستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نباشد تکیه گاهی غنچه را بهتر ز شاخ گل</p></div>
<div class="m2"><p>سر منصور را بالین ز چوب دار بایستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جنون را می نماید چون فلاخن سنگ دست افشان</p></div>
<div class="m2"><p>دل دیوانه ما بر سر بازار بایستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به آبم راند غفلت، ورنه این عمر گرامی را</p></div>
<div class="m2"><p>که در گفتار کردم صرف، در کردار بایستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دهان مور را پر خاک دارد بی زبانی ها</p></div>
<div class="m2"><p>مرا تیغ زبان چون مار بی زنهار بایستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نشد از چشم شوخ او نگاهی قسمتم هرگز</p></div>
<div class="m2"><p>مرا هم بهره ای زین دولت بیدار بایستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی صد شد ز تسبیح ریایی عقده کارم</p></div>
<div class="m2"><p>مرا از خط ساغر بر کمر زنار بایستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به تار اشک صائب می کشیدم ریگ هامون را</p></div>
<div class="m2"><p>به قدر جرم اگر تسبیح استغفار بایستی</p></div></div>