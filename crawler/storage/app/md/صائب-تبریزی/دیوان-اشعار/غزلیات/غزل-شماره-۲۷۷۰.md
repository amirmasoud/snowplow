---
title: >-
    غزل شمارهٔ ۲۷۷۰
---
# غزل شمارهٔ ۲۷۷۰

<div class="b" id="bn1"><div class="m1"><p>با زمین گیری کمان آسمان نتوان کشید</p></div>
<div class="m2"><p>تا نگردی راست چون تیر، این کمان نتوان کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا نسازی نفس سرکش را چو عیسی زیر دست</p></div>
<div class="m2"><p>توسن افلاک را در زیر ران نتوان کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خودنمایی راست صد زخم نمایان در کمین</p></div>
<div class="m2"><p>در هوای تیر، گردن چون نشان نتوان کشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از ملامت روی نتوان تافتن در راه عشق</p></div>
<div class="m2"><p>پا به فریاد جرس از کاروان نتوان کشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زندگی با هوشیاری زیر گردون مشکل است</p></div>
<div class="m2"><p>تا نگردی مست این بار گران نتوان کشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می زنم بر کوچه دیوانگی در این بهار</p></div>
<div class="m2"><p>بیش ازین خجلت ز روی کودکان نتوان کشید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پنجه در سر پنجه شاهین اگر باید فکند</p></div>
<div class="m2"><p>دست خود چون بهله زان موی میان نتوان کشید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با تهیدستی توان مغلوب کردن نفس را</p></div>
<div class="m2"><p>اسب سرکش را به دست پر، عنان نتوان کشید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناله ام در دل گره شد، رفت تا بلبل زباغ</p></div>
<div class="m2"><p>بی هم آوازی نفس در گلستان نتوان کشید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برنیارد زهد خشک از تن به گردون رو(ح را)</p></div>
<div class="m2"><p>بر فلک خود را به پای نردبان نتوان کشید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر امید گنج نتوان دید روی ما را</p></div>
<div class="m2"><p>تلخرویی بهر گل از باغبان نتوان کشید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چند خواهی کرد صائب عشقبازی در لباس؟</p></div>
<div class="m2"><p>پرده بر رخساره ماه از کتان نتوان کشید</p></div></div>