---
title: >-
    غزل شمارهٔ ۲۹۲۴
---
# غزل شمارهٔ ۲۹۲۴

<div class="b" id="bn1"><div class="m1"><p>چه غم دیوانه ما از گزند آسمان دارد؟</p></div>
<div class="m2"><p>که نیل چشم زخم از جای سنگ کودکان دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکوه خامشی در ظرف گفت وگو نمی گنجد</p></div>
<div class="m2"><p>سخن هر چند سنجیده است هیبت را زیان دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به احوال من زیر و زبر گردیده می پرسی؟</p></div>
<div class="m2"><p>زلنگر کشتی دریایی من بادبان دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلاصی نیست ممکن زخمی آن تیغ مژگان را</p></div>
<div class="m2"><p>کجا پنهان شود صیدی که زخم خونچکان دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه افتاده است بلبل سر ز زیر پر برون آرد؟</p></div>
<div class="m2"><p>در آن گلشن که هر برگی زشبنم دیده بان دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عجب دارم کلید ناله من نشکند صائب</p></div>
<div class="m2"><p>که این گلزار قفل سختی از گوش گران دارد</p></div></div>