---
title: >-
    غزل شمارهٔ ۵۶۷۴
---
# غزل شمارهٔ ۵۶۷۴

<div class="b" id="bn1"><div class="m1"><p>گر چه از وعده احسان فلک پیر شدیم</p></div>
<div class="m2"><p>نعمتی بود که از هستی خود سیر شدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست زین سبز چمن کلفت ما امروزی</p></div>
<div class="m2"><p>غنچه بودیم درین باغ که دلگیر شدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حرص در آخر پیری کمر ما را بست</p></div>
<div class="m2"><p>با قد همچو کمان همسفر تیر شدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر چه از کوشش تدبیر نچیدیم گلی</p></div>
<div class="m2"><p>اینقدر بود که تسلیم به تقدیر شدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شست آن روز قضا دست ز آبادی ما</p></div>
<div class="m2"><p>که گرفتار به آب و گل تعمیر شدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>استخوان سوخته ای بود شب هستی ما</p></div>
<div class="m2"><p>دامن صبح گرفتیم طباشیر شدیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل خوش مشرب ما داشت جوان عالم را</p></div>
<div class="m2"><p>شد جهان پیر همان روز که ما پیر شدیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تن ندادیم به آغوش زلیخای هوس</p></div>
<div class="m2"><p>راضی از سلسله زلف به زنجیر شدیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می چکید از لب ما شیر ز طفلی چون صبح</p></div>
<div class="m2"><p>کزدم گرم چو خورشید جهانگیر شدیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تنگ شد شهر چون مجنون ز ملامت بر ما</p></div>
<div class="m2"><p>آخر از زخم زبان در دهن شیر شدیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سالها گرد سر سرو چو قمری گشتیم</p></div>
<div class="m2"><p>تا سزاوار به یک حلقه زنجیر شدیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ناز یوسف ز سیه رویی خود چون نکشیم</p></div>
<div class="m2"><p>ما که شایسته عفو از ره تقصیر شدیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صلح گردیم به یک نقش ز نقاش جهان</p></div>
<div class="m2"><p>محو یک چهره چو آیینه تصویر شدیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گره خاطر صیاد ز دام افزون است</p></div>
<div class="m2"><p>منت ما بود درین بادیه نخجیر شدیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر چه اول مس ما قابل اکسیر نبود</p></div>
<div class="m2"><p>آنقدر سعی نمودیم که اکسیر شدیم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جز ندامت چه بود کوشش ما را حاصل</p></div>
<div class="m2"><p>ما که در صبحدم آماده شبگیر شدیم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صائب آن طفل یتیمیم در آغوش جهان</p></div>
<div class="m2"><p>که به دریوزه به صد خانه پی شیر شدیم</p></div></div>