---
title: >-
    غزل شمارهٔ ۳۳۹۰
---
# غزل شمارهٔ ۳۳۹۰

<div class="b" id="bn1"><div class="m1"><p>بر ز نخدان تو هرکس که نگاه اندازد</p></div>
<div class="m2"><p>گر بود خضر، دل خویش به چاه اندازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرد بر دامن دریای کرم ننشیند</p></div>
<div class="m2"><p>ابر اگر سایه رحمت به گیاه اندازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقده مشکل ما را به نسیمی دریاب</p></div>
<div class="m2"><p>تا به کی آه به اشک، اشک به آه اندازد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در گذر از سر نظاره آن قد بلند</p></div>
<div class="m2"><p>کاین تماشا ز سر چرخ کلاه اندازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دور باش مژه از هر دو طرف استاده است</p></div>
<div class="m2"><p>زهره کیست بر آن چشم نگاه اندازد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکوه در دل گره و جرأت گفتارم نیست</p></div>
<div class="m2"><p>مگر این سلسله را اشک به راه اندازد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب از درد تغافل دل اگر خون گردد</p></div>
<div class="m2"><p>به ازان است کسی رو به نگاه اندازد</p></div></div>