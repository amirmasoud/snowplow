---
title: >-
    غزل شمارهٔ ۵۸۸۰
---
# غزل شمارهٔ ۵۸۸۰

<div class="b" id="bn1"><div class="m1"><p>ما همچو خار سلسله جنبان آتشیم</p></div>
<div class="m2"><p>سنگ فسان تیزی مژگان آتشیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا تازه ایم نبض بهاریم همچو خار</p></div>
<div class="m2"><p>چون خشک می شویم رگ جان آتشیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از درد و داغ عشق نداریم شکوه ای</p></div>
<div class="m2"><p>ما چون شرار طفل دبستان آتشیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا غنچه ایم پرده رازیم عشق را</p></div>
<div class="m2"><p>چون باز می شویم گلستان آتشیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بال پری ز غیرت ما می تپد به خاک</p></div>
<div class="m2"><p>پروانه وار چتر سلیمان آتشیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>افسرده خاطریم چو پروانه روزها</p></div>
<div class="m2"><p>شبها چو شمع دست و گریبان آتشیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از اشک گرم آب حیاتیم خاک را</p></div>
<div class="m2"><p>از آه سرد سنبل و ریحان آتشیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاشاک ما به عشق جهانسوز بار نیست</p></div>
<div class="m2"><p>از پیچ و تاب، زلف پریشان آتشیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از روی گرم ماست دل لاله سنگداغ</p></div>
<div class="m2"><p>هر چند فرد باطل دیوان آتشیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از ما اثر مجوی که چون دانه سپند</p></div>
<div class="m2"><p>خرمن به باد داده جولان آتشیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون گل ز دامن تر ما آب می چکد</p></div>
<div class="m2"><p>عمری است گر چه در ته دامان آتشیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حیف است حیف سوخته گردد کباب ما</p></div>
<div class="m2"><p>کز اشک لاله گون نمک خوان آتشیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ما را چو داغ لاله امید نجات نیست</p></div>
<div class="m2"><p>پای به خواب رفته دامان آتشیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زین خاکدان به عالم بالاست چشم ما</p></div>
<div class="m2"><p>چون دود، گردباد بیابان آتشیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از درد و داغ عشق بود آب و تاب ما</p></div>
<div class="m2"><p>ما همچو شمع زنده به احسان آتشیم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در دست ماست نبض دل داغدار عشق</p></div>
<div class="m2"><p>چون رشته های شمع رگ جان آتشیم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پروانه ها ز ما به حیات ابد رسند</p></div>
<div class="m2"><p>چون شمع، خضر چشمه حیوان آتشیم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کی سوختن بر آتش ما آب می زند؟</p></div>
<div class="m2"><p>صائب چنین که تشنه طوفان آتشیم</p></div></div>