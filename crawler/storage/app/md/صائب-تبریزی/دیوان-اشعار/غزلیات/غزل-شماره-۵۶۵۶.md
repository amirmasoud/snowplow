---
title: >-
    غزل شمارهٔ ۵۶۵۶
---
# غزل شمارهٔ ۵۶۵۶

<div class="b" id="bn1"><div class="m1"><p>شکوه از کجروی طالع وارون چه کنم؟</p></div>
<div class="m2"><p>اژدها می شود این مار ز افسون چه کنم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم از زخم زبان کاغذ سوزن زده شد</p></div>
<div class="m2"><p>همچو عیسی نکشم رخت به گردون چه کنم؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در و دیوار به وحشت زدگان زندان است</p></div>
<div class="m2"><p>ننهم روی خود از شهر به هامون چه کنم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفت صبحت خلق از دد و دام افزون است</p></div>
<div class="m2"><p>نروم در دهن شیر چو مجنون چه کنم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هست در گوشه نشینی دل جمعی گرهست</p></div>
<div class="m2"><p>در خم می نگریزم چو فلاطون چه کنم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من که داغ است گران بر سر سودا زده ام</p></div>
<div class="m2"><p>چتر کیخسروی و تاج فریدون چه کنم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم سخت فلک از آب مروت خالی است</p></div>
<div class="m2"><p>طمع باده ازین کاسه وارون چه کنم؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دردها کم شود از گفتن و دردی که مراست</p></div>
<div class="m2"><p>از تهی کردن دل می شود افزون چه کنم؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بود تا از دل صد پاره اثر، کردم صبر</p></div>
<div class="m2"><p>رفت یکبارگی از دست دل اکنون چه کنم؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من که از خون جگر نشأه می می یابم</p></div>
<div class="m2"><p>لاله گون روی خود از باده گلگون چه کنم؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سازگاران جهان را دل ازو پر خون است</p></div>
<div class="m2"><p>من به این طالع ناساز به گردون چه کنم؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من گرفتم به گرستن شودم دیده تهی</p></div>
<div class="m2"><p>با لب پر سخن وبا دل پر خون چه کنم؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من نه آنم که تراوش کند از من گله ای</p></div>
<div class="m2"><p>می دهد خون جگر رنگ به بیرون چه کنم؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نتوان ساخت تهی دل چو درین عالم تنگ</p></div>
<div class="m2"><p>دست صائب ننهم بر دل پر خون چه کنم؟</p></div></div>