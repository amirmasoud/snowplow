---
title: >-
    غزل شمارهٔ ۲۰۳۸
---
# غزل شمارهٔ ۲۰۳۸

<div class="b" id="bn1"><div class="m1"><p>حسن ترا به نقش و نگار احتیاج نیست</p></div>
<div class="m2"><p>روی شکفته را به بهار احتیاج نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندیشه وصال ندارند عاشقان</p></div>
<div class="m2"><p>از دست رفته را به کنار احتیاج نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کان نمک کجا به نمکدان برد نیاز؟</p></div>
<div class="m2"><p>شور مرا به خنده یار احتیاج نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما صلح کرده ایم به دل از جهان گل</p></div>
<div class="m2"><p>هر جا که مهره هست به مار احتیاج نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوش سخن شنو نکشد رنج گوشمال</p></div>
<div class="m2"><p>دلهای نرم را به فشار احتیاج نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از مشرب وسیع به جنت فتاده ایم</p></div>
<div class="m2"><p>این نخل موم را به بهار احتیاج نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک آینه است شش جهت از نور روی تو</p></div>
<div class="m2"><p>حسن ترا به آینه دار احتیاج نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از بس هوای کشور مازندان ترست</p></div>
<div class="m2"><p>مخمور را به آب خمار احتیاج نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از جوش صید، پر نتواند گشود تیر</p></div>
<div class="m2"><p>اینجا کمین برای شکار احتیاج نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رنگینی کلام، گلستان من بس است</p></div>
<div class="m2"><p>صائب مرا به باغ و بهار احتیاج نیست</p></div></div>