---
title: >-
    غزل شمارهٔ ۶۲۳۲
---
# غزل شمارهٔ ۶۲۳۲

<div class="b" id="bn1"><div class="m1"><p>به یک خمیازه گل طی شد ایام بهار من</p></div>
<div class="m2"><p>به یک شبنم نشست از جوش خون لاله زار من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب امیدواری می شمردم خط مشکین را</p></div>
<div class="m2"><p>ندانستم کز او خواهد سیه شد روزگار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنین کز شوق دامان تو خود را جمع می سازد</p></div>
<div class="m2"><p>عجب دارم پریشان گردد از صرصر غبار من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه آن صیدم که عشق از فکر من غافل تواند شد</p></div>
<div class="m2"><p>نمک در چشم ریزد دام را ذوق شکار من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگو تا آستین از دیده خونبار بردارم</p></div>
<div class="m2"><p>غباری هست اگر بر خاطرت از رهگذار من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشاندی از فریب وعده صد بارم به خاک و خون</p></div>
<div class="m2"><p>نکردی شرم یک بار از دل امیدوار من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نفس در خانه آیینه اینجا راست می کردی</p></div>
<div class="m2"><p>اگر آگاه می گشتی ز درد انتظار من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حصار عافیت شد طوق قمری سروبستان را</p></div>
<div class="m2"><p>مکن پهلو تهی ای سرو بالا از کنار من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا زین خودپرستان نیست صائب چشم همراهی</p></div>
<div class="m2"><p>مگر دستی گذارد بیخودی در زیر بار من</p></div></div>