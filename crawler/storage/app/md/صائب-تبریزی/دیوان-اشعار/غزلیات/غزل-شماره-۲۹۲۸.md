---
title: >-
    غزل شمارهٔ ۲۹۲۸
---
# غزل شمارهٔ ۲۹۲۸

<div class="b" id="bn1"><div class="m1"><p>مه ناشسته رو کی رتبه دلدار من دارد؟</p></div>
<div class="m2"><p>که با آن تازگی گل حکم تقویم کهن دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا آیینه رویی مهر حیرت بر دهن دارد</p></div>
<div class="m2"><p>خوشا طوطی که از آیینه میدان سخن دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لب لعلی که می دارد دریغ از من تبسم را</p></div>
<div class="m2"><p>زخط در چاشنی صد طوطی شکرشکن دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندارد دیده دریانوردان نور یعقوبی</p></div>
<div class="m2"><p>وگرنه هر حبابی یوسفی در پیرهن دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درین میخانه پرشور هر جامی که می بینم</p></div>
<div class="m2"><p>زیاد لعل سیراب تو آتش در دهن دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قفس کم نیست از گلزار اگر باشد فراموشی</p></div>
<div class="m2"><p>مرا دلگیر از غربت همین یاد وطن دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درین محفل چراغی بر نسیم صبح می خندد</p></div>
<div class="m2"><p>که از فانوس با خود خلوتی در انجمن دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تن آسانی نگیرد دامن دلهای روشن را</p></div>
<div class="m2"><p>که شبنم نعل در آتش زگلهای چمن دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ندارد استخوان خودپرستان مغز آگاهی</p></div>
<div class="m2"><p>جهان پوچ را گر هست مغزی، خودشکن دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگرچه چون قلم صد سینه چاکش هست هر جانب</p></div>
<div class="m2"><p>سخن نازی که دارد با من عاشق سخن دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خبر زان گوهر نایاب هر موجی کجا یابد؟</p></div>
<div class="m2"><p>که از گرداب، دریا مهر حیرت بر دهند ارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرنجان از نقاب ای سنگدل آن روی نازک را</p></div>
<div class="m2"><p>که یوسف را لطافت بی نیاز از پیرهن دارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دهان می کند خوش از خمار آن لب میگون</p></div>
<div class="m2"><p>عقیقی کز شفق خورشید تابان در دهن دارد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کجا زیر نگین آرد دل پرخون من صائب؟</p></div>
<div class="m2"><p>سهیلی را که صد خونین جگر همچون یمن دارد</p></div></div>