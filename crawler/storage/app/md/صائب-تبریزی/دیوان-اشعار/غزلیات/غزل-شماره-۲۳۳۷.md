---
title: >-
    غزل شمارهٔ ۲۳۳۷
---
# غزل شمارهٔ ۲۳۳۷

<div class="b" id="bn1"><div class="m1"><p>می خورد با دیگران مستانه بر ما بگذرد</p></div>
<div class="m2"><p>در فرنگ این ظلم و این بیداد حاشا بگذرد!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دل هر نقطه خالش سواد اعظمی است</p></div>
<div class="m2"><p>کیست بر مجموعه حسنش سراپا بگذرد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آب می پیچد زحیرانی به دست و پای سرو</p></div>
<div class="m2"><p>از گلستانی که آن شمشاد بالا بگذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وحشیان را دست و تیغش چشم قربانی کند</p></div>
<div class="m2"><p>چون به عزم صید بر دامان صحرا بگذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سنبل و ریحان توان از دود آهش دسته بست</p></div>
<div class="m2"><p>در دل هر کس که آن زلف چلیپا بگذرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرو بالایی که من زنجیری اویم چو آب</p></div>
<div class="m2"><p>الامان خیزد ز رفتارش به هر جا بگذرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لیلی از اندیشه مجنون به خود لرزد چوبید</p></div>
<div class="m2"><p>گر نسیمی تند بر دامان صحرا بگذرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا قیامت بوی خون آید زدیوار و درش</p></div>
<div class="m2"><p>کوچه ای کز وی دل صد پاره ما بگذرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می تواند کرد سیر سینه پر داغ من</p></div>
<div class="m2"><p>هر که از دریای آتش بی محابا بگذرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شور صدزنجیر فیل مست می آید به گوش</p></div>
<div class="m2"><p>هر کجا مجنون ما زنجیر در پا بگذرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کوه و صحرا در سفر بر یکدگر سبقت کنند</p></div>
<div class="m2"><p>گر نسیم شوق او بر کوه و صحرا بگذرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می شود از عقل و هوش و دین و دانش پاکباز</p></div>
<div class="m2"><p>هر که را از پیش چشم آن پاک سیما بگذرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>باعث رقت شود آزار ما نازکدلان</p></div>
<div class="m2"><p>سنگ با چشم پر آب از شیشه ما بگذرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نشأه می با جوانی آب یک سرچشمه است</p></div>
<div class="m2"><p>از جوانی بگذرد هر کس زصهبا بگذرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در خراباتی که آید سینه گرمم به جوش</p></div>
<div class="m2"><p>از سرخم جوش می یک نیزه بالا بگذرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ترک فانی بهر باقی در شمار زهد نیست</p></div>
<div class="m2"><p>اوست زاهد کز سر دنیا و عقبی بگذرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نقش پای رفتگان آیینه دار عبرت است</p></div>
<div class="m2"><p>وای بر آن کس که غافل زین تماشا بگذرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کشتی غافل خطرها دارد از موج سراب</p></div>
<div class="m2"><p>تر نگردد پای عارف گر زدریا بگذرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون تواند دیده صائب گذشت از روی خوب؟</p></div>
<div class="m2"><p>از سر خورشید نتوانست عیسی بگذرد</p></div></div>