---
title: >-
    غزل شمارهٔ ۱۴۶۸
---
# غزل شمارهٔ ۱۴۶۸

<div class="b" id="bn1"><div class="m1"><p>معنی از لفظ سبکروح فلک پروازست</p></div>
<div class="m2"><p>لفظ پرداخته بال وپر این شهبازست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق بالاتر از آن است که در وصف آید</p></div>
<div class="m2"><p>چرخ کبکی است که در پنجه این شهبازست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خامشی پرده اسرار حقیقت نشود</p></div>
<div class="m2"><p>مشک هر چند که در پرده بود غمازست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می توان خط برون نامده را خواند چو آب</p></div>
<div class="m2"><p>بس که آیینه رخسار تو خوش پردازست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خط مشکین تو در دایره سبزخطان</p></div>
<div class="m2"><p>چون شب قدر ز شبهای دگر ممتازست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خار را قرب گل از خوی بد خود نرهاند</p></div>
<div class="m2"><p>هر که ناساز بود، در همه جا ناسازست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مکش از بیخبری گردن دعوی چون شمع</p></div>
<div class="m2"><p>که گریبان قبای تو دهان گازست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قدم سعی تو در دامن تن پیچیده است</p></div>
<div class="m2"><p>ورنه افلاک ترا اطلس پای اندازست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عشق کوتاه کند زمزمه دعوی را</p></div>
<div class="m2"><p>خانمان سوختگی سرمه این آوازست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیش جمعی که شناسند خطا را ز صواب</p></div>
<div class="m2"><p>فکر صائب ز خیالات دگر ممتازست</p></div></div>