---
title: >-
    غزل شمارهٔ ۴۴۱۹
---
# غزل شمارهٔ ۴۴۱۹

<div class="b" id="bn1"><div class="m1"><p>در کودکی از جبهه من عشق عیان بود</p></div>
<div class="m2"><p>گهواره ز بیتابی من تخت روان بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انگشت نما بود دل سوخته من</p></div>
<div class="m2"><p>آن روز که از عشق نه نام ونه نشان بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نابسته به ظاهر کمر هستی موهوم</p></div>
<div class="m2"><p>در رشته جان پیچ وخم موی میان بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خاک نشینان عدم بود خرابات</p></div>
<div class="m2"><p>روزی که دل ازجمله خونابه کشان بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیدارشد ازناله من چرخ گرانخواب</p></div>
<div class="m2"><p>بیتابی من سلسله جنبان زمان بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>. . . جگر لاله ستان بود نمکسود</p></div>
<div class="m2"><p>تا شور جنونم نمک خوان جهان بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن درد نصیبم که در ایام بهاران</p></div>
<div class="m2"><p>رنگم گل روی سبد فصل خزان بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از من به چه تقصیر قدم باز گرفتی</p></div>
<div class="m2"><p>رفتار تو در خانه دل آب روان بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیرابم ازین وادی تفسیده برون برد</p></div>
<div class="m2"><p>از صبر عقیقی که مرا زیر زبان بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون دست سبو زیر سر از فکر تو شد خشک</p></div>
<div class="m2"><p>دستی که براوبوسه ناکرده گران بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از خون شهیدان تو دایم جگر خاک</p></div>
<div class="m2"><p>رنگین تر و شادابتر از لاله ستان بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صائب نشد از وصل تسلی دل خونین</p></div>
<div class="m2"><p>در دامن گل شبنم من دل نگران بود</p></div></div>