---
title: >-
    غزل شمارهٔ ۶۴۱۳
---
# غزل شمارهٔ ۶۴۱۳

<div class="b" id="bn1"><div class="m1"><p>ما از صفای سینه بی کینه بر زمین</p></div>
<div class="m2"><p>مالیده ایم چهره آیینه بر زمین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از راه خلق مطلب ما خار چیدن است</p></div>
<div class="m2"><p>گر می کشیم خرقه پشمینه بر زمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن خودپرست اگر نه گرفتار خود شده است</p></div>
<div class="m2"><p>از دست چون نمی نهد آیینه بر زمین؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می گشتمش چو کعبه به اخلاص گردسر</p></div>
<div class="m2"><p>می یافتم اگر دل بی کینه بر زمین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در وصف خط او نرسد پای کلک من</p></div>
<div class="m2"><p>چون پای کودکان شب آدینه بر زمین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عالم فروز گردد اگر آه گرم من</p></div>
<div class="m2"><p>دریا نهد چو تشنه لبان سینه بر زمین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب درین زمانه ز پیران زنده دل</p></div>
<div class="m2"><p>یک تن نمانده جز می دیرینه بر زمین</p></div></div>