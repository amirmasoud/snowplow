---
title: >-
    غزل شمارهٔ ۴۱۸۴
---
# غزل شمارهٔ ۴۱۸۴

<div class="b" id="bn1"><div class="m1"><p>از دور باش کی حذر اغیار می‌کند</p></div>
<div class="m2"><p>گلچین کجا ملاحظه از خار می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیراب اگر شود جگر تشنه از سراب</p></div>
<div class="m2"><p>کوثر علاج تشنه دیدار می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هموار می‌کند به خود این سنگلاخ را</p></div>
<div class="m2"><p>از خلق هرکه روی به دیوار می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مژگان اشکبار شود موی بر تنش</p></div>
<div class="m2"><p>در هر دلی که ناله من کار می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیری که از سیاه‌دلی می‌کند خضاب</p></div>
<div class="m2"><p>صبح امید خویش شب تار می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیوانه را ز سنگ ملامت هراس نیست</p></div>
<div class="m2"><p>این کبک مست خنده به کهسار می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ایمن ز دور باش بود دیده‌های پاک</p></div>
<div class="m2"><p>آیینه را که منع ز دیدار می‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دستی که شد بریده ز دامان اختیار</p></div>
<div class="m2"><p>چون بهله دست در کمر یار می‌کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زان چشم نیم مست نصیب دل من است</p></div>
<div class="m2"><p>بیماریی که کار پرستار می‌کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل را نکرده جمع شود هرکه گوشه‌گیر</p></div>
<div class="m2"><p>در خانه سیر کوچه و بازار می‌کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر هر دلی که زنگ قساوت گرفته است</p></div>
<div class="m2"><p>هر داغ کار دیده بیدار می‌کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون از نظارگی نبرد خیرگی برون</p></div>
<div class="m2"><p>آیینه را حجاب تو ستّار می‌کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرغی که زیرک است درین بوستان‌سرا</p></div>
<div class="m2"><p>از گل فزون ملاحظه از خار می‌کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون شمع از زیاده‌سری‌ها لباس دوست</p></div>
<div class="m2"><p>سر در سر علاقه زر تار می‌کند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در چشم خرده‌بین نبود پرده حجاب</p></div>
<div class="m2"><p>در نقطه سیر گردش پرگار می‌کند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صائب خطی که دیده من روشن است ازو</p></div>
<div class="m2"><p>خاک سیه به دیده اغیار می‌کند</p></div></div>