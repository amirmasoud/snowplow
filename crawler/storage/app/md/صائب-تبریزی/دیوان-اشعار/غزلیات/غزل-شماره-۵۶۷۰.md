---
title: >-
    غزل شمارهٔ ۵۶۷۰
---
# غزل شمارهٔ ۵۶۷۰

<div class="b" id="bn1"><div class="m1"><p>صفحه دل، سیه از مشق تمنا کردیم</p></div>
<div class="m2"><p>کعبه را بتکده زین خط چلیپا کردیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سیه کاری انفاس، دل روشن را</p></div>
<div class="m2"><p>آخرالامر سیه خانه سودا کردیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رشته، گوهر سنجیده، عبرتها بود</p></div>
<div class="m2"><p>نگهی چند که ما صرف تماشا کردیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفسی چند که در غم گذاردن ستم است</p></div>
<div class="m2"><p>همچو گل صرف شکر خنده بیجا کردیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به زر قلب ز کف دامن یوسف دادیم</p></div>
<div class="m2"><p>دل ما خوش که درین قافله سودا کردیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عمر در بیهده گردی گذراندیم چو موج</p></div>
<div class="m2"><p>از گهر صلح به خار و خس دریا کردیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیلی مرگی به عقبی نکند ما را روی</p></div>
<div class="m2"><p>این چنین کز ته دل روی به دنیا کردیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه خیال است توانیم کمر بستن باز</p></div>
<div class="m2"><p>ما که در رهگذر سیل کمر وا کردیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هیچ زنگار به آیینه روشن نکند</p></div>
<div class="m2"><p>آنچه ما با دل و با دیده بینا کردیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نظری را که گشاد دو جهان بود ازو</p></div>
<div class="m2"><p>شانه زلف گرهگیر تماشا کردیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر چه ز افسرده دلانیم به ظاهر صائب</p></div>
<div class="m2"><p>عالمی را به دم گرم خود احیا کردیم</p></div></div>