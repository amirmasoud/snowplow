---
title: >-
    غزل شمارهٔ ۴۳۹۳
---
# غزل شمارهٔ ۴۳۹۳

<div class="b" id="bn1"><div class="m1"><p>اندیشه چرا عشق ز کس داشته باشد</p></div>
<div class="m2"><p>پروانه چه پروای عسس داشته باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سینه صد چاک نگنجد دل عارف</p></div>
<div class="m2"><p>سیمرغ محال است قفس داشته باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشمی که در او نور حیا پرده نشین نیست</p></div>
<div class="m2"><p>ره در همه جا همچو مگس داشته باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رخساره چون ماه تو امروز گرفته است</p></div>
<div class="m2"><p>آیینه که راپیش نفس داشته باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمی است که برهم زده از موی زیادست</p></div>
<div class="m2"><p>تا دل رگ خامی ز هوس داشته باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از مردم کم ظرف نیاید سفر بحر</p></div>
<div class="m2"><p>پیداست حبابی چه نفس داشته باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شادآن دل صد چاک که در خلوت محمل</p></div>
<div class="m2"><p>راه سخنی همچو جرس داشته باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در میکده صائب چه نفس راست نماید</p></div>
<div class="m2"><p>از سایه خود هرکه عسس داشته باشد</p></div></div>