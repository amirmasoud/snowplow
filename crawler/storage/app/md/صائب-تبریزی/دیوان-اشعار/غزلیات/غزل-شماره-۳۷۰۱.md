---
title: >-
    غزل شمارهٔ ۳۷۰۱
---
# غزل شمارهٔ ۳۷۰۱

<div class="b" id="bn1"><div class="m1"><p>ز می‌پرستی خود لاله برنمی‌گردد</p></div>
<div class="m2"><p>شب سیاه درونان سحر نمی‌گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دمید خط و دل سخت یار نرم نشد</p></div>
<div class="m2"><p>ز دود، دیده، آیینه تر نمی‌گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلیل راحت ملک عدم همین کافی است</p></div>
<div class="m2"><p>که هرکه رفت به آن راه برنمی‌گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مدار چشم اقامت ز دولت دنیا</p></div>
<div class="m2"><p>که آفتاب ملول از سفر نمی‌گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درین محیط که از صدق می‌گشاید لب؟</p></div>
<div class="m2"><p>که چون دهان صدف پرگهر نمی‌گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز شست صاف تو صیدی که می‌گشاید لب؟</p></div>
<div class="m2"><p>کباب تا نشود باخبر نمی‌گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مکن ز چتر مرصع به بی‌کلاهان فخر</p></div>
<div class="m2"><p>که پیش تیر حوادث سپر نمی‌گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درین ریاض به جز آب تیشه، نخل امید</p></div>
<div class="m2"><p>ز هیچ آب دگر بارور نمی‌گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز آفتاب دل ذره سرد شد صائب</p></div>
<div class="m2"><p>دل من است که از یار برنمی‌گردد</p></div></div>