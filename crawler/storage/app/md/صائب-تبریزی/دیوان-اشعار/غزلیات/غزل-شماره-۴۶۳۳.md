---
title: >-
    غزل شمارهٔ ۴۶۳۳
---
# غزل شمارهٔ ۴۶۳۳

<div class="b" id="bn1"><div class="m1"><p>می شود از درد و داغ عشق دلها دیده ور</p></div>
<div class="m2"><p>دربهاران می شود از لاله صحرا دیده ور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست غیر ازداغ درمانی دل افسرده را</p></div>
<div class="m2"><p>کز شرار شوخ گردد سنگ خارا دیده ور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنچنان کز دیدن خورشید آب آید به چشم</p></div>
<div class="m2"><p>از تماشای تو می گردد تماشا دیده ور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نظر بازان کمال حسن افزون می شود</p></div>
<div class="m2"><p>کز حباب شوخ گردد روی دریا دیده ور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو نرگس بسته چشم آید برون فردا ز خاک</p></div>
<div class="m2"><p>هر که از غفلت نگردیده است اینجا دیده ور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیده روشن نمی ماند چو سوزن بر زمین</p></div>
<div class="m2"><p>سربرآرد از گریبان مسیحا دیده ور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داشتم امید آزادی ز خط، غافل که حسن</p></div>
<div class="m2"><p>گردد از هر حلقه ای درصید دلها دیده ور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می کند اهل بصیرت راهرو راسوز عشق</p></div>
<div class="m2"><p>در ره تفسیده می گردد کف پادیده ور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیده امیدواران خانه روشن می کند</p></div>
<div class="m2"><p>تازیوسف گشت یعقوب وزلیخا دیده ور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نور بینش بود درصحرای امکان توتیا</p></div>
<div class="m2"><p>ذره ها را کرد مهر عالم آرا دیده ور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دیده شب زنده داران را ز ظلمت باک نیست</p></div>
<div class="m2"><p>روز روشن شمع خاموش است وشبها دیده ور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیست در آهن دلان اکسیر صحبت رااثر</p></div>
<div class="m2"><p>سوزن ناقص نشداز قرب عیسی دیده ور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از جواهر سرمه خال تو اکنون روشن است</p></div>
<div class="m2"><p>پیش ازین گربود دلها از سویدادیده ور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وقت آن گل پیرهن خوش کز نسیم مرحمت</p></div>
<div class="m2"><p>کرد در پیرانه سر یعقوب ما را دیده ور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دور بینان پیش پای خویش نتوانند دید</p></div>
<div class="m2"><p>نیست مرد آخرت در کار دنیا دیده ور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنچنان کز روزن وجام روشن خانه ها</p></div>
<div class="m2"><p>می شود صائب به قدر داغ، دلها دیده ور</p></div></div>