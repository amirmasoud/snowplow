---
title: >-
    غزل شمارهٔ ۵۴۱
---
# غزل شمارهٔ ۵۴۱

<div class="b" id="bn1"><div class="m1"><p>وصل و هجرست یکی چشم و دل حیران را</p></div>
<div class="m2"><p>که زر و سنگ تفاوت نکند میزان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کار موقوف به وقت است که چون وقت رسید</p></div>
<div class="m2"><p>خوابی از بند رهانید مه کنعان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اشک اگر پای شفاعت نگذارد به میان</p></div>
<div class="m2"><p>که جدا می کند از هم دو صف مژگان را؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به که ارباب شفاعت به سر خویش زنند</p></div>
<div class="m2"><p>نکهت منت اگر هست گل احسان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر شود دولت بیدار مساعد روزی</p></div>
<div class="m2"><p>صائب آن نیست فراموش کند یاران را</p></div></div>