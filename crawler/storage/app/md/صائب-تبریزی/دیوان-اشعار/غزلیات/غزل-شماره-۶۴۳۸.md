---
title: >-
    غزل شمارهٔ ۶۴۳۸
---
# غزل شمارهٔ ۶۴۳۸

<div class="b" id="bn1"><div class="m1"><p>ای خطت رهنمای سوختگان</p></div>
<div class="m2"><p>لب لعلت دوای سوختگان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواب مخمل شود ز همواری</p></div>
<div class="m2"><p>خار در زیر پای سوختگان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می کند آب تلخ، کار گلاب</p></div>
<div class="m2"><p>در مقام رضای سوختگان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل آیینه را دهد پرداز</p></div>
<div class="m2"><p>چهره با صفای سوختگان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مژه آفتاب می سوزد</p></div>
<div class="m2"><p>از فروغ لقای سوختگان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کاه را می کند ز دانه جدا</p></div>
<div class="m2"><p>رخ چون کهربای سوختگان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نقش امید می زند بر پای (کذا)</p></div>
<div class="m2"><p>موجه بوریای سوختگان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برنگردد ز آستان اثر</p></div>
<div class="m2"><p>دست خالی، دعای سوختگان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خال رخساره قبول بود</p></div>
<div class="m2"><p>طاعت بی ریای سوختگان</p></div></div>