---
title: >-
    غزل شمارهٔ ۶۷۰۲
---
# غزل شمارهٔ ۶۷۰۲

<div class="b" id="bn1"><div class="m1"><p>چشم خونبارست ابر نوبهار زندگی</p></div>
<div class="m2"><p>آه افسوس است سرو جویبار زندگی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست غیر از لب گزیدن نقلی این پیمانه را</p></div>
<div class="m2"><p>دردسر بسیار دارد میگسار زندگی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برگ او از دست افسوس و ثمر باردل است</p></div>
<div class="m2"><p>دل منه چون غافلان بر برگ و بار زندگی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده از روی تائمل باز کن چون عارفان</p></div>
<div class="m2"><p>کز نگاهی ریزد از هم پود و تار زندگی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می برد با خود ز بی تابی کمند و دام را</p></div>
<div class="m2"><p>در کمند هر که می افتد شکار زندگی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اعتمادی نیست بر شیرازه موج سراب</p></div>
<div class="m2"><p>دل منه بر جلوه ناپایدار زندگی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک دم خوش را هزاران آه حسرت در قفاست</p></div>
<div class="m2"><p>خرج بیش از دخل باشد در دیار زندگی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باده یک ساغرند و پشت و روی یک ورق</p></div>
<div class="m2"><p>چون گل رعنا خزان و نوبهار زندگی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از تزلزل بیخودان نیستی آسوده اند</p></div>
<div class="m2"><p>بر نفس پیوسته لرزد شیشه بار زندگی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در شبستان عدم باشد حضور خواب امن</p></div>
<div class="m2"><p>نیست جز تشویش خاطر در دیار زندگی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون حباب پوچ از پاس نفس غافل مشو</p></div>
<div class="m2"><p>کز نسیمی رخنه افتد در حصار زندگی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بارها سر داد بر باد و همان از سادگی</p></div>
<div class="m2"><p>شمع گردن می کشد از انتظار زندگی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دارد از برق سبک جولان طمع استادگی</p></div>
<div class="m2"><p>هر که از غفلت دهد با خود قرار زندگی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون نگردد سبز در میدان جانبازان عشق؟</p></div>
<div class="m2"><p>نیست خضر نیک پی گر شرمسار زندگی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر به سختی بیستون گردیده ای، چون جوی شیر</p></div>
<div class="m2"><p>نرم سازد استخوانت را فشار زندگی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مرگ چون موی از خمیر آسان کشد بیرون ترا</p></div>
<div class="m2"><p>ریشه گر در سنگ داری در دیار زندگی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون شرر با روی خندان خرده جان کن نثار</p></div>
<div class="m2"><p>چند لرزی بر زر ناقص عیار زندگی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا نگردیده است دست از رعشه ات بی اختیار</p></div>
<div class="m2"><p>دست بردار از عنان اختیار زندگی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>موج آب زندگانی می شمارد تیغ را</p></div>
<div class="m2"><p>هر که پیش از مرگ شست از خود غبار زندگی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>می تواند شد شفیع روزگاران دگر</p></div>
<div class="m2"><p>آنچه صرف عشق شد از روزگار زندگی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا دم صبح قیامت نقش بندد بر زمین</p></div>
<div class="m2"><p>هر که افتد از نفس در زیر بار زندگی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خاک صحرای عدم را توتیا خواهیم کرد</p></div>
<div class="m2"><p>آنچه آمد پیش ما از رهگذار زندگی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سبزه زیر سنگ نتوانست قامت راست کرد</p></div>
<div class="m2"><p>چیست حال خضر یارب زیر بار زندگی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>برگ سبزی می کند ما بینوایان را نهال</p></div>
<div class="m2"><p>بیش از این خشکی مکن ای نوبهار زندگی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دارد از هر موجه ای صائب درین وحشت سرا</p></div>
<div class="m2"><p>نعل بی تابی در آتش جویبار زندگی</p></div></div>