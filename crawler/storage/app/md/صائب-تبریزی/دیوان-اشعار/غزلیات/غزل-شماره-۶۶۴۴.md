---
title: >-
    غزل شمارهٔ ۶۶۴۴
---
# غزل شمارهٔ ۶۶۴۴

<div class="b" id="bn1"><div class="m1"><p>خراب گشت ز می زاهد شراب ندیده</p></div>
<div class="m2"><p>که تاب آب ندارد سفال تاب ندیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز فکر، رشته جانی که پیچ و تاب ندیده</p></div>
<div class="m2"><p>خیال گوهر شهوار را به خواب ندیده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر چه هست بر آن زلف پیچ و تاب مسلم</p></div>
<div class="m2"><p>نظر به موی میان رشته ای است تاب ندیده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیاض گردن بی خال اوست حجت ناطق</p></div>
<div class="m2"><p>که از نظارگیان داغ انتخاب ندیده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مکن چو بی جگران از عتاب تلخ شکایت</p></div>
<div class="m2"><p>که لطف دوست گلابی است آفتاب ندیده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو مست ناز چه دانی عیار سوختگان را؟</p></div>
<div class="m2"><p>که چشم شوخ تو جز دود ازین کباب ندیده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مجوی خواب فراغت ز دیده من حیران</p></div>
<div class="m2"><p>که چشم آینه پوشیدگی به خواب ندیده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نچیده است گل از آفتاب در دل شبها</p></div>
<div class="m2"><p>ترا کسی که به گلگشت ماهتاب ندیده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پلنگ تندی خوی ترا خیال نکرده</p></div>
<div class="m2"><p>غزال مستی چشم ترا به خواب ندیده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روانی از سخنم برد خشک مغزی زاهد</p></div>
<div class="m2"><p>که سیل کند رود در زمین آب ندیده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مجوی پختگی از منکران عشق ز خامی</p></div>
<div class="m2"><p>که نارس است ثمرهای آفتاب ندیده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا دلی است درین بوستان چو غنچه پیکان</p></div>
<div class="m2"><p>که از نسیم گشایش به هیچ باب ندیده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>منم که پاکی چشم از نظاره نیست حجابم</p></div>
<div class="m2"><p>وگرنه کیست که بی رویی از نقاب ندیده؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کجا ز صورت احوال ماست با خبر آن کس</p></div>
<div class="m2"><p>که عکس خویش در آیینه از حجاب ندیده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به تلخکامی دردی کشان چگونه نخندد؟</p></div>
<div class="m2"><p>که آن لب نمکین تلخی از شراب ندیده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز بحر شعر نصیب سخنوران لب خشکی است</p></div>
<div class="m2"><p>صدف تمتعی از گوهر خوشاب ندیده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز چهره رنگ رود از نگاه گرم تو صائب</p></div>
<div class="m2"><p>مبین دلیر به گلهای آفتاب ندیده</p></div></div>