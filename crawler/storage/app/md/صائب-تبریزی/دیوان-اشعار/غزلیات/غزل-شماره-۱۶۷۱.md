---
title: >-
    غزل شمارهٔ ۱۶۷۱
---
# غزل شمارهٔ ۱۶۷۱

<div class="b" id="bn1"><div class="m1"><p>بنای صبر که همسنگ کوه الوندست</p></div>
<div class="m2"><p>به یک اشاره موی میان او بندست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کجا ز دامن این دشت می تواند رفت؟</p></div>
<div class="m2"><p>ز چشم آهو، مجنون ما نظر بندست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به یک اشاره گره می گشاید از ابرو</p></div>
<div class="m2"><p>فغان که بند قبای تو سست پیوندست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قسم به مصحف خط غبار عارض تو</p></div>
<div class="m2"><p>که پیش خط دلم از زلف بیشتر بندست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گلوی خامه ز وصفش چو شمع می سوزد</p></div>
<div class="m2"><p>چه چاشنی است که با آن دهان چو قندست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به توتیا نکنم چشم التفات سیاه</p></div>
<div class="m2"><p>به خاک پای تو چشمی که آرزومندست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تلاش بوسه نداریم چون هوسناکان</p></div>
<div class="m2"><p>نگاه ما به نگاهی ز دور خرسندست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به پاره دل و لخت جگر قناعت کن</p></div>
<div class="m2"><p>که نان خلق گلوگیرتر ز سوگندست</p></div></div>