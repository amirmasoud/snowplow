---
title: >-
    غزل شمارهٔ ۴۷۱۴
---
# غزل شمارهٔ ۴۷۱۴

<div class="b" id="bn1"><div class="m1"><p>از بوسه ظلم بر رخ جانان روا مدار</p></div>
<div class="m2"><p>سیلی به روی یوسف کنعان روا مدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان چیست تا نثار کنی در طریق عشق ؟</p></div>
<div class="m2"><p>این گرد را به دامن جانان روا مدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در بارگاه عشق مبر زهد خشک را</p></div>
<div class="m2"><p>پای ملخ به بزم سلیمان روا مدار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دستی که دامن تو گرفته است بارها</p></div>
<div class="m2"><p>زین بیشتر به چاک گریبان روا مدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم مرا که ره به شبستان زلف داشت</p></div>
<div class="m2"><p>در پیچ و تاب خواب پریشان روا مدار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در قتل من لبان می آلود خویش را</p></div>
<div class="m2"><p>زین بیش در شکنجه دندان روا مدار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>احرام طوف دامن پاک تو بسته است</p></div>
<div class="m2"><p>خون مرابه خار مغیلان روا مدار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای عشق، بی گناه چو یوسف دل مرا</p></div>
<div class="m2"><p>گاهی به چاه و گاه به زندان روا مدار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگشای چشم من چو فکندی سرم به تیغ</p></div>
<div class="m2"><p>زین بیش ظلم برمن حیران روا مدار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>واکن گره ز غنچه دل از نسیم لطف</p></div>
<div class="m2"><p>این عقده را به ناخن و دندان روا مدار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر عاجزان ستم نه طریق مروت است</p></div>
<div class="m2"><p>بر دوش درد، منت درمان روا مدار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در بزم باده راه مده هوشیار را</p></div>
<div class="m2"><p>این خار خشک را به گلستان روا مدار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از شور عشق، داغ مرا تازه روی کن</p></div>
<div class="m2"><p>دلجویی مرا به نمکدان روا مدار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عیش جهان ز گریه من تلخ می شود</p></div>
<div class="m2"><p>این شمع را به هیچ شبستان روا مدار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صائب ز قید عقل دل خویش را برآر</p></div>
<div class="m2"><p>این طفل شوخ را به دبستان روا مدار</p></div></div>