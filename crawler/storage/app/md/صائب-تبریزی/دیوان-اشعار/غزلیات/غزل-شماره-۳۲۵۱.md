---
title: >-
    غزل شمارهٔ ۳۲۵۱
---
# غزل شمارهٔ ۳۲۵۱

<div class="b" id="bn1"><div class="m1"><p>یوسفی نیست دل خوش که هویدا گردد</p></div>
<div class="m2"><p>عافیت گمشده ای نیست که پیدا گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سنگ اطفال به دیوانگی ما افزود</p></div>
<div class="m2"><p>خنده کبک ز کهسار دو بالا گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از فضا کم نشود وحشت خونین جگران</p></div>
<div class="m2"><p>لاله را دل سیه از دامن صحرا گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صیقل آینه غیب همان در غیب است</p></div>
<div class="m2"><p>دل محال است به تدبیر مصفا گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل وحشت زده از سینه کجا یاد کند؟</p></div>
<div class="m2"><p>چه خیال است که گوهر به صدف واگردد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قطره تا موج سبکسیر تواند گردید</p></div>
<div class="m2"><p>حیف باشد گره خاطر دریا گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در دل ساده ما عقل کند جلوه عشق</p></div>
<div class="m2"><p>نقطه سهو بر این صفحه سویدا گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رشته گوهر عبرت که نگاهش خوانند</p></div>
<div class="m2"><p>تا کی از بی بصری دام تماشا گردد؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چهره شمع شد از سیلی پروانه کبود</p></div>
<div class="m2"><p>به چه امید کسی انجمن آرا گردد؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سینه چاک مرا بخیه زدن ممکن نیست</p></div>
<div class="m2"><p>هر سر خاری اگر سوزن عیسی گردد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عشق در پرده ناموس نماند صائب</p></div>
<div class="m2"><p>قاف پوشیده کجا از پر عنقا گردد؟</p></div></div>