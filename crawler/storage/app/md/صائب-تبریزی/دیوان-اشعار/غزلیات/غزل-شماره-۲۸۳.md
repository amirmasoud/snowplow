---
title: >-
    غزل شمارهٔ ۲۸۳
---
# غزل شمارهٔ ۲۸۳

<div class="b" id="bn1"><div class="m1"><p>آسمان را خانه زنبور می دانیم ما</p></div>
<div class="m2"><p>انجمش را دیده های شور می دانیم ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشأه سرشار در میخانه افلاک نیست</p></div>
<div class="m2"><p>صبح را خمیازه مخمور می دانیم ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز فضای دل، به زیر آسمان هر جا که هست</p></div>
<div class="m2"><p>تنگتر از چشم تنگ مور می دانیم ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نعمت الوان ندارد غیر خون خوردن ثمر</p></div>
<div class="m2"><p>قدر نان خشک و آب شور می دانیم ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که می پوشد ز بیداری نظر دلهای شب</p></div>
<div class="m2"><p>در طریق معرفت شبکور می دانیم ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ذره ای خالی ازان خورشید عالمسوز نیست</p></div>
<div class="m2"><p>لاله را فانوس شمع طور می دانیم ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون برون آرد شراب لعل ما را از خمار؟</p></div>
<div class="m2"><p>خون دل را باده کم زور می دانیم ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر سفالی را که از آبش دلی گردد خنک</p></div>
<div class="m2"><p>به ز چینی خانه فغفور می دانیم ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می کشد ما را کجی در خاک و خون چون تیغ کج</p></div>
<div class="m2"><p>راستی را رایت منصور می دانیم ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیده ما از رخ مستور روشن می شود</p></div>
<div class="m2"><p>چهره بی شرم را بی نور می دانیم ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر چه ما با ماه کنعان زیر یک پیراهنیم</p></div>
<div class="m2"><p>از حیا خود را همان مهجور می دانیم ما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ساده لوحی بین، که خود را با کمال اختیار</p></div>
<div class="m2"><p>از غلط بینی همان مجبور می دانیم ما</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با دل مجروح ما هر کس خنک بر می خورد</p></div>
<div class="m2"><p>بی تکلف، مرهم کافور می دانیم ما</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر که بر عیب کسان دارد نظر از عیب خویش</p></div>
<div class="m2"><p>گر سراپا چشم باشد، کور می دانیم ما</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خانه هر دل که از سیلاب بی زنهار عشق</p></div>
<div class="m2"><p>می شود زیر و زبر، معمور می دانیم ما</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دیده ما چون شود روشن ز دیدار بهشت؟</p></div>
<div class="m2"><p>زال دنیا را ز مستی حور می دانیم ما</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چشم ما از سرمه توحید تا روشن شده است</p></div>
<div class="m2"><p>سنگلاخ این جهان را طور می دانیم ما</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نیست صائب در نگاه گرم ما را اختیار</p></div>
<div class="m2"><p>این کشش از جانب منظور می دانیم ما</p></div></div>