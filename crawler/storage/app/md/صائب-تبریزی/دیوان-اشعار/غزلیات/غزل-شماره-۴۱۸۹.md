---
title: >-
    غزل شمارهٔ ۴۱۸۹
---
# غزل شمارهٔ ۴۱۸۹

<div class="b" id="bn1"><div class="m1"><p>عاشق کجا به شکوه دهن باز می‌کند</p></div>
<div class="m2"><p>این کبک خنده بر رخ شهباز می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تمکین ترا به جاست ز سنگین‌دلان که حسن</p></div>
<div class="m2"><p>در دامن تو تربیت ناز می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خون دل همیشه نگارین بود کفش</p></div>
<div class="m2"><p>مشاطه‌ای که زلف ترا باز می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خود را چو داغ لاله کند جمع شام هجر</p></div>
<div class="m2"><p>چون صبح وصل روشنی آغاز می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرغی که زیرک است درین بوستان‌سرا</p></div>
<div class="m2"><p>گل را خیال چنگل شهباز می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در گوشمال عمر سر آمد مگر قضا</p></div>
<div class="m2"><p>ما را برای بزم دگر ساز می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خون می‌چکد چو زخم نمایان ز خنده‌اش</p></div>
<div class="m2"><p>کبکی که بی‌ملاحظه پرواز می‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نتوان به برگ نکهت گل را نهفته داشت</p></div>
<div class="m2"><p>این نه صدف چه با گهر راز می‌کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بلبل به راز غنچه سربسته می‌رسد</p></div>
<div class="m2"><p>این نامه را نسیم عبث باز می‌کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر سرمه‌ای که هست درین خاکدان سپهر</p></div>
<div class="m2"><p>در کار طوطیان سخن‌ساز می‌کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب دلم به سیر چمن می‌کشد مگر</p></div>
<div class="m2"><p>از بلبلان مرا یکی آواز می‌کند</p></div></div>