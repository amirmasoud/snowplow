---
title: >-
    غزل شمارهٔ ۴۷۶۱
---
# غزل شمارهٔ ۴۷۶۱

<div class="b" id="bn1"><div class="m1"><p>کی شود کشت امید از دیده نمناک سبز؟</p></div>
<div class="m2"><p>تاک را هرگز نسازد آب چشم تاک سبز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خط مشکین سرزد از خالش به اندک فرصتی</p></div>
<div class="m2"><p>تخم قابل زود گردد در زمین پاگ سبز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دل خوش مشرب ما دست آفت کوته است</p></div>
<div class="m2"><p>در دل آتش شود این دانه بیباک سبز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشت ما بی حاصلان رفته است از یاد بهار</p></div>
<div class="m2"><p>زنگ سازد دانه ما را مگر درخاک سبز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سینه روشن سخنور را به گفتار آورد</p></div>
<div class="m2"><p>نطق طوطی را کند آیینه های پاک سبز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خشکی زاهد به صد دریا نگردد برطرف</p></div>
<div class="m2"><p>نیست ممکن، گردد از آب دهن مسواک سبز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی نیازی هرزه گویان را شود بند زبان</p></div>
<div class="m2"><p>دامن رهرو نگیرد تا بود خاشاک سبز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کی به درد آید دلش از رنگ زرد سایلان؟</p></div>
<div class="m2"><p>رو سیاهی را که نان شد در بغل ز امساک سبز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاکساری اهل دل را پله نشو و نماست</p></div>
<div class="m2"><p>دانه هیهات است گردد بی وجود خاک سبز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زان بود بی وسمه ابرویش که نتواند شدن</p></div>
<div class="m2"><p>زهر با آن زهره پیش تیغ آن بیباک سبز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>میکشان را بی نیاز از میفروشان می کند</p></div>
<div class="m2"><p>باغبان سازد کدو را گرزاشک تاک سبز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جانگدازان فارغند از منت ابر بهار</p></div>
<div class="m2"><p>شمع دارد خویش رااز دیده نمناک سبز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بیغبار غم نخیزد آه سرد از سینه ها</p></div>
<div class="m2"><p>در سفال خشک ریحان کی شود بی خاک سبز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صائب از سیمای ما گردکدورت رانشست</p></div>
<div class="m2"><p>خوشه اشکی کزوشد طارم افلاک سبز</p></div></div>