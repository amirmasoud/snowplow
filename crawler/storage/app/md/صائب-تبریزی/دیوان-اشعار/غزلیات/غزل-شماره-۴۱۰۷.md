---
title: >-
    غزل شمارهٔ ۴۱۰۷
---
# غزل شمارهٔ ۴۱۰۷

<div class="b" id="bn1"><div class="m1"><p>تا چند دل ترا به هوا وهوس کشد</p></div>
<div class="m2"><p>چون عنکبوت دام به صید مگس کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بویی شنیده است ز گلزار اتحاد</p></div>
<div class="m2"><p>هر بلبلی که ناز گل از خار وخس کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روشندلی است عاشق صادق که همچو صبح</p></div>
<div class="m2"><p>در اولین نفس نفس باز پس کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فارغ ز انقلاب بهار وخزان شود</p></div>
<div class="m2"><p>سرزیر بال هر که به کنج قفس کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غافل مشو ز خال ته زلف آن نگار</p></div>
<div class="m2"><p>کاین دزد خیره حلقه به گوش عسس کشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون بحر دست جذبه برآرد ز آستین</p></div>
<div class="m2"><p>مقدور نیست سیل عنان باز پس کشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در روز بازخواست بود نامه اش سفید</p></div>
<div class="m2"><p>چون صبح اگر کسی به تامل نفس کشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تاکی دل گسسته عنان را ز بی تهی</p></div>
<div class="m2"><p>چون موجه سراب به هر سو هوس کشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیوند خام نیست ز خامان گسستنی</p></div>
<div class="m2"><p>چون طفل دست از ثمر نیمرس کشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگشا نظر که خود بود اول شکار او</p></div>
<div class="m2"><p>دامی که عنکبوت برای مگس کشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شیرافکن است هرکه سگ نفس خویش را</p></div>
<div class="m2"><p>در موسم شباب به قید مرس کشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در اصفهان ز طبع روانی مدار چشم</p></div>
<div class="m2"><p>در خاک سرمه خیز کسی چون نفس کشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دریا کند چگونه نفس راست در حباب</p></div>
<div class="m2"><p>صائب به زیر سقف فلک چون نفس کشد</p></div></div>