---
title: >-
    غزل شمارهٔ ۳۶۵۸
---
# غزل شمارهٔ ۳۶۵۸

<div class="b" id="bn1"><div class="m1"><p>خوشا کسی که دل خود به چشم مست تو داد</p></div>
<div class="m2"><p>ز سر گذشت و به دنبال این بلا افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو تا شکفته شدی گل به خویشتن بالید</p></div>
<div class="m2"><p>تو تا بلند شدی قد کشید نخل مراد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چگونه دل به دو زلف معنبرش ندهم؟</p></div>
<div class="m2"><p>نمی توان به دو عالم به یک طرف افتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنین که رحمت او بی دریغ می بخشد</p></div>
<div class="m2"><p>چرا خموش نباشد زبان استعداد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رود ز پنجه جوهر کنون چو موم برون</p></div>
<div class="m2"><p>دلی که بود به سختی چو بیضه فولاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قضا چو دست برآورد ناله بی اثرست</p></div>
<div class="m2"><p>سپند از آتش سوزان نجست از فریاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درست چون نگذارند خشت اول را</p></div>
<div class="m2"><p>اگر به چرخ رسد کج بود همان بنیاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هنوز از جگر چاک بیستون صائب</p></div>
<div class="m2"><p>به گوش می رسد آواز تیشه فرهاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جواب آن غزل مولوی است این صائب</p></div>
<div class="m2"><p>که بحر لطف بجوشید و بندها بگشاد</p></div></div>