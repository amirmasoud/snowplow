---
title: >-
    غزل شمارهٔ ۱۲۴۴
---
# غزل شمارهٔ ۱۲۴۴

<div class="b" id="bn1"><div class="m1"><p>شمع فانوس خیال آسمان پیداست کیست</p></div>
<div class="m2"><p>شعله جواله این دودمان پیداست کیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن به دل نزدیک دور از چشم، کز لطف گهر</p></div>
<div class="m2"><p>در جهان است و برون است از جهان پیداست کیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجلس آرایی که چون جان جلوه پیدایی اش</p></div>
<div class="m2"><p>برنمی دارد اشارات نهان پیداست کیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با همه نیرنگ سازی، آن که در گلزار او</p></div>
<div class="m2"><p>نیست رنگی از بهار و از خزان پیداست کیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیده یوسف شناسان در غبار کثرت است</p></div>
<div class="m2"><p>ورنه یوسف در میان کاروان پیداست کیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسن مستوری که آورده است از نظاره اش</p></div>
<div class="m2"><p>نرگس عین الیقین آب گمان پیداست کیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه پیدا و نهان با هم نمی گردند جمع</p></div>
<div class="m2"><p>آن که پنهان است و پیدا در جهان پیداست کیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن که ذرات دو عالم را نسیم لطف او</p></div>
<div class="m2"><p>می کند بیدار از خواب گران پیداست کیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آهوی وحشی چه می داند طریق دلبری؟</p></div>
<div class="m2"><p>مردمی آموز چشم دلبران پیداست کیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست در شان عسل حسن گلوسوز این قدر</p></div>
<div class="m2"><p>چاشنی بخش لب شکرفشان پیداست کیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نقشبندی بی قلم نه کار هر صورتگری است</p></div>
<div class="m2"><p>چهره پرداز خط سبز بتان پیداست کیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خضر اگر تیری به تاریکی فکند از ره مرو</p></div>
<div class="m2"><p>آن که می بخشد حیات جاودان پیداست کیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این جواب آن که شیخ مغربی فرموده است</p></div>
<div class="m2"><p>مخفی اندر پیر و پیدا در جوان پیداست کیست</p></div></div>