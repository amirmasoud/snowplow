---
title: >-
    غزل شمارهٔ ۴۵۹۴
---
# غزل شمارهٔ ۴۵۹۴

<div class="b" id="bn1"><div class="m1"><p>ای بر روی تو از آینه گل صافتر</p></div>
<div class="m2"><p>فتنه روی زمین زلف تو را در زیر سر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که از بت روی گردان شد نبیند روی حق</p></div>
<div class="m2"><p>هر که از زنار برگردد نمی بندد کمر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آتش سوزنده رانتوان به چوب اندام داد</p></div>
<div class="m2"><p>چوب گل دیوانگان رامی کند دیوانه تر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوربینان از خزان تنگدستی فراغند</p></div>
<div class="m2"><p>مرغ زیرک در بهاران می کشد سرزیرپر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاه باشد کز غباری لشکری بر هم خورد</p></div>
<div class="m2"><p>تا خطش سر زد، سپاه زلف شد زیرو زبر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در رگ جان هرکه را چون رشته پیچ وتاب نیست</p></div>
<div class="m2"><p>زود باشدسر برآرد از گریبان گهر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بس که درشر خیر ودر خیرجهان شر دیده ام</p></div>
<div class="m2"><p>مانده ام عاجز میان اختیار خیروشر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست ذوق سلطنت مارا، وگرنه ریخته است</p></div>
<div class="m2"><p>چون حباب وموج در بحر فنا تاج وکمر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا ازان شیرین سخن حرفی مکرر بشنوم</p></div>
<div class="m2"><p>خویش راصائب کنم دربزم او دانسته کر</p></div></div>