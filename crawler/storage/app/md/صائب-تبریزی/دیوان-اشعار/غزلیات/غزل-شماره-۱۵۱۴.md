---
title: >-
    غزل شمارهٔ ۱۵۱۴
---
# غزل شمارهٔ ۱۵۱۴

<div class="b" id="bn1"><div class="m1"><p>در کف هر که بود ساغر می، خاتم ازوست</p></div>
<div class="m2"><p>هر که در عالم آب است همه عالم ازوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که پوشید نظر، گوهر بینایی یافت</p></div>
<div class="m2"><p>هر که پرداخت دل از وسوسه جام جم ازوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هوس تخت سلیمان گرهی بر بادست</p></div>
<div class="m2"><p>هر که در حلقه انصاف بود خاتم ازوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دم جان بخش همین قسمت روح الله نیست</p></div>
<div class="m2"><p>هر که لب از سخن بیهده بندد دم ازوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به من کار فرو بسته کجا پردازد؟</p></div>
<div class="m2"><p>آن که پیشانی گل در گره شبنم ازوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دم همت ز لب خامش پیمانه طلب</p></div>
<div class="m2"><p>که درین عهد گلستان کرم را نم ازوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جز از خامه صائب نتوان داد نشان</p></div>
<div class="m2"><p>رگ ابری که همه روی زمین خرم ازوست</p></div></div>