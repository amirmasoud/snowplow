---
title: >-
    غزل شمارهٔ ۳۷۲۳
---
# غزل شمارهٔ ۳۷۲۳

<div class="b" id="bn1"><div class="m1"><p>کسی که با تو نشد آشنا که را دارد؟</p></div>
<div class="m2"><p>ترا کسی که ندارد چه آشنا دارد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فغان که تاج سر من شده است همچو حباب</p></div>
<div class="m2"><p>تعینی که ز دریا مرا جدا دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به راستی ز فلک پیش می توان افتاد</p></div>
<div class="m2"><p>ز نیل می گذرد هرکه این عصا دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خود برون شده را نقش پا نمی باشد</p></div>
<div class="m2"><p>عبث سر از پی ما عقل نارسا دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به خون تپیدن من دورباش عشق بس است</p></div>
<div class="m2"><p>ز پیچ و تاب من این گنج اژدها دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حضور سایه دیورا خویش هرکس یافت</p></div>
<div class="m2"><p>حذر ز سایه بال و پر هما دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سفینه ای که به دریای بیکنار افتاد</p></div>
<div class="m2"><p>چه احتیاج به تدبیر ناخدا دارد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ترحم است درین بوستان بر آن طاوس</p></div>
<div class="m2"><p>که چشم بد ز پر و بال در قفا دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شده است خواب به مخمل حرام از غیرت</p></div>
<div class="m2"><p>ز نقشهای مرادی که بوریا دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز خوردن دل ما نیست عشق را سیری</p></div>
<div class="m2"><p>که بیشتر ز دهن تیغ اشتها دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چرا چو زلف نیفتم به پای او صائب؟</p></div>
<div class="m2"><p>مرا که لذت افتادگی بپا دارد</p></div></div>