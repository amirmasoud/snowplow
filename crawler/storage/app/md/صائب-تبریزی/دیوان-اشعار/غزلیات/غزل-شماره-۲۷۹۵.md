---
title: >-
    غزل شمارهٔ ۲۷۹۵
---
# غزل شمارهٔ ۲۷۹۵

<div class="b" id="bn1"><div class="m1"><p>زعکسش لرزه بر آیینه گوهرنگار افتد</p></div>
<div class="m2"><p>صدف بر خویش می لرزد چو گوهر شاهوار افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زناحق کشتگان پروا ندارد آن سبک جولان</p></div>
<div class="m2"><p>نسوزد دل نسیمی را که ره بر لاله زار افتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زبی پروا نگاهی آب در چشمش نمی گردد</p></div>
<div class="m2"><p>سر خورشید اگر آن سنگدل را در گذار افتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیندازد به خاک آن را که عشق از خاک بردارد</p></div>
<div class="m2"><p>سر منصور هیهات است از آغوش دار افتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مخور بر دل مرا کز زخم دندان پشیمانی</p></div>
<div class="m2"><p>به اندک روزگاری بخیه ات بر روی کار افتد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به صیقل مشکل است از دل زدودن زنگ ذاتی را</p></div>
<div class="m2"><p>که عنبر تیره از دریای روشن بر کنار افتد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشد از جستجو زنجیر مانع شوق مجنون را</p></div>
<div class="m2"><p>کی از رفتار آب از پیچ و تاب جویبار افتد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ملرزان ذره ای را دل که خورشید بلند اختر</p></div>
<div class="m2"><p>به این تقصیر هر روزی ز اوج اعتبار افتد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به روی تازه نتوان پرده پوش فقر گردیدن</p></div>
<div class="m2"><p>که آتش عاقبت از دست خالی در چنار افتد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مصور می شود بی پرده آن آیینه رو صائب</p></div>
<div class="m2"><p>اگر آیینه دل از علایق بی غبار افتد</p></div></div>