---
title: >-
    غزل شمارهٔ ۸۳۷
---
# غزل شمارهٔ ۸۳۷

<div class="b" id="bn1"><div class="m1"><p>زهی به ساعد سیمین شکوفه ید بیضا</p></div>
<div class="m2"><p>نظر به نور جمال تو مهر دیده حربا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به جستجوی تو چندان عنان گسسته دویدم</p></div>
<div class="m2"><p>که گشت صفحه مسطر کشیده، دامن صحرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مکن نصیحت اهل لباس، بخیه به لب زن</p></div>
<div class="m2"><p>عبث گلاب میفشان به روی صورت دیبا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عذار ماه کلف دار شد ز پرتو منت</p></div>
<div class="m2"><p>در آفتاب بسوز و مرو به سایه طوبی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بمال بر لب خونخوار حرص، خاک قناعت</p></div>
<div class="m2"><p>وگرنه تشنگی افزاست آب شور تمنا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آن سرست بزرگی که نیست فکر بزرگی</p></div>
<div class="m2"><p>در آن دل است تماشا که نیست راه تماشا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلم چو خانه زنبور گشته است ز کاوش</p></div>
<div class="m2"><p>سرم چو کلک مصور شده است از رگ سودا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه حاجت است به شمع و چراغ کعبه روان را؟</p></div>
<div class="m2"><p>که همچو ریگ روان ریخته است آبله پا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در آن ریاض که باد بهار عدل بجنبد</p></div>
<div class="m2"><p>چو گل شکفته شود هر کسی که غنچه شد اینجا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز ترکتاز حوادث مکن ملاحظه صائب</p></div>
<div class="m2"><p>چه کرد سیل به پیشانی گشاده صحرا؟</p></div></div>