---
title: >-
    غزل شمارهٔ ۲۴۶۷
---
# غزل شمارهٔ ۲۴۶۷

<div class="b" id="bn1"><div class="m1"><p>از دیار مردمی دیار در عالم نماند</p></div>
<div class="m2"><p>آشنارویی به جز دیوار در عالم نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیشه فولاد انگشت ندامت می گزد</p></div>
<div class="m2"><p>حیف یک فرهاد شیرین کار در عالم نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کجا خاری است در پیراهن من می خلد</p></div>
<div class="m2"><p>گرچه از چشم تر من خار در عالم نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بنای استوار شرع با آن محکمی</p></div>
<div class="m2"><p>غیر برفین گنبد دستار در عالم نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوشه چشمی نماند از مردمی در روزگار</p></div>
<div class="m2"><p>سرمه واری نرمی گفتار در عالم نماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طالب آمل گذشت و طبعها افسرده شد</p></div>
<div class="m2"><p>کز چه رو آن آتشین گفتار در عالم نماند</p></div></div>