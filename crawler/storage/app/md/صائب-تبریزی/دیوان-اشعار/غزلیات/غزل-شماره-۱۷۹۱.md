---
title: >-
    غزل شمارهٔ ۱۷۹۱
---
# غزل شمارهٔ ۱۷۹۱

<div class="b" id="bn1"><div class="m1"><p>ز عشق در اگر نور آشنایی هست</p></div>
<div class="m2"><p>به زیر خاک هم امید روشنایی هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حریم وصل محال است بی قریب بود</p></div>
<div class="m2"><p>که هر کجا که بود عید، روستایی هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه گل ز دیدن صیاد می توانی چید؟</p></div>
<div class="m2"><p>ترا که در قفس اندیشه رهایی هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز داغ عشق مکش سر، که خانه دل را</p></div>
<div class="m2"><p>به قدر روزنه داغ، روشنایی هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عنایتی است که بند قبا گشایی خود</p></div>
<div class="m2"><p>وگرنه دست مرا در گرهگشایی هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همین زیادتی زلف و خط و خال بود</p></div>
<div class="m2"><p>میانه تو و خورشید اگر جدایی هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه نعمتی است که تن پروران نمی دانند</p></div>
<div class="m2"><p>که عیش روی زمین در برهنه پایی هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکستگی نشود در وجود پا بر جای</p></div>
<div class="m2"><p>در آن دیار که امید مومیایی هست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ببر ز هر دو جهان چون مجردان صائب</p></div>
<div class="m2"><p>اگر به عشق ترا ذوق آشنایی هست</p></div></div>