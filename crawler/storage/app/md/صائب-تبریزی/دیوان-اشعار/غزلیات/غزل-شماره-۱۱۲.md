---
title: >-
    غزل شمارهٔ ۱۱۲
---
# غزل شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>گل نزد آبی بر آتش بلبل خودکام را</p></div>
<div class="m2"><p>نیست غیر از ناامیدی حاصلی ابرام را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چهره خورشید رویان را سپندی لازم است</p></div>
<div class="m2"><p>از شب جمعه است نیل چشم زخم ایام را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق عالمسوز می باید دل افسرده را</p></div>
<div class="m2"><p>می پزد خورشید تابان میوه های خام را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست ممکن از زبان خوش کسی نقصان کند</p></div>
<div class="m2"><p>چرب نرمی غوطه در شکر دهد بادام را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون شرر بر جان نمی لرزم ز بیم نیستی</p></div>
<div class="m2"><p>دیده ام در نقطه آغاز خود، انجام را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با ضعیفان پنجه کردن نیست کار اقویا</p></div>
<div class="m2"><p>در قفس دارد نیستان شیر خون آشام را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صبح چون روشن شود، از خواب غفلت سر برآر</p></div>
<div class="m2"><p>تا کفن بر خود نسازی جامه احرام را</p></div></div>