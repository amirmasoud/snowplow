---
title: >-
    غزل شمارهٔ ۲۵۱۷
---
# غزل شمارهٔ ۲۵۱۷

<div class="b" id="bn1"><div class="m1"><p>زلف مشکینت دهان شانه پر عنبر کند</p></div>
<div class="m2"><p>سرمه خاموش را چشمت زبان آور کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن که می گوید قیامت بر نمی خیزد، کجاست؟</p></div>
<div class="m2"><p>تا در آن مژگان تماشای صف محشر کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اشتیاق صفحه رخسار شبنم زیب او</p></div>
<div class="m2"><p>دامن گل را به شبنم آتشین بستر کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از عدالت نیست افکندن در آتش روز حشر</p></div>
<div class="m2"><p>عود خامی را که خون در دیده مجمر کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سینه خود عالمی چون صبح صیقل داده اند</p></div>
<div class="m2"><p>آفتاب معرفت تا از کجا سر بر کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رنج باریک است جان را قسمت از تن پروران</p></div>
<div class="m2"><p>چربی پهلوی گوهر رشته را لاغر کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان به کوشش برنمی آید ز زندان جهات</p></div>
<div class="m2"><p>رخنه هیهات است زور نقش در ششدر کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بس که بسته است آسمان سفله کشتی را به خشک</p></div>
<div class="m2"><p>دیدن خورشید ممکن نیست چشمی تر کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آتش غیرت سراسر می رود در جان خضر</p></div>
<div class="m2"><p>تا مباد از چشمه حیوان کسی لب تر کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون نگردد قالب بی جان دل تن پروران؟</p></div>
<div class="m2"><p>کاسه چون افتاد فربه کیسه را لاغر کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از نگاه تلخ صائب زهر می ریزم بر او</p></div>
<div class="m2"><p>دایه بیدرد در شیرم اگر شکر کند</p></div></div>