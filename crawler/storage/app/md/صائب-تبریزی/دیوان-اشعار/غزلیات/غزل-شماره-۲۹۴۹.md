---
title: >-
    غزل شمارهٔ ۲۹۴۹
---
# غزل شمارهٔ ۲۹۴۹

<div class="b" id="bn1"><div class="m1"><p>کجا از تیغ سرگرم محبت باک می دارد؟</p></div>
<div class="m2"><p>سرعاشق کمند وحدت از فتراک می دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندارد پاک گوهر شکوه ای از تلخی دوران</p></div>
<div class="m2"><p>صدف در سینه دریا دهن را پاک می دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مپرس از زاهدان خشک سر گوهر عرفان</p></div>
<div class="m2"><p>که از دریا لب ساحل خس و خاشاک می دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زچوب خشک گردد شعله بیباک سرکش تر</p></div>
<div class="m2"><p>کجا از دار پروا عاشق بیباک می دارد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لباس غنچه تنگی می کند بر خنده گلها</p></div>
<div class="m2"><p>فلک دانسته اهل عشق را غمناک می دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندارد زلف آن بیباک پروا از غبار خط</p></div>
<div class="m2"><p>کجا اندیشه چشم شوخ دام از خاک می دارد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میفشان آستین بی نیازی برنواسنجی</p></div>
<div class="m2"><p>که چون گل هر طرف چندین گریبان چاک می دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مکن زنهار تکلیف قبا دیوانه ما را</p></div>
<div class="m2"><p>که عاشق از گریبان حلقه فتراک می دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به بخت تیره صائب صلح کن با نور آگاهی</p></div>
<div class="m2"><p>که زنگ از بخت سبز آیینه ادراک می دارد</p></div></div>