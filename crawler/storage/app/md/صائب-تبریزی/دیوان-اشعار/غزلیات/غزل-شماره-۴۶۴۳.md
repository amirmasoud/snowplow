---
title: >-
    غزل شمارهٔ ۴۶۴۳
---
# غزل شمارهٔ ۴۶۴۳

<div class="b" id="bn1"><div class="m1"><p>می شوی آواره از عالم عنان ما مگیر</p></div>
<div class="m2"><p>راه بر سیلی که دارد روی دریا مگیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخیه منت جراحت را کند ناسورتر</p></div>
<div class="m2"><p>رشته از مریم مخواه و سوزن از عیسی مگیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رتبه کامل عیاران ازمحک ظاهر شود</p></div>
<div class="m2"><p>تن به سنگ کودکان ده ،دامن صحرامگیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گریه های تلخ دارد خنده های شکرین</p></div>
<div class="m2"><p>گردهد دامن به دستت گل ز استغنامگیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوزخ نقدست صحبت با خدا بیگانگان</p></div>
<div class="m2"><p>رحم برخود کن ،درین زندان وحشت جامگیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جوش این میخانه از رخسار آتشناک توست</p></div>
<div class="m2"><p>ای بهشتی روی از خاک شهیدان پامگیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می کند ناسور داغ تشنگی راآب شور</p></div>
<div class="m2"><p>چون صدف دندان به دل نه ،آب از دریامگیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می شود آخربیابان مرگ، جویای سراب</p></div>
<div class="m2"><p>رحم اگر برپای خود داری پی دنیامگیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در سیاهی یافت صائب خضر آب زندگی</p></div>
<div class="m2"><p>هیچ دامانی بغیر از دامن شبهامگیر</p></div></div>