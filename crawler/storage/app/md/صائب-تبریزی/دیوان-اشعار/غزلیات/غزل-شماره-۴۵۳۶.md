---
title: >-
    غزل شمارهٔ ۴۵۳۶
---
# غزل شمارهٔ ۴۵۳۶

<div class="b" id="bn1"><div class="m1"><p>چو خوش است اتحادی که حجاب تن نماند</p></div>
<div class="m2"><p>که من آن زمان شوم من که اثر ز من نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شده محو جان روشن تن ساده لوح غافل</p></div>
<div class="m2"><p>که ز شمع غیرداغی به دل لگن نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز کلام پوچ عالم جرسی است پر ز غوغا</p></div>
<div class="m2"><p>به چه خلوت آورم رو که به انجمن نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز نشان ونام بگذر به امید بازگشتن</p></div>
<div class="m2"><p>که عقیق نامجو را هوس یمن نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو قلم ازان ز خجلت سرخودبه زیر دارم</p></div>
<div class="m2"><p>که ز من به جای چیزی به جز از سخن نماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه شب در انتظارم که چو شمع صبحگاهی</p></div>
<div class="m2"><p>نفسی بر آرم از دل که نفس به من نماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگذاشت جان روشن اثری ز جسم صائب</p></div>
<div class="m2"><p>که زشمع هیچ بر جا ز گداختن نماند</p></div></div>