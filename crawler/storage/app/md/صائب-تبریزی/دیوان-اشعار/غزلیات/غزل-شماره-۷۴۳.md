---
title: >-
    غزل شمارهٔ ۷۴۳
---
# غزل شمارهٔ ۷۴۳

<div class="b" id="bn1"><div class="m1"><p>دایم ز نازکی است دل افگار شیشه را</p></div>
<div class="m2"><p>خون می چکد مدام ز گفتار شیشه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یادآور از خمار گلوگیر صبحگاه</p></div>
<div class="m2"><p>خالی مکن ز باده به یکبار شیشه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چند خوشگوار بود باده غرور</p></div>
<div class="m2"><p>زین می فزون ز سنگ نگه دار شیشه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خنده صلح کن به تبسم که می شود</p></div>
<div class="m2"><p>قالب تهی ز خنده بسیار شیشه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاید به جوی رفته کند آب بازگشت</p></div>
<div class="m2"><p>چون شد تهی ز باده، مبین خوار شیشه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در شکوه های تلخ مرا اختیار نیست</p></div>
<div class="m2"><p>می آورد شراب به گفتار شیشه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون آمدی به کوی خرابات بی طلب</p></div>
<div class="m2"><p>بر طاق نه صلاح و فرود آر شیشه را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل می دود به سنگ ملامت به زور عشق</p></div>
<div class="m2"><p>می سازد این شراب جگردار شیشه را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باشد قدح همیشه ز افتادگی عزیز</p></div>
<div class="m2"><p>از سرکشی کنند نگونسار شیشه را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در محفلی که راز شرر می جهد ز سنگ</p></div>
<div class="m2"><p>ما کرده ایم پرده اسرار شیشه را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با مشت خاک من چه کند آتشین میی</p></div>
<div class="m2"><p>کآورد در سماع فلک وار شیشه را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سنگ و سبوست دشمنی توبه و شراب</p></div>
<div class="m2"><p>تا از خم است پشت به کهسار شیشه را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در ساغر من است شرابی که می برد</p></div>
<div class="m2"><p>بیخود به سیر کوچه و بازار شیشه را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این باده ای که آن لب میگون رسانده است</p></div>
<div class="m2"><p>چون نار شق کند دل بسیار شیشه را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صید از حرم برون چو نهد پای، کشتنی است</p></div>
<div class="m2"><p>زنهار زیر خرقه نگه دار شیشه را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خوردم فریب چرخ به همواریی که داشت</p></div>
<div class="m2"><p>کردم غلط به مرهم زنگار شیشه را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر چرخ سست عهد منه دل ز سادگی</p></div>
<div class="m2"><p>طاق شکسته نیست سزاوار شیشه را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صائب ز پرده داری ناموس شد خلاص</p></div>
<div class="m2"><p>هر کس شکست بر سر بازار شیشه را</p></div></div>