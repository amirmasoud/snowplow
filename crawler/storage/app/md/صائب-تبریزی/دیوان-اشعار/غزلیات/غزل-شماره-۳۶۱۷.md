---
title: >-
    غزل شمارهٔ ۳۶۱۷
---
# غزل شمارهٔ ۳۶۱۷

<div class="b" id="bn1"><div class="m1"><p>بر سر حرف، گر آن چشم فسون ساز آید</p></div>
<div class="m2"><p>با نفس سوختگی سرمه به آواز آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از غریبی به وطن می روم و می گویم</p></div>
<div class="m2"><p>وقت آن خوش که به غربت ز وطن باز آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذوق کاوش اگر این است که من یافته ام</p></div>
<div class="m2"><p>سینه کبک به عذر قدم باز آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساده دل را نبود بند خموشی به زبان</p></div>
<div class="m2"><p>پرده پوشی کی از آیینه غماز آید؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رگ جانم هدف نشتر الماس شود</p></div>
<div class="m2"><p>ناخن ریشی اگر بر جگر ساز آید</p></div></div>