---
title: >-
    غزل شمارهٔ ۲۱۸۷
---
# غزل شمارهٔ ۲۱۸۷

<div class="b" id="bn1"><div class="m1"><p>لب بسته ما بیخبر از راز جهان نیست</p></div>
<div class="m2"><p>بسیار بود حرف کسی را که زبان نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از شرم در بسته روزی نگشاید</p></div>
<div class="m2"><p>روزی ز دل خود بود آن را که دهان نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جانها همه از شوق عدم جامه درانند</p></div>
<div class="m2"><p>آرام درین قافله ریگ روان نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق خبر از کعبه و بتخانه ندارد</p></div>
<div class="m2"><p>این تیر سبکسیر مقید به نشان نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بستر نرم است گرانخوابی مخمل</p></div>
<div class="m2"><p>بالین اگر از سنگ بود خواب گران نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سنگ سبکبار شود نخل برومند</p></div>
<div class="m2"><p>بر خاک نشینان سخن سخت گران نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با اینهمه نعمت که بر این سفره مهیاست</p></div>
<div class="m2"><p>صائب لب بی شکوه به غیر از لب نان نیست</p></div></div>