---
title: >-
    غزل شمارهٔ ۴۰۳۱
---
# غزل شمارهٔ ۴۰۳۱

<div class="b" id="bn1"><div class="m1"><p>دمید صبح تجلی به جان شتاب کنید</p></div>
<div class="m2"><p>ز سردسیر جهان رو به آفتاب کنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین محل که گشوده است صبح دفتر فیض</p></div>
<div class="m2"><p>ستاره ای ز برای خود انتخاب کنید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کنون که زر به سپر بخش می کند خورشید</p></div>
<div class="m2"><p>چه لازم است نظر را سیه به خواب کنید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هوای عالم بالا کنید چون شبنم</p></div>
<div class="m2"><p>بس است چند اقامت درین خراب کنید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز روی دل بفشانید گرد هستی را</p></div>
<div class="m2"><p>نظر به شاهد مقصود بی حجاب کنید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر رسید به پابوس بحر چون سیلاب</p></div>
<div class="m2"><p>به آه گرم دل سنگ خویش آب کنید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عمارت نفس پوچ را بقایی نیست</p></div>
<div class="m2"><p>ز بحر چند جدا خانه چون حباب کنید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز خون دیده خود می به ساغر اندازید</p></div>
<div class="m2"><p>ز رنگ چهره خود سیر ماهتاب کنید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چراغ دولت بیدار را فروغی نیست</p></div>
<div class="m2"><p>نظر به گوشه آن چشم نیمخواب کنید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز شعر مولوی روم چون بپردازید</p></div>
<div class="m2"><p>موحدان غزل صائب انتخاب کنید</p></div></div>