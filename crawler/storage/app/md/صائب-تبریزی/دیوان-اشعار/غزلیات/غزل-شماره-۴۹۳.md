---
title: >-
    غزل شمارهٔ ۴۹۳
---
# غزل شمارهٔ ۴۹۳

<div class="b" id="bn1"><div class="m1"><p>کیست گردن ننهد دام جهانگیر ترا؟</p></div>
<div class="m2"><p>چرخ یک حلقه بود زلف چو زنجیر ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شست صاف تو مریزاد، که خون دو جهان</p></div>
<div class="m2"><p>نشود مانع پرواز، پر تیر ترا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کار سنگ بده از لوح مزارش آید</p></div>
<div class="m2"><p>هر که در خاک برد حسرت شمشیر ترا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بحر در حوصله قطره نگنجد هیهات</p></div>
<div class="m2"><p>دیده چون درک کند حسن جهانگیر ترا؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حاصل ملک جهان پیش سلیمان بادست</p></div>
<div class="m2"><p>به چه تسخیر نمایم نظر سیر ترا؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نتوان داشت به زنجیر، نگاهش صائب</p></div>
<div class="m2"><p>هر که از دل شنود ناله شبگیر ترا</p></div></div>