---
title: >-
    غزل شمارهٔ ۴۲۶۲
---
# غزل شمارهٔ ۴۲۶۲

<div class="b" id="bn1"><div class="m1"><p>دل چون کمال یافت سخن مختصر شود</p></div>
<div class="m2"><p>لب وا نمی کند چو صدف پر گهر شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آیینه شکوه بیهده از زنگ می کند</p></div>
<div class="m2"><p>اینش سزاست هر که پریشان نظر شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این بیغمی که در دل سنگین توست فرش</p></div>
<div class="m2"><p>از زور خنده چشم تو مشکل که تر شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تلخی نمی کشد چو صدف در میان بحر</p></div>
<div class="m2"><p>قانع ز ابر هر که به آب گهر شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بوی گل به جذبه کند رهبری مرا</p></div>
<div class="m2"><p>دام وقفس به بلبل من بال وپر شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دل سیاه نیست دم گرم را اثر</p></div>
<div class="m2"><p>از جوش بحر عنبر تر خامتر شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ابر سیاه حامل باران رحمت است</p></div>
<div class="m2"><p>از خط امیدواری من بیشتر شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در بحر عشق تن به فنا ده که چون حباب</p></div>
<div class="m2"><p>هر کس که سر نهاد در او تاجور شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پرهیز کن ز صحبت آهن دلان که آب</p></div>
<div class="m2"><p>جاری به جوی تیغ چو شد بد گهر شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نادان به سیم وزر شود از صاحبان هوش</p></div>
<div class="m2"><p>از گوشوار اگر شنوا گوش کر شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ناقص بود ز آفت عین الکمال امن</p></div>
<div class="m2"><p>ماه تمام دنبه گداز از نظر شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از آتش است سنگ محک بید و عود را</p></div>
<div class="m2"><p>اخلاق خوب وزشت عیان از سفر شود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زینسان که در زمانه ما خوار شد سخن</p></div>
<div class="m2"><p>طوطی کجا ز خوش سخنی معتبر شود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شوید ز روی عنبر اگر بحر تیرگی</p></div>
<div class="m2"><p>صائب امید هست شب ما سحر شود</p></div></div>