---
title: >-
    غزل شمارهٔ ۱۳۳۶
---
# غزل شمارهٔ ۱۳۳۶

<div class="b" id="bn1"><div class="m1"><p>قامتت خم گشت و پشتت بار طاعت برنداشت</p></div>
<div class="m2"><p>چهره بی شرمیت رنگ خجالت برنداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد بناگوشت سفید و بخت خواب آلود تو</p></div>
<div class="m2"><p>در چنین صبحی سر از بالین غفلت برنداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پایت از رفتار ماند و پایی ننهادی به راه</p></div>
<div class="m2"><p>ریخت دندان و لبت زخم ندامت برنداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با وجود رعشه پیری، کف لرزان تو</p></div>
<div class="m2"><p>از گریبان تعلق دست رغبت برنداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که در فصل بهاران دانه اشکی نریخت</p></div>
<div class="m2"><p>وقت خرمن خوشه ای جز آه حسرت برنداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در چنین هنگامه ای صائب دل بی شرم تو</p></div>
<div class="m2"><p>پشت بیدردی ز دیوار فراغت برنداشت</p></div></div>