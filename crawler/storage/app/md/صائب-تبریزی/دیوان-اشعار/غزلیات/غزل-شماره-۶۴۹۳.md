---
title: >-
    غزل شمارهٔ ۶۴۹۳
---
# غزل شمارهٔ ۶۴۹۳

<div class="b" id="bn1"><div class="m1"><p>غافل از داغ جنون ای دیده روشن مشو</p></div>
<div class="m2"><p>گر رهایی چشم داری غافل از روزن مشو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دامن رستم به دست جذبه سوی خویش کش</p></div>
<div class="m2"><p>دل نهاد این چه تاریک چون بیژن مشو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر نچینی خوشه ای، چون مور بر چین دانه ای</p></div>
<div class="m2"><p>دست و دامان تهی زنهار ازین خرمن مشو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این سیه کاران سزاوار توجه نیستند</p></div>
<div class="m2"><p>پیش این تردامنان آیینه روشن مشو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>احتیاط از کف مده هر چند در راه حقی</p></div>
<div class="m2"><p>همچو موسی بی عصا در وادی ایمن مشو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باش زیر چرخ تا آیینه ات دارد غبار</p></div>
<div class="m2"><p>چون شدی روشن، غبار خاطر گلخن مشو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دامن اهل تجرد با گرانجانی مگیر</p></div>
<div class="m2"><p>بر مسیحای سبکرو بار چون سوزن مشو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خون فاسد در بدن آهن ربای نشترست</p></div>
<div class="m2"><p>نیش مردم برنمی تابی رگ گردن مشو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جمع کن چون شبنم گل پا به دامان ادب</p></div>
<div class="m2"><p>از نگاه خیره گل را خار پیراهن مشو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آبرویی نیست در گلزار ابر خشک را</p></div>
<div class="m2"><p>چون نداری چشم تر، در حلقه شیون مشو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شکوه ناسازی گردون به اهل دل مبر</p></div>
<div class="m2"><p>یوسف گل پیرهن را خار پیراهن مشو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر چراغ ما کز او چشم جهانی روشن است</p></div>
<div class="m2"><p>تا توان فانوس شد، ای سنگدل دامن مشو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جستجو صائب به جایی می رساند خویش را</p></div>
<div class="m2"><p>هر قدر سختی ببینی سست در رفتن مشو</p></div></div>