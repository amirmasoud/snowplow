---
title: >-
    غزل شمارهٔ ۲۷۳۸
---
# غزل شمارهٔ ۲۷۳۸

<div class="b" id="bn1"><div class="m1"><p>شاخ گل از دست و چوگان تو یادم می دهد</p></div>
<div class="m2"><p>غنچه از گوی گریبان تو یادم می دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جلوه خورشید تابان در ته دامان ابر</p></div>
<div class="m2"><p>زیر زلف از ماه تابان تو یادم می دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برق عالمسوز در ابر سیاه نوبهار</p></div>
<div class="m2"><p>از نگاه چشم فتان تو یادم می دهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از شفق حال شهیدان تو می گردد عیان</p></div>
<div class="m2"><p>ماه نو از تیغ عریان تو یادم می دهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>انتظام گوهر شهوار در کام صدف</p></div>
<div class="m2"><p>زیر لب از عقده دندان تو یادم می دهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خط به دور جام لبریز از شراب لاله رنگ</p></div>
<div class="m2"><p>گرد لب از خط ریحان تو یادم می دهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می دهد یاد از دل پرخون من هر غنچه ای</p></div>
<div class="m2"><p>هرگلی از روی خندان تو یادم می دهد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از صف محشر دلم لرزان شود چون برگ بید</p></div>
<div class="m2"><p>کز صف برگشته مژگان تو یادم می دهد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در میان جان شیرین چون الف جا می دهم</p></div>
<div class="m2"><p>هر چه از سرو خرامان تو یادم می دهد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در کنار بوستان، مجموعه رنگین گل</p></div>
<div class="m2"><p>صائب از اوراق دیوان تو یادم می دهد</p></div></div>