---
title: >-
    غزل شمارهٔ ۲۱۰۷
---
# غزل شمارهٔ ۲۱۰۷

<div class="b" id="bn1"><div class="m1"><p>ماهی که ز پرتو به جهان شور درانداخت</p></div>
<div class="m2"><p>پیش رخت از هاله مکرر سپر انداخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با گوشه دل غنچه صفت ساخته بودم</p></div>
<div class="m2"><p>بوی تو مرا همچو صبا دربدر انداخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دیده صاحب نظران موی زیادم</p></div>
<div class="m2"><p>زان روز که چشم تو مرا از نظر انداخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا دامن محشر نتوان دوخت به سوزن</p></div>
<div class="m2"><p>مژگان تو چاکی که مرا در جگر انداخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فریاد که شیرین سخنی طوطی ما را</p></div>
<div class="m2"><p>مشغول سخن کرد و ز فکر شکر انداخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن را که به دولت نتوانیش رساندن</p></div>
<div class="m2"><p>مانند هما سایه نباید به سر انداخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب شدم آسوده ازین کارگشایان</p></div>
<div class="m2"><p>تا کار مرا عشق به آه سحر انداخت</p></div></div>