---
title: >-
    غزل شمارهٔ ۲۹۴۸
---
# غزل شمارهٔ ۲۹۴۸

<div class="b" id="bn1"><div class="m1"><p>به قامت سرو را از قد کشیدن باز می دارد</p></div>
<div class="m2"><p>به عارض رنگ گل را از پریدن باز می دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من این رخسار حیرت آفرین کز یار می بینم</p></div>
<div class="m2"><p>سرشک گرمرو را از چکیدن باز می دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا کرده است چون آیینه حیران مجلس آرایی</p></div>
<div class="m2"><p>که می را در رگ مست از دویدن باز می دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من این مژگان گیرایی کز آن خوش چشم می بینم</p></div>
<div class="m2"><p>نگاه وحشیان را از رمیدن باز می دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر بی پرده در بازار مصر آیی، زلیخا را</p></div>
<div class="m2"><p>تماشای تو از یوسف خریدن باز می دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه مغرورست خورشید جهان افروز حسن او</p></div>
<div class="m2"><p>که صبح آرزو را از دمیدن باز می دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از ان ظاهر نشد خونریزی مژگان خونخوارش</p></div>
<div class="m2"><p>که تیغ تشنه خون را از چکیدن باز می دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به ظاهر تلخیی دارد سر پستان پیکانش</p></div>
<div class="m2"><p>که طفلان هوس را از مکیدن باز می دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نشد زان بیقراریهای من خاطر نشان تو</p></div>
<div class="m2"><p>که تمکین تو دل را از تپیدن باز می دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نمی سازد به خود مشغول دنیا اهل بینش را</p></div>
<div class="m2"><p>که وحشت آهوان را از چریدن باز می دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حجاب سهل بسیارست ارباب بصیرت را</p></div>
<div class="m2"><p>نظر را برگ کاهی از پریدن باز می دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ره هموار پیش دوربینان این خطر دارد</p></div>
<div class="m2"><p>که رهرو را زپیش پای دیدن باز می دارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زپیریها همین افسوس دل را می گزد صائب</p></div>
<div class="m2"><p>که بی دندانیم از لب گزیدن باز می دارد</p></div></div>