---
title: >-
    غزل شمارهٔ ۲۰۴۵
---
# غزل شمارهٔ ۲۰۴۵

<div class="b" id="bn1"><div class="m1"><p>دلهای آرمیده به مطلب سوار نیست</p></div>
<div class="m2"><p>رحم است بر کسی که دلش برقرار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دامن است شعله جواله بی نیاز</p></div>
<div class="m2"><p>موقوف، شور من به نسیم بهار نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دست اگر چه هست به ظاهر عنان مرا</p></div>
<div class="m2"><p>چون طفل نوسوار مرا اختیار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیل گران رکاب رسد زودتر به بحر</p></div>
<div class="m2"><p>بر دل مرا ز پیکر خاکی غبار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاری به راه جان سبکرو درین جهان</p></div>
<div class="m2"><p>بالاتر از مساعدت روزگار نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندیشه پنبه زده را نیست از کمان</p></div>
<div class="m2"><p>حلاج را ملاحظه از چوب دار نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیهوده همچو موج چرا دست و پا زنیم؟</p></div>
<div class="m2"><p>دریای بیقراری ما را کنار نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نبود به تن علاقه ز دنیا گذشته را</p></div>
<div class="m2"><p>سرو روان مقید این جویبار نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پروانه خودکشی نکند بر چراغ روز</p></div>
<div class="m2"><p>عشاق را به چهره بی شرم کار نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غواص از یگانگی بحر غافل است</p></div>
<div class="m2"><p>ورنه حباب بی گهر شاهوار نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طامع ز تشنگی به بزرگان برد پناه</p></div>
<div class="m2"><p>غافل که هیچ چشمه درین کوهسار نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صائب بگو، که سوخته جانان عشق را</p></div>
<div class="m2"><p>آب حیات جز سخن آبدار نیست</p></div></div>