---
title: >-
    غزل شمارهٔ ۳۲۳۲
---
# غزل شمارهٔ ۳۲۳۲

<div class="b" id="bn1"><div class="m1"><p>به هر نامحرمی عاشق لب اظهار نگشاید</p></div>
<div class="m2"><p>گل این باغ، دفتر در حضور خار نگشاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکایت نامه ما سنگ را در گریه می آرد</p></div>
<div class="m2"><p>الهی هیچ کافر مهر ازین طومار نگشاید؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هوادار سر زلف صنم چون شمع می باید</p></div>
<div class="m2"><p>که گر در آتش افتد از میان زنار نگشاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگه خون گشت در چشمم زبس نادیدنی دیدم</p></div>
<div class="m2"><p>الهی هیچ کس آیینه در بازار نگشاید!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جوش مشتری هر کس چو یوسف بر نمی آید</p></div>
<div class="m2"><p>همان بهتر که دکان بر سر بازار نگشاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که این ابر بلا را از سر من دور می سازد؟</p></div>
<div class="m2"><p>اگر جوش جنون از سر مرا دستار نگشاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همان در ناله طوفان می کنم با آن که می دانم</p></div>
<div class="m2"><p>جرس را عقده از دل ناله های زار نگشاید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلم دارد حضوری با خیال یار در خلوت</p></div>
<div class="m2"><p>که تا صبح قیامت در به روی یار نگشاید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زسیر باغ جنت داغ عاشق تازه می گردد</p></div>
<div class="m2"><p>دل آتش پرست از جلوه گلزار نگشاید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سری فردازد و عالم چون سر منصور می خواهد</p></div>
<div class="m2"><p>به هر مشت گلی آغوش رغبت دار نگشاید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به عمری ناله ای از دل نخیزد عندلیبان را</p></div>
<div class="m2"><p>اگر صائب درین گلشن لب گفتار نگشاید</p></div></div>