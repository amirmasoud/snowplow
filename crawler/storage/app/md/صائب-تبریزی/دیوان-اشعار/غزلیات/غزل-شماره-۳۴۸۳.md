---
title: >-
    غزل شمارهٔ ۳۴۸۳
---
# غزل شمارهٔ ۳۴۸۳

<div class="b" id="bn1"><div class="m1"><p>نه زر و سیم و نه باغ و نه دکان می ماند</p></div>
<div class="m2"><p>هرچه در راه خدا می دهی آن می ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آنچه امروز به جمعیت آن مغروری</p></div>
<div class="m2"><p>به تو آخر دل و چشم نگران می ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل بر این عمر مبندید که از صحبت تیر</p></div>
<div class="m2"><p>عاقبت خانه خالی به کمان می ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از جهان گذران کیست که آسان گذرد؟</p></div>
<div class="m2"><p>رفرف موج درین ریگ روان می ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزگاری است که دریا چو دهد قطره به ابر</p></div>
<div class="m2"><p>در عقب چشم حبابش نگران می ماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دل تنگ ندارم سر صحرای بهشت</p></div>
<div class="m2"><p>که دل تنگ به آن غنچه دهان می ماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خار خاری که ز رفتار تو در دلها هست</p></div>
<div class="m2"><p>خس و خاری است که از آب روان می ماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پرده شرم و حیا شهپر عنقا شده است</p></div>
<div class="m2"><p>پیر این عهد ز شوخی به جوان می ماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بس که در غارت دل جلوه او سرگرم است</p></div>
<div class="m2"><p>سایه هر گام ازان سرو روان می ماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لب فرو بستم از شکوه ز خرسندی نیست</p></div>
<div class="m2"><p>نفس سوخته دستم به دهان می ماند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نسبت روی تو با چهره گل بی بصری است</p></div>
<div class="m2"><p>کز عرق بر گل روی تو نشان می ماند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چه کند سبزه نورس به گرانجانی سنگ؟</p></div>
<div class="m2"><p>پای من در ته این خواب گران می ماند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با کمال سبکی بر دل خلق است گران</p></div>
<div class="m2"><p>زاهد خشک به ماه رمضان می ماند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زخم شمشیر به تدبیر بهم می آید</p></div>
<div class="m2"><p>تا قیامت اثر زخم زبان می ماند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صائب از غیرت آن زلف به خود می پیچم</p></div>
<div class="m2"><p>که ز هر حلقه به چشم نگران می ماند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نام هرکس که بلند از سخن صائب شد</p></div>
<div class="m2"><p>تا سخن هست بر اوراق جهان می ماند</p></div></div>