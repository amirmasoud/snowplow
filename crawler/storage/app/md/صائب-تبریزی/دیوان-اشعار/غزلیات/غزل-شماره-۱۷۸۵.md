---
title: >-
    غزل شمارهٔ ۱۷۸۵
---
# غزل شمارهٔ ۱۷۸۵

<div class="b" id="bn1"><div class="m1"><p>کدام زهره جبین گوشه نقاب شکست؟</p></div>
<div class="m2"><p>که رعشه ساغر زرین آفتاب شکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقاب شرم تو خواهد به یک طرف افتاد</p></div>
<div class="m2"><p>نمی شود نخورد فرد انتخاب، شکست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی کز آن لب میگون به باده قانع شد</p></div>
<div class="m2"><p>خمار باده گلرنگ را به آب شکست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به ماهتاب غلط می کند تماشایی</p></div>
<div class="m2"><p>ز خجلت تو ز بس رنگ آفتاب شکست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دگر ز بخیه مژگان بهم نمی آید</p></div>
<div class="m2"><p>ز انتظار تو در دیده ای که خواب شکست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شراب سوختگان می رسد ز پرده غیب</p></div>
<div class="m2"><p>خمار شعله ز خونابه کباب شکست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به باد داد سر خویش را ز بی مغزی</p></div>
<div class="m2"><p>کلاه گوشه به دریا اگر حباب شکست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دگر چگونه کنم در لباس دعوی زهد؟</p></div>
<div class="m2"><p>که زیر خرقه مرا شیشه شراب شکست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چسان احاطه کند فیض صبح را دل من؟</p></div>
<div class="m2"><p>که شیشه فلک از زور این شراب شکست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رهین منت دریا چرا شوم صائب</p></div>
<div class="m2"><p>مرا که تشنگی از موجه سراب شکست</p></div></div>