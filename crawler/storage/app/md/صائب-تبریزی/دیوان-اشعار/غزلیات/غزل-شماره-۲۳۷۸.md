---
title: >-
    غزل شمارهٔ ۲۳۷۸
---
# غزل شمارهٔ ۲۳۷۸

<div class="b" id="bn1"><div class="m1"><p>یاد باد آن بی حقیقت را که یاد ما نکرد</p></div>
<div class="m2"><p>بر فلک شد دود آه ما و سر بالا نکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوسه ها پیچید در مکتوب بهر دیگران</p></div>
<div class="m2"><p>وز تریها نامه خشکی به ما انشا نکرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه آمد نخلش از دست دعای ما به بار</p></div>
<div class="m2"><p>در برومندی به برگ سبز یاد ما نکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه گردیدیم خاک راهش از روی نیاز</p></div>
<div class="m2"><p>زیر پای خود نگاه از ناز و استغنا نکرد</p></div></div>