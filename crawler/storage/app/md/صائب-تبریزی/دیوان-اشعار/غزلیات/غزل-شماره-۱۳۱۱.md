---
title: >-
    غزل شمارهٔ ۱۳۱۱
---
# غزل شمارهٔ ۱۳۱۱

<div class="b" id="bn1"><div class="m1"><p>طاعت ظاهر طریق مردم آزاده نیست</p></div>
<div class="m2"><p>پرده بیگانگی اینجا به جز سجاده نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در صف مستان که بیرون رفتن از خود طاعت است</p></div>
<div class="m2"><p>بادبان کشتی می کمتر از سجاده نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از هوا مرغان فارغبال روزی می خورند</p></div>
<div class="m2"><p>در قفس هم رزق ما بی طالعان آماده نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لغزش مستانه ما عذرها دارد، ولی</p></div>
<div class="m2"><p>عذر ما را کی پذیرد هر که کار افتاده نیست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقشبندان معانی را برای مشق فکر</p></div>
<div class="m2"><p>تخته مشقی به از رخساره های ساده نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راه حرف از خنده گل عندلیبان یافتند</p></div>
<div class="m2"><p>دور باشی حسن را چون جبهه نگشاده نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیقراری لازم آغاز عشق افتاده است</p></div>
<div class="m2"><p>جوش خامی در زمان پختگی با باده نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دعوی آزادگی از سرو، رعنایی بود</p></div>
<div class="m2"><p>سرکشی صائب طریق مردم آزاده نیست</p></div></div>