---
title: >-
    غزل شمارهٔ ۱۰۳۵
---
# غزل شمارهٔ ۱۰۳۵

<div class="b" id="bn1"><div class="m1"><p>با لب خاموش حفظ آه کردن مشکل است</p></div>
<div class="m2"><p>از گره این رشته را کوتاه کردن مشکل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون قلم شق شد، سیاهی بیش بیرون می دهد</p></div>
<div class="m2"><p>منع دلهای دو نیم از آه کردن مشکل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می توان کردن به نشتر زنده خون مرده را</p></div>
<div class="m2"><p>خواب غفلت برده را آگاه کردن مشکل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جوهر از فولاد آسان است آوردن برون</p></div>
<div class="m2"><p>ریشه کن از سینه حب جاه کردن مشکل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون جرس مجموعه چاک است سر تا پای من</p></div>
<div class="m2"><p>حفظ این منزل ز چندین راه کردن مشکل است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هست تا دامن کشان سروی درین بستانسرا</p></div>
<div class="m2"><p>از گریبان دست ما کوتاه کردن مشکل است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می توان با رشته آسان گوهر شهوار سفت</p></div>
<div class="m2"><p>در دل سخت نکویان راه کردن مشکل است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر عزیزان این چنین گردند صائب خوار و زار</p></div>
<div class="m2"><p>امتیاز زعفران از کاه کردن کردن مشکل است</p></div></div>