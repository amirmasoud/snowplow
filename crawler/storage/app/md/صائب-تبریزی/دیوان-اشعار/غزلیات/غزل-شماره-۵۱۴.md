---
title: >-
    غزل شمارهٔ ۵۱۴
---
# غزل شمارهٔ ۵۱۴

<div class="b" id="bn1"><div class="m1"><p>تلخی عالم ناساز شراب است مرا</p></div>
<div class="m2"><p>تری بدگهران عالم آب است مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا ازان روی عرقناک، نظر دادم آب</p></div>
<div class="m2"><p>آب حیوان به نظر موج سراب است مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لب به دریوزه می تلخ نسازم چون جام</p></div>
<div class="m2"><p>آبرو جمع چو شد، عالم آب است مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست بی سوختگان شور مرا چون آتش</p></div>
<div class="m2"><p>می ز خونابه دلهای کباب است مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز در دوست که بیداری دل می بخشد</p></div>
<div class="m2"><p>تکیه بر هر چه کنم باعث خواب است مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می دهد شادی بی درد مرا غوطه به خون</p></div>
<div class="m2"><p>خنده کبک دری، چنگ عقاب است مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می دهم عرض به دشمن گره مشکل خویش</p></div>
<div class="m2"><p>از هوا چشم گشایش چو حباب است مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر چه همخانه دریای گرامی گهرم</p></div>
<div class="m2"><p>چون صدف، دانه روزی ز سحاب است مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کمتر از جنبش ابروست مرا دور نشاط</p></div>
<div class="m2"><p>خوشدلی چون مه نو پا به رکاب است مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تلخی زهر عتاب است گوارا بر من</p></div>
<div class="m2"><p>با شکرخنده خوبان شکراب است مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مطلب افتاده مرا تندی و بدخویی تو</p></div>
<div class="m2"><p>غرض از نامه نه امید جواب است مرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حسن بی پرده کند آب نگه را، ورنه</p></div>
<div class="m2"><p>دست، گستاخ به آن بند نقاب است مرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>راست کیشم، به نشان می رسد آخر تیرم</p></div>
<div class="m2"><p>خود حسابم، چه غم از روز حساب است مرا؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیست کاری به دورویان جهانم صائب</p></div>
<div class="m2"><p>روی دل از همه عالم به کتاب است مرا</p></div></div>