---
title: >-
    غزل شمارهٔ ۲۵۰
---
# غزل شمارهٔ ۲۵۰

<div class="b" id="bn1"><div class="m1"><p>حاجت دام و کمندی نیست در تسخیر ما</p></div>
<div class="m2"><p>گردش چشمی بود بس حلقه زنجیر ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما خراب از آب شمشیر تغافل گشته ایم</p></div>
<div class="m2"><p>می توان کردن به گرد دامنی تعمیر ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از عیار نامه ما دردمندان آگهند</p></div>
<div class="m2"><p>می شود در زخم ظاهر جوهر شمشیر ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون کمان هر چند مشت استخوانی گشته ایم</p></div>
<div class="m2"><p>می شود از جوشن گردون ترازو تیر ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل ز بیم غمزه از زلفش نمی آید برون</p></div>
<div class="m2"><p>بیشتر در پرده شب می چرد نخجیر ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در فضای خاطر ما تیر پیکان می شود</p></div>
<div class="m2"><p>آه می گردد گره در سینه دلگیر ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منزل نقل مکان ماست اوج لامکان</p></div>
<div class="m2"><p>وادی امکان ندارد عرصه شبگیر ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از خجالت چون نگردد تیشه فرهاد آب؟</p></div>
<div class="m2"><p>کوه را برداشت از جا ناله زنجیر ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواب ما با خواب چشم یار از یک پرده است</p></div>
<div class="m2"><p>هیچ کس بیرون نمی آرد سر از تعبیر ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گنجها در گوشه ویران ما در خاک هست</p></div>
<div class="m2"><p>آبروی سعی را گوهر کند تعمیر ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دیدن ما تلخکامان تلخ سازد کام را</p></div>
<div class="m2"><p>دایه گویا داد از پستان حنظل شیر ما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مادر از فرزند ناهموار خجلت می کشد</p></div>
<div class="m2"><p>خاک سر بالا نیارد کرد از تقصیر ما</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خود هم از زلف دراز خویش دربند بلاست</p></div>
<div class="m2"><p>یک سرش بر گردن یوسف بود زنجیر ما</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این که صائب دست ما از دامن او کوته است</p></div>
<div class="m2"><p>نارسایی های اقبال است دامنگیر ما</p></div></div>