---
title: >-
    غزل شمارهٔ ۶۱۶۴
---
# غزل شمارهٔ ۶۱۶۴

<div class="b" id="bn1"><div class="m1"><p>آن کف نظارگی، این از دو عالم می برد</p></div>
<div class="m2"><p>در میان این دو یوسف فرق ای بینا ببین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر ندیدی ترجمان رازهای غیب را</p></div>
<div class="m2"><p>آن خط نازک رقم را گرد آن لبها ببین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در چنین وقتی که از خط صبح محشر می دمد</p></div>
<div class="m2"><p>چشم خواب آلود آن معشوق بی پروا ببین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این سفر کوته نمی گردد به شبگیر بلند</p></div>
<div class="m2"><p>عمر جاویدان به دست آر آن قد رعنا ببین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آسمان را یک نفس از شور عشق آرام نیست</p></div>
<div class="m2"><p>زین می پر زور دست افشانی مینا ببین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیده را صائب ز خورشید قیامت آب ده</p></div>
<div class="m2"><p>بعد ازان بر چهره آن آتشین سیما ببین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روی در میخانه کن آرامش دلها ببین</p></div>
<div class="m2"><p>عالمی را فارغ از اندیشه فردا ببین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این تعین چون حباب از بسته چشمی های توست</p></div>
<div class="m2"><p>چشم بگشا، هیچی خود را درین دریا ببین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عشق بی معشوق هیهات است گردد جلوه گر</p></div>
<div class="m2"><p>در لباس بید مجنون جلوه لیلی ببین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نسبت دیوانه و شهرست طوفان و تنور</p></div>
<div class="m2"><p>عرض سودای مرا در دامن صحرا ببین</p></div></div>