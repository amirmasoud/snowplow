---
title: >-
    غزل شمارهٔ ۶۳۵۸
---
# غزل شمارهٔ ۶۳۵۸

<div class="b" id="bn1"><div class="m1"><p>ز بی قراری من می کند سفر بالین</p></div>
<div class="m2"><p>ز دست خویش کنم چو سبو مگر بالین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همان ز پستی بالین نمی برد خوابم</p></div>
<div class="m2"><p>ز گرد بالش گردون کنم اگر بالین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بی قراری من چون سپند جست از جای</p></div>
<div class="m2"><p>نشست هر که مرا چون چراغ بر بالین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز گرمی جگرم لعل آتشین گردد</p></div>
<div class="m2"><p>به وقت خواب کنم خشت خام اگر بالین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز دست و تیغ خزان گوییا خبر دارد</p></div>
<div class="m2"><p>که می کند گل این بوستان سپر بالین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا سری است که چون لاله داغدار شود</p></div>
<div class="m2"><p>کنم ز کاسه زانوی خود اگر بالین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عجب نباشد اگر بال و پر برون آرد</p></div>
<div class="m2"><p>کشید از سر من بس که دردسر بالین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی به ملک غریبی عزیز می گردد</p></div>
<div class="m2"><p>که در وطن کند از سنگ چون گهر بالین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چگونه خواب پریشان نسازدم بیدار؟</p></div>
<div class="m2"><p>که کج گذاشت مرا زلف زیر سر بالین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا به داغ جنون نیست الفت امروزی</p></div>
<div class="m2"><p>همیشه داشت ز سرگرمیم خطر بالین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رهین پرتو منت چرا شوم صائب؟</p></div>
<div class="m2"><p>مرا که از تب گرم است شمع بر بالین</p></div></div>