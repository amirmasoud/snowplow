---
title: >-
    غزل شمارهٔ ۱۶۱۵
---
# غزل شمارهٔ ۱۶۱۵

<div class="b" id="bn1"><div class="m1"><p>هیچ کس غیر تو در پرده بینایی نیست</p></div>
<div class="m2"><p>حسن مستور ترا جز تو تماشایی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشرق و مغربش از رخنه دل باشد و بس</p></div>
<div class="m2"><p>همچو مه پرتو رخسار تو هر جایی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیقراران تو منزل نشناسند که چیست</p></div>
<div class="m2"><p>ریگ را ماندگی از بادیه پیمایی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر سر دولت تنهایی خود می لرزد</p></div>
<div class="m2"><p>اضطراب دل خورشید ز تنهایی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طوطی من سبق از سینه خود می گیرد</p></div>
<div class="m2"><p>پشت آیینه مرا مانع گویایی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناخدا لنگر بیتابی خلق است، ارنه</p></div>
<div class="m2"><p>کشتیی نیست درین بحر که دریایی نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل بود مانع بینایی عارف صائب</p></div>
<div class="m2"><p>چشم پوشیدن ما مانع بینایی نیست</p></div></div>