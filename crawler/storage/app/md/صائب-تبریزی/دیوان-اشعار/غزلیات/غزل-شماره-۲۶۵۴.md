---
title: >-
    غزل شمارهٔ ۲۶۵۴
---
# غزل شمارهٔ ۲۶۵۴

<div class="b" id="bn1"><div class="m1"><p>چون اثر نگذاشت ازمن غم ز غمخواری چه سود؟</p></div>
<div class="m2"><p>چون نماند از دل بجا چیزی ز دلداری چه سود؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کوه طاقت برنمی آید به موج حادثات</p></div>
<div class="m2"><p>پیش این سیلاب بی زنهار خودداری چه سود؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مطلب از بیدار خوابی نیست جز اصلاح خود</p></div>
<div class="m2"><p>چون به فکر خود نمی افتی ز بیداری چه سود؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زخم شمشیر قضا از سینه می روید چو گل</p></div>
<div class="m2"><p>از زره پوشی چه حاصل، از سپرداری چه سود؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می کند هموار سوهان تیغ ناهموار را</p></div>
<div class="m2"><p>هر کجا باید درشتی کرد همواری چه سود؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرصتی تا هست دل را کن تهی از اشک و آه</p></div>
<div class="m2"><p>وقت چون گردید فوت از گریه و زاری چه سود؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چند بتوان ساخت موی خویش چون قیر ازخضاب؟</p></div>
<div class="m2"><p>چون نمی گردد جوان دل زین سیه کاری چه سود؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست حرف تلخ را تأثیر دل دلمردگان</p></div>
<div class="m2"><p>کور چون شد چشم باطن غوره افشاری چه سود؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیش سیلاب فنا یکسان بود چون کوه و کاه</p></div>
<div class="m2"><p>از گرانجانی چه حاصل، از سبکباری چه سود؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بار را نتوان به مکر و حیله رام خویش کرد</p></div>
<div class="m2"><p>چون طرف عیارتر از توست عیاری چه سود؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چشم بینا می کند نزدیک راه دور را</p></div>
<div class="m2"><p>نیست چون دردیده نوری از طلبکاری چه سود؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در جوانی می توان برخورد صائب از حیات</p></div>
<div class="m2"><p>در بهار این چنین تخمی نمی کاری چه سود</p></div></div>