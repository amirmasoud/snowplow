---
title: >-
    غزل شمارهٔ ۲۰۱۳
---
# غزل شمارهٔ ۲۰۱۳

<div class="b" id="bn1"><div class="m1"><p>از بس نهاده ام به دل داغدار دست</p></div>
<div class="m2"><p>گشته است داغدار مرا لاله وار دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای ساقیی که توبه ما را شکسته ای</p></div>
<div class="m2"><p>زنهار از شکسته نوازی مدار دست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ریزند می چو شیشه مگر در گلوی من</p></div>
<div class="m2"><p>می لرزد این چنین که مرا از خمار دست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای گل چه آفتی تو که از خون بلبلان</p></div>
<div class="m2"><p>در مهد غنچه بود ترا در نگار دست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در عهد خوبی تو گذارند گلرخان</p></div>
<div class="m2"><p>گاهی به روی و گاه به دل غنچه وار دست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از اشتیاق دامن آن سرو خوش خرام</p></div>
<div class="m2"><p>از آستین چو تاک برآرم هزار دست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زان پر گل است گلشن حسنت که می رود</p></div>
<div class="m2"><p>از دیدنت نظارگیان را ز کار دست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گوهر شود ز گرد یتیمی گرانبها</p></div>
<div class="m2"><p>ای سنگدل مشوی ازین خاکسار دست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دریا خمش به پنجه مرجان نمی شود</p></div>
<div class="m2"><p>سودی نمی دهد به دل بیقرار دست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می کرد در تهیه افسوس کوتهی</p></div>
<div class="m2"><p>می بود همچو سرو مرا گر هزار دست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از امتحان غمزه خونخوار درگذر</p></div>
<div class="m2"><p>نتوان گذشتن به دم ذوالفقار دست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صد بار جوی خون شده است آستین من</p></div>
<div class="m2"><p>تا برده ام به لعل آب آن نگار دست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون خرده زری که ترا هست رفتنی است</p></div>
<div class="m2"><p>در آستین گره چه کنی غنچه وار دست؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دستی نشد بلند پی دستگیریم</p></div>
<div class="m2"><p>شد توتیا اگر چه مرا زیربار دست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بی بادبان سفینه به ساحل نمی رسد</p></div>
<div class="m2"><p>صائب ز طرف دامن دل بر مدار دست</p></div></div>