---
title: >-
    غزل شمارهٔ ۴۹۷۹
---
# غزل شمارهٔ ۴۹۷۹

<div class="b" id="bn1"><div class="m1"><p>خط دمیده است ز لعل لب شکرشکنش ؟</p></div>
<div class="m2"><p>یا به خون چشم سیه کرده عقیق یمنش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این چه لطف است که برخود چونظر اندازد</p></div>
<div class="m2"><p>یوسفستان شود ازپرتو عارض بدنش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آتشین لعل لب یار فروغی دارد</p></div>
<div class="m2"><p>که سخن همچو گهر آب شود در دهنش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یوسفی را که من ازخیل نظر بازانم</p></div>
<div class="m2"><p>پرده دیده یعقوب بود پیرهنش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داغ عشق از دل افسرده اغیار مجو</p></div>
<div class="m2"><p>این سهیلی است که باشد دل خونین یمنش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی ازجمله خونابه کشان است سهیل</p></div>
<div class="m2"><p>دلبری را که منم واله سیب ذقنش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرگز ازسیلی اخوان نرود بریوسف</p></div>
<div class="m2"><p>آنچه از سبزه خط رفت به برگ سمنش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشم روزن ز شکرخواب نگردد بیدار</p></div>
<div class="m2"><p>درحریمی که بخندد لب شکرشکنش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر چه در بسته به یک کس نگذارند بهشت</p></div>
<div class="m2"><p>چه بهشتی است گذارند حریفان به منش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صائب آن لب به خموشی جگر عالم سوخت</p></div>
<div class="m2"><p>تاچه باشد نمک خنده و شور سخنش</p></div></div>