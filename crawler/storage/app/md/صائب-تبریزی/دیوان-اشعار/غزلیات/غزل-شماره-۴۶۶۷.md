---
title: >-
    غزل شمارهٔ ۴۶۶۷
---
# غزل شمارهٔ ۴۶۶۷

<div class="b" id="bn1"><div class="m1"><p>چین پیشانی ما شد مه عید آخر کار</p></div>
<div class="m2"><p>آن چه می جست دل غمزده، دید آخر کار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی نسیم سحری غنچه ما خندان شد</p></div>
<div class="m2"><p>قفل از پره خود ساخت کلید آخر کار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماه عیدی که ز آفاق طلب می کردیم</p></div>
<div class="m2"><p>از غبار دل ما گشت پدید آخر کار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دانه سوخته ماز عرق ریزی سعی</p></div>
<div class="m2"><p>چون شرر از جگر سنگ دمید آخر کار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آب شد گر چه دل شبنم ما از گردش</p></div>
<div class="m2"><p>اینقدر شد که به خورشید رسید آخر کار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ورق دیده یعقوب همین مضمون است</p></div>
<div class="m2"><p>که شود صبح طرب چشم سفید آخر کار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه از چهره گل شبنم ما دور افتاد</p></div>
<div class="m2"><p>به لب تشنه خورشید رسید آخرکار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کاش در جوش گل از خاک مرا بر می داشت</p></div>
<div class="m2"><p>پرو بالی که به فریاد رسیدآخرکار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ثمر تلخی ایام تهیدستی بود</p></div>
<div class="m2"><p>ازنبات آنچه چشاندند به بید آخرکار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از وصال رخ او کامرواشد صائب</p></div>
<div class="m2"><p>انتقام خود از ایام کشید آخرکار</p></div></div>