---
title: >-
    غزل شمارهٔ ۱۶۲
---
# غزل شمارهٔ ۱۶۲

<div class="b" id="bn1"><div class="m1"><p>چهره شد نیلوفری از سیلی اخوان مرا</p></div>
<div class="m2"><p>خوش گلی آخر شکفت از گلشن احزان مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیغ بر فرقم زنند و گوهر از دستم برند</p></div>
<div class="m2"><p>چون صدف شد دشمن جان گوهر رخشان مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل چو رو گرداند، بر گرداندن او مشکل است</p></div>
<div class="m2"><p>روی دل تا برنگردیده است، بر گردان مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ذوق همچشمی ندارد شهرتم با آفتاب</p></div>
<div class="m2"><p>گرد عالم از چه دارد چرخ سرگردان مرا؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که بر من پرده پوشد خویش را رسوا کند</p></div>
<div class="m2"><p>من نه آن شمعم که بتوان داشتن پنهان مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیستم پیراهن یوسف، چرا هر جا روم</p></div>
<div class="m2"><p>خون تهمت می چکد از گوشه دامان مرا؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست صائب در خرابات مغان دریا دلی</p></div>
<div class="m2"><p>تا به یک ساغر کند شرمنده احسان مرا</p></div></div>