---
title: >-
    غزل شمارهٔ ۵۶۹۸
---
# غزل شمارهٔ ۵۶۹۸

<div class="b" id="bn1"><div class="m1"><p>سبک به چشم تو از شیوه وفا شده‌ام</p></div>
<div class="m2"><p>سزای من که به بیگانه آشنا شده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی به خاک چو من گوهری نیندازد</p></div>
<div class="m2"><p>به سهو از گره روزگار وا شده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز خون شکوه دهانم پرست چون سوفار</p></div>
<div class="m2"><p>خدنگ راست روم از هدف خطا شده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ملایمت شکند شاخ تندخویان را</p></div>
<div class="m2"><p>ز خار نیست غمم تا برهنه‌پا شده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کیم من و چه بود رزق همچو من موری</p></div>
<div class="m2"><p>که بار خاطر این هفت آسیا شده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمک به دیده من رنگ خواب می‌ریزد</p></div>
<div class="m2"><p>ز چشم سرمه‌فریب تو تا جدا شده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هنوز نقش تعلق به لوح دل باقی است</p></div>
<div class="m2"><p>ز فقر نیست که قانع به بوریا شده‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به ناله چون جرسم صد زبان آهن هست</p></div>
<div class="m2"><p>ز بیم خوی تو چون غنچه بی‌صدا شده‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز هیچ هم‌نفسی روی دل نمی‌بینم</p></div>
<div class="m2"><p>چو پشت آینه زان روی بی‌صفا شده‌ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میان اهل سخن امتیاز من صائب</p></div>
<div class="m2"><p>همین بس است که با طرز آشنا شده‌ام</p></div></div>