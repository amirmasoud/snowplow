---
title: >-
    غزل شمارهٔ ۳۷۱۱
---
# غزل شمارهٔ ۳۷۱۱

<div class="b" id="bn1"><div class="m1"><p>جز آن دهن که ازو خنده سر برون آرد</p></div>
<div class="m2"><p>که دیده پسته که از خود شکر برون آرد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فغان که طوق گلوگیر عشق، قمری را</p></div>
<div class="m2"><p>امان نداد که از بیضه سر برون آرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل از عزیزی غربت نمی توان برداشت</p></div>
<div class="m2"><p>ز گوهر آب محال است سر برون آرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برون نمی رود از مجمر تو نکهت عود</p></div>
<div class="m2"><p>ز محفل تو کسی چون خبر برون آرد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به روی سخت توان خرده از بخیل گرفت</p></div>
<div class="m2"><p>که آهن از دل خارا شرر برون آرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر ز کنج قناعت قدم برون ننهی</p></div>
<div class="m2"><p>چو عنکبوت ترا رزق پر برون آرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو بیجگر کنی اندیشه از اجل، ورنه</p></div>
<div class="m2"><p>ز شوق راه فنا مور پر برون آرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هزار ناخن تدبیر غوطه زد در خون</p></div>
<div class="m2"><p>که تا ز عقده زلف تو سر برون آرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همان ز شوق دل خویش می خورد صائب</p></div>
<div class="m2"><p>اگر ز جیب گهر رشته سر برون آرد</p></div></div>