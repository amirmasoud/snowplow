---
title: >-
    غزل شمارهٔ ۴۰۶۶
---
# غزل شمارهٔ ۴۰۶۶

<div class="b" id="bn1"><div class="m1"><p>هر کس ز قید تن دل روشن برآورد</p></div>
<div class="m2"><p>اخگر برون ز توده خاکستر آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست از طلب مکش که سمندر ز جذب عشق</p></div>
<div class="m2"><p>از بال وپر بهم زدن آتش برآورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشمی که ساخت سرمه عبرت منورش</p></div>
<div class="m2"><p>از حقه حباب برون گوهر آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیکان قرار در تن مردم نمی کند</p></div>
<div class="m2"><p>دل هر زمان ز جای دگر سربرآورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آرزو فتاد برون آدم از بهشت</p></div>
<div class="m2"><p>تا آرزو ترا چه بلا برسر آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان پرورست صحبت پاکیزه گوهران</p></div>
<div class="m2"><p>هرکس به بحر موم برد عنبر آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شب زنده دار باش که گردد سفید روی</p></div>
<div class="m2"><p>آیینه چون پناه به خاکسترآورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ایمن مباش ازان خط مشکین به گردلب</p></div>
<div class="m2"><p>کاین مور زود گرد ز شکر برآورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از زلف وخط گرفتن دل سخت مشکل است</p></div>
<div class="m2"><p>بیرون چگونه مهره کس از ششدر آورد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از روی درد بلبل اگر ناله سر کند</p></div>
<div class="m2"><p>گل را نفس گسسته به زیر پرآورد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از شش جهت به دل غم دنیا نهاد روی</p></div>
<div class="m2"><p>یک تن چگونه حمله بر این لشکر آورد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ساز غضب حلیم گرانسنگ را سبک</p></div>
<div class="m2"><p>کف وقت جوش بحر گهر بر سرآورد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جان تازه می شود ز پریخانه خیال</p></div>
<div class="m2"><p>صائب چگونه سر ز گریبان برآورد</p></div></div>