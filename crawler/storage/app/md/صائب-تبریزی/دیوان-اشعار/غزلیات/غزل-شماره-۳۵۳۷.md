---
title: >-
    غزل شمارهٔ ۳۵۳۷
---
# غزل شمارهٔ ۳۵۳۷

<div class="b" id="bn1"><div class="m1"><p>رفع دلتنگی من نشأه صهبا نکند</p></div>
<div class="m2"><p>هیچ کس غنچه پیکان به نفس وا نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سپر، تیر قضا روی نمی گرداند</p></div>
<div class="m2"><p>سیل از خانه در بسته محابا نکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر شود دامن پیراهن یوسف صد چاک</p></div>
<div class="m2"><p>رخنه در پرده ناموس زلیخا نکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سعی در خون خود از خصم فزونتر دارد</p></div>
<div class="m2"><p>هرکه با دشمن خونخوار مدارا نکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شود از گوهر عبرت صدفش سینه بحر</p></div>
<div class="m2"><p>دوربینی که نگه خرج تماشا نکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می کند زلف دراز تو به دلهای حزین</p></div>
<div class="m2"><p>آنچه با خسته روانان شب یلدا نکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخن تلخ نگردد به تبسم شیرین</p></div>
<div class="m2"><p>چاره تلخی می قهقه مینا نکند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرکه چون شانه نسازد دل خود را صد چاک</p></div>
<div class="m2"><p>پنجه در پنجه آن زلف چلیپا نکند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سنگ دارند ز دیوانه دریغ اطفالش</p></div>
<div class="m2"><p>چون ازین شهر کسی روی به صحرا نکند؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سوز عشق از سر عاشق به مداوا نرود</p></div>
<div class="m2"><p>که علاج تب خورشید مسیحا نکند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می رسد روزیش از عالم بالا صائب</p></div>
<div class="m2"><p>چون صدف هرکه دهن باز به دریا نکند</p></div></div>