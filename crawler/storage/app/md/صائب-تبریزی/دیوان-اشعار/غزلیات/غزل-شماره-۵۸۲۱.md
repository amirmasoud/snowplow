---
title: >-
    غزل شمارهٔ ۵۸۲۱
---
# غزل شمارهٔ ۵۸۲۱

<div class="b" id="bn1"><div class="m1"><p>چون شمع چند من به زبان گفتگو کنم؟</p></div>
<div class="m2"><p>روشندلی کجاست به جان گفتگو کنم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تلقین خون مرده دلم را سیاه کرد</p></div>
<div class="m2"><p>تا چند با سیاه دلان گفتگو کنم؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روشندلی نمانده درین باغ و بوستان</p></div>
<div class="m2"><p>با خود مگر چو آب روان گفتگو کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خیزد ز شیشه خانه دل بانگ الامان</p></div>
<div class="m2"><p>هر جا من شکسته زبان گفتگو کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لوح سیاه کرده پذیرای نقش نیست</p></div>
<div class="m2"><p>چون خامه من به ساده دلان گفتگو کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با تیغ او که از رگ جانهاست جوهرش</p></div>
<div class="m2"><p>چون من برای خرده جان گفتگو کنم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب ز پیچ و تاب گره می شود سخن</p></div>
<div class="m2"><p>گاهی کز آن دهان و میان گفتگو کنم</p></div></div>