---
title: >-
    غزل شمارهٔ ۲۵۱۰
---
# غزل شمارهٔ ۲۵۱۰

<div class="b" id="bn1"><div class="m1"><p>پرده پوشی عشق عالمسوز را پیدا کند</p></div>
<div class="m2"><p>پرده تبخال تب را بیشتر رسوا کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خرده راز محبت می شکافد سینه را</p></div>
<div class="m2"><p>این شرار شوخ کار تیشه با خارا کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر نیاید هر تنک ظرفی به حفظ راز عشق</p></div>
<div class="m2"><p>باده پرزور کار سنگ را مینا کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تنگدستی نفس سرکش را شود رهبر به حق</p></div>
<div class="m2"><p>ابر چون بی آب گردد روی در دریا کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی خیالش می شود در دل مصور بی نقاب؟</p></div>
<div class="m2"><p>حسن محجوبی که بر آیینه استغنا کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست از عرض تجمل تنگ چشمان را گزیر</p></div>
<div class="m2"><p>دانه صد تیغ زبان در خوشه ای پیدا کند</p></div></div>