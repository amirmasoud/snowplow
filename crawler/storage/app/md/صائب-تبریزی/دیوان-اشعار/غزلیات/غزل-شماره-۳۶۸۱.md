---
title: >-
    غزل شمارهٔ ۳۶۸۱
---
# غزل شمارهٔ ۳۶۸۱

<div class="b" id="bn1"><div class="m1"><p>ز بردباری من موج می شود لنگر</p></div>
<div class="m2"><p>ز خاکساری من صدر آستان گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فلک ز بار غم من به خاک بندد نقش</p></div>
<div class="m2"><p>زمین ز بال و پر شوقم آسمان گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر چه خضر ز آب حیات سیراب است</p></div>
<div class="m2"><p>ز یاد تیغ تواش آب در دهان گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شدی چو پیر ز اهل جهان کناری گیر</p></div>
<div class="m2"><p>که هرکه مانده شود بار کاروان گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به قسمت ازلی باش از جهان خرسند</p></div>
<div class="m2"><p>که چون فضول شود میهمان گران گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو ماه عید عزیز جهان شود صائب</p></div>
<div class="m2"><p>ز بار درد قد هر که چون کمان گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نسیم گل ز سبکروحیم گران گردد</p></div>
<div class="m2"><p>ز چرب نرمی من مغز استخوان گردد</p></div></div>