---
title: >-
    غزل شمارهٔ ۴۴۴۱
---
# غزل شمارهٔ ۴۴۴۱

<div class="b" id="bn1"><div class="m1"><p>خورشید اگر از چشم کسان آب گشاید</p></div>
<div class="m2"><p>رخساره گلرنگ تو خوناب گشاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از چشمه خورشید مجو آب مروت</p></div>
<div class="m2"><p>کاین چشمه ز چشم دگران آب گشاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در نیم نفس می شود از پاک فروشان</p></div>
<div class="m2"><p>هر کس چو کتان بار به مهتاب گشاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بحر سر از حلقه گرداب برآرد</p></div>
<div class="m2"><p>هر کس که کمر در ره سیلاب گشاید</p></div></div>