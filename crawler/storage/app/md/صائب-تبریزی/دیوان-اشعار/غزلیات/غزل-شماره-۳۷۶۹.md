---
title: >-
    غزل شمارهٔ ۳۷۶۹
---
# غزل شمارهٔ ۳۷۶۹

<div class="b" id="bn1"><div class="m1"><p>قدح به حوصله ما چه می تواند کرد؟</p></div>
<div class="m2"><p>سفینه با دل دریا چه می تواند کرد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر دو یار موافق زبان یکی سازند</p></div>
<div class="m2"><p>فلک به یک تن تنها چه می تواند کرد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>علاج درد خداداد صبر و تسلیم است</p></div>
<div class="m2"><p>به درد عشق مداوا چه می تواند کرد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حصار عافیت روزگار همواری است</p></div>
<div class="m2"><p>هجوم سیل به صحرا چه می تواند کرد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کمند جذبه کوتاه خانه یعقوب</p></div>
<div class="m2"><p>به اشتیاق زلیخا چه می تواند کرد؟</p></div></div>