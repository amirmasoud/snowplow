---
title: >-
    غزل شمارهٔ ۴۶۳۷
---
# غزل شمارهٔ ۴۶۳۷

<div class="b" id="bn1"><div class="m1"><p>از می گلرنگ می گردد اگر پیمانه سیر</p></div>
<div class="m2"><p>می شود از خوردن خون هم دل دیوانه سیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میوه جنت اگر برآدمی گردد گران</p></div>
<div class="m2"><p>می شود از سنگ طفلان هم دل دیوانه سیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اشتهای آتش سوزان ندارد سوختن</p></div>
<div class="m2"><p>حرص را کی می توان کردن زآب ودانه سیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می شود سیراب از شبنم اگر ریگ روان</p></div>
<div class="m2"><p>می کند مخمور رااز باده هم پیمانه سیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرخ سنگین دل نگردداز شکست دل ملول</p></div>
<div class="m2"><p>نیست ممکن آسیارا ساختن از دانه سیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیری از خوان سپهر سفله محض آرزوست</p></div>
<div class="m2"><p>از جگرخوردن مگرگردم درین غمخانه سیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حلقه های زلف سیر از دلربایی می شود</p></div>
<div class="m2"><p>گر بود ممکن که گرددچشم دام از دانه سیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عارفان از دیدن حسن مجازآسوده اند</p></div>
<div class="m2"><p>تشنه خم کی شود از شیشه و پیمانه سیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لقمه ای برخوان گردون نیست بی خون جگر</p></div>
<div class="m2"><p>چون نگردد میهمان از جان درین غمخانه سیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کیست صائب از تردد نفس رامانع شود</p></div>
<div class="m2"><p>کی شود مور حریص از جستجوی دانه سیر</p></div></div>