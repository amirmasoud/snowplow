---
title: >-
    غزل شمارهٔ ۲۷۱۷
---
# غزل شمارهٔ ۲۷۱۷

<div class="b" id="bn1"><div class="m1"><p>دل ز احیای شب دیجور روشن می شود</p></div>
<div class="m2"><p>زین جواهر سرمه چشم کور روشن می شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خویش را زیر و زبر کن کز فروغ آفتاب</p></div>
<div class="m2"><p>بیشتر ویرانه از معمور روشن می شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خط شبرنگ می گردد نمایان آن دهن</p></div>
<div class="m2"><p>راه این تنگ شکر از مور روشن می شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با دل آزاری نگردد جمع حسن عاقبت</p></div>
<div class="m2"><p>ز آتش آخر خانه زنبور روشن می شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با دل سنگین نیم از رحمت حق ناامید</p></div>
<div class="m2"><p>کز چراغان نجلی طور روشن می شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شمع بی فانوس می سازد دل ما را سیاه</p></div>
<div class="m2"><p>دیده ما از رخ مستور روشن می شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شمع کافوری ندارد سود بر روی مزار</p></div>
<div class="m2"><p>صائب از نور عبادت گور روشن می شود</p></div></div>