---
title: >-
    غزل شمارهٔ ۶۹۲۹
---
# غزل شمارهٔ ۶۹۲۹

<div class="b" id="bn1"><div class="m1"><p>تا کی ز کف عنان توکل رها کنی؟</p></div>
<div class="m2"><p>از نقش پای راهروان رهنما کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون حلقه، دیده نگران شو تمام عمر</p></div>
<div class="m2"><p>شاید به روی خود در توفیق وا کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز نقش یوسفی نبود در بساط صبر</p></div>
<div class="m2"><p>تو جهد کن که آینه را (با صفا کنی)</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اطعام، رزق روح و طعام است (رزق تن)</p></div>
<div class="m2"><p>تا کی ز رزق روح به تن اکتفا کنی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست خود از نگار علایق بشوی پاک</p></div>
<div class="m2"><p>تا صد گره گشاده به دست دعا کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در نامرادی این همه بیداد می کنی</p></div>
<div class="m2"><p>گر چرخ بر مراد تو گردد چها کنی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آشفتگی ز مغز ( نمی رود)</p></div>
<div class="m2"><p>دستار نیست (این که ز سر زود وا کنی)</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قالب تهی ز خویش ( )</p></div>
<div class="m2"><p>چون بهله دست (در کمر مدعا کنی)</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تنگ شکر ( )</p></div>
<div class="m2"><p>گر خوابگاه (خویشتن از بوریا کنی)</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا کی دهان خویش ( )</p></div>
<div class="m2"><p>چند اکتفا ( )</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>) در خانه، کور (</p></div>
<div class="m2"><p>) عصا کنی (</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از خودسری و بی بصری، چند چون حباب</p></div>
<div class="m2"><p>صائب ز بحر خانه خود را جدا کنی؟</p></div></div>