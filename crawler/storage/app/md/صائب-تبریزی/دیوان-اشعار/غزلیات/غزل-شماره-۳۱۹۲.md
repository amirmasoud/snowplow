---
title: >-
    غزل شمارهٔ ۳۱۹۲
---
# غزل شمارهٔ ۳۱۹۲

<div class="b" id="bn1"><div class="m1"><p>گر از نظاره خورشید در چشم آب می‌آید</p></div>
<div class="m2"><p>ز روی لاله‌رنگش در نظر خوناب می‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آن محفل که بی‌آتش سپند از جای برخیزد</p></div>
<div class="m2"><p>کجا خودداری از پروانه بی‌تاب می‌آید؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندارد صیدی از من صیدگاه عشق لاغرتر</p></div>
<div class="m2"><p>که از قتلم به چشم جوهر تیغ آب می‌آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر شد نرم یاقوت لب او از غبار خط؟</p></div>
<div class="m2"><p>که حرف بوسه از دل بر زبان بی‌تاب می‌آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همانا بخت من از نارسایی‌ها برون آمد</p></div>
<div class="m2"><p>که بی‌تکلیف در ویرانه‌ام سیلاب می‌آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل آگاه در پیری ز غفلت بیش می‌لرزد</p></div>
<div class="m2"><p>که وقت صبح اکثر شبروان را خواب می‌آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو ماهی گر برآرم پر درین دریا عجب نبود</p></div>
<div class="m2"><p>که هر موجی به چشم وحشتم قلاب می‌آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنان نازک شده است از گریه کردن پرده چشمم</p></div>
<div class="m2"><p>که آبم در نظر از پرتو مهتاب می‌آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نباشد پرده‌پوشی تیر کج را چون کمان صائب</p></div>
<div class="m2"><p>کجا زاهد برون از گوشه محراب می‌آید؟</p></div></div>