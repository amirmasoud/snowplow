---
title: >-
    غزل شمارهٔ ۶۳۸۳
---
# غزل شمارهٔ ۶۳۸۳

<div class="b" id="bn1"><div class="m1"><p>از خود برون نرفته هوای سفر مکن</p></div>
<div class="m2"><p>این راه را به پای زمین گیر سر مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در قلزمی که ابر کرم موج می زند</p></div>
<div class="m2"><p>اندیشه چون حباب ز دامان تر مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوهر چه صرفه می برد از روی سخت سنگ؟</p></div>
<div class="m2"><p>تا ممکن است عربده با بدگهر مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با قصد کار بنده مأمور را چه کار؟</p></div>
<div class="m2"><p>در کارهای حق سخن از خیر و شر مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از زخم خار یک دهن خنده است گل</p></div>
<div class="m2"><p>ای سست رگ ملاحظه از نیشتر مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سود سفر بود گذراندن ز همرهان</p></div>
<div class="m2"><p>زنهار با رفیق موافق سفر مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>معشوق تازه رو خط آزادی غم است</p></div>
<div class="m2"><p>در گلشنی که سرو نباشد گذر مکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای زاهد فسرده، دل از عشق جمع دار</p></div>
<div class="m2"><p>ای خون مرده دغدغه از نیشتر مکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواهی نریزد از مژه ات اشک آتشین</p></div>
<div class="m2"><p>در روی آفتاب جبینان نظر مکن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در توست هر چه می طلبی صائب از جهان</p></div>
<div class="m2"><p>بیرون ز خود به هیچ مقامی سفر مکن</p></div></div>