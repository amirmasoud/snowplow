---
title: >-
    غزل شمارهٔ ۳۸۱۰
---
# غزل شمارهٔ ۳۸۱۰

<div class="b" id="bn1"><div class="m1"><p>نفس به سینه ام از اضطراب می سوزد</p></div>
<div class="m2"><p>چنان که تیر شهاب از شتاب می سوزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز قید عقل در اقلیم عشق فارغ باش</p></div>
<div class="m2"><p>که سایه در قدم آفتاب می سوزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طراوت تو کند سبز تخم سوخته را</p></div>
<div class="m2"><p>خوش آن کتان که درین ماهتاب می سوزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خون سوختگان عشق مجلس افروزست</p></div>
<div class="m2"><p>چراغ شعله به اشک کباب می سوزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گلی که گریه گرم من است میرابش</p></div>
<div class="m2"><p>ز شبنمش جگر آفتاب می سوزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازان زمان که لب از خون گرم من تر کرد</p></div>
<div class="m2"><p>هنوز در جگر تیغ آب می سوزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنان که شهپر عقل از شراب آتشناک</p></div>
<div class="m2"><p>ز آفتاب رخ او نقاب می سوزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا جدایی او سوخت، وقت شبنم خوش</p></div>
<div class="m2"><p>که در مشاهده آفتاب می سوزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگرچه در دل دریاست جای من صائب</p></div>
<div class="m2"><p>ز تشنگی جگرم چون سراب می سوزد</p></div></div>