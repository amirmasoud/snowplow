---
title: >-
    غزل شمارهٔ ۸۳۳
---
# غزل شمارهٔ ۸۳۳

<div class="b" id="bn1"><div class="m1"><p>از آه روز گردان شبهای تار خود را</p></div>
<div class="m2"><p>آیینه دو رو کن لیل و نهار خود را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ملک دل مگردان مطلق عنان هوس را</p></div>
<div class="m2"><p>از دست باد بستان مشت غبار خود را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان گوهر گرامی هرگز خبر نیابی</p></div>
<div class="m2"><p>از گریه تا نسازی دریا کنار خود را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلسوزی عزیزان چون برق در گذارست</p></div>
<div class="m2"><p>از سوز دل برافروز شمع مزار خود را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیکاری و توکل دورست از مروت</p></div>
<div class="m2"><p>بر دوش خلق مفکن زنهار بار خود را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آب و هوا و آتش مرکز شناس گشتند</p></div>
<div class="m2"><p>تو بی خبر ندانی راه دیار خود را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دایم بود فروزان چون آتش دل لعل</p></div>
<div class="m2"><p>هر کس نداد بیرون از دل شرار خود را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواهی که آسمان ها در بر رخت نبندند</p></div>
<div class="m2"><p>با خاک کن برابر اول حصار خود را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زان چشم های میگون شرمی بدار صائب</p></div>
<div class="m2"><p>از هر شراب تلخی مشکن خمار خود را</p></div></div>