---
title: >-
    غزل شمارهٔ ۱۳۳
---
# غزل شمارهٔ ۱۳۳

<div class="b" id="bn1"><div class="m1"><p>از سر زلف تو بر دل کار مشکل شد مرا</p></div>
<div class="m2"><p>این ره پر پیچ و خم بر پا سلاسل شد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تخم امیدی که دل در سینه خرمن کرده بود</p></div>
<div class="m2"><p>در زمین شور دنیا جمله باطل شد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کرد کار سیل بی زنهار با ویرانه ام</p></div>
<div class="m2"><p>خرمنی کز دانه های اشک حاصل شد مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیستم بر خاطر دریا گران چون خار و خس</p></div>
<div class="m2"><p>می تواند هر کف بی مغز، ساحل شد مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خار خشکم، می شوم قانع به اندک گرمیی</p></div>
<div class="m2"><p>هر شراری می تواند شمع محفل شد مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست خود چون موج شستم از عنان اختیار</p></div>
<div class="m2"><p>تا به دریای حقیقت قطره واصل شد مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شرم عشق از دیدن رخسار یارم بازداشت</p></div>
<div class="m2"><p>از تماشا این حجاب نور حایل شد مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ضعف بر مجنون من گر این چنین زور آورد</p></div>
<div class="m2"><p>موجه ریگ روان خواهد سلاسل شد مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قامت خم برد آرام و قرار از جان من</p></div>
<div class="m2"><p>خواب شیرین، تلخ ازین دیوار مایل شد مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شاهد کیفیت می، شور میخواران بس است</p></div>
<div class="m2"><p>رهبر تیغ شهادت رقص بسمل شد مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حسن عالمگیر لیلی تا نقاب از رخ گرفت</p></div>
<div class="m2"><p>دامن دشت جنون دامان محمل شد مرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در طریقت بار هر کس را نگرفتم به دوش</p></div>
<div class="m2"><p>چون گشودم چشم بینش، بار بر دل شد مرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عشق تا دست نوازش بر سر دوشم کشید</p></div>
<div class="m2"><p>زال شد صائب اگر رستم مقابل شد مرا</p></div></div>