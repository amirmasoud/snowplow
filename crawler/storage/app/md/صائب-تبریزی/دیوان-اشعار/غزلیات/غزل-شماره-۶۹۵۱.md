---
title: >-
    غزل شمارهٔ ۶۹۵۱
---
# غزل شمارهٔ ۶۹۵۱

<div class="b" id="bn1"><div class="m1"><p>دل آب کند برق جلالی که تو داری</p></div>
<div class="m2"><p>آیینه گدازست جمالی که تو داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آینه و آب نگشته است مصور</p></div>
<div class="m2"><p>از بس که بود شوخ مثالی که تو داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با ناخن مشکین چه جگرها که کند ریش</p></div>
<div class="m2"><p>از خط بناگوش هلالی که تو داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس حلقه که در گوش کشد شیردلان را</p></div>
<div class="m2"><p>از چشم سیه مست، غزالی که تو داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسیار کند در دل نظارگیان خون</p></div>
<div class="m2"><p>این لعل لب و چهره آلی که تو داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر کبک کند چنگل شهباز هوا را</p></div>
<div class="m2"><p>از شوخی مژگان پر و بالی که تو داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر هم زن جمعیت مرغان بهشت است</p></div>
<div class="m2"><p>در کنج لب آن دانه خالی که تو داری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرمشق جنون، مرکز پرگار نظرهاست</p></div>
<div class="m2"><p>بر صفحه عارض خط و خالی که تو داری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه خواب گذارد به نظرها نه خیالی</p></div>
<div class="m2"><p>از چشم و دهن خواب و خیالی که تو داری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در پنجه مژگان تو فولاد شود موم</p></div>
<div class="m2"><p>در سنگ کند ریشه نهالی که تو داری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سی شب به تماشایی رخسار تو عیدست</p></div>
<div class="m2"><p>از دیدن ابروی هلالی که تو داری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر روز به خورشید زوالی رسد از چرخ</p></div>
<div class="m2"><p>ایمن بود از نقص کمالی که تو داری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در معنی و لفظ تو تفاوت نتوان یافت</p></div>
<div class="m2"><p>خوشتر بود از روی، خصالی که تو داری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ظلم است که بر سوخته جانان نکنی رحم</p></div>
<div class="m2"><p>در لعل لب این آب زلالی که تو داری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صائب نشود فکر تو چون نازک و باریک؟</p></div>
<div class="m2"><p>زان موی میان راه خیالی که تو داری</p></div></div>