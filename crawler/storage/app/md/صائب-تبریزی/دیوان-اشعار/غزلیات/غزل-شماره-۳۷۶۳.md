---
title: >-
    غزل شمارهٔ ۳۷۶۳
---
# غزل شمارهٔ ۳۷۶۳

<div class="b" id="bn1"><div class="m1"><p>مرا به زخم زبان روزگار می‌گذرد</p></div>
<div class="m2"><p>مدار آبله من به خار می‌گذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به اعتبار عزیز جهان شدن سهل است</p></div>
<div class="m2"><p>عزیز اوست که از اعتبار می‌گذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به آب و رنگ جهان هرکه چشم کرد سیاه</p></div>
<div class="m2"><p>چو لاله با جگر داغدار می‌گذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفس شمرده برآور که خود حسابان را</p></div>
<div class="m2"><p>حساب زود به روزشمار می‌گذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز غفلت آن که نگیرد ز دیگران عبرت</p></div>
<div class="m2"><p>ز صیدگان جهان بی‌شکار می‌گذرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل رمیده بود در بغل بیابانگرد</p></div>
<div class="m2"><p>که موج در دل بحر از کنار می‌گذرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه سود ازین که سراپا چو نرگسی همه چشم؟</p></div>
<div class="m2"><p>ترا که عمر به خواب و خمار می‌گذرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به قدر جام تو از باده می‌کنی مستی</p></div>
<div class="m2"><p>وگرنه بحر ازین جویبار می‌گذرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به وصل سوخته‌ای زود خویش را برسان</p></div>
<div class="m2"><p>وگرنه خرده جان چون شرار می‌گذرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مخور ز بی‌خبری روی دست بیکاری</p></div>
<div class="m2"><p>که مزد می‌رود و وقت کار می‌گذرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عجب که صورت دیوار جان نمی‌یابد</p></div>
<div class="m2"><p>به محفلی که در او حرف یار می‌گذرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگرچه وعده خوبان وفا نمی‌دارد</p></div>
<div class="m2"><p>خوش آن حیات که در انتظار می‌گذرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ترحم است بر آن مرده دل که از دنیا</p></div>
<div class="m2"><p>به روشنایی شمع مزار می‌گذرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در آن چمن که تو لنگر فکنده‌ای صائب</p></div>
<div class="m2"><p>گل پیاده سبک چون سوار می‌گذرد</p></div></div>