---
title: >-
    غزل شمارهٔ ۲۶۶۸
---
# غزل شمارهٔ ۲۶۶۸

<div class="b" id="bn1"><div class="m1"><p>جزرخش کز وی زمین و آسمان پر گل شود</p></div>
<div class="m2"><p>کس ندارد یاد کز یک گل جهان پر گل شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خارخار سیر جنت از دلش بیرون رود</p></div>
<div class="m2"><p>دیده هرکس ز روی دوستان پر گل شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا به چند ای غنچه لب در پرده خواهی حرف گفت؟</p></div>
<div class="m2"><p>دست بردار از دهان تا بوستان پر گل شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا چه گلها بشکفد از غنچه منقار او</p></div>
<div class="m2"><p>بلبلی کز خار خارش آشیان پر گل شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخیه زخم نمایان من از اشک من است</p></div>
<div class="m2"><p>از کواکب کوچه باغ کهکشان پر گل شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسن هیهات است حق، عشق را ضایع کند</p></div>
<div class="m2"><p>بلبلان را ازحدیث گل دهان پر گل شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر برآید ماه مصر از چاه با این آب و تاب</p></div>
<div class="m2"><p>کوه و دشت ازنقش پای کاروان پر گل شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برگ عیش عاشقان از برگریزان فناست</p></div>
<div class="m2"><p>از فروغ ماه دامان کتان پر گل شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چار دیوار قفس از نغمه رنگین من</p></div>
<div class="m2"><p>در بهاران چون حریم گلستان پر گل شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر نواسنجی که سر در زیر بال خود کشد</p></div>
<div class="m2"><p>خلوتش چون غنچه صائب در خزان پر گل شود</p></div></div>