---
title: >-
    غزل شمارهٔ ۷۶۲
---
# غزل شمارهٔ ۷۶۲

<div class="b" id="bn1"><div class="m1"><p>داغ است لاله زار دل دردمند ما</p></div>
<div class="m2"><p>خواند نوا به آتش سوزان سپند ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا دور ازان لب شکرین همچو نی شدیم</p></div>
<div class="m2"><p>ترجیع بند ناله بود بندبند ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما از شراب لعل به همت گذشته ایم</p></div>
<div class="m2"><p>سیلاب گیر نیست زمین بلند ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از درد و داغ عشق تهی ساخت سینه را</p></div>
<div class="m2"><p>کفران نعمت دل نادردمند ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از قید عشق، بلبل ما خوش نوا شود</p></div>
<div class="m2"><p>بند زبان ما چو قلم نیست بند ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چند رشته می شود از پیچ و خم گره</p></div>
<div class="m2"><p>گردد ز پیچ و تاب رساتر کمند ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سنگین دلی تو، ورنه ز فریاد آتشین</p></div>
<div class="m2"><p>سوراخ می کند دل مجمرسپند ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از زهر چشم سنگدلان امن نیستیم</p></div>
<div class="m2"><p>چون پسته در لباس بود نوشخند ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سالم ز آب خنجر قصاب بگذرد</p></div>
<div class="m2"><p>گر تن به فربهی ندهد گوسفند ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>موی سفید، غفلت ما را زیاده کرد</p></div>
<div class="m2"><p>این تازیانه شد رگ خواب سمند ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب چو آفتاب جهانگیر می شود</p></div>
<div class="m2"><p>حسنی که خوش کند دل مشکل پسند ما</p></div></div>