---
title: >-
    غزل شمارهٔ ۶۵۳۴
---
# غزل شمارهٔ ۶۵۳۴

<div class="b" id="bn1"><div class="m1"><p>از بس ز خون ما شده گلگون عقیق تو</p></div>
<div class="m2"><p>در ساغر سهیل کند خون عقیق تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می برد اگر عقیق ازین پیش تشنگی</p></div>
<div class="m2"><p>سازد به عکس، تشنگی افزون عقیق تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر قطره خون من جگر داغدیده ای است</p></div>
<div class="m2"><p>تا شد دگر ز خون که گلگون عقیق تو؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یاقوت آبدار شود اشک شمع ها</p></div>
<div class="m2"><p>در محفلی که گردد میگون عقیق تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خورشید اگر کند عرق خون، ز صلب سنگ</p></div>
<div class="m2"><p>بیرون نیاورد گهری چون عقیق تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>موج سراب رشته یاقوت می شود</p></div>
<div class="m2"><p>گر پرتو افکند سوی هامون عقیق تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شب بیشتر کند دل خونخوار را سیاه</p></div>
<div class="m2"><p>در عهد خط زیاده کند خون عقیق تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دایم به خوشدلی گذرانده است روزگار</p></div>
<div class="m2"><p>از خط ندیده است شبیخون عقیق تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نقش امید بوسه به وجه حسن نشست</p></div>
<div class="m2"><p>تا شد نهفته در خط شبگون عقیق تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یک نقش بیش نیست نگین های ساده را</p></div>
<div class="m2"><p>دارد هزار نکته موزون عقیق تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر چند فکر صائب ما خون خویش خورد</p></div>
<div class="m2"><p>ما را نساخت از صله ممنون عقیق تو</p></div></div>