---
title: >-
    غزل شمارهٔ ۵۸۵۳
---
# غزل شمارهٔ ۵۸۵۳

<div class="b" id="bn1"><div class="m1"><p>ما نقل باده را ز لب جام کرده ایم</p></div>
<div class="m2"><p>عادت به تلخکامی از ایام کرده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانسته ایم بوسه زیاد از دهان ماست</p></div>
<div class="m2"><p>صلح از دهان یار به پیغام کرده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از ما متاب روی که از آه نیمشب</p></div>
<div class="m2"><p>بسیار صبح آینه را شام کرده ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما را فریب دانه نمی آورد به دام</p></div>
<div class="m2"><p>کز دانه صلح با گره دام کرده ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در حسرت بنفشه خطان زمانه است</p></div>
<div class="m2"><p>چشمی که ما سفید چو بادام کرده ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آخرین نفس کفن خویش را چو صبح</p></div>
<div class="m2"><p>از شوق کعبه جامه احرام کرده ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما همچو آدم از طمع خام دست خویش</p></div>
<div class="m2"><p>در خلد نان پخته خود خام کرده ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سازند ازان سیاه رخ ما که چون عقیق</p></div>
<div class="m2"><p>هموار خویش را ز پی نام کرده ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشم گرسنه حلقه دام است صید را</p></div>
<div class="m2"><p>ما خویش را خلاص ازین دام کرده ایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صائب به تنگ عیشی ما نیست میکشی</p></div>
<div class="m2"><p>چون لاله اختصار به یک جام کرده ایم</p></div></div>