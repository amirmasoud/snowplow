---
title: >-
    غزل شمارهٔ ۵۹۲۴
---
# غزل شمارهٔ ۵۹۲۴

<div class="b" id="bn1"><div class="m1"><p>هرگز به خراش جگری شاد نگردیم</p></div>
<div class="m2"><p>گر تیشه شویم امت فرهاد نگردیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا محمل لیلی نشود سلسله جنبان</p></div>
<div class="m2"><p>ما همچو جرس مشرق فریاد نگردیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آزادگی و بی ثمری جامه فتح است</p></div>
<div class="m2"><p>چون سرو چرا از ثمر آزاد نگردیم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا پا نکشیم از گل لغزنده تعمیر</p></div>
<div class="m2"><p>چون نکهت گل همسفر باد نگردیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشهور سخن سنج بود بلبل این باغ</p></div>
<div class="m2"><p>صائب ز چه گرد فرح آباد نگردیم؟</p></div></div>