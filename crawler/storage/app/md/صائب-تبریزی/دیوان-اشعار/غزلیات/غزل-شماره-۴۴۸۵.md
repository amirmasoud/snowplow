---
title: >-
    غزل شمارهٔ ۴۴۸۵
---
# غزل شمارهٔ ۴۴۸۵

<div class="b" id="bn1"><div class="m1"><p>همین نه سینه ما آه صبحگاه ندارد</p></div>
<div class="m2"><p>زمانه ای است که در سینه صبح آه ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نسیم تفرقه خاطرست جنبش مژگان</p></div>
<div class="m2"><p>من و سراسر دشتی که یک گیاه ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کمند جاذبه مسطر کشیده است زمین را</p></div>
<div class="m2"><p>چه شد به ظاهر اگر کعبه شاهراه ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز قرب آینه در دل غبار رشک ندارم</p></div>
<div class="m2"><p>که چشم شیشه دلان جوهر نگاه ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز شرم عفو نگردد سفید در صف محشر</p></div>
<div class="m2"><p>کسی که در کف خود نامه سیاه ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زبان لاف بریده است در قلمرو معنی</p></div>
<div class="m2"><p>حباب قلزم ما باد در کلاه ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه نسبت است به یوسف عزیزکرده مارا</p></div>
<div class="m2"><p>صفای چشمه خورشید آب چاه ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مدار چشم ترحم ز چرخ کاهکشانش</p></div>
<div class="m2"><p>که کس خلاصی ازین آب زیر کاه ندارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلی که نیست در او گوشه ای ز وسعت مشرب</p></div>
<div class="m2"><p>چوخانه ای است که ایوان وپیشگاه ندارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بس است مصرع رنگین دلیل فطرت صائب</p></div>
<div class="m2"><p>که هیچ مدعیی این چنین گواه ندارد</p></div></div>