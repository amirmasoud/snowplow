---
title: >-
    غزل شمارهٔ ۵۹۷۸
---
# غزل شمارهٔ ۵۹۷۸

<div class="b" id="bn1"><div class="m1"><p>از رخش خواهند جای بوسه نافهمیدگان</p></div>
<div class="m2"><p>در حرم محراب می جویند این نادیدگان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوق را افسرده می سازد وصال دایمی</p></div>
<div class="m2"><p>می برند از وصل لذت بیش هجران دیدگان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می شوند از سادگی در بوته خجلت گلاب</p></div>
<div class="m2"><p>چون گل بی درد در باغ جهان خندیدگان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عیب دنیا را نمی بینند با صد چشم خلق</p></div>
<div class="m2"><p>گر چه بی پرده است در چشم نظرپوشیدگان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیستند از روی میزان قیامت منفعل</p></div>
<div class="m2"><p>با دو چشم عاقبت بین خویش را سنجیدگان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در شبستان لحد خواب فراغت می کنند</p></div>
<div class="m2"><p>در دل شبها ز بیداری به خود پیچیدگان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که دستار تعین از سر خود وا نکرد</p></div>
<div class="m2"><p>در صف مردان بود کمتر ز سر پوشیدگان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می شوند از لاغری در هفته ای پا در رکاب</p></div>
<div class="m2"><p>از فروغ عاریت چون ماه نوبالیدگان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون گل رعناست می را زردرویی در قفا</p></div>
<div class="m2"><p>سرخ رو باشند دایم خون دل نوشیدگان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قدر درویشی کسی داند که شاهی کرده است</p></div>
<div class="m2"><p>راحت ساحل شود ظاهر به طوفان دیدگان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از خموشی های اهل فهم در تحسین شعر</p></div>
<div class="m2"><p>می خلد افزون به دل تحسین نافهمیدگان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با کمال بی بری باشند صائب تازه رو</p></div>
<div class="m2"><p>در گلستان جهان چون سرو دامن چیدگان</p></div></div>