---
title: >-
    غزل شمارهٔ ۱۲۰۱
---
# غزل شمارهٔ ۱۲۰۱

<div class="b" id="bn1"><div class="m1"><p>خاک با این رتبه تمکین، جناب آدمی است</p></div>
<div class="m2"><p>چرخ با آن شان و شوکت در رکاب آدمی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر جمالی را نقابی، هر گلی را غنچه ای است</p></div>
<div class="m2"><p>پرده زنبوری گردن، نقاب آدمی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست در مجموعه افلاک با آن طول و عرض</p></div>
<div class="m2"><p>از حقایق آنچه مثبت در کتاب آدمی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشأه عشق الهی را به انسان داده اند</p></div>
<div class="m2"><p>گردش این نه خم از جوش شراب آدمی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاهد فرزندی آدم نه تنها صورت است</p></div>
<div class="m2"><p>هر که دارد حسن معنی در حساب آدمی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با دل مجروح آدم کار دارد شور عشق</p></div>
<div class="m2"><p>این نمک از سینه چاکان کباب آدمی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست انجم این که می بینی بر اوراق فلک</p></div>
<div class="m2"><p>جبهه گردون عرق ریز از حجاب آدمی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وسعت مشرب عبارت از فضای جنت است</p></div>
<div class="m2"><p>چشمه کوثر همین چشم پر آب آدمی است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نقش بر آب است پیش باددستی های عشق</p></div>
<div class="m2"><p>عقل پرکاری که سر لوح کتاب آدمی است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رزق، خود را می رساند هر کجا قسمت بود</p></div>
<div class="m2"><p>خنده سرشار گندم بر شتاب آدمی است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل منه بر مجمر زرین گردون زینهار</p></div>
<div class="m2"><p>کاختر سیاره اش اشک کباب آدمی است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آدمیت حسن گندم گون پسندیدن بود</p></div>
<div class="m2"><p>هر که باشد این مذاقش در حساب آدمی است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این جواب آن غزل صائب که ناصح گفته است</p></div>
<div class="m2"><p>آفتاب بی زوالی در نقاب آدمی است</p></div></div>