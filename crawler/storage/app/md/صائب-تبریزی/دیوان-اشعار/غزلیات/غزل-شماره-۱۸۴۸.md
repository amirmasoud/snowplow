---
title: >-
    غزل شمارهٔ ۱۸۴۸
---
# غزل شمارهٔ ۱۸۴۸

<div class="b" id="bn1"><div class="m1"><p>ز نوبهار جهان زینت تمام گرفت</p></div>
<div class="m2"><p>شکوفه روی زمین را به سیم خام گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شدند سوخته جانان امیدوار آن روز</p></div>
<div class="m2"><p>که داغ لاله به کف جام لعل فام گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز غنچه، مستی بلبل دو روز بیش نبود</p></div>
<div class="m2"><p>سزای آن که ز نو کیسه زر به وام گرفت!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تهی است جیب و کنارش ز دور باش حیا</p></div>
<div class="m2"><p>اگر چه هاله به بر ماه را تمام گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمی توان به نظر کرد عشق را تسخیر</p></div>
<div class="m2"><p>محیط را نتواند کسی به دام گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چرا به حال غریبان نمی کنی اقبال؟</p></div>
<div class="m2"><p>ترا که صبح بناگوش رنگ شام گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سپهر سفله نگردد حجاب، قسمت را</p></div>
<div class="m2"><p>صدف ز آب گهر در محیط کام گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فغان که گریه شادی نمی تواند شست</p></div>
<div class="m2"><p>حلاوتی که لب قاصد از پیام گرفت!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکستگی نرسد خامه ترا صائب!</p></div>
<div class="m2"><p>که از تو کار سخن رونق تمام گرفت</p></div></div>