---
title: >-
    غزل شمارهٔ ۴۸۹۳
---
# غزل شمارهٔ ۴۸۹۳

<div class="b" id="bn1"><div class="m1"><p>هرکه درمد نظر نازک میانی نیستش</p></div>
<div class="m2"><p>دربساط زندگانی نیم جانی نیستش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون گل در باغ بی دیوار می باشد هدر</p></div>
<div class="m2"><p>وای بر حسنی که بر سر دیده بانی نیستش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برگ برگ این گلستانرا سراسر دیده ام</p></div>
<div class="m2"><p>چون گل رعنا بهار بی خزانی نیستش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرکه را از بیقراری نبض جان آسوده است</p></div>
<div class="m2"><p>در ریاض زندگی آب روانی نیستش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان عاشق هیچ جایک دم نمیگیرد قرار</p></div>
<div class="m2"><p>می توان دانست کاینجا آشیانی نیستش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون نمکدان در نمک پاشی است سر تا پا دهان</p></div>
<div class="m2"><p>آن که وقت بوسه دادنها دهانی نیستش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که را چون صبح موی سر ز پیری شد سفید</p></div>
<div class="m2"><p>می شود بیدار اگر خواب گرانی نیستش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خلق را بی حفظ حق نگشاید از هم هیچ کار</p></div>
<div class="m2"><p>گله از گرگ است چون بر سر شبانی نیستش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرمی هنگامه فکرش دو روزی بیش نیست</p></div>
<div class="m2"><p>در سخن هر کس طرف آتش زبانی نیستش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می شود چون مرغ یک بال از پرافشانی ملول</p></div>
<div class="m2"><p>در طریق فکر هر کس همعنانی نیستش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن که دولت در رکاب سایه او میرود</p></div>
<div class="m2"><p>چون هما از خوان قسمت استخوانی نیستش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر فقیران محنت پیری نباشد نا گوار</p></div>
<div class="m2"><p>کی غم دندان خورد آن کس که نانی نیستش ؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرچه آن آیینه رو دارد نواسنجان بسی</p></div>
<div class="m2"><p>همچو صائب طوطی شیرین زبانی نیستش</p></div></div>