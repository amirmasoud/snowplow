---
title: >-
    غزل شمارهٔ ۶۸۷۰
---
# غزل شمارهٔ ۶۸۷۰

<div class="b" id="bn1"><div class="m1"><p>گذشت عمر و تو مست شراب گلرنگی</p></div>
<div class="m2"><p>دمید صبح و تو چون سبزه در ته سنگی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دید پرده گوش فلک ز ناله صور</p></div>
<div class="m2"><p>همان تو گوش بر آواز نغمه چنگی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به پرتوی و نسیمی ز هم فرو ریزی</p></div>
<div class="m2"><p>سبک رکاب چو بو، بی ثبات چون رنگی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زمین به خشک پلنگ تو تنگ میدان است</p></div>
<div class="m2"><p>به ماه در جدل و با ستاره در جنگی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز دیدن تو خورد بر هم آبگینه و آب</p></div>
<div class="m2"><p>غبار آینه اهل دید چون زنگی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر چه روی زمین نیلی از گرانی توست</p></div>
<div class="m2"><p>چو برگ کاه به میزان عقل بی سنگی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گواه مردی آزادگان دم و قدم است</p></div>
<div class="m2"><p>درین دو پله تو نامرد گنگی و لنگی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز داغ لاله زمین دلت سیاهترست</p></div>
<div class="m2"><p>به چهره چون ورق لاله گر چه خوش رنگی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز بار حرص نداری قرار بر یک جا</p></div>
<div class="m2"><p>گران و پر حرکت همچو آسیا سنگی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به دیده همه عالم چو خار ناسازی</p></div>
<div class="m2"><p>به لقمه همه کس ناگوار چون سنگی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو دست پیش تو دارد کسی گره گردی</p></div>
<div class="m2"><p>ولی به وقت خراش دل آهنین چنگی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مکن چو اهل کرم دعوی فراخ دلی</p></div>
<div class="m2"><p>که همچو دایره خلق مفلسان تنگی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز نه سپهر گذشتند گرم رفتاران</p></div>
<div class="m2"><p>تو سست عزم همان در شمار فرسنگی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نوای زاغ درین باغ نیست بی تائثیر</p></div>
<div class="m2"><p>تو بی اثر چه نوایی، کدام آهنگی؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز ناله تو دل سنگ آب شد صائب</p></div>
<div class="m2"><p>مگر به عارف خاک فرج هم آهنگی؟</p></div></div>