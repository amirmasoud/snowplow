---
title: >-
    غزل شمارهٔ ۴۳۶۶
---
# غزل شمارهٔ ۴۳۶۶

<div class="b" id="bn1"><div class="m1"><p>ساقی دهن شیشه ما باز به لب کرد</p></div>
<div class="m2"><p>جان عجبی در تن ارباب طرب کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دریا نه کریمی است که بی خواست نبخشد</p></div>
<div class="m2"><p>بیهوده صدف باز دهن را به طلب کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خامش منشینید که از خامشی شمع</p></div>
<div class="m2"><p>پروانه بی غیرت ما روز به شب کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با کوزه سربسته ز دریا چه توان برد</p></div>
<div class="m2"><p>محروم ز وصل تو مرا شرم وادب کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برگی است خزان دیده که از ثمرش نیست</p></div>
<div class="m2"><p>دستی که طمعکار بدآموز طلب کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی ابر صدف قطره ای از بحر نیابد</p></div>
<div class="m2"><p>درعالم امکان نتوان ترک سبب کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیوست به گل خورشید جهان روشنی شمع</p></div>
<div class="m2"><p>زان گریه جانسوز که در دامن شب کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از چوب گل آتش ننهد سرکشی از سر</p></div>
<div class="m2"><p>عاقل نتوان اهل جنون رابه ادب کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در خواب زد از دولت بیدار جهان دست</p></div>
<div class="m2"><p>از ساده دلی هر که تفاخر به نسب کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آراست نسب نامه خود را به دو مطلع</p></div>
<div class="m2"><p>هر کس نسب خویش مزین به حسب کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در خلوت خورشید ز خمیازه آغوش</p></div>
<div class="m2"><p>بیطاقتی صبح مرا مست طلب کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صائب چه کند باده ننوشد که درین بزم</p></div>
<div class="m2"><p>هشیار ز جانان نتوان بوسه طلب کرد</p></div></div>