---
title: >-
    غزل شمارهٔ ۱۹۳۵
---
# غزل شمارهٔ ۱۹۳۵

<div class="b" id="bn1"><div class="m1"><p>آن روی آتشین که جگرها کباب اوست</p></div>
<div class="m2"><p>نور و صفای شمع و گل از آب و تاب اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در چهره گشاده صبح بهار نیست</p></div>
<div class="m2"><p>فیضی که در گشودن بند نقاب اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در هیچ دیده آب نخواهد گذاشتن</p></div>
<div class="m2"><p>این روشنی که با رخ چون آفتاب اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دور باش غیر ندارم شکایتی</p></div>
<div class="m2"><p>هر شکوه ای که هست مرا از حجاب اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز حساب اگر چه ندارد نهایتی</p></div>
<div class="m2"><p>کوته به پرسش ستم بی حساب اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از ضعف اگر چه ما به زمین نقش بسته ایم</p></div>
<div class="m2"><p>جان نفس گسسته ما در رکاب اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک مو ز پیچ و تاب میان تو کم نشد</p></div>
<div class="m2"><p>هر چند پیچ و تاب من از پیچ و تاب اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر دیگران به لطف و به احسان مقیدند</p></div>
<div class="m2"><p>صائب اسیر شیوه ناز و عتاب اوست</p></div></div>