---
title: >-
    غزل شمارهٔ ۲۳۵۳
---
# غزل شمارهٔ ۲۳۵۳

<div class="b" id="bn1"><div class="m1"><p>کاوش مژگان او دل را قیامت زار کرد</p></div>
<div class="m2"><p>خون گرم این مست خواب آلود را بیدار کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صفحه آیینه از زنگ کدورت ساده بود</p></div>
<div class="m2"><p>عکس طوطی این افق را مشرق زنگار کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون زنم مژگان به یکدیگر، که مژگان مرا</p></div>
<div class="m2"><p>حیرت گلزار او خار سر دیوار کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می شود پیراهن تن یوسف گم کرده را</p></div>
<div class="m2"><p>هر که چشم خویش را از گریه چون دستار کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در زبان هیچ کس زخم زبان نگذاشتم</p></div>
<div class="m2"><p>جلوه مجنون من این دشت را بی خار کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند باشی در شکست کارم ای گردون، بس است</p></div>
<div class="m2"><p>استخوانم را هجوم زخم جوهردار کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخت طفلانه است جوی شیر آوردن زسنگ</p></div>
<div class="m2"><p>کوهکن بیهوده جان را در سر این کار کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>(من که باشم تا نمایم صورت احوال خود؟</p></div>
<div class="m2"><p>حیرت رخسار او آیینه را ستار کرد)</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من که صائب در وطن حال غریبان داشتم</p></div>
<div class="m2"><p>چون تواند درد غربت در دل من کار کرد؟</p></div></div>