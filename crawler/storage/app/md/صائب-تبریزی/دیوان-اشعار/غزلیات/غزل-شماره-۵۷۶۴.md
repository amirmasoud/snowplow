---
title: >-
    غزل شمارهٔ ۵۷۶۴
---
# غزل شمارهٔ ۵۷۶۴

<div class="b" id="bn1"><div class="m1"><p>شدند جمع دل و زلف از آشنایی هم</p></div>
<div class="m2"><p>شکستگان جهانند مومیایی هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شود جهان لب پر خنده ای، اگر مردم</p></div>
<div class="m2"><p>کنند دست یکی در گرهگشایی هم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فغان که نیست به جز عیب یکدگر جستن</p></div>
<div class="m2"><p>نصیب مردم عالم ز آشنایی هم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شدند تشنه لبان جهان بیابان مرگ</p></div>
<div class="m2"><p>چو موجهای سراب از غلط نمایی هم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درین قلمرو ظلمت چو رهروان نجوم</p></div>
<div class="m2"><p>روند سوخته جانان به روشنایی هم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز سنگ تفرقه روزگار بیخبرند</p></div>
<div class="m2"><p>جماعتی که دلیرند در جدایی هم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شود بساط جهان پر زر تمام عیار</p></div>
<div class="m2"><p>کنند کوشش اگر خلق در روایی هم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شدند شهره عالم چو بلبلان صائب</p></div>
<div class="m2"><p>سخنوران جهان از سخنسرایی هم</p></div></div>