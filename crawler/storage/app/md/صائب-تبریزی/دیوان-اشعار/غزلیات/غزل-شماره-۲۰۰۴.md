---
title: >-
    غزل شمارهٔ ۲۰۰۴
---
# غزل شمارهٔ ۲۰۰۴

<div class="b" id="bn1"><div class="m1"><p>گنجینه جواهر ما پاک گوهری است</p></div>
<div class="m2"><p>نقدی که در خزانه ما هست بی زری است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر ما چه اعتراض که بی قدر و قیمتم؟</p></div>
<div class="m2"><p>گوهر اگر به خاک فتد جرم جوهری است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کار عشق سعی به جایی نمی رسد</p></div>
<div class="m2"><p>در بحر دست و پا زدن از ناشناوری است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارد دل ترا هوس از عشق بی نصیب</p></div>
<div class="m2"><p>این شیشه چون تهی شود از می پر از پری است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در اشک و آه اگر نکند صرف، غافل است</p></div>
<div class="m2"><p>چون شمع زندگانی آن کس که سرسری است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیری چه خون که در جگر ما نمی کند</p></div>
<div class="m2"><p>قد دوتای ما دویم چرخ چنبری است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتار دلفریب تو در پرده حجاب</p></div>
<div class="m2"><p>سیلاب عقل و هوش چو سر گوشی پری است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باقی ز خیر کن زر و سیم فناپذیر</p></div>
<div class="m2"><p>ای خواجه گر ترا هوس کیمیاگری است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلبسته هوا به نسیمی فتد ز پا</p></div>
<div class="m2"><p>کوتاهی حیات حباب از سبکسری است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صائب ز مال حرص یکی می شود هزار</p></div>
<div class="m2"><p>بیدرد را گمان که غنا در توانگری است</p></div></div>