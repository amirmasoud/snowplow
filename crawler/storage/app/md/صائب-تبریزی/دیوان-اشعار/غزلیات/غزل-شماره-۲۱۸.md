---
title: >-
    غزل شمارهٔ ۲۱۸
---
# غزل شمارهٔ ۲۱۸

<div class="b" id="bn1"><div class="m1"><p>هر که دید از باده لعلی به سامان شیشه را</p></div>
<div class="m2"><p>می دهد ترجیح بر کان بدخشان شیشه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه در ابر تنک خورشید را نتوان نهفت</p></div>
<div class="m2"><p>می کنم از سادگی در خرقه پنهان شیشه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر به رقص آرد دل بی تاب ما را دور نیست</p></div>
<div class="m2"><p>باده شوخی که سازد پایکوبان شیشه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با شراب عشق خودداری نمی آید ز دل</p></div>
<div class="m2"><p>جوش این می می دهد کشتی به طوفان شیشه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جلوه خورشید دارد در کنار صبحدم</p></div>
<div class="m2"><p>باده گلرنگ در چاک گریبان شیشه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خراباتی که ما لنگر ز مستی کرده ایم</p></div>
<div class="m2"><p>دعوی جلوه است با سرو خرامان شیشه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زان شراب لعل سرگرمم که از هر قطره اش</p></div>
<div class="m2"><p>اخگر خورشید باشد در گریبان شیشه را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرو همت را برومندی بود در بر گریز</p></div>
<div class="m2"><p>خنده می ریزد ز لب در وقت احسان شیشه را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کار هر دل نیست راز عشق پنهان داشتن</p></div>
<div class="m2"><p>زور این می می کند چون نار خندان شیشه را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می کشان را شکوه ای از گردش افلاک نیست</p></div>
<div class="m2"><p>در بغل دارند صائب می پرستان شیشه را</p></div></div>