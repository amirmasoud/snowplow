---
title: >-
    غزل شمارهٔ ۶۰۷۲
---
# غزل شمارهٔ ۶۰۷۲

<div class="b" id="bn1"><div class="m1"><p>سرکشی بگذار پیش امر حق تسلیم کن</p></div>
<div class="m2"><p>آتش نمرود را گلزار ابراهیم کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر تو دشوارست اگر یکجا وداع مال و جان</p></div>
<div class="m2"><p>پیشتر از رفتن جان، مال را تسلیم کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نخل بهتر در زمین نرم بالا می کشد</p></div>
<div class="m2"><p>خاکساران جهان را بیشتر تعظیم کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برمدار از سجده حق هفت عضو خویش را</p></div>
<div class="m2"><p>همچو مردان خدا تسخیر هفت اقلیم کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیچ نگشاید به جز وسواس از علم نجوم</p></div>
<div class="m2"><p>چهره را از جدول خون صفحه تقویم کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در گذر از ثابت و سیار، صائب همچو برق</p></div>
<div class="m2"><p>روی از یک قبله روشن همچو ابراهیم کن</p></div></div>