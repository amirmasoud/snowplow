---
title: >-
    غزل شمارهٔ ۴۲۳۹
---
# غزل شمارهٔ ۴۲۳۹

<div class="b" id="bn1"><div class="m1"><p>آن را که در جگر نفس آتشین بود</p></div>
<div class="m2"><p>خورشید آسمان وچراغ زمین بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون ماه حسن ساخته بیش ازدوهفته نیست</p></div>
<div class="m2"><p>مارا نظر به حسن خدا آفرین بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معلوم شد زخواب گران گذشتگان</p></div>
<div class="m2"><p>کآسودگی نهفته به زیر زمین بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روزی به آبروی نیابند خاکیان</p></div>
<div class="m2"><p>رزق تنور از قفس آتشین بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون آفتاب هر که ننازد به اعتبار</p></div>
<div class="m2"><p>گر بر فلک رود نظرش بر زمین بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن خرمن گلی که نظر نیست محرمش</p></div>
<div class="m2"><p>مپسند بی حجاب در آغوش زین بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون برق و باد دولت دنیا سبکروست</p></div>
<div class="m2"><p>در دست دیو یک دو سه روزی نگین بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گویند سنت است که در وقت احتضار</p></div>
<div class="m2"><p>ذکر بلند ورد زبان حزین بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون ذکر را بلند نگوییم روز وشب</p></div>
<div class="m2"><p>ماراکه هرنفس نفس واپسین بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جان تازه شد ز روی عرقناک او مرا</p></div>
<div class="m2"><p>باران نرم روزی مغز زمین بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب صبور باش که تا یار خوشدل است</p></div>
<div class="m2"><p>عاشق همیشه خسته و زار و حزین بود</p></div></div>