---
title: >-
    غزل شمارهٔ ۲۷۳
---
# غزل شمارهٔ ۲۷۳

<div class="b" id="bn1"><div class="m1"><p>یاد رخسار ترا در دل نهان داریم ما</p></div>
<div class="m2"><p>در دل دوزخ بهشت جاودان داریم ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در چنین راهی که مردان توشه از دل کرده اند</p></div>
<div class="m2"><p>ساده لوحی بین که فکر آب و نان داریم ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منزل ما همرکاب ماست هر جا می رویم</p></div>
<div class="m2"><p>در سفرها طالع ریگ روان داریم ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچنان در قطع راه عشق کندی می کنیم</p></div>
<div class="m2"><p>گر چه از سنگ ملامت صد فسان داریم ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چیست خاک تیره تا باشد تماشاگاه ما؟</p></div>
<div class="m2"><p>سیرها در خویشتن چون آسمان داریم ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قسمت ماچون کمان از صید خود خمیازه ای است</p></div>
<div class="m2"><p>هر چه داریم از برای دیگران داریم ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بهار ما خزان ها چون حنا پوشیده است</p></div>
<div class="m2"><p>گر چه در ظاهر بهار بی خزان داریم ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همت پیران دلیل ماست هر جا می رویم</p></div>
<div class="m2"><p>قوت پرواز چون تیر از کمان داریم ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر چه می دانیم آخر سر به سر افسانه ایم</p></div>
<div class="m2"><p>پنبه ها در گوش از خواب گران داریم ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست جان سخت ما از سختی دوران ملول</p></div>
<div class="m2"><p>زندگانی چون هما از استخوان داریم ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر چه غیر از سایه ما را نیست دیگر میوه ای</p></div>
<div class="m2"><p>منت روی زمین بر باغبان داریم ما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر چه صائب دست ما خالی است از نقد جهان</p></div>
<div class="m2"><p>چون جرس آوازه ای در کاروان داریم ما</p></div></div>