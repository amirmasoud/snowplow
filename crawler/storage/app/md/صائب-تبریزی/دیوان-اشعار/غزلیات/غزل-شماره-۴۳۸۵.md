---
title: >-
    غزل شمارهٔ ۴۳۸۵
---
# غزل شمارهٔ ۴۳۸۵

<div class="b" id="bn1"><div class="m1"><p>روزی که مرا موج نفس دام سخن شد</p></div>
<div class="m2"><p>شدطوطی چرخ آینه وواله من شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر مد فغان کز دل پردرد کشیدم</p></div>
<div class="m2"><p>شد شاخ گل وسر خط مرغان چمن شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خدمت آیینه دل صرف شدی کاش</p></div>
<div class="m2"><p>عمری که مرا صرف به پرداز سخن شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر آه که بی خواست برآمدزدل من</p></div>
<div class="m2"><p>از بهر برون آمدن از خویش رسن شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برپیری من چرخ سیه کاسه نبخشید</p></div>
<div class="m2"><p>هرچندکه هرموبه تنم تیغ وکفن شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هشدار که از باغ سرافکنده برون رفت</p></div>
<div class="m2"><p>هر کس که مقید به تماشای چمن شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از تبت وارونه خط هرشکن زلف</p></div>
<div class="m2"><p>آغوش وداع دل سرگشته من شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب گره دل به تکلف بگشاید</p></div>
<div class="m2"><p>دستی که گرفتار سر زلف سخن شد</p></div></div>