---
title: >-
    غزل شمارهٔ ۴۵۸۲
---
# غزل شمارهٔ ۴۵۸۲

<div class="b" id="bn1"><div class="m1"><p>از فروغ لاله آتش زیر پا دارد بهار</p></div>
<div class="m2"><p>چون گل رعنا خزان را در قفا دارد بهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم تا وا کرده ای چون شبنم گل رفته است</p></div>
<div class="m2"><p>در رکاب زرنگار برق، پا دارد بهار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غنچه های تنگ میدان را مقام جلوه نیست</p></div>
<div class="m2"><p>ورنه چندین جلوه چون باد صبا دارد بهار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاهدان خشک را گوش زبان فهمی کرست</p></div>
<div class="m2"><p>ورنه ازآن بی نشان پیغامها دارد بهار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم ظاهر بین چو شبنم نگذرد از رنگ و بو</p></div>
<div class="m2"><p>دیده دل باز کن بنگر چها دارد بهار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خارخار عشق را پوشیده نتوان داشتن</p></div>
<div class="m2"><p>خار در پیراهن (از) نشو و نما دارد بهار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم اگر گردد سفید از گریه، خون دل مخور</p></div>
<div class="m2"><p>چون نسیم مصر با خود توتیا دارد بهار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می کند در هر مزاجی کار دیگر چون شراب</p></div>
<div class="m2"><p>زاهد میخواره را سر در هوا دارد بهار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جوش حسن لاله و گل نیست بیش از هفته ای</p></div>
<div class="m2"><p>حسن (روز)افزون مشرب را کجا دارد بهار؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پرده از پوشیده رویان تجلی باز کرد</p></div>
<div class="m2"><p>طرفه دستی بر گریبان حیادارد بهار</p></div></div>