---
title: >-
    غزل شمارهٔ ۳۲۶۱
---
# غزل شمارهٔ ۳۲۶۱

<div class="b" id="bn1"><div class="m1"><p>چه بهشتی است که یارم ز سفر برگردد</p></div>
<div class="m2"><p>از نظر ناشده چون نور نظر برگردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قدرت عجز اگر این است که من یافته ام</p></div>
<div class="m2"><p>تیغ دندانه شود تا ز سپر برگردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سفر عشق محال است مکرر نشود</p></div>
<div class="m2"><p>هرکه این راه به پا رفت به سر برگردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه چنان رفته ام از خود که به خود باز آیم</p></div>
<div class="m2"><p>به دل سنگ محال است شرر برگردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترک دنیا نکند حرص به دست افشاندن</p></div>
<div class="m2"><p>مگس خیره مکرر به شکر برگردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیر آه من و اندیشه ز گردون، هیهات</p></div>
<div class="m2"><p>ناوک سخت کمان کی ز سپر برگردد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رم آهو که عنانش به کف خودرایی است</p></div>
<div class="m2"><p>به تماشای تو ای طرفه پسر برگردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از غریبی دل خود همچو مه بدر مخور</p></div>
<div class="m2"><p>که به خورشید همان نور قمر برگردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تیر آهی که به صد زور گشایم ز جگر</p></div>
<div class="m2"><p>از نگونساری طالع به جگر برگردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جان شیرین نکند یاد ز دنیای خسیس</p></div>
<div class="m2"><p>به نی خشک محال است شکر برگردد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عمر چون رفت ز کف، سود ندارد افسوس</p></div>
<div class="m2"><p>کی به نیسان ز صدف آب گهر برگردد؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیخبر یار مگر بر سرم آید صائب</p></div>
<div class="m2"><p>ور نه آن صبر که دارد خبر برگردد؟</p></div></div>