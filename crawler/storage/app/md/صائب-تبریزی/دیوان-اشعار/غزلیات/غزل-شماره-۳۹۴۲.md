---
title: >-
    غزل شمارهٔ ۳۹۴۲
---
# غزل شمارهٔ ۳۹۴۲

<div class="b" id="bn1"><div class="m1"><p>کجا مرا می گلگون دماغ تازه کند</p></div>
<div class="m2"><p>که تخم سوخته را ابر داغ تازه کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کجاست سوختگان را دماغ خودسازی</p></div>
<div class="m2"><p>به ناخن دگران لاله داغ تازه کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درین بهار که صد جامه خار گردانید</p></div>
<div class="m2"><p>نشد که جامه خودسروباغ تازه کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دماغ سافی ما می خورد ز جیحون آب</p></div>
<div class="m2"><p>مگر به خون جگر، دل دماغ تازه کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز خط سیه نشودروز آتشین رویی</p></div>
<div class="m2"><p>که داغ کهنه ما را به داغ تازه کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلی که داغ نهان نیست مجلس افروزش</p></div>
<div class="m2"><p>دماغ خود به کدامین ایاغ تازه کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز داغ سینه من آب وتاب دیگر یافت</p></div>
<div class="m2"><p>که تازه رویی گل جان باغ تازه کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درین صحیفه من آن خامه سیه روزم</p></div>
<div class="m2"><p>که مغز خشک به دودچراغ تازه کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دمی که صائب ازوبوی صدق می آید</p></div>
<div class="m2"><p>چوباد صبح جهان را دماغ تازه کند</p></div></div>