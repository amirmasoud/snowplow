---
title: >-
    غزل شمارهٔ ۶۹۷۹
---
# غزل شمارهٔ ۶۹۷۹

<div class="b" id="bn1"><div class="m1"><p>تلخ منشین شراب اگر داری</p></div>
<div class="m2"><p>شور کم کن کباب اگر داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلی از روزگار خالی کن</p></div>
<div class="m2"><p>شیشه ای پر شراب اگر داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از جگرتشنگان دریغ مدار</p></div>
<div class="m2"><p>قطره ای چون سحاب اگر داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دهن خویش کن چو آبله مهر</p></div>
<div class="m2"><p>چشم آب از سراب اگر داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خشک مگذر ز خار آبله وار</p></div>
<div class="m2"><p>همه یک قطره آب اگر داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با تو طوفان چه می تواند کرد؟</p></div>
<div class="m2"><p>شیشه ای پر شراب اگر داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تخت از تاج می توانی کرد</p></div>
<div class="m2"><p>چون گهر آب و تاب اگر داری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آشیان در زمین پست مکن</p></div>
<div class="m2"><p>پر و بال عقاب اگر داری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باش بیدار در دل شبها</p></div>
<div class="m2"><p>در لحد چشم خواب اگر داری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نفسی راست می توانی کرد</p></div>
<div class="m2"><p>خلوتی چون حباب اگر داری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قدم خویش را شمرده گذار</p></div>
<div class="m2"><p>در رسیدن شتاب اگر داری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گنج امید فرش خانه توست</p></div>
<div class="m2"><p>دل و جان خراب اگر داری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سر به آزادگی برآر چو سرو</p></div>
<div class="m2"><p>حذر از انقلاب اگر داری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نفس خود شمرده ساز چو صبح</p></div>
<div class="m2"><p>خبری از حساب اگر داری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>می توانی ز گلرخان گل چید</p></div>
<div class="m2"><p>دیده بی حجاب اگر داری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون غزالان به ناف پیچ بساز</p></div>
<div class="m2"><p>هوس مشک ناب اگر داری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جمع کن خویش را چو شبنم گل</p></div>
<div class="m2"><p>چشم بر آفتاب اگر داری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سپرانداز پیش اهل جدل</p></div>
<div class="m2"><p>صد جواب صواب اگر داری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به فشاندن نگاهداری کن</p></div>
<div class="m2"><p>نعمت بی حساب اگر داری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نیست چون نافه حاجت اظهار</p></div>
<div class="m2"><p>در گره مشک ناب اگر داری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مشو از چشم بستگان غافل</p></div>
<div class="m2"><p>یوسفی در نقاب اگر داری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در صحبت به روی خلق ببند</p></div>
<div class="m2"><p>هوس فتح باب اگر داری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پیرو سایه خودی همه جا</p></div>
<div class="m2"><p>پشت بر آفتاب اگر داری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آب در شیر خود مکن ز چراغ</p></div>
<div class="m2"><p>در سرا ماهتاب اگر داری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دار پوشیده ریزش خود را</p></div>
<div class="m2"><p>در سخاوت حجاب اگر داری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>می دهد جا به دیده ات گوهر</p></div>
<div class="m2"><p>رشته سان پیچ و تاب اگر داری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یک قلم پرده های غفلت توست</p></div>
<div class="m2"><p>صد مجلد کتاب اگر داری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سبک از خواب می توانی خاست</p></div>
<div class="m2"><p>خشت بالین خواب اگر داری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>صائب از باده کهن مگذر</p></div>
<div class="m2"><p>آرزوی شباب اگر داری</p></div></div>