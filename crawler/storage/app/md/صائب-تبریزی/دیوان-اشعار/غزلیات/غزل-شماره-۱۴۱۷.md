---
title: >-
    غزل شمارهٔ ۱۴۱۷
---
# غزل شمارهٔ ۱۴۱۷

<div class="b" id="bn1"><div class="m1"><p>قد موزون تو روزی که به جولان برخاست</p></div>
<div class="m2"><p>هر که را بود دلی، از سر ایمان برخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خار خار دلم از سینه نمایان گردید</p></div>
<div class="m2"><p>بخیه تنگ رفویم ز گریبان برخاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شرم عشق است که پامال نگردد هرگز</p></div>
<div class="m2"><p>لاله افکنده سر از خاک شهیدان برخاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که دگر ز اهل کرم رحم به محتاج کند؟</p></div>
<div class="m2"><p>ابر با دیده خشک از لب عمان برخاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر دل غنچه اگر خورد نسیمی گستاخ</p></div>
<div class="m2"><p>شور محشر به دل بیضه ز مرغان برخاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همت آبله پای طلب را نازم!</p></div>
<div class="m2"><p>که به مشاطگی خار مغیلان برخاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زد همان روز که با غنچه محجوب تو لاف</p></div>
<div class="m2"><p>قفل شرم از دهن پسته خندان برخاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همدمی سیر مقامات نفرمود او را</p></div>
<div class="m2"><p>نی ما تا به چه طالع ز نیستان برخاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگسل از اهل کرم تا شودت پایه بلند</p></div>
<div class="m2"><p>صدف از خاک به یک ریزش نیسان برخاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قالبی نیست سخن سنجی ما چون طوطی</p></div>
<div class="m2"><p>بلبل ما ز دل بیضه غزلخوان برخاست</p></div></div>