---
title: >-
    غزل شمارهٔ ۳۸۶۹
---
# غزل شمارهٔ ۳۸۶۹

<div class="b" id="bn1"><div class="m1"><p>نشاط لازم نقص عقول می باشد</p></div>
<div class="m2"><p>به قدر هوش وخرد دل ملول می باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز زلف چون به خط افتادکارخوشدل باش</p></div>
<div class="m2"><p>که این برات قریب الوصول می باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خوش عیاری انگور بسته خوبی می</p></div>
<div class="m2"><p>جنون خلق به قدر عقول می باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ببخش اگر ز تو خواهم مراد هر دو جهان</p></div>
<div class="m2"><p>که میهمان کریمان فضول می باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی که زخمی شهرت شده است چون صائب</p></div>
<div class="m2"><p>همیشه طالب کنج خمول می باشد</p></div></div>