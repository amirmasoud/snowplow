---
title: >-
    غزل شمارهٔ ۱۳۵
---
# غزل شمارهٔ ۱۳۵

<div class="b" id="bn1"><div class="m1"><p>عشق پنهان باعث روشن روانی شد مرا</p></div>
<div class="m2"><p>روشن این غمخانه از سوز نهانی شد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بلندی، عمر من چون شمع کوتاهی نداشت</p></div>
<div class="m2"><p>زندگانی کوته از آتش زبانی شد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون در دوزخ ز چشم باز بودم در عذاب</p></div>
<div class="m2"><p>چشم پوشیدن بهشت جاودانی شد مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا شدم خاموش چون ماهی، محیط پر خطر</p></div>
<div class="m2"><p>مهد آسایش ز فیض بی زبانی شد مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نخل امید مرا جز بار دل حاصل نبود</p></div>
<div class="m2"><p>حیف ازان عمری که صرف باغبانی شد مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پای در دامان عزلت کش که چون موج سراب</p></div>
<div class="m2"><p>زندگی پا در رکاب از خوش عنانی شد مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ریخت هر خونی که چرخ سنگدل در ساغرم</p></div>
<div class="m2"><p>از هواجویی شراب ارغوانی شد مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حاصلش چون خنده برق است اشک بی شمار</p></div>
<div class="m2"><p>آنچه صرف عیش از ایام جوانی شد مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خرده جانی که در غم صرف کردن ظلم بود</p></div>
<div class="m2"><p>چون گل بی درد خرج شادمانی شد مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کشتی جسمی کز او امید ساحل داشتم</p></div>
<div class="m2"><p>در دل دریا زمین گیر از گرانی شد مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عرض مطلب می کند کوتاه طول عمر را</p></div>
<div class="m2"><p>حفظ آبرو، حیات جاودانی شد مرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کرد شعر آبدار از آب خضرم بی نیاز</p></div>
<div class="m2"><p>مزرع امید سبز از ترزبانی شد مرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر کمال لطف رخسارست نادیدن دلیل</p></div>
<div class="m2"><p>رغبت دیدار بیش از لن ترانی شد مرا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیست صائب کوتهی در جذبه افتادگان</p></div>
<div class="m2"><p>راه دور عشق طی از ناتوانی شد مرا</p></div></div>