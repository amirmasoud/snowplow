---
title: >-
    غزل شمارهٔ ۶۵۴۴
---
# غزل شمارهٔ ۶۵۴۴

<div class="b" id="bn1"><div class="m1"><p>در هیچ پرده نیست، نباشد نوای تو</p></div>
<div class="m2"><p>عالم پُر است از تو و خالی است جای تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند کاینات گدای در تواند</p></div>
<div class="m2"><p>یک آفریده نیست که داند سرای تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تاج و کمر چو موج و حباب است ریخته</p></div>
<div class="m2"><p>در هر کناره ای ز محیط سخای تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آیینه خانه ای است پر از آفتاب و ماه</p></div>
<div class="m2"><p>دامان خاک تیره ز موج صفای تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر غنچه را ز حمد تو جزوی است در بغل</p></div>
<div class="m2"><p>هر خار می کند به زبانی ثنای تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک قطره اشک سوخته، یک مهره گل است</p></div>
<div class="m2"><p>دریا و کان نظر به محیط سخای تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاک سیه به کاسه نمرود می کند</p></div>
<div class="m2"><p>هر پشه ای که بال زند در هوای تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در مشت خاک من چه بود لایق نثار؟</p></div>
<div class="m2"><p>هم از تو جان ستانم و سازم فدای تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عام است التفات کهن خرقه عقول</p></div>
<div class="m2"><p>تشریف عشق تا به که بخشد عطای تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غیر از نیاز و عجز که در کشور تو نیست</p></div>
<div class="m2"><p>این مشت خاک تیره چه دارد سزای تو؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عمر ابد که خضر بود سایه پرورش</p></div>
<div class="m2"><p>سروی است پست بر لب آب بقای تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صائب چه ذره است و چه دارد فدا کند؟</p></div>
<div class="m2"><p>ای صد هزار جان مقدس فدای تو</p></div></div>