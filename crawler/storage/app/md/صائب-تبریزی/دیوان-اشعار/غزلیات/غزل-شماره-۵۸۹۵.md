---
title: >-
    غزل شمارهٔ ۵۸۹۵
---
# غزل شمارهٔ ۵۸۹۵

<div class="b" id="bn1"><div class="m1"><p>ما داغ خود به تاج فریدون نمی دهیم</p></div>
<div class="m2"><p>عریان تنی به اطلس گردون نمی دهیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سینه می کنیم گره شور عشق را</p></div>
<div class="m2"><p>عرض جنون به دامن هامون نمی دهیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قانع به کوه درد ز سنگ ملامتیم</p></div>
<div class="m2"><p>تصدیع اهل شهر چو مجنون نمی دهیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دریا اگر به ساغر ما می کند سپهر</p></div>
<div class="m2"><p>نم چون گهر ز حوصله بیرون نمی دهیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از سیم و زر به چهره زرین خود خوشیم</p></div>
<div class="m2"><p>زین گنج، خاک تیره به قارون نمی دهیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ظلم است هر چه در خم می غیر می کنند</p></div>
<div class="m2"><p>جای شراب را به فلاطون نمی دهیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما از گزیده است ز بس تلخی خمار</p></div>
<div class="m2"><p>از ترس بوسه بر لب میگون نمی دهیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خون خورده ایم تا دل پر خون گرفته ایم</p></div>
<div class="m2"><p>آسان ز دست این قدح خون نمی دهیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وحشی تر از فروغ تجلی است صید ما</p></div>
<div class="m2"><p>دست از دل رمیده به گردون نمی دهیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برگرد خویش سیر چو گرداب می کنیم</p></div>
<div class="m2"><p>چون موج بوسه بر لب جیحون نمی دهیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر چند زیر خرقه بود خون غذای ما</p></div>
<div class="m2"><p>صائب چو نافه رنگ به بیرون نمی دهیم</p></div></div>