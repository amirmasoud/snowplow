---
title: >-
    غزل شمارهٔ ۲۶۲۹
---
# غزل شمارهٔ ۲۶۲۹

<div class="b" id="bn1"><div class="m1"><p>پایه نظم بلند از علم کمتر چون بود؟</p></div>
<div class="m2"><p>علم موزون کم چرا از علم ناموزون بود؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گردبادش جلوه انگشت زنهاری کند</p></div>
<div class="m2"><p>دامن دشتی که گرم از سینه مجنون بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کنج عزلت کرد مستغنی مرا از احتیاج</p></div>
<div class="m2"><p>خم لباس و خانه و گلزار افلاطون بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست ممکن نخل احسانی کند نشو و نما</p></div>
<div class="m2"><p>تا به مغز خاک پنهان ریشه قارون بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر ببندد محتسب میخانه را در، گو ببند</p></div>
<div class="m2"><p>ساقی و نقل و شراب ما لب میگون بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می شود هم پله قارون به اندک فرصتی</p></div>
<div class="m2"><p>دوش هر کس زیر بار منت گردون بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جوش گل سازد خروش بلبلان صائب زیاد</p></div>
<div class="m2"><p>عشق روزافزون شود چون حسن روزافزون بود</p></div></div>