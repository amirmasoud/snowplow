---
title: >-
    غزل شمارهٔ ۶۶۵۳
---
# غزل شمارهٔ ۶۶۵۳

<div class="b" id="bn1"><div class="m1"><p>می فشانم آستین بر افسر گوهرنگار</p></div>
<div class="m2"><p>تا سرم را زیر پای خوش خرام آورده ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از عبادت بر قبول خلق اگر داری نظر</p></div>
<div class="m2"><p>روی در بتخانه از بیت الحرام آورده ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می کشی دست نوازش هر نفس بر دوش زلف</p></div>
<div class="m2"><p>تا کدامین مرغ زیرک را به دام آورده ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست از غیرت به هر کس عرض دادن بی حجاب</p></div>
<div class="m2"><p>دختر رز را چو در عقد دوام آورده ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیده رغبت گر از دنیای دون پوشیده ای</p></div>
<div class="m2"><p>در همین جا روی در دارالسلام آورده ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آرزوی عمر جاویدان ترا صائب بجاست</p></div>
<div class="m2"><p>بی غم از ایام اگر صبحی به شام آورده ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا به روی کار خط مشکفام آورده ای</p></div>
<div class="m2"><p>یکقلم روشن سوادان را به دام آورده ای</p></div></div>