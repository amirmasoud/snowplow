---
title: >-
    غزل شمارهٔ ۲۷۴۳
---
# غزل شمارهٔ ۲۷۴۳

<div class="b" id="bn1"><div class="m1"><p>صیقل دل فیض آه صبحگاهی می دهد</p></div>
<div class="m2"><p>صبحدم بر صدق این معنی گواهی می دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما به دریا لب نیالاییم و چرخ آبگون</p></div>
<div class="m2"><p>می به ما دریاکشان از گوش ماهی می دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که می داند که دردسر به قدر دولت است</p></div>
<div class="m2"><p>کی کلاه خود به تاج پادشاهی می دهد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خنده رویی می دهد یاد از پریشان خاطری</p></div>
<div class="m2"><p>قبض بر جمعیت خاطر گواهی می دهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قهرمان عشق بیتاب است در خون ریختن</p></div>
<div class="m2"><p>این محیط از موج خود سوزن به ماهی می دهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این جواب آن غزل صائب که ناصح گفته است</p></div>
<div class="m2"><p>تا لب ساغر به خون من گواهی می دهد</p></div></div>