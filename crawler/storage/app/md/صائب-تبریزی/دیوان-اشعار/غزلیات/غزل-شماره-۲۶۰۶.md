---
title: >-
    غزل شمارهٔ ۲۶۰۶
---
# غزل شمارهٔ ۲۶۰۶

<div class="b" id="bn1"><div class="m1"><p>گر چنین خوبان صلای جام الفت می دهند</p></div>
<div class="m2"><p>بلبل محجوب ما را بال جرأت می دهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حیرتی دارم که کافر نعمتان درد و داغ</p></div>
<div class="m2"><p>چون به دست آه طومار شکایت می دهند؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طفل طبعان چون مگس بر شهد جان چسبیده اند</p></div>
<div class="m2"><p>تلخکامان جان شیرین را به رغبت می دهند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خون ما را روز محشر شاهدی در کار نیست</p></div>
<div class="m2"><p>لاله رخساران به خون ما شهادت می دهند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دولت حسن غریب آسان نمی آید به دست</p></div>
<div class="m2"><p>روزگاری خاکمال گرد غربت می دهند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از برای عاقلان نزل بلا آماده اند</p></div>
<div class="m2"><p>غافلان را سر به صحرای فراغت می دهند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خضر راهت گر کنند از راهزن ایمن مباش</p></div>
<div class="m2"><p>در خور بیداری اینجا خواب غفلت می دهند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاشقان در حسرت تیغ شهادت سوختند</p></div>
<div class="m2"><p>آب این لب تشنگان را خوش به حکمت می دهند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب آن جمعی که تحصیل مروت کرده اند</p></div>
<div class="m2"><p>سر اگر خواهد به خصم بی مروت می دهند</p></div></div>