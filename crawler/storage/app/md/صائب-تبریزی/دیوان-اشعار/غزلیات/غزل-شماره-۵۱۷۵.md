---
title: >-
    غزل شمارهٔ ۵۱۷۵
---
# غزل شمارهٔ ۵۱۷۵

<div class="b" id="bn1"><div class="m1"><p>تیغ سیراب است موج بحر طوفان زای عشق</p></div>
<div class="m2"><p>داغ ناسورست فلس ماهی دریای عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرده گوش فلک گردید شق از کهکشان</p></div>
<div class="m2"><p>نیست هر نازکدلی را طاقت غوغای عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نور عقلی کز فروغش چشم عالم روشن است</p></div>
<div class="m2"><p>پرده خواب است پیش دیده بینای عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سینه صافان سبز می سازند حرف خصم را</p></div>
<div class="m2"><p>زنگ را طوطی کند آینه سیمای عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جای حیرت نیست گر شد سینه ما چاک چاک</p></div>
<div class="m2"><p>شیشه را چون نار خندان می کند صهبای عشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش چشم هر که چون مجنون غبار عقل نیست</p></div>
<div class="m2"><p>خیمه لیلی است داغ لاله صحرای عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرده ناموس زیبنده است بر بالای عقل</p></div>
<div class="m2"><p>تن به هر تشریف ناقص کی دهد بالای عشق؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در سر شوریده ما عقل سودا می شود</p></div>
<div class="m2"><p>می کند عنبر کف بی مغز را دریای عشق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دست خود بوسید هر کس دامن پاکان گرفت</p></div>
<div class="m2"><p>شد زلیخا رفته رفته یوسف از سودای عشق</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در وصال و هجر صائب اضطراب دل یکی است</p></div>
<div class="m2"><p>هیچ جا لنگر نمی گیرد به خود دریای عشق</p></div></div>