---
title: >-
    غزل شمارهٔ ۳۲۵۶
---
# غزل شمارهٔ ۳۲۵۶

<div class="b" id="bn1"><div class="m1"><p>آب خوب است لب خشکی ازو تر گردد</p></div>
<div class="m2"><p>گره دل شود آن قطره که گوهر گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خار پیراهن ماهی است به اندازه فلس</p></div>
<div class="m2"><p>جای رحم است بر آن کس که توانگر گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکه قانع به در دل نشود از درها</p></div>
<div class="m2"><p>از پریشان نظری حلقه هر در گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مکن اندیشه ز وحشت که به سودازدگان</p></div>
<div class="m2"><p>دامن دشت جنون، دامن مادر گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرکه مجنون تو گردید نگردد عاقل</p></div>
<div class="m2"><p>خون چو شد مشک محال است دگر برگردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر بنه بر خط فرمان که برات خط سبز</p></div>
<div class="m2"><p>نیست ممکن که به صد تیغ دو دم برگردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می شود قند گلو سوز مکرر چون شد</p></div>
<div class="m2"><p>چه شود چون سخن تلخ مکرر گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل چو معمور شد از داغ، شود گنج گهر</p></div>
<div class="m2"><p>سر چو از درد گرانبار شد افسر گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می رسد خشک نگردیده به تشریف جواب</p></div>
<div class="m2"><p>نامه شوقم اگر بال کبوتر گردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی حیایان به نگه خانه زنبور کنند</p></div>
<div class="m2"><p>پرده شرم اگر سد سکندر گردد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بخت خوابیده به فریاد نگردد بیدار</p></div>
<div class="m2"><p>خون چو شد مرده، کجا زنده به نشتر گردد؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باش خرسند چو مردان به قناعت صائب</p></div>
<div class="m2"><p>که فقیر از دل خرسند توانگر گردد</p></div></div>