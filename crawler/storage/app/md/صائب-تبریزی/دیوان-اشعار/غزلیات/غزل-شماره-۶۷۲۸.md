---
title: >-
    غزل شمارهٔ ۶۷۲۸
---
# غزل شمارهٔ ۶۷۲۸

<div class="b" id="bn1"><div class="m1"><p>من به حال مرگ و تو درمان دشمن می کنی</p></div>
<div class="m2"><p>این ستم ها چیست ای بی درد بر من می کنی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بد نکردم چون تویی را برگزیدم از جهان</p></div>
<div class="m2"><p>خاک عالم را چرا در دیده من می کنی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می توان دل را به اندک روی گرمی زنده داشت</p></div>
<div class="m2"><p>آتش ما را چرا محتاج دامن می کنی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیستی گردون، ولی بر عادت گردون تو هم</p></div>
<div class="m2"><p>می کشی آخر چراغی را که روشن می کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرم می پرسی مرا بهر فریب دیگران</p></div>
<div class="m2"><p>در لباس دوستداران کار دشمن می کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست با سنگین دلان هرگز سر و کاری ترا</p></div>
<div class="m2"><p>خنده بر سرگشتگی های فلاخن می کنی</p></div></div>