---
title: >-
    غزل شمارهٔ ۶۶۹۳
---
# غزل شمارهٔ ۶۶۹۳

<div class="b" id="bn1"><div class="m1"><p>بندگی کردن پسندیده است با آزادگی</p></div>
<div class="m2"><p>سرو را خط امان شد از خزان استادگی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد بهار تازه رو را سرو شد شمع مزار</p></div>
<div class="m2"><p>می کشد از چشمه سار خضر آب آزادگی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می شود هر کس به مقدار تواضع سربلند</p></div>
<div class="m2"><p>قطره ناچیز گردد گوهر از افتادگی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساده لوحان بهره ور گردند از نقش مراد</p></div>
<div class="m2"><p>می شود آیینه گلزار، آب از سادگی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چه در میزان بینش از گرانقدران بود</p></div>
<div class="m2"><p>از سبک سنگان بود در پله افتادگی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رد نمی گردد دعای پاک دامانان که اشک</p></div>
<div class="m2"><p>قرة العین اجابت شد ز مردم زادگی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بس که ننشستم ز پا از بی قراری های شوق</p></div>
<div class="m2"><p>بر مجنون را برون آوردم از بی جادگی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من که صد میخانه می کردم به مخموران سبیل</p></div>
<div class="m2"><p>می مکم اکنون لب پیمانه از بی بادگی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صحبت روشن ضمیران زنگ از دل می برد</p></div>
<div class="m2"><p>آب در گوهر نگردد سبز از استادگی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ساده کن صائب دل خود را ز هر نقشی که صبح</p></div>
<div class="m2"><p>می کشد خورشید تابان را به بر از سادگی</p></div></div>