---
title: >-
    غزل شمارهٔ ۱۱۵۴
---
# غزل شمارهٔ ۱۱۵۴

<div class="b" id="bn1"><div class="m1"><p>مردمی در طینت اهل جهان کم مانده است</p></div>
<div class="m2"><p>صورت بی معنیی بر جا ز آدم مانده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نام شاهان از اثر در دور می باشد مدام</p></div>
<div class="m2"><p>شاهد این گفتگو جامی است کز جم مانده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی سخن بر دامن پاکش گواهی می دهد</p></div>
<div class="m2"><p>نسخه ای کز روی شرم آلود مریم مانده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خرد مشمر جرم را کز خوردن گندم به خلد</p></div>
<div class="m2"><p>سینه چاکی به فرزندان آدم مانده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نام باقی در زوال مال فانی بسته است</p></div>
<div class="m2"><p>کز سخاوت بر زبان ها نام حاتم مانده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای که می پرسی ز صحبتها گریزانی چرا</p></div>
<div class="m2"><p>در بساطم وقت ضایع کردنی کم مانده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عام گردیده است بی دردی میان مردمان</p></div>
<div class="m2"><p>حرفی از درد سخن صائب به عالم مانده است</p></div></div>