---
title: >-
    غزل شمارهٔ ۴۴۳
---
# غزل شمارهٔ ۴۴۳

<div class="b" id="bn1"><div class="m1"><p>مده از دست در پیری شراب ارغوانی را</p></div>
<div class="m2"><p>شراب کهنه از دل می برد یاد جوانی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندامت چون لبم را در ته دندان نفرساید؟</p></div>
<div class="m2"><p>چو گل در خنده کردم صرف، ایام جوانی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه خون ها می خورم در پرده دل تا نگه دارم</p></div>
<div class="m2"><p>ز چشم سوزن نامحرم این زخم نهانی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به عاشق می دهی تعلیم جان دادن، چه بی دردی!</p></div>
<div class="m2"><p>چراغ صبح می داند طریق جان فشانی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زبون کش نیستم چون باد صبح از پرتو همت</p></div>
<div class="m2"><p>وگرنه یاد می دادم به شمع آتش زبانی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به امیدی که چون باد بهار از در درون آیی</p></div>
<div class="m2"><p>چو گل در دست خود داریم نقد زندگانی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عجب دارم که بردارد به تن عذر مرا صائب</p></div>
<div class="m2"><p>به جان آزرده ام از خویشتن آن یار جانی را</p></div></div>