---
title: >-
    غزل شمارهٔ ۴۳۸۴
---
# غزل شمارهٔ ۴۳۸۴

<div class="b" id="bn1"><div class="m1"><p>از عود دل گرم من اخگربگریزد</p></div>
<div class="m2"><p>از بزم نفس سوخته مجمر بگریزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اشکم چو عنان ریز نهد روی به دریا</p></div>
<div class="m2"><p>دریا به نهانخانه گوهربگریزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در حشر چو آهم علم شعله فرازد</p></div>
<div class="m2"><p>خورشید به سرچشمه کوثربگریزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک چشم زدن گر قدح از دست گذارم</p></div>
<div class="m2"><p>جان آبله پا تا لب ساغر بگریزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از گرمی خونم که دل سنگ گدازد</p></div>
<div class="m2"><p>چون برق نفس سوخته نشتر بگریزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواری کش عشق تو به گل دست فشاند</p></div>
<div class="m2"><p>سرگرم تو از سایه افسر بگریزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر سینه کند باز دل داغ فریبم</p></div>
<div class="m2"><p>داغ از جگر لاله احمر بگریزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون شعله خوی تو کشد تیغ سیاست</p></div>
<div class="m2"><p>آتش به ته بال سمندر بگریزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون دست گهربار برون آورد از جیب</p></div>
<div class="m2"><p>از پشت صدف نطفه گوهر بگریزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صائب چو شوم گرم به توصیف ظفرخان</p></div>
<div class="m2"><p>از خامه من بال سمندر بگریزد</p></div></div>