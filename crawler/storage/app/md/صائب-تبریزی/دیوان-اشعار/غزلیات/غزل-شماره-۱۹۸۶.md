---
title: >-
    غزل شمارهٔ ۱۹۸۶
---
# غزل شمارهٔ ۱۹۸۶

<div class="b" id="bn1"><div class="m1"><p>لعلت به خنده پرده گل را دریده است</p></div>
<div class="m2"><p>آیینه از رخت گل خورشید چیده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نظاره تو تازه کند داغ کهنه را</p></div>
<div class="m2"><p>این لاله گویی ز دل آتش چکیده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اسباب تیره بختی ما دست داده است</p></div>
<div class="m2"><p>تا سرمه ات به گوشه ابرو رسیده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کار تو نیست چاره درد من ای مسیح</p></div>
<div class="m2"><p>این شیوه را تبسم او آفریده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما برق را بر آتش غیرت نشانده ایم</p></div>
<div class="m2"><p>سیماب در قلمرو ما آرمیده است</p></div></div>