---
title: >-
    غزل شمارهٔ ۶۸۲۵
---
# غزل شمارهٔ ۶۸۲۵

<div class="b" id="bn1"><div class="m1"><p>سینه باغی است که گلشن شود از خاموشی</p></div>
<div class="m2"><p>دل چراغی است که روشن شود از خاموشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیشتر فتنه عالم ز سخن می زاید</p></div>
<div class="m2"><p>مادر فتنه سترون شود از خاموشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مهر زن بر لب گفتار که در بزم جهان</p></div>
<div class="m2"><p>شمع آسوده ز کشتن شود از خاموشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل که در رهگذر باد حوادث شمعی است</p></div>
<div class="m2"><p>چون چراغ ته دامن شود از خاموشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلبل از زمزمه خویش به بند افتاده است</p></div>
<div class="m2"><p>از قفس مرغ به گلشن شود از خاموشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچ طفلی نشنیدیم درین عبرتگاه</p></div>
<div class="m2"><p>که لبش زخمی سوزن شود از خاموشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل ز روشنگر حیرت ید بیضا گردد</p></div>
<div class="m2"><p>سینه ها وادی ایمن شود از خاموشی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر توانی سپر از مهر خموشی انداخت</p></div>
<div class="m2"><p>مو بر اندام تو جوشن شود از خاموشی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل آزاد تو آن روز شود بی زنگار</p></div>
<div class="m2"><p>که زبان سبز چو سوسن شود از خاموشی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خاک اگر در دهن رخنه گفتار زند</p></div>
<div class="m2"><p>آدمی قلعه آهن شود از خاموشی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیست جز مهر خموشی به جهان جام جمی</p></div>
<div class="m2"><p>راز عالم به تو روشن شود از خاموشی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر زبان را ز سخن پاک توانی کردن</p></div>
<div class="m2"><p>خوشه ات صاحب خرمن شود از خاموشی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رشته عمر که بر سستی خود می لرزد</p></div>
<div class="m2"><p>ایمن از بیم گسستن شود از خاموشی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کثرت و تفرقه در عالم گفتار بود</p></div>
<div class="m2"><p>که جهانی همه یک تن شود از خاموشی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از ره حرف بود رنجش مردم صائب</p></div>
<div class="m2"><p>کس ندیدیم که دشمن شود از خاموشی</p></div></div>