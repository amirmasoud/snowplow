---
title: >-
    غزل شمارهٔ ۱۰۸۳
---
# غزل شمارهٔ ۱۰۸۳

<div class="b" id="bn1"><div class="m1"><p>خاکساری تا دلیل جان آگاه من است</p></div>
<div class="m2"><p>می کند هموار هر چاهی که در راه من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشت بر خارا زدن، بازوی خود رنجاندن است</p></div>
<div class="m2"><p>می کند با خویش بد هر کس که بدخواه من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>انتقام از دشمن عاجز به نیکی می کشم</p></div>
<div class="m2"><p>می کنم سرسبز خاری را که در راه من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خصم می پیچد به خویش از بردباری های من</p></div>
<div class="m2"><p>این خروش سیل از دیوار کوتاه من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلبل از غیرت به خون من گواهی می دهد</p></div>
<div class="m2"><p>ورنه هر برگی درین گلشن هواخواه من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دشت مجنون آهنین پایی ندارد همچو من</p></div>
<div class="m2"><p>دود از هر جا که برخیزد قدمتگاه من است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این جواب آن غزل صائب که می گوید کلیم</p></div>
<div class="m2"><p>هر چه جانکاه است در این راه، دلخواه من است</p></div></div>