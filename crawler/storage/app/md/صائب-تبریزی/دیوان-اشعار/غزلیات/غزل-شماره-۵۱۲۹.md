---
title: >-
    غزل شمارهٔ ۵۱۲۹
---
# غزل شمارهٔ ۵۱۲۹

<div class="b" id="bn1"><div class="m1"><p>مابه خون جگریم از می گلگون قانع</p></div>
<div class="m2"><p>با خماریم ز لعل لب میگون قانع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز از می نشود جام نگونش خالی</p></div>
<div class="m2"><p>هرکه چون لاله شود بادل پر خون قانع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می شود ساغرش ازباده حکمت لبریز</p></div>
<div class="m2"><p>شد به خم هرکه ز مسکن چو فلاطون قانع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فارغ از دردسر جاه شود ، هرکس شد</p></div>
<div class="m2"><p>به کلاه نمدازتاج فریدون قانع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خفظ اندازه محال است توان در می کرد</p></div>
<div class="m2"><p>چون به صد بوسه شوم زان لب میگون قانع؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از نظربازی آن لیلی عالم صائب</p></div>
<div class="m2"><p>به تماشای غزالیم چو مجنون قانع</p></div></div>