---
title: >-
    غزل شمارهٔ ۵۲۲۵
---
# غزل شمارهٔ ۵۲۲۵

<div class="b" id="bn1"><div class="m1"><p>می شود دایره خلق ز بیماری تنگ</p></div>
<div class="m2"><p>زین سبب چشم تو پیوسته بود بر سر جنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تندخو را نشود آینه دل بی زنگ</p></div>
<div class="m2"><p>که محال است سیاهی فتد از داغ پلنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دل سخت بتان عجز چه تاثیر کند</p></div>
<div class="m2"><p>نخل مومین چه رگ ریشه دواند درسنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم آسودگی از عالم پر شور خطاست</p></div>
<div class="m2"><p>مهد آسایش این بحر بود کام نهنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عجبی نیست اگر پشت کمان راست شود</p></div>
<div class="m2"><p>از هم آغوشی آن قامت چون تیر خدنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داده خویش نگیرند کریمان واپس</p></div>
<div class="m2"><p>لعل ویاقوت ز خورشید نمی بازد رنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشود روزی شیرین سخنان آزادی</p></div>
<div class="m2"><p>تا برآمد شکر از بند نی افتاد به تنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در ریاضی که بود شبنم گلها سیماب</p></div>
<div class="m2"><p>به چه امید زند بلبل مابرآهنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل ازان زلف محال است رهایی یابد</p></div>
<div class="m2"><p>چه خیال است مسلمان از قید فرنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به شکوهی که نشسته است مرا در دل عشق</p></div>
<div class="m2"><p>هیچ شاهی ننشسته است چنان بر او رنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>محملی لیلی اگر در صدد جولان نیست</p></div>
<div class="m2"><p>چون درین بادیه هر ذره بود گوش به زنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر که را درد طلب هست ز پا ننشیند</p></div>
<div class="m2"><p>نیست در قافله ریگ روان صائب لنگ</p></div></div>