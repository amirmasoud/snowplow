---
title: >-
    غزل شمارهٔ ۳۸۶
---
# غزل شمارهٔ ۳۸۶

<div class="b" id="bn1"><div class="m1"><p>سفیدی های مو بیدار کی سازد سیه دل را؟</p></div>
<div class="m2"><p>که گلبانگ رحیل افسانه خواب است غافل را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز نقصان بصیرت طامعان را نیست پروایی</p></div>
<div class="m2"><p>که چشم کور گردد کاسه دریوزه سایل را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نلرزد چون ز بی آرامیم مهد لحد بر خود؟</p></div>
<div class="m2"><p>که من در راه کردم از گرانی خواب منزل را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شهادت می کند ایجاد اسباب طرب از خود</p></div>
<div class="m2"><p>که مطرب باشد از بال و پر خود رقص بسمل را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زبان عذرخواهی صید بسمل را نمی باشد</p></div>
<div class="m2"><p>مگر خواهم به حیرت عذر دست و تیغ قاتل را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز سنگ کودکان پهلو تهی کردم، ندانستم</p></div>
<div class="m2"><p>که می گردد شکستن مومیایی شیشه دل را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو قمری سر و بر گردن نهد طوق گرفتاری</p></div>
<div class="m2"><p>اگر افتد به گلشن راه آن مشکین سلاسل را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگوید عشق اسرار محبت با هوسناکان</p></div>
<div class="m2"><p>نیفشاند به خاک شوره دهقان تخم قابل را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به خط امیدها دارد دل بی طاقت عاشق</p></div>
<div class="m2"><p>که سازد توتیای چشم، طوفان دیده ساحل را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ازان هر لحظه مجنون در بیابانی کند جولان</p></div>
<div class="m2"><p>که در هر جلوه لیلی می دهد تغییر محمل را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شوند از اهل مشرب زاهدان خشک هم صائب</p></div>
<div class="m2"><p>توان گر گوهر شهوار کردن مهره گل را</p></div></div>