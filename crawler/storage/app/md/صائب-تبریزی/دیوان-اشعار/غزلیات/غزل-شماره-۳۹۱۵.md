---
title: >-
    غزل شمارهٔ ۳۹۱۵
---
# غزل شمارهٔ ۳۹۱۵

<div class="b" id="bn1"><div class="m1"><p>سخنوران که درین بوستان نوا سازند</p></div>
<div class="m2"><p>کباب یکدگر از شعله های آوازند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مبین به چشم حقارت شکسته بالان را</p></div>
<div class="m2"><p>که در گرفتن عبرت هزار شهبازند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چگونه کاسه پرزهر مرگ را نوشند</p></div>
<div class="m2"><p>جماعتی که بدآموز نعمت ونازند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شوندکامروا چون دعای دامن شب</p></div>
<div class="m2"><p>جماعتی که مشکین خطان نظربازند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هلاک کنج لب وگوشه های آن چشمم</p></div>
<div class="m2"><p>که دلپذیر تر از گوشه های شیرازند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز رفتگان ره دشوار مرگ شد آسان</p></div>
<div class="m2"><p>گذشتگان پل این سیل خانه پردازند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمی رسند به معراج گفتگو صائب</p></div>
<div class="m2"><p>جماعتی که به دعوی بلند پروازند</p></div></div>