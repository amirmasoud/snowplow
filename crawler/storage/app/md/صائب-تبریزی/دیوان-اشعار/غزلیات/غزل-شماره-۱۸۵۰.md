---
title: >-
    غزل شمارهٔ ۱۸۵۰
---
# غزل شمارهٔ ۱۸۵۰

<div class="b" id="bn1"><div class="m1"><p>شب گذشته دل از زلف پر شکن می گفت</p></div>
<div class="m2"><p>غریب بود، ز حب الوطن سخن می گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گهر چو کرد وداع صدف عزیز شود</p></div>
<div class="m2"><p>عزیز مصر به یعقوب این سخن می گفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر پیاله سراپا دهن نمی گردید</p></div>
<div class="m2"><p>که حرف بوسه ما را به آن دهن می گفت؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ازان خموش به کنجی نشسته بودم دوش</p></div>
<div class="m2"><p>که شرح حال مرا شمع انجمن می گفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هلال واری ازان سینه دید و رفت از دست</p></div>
<div class="m2"><p>گلی که روز وز شب از چاک پیرهن می گفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همیشه آه هوادار لاله رویان بود</p></div>
<div class="m2"><p>نسیم تا نفس آخر از چمن می گفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو غنچه مشت زری عندلیب اگر می داشت</p></div>
<div class="m2"><p>هزار نکته رنگین به یک دهن می گفت</p></div></div>