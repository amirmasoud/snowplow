---
title: >-
    غزل شمارهٔ ۶۳۸۱
---
# غزل شمارهٔ ۶۳۸۱

<div class="b" id="bn1"><div class="m1"><p>دل از گناه پاک چو دارالسلام کن</p></div>
<div class="m2"><p>خاک سیاه بر سر مینا و جام کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون برق، ذوق باده بود پای در رکاب</p></div>
<div class="m2"><p>عیش مدام خواهی، ترک مدام کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواهی چو شعله چشم و چراغ جهان شوی</p></div>
<div class="m2"><p>در پیش پای هر خس و خاری قیام کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آب حیات در ظلمات است، زینهار</p></div>
<div class="m2"><p>مانند شمع در دل شبها قیام کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون سرو پا به دامن آزادگی بکش</p></div>
<div class="m2"><p>آنگاه در بهشت فراغت خرام کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در زیر زلف یأس بود چهره امید</p></div>
<div class="m2"><p>هر جا دلت فرود نیاید مقام کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آب حیات دولت فانی است نام نیک</p></div>
<div class="m2"><p>این دولت دو روزه خود مستدام کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر چند ناله تو کند دانه را سپند</p></div>
<div class="m2"><p>ای مرغ خوش نوا، حذر از چشم دام کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما خون گرم خویش حلال تو کرده ایم</p></div>
<div class="m2"><p>خواهی به شیشه افکن و خواهی به جام کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بزم شراب، بی مزه بوسه ناقص است</p></div>
<div class="m2"><p>پیش آی و عیش ناقص ما را تمام کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خواهی که بر رخ تو در فیض وا شود</p></div>
<div class="m2"><p>چون صائب اقتدا به حدیث و کلام کن</p></div></div>