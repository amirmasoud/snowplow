---
title: >-
    غزل شمارهٔ ۴۵۲۵
---
# غزل شمارهٔ ۴۵۲۵

<div class="b" id="bn1"><div class="m1"><p>ترا چون صبح خندان آفریدند</p></div>
<div class="m2"><p>مرا چون ابر گریان آفریدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من آن روز از سلامت دست شستم</p></div>
<div class="m2"><p>که آن چاه زنخدان آفریدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلاهای سیه را جمع کردند</p></div>
<div class="m2"><p>ازان زلف پریشان آفریدند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دونیم آن روز شد چون پسته دلها</p></div>
<div class="m2"><p>که آن لبهای خندان آفریدند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکست آن روز شاخ زلف خوبان</p></div>
<div class="m2"><p>که آن خط چو ریحان آفریدند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لطافتهای عالم گرد کردند</p></div>
<div class="m2"><p>ازان سیب زنخدان آفریدند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برای شمع آن روی دل افروز</p></div>
<div class="m2"><p>ز بخت ما شبستان آفریدند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ازان مژگان شرم آلود در دل</p></div>
<div class="m2"><p>جراحتهای پنهان آفریدند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکست آن روز بر قلب دل افتاد</p></div>
<div class="m2"><p>که آن صفهای مژگان آفریدند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فلکها شد چو گو آن روز غلطان</p></div>
<div class="m2"><p>که آن زلف چو چوگان آفریدند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سرزلف سبکدست بتان را</p></div>
<div class="m2"><p>پی تاراج ایمان آفریدند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر در حسن خوبان هست آنی</p></div>
<div class="m2"><p>سراپای ترا زان آفریدند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پی تاراج خرمنگاه هستی</p></div>
<div class="m2"><p>نگاه برق جولان آفریدند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو چشم یار ما دلخستگان را</p></div>
<div class="m2"><p>ز عین درد درمان آفریدند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به خود پرداختن زان دل نیاید</p></div>
<div class="m2"><p>که چون آیینه حیران آفریدند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ازان لبها شراب ونقل صائب</p></div>
<div class="m2"><p>برای می پرستان آفریدند</p></div></div>