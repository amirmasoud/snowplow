---
title: >-
    غزل شمارهٔ ۱۶۸۰
---
# غزل شمارهٔ ۱۶۸۰

<div class="b" id="bn1"><div class="m1"><p>به لب مباد رهش ناله ای که بی اثرست</p></div>
<div class="m2"><p>گره شود به گلو گریه ای که بیجگرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل نمک به حرامی است تیره روزی داغ</p></div>
<div class="m2"><p>شکسته رنگی خون از خمار نیشترست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لبش به حرف عتاب آشنا نگردیده است</p></div>
<div class="m2"><p>هنوز آتش یاقوت، مفلس شررست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کدام فتنه گر امشب درین چمن بوده است؟</p></div>
<div class="m2"><p>که رخت لاله پر از خون و گل شکسته سرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمک ز خنده گل برده است گریه من</p></div>
<div class="m2"><p>ز چشم بی ادبم باغبان باغ ترست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی که پاس نفس چون حباب نتواند</p></div>
<div class="m2"><p>همیشه چون صدف هرزه خند بی گهرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکایت از ستم چرخ ناجوانمردی است</p></div>
<div class="m2"><p>که گوشمال پدر خیرخواهی پسرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نخورده ام به دل شبنمی درین گلشن</p></div>
<div class="m2"><p>چو خون لاله و گل خون من چرا هدرست؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هزار طاقت ایوب می شود کمری</p></div>
<div class="m2"><p>چه دستگاه سرین و چه پیچش کمرست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لبی که از نفس بوسه رنگ می بازد</p></div>
<div class="m2"><p>چه جای جلوه تبخاله های بد گهرست؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سپر فکند فلک پیش آه من صائب</p></div>
<div class="m2"><p>علاج دشمن غالب فکندن سپرست</p></div></div>