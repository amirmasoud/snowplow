---
title: >-
    غزل شمارهٔ ۵۹
---
# غزل شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>داد سیل گریه من غوطه در گل بحر را</p></div>
<div class="m2"><p>گوهر از گرد یتیمی کرد ساحل بحر را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همت سرشار از بی سایلی خون می خورد</p></div>
<div class="m2"><p>کز گهر باشد هزاران عقده در دل بحر را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق دریا دل نمی اندیشد از زخم زبان</p></div>
<div class="m2"><p>کی خلد در دل خس و خاشاک ساحل بحر را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در غریبی کی فتند از جستجو روشندلان؟</p></div>
<div class="m2"><p>در سفر کردن به جز خود نیست منزل بحر را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قاصدان از ابر گوهربار دارد هر طرف</p></div>
<div class="m2"><p>کی کند دوری ز خاک خشک غافل بحر را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کجا دفتر گشاید دیده پر شور من</p></div>
<div class="m2"><p>از نظرها افکند چون فرد باطل بحر را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کی شود زنجیر صائب مانع شور جنون؟</p></div>
<div class="m2"><p>موج نتواند کشیدن در سلاسل بحر را</p></div></div>