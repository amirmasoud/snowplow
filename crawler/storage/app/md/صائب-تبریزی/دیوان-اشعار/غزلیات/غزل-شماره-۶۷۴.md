---
title: >-
    غزل شمارهٔ ۶۷۴
---
# غزل شمارهٔ ۶۷۴

<div class="b" id="bn1"><div class="m1"><p>زهی نقاب جمالت برهنه‌رویی‌ها</p></div>
<div class="m2"><p>خموشی تو زبان‌بند کام‌جویی‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز سرو قد تو یک جلوه، عالم آشوبی</p></div>
<div class="m2"><p>ز نوبهار تو یک برق، تندخویی‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که نام شهرت یاقوت می‌برد امروز؟</p></div>
<div class="m2"><p>که ختم شد به عقیق تو نامجویی‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فتاده است چو تقویم کهنه از پرگار</p></div>
<div class="m2"><p>به دور حسن تو، مجموعه نکویی‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگرچه آن مژه را خواب ناز سنگین است</p></div>
<div class="m2"><p>دمی ز پا ننشیند ز فتنه‌جویی‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بشوی دست ز اصلاح تن، به جان پرداز</p></div>
<div class="m2"><p>که دل سفید نگردد ز جامه‌شویی‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر توقع آسایش از جهان داری</p></div>
<div class="m2"><p>مدار دست ز نبض مزاج‌گویی‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به خنده زندگی خویش را مکن کوتاه</p></div>
<div class="m2"><p>که صبح غوطه به خون زد ز خنده‌رویی‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جز این که داد سر خویش را به باد حباب</p></div>
<div class="m2"><p>چه طرف بست ندانم ز پوچ‌گویی‌ها؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو فرد آینه با کاینات یک‌رو باش</p></div>
<div class="m2"><p>که شد سیاه رخ کاغذ از دورویی‌ها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنان که شیر کند خواب طفل را شیرین</p></div>
<div class="m2"><p>فزود غفلت من از سفیدمویی‌ها</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر نکو نشوی صائب از بدی بگذر</p></div>
<div class="m2"><p>که هست ترک بدی‌ها سر نکویی‌ها</p></div></div>