---
title: >-
    غزل شمارهٔ ۴۳۲۲
---
# غزل شمارهٔ ۴۳۲۲

<div class="b" id="bn1"><div class="m1"><p>با عشق انتقام توان ز آسمان کشید</p></div>
<div class="m2"><p>نتوان به زور بازوی عقل این کمان کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیگر چه لازم است که مشق جنون کند</p></div>
<div class="m2"><p>دیوانه ای که خط به سواد جهان کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با خامشی بساز که تلخی نمی کشد</p></div>
<div class="m2"><p>این شهد را کسی که به کام وزبان کشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیوان عاشقان به قیامت نمی کشد</p></div>
<div class="m2"><p>خط انتقام ما ز رخ دلستان کشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرد کدورتی که ز اغیار داشتم</p></div>
<div class="m2"><p>دیوار در میان من ودلستان کشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد کند از ملایمت من زبان خصم</p></div>
<div class="m2"><p>دندان مار را به نمد می توان کشید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب بغیر گوشه دل نیست در جهان</p></div>
<div class="m2"><p>امروز گوشه ای که نفس می توان کشید</p></div></div>