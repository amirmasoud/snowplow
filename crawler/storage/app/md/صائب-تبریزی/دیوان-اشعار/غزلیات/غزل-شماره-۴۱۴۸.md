---
title: >-
    غزل شمارهٔ ۴۱۴۸
---
# غزل شمارهٔ ۴۱۴۸

<div class="b" id="bn1"><div class="m1"><p>مردم ز فیض عالم بالا چه دیده اند</p></div>
<div class="m2"><p>غیر از حباب وموج ز دریا چه دیده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما پیش پای خویش ندیدیم همچو شمع</p></div>
<div class="m2"><p>تا دیگران ز دیده بیناچه دیده اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما سر تیره بختی خود را نیافتیم</p></div>
<div class="m2"><p>تا روشنان عالم بالا چه دیده اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جمعی که بسته اند کمر در شکست ما</p></div>
<div class="m2"><p>غیر از صفا ز آینه ما چه دیده اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل چون گشاده نیست چه صحرا چه کوچه بند</p></div>
<div class="m2"><p>سوداییان ز دامن صحرا چه دیده اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنها که ترک دولت جاوید کرده اند</p></div>
<div class="m2"><p>زین پنج روز دولت دنیا چه دیده اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جمعیت است سلسله جنبان افتراق</p></div>
<div class="m2"><p>مردم ز جمع کردن دنیا چه دیده اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما حاصلی ز پرورش خود نیافتیم</p></div>
<div class="m2"><p>تا نه فلک ز پرورش ما چه دیده اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صد زخم می خورند و ز دنبال می روند</p></div>
<div class="m2"><p>مردم ز خار خار تمنا چه دیده اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پوشیده چشم می گذرند از در بهشت</p></div>
<div class="m2"><p>تا اهل دل ز رخنه دلها چه دیده اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جمعی که راه عقل به پایان رسانده اند</p></div>
<div class="m2"><p>جز ماندگی وآبله پا چه دیده اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون نرگس این گروه که ارباب بینشند</p></div>
<div class="m2"><p>جز پیش پا ز دیده بینا چه دیده اند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون می کند به وعده وفا عاقبت کریم</p></div>
<div class="m2"><p>این شوخ دیدگان ز تقاضا چه دیده اند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در پیش پای خویش نبینند از غرور</p></div>
<div class="m2"><p>نادیدگان ز خویشتن آیاچه دیده اند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون کار کردنی است هم امروز خوشترست</p></div>
<div class="m2"><p>این کاهلان ز مهلت فردا چه دیده اند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از عقل نیست دل به سر زلف باختن</p></div>
<div class="m2"><p>یاران موشکاف در اینجا چه دیده اند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در حیرتم که نغمه سرایان این چمن</p></div>
<div class="m2"><p>در گل بغیر خنده بیجا چه دیده اند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در چشم بستن است تماشای هر دوکون</p></div>
<div class="m2"><p>این کور باطنان ز تماشاچه دیده اند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صائب چو در شکستن خود امید نصرت است</p></div>
<div class="m2"><p>احباب در شکستن اعدا چه دیده اند</p></div></div>