---
title: >-
    غزل شمارهٔ ۴۰۹۶
---
# غزل شمارهٔ ۴۰۹۶

<div class="b" id="bn1"><div class="m1"><p>تا دیده محو روی تو شد کامیاب شد</p></div>
<div class="m2"><p>شبنم به آفتاب رسید آفتاب شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسن تو از دمیدن خط کامیاب شد</p></div>
<div class="m2"><p>پیغمبر جمال تو صاحب کتاب شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از شرم زلف وروی تو در ناف آهوان</p></div>
<div class="m2"><p>صد بار مشک خون شد وخون مشک ناب شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک چشم خواب تلخ جهان در بساط داشت</p></div>
<div class="m2"><p>آن هم نصیب دیده شور حباب شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا چهره تو در عرق شرم غوطه زد</p></div>
<div class="m2"><p>هر آرزو که در دل من بود آب شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آب حیات خضر گل آلود منت است</p></div>
<div class="m2"><p>خوش وقت تشنه ای که دچار سراب شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون دید گل به دیده شبنم بقای عمر</p></div>
<div class="m2"><p>در بوته گداز در آمد گلاب شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از رفتن حباب چه پرواست بحر را</p></div>
<div class="m2"><p>عشق ترا ازین چه که عالم خراب شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب ز فیض جاذبه عشق عاقبت</p></div>
<div class="m2"><p>با آفتاب ذره من همرکاب شد</p></div></div>