---
title: >-
    غزل شمارهٔ ۳۶۲۸
---
# غزل شمارهٔ ۳۶۲۸

<div class="b" id="bn1"><div class="m1"><p>کلکم از سیر بدخشان سخن می آید</p></div>
<div class="m2"><p>سرخ رو از سر میدان سخن می آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شور غیرت به نمکدان مسیح افکندن</p></div>
<div class="m2"><p>از شکر خنده پنهان سخن می آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تیر از جوشن الماس ترازو کردن</p></div>
<div class="m2"><p>از کمین جنبش مژگان سخن می آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سبزی بخت به طاوس دهد راه آورد</p></div>
<div class="m2"><p>طوطیی کز شکرستان سخن می آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جوی شیری که سفیدست ازو روی بهشت</p></div>
<div class="m2"><p>از سیه چشمه پستان سخن می آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه جا دست بدستش به سر کلک برند</p></div>
<div class="m2"><p>هر متاعی که ز یونان سخن می آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر نسیمی که گشاید گره از کار دلی</p></div>
<div class="m2"><p>می توان یافت ز بستان سخن می آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باطن اهل سخن تیغ به کف استاده است</p></div>
<div class="m2"><p>تا که گستاخ به میدان سخن می آید؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه شکنها که ز سرپنجه ارباب سخن</p></div>
<div class="m2"><p>به سر زلف پریشان سخن می آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صبر کن بر ستم چرخ دو روزی صائب</p></div>
<div class="m2"><p>نوبت قافیه سنجان سخن می آید</p></div></div>