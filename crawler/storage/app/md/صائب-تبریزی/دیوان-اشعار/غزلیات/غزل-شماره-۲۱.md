---
title: >-
    غزل شمارهٔ ۲۱
---
# غزل شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>یک نظر بازست نرگس چشم بیمار ترا</p></div>
<div class="m2"><p>گل یکی از سینه چاکان است دستار ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می کند شبنم گرانی بر عذار نازکت</p></div>
<div class="m2"><p>ابر می بوسد زمین از دور گلزار ترا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خشک می آید به چشمش جلوه آب حیات</p></div>
<div class="m2"><p>هر که در مستی تماشا کرده رفتار ترا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سبز می گردد ز حیرت حرف در منقارشان</p></div>
<div class="m2"><p>طوطیان آیینه گر سازند رخسار ترا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تماشای تو خورشیدست یک چشم پر آب</p></div>
<div class="m2"><p>چون تواند سیر دیدن دیده دیدار ترا؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس که می چسبد به هم کام و لب از شیرینی اش</p></div>
<div class="m2"><p>نقل نتوان کرد گفتار شکربار ترا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا چه در پیراهن گلهای بی خارش بود</p></div>
<div class="m2"><p>ناز مژگان است در سر، خار دیوار ترا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ساده می سازد ز جوهر، روشنی آیینه را</p></div>
<div class="m2"><p>نیست پروای خط شبرنگ، رخسار ترا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دست گلچین را ز حیرت پای خواب آلود ساخت</p></div>
<div class="m2"><p>احتیاج دور باشی نیست گلزار ترا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آب می گردید در چشم ترازو گوهرش</p></div>
<div class="m2"><p>یوسف مصری اگر می دید بازار ترا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اهل دین را می برد از راه، زلف کافرت</p></div>
<div class="m2"><p>در بغل چون رشته گیرد سبحه زنار ترا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کردم از دین و دل و هوش و خرد قطع نظر</p></div>
<div class="m2"><p>من همان روزی که دیدم چشم عیار ترا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرگ نتواند عنان بی قراران را گرفت</p></div>
<div class="m2"><p>نیست زیر خاک آسایش طلبکار ترا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قابل قسمت شمارد نقطه موهوم را</p></div>
<div class="m2"><p>هر که بیند در سخن لعل گهربار ترا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گردی از دور از نمکدان قیامت دیده است</p></div>
<div class="m2"><p>هر که صائب از تو نشنیده است گفتار ترا</p></div></div>