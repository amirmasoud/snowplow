---
title: >-
    غزل شمارهٔ ۴۴۷۵
---
# غزل شمارهٔ ۴۴۷۵

<div class="b" id="bn1"><div class="m1"><p>از عشق یار نوخط دل زود می گشاید</p></div>
<div class="m2"><p>فصل بهاراز دل زنگار می زداید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسن برهنه رویان بر یک قرار باشد</p></div>
<div class="m2"><p>هر روز خط کمالی بر حسن می فزاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از یار چارابرو سخت است دل گرفتن</p></div>
<div class="m2"><p>کشتی ز چار موجه کمتر به ساحل آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کس فکند خود راافکند عالمی را</p></div>
<div class="m2"><p>هر کس به خود برآید با عالمی برآید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق است بی تکلف حسن است لاابالی</p></div>
<div class="m2"><p>تا با که خوش بر آید تا از کجا نماید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آیینه دار عشقند ذرات هر دو عالم</p></div>
<div class="m2"><p>این آفتاب جانسوز تا از کجا برآید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گلهای بوستانی بر هم نهند دیوان</p></div>
<div class="m2"><p>دیوان خویش صائب در هر کجا گشاید</p></div></div>