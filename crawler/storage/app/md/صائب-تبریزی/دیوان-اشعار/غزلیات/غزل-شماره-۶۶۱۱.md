---
title: >-
    غزل شمارهٔ ۶۶۱۱
---
# غزل شمارهٔ ۶۶۱۱

<div class="b" id="bn1"><div class="m1"><p>بریده نعل ز عشق که بر جگر لاله؟</p></div>
<div class="m2"><p>به سنبل که سیه کرده چشم تر لاله؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کند به زاهد و میخواره یک روش تأثیر</p></div>
<div class="m2"><p>فتاده است چو آتش به خشک و تر لاله</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سبک به باد فنا چون شرار خواهد رفت</p></div>
<div class="m2"><p>اگر ز کوه زند دست بر کمر لاله</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز لطف طبع بهاران مگر خبر دارد؟</p></div>
<div class="m2"><p>که دود آه گره کرده در جگر لاله</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکوفه صبح بهارست و لاله است شفق</p></div>
<div class="m2"><p>پس از شکوفه ازان گشت جلوه گر لاله</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز زهد خشک اثر در جهان نخواهد ماند</p></div>
<div class="m2"><p>اگر چنین شود از باغ شعله ور لاله</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر نه از رخ گلرنگ یار منفعل است</p></div>
<div class="m2"><p>چرا ز خاک برآید فکنده سر لاله؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به وقت لاله می لاله رنگ حاجت نیست</p></div>
<div class="m2"><p>که همچو جام صبوحی کند اثر لاله</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز درد و صافی تهی نیست جام سوختگان</p></div>
<div class="m2"><p>که نصف خون بود و نصف مشک تر لاله</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز خاک تیره چه می جوید و چه گم کرده است؟</p></div>
<div class="m2"><p>که برفروخت چراغی به هر گذر لاله</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سواد شهر به خونین دلان کند تنگی</p></div>
<div class="m2"><p>به کوه و دشت کند جوش بیشتر لاله</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مدار دست ز مینا و جام در فصلی</p></div>
<div class="m2"><p>که شیشه ساز شود غنچه، کاسه گر لاله</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به هر دو دست سر خویش توبه می گیرد</p></div>
<div class="m2"><p>چو تاج لعل نهد کج به طرف سر لاله</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به چشم هر که چو مجنون سوادخوان شده است</p></div>
<div class="m2"><p>سیاه خیمه لیلی است داغ هر لاله</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به داغ عشق سرآمد توان ز اقران شد</p></div>
<div class="m2"><p>که از سراسر گلهاست تا جور لاله</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر چه لاله بسی هست نوبهاران را</p></div>
<div class="m2"><p>ز چهره تو ندارد برشته تر لاله</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به نقل و باده درین موسم احتیاجی نیست</p></div>
<div class="m2"><p>که هم شراب بود هم کباب تر لاله</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به یک پیاله نگردد کس این چنین مدهوش</p></div>
<div class="m2"><p>سیاه مست شد از باده دگر لاله</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پس از شکوفه مده سیر لاله زار از دست</p></div>
<div class="m2"><p>که هست چون شفق صبح در گذر لاله</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به برگ لاله نه شبنم بود، که دندان را</p></div>
<div class="m2"><p>ز رشک روی تو افشرده بر جگر لاله</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنان که در جگر لعل آب پنهان است</p></div>
<div class="m2"><p>نهان شده است چنان تیغ کوه در لاله</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عجب که توبه سنگین ما کمر بندد</p></div>
<div class="m2"><p>که کوه را زده از جوش بر کمر لاله</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به مشت خار و خس ما دل که خواهد سوخت؟</p></div>
<div class="m2"><p>در آن ریاض که گردیده پی سپر لاله</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز نور عاریه مستغنی اند سوختگان</p></div>
<div class="m2"><p>به روشنایی خود می کند سفر لاله</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دوروزه مهلت خود صرف میگساری کرد</p></div>
<div class="m2"><p>چه غافل است به این عمر مختصر لاله</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دلش چو پای چراغ است ازان سیاه مدام</p></div>
<div class="m2"><p>که روز و شب بود از باده بی خبر لاله</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>توان به خون جگر گوهر بصیرت یافت</p></div>
<div class="m2"><p>که شد ز خوردن خونابه دیده ور لاله</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز شرم، روی تو هر جا عرق فشان گردد</p></div>
<div class="m2"><p>شکسته کاسه دریوزه ای است هر لاله</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مگر خیال شبیخون تازه ای دارد؟</p></div>
<div class="m2"><p>که یکه تاز برون آمده است هر لاله</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شکوفه چون سپه روم روی گردانده است</p></div>
<div class="m2"><p>شده است تا چو قزلباش جلوه گر لاله</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بناز بر جگر داغدار خود صائب</p></div>
<div class="m2"><p>که هیچ باغ نمی دارد اینقدر لاله</p></div></div>