---
title: >-
    غزل شمارهٔ ۴۴۱۷
---
# غزل شمارهٔ ۴۴۱۷

<div class="b" id="bn1"><div class="m1"><p>چون شبنم می بر رخ جانان بنشیند</p></div>
<div class="m2"><p>در آب وعرق چشمه حیوان بنشیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرمنده خونگرمی اشکم که همه عمر</p></div>
<div class="m2"><p>نگذاشت مراگردبه مژگان بنشیند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل صاف کن آن گاه ز ماحرف طلب کن</p></div>
<div class="m2"><p>از آینه طوطی به دبستان بنشیند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مژگان شمرم بوسه زنم بر کف پایش</p></div>
<div class="m2"><p>در چشمم اگر خار مغیلان بنشیند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن کس که چو یوسف بودش چشم عزیزی</p></div>
<div class="m2"><p>شرط است که یک چند به زندان بنشیند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از طعنه خامان نشود کند طبیعت</p></div>
<div class="m2"><p>کی آتش سوزنده به دامان بنشیند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر خضر ببیند لب جان پرور او را</p></div>
<div class="m2"><p>در ماتم سر چشمه حیوان بنشیند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر کس که چو صائب به تکلف نکند زیست</p></div>
<div class="m2"><p>پیوسته چو گل خرم وخندان بنشیند</p></div></div>