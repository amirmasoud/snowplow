---
title: >-
    غزل شمارهٔ ۵۱۹۳
---
# غزل شمارهٔ ۵۱۹۳

<div class="b" id="bn1"><div class="m1"><p>صبح قیامت بود چاک گریبان عشق</p></div>
<div class="m2"><p>شور دو عالم بود گرد نمکدان عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کورسوادان عقل محو کتابند و لوح</p></div>
<div class="m2"><p>سینه روشن بود لوح دبستان عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر سو مو بر تنش شمع تجلی شود</p></div>
<div class="m2"><p>دررگ هرکس دوید باده سوزان عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاک وجودش شود همسفر گردباد</p></div>
<div class="m2"><p>درقدم هرکه رفت خار بیابان عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون نتواند گرفت گردش خود راعنان ؟</p></div>
<div class="m2"><p>نیست اگر گوی چرخ زخمی چوگان عشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آینه اهل دل نقش نگیرد به خود</p></div>
<div class="m2"><p>فلس ندارد به تن ماهی عمان عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آب شود هرکه دید چهره شرمین حسن</p></div>
<div class="m2"><p>محو شود هرکه یافت چاشنی خوان عشق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از پی رزق اهل عقل گرد جهان می دوند</p></div>
<div class="m2"><p>ازجگر خود بود روزی مهمان عشق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تیغ ستم دل شکافت ناوک غم دیده دوز</p></div>
<div class="m2"><p>کیست که آید دلیر برسر میدان عشق؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ریخت چو برگ خزان ناخن تدبیر را</p></div>
<div class="m2"><p>عقده سر درگم زلف پریشان عشق</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خامه صائب عبث عرض سخن می دهد</p></div>
<div class="m2"><p>پای ملخ راچه قدر پیش سلیمان عشق</p></div></div>