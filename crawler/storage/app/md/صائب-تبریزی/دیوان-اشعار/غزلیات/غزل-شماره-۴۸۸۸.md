---
title: >-
    غزل شمارهٔ ۴۸۸۸
---
# غزل شمارهٔ ۴۸۸۸

<div class="b" id="bn1"><div class="m1"><p>عقل اگر از سرپرد زاغ جگر خواری مباش</p></div>
<div class="m2"><p>مغز اگر بیجا شود آشفته دستاری مباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حلقه تن گر ز سیلاب فناصحرا شود</p></div>
<div class="m2"><p>در سواد اعظم دل چار دیواری مباش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رشته جان گر شود کوته ز مقراض اجل</p></div>
<div class="m2"><p>برمیان جسم کافر کیش زناری مباش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باد هستی از سر بی مغز اگر بیرون رود</p></div>
<div class="m2"><p>یک حباب پوچ در دریای زخاری مباش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازشنیدن گر شود معزول گوش ظاهری</p></div>
<div class="m2"><p>در بساط قلزم و عمان صدف واری مباش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لب اگر خامش شود، یک رخنه غم بسته گیر</p></div>
<div class="m2"><p>چشم اگر پوشیده گردد، داغ خونباری مباش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پای سیر از خواب سنگین اجل گر بشکند</p></div>
<div class="m2"><p>خاکدان دهر را بیهوده رفتاری مباش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چند صائب بردل گم گشته خواهی خون گریست؟</p></div>
<div class="m2"><p>در جگر پیکان زهر آلود خونخواری مباش</p></div></div>