---
title: >-
    غزل شمارهٔ ۳۶۰۸
---
# غزل شمارهٔ ۳۶۰۸

<div class="b" id="bn1"><div class="m1"><p>حسن را حلقه خط مانع رفتن نشود</p></div>
<div class="m2"><p>نور خورشید، نظربند ز روزن نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبرد خنده ظاهر ز دل تنگ ملال</p></div>
<div class="m2"><p>غنچه را دل تهی از خون به شکفتن نشود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باد دستان ز گرانباری زر آزادند</p></div>
<div class="m2"><p>سنگ یک لحظه فزون بار فلاخن نشود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می شود خرده جانها یکی از وصل هزار</p></div>
<div class="m2"><p>آه اگر دانه من واصل خرمن نشود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل روشن ز هوادار نبالد بر خویش</p></div>
<div class="m2"><p>شعله ور آتش یاقوت ز دامن نشود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبح خورشید جهانتاب بود چشم سفید</p></div>
<div class="m2"><p>چشم یعقوب محال است که روشن نشود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چاه خس پوش خطر بیش ز رهزن دارد</p></div>
<div class="m2"><p>دوربین امن ز همواری دشمن نشود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برمیاور سر دعوی ز گریبان غرور</p></div>
<div class="m2"><p>که علم کس به کمال از رگ گردن نشود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سوز دل کم نشد از تیغ شهادت صائب</p></div>
<div class="m2"><p>آتش سنگ خموش از نم آهن نشود</p></div></div>