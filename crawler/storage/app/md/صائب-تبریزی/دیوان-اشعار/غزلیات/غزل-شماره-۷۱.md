---
title: >-
    غزل شمارهٔ ۷۱
---
# غزل شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>از فغان شد سر گرانی بیش آن طناز را</p></div>
<div class="m2"><p>ناله عاشق بود افسانه خواب ناز را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ریاضت دامن مقصود می آید به چنگ</p></div>
<div class="m2"><p>گوشمال آخر شود دست نوازش ساز را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از زبان بازی سخن چین را زبان گردد دراز</p></div>
<div class="m2"><p>می شود مهر دهن، شمع از خموشی گاز را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می گشاید بال شهرت ناله در کنج قفس</p></div>
<div class="m2"><p>می کند خس پوش گلشن شعله آواز را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صفحه ننوشته را از حرف گیران باک نیست</p></div>
<div class="m2"><p>بستن لب می شود مهر دهن غماز را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می زند ناخن به دل هر چند هر سازی که هست</p></div>
<div class="m2"><p>دست دیگر در خراش دل بود آواز را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از نظر بستن ز دنیا، رغبت زاهد فزود</p></div>
<div class="m2"><p>حرص صید از چشم بستن بیش گردد باز را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لفظ نازک، حسن معنی را دو بالا می کند</p></div>
<div class="m2"><p>شیشه شیراز می باید می شیراز را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از خود آرایی سبک پروازی از طاوس رفت</p></div>
<div class="m2"><p>گشت رنگینی حنای بال و پر پرواز را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تیر روی ترکش از خون بیش روزی می خورد</p></div>
<div class="m2"><p>می رسد از چرخ زحمت بیشتر ممتاز را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بوی گل را برگ نتواند ز جولان بازداشت</p></div>
<div class="m2"><p>چون کنم صائب نهان در پرده دل راز را؟</p></div></div>