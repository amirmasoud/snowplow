---
title: >-
    غزل شمارهٔ ۲۸۲۱
---
# غزل شمارهٔ ۲۸۲۱

<div class="b" id="bn1"><div class="m1"><p>مبادا دولت دنیا نصیب بد گهر گردد</p></div>
<div class="m2"><p>که تیغ از آبداری تشنه خون بیشتر گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منه زاندازه بیرون پا، اگر آسودگی خواهی</p></div>
<div class="m2"><p>که خون فاسد چو شد آهن ربای نیشتر گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمک ریزد زچشم شور، شبنم در گریبانش</p></div>
<div class="m2"><p>اگر داغی نصیب لاله خونین جگر گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غبار کلفت از دل ساغر سرشار می شوید</p></div>
<div class="m2"><p>اگر گرد یتیمی شسته از روی گهر گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به عهد ما که آمیزش کدورت بار می آرد</p></div>
<div class="m2"><p>عجب دارم که از پیوند نخلی خوش ثمر گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخن بی پرده می گویند صائب راست گفتاران</p></div>
<div class="m2"><p>که بیجو هر بود تیغی که پنهان در سپر گردد</p></div></div>