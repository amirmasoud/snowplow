---
title: >-
    غزل شمارهٔ ۱۶۹۴
---
# غزل شمارهٔ ۱۶۹۴

<div class="b" id="bn1"><div class="m1"><p>تن آهنین و نفس گرم و دل رمیده خوش است</p></div>
<div class="m2"><p>سپند مضطرب و مجمر آرمیده خوش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صدف پر از گهر و ابر قطره بار نکوست</p></div>
<div class="m2"><p>عذار یار عرقناک و می چکیده خوش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سکون ز مرکز و گردش بجاست از پرگار</p></div>
<div class="m2"><p>پیاله در حرکت، صحبت آرمیده خوش است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پای قافله نتوان شدن فلک پرواز</p></div>
<div class="m2"><p>سفر چو عیسی ازین خاکدان جریده خوش است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکستنی است کلیدی که بستگی آرد</p></div>
<div class="m2"><p>زبان کز او نگشاید دلی، بریده خوش است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تمام سوز نگشت از شتاب، پروانه</p></div>
<div class="m2"><p>سفر در آتش سوزان عنان کشیده خوش است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمان به گوشه ابرو بلند می گوید</p></div>
<div class="m2"><p>که راست خانگی از مدم خمیده خوش است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عطا و منع مساوی است با رضامندی</p></div>
<div class="m2"><p>درین ریاض گل چیده و نچیده خوش است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه عمر پوچ به گفتار می کنی صائب؟</p></div>
<div class="m2"><p>سخن که نیست در او مغز، ناشنیده خوش است</p></div></div>