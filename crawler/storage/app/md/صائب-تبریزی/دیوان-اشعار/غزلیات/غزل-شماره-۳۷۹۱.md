---
title: >-
    غزل شمارهٔ ۳۷۹۱
---
# غزل شمارهٔ ۳۷۹۱

<div class="b" id="bn1"><div class="m1"><p>ز عشق رشته جانی که پیچ و تاب نخورد</p></div>
<div class="m2"><p>ز چشمه گهر شاهوار آب نخورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منم که رنگ ندارم ز روی گلرنگش</p></div>
<div class="m2"><p>وگرنه لعل چه خونها ز آفتاب نخورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کجا به شبنم و گل التفات خواهد کرد؟</p></div>
<div class="m2"><p>ز چهره عرق افشان، دلی که آب نخورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خاک پای تو خون می خورد به رغبت می</p></div>
<div class="m2"><p>همان حریف که در پای گل شراب نخورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درین بهار که یک غنچه ناشکفته نماند</p></div>
<div class="m2"><p>غنیمت است که دستی بر آن نقاب نخورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنان گرفت تکلف بساط عالم را</p></div>
<div class="m2"><p>که خاک تشنه جگر آب بی گلاب نخورد!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تویی که سنگدلی، ورنه هیچ زهره جبین</p></div>
<div class="m2"><p>به هر مکیدن لب خون آفتاب نخورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صبور باش که در انتظار ابر بهار</p></div>
<div class="m2"><p>صدف به تشنه لبی از محیط آب نخورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز خود برآی که در سنگ آتش سوزان</p></div>
<div class="m2"><p>شراب لعل ز خونابه کباب نخورد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زمانه کشتی احسان چنان به خشکی بست</p></div>
<div class="m2"><p>که هیچ تشنه جگر بازی سراب نخورد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ندامت است سرانجام میکشی صائب</p></div>
<div class="m2"><p>خوشا کسی که ازین چشمه سار آب نخورد</p></div></div>