---
title: >-
    غزل شمارهٔ ۴۳۸۷
---
# غزل شمارهٔ ۴۳۸۷

<div class="b" id="bn1"><div class="m1"><p>تیغ تو می وساقی وپیمانه من شد</p></div>
<div class="m2"><p>هر زخم نمایان در میخانه من شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گو شاخ گل آسوده شو از جلوه طرازی</p></div>
<div class="m2"><p>اکنون که ته بال پریخانه من شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از وحشت نخجیر شود دام پریشان</p></div>
<div class="m2"><p>زنجیر سرزلف تو دیوانه من شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از شوخی او زلزله در مغز زمین است</p></div>
<div class="m2"><p>آن خانه برانداز که همخانه من شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیگر نکشد منت خشک از لب دریا</p></div>
<div class="m2"><p>ابری که تر از گریه مستانه من شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آوردم اگر روی به محراب عبادت</p></div>
<div class="m2"><p>از بیخبری گوشه میخانه من شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر خانه چشمی که شبستان جهان داشت</p></div>
<div class="m2"><p>در بسته ز شیرینی افسانه من شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وحشت کند از کلبه ویرانه من سیل</p></div>
<div class="m2"><p>رحم است برآن جغد که همخانه من شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناز تو فزون گشت ز اظهار نیازم</p></div>
<div class="m2"><p>خواب تو گرانسنگ ز افسانه من شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>راضی به قضا باش که کنج قفس تنگ</p></div>
<div class="m2"><p>از ریختن بال پریخانه من شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرسرو به خوبی علم از فاخته گردید</p></div>
<div class="m2"><p>شمع تو جهانسوز ز پروانه من شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در کلبه من گرد علایق نبود فرش</p></div>
<div class="m2"><p>سیلاب تهیدست ز کاشانه من شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بی برگی من از سخن سرد طمع بود</p></div>
<div class="m2"><p>مهری که زدم بر لب خود دانه من شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در دوستی ساخته صائب نبود فیض</p></div>
<div class="m2"><p>ممنونم ازان شمع که بیگانه من شد</p></div></div>