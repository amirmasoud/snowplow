---
title: >-
    غزل شمارهٔ ۴۹۵۷
---
# غزل شمارهٔ ۴۹۵۷

<div class="b" id="bn1"><div class="m1"><p>اگر چه بی نیاز ست از دو عالم ناز تمکینش</p></div>
<div class="m2"><p>چه بیتابانه می چسبد به دل لبهای شیرینش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازان در چشم او عاشق بود از خاک ره کمتر</p></div>
<div class="m2"><p>که قمری می کند نقش قدم را سرو سیمینش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا چون مهرتابان داغ دارد آسمان چشمی</p></div>
<div class="m2"><p>که تابد پنجه الماس رامژگان زرینش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دگر ماه نوی بر سینه من می زند ناخن</p></div>
<div class="m2"><p>که گوهر در صدف پنهان شده است از شرم پروینش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به بوی مشک بتوان صدبیابان رفت دنبالش</p></div>
<div class="m2"><p>ز شوخیها اگر پی گم کند آهوی مشکینش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درین بستان شبی راهر که دارد زنده چون شبنم</p></div>
<div class="m2"><p>چراغ آفتاب آید به پای خود به بالینش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگین را در نگین دان رتبه دیگر بود صائب</p></div>
<div class="m2"><p>اگر باور نداری سیر کن درخانه زینش</p></div></div>