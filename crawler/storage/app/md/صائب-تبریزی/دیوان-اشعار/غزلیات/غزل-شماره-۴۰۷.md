---
title: >-
    غزل شمارهٔ ۴۰۷
---
# غزل شمارهٔ ۴۰۷

<div class="b" id="bn1"><div class="m1"><p>فروغ حسن از خط بیش گردد لاله رویان را</p></div>
<div class="m2"><p>که خاموشی بود کمتر، چراغ زیر دامان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهان در خط سبز آن لعل شکر بار را بنگر</p></div>
<div class="m2"><p>ندیدی زیر بال طوطیان گر شکرستان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به ریزش می توان داغ سیاهی را ز دل شستن</p></div>
<div class="m2"><p>که باشد ابر بی باران، شب آدینه مستان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به جوش سینه من برنیاید مهر خاموشی</p></div>
<div class="m2"><p>حبابی پرده داری چون تواند کرد طوفان را؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حریصان می شوند از دور باش منع مبرم تر</p></div>
<div class="m2"><p>که دندان طمع مسواک سازد چوب دربان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کریم پاک گوهر چشم سایل را کند روشن</p></div>
<div class="m2"><p>صدف با بسته چشمی می شناسد ابر نیسان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگر روی عرقناک ترا دیده است، کز غیرت</p></div>
<div class="m2"><p>ز برگ لاله شبنم بر جگر افشرده دندان را؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سخن های پریشان مرا نشنیدن اولی تر</p></div>
<div class="m2"><p>که باران اشک حسرت باشد این ابر پریشان را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>توان مضمون مکتوب مرا دریافت از عنوان</p></div>
<div class="m2"><p>که چین آستین بر جبهه باشد تنگدستان را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا از قرب شبنم در گلستان شد چنین روشن</p></div>
<div class="m2"><p>که خوبان از هوا گیرند صائب پاک چشمان را</p></div></div>