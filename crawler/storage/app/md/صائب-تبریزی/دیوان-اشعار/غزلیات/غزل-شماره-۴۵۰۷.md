---
title: >-
    غزل شمارهٔ ۴۵۰۷
---
# غزل شمارهٔ ۴۵۰۷

<div class="b" id="bn1"><div class="m1"><p>دل صاف پروای محشر ندارد</p></div>
<div class="m2"><p>که دریاغم از دامن ترندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بساز ای خردمند با تیره بختی</p></div>
<div class="m2"><p>که دریا گزیری ز عنبر ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شود تخته مشق هر خاروخس را</p></div>
<div class="m2"><p>چو دریا بزرگی که لنگر ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شود خشک همچون سبو دست آن کس</p></div>
<div class="m2"><p>که باری ز دوش کسی برندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز طعن خسان پاک گوهر نترسد</p></div>
<div class="m2"><p>رگ لعل پروای نشترندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل روشن از انقلاب است ایمن</p></div>
<div class="m2"><p>ز طوفان خطر آب گوهر ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نخواهد سرگرم دستار صائب</p></div>
<div class="m2"><p>که خورشید حاجت به افسر ندارد</p></div></div>