---
title: >-
    غزل شمارهٔ ۱۷۱۸
---
# غزل شمارهٔ ۱۷۱۸

<div class="b" id="bn1"><div class="m1"><p>زمین ز جلوه قربانیان گلستان است</p></div>
<div class="m2"><p>بریز خون صراحی که عید قربان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غبار هستی خود را بشو به زمزم اشک</p></div>
<div class="m2"><p>که محرم است، ازین جامه هر که عریان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به راه کعبه گل، پای سعی رنجه مکن</p></div>
<div class="m2"><p>که دستگیری مردم هزار چندان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برآ ز عالم گل، باش در حرم دایم</p></div>
<div class="m2"><p>که از طواف، غرض قطع این بیابان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به خصم گل زدن از دست من نمی آید</p></div>
<div class="m2"><p>وگر نه آبله ام تشنه مغیلان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ببند در به رخ آرزو اگر مردی</p></div>
<div class="m2"><p>وگرنه بستن سد سکندر آسان ا ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خط مسلمی از گردش سپهر مجوی</p></div>
<div class="m2"><p>که این پیاله به نوبت مدام گردان است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل رمیده من در میان خلق، بود</p></div>
<div class="m2"><p>سفینه ای که عنانش به دست طوفان است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چگونه فکر اقامت کند درین میدان؟</p></div>
<div class="m2"><p>سری که در خم فرمان هفت چوگان است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مجوی در صدف تن ز جان پاک قرار</p></div>
<div class="m2"><p>که بیقرار بود گوهری که غلطان است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زیاد آن خط مشکین، دل شکسته من</p></div>
<div class="m2"><p>همیشه تازه و تر چون سفال ریحان است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به سیم قلب شدم قانع و ز بیقدری</p></div>
<div class="m2"><p>بهای یوسف من بار بر عزیزان است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تسلی دل بیتاب من به نامه خشک</p></div>
<div class="m2"><p>علاج رعشه دریا به دست مرجان است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چه نسبت است ندانم به زلف یار، مرا</p></div>
<div class="m2"><p>که عالمی ز پریشانیم پریشان است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز دور باش رقیبان نهال قامت تو</p></div>
<div class="m2"><p>گران به دیده مردم چو چوب دربان است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شکستن کمر کوه قاف چندان نیست</p></div>
<div class="m2"><p>به مور هر که مدارا کند سلیمان است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مراست خاتم اقبال از جهان صائب</p></div>
<div class="m2"><p>که مور من طرف حرف با سلیمان است</p></div></div>