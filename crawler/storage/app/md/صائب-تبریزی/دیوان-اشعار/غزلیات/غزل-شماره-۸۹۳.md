---
title: >-
    غزل شمارهٔ ۸۹۳
---
# غزل شمارهٔ ۸۹۳

<div class="b" id="bn1"><div class="m1"><p>روز روشن گل و شمع شب تارست شراب</p></div>
<div class="m2"><p>برگ عیش و طرب لیل و نهارست شراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بود در دل خم، هست فلاطون زمان</p></div>
<div class="m2"><p>محفل آرا چو شود، باغ و بهارست شراب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی عقل است ز سر پنجه تاکش نیلی</p></div>
<div class="m2"><p>با همه شیشه دلی شیر شکارست شراب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نعل بی طاقتی از جام در آتش دارد</p></div>
<div class="m2"><p>بس که مشتاق به لعل لب یارست شراب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر حریمی که در او ساقی تردستی نیست</p></div>
<div class="m2"><p>جام خمیازه خشک است و غبارست شراب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می کند با لب میگون تو می کار نمک</p></div>
<div class="m2"><p>چشم مخمور ترا آب خمارست شراب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هست از روی تو چون برگ خزان دیده خجل</p></div>
<div class="m2"><p>گر چه گلگونه هر لاله عذارست شراب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه حباب است که در ساغر می جلوه گرست</p></div>
<div class="m2"><p>عرق آلود ز شرم لب یارست شراب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گریه تلخ بود حاصل میخواری من</p></div>
<div class="m2"><p>بی تو در دیده من غوره فشارست شراب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نتواند طرف عشق شد از بی جگری</p></div>
<div class="m2"><p>گر چه بر عقل زبردست سوارست شراب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ظلمت غم چو کند تیره جهان را صائب</p></div>
<div class="m2"><p>روشنی بخش دل و جان فگارست شراب</p></div></div>