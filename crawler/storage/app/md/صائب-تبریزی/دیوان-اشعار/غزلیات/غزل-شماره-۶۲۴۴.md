---
title: >-
    غزل شمارهٔ ۶۲۴۴
---
# غزل شمارهٔ ۶۲۴۴

<div class="b" id="bn1"><div class="m1"><p>به خون غلطد چمن از ناله دردآشنای من</p></div>
<div class="m2"><p>قفس پر گل شود از بلبل رنگین نوای من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گران خیزند همراهان بی پروای من، ورنه</p></div>
<div class="m2"><p>ره خوابیده را بیدار می سازد درای من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیم بی مایه تا بر سود باشد از سفر چشمم</p></div>
<div class="m2"><p>مرا این بس که خاری نشکند در زیر پای من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به استغنا توان خو در جگر کردن بخیلان را</p></div>
<div class="m2"><p>فلک را داغ دارد خاطر بی مدعای من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندارد عالم تجرید چون من خانه پردازی</p></div>
<div class="m2"><p>نمی گردد غبارآلود سیلاب از سرای من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا می زیبد از اهل قناعت لاف بی برگی</p></div>
<div class="m2"><p>که از پهلوی خشک خویش باشد بوریای من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز برق تیشه من کوه آهن آب می گردد</p></div>
<div class="m2"><p>چه باشد بیستون در پنجه زورآزمای من؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنان کز جنبش افزاید گرانی مهد طفلان را</p></div>
<div class="m2"><p>به لنگر شد ز طوفان کشتی بی ناخدای من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنان صائب فشاندم آستین بر خواهش دنیا</p></div>
<div class="m2"><p>که همت از در دلها نمی خواهد گدای من</p></div></div>