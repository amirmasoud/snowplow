---
title: >-
    غزل شمارهٔ ۶۲۲۵
---
# غزل شمارهٔ ۶۲۲۵

<div class="b" id="bn1"><div class="m1"><p>به یک زخم نمایان سرفرازم از شهیدان کن</p></div>
<div class="m2"><p>چو صبح وصل خندانم ازین لطف نمایان کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر خواهی که خورشید از گریبانت برون آید</p></div>
<div class="m2"><p>سحرخیزی فن خود همچون صبح پاکدامان کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگاه شور چشمان می برد شیرینی از شکر</p></div>
<div class="m2"><p>لب پر خنده خود را ز چشم غیر پنهان کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به زلف خود مشو مغرور و عالم را مزن بر هم</p></div>
<div class="m2"><p>حذر از ناله زنجیر سوز بیگناهان کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به عزم سیر با اغیار چون در بوستان گردی</p></div>
<div class="m2"><p>چو بینی سنبلی را یاد این خاطر پریشان کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گلت پا در رکاب جلوه باد خزان دارد</p></div>
<div class="m2"><p>برو ای بلبل بی درد آه و ناله سامان کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیال زنده رود از سینه گرد غم برد صائب</p></div>
<div class="m2"><p>چو غم زور آورد بر خاطرت یاد صفاهان کن</p></div></div>