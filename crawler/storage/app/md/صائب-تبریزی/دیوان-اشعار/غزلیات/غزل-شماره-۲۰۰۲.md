---
title: >-
    غزل شمارهٔ ۲۰۰۲
---
# غزل شمارهٔ ۲۰۰۲

<div class="b" id="bn1"><div class="m1"><p>در هر نظاره ام ز تو پیغام تازه ای است</p></div>
<div class="m2"><p>هر گردشی ز چشم توام جام تازه ای است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر روز از لب تو دل تلخکام من</p></div>
<div class="m2"><p>امیدوار بوسه و پیغام تازه ای است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از پختگی اگر چه مرا عشق سوخته است</p></div>
<div class="m2"><p>هر لحظه در دلم هوس خام تازه ای است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر زخم تازه بر دل من یار کهنه ای است</p></div>
<div class="m2"><p>هر داغ کهنه در جگرم جام تازه ای است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با عاشقان مضایقه کردن به حرف تلخ</p></div>
<div class="m2"><p>آگاه نیستی که چه دشنام تازه ای است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چند کهنه تر شود آن یار تازه رو</p></div>
<div class="m2"><p>ما را ازو توقع انعام تازه ای است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای دل حساب خویش به آن زلف پاک کن</p></div>
<div class="m2"><p>کز خط رخش به فکر سرانجام تازه ای است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن را که هست کعبه مقصود در نظر</p></div>
<div class="m2"><p>چشم سفید، جامه احرام تازه ای است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب به دور عارضش از خط مشکبار</p></div>
<div class="m2"><p>بر هر طرف که می نگری دام تازه ای است</p></div></div>