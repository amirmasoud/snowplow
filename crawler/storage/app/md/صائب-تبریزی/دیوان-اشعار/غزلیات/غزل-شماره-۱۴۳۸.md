---
title: >-
    غزل شمارهٔ ۱۴۳۸
---
# غزل شمارهٔ ۱۴۳۸

<div class="b" id="bn1"><div class="m1"><p>زان دم تیغ که از آب بقا سیراب است</p></div>
<div class="m2"><p>آب بردار که صحرای فنا بی آب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیر کنعان نظر از راه نظر بستن یافت</p></div>
<div class="m2"><p>چشم پوشیدن این طایفه فتح الباب است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طوق زنجیر، گریبان سورست مرا</p></div>
<div class="m2"><p>موی چون تیغ زند بر تن من، سنجاب است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا رسیده است به آن موی کمر پیچیده است</p></div>
<div class="m2"><p>رشته جان من و موی کمر همتاب است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ذره ای نیست در آفاق که سرگردان نیست</p></div>
<div class="m2"><p>این محیطی است که هر قطره او گرداب است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اشک در دیده شرابی است که در جام جم است</p></div>
<div class="m2"><p>داغ بر سینه چراغی است که در محراب است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فارغ از دردسر منت تعمیرم ساخت</p></div>
<div class="m2"><p>صندل جبهه ویرانه من سیلاب است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حیف و صد حیف که از آب مروت خالی است</p></div>
<div class="m2"><p>این همه کاسه زرین که بر این دولاب است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواب و بیداری آگاه دلان نیست به چشم</p></div>
<div class="m2"><p>شب این طایفه روزی است که دل در خواب است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا گرفته است ز لب مهر خموشی صائب</p></div>
<div class="m2"><p>گوش این نغمه شناسان، صدف سیماب است</p></div></div>