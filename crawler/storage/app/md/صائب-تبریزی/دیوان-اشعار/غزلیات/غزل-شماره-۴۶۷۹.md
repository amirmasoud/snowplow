---
title: >-
    غزل شمارهٔ ۴۶۷۹
---
# غزل شمارهٔ ۴۶۷۹

<div class="b" id="bn1"><div class="m1"><p>چند روزی چو قلم سربه ته انداخته گیر</p></div>
<div class="m2"><p>ورقی چند به بازیچه سیه ساخته گیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست در عالم ناساز چو امیدثابت</p></div>
<div class="m2"><p>خانه ها درگذر سیل فنا ساخته گیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این گهرها که به جمعیت آن می نازی</p></div>
<div class="m2"><p>آخر الامر چو اشک از نظر انداخته گیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقش هر لحظه به روی دگری می خندد</p></div>
<div class="m2"><p>هرچه بردی ز حریفان دغا، باخته گیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست چون حوصله یک نگه دورترا</p></div>
<div class="m2"><p>پرده از چهره مقصود بر انداخته گیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی مثال است چو رخساره آن جان جهان</p></div>
<div class="m2"><p>از علایق دل چون آینه پرداخته گیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست شایسته افسون، جدایی از خلق</p></div>
<div class="m2"><p>جامه پرشپش از دوش خود انداخته گیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست امید اجابت چو فغان را صائب</p></div>
<div class="m2"><p>علم ناله به افلاک بر افراخته گیر</p></div></div>