---
title: >-
    غزل شمارهٔ ۳۳۷۰
---
# غزل شمارهٔ ۳۳۷۰

<div class="b" id="bn1"><div class="m1"><p>چند دل خون خود از دوری احباب خورد؟</p></div>
<div class="m2"><p>کف خاکی چه قدر سیلی سیلاب خورد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترک آداب بود حاصل هنگامه می</p></div>
<div class="m2"><p>می حرام است بر آن کس که به آداب خورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شود از زخم نمایان جگرش جوهردار</p></div>
<div class="m2"><p>هرکه از چشمه تیغ تو دمی آب خورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا بود دل بصفا، نفس مکدر باشد</p></div>
<div class="m2"><p>دزد بیدل جگر خویش به مهتاب خورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیقراری ز رگ جان حریصان نرود</p></div>
<div class="m2"><p>بر سر گنج بود مار و همان تاب خورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فتنه در سایه آن زلف سیه در خواب است</p></div>
<div class="m2"><p>آه اگر باد بر آن زلف سیه تاب خورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکه چون شبنم گل صاف کند مشرب خویش</p></div>
<div class="m2"><p>آب از چشمه خورشید جهانتاب خورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روزی بی دهنان می رسد از عالم غیب</p></div>
<div class="m2"><p>کوزه سر بسته چو گردید می ناب خورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه کند با جگر تشنه صائب دریا؟</p></div>
<div class="m2"><p>ریگ از چشمه سوزن چه قدر آب خورد؟</p></div></div>