---
title: >-
    غزل شمارهٔ ۴۴۰۰
---
# غزل شمارهٔ ۴۴۰۰

<div class="b" id="bn1"><div class="m1"><p>آن آفت جان بر سر انصاف نیامد</p></div>
<div class="m2"><p>آن تلخ زبان بر سر انصاف نیامد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مور خط ازان تنگ شکر گرد برآورد</p></div>
<div class="m2"><p>آن مور میان بر سر انصاف نیامد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چیدند وکشیدند گلاب از گل کاغذ</p></div>
<div class="m2"><p>آن غنچه دهان بر سر انصاف نیامد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کردم به فسون نرم کمان مه نو را</p></div>
<div class="m2"><p>آن سخت کمان بر سر انصاف نیامد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چند که از خرمن ما دود بر آمد</p></div>
<div class="m2"><p>آن برق عنان بر سر انصاف نیامد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دین و دل و عقل و خرد و هوش فشاندم</p></div>
<div class="m2"><p>آن دشمن جان بر سر انصاف نیامد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک عمر خضر در قدم او گذراندم</p></div>
<div class="m2"><p>آن سرو روان بر سر انصاف نیامد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتیم که انصاف دهد چرخ پس از مرگ</p></div>
<div class="m2"><p>مردیم وهمان بر سر انصاف نیامد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر چند که صد عمر خضر دید به رویش</p></div>
<div class="m2"><p>چشم نگران بر سر انصاف نیامد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر چند که صائب زنی کلک شکر ریخت</p></div>
<div class="m2"><p>آن پسته دهان بر سر انصاف نیامد</p></div></div>