---
title: >-
    غزل شمارهٔ ۱۳۵۸
---
# غزل شمارهٔ ۱۳۵۸

<div class="b" id="bn1"><div class="m1"><p>از سر خاک شهیدان یار خوش سنگین گذشت</p></div>
<div class="m2"><p>از محیط آتشین نتوان به این تمکین گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشک می جوشد به جای خون ز ناف لاله زار</p></div>
<div class="m2"><p>تا ازین صحرا کدامین آهوی مشکین گذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دورباشی نیست حاجت حسن شرم آلود را</p></div>
<div class="m2"><p>بارها دست تهی زین گلستان گلچین گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من کیم تا شمع باشد بر سر بالین من؟</p></div>
<div class="m2"><p>شعله سرگرمیم یک نیزه از بالین گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرگ عاشق تلختر از کام زهرآشام اوست</p></div>
<div class="m2"><p>از هلاک کوهکن یارب چه بر شیرین گذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صحبت ما با رخ او، چون نسیم و لاله زار</p></div>
<div class="m2"><p>گر چه زود آمد به سر، بی چشم بد رنگین گذشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آه حسرت در دلم چون سبزه زیر سنگ ماند</p></div>
<div class="m2"><p>بس که از من آن سراپا ناز با تمکین گذشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عصمت یوسف حصار کاروانی می شود</p></div>
<div class="m2"><p>دید تا روی ترا از خون گل گلچین گذشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رتبه گفتار را حیرت تلافی می کند</p></div>
<div class="m2"><p>چاره خاموشی است شعری را که از تحسین گذشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دوستی و دشمنی بسا خلق صائب آفتاب است</p></div>
<div class="m2"><p>از جدل آسوده شد هر کس ز مهر و کین گذشت</p></div></div>