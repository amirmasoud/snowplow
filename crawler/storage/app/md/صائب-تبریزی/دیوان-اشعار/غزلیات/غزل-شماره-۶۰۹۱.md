---
title: >-
    غزل شمارهٔ ۶۰۹۱
---
# غزل شمارهٔ ۶۰۹۱

<div class="b" id="bn1"><div class="m1"><p>چون دو تا شد قدت از پیری گرانجانی مکن</p></div>
<div class="m2"><p>بیش ازین استادگی با اسب چوگانی مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش دریا قطره را نعل سفر در آتش است</p></div>
<div class="m2"><p>در گهر این قطره را زین بیش زندانی مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو اوراق خزان اسباب دنیا رفتنی است</p></div>
<div class="m2"><p>خواب را بر چشم خود تلخ از نگهبانی مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر شب خود را نسازی از دل بیدار روز</p></div>
<div class="m2"><p>روز را چون شب ز خواب روز ظلمانی مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صورت دیوار می سازد ترا تن پروری</p></div>
<div class="m2"><p>تکیه از غفلت به دیوار تن آسانی مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرغ زیرک دام را در دانه می بیند عیان</p></div>
<div class="m2"><p>در حضور موشکافان سبحه گردانی مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گریه را باشد اثرهای نمایان در سحر</p></div>
<div class="m2"><p>با زمین پاک بخل از دانه افشانی مکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زیر گردون ماهرویی نیست بی داغ کلف</p></div>
<div class="m2"><p>پیش این ناشسته رویان مشق حیرانی مکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تیزتر گردد ز سوهان تیغ های بی امان</p></div>
<div class="m2"><p>بی سبب در کار مبرم چین پیشانی مکن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پاس دار از شور چشمان سنبل فردوس را</p></div>
<div class="m2"><p>در میان جمع، اظهار پریشانی مکن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حرف حق با باطلان گفتن ندارد حاصلی</p></div>
<div class="m2"><p>در زمین شور صائب دانه افشانی مکن</p></div></div>