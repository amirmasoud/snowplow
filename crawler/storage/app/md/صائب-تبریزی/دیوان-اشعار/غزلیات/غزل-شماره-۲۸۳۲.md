---
title: >-
    غزل شمارهٔ ۲۸۳۲
---
# غزل شمارهٔ ۲۸۳۲

<div class="b" id="bn1"><div class="m1"><p>دل سنگ از شکست دانه من آب می‌گردد</p></div>
<div class="m2"><p>ز عاجزنالی من آسیا گرداب می‌گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زبال افشانی پروانه می‌ریزم ز یکدیگر</p></div>
<div class="m2"><p>سرشک شمع در ویرانه‌ام سیلاب می‌گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلال جویبار تیغ او خاصیتی دارد</p></div>
<div class="m2"><p>که هرکس می‌گذارد سر در او سیراب می‌گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سهی سروی که من چون سایه می‌گردم به دنبالش</p></div>
<div class="m2"><p>زمین چون آسمان از جلوه‌اش بی‌تاب می‌گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به آن موی میان از پیچ و تاب امیدها دارم</p></div>
<div class="m2"><p>که می‌گردد یکی چون رشته‌ها همتاب می‌گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مپیچ از خاکساری سر، که هرکس از سر رغبت</p></div>
<div class="m2"><p>به این دیوار پشت خود دهد محراب می‌گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز نومیدی گل امید آب و رنگ می‌گیرد</p></div>
<div class="m2"><p>که از لب‌تشنگی تبخاله‌ها سیراب می‌گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به این سامان نخواهد ماند دایم چرخ دولابی</p></div>
<div class="m2"><p>شود ویران دکان هرکه از دولاب می‌گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منم آن ماهی حیران درین دریای بی‌پایان</p></div>
<div class="m2"><p>که از خشکی نفس در کام من قلاب می‌گردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ندارد هیچ کس چون ابر آیین سخاوت را</p></div>
<div class="m2"><p>که گوهر می‌فشاند و ز خجالت آب می‌گردد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به بی‌برگی قناعت با دل بیدار کن صائب</p></div>
<div class="m2"><p>که اسباب فراغت پرده‌های خواب می‌گردد</p></div></div>