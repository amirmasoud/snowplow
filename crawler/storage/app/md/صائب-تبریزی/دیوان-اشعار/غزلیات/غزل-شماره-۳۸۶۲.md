---
title: >-
    غزل شمارهٔ ۳۸۶۲
---
# غزل شمارهٔ ۳۸۶۲

<div class="b" id="bn1"><div class="m1"><p>دل گشاده من گلستان من باشد</p></div>
<div class="m2"><p>سراب من نفس خونچکان من باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به شرح حال به حرف احتیاج نیست مرا</p></div>
<div class="m2"><p>که بوی سوختگی ترجمان من باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لب سؤال به آب حیات تر نکنم</p></div>
<div class="m2"><p>اگر عقیق لبت در دهان من باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به بال کاغذی از بحرآتشین گذرم</p></div>
<div class="m2"><p>حمایت تو اگر پاسبان من باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیم زشیشه دلان کز عتاب اندیشم</p></div>
<div class="m2"><p>که حرف سخت تو رطل گران من باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زنارسایی طالع به خاک می افتد</p></div>
<div class="m2"><p>اگر خدنگ قضا در کمان من باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو زلف سایه بالش فتد شکسته به خاک</p></div>
<div class="m2"><p>اگر غذای هما استخوان من باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز بوی گل نفسم گلستان شود صائب</p></div>
<div class="m2"><p>اگر نسیم سحر مهربان من باشد</p></div></div>