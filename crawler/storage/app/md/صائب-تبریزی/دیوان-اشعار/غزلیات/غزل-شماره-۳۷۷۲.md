---
title: >-
    غزل شمارهٔ ۳۷۷۲
---
# غزل شمارهٔ ۳۷۷۲

<div class="b" id="bn1"><div class="m1"><p>سبکروی که ز سرپا نمی تواند کرد</p></div>
<div class="m2"><p>سفر چو قطره به دریا نمی تواند کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بس که منفعل از کرده های خویشتن است</p></div>
<div class="m2"><p>فلک نگاه به بالا نمی تواند کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی که سیر پریخانه قناعت کرد</p></div>
<div class="m2"><p>نظر به شاهد دنیا نمی تواند کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی که در دل ما جای خویش وا نکند</p></div>
<div class="m2"><p>دگر به هیچ دلی جا نمی تواند کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان ز ناله بلبل فضای باغ پرست</p></div>
<div class="m2"><p>که غنچه بند قبا وا نمی تواند کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به کام هرکه کشیدند شهد خاموشی</p></div>
<div class="m2"><p>لب از حلاوت آن وا نمی تواند کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مسیح اگرچه کند زنده مرده را صائب</p></div>
<div class="m2"><p>علاج درد دل ما نمی تواند کرد</p></div></div>