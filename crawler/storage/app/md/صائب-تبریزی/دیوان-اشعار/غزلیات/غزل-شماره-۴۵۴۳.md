---
title: >-
    غزل شمارهٔ ۴۵۴۳
---
# غزل شمارهٔ ۴۵۴۳

<div class="b" id="bn1"><div class="m1"><p>خال او یک نظر از دیده ما دور نباشد</p></div>
<div class="m2"><p>دانه سوخته اینجاست که بی مور نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زند آتش به جهان بلبل آتش نفس من</p></div>
<div class="m2"><p>اگرم چون قفس از شجر طور نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو نگاهت ز پریشان نظری نیست به یک کس</p></div>
<div class="m2"><p>چون زید عاشق بیچاره اگر کور نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سخن حق نکند گوش کس امروز وگرنه</p></div>
<div class="m2"><p>هیچ کس نیست که در پله منصور نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر گه از فیض هواقد بکشد سبزه مینا</p></div>
<div class="m2"><p>پنبه شیشه گل ابر شود دور نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خون دل نغز شرابی است اگر کام نسوزد</p></div>
<div class="m2"><p>خوش کبابی است کباب دل اگر شور نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دمبدم تشنه دیدار کند ساده رخان را</p></div>
<div class="m2"><p>آب آیینه عجب دارم اگر شور نباشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه رسایی است که با طبع تو آمیخته صائب</p></div>
<div class="m2"><p>مصرعی نیست ز دیوان تو مشهور نباشد</p></div></div>