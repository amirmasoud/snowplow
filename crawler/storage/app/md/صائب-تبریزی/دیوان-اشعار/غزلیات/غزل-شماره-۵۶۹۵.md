---
title: >-
    غزل شمارهٔ ۵۶۹۵
---
# غزل شمارهٔ ۵۶۹۵

<div class="b" id="bn1"><div class="m1"><p>به حرف و صوت ز بوس و کنار ساخته ام</p></div>
<div class="m2"><p>به بوی گل چو نسیم از بهار ساخته ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>توقع خوشی دیگر از جهانم نیست</p></div>
<div class="m2"><p>به وقت خوش من ازین روزگار ساخته ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیم چو شبنم گستاخ بار خاطر گل</p></div>
<div class="m2"><p>به خار خاری از آن گلعذار ساخته ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پای خم نبرم دردسر چون بی ظرفان</p></div>
<div class="m2"><p>ز خون دل به می بی خمار ساخته ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به آبروی خود از عقد گوهرم قانع</p></div>
<div class="m2"><p>ز بحر من به همین چشمه سار ساخته ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نظر سیاه نسازم به مرهم دگران</p></div>
<div class="m2"><p>چو لاله با جگر داغدار ساخته ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به من دورویی مردم چه می تواند کرد</p></div>
<div class="m2"><p>که با دو رنگی لیل و نهار ساخته ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو کودکان به تماشا زعبرتم قانع</p></div>
<div class="m2"><p>به رشته از گهر شاهوار ساخته ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به من ز طالع ناساز غم نمی سازد</p></div>
<div class="m2"><p>وگرنه من به غم از غمگسار ساخته ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مگر شود دل روشن ز جسم تیره خلاص</p></div>
<div class="m2"><p>چو شمع با مژه اشکبار ساخته ام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به راه سیل درین خاکدان ز همواری</p></div>
<div class="m2"><p>بنای هستی خود پایدار ساخته ام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به خون ز نعمت الوان عالمم قانع</p></div>
<div class="m2"><p>چو نافه با نفس مشکبار ساخته ام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شود خموش ز تردامنان ستاره من</p></div>
<div class="m2"><p>از آن به سوختگان چون شرار ساخته ام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به پای گهر من چرا ننازد بحر</p></div>
<div class="m2"><p>که قطره را گهر شاهوار ساخته ام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تهی زسنگ ملامت نمی کنم پهلو</p></div>
<div class="m2"><p>چو کبک مست به این کوهسار ساخته ام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز ممسکی فلک از من دریغ داشته است</p></div>
<div class="m2"><p>به زخم خار اگر از خارزار ساخته ام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از آن به روی زمین بار نیست سایه من</p></div>
<div class="m2"><p>که من به دست تهی چون چنار ساخته ام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز بوسه صلح به پیغام کرده ام صائب</p></div>
<div class="m2"><p>به حرف از آن لب شکر نثار ساخته ام</p></div></div>