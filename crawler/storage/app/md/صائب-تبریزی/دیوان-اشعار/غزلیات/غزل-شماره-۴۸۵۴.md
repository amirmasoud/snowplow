---
title: >-
    غزل شمارهٔ ۴۸۵۴
---
# غزل شمارهٔ ۴۸۵۴

<div class="b" id="bn1"><div class="m1"><p>حیف است که سر در سر مینانکند کس</p></div>
<div class="m2"><p>با دختر رزعیش دوبالا نکند کس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان پیش که در خاک رود، قطره خود را</p></div>
<div class="m2"><p>حیف است که پیوسته به دریا نکند کس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در گرگ نبیند اثر جلوه یوسف</p></div>
<div class="m2"><p>تا آینه خویش مصفا نکند کس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیوانه درین شهر گران است به سنگی</p></div>
<div class="m2"><p>چون سیل چرا روی به صحرا نکند کس؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در چشم کند خانه، مگس را چو دهی روی</p></div>
<div class="m2"><p>با سفله همان به که مدارا نکند کس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حال دل صائب ز جبین روشن و پیداست</p></div>
<div class="m2"><p>این آینه ای نیست که رسوا نکند کس</p></div></div>