---
title: >-
    غزل شمارهٔ ۲۴۰۳
---
# غزل شمارهٔ ۲۴۰۳

<div class="b" id="bn1"><div class="m1"><p>مست ما امروز نقش تازه ای بر آب زد</p></div>
<div class="m2"><p>شیشه می را به طاق ابروی محراب زد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بر آرم سر میان خاک و خون غلتیدگان؟</p></div>
<div class="m2"><p>بال من سیلی به روی خنجر قصاب زد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبح بیداری ندارد در پی این خواب گران</p></div>
<div class="m2"><p>ورنه طوفان بارها بر روی بختم آب زد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون صدف در دامن خود گوهر مقصود یافت</p></div>
<div class="m2"><p>هر که گرد خویش دوری چند چون گرداب زد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خضر و سیر ظلمت و آب حیات افسانه است</p></div>
<div class="m2"><p>تازه شد هر کس شراب کهنه در مهتاب زد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست راه خار در پیراهن عریان تنی</p></div>
<div class="m2"><p>شعله آفت سر از خاکستر سنجاب زد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شعله خوی تو دست آورد بیرون ز آستین</p></div>
<div class="m2"><p>سیلی بیطاقتی بر چهره سیماب زد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب از بس ساده لوحی بر خیال عارضش</p></div>
<div class="m2"><p>بوسه‌ها از دور امشب بر رخ مهتاب زد</p></div></div>