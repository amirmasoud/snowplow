---
title: >-
    غزل شمارهٔ ۱۴۶۵
---
# غزل شمارهٔ ۱۴۶۵

<div class="b" id="bn1"><div class="m1"><p>عشق هر چند که در پرده بود مشهورست</p></div>
<div class="m2"><p>حسن هر چند که بی پرده بود مستورست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که از چاه زنخدان تو سالم گذرد</p></div>
<div class="m2"><p>گر بود صاحب صد دیده روشن، کورست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود از زخم زبان خار بیابان جنون</p></div>
<div class="m2"><p>نمک مایده عشق ز چشم شورست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جگر دیدن عیب و هنر خویش کراست؟</p></div>
<div class="m2"><p>زشت اگر پشت به آیینه کند معذورست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می دهد قطره و سیلاب عوض می گیرد</p></div>
<div class="m2"><p>شهرت بحر به همت غلط مشهورست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به سخن دعوی حق را نتوان برد از پیش</p></div>
<div class="m2"><p>هر که سر در سر این کار کند منصورست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سپری زود شود زندگی تن پرور</p></div>
<div class="m2"><p>زودتر پاره کند زه چو کمان پرزورست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حسن از دیدن آیینه نمی گردد سیر</p></div>
<div class="m2"><p>آب سرچشمه آیینه همانا شورست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یک کف خاک ز بیداد فلک بی خون نیست</p></div>
<div class="m2"><p>گر شکافند جگرگاه زمین یک گورست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سیری از شور سخن نیست دل صائب را</p></div>
<div class="m2"><p>تشنگی بیش کند آب چو تلخ و شورست</p></div></div>