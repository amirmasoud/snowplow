---
title: >-
    غزل شمارهٔ ۴۹۴۳
---
# غزل شمارهٔ ۴۹۴۳

<div class="b" id="bn1"><div class="m1"><p>چسان دل رانگه دارد کسی از چشم قتالش ؟</p></div>
<div class="m2"><p>که گیراتر ز شاهین است مژگان سبکبالش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگردد چون نگاه خلق حیران خط و خالش؟</p></div>
<div class="m2"><p>که گیراتر بوداز خون ناحق چهره آلش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازان آن چهره زیباست از عین الکمال ایمن</p></div>
<div class="m2"><p>که برآتش سپند خانه زادی دارداز خالش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همان درخواب خون خلق راچون آب می نوشد</p></div>
<div class="m2"><p>به خون مردمان تشنه است از بس چشم قتالش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز حسن بی مثالی دارم امید هم آغوشی</p></div>
<div class="m2"><p>که نگرفته است در آغوش خود آیینه تمثالش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندیده است از غرور حسن هرگزسایه خودرا</p></div>
<div class="m2"><p>ندارد رحم برخود هر که می افتد به دنبالش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرا پروانه ای ممنون شمع انجمن گردد</p></div>
<div class="m2"><p>که آتش می جهد چون سنگ و آهن از پرو بالش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا آیینه رویی همچو پرتو مضطرب دارد</p></div>
<div class="m2"><p>که از شوخی نبندد نقش درآیینه تمثالش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زرویش چون نگه دارم نگاه طفل مشرب را؟</p></div>
<div class="m2"><p>که صد دام تماشاهست در هر دانه خالش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نمی دانم کجا می خورده است آن شوخ بی پروا</p></div>
<div class="m2"><p>که شرم آلود می ریزد عرق ازچهره آلش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>(نمی آید به حال ز تدبیر مسیحا هم</p></div>
<div class="m2"><p>کسی کز گردش چشمی دگرگون گشت احوالش )</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ندارد زهره گفتار صائب درقیامت هم</p></div>
<div class="m2"><p>نظر بازی که می سازد شکوه حسن اولالش</p></div></div>