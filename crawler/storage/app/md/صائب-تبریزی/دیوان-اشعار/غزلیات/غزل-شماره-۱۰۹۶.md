---
title: >-
    غزل شمارهٔ ۱۰۹۶
---
# غزل شمارهٔ ۱۰۹۶

<div class="b" id="bn1"><div class="m1"><p>چرخ را خون شفق در دل ز استغنای اوست</p></div>
<div class="m2"><p>رنگ زرد آفتاب از آتش سودای اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از علم غافل نگردد لشکری در کارزار</p></div>
<div class="m2"><p>فتنه روی زمین را چشم بر بالای اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن که کوه صبر ما را سر به صحرا داده است</p></div>
<div class="m2"><p>کوه طور از وحشیان دامن صحرای اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آرزو در دل، نگه در چشم سوزد خلق را</p></div>
<div class="m2"><p>از حیا نوری که در آیینه سیمای اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هست دیوان قیامت را اگر بسم اللهی</p></div>
<div class="m2"><p>پیش ارباب بصیرت، قامت رعنای اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن که ما را سر به صحرا داده چون موج سراب</p></div>
<div class="m2"><p>در لباس شبروان آب خضر جویای ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق هیهات است گردد جمع صائب با خرد</p></div>
<div class="m2"><p>هر سری کز عقل خالی شد پر از سودای اوست</p></div></div>