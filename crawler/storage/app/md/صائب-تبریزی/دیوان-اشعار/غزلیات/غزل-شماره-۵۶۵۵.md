---
title: >-
    غزل شمارهٔ ۵۶۵۵
---
# غزل شمارهٔ ۵۶۵۵

<div class="b" id="bn1"><div class="m1"><p>با دل تشنه و سوز جگر خود چه کنم؟</p></div>
<div class="m2"><p>در صدف آب نسازم گهر خود چه کنم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرهم امروز تویی داغ جگرریشان را</p></div>
<div class="m2"><p>ننمایم به تو داغ جگر خود چه کنم؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صندل امروز تویی دردسر عالم را</p></div>
<div class="m2"><p>پیش عیسی نبرم دردسر خود، چه کنم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من که از دوری منزل نفسم سوخته است</p></div>
<div class="m2"><p>با درازی شب بی سحر خود چه کنم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون خریدار گلوسوز درین عالم نیست</p></div>
<div class="m2"><p>ندهم طرح به موران شکر خود چه کنم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خانه تنگ جهان جای پرافشانی نیست</p></div>
<div class="m2"><p>گر بر آتش ننهم بال و پر خود چه کنم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست از سوخته جانان اثری چون پیدا</p></div>
<div class="m2"><p>در دل سنگ ندزدم شرر خود چه کنم؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>(من که سر رشته تدبیر ز دستم رفته است</p></div>
<div class="m2"><p>نکنم خاک زمین را به سر خود، چه کنم؟)</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هیچ کس را خبری نیست چو از خود صائب</p></div>
<div class="m2"><p>من عاجز ز که پرسم خبر خود، چه کنم</p></div></div>