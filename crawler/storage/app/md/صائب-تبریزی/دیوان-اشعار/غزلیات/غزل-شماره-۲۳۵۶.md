---
title: >-
    غزل شمارهٔ ۲۳۵۶
---
# غزل شمارهٔ ۲۳۵۶

<div class="b" id="bn1"><div class="m1"><p>هر که رو زین خلق ناهموار در دیوار کرد</p></div>
<div class="m2"><p>سنگلاخ دهر را بر خویشتن هموار کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیره چشمان را اگر محجوب سازد دور نیست</p></div>
<div class="m2"><p>روی شرم آلود او آیینه را ستار کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از تراشیدن غبار لشکر خط شد بلند</p></div>
<div class="m2"><p>آب تیغ این سبزه خوابیده را بیداد کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لشکر سنگین غفلت بیجگر افتاده است</p></div>
<div class="m2"><p>مشت آبی می تواند خفته را بیدار کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرد کلفت بس که از دوران به روی من نشست</p></div>
<div class="m2"><p>هر که رو آورد در من، روی در دیوار کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چین زدن بر جبهه ای آیینه رو انصاف نیست</p></div>
<div class="m2"><p>تنگ بر طوطی نباید عرصه گفتار کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نمی گشتند زاغان سرمه آواز ما</p></div>
<div class="m2"><p>می توانستیم فریادی درین گلزار کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بس که در تمثال شیرین برد تردستی به کار</p></div>
<div class="m2"><p>در دل آیینه خون فرهاد شیرین کار کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روی گرم کارفرما گر هواداری کند</p></div>
<div class="m2"><p>می توانم بیستون را زر دست افشار کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اعتبار ناقص از بی اعتباری بدترست</p></div>
<div class="m2"><p>قیمت نازل به یوسف چاه را هموار کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گوشه گیری کشتی نوح است طوفان دیده را</p></div>
<div class="m2"><p>ذوق تنهایی ز صحبتها مرا بیزار کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همچو بخت سبز گیرد از هوا زنگار را</p></div>
<div class="m2"><p>از نمد آیینه ام تا روی در بازار کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عمر خود کوتاه کرد و نامه خود را سیاه</p></div>
<div class="m2"><p>هر که صائب چون قلم سر درسر گفتار کرد</p></div></div>