---
title: >-
    غزل شمارهٔ ۳۵۹۵
---
# غزل شمارهٔ ۳۵۹۵

<div class="b" id="bn1"><div class="m1"><p>باده در شیشه و پیمانه من سنگ شود</p></div>
<div class="m2"><p>سبزی بخت بر آیینه من زنگ شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا فشاندم به جهان دست سبکبار شدم</p></div>
<div class="m2"><p>شود آسوده فلاخن چو سبک سنگ شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر زمین می زندش سنگدلیهای فلک</p></div>
<div class="m2"><p>ساز هرکس که درین دایره آهنگ شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوه تمکین تو در پله نازست تمام</p></div>
<div class="m2"><p>این نه سنگی است که محتاج به پاسنگ شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بوی گل در گره غنچه کند خود را جمع</p></div>
<div class="m2"><p>غم محال است که بیرون ز دل تنگ شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عمر را کوه غم و درد سبک جولان کرد</p></div>
<div class="m2"><p>می رود تند چو سیلاب گرانسنگ شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکه را تنگ شود خلق ز سودا صائب</p></div>
<div class="m2"><p>چرخ و انجم به نظر دامن پرسنگ شود</p></div></div>