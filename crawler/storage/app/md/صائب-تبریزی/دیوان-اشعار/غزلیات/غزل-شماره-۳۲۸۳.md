---
title: >-
    غزل شمارهٔ ۳۲۸۳
---
# غزل شمارهٔ ۳۲۸۳

<div class="b" id="bn1"><div class="m1"><p>دیدنت باعث سرسبزی جان می‌گردد</p></div>
<div class="m2"><p>پیر در سایه سرو تو جوان می‌گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده مگشا به تماشا که درین عبرتگاه</p></div>
<div class="m2"><p>هرکه پوشد نظر از دیده‌وران می‌گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در سبک‌مغز ندارد سخن سخت اثر</p></div>
<div class="m2"><p>که فلاخن سبک از سنگ گران می‌گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر مدار از لب خود مهر خموشی زنهار</p></div>
<div class="m2"><p>کاین سپر مانع شمشیر زبان می‌گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بدان فیض محال است به نیکان نرسد</p></div>
<div class="m2"><p>تیر کج باعث آرام نشان می‌گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عالم از جلوه مستانه او شد ویران</p></div>
<div class="m2"><p>آب از قوت سرچشمه روان می‌گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب از دور محال است که افتد جامش</p></div>
<div class="m2"><p>هرکه در میکده از دُردکشان می‌گردد</p></div></div>