---
title: >-
    غزل شمارهٔ ۵۳۲۸
---
# غزل شمارهٔ ۵۳۲۸

<div class="b" id="bn1"><div class="m1"><p>از هوای تر بر افروزد چراغ عشرتم</p></div>
<div class="m2"><p>رشته باران بود شیرازه جمعیتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست جز مهر خموشی حلقه ای بر در مرا</p></div>
<div class="m2"><p>می خورد بر یکدیگر از چشم گویاخلوتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از کمال بی دماغی صحبت ارباب حال</p></div>
<div class="m2"><p>خانه زنبور می آید به چشم وحشتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تلخ دارد عیش بر کنج دهان گلرخان</p></div>
<div class="m2"><p>از شکر شیرینی بسیار کنج عزلتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آبروی من چو گوهر سر به مهر عزت است</p></div>
<div class="m2"><p>روزه از حرف طمع دارد زبان حاجتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مستی و دیوانگی می ریزد از رفتار من</p></div>
<div class="m2"><p>نقش پا رطل گران می گردد از کیفیتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از تماشای شکار جرگه دارد بی نیاز</p></div>
<div class="m2"><p>شادی جمعیت دل در کمند وحدتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می گذارم گر چه چون خورشید پهلو بر زمین</p></div>
<div class="m2"><p>آسمان را داغ دارد از بلندی همتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حرف حق را برزبان می آوردم منصوروار</p></div>
<div class="m2"><p>تیغ می مالد زبان بر خاک پیش جراتم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با همه بی حاصلی دارم دل آزاده ای</p></div>
<div class="m2"><p>برنیاید از بغل چون سرودست حاجتم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همچو عنقا سعی درگمنامی خود می کنم</p></div>
<div class="m2"><p>نیست صائب چون نواسنجان تلاش شهرتم</p></div></div>