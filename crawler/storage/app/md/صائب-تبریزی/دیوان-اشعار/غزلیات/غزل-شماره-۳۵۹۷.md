---
title: >-
    غزل شمارهٔ ۳۵۹۷
---
# غزل شمارهٔ ۳۵۹۷

<div class="b" id="bn1"><div class="m1"><p>باده کو تا به من آن تلخ زبان رام شود؟</p></div>
<div class="m2"><p>تلخی می نمک تلخی بادام شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوسه در ذائقه اش باده لب شیرین است</p></div>
<div class="m2"><p>تلخکامی که بدآموز به دشنام شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رهنوردان ترا مرگ نگیرد دامن</p></div>
<div class="m2"><p>بر شهید تو کفن جامه احرام شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لب جام از هوس بوسه دهن غنچه کند</p></div>
<div class="m2"><p>چون ز می صفحه رخسار تو گلفام شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موج گرداب نیم، گردش پرگار نیم</p></div>
<div class="m2"><p>تا به کی نقطه آغاز من انجام شود؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شوق دریاکش و در شیشه کم ظرف فلک</p></div>
<div class="m2"><p>آنقدر خون جگر نیست که یک جام شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا در آن زلف توان رفت سراسر صائب</p></div>
<div class="m2"><p>دل رم کرده چه افتاده به من رام شود؟</p></div></div>