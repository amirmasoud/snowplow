---
title: >-
    غزل شمارهٔ ۶۱۱۰
---
# غزل شمارهٔ ۶۱۱۰

<div class="b" id="bn1"><div class="m1"><p>چند آواز تو از بیرون رباید هوش من؟</p></div>
<div class="m2"><p>ره نیابد دردرون چون حلقه در گوش من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در میان سرو، قمری دست خود را حلقه کرد</p></div>
<div class="m2"><p>چند باشد حلقه بیرون در آغوش من؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون شراب کهنه ام آسوده در مینا، ولی</p></div>
<div class="m2"><p>می نماید خویش را در کاسه سر، جوش من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوه را از بردباری گر چه بر سر می نهم</p></div>
<div class="m2"><p>سایه دست نوازش برنتابد دوش من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دشمنان را می شود از هیبت من دل دو نیم</p></div>
<div class="m2"><p>جوهر تیغ دو دم دارد لب خاموش من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیش می گردد جنون من ز سنگ کودکان</p></div>
<div class="m2"><p>نیستم بحری که از لنگر نشیند جوش من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون شکرخندی دهد رو، می شوم صائب غمین</p></div>
<div class="m2"><p>نیش چون زنبور در دنبال دارد نوش من</p></div></div>