---
title: >-
    غزل شمارهٔ ۷۰۵
---
# غزل شمارهٔ ۷۰۵

<div class="b" id="bn1"><div class="m1"><p>مستی ز خط زیاده شد آن دلنواز را</p></div>
<div class="m2"><p>خط صبح نوبهار بود خواب ناز را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیگر عنان دل نتواند نگاه داشت</p></div>
<div class="m2"><p>در جلوه هر که بنگرد آن سرو ناز را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با قهرمان عشق چه سازد غرور عقل؟</p></div>
<div class="m2"><p>از کبک مست نیست حذر شاهباز را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشاق را ز فقر مترسان که سادگی است</p></div>
<div class="m2"><p>نقش مراد، آینه پاکباز را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برهند اگر چه دولت محمود دست یافت</p></div>
<div class="m2"><p>گردن نهاد حلقه زلف ایاز را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از کار می روند به یکبار عاشقان</p></div>
<div class="m2"><p>موسم یکی است قافله های حجاز را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آتشند سوختگان، تا بریده اند</p></div>
<div class="m2"><p>بر قد شمع جامه سوز و گداز را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ترسم که شیوه های هوس آفرین تو</p></div>
<div class="m2"><p>سازد نیازمند دل بی نیاز را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر کن حدیث زلف که مقراض کوتهی است</p></div>
<div class="m2"><p>این خوش فسانه ها ره دور و دراز را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لب می خورد ز پاس زبان خون خود مدام</p></div>
<div class="m2"><p>ز اصلاح شمع، دل به دو نیم است گاز را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ماری است مار شید که در کیسه خوشترست</p></div>
<div class="m2"><p>در خانه واگذار نماز دراز را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شرم و حیاست لازم آغاز دلبری</p></div>
<div class="m2"><p>کم کم کنند باز، نظر شاهباز را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صائب گرفت رنگ حقیقت مجاز من</p></div>
<div class="m2"><p>تا یافتم حقیقت عشق مجاز را</p></div></div>