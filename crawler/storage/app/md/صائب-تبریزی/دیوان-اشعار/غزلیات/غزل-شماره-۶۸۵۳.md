---
title: >-
    غزل شمارهٔ ۶۸۵۳
---
# غزل شمارهٔ ۶۸۵۳

<div class="b" id="bn1"><div class="m1"><p>اگر نسیم سحرگاه مهربان بودی</p></div>
<div class="m2"><p>ز بوی گل قفسم رشک گلستان بودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عنان گسسته نمی رفت باد پای نفس</p></div>
<div class="m2"><p>اگر حضور درین تیره خاکدان بودی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گهر غبار یتیمی فشاندی از دامن</p></div>
<div class="m2"><p>ز خاکمال حوادث اگر امان بودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شدی ز شکوه خونین من جگرها داغ</p></div>
<div class="m2"><p>اگر چو زخم، دهان مرا زبان بودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زدی غرور سعادت به مغز من آتش</p></div>
<div class="m2"><p>اگر نه رزق همای من استخوان بودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر به چشم تر ما ملایمت کردی</p></div>
<div class="m2"><p>طراوت گل روی تو جاودان بودی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر نهفته نمی بود کارفرمایی</p></div>
<div class="m2"><p>جهان چنان که تو می خواستی چنان بودی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هنوز بود زمین گیر چرخ مینایی</p></div>
<div class="m2"><p>که چون شراب صبوحی به دل روان بودی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز روی گرم تو آتش به جانم افتاده است</p></div>
<div class="m2"><p>خوش آن زمان که به عشاق سرگران بودی!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شکرفشانی نطق تو نیست امروزی</p></div>
<div class="m2"><p>به گاهواره چو عیسی تو خوش زبان بودی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ستاره تو دلا آن زمان سعادت داشت</p></div>
<div class="m2"><p>که همچو خال در آن گوشه دهان بودی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فریب دانه به چشم تو خاک زد چون دام</p></div>
<div class="m2"><p>وگرنه ساکن فردوس جاودان بودی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر ز آینه رویان سخن کشی می داشت</p></div>
<div class="m2"><p>جهان ز طوطی صائب شکرستان بودی</p></div></div>