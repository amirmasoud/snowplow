---
title: >-
    غزل شمارهٔ ۶۵۸۴
---
# غزل شمارهٔ ۶۵۸۴

<div class="b" id="bn1"><div class="m1"><p>به مطلب می رسد جویای کام آهسته آهسته</p></div>
<div class="m2"><p>ز دریا می کشد صیاد دام آهسته آهسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به مغرب می تواند رفت در یک روز از مشرق</p></div>
<div class="m2"><p>گذارد هر که چون خورشید گام آهسته آهسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به همواری بلندی جو که تیغ کوه را آرد</p></div>
<div class="m2"><p>به زیر پای، کبک خوشخرام آهسته آهسته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز تدبیر جنون پخته کار عقل می آید</p></div>
<div class="m2"><p>که مجنون آهوان را کرد رام آهسته آهسته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشو از زیردست خویش ایمن در زبردستی</p></div>
<div class="m2"><p>که خون شیشه را نوشید جام آهسته آهسته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیال نازک آخر می فروزد چهره شهرت</p></div>
<div class="m2"><p>مه نو می شود ماه تمام آهسته آهسته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلی از آه می گفتم شود خالی، ندانستم</p></div>
<div class="m2"><p>که پیچد بر سراپایم چو دام آهسته آهسته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به شکرخند ازان لبهای خوش دشنام قانع شو</p></div>
<div class="m2"><p>که خواهد تلخ گردید این مدام آهسته آهسته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر چه رشته از بار گهرپیچان و لاغر شد</p></div>
<div class="m2"><p>کشید از مغز گوهر انتقام آهسته آهسته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر نام بلند از چرخ خواهی صبر کن صائب</p></div>
<div class="m2"><p>ز پستی می توان رفتن به بام آهسته آهسته</p></div></div>