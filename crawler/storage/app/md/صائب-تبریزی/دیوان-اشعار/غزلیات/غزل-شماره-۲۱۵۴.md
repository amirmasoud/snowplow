---
title: >-
    غزل شمارهٔ ۲۱۵۴
---
# غزل شمارهٔ ۲۱۵۴

<div class="b" id="bn1"><div class="m1"><p>بودی که نمودست وجودش، دهن اوست</p></div>
<div class="m2"><p>سیبی که سهیل است کبابش، ذقن اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا پنجه اقبال که پر زور برآید؟</p></div>
<div class="m2"><p>دست دو جهان در خم سیب ذقن اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وصل مه کنعان چه مناسب به زلیخاست؟</p></div>
<div class="m2"><p>یعقوب شناسد که چه در پیرهن اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک حرف ازان غنچه دهن رنگ ندارم</p></div>
<div class="m2"><p>هر چند که ده رنگ زبان در دهن اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون مرغ چمن جامه جان چاک نسازد؟</p></div>
<div class="m2"><p>پیراهن گلها ز سر پیرهن اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از لعل، سخن پیش رخ یار مگویید</p></div>
<div class="m2"><p>صد برگ خزان دیده چنین در چمن اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر فتنه که امروز ازو نام توان برد</p></div>
<div class="m2"><p>زیر علم زلف شکن بر شکن اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در دیده همت، فلک و کاهکشانش</p></div>
<div class="m2"><p>موری است که پای ملخی در دهن اوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با این همه مشکین نفسی، خامه صائب</p></div>
<div class="m2"><p>یک آهوی رم کرده دشت ختن اوست</p></div></div>