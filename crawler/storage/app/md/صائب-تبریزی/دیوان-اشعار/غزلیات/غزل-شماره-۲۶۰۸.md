---
title: >-
    غزل شمارهٔ ۲۶۰۸
---
# غزل شمارهٔ ۲۶۰۸

<div class="b" id="bn1"><div class="m1"><p>منکران چون دیده شرم و حیا بر هم نهند</p></div>
<div class="m2"><p>تهمت آلودگی بر دامن مریم نهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساده لوحانی که دل بر زندگانی بسته اند</p></div>
<div class="m2"><p>بر سر ریگ روان بنیاد از شبنم نهند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نام رنگین فکرتان برگرد عالم می دود</p></div>
<div class="m2"><p>بر دل خود دست اگر یک چند چون خاتم نهند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیر چشمان قناعت با لب تبخاله ریز</p></div>
<div class="m2"><p>داغهای بی نیازی بر دل زمزم نهند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اینقدر استادگی در زخم ناخن می کنند</p></div>
<div class="m2"><p>وای اگر این ناکسان بر زخم ما مرهم نهند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کیمیای سازگاری خار را گل می کند</p></div>
<div class="m2"><p>غم چه سازد با حریفانی که دل بر غم نهند؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست حیف و میل در میزان عدل کردگار</p></div>
<div class="m2"><p>هر چه زین سر بر تو افزودند زان سر کم نهند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زود باشد حشرشان در خاک با قارون شود</p></div>
<div class="m2"><p>این گرانجانان که سیم و زر به روی هم نهند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب ارباب هوس دارند جوش العطش</p></div>
<div class="m2"><p>روی اگر بر روی گل چون قطره شبنم نهند</p></div></div>