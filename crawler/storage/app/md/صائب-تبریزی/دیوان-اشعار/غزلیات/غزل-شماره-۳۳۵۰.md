---
title: >-
    غزل شمارهٔ ۳۳۵۰
---
# غزل شمارهٔ ۳۳۵۰

<div class="b" id="bn1"><div class="m1"><p>روی آیینه دل تار نمی باید کرد</p></div>
<div class="m2"><p>پشت بر دولت دیدار نمی باید کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از پریشان سخنی عمر قلم شد کوتاه</p></div>
<div class="m2"><p>زندگی در سر گفتار نمی باید کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بالش نرم، کمینگاه گرانخوابیهاست</p></div>
<div class="m2"><p>تکیه بر دولت بیدار نمی باید کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای گل از آتش اگر خط امان می خواهی</p></div>
<div class="m2"><p>خنده بر مرغ گرفتار نمی باید کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با متاعی که به سیم و زر قلب است گران</p></div>
<div class="m2"><p>ناز یوسف به خریدار نمی باید کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بوسه گر نیست دل خسته به پیغام خوش است</p></div>
<div class="m2"><p>زندگی تلخ به بیمار نمی باید کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ابر رحمت نبود قابل هر شوره زمین</p></div>
<div class="m2"><p>باده تکلیف به هشیار نمی باید کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دردها می شود اکثر ز طبیبان افزون</p></div>
<div class="m2"><p>غم خود عرض به غمخوار نمی باید کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از رخ تازه زند خون خریداران جوش</p></div>
<div class="m2"><p>جنس خود کهنه به بازار نمی باید کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر لباس است نظر مردم کوته بین را</p></div>
<div class="m2"><p>سر خود در سر دستار نمی باید کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بی نگهبان چو شود حسن خطرها دارد</p></div>
<div class="m2"><p>خار را دور ز گلزار نمی باید کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل آزاده خود را چو بخیلان صائب</p></div>
<div class="m2"><p>کیسه درهم و دینار نمی باید کرد</p></div></div>