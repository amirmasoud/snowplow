---
title: >-
    غزل شمارهٔ ۱۴۲۱
---
# غزل شمارهٔ ۱۴۲۱

<div class="b" id="bn1"><div class="m1"><p>از لب خشک صدف ریزش نیسان پیداست</p></div>
<div class="m2"><p>خشکی بحر ز سر پنجه مرجان پیداست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نامه ای نیست که عنوان نشود غمازش</p></div>
<div class="m2"><p>کرم و بخل ز پیشانی دربان پیداست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داغ سودای تو از سینه سودازدگان</p></div>
<div class="m2"><p>چون سیه خیمه لیلی ز بیابان پیداست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنقدرها که نگین دان به نگین مشتاق است</p></div>
<div class="m2"><p>بوسه را جای در آن غنچه خندان پیداست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می دهد رخنه دیوار ز گلزار خبر</p></div>
<div class="m2"><p>لطف اندام تو از چاک گریبان پیداست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دل سوخته ما اثری پیدا نیست</p></div>
<div class="m2"><p>دانه هر چند ازان سیب زنخدان پیداست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که دیده است ترا، قدر مرا می داند</p></div>
<div class="m2"><p>حسن سعی چمن آرا ز گلستان پیداست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شبنمی را نتوانست نهان کردن گل</p></div>
<div class="m2"><p>از گل روی تو می خوردن پنهان پیداست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خبر از وحشت نخجیر دهد جنبش دام</p></div>
<div class="m2"><p>پیچ و تاب دل ازان طره پیچان پیداست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در دل خم می پر زور نگیرد آرام</p></div>
<div class="m2"><p>جوش گل از سر دیوار گلستان پیداست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نشود پرتو خورشید نهان در ته ابر</p></div>
<div class="m2"><p>نور واجب ز سراپرده امکان نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رتبه عاشق از ارباب هوس معلوم است</p></div>
<div class="m2"><p>دیده شیر چو آتش ز نیستان پیداست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نور فیض است که بر زنده دلان می بارد</p></div>
<div class="m2"><p>این نه شمع است که از خاک شهیدان پیداست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بستن لب نشود مانع اظهار کمال</p></div>
<div class="m2"><p>در صدف رتبه این گوهر غلطان پیداست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بسته است آینه موی شکافان زنگار</p></div>
<div class="m2"><p>ورنه از جبهه من حال پریشان پیداست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دل آزاده درین باغ اقامت نکند</p></div>
<div class="m2"><p>وحشت سرو ز برچیدن دامان پیداست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>میزبانی سفره دعوی نکند بیهده باز</p></div>
<div class="m2"><p>شکوه و شکر ز پیشانی مهمان پیداست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>می دهد سادگی دل خبر از آزادی</p></div>
<div class="m2"><p>صافی شست ز بیرنگی پیکان پیداست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>فکر رنگین تو صائب ز خیالات دگر</p></div>
<div class="m2"><p>چون گل سرخ ز خاروخس بستان پیداست</p></div></div>