---
title: >-
    غزل شمارهٔ ۵۹۵۰
---
# غزل شمارهٔ ۵۹۵۰

<div class="b" id="bn1"><div class="m1"><p>از باد دستی خود ما میکشان خرابیم</p></div>
<div class="m2"><p>در کاسه سرنگونی همچشم با حبابیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با محتسب به جنگیم از زاهدان به تنگیم</p></div>
<div class="m2"><p>با شیشه ایم یکدل، یکرنگ با شرابیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنجا که میکشانند چون ابر تر زبانیم</p></div>
<div class="m2"><p>آنجا که زاهدانند لب خشک چون سرابیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در گوش عشقبازان چون مژده وصالیم</p></div>
<div class="m2"><p>در چشم می پرستان چون قطره شرابیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با خاص و عام یکرنگ از مشرب رساییم</p></div>
<div class="m2"><p>بر خار و گل سمن ریز چون نور ماهتابیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنجا که داغ بیدرد گل کرد، پنبه زاریم</p></div>
<div class="m2"><p>آنجا که زخم عشاق خندید، مشک نابیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنجا که گل شکفته است شبنم طراز اشکیم</p></div>
<div class="m2"><p>آنجا که خار خشکی است چشم تر سحابیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون می به مجلس آید از ما ادب مجویید</p></div>
<div class="m2"><p>تا نیست دختر رز در پرده حجابیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در پله نظرها هرگز گران نگردیم</p></div>
<div class="m2"><p>ما در سواد عالم چون شعر انتخابیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زلف معنبری نیست زان روی بی دماغیم</p></div>
<div class="m2"><p>حسن برشته ای نیست از بهر آن کبابیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر تیغ حدت طبع در جمع موشکافان</p></div>
<div class="m2"><p>ما جوهریم ازان رو در قید پیچ و تابیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از مشرق بناگوش خندید صبح پیری</p></div>
<div class="m2"><p>ما تیره روزگاران در سیر ماهتابیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یک ره به گوشه چشم در زی پا نظر کن</p></div>
<div class="m2"><p>عمری است پایمالت چون دیده رکابیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا اقتدا نمودیم بر فطرت ظفرخان</p></div>
<div class="m2"><p>چون فکرهای صائب پیوسته بر صوابیم</p></div></div>