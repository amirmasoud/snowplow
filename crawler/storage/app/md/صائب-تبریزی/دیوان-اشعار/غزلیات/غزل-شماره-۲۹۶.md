---
title: >-
    غزل شمارهٔ ۲۹۶
---
# غزل شمارهٔ ۲۹۶

<div class="b" id="bn1"><div class="m1"><p>راز دل را می توان دریافت از سیمای ما</p></div>
<div class="m2"><p>نشأه می تابد چو رنگ از پرده مینای ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قهرمان عدل چون پرسش کند روز حساب</p></div>
<div class="m2"><p>از بهشت عافیت خاری نگیرد پای ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چه او هرگز نمی گیرد ز حال ما خبر</p></div>
<div class="m2"><p>درد او هر شب خبر گیرد ز سر تا پای ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دل پر خون ما بی چاشنی نتوان گذشت</p></div>
<div class="m2"><p>خون رغبت را به جوش آرد می حمرای ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوهر خورشید اگر از دست ما افتد به خاک</p></div>
<div class="m2"><p>زیر پای خود نبیند طبع بی پروای ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سبحه ذکر ملایک از نظام افتاده است</p></div>
<div class="m2"><p>بس که پیچیده است در گوش فلک غوغای ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از خط فرمان او روزی که پا بیرون نهیم</p></div>
<div class="m2"><p>تیشه گردد هر سر خاری به قصد پای ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون بساط سبزه زیر پای سرو افتاده است</p></div>
<div class="m2"><p>آسمان در زیر پای همت والای ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ریخت شور حشر در پیمانه عالم نمک</p></div>
<div class="m2"><p>می زند جوش سیه مستی همان صهبای ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حال باطن را قیاس از حال ظاهر می کند</p></div>
<div class="m2"><p>دام را در خاک می بیند دل دانای ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پای ما یک خار را نگذاشت صائب بی شکست</p></div>
<div class="m2"><p>آه اگر خار انتقام خود کشد از پای ما</p></div></div>