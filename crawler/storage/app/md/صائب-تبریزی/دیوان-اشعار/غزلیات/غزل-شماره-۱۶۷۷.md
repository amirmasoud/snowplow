---
title: >-
    غزل شمارهٔ ۱۶۷۷
---
# غزل شمارهٔ ۱۶۷۷

<div class="b" id="bn1"><div class="m1"><p>حضور خاطر اگر در نماز معتبرست</p></div>
<div class="m2"><p>امید ما به نماز نکرده بیشترست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گرمی جگر ما دل که خواهد سوخت؟</p></div>
<div class="m2"><p>درین بساط که خورشید آتشین جگرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شرر به آتش و شبنم به بوستان برگشت</p></div>
<div class="m2"><p>حضور خاطر عاشق هنوز در سفرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دار و گیر خزان و بهار آسوده است</p></div>
<div class="m2"><p>چو سرو هر که درین روزگار بی ثمرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حباب کسب هوا می کند ز بی بصری</p></div>
<div class="m2"><p>درین محیط که کشتی نوح در خطرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دمید صبح قیامت، رسید روز جزا</p></div>
<div class="m2"><p>هنوز صائب مغرور مست و بیخبرست</p></div></div>