---
title: >-
    غزل شمارهٔ ۵۰۶۹
---
# غزل شمارهٔ ۵۰۶۹

<div class="b" id="bn1"><div class="m1"><p>خود کرده ام به شکوه تراخصم جان خویش</p></div>
<div class="m2"><p>کافر مباد کشته تیغ زبان خویش !</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک مرد در قلمرو جرأت نیافتم</p></div>
<div class="m2"><p>در دل چوآفتاب شکستم سنان خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرگز چنان نشد که درین دشت پرشکار</p></div>
<div class="m2"><p>دست نوازشی بکشم برکمان خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آتش به مصحف پر پروانه می زند</p></div>
<div class="m2"><p>این شمع هیچ رحم ندارد به جان خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در وادیی که خضرزند جوش العطش</p></div>
<div class="m2"><p>دارم عقیق صبربه زیر زبان خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون موج ازکشاکش این بحر نیلگون</p></div>
<div class="m2"><p>فرصت نیافتم که بگیرم عنان خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلبل به خاکساری من رشک میبرد</p></div>
<div class="m2"><p>افتاده ام ز جوش گل ازآشیان خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب به گردکعبه مقصد کجارسد؟</p></div>
<div class="m2"><p>دارد هزار مرحله تاآستان خویش</p></div></div>