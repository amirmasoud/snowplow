---
title: >-
    غزل شمارهٔ ۲۲۲۵
---
# غزل شمارهٔ ۲۲۲۵

<div class="b" id="bn1"><div class="m1"><p>موج سراب دنیا، شمشیر آبدارست</p></div>
<div class="m2"><p>آبش لعاب افعی، خارش زبان مارست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خمیازه نشاط است گلهای خنده رویش</p></div>
<div class="m2"><p>سر جوش باده او ته جرعه خمارست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تاجش به دیده عقل کیلی است عمرپیما</p></div>
<div class="m2"><p>تختش به چشم عبرت کرسی زیر دارست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون کوه پایدارست درد گران رکابش</p></div>
<div class="m2"><p>عمر سبک عنانش چون برق در گذارست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیداست تا چه باشد الوان نعمت او</p></div>
<div class="m2"><p>جایی که شیر مادر خون نقابدارست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون سگ گزیده از آب وحشت کند ز دنیا</p></div>
<div class="m2"><p>آیینه بصیرت آن را کی بی غبارست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرد کدورت از دل بی اشک برنخیزد</p></div>
<div class="m2"><p>روشنگر وجودست چشمی که اشکبارست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از خلق خوش توان شد در چشم خلق شیرین</p></div>
<div class="m2"><p>صبح گشاده رو را انجم زر نثارست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در گوهر آب گوهر در بحر می کند سیر</p></div>
<div class="m2"><p>واصل بود به جانان جانی که بیقرارست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از خود کناره گیران صائب مدام شادند</p></div>
<div class="m2"><p>پیوسته صاف باشد بحری که بیکنارست</p></div></div>