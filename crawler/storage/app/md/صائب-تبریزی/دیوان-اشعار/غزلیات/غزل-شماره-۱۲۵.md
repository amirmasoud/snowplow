---
title: >-
    غزل شمارهٔ ۱۲۵
---
# غزل شمارهٔ ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>شور عشقی کو، که رسوای جهان سازد مرا؟</p></div>
<div class="m2"><p>بی نیاز از نام و فارغ از نشان سازد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند چون آب گهر باشم گره در یک مقام؟</p></div>
<div class="m2"><p>خضر راهی کو، که موج خوش عنان سازد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می گریزم در پناه بی خودی از خلق، چند</p></div>
<div class="m2"><p>خودفروشی بنده این کاروان سازد مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوشتر از کنج دهان یار می آید به چشم</p></div>
<div class="m2"><p>گوشه ای کز دیده مردم نهان سازد مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می کنم پهلو تهی از قرب، تا کی چون صدف</p></div>
<div class="m2"><p>چربی پهلوی گوهر، استخوان سازد مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وادی پیموده را از سرگرفتن مشکل است</p></div>
<div class="m2"><p>چون زلیخا، عشق می ترسم جوان سازد مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بخیه از جوهر زنم بر چشم شوخ آیینه ا</p></div>
<div class="m2"><p>چهره محجوب او گر دیده بان سازد مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جلوه دست و گریبان گل این بوستان</p></div>
<div class="m2"><p>سخت می ترسم خجل از باغبان سازد مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>استخوانم همچو صبح آغوش رغبت واکند</p></div>
<div class="m2"><p>گر نشان تیر، آن ابرو کمان سازد مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر چه خاک راه عشقم، می خورم خون گر به سهو</p></div>
<div class="m2"><p>بادپیمایی طرف با آسمان سازد مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب از راز دهان او نیارم سر برون</p></div>
<div class="m2"><p>فکر اگر باریک چون موی میان سازد مرا</p></div></div>