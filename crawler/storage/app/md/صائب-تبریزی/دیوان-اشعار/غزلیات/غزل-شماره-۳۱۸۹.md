---
title: >-
    غزل شمارهٔ ۳۱۸۹
---
# غزل شمارهٔ ۳۱۸۹

<div class="b" id="bn1"><div class="m1"><p>به امید چه از تن غافلان را جان برون آید؟</p></div>
<div class="m2"><p>به کشتن می رود چون خونی از زندان برون آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمشرق می شود هر اختری در وقت خود طالع</p></div>
<div class="m2"><p>رسد چون نوبت نان طفل را دندان برون آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مخور زنهار روی دست این دریانوردان را</p></div>
<div class="m2"><p>که خشک از بحر گوهر پنجه مرجان برون آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زر قلب است نقدی هست اگر این کاروانی را</p></div>
<div class="m2"><p>به امید چه یوسف از چه کنعان برون آید؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مبر پیش فلک زنهار آب روی خواهش را</p></div>
<div class="m2"><p>که طوفان از تنور او به جای نان برون آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سبکدستی کز او دلهای سرگردان شود زخمی</p></div>
<div class="m2"><p>زمیدان سر به پیش افکنده چون چوگان برون آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نصیحت در شرارت گرم سازد سخت رویان را</p></div>
<div class="m2"><p>که چون بر سنگ آید آتش از پیکان برون آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جدا از گوشه عزلت ندیدم روی امنیت</p></div>
<div class="m2"><p>به جان لرزد چراغی کز ته دامان برون آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گدایی دارم از مطرب نوای خانه پردازی</p></div>
<div class="m2"><p>که جان از تنگنای سینه دست افشان برون آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نمی گردد تهی صائب زبرگ عیش دامانش</p></div>
<div class="m2"><p>گلستانی کز او نظارگی خندان برون آید</p></div></div>