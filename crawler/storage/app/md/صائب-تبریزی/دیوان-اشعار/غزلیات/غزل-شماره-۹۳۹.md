---
title: >-
    غزل شمارهٔ ۹۳۹
---
# غزل شمارهٔ ۹۳۹

<div class="b" id="bn1"><div class="m1"><p>چرخ ماند از گردش اما اضطراب دل بجاست</p></div>
<div class="m2"><p>تیغ شد کند و سماع طایر بسمل بجاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق بی تاب است تا دوران خط آخر شدن</p></div>
<div class="m2"><p>چشم مجنون می پرد تا گردی از محمل بجاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تیغ خونریزست تا یک کشتنی در عرصه هست</p></div>
<div class="m2"><p>حسن مغرورست تا یک عاشق بیدل بجاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شش جهت از کعبه دل در کمند اندازیند</p></div>
<div class="m2"><p>گر به هر جانب شود آن شاخ گل مایل بجاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیم جانی داده اند و یک جهان دل برده اند</p></div>
<div class="m2"><p>روز محشر با شهیدان دعوی قاتل بجاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچ کافر را مبادا خودپرستی سد راه!</p></div>
<div class="m2"><p>آسمان شد با زمین هموار و این حایل بجاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل چو از جا رفت، عالم می شود زیر و زبر</p></div>
<div class="m2"><p>نیست بی پرگار دور آسمان تا دل بجاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نور و ظلمت با جهان آب و گل آمیخته است</p></div>
<div class="m2"><p>تا زمین و آسمان باشد حق و باطل بجاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا نگردیده است عادت، نشأه می بخشد شراب</p></div>
<div class="m2"><p>گر به امید جنون از نو شوم عاقل بجاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا شکاری هست، در پرواز باشد چشم دام</p></div>
<div class="m2"><p>نیست زلف یار را آرام تا یک دل بجاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زشت صائب زیر گل خواهد نهان آیینه را</p></div>
<div class="m2"><p>خصمی گردون دون با مردم قابل بجاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این جواب حضرت میرزا سعید ما که گفت</p></div>
<div class="m2"><p>این گره از رشته ما وا شد و مشکل بجاست</p></div></div>