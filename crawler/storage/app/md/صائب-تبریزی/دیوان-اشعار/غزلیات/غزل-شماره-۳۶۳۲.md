---
title: >-
    غزل شمارهٔ ۳۶۳۲
---
# غزل شمارهٔ ۳۶۳۲

<div class="b" id="bn1"><div class="m1"><p>اگر از سنگ رگ سنگ برون می‌آید</p></div>
<div class="m2"><p>ریشه غم ز دل تنگ برون می‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باده روح درین شیشه نخواهد ماندن</p></div>
<div class="m2"><p>آخر این آینه از زنگ برون می‌آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سنگ اطفال به مجنون چه تواند کردن؟</p></div>
<div class="m2"><p>این شرار از جگر سنگ برون می‌آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شیوه عشق وفادار همان یکرنگی است</p></div>
<div class="m2"><p>حُسن هرچند به صد رنگ برون می‌آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خون چو شد مشک، نماند به ته پوست نهان</p></div>
<div class="m2"><p>از عقیقش خط شبرنگ برون می‌آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسن سنگین دل اگر گشت ملایم چه عجب؟</p></div>
<div class="m2"><p>مومیایی ز دل سنگ برون می‌آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌شود صائب از آن موی‌کمر نازک‌تر</p></div>
<div class="m2"><p>تا سخن زان دهن تنگ برون می‌آید</p></div></div>