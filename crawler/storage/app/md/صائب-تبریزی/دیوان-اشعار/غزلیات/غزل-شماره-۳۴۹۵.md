---
title: >-
    غزل شمارهٔ ۳۴۹۵
---
# غزل شمارهٔ ۳۴۹۵

<div class="b" id="bn1"><div class="m1"><p>ما به ساقی و حریفان به شراب افتادند</p></div>
<div class="m2"><p>ما به سرچشمه و یاران به سراب افتادند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ثمر از ماست اگر برگ دگرها بردند</p></div>
<div class="m2"><p>گوهر از ماست اگر خلق در آب افتادند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خبر از داغ جگرسوز غریبان دارند</p></div>
<div class="m2"><p>موجهایی که ز دریا به سراب افتادند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پشت دادند به دیوار صدف چون گوهر</p></div>
<div class="m2"><p>قطره هایی که به دریا ز سحاب افتادند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آه افسوس بود حاصل معمارانی</p></div>
<div class="m2"><p>که به تعمیر من خانه خراب افتادند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در چمن رشک بر آن بال فشانان دارم</p></div>
<div class="m2"><p>که به سرپنجه شاهین و عقاب افتادند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شب زلف تو چه افسانه ندانم سر کرد</p></div>
<div class="m2"><p>عاملانی که به دیوان حساب افتادند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل معمور از آن قوم طلب کن صائب</p></div>
<div class="m2"><p>که به یک جلوه مستانه خراب افتادند</p></div></div>