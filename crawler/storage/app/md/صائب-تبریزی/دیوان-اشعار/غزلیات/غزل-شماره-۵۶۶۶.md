---
title: >-
    غزل شمارهٔ ۵۶۶۶
---
# غزل شمارهٔ ۵۶۶۶

<div class="b" id="bn1"><div class="m1"><p>ما به دنیا نه پی ناز و نعم آمده ایم</p></div>
<div class="m2"><p>بهر تحصیل غم و درد و الم آمده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دامن از ما مکش ای ساحل امید که ما</p></div>
<div class="m2"><p>به زمین بوس تو از بحر عدم آمده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قطع و وصل شب و روز از نفس روشن ماست</p></div>
<div class="m2"><p>نور صبحیم که با تیغ دودم آمده ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باش گو وقت سفر تنگتر از شق قلم</p></div>
<div class="m2"><p>ما کمر بسته برون همچو قلم آمده ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نعل وارون نشود رهزن ماراست روان</p></div>
<div class="m2"><p>کز ره دیر مکرر به حرم آمده ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی نیازی ز دل یار گدایی داریم</p></div>
<div class="m2"><p>ما به این در نه به امید درم آمده ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب از تیغ ز درگاه کرم پا نکشیم</p></div>
<div class="m2"><p>ما درین راه به سر همچو قلم آمده ایم</p></div></div>