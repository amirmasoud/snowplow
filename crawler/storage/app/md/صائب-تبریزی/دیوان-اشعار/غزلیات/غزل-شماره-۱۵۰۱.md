---
title: >-
    غزل شمارهٔ ۱۵۰۱
---
# غزل شمارهٔ ۱۵۰۱

<div class="b" id="bn1"><div class="m1"><p>عشق سرمایه تسکین دل زار من است</p></div>
<div class="m2"><p>خانه پرداز جهان خانه نگهدار من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درد را طاقت من کسوت درمان پوشد</p></div>
<div class="m2"><p>صندل جبهه من زدی رخسار من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست در خلوت من پرتو منت را راه</p></div>
<div class="m2"><p>شمع کاشانه من دیده بیدار من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشتی خالیم، آرام نمی دانم چیست</p></div>
<div class="m2"><p>هر که باری ننهد بر دل من، بار من است!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نکند شعله بدل جامه ز رنگینی موم</p></div>
<div class="m2"><p>می عبث در پی رنگینی رخسار من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخن تلخ به شیرینی جان می گیرم</p></div>
<div class="m2"><p>هر که را هست زر قلب، خریدار من است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پا به دولت زند آن کس که زند پای به من</p></div>
<div class="m2"><p>سایه بال هما سایه دیوار من است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آتش از گرمی افسانه من گوش گرفت</p></div>
<div class="m2"><p>گوش هر خام کجا لایق اسرار من است؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که گم کرد غمی، در دل من می یابد</p></div>
<div class="m2"><p>وعده گاه غم عالم دل افگار من است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لامکان سیرتر از همت خویشم صائب</p></div>
<div class="m2"><p>خویش را گم کند آن کس که طلبکار من است</p></div></div>