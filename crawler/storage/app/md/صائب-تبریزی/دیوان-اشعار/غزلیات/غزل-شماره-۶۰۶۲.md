---
title: >-
    غزل شمارهٔ ۶۰۶۲
---
# غزل شمارهٔ ۶۰۶۲

<div class="b" id="bn1"><div class="m1"><p>چرخ را خاکستری از برق سودا کرد حسن</p></div>
<div class="m2"><p>نقطه خاک سیه را چون سویدا کرد حسن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غوطه در خون شفق زد ساعد سیمین صبح</p></div>
<div class="m2"><p>تا به خونریز اسیران دست بالا کرد حسن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوه طوری را که بر زانوی تمکین تکیه داشت</p></div>
<div class="m2"><p>از نگاه برق جولان آتشین پا کرد حسن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غوطه در موج حلاوت زد زمین و آسمان</p></div>
<div class="m2"><p>تا ز شکر خنده پنهان گره وا کرد حسن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می برد هر قطره اشکم لذتی از دیدنش</p></div>
<div class="m2"><p>شبنم این بوستان را چشم بینا کرد حسن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کوه را دل آب شد تا لعل او سیراب گشت</p></div>
<div class="m2"><p>غوطه در خون زد جهان تا رنگ پیدا کرد حسن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صورت احوال مجنون چون بماند در نقاب؟</p></div>
<div class="m2"><p>نقش پای ناقه را آیینه سیما کرد حسن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیده را آیینه دار مهر کردن مشکل است</p></div>
<div class="m2"><p>حیرتی دارم که چون خود را تماشا کرد حسن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>(دید دیگر نیست) ما را تاب درد انتظار</p></div>
<div class="m2"><p>نعمت فردوس را اینجا مهیا کرد حسن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طفل شوخ و عزت مصحف، چه فکر باطل است؟</p></div>
<div class="m2"><p>دفتر دل را به یک ساعت مجزا کرد حسن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر چه صائب روز ما را تیره تر از زلف ساخت</p></div>
<div class="m2"><p>داغ ما را آفتاب عالم آرا کرد حسن</p></div></div>