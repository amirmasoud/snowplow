---
title: >-
    غزل شمارهٔ ۱۱۳۶
---
# غزل شمارهٔ ۱۱۳۶

<div class="b" id="bn1"><div class="m1"><p>داغ سودا فارغ از فکر کلاهم کرده است</p></div>
<div class="m2"><p>بی نیاز از افسر این چتر سیاهم کرده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خار دامنگیر گردد شهپر پرواز من</p></div>
<div class="m2"><p>تا محبت جذبه خود خضر را هم کرده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از پناه خود مرا حاشا که سازد ناامید</p></div>
<div class="m2"><p>آن که چون صحرای محشر بی پناهم کرده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر امید ناامیدان برنمی آرد، چرا</p></div>
<div class="m2"><p>ناامید از عالم آن امیدگاهم کرده است؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تیره روزم، لیک از غیرت دل خود می خورم</p></div>
<div class="m2"><p>مهر عالمتاب اگر روشن چو ماهم کرده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاهد دلسوزی گردون عاجزکش بس است</p></div>
<div class="m2"><p>خنده برق آنچه با مشت گیاهم کرده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیل بی زنهار را مانع ز جولان می شود</p></div>
<div class="m2"><p>خواب سنگینی که غفلت سنگ را هم کرده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در چه افکنده است باز از قیمت نازل مرا</p></div>
<div class="m2"><p>کاروانی گر خلاص از قید چاهم کرده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غنچه خسبان می ربایندم ز دست یکدگر</p></div>
<div class="m2"><p>تا سبکروحی نسیم صبحگاهم کرده است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا گشودم چشم روشن در شبستان وجود</p></div>
<div class="m2"><p>راستی، چون شمع، خرج اشک و آهم کرده است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خواهد از داغ ندامت سوخت صائب چون چراغ</p></div>
<div class="m2"><p>آن که دور از محفل خود بیگناهم کرده است</p></div></div>