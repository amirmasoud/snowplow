---
title: >-
    غزل شمارهٔ ۵۳۹۰
---
# غزل شمارهٔ ۵۳۹۰

<div class="b" id="bn1"><div class="m1"><p>عافیت زان غمزه خونخوار می‌خواهد دلم</p></div>
<div class="m2"><p>آب رحم از تیغ بی‌زنهار می‌خواهد دلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راه حرفی پیش لعل یار می‌خواهد دلم</p></div>
<div class="m2"><p>خلوتی در پرده اسرار می‌خواهد دلم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قصه سودای من دور و دراز افتاده است</p></div>
<div class="m2"><p>کوچه راهی همچو زلف یار می‌خواهد دلم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیستم چون بلبلان قانع به گفت‌وگوی گل</p></div>
<div class="m2"><p>باغ را در غنچه منقار می‌خواهد دلم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا نگردیده است از خط تنگ وقت آن دهان</p></div>
<div class="m2"><p>بوسه‌ای زان لعل شکربار می‌خواهد دلم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مست و خواب‌آلود اگر گردد دچار من شبی</p></div>
<div class="m2"><p>خون خود زان لعل گوهربار می‌خواهد دلم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روی حرفم چون قلم با لوح‌های ساده است</p></div>
<div class="m2"><p>صحبت دیوانگان بسیار می‌خواهد دلم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیش همت از ادب دور است تکرار سؤال</p></div>
<div class="m2"><p>هردو عالم را از او یک بار می‌خواهد دلم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرهم راحت مرا در خواب بی‌دردی فکند</p></div>
<div class="m2"><p>کاو کاو نشتر آزار می‌خواهد دلم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ساده‌لوحی بین که با چندین نسیم پرده‌در</p></div>
<div class="m2"><p>غنچه مستور ازین گلزار می‌خواهد دلم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وادی سر گشتگی دارد سراپا گشتنی</p></div>
<div class="m2"><p>پایی از فولاد چون پرگار می‌خواهد دلم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دیده بیدار نتوان یافت در روی زمین</p></div>
<div class="m2"><p>زین گران‌خوابان دل بیدار می‌خواهد دلم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>می‌کند تنگی قفس بر خنده سرشار من</p></div>
<div class="m2"><p>کبک مستم، دامن کهسار می‌خواهد دلم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اختلاف کفر و دین از وحدتم بیگانه ساخت</p></div>
<div class="m2"><p>رشته تسبیح از زنار می‌خواهد دلم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هیچ کس خون کباب از آتش سوزان نخواست</p></div>
<div class="m2"><p>خون‌بهای دل از آن رخسار می‌خواهد دلم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نیست تاب چشم زخم آیینه‌های صاف را</p></div>
<div class="m2"><p>چند روزی مرهم زنگار می‌خواهد دلم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سیل هیهات است تا دریا کند جایی مقام</p></div>
<div class="m2"><p>لنگر از عمر سبک‌رفتار می‌خواهد دلم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>توبه مستی و مخموری ندارد اعتبار</p></div>
<div class="m2"><p>فرصتی از بهر استغنار می‌خواهد دلم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در رهی کز خار مجروح است پای آفتاب</p></div>
<div class="m2"><p>سوزن عیسی برای خار می‌خواهد دلم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در علاج درد من صائب مسیحا عاجز است</p></div>
<div class="m2"><p>چاره درد خود از عطار می‌خواهد دلم</p></div></div>