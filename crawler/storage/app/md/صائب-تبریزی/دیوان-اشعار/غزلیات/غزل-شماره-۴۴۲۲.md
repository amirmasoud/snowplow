---
title: >-
    غزل شمارهٔ ۴۴۲۲
---
# غزل شمارهٔ ۴۴۲۲

<div class="b" id="bn1"><div class="m1"><p>گر یار ز احوال من آگاه نمی بود</p></div>
<div class="m2"><p>درد من سودازده جانکاه نمی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جا در دل او دارم واز من خبرش نیست</p></div>
<div class="m2"><p>ای کاش مرا در دل اوراه نمی بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از سردی آه است که جان می برم از اشک</p></div>
<div class="m2"><p>می سوخت مرا اشک اگر آه نمی بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق گهر اشک به دامان که می ریخت</p></div>
<div class="m2"><p>گر دامن اقبال سحرگاه نمی بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دردست به مقصود رساننده سالک</p></div>
<div class="m2"><p>گر درد نمی بود به حق راه نمی بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در قبضه آه است کلید در مقصود</p></div>
<div class="m2"><p>ای وای به من صائب اگر آه نمی بود</p></div></div>