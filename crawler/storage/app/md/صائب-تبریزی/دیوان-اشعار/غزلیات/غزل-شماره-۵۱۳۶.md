---
title: >-
    غزل شمارهٔ ۵۱۳۶
---
# غزل شمارهٔ ۵۱۳۶

<div class="b" id="bn1"><div class="m1"><p>منم به نکهت خشکی ز بوستان قانع</p></div>
<div class="m2"><p>ز وصل گل به خس و خار آشیان قانع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درون خانه شکارش آماده است</p></div>
<div class="m2"><p>کسی که گشت به خمیازه چون کمان قانع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زبان دراز بود، هرکه همچو تیغ شود</p></div>
<div class="m2"><p>به خون ز نعمت الوان این جهان قانع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چرا طفیلی زاغ سیاه کاسه شود؟</p></div>
<div class="m2"><p>کسی که همچو هما شد به استخوان قانع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به سیم ، دامن یوسف ز دست نتوان داد</p></div>
<div class="m2"><p>ازان جهان نتوان شد به این جهان قانع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همیشه بر لب بام خطر بود درخواب</p></div>
<div class="m2"><p>ز صدر هرکه نگردد به آستان قانع</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز آب خضر حیات ابد تمنا کن</p></div>
<div class="m2"><p>مشو ز تیغ شهادت به نیم جان قانع</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شود خزینه اسرار سینه اش صائب</p></div>
<div class="m2"><p>کسی که شد به لب خامش ازبیان قانع</p></div></div>