---
title: >-
    غزل شمارهٔ ۲۴۱۵
---
# غزل شمارهٔ ۲۴۱۵

<div class="b" id="bn1"><div class="m1"><p>تا تو رفتی عالم روشن به چشمم تار شد</p></div>
<div class="m2"><p>باده بی غش به جامم شربت بیمار شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دهان باز بودم حلقه بیرون در</p></div>
<div class="m2"><p>تا زبان بستم دلم گنجینه اسرار شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رو سفیدی بود در کردار و عمر من تمام</p></div>
<div class="m2"><p>چون قلم از دل سیاهی صرف در گفتار شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست در دل خاری از منع چمن پیرا مرا</p></div>
<div class="m2"><p>جوش گل مانع مرا از سیر این گلزار شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرب نرمی شد حصار عافیت ز آتش مرا</p></div>
<div class="m2"><p>از گداز ایمن بود هر زر که دست افشار شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خموشی گشت روشن تا دل تاریک من</p></div>
<div class="m2"><p>طوطی خوش حرف بر آیینه ام زنگار شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر جنون دوری من حلقه دیگر فزود</p></div>
<div class="m2"><p>نقطه خالش زخط روزی که خوش پرگار شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیچ و تاب نامرادیها به قدر دانش است</p></div>
<div class="m2"><p>می خورد خون بیش هر تیغی که جوهردار شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می کشد ناصح زبان از روی سخت من به کام</p></div>
<div class="m2"><p>خواهد این سوهان ز ناهمواریم هموار شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در شبستان فنا صبح امیدی می شود</p></div>
<div class="m2"><p>آنچه از انفاس، صائب صرف استغفار شد</p></div></div>