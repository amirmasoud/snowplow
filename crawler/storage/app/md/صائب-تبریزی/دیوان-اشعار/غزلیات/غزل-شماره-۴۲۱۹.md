---
title: >-
    غزل شمارهٔ ۴۲۱۹
---
# غزل شمارهٔ ۴۲۱۹

<div class="b" id="bn1"><div class="m1"><p>گر یوسف مرا به دو عالم بها کنند</p></div>
<div class="m2"><p>گرد کسادیم به نظر توتیا کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جمعی که زیر چرخ شبی روز کرده اند</p></div>
<div class="m2"><p>چون شمع دل خنک به نسیم فنا کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون برق تیغ نعل زوالش در آتش است</p></div>
<div class="m2"><p>کسب سعادتی که ز بال هما کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نتوان به خواب دردل شب فیض صبح یافت</p></div>
<div class="m2"><p>کاین در به روی دیده بیدار واکنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این راه دور زود به انجام می رسد</p></div>
<div class="m2"><p>از دست اختیار عنان گر رها کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آزادگان که دست به عالم فشانده اند</p></div>
<div class="m2"><p>سیر بهشت در دل بی مدعاکنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جای ترحم است به جمعی که چون حباب</p></div>
<div class="m2"><p>خود را ز بحر دور به کسب هوا کنند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای مدعی بسوز که عشاق بی زبان</p></div>
<div class="m2"><p>صد داستان به یک تپش دل ادا کنند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اشکش ز دل غبار کدورت نمی برند</p></div>
<div class="m2"><p>چشمی که تر به یاوری توتیا کنند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صائب جماعتی که به معنی رسیده اند</p></div>
<div class="m2"><p>حاشا که التفات به آب بقا کنند</p></div></div>