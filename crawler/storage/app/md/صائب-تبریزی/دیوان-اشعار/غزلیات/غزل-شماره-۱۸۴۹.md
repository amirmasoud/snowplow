---
title: >-
    غزل شمارهٔ ۱۸۴۹
---
# غزل شمارهٔ ۱۸۴۹

<div class="b" id="bn1"><div class="m1"><p>دلم ز گریه مستانه هم صفا نگرفت</p></div>
<div class="m2"><p>فغان که آب شد آیینه و جلا نگرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیامد از ته حرف شکوه ام به زبان</p></div>
<div class="m2"><p>شرر ز آتش آسوده ام هوا نگرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کجا به مردم بیگانه انس می گیرد؟</p></div>
<div class="m2"><p>رمیده ای که سلامی ز آشنا نگرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز چشم، کاسه دریوزه سیر چشمی من</p></div>
<div class="m2"><p>به رنگ بی بصران پیش توتیا نگرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز مد عمر، نصیبش سیاهکاری بود</p></div>
<div class="m2"><p>کسی که سرخط مشق جنون ز ما نگرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شود به باد کجا حکم او روان چون آب؟</p></div>
<div class="m2"><p>سبکروی که هوا را به زیر پا نگرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بس است سایه تیر تو استخوان مرا</p></div>
<div class="m2"><p>مرا به زیر پر و بال اگر هما نگرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کجا رسدبه گریبان مدعا صائب؟</p></div>
<div class="m2"><p>که دست کوته ما دامن دعا نگرفت</p></div></div>