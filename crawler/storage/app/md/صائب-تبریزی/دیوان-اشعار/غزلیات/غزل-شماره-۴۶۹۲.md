---
title: >-
    غزل شمارهٔ ۴۶۹۲
---
# غزل شمارهٔ ۴۶۹۲

<div class="b" id="bn1"><div class="m1"><p>ترا لبی است ز چشم ستاره خندانتر</p></div>
<div class="m2"><p>مرادلی ز دهان تو تنگ میدانتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلی است در بر من زین جهان پر وحشت</p></div>
<div class="m2"><p>ز چشم شوخ تو از مردمان گریزانتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حذر ز خیرگی من مکن که دیده من</p></div>
<div class="m2"><p>ز چشم آینه صدپرده است حیرانتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگرچه در کمر یار حلقه کردم دست</p></div>
<div class="m2"><p>همان ز زلف بود خاطرم پریشانتر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برآن عذار جهانسوز، قطره عرق است</p></div>
<div class="m2"><p>ستاره ای که بود ز آفتاب بخشانتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر چه سینه آیینه از غرض پاک است</p></div>
<div class="m2"><p>ز دیده تر من نیست پاکدامنتر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد از سفیدی مو بیش بیقراری دل</p></div>
<div class="m2"><p>که می شود به دم صبح شمع لرزانتر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صلاح خالص ازان کن طلب که طاعت را</p></div>
<div class="m2"><p>کند ز دیده خلق از گناه پنهانتر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز سوز عشق رگ جان به تن مرا صائب</p></div>
<div class="m2"><p>ز مو برآتش سوزنده است پیچانتر</p></div></div>