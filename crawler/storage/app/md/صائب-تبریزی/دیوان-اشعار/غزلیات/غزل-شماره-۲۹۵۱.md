---
title: >-
    غزل شمارهٔ ۲۹۵۱
---
# غزل شمارهٔ ۲۹۵۱

<div class="b" id="bn1"><div class="m1"><p>دل بی غم نصیب از نقطه سودا نمی دارد</p></div>
<div class="m2"><p>که هرگز آب شیرین عنبر سارا نمی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدار ای ناصح بیکار دست از جستجوی ما</p></div>
<div class="m2"><p>که از خود رفته در دنبال نقش پا نمی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندارد راه در دارالامان خامشی آفت</p></div>
<div class="m2"><p>صدف اندیشه ای از تلخی دریا نمی دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به نور شمع نتوان برد راه از خویشتن بیرون</p></div>
<div class="m2"><p>که این ظلمت چراغی جز دل بیتا نمی دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مکن از بیخودی منع دل سودایی صائب</p></div>
<div class="m2"><p>که وحشت دیده دست از دامن صحرا نمی دارد</p></div></div>