---
title: >-
    غزل شمارهٔ ۴۳۴۵
---
# غزل شمارهٔ ۴۳۴۵

<div class="b" id="bn1"><div class="m1"><p>تر دامنیم آه غم آلود ندارد</p></div>
<div class="m2"><p>این چوب تر ازبی ثمری دود ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل بر سر آتش زهوا وهوس ماست</p></div>
<div class="m2"><p>این مجمره جز خامی ما عود ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از گریه ما نرم نگردید دل صبح</p></div>
<div class="m2"><p>در شوره زمین آب گهر سود ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غیر ازدل روشن که دلیلی است خدایی</p></div>
<div class="m2"><p>یک قبله نما کعبه مقصود ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ملک صباحت نتوان یافت ملاحت</p></div>
<div class="m2"><p>این لاله ستان داغ نمکسود ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از عشق دل خام شنیده است حدیثی</p></div>
<div class="m2"><p>آهن خبر از پنجه داود ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون حلقه کعبه است سزاوار پرستش</p></div>
<div class="m2"><p>چشمی که نگاه هوس آلود ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صاحب سخنی را که سخن سنج نباشد</p></div>
<div class="m2"><p>مانند از ایازی است که محمود ندارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون گوهر شب تاب چراغی که خدایی است</p></div>
<div class="m2"><p>هم خانه کند روشن و هم دود ندارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باجلوه خورشید چه حاجت به چراغ است</p></div>
<div class="m2"><p>دیوانه غم اختر مسعود ندارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون غنچه پیکان گذرانده است به سختی</p></div>
<div class="m2"><p>صائب خبری از دل خوشنود ندارد</p></div></div>