---
title: >-
    غزل شمارهٔ ۶۱۰۲
---
# غزل شمارهٔ ۶۱۰۲

<div class="b" id="bn1"><div class="m1"><p>آسمان ها را به چرخ آرد دل پر شور من</p></div>
<div class="m2"><p>می کند از جای خم را باده پر زور من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاکیان بی بصیرت را نمی آرد به شور</p></div>
<div class="m2"><p>ورنه می ریزد نمک در چشم اختر شور من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده رغبت به هر شهدی نمی سازم سیاه</p></div>
<div class="m2"><p>بر شکرخند سلیمان است چشم مور من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حرف حق را بر زمین انداختن بی حرمتی است</p></div>
<div class="m2"><p>از سریر دار منبر می کند منصور من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از رگ خامی نباشد میوه من ریشه دار</p></div>
<div class="m2"><p>نشأه می می دهد در غورگی انگور من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود کوه بیستون فرهاد را گر سنگ زور</p></div>
<div class="m2"><p>از دل سنگین خوبان است سنگ زور من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست غیر از نیش رزق من ز کافر نعمتان</p></div>
<div class="m2"><p>شش جهت هر چند شد پر شهد از زنبور من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر چه شد صحن زمین از کاسه ام چینی نگار</p></div>
<div class="m2"><p>باده از جام سفالین می خورد فغفور من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از جواهر سرمه الماس روشن گشته است</p></div>
<div class="m2"><p>کی به مرهم چشم می سازد سیه ناسور من؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کی به درمان تن دهد آن کس که ذوق درد یافت؟</p></div>
<div class="m2"><p>دست از دست مسیحا می کشد رنجور من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این نمک کز شورش عالم به زخم من رسید</p></div>
<div class="m2"><p>می شود صبح قیامت مرهم کافور من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز اختر طالع چه بگشاید، که خورشید منیر</p></div>
<div class="m2"><p>خال روی زنگیان شد از شب دیجور من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از تب سوزان دل شبها چراغم روشن است</p></div>
<div class="m2"><p>نیست حاجت شمع دیگر بر سر رنجور من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا به جمع مال حرص اغنیا را دیده است</p></div>
<div class="m2"><p>می کشد، گر دانه ای دارد، به خرمن مور من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جلوه برق تجلی را مکان خاص نیست</p></div>
<div class="m2"><p>از دل سنگین خوبان است کوه طور من</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر در احیای سخن کردم قیامت دور نیست</p></div>
<div class="m2"><p>کز صریر خامه خود بود صائب صور من</p></div></div>