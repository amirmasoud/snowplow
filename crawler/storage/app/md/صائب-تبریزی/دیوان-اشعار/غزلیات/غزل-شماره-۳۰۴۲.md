---
title: >-
    غزل شمارهٔ ۳۰۴۲
---
# غزل شمارهٔ ۳۰۴۲

<div class="b" id="bn1"><div class="m1"><p>زرخسار تو رنگ از گلشن ایجاد می خیزد</p></div>
<div class="m2"><p>زرفتار تو از آب روان فریاد می خیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هر گلشن که با آن قد رعنا جلوه گر گردی</p></div>
<div class="m2"><p>به تعظیم تو سرو از جای خود آزاد می خیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز آب آیینه روشن پذیرد زنگ و شیرین را</p></div>
<div class="m2"><p>زدل زنگار از تردستی فرهاد می خیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کجا خون می تواند شست رنگ از غنچه پیکان؟</p></div>
<div class="m2"><p>به می کی گرد کلفت از دل تا شاد می خیزد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چون کاسه خالی نیستند از مغز این سرها</p></div>
<div class="m2"><p>چرا انگشت بر هر لب زنی فریاد می خیزد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کند از علم رسمی پاک، دل را ساده لوحیها</p></div>
<div class="m2"><p>به صیقل جوهر از آیینه فولاد می خیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرا صائب به هم دارند غیرت کشتگان او؟</p></div>
<div class="m2"><p>رقم یکدست اگر از خامه فولاد می خیزد</p></div></div>