---
title: >-
    غزل شمارهٔ ۵۷۹۱
---
# غزل شمارهٔ ۵۷۹۱

<div class="b" id="bn1"><div class="m1"><p>تا همچو لعل، رنگ به رخسار داشتم</p></div>
<div class="m2"><p>خون در دل از شکست خریدار داشتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز قرین نگشت به هم قول و فعل من</p></div>
<div class="m2"><p>کردار را همیشه به گفتار داشتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا با خدا افتاد مرا کار، به شدم</p></div>
<div class="m2"><p>شربت نداشتم چو پرستار داشتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا چون حباب چشم گشودم ز یکدگر</p></div>
<div class="m2"><p>سر در کنار قلزم خونخوار داشتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگز دلم ز فکر غزالان تهی نبود</p></div>
<div class="m2"><p>دایم درین خرابه دو بیمار داشتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون شبنم از تجلی خورشید محو شد</p></div>
<div class="m2"><p>چشم تری که از غم گلزار داشتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرگز نداشت میل، ترازوی مشربم</p></div>
<div class="m2"><p>دایم به دست سبحه و ز نار داشتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون زلف، تار و پود حواسم نبود جمع</p></div>
<div class="m2"><p>تا فکر جامه و غم دستار داشتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فریاد من ز قحط هم آواز پست شد</p></div>
<div class="m2"><p>کارم بلند بود چو همکار داشتم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>داغ ترا به غیر نمودم ز سادگی</p></div>
<div class="m2"><p>آیینه پیش صورت دیوار داشتم !</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرگز نصیب گوشه نشینان نمی شود</p></div>
<div class="m2"><p>آن خلوتی که بر سر بازار داشتم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صائب هزار شکر که بر دل گذاشتم</p></div>
<div class="m2"><p>دستی که بر سر از غم دلدار داشتم</p></div></div>