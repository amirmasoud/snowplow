---
title: >-
    غزل شمارهٔ ۲۰۲۵
---
# غزل شمارهٔ ۲۰۲۵

<div class="b" id="bn1"><div class="m1"><p>در حفظ جسم این همه فکر محال چیست؟</p></div>
<div class="m2"><p>غیر از شکست، عاقبت این سفال چیست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمر دوباره مهر ز صبح از زوال یافت</p></div>
<div class="m2"><p>لرزیدن تو این همه بهر زوال چیست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آیینه بی مثال نماند چو باصفاست</p></div>
<div class="m2"><p>ای جان، پی خرابی جسم این ملال چیست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در زیر آسمان چه بود جز مثال چند؟</p></div>
<div class="m2"><p>در پرده های خواب بغیر از خیال چیست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>انگار عید آمد و نوروز هم رسید</p></div>
<div class="m2"><p>جز طی عمر در گره ماه و سال چیست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عزم درست، کار پر و بال می کند</p></div>
<div class="m2"><p>ای مرغ پر شکسته تمنای بال چیست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آب گهر برای گهر ترجمان بس است</p></div>
<div class="m2"><p>گر در تو هست حالتی، اظهار حال چیست؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن شاخ گل اگر نگذشته است از چمن</p></div>
<div class="m2"><p>بر جبهه گل این عرق انفعال چیست؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از قیل و قال چون نشود کس ز اهل حال</p></div>
<div class="m2"><p>صائب بگو که حاصل این قیل و قال چیست؟</p></div></div>