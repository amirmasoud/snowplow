---
title: >-
    غزل شمارهٔ ۱۸۶۳
---
# غزل شمارهٔ ۱۸۶۳

<div class="b" id="bn1"><div class="m1"><p>فتح و ظفر ز خودشکنی زیر دست ماست</p></div>
<div class="m2"><p>چون زلف و خط، درستی ما در شکست ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آشوب عالمیم ز هر مصرعی چو زلف</p></div>
<div class="m2"><p>سر رشته تپیدن دلها به دست ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باطل حجاب دیده حق بین نمی شود</p></div>
<div class="m2"><p>دنیا بهشت در نظر حق پرست ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خمخانه شد تهی و ندادیم نم برون</p></div>
<div class="m2"><p>منصور، داغ حوصله دیرمست ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گنجینه دار گوهر دریای رحمتیم</p></div>
<div class="m2"><p>چون ابر، چشم پاک صدفها به دست ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون توبه بهار، درین سبز انجمن</p></div>
<div class="m2"><p>صائب به هر که می نگری در شکست ماست</p></div></div>