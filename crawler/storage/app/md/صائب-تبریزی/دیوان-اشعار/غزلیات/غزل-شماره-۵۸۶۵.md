---
title: >-
    غزل شمارهٔ ۵۸۶۵
---
# غزل شمارهٔ ۵۸۶۵

<div class="b" id="bn1"><div class="m1"><p>تا واله نظاره آن ماهپاره ایم</p></div>
<div class="m2"><p>از خود پیاده ایم و به گردون سواره ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیر وجود ما نتوان کرد سالها</p></div>
<div class="m2"><p>هر چند قطره ایم ولی بیکناره ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هستی ما و نیستی ما برابرست</p></div>
<div class="m2"><p>محو فروغ مهر چو نور ستاره ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در محفلی که شعله ندارد زبان لاف</p></div>
<div class="m2"><p>ماگرم خودنمایی خود چون شراره ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نتوان به ریگ بادیه ما را شمار کرد</p></div>
<div class="m2"><p>چون درد و داغ اهل هنر بی شماره ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاک اوفتاده نیست اگر ما فتاده ایم</p></div>
<div class="m2"><p>گردون پیاده است اگر ما سواره ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آسیب ما به سوخته جانان نمی رسد</p></div>
<div class="m2"><p>هر چند آتشیم ولی بی شراره ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هشیار از تپانچه محشر نمی شویم</p></div>
<div class="m2"><p>ما این چنین که از می غفلت گذاره ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا کی ز جوی شیر و ز جنت سخن کنی؟</p></div>
<div class="m2"><p>ای واعظ فسرده، نه ما شیر خواره ایم!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از چاره بی نیاز بود درد و داغ عشق</p></div>
<div class="m2"><p>ما بهر چشم زخم، طلبکار چاره ایم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ماند چسان درست دل چون کتان ما؟</p></div>
<div class="m2"><p>آیینه دان جلوه آن ماهپاره ایم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هرگز ز کار خود گرهی وا نکرده ایم</p></div>
<div class="m2"><p>با آن که دستگیرتر از استخاره ایم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روز جزا که پرده بر افتد ز کار ما</p></div>
<div class="m2"><p>روشن شود به بی بصران ما چه کاره ایم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این آن غزل که مولوی روم گفته است</p></div>
<div class="m2"><p>در شکر همچو چشمه و در صبر خاره ایم</p></div></div>