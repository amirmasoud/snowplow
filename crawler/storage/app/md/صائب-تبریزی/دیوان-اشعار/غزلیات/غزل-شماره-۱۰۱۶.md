---
title: >-
    غزل شمارهٔ ۱۰۱۶
---
# غزل شمارهٔ ۱۰۱۶

<div class="b" id="bn1"><div class="m1"><p>هر که شد با درد قانع از مداوا فارغ است</p></div>
<div class="m2"><p>نرگس بیمار از ناز مسیحا فارغ است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طفل طبعان را دل از بهر تماشا می دود</p></div>
<div class="m2"><p>خو به عزلت کرده از سیر و تماشا فارغ است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خارخار آرزو در سینه عشاق نیست</p></div>
<div class="m2"><p>هر که واصل شد به مطلب، از تمنا فارغ است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست با خورشید تابان حاجت شمع و چراغ</p></div>
<div class="m2"><p>هر که را دل روشن است، از چشم بینا فارغ است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سیرچشمی می کند دل را ز دنیا بی نیاز</p></div>
<div class="m2"><p>گوهر قانع ز روی تلخ دریا فارغ است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نسبت عارف به خاک و مسند دولت یکی است</p></div>
<div class="m2"><p>از تکلف آفتاب عالم آرا فارغ است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست از خواب پریشان چشم بسمل را خبر</p></div>
<div class="m2"><p>محو عشق، از دیدن اوضاع دنیا فارغ است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عالم سرگشتگی دارالامان رهروست</p></div>
<div class="m2"><p>گردباد از سنگ راه و خار صحرا فارغ است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ذوق کار عشق، دارد جنگ با آسودگی</p></div>
<div class="m2"><p>کوهکن از اهتمام کارفرما فارغ است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سنگ بر دریا زدن، بازوی خود رنجاندن است</p></div>
<div class="m2"><p>از غم عالم دل خوش مشرب ما فارغ است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ما به خود صائب ز نادانی بساطی چیده ایم</p></div>
<div class="m2"><p>ورنه عشق از نیستی و هستی ما فارغ است</p></div></div>