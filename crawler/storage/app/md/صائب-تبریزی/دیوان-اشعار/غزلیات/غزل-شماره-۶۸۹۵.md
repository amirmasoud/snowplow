---
title: >-
    غزل شمارهٔ ۶۸۹۵
---
# غزل شمارهٔ ۶۸۹۵

<div class="b" id="bn1"><div class="m1"><p>از مردمان اگر چه کناری گرفته ای</p></div>
<div class="m2"><p>این گوشه را برای شکاری گرفته ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر هر چه جز خدای دل خویش بسته ای</p></div>
<div class="m2"><p>آیینه دام کرده غباری گرفته ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قانع به رنگ و بو شده ای همچو شاخ گل</p></div>
<div class="m2"><p>دستی دراز کرده نگاری گرفته ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در زیر برگ سرمکش از تیغ آفتاب</p></div>
<div class="m2"><p>بعد از هزار سال که باری گرفته ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون گل ترا به آتش سوزان شود دلیل</p></div>
<div class="m2"><p>از نقد عمر اگر نه شماری گرفته ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قانع چو سرو و بید به برگ از ثمر مشو</p></div>
<div class="m2"><p>این یک نفس که رنگ بهاری گرفته ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صبح امید درشکن آستین توست</p></div>
<div class="m2"><p>گر زان که دامن شب تاری گرفته ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در هر گشودن نظر و بستن نظر</p></div>
<div class="m2"><p>ملکی گشاده ای و حصاری گرفته ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زین دعوی بلند که با خلق می کنی</p></div>
<div class="m2"><p>از بهر خود تهیه داری گرفته ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از جهل کرده ای دل خود زنده زیر خاک</p></div>
<div class="m2"><p>بر دل اگر ز کینه غباری گرفته ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ماهی است پیش راه تو در ظلمت فنا</p></div>
<div class="m2"><p>شمعی اگر به راهگذاری گرفته ای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خواهد فتاد دامن منزل به دست تو</p></div>
<div class="m2"><p>صائب اگر رکاب سواری گرفته ای</p></div></div>