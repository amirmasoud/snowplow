---
title: >-
    غزل شمارهٔ ۵۵۳۸
---
# غزل شمارهٔ ۵۵۳۸

<div class="b" id="bn1"><div class="m1"><p>به قلب عشق می‌تازد دل زاری که من دارم</p></div>
<div class="m2"><p>زبان‌بازی به آتش می‌کند خاری که من دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که آرد ریشه کفر از دل سنگین من بیرون</p></div>
<div class="m2"><p>که محکم چون سلیمانی است ز ناری که من دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندانم سنگ از دست کدامین طفل بستانم</p></div>
<div class="m2"><p>که دارد در جنون آدینه‌بازاری که من دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز صد دامن گل بی‌خار در چشم بود خوش‌تر</p></div>
<div class="m2"><p>ز روی نو خط او در جگرخاری که من دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خورد چون شربت عناب خود بی‌گناهان را</p></div>
<div class="m2"><p>ز بی‌باکی نظر بر چشم بیماری که من دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به سیم قلب از اخوان نگیرد ماه کنعان را</p></div>
<div class="m2"><p>که دارد از عزیزان این خریداری که من دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نفس در سینه خورشید عالم‌تاب می‌سوزد</p></div>
<div class="m2"><p>درین گلشن چو شبنم چشم بیداری که من دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کند گر در نوازش کارفرما کو تهی با من</p></div>
<div class="m2"><p>ز ذوق کار مزدش می‌رسد کاری که من دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سبک کرده است در میزان من سد سکندر را</p></div>
<div class="m2"><p>به پیش روی خود از جسم دیواری که من دارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تماشای بهشت از خانه‌ام بیرون نمی‌آرد</p></div>
<div class="m2"><p>ز داغ آتشین در سینه گلزاری که من دارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به درمان می‌توان تخفیف دادن درد را صائب</p></div>
<div class="m2"><p>عجب دردی است بی‌درمان، پرستاری که من دارم</p></div></div>