---
title: >-
    غزل شمارهٔ ۵۴۷۵
---
# غزل شمارهٔ ۵۴۷۵

<div class="b" id="bn1"><div class="m1"><p>ما کجا دست آشنا با مهره گل می‌کنیم</p></div>
<div class="m2"><p>مشورت چون غنچه با سی پاره دل می‌کنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بحر پرشور حوادث در کف ما عاجزست</p></div>
<div class="m2"><p>موج را از لنگر تسلیم ساحل می‌کنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو مژگان روز و شب در پیش چشم استاده است</p></div>
<div class="m2"><p>از خیالش خویش را چندان که غافل می‌کنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داغ عشقی را که در صد پرده می‌باید نهان</p></div>
<div class="m2"><p>لاله دامان دشت و شمع محفل می‌کنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناخن فولاد دارد منت روشنگران</p></div>
<div class="m2"><p>تیغ جان را روشن از خاکستر دل می‌کنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دولتی کز سایه خیزد تیرگی بار آورد</p></div>
<div class="m2"><p>صفحه بال هما را فرد باطل می‌کنیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کی به دست سنبل فردوس دل خواهیم داد</p></div>
<div class="m2"><p>ما که در سودای زلف یار دل‌دل می‌کنیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شبنمیم اما ز بس گرد حوادث خورده‌ایم</p></div>
<div class="m2"><p>چشمه خورشید عالم‌تاب را گل می‌کنیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فکر ما صائب سحاب نوبهار رحمت است</p></div>
<div class="m2"><p>ما زمین شور را یک لحظه قابل می‌کنیم</p></div></div>