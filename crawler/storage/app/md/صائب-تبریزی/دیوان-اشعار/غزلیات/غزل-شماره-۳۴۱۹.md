---
title: >-
    غزل شمارهٔ ۳۴۱۹
---
# غزل شمارهٔ ۳۴۱۹

<div class="b" id="bn1"><div class="m1"><p>دل به مطلب اگر از راه تپیدن نرسد</p></div>
<div class="m2"><p>گو مکن سعی که هرگز به دویدن نرسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهترین پایه صاحب نظران حیرانی است</p></div>
<div class="m2"><p>دیده هرگز به مقامی ز پریدن نرسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پای فهمیده در آن سلسله زلف گذار</p></div>
<div class="m2"><p>که در آن کوچه به خورشید دویدن نرسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دل پخته مزن لاف که این میوه خام</p></div>
<div class="m2"><p>نرسد تا به لبت جان، به رسیدن نرسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وحشتی قسمت مجنون من از عشق شده است</p></div>
<div class="m2"><p>که به من هیچ غزالی به رمیدن نرسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرچه چون لاله نگونسار بود کاسه من</p></div>
<div class="m2"><p>از دل سوخته خونم به چکیدن نرسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نرسد زان به تو از چشم بدان آسیبی</p></div>
<div class="m2"><p>کز لطافت گل روی تو به دیدن نرسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه چنان رفته ام از خود که دگر بازآیم</p></div>
<div class="m2"><p>دامن رفته ز دستم به کشیدن نرسد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می دود در پی آن چشم دل خام طمع</p></div>
<div class="m2"><p>طفل هرچند به آهو به دویدن نرسد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از لطافت به تماشایی آن سیب ذقن</p></div>
<div class="m2"><p>بجز از دست و لب خویش گزیدن نرسد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنچنان محو تو شد دیده نظار گیان</p></div>
<div class="m2"><p>که به گلهای چمن نوبت چیدن نرسد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل سودا زده و حرف شکایت، هیهات</p></div>
<div class="m2"><p>دانه سوخته هرگز به دمیدن نرسد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مهربان گر به یتیمان نشود دایه لطف</p></div>
<div class="m2"><p>هوش اطفال به انگشت مکیدن نرسد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چه کند با دل سنگین طبیبان صائب؟</p></div>
<div class="m2"><p>ناتوانی که فغانش به شنیدن نرسد</p></div></div>