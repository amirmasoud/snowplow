---
title: >-
    غزل شمارهٔ ۲۱۴۳
---
# غزل شمارهٔ ۲۱۴۳

<div class="b" id="bn1"><div class="m1"><p>چون آینه هر دل که ز روشن گهران است</p></div>
<div class="m2"><p>در نقش بد و نیک به حیرت نگران است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غیر از نظر پاک بر آن آینه رخسار</p></div>
<div class="m2"><p>گر آب حیات است، که چون زنگ گران است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشمی که ز بی شرمی ازو آب نرفته است</p></div>
<div class="m2"><p>چون دیده نرگس به ته پا نگران است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارد دلی آسوده تر از نقطه مرکز</p></div>
<div class="m2"><p>چون دایره هر کس که ز بی پا و سران است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سهل است اگر گوهر ما را نخریدند</p></div>
<div class="m2"><p>یوسف به زر قلب درین شهر گران است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با قامت او هر که به سروست نظرباز</p></div>
<div class="m2"><p>چون فاخته سر حلقه کوته نظران است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این راز که چون خرده گل در جگر ماست</p></div>
<div class="m2"><p>فریاد که چون بوی گل از پرده دران است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>انصاف نمانده است درین موی میانان</p></div>
<div class="m2"><p>کوه غم ما فربه ازین خوش کمران نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی خون جگر، آبی اگر هست درین دور</p></div>
<div class="m2"><p>در سینه سنگ و گره بدگهران است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سر حلقه بالغ نظران است چو صائب</p></div>
<div class="m2"><p>چشمی که نظرباز به نوخط پسران است</p></div></div>