---
title: >-
    غزل شمارهٔ ۲۷۷۷
---
# غزل شمارهٔ ۲۷۷۷

<div class="b" id="bn1"><div class="m1"><p>نیست از خورشید و ماه این گنبد گردان سفید</p></div>
<div class="m2"><p>ز استخوان بیگناهان است این زندان سفید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیر آه از سینه ام بیرنگ می آید برون</p></div>
<div class="m2"><p>وای بر صیدی کز او آید برون پیکان سفید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یوسف من زان همه قصر و سرای دلفریب</p></div>
<div class="m2"><p>خانه چشمی بجا مانده است در کنعان سفید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قطع پیوند دل از آهو نگاهان مشکل است</p></div>
<div class="m2"><p>از جدایی نافه را شد موی سر زینسان سفید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نامه چون برف می خواهند در دیوان حشر</p></div>
<div class="m2"><p>تو در آن فکری که باشد سفره ات را نان سفید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خانه پردازی چراغ خانه گورست و تو</p></div>
<div class="m2"><p>می کنی از ساده لوحی خانه و ایوان سفید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پاک طینت می رساند فیض بعد از سوختن</p></div>
<div class="m2"><p>عود خاکستر چو گردد می کند دندان سفید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صبح پیری در رکاب پرتو منت بود</p></div>
<div class="m2"><p>زان به یک شب گشت ابروی مه تابان سفید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما سبکروحان مشرب را به دست کم مگیر</p></div>
<div class="m2"><p>کز کف بی مغز باشد چهره عمان سفید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پاک کن از غیبت مردم دهان خویش را</p></div>
<div class="m2"><p>ای که از مسواک هر دم می کنی دندان سفید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ماهرویان بس که در هر کوچه جولان می کنند</p></div>
<div class="m2"><p>ماه نتواند شدن صائب در اصفاهان سفید</p></div></div>