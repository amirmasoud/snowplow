---
title: >-
    غزل شمارهٔ ۳۸۶۸
---
# غزل شمارهٔ ۳۸۶۸

<div class="b" id="bn1"><div class="m1"><p>نشاط زنده دلان پایدار می باشد</p></div>
<div class="m2"><p>درین پیاله می بی خمار می باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آفتاب جهانتاب محوگردیدن</p></div>
<div class="m2"><p>نصیب شبنم شب زنده دار می باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل گرفته ز زخم زبان نیندیشد</p></div>
<div class="m2"><p>گشادآبله درخارزار می باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز درد وداغ ندارندعاشقان سیری</p></div>
<div class="m2"><p>زمین سوخته عاشق شرار می باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حذر زآه جگرسوز بینوایان کن</p></div>
<div class="m2"><p>که تیغ خشک لبان آبدار می باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بخت تیره دل سخت نرم می گردد</p></div>
<div class="m2"><p>که شمع در دل شب اشکبار می باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نتیجه دل سخت است تنگ خلقیها</p></div>
<div class="m2"><p>پلنگ خشم درین کوهسار می باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز مکر نفس بیندیش در کهنسالی</p></div>
<div class="m2"><p>که زهردربن دندان مار می باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو عیسی آن که کند نفس را عنانداری</p></div>
<div class="m2"><p>به دوش چرخ سبکرو سوار می باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چگونه سرو نباشد خجل ز دعوی خویش</p></div>
<div class="m2"><p>که برگ بر دل آزاده بار می باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز پرده حسن همان فیض خویش می بخشد</p></div>
<div class="m2"><p>نقاب چهره عنبر بهار می باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بساز با دل پرخون درین جهان صائب</p></div>
<div class="m2"><p>که نافه را نفس مشکبار می باشد</p></div></div>