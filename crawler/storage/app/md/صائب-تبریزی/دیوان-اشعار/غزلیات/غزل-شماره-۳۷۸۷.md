---
title: >-
    غزل شمارهٔ ۳۷۸۷
---
# غزل شمارهٔ ۳۷۸۷

<div class="b" id="bn1"><div class="m1"><p>به خاک راه تو هرکس که جبهه سایی کرد</p></div>
<div class="m2"><p>تمام عمر چو خورشید خودنمایی کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فغان که ساغر زرین بی نیازی را</p></div>
<div class="m2"><p>گرسنه چشمی ما کاسه گدایی کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خدنگ آه جگردوز را ز بیدردی</p></div>
<div class="m2"><p>هواپرستی ما ناوک هوایی کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به مومیایی مردم چه حاجت است مرا؟</p></div>
<div class="m2"><p>که استخوان مرا سنگ مومیایی کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازان ز گریه نشد خشک شمع را مژگان</p></div>
<div class="m2"><p>که روشنایی خود صرف آشنایی کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهوش باش دلی را به سهو نخراشی</p></div>
<div class="m2"><p>به ناخنی که توانی گرهگشایی کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا به آتش سوزنده رحم می آید</p></div>
<div class="m2"><p>که زندگانی خود صرف ژاژخایی کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به رنگ و بوی جهان دل گذاشتن ستم است</p></div>
<div class="m2"><p>چه خوب کرد که شبنم ز گل جدایی کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هنوز خط تو صورت نبسته بود از غیب</p></div>
<div class="m2"><p>که درد صفحه روی مرا حنایی کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خوش است گاه به عشاق خویش دل دادن</p></div>
<div class="m2"><p>نمی توان همه عمر دلربایی کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نداد سر به بیابان درین بهار مرا</p></div>
<div class="m2"><p>نسیم زلف تو بسیار نارسایی کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز رشک شمع دل خویش می خورم صائب</p></div>
<div class="m2"><p>که جسم تیره خود صرف روشنایی کرد</p></div></div>