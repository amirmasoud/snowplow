---
title: >-
    غزل شمارهٔ ۵۸۳۳
---
# غزل شمارهٔ ۵۸۳۳

<div class="b" id="bn1"><div class="m1"><p>از دست رفت فرصت و ما پا شکسته ایم</p></div>
<div class="m2"><p>در راه آرمیده چو منزل نشسته ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون شیشه نیمه گشت کمر بسته می شود</p></div>
<div class="m2"><p>شد عمر ما تمام و میان را نبسته ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیلاب حادثات ز فریادیان ماست</p></div>
<div class="m2"><p>تا همچو کوه پای به دامن شکسته ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داریم فکر ریشه دواندن ز سادگی</p></div>
<div class="m2"><p>با آن که چون سپند بر آتش نشسته ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون تن دهیم روز قیامت به زندگی؟</p></div>
<div class="m2"><p>خون خورده تا ازین قفس تنگ جسته ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما را امید وصل شکر باغ دلگشاست</p></div>
<div class="m2"><p>در زیر پوست خنده زنان همچو پسته ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر چند خون شود به مقامی نمی رسد</p></div>
<div class="m2"><p>این شیشه ها که در ره دل ما شکسته ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>داغ است چرخ شیشه دل از جان سخت ما</p></div>
<div class="m2"><p>چون کوه زیر تیغ به تمکین نشسته ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب فضای سینه ما شیشه خانه ای است</p></div>
<div class="m2"><p>از بس که آرزو به دل خود شکسته ایم</p></div></div>