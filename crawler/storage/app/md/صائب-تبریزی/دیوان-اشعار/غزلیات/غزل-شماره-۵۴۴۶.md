---
title: >-
    غزل شمارهٔ ۵۴۴۶
---
# غزل شمارهٔ ۵۴۴۶

<div class="b" id="bn1"><div class="m1"><p>عشق را در تنگنای سینه پنهان کرده‌ایم</p></div>
<div class="m2"><p>شور محشر را حصاری در نمکدان کرده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در صفای سینه ما طوطیان را حرف نیست</p></div>
<div class="m2"><p>از تری‌های فلک آیینه پنهان کرده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سنگ طفلان را دهد گرد یتیمی خاکمال</p></div>
<div class="m2"><p>از سواد شهر تا رو در بیابان کرده‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست طول عمر را کیفیت عرض حیات</p></div>
<div class="m2"><p>ما به آب تلخ صلح از آب حیوان کرده‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا عزیزان جهان ما را فرامش کرده‌اند</p></div>
<div class="m2"><p>سجده‌های شکر پیش طاق نسیان کرده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مطلب ما ترک سر بر خویش آسان کردن است</p></div>
<div class="m2"><p>گر لبی چون پسته زیر پوست خندان کرده‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کشت ما را خوشه‌ای گر هست آه حسرت است</p></div>
<div class="m2"><p>در زمین شور تخم خود پریشان کرده‌ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در شبستان عدم صبح امید ما بس است</p></div>
<div class="m2"><p>آنچه از انفاس صرف آه و افغان کرده‌ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون به صید جغد چون دون همتان قانع شویم؟</p></div>
<div class="m2"><p>ما که خود را بر امید گنج ویران کرده‌ایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون سمندر صائب از اقبال عشق بی‌زوال</p></div>
<div class="m2"><p>آتش سوزنده را بر خود گلستان کرده‌ایم</p></div></div>