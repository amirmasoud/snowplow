---
title: >-
    غزل شمارهٔ ۶۱۶۲
---
# غزل شمارهٔ ۶۱۶۲

<div class="b" id="bn1"><div class="m1"><p>خال یا تخم امید عاشق شیداست این؟</p></div>
<div class="m2"><p>زلف یا شیراه جمعیت دلهاست این؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلفش از معموره دلها برآورده است گرد</p></div>
<div class="m2"><p>یا بهار بی خزان عنبر ساراست این؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فتنه روز قیامت در رکابش می رود</p></div>
<div class="m2"><p>رایت حسن بلند اقبال، یا بالاست این؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر سر خورشید را بیند به زیر پای خویش</p></div>
<div class="m2"><p>آب در چشمش نمی گردد، چه بی پرواست این؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست ممکن فکر زلفش را برآوردن ز دل</p></div>
<div class="m2"><p>می شود هر روز افزون، ریشه سوداست این</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خط که حسن دیگران را می شود فرمان عزل</p></div>
<div class="m2"><p>استمالت نامه آن حسن بی پرواست این</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از دمیدن های خط صائب ازو ایمن مشو</p></div>
<div class="m2"><p>جوهر بی رحمی شمشیر استغناست این</p></div></div>