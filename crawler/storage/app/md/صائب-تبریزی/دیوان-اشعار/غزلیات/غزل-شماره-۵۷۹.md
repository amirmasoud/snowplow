---
title: >-
    غزل شمارهٔ ۵۷۹
---
# غزل شمارهٔ ۵۷۹

<div class="b" id="bn1"><div class="m1"><p>به تیغ کج نشود راست هیچ کار اینجا</p></div>
<div class="m2"><p>دل دو نیم کند کار ذوالفقار اینجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز صدق، صبح نفس زد به آفتاب رسید</p></div>
<div class="m2"><p>به صدق دل، نفسی از جگر برآر اینجا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوشا گشاده جبینی که چون گل رعنا</p></div>
<div class="m2"><p>خزان خویشتن آمیخت با بهار اینجا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جمال شاهد مقصود چشم بر راه است</p></div>
<div class="m2"><p>بکوش و پاک کن آیینه از غبار اینجا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شوی ز نعمت الوان خلد کامروا</p></div>
<div class="m2"><p>به خون دل گذرانی اگر مدار اینجا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آن چمن گل بی خار، سینه چاک کسی است</p></div>
<div class="m2"><p>که ریخت گل به گریبان ز خار خار اینجا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چگونه مار نپیچد به گردنت فردا؟</p></div>
<div class="m2"><p>ترا که طول امل کرده در مهار اینجا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رهی دراز ترا پیش پا گذاشته اند</p></div>
<div class="m2"><p>مزن چو شعله نفس های بی شمار اینجا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز برگریز قیامت اگر خبر داری</p></div>
<div class="m2"><p>نهال خویش سبک کن ز برگ و بار اینجا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز تنگنای لحد می جهد برون چون تیر</p></div>
<div class="m2"><p>سبکروی که سبکبار شد ز بار اینجا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز آفتاب قیامت کباب تا نشوی</p></div>
<div class="m2"><p>ز دست جود به بی حاصلان ببار اینجا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چه پای در گل اندیشه مانده ای صائب؟</p></div>
<div class="m2"><p>ز تخم اشک، تو هم دانه ای بکار اینجا</p></div></div>