---
title: >-
    غزل شمارهٔ ۴۶۴۰
---
# غزل شمارهٔ ۴۶۴۰

<div class="b" id="bn1"><div class="m1"><p>زیرتیغ از مرغ بسمل پرفشانی یاد گیر</p></div>
<div class="m2"><p>از سبکروحان نصیحت بی گرانی یاد گیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمر فرصت درگذارووقت پرواز ست تنگ</p></div>
<div class="m2"><p>در حریم بیضه بال و پر فشانی یاد گیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای که تمکین یاد می گیری زجسم خاکسار</p></div>
<div class="m2"><p>از بهار عمر هم آتش عنانی یاد گیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شهپرپرواز سامان می دهد از برگ عیش</p></div>
<div class="m2"><p>شیوه رفتار از باد خزانی یادگیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست ممکن راست کردن چوبهای خشک را</p></div>
<div class="m2"><p>پند پیران را درایام جوانی یاد گیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مور رااز دست خود بخشد سلیمان پایتخت</p></div>
<div class="m2"><p>یا فرودستان طریق مهربانی یاد گیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر بود چون غنچه گل صد زبانت در دهان</p></div>
<div class="m2"><p>سرفروبردر گریبان ،بی زبانی یاد گیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در رگ زنار تاب ودر دل سبحه است تار</p></div>
<div class="m2"><p>از دل خوش مشرب ماخوش عنانی یادگیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر دل شب رانیفروزی به آه آتشین</p></div>
<div class="m2"><p>از فلک هر صبحدم اختر فشانی یاد گیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می دهددر پرده شب عمر جاویدان به خضر</p></div>
<div class="m2"><p>شرم همت رازآب زندگانی یادگیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از ته دل برتو گردشوار باشد دوستی</p></div>
<div class="m2"><p>از برای مصلحت لطف زبانی یاد گیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می فشاند گوهر شهواراز لب زیرتیغ</p></div>
<div class="m2"><p>از صدف صائب طریق زندگانی یادگیر</p></div></div>