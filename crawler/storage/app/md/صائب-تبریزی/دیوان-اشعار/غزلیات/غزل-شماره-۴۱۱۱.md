---
title: >-
    غزل شمارهٔ ۴۱۱۱
---
# غزل شمارهٔ ۴۱۱۱

<div class="b" id="bn1"><div class="m1"><p>از روی درد هر که ز دل آه می کشد</p></div>
<div class="m2"><p>بی چشم زخم یوسفی از چاه می کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی آه گرم نیست دل دردمند عشق</p></div>
<div class="m2"><p>شمعی که روشن است مدام آه می کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در زلف دود شعله حصاری نمی شود</p></div>
<div class="m2"><p>بیهوده ابر پرده در آن ماه می کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمع که دیده است سرانجام خامشی</p></div>
<div class="m2"><p>گردن در انتظار سحرگاه می کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بردوش خلق بار بود زندگانیش</p></div>
<div class="m2"><p>هر کس که بار خلق به اکراه می کشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا روی آتشین تو در بزم دیده است</p></div>
<div class="m2"><p>پیوسته شمع جای نفس آه می کشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شوخی که رم ز دیدن پنهان من کند</p></div>
<div class="m2"><p>با مدعی جناغ به دلخواه می کشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>افتاده جذبه طمع زرد روبلند</p></div>
<div class="m2"><p>این کهربا ز کاهکشان کاه می کشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از اشتیاق چهره چون آفتاب توست</p></div>
<div class="m2"><p>نیلی که آه من به رخ ماه می کشد</p></div></div>