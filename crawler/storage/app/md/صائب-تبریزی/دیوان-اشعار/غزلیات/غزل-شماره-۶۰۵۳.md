---
title: >-
    غزل شمارهٔ ۶۰۵۳
---
# غزل شمارهٔ ۶۰۵۳

<div class="b" id="bn1"><div class="m1"><p>زندگی بخشا! روان چند کس خواهی شدن؟</p></div>
<div class="m2"><p>کشته بسیارست، جان چند کس خواهی شدن؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد جگرگاه زمین از کشتگانت لاله زار</p></div>
<div class="m2"><p>مرهم داغ نهان چند کس خواهی شدن؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون کتان شد جامه جانها شق از مهتاب تو</p></div>
<div class="m2"><p>بخیه زخم کتان چند کس خواهی شدن؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از تو دارد هر سیه روزی تمنای چرغ</p></div>
<div class="m2"><p>شبچراغ دودمان چند کس خواهی شدن؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم بر راه تو دارد قاف تا قاف جهان</p></div>
<div class="m2"><p>ای پریرو، میهمان چند کس خواهی شدن؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از تماشایت جهانی قالب بی جان شده است</p></div>
<div class="m2"><p>تو به این تمکین روان چند کس خواهی شدن؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با چنان رویی کز او بی پرده گردد رازها</p></div>
<div class="m2"><p>پرده راز نهان چند کس خواهی شدن؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لازم افتاده است دل دادن به هر دل پاره ای</p></div>
<div class="m2"><p>تو به یک دل، دلستان چند کس خواهی شدن؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از تو آب و رنگ خواهد صد خزان بی بهار</p></div>
<div class="m2"><p>نوبهار بی خزان چند کس خواهی شدن؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی قراران تو بیرون از شمارند و حساب</p></div>
<div class="m2"><p>باعث آرام جان چند کس خواهی شدن؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من گرفتم سرمه سا گردید چشم پرفنت</p></div>
<div class="m2"><p>مانع آه و فغان چند کس خواهی شدن؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر کسی تنها ترا خواهد که باشی زان او</p></div>
<div class="m2"><p>تو به تنهایی ازان چند کس خواهی شدن؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این جواب آن غزل صائب که خسرو گفته است</p></div>
<div class="m2"><p>ای جهانی کشته، جان چند کس خواهی شدن؟</p></div></div>