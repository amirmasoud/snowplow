---
title: >-
    غزل شمارهٔ ۱۰۷۹
---
# غزل شمارهٔ ۱۰۷۹

<div class="b" id="bn1"><div class="m1"><p>نوبهار آیینه طبع سخنساز من است</p></div>
<div class="m2"><p>برگ گل چون عندلیبان پرده ساز من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناله مستانه من بیخودی می آورد</p></div>
<div class="m2"><p>هر که از خود می دود بیرون به آواز من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون صدف، آبی که دارد گوهر من در گره</p></div>
<div class="m2"><p>همچو سیل نوبهاران خانه پرداز من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خار صحرای علایق نیست دامنگیر من</p></div>
<div class="m2"><p>گردبادم، ریشه من بال پرواز من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون صدف نتوان به تیغ از هم جدا کردن لبم</p></div>
<div class="m2"><p>خون خود را می خورد آن کس که غماز من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از نظر بازی شود روشن دل تاریک من</p></div>
<div class="m2"><p>روزن غمخانه من دیده باز من است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از نگاهی می توان صائب مرا تسخیر کرد</p></div>
<div class="m2"><p>هر که را مژگان گیرایی است شهباز من است</p></div></div>