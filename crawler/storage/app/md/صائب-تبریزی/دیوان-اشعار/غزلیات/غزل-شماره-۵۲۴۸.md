---
title: >-
    غزل شمارهٔ ۵۲۴۸
---
# غزل شمارهٔ ۵۲۴۸

<div class="b" id="bn1"><div class="m1"><p>نیم ز پرسش محشر به هیچ باب خجل</p></div>
<div class="m2"><p>که خود حساب نمی گردد از حساب خجل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نکرد تربیت عشق در دلم تاثیر</p></div>
<div class="m2"><p>چو تخم سوخته گردیدم از سحاب خجل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنین که من خجل از سایلم ز بی برگی</p></div>
<div class="m2"><p>ز تشنگان نبود موجه سراب خجل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز سنگ ناوک ابرام بر نمی گردد</p></div>
<div class="m2"><p>گدا نمی شود از سختی جواب خجل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس ازتمام شدن ازچه روی می کاهد</p></div>
<div class="m2"><p>ز نور عاریه گر نیست ماهتاب خجل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دهد گشودن لب انفعال نادان را</p></div>
<div class="m2"><p>که هست خانه مفلس ز فتح باب خجل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز خط به چشم هوسناک شد جهان تاریک</p></div>
<div class="m2"><p>که کور فهم شد زود ازکتاب خجل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نظاره اش به نظر اشک گرم می آرد</p></div>
<div class="m2"><p>شد از عذار تو از بس که آفتاب خجل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جواب آن غزل حافظ است این صائب</p></div>
<div class="m2"><p>که کس مباد ز کردار ناصواب خجل</p></div></div>