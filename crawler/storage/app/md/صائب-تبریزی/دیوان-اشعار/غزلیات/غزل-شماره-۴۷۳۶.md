---
title: >-
    غزل شمارهٔ ۴۷۳۶
---
# غزل شمارهٔ ۴۷۳۶

<div class="b" id="bn1"><div class="m1"><p>از ره مرو به جلوه ناپایدار عمر</p></div>
<div class="m2"><p>کز موجه سراب بود پود و تار عمر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرصت نمی دهد که بشویم ز دیده خواب</p></div>
<div class="m2"><p>از بس که تند می گذرد جویبار عمر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برگ سفر بساز که با دست رعشه دار</p></div>
<div class="m2"><p>نتوان گرفت دامن باد بهار عمر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کمتر بود ز صحبت برق و گیاه خشک</p></div>
<div class="m2"><p>در جسم زار جلوه ناپایدار عمر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر چهره من آنچه سفیدی کند نه موست</p></div>
<div class="m2"><p>گردی است مانده بررخم از رهگذار عمر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آبی که ماند درته جو سبز می شود</p></div>
<div class="m2"><p>چون خضر زینهار مکن اختیار عمر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زنگ ندامتی است که روزم سیاه ازوست</p></div>
<div class="m2"><p>در دست من ز نقره کامل عیار عمر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دست از ثمر بشوی که هرگز نرسته است</p></div>
<div class="m2"><p>جز آه سرد، سنبلی از جویبار عمر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فهمیده خرج کن نفس خود که بسته است</p></div>
<div class="m2"><p>در رشته نفس گهر آبدار عمر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مشکل که سر برآورد از خاک روز حشر</p></div>
<div class="m2"><p>آن را که کرد بی ثمری شرمسار عمر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زهری است زهر مرگ که شیرین نمی شود</p></div>
<div class="m2"><p>هرچند تلخ می گذرد روزگار عمر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>روز مبارکی است که با عشق بوده ام</p></div>
<div class="m2"><p>روز گذشته ای که بود در شمار عمر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اشگ ندامتی است چو باران نوبهار</p></div>
<div class="m2"><p>چیزی که مانده است به من از بهار عمر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا چند بر صحیفه ایام چون قلم</p></div>
<div class="m2"><p>صائب به گفتگو گذرانی مدار عمر</p></div></div>