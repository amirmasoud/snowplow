---
title: >-
    غزل شمارهٔ ۶۱۱۶
---
# غزل شمارهٔ ۶۱۱۶

<div class="b" id="bn1"><div class="m1"><p>نیست جز لخت جگر چیزی دگر بر خوان من</p></div>
<div class="m2"><p>از پشیمانی دل خود می خورد مهمان من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مصیبت خانه ام گرد تعلق فرش نیست</p></div>
<div class="m2"><p>سیل خجلت می برد از خانه ویران من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از تنور خاک، نان من فطیر آمد برون</p></div>
<div class="m2"><p>از تنور آسمان تا چون برآید نان من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قطع پیوند تعلق کرده ام زین خاکدان</p></div>
<div class="m2"><p>داغ دارد خار را کوتاهی دامان من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می کند با آستین جوهر ز روی تیغ پاک</p></div>
<div class="m2"><p>آن که می چیند به دامن اشک از مژگان من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گریه من بحر را در حقه گرداب کرد</p></div>
<div class="m2"><p>کیست مرجان تا زند سرپنجه با مژگان من؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از تنور هر حبابی سر کشد طوفان نوح</p></div>
<div class="m2"><p>چون به دریا رو نهد چشم محیط افشان من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نکهت زلف تو راه شش جهت را بسته است</p></div>
<div class="m2"><p>از کدامین ره به هوش آید دل حیران من؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در سر شوریده من عقل شد سودای عشق</p></div>
<div class="m2"><p>دیو یوسف می شود در پله میزان من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صائب از بس شور معنی هر طرف انگیخته است</p></div>
<div class="m2"><p>یادی از دیوان محشر می دهد دیوان من</p></div></div>