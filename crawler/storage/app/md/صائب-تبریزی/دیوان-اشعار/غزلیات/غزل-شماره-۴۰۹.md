---
title: >-
    غزل شمارهٔ ۴۰۹
---
# غزل شمارهٔ ۴۰۹

<div class="b" id="bn1"><div class="m1"><p>ز منع افزون شود شوق گرستن بی قراران را</p></div>
<div class="m2"><p>که افزاید رسایی از گره در رشته باران را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز طوفان پنجه مرجان نگردد بحر را مانع</p></div>
<div class="m2"><p>کجا ساکن کند دست نوازش بی قراران را؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به قدر سعی، از مقصود هر کس بهره ای دارد</p></div>
<div class="m2"><p>که منزل پیش پای خود بود، دامن سواران را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه پروای دل صد پاره دارد تیغ سیرابش؟</p></div>
<div class="m2"><p>که هر برگی زبان شکر باشد نوبهاران را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به ابر امید دارد دانه تا زیر زمین باشد</p></div>
<div class="m2"><p>نظر بر عالم بالاست دایم خاکساران را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به خورشید درخشان، نسبت همت بود تهمت</p></div>
<div class="m2"><p>که ریزش اختیاری نیست دست رعشه داران را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>علایق را بود کوتاه، دست از دامن همت</p></div>
<div class="m2"><p>ز گرد ره نباشد زحمتی گردون سواران را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به خط امیدها دارد دل بی طاقت عاشق</p></div>
<div class="m2"><p>که وقت شام، صبح عید باشد روزه داران را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نسازد مایه داران مروت را زیان غمگین</p></div>
<div class="m2"><p>نشاط باخت بیش از برد باشد خوش قماران را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به عقل شیشه دل باشد گران حرف ملامتگر</p></div>
<div class="m2"><p>سر دیوانه گلریزان شمارد سنگباران را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نمی پیچد سر از سنگ ملامت هر که مجنون شد</p></div>
<div class="m2"><p>که سازد سرخ رو سنگ محک کامل عیاران را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز خوشوقتی گوارا می شود هر ناخوشی صائب</p></div>
<div class="m2"><p>که چشم شور کوکب نقل باشد میگساران را</p></div></div>