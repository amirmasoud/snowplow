---
title: >-
    غزل شمارهٔ ۴۷۲۵
---
# غزل شمارهٔ ۴۷۲۵

<div class="b" id="bn1"><div class="m1"><p>از سعی، کار عشق شود خام بیشتر</p></div>
<div class="m2"><p>پیچد به مرغ بال فشان دام بیشتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خط فزود شوخی آن چشم پر خمار</p></div>
<div class="m2"><p>درنوبهار دور کند جام بیشتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا بر محک زدم می شیرین و تلخ را</p></div>
<div class="m2"><p>دارم ز بوسه رغبت دشنام بیشتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سنگها عقیق به همواریی که داشت</p></div>
<div class="m2"><p>تحصیل نام کرد درایام بیشتر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیران تلاش رزق فزون از جوان کنند</p></div>
<div class="m2"><p>حرص گدا شود صرف شام بیشتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از اوج اعتبار نیفتد اهل خلق</p></div>
<div class="m2"><p>مست غرور افتد ازین بام بیشتر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>موی سفید مرهم کافوری دل است</p></div>
<div class="m2"><p>بیمار را سحر بود آرام بیشتر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از زاهدان سرد نفس پختگی مجوی</p></div>
<div class="m2"><p>در سردسیر میوه بود خام بیشتر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مانند آب چشمه ز کاوش فزون شود</p></div>
<div class="m2"><p>چندان که می خوری غم ایام بیشتر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از ره مرو به ظاهر هموار مردمان</p></div>
<div class="m2"><p>در خاکهای نرم بود دام بیشتر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب به گریه کوش که از دیده سفید</p></div>
<div class="m2"><p>آن کعبه راست جامه احرام بیشتر</p></div></div>