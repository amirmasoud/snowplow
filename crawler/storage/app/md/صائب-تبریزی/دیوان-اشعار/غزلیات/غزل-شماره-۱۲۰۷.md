---
title: >-
    غزل شمارهٔ ۱۲۰۷
---
# غزل شمارهٔ ۱۲۰۷

<div class="b" id="bn1"><div class="m1"><p>دل به نور شمع نتوان در گذار باد بست</p></div>
<div class="m2"><p>ساده لوح آن کس که دل بر عمر بی بنیاد بست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می شود نام بزرگان از هنرمندان بلند</p></div>
<div class="m2"><p>طرف شهرت بیستون از تیشه فرهاد بست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رو به هر مطلب که آرد، می زند نقش مراد</p></div>
<div class="m2"><p>صفحه رویی که نقش از سیلی استاد بست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرده دار دیده عاشق حجاب او بس است</p></div>
<div class="m2"><p>چشم ما را بی سبب آن غمزه جلاد بست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناله کردن در حریم وصل، کافر نعمتی است</p></div>
<div class="m2"><p>در بهاران عندلیب ما لب از فریاد بست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می تراود حسرت آغوش از آغوش ما</p></div>
<div class="m2"><p>زخم را نتوان دهان از شکوه بیداد بست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کوه را از جا درآرد شوخی تمثال حسن</p></div>
<div class="m2"><p>نقش شیرین را به سنگ خاره چون فرهاد بست؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناخن تدبیر سر از کار ما بیرون نبرد</p></div>
<div class="m2"><p>این رگ پیچیده، دست نشتر فصاد بست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روزگار آن سبکرو خوش که مانند شرار</p></div>
<div class="m2"><p>تا نظر وا کرد، چشم از عالم ایجاد بست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون توانم زیست ایمن، کز برای کشتنم</p></div>
<div class="m2"><p>تیغ از جوهر کمر در بیضه فولاد بست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل دو نیم از درد چون شد، شاهراه آفت است</p></div>
<div class="m2"><p>چون توان صائب ره غم بر دل ناشاد بست؟</p></div></div>