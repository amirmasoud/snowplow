---
title: >-
    غزل شمارهٔ ۲۷۱۱
---
# غزل شمارهٔ ۲۷۱۱

<div class="b" id="bn1"><div class="m1"><p>کار ما از ساغر پرمی به سامان می شود</p></div>
<div class="m2"><p>مجلس ما از گل ابری گلستان می شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناخن الماس ازکارم سری بیرون نبرد</p></div>
<div class="m2"><p>مشکل من کی به سعی سوزن آسان می شود؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یوسف این زخمی که داری از عزیزان وطن</p></div>
<div class="m2"><p>مرهمش خاکستر شام غریبان می شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جای هر نیشی که از دست تو دارم بر جگر</p></div>
<div class="m2"><p>گر به هم دوزند صد زخم نمایان می شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سر خاک شهیدان شمع آهی برده ایم</p></div>
<div class="m2"><p>خون ما با دامنی دست و گریبان می شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صائب از تنگ دهان یار پیش دل مگو</p></div>
<div class="m2"><p>طفل ما بدخوست بهر هیچ گریان می شود!</p></div></div>