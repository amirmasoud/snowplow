---
title: >-
    غزل شمارهٔ ۲۷۷۳
---
# غزل شمارهٔ ۲۷۷۳

<div class="b" id="bn1"><div class="m1"><p>از گرانان هر که چون عنقا گرانجانی کشید</p></div>
<div class="m2"><p>بار کوه قاف بتواند به آسانی کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش آن طاق دو ابرو بر زمین نه پشت دست</p></div>
<div class="m2"><p>قبله خود کن کمانی را که نتوانی کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون عرق کردم زدست و پای بیتابی زدن</p></div>
<div class="m2"><p>تا چو قربانی سر و کارم به حیرانی کشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در غبار خط نهان گردید آن لبهای لعل</p></div>
<div class="m2"><p>گنج رخت از بیم چشم بد به ویرانی کشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاکساری می کند افتادگان را سرفراز</p></div>
<div class="m2"><p>وقت آن کس خوش که این صندل به پیشانی کشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روز محشر را کند شب، نامه ناشسته اش</p></div>
<div class="m2"><p>هر که دست از دامن اشک پشیمانی کشید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق صائب می شود ظاهر به هر صورت که هست</p></div>
<div class="m2"><p>این می پرزور را نتوان به پنهانی کشید</p></div></div>