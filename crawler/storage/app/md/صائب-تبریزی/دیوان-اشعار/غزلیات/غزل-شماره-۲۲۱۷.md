---
title: >-
    غزل شمارهٔ ۲۲۱۷
---
# غزل شمارهٔ ۲۲۱۷

<div class="b" id="bn1"><div class="m1"><p>به که بر خود نبندد از دربان</p></div>
<div class="m2"><p>در دولت به هر که باز شده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرده تا روی خود به درگه حق</p></div>
<div class="m2"><p>صائب از خلق بی نیاز شده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خطش از خال حقه باز شده است</p></div>
<div class="m2"><p>خالش از خط زبان دراز شده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون سپر روی چرخ پرچین است</p></div>
<div class="m2"><p>گره از جبهه که باز شده است؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صف مژگانش در زبان بازی است</p></div>
<div class="m2"><p>گر چه چشمش به خواب ناز شده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست یک دل گشاده، حیرانم</p></div>
<div class="m2"><p>که در فیض بر که باز شده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خط مشکین او که ابجد ماست</p></div>
<div class="m2"><p>بوالهوس را خط جواز شده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رو به دریا نهاده بی لنگر</p></div>
<div class="m2"><p>بی حضور آن که در نماز شده است</p></div></div>