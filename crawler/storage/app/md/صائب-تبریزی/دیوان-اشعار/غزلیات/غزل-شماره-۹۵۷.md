---
title: >-
    غزل شمارهٔ ۹۵۷
---
# غزل شمارهٔ ۹۵۷

<div class="b" id="bn1"><div class="m1"><p>خاکساری برگ عیش خاطر آگاه ماست</p></div>
<div class="m2"><p>چون گهر گرد یتیمی خاک بازیگاه ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست از گرد خودی در کاروان ما اثر</p></div>
<div class="m2"><p>هر که پیش افتاده است از خویشتن همراه ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین چمن چون سرو دامان تعلق چیده ایم</p></div>
<div class="m2"><p>خار را خون در جگر از دامن کوتاه ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون دم شمشیر از سختی نگردانیم روی</p></div>
<div class="m2"><p>می شود سنگ فسان، سنگی اگر در راه ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از قمار عشق، ما را پاکبازی مطلب است</p></div>
<div class="m2"><p>نیست غیر از نقش کم، نقشی که خاطرخواه ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غافلیم از جان بی تقصیر در زندان تن</p></div>
<div class="m2"><p>یوسف مصر ز فرامش گشتگان چاه ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مطلب از ته کردن زانوست تحصیل شکست</p></div>
<div class="m2"><p>ورنه معلومات عالم در دل آگاه ماست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست صائب ناله ما همچو بلبل بی اثر</p></div>
<div class="m2"><p>گوش گل خونین جگر از ناله جانکاه ماست</p></div></div>