---
title: >-
    غزل شمارهٔ ۴۳۳۲
---
# غزل شمارهٔ ۴۳۳۲

<div class="b" id="bn1"><div class="m1"><p>در سینه نهان گریه مستانه نگردد</p></div>
<div class="m2"><p>سیلاب گره در دل ویرانه نگردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جویای دل صاف بود چهره روشن</p></div>
<div class="m2"><p>آیینه محال است پریخانه نگردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نزدیکی شمع است جهانسوز وگرنه</p></div>
<div class="m2"><p>فانوس حجاب پر پروانه نگردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کافر ز قبول نظر خلق شود دل</p></div>
<div class="m2"><p>این کعبه محال است صنمخانه نگردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از سرکشی نفس شود زیر و زبر جسم</p></div>
<div class="m2"><p>در خانه نگهبان سگ دیوانه نگردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هنگامه مستان نشود گرم ز هشیار</p></div>
<div class="m2"><p>از شیشه خالی سر پیمانه نگردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شایسته زنجیر بود حق نشناسی</p></div>
<div class="m2"><p>کز سلسله زلف تو دیوانه نگردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خال لب او چون خط شبرنگ برآورد</p></div>
<div class="m2"><p>در کان نمک سبز اگر دانه نگردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زلفی که بود درشکن او دل صد چاک</p></div>
<div class="m2"><p>محتاج به مشاطگی شانه نگردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روزی به در خانه او بی طلب آید</p></div>
<div class="m2"><p>درویش اگر بر در هر خانه نگردد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب نبود هیچ کم از دولت بیدار</p></div>
<div class="m2"><p>خوابی که گرانسنگ به افسانه نگردد</p></div></div>