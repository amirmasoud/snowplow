---
title: >-
    غزل شمارهٔ ۴۱۴۳
---
# غزل شمارهٔ ۴۱۴۳

<div class="b" id="bn1"><div class="m1"><p>گر خلق را به حرف دهن باز کرده اند</p></div>
<div class="m2"><p>چشم مرا به روی سخن باز کرده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بازآکه از جدایی تیغ تو زخمها</p></div>
<div class="m2"><p>چون ماهیان تشنه دهن باز کرده اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما طوطیان مصر شکرخیز غربتیم</p></div>
<div class="m2"><p>ما را ز شیر صبح وطن باز کرده اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در زیر خاک غنچه نسازند بلبلان</p></div>
<div class="m2"><p>بالی که در هوای چمن باز کرده اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داغ جنون کباب جگرهای خسته است</p></div>
<div class="m2"><p>چشم سهیل را به یمن باز کرده اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیر محیط در گره قطره می کنم</p></div>
<div class="m2"><p>تا چون حباب دیده من باز کرده اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فردا ز پشت دست ندامت خورند رزق</p></div>
<div class="m2"><p>جمعی که پیش خلق دهن باز کرده اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان تازه می شود به حریمی که عاشقان</p></div>
<div class="m2"><p>طومار دردهای کهن باز کرده اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یارب چه گل شکفته که امروز در چمن</p></div>
<div class="m2"><p>گلها به جای چشم دهن باز کرده اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باز سفید عالم غیب اند عاشقان</p></div>
<div class="m2"><p>در زیر خاک بال کفن باز کرده اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب سپهر شبنم پا در رکاب اوست</p></div>
<div class="m2"><p>درگلشنی که دیده من باز کرده اند</p></div></div>