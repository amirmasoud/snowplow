---
title: >-
    غزل شمارهٔ ۳۵۵۸
---
# غزل شمارهٔ ۳۵۵۸

<div class="b" id="bn1"><div class="m1"><p>دست و دامن چه سزاوار عطای تو بود؟</p></div>
<div class="m2"><p>ظرف دریوزه کند هرکه گدای تو بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی نیاز از زر و سیمند طلبکارانت</p></div>
<div class="m2"><p>گنج زیر قدم آبله پای تو بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون کند در دل گلگونه حوران بهشت</p></div>
<div class="m2"><p>خون هرکس که حنای کف پای تو بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گنج را گوشه ویرانی بلاگردانی است</p></div>
<div class="m2"><p>ورنه دل لایق آن نیست که جای تو بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دامن دولت جاوید به دست آورده است</p></div>
<div class="m2"><p>هرکه در سلسله زلف رسای تو بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شبنمی سیر ندیده است گل روی ترا</p></div>
<div class="m2"><p>وای بر بلبل اگر گل به صفای تو بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خضر از سبزه خوابیده گران خیزترست</p></div>
<div class="m2"><p>آتش شوقی اگر در ته پای تو بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برق خار و خس تقصیر هزاران سال است</p></div>
<div class="m2"><p>یک دم گرم که مقرون رضای تو بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نرسد چاشنی خواب به شیرینی وصل</p></div>
<div class="m2"><p>چه خیال است خیال تو به جای تو بود؟</p></div></div>