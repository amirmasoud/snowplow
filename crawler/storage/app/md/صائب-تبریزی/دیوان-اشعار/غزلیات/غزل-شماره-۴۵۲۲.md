---
title: >-
    غزل شمارهٔ ۴۵۲۲
---
# غزل شمارهٔ ۴۵۲۲

<div class="b" id="bn1"><div class="m1"><p>برانگیزد غبار از مغز جان درد</p></div>
<div class="m2"><p>برآرد گرد از آب روان درد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که می گیرد عیار صبرها را</p></div>
<div class="m2"><p>اگر گیرد کناری از میان درد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو مست خواب و ما را تا گل صبح</p></div>
<div class="m2"><p>سراسر می رود در استخوان درد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمی دادند درد سر دوا را</p></div>
<div class="m2"><p>اگر می داشتند این ناکسان درد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به درد آمد دلت از صحبت من</p></div>
<div class="m2"><p>ندانستی که می باشد گران درد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دنبال دوا سرگشته زانم</p></div>
<div class="m2"><p>که در یک جا نمی گیرد مکان درد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همان دردی که ما داریم خورشید</p></div>
<div class="m2"><p>چو برگ بید می لرزد ازان درد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر بازوی مردی را بگیرد</p></div>
<div class="m2"><p>نخواهد کرد دست آسمان درد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر هر موی صائب را بکاوند</p></div>
<div class="m2"><p>فتاده کاروان در کاروان درد</p></div></div>