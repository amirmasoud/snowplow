---
title: >-
    غزل شمارهٔ ۵۳۷۰
---
# غزل شمارهٔ ۵۳۷۰

<div class="b" id="bn1"><div class="m1"><p>می شود از اشک چشم عاشق دیوانه گرم</p></div>
<div class="m2"><p>کز شراب تلخ گردد دیده پیمانه گرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برگ عیش ما بود چون لاله داغی از بهار</p></div>
<div class="m2"><p>می توان کردن سرما را به یک پیمانه گرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ریزش ابر آورد در خنده ما را همچو برق</p></div>
<div class="m2"><p>صحبت ما می شود از گریه مستانه گرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دل ما غنچه پیکان او گلگل شکفت</p></div>
<div class="m2"><p>شاد گردد میهمان باشد چو صاحبخانه گرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در بساط دل مرا از پاکبازی اه نیست</p></div>
<div class="m2"><p>بگذرد سیل گران تمکین ازین ویرانه گرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاکدان عالم از بی رونقی افسرده بود</p></div>
<div class="m2"><p>شد از آه من درو دیوار این غمخانه گرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حلقه بیرون در دارد مرا افسردگی</p></div>
<div class="m2"><p>ورنه با شمع است دایم صحبت پروانه گرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک دل بی غم کند آزاد صد دل را ز غم</p></div>
<div class="m2"><p>می شود هنگامه اطفال از دیوانه گرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست جای خواب آسایش گذرگاه جهان</p></div>
<div class="m2"><p>تا به کی سازی به پهلو بستر بیگانه گرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روزی دیوانه می آید برون صائب زسنگ</p></div>
<div class="m2"><p>هست تا در کوچه ها هنگامه طفلانه گرم</p></div></div>