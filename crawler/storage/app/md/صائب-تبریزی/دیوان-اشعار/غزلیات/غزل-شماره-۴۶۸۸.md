---
title: >-
    غزل شمارهٔ ۴۶۸۸
---
# غزل شمارهٔ ۴۶۸۸

<div class="b" id="bn1"><div class="m1"><p>بیاکه عقده زکارجهان گشاد بهار</p></div>
<div class="m2"><p>بهشت سربه گریبان غنچه دادبهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهشت یک دل افسرده درقلمروخاک</p></div>
<div class="m2"><p>برات عیش به خلق ازشکوفه دادبهار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زخرمی درودیوارگلستان شدمست</p></div>
<div class="m2"><p>زهرگلی درمیخانه ای گشاد بهار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به روشنایی مهتاب گل نشد قانع</p></div>
<div class="m2"><p>چراغ لاله به هر رهگذرنهاد بهار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشید دشنه برق ازنیان ابر برون</p></div>
<div class="m2"><p>به خرمن غم بی حاصلان فتادبهار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گشت آنکه زدی طبل رعد زیرگلیم</p></div>
<div class="m2"><p>صلای عیش به بانگ بلند داد بهار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برآر سرزگریبان خامشی صائب</p></div>
<div class="m2"><p>کنون که غنچه منقارها گشاد بهار</p></div></div>