---
title: >-
    غزل شمارهٔ ۱۷۶۷
---
# غزل شمارهٔ ۱۷۶۷

<div class="b" id="bn1"><div class="m1"><p>میی که درد ندارد صفای درویشی است</p></div>
<div class="m2"><p>گلی که رنگ نبازد لقای درویشی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نسیم پیرهن یوسف از تهیدستی</p></div>
<div class="m2"><p>خجل ز نافه پشمین قبای درویشی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به سوزنم نتوان دوخت بر لباس حریر</p></div>
<div class="m2"><p>دلم ربوده آهن ربای درویشی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم ز سیل حوادث نمی رود از جای</p></div>
<div class="m2"><p>به کوه، پشت من از متکای درویشی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شعاع مهر که تیغش به ابر می ساید</p></div>
<div class="m2"><p>اتاقه سر خورشید سای درویشی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار تنگ شکر خواب در بغل دارد</p></div>
<div class="m2"><p>چه راحت است که با بوریای درویشی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غبار حادثه در خلوتش ندارد راه</p></div>
<div class="m2"><p>دلی که آینه دانش ردای درویشی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چرا به مشعل زرین شاه رشک برد؟</p></div>
<div class="m2"><p>چراغ زنده دلی در سرای درویشی است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز چنگ نعمت الوان خرید خون مرا</p></div>
<div class="m2"><p>چه لذت است که با شوربای درویشی است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز نغمه سنجی داود، گوش می گیرد</p></div>
<div class="m2"><p>دلی که حلقه به گوش نوای درویشی است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شمیم نافه ز پشمینه پوشی فقرست</p></div>
<div class="m2"><p>حلاوت شکر از بوریای درویشی است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نفس گداخته خود را به گوشه ای برسان</p></div>
<div class="m2"><p>که حب جاه چو سگ در قفای درویشی است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کجا به خرقه شود حاصل آشنایی فقر؟</p></div>
<div class="m2"><p>خوشا کسی که به دل آشنای درویشی است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خمیر صاف نهادان قدس را مالید</p></div>
<div class="m2"><p>اگر چه طینت آدم ز لای درویشی است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به رستگاری جاوید چون ننازد فقر؟</p></div>
<div class="m2"><p>محمد عربی رهنمای درویشی است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>من شکسته زبان مدح فقر چون گویم؟</p></div>
<div class="m2"><p>زبان معجزه مدحتسرای درویشی است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سخن رسید به نعت رسول حق صائب</p></div>
<div class="m2"><p>ببوس خاک ادب را که جای درویشی است</p></div></div>