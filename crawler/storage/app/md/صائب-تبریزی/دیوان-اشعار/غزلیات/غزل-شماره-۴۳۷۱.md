---
title: >-
    غزل شمارهٔ ۴۳۷۱
---
# غزل شمارهٔ ۴۳۷۱

<div class="b" id="bn1"><div class="m1"><p>دل را چه خیال است به می شاد توان کرد</p></div>
<div class="m2"><p>این غمکده ای نیست که آباد توان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر دامن وحشت ادب عشق نگیرد</p></div>
<div class="m2"><p>خون در دل بیرحمی صیاد توان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معذور بود هر که فراموش کند از من</p></div>
<div class="m2"><p>وحشی تر ازانم که مرا یاد توان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از ناله جرس را نگشاید گره دل</p></div>
<div class="m2"><p>دل چون تهی از درد به فریاد توان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگز نشود تیر کج از زور کمان راست</p></div>
<div class="m2"><p>ما را چه خیال است که ارشاد توان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون شعله خس نیم نفس بیش نباشد</p></div>
<div class="m2"><p>از مستی اگر وقت خوش ایجاد توان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از فکر کنی خالی اگر شیشه دل را</p></div>
<div class="m2"><p>از ذکر خدا پر ز پریزاد توان کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فریاد که در سینه من بر سر هم غم</p></div>
<div class="m2"><p>چندان نفتاده است که فریاد توان کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل نیز خنک می شود از آه سحرگاه</p></div>
<div class="m2"><p>صائب اگر آتش خمش از باد توان کرد</p></div></div>