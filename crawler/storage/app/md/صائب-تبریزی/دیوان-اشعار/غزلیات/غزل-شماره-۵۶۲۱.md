---
title: >-
    غزل شمارهٔ ۵۶۲۱
---
# غزل شمارهٔ ۵۶۲۱

<div class="b" id="bn1"><div class="m1"><p>دست در دامن رنگین بهاری نزدم</p></div>
<div class="m2"><p>ناخنی بر دل گلزار چو خاری نزدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شبنمی نیست درین باغ به محرومی من</p></div>
<div class="m2"><p>که دلم خون شد و بر لاله عذاری نزدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دهشت سختی این راه گره کرد مرا</p></div>
<div class="m2"><p>سینه چون آبله بر نشتر خاری نزدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساختم چون خس گرداب به سرگردانی</p></div>
<div class="m2"><p>دست چون موج به دامان کناری نزدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گنج پر گوهر من اشک ندامت کافی است</p></div>
<div class="m2"><p>بر سر گنجی اگر حلقه چو ماری نزدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در شکست دل من چرخ چرا می کوشد؟</p></div>
<div class="m2"><p>سنگ بر شیشه پیمانه گساری نزدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زان ز عیب و هنر خویش نگشتم آگاه</p></div>
<div class="m2"><p>که به اخلاص در آینه داری نزدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد سرم خاک درین بادیه و ز پاس ادب</p></div>
<div class="m2"><p>دست چون گرد به فتراک سواری نزدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گشت خرج کف افسوس، حنای خونم</p></div>
<div class="m2"><p>بوسه بر پای بلورین نگاری نزدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر چه سر حلقه دلسوختگانم چون داغ</p></div>
<div class="m2"><p>ناخنی بر جگر لاله عذاری نزدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تیر تخشی طمع از شیر شکاران دارم</p></div>
<div class="m2"><p>که ز کوتاهی اقبال شکاری نزدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سیل بر خانه من زور چرا می آرد؟</p></div>
<div class="m2"><p>من چون بی وقت در خانه یاری نزدم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کار این نشأه سزاوار به اقبال نبود</p></div>
<div class="m2"><p>نه ز عجزست اگر دست به کاری نزدم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به چه تقصیر زرم قسمت آتش گردید؟</p></div>
<div class="m2"><p>خنده چون گل به تهیدستی خاری نزدم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر چه چون شانه دو صد زخم نمایان خوردم</p></div>
<div class="m2"><p>دست صائب به سر زلف نگاری نزدم</p></div></div>