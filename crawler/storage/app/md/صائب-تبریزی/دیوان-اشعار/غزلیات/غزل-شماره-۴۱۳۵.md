---
title: >-
    غزل شمارهٔ ۴۱۳۵
---
# غزل شمارهٔ ۴۱۳۵

<div class="b" id="bn1"><div class="m1"><p>جمعی که بوسه بر قدم دار داده اند</p></div>
<div class="m2"><p>چون نقطه اختیار به پرگار داده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا ساغری ز خنده خونین گرفته اند</p></div>
<div class="m2"><p>چون گل هزار بوسه به هر خارداده اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آشفتگان مقید شیرازه نیستند</p></div>
<div class="m2"><p>ترک ردا وسبحه و زنار داده اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون مار کرده اند مرا تا کلید گنج</p></div>
<div class="m2"><p>از زهر صد پیاله سرشار داده اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صد بار غوطه در جگر شعله خورده ام</p></div>
<div class="m2"><p>تا چون شرر مرا دل بیدار داده اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مردان ز حمله فلک ازجا نمی روند</p></div>
<div class="m2"><p>کز جان سخت پشت به کهسار داده اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دامن فشان چو موج ز کوثر گذر کنند</p></div>
<div class="m2"><p>جمعی که دل به لعل لب یار داده اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان باده ای که میکده پرداز آن منم</p></div>
<div class="m2"><p>ته جرعه ای به نرگس بیمار داده اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این زاهدان خشک اگر مرده نیستند</p></div>
<div class="m2"><p>چون تن به زیر گنبد دستار داده اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صائب ز روی صبح گشایش طمع مدار</p></div>
<div class="m2"><p>کاین فیض را به زلف شب تار داده اند</p></div></div>