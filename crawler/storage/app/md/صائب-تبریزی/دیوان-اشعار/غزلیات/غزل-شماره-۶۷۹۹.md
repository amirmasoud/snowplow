---
title: >-
    غزل شمارهٔ ۶۷۹۹
---
# غزل شمارهٔ ۶۷۹۹

<div class="b" id="bn1"><div class="m1"><p>طعمه مور شوی گر چه سلیمان شده ای</p></div>
<div class="m2"><p>زال می گردی اگر رستم دستان شده ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای که چون موج به بازوی شنا می نازی</p></div>
<div class="m2"><p>عنقریب است که بازیچه طوفان شده ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالم خاک به جز صورت دیواری نیست</p></div>
<div class="m2"><p>چه درین صورت دیوار تو حیران شده ای؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست در دامن دریای کرم زن، ورنه</p></div>
<div class="m2"><p>تشنه می میری اگر چشمه حیوان شده ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می کند هستی فانی ترا باقی، مرگ</p></div>
<div class="m2"><p>تو چه از دولت جاوید گریزان شده ای؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چرخ نه جامه فانوس مهیا کرده است</p></div>
<div class="m2"><p>بهر شمع تو، تو از بهر چه گریان شده ای؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مصر عزت به تمنای تو نیلی پوش است</p></div>
<div class="m2"><p>چه بدآموز به این گوشه زندان شده ای؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چرخ و انجم به دو صد چشم ترا می جوید</p></div>
<div class="m2"><p>در زوایای زمین بهر چه پنهان شده ای؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آسیای فلک از بهر تو سرگردان است</p></div>
<div class="m2"><p>تو ز اندیشه روزی چه پریشان شده ای؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شکوه از درد نمودن گل بی دردیهاست</p></div>
<div class="m2"><p>شکر کن شکر که شایسته درمان شده ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بود سی پاره اجزای تو هر یک جایی</p></div>
<div class="m2"><p>این چنین جمع به سعی که چو قرآن شده ای؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کمر و تاج به هر بی سروپایی ندهند</p></div>
<div class="m2"><p>به چه خدمت تو سزاوار دل و جان شده ای؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دامن دولت خورشید چو شبنم به کف آر</p></div>
<div class="m2"><p>چه مقید به تماشای گلستان شده ای؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون به میزان قیامت همه را می سنجند</p></div>
<div class="m2"><p>بهر سنجیدن مردم تو چه میزان شده ای؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بیخودی جامه فتح است درین خارستان</p></div>
<div class="m2"><p>تو درین خانه زنبور چه عریان شده ای؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پیش عفو و کرم و رحمت یزدان صائب</p></div>
<div class="m2"><p>کم گناهی است که از جرم پشیمان شده ای؟</p></div></div>