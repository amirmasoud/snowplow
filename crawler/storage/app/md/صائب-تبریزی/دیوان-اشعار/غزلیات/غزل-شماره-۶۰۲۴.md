---
title: >-
    غزل شمارهٔ ۶۰۲۴
---
# غزل شمارهٔ ۶۰۲۴

<div class="b" id="bn1"><div class="m1"><p>دل مدام از خط و زلف یار می گوید سخن</p></div>
<div class="m2"><p>هر که سودایی شود بسیار می گوید سخن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست مانع چشم او را خواب ناز از گفتگو</p></div>
<div class="m2"><p>آنچنان کز بیخودی بیمار می گوید سخن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در صف آزاد مردان کمترست از جوز پوچ</p></div>
<div class="m2"><p>هر سبک مغزی که از دستار می گوید سخن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش رخساری که می لغزد بر او پای نگاه</p></div>
<div class="m2"><p>ساده لوح آن کس که از گلزار می گوید سخن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با پشیمانی نگردد قدرت گفتار جمع</p></div>
<div class="m2"><p>نیست نادم هر که ز استغفار می گوید سخن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که گرد حرف حرف خود نگردد بارها</p></div>
<div class="m2"><p>گر بود مرکز، که بی پرگار می گوید سخن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می کند نزدیک راه عیبجویان را به خود</p></div>
<div class="m2"><p>کارپردازی که دور از کار می گوید سخن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست ساحل را ز راز سینه دریا خبر</p></div>
<div class="m2"><p>وای بر مستی که با هشیار می گوید سخن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می کند ناقص عیاری های خود را سکه دار</p></div>
<div class="m2"><p>کاملی کز درهم و دینار می گوید سخن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عقل میدان سخن بر عاقلان کرده است تنگ</p></div>
<div class="m2"><p>ورنه مجنون با در و دیوار می گوید سخن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می شود کوته به اندک روزگاری عمر او</p></div>
<div class="m2"><p>هر که صائب چون قلم بسیار می گوید سخن</p></div></div>