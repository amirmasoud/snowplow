---
title: >-
    غزل شمارهٔ ۴۴۲۷
---
# غزل شمارهٔ ۴۴۲۷

<div class="b" id="bn1"><div class="m1"><p>خطی که ازان چهره روشن بدر آید</p></div>
<div class="m2"><p>آهی است که سینه خورشید برآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم تو نه خوابی است که تعبیر توان کرد</p></div>
<div class="m2"><p>زلف تو شبی نیست به افسانه سرآید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کام صدف تلخ کند آب گهر را</p></div>
<div class="m2"><p>حرفی که ازان لعل شکربار برآید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مه کاسه دریوزه کند هاله خود را</p></div>
<div class="m2"><p>خورشید تو چون در دل شب جلوه گر آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دور لب لعل تو یاقوت ز معدن</p></div>
<div class="m2"><p>چون لاله جگرسوخته از سنگ برآید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یوسف کندش تکمه پیراهن عصمت</p></div>
<div class="m2"><p>هر قطره اشکی که مرا از جگر آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در روز جزا سنبل گلزار بهشت است</p></div>
<div class="m2"><p>عمری که به اندیشه زلف تو سرآید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد آینه از دیدن رخسار تو محروم</p></div>
<div class="m2"><p>تا روی لطیف تو که را در نظر آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قانع به دو عالم ندهد قطره خود را</p></div>
<div class="m2"><p>دریا چه خیال است به چشم گهر آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از صحبت نیکان نشود طینت بدنیک</p></div>
<div class="m2"><p>بادام همان تلخ برون از شکر آید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آزادی کونین گرفتاری عشق است</p></div>
<div class="m2"><p>رحم است به پایی که ازین گل بدر آید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در قبضه سعی است کلید در روزی</p></div>
<div class="m2"><p>شیر از کشش طفل ز پستان بدر آید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صائب مشو از همت مردانه تسلی</p></div>
<div class="m2"><p>چون بیضه اگرچرخ ترا زیر پر آید</p></div></div>