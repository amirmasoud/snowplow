---
title: >-
    غزل شمارهٔ ۶۳۲۳
---
# غزل شمارهٔ ۶۳۲۳

<div class="b" id="bn1"><div class="m1"><p>زکات صحت جسم است خسته پرسیدن</p></div>
<div class="m2"><p>نگاهبانی عمرست پیش پا دیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر چه خواب ترا نیست بخت بیداری</p></div>
<div class="m2"><p>مدار دست ز تمهید چشم مالیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هیچ عذر نمانده است دسترس ما را</p></div>
<div class="m2"><p>به غیر ناخن خجلت زمین خراشیدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه میوه های گلوسوز در قفا دارد</p></div>
<div class="m2"><p>به خاک ره زر خود چون شکوفه پاشیدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشو ز لغزش پا ناامید در ره عشق</p></div>
<div class="m2"><p>که قطع می شود این ره به پای لغزیدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خموش باش که سنجیدگان عالم را</p></div>
<div class="m2"><p>سبکسری است به میزان خویش سنجیدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بپوش چشم خود از عیب مردمان صائب</p></div>
<div class="m2"><p>ترا که نیست میسر برهنه پوشیدن</p></div></div>