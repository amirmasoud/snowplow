---
title: >-
    غزل شمارهٔ ۱۳۲۹
---
# غزل شمارهٔ ۱۳۲۹

<div class="b" id="bn1"><div class="m1"><p>کی حذر از خون خلق آن غمزه خونریز داشت؟</p></div>
<div class="m2"><p>داشت پرهیزی گر این بیمار، از پرهیز داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا نشد آب، از نقاب غنچه سر بیرون نکرد</p></div>
<div class="m2"><p>بس که گل شرمندگی زان روی شبنم خیز داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رهنوردی بود چون شبدیز اگر پرویز را</p></div>
<div class="m2"><p>کوهکن هم قاصدی چون ناله شبخیز داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برد همت ذره ما را به اوج آسمان</p></div>
<div class="m2"><p>ورنه کی خورشید پروای من ناچیز داشت؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پرده تا برداشت از رخ بی نیازیهای حق</p></div>
<div class="m2"><p>زیر پا افکند هر کس هر چه دستاویز داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در شهادتگاه وحدت عاشقان را یکسرند</p></div>
<div class="m2"><p>آن که بر سر تیشه زد، قصد سر پرویز داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا قیامت گر به من صائب بنازد دور نیست</p></div>
<div class="m2"><p>کی چو من آتش زبانی کشور تبریز داشت؟</p></div></div>