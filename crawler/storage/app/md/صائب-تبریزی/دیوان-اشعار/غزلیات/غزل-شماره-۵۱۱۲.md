---
title: >-
    غزل شمارهٔ ۵۱۱۲
---
# غزل شمارهٔ ۵۱۱۲

<div class="b" id="bn1"><div class="m1"><p>ندارد صفحه رخسار خوبان اعتبار خط</p></div>
<div class="m2"><p>در آتش دارد از هر حلقه نعلی بیقرار خط</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزاران چشم روشن ساختی در روزگار خط</p></div>
<div class="m2"><p>کرامت کن به ما هم سرمه واری از غبار خط</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نخواندی چون به سیر باغ خود در جوش گل ما را</p></div>
<div class="m2"><p>به برگ سبز باری یاد کن در نوبهار خط</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به روی گرم کن این شمع ناحق کشته را روشن</p></div>
<div class="m2"><p>که شد چشم امید من سفید از انتظار خط</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نکردی در زمان زلف اگر شیرازه دلها را</p></div>
<div class="m2"><p>مشو غافل به عهد دولت ناپایدار خط</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر چه خال باشد مرکز پرگار خوبی را</p></div>
<div class="m2"><p>کند از شرمساری روی پنهان در غبار خط</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به این خاک مراد امیدها را وعده می دادم</p></div>
<div class="m2"><p>ندانستم نمک در دیده ام ریزد غبار خط</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز حسن نو خطان بسیار دشوارست دل کندن</p></div>
<div class="m2"><p>دواند ریشه چون جوهر در آهن خارخار خط</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صفای گوهر از گرد یتیمی بیش می گردد</p></div>
<div class="m2"><p>غباری نیست بر خاطر مرا از رهگذار خط</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنان کافزاید از بیهوش دارو نشأه صهبا</p></div>
<div class="m2"><p>دو بالا می شود کیفیت لب از غبار خط</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه نسبت خط مشکین را به روی ساده خوبان؟</p></div>
<div class="m2"><p>در آتش دارد از هر حلقه نعلی بیقرار خط</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بود خواب پریشان سنبل فردوس در چشمش</p></div>
<div class="m2"><p>چرانیده است هرکس چشم خود در سبزه زار خط</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کند از سر کشی هرخون که در دل زلف، عاشق را</p></div>
<div class="m2"><p>تلافی می کند آخر نسیم مشکبار خط</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خمار صبح را ته شیشه شب می کند درمان</p></div>
<div class="m2"><p>مشو غافل ز رخسار بتان در روزگار خط</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نثاری هست لازم، خواندن فرمان شاهان را</p></div>
<div class="m2"><p>نسازم نقد دین و دل چرا صائب نثار خط؟</p></div></div>