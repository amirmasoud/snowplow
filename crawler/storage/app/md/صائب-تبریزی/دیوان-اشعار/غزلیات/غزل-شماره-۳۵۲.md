---
title: >-
    غزل شمارهٔ ۳۵۲
---
# غزل شمارهٔ ۳۵۲

<div class="b" id="bn1"><div class="m1"><p>اگر از اهل ایمانی مهیا باش آفت را</p></div>
<div class="m2"><p>که دندان می گزد پیوسته انگشت شهادت را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل صد پاره ما را نگاهی جمع می سازد</p></div>
<div class="m2"><p>که از یک رشته بتوان بخیه زد چندین جراحت را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمک می ریزد از لبهای جانان وقت خاموشی</p></div>
<div class="m2"><p>نمکدان چون کند در حقه آن کان ملاحت را؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به اندک فرصتی نخل از زمین پاک می بالد</p></div>
<div class="m2"><p>مکن در صبحدم زنهار فوت آه ندامت را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کریمان را خدای مهربان درمانده نگذارد</p></div>
<div class="m2"><p>که می روید زر از کف همچو گل اهل سخاوت را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دشواری زلیخا داد از کف دامن یوسف</p></div>
<div class="m2"><p>به آسانی من از کف چون دهم دامان فرصت را؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز منت شمع هر کس سیلیی خورده است، می داند</p></div>
<div class="m2"><p>که از صرصر خطر افزون بود دست حمایت را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نمی شد زنگ کلفت سبزه امید من صائب</p></div>
<div class="m2"><p>اگر می بود آبی در جگر ابر مروت را</p></div></div>