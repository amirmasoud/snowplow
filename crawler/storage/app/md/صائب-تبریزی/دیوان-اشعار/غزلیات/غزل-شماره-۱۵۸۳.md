---
title: >-
    غزل شمارهٔ ۱۵۸۳
---
# غزل شمارهٔ ۱۵۸۳

<div class="b" id="bn1"><div class="m1"><p>خلوت آینه را طوطی غمازی هست</p></div>
<div class="m2"><p>هر کجا روی نهادیم سخنسازی هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست مجنون وفادار مرا پای گریز</p></div>
<div class="m2"><p>ورنه چون زور جنون سلسله پردازی هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم نظارگیان تاب ندارد، ورنه</p></div>
<div class="m2"><p>دل تاریک مرا آینه پردازی هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نفس های پریشان غبارآلودم</p></div>
<div class="m2"><p>می توان یافت که در سینه سبکتازی هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فیض سر رشته امید عمومی دارد</p></div>
<div class="m2"><p>در حریمی که نگاه غلط اندازی هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دامن گل نشود زخمی سر پنجه خار</p></div>
<div class="m2"><p>گلستانی که در او شعله آوازی هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>انتظار جگر سوختگان سنگ ره است</p></div>
<div class="m2"><p>ورنه ما را چو شرر رخصت پروازی هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا نبارد به سرش تیغ، دهن نگشاید</p></div>
<div class="m2"><p>چون صدف در دل هر کس گهر رازی هست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روی برتافتن از سیلی غم بی‌جگری است</p></div>
<div class="m2"><p>ورنه چون رنگ، مرا شهپر پروازی هست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون شرر آمدن و رفتن ما هر دو یکی است</p></div>
<div class="m2"><p>ما چه دانیم که انجامی و آغازی هست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حیف باشد که درین دشت شکاری نکند</p></div>
<div class="m2"><p>هر که را قوت سرپنجه شهبازی هست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از نواهای جگرسوز تو صائب پیداست</p></div>
<div class="m2"><p>که ترا در دل صد پاره نواسازی هست</p></div></div>