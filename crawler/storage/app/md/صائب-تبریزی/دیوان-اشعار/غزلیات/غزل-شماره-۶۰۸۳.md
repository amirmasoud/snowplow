---
title: >-
    غزل شمارهٔ ۶۰۸۳
---
# غزل شمارهٔ ۶۰۸۳

<div class="b" id="bn1"><div class="m1"><p>بر نظربازان ستم در ابتدای خط مکن</p></div>
<div class="m2"><p>خشک مغزی در بهار جانفزای خط مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قطع پیوند محبت می کند مکتوب خشک</p></div>
<div class="m2"><p>سرکشی با عاشقان در ابتدای خط مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می کند بیدار آب این سبزه خوابیده را</p></div>
<div class="m2"><p>تیغ را از ساده لوحی آشنای خط مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم تا بر هم زنی این مور گردیده است مار</p></div>
<div class="m2"><p>بی سبب تعجیل در نشو و نمای خط مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شعله خس می شود خامش به اندک فرصتی</p></div>
<div class="m2"><p>تکیه بر حسن سبکسیر صفای خط مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از زبان بازی پریشان می شود زلف حواس</p></div>
<div class="m2"><p>شانه را تا می توانی آشنای خط مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می شود زیر و زبر از لشکر بیگانه ملک</p></div>
<div class="m2"><p>زلف را باز از سر خود از برای خط مکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حکم نتوان بر فلک راندن به تقویم کهن</p></div>
<div class="m2"><p>ناز بر صاحبدلان در انتهای خط مکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از نزول آیه رحمت خجل گشتن خطاست</p></div>
<div class="m2"><p>روی خود پنهان ز صائب از حیای خط مکن</p></div></div>