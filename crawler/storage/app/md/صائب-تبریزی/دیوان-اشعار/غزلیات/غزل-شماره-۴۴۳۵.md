---
title: >-
    غزل شمارهٔ ۴۴۳۵
---
# غزل شمارهٔ ۴۴۳۵

<div class="b" id="bn1"><div class="m1"><p>گر چشم تر از پوست چو بادام برآید</p></div>
<div class="m2"><p>آسان ز وصال شکرش کام برآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان من مشتاق به لب می رسد از شوق</p></div>
<div class="m2"><p>تا از دهن تنگ تو پیغام برآید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون در دل یاقوت زند جوش ز غیرت</p></div>
<div class="m2"><p>هر جا ز عقیق لب او نام برآید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشکل بود از رشته امید گسستن</p></div>
<div class="m2"><p>چون مرغ گرفتار من از دام برآید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیرون ندهد نم ز جگر لاله سیراب</p></div>
<div class="m2"><p>کی حرف به مستی ز لب جام برآید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن را که بود همچو شرر دیده روشن</p></div>
<div class="m2"><p>در نقطه آغاز ز انجام برآید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از موج حوادث نشود پخته سبک مغز</p></div>
<div class="m2"><p>از بحر همان عنبر تر خام برآید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زنهار مکن سرکشی از حلقه عشاق</p></div>
<div class="m2"><p>کز فاخته این سرو به اندام برآید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زآتشکده هند شد آدم ز گنه پاک</p></div>
<div class="m2"><p>زین بوته محال است کسی خام برآید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در کلبه مقصود نفس راست نماید</p></div>
<div class="m2"><p>از هر دو جهان هر که به یک گام برآید</p></div></div>