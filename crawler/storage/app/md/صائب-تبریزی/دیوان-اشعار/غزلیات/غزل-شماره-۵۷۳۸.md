---
title: >-
    غزل شمارهٔ ۵۷۳۸
---
# غزل شمارهٔ ۵۷۳۸

<div class="b" id="bn1"><div class="m1"><p>اگر چه نیک نیم در پناه نیکانم</p></div>
<div class="m2"><p>عجب که تشنه بمانم، سفال ریحانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز اشک شمع به شبگردی اشک من پیش است</p></div>
<div class="m2"><p>ز گریه زمزم صد کعبه شبستانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چرا عزیز نباشم به دیده ها چون خال</p></div>
<div class="m2"><p>کبوتر حرم آن چه زنخدانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صدف به آب گهر تا دهن نمی شوید</p></div>
<div class="m2"><p>نمی برد به زبان نام چشم گریانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همان تلاش نهانخانه وطن دارم</p></div>
<div class="m2"><p>اگر دهد به کف دست جا سلیمانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به هر که منکر جوش سرشک من باشد</p></div>
<div class="m2"><p>اگر بود پسر نوح، موج طوفانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گشاده روی به احباب چون گل صبحم</p></div>
<div class="m2"><p>به گرمخونی چون آفتاب تابانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تلاش میوه جنت نمی کنم صائب</p></div>
<div class="m2"><p>هلاک سیب زنخدان و نار پستانم</p></div></div>