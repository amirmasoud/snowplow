---
title: >-
    غزل شمارهٔ ۱۷۷۳
---
# غزل شمارهٔ ۱۷۷۳

<div class="b" id="bn1"><div class="m1"><p>سفر نکردن ازان کشور از گرانجانی است</p></div>
<div class="m2"><p>که مرگی دل و قحط غذای روحانی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لب محیط به بانگ بلند می گوید</p></div>
<div class="m2"><p>برهنه شو که گهر مزد دست عریانی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سفر خوش است که بی اختیار روی دهد</p></div>
<div class="m2"><p>سپند، منتظر آتش از گرانجانی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به نان خشک قناعت نمی توان کردن</p></div>
<div class="m2"><p>چه نعمتی است که افلاک سر که پیشانی است!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آرمیدگی ظاهرم فریب مخور</p></div>
<div class="m2"><p>اگر چه ساکن شهرم، دلم بیابانی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز جوش وحش چه غوغاست بر سر مجنون؟</p></div>
<div class="m2"><p>اگر نه داغ جنون خاتم سلیمانی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همیشه آب به چشم پیاله می گردد</p></div>
<div class="m2"><p>جبین پیر خرابات بس که نورانی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلی که از سخن تازه شد جوان، داند</p></div>
<div class="m2"><p>که سبزی پر طوطی، گل سخندانی است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جواب آن غزل است این که نقد حیدر گفت</p></div>
<div class="m2"><p>ازو چه شکوه کنم، عالم پریشانی است</p></div></div>