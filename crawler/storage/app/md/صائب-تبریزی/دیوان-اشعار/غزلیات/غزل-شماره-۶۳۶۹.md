---
title: >-
    غزل شمارهٔ ۶۳۶۹
---
# غزل شمارهٔ ۶۳۶۹

<div class="b" id="bn1"><div class="m1"><p>خاکم به چشم در نگه واپسین مزن</p></div>
<div class="m2"><p>زنهار بر چراغ سحر آتشین مزن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افتاده را دوباره فکندن کمال نیست</p></div>
<div class="m2"><p>آن را که خاک راه تو شد بر زمین مزن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کافی است بهر سوختنم یک نگاه گرم</p></div>
<div class="m2"><p>آتش به جانم از سخن آتشین مزن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگذار چشم فتنه خوابیده را به خواب</p></div>
<div class="m2"><p>ناخن به داغ سینه اندوهگین مزن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>انصاف نیست آیه رحمت شود عذاب</p></div>
<div class="m2"><p>چینی که حق زلف بود بر جبین مزن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون شیشه توتیا شود از سنگ فارغ است</p></div>
<div class="m2"><p>بر سنگ خاره شیشه ما بیش ازین مزن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواهی که گیرد از تو سپهر برین حساب</p></div>
<div class="m2"><p>زنهار ناشمرده قدم بر زمین مزن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب به گوشه دل خود تا توان خزید</p></div>
<div class="m2"><p>زنهار حلقه بر در خلق برین مزن</p></div></div>