---
title: >-
    غزل شمارهٔ ۳۹۶۹
---
# غزل شمارهٔ ۳۹۶۹

<div class="b" id="bn1"><div class="m1"><p>مباد اهل عمل بیخود از شراب شود</p></div>
<div class="m2"><p>که از خرابی او عالمی خراب شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقاب اگر به رخ دلبران حجاب شود</p></div>
<div class="m2"><p>رخ لطیف تو بی پرده از نقاب شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همان ز تشنه لبی چون سهیل می سوزم</p></div>
<div class="m2"><p>اگر عقیق لبش در دهانم آب شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شده است حلقه خط سخت تنگ می ترسم</p></div>
<div class="m2"><p>که باده لب او پای در رکاب شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کند ز دود سیه مست هوشیاران را</p></div>
<div class="m2"><p>دلی کز آتش رخسار او کباب شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گلاب پیرهن آفتاب می گردد</p></div>
<div class="m2"><p>درین ریاض چو شبنم دلی که آب شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چگونه رنگ توانم به روی گلشن دید</p></div>
<div class="m2"><p>مرا که بوی گل از وصل گل حجاب شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز آتش رخ ساقی که می جهد سالم</p></div>
<div class="m2"><p>به محفلی که بط می در او کباب شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زشعله بال سمندر خطر نمی دارد</p></div>
<div class="m2"><p>ز باده حسن محال است بی حجاب شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز وعده های دروغ تو شوق من افزود</p></div>
<div class="m2"><p>که شور تشنه لبی بیش از سراب شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر ز اهل دلی با شکستگی خوش باش</p></div>
<div class="m2"><p>که مه تمام چو شد دور از آفتاب شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنین که شد ز قساوت مرا جگر بی آب</p></div>
<div class="m2"><p>عجب که دیده من تر ز آفتاب شود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حریف نخوت نو دولتان نمی گردید</p></div>
<div class="m2"><p>حذر کنید ز خونی که مشک ناب شود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عیارمنت احسان چرخ اگر این است</p></div>
<div class="m2"><p>ستاره سوختگی خال انتخاب شود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همیشه درد به عضو ضعیف می ریزد</p></div>
<div class="m2"><p>که مرغ بیوه زنان قسمت عقاب شود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کند شماتت زاهد فرنگ عالم را</p></div>
<div class="m2"><p>خدا نخواسته میخانه گر خراب شود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز گریه اش جگر سنگ خون شود صائب</p></div>
<div class="m2"><p>دلی که از نفس گرم من کباب شود</p></div></div>