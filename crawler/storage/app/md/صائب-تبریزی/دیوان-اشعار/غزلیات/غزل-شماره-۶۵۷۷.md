---
title: >-
    غزل شمارهٔ ۶۵۷۷
---
# غزل شمارهٔ ۶۵۷۷

<div class="b" id="bn1"><div class="m1"><p>بی تأمل بر بساط پاکبازان پا منه</p></div>
<div class="m2"><p>تا نشویی دست از جان پای در دریا منه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قسمت صیاد از صید حرم دل خوردن است</p></div>
<div class="m2"><p>امن می خواهی، ز حد خویش بیرون پا منه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون نداری ترجمانی همچو عیسی در کنار</p></div>
<div class="m2"><p>مهر خاموشی چو مریم بر لب گویا منه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوشه گیری در میان خلق تنها بودن است</p></div>
<div class="m2"><p>بر دل خود بار کوه قاف چون عنقا منه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سبکروحان گران گردیدن از انصاف نیست</p></div>
<div class="m2"><p>برنداری باری از دل، بار بر دلها منه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چاه خس پوشی است در هر گام این وحشت سرا</p></div>
<div class="m2"><p>بی عصا زنهار در صحرای امکان پا منه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوشه گیر از خلق چون آیینه ات بی زنگ شد</p></div>
<div class="m2"><p>خرمن خود را چو کردی پاک در صحرا منه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آه سرد ناامیدی می کند کار خزان</p></div>
<div class="m2"><p>چوب منع ای باغبان در پیش راه ما منه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می شود بر زود سیریها گواه پا بجا</p></div>
<div class="m2"><p>وقت رفتن میهمان را کفش پیش پا منه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بالش خار است سر از خواب چون سنگین شود</p></div>
<div class="m2"><p>زیر سر چون تن پرستان بالش خارا منه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شهپر پروانه نتواند نقاب شمع شد</p></div>
<div class="m2"><p>پرده بر رخسار خود ای آتشین سیما منه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نسخه داغی به دست آر از دل پرشور ما</p></div>
<div class="m2"><p>دل به داغ بی نمک چون لاله حمرا منه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از تواضع های رسمی می کنندت سنگسار</p></div>
<div class="m2"><p>تا میسر می شود از خانه بیرون پا منه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دیده را از خون دل مگذار صائب بی نصیب</p></div>
<div class="m2"><p>آنچه می باید به ساغر ریخت در مینا منه</p></div></div>