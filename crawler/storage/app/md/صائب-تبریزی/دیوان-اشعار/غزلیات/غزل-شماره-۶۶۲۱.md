---
title: >-
    غزل شمارهٔ ۶۶۲۱
---
# غزل شمارهٔ ۶۶۲۱

<div class="b" id="bn1"><div class="m1"><p>آن را که نیست دلبری از دل چه فایده؟</p></div>
<div class="m2"><p>جایی که برق نیست ز حاصل چه فایده؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنجیر تازیانه بود فیل مست را</p></div>
<div class="m2"><p>دیوانه ترا ز سلاسل چه فایده؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این سیل رخنه در دل فولاد می کند</p></div>
<div class="m2"><p>بستن به روی عشق در دل چه فایده؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اکنون که شد سفید مرا چشم انتظار</p></div>
<div class="m2"><p>از سرمه سیاهی منزل چه فایده؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مجنون چو نسخه از رخ لیلی گرفته است</p></div>
<div class="m2"><p>دیگر ز پرده داری محمل چه فایده؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن را که هست چون گهر از آب خود خطر</p></div>
<div class="m2"><p>از لنگر سلامت ساحل چه فایده؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در چشم تنگ مور جهان چشم سوزن است</p></div>
<div class="m2"><p>دلتنگ را ز وسعت منزل چه فایده؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشم گرسنه سیر ز نعمت نمی شود</p></div>
<div class="m2"><p>غربال را ز کثرت حاصل چه فایده؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا روشن است دل ز دو عالم بشوی دست</p></div>
<div class="m2"><p>چون غوطه خورد آینه در گل چه فایده؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صائب ترا که طاقت دیدار یار نیست</p></div>
<div class="m2"><p>از انتظار دوست چه حاصل، چه فایده؟</p></div></div>