---
title: >-
    غزل شمارهٔ ۶۳۹۹
---
# غزل شمارهٔ ۶۳۹۹

<div class="b" id="bn1"><div class="m1"><p>فیض نسیم صبح بود با فغان من</p></div>
<div class="m2"><p>بر شاخ گل گران نبود آشیان من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ریگ روان بادیه بی نشانیم</p></div>
<div class="m2"><p>سرگشتگی است راهبر کاروان من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون برق، منتهای نفس منزل من است</p></div>
<div class="m2"><p>بیچاره رهروی که شود همعنان من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دستش ز تیر زودتر افتد به خاک راه</p></div>
<div class="m2"><p>آن ساده دل که زور زند بر کمان من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون دانه سپند، بر آتش نشسته است</p></div>
<div class="m2"><p>مهر خموشی از لب آتش بیان من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشنو ز من دروغ که از راست خانگی</p></div>
<div class="m2"><p>پیچیده نیست جوهر تیغ زبان من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>انصاف نیست مانع نظارگی شدن</p></div>
<div class="m2"><p>کز جوش گل شکست در بوستان من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بستم به خاک نقش و همان میل می کشد</p></div>
<div class="m2"><p>در چشم دشمنان، قلم استخوان من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب ز بس مراد که در خاک کرده ام</p></div>
<div class="m2"><p>خاک مراد خلق شده است آستان من</p></div></div>