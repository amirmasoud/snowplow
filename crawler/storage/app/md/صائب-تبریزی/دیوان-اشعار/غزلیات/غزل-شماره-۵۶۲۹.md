---
title: >-
    غزل شمارهٔ ۵۶۲۹
---
# غزل شمارهٔ ۵۶۲۹

<div class="b" id="bn1"><div class="m1"><p>از دل سوخته اخگر به گریبان دارم</p></div>
<div class="m2"><p>سینه ای گرمتر از خاک شهیدان دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساده از نقش تمناست دل خرسندم</p></div>
<div class="m2"><p>عالمی امن تر از دیده حیران دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به تهیدستی من خنده زند موج سراب</p></div>
<div class="m2"><p>دامن بحر به کف گر چه چو مرجان دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قسمت زنگی از آیینه روشن نشود</p></div>
<div class="m2"><p>انفعالی که من از صاف ضمیران دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که محروم شد تا از نان جوین می داند</p></div>
<div class="m2"><p>که چون خون در جگر از نعمت الوان دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خرقه پوشیدن من نیست ز بیدار دلی</p></div>
<div class="m2"><p>پای خوابیده نهان در ته دامان دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بوی خون شفق از خنده من می آید</p></div>
<div class="m2"><p>گر چه چون صبح به ظاهر لب خندان دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون تو از پشت ورق روی ورق می خوانی</p></div>
<div class="m2"><p>حال خود از تو چه پوشیده و پنهان دارم؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر چه چون شانه ز من باز شودهر گرهی</p></div>
<div class="m2"><p>سری آشفته تر از زلف پریشان دارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می کند شهپر پرواز قفس را صائب</p></div>
<div class="m2"><p>خار خاری که من از شوق گلستان دارم</p></div></div>