---
title: >-
    غزل شمارهٔ ۶۶۸۱
---
# غزل شمارهٔ ۶۶۸۱

<div class="b" id="bn1"><div class="m1"><p>تکیه چند از ضعف بر دوش عصا دارد کسی؟</p></div>
<div class="m2"><p>این بنای سست را تا کی بپا دارد کسی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اعتمادی نیست بر جمعیت بی نسبتان</p></div>
<div class="m2"><p>چند پاس آتش و آب و هوا دارد کسی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند بتوان عقده در کار نفس زد چون حباب؟</p></div>
<div class="m2"><p>این بنا را چند بر پا از هوا دارد کسی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عمر با صد ساله الفت بی وفایی کرد و رفت</p></div>
<div class="m2"><p>از که دیگر در جهان چشم وفا دارد کسی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من که دارم تا غبار افشاند از بال و پرم؟</p></div>
<div class="m2"><p>وقت بلبل خوش که چون باد صبا دارد کسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مطلب کونین در آغوش ترک مدعاست</p></div>
<div class="m2"><p>برنیاید مطلبش تا مدعا دارد کسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>استخوانم توتیا شد از گرانی های جان</p></div>
<div class="m2"><p>این زره را چند در زیر قبا دارد کسی؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رنج میل آتشین و پرتو منت یکی است</p></div>
<div class="m2"><p>چشم بینایی چرا از توتیا دارد کسی؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خار صحرای ملامت خواب مخمل می شود</p></div>
<div class="m2"><p>آتش شوقی اگر در زیر پا دارد کسی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می شود آخر بیابان مرگ، بی درد طلب</p></div>
<div class="m2"><p>چون خضر هر گام اگر صد رهنما دارد کسی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر که با حق آشنا شد، از جهان بیگانه شد</p></div>
<div class="m2"><p>نیست با حق آشنا تا آشنا دارد کسی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بی تکلف، آب خوردن بی رفیقان مشکل است</p></div>
<div class="m2"><p>من گرفتم چون خضر آب بقا دارد کسی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پرده جمعیت خاطر بود صائب حجاب</p></div>
<div class="m2"><p>بد نبیند تا نظر بر پشت پا دارد کسی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این جواب آن غزل صائب که می گوید ملک</p></div>
<div class="m2"><p>چند پاس وعده هر بی وفا دارد کسی؟</p></div></div>