---
title: >-
    غزل شمارهٔ ۱۸۸۴
---
# غزل شمارهٔ ۱۸۸۴

<div class="b" id="bn1"><div class="m1"><p>آن را که در وطن لب نانی میسرست</p></div>
<div class="m2"><p>سی شب ز ماه عید سرایش منورست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خانه های کهنه بود مور و مار بیش</p></div>
<div class="m2"><p>حرص و امل به طینت پیران فزونترست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ارباب احتیاج اگر آبروی خویش</p></div>
<div class="m2"><p>گردآوری کنند، به از عقد گوهرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرگز نگردد آینه را دل به آب صاف</p></div>
<div class="m2"><p>ظلمت ز آب خضر نصیب سکندرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کنه ذات، فکر به جایی نمی رسد</p></div>
<div class="m2"><p>دریای بیکنار چه جای شناورست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فردی که ساده است نیارند در حساب</p></div>
<div class="m2"><p>دیوانه را چه کار به دیوان محشرست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از بس گزیده است سلامت روی مرا</p></div>
<div class="m2"><p>موج خطر به چشم من آغوش مادرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در قطره ای چه جلوه کند بحر بیکنار؟</p></div>
<div class="m2"><p>در چشم مور ملک سلیمان محقرست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب به غیر نامه عالم نورد من</p></div>
<div class="m2"><p>هر نامه ای که هست و بال کبوترست</p></div></div>