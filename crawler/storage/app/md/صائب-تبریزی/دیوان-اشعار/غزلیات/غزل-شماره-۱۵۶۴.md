---
title: >-
    غزل شمارهٔ ۱۵۶۴
---
# غزل شمارهٔ ۱۵۶۴

<div class="b" id="bn1"><div class="m1"><p>مایه پرورش عالم اسباب یکی است</p></div>
<div class="m2"><p>باغ هر چند به صد رنگ بود آب یکی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لطف چون قهر مرا زیر و زبر می سازد</p></div>
<div class="m2"><p>نسبت سیل به این خانه و مهتاب یکی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>محو دیدار ندارد خبر از لطف و عتاب</p></div>
<div class="m2"><p>چشم حیرت زدگان را نمک و خواب یکی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غافل از مستی حسنی ز جگرسوختگان</p></div>
<div class="m2"><p>داغ در چشم تو و لاله سیراب یکی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه کنم آه که در دیده بی پروایان</p></div>
<div class="m2"><p>صبر آیینه و بیتابی سیماب یکی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عجز و قدرت نشود مانع بیباکی عشق</p></div>
<div class="m2"><p>خانه شاه و گدا در ره سیلاب یکی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قانع از قامت یارست به خمیازه خشک</p></div>
<div class="m2"><p>بخت آغوش من و طالع محراب یکی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل سودازده را مایه سرگردانی است</p></div>
<div class="m2"><p>حلقه چشم تو و حلقه گرداب یکی است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست در مشرب من ساده و نوخط را فرق</p></div>
<div class="m2"><p>درد پیش من مخمور و می ناب یکی است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رشته جان من و رشته آن موی کمر</p></div>
<div class="m2"><p>چون نپیچند به یکدیگر اگر تاب یکی است؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در میان گل و مل نیست دورنگی صائب</p></div>
<div class="m2"><p>مدت جوش گل و جوش می ناب یکی است</p></div></div>