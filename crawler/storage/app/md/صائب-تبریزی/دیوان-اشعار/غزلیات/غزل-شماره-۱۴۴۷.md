---
title: >-
    غزل شمارهٔ ۱۴۴۷
---
# غزل شمارهٔ ۱۴۴۷

<div class="b" id="bn1"><div class="m1"><p>مستی حسن، هم از ساغر سرشار خودست</p></div>
<div class="m2"><p>باده لعلیش از لعل گهربار خودست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می توان یافتن از حلقه شدنهای خطش</p></div>
<div class="m2"><p>که به صد چشم دلش واله رخسار خودست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر گرفتاری ما کی جگرش می سوزد؟</p></div>
<div class="m2"><p>آن که بیش از همه عشاق گرفتار خودست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به کباب دل عاشق نکند تلخ دهن</p></div>
<div class="m2"><p>هر که را نقل می از لعل شکربار خودست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی به نقد دل و جان دگران پردازد؟</p></div>
<div class="m2"><p>چون مه مصر، عزیزی که گرفتار خودست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل چون آینه از سنگ توقع دارد</p></div>
<div class="m2"><p>بس که آن آینه رو تشنه دیوار خودست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می کند جلوه مستانه نکویان را مست</p></div>
<div class="m2"><p>بیشتر مستی طاوس ز رفتار خودست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکقلم فاختگان را خط آزادی داد</p></div>
<div class="m2"><p>سرو موزون تو از بس که هوادار خودست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به پریشانی عشاق کجا پردازد؟</p></div>
<div class="m2"><p>آن که از سلسله زلف، گرفتار خودست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نظر از جلوه خورشید کجا آب دهد؟</p></div>
<div class="m2"><p>شبنم هر که نظرباز به گلزار خودست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عکس خود سیر ندیده است در آینه و آب</p></div>
<div class="m2"><p>بس که اندیشه اش از غمزه خونخوار خودست!</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چه گل از روی تو نظاره تواند چیدن؟</p></div>
<div class="m2"><p>که به مستی عرق شرم تو هشیار خودست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چند پوشیده کنی عشق خود از اهل نظر؟</p></div>
<div class="m2"><p>نیست بیمار کسی چشم تو بیمار خودست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چه دهم دل به نگاری که بود واله خویش؟</p></div>
<div class="m2"><p>چه پرستم صنمی را که پرستار خودست؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نیست ممکن که شود رام به مجنون صائب</p></div>
<div class="m2"><p>رم ز خود کرده غزالی که طلبکار خودست</p></div></div>