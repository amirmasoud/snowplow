---
title: >-
    غزل شمارهٔ ۶۱۸
---
# غزل شمارهٔ ۶۱۸

<div class="b" id="bn1"><div class="m1"><p>زبان هر هرزه درایی به جان رساند مرا</p></div>
<div class="m2"><p>لب خموش به دارالامان رساند مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ادا چگونه کنم شکر آه را، کاین تیر</p></div>
<div class="m2"><p>ز یک گشاد به چندین نشان رساند مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بی کسی چه شکایت کنم به هر ناکس؟</p></div>
<div class="m2"><p>که بی کسی به کس بی کسان رساند مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر چه بی بروپالی است سنگ راه عروج</p></div>
<div class="m2"><p>دل شکسته به آن دلستان رساند مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دیده چون ندهم جای، اشک را صائب؟</p></div>
<div class="m2"><p>که سیل گریه به آن آستان رساند مرا</p></div></div>