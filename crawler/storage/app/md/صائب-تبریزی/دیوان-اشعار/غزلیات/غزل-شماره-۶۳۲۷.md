---
title: >-
    غزل شمارهٔ ۶۳۲۷
---
# غزل شمارهٔ ۶۳۲۷

<div class="b" id="bn1"><div class="m1"><p>مکن تعجب اگر شد چراغ ما روشن</p></div>
<div class="m2"><p>چراغ زنده دلان را کند خدا روشن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ملایمت ز طمع پیشگان به آن ماند</p></div>
<div class="m2"><p>که شمع موم به منزل کند گدا روشن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همیشه بخت سیه روز در میانم داشت</p></div>
<div class="m2"><p>مرا چو شمع نگردید پیش پا روشن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر چه هست کدورت میان چشم و غبار</p></div>
<div class="m2"><p>شد از غبار خط او سواد ما روشن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به غور معنی نازک رسند صافدلان</p></div>
<div class="m2"><p>هلال چهره نماید چو شد هوا روشن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امید هست که چشم بصارت صائب</p></div>
<div class="m2"><p>شود ز خاک در شاه اولیا روشن</p></div></div>