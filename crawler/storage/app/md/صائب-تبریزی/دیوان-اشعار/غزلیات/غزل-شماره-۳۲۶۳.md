---
title: >-
    غزل شمارهٔ ۳۲۶۳
---
# غزل شمارهٔ ۳۲۶۳

<div class="b" id="bn1"><div class="m1"><p>که گمان داشت ز خط حسن تو زایل گردد؟</p></div>
<div class="m2"><p>فرد خورشید که می گفت که باطل گردد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می تواند ز رخ شمع کسی گل چیدن</p></div>
<div class="m2"><p>که چو پروانه به گرد سر محفل گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناف دریاست چو گرداب مرا لنگرگاه</p></div>
<div class="m2"><p>نیستم موج که سعیم پی ساحل گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرغ روح شهدا پر به پر هم بسته است</p></div>
<div class="m2"><p>زهره کیست که گرد سر قاتل گردد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخن تلخ فرو برده و قهقه زده ام</p></div>
<div class="m2"><p>کام من تلخ کی از زهر هلاهل گردد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر مژگان سبکرو به سلامت باشد!</p></div>
<div class="m2"><p>پا اگر آبله از دوری منزل گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شبنم آینه کس چهره خورشید نکر</p></div>
<div class="m2"><p>به چه رو با رخش آیینه مقابل گردد؟</p></div></div>