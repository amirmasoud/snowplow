---
title: >-
    غزل شمارهٔ ۵۳۶۳
---
# غزل شمارهٔ ۵۳۶۳

<div class="b" id="bn1"><div class="m1"><p>پیش چشمم شد روان گر تشنه دریا شدم</p></div>
<div class="m2"><p>یافتم جویاتر از خود هر چه را جویا شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون الف کز مد بسم الله بیرون شد نیافت</p></div>
<div class="m2"><p>محو در نظاره ان قامت رعنا شدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون شرر بر نقد جان می لرزم از آهن دلان</p></div>
<div class="m2"><p>در ته سنگ ملامت گرچه نا پیدا شدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کور بودم تا نظر بر عیب مردم داشتم</p></div>
<div class="m2"><p>از نظر بستن به عیب خویشتن بینا شدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دام زیر خاک شد رگ در تنم از خاکمال</p></div>
<div class="m2"><p>تا چو سیل نوبهاران واصل دریا شدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از گرانسنگی به کوه قاف پهلو می زنم</p></div>
<div class="m2"><p>تا زوحشت گوشه گیر از خلق چون عنقا شدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در میان مردمان بودم به گمراهی علم</p></div>
<div class="m2"><p>رهبر عالم شدم چون خضر تا تنها شدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نامه سربسته بودم تا زبانم بسته بود</p></div>
<div class="m2"><p>چون قلم شق در دلم افتاد تا گویا شدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر سر هر برگ می لرزد دل بی حاصلم</p></div>
<div class="m2"><p>گرچه در آزادگی چون سرو پا بر جا شدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شد به کاغذ باد اوراق حواسم همسفر</p></div>
<div class="m2"><p>تا درین بستانسرا چون غنچه گل وا شدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در شکستم هر خم طاقی میان بسته ای است</p></div>
<div class="m2"><p>تا تهی از باده گلرنگ چون مینا شدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من که بودم گردباد این بیابان عافیت</p></div>
<div class="m2"><p>چون ره خوابیده بار خاطر صحرا شدم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در کنار لاله و گل دارم آتش زیرپا</p></div>
<div class="m2"><p>تا چو شبنم با خبر از عالم بالا شدم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از لگدکوب حوادث صائب ایمن نیستم</p></div>
<div class="m2"><p>در بساط خاکساری گرچه نقش پا شدم</p></div></div>