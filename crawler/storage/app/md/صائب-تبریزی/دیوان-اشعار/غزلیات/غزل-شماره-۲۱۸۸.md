---
title: >-
    غزل شمارهٔ ۲۱۸۸
---
# غزل شمارهٔ ۲۱۸۸

<div class="b" id="bn1"><div class="m1"><p>منظور من آن موی میان است و میان نیست</p></div>
<div class="m2"><p>رزق من ازان تنگ دهان است و دهان نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فریاد که آن دلبر شیرین سخن از شرم</p></div>
<div class="m2"><p>چون غنچه سراپای زبان است و زبان نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بوالعجبیهاست که شیرینی عالم</p></div>
<div class="m2"><p>مستور در آن تنگ دهان است و دهان نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این با که توان گفت که سررشته جانها</p></div>
<div class="m2"><p>وابسته به آن موی میان است و میان نیست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فریاد که از بی دهنی درد دل ما</p></div>
<div class="m2"><p>وقوف به تقریر زبان است و زبان نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نوری که بود روشن ازو دیده عالم</p></div>
<div class="m2"><p>چون مهر جهانتاب عیان است و عیان نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن جام جهانی که جهان در طلب اوست</p></div>
<div class="m2"><p>از دیده ادراک نهان است و نهان نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر چند که با هم نشود سیر و سکون جمع</p></div>
<div class="m2"><p>در صلب گهر، آب روان است و روان نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از بی بصری در نظر تنگ خسیسان</p></div>
<div class="m2"><p>یوسف به زر قلب گران است و گران نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن پیر سیه دل که مقید به خضاب است</p></div>
<div class="m2"><p>در چشم خود از جهل جوان است و جوان نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این طرفه که صائب دل صد پاره ما را</p></div>
<div class="m2"><p>شیرازه ازان موی میان است و میان نیست</p></div></div>