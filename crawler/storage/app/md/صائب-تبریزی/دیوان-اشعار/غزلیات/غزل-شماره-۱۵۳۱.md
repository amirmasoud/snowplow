---
title: >-
    غزل شمارهٔ ۱۵۳۱
---
# غزل شمارهٔ ۱۵۳۱

<div class="b" id="bn1"><div class="m1"><p>هر که را می نگرم سوخته جان افتاده است</p></div>
<div class="m2"><p>این چه برق است درین لاله ستان افتاده است؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست ممکن که به خورشید درخشان نرسد</p></div>
<div class="m2"><p>هر که چون قطره شبنم نگران افتاده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حال ما رهروان آبله پایی داند</p></div>
<div class="m2"><p>که نفس سوخته در ریگ روان افتاده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نهانخانه گوهر چه خبر خواهد داشت؟</p></div>
<div class="m2"><p>خس و خاری که ز دریا به کران افتاده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای که در کعبه خبر از دل ما می گیری</p></div>
<div class="m2"><p>روزگاری است که در دیر مغان افتاده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زود باشد سر خود در سر این کار کند</p></div>
<div class="m2"><p>چون قلم هر که به دنبال زبان افتاده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در سر کوی تو ای انجمن آرای بهار</p></div>
<div class="m2"><p>چهره زرد چو اوراق خزان افتاده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وسعت دایره مشرب ما می داند</p></div>
<div class="m2"><p>هر که چون نقطه مرکز به میان افتاده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جود کن کز دهن خالی موری بسیار</p></div>
<div class="m2"><p>رخنه در ملک سلیمان زمان افتاده است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جسم ما بر سر این عمر سبکرو صائب</p></div>
<div class="m2"><p>برگ سبزی است که در آب روان افتاده است</p></div></div>