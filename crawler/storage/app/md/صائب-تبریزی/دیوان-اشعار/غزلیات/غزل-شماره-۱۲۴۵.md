---
title: >-
    غزل شمارهٔ ۱۲۴۵
---
# غزل شمارهٔ ۱۲۴۵

<div class="b" id="bn1"><div class="m1"><p>عارض او در نقاب از دیده گستاخ کیست؟</p></div>
<div class="m2"><p>زیر ابر این آفتاب از دیده گستاخ کیست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شهسوار من ز شوخی چون نمی آید به چشم</p></div>
<div class="m2"><p>آب در چشم رکاب از دیده گستاخ کیست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون نظرها آب شد از روی آتشناک او</p></div>
<div class="m2"><p>یارب آن رو در حجاب از دیده گستاخ کیست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شرم بلبل خار در چشم هوسناکان زده است</p></div>
<div class="m2"><p>تلخی اشک گلاب از دیده گستاخ کیست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر بیاض گردن او خال دیدم، سوختم</p></div>
<div class="m2"><p>کاین نشان انتخاب از دیده گستاخ کیست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم شبنم حلقه بیرون در گردیده است</p></div>
<div class="m2"><p>نرگس او نیمخواب از دیده گستاخ کیست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست صائب شکوه از آتش دل خرسند را</p></div>
<div class="m2"><p>دود تلخ این کباب از دیده گستاخ کیست؟</p></div></div>