---
title: >-
    غزل شمارهٔ ۴۰۹۱
---
# غزل شمارهٔ ۴۰۹۱

<div class="b" id="bn1"><div class="m1"><p>هر ساغری به آن لب خندان نمی‌رسد</p></div>
<div class="m2"><p>هر تشنه‌لب به چشمه حیوان نمی‌رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آه من است در دل شب‌های انتظار</p></div>
<div class="m2"><p>طومار شکوه‌ای که به پایان نمی‌رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشق کجا و بوسه آن لعل آبدار</p></div>
<div class="m2"><p>آب گهر به خار مغیلان نمی‌رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از جوش عاشقان نشود تنگ خلق عشق</p></div>
<div class="m2"><p>تنگی ز کاروان به بیابان نمی‌رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کا مرا به مرگ نخواهد گذاشت عشق</p></div>
<div class="m2"><p>این کشتی شکسته به طوفان نمی‌رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در کشوری که پاره دل خرج می‌شود</p></div>
<div class="m2"><p>انگشتری به داد سلیمان نمی‌رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وقت خوشی چو روی دهد مغتنم شمار</p></div>
<div class="m2"><p>دایم نسیم مصر به کنعان نمی‌رسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کوتاهی از من است نه از سرو ناز من</p></div>
<div class="m2"><p>دست ز کار رفته به دامان نمی‌رسد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرچند صبح عید ز دل زنگ می‌برد</p></div>
<div class="m2"><p>صائب به فیض چاک گریبان نمی‌رسد</p></div></div>