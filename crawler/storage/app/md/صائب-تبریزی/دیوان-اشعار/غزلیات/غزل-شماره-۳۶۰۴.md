---
title: >-
    غزل شمارهٔ ۳۶۰۴
---
# غزل شمارهٔ ۳۶۰۴

<div class="b" id="bn1"><div class="m1"><p>چون ز خط صفحه رخسار تو ضایع نشود؟</p></div>
<div class="m2"><p>خط شبرنگ براتی است که راجع نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخن خوب محال است که شایع نشود</p></div>
<div class="m2"><p>نفس پاک براتی است که راجع نشود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا سبو، یا خم می، یا قدح باده کنند</p></div>
<div class="m2"><p>یک کف خاک درین میکده ضایع نشود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لازم حسن فتاده است پریشان نظری</p></div>
<div class="m2"><p>حفظ پرتو نتوان کرد که ساطع نشود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بوسه هرچند که در کیش محبت کفرست</p></div>
<div class="m2"><p>کیست لبهای ترا بیند و طامع نشود؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این لب بوسه فریبی که ترا داده خدا</p></div>
<div class="m2"><p>ترسم آیینه به دیدن ز تو قانع نشود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می شناسد همه کس سوخته عشق ترا</p></div>
<div class="m2"><p>داغ سودا نه چراغی است که لامع نشود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حسن هرچند که در پرده در آغوش آید</p></div>
<div class="m2"><p>ادب عشق محال است که مانع نشود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ورق حسن محال است نگردد صائب</p></div>
<div class="m2"><p>هیچ متبوع ندیدیم که تابع نشود</p></div></div>