---
title: >-
    غزل شمارهٔ ۶۱۶۱
---
# غزل شمارهٔ ۶۱۶۱

<div class="b" id="bn1"><div class="m1"><p>مجلس رقص است، بر تمکین بیفشان آستین</p></div>
<div class="m2"><p>دست بالا کن، گلی از عالم بالا بچین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می توان رفت از فلک بیرون به دست افشاندنی</p></div>
<div class="m2"><p>در نگین دان تا به کی باشی حصاری چون نگین؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می شود از پایکوبی قطع راه دور عشق</p></div>
<div class="m2"><p>چند از تمکین نهی بر پای، بند آهنین؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پایکوبان شو، ببین در زیر پا افلاک را</p></div>
<div class="m2"><p>دست بالا کن، جهان را زیر دست خود ببین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نخل نوخیز تو بهر بوستان دیگرست</p></div>
<div class="m2"><p>ریشه را محکم مکن زنهار در مغز زمین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می ز خون خود کن و مطرب ز بال خویشتن</p></div>
<div class="m2"><p>کم مباش از مرغ بسمل در شهادتگاه دین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فارغ است از سنگ ره تخت روان بیخودی</p></div>
<div class="m2"><p>گر طواف کعبه می خواهی بر این محمل نشین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قطره از پیوستگی شد سیل و در دریا رسید</p></div>
<div class="m2"><p>در طریق عشق، یاران موافق بر گزین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پرده ناموس را بال و پر پرواز کن</p></div>
<div class="m2"><p>حسن در هر جا که سازد چهره از می آتشین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی سپند شوخ، مجمر چشم خواب آلوده ای است</p></div>
<div class="m2"><p>بزم را پر شور گردان از نوای آتشین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رخنه ملک است چشم هوشیاران، زینهار</p></div>
<div class="m2"><p>خاک زان از درد می در چشم عقل دور بین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از شفق زد غوطه در می صبح با موی سفید</p></div>
<div class="m2"><p>در کهنسالی دکان زهد و سالوسی مچین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شبنم از روشندلی هم ساغر خورشید شد</p></div>
<div class="m2"><p>چند در مینای تن چون درد باشی ته نشین؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>می ربایندش چو گل خوبان ز دست یکدگر</p></div>
<div class="m2"><p>صفحه ای کز فکر صائب شد نگارستان چین</p></div></div>