---
title: >-
    غزل شمارهٔ ۳۶۸۴
---
# غزل شمارهٔ ۳۶۸۴

<div class="b" id="bn1"><div class="m1"><p>ملایمت سپر خصم تندخو گردد</p></div>
<div class="m2"><p>شراب شیشه شکن عاجز کدو گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به جوی رفته دگر بار آب می آید</p></div>
<div class="m2"><p>که خاک باده کشان عاقبت سبو گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر تو چشم توانی ز هر دو عالم بست</p></div>
<div class="m2"><p>دل سیاه تو آیینه دورو گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیه ز نام به چشم عقیق شد عالم</p></div>
<div class="m2"><p>دگر کسی به چه امید نامجو گردد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به حرف هیچ کس انگشت اعتراض منه</p></div>
<div class="m2"><p>که مستفید شود از تو و عدو گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی که چاشنی بوسه کرده است ادراک</p></div>
<div class="m2"><p>چسان تسلی ازان لب به گفتگو گردد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز خامشی چو توان مایه دار شد صائب</p></div>
<div class="m2"><p>چه لازم است کسی خرج گفتگو گردد؟</p></div></div>