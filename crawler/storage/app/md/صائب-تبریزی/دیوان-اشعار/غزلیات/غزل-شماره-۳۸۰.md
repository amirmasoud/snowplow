---
title: >-
    غزل شمارهٔ ۳۸۰
---
# غزل شمارهٔ ۳۸۰

<div class="b" id="bn1"><div class="m1"><p>به زلف عنبرین روبند خوبان جلوه گاهش را</p></div>
<div class="m2"><p>به نوبت پاس می دارند گلها خار راهش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دست کوته مشاطه این جرأت نمی آید</p></div>
<div class="m2"><p>مگر گردون ز پستی بشکند طرف کلاهش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به این شوکت ندارد یاد، گردون صاحب اقبالی</p></div>
<div class="m2"><p>نمی پاشد ز هم باد صبا گرد سپاهش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کند از دورباش ناز او پهلو تهی گردون</p></div>
<div class="m2"><p>چه حد دارد که در آغوش گیرد هاله ماهش را؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز شوخی گر چه می ماند به آهو چشم پر کارش</p></div>
<div class="m2"><p>شکوه پنجه شیرست مژگان سیاهش را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز دست انداز او گردد نگارین، پای سیمینش</p></div>
<div class="m2"><p>نپیچد بر کمر در جلوه، گر زلف سیاهش را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به سیر کوچه باغ خلد اگر اقبال فرماید</p></div>
<div class="m2"><p>عبیر پیرهن سازند حوران خاک راهش را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عزیز مصر تا کنعان گریبان چاک می آمد</p></div>
<div class="m2"><p>اگر می داشت یوسف در نکویی دستگاهش را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز فکر قامت رعنای او دل حسرتی دارد</p></div>
<div class="m2"><p>که چون طول امل پایان نباشد مد آهش را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ازان غارتگر ایمان و دل، رویی که من دیدم</p></div>
<div class="m2"><p>عجب دارم به رو آرند در محشر گناهش را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زد از بی تابی دل بر در بیگانگی صائب</p></div>
<div class="m2"><p>پس از عمری که با خود آشنا کردم نگاهش را</p></div></div>