---
title: >-
    غزل شمارهٔ ۵۱۴۸
---
# غزل شمارهٔ ۵۱۴۸

<div class="b" id="bn1"><div class="m1"><p>سخن عشق مدار از دل افگار دریغ</p></div>
<div class="m2"><p>که ندارند چراغ از سر بیمار دریغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قطره را سلسله موج رساند به محیط</p></div>
<div class="m2"><p>دل مدارید ازان طره طرار دریغ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن سبکروح درین راه به معراج رسد</p></div>
<div class="m2"><p>که ندارد سر خود از قدم یار دریغ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آب آن نرگس بیمار بغیر از خون نیست</p></div>
<div class="m2"><p>آب زنهار مدارید ز بیمار دریغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مکن ازلاله رخان نقد روان را امساک</p></div>
<div class="m2"><p>آب خود را نتوان داشت ز گلزار دریغ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاکساران ز تو محروم چرا می باشند؟</p></div>
<div class="m2"><p>نیست چون پرتو مهر از درو دیوار دریغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نر هاندیم ازین جسم گرانجان دل را</p></div>
<div class="m2"><p>ماند این گنج نهان درته دیوار دریغ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست جز بی ثمری حاصل پیوند جهان</p></div>
<div class="m2"><p>برگ این نخل ز افسوس بود، بار دریغ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روزی بیهده گویان جهان است افسوس</p></div>
<div class="m2"><p>هیچ خاموشی نخورده است به گفتار دریغ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ماند در سلسله طول امل گوهر دل</p></div>
<div class="m2"><p>مهره خود نربودیم ازین مار دریغ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از گران محملی خواب زمین گیر شدیم</p></div>
<div class="m2"><p>نرسیدیم به آن قافله سالار دریغ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر چه گردید دوتاقامت ما چون صیقل</p></div>
<div class="m2"><p>تیغ خود را نزدودیم ز زنگار دریغ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر چه صد غوطه درین قلزم خونخوار زدیم</p></div>
<div class="m2"><p>ره نبردیم به آن گوهر شهوار دریغ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون نکردی دل خود صاف ز زنگار، مدار</p></div>
<div class="m2"><p>سنگ راباری ازین آینه تار دریغ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر چه ازخامه من شعر با معراج رسید</p></div>
<div class="m2"><p>حیف ازان عمر که شد صرف درین کار دریغ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چاک شد چون صدف از فکر دل ما و ندید</p></div>
<div class="m2"><p>روی گرمی گهر ما ز خریدار دریغ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خورد خون من و از تنگی فرصت صائب</p></div>
<div class="m2"><p>خون خود را نگرفتم ز لب یار دریغ</p></div></div>