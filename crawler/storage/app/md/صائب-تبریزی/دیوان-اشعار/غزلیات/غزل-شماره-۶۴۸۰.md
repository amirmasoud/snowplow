---
title: >-
    غزل شمارهٔ ۶۴۸۰
---
# غزل شمارهٔ ۶۴۸۰

<div class="b" id="bn1"><div class="m1"><p>دوست را از دیگران ای عاشق شیدا مجو</p></div>
<div class="m2"><p>آنچه شد در خانه گم از دامن صحرا مجو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون هوسناکان دورویی نیست کار عاشقان</p></div>
<div class="m2"><p>در بهارستان یکرنگی گل رعنا مجو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حقه حنظل چه دارد غیر زهر جانستان؟</p></div>
<div class="m2"><p>عیش شیرین در میان قبه خضرا مجو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون صدف نسبت به ابر نوبهاران کن درست</p></div>
<div class="m2"><p>در میان بحر باش و آب از دریا مجو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ظاهرآرایان سراسر محو دیدار خودند</p></div>
<div class="m2"><p>چشم پوشیدن ز خود از صورت دیبا مجو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در رکاب دیده بیناست هر نعمت که هست</p></div>
<div class="m2"><p>از خدا چیزی به غیر از دیده بینا مجو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شبنم از همت به خورشید بلند اختر رسید</p></div>
<div class="m2"><p>شهپر پرواز غیر از همت والا مجو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مردآزار رقیبان نیستی، عاشق مشو</p></div>
<div class="m2"><p>برنمی آیی به دنیا دوستان، دنیا مجو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نخل مومین چون تواند پنجه زد با آفتاب؟</p></div>
<div class="m2"><p>صبر از ما بیش ازین ای آتشین سیما مجو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر نفس در عالمی جولان کند همچون حباب</p></div>
<div class="m2"><p>کشتی بی لنگر ما را درین دریا مجو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سر به سر گفتار صائب پخته و سنجیده است</p></div>
<div class="m2"><p>میوه خام از نهال سدره و طوبی مجو</p></div></div>