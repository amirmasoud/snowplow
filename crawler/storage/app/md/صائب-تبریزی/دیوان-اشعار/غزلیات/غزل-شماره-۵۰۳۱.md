---
title: >-
    غزل شمارهٔ ۵۰۳۱
---
# غزل شمارهٔ ۵۰۳۱

<div class="b" id="bn1"><div class="m1"><p>نمی روم به بهشت برین زخانه خویش</p></div>
<div class="m2"><p>به گل فرو شده پایم درآستانه خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گنجها نتوان درد را خرید از من</p></div>
<div class="m2"><p>به زر بدل نکنم رنگ عاشقانه خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به نغمه دگران احتیاج نیست مرا</p></div>
<div class="m2"><p>که هست چون خم می مطربم ز خانه خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو یوسفم که به چاه افتد از کنار پدر</p></div>
<div class="m2"><p>اگر به چرخ برآیم ز آستانه خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چه هر نفسم گرد کاروان غمی است</p></div>
<div class="m2"><p>بجان رسیده ام از وضع بیغمانه خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بلاست رتبه گفتار چون بلند افتاد</p></div>
<div class="m2"><p>به خواب چند توان رفتن ازافسانه خویش ؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به بینوایی و آزادگی خوشم صائب</p></div>
<div class="m2"><p>مرا قفس نفریبد به آب و دانه خویش</p></div></div>