---
title: >-
    غزل شمارهٔ ۳۴۸۰
---
# غزل شمارهٔ ۳۴۸۰

<div class="b" id="bn1"><div class="m1"><p>نه زر و سیم و نه لعل و نه گهر خواهد ماند</p></div>
<div class="m2"><p>در بساط تو همین گرد سفر خواهد ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین گلستان که به رنگینی آن مغروری</p></div>
<div class="m2"><p>مشت خاکی به تو ای باد سحر خواهد ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زینهمه لاله بی داغ که در گلزارست</p></div>
<div class="m2"><p>داغ افسوس بر اوراق جگر خواهد ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کام بی برگ و نوایان به ثمر شیرین کن</p></div>
<div class="m2"><p>در ریاضی که نه برگ و نه ثمر خواهد ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>توشه ره، دل ازین عالم فانی بردار</p></div>
<div class="m2"><p>که همین با تو ز اسباب سفر خواهد ماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون فلک وام عناصر ز تو واپس گیرد</p></div>
<div class="m2"><p>از تو ای خواجه نظر کن چه دگر خواهد ماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خشت، بالین تو سازند پرستارانت</p></div>
<div class="m2"><p>از تو هرچند دو صد بالش پر خواهد ماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این جهان آینه و هستی ما نقش و نگار</p></div>
<div class="m2"><p>نقش در آینه آخر چه قدر خواهد ماند؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عشق دل را چه خیال است به ما بگذارد؟</p></div>
<div class="m2"><p>به صدف سینه چاکی ز گهر خواهد ماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تیشه بر پای خود آن کس که درین ره نزند</p></div>
<div class="m2"><p>در دل سنگ نهان همچو شرر خواهد ماند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مشق پرواز ز بی بال و پری کن صائب</p></div>
<div class="m2"><p>که درین بادیه نه بال و نه پر خواهد ماند</p></div></div>