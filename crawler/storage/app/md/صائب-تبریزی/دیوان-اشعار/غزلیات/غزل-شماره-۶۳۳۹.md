---
title: >-
    غزل شمارهٔ ۶۳۳۹
---
# غزل شمارهٔ ۶۳۳۹

<div class="b" id="bn1"><div class="m1"><p>کرم به ابر سبکدست همچو عمان کن</p></div>
<div class="m2"><p>تمام روی زمین را رهین احسان کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز باده چهره گلرنگ را فروزان کن</p></div>
<div class="m2"><p>ز قطره های عرق بزم را چراغان کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به شکر این که جبین گشاده ای داری</p></div>
<div class="m2"><p>ملایمت به خس و خار این گلستان کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به آبروی عزیزان مگر شوی سیراب</p></div>
<div class="m2"><p>سفال تشنه خود وقف می پرستان کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هوای نفس چو گردید زیردست ترا</p></div>
<div class="m2"><p>ز باد تختگه خویش چون سلیمان کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فضای شهر مقام نفس کشیدن نیست</p></div>
<div class="m2"><p>چو گرد باد نفس راست در بیابان کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مدار فیض خود از ابر همچو بحر دریغ</p></div>
<div class="m2"><p>تمام روی زمین را رهین احسان کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شود کلید ز اعجاز عشق آخر قفل</p></div>
<div class="m2"><p>نظر به پیرهن و چشم پیر کنعان کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نظر به چشمه حیوان سیه مکن صائب</p></div>
<div class="m2"><p>به آبروی، قناعت ز آب حیوان کن</p></div></div>