---
title: >-
    غزل شمارهٔ ۶۰۹۴
---
# غزل شمارهٔ ۶۰۹۴

<div class="b" id="bn1"><div class="m1"><p>دامن خود را کشید آه سرو ناز از دست من</p></div>
<div class="m2"><p>آه کان آهوی وحشی جست باز از دست من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ره بیچارگی می آرمش در دام خویش</p></div>
<div class="m2"><p>گر به گردون می رود آن چاره ساز از دست من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از ادب هر چند کوتاه است دست جرأتم</p></div>
<div class="m2"><p>چاک ها دارد چو گل دامان ناز از دست من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صید من وحشی است، بی زحمت نمی آید به دست</p></div>
<div class="m2"><p>صد الف بر سینه دارد شاهباز از دست من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از غبار خاطرم آیینه ها دربسته شد</p></div>
<div class="m2"><p>سنگ بر دل می زند آیینه ساز از دست من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گریه شادی مرا از وصل او محروم کرد</p></div>
<div class="m2"><p>برد وسواس وضو وقت نماز از دست من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بس که پیچیدم به فکر زلف، صائب روز و شب</p></div>
<div class="m2"><p>بر جنون زد خامه معنی طراز از دست من</p></div></div>