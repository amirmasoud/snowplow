---
title: >-
    غزل شمارهٔ ۵۰۵۶
---
# غزل شمارهٔ ۵۰۵۶

<div class="b" id="bn1"><div class="m1"><p>از خط نگشته سبز لب روح پرورش</p></div>
<div class="m2"><p>ننشسته است گرد یتیمی به گوهرش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازانفعال ،مشرق پروین شود رخش</p></div>
<div class="m2"><p>گردد ز ساده لوحی اگر مه برابرش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از چشم آفتاب کند جوی خون روان</p></div>
<div class="m2"><p>درزیر زلف جلوه رخسار انورش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رنگی ز بوی پیرهنش نیست باد را</p></div>
<div class="m2"><p>از بس گرفته است قبا تنگ دربرش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر در خیال تیغ کند غمزه اش گذار</p></div>
<div class="m2"><p>ابریشم بریده شود زلف جوهرش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر مطربی که درد دلش را فشرده است</p></div>
<div class="m2"><p>سیلاب عقل و هوش بود نغمه ترش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون نخل پرشکوفه بود هرکه باد ست</p></div>
<div class="m2"><p>بی سکه خرج خاک نشینان شود زرش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون صبر برشکنجه دام و قفس کند؟</p></div>
<div class="m2"><p>آزاده ای که نقش گران است برپرش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اندیشه شکر شکند نی به ناخنش</p></div>
<div class="m2"><p>موری که کرد خاک قناعت توانگرش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر در سیاهی سخن آب حیات نیست</p></div>
<div class="m2"><p>سبزست چون همیشه خط روح پرورش ؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب به خنده هر که دهن باز می کند</p></div>
<div class="m2"><p>چون گل به خرج باد رود زود دفترش</p></div></div>