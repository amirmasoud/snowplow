---
title: >-
    غزل شمارهٔ ۱۳۹۶
---
# غزل شمارهٔ ۱۳۹۶

<div class="b" id="bn1"><div class="m1"><p>عمر اگر باقی است بوسی زان دهن خواهم گرفت</p></div>
<div class="m2"><p>خون خود را ازان لب شکرشکن خواهم گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر به هشیاری حجابش مانع احسان شود</p></div>
<div class="m2"><p>در سر مستی ازان شیرین سخن خواهم گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا به خون خود لبش را می کنم یاقوت رنگ</p></div>
<div class="m2"><p>یا عقیق آبدارش در دهن خواهم گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از لطافت گر ز آغوشم کند پهلو تهی</p></div>
<div class="m2"><p>رخصت نظاره ای زان سیمتن خواهم گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رشته هستی ز پیچ وتاب اگر کوته نشد</p></div>
<div class="m2"><p>جرعه آبی ازان چاه ذهن خواهم گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو قمری رخصت بر گرد سر گردیدنی</p></div>
<div class="m2"><p>هر چه باداباد، ازان سرو چمن خواهم گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه از مینا کسی نگرفته خون جام را</p></div>
<div class="m2"><p>خونبهای دل ازان پیمان شکن خواهم گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشم من در پاکدامانی کم از یعقوب نیست</p></div>
<div class="m2"><p>سرمه بینش ز بوی پیرهن خواهم گرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می شود پامال صائب چون شود دعوی کهن</p></div>
<div class="m2"><p>در همین جا خونبهای خویشتن خواهم گرفت</p></div></div>