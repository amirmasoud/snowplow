---
title: >-
    غزل شمارهٔ ۶۳۱۹
---
# غزل شمارهٔ ۶۳۱۹

<div class="b" id="bn1"><div class="m1"><p>گرفتم این که نظر باز می توان کردن</p></div>
<div class="m2"><p>به بال چشم چه پرواز می توان کردن؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کلاه گوشه همت اگر بلند افتد</p></div>
<div class="m2"><p>به مطلب دو جهان ناز می توان کردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کباب آتش رخسار اگر نسازی، دل</p></div>
<div class="m2"><p>سپند شعله آواز می توان کردن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر چه سینه من چاک چاک چون قفس است</p></div>
<div class="m2"><p>خزینه گهر راز می توان کردن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز موج باده اگر ناخنی به دست افتد</p></div>
<div class="m2"><p>چه عقده ها که ز دل باز می توان کردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هنوز از کف خاکستر فسرده من</p></div>
<div class="m2"><p>هزار آینه پرداز می توان کردن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به بال و پر نشود راه عشق اگر کوتاه</p></div>
<div class="m2"><p>ز دل تهیه پرواز می توان کردن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز سوز عشق زبان را اگر نصیبی هست</p></div>
<div class="m2"><p>چو شمع در دهن گاز می توان کردن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تمام درد و سراپای زخم ناسوریم</p></div>
<div class="m2"><p>به روی ما در دل باز می توان کردن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شکار ما به توجه اگر نخواهی کرد</p></div>
<div class="m2"><p>به ناوک غلط انداز می توان کردن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نمانده از شب آن زلف اگر چه پاسی پیش</p></div>
<div class="m2"><p>هنوز درد دل آغاز می توان کردن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز اهل عشق پسندیده نیست بی رحمی</p></div>
<div class="m2"><p>وگرنه خون به دل ناز می توان کردن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>علاج سینه مجروح خویش را صائب</p></div>
<div class="m2"><p>ز سیر سینه شهباز می توان کردن</p></div></div>