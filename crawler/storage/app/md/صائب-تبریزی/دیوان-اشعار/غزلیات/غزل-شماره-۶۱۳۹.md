---
title: >-
    غزل شمارهٔ ۶۱۳۹
---
# غزل شمارهٔ ۶۱۳۹

<div class="b" id="bn1"><div class="m1"><p>چون ز طرف باغ آن سرو روان آید برون</p></div>
<div class="m2"><p>گل ز دنبالش چو سنبل موکشان آید برون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ریزد از خون غزالان حرم رنگ شکار</p></div>
<div class="m2"><p>چون به عزم صید آن ابرو کمان آید برون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می گشاید جوی خون از مغز سنگ خاره را</p></div>
<div class="m2"><p>ناله هر کس چو نی از استخوان آید برون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر تمنایی که پختم زیر گردون خام شد</p></div>
<div class="m2"><p>زین تنور سرد هیهات است نان آید برون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خامه من پیشتر از نامه می گردد تمام</p></div>
<div class="m2"><p>نی سوار از دشت پر آتش چسان آید برون؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راز عشق از پرده ناموس بیرون اوفتاد</p></div>
<div class="m2"><p>چون ز تسخیر فروغ مه کتان آید برون؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آه می آید برون از سینه پر ناوکم</p></div>
<div class="m2"><p>همچو شیری کز میان نیستان آید برون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برنمی گردم به در بستن ازین بستانسرا</p></div>
<div class="m2"><p>بسته ام همت که نخل باغبان آید برون!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سایه میخانه صائب از سر ما کم مباد!</p></div>
<div class="m2"><p>هر که پیر آید به این منزل جوان آید برون</p></div></div>