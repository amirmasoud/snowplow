---
title: >-
    غزل شمارهٔ ۴۴۹۸
---
# غزل شمارهٔ ۴۴۹۸

<div class="b" id="bn1"><div class="m1"><p>دولت روشندلی زوال ندارد</p></div>
<div class="m2"><p>آب گهر بیم خشکسال ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوخته را هیچ کس دوبار نسوزد</p></div>
<div class="m2"><p>اختر اهل سخن وبال ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست کم از وصل گل ندیدن گلچین</p></div>
<div class="m2"><p>بلبل ما از قفس ملال ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاک نشینی کمال صافدلان است</p></div>
<div class="m2"><p>آب لباسی به از سفال ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ابر بهاران چرا خموش نشسته است</p></div>
<div class="m2"><p>گر صدف ما لب سؤال ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که دل خویش را چو عود نسوزد</p></div>
<div class="m2"><p>ذوق پریخوانی خیال ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از دل منعم مجو نسیم گشایش</p></div>
<div class="m2"><p>خانه تن پروران شمال ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب اگر چشم موشکاف ترا هست</p></div>
<div class="m2"><p>جامه اطلس قماش شال ندارد</p></div></div>