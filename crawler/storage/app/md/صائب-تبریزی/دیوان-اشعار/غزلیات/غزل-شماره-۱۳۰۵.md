---
title: >-
    غزل شمارهٔ ۱۳۰۵
---
# غزل شمارهٔ ۱۳۰۵

<div class="b" id="bn1"><div class="m1"><p>داغ من ممنون شکرخند پنهان تو نیست</p></div>
<div class="m2"><p>زیر بار منت گرد نمکدان تو نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست گستاخ نسیم از گلستانت کوته است</p></div>
<div class="m2"><p>هرزه خندی شیوه چاک گریبان تو نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دل سختت ندارد رحم آتشدست راه</p></div>
<div class="m2"><p>خون گرم لعل در کان بدخشان تو نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سنبل خواب پریشان روید از بالین مرا</p></div>
<div class="m2"><p>شب که در مد نظر زلف پریشان تو نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امت خضر گرانجان بودن از بی جوهری است</p></div>
<div class="m2"><p>خون ما را مصرفی چون تیغ مژگان تو نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا به چند ای کوهکن سختی کشی در بیستون؟</p></div>
<div class="m2"><p>تیشه آتش نفس گویا به فرمان تو نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می برم چون نام آغوش از کنارم می رمی</p></div>
<div class="m2"><p>این قبا چسبان به شمشاد خرامان تو نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به که در غربت بود پایم به زندان ای پدر</p></div>
<div class="m2"><p>یک قدم بی چاه در صحرای کنعان تو نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می کنم شوق ترا از روی شوق خود قیاس</p></div>
<div class="m2"><p>احتیاج نامه های شوق عنوان تو نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای نسیم پیرهن برگرد از کنعان به مصر</p></div>
<div class="m2"><p>شعله شوق مرا حاجت به دامان تو نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یوسف من زیر لب تا کی گذاری خال نیل؟</p></div>
<div class="m2"><p>این کبوتر در خور چاه زنخدان تو نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خانخانان را به بزم و رزم، صائب دیده ام</p></div>
<div class="m2"><p>در سخا و در شجاعت چون ظفرخان تو نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دیده ام صائب همه گلهای باغ هند را</p></div>
<div class="m2"><p>چون گل نشکفته باغ صفاهان تو نیست</p></div></div>