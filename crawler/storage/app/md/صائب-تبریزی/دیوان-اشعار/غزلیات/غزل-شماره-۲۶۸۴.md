---
title: >-
    غزل شمارهٔ ۲۶۸۴
---
# غزل شمارهٔ ۲۶۸۴

<div class="b" id="bn1"><div class="m1"><p>دل به دشمن چون ملایم شد مصفا می‌شود</p></div>
<div class="m2"><p>سنگ با آتش چو نرمی کرد مینا می‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای نسیم بی‌مروت باددستی واگذار</p></div>
<div class="m2"><p>صبح می‌سوزد نفس تا غنچه‌ای وامی‌شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون رود بیرون ز باغ آن یوسف گل پیرهن</p></div>
<div class="m2"><p>گل به دامنگیریش دست زلیخا می‌شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرد عصیان بحر رحمت را نمی‌آرد به جوش</p></div>
<div class="m2"><p>صاف گردد سیل چون واصل به دریا می‌شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاکساران قدردان صحبت یکدیگرند</p></div>
<div class="m2"><p>می‌جهم گردی اگر از دور پیدا می‌شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیره می‌گردد نظر از پرتو خال رخش</p></div>
<div class="m2"><p>ذره این بوم و بر خورشیدسیما می‌شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با خیال یار صحبت داشتن خوش دولتی است</p></div>
<div class="m2"><p>می‌برم غیرت بر آن عاشق که تنها می‌شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این قدر کیفیت دیدار هم می‌بوده است؟</p></div>
<div class="m2"><p>تا عرق از چهره‌اش گل کرد صهبا می‌شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب از اندیشه آن زلف و کاکل درگذر</p></div>
<div class="m2"><p>فکر چون بسیار در دل ماند سودا می‌شود</p></div></div>