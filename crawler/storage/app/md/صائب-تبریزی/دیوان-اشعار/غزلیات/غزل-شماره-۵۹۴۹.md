---
title: >-
    غزل شمارهٔ ۵۹۴۹
---
# غزل شمارهٔ ۵۹۴۹

<div class="b" id="bn1"><div class="m1"><p>چون چشم آبگینه، هر چند پاک بینم</p></div>
<div class="m2"><p>در پرده خجالت، زان روی شرمگینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از زلف مشکبویان مغزم شود پریشان</p></div>
<div class="m2"><p>تا ریشه کرد در دل آن خط عنبرینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک برگ کاه ایشان بی کوه منتی نیست</p></div>
<div class="m2"><p>از خرمن بزرگان عمری است خوشه چینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افغان که همچو پرگار با پای آهنین من</p></div>
<div class="m2"><p>چندان که می زنم دور در گام اولینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی بمیر تا من از نو دهم حیاتت</p></div>
<div class="m2"><p>ای روح بخش عالم من مرده همینم!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رازی که در دلم هست صائب ز طینت پاک</p></div>
<div class="m2"><p>چون آب می توان خواند از صفحه جبینم</p></div></div>