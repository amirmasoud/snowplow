---
title: >-
    غزل شمارهٔ ۱۰۲۷
---
# غزل شمارهٔ ۱۰۲۷

<div class="b" id="bn1"><div class="m1"><p>از تن خاکی به جد و جهد رستن مشکل است</p></div>
<div class="m2"><p>رشته جان را به زور خود گسستن مشکل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رستمی باید که بیژن را برون آرد ز چاه</p></div>
<div class="m2"><p>بی کمند جذبه از دنیا گسستن مشکل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در تنور سرد خودداری نمی آید ز نان</p></div>
<div class="m2"><p>درد و داغ عشق را بر خویش بستن مشکل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی دل روشن خداجویی خیال باطلی است</p></div>
<div class="m2"><p>این گهر را با چراغ مرده جستن مشکل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در جهان آفرینش ذره ای بیکار نیست</p></div>
<div class="m2"><p>در چنین هنگامه ای فارغ نشستن مشکل است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زندگی چون گشت از قد دو تا پا در رکاب</p></div>
<div class="m2"><p>از سرانجام سفر غافل نشستن مشکل است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از فضای حق مشو غافل که با این مشت خاک</p></div>
<div class="m2"><p>پیش این سیلاب بی زنهار بستن مشکل است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا نباشد آتشی در زیر پایت چون سپند</p></div>
<div class="m2"><p>صائب از هنگامه ایجاد جستن مشکل است</p></div></div>