---
title: >-
    غزل شمارهٔ ۴۱۱۰
---
# غزل شمارهٔ ۴۱۱۰

<div class="b" id="bn1"><div class="m1"><p>آیینه‌ام ز روشنی آزار می‌کشد</p></div>
<div class="m2"><p>خاطر به سیر سبزه زنگار می‌کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با زاهدان خشک مگو حرف حق بلند</p></div>
<div class="m2"><p>منصور را ببین که چه از دار می‌کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این بوستان کیست که مژگان آفتاب</p></div>
<div class="m2"><p>چون خار گردن از سر دیوار می‌کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درمانده ملایمت من شده است خصم</p></div>
<div class="m2"><p>اینجا ز موم نشتر آزار می‌کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواهد به ابر پنبه زدن برق داغ من</p></div>
<div class="m2"><p>این گل سری به گوشه دستار می‌کشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن زلف مشکبار که یادش بخیر باد</p></div>
<div class="m2"><p>یارب چه دور از آن گل‌رخسار می‌کشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از چشمه‌سار آبله‌ام آب می‌خورد</p></div>
<div class="m2"><p>خاری که نیشتر از دهن مار می‌کشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای دوست غافلی که درین یک دو روزه هجر</p></div>
<div class="m2"><p>صائب چه‌ها ز چرخ ستمکار می‌کشد</p></div></div>