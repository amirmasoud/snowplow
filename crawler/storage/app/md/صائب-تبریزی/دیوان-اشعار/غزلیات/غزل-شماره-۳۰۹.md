---
title: >-
    غزل شمارهٔ ۳۰۹
---
# غزل شمارهٔ ۳۰۹

<div class="b" id="bn1"><div class="m1"><p>در بهاران از چمن ای باغبان بیرون میا</p></div>
<div class="m2"><p>تا گلی دربار هست از گلستان بیرون میا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نمی گردد سری از سایه ات اقبالمند</p></div>
<div class="m2"><p>ای هما در روز ابر از آشیان بیرون میا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قطره باران ز فیض گوشه گیری شد گهر</p></div>
<div class="m2"><p>زینهار از خلوت ای روشن روان بیرون میا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش دمسردان زبان گفتگو در کام کش</p></div>
<div class="m2"><p>از غلاف ای برگ در فصل خزان بیرون میا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می شوی از قیمت نازل سبک چون ماه مصر</p></div>
<div class="m2"><p>زینهار از چه به امداد خسان بیرون میا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیر کج را گوشه گیری پرده پوشی می کند</p></div>
<div class="m2"><p>تا نسازی راست خود را، از کمان بیرون میا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اتفاق رهروان با هم دعای جوشن است</p></div>
<div class="m2"><p>در بیابان طلب از کاروان بیرون میا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با دل خرسند قانع شو ز فکر آب و نان</p></div>
<div class="m2"><p>بهر گندم از بهشت جاودان بیرون میا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زندگی را کن سپرداری به مهر خامشی</p></div>
<div class="m2"><p>چون زبان مار هر دم از دهان بیرون میا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در کنار بحر بیش از بحر می باشد خطر</p></div>
<div class="m2"><p>پا به دامن کش چو مرکز از میان بیرون میا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قطره در اندیشه دریا چو باشد واصل است</p></div>
<div class="m2"><p>هر کجا باشی ز فکر دلستان بیرون میا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا نسازی قطره بی قیمت خود را گهر</p></div>
<div class="m2"><p>چون صدف از قعر بحر بیکران بیرون میا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نیست حق تربیت صائب فرامش کردنی</p></div>
<div class="m2"><p>در برومندی ز فکر باغبان بیرون میا</p></div></div>