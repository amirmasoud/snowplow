---
title: >-
    غزل شمارهٔ ۱۲۰۸
---
# غزل شمارهٔ ۱۲۰۸

<div class="b" id="bn1"><div class="m1"><p>هر که دل در غمزه خونریز آن جلاد بست</p></div>
<div class="m2"><p>رشته جان بر زبان نشتر فصاد بست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سنگ اگر در مرگ عاشق خون نمی گرید، چرا</p></div>
<div class="m2"><p>بیستون از لاله نخل ماتم فرهاد بست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رشته بی تابی غیرت اگر باشد رسا</p></div>
<div class="m2"><p>می توان بر چوب دست شانه شمشاد بست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناله بلبل نیفشارد اگر دل غنچه را</p></div>
<div class="m2"><p>چون جرس یک لحظه نتواند لب از فریاد بست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرده ام لوح مزار خویش از سنگ فسان</p></div>
<div class="m2"><p>زنگ اگر از خون من آن خنجر فولاد بست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بال سیر شعله جواله بستن مشکل است</p></div>
<div class="m2"><p>نقش شیرین را چسان در بیستون فرهاد بست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر رخ بحر از نسیم آه سرد من حباب</p></div>
<div class="m2"><p>سخت تر صد پیرهن از بیضه فولاد بست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرمه سا چشمی که من زان مجلس آرا دیده ام</p></div>
<div class="m2"><p>بر گلوی شیشه بتواند ره فریاد بست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون زبان مار، خار آشیانم می گزد</p></div>
<div class="m2"><p>تا در فیض قفس بر روی من صیاد بست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شمع را در وقت کشتن چشم بستن رسم نیست</p></div>
<div class="m2"><p>حیرتی دارم که چون چشم مرا جلاد بست؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بس که صائب از نگاه عجز من خون می چکید</p></div>
<div class="m2"><p>دیده خود را به وقت کشتنم جلاد بست</p></div></div>