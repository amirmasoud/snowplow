---
title: >-
    غزل شمارهٔ ۴۷۲
---
# غزل شمارهٔ ۴۷۲

<div class="b" id="bn1"><div class="m1"><p>به هر شورش مده چون موج از کف دامن دریا</p></div>
<div class="m2"><p>که باشد عقد گوهر خوشه ای از خرمن دریا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وصال دایمی افسرده سازد شوق عاشق را</p></div>
<div class="m2"><p>سری گاهی بر آور چون حباب از روزن دریا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو موج آن کس که داد از کف عنان اختیار خود</p></div>
<div class="m2"><p>حمایل ساخت دست خویش را بر گردن دریا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صفای دل مرا آزاد کرد از قید خودبینی</p></div>
<div class="m2"><p>که نتوان دید عکس خود در آب روشن دریا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به خاموشی توان شد گوهر اسرار را محرم</p></div>
<div class="m2"><p>صدف تابست از گفتار لب، شد مخزن دریا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز خواب خوش به روی دولت بیدار برخیزد</p></div>
<div class="m2"><p>حبابی را که باشد خوابگاه از دامن دریا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز طوفان حوادث عاشقان را نیست پروایی</p></div>
<div class="m2"><p>نیندیشد نهنگ پر دل از آشفتن دریا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گوارا می کند مشرب به خود ناسازگاران را</p></div>
<div class="m2"><p>بود ماهی گل بی خار در پیراهن دریا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز لنگر تا کدامین کشتی بی ظرف می لافد؟</p></div>
<div class="m2"><p>که برمی خیزد از موج خطر مو بر تن دریا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز تردستی زمین ها را کند گنجینه گوهر</p></div>
<div class="m2"><p>چو ابر آن کس که باشد خوشه چین خرمن دریا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز دست گوهرافشان برگ عیش تنگدستان شو</p></div>
<div class="m2"><p>که فلس ماهیان گردد دعای جوشن دریا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به دریا غوطه زن گر گوهر شهوار می خواهی</p></div>
<div class="m2"><p>که غیر از کف نباشد حاصل از پیرامن دریا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بزرگان را کند تردستی از آفت سپرداری</p></div>
<div class="m2"><p>که از موج گهر باشد دعای جوشن دریا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز خون بی گناهان تیغ او را نیست پروایی</p></div>
<div class="m2"><p>نگیرد پنجه خونین مرجان دامن دریا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برآ از پرده شرم و حیا صائب که می گردد</p></div>
<div class="m2"><p>حباب از شوخ چشمی تکمه پیراهن دریا</p></div></div>