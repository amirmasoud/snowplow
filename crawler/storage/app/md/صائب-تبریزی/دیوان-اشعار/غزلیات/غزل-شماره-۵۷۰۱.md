---
title: >-
    غزل شمارهٔ ۵۷۰۱
---
# غزل شمارهٔ ۵۷۰۱

<div class="b" id="bn1"><div class="m1"><p>به توبه راهنمون گشت باده نابم</p></div>
<div class="m2"><p>کمند دولت بیدار شد رگ خوابم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا به گوشه ظلمت سرای خود ببرید</p></div>
<div class="m2"><p>که زخم دیده نمکسود شد ز مهتابم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به پای خم برسانید سجده ای از من</p></div>
<div class="m2"><p>که زنده در ته دیوار کرد محرابم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه عقده وا شود از دل به زهد خشک مرا</p></div>
<div class="m2"><p>چه دانه خرد کند آسیای بی آبم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به حکمت از لب من مهر خامشی بردار</p></div>
<div class="m2"><p>که پر چو کوزه سربسته از می نابم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من رمیده کجا تنگنای چرخ کجا</p></div>
<div class="m2"><p>حریف شیشه سر بسته نیست سیمابم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز من تلاطم این بحر بیکنار مپرس</p></div>
<div class="m2"><p>که خوشتر از کمر وحدت است گردابم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شده است یک گره از پیچ و تاب رشته من</p></div>
<div class="m2"><p>هنوز چرخ سبکدست می دهد تابم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نشد به یار رسد نامه شکایت من</p></div>
<div class="m2"><p>غبار گشت به نزدیک بحر سیلابم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زبان شکوه بود سبزه تخم سوخته را</p></div>
<div class="m2"><p>از آن نمی دهد این چرخ شیشه دل آبم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز چشم شور فلک امن نیستم صائب</p></div>
<div class="m2"><p>و گر نه در گذر سیل می برد خوابم</p></div></div>