---
title: >-
    غزل شمارهٔ ۵۵۸
---
# غزل شمارهٔ ۵۵۸

<div class="b" id="bn1"><div class="m1"><p>خوش کن از لاله رخان زلف پریشانی را</p></div>
<div class="m2"><p>از دل گرم برافروز شبستانی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گریه با سینه سوزان چه تواند کردن؟</p></div>
<div class="m2"><p>نکند آبله سیراب، بیابانی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باده خوب است به اندازه ساغر باشد</p></div>
<div class="m2"><p>چه کند بلبل بی ظرف، گلستانی را؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا نرفته است سر رشته فرصت از دست</p></div>
<div class="m2"><p>به که شیرازه شوی جمع پریشانی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر همه خانه کعبه است، که تعمیر مکن</p></div>
<div class="m2"><p>تا توان کرد عمارت دل ویرانی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عالم از تشنه لبان یک جگر سوخته است</p></div>
<div class="m2"><p>به که بخشد لب او قطره بارانی را؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اختیار لب خود را به خط سبز مده</p></div>
<div class="m2"><p>نتوان داد به طوطی شکرستانی را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که از دست زلیخای هوس سالم جست</p></div>
<div class="m2"><p>به دو عالم ندهد گوشه زندانی را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از شکرخنده بی پرده گلها پیداست</p></div>
<div class="m2"><p>که ندیده است گلستان لب خندانی را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حلقه گوش کند حرف پریشان سخنان</p></div>
<div class="m2"><p>هر که دیده است سر زلف پریشانی را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پیش آن کان ملاحت، دهن خوبان چیست؟</p></div>
<div class="m2"><p>در نمکزار چه قدرست نمکدانی را؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عالم خاک، برومند ز بالای تو شد</p></div>
<div class="m2"><p>بهر یک سرو دهند آب، خیابانی را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خبرش نیست که آیینه ز طوطی چه کشید</p></div>
<div class="m2"><p>به سخن هر که نیاورد سخندانی را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در عنانداری چشم تر من حیران است</p></div>
<div class="m2"><p>در تنور آن که گره ساخته طوفانی را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به صف آرایی خود محشر ازان می نازد</p></div>
<div class="m2"><p>که ندیده است صف آرایی مژگانی را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وقت بسیار عزیزست، گرامی دارش</p></div>
<div class="m2"><p>به زر قلب مده یوسف کنعانی را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دل به آن چشم به افسانه و افسون مدهید</p></div>
<div class="m2"><p>که به کافر نتوان داد مسلمانی را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در هزاران نظر شوخ نباشد صائب</p></div>
<div class="m2"><p>آنچه در پرده بود دیده حیرانی را</p></div></div>