---
title: >-
    غزل شمارهٔ ۶۶۳۴
---
# غزل شمارهٔ ۶۶۳۴

<div class="b" id="bn1"><div class="m1"><p>از حسن تو یک رقعه به گلزار رسیده</p></div>
<div class="m2"><p>از زلف تو یک نافه به تاتار رسیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان دست که حسن تو فشانده است به گلزار</p></div>
<div class="m2"><p>دامان پر از گل به خس و خار رسیده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دیدن گل مست و خرابند جهانی</p></div>
<div class="m2"><p>این جام همانا به لب یار رسیده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کو دیده یعقوب که بی پرده ببیند</p></div>
<div class="m2"><p>صد قافله از مصر به یکباره رسیده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاخ گل ازان جلوه مستانه که دارد</p></div>
<div class="m2"><p>پیداست که از خانه خمار رسیده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ظلم است کسی خرده جان را نکند خرج</p></div>
<div class="m2"><p>امروز که گل بر سر بازار رسیده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دامان نسیم سحری گیر و روان شو</p></div>
<div class="m2"><p>کز غیب رسولی ا ست به این کار رسیده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کاشانه اش از نقش مرادست نگارین</p></div>
<div class="m2"><p>چشمی که به آن آینه رخسار رسیده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیگر چه خیال است که از سینه کند یاد</p></div>
<div class="m2"><p>هر دل که به آن طره طرار رسیده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از کوچه آن زلف که سالم بدر آید؟</p></div>
<div class="m2"><p>کآنجا سر خورشید به دیوار رسیده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از شور قیامت بودش مرهم کافور</p></div>
<div class="m2"><p>زخمی که مرا بر دل افگار رسیده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از شرم برون آی که تسلیم نمایم</p></div>
<div class="m2"><p>جانی که مرا بر لب اظهار رسیده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صائب زند آتش به جهان از نفس گرم</p></div>
<div class="m2"><p>هر نی که به آن لعل شکربار رسیده</p></div></div>