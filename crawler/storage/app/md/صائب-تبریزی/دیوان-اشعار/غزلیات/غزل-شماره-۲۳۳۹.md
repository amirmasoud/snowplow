---
title: >-
    غزل شمارهٔ ۲۳۳۹
---
# غزل شمارهٔ ۲۳۳۹

<div class="b" id="bn1"><div class="m1"><p>تا به کی در خواب سنگین روزگارم بگذرد</p></div>
<div class="m2"><p>زندگی در سنگ خارا چون شرارم بگذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند اوقات گرامی همچو طفل نوسواد</p></div>
<div class="m2"><p>در ورق گردانی لیل و نهارم بگذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس که ناز کارنشناسان ملولم ساخته است</p></div>
<div class="m2"><p>دست می مالم به هم تا وقت کارم بگذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون چراغ کشته گیرم زندگانی را زسر</p></div>
<div class="m2"><p>آتشین رخساره ای گر بر مزارم بگذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از شکوه خاکساری بحر با آن دستگاه</p></div>
<div class="m2"><p>می شود باریک تا از جویبارم بگذرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز انتظار تیغ عمری شد که گردن می کشم</p></div>
<div class="m2"><p>آه اگر صیاد غافل از شکارم بگذرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در محیط من به جان خویش می لرزد خطر</p></div>
<div class="m2"><p>کیست طوفان تا زبحر بیکنارم بگذرد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بار منت بر نمی تابد دل آزاده ام</p></div>
<div class="m2"><p>غنچه گردم گر نسیم از شاخسارم بگذرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با خیال او قناعت می کنم، من کیستم</p></div>
<div class="m2"><p>تا وصالش در دل امیدوارم بگذرد؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون کشم آه از دل پر خون، که باد خوش عنان</p></div>
<div class="m2"><p>می خورد صدکاسه خون کز لاله زارم بگذرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با ضعیفی بر زبردستان عالم غالبم</p></div>
<div class="m2"><p>برق می لرزد به جان کز خارزارم بگذرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از دل پردرد و داغم زهره می بازد پلنگ</p></div>
<div class="m2"><p>پر بریزد گر عقاب از کوهسارم بگذرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من که چون خورشید تابان لعل سازم سنگ را</p></div>
<div class="m2"><p>از شفق صائب به خون دل مدارم بگذرد</p></div></div>