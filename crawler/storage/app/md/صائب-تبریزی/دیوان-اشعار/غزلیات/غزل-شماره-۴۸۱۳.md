---
title: >-
    غزل شمارهٔ ۴۸۱۳
---
# غزل شمارهٔ ۴۸۱۳

<div class="b" id="bn1"><div class="m1"><p>بالا نکرده ساعد او راحیا هنوز</p></div>
<div class="m2"><p>بیعت نکرده است به دستش حنا هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آواز عندلیب به گوشش نخورده است</p></div>
<div class="m2"><p>برگرد او نگشته نسیم صبا هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در غنچه است جلوه گلزار شوخی اش</p></div>
<div class="m2"><p>دستش گلی نچیده ز رنگ حنا هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل عیب بیوفایی خود را علاج کرد</p></div>
<div class="m2"><p>نشنیده است عهد تو بوی وفا هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صدبار چین ابروی او داد رخصتم</p></div>
<div class="m2"><p>من سر نمی کشم ز کمند وفا هنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای دیده از غبار ره او چه دیده ای</p></div>
<div class="m2"><p>در پرده است خاصیت توتیا هنوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب هزار قاصد یأس آمد و گذشت</p></div>
<div class="m2"><p>چون برق می پرد به رهش چشم ما هنوز</p></div></div>