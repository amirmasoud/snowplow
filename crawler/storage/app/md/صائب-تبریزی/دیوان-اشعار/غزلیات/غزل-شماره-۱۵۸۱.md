---
title: >-
    غزل شمارهٔ ۱۵۸۱
---
# غزل شمارهٔ ۱۵۸۱

<div class="b" id="bn1"><div class="m1"><p>عشق را با دل صد پاره من کاری هست</p></div>
<div class="m2"><p>در دل غنچه من خرده اسراری هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو طوطی سخنش نقل مجالس گردد</p></div>
<div class="m2"><p>هر که را پیش نظر آینه رخساری هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبنم بی ادب از دور زمین می بوسد</p></div>
<div class="m2"><p>گلستانی که در او مرغ گرفتاری هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواب ما از ره خوابیده گرانخواب ترست</p></div>
<div class="m2"><p>زین چه حاصل که پی قافله بیداری هست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلبلی را که به دیدار ز گل قانع شد</p></div>
<div class="m2"><p>در اگر بسته شود رخنه دیواری هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می توان فیض بهار از نفس گرمش یافت</p></div>
<div class="m2"><p>هر که را در جگر از تازه گلی خاری هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باد دستی است که باری ز دلی بردارد</p></div>
<div class="m2"><p>گر درین قافله امروز سبکباری هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرد بر دامن گلها ز خزان نشیند</p></div>
<div class="m2"><p>در ریاضی که رگ ابر گهرباری هست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکوه از بی نمکیهای جهان بی دردی است</p></div>
<div class="m2"><p>بی نمک نیست جهان گر دل افگاری هست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیش من گرد کسادی و یتیمی است یکی</p></div>
<div class="m2"><p>گوهر من چه شناسد که خریداری هست؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ظلمت و نور درین نشأه به هم پیوسته است</p></div>
<div class="m2"><p>هر کجا آینه ای هست، سیه کاری هست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیست سودی که زیانش نبود در دنبال</p></div>
<div class="m2"><p>بار می بندم ازان شهر که بازاری هست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نشود خرج خزان برگ نشاطش صائب</p></div>
<div class="m2"><p>در چمن شاخ گلی را که هواداری هست</p></div></div>