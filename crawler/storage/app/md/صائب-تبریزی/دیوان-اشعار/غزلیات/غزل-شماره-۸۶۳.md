---
title: >-
    غزل شمارهٔ ۸۶۳
---
# غزل شمارهٔ ۸۶۳

<div class="b" id="bn1"><div class="m1"><p>صبح روشن شد، بده ساقی می چون آفتاب</p></div>
<div class="m2"><p>تا به روی دولت بیدار برخیزم ز خواب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که در میخانه بردارد ز روی صدق دست</p></div>
<div class="m2"><p>از بیاض گردن مینا شود مالک رقاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماهیان از بی زبانی بحر بر سر می کشند</p></div>
<div class="m2"><p>گفتگو داروی بیهوشی است در بزم شراب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عذر بیداد رخ او را خط از عشاق خواست</p></div>
<div class="m2"><p>راه خود را پاک سازد خون چو گردد مشک ناب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از خط شبرنگ گفتم شرم او کمتر شود</p></div>
<div class="m2"><p>پرده دیگر ز خط افزود بر شرم و حجاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در زمان خط، مدار چشم او بر مردمی است</p></div>
<div class="m2"><p>گردن عامل بود باریک در پای حساب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از نگاه گرم، چون مویی که بر آتش نهند</p></div>
<div class="m2"><p>می شود افزون میان نازکش را پیچ و تاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فیض گردون بلنداختر بود ز اقبال عشق</p></div>
<div class="m2"><p>تاج بخشی می کند از همت دریا حباب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کیمیای دانه احسان، زمین قابل است</p></div>
<div class="m2"><p>گوهر شهوار گردد در صدف اشک سحاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مردم بی برگ را اسباب عیش آماده است</p></div>
<div class="m2"><p>بستر خار است، در هر جا که سنگین گشت خواب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ایمنی جستم ز ویرانی، ندانستم که چرخ</p></div>
<div class="m2"><p>گنج خواهد خواست جای باج ازین ملک خراب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در بلندی ناله صائب ندارد کوتهی</p></div>
<div class="m2"><p>کوه تمکین تو می سازد صدا را بی جواب</p></div></div>