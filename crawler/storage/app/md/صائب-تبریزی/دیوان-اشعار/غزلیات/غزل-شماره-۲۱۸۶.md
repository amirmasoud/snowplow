---
title: >-
    غزل شمارهٔ ۲۱۸۶
---
# غزل شمارهٔ ۲۱۸۶

<div class="b" id="bn1"><div class="m1"><p>آن را که بود تیغ زبان بی لب نان نیست</p></div>
<div class="m2"><p>روزی ز دل خود بود آن را که دهان نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محتاج به دریا نبود گوهر سیراب</p></div>
<div class="m2"><p>در ملک قناعت دل و چشم نگران نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر درد کشان ظلمت ایام بود صاف</p></div>
<div class="m2"><p>بر خاطر ما ابر شب جمعه گران نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این پخته نمایان همه خامند سراسر</p></div>
<div class="m2"><p>یک داغ جگرسوز درین لاله ستان نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل را تهی از شکوه به گفتار توان کرد</p></div>
<div class="m2"><p>بسیار بود حرف کسی را که زبان نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از قرب کجان، راست برآرد به ستم دست</p></div>
<div class="m2"><p>از تیرچه اندیشه، چو در بحر کمان نیست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>امید خراج از عدم آباد، فضولی است</p></div>
<div class="m2"><p>ما را طمع بوسه ازان غنچه دهان نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگذاشت نفس رات کنم عمر سبکسیر</p></div>
<div class="m2"><p>آرام درین قافله چون ریگ روان نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کوتاه نظر عاقبت اندیش نباشد</p></div>
<div class="m2"><p>تیری که هوایی است مقید به نشان نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکرنگ بود سال و مه کوی خرابات</p></div>
<div class="m2"><p>اینجا شب آدینه و روز رمضان نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کفاره تقصیر بود خواب پریشان</p></div>
<div class="m2"><p>ما را گله ای صائب از اوضاع جهان نیست</p></div></div>