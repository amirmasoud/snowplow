---
title: >-
    غزل شمارهٔ ۴۴۷۰
---
# غزل شمارهٔ ۴۴۷۰

<div class="b" id="bn1"><div class="m1"><p>دولت چو نیست باقی بر باد رفته باشد</p></div>
<div class="m2"><p>خوابی که از خیال است از یاد رفته باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از جمع و خرج هستی چون حاصلی نداریم</p></div>
<div class="m2"><p>اوراق زندگانی بر باد رفته باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکس که زندگی را در بندگی سرآورد</p></div>
<div class="m2"><p>امید هست از اینجا آزاد رفته باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین صیدگاه ما را دل‌بستگی به دام است</p></div>
<div class="m2"><p>چون دام هست در خاک صیاد رفته باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیچیده است در کوه آواز تیشه او</p></div>
<div class="m2"><p>هرچند از نظرها فرهاد رفته باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در جمع کردن دل کوشش به جاست ما را</p></div>
<div class="m2"><p>گر زین خرابه یک دل آباد رفته باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از امتداد هجران ترسی که دارم این است</p></div>
<div class="m2"><p>کز یاد او مبادا بیداد رفته باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر عمر رفته افسوس صاحبدلان ندارند</p></div>
<div class="m2"><p>خرمن چو پاک گردید گو باد رفته باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بعد از هلاک گشتن دردی که دارم این است</p></div>
<div class="m2"><p>کز دل غمش مبادا ناشاد رفته باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با یاد آن یگانه صائب اگر دو عالم</p></div>
<div class="m2"><p>از یاد رفته باشد از یاد رفته باشد</p></div></div>