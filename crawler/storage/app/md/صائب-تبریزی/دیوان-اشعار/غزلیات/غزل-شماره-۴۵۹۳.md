---
title: >-
    غزل شمارهٔ ۴۵۹۳
---
# غزل شمارهٔ ۴۵۹۳

<div class="b" id="bn1"><div class="m1"><p>شد خرابات مغان از توبه ام زیر و زبر</p></div>
<div class="m2"><p>می زند باد مخالف بحر را بر یکدگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>توبه من بازگشت عالمی راشد سبب</p></div>
<div class="m2"><p>لشکری را گاه بیدل می کند یک بیجگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از لب میگون او قانع به دشنامم که می</p></div>
<div class="m2"><p>از رگ تلخی دواند ریشه دردل بیشتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درنمی آید به چشم موشکاف از نازکی</p></div>
<div class="m2"><p>ورنه بیش از جوهر تیغ است تاب آن کمر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خط به لعل آتشین یارشد خضر رهم</p></div>
<div class="m2"><p>روز روشن دود می گرددبه آتش راهبر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست جز کاهش نصیب عاشق از سیمین بران</p></div>
<div class="m2"><p>رنج باریک است رزق رشته از قرب گهر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بس که چرخ آهنین بازو مرا درهم فشرد</p></div>
<div class="m2"><p>مغزمن صد پیرهن ازاستخوان شد خشک تر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رشته اشک از درازی درنمی آید به چشم</p></div>
<div class="m2"><p>دستگاه خنده است از چشم سوزن تنگتر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شهپر زرین زنور خود چو طاوسش دهد</p></div>
<div class="m2"><p>شمع اگر پروانه راسوزد به ظاهر بال وپر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیستم نومید از تردستی پیر مغان</p></div>
<div class="m2"><p>چون سبوهر چند دستم خشک شد در زیر سر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حسن رافیض نظر بازان کند صائب تمام</p></div>
<div class="m2"><p>تا نپیوندد به شبنم گل نگردد دیده ور</p></div></div>