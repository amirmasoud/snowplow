---
title: >-
    غزل شمارهٔ ۲۳۸۸
---
# غزل شمارهٔ ۲۳۸۸

<div class="b" id="bn1"><div class="m1"><p>عیب‌جو چندان که عیب از ما به در می‌آورد</p></div>
<div class="m2"><p>غیرت ما زور بر کسب هنر می‌آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک دل آگاه گمراهان عالم را بس است</p></div>
<div class="m2"><p>کاروانی را به منزل راهبر می‌آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لطف عام او عجب دارم نصیب من شود</p></div>
<div class="m2"><p>با چنین بختی که از دریا خبر می‌آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد برومند از سر منصور چوب خشک دار</p></div>
<div class="m2"><p>در چه موسم نخل ما یارب ثمر می‌آورد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌برد چندان که از هوشم دو چشمِ مست او</p></div>
<div class="m2"><p>موکشانم باز آن موی کمر می‌آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیرچشمان را غرض از جمع دنیا ترک اوست</p></div>
<div class="m2"><p>سکته بهر پشت کردن رو به زر می‌آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکه چون غواص می‌سازد نفس در دل گره</p></div>
<div class="m2"><p>صائب از دریا برون عقد گهر می‌آورد</p></div></div>