---
title: >-
    غزل شمارهٔ ۱۲۶۰
---
# غزل شمارهٔ ۱۲۶۰

<div class="b" id="bn1"><div class="m1"><p>پاره های دل گران بر دیده خونبار نیست</p></div>
<div class="m2"><p>جای در چشم است آن کس را که بر دل بار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غافلان اندیشه از سنگ ملامت می کنند</p></div>
<div class="m2"><p>ورنه کبک مست را پروایی از کهسار نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرده خواب است ظلمت روشنایی دیده را</p></div>
<div class="m2"><p>چشم پوشیدن ز اوضاع جهان دشوار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش ما کوتاه دستان کز هوس آزاده ایم</p></div>
<div class="m2"><p>خار بی گل در صفا کم از گل بی خار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرمه سازد سنگ را برق نگاه احتیاط</p></div>
<div class="m2"><p>پیش عاقل سنگلاخ دهر ناهموار نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غفلت ما بی شعوران را نمی باید سبب</p></div>
<div class="m2"><p>پای خواب آلود را افسانه ای در کار نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیم و زر چون آب شد، از بوته پاک آید برون</p></div>
<div class="m2"><p>با خجالت جرم را حاجت به استغفار نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیستون در پنجه فرهاد شد چون موم نرم</p></div>
<div class="m2"><p>عاشقان را احتیاج زر دست افشار نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در ته پیراهن آیینه شکر می خورند</p></div>
<div class="m2"><p>طوطیان را گر به ظاهر نسبت زنگار نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون فلاخن هر که نگشاید بغل از شوق سنگ</p></div>
<div class="m2"><p>پیش این کودک مزاجان قابل آزار نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر سمندر شعله جانسوز آب زندگی است</p></div>
<div class="m2"><p>عشق چون باشد، در آتش زندگی دشوار نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می گریزند از خیال یار وحشت پیشگان</p></div>
<div class="m2"><p>بوی گل را در حریم بی دماغان بار نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>غافلند از مرگ، مردم، ورنه در روی زمین</p></div>
<div class="m2"><p>کیست کز تن آفتابش بر لب دیوار نیست؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خورد عالم را و بندد بر شکم سنگ مزار</p></div>
<div class="m2"><p>سیر چشمی در بساط خاک مردمخوار نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آنچه باید کم نمی گردد، که در ایام دی</p></div>
<div class="m2"><p>نخل ها بی برگ گردد سایه چون در کار نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ذوق طفلی در نمی یابند تمکین پیشگان</p></div>
<div class="m2"><p>هر کجا دیوانه ای در کوچه و بازار نیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از دل مجروح صائب شور عالم را بپرس</p></div>
<div class="m2"><p>بی نمک داند جهان را هر دلی کافگار نیست</p></div></div>