---
title: >-
    غزل شمارهٔ ۱۴۷۷
---
# غزل شمارهٔ ۱۴۷۷

<div class="b" id="bn1"><div class="m1"><p>ناله سوخته جانان به اثر نزدیک است</p></div>
<div class="m2"><p>دست خورشید به دامان سحر نزدیک است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قسمت من چو صدف چون لب خشک است از بحر</p></div>
<div class="m2"><p>زین چه حاصل که به من آب گهر نزدیک است؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وصل با کوتهی دست ندارد ثمری</p></div>
<div class="m2"><p>بهله رازین چه که دستش به کمر نزدیک است؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صرف خمیازه آغوش شود اوقاتش</p></div>
<div class="m2"><p>هاله هر چند به ظاهر به قمر نزدیک است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی دنیای فرومایه به بی رویان است</p></div>
<div class="m2"><p>همه جا پشت ز آیینه به زر نزدیک است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل ز خط زودتر از زلف شود کامروا</p></div>
<div class="m2"><p>شب ایام بهاران به سحر نزدیک است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کار آتش کند آبی که به تلخی بخشند</p></div>
<div class="m2"><p>ورنه دریا به من تشنه جگر نزدیک است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سکه سان رویی از آهن به کف آور صائب</p></div>
<div class="m2"><p>کاین متاعی است که امروز به زر نزدیک است</p></div></div>