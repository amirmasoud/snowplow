---
title: >-
    غزل شمارهٔ ۳۸۲۶
---
# غزل شمارهٔ ۳۸۲۶

<div class="b" id="bn1"><div class="m1"><p>عرق نه از رخ آن گلعذار می ریزد</p></div>
<div class="m2"><p>ستاره از فلک فتنه بار می ریزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گره به رشته پرواز من گلی زده است</p></div>
<div class="m2"><p>که از نسیم توجه ز بار می ریزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنای زندگی خضر هم به آب رسید</p></div>
<div class="m2"><p>هنوز از لب تیغش خمار می ریزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حذر ز صحبت ناجنس حرز عافیت است</p></div>
<div class="m2"><p>که خون ز سر انگشت خار می ریزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درین زمانه که رسم گرفتگی عام است</p></div>
<div class="m2"><p>چگونه رنگ ز دست بهار می ریزد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو تاک سر زده، هرجا که حرف می گذرد</p></div>
<div class="m2"><p>سرشکم از مژه بی اختیار می ریزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لبی که تنگ شکر شد دهان ساغر ازو</p></div>
<div class="m2"><p>به چشم من نمک انتظار می ریزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا به زخم زبان خصم می دهد تهدید</p></div>
<div class="m2"><p>به چاک پیرهن شعله خار می ریزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صفیر خامه صائب بلند چون گردد</p></div>
<div class="m2"><p>ز آبگینه دلها غبار می ریزد</p></div></div>