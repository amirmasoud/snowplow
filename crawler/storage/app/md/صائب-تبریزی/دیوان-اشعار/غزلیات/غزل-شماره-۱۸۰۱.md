---
title: >-
    غزل شمارهٔ ۱۸۰۱
---
# غزل شمارهٔ ۱۸۰۱

<div class="b" id="bn1"><div class="m1"><p>اگر چه کعبه مقصد نصیب هر دل نیست</p></div>
<div class="m2"><p>ز پا فتادن این راه، کم ز منزل نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهار را به خزان پرده دار می گردند</p></div>
<div class="m2"><p>شکسته رنگی عشاق از ته دل نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به مغز بیش رسد فیض گل چو دسته شود</p></div>
<div class="m2"><p>ورگر نه زور جنون عاجز سلاسل نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مکش عنان به سخن از طلب که همچو قلم</p></div>
<div class="m2"><p>سخن به راه کند رهروی که کاهل نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گذشتن از لب میگون یار دشوارست</p></div>
<div class="m2"><p>وگرنه از می گلرنگ توبه مشکل نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس است حلقه ماتم ز حلقه فتراک</p></div>
<div class="m2"><p>مرا که نخل به جز دست و تیغ قاتل نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل تو لنگر تسلیم را ز کف داده است</p></div>
<div class="m2"><p>وگرنه موج خطر هیچ کم ز ساحل نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نکرد گریه ما در دل فلک تأثیر</p></div>
<div class="m2"><p>گناه تخم چه باشد، زمین چو قابل نیست؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به هر چه می کند آتش، سپند من راضی است</p></div>
<div class="m2"><p>مرا امید شفاعت ز اهل محفل نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز جام چشم غزالان خمار می شکنیم</p></div>
<div class="m2"><p>دل رمیده ما در کمین محمل نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه شد که بر فلک ناز می کند جولان؟</p></div>
<div class="m2"><p>ز حال پرتو خود آفتاب غافل نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حجاب نیست ز هم حسن و عشق را صائب</p></div>
<div class="m2"><p>میان ذره و خورشید چرخ حایل نیست</p></div></div>