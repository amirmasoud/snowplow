---
title: >-
    غزل شمارهٔ ۶۳۹۰
---
# غزل شمارهٔ ۶۳۹۰

<div class="b" id="bn1"><div class="m1"><p>یک دل نشد گشاده ز گفت و شنید من</p></div>
<div class="m2"><p>با هیچ قفل راست نیامد کلید من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سنگ از شرار و شرر می دهم خبر</p></div>
<div class="m2"><p>افلاک یک ستاره ندارد به دید من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با تیغ پاک کرده ام اینجا حساب خود</p></div>
<div class="m2"><p>از خاک، روی شسته برآید شهید من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مردان هزار فوج ز همت شکسته اند</p></div>
<div class="m2"><p>غافل مباش از سپه ناپدید من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ابر سیاه، پرده سیلاب فتنه است</p></div>
<div class="m2"><p>ایمن مشو ز آفت چشم سفید من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این آن غزل که گفت مسیحای زنده دل</p></div>
<div class="m2"><p>کاین خلق نیست در خور گفت و شنید من</p></div></div>