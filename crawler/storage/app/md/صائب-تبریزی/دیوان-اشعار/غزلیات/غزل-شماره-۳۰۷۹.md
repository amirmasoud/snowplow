---
title: >-
    غزل شمارهٔ ۳۰۷۹
---
# غزل شمارهٔ ۳۰۷۹

<div class="b" id="bn1"><div class="m1"><p>ز خط رویش چراغ دیده شب زنده داران شد</p></div>
<div class="m2"><p>غبار خط او خاک مراد خاکساران شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برآرد در دل شب آب حیوان دست جان بخشی</p></div>
<div class="m2"><p>لب میگون او در دور خط از میگساران شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برآمد از حجاب شرم در دوران خط رویش</p></div>
<div class="m2"><p>هلال خط مشکین ماه عید روزه داران شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنای طاقت من گرچه بود از بیستون افزون</p></div>
<div class="m2"><p>به بازی بازی آخر پایمال نی سواران شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشد از گریه مستانه چشمم خشک چون مینا</p></div>
<div class="m2"><p>غبار هستی من خرج سیل نوبهاران شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شدم چون سرو تا سرسبز از تشریف آزادی</p></div>
<div class="m2"><p>دم سرد خزان بر من نسیم نوبهاران شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من آن مجنون بیباکم از بیتابی شوقم</p></div>
<div class="m2"><p>ره خوابیده چون موج سراب از بیقراران شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همان چشم حسودان بر ندارد سر زدنبالم</p></div>
<div class="m2"><p>اگرچه شیشه من توتیا از سنگباران شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بلایی آدمی را بدتر از شهرت نمی باشد</p></div>
<div class="m2"><p>سیه شد روی هر کس چون عقیق از نامداران شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به خلق آن کس که رو آورد می باشد زخود غافل</p></div>
<div class="m2"><p>نبیند عیب خود هر کس که از آیینه داران شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نمی سازد مرا چون کبک خامش سختی دوران</p></div>
<div class="m2"><p>که شق چون خامه منقارم زتیغ کوهساران شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز بیدردی کنون صائب خمش چون مرغ تصویرم</p></div>
<div class="m2"><p>اگرچه ناله من باعث شور هزاران شد</p></div></div>