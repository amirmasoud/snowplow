---
title: >-
    غزل شمارهٔ ۳۰۹۷
---
# غزل شمارهٔ ۳۰۹۷

<div class="b" id="bn1"><div class="m1"><p>دمی چون صبح می خواهم درین عالم ز من باشد</p></div>
<div class="m2"><p>که روشن می کنم آفاق را چون دم ز من باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به چشم سیر من اسباب دنیا در نمی آید</p></div>
<div class="m2"><p>همین وقت خوشی می خواهم از عالم ز من باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو عیسی هر که صاحب دم شد از کشتن نیندیشد</p></div>
<div class="m2"><p>نمی اندیشم از تیغ دودم گر دم ز من باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ازین دامن، وزان سر می کشم از بی نیازیها</p></div>
<div class="m2"><p>اگر تاج فریدون و سریر جم ز من باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندارد حاصلی جز دردسر ملک سلیمانی</p></div>
<div class="m2"><p>نمی دارم دریغ از دیو اگر خاتم ز من باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز اشک و آه دارم تازه داغ دردمندان را</p></div>
<div class="m2"><p>من آن شمعم که سوز حلقه ماتم ز من باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل خوش مشرب من داغ دارد اهل عالم را</p></div>
<div class="m2"><p>همان از بیغمانم گر غم عالم ز من باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه سروم کز رعونت تازه دارم روی خود تنها</p></div>
<div class="m2"><p>چو ابر نوبهاران عالمی خرم زمن باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به یک نظاره زان رخسار گندم گون کنم سودا</p></div>
<div class="m2"><p>اگر در بسته باغ خلد چون آدم ز من باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو سوزن از گرانی دامن خود بر زمین دوزم</p></div>
<div class="m2"><p>اگر همچون مسیحا رشته مریم ز من باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا بگذار چون خار سر دیوار با خشکی</p></div>
<div class="m2"><p>که طوفان می کنم گر قطره ای شبنم ز من باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مدار آیینه پیش لب مرا زنهار ای همدم</p></div>
<div class="m2"><p>چرا در وقت رفتن خاطری در هم ز من باشد؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به قدر نقش باشد دیده بد در کمین صائب</p></div>
<div class="m2"><p>ز چشم آسوده ام چندان که نقش کم ز من باشد</p></div></div>