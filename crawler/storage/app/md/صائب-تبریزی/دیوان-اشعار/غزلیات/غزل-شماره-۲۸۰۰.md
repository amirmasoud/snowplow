---
title: >-
    غزل شمارهٔ ۲۸۰۰
---
# غزل شمارهٔ ۲۸۰۰

<div class="b" id="bn1"><div class="m1"><p>مبادا کافر از طاق دل پیر مغان افتد!</p></div>
<div class="m2"><p>که رزق خاک گردد تیر چون دور از کمان افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جدا از حلقه آن زلف حال دل چه می پرسی؟</p></div>
<div class="m2"><p>چه باشد حال مرغ بی پری کز آشیان افتد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا از تندخویی یار ترساند، ازین غافل</p></div>
<div class="m2"><p>که از آتش سمندر در بهشت جاودان افتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز شرم او نگاهم دست و پا گم کرد چون طفلی</p></div>
<div class="m2"><p>که چشمش وقت گل چیدن به چشم باغبان افتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رسانم گر به دولت چون هما از سایه عالم را</p></div>
<div class="m2"><p>همان از خوان قسمت قرعه ام بر استخوان افتد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز دست هم ربایندش سرافرازان بستانی</p></div>
<div class="m2"><p>درین بستانسرا چون تاک هر کس خوش عنان افتد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرایت می کند آه ضعیفان در قوی حالان</p></div>
<div class="m2"><p>نبخشاید به شیران برق چون در نیستان افتد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ببر از تنگ چشمان گر سر آزاده می خواهی</p></div>
<div class="m2"><p>که با سوزن چو پیوندد، گره در ریسمان افتد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مکن با خاکساران سرکشی ای شاخ گل چندین</p></div>
<div class="m2"><p>که شمع از پرسش پروانه هر شب از زبان افتد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز رسوایی نیندیشد دل سرگرم من صائب</p></div>
<div class="m2"><p>اگر چون مهر طشت من زبام آسمان افتد</p></div></div>