---
title: >-
    غزل شمارهٔ ۵۶۴۸
---
# غزل شمارهٔ ۵۶۴۸

<div class="b" id="bn1"><div class="m1"><p>چه شکایت ز تو ای خانه برانداز کنم</p></div>
<div class="m2"><p>هر چند انجام ندارد ز چه آغاز کنم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در نهانخانه غیب است کلید دل من</p></div>
<div class="m2"><p>این نه چشم است که برهم نهم و باز کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دام مرغان گرفتار بود ناله من</p></div>
<div class="m2"><p>آه از آن روز که دام تو پرواز کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>التفات تو مرا بر سر ناز آورده است</p></div>
<div class="m2"><p>گر کنم ناز به عالم، به تو چون ناز کنم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خضر در بادیه شوق ز همراهی من</p></div>
<div class="m2"><p>آنقدر دور نمانده است که آواز کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پرده طاقش از شیشه تنکتر گردد</p></div>
<div class="m2"><p>سنگ را گر صدف گوهر این راز کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صورت حال من آن روز شود بر تو عیان</p></div>
<div class="m2"><p>که دل سنگ ترا آینه پرداز کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می کند چرخ ستمگر به شکر خنده حساب</p></div>
<div class="m2"><p>لب مخمور به خمیازه اگر باز کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب از عشق جوانمرد گدایی دارم</p></div>
<div class="m2"><p>آنقدر صبر که خون در جگر ناز کنم</p></div></div>