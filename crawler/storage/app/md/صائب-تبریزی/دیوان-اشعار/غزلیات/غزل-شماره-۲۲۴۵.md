---
title: >-
    غزل شمارهٔ ۲۲۴۵
---
# غزل شمارهٔ ۲۲۴۵

<div class="b" id="bn1"><div class="m1"><p>از نظر هرگز خیالش دور نیست</p></div>
<div class="m2"><p>یک نفس دریای ما بی شور نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دهان اژدهای خم رود</p></div>
<div class="m2"><p>مست بی پرواتر از مخمور نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خنده بر برق تجلی می زند</p></div>
<div class="m2"><p>خانه دل چون بنای طور نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست در فرمان بدگویان زبان</p></div>
<div class="m2"><p>اختیار نیش با زنبور نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر ندارد سکته چین بر جبین</p></div>
<div class="m2"><p>بیت ابروی تو چون مشهور نیست؟</p></div></div>