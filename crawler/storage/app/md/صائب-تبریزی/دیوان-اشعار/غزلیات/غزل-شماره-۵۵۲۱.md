---
title: >-
    غزل شمارهٔ ۵۵۲۱
---
# غزل شمارهٔ ۵۵۲۱

<div class="b" id="bn1"><div class="m1"><p>چنان سرگرمیی از شوق آن گلگون قبا دارم</p></div>
<div class="m2"><p>که بر گل می خرامم خاراگر در زیر پا دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کنار شوق من چون موج آسایش نمی داند</p></div>
<div class="m2"><p>به دریا می روم دست و بغل تا دست و پا دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر چه در ته یک پیرهن با ماه کنعانم</p></div>
<div class="m2"><p>به بوی پیرهن سر در پی باد صبا دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گره در کاکلش نگذاشت مژگان بلند او</p></div>
<div class="m2"><p>چه خونها در جگر زان ناوک کاکل ربا دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز خار خشک من ای شاخ گل دامن کشان مگذر</p></div>
<div class="m2"><p>که رنگ مردم بیگانه بوی آشنا دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا ای عشق اگر داری دماغ جلوه پردازی</p></div>
<div class="m2"><p>که از داغ جنون آیینه های خوش جلا دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به یک عالم توجه از تو چون قانع توانم شد</p></div>
<div class="m2"><p>که من از جمله عالم ترا دارم، ترا دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو بوی گل نمی گردد به دامن آشنا پایم</p></div>
<div class="m2"><p>به ظاهر گر چه دست و پای کوشش در حنا دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زلال زندگی در ساغر من رنگ گرداند</p></div>
<div class="m2"><p>همان خون می خورم گر در قدح آب بقا دارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنان در پاکبازی از علایق گشته ام عریان</p></div>
<div class="m2"><p>که حال مهره ششدر ز نقش بوریا دارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جنونم اختیاری نیست تا گردم عنانگیرش</p></div>
<div class="m2"><p>چو برگ کاه پروازی به بال کهربا دارم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر چه دوربینان چشم دریا می شمارندم</p></div>
<div class="m2"><p>حباب آسا به کف جای گهرمشتی هوا دارم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرا نتوان به تیغ از درد بی درمان جدا کردن</p></div>
<div class="m2"><p>که از هر بند خود با درد پیوندی جدا دارم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هوای عالم آزادگی کم مختلف گردد</p></div>
<div class="m2"><p>از آن چون سرو من در چار موسم یک قبا دارم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زاکسیر قناعت خون آهو مشک می گردد</p></div>
<div class="m2"><p>من این تعلیم صائب از غزالان ختا دارم</p></div></div>