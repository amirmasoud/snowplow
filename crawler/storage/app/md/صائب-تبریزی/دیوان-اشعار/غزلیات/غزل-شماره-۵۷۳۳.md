---
title: >-
    غزل شمارهٔ ۵۷۳۳
---
# غزل شمارهٔ ۵۷۳۳

<div class="b" id="bn1"><div class="m1"><p>مرا که گفت که دست از عنان یار کشم</p></div>
<div class="m2"><p>کشد رقیب رکاب و من انتظار کشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا که هست میسر سبوکشی در دیر</p></div>
<div class="m2"><p>چه لازم است که درد سر خمار کشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا که صبحت داغی همیشه داشته‌ام</p></div>
<div class="m2"><p>چه اوفتاده که دامن ز لاله‌زار کشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا که دست و دل از روزگار سرد شده است</p></div>
<div class="m2"><p>چسان به رشته گهرهای آبدار کشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا که زندگی از آتش است همچون شمع</p></div>
<div class="m2"><p>چرا ز شعله برون رخت چون شرار کشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز شوربختی من هر حباب گردابی است</p></div>
<div class="m2"><p>چگونه کشتی ازین ورطه بر کنار کشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر نه خاطر روی تو در میان باشد</p></div>
<div class="m2"><p>به روی آینه دل خط غبار کشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو خار خشک سزاوار سوختن شده‌ام</p></div>
<div class="m2"><p>عبث چه منت مشاطه بهار کشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به مصر رفتم و از مشتری ندیدم روی</p></div>
<div class="m2"><p>متاع آینه خود به زنگبار کشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به یک نسیم توجه ز خاک بردارم</p></div>
<div class="m2"><p>ز ضعف پا به زمین چند چون غبار کشم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ندیده هجر دل ناز پرورم صائب</p></div>
<div class="m2"><p>عجب نباشد اگر ناله‌های زار کشم</p></div></div>