---
title: >-
    غزل شمارهٔ ۲۱۴۹
---
# غزل شمارهٔ ۲۱۴۹

<div class="b" id="bn1"><div class="m1"><p>چشمی که نظرباز به آن طاق دو ابروست</p></div>
<div class="m2"><p>دایم دو دل از عشق چو شاهین ترازوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی نرگس گویا، به سخن لب نگشاییم</p></div>
<div class="m2"><p>ما را طرف حرف همین چشم سخنگوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس خون که کند در دل مرغان چمن زاد</p></div>
<div class="m2"><p>این حسن خداداد که با آن گل خودروست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در پرده بینایی من نقش دویی نیست</p></div>
<div class="m2"><p>هر داغ پلنگم به نظر دیده آهوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا غنچه نگردیم دل ما نگشاید</p></div>
<div class="m2"><p>در خلوت ما رطل گران کاسه زانوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در روز به مجلس مطلب دختر رز را</p></div>
<div class="m2"><p>صحبت به شب انداز، که صحبت گل شب بوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب چه خیال است که از سینه کند یاد؟</p></div>
<div class="m2"><p>هر دل که گرفتار در آن حلقه گیسوست</p></div></div>