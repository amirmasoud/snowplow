---
title: >-
    غزل شمارهٔ ۱۲۷
---
# غزل شمارهٔ ۱۲۷

<div class="b" id="bn1"><div class="m1"><p>تر زبانی معدن زنگار می‌سازد مرا</p></div>
<div class="m2"><p>خامشی آیینه اسرار می‌سازد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفتاب غیب، فرش خانه بی‌روزن است</p></div>
<div class="m2"><p>چشم بستن مطلع انوار می‌سازد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در میان مستی و هشیاری من پرده‌ای است</p></div>
<div class="m2"><p>نعره مستانه‌ای هشیار می‌سازد مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سایه سروی که من در پای او آسوده‌ام</p></div>
<div class="m2"><p>از شکر خواب عدم بیدار می‌سازد مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌تواند چشم بیماری مسیح من شدن</p></div>
<div class="m2"><p>فتنه خوابیده‌ای بیدار می‌سازد مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کف چه حد دارد نقاب شورش دریا شود؟</p></div>
<div class="m2"><p>مستی سرشار، بی‌دستار می‌سازد مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آفتاب گرم‌رویی دشمن جان من است</p></div>
<div class="m2"><p>نخل مومم، سردی بازار می‌سازد مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تنگ می‌سازد بیابان را به رهرو کفش تنگ</p></div>
<div class="m2"><p>تنگدستی از جهان بیزار می‌سازد مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عز آزادی به ذل بندگی نتوان فروخت</p></div>
<div class="m2"><p>بخل بیش از جود منت دار می‌سازد مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هیچ سوهان راهرو را چون ره باریک نیست</p></div>
<div class="m2"><p>فکر آن موی میان هموار می‌سازد مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرچه چون سیل از غبار ره گران گردیده‌ام</p></div>
<div class="m2"><p>جذبه دریا سبک‌رفتار می‌سازد مرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این جواب آن غزل صائب، که می‌گوید اسیر</p></div>
<div class="m2"><p>خواب چون گردد گران، بیدار می‌سازد مرا</p></div></div>