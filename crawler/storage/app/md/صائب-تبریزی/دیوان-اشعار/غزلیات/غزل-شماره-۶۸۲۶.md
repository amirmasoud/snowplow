---
title: >-
    غزل شمارهٔ ۶۸۲۶
---
# غزل شمارهٔ ۶۸۲۶

<div class="b" id="bn1"><div class="m1"><p>سوختی در عرق شرم و حیا ای ساقی</p></div>
<div class="m2"><p>دو سه جامی بکش از شرم برآ ای ساقی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از می و نقل به یک بوسه قناعت کردیم</p></div>
<div class="m2"><p>رحم کن بر جگر تشنه ما ای ساقی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند چون شمع ز فانوس حصاری باشی؟</p></div>
<div class="m2"><p>بی تکلف بگشا بند قبا ای ساقی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پنبه را وقت سحر از سر مینا بردار</p></div>
<div class="m2"><p>تا برآید می خورشیدلقا ای ساقی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بوسه دادی به لب جام و به دستم دادی</p></div>
<div class="m2"><p>عمر باد و مزه عمر ترا ای ساقی!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شده ام برگ خزان دیده ای از رنج خمار</p></div>
<div class="m2"><p>در قدح ریز می لعل قبا ای ساقی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دهنم از لب شیرین تو شد تنگ شکر</p></div>
<div class="m2"><p>چون بگویم به دو لب شکر ترا ای ساقی؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شعله بی روغن اگر زنده تواند بودن</p></div>
<div class="m2"><p>طبع بی می نکند نشو و نما ای ساقی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>(زحمت رنگ حنا بر ید بیضا مپسند</p></div>
<div class="m2"><p>می کند پرتو می کار حنا ای ساقی)</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>(خضر اگر بوی ز کیفیت ساغر ببرد</p></div>
<div class="m2"><p>آب حیوان بدهد روی نما ای ساقی)</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>(قطره ای گر بچکد از می خونگرم به خاک</p></div>
<div class="m2"><p>روید از شوره زمین مهرگیا ای ساقی)</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صائب تشنه جگر را که کمین بنده توست</p></div>
<div class="m2"><p>از نظر چند برانی به جفا ای ساقی؟</p></div></div>