---
title: >-
    غزل شمارهٔ ۴۱۰۳
---
# غزل شمارهٔ ۴۱۰۳

<div class="b" id="bn1"><div class="m1"><p>از شب نشین هند دل من سیاه شد</p></div>
<div class="m2"><p>عمرم چو شمع در قدم اشک وآه شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پنداشتم ز هند شود بخت تیره سبز</p></div>
<div class="m2"><p>این خاک هم علاوه بخت سیاه شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبح وطن کجاست که در شام انتظار</p></div>
<div class="m2"><p>چون شمع افسر وکمرم اشک وآه شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگذر زحسن گندمی ومگذر از بهشت</p></div>
<div class="m2"><p>زین برق فتنه خرمن آدم تباه شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باشد همیشه در صف عشاق سربلند</p></div>
<div class="m2"><p>آن را که آه ابلق طرف کلاه شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می جستم از زمین خبر صدق لب به لب</p></div>
<div class="m2"><p>از غیب اشاره ام به دم صبحگاه شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محراب سر به سجده افتادگی نهاد</p></div>
<div class="m2"><p>روزی که طاق ابروی او قبله گاه شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سنگ ملامت از کف طفلان گرفت اوج</p></div>
<div class="m2"><p>داغ جنون به فرق مرا تا کلاه شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از بس چراغ دیده به راه تو سوختیم</p></div>
<div class="m2"><p>از پیه دیده شعله نور نگاه شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غافل نظر به چهره زرد منش فتاد</p></div>
<div class="m2"><p>زان روز باز رنگ ز رخسار کاه شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب چه اعتبار براخوان روزگار</p></div>
<div class="m2"><p>یوسف به ریسمان برادر به چاه شد</p></div></div>