---
title: >-
    غزل شمارهٔ ۶۴۳۶
---
# غزل شمارهٔ ۶۴۳۶

<div class="b" id="bn1"><div class="m1"><p>چون غنچه هر که ننشست در خار تا به گردن</p></div>
<div class="m2"><p>از می نشد چو مینا سرشار تا به گردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون شمع هر که افراخت گردن به افسر زر</p></div>
<div class="m2"><p>در اشک خود نشیند بسیار تا به گردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بتوان ز روزن دل دیدن جهان جان را</p></div>
<div class="m2"><p>زین رخنه سربرآور یک بار تا به گردن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون خم همان دهانش خمیازه ریز باشد</p></div>
<div class="m2"><p>در می اگر نشیند خمار تا به گردن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک طوق پیرهن دان پرگار آسمان را</p></div>
<div class="m2"><p>تا در وصال باشی با یار تا به گردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک بار غنچه او بر روی باغ خندید</p></div>
<div class="m2"><p>در موج گل نهان شد دیوار تا به گردن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صبح بیاض گردن صاحب شفق نگردید</p></div>
<div class="m2"><p>نگرفت خون ما را دلدار تا به گردن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زنهار با بزرگان گستاخ درنیایی</p></div>
<div class="m2"><p>تیغ جگر شکافی است کهسار تا به گردن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جمعی که سرندادند در راه عشقبازی</p></div>
<div class="m2"><p>مستغرقند صائب در عار تا به گردن</p></div></div>