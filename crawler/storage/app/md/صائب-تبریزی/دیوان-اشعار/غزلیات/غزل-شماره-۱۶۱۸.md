---
title: >-
    غزل شمارهٔ ۱۶۱۸
---
# غزل شمارهٔ ۱۶۱۸

<div class="b" id="bn1"><div class="m1"><p>جوش می خشتی اگر از خم صهبا برداشت</p></div>
<div class="m2"><p>سقف این میکده را جوش من از جا برداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست اگر در کمر کوه کند می گسلد</p></div>
<div class="m2"><p>زور شوقی که مرا سلسله از پا برداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شوری از ناله مجنون به بیابان افتاد</p></div>
<div class="m2"><p>که دل از سینه لیلی ره صحرا برداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من نه آنم که تراوش کند از من سخنی</p></div>
<div class="m2"><p>پرده از راز من آن آینه سیما برداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پای من بر سر گنج است به هر جا که روم</p></div>
<div class="m2"><p>تا که از خاک مرا آبله پا برداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه ز اندیشه تجرید به خود می لرزی؟</p></div>
<div class="m2"><p>سوزنی بود درین راه، مسیحا برداشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شرم اندیشه گداز تو که روزافزون باد</p></div>
<div class="m2"><p>از دل اهل هوس یاد تمنا برداشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طاقت دیدن همچشم که دارد صائب؟</p></div>
<div class="m2"><p>دید از دور مرا بلبل و غوغا برداشت</p></div></div>