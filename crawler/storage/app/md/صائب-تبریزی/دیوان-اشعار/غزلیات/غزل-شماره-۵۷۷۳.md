---
title: >-
    غزل شمارهٔ ۵۷۷۳
---
# غزل شمارهٔ ۵۷۷۳

<div class="b" id="bn1"><div class="m1"><p>به جای باده اگر در پیاله آب کنیم</p></div>
<div class="m2"><p>ز تنگ حوصلگی مستی شراب کنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو نخل موم بر و بار ما ملایمت است</p></div>
<div class="m2"><p>چگونه سینه سپر پیش آفتاب کنیم؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو موج بر صف دریا زنیم و خوش باشیم</p></div>
<div class="m2"><p>به خویش کار چرا تنگ چون حباب کنیم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر نه خاطر روی تو در میان باشد</p></div>
<div class="m2"><p>ز آه چشمه آیینه را سراب کنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیاض گردن او گر به دست ما افتد</p></div>
<div class="m2"><p>چه بوسه های گلوسوز انتخاب کنیم!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کدام عیش به این عیش می رسد صائب؟</p></div>
<div class="m2"><p>که ما و دختر رز سیر ماهتاب کنیم</p></div></div>