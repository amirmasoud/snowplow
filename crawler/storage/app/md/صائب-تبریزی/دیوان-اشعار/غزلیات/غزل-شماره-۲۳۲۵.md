---
title: >-
    غزل شمارهٔ ۲۳۲۵
---
# غزل شمارهٔ ۲۳۲۵

<div class="b" id="bn1"><div class="m1"><p>از حریم ما سخن چین چون سخن بیرون برد؟</p></div>
<div class="m2"><p>باد نتوانست نکهت زین چمن بیرون برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمع را خاکستر پروانه اینجا سرمه داد</p></div>
<div class="m2"><p>کیست راز عشق را از انجمن بیرون برد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در به روی طوطیان آیینه از زنگار بست</p></div>
<div class="m2"><p>این سزای آن که از خلوت سخن بیرون برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرتو لعل لب او گر نیفروزد چراغ</p></div>
<div class="m2"><p>راه نتواند تبسم زان دهن بیرون برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دولت تردامنان پا در رکاب نیستی است</p></div>
<div class="m2"><p>زود شبنم رخت هستی از چمن بیرون برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شمع تر کرد از عرق پیراهن فانوس را</p></div>
<div class="m2"><p>کیست تا پروانه را از انجمن بیرون برد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرمه خط خامشی گرد لب ساغر کشید</p></div>
<div class="m2"><p>تا مباد از مجلس مستان سخن بیرون برد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که می خواهد شود فکر جهانگردش غریب</p></div>
<div class="m2"><p>به که چون صائب گرانی از وطن بیرون برد</p></div></div>