---
title: >-
    غزل شمارهٔ ۱۷۶۰
---
# غزل شمارهٔ ۱۷۶۰

<div class="b" id="bn1"><div class="m1"><p>به دوست نامه نوشتن، شعار بیگانه است</p></div>
<div class="m2"><p>به شمع، نامه پروانه بال پروانه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی است بستن احرام و بستن زنار</p></div>
<div class="m2"><p>ترا که روی دل از کعبه سوی بتخانه است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر ز عشق دلت چاک شد، مشو در هم</p></div>
<div class="m2"><p>که دل چو چاک شود زلف یار را شانه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به جوی شیر چو فرهاد تیشه فرسودن</p></div>
<div class="m2"><p>یکی ز جمله بازیچه های طفلانه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز تن ملال ندارد روان دون همت</p></div>
<div class="m2"><p>که مرغ ریخته پر را قفس پریخانه است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حذر ز سایه خود می کنند شیشه دلان</p></div>
<div class="m2"><p>ز عقل سنگ ملامت حصار دیوانه است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر ز اهل دلی، فیض آسمان از توست</p></div>
<div class="m2"><p>که شیشه هر چند کند جمع، بهر پیمانه است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنین که دیدن صیاد رزق من شده است</p></div>
<div class="m2"><p>به خاطر آنچه نگردد، تصور دانه است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به فکر دل نفتادیم صائب از غفلت</p></div>
<div class="m2"><p>نیافتیم که لیلی درین سیه خانه است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به خانه ای که توان رفت بی طلب صائب</p></div>
<div class="m2"><p>درین زمانه پر دار و گیر، میخانه است</p></div></div>