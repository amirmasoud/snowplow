---
title: >-
    غزل شمارهٔ ۶۰۳۰
---
# غزل شمارهٔ ۶۰۳۰

<div class="b" id="bn1"><div class="m1"><p>دست رد مشکل بود بر توشه عقبی زدن</p></div>
<div class="m2"><p>ورنه آسان است پشت پای بر دنیا زدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سبکروحی اگر بر دل گذاری بار خلق</p></div>
<div class="m2"><p>می توانی همچو کشتی سینه بر دریا زدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می کشد سنگ انتقام خویش از آهن دلان</p></div>
<div class="m2"><p>شد سر فرهاد شق از تیشه بر خارا زدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نصیحت منع کردن نیکخواهان را خطاست</p></div>
<div class="m2"><p>از ادب دورست سوزن بر لب عیسی زدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر کنی در راستی استادگی، بی چشم زخم</p></div>
<div class="m2"><p>می توان بر قلب لشکر چون علم تنها زدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در گلستانی که می باید سراپا چشم شد</p></div>
<div class="m2"><p>بخیه می باید مرا بر دیده بینا زدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر به دل خوردن شوی قانع درین مهمانسرا</p></div>
<div class="m2"><p>می توان صائب به چرخ سفله استغنا زدن</p></div></div>