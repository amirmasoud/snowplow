---
title: >-
    غزل شمارهٔ ۳۹۰۸
---
# غزل شمارهٔ ۳۹۰۸

<div class="b" id="bn1"><div class="m1"><p>اگر ز چهره داغم نقاب بردارند</p></div>
<div class="m2"><p>جهانیان نظر از آفتاب بردارند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان مکن که به حال خودت گذارد عشق</p></div>
<div class="m2"><p>نه دوستی است که دست از کباب بردارند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز چشم شور تماشاییان مشو غافل</p></div>
<div class="m2"><p>که رنگ نشأه ز روی شراب بردارند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز شرم وصل شدم آب دوستان چه شدند</p></div>
<div class="m2"><p>که نخل موم من از آفتاب بردارند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر به مجلس روحانیان رسی صائب</p></div>
<div class="m2"><p>بگو که قسمت ما را شراب بردارند</p></div></div>