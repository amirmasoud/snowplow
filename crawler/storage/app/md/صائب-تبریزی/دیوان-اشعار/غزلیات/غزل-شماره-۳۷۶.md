---
title: >-
    غزل شمارهٔ ۳۷۶
---
# غزل شمارهٔ ۳۷۶

<div class="b" id="bn1"><div class="m1"><p>به عزم صید چین سازد چو زلف صیدبندش را</p></div>
<div class="m2"><p>رم آهو به استقبال می آید کمندش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که دارد شهسواری این چنین یاد از پری رویان؟</p></div>
<div class="m2"><p>که از شادی نمی باشد نشان پاسمندش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شود هر حلقه ای انگشتر پای نگارینش</p></div>
<div class="m2"><p>نبندد بر کمر آن شوخ اگر زلف بلندش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز شیرینی به هم چسبد لب خمیازه پردازش</p></div>
<div class="m2"><p>به خاطر بگذراند هر که لعل نوشخندش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمی پیچید سر چون قمریان از طوق فرمانش</p></div>
<div class="m2"><p>اگر صید حرم می دید زلف صید بندش را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حیات جاودانی از خدا چون خضر می خواهم</p></div>
<div class="m2"><p>که آرم در نظر با کام دل، قد بلندش را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بی باکی به درد عاشق بی دل نپردازد</p></div>
<div class="m2"><p>مگر خط مهربان سازد دل نادردمندش را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به دیدن صید دلها می کند زلف رسای او</p></div>
<div class="m2"><p>ز گیرایی نباشد احتیاج چین کمندش را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که دارد صائب از خوبان چنین حسن گلوسوزی؟</p></div>
<div class="m2"><p>که بلبل می کند از خرده گلها سپندش را</p></div></div>