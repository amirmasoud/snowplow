---
title: >-
    غزل شمارهٔ ۶۵۷
---
# غزل شمارهٔ ۶۵۷

<div class="b" id="bn1"><div class="m1"><p>ز خون شکفته شود چون شراب شیشه ما</p></div>
<div class="m2"><p>شکسته دل نشود ز انقلاب شیشه ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر شکنجه کنندش به آب تلخ، کند</p></div>
<div class="m2"><p>ز کیمیای قناعت گلاب شیشه ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز خشکسال نمی گردد آب گوهر کم</p></div>
<div class="m2"><p>شود چو آبله پر از سراب شیشه ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شراب بی جگران را دلیر می سازد</p></div>
<div class="m2"><p>چرا ز سنگ کند اجتناب شیشه ما؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز سنگ اگر به دل نازکش رسد آسیب</p></div>
<div class="m2"><p>به روی خویش نیارد چو آب شیشه ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لب شکایت ما را که می تواند بست؟</p></div>
<div class="m2"><p>شکسته است ز زور شراب شیشه ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>توان به باطن ما راه بردن از ظاهر</p></div>
<div class="m2"><p>به روی باده نگردد حجاب شیشه ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به میکشان کمرو تاج لعل می بخشد</p></div>
<div class="m2"><p>به هر پیاله ز موج و حباب شیشه ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سپاه عقل گرانسنگ را به هم شکند</p></div>
<div class="m2"><p>نهد ز جام چو پا در رکاب شیشه ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شکستن دل ما را به سنگ حاجت نیست</p></div>
<div class="m2"><p>که از نفس شکند چون حباب شیشه ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز سرکشی به سلیمان فرو نیارد سر</p></div>
<div class="m2"><p>پر از پری چو شود از شراب شیشه ما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کند ز خنده دل آفتاب خون، تا شد</p></div>
<div class="m2"><p>ز باده شفقی کامیاب شیشه ما</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بنای میکده ها را رسانده است به آب</p></div>
<div class="m2"><p>ز بوی باده نگردد خراب شیشه ما</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مدار دست ز ریزش که شد ز راه کرم</p></div>
<div class="m2"><p>به کوی میکده مالک رقاب شیشه ما</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شود چو سرو، علم در چمن به سرسبزی</p></div>
<div class="m2"><p>به تخم سوخته بخشد گر آب شیشه ما</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز سنگ حادثه هر چند توتیا گردید</p></div>
<div class="m2"><p>نشد که چشم بمالد ز خواب شیشه ما</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به خم نمی کند از احتیاج، گردن کج</p></div>
<div class="m2"><p>مگر ز خویش برآرد ز شراب شیشه ما</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به ظرف کم چه تمتع توان ز دریا یافت؟</p></div>
<div class="m2"><p>مگر شکسته شود چون حباب شیشه ما</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همان زمان به لب تشنه ای پیاله رساند</p></div>
<div class="m2"><p>گرفت هر چه ز خم چون سحاب شیشه ما</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز وصل سیمبران پیرهن حجاب بود</p></div>
<div class="m2"><p>مگر ز گرمی می، گردد آب شیشه ما</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز فیض خوش نفسی، خون گرم صهبا را</p></div>
<div class="m2"><p>به ناف جام کند مشک ناب شیشه ما</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حدیث حوصله بر طاق نه که دریا را</p></div>
<div class="m2"><p>به نیم جرعه نماید خراب شیشه ما</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اگر چه در سر می کرد عمر خود صائب</p></div>
<div class="m2"><p>نشد ز نشأه می کامیاب شیشه ما</p></div></div>