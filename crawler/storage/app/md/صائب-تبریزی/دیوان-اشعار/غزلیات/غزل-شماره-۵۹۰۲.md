---
title: >-
    غزل شمارهٔ ۵۹۰۲
---
# غزل شمارهٔ ۵۹۰۲

<div class="b" id="bn1"><div class="m1"><p>آتش به دل از گرمی این مرحله دارم</p></div>
<div class="m2"><p>پا بر سر گنج گهر از آبله دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آتش به زر اینجا نفروشند و من خام</p></div>
<div class="m2"><p>گرمی طمع از مردم این قافله دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن راهنوردم که تهی پایی خود را</p></div>
<div class="m2"><p>پیوسته نهان از نظر آبله دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سلسله زلف کسی طرف نبسته است</p></div>
<div class="m2"><p>عمری است که من ربط به این سلسله دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مینای فلک ظرف می عشق ندارد</p></div>
<div class="m2"><p>کی طاقت این می من بی حوصله دارم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گویند به هم مردم عالم گله خویش</p></div>
<div class="m2"><p>پیش که روم من که زعالم گله دارم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب به جز از سینه خود چاک زدن نیست</p></div>
<div class="m2"><p>شغلی که درین عالم پر مشغله دارم</p></div></div>