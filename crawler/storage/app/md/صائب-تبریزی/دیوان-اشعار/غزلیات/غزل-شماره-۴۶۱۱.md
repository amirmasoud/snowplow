---
title: >-
    غزل شمارهٔ ۴۶۱۱
---
# غزل شمارهٔ ۴۶۱۱

<div class="b" id="bn1"><div class="m1"><p>دل بود مایل به خط عنبرافشان بیشتر</p></div>
<div class="m2"><p>هست در ابر سیاه امید باران بیشتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خط برون می آورد شیرین لبان را ازحجاب</p></div>
<div class="m2"><p>می شود در پرده شب غنچه خندان بیشتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خار خار دلبری از خط فزون شد حسن را</p></div>
<div class="m2"><p>حرص گل در جمع زر گردد زدامان بیشتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناز خوبان می شود در روزگار خط زیاد</p></div>
<div class="m2"><p>خواب می گردد گران ازبوی ریحان بیشتر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می برد دل بیش ازان لبهای شیرین خط سبز</p></div>
<div class="m2"><p>رتبه این ظلمت است ازآب حیوان بیشتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرچه امید ظفر با لشکر برگشته نیست</p></div>
<div class="m2"><p>می کند صید دل آن برگشته مژگان بیشتر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روزی بی دست وپایان می رسداز خوان غیب</p></div>
<div class="m2"><p>قسمت دیوانه گردد سنگ طفلان بیشتر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پرده پوشی می کند بی پرده راز عشق را</p></div>
<div class="m2"><p>می کشد این شمع قد در زیر دامان بیشتر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در بساط بی سرو پایان مجو صبر وقرار</p></div>
<div class="m2"><p>تشنه سیرست گوهرهای غلطان بیشتر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سیل بی زنهار در معموره طوفان می کند</p></div>
<div class="m2"><p>شور مجنون است درشهر ازبیابان بیشتر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر چه گردد کم صدا چون کاسه چینی شکست</p></div>
<div class="m2"><p>درشکستن می کند دل صائب افغان بیشتر</p></div></div>