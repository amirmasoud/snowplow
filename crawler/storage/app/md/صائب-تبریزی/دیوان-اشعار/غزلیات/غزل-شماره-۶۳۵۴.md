---
title: >-
    غزل شمارهٔ ۶۳۵۴
---
# غزل شمارهٔ ۶۳۵۴

<div class="b" id="bn1"><div class="m1"><p>ز تن شکفته رود جان صادقان بیرون</p></div>
<div class="m2"><p>که تیر راست جهد صاف از کمان بیرون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حضور خانه خود مغتنم شمار که تیر</p></div>
<div class="m2"><p>به زور می رود از خانه کمان بیرون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی که چشم گشایش ز بستگی دارد</p></div>
<div class="m2"><p>قدم چو در نگذارد ز آستان بیرون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به التماسم اگر خضر بخشد آب حیات</p></div>
<div class="m2"><p>عقیق صبر نمی آرم از دهان بیرون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترحم است بر آن غنچه گرفته جبین</p></div>
<div class="m2"><p>که ناشکفته برندش ز گلستان بیرون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی است عاشق یکرنگ گلستان صائب</p></div>
<div class="m2"><p>که از چمن نرود موسم خزان بیرون</p></div></div>