---
title: >-
    غزل شمارهٔ ۴۴۱۱
---
# غزل شمارهٔ ۴۴۱۱

<div class="b" id="bn1"><div class="m1"><p>از دست رود خامه چو نام تو نویسند</p></div>
<div class="m2"><p>پرواز کند دل چو پیام تو نویسند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حرزی که بردزنگ ز آیینه دلها</p></div>
<div class="m2"><p>از روی خط غالیه فام تو نویسند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در حوصله دفتر افلاک نگنجد</p></div>
<div class="m2"><p>گرشمه ای از ماه تمام تو نویسند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون شهپر جبریل برافلاک کند سیر</p></div>
<div class="m2"><p>برصفحه هر دل که کلام تو نویسند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه ماه فلک سیرم ونه مهر جهانتاب</p></div>
<div class="m2"><p>تا بوسه من برلب بام تو نویسند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون سبزه تو سنگ ز تمکین تو مانند</p></div>
<div class="m2"><p>گر حشر شهیدان بخ خرام تو نویسند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شرط است که از هر دوجهان دست بشوید</p></div>
<div class="m2"><p>سیرابی هرکس که به جام تو نویسند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشاق به امید نگاه غلط انداز</p></div>
<div class="m2"><p>در نامه اغیار سلام تو نویسند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در بیضه ز بال وپرخودنغمه سرایان</p></div>
<div class="m2"><p>صدنامه سربسته به دام تو نویسند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صائب به شکر ریزی تسلیم شکر کن</p></div>
<div class="m2"><p>از قسمت اگر زهربه جام تو نویسند</p></div></div>