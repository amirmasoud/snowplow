---
title: >-
    غزل شمارهٔ ۱۴۸۹
---
# غزل شمارهٔ ۱۴۸۹

<div class="b" id="bn1"><div class="m1"><p>خط نارسته که در لعل لب جانان است</p></div>
<div class="m2"><p>همچو زهری است که در زیر نگین پنهان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خال مشکین تو از زلف دلاویزترست</p></div>
<div class="m2"><p>خط ریحان تو گیرنده تر از قرآن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قفل گردیدن دریاست نظر بستن من</p></div>
<div class="m2"><p>مژه بر هم زدنم بال وپر طوفان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زینهار از لب خندان به دل تنگ بساز</p></div>
<div class="m2"><p>که گشاد تو چو تیر از گره پیکان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کار بر زنده دلان چرخ نمی سازد تنگ</p></div>
<div class="m2"><p>پسته هر چند که در پوست بود خندان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سبز از آبله دست شود تخم امید</p></div>
<div class="m2"><p>گر چه ظاهر سبب نشو و نما باران است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عمر پیران کهنسال به سرعت گذرد</p></div>
<div class="m2"><p>رو به پستی چو نهد آب، سبک جولان است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یوسف افتاد گر از مکر زلیخا در بند</p></div>
<div class="m2"><p>مصر از جوش خریدار به من زندان است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست از داغ غباری به دل من صائب</p></div>
<div class="m2"><p>نفس سوختگان مغز مرا ریحان است</p></div></div>