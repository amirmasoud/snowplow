---
title: >-
    غزل شمارهٔ ۳۶۹۳
---
# غزل شمارهٔ ۳۶۹۳

<div class="b" id="bn1"><div class="m1"><p>فلک ز لنگر من باوقار می گردد</p></div>
<div class="m2"><p>زمین ز سایه من بیقرار می گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جنون ز سنگ ملامت نمی کند پروا</p></div>
<div class="m2"><p>چو کبک مست درین کوهسار می گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر کلافه اگر گم نکرده چرخ، چرا</p></div>
<div class="m2"><p>به گرد خاک چنین بیقرار می گردد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز چارپای عناصر پیاده هرکس شد</p></div>
<div class="m2"><p>به دوش چرخ چو عیسی سوار می گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز غرق امن بود کشتی سبکباران</p></div>
<div class="m2"><p>به خار و خس کف دریا کنار می گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به آفتاب جمال تو چشم هرکه فتاد</p></div>
<div class="m2"><p>چو سایه گرد تو بی اختیار می گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز دوری تو به من برخورد اگر سیماب</p></div>
<div class="m2"><p>ز بیقراری خود شرمسار می گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنین که چشم تو مست است از شراب غرور</p></div>
<div class="m2"><p>کجا ز سیلی خط هوشیار می گردد؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چرا خط از لب میگون او نگردد سبز؟</p></div>
<div class="m2"><p>ز باده راز نهان آشکار می گردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مده عنان سخن را ز دست چون منصور</p></div>
<div class="m2"><p>که چون بلند شود حرف، دار می گردد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مکش سر از خط فرمان تیغ همچو قلم</p></div>
<div class="m2"><p>که دل دو نیم چو شد ذوالفقار می گردد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو خضر تن به حیات ابد مده زنهار</p></div>
<div class="m2"><p>که آب، سبز درین جویبار می گردد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به هرکه عشق سر زنده ای کرامت کرد</p></div>
<div class="m2"><p>چو شمع در دل شب اشکبار می گردد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر ز نعمت الوان به خون شوی قانع</p></div>
<div class="m2"><p>ترا چو نامه نفس مشکبار می گردد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فغان که آدمی از پیش پای خود، آگاه</p></div>
<div class="m2"><p>به روشنایی شمع مزار می گردد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز خواب قطع نظر کن که وصل گل صائب</p></div>
<div class="m2"><p>نصیب شبنم شب زنده دار می گردد</p></div></div>