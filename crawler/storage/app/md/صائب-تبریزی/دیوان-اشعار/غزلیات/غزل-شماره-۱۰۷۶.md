---
title: >-
    غزل شمارهٔ ۱۰۷۶
---
# غزل شمارهٔ ۱۰۷۶

<div class="b" id="bn1"><div class="m1"><p>شبچراغ اهل معنی چشم بیدار من است</p></div>
<div class="m2"><p>همچو اختر در دل شب، روز بازار من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مصر انصاف از زلیخاهمتان خالی شده است</p></div>
<div class="m2"><p>ورنه چندین ماه کنعانی به بازار من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چه در کار کسی هرگز گره نفکنده ام</p></div>
<div class="m2"><p>سبحه صد دانه غم رشته کار من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>موج طوفان بلا چون دست بر ترکش زند</p></div>
<div class="m2"><p>تیر روی ترکشش مژگان خونبار من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صائب از بس همت من سربلند افتاده است</p></div>
<div class="m2"><p>لاله خورشید ننگ طرف دستار من است</p></div></div>