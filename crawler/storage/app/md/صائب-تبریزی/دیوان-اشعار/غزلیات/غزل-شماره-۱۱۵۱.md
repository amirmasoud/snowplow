---
title: >-
    غزل شمارهٔ ۱۱۵۱
---
# غزل شمارهٔ ۱۱۵۱

<div class="b" id="bn1"><div class="m1"><p>ابر رحمت با دل و دست گهربار آمده است</p></div>
<div class="m2"><p>چشم پل روشن، که آب امسال سرشار آمده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می زند جوش پریزاد از ریاحین بوستان</p></div>
<div class="m2"><p>کاروان در کاروان یوسف به بازار آمده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در حریم باغ، خاری بی گل بی خار نیست</p></div>
<div class="m2"><p>جوش خون لاله تا مژگان دیوار آمده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس که مرغان چمن بدمستی از حد می برند</p></div>
<div class="m2"><p>گل ز شبنم با هزاران چشم بیدار آمده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رخنه دیوارها چاک گریبان گل است</p></div>
<div class="m2"><p>هر سر خاری چو مژگان گهربار آمده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از شکوفه هر خیابانی کهکشانی گشته است</p></div>
<div class="m2"><p>صد هزاران اختر مسعود سیار آمده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از فروغ لاله و گل گشته یک چشم پر آب</p></div>
<div class="m2"><p>هر که چون شبنم به سیر باغ و گلزار آمده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بوستان را در کنار شاخ از هر بلبلی</p></div>
<div class="m2"><p>عیسیی در مهد پنداری به گفتار آمده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سبزه ها چون فوج طوطی از زمین برخاسته است</p></div>
<div class="m2"><p>از شکوفه شاخه ها دست شکربار آمده است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سنگ را از جا درآورده است شور نوبهار</p></div>
<div class="m2"><p>کوه چون کبک از سبکروحی به رفتار آمده است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از گل ابر آسمان یک دامن پر گل شده است</p></div>
<div class="m2"><p>کان لعل از هر رگ سنگی پدیدار آمده است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از هجوم لاله و گل، بر سر دیوار، خار</p></div>
<div class="m2"><p>چون زبان مار زنهاری به زنهار آمده است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از شفق خورشید تابان کاسه در صهبا زده است</p></div>
<div class="m2"><p>صبح از مستی برون آشفته دستار آمده است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خاک هر گنجی که در دل داشت بیرون داده است</p></div>
<div class="m2"><p>صبح محشر گویی از گلشن پدیدار آمده است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کلک گوهربار صائب تا نواپرداز شد</p></div>
<div class="m2"><p>خون به جای ناله بلبل را ز منقار آمده است</p></div></div>