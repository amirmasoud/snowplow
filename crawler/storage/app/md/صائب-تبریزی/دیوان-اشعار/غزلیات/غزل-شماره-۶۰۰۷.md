---
title: >-
    غزل شمارهٔ ۶۰۰۷
---
# غزل شمارهٔ ۶۰۰۷

<div class="b" id="bn1"><div class="m1"><p>پیچ و تاب عشق را نتوان ز جان برداشتن</p></div>
<div class="m2"><p>نیست ممکن موج از آب روان برداشتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون صدف من هم ز گوهر دامنی می داشتم</p></div>
<div class="m2"><p>می توانستم اگر دست از دهان برداشتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خانه خالی پر و بالی است بهر سالکان</p></div>
<div class="m2"><p>تیر را آسان بود دل از کمان برداشتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که از دل بار بردارد، گران بر دوش نیست</p></div>
<div class="m2"><p>از سبوی می گرانی می توان برداشتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست در دریای شورانگیز عالم موج را</p></div>
<div class="m2"><p>هیچ تدبیری به از دست از عنان برداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خدا تا کی به دنیای دنی قانع شدن؟</p></div>
<div class="m2"><p>چند از خوان سلیمان استخوان برداشتن؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برنمی گردد به ابر از گوهر شهوار آب</p></div>
<div class="m2"><p>نیست ممکن دل ز لعل دلستان برداشتن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پسته بی مغز در لب بستگی رسواترست</p></div>
<div class="m2"><p>نیست حاجت پرده از کار جهان برداشتن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوشتر از صد باغ و بستان است کنج عافیت</p></div>
<div class="m2"><p>با قفس سهل است دل از گلستان برداشتن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می توان برداشت دل صائب به آسانی ز جان</p></div>
<div class="m2"><p>لیک دشوارست دل از دوستان برداشتن</p></div></div>