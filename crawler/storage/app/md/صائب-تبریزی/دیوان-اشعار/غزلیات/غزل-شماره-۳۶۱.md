---
title: >-
    غزل شمارهٔ ۳۶۱
---
# غزل شمارهٔ ۳۶۱

<div class="b" id="bn1"><div class="m1"><p>خوشا روزی که بینم دلبر بگزیده خود را</p></div>
<div class="m2"><p>ز رخسارش برافروزم چراغ دیده خود را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرا ممنون شوم از گلشن آرا من که می دانم</p></div>
<div class="m2"><p>به از صد دسته گل، دامن برچیده خود را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دامان صدف بار دگر افکندم از ساحل</p></div>
<div class="m2"><p>ز قحط قدردانان گوهر سنجیده خود را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرآمد چون جرس هر چند در فریاد عمر من</p></div>
<div class="m2"><p>نشد بیدار سازم طالع خوابیده خود را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آب زندگی ریگ روان سیری نمی دارد</p></div>
<div class="m2"><p>ز می سیراب چون سازم دل غم دیده خود را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صدف از ابر نیسان می کند بیجا گهر پنهان</p></div>
<div class="m2"><p>نگیرد پس کریم از سایلان بخشیده خود را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیندازد به هر آلوده دامن عشق او سایه</p></div>
<div class="m2"><p>به خاصان می دهد شه، جامه پوشیده خود را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همان شایسته رخسار او صائب نمی دانم</p></div>
<div class="m2"><p>اگر در چشمه خورشید شویم دیده خود را</p></div></div>