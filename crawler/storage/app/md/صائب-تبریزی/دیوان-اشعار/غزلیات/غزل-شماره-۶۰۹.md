---
title: >-
    غزل شمارهٔ ۶۰۹
---
# غزل شمارهٔ ۶۰۹

<div class="b" id="bn1"><div class="m1"><p>چو تار چنگ، فلک چون نمی نواخت مرا</p></div>
<div class="m2"><p>به حیرتم که چرا این قدر گداخت مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر چه سوز محبت ز من اثر نگذاشت</p></div>
<div class="m2"><p>به بوی سوختگی می توان شناخت مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چرا به آتش هجران حواله باید کرد؟</p></div>
<div class="m2"><p>چو می توان به نگاهی کباب ساخت مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر چه نقش حریفان شش و ز من یک بود</p></div>
<div class="m2"><p>رهین طالع خویشم که کم نباخت مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کباب داغ جنونم، که این ستاره شوخ</p></div>
<div class="m2"><p>ز آفتاب قیامت خجل نساخت مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درین ستمکده آن شمع تیره روزم من</p></div>
<div class="m2"><p>که انتظار نسیم سحر گداخت مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکست هر که مرا، در شکست خود کوشید</p></div>
<div class="m2"><p>ز خویش گرد برآورد هر که تاخت مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو ماه مصر عزیز جهان نمی گشتم</p></div>
<div class="m2"><p>اگر تپانچه اخوان نمی نواخت مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کنم چگونه ادا شکر بی وجودی را؟</p></div>
<div class="m2"><p>که از شکنجه هستی خلاص ساخت مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا چو رشته به مکتوب می توان پیچید</p></div>
<div class="m2"><p>ز بس که دوری آن سنگدل گداخت مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه یار و دوست شناسم نه خویش را صائب</p></div>
<div class="m2"><p>که آشنایی او کرد ناشناخت مرا</p></div></div>