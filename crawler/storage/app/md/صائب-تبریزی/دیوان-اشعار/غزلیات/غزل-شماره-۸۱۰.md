---
title: >-
    غزل شمارهٔ ۸۱۰
---
# غزل شمارهٔ ۸۱۰

<div class="b" id="bn1"><div class="m1"><p>گمراه کند غفلت من راهبران را</p></div>
<div class="m2"><p>چون خواب، زمین گیر کند همسفران را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی بهره ز معشوق بود عاشق محجوب</p></div>
<div class="m2"><p>روزی ز دل خویش بود بی جگران را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کوه و کمر از ره باریک خطرهاست</p></div>
<div class="m2"><p>زنهار به دنبال مرو خوش کمران را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون صبح مدر پرده شب را که مکافات</p></div>
<div class="m2"><p>در خون جگر غوطه دهد پرده دران را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آتش نفسان نرم نگردد دل سختم</p></div>
<div class="m2"><p>این سنگ کند خون به جگر شیشه گران را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اکسیر شد از قرب گهر گرد یتیمی</p></div>
<div class="m2"><p>از دست مده دامن روشن گهران را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر نامه که انشا کنم از درد جدایی</p></div>
<div class="m2"><p>مقراض شود بال و پر نامه بران را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با دیده حیران چه کند خواب پریشان؟</p></div>
<div class="m2"><p>صائب چه غم از شور جهان بی خبران را؟</p></div></div>