---
title: >-
    غزل شمارهٔ ۵۱۷۳
---
# غزل شمارهٔ ۵۱۷۳

<div class="b" id="bn1"><div class="m1"><p>آتشین شد چهره خاک از می گلرنگ عشق</p></div>
<div class="m2"><p>چرخ شد خاکستری از آتش بی رنگ عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می نماید چون گل خورشید از آب روان</p></div>
<div class="m2"><p>چهره اندیشه از آیینه بی زنگ عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون گذشتی از فضای دل درین وحشت سرا</p></div>
<div class="m2"><p>درخور جولان ندارد عرصه ای شبرنگ عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با کدامین شیشه دل گویم که در میدان رزم</p></div>
<div class="m2"><p>کرد کار مومیایی با دل من سنگ عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک سیه خانه است در سرتاسر صحرای عقل</p></div>
<div class="m2"><p>کعبه ای سرگشته می گردد به هر فرسنگ عشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست ابر و آفتاب نوبهاران را بقا</p></div>
<div class="m2"><p>ساده لوح آن کس که دل بندد به صلح و جنگ عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زور بازوی یداللهی بلند افتاده است</p></div>
<div class="m2"><p>چون ننالد نه کمان آسمان در چنگ عشق؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خامسوزان هوس بر خود بساطی چیده اند</p></div>
<div class="m2"><p>ورنه خاکستر ندارد آتش بی رنگ عشق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا به حشر از چشم زخم نیستی آسوده است</p></div>
<div class="m2"><p>چهره هرکس که شد نیلوفری از سنگ عشق</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جوشن داودی اینجا شاهراه ناوک است</p></div>
<div class="m2"><p>من کیم تا سینه را سازم سپر در جنگ عشق؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ذره تا خورشید گلبانگ انالحق می زند</p></div>
<div class="m2"><p>نغمه خارج ندارد ساز سیر آهنگ عشق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دامن رغبت زلیخا از کف یوسف کشد</p></div>
<div class="m2"><p>دست چون بیرون کند از آستین نیرنگ عشق</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خامه اش را شق به شمشیر شهادت می زنند</p></div>
<div class="m2"><p>هرکه چون شیر خدا صائب بود یکرنگ عشق</p></div></div>