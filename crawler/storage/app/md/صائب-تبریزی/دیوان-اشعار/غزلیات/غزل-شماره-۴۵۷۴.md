---
title: >-
    غزل شمارهٔ ۴۵۷۴
---
# غزل شمارهٔ ۴۵۷۴

<div class="b" id="bn1"><div class="m1"><p>بیقرار عشق در یک جا نمی گیرد قرار</p></div>
<div class="m2"><p>کوه اگر لنگر شود دریا نمی گیرد قرار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسمان بیهوده در اندیشه تسخیر ماست</p></div>
<div class="m2"><p>باده پر زور در مینا نمی گیرد قرار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیو را شیشه سر بسته نتوان بند کرد</p></div>
<div class="m2"><p>هیچ دل در قبه خضرا نمی گیرد قرار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخیه نتوان زد به شبنم دیده خورشید را</p></div>
<div class="m2"><p>خواب در چشم ودل بینانمی گیرد قرار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رشته شیرازه اوراق افلاکیم ما</p></div>
<div class="m2"><p>نظم عالم بی وجود مانمی گیرد قرار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا نظر بازست، دل در سینه دارد اضطراب</p></div>
<div class="m2"><p>شمع بی فانوس در صحرا نمی گیرد قرار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می دود درکوچه وبازار آخر راز عشق</p></div>
<div class="m2"><p>این شرر در سینه خارا نمی گیرد قرار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غیر دل کز پهلوی من برنخیزد روزوشب</p></div>
<div class="m2"><p>هیچ پیکان دربدن یک جا نمی گیرد قرار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غیر دریا، سیل در هر جا بود زندان اوست</p></div>
<div class="m2"><p>عاشق شوریده در دنیا نمی گیردقرار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پرتو خورشید بستر بر سر دریا فکند</p></div>
<div class="m2"><p>عکس او در چشم خونپالانمی گیرد قرار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر که چون دل کوچه گرد زلف وکاکل گشته است</p></div>
<div class="m2"><p>در فضای جنت المأوی نمی گیرد قرار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شیشه ساعت بود گردون وغم ریگ روان</p></div>
<div class="m2"><p>یکدم از گردش غم دنیا نمی گیرد قرار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>محنت دنیابه نوبت سیر دلها می کند</p></div>
<div class="m2"><p>کاروان ریگ دریک جا نمی گیرد قرار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عاقبت از خانه آیینه هم دلگیر شد</p></div>
<div class="m2"><p>در بهشت آن شوخ بی پروا نمی گیرد قرار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر نباشد گوشه چشم غزالان در نظر</p></div>
<div class="m2"><p>یک نفس مجنون درین صحرا نمی گیرد قرار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بوی پیراهن سفیدی می برد از چشم ما</p></div>
<div class="m2"><p>ابر دایم بر دریا نمی گیرد قرار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>روح قدسی چون کند لنگر درین وحشت سرا؟</p></div>
<div class="m2"><p>کوه در دامان (این) صحرا نمی گیرد قرار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کوه غم لنگر نیفکنده است صائب دردلش</p></div>
<div class="m2"><p>نقش پای هرکه در خارا نمی گیرد قرار</p></div></div>