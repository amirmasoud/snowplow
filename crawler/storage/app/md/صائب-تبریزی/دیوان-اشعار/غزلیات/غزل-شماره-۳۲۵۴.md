---
title: >-
    غزل شمارهٔ ۳۲۵۴
---
# غزل شمارهٔ ۳۲۵۴

<div class="b" id="bn1"><div class="m1"><p>جوهر می ز رگ ابر مثنی گردد</p></div>
<div class="m2"><p>از شفق رنگ می لعل دو بالا گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک زمان پرده ازان روی دل آرا بردار</p></div>
<div class="m2"><p>تا سیه خانه این دشت سویدا گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاکساری است که از درد طلب می پیچد</p></div>
<div class="m2"><p>گردبادی که درین دامن صحرا گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شوق اگر عام کند سلسله جنبانی را</p></div>
<div class="m2"><p>کوه چون ریگ روان بادیه پیما گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شود از آه پریشان دل خورشید سیاه</p></div>
<div class="m2"><p>خط ز رخسار تو روزی که هویدا گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کوهکن را به سخن صورت شیرین نگذاشت</p></div>
<div class="m2"><p>لاف بیکار بود کار چو گویا گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نامه تسکین ندهد دیده مشتاقان را</p></div>
<div class="m2"><p>کف محال است که مهر لب دریا گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گریه مردم بیدرد شود خرج زمین</p></div>
<div class="m2"><p>این نه سیلی است که پیوسته به دریا گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر بداند چه ثمرهاست تهیدستی را</p></div>
<div class="m2"><p>سرو آواره ز گلزار به یک پا گردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرکه صائب شود از باده عرفان سرگرم</p></div>
<div class="m2"><p>همچو خورشید درین دایره تنها گردد</p></div></div>