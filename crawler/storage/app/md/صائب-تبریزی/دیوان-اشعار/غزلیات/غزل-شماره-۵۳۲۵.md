---
title: >-
    غزل شمارهٔ ۵۳۲۵
---
# غزل شمارهٔ ۵۳۲۵

<div class="b" id="bn1"><div class="m1"><p>گفتگوی عشق را من در میان انداختم</p></div>
<div class="m2"><p>طرح جوهر من به شمشیر زبان انداختم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نامی از شور محبت بر زبانها مانده بود</p></div>
<div class="m2"><p>این نمک من در خمیر خاکیان انداختم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داشت بردورهدف جولان خدنگ اهل فکر</p></div>
<div class="m2"><p>این پریشان سیر را من بر نشان انداختم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی دریای سخن را خاروخس پوشیده داشت</p></div>
<div class="m2"><p>این خس و خاشاک را من برکران انداختم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرخ کاه کهنه ای می داد پیش از من به باد</p></div>
<div class="m2"><p>دانه من در آسیای آسمان انداختم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من ز لوح خاک شستم ابجد عشق مجاز</p></div>
<div class="m2"><p>شورش عشق حقیقی در جهان انداختم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جلوه یوسف نیفکنده است در بازار مصر</p></div>
<div class="m2"><p>از سخن شوری که در اصفهان انداختم</p></div></div>