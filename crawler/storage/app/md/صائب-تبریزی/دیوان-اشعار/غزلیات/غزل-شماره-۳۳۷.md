---
title: >-
    غزل شمارهٔ ۳۳۷
---
# غزل شمارهٔ ۳۳۷

<div class="b" id="bn1"><div class="m1"><p>نه بوی گل، نه رنگ لاله از جا می برد ما را</p></div>
<div class="m2"><p>به گلشن لذت ترک تماشا می برد ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو عالم از تمنا شد بیابان مرگ ناکامی</p></div>
<div class="m2"><p>همان خامی به دنبال تمنا می برد ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مکن تکلیف همراهی به ما ای سیل پا در گل</p></div>
<div class="m2"><p>که دست از جان خود شستن به دریا می برد ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر چه در دو عالم نیست میدان جنون ما</p></div>
<div class="m2"><p>همان بی طاقتی صحرا به صحرا می برد ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کمند جذبه خورشید اگر رحمت نفرماید</p></div>
<div class="m2"><p>که چون شبنم ازین پستی به بالا می برد ما را؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنان آماده عشقیم از فیض سبکروحی</p></div>
<div class="m2"><p>که حسن صورت دیوار از جا می برد ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به طوفان گوهر از گرد یتیمی برنمی آید</p></div>
<div class="m2"><p>چه گرد از چهره دل موج صهبا می برد ما را؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که باور می کند با این توانایی ز ما صائب؟</p></div>
<div class="m2"><p>که چشم ناتوان او به یغما می برد ما را</p></div></div>