---
title: >-
    غزل شمارهٔ ۳۵۷۰
---
# غزل شمارهٔ ۳۵۷۰

<div class="b" id="bn1"><div class="m1"><p>باد را راه در آن طره پیچان نبود</p></div>
<div class="m2"><p>شانه را دست بر آن زلف پریشان نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در شهادت دل من همت دیگر دارد</p></div>
<div class="m2"><p>نشوم کشته به زخمی که نمایان نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عندلیبی که به هر غنچه دلش می لرزد</p></div>
<div class="m2"><p>بهتر آن است که در صحن گلستان نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل ز تسخیر سر زلف تو شد شادی مرگ</p></div>
<div class="m2"><p>مور را حوصله ملک سلیمان نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صحبت دختر رز طرفه خماری دارد</p></div>
<div class="m2"><p>هیچ کس نیست که از توبه پشیمان نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شرمگینان به خموشی ادب خصم کنند</p></div>
<div class="m2"><p>تیغ این طایفه در معرکه عریان نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شانه را ناخن تدبیر به سرپنجه نماند</p></div>
<div class="m2"><p>مشکل زلف گشودن زهم، آسان نبود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنقدر شور که باید همه با خود دارد</p></div>
<div class="m2"><p>داغ ما در خم تاراج نمکدان نبود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مصرعی سر نزند از لب فکرت صائب</p></div>
<div class="m2"><p>اگر آن زلف سیه سلسله جنبان نبود</p></div></div>