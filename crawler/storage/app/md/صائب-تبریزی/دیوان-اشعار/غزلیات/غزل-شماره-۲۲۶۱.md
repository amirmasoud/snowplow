---
title: >-
    غزل شمارهٔ ۲۲۶۱
---
# غزل شمارهٔ ۲۲۶۱

<div class="b" id="bn1"><div class="m1"><p>غباری بجا از زمین مانده است</p></div>
<div class="m2"><p>بخاری ز چرخ برین مانده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز گل خار مانده است و از می خمار</p></div>
<div class="m2"><p>چه ها از که ها بر زمین مانده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پریده است عقل از سر مردمان</p></div>
<div class="m2"><p>نگین خانه ها بی نگین مانده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به این تنگدستان ز ارباب حال</p></div>
<div class="m2"><p>لباس فراخ آستین مانده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همین ریش و دستار و عرض شکم</p></div>
<div class="m2"><p>بجا از بزرگان دین مانده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز ازرق لباسان خورشید روی</p></div>
<div class="m2"><p>همین یک سپهر برین مانده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منم صائب امروز بر لوح خاک</p></div>
<div class="m2"><p>اگر یک سخن آفرین مانده است</p></div></div>