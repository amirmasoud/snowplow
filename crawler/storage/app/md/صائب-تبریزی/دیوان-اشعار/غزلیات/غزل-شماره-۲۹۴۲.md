---
title: >-
    غزل شمارهٔ ۲۹۴۲
---
# غزل شمارهٔ ۲۹۴۲

<div class="b" id="bn1"><div class="m1"><p>دگر هر ذره خاکم هوای کشوری دارد</p></div>
<div class="m2"><p>سر آسوده مغزم با پریشانی سری دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چسان مژگان آسایش به مژگان آشنا سازم؟</p></div>
<div class="m2"><p>به قصد خون من هر موی در کف خنجری دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی صد می شود تخم کدورت در دل تنگم</p></div>
<div class="m2"><p>زمین دردمندان خاک حاصل پروری دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوارا باد وصل خرمن گل عندلیبان را</p></div>
<div class="m2"><p>که آغوش من انداز میان لاغری دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مکن تقصیر در تعمیر دل تا دسترس داری</p></div>
<div class="m2"><p>که هر کس هر چه دارد از برای دیگری دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخن کش گو مجنبان گوشه ابرو به تحسینم</p></div>
<div class="m2"><p>سخن بر جا نمی ماند اگر بال و پری دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگردد در قیامت تکمه پیراهن خجلت</p></div>
<div class="m2"><p>سر هر کس که اینجا با سر زانو سری دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مبادا لب به آب زندگی چون خضرترسازی</p></div>
<div class="m2"><p>که هر تبخاله ای در پرده دل کوثری دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تهیدستی به میدان می دواند اهل دعوی را</p></div>
<div class="m2"><p>نمی جنبد صدف از جای خود تا گوهری دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به گوش من زبان تیشه فرهاد می گوید</p></div>
<div class="m2"><p>به سختی بگذراند عمر، هر کس جوهری دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شکر شیرینی بسیار، دل را می گزد صائب</p></div>
<div class="m2"><p>وگرنه طوطی ما نیز تنگ شکری دارد</p></div></div>