---
title: >-
    غزل شمارهٔ ۲۲۱
---
# غزل شمارهٔ ۲۲۱

<div class="b" id="bn1"><div class="m1"><p>نیست در طالع قدوم میهمان این خانه را</p></div>
<div class="m2"><p>سیل بردارد مگر از خاک، این ویرانه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست و پا گم کردم از نظاره آن چشم مست</p></div>
<div class="m2"><p>من که بر سر می کشیدم این نفس میخانه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این که کردم خرده جان صرف این بی حاصلان</p></div>
<div class="m2"><p>می فشاندم در زمین شور کاش این دانه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پنجه مشکل گشا هرگز نمی افتد ز کار</p></div>
<div class="m2"><p>هست در خشکی گشایش بیش، دست شانه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد جهان بر چشم من از رفتن جانان سیاه</p></div>
<div class="m2"><p>برد با خود میهمان من چراغ خانه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بحر را موج خطر مانع نمی گردد ز شور</p></div>
<div class="m2"><p>می کند ویرانه تر زنجیر این دیوانه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آب در استادگی از سرو یابد فیض بیش</p></div>
<div class="m2"><p>چشم حیران قدر داند جلوه مستانه را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاشقان را نیست بر دل، سردی معشوق بار</p></div>
<div class="m2"><p>شمع کافوری نسازد دل خنک پروانه را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چوبکاری آتش سوزنده را بال و پرست</p></div>
<div class="m2"><p>چوب گل سازد دو بالا شورش دیوانه را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل نگیرد یک نفس در سینه گرمم قرار</p></div>
<div class="m2"><p>تابه تفسیده از خود دور سازد دانه را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هست زور می کلید خانگی این قفل را</p></div>
<div class="m2"><p>از برون گر محتسب بندد در میخانه را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بی سخن، در کوزه لب بسته دارد خامشی</p></div>
<div class="m2"><p>گر شراب بی خماری هست این میخانه را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>می برد خاشاک اگر از طبع آتش سرکشی</p></div>
<div class="m2"><p>چوب گل هم می کند عاقل من دیوانه را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عاشقان را وصل در سرگشتگی باشد که شمع</p></div>
<div class="m2"><p>مرکز پرگار بال و پر شود پروانه را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نیست صائب در ترازوی شعورش سنگ کم</p></div>
<div class="m2"><p>هر که در یک پله دارد کعبه و بتخانه را</p></div></div>