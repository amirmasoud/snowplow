---
title: >-
    غزل شمارهٔ ۴۳۶۵
---
# غزل شمارهٔ ۴۳۶۵

<div class="b" id="bn1"><div class="m1"><p>آسایش تن غافلم از یاد خداکرد</p></div>
<div class="m2"><p>از طینت بادام به شکر نتوان برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این خانه خرابی به حباب است سزاوار</p></div>
<div class="m2"><p>از آب روان خانه نبایست جداکرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی جذبه به جایی نرسد کوشش رهرو</p></div>
<div class="m2"><p>برگردم ازان ره که توان رو به قفاکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون نافه نفس در جگر باد صبا سوخت</p></div>
<div class="m2"><p>تا یک گره از زلف گرهگیر تو واکرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در رهگذرش چاه شود دیده حسرت</p></div>
<div class="m2"><p>از راستی آن کس که درین راه عصا کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی رنج طلب روی دهد آنچه نخواهی</p></div>
<div class="m2"><p>دولت عجبی نیست اگر روی به ما کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در معرکه عشق دلیرانه متازید</p></div>
<div class="m2"><p>بر صفحه دریا نتوان مشق شناکرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اقرار نکردن به گنه عین گناه است</p></div>
<div class="m2"><p>قایل نشد آن کس که به تقصیر خطا کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قایل به خطا باش که مردود جهان شد</p></div>
<div class="m2"><p>هر کس گنه خویش حوالت به قضا کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دور فلک از زمزمه عشق تهی بود</p></div>
<div class="m2"><p>این دایره را خامه صائب به نوا کرد</p></div></div>