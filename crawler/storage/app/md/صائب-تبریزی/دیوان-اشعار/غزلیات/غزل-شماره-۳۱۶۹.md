---
title: >-
    غزل شمارهٔ ۳۱۶۹
---
# غزل شمارهٔ ۳۱۶۹

<div class="b" id="bn1"><div class="m1"><p>سخن پوشیده در لعل لب جانان نمی‌ماند</p></div>
<div class="m2"><p>اگرچه در عدم باشد سخن پنهان نمی‌ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نپوشد خط مشکین آب و رنگ لعل جانان را</p></div>
<div class="m2"><p>نهان در تیرگی این چشمه حیوان نمی‌ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خوابی می‌شود آزاد روح از قید آب و گل</p></div>
<div class="m2"><p>تمام عمر ماه مصر در زندان نمی‌ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شود هر اختری زیر فلک در وقت خود طالع</p></div>
<div class="m2"><p>رسد چون نوبت نان طفل بی‌دندان نمی‌ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بشو دست از دل آسوده در دوران زلف او</p></div>
<div class="m2"><p>که گر این است چوگان، گوی در میدان نمی‌ماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخن بر گرد عالم می‌دود گر رتبه‌ای دارد</p></div>
<div class="m2"><p>متاع یوسفی در گوشه دکان نمی‌ماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همانا دانه امید ما را سوخت نومیدی</p></div>
<div class="m2"><p>وگرنه تخم در زیر زمین پنهان نمی‌ماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کدامین شوخ چشم امروز جا دارد درین گلشن؟</p></div>
<div class="m2"><p>که در کاویدن دل خارش از مژگان نمی‌ماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به دلتنگی قناعت کن ثبات عمر اگر خواهی</p></div>
<div class="m2"><p>که چون شد غنچه گل، در بوستان چندان نمی‌ماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عبث در پنبه داغ خویش پنهان می‌کنم صائب</p></div>
<div class="m2"><p>چراغ شوخ هرگز در ته دامان نمی‌ماند</p></div></div>