---
title: >-
    غزل شمارهٔ ۱۶۵۳
---
# غزل شمارهٔ ۱۶۵۳

<div class="b" id="bn1"><div class="m1"><p>اگر ز عالم تسلیم گوشه ای داری</p></div>
<div class="m2"><p>بهشت و طوبی و حوران خوش لقا اینجاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهار در دل هر غنچه عالمی دارد</p></div>
<div class="m2"><p>ترا خیال که عالم همین و جا اینجاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر تو سر به گریبان خود بری چو گره</p></div>
<div class="m2"><p>گرهگشای تو با روی دلگشا اینجاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آن جهان نتوان یافتن سعادت عشق</p></div>
<div class="m2"><p>سری برآر ز خود، سایه هما اینجاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه چشم، کز تو به هر جا نظر کند عاشق</p></div>
<div class="m2"><p>کند خیال که حسن ترا حیا اینجاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشیده دار درین دشت پر فریب، عنان</p></div>
<div class="m2"><p>که صد هزار سراب غلط نما اینجاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه احتیاج دلیل است بوی یوسف را؟</p></div>
<div class="m2"><p>نسیم پیرهن و بوی آشنا اینجاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوای درد طلب نیست در جهان صائب</p></div>
<div class="m2"><p>ترا خیال که این درد را دوا اینجاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز کوی عشق به جنت روی، بلا اینجاست</p></div>
<div class="m2"><p>ره صواب ندانسته ای، خطا اینجاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>توان ز خدمت پیر مغان جوانی یافت</p></div>
<div class="m2"><p>نهان مکن مس خود را که کیمیا اینجاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر ز خویش برون خواهی آمدن روزی</p></div>
<div class="m2"><p>قدم به راه نه اکنون که رهنما اینجاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز بر نیامدن مدعا مباش غمین</p></div>
<div class="m2"><p>چه مدعا به جز از ترک مدعا اینجاست؟</p></div></div>