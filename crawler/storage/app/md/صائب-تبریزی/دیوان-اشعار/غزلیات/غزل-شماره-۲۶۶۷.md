---
title: >-
    غزل شمارهٔ ۲۶۶۷
---
# غزل شمارهٔ ۲۶۶۷

<div class="b" id="bn1"><div class="m1"><p>هر سبک مغزی که غافل شد ز دل باطل شود</p></div>
<div class="m2"><p>کاه چون بی دانه گردد خرج آب و گل شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از غبار جسم پروا نیست سالک را که سیل</p></div>
<div class="m2"><p>از گرانسنگی به دریا زودتر واصل شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می برد از دیدن هر ذره فیض آفتاب</p></div>
<div class="m2"><p>دیده هرکس که روشن از فروغ دل شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>موش با جاروب در سوراخ نتوانست رفت</p></div>
<div class="m2"><p>خواجه با چندین علایق چون به حق واصل شود؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در بیابان سهل باشد چشم پوشیدن ز خضر</p></div>
<div class="m2"><p>وای بر آن کس که از یاد خدا غافل شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لعل گردد سنگ اگر از انقلاب روزگار</p></div>
<div class="m2"><p>نیست ممکن هر که مجنون شد دگر عاقل شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می کشد هرکس که آهی ما پریشان می شویم</p></div>
<div class="m2"><p>بید مجنون از نسیمی هر طرف مایل شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان ز قرب جسم در رفتن گرانی می کند</p></div>
<div class="m2"><p>هر که با کاهل رفیق راه شد کاهل شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جبهه واکرده بر محتاج ابر رحمت است</p></div>
<div class="m2"><p>چین ابرو آیه نومیدی سایل شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مد آهی می کند زیر وزبر افلاک را</p></div>
<div class="m2"><p>آنچنان کز خط کشیدن صفحه ای باطل شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مشت خاکی چون شود سیلاب را مانع ز بحر؟</p></div>
<div class="m2"><p>دیده ما را کجا دیوار و در حایل شود؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در زوال خویش دارد سعی همچون آفتاب</p></div>
<div class="m2"><p>هر که از پستی به معراج شرف مایل شود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از تراشیدن نگردد صاف روی نوخطان</p></div>
<div class="m2"><p>ریشه جوهر به آب تیغ کی زایل شود؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیست در یکتایی حق هیچ کس را اشتباه</p></div>
<div class="m2"><p>در نماز و تر ممکن نیست شک داخل شود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>داغ چون شد کهنه بر خاطر گرانی می کند</p></div>
<div class="m2"><p>شمع چون خاموش گردد بار بر محفل شود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سیل را هر موجه دریا عنان دیگرست</p></div>
<div class="m2"><p>رهنورد شوق کی آسوده در منزل شود؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دست از تعمیر تن بردار در پیرانه سر</p></div>
<div class="m2"><p>راست نتوان ساختن دیوار چون مایل شود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دیده پوشیده را صائب گشاد از حیرت است</p></div>
<div class="m2"><p>بر خط تسلیم سر نه، کار چون مشکل شود</p></div></div>