---
title: >-
    غزل شمارهٔ ۴۱۳۰
---
# غزل شمارهٔ ۴۱۳۰

<div class="b" id="bn1"><div class="m1"><p>روی ترا به آتش دل‌ها برشته‌اند</p></div>
<div class="m2"><p>لعل ترا به خون جگرها سرشته‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای شاخ گل ببال که در مزرع وجود</p></div>
<div class="m2"><p>چون خال دل‌فریب تو تخمی نَکِشته‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ظاهر نمی‌شود که چه مضمون دلکش است</p></div>
<div class="m2"><p>هرچند خط سبز ترا خوش نوشته‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل را به آب دیده خونبار تازه دار</p></div>
<div class="m2"><p>کاین دانه را به زحمت بسیار کِشته‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از کجروی عنان نگه را کشیده دار</p></div>
<div class="m2"><p>کاین تار را به دقت بسیار رشته‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اوراق چرخ زیر وزبر گشته سال‌ها</p></div>
<div class="m2"><p>تا نسخه وجود تو بیرون نوشته‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نان تو چون فطیر بماند در این تنور</p></div>
<div class="m2"><p>با دست خود خمیر تو در هم سرشته‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از دودش آفتاب قیامت زبانه‌ای است</p></div>
<div class="m2"><p>در آتشی که دانه ما را برشته‌اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جمعی که سایه بر سر افتادگان کنند</p></div>
<div class="m2"><p>در سایه حمایت بال فرشته‌اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ایمن مشو که نیست مگر بهر امتحان</p></div>
<div class="m2"><p>صائب اگر عنان تو از دست هِشته‌اند</p></div></div>