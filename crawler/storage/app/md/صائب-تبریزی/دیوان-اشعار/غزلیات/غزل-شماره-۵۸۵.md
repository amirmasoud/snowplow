---
title: >-
    غزل شمارهٔ ۵۸۵
---
# غزل شمارهٔ ۵۸۵

<div class="b" id="bn1"><div class="m1"><p>چه نسبت است به گردنکشی مدارا را؟</p></div>
<div class="m2"><p>قدح خراج به گردن نهاد مینا را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان که روشنی خانه است از روزن</p></div>
<div class="m2"><p>به قدر داغ بود نور فیض، دلها را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز من مپرس که در دل چه آرزو داری</p></div>
<div class="m2"><p>که سوخت عشق رگ و ریشه تمنا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عنان سیل سبکرو به دست خودرایی است</p></div>
<div class="m2"><p>چه انتظام توان داد کار دنیا را؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز همرهان گرانجان ببر که سوزن دوخت</p></div>
<div class="m2"><p>به دامن فلک چارمین مسیحا را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرفت در عوض آب تلخ، گوهر ناب</p></div>
<div class="m2"><p>چه منت است به ابر بهار دریا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز نقطه حرف شناسان کتاب دان شده اند</p></div>
<div class="m2"><p>به چشم کم منگر نقطه سویدا را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به منتهای مطالب رسیدن آسان است</p></div>
<div class="m2"><p>اگر شمرده توانی گذاشتن پا را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به یک گواه لباسی که ماه مصر آورد</p></div>
<div class="m2"><p>سیاه کرد رخ دعوی زلیخا را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز نقش پای غزالان دشت بتوان یافت</p></div>
<div class="m2"><p>به بوی مشک، پی آن غزال رعنا را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر چه گریه من کوه را بیابان کرد</p></div>
<div class="m2"><p>نمود کوه غمم کوهسار صحرا را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جواب آن غزل مولوی است این صائب</p></div>
<div class="m2"><p>که چشم بند کند سحرهاش بینا را</p></div></div>