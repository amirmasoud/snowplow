---
title: >-
    غزل شمارهٔ ۵۱۱۶
---
# غزل شمارهٔ ۵۱۱۶

<div class="b" id="bn1"><div class="m1"><p>افتاد ماه عارض او در وبال خط</p></div>
<div class="m2"><p>زلفش هوا گرفت به یک گوشمال خط</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منسوخ گشت چون خط کوفی ز خط نسخ</p></div>
<div class="m2"><p>طغرای چین ابروی او از مثال خط</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر صد هزار تبت وارونه خوانده ای</p></div>
<div class="m2"><p>بیرون نمی رود ز ضمیرت ملال خط</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قیر دوال پا که شنیدی خط است و بس</p></div>
<div class="m2"><p>چند آبروی تیغ بری در زوال خط</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواهد فتاد کوکب بخت بلند تو</p></div>
<div class="m2"><p>از آه و دود سوختگان در وبال خط</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رحمی به خاکساری عاشق نمی کنی</p></div>
<div class="m2"><p>تا روی نازکت نخورد خاکمال خط</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب چه دولتی است که روشن نموده است</p></div>
<div class="m2"><p>چشمم سواد خط غبار از خیال خط</p></div></div>