---
title: >-
    غزل شمارهٔ ۶۹۸۱
---
# غزل شمارهٔ ۶۹۸۱

<div class="b" id="bn1"><div class="m1"><p>نمی آید از من دگر بردباری</p></div>
<div class="m2"><p>دو دست من و دامن بی قراری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من و طفل شوخی که صد خانه زین</p></div>
<div class="m2"><p>ز مردان تهی ساخت در نی سواری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معلم کباب است از شوخی او</p></div>
<div class="m2"><p>کند برق را ابر چون پرده داری؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کند کبک تقلید رفتار او را</p></div>
<div class="m2"><p>ادب نیست در مردم کوهساری!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا کارافتاده صائب به شوخی</p></div>
<div class="m2"><p>که کاری ندارد به جز زخم کاری</p></div></div>