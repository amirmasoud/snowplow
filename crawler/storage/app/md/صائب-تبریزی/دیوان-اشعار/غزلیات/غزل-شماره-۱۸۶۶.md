---
title: >-
    غزل شمارهٔ ۱۸۶۶
---
# غزل شمارهٔ ۱۸۶۶

<div class="b" id="bn1"><div class="m1"><p>در عین بحر، گوشه نشین را کناره هاست</p></div>
<div class="m2"><p>در یتیم را ز صدف گاهواره هاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا داده ام عنان توکل ز دست خویش</p></div>
<div class="m2"><p>کارم همیشه در گره از استخاره هاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از زاهدان خشک حدث گهر مپرس</p></div>
<div class="m2"><p>خار و خس از محیط نصیب کناره هاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نادان دلش خوش است به تدبیر ناخدا</p></div>
<div class="m2"><p>غافل که ناخدا هم ازین تخته پاره هاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آب فسرده در صدف پاک گو مباش</p></div>
<div class="m2"><p>گوش ترا چه حاجت این گوشواره هاست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از راز عشق، زاهد خشک است بیخبر</p></div>
<div class="m2"><p>ابروی قبله را چه خبر از اشاره هاست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از ما مجوی صبر که سررشته شکیب</p></div>
<div class="m2"><p>از دست رفته تر ز عنان نظاره هاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مور ضعیف اگر چه برابر بود به خاک</p></div>
<div class="m2"><p>نسبت به خاکساری من از سواره هاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دربسته ماند میکده از زاهدان خشک</p></div>
<div class="m2"><p>خس پوش بحر رحمت ازین تخته پاره هاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نگذاشت گریه در نظرم آرزوی خام</p></div>
<div class="m2"><p>دامان صبح، پاک ز اشک ستاره هاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب ز درد و داغ ندارد شکایتی</p></div>
<div class="m2"><p>باغ و بهار سوخته جانان شراره هاست</p></div></div>