---
title: >-
    غزل شمارهٔ ۲۲۵
---
# غزل شمارهٔ ۲۲۵

<div class="b" id="bn1"><div class="m1"><p>کرد سودا آسمان سیر این دل دیوانه را</p></div>
<div class="m2"><p>سوختن شد باعث نشو و نما این دانه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محو شد در حسن آن کان ملاحت، دیده ها</p></div>
<div class="m2"><p>از زمین شور، بیرون شد نباشد دانه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق سازد حسن عالمسوز را در خون دلیر</p></div>
<div class="m2"><p>ذوالفقار شمع باشد بال و پر پروانه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می شود در ساغر مخمور، می آب حیات</p></div>
<div class="m2"><p>عاشقان دانند قدر جلوه مستانه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست پروا سیل بی زنهار را از کوچه بند</p></div>
<div class="m2"><p>می گشاید زور می آخر در میخانه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در حریم کعبه خودبین سجده بت می کند</p></div>
<div class="m2"><p>قبله رو گرداندن است از خویشتن این خانه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از سفر با خود رهاوردی که آرد میهمان</p></div>
<div class="m2"><p>بهتر از ترک فضولی نیست، صاحبخانه را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بس که دیدم کجروی از راست طبعان جهان</p></div>
<div class="m2"><p>گردش گردون شمارم گردش پیمانه را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گنج را زین پیش در ویرانه می کردم نهان</p></div>
<div class="m2"><p>این زمان در گنج پنهان می کنم ویرانه را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خلق دریا را نسازد گوهر شهوار تنگ</p></div>
<div class="m2"><p>نیست پروایی ز سنگ کودکان دیوانه را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مصرف بیهوشدارو نیست مغز غافلان</p></div>
<div class="m2"><p>پیش خواب آلودگان کوته کن این افسانه را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا مگر ذکر مرا کیفیتی پیدا شود</p></div>
<div class="m2"><p>از گل پیمانه سازم سبحه صد دانه را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یافت مژگان من از نور سحرخیزی فروغ</p></div>
<div class="m2"><p>زلف شب سرپنجه خورشید کرد این شانه را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>می گرفتم پیش ازین از دست ساقی می به ناز</p></div>
<div class="m2"><p>این زمان از دور می بوسم لب پیمانه را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در ترازوی قیامت نیست صائب سنگ کم</p></div>
<div class="m2"><p>عشق در یک پله دارد کعبه و بتخانه را</p></div></div>