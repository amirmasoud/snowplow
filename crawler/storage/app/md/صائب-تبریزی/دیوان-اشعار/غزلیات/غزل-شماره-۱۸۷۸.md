---
title: >-
    غزل شمارهٔ ۱۸۷۸
---
# غزل شمارهٔ ۱۸۷۸

<div class="b" id="bn1"><div class="m1"><p>تازه است دایم از سیهی داغ عندلیب</p></div>
<div class="m2"><p>در گلشنی که زاغ و زغن بی نهایت است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشمار سهل رخنه گفتار خویش را</p></div>
<div class="m2"><p>کاین رخنه در خرابی تن بی نهایت است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست ز کار رفته ز برگ است بیشتر</p></div>
<div class="m2"><p>در کشوری که سیب ذقن بی نهایت است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در غربت است چشم حسودان به زیر خاک</p></div>
<div class="m2"><p>این چاه در زمین وطن بی نهایت است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از مستمع گشوده شود چشمه سخن</p></div>
<div class="m2"><p>هر جا سخن کش است، سخن بی نهایت است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دندان به دل فشار که بر خوان روزگار</p></div>
<div class="m2"><p>این لقمه های دست و دهن بی نهایت است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جای دو مغز در ته یک پوست بیش نیست</p></div>
<div class="m2"><p>در تنگنای چرخ دو تن بی نهایت است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب سخن پذیر درین روزگار نیست</p></div>
<div class="m2"><p>ورنه مرا به سینه سخن بی نهایت است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زان غنچه لب شکایت من بی نهایت است</p></div>
<div class="m2"><p>تنگ است وقت، ورنه سخن بی نهایت است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در سینه گشاده من درد و داغ عشق</p></div>
<div class="m2"><p>چون نافه در زمین ختن بی نهایت است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پرهیز در زمان خط از یار مشکل است</p></div>
<div class="m2"><p>در نوبهار، توبه شکن بی نهایت است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ماه تمام می کند ایجاد هاله را</p></div>
<div class="m2"><p>تا شمع روشن است لگن بی نهایت است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون میوه در تو تا رگ خامی به جای هست</p></div>
<div class="m2"><p>گردن مکش، که دار و رسن بی نهایت است</p></div></div>