---
title: >-
    غزل شمارهٔ ۴۶۴۶
---
# غزل شمارهٔ ۴۶۴۶

<div class="b" id="bn1"><div class="m1"><p>خط سبز از دعای صبح خیزان است گیراتر</p></div>
<div class="m2"><p>لب میگون زخون بیگناهان است گیراتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز روی نو خط آن خوش پسر چون چشم بردارم؟</p></div>
<div class="m2"><p>کزاو هرحلقه ای ازچشم فتان است گیراتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر چه دانه راکمتر بود ازدام گیرایی</p></div>
<div class="m2"><p>ز زلف عنبر افشان خال جانان است گیراتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درآن گلشن که من دارم به خاطر فکر آزادی</p></div>
<div class="m2"><p>گل بی خارش از خارمغیلان است گیراتر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی چون چشم از آن چشم خمارآلود بردارد؟</p></div>
<div class="m2"><p>که از قلاب ،آن برگشته مژگان است گیراتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گزیدم راستی تاایمن از زخم زبان گردم</p></div>
<div class="m2"><p>ندانستم که آتش در نیستان است گیراتر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دنیا بیش می چسبند پیران درکهنسالی</p></div>
<div class="m2"><p>که خار خشک در تسخیر دامان است گیراتر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به عنوانی مبدل شد به خشکی چرب نرمیها</p></div>
<div class="m2"><p>که شیر از استخوان درکام طفلان است گیراتر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سروکارمن افتاده است با شیرین لبی صائب</p></div>
<div class="m2"><p>که حرف تلخ او از شکرستان است گیراتر</p></div></div>