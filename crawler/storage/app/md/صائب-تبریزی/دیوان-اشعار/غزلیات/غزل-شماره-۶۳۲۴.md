---
title: >-
    غزل شمارهٔ ۶۳۲۴
---
# غزل شمارهٔ ۶۳۲۴

<div class="b" id="bn1"><div class="m1"><p>خوش است فصل بهاران شراب نوشیدن</p></div>
<div class="m2"><p>به روی سبزه و گل همچو آب غلطیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان بهشت شد از نوبهار، باده بیار</p></div>
<div class="m2"><p>که در بهشت حلال است باده نوشیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کنون که شیشه می مالک الرقاب شده است</p></div>
<div class="m2"><p>ز عقل نیست سر از خط جام پیچیدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو نعمت است که بالاترین نعمتهاست</p></div>
<div class="m2"><p>شراب خوردن و در پای یار غلطیدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیاله از کف ساقی به ناز می گیرم</p></div>
<div class="m2"><p>درین بهار که دارد دماغ گل چیدن؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به غیر عشق که هر روز سخت تر گردید</p></div>
<div class="m2"><p>کدام کار که آسان نشد به ورزیدن؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لباس شهرت شمع است جامه فانوس</p></div>
<div class="m2"><p>به راز عشق محال است پرده پوشیدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به اشک و آه اگر دسترس بود صائب</p></div>
<div class="m2"><p>خوش است دامن شب را به دست پیچیدن</p></div></div>