---
title: >-
    غزل شمارهٔ ۶۰۹۹
---
# غزل شمارهٔ ۶۰۹۹

<div class="b" id="bn1"><div class="m1"><p>چند گردد قسمت افسردگان گفتار من؟</p></div>
<div class="m2"><p>تا به کی تلقین خون مرده باشد کار من؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاکیان از سیر و دور من کجا واقف شوند؟</p></div>
<div class="m2"><p>آسمان جایی که باشد نقطه پرگار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می زند موج حلاوت بوستان از ناله ام</p></div>
<div class="m2"><p>اشک شبنم گریه تلخی است از گلزار من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرم جولانی ندارد همچو من این خاکدان</p></div>
<div class="m2"><p>داغها دارد زمین بر سینه از رفتار من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بال اقبال هما را در سعادت گستری</p></div>
<div class="m2"><p>می شمارد فرد باطل سایه دیوار من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون رگ کان نیست ممکن از گهر مفلس شود</p></div>
<div class="m2"><p>هر رگ ابری که برخیزد ز دریا بار من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون فلک، سیر مه و اختر دلم را وا نکرد</p></div>
<div class="m2"><p>وا نشد زین ناخن و دندان گره از کار من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچو قارون در ضمیر خاک پنهان گشته است</p></div>
<div class="m2"><p>در ته گرد کسادی گوهر شهوار من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیش من کفرست از یاد خدا غافل شدن</p></div>
<div class="m2"><p>از رگ خواب است زنار دل بیدار من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست بی حاصلتری از من که پیر می فروش</p></div>
<div class="m2"><p>برنمی گیرد به جامی جبه و دستار من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر زبان و دل مرا جز گفتگوی عشق نیست</p></div>
<div class="m2"><p>می جهد چون سنگ و آهن آتش از گفتار من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پنجه مرجان شود چون دست دریا رعشه دار</p></div>
<div class="m2"><p>چون برآید ز آستین مژگان گوهربار من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از تب گرم است صائب شمع بر بالین مرا</p></div>
<div class="m2"><p>از سرشک تلخ باشد شربت بیمار من</p></div></div>