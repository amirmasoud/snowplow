---
title: >-
    غزل شمارهٔ ۱۰۷
---
# غزل شمارهٔ ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>خلق خوش چون صلح می سازد گوارا جنگ را</p></div>
<div class="m2"><p>می نماید چرب نرمی مومیایی سنگ را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر فقیران مرگ آسان تر بود از اغنیا</p></div>
<div class="m2"><p>راحت افزون است در کندن، قبای تنگ را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خورد از بس زخم های منکر از نادیدنی</p></div>
<div class="m2"><p>مرهم زنگار کرد آیینه من زنگ را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گریه را در پرده دل آب و تاب دیگرست</p></div>
<div class="m2"><p>حسن دیگر هست در مینا می گلرنگ را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتگوی ناصحان بر دل گرانی می کند</p></div>
<div class="m2"><p>ور نه گیرد از هوا دیوانه من سنگ را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از نواهای مخالف می کشند آزار خلق</p></div>
<div class="m2"><p>گوشمالی نیست حاجت ساز سیر آهنگ را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ظاهرآرایان ز چشم شور ایمن نیستند</p></div>
<div class="m2"><p>نیست از خورشید پروایی گل بی رنگ را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سحر را تأثیر نبود در عصای موسوی</p></div>
<div class="m2"><p>راستی در هم نوردد حیله و نیرنگ را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مفت شیطانند صائب کوته اندیشان که سگ</p></div>
<div class="m2"><p>صید خود سازد به آسانی شکار لنگ را</p></div></div>