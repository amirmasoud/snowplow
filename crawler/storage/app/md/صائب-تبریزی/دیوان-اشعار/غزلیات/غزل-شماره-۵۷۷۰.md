---
title: >-
    غزل شمارهٔ ۵۷۷۰
---
# غزل شمارهٔ ۵۷۷۰

<div class="b" id="bn1"><div class="m1"><p>ز میوه گر چه درین بوستان سبکباریم</p></div>
<div class="m2"><p>همان چو سرو به آزادگی گرفتاریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمین مرده شد از نوبهار زنده و ما</p></div>
<div class="m2"><p>به خواب بیخبری همچو نقش دیواریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار پرده دل ما ز شب سیاهترست</p></div>
<div class="m2"><p>به چشم ظاهر اگر چون ستاره بیداریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مکن چو ذره ز وجد و سماع ما را منع</p></div>
<div class="m2"><p>که ما به بال و پر آفتاب سیاریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به چشم اگر چه به نقش و نگار مشغولیم</p></div>
<div class="m2"><p>دلی ز خانه آیینه پاکتر داریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهان ز قیمت ما مفلس است و بی بصران</p></div>
<div class="m2"><p>گمان برند که ما مفلس خریداریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر چه طوطی ما سبز کرده سخن است</p></div>
<div class="m2"><p>گران به خاطر آیینه همچو زنگاریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو ابر بر رخ ما تیغ می کشند از برق</p></div>
<div class="m2"><p>به جرم این که درین بوستان گهرباریم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز آب گوهر ما تر شود گلوی جهان</p></div>
<div class="m2"><p>لبی اگر چه ز شمشیر خشک تر داریم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همان ز سنگدلی در شکست ما کوشند</p></div>
<div class="m2"><p>چو آب آینه هر چند صاف و همواریم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر چه شهپر پرواز ماست لاله و گل</p></div>
<div class="m2"><p>همان چو قطره شبنم به بوستان باریم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به قاف عزلت ازان رفته ایم چون عنقا</p></div>
<div class="m2"><p>که ما شکار پریزاد در نظر داریم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه نعمتی است که زاغ و زغن نمی دانند</p></div>
<div class="m2"><p>که ما به کنج قفس در میان گلزاریم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کمند همت ما چین به خود نمی گیرد</p></div>
<div class="m2"><p>به هر شکار محال است سر فرود آریم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>توقع کرم از سفله داشتن کفرست</p></div>
<div class="m2"><p>سپهر هر چه به ما می کند سزاواریم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ازان ترانه ما هوش می برد صائب</p></div>
<div class="m2"><p>که پیرو سخن مولوی و عطاریم</p></div></div>