---
title: >-
    غزل شمارهٔ ۴۱۹۰
---
# غزل شمارهٔ ۴۱۹۰

<div class="b" id="bn1"><div class="m1"><p>از نسبت عذار تو گل ناز می‌کند</p></div>
<div class="m2"><p>سنبل به بال زلف تو پرواز می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بس که مرغ من ز قضا طبل خورده است</p></div>
<div class="m2"><p>گل را خیال چنگل شهباز می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در بوستان چو برگ خزان دیده بی‌رخت</p></div>
<div class="m2"><p>رنگ شکسته است که پرواز می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حسن تو بی‌نیاز بود از نیازمند</p></div>
<div class="m2"><p>هر عضوی از تو بر دگری ناز می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در انتهای خوبی و انجام دلبری</p></div>
<div class="m2"><p>حسنت هنوز جلوه آغاز می‌کند</p></div></div>