---
title: >-
    غزل شمارهٔ ۵۱۳۵
---
# غزل شمارهٔ ۵۱۳۵

<div class="b" id="bn1"><div class="m1"><p>به خط ازان رخ چون برگ لاله ام قانع</p></div>
<div class="m2"><p>ز صاف باده به درد پیاله ام قانع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوشم به یک نگه دور از سیه چشمان</p></div>
<div class="m2"><p>به بوی مشک ز ناف غزاله ام قانع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خط مرا نظر از روی ساده بیشتر ست</p></div>
<div class="m2"><p>ز حسن ماه جبینان به هاله ام قانع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وبال گردن مینا نمی شود دستم</p></div>
<div class="m2"><p>ز می به گردش چشم پیاله ام قانع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چه ماه تمامم، ز هفت خوان سپهر</p></div>
<div class="m2"><p>به خوردن دل خود از نواله ام قانع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درین دبستان آن طفل بیسوادم من</p></div>
<div class="m2"><p>که با شمار ورق از رساله ام قانع</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درین ریاض من آن عندلیب دلگیرم</p></div>
<div class="m2"><p>که از بهار به فریاد و ناله ام قانع</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز برگ عیش به لخت جگر خوشم صائب</p></div>
<div class="m2"><p>به خون ز نعمت الوان چو لاله ام قانع</p></div></div>