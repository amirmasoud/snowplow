---
title: >-
    غزل شمارهٔ ۵۵۴۴
---
# غزل شمارهٔ ۵۵۴۴

<div class="b" id="bn1"><div class="m1"><p>غبار خط یارم توتیا در آستین دارم</p></div>
<div class="m2"><p>به دامن نور بینایی جلا در آستین دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیند این بسته چشمان لایق تشریف پیراهن</p></div>
<div class="m2"><p>و گر نه بوی یوسف چون صبا در آستین دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به ظاهر در نظرها چون قلم گر خشک می آیم</p></div>
<div class="m2"><p>چو افتد کار بر سر گریه ها در آستین دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا بی همدمی مهر لب و بند زبان گشته</p></div>
<div class="m2"><p>وگرنه همچو نی فریادها در آستین دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به اوراق پر و بالم ز غفلت سرسری مگذر</p></div>
<div class="m2"><p>که من از سایه دولت چون هما در آستین دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مدار از من دریغ ای ابر رحمت گوهر خود را</p></div>
<div class="m2"><p>که من چون تاک صد دست دعا در آستین دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزاران چشم در دنبال دارد هرپر کاهی</p></div>
<div class="m2"><p>و گرنه جذبه ها چون کهربا در آستین دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز بس چون غنچه گل زین جهان تنگ دلگیرم</p></div>
<div class="m2"><p>مرا هر کس که چیند خونبها در آستین دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به خون گل مزن دست ای چمن پیرا به دامانم</p></div>
<div class="m2"><p>که جوی خون از آن گلگون قبا در آستین دارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مکن در راه من چاه حسد، ای خصم کوته بین</p></div>
<div class="m2"><p>که من از راستی چندین عصا در آستین دارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیفشان برگ از خود گر نوا زین باغ می خواهی</p></div>
<div class="m2"><p>که من چون نی ز بی برگی نوا در آستین دارم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حضور دل مرا چون غنچه در تنگی بود صائب</p></div>
<div class="m2"><p>وگرنه خنده های دلگشا در آستین دارم</p></div></div>