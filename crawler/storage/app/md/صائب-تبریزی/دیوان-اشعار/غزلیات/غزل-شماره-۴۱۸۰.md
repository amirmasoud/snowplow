---
title: >-
    غزل شمارهٔ ۴۱۸۰
---
# غزل شمارهٔ ۴۱۸۰

<div class="b" id="bn1"><div class="m1"><p>معشوق کی زاهل هوس یاد می کند</p></div>
<div class="m2"><p>شکر کجا ز مور ومگس یاد می کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغی که شد زکاهلی از دست دانه خوار</p></div>
<div class="m2"><p>در آشیان ز کنج قفس یاد می کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همت ز عاجزان طلبد ظلم وقت عزل</p></div>
<div class="m2"><p>چون شعله شد ضعیف زخس یاد می کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیچد به دست وپای چو زنجیر ناقه را</p></div>
<div class="m2"><p>از بازماندگان چو جرس یاد می کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاخ گلی که می کند از سایه سرکشی</p></div>
<div class="m2"><p>صائب کی از اسیر قفس یاد می کند</p></div></div>