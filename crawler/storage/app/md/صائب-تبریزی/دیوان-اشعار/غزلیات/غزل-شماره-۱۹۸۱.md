---
title: >-
    غزل شمارهٔ ۱۹۸۱
---
# غزل شمارهٔ ۱۹۸۱

<div class="b" id="bn1"><div class="m1"><p>امروز قدر نکته موزون نمانده است</p></div>
<div class="m2"><p>انصاف در قلمرو گردون نمانده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچ است صد رساله حکمت به چشم ما</p></div>
<div class="m2"><p>بهتر ز خم اثر ز فلاطون نمانده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک عمر می توان سخن از زلف یار گفت</p></div>
<div class="m2"><p>در بند آن مباش که مضمون نمانده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صائب پیاله گیر که تا کرده ای نگاه</p></div>
<div class="m2"><p>یک خشت از عمارت گردون نمانده است</p></div></div>