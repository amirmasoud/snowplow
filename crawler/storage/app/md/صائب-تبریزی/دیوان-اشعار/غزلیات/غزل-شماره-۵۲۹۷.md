---
title: >-
    غزل شمارهٔ ۵۲۹۷
---
# غزل شمارهٔ ۵۲۹۷

<div class="b" id="bn1"><div class="m1"><p>تا نظر از عارض گل‌فام او پوشیده‌ام</p></div>
<div class="m2"><p>خار در چشمم اگر روی فراغت دیده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در به هم پیچیدن زلف درازش عاجزم</p></div>
<div class="m2"><p>من که طومار دو عالم را به هم پیچیده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سال‌ها در پردهٔ دل خون خود را خورده‌ام</p></div>
<div class="m2"><p>تا درین گلزار چون گل یک دهن خندیده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من که شمع محفل قُربم درین وحشت‌سرا</p></div>
<div class="m2"><p>کافرم گر پیش پای خویشتن را دیده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دهان آتش سوزان به جرات می‌روم</p></div>
<div class="m2"><p>جامهٔ فتحی ز نقش بوریا پوشیده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باد می‌سنجم کنون و شُکر طالع می‌کنم</p></div>
<div class="m2"><p>در ترازویی که گوهربارها سنجیده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌توان چون آب خواندن از بیاض چشم من</p></div>
<div class="m2"><p>نامهٔ او را ز بس بر چشم تر مالیده‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کوه در دامن نگنجد در فضای لامکان</p></div>
<div class="m2"><p>زیر گردون حیرتی دارم که چون گُنجیده‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جبههٔ من غوطه در گرد کدورت خورده است</p></div>
<div class="m2"><p>غیر پندارد که صندل بر جبین مالیده‌ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در بیابان طلب در اولین گامم هنوز</p></div>
<div class="m2"><p>من که چون خورشید بر گرد جهان گردیده‌ام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کی پریشان می‌کند خواب اجل صائب مرا</p></div>
<div class="m2"><p>من که در بیداری این خواب پریشان دیده‌ام</p></div></div>