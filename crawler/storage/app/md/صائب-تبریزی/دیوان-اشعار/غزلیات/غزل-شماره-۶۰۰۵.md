---
title: >-
    غزل شمارهٔ ۶۰۰۵
---
# غزل شمارهٔ ۶۰۰۵

<div class="b" id="bn1"><div class="m1"><p>نیست معشوقی همین زلف چلیپا داشتن</p></div>
<div class="m2"><p>دردسر بسیار دارد پاس دلها داشتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسن عالمسوز یوسف چون برانداز نقاب</p></div>
<div class="m2"><p>نیست ممکن پاس عصمت از زلیخا داشتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون تو از ما شیشه جانان می کنی پهلو تهی</p></div>
<div class="m2"><p>چیست حاصل از دل سنگ چو خارا داشتن؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا توان گردآوری کرد آبروی خویش را</p></div>
<div class="m2"><p>بهر گوهر دست نتوان پیش دریا داشتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جنگ دارد صحبت سوداییان با خلق تنگ</p></div>
<div class="m2"><p>جبهه واکرده ای باید چو صحرا داشتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از لب بیهوده گویان امن نتوان زیستن</p></div>
<div class="m2"><p>سوزنی با خویش باید همچو عیسی داشتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا تو نتوانی به همت داد سامان کار خلق</p></div>
<div class="m2"><p>از مروت نیست دست از کار دنیا داشتن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر چه دارد جنگ صائب خانه داری با جنون</p></div>
<div class="m2"><p>می توانم خانه زنجیر بر پا داشتن</p></div></div>