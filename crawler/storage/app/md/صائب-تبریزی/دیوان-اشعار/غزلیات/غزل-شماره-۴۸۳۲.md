---
title: >-
    غزل شمارهٔ ۴۸۳۲
---
# غزل شمارهٔ ۴۸۳۲

<div class="b" id="bn1"><div class="m1"><p>پامنه بیرون زحد خود کمال این است و بس</p></div>
<div class="m2"><p>پیش اهل دید ملک بی زوال این است و بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درد خودبینی بود صد پرده از کوری بتر</p></div>
<div class="m2"><p>اختر ارباب بینش را وبال این است وبس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون دل خوردن پشیمانی ندارد در قفا</p></div>
<div class="m2"><p>گر شرابی هست درعالم حلال این است و بس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم پوشیدن جهان را زیر بال آوردن است</p></div>
<div class="m2"><p>شاهباز معرفت را شاهبال این است و بس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باطن خود را مزین کن به اخلاق جمیل</p></div>
<div class="m2"><p>کانچه می ماند به حسن لایزال این است و بس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوش سنگین، سنگ دندان سبک مغزان بود</p></div>
<div class="m2"><p>هرزه گویان جهان را گوشمال این است وبس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از گناه خود اگر شرمنده ای دیگر مکن</p></div>
<div class="m2"><p>شاهد خجلت، دلیل انفال این است و بس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خویش رانزدیک می دانی، ازان دوری ز حق</p></div>
<div class="m2"><p>دورشو ز اندیشه باطل،وصال این است و بس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عشرت ما در رکاب معنی نازک بود</p></div>
<div class="m2"><p>عید مانازک خیالان را هلال این است وبس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا مگر بر چون خودی در گفتگو غالب شوند</p></div>
<div class="m2"><p>مطلب ارباب علم از قیل و قال این است و بس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا به خودداری گمان علم و دانش، ناقصی</p></div>
<div class="m2"><p>چون به نقص خود شدی قایل، کمال این است و بس</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دست تحسین برسر دوش قلم صائب بکش</p></div>
<div class="m2"><p>منتهای فکر ارباب کمال این است و بس</p></div></div>