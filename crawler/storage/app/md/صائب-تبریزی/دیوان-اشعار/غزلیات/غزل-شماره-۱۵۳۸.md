---
title: >-
    غزل شمارهٔ ۱۵۳۸
---
# غزل شمارهٔ ۱۵۳۸

<div class="b" id="bn1"><div class="m1"><p>دل من تیره ز بسیاری گفتار شده است</p></div>
<div class="m2"><p>زین پریشان نفس آیینه من تار شده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون سیه روی نباشم، که ز بیمغزی ها</p></div>
<div class="m2"><p>مد عمرم چو قلم صرف به گفتار شده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو رهزن به دلش دیدن منزل بارست</p></div>
<div class="m2"><p>هر که را درد طلب قافله سالار شده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هست آگاه ز محرومی من از دیدار</p></div>
<div class="m2"><p>طفل شوخی که تهیدست ز گلزار شده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می گدازد چو مه چارده از دیده شور</p></div>
<div class="m2"><p>ساغر هر که درین میکده سرشار شده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست از دوزخم اندیشه که از شرم گناه</p></div>
<div class="m2"><p>هر سر مو به تنم ابر گهربار شده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون سپندست سویدا به دلم بی آرام</p></div>
<div class="m2"><p>خال تا گوشه نشین دهن یار شده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تن به تسلیم و رضا ده که ازین خوش نفسان</p></div>
<div class="m2"><p>خار در پیرهن من گل بی خار شده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب از سنگ ملامت گله ای نیست مرا</p></div>
<div class="m2"><p>کبک من مست ازین دامن کهسار شده است</p></div></div>