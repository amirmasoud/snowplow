---
title: >-
    غزل شمارهٔ ۵۶۲۶
---
# غزل شمارهٔ ۵۶۲۶

<div class="b" id="bn1"><div class="m1"><p>قطره بی سرو پایم دل دریا دارم</p></div>
<div class="m2"><p>ذره خاکم و پیشانی صحرا دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست از سیل گرانسنگ حوادث خطرم</p></div>
<div class="m2"><p>خانه در کوچه گمنامی عنقا دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو شبنم چه به مجموعه گل دل بندم؟</p></div>
<div class="m2"><p>من که در دیده خورشید فلک جا دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جگر سنگ به نومیدی من می سوزد</p></div>
<div class="m2"><p>شیشه آبله ام، راه به خارا دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من تنک حوصله و دختر رزشیشه دل است</p></div>
<div class="m2"><p>چه عجب گر هوس توبه ز صهبا دارم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به یک آغوش چه گل چینم از آن نخل امید؟</p></div>
<div class="m2"><p>همچو گل یک بغل آغوش تمنا دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>موم از چرب زبانی نکند صید مرا</p></div>
<div class="m2"><p>شعله ام شعله، سرعالم بالا دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گریه تاب نشست از رخ من گرد خمار</p></div>
<div class="m2"><p>چشم بر خوشه انگور ثریا دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نقش امید محال است که صورت بندد</p></div>
<div class="m2"><p>چند آیینه بر صورت عنقا دارم؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روزگاری است ز چشم گهرافشان صائب</p></div>
<div class="m2"><p>همچو گرداب وطن در دل دریا دارم</p></div></div>