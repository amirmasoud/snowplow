---
title: >-
    غزل شمارهٔ ۶۰۷۳
---
# غزل شمارهٔ ۶۰۷۳

<div class="b" id="bn1"><div class="m1"><p>صبح شد ساقی بیا فکر من افتاده کن</p></div>
<div class="m2"><p>از می چون آفتاب این سنگ را بیجاده کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آب و رنگی ده غبار آلودگان زهد را</p></div>
<div class="m2"><p>باده در قندیل و گل در دامن سجاده کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که باشد می تواند نقش را از دل زدود</p></div>
<div class="m2"><p>از قبول نقش لوح خویشتن را ساده کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دامن سروی به دست آور درین بستانسرا</p></div>
<div class="m2"><p>نقد جان را صرف راه مردم آزاده کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیچ مرهم به ز خون گرم نبود زخم را</p></div>
<div class="m2"><p>رخنه دل را رفوکاری به درد باده کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در زمین ساده دهقان می فشاند تخم را</p></div>
<div class="m2"><p>از خس و خاشاک بی حاصل زمین را ساده کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقل سختی دیدگان شمشیر صیقل داده ای است</p></div>
<div class="m2"><p>مشورت زنهار با مردان کار افتاده کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاکساری پیشه خود ساز چون آب روان</p></div>
<div class="m2"><p>سرو را چون بندگان در پیش خود استاده کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هست اگر صائب ترا در سر هوای صید عام</p></div>
<div class="m2"><p>دانه از تسبیح ساز و دام از سجاده کن</p></div></div>