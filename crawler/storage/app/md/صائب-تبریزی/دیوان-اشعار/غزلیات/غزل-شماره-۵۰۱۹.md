---
title: >-
    غزل شمارهٔ ۵۰۱۹
---
# غزل شمارهٔ ۵۰۱۹

<div class="b" id="bn1"><div class="m1"><p>گرفت ازسرخم خشت پیر باده فروش</p></div>
<div class="m2"><p>چراغ عیش برون آمد از ته سرپوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز حرف تلخ ملامتگران نیندیشد</p></div>
<div class="m2"><p>به گوش هرکه رسیده است بانگ نوشانوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار خرقه آلوده را به قیمت می</p></div>
<div class="m2"><p>گرفت ازره انصاف پیر باده فروش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زجوش کم نشود آب بحر ،دل خوش دار</p></div>
<div class="m2"><p>مکن چودیگ تنک ظرف کوتهی درجوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به آفتاب رسانیده ایم پرتو را</p></div>
<div class="m2"><p>ز باد صبح نگردد چراغ ماخاموش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترا که بهره زنوش است نیش چون زنبور</p></div>
<div class="m2"><p>ازین چه سودکه داری هزار چشمه نوش؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آفتاب قیامت عرق نمی ریزد</p></div>
<div class="m2"><p>ز بار خلق ندزدد کسی که اینجا دوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مخور به هیچ دل زار و هرچه خواهی خور</p></div>
<div class="m2"><p>بپوش چشم خود از عیب و هرچه خواهی پوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز جوش لاف دل چشمه ها تهی گردید</p></div>
<div class="m2"><p>درین دو هفته که دریای مانشست ازجوش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فغان که تشنه لبان سخن نمی دانند</p></div>
<div class="m2"><p>که کار تیغ دو دم می کندلب خاموش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خموش بگذر ازین خاکدان چو سایه ابر</p></div>
<div class="m2"><p>مکن چو سیل ز پست و بلند راه خروش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شراب تلخ کجا چاره تو خواهد کرد؟</p></div>
<div class="m2"><p>تراکه ناله صائب نمی برد از هوش</p></div></div>