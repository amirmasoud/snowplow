---
title: >-
    غزل شمارهٔ ۵۳۷۳
---
# غزل شمارهٔ ۵۳۷۳

<div class="b" id="bn1"><div class="m1"><p>گر فروغ مهر تابان آب می آرد به چشم</p></div>
<div class="m2"><p>روی آتشناک او خوناب می آرد به چشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیقرار گل نپردازد به اوراق خزان</p></div>
<div class="m2"><p>مهر و مه را کی دل بیتاب می آرد به چشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گردش چشم تو گردون را کند زیروزبر</p></div>
<div class="m2"><p>کشتی مار ا کی این گرداب می آرد به چشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از شکر خند تو می ریزد نمک در چشم خواب</p></div>
<div class="m2"><p>گر چه صبح نوبهاران خواب می آرد به چشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش ابروی تو می بوسد زمین نه آسمان</p></div>
<div class="m2"><p>سجده ما را کی این محراب می آرد به چشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صرف گردد باده ممزوج در پیمانه ات</p></div>
<div class="m2"><p>بس که رخسارت قدح را آب می آرد به چشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اشتیاق بوسه لعل لب میگون تو</p></div>
<div class="m2"><p>جام خالی راشراب ناب می آرد به چشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگسل از ما ناتوانان کز برای مصلحت</p></div>
<div class="m2"><p>رشته را هم گوهر سیراب می آرد به چشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دور باش پاکی دامان ان آیینه رو</p></div>
<div class="m2"><p>اشک را لرزانتر از سیماب می آرد به چشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شعله بیمایه می پیچد به هر خار و خسی</p></div>
<div class="m2"><p>کی پر پروانه را مهتاب می آرد به چشم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بس که خوار وزار شد در روزگار حسن تو</p></div>
<div class="m2"><p>دیدن خورشید تابان آب می آرد به چشم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر که را صائب دل از ترک علایق گرم شد</p></div>
<div class="m2"><p>کی سمور و قاقم و سنجاب می آرد به چشم</p></div></div>