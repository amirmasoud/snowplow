---
title: >-
    غزل شمارهٔ ۵۸۵۴
---
# غزل شمارهٔ ۵۸۵۴

<div class="b" id="bn1"><div class="m1"><p>چندین کتاب در گرو باده کرده ایم</p></div>
<div class="m2"><p>تا از غبار، صفحه دل ساده کرده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز نیست دست سبو زیر بار ما</p></div>
<div class="m2"><p>دایم مدد به مردم افتاده کرده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از ترکتاز حادثه از جا نمی رویم</p></div>
<div class="m2"><p>بر گرد خود حصار، خم باده کرده ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آفتاب زرد خزان خنده می زنیم</p></div>
<div class="m2"><p>خود را چو سرو از ثمر آزاده کرده ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دشمن ز سنگ خاره اگر ساخته است دل</p></div>
<div class="m2"><p>ما هم ز شیشه جوشنی آماده کرده ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راز دو کون در نظر ما دو عینک است</p></div>
<div class="m2"><p>تا همچو آبگینه ورق ساده کرده ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب به طرف جبهه ما نیست چین منع</p></div>
<div class="m2"><p>ما قفل خانه از دل بگشاده کرده ایم</p></div></div>