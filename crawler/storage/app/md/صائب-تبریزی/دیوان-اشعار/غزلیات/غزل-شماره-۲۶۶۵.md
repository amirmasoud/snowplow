---
title: >-
    غزل شمارهٔ ۲۶۶۵
---
# غزل شمارهٔ ۲۶۶۵

<div class="b" id="bn1"><div class="m1"><p>چون صنوبر بادپیما گر سراپا دل شود</p></div>
<div class="m2"><p>میوه مقصود هیهات است ازو حاصل شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می گدازد غیرت همچشم صاحب درد را</p></div>
<div class="m2"><p>آب گردم چون به دریا قطره ای واصل شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دار نتواند سر منصور را در بر گرفت</p></div>
<div class="m2"><p>شاخ زندان می شود بر میوه چون کامل شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حسن عالمگیر لیلی چون براندازد نقاب</p></div>
<div class="m2"><p>دامن صحرا به مجنون دامن محمل شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درشمار نقطه سهوست در دیوان حشر</p></div>
<div class="m2"><p>خون گستاخی که داغ دامن قاتل شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که بردارد سر از نخوت ز پای اهل فقر</p></div>
<div class="m2"><p>خاک چون شد کاسه در یوزه سایل شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو چشم بد بلایی نیست حسن و عشق را</p></div>
<div class="m2"><p>در میان بلبل و گل شبنمی حایل شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوش عنانی لازم دیوانگی افتاده است</p></div>
<div class="m2"><p>بید مجنون از نسیمی هر طرف مایل شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پرده وحدت مقام نغمه منصور نیست</p></div>
<div class="m2"><p>بی محل چون مرغ بر آهنگ زد بسمل شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سیل دریا دیده هرگز برنمی گردد به جوی</p></div>
<div class="m2"><p>نیست ممکن هر که مجنون شد دگر عاقل شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می زند صائب به چوب دار حدش روزگار</p></div>
<div class="m2"><p>از می منصور هر کس مست و لایعقل شود</p></div></div>