---
title: >-
    غزل شمارهٔ ۴۴۷۴
---
# غزل شمارهٔ ۴۴۷۴

<div class="b" id="bn1"><div class="m1"><p>کی از ستاره برمن سنگ ستم نیامد</p></div>
<div class="m2"><p>از کهکشان به فرقم تیغ دو دم نیامد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا دانه امیدم خاکستری نگردید</p></div>
<div class="m2"><p>دامن کشان به کشتم ابر کرم نیامد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گویا به خواب رفته است بخت سیه که امروز</p></div>
<div class="m2"><p>حرفی رقم نمودم مو بر قلم نیامد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز اهل کرم زمانه پیوسته بود مفلس</p></div>
<div class="m2"><p>یا در زمانه مامرد کرم نیامد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در امتحان زلفش دل پای کرد قایم</p></div>
<div class="m2"><p>در پله فلاخن این سنگ کم نیامد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از شوق آن بر ودوش روزی بغل گشودم</p></div>
<div class="m2"><p>آغوش من چو محراب دیگر بهم نیامد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب چه چشم داری از فصلهای دیگر</p></div>
<div class="m2"><p>این قسم نوبهاری برخاک نم نیامد</p></div></div>