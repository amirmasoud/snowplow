---
title: >-
    غزل شمارهٔ ۲۸۰۹
---
# غزل شمارهٔ ۲۸۰۹

<div class="b" id="bn1"><div class="m1"><p>خط از بیباکی آن حسن عالمگیر می پیچد</p></div>
<div class="m2"><p>که جوهر بر خود از خونریزی شمشیر می پیچد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حنون را هست در غافل حریفی دست گیرایی</p></div>
<div class="m2"><p>که مجنون با کمال ضعف گوش شیر می پیچد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میسر نیست دل را از غبار خط برون رفتن</p></div>
<div class="m2"><p>که پای سیل را این خاک دامنگیر می پیچد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گزیر از دوزخ سوزان نباشد نفس کجرو را</p></div>
<div class="m2"><p>به آتش راست بتوان ساختن چون تیر می پیچد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کند عزت دنیاست پیچ و تاب خواریها</p></div>
<div class="m2"><p>عبث در کنج زندان یوسف از زنجیر می پیچد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که در صید دل من می کند چین زلف مشکین را؟</p></div>
<div class="m2"><p>که در هر گام دست و پای این نخجیر می پیچد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشد خط غمزه بیباک را مانع زخونریزی</p></div>
<div class="m2"><p>زجوهر کی زبان جرأت شمشیر می پیچد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زبیباکی حنا بر پای خواب آلود می بندد</p></div>
<div class="m2"><p>گرانجانی که بر آب و گل تعمیر می پیچد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نخواهد دید فردا روی آتش را گنهکاری</p></div>
<div class="m2"><p>که بی آتش چو مو از خجلت تقصیر می پیچد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به آب حضر صائب گرد راه از خویش می شوید</p></div>
<div class="m2"><p>ز روی صدق هر رهرو که بر شبگیر می پیچد</p></div></div>