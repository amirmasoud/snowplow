---
title: >-
    غزل شمارهٔ ۲۴۵۴
---
# غزل شمارهٔ ۲۴۵۴

<div class="b" id="bn1"><div class="m1"><p>حسرت از منقار خون‌آلود بلبل می‌چکد</p></div>
<div class="m2"><p>پاکدامانی چون شبنم از رخ گل می‌چکد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آب و رنگ گلستان حسن افزون می‌شود</p></div>
<div class="m2"><p>هر قدر خون بیش از تیغ تغافل می‌چکد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر نصیب خار می‌گردد گناه قسمت است</p></div>
<div class="m2"><p>اشک شبنم در هوای دامن گل می‌چکد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نسبت آن طره شاداب با سنبل خطاست</p></div>
<div class="m2"><p>آب کی در پیچ و تاب از زلف سنبل می‌چکد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حیرت سرشار، ثابت می‌کند سیاره را</p></div>
<div class="m2"><p>چون عرق از روی ساقی بی‌تأمل می‌چکد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنچه از گیرایی آن زلف و کاکل دیده‌ام</p></div>
<div class="m2"><p>خون دل کی بر زمین زان زلف و کاکل می‌چکد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زیر طاق آسمان‌ها جای خواب امن نیست</p></div>
<div class="m2"><p>بیم سیل نوبهار از سایه پل می‌چکد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وسمه بر ابروی تلخ آن نگار تندخو</p></div>
<div class="m2"><p>زهر خونخواری است کز تیغ تغافل می‌چکد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زود خواهد دست گلچین را گرفتن در نگار</p></div>
<div class="m2"><p>لاله‌های خون که از منقار بلبل می‌چکد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از عرق هردم دهد شهری به طوفان چهره‌اش</p></div>
<div class="m2"><p>شبنمی گر وقت صبح از چهره گل می‌چکد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با تو کل تشنگان را گر بود بیعت درست</p></div>
<div class="m2"><p>آب خضر از پنجه خشک توکل می‌چکد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صائب از کلک سخن‌پرداز ما در آتش است</p></div>
<div class="m2"><p>خون گرمی کز سر منقار بلبل می‌چکد</p></div></div>