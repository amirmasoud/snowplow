---
title: >-
    غزل شمارهٔ ۶۴۷۵
---
# غزل شمارهٔ ۶۴۷۵

<div class="b" id="bn1"><div class="m1"><p>جوش غیرت می زند خون حنای پای تو</p></div>
<div class="m2"><p>تا که بوسیده است گستاخانه جای پای تو؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پنبه در گوش از صدای خنده گل می نهد</p></div>
<div class="m2"><p>گوش هر کس آشنا شد با صدای پای تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش نماید چون شراب لعل در جام بلور</p></div>
<div class="m2"><p>عاشقان را در نظر رنگ حنای پای تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر چه باشد شمع کافوری به خوش ساقی علم</p></div>
<div class="m2"><p>پای طاوس است نسبت به صفای پای تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داغ دارد هاله مه را ز روشن دیدگی</p></div>
<div class="m2"><p>حلقه خلخال از نور و ضیای پای تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس که در یک جا ز شوخی ها نمی گیری قرار</p></div>
<div class="m2"><p>ساده باشد وادی امکان ز جای پای تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خجلت روی زمین از تنگدستی می کشد</p></div>
<div class="m2"><p>نقد جان را گر کند صائب فدای پای تو</p></div></div>