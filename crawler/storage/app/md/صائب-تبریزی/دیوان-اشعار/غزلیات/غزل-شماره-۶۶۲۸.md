---
title: >-
    غزل شمارهٔ ۶۶۲۸
---
# غزل شمارهٔ ۶۶۲۸

<div class="b" id="bn1"><div class="m1"><p>هر چند هست مشرق دیدار آینه</p></div>
<div class="m2"><p>باشد نظر به سینه من تار آینه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوهر ده است خواب پریشان به دیده اش</p></div>
<div class="m2"><p>تا دیده روی نوخط دلدار آینه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا از عرق شده است گهربار روی یار</p></div>
<div class="m2"><p>لب باز کرده است صدف وار آینه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون آب زیر سبزه خوابیده شد نهان</p></div>
<div class="m2"><p>از خجلت تو در ته زنگار آینه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از نقش، ساده چون دل بی مدعا شده است</p></div>
<div class="m2"><p>در عهد او ز حیرت سرشار آینه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون روی شرمناک کند جلوه در نظر</p></div>
<div class="m2"><p>از بس ترست ازان گل رخسار آینه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شوقم به دلبر از دل روشن زیاده شد</p></div>
<div class="m2"><p>باشد سراب تشنه دیدار آینه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دربسته خانه ای است که قفلش ز جوهرست</p></div>
<div class="m2"><p>با چهره گشاده دلدار آینه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون نامه دریده ز شرم عذار او</p></div>
<div class="m2"><p>دارد تلاش رخنه دیوار آینه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر روی کار، بخیه اش از جوهر اوفتاد</p></div>
<div class="m2"><p>تا شد طرف به عارض دلدار آینه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دارد ز انفعال رخ تازه خط او</p></div>
<div class="m2"><p>در پیرهن ز جوهر خود خار آینه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا صفحه عذار ترا دیده ام، شده است</p></div>
<div class="m2"><p>چون فرد باطلم به نظرخوار آینه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از چشم شور، امن ز بخت سیه شدم</p></div>
<div class="m2"><p>آسوده می شود چو شود تار آینه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نتوان به فکر راز فلک یافتن که هست</p></div>
<div class="m2"><p>اندیشه مور و این در و دیوار آینه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صحرای ساده ای است که در وی گیاه نیست</p></div>
<div class="m2"><p>نسبت به روی نوخط دلدار آینه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شام گرفته ای است که دل می کند سیاه</p></div>
<div class="m2"><p>صائب نظر به عارض دلدار آینه</p></div></div>