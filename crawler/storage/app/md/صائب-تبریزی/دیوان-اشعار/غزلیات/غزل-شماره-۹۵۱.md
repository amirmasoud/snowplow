---
title: >-
    غزل شمارهٔ ۹۵۱
---
# غزل شمارهٔ ۹۵۱

<div class="b" id="bn1"><div class="m1"><p>آنچه می دانند ماتم تن پرستان سور ماست</p></div>
<div class="m2"><p>دار، نخل دیگران و رایت منصور ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون شاخ گل به جوش از بلبل پر شور ماست</p></div>
<div class="m2"><p>پایکوبان دار از زور می منصور ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما ز تلخی چون شراب تلخ لذت می بریم</p></div>
<div class="m2"><p>موج دریای حلاوت نشتر زنبور ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر چه اوج لامکان بسیار دور افتاده است</p></div>
<div class="m2"><p>منزل نقل مکان فکرهای دور ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آتش ما را به خاکستر نهفتن مشکل است</p></div>
<div class="m2"><p>داغ ها چشم پر آب از سینه پر نور ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دل صد پاره ما عقل فرد باطلی است</p></div>
<div class="m2"><p>عشق گردی از نمکدان سرپر شور ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با دل پرخون ز نعمت های الوان فارغیم</p></div>
<div class="m2"><p>عشرت روی زمین در غنچه مستور ماست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با عیان صلح از بیان چون شاخ نرگس کرده ایم</p></div>
<div class="m2"><p>کاسه دریوزه ما دیده مخمور ماست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کاسه لیس شهد این حنظل جبینان نیستیم</p></div>
<div class="m2"><p>بر شکرخند سلیمان چشم تنگ مور ماست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کعبه از آبادی بتخانه ویران مانده است</p></div>
<div class="m2"><p>دل به خاک ره برابر از تن معمور ماست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از گرانخوابی دل شبهاست روز عیش ما</p></div>
<div class="m2"><p>روز روشن از سیه کاری شب دیجور ماست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>داغ ما افسردگان را تازه سازد روی گرم</p></div>
<div class="m2"><p>سردی ابنای دنیا مرهم کافور ماست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هست فریادی ز داغ آتشین ما نمک</p></div>
<div class="m2"><p>سوده الماس زخمی از دل ناسور ماست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نامرادی عاجزان را می شود خاک مراد</p></div>
<div class="m2"><p>آه سرد از دل کشیدن رایت منصور ماست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زین نمک کز شورش عالم به زخم ما رسید</p></div>
<div class="m2"><p>خنده صبح قیامت مرهم کافور ماست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>موسی ما صائب از سیر و سفر آسوده است</p></div>
<div class="m2"><p>کز دل سنگین خود آماده کوه طور ماست</p></div></div>