---
title: >-
    غزل شمارهٔ ۴۱۴۷
---
# غزل شمارهٔ ۴۱۴۷

<div class="b" id="bn1"><div class="m1"><p>این آهوان که گردن دعوی کشیده‌اند</p></div>
<div class="m2"><p>خال بیاض گردن او را ندیده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنها که وصف میوه فردوس می‌کنند</p></div>
<div class="m2"><p>از نخل حسن سیب زنخدان نچیده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جمعی که در کمینگه صبح قیامتند</p></div>
<div class="m2"><p>آن سینه را ز چاک گریبان ندیده‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنان که نسبت تو به آب خضر کنند</p></div>
<div class="m2"><p>از لعل روح‌بخش تو حرفی شنیده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این کورباطنان که ز حسن تو غافلند</p></div>
<div class="m2"><p>خورشید را به دیده خفاش دیده‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا لعل آبدار ترا نقش بسته‌اند</p></div>
<div class="m2"><p>آب عقیق و خون یمن را مکیده‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مد رسایی از قلم صنع برده‌اند</p></div>
<div class="m2"><p>تا قامت بلند ترا آفریده‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از شرم نرگس تو غزالان شوخ‌چشم</p></div>
<div class="m2"><p>خود را به زیر خیمه لیلی کشیده‌اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از چشم آهوان حرم حرف می‌زنند</p></div>
<div class="m2"><p>این غافلان نگاه ترا دور دیده‌اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواب فراغت از سر ایام رفته است</p></div>
<div class="m2"><p>تا چشم نیم‌خواب ترا آفریده‌اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا قامت بلند تو در جلوه آمده است</p></div>
<div class="m2"><p>مرغان قدس از سر طوبی پریده‌اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از خجلت رخ تو که خوندار لاله است</p></div>
<div class="m2"><p>گل‌ها به زیر شهپر مرغان خزیده‌اند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رخسار توست لاله بی‌داغ این چمن</p></div>
<div class="m2"><p>این لاله‌های باغ همه داغ‌دیده‌اند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در روزگار چهره شبنم فریب تو</p></div>
<div class="m2"><p>گل‌های باغ روی طراوت ندیده‌اند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>امروز در قلمرو خواری کشان توست</p></div>
<div class="m2"><p>آن را که مصریان به عزیزی خریده‌اند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صائب به حسن طبع تو اقرار کرده‌اند</p></div>
<div class="m2"><p>جمعی که در نزاکت معنی رسیده‌اند</p></div></div>