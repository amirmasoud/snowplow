---
title: >-
    غزل شمارهٔ ۷۱۵
---
# غزل شمارهٔ ۷۱۵

<div class="b" id="bn1"><div class="m1"><p>در کوی عشق ره نبود جبرئیل را</p></div>
<div class="m2"><p>پی کرده است تیزی این ره دلیل را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخت سیه گلیم ندارد غم گزند</p></div>
<div class="m2"><p>حاجت به نیل نیست رخ رود نیل را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خورشید و مه مرا نتواند ز راه برد</p></div>
<div class="m2"><p>هر شوخ دیده ای نفریبد خلیل را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل می دهد به نیم تپش عرض حال خود</p></div>
<div class="m2"><p>حاجت به نامه بر نبود جبرئیل را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در بزم اهل دید، نگه ترجمان بس است</p></div>
<div class="m2"><p>گل می زنیم روزنه قال و قیل را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر زور خود مناز که یک مشت بال و پر</p></div>
<div class="m2"><p>درهم شکست شوکت اصحاب فیل را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حیرانی جمال تو گردم که کرده است</p></div>
<div class="m2"><p>از حسن سیر چشم، خدای جمیل را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گویند بازگشت بخیلان بود به خاک</p></div>
<div class="m2"><p>حاشا که هیچ خاک پذیرد بخیل را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر جا حدیث اهل سخن در میان فتد</p></div>
<div class="m2"><p>صائب بخوان تو این غزل بی بدیل را</p></div></div>