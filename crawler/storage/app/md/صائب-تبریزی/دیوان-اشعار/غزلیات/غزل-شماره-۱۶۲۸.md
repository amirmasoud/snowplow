---
title: >-
    غزل شمارهٔ ۱۶۲۸
---
# غزل شمارهٔ ۱۶۲۸

<div class="b" id="bn1"><div class="m1"><p>دل ازان زلف چلیپا نتوانست گذشت</p></div>
<div class="m2"><p>طفل از دام تماشا نتوانست گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوز ما را نتوان کرد به مجنون نسبت</p></div>
<div class="m2"><p>هیچ مرغی ز سرما نتوانست گذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دامن از خار علایق نتوان آسان چید</p></div>
<div class="m2"><p>سیل از دامن صحرا نتوانست گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر چه از فاختگان یافت پر و بال عروج</p></div>
<div class="m2"><p>سرو ازان قامت رعنا نتوانست گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاقبت سرد به جان کندن بسیار گذاشت</p></div>
<div class="m2"><p>آن که گرم از سر دنیا نتوانست گذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دامن دولت جاوید به دستی افتاد</p></div>
<div class="m2"><p>که ز دریوزه دلها نتوانست گذشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیده از روی نکویان که تواند برداشت؟</p></div>
<div class="m2"><p>که ز خورشید، مسیحا نتوانست گذشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب از خوردن می گر چه دلش گشت سیاه</p></div>
<div class="m2"><p>لاله از باده حمرا نتوانست گذشت</p></div></div>