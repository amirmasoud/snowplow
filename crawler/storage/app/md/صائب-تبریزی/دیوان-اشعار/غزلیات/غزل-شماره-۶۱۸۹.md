---
title: >-
    غزل شمارهٔ ۶۱۸۹
---
# غزل شمارهٔ ۶۱۸۹

<div class="b" id="bn1"><div class="m1"><p>چه آسان است با بی برگی احرام سفر بستن</p></div>
<div class="m2"><p>که هم مرکب بود هم توشه، دامن بر کمر بستن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حباب ما سبکروحان گرانجانی نمی داند</p></div>
<div class="m2"><p>یکی باشد نظر وا کردن ما با نظر بستن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمی سازد پریشان شغل دنیا وقت عارف را</p></div>
<div class="m2"><p>صدف را شور دریا نیست مانع از گهر بستن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فشاندن بر ثمر دامان خود چون سرو، ازین غافل</p></div>
<div class="m2"><p>که می باید به برگ از بی بری دل چون ثمر بستن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو گل با روی خندان صرف کن گر خرده ای داری</p></div>
<div class="m2"><p>که دل را تنگ سازد در گره چون غنچه زر بستن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا از چین ابرو نیست پروایی که عاشق را</p></div>
<div class="m2"><p>در امیدواری باز می گردد ز در بستن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز غفلت پشت بر دیوار دارد برگ کاه ما</p></div>
<div class="m2"><p>در آن وادی که باشد کوه در کار کمر بستن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به خود بسته است قانع راه احسان کریمان را</p></div>
<div class="m2"><p>ز دریا نیست ممکن آب در جوی گهر بستن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میان سنگ و مینا دوستی صورت نمی بندد</p></div>
<div class="m2"><p>دل ما و ترا مشکل بود بر یکدگر بستن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر سیر مقامات است در خاطر ترا صائب</p></div>
<div class="m2"><p>در اثنای دمیدن همچو نی باید کمر بستن</p></div></div>