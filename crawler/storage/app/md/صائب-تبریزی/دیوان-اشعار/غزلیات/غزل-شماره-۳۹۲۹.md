---
title: >-
    غزل شمارهٔ ۳۹۲۹
---
# غزل شمارهٔ ۳۹۲۹

<div class="b" id="bn1"><div class="m1"><p>فغان چه با دل سنگین آن نگار کند</p></div>
<div class="m2"><p>خروش بحر به گوش صدف چه کار کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز قرب زلف دل تنگ من گشاده نشد</p></div>
<div class="m2"><p>چه عقده باز ز دل دست رعشه دارکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود ز وسمه دو ابروی آن بهشتی رو</p></div>
<div class="m2"><p>دوبرگ سبز که خون در دل بهار کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چوشانه شددل صدچاک من تمام انگشت</p></div>
<div class="m2"><p>نشد که حلقه آن زلف را شمار کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به خون صید چرا دامن خود آلاید</p></div>
<div class="m2"><p>میسرست کسی را که دل شکار کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز باده توبه نمودن دلیل بیخردی است</p></div>
<div class="m2"><p>چگونه عقل پشیمانی اختیار کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه نسبت است به خورشید شان حسن ترا</p></div>
<div class="m2"><p>فلک پیاده شود تا ترا سوارکند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در آن چمن که ندارندباربی برگان</p></div>
<div class="m2"><p>نهال ما به چه امید برگ وبار کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فسان دشنه یکدیگرندسنگدلان</p></div>
<div class="m2"><p>کسی چه شکوه به ابنای روزگار کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کدام ذکر به این ذکر می رسد صائب</p></div>
<div class="m2"><p>که آدمی نفس خویش را شمار کند</p></div></div>