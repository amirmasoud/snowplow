---
title: >-
    غزل شمارهٔ ۲۱۹۰
---
# غزل شمارهٔ ۲۱۹۰

<div class="b" id="bn1"><div class="m1"><p>در موج پریشانی ما فاصله ای نیست</p></div>
<div class="m2"><p>امروز به جمعیت ما سلسله ای نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فریاد که اسباب گرفتاری ما را</p></div>
<div class="m2"><p>چون حلقه زنجیر ز هم فاصله ای نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی دیده بینا چه گل از خار توان چید؟</p></div>
<div class="m2"><p>رحم است به پایی که در او آبله ای نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>موقوف به وقت است سماع دل عارف</p></div>
<div class="m2"><p>هر روز در اجزای زمین زلزله ای نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از ظرف حریفان نتوان سر به در آورد</p></div>
<div class="m2"><p>در بزم شرابی که تنک حوصله ای نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بوی گل و باد سحری بر سر راهند</p></div>
<div class="m2"><p>گر می روی از خود، به ازین قافله ای نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب ز سر زلف سخن دست ندارد</p></div>
<div class="m2"><p>هر چند به جز گوشه ابرو صله ای نیست</p></div></div>