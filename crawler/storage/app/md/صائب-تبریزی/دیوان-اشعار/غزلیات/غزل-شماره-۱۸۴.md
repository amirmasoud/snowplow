---
title: >-
    غزل شمارهٔ ۱۸۴
---
# غزل شمارهٔ ۱۸۴

<div class="b" id="bn1"><div class="m1"><p>نیست بر خاطر غباری از پریشانی مرا</p></div>
<div class="m2"><p>جامه فتح است چون شمشیر عریانی مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه از آتش زبانی شمع این نه محفلم</p></div>
<div class="m2"><p>نیست رزقی جز سرانگشت پشیمانی مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون گهر گرد یتیمی سرنوشت من شده است</p></div>
<div class="m2"><p>نیست ممکن شستن این صندل ز پیشانی مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فارغ از آمد شد نقش بد و نیکم، که ساخت</p></div>
<div class="m2"><p>خانه دربسته، چون آیینه، حیرانی مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زندگی گردید از قد دو تا پا در رکاب</p></div>
<div class="m2"><p>برد از عالم برون این اسب چوگانی مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا سرافرازم به داغ بندگی کرده است عشق</p></div>
<div class="m2"><p>هست در زیر نگین ملک سلیمانی مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در دبستان تأمل کرده ام روشن سواد</p></div>
<div class="m2"><p>ابجد اطفال باشد خط پیشانی مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست احسان بنده کردن مردم آزاد را</p></div>
<div class="m2"><p>دانه چینی خوشترست از دانه افشانی مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون صدف بر هم نمی پیچد مرا زخم زبان</p></div>
<div class="m2"><p>زیر تیغ تیز باشد گوهرافشانی مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نعل وارون است آه و گریه یعقوبیم</p></div>
<div class="m2"><p>ورنه یوسف در دل تنگ است زندانی مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پنجه خونین تهمت جلوه گل می کند</p></div>
<div class="m2"><p>در گریبان حیا از پاکدامانی مرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از خرابی های ظاهر شکوه صائب چون کنم؟</p></div>
<div class="m2"><p>مغرب گنج گهر گرداند ویرانی مرا</p></div></div>