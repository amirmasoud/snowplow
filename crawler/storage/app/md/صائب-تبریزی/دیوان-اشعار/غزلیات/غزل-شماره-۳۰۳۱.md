---
title: >-
    غزل شمارهٔ ۳۰۳۱
---
# غزل شمارهٔ ۳۰۳۱

<div class="b" id="bn1"><div class="m1"><p>ز اشک دیده بیدرد زنگ از دل کجا خیزد؟</p></div>
<div class="m2"><p>اثر در دل ندارد گریه ای کز توتیا خیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازان بر آسمان ساید سرش از سرفرازیها</p></div>
<div class="m2"><p>که پیش پای هر خار و خسی آتش زجاخیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مسلم کی گذارد ناله مظلوم ظالم را؟</p></div>
<div class="m2"><p>که پیش از دانه فریاد از نهاد آسیا خیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گزیری نیست از غفلت دل ارباب دولت را</p></div>
<div class="m2"><p>که این ابر سیه از سایه بال هما خیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا جان تازه شد زان خط پشت لب، چنین باشد</p></div>
<div class="m2"><p>رگ ابری که از آب روان بخش بقا خیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حواس جمع من چون دود از روزن رود بیرون</p></div>
<div class="m2"><p>چو از بیرون در آواز پای آشنا خیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من بی دست و پاآیم چسان بیرون از ان محفل</p></div>
<div class="m2"><p>که نتواند سپند از حیرت رویش زجا خیزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پر و بال سمندر را زآتش نیست پروایی</p></div>
<div class="m2"><p>به می زان روی آتشناک کی رنگ حیا خیزد؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لباس فقر بر تن پروران صائب نمی چسبد</p></div>
<div class="m2"><p>که از پهلوی فربه زود نقش بوریا خیزد</p></div></div>