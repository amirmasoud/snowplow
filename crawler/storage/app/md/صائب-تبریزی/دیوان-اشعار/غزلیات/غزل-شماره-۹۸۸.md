---
title: >-
    غزل شمارهٔ ۹۸۸
---
# غزل شمارهٔ ۹۸۸

<div class="b" id="bn1"><div class="m1"><p>حسن را با بی قراران گیر و دار دیگرست</p></div>
<div class="m2"><p>مهر را هر ذره ای آیینه دار دیگرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مستی چشم غزالان نشکند ما را خمار</p></div>
<div class="m2"><p>چشم لیلی دیده ما را خمار دیگرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به که برگردد به مصر از راه، بوی پیرهن</p></div>
<div class="m2"><p>دیده یعقوب ما را انتظار دیگرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر چه از سنگ ملامت کوه از جا می رود</p></div>
<div class="m2"><p>عاشقان را لنگر صبر و قرار دیگرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش بت هر چند باشد کافر اصلی عزیز</p></div>
<div class="m2"><p>دین به غارت دادگان را اعتبار دیگرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیل معذورست اگر منزل نمی داند که چیست</p></div>
<div class="m2"><p>بحر را هر موج آغوش و کنار دیگرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لشکر بیگانه را در کشور ما راه نیست</p></div>
<div class="m2"><p>ملک ما زیر و زبر از شهسوار دیگرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر چه در زندان عزلت می توان آسوده زیست</p></div>
<div class="m2"><p>با زمین هموار گردیدن حصار دیگرست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر رگ سنگی پی آزار ما دیوانگان</p></div>
<div class="m2"><p>در کف اطفال، نبض بی قرار دیگرست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از لب سیراب او امیدوار بوسه را</p></div>
<div class="m2"><p>هر جواب خشک، تیغ آبدار دیگرست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تنگ چشمان دام در راه هما می گسترند</p></div>
<div class="m2"><p>دام ما را چشم بر راه شکار دیگرست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پیش آن کس کز دل گرم است در آتش مدام</p></div>
<div class="m2"><p>هر دم سردی نسیم نوبهار دیگرست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زخم از مرهم گواراتر بود بر عارفان</p></div>
<div class="m2"><p>رخنه در زندان به از نقش و نگار دیگرست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیست صادق دشت پیمای طلب را تشنگی</p></div>
<div class="m2"><p>ورنه هر موج سرابی جویبار دیگرست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر چه صائب نازک افتاده است آن موی میان</p></div>
<div class="m2"><p>فکر ما نازک خیالان را عیار دیگرست</p></div></div>