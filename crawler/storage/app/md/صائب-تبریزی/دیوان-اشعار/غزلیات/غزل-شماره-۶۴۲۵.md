---
title: >-
    غزل شمارهٔ ۶۴۲۵
---
# غزل شمارهٔ ۶۴۲۵

<div class="b" id="bn1"><div class="m1"><p>در عشق اگر صادقی از قرب حذر کن</p></div>
<div class="m2"><p>چون آینه از دور قناعت به نظر کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان چهره کز او جای عرق می چکد آتش</p></div>
<div class="m2"><p>در کار من سوخته دل نیم شرر کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان چاه زنخدان که پر از آب حیات است</p></div>
<div class="m2"><p>یک قطره عنایت به من تشنه جگر کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل باز نمی آید ازان زلف دلاویز</p></div>
<div class="m2"><p>زان یار سفرکرده قناعت به خبر کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منمای به کوته نظران چهره خود را</p></div>
<div class="m2"><p>از آه من ای آینه رخسار حذر کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با تیره دلی چهره مطلب نتوان دید</p></div>
<div class="m2"><p>این آینه را صیقلی از آه سحر کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تسلیم بود جوشن داودی آفات</p></div>
<div class="m2"><p>در رهگذر تیر قضا سینه سپر کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لنگر نتوان کرد درین عالم پرشور</p></div>
<div class="m2"><p>چون موج ازین بحر پرآشوب گذر کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون سرو اگر از جمله آزاده روانی</p></div>
<div class="m2"><p>با بار دل خویش قناعت ز ثمر کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر چند ز ما هیچکسان کار نیاید</p></div>
<div class="m2"><p>کاری که به همت رود از پیش، خبر کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بی رشته محال است که گلدسته شود جمع</p></div>
<div class="m2"><p>شیرازه اوراق دل از آه سحر کن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شاید که به آن گوهر نایاب بری راه</p></div>
<div class="m2"><p>یک چند ز سر پای درین بحر خطر کن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صائب چو صدف گر لب دریوزه گشایی</p></div>
<div class="m2"><p>حاجت طلب از مردم پاکیزه گهر کن</p></div></div>