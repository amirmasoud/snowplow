---
title: >-
    غزل شمارهٔ ۵۸۴۳
---
# غزل شمارهٔ ۵۸۴۳

<div class="b" id="bn1"><div class="m1"><p>یک عمر پشت دست به دندان گرفته ایم</p></div>
<div class="m2"><p>تا بوسه ای ازان لب خندان گرفته ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گردیده است در نظر ما جهان سیاه</p></div>
<div class="m2"><p>تا جرعه ای ز چشمه حیوان گرفته ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>افتاده ایم در ته پا سالها چو مور</p></div>
<div class="m2"><p>تا جا به روی دست سلیمان گرفته ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بوته گداز چو مه آب گشته ایم</p></div>
<div class="m2"><p>کز خوان آفتاب لب نان گرفته ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را ز چوب منع مترسان که همچو صبح</p></div>
<div class="m2"><p>ما تیغ آفتاب به دندان گرفته ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آورده است معنی بیگانه رو به ما</p></div>
<div class="m2"><p>تا ترک آشنایی یاران گرفته ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>انگشت حیرتی است که داریم در دهن</p></div>
<div class="m2"><p>کامی که ما ازان لب خندان گرفته ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون دست ما ز چاک گریبان شود جدا؟</p></div>
<div class="m2"><p>گستاخ دامن مه کنعان گرفته ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نگرفته است خضر ز سرچشمه حیات</p></div>
<div class="m2"><p>کامی که ما ز چاه زنخدان گرفته ایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون صبح از عزیمت صادق به یک نفس</p></div>
<div class="m2"><p>روی زمین به چهره خندان گرفته ایم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دلگیر نیستیم ز بخت سیاه خویش</p></div>
<div class="m2"><p>فیض سحر ز شام غریبان گرفته ایم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جز پیچ و تاب نیست، که عمرش دراز باد</p></div>
<div class="m2"><p>کامی که ما ز سلسله مویان گرفته ایم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر روی بی طمع نشود بسته هیچ در</p></div>
<div class="m2"><p>ما چوب منع از کف دربان گرفته ایم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رو تافتن ز جوربتان نیست کار ما</p></div>
<div class="m2"><p>چون صبح تیغ مهر به دندان گرفته ایم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بی چشم زخم، گوهر شهوار عبرت است</p></div>
<div class="m2"><p>صائب تمتعی که زدوران گرفته ایم</p></div></div>