---
title: >-
    غزل شمارهٔ ۳۱۹۷
---
# غزل شمارهٔ ۳۱۹۷

<div class="b" id="bn1"><div class="m1"><p>درین صحرا که یارب از پی نخجیر می‌آید؟</p></div>
<div class="m2"><p>که آهو بی‌محابا در پناه شیر می‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل بیدار می‌باید وصال زلف جانان را</p></div>
<div class="m2"><p>ره خوابیده را طی کردن از شبگیر می‌آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شده است از سوده الماس چون گنجینه گوهر</p></div>
<div class="m2"><p>کجا داغ مرا مرهم به چشم سیر می‌آید؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بس در سینه من می‌خورد بر یکدگر پیکان</p></div>
<div class="m2"><p>به گوش هم‌نشینان نالهٔ زنجیر می‌آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان از زلف لیلی مشکبو شد دامن صحرا</p></div>
<div class="m2"><p>که بوی ناف آهو از دهان شیر می‌آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز درد و داغ دل برداشتن آسان نمی‌باشد</p></div>
<div class="m2"><p>عجب نبود اگر جان بر لب من دیر می‌آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فلک‌ها را ز طعن کج‌روی خونین‌جگر دارم</p></div>
<div class="m2"><p>که حرف راست بر دل کارگر چون تیر می‌آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به زور مرگ از هم نگسلد پیوند روحانی</p></div>
<div class="m2"><p>هنوز از بید مجنون نالهٔ زنجیر می‌آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مگر بازوی همت دستگیر کوهکن گردد</p></div>
<div class="m2"><p>وگرنه از دهان تیشه بوی شیر می‌آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر گرد تعلق راهرو از دامن افشاند</p></div>
<div class="m2"><p>چه کار از دست خشک خار دامنگیر می‌آید؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز دلگیری به خون خود به نوعی تشنه‌ام صائب</p></div>
<div class="m2"><p>که آبم در دهان از دیدن شمشیر می‌آید</p></div></div>