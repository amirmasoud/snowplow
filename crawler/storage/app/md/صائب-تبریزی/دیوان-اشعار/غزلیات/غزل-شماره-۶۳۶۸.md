---
title: >-
    غزل شمارهٔ ۶۳۶۸
---
# غزل شمارهٔ ۶۳۶۸

<div class="b" id="bn1"><div class="m1"><p>پیش قضای حق دم چون و چرا مزن</p></div>
<div class="m2"><p>در بحر بی کنار عبث دست و پا مزن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا در دل تو داعیه اعتراض هست</p></div>
<div class="m2"><p>خاموش باش و دم ز مقام رضا مزن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوته شود زبان ملامت ز احتیاط</p></div>
<div class="m2"><p>با دیده گشاده قدم بی عصا مزن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سهل است ناامید ز بیگانگان شدن</p></div>
<div class="m2"><p>با جان پر امید در آشنا مزن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آتش است نعل سفر رنگ و بوی را</p></div>
<div class="m2"><p>دامن گره به دامن این بی وفا مزن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خاک کن نهان قلم استخوان من</p></div>
<div class="m2"><p>آتش به دفتر پر و بال هما مزن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مجنون گرفت دامن محمل به دست صبر</p></div>
<div class="m2"><p>بیهوده قطره در طلب مدعا مزن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب کباب شد دل عالم ز ناله ات</p></div>
<div class="m2"><p>در پرده بیش ازین سخن آشنا مزن</p></div></div>