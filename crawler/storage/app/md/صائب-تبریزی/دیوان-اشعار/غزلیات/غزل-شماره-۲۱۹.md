---
title: >-
    غزل شمارهٔ ۲۱۹
---
# غزل شمارهٔ ۲۱۹

<div class="b" id="bn1"><div class="m1"><p>دید تا در آتش تعجیل، نعل لاله را</p></div>
<div class="m2"><p>می کند در هفته ای گل خنده یکساله را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست در سرگشتگی آرامش صاحبدلان</p></div>
<div class="m2"><p>نیست بی گردش وجودی شعله جواله را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشقان را قرب خوبان برنیارد از خمار</p></div>
<div class="m2"><p>قسمت از مه یک دهن خمیازه باشد هاله را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کاهلان را بیشتر باشد خطر از رهروان</p></div>
<div class="m2"><p>می زند از کاروان ها راهزن دنباله را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از ضعیفان دلخراشی، شاهد سنگین دلی است</p></div>
<div class="m2"><p>از پرستاران کن ای بیمار، پنهان ناله را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شیوه های بی شمار دستگاه حسن او</p></div>
<div class="m2"><p>می زند مهر خموشی بر دهن دلاله را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی جگر با سخت رویان چهره نتواند شدن</p></div>
<div class="m2"><p>لاله و گل چون سپرداری نماید ژاله را؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می شود از خال، حسن لاله رویان بیشتر</p></div>
<div class="m2"><p>داغ زینت می دهد دلهای خوش پرگاله را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برنیاید مهر خاموشی به حفظ راز عشق</p></div>
<div class="m2"><p>سد مومین نیست مانع آتش سیاله را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دوربین می گیرد از ایام، حیف خویش را</p></div>
<div class="m2"><p>می کند در هفته ای گل خنده یکساله را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می رسد در پرده رزق تشنگان بسته لب</p></div>
<div class="m2"><p>از تب گرم است سیرابی گل تبخاله را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فکر صائب گوش ها را می کند تنگ شکر</p></div>
<div class="m2"><p>این گلوسوزی نباشد شکر بنگاله را</p></div></div>