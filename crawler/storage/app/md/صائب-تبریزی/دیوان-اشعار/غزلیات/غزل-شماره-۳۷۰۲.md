---
title: >-
    غزل شمارهٔ ۳۷۰۲
---
# غزل شمارهٔ ۳۷۰۲

<div class="b" id="bn1"><div class="m1"><p>دل رمیده ملول از سفر نمی‌گردد</p></div>
<div class="m2"><p>فتاد هرکه به این راه برنمی‌گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شده است خشک چنان چشم من ز بی‌دردی</p></div>
<div class="m2"><p>که از نظاره خورشید تر نمی‌گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشو به سنگدلی غرّه ای کمان‌ابرو</p></div>
<div class="m2"><p>که تیر آه من از سنگ برنمی‌گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل از عقیق لب او چگونه بردارم؟</p></div>
<div class="m2"><p>که تشنه سیر ز آب گهر نمی‌گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمین ساده‌دلی‌هاست سخت دامنگیر</p></div>
<div class="m2"><p>ز آبگینه من نقش بر نمی‌گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمی‌شوند بزرگان ز پاس خود غافل</p></div>
<div class="m2"><p>که تیغ کوه جدا از کمر نمی‌گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سراب تشنه‌لبان را نمی‌کند سیراب</p></div>
<div class="m2"><p>که حرص جاه کم از سیم و زر نمی‌گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز زور آب شناور نمی‌شود عاجز</p></div>
<div class="m2"><p>ز باده ساقی ما بی‌خبر نمی‌گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به غیر خون جگر باده‌ای درین دوران</p></div>
<div class="m2"><p>نصیب صائب خونین‌جگر نمی‌گردد</p></div></div>