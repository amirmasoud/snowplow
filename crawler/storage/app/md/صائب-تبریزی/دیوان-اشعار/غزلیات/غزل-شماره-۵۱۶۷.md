---
title: >-
    غزل شمارهٔ ۵۱۶۷
---
# غزل شمارهٔ ۵۱۶۷

<div class="b" id="bn1"><div class="m1"><p>معنی ز لفظ جوهر خود را عیان کند</p></div>
<div class="m2"><p>زان چهره لطیف مکن مو به یک طرف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تاسر برآورد ز گریبان پیرهن</p></div>
<div class="m2"><p>هردم کند نسیم تکاپو به یک طرف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکسان به دیر و کعبه نظر کن که میل نیست</p></div>
<div class="m2"><p>شاهین عدل را زترازو به یک طرف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حیرت نگر که بی سرو سامان عشق را</p></div>
<div class="m2"><p>چوگان به یک طرف رود وگو به یک طرف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صائب مدار فیض خود راتشنگان دریغ</p></div>
<div class="m2"><p>این آب تا نرفته ازین جو به یک طرف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گلها تمام یک طرف آن رو به یک طرف</p></div>
<div class="m2"><p>چین و ختا به یک طرف آن مو به یک طرف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدمستی سپهر جفا جو به یک طرف</p></div>
<div class="m2"><p>مستانه جلوه های قد اوبه یک طرف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آخر نشانه ای چه کند با هزار تیر؟</p></div>
<div class="m2"><p>دل یک طرف هزار پریرو به یک طرف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از پیچ وتاب ،رشته عمرش شود تمام</p></div>
<div class="m2"><p>با هر که افتد آن خم گیسو به یک طرف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اکنون که زلف بر خط انصاف سرنهاد</p></div>
<div class="m2"><p>افتاده است خال لب اوبه یک طرف</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دروادیی که لیلی بیگانه خوی ماست</p></div>
<div class="m2"><p>مجنون به یک طرف رود آهو به یک طرف</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گردد عصای موسوی انگشت زینهار</p></div>
<div class="m2"><p>هرجا فتاد غمزه جادو به یک طرف</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با دوست هم لباسم و چون اشک و آه شمع</p></div>
<div class="m2"><p>من میروم به یک طرف و او به یک طرف</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عام است فیض عشق به ذرات کاینات</p></div>
<div class="m2"><p>حاشا که آفتاب کند روبه یک طرف</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بیرون فتاد مهره اش از ششدر جهات</p></div>
<div class="m2"><p>آن راکه برد جاذبه او به یک طرف</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>باشش جهت توجه آن بی جهت یکی است</p></div>
<div class="m2"><p>بیچاره رهروی که کند رو به یک طرف</p></div></div>