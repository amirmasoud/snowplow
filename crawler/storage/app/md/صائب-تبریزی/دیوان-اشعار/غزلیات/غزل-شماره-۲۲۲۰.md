---
title: >-
    غزل شمارهٔ ۲۲۲۰
---
# غزل شمارهٔ ۲۲۲۰

<div class="b" id="bn1"><div class="m1"><p>آب خضر و می شبانه یکی است</p></div>
<div class="m2"><p>مستی و عمر جاودانه یکی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر دل ماست چشم، خوبان را</p></div>
<div class="m2"><p>صد کماندار را نشانه یکی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش آن چشمهای خواب آلود</p></div>
<div class="m2"><p>ناله عاشق و فسانه یکی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در مقامی که غور باید کرد</p></div>
<div class="m2"><p>قطره و بحر بیکرانه یکی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کثرت خلق، عین توحیدست</p></div>
<div class="m2"><p>خوشه چندین هزار و دانه یکی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پله دین و کفر چون میزان</p></div>
<div class="m2"><p>دو نماید، ولی زبانه یکی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رهروانی که راست چون تیرند</p></div>
<div class="m2"><p>همه را مقصد و نشانه یکی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دو جهان سنگ راه سالک نیست</p></div>
<div class="m2"><p>نسبت تیر با دو خانه یکی است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر هزارست بلبل این باغ</p></div>
<div class="m2"><p>همه را نغمه و ترانه یکی است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نشود نور مهر پست و بلند</p></div>
<div class="m2"><p>پیش ما صدر و آستانه یکی است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عاشقی را که غیرتی دارد</p></div>
<div class="m2"><p>چین ابرو و تازیانه یکی است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خنده در چشم آب گرداند</p></div>
<div class="m2"><p>ماتم و سور این زمانه یکی است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پیش مرغ شکسته پر صائب</p></div>
<div class="m2"><p>قفس و باغ و آشیانه یکی است</p></div></div>