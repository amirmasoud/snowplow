---
title: >-
    غزل شمارهٔ ۱۵۲۳
---
# غزل شمارهٔ ۱۵۲۳

<div class="b" id="bn1"><div class="m1"><p>هر طرف روی نهی باده جان ریخته است</p></div>
<div class="m2"><p>این چه فیض است که در دیر مغان ریخته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کجا فاخته ای هست درین سبز چمن</p></div>
<div class="m2"><p>بال در جستن آن سرو روان ریخته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سهل مشمار عدو را که مکرر در رزم</p></div>
<div class="m2"><p>دهن تیغ من از آب روان ریخته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نخل شمع است خزان دیده و از یکرنگی</p></div>
<div class="m2"><p>بال پروانه چو اوراق خزان ریخته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در بیابان طلب راهبری حاجت نیست</p></div>
<div class="m2"><p>گوهر آبله چون ریگ روان ریخته است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غم خود خور تو که در کلبه ما بی برگان</p></div>
<div class="m2"><p>برگ عیش است که چون برگ خزان ریخته است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگرانم که چسان پای گذارم به زمین</p></div>
<div class="m2"><p>بس که هر سو دل و چشم نگران ریخته است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هیچ جا نیست که در خاک نباشد تیغی</p></div>
<div class="m2"><p>بس که بر روی زمین تیغ زبان ریخته است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون به دامن نکشم پای، که در دامن خاک</p></div>
<div class="m2"><p>هر جا پای نهی شیره جان ریخته است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا تو شیرازه اش از طول امل می سازی</p></div>
<div class="m2"><p>دفتر عمر چو اوراق خزان ریخته است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صفحه خاک سراسر شکرستان شده است</p></div>
<div class="m2"><p>کلک صائب شکر از بس ز بیان ریخته است</p></div></div>