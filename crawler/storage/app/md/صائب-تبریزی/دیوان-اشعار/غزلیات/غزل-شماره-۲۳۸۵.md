---
title: >-
    غزل شمارهٔ ۲۳۸۵
---
# غزل شمارهٔ ۲۳۸۵

<div class="b" id="bn1"><div class="m1"><p>عجز بر سر پنجه اقبال چون زور آورد</p></div>
<div class="m2"><p>از شکرخند سلیمان روزی مور آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حاصل روی زمین بردار از یک کف زمین</p></div>
<div class="m2"><p>هر سحرخیزی که بر دست دعا زور آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روز محشر چشمه کوثر به فریادش رسد</p></div>
<div class="m2"><p>هر که وقت صبح جامی پیش مخمور آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نیندازم به پای عشق سر از بخل نیست</p></div>
<div class="m2"><p>چون کسی جام سفالین پیش فغفور آورد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر به پیش افکنده چوگان رفت از میدان برون</p></div>
<div class="m2"><p>این سزای آن که بر افتادگان زور آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تنگ چشمان بر سر دنیا به هم دارند جنگ</p></div>
<div class="m2"><p>از دهان مور بیرون دانه را مور آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عالم آب از سبک مغزان خورد بر یکدگر</p></div>
<div class="m2"><p>بحر را باد مخالف بر سر شور آورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عارفان مستغنی اند از زهد خشک زاهدان</p></div>
<div class="m2"><p>کی عصا بینا برون از پنجه کور آورد؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کوهکن را برق آتشدستیم دارد کباب</p></div>
<div class="m2"><p>بیستون را تیشه ام در رقص چون طور آورد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیده یعقوب می باید قماش حسن را</p></div>
<div class="m2"><p>بوی پیراهن به هر چشمی کجا نور آورد؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روزگاری شد که از مشق سخن افتاده ایم</p></div>
<div class="m2"><p>کیست صائب فکر ما را بر سر شور آورد؟</p></div></div>