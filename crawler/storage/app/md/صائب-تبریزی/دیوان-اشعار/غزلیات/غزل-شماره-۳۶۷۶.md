---
title: >-
    غزل شمارهٔ ۳۶۷۶
---
# غزل شمارهٔ ۳۶۷۶

<div class="b" id="bn1"><div class="m1"><p>دل از سفر ز بد و نیک باخبر گردد</p></div>
<div class="m2"><p>به قدر آبله هر پای دیده ور گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترا ز گرمروان آن زمان حساب کنند</p></div>
<div class="m2"><p>که نقش پای تو گنجینه گهر گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز شرم حسن محابا نمی کند عاشق</p></div>
<div class="m2"><p>حجاب عشق مگر پرده نظر گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>توانگری ندهد سود تنگ چشمان را</p></div>
<div class="m2"><p>که حرص مور ز خرمن زیادتر گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر ز پای درآید نیفتد از پرگار</p></div>
<div class="m2"><p>به گرد نقطه دل هرکه بیشتر گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز روشنایی دل نفس گوشه گیر شده است</p></div>
<div class="m2"><p>که دزد در شب مهتاب بیجگر گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طمع ز عمر سبکرو مدار خودداری</p></div>
<div class="m2"><p>چگونه سیل ز دریا به کوه بر گردد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کجا رسد خبر دوستان به مشتاقی</p></div>
<div class="m2"><p>که از رسیدن مکتوب بیخبر گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بس است زهد مرا بویی از شراب کهن</p></div>
<div class="m2"><p>که خار خشک فروزان به یک شرر گردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کشیده دار عنان نظر ز چهره یار</p></div>
<div class="m2"><p>که این ورق به نسیم نگاه برگردد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنین به جلوه درآیند اگر بلندقدان</p></div>
<div class="m2"><p>فلک چو سبزه خوابیده پی سپر گردد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به روی تازه قناعت کن از ثمر صائب</p></div>
<div class="m2"><p>که سرو و بید محال است بارور گردد</p></div></div>