---
title: >-
    غزل شمارهٔ ۱۳۰۳
---
# غزل شمارهٔ ۱۳۰۳

<div class="b" id="bn1"><div class="m1"><p>آفت دولت به ابنای زمان معلوم نیست</p></div>
<div class="m2"><p>لقمه چون افتاد فربه استخوان معلوم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خدنگ عشق چون تیر جگر دوز قضا</p></div>
<div class="m2"><p>از لطافت هیچ جز گرد از نشان معلوم نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کجا آزادگی باشد، نباشد انقلاب</p></div>
<div class="m2"><p>در بساط سرو آثار خزان معلوم نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوسه می خواهد که راه آشنایی وا کند</p></div>
<div class="m2"><p>بر نفس هر چند راه آن دهان معلوم نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از شتاب عمر دارد بی خبر غفلت ترا</p></div>
<div class="m2"><p>از هجوم سبزه این آب روان معلوم نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا ز خود بیرون نیایی خویش را نتوان شناخت</p></div>
<div class="m2"><p>عیب تیر کج در آغوش کمان معلوم نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می شوی وقت رحیل از غفلت خود باخبر</p></div>
<div class="m2"><p>در حضر سنگینی خواب گران معلوم نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طفل داند دایه را حور و بهشت و جوی شیر</p></div>
<div class="m2"><p>زشتی زال جهان بر ناقصان معلوم نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیشتر پاس ادب دارند شرم آلودگان</p></div>
<div class="m2"><p>در گلستانی که آنجا باغبان معلوم نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در رگ کان تا بود یاقوت، خون مرده ای است</p></div>
<div class="m2"><p>در خموشی جوهر تیغ زبان معلوم نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مشکل است از جستجو آزادگان را یافتن</p></div>
<div class="m2"><p>از سبکباری پی این کاروان معلوم نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در غریبی می نماید فکر صائب خویش را</p></div>
<div class="m2"><p>نکهت گل تا بود در گلستان معلوم نیست</p></div></div>