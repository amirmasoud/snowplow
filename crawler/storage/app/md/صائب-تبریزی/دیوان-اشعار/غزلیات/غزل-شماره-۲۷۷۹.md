---
title: >-
    غزل شمارهٔ ۲۷۷۹
---
# غزل شمارهٔ ۲۷۷۹

<div class="b" id="bn1"><div class="m1"><p>چشم ما را پرده غفلت شد ابروی سفید</p></div>
<div class="m2"><p>باز ناورد از ختا این نافه را موی سفید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیگران را گر ز پیری صبح آگاهی دمید</p></div>
<div class="m2"><p>شد دل ما شیر مست غفلت از موی سفید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی شود طبع هوسناکان زپیری تنگدل؟</p></div>
<div class="m2"><p>ماه عید طفل طبعان است ابروی سفید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از جوانان نیست کم چون زنده دل افتاد پیر</p></div>
<div class="m2"><p>صبح می رو بد زدلها غم به گیسوی سفید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با سیه رویان بود عفو خدا را روی حرف</p></div>
<div class="m2"><p>قابل اقبال نبود نامه را روی سفید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تار و پود زندگانی را پریشان کردن است</p></div>
<div class="m2"><p>جمع کردن خنده را چون صبح با موی سفید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کاکل عنبرفشان بر پشت آن سیمین بدن</p></div>
<div class="m2"><p>هست چون خط سیه بر پشت آهوی سفید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که صائب روی گردان شد زاهل روزگار</p></div>
<div class="m2"><p>می برد از ظلمت آباد جهان روی سفید</p></div></div>