---
title: >-
    غزل شمارهٔ ۶۸۴۸
---
# غزل شمارهٔ ۶۸۴۸

<div class="b" id="bn1"><div class="m1"><p>ز خط سیه رخ چون لاله زار خود کردی</p></div>
<div class="m2"><p>ستم به روز من و روزگار خود کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همان ز ماه تمام تو نور می بارد</p></div>
<div class="m2"><p>اگر چه هاله خط را حصار خود کردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار دیده تر در قفا ز شبنم داشت</p></div>
<div class="m2"><p>گلی که از رخ خود در کنار خود کردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا که ساخته بودم به داغ نومیدی</p></div>
<div class="m2"><p>دگر برای چه امیدوار خود کردی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزار شکوه جانسوز داشتم در دل</p></div>
<div class="m2"><p>مرا به نیم نگه شرمسار خود کردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نکرد برق جهانسوز با خس و خاشاک</p></div>
<div class="m2"><p>ز گرمی آنچه تو با بی قرار خود کردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگشت حرمت دین سنگ راه شوخی تو</p></div>
<div class="m2"><p>اگر به کعبه رسیدی شکار خود کردی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز وعده ای که دلت را خبر نبود ازان</p></div>
<div class="m2"><p>چه خون که در دلم از انتظار خود کردی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مباد آفت پژمردگی بهار ترا</p></div>
<div class="m2"><p>چنین که تازه مرا از بهار خود کردی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چها کنی به دل آب کرده عاشق</p></div>
<div class="m2"><p>که آب آینه را بی قرار خود کردی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گهر ز گرد یتیمی گرانبها گردد</p></div>
<div class="m2"><p>کناره بهر چه از خاکسار خود کردی؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هنوز کوه به خدمت نبسته بود کمر</p></div>
<div class="m2"><p>که همچو لاله مرا داغدار خود کردی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو از کجا و تعلق به آب و گل صائب؟</p></div>
<div class="m2"><p>ستم به آینه بی غبار خود کردی</p></div></div>