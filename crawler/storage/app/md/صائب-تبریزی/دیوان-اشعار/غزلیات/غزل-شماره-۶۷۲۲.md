---
title: >-
    غزل شمارهٔ ۶۷۲۲
---
# غزل شمارهٔ ۶۷۲۲

<div class="b" id="bn1"><div class="m1"><p>حیرتی از چشم مست یار دارم دیدنی</p></div>
<div class="m2"><p>خوابها در دیده بیدار دارم دیدنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه چون مرکز زمین گیرم به چشم غافلان</p></div>
<div class="m2"><p>سیرها در خویش چون پرگار دارم دیدنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیستم ایمن ز چشم شور، ورنه من ز داغ</p></div>
<div class="m2"><p>لاله زاری در دل افگار دارم دیدنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوه غم بر خاطر آزاده من بار نیست</p></div>
<div class="m2"><p>مستیی چون کبک در کهسار دارم دیدنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چه مهر خامشی دارم به ظاهر بر دهن</p></div>
<div class="m2"><p>در گره چون غنچه صد گلزار دارم دیدنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل ز گرد خاکساری بر گرفتن مشکل است</p></div>
<div class="m2"><p>ورنه گنجی در ته دیوار دارم دیدنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آب مروارید آورده است چشم جوهری</p></div>
<div class="m2"><p>ورنه من لعل و گهر بسیار دارم دیدنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در خراش سینه ها بی دست و پا افتاده ام</p></div>
<div class="m2"><p>ورنه دستی در گشاد کار دارم دیدنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست در روی زمین جوهرشناسی، ورنه من</p></div>
<div class="m2"><p>تیغها پوشیده در زنگار دارم دیدنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیل چشم زخم من دارد جمال یوسفی</p></div>
<div class="m2"><p>در سیاهی یک جهان انوار دارم دیدنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرچه از بس بی وجودی درنمی آیم به چشم</p></div>
<div class="m2"><p>گوشه ها همچون دهان یار دارم دیدنی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ملک و مالی نیست صائب گرچه در عالم مرا</p></div>
<div class="m2"><p>باغی از رنگینی گفتار دارم دیدنی</p></div></div>