---
title: >-
    غزل شمارهٔ ۳۰۸۷
---
# غزل شمارهٔ ۳۰۸۷

<div class="b" id="bn1"><div class="m1"><p>به مقدار تمنا داغ در دل جلوه گر باشد</p></div>
<div class="m2"><p>به قدر خار و خس در آتش سوزان شرر باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زسیلاب حوادث عارف از جا در نمی آید</p></div>
<div class="m2"><p>کمند وحدت صاحبدلان موج خطر باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منه خشت اقامت بر زمین در عالم امکان</p></div>
<div class="m2"><p>که چون ریگ روان اجزای عالم در سفر باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حقارت پیشه کن گر اعتبار از عشق می خواهی</p></div>
<div class="m2"><p>که پیش پادشاهان مهر کوچک معتبر باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>محبت بیشتر دلهای شاهان را به دام آرد</p></div>
<div class="m2"><p>حباب و موج بحر عشق از تاج و کمر باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حواس جمع خواهی نازک اندامی به دست آور</p></div>
<div class="m2"><p>که این اوراق را شیرازه از موی کمر باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شود ریحان تر خواب پریشان، گرد بالینش</p></div>
<div class="m2"><p>ز اسباب جهان آن را که خشتی زیر سر باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به عقل ناقص خود داشتم امیدها صائب</p></div>
<div class="m2"><p>ندانستم که دام مرغ زیرک سخت تر باشد</p></div></div>