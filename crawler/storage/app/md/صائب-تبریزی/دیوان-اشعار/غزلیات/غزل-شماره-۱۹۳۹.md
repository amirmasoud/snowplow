---
title: >-
    غزل شمارهٔ ۱۹۳۹
---
# غزل شمارهٔ ۱۹۳۹

<div class="b" id="bn1"><div class="m1"><p>شاهنشهی است عشق که دل جلوه گاه اوست</p></div>
<div class="m2"><p>آهی که خیزد از دل ما گرد راه اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل را ز کام هر دو جهان سرد ساختن</p></div>
<div class="m2"><p>تأثیر اولین نفس صبحگاه اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از یک نگاه، زیر و زبر کردن جهان</p></div>
<div class="m2"><p>بازیچه ای ز گردش چشم سیاه اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون نور آفتاب، پریشان خرام نیست</p></div>
<div class="m2"><p>دلهای چاک، مشرق روی چو ماه اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گردون که صبح و شام زنده غوطه در شفق</p></div>
<div class="m2"><p>صید به خون تپیده ای از صیدگاه است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نتوان شکست لشکر دل را به ترکتاز</p></div>
<div class="m2"><p>این فتح در شکستن طرف کلاه اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر سینه ای که پاک شد از گرد آرزو</p></div>
<div class="m2"><p>میدان تیغ بازی برق نگاه اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فتح از سپاه عشق بود، گر چه وقت جنگ</p></div>
<div class="m2"><p>انگشت زینهار، لوای سپاه اوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عشق تو آهویی است که از چشمه سار دل</p></div>
<div class="m2"><p>هر تخم آرزو که برآید گیاه اوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از خسروی است فتح که هنگام دار و گیر</p></div>
<div class="m2"><p>دست دعای خلق لوای سپاه اوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب به غیر چهره زرین عشق نیست</p></div>
<div class="m2"><p>آن کهربا که کاهکشان برگ کاه اوست</p></div></div>