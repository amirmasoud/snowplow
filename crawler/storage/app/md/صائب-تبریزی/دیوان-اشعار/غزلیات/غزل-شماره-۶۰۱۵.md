---
title: >-
    غزل شمارهٔ ۶۰۱۵
---
# غزل شمارهٔ ۶۰۱۵

<div class="b" id="bn1"><div class="m1"><p>تندخویان می زنند آتش به جان خویشتن</p></div>
<div class="m2"><p>می خورد دل شمع دایم از زبان خویشتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راه حرف آشنایان سبزه بیگانه بست</p></div>
<div class="m2"><p>اینقدر غافل مباش از گلستان خویشتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست نرگس را چو برگ گل به شبنم احتیاج</p></div>
<div class="m2"><p>دولت بیدار باشد دیده بان خویشتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون گل رعنا فریب مهلت دوران مخور</p></div>
<div class="m2"><p>در بهاران بگذران فصل خزان خویشتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون صدف ناچار اگر باید لب خواهش گشاد</p></div>
<div class="m2"><p>پیش هر ناشسته رو مگشا دهان خویشتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرفرو نارد به چرخ پست فطرت، هر که ساخت</p></div>
<div class="m2"><p>از بلندی های همت آسمان خویشتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ریزه چین خوان من ذرات و من چون آفتاب</p></div>
<div class="m2"><p>از شفق در خون زنم هر روز نان خویشتن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از زبردستان کسی زه بر کمان من نبست</p></div>
<div class="m2"><p>حلقه بیرون بردم از میدان کمان خویشتن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بلبلان افسرده می گردند صائب، ورنه من</p></div>
<div class="m2"><p>تخته می کردم ز خاموشی دکان خویشتن</p></div></div>