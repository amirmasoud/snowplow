---
title: >-
    غزل شمارهٔ ۱۹۷۸
---
# غزل شمارهٔ ۱۹۷۸

<div class="b" id="bn1"><div class="m1"><p>خال تو ریشه در شکرستان دوانده است</p></div>
<div class="m2"><p>از خط سبز، شهپر طوطی رسانده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز خط دل سیه که مبیناد روز خوش</p></div>
<div class="m2"><p>بر شمع آفتاب که دامن فشانده است؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجنون من ز کندن جان در طریق عشق</p></div>
<div class="m2"><p>فرهاد را به کوه مکرر جهانده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا قامت بلند تو در جلوه آمده است</p></div>
<div class="m2"><p>از رعشه سرو فاختگان را پرانده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موج سراب می شمرد سلسبیل را</p></div>
<div class="m2"><p>هر کس ز خط سبز تو چشمی چرانده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با قامت تو سبزه خوابیده است سرو</p></div>
<div class="m2"><p>با چهره تو لاله چراغ نشانده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دستی است شاخ گل که به مستی نگار من</p></div>
<div class="m2"><p>صائب ز روی ناز به گلشن فشانده است</p></div></div>