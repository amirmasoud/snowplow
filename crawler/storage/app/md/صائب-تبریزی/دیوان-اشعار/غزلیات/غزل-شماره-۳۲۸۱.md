---
title: >-
    غزل شمارهٔ ۳۲۸۱
---
# غزل شمارهٔ ۳۲۸۱

<div class="b" id="bn1"><div class="m1"><p>سر ارباب جدل خرج زبان می‌گردد</p></div>
<div class="m2"><p>رگ گردن چو قوی گشت سنان می‌گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از شنیدن سبق نطق روان می‌گردد</p></div>
<div class="m2"><p>به سخن هرکه دهد گوش، زبان می‌گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌برد راستی از طبع، کج‌اندیش برون</p></div>
<div class="m2"><p>تیر در قبضه ناراست، کمان می‌گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیم و زر پرده بینایی حق‌جویان است</p></div>
<div class="m2"><p>راه پوشیده ازین ریگ روان می‌گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غفلت نفس یکی صد شود از موی سفید</p></div>
<div class="m2"><p>خواب سگ وقت سحرگاه گران می‌گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیده عاقبت اندیش نظر نگشاید</p></div>
<div class="m2"><p>به بهاری که مبدل به خزان می‌گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جذبه بحر نگردد ز غریبان غافل</p></div>
<div class="m2"><p>در گهر آب گهر قطره زنان می‌گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سنگ اطفال گران نیست به سودازدگان</p></div>
<div class="m2"><p>کبک در کوه و کمر خنده‌زنان می‌گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می‌توان یافت که برده است به مرکز راهی</p></div>
<div class="m2"><p>آن که پرگارصفت گرد جهان می‌گردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شکر این لطف نمایان چه توانم کردن؟</p></div>
<div class="m2"><p>که مثال تو در آیینه عیان می‌گردد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اژدها می‌شود از طول زمان آخر مار</p></div>
<div class="m2"><p>رگ گردن چو قوی گشت سنان می‌گردد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صائب آن کس که بود خواب گران در بارش</p></div>
<div class="m2"><p>در بیابان طلب سنگ نشان می‌گردد</p></div></div>