---
title: >-
    غزل شمارهٔ ۴۵۰۱
---
# غزل شمارهٔ ۴۵۰۱

<div class="b" id="bn1"><div class="m1"><p>عشق مقید به خط وخال نگردد</p></div>
<div class="m2"><p>رهزن مرغان قدس دانه نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوالهوس وعشق بی غرض چه خیال است</p></div>
<div class="m2"><p>گریه اطفال بی بهانه نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کار سپر می کند گشاده جبینی</p></div>
<div class="m2"><p>وای بر آن کس که شادمانه نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سلسله جنبان چه می کند دل عاشق</p></div>
<div class="m2"><p>جنبش گردون به تازیانه نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لازم فقرست تیره رویی دارین</p></div>
<div class="m2"><p>لیلی ما بی سیاه خانه نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق به رنگ هوس ز پرده برآید</p></div>
<div class="m2"><p>صائب اگر شرم در میانه نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مستی ما از می شبانه نباشد</p></div>
<div class="m2"><p>مطرب ما از برون خانه نباشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جوش شکایت کجاوخون شهیدان</p></div>
<div class="m2"><p>آتش یاقوت را زبانه نباشد</p></div></div>