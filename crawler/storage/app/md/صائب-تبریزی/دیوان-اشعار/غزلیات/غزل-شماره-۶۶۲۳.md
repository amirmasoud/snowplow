---
title: >-
    غزل شمارهٔ ۶۶۲۳
---
# غزل شمارهٔ ۶۶۲۳

<div class="b" id="bn1"><div class="m1"><p>در دور خط به حرف رسیدن چه فایده؟</p></div>
<div class="m2"><p>در وقت عزل شکوه شنیدن چه فایده؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خط نیست دشمنی که بتابد ز تیغ روی</p></div>
<div class="m2"><p>بر روی خویش تیغ کشیدن چه فایده؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر رشته نگاه چو از دست رفت، رفت</p></div>
<div class="m2"><p>دنبال صید جسته دویدن چه فایده؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اکنون که شعله زد ز جگر سوزش نهان</p></div>
<div class="m2"><p>چون شمع دست خویش گزیدن چه فایده؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ابر تنک نهان نکند آفتاب را</p></div>
<div class="m2"><p>بر داغ عشق پرده کشیدن چه فایده؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پست و بلند پیش نسیم خزان یکی است</p></div>
<div class="m2"><p>چون تاک بر درخت دویدن چه فایده؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گل می کند پیاله کشی از بهار رنگ</p></div>
<div class="m2"><p>پیمانه را نهفته کشیدن چه فایده؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون تیر می جهد ز کمان گفتگوی حق</p></div>
<div class="m2"><p>منصور را به دار کشیدن چه فایده؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تیغ زمانه را به جگر آب رحم نیست</p></div>
<div class="m2"><p>خون خوردن و به خاک تپیدن چه فایده؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صائب چو یار با دگران باده می کشد</p></div>
<div class="m2"><p>گردن ز انتظار کشیدن چه فایده؟</p></div></div>