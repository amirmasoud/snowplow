---
title: >-
    غزل شمارهٔ ۵۹۳۶
---
# غزل شمارهٔ ۵۹۳۶

<div class="b" id="bn1"><div class="m1"><p>ما فرق به تردستی حاتم نفروشیم</p></div>
<div class="m2"><p>این گوهر سیراب به شبنم نفروشیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشکل بود از حسن گلوسوز گذشتن</p></div>
<div class="m2"><p>ما تشنگی خویش به زمزم نفروشیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قانع نتوان شد به صباحت ز ملاحت</p></div>
<div class="m2"><p>ما زخم نمکسود به مرهم نفروشیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صحرای جنون نیست کم از ملک سلیمان</p></div>
<div class="m2"><p>ما حلقه زنجیر به خاتم نفروشیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما سوختگان دولت پاینده غم را</p></div>
<div class="m2"><p>چون صبح به خوشحالی یک دم نفروشیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یوسف به زر قلب فروشان دگرانند</p></div>
<div class="m2"><p>ما وقت خوش خود به دو عالم نفروشیم</p></div></div>