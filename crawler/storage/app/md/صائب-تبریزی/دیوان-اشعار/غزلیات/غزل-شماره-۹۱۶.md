---
title: >-
    غزل شمارهٔ ۹۱۶
---
# غزل شمارهٔ ۹۱۶

<div class="b" id="bn1"><div class="m1"><p>جای صدف بود ز گرانی زمین در آب</p></div>
<div class="m2"><p>باشد حباب از سبکی خوش نشین در آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاه و گدا به دیده دریادلان یکی است</p></div>
<div class="m2"><p>پوشیده است پست و بلند زمین در آب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در راه سالکی که چو خاشاک شد سبک</p></div>
<div class="m2"><p>هر موجه ای پلی است خدا آفرین در آب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارم به بادبان توکل امیدها</p></div>
<div class="m2"><p>هر چند شد سفینه من کاغذین در آب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون عکس آفتاب، نگردد دلش خنک</p></div>
<div class="m2"><p>صد غوطه گر زند جگر آتشین در آب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غمگین نشد دل تو ز گرد ملال من</p></div>
<div class="m2"><p>هر چند کرد آب گهر را گلین در آب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از اشک گرم شد دل سوزان من خنک</p></div>
<div class="m2"><p>وا شد به روی من در خلد برین در آب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشم از لباس جسم، پر و بال داشتم</p></div>
<div class="m2"><p>غافل که بند دست شود آستین در آب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از خامشی خطر نبود سوز عشق را</p></div>
<div class="m2"><p>خورشید می کشد نفس آتشین در آب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در خون باده چند روم، چون نمی رود</p></div>
<div class="m2"><p>گرد یتیمی از رخ در ثمین در آب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از سرکشی نگون ننماید به دیده ها</p></div>
<div class="m2"><p>افتد اگر مثال تو ای نازنین در آب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در چشم من خیال رخ لاله رنگ تو</p></div>
<div class="m2"><p>خوشتر بود ز عکس گل آتشین در آب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از کاکل تو آب دهد گر حباب چشم</p></div>
<div class="m2"><p>هر موجه ای چو زلف شود عنبرین در آب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زینسان که من به فکر فرو رفته ام، نرفت</p></div>
<div class="m2"><p>غواص در تلاش گهر این چنین در آب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پهلو زند به چشمه خورشید هر حباب</p></div>
<div class="m2"><p>شویی چو روی خویشتن ای مه جبین در آب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر حلم زینهار مکن تندی اختیار</p></div>
<div class="m2"><p>تا هست پل به جا، نرود دوربین در آب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تر می کند زمین خود از آب دیگران</p></div>
<div class="m2"><p>با نقش خود مضایقه دارد نگین در آب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفتار سرد، یک جهتان را دودل کند</p></div>
<div class="m2"><p>سازد ز موم خانه جدا انگبین در آب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از عمر برق سیر بود پیچ و تاب من</p></div>
<div class="m2"><p>باشد به قدر سرعت رفتار، چین در آب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پستی گزین که کف ز بلندی نمی رسد</p></div>
<div class="m2"><p>صائب به رتبه صدف ته نشین در آب</p></div></div>