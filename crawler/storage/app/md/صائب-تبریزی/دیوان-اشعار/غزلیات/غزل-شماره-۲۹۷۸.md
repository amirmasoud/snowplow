---
title: >-
    غزل شمارهٔ ۲۹۷۸
---
# غزل شمارهٔ ۲۹۷۸

<div class="b" id="bn1"><div class="m1"><p>چو شاهین بر سر دست آن شکار انداز می گیرد</p></div>
<div class="m2"><p>تذرو رنگ از رخسار گل پرواز می گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خال زیر زلفی عشق رو کرده است رزقم را</p></div>
<div class="m2"><p>تذروم دانه را از چنگل شهباز می گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازین دلسوزتر ای باغبان با بلبلان سر کن</p></div>
<div class="m2"><p>گل این باغ رنگ از شعله آواز می گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنین از سرنوشت پیچ و تابم می شود ظاهر</p></div>
<div class="m2"><p>که دل از دستم آن زلف کمند انداز می گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غبار کوی او را تا به سیر بوستان آرد</p></div>
<div class="m2"><p>زبرگ گل صبا هر روز پای انداز می گیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به من درس مقامات محبت می دهد بلبل</p></div>
<div class="m2"><p>سیه مستی ببین کز دست مطرب ساز می گیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به چشم نکته پردازش مسیحا بر نمی آید</p></div>
<div class="m2"><p>نگاه او سخن را از لب اعجاز می گیرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>علاج حسرت دیرین خود را می کند صائب</p></div>
<div class="m2"><p>اگر این بار جا در بزم آن دمساز می گیرد</p></div></div>