---
title: >-
    غزل شمارهٔ ۵۴۴۲
---
# غزل شمارهٔ ۵۴۴۲

<div class="b" id="bn1"><div class="m1"><p>زیر سقف چرخ بیدردانه پا افشرده ایم</p></div>
<div class="m2"><p>سیل بی زنهار ما در خانه پا افشرده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر در هرکس نمی ساییم رخ چون آفتاب</p></div>
<div class="m2"><p>گنج سان در گوشه ویرانه پا افشرده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون گل پیمانه هردم بر سر دستی نه ایم</p></div>
<div class="m2"><p>چون خم می در دل میخانه پا افشرده ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوچه گرد آستین چون اشک حسرت نیستیم</p></div>
<div class="m2"><p>همچو مژگان بر در یک خانه پا افشرده ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صد کبورت گر فرستد کعبه، بالین نشکنیم</p></div>
<div class="m2"><p>ما و بت یک روز در بتخانه پا افشرده ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکوه زلف از زبان ما نمی آید برون</p></div>
<div class="m2"><p>زیر دست انداز او چون شانه پا افشرده ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر سر ما بگذرد چون خوشه از گردون، رواست</p></div>
<div class="m2"><p>در زمین قابلی چون دانه پا افشرده ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون خمار می به طرف باغ زور آورده است</p></div>
<div class="m2"><p>بر گلوی تاک بیرحمانه پا افشرده ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ریشه در فولاد جوهر اینقدر محکم نکرد</p></div>
<div class="m2"><p>زیر تیغ او عجب مردانه پا افشرده ایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خال او صائب هزاران مور دل پامال کرد</p></div>
<div class="m2"><p>ما عبث در بردن این دانه پا افشرده ایم</p></div></div>