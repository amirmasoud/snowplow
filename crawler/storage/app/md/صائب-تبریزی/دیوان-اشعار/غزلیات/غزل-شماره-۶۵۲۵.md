---
title: >-
    غزل شمارهٔ ۶۵۲۵
---
# غزل شمارهٔ ۶۵۲۵

<div class="b" id="bn1"><div class="m1"><p>مشو چو موج شلاین به هر کنار و برو</p></div>
<div class="m2"><p>کمند طول امل را فراهم آر و برو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان تیره نه جای سپیدکاران است</p></div>
<div class="m2"><p>سبک ز دل نفسی چون سحر برآر و برو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بریز برگ تعلق ز خود مسیحاوار</p></div>
<div class="m2"><p>سر سپهر به زیر قدم درآر و برو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قمار عشق ندارد ندامت از دنبال</p></div>
<div class="m2"><p>بباز هر دو جهان را درین قمار و برو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نثار توست همه گنج های روی زمین</p></div>
<div class="m2"><p>مشو مقید سیم و زر نثار و برو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مکن چو شمع به یک خانه عمر خود را صرف</p></div>
<div class="m2"><p>چو آفتاب به هر جا سری بدار و برو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان شکار و تو چون برق بر جناح سفر</p></div>
<div class="m2"><p>بگیر ران کبابی ازین شکار و برو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو پیش روی تو آید هر آنچه می کاری</p></div>
<div class="m2"><p>مکن نگاه به دنبال خود، بکار و برو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو رفتن از سر کوی وجود ناچارست</p></div>
<div class="m2"><p>چو شمع، ماتم خود پیشتر بدار و برو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز انتظار مکش طایران قدسی را</p></div>
<div class="m2"><p>سری ز بیضه درین آشیان برآ و برو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به یک رفیق موافق بساز در عالم</p></div>
<div class="m2"><p>منافقان جهان را به هم گذار و برو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز لاله زار جهان نیست حاصلی جز داغ</p></div>
<div class="m2"><p>مبند دل به تماشای لاله زار و برو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نسیم مصر طلبکار پاک چشمان است</p></div>
<div class="m2"><p>سفید ساز نظر را ز انتظار و برو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مشو مقید ویرانه جهان چون سیل</p></div>
<div class="m2"><p>سبک دو پای تعلق (ز) گل برآر و برو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز فیض بی ثمری سرو فارغ از سنگ است</p></div>
<div class="m2"><p>به برگ سبز قناعت کن از (بهار) و برو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زمین پاک درین روزگار اکسیرست</p></div>
<div class="m2"><p>مریز دانه خود را به شوره زار و برو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به قدر سعی، صفا یافتند راهروان</p></div>
<div class="m2"><p>به هر دو گام درین راه سر مخار و برو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هزار زخم نمایان اگر خوری بر دل</p></div>
<div class="m2"><p>به روی دشمن خونخوار خود میار و برو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مباد دولت بیدار را به خواب دهی</p></div>
<div class="m2"><p>نمک به چشم گرانخواب خود فشار و برو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو می برند بخواهی نخواهی از دستت</p></div>
<div class="m2"><p>ببوس نقد دل و بر زمین گذار و برو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حریف راهزنان عدم نمی گردی</p></div>
<div class="m2"><p>به زلف او دل و دین و خرد سپار و برو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>میانجی می و مینا نه کار سنگ بود</p></div>
<div class="m2"><p>دل مرا و غمش را به هم گذار و برو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جهان کرایه دیدن نمی کند صائب</p></div>
<div class="m2"><p>چو غنچه سر ز گریبان برون میار و برو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جواب آن غزل است این که گفت عارف روم</p></div>
<div class="m2"><p>به هر زمین که رسی دانه ای بکار و برو</p></div></div>