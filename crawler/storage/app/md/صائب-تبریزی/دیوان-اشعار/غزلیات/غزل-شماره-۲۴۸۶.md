---
title: >-
    غزل شمارهٔ ۲۴۸۶
---
# غزل شمارهٔ ۲۴۸۶

<div class="b" id="bn1"><div class="m1"><p>شوخ‌چشمان درد بیش و کم به دل افزوده‌اند</p></div>
<div class="m2"><p>ورنه ارباب رضا از بیش و کم آسوده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شور محشر را صفیر نی تصور می‌کنند</p></div>
<div class="m2"><p>این سیه‌مستان غفلت بس که خواب‌آلوده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکه دید آن خال‌ها بر دور چشم یار، گفت</p></div>
<div class="m2"><p>این غزالان بین که برگرد حرم آسوده‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کیستم من تا به گرد محمل لیلی رسم؟</p></div>
<div class="m2"><p>برق و باد از دور گردان قدم فرسوده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با چنین عجزی که بیکاری نمی‌آید ز ما</p></div>
<div class="m2"><p>کار دنیا را و عقبی را به ما فرموده‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این جواب آن غزل صائب که می‌گوید حکیم</p></div>
<div class="m2"><p>«بر بناگوشت مثال کفر و دین بنموده‌اند»</p></div></div>