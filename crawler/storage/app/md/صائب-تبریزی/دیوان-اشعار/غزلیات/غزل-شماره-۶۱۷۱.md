---
title: >-
    غزل شمارهٔ ۶۱۷۱
---
# غزل شمارهٔ ۶۱۷۱

<div class="b" id="bn1"><div class="m1"><p>چشم خواب آلود او را در خم ابرو ببین</p></div>
<div class="m2"><p>تیزی شمشیر بنگر، غفلت آهو ببین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کف دست سلیمان گر ندیدی مور را</p></div>
<div class="m2"><p>چشم بگشا خال را بر صفحه آن رو ببین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیچ و تاب دلربایی نیست مخصوص کمر</p></div>
<div class="m2"><p>صاف کن آیینه را این شیوه در هر مو ببین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلفش از هر حلقه دارد چشم بر راه دلی</p></div>
<div class="m2"><p>در به دست آوردن دل اهتمام او ببین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگز از خون شکاری بال تیرش تر نشد</p></div>
<div class="m2"><p>شست صاف دلگشای آن کمان ابرو ببین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می گدازد چشم را خورشید بی ابر تنک</p></div>
<div class="m2"><p>جلوه آن سرو سیم اندام را در جو ببین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلوت آغوش را از نقش انجم پاک کن</p></div>
<div class="m2"><p>بعد ازان چون هاله دلجویی ازان مه رو ببین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شهسواری نیست یار ما کز او گردن کشند</p></div>
<div class="m2"><p>در خم چوگان حکمش چرخ را چون گو ببین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می کند نامرد آب و نان دنیا مرد را</p></div>
<div class="m2"><p>همچو مردان خون دل خور، قوت بازو ببین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می کشد زنگار قد چون سرو بر آیینه ام</p></div>
<div class="m2"><p>تخم غم را در زمین پاک من نیرو ببین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنچه جم در جام از اسرار نتوانست دید</p></div>
<div class="m2"><p>خویش را بر هم شکن در کاسه زانو ببین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خوبی دنیا ز عقبی پشت کاری بیش نیست</p></div>
<div class="m2"><p>چند صائب محو پشت کار باشی، رو ببین</p></div></div>