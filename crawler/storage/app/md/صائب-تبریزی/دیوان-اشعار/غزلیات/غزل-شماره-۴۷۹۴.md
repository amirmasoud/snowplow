---
title: >-
    غزل شمارهٔ ۴۷۹۴
---
# غزل شمارهٔ ۴۷۹۴

<div class="b" id="bn1"><div class="m1"><p>ز سروقدتو شد شوره زار امکان سبز</p></div>
<div class="m2"><p>ز شمع سبز تو شد بخت این شبستان سبز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز خط پشت لبت زنده می شود دلها</p></div>
<div class="m2"><p>چنین بود چو کند سبزه آب حیوان سبز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میان اهل جنون سبز چون توانم شد؟</p></div>
<div class="m2"><p>نشد ز گریه من خار این بیابان سبز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به رود نیل رسان خویش راکه هیهات است</p></div>
<div class="m2"><p>کز آب چاه شود بخت ماه کنعان سبز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز هم گزیر ندارند نوش ونیش جهان</p></div>
<div class="m2"><p>که بی گره نشود نی زشکرستان سبز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو زنگ از دل من برد باده دانستم</p></div>
<div class="m2"><p>که تخم سوخته گرددبه ابراحسان سبز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تجردی که بود در لباس ،محفوظ است</p></div>
<div class="m2"><p>پناه شیر بود ،هست تانیستان سبز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل حریص به زنگ قساوت آلوده است</p></div>
<div class="m2"><p>که نان همیشه گدا را شود درانبان سبز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زچشم شور کواکب مجوبرومندی</p></div>
<div class="m2"><p>که هیچ دانه نگرددبه زیردندان سبز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر گشایش دل خواهی از بلا مگریز</p></div>
<div class="m2"><p>که دانه می شود اینجاز تیر باران سبز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به باده جوش نزد خون خشک من صائب</p></div>
<div class="m2"><p>نشد ز تربیت بحر شاخ مرجان سبز</p></div></div>