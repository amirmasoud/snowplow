---
title: >-
    غزل شمارهٔ ۱۱۱۹
---
# غزل شمارهٔ ۱۱۱۹

<div class="b" id="bn1"><div class="m1"><p>لعل نسبت با لب یاقوت او بیجاده است</p></div>
<div class="m2"><p>صبح با آن چهره خندان در نگشاده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دشت از چشم غزالان سینه پر داغ اوست</p></div>
<div class="m2"><p>آن که ما دیوانگان را سر به صحرا داده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حاصل عمر از حضور دوستان گل چیدن است</p></div>
<div class="m2"><p>ورنه آب و دانه در کنج قفس آماده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق محتاج دلیل و رهنما چون عقل نیست</p></div>
<div class="m2"><p>خضر در قطع بیابان بی نیاز از جاده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می کند در خانه خود سیر صحرای بهشت</p></div>
<div class="m2"><p>سینه هر کس که از خار تمنا ساده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که گردانید از دنیا رهزن روی خویش</p></div>
<div class="m2"><p>بی تردد پشت بر دیوار منزل داده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرد ظلمت شسته است از روی آب زندگی</p></div>
<div class="m2"><p>هر سری کز سایه بال هما آزاده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سردی دوران به ما دست و دلی نگذاشته است</p></div>
<div class="m2"><p>در خزان اشجار را برگ سفر آماده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اختر بی طالع ما در بساط آسمان</p></div>
<div class="m2"><p>خال موزونی است بر رخسار زشت افتاده است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سینه ما صائب از خود می دهد بیرون گهر</p></div>
<div class="m2"><p>پیش نیسان این صدف هرگز دهن نگشاده است</p></div></div>