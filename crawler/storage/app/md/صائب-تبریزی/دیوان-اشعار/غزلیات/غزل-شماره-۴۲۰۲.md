---
title: >-
    غزل شمارهٔ ۴۲۰۲
---
# غزل شمارهٔ ۴۲۰۲

<div class="b" id="bn1"><div class="m1"><p>ریزش چو شیشه هرکه به آوازه می کند</p></div>
<div class="m2"><p>در هرپیاله زخم مرا تازه می کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از صحبت آن که خاطر جمع است مطلبش</p></div>
<div class="m2"><p>سی پاره را به تفرقه شیرازه می کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخسار چون بهشت تو در هر نظاره ای</p></div>
<div class="m2"><p>ایمان من به خلد برین تازه می کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امشب کراست عزم تماشای ماهتاب</p></div>
<div class="m2"><p>کز هاله مه تهیه خمیازه می کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن روی چون گل است ز گلگونه بی نیاز</p></div>
<div class="m2"><p>مشاطه خون عبث به دل غازه می کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مستان برون ز عالم اندازه رفته اند</p></div>
<div class="m2"><p>ساقی همان رعایت اندازه می کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون ابر حاصلش ز کرم شهرت است وبس</p></div>
<div class="m2"><p>صائب کسی که جود به آوازه می کند</p></div></div>