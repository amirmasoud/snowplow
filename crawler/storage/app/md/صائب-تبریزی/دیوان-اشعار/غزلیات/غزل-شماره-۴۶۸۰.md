---
title: >-
    غزل شمارهٔ ۴۶۸۰
---
# غزل شمارهٔ ۴۶۸۰

<div class="b" id="bn1"><div class="m1"><p>گشاده رویی من برد دست خصم از کار</p></div>
<div class="m2"><p>شراب شیشه شکن در پیاله شد هموار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کباب سینه گرم من است داغ جنون</p></div>
<div class="m2"><p>زمین سوخته جان می دهد به تخم شرار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مکن ز سختی ره شکوه همچو نو سفران</p></div>
<div class="m2"><p>که خاک نرم کند آب را گران رفتار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی هزار شد از باده زنگ کلفت من</p></div>
<div class="m2"><p>که آب سبزه خوابیده را کند بیدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به زهد خشک به جایی نمی رسد زاهد</p></div>
<div class="m2"><p>که پای آبله دارست دست سبحه شمار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میان بلبل و پروانه فرق بسیارست</p></div>
<div class="m2"><p>کجا به رتبه کردار می رسد گفتار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شده است سرو حصاری ز طوق فاختگان</p></div>
<div class="m2"><p>قد خدنگ تو تا گشته در چمن سیار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گناه مانع ایجاد ما نشد اول</p></div>
<div class="m2"><p>چگونه مانع غفران شود در آخر کار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز موج ریگ روان است بر جناح سفر</p></div>
<div class="m2"><p>مدار چشم اقامت ز عمر خوش رفتار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جگر خراش تر از شیشه است باده عشق</p></div>
<div class="m2"><p>بشوی دست ز جان، لب براین پیاله گذار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شود پر از گهر از حفظ آبرو صائب</p></div>
<div class="m2"><p>صدف اگر نگشاید دهن به ابر بهار</p></div></div>