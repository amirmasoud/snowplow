---
title: >-
    غزل شمارهٔ ۶۲۷۱
---
# غزل شمارهٔ ۶۲۷۱

<div class="b" id="bn1"><div class="m1"><p>نرسد هیچ کمالی به سخن سنجیدن</p></div>
<div class="m2"><p>که سخن را صله ای نیست به از فهمیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می خلد بیشتر از شیون ماتم در دل</p></div>
<div class="m2"><p>سخن آهسته نگفتن، به صدا خندیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لب خاموش مرا بر سر حرف آوردن</p></div>
<div class="m2"><p>هست احوال ز بیمار گران پرسیدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خار از چیدن دامن، گل بی خار شود</p></div>
<div class="m2"><p>پرده عیب جهان است نظر پوشیدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عید و نوروز به مردم چه مبارک می بود</p></div>
<div class="m2"><p>چشم وادید نمی داشت گر از پی، دیدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حرص را بستر آرام نمی گردد مرگ</p></div>
<div class="m2"><p>مار را پیچ و خم افزون شود از خوابیدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کیست در وادی ایجاد به گمراهی من؟</p></div>
<div class="m2"><p>که نشان قدمم محو شد از لغزیدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غافلان را نبود بهره ای از عالم غیب</p></div>
<div class="m2"><p>پای خوابیده چه در خواب تواند دیدن؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از گرانسنگی کوه گنه خود شادم</p></div>
<div class="m2"><p>که به میزان قیامت نتوان سنجیدن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سبک از خشم نگردند گران تمکینان</p></div>
<div class="m2"><p>که محال است شود بحر کم از جوشیدن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آب تا بود دلم، در دل دریا بودم</p></div>
<div class="m2"><p>کرد از بحر مرا دور، گهر گردیدن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بال مرغان گلستان شودش دست دعا</p></div>
<div class="m2"><p>هر که قانع شود از چیدن گل با دیدن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چین بر ابرو من از موج حوادث صائب</p></div>
<div class="m2"><p>که دودم می شود این تیغ ز سر پیچیدن</p></div></div>