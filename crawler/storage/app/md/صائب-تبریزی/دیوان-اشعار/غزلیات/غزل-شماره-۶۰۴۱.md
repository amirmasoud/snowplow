---
title: >-
    غزل شمارهٔ ۶۰۴۱
---
# غزل شمارهٔ ۶۰۴۱

<div class="b" id="bn1"><div class="m1"><p>شوق ما بال و پر جسم گران خواهد شدن</p></div>
<div class="m2"><p>دار بر منصور ما تخت روان خواهد شدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق دارد سختیی اما گوارا می شود</p></div>
<div class="m2"><p>بیستون بر کوهکن رطل گران خواهد شدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هست اگر هر گریه ای را خنده ای در چاشنی</p></div>
<div class="m2"><p>ریشه غم در دل ما زعفران خواهد شدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از هواداران مشو غافل که وقت برگریز</p></div>
<div class="m2"><p>طوق قمری سرو را خط امان خواهد شدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون زلیخا هر که در عشق جوانان پیر شد</p></div>
<div class="m2"><p>از ورق گردانی دوران جوان خواهد شدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر به این عنوان شود اوضاع دنیا ناگوار</p></div>
<div class="m2"><p>خضر بیزار از حیات جاودان خواهد شدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رشته سر در گم ما را نخواهد یافتن</p></div>
<div class="m2"><p>سوزن عیسی اگر بر آسمان خواهد شدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زین شکست و بست کز گردون مرا در طالع است</p></div>
<div class="m2"><p>استخوانم مغز و مغزم استخوان خواهد شدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب آن آیینه رو خواهد به فکر ما فتاد</p></div>
<div class="m2"><p>طوطی خاموش ما شکرفشان خواهد شدن</p></div></div>