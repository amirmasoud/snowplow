---
title: >-
    غزل شمارهٔ ۲۴۸۱
---
# غزل شمارهٔ ۲۴۸۱

<div class="b" id="bn1"><div class="m1"><p>گوشه‌گیرانی که رو در خلوت دل کرده‌اند</p></div>
<div class="m2"><p>رشته جان را خلاص از مهره گِل کرده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اهل دنیا در نظربازی به اسباب جهان</p></div>
<div class="m2"><p>حلقه‌ای هر لحظه افزون بر سلاسل کرده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کارافزایان که دنبال تکلف رفته‌اند</p></div>
<div class="m2"><p>زندگی و مرگ را بر خویش مشکل کرده‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر رخ بی‌پرده مقصود، کوته دیدگان</p></div>
<div class="m2"><p>پرده‌ها افزون ز دامان وسایل کرده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مد احسان می‌شمارند این گروه تنگ‌چشم</p></div>
<div class="m2"><p>چین ابرویی اگر در کار سایل کرده‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از ورق‌گردانی افلاک فارغ گشته‌اند</p></div>
<div class="m2"><p>خرده‌بینانی که سیر نقطه دل کرده‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوربینانی که نبض ره به دست آورده‌اند</p></div>
<div class="m2"><p>خار را از پای خود بیرون به منزل کرده‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گوشه‌گیرانی که دل را از هوس نزدوده‌اند</p></div>
<div class="m2"><p>خلوت خود را ز فکر پوچ، محفل کرده‌اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از پی روپوش، واصل گشتگان همچون جرس</p></div>
<div class="m2"><p>ناله‌های خون‌چکان در پای محمل کرده‌اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لنگر تسلیم از دست تو بیرون رفته است</p></div>
<div class="m2"><p>ورنه از موج خطر بسیار ساحل کرده‌اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کشتگان عشق اگر دستی برون آورده‌اند</p></div>
<div class="m2"><p>خونبهای خویش در دامان قاتل کرده‌اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در بهار بی‌خزان حشر، با صد شاخ و برگ</p></div>
<div class="m2"><p>سبز خواهد گشت هر تخمی که در گِل کرده‌اند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چشم می‌پوشند صائب از تماشای بهشت</p></div>
<div class="m2"><p>رهنوردانی که سیر عالم دل کرده‌اند</p></div></div>