---
title: >-
    غزل شمارهٔ ۷۳۴
---
# غزل شمارهٔ ۷۳۴

<div class="b" id="bn1"><div class="m1"><p>رویت ز هاله حلقه کند نام ماه را</p></div>
<div class="m2"><p>دلسرد از آفتاب کند صبحگاه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر جلوه ای ز قد قیامت خرام تو</p></div>
<div class="m2"><p>از دل نفس گسسته برون آرد آه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دیده نظارگیان میل سرمه کرد</p></div>
<div class="m2"><p>رخسار آتشین تو مد نگاه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خط رسد به نشو و نما سبزه امید</p></div>
<div class="m2"><p>باران بود زیادتر، ابر سیاه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا بر سرشکسته نوازی است آفتاب</p></div>
<div class="m2"><p>پروایی از شکستن خود نیست ماه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سستی مکن که جاذبه کعبه امید</p></div>
<div class="m2"><p>بسیار کرده شهپر دیوار، کاه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مستغنی از دلیل بود دل چو آگه است</p></div>
<div class="m2"><p>ننموده کس به قبله نما قبله گاه را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جای قرار نیست درین تیره خاکدان</p></div>
<div class="m2"><p>در بحر همچو سیل فشان گرد راه را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جایی که بحر و کان، لب خشک است و چشم تر</p></div>
<div class="m2"><p>پیداست تا چه قدر بود خاک راه را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون سرخوشان مکن به یمین و یسار میل</p></div>
<div class="m2"><p>از عرض ره دراز مکن طول راه را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شیرازه قلمرو کثرت ز وحدت است</p></div>
<div class="m2"><p>دارد علم بپا ز ستادن سپاه را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بال و پر نهال امیدست خاک پاک</p></div>
<div class="m2"><p>زنهار وقت صبح مکن فوت آه را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صائب مباش در صدد معذرت که نیست</p></div>
<div class="m2"><p>بهتر ز انفعال، شفیعی گناه را</p></div></div>