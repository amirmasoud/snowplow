---
title: >-
    غزل شمارهٔ ۶۰۷
---
# غزل شمارهٔ ۶۰۷

<div class="b" id="bn1"><div class="m1"><p>گداخت دیدن آن روی بی نقاب مرا</p></div>
<div class="m2"><p>چو نخل موم، نمی سازد آفتاب مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جنون به بادیه پرورده چون سراب مرا</p></div>
<div class="m2"><p>سواد شهر بود آیه عذاب مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو ماه نو به تواضع ز خاک می گذرم</p></div>
<div class="m2"><p>اگر سپهر دهد بوسه بر رکاب مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز پنبه سر مینا به حلقم آب چکان</p></div>
<div class="m2"><p>نمی رود به گلو آب، بی شراب مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز سینه ام دل پرداغ را برون آرید</p></div>
<div class="m2"><p>که سیر کرد ز جان دود این کباب مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی به موی نیاویخته است خرمن گل</p></div>
<div class="m2"><p>غم میان تو دارد به پیچ و تاب مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به یک دو قطره که خواهد گهر شدن روزی</p></div>
<div class="m2"><p>رهین منت خود گو مکن سحاب مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عبث چه عمر به افسانه می کنی ضایع؟</p></div>
<div class="m2"><p>چو چشم رخنه دیوار نیست خواب مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فغان که با همه کاوش که کرد ناخن سعی</p></div>
<div class="m2"><p>نشد گشادی ازان غنچه نقاب مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه ذره ام که به خورشید همعنان گردم؟</p></div>
<div class="m2"><p>بس است گوشه چشمی ازان رکاب مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درین بهار که گل کرد رازها صائب</p></div>
<div class="m2"><p>نشد گشادی ازان غنچه نقاب مرا</p></div></div>