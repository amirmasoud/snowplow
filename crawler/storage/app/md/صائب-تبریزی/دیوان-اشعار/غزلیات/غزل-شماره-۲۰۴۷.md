---
title: >-
    غزل شمارهٔ ۲۰۴۷
---
# غزل شمارهٔ ۲۰۴۷

<div class="b" id="bn1"><div class="m1"><p>بازآ که بی تو مجلس ما را حضور نیست</p></div>
<div class="m2"><p>در جبهه صراحی و پیمانه نور نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از زنده رود زنده دلی آب خورده ایم</p></div>
<div class="m2"><p>در موج خیز غم دل ما بی سرور نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرگان روزگار ز یکدیگرش درند</p></div>
<div class="m2"><p>آن را که پوستین گریبان سمور نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیراهنی کجاست، که بر اهل روزگار</p></div>
<div class="m2"><p>روشن شود که دیده یعقوب کور نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از پرتو جمال تو خواهد گداختن</p></div>
<div class="m2"><p>آخر خمیر آینه از سنگ طور نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر جا نفیر خواب کند بخت ما بلند</p></div>
<div class="m2"><p>آنجا مجال دم زدن نفخ صور نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر گرم عشق را به کلاه نمد چه کار؟</p></div>
<div class="m2"><p>خورشید اگر برهنه نگردد قصور نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از برق حادثات به باد فنا رود</p></div>
<div class="m2"><p>هر خرمنی که گوشه چشمش به مور نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا چند در میان فکنی باد و شانه را؟</p></div>
<div class="m2"><p>دل را نمی دهیم به زلف تو، زور نیست!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دست سبو سلامت و پای خم شراب!</p></div>
<div class="m2"><p>ما را چه شد که دست به زانوی حور نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کوته نظر تلاش کند قرب دوست را</p></div>
<div class="m2"><p>نزدیک را خبر ز نگه های دور نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صائب چه آتشی است، که در بزم روزگار</p></div>
<div class="m2"><p>بی شعله طبیعت او هیچ نور نیست</p></div></div>