---
title: >-
    غزل شمارهٔ ۵۹۲۲
---
# غزل شمارهٔ ۵۹۲۲

<div class="b" id="bn1"><div class="m1"><p>از صبر عنان دل خود کام گرفتیم</p></div>
<div class="m2"><p>آن طایر وحشی به همین دام گرفتیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بردانه نا پخته دویدیم چو آدم</p></div>
<div class="m2"><p>ما کار خود از روز ازل خام گرفتیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چند چو دستار شد از گریه بسیار</p></div>
<div class="m2"><p>از دیده خود جامه احرام گرفتیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بودیم سبکسر چو سپند از رگ خامی</p></div>
<div class="m2"><p>از سوختگی دامن آرام گرفتیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیدیم زمین تخته مشق است فلک را</p></div>
<div class="m2"><p>ننشسته درین خانه ره بام گرفتیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد لخت جگر تا به لب خویش رساندیم</p></div>
<div class="m2"><p>هر لقمه که از خلق به ابرام گرفتیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مخمور ز نقل و می روشن نگرفته است</p></div>
<div class="m2"><p>فیضی که ازان چشم چو بادام گرفتیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سودیم به گردون کله از فخر چو خورشید</p></div>
<div class="m2"><p>تا بوسه چند از لب آن بام گرفتیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از چشمه کوثر طمع خام نداریم</p></div>
<div class="m2"><p>ما داد خود اینجا ز لب جام گرفتیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون فاخته از رتبه اقبال محبت</p></div>
<div class="m2"><p>جا در بر آن سرو گل اندام گرفتیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پروانه صفت صدق طلب رهبر ما شد</p></div>
<div class="m2"><p>صائب خط پروانگی از شام گرفتیم</p></div></div>