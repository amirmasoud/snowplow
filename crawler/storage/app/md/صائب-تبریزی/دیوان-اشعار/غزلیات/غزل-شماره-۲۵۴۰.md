---
title: >-
    غزل شمارهٔ ۲۵۴۰
---
# غزل شمارهٔ ۲۵۴۰

<div class="b" id="bn1"><div class="m1"><p>زلف دلها را به دور خط نگهبانی کند</p></div>
<div class="m2"><p>چون شود معزول عامل سبحه گردانی کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست گلچین می شود هر خار مژگانی که هست</p></div>
<div class="m2"><p>از عرق چون چهره ساقی گل افشانی کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکر قاتل را به خاموشی ادا کردم که نقش</p></div>
<div class="m2"><p>خامه نقاش را تحسین به حیرانی کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون صنوبر در سر هر موی دارد ناله ای</p></div>
<div class="m2"><p>هر که دلهای پریشان را نگهبانی کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>معنی فرمانروایی نیست جز اجرای حکم</p></div>
<div class="m2"><p>در سرای خویش هر موری سلیمانی کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شرط مهمانی غذای روح سامان دادن است</p></div>
<div class="m2"><p>اهل دل راهر که می خواهد که مهمانی کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از گرانجانان سبکروحی که کلفت می کشد</p></div>
<div class="m2"><p>با سبکروحان نمی باید گرانجانی کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زندگانی تلخ بر دریا شود هر گه صدف</p></div>
<div class="m2"><p>دست خود را باز پیش ابر نیسانی کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قد چو خم شد، زود می آید بسر دوران عمر</p></div>
<div class="m2"><p>وسعت میدان چه با این اسب چوگانی کند؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زخمی شمشیر زهرآلود منت، از کریم</p></div>
<div class="m2"><p>مد احسان را شمار چین پیشانی کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نغمه داودی اینجا در پس صد پرده است</p></div>
<div class="m2"><p>پیش صائب کیست بلبل تا غزلخوانی کند؟</p></div></div>