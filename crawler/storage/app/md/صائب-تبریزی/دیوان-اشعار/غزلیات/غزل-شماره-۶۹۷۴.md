---
title: >-
    غزل شمارهٔ ۶۹۷۴
---
# غزل شمارهٔ ۶۹۷۴

<div class="b" id="bn1"><div class="m1"><p>افتاده کار ما را با یار شوخ و شنگی</p></div>
<div class="m2"><p>در جنگ دیر صلحی در صلح زود جنگی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل مرا سبک کرد درد مرا گران ساخت</p></div>
<div class="m2"><p>چشم تمام خوابی رخسار نیمرنگی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما را به یک نوازش بستان ز دست عالم</p></div>
<div class="m2"><p>آخر گران نگردد دیوانه ای به سنگی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از صلح و جنگ عالم آسوده ایم و فارغ</p></div>
<div class="m2"><p>ما را که هست با خود هر لحظه صلح و جنگی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از خود برون دویدیم دیوانه وار صائب</p></div>
<div class="m2"><p>هر طفل را که دیدیم در دست داشت سنگی</p></div></div>