---
title: >-
    غزل شمارهٔ ۳۹۷۵
---
# غزل شمارهٔ ۳۹۷۵

<div class="b" id="bn1"><div class="m1"><p>سری که خالی از اندیشه محال شود</p></div>
<div class="m2"><p>ز فیض عشق پریخانه خیال شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به حسن ساخته زنهار اعتماد مکن</p></div>
<div class="m2"><p>که در دو هفته مه چارده هلال شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به جلوه ای زتو چون چشم ما شود خرسند</p></div>
<div class="m2"><p>چگونه آینه قانع به یک مثال شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همین سیاهیی از آب زندگی دیده است</p></div>
<div class="m2"><p>ز حسن هر که مقید به خط وخال شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمی کشند صراحی قدان سر از حکمش</p></div>
<div class="m2"><p>لبی که چون لب پیمانه بی سؤال شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز اهل درد ترا آن زمان حساب کنند</p></div>
<div class="m2"><p>که زعفران به دلت ریشه ملال شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مدار دست ز دامان کیمیاگر فقر</p></div>
<div class="m2"><p>کز احتیاج حرام جهان حلال شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فلک ز خرده انجم تمام چشم شده است</p></div>
<div class="m2"><p>که همچو شبنم گل محو آن جمال شده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نظر بلند گردد ز عشق، داغ پلنگ</p></div>
<div class="m2"><p>هزار پرده به از دیده غزال شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در آن مقام که مستان به رقص برخیزند</p></div>
<div class="m2"><p>فلک چو سبزه خوابیده پایمال شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فلک به خاک نهادان چه می تواند کرد</p></div>
<div class="m2"><p>سبو شکسته چو شدساغر سفال شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو سعی کن که به روشندلان رسی صائب</p></div>
<div class="m2"><p>که سیل واصل دریا چو شد زلال شود</p></div></div>