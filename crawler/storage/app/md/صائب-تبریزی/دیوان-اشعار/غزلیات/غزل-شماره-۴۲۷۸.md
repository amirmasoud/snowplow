---
title: >-
    غزل شمارهٔ ۴۲۷۸
---
# غزل شمارهٔ ۴۲۷۸

<div class="b" id="bn1"><div class="m1"><p>از یاد وصل، دیده من سیر می‌شود</p></div>
<div class="m2"><p>مهتاب در پیاله من شیر می‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز به سوی خویش نمی‌بینی از حجاب</p></div>
<div class="m2"><p>در خلوت تو آینه دلگیر می‌شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دور نشاط زود به انجام می‌رسد</p></div>
<div class="m2"><p>می چون دو سال عمر کند پیر می‌شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ظالم به مرگ دست نمی‌دارد از ستم</p></div>
<div class="m2"><p>آخر پر عقاب پر تیر می‌شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن را که روزگار نگیرد به هر گناه</p></div>
<div class="m2"><p>چون جمع شد گناه، خداگیر می‌شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از چشم آهوانه لیلی حذر کند</p></div>
<div class="m2"><p>مجنون اگرچه در دهن شیر می‌شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تدبیر بنده سایه تقدیر ایزدست</p></div>
<div class="m2"><p>ورنه کدام کار به تدبیر می‌شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اشک ندامت تو به دامن نمی‌رسد</p></div>
<div class="m2"><p>هرچند بیشتر ز تو تقصیر می‌شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طومار شکوه تو به افلاک می‌رسد</p></div>
<div class="m2"><p>یک لحظه روزی تو اگر دیر می‌شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون آفتاب، فکر من آفاق را گرفت</p></div>
<div class="m2"><p>حسن غریب زود جهانگیر می‌شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نتوان گذشتن از دو جهان بی‌جهاد نفس</p></div>
<div class="m2"><p>این راه دور قطع به شمشیر می‌شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صائب به گریه گرد برآورد از جهان</p></div>
<div class="m2"><p>سیل بهار را که عنانگیر می‌شود</p></div></div>