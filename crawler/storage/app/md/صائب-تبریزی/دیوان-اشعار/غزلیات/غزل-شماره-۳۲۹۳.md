---
title: >-
    غزل شمارهٔ ۳۲۹۳
---
# غزل شمارهٔ ۳۲۹۳

<div class="b" id="bn1"><div class="m1"><p>دل آسوده طمع هر که ز دنیا دارد</p></div>
<div class="m2"><p>زیر بال و پر خود بیضه عنقا دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غافل از حق نشود روح به ویرانه جسم</p></div>
<div class="m2"><p>سیل هر جا که بود روی به دریا دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خویش را تا نگذارد ننشیند از پا</p></div>
<div class="m2"><p>هرکه چون شمع سر عالم بالا دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دم جان بخش اثر در دل آهن نکند</p></div>
<div class="m2"><p>چشم سوزن چه تمتع ز مسیحا دارد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از علم لشکریان را نتوان غافل کرد</p></div>
<div class="m2"><p>دو جهان چشم بر آن قامت رعنا دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کار چون در گره افتد به دعا دست برآر</p></div>
<div class="m2"><p>شانه در عقده گشایی ید طولی دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست خالی سر مویی به تن از جان لطیف</p></div>
<div class="m2"><p>هر که را جا نبود، در همه جا جا دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل محال است که ساکن شود از لرزیدن</p></div>
<div class="m2"><p>شانه تا راه در آن زلف چلیپا دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر چه یعقوب مرا پای طلب کوتاه است</p></div>
<div class="m2"><p>بوی پیراهن یوسف ید طولی دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو ز طفلی است که در خانه نداری آرام</p></div>
<div class="m2"><p>ور نه هنگامه عالم چه تماشا دارد؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لازم برق بود ریزش باران صائب</p></div>
<div class="m2"><p>گریه بسیار ز پی خنده بیجا دارد</p></div></div>