---
title: >-
    غزل شمارهٔ ۱۶۵۶
---
# غزل شمارهٔ ۱۶۵۶

<div class="b" id="bn1"><div class="m1"><p>عتاب و لطف ز ابروی گلرخان پیداست</p></div>
<div class="m2"><p>صفای هر چمن از روی باغبان پیداست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا که خرمن گل در کنار می باید</p></div>
<div class="m2"><p>ازین چه سود که دیوار گلستان پیداست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گلی ز غنچه پیکان یار خواهم چید</p></div>
<div class="m2"><p>گشاد کار من از خانه کمان پیداست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به چشم بلبل مستی که عشق سرمه کشید</p></div>
<div class="m2"><p>رخ بهار ز آیینه خزان پیداست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به طرز تازه قسم یاد می کنم صائب</p></div>
<div class="m2"><p>که جای طالب آمل در اصفهان پیداست</p></div></div>