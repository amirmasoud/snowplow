---
title: >-
    غزل شمارهٔ ۶۳۱۴
---
# غزل شمارهٔ ۶۳۱۴

<div class="b" id="bn1"><div class="m1"><p>سخن ز مهر و وفا با تو بی وفا گفتن</p></div>
<div class="m2"><p>بود به گوش گران حرف بی صدا گفتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه آنچنان تو به بیگانگی برآمده ای</p></div>
<div class="m2"><p>که در لباس توان حرف آشنا گفتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به داد من برس ای کافر خداناترس</p></div>
<div class="m2"><p>که مانده شد نفسم از خدا خدا گفتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترحم است بر آن بی زبان بزم وصال</p></div>
<div class="m2"><p>که یک سخن نتواند به مدعا گفتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکایتی که ز گفتن یکی هزار شود</p></div>
<div class="m2"><p>هزار بار به از گفتن است نا گفتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تسلی از دو لبش چون شوم به یک دشنام؟</p></div>
<div class="m2"><p>که تلخ، قند مکرر شود ز وا گفتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل گرفته شود باز از هواخواهان</p></div>
<div class="m2"><p>خوش است غنچه صفت راز با صبا گفتن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پناه گیر به دارالامان خاموشی</p></div>
<div class="m2"><p>ترا که نیست میسر سخن بجا گفتن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه حاجت است به اجماع، جود خالص را؟</p></div>
<div class="m2"><p>برای نام بود خلق را صلا گفتن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به شیشه خانه عمر خودست سنگ زدن</p></div>
<div class="m2"><p>ز ممسکی سخن سخت با گدا گفتن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ازان شکسته شود نفس وزین شود مغرور</p></div>
<div class="m2"><p>هجای خلق بود بهتر از ثنا گفتن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چگونه نسبت درویش به مه کنم صائب؟</p></div>
<div class="m2"><p>مرا زبان چو نگردد به ناسزا گفتن</p></div></div>