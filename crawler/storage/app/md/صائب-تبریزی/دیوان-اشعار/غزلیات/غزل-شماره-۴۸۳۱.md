---
title: >-
    غزل شمارهٔ ۴۸۳۱
---
# غزل شمارهٔ ۴۸۳۱

<div class="b" id="bn1"><div class="m1"><p>میوه باغ امیدم داغ حرمان است و بس</p></div>
<div class="m2"><p>یار دلسوزی که می‌بینم نمکدان است و بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پشت و روی این ورق را بارها گردیده‌ام</p></div>
<div class="m2"><p>عالم از جهان مرکب یک شبستان است و بس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نور شرم از دیده خوبان بازاری مجوی</p></div>
<div class="m2"><p>این جواهر سرمه در چشم غزالان است و بس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سنگ را یاقوت می‌سازم به صد خون جگر</p></div>
<div class="m2"><p>روزیم چون آفتاب از چرخ یک نان است و بس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن که گاهی عقده‌ای وامی‌کند از کار من</p></div>
<div class="m2"><p>در بیابان طلب خار مغیلان است و بس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌کشد هرکس که در قید لباس آرد مرا</p></div>
<div class="m2"><p>حلقه فتراک من طوق گریبان است وبس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون نگردم گرد سرتاپای او چون گردباد؟</p></div>
<div class="m2"><p>پاکدامانی که می‌بینم بیابان است و بس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل نیازردن اگر شرط مسلمانی بود</p></div>
<div class="m2"><p>می‌توان گفتن همین هندو مسلمان است و بس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشم عبرت باز کن صائب ز شبنم پندگیر</p></div>
<div class="m2"><p>حاصل قرب نکویان چشم گریان است و بس</p></div></div>