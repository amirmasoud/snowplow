---
title: >-
    غزل شمارهٔ ۱۱۲۶
---
# غزل شمارهٔ ۱۱۲۶

<div class="b" id="bn1"><div class="m1"><p>ساقی ما از می گلگون به دور افتاده است</p></div>
<div class="m2"><p>همچو ساغر آن لب میگون به دور افتاده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دل شب عاشقان را حلقه بر در می زند</p></div>
<div class="m2"><p>گرد رویش تا خط شبگون به دور افتاده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمع در پیراهن فانوس گردیده است آب</p></div>
<div class="m2"><p>تا ز رقص آن قامت موزون به دور افتاده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می کشد خط بر زمین از شرمساری گردباد</p></div>
<div class="m2"><p>در بیابانی که این مجنون به دور افتاده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا که دیگر در خمار افتاده، کز هر لاله ای</p></div>
<div class="m2"><p>هر طرف پیمانه ای پر خون به دور افتاده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می نشیند گردباد از پا به اندک جلوه ای</p></div>
<div class="m2"><p>تا غبار کیست در هامون به دور افتاده است؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جای حیرت نیست گر من پایکوبان گشته ام</p></div>
<div class="m2"><p>خم به زور باده چون گردون به دور افتاده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا به آب غیب، ایمان تازه سازی هر نفس</p></div>
<div class="m2"><p>بنگر این نه آسیا را چون به دور افتاده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب از وحدت نیفتد نوبهار از جوش گل</p></div>
<div class="m2"><p>در هزاران لفظ یک مضمون به دور افتاده است</p></div></div>