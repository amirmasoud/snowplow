---
title: >-
    غزل شمارهٔ ۳۶۷۰
---
# غزل شمارهٔ ۳۶۷۰

<div class="b" id="bn1"><div class="m1"><p>قبا ز شرم بر آن سیمتن نمی چسبد</p></div>
<div class="m2"><p>که شمع را به بدن پیرهن نمی چسبد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به حیرتم که چرا زلف یار با این قرب</p></div>
<div class="m2"><p>به هر دو دست به سیب ذقن نمی چسبد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز گل توقع خونگرمیم ز ساده دلی است</p></div>
<div class="m2"><p>که خار خشک به دامان من نمی چسبد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر ز جانب شیرین توجهی نبود</p></div>
<div class="m2"><p>به کار دست و دل کوهکن نمی چسبد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>علاقه ای به حیات دو روزه نیست مرا</p></div>
<div class="m2"><p>چو گل به دامن کس خون من نمی چسبد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دهان شکوه ما را به حرف نتوان بست</p></div>
<div class="m2"><p>که زخم تیغ به آب دهن نمی چسبد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به نامه یاد نکردن نه از فراموشی است</p></div>
<div class="m2"><p>ز دوریت به قلم دست من نمی چسبد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شهید را ز کفن چشم پرده پوشی نیست</p></div>
<div class="m2"><p>نمک به سینه مجروح من نمی چسبد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به هر دلی که ندارد ز معرفت خبری</p></div>
<div class="m2"><p>کلام صائب شیرین سخن نمی چسبد</p></div></div>