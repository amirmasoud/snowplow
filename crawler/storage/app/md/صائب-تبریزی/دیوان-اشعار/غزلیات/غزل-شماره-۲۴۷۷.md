---
title: >-
    غزل شمارهٔ ۲۴۷۷
---
# غزل شمارهٔ ۲۴۷۷

<div class="b" id="bn1"><div class="m1"><p>ساده لوحانی که درد خود به درمان داده اند</p></div>
<div class="m2"><p>دامن یوسف زدست از مکر اخوان داده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محرمان کعبه مقصود، از تار نفس</p></div>
<div class="m2"><p>جامه احرام خود چون صبح سامان داده اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک گل بی خار گردیده است در چشمم جهان</p></div>
<div class="m2"><p>تا مرا چون شبنم گل چشم حیران داده اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست غافل عاشق از پاس ادب در بیخودی</p></div>
<div class="m2"><p>عندلیب مست را سر در گلستان داده اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناله زنجیر دارد حلقه چشم غزال</p></div>
<div class="m2"><p>تا من دیوانه را سر در بیابان داده اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اهل دنیا حلقه بیرون در گردیده اند</p></div>
<div class="m2"><p>همچو زنبور عسل تا خانه سامان داده اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عارفان را شکوه ای از گردش افلاک نیست</p></div>
<div class="m2"><p>اختیار کشتی خود را به طوفان داده اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زیر بار منت گردون دو تا گردیده ام</p></div>
<div class="m2"><p>همچو ماه نو مرا تا یک لب نان داده اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتگوی ماست بی حاصل، وگرنه مور را</p></div>
<div class="m2"><p>مزد گفتار از شکرخند سلیمان داده اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از دل پرخون و آه آتشین و اشک گرم</p></div>
<div class="m2"><p>آنچه می باید مرا صائب به سامان داده اند</p></div></div>