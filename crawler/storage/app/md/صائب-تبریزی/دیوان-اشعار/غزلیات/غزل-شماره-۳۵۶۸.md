---
title: >-
    غزل شمارهٔ ۳۵۶۸
---
# غزل شمارهٔ ۳۵۶۸

<div class="b" id="bn1"><div class="m1"><p>دل دیوانه من قابل زنجیر نبود</p></div>
<div class="m2"><p>ورنه کوتاهی ازان زلف گرهگیر نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمر مردم همه در پرده حیرانی رفت</p></div>
<div class="m2"><p>عالم خاک کم از عالم تصویر نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کار بر نعمت الوان جهان می شد تنگ</p></div>
<div class="m2"><p>اگر از خوردن دل، دیده ما سیر نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وحشت آن به که ز تکرار دوبالا نشود</p></div>
<div class="m2"><p>خواب آشفته ما قابل تعبیر نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داشت تا شرم کرم راه سخن در دیوان</p></div>
<div class="m2"><p>عذر هرگز به پذیرایی تقصیر نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر ازان بر خط تسلیم نهادیم که عشق</p></div>
<div class="m2"><p>دولتی بود که محتاج به تدبیر نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق برداشت ز کوچکدلی از خاک مرا</p></div>
<div class="m2"><p>ورنه ویرانه من قابل تعمیر نبود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی تو گر روی به محراب نماز آوردم</p></div>
<div class="m2"><p>چون کمانخانه ابروی تو بی تیر نبود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شرح خط پیچ و خمی چند به گفتار افزود</p></div>
<div class="m2"><p>نقطه خال تو محتاج به تفسیر نبود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خشکی طالع ما سد سکندر گردید</p></div>
<div class="m2"><p>ورنه پستان نصیب اینهمه بی شیر نبود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ناله اهل جنون بود برون از پرگار</p></div>
<div class="m2"><p>صائب امروز که در حلقه زنجیر نبود</p></div></div>