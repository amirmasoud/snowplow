---
title: >-
    غزل شمارهٔ ۳۰۰۸
---
# غزل شمارهٔ ۳۰۰۸

<div class="b" id="bn1"><div class="m1"><p>برای رزق من گردون عبث تدبیر می سازد</p></div>
<div class="m2"><p>که دل خوردن مرا از زندگانی سیر می سازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زآهو تا جدا شد نافه چون دستار شد مویش</p></div>
<div class="m2"><p>غریبی در جوانی آدمی را پیر می سازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا بس در میان دور گردان این سرافرازی</p></div>
<div class="m2"><p>که مکتوب مرا جانان نشان تیر می سازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خموشی خوب می گوید جواب هرزه گویان را</p></div>
<div class="m2"><p>نسیم بی ادب را غنچه تصویر می سازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خرد شهری است از وحشی نژادان می کند وحشت</p></div>
<div class="m2"><p>بیابانی است سودا با پلنگ و شیر می سازد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل تدبیرهای بی ثمر باشد پشیمانی</p></div>
<div class="m2"><p>نگیرد لب به دندان هر که با تقدیر می سازد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم آوازی چو باشد نعره واری نیست تا منزل</p></div>
<div class="m2"><p>من دیوانه را همراهی زنجیر می سازد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنین گر فکر زلفش می دواند ریشه در جانم</p></div>
<div class="m2"><p>رگ سودا سرم را خامه تصویر می سازد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زبس دلها نیامیزد به هم صائب عجب دارم</p></div>
<div class="m2"><p>که چون در روزگار ما شکر با شیر می سازد؟</p></div></div>