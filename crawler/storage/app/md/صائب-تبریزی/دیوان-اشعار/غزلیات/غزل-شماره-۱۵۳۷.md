---
title: >-
    غزل شمارهٔ ۱۵۳۷
---
# غزل شمارهٔ ۱۵۳۷

<div class="b" id="bn1"><div class="m1"><p>موج خط حلقه بر آن عارض گلگون زده است</p></div>
<div class="m2"><p>جوهر از آینه حسن تو بیرون زده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خط مشکین تو بسیار به خود پیچیده است</p></div>
<div class="m2"><p>تا بر آن عارض گلرنگ شبیخون زده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی نیازست ز خلق آن که رسیده است به حق</p></div>
<div class="m2"><p>فارغ از لفظ بود هر که به مضمون زده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داغم از لاله که از صبح ازل کاسه خویش</p></div>
<div class="m2"><p>از دل خاک برآورده و در خون زده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پرده چشم غزال است سیه خانه او</p></div>
<div class="m2"><p>آن پریزاد که راه دل مجنون زده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>موج دریای ملال است مه عید فلک</p></div>
<div class="m2"><p>پی این نعل مگیرید که وارون زده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا قیامت دهد از سلطنت مجنون یاد</p></div>
<div class="m2"><p>سکه داغ که بر لاله هامون زده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عزت داغ جنون دار که فرمانده عقل</p></div>
<div class="m2"><p>بوسه از دور بر این مهر همایون زده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می شمارند کنون بیخبران باد سموم</p></div>
<div class="m2"><p>از جگر هر نفس گرم که مجنون زده است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست در وادی مجنون اثر از نقش سراب</p></div>
<div class="m2"><p>موج بیتابی عشق است که بیرون زده است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیست یک جلوه کم از شاهد معنی صائب</p></div>
<div class="m2"><p>که ره فاخته یک مصرع موزون زده است</p></div></div>