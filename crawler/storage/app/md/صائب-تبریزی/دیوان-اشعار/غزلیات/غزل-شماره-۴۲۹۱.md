---
title: >-
    غزل شمارهٔ ۴۲۹۱
---
# غزل شمارهٔ ۴۲۹۱

<div class="b" id="bn1"><div class="m1"><p>تسکین دل به شور محبت نمی‌شود</p></div>
<div class="m2"><p>این داغ، خوش‌نمک به قیامت نمی‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لیلی عنان گسسته به صحرا نهاد روی</p></div>
<div class="m2"><p>تمکین حریف جذب محبت نمی‌شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در اشک تلخ نیست کمی دیده مرا</p></div>
<div class="m2"><p>دستم دچار دامن فرصت نمی‌شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در کوهسار شورش سیل است بیشتر</p></div>
<div class="m2"><p>اصلاح ما به سنگ ملامت نمی‌شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فانوس شمع را نتواند نهفته داشت</p></div>
<div class="m2"><p>چشم حسود پرده شهرت نمی‌شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مسند به روی دست سلیمان فکند مور</p></div>
<div class="m2"><p>خواری نصیب اهل قناعت نمی‌شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از آرزوست خواب پریشان دل تمام</p></div>
<div class="m2"><p>تا نقش هست آینه خلوت نمی‌شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از آب تیغ دانه ما سبز می‌شود</p></div>
<div class="m2"><p>قطع امید ما به شهادت نمی‌شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از تاک زور باده کجی را برون نبرد</p></div>
<div class="m2"><p>هموار بدگهر به نصیحت نمی‌شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدار گشت سبزه خوابیده از سحاب</p></div>
<div class="m2"><p>از می علاج زنگ کدورت نمی‌شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شمعی که دیده‌هاست سرانجام خامشی</p></div>
<div class="m2"><p>منت‌پذیر دست حمایت نمی‌شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صائب ز خود برآی که حسن سخن، غریب</p></div>
<div class="m2"><p>بی‌خاکمال وادی غربت نمی‌شود</p></div></div>