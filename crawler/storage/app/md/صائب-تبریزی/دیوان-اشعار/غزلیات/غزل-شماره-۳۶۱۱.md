---
title: >-
    غزل شمارهٔ ۳۶۱۱
---
# غزل شمارهٔ ۳۶۱۱

<div class="b" id="bn1"><div class="m1"><p>تن حجاب سفر جان هوایی نشود</p></div>
<div class="m2"><p>سیر شبنم گره از آبله پایی نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عندلیبی که شکایت کند از دام و قفس</p></div>
<div class="m2"><p>نیست ممکن که گرفتار رهایی نشود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هردو عالم به نظر هیچ بود مستان را</p></div>
<div class="m2"><p>طاعت اهل خرابات ریایی نشود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برد آرام مرا چشم پریشان نظرش</p></div>
<div class="m2"><p>هیچ کافر هدف تیر هوایی نشود!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می کشد سلسله موج به دریا صائب</p></div>
<div class="m2"><p>عشق مخلوق محال است خدایی نشود</p></div></div>