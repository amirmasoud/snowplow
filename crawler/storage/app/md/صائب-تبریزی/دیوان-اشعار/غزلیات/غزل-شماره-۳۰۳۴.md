---
title: >-
    غزل شمارهٔ ۳۰۳۴
---
# غزل شمارهٔ ۳۰۳۴

<div class="b" id="bn1"><div class="m1"><p>زدین ناقصم از سبحه استغفار برخیزد</p></div>
<div class="m2"><p>زننگ کفر من مو بر تن زنار برخیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگیر از آتش سوزنده تعلیم سبکروحی</p></div>
<div class="m2"><p>که با آن سرکشی در پیش پای خار برخیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خود چون مار می پیچم زرشک زلف، کی باشد</p></div>
<div class="m2"><p>که این ابر سیه زان دامن گلزار برخیزد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر وصف سرزلف تو در طومار بنویسم</p></div>
<div class="m2"><p>چو شمع کشته دودم از سر طومار برخیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنین کافتادم از طاق دل نشو و نما، مشکل</p></div>
<div class="m2"><p>که مو از پیکرم چون کاه از دیوار برخیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عبث صیقل عرق می ریزد از بهر جلای من</p></div>
<div class="m2"><p>عجب دارم که از آیینه ام زنگار برخیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پی طرف کلاهش لاله دارد نعل در آتش</p></div>
<div class="m2"><p>زخواب ناز گل از شوق آن دستار برخیزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زطرز تازه صائب داغ داری نکته سنجان را</p></div>
<div class="m2"><p>عجب دارم کز آمل چون تو خوش گفتار برخیزد</p></div></div>