---
title: >-
    غزل شمارهٔ ۲۱۷
---
# غزل شمارهٔ ۲۱۷

<div class="b" id="bn1"><div class="m1"><p>نیست از داغ جنون پروا دل غم پیشه را</p></div>
<div class="m2"><p>دیده شیرست کرم شبچراغ این بیشه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راز عشق از دل تراوش می کند بی اختیار</p></div>
<div class="m2"><p>این شراب برق جولان می گدازد شیشه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیر را طول امل بیش از جوان پیچید به هم</p></div>
<div class="m2"><p>می کند مطلق عنان خاک ملایم ریشه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست غافل عشق بی پروا ز مرگ کوهکن</p></div>
<div class="m2"><p>نقش شیرین می کند شیرین دهان تیشه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صائب از اندیشه موی میان غافل مباش</p></div>
<div class="m2"><p>کاین ره باریک نازک می کند اندیشه را</p></div></div>