---
title: >-
    غزل شمارهٔ ۴۶۲۰
---
# غزل شمارهٔ ۴۶۲۰

<div class="b" id="bn1"><div class="m1"><p>لاله ها چشم غزالان می نماید در نظر</p></div>
<div class="m2"><p>خارها صفهای مژگان می نماید در نظر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر غباری کز زمین خیزد در این آب و هوا</p></div>
<div class="m2"><p>سرو سیم اندام جولان می نماید در نظر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خروش بلبلان و جوش گلها صحن باغ</p></div>
<div class="m2"><p>مجلس پر شور مستان می نماید در نظر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ظاهر سنگ از هجوم لاله های آبدار</p></div>
<div class="m2"><p>باطن کان بدخشان می نماید درنظر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خنده گل خاک را یک روی خندان کرده است</p></div>
<div class="m2"><p>آسمان یک چشم حیران می نماید درنظر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از رگ ابر بهاران آسمان تلخروی</p></div>
<div class="m2"><p>خوشتر از زلف پریشان می نماید در نظر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک دهن خنده است صحرا از نشاط نوبهار</p></div>
<div class="m2"><p>چون کواکب ریگ خندان می نماید در نظر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا که زلف افشاند بر صحرا که از موج سراب</p></div>
<div class="m2"><p>روی صحرا سنبلستان می نماید در نظر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از هجوم داغ، هر زخم نمایان بر تنم</p></div>
<div class="m2"><p>رخنه دیوار بستان می نماید در نظر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چشم تنگ مور از تنگی دل تنگ مرا</p></div>
<div class="m2"><p>عرصه ملک سلیمان می نماید در نظر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شوخ چشمی را که شوخی سر به صحرا می دهد</p></div>
<div class="m2"><p>خلوت آیینه زندان می نماید در نظر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بس که شور عشق پیچیده است در پیکر مرا</p></div>
<div class="m2"><p>داغها صائب نمکدان می نماید در نظر</p></div></div>