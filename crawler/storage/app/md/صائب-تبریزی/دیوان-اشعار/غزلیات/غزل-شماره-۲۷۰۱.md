---
title: >-
    غزل شمارهٔ ۲۷۰۱
---
# غزل شمارهٔ ۲۷۰۱

<div class="b" id="bn1"><div class="m1"><p>دل خراب از خندهٔ پنهان آن گل می‌شود</p></div>
<div class="m2"><p>سنگ این مینای خالی پرتو مل می‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساحل دریای آشوب است ترک اختیار</p></div>
<div class="m2"><p>موج بر خاشاک از افتادگی پل می‌شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در طریق ما که نعل واژگون خضر ره است</p></div>
<div class="m2"><p>بیشتر خون بر سر تیغ تغافل می‌شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیل را کوتاهی دیوار عاجز می‌کند</p></div>
<div class="m2"><p>سد راه دشمن غالب تحمل می‌شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پاک گردد هر که صائب دامن پاکان گرفت</p></div>
<div class="m2"><p>اشک شبنم سرخ‌رو از دامن گل می‌شود</p></div></div>