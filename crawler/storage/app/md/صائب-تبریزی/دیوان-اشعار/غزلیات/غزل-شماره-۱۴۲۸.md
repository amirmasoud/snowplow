---
title: >-
    غزل شمارهٔ ۱۴۲۸
---
# غزل شمارهٔ ۱۴۲۸

<div class="b" id="bn1"><div class="m1"><p>از زمین اوج گرفته است غباری که مراست</p></div>
<div class="m2"><p>ایمن از سیلی موج است کناری که مراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم پوشیده ام از هر چه درین عالم هست</p></div>
<div class="m2"><p>چه کند سیل حوادث به حصاری که مراست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کار زنگار کند با دل چون آینه ام</p></div>
<div class="m2"><p>گر چه هست از دگران نقش و نگاری که مراست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست ممکن که مرا پاک نسازد از عیب</p></div>
<div class="m2"><p>از سر زانوی خود آینه داری که مراست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان غربت زده را زود به پابوس وطن</p></div>
<div class="m2"><p>می رساند نفس برق سواری که مراست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دارد از گلشن فردوس مرا مستغنی</p></div>
<div class="m2"><p>زیر بال و پر خود باغ و بهاری که مراست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست از خاک گرانسنگ به دل قارون را</p></div>
<div class="m2"><p>بر دل از رهگذر جسم غباری که مراست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خضر را می کند از چشمه حیوان دلسرد</p></div>
<div class="m2"><p>در سراپرده شب آب خماری که مراست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ید بیضا سیهی از دل فرعون نبرد</p></div>
<div class="m2"><p>صبح روشن چه کند با شب تاری که مراست؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حیف و صد حیف که از قحط جگرسوختگان</p></div>
<div class="m2"><p>در دل سنگ شد افسرده، شراری که مراست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گل بی خار ز خار سر دیوار شکفت</p></div>
<div class="m2"><p>تا چها گل کند از بوته خاری که مراست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می کنم خوش دل خود را به تمنای وصال</p></div>
<div class="m2"><p>سایه مرغ هوایی است شکاری که مراست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نیست در عالم ایجاد فضایی صائب</p></div>
<div class="m2"><p>که نفس راست کند مشت غباری که مراست</p></div></div>