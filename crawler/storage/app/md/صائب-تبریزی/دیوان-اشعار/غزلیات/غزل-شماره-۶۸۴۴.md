---
title: >-
    غزل شمارهٔ ۶۸۴۴
---
# غزل شمارهٔ ۶۸۴۴

<div class="b" id="bn1"><div class="m1"><p>باش در ذکر خدا دایم اگر جویایی</p></div>
<div class="m2"><p>کاین براقی است که تا عرش ناستد جایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پای من بر سر گنج است ز جمعیت دل</p></div>
<div class="m2"><p>نیست در دستم اگر چون دگران دنیایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لاله را نعل بود بر سر آتش در کوه</p></div>
<div class="m2"><p>در ره سیل حوادث تو چه پا برجایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم خفاش ز خورشید ندارد قسمت</p></div>
<div class="m2"><p>ورنه در دیده روشن گهران پیدایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طوق هر فاخته ای دیده حیرت زده ای است</p></div>
<div class="m2"><p>در ریاضی که بود سرو سهی بالایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم کوته نظر آیینه ظاهربین است</p></div>
<div class="m2"><p>ورنه در سینه هر قطره بود دریایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب از هر دو جهان قطع نظر آسان است</p></div>
<div class="m2"><p>اگر از جانب معشوق بود ایمایی</p></div></div>