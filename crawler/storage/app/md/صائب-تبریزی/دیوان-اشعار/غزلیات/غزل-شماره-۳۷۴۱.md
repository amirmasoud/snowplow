---
title: >-
    غزل شمارهٔ ۳۷۴۱
---
# غزل شمارهٔ ۳۷۴۱

<div class="b" id="bn1"><div class="m1"><p>درین محیط چو غواص هرکه ته دارد</p></div>
<div class="m2"><p>چو موج به که سر رشته را نگه دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شراب روز دل لاله را سیه دارد</p></div>
<div class="m2"><p>چه حاجت است به شاهد سخن چو ته دارد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عنان سیل سبکرو به دست خودرایی است</p></div>
<div class="m2"><p>پیادگی است که اندازه را نگه دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشو مقید رهبر، قدم به راه گذار</p></div>
<div class="m2"><p>که شش جهت به خرابات عشق ره دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز دار و گیر خرد فارغند بی مغزان</p></div>
<div class="m2"><p>ز سر گذشته چه پروای پادشه دارد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلیل منزل آزادگان سبکباری است</p></div>
<div class="m2"><p>به منزلی نرسد هرکه زاد ره دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برآورد ز گریبان رستگاری سر</p></div>
<div class="m2"><p>کسی که مرتبه از خجلت گنه دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به بحر غور سخن گر فرو توانی رفت</p></div>
<div class="m2"><p>بدانی این سخنان بلند ته دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر عبیر شود مغز من شگفت مدان</p></div>
<div class="m2"><p>نسیم زلف دماغ مرا تبه دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو آفتاب بکش جام آتشین بر سر</p></div>
<div class="m2"><p>که از خمار، عذار تو رنگ مه دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر به غور معانی رسیده ای صائب</p></div>
<div class="m2"><p>ازین غزل مگذر سرسری که ته دارد</p></div></div>