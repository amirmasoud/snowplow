---
title: >-
    غزل شمارهٔ ۳۸۸۳
---
# غزل شمارهٔ ۳۸۸۳

<div class="b" id="bn1"><div class="m1"><p>دهان تنگ تو هر خرده‌دان نمی‌داند</p></div>
<div class="m2"><p>که غیب را به جز از غیب‌دان نمی‌داند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگرچه گام نخستین گذشتم از دو جهان</p></div>
<div class="m2"><p>هنوز شوق مرا خوش عنان نمی‌داند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی که نیست تُنُک‌مایه از شعور و خرد</p></div>
<div class="m2"><p>به هر بها که بود می گران نمی‌داند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفس گداخته خود را به گلستان برسان</p></div>
<div class="m2"><p>که ایستادگی این کاروان نمی‌داند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ملایمت سپر انقلاب دوران است</p></div>
<div class="m2"><p>که نخل موم بهار و خزان نمی‌داند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به نام بلبل من گرچه باغ شد مشهور</p></div>
<div class="m2"><p>هنوز نام مرا باغبان نمی‌داند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به داغ عشق بسوز و بساز چون مردان</p></div>
<div class="m2"><p>که آفتاب قیامت امان نمی‌داند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوشند اهل سعادت به سختی از دنیا</p></div>
<div class="m2"><p>هما ز مایده جز استخوان نمی‌داند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عجب که برخورد از روزگار پیری هم</p></div>
<div class="m2"><p>چنین که قدر جوانی جوان نمی‌داند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز پوست راه به مغز ندیده نتوان برد</p></div>
<div class="m2"><p>طبیب حال دل ناتوان نمی‌داند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نشد به رخنه دل هرکه آشنا صائب</p></div>
<div class="m2"><p>ره برون شد ازین خاکدان نمی‌داند</p></div></div>