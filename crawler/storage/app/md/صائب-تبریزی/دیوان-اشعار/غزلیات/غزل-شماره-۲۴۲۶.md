---
title: >-
    غزل شمارهٔ ۲۴۲۶
---
# غزل شمارهٔ ۲۴۲۶

<div class="b" id="bn1"><div class="m1"><p>تا به خط از زلف کار دل فتاد آسوده شد</p></div>
<div class="m2"><p>راهرو آسوده گردد راه چون پیموده شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چهره خندان او تا در گلستان جلوه کرد</p></div>
<div class="m2"><p>بلبلان را در نظر گل چهره نگشوده شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صحبت زاهد مرا خاموش کرد از حرف عشق</p></div>
<div class="m2"><p>طوطی من لال ازین آیینه نزدوده شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می کند روشن سواد مردم از نقش قدم</p></div>
<div class="m2"><p>چون قلم پایی که در راه سخن فرسوده شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود خار پیرهن امید سرسبزی مرا</p></div>
<div class="m2"><p>تخم من تا سوخت در زیرزمین آسوده شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی نظر بستن میسر نیست زین زندان نجات</p></div>
<div class="m2"><p>فتح بابی هر که را شد زین درنگشوده شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می تراود شکوه خونینم از تیغ زبان</p></div>
<div class="m2"><p>گرچه دندانم زنعمت خوارگی فرسوده شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در گشاد کار من هر کس سری در جیب برد</p></div>
<div class="m2"><p>عقده ای دیگر به کار مشکلم افزوده شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شمع را در خواب خواهد دید باد صبحدم</p></div>
<div class="m2"><p>گر چنین خاکستر پروانه خواهد توده شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواب منزل رهنوردان را دلیل غفلت است</p></div>
<div class="m2"><p>خواب بر من تلخ شد تا راه من پیموده شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خجلت لب باز کردن پیش نیسان سهل نیست</p></div>
<div class="m2"><p>آب روی من چو گوهر در صدف پالوده شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اشک شادی زود می سازد مرا پاک از گناه</p></div>
<div class="m2"><p>دامن تیغش به خون من اگر آلوده شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>غیرت مردانه من بر نتابد کاهلی</p></div>
<div class="m2"><p>کارفرما گشت هر کاری به من فرموده شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون مگس طی شد به دست و پا زدن اوقات من</p></div>
<div class="m2"><p>تا به شهد زندگی بال و پرم آلوده شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>می توان از جوش خون گل یکایک را شنید</p></div>
<div class="m2"><p>گر به ظاهر ناله های زار من نشنوده شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شد مخطط آستان او ز خط سرنوشت</p></div>
<div class="m2"><p>بس که پیشانی به خاک آستانش سوده شد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سر نپیچیدم ز تیغ موج تا همچون حباب</p></div>
<div class="m2"><p>چشم من بر روی دریای بقا بگشوده شد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صائب از فیض ندامت کار من بالا گرفت</p></div>
<div class="m2"><p>شهپر توفیقم آخر دست بر هم سوده شد</p></div></div>