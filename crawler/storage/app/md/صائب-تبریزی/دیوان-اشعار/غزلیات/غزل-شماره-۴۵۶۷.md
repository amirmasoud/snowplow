---
title: >-
    غزل شمارهٔ ۴۵۶۷
---
# غزل شمارهٔ ۴۵۶۷

<div class="b" id="bn1"><div class="m1"><p>از زمین برخاستن چشم از زمین داران مدار</p></div>
<div class="m2"><p>راست گردیدن توقع زین گرانباران مدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسن بیتاب است در اظهار راز عاشقان</p></div>
<div class="m2"><p>پرده پوشی چشم ازین آیینه رخساران مدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون علم شد سرنگون لشکر پریشان می شود</p></div>
<div class="m2"><p>پای چون لغزد امید از هواداران مدار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خزان از عندلیبان بانگ افسوسی نخاست</p></div>
<div class="m2"><p>چون ورق بر گشت چشم یاری از یاران مدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مردم بیدرد را پروای اهل درد نیست</p></div>
<div class="m2"><p>مهربانی چشم زنهاراز پرستاران مدار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خانه آب و گل از سیلاب می لرزد به خویش</p></div>
<div class="m2"><p>چون شدی از خانه بردوشان غم باران مدار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کاروان عمر رانعل سفردرآتش است</p></div>
<div class="m2"><p>ایستادن چشم ازین سیلاب رفتاران مدار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سد راه نشأه می می شود چین جبین</p></div>
<div class="m2"><p>روترش زنهار در بزم قدح خواران مدار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جز ندامت نیست حاصل دانه بی مغز را</p></div>
<div class="m2"><p>گوش بر افسانه بیهوده گفتاران مدار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حرف دل صائب مکن سر پیش ارباب هوس</p></div>
<div class="m2"><p>زینهار آیینه پیش این سیه کاران مدار</p></div></div>