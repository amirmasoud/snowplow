---
title: >-
    غزل شمارهٔ ۳۳۹۹
---
# غزل شمارهٔ ۳۳۹۹

<div class="b" id="bn1"><div class="m1"><p>اشک گرمم جگر وادی محشر سوزد</p></div>
<div class="m2"><p>داغ تبخال به کنج لب کوثر سوزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آستین دست ندارد به چراغ گل داغ</p></div>
<div class="m2"><p>این چراغی است که تا دامن محشر سوزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آتش عشق ز خاکستر هندست بلند</p></div>
<div class="m2"><p>زن درین شعله ستان بر سر شوهر سوزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از می این چهره که امروز تو افروخته ای</p></div>
<div class="m2"><p>گر کنی باد زن از بال سمندر، سوزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از کلاه نمدی دود کند اخگر عشق</p></div>
<div class="m2"><p>این نه عودی است که در مجمر افسر سوزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به که سر بر سر بالین سلامت بنهم</p></div>
<div class="m2"><p>چند از پهلوی من سینه بستر سوزد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از چه برده است نواهای ملال انگیزت؟</p></div>
<div class="m2"><p>که بر افغان تو صائب دل کافر سوزد</p></div></div>