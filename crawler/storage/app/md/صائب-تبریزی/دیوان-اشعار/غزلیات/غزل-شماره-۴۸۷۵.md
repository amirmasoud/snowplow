---
title: >-
    غزل شمارهٔ ۴۸۷۵
---
# غزل شمارهٔ ۴۸۷۵

<div class="b" id="bn1"><div class="m1"><p>چون ترا مسکن میسر شد تزیین مباش</p></div>
<div class="m2"><p>تخته کز دریا ترا بیرون برد رنگین مباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده بد می کشد رنگین لباسان رابه خون</p></div>
<div class="m2"><p>شمع مارا جامه فانوس گو رنگین مباش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>افسر زرین سر خورشید رادر کارنیست</p></div>
<div class="m2"><p>داغدار عشق راگو شمع بربالین مباش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تخم قابل در زمین پاک گوهر می شود</p></div>
<div class="m2"><p>وقت صبح صافدل بی گریه خونین مباش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برگریزان کرم رانوبهاران درقفاست</p></div>
<div class="m2"><p>تاگلی در باغ داری مانع گلچین مباش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پرده بینش نگردد موشکافان رالباس</p></div>
<div class="m2"><p>همچو شیادان نهان در خرقه پشمین مباش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از گرانقدری است هر مطلب که دیر آید به دست</p></div>
<div class="m2"><p>از تهی بر گشتن دست دعا غمگین مباش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست در بند زر و آهن تفاوت ،زینهار</p></div>
<div class="m2"><p>تامیسر می شود در قید مهر و کین مباش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر طمع داری که صائب ازخدابینان شوی</p></div>
<div class="m2"><p>خودپسند و خود نما و خود سرو خودبین مباش</p></div></div>