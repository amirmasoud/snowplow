---
title: >-
    غزل شمارهٔ ۱۵۰۹
---
# غزل شمارهٔ ۱۵۰۹

<div class="b" id="bn1"><div class="m1"><p>عقل نخلی است خزان دیده که ماتم با اوست</p></div>
<div class="m2"><p>عشق سروی است که سرسبزی عالم با اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که در معرکه با جوهر ذاتی چون تیغ</p></div>
<div class="m2"><p>روزگارش به خموشی گذرد، دم با اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاصیی را که سروکار به دوزخ باشد</p></div>
<div class="m2"><p>در بهشت است، اگر دیده پر نم با اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل سودازده را وصل نیاورد به حال</p></div>
<div class="m2"><p>چه کند عید به آن کس که محرم با اوست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل هر کس که در آن زلف پریشان آویخت</p></div>
<div class="m2"><p>می توان گفت که سررشته عالم با اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که زد مهر خموشی به لب چون و چرا</p></div>
<div class="m2"><p>گر چه مورست درین دایره خاتم با اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمک عشق به بی درد رام است حرام</p></div>
<div class="m2"><p>جای رحم است بر آن زخم که مرهم با اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با غم عشق غم عالم فانی هیچ است</p></div>
<div class="m2"><p>غم عالم نخورد هر که همین غم با اوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که چون سوزن عریان مژه بر هم نزند</p></div>
<div class="m2"><p>می توان یافت که سررشته عالم با اوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صیقل آینه حسن بود دیده پاک</p></div>
<div class="m2"><p>روی گل تازه ازان است که شبنم با اوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر که صائب ز بد خویش پشیمان نشود</p></div>
<div class="m2"><p>تخم دیوست اگر صورت آدم با اوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر که صائب نکشد در دل خود آتش حرص</p></div>
<div class="m2"><p>گر چه در باغ بهشت است جهنم با اوست</p></div></div>