---
title: >-
    غزل شمارهٔ ۷۵۰
---
# غزل شمارهٔ ۷۵۰

<div class="b" id="bn1"><div class="m1"><p>بشنو ز من ترانه غیرت فزای را</p></div>
<div class="m2"><p>گر مردی ای سپند، نگه دار جای را!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سختی پذیر باش گر اهل سعادتی</p></div>
<div class="m2"><p>کز استخوان گزیر نباشد همای را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چند سر به دامن محمل گذاشته است</p></div>
<div class="m2"><p>دل می تپد همان ز جدایی درای را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند ای سیه درون خود آرا درین بساط</p></div>
<div class="m2"><p>پنهان کنی به بال و پر خویش پای را؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روشن ضمیر باش که این بال آتشین</p></div>
<div class="m2"><p>بر چرخ برد شبنم بی دست و پای را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جمعی که از ملایمت آزار دیده اند</p></div>
<div class="m2"><p>بر برگ گل شمرده گذارند پای را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدطینتان برای شکم خون هم خورند</p></div>
<div class="m2"><p>سگ دشمن است بر سر روزی گدای را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب به غور ناله عشاق می رسد</p></div>
<div class="m2"><p>در راه فکر هر که فشرده است پای را</p></div></div>