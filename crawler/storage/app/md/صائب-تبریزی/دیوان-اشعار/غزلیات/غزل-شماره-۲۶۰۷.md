---
title: >-
    غزل شمارهٔ ۲۶۰۷
---
# غزل شمارهٔ ۲۶۰۷

<div class="b" id="bn1"><div class="m1"><p>داغ ناسور مرا گر بر دل صحرا نهند</p></div>
<div class="m2"><p>از خجالت لاله ها بر کوه پا بالا نهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از لب پیمانه ها خیزد نوای العطش</p></div>
<div class="m2"><p>پنبه مغز مرا گر بر سر مینا نهند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این عزیزانی که من در مصر دولت دیده ام</p></div>
<div class="m2"><p>در ترازو جنس یوسف را به استغنا نهند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشتی جمعی که دارد از توکل بادبان</p></div>
<div class="m2"><p>بیشتر در غیر موسم روی در دریا نهند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پرتو رویش چنین گر مجلس افروزی کند</p></div>
<div class="m2"><p>زود باشد شمعها سر را به جای پا نهند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غنچه خسبانی که سر پیچیده اند از روزگار</p></div>
<div class="m2"><p>سر چو صائب بر سر زانوی استغنا نهند</p></div></div>