---
title: >-
    غزل شمارهٔ ۳۴۸۷
---
# غزل شمارهٔ ۳۴۸۷

<div class="b" id="bn1"><div class="m1"><p>محض حرف است که او را دهنی ساخته‌اند</p></div>
<div class="m2"><p>در میان نیست دهانی، سخنی ساخته‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل روشن‌گهران فلکی آب شده است</p></div>
<div class="m2"><p>تا چو تو دلبر سیمین‌بدنی ساخته‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آب ده چشمی از آن سیب زنخدان که فلک</p></div>
<div class="m2"><p>دورها کرده که سیب ذقنی ساخته‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گنج در گوشه ویرانه جمعی فرش است</p></div>
<div class="m2"><p>کز زر و سیم به سیمین‌بدنی ساخته‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان غباری که خط از لعل تو انگیخته است</p></div>
<div class="m2"><p>هر طرف طوطی شکرسخنی ساخته‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زلف مشکین تو بر دامن صحرای وجود</p></div>
<div class="m2"><p>سایه افکنده، ختا و ختنی ساخته‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در دل سنگ صنم قحط شرار افتاده است</p></div>
<div class="m2"><p>تا به سرگرمی من برهمنی ساخته‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان شراری که گرفته است هوا زآتش گل</p></div>
<div class="m2"><p>هر طرف بلبل رنگین‌سخنی ساخته‌اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جای شکرست که غم‌های گران‌مایه تو</p></div>
<div class="m2"><p>با دل سوخته همچو منی ساخته‌اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نقطه و دایره و قطره و دریاست یکی</p></div>
<div class="m2"><p>خودپرستان جهان ما و منی ساخته‌اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آه کاین مرده‌دلان جامه احرامی صبح</p></div>
<div class="m2"><p>بر تن خویش ز غفلت کفنی ساخته‌اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فارغ از فکر لباسند نظردوختگان</p></div>
<div class="m2"><p>چون حباب از تن خود پیرهنی ساخته‌اند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عارفان از نظر پاک، چو شبنم صائب</p></div>
<div class="m2"><p>زنگ آیینه دل را چمنی ساخته‌اند</p></div></div>