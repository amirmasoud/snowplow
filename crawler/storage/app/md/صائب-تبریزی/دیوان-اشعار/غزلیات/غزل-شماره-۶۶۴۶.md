---
title: >-
    غزل شمارهٔ ۶۶۴۶
---
# غزل شمارهٔ ۶۶۴۶

<div class="b" id="bn1"><div class="m1"><p>مباش از سخن سخت در شکست پیاله</p></div>
<div class="m2"><p>که بهتر از ید بیضاست پشت دست پیاله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار شیشه تقوی خورد به سنگ ملامت</p></div>
<div class="m2"><p>چو گرم عشوه شود چشم نیم مست پیاله</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چرا تراود ازو صد هزار سجده رنگین؟</p></div>
<div class="m2"><p>اگر صراحی می نیست پای بست پیاله</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز هوش ناقص ما بیدلان چه کار گشاید؟</p></div>
<div class="m2"><p>که عقل می خورد امروز روی دست پیاله</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به غیر سینه صائب به هیچ جا ننشیند</p></div>
<div class="m2"><p>خدنگ غمزه چو بیرون جهد ز شست پیاله</p></div></div>