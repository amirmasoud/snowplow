---
title: >-
    غزل شمارهٔ ۵۲۱۸
---
# غزل شمارهٔ ۵۲۱۸

<div class="b" id="bn1"><div class="m1"><p>دل را نکند گریه ز اندوه جهان پاک</p></div>
<div class="m2"><p>ازداغ به باران نشود لاله ستان پاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از پرتو خورشید دلم داغ و کباب است</p></div>
<div class="m2"><p>کآمد به جهان پاک وبرون شد ز جهان پاک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیلاب حوادث شود افسانه خوابش</p></div>
<div class="m2"><p>آن را که بود خانه ز اسباب جهان پاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون تیر هدف رانکنی دست درآغوش</p></div>
<div class="m2"><p>تا خانه خود رانکنی همچو کمان پاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در حوصله اش قطره شود گوهر شهوار</p></div>
<div class="m2"><p>آن را که بود همچو صدف کام و دهان پاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر غوطه به دریا دهیش پاک نگردد</p></div>
<div class="m2"><p>هرکس که نشد ازنظر پیر مغان پاک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سر زدم از بس که بیطاقتی شوق</p></div>
<div class="m2"><p>شد بادیه عشق تو از سنگ نشان پاک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خون می خورم از غیرت آن تیغ که کرده است</p></div>
<div class="m2"><p>از صفحه رخسار تو خط را به زبان پاک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خون سنگ که از پرتو خورشید شود لعل</p></div>
<div class="m2"><p>از عشق مراگشت دل و جان و زبان پاک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در هیچ دلی نیست غم رزق نباشد</p></div>
<div class="m2"><p>صائب نشد این سفره ز اندیشه نان پاک</p></div></div>