---
title: >-
    غزل شمارهٔ ۳۴۰
---
# غزل شمارهٔ ۳۴۰

<div class="b" id="bn1"><div class="m1"><p>نگردید آتشین رخساره ای فریادرس ما را</p></div>
<div class="m2"><p>مگر از شعله آواز درگیرد قفس ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بی دردی به درد ما نپردازند غمخواران</p></div>
<div class="m2"><p>همین آیینه می گیرد خبر، گاه از نفس ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نچیدم از گرفتاری گلی هر چند از خواری</p></div>
<div class="m2"><p>ز زخم خار و خس دست حمایت شد قفس ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر چه پنجه ما را، ز نرمی موم می تابد</p></div>
<div class="m2"><p>زبان آهنین در ناله باشد چون جرس ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حلاوت برده بود از زندگی آمیزش مردم</p></div>
<div class="m2"><p>ترشرویی حصاری گشت از مور و مگس ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گیاه تشنه ما سنگ را در دل آب می سازد</p></div>
<div class="m2"><p>به پای خم برد از گوشه زندان عسس ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به مهر خامشی غواص ما امیدها دارد</p></div>
<div class="m2"><p>به گوهر می رساند زود، جان بی نفس ما را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به مردن برنیاید ریشه طول امل از دل</p></div>
<div class="m2"><p>رهایی نیست زیر خاک چون سگ زین مرس ما را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به است از باغ بی گل، گوشه زندان ناکامی</p></div>
<div class="m2"><p>در ایام خزان بیرون میاور از قفس ما را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بهار از غنچه منقار ما برگ و نوا گیرد</p></div>
<div class="m2"><p>به زر چون غنچه گل گر نباشد دسترس ما را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به مهر خامشی کردیم صلح از گفتگو صائب</p></div>
<div class="m2"><p>غباری بر دل آیینه ننشت از نفس ما را</p></div></div>