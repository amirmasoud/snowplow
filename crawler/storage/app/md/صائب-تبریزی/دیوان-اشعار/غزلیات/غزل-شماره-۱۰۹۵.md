---
title: >-
    غزل شمارهٔ ۱۰۹۵
---
# غزل شمارهٔ ۱۰۹۵

<div class="b" id="bn1"><div class="m1"><p>آفتاب آتشین رخسار، داغ حسن اوست</p></div>
<div class="m2"><p>شمع یک پروانه پای چراغ حسن اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داروی بیهوشی ارباب بینش گشته است</p></div>
<div class="m2"><p>گر چه خط عنبرین درد ایاغ حسن اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چه از خط آفتابش روی در زردی گذاشت</p></div>
<div class="m2"><p>همچنان ناز بهاران در دماغ حسن اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچ پروایی ندارد از نسیم آه سرد</p></div>
<div class="m2"><p>روغن خورشید گویا در چراغ حسن اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن که مژگانش ترازو می شد از دل خلق را</p></div>
<div class="m2"><p>این زمان خار سر دیوار باغ حسن اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو صائب بلبلی کز نغمه اش خون می چکد</p></div>
<div class="m2"><p>روزگاری شد که در بیرون باغ حسن اوست</p></div></div>