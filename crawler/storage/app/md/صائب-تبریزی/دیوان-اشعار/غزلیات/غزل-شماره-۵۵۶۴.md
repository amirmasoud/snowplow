---
title: >-
    غزل شمارهٔ ۵۵۶۴
---
# غزل شمارهٔ ۵۵۶۴

<div class="b" id="bn1"><div class="m1"><p>ز پند ناصحان بی نمک پرشور شد گوشم</p></div>
<div class="m2"><p>ازین بیهوده گویان خانه زنبور شد گوشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شنیدم پوچ چندان زین سبک مغزان بی حاصل</p></div>
<div class="m2"><p>که پوچ از مغز همچون کاسه طنبور شد گوشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من و هنگامه بیهوده گفتاران معاذالله</p></div>
<div class="m2"><p>که حمام زنان ز آواز پای مور شد گوشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود صد پرده افزون از دهان مار در تلخی</p></div>
<div class="m2"><p>ز گفتار شکر ریز تو تا مهجور شد گوشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز پیغامی که آورد از لب شیرین او قاصد</p></div>
<div class="m2"><p>پر از شهد و شکر چون خانه زنبور شد گوشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمی دانم چها گفت آن بهشتی رو، همین دانم</p></div>
<div class="m2"><p>که چون قصر بهشت جاویدان پر حور شد گوشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرم روشن به چشم خلق چون فانوس می آید</p></div>
<div class="m2"><p>ز گفتار گلوسوز تو تا پر نور شد گوشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز آواز پر جبریل بر هم می خورد و قتم</p></div>
<div class="m2"><p>چو موسی آشنا تا با خطاب طور شد گوشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز گفت و گوی تلخ ناصحان بیدار چون گردم</p></div>
<div class="m2"><p>که از غفلت گرانتر از نوای صور شد گوشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سبک تا پنبه غفلت برون آوردم از مغزش</p></div>
<div class="m2"><p>درین وحدت سرا پر نغمه منصور شد گوشم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شنیدم از شکست آرزو در سینه آوازی</p></div>
<div class="m2"><p>که مستغنی ز ساز چینی فغور شد گوشم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نمی دانم چه خواهد کرد با من ناله بلبل</p></div>
<div class="m2"><p>کز آواز شکست رنگ گل ناسور شد گوشم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کف افسوس بود از بحر چون ساحل نصیب من</p></div>
<div class="m2"><p>از آن لبها ز گوهر چون صدف معمور شد گوشم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شمارم خنده مینای می را نوحه ماتم</p></div>
<div class="m2"><p>ز آواز به دل نزدیک او تا دور شد گوشم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به حرفی از لب میگون خود بشکن خمارم را</p></div>
<div class="m2"><p>که از خمیازه عاجز چون لب مخمور شد گوشم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نگردد تلخ از شور قیامت خواب شیرینم</p></div>
<div class="m2"><p>به زیر پرده غفلت ز بس مستور شد گوشم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نمی دانم چه خواهد بادل مجروح من کردن</p></div>
<div class="m2"><p>نمکدان قیامت زان لب پر شور شد گوشم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرا چون غنچه از بی همدمیها بود دلتنگی</p></div>
<div class="m2"><p>ز گلبانگ هزاران همچو گل مسرور شد گوشم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مشو از حرف عشق ای خامه آتش زبان خامش</p></div>
<div class="m2"><p>کز این روشن بیان فانوس شمع طور شد گوشم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دی خالی ز غیبت در حضورم می توان کردن</p></div>
<div class="m2"><p>نیم غمگین به سنگینی اگر مشهور شد گوشم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز تلقین دم افسرده دلمردگان صائب</p></div>
<div class="m2"><p>غبارآلود ماتم چون دهان گور شد گوشم</p></div></div>