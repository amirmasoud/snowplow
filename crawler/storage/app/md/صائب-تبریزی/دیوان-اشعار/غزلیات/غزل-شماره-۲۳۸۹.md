---
title: >-
    غزل شمارهٔ ۲۳۸۹
---
# غزل شمارهٔ ۲۳۸۹

<div class="b" id="bn1"><div class="m1"><p>عیب‌جو چندان که عیب از ما به در می‌آورد</p></div>
<div class="m2"><p>غیرت ما زور بر کسب هنر می‌آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر گهر در آتش افتد، به که از قیمت فتد</p></div>
<div class="m2"><p>یوسف ما در چه کنعان به سر می‌آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست کوته دار از طول امل کاین شاخسار</p></div>
<div class="m2"><p>چون به بار آید پشیمانی ثمر می‌آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرکه را چون رشته دور چرخ پیچ و تاب داد</p></div>
<div class="m2"><p>سر ز جیب گوهر سیراب برمی‌آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نخل مومین، میوه خورشید بار آورد و ریخت</p></div>
<div class="m2"><p>در چه موسم نخل ما یارب ثمر می‌آورد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آب تیغ او عجب دارم نصیب من شود</p></div>
<div class="m2"><p>طالعی دارم که از دریا خبر می‌آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بخت ما حاضرجوابی از مزاج کوه برد</p></div>
<div class="m2"><p>کی جواب نامهٔ ما نامه‌بر می‌آورد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حُسن در هرجا که باشد چشم‌زخمی لازم است</p></div>
<div class="m2"><p>سوزن از جیب مسیحا سر به در می‌آورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب از تلخی مذاق عیب‌جو رد می‌کند</p></div>
<div class="m2"><p>ابر ما گر آب از جوی گهر می‌آورد</p></div></div>