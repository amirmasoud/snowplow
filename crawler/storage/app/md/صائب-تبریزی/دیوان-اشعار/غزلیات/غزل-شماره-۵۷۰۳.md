---
title: >-
    غزل شمارهٔ ۵۷۰۳
---
# غزل شمارهٔ ۵۷۰۳

<div class="b" id="bn1"><div class="m1"><p>ادب گذشته بر روی یکدیگر دستم</p></div>
<div class="m2"><p>وگرنه همچو صدف نیست بی گهر دستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تهی شود به لبم نارسیده رطل گران</p></div>
<div class="m2"><p>ز بس که ریشه دوانده است رعشه در دستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جدا چودست سبو از سرم نمی گردد</p></div>
<div class="m2"><p>ز بس به فکر تو مانده است زیر سردستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گره زکار دو عالم گشودن آسان است</p></div>
<div class="m2"><p>نمیرود پی این کار مختصر دستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کنون که شمع برون آمده است از فانوس</p></div>
<div class="m2"><p>زبال و پر کف خاکستری است در دستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز آب گوهر من روی عالمی تازه است</p></div>
<div class="m2"><p>چو خاک اگر چه تهی مانده از گهر دستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به فکر مور میانی فتاده ام صائب</p></div>
<div class="m2"><p>عجب رگی ز سخن آمده است در دستم</p></div></div>