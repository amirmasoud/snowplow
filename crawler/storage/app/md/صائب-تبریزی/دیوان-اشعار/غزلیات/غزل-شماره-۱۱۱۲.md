---
title: >-
    غزل شمارهٔ ۱۱۱۲
---
# غزل شمارهٔ ۱۱۱۲

<div class="b" id="bn1"><div class="m1"><p>دور باش از خط رخ دلدار هم می داشته است؟</p></div>
<div class="m2"><p>باغ جنت گرد خود دیوار هم می داشته است؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از هجوم شرم نتوان دید در رخسار یار</p></div>
<div class="m2"><p>چوب منع از جوش گل گلزار هم می داشته است؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خط پشت لبش شد تازه جان عالمی</p></div>
<div class="m2"><p>آب حیوان ابر گوهر بار هم می داشته است؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یافتم از بیخودی ره در حریم وصل یار</p></div>
<div class="m2"><p>خواب سنگین دولت بیدار هم می داشته است؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیستون بتخانه چین شد ز سعی کوهکن</p></div>
<div class="m2"><p>اینقدر عاشق دماغ کار هم می داشته است؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناله از جا در نیارد کوه تمکین ترا</p></div>
<div class="m2"><p>در جواب، استادگی کهسار هم می داشته است؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا دلم سرد از جهان شد، از ثمر شد کامیاب</p></div>
<div class="m2"><p>نخل سرما برده برگ و بار هم می داشته است؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خامشان هم نیستند آسوده از زخم زبان</p></div>
<div class="m2"><p>خار بی گل این گل بی خار هم می داشته است؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل دو نیم است از خموشیهای من غماز را</p></div>
<div class="m2"><p>بی زبانی تیغ لنگردار هم می داشته است؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر سویدای دل ما می کند افلاک سیر</p></div>
<div class="m2"><p>نقطه ای در دور نه پرگار هم می داشته است؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سنبلستان شد زمین از نقش پای کلک من</p></div>
<div class="m2"><p>پای چوبین اینقدر رفتار هم می داشته است؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برده صائب سبزه خط زنگ غم از دل مرا</p></div>
<div class="m2"><p>دست در پرداز دل زنگار هم می داشته است؟</p></div></div>