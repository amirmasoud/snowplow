---
title: >-
    غزل شمارهٔ ۶۹۶۱
---
# غزل شمارهٔ ۶۹۶۱

<div class="b" id="bn1"><div class="m1"><p>با زلف تو دم می زند از نافه گشایی</p></div>
<div class="m2"><p>بی شرمی مشک است ز مادر بخطایی!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از وصل نگیرد دل سودازده آرام</p></div>
<div class="m2"><p>در بحر همان موج کند سلسله خایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون گوی شدم بی سروپا تا شوم آزاد</p></div>
<div class="m2"><p>سرگشتگیم بیش شد از بی سرو پایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آینه تردست اگر زنگ زداید</p></div>
<div class="m2"><p>غم هم کند از دل به می ناب جدایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>افزایش ناقص بود از شهرت کاذب</p></div>
<div class="m2"><p>بر خود مه نو بالد از انگشت نمایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چند گلوسوز بود چاشنی وصل</p></div>
<div class="m2"><p>از دل نبرد تلخی ایام جدایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون شانه شمشاد به سر جای دهندش</p></div>
<div class="m2"><p>با دست تهی هر که کند عقده گشایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا هست به جا رشته ای از خرقه هستی</p></div>
<div class="m2"><p>از خار علایق نتوان یافت رهایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن را که بود در ته پا آتش شوقی</p></div>
<div class="m2"><p>در راه نگردد گره از آبله پایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زان زلف گرهگیر حذر کن که ز صیاد</p></div>
<div class="m2"><p>در چین کمندست نهان مد رسایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب نرود داغ کلف از رخ زردش</p></div>
<div class="m2"><p>تا ماه کند نور ز خورشید گدایی</p></div></div>