---
title: >-
    غزل شمارهٔ ۴۷۱۷
---
# غزل شمارهٔ ۴۷۱۷

<div class="b" id="bn1"><div class="m1"><p>خشتی به خیر چون خم می بر زمین گذار</p></div>
<div class="m2"><p>دیگر قدم به قصر بهشت برین گذار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اینک سپاه برق عنان ریز می رسد</p></div>
<div class="m2"><p>دست مروتی به دل خوشه چین گذار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون سوزن از لباس تعلق برهنه شو</p></div>
<div class="m2"><p>پا چون مسیح برفلک چارمین گذار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر چین چو عنکبوت کمند فریب را</p></div>
<div class="m2"><p>زنبوروار خانه پرانگبین گذار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کمتر نه ای ز خامه بی مغز در وجود</p></div>
<div class="m2"><p>بر صفحه جهان، سخن دلنشین گذار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حرص توانگران ز گدایان فزونترست</p></div>
<div class="m2"><p>جان راببوس وپیش خضر برزمین گذار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جویای توست خوشه گندم به صدزبان</p></div>
<div class="m2"><p>برپای سعی سلسله آهنین گذار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اول بگیر رخنه طوفان نوح را</p></div>
<div class="m2"><p>دیگر بیا به دیده من آستین گذار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب علاج آتش سوداست چوب گل</p></div>
<div class="m2"><p>کار عدو به کلک سخن آفرین گذار</p></div></div>