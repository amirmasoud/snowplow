---
title: >-
    غزل شمارهٔ ۲۸۱۷
---
# غزل شمارهٔ ۲۸۱۷

<div class="b" id="bn1"><div class="m1"><p>زدندان ریختن عقد سخن زیر و زبرگردد</p></div>
<div class="m2"><p>کف افسوس می گردد صدف چون بی گهر گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به اندک فرصتی می گردد از جان سیر تن پرور</p></div>
<div class="m2"><p>زگوهرهای فربه رشته لاغر زودتر گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مکش رو در هم از طوفان چو بی ظرفان درین دریا</p></div>
<div class="m2"><p>که هر چینی که بر ابرو زنی موج خطر گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر چون خار و خس خود را زبی برگی سبک سازی</p></div>
<div class="m2"><p>درین دریا ترا هر موجه ای بال دگر گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زخود بیگانه، با خلق آشنا گشتم ندانستم</p></div>
<div class="m2"><p>که هر کس آشنای خود نگردد دربدر گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا می زیبد از اهل بصیرت لاف بینایی</p></div>
<div class="m2"><p>به قدر داغ اگر دل آدمی را دیده ور گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به ذوقی شویم از جان دست در سرچشمه تیغش</p></div>
<div class="m2"><p>که خضر از آب حیوان با دهان خشک برگردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رود از دست بیرون زر چو بیش از قدر حاجت شد</p></div>
<div class="m2"><p>که خون فاسد چو شد آهن ربای نیشتر گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کنار و بوس می خواهم زخوبان، نیستم طوطی</p></div>
<div class="m2"><p>که از آیینه رخساران به حرف و صوت برگردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل روشن زموج انقلاب آسوده می باشد</p></div>
<div class="m2"><p>نجنبد آب گوهر بحر اگر زیر و زبر گردد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل افسرده نگشاید به حرف دلگشا صائب</p></div>
<div class="m2"><p>نسیم از غنچه پیکان گریبان چاک برگردد</p></div></div>