---
title: >-
    غزل شمارهٔ ۴۲۲۲
---
# غزل شمارهٔ ۴۲۲۲

<div class="b" id="bn1"><div class="m1"><p>روزی که زخم کاهکشان را رفوکنند</p></div>
<div class="m2"><p>بر روی چاک سینه ما در فروکنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنان که آستین به دو عالم فشانده اند</p></div>
<div class="m2"><p>بالین ز دست کوته خود چون سبو کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دردی کشان ز آینه خشت دیده اند</p></div>
<div class="m2"><p>رازی که در حقیقت آن گفتگو کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دل غبار غم به گریستن نمی رود</p></div>
<div class="m2"><p>این خانه را به سیل مگر رفت ورو کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دایم به عزتند کسانی که چون گهر</p></div>
<div class="m2"><p>از چشمه سار آب رخ خود وضو کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوهر فروز عقده تبخال می شود</p></div>
<div class="m2"><p>آبی که قطره قطره به حلق سبو کنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آتش سزای دیده بی شرم ما نداد</p></div>
<div class="m2"><p>ما را مگر به نامه ما روبرو کنند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب گهر به چشم صدف مردمک شود</p></div>
<div class="m2"><p>در بحر اگر گلیم مرا شستشو کنند</p></div></div>