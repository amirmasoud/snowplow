---
title: >-
    غزل شمارهٔ ۴۰۶۸
---
# غزل شمارهٔ ۴۰۶۸

<div class="b" id="bn1"><div class="m1"><p>کی غم مرا ز دل می احمر برآورد</p></div>
<div class="m2"><p>صیقل چگونه ز آینه جوهر برآورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن را که هست در رگ جان پیچ وتاب عشق</p></div>
<div class="m2"><p>چون رشته عاقبت ز گهر سربرآورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خوی آتشین تو هرجا سمندری است</p></div>
<div class="m2"><p>انگشت زینهار ز هر پر برآورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز افتادگی غبار به دل ره مده که مور</p></div>
<div class="m2"><p>عمرش تمام گردد اگر پر برآورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خودبین مشو کز آب روان بخش زندگی</p></div>
<div class="m2"><p>آیینه در به روی سکندر برآورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون آفتاب دولت دنیای زودسیر</p></div>
<div class="m2"><p>هر روز سر ز روزن دیگر برآورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قانع چو کهربا به پرکاه اگر شوم</p></div>
<div class="m2"><p>صد چشم در گرفتن آن پربرآورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پا در رکاب برق بود فصل نوبهار</p></div>
<div class="m2"><p>صائب ز زیر بال چرا سربرآورد</p></div></div>