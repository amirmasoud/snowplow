---
title: >-
    غزل شمارهٔ ۴۱۵۰
---
# غزل شمارهٔ ۴۱۵۰

<div class="b" id="bn1"><div class="m1"><p>گر یار را غنی ز نیاز آفریده اند</p></div>
<div class="m2"><p>ما را نیازمند به نازآفریده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قد ترا ز جلوه ناز آفریده اند</p></div>
<div class="m2"><p>روی مرا ز خاک نیاز آفریده اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لعل ترا که نقطه پرگار حیرت است</p></div>
<div class="m2"><p>پوشیده تر ز خرده راز آفریده اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آفتاب دل نربوده است هیچ کس</p></div>
<div class="m2"><p>دست تصرف تو دراز آفریده اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خیرگی نگاه مرا نیست کوتهی</p></div>
<div class="m2"><p>روی ترا نظاره گداز آفریده اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صورت پذیر نیست جمال لطیف یار</p></div>
<div class="m2"><p>دل را چه شد که آینه سازآفریده اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل را گداخت دیدن آن روی آتشین</p></div>
<div class="m2"><p>این باده را چه شیشه گداز آفریده اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خورشید طلعتان دل عشاق را چو ماه</p></div>
<div class="m2"><p>صد ره بهم شکسته وباز آفریده اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ننواخت هیچ کس دل زار مرا به لطف</p></div>
<div class="m2"><p>این رشته را برای چه سازآفریده اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بهر نیاز هر خم ابروست قبله ای</p></div>
<div class="m2"><p>این قبله از برای نماز آفریده اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عالم سیاه در نظر آب زندگی است</p></div>
<div class="m2"><p>تا آن عقیق تشنه نواز آفریده اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کوته ز آفتاب قیامت نمی شود</p></div>
<div class="m2"><p>شبهای هجر را چه دراز آفریده اند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کبکم ولیک خون من بیگناه را</p></div>
<div class="m2"><p>گیرنده تر ز چنگل باز آفریده اند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از خاکدان دهر سلامت طوع مدار</p></div>
<div class="m2"><p>کاین بوته را برای گدازآفریده اند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صائب ز دلشکستگی خود غمین مباش</p></div>
<div class="m2"><p>کان زلف را شکسته نواز آفریده اند</p></div></div>