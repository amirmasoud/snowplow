---
title: >-
    غزل شمارهٔ ۶۹۳۶
---
# غزل شمارهٔ ۶۹۳۶

<div class="b" id="bn1"><div class="m1"><p>زین گریه دروغ که ای پیر می کنی</p></div>
<div class="m2"><p>آبی به شیراز سر تزویر می کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان به بود که سیر کنی صد گرسنه را</p></div>
<div class="m2"><p>چشم گرسنه خود اگر سیر می کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از سیر نیست مانع عمر سبک خرام</p></div>
<div class="m2"><p>موی خود از خضاب اگر قیر می کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مویت سفید و نامه اعمال شد سیاه</p></div>
<div class="m2"><p>در توبه اینقدر ز چه تائخیر می کنی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کافور مرگ آتش حرص ترا، کم است</p></div>
<div class="m2"><p>تو ساده لوح فکر طباشیر می کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طی شد شب جوانی و خندید صبح شیب</p></div>
<div class="m2"><p>تو این زمان تهیه شبگیر می کنی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در خامشی گریز ز تقصیرهای خویش</p></div>
<div class="m2"><p>تمهید عذر بهر چه تقصیر می کنی؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این خانه را که طعمه سیلاب می شود</p></div>
<div class="m2"><p>ای خانمان خراب چه تعمیر می کنی؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کم کرده ای گناه، که در وقت بازخواست</p></div>
<div class="m2"><p>تقصیر خود حواله به تقدیر می کنی؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن خصم نیست نفس کز احسان شود مطیع</p></div>
<div class="m2"><p>غافل مشو که تربیت شیر می کنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سال دراز کعبه نگرداند رخت خویش</p></div>
<div class="m2"><p>تو هر دو روز رخت چه تغییر می کنی؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن پرده سوز، قابل تصویر خلق نیست</p></div>
<div class="m2"><p>در پرده است هر چه تو تصویر می کنی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون سینه را هدف کنی ای بیجگر، که تو</p></div>
<div class="m2"><p>در خانه کمان حذر از تیر می کنی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صائب مس تو نیست پذیرای نور فیض</p></div>
<div class="m2"><p>بیهوده عمر خرج در اکسیر می کنی</p></div></div>