---
title: >-
    غزل شمارهٔ ۶۲۵۸
---
# غزل شمارهٔ ۶۲۵۸

<div class="b" id="bn1"><div class="m1"><p>چشم خورشید به رخسار تو باشد روشن</p></div>
<div class="m2"><p>نیست یک سرو به غیر از تو درین سبز چمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یوسف از غیرت آن نرگس نیلوفر رنگ</p></div>
<div class="m2"><p>رفت تا مصر که در نیل زند پیراهن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگذار از پرده ناموس که سرگرمی عشق</p></div>
<div class="m2"><p>نه چراغی است که پوشیده شود از دامن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچنان می پرد از بی خبری چشم حباب</p></div>
<div class="m2"><p>گر چه با بحر بود در ته یک پیرهن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هاله ماه ز شوق تو گشاده است آغوش</p></div>
<div class="m2"><p>چند چون شمع توان بود گرفتار لگن؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تن به زندان غریبی ندهد کس، چه کند؟</p></div>
<div class="m2"><p>نیست بی چاه حسد دامن صحرای وطن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرگ در مذهب ما رخصت بال افشانی است</p></div>
<div class="m2"><p>صبح امید دمد اهل صفا را ز کفن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب این آن غزل مرشد روم است که گفت</p></div>
<div class="m2"><p>بشکن شاخ نبات و دل ما را مشکن</p></div></div>