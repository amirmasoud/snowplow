---
title: >-
    غزل شمارهٔ ۳۹۷۲
---
# غزل شمارهٔ ۳۹۷۲

<div class="b" id="bn1"><div class="m1"><p>کسی که کشته آن تیغ آبدار شود</p></div>
<div class="m2"><p>اگر چه قطره بود بحر بیکنار شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محیط حسن ز خط عنبرین کنار شود</p></div>
<div class="m2"><p>عقیق لب ز خط سبز نامدار شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر به صید زبون تیغ او کند اقبال</p></div>
<div class="m2"><p>ز خون صید حرم کعبه لاله زار شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز رام گشتن آهو به صحبت لیلی</p></div>
<div class="m2"><p>به آن رسیده که مجنون امیدوار شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی که در جگرش هست خار خارگلی</p></div>
<div class="m2"><p>غمش ز ناله بلبل یکی هزار شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هنوز خط ترا ابتدای نشو ونماست</p></div>
<div class="m2"><p>کجاست جوهر حسن تو آشکار شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بخت تیره ندارد گریز اهل سخن</p></div>
<div class="m2"><p>سیاه روز عقیقی که نامدار شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به نوشخند حلاوت مکن دهن شیرین</p></div>
<div class="m2"><p>که باده تلخ چو گردید خوشگوار شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی هزار شد از قمریان رعونت سرو</p></div>
<div class="m2"><p>الف چو نقطه بیابد یکی هزار شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا شد از کلف ماه روشن این معنی</p></div>
<div class="m2"><p>که دل، سیاه ز تشریف مستعار شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درین بساط کسی مایه دار می گردد</p></div>
<div class="m2"><p>که نقد زندگیش خرج انتظار شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مسیح برفلک از راه خاکساری رفت</p></div>
<div class="m2"><p>پیاده هر که شد اینجا فلک سوار شود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو بیجگر کند از تیغ زهر داده حذر</p></div>
<div class="m2"><p>اگر به خضر طلبکار او دچار شود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به آن گرم درین بوته آب کن دل را</p></div>
<div class="m2"><p>که گل گلاب چو گردید پایدار شود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دلی که از نفس گرم آب شد صائب</p></div>
<div class="m2"><p>اگر به خاک چکد در شاهوار شود</p></div></div>