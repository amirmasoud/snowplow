---
title: >-
    غزل شمارهٔ ۲۳۲
---
# غزل شمارهٔ ۲۳۲

<div class="b" id="bn1"><div class="m1"><p>صاف کن ای سنگدل با دردمندان سینه را</p></div>
<div class="m2"><p>می کند دربسته آهی خانه آیینه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درد و داغ عشق را در دل نهفتن مشکل است</p></div>
<div class="m2"><p>این سپند شوخ، مجمر می کند گنجینه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمر باقی مانده را نتوان به غفلت صرف کرد</p></div>
<div class="m2"><p>ساقیا پیش آر آن ته شیشه دوشینه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زنگ از آیینه تاریک صیقل می برد</p></div>
<div class="m2"><p>مگذران بی باده روشن شب آدینه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیچ سیل خانه پردازی چو گرد کینه نیست</p></div>
<div class="m2"><p>در درون خانه باشد خصم، صاحب کینه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل ز شبنم در دل شبها نمی باشد جدا</p></div>
<div class="m2"><p>خودپرستان در بغل گیرند شب آیینه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از نمد، آیینه صائب در حصار آهن است</p></div>
<div class="m2"><p>صوفیان دانند قدر خرقه پشمینه را</p></div></div>