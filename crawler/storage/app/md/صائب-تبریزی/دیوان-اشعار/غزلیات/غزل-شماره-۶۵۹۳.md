---
title: >-
    غزل شمارهٔ ۶۵۹۳
---
# غزل شمارهٔ ۶۵۹۳

<div class="b" id="bn1"><div class="m1"><p>سرو من طرح نو انداخته‌ای یعنی چه؟</p></div>
<div class="m2"><p>جامه را فاخته‌ای ساخته‌ای یعنی چه؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو که از شرم به مشاطه نمی‌پردازی</p></div>
<div class="m2"><p>یک جهان آینه پرداخته‌ای یعنی چه؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو که در خانه ز شوخی ننشینی هرگز</p></div>
<div class="m2"><p>خانه در ملک کسان ساخته‌ای یعنی چه؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شیر در بیشه خشم تو جگر می‌بازد</p></div>
<div class="m2"><p>رنگ چون بی‌جگران باخته‌ای یعنی چه؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عالمی زیر و زبر کردی و از پرکاری</p></div>
<div class="m2"><p>علم زلف نگون ساخته‌ای یعنی چه؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تشنه خون منی همچو صراحی در دل</p></div>
<div class="m2"><p>دست در گردنم انداخته‌ای یعنی چه؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تیر بر سینه اهل نظر انداخته‌ای</p></div>
<div class="m2"><p>بعد از آن سینه سپر ساخته‌ای یعنی چه؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرد پاپوش نیفشانده به صحرای وطن</p></div>
<div class="m2"><p>باز طرح سفر انداخته‌ای یعنی چه؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شرمی از حافظ شیراز نداری صائب؟</p></div>
<div class="m2"><p>این چنین تیغ زبان آخته‌ای یعنی چه؟</p></div></div>