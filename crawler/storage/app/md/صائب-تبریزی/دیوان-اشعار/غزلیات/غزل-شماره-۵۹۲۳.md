---
title: >-
    غزل شمارهٔ ۵۹۲۳
---
# غزل شمارهٔ ۵۹۲۳

<div class="b" id="bn1"><div class="m1"><p>ما چاشنی بوسه ز دشنام گرفتیم</p></div>
<div class="m2"><p>فیض شکر از تلخی بادام گرفتیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل صاف نمودیم به نیک و بد ایام</p></div>
<div class="m2"><p>فیض دم صبح از نفس شام گرفتیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناکامی جاوید چو در کام جهان بود</p></div>
<div class="m2"><p>از کام جهان دست به ناکام گرفتیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در رهگذر سیل فنا خواب حرام است</p></div>
<div class="m2"><p>رفتیم برون از فلک آرام گرفتیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر کنگره عرش ضرورست کمندی</p></div>
<div class="m2"><p>چون شانه سر زلف دلارام گرفتیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رفتیم ازین قلزم خونین به کناری</p></div>
<div class="m2"><p>زین معرکه خود را به لب بام گرفتیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیدیم که پرگار فلک در کف ما نیست</p></div>
<div class="m2"><p>چون نقطه درین دایره آرام گرفتیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زهاد گرفتند ره گلشن فردوس</p></div>
<div class="m2"><p>ما گردن مینا و لب جام گرفتیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کردیم دل سنگدلان را به سخن نرم</p></div>
<div class="m2"><p>از ریگ روان روغن بادام گرفتیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در دست فلاخن نکند سنگ اقامت</p></div>
<div class="m2"><p>ما زیر فلک بهر چه آرام گرفتیم؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب ز سر میوه فردوس گذشتیم</p></div>
<div class="m2"><p>تا بوسه تلخ از دهن جام گرفتیم</p></div></div>