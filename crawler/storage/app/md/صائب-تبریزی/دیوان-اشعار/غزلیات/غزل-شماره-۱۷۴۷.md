---
title: >-
    غزل شمارهٔ ۱۷۴۷
---
# غزل شمارهٔ ۱۷۴۷

<div class="b" id="bn1"><div class="m1"><p>به هر دل آتشی از روی دلبر افتاده است</p></div>
<div class="m2"><p>سپند ماست که از چشم مجمر افتاده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلال وصل تو یارب چه خاصیت دارد</p></div>
<div class="m2"><p>کز آتش تو جهانی به کوثر افتاده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز خط نگشته بناگوش او غبارآلود</p></div>
<div class="m2"><p>که عکس گرد یتیمی ز گوهر افتاده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا ز گوشه عزلت مخوان به سیر بهشت</p></div>
<div class="m2"><p>که چشم من به تماشای دیگر افتاده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به بیشی و کمی مال نیست فقر و غنا</p></div>
<div class="m2"><p>ز توست عالم اگر دل توانگر افتاده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مجوی از دل بی طاقتان عشق قرار</p></div>
<div class="m2"><p>که این سپند به صحرای محشر افتاده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز نافه مغز شکارافکنان کند معمور</p></div>
<div class="m2"><p>غزال وحشی ما گر چه لاغر افتاده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قسم به پاکی ما می خورند جوهریان</p></div>
<div class="m2"><p>چه شد که دامن ما چون گهر تر افتاده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ستاره ای که من از داغ عشق او دارم</p></div>
<div class="m2"><p>به آفتاب قیامت برابر افتاده است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نگشته پشت لب او ز خط مشکین سبز</p></div>
<div class="m2"><p>که سایه پر طوطی به شکر افتاده است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عجب که روی به آیینه سخن آرد</p></div>
<div class="m2"><p>چنین که طوطی صائب به شکر افتاده است</p></div></div>