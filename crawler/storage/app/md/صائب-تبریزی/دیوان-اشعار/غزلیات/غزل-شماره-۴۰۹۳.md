---
title: >-
    غزل شمارهٔ ۴۰۹۳
---
# غزل شمارهٔ ۴۰۹۳

<div class="b" id="bn1"><div class="m1"><p>تا گردباد آه به گردون نمی رسد</p></div>
<div class="m2"><p>از گرد راه قاصد مجنون نمی رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرجا دچار وصل شوی کام دل بکیر</p></div>
<div class="m2"><p>هر روز نافه بر سر مجنون نمی رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر مصرع بلند به عمری برابرست</p></div>
<div class="m2"><p>زین بیشتر به مردم موزون نمی رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا دختری ز سلسله تاک مانده است</p></div>
<div class="m2"><p>وحدت سرای خم به فلاطون نمی رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صائب نمی کشم نفسی کز دیار عشق</p></div>
<div class="m2"><p>صد درد نوبه سینه محزون نمی رسد</p></div></div>