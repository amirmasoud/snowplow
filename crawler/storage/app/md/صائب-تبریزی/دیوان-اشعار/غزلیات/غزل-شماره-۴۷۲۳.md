---
title: >-
    غزل شمارهٔ ۴۷۲۳
---
# غزل شمارهٔ ۴۷۲۳

<div class="b" id="bn1"><div class="m1"><p>ویرانه های کهنه بود جای مور و مار</p></div>
<div class="m2"><p>در طبع پیر حرص و تمناست بیشتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در پرده حجاب کند غنچه نوشخند</p></div>
<div class="m2"><p>دلهای شب گشایش دلهاست بیشتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میدان دهد به سرکشی اسب بال و پر</p></div>
<div class="m2"><p>شور جنون به دامن صحراست بیشتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مجنون حسد به شورش فرهاد می برد</p></div>
<div class="m2"><p>درکوهسار سیل به غوغاست بیشتر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صائب کسی که عاقبت اندیش اوفتاد</p></div>
<div class="m2"><p>روی دلش به عالم عقباست بیشتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل روشن از سیاهی سوداست بیشتر</p></div>
<div class="m2"><p>سوز و گداز شمع به شبهاست بیشتر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در زیر خاک دانه به ابرست امیدوار</p></div>
<div class="m2"><p>دل را نظر به عالم بالاست بیشتر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در دل گره زیاده به چشم است اشک من</p></div>
<div class="m2"><p>گوهر نهفته در ته دریاست بیشتر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پوشیده است دردل عنبر بهارها</p></div>
<div class="m2"><p>صبح امید دردل شبهاست بیشتر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سوزن همیشه خون خورد از خارپای خلق</p></div>
<div class="m2"><p>زحمت نصیب دیده بیناست بیشتر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دولت شود ز پله تمکین گران رکاب</p></div>
<div class="m2"><p>در کوه قاف شوکت عنقاست بیشتر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دشنام در مذاق من از بوسه خوشترست</p></div>
<div class="m2"><p>چون باده تلخ گشت گواراست بیشتر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اشک ندامت است سیه کار را فزون</p></div>
<div class="m2"><p>باران در ابر تیره مهیاست بیشتر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر نیست تخم سوخته نشو ونما پذیر</p></div>
<div class="m2"><p>چون دربهار شورش سوداست بیشتر؟</p></div></div>