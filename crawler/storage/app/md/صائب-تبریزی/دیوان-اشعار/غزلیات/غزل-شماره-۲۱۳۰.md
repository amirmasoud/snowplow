---
title: >-
    غزل شمارهٔ ۲۱۳۰
---
# غزل شمارهٔ ۲۱۳۰

<div class="b" id="bn1"><div class="m1"><p>دل در نظر مردم فرزانه بزرگ است</p></div>
<div class="m2"><p>طفلان چه شناسند که دیوانه بزرگ است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون اشک، فکندن ز نظر هر دو جهان را</p></div>
<div class="m2"><p>سهل است، اگر همت مردانه بزرگ است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بی ادبان کعبه گل می گذراند</p></div>
<div class="m2"><p>با دل به ادب باش که این خانه بزرگ است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با وسعت مشرب چه بود کوه غم عشق؟</p></div>
<div class="m2"><p>در حوصله تنگ تو این دانه بزرگ است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دارد صدف از سینه هر قطره دلتنگ</p></div>
<div class="m2"><p>هر چند که آن گوهر یکدانه بزرگ است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در ذره به حشمت نگرد دیده عارف</p></div>
<div class="m2"><p>هر خرد درین گوشه میخانه بزرگ است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در پله میزان نظر، سنگ کمش نیست</p></div>
<div class="m2"><p>چون کعبه به چشمی که صنمخانه بزرگ است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خون در خور پیمانه دهد ساقی دوران</p></div>
<div class="m2"><p>مغرور نگردی که ترا خانه بزرگ است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در پایه خود هیچ کسی خرد نباشد</p></div>
<div class="m2"><p>تا جغد بود ساکن ویرانه بزرگ است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر توست فلک ها ز پریشان سفری تنگ</p></div>
<div class="m2"><p>خود را چو کنی جمع تو، این خانه بزرگ است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در کعبه و بتخانه ز گفتار دلاویز</p></div>
<div class="m2"><p>هر جا که رود صائب فرزانه بزرگ است</p></div></div>