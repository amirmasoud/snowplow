---
title: >-
    غزل شمارهٔ ۲۱۸۲
---
# غزل شمارهٔ ۲۱۸۲

<div class="b" id="bn1"><div class="m1"><p>خورشید نقاب رخ چون یاسمن کیست؟</p></div>
<div class="m2"><p>پیراهن صبح آینه دان بدن کیست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون راه سخن نیست در آن غنچه مستور</p></div>
<div class="m2"><p>گوش دو جهان تنگ شکر از سخن کیست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخسار که روشنگر آیینه روزست؟</p></div>
<div class="m2"><p>شب سایه گیسوی شکن بر شکن کیست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر شبنمی از دیده یعقوب دهد یاد</p></div>
<div class="m2"><p>پیراهن گلها ز سر پیرهن کیست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در نافه شب، خون شفق مشک که کرده است؟</p></div>
<div class="m2"><p>این مرحمت از طره عنبرشکن کیست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خون شفق، ساعد صبح و کف خورشید</p></div>
<div class="m2"><p>از حیرت نظاره سیب ذقن کیست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون خانه زنبور عسل، شش جهت خاک</p></div>
<div class="m2"><p>لبریز ز شهد از لب شکرشکن کیست؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دریای وجود و عدم آمیخته با هم</p></div>
<div class="m2"><p>چون شیر و شکر از دهن خوش سخن کیست؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از نکهت پیراهن یوسف گله دارد</p></div>
<div class="m2"><p>این مغز، بدآموز نسیم چمن کیست؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر چند که هنگامه دلهاست ازو گرم</p></div>
<div class="m2"><p>روشن نتوان گفت که در انجمن کیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جز زلف تو ای صف شکن صبر و تحمل</p></div>
<div class="m2"><p>افتادن و افکندن عشاق فن کیست؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دست و دهن موسی ازین مایده شد داغ</p></div>
<div class="m2"><p>این لقمه به اندازه کام و دهن کیست؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر کس گلی از شوق تو در آب گرفته است</p></div>
<div class="m2"><p>تا قامت رعنای تو سرو چمن کیست؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سودای تو در انجمن آرایی دلهاست</p></div>
<div class="m2"><p>تا شمع جهانسوز تو در انجمن کیست؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دلها شده از پرده فانوس تنکتر</p></div>
<div class="m2"><p>تا شعله سودای تو هم پیرهن کیست؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در گلشن جنت ننشیند دل صائب</p></div>
<div class="m2"><p>تا در سر این مرغ هوای چمن کیست؟</p></div></div>