---
title: >-
    غزل شمارهٔ ۳۹۱۶
---
# غزل شمارهٔ ۳۹۱۶

<div class="b" id="bn1"><div class="m1"><p>خوش آن گروه که تن راز عشق جان سازند</p></div>
<div class="m2"><p>زمین خویش به تدبیر آسمان سازند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جماعتی که به تن از جهان جان سازند</p></div>
<div class="m2"><p>به تخته پاره ای از بحر بیکران سازند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه فارغند ز اندیشه شراب وکباب</p></div>
<div class="m2"><p>جماعتی که به دلهای خونچکان سازند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زسایه روی زمین را به پرنیان گیرم</p></div>
<div class="m2"><p>اگر همای مرا سیر از استخوان سازند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سبکروان نفسی بهر راه تازه کنند</p></div>
<div class="m2"><p>اگر دو روز به این تیره خاکدان سازند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به زخم خار گروهی که برنمی آیند</p></div>
<div class="m2"><p>همان به است که با یاد گلستان سازند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چوتیر راست روان زمانه را شرط است</p></div>
<div class="m2"><p>که با کشاکش گردون چون کمان سازند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جماعتی که ز ساقی به جام صلح کنند</p></div>
<div class="m2"><p>به یک حباب ز دریای بیکران سازند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غباردر دل هیچ آفریده نگذارم</p></div>
<div class="m2"><p>اگر چوسیل مرا مطلق العنان سازند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بجاست تا رگ گردن ترا مثال هدف</p></div>
<div class="m2"><p>ز هر طرف که رسد ناوکی نشان سازند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نمی کشند خجالت اگر تهیدستان</p></div>
<div class="m2"><p>به ناله جرس از وصل کاروان سازند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر آن گروه حرام است خامشی صائب</p></div>
<div class="m2"><p>که کار خلق توانند از زبان سازند</p></div></div>