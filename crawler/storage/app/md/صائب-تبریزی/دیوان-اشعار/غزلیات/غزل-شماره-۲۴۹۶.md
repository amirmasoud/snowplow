---
title: >-
    غزل شمارهٔ ۲۴۹۶
---
# غزل شمارهٔ ۲۴۹۶

<div class="b" id="bn1"><div class="m1"><p>اهل همت جنس خواری را به عزت می خرند</p></div>
<div class="m2"><p>خاک راه را از تهیدستان به قیمت می خرند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از کسادی نیشکر انگشت حسرت می مکد</p></div>
<div class="m2"><p>مردم از کام مگس شهد حلاوت می خرند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آه از این افسردگان، فریاد ازین دلمردگان</p></div>
<div class="m2"><p>شمع کافوری پی گرمی صحبت می خرند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با کباب تر نمک را التیام دیگرست</p></div>
<div class="m2"><p>سینه مجروحان به جان شور قیامت می خرند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناامید از آبروی جبهه خجلت مباش</p></div>
<div class="m2"><p>کاین متاع ناروا را در قیامت می خرند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حج خریدن در دیار عشقبازان رسم نیست</p></div>
<div class="m2"><p>هر که مرد اینجا، برای او شهادت می خرند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوهر سیراب را صائب درین خاک سیاه</p></div>
<div class="m2"><p>گر به نرخ خاک بفروشی، به نفرت می خرند</p></div></div>