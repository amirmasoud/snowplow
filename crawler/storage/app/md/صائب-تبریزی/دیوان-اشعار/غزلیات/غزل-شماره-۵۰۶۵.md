---
title: >-
    غزل شمارهٔ ۵۰۶۵
---
# غزل شمارهٔ ۵۰۶۵

<div class="b" id="bn1"><div class="m1"><p>سر سبز آن که سعی کنددر هلاک خویش</p></div>
<div class="m2"><p>چیندچو سرو دامن همت ز خاک خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکس نداشت زنده شبی رابرای دوست</p></div>
<div class="m2"><p>درزندگی نبرد چراغی به خاک خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازدشمن غیور تنزل نمی کنم</p></div>
<div class="m2"><p>در دیده سپهر زنم مشت خاک خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جرأت به تیزدستی من فخر می کند</p></div>
<div class="m2"><p>ازکوهکن دلیر ترم در هلاک خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن زلف همچو دام، که عمرش دراز باد</p></div>
<div class="m2"><p>هرگز نکرد یاد اسیران خاک خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشق چرا امید نبندد به عشق پاک؟</p></div>
<div class="m2"><p>شبنم عزیز باغ شد از چشم پاک خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب نیم ز تنگی دل غنچه سان ملول</p></div>
<div class="m2"><p>چون گل شکفته ام ز دل چاک چاک خویش</p></div></div>