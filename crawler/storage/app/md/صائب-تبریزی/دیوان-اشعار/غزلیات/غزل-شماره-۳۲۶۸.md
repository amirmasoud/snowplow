---
title: >-
    غزل شمارهٔ ۳۲۶۸
---
# غزل شمارهٔ ۳۲۶۸

<div class="b" id="bn1"><div class="m1"><p>نخل قد تو به باغی که خرامان گردد</p></div>
<div class="m2"><p>سرو در زیر پر فاخته پنهان گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون به گلزار روی خواب خمار آلوده</p></div>
<div class="m2"><p>گل ز خمیازه آغوش پریشان گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر سیه روز به کیفیت چشمش نرسد</p></div>
<div class="m2"><p>سرمه را جوهر آن نیست که حیران گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رنگ از چهره گلهای هوس محو شود</p></div>
<div class="m2"><p>چون سهیل عرق شرم فروزان گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شرط عشق است که تا شور محبت باقی است</p></div>
<div class="m2"><p>زخم ناسور به دنبال نمکدان گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صائب از پرتو حسن است که بلبل شده است</p></div>
<div class="m2"><p>طوطی از صحبت آیینه سخندان گردد</p></div></div>