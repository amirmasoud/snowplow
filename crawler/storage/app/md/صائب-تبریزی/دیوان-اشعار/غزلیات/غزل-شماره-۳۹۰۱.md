---
title: >-
    غزل شمارهٔ ۳۹۰۱
---
# غزل شمارهٔ ۳۹۰۱

<div class="b" id="bn1"><div class="m1"><p>سزد که خرده جان را کند نثار سپند</p></div>
<div class="m2"><p>که یافت راه سخن در حریم یار سپند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرشک گرم که گوهر فروز این دریاست</p></div>
<div class="m2"><p>که مجمرست صدف در شاهوار سپند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز آتشین رخ او بزم آب ورنگی یافت</p></div>
<div class="m2"><p>که شد چو دانه یاقوت آبدار سپند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنین که عشق مرا بیقرار ساخته است</p></div>
<div class="m2"><p>ز آرمیده دلان است ازین قرار سپند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مدار دست ز بیطاقتی که می گردد</p></div>
<div class="m2"><p>به دوش شعله ز بیطاقتی سوار سپند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فروغ حسن نفس سرمه می کند در کام</p></div>
<div class="m2"><p>چه دل تهی کند از ناله پیش یار سپند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به عیش خلوت خاص تو چشم بی مرساد</p></div>
<div class="m2"><p>که پایکوبان ز آتش کند گذار سپند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قیامت است در آن انجمن که عارض او</p></div>
<div class="m2"><p>ز می فروزد و ریزد ستاره وار سپند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>توان به بال رمیدن گذشت از عالم</p></div>
<div class="m2"><p>که جسته جسته ز آتش کند گذار سپند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه شد که ظاهر اهل دل آرمیده بود</p></div>
<div class="m2"><p>که مجمرست زمین گیر وبیقرار سپند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنان ز دایره روی یار حیران شد</p></div>
<div class="m2"><p>که همچو مرکز گردید پایدار سپند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نوای سوختگان کوه را به رقص آرد</p></div>
<div class="m2"><p>بنای صبر مرا کرد تارومار سپند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز حسن طبع رهی باد دیده بد دور</p></div>
<div class="m2"><p>که دشت مجمره گردید وکوهسار سپند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به اضطراب دل ما نمی رسد صائب</p></div>
<div class="m2"><p>اگر چه هست به بیطاقتی سوار سپند</p></div></div>