---
title: >-
    غزل شمارهٔ ۵۳۸۲
---
# غزل شمارهٔ ۵۳۸۲

<div class="b" id="bn1"><div class="m1"><p>رخت ازین دنیای پر وحشت به یک سومی کشم</p></div>
<div class="m2"><p>خویش را در گوشه آن چشم جادو می کشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می کند موج سرابش کار تیغ آبدار</p></div>
<div class="m2"><p>در بیابانی که من گردن چو آهو می کشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا مگر مغزم به بوی آشنایی برخورد</p></div>
<div class="m2"><p>عمرها شد چون صبا در گلستان بو می کشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوسر فردی که از عالم سبکبارم کند</p></div>
<div class="m2"><p>کز دو سر دایم گرانی چون ترازو می کشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا شنیدم می شود از شکر، نعمتها زیاد</p></div>
<div class="m2"><p>هر که رو گردان شد از من دست بررو می کشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش ازین آهو به چشمم اعتبار سگ نداشت</p></div>
<div class="m2"><p>این زمان نازسگ لیلی ز آهو می کشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست تاب باز منت سرو آزاد مرا</p></div>
<div class="m2"><p>دست بر دل می نهم، پا زین لب جو می کشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از دل بی دست و پای خویش می گیرم خبر</p></div>
<div class="m2"><p>دست اگر گاهی به زلف و کاکل او می کشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشم اگر افتد به مهر خامشی صائب مرا</p></div>
<div class="m2"><p>حرف ازوبی پرده چون چشم سخنگومی کشم</p></div></div>