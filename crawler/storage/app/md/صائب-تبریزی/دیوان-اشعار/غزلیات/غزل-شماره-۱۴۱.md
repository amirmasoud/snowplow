---
title: >-
    غزل شمارهٔ ۱۴۱
---
# غزل شمارهٔ ۱۴۱

<div class="b" id="bn1"><div class="m1"><p>پرده دار و حاجب و دربان نمی باشد مرا</p></div>
<div class="m2"><p>خانه چون آیینه بی مهمان نمی باشد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درد و صاف عالم امکان ز یک سرچشمه است</p></div>
<div class="m2"><p>شکوه ای از ساقی دوران نمی باشد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کعبه و بتخانه یکسان است پیش چشم من</p></div>
<div class="m2"><p>سنگ کم در پله میزان نمی باشد مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خرابات تجرد می کنم چون عشق شیر</p></div>
<div class="m2"><p>خانه در معموره امکان نمی باشد مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طوق من چون قمریان از حلقه ماتم بود</p></div>
<div class="m2"><p>خاطر شاد و لب خندان نمی باشد مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنچه چون آیینه دارم در نظر، نقش دل است</p></div>
<div class="m2"><p>از کسی پوشیده و پنهان نمی باشد مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شعله را در پاکبازی داغ دارد همتم</p></div>
<div class="m2"><p>خارخار آرزو در جان نمی باشد مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قانعم با قطره آبی که دارم چون گهر</p></div>
<div class="m2"><p>چشم آب از قلزم و عمان نمی باشد مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیک و بد یک جلوه چون آیینه دارد در دلم</p></div>
<div class="m2"><p>شکوه از چشم و دل حیران نمی باشد مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>داده ام دل را به دست عشق در روز ازل</p></div>
<div class="m2"><p>یوسف بی جرم در زندان نمی باشد مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همچو مژگان تیر یک ترکش بود افکار من</p></div>
<div class="m2"><p>مصرع بی رتبه در دیوان نمی باشد مرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خود به خود چون غنچه صائب عقده ام وا می شود</p></div>
<div class="m2"><p>احتیاج ناخن و دندان نمی باشد مرا</p></div></div>