---
title: >-
    غزل شمارهٔ ۳۸۱۸
---
# غزل شمارهٔ ۳۸۱۸

<div class="b" id="bn1"><div class="m1"><p>ز جلوه تو چو سیلاب الامان خیزد</p></div>
<div class="m2"><p>ز پیش راه تو چون گرد آسمان خیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز فکر روی تو روشن شد آنچنان دل من</p></div>
<div class="m2"><p>که همچو صبح مرا نور از دهان خیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به کام دل نفس آتشین چگونه کشم؟</p></div>
<div class="m2"><p>مرا کز آتش گل دود از آشیان خیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشو ز پاس دل رام گشته ام غافل</p></div>
<div class="m2"><p>که از رمیدن من گرد از جهان خیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشست گرد یتیمی ز روی گوهر، بحر</p></div>
<div class="m2"><p>چه زنگ از دلم از چشم خونفشان خیزد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز زخم تیر مکافات ظالمان نرهند</p></div>
<div class="m2"><p>که پیشتر ز نشان ناله از کمان خیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه آنچنان ز گرانی نشسته است به گل</p></div>
<div class="m2"><p>سفینه ام، که به امداد بادبان خیزد</p></div></div>