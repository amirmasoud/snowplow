---
title: >-
    غزل شمارهٔ ۲۴۰۶
---
# غزل شمارهٔ ۲۴۰۶

<div class="b" id="bn1"><div class="m1"><p>هر که بر دار فنا مردانه پشت پا نزد</p></div>
<div class="m2"><p>غوطه در سرچشمه خورشید چون عیسی نزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون صدف در دامن خود گوهر مقصد نیافت</p></div>
<div class="m2"><p>تا به جان بی نفس غواص بر دریا نزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون دل شد در بساط سینه اش یاقوت و لعل</p></div>
<div class="m2"><p>هر که زیر تیغ چون کهسار دست و پا نزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هست راهی چون گهر دلهای سنگین را به هم</p></div>
<div class="m2"><p>تیشه خود کوهکن بیهوده بر خارا نزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرد آن وحشی به جست وجو نمی آید به چشم</p></div>
<div class="m2"><p>قطره بیش از من کسی در دامن صحرا نزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سیاهی داغ آب زندگی آمد برون</p></div>
<div class="m2"><p>مشت آبی هیچ کس بر روی بخت ما نزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جوش خون صائب دل تنگ مرا در هم شکست</p></div>
<div class="m2"><p>هیچ کس جز زور می سنگی بر این مینا نزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد ز وصل غنچه صائب مشکبو باد سحر</p></div>
<div class="m2"><p>وای بر آن کس که دستی بر در دلها نزد</p></div></div>