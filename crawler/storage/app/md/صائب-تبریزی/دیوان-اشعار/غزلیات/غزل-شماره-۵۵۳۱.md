---
title: >-
    غزل شمارهٔ ۵۵۳۱
---
# غزل شمارهٔ ۵۵۳۱

<div class="b" id="bn1"><div class="m1"><p>امید چرب نرمی از خسیسان جهان دارم</p></div>
<div class="m2"><p>چه مجنونم که چشم روغن از ریگ روان دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فروغ آفتابم، سرکشی از من نمی آید</p></div>
<div class="m2"><p>اگر بر آسمان باشم نظر بر آستان دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هر جانب که روآورم شکستم بر شکست آید</p></div>
<div class="m2"><p>همیشه همچو رنگ عاشقان رو در خزان دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زبان گندمین نان مرا پخته است در عالم</p></div>
<div class="m2"><p>چرا چون خوشه گردن کج به پیش این و آن دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به من از رخنه دیوار، خود را می رساند گل</p></div>
<div class="m2"><p>چه لازم دامن در یوزه پیش باغبان دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به ظاهر خنده رو افتاده ام چون صبحدم، اما</p></div>
<div class="m2"><p>تبی چون آفتاب گرمرو در استخوان دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ندارد ریشه در خاک تعلق سرو آزادم</p></div>
<div class="m2"><p>دلی آماده پرواز چون برگ خزان دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز چرخ آهنین باز و چرا چون تیر نگریزم</p></div>
<div class="m2"><p>تن خشکی مهیای شکستن چون کمان دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به اندک سختیی چون نخل موم از هم نمی پاشم</p></div>
<div class="m2"><p>اگر چه مغزم اما جان سخت استخوان دارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زلیخا همتی در عرصه عالم نمی بینم</p></div>
<div class="m2"><p>وگر نه جنس یوسف کاروان در کاروان دارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مزاج نازک احباب را فهمیده ام صائب</p></div>
<div class="m2"><p>چو غنچه مهر خاموشی به لب با صد زبان دارم</p></div></div>