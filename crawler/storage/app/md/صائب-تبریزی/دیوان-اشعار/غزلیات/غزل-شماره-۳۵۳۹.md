---
title: >-
    غزل شمارهٔ ۳۵۳۹
---
# غزل شمارهٔ ۳۵۳۹

<div class="b" id="bn1"><div class="m1"><p>غم محال است که تدبیر دل من نکند</p></div>
<div class="m2"><p>این نه برقی است که دلسوزی خرمن نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرو چون قامت عاشق طلبی جلوه دهد</p></div>
<div class="m2"><p>چه کند فاخته گر طوق به گردن نکند؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما و فرهاد به یک زخم ز عالم شده ایم</p></div>
<div class="m2"><p>خون ما خواب به افسانه دشمن نکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه شب ناخن من با دل من در جنگ است</p></div>
<div class="m2"><p>چه کند صیقل اگر آینه روشن نکند؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بال پروانه ما شمع تجلی طلب است</p></div>
<div class="m2"><p>عشقبازی به جگرگوشه گلخن نکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس که غم قفل به دلهای پریشان زده است</p></div>
<div class="m2"><p>غنچه ای در دل شب یاد شکفتن نکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم صائب ز جمال تو چنان معمورست</p></div>
<div class="m2"><p>که توجه به گل و لاله ایمن نکند</p></div></div>