---
title: >-
    غزل شمارهٔ ۲۲۹۴
---
# غزل شمارهٔ ۲۲۹۴

<div class="b" id="bn1"><div class="m1"><p>آفاق را کند به نفش مشکبار صبح</p></div>
<div class="m2"><p>باشد بهار عنبر شبهای تار صبح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دم را کنند صاف ضمیران شمرده خرج</p></div>
<div class="m2"><p>از سینه می کشد نفسی را دوباره صبح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا چشمش از ستاره فشانی نشد سفید</p></div>
<div class="m2"><p>از وصل آفتاب نشد کامکار صبح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست از طلب مدار درین ره، که می کشد</p></div>
<div class="m2"><p>خورشید را ز صدق طلب در کنار صبح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زینسان که شد زمانه تهی از فروغ صدق</p></div>
<div class="m2"><p>مشکل شود سفید درین روزگار صبح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در نور صدق محو نشود ظلمت دروغ</p></div>
<div class="m2"><p>شب را کند به نیم نفس تارومار صبح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شب پرده پوش و روز سفیدست پرده در</p></div>
<div class="m2"><p>باشد ازان به چشم سیه کار، بار صبح</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما را شبی است از دل فرعون تیره تر</p></div>
<div class="m2"><p>بیهوده می برد ید بیضا به کار صبح</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تاریکی لحد نشود از چراغ کم</p></div>
<div class="m2"><p>با خاطر گرفته نیاید به کار صبح</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عمرش تمام شد به نفس راست کردنی</p></div>
<div class="m2"><p>هر چند بست پا ز شفق در نگار صبح</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پیوند تیرگی به شب من زیاده شد</p></div>
<div class="m2"><p>چندان که برد تیغ دو دم را به کار صبح</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از چشم شور، خون شفق شد، به خاک ریخت</p></div>
<div class="m2"><p>شیری که داشت در قدح زرنگار صبح</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صائب زمین پاک کند دانه را گهر</p></div>
<div class="m2"><p>از ابر دیده، قطره چندی ببار صبح</p></div></div>