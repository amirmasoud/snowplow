---
title: >-
    غزل شمارهٔ ۲۴۴۳
---
# غزل شمارهٔ ۲۴۴۳

<div class="b" id="bn1"><div class="m1"><p>عشق یکسان ناز درویش و توانگر می‌کشد</p></div>
<div class="m2"><p>این ترازو سنگ و گوهر را برابر می‌کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفتاب روز محشر بیشتر می‌سوزدش</p></div>
<div class="m2"><p>هرکه اینجا درد و داغ عشق کمتر می‌کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا به کام دل کند جولان سپند شوخ ما</p></div>
<div class="m2"><p>انتظار دامن صحرای محشر می‌کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوزخ روشندلان در بند هستی بودن است</p></div>
<div class="m2"><p>شمع این هنگامه آه از بهر صرصر می‌کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌شود از ناتوانی دشمن عاجز قوی</p></div>
<div class="m2"><p>خنجری هر خار بر نخجیر لاغر می‌کشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آتشین‌رویی که من پروانه او گشته‌ام</p></div>
<div class="m2"><p>هر شرارش روغن از چشم سمندر می‌کشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرمه خواهد کرد چشم خفتگان خاک را</p></div>
<div class="m2"><p>بر زمین این دامن نازی که محشر می‌کشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می‌کشد آن روی نازک از نگاه گرم ما</p></div>
<div class="m2"><p>آنچه از خورشید محشر سایه‌پرور می‌کشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیمی از کشتن ندارد شعله بی‌باک ما</p></div>
<div class="m2"><p>شمع ما گردن به امید صبا برمی‌کشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست هر ناشسته‌رویی قابل جولان اشک</p></div>
<div class="m2"><p>این رقم را عشق بر رخسار چون زر می‌کشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پاک گوهر را ز درد و داغ عشق اندیشه نیست</p></div>
<div class="m2"><p>در دل آتش دم خوش عود و عنبر می‌کشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کوری فرزند روشن می‌کند چشم گدا</p></div>
<div class="m2"><p>ناز دونان را سپهر سفله‌پرور می‌کشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>می‌گدازد رشته را گوهر، ولیکن رشته هم</p></div>
<div class="m2"><p>انتقام کاهش خود را ز گوهر می‌کشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سر ز جیب صبح برمی‌آورد چون آفتاب</p></div>
<div class="m2"><p>هرکه صائب در دل شب یک دو ساغر می‌کشد</p></div></div>