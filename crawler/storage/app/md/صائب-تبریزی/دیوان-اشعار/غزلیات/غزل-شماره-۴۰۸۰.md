---
title: >-
    غزل شمارهٔ ۴۰۸۰
---
# غزل شمارهٔ ۴۰۸۰

<div class="b" id="bn1"><div class="m1"><p>عیسی دمی کجاست به درد سخن رسد</p></div>
<div class="m2"><p>پیش از دم هلاک به بالین من رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانی چه روز دست دعا می رسد به عرش</p></div>
<div class="m2"><p>روزی که این غریب به تخت وطن رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالم تمام پرده فانوس حسن اوست</p></div>
<div class="m2"><p>اینجا به شمع طور پیرهن رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی پرده نقش صورت شیرین نگاشته است</p></div>
<div class="m2"><p>کوتیشه تا به داد سرکوهکن رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون شمع آههای گلوسوز می کشم</p></div>
<div class="m2"><p>تا باد صبح بر سر بالین من رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی حد ماست دست درازی به شاخ گل</p></div>
<div class="m2"><p>مارا بس است خاری اگر از چمن رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب میان اینهمه شکرلبان که هست</p></div>
<div class="m2"><p>بادام چشم کیست به مغز سخن رسد</p></div></div>