---
title: >-
    غزل شمارهٔ ۲۷۶۱
---
# غزل شمارهٔ ۲۷۶۱

<div class="b" id="bn1"><div class="m1"><p>جوش گل شد باده سرجوش می باید کشید</p></div>
<div class="m2"><p>حلقه از ساغر به گوش هوش می باید کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلشن از نازک نهالان یک تن سیمین شده است</p></div>
<div class="m2"><p>باغ را چون ابر در آغوش می باید کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در لب خاموش ساغر گفتگو بسیار هست</p></div>
<div class="m2"><p>پنبه چون مینای می از گوش می باید کشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باده گلرنگ را با ساقیان گلعذار</p></div>
<div class="m2"><p>بر رخ گلهای شبنم پوش می باید کشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست هر افسرده را از گوهر عرفان خبر</p></div>
<div class="m2"><p>حرف عشق از سینه پرجوش می باید کشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هوشیاران خون مستان را به ساغر می کنند</p></div>
<div class="m2"><p>باده را با مردم بیهوش می باید کشید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رازهای سر به مهر سینه میخانه را</p></div>
<div class="m2"><p>از لب پیمانه خاموش می باید کشید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مدتی سجاده تقوی به دوش انداختی</p></div>
<div class="m2"><p>چند روزی هم سبو بر دوش می باید کشید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می برد شیرینی بسیار دلها را زکار</p></div>
<div class="m2"><p>حرف تلخی زان لب چون نوش می باید کشید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بزم چون پرشور باشد مطربی در کار نیست</p></div>
<div class="m2"><p>باده در گلبانگ نوشانوش می باید کشید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با زبان نتوان برآمد با نواسنجان عشق</p></div>
<div class="m2"><p>حرف صائب چون بر آید گوش می باید کشید</p></div></div>