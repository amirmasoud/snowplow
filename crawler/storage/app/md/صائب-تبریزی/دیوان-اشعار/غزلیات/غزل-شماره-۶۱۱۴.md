---
title: >-
    غزل شمارهٔ ۶۱۱۴
---
# غزل شمارهٔ ۶۱۱۴

<div class="b" id="bn1"><div class="m1"><p>از گهر گرد یتیمی شست آب چشم من</p></div>
<div class="m2"><p>توتیا شد خاک در عهد سحاب چشم من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوهر بینایی من پرده سوز افتاده است</p></div>
<div class="m2"><p>کی سفیدی می تواند شد نقاب چشم من؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنچنان کز خنده گردد غنچه گل بی گره</p></div>
<div class="m2"><p>از دل بیدار باشد فتح باب چشم من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رشته اشکم بعینه سبحه بگسسته است</p></div>
<div class="m2"><p>بس که می آید غبارآلود آب چشم من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موجه تردست را خشکی کند سوهان روح</p></div>
<div class="m2"><p>آب بردارد گر از دریا سحاب چشم من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیده من تا به خال دلفریب او فتاد</p></div>
<div class="m2"><p>مردمک شد نقطه سهو کتاب چشم من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تشنه عرض گهر چون تنگ چشمان نیستم</p></div>
<div class="m2"><p>گریه بی اشک باشد انتخاب چشم من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بس که می ریزم به تلخی اشک، هر مژگان من</p></div>
<div class="m2"><p>می شود انگشت زنهاری ز آب چشم من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیده بیدار انجم محو شد در خواب روز</p></div>
<div class="m2"><p>همچنان در پرده غیب است خواب چشم من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون تواند بحر با من لاف همچشمی زدن؟</p></div>
<div class="m2"><p>می زند پهلو به گردون هر حباب چشم من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جای حیرت نیست گردد گر حصاری در تنور</p></div>
<div class="m2"><p>در مقام لاف، طوفان از حجاب چشم من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه ز ساقی ناز و نه از خم بزرگی می کشم</p></div>
<div class="m2"><p>تا ز خون دل مهیا شد شراب چشم من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون رگ سنگ است صائب در نظر مژگان مرا</p></div>
<div class="m2"><p>بس که از غفلت گرانسنگ است خواب چشم من</p></div></div>