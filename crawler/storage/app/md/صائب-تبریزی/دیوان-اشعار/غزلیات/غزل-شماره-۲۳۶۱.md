---
title: >-
    غزل شمارهٔ ۲۳۶۱
---
# غزل شمارهٔ ۲۳۶۱

<div class="b" id="bn1"><div class="m1"><p>هر که در دنیا فانی زاد عقبی جمع کرد</p></div>
<div class="m2"><p>قسمت امروز خورد و دل زفردا جمع کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پایکوبان می شود ز آوازه طبل رحیل</p></div>
<div class="m2"><p>خویش را پیش از سفر چون راه پیما جمع کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجمر از تسخیر بوی عود و عنبر عاجزست</p></div>
<div class="m2"><p>چون تواند آسمان بال و پر ما جمع کرد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غوطه زد در چشمه خورشید تا وا کرد چشم</p></div>
<div class="m2"><p>هر که چون شبنم درین گلزار خود را جمع کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با سر آزاده این بیهوده گردی تا به چند؟</p></div>
<div class="m2"><p>کوه زیر تیغ در دامان خود پا جمع کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حاصل جمعیت دنیا پریشان خاطری است</p></div>
<div class="m2"><p>روی جمعیت نبیند هر که دنیا جمع کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقده ای چون آسمان در رشته کارش فتاد</p></div>
<div class="m2"><p>با تجر دهر که سوزن همچون عیسی جمع کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که در جمعیت اسباب عمرش صرف شد</p></div>
<div class="m2"><p>پرده های خواب بهر چشم بینا جمع کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دست در پیری به هم سودن ندارد حاصلی</p></div>
<div class="m2"><p>پیش ازین سیلاب می بایست خود را جمع کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خرج روی سخت آهن شد به اندک فرصتی</p></div>
<div class="m2"><p>خرده چندی که در دل سنگ خارا جمع کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شد سویدا حلقه بیرون در این خانه را</p></div>
<div class="m2"><p>بس که دل از سادگی تخم تمنا جمع کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عاقلان را بهر جمعیت پناهی لازم است</p></div>
<div class="m2"><p>ورنه مجنون می تواند دل به صحرا جمع کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در گره کار تهیدستان نمی ماند مدام</p></div>
<div class="m2"><p>قسمت پیمانه گردد هر چه مینا جمع کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>می شود یک لحظه خرج چشم گوهربار من</p></div>
<div class="m2"><p>آنچه از گوهر تمام عمر دریا جمع کرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نیست از غفلت، خمار چشم لیلی ظالم است</p></div>
<div class="m2"><p>گرد خود مجنون ما گر آهوان را جمع کرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>من همان دیوانه ام کز دانه زنجیر من</p></div>
<div class="m2"><p>خرمنی هر کس درین دامان صحرا جمع کرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از دلم هر پاره صائب پیش آتشپاره ای است</p></div>
<div class="m2"><p>چون توانم خاطر خود را زصدجا جمع کرد؟</p></div></div>