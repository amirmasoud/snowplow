---
title: >-
    غزل شمارهٔ ۹۴۵
---
# غزل شمارهٔ ۹۴۵

<div class="b" id="bn1"><div class="m1"><p>از دل سخت بتان از ناله ای فریاد خاست</p></div>
<div class="m2"><p>خوش همایون طایری زین بیضه فولاد خاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من که در خاموشی از آیینه می بردم سبق</p></div>
<div class="m2"><p>نوخطی دیدم که از هر موی من فریاد خاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا دل سنگین شیرین هیچ جا منزل نکرد</p></div>
<div class="m2"><p>هر شراری کز زبان تیشه فرهاد خاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ماه بر گردن نهاد از هاله طوق بندگی</p></div>
<div class="m2"><p>سرو موزون تو تا از گلشن ایجاد خاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می کند چون دام، چشم شوخ انجم را به خاک</p></div>
<div class="m2"><p>از خرام او مرا گردی که از بنیاد خاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در شکست قلب ما آن زلف و کاکل بس نبود؟</p></div>
<div class="m2"><p>کان غبار خط مشکین هم پی امداد خاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وای بر بی طاقتان، کز روی آتشناک او</p></div>
<div class="m2"><p>چون سپند از مهر خاموشی مرا فریاد خاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ساغرش چون لاله صائب دایم از می سرخ روست</p></div>
<div class="m2"><p>هر که از خاک سیه با داغ مادرزاد خاست</p></div></div>