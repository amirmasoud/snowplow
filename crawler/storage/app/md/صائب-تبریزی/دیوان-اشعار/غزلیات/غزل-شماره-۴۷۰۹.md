---
title: >-
    غزل شمارهٔ ۴۷۰۹
---
# غزل شمارهٔ ۴۷۰۹

<div class="b" id="bn1"><div class="m1"><p>از بیغمان جمیله غم را نگاه دار</p></div>
<div class="m2"><p>از چشم شور دردوالم را نگاه دار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شادی به حسن عاقبت غم نمی رسد</p></div>
<div class="m2"><p>بیش از نشاط، عزت غم را نگاه دار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشکن به حرف سخت دل اولیای حق</p></div>
<div class="m2"><p>پاس کبوتران حرم را نگاه دار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رحمی به روزنامه اعمال خویش کن</p></div>
<div class="m2"><p>از کجروی زبان قلم را نگاه دار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فتح و ظفر به آه سحر گاه بسته است</p></div>
<div class="m2"><p>از تیغ بیش پاس علم را نگاه دار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی روزی حلال دعا نیست مستجاب</p></div>
<div class="m2"><p>از لقمه حرام شکم را نگاه دار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آه ستم رسیده محال است رد شود</p></div>
<div class="m2"><p>ای سنگدل عنان ستم را نگاه دار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگذار لب به حرف طمع واکند فقیر</p></div>
<div class="m2"><p>زنهار آبروی کرم را نگاه دار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون مایه ات وفا به فشاندن نمی کند</p></div>
<div class="m2"><p>باری به حسن خلق خدم را نگاه دار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هنگام صبح نغمه سرایان بوستان</p></div>
<div class="m2"><p>فریاد می کنند که دم را نگاه دار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از قیل و قال تیره مکن وقت اهل حال</p></div>
<div class="m2"><p>صائب به پیش آینه دم را نگاه دار</p></div></div>