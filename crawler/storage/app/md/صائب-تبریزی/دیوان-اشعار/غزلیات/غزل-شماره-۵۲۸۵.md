---
title: >-
    غزل شمارهٔ ۵۲۸۵
---
# غزل شمارهٔ ۵۲۸۵

<div class="b" id="bn1"><div class="m1"><p>روزگاری شد ز چشم اعتبار افتاده‌ام</p></div>
<div class="m2"><p>چون نگاه آشنا از چشم یار افتاده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست رغبت کس نمی‌سازد به سوی من دراز</p></div>
<div class="m2"><p>چون گل پژمرده بر روی مزار افتاده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اختیارم نیست چون گرداب بر سرگشتگی</p></div>
<div class="m2"><p>نبض موجم در تپیدن بی‌قرار افتاده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عقده‌ای هرگز نکردم باز از کار کسی</p></div>
<div class="m2"><p>در چمن بیکار چون دست چنار افتاده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیستم یک چشم زد ایمن ز آسیب شکست</p></div>
<div class="m2"><p>گوییا آیینه‌ام در زنگبار افتاده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو گوهر گر دلم از سنگ گردد دور نیست</p></div>
<div class="m2"><p>دور از مژگان ابر نوبهار افتاده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من که صائب کار یک‌رو کرده‌ام با کاینات</p></div>
<div class="m2"><p>در میان مردم عالم چه کار افتاده‌ام</p></div></div>