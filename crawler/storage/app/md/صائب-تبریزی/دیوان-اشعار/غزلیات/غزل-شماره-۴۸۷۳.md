---
title: >-
    غزل شمارهٔ ۴۸۷۳
---
# غزل شمارهٔ ۴۸۷۳

<div class="b" id="bn1"><div class="m1"><p>روح قدسی، بیش ازین درتنگنای تن مباش</p></div>
<div class="m2"><p>عیسی وقتی، گره در چشمه سوزن مباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از لباس تن مجرد کن روان پاک را</p></div>
<div class="m2"><p>یوسف سیمین تنی، درقید پیراهن مباش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرمه کن از برق بینش پرده های خواب را</p></div>
<div class="m2"><p>بیش ازین در زیر ابر ای دیده روشن مباش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ازسنان آه، بام چرخ راسوراخ کن</p></div>
<div class="m2"><p>بیگنه چندین درین زندان بی روزن مباش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می توان دل را به همت بر فراز عرش برد</p></div>
<div class="m2"><p>رستمی داری، اسیر چاه چون بیژن مباش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد سفید از انتظارت چشم خلد از جوی شیر</p></div>
<div class="m2"><p>همچو بلبل محو آب ورنگ این گلشن مباش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون سلیمان خاتم فرمان برآر ازدست دیو</p></div>
<div class="m2"><p>قهرمان عالمی، فرمان پذیر تن مباش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درزمین چهره خود دانه اشکی بکار</p></div>
<div class="m2"><p>در غم آب و زمین و دانه و خرمن مباش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون ز زنگار خودی آیینه راپرداختی</p></div>
<div class="m2"><p>همچو خاکستر مقیم گوشه گلخن مباش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بادرشت و نرم یکسان چون ترازو کن سلوک</p></div>
<div class="m2"><p>درمقام عیبجویی چشم پرویزن مباش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می توان دیدن به چشم عیبجویان عیب خویش</p></div>
<div class="m2"><p>تا میسر می شود زنهار بی دشمن مباش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این جواب آن که می گوید حکیم غزنوی</p></div>
<div class="m2"><p>ای سنایی خواجه جانی غلام تن مباش</p></div></div>