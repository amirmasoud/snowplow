---
title: >-
    غزل شمارهٔ ۲۵۳۴
---
# غزل شمارهٔ ۲۵۳۴

<div class="b" id="bn1"><div class="m1"><p>بوی پیراهن زلیخا را کجا روشن کند؟</p></div>
<div class="m2"><p>شمع هیهات است پای خویش را روشن کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم بر راهند در کنعان دو صد امیدوار</p></div>
<div class="m2"><p>تا نسیم پیرهن چشم که را روشن کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می کند تأثیر در آهن دلان هم حرف سخت</p></div>
<div class="m2"><p>چشم سوزن را اگر آهن ربا روشن کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نسیم صبح چون خورشید روشن تر شود</p></div>
<div class="m2"><p>هر چراغی را که آه گرم ما روشن کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست دلسوزی درین ظلمت سرا از رهبران</p></div>
<div class="m2"><p>گرم رفتاری مگر راه مرا روشن کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کار مردم نیست غیر از جستجوی عیب هم</p></div>
<div class="m2"><p>تا به عیب خود خدا چشم که را روشن کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوختم از رشک، تا کی در حضور چشم من</p></div>
<div class="m2"><p>آن خودآرا خانه آیینه را روشن کند؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می کند فانوس، صائب پرده داری شمع را</p></div>
<div class="m2"><p>چهره آیینه رویان را حیا روشن کند</p></div></div>