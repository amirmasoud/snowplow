---
title: >-
    غزل شمارهٔ ۹۴۳
---
# غزل شمارهٔ ۹۴۳

<div class="b" id="bn1"><div class="m1"><p>خشک شد کشت امیدم ابر احسانی کجاست؟</p></div>
<div class="m2"><p>آب را گر پا به گل رفته است بارانی کجاست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند لرزد شمع من بر خود ز بیداد صبا؟</p></div>
<div class="m2"><p>نیستم گر قابل فانوس، دامانی کجاست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد ز خشکی دود ریحان در سفال تشنه ام</p></div>
<div class="m2"><p>آب اگر سنگین دل افتاده است بارانی کجاست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آب چون نبود تیمم می توان کردن به خاک</p></div>
<div class="m2"><p>نیست گر زلف پریشان، خط ریحانی کجاست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا به یک جولان برآرد دود از خرمن مرا</p></div>
<div class="m2"><p>در میان نی سواران برق جولانی کجاست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز انتظار قطره ای باران، لب خشک صدف</p></div>
<div class="m2"><p>شد پر از تبخال، یارب ابر نیسانی کجاست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از شب و روز مکرر دل سیه گردیده است</p></div>
<div class="m2"><p>روی آتشناک و زلف عنبرافشانی کجاست؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درد و داغ عشق از دل روی گردان گشته است</p></div>
<div class="m2"><p>این صف برگشته را برگشته مژگانی کجاست؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این دل سرگشته را چون گوی در میدان خاک</p></div>
<div class="m2"><p>رفت سرگردانی از حد، دست و چوگانی کجاست؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شد ز بی عشقی سیه عالم به چشم داغ من</p></div>
<div class="m2"><p>تا به شور آرد مرا صائب نمکدانی کجاست؟</p></div></div>