---
title: >-
    غزل شمارهٔ ۵۰۰۳
---
# غزل شمارهٔ ۵۰۰۳

<div class="b" id="bn1"><div class="m1"><p>لطیفه ای عجب است این که لعل سیرابش</p></div>
<div class="m2"><p>مدام می چکد وکم نمی شود آبش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی که راه به بحر محیط وحدت برد</p></div>
<div class="m2"><p>غریب نیست درآغوش دشت سیلابش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو مرده ای است که خوابانده اند در کافور</p></div>
<div class="m2"><p>کسی که در شب مهتاب می برد خوابش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو تیر سخت کمان می‌جهد برون عارف</p></div>
<div class="m2"><p>ز مسجدی که بود رو به خلق محرابش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قدی که خم شود از بار درد و غم صائب</p></div>
<div class="m2"><p>نهنگ می کشد از بحر عشق قلابش</p></div></div>