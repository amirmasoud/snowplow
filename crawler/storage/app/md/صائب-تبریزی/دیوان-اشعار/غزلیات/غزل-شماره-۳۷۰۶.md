---
title: >-
    غزل شمارهٔ ۳۷۰۶
---
# غزل شمارهٔ ۳۷۰۶

<div class="b" id="bn1"><div class="m1"><p>بغیر اشک که راه نگاه من بندد</p></div>
<div class="m2"><p>که دیده قافله ای چشم راهزن بندد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روا مدار خدایا که متحست زر می</p></div>
<div class="m2"><p>به زور گیرد و بر گوشه کفن بندد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بغیر سوختن و گریه کردن و مردن</p></div>
<div class="m2"><p>چه طرف شمع ازین تیره انجمن بندد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمی کند گله ام گوش، اگرچه بتواند</p></div>
<div class="m2"><p>در هزار شکایت به یک سخن بندد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نسیم مصر به کوی تو گر گذار کند</p></div>
<div class="m2"><p>عبیر خاک رهت را به پیرهن بندد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به انتقام دل پرخراش، جا دارد</p></div>
<div class="m2"><p>که بیستون کمر قتل کوهکن بندد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عجب مدار ز هر مو چو چنگ اگر نالم</p></div>
<div class="m2"><p>که عشق زمزمه بر تار پیرهن بندد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خزان ز سردی آهم چو بید می لرزد</p></div>
<div class="m2"><p>اگرچه در نفسی نخل صد چمن بندد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به این ثبات قدم شرم باد شبنم را</p></div>
<div class="m2"><p>که صف برابر خورشید تیغ زن بندد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ازین چه سود که دیوار باغ افتاده است؟</p></div>
<div class="m2"><p>که شرم عشق همان در به روی من بندد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نکرد از زر گل بی نیاز بلبل را</p></div>
<div class="m2"><p>کدام مرغ، دگر دل درین چمن بندد؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که غیر شاعر شیرین سخن دگر صائب</p></div>
<div class="m2"><p>بلند نام شود چون لب از سخن بندد؟</p></div></div>