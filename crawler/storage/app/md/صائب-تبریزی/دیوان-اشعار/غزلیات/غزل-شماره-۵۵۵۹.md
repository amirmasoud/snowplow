---
title: >-
    غزل شمارهٔ ۵۵۵۹
---
# غزل شمارهٔ ۵۵۵۹

<div class="b" id="bn1"><div class="m1"><p>همان بیگانه ام با خلق هر چند آشنا باشم</p></div>
<div class="m2"><p>چو نور دیده در یک خانه از مردم جدا باشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز گرد سرمه چشم غزالان است خاک من</p></div>
<div class="m2"><p>شود بیگانه از عالم به هر کس آشنا باشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپهر از کجرویها توتیا کرد استخوانم را</p></div>
<div class="m2"><p>چو بارم آرد شد دیگر چرا در آسیا باشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر چه سایه ام منشور دولت در بغل دارد</p></div>
<div class="m2"><p>برای استخوان سرگشته دایم چون هما باشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جان بخشی سیاهی از سرداغم نمی خیزد</p></div>
<div class="m2"><p>همان از تیره بختانم اگر آب بقا باشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کمند جذبه من کوه آهن بر کمر دارد</p></div>
<div class="m2"><p>به سوزن برنمی آیم اگر آهن ربا باشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همان بهتر کز این محفل برآیم آستین افشان</p></div>
<div class="m2"><p>که بار گردن خلقم اگر دست دعا باشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر چه سنگ را در ناله آرد بار درد من</p></div>
<div class="m2"><p>فتد چون سیل اگر بر کوه راهم بی صدا باشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بحمدالله مکافات عمل از پیشدستیها</p></div>
<div class="m2"><p>مرا نگذاشت در اندیشه روز جزا باشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قمار پاکبازی مهره بی نقش می خواهد</p></div>
<div class="m2"><p>چه افتاده است در ششدرز نقش بوریا باشم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ندارم آبروی شبنمی در پیشگاه گل</p></div>
<div class="m2"><p>به این خواری و بیقدری درین گلشن چرا باشم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز پیش پا ندیدن سیل آمد راست تا دریا</p></div>
<div class="m2"><p>چه غماز بلند و پست عالم چون عصا باشم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز راه خاکساری کسب عزت کرده ام صائب</p></div>
<div class="m2"><p>که چون خورشید هم بالای سر، هم زیر پا باشم</p></div></div>