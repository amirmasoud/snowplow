---
title: >-
    غزل شمارهٔ ۴۸۴۶
---
# غزل شمارهٔ ۴۸۴۶

<div class="b" id="bn1"><div class="m1"><p>زپرده داری هستی است در حجاب، نفس</p></div>
<div class="m2"><p>که درفنا زته دل کشد حباب، نفس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز اضطراب دل آید به اضطراب نفس</p></div>
<div class="m2"><p>زآرمیدگی دل رود به خواب نفس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میان گریه وگفتار من تفاوت نیست</p></div>
<div class="m2"><p>زبس که در دل گرمم شده است آب نفس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درین محیط، نشان گهر زجمعی جوی</p></div>
<div class="m2"><p>که سر به مهر کشیدندچون حباب نفس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>علاج خنجر سیراب عشق تسلیم است</p></div>
<div class="m2"><p>چه دست و پای تواند زدن درآب نفس؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به مرگ باز نمانند سالکان ز طلب</p></div>
<div class="m2"><p>همان تردد خود می کند به خواب نفس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غبار حادثه از یکدیگر نمی گسلد</p></div>
<div class="m2"><p>بجان رسید درین منزل خراب نفس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو آب خضر سیه پوش شد محیط سراب</p></div>
<div class="m2"><p>ز بس که سوخت درین دشت سینه تاب نفس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز عرض حال ازان خامشند سوختگان</p></div>
<div class="m2"><p>که شعله می کشد ار جانب کباب نفس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به جستجو نتوان دامن وصال گرفت</p></div>
<div class="m2"><p>و گرنه سوخت درین راه آفتاب نفس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز وصف لعل لبش شد حدیث من رنگین</p></div>
<div class="m2"><p>اگر چه رنگ نمی گیرد از شراب نفس</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>محاسبان قیامت حساب می طلبند</p></div>
<div class="m2"><p>درین بساط مکن خرج بی حساب نفس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بنای خانه گردون چو همتش پست است</p></div>
<div class="m2"><p>چگونه راست کند قد درین خراب نفس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زبیم خوی تو چون موی زنگیان شده است</p></div>
<div class="m2"><p>درون سینه صائب ز پیچ و تاب نفس</p></div></div>