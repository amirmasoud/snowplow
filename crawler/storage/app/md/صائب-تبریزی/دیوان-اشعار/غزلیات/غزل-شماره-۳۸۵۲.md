---
title: >-
    غزل شمارهٔ ۳۸۵۲
---
# غزل شمارهٔ ۳۸۵۲

<div class="b" id="bn1"><div class="m1"><p>به خاک دیر جبینی که آشنا باشد</p></div>
<div class="m2"><p>اگر به کعبه رود روی بر قفا باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشاط هردو جهان بی حضور دل بادست</p></div>
<div class="m2"><p>بجاست تخت سلیمان چودل بجا باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چونیست نشأه مستی ز پادشاهی کم</p></div>
<div class="m2"><p>بط شراب چراکمتر ازهما باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به جرم جوهرذاتی وپاکی گوهر</p></div>
<div class="m2"><p>چوتیغ قسمت من آب ناشتا باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمک به دیده گستاخ شبنم افشانند</p></div>
<div class="m2"><p>در آن چمن که نگهبان در او احیا باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برآر رخت اقامت زچار موجه صوف</p></div>
<div class="m2"><p>که حرز عافیت از نقش بوریا باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بشو ز کاسه سرگرد عقل را به شراب</p></div>
<div class="m2"><p>مهل که جام جم عشق بی صفاباشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کمند جاذبه چهره شکسته من</p></div>
<div class="m2"><p>نهشت کاه در آغوش کهربا باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زشرم عقده سردرگم من آب شود</p></div>
<div class="m2"><p>اگر چه غنچه پیکان گرهگشا باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنان ضعیف شدم در فراق اوصائب</p></div>
<div class="m2"><p>که بال سیر من از جذب کهربا باشد</p></div></div>