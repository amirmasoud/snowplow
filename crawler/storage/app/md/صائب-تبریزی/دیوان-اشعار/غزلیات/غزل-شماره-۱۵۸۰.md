---
title: >-
    غزل شمارهٔ ۱۵۸۰
---
# غزل شمارهٔ ۱۵۸۰

<div class="b" id="bn1"><div class="m1"><p>پاک شد دل چو به آن آینه سیما پیوست</p></div>
<div class="m2"><p>سیل ناصاف نماند چو به دریا پیوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می کشد سلسله موج به دریا آخر</p></div>
<div class="m2"><p>وقت دل خوش که به آن زلف چلیپا پیوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مادر از دامن فرزند نمی دارد دست</p></div>
<div class="m2"><p>طعمه خاک شودهر که به دنیا پیوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیل چون پهن شود، خرج زمین می گردد</p></div>
<div class="m2"><p>جای رحم است بر آن دل که به صد جا پیوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که دارد نظر پاک، نماند به زمین</p></div>
<div class="m2"><p>سوزن از دیده روشن به مسیحا پیوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دورگردست ز افسردگی خویش همان</p></div>
<div class="m2"><p>گر به ظاهر کف بی مغز به دریا پیوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دست در دامن خورشید زند چون شبنم</p></div>
<div class="m2"><p>هر که صائب به دل و دیده بینا پیوست</p></div></div>