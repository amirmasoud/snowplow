---
title: >-
    غزل شمارهٔ ۸۱۴
---
# غزل شمارهٔ ۸۱۴

<div class="b" id="bn1"><div class="m1"><p>نه کفر شناسد دل حیران و نه دین را</p></div>
<div class="m2"><p>از نقش چپ و راست خبر نیست نگین را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند حجاب تو زبان بند هوسهاست</p></div>
<div class="m2"><p>زنهار ز سر باز مکن چین جبین را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم تو به دل فرصت نظاره نبخشد</p></div>
<div class="m2"><p>این صید ز صیاد گرفته است کمین را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر جا لب لعل تو به گفتار درآید</p></div>
<div class="m2"><p>در آب گهر غوطه دهد مغز زمین را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آخر که ترا گفت که از خانه خرابان</p></div>
<div class="m2"><p>تنها کنی آباد همین خانه زین را؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آسوده بود عشق ز بی تابی عاشق</p></div>
<div class="m2"><p>از زلزله خاک چه غم چرخ برین را؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگذار به لعل تو فتد چشم هوسناک</p></div>
<div class="m2"><p>کاین ابر بود ریگ روان آب نگین را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می ترسم ازان چشم سیه مست که آخر</p></div>
<div class="m2"><p>از راه برد صائب سجاده نشین را</p></div></div>