---
title: >-
    غزل شمارهٔ ۶۸۶۴
---
# غزل شمارهٔ ۶۸۶۴

<div class="b" id="bn1"><div class="m1"><p>اگر چه هست به ظاهر خراب درویشی</p></div>
<div class="m2"><p>ز وصل گنج بود کامیاب درویشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترا ز درد سر آن جهان خلاص کند</p></div>
<div class="m2"><p>اگر چه تلخ بود چون گلاب درویشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازان به خرقه پشمین چو نافه ساخته است</p></div>
<div class="m2"><p>که خون خویش کند مشک ناب درویشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزار گوهر شهوار در دل شبها</p></div>
<div class="m2"><p>کشد به رشته ز هر پیچ و تاب درویشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همیشه روزیش از خوان فیض آماده است</p></div>
<div class="m2"><p>نمی خورد غم نان را چو آب درویشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترا به روز حساب این سخن شود معلوم</p></div>
<div class="m2"><p>که بوده سلطنت بی حساب درویشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ازان به گوهر مقصود راه یافته است</p></div>
<div class="m2"><p>که داده هر دو جهان را به آب درویشی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تمام موجه دریا اگر شود شمشیر</p></div>
<div class="m2"><p>نمی خورد غم سر چون حباب درویشی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حصار زیر و زبر گشتن است ویرانی</p></div>
<div class="m2"><p>ز سیل فتنه نگردد خراب درویشی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز لوح سینه من نقش هر دو عالم شست</p></div>
<div class="m2"><p>دگر چه نقش زند تا بر آب درویشی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نقابدار کند آفتاب را صائب</p></div>
<div class="m2"><p>اگر برافکند از رخ نقاب درویشی</p></div></div>