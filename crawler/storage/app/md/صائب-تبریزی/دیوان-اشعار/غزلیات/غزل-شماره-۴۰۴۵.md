---
title: >-
    غزل شمارهٔ ۴۰۴۵
---
# غزل شمارهٔ ۴۰۴۵

<div class="b" id="bn1"><div class="m1"><p>زخمی که ره به لذت ناسور می برد</p></div>
<div class="m2"><p>فیض نمک ز مرهم کافور می برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پروانه مرا جگر ماهتاب نیست</p></div>
<div class="m2"><p>موسی مرا به انجمن طور می برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از جمع مال رزق حریص آه حسرت است</p></div>
<div class="m2"><p>ازنوش غیر نیش چه زنبورمی برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اکنون که چرخ بر سر انصاف آمده است</p></div>
<div class="m2"><p>فیروزه مرا به نشابور می برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان ساقی کریم مرا هیچ شکوه نیست</p></div>
<div class="m2"><p>حیرت مرا ز میکده مخمور می برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا کی ز حسرت لب خاموش خون خورم</p></div>
<div class="m2"><p>این آرزو مرا به لب گور می برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما گرد هستی از نمد خود فشانده ایم</p></div>
<div class="m2"><p>دار فنا چه صرفه ز منصور می برد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زنهار از دماغ برون کن غرور را</p></div>
<div class="m2"><p>کاین باد افسر از سر فغفور می برد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می هوش می رباید و این طرفه تر که یار</p></div>
<div class="m2"><p>هوش مرا به نرگس مخمور می برد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نزدیکتر به کعبه مقصود می شوم</p></div>
<div class="m2"><p>چندان که اضطراب مرا دور می برد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب فریب مرهم راحت نمی خورد</p></div>
<div class="m2"><p>داغ دلی که غیرت ناسور می برد</p></div></div>