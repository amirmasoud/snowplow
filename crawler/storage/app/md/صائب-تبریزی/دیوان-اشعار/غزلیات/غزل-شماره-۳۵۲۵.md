---
title: >-
    غزل شمارهٔ ۳۵۲۵
---
# غزل شمارهٔ ۳۵۲۵

<div class="b" id="bn1"><div class="m1"><p>چون نفس زیر فلک دل به هوس راست کند؟</p></div>
<div class="m2"><p>زیر سرپوش، چراغی چه نفس راست کند؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون مگس گیر، ز تسبیح ریایی زاهد</p></div>
<div class="m2"><p>دام تزویر پی صید مگس راست کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل افسرده به فریاد نگردد بیدار</p></div>
<div class="m2"><p>ره خوابیده کجا قد به جرس راست کند؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا به مقصد نرسد شوق نگیرد آرام</p></div>
<div class="m2"><p>سیل در راه محال است نفس راست کند</p></div></div>