---
title: >-
    غزل شمارهٔ ۲۲۵۲
---
# غزل شمارهٔ ۲۲۵۲

<div class="b" id="bn1"><div class="m1"><p>از حسن تو جیب خاک پر ماه است</p></div>
<div class="m2"><p>یوسف ز خجالت تو در چاه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خالی که ز گردن تو می تابد</p></div>
<div class="m2"><p>همچشم ستاره سحرگاه است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگذار جگی جگی ببوسم من</p></div>
<div class="m2"><p>خالی که بر آن جگی جگی گاه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عمر عاشق ز خضر کمتر نیست</p></div>
<div class="m2"><p>این رشته ز پیچ و تاب کوتاه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر آینه راست جوهر خاصی</p></div>
<div class="m2"><p>آیینه سینه جوهرش آه است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در منزل کفر و دین نمی ماند</p></div>
<div class="m2"><p>با عشق سبکروی که همراه است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>انگشت به هیچ حرف نگذارد</p></div>
<div class="m2"><p>از درد سخن کسی که آگاه است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب ز زمین دل برون آور</p></div>
<div class="m2"><p>طول املی که ریشه آه است</p></div></div>