---
title: >-
    غزل شمارهٔ ۵۰۹۵
---
# غزل شمارهٔ ۵۰۹۵

<div class="b" id="bn1"><div class="m1"><p>ز موج لطف ،آن سیمین بنا گوش</p></div>
<div class="m2"><p>مرا کرده است چون خط حلقه درگوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به چشم من بهشت و جوی شیرست</p></div>
<div class="m2"><p>رخ گلرنگ و آن صبح بنا گوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همان درزیر خاکم می پرد چشم</p></div>
<div class="m2"><p>که این می در قدح ننشیند از جوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیاسوده است تا واکرده ام چشم</p></div>
<div class="m2"><p>مرا چون غنچه از خمیازه آغوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو خط از چهره خوبان، هویداست</p></div>
<div class="m2"><p>به چشم موشکافان بیت ابرویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خلد چون خار، گل درپرده چشم</p></div>
<div class="m2"><p>گران چون سنگ باشد پنبه درگوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خرام خامه مشکین صائب</p></div>
<div class="m2"><p>بود از شوخ چشمان خطاپوش</p></div></div>