---
title: >-
    غزل شمارهٔ ۴۵۸۰
---
# غزل شمارهٔ ۴۵۸۰

<div class="b" id="bn1"><div class="m1"><p>تا تو ای سرو روان از باغ بیرون رفته ای</p></div>
<div class="m2"><p>می تراود ناله از هر غنچه ای منقاروار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش ارباب بصیرت چشم خواب آلوده ای است</p></div>
<div class="m2"><p>گر به ظاهر دولت دنیا بود بیداروار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشرتم راگریه خونین بود در آستین</p></div>
<div class="m2"><p>بوی خون گل می کند ازخنده ام سوفاروار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می توان دانست گنجی هست درویرانه اش</p></div>
<div class="m2"><p>هر که می دزدد زمردم خویش راعیاروار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زنده کن دل را به نور عشق، بر افلاک رو</p></div>
<div class="m2"><p>ورنه خرج کرکسان خواهی شدن مرداروار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست صائب درمحبت پیچ وتاب من عبث</p></div>
<div class="m2"><p>حلقه بر در می زنم گنج گهر را ماروار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر نمی پیچم ز خار سرزنش دیواروار</p></div>
<div class="m2"><p>تیغ را جا بر سر خود می دهم کهساروار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر نمی دارد ترا از خاک بوی پیرهن</p></div>
<div class="m2"><p>تا نسازی ازگرستن چشم خود دستاروار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون سلیمانی است هر لخت از دل صد پاره ام</p></div>
<div class="m2"><p>بس که پیچیده است دردل آه من زناروار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با کمال خرده بینی نقطه خالش مرا</p></div>
<div class="m2"><p>کرد در سر گشتگی ثابت قدم پرگاروار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طوطی شیرین زبانم لیک آن آیینه رو</p></div>
<div class="m2"><p>می شمارد سبزه بیگانه ام زنگاروار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برگ عیشم چون خزان پا در رکاب رحلت است</p></div>
<div class="m2"><p>یک دهن افزون نباشد خنده ام گلزاروار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر چه نیل چشم زخمم شاهدان باغ را</p></div>
<div class="m2"><p>می زنند آتش به جان ناتوانم خاروار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا میسر می شود، کردارخود پوشیده دار</p></div>
<div class="m2"><p>تانیفتی دردهان مردمان گفتاروار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>میل عقبی کن زدنیا، کآدم خاکی نهاد</p></div>
<div class="m2"><p>می فتد، مایل به هر جانب شود، دیواروار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بس که کردم سازگاری، غم به آن سنگین دلی</p></div>
<div class="m2"><p>می کند اکنون پرستاری مراغمخواروار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر نپیچم یک زمان بر خود، پریشان می شوم</p></div>
<div class="m2"><p>می کند شیرازه، پیچیدن مراطوماروار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بی تو گر بالین من سازند از زانوی حور</p></div>
<div class="m2"><p>می کنم تغییر بالین هر زمان بیماروار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>می شود مهر لب اظهار من شرم حضور</p></div>
<div class="m2"><p>ورنه دارم شکوه ها درآستین طوماروار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پیش ازین اغیاردر چشمم نمود یارداشت</p></div>
<div class="m2"><p>این زمان ازیار وحشت می کنم اغیاروار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ذره ای از حسن عالمگیر او بی بهره نیست</p></div>
<div class="m2"><p>از درو دیوار لذت می برم دیداروار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>رشته عمرش ز پیچ وتاب می گردد گره</p></div>
<div class="m2"><p>هر که با موی میان دارد سری زناروار</p></div></div>