---
title: >-
    غزل شمارهٔ ۶۵۲۳
---
# غزل شمارهٔ ۶۵۲۳

<div class="b" id="bn1"><div class="m1"><p>ز من شکیب به قدر دل فگار بجو</p></div>
<div class="m2"><p>به من دلی بنما بعد ازان قرار بجو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا به مرگ ز کوی تو پای رفتن نیست</p></div>
<div class="m2"><p>غبار من به سر راه انتظار بجو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکستگی طلبم، از میان اگر بروم</p></div>
<div class="m2"><p>مرا به سلسله زلف آن نگار بجو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گهرفشانی ابر سیاه مشهورست</p></div>
<div class="m2"><p>مراد در دل شب ای سیاه کار بجو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو دور من بسر آید ز گردش دوران</p></div>
<div class="m2"><p>مرا به حلقه آن چشم پرخمار بجو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلی که داشتی ای جان شده است هر جایی</p></div>
<div class="m2"><p>دل دگر ز برای تن فگار بجو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نکرده گریه تمنای بخت سبز مکن</p></div>
<div class="m2"><p>بریز دانه خود در زمین، بهار بجو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به امتحان بکش از ریگ روغن بادام</p></div>
<div class="m2"><p>دگر مروت ازین اهل روزگار بجو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه ذره بی سرو پا شو درین جهان صائب</p></div>
<div class="m2"><p>دگر به حضرت خورشید عشق بار بجو</p></div></div>