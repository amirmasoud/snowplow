---
title: >-
    غزل شمارهٔ ۴۵۶۰
---
# غزل شمارهٔ ۴۵۶۰

<div class="b" id="bn1"><div class="m1"><p>ای دل غافل زمانی از گریبان سر برآر</p></div>
<div class="m2"><p>نیستی از مورکم، از شوق شکر پر برآر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبض هر خاری که می جنبددرین صحرابگیر</p></div>
<div class="m2"><p>از گریبان فنا چون برق، دیگر سربرآر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درکتاب عالم از روی بصیرت غورکن</p></div>
<div class="m2"><p>چون به معنی راه بردی دود ازین دفتر برآر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر دلها چه می گردی برای حبه ای؟</p></div>
<div class="m2"><p>دست کن در جیب خود چون غنچه گل زر بر آر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش نیسان چون صدف تا کی دهن خواهی گشود؟</p></div>
<div class="m2"><p>دم چو غواصان گره کن در جگر،گوهر برآر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساده کن لوح دل از نقش ونگار آرزو</p></div>
<div class="m2"><p>هر نفس از جیب خود آیینه دیگربرآر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چند باشی عنکبوت رشته طول امل ؟</p></div>
<div class="m2"><p>از گریبان تجرد همچو سوزن سر برآر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گوشهٔ بی توشه‌ای کن از دو عالم اختیار</p></div>
<div class="m2"><p>از غبار دل به روی آرزوها در برآر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از نسیمی شعله هستی شود پادررکاب</p></div>
<div class="m2"><p>فرصتی تا هست سر از روزن مجمر برآر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا نیفسرده است دل، زین خاکدان یک سو نشین</p></div>
<div class="m2"><p>تا حیاتی هست با اخگر،ز خاکستر برآر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بی تزلزل نیست بنیاد جهان آب و گل</p></div>
<div class="m2"><p>کشتی خود راازین دریای بی لنگر برآر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل دونیم از آه چون شد ذوالفقار حیدرست</p></div>
<div class="m2"><p>در جهاد نفس این شمشیر پر جوهر برآر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شکوه تاریکی دل را به اهل دل بگو</p></div>
<div class="m2"><p>از بغل آیینه رادر پیش روشنگر برآر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صلح کن بانان خشک ازنعمت الوان دهر</p></div>
<div class="m2"><p>ازجگر این خون فاسد را به این نشتر برآر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>غوطه زن درآب چشم خویش دردلهای شب</p></div>
<div class="m2"><p>پیش آن خورشید تابان سر چو نیلوفر برآر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خویش راصائب درین عبرت سراپامال کن</p></div>
<div class="m2"><p>از سرافرازی علمها در صف محشر برآر</p></div></div>