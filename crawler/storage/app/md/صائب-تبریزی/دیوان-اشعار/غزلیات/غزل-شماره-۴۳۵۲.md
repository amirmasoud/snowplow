---
title: >-
    غزل شمارهٔ ۴۳۵۲
---
# غزل شمارهٔ ۴۳۵۲

<div class="b" id="bn1"><div class="m1"><p>چشم تو که پروای نظر باز ندارد</p></div>
<div class="m2"><p>چون است که از سرمه نظر باز ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اهل دل وحرف گله آمیز محال است</p></div>
<div class="m2"><p>در قافله ما جرس آواز ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طومار شکایت چه به دستش دهی ای دل</p></div>
<div class="m2"><p>پروای سر زلف خود از ناز ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون رو به ره شوق گذاریم که از ضعف</p></div>
<div class="m2"><p>رنگ رخ ما قوت پرواز ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل آب شد از ذوق نواسنجی بلبل</p></div>
<div class="m2"><p>آتش اثر شعله آواز ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کس نتواند به تو حال دل خود گفت</p></div>
<div class="m2"><p>هر تیغ زبان جوهر این راز ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کبکی که نریزد ز لبش قهقهه شوق</p></div>
<div class="m2"><p>شایستگی چنگل شهباز ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب من وتو بلبل دستان زن شوقیم</p></div>
<div class="m2"><p>ما را ز نوا فصل خزان باز ندارد</p></div></div>