---
title: >-
    غزل شمارهٔ ۱۷۳۹
---
# غزل شمارهٔ ۱۷۳۹

<div class="b" id="bn1"><div class="m1"><p>گلی که طرح دهد رخ به نوبهار این است</p></div>
<div class="m2"><p>لبی که می شکند دیدنش خمار این است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلند بخت نهالی که از خجالت او</p></div>
<div class="m2"><p>الف کشد به زمین سرو جویبار این است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به چشم دیده وران آفتاب عالمتاب</p></div>
<div class="m2"><p>پیاده ای است زمین گیر اگر سوار این است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کند ز نشو و نما منع سبزه خط را</p></div>
<div class="m2"><p>اگر حلاوت آن لعل آبدار این است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهان به دیده خورشید تار می سازد</p></div>
<div class="m2"><p>اگر ترقی آن خط مشکبار این است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز زنگ، آینه آفتاب در خطرست</p></div>
<div class="m2"><p>اگر عیار تریهای روزگار این است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز زهد خشک اثر در جهان نخواهد ماند</p></div>
<div class="m2"><p>اگر طراوت ایام نوبهار این است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قدم ز گوشه عزلت برون منه صائب</p></div>
<div class="m2"><p>که چاره دل آشفته روزگار این است</p></div></div>