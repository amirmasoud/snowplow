---
title: >-
    غزل شمارهٔ ۲۰۳۶
---
# غزل شمارهٔ ۲۰۳۶

<div class="b" id="bn1"><div class="m1"><p>بیمار عشق را به دوا احتیاج نیست</p></div>
<div class="m2"><p>دل زنده را به آب بقا احتیاج نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دستگیر، دست بریده است بی نیاز</p></div>
<div class="m2"><p>از سر گذشته را به هما احتیاج نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اندیشه صواب و خطا فرع خواهش است</p></div>
<div class="m2"><p>تدبیر در مقام رضا احتیاج نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شستم ز اختیار، به خون دست خویش را</p></div>
<div class="m2"><p>دیگر مرا به دست دعا احتیاج نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صدق عزیمت است به منزل مرا دلیل</p></div>
<div class="m2"><p>گوش مرا به بانگ درا احتیاج نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داغ جنون به افسر شاهی برابرست</p></div>
<div class="m2"><p>دیوانه را به بال هما احتیاج نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از پوست بی نیاز بود هر که مغز یافت</p></div>
<div class="m2"><p>حق جوی را به هر دو سرا احتیاج نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بال من است پای به دامن کشیده ام</p></div>
<div class="m2"><p>سیر مرا به جنبش پا احتیاج نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز اوضاع ناگوار بس است آنچه دیده ام</p></div>
<div class="m2"><p>آیینه مرا به جلا احتیاج نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در تنگی دل است شکرخنده ها نهان</p></div>
<div class="m2"><p>این غنچه را به باد صبا احتیاج نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>افتاده است جذبه بحر کرم رسا</p></div>
<div class="m2"><p>سیلاب را به راهنما احتیاج نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پوشیده است راه حق از چشم باطلان</p></div>
<div class="m2"><p>بتخانه را به قبله نما احتیاج نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بی تربیت رسانده به معراج، خویش را</p></div>
<div class="m2"><p>سرو ترا به نشو و نما احتیاج نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خورشید از سیاهی لشکر بود غنی</p></div>
<div class="m2"><p>آن چهره را به زلف دو تا احتیاج نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سر بر نیاورم چو حباب از دل محیط</p></div>
<div class="m2"><p>صائب مرا به کسب هوا احتیاج نیست</p></div></div>