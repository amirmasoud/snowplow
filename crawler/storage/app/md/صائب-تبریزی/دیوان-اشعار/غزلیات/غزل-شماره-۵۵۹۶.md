---
title: >-
    غزل شمارهٔ ۵۵۹۶
---
# غزل شمارهٔ ۵۵۹۶

<div class="b" id="bn1"><div class="m1"><p>گهی از دل، گهی از دیده، گاه از جان ترا جویم</p></div>
<div class="m2"><p>نمی دانم ترا ای یار هر جایی کجا جویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز شوخی هر نفس در عالم دیگر کنی جولان</p></div>
<div class="m2"><p>ترا با بی پرو بالی من حیران کجا جویم؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندارم همچنان یک جا قرار از بیقراریها</p></div>
<div class="m2"><p>اگر چه در حقیقت حاضری هر جا ترا جویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر چه از رگ گردن تویی نزدیکتر با من</p></div>
<div class="m2"><p>ترا هر لحظه از جایی من سر در هوا جویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز محراب اجابت می شود مقبول طاعتها</p></div>
<div class="m2"><p>نجویم گر ترا ای قبله عالم که را جویم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بیم شور چشمان افکنم تیری به تاریکی</p></div>
<div class="m2"><p>اگر عمر ابد چون خضر از آب بقا جویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شفا چون آیه رحمت شود از آسمان نازل</p></div>
<div class="m2"><p>من مجنون علاج خویش از دارالشفا جویم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نمی سازد دو دل بسیاری آیینه عارف را</p></div>
<div class="m2"><p>تویی منظور از آیینه رویان هر که را جویم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اثر از گرم رفتاران درین عالم نمی ماند</p></div>
<div class="m2"><p>چه از پروانه در دریای آتش نقش پا جویم؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کلید خانه زاد از پره دارد قفل دلتنگی</p></div>
<div class="m2"><p>گشاد دل چرا چون غنچه از باد صبا جویم؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سیه دل خون مردم را به جای آب می نوشد</p></div>
<div class="m2"><p>دل خون گشته خود را جه از زلف دو تا جویم؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر چه نور ایمان در فرنگستان نمی باشد</p></div>
<div class="m2"><p>از آن چشم سیه دل من نگاه آشنا می جویم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هما از سایه ام کسب سعادت می کند صائب</p></div>
<div class="m2"><p>نه بی مغزم که دولت از پر و بال هما جویم</p></div></div>