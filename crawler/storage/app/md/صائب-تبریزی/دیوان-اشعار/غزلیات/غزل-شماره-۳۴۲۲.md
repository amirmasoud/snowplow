---
title: >-
    غزل شمارهٔ ۳۴۲۲
---
# غزل شمارهٔ ۳۴۲۲

<div class="b" id="bn1"><div class="m1"><p>دل تهی ناشده از خویش به جایی نرسد</p></div>
<div class="m2"><p>تا بود پر ز شکر نی به نوایی نرسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیر را شهپر پرواز بود پاکی شست</p></div>
<div class="m2"><p>آه با دامن آلوده به جایی نرسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست در سینه هرکس که ز غفلت آهی</p></div>
<div class="m2"><p>همچو کوری است که دستش به عصایی نرسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در شفاخانه ایجاد به جز بیدردی</p></div>
<div class="m2"><p>هیچ دردی نشنیدم به دوایی نرسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پنبه زاری است ترا گوش ز غفلت، ورنه</p></div>
<div class="m2"><p>نفسی نیست که از غیب ندایی نرسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قیمت گوهر نادیده که می داند چیست؟</p></div>
<div class="m2"><p>چه عجب گر سخن ما به بهایی نرسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر مرا نیست چو خار سر دیوار گلی</p></div>
<div class="m2"><p>گلم این بس که ز من زخم به پایی نرسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نشود زشتی دنیا به تو روشن چون آب</p></div>
<div class="m2"><p>تا ترا آینه دل به جلایی نرسد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کیست از اهل مروت که کند سیرابش؟</p></div>
<div class="m2"><p>بر سر خار اگر آبله پایی نرسد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خرج ره می شود این خرده جانی که مراست</p></div>
<div class="m2"><p>گر به فریاد من آواز درایی نرسد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه گل از برگ خود آن خونی احسان چیند؟</p></div>
<div class="m2"><p>که ازو دست یتیمی به حنایی نرسد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل هرکس که شود آب چو شبنم صائب</p></div>
<div class="m2"><p>نیست ممکن که به خورشید لقایی نرسد</p></div></div>