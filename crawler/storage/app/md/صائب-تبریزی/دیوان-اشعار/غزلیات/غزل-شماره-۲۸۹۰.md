---
title: >-
    غزل شمارهٔ ۲۸۹۰
---
# غزل شمارهٔ ۲۸۹۰

<div class="b" id="bn1"><div class="m1"><p>تو چون نوخط شوی طاوس جنت پر برون آرد</p></div>
<div class="m2"><p>تو چون بر هم زنی لب، بال و پر کوثر برون آرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نباشد سرمه توفیق در هر گوشه چشمی</p></div>
<div class="m2"><p>کجا زاهد سر از خط لب ساغر برون آرد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر رخسار چون گل را به بالین آشنا سازی</p></div>
<div class="m2"><p>چو بلبل غنچه تصویر بال و پر برون آرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر ساقی زمستی یک نفس از پای بنشیند</p></div>
<div class="m2"><p>زجذب شوق میخواران صراحی پر برون آرد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به نشتر کوچه بندی می کنی رگ را، نمی ترسی</p></div>
<div class="m2"><p>که هر یک قطره خونم زصد جا سر برون آرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر این یک مشت خاکستر که دل گویند، نگدازم</p></div>
<div class="m2"><p>به زور تشنگی آب از دل گوهر برون آرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمی دانند مردم آفتابی هست در عالم</p></div>
<div class="m2"><p>خدا آیینه ما را زخاکستر برون آرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شهید می چو از خاک لحد سرمست برخیزد</p></div>
<div class="m2"><p>به جای نامه برگ تاک در محشر برون آرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که را داریم ما غیر ظفرخان در جهان صائب؟</p></div>
<div class="m2"><p>نهال آرزوی ما در اینجا پر برون آرد</p></div></div>