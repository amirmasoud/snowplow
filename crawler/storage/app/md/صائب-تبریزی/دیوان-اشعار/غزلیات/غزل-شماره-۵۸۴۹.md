---
title: >-
    غزل شمارهٔ ۵۸۴۹
---
# غزل شمارهٔ ۵۸۴۹

<div class="b" id="bn1"><div class="m1"><p>خورشید داغ گوهر عالم‌فروز ماست</p></div>
<div class="m2"><p>دریا روان ز چشم خریدار کرده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داغ است چرخ از دل بی‌آرزوی ما</p></div>
<div class="m2"><p>این دشت را تهی ز خس و خار کرده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از برگریز حادثه آسوده خاطریم</p></div>
<div class="m2"><p>از گل به خار صلح چو دیوار کرده‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طبل از هجوم سنگ ملامت نمی‌خوریم</p></div>
<div class="m2"><p>چون کبک مست خنده به کهسار کرده‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را فریب دانه نمی‌آورد به دام</p></div>
<div class="m2"><p>اول نظر به آخر هر کار کرده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آلوده از نظاره جنت نمی‌کنیم</p></div>
<div class="m2"><p>چشمی که باز بر رخ دلدار کرده‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دانسته‌ایم سختی این راه دور را</p></div>
<div class="m2"><p>خود را ز هرچه هست سبکبار کرده‌ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نتوان گره بر رشته ما یافتن چو موج</p></div>
<div class="m2"><p>قطع نظر ز گوهر شهوار کرده‌ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منظور ما چو لاله نبوده است غیر داغ</p></div>
<div class="m2"><p>چشمی اگر سیاه به گلزار کرده‌ایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون شمع بود از پی پروانه نجات</p></div>
<div class="m2"><p>دستی اگر بلند شب تار کرده‌ایم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بی‌حاصلی نگر که ز کردار دل‌پذیر</p></div>
<div class="m2"><p>صائب چو خامه صلح به گفتار کرده‌ایم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صلح از فلک به دیده بیدار کرده‌ایم</p></div>
<div class="m2"><p>رو در صفا و پشت به زنگار کرده‌ایم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جان را ز قید جسم سبکبار کرده‌ایم</p></div>
<div class="m2"><p>دامن خلاص ازین ته دیوار کرده‌ایم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زیبا و زشت در نظر ما یکی شده است</p></div>
<div class="m2"><p>تا خویش را چو آینه هموار کرده‌ایم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر خود نچیده‌ایم بساطی ز شید و زرق</p></div>
<div class="m2"><p>ترک ردا و جبه و دستار کرده‌ایم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>طفلان به شوق ما همه صحرا گرفته‌اند</p></div>
<div class="m2"><p>ما راه عشق را ره بازار کرده‌ایم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هموار گشته است به ما سنگلاخ دهر</p></div>
<div class="m2"><p>تا روی خود ز خلق به دیوار کرده‌ایم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>انگشت اعتراض به حرفی نمی‌نهیم</p></div>
<div class="m2"><p>خود را خلاص ازین دهن مار کرده‌ایم</p></div></div>