---
title: >-
    غزل شمارهٔ ۵۳۲۶
---
# غزل شمارهٔ ۵۳۲۶

<div class="b" id="bn1"><div class="m1"><p>زان لب جان بخش با خط معنبر ساختم</p></div>
<div class="m2"><p>من به ظلمت ز آب حیوان چون سکندر ساختم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در محیط عشق غواصی نمی آمد ز من</p></div>
<div class="m2"><p>با کف بی مغز ازان دریای گوهر ساختم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بازشد از شش جهت بر روی من هر در که بود</p></div>
<div class="m2"><p>تا ازین درهای بی حاصل به یک در ساختم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچنان چون عود خامم در محبت گرچه من</p></div>
<div class="m2"><p>سینه را از آه آتشباز مجمر ساختم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من که دریا در نمی آمد به چشم همتم</p></div>
<div class="m2"><p>عاقبت با قطره آبی چو گوهر ساختم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می شمارند اهل درد از بیغمانم گرچه من</p></div>
<div class="m2"><p>داغ خود را خوش نمک از شورمحشر ساختم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می کشم خجلت زبینایان ز کوته دیدگی</p></div>
<div class="m2"><p>تا ترا با آفتاب و مه برابر ساختم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حاصلی جز سنگ طفلان در برومندی نبود</p></div>
<div class="m2"><p>من به برگ از گلشن ایجاد از بر ساختم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آفتاب مغفرت می خواست میدان وسیع</p></div>
<div class="m2"><p>دامن خود را به جای دیده من ترساختم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شوق من از نامه پردازی به دیدارش فزود</p></div>
<div class="m2"><p>چشم خود را حلقه پای کبوتر ساختم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر سر بی مغز درخورد کلاه فقر نیست</p></div>
<div class="m2"><p>من زناشایستگی با افسر زر ساختم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شیشه خشک است در کامم شراب لعل فام</p></div>
<div class="m2"><p>تا به خون دل دهان خویش راترساختم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چهره زرین ز چشم زخم صائب ایمن است</p></div>
<div class="m2"><p>از زروسیم جهان باروی چون زرساختم</p></div></div>