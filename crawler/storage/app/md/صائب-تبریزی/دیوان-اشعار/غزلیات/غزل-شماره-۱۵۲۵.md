---
title: >-
    غزل شمارهٔ ۱۵۲۵
---
# غزل شمارهٔ ۱۵۲۵

<div class="b" id="bn1"><div class="m1"><p>رگ جانها به دم تیغ عدم پیوسته است</p></div>
<div class="m2"><p>زود بر باد رود هر چه به دم پیوسته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>استواری طمع از عمر سبکسیر مدار</p></div>
<div class="m2"><p>کز دو سر، رشته جانها به عدم پیوسته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون قلم گر چه جدا گشته مرا بند از بند</p></div>
<div class="m2"><p>شکرلله که دم من به قدم پیوسته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نسبت آهوی رم کرده و صحرا دارد</p></div>
<div class="m2"><p>گر به ظاهر تن و جان هر دو به هم پیوسته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر مدار از قدم تیغ شهادت سر خویش</p></div>
<div class="m2"><p>کاین رگ ابر به دریای کرم پیوسته است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هست با ناوک مژگان تو زور دو کمان</p></div>
<div class="m2"><p>تا دو ابروی بلند تو به هم پیوسته است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آه شیرازه جمعیت اوراق دل است</p></div>
<div class="m2"><p>که صف آرایی لشکر به علم پیوسته است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نشود یک جهان را در و دیوار حجاب</p></div>
<div class="m2"><p>هر کجا هست برهمن به صنم پیوسته است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون کنم فکر رهایی، که مرا بر پیکر</p></div>
<div class="m2"><p>داغ چون حلقه زنجیر به هم پیوسته است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست ممکن که رود چین ز جبینش صائب</p></div>
<div class="m2"><p>هر که چون سکه به دینار و درم پیوسته است</p></div></div>