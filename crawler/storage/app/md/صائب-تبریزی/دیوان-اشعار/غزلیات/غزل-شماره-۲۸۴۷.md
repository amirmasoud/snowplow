---
title: >-
    غزل شمارهٔ ۲۸۴۷
---
# غزل شمارهٔ ۲۸۴۷

<div class="b" id="bn1"><div class="m1"><p>ز گل تنها کجا بزم گلستان ساز می‌گردد؟</p></div>
<div class="m2"><p>که این هنگامه گرم از شعله آواز می‌گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امید بازگشتن دل به زلف او عبث دارد</p></div>
<div class="m2"><p>به ناف آهوان کی نافه هرگز بازمی‌گردد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به روی بستر گل خواب راحت نیست شبنم را</p></div>
<div class="m2"><p>نقاب از روی گلرنگ که امشب باز می‌گردد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تعجب نیست گردد گرد خط داروی بیهوشی</p></div>
<div class="m2"><p>نگه در پرده چشمی که خواب ناز می‌گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشبک می‌شود چون پرده زنبوری از کاوش</p></div>
<div class="m2"><p>اگر سد سکندر پرده این راز می‌گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو کز اهل بصیرت نیستی قطع منازل کن</p></div>
<div class="m2"><p>که بینا چون شرر و اصل به یک پرواز می‌گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ندارد در کمند جذبه بحر لطف کوتاهی</p></div>
<div class="m2"><p>که هر موجی که می‌بینی به دریا بازمی‌گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ملامتگر سر از دنبال بد گوهر نمی‌دارد</p></div>
<div class="m2"><p>زبان آتشین شمع خرج گاز می‌گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به فردای قیامت می‌فتد نشو و نمای ما</p></div>
<div class="m2"><p>به این تمکین اگر قانون طالع ساز می‌گردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سخن را روی گرم از قید خاموشی برون آرد</p></div>
<div class="m2"><p>سپند از آتش سوزان بلند آواز می‌گردد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو انجم تا سحر مژگان به یکدیگر نخواهی زد</p></div>
<div class="m2"><p>اگر دانی چه درها در دل شب باز می‌گردد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>درون پیکر خشک آتشی از عشق او دارم</p></div>
<div class="m2"><p>که می‌سوزد چونی هرکس به من دمساز می‌گردد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به شمع صبح ماند شعله آواز بلبل را</p></div>
<div class="m2"><p>همانا خامه صائب نواپرداز می‌گردد</p></div></div>