---
title: >-
    غزل شمارهٔ ۶۴۱۰
---
# غزل شمارهٔ ۶۴۱۰

<div class="b" id="bn1"><div class="m1"><p>افشان خال بر رخ آن دلربا ببین</p></div>
<div class="m2"><p>در روز اگر ستاره ندیدی بیا ببین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با غیر التفات نماید به رغم من</p></div>
<div class="m2"><p>در مدعی نظر کن و در مدعا ببین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیمار را چو پرسش بیمار رسم نیست</p></div>
<div class="m2"><p>گاهی به چشم خویشتن از چشم ما ببین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا کی توان به مردم بیگانه شد طرف؟</p></div>
<div class="m2"><p>گاهی به سهو هم طرف آشنا ببین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با قامت تو سرو به دعوی برآمده است</p></div>
<div class="m2"><p>ترکیب را نظر کن و اندام را ببین!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صائب حریف آه ندامت نمی شوی</p></div>
<div class="m2"><p>در ابتدا به عاقبت کارها ببین</p></div></div>