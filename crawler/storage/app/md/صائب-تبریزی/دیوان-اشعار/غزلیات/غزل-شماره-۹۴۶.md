---
title: >-
    غزل شمارهٔ ۹۴۶
---
# غزل شمارهٔ ۹۴۶

<div class="b" id="bn1"><div class="m1"><p>کی سری بردم به جیب خود که طوفان برنخاست</p></div>
<div class="m2"><p>همچو شمع کشته دودم از گریبان برنخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمع بالینش نشد چون صبح خورشید بلند</p></div>
<div class="m2"><p>با لب پرخنده هر کس از سر جان برنخاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از نوای شور مجنون بود رقص گردباد</p></div>
<div class="m2"><p>رفت تا مجنون، غباری زین بیابان برنخاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقد جان را رونمای تیشه فولاد داد</p></div>
<div class="m2"><p>از دل فرهاد این کوه غم آسان برنخاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پاک طینت از حدیث سرد از جا کی رود؟</p></div>
<div class="m2"><p>آتش یاقوت از تحریک دامان برنخاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حیرتی دارم که چون از های هوی ناله ام</p></div>
<div class="m2"><p>از شکر خواب عدم چشم شهیدان برنخاست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عمرها در آب چشم خویشتن لنگر فکند</p></div>
<div class="m2"><p>از دل صائب غبار کلفت آسان برنخاست</p></div></div>