---
title: >-
    غزل شمارهٔ ۴۹۸۴
---
# غزل شمارهٔ ۴۹۸۴

<div class="b" id="bn1"><div class="m1"><p>دین به دنیای دنی ای دل نادان مفروش</p></div>
<div class="m2"><p>آنچه درمصر عزیزست به کنعان مفروش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همتی را که به روشن گهری مشهورست</p></div>
<div class="m2"><p>چون گدایان تنک مایه به یک نان مفروش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نبرد آب گهر تلخی منت ز مذاق</p></div>
<div class="m2"><p>چون صدف آب رخ خویش به نیسان مفروش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دامن وصل، طلبکار تهیدستان است</p></div>
<div class="m2"><p>فقر را ای دل آگاه به سامان مفروش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باد دستانه مکن عمر گرامی را صرف</p></div>
<div class="m2"><p>آنچه ارزان به تو دادند تو ارزان مفروش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رشته عمر ابد بی گره منت نیست</p></div>
<div class="m2"><p>جگر تشنه به سرچشمه حیوان مفروش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساکنان حرم از قبله نما آزادند</p></div>
<div class="m2"><p>رهنمایی به من ای خضر بیابان مفروش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گریه ساخته در انجمن عشق مکن</p></div>
<div class="m2"><p>بیش ازین دانه پوسیده به دهقان مفروش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیش من بحر ز گرداب بود حلقه بگوش</p></div>
<div class="m2"><p>دیده تر به من ای ابربهاران مفروش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عارفان زهد لباسی به جوی نستانند</p></div>
<div class="m2"><p>برو ای شیخ، به ما پاکی دامان مفروش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سطحیان غور معانی نتوانند نمود</p></div>
<div class="m2"><p>بیش ازین جلوه به آیینه حیران مفروش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سخن از پردگیان حرم توفیق است</p></div>
<div class="m2"><p>صائب او رابه زر و سیم لئیمان مفروش</p></div></div>