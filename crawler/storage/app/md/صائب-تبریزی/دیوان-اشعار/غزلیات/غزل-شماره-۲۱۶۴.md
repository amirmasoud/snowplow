---
title: >-
    غزل شمارهٔ ۲۱۶۴
---
# غزل شمارهٔ ۲۱۶۴

<div class="b" id="bn1"><div class="m1"><p>رخسار تو روز سیه ریش ندیده است</p></div>
<div class="m2"><p>زلف سیهت مفلسی دل نکشیده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر برگ گلت گرد کسادی ننشسته است</p></div>
<div class="m2"><p>دنبال خریدار، نگاهت ندویده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ابروی تو پیوسته به خوبی گذرانده است</p></div>
<div class="m2"><p>چشم تو خمار می گلگون نکشیده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تلخی ندامت نچشیده است دهانت</p></div>
<div class="m2"><p>دندان تأسف لب لعلت نگزیده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>معذوری اگر قدر گرفتاری ندانی</p></div>
<div class="m2"><p>پروانه ای از پای چراغت نپریده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حق بر طرف توست در آزردن صائب</p></div>
<div class="m2"><p>سر رشته پیمان تو هرگز نبریده است</p></div></div>