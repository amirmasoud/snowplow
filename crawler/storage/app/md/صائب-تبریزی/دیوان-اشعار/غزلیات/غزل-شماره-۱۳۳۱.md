---
title: >-
    غزل شمارهٔ ۱۳۳۱
---
# غزل شمارهٔ ۱۳۳۱

<div class="b" id="bn1"><div class="m1"><p>از پر سیمرغ اگر دست حمایت زال داشت</p></div>
<div class="m2"><p>از غم عالم مرا هم عشق فارغبال داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داشت تا اندیشه او بر سر زانو سرم</p></div>
<div class="m2"><p>ساق عرش از قامت خم گشته ام خلخال داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زنگ ظلمت بود از آب زندگانی قسمتش</p></div>
<div class="m2"><p>تا سکندر روی در آیینه اقبال داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داشت آتش زیر پا امشب خیالش در نظر</p></div>
<div class="m2"><p>این غزال شوخ تا چشم که در دنبال داشت؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا چو شبنم چشم وا کردم درین بستانسرا</p></div>
<div class="m2"><p>سبزه بخت مرا خواب گران پامال داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عالمی بر عیش خوش پرگار من می برد رشک</p></div>
<div class="m2"><p>تا دل دیوانه جا در حلقه اطفال داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شبنم از مهر خموشی محرم گلزار شد</p></div>
<div class="m2"><p>بلبل ما را ز گل محروم قیل و قال داشت</p></div></div>