---
title: >-
    غزل شمارهٔ ۱۰۹۳
---
# غزل شمارهٔ ۱۰۹۳

<div class="b" id="bn1"><div class="m1"><p>نقطه خالش که نه پرگار سرگردان اوست</p></div>
<div class="m2"><p>کیست کز فرمان او گردن کشد، دوران اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفتابی را که شد چشم تر من پرده دار</p></div>
<div class="m2"><p>صبح محشر سینه چاک خنجر مژگان اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برق جولانی که دارد در خم چوگان مرا</p></div>
<div class="m2"><p>آسمان بی سر و پا، گویی از میدان اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست در مغز زمین موج طراوت از محیط</p></div>
<div class="m2"><p>این سفال خشک، سیراب از خط ریحان اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آسمان چشمی که من بیمار او گردیده ام</p></div>
<div class="m2"><p>چهره خورشید، زرد از درد بی درمان اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هاله غبغب که پهلو می زند با ماه عید</p></div>
<div class="m2"><p>موج دور افتاده ای از چشمه حیوان اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست کار آسمان دل را مصفا ساختن</p></div>
<div class="m2"><p>از دل هر کس غباری خیزد، از جولان اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از خرام او به عمر جاودان قانع مشو</p></div>
<div class="m2"><p>کاین چنین صد مصرع برجسته در دیوان اوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قلزم عشقی که من خاشاک او گردیده ام</p></div>
<div class="m2"><p>چهره گردون کبود از سیلی طوفان است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آتشین رویی که نعل من ازو در آتش است</p></div>
<div class="m2"><p>آسمان چون دیده قربانیان حیران اوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیست آسان در حریم وصل او ره یافتن</p></div>
<div class="m2"><p>چرخ نیلی، یک گره از جبهه دربان اوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عشق سلطانی است بی پروا که چندین ماه مصر</p></div>
<div class="m2"><p>از فرامش گشتگان گوشه زندان اوست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر چه دارد نعمت الوان فراوان خوان عشق</p></div>
<div class="m2"><p>می خورد هر کس جگر بی گفتگو مهمان اوست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیست صائب شکوه ای از گردش دوران مرا</p></div>
<div class="m2"><p>درد روز افزون من از حسن بی پایان اوست</p></div></div>