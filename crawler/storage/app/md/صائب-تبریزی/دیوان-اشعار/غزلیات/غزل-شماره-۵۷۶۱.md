---
title: >-
    غزل شمارهٔ ۵۷۶۱
---
# غزل شمارهٔ ۵۷۶۱

<div class="b" id="bn1"><div class="m1"><p>درین جهان نشود حال آن جهان معلوم</p></div>
<div class="m2"><p>که مغز را نتوان کرد از استخوان معلوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که دیده حاشیه باشد ز متن مشکلتر؟</p></div>
<div class="m2"><p>نشد ز سبزه خط راز آن دهان معلوم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عیار ناز ترا اهل عشق می دانند</p></div>
<div class="m2"><p>که بی کشش نشود زور هر کمان معلوم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر چه معنی نازک شود برهنه زلفظ</p></div>
<div class="m2"><p>مرا نشد ز کمر هیچ ازان میان معلوم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز شوق من چه تواند زبان خامه نوشت؟</p></div>
<div class="m2"><p>ضمیر لال نگردد به ترجمان معلوم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>توان ز سختی ایام صبر هر کس یافت</p></div>
<div class="m2"><p>عیار زر شود از سنگ امتحان معلوم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جنون من به خط سبز گلرخان بسته است</p></div>
<div class="m2"><p>که در بهار شود شور بلبلان معلوم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز حسن عاقبت آغاز را توان دریافت</p></div>
<div class="m2"><p>که هست تیر کج و راست در نشان معلوم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز اشک راز دل بیقرار من شد فاش</p></div>
<div class="m2"><p>که از ستاره شود سیر آسمان معلوم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بلندی سخن دلپذیر ما صائب</p></div>
<div class="m2"><p>ز گرد سرمه نگردد در اصفهان معلوم</p></div></div>