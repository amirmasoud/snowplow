---
title: >-
    غزل شمارهٔ ۳۴۹۴
---
# غزل شمارهٔ ۳۴۹۴

<div class="b" id="bn1"><div class="m1"><p>کیستند اهل جهان، بی سر و سامانی چند</p></div>
<div class="m2"><p>در ره سیل حوادث، ده ویرانی چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرخ کز خون شفق چهره خود دارد سرخ</p></div>
<div class="m2"><p>چه سرانجام دهد کار پریشانی چند؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین گلستان که چو گل خیمه در آنجا زده ای</p></div>
<div class="m2"><p>چیست در دست تو جز چاک گریبانی چند؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو سه روزی است تماشای گلستان جهان</p></div>
<div class="m2"><p>در دل خود برسانید گلستانی چند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست از مردم بی شرم عجب پرده دری</p></div>
<div class="m2"><p>پوشش امید چه دارید ز عریانی چند؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل سیه شد ز پریشان سخنان، صبح کجاست؟</p></div>
<div class="m2"><p>تا بگیرد سر این شمع پریشانی چند؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داغ دیگر به دل از لاله ستانم افزود</p></div>
<div class="m2"><p>چه تراوش کند از سینه سوزانی چند؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن که بر آتش ما آب نصیحت می ریخت</p></div>
<div class="m2"><p>کاش می زد به دل سوخته، دامانی چند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه کنم آه که هر لحظه برون می آرد</p></div>
<div class="m2"><p>عرق شرم تو از پرده نگهبانی چند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شد ز یک صبح قیامت همه عالم پرشور</p></div>
<div class="m2"><p>چه کند دل به شکر خنده پنهانی چند؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وقت آن راهروی خوش که چو دریای سراب</p></div>
<div class="m2"><p>دارد از موجه خود سلسله جنبانی چند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رهروان تو چه پروای علایق دارند؟</p></div>
<div class="m2"><p>چه کند خار به این برزده دامانی چند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نبرد آینه از آینه هرگز زنگار</p></div>
<div class="m2"><p>چه دهی حیرت خود عرض به حیرانی چند؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صائب از قحط سخندان همه کس موزون است</p></div>
<div class="m2"><p>کاش می بود درین عهد سخندانی چند</p></div></div>