---
title: >-
    غزل شمارهٔ ۱۶۷۶
---
# غزل شمارهٔ ۱۶۷۶

<div class="b" id="bn1"><div class="m1"><p>ازان به خاطر من ترک کار دشوارست</p></div>
<div class="m2"><p>که بار دوش توکل شدن به دل بارست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اثر گذار اگر عمر جاودان خواهی</p></div>
<div class="m2"><p>که زندگانی هر کس به قدر آثارست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازان به تلخی هجر از وصال ساخته ام</p></div>
<div class="m2"><p>که رعشه دارم و این جام سخت سرشارست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امید هست که شیرازه گهر گردد</p></div>
<div class="m2"><p>ز تار و پود جهان رشته ای که هموارست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد از شکست خریدار، توتیا گهرم</p></div>
<div class="m2"><p>همان ز ساده دلی تشنه خریدارست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازان همیشه بود وقت می پرستان خوش</p></div>
<div class="m2"><p>که هر کجا که غمی هست رزق هشیارست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو بی دریغ به ویرانه گنج می بخشی</p></div>
<div class="m2"><p>وگرنه درد ترا دل کجا سزاوارست؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نفس شمرده زنان راست دل بجا صائب</p></div>
<div class="m2"><p>چمن صحیح بود تا نسیم بیمارست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جواب آن غزل آصفی است این صائب</p></div>
<div class="m2"><p>زمانه ای است که هر کس به خود گرفتارست</p></div></div>