---
title: >-
    غزل شمارهٔ ۲۹۰۶
---
# غزل شمارهٔ ۲۹۰۶

<div class="b" id="bn1"><div class="m1"><p>از ان سرو از درختان سرفرازی بیشتر دارد</p></div>
<div class="m2"><p>که با دست تهی صد بینوا را زیر پا دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به کیش مردم بیدار دل کفرست نومیدی</p></div>
<div class="m2"><p>چراغ اینجا امید بازگشتن از شرر دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از ان جوش نشاط از سینه خم کم نمی گردد</p></div>
<div class="m2"><p>که از معموره آفاق خشتی زیر سر دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دامانش نیاویزم، به دامان که آویزم؟</p></div>
<div class="m2"><p>همین صبح است در عالم که آهی در جگر دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر از سینه مور ضعیفی پرده برداری</p></div>
<div class="m2"><p>هزاران کوه غم بر دل از ان موی کمر دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صدف از تنگدستی شکوه ها دارد گره در دل</p></div>
<div class="m2"><p>نمی داند که دریا چشم بر آب گهر دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گهی بر دل شبیخون می زند گاهی بر ایمانم</p></div>
<div class="m2"><p>همیشه کاکل او فتنه ای در زیر سر دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکست از سرکشیهای نهال او پر و بالم</p></div>
<div class="m2"><p>خوشا قمری که یار خویش را در زیر پر دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سواد طره موج از بیاض گردن مینا</p></div>
<div class="m2"><p>خوشاینده است اما زلف او جای دگر دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تلاش عشق داری، عقل رسمی را زسروا کن</p></div>
<div class="m2"><p>نمی سنجد گوهر در ترازویی که سر دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از ان پیچیده ام بر رشته جان چون گره صائب</p></div>
<div class="m2"><p>که اندک نسبت دوری به آن موی کمر دارد</p></div></div>