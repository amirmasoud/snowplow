---
title: >-
    غزل شمارهٔ ۶۷۹۲
---
# غزل شمارهٔ ۶۷۹۲

<div class="b" id="bn1"><div class="m1"><p>به توحید خدا همچون الف گویاست تنهایی</p></div>
<div class="m2"><p>دویی در پله شرک است و بی همتاست تنهایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تجرد پیشگان را نیست کثرت مانع از وحدت</p></div>
<div class="m2"><p>که در دریای لشکر چون علم تنهاست تنهایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به اندک سختیی رو از تو گردانند همراهان</p></div>
<div class="m2"><p>روی گر در دهان اژدها همپاست تنهایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حدیث قاف و عنقا را مدان افسانه چون طفلان</p></div>
<div class="m2"><p>که کوه قاف کنج عزلت و عنقاست تنهایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل رم کرده هر کس را بود در سینه، می داند</p></div>
<div class="m2"><p>که صحبت دامگاه و دامن صحراست تنهایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تجرد شهپر پرواز گردون شد مسیحا را</p></div>
<div class="m2"><p>زمین گیرست جمعیت، فلک پیماست تنهایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو مرغ خانگی بر گرد آب و گل نمی گردد</p></div>
<div class="m2"><p>همای خوش نشین اوج استغناست تنهایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو بوی گل که در آغوش گل با گل نیامیزد</p></div>
<div class="m2"><p>اگر چه هست در دنیا، نه در دنیاست تنهایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز خود دورافکند چون نافه صائب سایه خود را</p></div>
<div class="m2"><p>غزال وحشی دامان این صحراست تنهایی</p></div></div>