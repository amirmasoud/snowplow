---
title: >-
    غزل شمارهٔ ۵۰۵۰
---
# غزل شمارهٔ ۵۰۵۰

<div class="b" id="bn1"><div class="m1"><p>حسن تو باده ای است که شرم است شیشه اش</p></div>
<div class="m2"><p>خال تو دانه ای است که دام است ریشه اش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی تو آتشی است که زلف است دود او</p></div>
<div class="m2"><p>شیری است غمزه تو که دلهاست بیشه اش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سروی است قامت تو که ازجای می کند</p></div>
<div class="m2"><p>در هر دلی که پنجه فرو برد ریشه اش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست از هنر چگونه نشوید کسی به خون ؟</p></div>
<div class="m2"><p>فرهاد را ز پای درآورد تیشه اش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون اختیار پیشه و شغل دگر کند؟</p></div>
<div class="m2"><p>صائب که شد ز روز ازل عشق پیشه اش</p></div></div>