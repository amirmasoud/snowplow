---
title: >-
    غزل شمارهٔ ۳۰۳۰
---
# غزل شمارهٔ ۳۰۳۰

<div class="b" id="bn1"><div class="m1"><p>ز ماتمخانه ما نغمه عشرت کجا خیزد؟</p></div>
<div class="m2"><p>سپند از آتش ما تنگدستان بینوا خیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نصیحت برنینگیزد زمین گیران غفلت را</p></div>
<div class="m2"><p>ره خوابیده هیهات است از بانگ درا خیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عبوس زاهد خشک از می گلگون نگردد کم</p></div>
<div class="m2"><p>مگر در سوختن چین از جبین بوریا خیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پشیمانی ندارد در طلب از پای افتادن</p></div>
<div class="m2"><p>درین وادی کسی کز پا درآید بی عصا خیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به خاموشی مباش از انتقام عاجزان ایمن</p></div>
<div class="m2"><p>که سیل از کوهسار خاکساران بی صدا خیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به وصل از دامن عاشق ندارد دست دلگیری</p></div>
<div class="m2"><p>که ممکن نیست زنگ آهن از آهن ربا خیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درون پرده دل با خیالش خلوتی دارم</p></div>
<div class="m2"><p>که صحبت می خورد بر هم سپندی گر زجا خیزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دو عالم را به یک پیمانه می بخشند مخموران</p></div>
<div class="m2"><p>اگر قارون نشیند با می آشامان گدا خیزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مگو تأثیر در افغان سنگین دل نمی باشد</p></div>
<div class="m2"><p>که دل را آب سازد ناله ای کز آسیا خیزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سعادت نیست چون ذاتی، شقاوت می شود آخر</p></div>
<div class="m2"><p>نخواهم دولتی کز سایه بال هما خیزد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر قسمت نگیرد دست ما گم کرده راهان را</p></div>
<div class="m2"><p>چه از پای طلب آید، چه از دست دعا خیزد؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز تن پرور کند پهلو تهی آثار درویشی</p></div>
<div class="m2"><p>که از پهلوی فربه زود نقش بوریا خیزد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زعشق پاکدامن مدعا این است عاشق را</p></div>
<div class="m2"><p>که از بزم تو یک ره با دل بی مدعا خیزد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جدایی مشکل است از دشمن جانسوز اگر باشد</p></div>
<div class="m2"><p>کز آتش دور چون گردد سپند، از وی صدا خیزد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ازان صائب نظر از خاک پایش برنمی دارم</p></div>
<div class="m2"><p>که سازد چشم روشن گریه ای کز توتیا خیزد</p></div></div>