---
title: >-
    غزل شمارهٔ ۵۰۴۹
---
# غزل شمارهٔ ۵۰۴۹

<div class="b" id="bn1"><div class="m1"><p>هردل که داغدار شود از نظاره اش</p></div>
<div class="m2"><p>پهلو به آفتاب زند هر ستاره اش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باشد ستاره درشب تاریک رهنما</p></div>
<div class="m2"><p>شد زیر زلف رهزن من گوشواره اش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ابروی او اگر چه هلال گرفته ای است</p></div>
<div class="m2"><p>تیغ برهنه ای است زبان اشاره اش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شرمنده است پیش قدش درنشست و خاست</p></div>
<div class="m2"><p>باغ از گل پیاده و سرو سواره اش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازگریه چشم هرکه چو بادام شد سفید</p></div>
<div class="m2"><p>نظاره بنفشه خطان است چاره اش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن را که دربساط چو گل هست خرده ای</p></div>
<div class="m2"><p>در دست صد کس است گریبان پاره اش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زاهد نظر به مردم بالغ نظر، بود</p></div>
<div class="m2"><p>طفلی که زهد خشک بود گاهواره اش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر شیشه دل است گواراتر از شراب</p></div>
<div class="m2"><p>زخمی که می زند دل چون سنگ خاره اش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب فتاد هر که درین بحر بی کنار</p></div>
<div class="m2"><p>چون گوهرست گرد یتیمی کناره اش</p></div></div>