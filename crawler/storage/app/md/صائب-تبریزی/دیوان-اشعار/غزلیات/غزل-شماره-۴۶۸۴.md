---
title: >-
    غزل شمارهٔ ۴۶۸۴
---
# غزل شمارهٔ ۴۶۸۴

<div class="b" id="bn1"><div class="m1"><p>چو شمع، جان ز نسیم سحر دریغ مدار</p></div>
<div class="m2"><p>ز دوستان سبکروح سر دریغ مدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بوی سوختگی روح تازه می گردد</p></div>
<div class="m2"><p>ز شمع خرده جان چون شرر دریغ مدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درین حدیقه اگر دوستدار چشم خودی</p></div>
<div class="m2"><p>نظر ز مردم روشن گهر دریغ مدار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی است کام نهنگ و صدف درین دریا</p></div>
<div class="m2"><p>ز هر که لب بگشاید گهر دریغ مدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به کار دشمن خونخوار خود گره مپسند</p></div>
<div class="m2"><p>ز هیچ آبله ای نیشتر دریغ مدار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به یک نظر سر شبنم به آفتاب رسید</p></div>
<div class="m2"><p>توجه از من بی پا وسر دریغ مدار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جبین روشن خورشید لوح تعلیمی است</p></div>
<div class="m2"><p>که روی زرد خود از هیچ در دریغ مدار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو آفتاب اگر میل تاج زر داری</p></div>
<div class="m2"><p>ز هیچ ذره فروغ نظر دریغ مدار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکوفه گل ز وصال ثمر به ریزش چید</p></div>
<div class="m2"><p>ز خاک راهگذر سیم و زر دریغ مدار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سری ز رخنه دیوار باغ بیرون کن</p></div>
<div class="m2"><p>ز هیچ راهنوردی ثمر دریغ مدار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درین دو هفته که میراب این چمن شده ای</p></div>
<div class="m2"><p>نظر ز صائب آتش جگر دریغ مدار</p></div></div>