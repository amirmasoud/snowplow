---
title: >-
    غزل شمارهٔ ۱۷۶۸
---
# غزل شمارهٔ ۱۷۶۸

<div class="b" id="bn1"><div class="m1"><p>شدم غبار و همان خارخار من باقی است</p></div>
<div class="m2"><p>توجه چمن آرا به این چمن باقی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار جامه بدل کرد روزگار و هنوز</p></div>
<div class="m2"><p>حدیث دیده یعقوب و پیرهن باقی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به رنگ و بوی جهان دل منه، تماشا کن</p></div>
<div class="m2"><p>که آه سرد و کف خاکی از چمن باقی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گذشت فصل بهار و چمن ورق گرداند</p></div>
<div class="m2"><p>همان به تازگی خویش داغ من باقی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلیل این که سخن آب زندگی خورده است</p></div>
<div class="m2"><p>همین بس است که از رفتگان سخن باقی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه شیشه ها که تهی شد، چه جامها که شکست</p></div>
<div class="m2"><p>به حال خود دل سنگین انجمن باقی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز پادشاهی پرویز جز فسانه نماند</p></div>
<div class="m2"><p>هزار نقش نمایان ز کوهکن باقی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جواب آن غزل است این که گفت عرفی ما</p></div>
<div class="m2"><p>هزار شمع بکشتند و انجمن باقی است</p></div></div>