---
title: >-
    غزل شمارهٔ ۳۱۱۰
---
# غزل شمارهٔ ۳۱۱۰

<div class="b" id="bn1"><div class="m1"><p>طلبکار خدا را درد دل بسیار می باشد</p></div>
<div class="m2"><p>گره در سبحه بیش از رشته زنار می باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خطر بسیار دارد حرف حق با باطلان گفتن</p></div>
<div class="m2"><p>سر منصور را بالین زچوب دار می باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بپوش از خواب شیرین چشم اگر جویای دیداری</p></div>
<div class="m2"><p>که فتح الباب دولت، دیده بیدار می باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زدل هر کس نظر برداشت بی حاصل بود سیرش</p></div>
<div class="m2"><p>زمرکز هر که غافل گشت بی پرگار می باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل روشن به جسم تیره هیهات است پردازد</p></div>
<div class="m2"><p>که پشت آیینه را پیوسته بر دیوار می باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی صد شد شتاب عمر از سنگینی خوابم</p></div>
<div class="m2"><p>که سیلاب از گرانسنگی سبکرفتار می باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به مقدار پرستاران بود رنجوری هر کس</p></div>
<div class="m2"><p>خوشا احوال بیماری که بی غمخوار می باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به جای زرمکن خرج آبروی خویش را صائب</p></div>
<div class="m2"><p>مخواه از آشنایان هر چه در بازار می باشد</p></div></div>