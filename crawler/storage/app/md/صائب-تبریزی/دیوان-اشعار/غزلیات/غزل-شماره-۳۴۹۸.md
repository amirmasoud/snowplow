---
title: >-
    غزل شمارهٔ ۳۴۹۸
---
# غزل شمارهٔ ۳۴۹۸

<div class="b" id="bn1"><div class="m1"><p>با لب تشنه جگر سر به سرابم دادند</p></div>
<div class="m2"><p>آتشم را ننشاندند و به آبم دادند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمک شوری بختم به جگر افشاندند</p></div>
<div class="m2"><p>تکیه بر بسر آتش چو کبابم دادند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خنده بیغمی و گریه شادی بردند</p></div>
<div class="m2"><p>جگر تشنه و مژگان پرآبم دادند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حاش لله که بیابد گهرم آب قبول</p></div>
<div class="m2"><p>منم آن قطره که واپس به سحابم دادند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیستم خال، بر آتش چه نشاندند مرا؟</p></div>
<div class="m2"><p>نیستم زلف، چرا اینهمه تابم دادند؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صلح در ذایقه ام باده لب شیرین است</p></div>
<div class="m2"><p>بس که عادت به می تلخ عتابم دادند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من جدا می روم و خرقه پشمینه جدا</p></div>
<div class="m2"><p>تا ز خمخانه تجرید شرابم دادند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون نلرزم به سر هر نفسی همچو حباب؟</p></div>
<div class="m2"><p>خانه ای تنگتر از چشم حبابم دادند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فکر من همچو ظفرخان همه باشد به صواب</p></div>
<div class="m2"><p>صائب از مبداء فیاض خطابم دادند</p></div></div>