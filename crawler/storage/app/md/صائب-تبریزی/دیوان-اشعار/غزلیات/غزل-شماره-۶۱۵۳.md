---
title: >-
    غزل شمارهٔ ۶۱۵۳
---
# غزل شمارهٔ ۶۱۵۳

<div class="b" id="bn1"><div class="m1"><p>از تن خاکی دل صد پاره می آید برون</p></div>
<div class="m2"><p>این شرر آخر ز سنگ خاره می آید برون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست از بخت سیه دلهای روشن را غبار</p></div>
<div class="m2"><p>روشن از خاکستر آتشپاره می آید برون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل مخور ز اندیشه روزی که گندم از زمین</p></div>
<div class="m2"><p>سینه چاک از شوق روزی خواره می آید برون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غنچه چون گل شد، ز حفظ بوی خود عاجز شود</p></div>
<div class="m2"><p>آه بی تاب از دل صد پاره می آید برون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان دل سنگین اگر جویم ترحم دور نیست</p></div>
<div class="m2"><p>مومیایی هم ز سنگ خاره می آید برون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می شود در بی کسی این چشمه رحمت روان</p></div>
<div class="m2"><p>شیرکی ز انگشت در گهواره می آید برون؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون حبابی می تواند بحر را در بر کشید؟</p></div>
<div class="m2"><p>از تماشای تو کی نظاره می آید برون؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می دهم تصدیع صائب چاره جویان را عبث</p></div>
<div class="m2"><p>از علاج درد من کی چاره می آید برون؟</p></div></div>