---
title: >-
    غزل شمارهٔ ۲۸۲۴
---
# غزل شمارهٔ ۲۸۲۴

<div class="b" id="bn1"><div class="m1"><p>زفیض عشق دلهای مخالف مهربان گردد</p></div>
<div class="m2"><p>زآتش رشته های شمع با هم یکزبان گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زکوه غم مترسان سینه دریادل ما را</p></div>
<div class="m2"><p>که این بار گران برکشتی ما بادبان گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تماشای رخش بی پرده از چشم که می آید؟</p></div>
<div class="m2"><p>مباد آن روز کاین آیینه بی آیینه دان گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی صد شد زپند ناصحان سرگرمی عشقم</p></div>
<div class="m2"><p>که بر دیوانه سنگ کودکان رطل گران گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا صبح امید آن روز از مشرق شود طالع</p></div>
<div class="m2"><p>که آن ابر و کمان را استخوان من نشان گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مکن از تیغ خود نومید ما امیدواران را</p></div>
<div class="m2"><p>مروت نیست ماه عید از طفلان نهان گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زخار راه افزون می شود سامان پروازش</p></div>
<div class="m2"><p>چو برق آن کس که در راه طلب آتش عنان گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گل از سیر چمن آن غنچه بیدار دل چیند</p></div>
<div class="m2"><p>که عریان از لباس رنگ و بو پیش از خزان گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به سیل نوبهار از جان نمی خیزد غبار من</p></div>
<div class="m2"><p>خوش آن رهرو که تا گویند راهی شو، روان گردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جوان را صحبت پیران حصار عافیت باشد</p></div>
<div class="m2"><p>به خاک و خون نشیند تیر چون دور از کمان گردد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قناعت کن که رزق آفتاب از سفره گردون</p></div>
<div class="m2"><p>همان قرصی است گر صد قرن بر گرد جهان گردد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر همراه مایی، خیر باد هر دو عالم کن</p></div>
<div class="m2"><p>که بوی پیرهن بار دل این کاروان گردد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ندارد مسند عزت زیان خاکی نهادان را</p></div>
<div class="m2"><p>که صدر از کیمیای خاکساری آستان گردد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بجز زخم زبان رزق از سخن نبود سخنور را</p></div>
<div class="m2"><p>که از گلزار خار و خس نصیب باغبان گردد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چنین کان سنگدل را حال من باور نمی آید</p></div>
<div class="m2"><p>عجب دارم به مردن درد من خاطر نشان گردد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زخط گفتم زمان حسن او آخر شود صائب</p></div>
<div class="m2"><p>ندانستم که خطش فتنه آخر زمان گردد</p></div></div>