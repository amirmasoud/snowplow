---
title: >-
    غزل شمارهٔ ۱۸۱۸
---
# غزل شمارهٔ ۱۸۱۸

<div class="b" id="bn1"><div class="m1"><p>طریقِ مردمِ سنجیده خودستایی نیست</p></div>
<div class="m2"><p>که کارِ آتشِ یاقوت ژاژخایی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به اهلِ دل چه کند حرفِ بادپیمایان؟</p></div>
<div class="m2"><p>نشانه را خطر از ناوکِ هوایی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز خنده‌روییِ گردون فریبِ رحم مخور</p></div>
<div class="m2"><p>که رخنه‌های قفس رخنهٔ رهایی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگرچه دامنِ گل خوابگاهِ شبنم شد</p></div>
<div class="m2"><p>خوشم که دولتِ تردامنان بقایی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکنجهٔ نظرِ شورِ خلق دلسوز است</p></div>
<div class="m2"><p>به مدعا نرسیدن ز نارسایی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر ترددِ خاطر سخن قبول کند</p></div>
<div class="m2"><p>کلیدِ رزق به‌غیر از شکسته‌پایی نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همیشه سروِ تهیدست ازان بود سرسبز</p></div>
<div class="m2"><p>که هیچ چشم به‌دنبالِ بینوایی نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کناره گیر ز مردم که بی‌دماغان را</p></div>
<div class="m2"><p>شکنجه‌ای بتر از پاسِ آشنایی نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به هر که هر چه دهی نامِ آن مبر صائب</p></div>
<div class="m2"><p>که حقِ خود طلبیدن کم از گدایی نیست</p></div></div>