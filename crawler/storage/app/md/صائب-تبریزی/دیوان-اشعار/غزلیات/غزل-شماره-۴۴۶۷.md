---
title: >-
    غزل شمارهٔ ۴۴۶۷
---
# غزل شمارهٔ ۴۴۶۷

<div class="b" id="bn1"><div class="m1"><p>از حلقه های آن زلف دل صاحب نظر شد</p></div>
<div class="m2"><p>این مرغ چشم بسته از دام دیده ور شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسنی که کامل افتاد ایجاد می کند عشق</p></div>
<div class="m2"><p>هر قطره اشک این شمع پروانه دگر شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حاشا که از کدورت نقصان کند دل پاک</p></div>
<div class="m2"><p>دریا ز تلخرویی گنجینه گهر شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست از فغان مدارید گر ذوق وصل دارید</p></div>
<div class="m2"><p>کز ناله گلوسوز این نی پراز شکر شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غیر از خودی ندارد این راه دورسنگی</p></div>
<div class="m2"><p>هرکس ز خودبرآمد با خضر همسفر شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آسوده بود بلبل تا گل نبود در باغ</p></div>
<div class="m2"><p>بیتابی دل ما از وصل بیشتر شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون شوق کامل افتاد حاجت به رهنمانیست</p></div>
<div class="m2"><p>سیلاب را به دریا آخر که راهبرشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در قید تن نماند جانی که پاک گردد</p></div>
<div class="m2"><p>کی در ختا گذارند خونی که مشک تر شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در دامن صدف کی در یتیم ماند</p></div>
<div class="m2"><p>شد گوشوار گردون عیسی چو بی پدر شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل در فلک حصاری از راه عقل وهوش است</p></div>
<div class="m2"><p>در لامکان کند سیرجانی که بیخبر شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا دل به یار پیوست دیگر نکردیادم</p></div>
<div class="m2"><p>با سرچه کاردارد دستی که در کمر شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا شوق در ترقی است امید وصل باقی است</p></div>
<div class="m2"><p>چون مورپربرآوردمحروم از شکر شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دل چشم بوسه زان لب در روزگار خط داشت</p></div>
<div class="m2"><p>یکبارگی دهانش پوشیده از نظر شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هرچند خوابها را سنگین کند بهاران</p></div>
<div class="m2"><p>در دور خط مشکین آن چشم شوختر شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفتم خزان برآرد این خار خارم از دل</p></div>
<div class="m2"><p>رنگ شکسته گل را آرایش دگر شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شور کلام صائب در عهد پیری افزود</p></div>
<div class="m2"><p>چندان که ماند این می درشیشه تلخترشد</p></div></div>