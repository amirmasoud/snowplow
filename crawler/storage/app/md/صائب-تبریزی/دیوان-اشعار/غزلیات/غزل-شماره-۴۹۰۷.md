---
title: >-
    غزل شمارهٔ ۴۹۰۷
---
# غزل شمارهٔ ۴۹۰۷

<div class="b" id="bn1"><div class="m1"><p>با تن خاکی، نظر زان عالم روشن بپوش</p></div>
<div class="m2"><p>پای در زنجیر داری، چشم از روزن بپوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پوست چون کرباس بر تن می‌درد زور جنون</p></div>
<div class="m2"><p>ناصح بی درد می گوید که پیراهن بپوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبنم گستاخ رابنگر چه برد از بوستان</p></div>
<div class="m2"><p>ازوفای لاله رویان دیده روشن بپوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در غبار دل نهانم چون چراغ آسیا</p></div>
<div class="m2"><p>گر غبارآلود باشد حرف من، بر من بپوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باسبکروحان بهار زندگی را بگذران</p></div>
<div class="m2"><p>چشم باگل واکن وبا شبنم گلشن بپوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جوشن داودی اینجا شاهراه ناوک است</p></div>
<div class="m2"><p>از دل محکم زره در زیر پیراهن بپوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلوت عشق است و صد غماز صائب درکمین</p></div>
<div class="m2"><p>رخنه در را ببند و دیده روزن بپوش</p></div></div>