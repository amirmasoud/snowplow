---
title: >-
    غزل شمارهٔ ۱۴۸۱
---
# غزل شمارهٔ ۱۴۸۱

<div class="b" id="bn1"><div class="m1"><p>آنچنان بلبل من واله و حیران گل است</p></div>
<div class="m2"><p>که شکاف قفسم چاک گریبان گل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر طرف می نگری نعمت الوان گل است</p></div>
<div class="m2"><p>قاف تا قاف جهان سفره احسان گل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می شود مایده حسن گلوسوز از عشق</p></div>
<div class="m2"><p>شور مرغان گلستان نمک خوان گل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آب گردد به نظر خنده چو سرشار افتد</p></div>
<div class="m2"><p>اشک شبنم اثر چهره خندان گل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه خیال است که دیوانه نگردد بلبل</p></div>
<div class="m2"><p>تا نسیم سحری سلسله جنبان گل است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسن را تربیت عشق کند صاحب درد</p></div>
<div class="m2"><p>شور بلبل نمک زخم نمایان گل است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نتوان کرد نظر بند پریرویان را</p></div>
<div class="m2"><p>ورنه شبنم به دو صد چشم نگهبان گل است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون به خورشید درخشان سر شبنم نرسد؟</p></div>
<div class="m2"><p>تربیت یافته گوشه دامان گل است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر از بال پری بود سلیمان را چتر</p></div>
<div class="m2"><p>شهپر بلبل ما چتر سلیمان گل است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خار گل می شود از پرتو روشن گهران</p></div>
<div class="m2"><p>مژه در دیده خونبار خیابان گل است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نفس از سینه مجروح شود صاحب فیض</p></div>
<div class="m2"><p>خرج باد سحر از کیسه احسان گل است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ناتوانان سبب نظم جهان می باشند</p></div>
<div class="m2"><p>خار شیرازه اوراق پریشان گل است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دل صد اره به از آه ندارد اثری</p></div>
<div class="m2"><p>بوی خوش مصرع برجسته دیوان گل است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حاصل ما ز نکویان جگر پر داغی است</p></div>
<div class="m2"><p>سوز دل قسمت بلبل ز چراغان گل است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا فتاده است به آن گوشه دستار رهش</p></div>
<div class="m2"><p>گر همه باغ بهشت است که زندان گل است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مشت خاکستری از نغمه سرایان مانده است</p></div>
<div class="m2"><p>زان چراغی که نهان در ته دامن گل است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دو سه روزی که بود خون بهاران در جوش</p></div>
<div class="m2"><p>کشتی می مده از دست که طوفان گل است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صحبت جسم و روان زود ز هم می پاشد</p></div>
<div class="m2"><p>یک نفس شبنم غربت زده مهمان گل است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زخمی خار گمان است دل بیجگران</p></div>
<div class="m2"><p>ورنه از گریه من راه خیابان گل است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رنگ و بو پرده بینایی بلبل شده است</p></div>
<div class="m2"><p>ورنه هر خار درین باغ رگ جان گل است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مغتنم دان اگر از عشق ترا داغی هست</p></div>
<div class="m2"><p>که سبکسیرتر از اختر تابان گل است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نقد شادی که چو اکسیر نهان بود، امروز</p></div>
<div class="m2"><p>جمع یکجا همه در پله میزان گل است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>برگریزان فنا را پس سر خواهد دید</p></div>
<div class="m2"><p>تازه هر دل که ز روی عرق افشان گل است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نقطه خاک که دلتنگی ازو می بارید</p></div>
<div class="m2"><p>یک دهن خنده ز رخساره خندان گل است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>می چکد از نفسم خون شکایت صائب</p></div>
<div class="m2"><p>مغز من گر چه پریخانه ز احسان گل است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چشم صائب ز تماشای تو گل می چیند</p></div>
<div class="m2"><p>دیده شبنم اگر واله و حیران گل است</p></div></div>