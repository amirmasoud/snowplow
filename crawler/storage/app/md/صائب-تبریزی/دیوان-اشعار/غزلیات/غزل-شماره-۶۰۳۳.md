---
title: >-
    غزل شمارهٔ ۶۰۳۳
---
# غزل شمارهٔ ۶۰۳۳

<div class="b" id="bn1"><div class="m1"><p>باده بی لعل لب دلبر نمی باید زدن</p></div>
<div class="m2"><p>غوطه در دریای بی گوهر نمی باید زدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با حیا نتوان ز لعل دلبران سیراب شد</p></div>
<div class="m2"><p>کوزه سربسته بر کوثر نمی باید زدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست چین در کار آن پیشانی واکرده را</p></div>
<div class="m2"><p>صفحه آیینه را مسطر نمی باید زدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رنج باریک آورد آمیزش سیمین بران</p></div>
<div class="m2"><p>سر برون چون رشته از گوهر نمی باید زدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رخنه ای زندان گردون را به جز تسلیم نیست</p></div>
<div class="m2"><p>در قفس بیهوده بال و پر نمی باید زدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواب آسایش گرانسنگ است خون مرده را</p></div>
<div class="m2"><p>بر رگ این غافلان نشتر نمی باید زدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا به آن خشک چون آیینه بتوان ساختن</p></div>
<div class="m2"><p>قطره در ظلمت چو اسکندر نمی باید زدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زردرویی می کند یکسان به خاک تیره ات</p></div>
<div class="m2"><p>حلقه چون خورشید بر هر در نمی باید زدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تشنه چشمان آب و رنگ از لعل، صائب می برند</p></div>
<div class="m2"><p>در حضور زاهدان ساغر نمی باید زدن</p></div></div>