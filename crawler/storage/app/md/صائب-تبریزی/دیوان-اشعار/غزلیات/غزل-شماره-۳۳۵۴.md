---
title: >-
    غزل شمارهٔ ۳۳۵۴
---
# غزل شمارهٔ ۳۳۵۴

<div class="b" id="bn1"><div class="m1"><p>حسن روزی که صف آرایی آن مژگان کرد</p></div>
<div class="m2"><p>صف محشر علم شهرت خود پنهان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش ازین گر به شکر پسته نهان می کردند</p></div>
<div class="m2"><p>لب نو خط تو در پسته شکر پنهان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست ممکن که دگر قامت خود راست کند</p></div>
<div class="m2"><p>هر که را جلوه مستانه او ویران کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داد بر باد سر سبز خود از بی مغزی</p></div>
<div class="m2"><p>هر که چون پسته درین بزم لبی خندان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه ضرورست به تدبیر کنی مشکلتر؟</p></div>
<div class="m2"><p>مشکلی را که به تسلیم توان آسان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیقراری نتوان برد به دریا از موج</p></div>
<div class="m2"><p>به دوا درد طلب را نتوان درمان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که با ابر کرم کرد چو دریا صائب</p></div>
<div class="m2"><p>در حقیقت به همه روی زمین احسان کرد</p></div></div>