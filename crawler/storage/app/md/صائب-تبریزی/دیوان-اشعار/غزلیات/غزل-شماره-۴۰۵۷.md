---
title: >-
    غزل شمارهٔ ۴۰۵۷
---
# غزل شمارهٔ ۴۰۵۷

<div class="b" id="bn1"><div class="m1"><p>زان قامت بلند نظر باز نگذرد</p></div>
<div class="m2"><p>زین سرو هیچ مرغ به پرواز نگذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هنگامه سخن به سخن گرم می شود</p></div>
<div class="m2"><p>صاحب سخن ز چشم سخنسار نگذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اشک از غبار خاطر من ره برون نبرد</p></div>
<div class="m2"><p>زین گرد سیل خانه برانداز نگذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سیر لاله زار زند نعل واژگون</p></div>
<div class="m2"><p>برخاک کشتگان اگر از ناز نگذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سینه من است ازان کبک خوشخرام</p></div>
<div class="m2"><p>کوهی کز آن عقاب به پرواز نگذرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صائب ز عجز نیست ز راه مروت است</p></div>
<div class="m2"><p>فریاد من اگر ز هم آواز نگذرد</p></div></div>