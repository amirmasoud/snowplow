---
title: >-
    غزل شمارهٔ ۸۵۵
---
# غزل شمارهٔ ۸۵۵

<div class="b" id="bn1"><div class="m1"><p>دید ز خون دلم لاله ستان خاک را</p></div>
<div class="m2"><p>آبله دل شکست شیشه افلاک را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لاله و گل خون کنند بر سر هر شبنمی</p></div>
<div class="m2"><p>گر به گلستان بری روی عرقناک را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا لب ساغر رسید بر لب و دندان او</p></div>
<div class="m2"><p>سر به ثریا رسید سلسله تاک را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر دل آیینه ابر سایه دشمن بود</p></div>
<div class="m2"><p>غوطه به خون می دهد باده دل پاک را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این سر خونین کیست، کز نفس آتشین</p></div>
<div class="m2"><p>چشمه خورشید کرد حلقه فتراک را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسن خداداد را مرتبه دیگرست</p></div>
<div class="m2"><p>باده چه مستی دهد جان طربناک را؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روزن هر خانه ای در خور وسعت بود</p></div>
<div class="m2"><p>دیده دل روزن است خانه افلاک را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من کیم و کیستم تا سر سوداییم</p></div>
<div class="m2"><p>داغ گذارد به دل لاله فتراک را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوهر شهوار را مهره گل نشمرد</p></div>
<div class="m2"><p>هر که ز صائب شنید این غزل پاک را</p></div></div>