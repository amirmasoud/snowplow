---
title: >-
    غزل شمارهٔ ۳۳۱۴
---
# غزل شمارهٔ ۳۳۱۴

<div class="b" id="bn1"><div class="m1"><p>از بتان شسته عذاری که حجابی دارد</p></div>
<div class="m2"><p>چشم بد دور که خوش عالم آبی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خار در دیده بی پرده شبنم شکند</p></div>
<div class="m2"><p>از حیا چهره هر گل که نقابی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دل روشن اگر یار نمی پردازد</p></div>
<div class="m2"><p>حسن مستور ز آیینه حجابی دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نتوان دید در آن روی عرقناک دلیر</p></div>
<div class="m2"><p>گل این باغ عجب تلخ گلابی دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شب اندوه وفادار ندارد پایان</p></div>
<div class="m2"><p>صبح عشرت نفس پا به رکابی دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست ممکن که به خورشید درخشان نرسد</p></div>
<div class="m2"><p>هر که چون شبنم گل چشم پرآبی دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دم نشمرده محال است برآرد چون صبح</p></div>
<div class="m2"><p>هر که در مد نظر روز حسابی دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون نفس راست کنم من، که به صحرای طلب</p></div>
<div class="m2"><p>گر همه سنگ نشان است شتابی دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خضر را تشنه ز سرچشمه حیوان آورد</p></div>
<div class="m2"><p>وادی عشق فریبنده سرابی دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا به خاری نرساند ننشیند از پا</p></div>
<div class="m2"><p>در جگر آبله تا قطره آبی دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شب تارش به دو خورشید بود آبستن</p></div>
<div class="m2"><p>هر که در وقت سحر جام شرابی دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مزرع ماست که آبش بود از دیده خویش</p></div>
<div class="m2"><p>ور نه هر کشت که دیدیم سحابی دارد</p></div></div>