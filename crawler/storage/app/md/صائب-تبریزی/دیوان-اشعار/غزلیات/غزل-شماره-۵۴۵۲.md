---
title: >-
    غزل شمارهٔ ۵۴۵۲
---
# غزل شمارهٔ ۵۴۵۲

<div class="b" id="bn1"><div class="m1"><p>ما چو سرو از راستی دامن به بار افشانده ایم</p></div>
<div class="m2"><p>آستنی چون شاخ گل بر نوبهار افشانده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در زمین قابل و ناقابل از دریادلی</p></div>
<div class="m2"><p>تخم مهری همچو ابر نوبهار افشانده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچنان باریم بر دلها چو نخل بی ثمر</p></div>
<div class="m2"><p>گرچه از هرکس که سنگی خورده بار افشانده ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر بر آورده است چون مژگان ز پیش چشم ما</p></div>
<div class="m2"><p>از عداوت زیر پای هرکه خار افشانده ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حاصل ما از سخن جز دود آهی بیش نیست</p></div>
<div class="m2"><p>در زمین کاغذین تخم شرار افشانده ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شهپر دریا رسیدن نیست ما را همچو موج</p></div>
<div class="m2"><p>مشت خاری پیش سیل نوبهار افشانده ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساحل آماده ای گشته است هر آغوش موج</p></div>
<div class="m2"><p>گر غبار از دل به بحر بیکنار افشانده ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست غیر از بحر چون سیلاب ما را منزلی</p></div>
<div class="m2"><p>گرد راه از خویش در آغوش یار افشانده ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نونیاز سنگ طفلان نیست جان سخت ما</p></div>
<div class="m2"><p>ما در آغاز جنون این شاخسار افشانده ایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیستیم از جلوه باران رحمت ناامید</p></div>
<div class="m2"><p>تخم خشکی در زمین انتظار افشانده ایم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خواب غفلت شسته ایم از چشم خواب آلودگان</p></div>
<div class="m2"><p>هرکجا اشکی ز چشم اشکبار افشانده ایم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سنبلستانی شده است از پرده غیب آشکار</p></div>
<div class="m2"><p>هرکجا چون خامه جعد مشکبار افشانده ایم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دست بیدادی گریبانگیر ما گردیده است</p></div>
<div class="m2"><p>از رعونت دامن خود گر ز خار افشانده ایم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دست ما در دامن روز جزا خواهد گرفت</p></div>
<div class="m2"><p>بر ثمردستی که چون سرو و چنار افشانده ایم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سرفرازان جهان در پیش ما سر می نهند</p></div>
<div class="m2"><p>تا چو نخل دار از خود برگ و بار افشانده ایم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گرچه از دریاست دخل ما چو ابر نوبهار</p></div>
<div class="m2"><p>در کنار بحر گوهر بی شمار افشانده ایم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اشک ما را نیست جز دامان خود سرمنزلی</p></div>
<div class="m2"><p>تخم خود از بی زمینی در کنار افشانده ایم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باشد از آهن دلان صائب گشاد کار ما</p></div>
<div class="m2"><p>تخم خود در سنگ ما همچون شرار افشانده ایم</p></div></div>