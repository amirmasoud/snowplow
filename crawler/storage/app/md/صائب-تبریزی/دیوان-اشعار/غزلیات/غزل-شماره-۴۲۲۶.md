---
title: >-
    غزل شمارهٔ ۴۲۲۶
---
# غزل شمارهٔ ۴۲۲۶

<div class="b" id="bn1"><div class="m1"><p>یوسف رخان ز شوق سراغ تومی کنند</p></div>
<div class="m2"><p>از پیرهن فتیله داغ تومی کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون آفتاب اگر چه جهان را گرفته ای</p></div>
<div class="m2"><p>ذرات کاینات سراغ تومی کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گردنکشان که باج زعالم گرفته اند</p></div>
<div class="m2"><p>چون شیشه سجده پیش ایاغ تومی کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جمعی که چشم بسته گذشتند از بهشت</p></div>
<div class="m2"><p>دریوزه نسیم ز باغ تومی کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کج نه کله که لاله عذاران این چمن</p></div>
<div class="m2"><p>دل خوش به عنبرینه داغ تومی کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می نوش وشاد باش که گلهای این چمن</p></div>
<div class="m2"><p>کسب شکفتگی ز دماغ تومی کنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من کیستم که پردگیان حریم قدس</p></div>
<div class="m2"><p>پروانه وار طوف چراغ تومی کنند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب چه بلبلی تو که گلهای این چمن</p></div>
<div class="m2"><p>از دیده خون روان به سراغ تومی کنند</p></div></div>