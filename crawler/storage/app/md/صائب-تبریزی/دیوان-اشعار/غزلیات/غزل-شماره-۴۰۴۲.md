---
title: >-
    غزل شمارهٔ ۴۰۴۲
---
# غزل شمارهٔ ۴۰۴۲

<div class="b" id="bn1"><div class="m1"><p>عاشق ز رفتن دل بی‌تاب می‌برد</p></div>
<div class="m2"><p>فیضی که خاک از آمدن آب می‌برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سینه‌های صاف نگیرد قرار دل</p></div>
<div class="m2"><p>آیینه اختیار ز سیماب می‌برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در چشم داغ دیده کشد سرمه از نمک</p></div>
<div class="m2"><p>پروانه را کسی که به مهتاب می‌برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگذاشت آب در جگر تیغ زخم من</p></div>
<div class="m2"><p>جان از سفال تشنه کجا آب می‌برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رویی که چشم من شده محو نظاره‌اش</p></div>
<div class="m2"><p>بی‌طاقتی ز گوهر سیراب می‌برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مستانه جلوه‌های تو ای آب زندگی</p></div>
<div class="m2"><p>گردش ز یاد حلقه گرداب می‌برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از پیچ و تاب رشته عمرش گره شود</p></div>
<div class="m2"><p>از هر دلی که موی میان تاب می‌برد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در باده نشئه از نظر زاهدان نماند</p></div>
<div class="m2"><p>چشم ندیدگان ز گهر آب می‌برد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب مرا چو آب خمار آورد به هوش</p></div>
<div class="m2"><p>هرچند هوش خلق می ناب می‌برد</p></div></div>