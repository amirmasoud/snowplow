---
title: >-
    غزل شمارهٔ ۲۹۱۸
---
# غزل شمارهٔ ۲۹۱۸

<div class="b" id="bn1"><div class="m1"><p>اگرچه هر گلی زین گلستان جای دگر دارد</p></div>
<div class="m2"><p>بهم غلطیدن گلها تماشای دگر دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زکوکو گفتن قمری چنین معلوم می گردد</p></div>
<div class="m2"><p>که نعل طوق در آتش زبالای دگر دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زنبض بیقرارش می توان دریافت این معنی</p></div>
<div class="m2"><p>که در مدنظر این موج دریای دگر دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در این صحرای پروحشت نفس را راست چون سازد؟</p></div>
<div class="m2"><p>که صید وحشی من رو به صحرای دگر دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرا زین خانه دلگیر بیرون پای نگذارد؟</p></div>
<div class="m2"><p>اگر غیر از دل آن جان جهان جای دگر دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگرچه از تماشا گوهر عبرت به دست افتد</p></div>
<div class="m2"><p>نظر پوشیدن از دنیا تماشای دگر دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا از مستی سرشار چشم یار روشن شد</p></div>
<div class="m2"><p>که این پیمانه زیر پرده مینای دگر دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به حرف و صوت از آیینه چون طوطی نیم قانع</p></div>
<div class="m2"><p>کز آن آیینه سیما دل تمنای دگر دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زمن پوشیده با اغیار می گردی، نمی دانی</p></div>
<div class="m2"><p>که از هر داغ، عاشق چشم بینای دگر دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به فکر سینه دل در زلف مشکینش کجا افتد؟</p></div>
<div class="m2"><p>که در هر حلقه ای دام تماشای دگر دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به سنگ کودکان مجنون از ان تن می دهد صائب</p></div>
<div class="m2"><p>که در کهسار سیل تند غوغای دگر دارد</p></div></div>