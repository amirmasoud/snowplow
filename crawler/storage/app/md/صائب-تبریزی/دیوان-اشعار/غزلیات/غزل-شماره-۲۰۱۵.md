---
title: >-
    غزل شمارهٔ ۲۰۱۵
---
# غزل شمارهٔ ۲۰۱۵

<div class="b" id="bn1"><div class="m1"><p>از لخت دل مرا مژه در چشم تر شکست</p></div>
<div class="m2"><p>چون شاخ نازکی که ز جوش ثمر شکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون تیغ آب جوهر من شد زیادتر</p></div>
<div class="m2"><p>چندان که روزگار مرا بیشتر شکست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جای شراب عشق نگیرد شراب عقل</p></div>
<div class="m2"><p>نتوان خمار بحر به آب گهر شکست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در عاشقی همین دل بلبل شکسته نیست</p></div>
<div class="m2"><p>اول سبوی غنچه درین رهگذر شکست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از جام غیر، آن لب میگون زیاد نیست</p></div>
<div class="m2"><p>باید خمار خود ز شراب دگر شکست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گردید توتیای قلم استخوان من</p></div>
<div class="m2"><p>از بس مرا فراق تو بر یکدگر شکست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مژگان اشکبار تو ای شمع انجمن</p></div>
<div class="m2"><p>صد تیغ آبدار مرا در جگر شکست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خون می گشاید از رگ الماس سایه اش</p></div>
<div class="m2"><p>آن نیش غمزه ای که مرا در جگر شکست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب چه شکرهاست که ما را چو زلف، یار</p></div>
<div class="m2"><p>در هر شکستنی به طریق دگر شکست</p></div></div>