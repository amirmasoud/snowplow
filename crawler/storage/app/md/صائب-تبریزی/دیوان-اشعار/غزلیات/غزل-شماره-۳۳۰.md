---
title: >-
    غزل شمارهٔ ۳۳۰
---
# غزل شمارهٔ ۳۳۰

<div class="b" id="bn1"><div class="m1"><p>سپند از مردم چشم است حسن عالم آرا را</p></div>
<div class="m2"><p>که نیل چشم زخم از عنبر ساراست دریا را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کند مژگان من هرگاه دست از آستین بیرون</p></div>
<div class="m2"><p>شود گرداب بر کف کاسه دریوزه دریا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه پروا دارد از سنگ ملامت دل چو شد وحشی؟</p></div>
<div class="m2"><p>که کوه قاف نتواند شکستن بال عنقا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر آن سرو بالا بر سر من سایه اندازد</p></div>
<div class="m2"><p>وگرنه سایه بی دست شاخ و برگ، سودا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگردد مانع پرواز جان را تار و پود تن</p></div>
<div class="m2"><p>نبندد رشته مریم پر و بال مسیحا را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هوس هر چند گستاخ است، عذرش صورتی دارد</p></div>
<div class="m2"><p>به یوسف می توان بخشید تقصیر زلیخا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کند موج سراب دشت پیما را عنانداری</p></div>
<div class="m2"><p>هوسناکی که می پیچد به کف دامان دنیا را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مبین زنهار اسباب تعلق را به چشم کم</p></div>
<div class="m2"><p>که سوزن لنگر پرواز می گردد مسیحا را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به اندک التفاتی، نقش پای ناقه لیلی</p></div>
<div class="m2"><p>به مجنون دامن گل می کند دامان صحرا را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سیه بختی چه سازد با من حرف آفرین صائب؟</p></div>
<div class="m2"><p>نگردد سرمه از گفتار مانع چشم گویا را</p></div></div>