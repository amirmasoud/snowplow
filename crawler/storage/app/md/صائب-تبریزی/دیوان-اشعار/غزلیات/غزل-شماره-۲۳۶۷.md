---
title: >-
    غزل شمارهٔ ۲۳۶۷
---
# غزل شمارهٔ ۲۳۶۷

<div class="b" id="bn1"><div class="m1"><p>خاطر جمع مرا پیری پریشان حال کرد</p></div>
<div class="m2"><p>تار و پود هستیم را رشته آمال کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد به دست افشاندن از روی زمین حاصل مرا</p></div>
<div class="m2"><p>آنچه اسکندر به زور بازوی اقبال کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با وجود خاکساری سربلند افتاده ام</p></div>
<div class="m2"><p>چون فروغ مهر و مه نتوان مرا پامال کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تشنه چشمان هوس را سیری از دیدار نیست</p></div>
<div class="m2"><p>چون توان آیینه را قانع به یک تمثال کرد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فاش شد از یک قدح رازی که در دل داشتم</p></div>
<div class="m2"><p>سوز پنهان مرا بی پرده این تبخال کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر سر خاک شهیدان رنجه سازی گر قدم</p></div>
<div class="m2"><p>دعوی خون را همین جامی توان پامال کرد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کرد چون مه عاقبت از رنج باریکش هلال</p></div>
<div class="m2"><p>جام هر کس را فلک از مهر مالامال کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چرخ سنگین دل بر آتش داشت روی نازکش</p></div>
<div class="m2"><p>از شراب بیغمی تا چهره را گل آل کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حسن را آرایشی چون چشم پاک عشق نیست</p></div>
<div class="m2"><p>طوق قمری سرو را مستغنی از خلخال کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>داشتم از زندگی امید حسن عاقبت</p></div>
<div class="m2"><p>عشق در پیری مرا بازیچه اطفال کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از دل پررخنه فیض ابر رحمت یافتیم</p></div>
<div class="m2"><p>کشت مار اسیر چشم از آب این غربال کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر به این عنوان شود صائب سخن بی اعتبار</p></div>
<div class="m2"><p>طوطی گویای ما را زود خواهد لال کرد</p></div></div>