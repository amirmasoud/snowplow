---
title: >-
    غزل شمارهٔ ۳۷۲۵
---
# غزل شمارهٔ ۳۷۲۵

<div class="b" id="bn1"><div class="m1"><p>که می تواند ازان چشم چشم بردارد؟</p></div>
<div class="m2"><p>که ریشه از صف مژگان به هر جگر دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا کشیده به زنجیر نازک اندامی</p></div>
<div class="m2"><p>که پیچ و تاب سر زلف در کمر دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بس که محو شدم در نظاره قاتل</p></div>
<div class="m2"><p>نشد که زخم من از تیغ آب بر دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز برق و باد قدم وام کن به سیر چمن</p></div>
<div class="m2"><p>که نوبهار ز هر برگ بال و پر دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز کام هر دو جهان آستین فشان گذرد</p></div>
<div class="m2"><p>دل رمیده ما تا چه در نظر دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز سست عزمی خود ما به خضر محتاجیم</p></div>
<div class="m2"><p>و گرنه سیل چه حاجت به راهبر دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشو به تافتن رو، ز خصم کجرو امن</p></div>
<div class="m2"><p>هدف ز پشت کمان بیشتر خطر دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خطر ز راهزنان کمترست پیرو را</p></div>
<div class="m2"><p>که پیش روی خود از رهنما سپر دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مکن ز بستگی کار خویش شکوه که نی</p></div>
<div class="m2"><p>به قدر بند درین بوستان شکر دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مگر فتد به غلط سیل را به اینجا راه</p></div>
<div class="m2"><p>وگرنه کیست که ما را ز خاک بردارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رسید سرو ز بی حاصلی به آزادی</p></div>
<div class="m2"><p>کدام نخل برومند این ثمر دارد؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز قرب سیمبران کاهش است قسمت ما</p></div>
<div class="m2"><p>که در گداز بود رشته تا گهر دارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>درآ به عالم آب از جهان هشیاری</p></div>
<div class="m2"><p>که هر حباب در او عالم دگر دارد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از شب دگران راست یک سحر صائب</p></div>
<div class="m2"><p>ز آه سرد شب ما دو صد سحر دارد</p></div></div>