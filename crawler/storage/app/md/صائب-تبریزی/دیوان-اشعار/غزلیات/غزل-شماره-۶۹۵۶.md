---
title: >-
    غزل شمارهٔ ۶۹۵۶
---
# غزل شمارهٔ ۶۹۵۶

<div class="b" id="bn1"><div class="m1"><p>ای آه جگرسوز ز شست تو خدنگی</p></div>
<div class="m2"><p>کوه الم از دامن صحرای تو سنگی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دشت خطرناک تو هر خار سنانی</p></div>
<div class="m2"><p>از بحر پرآشوب تو هر موج نهنگی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گردون سراسیمه و این خاک گرانسنگ</p></div>
<div class="m2"><p>در کوچه سودای تو دیوانه و سنگی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در راه تمنای تو ارباب طلب را</p></div>
<div class="m2"><p>عمر ابد و مرگ، شتابی و درنگی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صحرایی سودای تو هر نافه بویی</p></div>
<div class="m2"><p>سودایی صحرای تو هر لاله رنگی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برقی که ازو طور به زنهار درآید</p></div>
<div class="m2"><p>از نرگس مژگان تو رم خورده خدنگی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با شوخی چشم تو رم چشم غزالان</p></div>
<div class="m2"><p>در دیده روشن گهران آهوی لنگی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>موجی که بود سلسله جنبان تلاطم</p></div>
<div class="m2"><p>با شوخی مژگان تو همچون رگ سنگی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یاقوت ز شرم لب رنگین سخن تو</p></div>
<div class="m2"><p>چون چهره خجلت زده هر لحظه به رنگی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از حسن پر از شیوه آن کان ملاحت</p></div>
<div class="m2"><p>قانع نتوان گشت به صلحی و به جنگی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از بار شکوه تو بود خامه صائب</p></div>
<div class="m2"><p>چون سبزه نورسته نهان در ته سنگی</p></div></div>