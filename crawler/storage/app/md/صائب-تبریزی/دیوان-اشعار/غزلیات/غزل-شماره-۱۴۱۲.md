---
title: >-
    غزل شمارهٔ ۱۴۱۲
---
# غزل شمارهٔ ۱۴۱۲

<div class="b" id="bn1"><div class="m1"><p>ریخت دندان و هوای می و پیمانه بجاست</p></div>
<div class="m2"><p>مهره برچیده شد و بازی طفلانه بجاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل سیاه است اگر گشت بناگوش سفید</p></div>
<div class="m2"><p>پا اگر نیست بجا، لغزش مستانه بجاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خارخاری به دل از عمر سبکرو مانده است</p></div>
<div class="m2"><p>مشت خار و خسی از سیل به ویرانه بجاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آسیا گر چه برآورد ز بنیادش گرد</p></div>
<div class="m2"><p>هوس نشو و نما در گره دانه بجاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نسبت شوق به هجران و وصال است یکی</p></div>
<div class="m2"><p>رفت ایام گل و شورش دیوانه بجاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یار نوخط شد و آغاز جنون است مرا</p></div>
<div class="m2"><p>شمع خاموش شد و گرمی پروانه بجاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم من بر در و دیوار حرم افتاده است</p></div>
<div class="m2"><p>نگذارند مرا گر به صنمخانه، بجاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر چه در خواب گران عمر سر آمد صائب</p></div>
<div class="m2"><p>همچنان رغبت شیرینی افسانه بجاست</p></div></div>