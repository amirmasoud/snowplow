---
title: >-
    غزل شمارهٔ ۵۷۴۸
---
# غزل شمارهٔ ۵۷۴۸

<div class="b" id="bn1"><div class="m1"><p>بر آن سرم که وطن در دیار خویش کنم</p></div>
<div class="m2"><p>تأملی که ندارم به کار خویش کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کنم چو صیقل فولاد، رویی از آهن</p></div>
<div class="m2"><p>جلای آینه پر غبار خویش کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نهم چو آینه روز شمار را در پیش</p></div>
<div class="m2"><p>شمار معصیت بی شمار خویش کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به گوش تا نرسیده است بانگ طبل رحیل</p></div>
<div class="m2"><p>ز جای خیزم و سامان کار خویش کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز چشم عیب شناسان نگاه وام کنم</p></div>
<div class="m2"><p>نظر به روز خود و روزگار خویش کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عنان کشم ز پی لشکر شکسته خلق</p></div>
<div class="m2"><p>چو گرد، سر ز پی شهسوار خویش کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به کار خویش ببندم حواس را هر یک</p></div>
<div class="m2"><p>نظام کارکنان دیار خویش کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میان خدمت میر و وزیر بگشایم</p></div>
<div class="m2"><p>همین ملازمت کردگار خویش کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به عرصه تا محک امتحان نیامده است</p></div>
<div class="m2"><p>علاج این زر ناقص عیار خویش کنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کنم ز سنگ بنا، خانه ای به رنگ صدف</p></div>
<div class="m2"><p>حمایت گهر آبدار خویش کنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو شمع، خلوت فانوسی اختیار کنم</p></div>
<div class="m2"><p>غذای خویش ز جسم نزار خویش کنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به بوی سیب قناعت کنم ز باغ جهان</p></div>
<div class="m2"><p>لباس خویش چو به از غبار خویش کنم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به خون دل ز می لاله گون بشویم دست</p></div>
<div class="m2"><p>به اشک تلخ علاج خمار خویش کنم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دگر سیاه نسازم نظر به هیچ کتاب</p></div>
<div class="m2"><p>نظر به دفتر لیل و نهار خویش کنم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به نان خشک قناعت کنم ز ناز و نعیم</p></div>
<div class="m2"><p>لبی تر ا ز مژه اشکبار خویش کنم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو خار خشک بسازم به برگ بی برگی</p></div>
<div class="m2"><p>خزان سرد نفس را بهار خویش کنم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>برون کنم ز جگر خار خار گلشن را</p></div>
<div class="m2"><p>نظاره جگر داغدار خویش کنم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دل رمیده خود را به حیله سازم رام</p></div>
<div class="m2"><p>شکار خلق گذارم، شکار خویش کنم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به هر فسرده نفس عرض گفتگو ندهم</p></div>
<div class="m2"><p>نثار سوخته جانان شرار خویش کنم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بس است آنچه به غفلت گذشته است از عمر</p></div>
<div class="m2"><p>گذشته را سبق روزگار خویش کنم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>قدم ز گوشه عزلت برون نهم وقتی</p></div>
<div class="m2"><p>که نقد هر دو جهان در کنار خویش کنم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز دامن طلب آن روز دست بردارم</p></div>
<div class="m2"><p>که دست تنگ در آغوش یار خویش کنم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو بوی سوخته ای در جهان نمی یابم</p></div>
<div class="m2"><p>ز خلق رو به دل داغدار خویش کنم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کمین دشمن دانا، مربی مردست</p></div>
<div class="m2"><p>نظر ز روی عداوت به کار خویش کنم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو نیست آب مروت به چشم خلق، آن به</p></div>
<div class="m2"><p>که تازه روی خود از جویبار خویش کنم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>علاج سیل حوادث جز این نمی دانم</p></div>
<div class="m2"><p>که خاکساری خود را حصار خویش کنم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اسیر کشمکش جلوه های تقدیرم</p></div>
<div class="m2"><p>کجاست فرصت آنم که کار خویش کنم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جواب آن غزل اوحدی است این صائب</p></div>
<div class="m2"><p>که او شمار خود و من شمار خویش کنم</p></div></div>