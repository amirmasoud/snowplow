---
title: >-
    غزل شمارهٔ ۶۴۴۶
---
# غزل شمارهٔ ۶۴۴۶

<div class="b" id="bn1"><div class="m1"><p>من که در فردوس افتادم به نقد از یاد او</p></div>
<div class="m2"><p>بی نیازم از تمنای بهشت آباد او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سر کون و مکان آزاد برخیزد چو سرو</p></div>
<div class="m2"><p>بر سر هر کس که افتد سایه شمشاد او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رتبه بیداد او بالاترست از التفات</p></div>
<div class="m2"><p>وای بر آن کس که دارد شکوه از بیداد او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آدم مسکین به یک خامی که در فردوس کرد</p></div>
<div class="m2"><p>چاک شد چون دانه گندم دل اولاد او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می تواند داد صائب آسمان را خاکمال</p></div>
<div class="m2"><p>هر که را بر کوه باشد پشت از امداد او</p></div></div>