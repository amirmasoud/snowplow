---
title: >-
    غزل شمارهٔ ۵۷۸۱
---
# غزل شمارهٔ ۵۷۸۱

<div class="b" id="bn1"><div class="m1"><p>روی دلی چو غنچه ز بلبل ندیده ام</p></div>
<div class="m2"><p>نقش مراد از آینه گل ندیده ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن صید تشنه ام که درین دشت آتشین</p></div>
<div class="m2"><p>آبی بغیر تیغ تغافل ندیده ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در باغ اگر چه چشم چو شبنم گشوده ام</p></div>
<div class="m2"><p>از شرم عندلیب رخ گل ندیده ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان زنده مانده ام که هنوز از حجاب عشق</p></div>
<div class="m2"><p>رخسار یار را به تأمل ندیده ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرد مصاف در همه جا یافت می شود</p></div>
<div class="m2"><p>در هیچ عرصه مرد تحمل ندیده ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با خصم در مقام تلافی ازان نیم</p></div>
<div class="m2"><p>کز تیغ انتقام تعلل ندیده ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قانع به بوی پیرهن از وصل گل شده است</p></div>
<div class="m2"><p>عاشق به سیر چشمی بلبل ندیده ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوری ز یار صائب و خامش نشسته ای</p></div>
<div class="m2"><p>عاشق به این شکیب و تحمل ندیده ام</p></div></div>