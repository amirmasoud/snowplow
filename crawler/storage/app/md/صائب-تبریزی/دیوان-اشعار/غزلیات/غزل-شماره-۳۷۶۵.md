---
title: >-
    غزل شمارهٔ ۳۷۶۵
---
# غزل شمارهٔ ۳۷۶۵

<div class="b" id="bn1"><div class="m1"><p>ترا چه غم که شب ما دراز می‌گذرد؟</p></div>
<div class="m2"><p>که روزگار تو در خواب ناز می‌گذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غرض ز سنگدلی داغ کردن شهداست</p></div>
<div class="m2"><p>به لاله‌زار اگر آن سرو ناز می‌گذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیازمندی از او همچو ناز می‌بارد</p></div>
<div class="m2"><p>ز ناز اگر چه ز من بی‌نیاز می‌گذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز پا کشیدن زلف و غبار خط پیداست</p></div>
<div class="m2"><p>که وقت خوبی آن دل‌نواز می‌گذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو همچو باد سبک می‌روی، چه می‌دانی</p></div>
<div class="m2"><p>بر این خرابه چه از ترکتاز می‌گذرد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز پرده‌داری دل سینه‌ام چو گل شد چاک</p></div>
<div class="m2"><p>چه بر صدف ز گهرهای راز می‌گذرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حیات زنده‌دلان در گداز خویشتن است</p></div>
<div class="m2"><p>نمرده شمع کج از گداز می‌گذرد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خبر ز عشق حقیقی ندارد آن غافل</p></div>
<div class="m2"><p>که زندگیش به عشق مجاز می‌گذرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز کشور دل محمود گرد می‌خیزد</p></div>
<div class="m2"><p>اگر نسیم به زلف ایاز می‌گذرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زبان تیغ شهادت چنان فریبنده است</p></div>
<div class="m2"><p>که خضر از سر عمر دراز می‌گذرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو صائب آن که به دولت‌سرای فقر رسید</p></div>
<div class="m2"><p>ز صاحبان کرم بی‌نیاز می‌گذرد</p></div></div>