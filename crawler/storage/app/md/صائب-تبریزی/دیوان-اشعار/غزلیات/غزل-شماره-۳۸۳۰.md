---
title: >-
    غزل شمارهٔ ۳۸۳۰
---
# غزل شمارهٔ ۳۸۳۰

<div class="b" id="bn1"><div class="m1"><p>ترا که روی به خلق است از خدا چه رسد؟</p></div>
<div class="m2"><p>به پشت آینه پیداست کز صفا چه رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه زلف شانه کند نه به چشم سرمه کشد</p></div>
<div class="m2"><p>به خود نمی رسد آن شوخ تا به ما چه رسد!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دویدن است ز نعمت نصیب چشم حریص</p></div>
<div class="m2"><p>ز دانه غیر تردد به آسیا چه رسد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز شبنم است مهیا هزار دیده شور</p></div>
<div class="m2"><p>ازین بهار به مرغان بینوا چه رسد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز حرف مردم بیگانه گوش می گیرم</p></div>
<div class="m2"><p>به آشنا و سخنهای آشنا چه رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به چشم خیره خورشید آب می گردد</p></div>
<div class="m2"><p>به دیده من ازان آتشین لقا چه رسد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز چشم منتظران تا به مصر یک دام است</p></div>
<div class="m2"><p>ز بوی پیرهن آخر به چشم ما چه رسد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز عشق قسمت زاهد کلام بی مغزی است</p></div>
<div class="m2"><p>بغیر کاه ز خرمن به کهربا چه رسد؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رساند کسب هوا خانه حباب به آب</p></div>
<div class="m2"><p>به مغز پوچ تو تا صائب از هوا چه رسد</p></div></div>