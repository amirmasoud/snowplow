---
title: >-
    غزل شمارهٔ ۳۲۶۷
---
# غزل شمارهٔ ۳۲۶۷

<div class="b" id="bn1"><div class="m1"><p>چشم شوخ تو چو بر همزن مژگان گردد</p></div>
<div class="m2"><p>دو جهان فتنه به هم دست و گریبان گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در غبار دل ما آه عبث پیچیده است</p></div>
<div class="m2"><p>این نه ابری است که از باد پریشان گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حیرت وصل زبان بند لب گفتارست</p></div>
<div class="m2"><p>طوطی آن به که جدا از شکرستان گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی حجاب تن خاکی نرسد جان به کمال</p></div>
<div class="m2"><p>پسته بی پوست محال است که خندان گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داغ محرومی اگر آب کند سایل را</p></div>
<div class="m2"><p>به ازان است که شرمنده احسان گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از کفن جامه احرام سرانجام دهد</p></div>
<div class="m2"><p>هرکه را درد طلب سلسله جنبان گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق هر روز شد از روز دگر مشکلتر</p></div>
<div class="m2"><p>نیست در طالع این کار که آسان گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرکه چون آبله در حلقه آهل نظرست</p></div>
<div class="m2"><p>هر قدم گرد سر خار مغیلان گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در پریخانه دل نیست قرارش صائب</p></div>
<div class="m2"><p>طفل اشکی که بدآموز به دامان گردد</p></div></div>