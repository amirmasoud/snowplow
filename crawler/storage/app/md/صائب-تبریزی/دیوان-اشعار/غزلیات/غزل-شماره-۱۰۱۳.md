---
title: >-
    غزل شمارهٔ ۱۰۱۳
---
# غزل شمارهٔ ۱۰۱۳

<div class="b" id="bn1"><div class="m1"><p>گر نباشد حسن معنی، خط زیبا هم خوش است</p></div>
<div class="m2"><p>گر زبان گویا نباشد، دست گویا هم خوش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمع هم یاری است در هر جا نباشد آفتاب</p></div>
<div class="m2"><p>گر دل روشن نباشد، چشم بینا هم خوش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طفل طبعان را تماشا عمر ضایع کردن است</p></div>
<div class="m2"><p>چشم عبرت بین اگر باشد، تماشا هم خوش است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در مذاق قدردانان، قهر کم از لطف نیست</p></div>
<div class="m2"><p>گل اگر بر سر نباشد، خار در پا هم خوش ا ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند باشی همچو خون مرده در یک جا گره؟</p></div>
<div class="m2"><p>با غزالان چند روزی سیر صحرا هم خوش است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست دلگیری ز کوه بیستون فرهاد را</p></div>
<div class="m2"><p>عشق چون مشاطه گردد سنگ خارا هم خوش است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شسته رویان نیز می شویند گاه از دل غبار</p></div>
<div class="m2"><p>نوخطی هر جا نباشد، روی زیبا هم خوش است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر تو از بی لنگری، دریای پرشورست خاک</p></div>
<div class="m2"><p>ورنه هر کس دل به دریا کرد، دریا هم خوش است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر چه دارد نوبهار حسن او جوش دگر</p></div>
<div class="m2"><p>برگریزان دل صد پاره ما هم خوش است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیده یوسف شناسی نیست در مصر وجود</p></div>
<div class="m2"><p>ورنه با این تیرگی، زندان دنیا هم خوش است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عقل و هوش و صبر و دین و دل به یک نظاره رفت</p></div>
<div class="m2"><p>عشق چون دلال شد، سودای یکجا هم خوش است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وصل دایم، می کند افسرده صائب شوق را</p></div>
<div class="m2"><p>صحبت دریا خوش و دوری ز دریا هم خوش است</p></div></div>