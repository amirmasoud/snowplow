---
title: >-
    غزل شمارهٔ ۴۹۳۳
---
# غزل شمارهٔ ۴۹۳۳

<div class="b" id="bn1"><div class="m1"><p>من و عشقی که دست چرخ را چنبرکند زورش</p></div>
<div class="m2"><p>گذارد درفلاخن کوه قاف عقل راشورش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کمان نرم تیر سخت رادر چاشنی دارد</p></div>
<div class="m2"><p>مشو زنهار ایمن از فریب چشم رنجورش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز خال دلفریب یار مشکل جان توان بردن</p></div>
<div class="m2"><p>کنون کز گرد خط گردیده خاک آلود زنبورش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیاهی عذر خواهی همچو آب زندگی دارد</p></div>
<div class="m2"><p>مکن قطع امید از زلف و از شبهای دیجورش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درایام بهاران دیده نرگس شود گویا</p></div>
<div class="m2"><p>چه مستیها کند در دور خط تا چشم مخمورش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دامانش ز سیلاب حوادث گرد ننشیند</p></div>
<div class="m2"><p>خرابی راکه سازد گوشه چشم تو معمورش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه سازد باشراب عشق او یارب سبوی من</p></div>
<div class="m2"><p>که خندان می کند چون نار این نه شیشه رازورش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زمین سیر چشمان قناعت وسعتی دارد</p></div>
<div class="m2"><p>که دارد خنده برملک سلیمان دیده مورش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه آسوده است از دلگرمی غمخوار، بیماری</p></div>
<div class="m2"><p>که بربالین ز آه سرد باشد شمع کافورش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اثر دل زنده دارد شمع اقبال سکندر را</p></div>
<div class="m2"><p>که از آیینه بارد تا قیامت نور برگورش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز اقبال محبت درمقامی می یزنم جولان</p></div>
<div class="m2"><p>که طفل نی سوارآید به چشمم دارو منصورش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خوشا ابری که اشک خود به دامان صدف ریزد</p></div>
<div class="m2"><p>خوشا تاکی که گردد قسمت میخانه انگورش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چنین گیرد اگر دنبال ظالم اشک مظلومان</p></div>
<div class="m2"><p>برآرد جوش طوفان چون تنور نوح از گورش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خمار بحر هرگز نشکند ازقطره ای، صائب</p></div>
<div class="m2"><p>لب میگون چه سازد با خمار چشم مخمورش ؟</p></div></div>