---
title: >-
    غزل شمارهٔ ۵۴۶۴
---
# غزل شمارهٔ ۵۴۶۴

<div class="b" id="bn1"><div class="m1"><p>ما نفس بر لب به صد رنج و تعب می آوریم</p></div>
<div class="m2"><p>پیر می گردیم تا روزی به شب می آوریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزه حرف طلب دارد لب اهل کرم</p></div>
<div class="m2"><p>ما به منزل میهمان را بی طلب می آوریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رزق اگر دارد کلیدی در کف دست دعاست</p></div>
<div class="m2"><p>بی سبب ما زور بر پای طلب می آوریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منت مشکل گشایان نی به ناخن می کند</p></div>
<div class="m2"><p>زور بر دست دعای نیمشب می آوریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شوخ چشمی بین که پیش در شهوار حسب</p></div>
<div class="m2"><p>استخوان پوسیده ای چند از نسب می آوریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیستون را تیشه ما در فلاخن می نهد</p></div>
<div class="m2"><p>برجبین چون چین جواهر از غضب می آوریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب از اوضاع ما شوریده احوالان مپرس</p></div>
<div class="m2"><p>گوشه ای داریم و روزی را به شب می آوریم</p></div></div>