---
title: >-
    غزل شمارهٔ ۷۱۱
---
# غزل شمارهٔ ۷۱۱

<div class="b" id="bn1"><div class="m1"><p>پیوسته دل سیاه بود خلق تنگ را</p></div>
<div class="m2"><p>دایم ستاره سوخته باشد پلنگ را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد بیشتر ز قامت خم دل سیاهیم</p></div>
<div class="m2"><p>صیقل برد ز آینه هر چند زنگ را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر زر مگیر تنگ که از خرده شرار</p></div>
<div class="m2"><p>دایم به آهن است سر و کار سنگ را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از تیغ آبدار نترسند پردلان</p></div>
<div class="m2"><p>از چار موجه نیست محابا نهنگ را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از خلق تنگ بر تو جهان تنگ گشته است</p></div>
<div class="m2"><p>بیرون ز پای خویش کن این کفش تنگ را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حلوای آشتی است چو شد زهر عادتی</p></div>
<div class="m2"><p>رغبت به صلح نیست بدآموز جنگ را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد سحر ساحران ز عصای کلیم محو</p></div>
<div class="m2"><p>در راستان اثر نبود ریو و رنگ را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوزد ز یک خدنگ به هم، شست صاف تو</p></div>
<div class="m2"><p>چون دانه های سبحه قطار کلنگ را!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا هست در چمن اثر از رنگ و بوی گل</p></div>
<div class="m2"><p>صائب مده ز دست می لاله رنگ را</p></div></div>