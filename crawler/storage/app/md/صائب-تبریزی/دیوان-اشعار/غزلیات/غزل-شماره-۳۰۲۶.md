---
title: >-
    غزل شمارهٔ ۳۰۲۶
---
# غزل شمارهٔ ۳۰۲۶

<div class="b" id="bn1"><div class="m1"><p>خمار می مرا در گوشه میخانه می‌سوزد</p></div>
<div class="m2"><p>شراب من چو داغ لاله در پیمانه می‌سوزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کند تأثیر سوز عشق در شاه و گدا یکسان</p></div>
<div class="m2"><p>که بید و عود را آتش به یک دندانه می‌سوزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندارد گرمی هنگامه ما حاجت شمعی</p></div>
<div class="m2"><p>درین عشرت‌سرا پروانه از پروانه می‌سوزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آن رخسار آتشناک داغی بر جگر دارم</p></div>
<div class="m2"><p>که بیش از آشنا بر من دل بیگانه می‌سوزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کند در چشم مردم خواب را افسانه گر شیرین</p></div>
<div class="m2"><p>ز شیرینی مرا در دیده خواب افسانه می‌سوزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نگه دارد خدا از چشم بد آن آتشین‌رو را</p></div>
<div class="m2"><p>که در بیرون در از پرتوش پروانه می‌سوزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ندارد حاصل بی‌جذبه کوشش، ورنه هر موجی</p></div>
<div class="m2"><p>نفس در جستن آن گوهر یکدانه می‌سوزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مکن پهلو تهی از سوختن تا دیده ور گردی</p></div>
<div class="m2"><p>که سازد فاش را ز غیب را چون شانه می‌سوزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غم دنیا خوری بیش از غم عقبی، نمی‌دانی</p></div>
<div class="m2"><p>که قندیل حرم بی‌جا درین بتخانه می‌سوزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به فکر کلبه تاریک ما صائب نمی‌افتد</p></div>
<div class="m2"><p>چراغ آشنا رویی که در هر خانه می‌سوزد</p></div></div>