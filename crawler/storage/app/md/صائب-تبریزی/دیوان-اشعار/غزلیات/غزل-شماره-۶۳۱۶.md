---
title: >-
    غزل شمارهٔ ۶۳۱۶
---
# غزل شمارهٔ ۶۳۱۶

<div class="b" id="bn1"><div class="m1"><p>گشوده است در فیض رخنه دیوار</p></div>
<div class="m2"><p>به باغبان چه ضرورست دردسر دادن؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صدف ز دیده دریانژاد من دارد</p></div>
<div class="m2"><p>ز ابر آب گرفتن، عوض گهر دادن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کمینه بازی مژگان خونفشان من است</p></div>
<div class="m2"><p>عنان چو موج به دریای پر خط دادن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو سایه سرو نهاده است سر به دنبالش</p></div>
<div class="m2"><p>که یاد گیرد ازو پیچش کمر دادن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترا که نامه بری هست چون فغان صائب</p></div>
<div class="m2"><p>چه لازم است کتابت به نامه بر دادن؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به رنگ بی جگران رگ به نیشتر دادن</p></div>
<div class="m2"><p>بود به دشمن خونخوار خود جگر دادن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به قیمت گل و می ده اگر زری داری</p></div>
<div class="m2"><p>که بهترین هنرهاست زر به زر دادن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گل سر سبد بوستان خلق من است</p></div>
<div class="m2"><p>چو نخل سنگ بر سر خوردن و ثمر دادن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز پرفشانیم ای شمع رو چه می تابی؟</p></div>
<div class="m2"><p>به من نخست نبایست بال و پر دادن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه طعن لغزش مستانه می زنی بر من؟</p></div>
<div class="m2"><p>به من ز دور نبایست بیشتر دادن</p></div></div>