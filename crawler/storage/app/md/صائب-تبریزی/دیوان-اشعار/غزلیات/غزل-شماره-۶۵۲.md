---
title: >-
    غزل شمارهٔ ۶۵۲
---
# غزل شمارهٔ ۶۵۲

<div class="b" id="bn1"><div class="m1"><p>چو دیگران نه به ظاهر بود عبادت ما</p></div>
<div class="m2"><p>حضور قلب نمازست در شریعت ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازان ز دامن مقصود کوته افتاده است</p></div>
<div class="m2"><p>که پیش خلق درازست دست حاجت ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نکرده ایم چو شبنم بساطی از گل پهن</p></div>
<div class="m2"><p>چو غنچه بر سر زانوست خواب راحت ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو عنکبوت، مگس را نمی کنیم قدید</p></div>
<div class="m2"><p>هماشکار بود جذبه قناعت ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نهال خوش ثمر رهگذار طفلانیم</p></div>
<div class="m2"><p>که برگریز بود موسم فراغت ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر در آتش سوزان هزار غوطه خورد</p></div>
<div class="m2"><p>صدا بلند نسازد سپند غیرت ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تلاش گوشه عزلت ز تنگ خلقی هاست</p></div>
<div class="m2"><p>وگرنه بهر خدا نیست کنج عزلت ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که سرو قد ترا راه می تواند زد؟</p></div>
<div class="m2"><p>ز جلوه تو شود نقد اگر قیامت ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چراغ رهگذریم اوفتاده در ره باد</p></div>
<div class="m2"><p>که تا به سایه دستی کند حمایت ما؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ازان به دامن صحرا شکسته ایم قدم</p></div>
<div class="m2"><p>که عالمی شود آسوده از ملامت ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درین حدیقه گل، صائب از مروت نیست</p></div>
<div class="m2"><p>که غنچه ماند در جیب، دست رغبت ما</p></div></div>