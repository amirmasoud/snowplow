---
title: >-
    غزل شمارهٔ ۱۴۶
---
# غزل شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>سرنمی پیچم به سنگ بیستون از کار عشق</p></div>
<div class="m2"><p>جان شیرین بهر جوی شیر می باید مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از نوازش بیشتر می بالم از ریزش به خود</p></div>
<div class="m2"><p>جنبش گهواره بیش از شیر می باید مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست میدان دل پر وحشت من شهر را</p></div>
<div class="m2"><p>وادیی هموار چون نخجیر می باید مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پای دیوار مرا هر برگ کاهی تیشه ای است</p></div>
<div class="m2"><p>خضر تردستی پی تعمیر می باید مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی تلخ دایه نتواند مرا خاموش کرد</p></div>
<div class="m2"><p>طفل بدخویم، شکر در شیر می باید مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون هدف گردنکشی از خاکساری کرده ام</p></div>
<div class="m2"><p>سینه ای آماده صد تیر می باید مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست بی جا از شفق صائب اگر خون می خورم</p></div>
<div class="m2"><p>در نفس چون صبحدم تأثیر می باید مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد مسلسل بوی گل، زنجیر می باید مرا</p></div>
<div class="m2"><p>بند لنگرداری از تدبیر می باید مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از نسیم گل پریشان گردد اوراق حواس</p></div>
<div class="m2"><p>خلوتی چون غنچه تصویر می باید مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می کشد مجنون من ز آمد شد مردم ملال</p></div>
<div class="m2"><p>پاسبان ها از پلنگ و شیر می باید مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سر به صحرا داده چشم سیاه لیلیم</p></div>
<div class="m2"><p>چشم آهو حلقه زنجیر می باید مرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هیچ کاری بی کمان نگشاید از تیر خدنگ</p></div>
<div class="m2"><p>با جوانی، همتی از پیر می باید مرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هست از جوهر فزون صد حلقه پیچ و تاب من</p></div>
<div class="m2"><p>بستر و بالین ازان شمشیر می باید مرا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیست از غفلت اگر معماری دل می کنم</p></div>
<div class="m2"><p>گوشه ای زین عالم دلگیر می باید مرا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بی غبار خط مرا تسخیر کردن مشکل است</p></div>
<div class="m2"><p>بی قرارم، خاک دامنگیر می باید مرا</p></div></div>