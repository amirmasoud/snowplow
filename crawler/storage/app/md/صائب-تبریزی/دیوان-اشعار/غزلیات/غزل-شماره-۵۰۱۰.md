---
title: >-
    غزل شمارهٔ ۵۰۱۰
---
# غزل شمارهٔ ۵۰۱۰

<div class="b" id="bn1"><div class="m1"><p>نمانده زنده کس از دست و تیغ چالاکش</p></div>
<div class="m2"><p>هنوز می پرد از شوق،چشم فتراکش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>علم به خون مسیحا و خضر چرب کند</p></div>
<div class="m2"><p>چو از نیام کشد تیغ، حسن بیباکش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخی که هر دو جهان در فروغ اومحوست</p></div>
<div class="m2"><p>نظر چگونه کندبی نقاب ادراکش ؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خاک هر که نهال تو سایه اندازد</p></div>
<div class="m2"><p>زبان شکر برآید چو سبزه از خاکش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازان شراب مرا شیر گیر کن ساقی</p></div>
<div class="m2"><p>که همچو پنجه شیرست پنجه تاکش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درین بساط هرآن کس نفس به صدق کشد</p></div>
<div class="m2"><p>چو صبح،مهر شود طالع از دل چاکش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کسی که پاک نسازد دهن ز غیبت خلق</p></div>
<div class="m2"><p>همان کلید در دوزخ است مسواکش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر چه خون جهان ریخت غمزه اش صائب</p></div>
<div class="m2"><p>هنوز رغبت خون می چکد ز فتراکش</p></div></div>