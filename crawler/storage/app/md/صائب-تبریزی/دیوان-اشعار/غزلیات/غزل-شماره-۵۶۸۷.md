---
title: >-
    غزل شمارهٔ ۵۶۸۷
---
# غزل شمارهٔ ۵۶۸۷

<div class="b" id="bn1"><div class="m1"><p>نوبهارست بیا روی به میخانه کنیم</p></div>
<div class="m2"><p>مغز را از می گلرنگ پریخانه کنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوسه را گرد لب جام به دور اندازیم</p></div>
<div class="m2"><p>جگر سوخته خال لب پیمانه کنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شیوه خانه بدوشی به سبو بگذاریم</p></div>
<div class="m2"><p>پای محکم چو خم باده به میخانه کنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قصر گردون پی آسایش ما ساخته اند</p></div>
<div class="m2"><p>چند در زیر زمین مور صفت خانه کنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خانه پست نفس را به گلو می شکند</p></div>
<div class="m2"><p>به چه دل زیر فلک نعره مستانه کنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ادب شمع به بال و پر ما مقراض است</p></div>
<div class="m2"><p>ما نه آنیم که اندیشه ز پروانه کنیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بحر ته جرعه بر خاک ره افشانده ماست</p></div>
<div class="m2"><p>ما به این ظرف چه با شیشه و پیمانه کنیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تاک در معذرت مستی ما می گرید</p></div>
<div class="m2"><p>چه ضرورست که ما گریه مستانه کنیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب آن دست که پیمانه گران بود بر او</p></div>
<div class="m2"><p>ما چسان مزرعه سبحه صد دانه کنیم</p></div></div>