---
title: >-
    غزل شمارهٔ ۵۳۲۲
---
# غزل شمارهٔ ۵۳۲۲

<div class="b" id="bn1"><div class="m1"><p>بس که از نادیدنی دارد غبار آیینه ام</p></div>
<div class="m2"><p>می شمارد زنگ کلفت را بهار آیینه ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سواد نامه اعمال می بخشد خبر</p></div>
<div class="m2"><p>بس که از تردامنی گردیده تار آیینه ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی تامل سینه پیش می سازم سپر</p></div>
<div class="m2"><p>تا ز عکس خلق شد صورت نگار آیینه ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از هوا گیرم غبار کلفت وزنگ ملال</p></div>
<div class="m2"><p>تا شده است از سخت رویان سنگسار آیینه ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می زند از سخت جانی این زمان پهلو به سنگ</p></div>
<div class="m2"><p>داشت از نازکدلی گر شیشه بارآیینه ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جای خود می بایدش دیدن چو قارون زیرخاک</p></div>
<div class="m2"><p>آن که می خواهد که سازد بی غبار آیینه ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر ز ره زیر قبا پوشد چو جوهر دور نیست</p></div>
<div class="m2"><p>نیست امن از چشم زخم روزگار آیینه ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کرد بر لب تشنه دیدار بی خواهش سبیل</p></div>
<div class="m2"><p>اب خشکی داشت گر در جویبار آیینه ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیده اش حیران نقش پایدار دیگرست</p></div>
<div class="m2"><p>دل نمی بندد به هر نقش ونگار آیینه ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خجلت روی زمین صائب ز مردم می کشم</p></div>
<div class="m2"><p>کرد تا بی پرده گویی را شعار آیینه ام</p></div></div>