---
title: >-
    غزل شمارهٔ ۵۸۲۹
---
# غزل شمارهٔ ۵۸۲۹

<div class="b" id="bn1"><div class="m1"><p>تا چند پیر میکده را درد سر دهم؟</p></div>
<div class="m2"><p>رفتم ز می قرار به خون جگر دهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکسر ز تاج و تخت برآیند خسروان</p></div>
<div class="m2"><p>گر از حضور کنج قناعت خبر دهم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون بهله باز گشت مبادا به ساعدش</p></div>
<div class="m2"><p>روزی که دست خویش به دست دگر دهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل نیست وحشیی که شود رام با کسی</p></div>
<div class="m2"><p>دیوانه را به کوچه و بازار سر دهم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بحر سخاوتم که به هر قطره وقت جوش</p></div>
<div class="m2"><p>از موجه و حباب کلاه و کمر دهم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یوسف به سیم قلب فروش ز عقل نیست</p></div>
<div class="m2"><p>حاشا که فیض صبح به خواب سحر دهم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نقصان نمی کند دهد آن کس که زر به زر</p></div>
<div class="m2"><p>زان نقدجان خویش به آن سیمبر دهم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر آسمان کند نگه تلخ سوی من</p></div>
<div class="m2"><p>نه خرمنش به باد ز آه سحر دهم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مجنون من ز سنگ ملامت گرفته نیست</p></div>
<div class="m2"><p>چون کبک، داد خنده به کوه و کمر دهم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون نخل میوه دار درین بوستانسرا</p></div>
<div class="m2"><p>بارد اگر به فرق مرا سنگ، بر دهم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در حالت خمار ندارم اگر شعور</p></div>
<div class="m2"><p>هنگام مستی از ته دلها خبر دهم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرگ من است صحبت تردامنان دهر</p></div>
<div class="m2"><p>جان از برای سوختگان چون شرر دهم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بی حاصل است نخل امیدم چو بیدو سرو</p></div>
<div class="m2"><p>صائب مگر به تربیت عشق بردهم</p></div></div>