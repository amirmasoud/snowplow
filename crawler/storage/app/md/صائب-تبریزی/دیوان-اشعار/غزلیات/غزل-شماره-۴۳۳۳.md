---
title: >-
    غزل شمارهٔ ۴۳۳۳
---
# غزل شمارهٔ ۴۳۳۳

<div class="b" id="bn1"><div class="m1"><p>کی دست کرم خواجه ز امساک برآرد</p></div>
<div class="m2"><p>قارون چه خیال است سر از خاک برآرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از طول امل هر که دهد دام سرانجام</p></div>
<div class="m2"><p>چون موج ز دریا خس و خاشاک برآرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد روی ترا پرده عصمت خط مشکین</p></div>
<div class="m2"><p>خون مشک چو گردد نفس پاک برآرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون فاخته مرغی که ز کوته نظران نیست</p></div>
<div class="m2"><p>در بیضه سر از حلقه فتراک برآرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون چشم دهم آب ز رویی که حجابش</p></div>
<div class="m2"><p>از خلوت آیینه عرقناک برآرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از پنجه شیران نتوان طعمه ربودن</p></div>
<div class="m2"><p>دل چون کسی از دست تو بیباک برآرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در خون دل خود ز شفق غوطه زند صبح</p></div>
<div class="m2"><p>تا یک دو نفس از جگر چاک برآرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گلها همه تر دامن ومرغان همه بی شرم</p></div>
<div class="m2"><p>زین باغ کسی چون نظر پاک برآرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد سلسله جنبان ستم حسن ترا خط</p></div>
<div class="m2"><p>چون شعله که دست از خس و خاشاک برآرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیداست چه گل چیند ازین باغچه صائب</p></div>
<div class="m2"><p>دستی که در ایام خزان تاک برآرد</p></div></div>