---
title: >-
    غزل شمارهٔ ۳۴۶۵
---
# غزل شمارهٔ ۳۴۶۵

<div class="b" id="bn1"><div class="m1"><p>چهره زرد، مرا ساغر زر می بخشد</p></div>
<div class="m2"><p>سینه چاک، مرا فیض سحر می بخشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه آهونگهان روح فزایند همه</p></div>
<div class="m2"><p>چشم بیمار، مرا جان دگر می بخشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خرد شیشه دل از سنگ خطر می ترسد</p></div>
<div class="m2"><p>ورنه دیوانه به اطفال جگر می بخشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رزق صاحب نظران از تو بود خون، ورنه</p></div>
<div class="m2"><p>گل زر سرخ به شبنم به سپر می بخشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دارد از نقش قدم قافله ها در دنبال</p></div>
<div class="m2"><p>هرکه را شوق پر و بال سفر می بخشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شست از چشم جهان خواب دم تازه صبح</p></div>
<div class="m2"><p>نفس مردم شبخیز اثر می بخشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا یقین شد که بود نیش جهان پرده نوش</p></div>
<div class="m2"><p>سخن تلخ، مرا طعم شکر می بخشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرنه فرزند عزیزست به از جان عزیز</p></div>
<div class="m2"><p>چون پدر زندگی خود به پسر می بخشد؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غافل است از سر آزاد و دل فارغ من</p></div>
<div class="m2"><p>ساده لوحی که به من تاج و کمر می بخشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می توان برد پی از گرد به تعجیل سوار</p></div>
<div class="m2"><p>نفس از عمر سبکسیر خبر می بخشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پایه ابر ز دریاست ازان بالاتر</p></div>
<div class="m2"><p>که به هر خشک لبی آب گهر می بخشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تیر را شهپر پرواز بود صافی شست</p></div>
<div class="m2"><p>دل چو پاک است دعا زود اثر می بخشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خوابگاهش دهن شیر بود چون مجنون</p></div>
<div class="m2"><p>هرکه را عشق جوانمرد جگر می بخشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>توتیای نظر پاک بود دامن پاک</p></div>
<div class="m2"><p>نکهت مصر به یعقوب نظر می بخشد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کیسه بر چرخ مدوزید که این دون همت</p></div>
<div class="m2"><p>ماه را از دل خود زاد سفر می بخشد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کشت چون برق ز باران پیاپی سوزد</p></div>
<div class="m2"><p>می ز اندازه چو شد بیش، ضرر می بخشد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کرد دیوانه مرا ناله بلبل صائب</p></div>
<div class="m2"><p>ناله ای کز سر دردست اثر می بخشد</p></div></div>