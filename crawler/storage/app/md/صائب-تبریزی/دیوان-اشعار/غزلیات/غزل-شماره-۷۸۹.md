---
title: >-
    غزل شمارهٔ ۷۸۹
---
# غزل شمارهٔ ۷۸۹

<div class="b" id="bn1"><div class="m1"><p>رحمت گرفته روی ز گرد گناه ما</p></div>
<div class="m2"><p>آیینه تیره روز ز روی سیاه ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر قطره ای که در صدف ابر رحمت است</p></div>
<div class="m2"><p>چون مهره گل است ز گرد گناه ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر جسم آنقدر که فزودیم همچو شمع</p></div>
<div class="m2"><p>شد مایه زیادتی اشک و آه ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما چون حباب، تشنه محویم ازین محیط</p></div>
<div class="m2"><p>سهل است موج اگر برباید کلاه ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما در رکاب جذبه توفیق می رویم</p></div>
<div class="m2"><p>رطل گران چگونه شود سنگ راه ما؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون بحر در کشاکش موج است مضطرب</p></div>
<div class="m2"><p>روی زمین ز ریگ روان گناه ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما را غلط به لشکر اصحاب فیل کرد</p></div>
<div class="m2"><p>از دور دید کعبه چو کوه گناه ما!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>داریم چشم آن که شود روز بازخواست</p></div>
<div class="m2"><p>سر پیش پا فکندن ما، عذر خواه ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب که را گمان که سیه مست غفلتی</p></div>
<div class="m2"><p>در شاهراه توبه شود خضر راه ما؟</p></div></div>