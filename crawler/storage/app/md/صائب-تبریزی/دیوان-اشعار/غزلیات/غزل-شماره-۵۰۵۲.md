---
title: >-
    غزل شمارهٔ ۵۰۵۲
---
# غزل شمارهٔ ۵۰۵۲

<div class="b" id="bn1"><div class="m1"><p>گر این چنین چکد می گلرنگ ازلبش</p></div>
<div class="m2"><p>جام پراز شراب شود طوق غبغبش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میگون لبی که سوخت مرا درخمار می</p></div>
<div class="m2"><p>پیمانه برنگشته تهی هرگز ازلبش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در چشم خاک راه نشینان انتظار</p></div>
<div class="m2"><p>کار هلال عید کند نعل مرکبش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون سرو،قمریان همه گردن کشیده اند</p></div>
<div class="m2"><p>در آرزوی طوق گلوسوز غبغبش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سینه دل به زلف تو گردد طفل شوخ</p></div>
<div class="m2"><p>در کوچه است اگر چه بود جا به مکتبش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرچشمه ای که ریشه به دریا رسانده است</p></div>
<div class="m2"><p>از جوش تشنگان نشود تنگ مشربش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راه سخن به سایل مبرم نمی دهند</p></div>
<div class="m2"><p>رحم است برکسی که برآرند مطلبش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب به خون دل نزند کاسه،چون کند ؟</p></div>
<div class="m2"><p>هرکس که نیست دست به جام لبالبش</p></div></div>