---
title: >-
    غزل شمارهٔ ۵۱۶۰
---
# غزل شمارهٔ ۵۱۶۰

<div class="b" id="bn1"><div class="m1"><p>غافلی از دردمندی ای دل بیمار حیف</p></div>
<div class="m2"><p>پیش عیسی درد خود را می کنی اظهار حیف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیستی در فکر آزادی ازین جسم گران</p></div>
<div class="m2"><p>دامن خود برنیازی زین ته دیوار حیف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر خموشی می دهی ترجیح حرف پوچ را</p></div>
<div class="m2"><p>می شوی قانع به کف از بحر گوهربار حیف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد سفید از انتظارت دیده عبرت پذیر</p></div>
<div class="m2"><p>برنیاوردی ازین روزن سری یک بار حیف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دولت دنیا سبک پرواز چون بال هماست</p></div>
<div class="m2"><p>تو وفاداری طمع زین ناکس غدار حیف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساده لوحان از خیال خود گریزانند و تو</p></div>
<div class="m2"><p>می گشایی هرزمان آیینه در بازار حیف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پر برآوردند از درد طلب موران و تو</p></div>
<div class="m2"><p>پای ننهادی برون چون نقطه از پرگار حیف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در بیابانی که برق و باد ازو بیرون نرفت</p></div>
<div class="m2"><p>از علایق داده ای دامن به دست خار حیف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>استخوانت توتیا گردید از خواب گران</p></div>
<div class="m2"><p>تر نشد ز اشک ندامت دیده ات یک بار حیف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آمدی انگاره و انگاره رفتی از جهان</p></div>
<div class="m2"><p>با دو صد سوهان نکردی خویش را هموار حیف</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مغز را وامی کنند از سر سبکروحان و تو</p></div>
<div class="m2"><p>می زنی چون بیغمان گل بر سر دستار حیف</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مزدها بردند صائب کارپردازان و تو</p></div>
<div class="m2"><p>از تن آسانی نکردی اختیار کار حیف</p></div></div>