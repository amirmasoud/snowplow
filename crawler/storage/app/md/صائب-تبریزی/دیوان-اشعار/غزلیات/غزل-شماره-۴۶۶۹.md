---
title: >-
    غزل شمارهٔ ۴۶۶۹
---
# غزل شمارهٔ ۴۶۶۹

<div class="b" id="bn1"><div class="m1"><p>دوسه روزی است صفای رخ گلپوش بهار</p></div>
<div class="m2"><p>دیده ای آب ده از صبح بناگوش بهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانه سوخته از ابر نمی گردد سبز</p></div>
<div class="m2"><p>چه کند بادل افسرده ما جوش بهار؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دامن پاک حصاری است نکورویان را</p></div>
<div class="m2"><p>سروراسرکشیی نیست از آغوش بهار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>موی ژولیده چو دود از سر من باز شود</p></div>
<div class="m2"><p>گرچنین جوش زند مغز من از جوش بهار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون زند بلبل بی طالع ما بر آهنگ</p></div>
<div class="m2"><p>صدف گوهر سیماب شود گوش بهار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در حبابی چه پر و بال گشاید طوفان ؟</p></div>
<div class="m2"><p>ظرف بلبل چه کند بامی سرجوش بهار؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چمن از جوش گل و لاله گرانبار شده است</p></div>
<div class="m2"><p>جلوه ای کن که سبکبار شود دوش بهار</p></div></div>