---
title: >-
    غزل شمارهٔ ۹۹
---
# غزل شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>نیست در دوران من میخانه حاجت خلق را</p></div>
<div class="m2"><p>بس بود پیمانه من تا قیامت خلق را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کلک گوهربار من داد سخاوت می دهد</p></div>
<div class="m2"><p>باش گو در آستین دست سخاوت خلق را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می کند ایجاد، گفتار بلند اقبال من</p></div>
<div class="m2"><p>گر نباشد رحم و انصاف و مروت خلق را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر حریف چرخ کم فرصت نگردم، می کنم</p></div>
<div class="m2"><p>مهربان از راه گفت و گو به فرصت خلق را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون زمین هر چند زیردست و پا افتاده ام</p></div>
<div class="m2"><p>آسمانم از بلندی های فطرت خلق را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سوختم چون شمع تا روشن شد از من عالمی</p></div>
<div class="m2"><p>سرمه من کرد از اهل بصیرت خلق را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزل و هجو و پوچ نتوان یافت در دیوان من</p></div>
<div class="m2"><p>می رساند فال نیک من به دولت خلق را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون هما با هر که پیوستم سعادتمند شد</p></div>
<div class="m2"><p>سایه من کرد از اهل سعادت خلق را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عشق را آتش فروزم، حسن را روشنگرم</p></div>
<div class="m2"><p>می نمایم گرم در مهر و محبت خلق را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مستی آرد باده های تلخ و کلک من کند</p></div>
<div class="m2"><p>هوشیار از باده تلخ نصیحت خلق را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حرف حق از دشمنان خود نمی دارم دریغ</p></div>
<div class="m2"><p>می کنم واقف ز اسرار حقیقت خلق را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همچو صیقل صائب از دیوان من هر مصرعی</p></div>
<div class="m2"><p>پاک سازد سینه از زنگ قساوت خلق را</p></div></div>