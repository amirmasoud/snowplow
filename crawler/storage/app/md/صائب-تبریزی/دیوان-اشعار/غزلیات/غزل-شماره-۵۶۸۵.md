---
title: >-
    غزل شمارهٔ ۵۶۸۵
---
# غزل شمارهٔ ۵۶۸۵

<div class="b" id="bn1"><div class="m1"><p>پیش دل شرح زر و گوهر دنیا چه کنیم</p></div>
<div class="m2"><p>عرض خر مهره دجال به عیسی چه کنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از گرانجانی ما روی زمین نیلی شد</p></div>
<div class="m2"><p>آرزوی سفر عالم بالا چه کنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مهر آن نیست که از ذره فراموش کند</p></div>
<div class="m2"><p>طرف وعده کریم است تقاضا چه کنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می رود قافله عمر به سرعت امروز</p></div>
<div class="m2"><p>ما در اندیشه آنیم که فردا چه کنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جدل شبنم و خورشید بود مشت و درفش</p></div>
<div class="m2"><p>سپر تیر قضا غیر مدارا چه کنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کوه برفی است ز دستار تعین عالم</p></div>
<div class="m2"><p>ما به کنجی نگریزیم ز سرما، چه کنیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سنگ را موم کند باده گلگون صائب</p></div>
<div class="m2"><p>ما به این شیشه دلی روی به صهبا چه کنیم</p></div></div>