---
title: >-
    غزل شمارهٔ ۸۳
---
# غزل شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>غوطه دادم در دل الماس داغ خویش را</p></div>
<div class="m2"><p>روشن از آب گهر کردم چراغ خویش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد چو داغ لاله خاکستر نفس در سینه ام</p></div>
<div class="m2"><p>تا ز خون چون لاله پر کردم ایاغ خویش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون شوم با خار و خس محشور در یک پیرهن؟</p></div>
<div class="m2"><p>من که می دزدم ز بوی گل دماغ خویش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی خودی را گردش چشم تو عالمگیر ساخت</p></div>
<div class="m2"><p>از که گیرم، حیرتی دارم، سراغ خویش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می شود شور قیامت مرهم کافوریم</p></div>
<div class="m2"><p>من که پروردم به چشم شور، داغ خویش را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشرت ده روزه گل قابل تقسیم نیست</p></div>
<div class="m2"><p>وقف بلبل می کنم دربسته، باغ خویش را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیش ازین صائب نمی آید ز من اخفای عشق</p></div>
<div class="m2"><p>چند دارم در ته دامن چراغ خویش را؟</p></div></div>