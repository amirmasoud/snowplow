---
title: >-
    غزل شمارهٔ ۶۳۹۷
---
# غزل شمارهٔ ۶۳۹۷

<div class="b" id="bn1"><div class="m1"><p>دل می برد ز قند مکرر کلام من</p></div>
<div class="m2"><p>نی می کند به ناخن شکر کلام من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در قطع ره ز جادو بود خضر بی نیاز</p></div>
<div class="m2"><p>از راستی غنی است ز مسطر کلام من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در گفتگوی نازک من نیست کوتهی</p></div>
<div class="m2"><p>از مد مانوی است رساتر کلام من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در یک نفس ز مصر به کنعان کند نزول</p></div>
<div class="m2"><p>چون بوی یوسف است سبک پر کلام من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک روز(ه) چون کبوتر بغداد می رود</p></div>
<div class="m2"><p>از باختر به کشور خاور کلام من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از گوش پیشتر به دل مستمع رسد</p></div>
<div class="m2"><p>از دلپذیریی که بود در کلام من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از خلق در حجاب سیاهی نهفته نیست</p></div>
<div class="m2"><p>از آب زندگی است روانتر کلام من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صور قیامت است مرا کلک خوش صریر</p></div>
<div class="m2"><p>دارد نمک ز شورش محشر کلام من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کاغذ حریف آتش سوزان نمی شود</p></div>
<div class="m2"><p>دفتر کند ز بال سمندر کلام من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر سنگ می زنند ز دلهای همچو سنگ</p></div>
<div class="m2"><p>هر چند قیمتی است چو گوهر کلام من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از مدح و هجو و هزل و طمع شسته ام ورق</p></div>
<div class="m2"><p>پند و نصیحت است سراسر کلام من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نسبت به فکرهای قدیدش مکن که هست</p></div>
<div class="m2"><p>از تازگی چکیده کوثر کلام من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اندیشه اش ز ناخن یأجوج دخل نیست</p></div>
<div class="m2"><p>باشد متین چو سد سکندر کلام من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون مار پا به راه نهد کشته می شود</p></div>
<div class="m2"><p>انگشت اعتراض منه بر کلام من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گردید خشک همچو صدف پوست بر تنم</p></div>
<div class="m2"><p>تا گشت آبدار چو گوهر کلام من</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یک نقطه خرده بین نتواند به سهو یافت</p></div>
<div class="m2"><p>هر چند پیچ و تاب زند در کلام من</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای وامصیبتاه که شد خرج مردگان</p></div>
<div class="m2"><p>چون حافظ مزار سراسر کلام من</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صائب چو آفتاب ز دل تا نفس کشید</p></div>
<div class="m2"><p>آفاق را گرفت سراسر کلام من</p></div></div>