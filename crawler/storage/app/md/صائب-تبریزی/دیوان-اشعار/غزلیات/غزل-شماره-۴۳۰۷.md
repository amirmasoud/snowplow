---
title: >-
    غزل شمارهٔ ۴۳۰۷
---
# غزل شمارهٔ ۴۳۰۷

<div class="b" id="bn1"><div class="m1"><p>شوخی که عرض حسن به اغیار می دهد</p></div>
<div class="m2"><p>آب خضر به صورت دیوار می دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زودا که رخ به خون جگر لاله گون کند</p></div>
<div class="m2"><p>آن ساده دل که تربیت خار می دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روشندلان ز خصم ندارند جان دریغ</p></div>
<div class="m2"><p>آیینه آب سبزه زنگار می دهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از پاک طینتی است که ابر گرفته رو</p></div>
<div class="m2"><p>رزق صدف ز گوهر شهوار می دهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی حاصلی است حاصل دل تا بود درست</p></div>
<div class="m2"><p>این شاخ چون شکسته شود بار می دهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>موسی ز اشتیاق تجلی هلاک شد</p></div>
<div class="m2"><p>ایزد به طور وعده دیدار می دهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صد عقده باز می کند از کار خویشتن</p></div>
<div class="m2"><p>صائب کسی که سبحه به زنارمی دهد</p></div></div>