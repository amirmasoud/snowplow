---
title: >-
    غزل شمارهٔ ۵۶۰۰
---
# غزل شمارهٔ ۵۶۰۰

<div class="b" id="bn1"><div class="m1"><p>از نهانخانه عصمت به تماشا بخرام</p></div>
<div class="m2"><p>آهوان چشم به راهند به صحرا بخرام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای که از گوهر مقصود نشان می طلبی</p></div>
<div class="m2"><p>بر بساط گهر آبله پا بخرام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای صبا آتش غیرت به زبان آمده است</p></div>
<div class="m2"><p>به ادب در خم آن زلف چلیپا بخرام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوهکن سخت به سرپنجه خود می نازد</p></div>
<div class="m2"><p>ناخنی تیز کن ای آه و به خارا بخرام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قیمتی گوهر ساحل صدف دست تهی است</p></div>
<div class="m2"><p>گر گهر می طلبی در دل دریا بخرام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند صائب ز پی درد به هر جا بدوی؟</p></div>
<div class="m2"><p>قدمی چند به دنبال مداوا بخرام</p></div></div>