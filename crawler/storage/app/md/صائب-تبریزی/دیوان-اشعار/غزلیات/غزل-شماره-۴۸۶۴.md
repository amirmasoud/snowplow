---
title: >-
    غزل شمارهٔ ۴۸۶۴
---
# غزل شمارهٔ ۴۸۶۴

<div class="b" id="bn1"><div class="m1"><p>چشم و گوش و لب ببند، از شور و شر آسوده باش</p></div>
<div class="m2"><p>خویش را گردآوری کن از سپر آسوده باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازکمان توست هر تیری که در دل می خلد</p></div>
<div class="m2"><p>راست شو، از تیر طعن کج نظر آسوده باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چه صورت می پذیرد سایه کردار توست</p></div>
<div class="m2"><p>لب بگز از حرف تلخ،از نیشتر آسوده باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا ثمر دارد رگ خامی، ز دار آویخته است</p></div>
<div class="m2"><p>پخته شو، ازدار و گیر این شجر آسوده باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از جهانگردی غبار خاطر افزون می شود</p></div>
<div class="m2"><p>ازتو بیرون نیست منزل، از سفر آسوده باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پرتو خورشید هیهات است ماند برزمین</p></div>
<div class="m2"><p>از شبیخون فنا ای بیجگر آسوده باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلوت آیینه روشن از فروغ حیرت است</p></div>
<div class="m2"><p>ز اختیار خوب و زشت و خیر و شر آسوده باش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد زمین از بردباری مظهر حسن بهار</p></div>
<div class="m2"><p>گرچه خاک ره کنندت پی سپر آسوده باش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون صدف صائب ز دریا گوشه ای کن اختیار</p></div>
<div class="m2"><p>ز انقلاب موج آب چون گهر آسوده باش</p></div></div>