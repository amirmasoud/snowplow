---
title: >-
    غزل شمارهٔ ۴۹۸۲
---
# غزل شمارهٔ ۴۹۸۲

<div class="b" id="bn1"><div class="m1"><p>داغدار از عرق شرم شود نسرینش</p></div>
<div class="m2"><p>آب گردد ز اشارت بدن سیمینش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوی مشک ازنفس سوخته اش می آید</p></div>
<div class="m2"><p>در دل هرکه کند ریشه خط مشکینش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این چه لطف است که چون سرو شود مینارنگ</p></div>
<div class="m2"><p>از بغل گیری آیینه تن سیمینش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آب چون آینه رفتار فراموش کند</p></div>
<div class="m2"><p>سایه برآب روان گر فکند تمکینش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نتوان بافت بغیر از لب و دندان نگار</p></div>
<div class="m2"><p>ماه عیدی که هم آغوش بودپروینش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه چنان چشم چو بادام تو تلخ افتاده است</p></div>
<div class="m2"><p>که شکر خواب به افسانه کند شیرینش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سینه اش کان بدخشان شود از باده لعل</p></div>
<div class="m2"><p>هرکه از دست بود همچو سبو بالینش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آتشی هست نهان در دل صائب که مدام</p></div>
<div class="m2"><p>می چکد خون چو کباب ازنفس رنگینش</p></div></div>