---
title: >-
    غزل شمارهٔ ۴۵۵۱
---
# غزل شمارهٔ ۴۵۵۱

<div class="b" id="bn1"><div class="m1"><p>چندان که خواب صبح بود بر جوان لذیذ</p></div>
<div class="m2"><p>بیداری شب است به صاحبدلان لذیذ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیکان آبدار تو چون میوه بهشت</p></div>
<div class="m2"><p>گردیده است زخم مرا در دهان لذیذ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هست از طعام لذت اطعام بیشتر</p></div>
<div class="m2"><p>بر میزبان خورش شود از میهمان لذیذ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از باده جنون سر هر کس که گرم شد</p></div>
<div class="m2"><p>سنگ ملامت است چو رطل گران لذیذ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن تیغ آبدار در آغوش زخم من</p></div>
<div class="m2"><p>در کام تشنه است چو آب روان لذیذ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندیشه از عتاب ندارم که می شود</p></div>
<div class="m2"><p>دشنام تلخ ازان لب شکر فشان لذیذ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در پای نخل میوه دهد لذت دگر</p></div>
<div class="m2"><p>دشنام روبرو بود از دلستان لذیذ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ماهی ز آب بحر ندارد شکایتی</p></div>
<div class="m2"><p>باشد شراب تلخ به میخوارگان لذیذ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این چاشنی که دست ترا هست می شود</p></div>
<div class="m2"><p>چون نیشکر خدنگ تو در کام جان لذیذ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر کس به کیمیای قناعت رسیده است</p></div>
<div class="m2"><p>در کام او بود چو هما استخوان لذیذ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در کام قانع آب حیات است نان خشک</p></div>
<div class="m2"><p>بی نان خورش به منعم اگر نیست نان لذیذ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن مست ناز سوخت دلم را ز انتظار</p></div>
<div class="m2"><p>غافل که این کباب بود خونچکان لذیذ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صائب ز فیض چاشنی عشق گشته است</p></div>
<div class="m2"><p>اشعار آبدار تو در هر دهان لذیذ</p></div></div>