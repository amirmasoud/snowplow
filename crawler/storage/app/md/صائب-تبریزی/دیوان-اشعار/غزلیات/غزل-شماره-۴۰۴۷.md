---
title: >-
    غزل شمارهٔ ۴۰۴۷
---
# غزل شمارهٔ ۴۰۴۷

<div class="b" id="bn1"><div class="m1"><p>دیوانه را به دامن صحرا که می برد</p></div>
<div class="m2"><p>طفل یتیم را به تماشا که می برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکتوب خشک سلسله پای قاصدست</p></div>
<div class="m2"><p>موج سراب را سوی دریا که می برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راه نسیم نیست در آن زلف تابدار</p></div>
<div class="m2"><p>از بیدلان پیام به دلها که می برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جایی که زلف حلقه بیرون در بود</p></div>
<div class="m2"><p>نام دل شکسته ما را که می برد</p></div></div>