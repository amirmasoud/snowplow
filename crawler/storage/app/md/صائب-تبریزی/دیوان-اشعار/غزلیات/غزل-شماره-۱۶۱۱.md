---
title: >-
    غزل شمارهٔ ۱۶۱۱
---
# غزل شمارهٔ ۱۶۱۱

<div class="b" id="bn1"><div class="m1"><p>در بیابان جنون سلسله پردازی نیست</p></div>
<div class="m2"><p>روزگاری است درین دایره آوازی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه همین کوچه و بازار ز مجنون خالی است</p></div>
<div class="m2"><p>در بیابان جنون نیز نظربازی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وحشت آباد بود در نظر من شهری</p></div>
<div class="m2"><p>که به هر کوچه او خانه براندازی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برنیاید نفس از طوطی شیرین گفتار</p></div>
<div class="m2"><p>در حریمی که رخ آینه پردازی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به چراغ مه و خورشید نگردد روشن</p></div>
<div class="m2"><p>هر حریمی که در او شعله آوازی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می توان یافت ز پیچیدگی بال و پرم</p></div>
<div class="m2"><p>که به گیرایی مژگان تو شهبازی نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست ممکن که تراود سخن از من صائب</p></div>
<div class="m2"><p>در حریمی که در او چشم سخن سازی نیست</p></div></div>