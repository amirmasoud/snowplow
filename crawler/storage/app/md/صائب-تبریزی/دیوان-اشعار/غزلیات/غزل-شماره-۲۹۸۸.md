---
title: >-
    غزل شمارهٔ ۲۹۸۸
---
# غزل شمارهٔ ۲۹۸۸

<div class="b" id="bn1"><div class="m1"><p>فسون صبر در دل‌های پرخون در نمی‌گیرد</p></div>
<div class="m2"><p>چو دریا بیکران افتد به خود لنگر نمی‌گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیاهی بر سر داغ من آتش زیر پا دارد</p></div>
<div class="m2"><p>ز شوخی اخگر من گرد خاکستر نمی‌گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غرض از زندگی نام است، اگر آب خضر نبود</p></div>
<div class="m2"><p>کسی آیینه را از دست اسکندر نمی‌گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو رنگی نیست هر جا پای وحدت در میان آمد</p></div>
<div class="m2"><p>درین دریا خزف خود را کم از گوهر نمی‌گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگردد لخت دل از گریه مانع خار مژگان را</p></div>
<div class="m2"><p>گره در رشته ما راه بر گوهر نمی‌گیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز اقبال سکندر خضر بر دل داغ‌ها دارد</p></div>
<div class="m2"><p>که آب زندگانی جای چشم تر نمی‌گیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لبی کز حسرت آب خضر خون می‌خورد صائب</p></div>
<div class="m2"><p>چرا یک بوسه سیراب از ساغر نمی‌گیرد؟</p></div></div>