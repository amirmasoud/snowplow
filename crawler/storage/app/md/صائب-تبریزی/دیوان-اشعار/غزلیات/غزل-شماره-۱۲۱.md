---
title: >-
    غزل شمارهٔ ۱۲۱
---
# غزل شمارهٔ ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>خلق خوش در نوبهار عافیت دارد مرا</p></div>
<div class="m2"><p>خاکساری در حصار عافیت دارد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چه بدمستی ز من سرزد که دور روزگار</p></div>
<div class="m2"><p>در کشاکش از خمار عافیت دارد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آسمان گر از خزان درد پامالم کند</p></div>
<div class="m2"><p>به که سرسبز از بهار عافیت دارد مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا سبو بر دوش دارم از خمار آسوده ام</p></div>
<div class="m2"><p>میکشی در زیر بار عافیت دارد مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صبح محشر شور در عالم فکند و همچنان</p></div>
<div class="m2"><p>آسمان امیدوار عافیت دارد مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکر زنجیر جنون بر گردن من واجب است</p></div>
<div class="m2"><p>مدتی شد در حصار عافیت دارد مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهل دردی نیست صائب زین همه دردی کشان</p></div>
<div class="m2"><p>تا به جامی شرمسار عافیت دارد مرا</p></div></div>