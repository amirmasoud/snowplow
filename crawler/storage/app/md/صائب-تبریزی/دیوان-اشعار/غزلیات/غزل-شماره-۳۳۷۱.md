---
title: >-
    غزل شمارهٔ ۳۳۷۱
---
# غزل شمارهٔ ۳۳۷۱

<div class="b" id="bn1"><div class="m1"><p>اوست عاقل که درین غمکده صهبا نخورد</p></div>
<div class="m2"><p>روی دست از قدح و پای ز مینا نخورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاهد بیخبریهاست سکندر خوردن</p></div>
<div class="m2"><p>هر که فهمیده نهد پا به زمین، پا نخورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دو رویان نتوان داشت طمع یکرنگی</p></div>
<div class="m2"><p>بلبل آن به که فریب گل رعنا نخورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نکند زحمت ناآمده را استقبال</p></div>
<div class="m2"><p>هر که امروز غم روزی فردا نخورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همت آن است که موقوف نباشد به طلب</p></div>
<div class="m2"><p>رگ ارباب کرم نیش تقاضا نخورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ابر نیسان کند از آب گهر سیرابش</p></div>
<div class="m2"><p>هر که صائب چو صدف آب ز دریا نخورد</p></div></div>