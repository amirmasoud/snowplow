---
title: >-
    غزل شمارهٔ ۲۵۶۱
---
# غزل شمارهٔ ۲۵۶۱

<div class="b" id="bn1"><div class="m1"><p>دیده‌ها را چهرهٔ گلرنگ گلشن می‌کند</p></div>
<div class="m2"><p>روی آتشناک، شمع کشته روشن می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌حجابی بر فروغ حسن باد صرصرست</p></div>
<div class="m2"><p>شرم، خوبی را چراغ زیر دامن می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خانه چشم زلیخا شد سفید از انتظار</p></div>
<div class="m2"><p>بوی پیراهن به کنعان خانه روشن می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌شوند از گرمخونی آشنا، بیگانگان</p></div>
<div class="m2"><p>بر چراغ لاله شبنم کار روغن می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دردمندان را حصار آهنی در کار نیست</p></div>
<div class="m2"><p>داغ چون پیوست با هم، کار جوشن می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غافل است از پای خواب‌آلود من در زیر سنگ</p></div>
<div class="m2"><p>آن که از کویش مرا تکلیف رفتن می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرکشی و ناز را بر طاق نسیان می‌نهی</p></div>
<div class="m2"><p>گر خبر یابی که تنهایی چه با من می‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می‌دهد میدان به سیل تندرو از سادگی</p></div>
<div class="m2"><p>کوته‌اندیشی که همواری به دشمن می‌کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیدهٔ باریک‌بین را می‌شود مویی حجاب</p></div>
<div class="m2"><p>رشته عالم را سیه در چشم سوزن می‌کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قامت خم می‌شود مانع ز رفتن عمر را</p></div>
<div class="m2"><p>سنگ اگر لنگر در آغوش فلاخن می‌کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می‌کند با اهل دل صائب سپهر نیلگون</p></div>
<div class="m2"><p>آنچه با آیینه تاریک، گلخن می‌کند</p></div></div>