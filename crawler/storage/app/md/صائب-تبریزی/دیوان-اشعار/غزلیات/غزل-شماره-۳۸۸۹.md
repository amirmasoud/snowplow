---
title: >-
    غزل شمارهٔ ۳۸۸۹
---
# غزل شمارهٔ ۳۸۸۹

<div class="b" id="bn1"><div class="m1"><p>به زیر چرخ مقوس که جاودان ماند</p></div>
<div class="m2"><p>کدام تیر شنیدی که در کمان ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نصیب من ز جوانی دریغ وافسوس است</p></div>
<div class="m2"><p>ز گلستان خس وخاری به باغبان ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهشت بوته خاری است با کهنسالی</p></div>
<div class="m2"><p>خوش است عالم اگر آدمی جوان ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز زنگ آینه اش صیقلی نمی گردد</p></div>
<div class="m2"><p>چو خضر هر که درین نشأه جاودان ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنین که می پرد از حرص خاکیان را چشم</p></div>
<div class="m2"><p>عجب اگر پرکاهی به کهکشان ماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود ز قافله عشق چرخ آبله پا</p></div>
<div class="m2"><p>پیاده ای که به دنبال کاروان ماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخن رسد به خریدار چون غریب شود</p></div>
<div class="m2"><p>که ماه مصر محال است در دکان ماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو می توان به خرابی زگنج شد معمور</p></div>
<div class="m2"><p>کسی برای چه در قید خانمان ماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مپوش چشم ز روی نکو که چون شبنم</p></div>
<div class="m2"><p>به ما چراندن چشمی ز گلستان ماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنان مکن که سرحرف شکوه باز کند</p></div>
<div class="m2"><p>زبان من که به شمشیر خونچکان ماند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکی هزار شد از عیبجو بصیرت من</p></div>
<div class="m2"><p>ز دزد دیده بازی به پاسبان ماند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مصوری که شبیه ترا کند تصویر</p></div>
<div class="m2"><p>ز خامه اش سرانگشت در دهان ماند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز تنگ گیری چرخ خسیس نزدیک است</p></div>
<div class="m2"><p>که در گلوی هما صائب استخوان ماند</p></div></div>