---
title: >-
    غزل شمارهٔ ۳۳۵۷
---
# غزل شمارهٔ ۳۳۵۷

<div class="b" id="bn1"><div class="m1"><p>دامن دولت دنیا نتوان سخت گرفت</p></div>
<div class="m2"><p>سایه بال هما را به قفس نتوان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اینقدر کز تو دلی چند بود شاد، بس است</p></div>
<div class="m2"><p>زندگانی به مراد همه کس نتوان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شرر کذب به یک چشم زدن می میرد</p></div>
<div class="m2"><p>تکیه بر دوستی اهل هوس نتوان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نعمتی نیست که چشمی نبود در پی آن</p></div>
<div class="m2"><p>ترک وصل شکر از بهر مگس نتوان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صائب از طول امل دست هوس کوته دار</p></div>
<div class="m2"><p>که در این دام به جز صید مگس نتوان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیش ازین پیروی حرص و هوس نتوان کرد</p></div>
<div class="m2"><p>همعنانی به سگ هرزه مرس نتوان کرد</p></div></div>