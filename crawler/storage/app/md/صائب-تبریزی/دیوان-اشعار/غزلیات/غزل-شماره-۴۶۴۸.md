---
title: >-
    غزل شمارهٔ ۴۶۴۸
---
# غزل شمارهٔ ۴۶۴۸

<div class="b" id="bn1"><div class="m1"><p>شراب زندگی درخاکساریهاست بی غش تر</p></div>
<div class="m2"><p>بود نخل برومند از زمین نرم سرکش تر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خون آرزویی می تپدهر ذره ازخاکم</p></div>
<div class="m2"><p>ندارد گرد بادی دشت عشق ازمن مشوش تر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهانسوزی کزاومن منصب پروانگی دارم</p></div>
<div class="m2"><p>گل رخساراو درعالم آب است آتش تر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهشتی کزخیالش خواب زاهدتلخ می گردد</p></div>
<div class="m2"><p>نداردگوشه ای از گوشه چشم تو دلکش تر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نلغزد چون زبان طوطی من ،کان شکرلب را</p></div>
<div class="m2"><p>بررویی است از آیینه ادراک بی غش تر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فلک درکاربی رویان کند هرزینتی دارد</p></div>
<div class="m2"><p>که پشت آیینه را ازروی می باشدمنقش تر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در ایام خزان صاف است آب جویها صائب</p></div>
<div class="m2"><p>زلال زندگی درموسم پیری است بی غش تر</p></div></div>