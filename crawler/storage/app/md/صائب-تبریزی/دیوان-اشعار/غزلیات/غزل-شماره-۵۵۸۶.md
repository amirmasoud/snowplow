---
title: >-
    غزل شمارهٔ ۵۵۸۶
---
# غزل شمارهٔ ۵۵۸۶

<div class="b" id="bn1"><div class="m1"><p>تمتع با کمال قرب از آن رعنا نمی‌بینم</p></div>
<div class="m2"><p>که زیر پا نبیند یار و من بالا نمی‌بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر از دور گرد محمل لیلی نمایان شد؟</p></div>
<div class="m2"><p>که از مجنون اثر در دامن صحرا نمی‌بینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کمینگاه نگاه حسرت آلودی است هر مویم</p></div>
<div class="m2"><p>اگر در چهره محجوب او رسوا نمی‌بینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرامش وعده من گر نه مکری در نظر دارد</p></div>
<div class="m2"><p>چرا امروز ذوق از وعده فردا نمی‌بینم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به راهم خار ریزد خصم کوته‌بین، نمی‌داند</p></div>
<div class="m2"><p>که من چون شعله بی‌باک پیش پا نمی‌بینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه حاصل زین که چون پرگار پای آهنین دارم؟</p></div>
<div class="m2"><p>چو من راه نجات از گردنش بی‌جا نمی‌بینم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به درد و داغ غربت زان نهادم دل که چون گوهر</p></div>
<div class="m2"><p>گشاد این گره از ناخن دریا نمی‌بینم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من و دامان شب، کامروز در آفاق دامانی</p></div>
<div class="m2"><p>که داد من دهد، جز دامن شب‌ها نمی‌بینم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نگاه عجز تیغ بد گهر را تیزتر سازد</p></div>
<div class="m2"><p>فلک گر تیغ بارد بر سرم بالا نمی‌بینم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ربوده است آنچنان فکر و خیال او مرا صائب</p></div>
<div class="m2"><p>که پیش پا به چندین دیده بینا نمی‌بینم</p></div></div>