---
title: >-
    غزل شمارهٔ ۲۵۹۷
---
# غزل شمارهٔ ۲۵۹۷

<div class="b" id="bn1"><div class="m1"><p>هرکجا خوبان چراغ دلبری بر می‌کنند</p></div>
<div class="m2"><p>شمع را پروانه، آتش را سمندر می‌کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق را با ناتوانان التفات دیگرست</p></div>
<div class="m2"><p>فربه‌انصافان شکار صید لاغر می‌کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آه ازین خورشیدرخساران که از تردامنی</p></div>
<div class="m2"><p>از گریبان دگر هر صبح سر برمی‌کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غافلان را عمر در امروز و فردا می‌رود</p></div>
<div class="m2"><p>عارفان امروز را فردای محشر می‌کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نامه‌پردازان در ایام فراق دوستان</p></div>
<div class="m2"><p>با کدامین دست و دل یارب قلم سر می‌کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در زمین پاک خرسندی قناعت‌پیشگان</p></div>
<div class="m2"><p>خاک می‌لیسند و استغنا به شکر می‌کنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هوشیاران را غم ایام می‌سازد زبون</p></div>
<div class="m2"><p>دردنوشان زود غم را خاک بر سر می‌کنند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پخته شو تا روز محشر ایمن از دوزخ شوی</p></div>
<div class="m2"><p>ورنه عود خام را در کار مجمر می‌کنند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صحبت دریادلان صائب بهار رحمت است</p></div>
<div class="m2"><p>موم را در یک نفس این قوم عنبر می‌کنند</p></div></div>