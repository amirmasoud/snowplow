---
title: >-
    غزل شمارهٔ ۳۵۱۶
---
# غزل شمارهٔ ۳۵۱۶

<div class="b" id="bn1"><div class="m1"><p>حال من از نظر یار نهان می دارند</p></div>
<div class="m2"><p>خبر مرگ ز بیمار نهان می دارند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دردمندان که ز درد دگران داغ شوند</p></div>
<div class="m2"><p>درد خود را ز پرستار نهان می دارند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبر بر زخم زبان کن اگر از اهل دلی</p></div>
<div class="m2"><p>غنچه را در بغل خار نهان می دارند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوربینان که ز غمازی راز آگاهند</p></div>
<div class="m2"><p>راز خود از در و دیوار نهان می دارند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چاک چون غنچه شود سینه جمعی آخر</p></div>
<div class="m2"><p>که به دل خرده اسرار نهان می دارند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی نیازست ز پوشش سر ارباب جنون</p></div>
<div class="m2"><p>سر بی مغز به دستار نهان می دارند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قدردانان بلا از نظر بیدردان</p></div>
<div class="m2"><p>خار را چون گل بی خار نهان می دارند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هیچ کس را خبری نیست ازان موی میان</p></div>
<div class="m2"><p>کافران رشته زنار نهان می دارند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پرده از روی سخن پیش سیه دل مگشا</p></div>
<div class="m2"><p>چهره از آینه تار نهان می دارند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سخت جانان صدف گوهر اسرار دلند</p></div>
<div class="m2"><p>لعل در سینه کهسار نهان می دارند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عشق را ساده دلانی که بپوشند به صبر</p></div>
<div class="m2"><p>شعله در زلف شب تار نهان می دارند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>روی مقصود نبینند گروهی صائب</p></div>
<div class="m2"><p>که گل از مرغ گرفتار نهان می دارند</p></div></div>