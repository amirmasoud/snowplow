---
title: >-
    غزل شمارهٔ ۱۳۳۳
---
# غزل شمارهٔ ۱۳۳۳

<div class="b" id="bn1"><div class="m1"><p>شب که مجلس روشنی از طلعت جانانه داشت</p></div>
<div class="m2"><p>شمع پیش چشم دست از شهپر پروانه داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می کند خون در دل اکنون پنجه خورشید را</p></div>
<div class="m2"><p>طی شد آن فرصت که زلف او سری با شانه داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش آن آیینه رو با صد حدیث آشنا</p></div>
<div class="m2"><p>طوطی من اعتبار سبزه بیگانه داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خمارآلودگان بگذشت چون جام تهی</p></div>
<div class="m2"><p>چشم مخموری که در هر گوشه صد میخانه داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خراباتی که خاک از جرعه خواری مست بود</p></div>
<div class="m2"><p>بوی می از ما دریغ آن نرگس مستانه داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا شدم عاقل به چشم من جهان تاریک شد</p></div>
<div class="m2"><p>بود از داغ جنون گر روزنی این خانه داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کرد بر مجنون فضای دشت را چون شهر تنگ</p></div>
<div class="m2"><p>صحبت گرمی که با اطفال این دیوانه داشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تنگ ظرفی مانع شور جنون ما نشد</p></div>
<div class="m2"><p>باده ما جوش خم در سینه پیمانه داشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دوش کان رخسار آتشناک بزم افروز شد</p></div>
<div class="m2"><p>بی رواجی شمع را محتاج یک پروانه داشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صرف تن گردید اوقات شریف دل تمام</p></div>
<div class="m2"><p>کعبه دامن بر میان در خدمت بتخانه داشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر که را از حلقه زهاد دیدم ساده تر</p></div>
<div class="m2"><p>دام چون تسبیح پنهان در میان دانه داشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیکسی بیزار کرد از زندگی صائب را</p></div>
<div class="m2"><p>وقت آن کس خوش که غمخواری درین غمخانه داشت</p></div></div>