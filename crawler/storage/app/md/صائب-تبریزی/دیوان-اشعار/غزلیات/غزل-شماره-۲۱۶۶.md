---
title: >-
    غزل شمارهٔ ۲۱۶۶
---
# غزل شمارهٔ ۲۱۶۶

<div class="b" id="bn1"><div class="m1"><p>با طره او مشک ختا دود کبابی است</p></div>
<div class="m2"><p>با چهره او صورت چین موج سرابی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با شوخی آن چشم، رم چشم غزالان</p></div>
<div class="m2"><p>در دیده صاحب نظران پرده خوابی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم است سراپا که به رخسار تو نو شد</p></div>
<div class="m2"><p>هر شاخ گلی را که به کف جام شرابی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می نوش و برافروز که شاخ گل سیراب</p></div>
<div class="m2"><p>هنگامه پر شور ترا سیخ کبابی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دلبری اندام تو کم نیست ز رخسار</p></div>
<div class="m2"><p>هر بند قبای تو مرا بند نقابی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روزی است که خط مشق پریشان کند آغاز</p></div>
<div class="m2"><p>مکتوب مرا از تو گر امید جوابی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از هر نگه ما و تو چون پرده برافتد</p></div>
<div class="m2"><p>پوشیده و سربسته سؤالی و جوابی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در دیده من جوهر بیرحمی شمشیر</p></div>
<div class="m2"><p>از سوختگی سایه بید و لب آبی است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دستی که به احسان، فلک خشک گشاید</p></div>
<div class="m2"><p>در دیده روشن گوهران موج سرابی است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیداست که تا چند بود خانه نگهدار</p></div>
<div class="m2"><p>صائب که درین بحر پرآشوب حبابی است</p></div></div>