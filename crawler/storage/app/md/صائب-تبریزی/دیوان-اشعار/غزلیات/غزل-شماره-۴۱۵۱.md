---
title: >-
    غزل شمارهٔ ۴۱۵۱
---
# غزل شمارهٔ ۴۱۵۱

<div class="b" id="bn1"><div class="m1"><p>مردان اگر نفس به فراغت کشیده‌اند</p></div>
<div class="m2"><p>در زیر آب تیغ شهادت کشیده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن‌ها که در بلندی فطرت یگانه‌اند</p></div>
<div class="m2"><p>در شهر خویش تلخی غربت کشیده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردانه می‌روند چو مجنون به کام شیر</p></div>
<div class="m2"><p>جمعی که چارموجه کثرت کشیده‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بار کوه قاف ندزدند دوش خویش</p></div>
<div class="m2"><p>تا سایلان گرانی منت کشیده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از زهر مرگ تلخ نسازند روی خویش</p></div>
<div class="m2"><p>آن‌ها که جام تلخ نصیحت کشیده‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از تاب عارض تو سلامت‌گزیدگان</p></div>
<div class="m2"><p>خود را به آفتاب قیامت کشیده‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب بهشت نسیه خود نقد کرده‌اند</p></div>
<div class="m2"><p>جمعی که پا به دامن عزلت کشیده‌اند</p></div></div>