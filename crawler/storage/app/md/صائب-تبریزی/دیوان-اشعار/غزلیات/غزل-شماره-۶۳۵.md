---
title: >-
    غزل شمارهٔ ۶۳۵
---
# غزل شمارهٔ ۶۳۵

<div class="b" id="bn1"><div class="m1"><p>ببین به دور لبش خط عنبرافشان را</p></div>
<div class="m2"><p>که چون شراب برون داده راز پنهان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به باد دست، کلید خزانه را مسپار</p></div>
<div class="m2"><p>مده به دست صبا زلف عنبرافشان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مکن به ماه نو ابروی یار را تشبیه</p></div>
<div class="m2"><p>چه نسبت است به محراب طاق نسیان را؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درازدستی اهل هوس ز گستاخی</p></div>
<div class="m2"><p>به ماه مصر گوارا نمود زندان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز لطف و قهر تو مهرم نمی شود کم و بیش</p></div>
<div class="m2"><p>که پشت و رو نبود آفتاب تابان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گره به جبهه آیینه وار خویش مزن</p></div>
<div class="m2"><p>مکن به طوطی خوش حرف تنگ، میدان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر تو دامن خود را به دست ما ندهی</p></div>
<div class="m2"><p>ز دست ما نگرفته است کس گریبان را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز بس که خلق تنک مایه اند از انصاف</p></div>
<div class="m2"><p>به سیم قلب نگیرند ماه کنعان را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر آن گروه حلال است دعوی همت</p></div>
<div class="m2"><p>که چین جبهه شمارند مد احسان را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کباب حسن گلوسوز تشنگی گردم!</p></div>
<div class="m2"><p>که سرد بر دل من کرد آب حیوان را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جدا نمی شود از هم، دو دل یکی چو شود</p></div>
<div class="m2"><p>نمی توان ز دل ما کشید پیکان را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غلط به کاغذ ابری کنند دیده وران</p></div>
<div class="m2"><p>فشرد بس که فلک ابرهای احسان را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در آن سری که بود خارخار شوق، کند</p></div>
<div class="m2"><p>چو گردباد به یک پای طی، بیابان را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز ناقصان بصیرت بلندپروازی</p></div>
<div class="m2"><p>سر از دریچه برون کردن است کوران را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خرید خون خود از ناز نعمت الوان</p></div>
<div class="m2"><p>فشرد بر جگر خویش هر که دندان را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز آه دل نگشاید که از گشاد خدنگ</p></div>
<div class="m2"><p>دل گرفته نگردد شکفته پیکان را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سخن به مردم فهمیده عرض کن صائب</p></div>
<div class="m2"><p>به شوره زار مکن صرف، آب حیوان را</p></div></div>