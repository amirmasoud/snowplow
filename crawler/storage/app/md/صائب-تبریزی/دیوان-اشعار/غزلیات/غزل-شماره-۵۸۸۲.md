---
title: >-
    غزل شمارهٔ ۵۸۸۲
---
# غزل شمارهٔ ۵۸۸۲

<div class="b" id="bn1"><div class="m1"><p>ما زهر را به جبهه بگشاده چون کشیم؟</p></div>
<div class="m2"><p>خمیازه را به چاشنی باده چون کشیم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از کوه قاف بال پریزاد عاجزست</p></div>
<div class="m2"><p>بار جهان به خاطر آزاده چون کشیم؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در فکر آسمان و زمینیم روز و شب</p></div>
<div class="m2"><p>با این غبار ودود، نفس ساده چون کشیم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما خون ز دست یار به صد ناز خورده ایم</p></div>
<div class="m2"><p>از دست دیگران قدح باده چون کشیم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دریا و موج تشنه آمیزش همند</p></div>
<div class="m2"><p>از عشق دامن دل آزاده چون کشیم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طوفان به بادبان ننهفته است هیچ کس</p></div>
<div class="m2"><p>بر روی شید، پرده سجاده چون کشیم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما ره به خط بی قلم یار برده ایم</p></div>
<div class="m2"><p>منت ز حرفهای قلم زاده چون کشیم؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فرمان قتل نیست چو پروانه مراد</p></div>
<div class="m2"><p>ما ناز نوخطان ز رخ ساده چون کشیم؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب بهشت جای خس و خار خشک نیست</p></div>
<div class="m2"><p>زهاد را به انجمن باده چون کشیم؟</p></div></div>