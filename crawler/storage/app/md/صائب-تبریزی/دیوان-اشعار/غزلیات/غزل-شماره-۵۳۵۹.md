---
title: >-
    غزل شمارهٔ ۵۳۵۹
---
# غزل شمارهٔ ۵۳۵۹

<div class="b" id="bn1"><div class="m1"><p>حنظل افلاک شکر بار باشد صبحدم</p></div>
<div class="m2"><p>شاخ خشک کهکشان پربار باشد صبحدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفتاب فیض حق از رخ نقاب افکنده است</p></div>
<div class="m2"><p>هر طرف چشم افکنی دیدار باشد صبحدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می توان شب خرج کردن قلب روی اندود را</p></div>
<div class="m2"><p>روز بازار دل بیدار باشد صبحدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست جمعی را که می لرزند در عرض دعا</p></div>
<div class="m2"><p>آفتاب انگشتر زنهار باشد صبحدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رحمت تقریب جوی کردگار بی نیاز</p></div>
<div class="m2"><p>گوش بر آواز استغفار باشد صبحدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ابروی دیده بیدار اشک حسرت است</p></div>
<div class="m2"><p>وقت چشمی خوش که طوفان بار باشد صبحدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رحمت حق مرهم کافور سامان می دهد</p></div>
<div class="m2"><p>عاشقانی را که دل افگار باشد صبحدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از گریبانش نسیم مصر سر بیرون کند</p></div>
<div class="m2"><p>دیده هر کس که چون دستار باشد صبحدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زود بر فتراک می بندد سر خورشید را</p></div>
<div class="m2"><p>دام هر کس دیده بیدار باشد صبحدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نافه آهوی شب را می شکافد تیغ مهر</p></div>
<div class="m2"><p>آسمانها طبله عطار باشد صبحدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دیده لبریز سرشک و سینه مالامال آه</p></div>
<div class="m2"><p>کس به این سامان چرا بیکار باشد صبحدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می تواند فیض برد از آفتاب لطف حق</p></div>
<div class="m2"><p>هرکه چون صائب دلش بیدار باشد صبحدم</p></div></div>