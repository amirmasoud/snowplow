---
title: >-
    غزل شمارهٔ ۱۹۱۶
---
# غزل شمارهٔ ۱۹۱۶

<div class="b" id="bn1"><div class="m1"><p>عرش بلند مرتبه بنیان آدم است</p></div>
<div class="m2"><p>خورشید عقل، شمسه ایوان آدم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آدم چه جوهری است، که گنجینه سپهر</p></div>
<div class="m2"><p>با صد هزار چشم نگهبان آدم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لعلی که خون کند به جگر آفتاب را</p></div>
<div class="m2"><p>در مشت خاک بی سر و سامان آدم است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همت بلند دار که نه خاتم سپهر</p></div>
<div class="m2"><p>فرمان پذیر دست سلیمان آدم است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در بزم قدسیان خبری زین چراغ نیست</p></div>
<div class="m2"><p>سوز و گداز، شمع شبستان آدم است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از پیچ و تاب درد، ملک را نصیب نیست</p></div>
<div class="m2"><p>این تاب در کمند رگ جان آدم است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ده آیه حواس که منشور قدرت است</p></div>
<div class="m2"><p>نازل ز روی مرتبه در شأن آدم است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از قدر، پای بر سر گردون گذاشته است</p></div>
<div class="m2"><p>در خاک اگر چه گوشه دامان آدم است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر هر گل زمین، گل ابری گماشته است</p></div>
<div class="m2"><p>روی شکفته تازه کن جان آدم است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا دست می رسد به می و مطرب و نگار</p></div>
<div class="m2"><p>اندیشه بهشت ز کفران آدم است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از دلو آفتاب ربوده است اختیار</p></div>
<div class="m2"><p>این یوسفی که در چه کنعان آدم است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آدم نه ای، ازان ز فلک شکوه می کنی</p></div>
<div class="m2"><p>ورنه فلک مسخر فرمان آدم است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صائب جواب آن غزل سیدست این</p></div>
<div class="m2"><p>کامروز آدم است که شیطان آدم است</p></div></div>