---
title: >-
    غزل شمارهٔ ۴۴۷۳
---
# غزل شمارهٔ ۴۴۷۳

<div class="b" id="bn1"><div class="m1"><p>سر چون گران شد از می دستارگو نباشد</p></div>
<div class="m2"><p>در بحر گوهر از کف آثار گو نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از مشت آب سردی دیگی نشیند از جوش</p></div>
<div class="m2"><p>در بزم می پرستان هشیار گو نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از شرم عشق ما راچون نیست دست چیدن</p></div>
<div class="m2"><p>گلهای این گلستان بی خار گو نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درمان چو می شود درد چون کرد کامرانی</p></div>
<div class="m2"><p>منت کش طبیبان بیمار گو نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیمانه ای که باید بر خاک ریخت آخر</p></div>
<div class="m2"><p>از آب زندگانی سرشار گو نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون غنچه دل ز هریک باید چو عاقبت کند</p></div>
<div class="m2"><p>برگ نشاط مارابسیار گو نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بزم آفرینش چشم سیه دل ما</p></div>
<div class="m2"><p>عبرت پذیر چون نیست بیدارگو نباشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بادا روان سلامت گر جسم ریزد از هم</p></div>
<div class="m2"><p>بر روی گنج گوهر دیوارگو نباشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کاری که دلنشین نیست محتاج کارفرماست</p></div>
<div class="m2"><p>چون کار دلپذیر ست سرکارگو نباشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زهری که عادتی شد چون شکرست شیرین</p></div>
<div class="m2"><p>غم سازگار چون شد غمخوارگو نباشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قدر خزف نباشد بی جوهری گهررا</p></div>
<div class="m2"><p>هر جا سخن رسی نیست گفتارگو نباشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر بخت سبزما را قسمت نشد ز گردون</p></div>
<div class="m2"><p>بر آبگینه مازنگارگو نباشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون می شود به سوهان هموار نفس سرکش</p></div>
<div class="m2"><p>وضع جهان هستی هموارگو نباشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صائب چو می توان شد از یک دو جام گلزار</p></div>
<div class="m2"><p>در پیش چشم ما را گلزارگو نباشد</p></div></div>