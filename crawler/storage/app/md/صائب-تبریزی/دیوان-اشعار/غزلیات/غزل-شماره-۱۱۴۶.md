---
title: >-
    غزل شمارهٔ ۱۱۴۶
---
# غزل شمارهٔ ۱۱۴۶

<div class="b" id="bn1"><div class="m1"><p>از عرق تا چهره گلرنگ جانان تر شده است</p></div>
<div class="m2"><p>دامن گلهابه شبنم آتشین بستر شده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقد می سازد قیامت را به عاشق شور عشق</p></div>
<div class="m2"><p>دامن صحرا به مجنون دامن محشر شده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست در زندان آهن بی قراران را قرار</p></div>
<div class="m2"><p>سینه سنگ از شرار شوخ من مجمر شده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون توانم همسفر شد با سبکپایان شوق؟</p></div>
<div class="m2"><p>من که دامن پیش پایم سد اسکندر شده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در قیامت شسته رو برخیزد از آغوش خاک</p></div>
<div class="m2"><p>چهره هر کس که از اشک ندامت تر شده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مانع پرواز من کوتاهی بال و پرست</p></div>
<div class="m2"><p>بادبان بر کشتی بی طالعم لنگر شده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می شود طومار عمرش طی به اندک فرصتی</p></div>
<div class="m2"><p>چون قلم هر کس ز بی مغزی زبان آور شده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>علم رسمی تیره دارد سینه صاف مرا</p></div>
<div class="m2"><p>بی صفا آیینه ام از کثرت جوهر شده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون توانم داشت پنهان فقر را از چشم خلق؟</p></div>
<div class="m2"><p>خرقه صد پاره بر بی برگیم محضر شده است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خورده ام چون موی آتش دیده چندین پیچ و تاب</p></div>
<div class="m2"><p>تا رگ ابرم ز دریا رشته گوهر شده است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می کند بی دست و پایی دشمنان را مهربان</p></div>
<div class="m2"><p>شعله بر خاشاک من بسیار بال و پر شده است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا چه خواهد کرد صائب با دل مومین من</p></div>
<div class="m2"><p>آتشین رویی کز او آیینه خاکستر شده است</p></div></div>