---
title: >-
    غزل شمارهٔ ۳۷۰۸
---
# غزل شمارهٔ ۳۷۰۸

<div class="b" id="bn1"><div class="m1"><p>زبان شِکوهٔ ما لعل یار می‌بندد</p></div>
<div class="m2"><p>لب پیاله دهان خمار می‌بندد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز جوش باده خم از جای خویشتن نرود</p></div>
<div class="m2"><p>جنون چه طرف ازین خاکسار می‌بندد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غبار خاطر من آن قدر گران‌خیز است</p></div>
<div class="m2"><p>که ره به جلوه سیل بهار می‌بندد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به من عداوت این چرخ نیلگون غلط است</p></div>
<div class="m2"><p>کدام آینه طرف از غبار می‌بندد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به این امید که در دامن تو آویزد</p></div>
<div class="m2"><p>نسیم پیرهن از مصر بار می‌بندد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر نه روی تو آیینه را دهد پرداز</p></div>
<div class="m2"><p>دگر که آب درین جویبار می‌بندد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کلید آه ترا جوهری اگر باشد</p></div>
<div class="m2"><p>که بر رخ تو در این حصار می‌بندد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به دست، کار جهان را تمام نتوان کرد</p></div>
<div class="m2"><p>جهان ازوست که همت به کار می‌بندد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جواب آن غزل بلبل نشابورست</p></div>
<div class="m2"><p>«که رنگ لاله و گل برقرار می‌بندد»</p></div></div>