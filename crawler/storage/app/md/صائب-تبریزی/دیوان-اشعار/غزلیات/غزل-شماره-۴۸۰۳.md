---
title: >-
    غزل شمارهٔ ۴۸۰۳
---
# غزل شمارهٔ ۴۸۰۳

<div class="b" id="bn1"><div class="m1"><p>سبک ز سینه ما ای غبار غم برخیز</p></div>
<div class="m2"><p>ز همنشینی ما می کشی الم برخیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر قلم بشکن، مهر کن دهان دوات</p></div>
<div class="m2"><p>به این سیاه دلان کم نشین و کم برخیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گذشتن از سر گنج گهر سخاوت نیست</p></div>
<div class="m2"><p>کریمی از سر آوازه کرم برخیز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کلید گلشن فردوس دست احسان است</p></div>
<div class="m2"><p>بهشت می طلبی از سر درم برخیز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدار عزت موی سفید پیران را</p></div>
<div class="m2"><p>ز جای خویش به تعظیم صبحدم برخیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درین دو وقت، اجابت گشاده پیشانی است</p></div>
<div class="m2"><p>دل شب از نتوانی سپیده دم برخیز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرفت دامن گل شبنم از سحر خیزی</p></div>
<div class="m2"><p>زگرد خواب بشو دست و رو، توهم برخیز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>امید فتح و ظفر هست تا علم برجاست</p></div>
<div class="m2"><p>فروغ صبح نخوابانده تا علم برخیز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درین جهان نبود فرصت کمربستن</p></div>
<div class="m2"><p>زخاک تیره کمر بسته چون قلم برخیز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به فکر دوست به بالین گذار سر صائب</p></div>
<div class="m2"><p>چو آفتاب ز آغوش صبحدم برخیز</p></div></div>