---
title: >-
    غزل شمارهٔ ۱۱۸۱
---
# غزل شمارهٔ ۱۱۸۱

<div class="b" id="bn1"><div class="m1"><p>هر که غافل را نصیحت می کند دیوانه است</p></div>
<div class="m2"><p>خواب غفلت برده را طبل رحیل افسانه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نفس خائن زندگی را تلخ بر من کرده است</p></div>
<div class="m2"><p>وای بر آن کس که دزدش در درون خانه است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماتم و سور جهان با یکدگر آمیخته است</p></div>
<div class="m2"><p>صاف و درد این چمن چون لاله یک پیمانه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست در فکر گلستان بلبل بی درد ما</p></div>
<div class="m2"><p>بس که در کنج قفس مشغول آب و دانه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا دهن بازست روزی می رسد از خواب غیب</p></div>
<div class="m2"><p>عقد دندانها کلید رزق را دندانه است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اختر اقبال بی برگان بلند افتاده است</p></div>
<div class="m2"><p>هر که را شمع و چراغی هست از پروانه است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر چه غیر از نقطه وحدت درین دفتر بود</p></div>
<div class="m2"><p>دیده بالغ نظر را ابجد طفلانه است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حسن نتواند ز فرمان سرکشیدن عشق را</p></div>
<div class="m2"><p>شمع با آن سرکشی زیر پر پروانه است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برنیاید زاهد از فکر بهشت و جوی شیر</p></div>
<div class="m2"><p>نقل خواب آلودگان شیرینی افسانه است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گوهر ارزنده ای گر هست آب تلخ را</p></div>
<div class="m2"><p>در بساط دلفریبی گریه مستانه است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بلبلان در زیر پر سیر گلستان می کنند</p></div>
<div class="m2"><p>برگ عیش غنچه خسبان در درون خانه است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در مقام خویش هر زشتی بود صائب نکو</p></div>
<div class="m2"><p>می برد چون خال دل تا جغد در ویرانه است</p></div></div>