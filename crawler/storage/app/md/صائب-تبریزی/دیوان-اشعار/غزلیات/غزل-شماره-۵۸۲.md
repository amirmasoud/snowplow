---
title: >-
    غزل شمارهٔ ۵۸۲
---
# غزل شمارهٔ ۵۸۲

<div class="b" id="bn1"><div class="m1"><p>عجب که یک دل خوش در جهان شود پیدا</p></div>
<div class="m2"><p>ز شوره زار کجا گلستان شود پیدا؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مده چو تیر هوایی به باد عمر عزیز</p></div>
<div class="m2"><p>کشیده دار کمان تا نشان شود پیدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مزن چو تیغ به هر سنگ گوهر خود را</p></div>
<div class="m2"><p>خموش باش که سنگ فسان شود پیدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عزیز دار چو اکسیر خاکساران را</p></div>
<div class="m2"><p>که ماه مصر درین کاروان شود پیدا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کجی و راستی خلق را محک سفرست</p></div>
<div class="m2"><p>که حال تیر جدا از کمان شود پیدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز چهره سازی گل مطلب بهار این است</p></div>
<div class="m2"><p>که عندلیب درین گلستان شود پیدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه خامه ها که در انشای شوق شد کوتاه</p></div>
<div class="m2"><p>نشد که شیری ازین نیستان شود پیدا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حضور، پرده بینایی است و پنبه گوش</p></div>
<div class="m2"><p>که قدر بلبل ما در خزان شود پیدا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز هم جدا نبود نوش و نیش این گلشن</p></div>
<div class="m2"><p>که وقت چیدن گل باغبان شود پیدا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنین که همت ما را بلند ساخته اند</p></div>
<div class="m2"><p>عجب که مطلب ما در جهان شود پیدا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر تو آینه سینه را دهی پرواز</p></div>
<div class="m2"><p>هزار طوطی شیرین زبان شود پیدا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کدام نوش که در وی نهفته نیشی نیست؟</p></div>
<div class="m2"><p>نفاق بیشتر از دوستان شود پیدا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>توان برید چو مقراض صائب از عالم</p></div>
<div class="m2"><p>درین زمانه اگر همزبان شود پیدا</p></div></div>