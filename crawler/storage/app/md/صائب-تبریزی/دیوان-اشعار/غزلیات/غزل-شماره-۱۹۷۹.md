---
title: >-
    غزل شمارهٔ ۱۹۷۹
---
# غزل شمارهٔ ۱۹۷۹

<div class="b" id="bn1"><div class="m1"><p>دود دلی ز ابر گهربار مانده است</p></div>
<div class="m2"><p>داور تری ز قلزم زخار مانده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روشندلان به تیره دلان جا سپرده اند</p></div>
<div class="m2"><p>کف از محیط، از آینه زنگار مانده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بکسر زبان دعوی بی معنی اند خلق</p></div>
<div class="m2"><p>برگی به نخل معرفت از بار مانده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبح شعور، مست شکر خواب غفلت است</p></div>
<div class="m2"><p>افسانه ای ز دیده بیدار مانده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از عرض علم، مانده به جا عرض سینه ای</p></div>
<div class="m2"><p>از اهل حال، جبه و دستار مانده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داند که من ز جسم گرانجان چه می کشم</p></div>
<div class="m2"><p>دامان هر که در ته دیوار مانده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا صبح حشر هست مرا کار در کفن</p></div>
<div class="m2"><p>در سینه بس که نشتر آزار مانده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از حیرت خرام تو این چرخ آبگون</p></div>
<div class="m2"><p>چون آب آبگینه ز رفتار مانده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طوفان گره شده است مرا در دل تنور</p></div>
<div class="m2"><p>تا مهر شرم بر لب اظهار مانده است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در زردی آفتاب قیامت نهاد روی</p></div>
<div class="m2"><p>امید من به وعده دیدار مانده است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جوهر به چشم آینه خاشاک گشته است</p></div>
<div class="m2"><p>تا ناامید ازان گل رخسار مانده است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در تنگنای سینه صائب خیال دوست</p></div>
<div class="m2"><p>پیغمبر خداست که در غار مانده است</p></div></div>