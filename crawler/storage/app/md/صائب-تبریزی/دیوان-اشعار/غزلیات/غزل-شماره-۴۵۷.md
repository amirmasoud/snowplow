---
title: >-
    غزل شمارهٔ ۴۵۷
---
# غزل شمارهٔ ۴۵۷

<div class="b" id="bn1"><div class="m1"><p>رموز سرگذشت عاشقان گر دیدنی دارد</p></div>
<div class="m2"><p>خدا را سرسری مگذر ز اوراق خزان ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر در ملک صورت نیست ما را گوشه ای صائب</p></div>
<div class="m2"><p>سواد اعظم معنی است ملک بیکران ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندارد زآفتاب تربیت طالع بیان ما</p></div>
<div class="m2"><p>به سیلی رنگ گرداند ثمر در بوستان ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندیدیم از سخن فهمان عالم گوشه چشمی</p></div>
<div class="m2"><p>اگر چه سرمه شد از فکر مغز استخوان ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر لیلی، اگر مجنون ز ما دارند تلقین را</p></div>
<div class="m2"><p>به حسن و عشق حق تربیت دارد بیان ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کلام ما خلایق را به راه راست می آرد</p></div>
<div class="m2"><p>کجی از تیر بیرون می برد زور کمان ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عزیز قدردانی نیست در مصر سخن سنجی</p></div>
<div class="m2"><p>ندارد ورنه جنسی غیر یوسف کاروان ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گل خود می شمارد خنده صبح قیامت را</p></div>
<div class="m2"><p>چراغی کز دل بیدار دارد دودمان ما</p></div></div>