---
title: >-
    غزل شمارهٔ ۱۸۶۷
---
# غزل شمارهٔ ۱۸۶۷

<div class="b" id="bn1"><div class="m1"><p>آتش کباب کرده یاقوت آن لب است</p></div>
<div class="m2"><p>چشم سهیل در پی آن سیب غبغب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای خضر چند تیر به تاریکی افکنی؟</p></div>
<div class="m2"><p>سرچشمه حیات نهان در دل شب است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون می رسد به مجلس ما سجده می کند</p></div>
<div class="m2"><p>مینای ما که خضر ره اهل مشرب است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راه نفس ز کثرت تبخاله بسته شد</p></div>
<div class="m2"><p>گوید هنوز عشق که اینها گل تب است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دست دیگران بود آزاد کردنم</p></div>
<div class="m2"><p>در چارسوی دهر دلم طفل مکتب است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صائب نمی فروزد شمع مراد من</p></div>
<div class="m2"><p>تا صبحدم اگر چه لبم گرم یارب است</p></div></div>