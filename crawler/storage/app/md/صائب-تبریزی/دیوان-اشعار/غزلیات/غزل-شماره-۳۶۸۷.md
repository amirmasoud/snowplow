---
title: >-
    غزل شمارهٔ ۳۶۸۷
---
# غزل شمارهٔ ۳۶۸۷

<div class="b" id="bn1"><div class="m1"><p>خوشا سعادت آن دل که آب می‌گردد</p></div>
<div class="m2"><p>که شبنم آینه آفتاب می‌گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به آتشی است دل خون‌چکان من مایل</p></div>
<div class="m2"><p>که شسته روی به اشک کباب می‌گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشو ز وقت ملاقات دوستان غافل</p></div>
<div class="m2"><p>که هر دعا که کنی مستجاب می‌گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگرچه موی سفیدست تازیانه مرگ</p></div>
<div class="m2"><p>به چشم نرم تو رگ‌های خواب می‌گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه از برای تماشاست کوچه‌گردی من</p></div>
<div class="m2"><p>ز بیم سوختن خود کباب می‌گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همان که در طلبش رفته‌ای ز خود بیرون</p></div>
<div class="m2"><p>درون خلوت دل بی‌نقاب می‌گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فسانه می‌شمرد مست، شور محشر را</p></div>
<div class="m2"><p>کجا به چشم تو از ناله خواب می‌گردد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تپیدن دل ما صائب اختیاری نیست</p></div>
<div class="m2"><p>به تازیانه آتش، کباب می‌گردد</p></div></div>