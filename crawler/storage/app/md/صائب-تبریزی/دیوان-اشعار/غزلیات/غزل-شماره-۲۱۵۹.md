---
title: >-
    غزل شمارهٔ ۲۱۵۹
---
# غزل شمارهٔ ۲۱۵۹

<div class="b" id="bn1"><div class="m1"><p>از شش جهتم همچو شرر سنگ گرفته است</p></div>
<div class="m2"><p>این بار جنون سخت به من تنگ گرفته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در پنجه شیرست رگ و ریشه جانم</p></div>
<div class="m2"><p>تا شانه سر زلف تو در چنگ گرفته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان چهره گلرنگ خط سبز دمیده است؟</p></div>
<div class="m2"><p>یا آینه بینش من زنگ گرفته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ایام حیاتم شب قدرست سراسر</p></div>
<div class="m2"><p>تا دل ز من آن طره شبرنگ گرفته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خون می خلدم در جگر از رشک چو نشتر</p></div>
<div class="m2"><p>تیغ تو ز خون که دگر رنگ گرفته است؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون گوشه نگیرم ز عزیزان، که مکرر</p></div>
<div class="m2"><p>از آب گهر آینه ام زنگ گرفته است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تاب سخن سخت ز معشوق ندارد</p></div>
<div class="m2"><p>صائب که مکرر ز هوا سنگ گرفته است</p></div></div>