---
title: >-
    غزل شمارهٔ ۶۱۴۱
---
# غزل شمارهٔ ۶۱۴۱

<div class="b" id="bn1"><div class="m1"><p>گوهر راز از دل بی تاب می آید برون</p></div>
<div class="m2"><p>گنج ازین ویرانه چون سیلاب می آید برون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورده ام از بس که خون دل ز جام زندگی</p></div>
<div class="m2"><p>گر به خاکم خط کشی، خوناب می آید برون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بستن لب بر در روزی کند کار کلید</p></div>
<div class="m2"><p>کوزه از خم پر شراب ناب می آید برون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از میان نازک او گر برآید پیچ و تاب</p></div>
<div class="m2"><p>رشته جان هم ز پیچ و تاب می آید برون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می شود همچشم مجمر زود سقف خانه ام</p></div>
<div class="m2"><p>گر چنین آه از دل بی تاب می آید برون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس که از سوز دلم دیوار و در تفسیده است</p></div>
<div class="m2"><p>خشک از ویرانه ام سیلاب می آید برون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روزیش خون جگر می گردد از دریای شیر</p></div>
<div class="m2"><p>هر که بی می در شب مهتاب می آید برون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که از ناقص عیاری نیست خالص طاعتش</p></div>
<div class="m2"><p>روسیاه از بوته محراب می آید برون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از نگاه کج دل نازک به خون غلطد مرا</p></div>
<div class="m2"><p>از زمین من به ناخن آب می آید برون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از می گلگون یکی صد شد صفای عارضش</p></div>
<div class="m2"><p>گوهر شهوار خوب از آب می آید برون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر که صائب چون صدف بر لب زند مهر سکوت</p></div>
<div class="m2"><p>از دهانش گوهر سیراب می آید برون</p></div></div>