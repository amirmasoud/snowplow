---
title: >-
    غزل شمارهٔ ۱۳۵۷
---
# غزل شمارهٔ ۱۳۵۷

<div class="b" id="bn1"><div class="m1"><p>کاروان گریه از چشمم ندانم چون گذشت</p></div>
<div class="m2"><p>تا سر مژگان رسید، از صد محیط خون گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قمریی بر لوح خاک از نقش پایش نقش بست</p></div>
<div class="m2"><p>سرو من هر جا که با آن قامت موزون گذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آتش سودا نمی خوابد به افسون اجل</p></div>
<div class="m2"><p>مرغ نتواند هنوز از تربت مجنون گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب به مستی پنبه از داغ درون برداشتم</p></div>
<div class="m2"><p>مست شد از بوی گل هر کس که از بیرون گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست بی روشندلی هرگز خرابات مغان</p></div>
<div class="m2"><p>خم سلامت باد اگر دوران افلاطون گذشت</p></div></div>