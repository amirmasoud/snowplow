---
title: >-
    غزل شمارهٔ ۳۶۷۴
---
# غزل شمارهٔ ۳۶۷۴

<div class="b" id="bn1"><div class="m1"><p>زخ تو از نگه گرم خوش جلا گردد</p></div>
<div class="m2"><p>اگرچه نفس از آیینه بی صفا گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به شیوه های تو هرکس که آشنا شده است</p></div>
<div class="m2"><p>به حیرتم که دگر با که آشنا گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز حکم تیغ قضا سر نمی توان پیچید</p></div>
<div class="m2"><p>وگرنه کیست ازان آستان جدا گردد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز طاعت است فزون آبروی تقصیرش</p></div>
<div class="m2"><p>نماز هرکه ز نظاره ات قضا گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل از غبار کدورت کمال می گیرد</p></div>
<div class="m2"><p>گهر ز گرد یتیمی گرانبها گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به ناله های پریشان امیدها دارم</p></div>
<div class="m2"><p>جدا رود ز کمان تیر و جمع وا گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز فکر دانه مخور زیر آسمان دل خویش</p></div>
<div class="m2"><p>به آب خشک محال است آسیا گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی شود ز خموشی هزار بیگانه</p></div>
<div class="m2"><p>به یک سخن دو لب از یکدگر جدا گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسا بهار و خزان را که پشت سر بیند</p></div>
<div class="m2"><p>چو سرو هر که درین باغ یک قبا گردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بهشت نسیه خود نقد می کند صائب</p></div>
<div class="m2"><p>اگر به حکم قضا آدمی رضا گردد</p></div></div>