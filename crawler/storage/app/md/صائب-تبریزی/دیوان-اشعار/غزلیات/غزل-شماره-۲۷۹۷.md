---
title: >-
    غزل شمارهٔ ۲۷۹۷
---
# غزل شمارهٔ ۲۷۹۷

<div class="b" id="bn1"><div class="m1"><p>در آن مجلس که از مستی رخت طاقت گداز افتد</p></div>
<div class="m2"><p>اگر خورشید تابان چهره افروزد به گاز افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>علاج من همان از چشم بیمار تو می آید</p></div>
<div class="m2"><p>کجا درد محبت را مسیحا چاره ساز افتد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دامان غرور آب زمزم گرد ننشیند</p></div>
<div class="m2"><p>اگر صد تشنه از پا در بیابان حجاز افتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز زخم خنجر الماس پهلو می کنی خالی</p></div>
<div class="m2"><p>چه خواهی کرد اگر کارت به مژگان دراز افتد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هجوم بیقراران تیغ غیرت برنمی تابد</p></div>
<div class="m2"><p>مبادا دیده محمود بر زلف ایاز افتد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عجب نبود کز آن رو آب می گردد دل صائب</p></div>
<div class="m2"><p>هوا چون آتشین شد نخل مومین در گداز افتد</p></div></div>