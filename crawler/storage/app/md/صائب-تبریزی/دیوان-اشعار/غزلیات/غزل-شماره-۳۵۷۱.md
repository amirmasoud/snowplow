---
title: >-
    غزل شمارهٔ ۳۵۷۱
---
# غزل شمارهٔ ۳۵۷۱

<div class="b" id="bn1"><div class="m1"><p>مکن از بخت شکایت که وبالش می‌بود</p></div>
<div class="m2"><p>پای طاووس اگر چون پر و بالش می‌بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون ثمر دست به رعنایی اگر می‌افشاند</p></div>
<div class="m2"><p>لاف آزادگی از سرو حلالش می‌بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر برون نامدی از پرده جمالش، عالم</p></div>
<div class="m2"><p>کف خاکستری از برق جلالش می‌بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرگ می‌شد به نظر دولت بیدار مرا</p></div>
<div class="m2"><p>در قیامت اگر امید وصالش می‌بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سالم از آتش سوزان چو سمندر می‌رفت</p></div>
<div class="m2"><p>مرغ اگر نامه من بر پر و بالش می‌بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این قدر در دل خون گشته نمی‌گشت گره</p></div>
<div class="m2"><p>بوسه گر رنگی از آن چهره آلش می‌بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌شد انگشت‌نما چون مه نو صائب فکر</p></div>
<div class="m2"><p>اگر آن موی‌میان راه خیالش می‌بود</p></div></div>