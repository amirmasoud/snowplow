---
title: >-
    غزل شمارهٔ ۴۹۵۰
---
# غزل شمارهٔ ۴۹۵۰

<div class="b" id="bn1"><div class="m1"><p>چگونه جان برد صید از کمین چشم فتانش؟</p></div>
<div class="m2"><p>که گیراتر بود از خون ناحق تیغ مژگانش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز فیض عشق بر خورشید رخساری نظر دارم</p></div>
<div class="m2"><p>که می ساید به ابر از بس بلندی تیغ مژگانش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر شمع سهیل از آفت صرصر فرو میرد</p></div>
<div class="m2"><p>توان روشن نمود از پرتو سیب ز نخدانش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پی تسکین خاطرآرزویی می کنم رنگین</p></div>
<div class="m2"><p>وگرنه من کیم تا باشم زخیل شهیدانش؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سری شایسته سرگشتگی زلفش نمی یابد</p></div>
<div class="m2"><p>ازان رو بر هوا مانده است دایم دست و چوگانش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو مغز پسته در شکر شود گم حنظل گردون</p></div>
<div class="m2"><p>تبسم ریزچون گردد دهان شکر افشانش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گذارد بند بر پا آسمان را کوه تمکینش</p></div>
<div class="m2"><p>قیامت را به رفتار آورد سر و خرامانش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل خود می خوردموری اگر مهمان او گردد</p></div>
<div class="m2"><p>مخور صائب فریب آسمان و خوان احسانش</p></div></div>