---
title: >-
    غزل شمارهٔ ۱۹۹۰
---
# غزل شمارهٔ ۱۹۹۰

<div class="b" id="bn1"><div class="m1"><p>دل از حریم سینه به مژگان رسیده است</p></div>
<div class="m2"><p>کشتی به چار موجه طوفان رسیده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دل مجو قرار در آن زلف تابدار</p></div>
<div class="m2"><p>دیوانه ام به سلسله جنبان رسیده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا همچو خط لبی به او رسانده ام</p></div>
<div class="m2"><p>صد بار بیشتر به لبم جان رسیده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افتاده شو که از پر و بال فتادگی</p></div>
<div class="m2"><p>شبنم به آفتاب درخشان رسیده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز ماه ناتمام، که از خوان آفتاب</p></div>
<div class="m2"><p>در زیر آسمان به لب نان رسیده است؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طوق گلوی من شده خلخال ساق عرش</p></div>
<div class="m2"><p>قمری اگر به سرو خرامان رسیده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>احوال زخم و خنجر سیراب او مپرس</p></div>
<div class="m2"><p>لب تشنه ای به چشمه حیوان رسیده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون شانه تخته الف زخم گشته ام</p></div>
<div class="m2"><p>تا دست من به طره جانان رسیده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زان آتشین عذار که خورشید داغ اوست</p></div>
<div class="m2"><p>ته جرعه ای به لاله عذاران رسیده است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شد بوته گداز، تمامی هلال را</p></div>
<div class="m2"><p>رحم است بر سری که به سامان رسیده است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لرزد به خود ز قیمت نازل ز سنگ بیش</p></div>
<div class="m2"><p>تا گوهرم به پله میزان رسیده است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر چند بسته ام به زنجیر پای من</p></div>
<div class="m2"><p>شور جنون من به بیابان رسیده است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صائب همان ز غیرت خود درکشاکشم</p></div>
<div class="m2"><p>هر چند تیشه ام به رگ کان رسیده است</p></div></div>