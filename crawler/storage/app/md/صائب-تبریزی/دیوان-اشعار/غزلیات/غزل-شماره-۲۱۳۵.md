---
title: >-
    غزل شمارهٔ ۲۱۳۵
---
# غزل شمارهٔ ۲۱۳۵

<div class="b" id="bn1"><div class="m1"><p>مأوای تو از کعبه و بتخانه کدام است؟</p></div>
<div class="m2"><p>ای خانه برانداز، ترا خانه کدام است؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دیده یکتایی ما خال دویی نیست</p></div>
<div class="m2"><p>زنار چه و سبحه صددانه کدام است؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از کثرت روزن نشود مهر مکرر</p></div>
<div class="m2"><p>ای کج نظران کعبه و بتخانه کدام است؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر چاک گریبان نکند راهنمایی</p></div>
<div class="m2"><p>طفلان چه شناسند که دیوانه کدام است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق از ره تکلیف به دل پا نگذارد</p></div>
<div class="m2"><p>سیلاب نپرسد که در خانه کدام است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر روی دلی از طرف شمع ندیده است</p></div>
<div class="m2"><p>صائب سبب جرأت پروانه کدام است؟</p></div></div>