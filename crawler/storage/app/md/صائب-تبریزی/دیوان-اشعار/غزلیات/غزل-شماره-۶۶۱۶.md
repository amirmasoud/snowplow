---
title: >-
    غزل شمارهٔ ۶۶۱۶
---
# غزل شمارهٔ ۶۶۱۶

<div class="b" id="bn1"><div class="m1"><p>ای عالم از ظهور صفاتت عیان شده</p></div>
<div class="m2"><p>بست و گشاد دست تو دریا و کان شده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیدایی تو دست اشارات کرده قطع</p></div>
<div class="m2"><p>عریانی تو پرده چشم جهان شده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بی دریغ بخشی حسن کریم تو</p></div>
<div class="m2"><p>هر ذره ای به هستی خود بدگمان شده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چندین هزار فاخته از مرغزار قدس</p></div>
<div class="m2"><p>در جستجوی سرو تو بی آشیان شده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر سبزه ای که از جگر خاک سرزده است</p></div>
<div class="m2"><p>از جویبار ذکر تو رطب اللسان شده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندیشه بلندخیالان عرش سیر</p></div>
<div class="m2"><p>از دورباش کنه تو در دل نهان شده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آب روان ز حکم تو گردیده است سنگ</p></div>
<div class="m2"><p>سنگ از حجاب حسن تو آب روان شده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از صد هزار فتنه یکی از ریاض تو</p></div>
<div class="m2"><p>گل کرده است و نرگس چشم بتان شده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با یک زبان، به شکر تو هر سبزه ده زبان</p></div>
<div class="m2"><p>با صد زبان، به حمد تو گل یک زبان شده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یک قطره عرق ز رخ لاله رنگ تو</p></div>
<div class="m2"><p>بر برگ گل چکیده، لب دلستان شده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از خاک دانه سوز تو یک دانه ضعیف</p></div>
<div class="m2"><p>بیرون فتاده، خال لب دلبران شده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چندین هزار قامت از تیر راست تر</p></div>
<div class="m2"><p>در زیر بار عشق تو خم چون کمان شده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خواب گران به دیده ما پرده بسته است</p></div>
<div class="m2"><p>ورنه چنان که هست جمالت عیان شده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بی سرمه چشم را که چنین می کند سیاه؟</p></div>
<div class="m2"><p>عالم سیاه در نظر سرمه دان شده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گل را به روی تازه آتش چه نسبت است؟</p></div>
<div class="m2"><p>دوزخ فسرده است که باغ جنان شده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>این است اگر فریب تو، فرداست دیده ام</p></div>
<div class="m2"><p>صائب یکی ز جمله دردی کشان شده</p></div></div>