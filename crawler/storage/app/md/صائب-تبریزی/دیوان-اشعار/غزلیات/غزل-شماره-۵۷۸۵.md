---
title: >-
    غزل شمارهٔ ۵۷۸۵
---
# غزل شمارهٔ ۵۷۸۵

<div class="b" id="bn1"><div class="m1"><p>رنگین شده است بس که ز خونین ترانه‌ام</p></div>
<div class="m2"><p>مرغان غلط کنند به گل آشیانه‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر پاره از دلم در توحید می‌زند</p></div>
<div class="m2"><p>یک نقش بیش نیست در آیینه‌خانه‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل خوردن است قسمتم از گرد خوان چرخ</p></div>
<div class="m2"><p>از مرکز خودست چو پرگار دانه‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون موجه سراب درین دشت آتشین</p></div>
<div class="m2"><p>از پیچ و تاب خویش بود تازیانه‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمم چو شمع نیست به جام و سبوی کس</p></div>
<div class="m2"><p>از گریهٔ خود است شراب شبانه‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سودای زلف سلسله‌جنبان گفتگوست</p></div>
<div class="m2"><p>کوته نمی‌شود به شنیدن فسانه‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن بلبل غریب نوایم که در چمن</p></div>
<div class="m2"><p>ننشست جوش سینه گل از ترانه‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مستغنی‌ام ز خلق که اکسیر عشق ساخت</p></div>
<div class="m2"><p>چون آفتاب چهره زرین خزانه‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون غنچه داشتم دل جمعی درین چمن</p></div>
<div class="m2"><p>بر باد داد یک نفس بی‌غمانه‌ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صائب ز جای خود نبرد حرف حق مرا</p></div>
<div class="m2"><p>از تیر راست، روی نتابد نشانه‌ام</p></div></div>