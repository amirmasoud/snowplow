---
title: >-
    غزل شمارهٔ ۳۹۳۹
---
# غزل شمارهٔ ۳۹۳۹

<div class="b" id="bn1"><div class="m1"><p>بهار و باغ به دلهای آتشین چه کند</p></div>
<div class="m2"><p>به تخم سوخته دلسوزی زمین چه کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل پیاده اوسرورا خجل دارد</p></div>
<div class="m2"><p>اگر سوار شود در میان زین چه کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر نه فکر عقیق دهان او باشد</p></div>
<div class="m2"><p>کسی علاج جگرهای آتشین چه کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه مرده اید که رحمت به ما چه خواهد کرد</p></div>
<div class="m2"><p>جز این لطف کند یار نازنین چه کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز وصف ذره بودبی نیاز پرتومهر</p></div>
<div class="m2"><p>سخن بلند چو افتاد آفرین چه کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی است نسبت برق فنا به آهن و موم</p></div>
<div class="m2"><p>حصار عافیت خودکس آهنین چه کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیالش از دل تنگم چه می کشد صائب</p></div>
<div class="m2"><p>به تنگنای صدف گوهر ثمین چه کند</p></div></div>