---
title: >-
    غزل شمارهٔ ۵۱۲۷
---
# غزل شمارهٔ ۵۱۲۷

<div class="b" id="bn1"><div class="m1"><p>گر چه صاحب نظرانند تماشایی شمع</p></div>
<div class="m2"><p>بهر پروانه بود انجمن آرایی شمع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچ جا تا دل پروانه نگیرد آرام</p></div>
<div class="m2"><p>هر شراری که جهد از دل شیدایی شمع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرچند در خاطر پروانه عاشق مصور گردد</p></div>
<div class="m2"><p>می توان دید در آیینه بینایی شمع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جوهر عشق ز پیشانی عاشق گویاست</p></div>
<div class="m2"><p>نشود سوختگی سرمه گویایی شمع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق روزی که قرار از دل پروانه ربود</p></div>
<div class="m2"><p>رعشه افتاد به سرپنچه گیرایی شمع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل چو روشن شود از عشق ،زبان کند شود</p></div>
<div class="m2"><p>تا دم صبح بود جلوه رعنایی شمع</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خط به آن چهره روشن چه تواند کردن؟</p></div>
<div class="m2"><p>شب تاریک بود سرمه بینایی شمع</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق درپرده ناموس نهفتم، غافل</p></div>
<div class="m2"><p>که ز فانوس بود جامه رسوایی شمع</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یارب این بزم چه بزم است که از گریه وآه</p></div>
<div class="m2"><p>غم پروانه ندارد سر سودایی شمع</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تادرین انجمن از سوختنی هست نشان</p></div>
<div class="m2"><p>پابه دامن نکشد جلوه هر جایی شمع</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کثرت خلق به توحید چه نقصان دارد؟</p></div>
<div class="m2"><p>چه خلل می رسد از رشته به یکتایی شمع؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می کند گریه و همدرد ندارد،صائب</p></div>
<div class="m2"><p>جای رحم است درین بزم به تنهایی شمع</p></div></div>