---
title: >-
    غزل شمارهٔ ۳۲۰
---
# غزل شمارهٔ ۳۲۰

<div class="b" id="bn1"><div class="m1"><p>ز سختی های دوران دیده بینا شود پیدا</p></div>
<div class="m2"><p>شرار زنده دل از آهن (و) خارا شود پیدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهد پیوسته نبض موج در دریای پرشورش</p></div>
<div class="m2"><p>دل آسوده هیهات است در دنیا شود پیدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خون خوردن گشاید عقده سر در گم عالم</p></div>
<div class="m2"><p>چنان کز باده روشن، ته دلها شود پیدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گذارد سرو را از طوق قمری نعل در آتش</p></div>
<div class="m2"><p>به هر گلشن که آن سرو سهی بالا شود پیدا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز شوق جستن آتش زیر پا دارد شرار من</p></div>
<div class="m2"><p>چه آسایش مرا از بستر خارا شود پیدا؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>توان در پرده شرم از عذار یار گل چیدن</p></div>
<div class="m2"><p>که حسن باده گلرنگ در مینا شود پیدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جز خفت ندارد حاصلی خشم و غضب صائب</p></div>
<div class="m2"><p>به غیر از کف چه از آشفتن دریا شود پیدا؟</p></div></div>