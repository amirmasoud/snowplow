---
title: >-
    غزل شمارهٔ ۵۳۵۰
---
# غزل شمارهٔ ۵۳۵۰

<div class="b" id="bn1"><div class="m1"><p>سوختم تا ره در آن زلف معنبر یافتم</p></div>
<div class="m2"><p>خشک چون سوزن شدم کاین رشته را سر یافتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می توانم از نگاهی ذره را خورشید کرد</p></div>
<div class="m2"><p>فیض آن صبح بنا گوشی که من دریافتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان به گرد خویش چون پرگار می گردم که من</p></div>
<div class="m2"><p>از سویدای دل خود کعبه را دریافتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سایه ارباب دولت شمع راه ظلمت است</p></div>
<div class="m2"><p>آب حیوان را به اقبال سکندر یافتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون غبار خاکساران را نسازم توتیا</p></div>
<div class="m2"><p>من که در گرد یتیمی آب گوهر یافتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رخنه گفتار بر من زندگی را تلخ داشت</p></div>
<div class="m2"><p>تا شدم خاموش خود را تنگ شکر یافتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دامن داغ جنون آسان نمی آید به دست</p></div>
<div class="m2"><p>سوختم چون شمع تا از آتش افسر یافتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باغ جنت را که تنگ است آسمان بر جلوه اش</p></div>
<div class="m2"><p>سر به زیر بال بردم در ته پر یافتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به که بردارم زلب مهر خموشی شکوه را</p></div>
<div class="m2"><p>من که صائب دست بر دامان دلبر یافتم</p></div></div>