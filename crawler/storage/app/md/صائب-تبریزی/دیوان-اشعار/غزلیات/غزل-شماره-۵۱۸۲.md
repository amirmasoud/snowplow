---
title: >-
    غزل شمارهٔ ۵۱۸۲
---
# غزل شمارهٔ ۵۱۸۲

<div class="b" id="bn1"><div class="m1"><p>پای گستاخ منه بر در کاشانه عشق</p></div>
<div class="m2"><p>سر منصور بود کنگره خانه عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حیف فرهاد که با آنهمه شیرین کاری</p></div>
<div class="m2"><p>شد به خواب عدم از تلخی افسانه عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر درآتش روی از خامیت آتش سوزد</p></div>
<div class="m2"><p>تا سرت گرم نگشته است زپیمانه عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شیشه بندان ظرافت بهمش می شکنند</p></div>
<div class="m2"><p>محتسب گر گذرد از دل میخانه عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با چه رو، روی به طوف حرم کعبه کنیم؟</p></div>
<div class="m2"><p>نیست بر جبهه ما صندل بتخانه عشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو سبو شانه ندزدیده ام از باده کشی</p></div>
<div class="m2"><p>کرده ام ازدل و جان خدمت میخانه عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من به معموره عقلم به پشیزی محتاج</p></div>
<div class="m2"><p>گنج برروی هم افتاده به ویرانه عشق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قطره ای نیست هوایی نبود در سر او</p></div>
<div class="m2"><p>می پرد چشم حباب از پی پیمانه عشق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شعله رابا خس و خاشاک بهم درپیچد</p></div>
<div class="m2"><p>چون شود برق عنان گریه مستانه عشق</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رفته ام دررگ و در ریشه دیوار چوکاه</p></div>
<div class="m2"><p>نتوان کرد مرا دور ز کاشانه عشق</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چشم زخمی به سبکدستی آن کس مرساد</p></div>
<div class="m2"><p>که ز خاکستر دل ریخت پی خانه عشق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خون دل نیز شریک است درین آمیزش</p></div>
<div class="m2"><p>نه همین اشک بود گوهر یکدانه عشق</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سر پیچیدن دستار ندارم صائب</p></div>
<div class="m2"><p>می روم گرد سر وضع غریبانه عشق</p></div></div>