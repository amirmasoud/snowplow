---
title: >-
    غزل شمارهٔ ۵۱۳۸
---
# غزل شمارهٔ ۵۱۳۸

<div class="b" id="bn1"><div class="m1"><p>دل مست حیرت و سر پرشور درسماع</p></div>
<div class="m2"><p>موسی به خواب بیخودی وطور درسماع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلقی به یکدیگر کف افسوس می زنند</p></div>
<div class="m2"><p>خون ازنشاط دررگ منصوردرسماع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جایی که ریگ رقص روانی نمی کند</p></div>
<div class="m2"><p>مجنون ساده لوح کند شور درسماع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش باده ای است عشق که چندین هزاربط</p></div>
<div class="m2"><p>آمد به بال این می پرزور درسماع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر سبز باد هند که از آرمیدگی</p></div>
<div class="m2"><p>درزیر پای فیل بود مور درسماع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صائب زشور فکر توآمد به زیر خاک</p></div>
<div class="m2"><p>مرغ دل امیدی و شاپور درسماع</p></div></div>