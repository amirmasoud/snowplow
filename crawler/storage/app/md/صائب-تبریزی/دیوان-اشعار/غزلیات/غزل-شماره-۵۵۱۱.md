---
title: >-
    غزل شمارهٔ ۵۵۱۱
---
# غزل شمارهٔ ۵۵۱۱

<div class="b" id="bn1"><div class="m1"><p>برآن می داردم همت که از افغان دهن بندم</p></div>
<div class="m2"><p>ز سنگ سرمه سدی پیش یأجوج سخن بندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرفتم نیست در پیراهن من چاک رسوایی</p></div>
<div class="m2"><p>ز مشت خون خود چون گرگ تهمت را دهن بندم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز جوی شیر روشن ساخت راه قصر شیرین را</p></div>
<div class="m2"><p>کمر چون تیشه می خواهم به خون کوهکن بندم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به نامم خاتم شه در غریبی خانه می سازد</p></div>
<div class="m2"><p>چرا دل چون عقیق از ساده لوحی بر یمن بندم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز چشم زخم کثرت دور با خود خلوتی دارم</p></div>
<div class="m2"><p>که در بر روی ماه مصر و بوی پیرهن بندم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به این افسره طبعان صحبت من در نمی گیرد</p></div>
<div class="m2"><p>اگر چون شمع آتش بر زبان خویشتن بندم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه آسان است مروارید را یاقوت گرداندن</p></div>
<div class="m2"><p>چه خونها می خورم تا رنگ بر روی سخن بندم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از بس ترسیده چشم صائب از قرب گرانجانان</p></div>
<div class="m2"><p>نسیم مصر اگر آید در بیت الحزن بندم</p></div></div>