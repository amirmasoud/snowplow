---
title: >-
    غزل شمارهٔ ۲۰۸۷
---
# غزل شمارهٔ ۲۰۸۷

<div class="b" id="bn1"><div class="m1"><p>از داغ، روشنی جگر پاره پاره یافت</p></div>
<div class="m2"><p>جان این زمین سوخته از یک شراره یافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد تازه داغ غیرت خونین دلان عشق</p></div>
<div class="m2"><p>تا لاله زین چمن جگر پاره پاره یافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گردید از میانجی گوش و زبان خلاص</p></div>
<div class="m2"><p>ز اهل نظر کسی که زبان اشاره یافت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آسوده از حساب به روز شمار شد</p></div>
<div class="m2"><p>اینجا کسی که درد و غم بی شماره یافت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در وادیی که شوق بود میر کاروان</p></div>
<div class="m2"><p>گرد پیاده را نتواند سواره یافت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست از طلب کشیدم، تا طفل شیرخوار</p></div>
<div class="m2"><p>با دست بسته رزق خود از گاهواره یافت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زان دم که دل عنان توکل ز دست داد</p></div>
<div class="m2"><p>در کار خویش صد گره از استخاره یافت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آب عقیق یار ز خط آرمیده شد</p></div>
<div class="m2"><p>این گوهر از غبار یتیمی کناره یافت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فیضی که ناخدا دل شب یافت از نجوم</p></div>
<div class="m2"><p>دل در سواد زلف ازان گوشواره یافت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ابرام می کند به در بسته کار سنگ</p></div>
<div class="m2"><p>آهن ز روی سخت، شررها ز خاره یافت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شمع از نفس درازی، شب را بسر نبرد</p></div>
<div class="m2"><p>صبح از دم شمرده حیات دوباره یافت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ره می برد به آن دهن تنگ، بی سخن</p></div>
<div class="m2"><p>در آفتاب هر که تواند ستاره یافت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صائب مرا بس است ز خوان وصال او</p></div>
<div class="m2"><p>این لذتی که دیده من از نظاره یافت</p></div></div>