---
title: >-
    غزل شمارهٔ ۱۴۰۲
---
# غزل شمارهٔ ۱۴۰۲

<div class="b" id="bn1"><div class="m1"><p>دوش مجلس از زبان شکوه ام در می گرفت</p></div>
<div class="m2"><p>کاش این شمع پریشان را کسی سر می گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کوه تمکین و سبکساری کنون هم پله اند</p></div>
<div class="m2"><p>رفت آن موسم که بحر عشق لنگر می گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده ابلیس اگر می داشت نور معرفت</p></div>
<div class="m2"><p>خاک را از چهره چون خورشید در زر می گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن که می زد از نصیحت آب بر آتش مر</p></div>
<div class="m2"><p>کاش اول پرده از رخسار او برمی گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم خود را داده بود از آب حیوان خضر آب</p></div>
<div class="m2"><p>تا غرور آیینه از دست سکندر می گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با ضعیفان سختگیریهای چرخ امروز نیست</p></div>
<div class="m2"><p>دایم این بیدادگر نخجیر لاغر می گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شرم اگر بیرون در می بود و می در اندرون</p></div>
<div class="m2"><p>صحبت ما و تو امشب رنگ دیگر می گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب از بزمی که من افسرده بیرون آمدم</p></div>
<div class="m2"><p>پنبه مینا ز روی گرم می در می گرفت</p></div></div>