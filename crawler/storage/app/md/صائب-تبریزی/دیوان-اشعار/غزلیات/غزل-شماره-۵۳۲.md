---
title: >-
    غزل شمارهٔ ۵۳۲
---
# غزل شمارهٔ ۵۳۲

<div class="b" id="bn1"><div class="m1"><p>تا دل از روی تو شد مطلع انوار مرا</p></div>
<div class="m2"><p>چشم خورشید شود خیره ز رخسار مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی نصیب است ز من دیده ظاهربینان</p></div>
<div class="m2"><p>می توان یافت به نور دل بیدار مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هست بر خاطر من دیدن غمخوار گران</p></div>
<div class="m2"><p>ورنه کوه غم او نیست به دل بار مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در سیه رویی ازان گشته ام انگشت نما</p></div>
<div class="m2"><p>که سر آمد چو قلم عمر به گفتار مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون کنم پیش طبیبان دگر دست دراز؟</p></div>
<div class="m2"><p>ناز عیسی است گران بر دل بیمار مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از کف دست اگر موی برون می آید</p></div>
<div class="m2"><p>می رسد دست به موی کمر یار مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرو آزاد ترا دیده بدبین مرساد</p></div>
<div class="m2"><p>که به هر جلوه کند تازه گرفتار مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حلقه ای می زنم از دور بر آن در صائب</p></div>
<div class="m2"><p>باغبان گر ندهد راه به گلزار مرا</p></div></div>