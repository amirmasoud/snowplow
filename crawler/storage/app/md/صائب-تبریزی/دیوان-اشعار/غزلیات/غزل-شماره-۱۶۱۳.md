---
title: >-
    غزل شمارهٔ ۱۶۱۳
---
# غزل شمارهٔ ۱۶۱۳

<div class="b" id="bn1"><div class="m1"><p>در غریبی دلم از یاد وطن خالی نیست</p></div>
<div class="m2"><p>غنچه هر جا بود از فکر چمن خالی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روح در جسم من از شوق ندارد آرام</p></div>
<div class="m2"><p>در گهر آب من از قطره زدن خالی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون سر زلف همان حلقه بیرون درم</p></div>
<div class="m2"><p>گر چه یک مویم ازان عهد شکن خالی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم بد را به لب خشک ز خود دور کنم</p></div>
<div class="m2"><p>ورنه از خون جگر ساغر من خالی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سراپای تو هر گوشه که آید به نظر</p></div>
<div class="m2"><p>از شکر خنده چو آن کنج دهن خالی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسن بیرنگ به هر کس ننماید خود را</p></div>
<div class="m2"><p>ورنه در فصل خزان نیز چمن خالی نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر اندیشه معشوق هم آغوش بود</p></div>
<div class="m2"><p>سر کشیدن به گریبان کفن خالی نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لب هر جام درین بزم لب منصورست</p></div>
<div class="m2"><p>گر چه این معرکه از دار و رسن خالی نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>داغ در زیر سیاهی بود از چشم ایمن</p></div>
<div class="m2"><p>من و آن باغ که از زاغ و زغن خالی نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مصر را شوق وطن کرد به یوسف زندان</p></div>
<div class="m2"><p>گر چه از چاه حسد خاک وطن خالی نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جوی خشکی است، چو ساقی نبود، شیشه و جام</p></div>
<div class="m2"><p>از گل و سرو چه حاصل که چمن خالی نیست؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جز سخن مغز دگر نیست درین عالم پوچ</p></div>
<div class="m2"><p>این چه پوچ است که گویند سخن خالی نیست؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لاله طور تجلی است دل من صائب</p></div>
<div class="m2"><p>هرگز از داغ جنون کاسه من خالی نیست</p></div></div>