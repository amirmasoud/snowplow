---
title: >-
    غزل شمارهٔ ۲۴۶
---
# غزل شمارهٔ ۲۴۶

<div class="b" id="bn1"><div class="m1"><p>در گذر ای آسمان از وادی آزار ما</p></div>
<div class="m2"><p>شیشه خود را مزن بر سنگ بی زنهار ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناتوانانیم، اما کار چون بر سر فتد</p></div>
<div class="m2"><p>دود برمی آورد از مغز آتش خار ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لشکر خواب گران را قطره آبی بس است</p></div>
<div class="m2"><p>برحذر باش از شبیخون دل بیدار ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست از مردی، رساندن خانه ما را به آب</p></div>
<div class="m2"><p>عالمی آسوده اند از سایه دیوار ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صائب از بالین ما دشمن چسان خوشدل رود؟</p></div>
<div class="m2"><p>سنگ را در گریه آرد ناله بیمار ما</p></div></div>