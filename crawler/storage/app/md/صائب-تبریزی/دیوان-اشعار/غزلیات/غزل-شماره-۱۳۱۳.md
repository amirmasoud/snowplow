---
title: >-
    غزل شمارهٔ ۱۳۱۳
---
# غزل شمارهٔ ۱۳۱۳

<div class="b" id="bn1"><div class="m1"><p>در ریاض آفرینش خاطر آسوده نیست</p></div>
<div class="m2"><p>برگ عیش این چمن جز دست بر هم سوده نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خنده گل می دهد یادی ز آغوش وداع</p></div>
<div class="m2"><p>در بهاران ناله مرغ چمن بیهوده نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه می ریزم ز مژگان اشک گرم، اما چو شمع</p></div>
<div class="m2"><p>در سراپای وجودم یک رگ نگشوده نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تیغ لنگردار، سیلاب گرانسنگ فناست</p></div>
<div class="m2"><p>چشم ما را تاب آن مژگان خواب آلوده نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بوالهوس را آبرویی نیست در درگاه عشق</p></div>
<div class="m2"><p>آستان سرکشان جای جبین سوده نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی گناهی می رود در خون شبنم هر سحر</p></div>
<div class="m2"><p>چهره خورشید بی موجب به خون اندوده نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خون به جای شیر می جوشد ز پستان صبح را</p></div>
<div class="m2"><p>وقت طفلی خوش که در مهد زمین آسوده نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رحمت حق می کند خالی دل از عصیان ما</p></div>
<div class="m2"><p>ابر این دریا به غیر از دامن آلوده نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غنچه تصویر می لرزد به رنگ و بوی خویش</p></div>
<div class="m2"><p>در ریاض آفرینش یک دل آسوده نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می توان خواند از جبین، راز دل عشاق را</p></div>
<div class="m2"><p>در کف اهل قیامت نامه نگشوده نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دست زن در دامن بی حاصلی صائب که نخل</p></div>
<div class="m2"><p>تا ثمر دارد ز سنگ کودکان آسوده نیست</p></div></div>