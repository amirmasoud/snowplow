---
title: >-
    غزل شمارهٔ ۱۳۲۷
---
# غزل شمارهٔ ۱۳۲۷

<div class="b" id="bn1"><div class="m1"><p>تا مرا عشق بلند اقبال در زنجیر داشت</p></div>
<div class="m2"><p>پیچ و تاب من شکوه جوهر شمشیر داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سینه ام هرگز ز داغ گلرخان خالی نبود</p></div>
<div class="m2"><p>این بیابان آتشی دایم ز چشم شیر داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در گلستانی که عمر ما به دلتنگی گذشت</p></div>
<div class="m2"><p>خنده ها در آستین هر غنچه تصویر داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دامن ابر بهاران در فلک می کرد سیر</p></div>
<div class="m2"><p>خار ما بی حاصلان تا دست دامنگیر داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یاد ایامی که از بیتابی مجنون ما</p></div>
<div class="m2"><p>حلقه چشم غزالان ناله زنجیر داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حق اگر بندد دری، ده در گشاید در عوض</p></div>
<div class="m2"><p>طفل بی مادر ز هر انگشت جوی شیر داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیل در ویرانه من داشت صائب گل در آب</p></div>
<div class="m2"><p>در دل من راه تا اندیشه تعمیر داشت</p></div></div>