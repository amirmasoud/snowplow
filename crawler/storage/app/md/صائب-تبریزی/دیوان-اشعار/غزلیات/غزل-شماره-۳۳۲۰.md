---
title: >-
    غزل شمارهٔ ۳۳۲۰
---
# غزل شمارهٔ ۳۳۲۰

<div class="b" id="bn1"><div class="m1"><p>هر کف خاک ز احسان تو جانی دارد</p></div>
<div class="m2"><p>هر حبابی ز محیط تو جهانی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچ قفلی به کلید دگری وا نشود</p></div>
<div class="m2"><p>هر زبان گوشی و هر گوش زبانی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خبر دوری راه از دگران می شنود</p></div>
<div class="m2"><p>هر که چون بیخبری تخت روانی دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جگر ماست ولی نعمت هر جا داغی است</p></div>
<div class="m2"><p>لاله از سفره ما سوخته نانی دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می تواند کسی از خار مغیلان گل چید</p></div>
<div class="m2"><p>که ز هر آبله چشم نگرانی دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم بر روی مه عید گشاید هر شام</p></div>
<div class="m2"><p>هر که از خوان قناعت لب نانی دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رخنه ملک محال است نگیرند شهان</p></div>
<div class="m2"><p>می رسد رزق به هرکس که دهانی دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چرخ دل زنده ز همصحبتی خورشیدست</p></div>
<div class="m2"><p>پیر هرگز نشود هرکه جوانی دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب این آن غزل حافظ شیرین سخن است</p></div>
<div class="m2"><p>کلک ما نیز زبانی و بیانی دارد</p></div></div>