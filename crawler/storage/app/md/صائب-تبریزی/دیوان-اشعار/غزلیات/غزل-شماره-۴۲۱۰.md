---
title: >-
    غزل شمارهٔ ۴۲۱۰
---
# غزل شمارهٔ ۴۲۱۰

<div class="b" id="bn1"><div class="m1"><p>در موج خیز غم دل آزاد نشکند</p></div>
<div class="m2"><p>جوهر طلسم بیضه فولاد نشکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیغ ترا ملاحظه از جان سخت نیست</p></div>
<div class="m2"><p>از کوه قاف بال پریزاد نشکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا می توان شکست پرو بال خویش را</p></div>
<div class="m2"><p>مرغ اسیر ما دل صیاد نشکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گو تخته کن دکان که سرآمد نمی شود</p></div>
<div class="m2"><p>طفلی که تخته بر سر استاد نشکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این می کشد مرا که مبادا ز لاغری</p></div>
<div class="m2"><p>خونم خمار خنجر جلاد نشکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گودم مزن ز خشکی سودا که ناقص است</p></div>
<div class="m2"><p>در هر رگی که نشتر فصاد نشکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این دامنی که زد به کمر کوه بیستون</p></div>
<div class="m2"><p>سخت است تیشه بر سر فرهاد نشکند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر سرکشی مناز که سروی درین چمن</p></div>
<div class="m2"><p>قامت نکرد راست که آزاد نشکند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دستی نشد دراز بر این گرد خوان که نی</p></div>
<div class="m2"><p>در ناخنش قلمرو ایجاد نشکند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کام از جهان مجو که درین صید گاه نیست</p></div>
<div class="m2"><p>مرغی که بیضه بر سر صیاد نشکند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ایمن نیم ز تنگی دل بر خیال او</p></div>
<div class="m2"><p>از شیشه گر چه بال پریزاد نشکند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صائب جهان فروز نگردد چو آفتاب</p></div>
<div class="m2"><p>رنگی که از تپانچه استاد نشکند</p></div></div>