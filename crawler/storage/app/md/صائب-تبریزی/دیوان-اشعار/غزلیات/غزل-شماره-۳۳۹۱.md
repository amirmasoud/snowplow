---
title: >-
    غزل شمارهٔ ۳۳۹۱
---
# غزل شمارهٔ ۳۳۹۱

<div class="b" id="bn1"><div class="m1"><p>اشک را دیده من گوهر غلطان سازد</p></div>
<div class="m2"><p>آه را سینه من سنبل و ریحان سازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست چون تیغ دودم در نظر غیرت من</p></div>
<div class="m2"><p>حسن را آینه هر چند دو چندان سازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گریه از جا نبرد حسن گران تمکین را</p></div>
<div class="m2"><p>سرو را آب محال است خرامان سازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرده پوشی طمع از پرده دران نتوان داشت</p></div>
<div class="m2"><p>چون کسی عیب خود از آینه پنهان سازد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شور محشر که کند زیر و زبر عالم را</p></div>
<div class="m2"><p>وقت ما را نتوانست پریشان سازد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عیب خود صاف ضمیران نتوانند نهفت</p></div>
<div class="m2"><p>مرده را آب محال است که پنهان سازد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زرد رویی نکشد روز قیامت صائب</p></div>
<div class="m2"><p>هر که با خون دل از نعمت الوان سازد</p></div></div>