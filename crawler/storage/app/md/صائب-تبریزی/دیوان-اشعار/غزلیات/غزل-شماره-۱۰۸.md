---
title: >-
    غزل شمارهٔ ۱۰۸
---
# غزل شمارهٔ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>نعل در آتش نهد دیوانه من سنگ را</p></div>
<div class="m2"><p>شعله جواله سازد بی فلاخن سنگ را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخت جانانند باغ دلگشای یکدگر</p></div>
<div class="m2"><p>می کند گلریز، روی سخت آهن سنگ را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نفس سرکش را دل روشن به اصلاح آورد</p></div>
<div class="m2"><p>نرم از آتش می شود رگ های گردن سنگ را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سهل باشد گر ز آتشدستی فرهاد من</p></div>
<div class="m2"><p>هر رگی گردد چو تار شمع، روشن سنگ را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواب سنگین شد سبک از شوخی مژگان او</p></div>
<div class="m2"><p>شهپر پرواز می گردد فلاخن سنگ را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر شکیبایی مناز ای دل که آن مژگان شوخ</p></div>
<div class="m2"><p>خانه زنبور می سازد به سوزن سنگ را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دامن دشتی اگر می بود چون مجنون مرا</p></div>
<div class="m2"><p>بهر طفلان جمع می کردم به دامن سنگ را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این زمان بی برگ و بارم، ور نه از جوش ثمر</p></div>
<div class="m2"><p>منت دست نوازش بود بر من سنگ را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما به زور می درین میخانه از خود می رویم</p></div>
<div class="m2"><p>می شود سیلاب، گاهی پای رفتن سنگ را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتگو با سخت رویان زحمت خود دادن است</p></div>
<div class="m2"><p>می کشد آزار، دست از دل فشردن سنگ را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بی بری دارد مرا از حلقه اطفال دور</p></div>
<div class="m2"><p>ورنه گیرد از هوا دیوانه من سنگ را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می توان سنگین دلان را چین قهر از جبهه برد</p></div>
<div class="m2"><p>نقش اگر بتوان به دست از دل ستردن سنگ را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر که دارد عذرخواهی، بر گنه باشد دلیر</p></div>
<div class="m2"><p>مومیایی می دهد دل در شکستن سنگ را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شد یکی صد غفلت من صائب از قد دوتا</p></div>
<div class="m2"><p>خواب سنگین شد در آغوش فلاخن سنگ را</p></div></div>