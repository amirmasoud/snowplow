---
title: >-
    غزل شمارهٔ ۱۶۵۵
---
# غزل شمارهٔ ۱۶۵۵

<div class="b" id="bn1"><div class="m1"><p>شکستگی دل از دیده ترم پیداست</p></div>
<div class="m2"><p>به سنگ خوردن مینا ز ساغرم پیداست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دهان زخم بود ترجمان تیغ خموش</p></div>
<div class="m2"><p>ز جوی شیر چو فرهاد جوهرم پیداست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز ناتوانی من خامه می گزد انگشت</p></div>
<div class="m2"><p>که رگ ز صفحه تن همچو مسطرم پیداست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشد نهفته ز تن داغهای پنهانم</p></div>
<div class="m2"><p>همان ز گرد، سیاهی لشکرم پیداست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان که شمع نماید ز پرده فانوس</p></div>
<div class="m2"><p>برون ز نه صدف چرخ گوهرم پیداست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو بوریاست ز پهلوی خشک بستر من</p></div>
<div class="m2"><p>قماش خواب ز نرمی بسترم پیداست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به غیر موی سر خود مرا کلاهی نیست</p></div>
<div class="m2"><p>گذشتن از سر دنیا ز افسرم پیداست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به حلم دوست دلیل است خواب غفلت من</p></div>
<div class="m2"><p>بهم نخوردن دریا ز لنگرم پیداست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر چه بحر گرانمایه است دایه من</p></div>
<div class="m2"><p>همان غبار یتیمی ز گوهرم پیداست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز کاسه سر منصور باده می نوشم</p></div>
<div class="m2"><p>عیار حوصله من ز ساغرم پیداست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز گرد خوان فلک زله ای که من بستم</p></div>
<div class="m2"><p>چو ماه عید ز پهلوی لاغرم پیداست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نهان چگونه کنم فیض کنج عزلت را؟</p></div>
<div class="m2"><p>که فتح باب ز نگشودن درم پیداست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ستاره سوخته ای همچو من ندارد عشق</p></div>
<div class="m2"><p>که روز روشن از افلاک اخترم پیداست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>توان ز گریه من یافت درد من صائب</p></div>
<div class="m2"><p>شکوه بحر ز سیمای گوهرم پیداست</p></div></div>