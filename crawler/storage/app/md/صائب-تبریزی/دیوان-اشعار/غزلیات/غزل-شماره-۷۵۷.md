---
title: >-
    غزل شمارهٔ ۷۵۷
---
# غزل شمارهٔ ۷۵۷

<div class="b" id="bn1"><div class="m1"><p>ای جبهه تو آینه سرنوشت ما</p></div>
<div class="m2"><p>روشن چو آفتاب به تو خوب و زشت ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در پله نشیب به قارون برابرست</p></div>
<div class="m2"><p>میزان ز بس گرانی اعمال زشت ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما را به شکوه تنگی عالم نیاورد</p></div>
<div class="m2"><p>خلق گشاده است فضای بهشت ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آب خضر دانه ما سبز گشته است</p></div>
<div class="m2"><p>دست آزمای برق فنا نیست کشت ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با آب شور کعبه نگردیم هم نمک</p></div>
<div class="m2"><p>تا یک دم آب تلخ بود در کنشت ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون آفتاب اگر سر ما بگذرد ز چرخ</p></div>
<div class="m2"><p>افتادگی برون نرود از سرشت ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای ابر رحمت این همه استادگی چرا؟</p></div>
<div class="m2"><p>وقت است برق ریشه دواند به کشت ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نور و صفا در آب و گل ما سرشته اند</p></div>
<div class="m2"><p>بر روی آفتاب کشد تیغ، خشت ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب کشید شعله ز دل داغ تازه ای</p></div>
<div class="m2"><p>گل کرد شمع لاله ز دامان کشت ما</p></div></div>