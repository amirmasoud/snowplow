---
title: >-
    غزل شمارهٔ ۴۷۵۱
---
# غزل شمارهٔ ۴۷۵۱

<div class="b" id="bn1"><div class="m1"><p>هر چند ترا دیده بدکرد زمن دور</p></div>
<div class="m2"><p>درکنج قفس نیست زگل مرغ چمن دور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با تلخی غربت چه کند مصرشکرخیز؟</p></div>
<div class="m2"><p>از بهرعزیزی نتوان شدزوطن دور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درکوثر اگرغوطه زند تشنه برآید</p></div>
<div class="m2"><p>هرسوخته جانی که شد ازچاه ذقن دور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درخانه نشینی نتوان نام برآورد</p></div>
<div class="m2"><p>گمنام عقیقی که نگرددزوطن دور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر مورکند تکیه گه از دست سلیمان</p></div>
<div class="m2"><p>بسیار مدانید زاعجاز سخن دور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد موی سرش پاک سفید از غم هجران</p></div>
<div class="m2"><p>تانافه شد از ناف غزالان ختن دور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوردند به کف آب زچاه اهل توکل</p></div>
<div class="m2"><p>برتشنه ما آب شد از دلو رسن دور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر چند بود درته پیراهن من یار</p></div>
<div class="m2"><p>چون جامه فانوسم ازان سیم بدن دور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب زنظر بازی این تازه جوانان</p></div>
<div class="m2"><p>از دل نشود دوستی یارکهن دور</p></div></div>