---
title: >-
    غزل شمارهٔ ۲۵۳۳
---
# غزل شمارهٔ ۲۵۳۳

<div class="b" id="bn1"><div class="m1"><p>کی زلیخا را منور بوی پیراهن کند؟</p></div>
<div class="m2"><p>شمع هیهات است پای خویش را روشن کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوختم ز افسردگیها، آتشین رویی کجاست؟</p></div>
<div class="m2"><p>کز نگاه گرم شمع کشته را روشن کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم بینا شهپر پرواز باشد روح را</p></div>
<div class="m2"><p>از گریبان مسیحا سر برون سوزن کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرده غفلت شود از نرمی بستر زیاد</p></div>
<div class="m2"><p>پای خواب آلود را بیدار کی دامن کند؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر برومندان مبر غیرت که دهقان فلک</p></div>
<div class="m2"><p>آخر این گوساله ها را گاو در خرمن کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از تحمل می توان مغلوب کردن خصم را</p></div>
<div class="m2"><p>زیردست از چرب نرمی آب را روغن کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صبر کن صائب به درد و داغ چون مردان که عشق</p></div>
<div class="m2"><p>بر سمندر آتش سوزنده را گلشن کند</p></div></div>