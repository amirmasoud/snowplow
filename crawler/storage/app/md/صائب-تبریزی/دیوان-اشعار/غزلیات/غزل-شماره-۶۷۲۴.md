---
title: >-
    غزل شمارهٔ ۶۷۲۴
---
# غزل شمارهٔ ۶۷۲۴

<div class="b" id="bn1"><div class="m1"><p>چند اسباب اقامت جمع در عالم کنی؟</p></div>
<div class="m2"><p>ریشه تا کی در زمین عاریت محکم کنی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند در پیری ز فوت مطلب دنیای دون</p></div>
<div class="m2"><p>قامت خم گشته خود حلقه ماتم کنی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فکر آب و نان برآورد از حضور دل ترا</p></div>
<div class="m2"><p>ترک جنت بهر گندم چند چون آدم کنی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می شود بی منت مرهم چو داغ لاله خشک</p></div>
<div class="m2"><p>داغ خود را گر ز خون گرم خود مرهم کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می توانی همچو عیسی آسمان پرواز شد</p></div>
<div class="m2"><p>سوزن خود گر جدا از رشته مریم کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر کنی گردآوری خود را درین بستانسرا</p></div>
<div class="m2"><p>سر برون از روزن خورشید چون شبنم کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خون دل چون آب حیوان بر تو گردد خوشگوار</p></div>
<div class="m2"><p>با سفال خود قناعت گر ز جام جم کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آستانت بوسه گاه راست کیشان می شود</p></div>
<div class="m2"><p>از عبادت چون کمان گر قامت خود خم کنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر دل خود را به تیغ آه سازی چاک چاک</p></div>
<div class="m2"><p>پنجه در سرپنجه آن زلف خم در خم کنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جز شکار دل که بوی مشک می آید ازو</p></div>
<div class="m2"><p>بوی خون آید ز هر صیدی که در عالم کنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در گریبان تنک ظرف اخگری افکنده ای</p></div>
<div class="m2"><p>هر که را جز دل به راز خویشتن محرم کنی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می شوی از شش جهت روشندلان را قبله گاه</p></div>
<div class="m2"><p>صلح اگر چون کعبه با شورابه زمزم کنی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>می کنی پیدا به حرف و صوت دشمن بهر خود</p></div>
<div class="m2"><p>از ره برهان و حجت هر که را ملزم کنی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هیچ کس انگشت بر حرف تو نتواند نهاد</p></div>
<div class="m2"><p>گر به نقش راست از چپ صلح چون خاتم کنی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کشف گردد بر تو صائب جمله اسرار جهان</p></div>
<div class="m2"><p>کاسه زانوی خود را گر تو جام جم کنی</p></div></div>