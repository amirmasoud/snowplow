---
title: >-
    غزل شمارهٔ ۲۹۰۳
---
# غزل شمارهٔ ۲۹۰۳

<div class="b" id="bn1"><div class="m1"><p>مغیلان پای نازک طینتان را در حنا دارد</p></div>
<div class="m2"><p>چه غم دارد زخار آن کس که آتش زیر پا دارد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکش رو در هم از حکم قضا، ورمی کشی در هم</p></div>
<div class="m2"><p>چه پروا آتش از چین جبین بوریا دارد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشان مردمی در مردم عالم نمی یابم</p></div>
<div class="m2"><p>اگر دارد وجودی مردمی، مردم گیا دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درین صحرای وحشت خضر دلسوزی نمی بینم</p></div>
<div class="m2"><p>مگر هم گرم رفتاری چراغم پیش پا دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمین خاکساری سایه گل بر نمی تابد</p></div>
<div class="m2"><p>صبا با آن سبکروحی درین ره نقش پا دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درخت رز به صد رعنایی اول برون آمد</p></div>
<div class="m2"><p>زپا هرگز نیفتد هر که دستی در سخا دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به خاموشی زمکر دشمن بدرگ مشو ایمن</p></div>
<div class="m2"><p>چو توسن گوش خواباند لگدها در قفا دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زپیش دیده صائب چنین دامن کشان مگذر</p></div>
<div class="m2"><p>که چون بند قبا صد جا سر بند ترا دارد</p></div></div>