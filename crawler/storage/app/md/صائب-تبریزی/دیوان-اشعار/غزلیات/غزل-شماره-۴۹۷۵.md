---
title: >-
    غزل شمارهٔ ۴۹۷۵
---
# غزل شمارهٔ ۴۹۷۵

<div class="b" id="bn1"><div class="m1"><p>خواب چشم تو که ازناز بود تعبیرش</p></div>
<div class="m2"><p>مژه راسبزه خوابیده کند تقریرش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسمل او به سر جان نتواند لرزید</p></div>
<div class="m2"><p>بس که ازلنگر نازست گران شمشیرش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این چه مژگان بلندست که نگشوده ،شود</p></div>
<div class="m2"><p>درکمانخانه ز نخجیر ترازو،تیرش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر از سلسله زلف رهایی یابد</p></div>
<div class="m2"><p>چه کند دل به غبار خط دامنگیرش ؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرکه را شوخی چشم تو بیابانی کرد</p></div>
<div class="m2"><p>حلقه چشم غزالان نکند زنجیرش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسن مغرور چو افتاد نسازد با خود</p></div>
<div class="m2"><p>چه عجب خانه آیینه کند دلگیرش؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بادل بیخبر، اظهار ندامت ز گناه</p></div>
<div class="m2"><p>همچو خوابی است که درخواب کنی تعبیرش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حذر از آه جگردوز کهنسالان کن</p></div>
<div class="m2"><p>کاین کمانی است که برخاک نیفتد تیرش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پای ویرانه هر کس که فرو رفت به گنج</p></div>
<div class="m2"><p>نیست صائب غم معمار و سر تعمیرش</p></div></div>