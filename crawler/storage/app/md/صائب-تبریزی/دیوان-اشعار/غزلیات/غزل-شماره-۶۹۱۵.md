---
title: >-
    غزل شمارهٔ ۶۹۱۵
---
# غزل شمارهٔ ۶۹۱۵

<div class="b" id="bn1"><div class="m1"><p>ای حسن خط ز مصحف روی تو آیتی</p></div>
<div class="m2"><p>از خوبی تو قصه یوسف حکایتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درد کهن به پرسش رسمی نمی رود</p></div>
<div class="m2"><p>کی می دهد تسلی عاشق عنایتی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پاس ادب عنان سخن را کشیده داشت</p></div>
<div class="m2"><p>روزی که داشت درددل ما نهایتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما را زبان شکوه چو صائب نداده اند</p></div>
<div class="m2"><p>می داشت کاش درد دل ما نهایتی</p></div></div>