---
title: >-
    غزل شمارهٔ ۳۴۳۸
---
# غزل شمارهٔ ۳۴۳۸

<div class="b" id="bn1"><div class="m1"><p>چشم ناقص گهران بر زر و زیور باشد</p></div>
<div class="m2"><p>زینت ساده دلان پاکی گوهر باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرده چشم خدابین نشود خودبینی</p></div>
<div class="m2"><p>مرد را آینه زندان سکندر باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خود حسابان نگذارند به فردا کاری</p></div>
<div class="m2"><p>عید این طایفه روزی است که محشر باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گشت در سنگ ملامت به تن زارم پنهان</p></div>
<div class="m2"><p>رشته شیرازه جمعیت گوهر باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جلوه شاهد غیبی به تو رو ننماید</p></div>
<div class="m2"><p>خانه چشم تو چندان که مصور باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غم افسر نخورند اهل خرابات مغان</p></div>
<div class="m2"><p>سر گران چون ز می ناب شد افسر باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهل مسجد ز خرابات سیه مست ترند</p></div>
<div class="m2"><p>گردش سبحه در او گردش ساغر باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمع و پروانه به دلگرمی هم می سوزند</p></div>
<div class="m2"><p>شعله خواهش ما نیست که یک سر باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هوس گلشن فردوس ندارد صائب</p></div>
<div class="m2"><p>هر که را گوشه میخانه میسر باشد</p></div></div>