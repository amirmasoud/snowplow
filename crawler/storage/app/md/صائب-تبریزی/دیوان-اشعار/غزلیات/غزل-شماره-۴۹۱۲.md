---
title: >-
    غزل شمارهٔ ۴۹۱۲
---
# غزل شمارهٔ ۴۹۱۲

<div class="b" id="bn1"><div class="m1"><p>کاش می‌دیدی به چشم عاشقان رخسار خویش</p></div>
<div class="m2"><p>تا دریغ از چشم خود می‌داشتی دیدار خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر به دل‌ها داده‌ای مژگان خواب‌آلود را</p></div>
<div class="m2"><p>برنمی‌آیی مگر با تیغ لنگردار خویش؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حسن عالم‌سوز را مشاطه‌ای در کار نیست</p></div>
<div class="m2"><p>گرم دارد از فروغ خود گهر بازار خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای که می‌جویی گشاد کار خود از آسمان</p></div>
<div class="m2"><p>آسمان از ما بود سرگشته‌تر در کار خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شرم دار از غنچه خاموش با چندین زبان</p></div>
<div class="m2"><p>همچو بلبل چند باشی عاشق گفتار خویش؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچ کس را کار یا رب با خودآرایی مباد</p></div>
<div class="m2"><p>گل به خون می‌غلطد از رنگینی دستار خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روزگار برق فرصت خنده واری بیش نیست</p></div>
<div class="m2"><p>مگذران درخواب غفلت دولت بیدار خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در دهانش خاک بادا، نام شکّر گر برد</p></div>
<div class="m2"><p>هرکه بتواند زبان مالید بر دیوار خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برنمی دارد گرانباری ره دور عدم</p></div>
<div class="m2"><p>چون گرانی می‌بری، باری سبک کن بار خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لب به آب تیغ می‌شوید ز شهد زندگی</p></div>
<div class="m2"><p>هرکه چون منصور بیرون می‌دهد اسرار خویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می‌روم چون لغزش مستان به پای بیخودی</p></div>
<div class="m2"><p>تا کجا سر برکنم زین سیر بی‌پرگار خویش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این جواب آن غزل صائب که فرمود اوحدی</p></div>
<div class="m2"><p>مؤمن و سجاده خود، کافر و زنار خویش</p></div></div>