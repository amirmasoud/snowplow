---
title: >-
    غزل شمارهٔ ۵۶۰۸
---
# غزل شمارهٔ ۵۶۰۸

<div class="b" id="bn1"><div class="m1"><p>گر چه از مشق جنون خواب پریشان شده ام</p></div>
<div class="m2"><p>خط آزادی اطفال دبستان شده ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود فروشی است گران بر دل آزاده من</p></div>
<div class="m2"><p>راضی از جوش خریدار به زندان شده ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منم آن کشتی بی حوصله در بحر وجود</p></div>
<div class="m2"><p>کز گرانباری خود تشنه طوفان شده ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منت ابر بهارست مرا بر خس و خار</p></div>
<div class="m2"><p>تا درین بادیه از آبله پایان شده ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شهر افسرده تر از خاک فراموشان است</p></div>
<div class="m2"><p>تا من از شهر چو مجنون به بیابان شده ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غوطه ها در عرق خود زده ام چون گل صبح</p></div>
<div class="m2"><p>تا سرافراز به یک زخم نمایان شده ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آب را سرو اگر سر به گلستان داده است</p></div>
<div class="m2"><p>من زمین گیر از آن سرو خرامان شده ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی تأمل دل سنگین تو می گردد آب</p></div>
<div class="m2"><p>گر بدانی چه قدر تشنه باران شده ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل سیلاب به ویرانی من می سوزد</p></div>
<div class="m2"><p>بس که در حسرت تعمیر تو ویران شده ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مشق نظاره روی تو مرا منظورست</p></div>
<div class="m2"><p>اگر از جمله خورشید پرستان شده ام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از کلف چهره من چون مه کنعان پاک است</p></div>
<div class="m2"><p>رو سیاه از اثر سیلی اخوان شده ام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مژه در چشم ترم پنجه مرجان شده است</p></div>
<div class="m2"><p>تا نظر باز به آن سیب زنخدان شده ام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>می گزم در حرم وصل ز محرومی دست</p></div>
<div class="m2"><p>خشک در بحر چو سرپنجه مرجان شده ام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به زبان آمده صائب در و دیوار به من</p></div>
<div class="m2"><p>تا سخنگوی و سخنساز و سخندان شده ام</p></div></div>