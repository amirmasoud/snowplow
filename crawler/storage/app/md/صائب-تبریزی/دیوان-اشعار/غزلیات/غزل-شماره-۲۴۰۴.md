---
title: >-
    غزل شمارهٔ ۲۴۰۴
---
# غزل شمارهٔ ۲۴۰۴

<div class="b" id="bn1"><div class="m1"><p>از عرق آتش به جانم آن گل سیراب زد</p></div>
<div class="m2"><p>باز آن آیینه رو نقشی عجب بر آب زد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمع من امشب کجا بودی، که بر یادرخت</p></div>
<div class="m2"><p>تا سحر پروانه من سینه بر مهتاب زد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرد خجلت از دل بیرحم قاتل می فشاند</p></div>
<div class="m2"><p>دست و پایی زیر تیغش گر دل بیتاب زد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواب ناز گل گرانتر شد زبخت بلبلان</p></div>
<div class="m2"><p>هر قدر شبنم به رخسار گلستان آب زد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غم به سر وقت من از روشندلیها اوفتاد</p></div>
<div class="m2"><p>دزد بر گنجینه ام زین گوهر شب تاب زد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مهر بر لب زن که از یک خنده بیجا که کرد</p></div>
<div class="m2"><p>غوطه در خون شفق خورشید عالمتاب زد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زنگ می گیرد ز آب زندگی آیینه اش</p></div>
<div class="m2"><p>کی سکندر می تواند چون خضر بر آب زد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ابر چون گردد طرف با من، که سیل اشک من</p></div>
<div class="m2"><p>بحر را مهر خموشی بر لب از گرداب زد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سختی دل از عبادت کرد رو گردان مرا</p></div>
<div class="m2"><p>چند بتوان سنگ بر پیشانی محراب زد؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رهنوردی را که شد صدق عزیمت خضر راه</p></div>
<div class="m2"><p>در میان راه در دامان منزل خواب زد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیست چشم عاقبت بین جوشن تدبیر را</p></div>
<div class="m2"><p>خویش را ماهی زحرص طعمه بر قلاب زد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قطع شد در یک نفس راه هزاران ساله اش</p></div>
<div class="m2"><p>هر که صائب پشت پا بر عالم اسباب زد</p></div></div>