---
title: >-
    غزل شمارهٔ ۶۳۳۰
---
# غزل شمارهٔ ۶۳۳۰

<div class="b" id="bn1"><div class="m1"><p>مرا ز لاله چراغ نظر شود روشن</p></div>
<div class="m2"><p>ز قرب سوخته جانان شرر شود روشن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو آتش جگر لعل، بی زوال بود</p></div>
<div class="m2"><p>چراغ هر که به خون جگر شود روشن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بس گرفته ز نادیدنی شده است دلم</p></div>
<div class="m2"><p>ز زنگ، آینه ام بیشتر شود روشن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز حرف سرد دل ما چو غنچه بگشاید</p></div>
<div class="m2"><p>چراغ ما به نسیم سحر شود روشن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلی که تیره ز اوضاع روزگار شده است</p></div>
<div class="m2"><p>در آفتاب قیامت مگر شود روشن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به گرمخونی من خسته ای ندارد عشق</p></div>
<div class="m2"><p>چو شمع از رگ من نیشتر شود روشن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گره ز کار دل من شود به آبله باز</p></div>
<div class="m2"><p>چنان که چشم صدف از گهر شود روشن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نکرد گرمی پرواز بی پر و بالم</p></div>
<div class="m2"><p>کجا ز شمع مرا بال و پر شود روشن؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درین محیط عنان را کشیده دار چو موج</p></div>
<div class="m2"><p>که از استادگی آب گهر شود روشن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چراغ هر که ز دلهای گرم افروزد</p></div>
<div class="m2"><p>ز آستین صبا بیشتر شود روشن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز رشک حسن گلوسوز یار نیست بعید</p></div>
<div class="m2"><p>چو شمع سبز اگر نیشکر شود روشن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز عمر قسمت ما نیست جز زمان وداع</p></div>
<div class="m2"><p>چو آن چراغ که وقت سحر شود روشن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نرفت تیرگی از دل به سعی ما صائب</p></div>
<div class="m2"><p>مگر ز پرتو اهل نظر شود روشن</p></div></div>