---
title: >-
    غزل شمارهٔ ۴۳۳۷
---
# غزل شمارهٔ ۴۳۳۷

<div class="b" id="bn1"><div class="m1"><p>از گردش افلاک کجا دل گله دارد</p></div>
<div class="m2"><p>این خانه ویران چه غم زلزله دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند شکستن پر و بالی است گهر را</p></div>
<div class="m2"><p>یوسف ز دل آزاری اخوان گله دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از شکوه همین موج سراپای زبان نیست</p></div>
<div class="m2"><p>دریا ز صدف هم دل پر آبله دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ابلیس کند راهزنی پیشروان را</p></div>
<div class="m2"><p>این گرگ نظر از رمه بر سر گله دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون شمع به معراج رسد کوکب بختش</p></div>
<div class="m2"><p>در بزم جهان هرکه زبان گله دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشتاب در ین ره که نفس سوختگانند</p></div>
<div class="m2"><p>هر لاله دلسوخته کاین مرحله دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از زلف حذر کن که دلش چاک چو شانه است</p></div>
<div class="m2"><p>هر کس که فزون ربط به این سلسله دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن را که بود شوق به تن بار نگردد</p></div>
<div class="m2"><p>ریگی که روان نیست غم راحله دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در سلسله اشک بود گوهر مقصود</p></div>
<div class="m2"><p>گر هست ز یوسف خبر این قافله دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صائب به زر قلب دهد یوسف خود را</p></div>
<div class="m2"><p>پاکیزه کلامی که نظر بر صله دارد</p></div></div>