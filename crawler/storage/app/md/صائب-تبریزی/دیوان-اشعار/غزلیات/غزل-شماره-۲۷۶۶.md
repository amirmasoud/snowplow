---
title: >-
    غزل شمارهٔ ۲۷۶۶
---
# غزل شمارهٔ ۲۷۶۶

<div class="b" id="bn1"><div class="m1"><p>تا خط مشکین لب لعل ترا در بر کشید</p></div>
<div class="m2"><p>موج بیتابی الف بر سینه کوثر کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنگ هستی از دل ما برد ذوق نیستی</p></div>
<div class="m2"><p>عود ما آخر دم خوش در دل مجمر کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این که گرد ماه تابان می نماید هاله نیست</p></div>
<div class="m2"><p>ماه از شرم جمال او سپر بر سر کشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تنگدستی مرگ را در کام شیرین می کند</p></div>
<div class="m2"><p>بید از بی حاصلی بر خویشتن خنجر کشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جوهر از بیطاقتی چون مار می پیچد به خود</p></div>
<div class="m2"><p>زخم گستاخ که شمشیر ترا در بر کشید؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کاسه دریوزه دریا از صدف بر کف گرفت</p></div>
<div class="m2"><p>هر کجا مژگان صائب رشته از گوهر کشید</p></div></div>