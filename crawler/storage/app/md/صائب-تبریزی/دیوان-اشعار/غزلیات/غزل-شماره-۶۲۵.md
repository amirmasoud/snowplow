---
title: >-
    غزل شمارهٔ ۶۲۵
---
# غزل شمارهٔ ۶۲۵

<div class="b" id="bn1"><div class="m1"><p>کجا به دام کشد سایه نهال مرا</p></div>
<div class="m2"><p>شکوفه خنده شیرست از ملال مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فروغ گوهر من از نژاد خورشیدست</p></div>
<div class="m2"><p>به خیرگی نتوان کرد پایمال مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنین که لقمه غم در گلوی من گره است</p></div>
<div class="m2"><p>می حرام بود روزی حلال مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چسان به خنده گشایم دهن، که همچون برق</p></div>
<div class="m2"><p>لب شکفته بود مشرق زوال مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پی شکست من ای سنگ، پر بهم مرسان</p></div>
<div class="m2"><p>که می کند تپش دل شکسته بال مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فریب عشوه دنیا نمی خورم صائب</p></div>
<div class="m2"><p>نظر به حسن مآل است نه به مال مرا</p></div></div>