---
title: >-
    غزل شمارهٔ ۶۵۸۹
---
# غزل شمارهٔ ۶۵۸۹

<div class="b" id="bn1"><div class="m1"><p>شنیدم آه گرمی با تو گستاخانه سرکرده</p></div>
<div class="m2"><p>به جسم نازکت بیماری چشمت اثر کرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل رخسارت از دلسوزی تب آتشین گشته</p></div>
<div class="m2"><p>ملاقات لبت تبخاله را تنگ شکر کرده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خمار خون مظلومان که بی قیدانه می خوردی</p></div>
<div class="m2"><p>سر بی مهریت را آشنای دردسر کرده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رگ دست ترا کز رشته جان است نازکتر</p></div>
<div class="m2"><p>طبیب بی مروت بوسه گاه نیشتر کرده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به امیدی که با نبض تو دستی آشنا سازد</p></div>
<div class="m2"><p>مسیح از خانه خورشید آهنگ سفر کرده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترا صائب اگر پای عیادت هست خوش باشد</p></div>
<div class="m2"><p>که ما را این خبر از هستی خود بی خبر کرده</p></div></div>