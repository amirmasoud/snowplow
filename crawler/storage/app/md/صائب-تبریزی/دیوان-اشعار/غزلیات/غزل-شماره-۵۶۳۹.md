---
title: >-
    غزل شمارهٔ ۵۶۳۹
---
# غزل شمارهٔ ۵۶۳۹

<div class="b" id="bn1"><div class="m1"><p>منم آن سیل که دریا نکند خاموشم</p></div>
<div class="m2"><p>کوه را کشتی طوفان زده سازد جوشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ملامت نکنم شکوه ز بی حوصلگی</p></div>
<div class="m2"><p>سخن تلخ می تلخ بود در گوشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جوش من لنگر آرام نمی داند چیست</p></div>
<div class="m2"><p>نیست چون باده نارس دو سه روزی جوشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خرابات مغان پای بروی نگذارم</p></div>
<div class="m2"><p>تا سبو دست نوازش نکشد بر دوشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم پرکار بتان ساغر خالی است مرا</p></div>
<div class="m2"><p>می گلرنگ چه باشد که رباید هوشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیم ایمن ز پشیمانی بی انصافان</p></div>
<div class="m2"><p>به زر قلب اگر یوسف خود بفروشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه از شمع تهی نیست کنارم شبها</p></div>
<div class="m2"><p>دایم از شرم چو محراب تهی آغوشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست از نوش چو زنبور به جز نیش مرا</p></div>
<div class="m2"><p>اگر چه نه دایره شد شان عسل از نوشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منم آن کودک بدخو که ز ناسازی دل</p></div>
<div class="m2"><p>نتوان کرد به کام دو جهان خاموشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون به پای خم می سر نگذارم صائب؟</p></div>
<div class="m2"><p>من که از باده گلرنگ فزاید هوشم</p></div></div>