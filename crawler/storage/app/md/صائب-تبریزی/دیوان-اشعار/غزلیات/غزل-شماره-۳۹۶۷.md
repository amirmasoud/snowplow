---
title: >-
    غزل شمارهٔ ۳۹۶۷
---
# غزل شمارهٔ ۳۹۶۷

<div class="b" id="bn1"><div class="m1"><p>به چاره سوز محبت ز جان برون نرود</p></div>
<div class="m2"><p>تبی است عشق که از استخوان برون نرود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کنون که شاخ گل از پای تابه سر گوش است</p></div>
<div class="m2"><p>ز ضعف ناله ام از آشیان برون نرود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز قد خم به ره راست دل قدم ننهاد</p></div>
<div class="m2"><p>کجی ز تیر به زور کمان برون نرود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درازدستی رهزن چه می تواند کرد</p></div>
<div class="m2"><p>ز راه راست اگر کاروان برون نرود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه گل توان ز تماشای گلعذاران چید</p></div>
<div class="m2"><p>به گلشنی که ازو باغبان برون نرود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>توان به بوی گل از خار خشک گل چیدن</p></div>
<div class="m2"><p>ز باغ بلبل ما در خزان برون نرود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شده است خاک چمن سرمه ای ز سایه زاغ</p></div>
<div class="m2"><p>چگونه بلبل ازین گلستان برون نرود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همیشه درد به عضو ضعیف می ریزد</p></div>
<div class="m2"><p>که پیچ وتاب ز موی میان برون نرود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به زور درد دل جسته است هیهات است</p></div>
<div class="m2"><p>که تیر آه من از آسمان برون نرود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در آن حریم که صائب سخن شناسی نیست</p></div>
<div class="m2"><p>بهوش باش که حرف از دهان برون نرود</p></div></div>