---
title: >-
    غزل شمارهٔ ۲۹۳۶
---
# غزل شمارهٔ ۲۹۳۶

<div class="b" id="bn1"><div class="m1"><p>سمندر داغها از آتش رخسار او دارد</p></div>
<div class="m2"><p>کجا یاقوت تاب گرمی بازار او دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه تنها نقطه خاک است چون ناقوس ازو نالان</p></div>
<div class="m2"><p>که چرخ از کهکشان هم بر کمر زنار او دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به تیغ کوه خون خویش را چون لاله می ریزد</p></div>
<div class="m2"><p>زبس کبک خرامان خجلت از رفتار او دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زخط سبز دارد طوطیان خوش سخن با خود</p></div>
<div class="m2"><p>کجا پروای طوطی لعل شکربار او دارد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه حاجت شبنم بیگانه گلزار الهی را؟</p></div>
<div class="m2"><p>نظر بر زیب و زینت کی گل رخسار او دارد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر پوشیده چشمی دست گیرد پیر کنعان را</p></div>
<div class="m2"><p>درین گلشن که حسن یوسفی هر خار او دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زسرو خوشخرام او که غافل می تواند شد؟</p></div>
<div class="m2"><p>که دل تعلیم از خود رفتن از رفتار او دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گریبان چاک سازد چون می پرزور مینا را</p></div>
<div class="m2"><p>به خون هر که رغبت غمزه خونخوار او دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نخواهد رخنه زخم نمایان ماند در دلها</p></div>
<div class="m2"><p>اگر این چاشنی شیرینی گفتار او دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل روشن به دست آور اگر دیدار می خواهی</p></div>
<div class="m2"><p>که این آیینه تاب جلوه دیدار او دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا بیکار داند عقل کارافزا، نمی داند</p></div>
<div class="m2"><p>که هر کس را به کاری غمزه پرکار او دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نگرداند زخورشید قیامت روی خود صائب</p></div>
<div class="m2"><p>خریداری که داغ گرمی بازار او دارد</p></div></div>