---
title: >-
    غزل شمارهٔ ۶۷۳۲
---
# غزل شمارهٔ ۶۷۳۲

<div class="b" id="bn1"><div class="m1"><p>سرمپیچ از داغ تا سرحلقه مردان شوی</p></div>
<div class="m2"><p>در سیاهی غوطه زن تا چشمه حیوان شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می شود در تنگنای جسم کامل جان پاک</p></div>
<div class="m2"><p>از صدف بیرون میا تا گوهر غلطان شوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون زلیخا دست از دامان یوسف برمدار</p></div>
<div class="m2"><p>تا مگر چون بوی پیراهن سبک جولان شوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با سر آزاده چون سرو از بهاران صلح کن</p></div>
<div class="m2"><p>تا در ایام خزان پیرایه بستان شوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تو بیرون نیست هر نقشی که در نه پرده هست</p></div>
<div class="m2"><p>از لباس زنگ چون آیینه گر عریان شوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا به چند این سبزه خوابیده زنجیرت شود؟</p></div>
<div class="m2"><p>پشت پا زن بر فلک تا سرو این بستان شوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یوسف از زندان قدم بر مسند عزت گذاشت</p></div>
<div class="m2"><p>سعی کن تا از فراموشان این زندان شوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خضر آب زندگی دست از علایق شستن است</p></div>
<div class="m2"><p>چون سکندر چند در ظلمات سرگردان شوی؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سکه پشت خویش بر زر داد، در زر غوطه زد</p></div>
<div class="m2"><p>در تو رو می آورد از هر چه روگردان شوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست جز افسوس حاصل سیر بی پرگار را</p></div>
<div class="m2"><p>ره به مرکز می بری روزی که سرگردان شوی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چند روزی مهر خاموشی به لب زن غنچه وار</p></div>
<div class="m2"><p>چون زر گل چند خرج چهره خندان شوی؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آب کن صائب دل خود را به آه آتشین</p></div>
<div class="m2"><p>تا چو شبنم محرم گلهای این بستان شوی</p></div></div>