---
title: >-
    غزل شمارهٔ ۳۷۱۰
---
# غزل شمارهٔ ۳۷۱۰

<div class="b" id="bn1"><div class="m1"><p>ترا کسی که به گلگشت بوستان آرد</p></div>
<div class="m2"><p>خط مسلمی باغ از خزان آرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خدا به آن لب جان بخش بخشد انصافی</p></div>
<div class="m2"><p>که بوسه ای ندهد تا مرا بجان آرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو مشرق از نفسش عالمی شود روشن</p></div>
<div class="m2"><p>حدیث روی تو هرکس که بر زبان آرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حجاب روی عرقناک یار، نزدیک است</p></div>
<div class="m2"><p>که پیچ و تاب به گوهر ز ریسمان آرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمی کشد ز ره آورد خویشتن خجلت</p></div>
<div class="m2"><p>به یوسف آینه آن کس که ارمغان آرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی است حرف بزرگان، قیاس کن از کوه</p></div>
<div class="m2"><p>که هرچه می شنود بر زبان همان آرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به برگ سبز کند یاد باغبان صائب</p></div>
<div class="m2"><p>سخن به اهل سخن هرکه ارمغان آرد</p></div></div>