---
title: >-
    غزل شمارهٔ ۸۶۵
---
# غزل شمارهٔ ۸۶۵

<div class="b" id="bn1"><div class="m1"><p>بس که افکنده است پیری در وجودم انقلاب</p></div>
<div class="m2"><p>خواب من بیداری و بیداریم گشته است خواب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سراپای وجودم ذره ای بی درد نیست</p></div>
<div class="m2"><p>یک سر مو نیست بر اندام من بی پیچ و تاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از لطایف آنچه در مجموعه دل ثبت بود</p></div>
<div class="m2"><p>یک قلم شد محو، غیر از یاد ایام شباب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از کشاکش قامتم تا چون کمان گردید خم</p></div>
<div class="m2"><p>مد عمر از قبضه بیرون رفت چون تیر شهاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوش سنگینی، بصر کندی، زبان لکنت گرفت</p></div>
<div class="m2"><p>این صدف های گهر شد از تهی مغزی حباب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رفت گیرایی برون از دست چون برگ خزان</p></div>
<div class="m2"><p>از قدم ها قوت رفتار شد پا در رکاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ریخت از هم پیکر فرسوده را موی سفید</p></div>
<div class="m2"><p>بال و پر شد این زمین شوره را موج سراب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ریخت تا دندان، ز هم پاشید اوراق دلم</p></div>
<div class="m2"><p>می رود بر باد، بی شیرازه گردد چون کتاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صبح پیری نیست گر صبح قیامت، از چه کرد</p></div>
<div class="m2"><p>پیش چشم من ز عینک نصب، میزان حساب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بادپای عمر را نتوان ز سرعت بازداشت</p></div>
<div class="m2"><p>چند صائب موی خود چون قیر سازی از خضاب؟</p></div></div>