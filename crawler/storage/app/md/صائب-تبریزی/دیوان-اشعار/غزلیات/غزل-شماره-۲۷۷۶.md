---
title: >-
    غزل شمارهٔ ۲۷۷۶
---
# غزل شمارهٔ ۲۷۷۶

<div class="b" id="bn1"><div class="m1"><p>دل نیاسود از تردد تا نشد منزل سفید</p></div>
<div class="m2"><p>کرد ما را رو سفید آخر، که روی دل سفید!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوی پیراهن نیامد، پیر کنعان تا نکرد</p></div>
<div class="m2"><p>از دو چشم خود در دولتسرای دل سفید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم ما آب سیاه آورد از بس انتظار</p></div>
<div class="m2"><p>تا شد از دامان صحرای طلب منزل سفید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حلقه بیرون در آتش است از نور شمع</p></div>
<div class="m2"><p>چون سپند ما تواند شد درین محفل سفید؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد زلخت دل یکی صد آبروی چشم ما</p></div>
<div class="m2"><p>کرد گوهر روی این دریای بی حاصل سفید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در جواب تلخ دادن ترشرویی می کنند</p></div>
<div class="m2"><p>چون شود در عهد این بی حاصلان سایل سفید؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همت ما صرف در پرداز دل شد از جهان</p></div>
<div class="m2"><p>ما همین یک خانه را کردیم ازین منزل سفید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر به دریا سایه اندازد گلیم بخت ما</p></div>
<div class="m2"><p>در شبستان صدف گوهر شود مشکل سفید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کلک صائب تازه شد تا این غزل را نقش بست</p></div>
<div class="m2"><p>روی دهقان را کند سرسبزی حاصل سفید</p></div></div>