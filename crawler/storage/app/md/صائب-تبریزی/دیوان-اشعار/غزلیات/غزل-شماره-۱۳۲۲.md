---
title: >-
    غزل شمارهٔ ۱۳۲۲
---
# غزل شمارهٔ ۱۳۲۲

<div class="b" id="bn1"><div class="m1"><p>غفلت تر دامنان را حاجت پیمانه نیست</p></div>
<div class="m2"><p>چشم خواب آلود نرگس گوش بر افسانه نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوهر درج خموشی از شکستن ایمن است</p></div>
<div class="m2"><p>زخم دندان تأسف بر لب پیمانه نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خشکی سودا، قلم در ناخنش نشکسته است</p></div>
<div class="m2"><p>آن که می گوید قلم بر مردم دیوانه نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که می آید، به آب رو از اینجا می رود</p></div>
<div class="m2"><p>قفل منع و چین ابرو بر در میخانه نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حسن ذاتی بی نیاز از صنعت مشاطه است</p></div>
<div class="m2"><p>زلف جوهر دست فرسود نسیم و شانه نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مهربانی های صیادست دامنگیر ما</p></div>
<div class="m2"><p>در قفس دلبستگی ما را به آب و دانه نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رشته کار تو صائب ناخنم را ریشه ساخت</p></div>
<div class="m2"><p>این قدر عقد گره در سبحه صد دانه نیست</p></div></div>