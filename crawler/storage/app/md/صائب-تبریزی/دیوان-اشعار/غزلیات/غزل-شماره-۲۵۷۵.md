---
title: >-
    غزل شمارهٔ ۲۵۷۵
---
# غزل شمارهٔ ۲۵۷۵

<div class="b" id="bn1"><div class="m1"><p>سایه تا بر گلستان آن قامت رعنا فکند</p></div>
<div class="m2"><p>شاخ گل را رعشه از کف ساغر صهبا فکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنچنان کز خط کشیدن صفحه باطل می شود</p></div>
<div class="m2"><p>جلوه او یک خیابان سرو را از پافکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون سپند آید سویدا در دل عاشق به رقص</p></div>
<div class="m2"><p>پرده تا از روی خود آن آتشین سیما فکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با وجود مغز، لایق نیست پیچیدن به پوست</p></div>
<div class="m2"><p>حق پرستی هر دو عالم را زچشم مافکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد ره خوابیده هم پرواز با موج سراب</p></div>
<div class="m2"><p>تا غزال وحشی من سایه بر صحرافکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که پشت پا نزد بر خواب در راه طلب</p></div>
<div class="m2"><p>کی به منزل می تواند پا به روی پافکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من به آهی کوه غم از پیش دل برداشتم</p></div>
<div class="m2"><p>رخنه ها فرهاد اگر از تیشه در خار افکند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سوزنی صائب بود در عالم تجرید بار</p></div>
<div class="m2"><p>در میاه راه بار خود ازان عیسی فکند</p></div></div>