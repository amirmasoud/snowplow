---
title: >-
    غزل شمارهٔ ۵۸۴
---
# غزل شمارهٔ ۵۸۴

<div class="b" id="bn1"><div class="m1"><p>فکنده ایم به امروز کار فردا را</p></div>
<div class="m2"><p>ازین حیات چه آسودگی بود ما را؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگاه دار سر رشته تا نگه دارند</p></div>
<div class="m2"><p>که می زنند به سوزن لب مسیحا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به چشم ظاهر اگر رخصت تماشا نیست</p></div>
<div class="m2"><p>نبسته است کسی شاهراه دلها را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر ز ابروی همت اشارتی باشد</p></div>
<div class="m2"><p>تهی کنیم به جام حباب دریا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خدا سزا دهد این اشک گرم را صائب</p></div>
<div class="m2"><p>که شست از نظرم سرمه تماشا را</p></div></div>