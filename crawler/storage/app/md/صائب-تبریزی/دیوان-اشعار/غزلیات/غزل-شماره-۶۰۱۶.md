---
title: >-
    غزل شمارهٔ ۶۰۱۶
---
# غزل شمارهٔ ۶۰۱۶

<div class="b" id="bn1"><div class="m1"><p>برده ام تا از سر کویت نشان خویشتن</p></div>
<div class="m2"><p>هم به جان تو که بیزارم ز جان خویشتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه بر آتش می نشاند، گه به آبم می دهد</p></div>
<div class="m2"><p>عاجزم در دست چشم خون فشان خویشتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دیار ما که از مغز قناعت آگهیم</p></div>
<div class="m2"><p>طعمه می سازد هما از استخوان خویشتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای که می نازی به صبر خویشتن، آیینه هست</p></div>
<div class="m2"><p>می توان کرد از نگاهی امتحان خویشتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر گریبان چاک صبحی رو به مشرق آوری</p></div>
<div class="m2"><p>آفتاب از شرم نگشاید دکان خویشتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خویش را گم کرده ام از بس پریشان خاطری</p></div>
<div class="m2"><p>از سر زلف تو می پرسم نشان خویشتن</p></div></div>