---
title: >-
    غزل شمارهٔ ۱۰۹۸
---
# غزل شمارهٔ ۱۰۹۸

<div class="b" id="bn1"><div class="m1"><p>آتش افروز شکر شیرینی پیغام توست</p></div>
<div class="m2"><p>زخم پیرای ملاحت تلخی دشنام توست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سبزه ای کز آتش یاقوت فرسای کلیم</p></div>
<div class="m2"><p>می زند جوش طراوت، خط عنبر فام توست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ابر سیرابی که بر خارا کند گوهر نثار</p></div>
<div class="m2"><p>وز ندامت تر نگردد، التفات عام توست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای تغافل پیشه بر پرواز ما دل بد مکن</p></div>
<div class="m2"><p>خاک ما افتادگان در شهر بند دام توست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کار خود صائب به تأثیر محبت واگذار</p></div>
<div class="m2"><p>این ندیدنها گناه شوخی ابرام توست</p></div></div>