---
title: >-
    غزل شمارهٔ ۶۲۳۷
---
# غزل شمارهٔ ۶۲۳۷

<div class="b" id="bn1"><div class="m1"><p>نبالد بر خود از شهرت دل نازک خیال من</p></div>
<div class="m2"><p>ز انگشت اشارت بیش می کاهد هلال من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز برق تشنگی از خرمن من دود اگر خیزد</p></div>
<div class="m2"><p>به آب زندگی لب تر نمی سازد سفال من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نبیند با هزاران چشم پیش پای خود گردون</p></div>
<div class="m2"><p>اگر از دل قدم بیرون نهد گرد ملال من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تمنای وصالش چون به گرد خاطرم گردد؟</p></div>
<div class="m2"><p>پریرویی که از تمکین نیاید در خیال من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به امید چه روز حشر از لب مهر بردارم؟</p></div>
<div class="m2"><p>که کوته می کند طول زمان را عرض حال من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز حیرت سروها را می رود از یاد بالیدن</p></div>
<div class="m2"><p>به هر گلشن که گردد جلوه گر نازک نهال من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ازان از فربهی چون ماه می سازم تهی پهلو</p></div>
<div class="m2"><p>که بیش از بدر ناخن می زند بر دل هلال من</p></div></div>