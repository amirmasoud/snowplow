---
title: >-
    غزل شمارهٔ ۵۸۴۴
---
# غزل شمارهٔ ۵۸۴۴

<div class="b" id="bn1"><div class="m1"><p>ما هوش خود به باده گلرنگ داده ایم</p></div>
<div class="m2"><p>گردن چو شیشه بر خط ساغر نهاده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر روی دست باد مرا دست سیر ما</p></div>
<div class="m2"><p>چون موج تا عنان به کف بحر داده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک عمر همچو غنچه درین بوستانسرا</p></div>
<div class="m2"><p>خون خورده ایم تا گره دل گشاده ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از زندگی است یک دو نفس در بساط ما</p></div>
<div class="m2"><p>چون صبح ما ز روز ازل پیر زاده ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر هیچ خاطری ننشسته است گرد ما</p></div>
<div class="m2"><p>افتاده نیست خاک، اگر ما فتاده ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون طفل نی سوار به میدان اختیار</p></div>
<div class="m2"><p>در چشم خود سوار ولیکن پیاده ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عمری است تا به پای زمین گیر همچو سنگ</p></div>
<div class="m2"><p>در رهگذار سیل حوادث فتاده ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون سبزه پا شکسته این باغ نیستیم</p></div>
<div class="m2"><p>ز آزادگی چو سرو به یک پا ستاده ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوهر نمی فتد ز بها از فتادگی</p></div>
<div class="m2"><p>سهل است اگر به خاک دو روزی فتاده ایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صائب بود ازان لب میگون خمار ما</p></div>
<div class="m2"><p>بیدرد را خیال که مخمور باده ایم</p></div></div>