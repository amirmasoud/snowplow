---
title: >-
    غزل شمارهٔ ۴۸۲۱
---
# غزل شمارهٔ ۴۸۲۱

<div class="b" id="bn1"><div class="m1"><p>به مژگان اشک پاشیدن میاموز</p></div>
<div class="m2"><p>به ابر تیره باریدن میاموز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به زلف آه،پیچیدن مده یاد</p></div>
<div class="m2"><p>به دراشک، غلطیدن میاموز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل ما را به درد خویش بگذار</p></div>
<div class="m2"><p>به ماتم دیده نالیدن میاموز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمی آید عنانداری ز سیلاب</p></div>
<div class="m2"><p>به عاشق پای لغزیدن میاموز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مکن تکلیف جان دادن به عشاق</p></div>
<div class="m2"><p>به مستان جامه بخشیدن میاموز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به شام هجر، دلگیری مده یاد</p></div>
<div class="m2"><p>به مهر و مه درخشیدن میاموز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مده تعلیم خونریزی به نشتر</p></div>
<div class="m2"><p>به مژگان سینه کاویدن میاموز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هوس بیطاقتی راخوب دارد</p></div>
<div class="m2"><p>به سرما خورده لرزیدن میاموز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مگو با عاقلان افسانه عشق</p></div>
<div class="m2"><p>به خون مرده جوشیدن میاموز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مجو وجد و سماع از زاهد خشک</p></div>
<div class="m2"><p>به نبض مرده جنبیدن میاموز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز خود بیرون شدن زاهد چه داند؟</p></div>
<div class="m2"><p>به چوب خشک بالیدن میاموز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هوس در بوسه دزدی اوستادست</p></div>
<div class="m2"><p>به طفل شوخ گل چیدن میاموز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خدادادست ناز و شیوه حسن</p></div>
<div class="m2"><p>به چشم آهوان دیدن میاموز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرا کز دودمان اهل عشقم</p></div>
<div class="m2"><p>به گرد شمع گردیدن میاموز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خدادادست علم عشقبازی</p></div>
<div class="m2"><p>به صائب عشق ورزیدن میاموز</p></div></div>