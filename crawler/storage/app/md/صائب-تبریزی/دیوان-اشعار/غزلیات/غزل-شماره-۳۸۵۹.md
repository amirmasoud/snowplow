---
title: >-
    غزل شمارهٔ ۳۸۵۹
---
# غزل شمارهٔ ۳۸۵۹

<div class="b" id="bn1"><div class="m1"><p>مرا جلای دل از چشم خونفشان باشد</p></div>
<div class="m2"><p>که آب صیقل خاک است تا روان باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مده غبار به خاطر زخاکساری راه</p></div>
<div class="m2"><p>که چشم صدرنشینان بر آستان باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بلبلی که بدآموز شد به کنج قفس</p></div>
<div class="m2"><p>زبان مار خس وخار آشیان باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درازدستی شیطان ز دل سیاهی ماست</p></div>
<div class="m2"><p>چراغ دزد به شب خواب پاسبان باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گشاد در گره بستگی است دل خوش دار</p></div>
<div class="m2"><p>که لال را زده انگشت ترجمان باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوشا کسی که درین خارزار دامنگیر</p></div>
<div class="m2"><p>چو باد تند وچو برق آتشین عنان باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تنور سرد محال است نان به خود گیرد</p></div>
<div class="m2"><p>چسان علاقه زپیری مرا به جان باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غم مرا دگران بیش می خورند از من</p></div>
<div class="m2"><p>همیشه روزی من رزق دیگران باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوشم چو سروبه بی حاصلی درین بستان</p></div>
<div class="m2"><p>که بی بری خط آزادی از خزان باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به جان اگر دگران راست زندگانی صائب</p></div>
<div class="m2"><p>حیات من به ملاقات دوستان باشد</p></div></div>