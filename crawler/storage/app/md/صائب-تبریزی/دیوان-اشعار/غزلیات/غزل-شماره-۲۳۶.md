---
title: >-
    غزل شمارهٔ ۲۳۶
---
# غزل شمارهٔ ۲۳۶

<div class="b" id="bn1"><div class="m1"><p>چهره ات بال سمندر می کند آیینه را</p></div>
<div class="m2"><p>خنده ات دامان گوهر می کند آیینه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این شکوه حسن با خورشید عالمتاب نیست</p></div>
<div class="m2"><p>شوکت حسنت سکندر می کند آیینه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جلوه آن خط نوخیز و لب شکرفشان</p></div>
<div class="m2"><p>بال طوطی، تنگ شکر می کند آیینه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفتاب بی زوال حسن عالمسوز او</p></div>
<div class="m2"><p>گرم چون صحرای محشر می کند آیینه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جلوه روی عرقناک تو ای ماه تمام</p></div>
<div class="m2"><p>سیر چشم از ماه و اختر می کند آیینه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا چه خواهد کرد یارب با دل مومین من</p></div>
<div class="m2"><p>آتشین رویی که مجمر می کند آیینه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اشتیاق گردسر گردیدنت، بی اختیار</p></div>
<div class="m2"><p>در کف مشاطه شهپر می کند آیینه را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صحبت روشن ضمیران کیمیای دولت است</p></div>
<div class="m2"><p>روی او خورشید منظر می کند آیینه را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جلوه همچشم، ابر نوبهار خجلت است</p></div>
<div class="m2"><p>آن رخ شبنم فشان، تر می کند آیینه را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ساده لوحان زود می گیرند رنگ همنشین</p></div>
<div class="m2"><p>صحبت طوطی سخنور می کند آیینه را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نعمت دیدار یوسف را نیارد در نظر</p></div>
<div class="m2"><p>گر چنین رویش توانگر می کند آیینه را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می کند از علم رسمی سینه ها را پاک عشق</p></div>
<div class="m2"><p>روشنی مفلس ز جوهر می کند آیینه را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از فروغ حسن، می گردد دل فولاد آب</p></div>
<div class="m2"><p>آن بهشتی روی، کوثر می کند آیینه را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون دل عاشق نگردد صائب از حسنش غیور؟</p></div>
<div class="m2"><p>صحبت او نازپرور می کند آیینه را</p></div></div>