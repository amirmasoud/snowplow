---
title: >-
    غزل شمارهٔ ۳۶۸۶
---
# غزل شمارهٔ ۳۶۸۶

<div class="b" id="bn1"><div class="m1"><p>حجاب پرده چشم پرآب می‌گردد</p></div>
<div class="m2"><p>وگرنه دلبر ما بی‌نقاب می‌گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همین ز جلوه آن شاخ گل خبر دارم</p></div>
<div class="m2"><p>که اشک در نظر من گلاب می‌گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه عارض است که از پرتو مشاهده‌اش</p></div>
<div class="m2"><p>به چشم جوهر آیینه آب می‌گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر ز ساغر خورشید ذره سرگرم است</p></div>
<div class="m2"><p>ز باده که سر آفتاب می‌گردد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امیدوار نباشم چرا به نومیدی؟</p></div>
<div class="m2"><p>سبوی آبله پر از سراب می‌گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز گریه اختر طالع نمی‌شود بیدار</p></div>
<div class="m2"><p>نمک به دیده بی‌درد خواب می‌گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خزان به خون گلستان عبث کمر بسته است</p></div>
<div class="m2"><p>که خود به خود ورق این کتاب می‌گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به خون قسمت من خاک آنچنان تشنه است</p></div>
<div class="m2"><p>که شیر در قدحم ماهتاب می‌گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز قُرب سوختگان دل نمی‌توان برداشت</p></div>
<div class="m2"><p>چگونه دود جدا از کباب می‌گردد؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در آن چمن که منم عندلیب آن صائب</p></div>
<div class="m2"><p>گل از نظاره شبنم گلاب می‌گردد</p></div></div>