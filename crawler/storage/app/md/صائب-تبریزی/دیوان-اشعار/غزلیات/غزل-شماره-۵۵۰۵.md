---
title: >-
    غزل شمارهٔ ۵۵۰۵
---
# غزل شمارهٔ ۵۵۰۵

<div class="b" id="bn1"><div class="m1"><p>نه بهر آب از سوز دل بیتاب می گردم</p></div>
<div class="m2"><p>که چون تبخال من از تشنگی سیراب می گردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سفیدی کرد چشمم را کف دریای نومیدی</p></div>
<div class="m2"><p>همان من در تلاش گوهر نایاب می گردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پر از گوهر کنم گر چون صدف دامان سایل را</p></div>
<div class="m2"><p>همان چون ابرنیسان از خجالت آب می گردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه حاصل کز درازی رشته عمرم شود افزون</p></div>
<div class="m2"><p>چو من از بیقراری خرج پیچ و تاب می گردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جواب خشک می گوید به رویم ابر دریا دل</p></div>
<div class="m2"><p>چو گوهر گر چه از یک قطره من سیراب می گردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همان مشت خس و خاری است از گردش مرا حاصل</p></div>
<div class="m2"><p>به گرد خویشتن چندان که چون گرداب می گردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به صد جانب برد دل در نمازم از پریشانی</p></div>
<div class="m2"><p>به رنگ سبحه من سرگشته در محراب می گردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ندارد نفس کافر در مقام فیض دست از من</p></div>
<div class="m2"><p>گران از خواب غفلت بیش در محراب می گردم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شبستان جهان را گر چه روشن از بیان دارم</p></div>
<div class="m2"><p>همان چون شمع صائب از خجالت آب می گردم</p></div></div>