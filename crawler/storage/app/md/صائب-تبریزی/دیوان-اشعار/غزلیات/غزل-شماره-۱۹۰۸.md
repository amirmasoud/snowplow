---
title: >-
    غزل شمارهٔ ۱۹۰۸
---
# غزل شمارهٔ ۱۹۰۸

<div class="b" id="bn1"><div class="m1"><p>نقشم به باد داد، نگار اینچنین خوش است</p></div>
<div class="m2"><p>خونم به خاک ریخت، بهار اینچنین خوش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل را گداخت، بوسه به این چاشنی است خوش</p></div>
<div class="m2"><p>دستم ز کار بود، کنار اینچنین خوش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از تاب چهر، برق خس و خار آرزوست</p></div>
<div class="m2"><p>رخسار آتشین نگار اینچنین خوش است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگذاشت غیر خانه زین، خانه دگر</p></div>
<div class="m2"><p>معمور در زمانه، سوار اینچنین خوش است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلها شد از غبار خطش مصحف غبار</p></div>
<div class="m2"><p>بی چشم زخم، خط غبار اینچنین خوش است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرگز دلم نزد نفسی بر مراد خویش</p></div>
<div class="m2"><p>آیینه پیش روی نگار اینچنین خوش است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل می رود به حلقه زلفش به پای خود</p></div>
<div class="m2"><p>دام آنچنان خوش است و شکار اینچنین خوش است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر خار بی گلی، گل بی خار شد ازو</p></div>
<div class="m2"><p>الحق که فیض عام بهار اینچنین خوش است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گل روی خود به اشک ندامت ز خواب شست</p></div>
<div class="m2"><p>در وقت صبح، آب خمار اینچنین خوش است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون حلقه های زلف دلم را قرار نیست</p></div>
<div class="m2"><p>پرگار خال چهره یار اینچنین خوش است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طوطی چو مغز پسته هم آغوش شکرست</p></div>
<div class="m2"><p>در هم خزیده عاشق و یار اینچنین خوش است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خونی که کرد در دل صیاد، مشک شد</p></div>
<div class="m2"><p>آهو به فکر میر شکار اینچنین خوش است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صائب به غیر عشق ندارد ترانه ای</p></div>
<div class="m2"><p>شعر اینچنین خوش است و شعار اینچنین خوش است</p></div></div>