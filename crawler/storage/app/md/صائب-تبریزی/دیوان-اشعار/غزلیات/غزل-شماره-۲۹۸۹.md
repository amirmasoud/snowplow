---
title: >-
    غزل شمارهٔ ۲۹۸۹
---
# غزل شمارهٔ ۲۹۸۹

<div class="b" id="bn1"><div class="m1"><p>کسی از زلف پریشان خونبهای دل نمی گیرد</p></div>
<div class="m2"><p>صبا را کس به خون لاله بسمل نمی گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زبخششهای عشق (پاک) طینت سینه ای دارم</p></div>
<div class="m2"><p>که چون آیینه کین سنگ را در دل نمی گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عجب دارم همای وصل بر من سایه اندازد</p></div>
<div class="m2"><p>که جغد ازنا کسی در خانه ام منزل نمی گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر دامن زند در کشتن ما بر میان قاتل</p></div>
<div class="m2"><p>به خاک و خون تپیدن را کس از بسمل نمی گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر خاکستر پروانه دارد شعله غیرت</p></div>
<div class="m2"><p>چرا خون چراغ کشته از محفل نمی گیرد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لحد گهواره سان می لرزد از بیتابی جسمم</p></div>
<div class="m2"><p>زشوخی کشتیم آرام در ساحل نمی گیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زسوز سینه مجنون صحرایی عجب دارم</p></div>
<div class="m2"><p>که چون فانوس، آتش در دل محمل نمی گیرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طلبکار تو چون سیلاب بر قلزم زند خود را</p></div>
<div class="m2"><p>به هر فرسنگ چون سنگ نشان منزل نمی گیرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چسان در رخنه دل داغ عشقش را کنم پنهان؟</p></div>
<div class="m2"><p>کسی آیینه خورشید را در گل نمی گیرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل ما در تلاش زخم دارد همت دیگر</p></div>
<div class="m2"><p>به یک زخم نمایان دست از قاتل نمی گیرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا این شیوه صائب ()خویشتن دارد</p></div>
<div class="m2"><p>که گر پیکان به چشمش می زنی در دل نمی گیرد</p></div></div>