---
title: >-
    غزل شمارهٔ ۱۵۰۶
---
# غزل شمارهٔ ۱۵۰۶

<div class="b" id="bn1"><div class="m1"><p>نفس سوخته شمع سر بالین من است</p></div>
<div class="m2"><p>مهر خاموشی من جام جهان بین من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیغ چون بید ز جان سختی من می لرزد</p></div>
<div class="m2"><p>موج بی بال وپر از لنگر تمکین من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر دلم گرد یتیمی چو گهر نیست گران</p></div>
<div class="m2"><p>عشرت روی زمین در دل غمگین من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لنگر از خویش سرانجام دهد کشتی من</p></div>
<div class="m2"><p>پله خواب، گران از دل سنگین من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حسن از تربیت عشق شود عالمسوز</p></div>
<div class="m2"><p>سرخی روی گل از نغمه رنگین من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواهد از نقش به نقاش رسانید مرا</p></div>
<div class="m2"><p>اتحادی که در آیینه حق بین من است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بحر از پنجه مرجان نپذیرد آرام</p></div>
<div class="m2"><p>ناصح از ساده دلی در پی تسکین من است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شده ام خانه دربسته ز حیرت صائب</p></div>
<div class="m2"><p>می خورد خون خود آن کس که سخن چین من است</p></div></div>