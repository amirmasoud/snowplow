---
title: >-
    غزل شمارهٔ ۸۱۳
---
# غزل شمارهٔ ۸۱۳

<div class="b" id="bn1"><div class="m1"><p>تسکین ندهد خوردن می سوز درون را</p></div>
<div class="m2"><p>آتش بود این آب، جگر تشنه خون را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راندن نکند خیرگی از طبع مگس دور</p></div>
<div class="m2"><p>اندیشه ز خواری نبود مرد دون را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از پیشروان دل نگرانی نتوان برد</p></div>
<div class="m2"><p>پیوسته بود چشم ز پی راهنمون را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگذاشت ز سر، سرکشی آن زلف ز آهم</p></div>
<div class="m2"><p>حرفی است که در مار اثرهاست فسون را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عقل است که موقوف به کسب است کمالش</p></div>
<div class="m2"><p>حاجت به معلم نبود مشق جنون را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صائب مکن از بخت طمع برگ فراغت</p></div>
<div class="m2"><p>کز باده نصیبی نبود جام نگون را</p></div></div>