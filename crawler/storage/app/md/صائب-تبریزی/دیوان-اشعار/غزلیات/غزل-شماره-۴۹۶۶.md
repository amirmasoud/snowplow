---
title: >-
    غزل شمارهٔ ۴۹۶۶
---
# غزل شمارهٔ ۴۹۶۶

<div class="b" id="bn1"><div class="m1"><p>رگ لعل است ازدلهای خونین قد دلجویش</p></div>
<div class="m2"><p>زجانهای پریشان رشته جان است هرمویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قبای اطلس افلاک شد پیراهن یوسف</p></div>
<div class="m2"><p>زبس پیچید درمغز پریشان جهان بویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز هر زخم نمایان مشرق خورشید می گردد</p></div>
<div class="m2"><p>شهیدی (را) که باشد شمع بالین پرتو رویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سجود آسمانها حلقه بیرون درباشد</p></div>
<div class="m2"><p>چه باشد سجده من در حضور طاق ابرویش</p></div></div>