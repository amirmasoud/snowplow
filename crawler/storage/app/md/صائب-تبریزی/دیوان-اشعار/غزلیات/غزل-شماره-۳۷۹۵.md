---
title: >-
    غزل شمارهٔ ۳۷۹۵
---
# غزل شمارهٔ ۳۷۹۵

<div class="b" id="bn1"><div class="m1"><p>غزال چشم تو ره بر پلنگ می گیرد</p></div>
<div class="m2"><p>حباب بحر تو باج از نهنگ می گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود مصاف تو ای چرخ با شکسته دلان</p></div>
<div class="m2"><p>همیشه شیر تو آهوی لنگ می گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مکش سر از خط تسلیم عشق کاین صیاد</p></div>
<div class="m2"><p>به دام موج ز دریا نهنگ می گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه طالع است که شیرازه سفینه من</p></div>
<div class="m2"><p>مزاج اره پشت نهنگ می گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به چشم جوهریان آب چون نگرداند؟</p></div>
<div class="m2"><p>ز آب این گهر آیینه زنگ می گیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز قید عقل، مرا هر که می کند آزاد</p></div>
<div class="m2"><p>اسیری از کف اهل فرنگ می گیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درین دیار چه لنگر فکنده ای صائب؟</p></div>
<div class="m2"><p>چه قیمت آینه در شهر زنگ می گیرد؟</p></div></div>