---
title: >-
    غزل شمارهٔ ۴۵۴
---
# غزل شمارهٔ ۴۵۴

<div class="b" id="bn1"><div class="m1"><p>ز خط سبز شد فیروزه ای لعل نگار ما</p></div>
<div class="m2"><p>جواهر سرمه ای می خواست چشم اشکبار ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر چه بی صفا گردد ز گرد آیینه روشن</p></div>
<div class="m2"><p>یکی صد شد ز گرد خط، صدای گلعذار ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خط آزادی اغیار شد گر خط شبرنگش</p></div>
<div class="m2"><p>شب قدری است بهر دیده شب زنده دار ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگه دارد خدا از چشم بد آن روی نو خط را!</p></div>
<div class="m2"><p>که دام عنبرین سامان دهد بهر شکار ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درین فرصت که خط پیچید دست زلف ظالم را</p></div>
<div class="m2"><p>مشو غافل ز احوال دل امیدوار ما</p></div></div>