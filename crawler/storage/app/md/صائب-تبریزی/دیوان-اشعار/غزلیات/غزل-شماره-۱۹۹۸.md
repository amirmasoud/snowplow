---
title: >-
    غزل شمارهٔ ۱۹۹۸
---
# غزل شمارهٔ ۱۹۹۸

<div class="b" id="bn1"><div class="m1"><p>جوهر غبار دیده حیران آینه است</p></div>
<div class="m2"><p>نقش و نگار، خواب پریشان آینه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داغ است از طراوت آن خط پشت لب</p></div>
<div class="m2"><p>طوطی که خضر چشمه حیوان آینه است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در عهد حسن شوخ تو سیماب جلوه شد</p></div>
<div class="m2"><p>حیرانیی که لنگر طوفان آینه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون آفتاب، خط شعاعی است جوهرش</p></div>
<div class="m2"><p>تا پرتو جمال تو مهمان آینه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تسخیر مشکل است پریزاد حسن را</p></div>
<div class="m2"><p>این نقش در نگین سلیمان آینه است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر صبح نیکوان به در خانه اش روند</p></div>
<div class="m2"><p>این منزلت ز پاکی دامان آینه است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>معشوق را حمایت عاشق بود حصار</p></div>
<div class="m2"><p>طوطی چو موم سبز، نگهبان آینه است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بازار حسن او ز خط سبز گرم شد</p></div>
<div class="m2"><p>زنگار اگر چه تخته دکان آینه است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در روزگار حسن تو شد خارخار شوق</p></div>
<div class="m2"><p>هر جوهر نهفته که در کان آینه است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خاکش به چشم اگر به دو عالم نظر کند</p></div>
<div class="m2"><p>آن را که چاک سینه خیابان آینه است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب مگر به مرهم زنگار به شود</p></div>
<div class="m2"><p>داغی که از صفا به دل و جان آینه است</p></div></div>