---
title: >-
    غزل شمارهٔ ۱۷۴۵
---
# غزل شمارهٔ ۱۷۴۵

<div class="b" id="bn1"><div class="m1"><p>به هیچ و پوچ مرا عمر چون شرر بسته است</p></div>
<div class="m2"><p>ز خود برون شدن من به یک نظر بسته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اثر ز جنت دربسته در جهان گر هست</p></div>
<div class="m2"><p>ازان کس است که بر روی خلق در بسته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شود ز روزن و در خانه ها غبارآلود</p></div>
<div class="m2"><p>صفای سینه به پوشیدن نظر بسته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا رفیق موافق به وجد می آرد</p></div>
<div class="m2"><p>عزیمت سفر من به همسفر بسته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کند جلای وطن دیده ور عزیزان را</p></div>
<div class="m2"><p>که تا به بحر بود، دیده گهر بسته است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به خرج آتش سوزنده می رود چو شرار</p></div>
<div class="m2"><p>چو غنچه در گره خویش هر که زر بسته است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز این که هر نفس از پیچ و تاب می کاهد</p></div>
<div class="m2"><p>چه طرف رشته ز نزدیکی گهر بسته است؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا ز داغ، دل تیره می شود روشن</p></div>
<div class="m2"><p>جلای سوخته من به این شرر بسته است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به آن سرد دل من چو غنچه باز شود</p></div>
<div class="m2"><p>گشاد من به هواداری سحر بسته است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به خون خویش محال است سرخ رو نشود</p></div>
<div class="m2"><p>ستمگری که ترا تیغ بر کمر بسته است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنان که راحت چشم است در ندیدنها</p></div>
<div class="m2"><p>حضور گوش به شنیدن خبر بسته است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چرا غم دگران می کند پریشانم</p></div>
<div class="m2"><p>اگر نه رشته جانها به یکدگر بسته است؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صدای طبل رحیل است شادیانه او</p></div>
<div class="m2"><p>کسی که توشه به اندازه سفر بسته است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به خرج می رود آخر درین جهان صائب</p></div>
<div class="m2"><p>چو سکه هر که دل خویش را به زر بسته است</p></div></div>