---
title: >-
    غزل شمارهٔ ۱۲۵۴
---
# غزل شمارهٔ ۱۲۵۴

<div class="b" id="bn1"><div class="m1"><p>لاله ای جز داغ در صحرای امکان نیست نیست</p></div>
<div class="m2"><p>سنبل این باغ جز خواب پریشان نیست نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانه خود را به آب رو چو گوهر تازه دار</p></div>
<div class="m2"><p>کز مروت نم به چشم ابر احسان نیست نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مزرع امید را در عهد این بی حاصلان</p></div>
<div class="m2"><p>جز تریهای فلک امید باران نیست نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پا به دامن کش که در درگاه این بی حاصلان</p></div>
<div class="m2"><p>مد احسانی به غیر از چوب دربان نیست نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از گذشت دامن شب بیکسان عشق را</p></div>
<div class="m2"><p>دستگیری غیر صبح پاکدامان نیست نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این جواب آن که فرموده است عبدالله عشق</p></div>
<div class="m2"><p>جان من معشوق بودن سهل و آسان نیست نیست</p></div></div>