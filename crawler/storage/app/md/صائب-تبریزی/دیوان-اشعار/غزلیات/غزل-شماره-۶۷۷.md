---
title: >-
    غزل شمارهٔ ۶۷۷
---
# غزل شمارهٔ ۶۷۷

<div class="b" id="bn1"><div class="m1"><p>بلبل نمی شود به قفس از چمن جدا</p></div>
<div class="m2"><p>فانوس، شمع را نکند ز انجمن جدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حیرت مباد پرده بینایی کسی!</p></div>
<div class="m2"><p>کز یوسفیم در ته یک پیرهن جدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هشدار کز خراش دل سنگ خاره شد</p></div>
<div class="m2"><p>آخر به تیغ کوه، سر کوهکن جدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دورباش سینه گرم ایستاده است</p></div>
<div class="m2"><p>فانوس وار از تن من پیرهن جدا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر پی برد به چاشنی آن دهن نفس</p></div>
<div class="m2"><p>مشکل به حرف و صوت شود زان دهن جدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون خامه در محبت هم بس که یکدلند</p></div>
<div class="m2"><p>از هم نمی کند دو لبش را سخن جدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب ز من مپرس حضور وطن که کرد</p></div>
<div class="m2"><p>اندیشه غریب، مرا از وطن جدا</p></div></div>