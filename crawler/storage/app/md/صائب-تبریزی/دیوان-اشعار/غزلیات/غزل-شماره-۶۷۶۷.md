---
title: >-
    غزل شمارهٔ ۶۷۶۷
---
# غزل شمارهٔ ۶۷۶۷

<div class="b" id="bn1"><div class="m1"><p>مرا چون دیگران گرزان که اسبابی نشد روزی</p></div>
<div class="m2"><p>به این شادم که دل را پرده خوابی نشد روزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوارا کرد بر من درد می را خاکساری ها</p></div>
<div class="m2"><p>اگر از جام قسمت باده نابی نشد روزی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به کوری سوختم چون شمع در بتخانه غفلت</p></div>
<div class="m2"><p>بسر بردن شبی در کنج محرابی نشد روزی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر از اشک حسرت دامن دریا به دست آرد</p></div>
<div class="m2"><p>خس بی دست و پایی را که سیلابی نشد روزی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندارد حاصلی جمعیت بسیار بی قسمت</p></div>
<div class="m2"><p>که گوهر را ز دریا قطره آبی نشد روزی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندید آسایش ساحل درین دریای پروحشت</p></div>
<div class="m2"><p>ز حیرت چشم هر کس را شکرخوابی نشد روزی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه حاصل زین که دریا را به شور آورد طبع ما؟</p></div>
<div class="m2"><p>چو بخت خفته ما را کف آبی نشد روزی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به کوری شست دست از تار و پود زندگی صائب</p></div>
<div class="m2"><p>کتان شوربختی را که مهتابی نشد روزی</p></div></div>