---
title: >-
    غزل شمارهٔ ۲۳۱۲
---
# غزل شمارهٔ ۲۳۱۲

<div class="b" id="bn1"><div class="m1"><p>خال موزون است هر جا بر رخ دلبر فتاد</p></div>
<div class="m2"><p>هیچ جا بیجا نباشد هر که نیک اختر فتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زود می شد محو تبخال از لب چون لعل او</p></div>
<div class="m2"><p>از کباب ما مگر اشکی بر این اخگر فتاد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقش شیرین را چو محمل سر به صحرا داده ایم</p></div>
<div class="m2"><p>شور ما صد پرده از فرهاد شیرین تر فتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم زخمی دامگاه عشق را در کار هست</p></div>
<div class="m2"><p>چون قفس پهلوی ما سهل است اگر لاغر فتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می کشد آخر به خجلت کامجوییهای دهر</p></div>
<div class="m2"><p>خار خار آرزو خواهد به پشت سر فتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با خیال روی گل از صحبت گل ساختیم</p></div>
<div class="m2"><p>سیر باغ و بوستان ما به زیر پر فتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از حریم عشق ما را هیچکس بیرون نکرد</p></div>
<div class="m2"><p>این سپند از شوخی خود دور از مجمر فتاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مبتلای شش جهت را چاره جز تسلیم نیست</p></div>
<div class="m2"><p>نقش بیکارست هر جا مهره در ششدر فتاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که را راه سخن دادند نعمتها ازوست</p></div>
<div class="m2"><p>طوطی ما تا دهن واکرد در شکر فتاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پای خواب آلود ما از هر دو عالم درگذشت</p></div>
<div class="m2"><p>بند نتواند شدن تیغی که خوش لنگر فتاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب از حسن گلو سوز که می گویی سخن؟</p></div>
<div class="m2"><p>کآتش از کلک جهانسوز تو در دفتر فتاد</p></div></div>