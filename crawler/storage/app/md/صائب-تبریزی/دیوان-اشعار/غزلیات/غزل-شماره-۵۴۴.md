---
title: >-
    غزل شمارهٔ ۵۴۴
---
# غزل شمارهٔ ۵۴۴

<div class="b" id="bn1"><div class="m1"><p>میزبانی که ز جان سیر کند مهمان را</p></div>
<div class="m2"><p>چه ضرورست که آراسته سازد خوان را؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاش یک بار به سر منزل ما می آمد</p></div>
<div class="m2"><p>آن که بر تربت ما ریخت گل و ریحان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیشدستی کن و دیوان خود امروز بپرس</p></div>
<div class="m2"><p>چه ضرورست به فردا فکنی دیوان را؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه کند پرده ناموس به بی تابی عشق؟</p></div>
<div class="m2"><p>بادبان بال و پر سیر بود طوفان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شادیی کز ته دل نیست، کدورت به ازوست</p></div>
<div class="m2"><p>خون کند خنده سوفار دل پیکان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیر را حرص دوبالا شود از رفتن عمر</p></div>
<div class="m2"><p>بیشتر گرم کند جستن گو، چوگان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که بی حد شود، از حد نکند پروایی</p></div>
<div class="m2"><p>چه غم از محتسب شهر بود مستان را؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بس که در لقمه من سنگ نهفته است فلک</p></div>
<div class="m2"><p>بی تأمل نگذارم به جگر دندان را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کار موقوف به وقت است که چون وقت رسید</p></div>
<div class="m2"><p>خوابی از بند رهانید مه کنعان را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بست بر خاک ز بی بال و پری صائب نقش</p></div>
<div class="m2"><p>مگر از دور زمین بوس کند جانان را</p></div></div>