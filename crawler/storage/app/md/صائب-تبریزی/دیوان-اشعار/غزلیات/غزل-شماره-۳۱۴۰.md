---
title: >-
    غزل شمارهٔ ۳۱۴۰
---
# غزل شمارهٔ ۳۱۴۰

<div class="b" id="bn1"><div class="m1"><p>شدم آسوده تا از دیده اشک لاله رنگ آمد</p></div>
<div class="m2"><p>نهادم پشت بر دیوار تا پایم به سنگ آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم عالم چه حد دارد به گرد عاشقان گردد؟</p></div>
<div class="m2"><p>حصار عافیت دیوانه را خوی پلنگ آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حذر از دشمنی کن کز طریق صلح می آید</p></div>
<div class="m2"><p>از ان دشمن چرا ترسد کسی کز راه جنگ آمد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صفیر دلخراشی می فشارد بر جگر ناخن</p></div>
<div class="m2"><p>کدامین شیشه دل باز در راهش به سنگ آمد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دست کوتهم رحمت کن ای دامان عریانی</p></div>
<div class="m2"><p>که از چین جبین آستین دستم به تنگ آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه از مسجد فتوحی شد نه از میخانه امدادی</p></div>
<div class="m2"><p>به هر جانب که رفتم پای امیدم به سنگ آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به اندک روزگاری جامه بر تن می درد صائب</p></div>
<div class="m2"><p>به رنگ غنچه هر کس در گلستان دست تنگ آمد</p></div></div>