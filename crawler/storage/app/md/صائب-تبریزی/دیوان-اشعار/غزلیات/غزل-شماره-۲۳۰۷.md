---
title: >-
    غزل شمارهٔ ۲۳۰۷
---
# غزل شمارهٔ ۲۳۰۷

<div class="b" id="bn1"><div class="m1"><p>اگر دو هفته بود چهره گلستان سرخ</p></div>
<div class="m2"><p>مدام از می لعلی است روی جانان سرخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهانیان همه گردن کشیده اند از دور</p></div>
<div class="m2"><p>شود به خون که تا دست و تیغ جانان سرخ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشان صافی شست است این که چشمش را</p></div>
<div class="m2"><p>نشد ز ریختن خون خدنگ مژگان سرخ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خون بی گنهان است آنقدر سیراب</p></div>
<div class="m2"><p>که دست می شود از دامنش چو مرجان سرخ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر حجاب سمندر شود، که می سوزد</p></div>
<div class="m2"><p>چنین شود اگر از می عذار جانان سرخ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه خون که در دلم از آرزوی بوسه کند</p></div>
<div class="m2"><p>در آن زمان که کند سبز من لب از پان سرخ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سهیل غوطه به خون عقیق خواهد زد</p></div>
<div class="m2"><p>ز تاب می چو شود سیب آن زنخدان سرخ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز غیرت رخ او خون گل چنان زد جوش</p></div>
<div class="m2"><p>که خار بر سر دیوار شد چو مرجان سرخ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نظر سیاه به آب حیات کی سازد؟</p></div>
<div class="m2"><p>شد از گزیدن لب هر که را که دندان سرخ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز جویبار حیاتش نرست شاخ گلی</p></div>
<div class="m2"><p>به خون هر که نگردید تیر جانان سرخ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سیاه خانه این دشت، داغ لاله شود</p></div>
<div class="m2"><p>اگر چنین شود از اشک من بیابان سرخ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کدام زهره جبین چهره از شراب افروخت؟</p></div>
<div class="m2"><p>که همچو جامه فانوس شد شبستان سرخ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کند کباب به خون ناکشیده آهو را</p></div>
<div class="m2"><p>ز بس ز گرمی آن شست گشت پیکان سرخ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به سر به راهی ما زلف یار می نازد</p></div>
<div class="m2"><p>شود ز گوی سبکسیر روی چوگان سرخ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>می دو آتشه را نشأه دگر باشد</p></div>
<div class="m2"><p>خوش آن زمان که لب یار گردد از پان سرخ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چراغ دل ز جگرگوشه می شود روشن</p></div>
<div class="m2"><p>بود ز لعل لب او رخ بدخشان سرخ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شکار لاغرم، این می کشد مرا که مباد</p></div>
<div class="m2"><p>ز خون من نشود دست و تیغ جانان سرخ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز شرم بی اثریهاست اشک من رنگین</p></div>
<div class="m2"><p>که از تپانچه بود چهره یتیمان سرخ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مخور ز چهره گلگون گل، فریب جمال</p></div>
<div class="m2"><p>که در مقام جلال است رخت شاهان سرخ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فزود دامن صحرا جنون مجنون را</p></div>
<div class="m2"><p>که گردد اخگر خامش ز باد دامان سرخ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جواب آن غزل طالب است این صائب</p></div>
<div class="m2"><p>کز اوست روی سخن گستران ایران سرخ</p></div></div>