---
title: >-
    غزل شمارهٔ ۵۷۴۷
---
# غزل شمارهٔ ۵۷۴۷

<div class="b" id="bn1"><div class="m1"><p>چه دست در خم آن زلف دلنواز کنم</p></div>
<div class="m2"><p>به ناخنی که ندارم چه عقده باز کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ببین چه ساده دل افتاده ام که می خواهم</p></div>
<div class="m2"><p>ترا به نیم دل از خلق بی نیاز کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا که هر مژه در عالمی است پا در گل</p></div>
<div class="m2"><p>نظر به شاهد وحدت چگونه باز کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فروغ عاریتی آنقدر گزیده مرا</p></div>
<div class="m2"><p>که همچو شمع زبان در دهان گاز کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی هزار شود قطره چون به بحر رسید</p></div>
<div class="m2"><p>چرا مضایقه جان به دلنواز کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا که نیست دلی، چون حضور دل باشد</p></div>
<div class="m2"><p>مرا که نیست نیازی چرا نماز کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من آنچه می کشم از خویش می کشم صائب</p></div>
<div class="m2"><p>چگونه از خودی خویش احتراز کنم</p></div></div>