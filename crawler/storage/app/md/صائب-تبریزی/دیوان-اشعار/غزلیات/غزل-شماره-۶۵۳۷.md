---
title: >-
    غزل شمارهٔ ۶۵۳۷
---
# غزل شمارهٔ ۶۵۳۷

<div class="b" id="bn1"><div class="m1"><p>از بس که سرکش است قد چون نهال تو</p></div>
<div class="m2"><p>در آب هم نگون ننماید مثال تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از حسن بی مثال کند ناز بر جهان</p></div>
<div class="m2"><p>آیینه دلی که پذیرد مثال تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چند بخت کوته و ایام نارساست</p></div>
<div class="m2"><p>نومید نیستم ز امید وصال تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چندان که دل فزون شکنی شوختر شوی</p></div>
<div class="m2"><p>گویا که در شکستن دلهاست بال تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سینه زعفران شودش ریشه ملال</p></div>
<div class="m2"><p>هر کس که بگذرد به دل بی ملال تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دایم بود ز خون شفق تازه رو چو صبح</p></div>
<div class="m2"><p>ناخن به هر دلی که رساند هلال تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فردای حشر مایه اشک ندامت است</p></div>
<div class="m2"><p>امروز خون هر که نشد پایمال تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یارب چه آتشی، که گلاب چکیده شد</p></div>
<div class="m2"><p>در شیشه های غنچه گل از انفعال تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خورشید آیدش ورق شسته در نظر</p></div>
<div class="m2"><p>چشمی که شد فریفته خط و خال تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در جام صبح و در قدح آفتاب نیست</p></div>
<div class="m2"><p>خونی که همچو شیر نباشد حلال تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دامان خاک پرده دام است سر به سر</p></div>
<div class="m2"><p>تا قسمت کمند که گردد غزال تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر دیگران به وصل تو خوشوقت می شوند</p></div>
<div class="m2"><p>صائب دلش خوش است به فکر و خیال تو</p></div></div>