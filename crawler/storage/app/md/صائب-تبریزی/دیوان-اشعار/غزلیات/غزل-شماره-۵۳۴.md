---
title: >-
    غزل شمارهٔ ۵۳۴
---
# غزل شمارهٔ ۵۳۴

<div class="b" id="bn1"><div class="m1"><p>از گلستان نشود غنچه دل باز مرا</p></div>
<div class="m2"><p>پنجه سرو بود چنگل شهباز مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می توان ناله شنید از کف خاکستر من</p></div>
<div class="m2"><p>نشود سوختگی سرمه آواز مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرده ساز شود نه فلک از ناله من</p></div>
<div class="m2"><p>ندهد سرمه گر آن چشم فسونساز مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زحمت آینه من مده ای روشنگر</p></div>
<div class="m2"><p>دل سیه می شود از منت پرداز مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آخرالامر عنانداری من خواهد کرد</p></div>
<div class="m2"><p>شهسواری که عنان داد ز آغاز مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دفتر بال و پرش طعمه مقراض شود</p></div>
<div class="m2"><p>آن که افکند ز سر رشته پرواز مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صبر چندان که در خانه به رویم بندد</p></div>
<div class="m2"><p>کشش دل کشد از خانه برون باز مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گوش بر بانگ هم آواز ندارم صائب</p></div>
<div class="m2"><p>بس بود ز اهل سخن خامه هم آواز مرا</p></div></div>