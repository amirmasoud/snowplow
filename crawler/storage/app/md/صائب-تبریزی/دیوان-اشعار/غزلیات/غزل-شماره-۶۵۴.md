---
title: >-
    غزل شمارهٔ ۶۵۴
---
# غزل شمارهٔ ۶۵۴

<div class="b" id="bn1"><div class="m1"><p>به هر ترنمی از جای می رود دل ما</p></div>
<div class="m2"><p>سبک رکاب چو بوی گل است محمل ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمین سینه ما درد و داغ پروردست</p></div>
<div class="m2"><p>یکی هزار شود تخم اشک در گل ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکست آینه ما و توتیا گردید</p></div>
<div class="m2"><p>همان خیال تو استاده در مقابل ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رسیده ایم به انجام و اول سفرست</p></div>
<div class="m2"><p>ز راه دورتر افتاده است منزل ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به پیشدستی ما نیست در کرم حاتم</p></div>
<div class="m2"><p>ز آبرو نشود تنگدست، سایل ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار ناخن تدبیر غوطه در خون زد</p></div>
<div class="m2"><p>نشد گشاده شود عقده های مشکل ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز سرو گلشن فردوس راست می گذریم</p></div>
<div class="m2"><p>نهال قد تو تا پا فشرده در دل ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نمی خوریم غم از هیچ رهگذر صائب</p></div>
<div class="m2"><p>خوشا کسی که درآید به گوشه دل ما</p></div></div>