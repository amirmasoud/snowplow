---
title: >-
    غزل شمارهٔ ۵۰۴۷
---
# غزل شمارهٔ ۵۰۴۷

<div class="b" id="bn1"><div class="m1"><p>هرجا نمی خرند متاعت گران مباش</p></div>
<div class="m2"><p>پرواز گیر و خار و خس آشیان مباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بوی گل ردای سیاحت فکن به دوش</p></div>
<div class="m2"><p>چون سبزه پاشکسته یک بوستان مباش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای شاخ گل به صحبت بلبل سری بکش</p></div>
<div class="m2"><p>بسیار بر رضای دل باغبان مباش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک حرف بشنو ازمن ودر خلدسیر کن</p></div>
<div class="m2"><p>درمجلسی که گوش توان شد زبان مباش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صائب که منع می کنداز جلوه یار را؟</p></div>
<div class="m2"><p>خورشید را که گفته که آتش عنان مباش ؟</p></div></div>