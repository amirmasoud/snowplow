---
title: >-
    غزل شمارهٔ ۱۱۶۵
---
# غزل شمارهٔ ۱۱۶۵

<div class="b" id="bn1"><div class="m1"><p>مهربانی از میان خلق دامن چیده است</p></div>
<div class="m2"><p>از تکلف، آشنایی برطرف گردیده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وسعت از دست و دل مردم به منزل رفته است</p></div>
<div class="m2"><p>جامه ها پاکیزه و دلها به خون غلطیده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در بساط آفرینش یک دل بیدار نیست</p></div>
<div class="m2"><p>رگ ز غفلت در تن مردم ره خوابیده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رحم و انصاف و مروت از جهان برخاسته است</p></div>
<div class="m2"><p>روی دل از قبله مهر و وفا گردیده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پرده شرم و حیا، بال و پر عنقا شده است</p></div>
<div class="m2"><p>صبر از دلها چون کوه قاف دامن چیده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست غیر از دست خالی پرده پوشی سرو را</p></div>
<div class="m2"><p>خار چندین جامه رنگین ز گل پوشیده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>موج دریا سینه بر خاشاک می مالد ز درد</p></div>
<div class="m2"><p>رشته سر تا پای در گوهر نهان گردیده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گوهر و خرمهره در یک سلک جولان می کنند</p></div>
<div class="m2"><p>تار و پود انتظام از یکدگر پاشیده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بلبلان را خار در پیراهن است از آشیان</p></div>
<div class="m2"><p>بستر گل، خوابگاه شبنم نادیده است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر تهیدستی ز بی شرمی درین بازارگاه</p></div>
<div class="m2"><p>در برابر ماه کنعان را دکانی چیده است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تر نگردد از زر قلبی که در کارش کنند</p></div>
<div class="m2"><p>یوسف بی طالع ما گرگ باران دیده است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خاطر آزاد ما از دور گردون فارغ است</p></div>
<div class="m2"><p>شیشه افلاک را بر طاق نسیان چیده است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در دل ما آرزوی دولت بیدار نیست</p></div>
<div class="m2"><p>چشم ما بسیار ازین خواب پریشان دیده است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اختر ما را کجا از خاک خواهد برگرفت؟</p></div>
<div class="m2"><p>آن که روی مهر تابان بر زمین مالیده است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر زمین آن کس که دامان می کشید از روی ناز</p></div>
<div class="m2"><p>عمرها شد زیر دامان زمین خوابیده است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر جهان زیر و زبر گردد، نمی جنبد ز جا</p></div>
<div class="m2"><p>هر که صائب پا به دامان رضا پیچیده است</p></div></div>