---
title: >-
    غزل شمارهٔ ۱۰۰۹
---
# غزل شمارهٔ ۱۰۰۹

<div class="b" id="bn1"><div class="m1"><p>پرده شرم و حیا را باده ناب آتش است</p></div>
<div class="m2"><p>بر گل کاغذ، هوای عالم آب آتش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسمان را عشق آورده است در وجد و سماع</p></div>
<div class="m2"><p>آسیای شعله جواله را آب آتش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون سمندر، عاشقان روی آتشناک را</p></div>
<div class="m2"><p>مطرب و ساقی و نقل و باده ناب آتش است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست با پهلوی خشک ما ملایم جای گرم</p></div>
<div class="m2"><p>این گیاه ناتوان را برق سنجاب آتش است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زرپرستان نیستند از ظلمت غفلت ملول</p></div>
<div class="m2"><p>جامه کعبه است دود آن را که محراب آتش است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از هوسناکان برآرد درد و داغ عشق دود</p></div>
<div class="m2"><p>ما سمندر مشربان را بستر خواب آتش است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کار آتش می کند در سوختن سرمای سخت</p></div>
<div class="m2"><p>کشت ما را سردمهری های احباب آتش است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از خیال یار می پاشد دل نازک ز هم</p></div>
<div class="m2"><p>چون کتان در پیرهن ما را ز مهتاب آتش است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می شود جان های روشن تیره از تر دامنی</p></div>
<div class="m2"><p>در سیه رو کردن آیینه ها آب آتش است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در دل عاشق تمنا جای نتواند گرفت</p></div>
<div class="m2"><p>آرزوها چون سپند و جان بی تاب آتش است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ظلمت غفلت هوا گیرد چو دل روشن شود</p></div>
<div class="m2"><p>نور بیداری برای پرده خواب آتش است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شد ز اشک آتشینم خانه گردون سیاه</p></div>
<div class="m2"><p>دود جای گرد می خیزد چو سیلاب آتش است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آتشین جان چون سمندر شو که دیوان مرا</p></div>
<div class="m2"><p>سطرها دود دل است و سرخی باب آتش است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بس که صائب شد ز خشکی مستعد سوختن</p></div>
<div class="m2"><p>مغز ما سوداییان را نور مهتاب آتش است</p></div></div>