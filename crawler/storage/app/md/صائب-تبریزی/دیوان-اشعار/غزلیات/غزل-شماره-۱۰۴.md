---
title: >-
    غزل شمارهٔ ۱۰۴
---
# غزل شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>هر تنک ظرفی ننوشد خون گرم تاک را</p></div>
<div class="m2"><p>جامی از فولاد باید آب آتشناک را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقده دل را به زور اشک نتوان باز کرد</p></div>
<div class="m2"><p>گریه نتواند گره از دل گشودن تاک را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل در اصلاح ما بیهوده می سازد دماغ</p></div>
<div class="m2"><p>چون جنون دوری از سر می رود افلاک را؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صائب از فکر گلوسوز تو لذت می برد</p></div>
<div class="m2"><p>هر که می داند زبان شعله ادراک را</p></div></div>