---
title: >-
    غزل شمارهٔ ۱۵۱۳
---
# غزل شمارهٔ ۱۵۱۳

<div class="b" id="bn1"><div class="m1"><p>هر که دارد نظری واله زیبایی توست</p></div>
<div class="m2"><p>حلقه دام تو از چشم تماشایی توست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست هر چند در این سرو قدان کوتاهی</p></div>
<div class="m2"><p>علم این صف آراسته رعنایی توست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این که هر طایفه ای قبله خاصی دارند</p></div>
<div class="m2"><p>نیست بیجا، سببش جلوه هر جایی توست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مد احسان محیط تو رسا افتاده است</p></div>
<div class="m2"><p>لاف یکتایی هر قطره ز یکتایی توست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چه در حجله نازست رخت پرده نشین</p></div>
<div class="m2"><p>شور هر انجمن از انجمن آرایی توست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کیست بی پرده به خورشید نظر باز کند؟</p></div>
<div class="m2"><p>چشم پوشیده ما حجت پیدایی توست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زلف چون سرکشی از شانه تواند کردن؟</p></div>
<div class="m2"><p>نبض جان همه در پنجه گیرایی توست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>موج بی جنبش دریا ره خوابیده بود</p></div>
<div class="m2"><p>هر که را درد طلب هست ز جویایی توست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آب حیوان که سکندر ز تمنایش سوخت</p></div>
<div class="m2"><p>در سیه خانه مغزی است که سودایی توست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از لطافت نتوان یافت کجا می باشی</p></div>
<div class="m2"><p>جای رحم است بر آن کس که تماشایی توست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روزن از مهر جهانتاب بصیرت دارد</p></div>
<div class="m2"><p>نور آگاهی ما پرتو بینایی توست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کیست صائب که به توحید تو گویا گردد؟</p></div>
<div class="m2"><p>قوت بازوی کلکش ز توانایی توست</p></div></div>