---
title: >-
    غزل شمارهٔ ۲۵۷۸
---
# غزل شمارهٔ ۲۵۷۸

<div class="b" id="bn1"><div class="m1"><p>سایه بر هر کس که آن سرو خرامان افکند</p></div>
<div class="m2"><p>رعشه چون آب روانش بر رگ جان افکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق بالا دست هر کس را که برگیرد زخاک</p></div>
<div class="m2"><p>آسمان را بر زمین چون سایه آسان افکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرده ناموس نتواند حریف عشق شد</p></div>
<div class="m2"><p>بادبان چون پرده بر رخسار طوفان افکند؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از گلوی خود بریدن وقت حاجت همت است</p></div>
<div class="m2"><p>ورنه هر کس گاه سیری پیش سگ نان افکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که را شرم کرم در زیر دامان پرورد</p></div>
<div class="m2"><p>در دل شب سایلان را زر به دامان افکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که اینجا جمع سازد خویش را، فردای حشر</p></div>
<div class="m2"><p>خویش را چون قطره در دریای غفران افکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رحم کن بر ناتوانان کز دهان شکوه مور</p></div>
<div class="m2"><p>می تواند رخنه در ملک سلیمان افکند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر ضعیفان رحم کردن، رحم بر خود کردن است</p></div>
<div class="m2"><p>وای بر شیری که آتش در نیستان افکند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من چسان صائب نگهداری کنم خود را، که خضر</p></div>
<div class="m2"><p>خویش را دانسته در چاه زنخدان افکند</p></div></div>