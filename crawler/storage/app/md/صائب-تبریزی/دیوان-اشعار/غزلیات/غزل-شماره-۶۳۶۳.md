---
title: >-
    غزل شمارهٔ ۶۳۶۳
---
# غزل شمارهٔ ۶۳۶۳

<div class="b" id="bn1"><div class="m1"><p>مژگان او ز سنگ کند جوی خون روان</p></div>
<div class="m2"><p>از سنگ این خدنگ کند جوی خون روان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن بلبلم که دیدن بال شکسته ام</p></div>
<div class="m2"><p>از چشم سخت سنگ کند جوی خون روان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از ناخن شکسته چه آید، که این گره</p></div>
<div class="m2"><p>الماس را ز چنگ کند جوی خون روان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رخسار او دو آتشه چون گردد از شراب</p></div>
<div class="m2"><p>یاقوت را ز رنگ کند جوی خون روان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زلف مسلسل تو ز بسیاری گره</p></div>
<div class="m2"><p>شمشاد را ز چنگ کند جوی خون روان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر هر زمین که بگذرد ابرو کمان من</p></div>
<div class="m2"><p>با قد چون خدنگ کند جوی خون روان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از دیده نظارگیان سرو ناز من</p></div>
<div class="m2"><p>از روی لاله رنگ کند جوی خون روان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلهای پینه بسته ابنای روزگار</p></div>
<div class="m2"><p>از ناخن پلنگ کند جوی خون روان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با کشتی شکسته ما تا چها کند</p></div>
<div class="m2"><p>بحری که از نهنگ کند جوی خون روان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فریاد دلخراش تو صائب ز روی درد</p></div>
<div class="m2"><p>از سنگ بی درنگ کند جوی خون روان</p></div></div>