---
title: >-
    غزل شمارهٔ ۴۹۱۸
---
# غزل شمارهٔ ۴۹۱۸

<div class="b" id="bn1"><div class="m1"><p>برنمی آیم به تسکین دل خودکام خویش</p></div>
<div class="m2"><p>چون فلک در بیقراری دیده ام آرام خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>موجه بی دست و پا رادایه ای چون بحر نیست</p></div>
<div class="m2"><p>فارغم از فکر آغاز و غم انجام خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشمه امید رانتوان به خاک انباشتن</p></div>
<div class="m2"><p>ورنه می گشتم ز بی صیدی شکار دام خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکرستانی برای تلخکامان گشته است</p></div>
<div class="m2"><p>غفلت شیرین لبان ازلذت دشنام خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دردسر بسیار دارد دردمندیها که من</p></div>
<div class="m2"><p>سوختم چون لاله تاپرکردم ازخون جام خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حرص برمن دردهای نسیه راکرده است نقد</p></div>
<div class="m2"><p>صبح نا گردیده می افتم به فکر شام خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه مطلب نیست در پیغام دردآلود من</p></div>
<div class="m2"><p>خجلتی دارم،که نتوان گفت، از پیغام خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شرم یوسف مانع رسوایی یعقوب ماست</p></div>
<div class="m2"><p>چشم ما درپرده دارد جامه احرام خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قوت گویاییی تا در زبان خامه هست</p></div>
<div class="m2"><p>ثبت کن بر صفحه ایام صائب نام خویش</p></div></div>