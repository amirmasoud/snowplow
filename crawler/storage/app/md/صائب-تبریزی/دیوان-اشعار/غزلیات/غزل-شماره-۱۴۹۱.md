---
title: >-
    غزل شمارهٔ ۱۴۹۱
---
# غزل شمارهٔ ۱۴۹۱

<div class="b" id="bn1"><div class="m1"><p>کوثر زنده دلی چشم تر مردان است</p></div>
<div class="m2"><p>دل پر آبله درج گهر مردان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبح اقبالی اگر در افق امکان هست</p></div>
<div class="m2"><p>رخنه سینه و چاک جگر مردان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در مصافی که زند موج بلا جوهر تیغ</p></div>
<div class="m2"><p>تیغ از دست فکندن سپر مردان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر سری در خور اقبال، کلاهی دارد</p></div>
<div class="m2"><p>سایه دار فنا تاج سر مردان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سفر اهل جهان در طلب کام بود</p></div>
<div class="m2"><p>از سر کام گذشتن سفر مردان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر پریشان سفری راهنمایی دارد</p></div>
<div class="m2"><p>ذوق بی پا و سری راهبر مردان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کیست خورشید که از فیض نظر لاف زند؟</p></div>
<div class="m2"><p>چرخ او حلقه به گوش نظر مردان است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لعل و یاقوت به ناقص گهران ارزانی</p></div>
<div class="m2"><p>پاکی ظاهر و باطن گهر مردان است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نقد هر طایفه ای در خور همت باشد</p></div>
<div class="m2"><p>آسمان دامن پر سیم و زر مردان است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون سر دار ز دستار گذشتن سهل است</p></div>
<div class="m2"><p>هر که سر داد درین راه، سر مردان است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سرمه را چون به شبستان نظر بار دهند؟</p></div>
<div class="m2"><p>گرد غم چشم به راه نظر مردان است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آسیای فلک و گرد حوادث در وی</p></div>
<div class="m2"><p>نسخه ای از سر پر شور و شر مردان است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چرخ، سیبی است که طفلی به هوا افکنده است</p></div>
<div class="m2"><p>در مقامی که عروج نظر مردان است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>داغی از سینه عشاق گدایی داریم</p></div>
<div class="m2"><p>چون نخواهیم چراغی، گذر مردان است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در مقامی که سخن از هنر و عیب کنند</p></div>
<div class="m2"><p>عیب خود فاش نمودن هنر مردان است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مرده رفتم به خرابات، مسیحا گشتم</p></div>
<div class="m2"><p>این چه فیض است که با بوم و بر مردان است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به ته بار گرانسنگ امانت رفتند</p></div>
<div class="m2"><p>کوه در تاب ز تاب کمر مردان است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آب در دیده خورشید فلک گرداندن</p></div>
<div class="m2"><p>چشمه کاری ز فروغ گهر مردان است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>قسمت مردم بیدرد نگردد یارب!</p></div>
<div class="m2"><p>داغ ناسور که رزق جگر مردان است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کف خاکستر صائب نشود چون اکسیر؟</p></div>
<div class="m2"><p>روزگاری است که خاک گذر مردان است</p></div></div>