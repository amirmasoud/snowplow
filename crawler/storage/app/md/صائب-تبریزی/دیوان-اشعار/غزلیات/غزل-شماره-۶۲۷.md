---
title: >-
    غزل شمارهٔ ۶۲۷
---
# غزل شمارهٔ ۶۲۷

<div class="b" id="bn1"><div class="m1"><p>ز خامشی دل روشن شود سیاه مرا</p></div>
<div class="m2"><p>سیاه خیمه لیلی است دود آه مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز داغ زیر نگین من است روی زمین</p></div>
<div class="m2"><p>ز اشک و آه بود لشکر و سپاه مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو گردباد به سرگشتگی برآمده ام</p></div>
<div class="m2"><p>نمی رود دل گمره به هیچ راه مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حباب مانع جوش و خروش دریا نیست</p></div>
<div class="m2"><p>ز مغز، شور نگردد کم از کلاه مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمی توان به نصیحت عنان من پیچید</p></div>
<div class="m2"><p>نداشت زلف به زنجیرها نگاه مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به رنگ زرد ز دینار گشته ام قانع</p></div>
<div class="m2"><p>چو کهربا نپرد چشم بهر کاه مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیم به همسفران بار از تهیدستی</p></div>
<div class="m2"><p>که هست از دل صد پاره زاد راه مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حریف سرکشی نفس نیست یوسف من</p></div>
<div class="m2"><p>حضیض چاه بود به ز اوج جاه مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو شمع، زندگیم صرف شد به لرزیدن</p></div>
<div class="m2"><p>نشد که دست حمایت شود پناه مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا به جاذبه، ای میر کاروان دریاب</p></div>
<div class="m2"><p>که مانده کرد گرانباری گناه مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر چه گوهر من چشم می کند روشن</p></div>
<div class="m2"><p>نمی خرند عزیزان به برگ کاه مرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا به عالم بالا رساند یک جهتی</p></div>
<div class="m2"><p>نگشت کعبه و بتخانه سنگ راه مرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز زهد خشک مرا زنده زیر خاک مکن</p></div>
<div class="m2"><p>مبر ز میکده زاهد به خانقاه مرا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زبان عذر ندارم ز روسیاهی ها</p></div>
<div class="m2"><p>مگر شود عرق شرم عذرخواه مرا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مرا ز سیل گرانسنگ هیچ پروا نیست</p></div>
<div class="m2"><p>خراب بیش کند آب زیر کاه مرا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هزار لطف طمع داشتم ز ساده دلی</p></div>
<div class="m2"><p>نکرد چشم تو ممنون به یک نگاه مرا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کجاست فرصت مشق جنون مرا صائب؟</p></div>
<div class="m2"><p>که برده دست و دل از کار، مد آه مرا</p></div></div>