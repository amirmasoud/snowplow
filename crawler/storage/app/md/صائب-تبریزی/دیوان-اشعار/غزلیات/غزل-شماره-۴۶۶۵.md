---
title: >-
    غزل شمارهٔ ۴۶۶۵
---
# غزل شمارهٔ ۴۶۶۵

<div class="b" id="bn1"><div class="m1"><p>نفس هرزه مرس رابه کشیدن مگذار</p></div>
<div class="m2"><p>سرکش افتاد چو توسن به دویدن مگذار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نگه درنظروحشی آهو چشمان</p></div>
<div class="m2"><p>رام مردم شو و از دست رمیدن مگذار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماه کنعان نه عزیزی است که از دست دهند</p></div>
<div class="m2"><p>دامن صحبتش از دست بریدن مگذار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وعده توبه ز اهمال به پیری مفکن</p></div>
<div class="m2"><p>پنبه درگوش به انداز شنیدن مگذار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درجوانی سبک ازخواب گران کن خود را</p></div>
<div class="m2"><p>تن به این بار گران وقت خمیدن مگذار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوشه گیری پی تسخیر دل خلق مکن</p></div>
<div class="m2"><p>دام درخاک به انداز کشیدن مگذار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دردازان بیشتر افتاده که تقریر کنند</p></div>
<div class="m2"><p>خبرخسته مارابه شنیدن مگذار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برشریف است گران، منت احسان خسیس</p></div>
<div class="m2"><p>کاه بردیده به هنگام پریدن مگذار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شیراگر دایه قسمت ز تو امساک کند</p></div>
<div class="m2"><p>تو چو طفلان، سرانگشت مکیدن مگذار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در دهنها ز رسیدن ثمر خام افتد</p></div>
<div class="m2"><p>گرنه ای خام، تن خود به رسیدن مگذار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بی تأمل سخن خود مده از دل به زبان</p></div>
<div class="m2"><p>غنچه تا گل نشود دست به چیدن مگذار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این می لعل، زیاد از دهن تیغ تو نیست</p></div>
<div class="m2"><p>خون ما بیگنهان را به چکیدن مگذار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صائب این آن غزل شاه مطیعاست که گفت</p></div>
<div class="m2"><p>سجده وقت جوانی به خمیدن مگذار</p></div></div>