---
title: >-
    غزل شمارهٔ ۴۰۳۴
---
# غزل شمارهٔ ۴۰۳۴

<div class="b" id="bn1"><div class="m1"><p>سیه‌دلی که ز دوران حضور می‌جوید</p></div>
<div class="m2"><p>میان دوزخ سوزنده حور می‌جوید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی که چشم تسلی ز آرزو دارد</p></div>
<div class="m2"><p>علاج تشنگی از آب شور می‌جوید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه ساده‌لوح فتاده است آبگینه ما</p></div>
<div class="m2"><p>ز سنگلاخ حوادث حضور می‌جوید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسا که دست ندامت به سرزند آن کس</p></div>
<div class="m2"><p>که تخم ریحان در خاک شور می‌جوید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فریب نعمت الوان مخور که چرخ بخیل</p></div>
<div class="m2"><p>حساب پای ملخ را ز مور می‌جوید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>توان به سوز جگر شمع کشته را افروخت</p></div>
<div class="m2"><p>ز آفتاب عبث ماه نور می‌جوید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چگونه سر به گریبان خامشی نکشم</p></div>
<div class="m2"><p>زمانه‌ای است که طوفان تنور می‌جوید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلی که ملک سلیمان بر او چو زندان بود</p></div>
<div class="m2"><p>حصار عافیت از چشم مور می‌جوید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فلک همیشه طلبکار تنگ‌چشمان است</p></div>
<div class="m2"><p>که روی زشت ز حق چشم کور می‌جوید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نظر به صافدلان است عشق خونی را</p></div>
<div class="m2"><p>شراب رنگین جام بلور می‌جوید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه ساده‌لوح فتاده است صائب این زاهد</p></div>
<div class="m2"><p>که حق گذاشته حور و قصور می‌جوید</p></div></div>