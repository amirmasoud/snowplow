---
title: >-
    غزل شمارهٔ ۶۶۱
---
# غزل شمارهٔ ۶۶۱

<div class="b" id="bn1"><div class="m1"><p>ز هم نمی گسلد عیش جاودانه ما</p></div>
<div class="m2"><p>خمار صبح ندارد می شبانه ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترا که ذوق سخن نیست فکر ساغر کن</p></div>
<div class="m2"><p>که گشت چاک گریبان شرابخانه ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فسانه دگران خواب در بغل دارد</p></div>
<div class="m2"><p>به چشم خواب نمک می زند فسانه ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عرق فشانی ابر بهار رنگین است</p></div>
<div class="m2"><p>کنون که خال لب کشت گشت دانه ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به ناز کی چه میانش، چه جسم لاغر من</p></div>
<div class="m2"><p>دویی کناره گرفته است از میانه ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زمین ز برگ خزان دیده خرقه پوش شود</p></div>
<div class="m2"><p>اگر بهار کند رنگ عاشقانه ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کجاست دام فنا تا گلوی ما گیرد؟</p></div>
<div class="m2"><p>قفس خلال شد از فکر آب و دانه ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی نماند که بر آه ما نسوخت دلش</p></div>
<div class="m2"><p>سری کشید به هر روزنی زبانه ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خمار عشقت اگر دردسر دهد صائب</p></div>
<div class="m2"><p>سری بکش به غزل های عاشقانه ما</p></div></div>