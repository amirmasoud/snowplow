---
title: >-
    غزل شمارهٔ ۱۶۳۵
---
# غزل شمارهٔ ۱۶۳۵

<div class="b" id="bn1"><div class="m1"><p>خم چو گردد قد افراخته می باید رفت</p></div>
<div class="m2"><p>پل بر این آب چو شد ساخته می باید رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راه باریک عدم راه گرانباران نیست</p></div>
<div class="m2"><p>هر چه داری همه انداخته می باید رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنچه در کار بود ساختنش خودسازی ا ست</p></div>
<div class="m2"><p>گو مشو کار جهان ساخته، می باید رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سنگ راه است غم قافله و فکر رفیق</p></div>
<div class="m2"><p>فرد و تنها همه جا تاخته می باید رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به نفس طی نشود دامن صحرای عدم</p></div>
<div class="m2"><p>این ره دور، نفس باخته می باید رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا مگر شاهد مقصود مصور گردد</p></div>
<div class="m2"><p>دل چون آینه پرداخته می باید رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سپر راهرو از راهزنان عریانی است</p></div>
<div class="m2"><p>تیغ جان را ز نیام آخته می باید رفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این ره پر خس و خاشاک شود پاک به آه</p></div>
<div class="m2"><p>علم آه برافراخته می باید رفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من گرفتم که قمار از همه عالم بردی</p></div>
<div class="m2"><p>دست آخر همه را باخته می باید رفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این سفر همچو سفرهای دگر صائب نیست</p></div>
<div class="m2"><p>بار هستی ز خود انداخته می باید رفت</p></div></div>