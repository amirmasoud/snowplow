---
title: >-
    غزل شمارهٔ ۳۶۹۰
---
# غزل شمارهٔ ۳۶۹۰

<div class="b" id="bn1"><div class="m1"><p>به دیده آب اگر از آفتاب می‌گردد</p></div>
<div class="m2"><p>دل از نظاره روی تو آب می‌گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میی که چشم تو زان کاسه‌کاسه می‌نوشد</p></div>
<div class="m2"><p>به یک پیاله سر آفتاب می‌گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو چون به جلوه درآیی، ز شرمساری سرو</p></div>
<div class="m2"><p>ز طوق فاخته پا در رکاب می‌گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به غیر بوسه، که از سرگذشتگان دیگر</p></div>
<div class="m2"><p>حریف آن لب حاضرجواب می‌گردد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برآورند به رویش در بهشت به گل</p></div>
<div class="m2"><p>میان ما و تو هرکس حجاب می‌گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز خط نشد دل سخت تو مهربان، ورنه</p></div>
<div class="m2"><p>به چشم آینه زین دود آب می‌گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشو ز صبح بناگوش نوخطان غافل</p></div>
<div class="m2"><p>که هر دعا که کنی مستجاب می‌گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سپند غیرت من پای می‌کند قایم</p></div>
<div class="m2"><p>در آتشی که سمندر کباب می‌گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا به آب رسد خانه شکیب و قرار</p></div>
<div class="m2"><p>ز درد دیده هرکس پرآب می‌گردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فریب نعمت الوان چرا خورم صائب؟</p></div>
<div class="m2"><p>مرا که خون به جگر مشک ناب می‌گردد</p></div></div>