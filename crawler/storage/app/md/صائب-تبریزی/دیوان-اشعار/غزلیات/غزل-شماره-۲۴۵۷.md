---
title: >-
    غزل شمارهٔ ۲۴۵۷
---
# غزل شمارهٔ ۲۴۵۷

<div class="b" id="bn1"><div class="m1"><p>شمع روشن شد چو اشک از دیده بینا فشاند</p></div>
<div class="m2"><p>خوشه ای برداشت هر کس دانه ای اینجا فشاند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تجرد چون مسیحا هیچ کس نقصان نکرد</p></div>
<div class="m2"><p>پنجه خورشید شد دستی که بر دنیا فشاند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بهاران خلعت سر سبزی جاوید یافت</p></div>
<div class="m2"><p>هر که دامن بر ثمر چون سرو از استغنا فشاند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا نپیوستم به تیغ یار، جان صافی نشد</p></div>
<div class="m2"><p>گرد راه از دامن خود سیل در دریافشاند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم ما جز حسرت خشک از وصال او نبرد</p></div>
<div class="m2"><p>هر چه از دریا گرفت این ابر بر دریافشاند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برق عالمسوز ما را شهپر پرواز داد</p></div>
<div class="m2"><p>آن که خار از دشمنی در رهگذار ما فشاند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حاصل ابر از زمین شور، اشک تلخ شد</p></div>
<div class="m2"><p>این سزای آن که تخم خویش را بیجافشاند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست عزلت مانع کلفت که دست روزگار</p></div>
<div class="m2"><p>بر گهر گرد یتیمی در دل دریافشاند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از برومندی دل سودایی ما فارغ است</p></div>
<div class="m2"><p>تخم ما را سوخت عشق آن گاه بر صحرافشاند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قسمت آدم شد از روز ازل سرجوش فیض</p></div>
<div class="m2"><p>جام اول را به خاک آن ساقی رعنا فشاند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون گهر دارد همان گرد یتیمی بر جبین</p></div>
<div class="m2"><p>گرچه صائب از رگ ابر قلم دریا فشاند</p></div></div>