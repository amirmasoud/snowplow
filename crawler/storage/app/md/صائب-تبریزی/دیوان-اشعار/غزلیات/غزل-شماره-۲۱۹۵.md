---
title: >-
    غزل شمارهٔ ۲۱۹۵
---
# غزل شمارهٔ ۲۱۹۵

<div class="b" id="bn1"><div class="m1"><p>رگ در تنت از پاکی گوهر نتوان یافت</p></div>
<div class="m2"><p>در آینه صاف تو جوهر نتوان یافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر موی خط سبز ترا پیچش خاصی است</p></div>
<div class="m2"><p>یک حرف درین صفحه مکرر نتوان یافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقشی به فریبندگی آن خط موزون</p></div>
<div class="m2"><p>در سلسله موجه کوثر نتوان یافت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این فتنه که در نرگس نیلوفری توست</p></div>
<div class="m2"><p>در پرده نه طارم اخضر نتوان یافت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غافل مشو از حسن خط یار که این دور</p></div>
<div class="m2"><p>چون عهد جوانی است که دیگر نتوان یافت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا شانه صفت سر ننهی در سر این کار</p></div>
<div class="m2"><p>سر رشته آن زلف معنبر نتوان یافت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در جام می آویز که در عالم هستی</p></div>
<div class="m2"><p>بی نشأه می، عالم دیگر نتوان یافت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>راز دل عشاق چو خورشید عیان است</p></div>
<div class="m2"><p>یک نامه پیچیده به محشر نتوان یافت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در فکر اثر باش که جز آینه امروز</p></div>
<div class="m2"><p>شمعی به سر خاک سکندر نتوان یافت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گردن مکش از تیغ که جز حلقه فتراک</p></div>
<div class="m2"><p>در خلد ره از رخنه دیگر نتوان یافت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا بر دل صد پاره خود تنگ نگیری</p></div>
<div class="m2"><p>چون غنچه گل، دامن پر زر نتوان یافت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در ابر تنک، جلوه خورشید عیان است</p></div>
<div class="m2"><p>چون حسن ترا در ته چادر نتوان یافت؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کوتاه زبان شو که ز دندان ندامت</p></div>
<div class="m2"><p>زخمی به لب خامش ساغر نتوان یافت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>امرو به جز کلک گهربار تو صائب</p></div>
<div class="m2"><p>شاخی که دهد میوه گوهر نتوان یافت</p></div></div>