---
title: >-
    غزل شمارهٔ ۶۲۹۵
---
# غزل شمارهٔ ۶۲۹۵

<div class="b" id="bn1"><div class="m1"><p>اشک خونین نه ز هر آب و گل آید بیرون</p></div>
<div class="m2"><p>این گل از دامن صحرای دل آید بیرون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سالها غوطه به خوناب جگر باید خورد</p></div>
<div class="m2"><p>تا ز دل یک نفس معتدل آید بیرون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می رود منفعل از مجلس مستان خورشید</p></div>
<div class="m2"><p>هر که ناخوانده درآید خجل آید بیرون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست ممکن که ز همصحبتی آب روان</p></div>
<div class="m2"><p>سرو را پای اقامت ز گل آید بیرون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شیشه چرخ به جان سختی خود می نازد</p></div>
<div class="m2"><p>چه تماشاست که آن سنگدل آید بیرون!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پرده داغ دریدن گل بی ظرفیهاست</p></div>
<div class="m2"><p>لاله از تربت ما منفعل آید بیرون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه کند آتش دوزخ به جگر سوخته ای</p></div>
<div class="m2"><p>که ز دیوان قیامت خجل آید بیرون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تن پرستان همه مشغول تماشای خودند</p></div>
<div class="m2"><p>تا که از خود به تماشای دل آید بیرون؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگذر از دردسر سوزن عیسی صائب</p></div>
<div class="m2"><p>غم نه خاری است که از پای دل آید بیرون</p></div></div>