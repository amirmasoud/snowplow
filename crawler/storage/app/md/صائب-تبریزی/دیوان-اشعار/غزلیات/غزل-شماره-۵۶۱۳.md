---
title: >-
    غزل شمارهٔ ۵۶۱۳
---
# غزل شمارهٔ ۵۶۱۳

<div class="b" id="bn1"><div class="m1"><p>عالم بیخبری بود بهشت آبادم</p></div>
<div class="m2"><p>تا به هوش آمدم از عرش به فرش افتادم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق بر باد اگر داد باکی نیست</p></div>
<div class="m2"><p>می کشد جانب خود باز چو کاغذ بادم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیستم از کشش موجه رحمت نومید</p></div>
<div class="m2"><p>گر چه از قلزم رحمت به کار افتادم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>موجه ریگ روانم که به هر جنبش باد</p></div>
<div class="m2"><p>می زند غوطه به دریای عدم بنیادم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منم آن طفل بدآموز شکر خواب عدم</p></div>
<div class="m2"><p>که شب اول گورست شب میلادم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گره از غنچه پیکان نگشاید به نسیم</p></div>
<div class="m2"><p>نتوان کرد به افسون طرب دلشادم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از دم تیغ که هر دم به سرم می بارد</p></div>
<div class="m2"><p>می توان یافت که سهو القلم ایجادم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عزت عشق جهانسوز بود عزت من</p></div>
<div class="m2"><p>گر چه خاکسترم، از آتش سوزان زادم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوشمال عبثی می دهد استاد مرا</p></div>
<div class="m2"><p>سبقی نیست محبت که رود از یادم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اختیاری نبود نقش بد و نیک مرا</p></div>
<div class="m2"><p>کعبتینم که گرفتار کف نرادم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از گرفتاری من هست اگر عار ترا</p></div>
<div class="m2"><p>می توان کرد به یک چین جبین آزادم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چه عجب صائب اگر شد سخن من یکدست</p></div>
<div class="m2"><p>من که از فکر متین چون قلم فولادم</p></div></div>