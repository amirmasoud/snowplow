---
title: >-
    غزل شمارهٔ ۳۸۱۹
---
# غزل شمارهٔ ۳۸۱۹

<div class="b" id="bn1"><div class="m1"><p>به ناله ای ز دل ما چه درد می خیزد؟</p></div>
<div class="m2"><p>ز یک نسیم چه مقدار گرد می خیزد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگاه نرگس نیلوفری کشنده ترست</p></div>
<div class="m2"><p>که فتنه از فلک لاجورد می خیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر چو صبح کشم آفتاب را در بر</p></div>
<div class="m2"><p>همان ز سینه من آه سرد می خیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو شمع موی سرم آتشین به چشم آید</p></div>
<div class="m2"><p>ز خاک سوختگان سبزه زرد می خیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سپهر سفله که باشد که دست من گیرد؟</p></div>
<div class="m2"><p>ز خاک، مرد به امداد مرد می خیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خرابی دل ما لشکری نمی خواهد</p></div>
<div class="m2"><p>ز نام سیل ازین خانه گرد می خیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمی رسند به درد سخن سخن سنجان</p></div>
<div class="m2"><p>وگرنه ناله صائب ز درد می خیزد</p></div></div>