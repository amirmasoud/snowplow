---
title: >-
    غزل شمارهٔ ۱۳۶۸
---
# غزل شمارهٔ ۱۳۶۸

<div class="b" id="bn1"><div class="m1"><p>لفظ معنی شد، در آن تنگ دهن مأوا نیافت</p></div>
<div class="m2"><p>خرده گل آب شد، در غنچه او جا نیافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خودنمایی شیوه ما نیست در راه طلب</p></div>
<div class="m2"><p>گرد ما را هیچ کس در دامن صحرا نیافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشت از کوتاه دستی پر گهر جیب صدف</p></div>
<div class="m2"><p>موج از طول امل گوهر درین دریا نیافت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا نشد عالم سیه در چشم ساغر از خمار</p></div>
<div class="m2"><p>صبح امید از بیاض گردن مینا نیافت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست معجز را اثر در طینت آهن دلان</p></div>
<div class="m2"><p>چشم سوزن روشنی از صحبت عیسی نیافت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیمه تا بیرون نزد صائب ازین بستانسرا</p></div>
<div class="m2"><p>در حریم دیده خورشید، شبنم جا نیافت</p></div></div>