---
title: >-
    غزل شمارهٔ ۳۸۹
---
# غزل شمارهٔ ۳۸۹

<div class="b" id="bn1"><div class="m1"><p>نکرد از ناله شبخیز با خود گرمخون گل را</p></div>
<div class="m2"><p>نشد روشن چراغ از شعله آواز بلبل را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به ناز و سر گرانی روی از خوبان نمی تابم</p></div>
<div class="m2"><p>کند دندانه جان سخت من تیغ تغافل را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تراوش می کند راز نهان از مهر خاموشی</p></div>
<div class="m2"><p>که شبنم مانع از جولان نگردد نکهت گل را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گهر ز افتادگی از باغ پای تخت کرد آخر</p></div>
<div class="m2"><p>که می گوید ترقی نیست در طالع تنزل را؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل بی تاب از دست نوازش کی به حال آید؟</p></div>
<div class="m2"><p>نسازد پا فشردن بر زمین، ساکن تزلزل را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حصاری چون تحمل از حوادث نیست پیران را</p></div>
<div class="m2"><p>که از سیلاب گردد بردباری پشتبان پل را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ندارد آبهای تیره به ز استادگی صیقل</p></div>
<div class="m2"><p>مده در گفتگو از دست، دامان تأمل را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مکن از کسب دست خویش کوته چون گرانجانان</p></div>
<div class="m2"><p>منه بر کاهلی زنهار بنیاد توکل را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر زیر فلک جای اقامت هست و آسایش</p></div>
<div class="m2"><p>زمین بر گاو چون بسته است از روز ازل جل را؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>منم کز ناله خود چاک می سازم گریبان را</p></div>
<div class="m2"><p>وگرنه هست از گل صد گریبان چاک بلبل را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز خوی تند نتوان روی گردان گشت از خوبان</p></div>
<div class="m2"><p>به زخم خار نتوان داد از کف دامن گل را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نباشد امتدادی دولت سر در هوایان را</p></div>
<div class="m2"><p>کند خط عاقبت کوتاه، دست زلف و کاکل را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بود گلبانگ بهتر از زر گل قدردانان را</p></div>
<div class="m2"><p>چمن پیرا ز سیر باغ مانع نیست بلبل را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نشد چشم مرا خواب پریشان از گرستن کم</p></div>
<div class="m2"><p>بود با چشمه ساران ریشه پیوند سنبل را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز تمکین سرمه می گردد خروش سیل را صائب</p></div>
<div class="m2"><p>اگر کوه گران از من سبق گیرد تحمل را</p></div></div>