---
title: >-
    غزل شمارهٔ ۴۹۱۵
---
# غزل شمارهٔ ۴۹۱۵

<div class="b" id="bn1"><div class="m1"><p>نیست رزقم تیر تخشی چون کمان ازتیر خویش</p></div>
<div class="m2"><p>قسمتم خمیازه خشکی است از نخجیر خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه صید لاغر من لایق فتراک نیست</p></div>
<div class="m2"><p>می توان کردن به سوزن امتحان شمشیر خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا توان چون خضر شد معمار دیوار یتیم</p></div>
<div class="m2"><p>ازمروت نیست کردن سعی درتعمیر خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جاهل از کفران کند زرق حلال خود حرام</p></div>
<div class="m2"><p>طفل از پستان گزیدن می کند خون شیر خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکقلم گردید پای آهوان خلخال دار</p></div>
<div class="m2"><p>بس که پاشیدم به صحرا دانه زنجیر خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون گهر بر من کسی را دل درین دریا نسوخت</p></div>
<div class="m2"><p>کردم از گرد یتیمی عاقبت تعمیر خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زنگ بست از مهر خاموشی مراتیغ زبان</p></div>
<div class="m2"><p>چند در زیر سپر پنهان کنم شمشیر خویش؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن ستمگر را پشیمان از دل آزادی نکرد</p></div>
<div class="m2"><p>زیر بار منتم از آه بی تأثیر خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست در ظاهر مراصائب اگر نقدی به دست</p></div>
<div class="m2"><p>زیر بار منتم ازآه بی تأثیر خویش</p></div></div>