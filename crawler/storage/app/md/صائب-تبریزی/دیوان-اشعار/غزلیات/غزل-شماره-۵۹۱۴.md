---
title: >-
    غزل شمارهٔ ۵۹۱۴
---
# غزل شمارهٔ ۵۹۱۴

<div class="b" id="bn1"><div class="m1"><p>از بخت سیه پست نگردید نوایم</p></div>
<div class="m2"><p>از سرمه شب بیش شد آواز درایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون از جگر آهن و فولاد گشاید</p></div>
<div class="m2"><p>چون ریزه الماس، خراشیده صدایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر سبزه خوابیده که در باغ جهان بود</p></div>
<div class="m2"><p>از خواب گران جست ز گلبانگ رسایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوری ز خرابات نه از خشکی زهدست</p></div>
<div class="m2"><p>ترسم گرو باده نگیرند ردایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون سرو گذشتم ز ثمر تا شوم آزاد</p></div>
<div class="m2"><p>صد سلسله از برگ نهادند به پایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در فکر گشاد دل من بس که فرو رفت</p></div>
<div class="m2"><p>افزود به دل عقده ای از عقده گشایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب ز سر خود به ته بال کشیدن</p></div>
<div class="m2"><p>عمری است که در سایه اقبال همایم</p></div></div>