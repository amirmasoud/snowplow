---
title: >-
    غزل شمارهٔ ۳۹۱۴
---
# غزل شمارهٔ ۳۹۱۴

<div class="b" id="bn1"><div class="m1"><p>مرا اگر چه کم از خاک راه می گیرند</p></div>
<div class="m2"><p>ز من فروغ گهر مهر وماه می گیرند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهوش باش که دیوانگان وادی عشق</p></div>
<div class="m2"><p>غزال را به کمند نگاه می گیرند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مباش تند که نتوان ز آفتاب گرفت</p></div>
<div class="m2"><p>تمنعی که ز رخسار ماه می گیرند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مدار دست ز دامان شب که غنچه دلان</p></div>
<div class="m2"><p>گشایش از نفس صبحگاه می گیرند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مکن ز پاکی دامن به بیگناهان فخر</p></div>
<div class="m2"><p>که که در دیار کرم بیگناه می گیرند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به مشت خار ضعیفان به چشم کم منگر</p></div>
<div class="m2"><p>که سیل حادثه را پیش راه می گیرند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چگونه منکر عصیان شوی که اهل حساب</p></div>
<div class="m2"><p>ز دست و پای تو اول گواه می گیرند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فغان ز پله انصاف این گرانجانان</p></div>
<div class="m2"><p>که کوه درد مرا برگ کاه می گیرند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز تاب آتش روی تو عافیت طلبان</p></div>
<div class="m2"><p>به آفتاب قیامت پناه می گیرند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر چه گرمروان همچو برق در گذرند</p></div>
<div class="m2"><p>ز نقش پای چراغی به راه می گیرند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ستمگران که به مظلوم می شوند طرف</p></div>
<div class="m2"><p>ز غفلت آینه در پیش آه می گیرند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به قدر آنچه شوی پست سربلندشوی</p></div>
<div class="m2"><p>عیار جاه عزیزان ز چاه می گیرند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شکستن دل ما را پری رخان صائب</p></div>
<div class="m2"><p>کم از شکستن طرف کلاه می گیرند</p></div></div>