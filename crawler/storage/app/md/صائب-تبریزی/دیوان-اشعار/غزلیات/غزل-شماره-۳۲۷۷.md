---
title: >-
    غزل شمارهٔ ۳۲۷۷
---
# غزل شمارهٔ ۳۲۷۷

<div class="b" id="bn1"><div class="m1"><p>عقده چون وقت رسد عقده‌گشا می‌گردد</p></div>
<div class="m2"><p>غنچه ممنون عبث از باد صبا می‌گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنگ روشنگر آیینه ما می‌گردد</p></div>
<div class="m2"><p>در پری‌خانه ما جغد هما می‌گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درد صاف از دل خوش‌مشرب ما می‌گردد</p></div>
<div class="m2"><p>در پری‌خانه ما جغد هما می‌گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل محال است ز دلدار شود روگردان</p></div>
<div class="m2"><p>هر طرف قبله بود قبله‌نما می‌گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خبر از سایه خود آهوی وحشی را نیست</p></div>
<div class="m2"><p>دل سرگشته چه دانم که کجا می‌گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم کوته‌نظران حلقه بیرون در است</p></div>
<div class="m2"><p>ورنه آن سرو روان در همه‌جا می‌گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌شود حلقه فتراک بر او دامن دشت</p></div>
<div class="m2"><p>از کمند تو شکاری که رها می‌گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیم آن فاخته کآزاد توان کرد مرا</p></div>
<div class="m2"><p>سرو را طوق من انگشتر پا می‌گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>محملی را که درین بادیه من می‌طلبم</p></div>
<div class="m2"><p>نه فلک در طلبش آبله پا می‌گردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رهنوردی که درین بادیه هموار رود</p></div>
<div class="m2"><p>خار در رهگذرش دست دعا می‌گردد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاه دریوزه همت ز فقیران دارد</p></div>
<div class="m2"><p>می‌رسد هرکه به درویش گدا می‌گردد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کاش اندیشه ما در دل او ره می‌داشت</p></div>
<div class="m2"><p>آن که پیوسته در اندیشه ما می‌گردد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سختی راه شود سنگ‌فسان رهرو را</p></div>
<div class="m2"><p>نرمی راه حنای کف پا می‌گردد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن که بر آتش بی‌تابی گل آب نزد</p></div>
<div class="m2"><p>کی چراغ سر خاک شهدا می‌گردد؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عمر چون باد به این سرعت اگر خواهد رفت</p></div>
<div class="m2"><p>دانه و کاه ز هم زود جدا می‌گردد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>می‌شود خس ز قبول نظر خلق شریف</p></div>
<div class="m2"><p>کاه اگر قیمتی از کاهربا می‌گردد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بی‌عصایی است درین راه دلیل کوری</p></div>
<div class="m2"><p>هرکه بیناست در اینجا به عصا می‌گردد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در بیابان طلب راهروان را شب‌ها</p></div>
<div class="m2"><p>نفس سوخته‌ام راهنما می‌گردد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>قامت هرکه خم از بار عبادت گردید</p></div>
<div class="m2"><p>قبله حاجت و محراب دعا می‌گردد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فکر صائب نه کلامی است کز او سیر شوند</p></div>
<div class="m2"><p>تشنه سیراب کی از آب بقا می‌گردد؟</p></div></div>