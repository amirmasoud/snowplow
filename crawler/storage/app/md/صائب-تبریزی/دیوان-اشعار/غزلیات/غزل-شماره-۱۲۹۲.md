---
title: >-
    غزل شمارهٔ ۱۲۹۲
---
# غزل شمارهٔ ۱۲۹۲

<div class="b" id="bn1"><div class="m1"><p>در نگارستان تهمت دامن گل پاک نیست</p></div>
<div class="m2"><p>گر همه پیراهن یوسف بود، بی چاک نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ثابت و سیار او سوزانتر از یکدیگرند</p></div>
<div class="m2"><p>آتش افسرده در خاکستر افلاک نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آسمان از گریه ما خاکساران فارغ است</p></div>
<div class="m2"><p>باغبان را هیچ پروایی سرشک تاک نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما سمندر مشربان را کی تواند صید کرد؟</p></div>
<div class="m2"><p>از می گلگون رخ بزمی که آتشناک نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاک بر فرقش اگر از کبر سر بالا کند</p></div>
<div class="m2"><p>هر که داند بازگشت او به غیر از خاک نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طره دستار می باید که باشد زرنگار</p></div>
<div class="m2"><p>اهل ظاهر را نظر بر شعله ادارک نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل به روی راست، خال او ز مردم می برد</p></div>
<div class="m2"><p>خانه صیاد اینجا از خس وخاشاک نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما ز هر روزن سری چون مهر بیرون کرده ایم</p></div>
<div class="m2"><p>روزن جنت به غیر از حلقه فتراک نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون تواند صبح پیش سینه من شد سفید؟</p></div>
<div class="m2"><p>در بساط صبح بیش از یک گریبان، چاک نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جاده چون مار سیه آوارگان را می گزد</p></div>
<div class="m2"><p>ما و آن راهی که دام نقش پا در خاک نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روزگارم تیره صائب زین سواد ناقص است</p></div>
<div class="m2"><p>شمع در ویرانه ام از شعله ادراک نیست</p></div></div>