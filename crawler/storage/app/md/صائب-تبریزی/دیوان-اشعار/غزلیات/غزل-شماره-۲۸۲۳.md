---
title: >-
    غزل شمارهٔ ۲۸۲۳
---
# غزل شمارهٔ ۲۸۲۳

<div class="b" id="bn1"><div class="m1"><p>نسیم صبحگاه از غنچه ام دلگیر برگردد</p></div>
<div class="m2"><p>گره چون محکم افتد ناخن تدبیر برگردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بشو دست از دل دیوانه چون گردید صحرایی</p></div>
<div class="m2"><p>که ممکن نیست کس زان خاک دامنگیر برگردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زجان سیرست هر کس از حریم عشق می آید</p></div>
<div class="m2"><p>که مهمان از سر خوان کریمان سیر برگردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غم از دل می برد نظاره لبهای میگونش</p></div>
<div class="m2"><p>چه صورت دارد از میخانه کس دلگیر برگردد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نظر چون عاشق بیتاب بردارد زرخساری</p></div>
<div class="m2"><p>که از گرداندن او چهره تصویر برگردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دل برگشت گرد آلود خجلت آهم از گردون</p></div>
<div class="m2"><p>چو صیادی که دست خالی از نخجیر برگردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به همواری توان مغلوب کردن خصم سرکش را</p></div>
<div class="m2"><p>زروی نرم موم اینجا دم شمشیر برگردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگرچه سنگ طفلان توتیا کرد استخوانش را</p></div>
<div class="m2"><p>نشد مجنون ما از کوچه زنجیر برگردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برات قسمت حق گرچه برگشتن نمی داند</p></div>
<div class="m2"><p>زبخت واژگون ما به پستان شیر برگردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کمان چرخ را زه می کند گردن فرازیها</p></div>
<div class="m2"><p>اگر دزدد هدف سر در گریبان، تیر برگردد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به زخم اولین از عشق بی پروا قناعت کن</p></div>
<div class="m2"><p>به صید کشته خود نیست ممکن شیر برگردد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ندارد حاصلی با سخت رویان گفتگو صائب</p></div>
<div class="m2"><p>که چون باشد هدف از سنگ خارا، تیر برگردد</p></div></div>