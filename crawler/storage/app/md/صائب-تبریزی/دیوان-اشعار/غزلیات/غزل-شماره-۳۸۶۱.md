---
title: >-
    غزل شمارهٔ ۳۸۶۱
---
# غزل شمارهٔ ۳۸۶۱

<div class="b" id="bn1"><div class="m1"><p>ز چهره حال دل زار من عیان باشد</p></div>
<div class="m2"><p>که از شکستن دل رنگ ترجمان باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز عمر رفته مراآه حسرت است نصیب</p></div>
<div class="m2"><p>که گرد لازم دنبال کاروان باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز عمر چشم اقامت مدار با قد خم</p></div>
<div class="m2"><p>مبند دل خدنگی که در کمان باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لبم ز شکوه خونین نمیشود رنگین</p></div>
<div class="m2"><p>دهان زخم مرا تیغ اگر زبان باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امین مخزن گوهر کنند بی سخنش</p></div>
<div class="m2"><p>چو ماهی آن که درین بحر بی زبان باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به هر کجا که نشینم خجل ز جای خودم</p></div>
<div class="m2"><p>نظر به پایه من صدرآستان باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز کیسه تو کند خرج هر که محتاج است</p></div>
<div class="m2"><p>کلید گنج تو در دست سایلان باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بغیر خط که ز لعل لب تو سر زده است</p></div>
<div class="m2"><p>که دیده آتش یاقوت را دخان باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دمی که صرف به ذکر خدا شود صائب</p></div>
<div class="m2"><p>هزار بار به از عمر جاودان باشد</p></div></div>