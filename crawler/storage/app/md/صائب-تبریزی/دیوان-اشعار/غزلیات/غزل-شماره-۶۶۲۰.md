---
title: >-
    غزل شمارهٔ ۶۶۲۰
---
# غزل شمارهٔ ۶۶۲۰

<div class="b" id="bn1"><div class="m1"><p>چون غافل است دل ز حق از دل چه فایده؟</p></div>
<div class="m2"><p>بی لیلی از نظاره محمل چه فایده؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیری ز مال نیست تهی چشم حرص را</p></div>
<div class="m2"><p>غربال را ز کثرت حاصل چه فایده؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از وصل شد تردد خاطر فزون مرا</p></div>
<div class="m2"><p>پروانه را ز بودن محفل چه فایده؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون هست در تصرف دریاعنان موج</p></div>
<div class="m2"><p>رفتن نفس گسسته به ساحل چه فایده؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زنجیر موج مانع رفتار سیل نیست</p></div>
<div class="m2"><p>بی تاب شوق را ز سلاسل چه فایده؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پر زر نساخت چون دهن عندلیب را</p></div>
<div class="m2"><p>گل را ز نقد خویش چه حاصل، چه فایده؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>موج سراب سلسله جنبان تشنگی است</p></div>
<div class="m2"><p>حق جوی را ز عالم باطل چه فایده؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون می شود زیاده ز ایثار، سیم و زر</p></div>
<div class="m2"><p>بستن در سؤال به سایل چه فایده؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا شهرت است مطلب از احسان سیم و زر</p></div>
<div class="m2"><p>از ریزش کریم چه حاصل، چه فایده؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیکان دلش ز خنده سوفار وا نشد</p></div>
<div class="m2"><p>چون نیست خرمی ز ته دل چه فایده؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون گرد خجلت از رخ قاتل نمی برد</p></div>
<div class="m2"><p>صائب ز پرفشانی بسمل چه فایده؟</p></div></div>