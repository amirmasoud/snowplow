---
title: >-
    غزل شمارهٔ ۴۲۳۶
---
# غزل شمارهٔ ۴۲۳۶

<div class="b" id="bn1"><div class="m1"><p>دولت ز دستگیری مردم بپابود</p></div>
<div class="m2"><p>فانوس این چراغ ز دست دعابود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون غنچه هست اگر دل جمعی درین چمن</p></div>
<div class="m2"><p>در گلشن همیشه بهار رضا بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دستی که شد بریده ز دامان اختیار</p></div>
<div class="m2"><p>دایم چو بهله در کمر مدعا بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بیقراری تو جهان است بیقرار</p></div>
<div class="m2"><p>شوریده نیست عالم اگر دل بجا بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از راست کردن نفسی می رود به باد</p></div>
<div class="m2"><p>هر سر که چون حباب اسیر هوا بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>انصاف نیست بار شدن بر شکستگان</p></div>
<div class="m2"><p>پهلوی خشک خویش مرا بوریا بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر دل که نیست یاد خدا در حریم او</p></div>
<div class="m2"><p>سرگشته تر ز کشتی بی ناخدابود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تیغ کج است پیش سیه دل حدیث راست</p></div>
<div class="m2"><p>فرعون را به چشم عصااژدها بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب بود ز سایه سریع الزوال تر</p></div>
<div class="m2"><p>پرواز دولتی که به بال هما بود</p></div></div>