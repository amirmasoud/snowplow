---
title: >-
    غزل شمارهٔ ۶۴۵
---
# غزل شمارهٔ ۶۴۵

<div class="b" id="bn1"><div class="m1"><p>ز داغ نیست محابا به درد ساخته را</p></div>
<div class="m2"><p>که آتش است گلستان، زر گداخته را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان به عهد تو آیین سرکشی شد عام</p></div>
<div class="m2"><p>که در بغل ندهد سرو جای، فاخته را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو گل ز چهره رنگین به خار غوطه زدیم</p></div>
<div class="m2"><p>شناختیم کنون قدر رنگ باخته را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز شرم خنجر مژگان بر کشیده او</p></div>
<div class="m2"><p>به خاک کرد نهان مهر، تیغ آخته را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به یک دو هفته مه چارده هلالی شد</p></div>
<div class="m2"><p>دوام نیست ازین بیش حسن ساخته را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکسته حالی ما شد حصار ما صائب</p></div>
<div class="m2"><p>که از خزان نبود بیم، رنگ باخته را</p></div></div>