---
title: >-
    غزل شمارهٔ ۶۹۹۱
---
# غزل شمارهٔ ۶۹۹۱

<div class="b" id="bn1"><div class="m1"><p>مرکز عیش است آن دهان که تو داری</p></div>
<div class="m2"><p>عمر دوباره است آن لبان که تو داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دل یاقوت آه سرد برآرد</p></div>
<div class="m2"><p>این لب لعل گهرفشان که تو داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خانه صبر مرا به آب رساند</p></div>
<div class="m2"><p>این گل روی عرق فشان که تو داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرد برآرد ز شیشه خانه دلها</p></div>
<div class="m2"><p>این دل سنگین بی امان که تو داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم تماشاییان چو حلقه رباید</p></div>
<div class="m2"><p>این قد و بالای چون سنان که تو داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حلقه کند زود نام شهرت یاقوت</p></div>
<div class="m2"><p>گرد لب آن خط دلستان که تو داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نقطه موهوم را دو نیم نماید</p></div>
<div class="m2"><p>در دهن تنگ آن زبان که تو داری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پنجه خورشید را چو موم گدازد</p></div>
<div class="m2"><p>این نگه آتشین عنان که تو داری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در جگر زهد خشک شیشه شکسته است</p></div>
<div class="m2"><p>این لب میگون می چکان که تو داری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هیچ کس از هیچ، هیچ چیز نخواهد</p></div>
<div class="m2"><p>ایمنی از بوسه زان دهان که تو داری!</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چین جبینی بس است کشتن ما را</p></div>
<div class="m2"><p>تیر نمی خواهد این کمان که تو داری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صائب مسکین کناره کرد ز عالم</p></div>
<div class="m2"><p>تا به کنار آرد آن میان که تو داری</p></div></div>