---
title: >-
    غزل شمارهٔ ۳۷۹۸
---
# غزل شمارهٔ ۳۷۹۸

<div class="b" id="bn1"><div class="m1"><p>ملال در دل آزاده جا نمی گیرد</p></div>
<div class="m2"><p>زمین ساده ما نقش پا نمی گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حریف پرتو منت نمی شود دل من</p></div>
<div class="m2"><p>ز صیقل آینه من جلا نمی گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کجا دراز شود پیش این سیاه دلان؟</p></div>
<div class="m2"><p>که رنگ، دست غیور از حنا نمی گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سری به افسر آزادگی سزاوارست</p></div>
<div class="m2"><p>که جا به سایه بال هما نمی گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هرکه نیست به حق آشنا، ندارد کار</p></div>
<div class="m2"><p>سگی است نفس که جز آشنا نمی گیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چگونه بلبل ازین گلستان کند پرواز؟</p></div>
<div class="m2"><p>که شبنم از رخ گلها هوا نمی گیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سبک ز دشت وجود آنچنان گذر کردیم</p></div>
<div class="m2"><p>که خون آبله ای پای ما نمی گیرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر سفر کنی از خویش در جوانی کن</p></div>
<div class="m2"><p>که جای پای سبکرو عصا نمی گیرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کریم را ز طرف نیست چشم استحقاق</p></div>
<div class="m2"><p>به کفر، رزق ز کافر خدا نمی گیرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر ز اهل دلی از گزند ایمن باش</p></div>
<div class="m2"><p>سگ محله عشق آشنا نمی گیرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کشیده ام ز طمع دست خود چنان صائب</p></div>
<div class="m2"><p>که نقش، پهلویم از بوریا نمی گیرد</p></div></div>