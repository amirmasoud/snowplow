---
title: >-
    غزل شمارهٔ ۵۶۷۲
---
# غزل شمارهٔ ۵۶۷۲

<div class="b" id="bn1"><div class="m1"><p>خط به اوراق جهان، دیده و نادیده زدیم</p></div>
<div class="m2"><p>پشت دستی به گل چیده و ناچیده زدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دم از ماتم برگی نتوان آه کشید</p></div>
<div class="m2"><p>چار تکبیر بر این نخل خزان دیده زدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حاصل ما ز عزیزان سفر کرده خویش</p></div>
<div class="m2"><p>مشت آبی است که بر آینه دیده زدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هدف ناوک دلدوز مکافات شدیم</p></div>
<div class="m2"><p>بر سر خاری اگر پای نفهمیده زدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قدم از چشم نمودند سبک رفتاران</p></div>
<div class="m2"><p>ما درین بادیه تن چون ره خوابیده زدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خار سیلاب پریشان نظری خواهد شد</p></div>
<div class="m2"><p>بخیه ای کز مژه بر دیده نادیده زدیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برشکست خزف اکنون دل ما می لرزد</p></div>
<div class="m2"><p>گر چه بر سنگ دوصد گوهر سنجیده زدیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد گرانخواب تر از کوشش ما صائب بخت</p></div>
<div class="m2"><p>لگدی چند بر این سبزه خوابیده زدیم</p></div></div>