---
title: >-
    غزل شمارهٔ ۲۵۵۸
---
# غزل شمارهٔ ۲۵۵۸

<div class="b" id="bn1"><div class="m1"><p>فکر جمعیت عبث دل را پریشان می‌کند</p></div>
<div class="m2"><p>آن که سر داده است ما را فکر سامان می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نرم کن دل را به آه آتشین کاین مشت خون</p></div>
<div class="m2"><p>سخت چون گردید، در تن کار پیکان می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکه زد بر آتش خشم آب، مانند خلیل</p></div>
<div class="m2"><p>آتش سوزنده را بر خود گلستان می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌کند از راه احسان بنده صد دیوانه را</p></div>
<div class="m2"><p>کودکان را هر که آزاد از دبستان می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لذت آزادگی یار بر او بادا حرام</p></div>
<div class="m2"><p>بنده خود خلق را هرکس به احسان می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خرج ابر از کیسه دریاست، حیرانم چرا</p></div>
<div class="m2"><p>این قدر استادگی با تشنه جانان می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غیرت آرد خون بحر پاک گوهر را به جوش</p></div>
<div class="m2"><p>چون صدف لب باز پیش ابر نیسان می‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در چنین وقتی که می‌ریزد ز هم اوراق عمر</p></div>
<div class="m2"><p>صائب از غفلت همان ترتیب دیوان می‌کند</p></div></div>