---
title: >-
    غزل شمارهٔ ۶۶۱۲
---
# غزل شمارهٔ ۶۶۱۲

<div class="b" id="bn1"><div class="m1"><p>روشن ز نور صدق بود جان صبحگاه</p></div>
<div class="m2"><p>بی وجه نیست چهره خندان صبحگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز انجم سپهر زر همه شب جمع می کند</p></div>
<div class="m2"><p>بهر نیاز مقدم سلطان صبحگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمر ابد که یافت ز آب حیات، خضر</p></div>
<div class="m2"><p>یک مد نارساست ز دیوان صبحگاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر خضر یافت زندگی از آب زندگی</p></div>
<div class="m2"><p>عالم حیات یافت ز احسان صبحگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون آب خضر نیست سیه کاسه و بخیل</p></div>
<div class="m2"><p>عام است فیض چشمه حیوان صبحگاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از نور صدق، دیده شوخ ستارگان</p></div>
<div class="m2"><p>گردید محو چهره تابان صبحگاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بادام چشم را به شکر غوطه ها دهد</p></div>
<div class="m2"><p>قند مکرر لب خندان صبحگاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آید ز فیض صدق طلب بی مزاحمت</p></div>
<div class="m2"><p>گرم از تنور سرد برون نان صبحگاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون مغز پسته غوطه به تنگ شکر دهد</p></div>
<div class="m2"><p>طوطی چرخ را شکرستان صبحگاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا از شفق نگشته به خون شیر او بدل</p></div>
<div class="m2"><p>بردار کام خویش ز پستان صبحگاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وقت طلوع را به شکرخواب مگذران</p></div>
<div class="m2"><p>چشم آب ده ز شمسه ایوان صبحگاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زنهار رومتاب ازین آستان که هست</p></div>
<div class="m2"><p>چرخ از ستاره، ریزه خور خوان صبحگاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فریاد مرغکان سحرخیز این بود</p></div>
<div class="m2"><p>کای خوابناک، جان تو و جان صبحگاه!</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در دیده تو پرده خواب دگر شود</p></div>
<div class="m2"><p>خالی اگر کنند نمکدان صبحگاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صائب رخ گشاده بود مشرق امید</p></div>
<div class="m2"><p>کوته مساز دست ز دامان صبحگاه</p></div></div>