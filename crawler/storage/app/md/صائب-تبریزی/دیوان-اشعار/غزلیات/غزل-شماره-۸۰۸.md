---
title: >-
    غزل شمارهٔ ۸۰۸
---
# غزل شمارهٔ ۸۰۸

<div class="b" id="bn1"><div class="m1"><p>از خلق خبر نیست ز خود بی خبران را</p></div>
<div class="m2"><p>با قافله کاری نبود فرد روان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسودگی و درد طلب آتش و آب است</p></div>
<div class="m2"><p>منزل نبود قافله ریگ روان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل سرد چو گردید ز دنیا، نشود بند</p></div>
<div class="m2"><p>حاجت به محرک نبود برگ خزان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای جذبه توفیق، به همت مددی کن</p></div>
<div class="m2"><p>شاید که به منزل برم این بار گران را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از عشق شود چاشنی عمر دو بالا</p></div>
<div class="m2"><p>بی جوش، قوامی نبود شیره جان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از گرد کدورت شود آیینه دل صاف</p></div>
<div class="m2"><p>بارست به دل، صافی می دردکشان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما را سر پرخاش فلک نیست، وگرنه</p></div>
<div class="m2"><p>سهل است رساندن به زمین پشت کمان را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در سینه ما قطره نشد گوهر شهوار</p></div>
<div class="m2"><p>تا همچو صدف مهر نکردیم دهان را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از دخل کج اندیشه ندارند سخن راست</p></div>
<div class="m2"><p>از ناوک کجرو خطری نیست نشان را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از ساده دلی هر که دهد پند به مغرور</p></div>
<div class="m2"><p>بیدار به افسانه کند خواب گران را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با آن دل آهن چه کند جوشن داود</p></div>
<div class="m2"><p>کز سنگ برآرد چو شرر خرده جان را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از دانه اثر نیست درین خرمن بی مغز</p></div>
<div class="m2"><p>از آه چه بر باد دهم کاهکشان را؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون نافه شود از نفست خون جگر مشک</p></div>
<div class="m2"><p>از غیبت اگر پاک کنی کام و دهان را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>با سوخته جانان چه کند حرف جگرسوز؟</p></div>
<div class="m2"><p>از داغ محابا نبود لاله ستان را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از رخنه شود سینه الماس مشبک</p></div>
<div class="m2"><p>مژگان کج او چه کند راست سنان را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>این بادیه غربال بود از چه خس پوش</p></div>
<div class="m2"><p>صائب به دو صد دست نگه دار عنان را</p></div></div>