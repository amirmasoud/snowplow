---
title: >-
    غزل شمارهٔ ۳۹۹۵
---
# غزل شمارهٔ ۳۹۹۵

<div class="b" id="bn1"><div class="m1"><p>اگر به پیرهن گل وگلاب باز آید</p></div>
<div class="m2"><p>امید هست به جوی من آب باز آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکسته پر وبالم درست خواهد شد</p></div>
<div class="m2"><p>به آشیانه چو مرغ کباب باز آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رم از طبیعت آهوی چشم اگر برود</p></div>
<div class="m2"><p>امید هست که عمر از شتاب بازآید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حضور رفته ز دوران مجوی هیهات است</p></div>
<div class="m2"><p>که شبنم از سفر آفتاب باز آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوبار اهل نظر را به آب نتوان راند</p></div>
<div class="m2"><p>به چشم من کی از افسانه خواب باز آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو ایستاد ز گردش کباب می سوزد</p></div>
<div class="m2"><p>چنان مکن که دل از اضطراب باز آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلم ز آینه رویان به سینه برگردید</p></div>
<div class="m2"><p>به حسرتی که سکندر ز آب باز آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عنان آه توان باز زد ز لب صائب</p></div>
<div class="m2"><p>اگر به روزن دود کباب باز آید</p></div></div>