---
title: >-
    غزل شمارهٔ ۴۵۲۶
---
# غزل شمارهٔ ۴۵۲۶

<div class="b" id="bn1"><div class="m1"><p>چه کار از یاری دوران برآید</p></div>
<div class="m2"><p>به همت کارها آسان برآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرآید چون زمان ناامیدی</p></div>
<div class="m2"><p>به خواب یوسف از زندان برآید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم از کودک مزاجیهای حرص است</p></div>
<div class="m2"><p>که در صد سالگی دندان برآید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمی گیرد تنور سردنان را</p></div>
<div class="m2"><p>تن افسرده چون باجان برآید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود مژگان خونین حاصل عشق</p></div>
<div class="m2"><p>ز دریا پنجه مرجان برآید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو شبنم هر که خودرا جمع سازد</p></div>
<div class="m2"><p>سبک از گلشن امکان برآید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رهی سرکن خدا را ای سبکدست</p></div>
<div class="m2"><p>که جان از جسم دست افشان برآید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ندارد حاصلی آمیزش خلق</p></div>
<div class="m2"><p>که شمع از انجمن گریان برآید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به صبر از ورطه هستی توان رست</p></div>
<div class="m2"><p>به لنگر کشتی از طوفان برآید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز زیر پوست هر دل را که مغزی است</p></div>
<div class="m2"><p>چو پسته با لب خندان برآید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو می باید گذشت آخر ز سامان</p></div>
<div class="m2"><p>خوشا آن سرکه بی سامان برآید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل از باد مرادعشق صائب</p></div>
<div class="m2"><p>ازین دریای بی پایان برآید</p></div></div>