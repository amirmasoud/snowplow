---
title: >-
    غزل شمارهٔ ۴۰۳۶
---
# غزل شمارهٔ ۴۰۳۶

<div class="b" id="bn1"><div class="m1"><p>کجا ز سینه من غم شراب می‌شوید</p></div>
<div class="m2"><p>چه زنگ از دل آیینه آب می‌شوید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه آب روشن ازین چرخ نیلگون جویم</p></div>
<div class="m2"><p>که رخ به خون شفق آفتاب می‌شوید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی که در پی اصلاح بخت تیره ماست</p></div>
<div class="m2"><p>سیاهی از پر و بال عقاب می‌شوید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>علاج تشنه دیدار نیست جز دیدار</p></div>
<div class="m2"><p>کجا غم از دل بلبل گلاب می‌شوید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به حسن ساده‌دلی چشم هر که باز شود</p></div>
<div class="m2"><p>به اشک تلخ ندامت کتاب می‌شوید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر غبار یتیمی توان ز گوهر شست</p></div>
<div class="m2"><p>کدورت از دل من هم شراب می‌شوید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بس که دلبر من تشنه جمال خود است</p></div>
<div class="m2"><p>به آبگینه ز رخ گرد خواب می‌شوید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به گریه تیرگی از دل رود که از ریزش</p></div>
<div class="m2"><p>ز روی خویش سیاهی سحاب می‌شوید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که گفته است در ابر سفید باران نیست</p></div>
<div class="m2"><p>رخش غم از دل من در نقاب می‌شوید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چراغ سوختگان می‌شود ز هم روشن</p></div>
<div class="m2"><p>به اشک چهره آتش کباب می‌شوید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غبار غم ننشیند به دامنی صائب</p></div>
<div class="m2"><p>که توبه نامه من با شراب می‌شوید</p></div></div>