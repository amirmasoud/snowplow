---
title: >-
    غزل شمارهٔ ۵۵۰۳
---
# غزل شمارهٔ ۵۵۰۳

<div class="b" id="bn1"><div class="m1"><p>به هر حالی که باشد گرد گل همچون صبا گردم</p></div>
<div class="m2"><p>نیم نگهت که از گل در پریشانی جدا گردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همین امید بر گرد جهان سرگشته ام دارد</p></div>
<div class="m2"><p>که او برگرد دل من گرد آن ناآشنا گردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر چه از جگرداری برآتش می توانم زد</p></div>
<div class="m2"><p>ندارم زهره تا برگرد آن گلگون قبا گردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دری نگشود بر روی چو مهر از دربدر گشتن</p></div>
<div class="m2"><p>مگر یک چند گرد خویشتن چون آسیا گردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر شمشیر بارد بر سرم در دل نمی گیرم</p></div>
<div class="m2"><p>نیم آیینه کز اندک غباری بی صفا گردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وفا دین من و مهربتان آیین من باشد</p></div>
<div class="m2"><p>رخم از قبله برگردد گر از مهر و وفا گردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنان با بیوفایان صائب آن بدمهر می جوشد</p></div>
<div class="m2"><p>که با این مهر نزدیک است من هم بیوفا گردم</p></div></div>