---
title: >-
    غزل شمارهٔ ۶۵۳۱
---
# غزل شمارهٔ ۶۵۳۱

<div class="b" id="bn1"><div class="m1"><p>میخانه ای که شوق تو باشد مدام او</p></div>
<div class="m2"><p>دایم به زور باده زند دور، جام او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سنگ ملامتی که به هم بشکند ترا</p></div>
<div class="m2"><p>چون کعبه واجب است به جان احترام او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طومار درد و داغ عزیزان رفته است</p></div>
<div class="m2"><p>این مهلتی که عمر درازست نام او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رحم است بر کسی که شود خرج مردگان</p></div>
<div class="m2"><p>چون حافظ مزار سراسر کلام او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در هر سری که هست تمنای گلرخی</p></div>
<div class="m2"><p>از بوی گل پری زده گردد مشام او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در هر دلی که عشق الهی کند نزول</p></div>
<div class="m2"><p>چون کعبه جای کسب هوا نیست بام او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب بس است قسمت من خون دل ز عشق</p></div>
<div class="m2"><p>دست که می رسد به می لعل فام او؟</p></div></div>