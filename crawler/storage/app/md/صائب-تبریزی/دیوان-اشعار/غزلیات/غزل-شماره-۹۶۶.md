---
title: >-
    غزل شمارهٔ ۹۶۶
---
# غزل شمارهٔ ۹۶۶

<div class="b" id="bn1"><div class="m1"><p>دوربین خونین جگر از نظم احوال خودست</p></div>
<div class="m2"><p>روز و شب طاوس لرزان بر پر و بال خودست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیشه ای کز طاق افتد بشکند، چون آسمان</p></div>
<div class="m2"><p>از هزاران طاق دل افتاد و بر حال خودست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاکساری شد حصار از دیده بدبین مرا</p></div>
<div class="m2"><p>گوهر از گرد یتیمی پرده حال خودست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عقده حرص از مرور زندگی گردد زیاد</p></div>
<div class="m2"><p>شاخ آهو پر گره از کثرت سال خودست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیش باشد قسمت زنبور از دریای شهد</p></div>
<div class="m2"><p>ممسک از قهر خدا بی بهره از مال خودست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می کند در راه خود دام گرفتاری به خاک</p></div>
<div class="m2"><p>دیده هر کس که چون طاوس دنبال خودست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کاملان از عیب خود بیش از هنر یابند فیض</p></div>
<div class="m2"><p>بهره طاوس از پا، بیش از بال خودست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از کنار آب حیوان باز گردد خشک لب</p></div>
<div class="m2"><p>چون سکندر هر که مستظهر به اقبال خودست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست خصمی آدمی را غیر خود چون عنکبوت</p></div>
<div class="m2"><p>دام راه هر کسی از تار آمال خودست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دولت پابوس بس باشد حنا را خونبها</p></div>
<div class="m2"><p>بی سبب صائب به فکر خون پامال خودست</p></div></div>