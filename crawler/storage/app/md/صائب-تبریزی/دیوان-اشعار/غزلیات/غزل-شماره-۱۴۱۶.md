---
title: >-
    غزل شمارهٔ ۱۴۱۶
---
# غزل شمارهٔ ۱۴۱۶

<div class="b" id="bn1"><div class="m1"><p>نه خط از چهره آن آینه سیما برخاست</p></div>
<div class="m2"><p>که درین آینه، جوهر به تماشا برخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب که صحبت به حدیث سر زلف تو گذشت</p></div>
<div class="m2"><p>هر که برخاست ز جا، سلسله برپا برخاست!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کرد تسلیم به من مسند بیتابی را</p></div>
<div class="m2"><p>هر سپندی که درین انجمن از جا برخاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچ مستی ز پی رقص نخیزد از جای</p></div>
<div class="m2"><p>به نشاطی که دلم از سر دنیا برخاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یوسفی را که به یعقوب بود روی نیاز</p></div>
<div class="m2"><p>زین چه حاصل که خریدار ز صد جا برخاست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد فلک درصدد معرکه سازی، اکنون</p></div>
<div class="m2"><p>کز دل کودک ما ذوق تماشا برخاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ظل خورشید جهانتاب، مخلد باشد!</p></div>
<div class="m2"><p>سایه مریم اگر از سر عیسی برخاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بزم روشن گهران جای گرانجانان نیست</p></div>
<div class="m2"><p>ابر تا گشت گران، از سر دریا برخاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یادگار جگر سوخته مجنون است</p></div>
<div class="m2"><p>لاله ای چند که از دامن صحرا برخاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برسان زود به من کشتی می را ساقی</p></div>
<div class="m2"><p>که عجب ابر تری باز ز دریا برخاست!</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خضر صد قافله مجنون بیابانی شد</p></div>
<div class="m2"><p>هر غباری که ازین بادیه پیما برخاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>روح سرگشته مجنون غبارآلودست</p></div>
<div class="m2"><p>گردبادی که ازین دامن صحرا برخاست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پا مکش از در دلها که درین لغزشگاه</p></div>
<div class="m2"><p>صائب از خاک ز دریوزه دلها برخاست</p></div></div>