---
title: >-
    غزل شمارهٔ ۶۷۵۶
---
# غزل شمارهٔ ۶۷۵۶

<div class="b" id="bn1"><div class="m1"><p>ز شیرینی عتاب او شکرخندست پنداری</p></div>
<div class="m2"><p>زبان در کام او بادام در قندست پنداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به پیچ و تاب طی می گردد ایام حیات من</p></div>
<div class="m2"><p>رگ جانم به آن موی میان بندست پنداری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو شاخ گل به هر جا از سراپایش نظر افتد</p></div>
<div class="m2"><p>چو آن لبهای شیرین در شکرخندست پنداری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کند چون حلقه های دام وحشت از نظربازان</p></div>
<div class="m2"><p>نگاه او غزال جسته از بندست پنداری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به عیب خود نیفتد دیده هرگز عیبجویان را</p></div>
<div class="m2"><p>به چشم بی بصیرت عیب فرزندست پنداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پر کاهی ز احسان سبک مغزان بی حاصل</p></div>
<div class="m2"><p>به چشم غیرت من کوه الوندست پنداری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به استقبال لیلی برنمی خیزد ز جای خود</p></div>
<div class="m2"><p>ز چشم آهوان مجنون نظربندست پنداری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ندارد بی پر پروانه آب و تاب در محفل</p></div>
<div class="m2"><p>نهال شمع، سبز از برگ پیوندست پنداری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به چشم هر که پوشیده است چشم از عالم امکان</p></div>
<div class="m2"><p>فلک بر طاق نسیان شیشه ای چندست پنداری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نمی آید به چشم موشکافان صائب از تنگی</p></div>
<div class="m2"><p>دهان تنگ او رزق هنرمندست پنداری</p></div></div>