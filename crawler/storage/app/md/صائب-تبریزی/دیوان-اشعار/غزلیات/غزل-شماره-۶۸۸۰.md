---
title: >-
    غزل شمارهٔ ۶۸۸۰
---
# غزل شمارهٔ ۶۸۸۰

<div class="b" id="bn1"><div class="m1"><p>چرا به سلسله زلف او نظر نکنی؟</p></div>
<div class="m2"><p>چرا به عالم بی منتها سفر نکنی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب دراز کمند غزال مقصودست</p></div>
<div class="m2"><p>چرا به آه شب خود درازتر نکنی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر تو آدمیی وز نژاد دیو نه ای</p></div>
<div class="m2"><p>ز شیشه خانه گردون چرا گذر نکنی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کدام غبن به این می رسد که فصل بهار</p></div>
<div class="m2"><p>کنار خود چو صدف مخزن گهر نکنی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به آه و دود مکافات برنمی آیی</p></div>
<div class="m2"><p>به حال سوختگان خنده چون شرر نکنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زبان به کام تو چون میوه بهشت شود</p></div>
<div class="m2"><p>اگر تو دست چو طفلان به هر ثمر نکنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غبار منت احسان گرانتر از دردست</p></div>
<div class="m2"><p>به صندل دگران رفع دردسر نکنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به روشنایی دل راز نه فلک خوانی</p></div>
<div class="m2"><p>اگر تو در دل شبها چراغ برنکنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز پر دلی گهر از بحر می برد غواص</p></div>
<div class="m2"><p>گناه کیست تو بیدل اگر جگر نکنی؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نسیم صبح نگردیده در سبکروحی</p></div>
<div class="m2"><p>به نازکان چمن دست در کمر نکنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل سیاه نقاب جمال خورشیدست</p></div>
<div class="m2"><p>چرا به آه شب خویش را سحر نکنی؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عجبتر از تو ندارد جهان تماشاگاه</p></div>
<div class="m2"><p>چرا به چشم تعجب به خود نظر نکنی؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حیات خضر چه باشد نظر به همت عشق؟</p></div>
<div class="m2"><p>نظر سیاه به این عمر مختصر نکنی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز اهل توحید آن روز می شمارندت</p></div>
<div class="m2"><p>که هیچ تفرقه از خاک تا شکر نکنی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نرفته است سر رشته تا ز دست برون</p></div>
<div class="m2"><p>سر از دریچه گوهر چرا بدر نکنی؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر به روی تو در چاک سینه باز کند</p></div>
<div class="m2"><p>ز چاک سینه خود رو به هیچ در نکنی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زمین سرای مصیبت بود، تو می خواهی</p></div>
<div class="m2"><p>که مشت خاکی ازین خاکدان به سر نکنی؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به هوشیاری من نیست هیچ کس در بزم</p></div>
<div class="m2"><p>مرا ز خویش محال است بیخبر نکنی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو خون مرده، گرانخوابی تو بی پروا</p></div>
<div class="m2"><p>به آن رسیده که پروای نیشتر نکنی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به پای سعی محال است قطع وادی عشق</p></div>
<div class="m2"><p>به پیچ و تاب اگر این راه مختصر نکنی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کنون که مرکب توفیق زیر ران داری</p></div>
<div class="m2"><p>ازین خرابه پرمرده چون سفر نکنی؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خبر ز راز دل بحر می توانی یافت</p></div>
<div class="m2"><p>اگر ملاحظه از موجه خطر نکنی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ترا به سر ندهد جا سپهر مینایی</p></div>
<div class="m2"><p>چو آفتاب اگر زیر پا نظر نکنی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>رفیق خانه به دوشان جریده می باید</p></div>
<div class="m2"><p>سفر نکرده ز خود، عزم این سفر نکنی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زبان شکوه من در نیام خاموشی است</p></div>
<div class="m2"><p>چرا به ساغر من زهر بیشتر نکنی؟</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>حریف اشک ندامت نمی شوی صائب</p></div>
<div class="m2"><p>چو تاک دست به هر شاخ در کمر نکنی</p></div></div>