---
title: >-
    غزل شمارهٔ ۳۷۵۱
---
# غزل شمارهٔ ۳۷۵۱

<div class="b" id="bn1"><div class="m1"><p>فسرده دل نفس خونچکان نمی دارد</p></div>
<div class="m2"><p>زمین شوره گل و ارغوان نمی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مپرس راه خرابات را ز زاهد خشک</p></div>
<div class="m2"><p>که تیر کج خبری از نشان نمی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به گرمی طلب آید به دست دامن رزق</p></div>
<div class="m2"><p>تنور سرد نصیبی ز نان نمی دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهان نوردی دیوانه اختیاری نیست</p></div>
<div class="m2"><p>خبر ز گردش خود آسمان نمی دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز سایه وحشت صیاد می کند آهو</p></div>
<div class="m2"><p>دل رمیده تعلق به جان نمی دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمی شود کف دریادلان شود بی برگ</p></div>
<div class="m2"><p>حنای پنجه مرجان خزان نمی دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گل از نظاره او بی حجاب چیند غیر</p></div>
<div class="m2"><p>که گلفروش غم بلبلان نمی دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تمام رحمت و لطفند اهل دل صائب</p></div>
<div class="m2"><p>که میوه های بهشت استخوان نمی دارد</p></div></div>