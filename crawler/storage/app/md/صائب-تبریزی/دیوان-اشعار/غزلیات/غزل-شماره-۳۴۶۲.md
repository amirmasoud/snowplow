---
title: >-
    غزل شمارهٔ ۳۴۶۲
---
# غزل شمارهٔ ۳۴۶۲

<div class="b" id="bn1"><div class="m1"><p>شکوه اهل دل از خلق نهان می باشد</p></div>
<div class="m2"><p>این عقیقی است که در زیر زبان می باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صحبت راست روان راست نیاید با چرخ</p></div>
<div class="m2"><p>تیر یک لحظه در آغوش کمان می باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی ندامت نبود صحبت بی حاصل خلق</p></div>
<div class="m2"><p>شمع در انجمن انگشت گزان می باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جگر غنچه ز همصبحتی خار گداخت</p></div>
<div class="m2"><p>غم به دلهای سبکروح گران می باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق هرچند که در نام و نشان ممتازست</p></div>
<div class="m2"><p>طالب مردم بی نام و نشان می باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسن را در دم خط ناز و غرور دگرست</p></div>
<div class="m2"><p>خواب در وقت سحرگاه گران می باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خط برآورد و همان چهره او ساده نماست</p></div>
<div class="m2"><p>در صفا جوهر آیینه نهان می باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در خموشی نشود جوهر مردم ظاهر</p></div>
<div class="m2"><p>صائب این گنج نهان زیر زبان می باشد</p></div></div>