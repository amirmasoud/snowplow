---
title: >-
    غزل شمارهٔ ۶۹۳۰
---
# غزل شمارهٔ ۶۹۳۰

<div class="b" id="bn1"><div class="m1"><p>تا کی ز جهل چاره حرص از طلب کنی؟</p></div>
<div class="m2"><p>از خارخار چند علاج جرب کنی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز نمی رسد به طباشیر استخوان</p></div>
<div class="m2"><p>پیش حسب مباد حدیث نسب کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب راز آه زنده دلان روز می کنند</p></div>
<div class="m2"><p>داری تو جد و جهد که روزی به شب کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>انداخت پیش ابر سپر، تیغ آفتاب</p></div>
<div class="m2"><p>آن به که خصم را به مدارا ادب کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نان گرسنه چشم فزاید گرسنگی</p></div>
<div class="m2"><p>از چون خودی مباد که روزی طلب کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در بحر صاحب گهر از ابر شد صدف</p></div>
<div class="m2"><p>چون غافلان مباد که ترک سبب کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بارست سایه بر دل آزادگان و تو</p></div>
<div class="m2"><p>بهر سفر رفیق موافق طلب کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب به غمگسار ز غم می توان رسید</p></div>
<div class="m2"><p>حیف است عمر صرف نشاط و طرب کنی</p></div></div>