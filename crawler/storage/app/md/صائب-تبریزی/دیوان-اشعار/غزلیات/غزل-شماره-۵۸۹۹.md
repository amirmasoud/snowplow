---
title: >-
    غزل شمارهٔ ۵۸۹۹
---
# غزل شمارهٔ ۵۸۹۹

<div class="b" id="bn1"><div class="m1"><p>تلخی ز لب لعل تو نشنفتم و رفتم</p></div>
<div class="m2"><p>خوش باش که ناکام دعا گفتم و رفتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کردم سفر از خویش به آوازه یوسف</p></div>
<div class="m2"><p>بانگ جرس از قافله نشنفتم و رفتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون سیل سبکسیر به رخساره پر گرد</p></div>
<div class="m2"><p>خار و خس این بادیه را رفتم و رفتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غافل نگذشتم ز سر خار ملامت</p></div>
<div class="m2"><p>از آبله هر گام گهر سفتم و رفتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون عود ز خامی نزدم جوش شکایت</p></div>
<div class="m2"><p>بوی جگر سوخته بنهفتم و رفتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نعل سفرم جای دگر بود در آتش</p></div>
<div class="m2"><p>در سایه دنیا مژه ای خفتم و رفتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دادند به من عرض متاع دو جهان را</p></div>
<div class="m2"><p>جز عبرت از آنها نپذیرفتم و رفتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون غنچه زباغی که نسیمش دم عیسی است</p></div>
<div class="m2"><p>از همت من بود که نشکفتم و رفتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دود از جگر حوصله طور برآورد</p></div>
<div class="m2"><p>این داغ جگر سوز که بنهفتم و رفتم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر کس گهری سفت درین بزم چو صائب</p></div>
<div class="m2"><p>من نیز ز مژگان گهری سفتم و رفتم</p></div></div>