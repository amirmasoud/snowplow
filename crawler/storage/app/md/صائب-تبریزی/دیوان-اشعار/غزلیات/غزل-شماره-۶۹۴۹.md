---
title: >-
    غزل شمارهٔ ۶۹۴۹
---
# غزل شمارهٔ ۶۹۴۹

<div class="b" id="bn1"><div class="m1"><p>دستی به سر زلف خود از ناز کشیدی</p></div>
<div class="m2"><p>تا حلقه به گوش که دگر باز کشیدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد بر لب دریاکش من مهر خموشی</p></div>
<div class="m2"><p>جامی که ز منصور سخن بازکشیدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کنج قفس چند کنی بال فشانی؟</p></div>
<div class="m2"><p>بس نیست ترا آنچه ز پرواز کشیدی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای آینه در روی زمین دیدنیی نیست</p></div>
<div class="m2"><p>بیهوده چرا منت پرداز کشیدی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز سرمه که برخاست به تعظیم تو از جای؟</p></div>
<div class="m2"><p>چندان که درین انجمن آواز کشیدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای کبک لب از خنده بیهوده نبستی</p></div>
<div class="m2"><p>تا رخت به سرپنجه شهباز کشیدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون برگ گل افزود به رسوایی نکهت</p></div>
<div class="m2"><p>هر پرده که بر چهره این راز کشیدی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خون گشت دل زمزمه پرداز تو صائب</p></div>
<div class="m2"><p>تا این غزل از طبع سخنساز کشیدی</p></div></div>