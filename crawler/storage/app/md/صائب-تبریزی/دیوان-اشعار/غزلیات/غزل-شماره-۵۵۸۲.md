---
title: >-
    غزل شمارهٔ ۵۵۸۲
---
# غزل شمارهٔ ۵۵۸۲

<div class="b" id="bn1"><div class="m1"><p>ز پستی بر فلک از پاکی گوهر شود شبنم</p></div>
<div class="m2"><p>ز چشم پاک با خورشید هم بستر شود شبنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به از همصحبت شایسته اکسیری نمی باشد</p></div>
<div class="m2"><p>ز قرب لاله از یاقوت رنگین تر شود شبنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به چشم پاک آسان است تسخیر نکورویان</p></div>
<div class="m2"><p>یکی با آفتاب از دیده انور شود شبنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به عشق پاک کردم صرف عمر خود، ندانستم</p></div>
<div class="m2"><p>که ازتر دامنی با غنچه هم بستر شود شبنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به روشن طلعتان پیوند اگر معراج می خواهی</p></div>
<div class="m2"><p>که از خورشید روشندل بلند اختر شود شبنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز آب چشم من گفتم شود بیدار، ازین غافل</p></div>
<div class="m2"><p>که خواب ناز گل را پرده دیگر شود شبنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز چشم پاک عشرتهای رنگین می توان کردن</p></div>
<div class="m2"><p>که گل را تکمه پیراهن احمر شود شبنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در آتش می گذارد حسن نعل پاک چشمان را</p></div>
<div class="m2"><p>که از خورشید چون سیماب بی لنگر شود شبنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در آن گلشن که از می چهره را چون گل برافروزی</p></div>
<div class="m2"><p>به روی آتشین لاله خاکستر شود شبنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به چشم عندلیب از جمله تردامنان باشد</p></div>
<div class="m2"><p>اگر در پاک چشمی قطره کوثر شود شبنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مدار ای پاک گوهر دست سعی از دامن پاکان</p></div>
<div class="m2"><p>که از آمیزش گلها پری پیکر شود شبنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ندارد آبرو گل پیش رخسار عرقناکش</p></div>
<div class="m2"><p>اگر از شوخ چشمی مهر آن محضر شود شبنم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نباشد راه در گلزار او هر شوخ چشمی را</p></div>
<div class="m2"><p>مگر با دیده تر حلقه آن در شود شبنم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تن آسانی دل بیدار را غافل نمی سازد</p></div>
<div class="m2"><p>کجا در خواب ناز از نرمی بستر شود شبنم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز خورشید قیامت آب در چشمش نمی گردد</p></div>
<div class="m2"><p>اگر آیینه دار آن رخ انور شود شبنم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مده از دست صائب دامن مژگان خونین را</p></div>
<div class="m2"><p>که در گلزارها محرم ز چشم تر شود شبنم</p></div></div>