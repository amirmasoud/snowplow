---
title: >-
    غزل شمارهٔ ۶۰۸۴
---
# غزل شمارهٔ ۶۰۸۴

<div class="b" id="bn1"><div class="m1"><p>در سرانجام عمارت عمر خود باطل مکن</p></div>
<div class="m2"><p>در زمین عاریت چون غافلان منزل مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا دل ویرانه ای را می توان تعمیر کرد</p></div>
<div class="m2"><p>نقد اوقات گرامی صرف آب و گل مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز زمین گیری ندارد پله عشق مجاز</p></div>
<div class="m2"><p>آشیان چون قمریان بر سر و پا در گل مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم اگر داری که پا در دامن منزل کشی</p></div>
<div class="m2"><p>همرهی در قطع ره با مردم کاهل مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقد جان را عاقبت تسلیم چون خواهی نمود</p></div>
<div class="m2"><p>از تپیدن بیش ازین خون در دل قاتل مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقل زاد آخرت با دست تنها مشکل است</p></div>
<div class="m2"><p>بخل در برگ و نوا زنهار با سایل مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی نگهبان نفس سرکش می شود مطلق عنان</p></div>
<div class="m2"><p>از کمین خویشتن صیاد را غافل مکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صرف باطل ساختی سر جوش ایام حیات</p></div>
<div class="m2"><p>باری این ته جرعه اوقات را باطل مکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شمع از آتش زبانی سر به جای پا نهاد</p></div>
<div class="m2"><p>از سبک مغزی زبان بازی به یک محفل مکن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یک جهت شو در طریق عشق چون مردان مرد</p></div>
<div class="m2"><p>بیش ازین اوقات صائب صرف لاطایل مکن</p></div></div>