---
title: >-
    غزل شمارهٔ ۲۸۷۲
---
# غزل شمارهٔ ۲۸۷۲

<div class="b" id="bn1"><div class="m1"><p>دل عاشق به جور از یار دیرین برنمی گردد</p></div>
<div class="m2"><p>که در سفتن زآب و رنگ خود گوهر نمی گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکن پهلو تهی از ما که خورشید بلند اختر</p></div>
<div class="m2"><p>به ماه نو اگر پهلو دهد لاغر نمی گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه پروا دارد آن مغرور از طوفان اشک ما؟</p></div>
<div class="m2"><p>زدریا دامن خورشید تابان تر نمی گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه داند عاشق حیران عیار حسن جانان را؟</p></div>
<div class="m2"><p>نگاه از چشم قربانی به مژگان برنمی گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سپهر سنگدل آسوده است از دود آه ما</p></div>
<div class="m2"><p>که آب از دود گرد دیده مجمر نمی گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قضای آسمانی می کند اجرای حکم خود</p></div>
<div class="m2"><p>برات خط به شمشیر تغافل برنمی گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رقیب از بزم وصل ا مرا بیهوده می راند</p></div>
<div class="m2"><p>سپند شوخ بار خاطر مجمر نمی گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زفکر آن لب میگون نمی آیم برون صائب</p></div>
<div class="m2"><p>به گرد خاطر مخمور جز ساغر نمی گردد</p></div></div>