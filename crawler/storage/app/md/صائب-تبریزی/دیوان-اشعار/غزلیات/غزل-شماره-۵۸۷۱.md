---
title: >-
    غزل شمارهٔ ۵۸۷۱
---
# غزل شمارهٔ ۵۸۷۱

<div class="b" id="bn1"><div class="m1"><p>ما خنده را به مردم بیغم گذاشتیم</p></div>
<div class="m2"><p>گل را به شوخ چشمی شبنم گذاشتیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قانع به تلخ و شور شدیم از جهان خاک</p></div>
<div class="m2"><p>چون کعبه دل به چشمه زمزم گذاشتیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردم به یادگار اثرها گذاشتند</p></div>
<div class="m2"><p>ما دست رد به سینه عالم گذاشتیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چیزی به روی هم ننهادیم در جهان</p></div>
<div class="m2"><p>جز دست اختیار که بر هم گذاشتیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دادند اگر عنان دو عالم به دست ما</p></div>
<div class="m2"><p>از بیخودی ز دست همان دم گذاشتیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>الماس، بی نمک شده بود از موافقت</p></div>
<div class="m2"><p>تدبیر زخم و داغ به مرهم گذاشتیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی حاصلی نگرکه حضور بهشت را</p></div>
<div class="m2"><p>از بهر یک دو دانه چو آدم گذاشتیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب فضای چرخ مقام نشاط نیست</p></div>
<div class="m2"><p>بیهوده پا به حلقه ماتم گذاشتیم</p></div></div>