---
title: >-
    غزل شمارهٔ ۲۷۴۸
---
# غزل شمارهٔ ۲۷۴۸

<div class="b" id="bn1"><div class="m1"><p>ذوق حیرانی به داد چشم خونپالا رسید</p></div>
<div class="m2"><p>سینه خم امن شد از جوش، تا صهبا رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفته بود از رفتن گل شورش ما کم شود</p></div>
<div class="m2"><p>نوبهار خط به فریاد جنون ما رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از ملامت شد جنون نارسای ما تمام</p></div>
<div class="m2"><p>شد می پرزور هر سنگی به این مینا رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صحبت ما دردمندان کیمیای صحت است</p></div>
<div class="m2"><p>مایه درمان شود هرکس به درد ما رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوی شهرت می توان بردن ز میدان بی طرف</p></div>
<div class="m2"><p>مفت زد مجنون که پیش از ما به این صحرا رسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرچه شبنم بود از افتادگان این چمن</p></div>
<div class="m2"><p>از سحرخیزی به خورشید جهان پیما رسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیکسان صائب نمی باشند بی فریادرس</p></div>
<div class="m2"><p>کوه قاف آخر به داد وحشت عنقا رسید</p></div></div>