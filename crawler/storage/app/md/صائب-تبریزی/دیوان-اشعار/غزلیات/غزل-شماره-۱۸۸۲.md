---
title: >-
    غزل شمارهٔ ۱۸۸۲
---
# غزل شمارهٔ ۱۸۸۲

<div class="b" id="bn1"><div class="m1"><p>خال لب تو داغ دل آب کوثرست</p></div>
<div class="m2"><p>پنهان تبسمت نمک شور محشرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حالا به فکر دلبری افتاده ابرویت</p></div>
<div class="m2"><p>تیغ برهنه روی تو نوخط جوهرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تنها نه من دل پری از باغ می برم</p></div>
<div class="m2"><p>شبنم هم از تبسم رسوای گل، ترست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کی رو ز تلخرویی دریا به هم کشد؟</p></div>
<div class="m2"><p>ابر مرا معامله با آب گوهرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دارد خبر ز آه من و تنگنای چرخ</p></div>
<div class="m2"><p>هر شعله ای که در قفس تنگ مجمرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پرویز داغ غیرت خود را علاج کرد</p></div>
<div class="m2"><p>شیرین تندخوی همان داغ شکرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از آستان عشق به جایی نمی رود</p></div>
<div class="m2"><p>صائب یکی ز حلقه به گوشان این درست</p></div></div>