---
title: >-
    غزل شمارهٔ ۶۲۷۳
---
# غزل شمارهٔ ۶۲۷۳

<div class="b" id="bn1"><div class="m1"><p>سرد شد دست و دعا صبح به یک خندیدن</p></div>
<div class="m2"><p>روح را گرم کند خنده به دل دزدیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاطر جمع و پریشان نظری هیهات است</p></div>
<div class="m2"><p>شانه زلف حواس است پریشان دیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیدن بحر به پوشیدن چشمی بندست</p></div>
<div class="m2"><p>چشم هر چند ز دریا نتوان پوشیدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رزق هر چند که چون سیل بهاران آید</p></div>
<div class="m2"><p>آسیا را نشود سنگ ره نالیدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پوست پوشیده به جولانگه لیلی رفتم</p></div>
<div class="m2"><p>در ره عشق ز مجنون نتوان لنگیدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صائب از پیچ و خم زلف سخن مویی شد</p></div>
<div class="m2"><p>اینقدر نیز نباید به سخن پیچیدن</p></div></div>