---
title: >-
    غزل شمارهٔ ۴۷۴۰
---
# غزل شمارهٔ ۴۷۴۰

<div class="b" id="bn1"><div class="m1"><p>فصل شباب رفت ره خانه پیش گیر</p></div>
<div class="m2"><p>کنجی نشین و سبحه صد دانه پیش گیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون جغد در خرابه دنیا گره مشو</p></div>
<div class="m2"><p>چون سیل راه بحر ز ویرانه پیش گیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با عشق پیشگان سخن ار لوح ساده کن</p></div>
<div class="m2"><p>با اهل عقل ابجد طفلانه پیش گیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نتوان به حق رسید به این پنج روزه عمر</p></div>
<div class="m2"><p>تنگ است وقت، راه صنمخانه پیش گیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لنگر مکن به دامن مریم مسیح وار</p></div>
<div class="m2"><p>راه فلک به همت مردانه پیش گیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تاهست در پیاله نم از باغ برمیا</p></div>
<div class="m2"><p>چون می تمام شد ره میخانه پیش گیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیرون میا ز خانه به تکلیف هیچ کس</p></div>
<div class="m2"><p>در فقر شیوه های ملوکانه پیش گیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نتوان به پای هوش رسیدن به هیچ جا</p></div>
<div class="m2"><p>در راه عشق لغزش مستانه پیش گیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مردان رسیده اند ز کوشش به مدعا</p></div>
<div class="m2"><p>صائب تو نیز کوشش مردانه پیش گیر</p></div></div>