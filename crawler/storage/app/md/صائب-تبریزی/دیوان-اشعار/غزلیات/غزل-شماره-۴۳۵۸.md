---
title: >-
    غزل شمارهٔ ۴۳۵۸
---
# غزل شمارهٔ ۴۳۵۸

<div class="b" id="bn1"><div class="m1"><p>پروای خط آن عارض گلفام ندارد</p></div>
<div class="m2"><p>از سادگی این صبح غم شام ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پاس دل خود دار که آن زلف گرهگیر</p></div>
<div class="m2"><p>یک دانه بغیر از گره دام ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با دوری دلها چه کند قرب مکانی</p></div>
<div class="m2"><p>شکر خبر از تلخی بادام ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمشیر کشیدی وبه خونم ننشاندی</p></div>
<div class="m2"><p>افسوس که آغاز تو انجام ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غافل مشو ای نخل امید از ثمرخویش</p></div>
<div class="m2"><p>حرفی است که عاشق طمع خام ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از شرم در بسته روزی نگشاید</p></div>
<div class="m2"><p>این قفل کلیدی به جز ابرام ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از نقش برون آی که آن کعبه مقصود</p></div>
<div class="m2"><p>جز ساده دل جامه احرام ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از پایه خود هر که نهد پای فراتر</p></div>
<div class="m2"><p>مستی است که پروای لب بام ندارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما در هوس نام چه خونها که نخوردیم</p></div>
<div class="m2"><p>آسوده عقیقی که سر نام ندارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در خانه دلگیر فلک چند توان بود</p></div>
<div class="m2"><p>فریاد که خانه ره بام ندارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از تلخی می شکوه مخمور محال است</p></div>
<div class="m2"><p>صائب گله از تلخی دشنام ندارد</p></div></div>