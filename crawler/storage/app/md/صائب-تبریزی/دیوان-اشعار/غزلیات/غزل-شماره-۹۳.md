---
title: >-
    غزل شمارهٔ ۹۳
---
# غزل شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>آه باشد به ز زلف عنبرین عشاق را</p></div>
<div class="m2"><p>اشک باشد بهتر از در ثمین عشاق را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آب حیوان است خوی آتشین عشاق را</p></div>
<div class="m2"><p>آیه رحمت بود چین جبین عشاق را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می کند ز آتش سمندر سیر گلزار خلیل</p></div>
<div class="m2"><p>درد و داغ عشق باشد دلنشین عشاق را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنچنان کز چشمه سنبل شسته رو آید برون</p></div>
<div class="m2"><p>پاک سازد دیده های پاک بین عشاق را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تهی چشمی بود عرض گهر دادن به خلق</p></div>
<div class="m2"><p>ور نه دریاها بود در آستین عشاق را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غافلان گر در بقای نام کوشش می کنند</p></div>
<div class="m2"><p>ساده از نام و نشان باشد نگین عشاق را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کوته اندیشان قیامت را اگر دانند دور</p></div>
<div class="m2"><p>نقد باشد پیش چشم دوربین عشاق را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر چه از نقش قدم در ظاهرند افتاده تر</p></div>
<div class="m2"><p>توسن افلاک باشد زیر زین عشاق را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آسان سیران نمی بینند صائب زیر پا</p></div>
<div class="m2"><p>نیست پروای غم روی زمین عشاق را</p></div></div>