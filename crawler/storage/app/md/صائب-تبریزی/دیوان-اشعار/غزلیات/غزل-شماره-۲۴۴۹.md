---
title: >-
    غزل شمارهٔ ۲۴۴۹
---
# غزل شمارهٔ ۲۴۴۹

<div class="b" id="bn1"><div class="m1"><p>تیرگی از مد احسان جان روشن می کشد</p></div>
<div class="m2"><p>رشته میل آتشین در چشم سوزن می کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست خرمن را وجود برگ کاهی پیش حرص</p></div>
<div class="m2"><p>قانع از هر دانه جو، ناز خرمن می کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس که دارد وحشت از زخم زبان ناصحان</p></div>
<div class="m2"><p>از دهان شیر، مجنون ناز مأمن می کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می کند سنگین دلان را نرم آه عجز من</p></div>
<div class="m2"><p>خشک مغزیهای من از ریگ روغن می کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با تو سرکش برنمی آیم، وگرنه شوق من</p></div>
<div class="m2"><p>آتش سوزان زسنگ و آب از آهن می کشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می فتد در اوج عزت طشتش از بام زوال</p></div>
<div class="m2"><p>بر زمین چون آفتاب آن کس که دامن می کشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیده ای کز سرمه توحید روشن گشته است</p></div>
<div class="m2"><p>از سر هر خار، ناز نخل ایمن می کشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زیر شمشیرست صائب جایش از گردنکشی</p></div>
<div class="m2"><p>از خط فرمان سبک مغزی که گردن می کشد</p></div></div>