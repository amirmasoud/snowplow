---
title: >-
    غزل شمارهٔ ۵۲۷۵
---
# غزل شمارهٔ ۵۲۷۵

<div class="b" id="bn1"><div class="m1"><p>گر تشنه اسراری پیش آر شراب اول</p></div>
<div class="m2"><p>گر گنج همی خواهی می گرد خراب اول</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ان نقطه خاموشی درحرف نمی گنجد</p></div>
<div class="m2"><p>بر طاق فراموشی بگذار کتاب اول</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از ترک هوا آخر با بحر یکی گردند</p></div>
<div class="m2"><p>هر چند هوا جویند مردم چو حباب اول</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از لطف بهار آخر دریای گهر گردد</p></div>
<div class="m2"><p>جز دود و بخاری نیست هر چند سحاب اول</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا در پس این پرده است دل صاف نمی گردد</p></div>
<div class="m2"><p>چون زنده دلان بگذر از پرده خواب اول</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خنده عشق ای دل زنهار مشو ایمن</p></div>
<div class="m2"><p>بستر ز نمک سازند از بهرکباب اول</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حاشا که طمع گردد دل سایل</p></div>
<div class="m2"><p>گر عرض دهد بردل تلخی جواب اول</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنان که خبر دارند از آخر کار خود</p></div>
<div class="m2"><p>شرط است که بگذارند پارا به حساب اول</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>افسرده تر از پیری است دولت چو کهن گردد</p></div>
<div class="m2"><p>هر چنددل افروزست چون عهد شباب اول</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر چند چمن پیرا درپاس چمن کوشد</p></div>
<div class="m2"><p>آتش نفسان از گل گیرند گلاب اول</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با ما سخنی سرکن کان مهر جهان آرا</p></div>
<div class="m2"><p>ذرات جهان را داد تشریف خطاب اول</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هشیار به حرف ما صائب نتوان پی برد</p></div>
<div class="m2"><p>تر طیب دماغی کن ازباده ناب اول</p></div></div>