---
title: >-
    غزل شمارهٔ ۳۵۵۹
---
# غزل شمارهٔ ۳۵۵۹

<div class="b" id="bn1"><div class="m1"><p>تا حیا سرمه کش نرگس جادوی تو بود</p></div>
<div class="m2"><p>شبنم خلد نظر باز گل روی تو بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شعله شوخ ملاحت ز رخت می تابید</p></div>
<div class="m2"><p>آب حیوان صباحت همه در جوی تو بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم بر سرمه نمی کرد سیه، مژگانت</p></div>
<div class="m2"><p>وسمه از طاق دل افتاده ابروی تو بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرو برجسته بستان رعونت بودی</p></div>
<div class="m2"><p>شاخ گل دست نشان قد دلجوی تو بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برق با آنهمه شوخی که جهان می سوزد</p></div>
<div class="m2"><p>شرر مرده آتشکده خوی تو بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لب نهادی به لب ساغر و رفتی از دست</p></div>
<div class="m2"><p>حیف ازان هیکل شرمی که به بازوی تو بود</p></div></div>