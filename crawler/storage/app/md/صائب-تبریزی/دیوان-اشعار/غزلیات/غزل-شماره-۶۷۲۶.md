---
title: >-
    غزل شمارهٔ ۶۷۲۶
---
# غزل شمارهٔ ۶۷۲۶

<div class="b" id="bn1"><div class="m1"><p>ای که فکر چاره بیماری دل می‌کنی</p></div>
<div class="m2"><p>نسبت خود را به چشم یار باطل می‌کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست جای خرمی ماتم‌سرای آسمان</p></div>
<div class="m2"><p>زیر تیغ از ساده‌لوحی رقص بسمل می‌کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌کند از هر سر مویت سفیدی راه مرگ</p></div>
<div class="m2"><p>تو ز غفلت همچنان تعمیر منزل می‌کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌توانی صد دل ویرانه را آباد کرد</p></div>
<div class="m2"><p>از زر و سیم آنچه صرف خانه گل می‌کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با تو از دنیا نیاید جز عمل چیزی به خاک</p></div>
<div class="m2"><p>مایه حسرت شود نقدی که حاصل می‌کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قد چو خم گردید غافل زیستن از عقل نیست</p></div>
<div class="m2"><p>خواب تا کی زیر این دیوار مایل می‌کنی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای که دنبال تکلف می‌روی چون غافلان</p></div>
<div class="m2"><p>زندگی و مرگ را بر خویش مشکل می‌کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست جای دانه امید این محنت‌سرا</p></div>
<div class="m2"><p>در زمین شوره تخم خویش باطل می‌کنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رشته عمری که دام مطلب حق می‌شود</p></div>
<div class="m2"><p>صرف در شیرازه اوراق باطل می‌کنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی‌تأمل می‌کنی فرموده ابلیس را</p></div>
<div class="m2"><p>چون رسد نوبت به کار خیر، دل‌دل می‌کنی</p></div></div>