---
title: >-
    غزل شمارهٔ ۱۰۹۲
---
# غزل شمارهٔ ۱۰۹۲

<div class="b" id="bn1"><div class="m1"><p>افسر سر گرمی مهر از فروغ جام اوست</p></div>
<div class="m2"><p>خرده انجم سپند روی آتش فام اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ذکر او دل زنده دارد چرخ مینا رنگ را</p></div>
<div class="m2"><p>جان این فیروزه در دست خواص نام اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبح محشر انتظار جلوه او می کشد</p></div>
<div class="m2"><p>چشم خورشید قیامت بر کنار بام اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل عبث در دامن باد صبا آویخته است</p></div>
<div class="m2"><p>گوش هر بی درد، کی شایسته پیغام اوست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی در بیت الحرام عشق دارد آفتاب</p></div>
<div class="m2"><p>پرنیان صبح صادق جامه احرام اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مردم باریک بین در وصل هجران می کشند</p></div>
<div class="m2"><p>مرغ زیرک گر به شاخ گل نشیند دام اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ابر سیرابی که بر خارا کند گوهر نثار</p></div>
<div class="m2"><p>وز ندامت تر نگردد، التفات عام اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از سر سرگشته گرداب و رقص گردباد</p></div>
<div class="m2"><p>می توان دانست بر و بحر بی آرام اوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون نترسد چشم من صائب ز زهر چشم او؟</p></div>
<div class="m2"><p>شور دریای محیط از تلخی بادام اوست</p></div></div>