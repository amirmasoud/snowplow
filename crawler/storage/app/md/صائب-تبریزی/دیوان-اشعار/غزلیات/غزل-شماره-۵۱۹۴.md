---
title: >-
    غزل شمارهٔ ۵۱۹۴
---
# غزل شمارهٔ ۵۱۹۴

<div class="b" id="bn1"><div class="m1"><p>در زلف تو آویخت دل از قید علایق</p></div>
<div class="m2"><p>سررشته پیوند بود تاب موافق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پهلو به حیات ابدی می زند آن زلف</p></div>
<div class="m2"><p>این است سوادی که به اصل است مطابق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از عشق شکایت گنه حوصله ماست</p></div>
<div class="m2"><p>باکودک بدخو چه کند دایه مشفق؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیگر نشود جمع به شیرازه محشر</p></div>
<div class="m2"><p>هردل که پریشان شود ازناله عاشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همت ز دل وعرض تجمل بود ازدست</p></div>
<div class="m2"><p>منت ز خلایق بود و رزق ز خالق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای سرو مزن باقد او لاف رعونت</p></div>
<div class="m2"><p>کاین جامه به هر بی سرو پانیست موافق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آگاه ز عیب و هنر خویش نگردد</p></div>
<div class="m2"><p>تاچشم نپوشد کسی ازعیب خلایق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نشگفت اگر ازتیغ تو وا شد دل صائب</p></div>
<div class="m2"><p>جان تازه کند صحبت یاران موافق</p></div></div>