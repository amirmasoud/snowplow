---
title: >-
    غزل شمارهٔ ۱۳۱۵
---
# غزل شمارهٔ ۱۳۱۵

<div class="b" id="bn1"><div class="m1"><p>ماه در گردون نوردی چون دل آواره نیست</p></div>
<div class="m2"><p>در بساط آسمان این کوکب سیاره نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از حجاب تن، دل رم کرده ما فارغ است</p></div>
<div class="m2"><p>دامن ما چون شرر در زیر سنگ خاره نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کار بیدردان بود گل در گریبان ریختن</p></div>
<div class="m2"><p>برگ عیش نامرادان جز دل صد پاره نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم شبنم تکمه پیراهن خورشید شد</p></div>
<div class="m2"><p>حسن، شرم آلودگان را مانع نظاره نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیزم تر، صندل تدبیر نفروشد به ما</p></div>
<div class="m2"><p>جز سر تسلیم، اینجا دردسر را چاره نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا بود دل تیره، تن با او مدارا می کند</p></div>
<div class="m2"><p>سنگ چون آیینه شد، ایمن ز سنگ خاره نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پا منه بیرون ز زهد خشک، چون عارف نه ای</p></div>
<div class="m2"><p>طفل را دارالامانی بهتر از گهواره نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از صفای وقت صائب در حجاب غفلت است</p></div>
<div class="m2"><p>در خرابات مغان هر کس که دردی خواره نیست</p></div></div>