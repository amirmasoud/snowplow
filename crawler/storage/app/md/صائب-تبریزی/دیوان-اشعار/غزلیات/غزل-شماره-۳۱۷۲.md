---
title: >-
    غزل شمارهٔ ۳۱۷۲
---
# غزل شمارهٔ ۳۱۷۲

<div class="b" id="bn1"><div class="m1"><p>سر شوریده را فکر سرانجامی نمی ماند</p></div>
<div class="m2"><p>چو عشق آمد دگر اندیشه خامی نمی ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همین راهی که از دوری نمایان نیست پایانش</p></div>
<div class="m2"><p>اگر از خود قدم بیرون نهی گامی نمی ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه آسوده است از دل واپسی جان سبکروحش</p></div>
<div class="m2"><p>کسی کز وی درین وحشت سرا نامی نمی ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنین گر آفتاب عشق سازد عام فیض خود</p></div>
<div class="m2"><p>جهان آب و گل را میوه خامی نمی ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر بی پرده گردد لذت خونخواری عاشق</p></div>
<div class="m2"><p>خرابات مغان را باده آشامی نمی ماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زشوخی جلوه او می برد با خویش دلها را</p></div>
<div class="m2"><p>از ان آهوی وحشی در زمین دامی نمی ماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنین پرشور از ان کان ملاحت گر جهان گردد</p></div>
<div class="m2"><p>رگ تلخی درین بستان به بادامی نمی ماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به جمع مال کوشد خواجه چون زنبور، ازین غافل</p></div>
<div class="m2"><p>که چون شد خانه اش پر، جای آرامی نمی ماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنین خواهد به هم انداخت ساقی گر حریفان را</p></div>
<div class="m2"><p>زسنگ فتنه سالم شیشه و جامی نمی ماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زترک غنچه خسبی شد پریشان صائب احوالم</p></div>
<div class="m2"><p>چوگل برداشت دست از خویش، اندامی نمی ماند</p></div></div>