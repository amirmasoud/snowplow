---
title: >-
    غزل شمارهٔ ۲۷۸۱
---
# غزل شمارهٔ ۲۷۸۱

<div class="b" id="bn1"><div class="m1"><p>بوسه از کنج دهان دلربا دارد امید</p></div>
<div class="m2"><p>این دل گستاخ را بنگر چها دارد امید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک در چشمی که در دوران آن خط غبار</p></div>
<div class="m2"><p>روشنی از سرمه و از توتیا دارد امید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در شمار خودفروشان است در بازار حشر</p></div>
<div class="m2"><p>کشته ای کز دست و تیغش خونبها دارد امید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نور اسلام از جبین کافران دارد طمع</p></div>
<div class="m2"><p>هر که از چشمش نگاه آشنا دارد امید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که از مژگان دلدوز تو می جوید امان</p></div>
<div class="m2"><p>راه گردانیدن از تیر قضا دارد امید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی نیازان را زحفظ آبرو آماده است</p></div>
<div class="m2"><p>آنچه خضر از چشمه آب بقا دارد امید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به که نگشاید زلب مهر خموشی غنچه وار</p></div>
<div class="m2"><p>جنت در بسته هر کس از خدا دارد امید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سایه بی قید را مانع زجولان می شود</p></div>
<div class="m2"><p>دولت پاینده هر کس از هما دارد امید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر ندارد هیچ کس بی مدعا دست دعا</p></div>
<div class="m2"><p>از دعا صائب دل بی مدعا دارد امید</p></div></div>