---
title: >-
    غزل شمارهٔ ۲۹۸
---
# غزل شمارهٔ ۲۹۸

<div class="b" id="bn1"><div class="m1"><p>زخم پنهانم اگر بیرون دهد خوناب‌ها</p></div>
<div class="m2"><p>رنگ خون پیدا کند در صلب گوهر آب‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عالمی را همچو خود سرگشته دارد آسمان</p></div>
<div class="m2"><p>چون برآید مشت خاشاکی ازین گرداب‌ها؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی‌قراران محبت زیر گردون چون کنند؟</p></div>
<div class="m2"><p>شیشه سربسته زندان است بر سیماب‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زنگ غفلت لازم تن‌پروری افتاده است</p></div>
<div class="m2"><p>سبز گردد از روانی چون بماند آب‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در وصال بحر، بی‌شوق رسا نتوان رسید</p></div>
<div class="m2"><p>خرج راه از نرم‌رفتاری شود سیلاب‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دولت بیدار اگر یک چند بی‌خوابی کشید</p></div>
<div class="m2"><p>کرد در ایام بخت ما قضای خواب‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کعبه و بتخانه از دل زندگان خالی شده است</p></div>
<div class="m2"><p>نیست جز قندیل، روشندل درین محراب‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از گل تن تا به آسانی تواند خاستن</p></div>
<div class="m2"><p>کشتی دل را سبک کن صائب از اسباب‌ها</p></div></div>