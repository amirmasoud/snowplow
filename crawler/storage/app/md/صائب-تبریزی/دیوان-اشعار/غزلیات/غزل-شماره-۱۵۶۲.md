---
title: >-
    غزل شمارهٔ ۱۵۶۲
---
# غزل شمارهٔ ۱۵۶۲

<div class="b" id="bn1"><div class="m1"><p>عالم امنی اگر هست همین بیهوشی است</p></div>
<div class="m2"><p>هست اگر جنت در بسته همین خاموشی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که افتاده به زندان خرد می داند</p></div>
<div class="m2"><p>که پریخانه صاحب نظران بیهوشی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای صبا درگذر از غنچه لب بسته من</p></div>
<div class="m2"><p>که گشاد دل من در گره خاموشی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در سر تیغ زبان بیهده گویان را نیست</p></div>
<div class="m2"><p>فتنه هایی که نهان زیر سر سر گوشی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پختگی در خور جوش است درین میخانه</p></div>
<div class="m2"><p>خامی باده نارس گنه کم جوشی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل بی خاری اگر هست درین خارستان</p></div>
<div class="m2"><p>پیش صاحب نظران مهر لب خاموشی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غرض از خوردن می صائب اگر بیخبری است</p></div>
<div class="m2"><p>خوردن خون دل خود چه کم از می نوشی است؟</p></div></div>