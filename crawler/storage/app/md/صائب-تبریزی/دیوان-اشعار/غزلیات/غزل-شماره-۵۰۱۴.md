---
title: >-
    غزل شمارهٔ ۵۰۱۴
---
# غزل شمارهٔ ۵۰۱۴

<div class="b" id="bn1"><div class="m1"><p>دلی که خانه زنبور شد ز پیکانش</p></div>
<div class="m2"><p>شفای خسته‌دلان است شیره جانش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خون خود نکند کشته‌اش دهن شیرین</p></div>
<div class="m2"><p>ز بس که تشنه خون است تیغ مژگانش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به غیر عشق کدامین محیط خون‌خوارست</p></div>
<div class="m2"><p>که دست، پنجه مرجان شود ز دامانش ؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امید گوهر سیراب ازین محیط مدار</p></div>
<div class="m2"><p>که غیر چین جبین نیست مد احسانش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نفس‌گداختگانند موج‌های سراب</p></div>
<div class="m2"><p>که شسته‌اند ز جان دست در بیابانش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بساز با جگر تشنه همچو اسکندر</p></div>
<div class="m2"><p>نظر سیاه مگردان به آب حیوانش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به سرمه دل شب چشم خویش روشن دار</p></div>
<div class="m2"><p>که تیغ سینه شکافی است صبح خندانش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز میر قافله عشق، رحم مدار</p></div>
<div class="m2"><p>که پر ز یوسف مصری است چاه نسیانش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز خوان چرخ فرومایه دست کوته دار</p></div>
<div class="m2"><p>که قدر خود شکند هرکه بشکند نانش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به صدق هرکه برآورد دم ز دل صائب</p></div>
<div class="m2"><p>چو صبح ،مشرق خورشید شد گریبانش</p></div></div>