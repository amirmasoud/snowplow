---
title: >-
    غزل شمارهٔ ۳۸۲۱
---
# غزل شمارهٔ ۳۸۲۱

<div class="b" id="bn1"><div class="m1"><p>به گریه کی ز دل من غبار می خیزد؟</p></div>
<div class="m2"><p>به آب چشم چه گل از مزار می خیزد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کند چه نشو و نما نخل ما در آن گلشن</p></div>
<div class="m2"><p>که العطش ز لب جویبار می خیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی که همچو صدف دامن از جهان چیند</p></div>
<div class="m2"><p>ز دامنش گهر شاهوار می خیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو صبح هرکه دل از مهر صیقلی کرده است</p></div>
<div class="m2"><p>ز سینه اش نفس بی غبار می خیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز تیغها که شکسته است آه در جگرم</p></div>
<div class="m2"><p>نفس ز سینه من زخمدار می خیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>علم شود به طراوت کسی که چون نرگس</p></div>
<div class="m2"><p>ز خواب ناز به روی بهار می خیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز آتشی که مرا در دل است همچو سپند</p></div>
<div class="m2"><p>هزار ناله بی اختیار می خیزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکایت از ستم عشق اختیاری نیست</p></div>
<div class="m2"><p>به تازیانه آتش شرار می خیزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سپهر شربت بیمار من کند شیرین</p></div>
<div class="m2"><p>به شیره ای که ز دندان مار می خیزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سپند آتش حسن ترا شماری نیست</p></div>
<div class="m2"><p>اگر یکی بنشیند هزار می خیزد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر به سوختگان گرم برخوری چه شود؟</p></div>
<div class="m2"><p>نه شعله نیز به تعظیم خار می خیزد؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نشان همت والاست وحشت از دو جهان</p></div>
<div class="m2"><p>که این پلنگ ازین کوهسار می خیزد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که چشم کرد دل داغدار صائب را؟</p></div>
<div class="m2"><p>که دود تلخی ازین لاله زار می خیزد</p></div></div>