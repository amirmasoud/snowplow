---
title: >-
    غزل شمارهٔ ۵۶۵۷
---
# غزل شمارهٔ ۵۶۵۷

<div class="b" id="bn1"><div class="m1"><p>به که بردیده گستاخ تمنا فکنم</p></div>
<div class="m2"><p>پرده ای کز رخ آن آینه سیما فکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش نشین نیست چنان جوهر بینایی من</p></div>
<div class="m2"><p>که به هر آینه رو طرح تماشا فکنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی تو گر چشم به رخسار بهشت اندازم</p></div>
<div class="m2"><p>مشت خاری است که در دیده بینا فکنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مایه عیش حیات ابدی می گردد</p></div>
<div class="m2"><p>هر نگاهی که بر آن قامت رعنا فکنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست در روی زمین کوه گران تمکینی</p></div>
<div class="m2"><p>تا بر او سایه اقبال چو عنقا فکنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشتی خاک ز آب گهرم طوفانی است</p></div>
<div class="m2"><p>به که این گوهر شهوار به دریا فکنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برنتابد دو جهان درد گرانسنگ مرا</p></div>
<div class="m2"><p>این نه کوهی است که در دامن صحرا فکنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاک در کاسه کنم دیده دون همت را</p></div>
<div class="m2"><p>به غلط دیده اگر بر رخ دنیا فکنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا گمان نفسی هست مرا، ممکن نیست</p></div>
<div class="m2"><p>بار خود بر دل گردون چو مسیحا فکنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خجلم چون کف بی مغز ز روشن گهران</p></div>
<div class="m2"><p>من که سجاده خود بر سر دریا فکنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می شود مشرق خورشید سعادت صائب</p></div>
<div class="m2"><p>چون هما سایه اقبال به هر جا فکنم</p></div></div>