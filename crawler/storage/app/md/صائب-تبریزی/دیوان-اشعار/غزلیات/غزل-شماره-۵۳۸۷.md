---
title: >-
    غزل شمارهٔ ۵۳۸۷
---
# غزل شمارهٔ ۵۳۸۷

<div class="b" id="bn1"><div class="m1"><p>با زبان گندمین از بینوایی فارغم</p></div>
<div class="m2"><p>خوشه ای دارم که از خرمن گدایی فارغم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>موج را سر رشته وحدت زدریا نگسلد</p></div>
<div class="m2"><p>بند بندم گر کند عشق از جدایی فارغم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جوهر من از دهان زخم گویا می شود</p></div>
<div class="m2"><p>چون لب خاموش تیغ از خودستایی فارغم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کاسه لبریز دریا را نمی آرد به چشم</p></div>
<div class="m2"><p>چشم پر خون، دارد از شبنم گدایی فارغم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بستر خار است بر دیوانه سختیهای عشق</p></div>
<div class="m2"><p>سنگ طفلان کرده است از مومیایی فارغم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همت من سر فرو نارد به مقصدهای پست</p></div>
<div class="m2"><p>از هدف عمری است چون تیر هوایی فارغم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست چون طاوس از هر پر در آتش نعل من</p></div>
<div class="m2"><p>جغد بی بال و پرم، از خودنمایی فارغم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آفتاب از لعل غافل نیست در زندان سنگ</p></div>
<div class="m2"><p>از تلاش رزق با بی دست و پایی فارغم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در بهشت عافیت افتاده ام، تا کرده است</p></div>
<div class="m2"><p>پاس وقت خود ز پاس آشنایی فارغم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ازمسلمانان نمی داند اگر زاهد مرا</p></div>
<div class="m2"><p>منت ایزد را ز کافر ماجرایی فارغم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون نگاه وحشیان الفت نمی دانم که چیست</p></div>
<div class="m2"><p>در میان مردمان از آشنایی فارغم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مشتری بسیار دارد چون گهر شد کم بها</p></div>
<div class="m2"><p>از شکست خویشتن از ناروایی فارغم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خاکساری بس بود صائب مرا خاک مراد</p></div>
<div class="m2"><p>بر در دو نان ز ننگ جبهه سایی فارغم</p></div></div>