---
title: >-
    غزل شمارهٔ ۴۳۱۵
---
# غزل شمارهٔ ۴۳۱۵

<div class="b" id="bn1"><div class="m1"><p>بی خواست حرف تلخی ازان نوش لب رسید</p></div>
<div class="m2"><p>آخر ز غیب روزی ما بی طلب رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خاکبوس دولت پابوس یافتم</p></div>
<div class="m2"><p>هر کس به هر کجا که رسید ار ادب رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر خضر زندگانی جاوید تلخ ساخت</p></div>
<div class="m2"><p>عمر دوباره ای که به من زان دولب رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در سینه حکمتی که فلاطون ذخیره داشت</p></div>
<div class="m2"><p>قالب تهی چو کرد به بنت العنب رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از اشک وآه کرد دل خویش را تهی</p></div>
<div class="m2"><p>چون شمع دست هر که به دامان شب رسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست از سبب مدار که با همت محیط</p></div>
<div class="m2"><p>آخر صدف به وصل گهر از سبب رسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرهیز کن که خونی غمهاست صحبتش</p></div>
<div class="m2"><p>چون باده نارسی که به جوش طرب رسید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از دست وپای بوسه فریب تو کار دل</p></div>
<div class="m2"><p>از دست رفته بود چو نوبت به لب رسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب حلاوت طلب او ز دل نرفت</p></div>
<div class="m2"><p>چندان که زخم خار به من زان رطب رسید</p></div></div>