---
title: >-
    غزل شمارهٔ ۳۳۷۶
---
# غزل شمارهٔ ۳۳۷۶

<div class="b" id="bn1"><div class="m1"><p>اگر آن غنچه دهن مهر ز لب برگیرد</p></div>
<div class="m2"><p>جگر تشنه خورشید به کوثر گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل مادر شکن زلف کند نشو و نما</p></div>
<div class="m2"><p>طفل ما پرورش از دامن محشر گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما چو مینا سر گفتار نداریم به خلق</p></div>
<div class="m2"><p>دیگری مهر مگر از لب ما برگیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مست عشق تو چه پروای ملامت دارد؟</p></div>
<div class="m2"><p>گردن شیشه به کف دامن محشر گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشق از نیستی آبستن هستی گردد</p></div>
<div class="m2"><p>مه نو فربهی از پهلوی لاغر گیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خلوت عشق کجا، نغمه منصور کجا؟</p></div>
<div class="m2"><p>کیست این شمع پریشان شده را سر گیرد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رشک بر دولت بیدار حباب است مرا</p></div>
<div class="m2"><p>که به هر چشم زدن عالم دیگر گیرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جلوه گاهش خم چوگان حوادث بادا</p></div>
<div class="m2"><p>صائب آن روز که سر از قدمت گیرد</p></div></div>