---
title: >-
    غزل شمارهٔ ۱۲۱۱
---
# غزل شمارهٔ ۱۲۱۱

<div class="b" id="bn1"><div class="m1"><p>بس که از زهر شکایت لب دل افگار بست</p></div>
<div class="m2"><p>دل مرا چون بسته در جیب و بغل زنگار بست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشرت فصل بهاران خنده واری بیش نیست</p></div>
<div class="m2"><p>وقت نخلی خوش که پیش از غنچه بستن بار بست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد ز پیوند تن افسرده، دل بی کسان به خاک</p></div>
<div class="m2"><p>وای بر خامی که نان خویش بر دیوار بست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست بی خورشید عالمتاب صبح انتظار</p></div>
<div class="m2"><p>پیر کنعان طرفها از چشم چون دستار بست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موم گردد سنگ خارا در کفش چون کوهکن</p></div>
<div class="m2"><p>روی گرم کارفرما هر که را بر کار بست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رشته پیوند یاران را بریدن کافری است</p></div>
<div class="m2"><p>تا برآمد از چمن گل بر میان زنار بست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که شد در حلقه سرگشتگان چون نقطه فرد</p></div>
<div class="m2"><p>از سر رغبت کمر در خدمتش پرگار بست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در عرق پوشیده گردید آن عذار شرمگین</p></div>
<div class="m2"><p>جوش گل راه تماشایی بر این گلزار بست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که صائب گوشه چشمی ز خواب امن دید</p></div>
<div class="m2"><p>بی تأمل در به روی دولت بیدار بست</p></div></div>