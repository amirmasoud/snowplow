---
title: >-
    غزل شمارهٔ ۷۵
---
# غزل شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>ننگ کفر من به فریاد آورد ناقوس را</p></div>
<div class="m2"><p>می کشد ایمان من در خون، لب افسوس را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از هوای نفس ظلمانی است سیر و دور خلق</p></div>
<div class="m2"><p>دود می آرد به جنبش صورت فانوس را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عیب خود دیدن مرا ز اهل هنر ممتاز کرد</p></div>
<div class="m2"><p>منفعت از پا زیاد از پر بود طاوس را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوف ما ز اعمال ناشایست خود باشد که نیست</p></div>
<div class="m2"><p>نامه قتلی به جز مکتوب خود، جاسوس را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عالم معقول صائب روی بنماید ترا</p></div>
<div class="m2"><p>گر توانی ترک کردن عالم محسوس را</p></div></div>