---
title: >-
    غزل شمارهٔ ۵۱۶
---
# غزل شمارهٔ ۵۱۶

<div class="b" id="bn1"><div class="m1"><p>نفس سوخته روشنگر جان است مرا</p></div>
<div class="m2"><p>چون شرر، زندگی از سوختگان است مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل سودا زده ام جوش بهاران دارد</p></div>
<div class="m2"><p>چهره از درد اگر برگ خزان است مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیخودی گرد ملال از دل من می شوید</p></div>
<div class="m2"><p>رفتن دل به نظر آب روان است مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر چه افتاده ام اما پی برداشتنم</p></div>
<div class="m2"><p>هر که قد راست کند تیر و سنان است مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گردش چرخ محال است مرا پیر کند</p></div>
<div class="m2"><p>همت پیر مغان، بخت جوان است مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نتوان شست به هر صید گشادن، ورنه</p></div>
<div class="m2"><p>آه تیری است که دایم به کمان است مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می کند سلسله عمر ابد را کوتاه</p></div>
<div class="m2"><p>گرهی چند که در رشته جان است مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در سفر عادت سیلاب بهاران دارم</p></div>
<div class="m2"><p>سختی راه طلب، سنگ فسان است مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در خریداری درد تو به جان بی تابم</p></div>
<div class="m2"><p>ورنه یوسف به زر قلب گران است مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست چون سرو، مرا بی ثمری بر دل بار</p></div>
<div class="m2"><p>که ز آسیب خزان خط امان است مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آب از دیده خورشید گشاید صائب</p></div>
<div class="m2"><p>در دل آیینه عذاری که نهان است مرا</p></div></div>