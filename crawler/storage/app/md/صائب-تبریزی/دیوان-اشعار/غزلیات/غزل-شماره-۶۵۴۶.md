---
title: >-
    غزل شمارهٔ ۶۵۴۶
---
# غزل شمارهٔ ۶۵۴۶

<div class="b" id="bn1"><div class="m1"><p>دام و کمند گردن دلهاست آرزو</p></div>
<div class="m2"><p>دل مشت خار و موجه دریاست آرزو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دامن گشاده صحرای سینه ها</p></div>
<div class="m2"><p>چون موجه سراب سبکپاست آرزو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از چشم سوزن است دل خلق تنگتر</p></div>
<div class="m2"><p>تا چون گره به رشته جانهاست آرزو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر لحظه خار پیرهن یوسفی شود</p></div>
<div class="m2"><p>گستاختر ز دست زلیخاست آرزو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گردی پدید نیست ازان آرزوی دل</p></div>
<div class="m2"><p>در عالمی که بادیه پیماست آرزو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آرزوست عالم ایجاد منتظم</p></div>
<div class="m2"><p>عالم بپاست تا به سرپاست آرزو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون خر به گل ز همت پست تو مانده است</p></div>
<div class="m2"><p>ورنه براق عالم بالاست آرزو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باقی شود چو صرف کنی در امور خیر</p></div>
<div class="m2"><p>هرچند بی ثبات چو دنیاست آرزو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا در تو هست خار هوس همچو گردباد</p></div>
<div class="m2"><p>بیهوده گرد دامن صحراست آرزو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عیسی به چرخ از دل بی آرزو رسید</p></div>
<div class="m2"><p>دل ساده کن که سلسله پاست آرزو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مهر سکوت با دل بی آرزو خوش است</p></div>
<div class="m2"><p>از خامشی چه سود چو گویاست آرزو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر کوچه ای که هست چو خورشید می دود</p></div>
<div class="m2"><p>یارب ز جستجوی که شیداست آرزو؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با خاک شد برابر ازین طفل مشربان</p></div>
<div class="m2"><p>ورنه ز نقص و عیب مبراست آرزو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زاهد اگر ز لذت دنیا گشته است</p></div>
<div class="m2"><p>چون طفل روزه دار سراپاست آرزو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در روزگار پاکی دامان حسن تو</p></div>
<div class="m2"><p>دست ز کار رفته دلهاست آرزو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کوتاه نیست دست تمنا ز هیچ کام</p></div>
<div class="m2"><p>جام جهان نمای نظرهاست آرزو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از آرزو اثر نبود در دل درست</p></div>
<div class="m2"><p>خونابه جراحت دلهاست آرزو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نتوان زدن به تیر هوایی نشانه را</p></div>
<div class="m2"><p>مقصود دل کجا و کجاهاست آرزو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صائب چو مومیایی و چون سنگ روز و شب</p></div>
<div class="m2"><p>در بستن و شکستن دلهاست آرزو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>این آن غزل که مولوی روم گفته است</p></div>
<div class="m2"><p>گر گوهری ببین که چه دریاست آرزو</p></div></div>