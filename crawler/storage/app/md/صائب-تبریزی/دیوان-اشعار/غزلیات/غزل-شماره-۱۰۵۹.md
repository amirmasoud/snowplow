---
title: >-
    غزل شمارهٔ ۱۰۵۹
---
# غزل شمارهٔ ۱۰۵۹

<div class="b" id="bn1"><div class="m1"><p>مرهم تیغ تغافل خون خود را خوردن است</p></div>
<div class="m2"><p>بخیه این زخم، دندان بر جگر افشردن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باده انگور کافی نیست مخمور مرا</p></div>
<div class="m2"><p>چاره من باغ را بر یکدگر افشردن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از سبکباری گرانجانان دنیا غافلند</p></div>
<div class="m2"><p>ورنه ذوق باختن بسیار بیش از بردن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لنگری چون بحر پیدا کن که روشن گوهری</p></div>
<div class="m2"><p>با کمال قدرت از هر موج سیلی خوردن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خون به خون شستن درین میدان، گل مردانگی است</p></div>
<div class="m2"><p>چاره مردن، به مرگ اختیاری مردن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غم ندارد راه در دارالامان خامشی</p></div>
<div class="m2"><p>غنچه تصویر فارغ از غم پژمردن است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غیر شغل دلفریب عشق، صائب در جهان</p></div>
<div class="m2"><p>رو به هر کاری که آری آخرش افسردن است</p></div></div>