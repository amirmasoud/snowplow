---
title: >-
    غزل شمارهٔ ۳۴۸۴
---
# غزل شمارهٔ ۳۴۸۴

<div class="b" id="bn1"><div class="m1"><p>عارفانی که به تسلیم و رضا ساخته اند</p></div>
<div class="m2"><p>مردمک را سپر تیر قضا ساخته اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وای بر ساده دلانی که ز دریا چو حباب</p></div>
<div class="m2"><p>از پی کسب هوا خانه جدا ساخته اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از پریشان نفسیهای تو تاریک شده است</p></div>
<div class="m2"><p>ورنه آیینه دل را بصفا ساخته اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جسم و جان تو نپیوسته به بازی برهم</p></div>
<div class="m2"><p>هر دو عالم به هم آورده ترا ساخته اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نفس خود نکنی راست درین وحشتگاه</p></div>
<div class="m2"><p>گر بدانی که ترا بهر کجا ساخته اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر به معمار ز غفلت نتوانی پی برد</p></div>
<div class="m2"><p>در کف خاک تو بنگر که چها ساخته اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون پریشان سفران خرج بیابان نشوند</p></div>
<div class="m2"><p>رهروانی که به یک راهنما ساخته اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لله الحمد که بی منت خواهش ز ازل</p></div>
<div class="m2"><p>رزق ما از دل صد پاره ما ساخته اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هیچ جا یک نفس از شوق ندارم آرام</p></div>
<div class="m2"><p>تا مرا همچو فلک بی سر و پا ساخته اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرکه خود را به تمامی شکند اوست تمام</p></div>
<div class="m2"><p>ماه را زین سبب انگشت نما ساخته اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خودپسندان که به کس دست ارادت ندهند</p></div>
<div class="m2"><p>از تکبر ز عصاکش به عصا ساخته اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صلح ارباب نفاق است پی جنگ دگر</p></div>
<div class="m2"><p>زره خویش نهان زیر قبا ساخته اند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فارغ از تنگی رزقند، نظر بازانی</p></div>
<div class="m2"><p>که ازان یار ستمگر به جفا ساخته اند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سفر کعبه حلال است به جمعی صائب</p></div>
<div class="m2"><p>که به همراهی هر آبله پا ساخته اند</p></div></div>