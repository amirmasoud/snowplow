---
title: >-
    غزل شمارهٔ ۶۵۷۳
---
# غزل شمارهٔ ۶۵۷۳

<div class="b" id="bn1"><div class="m1"><p>در گلویم اشک رنگارنگ می گردد گره</p></div>
<div class="m2"><p>کاروان در راههای تنگ می گردد گره</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست آغوش فلاخن جای لنگ سنگ را</p></div>
<div class="m2"><p>در سر مجنون کجا فرهنگ می گردد گره؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از تراوش زخم اگر مانع شود خوناب را</p></div>
<div class="m2"><p>شکوه هم در سینه های تنگ می گردد گره</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بعد عمری چون صدف گر قطره آبی خورم</p></div>
<div class="m2"><p>در گلوی تشنه ام چون سنگ می گردد گره</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در بیابانی که من چون گردباد افتاده ام</p></div>
<div class="m2"><p>راه می پیچد به خود، فرسنگ می گردد گره</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نعره از مستان تراوش می کند بی اختیار</p></div>
<div class="m2"><p>نغمه کی در ساز سیر آهنگ می گردد گره؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در کمان پیوسته می آید مرا بر سنگ تیر</p></div>
<div class="m2"><p>در دهان حرف من دلتنگ می گردد گره</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لنگر طاقت حریف خرده اسرار نیست</p></div>
<div class="m2"><p>این شرار شوخ کی در سنگ می گردد گره؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیچ و تابی موی آتشدیده را لازم بود</p></div>
<div class="m2"><p>گردرویش زان خط شبرنگ می گردد گره</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از دل خونگرم من دامن کشیدن مشکل است</p></div>
<div class="m2"><p>نقش بر آیینه ام چون زنگ می گردد گره</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرغ را در بیضه بال و پر گشودن مشکل است</p></div>
<div class="m2"><p>فکر صائب در زمین تنگ می گردد گره</p></div></div>