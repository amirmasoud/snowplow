---
title: >-
    غزل شمارهٔ ۳۶۸۵
---
# غزل شمارهٔ ۳۶۸۵

<div class="b" id="bn1"><div class="m1"><p>ز چهره تو نظرها پرآب می‌گردد</p></div>
<div class="m2"><p>ز آتش تو جگرها کباب می‌گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر به لب ز سر شیشه پنبه برداری</p></div>
<div class="m2"><p>ز یک پیاله دو عالم خراب می‌گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز دیده تو شود خیره، چشم گستاخی</p></div>
<div class="m2"><p>که بر ورق ورق آفتاب می‌گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حدیث تیغ تو هرجا که در میان آید</p></div>
<div class="m2"><p>دهان زخم شهیدان پرآب می‌گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فسرده‌ای که در اینجا به داغ عشق نسوخت</p></div>
<div class="m2"><p>در آفتاب قیامت کباب می‌گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به روزگار خط انداز کامجویی را</p></div>
<div class="m2"><p>که این دعا دل شب مستجاب می‌گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هلال غبغب جانان لطافتی دارد</p></div>
<div class="m2"><p>که از اشاره انگشت آب می‌گردد!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مدار چشم اقامت ز برگ عیش جهان</p></div>
<div class="m2"><p>که گل ز گرمرویی‌ها گلاب می‌گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز حسن عاقبت جستجو مشو نومید</p></div>
<div class="m2"><p>که خون سوختگان مشک ناب می‌گردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به نور عقل توان جمع ساختن خود را</p></div>
<div class="m2"><p>کتان درست درین ماهتاب می‌گردد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حجاب عشق گرفته است چشم ما صائب</p></div>
<div class="m2"><p>وگرنه دلبر ما بی‌حجاب می‌گردد</p></div></div>