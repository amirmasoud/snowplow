---
title: >-
    غزل شمارهٔ ۵۳۵۶
---
# غزل شمارهٔ ۵۳۵۶

<div class="b" id="bn1"><div class="m1"><p>تا به زانو رفته پای من به گل از لای خم</p></div>
<div class="m2"><p>پای رفتن نیست ازمیخانه ام چون پای خم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرد حلاجی می وحدت سر منصور را</p></div>
<div class="m2"><p>خشت بردارد می پرزور از بالای خم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخنه دل از می صافی نمی آید بهم</p></div>
<div class="m2"><p>می کنم اندود این ویرانه را از لای خم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دل پر جوش نتوانم به بالین سر نهاد</p></div>
<div class="m2"><p>نیست ممکن کف شود آسوده بر بالای خم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون توانم باده گلرنگ را بی پرده دید</p></div>
<div class="m2"><p>من که مستی می کنم از دیدن سیمای خم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرزموج می به فرقم تیغ بارد چون حباب</p></div>
<div class="m2"><p>نیست ممکن از سرم بیرون رود سودای خم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سفلگان در نعمت از منعم نمی آرند یاد</p></div>
<div class="m2"><p>چون سبو خالی شد ازمی می شود جویای خم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دخل دریا ابر را در خرج می سازد دلیر</p></div>
<div class="m2"><p>می کنم خالی به جرات شیشه را در پای خم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از دهان بسته باشد قفل روزی را کلید</p></div>
<div class="m2"><p>پر برآید کوزه لب بسته از دریای خم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کیست عقل شیشه دل تا کوس دانایی زند</p></div>
<div class="m2"><p>در خراباتی که افلاطون نگیرد جای خم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مذهب و مشرب به هم آمیختن حق من است</p></div>
<div class="m2"><p>می فشانم گرد راه کعبه را در پای خم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می گشاید دفتر صبح قیامت آسمان</p></div>
<div class="m2"><p>چون ز جوش باده آرد کف به لب دریای خم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شیشه نشکسته در پا گرچه کمتر می خلد</p></div>
<div class="m2"><p>توبه نشکسته افزون می خلد در پای خم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گربه این عنوان می روشن تجلی می کند</p></div>
<div class="m2"><p>همچو کوه طور می پاشد زهم اجزای خم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صیقل روح است صائب صحبت روشندلان</p></div>
<div class="m2"><p>می توان رودید در آیینه سیمای خم</p></div></div>