---
title: >-
    غزل شمارهٔ ۵۱۸۵
---
# غزل شمارهٔ ۵۱۸۵

<div class="b" id="bn1"><div class="m1"><p>جان آرمیده می‌شود از اضطراب عشق</p></div>
<div class="m2"><p>این رشته را دراز کند پیچ و تاب عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبح قیامت از دهن خم کند طلوع</p></div>
<div class="m2"><p>چون بر لب آورد کف مستی شراب عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مغزش ز جوش پرده افلاک می‌درد</p></div>
<div class="m2"><p>بر هر سری که سایه کند آفتاب عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آتش چه می‌کند به سپیدی که سوخته است؟</p></div>
<div class="m2"><p>از آفتاب حشر نسوزد کباب عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از خاک اهل عشق نظر خیره می‌شود</p></div>
<div class="m2"><p>از ابر پردگی نشود آفتاب عشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبض از هجوم درد شود بی‌قرارتر</p></div>
<div class="m2"><p>ساکن ز کوه غم نشود اضطراب عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نظاره شکسته‌دلان وحشت آورد</p></div>
<div class="m2"><p>سیلاب تند می‌گذرد از خراب عشق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صید مراد هردو جهان در کمند اوست</p></div>
<div class="m2"><p>در هر دلی که ریشه کند پیچ‌وتاب عشق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اکسیر بی‌نیازی ازین خاک می‌برند</p></div>
<div class="m2"><p>صائب چگونه پای کشد از جناب عشق؟</p></div></div>