---
title: >-
    غزل شمارهٔ ۲۰۲۳
---
# غزل شمارهٔ ۲۰۲۳

<div class="b" id="bn1"><div class="m1"><p>دلبستگی است مادر هر ماتمی که هست</p></div>
<div class="m2"><p>می زاید از تعلق ما، هر غمی که هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود را ز واصلان دیار فنا شمار</p></div>
<div class="m2"><p>تا بر دل تو سور شود ماتمی که هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با تشنگی باز که در زیر آسمان</p></div>
<div class="m2"><p>دلهای آب کرده بود، شبنمی که هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خود رمیده ای است که خود را نیافته است</p></div>
<div class="m2"><p>امروز در بساط جهان بیغمی که هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چند در دهان تو خاک سیه زنند</p></div>
<div class="m2"><p>چون نقش، خوش برآی بر هر خاتمی که هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زخم تو بی نیاز ز مرهم نمی شود</p></div>
<div class="m2"><p>تا صرف دیگران نکنی مرهمی که هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آتش ز سنگ و لعل ز خارا گرفته اند</p></div>
<div class="m2"><p>محکم بگیر دامن کوه غمی که هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر مهلت زمانه دون اعتماد نیست</p></div>
<div class="m2"><p>چون صبح در خوشی به سر آور دمی که هست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سالک اگر به دامن خود پای بشکند</p></div>
<div class="m2"><p>در دل کند مشاهده هر عالمی که هست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرگز سری ز روزن دل برنکرده اند</p></div>
<div class="m2"><p>جمعی که قانعند به این عالمی که هست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با خامشی بساز که در خاکدان دهر</p></div>
<div class="m2"><p>چاه فرامشی است همین محرمی که هست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صائب دو شش زدند درین عالم سپنج</p></div>
<div class="m2"><p>آنها که ساختند به نقش کسی که هست</p></div></div>