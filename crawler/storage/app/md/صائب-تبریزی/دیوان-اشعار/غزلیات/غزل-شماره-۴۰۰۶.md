---
title: >-
    غزل شمارهٔ ۴۰۰۶
---
# غزل شمارهٔ ۴۰۰۶

<div class="b" id="bn1"><div class="m1"><p>خرد به زور می ناب برنمی آید</p></div>
<div class="m2"><p>کتان ز عهده مهتاب برنمی آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درازدستی سنگ خطر اگر این است</p></div>
<div class="m2"><p>سبوی هیچ کس از آب برنمی آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل غیور مرا شکوه اختیاری نیست</p></div>
<div class="m2"><p>دهان زخم به خوناب بر نمی آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آن زمین که شهیدی به خون نغلطیده است</p></div>
<div class="m2"><p>بهار لاله سیراب برنمی آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر به آینه آفتاب سنگ خورد</p></div>
<div class="m2"><p>ز چشم سخت فلک آب برنمی آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز شور ناله صائب ز خواب مخمل جست</p></div>
<div class="m2"><p>هنوز چشم تو از خواب برنمی آید</p></div></div>