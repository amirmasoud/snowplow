---
title: >-
    غزل شمارهٔ ۲۷۷۲
---
# غزل شمارهٔ ۲۷۷۲

<div class="b" id="bn1"><div class="m1"><p>زلف او موی سفید نافه را در خون کشید</p></div>
<div class="m2"><p>شاخ سنبل را زگلشن موکشان بیرون کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رتبه من در سیه بختی بلند افتاده است</p></div>
<div class="m2"><p>کوکب من نیل بر رخساره گردون کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا به چشم خویش دید اشک سبکسیر مرا</p></div>
<div class="m2"><p>از خجالت موج پا در دامن جیحون کشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سنگ نااهلان درستی در سراپایم نهشت</p></div>
<div class="m2"><p>وقت مجنون خوش که پا در دامن هامون کشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روغن بادام می خواهد ز چشم آهوان</p></div>
<div class="m2"><p>خویش را در دامن صحرا ازان مجنون کشید</p></div></div>