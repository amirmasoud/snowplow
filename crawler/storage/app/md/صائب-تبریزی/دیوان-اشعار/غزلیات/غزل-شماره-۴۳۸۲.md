---
title: >-
    غزل شمارهٔ ۴۳۸۲
---
# غزل شمارهٔ ۴۳۸۲

<div class="b" id="bn1"><div class="m1"><p>گل مرتبه عارض جانانه نگیرد</p></div>
<div class="m2"><p>جای لب ساقی لب پیمانه نگیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیلاب بود کاسه همسایه این قوم</p></div>
<div class="m2"><p>کافر به سرکوی بتان خانه نگیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در سینه عشاق نماند گهر راز</p></div>
<div class="m2"><p>این تابه تفسیده به خود دانه نگیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیراب نگردد ز صدف تشنه گوهر</p></div>
<div class="m2"><p>پیش ره ما کعبه وبتخانه نگیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در پوست نگنجد دل خون گشته عاشق</p></div>
<div class="m2"><p>می چون رسد آرام به میخانه نگیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دایره سوختگان شمع خموش است</p></div>
<div class="m2"><p>تا شعله به بال وپر پروانه نگیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آورده محال است که چون آمده گردد</p></div>
<div class="m2"><p>عاقل ز جنون رتبه دیوانه نگیرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در دیده ما نیست به جز نقش تو محرم</p></div>
<div class="m2"><p>آیینه ما صورت بیگانه نگیرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون چاک نگردد دل شمشاد که آن زلف</p></div>
<div class="m2"><p>غیر از دل صد چاک به خود شانه نگیرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وحشت ز جهان لازم روشن گهران است</p></div>
<div class="m2"><p>جغدست دل هر که ز ویرانه نگیرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب ز خرابات محال است برآید</p></div>
<div class="m2"><p>تاجامی ازان نرگس مستانه نگیرد</p></div></div>