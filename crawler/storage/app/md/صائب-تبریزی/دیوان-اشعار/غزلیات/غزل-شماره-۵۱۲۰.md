---
title: >-
    غزل شمارهٔ ۵۱۲۰
---
# غزل شمارهٔ ۵۱۲۰

<div class="b" id="bn1"><div class="m1"><p>در عروج نشأه می می کند طوفان سماع</p></div>
<div class="m2"><p>کار دامن می کند بر آتش مستان سماع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از غبار کلفت و گرد غم و زنگ ملال</p></div>
<div class="m2"><p>پاک سازد صحن مجلس رابه یک جولان سماع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقده دلها ز رقص بیخودی وا می شود</p></div>
<div class="m2"><p>می کند این پسته لب بسته را خندان سماع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون خم چوگان به دست افشاندن مستانه ای</p></div>
<div class="m2"><p>گوی دلها را برد بیرون ازین میدان سماع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می کند جان مجرد را خلاص از قیدجسم</p></div>
<div class="m2"><p>ماه کنعان رابرون می آرد از زندان سماع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لنگر تمکین زاهد بادبان خواهد شدن</p></div>
<div class="m2"><p>فیض خودرا عام سازد گربه به این عنوان سماع</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه از دامن برافشاندن خطر دارد چراغ</p></div>
<div class="m2"><p>شمع صحبت را کند روشنتر از دامان سماع</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کشتی می را کند مستغنی از باد مراد</p></div>
<div class="m2"><p>چون شود در بزم مستان آستین افشان سماع</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در فلاخن می نهد برق تجلی طور را</p></div>
<div class="m2"><p>کوه را چون ابر می سازد سبک جولان سماع</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شوق درهر دل که باشد مطربی درکار نیست</p></div>
<div class="m2"><p>چون کف دریا کند دستار سرمستان سماع</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر چنین آهنگ خواهد شد سرود قمریان</p></div>
<div class="m2"><p>سروها را ریشه کن می سازد از بستان سماع</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کوچه ها پیداشود درآسمان چون رود نیل</p></div>
<div class="m2"><p>چون شود از مستی سرشار دست افشان سماع</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رقص هر جا هست باغ و بوستان در کار نیست</p></div>
<div class="m2"><p>بزم را از نونهالان می کند بستان سماع</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر به رقص آیند ارباب عمایم دور نیست</p></div>
<div class="m2"><p>آسیای آسمان را می کند گردان سماع</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صائب از رقص فلک هوش از سر من می رود</p></div>
<div class="m2"><p>باقد خم گر چه زیبا نیست از پیران سماع</p></div></div>