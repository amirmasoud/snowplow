---
title: >-
    غزل شمارهٔ ۳۳۰۱
---
# غزل شمارهٔ ۳۳۰۱

<div class="b" id="bn1"><div class="m1"><p>شوق من سرکشی از زلف معنبر دارد</p></div>
<div class="m2"><p>آتشم بال و پر از دامن محشر دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخن سرد چه تأثیر کند در دل گرم؟</p></div>
<div class="m2"><p>جوش دریا چه غم از خامی عنبر دارد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوان خورشید ز سرپوش بود مستغنی</p></div>
<div class="m2"><p>سر آزاده ما ننگ ز افسر دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عیب خود را چه خیال است نپوشد نادان؟</p></div>
<div class="m2"><p>کل محال است کلاه از سر خود بردارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تار سبحه است ز دل هر سر مو زان خم زلف</p></div>
<div class="m2"><p>گره افزون خورد آن رشته که گوهر دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست ممکن شود آسوده، دل از لرزیدن</p></div>
<div class="m2"><p>شانه تا راه در آن زلف معنبر دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بس که سیراب بود تیغ تو، در هر زخمی</p></div>
<div class="m2"><p>بر جگر سوختگان منت کوثر دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشم خورشید ز رخسار تو می آرد آب</p></div>
<div class="m2"><p>نسخه از روی تو آیینه چسان بردارد؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب از بس که جگر سوز بود مضمونش</p></div>
<div class="m2"><p>خطر از نامه من بال سمندر دارد</p></div></div>