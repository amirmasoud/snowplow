---
title: >-
    غزل شمارهٔ ۱۶۷۹
---
# غزل شمارهٔ ۱۶۷۹

<div class="b" id="bn1"><div class="m1"><p>ترا ز جان غم مال ای خسیس بیشترست</p></div>
<div class="m2"><p>علاقه تو به دستار بیشتر ز سرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خطر به قدر فزونی است مالداران را</p></div>
<div class="m2"><p>که خون فاسد، آهن ربای نیشترست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مریز پیش بخیل آب روی خود زنهار</p></div>
<div class="m2"><p>که آب تیشه سزاوار نخل بی ثمرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زمین پاک بود کهربای دانه پاک</p></div>
<div class="m2"><p>صدف ز پاکی دامن همیشه پرگهرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آفتاب نگردد به رنگ و بو غافل</p></div>
<div class="m2"><p>درین ریاض چو شبنم کسی که دیده ورست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترا ز داغ عزیزان رفته نیست خبر</p></div>
<div class="m2"><p>وگرنه لاله این باغ، پاره جگرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زبان شکوه ندارم ز خاکساریها</p></div>
<div class="m2"><p>چگونه سبز شود دانه ای که پی سپرست؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درآ به عالم بی انقلاب بیرنگی</p></div>
<div class="m2"><p>که ماهتاب و کتان همچو شیر با شکرست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی هزار شد از سینه بیقراری دل</p></div>
<div class="m2"><p>به مرغ وحشی ما آشیانه بال و پرست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز پرده سوزی شب، صبح شد گریبان چاک</p></div>
<div class="m2"><p>عدوی پرده خویش است هر که پرده درست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به مرگ باز نمانند سالکان ز طلب</p></div>
<div class="m2"><p>میان ره نکند خواب هر که دیده ورست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می رسیده ز خم جلوه می کند در جام</p></div>
<div class="m2"><p>نهفته های پدر جمله ظاهر از پسرست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به قدر پاس ادب فیض می رساند حسن</p></div>
<div class="m2"><p>که جای بهله کوتاه دست، بر کمرست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز دلشکستگی خود غمین مشو صائب</p></div>
<div class="m2"><p>که شیشه چون شکند در دکان شیشه گرست</p></div></div>