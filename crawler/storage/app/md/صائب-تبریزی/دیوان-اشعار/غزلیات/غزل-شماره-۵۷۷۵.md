---
title: >-
    غزل شمارهٔ ۵۷۷۵
---
# غزل شمارهٔ ۵۷۷۵

<div class="b" id="bn1"><div class="m1"><p>ز بوستان تو عشق بلند می گویم</p></div>
<div class="m2"><p>چو شبنم از گل روی تو دست می شویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمی توان دل مردم ربود و پس خم زد</p></div>
<div class="m2"><p>سواد زلف ترا مو بموی می جویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بس که تشنه بوی وفای نایابم</p></div>
<div class="m2"><p>به دستم ار گل کاغذ دهند می بویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حریف رشک نسیم دراز دست نیم</p></div>
<div class="m2"><p>حنای بیعت گل را ز دست می شویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وفا و مردمی از روزگار دارم چشم</p></div>
<div class="m2"><p>ببین ز ساده دلیها چه از که می جویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میان اینهمه نازک طبیعتان صائب</p></div>
<div class="m2"><p>منم که شعر ظفرخان پسند می گویم</p></div></div>