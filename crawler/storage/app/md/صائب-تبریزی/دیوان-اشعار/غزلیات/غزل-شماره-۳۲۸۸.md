---
title: >-
    غزل شمارهٔ ۳۲۸۸
---
# غزل شمارهٔ ۳۲۸۸

<div class="b" id="bn1"><div class="m1"><p>غنچه باغ حیا سر به گریبان خندد</p></div>
<div class="m2"><p>گل بی شرم بود آن که پریشان خندد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد چراغ ره تاریک عدم خنده برق</p></div>
<div class="m2"><p>کس درین غمکده دیگر به چه عنوان خندد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داغ خورشید گذارند به لخت جگرش</p></div>
<div class="m2"><p>هر که چون صبح درین بزم، پریشان خندد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبح را شرم شکر خند تو زندانی کرد</p></div>
<div class="m2"><p>غنچه گل به کدامین لب و دندان خندد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از ندامت همه دانند که گل خواهد چید</p></div>
<div class="m2"><p>بر رخ تیغ اگر زخم نمایان خندد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشود زخم زبان خار ره گرمروان</p></div>
<div class="m2"><p>ریگ بر کشمکش خار مغیلان خندد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل آگاه درین غمکده خرم نشود</p></div>
<div class="m2"><p>یوسف آن نیست که در گوشه زندان خندد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه تن شانه شمشاد ازان دندان است</p></div>
<div class="m2"><p>که به طول امل زلف پریشان خندد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مایه عشرت صائب دل آگاه بود</p></div>
<div class="m2"><p>دهن صبح ز خورشید درخشان خندد</p></div></div>