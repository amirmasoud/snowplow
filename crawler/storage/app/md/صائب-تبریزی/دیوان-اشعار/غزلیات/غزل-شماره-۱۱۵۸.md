---
title: >-
    غزل شمارهٔ ۱۱۵۸
---
# غزل شمارهٔ ۱۱۵۸

<div class="b" id="bn1"><div class="m1"><p>تنگ خلقی شعله دوزخ سرشتی بوده است</p></div>
<div class="m2"><p>جبهه وا کرده صحرای بهشتی بوده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اعتباری را که در خوبی سرآمد گشته بود</p></div>
<div class="m2"><p>ما به چشم عاقبت دیدیم زشتی بوده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دور باش صد بلا گردید درد و داغ عشق</p></div>
<div class="m2"><p>غوطه خوردن در دل آتش بهشتی بوده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در سر زاهد به غیر از خودپرستی هیچ نیست</p></div>
<div class="m2"><p>این کدوی پوچ، قندیل کنشتی بوده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از لحد خوابش نگردد تلخ چون تن پروران</p></div>
<div class="m2"><p>بستر و بالین هر کس خاک و خشتی بوده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شور لیلی از سر مجنون به جان دادن نرفت</p></div>
<div class="m2"><p>پیچ و تاب عشق، خط سرنوشتی بوده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرخ مینایی که عقل پیر یک دهقان اوست</p></div>
<div class="m2"><p>از سواد بیکران عشق، کشتی بوده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قامتش خم گشت و نگذارد قدم در راه راست</p></div>
<div class="m2"><p>راستی صائب عجب غفلت سرشتی بوده است</p></div></div>