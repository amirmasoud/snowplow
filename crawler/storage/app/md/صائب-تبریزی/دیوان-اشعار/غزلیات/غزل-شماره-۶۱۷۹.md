---
title: >-
    غزل شمارهٔ ۶۱۷۹
---
# غزل شمارهٔ ۶۱۷۹

<div class="b" id="bn1"><div class="m1"><p>سایه تا افتاد ازان مشکین سلاسل بر زمین</p></div>
<div class="m2"><p>آیه رحمت مسلسل گشت نازل بر زمین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون شفق از خاک خون آلود می خیزد غبار</p></div>
<div class="m2"><p>بس که در کوی تو آمد شیشه دل بر زمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر به این تمکین گذارد پای لیلی در رکاب</p></div>
<div class="m2"><p>از گرانباری گذارد سینه محمل بر زمین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لاله بی داغ تا دامان محشر سر زند</p></div>
<div class="m2"><p>خون ما هر جا چکد از تیغ قاتل بر زمین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در برومندی مکن با خاکساران سرکشی</p></div>
<div class="m2"><p>کز هجوم میوه گردد شاخ مایل بر زمین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از تحمل خصم بالادست گردد زیر دست</p></div>
<div class="m2"><p>موج تیغ از کف گذارد پیش ساحل بر زمین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست دست بی نیازان پست فطرت چون غبار</p></div>
<div class="m2"><p>ورنه افتاده است دامان وسایل بر زمین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روزی ثابت قدم آید به پای دیگران</p></div>
<div class="m2"><p>توشه را رهرو گذارد پیش منزل بر زمین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مشکل است از مردم آزاده دل برداشتن</p></div>
<div class="m2"><p>از صنوبر کی به افشاندن فتد دل بر زمین؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صفحه خاک سیه شایسته اقبال نیست</p></div>
<div class="m2"><p>هر طرف از جاده بنگر خط باطل بر زمین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر کف خاکی دهان شیر و کام اژدهاست</p></div>
<div class="m2"><p>چشم بگشا، پای خود مگذار غافل بر زمین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ترک این وحشت سرا شایسته افسوس نیست</p></div>
<div class="m2"><p>می زند بیهوده خود را مرغ بسمل بر زمین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کاهلی از بس که پیچیده است بر اعضای من</p></div>
<div class="m2"><p>می گذارد نقش پای من سلاسل بر زمین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کلک صائب در سخن چون سحرپردازی کند</p></div>
<div class="m2"><p>می شود یک چشم حیران چاه بابل بر زمین</p></div></div>