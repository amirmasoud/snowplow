---
title: >-
    غزل شمارهٔ ۶۰۸۵
---
# غزل شمارهٔ ۶۰۸۵

<div class="b" id="bn1"><div class="m1"><p>دل غمین ز اندیشه روزی درین عالم مکن</p></div>
<div class="m2"><p>بهر گندم پشت بر فردوس چون آدم مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ریزش خود را ز چشم مردمان پوشیده دار</p></div>
<div class="m2"><p>در سخاوت خویش را افسانه چون حاتم مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر نمی خواهی شود روشن به مردم حال تو</p></div>
<div class="m2"><p>راز خود را اخگر پیراهن محرم مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عالم بالاست جای این نهال بارور</p></div>
<div class="m2"><p>ریشه خود در زمین عاریت محکم مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نسیه کردن نعمت آماده را از عقل نیست</p></div>
<div class="m2"><p>التفات از کاسه زانو به جام جم مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عالم روشن به چشمت زود می سازد سیاه</p></div>
<div class="m2"><p>پشت خود خم در تلاش نام چون خاتم مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رو میاور در طواف کعبه با آلودگی</p></div>
<div class="m2"><p>گرد عصیان را غبار خاطر زمزم مکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لب ببند از حرف نیک و بد درین عبرت سرا</p></div>
<div class="m2"><p>خاطر آسوده خود شاهراه غم مکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشم اگر داری که با خورشید همزانو شوی</p></div>
<div class="m2"><p>گر ز گل بستر کنندت، خواب چون شبنم مکن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیش هر ناشسته رویی آبروی خود مریز</p></div>
<div class="m2"><p>در زمین شور، ابر خویش را بی نم مکن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خون ما را نیست جز اشک پشیمانی ثمر</p></div>
<div class="m2"><p>جوهر شمشیر خود را حلقه ماتم مکن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر چه صائب می دهد قسمت، به آن خرسند باش</p></div>
<div class="m2"><p>خاطر خود را غمین از فکر بیش و کم مکن</p></div></div>