---
title: >-
    غزل شمارهٔ ۳۱۰۵
---
# غزل شمارهٔ ۳۱۰۵

<div class="b" id="bn1"><div class="m1"><p>ز فرمان قضا گردنکشی دیوانگی باشد</p></div>
<div class="m2"><p>درین میدان سپر انداختن مردانگی باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زعجز من دلیر آن کس که می گردد نمی داند</p></div>
<div class="m2"><p>که پشت دست من سرپنجه مردانگی باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زیوسف من به بوی پیرهن قانع نمی گردم</p></div>
<div class="m2"><p>که دامان وسایل پرده بیگانگی باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به تشریف سجود آستان از دور خرسندم</p></div>
<div class="m2"><p>کیم من تا مرا اندیشه همخانگی باشد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دست دختر رز اختیار خویش را دادن</p></div>
<div class="m2"><p>در آیینه خردمندان نه از مردانگی باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زچندین قطره باران یکی گوهر شود صائب</p></div>
<div class="m2"><p>زصدعاقل یکی شایسته دیوانگی باشد</p></div></div>