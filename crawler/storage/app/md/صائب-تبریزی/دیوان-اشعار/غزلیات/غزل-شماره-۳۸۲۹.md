---
title: >-
    غزل شمارهٔ ۳۸۲۹
---
# غزل شمارهٔ ۳۸۲۹

<div class="b" id="bn1"><div class="m1"><p>فغان ز سینه آسوده محشر انگیزد</p></div>
<div class="m2"><p>گرستن از جگر گرم، کوثر انگیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه تن به مرده دلی داده ای، بر افغان زن</p></div>
<div class="m2"><p>که آه و ناله دل مرده را برانگیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زمین عرصه محشر گر آفتاب شود</p></div>
<div class="m2"><p>ترا عجب که به این دامن تر انگیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مباش کم ز سمندر درین جهان خنک</p></div>
<div class="m2"><p>که از بهم زدن بال، آذر انگیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو مور هرکه قناعت کند به تلخی عیش</p></div>
<div class="m2"><p>به هر طرف که رود گرد شکر انگیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دام عشق سزاوار، آتشین نفسی است</p></div>
<div class="m2"><p>که چون سپند ز جا دانه را برانگیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مکن به هر خس و خاری دهان خود را باز</p></div>
<div class="m2"><p>که خامشی ز دل غنچه ها زر انگیزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز آه ما مشو ای پادشاه حسن ملول</p></div>
<div class="m2"><p>که کیمیاست غباری که لشکر انگیزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شراب تلخ به دریا دلی حلال بود</p></div>
<div class="m2"><p>که چون محیط به هر موج گوهر انگیزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به گاه لطف چه احسان کند به خشک لبان</p></div>
<div class="m2"><p>به وقت خشم، محیطی که گوهر انگیزد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نمی رود دل خونین ز جا، که هیهات است</p></div>
<div class="m2"><p>که آتش از جگر لعل صرصر انگیزد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل غیور من از جا نمی رود به نگاه</p></div>
<div class="m2"><p>مگر سپند مرا روی دل برانگیزد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ربوده است ز من هوش ساقیی صائب</p></div>
<div class="m2"><p>که می ز ساغر چشم کبوتر انگیزد</p></div></div>