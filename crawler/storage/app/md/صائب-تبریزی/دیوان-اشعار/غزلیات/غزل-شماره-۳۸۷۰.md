---
title: >-
    غزل شمارهٔ ۳۸۷۰
---
# غزل شمارهٔ ۳۸۷۰

<div class="b" id="bn1"><div class="m1"><p>سبکروان ترا نقش پا نمی باشد</p></div>
<div class="m2"><p>اثر ز پاک فروشان بجانمی باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگاه حسن شناسان همیشه در سفرست</p></div>
<div class="m2"><p>دل غریب خیالان بجا نمی باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میان خال وخط وحسن راه بسیارست</p></div>
<div class="m2"><p>اگر چه لفظ ز معنی جدا نمی باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز نوبهار جوانی ذخیره ای بردار</p></div>
<div class="m2"><p>که رنگ وبوی جهان را وفا نمی باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مخور فریب تماشای روی کار جهان</p></div>
<div class="m2"><p>که هیچ آینه ای بی قفا نمی باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سعادت ازلی از دل شکسته طلب</p></div>
<div class="m2"><p>درین خرابه بغیر از همانمی باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غبار قافله آرزوست گردملال</p></div>
<div class="m2"><p>ملال در دل بی مدعا نمی باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گره چو وقت سرآیدگرهگشاگردد</p></div>
<div class="m2"><p>گشاد غنچه به دست صبا نمی باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به روشنایی هم می روندسوختگان</p></div>
<div class="m2"><p>به وادیی که منم نقش پا نمی باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به راه پر خطر عشق راست شو صائب</p></div>
<div class="m2"><p>که غیر راستی اینجا عصا نمی باشد</p></div></div>