---
title: >-
    غزل شمارهٔ ۳۹۳۵
---
# غزل شمارهٔ ۳۹۳۵

<div class="b" id="bn1"><div class="m1"><p>نقاب چهره چو آن زلف مشکفام کند</p></div>
<div class="m2"><p>صباح آینه را تیره تر ز شام کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا ز دام رهاکن که آن شکسته پرم</p></div>
<div class="m2"><p>که کار ناخنه بالم به چشم دام کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بال فاخته سرو تو سایبان دارد</p></div>
<div class="m2"><p>به هر طرف که چو آب روان خرام کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امیدوار چنانم که عشق زخم مرا</p></div>
<div class="m2"><p>رفو به رشته آن زلف مشکفام کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلند بخت حریفی که همچو شیشه می</p></div>
<div class="m2"><p>سر اطاعت خود وقف خط جام کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو شانه گر دل صد چاک صد زبان گردد</p></div>
<div class="m2"><p>به زلف او نتواند سخن تمام کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>توان به شب رخ راز نهان در او دیدن</p></div>
<div class="m2"><p>جلای آینه خاطری که جام کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فسون غیر زبان تواضعش بسته است</p></div>
<div class="m2"><p>مگر به گوشه ابروبه من سلام کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فتاده ام به زبانها چوشعر عام پسند</p></div>
<div class="m2"><p>سزای آن که چو عنقا تلاش نام کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تن چو سیم ازان چاک پیرهن منما</p></div>
<div class="m2"><p>مباد بوالهوسی آرزوی خام کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به خوان عفو نه آن شکرین مذاقم من</p></div>
<div class="m2"><p>که تلخ کام مرا زهر انتقام کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سترد نام مرا صائب از صحیفه دل</p></div>
<div class="m2"><p>خدای را کسی این ظلم را چه نام کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تلاش نام کند هر که در این جهان صائب</p></div>
<div class="m2"><p>سخن ز مدح ظفرخان نیکنام کند</p></div></div>