---
title: >-
    غزل شمارهٔ ۶۱۶۰
---
# غزل شمارهٔ ۶۱۶۰

<div class="b" id="bn1"><div class="m1"><p>دیده خونبار می خواهد نسیم پیرهن</p></div>
<div class="m2"><p>تشنه دیدار می خواهد نسیم پیرهن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرده ناموس زندان است حسن شوخ را</p></div>
<div class="m2"><p>کوچه و بازار می خواهد نسیم پیرهن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی سبب چشم زلیخا سرمه ضایع می کند</p></div>
<div class="m2"><p>چشم چون دستار می خواهد نسیم پیرهن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشک را با سینه چاکان التفات دیگرست</p></div>
<div class="m2"><p>خاطر افگار می خواهد نسیم پیرهن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فیض از مژگان خواب آلودگان رم می کند</p></div>
<div class="m2"><p>دیده بیدار می خواهد نسیم پیرهن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غنچه راز زلیخا هرزه خند افتاده است</p></div>
<div class="m2"><p>عاشق ستار می خواهد نسیم پیرهن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوش دکانی بر قماش ماه کنعان چیده است</p></div>
<div class="m2"><p>جلوه همکار می خواهد نسیم پیرهن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیچ و تاب سعی در دام آورد نخجیر را</p></div>
<div class="m2"><p>جان جوهردار می خواهد نسیم پیرهن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از خم زلفش که خون نافه را پامال کرد</p></div>
<div class="m2"><p>خاتم زنهار می خواهد نسیم پیرهن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر سحابی را دل از سرچشمه ای می نوشد آب</p></div>
<div class="m2"><p>چشم طوفان بار می خواهد نسیم پیرهن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زان عبیر ناز کز زلف تو می ریزد به خاک</p></div>
<div class="m2"><p>یک گریبان وار می خواهد نسیم پیرهن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فکر صائب را دلی چون برگ گل باید تنک</p></div>
<div class="m2"><p>ساحت گلزار می خواهد نسیم پیرهن</p></div></div>