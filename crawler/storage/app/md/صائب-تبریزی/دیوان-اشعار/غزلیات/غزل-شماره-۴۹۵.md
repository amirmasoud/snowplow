---
title: >-
    غزل شمارهٔ ۴۹۵
---
# غزل شمارهٔ ۴۹۵

<div class="b" id="bn1"><div class="m1"><p>نه به چشم و دل تنها نگرانیم ترا</p></div>
<div class="m2"><p>همچو دام از همه اعضا نگرانیم ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو به چندین نظر لطف نبینی در ما</p></div>
<div class="m2"><p>ما به یک دیده ز صد جا نگرانیم ترا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست نظاره رخسار تو مخصوص به چشم</p></div>
<div class="m2"><p>از سراپا، به سراپا نگرانیم ترا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرده چشم سزاوار تماشای تو نیست</p></div>
<div class="m2"><p>از سراپرده دلها نگرانیم ترا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل همان می تپد از شوق تماشای رخت</p></div>
<div class="m2"><p>گر به صد دیده بینا نگرانیم ترا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فارغیم از هوس سیر خیابان بهشت</p></div>
<div class="m2"><p>تا به آن قامت رعنا نگرانیم ترا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست مانع در و دیوار نظربازان را</p></div>
<div class="m2"><p>چون شرر در دل خارا نگرانیم ترا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه شود گر به نگاهی دل ما شاد کنی؟</p></div>
<div class="m2"><p>ما که از جمله دنیا نگرانیم ترا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست در دیده حیرت زده مطلب را راه</p></div>
<div class="m2"><p>ما نه از راه تمنا نگرانیم ترا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیده از خواب نمالیده روان می گردی</p></div>
<div class="m2"><p>گر بدانی چه قدرها نگرانیم ترا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نرسد دیده بدبین به تو ای وادی عشق</p></div>
<div class="m2"><p>که ز هر آبله پا نگرانیم ترا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هست با فکر تو کیفیت دیگر صائب</p></div>
<div class="m2"><p>نه به املا و به انشا نگرانیم ترا</p></div></div>