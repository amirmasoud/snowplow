---
title: >-
    غزل شمارهٔ ۱۹۰۵
---
# غزل شمارهٔ ۱۹۰۵

<div class="b" id="bn1"><div class="m1"><p>باغ و بهار چشم پر آب من آتش است</p></div>
<div class="m2"><p>ساقی و مطرب و می ناب من آتش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلبل نیم که آتش گل سازدم کباب</p></div>
<div class="m2"><p>پروانه ام، شراب و کباب من آتش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون عشق در طبیعت من انقلاب نیست</p></div>
<div class="m2"><p>صلح و نزاع و لطف و عتاب من آتش است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا روی آتشین نبود، وا نمی شوم</p></div>
<div class="m2"><p>چون اشک شمع، عالم آب من آتش است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سینه گداخته ام آه سرد نیست</p></div>
<div class="m2"><p>دریای آتشم که سحاب من آتش است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رسواترست پرده رازم، ز راز من</p></div>
<div class="m2"><p>داغ محبتم که نقاب من آتش است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طوطی نیم که آینه از من سخن کشد</p></div>
<div class="m2"><p>یاقوت کن قدح، که شراب من آتش است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باشد کباب آتش، هر جا سمندری است</p></div>
<div class="m2"><p>من آن سمندرم که کباب من آتش است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اشک یتیمم و عرق روی شرمگین</p></div>
<div class="m2"><p>از من حذر کنید که آب من آتش است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از اشک بلبلان گل من آب خورده است</p></div>
<div class="m2"><p>بگذر ز خون من که گلاب من آتش است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب من آن سمندر دیوانه مشربم</p></div>
<div class="m2"><p>کز دود خویش سلسله تاب من آتش است</p></div></div>