---
title: >-
    غزل شمارهٔ ۵۲۸۱
---
# غزل شمارهٔ ۵۲۸۱

<div class="b" id="bn1"><div class="m1"><p>از تحمل راه گفت و گو به دشمن بسته ام</p></div>
<div class="m2"><p>پیش سیلاب حوادث سد آهن بسته ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچنان دارد مرا سرگشته دوران گرچه من</p></div>
<div class="m2"><p>برشکم سنگ از قناعت چون فلاخن بسته ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دل آهن دم جان بخش را تاثیر نیست</p></div>
<div class="m2"><p>بی سبب خود را به عیسی همچو سوزن بسته ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سبکباران راه عشق خجلت می کشم</p></div>
<div class="m2"><p>بر کمر هر چند جای توشه دامن بسته ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست جزواکردن و پوشیدن چشم از جهان</p></div>
<div class="m2"><p>چون شرر طرفی که من از چشم روشن بسته ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ظلمت از کاشانه ام چون دود بیرون رفته است</p></div>
<div class="m2"><p>از فروغ عاریت تا چشم روزن بسته ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز خم سنگ آسوده سازد مار را از پیچ وتاب</p></div>
<div class="m2"><p>از جوانمردی کمر در خون دشمن بسته ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دانه ای هرچند صائب بس بود سالی مرا</p></div>
<div class="m2"><p>من کمر چون مور در تاراج خرمن بسته ام</p></div></div>