---
title: >-
    غزل شمارهٔ ۵۰۲۳
---
# غزل شمارهٔ ۵۰۲۳

<div class="b" id="bn1"><div class="m1"><p>رود چگونه به این ضعف کار من از پیش ؟</p></div>
<div class="m2"><p>که من به پای نسیم سحر روم ازخویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شود عیار بد ونیک در سفر ظاهر</p></div>
<div class="m2"><p>یکی است تیر کج و راست تا بود درکیش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عجب که برق فنا گرد من تواند یافت</p></div>
<div class="m2"><p>چنین که جلوه اومی برد مرا از خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشو ز ساقی یاقوت لب به می قانع</p></div>
<div class="m2"><p>مده به مطلب جزئی کریم راتشویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لب سؤال سزاوار بخیه بیشترست</p></div>
<div class="m2"><p>عبث به خرقه خود بخیه می زند درویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زکاوش مژه او حلاوتی دارم</p></div>
<div class="m2"><p>که جوی شهد بود درنظر مرا هر نیش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به خوردن دل خود همچو ماه قانع شو</p></div>
<div class="m2"><p>که دربساط فلک نیست رزق بی تشویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همان ز شرم کرم چهره اش عرق ریزست</p></div>
<div class="m2"><p>کریم اگر دو جهان را دهد به یک درویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلم به فقر و غنا ازقرار خویش نگشت</p></div>
<div class="m2"><p>به خشکی وتری آب گهر نشد کم و بیش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عیار ناله صائب مپرس از بیدرد</p></div>
<div class="m2"><p>نمک چه کار کندبادلی که نبود ریش ؟</p></div></div>