---
title: >-
    غزل شمارهٔ ۱۹۳۶
---
# غزل شمارهٔ ۱۹۳۶

<div class="b" id="bn1"><div class="m1"><p>در هر دلی که ریشه کند پیچ و تاب عشق</p></div>
<div class="m2"><p>پیوسته همچو زلف، سرش در کنار اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>موج سراب می شمرد سلسبیل را</p></div>
<div class="m2"><p>دلداده ای که تشنه بوس و کنار اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیراهنش قلمرو جولان یوسف است</p></div>
<div class="m2"><p>هر پرده دلی که در او خارخار اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چینی که از جبین نگشاید به زور می</p></div>
<div class="m2"><p>غافل مشو که سکه دارالعیار اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خونابه ای که می چکد از مو به موی ما</p></div>
<div class="m2"><p>بی اختیار دیده و دل، از فشار اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن پادشاه حسن که منظور صائب است</p></div>
<div class="m2"><p>خورشید، صید سلسله مشکبار اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن روی لاله رنگ که دل داغدار اوست</p></div>
<div class="m2"><p>چشم سهیل، خال لب جویبار اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رنگی که ریخت در قدح لعل، آفتاب</p></div>
<div class="m2"><p>ته جرعه ای ز لعل لب آبدار اوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با آن فروغ حسن، جگر گوشه سهیل</p></div>
<div class="m2"><p>برگ خزان رسیده ای از لاله زار اوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر شبنمی که هست درین باغ و بوستان</p></div>
<div class="m2"><p>گل را بهانه ساخته آیینه دار اوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گردون که نعل اوست در آتش ز آفتاب</p></div>
<div class="m2"><p>چون سبزه زیر سنگ ز کوه وقار اوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از دیده نظارگیان می برد غبار</p></div>
<div class="m2"><p>هر مصحف دلی که به خط غبار اوست</p></div></div>