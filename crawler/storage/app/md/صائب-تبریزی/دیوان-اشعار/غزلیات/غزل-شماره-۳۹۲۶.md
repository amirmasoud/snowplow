---
title: >-
    غزل شمارهٔ ۳۹۲۶
---
# غزل شمارهٔ ۳۹۲۶

<div class="b" id="bn1"><div class="m1"><p>دلی که آتش روی تواش کباب کند</p></div>
<div class="m2"><p>ز اشک شادی خودمستی شراب کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فغان که باده مردافکنی نمی یابم</p></div>
<div class="m2"><p>که چشم شوخ تو بیرحم را به خواب کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو چون در آیینه بینی عجب تماشایی است</p></div>
<div class="m2"><p>که آفتاب تماشای آفتاب کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سزای مرهم کافور سرد مهران است</p></div>
<div class="m2"><p>جراحتی که شکایت ز مشک ناب کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به حرف تلخ مرا مشفقی که توبه دهد</p></div>
<div class="m2"><p>علاج بیخودی بلبل از گلاب کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نظر ز تازه خطان دوختن به آن ماند</p></div>
<div class="m2"><p>که در بهار کسی توبه از شراب کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از آن دو زلف توزانوی خویش ته کرده است</p></div>
<div class="m2"><p>که پیش موی میان مشق پیچ وتاب کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز گرد رهزن صد کاروان هوش شود</p></div>
<div class="m2"><p>دلی که گردش چشم تواش خراب کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سراغ قبله کند در حرم سبک عقلی</p></div>
<div class="m2"><p>که جای بوسه ز روی تو انتخاب کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حدیث توبه رها کن که غفلت صائب</p></div>
<div class="m2"><p>ازان گذشته که اندیشه صواب کند</p></div></div>