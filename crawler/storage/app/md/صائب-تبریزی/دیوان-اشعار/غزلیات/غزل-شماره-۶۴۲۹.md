---
title: >-
    غزل شمارهٔ ۶۴۲۹
---
# غزل شمارهٔ ۶۴۲۹

<div class="b" id="bn1"><div class="m1"><p>ای دل به خرابات حقیقت گذری کن</p></div>
<div class="m2"><p>خود را به دو پیمانه جهان دگری کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با مردم دیوانه قلم را نبود کار</p></div>
<div class="m2"><p>از داغ جنون تیر قضا را سپری کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کردی سفر دور بسی سود نبخشید</p></div>
<div class="m2"><p>یک بار هم از خود سفر مختصری کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با آب و زمین عذر ز دهقان نپذیرند</p></div>
<div class="m2"><p>تقصیر مکن دانه خود را شجری کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از قیمت گوهر خبری نیست صدف را</p></div>
<div class="m2"><p>گنجینه خود عرض به صاحب نظری کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در هیچ زمین نیست که فیضی نبود فرش</p></div>
<div class="m2"><p>چون پرتو خورشید به هر جا گذری کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زر را به زر آن کس که دهد اهل تمیزست</p></div>
<div class="m2"><p>نقد دل و جان صرف بت سیمبری کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چن رشته دو تا شد ز گسستن شود ایمن</p></div>
<div class="m2"><p>پیوند دل زار به موی کمری کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کمتر نتوان بود به همت ز نگینی</p></div>
<div class="m2"><p>هر کار که نامی است به نام دگری کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در دایره بی خبران است خبرها</p></div>
<div class="m2"><p>تحقیق خبر از دل هر بی خبری کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سیرت نکند جلوه در آیینه فولاد</p></div>
<div class="m2"><p>زنهار در آیینه زانو نظری کن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در پرده دل گر همه یک قطره خون است</p></div>
<div class="m2"><p>چون آبله صرف قدم نیشتری کن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای چرخ ازین بیش مده جلوه خورشید</p></div>
<div class="m2"><p>این داغ جهانسوز به کار جگری کن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این آن غزل و الهی ماست که فرمود</p></div>
<div class="m2"><p>رو داغ به جانی نه و خون در جگری کن</p></div></div>