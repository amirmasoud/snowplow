---
title: >-
    غزل شمارهٔ ۶۱۲۱
---
# غزل شمارهٔ ۶۱۲۱

<div class="b" id="bn1"><div class="m1"><p>عشقبازی بود دایم در جهان آیین من</p></div>
<div class="m2"><p>چون سمندر بود از آتش بستر و بالین من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می شود در بستر تفسیده من گل گلاب</p></div>
<div class="m2"><p>می گدازد شمع را سرگرمی بالین من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فارغ از فکر مکافاتم که خصم کینه جو</p></div>
<div class="m2"><p>زنده زیر خاک باشد از غبار کین من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواب این دلمردگان از مرگ سنگین تر بود</p></div>
<div class="m2"><p>ورنه خون مرده گردد زنده از تلقین من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر دل پر شور من دست نوازش بیهده است</p></div>
<div class="m2"><p>پنجه مرجان چو دریا کی دهد تسکین من؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست یک دل کز ملال خاطرم دلگیر نیست</p></div>
<div class="m2"><p>باغ را در بسته دارد غنچه غمگین من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تلخکامی نیست چون من در میان خستگان</p></div>
<div class="m2"><p>زهر چشم یار باشد شربت شیرین من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب از غیرت شود خون مشک در ناف غزال</p></div>
<div class="m2"><p>هر کجا در جلوه آید خامه مشکین من</p></div></div>