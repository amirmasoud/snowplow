---
title: >-
    غزل شمارهٔ ۴۰۵۲
---
# غزل شمارهٔ ۴۰۵۲

<div class="b" id="bn1"><div class="m1"><p>چشم تو دل به شیوه پنهان نمی برد</p></div>
<div class="m2"><p>دزدیده این متاع به دکان نمی برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر در گلوی خامه بریزند آب خضر</p></div>
<div class="m2"><p>مکتوب اشتیاق به پایان نمی برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبنم کند به دامن پاکم چو گل نماز</p></div>
<div class="m2"><p>بلبل چرا مرا به گلستان نمی برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین خنجری که برق تجلی فسان زده است</p></div>
<div class="m2"><p>موسی اگر مسیح شود جان نمی برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیهوده حلقه بر در دل می زند نسیم</p></div>
<div class="m2"><p>این غنچه ره به خنده چو پیکان نمی برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیچیده آه ودود زلیخا به باد مصر</p></div>
<div class="m2"><p>زان بوی پیرهن سوی کنعان نمی برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طومار زلف یار که عمرش دراز باد</p></div>
<div class="m2"><p>دل را ز دست من به چه عنوان نمی برد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن را که ذوق تنگدلی در بغل گرفت</p></div>
<div class="m2"><p>لذت ز سیر چاک گریبان نمی برد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی اختیار عشق به دل پای می نهد</p></div>
<div class="m2"><p>سیل انتظار رخصت دربان نمی برد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای بوسه لب بگز که هنوز از هجوم شرم</p></div>
<div class="m2"><p>راهی به غنچه دهنش پان نمی برد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب سخن به بزم ظفرخان چه می بری</p></div>
<div class="m2"><p>حکمت کسی به خطه یونان نمی برد</p></div></div>