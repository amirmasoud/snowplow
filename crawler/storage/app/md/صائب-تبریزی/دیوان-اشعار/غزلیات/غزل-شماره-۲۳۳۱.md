---
title: >-
    غزل شمارهٔ ۲۳۳۱
---
# غزل شمارهٔ ۲۳۳۱

<div class="b" id="bn1"><div class="m1"><p>از فسون عالم اسباب خوابم می‌برد</p></div>
<div class="m2"><p>پیش پای یک جهان سیلاب خوابم می‌برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سبزه خوابیده را بیدار سازد آب و من</p></div>
<div class="m2"><p>چون شوم مست از شراب ناب خوابم می‌برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از سرم تا نگذرد می کم نگردد رعشه‌ام</p></div>
<div class="m2"><p>همچو ماهی در میان آب خوابم می‌برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در مقام فیض غفلت زور می‌آرد به من</p></div>
<div class="m2"><p>بیشتر در گوشه محراب خوابم می‌برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست غیر از گوشه عزلت مرا جایی قرار</p></div>
<div class="m2"><p>در صدف چون گوهر سیراب خوابم می‌برد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من که چون جوهر به روی تیغ دارم پیچ و تاب</p></div>
<div class="m2"><p>کی به روی سبزه سیراب خوابم می‌برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیش ازین بر روی خاک تیره آرامم نبود</p></div>
<div class="m2"><p>این زمان بر بستر سنجاب خوابم می‌برد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غفلت من از شتاب زندگی خواهد فزود</p></div>
<div class="m2"><p>رفته‌رفته زین صدای آب خوابم می‌برد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اضطراب راهرو را قرب منزل کم کند</p></div>
<div class="m2"><p>در کنار خنجر قصاب خوابم می‌برد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در حریم وصل حیرت می‌کند غافل مرا</p></div>
<div class="m2"><p>در چمن چون نرگس شاداب خوابم می‌برد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون کباب در نمک خوابیده شور من به جاست</p></div>
<div class="m2"><p>گاه‌گاهی گر شب مهتاب خوابم می‌برد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دارد از لغزش مرا صائب گرانی بی‌نصیب</p></div>
<div class="m2"><p>در کف آیینه چون سیماب خوابم می‌برد</p></div></div>