---
title: >-
    غزل شمارهٔ ۱۵۱۶
---
# غزل شمارهٔ ۱۵۱۶

<div class="b" id="bn1"><div class="m1"><p>بند و زندان گرامی گهران از جاه است</p></div>
<div class="m2"><p>یوسف ما به عزیزی چو رسد در چاه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راستان از سخن خویش نگردند به تیغ</p></div>
<div class="m2"><p>شمع تا کشته شدن با همه کس همراه است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر قدر جامه او بر قد سروست دراز</p></div>
<div class="m2"><p>جامه سرو سهی بر قد او کوتاه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به چه امید کسی از وطن آید بیرون؟</p></div>
<div class="m2"><p>منزل اول یوسف چو درین ره چاه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خال شبرنگ بر آن گوشه ابرو صائب</p></div>
<div class="m2"><p>عارفان را به نظر نقطه بسم الله است</p></div></div>