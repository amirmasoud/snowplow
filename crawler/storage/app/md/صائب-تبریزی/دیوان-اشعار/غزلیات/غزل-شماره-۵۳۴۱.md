---
title: >-
    غزل شمارهٔ ۵۳۴۱
---
# غزل شمارهٔ ۵۳۴۱

<div class="b" id="bn1"><div class="m1"><p>گر چه زیر تیغ لنگردار مسکن داشتم</p></div>
<div class="m2"><p>پای چون کوه از گرانسنگی به دامن داشتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیل چشم زخم شد سودای مجنون مرا</p></div>
<div class="m2"><p>جای هر سنگی که از اطفال بر تن داشتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذوق دلتنگی گریبانگیر شد چون غنچه ام</p></div>
<div class="m2"><p>ورنه برگ عیش چون گل من به دامن داشتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کاسه دریوزه از روزن نبردم پیش ماه</p></div>
<div class="m2"><p>خانه خود را ز برق آه روشن داشتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیخبر نگذشتم از پایی که زخم خار داشت</p></div>
<div class="m2"><p>چشم در دنبال دایم همچو سوزن داشتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا چو ماهی بر کنار افتادم از بحر عدم</p></div>
<div class="m2"><p>داغ حسرت گشت هر فلسی که بر تن داشتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قامت خم برنیاورد از گرانخوابی مرا</p></div>
<div class="m2"><p>پشت خود بر کوه چون سنگ از فلاخن داشتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برگ کاهی قسمت مور حریص من نشد</p></div>
<div class="m2"><p>از عزیزانی که من امید خرمن داشتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سرو را آزادیم در پیچ و تاب رشک داشت</p></div>
<div class="m2"><p>گرچه طوق بندگی صائب به گردن داشتم</p></div></div>