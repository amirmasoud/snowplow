---
title: >-
    غزل شمارهٔ ۳۶۱۵
---
# غزل شمارهٔ ۳۶۱۵

<div class="b" id="bn1"><div class="m1"><p>حسن از دیدن خود بر سر بیداد آید</p></div>
<div class="m2"><p>کار شمشیر ز آیینه فولاد آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشتگان تو ز غیرت همه محسود همند</p></div>
<div class="m2"><p>گرچه یکدست خط از خامه فولاد آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دل خونشده ماست نگارین پایش</p></div>
<div class="m2"><p>چون ازان زلف برون شانه شمشاد آید؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفس کامل شود از تنگی زندان بدن</p></div>
<div class="m2"><p>دیو ازین شیشه برون همچو پریزاد آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل اگر نالد ازان خنده پنهان چه عجب؟</p></div>
<div class="m2"><p>کز نمک آتش سوزنده به فریاد آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخن هرکه ندارد ز تأمل مغزی</p></div>
<div class="m2"><p>سست باشد، اگر از خامه فولاد آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاهد تیرگی جهل بود لاف گزاف</p></div>
<div class="m2"><p>که سگ از سرمه شب بیش به فریاد آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرچه از چهره پرد رنگ ز سیلی صائب</p></div>
<div class="m2"><p>رنگ بر روی من از سیلی استاد آید</p></div></div>