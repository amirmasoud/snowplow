---
title: >-
    غزل شمارهٔ ۲۱۲۱
---
# غزل شمارهٔ ۲۱۲۱

<div class="b" id="bn1"><div class="m1"><p>آفاق منور ز رخ انور صبح است</p></div>
<div class="m2"><p>این دایره را چشم و چراغ اختر صبح است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انگیختن از خواب گران مرده دلان را</p></div>
<div class="m2"><p>فیضی است که خاص دم جان پرور صبح است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیم و زر انجم که فلک شب همه شب جمع</p></div>
<div class="m2"><p>سازد، همه از بهر نثار سر صبح است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روشن نفسان شهپر بی بال و پرانند</p></div>
<div class="m2"><p>خورشید، فلک سیر به بال و پر صبح است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در عالم دربسته غیبم نبود راه</p></div>
<div class="m2"><p>گر هست در اینجا در فیضی در صبح است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرگز ز شکرخنده خوبان نتوان یافن</p></div>
<div class="m2"><p>این چاشنی خاص که در شکر صبح است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون پنجه خورشید شود زود زبردست</p></div>
<div class="m2"><p>هر دست دعایی که به زیر سر صبح است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز آیینه دلها به نفس زنگ زداید</p></div>
<div class="m2"><p>یارب ید بیضای که روشنگر صبح است؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خورشید که روشنگر آفاق جهان است</p></div>
<div class="m2"><p>چون بیضه نهان در ته بال و پر صبح است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از پنجه خونین شفق باک ندارد</p></div>
<div class="m2"><p>از پاکی سرشار که در گوهر صبح است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در عنبر شب همچو بهارست نهفته</p></div>
<div class="m2"><p>آن نور جهانتاب که در گوهر صبح است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از عالم بالا نظر ثابت و سیار</p></div>
<div class="m2"><p>یکسر همه محو رخ خوش منظر صبح است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ذرات جهان را نظر از خواب گشودن</p></div>
<div class="m2"><p>موقوف گشاد نظر انور صبح است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کم نیست ز سر جوش اگر وقت شناسی</p></div>
<div class="m2"><p>هر چند که شب درد ته ساغر صبح است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون صفحه خورشید ورق در کف صائب</p></div>
<div class="m2"><p>روشندل ازان است که مدحتگر صبح است</p></div></div>