---
title: >-
    غزل شمارهٔ ۴۱۹۲
---
# غزل شمارهٔ ۴۱۹۲

<div class="b" id="bn1"><div class="m1"><p>نازش کسی که بر پدر خویش می کند</p></div>
<div class="m2"><p>سلب نجابت از گهرخویش می کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از مستی غرور نبیند به پیش پا</p></div>
<div class="m2"><p>طاوس تا نظر به پر خویش می کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوهر که هست مردمک دیده صدف</p></div>
<div class="m2"><p>خاک از غبار دل به سر خویش می کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دیگری است هر چه گره می زنی برآن</p></div>
<div class="m2"><p>کی تر صدف لب از گهر خویش می کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در بر گریز بلبل رنگین خیال ما</p></div>
<div class="m2"><p>سیر چمن به زیر پر خویش می کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر باد می رود ز سبکدستی خزان</p></div>
<div class="m2"><p>چون غنچه هرکه جمع زر خویش می کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ایمن ز زخم خار شود هر که همچوگل</p></div>
<div class="m2"><p>روی گشاده را سپر خویش می کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دست از هوس بشوی که شبنم ز برگ گل</p></div>
<div class="m2"><p>بسترز پاکی نظر خویش می کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از عاجزان بترس که از زخم پشه فیل</p></div>
<div class="m2"><p>خاک سیه به فرق سر خویش می کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ایمن بود هنروری از چشم شور خلق</p></div>
<div class="m2"><p>کز عیب پرده هنر خویش می کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دندانه می کند دم شمشیر برق را</p></div>
<div class="m2"><p>خاری که دست را سپر خویش می کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دنبال چشم هر که زند قطره چون حباب</p></div>
<div class="m2"><p>سر در سر هوای سر خویش می کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دستش اکر به دامن پیر مغان رسد</p></div>
<div class="m2"><p>صائب علاج دردسر خویش می کند</p></div></div>