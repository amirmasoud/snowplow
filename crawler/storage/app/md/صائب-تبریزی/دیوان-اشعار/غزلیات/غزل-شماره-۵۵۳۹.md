---
title: >-
    غزل شمارهٔ ۵۵۳۹
---
# غزل شمارهٔ ۵۵۳۹

<div class="b" id="bn1"><div class="m1"><p>ز دامن نگذرد پای زمین گیری که من دارم</p></div>
<div class="m2"><p>گران محمل تر از خواب است شبگیری که من دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کند خون در جگر بسیار نعمتهای الوان را</p></div>
<div class="m2"><p>درین مهمانسرا چشم و دل سیری که من دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شود سیر از جهان برهر که افتد چشم سیر من</p></div>
<div class="m2"><p>کدامین کیمیاگر دارد اکسیری که من دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من و نالیدن از سودای عشق او معاذالله</p></div>
<div class="m2"><p>نمی آید صدا بیرون ز زنجیری که من دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز وحشت خانه صیاد داند سایه خود را</p></div>
<div class="m2"><p>درین وادی نظر بر صید نخجیری که من دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز خجلت آه بی تاثیر من در دل بود دایم</p></div>
<div class="m2"><p>ز ترکش بر نیاید از کجی تیری که من دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمی باید سلاحی تیز دستان شجاعت را</p></div>
<div class="m2"><p>که در سرپنجه خصم است شمشیری که من دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شراب کهنه در پیری مرا دارد جوان دایم</p></div>
<div class="m2"><p>که دارد از مریدان این چنین پیری که من دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مگر در خواب بیند کعبه مقصود را صائب</p></div>
<div class="m2"><p>درین وادی ز عزم سست شبگیری که من دارم</p></div></div>