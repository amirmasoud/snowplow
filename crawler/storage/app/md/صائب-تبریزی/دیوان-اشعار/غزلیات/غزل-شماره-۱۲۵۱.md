---
title: >-
    غزل شمارهٔ ۱۲۵۱
---
# غزل شمارهٔ ۱۲۵۱

<div class="b" id="bn1"><div class="m1"><p>عالم اسباب غیر از پرده های خواب نیست</p></div>
<div class="m2"><p>دیده بیدار دل بر عالم اسباب نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می کند خورشید هم دریوزه آب از دیده ها</p></div>
<div class="m2"><p>نه همین در دیده بی شرم انجم آب نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیر و دور ما به سیر و دور گردون بسته است</p></div>
<div class="m2"><p>اختیاری موج را در حلقه گرداب نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لرزد از ظالم فزون مظلوم در زیر فلک</p></div>
<div class="m2"><p>گرگ را چون گوسفند اندیشه از قصاب نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون به منزل پشت پا در رهنوردی می زند؟</p></div>
<div class="m2"><p>جذبه دریا اگر خضر ره سیلاب نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا مباد از قیمت نازل به خاکش افکنند</p></div>
<div class="m2"><p>گوهر ما در صدف بی رعشه سیماب نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در جهان ساده لوحی نقش نامحرم بود</p></div>
<div class="m2"><p>در حریم کعبه طاق ابروی محراب نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جوهر تیغ است داغ پیچ و تاب آن کمر</p></div>
<div class="m2"><p>این قدر در موی آتش دیده پیچ و تاب نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همچو غواصان به جای بی نفس کن جستجو</p></div>
<div class="m2"><p>گر چه در این نه صدف آن گوهر نایاب نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هوشیارانند صائب مصرف این سیم قلب</p></div>
<div class="m2"><p>در حریم میکشان رسم تکلف باب نیست</p></div></div>