---
title: >-
    غزل شمارهٔ ۲۷۸۷
---
# غزل شمارهٔ ۲۷۸۷

<div class="b" id="bn1"><div class="m1"><p>خوش بهاری می رسد میخانه ها سامان کنید</p></div>
<div class="m2"><p>برگ عیش آماده بهر جشن گلریزان کنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فصل گل در خانه بودن عمر ضایع کردن است</p></div>
<div class="m2"><p>با حریفان موافق روی در بستان کنید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از هوای تر جهان دریای رحمت گشته است</p></div>
<div class="m2"><p>کشتی می را درین دریا سبک جولان کنید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دفتر عیش و نشاط از یکدگر پاشیده است</p></div>
<div class="m2"><p>منتظم این نسخه را از رشته باران کنید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سینه ها را باده گلگون گلستان می کند</p></div>
<div class="m2"><p>دیده را از دیدن گلها نگارستان کنید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پله میزان روز و شب برابر گشته است</p></div>
<div class="m2"><p>روز و شب را در نشاط و خرمی یکسان کنید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گردش پرگار را یک نقطه بال و پر بس است</p></div>
<div class="m2"><p>هست تا در جام و مینا قطره ای، طوفان کنید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لنگر تمکین مناسب نیست در جوش بهار</p></div>
<div class="m2"><p>کوه را، هم سیر با ابر سبک جولان کنید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سینه را دریا کنید از ابر دست ساقیان</p></div>
<div class="m2"><p>دستها را از قدح سر پنجه مرجان کنید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا نیفتاده است باد نوبهاران از نفس</p></div>
<div class="m2"><p>غنچه ای گر هست در خاطر گره، خندان کنید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جوش گل دیوار و در را در سماع آورده است</p></div>
<div class="m2"><p>کم نه اید از مشت گل، رقصی درین بستان کنید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ابرهای تیره را صیقل شراب روشن است</p></div>
<div class="m2"><p>چاره این ظلمت از سرچشمه حیوان کنید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بزم را پرشور اگر خواهید و دلها را کباب</p></div>
<div class="m2"><p>کلک صائب را به تحسینی سبک جولان کنید</p></div></div>