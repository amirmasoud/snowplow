---
title: >-
    غزل شمارهٔ ۵۱۵۴
---
# غزل شمارهٔ ۵۱۵۴

<div class="b" id="bn1"><div class="m1"><p>نیست بر آیینه دُردی‌کشان گرد خلاف</p></div>
<div class="m2"><p>می‌توان چون جام می دیدن ته دل‌های صاف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان شراب لعل سرگرمم که کمتر قطره‌اش</p></div>
<div class="m2"><p>سوخت کام لاله آتش زبان را تا به ناف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باده بی‌درد از میخانه دوران مجوی</p></div>
<div class="m2"><p>لاله نتوانست یک پیمانه می را کرد صاف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاکساران محبت را شکوه دیگرست</p></div>
<div class="m2"><p>سبزه از بال و پر سیمرغ دارد کوه قاف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما کجا، اندیشه بر گرد سرگشتن کجا؟</p></div>
<div class="m2"><p>می‌کند خورشید تابان ذره را گرم طواف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درنگیرد صحبت عشق و خرد با یکدیگر</p></div>
<div class="m2"><p>چون دو شمشیرست عقل و عشق و دل چون یک غلاف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رو نگردانند عشاق از غبار حادثات</p></div>
<div class="m2"><p>آبروی جوهر مردی بود گرد مصاف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر صفت را از بهارستان قدرت صورتی است</p></div>
<div class="m2"><p>زان به شکل خنجر الماس می‌روید خلاف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غمزه‌اش از قحط دل، دزدیده می‌آید به چشم</p></div>
<div class="m2"><p>هیچ کافر برنگردد دست خالی از مصاف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرکه دستش بر زبان سبقت کند مرد است مرد</p></div>
<div class="m2"><p>ورنه هر ناقص جوانمرد است در میدان لاف</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در دل تنگم خیال طاق ابرویش ببین</p></div>
<div class="m2"><p>گر ندیدستی دو تیغ بی‌امان در یک غلاف</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در جواب این غزل گستاخ اگر پیش آمده است</p></div>
<div class="m2"><p>قاسم انوار خواهد داشت صائب را معاف</p></div></div>