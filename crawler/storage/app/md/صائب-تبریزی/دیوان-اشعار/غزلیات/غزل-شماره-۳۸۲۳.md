---
title: >-
    غزل شمارهٔ ۳۸۲۳
---
# غزل شمارهٔ ۳۸۲۳

<div class="b" id="bn1"><div class="m1"><p>به می غم از دل افگار برنمی خیزد</p></div>
<div class="m2"><p>به آب از آینه زنگار برنمی خیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز داغ نیست دل دردمند من خالی</p></div>
<div class="m2"><p>که شمع از سر بیمار برنمی خیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجو ملایمت از مردم خسیس نهاد</p></div>
<div class="m2"><p>که بوی گل ز خس و خار برنمی خیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کدام سرد نفس در میان این جمع است؟</p></div>
<div class="m2"><p>که مهرم از لب گفتار برنمی خیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکسته تو عمارت پذیر نیست چو ماه</p></div>
<div class="m2"><p>فتاده تو چو دیوار برنمی خیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به آب هرزه درا تهمت است همواری</p></div>
<div class="m2"><p>صدا ز مردم هموار برنمی خیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شده است عام ز بس قحط خنده شادی</p></div>
<div class="m2"><p>صدای کبک ز کهسار برنمی خیزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به محفلی که خوشامد فسانه پردازست</p></div>
<div class="m2"><p>ز خواب، دولت بیدار برنمی خیزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چگونه پشت لب یار سبز شد از خط؟</p></div>
<div class="m2"><p>گیاه اگر ز نمکزار برنمی خیزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گذشت حشر و همان خواب خواجه سنگین است</p></div>
<div class="m2"><p>ز خاک زود گرانبار برنمی خیزد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گهر شود چو صدف در زمین قابل تخم</p></div>
<div class="m2"><p>ز خاک میکده هشیار برنمی خیزد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نمی شود به زر و سیم حرص مستغنی</p></div>
<div class="m2"><p>به گنج پیچ و خم از مار برنمی خیزد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر نه سرمه خواب است تیرگی صائب</p></div>
<div class="m2"><p>چرا ز خواب، سیه کار برنمی خیزد؟</p></div></div>