---
title: >-
    غزل شمارهٔ ۲۳۷۴
---
# غزل شمارهٔ ۲۳۷۴

<div class="b" id="bn1"><div class="m1"><p>هر که چون آب روان آیینه خود ساده کرد</p></div>
<div class="m2"><p>سرو را چون بندگان در پیش خود استاده کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی به گرد من رسد مجنون، که کوه و دشت را</p></div>
<div class="m2"><p>دور باش وحشت من از غزالان ساده کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نغمه رنگین نبرد از جای خود زهاد را</p></div>
<div class="m2"><p>گرچه خم را پایکوبان نشأه این باده کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد به چشمم توتیا گرد یتیمی تا محیط</p></div>
<div class="m2"><p>از صدف گهواره در یتیم آماده کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از ملاحت مستی آن لعل میگون کم نشد</p></div>
<div class="m2"><p>کار صد بیهوشدارو این نمک با باده کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پاک کرد از آرزوها عشق صادق سینه را</p></div>
<div class="m2"><p>صبح از نقش پریشان آسمان را ساده کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا شود بر پیروان آسان ره دیوانگی</p></div>
<div class="m2"><p>دشت را مجنون صحراگرد من پر جاده کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاشقان پیش تو بیقدرند، ورنه شمع را</p></div>
<div class="m2"><p>انتظار صحبت پروانه ها استاده کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کم نشد چون غنچه صائب برگ عیش از خلوتش</p></div>
<div class="m2"><p>هر که از گلشن قناعت با دل نگشاده کرد</p></div></div>