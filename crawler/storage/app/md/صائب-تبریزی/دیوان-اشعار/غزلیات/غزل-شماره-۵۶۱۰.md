---
title: >-
    غزل شمارهٔ ۵۶۱۰
---
# غزل شمارهٔ ۵۶۱۰

<div class="b" id="bn1"><div class="m1"><p>رفت آن روز که دامان بهار از دستم</p></div>
<div class="m2"><p>رفت چون برگ خزان دیده قرار از دستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه چون موج ز دریا به کنار افتادم</p></div>
<div class="m2"><p>لله الحمد نرفته است کنار از دستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به سبکدستی من نیست کس از جانبازان</p></div>
<div class="m2"><p>می جهد خرده جان همچو شرار از دستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می برد دست و دل از کار غزالش، ترسم</p></div>
<div class="m2"><p>که به یکبار رود دام و شکار از دستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از رگ ابر قلم بس که فشاندم گوهر</p></div>
<div class="m2"><p>چون صدف شد دل بحر آبله دار از دستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خون ناحق شوم آنجا که فتد بر سر، کار</p></div>
<div class="m2"><p>رفته هر چند که گیرایی کار از دستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>توتیا می شود آیینه ز جان سختی من</p></div>
<div class="m2"><p>سنگ بر سینه زند آینه دار از دستم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا بر آن چاک گریبان نظر افتاد مرا</p></div>
<div class="m2"><p>رفت سر رشته آرام و قرار از دستم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب از زخم ندامت جگرم شد صد چاک</p></div>
<div class="m2"><p>سینه موری اگر گشت فگار از دستم</p></div></div>