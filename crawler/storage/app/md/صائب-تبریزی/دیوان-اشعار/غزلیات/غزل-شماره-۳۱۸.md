---
title: >-
    غزل شمارهٔ ۳۱۸
---
# غزل شمارهٔ ۳۱۸

<div class="b" id="bn1"><div class="m1"><p>فقیری پیشه کن، از اغنیا حاجت مخواه اینجا</p></div>
<div class="m2"><p>که از درویش، همت می کند دریوزه شاه اینجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برآ زین خاکدان گر گوشه آسودگی خواهی</p></div>
<div class="m2"><p>که چون صحرای محشر نیست امید پناه اینجا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز پستی می توان دریافت معراج بلندی را</p></div>
<div class="m2"><p>سرافراز از شکستن می شود طرف کلاه اینجا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دیوان قیامت چون شود حاضر گرانجانی</p></div>
<div class="m2"><p>که نتواند قدم برداشت از بار گناه اینجا؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به خون انداختم از حرص نان خود، ندانستم</p></div>
<div class="m2"><p>کز اکسیر قناعت مشک می گردد گیاه اینجا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز راه جذبه توفیق، سالک می شود واصل</p></div>
<div class="m2"><p>به بال کهربا پرواز گیرد برگ کاه اینجا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گزیر از سرمه نبود دیده آهو نگاهان را</p></div>
<div class="m2"><p>مکن گر اهل دیدی، شکوه از بخت سیاه اینجا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درین عبرت سرا مگشا نظر زنهار بی عبرت</p></div>
<div class="m2"><p>که می گردد ز گوهر قیمتی تار نگاه اینجا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جهان چون کاروان ریگ دارد نعل در آتش</p></div>
<div class="m2"><p>مکن چون غافلان ریگ روان را تکیه گاه اینجا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سر از یک جیب با خورشید بیرون آوری صائب</p></div>
<div class="m2"><p>ز صدق دل برآری گر نفس چون صبحگاه اینجا</p></div></div>