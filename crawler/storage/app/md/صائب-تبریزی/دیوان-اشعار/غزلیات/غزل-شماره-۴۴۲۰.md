---
title: >-
    غزل شمارهٔ ۴۴۲۰
---
# غزل شمارهٔ ۴۴۲۰

<div class="b" id="bn1"><div class="m1"><p>در زیر فلک چند خردمند توان بود</p></div>
<div class="m2"><p>هشیار درین غمکده تا چند توان بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در فصل گل از بلبل ما یاد نکردند</p></div>
<div class="m2"><p>دیگر به چه امید درین بند توان بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کامی به مراد دل خود برنگرفتیم</p></div>
<div class="m2"><p>چون خامه به فرمان سخن چند توان بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر دامن عشق از هوس خام بود پاک</p></div>
<div class="m2"><p>خرسند ز معشوق به فرزند توان بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از چشمه حیوان نتوان خشک گذشتن</p></div>
<div class="m2"><p>در میکده تا چند خردمند توان بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر حاصل ایام اگر دست فشانی</p></div>
<div class="m2"><p>چون سرو سبکبار ز پیوند توان بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیوانه مارا نخریدند به سنگی</p></div>
<div class="m2"><p>در کوچه این سنگدلان چند توان بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر چند ز شکر نتوان کرد به نی صلح</p></div>
<div class="m2"><p>با وعده بی مغز تو خرسند توان بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از بوسه به پیغام تسلی نتوان شد</p></div>
<div class="m2"><p>قانع به نی خشک کی از قند توان بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون شمع که سرسبزیش از دیده خویش است</p></div>
<div class="m2"><p>از گریه خود چند برومند توان بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب به سخن چند ازین آینه رویان</p></div>
<div class="m2"><p>چون طوطی بی حوصله خرسند توان بود</p></div></div>