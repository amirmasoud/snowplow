---
title: >-
    غزل شمارهٔ ۲۱۷۳
---
# غزل شمارهٔ ۲۱۷۳

<div class="b" id="bn1"><div class="m1"><p>در عالم بالاست تماشایی اگر هست</p></div>
<div class="m2"><p>بیرون ز مکان است و زمان جایی اگر هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چیزی که بجا مانده همین ترک تمناست</p></div>
<div class="m2"><p>در خاطر عشاق تمنایی اگر هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در غیبت خلق است اگر هست حضوری</p></div>
<div class="m2"><p>در ترک تماشاست تماشایی اگر هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اشکی است که در ماتم امید فشانند</p></div>
<div class="m2"><p>در روی زمین آب گوارایی اگر هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آهی است که از سینه افسوس برآید</p></div>
<div class="m2"><p>در باغ جهان نخل تمنایی اگر هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از ساده دلی چون گذری عالم مستی است</p></div>
<div class="m2"><p>در زیر فلک دامن صحرایی اگر هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر گرد جهان دور زدن بر تو حلال است</p></div>
<div class="m2"><p>خورشید صفت دیده بینایی اگر هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در آینه تار، پری دیو نمایند</p></div>
<div class="m2"><p>صاف است جهان جان مصفایی اگر هست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر طوطی جان، تلخی غربت ننماید</p></div>
<div class="m2"><p>در خانه دل آینه سیمایی اگر هست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گردست فشاندن به دو عالم نتوانی</p></div>
<div class="m2"><p>در دامن عزلت بشکن پایی اگر هست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زنهار که غافل مشو از خامه نقاش</p></div>
<div class="m2"><p>در مد نظر صورت زیبایی اگر هست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صائب دل پر خون بود و دیده خونبار</p></div>
<div class="m2"><p>در مجلس ما ساغر و مینایی اگر هست</p></div></div>