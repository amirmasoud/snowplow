---
title: >-
    غزل شمارهٔ ۱۹۰۳
---
# غزل شمارهٔ ۱۹۰۳

<div class="b" id="bn1"><div class="m1"><p>صبح امید من نفس سرد من بس است</p></div>
<div class="m2"><p>چشم سفید، روزن بیت الحزن بس است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دستم غبار دامن پاکان نمی شود</p></div>
<div class="m2"><p>بویی مرا ز یوسف گل پیرهن بس است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تر می شود به نامه خشکی دماغ من</p></div>
<div class="m2"><p>برگ خزان رسیده مرا از چمن بس است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عنوان بود نمکچش مکتوب سر به مهر</p></div>
<div class="m2"><p>زان غنچه لب وظیفه من یک سخن بس است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان میوه ها که وعده به فردوس داده اند</p></div>
<div class="m2"><p>ما را به نقد، نکهت سیب ذقن بس است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پروانه وار سوختن از بی مروتی است</p></div>
<div class="m2"><p>آن را که روی گرمی ازین انجمن بس است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از شغل دلخراش تو بدنام گشت عشق</p></div>
<div class="m2"><p>نقشی دگر بر آب زن ای کوهکن، بس است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>محتاج نیستم به سپرداری کسی</p></div>
<div class="m2"><p>جوهر دعای جوشن شمشیر من بس است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب ز بلبلان نشود گر صدا بلند</p></div>
<div class="m2"><p>کلک سخن طراز هم آواز من بس است</p></div></div>