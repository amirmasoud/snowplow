---
title: >-
    غزل شمارهٔ ۴۷۹۰
---
# غزل شمارهٔ ۴۷۹۰

<div class="b" id="bn1"><div class="m1"><p>بگیر جام هلالی ز رخ نقاب انداز</p></div>
<div class="m2"><p>ز رعشه سنگ به مینای آفتاب انداز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فریب حسن سبکسیر رنگ و بوی مخور</p></div>
<div class="m2"><p>محبت گل این باغ بر گلاب انداز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کمند جاذبه گوهرست بیتابی</p></div>
<div class="m2"><p>همین تو رشته جان را به پیچ و تاب انداز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میی که پشت ندارد نخوردنش اولاست</p></div>
<div class="m2"><p>به پای خم بنشین در قدح شراب انداز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشو چو قطره شبنم گره درین گلزار</p></div>
<div class="m2"><p>سربریدن به دامان آفتاب انداز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترا چه کار که این خاک پاک و آن شورست</p></div>
<div class="m2"><p>به هر زمین که رسی تخم چون سحاب انداز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر قبول نداری که بی تو چون داغیم</p></div>
<div class="m2"><p>بیا به سینه سوزان ما کباب انداز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نصیب کوزه سر بسته است باده ناب</p></div>
<div class="m2"><p>نهفته چشم برآن چشم نیمخواب انداز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جواب آن غزل است این که خواجه حافظ گفت</p></div>
<div class="m2"><p>مرا به میکده بر در خم شراب انداز</p></div></div>