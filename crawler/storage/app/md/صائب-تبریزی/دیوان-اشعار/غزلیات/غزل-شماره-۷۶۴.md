---
title: >-
    غزل شمارهٔ ۷۶۴
---
# غزل شمارهٔ ۷۶۴

<div class="b" id="bn1"><div class="m1"><p>دایم شکفته است دل داغدار ما</p></div>
<div class="m2"><p>موقوف وقت نیست چو عنبر بهار ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فارغ ز کعبه ایم و ز بتخانه بی نیاز</p></div>
<div class="m2"><p>خاک مراد ماست دل خاکسار ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آتش به پرده سوزی اسرار عشق نیست</p></div>
<div class="m2"><p>رحم است بر دلی که شود رازدار ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صد پیرهن ز دامن صبح است پاکتر</p></div>
<div class="m2"><p>دامان گل ز شبنم شب زنده دار ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوش داشتیم وقت حریفان بزم را</p></div>
<div class="m2"><p>چون می، گذشت اگر چه به تلخی مدار ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد پاره ای ز تن، دل ما از فسردگی</p></div>
<div class="m2"><p>خامی به رنگ برگ برآورد بار ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در روزگار ما دل بی درد و داغ نیست</p></div>
<div class="m2"><p>در سنگ همچو سوخته گیرد شرار ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از صدق، هر دو دست به دامان شب زدیم</p></div>
<div class="m2"><p>تا همچو صبح، تنگ شکر شد کنار ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوهر حریف گرد یتیمی نمی شود</p></div>
<div class="m2"><p>نتوان فشاند دامن ناز از غبار ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صائب چو خضر روی خزان فنا ندید</p></div>
<div class="m2"><p>شد سبز هر که از سخن آبدار ما</p></div></div>