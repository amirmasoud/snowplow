---
title: >-
    غزل شمارهٔ ۶۲۱۶
---
# غزل شمارهٔ ۶۲۱۶

<div class="b" id="bn1"><div class="m1"><p>زهی از شبنم رخساره ات چشم حیا روشن</p></div>
<div class="m2"><p>چراغ ماه را از شمع رویت پیش پا روشن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر من از غبار خاطر خود پرده بردارم</p></div>
<div class="m2"><p>نگردد تا قیامت آب این نه آسیا روشن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمی یابد مسیح از ناتوانی جسم زارم را</p></div>
<div class="m2"><p>خوشا کاهی که ضعف او شود بر کهربا روشن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به داغ منت و درد ندامت برنمی آیی</p></div>
<div class="m2"><p>مکن از خانه همسایه هرگز شمع را روشن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نسیم صبح چون پروانه افتاده است در پایش</p></div>
<div class="m2"><p>چراغی را که سازد پرتو لطف خدا روشن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فلک با تنگ چشمان گوشه چشم دگر دارد</p></div>
<div class="m2"><p>که چون فرزند کور آید، شود چشم گدا روشن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز فیض روح سید نعمت الله است این صائب</p></div>
<div class="m2"><p>اگر نه روی او بودی، نگشتی چشم ما روشن</p></div></div>