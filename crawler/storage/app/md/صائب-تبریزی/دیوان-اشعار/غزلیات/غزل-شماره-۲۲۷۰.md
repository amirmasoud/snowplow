---
title: >-
    غزل شمارهٔ ۲۲۷۰
---
# غزل شمارهٔ ۲۲۷۰

<div class="b" id="bn1"><div class="m1"><p>به داغ عشق نباشد مرا جگر محتاج</p></div>
<div class="m2"><p>به آفتاب ز خامی بود ثمر محتاج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ببر به جای دگر روی گرم خود خورشید!</p></div>
<div class="m2"><p>که نیست سوخته ما به این شرر محتاج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس است چهره زرین، خزانه عاشق</p></div>
<div class="m2"><p>که آفتاب نباشد به سیم و زر محتاج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزار شکر که این غنچه خود به خود وا شد</p></div>
<div class="m2"><p>نشد چو گل به هواداری سحر محتاج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازان زمان هک به دولتسرای فقر رسید</p></div>
<div class="m2"><p>دگر نگشت دل ما به هیچ در محتاج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مجوی بیش ز قسمت که تا قناعت کرد</p></div>
<div class="m2"><p>برای آب به دریا نشد گهر محتاج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکسته می شود از احتیاج، شاخ غرور</p></div>
<div class="m2"><p>ازان شدند خلایق به یکدگر محتاج</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهشت را دل ما در نظر نمی آورد</p></div>
<div class="m2"><p>نمود عشق تو ما را به یک نظر محتاج</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در آن مقام که ماییم، شوق تا حدی است</p></div>
<div class="m2"><p>که هیچ نامه نگردد به نامه بر محتاج</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر میان دو دل هست دوستی به قرار</p></div>
<div class="m2"><p>نمی شوند به آمد شد خبر محتاج</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کجا ز سوزش پروانه بو تواند برد؟</p></div>
<div class="m2"><p>سمندری که نگردد به بال و پر محتاج</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کجا ز سوزش پروانه بود تواند برد؟</p></div>
<div class="m2"><p>سمندری که نگردد به بال و پر محتاج</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همان به آبله خویشتن قناعت کرد</p></div>
<div class="m2"><p>اگر به آب شد این آتشین جگر محتاج</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>میان گشوده سرانجام خواب می گیری</p></div>
<div class="m2"><p>در آن طریق که نی شد به صد کمر محتاج</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به راه کعبه مقصد، تپیدن دل ماست</p></div>
<div class="m2"><p>سبکروی که نگردد به راهبر محتاج</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>طمع دلیل فرومایگی است کاهل را</p></div>
<div class="m2"><p>وگرنه نیست به تحسین کس هنر محتاج</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دل شکسته ما تا چه کفر نعمت کرد؟</p></div>
<div class="m2"><p>که شد به مرهم این ناکسان دگر محتاج</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ازان همیشه در فیض باز می باشد</p></div>
<div class="m2"><p>که روی خویش نیارد به هیچ در محتاج</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خوشیم با سفر دور بیخودی صائب</p></div>
<div class="m2"><p>که نیستیم به همراه و همسفر محتاج</p></div></div>