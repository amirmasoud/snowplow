---
title: >-
    غزل شمارهٔ ۵۴۰۷
---
# غزل شمارهٔ ۵۴۰۷

<div class="b" id="bn1"><div class="m1"><p>برق آهی کو که رو در خرمن گردون کنم</p></div>
<div class="m2"><p>این گره را باز از پیشانی هامون کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان خوشم با دامن صحرا که از چشم غزل</p></div>
<div class="m2"><p>حلقه ای هر لحظه بر زنجیر خود افزون کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من گرفتم رام گردیدند با من آهوان</p></div>
<div class="m2"><p>بر خمار سنگ طفلان صبر یارب چون کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>موی جوهر از خمیر آیینه را نتوان کشید</p></div>
<div class="m2"><p>خارخار عشق را از سینه چون بیرون کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از سواد شهر خاکستر نشین شد اخگرم</p></div>
<div class="m2"><p>تربیت این شعله را از دامن هامون کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون کنم در خدمت پیر مغان گردنکشی</p></div>
<div class="m2"><p>من که خم را از ادب تعظیم افلاطون کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از دهان یار دارد چاشنی گفتار من</p></div>
<div class="m2"><p>خامه ها را بی شق از شیرینی مضمون کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاهد خامی است جوش باده در آغوش خم</p></div>
<div class="m2"><p>حاش الله شکوه از ناسازی گردون کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از صفای سینه ام چشم جهان آورد آب</p></div>
<div class="m2"><p>آه اگر آیینه دل از بغل بیرون کنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از مروت نیست خوردن بر دل ازاد سرو</p></div>
<div class="m2"><p>ورنه من هم می توانم مصرعی موزون کنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون به بیدردان کنم تکلیف صائب جام خویش</p></div>
<div class="m2"><p>من که خونها می خورم تا ساغری پر خون کنم</p></div></div>