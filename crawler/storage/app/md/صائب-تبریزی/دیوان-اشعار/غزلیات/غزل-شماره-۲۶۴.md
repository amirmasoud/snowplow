---
title: >-
    غزل شمارهٔ ۲۶۴
---
# غزل شمارهٔ ۲۶۴

<div class="b" id="bn1"><div class="m1"><p>نه ز خامی نقش ها را خام می بندیم ما</p></div>
<div class="m2"><p>پرده بر چشم بد ایام می بندیم ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده خونخوار ما را نیست سیری از شکار</p></div>
<div class="m2"><p>خاکساری را به خود چون دام می بندیم ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فیض بالادست مینا را طلب در کار نیست</p></div>
<div class="m2"><p>چون لب ساغر، لب از ابرام می بندیم ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می شود همچون فلاخن شهپر پرواز ما</p></div>
<div class="m2"><p>سنگ اگر بر جان بی آرام می بندیم ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مطلب ما بی دلان از چشم بستن خواب نیست</p></div>
<div class="m2"><p>در به روی آرزوی خام می بندیم ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر چه زخم صبح از خورشید می گردد زیاد</p></div>
<div class="m2"><p>رخنه خمیازه را از جام می بندیم ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در به روی گفتگو، هر چند باشد دلپذیر</p></div>
<div class="m2"><p>با زبان چرب چون بادام می بندیم ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در ره افتادگی از ما کسی در پیش نیست</p></div>
<div class="m2"><p>نقش بر روی زمین هر گام می بندیم ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تیغ را دندانه می سازد سپر انداختن</p></div>
<div class="m2"><p>از دعا دایم راه دشنام می بندیم ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بستگی کفرست در آیین ما آزادگان</p></div>
<div class="m2"><p>می شود زنار اگر احرام می بندیم ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیست صائب چون شرر ما را به جان دلبستگی</p></div>
<div class="m2"><p>چشم در آغاز از انجام می بندیم ما</p></div></div>