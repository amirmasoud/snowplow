---
title: >-
    غزل شمارهٔ ۵۳۱۲
---
# غزل شمارهٔ ۵۳۱۲

<div class="b" id="bn1"><div class="m1"><p>شد گل صد برگ خار از اشک خوش پرگاله ام</p></div>
<div class="m2"><p>سبزه خوابیده در گلشن نماند از ناله ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گردد از سرگشتگی دوران عیش من تمام</p></div>
<div class="m2"><p>در بساط آفرینش شعله جواله ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد غبارم سرمه چشم غزالان و هنوز</p></div>
<div class="m2"><p>چشم لیلی بر نمی دارد سراز دنباله ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک سر ناخن ز خار این چمن ممنون نیم</p></div>
<div class="m2"><p>تازه رو از خون خود دایم چو داغ لاله ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با سبکروحان گرانی کردن از انصاف نیست</p></div>
<div class="m2"><p>جلوه شبنم کند بر چهره گل ژاله ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوهر سیراب را عین الکمالی لازم است</p></div>
<div class="m2"><p>نیست از سوز جگر برگرد لب تبخاله ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه دایم در کنارم بود آن ماه تمام</p></div>
<div class="m2"><p>رفت در خمیازه آغوش عمر هاله ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در گلستانی که من صائب نواسنجی کنم</p></div>
<div class="m2"><p>گوش گل چون لاله گردد داغدار از ناله ام</p></div></div>