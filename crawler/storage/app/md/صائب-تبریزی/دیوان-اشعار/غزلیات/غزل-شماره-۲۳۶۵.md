---
title: >-
    غزل شمارهٔ ۲۳۶۵
---
# غزل شمارهٔ ۲۳۶۵

<div class="b" id="bn1"><div class="m1"><p>سبزه خط دود از آن رخسار آتشناک کرد</p></div>
<div class="m2"><p>دیده آیینه را جوهر پر از خاشاک کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرنوشت جوهر از آیینه خواندن مشکل است</p></div>
<div class="m2"><p>آن خط نازک رقم را چون توان ادراک کرد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر برآورد از زمین در عهد ما بی حاصلان</p></div>
<div class="m2"><p>تخم قارونی که موسی پیش ازین در خاک کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ابر رحمت در دهانش گوهر شهوار ریخت</p></div>
<div class="m2"><p>چون صدف هر کس درین دریا دهن را پاک کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون نریزد از زبان ما صفیر دلخراش؟</p></div>
<div class="m2"><p>چون قلم درد سخن ما را گریبان چاک کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مزرع بی حاصل من داغ دارد برق را</p></div>
<div class="m2"><p>کهربایی می تواند خرمنم را پاک کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون نترسد دیده من از غبار خط او؟</p></div>
<div class="m2"><p>زلف صیادش در اینجا دام را در خاک کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرچه صائب می چکد آب گهر از کلک من</p></div>
<div class="m2"><p>دام بتوان در غبار خاطرم در خاک کرد</p></div></div>