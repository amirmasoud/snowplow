---
title: >-
    غزل شمارهٔ ۲۹۰۲
---
# غزل شمارهٔ ۲۹۰۲

<div class="b" id="bn1"><div class="m1"><p>کسی تاب خدنگ غمزه آن دلربا دارد</p></div>
<div class="m2"><p>که چون آیینه از جوهر زره زیرقبا دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آن وادی که من از تشنگی بر خاک می غلطم</p></div>
<div class="m2"><p>سراب ناامیدی جلوه آن بقا دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به اندک روزگاری تاک شد از سرو رعناتر</p></div>
<div class="m2"><p>نگردد زیر دست آن کس که دستی در سخا دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندیدم یک نفس راحت زحس ظاهر و باطن</p></div>
<div class="m2"><p>چه آسایش در آن کشور که ده فرمانروا دارد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من آن آتش نو امر غم که چون از یکدگر ریزم</p></div>
<div class="m2"><p>زگرمی استخوانم شمع در راه هما دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا بیطاقتی محروم دارد از وصال او</p></div>
<div class="m2"><p>که از آتش شرر را اضطراب دل جدا دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فریب دولت ده روزه دنیا مخور صائب</p></div>
<div class="m2"><p>که آخر بدورق گرداندنی بال هما دارد</p></div></div>