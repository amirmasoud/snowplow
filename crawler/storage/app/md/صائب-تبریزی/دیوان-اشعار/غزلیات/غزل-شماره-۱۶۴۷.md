---
title: >-
    غزل شمارهٔ ۱۶۴۷
---
# غزل شمارهٔ ۱۶۴۷

<div class="b" id="bn1"><div class="m1"><p>ز شرم در حرم وصل جان محرم سوخت</p></div>
<div class="m2"><p>فغان که تشنه ما در کنار زمزم سوخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گذشت پرتو روی تو بر بساط چمن</p></div>
<div class="m2"><p>عقیق لاله و گل در دهان شبنم سوخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس است سوختن خارزار تهمت را</p></div>
<div class="m2"><p>به نور چهره چراغی که شرم مریم سوخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز حد گذشت چو باران، ز برق کمتر نیست</p></div>
<div class="m2"><p>بهار و باغ من از گریه دمادم سوخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز چرب نرمی بدباطنان ز راه مرو</p></div>
<div class="m2"><p>که داغهای من از چشم نرم مرهم سوخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز انقلاب جهان زینهار امن مباش</p></div>
<div class="m2"><p>که شمع سور مکرر برای ماتم سوخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل گرفته ما را به حال خود بگذار</p></div>
<div class="m2"><p>که در گشودن این غنچه صبح را دم سوخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز چشم خیره تردامنان مشو ایمن</p></div>
<div class="m2"><p>که گل به آتش سوزان ز چشم شبنم سوخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همان ز خنده بیجا به مرگ خویش نشست</p></div>
<div class="m2"><p>اگر چه برق فنا خانمان عالم سوخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همان چراغ مرا نیست روشنی صائب</p></div>
<div class="m2"><p>اگر چه از نفس گرم من دو عالم سوخت</p></div></div>