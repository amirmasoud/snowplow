---
title: >-
    غزل شمارهٔ ۳۶۹۸
---
# غزل شمارهٔ ۳۶۹۸

<div class="b" id="bn1"><div class="m1"><p>ز نوبهار کجا گل شکفته می گردد؟</p></div>
<div class="m2"><p>گل از ترانه بلبل شکفته می گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز زلف و عارض دلدار غافل افتاده است</p></div>
<div class="m2"><p>دلی که از گل و سنبل شکفته می گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی کز آبله افتاده در گره کارش</p></div>
<div class="m2"><p>ز خار راه تو گلگل شکفته می گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا دلی است درین بحر نیلگون چو حباب</p></div>
<div class="m2"><p>که از نسیم تزلزل شکفته می گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آه سرد چه پرواست لاله رویان را؟</p></div>
<div class="m2"><p>که از نسیم سحر گل شکفته می گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گره ز غنچه پیکان شود به خون گر باز</p></div>
<div class="m2"><p>گل گرفته هم از مل شکفته می گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلی که گرد کدورت نبرد ازو سیلاب</p></div>
<div class="m2"><p>کجا ز سیر سر پل شکفته می گردد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلی که داشت تغافل به التفات، امروز</p></div>
<div class="m2"><p>ز زخم تیغ تغافل شکفته می گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلی که تنگ گرفته است در میان حرصش</p></div>
<div class="m2"><p>کی از نسیم توکل شکفته می گردد؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به خاکساری من نیست هیچ کس صائب</p></div>
<div class="m2"><p>اگر فلک ز تحمل شکفته می گردد</p></div></div>