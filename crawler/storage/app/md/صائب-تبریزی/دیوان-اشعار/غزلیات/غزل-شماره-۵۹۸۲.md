---
title: >-
    غزل شمارهٔ ۵۹۸۲
---
# غزل شمارهٔ ۵۹۸۲

<div class="b" id="bn1"><div class="m1"><p>مبتلای آرزوی نفس را عاقل مخوان</p></div>
<div class="m2"><p>عنکبوت رشته طول امل را دل مخوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رهبری کز خویش نستاند ترا رهزن شمار</p></div>
<div class="m2"><p>منزلی کز خود فرو نارد ترا منزل مخوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساحل آن باشد که امنیت در او لنگر کند</p></div>
<div class="m2"><p>جای دست انداز موج بحر را ساحل مخوان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فیض عام حق به ذرات جهان تابیده است</p></div>
<div class="m2"><p>هیچ نقشی را درین وحدت سرا باطل مخوان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشکل آن باشد که حل گردد در او فکر جهان</p></div>
<div class="m2"><p>مشکلی کز فکر حل آن شود مشکل مخوان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچ عیبی خاکیان را همچو کشف راز نیست</p></div>
<div class="m2"><p>از زمین ها جز زمین شور را قابل مخوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کاملی کز ناقصان بی بصیرت خویش را</p></div>
<div class="m2"><p>کم نداند در کمال معرفت، کامل مخوان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عیب خود نایافتن بالاترین عیبهاست</p></div>
<div class="m2"><p>جاهلان منفعل از جهل را جاهل مخوان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواجه ای کآزاد نبود از دو عالم، خواجه نیست</p></div>
<div class="m2"><p>بنده ای کز خویش نگریزد ورا مقبل مخوان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آب و رنگ چهره محفل شراب و شاهدست</p></div>
<div class="m2"><p>محفلی کز حسن و می خالی بود محفل مخوان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شورش عشق است دلها را نشان زندگی</p></div>
<div class="m2"><p>هر دلی کز عشق خالی گشت صائب دل مخوان</p></div></div>