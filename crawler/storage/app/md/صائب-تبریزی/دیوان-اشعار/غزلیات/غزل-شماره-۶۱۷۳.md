---
title: >-
    غزل شمارهٔ ۶۱۷۳
---
# غزل شمارهٔ ۶۱۷۳

<div class="b" id="bn1"><div class="m1"><p>ای لب لعل ترا خون یمن در آستین</p></div>
<div class="m2"><p>هر سر موی ترا چین و ختن در آستین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه دلگیرست چون شام غریبان طره اش</p></div>
<div class="m2"><p>دارد از رخسار او صبح وطن در آستین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غیرت عشق زلیخا بود مانع، ورنه داشت</p></div>
<div class="m2"><p>بوی یوسف ساکن بیت الحزن در آستین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در گلستانی که من گریان در آیم، غنچه ها</p></div>
<div class="m2"><p>خنده را پنهان کنند از شرمن من در آستین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دامن فانوس آن وسعت ندارد، ورنه من</p></div>
<div class="m2"><p>گریه ها دارم چو شمع انجمن در آستین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر به دست افتد شکستی، می کنم در کار دل</p></div>
<div class="m2"><p>من نه زانهایم که اندازم شکن در آستین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رشک مانع بود، ورنه تیشه من نیز داشت</p></div>
<div class="m2"><p>نقش های دلربا چون کوهکن در آستین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اعتمادی نیست بر عمر سبکسیر بهار</p></div>
<div class="m2"><p>از شکوفه شاخ ازان دارد کفن در آستین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی محرک نیست ممکن حرفی از من سر زند</p></div>
<div class="m2"><p>گر چه دارم چون قلم چندین سخن در آستین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر چه صائب ظاهر ما چون قلم بی حاصل است</p></div>
<div class="m2"><p>شکرستانهاست ما را از سخن در آستین</p></div></div>