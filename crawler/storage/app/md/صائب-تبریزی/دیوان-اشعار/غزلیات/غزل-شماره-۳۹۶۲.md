---
title: >-
    غزل شمارهٔ ۳۹۶۲
---
# غزل شمارهٔ ۳۹۶۲

<div class="b" id="bn1"><div class="m1"><p>اگر چه روی من از درد زعفرانی بود</p></div>
<div class="m2"><p>خمیرمایه صد رنگ شادمانی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز خشک مغزی پیری مرا یقین گردید</p></div>
<div class="m2"><p>که در سیاهی مو آب زندگانی بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فغان که جامه فانوس شمع هستی من</p></div>
<div class="m2"><p>ز روزگار همین آستیم فشانی بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سخن گسسته عنان راه حرف خارستان</p></div>
<div class="m2"><p>مدار زندگانی من به پاسبانی بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تمتعی که ازین خاکدان رسید به من</p></div>
<div class="m2"><p>سبک رکابتر از گرد کاروانی بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فتاد از نظرم تا ز خون تهی شد دل</p></div>
<div class="m2"><p>سبوی باده سبکروح از گرانی بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جرم هرزه درایی گداختند مرا</p></div>
<div class="m2"><p>زبان شکوه من گرچه بی زبانی بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من آن نیم که به نیرنگ دل دهم به کسی</p></div>
<div class="m2"><p>بلای چشم کبود تو آسمانی بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به بوسه ای نزدی مهر برلبم هرگز</p></div>
<div class="m2"><p>همیشه لطف تو با دوستان زبانی بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زپرده شعله دیدار کار خود می کرد</p></div>
<div class="m2"><p>جواب موسی ما گرچه لن ترانی بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ازان به تیغ زبان شد جهان ستان صائب</p></div>
<div class="m2"><p>که مدح گستر عباس شاه ثانی بود</p></div></div>