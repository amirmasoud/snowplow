---
title: >-
    غزل شمارهٔ ۵۷۲
---
# غزل شمارهٔ ۵۷۲

<div class="b" id="bn1"><div class="m1"><p>علم نصرت ما آه سحرگاهی ما</p></div>
<div class="m2"><p>مهر خاموشی ما چتر شهنشاهی ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما ز بی برگ و نوایی خط پاکی داریم</p></div>
<div class="m2"><p>چه کند باد خزانی به رخ کاهی ما؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چرخ چندان که زند نقش حوادث بر آب</p></div>
<div class="m2"><p>می شود جوهر آیینه آگاهی ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه توقع ز رفیقان دگر باید داشت؟</p></div>
<div class="m2"><p>سایه جایی که کشد پای ز همراهی ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفته بودیم که از وادی دل دور شویم</p></div>
<div class="m2"><p>نفس سوخته شد سرمه آگاهی ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر سر خار درین دشت چراغی گردید</p></div>
<div class="m2"><p>پای برجاست همان ظلمت گمراهی ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفت عمر و قدم از خود ننهادیم برون</p></div>
<div class="m2"><p>داد از غفلت ما، آه ز کوتاهی ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچنان خار به دل از رگ خامی داریم</p></div>
<div class="m2"><p>فلس اگر داغ شود بر بدن ماهی ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست در دامن این دشت، شکاری صائب</p></div>
<div class="m2"><p>که علم چرب کند آه سحرگاهی ما</p></div></div>