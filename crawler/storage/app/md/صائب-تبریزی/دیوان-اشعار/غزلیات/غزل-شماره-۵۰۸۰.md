---
title: >-
    غزل شمارهٔ ۵۰۸۰
---
# غزل شمارهٔ ۵۰۸۰

<div class="b" id="bn1"><div class="m1"><p>آن لاله عذاری که منم داغ و کبابش</p></div>
<div class="m2"><p>از خون جگر سوختگان است شرابش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیهوشیش از کاوش دل باز ندارد</p></div>
<div class="m2"><p>چشمی که بود شوخ چو مژگان رگ خوابش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این لنگر تمکین که به خود حسن سپرده است</p></div>
<div class="m2"><p>مشکل که کند حلقه خط پا به رکابش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خانه به بازار صبوحی زده آید</p></div>
<div class="m2"><p>حسنی که زآیینه بود عالم آبش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمی که شود صیقلی باده روشن</p></div>
<div class="m2"><p>از خشت سر خم بود آماده کتابش</p></div></div>