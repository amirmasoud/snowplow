---
title: >-
    غزل شمارهٔ ۱۶۰۶
---
# غزل شمارهٔ ۱۶۰۶

<div class="b" id="bn1"><div class="m1"><p>خسته چشم تو صاحب نظری نیست که نیست</p></div>
<div class="m2"><p>تشنه لعل تو روشن گهری نیست که نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این چه شورست که حسن تو به عالم افکند؟</p></div>
<div class="m2"><p>که نمکدان ملاحت جگری نیست که نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بخیه شبنم و گل بر رخ کار افتاده است</p></div>
<div class="m2"><p>ورنه حیران تو صاحب نظری نیست که نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه همین ذره درین دایره سرگردان است</p></div>
<div class="m2"><p>رقص سودای تو در هیچ سری نیست که نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عالم از حسن گلوسوز تو شد باغ خلیل</p></div>
<div class="m2"><p>در دل سنگ تو تخم شرری نیست که نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میوه سرو که گفته است همین آزادی است؟</p></div>
<div class="m2"><p>قامت سرکش او را ثمری نیست که نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه همین لاله و گل نعل در آتش دارند</p></div>
<div class="m2"><p>خار خار تو نهان در جگری نیست که نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فتنه هر دو جهان زیر سر خشت خم است</p></div>
<div class="m2"><p>در خرابات مغان شور و شری نیست که نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نظر پست تو شایسته جولان کف است</p></div>
<div class="m2"><p>ورنه در سینه دریا گهری نیست که نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون کنم نسبت آن لعل به یاقوت عقیم؟</p></div>
<div class="m2"><p>رو سفید از نمک او جگری نیست که نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برو ای عقل، به صحرای جنون پا مگذار</p></div>
<div class="m2"><p>شیشه باری تو و اینجا خطری نیست که نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زهر دشنام بود قسمت عاشق، ورنه</p></div>
<div class="m2"><p>در نهانخانه آن لب، شکری نیست که نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بعد ازین نامه مگر بر پر عنقا بندیم</p></div>
<div class="m2"><p>ورنه با نامه ما بال و پری نیست که نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نه همین دیده شبنم ز نظربازان است</p></div>
<div class="m2"><p>محو خورشید تو صاحب نظری نیست که نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر چه از بیخبرانیم به ظاهر صائب</p></div>
<div class="m2"><p>در فرامشکده ما خبری نیست که نیست</p></div></div>