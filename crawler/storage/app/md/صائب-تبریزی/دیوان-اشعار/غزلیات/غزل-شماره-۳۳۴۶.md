---
title: >-
    غزل شمارهٔ ۳۳۴۶
---
# غزل شمارهٔ ۳۳۴۶

<div class="b" id="bn1"><div class="m1"><p>خویش را پیشتر از مرگ خبر باید کرد</p></div>
<div class="m2"><p>در حضر فکر سرانجام سفر باید کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش ازان دم که شود تکمه پیراهن خاک</p></div>
<div class="m2"><p>سر ازین خرقه نه توی بدر باید کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حاصل کار جهان غیر پشیمانی نیست</p></div>
<div class="m2"><p>فکر شغل دگر و کار دگر باید کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفسی چند که در سینه پرخون باقی است</p></div>
<div class="m2"><p>صرف افغان شب و آه سحر باید کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیشتر زان که شود کشتی تن پا به رکاب</p></div>
<div class="m2"><p>کشتی فکر درین بحر خطر باید کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیر انجام در آیینه آغاز خوش است</p></div>
<div class="m2"><p>دام را پیشتر از دانه نظر باید کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا مگر اختر توفیق فروزان گردد</p></div>
<div class="m2"><p>گریه ای چند به هر شام و سحر باید کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیش ازان دم که زمین دوز کند خار اجل</p></div>
<div class="m2"><p>دامن سعی، میان بند کمر باید کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا گل ابری از ایام بهاران باقی است</p></div>
<div class="m2"><p>صدف خویش لبالب ز گهر باید کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به رفیقان گرانبار نپردازد شوق</p></div>
<div class="m2"><p>توشه این سفر از لخت جگر باید کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فکر جان در سفر عشق به خاطر بارست</p></div>
<div class="m2"><p>از گرانباری این راه حذر باید کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قسمت مردم بی برگ بود میوه خلد</p></div>
<div class="m2"><p>دهنی تلخ به امید ثمر باید کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نتوان راه عدم را به عصا طی کردن</p></div>
<div class="m2"><p>پا چو از کار شد اندیشه پر باید کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شارع قافله فیض بود رخنه دل</p></div>
<div class="m2"><p>چشم خود وقف بر این راهگذر باید کرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پرتو عاریتی نعل در آتش دارد</p></div>
<div class="m2"><p>شمع محراب ز رخسار چو زر باید کرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مادر خاک به فرزند نمی پردازد</p></div>
<div class="m2"><p>روی در منزل و مأوای پدر باید کرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یک جهت گر شده ای در سفر یکتایی</p></div>
<div class="m2"><p>صائب از هر دو جهان قطع نظر باید کرد</p></div></div>