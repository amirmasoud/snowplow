---
title: >-
    غزل شمارهٔ ۳۳۵۹
---
# غزل شمارهٔ ۳۳۵۹

<div class="b" id="bn1"><div class="m1"><p>تا حیا قطع نظر زان گل رخسار نکرد</p></div>
<div class="m2"><p>مور خط رخنه در آن لعل شکربار نکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دهن غنچه تصویر، تبسم زده شد</p></div>
<div class="m2"><p>دل ما نوبر یک خنده سرشار نکرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رفت چون آبله هر بدگهری دست بدست</p></div>
<div class="m2"><p>گوهر ما ز صدف روی به بازار نکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم بر شربت خون دو جهان دوخته است</p></div>
<div class="m2"><p>خویش را چشم تو بی واسطه بیمار نکرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یوسف ما به تهیدستی کنعان در ساخت</p></div>
<div class="m2"><p>جنس خود چون دگران کهنه به بازار نکرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکسی گوهر خود را به خریدار رساند</p></div>
<div class="m2"><p>صائب ماست که پروای خریدار نکرد</p></div></div>