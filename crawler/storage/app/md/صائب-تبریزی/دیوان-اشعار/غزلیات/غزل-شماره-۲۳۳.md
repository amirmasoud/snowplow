---
title: >-
    غزل شمارهٔ ۲۳۳
---
# غزل شمارهٔ ۲۳۳

<div class="b" id="bn1"><div class="m1"><p>هست یک نسبت به نیک و بد دل بی کینه را</p></div>
<div class="m2"><p>نیست صدر و آستانی خانه آیینه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راز عشق از دل تراوش می کند بی اختیار</p></div>
<div class="m2"><p>آب این گوهر به طوفان می دهد گنجینه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نسبت یکرنگی طوطی است باغ دلگشا</p></div>
<div class="m2"><p>نیست از زنگار در خاطر غبار آیینه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دامن پاک گهر از گرد تهمت فارغ است</p></div>
<div class="m2"><p>ابر اگر بر سینه دریا گذارد سینه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم خونخوار ترا خط کرد با من مهربان</p></div>
<div class="m2"><p>گر چه نتوان دوست کردن دشمن دیرینه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوشه چشمی اگر باشد ازان وحشی غزال</p></div>
<div class="m2"><p>سهل باشد نافه کردن خرقه پشمینه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برنمی دارد فشار قبر دست از دامنت</p></div>
<div class="m2"><p>تا ز روی دل نیفشانی غبار کینه را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر گرفت از خاک تا آیینه را عکس رخت</p></div>
<div class="m2"><p>آب خضر از دور می بوسد زمین آیینه را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می تواند کرد صائب روی عالم را به خود</p></div>
<div class="m2"><p>هر که چون آیینه سازد پاک، لوح سینه را</p></div></div>