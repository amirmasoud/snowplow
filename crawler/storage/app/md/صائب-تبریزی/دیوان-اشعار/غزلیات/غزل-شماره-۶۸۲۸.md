---
title: >-
    غزل شمارهٔ ۶۸۲۸
---
# غزل شمارهٔ ۶۸۲۸

<div class="b" id="bn1"><div class="m1"><p>می وصل تو به کم حوصله ها ارزانی</p></div>
<div class="m2"><p>نشائه خون جگر باد به ما ارزانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما تهیدستی خود را به دو عالم ندهیم</p></div>
<div class="m2"><p>نقد وصل تو به این مشت گدا ارزانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست ما کم شود از چاک گریبان خالی</p></div>
<div class="m2"><p>دست اغیار به آن بند قبا ارزانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همت ما نکشد منت یاری ز کسی</p></div>
<div class="m2"><p>بوی پیراهن یوسف به صبا ارزانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نمی شد ادبم بند زبان، می گفتم</p></div>
<div class="m2"><p>بوسه بر دست تو دادن به حنا ارزانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در چمن خانه گرفتن گل فارغبالی است</p></div>
<div class="m2"><p>چار دیوار قفس باد به ما ارزانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب آن گل نکند گوش اگر بر سخنت</p></div>
<div class="m2"><p>گلشن حسن مبادش به صفا ارزانی</p></div></div>