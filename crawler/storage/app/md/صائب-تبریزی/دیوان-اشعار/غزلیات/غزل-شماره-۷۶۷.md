---
title: >-
    غزل شمارهٔ ۷۶۷
---
# غزل شمارهٔ ۷۶۷

<div class="b" id="bn1"><div class="m1"><p>با اختیار حق چه بود اختیار ما؟</p></div>
<div class="m2"><p>با نور آفتاب چه باشد شرار ما؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای روشنان عالم بالا مدد کنید</p></div>
<div class="m2"><p>شاید ز قید سنگ برآید شرار ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از رنگ و بوی عاریه دامن کشیده ایم</p></div>
<div class="m2"><p>چون عنبرست از نفس خود بهار ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در تنگنای کوزه چه لازم بسر بریم؟</p></div>
<div class="m2"><p>دریا به خاک می تپد از انتظار ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چندین هزار خانه دل می رسد به آب</p></div>
<div class="m2"><p>تا از میان گرد برآید سوار ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در وصل و هجر کار دل ما تپیدن است</p></div>
<div class="m2"><p>دایم به یک قرار بود بی قرار ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دام و قفس نماند درین طرفه صیدگاه</p></div>
<div class="m2"><p>تا آرمیده شد دل وحشت شعار ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاقل به پای خویش به زندان نمی رود</p></div>
<div class="m2"><p>ای جسم، روز حشر مکش انتظار ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در ملک بی زوال رضا انقلاب نیست</p></div>
<div class="m2"><p>صائب یکی است فصل خزان و بهار ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این آن غزل که مولوی روم گفته است</p></div>
<div class="m2"><p>آمد بهار خرم و نامد بهار ما</p></div></div>