---
title: >-
    غزل شمارهٔ ۹۳۶
---
# غزل شمارهٔ ۹۳۶

<div class="b" id="bn1"><div class="m1"><p>هر که پیوندد به اهل حق ز مردان خداست</p></div>
<div class="m2"><p>آهن پیوسته با آهن ربا، آهن رباست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قدر روشندل فزون از خاکساری می شود</p></div>
<div class="m2"><p>بر گهر گرد یتیمی سایه بال هماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قهرمان عشق می باشد به عاشق مهربان</p></div>
<div class="m2"><p>کشتی غواص گوهر جو به دریا آشناست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از مآل شادمانی سربلندان غافلند</p></div>
<div class="m2"><p>اره این نخل سرکش خنده دندان نماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی دلان طفل مشرب زین سیاهی می رمند</p></div>
<div class="m2"><p>دیده بالغ نظر را خط مشکین توتیاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسن را بی پرده دیدن از ادب دورست دور</p></div>
<div class="m2"><p>دیده ما شرمگینان چون زره زیر قباست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه دست اهل دولت هست در ظاهر بلند</p></div>
<div class="m2"><p>دست ارباب دعا بالاترین دستهاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق در پیران بود چون طبل در زیر گلیم</p></div>
<div class="m2"><p>در جوانان عشق شورانگیز، عید و روستاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیده تن پروران آب سیاه آورده است</p></div>
<div class="m2"><p>ورنه شمشیر شهادت موجه آب بقاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از غبار دل، زبان آتشین گفتار من</p></div>
<div class="m2"><p>زنده زیر خاک صائب چون چراغ آسیاست</p></div></div>