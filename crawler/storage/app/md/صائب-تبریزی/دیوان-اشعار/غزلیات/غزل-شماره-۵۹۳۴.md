---
title: >-
    غزل شمارهٔ ۵۹۳۴
---
# غزل شمارهٔ ۵۹۳۴

<div class="b" id="bn1"><div class="m1"><p>ما فکر لباس و غم دستار نداریم</p></div>
<div class="m2"><p>اندیشه سامان چو سر دار نداریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر در ره آرایش ظاهر نتوان کرد</p></div>
<div class="m2"><p>چون گل سر آرایش دستار نداریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای سایه اقبال هما رو سر خود گیر</p></div>
<div class="m2"><p>ما شکوه ای از سایه دیوار نداریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر جا نبود سوخته ای رو ننماییم</p></div>
<div class="m2"><p>آیینه به پیش رخ زنگار نداریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داریم هوای سفر عالم بالا</p></div>
<div class="m2"><p>چون شبنم گل چشم به گلزار نداریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دنباله رو قبله نمای دل خویشیم</p></div>
<div class="m2"><p>چون کعبه روان روی به دیوار نداریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در جیب صدف گوهر ما چشم گشوده است</p></div>
<div class="m2"><p>ما طاقت رسوایی بازار نداریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد مخزن گوهر صدف از آبله دست</p></div>
<div class="m2"><p>ما جز گره دل ثمر از کار نداریم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگذار که در چاه مذلت بسر آریم</p></div>
<div class="m2"><p>ما حوصله ناز خریدار نداریم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما بیخبران قافله ریگ روانیم</p></div>
<div class="m2"><p>یک ذره ز سرگشتگی آزار نداریم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر چند که در گردن ما دست فکنده است</p></div>
<div class="m2"><p>از بیخبریها خبر از یار نداریم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صائب نفس شعله ما تشنه خارست</p></div>
<div class="m2"><p>با مردم کوتاه زبان کار نداریم</p></div></div>