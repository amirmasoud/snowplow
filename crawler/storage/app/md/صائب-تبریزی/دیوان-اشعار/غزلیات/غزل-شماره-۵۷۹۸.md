---
title: >-
    غزل شمارهٔ ۵۷۹۸
---
# غزل شمارهٔ ۵۷۹۸

<div class="b" id="bn1"><div class="m1"><p>با صد زبان چو غنچه گل بی زبان شدم</p></div>
<div class="m2"><p>تا پرده دار خرده راز نهان شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون ماه مصر، قیمت من خواست عذر من</p></div>
<div class="m2"><p>گر یک دو روز بار دل کاروان شدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خار راه من گل امید می دمد</p></div>
<div class="m2"><p>اکنون که همچو سیل به دریا روان شدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیلاب من کجا به محیط بقا رسد؟</p></div>
<div class="m2"><p>زینسان که از غبار علایق گران شدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>افتاد حرف من به زبان چون دهان یار</p></div>
<div class="m2"><p>هر چند بیشتر ز نظرها نهان شدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرگز شکوفه ام به ثمر بارور نشد</p></div>
<div class="m2"><p>چون صبح اگر چه پیر درین بوستان شدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون خار دلشکسته درین بوستانسرا</p></div>
<div class="m2"><p>شرمنده نسیم بهار و خزان شدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در موسمی که بال برآرد ز لاله سنگ</p></div>
<div class="m2"><p>چون بیضه پا شکسته درین آشیان شدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درد طلب به مرگ ز من دست بر نداشت</p></div>
<div class="m2"><p>آخر چو موج کشتی ریگ روان شدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رضوان نداشت منصب دربانی بهشت</p></div>
<div class="m2"><p>روزی که من ریاض ترا باغبان شدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا شد قبول پیر خرابات خدمتم</p></div>
<div class="m2"><p>صائب امیدوار به بخت جوان شدم</p></div></div>