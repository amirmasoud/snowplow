---
title: >-
    غزل شمارهٔ ۱۶۳۸
---
# غزل شمارهٔ ۱۶۳۸

<div class="b" id="bn1"><div class="m1"><p>از هوس گر تو به دنبال هواخواهی رفت</p></div>
<div class="m2"><p>زود بی برگ ازین دار فنا خواهی رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کوه تمکین تو چون کاه سبک می گردد</p></div>
<div class="m2"><p>اگر از هر سخن پوچ ز جا خواهی رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست ممکن دل بیتاب تو آسوده شود</p></div>
<div class="m2"><p>تا درین نشأه ندانی که کجا خواهی رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عمر ده روزه زیادست درین وحشتگاه</p></div>
<div class="m2"><p>تا به کی در طلب آب بقا خواهی رفت؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل خود آب کن، از هر دو جهان دست بشو</p></div>
<div class="m2"><p>گر به سر منزل مردان خدا خواهی رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می شوی رو به بقا روز قیامت محشور</p></div>
<div class="m2"><p>نگران گر تو ازین دار فنا خواهی رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گردی از محمل لیلی نتوانی دریافت</p></div>
<div class="m2"><p>گر تو از راه به آواز درا خواهی رفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در دل است آنچه تو در عالم گل می جویی</p></div>
<div class="m2"><p>چند در کعبه پی قبله نما خواهی رفت؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نکند در به رخت باز اگر رخنه دل</p></div>
<div class="m2"><p>تو ازین خانه دربسته کجا خواهی رفت؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو اگر تکیه کنی بر خرد ناقص خود</p></div>
<div class="m2"><p>زود در چاه ضلالت به عصا خواهی رفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به رفیقان موافق چه نهی دل صائب؟</p></div>
<div class="m2"><p>عاقبت از همه چون فرد و جدا خواهی رفت</p></div></div>