---
title: >-
    غزل شمارهٔ ۱۶۴۵
---
# غزل شمارهٔ ۱۶۴۵

<div class="b" id="bn1"><div class="m1"><p>که این نمک ز تبسم در آتشم انداخت؟</p></div>
<div class="m2"><p>که شور در دل و جان مشوشم انداخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو تیر راست، گریزان ز کجروی بودم</p></div>
<div class="m2"><p>فلک چرا چو کمان در کشاکشم انداخت؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خبر نداشت که آتش مراست آب حیات</p></div>
<div class="m2"><p>کسی که همچو سمندر در آتشم انداخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهشت نقد ترا باد روزی ای ساقی</p></div>
<div class="m2"><p>که بیخودی به عجب عالم خوشم انداخت!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عطیه ای است سزاوار قهر یار شدن</p></div>
<div class="m2"><p>چه شد که از نظر لطف، مهوشم انداخت؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز اشک ساخته، پروانه وار شمع مرا</p></div>
<div class="m2"><p>به آب راند و به دریای آتشم انداخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شدم ز بند غم آزاد آن زمان صائب</p></div>
<div class="m2"><p>که دل به حلقه آن زلف دلکشم انداخت</p></div></div>