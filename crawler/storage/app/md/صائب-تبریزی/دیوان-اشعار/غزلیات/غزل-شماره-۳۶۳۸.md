---
title: >-
    غزل شمارهٔ ۳۶۳۸
---
# غزل شمارهٔ ۳۶۳۸

<div class="b" id="bn1"><div class="m1"><p>سرو بستان حیا غنچه جبین می باید</p></div>
<div class="m2"><p>نرگس باغ ادب پرده نشین می باید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوخ چشمی که به صیادی دل می آید</p></div>
<div class="m2"><p>نگهش در پس مژگان به کمین می باید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم مخمور و نگه سرخوش و لبها میگون</p></div>
<div class="m2"><p>زاهد کوی خرابات چنین می باید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر سر تخت دم از عشق زدن بی معنی است</p></div>
<div class="m2"><p>عاشق بی سر و پا خاک نشین می باید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اشک چون بی اثر افتاد به خاکش بسپار</p></div>
<div class="m2"><p>صدف بدگهران زیر زمین می باید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه آهونگهان بر سر مجنون جمعند</p></div>
<div class="m2"><p>چشم بد دور، نظرباز چنین می باید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب اسباب جنونم همه آماده شده است</p></div>
<div class="m2"><p>گوشه چشمی ازان زهره جبین می باید</p></div></div>