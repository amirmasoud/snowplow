---
title: >-
    غزل شمارهٔ ۲۸۸۲
---
# غزل شمارهٔ ۲۸۸۲

<div class="b" id="bn1"><div class="m1"><p>چو احرام تماشای چمن آن سیمبر بندد</p></div>
<div class="m2"><p>زطوق خود به خدمت سرو را قمری کمر بندد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر حسن گلوسوز شکر این چاشنی دارد</p></div>
<div class="m2"><p>به حرف تلخ منقار مرا بر یکدیگر بندد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زدل چون در دو داغ عشق را مانع توانم شد؟</p></div>
<div class="m2"><p>به روی میهمان غیب حد کیست در بندد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چسان پنهان کند دل خرده راز محبت را؟</p></div>
<div class="m2"><p>که سنگ خاره نتوانست چشم این شرر بندد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زدم در بحر وحدت غوطه ها از چشم پوشیدن</p></div>
<div class="m2"><p>یکی گردد به دریا چون حباب از خود نظر بندد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حریصان را به هیچ و پوچ قانع صید خود سازد</p></div>
<div class="m2"><p>مگس را عنکبوت از تار سستی بال و پر بندد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر از جیب نبات آورد بیرون بید بی حاصل</p></div>
<div class="m2"><p>نمی دانیم کی نخل امید ما ثمر بندد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زخواب سیر در منزل تواند زله ها بستن</p></div>
<div class="m2"><p>سبکسیری که جای توشه دامن بر کمر بندد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زند تا پر بر هم صائب کف خاکستری گردد</p></div>
<div class="m2"><p>سمندر نامه ما را اگر بر بال و پر بندد</p></div></div>