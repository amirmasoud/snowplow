---
title: >-
    غزل شمارهٔ ۲۴۶۴
---
# غزل شمارهٔ ۲۴۶۴

<div class="b" id="bn1"><div class="m1"><p>از غبار خط دهان تنگ او پوشیده ماند</p></div>
<div class="m2"><p>دیدنی نادیده و نادیدنی در دیده ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه هر خاری به دامن گل ازان گلزار چید</p></div>
<div class="m2"><p>بیشتر گلهای باغ حسن او ناچیده ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طاق ابروی ترا تا بست معمار قضا</p></div>
<div class="m2"><p>روی من از قبله اسلام بر گردیده ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کرد اگر سیمین بران را پرده پوشی پیرهن</p></div>
<div class="m2"><p>از لطافت پیکر آن سیمبر پوشیده ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقش را بر آب، در آتش بود نعل رحیل</p></div>
<div class="m2"><p>حیرتی دارم که چون عکس رخش در دیده ماند؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راز ما خونین دلان را محرمی پیدا نشد</p></div>
<div class="m2"><p>در دل دریا گره این گوهر سنجیده ماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک نشو و نمای دانه در افتادگی است</p></div>
<div class="m2"><p>وقت مستی خوش که زیر پای خم غلطیده ماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خام اگر باشد خیال ما نه از تقصیر ماست</p></div>
<div class="m2"><p>دیگ ما از سردی ایام ناجوشیده ماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیستم یک جو زمیزان قیامت منفعل</p></div>
<div class="m2"><p>کز گرانی جرم من در حشر ناسنجیده ماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غیرت ما تن به اظهار شکایت درنداد</p></div>
<div class="m2"><p>تا قیامت سر به مهر این نامه پیچیده ماند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در بساط زندگی از گرم و سرد روزگار</p></div>
<div class="m2"><p>آه سرد و اشک گرمی در دل و دردیده ماند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عمر کوته را کند آزادگی صائب دراز</p></div>
<div class="m2"><p>سرو پابرجا زفیض دامن برچیده ماند</p></div></div>