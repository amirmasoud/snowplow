---
title: >-
    غزل شمارهٔ ۴۲۳۴
---
# غزل شمارهٔ ۴۲۳۴

<div class="b" id="bn1"><div class="m1"><p>از عیب پاک شو که هنرها همی‌دهند</p></div>
<div class="m2"><p>دست از خزف بشو که گهرها همی‌دهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راضی مشو به قلب که نقد جهان ز توست</p></div>
<div class="m2"><p>بفشان شکوفه را که ثمرها همی‌دهند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در راه او نثار کن این خرده حیات</p></div>
<div class="m2"><p>وان گه نظاره کن که چه زرها همی‌دهند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین زهرهای قندنما آستین‌فشان</p></div>
<div class="m2"><p>زان تنگ لب ببین چه شکرها همی‌دهند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پنهان مکن چو بیجگران روی در سپر</p></div>
<div class="m2"><p>از حفظ حق ببین چه سپرها همی‌دهند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگذر درین سر از سر بی‌مغز چون حباب</p></div>
<div class="m2"><p>زان سر نظاره کن که چه سرها همی‌دهند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طاوس‌وار پیش پر خویش عاشقی</p></div>
<div class="m2"><p>از غیب غافلی که چه پرها همی‌دهند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برگ است سنگ راه تو ای نخل خوش ثمر</p></div>
<div class="m2"><p>بی‌برگ شو ببین چه ثمرها همی‌دهند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در پایتخت عشق که تاج است بی‌سری</p></div>
<div class="m2"><p>بیرون رو از میان که کمرها همی‌دهند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زان ملک بی‌نشان که خبرها در او گم است</p></div>
<div class="m2"><p>یک بار نشنوی چه خبرها همی‌دهند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گشتی تمام عمر درین خاکدان بس است</p></div>
<div class="m2"><p>بیرون ز خود نشان سفرها همی‌دهند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یک بار رو چرا به در دل نمی‌کنند</p></div>
<div class="m2"><p>این ناکسان که زحمت درها همی‌دهند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در پیری از گرانی غفلت مباش امن</p></div>
<div class="m2"><p>خواب گران به وقت سحرها همی‌دهند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این آن غزل که مولوی روم گفته است</p></div>
<div class="m2"><p>امسال بلبلان چه خبرها همی‌دهند</p></div></div>