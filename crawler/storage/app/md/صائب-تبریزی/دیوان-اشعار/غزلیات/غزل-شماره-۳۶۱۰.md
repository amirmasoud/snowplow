---
title: >-
    غزل شمارهٔ ۳۶۱۰
---
# غزل شمارهٔ ۳۶۱۰

<div class="b" id="bn1"><div class="m1"><p>مانع گرمروان ساعت سنگین نشود</p></div>
<div class="m2"><p>سیل از کوه گرانسنگ به تمکین نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جنگ با گردش چرخ قدر انداز خطاست</p></div>
<div class="m2"><p>سپر تیر قضا جبهه پرچین نشود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از تماشای تو چون خلق نیارند ایمان؟</p></div>
<div class="m2"><p>کافرست آن که ترا بیند و بی دین نشود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست از چین جبین خیره نگاهان را باک</p></div>
<div class="m2"><p>خار از چیدن گل مانع گلچین نشود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هست بی صورت اگر مالک صد گنج بود</p></div>
<div class="m2"><p>تا توانگر کسی از چهره زرین نشود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون گل از خنده رنگین نگشاید صائب</p></div>
<div class="m2"><p>دل هرکس تهی از گریه خونین نشود</p></div></div>