---
title: >-
    غزل شمارهٔ ۱۷۳۷
---
# غزل شمارهٔ ۱۷۳۷

<div class="b" id="bn1"><div class="m1"><p>ثبات دولت خوبی ز کوه تمکین است</p></div>
<div class="m2"><p>حصار عافیت باغ، گوش سنگین است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه وقت توست که لب بر لب پیاله نهی؟</p></div>
<div class="m2"><p>برای حسن تو آیینه چشم بدبین است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بوالهوس نکنی سرکشی، نمی دانی</p></div>
<div class="m2"><p>که گل پیاده چو افتاد، مفت گلچین است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فریب دختر رزخورده ای، نمی دانی</p></div>
<div class="m2"><p>که این سیاه درون را حجاب کابین است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرا به روی تو هر کج نظر نگاه کند؟</p></div>
<div class="m2"><p>هزار حیف که پیشانی تو بی چین است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غرور حسن ندانم چه با تو خواهد کرد</p></div>
<div class="m2"><p>که مست حسنی و این خواب، سخت سنگین است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به گوش جان بشنو پندهای صائب را</p></div>
<div class="m2"><p>که از نصیحت او روی شرم رنگین است</p></div></div>