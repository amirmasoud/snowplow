---
title: >-
    غزل شمارهٔ ۷۰۱
---
# غزل شمارهٔ ۷۰۱

<div class="b" id="bn1"><div class="m1"><p>دیوانه کرد سبزه خطت بهار را</p></div>
<div class="m2"><p>در خاک و خون کشید رخت لاله زار را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر موی دلفریب تو شیرازه دلی است</p></div>
<div class="m2"><p>متراش زینهار خط مشکبار را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگذر ز حسن ترک که در گوشمال دل</p></div>
<div class="m2"><p>دست دگر بود کمر بهله دار را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست حنایی تو ز نیرنگ دلبری</p></div>
<div class="m2"><p>یکدست کرد حسن خزان و بهار را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سنگ یده است مهره گهواره یتیم</p></div>
<div class="m2"><p>جز گریه کار نیست دل داغدار را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون شوق پای در جگر سنگ بفشرد</p></div>
<div class="m2"><p>با کبک هم خرام کند کوهسار را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب حریف سیلی باد خزان نه ای</p></div>
<div class="m2"><p>پیش از خزان ز خود بفشان برگ و بار را</p></div></div>