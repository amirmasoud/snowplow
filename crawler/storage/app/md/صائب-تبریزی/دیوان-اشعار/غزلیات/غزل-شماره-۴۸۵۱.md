---
title: >-
    غزل شمارهٔ ۴۸۵۱
---
# غزل شمارهٔ ۴۸۵۱

<div class="b" id="bn1"><div class="m1"><p>صد گل به باد رفت و گلابی ندید کس</p></div>
<div class="m2"><p>صد تاک خشک گشت و شرابی ندید کس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باتشنگی بساز که در ساغرسپهر</p></div>
<div class="m2"><p>غیر از دل گداخته ،آبی ندیدکس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آب حیات می طلبد حرص تشنه لب</p></div>
<div class="m2"><p>در وادیی که موج سرابی ندید کس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طی شد جهان واهل دلی از جهان نخاست</p></div>
<div class="m2"><p>دریا به ته رسید و سحابی ندید کس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این ماتم دگر که درین دشت آتشین</p></div>
<div class="m2"><p>دل آب گشت وچشم پرآبی ندید کس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حرفی است این که خضر به آب بقا رسید</p></div>
<div class="m2"><p>زین چرخ دل سیه دم آبی ندید کس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از گردش فلک ،شب کوتاه زندگی</p></div>
<div class="m2"><p>زان سان بسر رسید که خوابی ندید کس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از دانش آنچه داد، کم رزق می نهد</p></div>
<div class="m2"><p>چون آسمان درست حسابی ندید کس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بشکن طلسم هستی خود راکه غیر از ین</p></div>
<div class="m2"><p>بر روی آن نگارنقابی ندیدکس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باد غرور در سرحیران عشق نیست</p></div>
<div class="m2"><p>در بحر آبگینه حبابی ندید کس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب به هر که می نگرم مست و بیخودست</p></div>
<div class="m2"><p>هر چند ساقیی و شرابی ندید کس</p></div></div>