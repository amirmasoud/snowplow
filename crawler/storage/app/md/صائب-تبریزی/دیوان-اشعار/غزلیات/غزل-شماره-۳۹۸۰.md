---
title: >-
    غزل شمارهٔ ۳۹۸۰
---
# غزل شمارهٔ ۳۹۸۰

<div class="b" id="bn1"><div class="m1"><p>شکوه بحر ز امواج آشکاره شود</p></div>
<div class="m2"><p>یکی هزار شود دل چو پاره پاره شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مباش در پی گرد آوری که ماه تمام</p></div>
<div class="m2"><p>ز خود تهی چو شود قابل اشاره شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خودی حصاری ساحل نموده بحر ترا</p></div>
<div class="m2"><p>ز خودکناره گزین بحر بی کناره شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا چوآینه سیری ز وصل ممکن نیست</p></div>
<div class="m2"><p>تمام عمرم اگر صرف یک نظاره شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مباش تلخ دهد عشق اگر گداز ترا</p></div>
<div class="m2"><p>که رو سفید شود قند چون دوباره شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به اصل خویش کند فرع میل می ترسم</p></div>
<div class="m2"><p>که شیشه دل من رفته رفته خاره شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز تنگنای فلک حال من کسی داند</p></div>
<div class="m2"><p>که همچو طفل مقید به گاهواره شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشو ز وحدت وکثرت دوبین که یک نورست</p></div>
<div class="m2"><p>که آفتاب شود روزوشب ستاره شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز خود برآی که جز عیسی مجرد نیست</p></div>
<div class="m2"><p>تهمتنی که به رخش فلک سواره شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>توآن زمانه به نظرهاعزیز می گردی</p></div>
<div class="m2"><p>که آتش تو چو یاقوت بی شراره شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نمی توان به جگر داغ عشق پنهان کرد</p></div>
<div class="m2"><p>کز آفتاب گریبان صبح پاره شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگیر دامن خورشید طلعتی صائب</p></div>
<div class="m2"><p>که همچو صبح ترا زندگی دوباره شود</p></div></div>