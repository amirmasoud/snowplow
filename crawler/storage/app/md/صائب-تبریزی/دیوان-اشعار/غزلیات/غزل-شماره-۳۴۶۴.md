---
title: >-
    غزل شمارهٔ ۳۴۶۴
---
# غزل شمارهٔ ۳۴۶۴

<div class="b" id="bn1"><div class="m1"><p>دل پیران کهنسال غمین می باشد</p></div>
<div class="m2"><p>قامت خم شده را داغ نگین می باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دل هرکه بود خرده رازی مستور</p></div>
<div class="m2"><p>همچو دریای گهر تلخ جبین می باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از لب ساغر می راز تراوش نکند</p></div>
<div class="m2"><p>ساکن کوی خرابات امین می باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یاد رخسار تو هم آتش بی زنهارست</p></div>
<div class="m2"><p>رتبه حسن گلوسوز همین می باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خال در کنج لب و گوشه چشم است مقیم</p></div>
<div class="m2"><p>دزد پیوسته طلبکار کمین می باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دهن خویش مکن باز به دریا صائب</p></div>
<div class="m2"><p>که غذای صدف از در ثمین می باشد</p></div></div>