---
title: >-
    غزل شمارهٔ ۶۰۴۵
---
# غزل شمارهٔ ۶۰۴۵

<div class="b" id="bn1"><div class="m1"><p>پیش مستان از خرد بیگانه می باید شدن</p></div>
<div class="m2"><p>چون به طفلان می رسی دیوانه می باید شدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مدتی در خواب بی دردی به سر بردی، بس است</p></div>
<div class="m2"><p>این زمان در عاشقی افسانه می باید شدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرزه خندی آبروی شیشه را بر خاک ریخت</p></div>
<div class="m2"><p>باده چون خوردی، لب پیمانه می باید شدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دامن بخت بلند آسان نمی آید به دست</p></div>
<div class="m2"><p>در زمین خاکساری دانه می باید شدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشقی و کوچه گردی در جوانی ها خوش است</p></div>
<div class="m2"><p>پیر چون گشتی وبال خانه می باید شدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست آسان در حریم زلف او محرم شدن</p></div>
<div class="m2"><p>بی زبان با صد زبان چون شانه می باید شدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خصم سرکش را توان ز افتادگی تسخیر کرد</p></div>
<div class="m2"><p>شیشه چون گردن کشد، پیمانه می باید شدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روزگاری شعله آواز مطرب بوده ای</p></div>
<div class="m2"><p>مدتی هم شمع ماتمخانه می باید شدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آشنای معنی بیگانه گشتن سهل نیست</p></div>
<div class="m2"><p>صائب از هر آشنا بیگانه می باید شدن</p></div></div>