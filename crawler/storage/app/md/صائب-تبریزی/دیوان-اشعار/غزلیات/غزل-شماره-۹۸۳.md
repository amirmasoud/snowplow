---
title: >-
    غزل شمارهٔ ۹۸۳
---
# غزل شمارهٔ ۹۸۳

<div class="b" id="bn1"><div class="m1"><p>گوش بی دردان گران از خواب باشد بهتر است</p></div>
<div class="m2"><p>این صدف پر گوهر سیماب باشد بهترست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رتبه خوبی دو بالا می شود از چشم پاک</p></div>
<div class="m2"><p>سرو موزون در کنار آب باشد بهترست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آب چشم از دامن پاکان به جایی می رسد</p></div>
<div class="m2"><p>شمع اگر در گوشه محراب باشد بهترست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرو بی حاصل اگر از جا نخیزد گو مخیز</p></div>
<div class="m2"><p>پای چوبین در حنای خواب باشد بهترست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی نیازی می شود بند زبان هرزه گو</p></div>
<div class="m2"><p>خار دامنگیر اگر سیراب باشد بهترست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شبنمی کز جرعه گلها خمارش نشکند</p></div>
<div class="m2"><p>طالب خورشید عالمتاب باشد بهترست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می کشد سر رشته جولان به دریا سیل را</p></div>
<div class="m2"><p>کار عاشق با دل بی تاب باشد بهترست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شهپر پرواز هم باشند روشن گوهران</p></div>
<div class="m2"><p>بستر و بالین موج از آب باشد بهترست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با دل روشن چه بگشاید ز تقریر زبان؟</p></div>
<div class="m2"><p>شمع اگر خاموش در مهتاب باشد بهترست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>داغ ما صائب حریف چشم شور خلق نیست</p></div>
<div class="m2"><p>جامی می در جام ما خوناب باشد بهترست</p></div></div>