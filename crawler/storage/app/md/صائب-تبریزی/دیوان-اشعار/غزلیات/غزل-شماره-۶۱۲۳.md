---
title: >-
    غزل شمارهٔ ۶۱۲۳
---
# غزل شمارهٔ ۶۱۲۳

<div class="b" id="bn1"><div class="m1"><p>چون زند دامان وحشت بر کمر سودای من</p></div>
<div class="m2"><p>خاک ساکن پر برون آرد ز نقش پای من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرم رفتاری چو من دشت جنون هرگز نداشت</p></div>
<div class="m2"><p>موی آتش دیده گردد خار زیر پای من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راست می سازد دل شبها نفس موج سراب</p></div>
<div class="m2"><p>راحت منزل ندارد شوق بی پروای من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون فلک باشد مسلسل دور سرگردانیم</p></div>
<div class="m2"><p>گردباد انگشت حیرت گشت در صحرای من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق عالمسوز هر داغی که سوزد بر دلم</p></div>
<div class="m2"><p>عینک دیگر شود بهر دل بینای من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتگوی سخت رویان بر دل من بار نیست</p></div>
<div class="m2"><p>هیچ جا لنگر نمی گیرد به خود دریای من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با کمال ناگواریها، گوارا کرده است</p></div>
<div class="m2"><p>محنت امروز را اندیشه فردای من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باده من جام را بی ساقی اندازد به دور</p></div>
<div class="m2"><p>شیشه را چون نار خندان می کند صهبای من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بندهای سست را صائب توان آسان گسیخت</p></div>
<div class="m2"><p>سهل باشد گر نباشد منتظم دنیای من</p></div></div>