---
title: >-
    غزل شمارهٔ ۵۵۹۵
---
# غزل شمارهٔ ۵۵۹۵

<div class="b" id="bn1"><div class="m1"><p>نشست از آسیای چرخ گرد شیب بر رویم</p></div>
<div class="m2"><p>سفیدی می کند راه فنا از هر سر مویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن بیماری من می شود هر روز سنگین تر</p></div>
<div class="m2"><p>که گیرد گوش خود با هر که درد خویش می گویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود در دیده حق بین من دیر و حرم یکسان</p></div>
<div class="m2"><p>ندارد سنگ کم در پله بینش ترازویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آن ساغر که در آغازش عشق از دست او خوردم</p></div>
<div class="m2"><p>همان بیخود شوم هر گاه دست خویش می بویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کلید از خانه دارد قفل وسواس و من از حیرت</p></div>
<div class="m2"><p>گشاد عقده دل را از هر بیدرد می جویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می گلگون چه سازد با دل پر خون من صائب؟</p></div>
<div class="m2"><p>که من از ساده لوحی خون به خون پیوسته می شویم</p></div></div>