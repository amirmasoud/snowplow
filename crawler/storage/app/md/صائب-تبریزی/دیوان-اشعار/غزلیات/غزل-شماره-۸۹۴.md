---
title: >-
    غزل شمارهٔ ۸۹۴
---
# غزل شمارهٔ ۸۹۴

<div class="b" id="bn1"><div class="m1"><p>تو که بی پرده رخ خود ننمایی در خواب</p></div>
<div class="m2"><p>چه خیال است به آغوش من آیی در خواب؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمع بالین خود از دیده بیدار کنی</p></div>
<div class="m2"><p>گر بدانی چه قدرها به صفایی در خواب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا به بیداری و مخموری و مستی چه کنی</p></div>
<div class="m2"><p>تو که چون چشم، دل از خلق ربایی در خواب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عالم از بی خبران، دیده خواب آلودی است</p></div>
<div class="m2"><p>به امیدی که رخ خود بنمایی در خواب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون تواند کسی از یاد تو غافل گردید</p></div>
<div class="m2"><p>که ز بی تابی دل، قبله نمایی در خواب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تن خاکی هدف ناوک دلدوز قضاست</p></div>
<div class="m2"><p>خبر از خویش نداری که کجایی در خواب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از خیال سفر هند، سیاه است دلت</p></div>
<div class="m2"><p>گر چه در پرده شبها چو حنایی در خواب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با تو یک صبح قیامت چه تواند کردن؟</p></div>
<div class="m2"><p>که ز هر مو، سر مژگان جدایی در خواب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سایه کوه در اینجا به جناح سفرست</p></div>
<div class="m2"><p>تو چه در ظل سبکسیر همایی در خواب؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پرده خواب بود عینک بیداردلان</p></div>
<div class="m2"><p>تو چنین با نظرباز، چرایی در خواب؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>راه خوابیده ز فریاد جرس شد بیدار</p></div>
<div class="m2"><p>تو چو افسانه به آواز درایی در خواب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این میانی که به قصد تو فلک ها بسته است</p></div>
<div class="m2"><p>جای دارد که میان را نگشایی در خواب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رفت از دست حواس و تو همان پا بر جای</p></div>
<div class="m2"><p>همرهان تو کجا و تو کجایی در خواب!</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این تعلق که ترا هست به آب و گل جسم</p></div>
<div class="m2"><p>باورم نیست که از خویش برآیی در خواب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ذره پیوست به خورشید و تو از همت پست</p></div>
<div class="m2"><p>در ته دامن افلاک، چو پایی در خواب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فلک از ثابت و سیار ترا می پاید</p></div>
<div class="m2"><p>چون به صد دشمن بیدار برآیی در خواب؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نیست ممکن، نشود خون تو صائب پامال</p></div>
<div class="m2"><p>که ته پای حوادث چو حنایی در خواب</p></div></div>