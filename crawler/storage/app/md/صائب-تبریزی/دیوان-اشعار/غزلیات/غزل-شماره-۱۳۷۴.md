---
title: >-
    غزل شمارهٔ ۱۳۷۴
---
# غزل شمارهٔ ۱۳۷۴

<div class="b" id="bn1"><div class="m1"><p>بوی زلف او حواسم را پریشان کرد و رفت</p></div>
<div class="m2"><p>برگ عیش پنج روزم را به دامان کرد و رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آه دود تلخکامان کار خود را می کند</p></div>
<div class="m2"><p>زلف پندارد را خاطر پریشان کرد و رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذره ای از آفتاب عشق در آفاق نیست</p></div>
<div class="m2"><p>این شرر را کوهکن در سنگ پنهان کرد و رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وقت آن کان ملاحت خوش که از یک نوشخند</p></div>
<div class="m2"><p>داغهای سینه ما را نمکدان کرد و رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که زین دریای پر آشوب سر زد چون حباب</p></div>
<div class="m2"><p>تاج و تخت خویش را تسلیم طوفان کرد و رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پاس لشکر داشتن از خسروان زیبنده است</p></div>
<div class="m2"><p>این نصیحت مور در کار سلیمان کرد و رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که بیرون آمد از دارالامان نیستی</p></div>
<div class="m2"><p>چون شرر در اوج هستی یک دو جولان کرد و رفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روزگار خوش عنانی خوش که چون سیل بهار</p></div>
<div class="m2"><p>کعبه گر سنگ رهش گردید، ویران کرد و رفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که صائب از حریم نیستی آمد برون</p></div>
<div class="m2"><p>بر سر خشت عناصر یک دو جولان کرد و رفت</p></div></div>