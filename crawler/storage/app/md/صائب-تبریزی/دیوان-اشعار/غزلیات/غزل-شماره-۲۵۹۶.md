---
title: >-
    غزل شمارهٔ ۲۵۹۶
---
# غزل شمارهٔ ۲۵۹۶

<div class="b" id="bn1"><div class="m1"><p>غنچه خسبانی که از زانوی خود بالین کنند</p></div>
<div class="m2"><p>از شکست تن کمند شوق را پرچین کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه در ظاهر به زیردست و پا افتاده اند</p></div>
<div class="m2"><p>بگذرند از نه فلک چون رخش همت زین کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سالها در خرقه پشمینه خون خود خورند</p></div>
<div class="m2"><p>تا دم خود را چو آهوی ختا مشکین کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در محیط تلخ، دندان بر سر دندان نهند</p></div>
<div class="m2"><p>تا چو گوهر استخوان خویش را شیرین کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کوههای درد چون رطل گران بر سر کشند</p></div>
<div class="m2"><p>تا زطاعت پله میزان خود سنگین کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سنگ را سازند لعل از روی دل چون آفتاب</p></div>
<div class="m2"><p>خانه ها را زرنگار از چهره زرین کنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر چراغ مرده از نور یقین عیسی شوند</p></div>
<div class="m2"><p>دردهای کهنه را درمان به درد دین کنند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در هوا چون خرده جان شرر رقصان شود</p></div>
<div class="m2"><p>گر ز روی شوق خون مرده را تلقین کنند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می شود در یک دم از اوتاد، چون کوه گران</p></div>
<div class="m2"><p>کاه برگی را که آن دریادلان تمکین کنند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرچه دارند اختیار بالش زانوی حور</p></div>
<div class="m2"><p>چون سبو در پای خم از دست خود بالین کنند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مایه داران مروت با لب خندان چو گل</p></div>
<div class="m2"><p>خون خود با خونبها در دامن گلچین کنند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صائب از دامان ایشان دست رغبت بر مدار</p></div>
<div class="m2"><p>کآبهای تلخ را این ابرها شیرین کنند</p></div></div>