---
title: >-
    غزل شمارهٔ ۶۴۹۰
---
# غزل شمارهٔ ۶۴۹۰

<div class="b" id="bn1"><div class="m1"><p>در کهنسالی ز مرگ ناگهان غافل مشو</p></div>
<div class="m2"><p>برگ چون شد زرد از باد خزان غافل مشو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امن نتوان زیست از اقبال و ادبار فلک</p></div>
<div class="m2"><p>از دم شمشیر و از پشت کمان غافل مشو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از چراغی می توان افروخت چندین شمع را</p></div>
<div class="m2"><p>دولتی چون رودهد از دوستان غافل مشو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا در ایام خزان برگ و نوایی باشدت</p></div>
<div class="m2"><p>در بهار از بلبلان ای باغبان غافل مشو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برگ از اندک نسیمی دست و پا گم می کند</p></div>
<div class="m2"><p>تا نفس باقی است از پاس زبان غافل مشو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیدی از اخوان چه پیش آمد عزیز مصر را</p></div>
<div class="m2"><p>زینهار از مکر اخوان زمان غافل مشو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر کجا چون شمع گرم محفل آرایی شوی</p></div>
<div class="m2"><p>از دهان گاز ای آتش زبان غافل مشو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا زبان شکر جای سبزه باشد حاصلت</p></div>
<div class="m2"><p>از زمین تشنه، ای آب روان غافل مشو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نابجا نبود سخن، مگشا لب گفتار خویش</p></div>
<div class="m2"><p>از هدف چون تیر در بحر کمان غافل مشو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برندارد دولت بیدار غفلت، زینهار</p></div>
<div class="m2"><p>در کنار بام از خواب گران غافل مشو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رشته هستی به قدر فکر می گردد بلند</p></div>
<div class="m2"><p>صائب از تحصیل عمر جاودان غافل مشو</p></div></div>