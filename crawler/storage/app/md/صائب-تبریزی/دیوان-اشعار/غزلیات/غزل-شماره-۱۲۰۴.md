---
title: >-
    غزل شمارهٔ ۱۲۰۴
---
# غزل شمارهٔ ۱۲۰۴

<div class="b" id="bn1"><div class="m1"><p>زیر پای سرو چون آب روان غلطیدنی است</p></div>
<div class="m2"><p>گل به تردستی ز عکس تازه رویان چیدنی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر لباس فاخری در عالم ایجاد هست</p></div>
<div class="m2"><p>از گناه زیردستان چشم خود پوشیدنی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیشدستی کن، ازین تشویش خود را وارهان</p></div>
<div class="m2"><p>جام پر زهر اجل چون عاقبت نوشیدنی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دم سرد خزان چون می رود آخر به باد</p></div>
<div class="m2"><p>با لب خندان به گلچین سر چو گل بخشیدنی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا به کی در استخوان بندی گدازی مغز خود؟</p></div>
<div class="m2"><p>این طلسم استخوانی چون ز هم پاشیدنی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وقت خود ضایع مکن چون غافلان در چیدنش</p></div>
<div class="m2"><p>چون بساط زندگانی عاقبت برچیدنی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر دل آزاده حسن خلق بند آهن است</p></div>
<div class="m2"><p>از گل بی خار بیش از خار دامن چیدنی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست از بخت سیه دلهای روشن را ملال</p></div>
<div class="m2"><p>دیده آیینه شبها ایمن از نادیدنی است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل ز اشک گرم خالی ساز هنگام صبوح</p></div>
<div class="m2"><p>در زمین پاک، صائب تخم خود پاشیدنی است</p></div></div>