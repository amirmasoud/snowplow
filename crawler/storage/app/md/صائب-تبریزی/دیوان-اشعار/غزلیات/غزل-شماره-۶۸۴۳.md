---
title: >-
    غزل شمارهٔ ۶۸۴۳
---
# غزل شمارهٔ ۶۸۴۳

<div class="b" id="bn1"><div class="m1"><p>عیش فرش است در آن محفل روح افزایی</p></div>
<div class="m2"><p>که فتد شیشه می جایی و ساقی جایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرد کلفت ننشیند به جبین در بزمی</p></div>
<div class="m2"><p>که بود دست فشان سرو سهی بالایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردمک مهر خموشی است نظربازان را</p></div>
<div class="m2"><p>در حریمی که نباشد نظر گویایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یوسف از قحط خریدار دل خود می خورد</p></div>
<div class="m2"><p>حسن مغرور تو می داشت اگر پروایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم ازان حسن جهانگیر چه ادراک کند؟</p></div>
<div class="m2"><p>در حبابی چه قدر جلوه کند دریایی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در تماشای تو افتاد کلاه از سر چرخ</p></div>
<div class="m2"><p>خبر از خویش نداری چه قدر رعنایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر خورشید درین راه به خاک افتاده است</p></div>
<div class="m2"><p>که به افتادگی سایه کند پروایی؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کوه را ناله من سر به بیابان داده است</p></div>
<div class="m2"><p>نیست در دامن این دشت چو من شیدایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جلوه بیهده ضایع مکن ای باغ بهشت</p></div>
<div class="m2"><p>که من از گوشه دل یافته ام مائوایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تنگی خاک مرا بر سر آن می آرد</p></div>
<div class="m2"><p>کز غبار دل خود طرح کنم صحرایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عشرت روی زمین خانه به دوشان دارند</p></div>
<div class="m2"><p>جای رشک است بر آن کس که ندارد جایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر کف خاک ز اسرار حقیقت لوحی است</p></div>
<div class="m2"><p>صائب از سرمه توفیق اگر بینایی</p></div></div>