---
title: >-
    غزل شمارهٔ ۳۲۷۶
---
# غزل شمارهٔ ۳۲۷۶

<div class="b" id="bn1"><div class="m1"><p>وقت ارباب دل آشفته به مویی گردد</p></div>
<div class="m2"><p>صید وحشت زده آواره به هویی گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی تأمل مژه مگشای درین عبرتگاه</p></div>
<div class="m2"><p>که ترازوی مکافات به مویی گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درس آزادگیش زود روان می گردد</p></div>
<div class="m2"><p>هرکه چون سرو مقیم لب جویی گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاقبت چون همه را خاک شدن در پیش است</p></div>
<div class="m2"><p>ای خوش آن خاک که جامی و سبویی گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جگر سوخته از جنبش مژگان ریزد</p></div>
<div class="m2"><p>چون قلم هر که گرفتار دورویی گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زخم شمشیر تغافل همه مخصوص من است</p></div>
<div class="m2"><p>این نه آبی است که هر روز به جویی گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صورت خوب به هر مشت گلی می بخشند</p></div>
<div class="m2"><p>تا که شایسته اخلاق نکویی گردد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهر روشندلی ما دم گرمی کافی است</p></div>
<div class="m2"><p>چشم یعقوب پر از نور به بویی گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بس بود از دو جهان محو تماشای ترا</p></div>
<div class="m2"><p>آنقدر وقت که مشغول وضویی گردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر غباری که ازو چشم نپوشی صائب</p></div>
<div class="m2"><p>در نهانخانه دل آینه رویی گردد</p></div></div>