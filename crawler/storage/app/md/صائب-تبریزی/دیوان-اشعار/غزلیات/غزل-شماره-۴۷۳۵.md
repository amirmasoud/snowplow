---
title: >-
    غزل شمارهٔ ۴۷۳۵
---
# غزل شمارهٔ ۴۷۳۵

<div class="b" id="bn1"><div class="m1"><p>برق سبک عنان نرسد در شتاب عمر</p></div>
<div class="m2"><p>زنهار دل مبند به مد شهاب عمر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بنگری به دیده عبرت، اشاره ای است</p></div>
<div class="m2"><p>هر ماه نو به جلوه پا در رکاب عمر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طول امل چه رشته که بر هم نتافته است</p></div>
<div class="m2"><p>شیرازه گیر نیست دریغا کتاب عمر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا ممکن است ضبط نفس کن که هر نفس</p></div>
<div class="m2"><p>تکبیر نیستی است به قصر حباب عمر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داغم ز عمر کوته و رعنایی امل</p></div>
<div class="m2"><p>می بود کاش طول امل در حساب عمر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صائب اگر امان دهدم عمر، می کنم</p></div>
<div class="m2"><p>از بوسه های کنج لبی انتخاب عمر</p></div></div>