---
title: >-
    غزل شمارهٔ ۳۰۱۵
---
# غزل شمارهٔ ۳۰۱۵

<div class="b" id="bn1"><div class="m1"><p>خمار باده مهر دوستان را کینه می سازد</p></div>
<div class="m2"><p>کدورت صبح شنبه را شب آدینه می سازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غباری از لباس فقر بر دل نیست صوفی را</p></div>
<div class="m2"><p>به روی تازه به با خرقه پشمینه می سازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخش از حلقه خط می کند پیدا نظربازان</p></div>
<div class="m2"><p>چه طوطیها زموم سبز این آیینه می سازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندارد نشأه سرجوش درد عالم امکان</p></div>
<div class="m2"><p>مرا جان تازه یاد مردم پیشینه می سازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صدف را دل دو نیم از گوهر دریا شکوهم شد</p></div>
<div class="m2"><p>مرا از دیده ها مستور کی گنجینه می سازد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به نسبت آشنایی کن که با ناجنس پیوستن</p></div>
<div class="m2"><p>ترا با خوش قماشی در نظرها پینه می سازد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به آسانی قدم بر اوج عزت می نهد صائب</p></div>
<div class="m2"><p>گرانقدری که از حفظ مراتب زینه می سازد</p></div></div>