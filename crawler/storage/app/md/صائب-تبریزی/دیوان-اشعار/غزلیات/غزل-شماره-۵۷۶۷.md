---
title: >-
    غزل شمارهٔ ۵۷۶۷
---
# غزل شمارهٔ ۵۷۶۷

<div class="b" id="bn1"><div class="m1"><p>به مهر داغ رسیده است جمله اعضایم</p></div>
<div class="m2"><p>ز پای تا به سر خویش چشم بینایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه لازم است چو مجنون شوم بیابان گرد؟</p></div>
<div class="m2"><p>که از غبار دل خود بس است صحرایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمی شود نشود داغ لاله ها ناسور</p></div>
<div class="m2"><p>که دشت کان نمک شد ز شور سودایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بغیر خانه زنجیر ازین جهان خراب</p></div>
<div class="m2"><p>به هیچ خانه دیگر نمی رود پایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا به غیرت همکار احتیاجی نست</p></div>
<div class="m2"><p>ز ذوق کار مهیاست کار فرمایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به سنگ رفته فرو پای من ز دل سختی</p></div>
<div class="m2"><p>نمی برد سخن سرد ناصح از جایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا ز قرب گرانان همین کفایت بس</p></div>
<div class="m2"><p>که کوه قاف سبک شد به دل چو عنقایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به نرخ خاک ز من مشتری نمی گیرد</p></div>
<div class="m2"><p>ز بس که گرد کسادی گرفته کالایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نهان چگونه کنم راز عشق را صائب؟</p></div>
<div class="m2"><p>که همچو نامه واکرده است سیمایم</p></div></div>