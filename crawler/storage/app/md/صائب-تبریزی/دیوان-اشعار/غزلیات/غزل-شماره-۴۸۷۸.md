---
title: >-
    غزل شمارهٔ ۴۸۷۸
---
# غزل شمارهٔ ۴۸۷۸

<div class="b" id="bn1"><div class="m1"><p>ظاهر مردان به زیور گر نباشد گو مباش</p></div>
<div class="m2"><p>حلقه بیرون در گر زر نباشد گو مباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخنه‌های دام، فتح‌الباب صید بسته است</p></div>
<div class="m2"><p>سینه ما را رفوگر گر نباشد گو مباش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حلقه زنجیر اگر از هم بریزد گو بریز</p></div>
<div class="m2"><p>کار دنیا را نظامی گر نباشد گو مباش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی‌زبانی آیه رحمت بود درشان جهل</p></div>
<div class="m2"><p>طفل را در دست اگر خنجر نباشد گو مباش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در عقیق بی‌نیازی هست آب صد محیط</p></div>
<div class="m2"><p>نم اگر در قلزم اخضر نباشد گو مباش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون صدف در پرده دل اشک باریدن خوش است</p></div>
<div class="m2"><p>گر به ظاهر چشم عاشق تر نباشد گو مباش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از تپیدن می‌توان کوتاه کرد این راه را</p></div>
<div class="m2"><p>قوت پرواز اگر در پر نباشد گو مباش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سایه بیدست، آه سرد اهل جرم را</p></div>
<div class="m2"><p>سایه گر در عرصه محشر نباشد گو مباش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خازن گنج گهر را خلق خوش دام بلاست</p></div>
<div class="m2"><p>در بساط بحر اگر عنبر نباشد گو مباش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از گداز جسم، خون خویش خوردن غافلی است</p></div>
<div class="m2"><p>درد اگر در باده احمر نباشد گو مباش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سوده یاقوت سازد پرتو می مغز را</p></div>
<div class="m2"><p>می‌کشان را تاج گوهر گر نباشد گو مباش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تخم امیدی ندارم تا کنم باران طلب</p></div>
<div class="m2"><p>آب اگر در دیده اختر نباشد گو مباش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از تماشا به ندارد حاصلی دنیای خشک</p></div>
<div class="m2"><p>صورت دیوار اگر در بر نباشد گو مباش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خوابگاه نرم، صائب سنگ راه رهروست</p></div>
<div class="m2"><p>بستر و بالین ما گر پر نباشد گو مباش</p></div></div>