---
title: >-
    غزل شمارهٔ ۶۴۵۳
---
# غزل شمارهٔ ۶۴۵۳

<div class="b" id="bn1"><div class="m1"><p>می دود هر کس ز خود بیرون به استقبال او</p></div>
<div class="m2"><p>سایه چون نقش قدم می ماند از دنبال او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس که سرو قامت او دلپذیر افتاده است</p></div>
<div class="m2"><p>برندارد دل به رفتن آب از تمثال او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقش پای او به خون بی گناهان محضری است</p></div>
<div class="m2"><p>بس که گردیده است خون عاشقان پامال او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما ز بوی پیرهن قانع به یاد یوسفیم</p></div>
<div class="m2"><p>نعمت آن باشد که چشمی نیست در دنبال او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ظاهر از شام غریبان است احوال غریب</p></div>
<div class="m2"><p>حال دل پیداست از زلف پریشان حال او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سکه تا آورد در زر روی، گردانید پشت</p></div>
<div class="m2"><p>ای خوشا جرمی که عذری هست در دنبال او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در میان پشت و روی ما را گر فرقی بود</p></div>
<div class="m2"><p>نیست از ادبار گردون فرق تا اقبال ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد در آن کنج دهن از خرده بینی گوشه گیر</p></div>
<div class="m2"><p>داغ دارد گوشه گیران جهان را خال او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دستگاه بوسه را زیر نگین آورده است</p></div>
<div class="m2"><p>دست اگر یابم، به دندان می کنم تبخال او!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می کند قالب تهی تا حسن گردانید روی</p></div>
<div class="m2"><p>بر امید جان نو آیینه از تمثال او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون مسیحا همت هر کس بلند افتاده است</p></div>
<div class="m2"><p>آسمان صائب بود چون بیضه زیر بال او</p></div></div>