---
title: >-
    غزل شمارهٔ ۱۲۱۰
---
# غزل شمارهٔ ۱۲۱۰

<div class="b" id="bn1"><div class="m1"><p>خط مشکین تو نقش تازه ای بر کار بست</p></div>
<div class="m2"><p>مصحف روی ترا شیرازه از زنار بست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از فروغ حسن نتوان کرد در رویش نگاه</p></div>
<div class="m2"><p>جوش گل راه تماشایی بر این گلزار بست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جوش خون بی بخیه می سازد دهان زخم را</p></div>
<div class="m2"><p>شکوه چون زور آورد نتوان لب اظهار بست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جذب عشق از در درون می آورد معشوق را</p></div>
<div class="m2"><p>طوطی ما را شکر در پسته منقار بست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در محبت کم گناهی نیست اظهار وجود</p></div>
<div class="m2"><p>تا نفس باقی است نتوان لب ز استغفار بست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کعبه سنگ ره نشد سرگشتگان عشق را</p></div>
<div class="m2"><p>چون تواند نقطه راه گردش پرگار بست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرم دارد جوش بلبل صحبت گلزار را</p></div>
<div class="m2"><p>شد جهان افسرده تا صائب لب از گفتار بست</p></div></div>