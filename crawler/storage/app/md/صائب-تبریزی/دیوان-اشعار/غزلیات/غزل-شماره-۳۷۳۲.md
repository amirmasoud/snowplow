---
title: >-
    غزل شمارهٔ ۳۷۳۲
---
# غزل شمارهٔ ۳۷۳۲

<div class="b" id="bn1"><div class="m1"><p>گلی که از عرق شرم دیده بان دارد</p></div>
<div class="m2"><p>خط امان ز شبیخون بلبلان دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به عشق نسبت خاصی است ناتوانان را</p></div>
<div class="m2"><p>گهر علاقه دیگر به ریسمان دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فراغ بال ز مرغان این چمن مطلب</p></div>
<div class="m2"><p>که گر همای بود درد استخوان دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فغان که آینه رخسار من نمی داند</p></div>
<div class="m2"><p>که آشنایی تردامنان زیان دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بجان رساند مرا داغ دوستان دیدن</p></div>
<div class="m2"><p>چه دلخوشی خضر از عمر جاودان دارد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چرا ز غیرت، پروانه خویش را نکشد؟</p></div>
<div class="m2"><p>که شمع با همه انجمن زبان دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وفا به وعده نکردن خلاف آداب است</p></div>
<div class="m2"><p>وگرنه شکوه ما مهر بر زبان دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه حالت است من خسته را نمی دانم</p></div>
<div class="m2"><p>که هرچه جز دل خود می خورم زیان دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لباس ماتم بلبل همیشه آماده است</p></div>
<div class="m2"><p>به هر چمن که در او زاغی آشیان دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چگونه دیده صائب گهرفشان نشود؟</p></div>
<div class="m2"><p>که رو ز ملک خراسان به اصفهان دارد</p></div></div>