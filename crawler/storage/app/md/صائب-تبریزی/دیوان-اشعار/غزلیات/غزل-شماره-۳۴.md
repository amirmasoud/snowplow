---
title: >-
    غزل شمارهٔ ۳۴
---
# غزل شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>حسن بی پروا به فرمان هوس باشد چرا؟</p></div>
<div class="m2"><p>برق عالمسوز در زنجیر خس باشد چرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باده پر زور، کار سنگ با مینا کند</p></div>
<div class="m2"><p>مست را اندیشه از بند عسس باشد چرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا هوا ابر و چمن پر گل بود، از زهد خشک</p></div>
<div class="m2"><p>آدمی در چار دیوار قفس باشد چرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دامن غواص پر گوهر شد از پاس نفس</p></div>
<div class="m2"><p>اینقدر غافل کس از پاس نفس باشد چرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا به خاموشی توان سنگ نشان گشتن، کسی</p></div>
<div class="m2"><p>در قطار هرزه نالان چون جرس باشد چرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا کسی دریا تواند گشتن از ترک هوا</p></div>
<div class="m2"><p>چون حباب پوچ در بند نفس باشد چرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن که کوه قاف چون عنقا بود یک لقمه اش</p></div>
<div class="m2"><p>بر سر خوان ها طفیلی چون مگس باشد چرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این جواب آن غزل صائب که می گوید حکیم</p></div>
<div class="m2"><p>تا نفس باشد، کسی بی همنفس باشد چرا؟</p></div></div>