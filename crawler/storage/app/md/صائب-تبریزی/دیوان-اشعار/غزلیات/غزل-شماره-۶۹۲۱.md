---
title: >-
    غزل شمارهٔ ۶۹۲۱
---
# غزل شمارهٔ ۶۹۲۱

<div class="b" id="bn1"><div class="m1"><p>تا کی غبار خاطر صحرا شود کسی؟</p></div>
<div class="m2"><p>چون گردباد، بادیه پیما شود کسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می بایدش هزار قدح خون به سر کشید</p></div>
<div class="m2"><p>تا در مذاق خلق گوارا شود کسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اوضاع زشت مردم عالم ندیدنی است</p></div>
<div class="m2"><p>امروز صرفه نیست که بینا شود کسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روشندلی که لذت تجرید یافته است</p></div>
<div class="m2"><p>بیرون رود ز خویش چو پیدا شود کسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا می توان ز آبله دست رزق خورد</p></div>
<div class="m2"><p>بهر چه خوشه چین ثریا شود کسی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنجاست آدمی که دلش آرمیده است</p></div>
<div class="m2"><p>هر لحظه ای اگر چه به صد جا شود کسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حرف مقام قافله بارست بر دلش</p></div>
<div class="m2"><p>چون پیشتر ز کوچ مهیا شود کسی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون در حباب، موج پر و بال وا کند؟</p></div>
<div class="m2"><p>در تنگنای چرخ چسان وا شود کسی؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در چشم این سیاه دلان صبح کاذب است</p></div>
<div class="m2"><p>در روشنی اگر ید بیضا شود کسی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا می توان ز لذت دیدار محو شد</p></div>
<div class="m2"><p>بیخود چرا ز نشائه صهبا شود کسی؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا ممکن است زیستن از خلق بی نیاز</p></div>
<div class="m2"><p>راضی چرا به ننگ تمنا شود کسی؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا سر توان نهاد به زانوی خود، چرا</p></div>
<div class="m2"><p>منت پذیر بالش خارا شود کسی؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>می بایدش به خون جگر خورد غوطه ها</p></div>
<div class="m2"><p>تا از غبار جسم مصفا شود کسی؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مژگان هنوز داد تماشا نداده است</p></div>
<div class="m2"><p>آن فرصت از کجاست که بینا شود کسی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یک اهل درد نیست به درد سخن رسد</p></div>
<div class="m2"><p>خونش به گردن است که گویا شود کسی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صائب بس است فکر خط و خال گلرخان</p></div>
<div class="m2"><p>تا کی سیاه خیمه سودا شود کسی؟</p></div></div>