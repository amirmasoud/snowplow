---
title: >-
    غزل شمارهٔ ۲۶۹۸
---
# غزل شمارهٔ ۲۶۹۸

<div class="b" id="bn1"><div class="m1"><p>گر شکر در جام ریزم زهر قاتل می‌شود</p></div>
<div class="m2"><p>چون صدف گر آب نوشم عقده دل می‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون سکندر می‌خورد آیینهٔ عمرش به سنگ</p></div>
<div class="m2"><p>از خضر یک آب خوردن هرکه غافل می‌شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جامه بر تن کعبه را مجنون ما خواهد درید</p></div>
<div class="m2"><p>کی ز سنگ کودکان دیوانه عاقل می‌شود؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زیر هر برگ گلی صد نیش خار آماده است</p></div>
<div class="m2"><p>با تن آسانی مکن عادت که مشکل می‌شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قطره اشکم اگر از دل چنین چیند غبار</p></div>
<div class="m2"><p>تا سر مژگان رسیدن مهره گل می‌شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان نخواهد برد صائب آفتاب از آه ما</p></div>
<div class="m2"><p>وای بر شمعی که با صرصر مقابل می‌شود</p></div></div>