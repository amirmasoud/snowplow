---
title: >-
    غزل شمارهٔ ۵۶۶۹
---
# غزل شمارهٔ ۵۶۶۹

<div class="b" id="bn1"><div class="m1"><p>از غم زلف تو در دام بلا افتادیم</p></div>
<div class="m2"><p>چه هوا در سر ما بود و کجا افتادیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوی پیراهن مصریم که از بی قیدی</p></div>
<div class="m2"><p>در گریبان گل و جیب صبا افتادیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پوست بر پیکر ارباب جنون زندان است</p></div>
<div class="m2"><p>سستی ماست که در بند قبا افتادیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچنان منتظر سرزنش خار و خسیم</p></div>
<div class="m2"><p>گر چه چون آبله در هر ته پا افتادیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خضر توفیق بود تشنه تنها گردان</p></div>
<div class="m2"><p>بی سبب در عقب راهنما افتادیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تکیه بر عقل مکن پیش زنخدان بتان</p></div>
<div class="m2"><p>که درین چاه مکرر به عصا افتادیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>موجه سبزه زنگار گذشت از سر ما</p></div>
<div class="m2"><p>تا از آن آینه رخسار جدا افتادیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب افسانه زلفش به جنون انجامید</p></div>
<div class="m2"><p>در کجا بود حکایت، به کجا افتادیم</p></div></div>