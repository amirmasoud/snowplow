---
title: >-
    غزل شمارهٔ ۳۷۲۷
---
# غزل شمارهٔ ۳۷۲۷

<div class="b" id="bn1"><div class="m1"><p>چه وسعت است که این بحر پرگهر دارد</p></div>
<div class="m2"><p>که هر حباب در او عالم دگر دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین محیط به هر موجه ای که می پیچم</p></div>
<div class="m2"><p>دل رمیده ای از ریگ تشنه تر دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه حکمت است که آسوده تر بود در راه</p></div>
<div class="m2"><p>ز دوش راهروان هر که بار بر دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بغیر دل که دو عالم بود به فرمانش</p></div>
<div class="m2"><p>کدام خوشه درین خاکدان دو سر دارد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همیشه خازن شهدست از حلاوت عیش</p></div>
<div class="m2"><p>کسی که خانه چو زنبور مختصر دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به اشک تاک دل باغبان نمی سوزد</p></div>
<div class="m2"><p>سرشک ما به دل چرخ کی اثر دارد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نصیب خاک نشینان بود حلاوت عیش</p></div>
<div class="m2"><p>درین مقام نی بوریا شکر دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز گرد تا نفتاده است آسیای فلک</p></div>
<div class="m2"><p>چرا کسی ز غم رزق دیده تر دارد؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به جانبی رود از شوق هر نفس دل ما</p></div>
<div class="m2"><p>در آشیانه ما بیضه بال و پر دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در آن محیط که باد مراد تسلیم است</p></div>
<div class="m2"><p>سفینه از نفس ناخدا خطر دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو گوش چون صدف از سنگ کرده ای، ورنه</p></div>
<div class="m2"><p>زبان موج خبرها ازان گهر دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به داغ عشق منه دل که این ستاره شوخ</p></div>
<div class="m2"><p>به هر تجلی خود مشرق دگر دارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به طوف کعبه رسیدن گذشتن است از خود</p></div>
<div class="m2"><p>خوشا کسی که سرو برگ این سفر دارد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به گرد چشم تو آب حیا نمی گردد</p></div>
<div class="m2"><p>درین زمانه که آیینه چشم تر دارد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مده به اهل سخن عرض، فکر خامی را</p></div>
<div class="m2"><p>که همچو طفل خرابات صد پدر دارد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دل تو قابل تأثیر فکر صائب نیست</p></div>
<div class="m2"><p>وگرنه ناله ما شعله اثر دارد</p></div></div>