---
title: >-
    غزل شمارهٔ ۳۱۵۹
---
# غزل شمارهٔ ۳۱۵۹

<div class="b" id="bn1"><div class="m1"><p>زگرمی خون من جوهر به تیغ او بسوزاند</p></div>
<div class="m2"><p>فروغ لاله من آب را در جو بسوزاند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل آن طالع کجا دارد کز آن رخسار گل چیند؟</p></div>
<div class="m2"><p>مگر دلهای شب داغی به یاد او بسوزاند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میسر نیست از دنیا گذشتن هر سبکرو را</p></div>
<div class="m2"><p>که این صحرا نفس در سینه آهو بسوزاند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به تیغ خویش رحمی کن نداری رحم اگر برمن</p></div>
<div class="m2"><p>که جوهر را زگرمی خون من چون مو بسوزاند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به داغ ناامیدی خرمن خورشید می سوزد</p></div>
<div class="m2"><p>کجا مشت خس و خار مرا آن رو بسوزاند؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نگردد آب از سنگین دلی در حلقه چشمش</p></div>
<div class="m2"><p>دو عالم را اگر برق نگاه او بسوزاند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پس از مردن به خاک من گل افشاندن به آن ماند</p></div>
<div class="m2"><p>که با صندل عزیز خویش را هندو بسوزاند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زدود عنبرینش بوی ریحان بهشت آید</p></div>
<div class="m2"><p>سپندی را که صائب آتش آن رو بسوزاند</p></div></div>