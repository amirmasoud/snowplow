---
title: >-
    غزل شمارهٔ ۵۵۵۶
---
# غزل شمارهٔ ۵۵۵۶

<div class="b" id="bn1"><div class="m1"><p>ز جام بیخودی چون لاله مست از خاک بر خیزم</p></div>
<div class="m2"><p>ز مهد غنچه چون گل با دل صد چاک بر خیزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه سروم کز رعونت سبزه را در زیر پامانم</p></div>
<div class="m2"><p>چمن از خاک برخیزد چو من از خاک برخیزم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا هر شمع چو پروانه از جا در نمی آرد</p></div>
<div class="m2"><p>مگر از جا به ذوق شعله ادراک برخیزم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا زافسردگی در تنگنای سنگ مردن به</p></div>
<div class="m2"><p>که چون آتش به امداد خس و خاشاک برخیزم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو شبنم کرده ام گردآوری خود را درین گلشن</p></div>
<div class="m2"><p>به اندک جذبه ای از هستی خود پاک برخیزم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا با خاکساریهاست پیوندی درین گلشن</p></div>
<div class="m2"><p>که می پیچم به خود تا از زمین چون تاک برخیزم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شلاین تر ز خون ناحقم در هر چه آویزم</p></div>
<div class="m2"><p>نه گردم کز بر افشاندن از آن فتراک برخیزم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نخوابیده است با کین کسی هرگز دل صافم</p></div>
<div class="m2"><p>ز بستر چون دعا از سینه های پاک برخیزم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل بی غم زمین شوره باشد تخم پاکم را</p></div>
<div class="m2"><p>بسامان همچو آه از سینه غمناک برخیزم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه زنگم کز گرانجانی به خاطرها گران باشم</p></div>
<div class="m2"><p>سبک چون عکس از آیینه ادراک برخیزم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من آن ابرم که در چشم گهرها آب نگذارم</p></div>
<div class="m2"><p>ز هر بحری که با این دیده نمناک برخیزم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز رشک بیقراران سوختم کو آتشین رویی</p></div>
<div class="m2"><p>که من هم چون سپند از جای خود چالاک برخیزم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرا از گوشه خلوت مخوان در مجلس عشرت</p></div>
<div class="m2"><p>چه افتاده است بنشینم خجل غمناک برخیزم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرا چون سبزه زیر سنگ دارد آسمان صائب</p></div>
<div class="m2"><p>شوم سر وی اگر از سایه افلاک برخیزم</p></div></div>