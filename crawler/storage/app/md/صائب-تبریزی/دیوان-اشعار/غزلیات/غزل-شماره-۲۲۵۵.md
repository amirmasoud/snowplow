---
title: >-
    غزل شمارهٔ ۲۲۵۵
---
# غزل شمارهٔ ۲۲۵۵

<div class="b" id="bn1"><div class="m1"><p>حلقه آه مرا سپهر نگین است</p></div>
<div class="m2"><p>گریه من روشناس روی زمین است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا تو به چشم رکاب پای نهادی</p></div>
<div class="m2"><p>عشرت روی زمین به خانه زین است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رزق من از شاهراه گوش درآید</p></div>
<div class="m2"><p>روزی من چون صدف ز در ثمین است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر چه بکاری، همان نصیب تو گردد</p></div>
<div class="m2"><p>دانه خود پاک کن که خاک امین است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همت سرشار، سرو عالم بالاست</p></div>
<div class="m2"><p>وسعت مشرب بهشت روی زمین است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاطر خرم دگر کسی ز که جوید؟</p></div>
<div class="m2"><p>صبح در ایام ما گرفته جبین است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مقصد کوته نظر، بلند نباشد</p></div>
<div class="m2"><p>منزل دور رکاب، خانه زین است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیهده صائب مکن ز بخت شکایت</p></div>
<div class="m2"><p>چشمه حیوان سیاه خانه نشین است</p></div></div>