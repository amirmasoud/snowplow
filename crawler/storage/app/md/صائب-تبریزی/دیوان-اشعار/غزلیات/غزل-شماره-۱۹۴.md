---
title: >-
    غزل شمارهٔ ۱۹۴
---
# غزل شمارهٔ ۱۹۴

<div class="b" id="bn1"><div class="m1"><p>کاسه زانوست جام جم دل آگاه را</p></div>
<div class="m2"><p>یوسف از روی زمین خوش تر شمارد چاه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از غبار خط مشکین حسن می بالد به خود</p></div>
<div class="m2"><p>گرد لشکر توتیای چشم باشد شاه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می نماید حسن در آغوش عاشق خویش را</p></div>
<div class="m2"><p>در کنار هاله باشد حسن دیگر ماه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اهل غیرت نیست ممکن بازی دنیا خورد</p></div>
<div class="m2"><p>شیر چون گردن گذارد حیله روباه را؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که را همواری بدباطنان از راه برد</p></div>
<div class="m2"><p>سیل بی زنهار داند آب زیر کاه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راستی از کج نهادان گرد برمی آورد</p></div>
<div class="m2"><p>از زدن مانع نگردد تیغ رهزن راه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست در عقل متین دست تصرف باده را</p></div>
<div class="m2"><p>می کند آگاه تر مستی دل آگاه را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواب می سوزد به چشم عارفان شکر وصول</p></div>
<div class="m2"><p>نیست آرام از رسیدن طالب الله را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ابر نتواند گرفتن رخنه جستن به برق</p></div>
<div class="m2"><p>مهر خاموشی نگیرد پیش راه آه را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کوته اندیشی است کردن شکوه از بخت سیاه</p></div>
<div class="m2"><p>روز رعنا در قفا باشد شب کوتاه را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آدمی را نقش کم ز آفت سپرداری کند</p></div>
<div class="m2"><p>چشم بد بسیار باشد نقش خاطرخواه را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پاک خواهد کرد از اشک ندامت راه خویش</p></div>
<div class="m2"><p>ابراز بی آبرویی گر بپوشد ماه را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تشنه تر گردند از نعمت تهی چشمان حرص</p></div>
<div class="m2"><p>آب هیهات است سازد سیر، چشم چاه را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صبر درد بی دوا را عاقبت درمان کند</p></div>
<div class="m2"><p>ناامیدی خضر ره شد رهرو گمراه را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برنیاید شعله را از سر هوای سرکشی</p></div>
<div class="m2"><p>نفس چون از دل برآرد ریشه حب جاه را؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فربهی از خوان مردم رنج باریک آورد</p></div>
<div class="m2"><p>کرد نور عاریت آخر هلالی ماه را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ترک دعوی می نماید پایه معنی بلند</p></div>
<div class="m2"><p>جامه کوتاه، رعنا می کند کوتاه را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شد جهان پر شور صائب از صریر کلک من</p></div>
<div class="m2"><p>بلبل از من یاد دارد ناله جانکاه را</p></div></div>