---
title: >-
    غزل شمارهٔ ۱۰۰۷
---
# غزل شمارهٔ ۱۰۰۷

<div class="b" id="bn1"><div class="m1"><p>گر چه در دفع کدورت هر نوایی دلکش است</p></div>
<div class="m2"><p>در میان سازها، نی تیر روی ترکش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر برآرد عشق دود از عقل، جای رحم نیست</p></div>
<div class="m2"><p>خانه زنبور کافر مستحق آتش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رزق خاموشان شود اکثر معانی لطیف</p></div>
<div class="m2"><p>کوزه سربسته را قسمت شراب بی غش است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچ رنگی نیست در آتش نباشد نعل او</p></div>
<div class="m2"><p>در میان رنگ ها زردی طلای بی غش است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حسن چون مستور باشد عشق زندانی بود</p></div>
<div class="m2"><p>عشق عالمسوز گردد یار چون لولی وش است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روز دعوی در صف زورین کمانان سخن</p></div>
<div class="m2"><p>مصرع برجسته صائب تیر روی ترکش است</p></div></div>