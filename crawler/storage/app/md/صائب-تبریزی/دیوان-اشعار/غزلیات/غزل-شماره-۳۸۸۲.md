---
title: >-
    غزل شمارهٔ ۳۸۸۲
---
# غزل شمارهٔ ۳۸۸۲

<div class="b" id="bn1"><div class="m1"><p>چنان که حسن ترا هیچ کس نمی‌داند</p></div>
<div class="m2"><p>ز عشق حال مرا هیچ کس نمی‌داند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترا ز اهل وفا هیچ کس نمی‌داند</p></div>
<div class="m2"><p>مرا سزای جفا هیچ کس نمی‌داند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به جز دلت که زبان با دلم یکی دارد</p></div>
<div class="m2"><p>عیار شوق مرا هیچ کس نمی‌داند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگرچه جوهریان عزیز دارد مصر</p></div>
<div class="m2"><p>بهای یوسف ما هیچ کس نمی‌داند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز خاکمال یتیمی گهر نگردد خوار</p></div>
<div class="m2"><p>چه شد که قدر وفا هیچ کس نمی‌داند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به غیر من که درین بوته‌ها گداخته‌ام</p></div>
<div class="m2"><p>عیار شرم و حیا هیچ کس نمی‌داند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زبان غنچه پیچیده را درین گلزار</p></div>
<div class="m2"><p>به جز نسیم صبا هیچ کس نمی‌داند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو عاجزان سپرانداز پیش مژگانش</p></div>
<div class="m2"><p>که دفع تیر قضا هیچ کس نمی‌داند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کلید مخزن اسرار غیب در غیب است</p></div>
<div class="m2"><p>دهان تنگ ترا هیچ کس نمی‌داند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درین بساط زبان شکسته‌دل را</p></div>
<div class="m2"><p>به غیر زلف دوتا هیچ کس نمی‌داند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حجاب نیست در بسته عیب‌جویان را</p></div>
<div class="m2"><p>بخیل را چو گدا هیچ کس نمی‌داند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز وعده تو گره‌ها که در دل است مرا</p></div>
<div class="m2"><p>به غیر بند قبا هیچ کس نمی‌داند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز بس یگانه شدم با جهان ز یکرنگی</p></div>
<div class="m2"><p>مرا ز خویش جدا هیچ کس نمی‌داند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قماش دست بلورین و پای سیمین را</p></div>
<div class="m2"><p>به جز نگار و حنا هیچ کس نمی‌داند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو موجه‌ای که به دریا بی‌کنار افتد</p></div>
<div class="m2"><p>قرارگاه مرا هیچ کس نمی‌داند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگرچه خانه آیینه است روی زمین</p></div>
<div class="m2"><p>نفس کشیدن ما هیچ کس نمی‌داند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به غیر نرگس بیمار گلرخان صائب</p></div>
<div class="m2"><p>علاج درد مرا هیچ کس نمی‌داند</p></div></div>