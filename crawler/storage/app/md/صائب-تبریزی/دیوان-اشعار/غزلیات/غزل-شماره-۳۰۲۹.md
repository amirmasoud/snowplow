---
title: >-
    غزل شمارهٔ ۳۰۲۹
---
# غزل شمارهٔ ۳۰۲۹

<div class="b" id="bn1"><div class="m1"><p>غبار تیره بختی از دهان شکوه می خیزد</p></div>
<div class="m2"><p>به قدر شق سیاهی از زبان خامه می ریزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر آن عاشق سرشک شمع آب زندگی گردد</p></div>
<div class="m2"><p>که چون پروانه بیباک از آتش نپرهیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همان سرگشته چون موج سرایم در بیابانها</p></div>
<div class="m2"><p>به جای سبزه خضر از رهگذر من اگر خیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امید دستگیری دارم از رهبر در آن وادی</p></div>
<div class="m2"><p>که خار از سرکشی در دامن رهرو نیاویزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غرور زهد آن روز از سر زاهد رود بیرون</p></div>
<div class="m2"><p>که از اشک ندامت آب بر دست سبو ریزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زشرم آن تبسمهای شرم آلود جا دارد</p></div>
<div class="m2"><p>که شکر خند گل در آستین غنچه بگریزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز آه آتشین در پرده دل می زنم آتش</p></div>
<div class="m2"><p>چو بینم شمع در بال و پر پروانه آمیزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نظر بر صبح دارد گریه شبخیز من صائب</p></div>
<div class="m2"><p>که انجم تخم خود را در زمین پاک می ریزد</p></div></div>