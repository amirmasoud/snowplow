---
title: >-
    غزل شمارهٔ ۳۴۱۶
---
# غزل شمارهٔ ۳۴۱۶

<div class="b" id="bn1"><div class="m1"><p>تا کیم نوحه به گوش از دل ناشاد رسد؟</p></div>
<div class="m2"><p>تا نظر باز کنم ناوک بیداد رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر هر گره زلف تو لرزم که مباد</p></div>
<div class="m2"><p>دست خشکیده ای از جانب شمشاد رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می شود دل نشود مضطرب از آمدنت؟</p></div>
<div class="m2"><p>دست و پا گم نکند صید چو صیاد رسد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جگرم تازه نشد از گهرافشانی ابر</p></div>
<div class="m2"><p>مگر این سوخته را برق به فریاد رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مور شو مور، که حکمت به سلیمان گذرد</p></div>
<div class="m2"><p>گر به صحرای تو با لشکر ایجاد رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نرود کاوش رشک از دل عاشق که هنوز</p></div>
<div class="m2"><p>ناله تیشه به گوش از دل فرهاد رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سر قسمت تیغ و قلم آید چو قضا</p></div>
<div class="m2"><p>منشی ظلم ترا خامه فولاد رسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رو به دل بردن صائب چو کند چهره او</p></div>
<div class="m2"><p>از دو جانب سپه زلف به امداد رسد</p></div></div>