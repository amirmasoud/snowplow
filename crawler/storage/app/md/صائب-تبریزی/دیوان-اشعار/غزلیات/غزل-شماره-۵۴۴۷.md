---
title: >-
    غزل شمارهٔ ۵۴۴۷
---
# غزل شمارهٔ ۵۴۴۷

<div class="b" id="bn1"><div class="m1"><p>ما دماغ خشک را از باده گلشن کرده‌ایم</p></div>
<div class="m2"><p>بارها این شمع را از آب روشن کرده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکه را دیدیم دارد حاصلی از عمر و ما</p></div>
<div class="m2"><p>عقده مشکل به جای دانه خرمن کرده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیگران را دست بر دل نه، که ما سوداییان</p></div>
<div class="m2"><p>با دل سرگشته عادت چون فلاخن کرده‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رتبه ما خاکساران را به چشم کم مبین</p></div>
<div class="m2"><p>خاک‌ها ز افتادگی در چشم دشمن کرده‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جوهر شمشیر را در پیچ و تاب آورده است</p></div>
<div class="m2"><p>جامه فتحی که ما از زخم بر تن کرده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسن گل عالم‌فروز از شعله آواز ماست</p></div>
<div class="m2"><p>این نهال خشک را ما نخل ایمن کرده‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بخیه را چون محرم زخم نهان خود کنیم؟</p></div>
<div class="m2"><p>ما که از غیرت نمک در چشم سوزن کرده‌ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون نسازد ذوق صید ما قفس را سینه‌چاک؟</p></div>
<div class="m2"><p>بیضه را چون خلوت خلوت فانوس روشن کرده‌ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از فروغ داغ ما چشم سمندر خیره است</p></div>
<div class="m2"><p>ما چراغ خود ز روی گرم روشن کرده‌ایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صائب از گوش گران باغبانان فارغیم</p></div>
<div class="m2"><p>ما که از افکار رنگین طرح گلشن کرده‌ایم</p></div></div>