---
title: >-
    غزل شمارهٔ ۶۰۳۶
---
# غزل شمارهٔ ۶۰۳۶

<div class="b" id="bn1"><div class="m1"><p>چند حرف آب و نان چون مردم غافل زدن؟</p></div>
<div class="m2"><p>تا به کی بر رخنه دیوار زندان گل زدن؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست جز تسلیم لنگر عالم پر شور را</p></div>
<div class="m2"><p>دست و پا پوچ است در دریای بی ساحل زدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از تن خاکی به مردی گرد چون مجنون برآر</p></div>
<div class="m2"><p>تا توانی دست خود بر دامن محمل زدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می شود چون رشته اشک از گره مطلق عنان</p></div>
<div class="m2"><p>رشته امید ما را عقده مشکل زدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حاصل سنگ از درخت بی ثمر بار دل است</p></div>
<div class="m2"><p>از تهی مغزی است حرف سخت با مدخل زدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست مانع از تردد وصل دریا سیل را</p></div>
<div class="m2"><p>قطره بیش از راه می باید درین منزل زدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر مشتی خون که رزق خاک گردد عاقبت</p></div>
<div class="m2"><p>دست، بی شرمی بود بر دامن قاتل زدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سبزه خوابیده را سهل است کردن پایمال</p></div>
<div class="m2"><p>نیست از مردی به قلب دشمنان غافل زدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر به رعنایی فشاند دامن، آزادست سرو</p></div>
<div class="m2"><p>ورنه آسان است پشت پای بر حاصل زدن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بحر را صائب نگردد مانع جوش و خروش</p></div>
<div class="m2"><p>از خس و خاشاک سوزن بر لب ساحل زدن</p></div></div>