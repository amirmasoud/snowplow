---
title: >-
    غزل شمارهٔ ۲۲۱۳
---
# غزل شمارهٔ ۲۲۱۳

<div class="b" id="bn1"><div class="m1"><p>عشق سلطان و عقل دهقان است</p></div>
<div class="m2"><p>رزق دهقان ز عدل سلطان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق چون آفتاب کنعانی</p></div>
<div class="m2"><p>عقل ده گانه همچو اخوان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کیست گردن ز حکم عشق کشد؟</p></div>
<div class="m2"><p>عشق انگشتر سلیمان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ظلمات سواد هستی را</p></div>
<div class="m2"><p>مشرب عشق، آب حیوان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عقل در بارگاه حضرت عشق</p></div>
<div class="m2"><p>منفعل چون نخوانده مهمان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عقل مرغی است در قفس محبوس</p></div>
<div class="m2"><p>عشق سیمرغ قاف امکان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقل فرمان پذیر تکلیف است</p></div>
<div class="m2"><p>عشق سیلاب کفر و ایمان است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آسمان سفره ای است پر نعمت</p></div>
<div class="m2"><p>عشق آن سفره را نمکدان است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اقتدا تا به مولوی کرده است</p></div>
<div class="m2"><p>شعر صائب تمام عرفان است</p></div></div>