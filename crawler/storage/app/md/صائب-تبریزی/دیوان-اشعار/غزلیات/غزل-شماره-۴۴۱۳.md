---
title: >-
    غزل شمارهٔ ۴۴۱۳
---
# غزل شمارهٔ ۴۴۱۳

<div class="b" id="bn1"><div class="m1"><p>آنها که به فردوس رخ یار فروشند</p></div>
<div class="m2"><p>از سادگی آیینه به زنگار فروشند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گنج دو جهان قیمت یک چشم زدن نیست</p></div>
<div class="m2"><p>گر زان که به زر لذت دیدار فروشند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درد دل بیمار به هرکس نتوان گفت</p></div>
<div class="m2"><p>این جنس گران را به پرستار فروشند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سازند عیان محضر بی‌مغزی خود را</p></div>
<div class="m2"><p>جمعی که به هم طره دستار فروشند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی‌زرق و ریا نیست نماز شب زاهد</p></div>
<div class="m2"><p>معیوب بود هرچه شب تار فروشند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون یوسف از امداد خسیسان مرو از راه</p></div>
<div class="m2"><p>کز چاه برآرند و به بازار فروشند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مفروش دلی را چو خریدی به دو عالم</p></div>
<div class="m2"><p>کاین نیست متاعی که به بازار فروشند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پروانه سبق برد ز بلبل به خموشی</p></div>
<div class="m2"><p>حیف است که کردار به گفتار فروشند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی‌مغز گروهی که به آشفته‌دماغان</p></div>
<div class="m2"><p>چون صبح پریشانی دستار فروشند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صائب مگشا لب که به بازار خموشان</p></div>
<div class="m2"><p>در جیب صدف گوهر شهوار فروشند</p></div></div>