---
title: >-
    غزل شمارهٔ ۲۶۱۴
---
# غزل شمارهٔ ۲۶۱۴

<div class="b" id="bn1"><div class="m1"><p>ای خط بیرحم ازان عارض دمیدن زود بود</p></div>
<div class="m2"><p>آن گل نشکفته را نادیده چیدن زود بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشت امید مرا می داشت شرمش تازه رو</p></div>
<div class="m2"><p>خون لعل آبدارش را مکیدن زود بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلف مشکین بود از دیوان رحمت آیتی</p></div>
<div class="m2"><p>بر سر او بی تأمل خط کشیدن زود بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست بیداد سیه مستان بلند افتاده است</p></div>
<div class="m2"><p>ورنه آن سیب زنخدان را مکیدن زود بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم او را فرصت نظاره می بایست داد</p></div>
<div class="m2"><p>نرگس این باغ را در خواب چیدن زود بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داشت تسخیر هزاران ملک دل را در نظر</p></div>
<div class="m2"><p>چشم او را زهر ناکامی چشیدن زود بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با دل صدپاره عشاق چندین کار داشت</p></div>
<div class="m2"><p>شوخی مژگان او را آرمیدن زود بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر گلستانی که از صد گل یکی نشکفته است</p></div>
<div class="m2"><p>چون خزان افسون بیرحمی دمیدن زود بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از سر رغبت به حرف دادخواهان می رسید</p></div>
<div class="m2"><p>حلقه انصاف در گوشش کشیدن زود بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر سر آن غمزه خونخوار در عین غرور</p></div>
<div class="m2"><p>چون بلای آسمان غافل رسیدن زود بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در زمین سینه ها تخم محبت می فشاند</p></div>
<div class="m2"><p>خال او را در پناه خط خزیدن زود بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خط ظالم برد از حد دل سیاهی را برون</p></div>
<div class="m2"><p>ورنه صائب از دل وحشی رمیدن زود بود</p></div></div>