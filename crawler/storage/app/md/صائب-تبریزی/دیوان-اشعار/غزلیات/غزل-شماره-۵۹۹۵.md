---
title: >-
    غزل شمارهٔ ۵۹۹۵
---
# غزل شمارهٔ ۵۹۹۵

<div class="b" id="bn1"><div class="m1"><p>چند چون خامان نظر بر ماهتاب انداختن؟</p></div>
<div class="m2"><p>تا کی این مشت نمک در چشم خواب انداختن؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه از من خامتر صیدی ندارد کوی عشق</p></div>
<div class="m2"><p>می توان در سینه گرمم کباب انداختن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس که از خواب پریشان چشم من ترسیده است</p></div>
<div class="m2"><p>چشم نتوانم به چشم نیمخواب انداختن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نیندازد، ستم بر نوبهار خود کند</p></div>
<div class="m2"><p>در خزان هر کس که بتواند شراب انداختن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در میان دلبران از چشم پر کار تو ماند</p></div>
<div class="m2"><p>دل ز مردم بردن و خود را به خواب انداختن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قطره ناچیز را دریای گوهر کردن است</p></div>
<div class="m2"><p>سر چو شبنم در کنار آفتاب انداختن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشرت ده روزه را عیش مخلد کردن است</p></div>
<div class="m2"><p>مهر گل از دور بینی بر گلاب انداختن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیش من خوشتر بود از منت آب حیات</p></div>
<div class="m2"><p>تشنه لب خود را به دریای سراب انداختن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دانه در صحرای پر آتش پریشان کردن است</p></div>
<div class="m2"><p>در زمین شور، گوهر چون سحاب انداختن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قلب روی اندود خود را سیم خالص کردن است</p></div>
<div class="m2"><p>نور بر آب روان چون ماهتاب انداختن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به که صائب بر ندارد چشم از رخسار ماه</p></div>
<div class="m2"><p>هر که نتواند نظر بر آفتاب انداختن</p></div></div>