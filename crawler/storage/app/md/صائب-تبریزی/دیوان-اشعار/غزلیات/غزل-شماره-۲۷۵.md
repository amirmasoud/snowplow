---
title: >-
    غزل شمارهٔ ۲۷۵
---
# غزل شمارهٔ ۲۷۵

<div class="b" id="bn1"><div class="m1"><p>گر چه ما را نیست بر روی زمین ویرانه ای</p></div>
<div class="m2"><p>خانه ها چون گنج در زیر زمین داریم ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از گریبان گل بی خار اگر سر بر زنیم</p></div>
<div class="m2"><p>خار در چشم از نگاه دوربین داریم ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نوخطی پیوسته ما را هست در مد نظر</p></div>
<div class="m2"><p>بر جگر دایم خراشی چون نگین داریم ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست غیر از نقش پای دشت پیمایان عشق</p></div>
<div class="m2"><p>آشنارویی که در روی زمین داریم ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان نثار طلعت خورشیدرویان می کنیم</p></div>
<div class="m2"><p>تا نفس بر لب چو صبح راستین داریم ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون به سیر لامکان از خویشتن راضی شویم؟</p></div>
<div class="m2"><p>همچو همت، توسنی در زیر زین داریم ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صاحب نامند از ما عالم و ما تیره روز</p></div>
<div class="m2"><p>طالع برگشته نقش نگین داریم ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دورباش نقطه وحدت عنان تاب دل است</p></div>
<div class="m2"><p>ورنه چون پرگار پایی آهنین داریم ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست صائب دست بر ما خاکمال چرخ را</p></div>
<div class="m2"><p>تا غبار خاکساری بر جبین داریم ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیش خرمن دست کی چون خوشه چین داریم ما؟</p></div>
<div class="m2"><p>تنگدستی را نهان در آستین داریم ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نان جو بر سفره ما گر نباشد، گو مباش</p></div>
<div class="m2"><p>نعمتی همچون زبان گندمین داریم ما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چین پیشانی بود شیرازه اوراق دل</p></div>
<div class="m2"><p>پاس دل چون غنچه از چین جبین داریم ما</p></div></div>