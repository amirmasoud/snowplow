---
title: >-
    غزل شمارهٔ ۲۲۸۹
---
# غزل شمارهٔ ۲۲۸۹

<div class="b" id="bn1"><div class="m1"><p>نمک به دیده غفلت کن از سفیده صبح</p></div>
<div class="m2"><p>که صد کتاب سخن هست در جریده صبح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما ز جامه احرام را کفن زنهار</p></div>
<div class="m2"><p>مشو چو مرده دلان غافل از سفیده صبح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازان سفینه خورشید آسمان سیرست</p></div>
<div class="m2"><p>که بادبان کند از پرده های دیده صبح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو آفتاب بود گرم، نان راهروی</p></div>
<div class="m2"><p>که روزیش بود از سفره کشیده صبح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیاض سینه روشندلان رقم سوزست</p></div>
<div class="m2"><p>ستاره نقطه سهوست بر جریده صبح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به سوزن مژه آفتاب هیهات است</p></div>
<div class="m2"><p>رفوپذیر شود سینه دریده صبح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا که با دل شب راز در میان دارم</p></div>
<div class="m2"><p>چه دل گشاده شود صائب از سفیده صبح؟</p></div></div>