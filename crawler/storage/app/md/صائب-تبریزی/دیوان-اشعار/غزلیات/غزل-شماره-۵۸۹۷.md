---
title: >-
    غزل شمارهٔ ۵۸۹۷
---
# غزل شمارهٔ ۵۸۹۷

<div class="b" id="bn1"><div class="m1"><p>روشن ز فروغ می ناب است حیاتم</p></div>
<div class="m2"><p>چون آتش یاقوت ز آب است حیاتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از کسب هوا نقش بر آب است حیاتم</p></div>
<div class="m2"><p>یک چشم زدن همچو حباب است حیاتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از رعشه عنان رگ جان رفته ز دستم</p></div>
<div class="m2"><p>وز قامت خم پا به رکاب است حیاتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از شور جزا شد جگر خاک نمکسود</p></div>
<div class="m2"><p>وز جهل همان مست و خراب است حیاتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با آن که سیه کرده در او نامه اعمال</p></div>
<div class="m2"><p>در حسرت ایام شباب است حیاتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشکل که دهد فرصت برداشتن زاد</p></div>
<div class="m2"><p>زین گونه که سرگرم شتاب است حیاتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر چند شوم پیر، شود غفلت من بیش</p></div>
<div class="m2"><p>افسانه شیرینی خواب است حیاتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از کهنگی افزون شودش مستی غفلت</p></div>
<div class="m2"><p>در شیشه تن همچو شراب است حیاتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از گریه من چون جگر سنگ نسوزد؟</p></div>
<div class="m2"><p>کز گرمی رفتار کباب است حیاتم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از ساده دلی ریشه کند در جگر خاک</p></div>
<div class="m2"><p>هر چند که چون نقش بر آب است حیاتم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در دیده کوته نظران گر چه بلندست</p></div>
<div class="m2"><p>کوتاهتر از مد شهاب است حیاتم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فریاد که در رفتن ازین پیکر خاکی</p></div>
<div class="m2"><p>بیتاب تر از موج سراب است حیاتم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جایی که بود عمر خضر نقش بر آبی</p></div>
<div class="m2"><p>صائب چو شرر درچه حساب است حیاتم؟</p></div></div>