---
title: >-
    غزل شمارهٔ ۱۲۱۹
---
# غزل شمارهٔ ۱۲۱۹

<div class="b" id="bn1"><div class="m1"><p>سرنزد از بلبلم هر چند دستانی درست</p></div>
<div class="m2"><p>ناله ام نگذاشت در گلشن گریبانی درست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه دایم در شکستم بود چشم شور خلق</p></div>
<div class="m2"><p>شور من نگذاشت در عالم نمکدانی درست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلبل از آوازه عالم را گلستان کرده بود</p></div>
<div class="m2"><p>تا گل خونین جگر می کرد دیوانی درست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آه ازین گردون کم فرصت که با این دستگاه</p></div>
<div class="m2"><p>در ضیافت خانه اش ننشست مهمانی درست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کیستم من تا نگیرد خار تهمت دامنم؟</p></div>
<div class="m2"><p>قسمت یوسف نشد زین بزم دامانی درست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با وجود بی وفایی بر سرش جا می دهند</p></div>
<div class="m2"><p>آه اگر می بود گل را عهد و پیمانی درست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آه نتوانست قامت راست کردن در دلم</p></div>
<div class="m2"><p>برنیامد زین گلستان شاخ ریحانی درست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عهد ما گر سست با قید و صلاح افتاده است</p></div>
<div class="m2"><p>با شکستن توبه ما راست پیمانی درست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>محمل گل همچو شبنم گشت غایب از نظر</p></div>
<div class="m2"><p>بلبل آتش نفس تا کرد دستانی درست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ماه عالمتاب خود را بارها در هم شکست</p></div>
<div class="m2"><p>تا شبی زین گرد خوان شد قسمتش نانی درست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چشم شوخش بیضه اسلام را بر سنگ زد</p></div>
<div class="m2"><p>زلف کافر کیش او نگذاشت ایمانی درست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با درشتان چرب نرمی کن که برمی آورد</p></div>
<div class="m2"><p>گل به همواری ز چنگ خار دامانی درست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لاف همت می رسد گل را که در صحن چمن</p></div>
<div class="m2"><p>پیش هر خاری گذارد بر زمین خوانی درست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از نگاه شور چشمان اشتهایش سوخته است</p></div>
<div class="m2"><p>هر که را چون لاله باشد در بغل نانی درست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نیست صائب بر تنم چون زلف مویی بی شکست</p></div>
<div class="m2"><p>در بساط من باشد غیر پیمانی درست</p></div></div>