---
title: >-
    غزل شمارهٔ ۲۳۴۱
---
# غزل شمارهٔ ۲۳۴۱

<div class="b" id="bn1"><div class="m1"><p>سر گردان چند از من آن سرو خرامان بگذرد</p></div>
<div class="m2"><p>از غبار هستی من دامن افشان بگذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل زمن می گیرد و روی دلش با دیگری است</p></div>
<div class="m2"><p>اینچنین ظلمی مگر در کافرستان بگذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرکز پرگار گردون گردد از آسودگی</p></div>
<div class="m2"><p>هر سر آزاده ای کز فکر سامان بگذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر بر آرد از گریبان حیات جاودان</p></div>
<div class="m2"><p>هر که زیر تیغ جانان از سر جان بگذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با دو صد امید خاک راه او گردیده ام</p></div>
<div class="m2"><p>آه اگر از خاک من برچیده دامان بگذرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون من حیران توانم از تماشایش گذشت؟</p></div>
<div class="m2"><p>آب نتوانست از آن سرو خرامان بگذرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در حریم وصل از نومیدی من آگه است</p></div>
<div class="m2"><p>با دهان خشک هر کس زآب حیوان بگذرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در برون باغ بوی گل مرا دیوانه کرد</p></div>
<div class="m2"><p>تا چها بر بلبل از قرب گلستان بگذرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در زمین پاک صائب قطره گوهر می شود</p></div>
<div class="m2"><p>از صدف ظلم است خشک آن ابر نیسان بگذرد</p></div></div>