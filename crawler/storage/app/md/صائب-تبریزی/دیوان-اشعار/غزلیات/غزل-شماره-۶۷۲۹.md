---
title: >-
    غزل شمارهٔ ۶۷۲۹
---
# غزل شمارهٔ ۶۷۲۹

<div class="b" id="bn1"><div class="m1"><p>پشت پا زن بر دو عالم تا فلک پیما شوی</p></div>
<div class="m2"><p>از سر دنیا و دین برخیز تا رعنا شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد حباب از خودنمایی گوی چوگان فنا</p></div>
<div class="m2"><p>سعی کن تا در محیط عشق ناپیدا شوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طوطی از خاموشی آیینه می آید به حرف</p></div>
<div class="m2"><p>مهر خاموشی به لب زن تا به دل گویا شوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بینش ظاهر غبار دیده باطن بود</p></div>
<div class="m2"><p>خاک زن در چشم ظاهر تا به جان بینا شوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غور کن در بحر هستی تا گهر آری به کف</p></div>
<div class="m2"><p>ورنه با دست تهی چون کف ازین دریا شوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با هوسناکان به یک پیمانه نتوان می کشید</p></div>
<div class="m2"><p>سعی کن صائب شهید تیغ استغنا شوی</p></div></div>