---
title: >-
    غزل شمارهٔ ۳۷۵۰
---
# غزل شمارهٔ ۳۷۵۰

<div class="b" id="bn1"><div class="m1"><p>سیه دل از غم دنیا خطر نمی دارد</p></div>
<div class="m2"><p>که خون مرده غم نیشتر نمی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز انقلاب جهان فارغند بی مغزان</p></div>
<div class="m2"><p>کف از تلاطم دریا خطر نمی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به قدر تلخی محنت بود حلاوت عیش</p></div>
<div class="m2"><p>نیی که بند ندارد شکر نمی دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صفای سینه ز اهل نفاق چشم مدار</p></div>
<div class="m2"><p>شب سیاه درونان سحر نمی دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی است در دل ما سوز داغ کهنه و نو</p></div>
<div class="m2"><p>درین چمن رگ خامی ثمر نمی دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز گل شکایت بلبل دلیل خامیهاست</p></div>
<div class="m2"><p>که هرچه سوخته گردد شرر نمی دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بود عزیز نظرها کسی که چون نرگس</p></div>
<div class="m2"><p>ز پشت پای ادب چشم بر نمی دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درین ریاض زمین گیر خواریم صائب</p></div>
<div class="m2"><p>که مهر را کسی از خاک بر نمی دارد</p></div></div>