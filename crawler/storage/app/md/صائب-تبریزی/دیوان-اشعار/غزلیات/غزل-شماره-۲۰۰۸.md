---
title: >-
    غزل شمارهٔ ۲۰۰۸
---
# غزل شمارهٔ ۲۰۰۸

<div class="b" id="bn1"><div class="m1"><p>چون صبح، زندگانی روشندلان دمی است</p></div>
<div class="m2"><p>اما دمی که باعث احیای عالمی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عیش غلط نمای جهان پرده غمی است</p></div>
<div class="m2"><p>شیرازه شکفتگیش زلف ماتمی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن را که راهزن نشود نعل واژگون</p></div>
<div class="m2"><p>ابروی ماه عید، هلال محرمی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در چشم آبگینه ما دل رمیدگان</p></div>
<div class="m2"><p>زنگ ملال، دامن صحرای خرمی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از سینه هر دمی که برآید ز روی صدق</p></div>
<div class="m2"><p>مانند صبح، صیقل زنگار عالمی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر هر دلی که زخمی تیغ زبان شود</p></div>
<div class="m2"><p>بی چشم زخم، سوده الماس مرهمی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر جا به عاجزی رود از پیش کارها</p></div>
<div class="m2"><p>هر مور اژدهایی و هر زال رستمی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن را که شد گزیده ز طول حیات خویش</p></div>
<div class="m2"><p>لیل و نهار در نظرش مار ارقمی است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلهای آب گشته مرغان بینواست</p></div>
<div class="m2"><p>هر جا به چهره گل این باغ شبنمی است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر چند نم برون ندهد خاک خشک مغز</p></div>
<div class="m2"><p>نسبت به آسمان سیه کاسه حاتمی است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در پنجه تصرف اغیار، زلف تو</p></div>
<div class="m2"><p>در دست دیو مانده گرفتار، خاتمی است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صائب به غیر چاه زنخدان یار نیست</p></div>
<div class="m2"><p>راز مرا گر از همه آفاق محرمی است</p></div></div>