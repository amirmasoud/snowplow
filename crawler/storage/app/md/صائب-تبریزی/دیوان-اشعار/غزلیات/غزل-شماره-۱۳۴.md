---
title: >-
    غزل شمارهٔ ۱۳۴
---
# غزل شمارهٔ ۱۳۴

<div class="b" id="bn1"><div class="m1"><p>سنگ طفلان از جنون رطل گرانی شد مرا</p></div>
<div class="m2"><p>درد و داغ عشق باغ و بوستانی شد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از گرفتاری به آزادی رسیدم در قفس</p></div>
<div class="m2"><p>خارخار دیدن گل آشیانی شد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد ز دنیا چشم بستن، جنت در بسته ام</p></div>
<div class="m2"><p>خط کشیدن بر جهان، خط امانی شد مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشرت ملک سلیمان می کنم در چشم مور</p></div>
<div class="m2"><p>قطره از دقت محیط بیکرانی شد مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا ز خاموشی زبان بی زبانان یافتم</p></div>
<div class="m2"><p>روی در دیوار کردم، همزبانی شد مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس که دیدم بی ثباتی از جهان بی وفا</p></div>
<div class="m2"><p>خاک ساکن در نظر آب روانی شد مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در جوانی توبه دمسرد پیرم کرده بود</p></div>
<div class="m2"><p>همت پیر مغان بخت جوانی شد مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تیر آهی از پشیمانی نجست از سینه ام</p></div>
<div class="m2"><p>گر چه از بار گنه، قد چون کمانی شد مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حرف پیمایی مرا پیوسته در خمیازه داشت</p></div>
<div class="m2"><p>مهر خاموشی به لب رطل گرانی شد مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پاس صحبت داشتن در دوزخم افکنده بود</p></div>
<div class="m2"><p>گوشه عزلت بهشت جاودانی شد مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتم از خط دار و گیر حسن او آخر شد</p></div>
<div class="m2"><p>عاقبت خط فتنه آخر زمانی شد مرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شوق من افتاده ای نگذاشت در روی زمین</p></div>
<div class="m2"><p>نقش پا از بی قراری کاروانی شد مرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پیش هر سنگی که کردم سینه را صائب سپر</p></div>
<div class="m2"><p>در بیابان طلب سنگ نشانی شد مرا</p></div></div>