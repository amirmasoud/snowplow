---
title: >-
    غزل شمارهٔ ۵۲۷۳
---
# غزل شمارهٔ ۵۲۷۳

<div class="b" id="bn1"><div class="m1"><p>ای از رخت هر خار سامان بستان در بغل</p></div>
<div class="m2"><p>هر ذره را از داغ تو خورشید تابان در بغل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر حلقه زلف ترا صد ملک چین درآستین</p></div>
<div class="m2"><p>هر پرده چشم ترا صد کافرستان در بغل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی چشم گستاخ مرا راه تماشا می دهد</p></div>
<div class="m2"><p>رویی که دارد از عرق چندین نگهبان در بغل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک ره برآر از آستین دست نگارین در چمن</p></div>
<div class="m2"><p>تا دستها پنهان کند سرو خرامان در بغل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر جا که دفتر واکند آن یوسف گل پیرهن</p></div>
<div class="m2"><p>صبح قیامت می نهد از شرم دیوان در بغل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جوش قیامت می زند خونم ز پند ناصحان</p></div>
<div class="m2"><p>باد مخالف را بود سامان طوفان در بغل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زان سان که سنبل چشمه را از دیده ها پنهان کند</p></div>
<div class="m2"><p>دارد چنان چشم مرا خواب پریشان در بغل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون غنچه سراز جیب خود بهر چه بیرون آورم</p></div>
<div class="m2"><p>من کز خیال روی او دارم گلستان در بغل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>امروز عاجز گشته ام درراز پنهان داشتن</p></div>
<div class="m2"><p>من کآسمان را کردمی چون شیشه پنهان در بغل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کو جذبه ای تا بگذرم زین خارزار بی امان</p></div>
<div class="m2"><p>تا کی فراهم آورم چون غنچه دامان در بغل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صد تیره آه از سینه اش یکبار می آید برون</p></div>
<div class="m2"><p>آن را که چون ترکش بود صدرنگ پیکان در بغل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از گنج بی پایان حق دخل کریمان می رسد</p></div>
<div class="m2"><p>هرگز نماند مهر را دست زرافشان در بغل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دست حوادث کوته است از دامن آزار من</p></div>
<div class="m2"><p>دارم چو بحر از موج خود صد تیغ عریان در بغل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ما و خرابات مغان کز وسعت مشرب بود</p></div>
<div class="m2"><p>هرمور از خود رفته را ملک سلیمان در بغل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عرض صفای دل مده درحلقه تن پروران</p></div>
<div class="m2"><p>عاقل کند در زنگبار آیینه پنهان در بغل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از ناتوان جهان گیرند همت پردلان</p></div>
<div class="m2"><p>شیر ژیان را پرورد دایم نیستان در بغل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گل می کند صائب همان از سینه پر خون من</p></div>
<div class="m2"><p>چندان که سازم داغ را چون لاله پنهان در بغل</p></div></div>