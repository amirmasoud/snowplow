---
title: >-
    غزل شمارهٔ ۵۱۳۹
---
# غزل شمارهٔ ۵۱۳۹

<div class="b" id="bn1"><div class="m1"><p>دل چون شود جدا ز سر زلف یار جمع؟</p></div>
<div class="m2"><p>کز رشته می شود گهر شاهوار جمع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گردید مخزن گهر ولعل سینه اش</p></div>
<div class="m2"><p>تاکرد پا به دامن خود کوهسار جمع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در یک نفس به باد فنا می دهد خزان</p></div>
<div class="m2"><p>چندان که برگ عیش کند نوبهار جمع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شستم ز کار هردو جهان دست،چون نشد</p></div>
<div class="m2"><p>کار غیور عشق تو با هیچ کار جمع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر خرده ای که جمع کنی خرج آتش است</p></div>
<div class="m2"><p>زنهار زر چو غنچه مکن دربهار جمع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوش وقت آن که چون گل رعنا درین چمن</p></div>
<div class="m2"><p>دل از خزان نمود به فصل بهار جمع</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در برگریز سبز بود،هر که می کند</p></div>
<div class="m2"><p>دامان خودچو سرو درین خارزار جمع</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باگریه همرکاب بود خنده اش چو برق</p></div>
<div class="m2"><p>دل چون کنم ز شادی ناپایدار جمع؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ته جرعه ای است ازجگر داغدار من</p></div>
<div class="m2"><p>داغی که هست درجگر لاله زار جمع</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکرنگی از دورنگ طمع داشتن خطاست</p></div>
<div class="m2"><p>چون دل کنم ز گردش لیل و نهار جمع ؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون تاک هررگم به رهی سیر می کند</p></div>
<div class="m2"><p>خاطر شود چگونه مرا درخمار جمع؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صائب ز باد دستی حسرت به خرج رفت</p></div>
<div class="m2"><p>چندان که کرد آه دل بیقرار جمع</p></div></div>