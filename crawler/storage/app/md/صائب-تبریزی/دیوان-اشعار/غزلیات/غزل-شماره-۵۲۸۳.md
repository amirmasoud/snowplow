---
title: >-
    غزل شمارهٔ ۵۲۸۳
---
# غزل شمارهٔ ۵۲۸۳

<div class="b" id="bn1"><div class="m1"><p>فکر حاصل ره ندارد در دل آزاده ام</p></div>
<div class="m2"><p>تخم خال عیب باشد در زمین ساده ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قطره بی ظرفم اما چون به جوش آید دلم</p></div>
<div class="m2"><p>می کند تنگی خم گردون به جوش باده ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چه صحرایی است بر مشت غبارم چشم مور</p></div>
<div class="m2"><p>دربغل دارد فلکها را دل بگشانده ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچ کس را دل نمی سوزد به من چون آفتاب</p></div>
<div class="m2"><p>گرچه از بام بلند آسمان افتاده ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اختیاری نیست سیر موجه بیتاب من</p></div>
<div class="m2"><p>سالها شد از بام بلند آسمان افتاده ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می زنم در لامکان پر با پریزادان قدس</p></div>
<div class="m2"><p>پشت بردیوار جسم از کاهلی ننهاده ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گردش چشمی که من زان دشمن دین دیده ام</p></div>
<div class="m2"><p>بادبان کشتی می می کند سجاده ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از بزرگان دیدن دربان مرا دلسرد ساخت</p></div>
<div class="m2"><p>کرد یک دیدن ز صد نادیدنی آزاده ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می شود قفل خموشی غنچه منقار او</p></div>
<div class="m2"><p>گر شود آیینه طوطی ضمیر ساده ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>انتظار همرهان صائب عنانگیر من است</p></div>
<div class="m2"><p>ورنه من عمری است تا پرواز را آماده ام</p></div></div>