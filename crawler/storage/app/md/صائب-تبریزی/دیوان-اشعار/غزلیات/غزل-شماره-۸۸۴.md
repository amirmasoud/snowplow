---
title: >-
    غزل شمارهٔ ۸۸۴
---
# غزل شمارهٔ ۸۸۴

<div class="b" id="bn1"><div class="m1"><p>مد کوتاهی است صبح از دفتر احسان شب</p></div>
<div class="m2"><p>سرمکش چون خامه زنهار از خط فرمان شب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشرق خورشید می گردد گریبانش چو صبح</p></div>
<div class="m2"><p>هر که آویزد ز روی صدق در دامان شب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هست در ابر سیه باران رحمت بیشتر</p></div>
<div class="m2"><p>تازه رو دارد سفال خاک را ریحان شب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ماهرویی هست پنهان زیر این چتر سیاه</p></div>
<div class="m2"><p>سرسری مگذر چو باد از زلف مشک افشان شب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می رساند دور گردان را به معراج وصول</p></div>
<div class="m2"><p>در نظر واکردنی شبدیز خوش جولان شب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تخم اشکی را که افشانند در دامان او</p></div>
<div class="m2"><p>همچو پروین خوشه گوهر کند دهقان شب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیوه او نیست غمازی چو صبح پی سفید</p></div>
<div class="m2"><p>عاصیان را پرده پوشی می کند دامان شب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پرده غفلت حجاب چشم خواب آلود توست</p></div>
<div class="m2"><p>ورنه لبریزست از الوان نعمت، خوان شب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون سکندر، عالمی سرگشته در این ظلمتند</p></div>
<div class="m2"><p>تا که را سیراب سازد چشمه حیوان شب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غافلان را پرده خواب است، ورنه از شرف</p></div>
<div class="m2"><p>اهل دل را جامه کعبه است شادروان شب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در ته این زنگ هست آیینه سیمایی نهان</p></div>
<div class="m2"><p>چشم ظاهربین نبیند خوبی پنهان شب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من چسان از روی ماهش چشم بردارم، که هست</p></div>
<div class="m2"><p>با هزاران چشم روشن، آسمان حیران شب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پیش چشم هر که صائب روشن است از نور دل</p></div>
<div class="m2"><p>آیه رحمت بود سر تا سر دیوان شب</p></div></div>