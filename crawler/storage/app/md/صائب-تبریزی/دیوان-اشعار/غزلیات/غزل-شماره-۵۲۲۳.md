---
title: >-
    غزل شمارهٔ ۵۲۲۳
---
# غزل شمارهٔ ۵۲۲۳

<div class="b" id="bn1"><div class="m1"><p>پای سعی دیگران آمد گر از صحرا به سنگ</p></div>
<div class="m2"><p>در وطن آمد مرا از خواب سنگین پا به سنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر دل پرخون عاشق نیست کوه غم گران</p></div>
<div class="m2"><p>می زند پهلو به زور باده این مینا به سنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خنده کبک از ترحم هایهای گریه شد</p></div>
<div class="m2"><p>تا که را در کوهسار عشق آمد پابه سنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از شکست زلف کی گردد پریشان خاطرش ؟</p></div>
<div class="m2"><p>آن که چندین شیشه دلی را زند یکجابه سنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باد عکس مراد آیینه اش صورت پذیر</p></div>
<div class="m2"><p>آن که از سنگین دلی زدشیشه مارابه سنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست چز دندان شکستن چاره ای کج بحث را</p></div>
<div class="m2"><p>ازدم عقرب گره نتوان گشود الا به سنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نگشتی جذبه فرهاد دامنگیر او</p></div>
<div class="m2"><p>کی ز شوخی می گرفتی نقش شیرین پابه سنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من به افسون نرم کردم آن دل چون سنگ را</p></div>
<div class="m2"><p>جوی شیر از تیشه گر فرهاد کرد انشا به سنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>راه سخت وهمراهان ناساز ومرکب کندرو</p></div>
<div class="m2"><p>هیچ رهرو را چندین جا نیاید پابه سنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همچنان در جستجوی رزق خودسر گشته ام</p></div>
<div class="m2"><p>گرچه گشتم چون فلاخن قانع ازدنیا به سنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیش و کم رابا نظر سنجند روشن گوهران</p></div>
<div class="m2"><p>احتیاجی نیست میزان قیامت رابه سنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با گرانجانی به معراج هنر نتوان رسید</p></div>
<div class="m2"><p>سخت دشوارست سیر عالم بالابه سنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آب چشم من ندارد در دل سخت توراه</p></div>
<div class="m2"><p>ورنه سازد چشمه حکم خویش رااجرابه سنگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ناتوانی عقده های سهل رامشکل کند</p></div>
<div class="m2"><p>خامه های سست راازنقطه آید پابه سنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بود از سنگ ملامت مهره گهواره ام</p></div>
<div class="m2"><p>نیست امروز از جنون ربط من شیدا به سنگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حرف سخت ازبردباری بر دل مابار نیست</p></div>
<div class="m2"><p>می دهد پهلو درخت میوه دار مابه سنگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آه کز خواب گران درراه سیل حادثات</p></div>
<div class="m2"><p>همچو دست آسیا رفته است پای مابه سنگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر دل پرخون ندارد سختی ایام دست</p></div>
<div class="m2"><p>نیست ممکن کشتی آید در دل دریا به سنگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از دل شب تیرگی بسیاری انجم نبرد</p></div>
<div class="m2"><p>از سر مجنون کجا بیرون سودابه سنگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>می شود از مهره موم این زمان دندانه دار</p></div>
<div class="m2"><p>بود اگر چون تیشه چندی ناخنم گیرا به سنگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر به سنگ آمد ز ساحل کشتی امید خلق</p></div>
<div class="m2"><p>صائب آمد کشتی ما در دل دریا به سنگ</p></div></div>