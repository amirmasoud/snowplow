---
title: >-
    غزل شمارهٔ ۲۶۱۶
---
# غزل شمارهٔ ۲۶۱۶

<div class="b" id="bn1"><div class="m1"><p>یوسف ما در دل چه بر سر بازار بود</p></div>
<div class="m2"><p>این گل از صبح ازل شیدایی دستار بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیشطاق شهرت از شعر بلندم رتبه یافت</p></div>
<div class="m2"><p>اینچنین زلفی رخ این صفحه را در کار بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوه و صحرا پر شد از آوازه زنجیر من</p></div>
<div class="m2"><p>پای صحرا گرد مجنون کی به این پرگار بود؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صائب این طرز سخن را از کجا آورده ای؟</p></div>
<div class="m2"><p>هر که را دیدیم داغ طرز این اشعار بود</p></div></div>