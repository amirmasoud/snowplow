---
title: >-
    غزل شمارهٔ ۲۷۷۱
---
# غزل شمارهٔ ۲۷۷۱

<div class="b" id="bn1"><div class="m1"><p>روز روشن می کند چون لاله می دل را سیاه</p></div>
<div class="m2"><p>در شب تاریک باید باده روشن کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر نیامد از زبردستان کسی با آسمان</p></div>
<div class="m2"><p>گوش تا گوش این کمان را آه گرم من کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش ازین می ریختم در ریگ روغن را چو آب</p></div>
<div class="m2"><p>این زمان از ریگ می باید مرا روغن کشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از قناعت بیش شد منت پذیریهای من</p></div>
<div class="m2"><p>باید از هر دانه اکنون ناز صد خرمن کشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از ملامت ترک نتوان کرد شغل عشق را</p></div>
<div class="m2"><p>پا به زخم خار نتوان صائب از گلشن کشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا ازین ویرانه آن خورشید رو دامن کشید</p></div>
<div class="m2"><p>آه میل آتشین در دیده روزن کشید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در دل سخت تو را هم نیست، ورنه جذب من</p></div>
<div class="m2"><p>بارها آتش زسنگ و آب از آهن کشید</p></div></div>