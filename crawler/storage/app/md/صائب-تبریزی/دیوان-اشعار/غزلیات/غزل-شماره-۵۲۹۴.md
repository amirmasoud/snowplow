---
title: >-
    غزل شمارهٔ ۵۲۹۴
---
# غزل شمارهٔ ۵۲۹۴

<div class="b" id="bn1"><div class="m1"><p>بی‌تن خاکی چو نام نیکمردان زنده‌ام</p></div>
<div class="m2"><p>سال‌ها شد این لباس عاریت را کنده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه برگ من زبان شکر و بار افتادگی است</p></div>
<div class="m2"><p>همچنان از حسن سعی باغبان شرمنده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس که چون یوسف گران بر خاطراخوان شدم</p></div>
<div class="m2"><p>از وطن هرکس مرا آزاد سازد بنده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مطلبم زین نعل وارون جز تلاش نام نیست</p></div>
<div class="m2"><p>چون عقیق از نام در ظاهر اگر دل کنده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون قلم تنگ بر من از سیه‌کاری جهان</p></div>
<div class="m2"><p>نیست جز یک پشت ناخن دستگاه خنده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست صائب غیر آه ناامیدی خوشه‌اش</p></div>
<div class="m2"><p>تخم امیدی که من در شوره‌زار افکنده‌ام</p></div></div>