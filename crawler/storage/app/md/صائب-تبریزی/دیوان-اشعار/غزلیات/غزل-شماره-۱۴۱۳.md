---
title: >-
    غزل شمارهٔ ۱۴۱۳
---
# غزل شمارهٔ ۱۴۱۳

<div class="b" id="bn1"><div class="m1"><p>تلخ شد عشرتم آن لعل شکربار کجاست؟</p></div>
<div class="m2"><p>دلم از کار شد آن غمزه پر کار کجاست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خنده از تنگی جا در دهنش غنچه شده است</p></div>
<div class="m2"><p>بوسه را راه سخن پیش لب یار کجاست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سفر اول پرواز به دام افتاده است</p></div>
<div class="m2"><p>بلبل ما نشنیده است که گلزار کجاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مزرع خانه تسبیح بود یک کف دست</p></div>
<div class="m2"><p>زهد را دستگه رشته زنار کجاست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ذوق نظاره گل در نگه پنهان است</p></div>
<div class="m2"><p>ای مقیمان چمن، رخنه دیوار کجاست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا به کی در ته دیوار تعلق باشم؟</p></div>
<div class="m2"><p>کوچه خانه بدوشان سبکبار کجاست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم تا کار کند گرد کسادی فرش است</p></div>
<div class="m2"><p>در بساط سخن امروز خریدار کجاست؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر سر موی شکافی است نگاهم صائب</p></div>
<div class="m2"><p>چین زلفی که دهد نافه به تاتار کجاست؟</p></div></div>