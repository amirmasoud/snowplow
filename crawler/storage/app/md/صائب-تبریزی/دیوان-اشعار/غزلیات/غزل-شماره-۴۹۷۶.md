---
title: >-
    غزل شمارهٔ ۴۹۷۶
---
# غزل شمارهٔ ۴۹۷۶

<div class="b" id="bn1"><div class="m1"><p>حرف عقبی که درین نشأه کنی تقریرش</p></div>
<div class="m2"><p>همچو خوابی است که درخواب کنی تعبیرش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق از پرده ناموس برون می آید</p></div>
<div class="m2"><p>این نه شمعی است که فانوس کند تسخیرش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی نیازی است محبت که به تکلیف گناه</p></div>
<div class="m2"><p>دست در گردن یوسف نکند زنجیرش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که را دایره خلق وسیع افتاده است</p></div>
<div class="m2"><p>چار دیوار عناصر نکند دلگیرش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز دهان توکه در سبزه خط پنهان شد</p></div>
<div class="m2"><p>نکته ای نیست که پوشیده کندتفسیرش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنچه روشن به من از شوخی آن حسن شده است</p></div>
<div class="m2"><p>چشم آیینه به صد سال نبیند سیرش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکه بیرون رود از شهر به امید نجات</p></div>
<div class="m2"><p>حکم عشق است نظر بندکند نخجیرش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می رسند اهل قلم از سخن آخر به نوال</p></div>
<div class="m2"><p>این نه طفلی است کز انگشت نزاید شیرش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب ازحلقه این سخت کمانان سخن</p></div>
<div class="m2"><p>خامه ماست که بر سنگ نیامد تیرش</p></div></div>