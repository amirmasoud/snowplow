---
title: >-
    غزل شمارهٔ ۷۳۷
---
# غزل شمارهٔ ۷۳۷

<div class="b" id="bn1"><div class="m1"><p>مشمر ز عمر خود نفس ناشمرده را</p></div>
<div class="m2"><p>دفتر مساز این ورق باد برده را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با زاهد فسرده مکن گفتگوی عشق</p></div>
<div class="m2"><p>تلقین نکرده است کسی خون مرده را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تخمی که سوخت، سبز نگردد ز نوبهار</p></div>
<div class="m2"><p>افسرده تر کند می گلگون فسرده را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بپذیر عذر باده کشان را، که همچو موج</p></div>
<div class="m2"><p>در دست خویش نیست عنان، آب برده را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندیشه کن ز باطن پیران که چون چنار</p></div>
<div class="m2"><p>هست آتشی نهفته به دل سالخورده را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صائب نظر به سیب زنخدان یار نیست</p></div>
<div class="m2"><p>دندان به پاره های دل خود فشرده را</p></div></div>