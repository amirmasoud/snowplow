---
title: >-
    غزل شمارهٔ ۶۱۷۸
---
# غزل شمارهٔ ۶۱۷۸

<div class="b" id="bn1"><div class="m1"><p>در گلستانی که ریزد خون بلبل بر زمین</p></div>
<div class="m2"><p>در لباس لاله گردد جلوه گر گل بر زمین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زود در چاه ندامت سرنگون خواهد فتاد</p></div>
<div class="m2"><p>هر که پای خود گذارد بی تأمل بر زمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرو پا در گل کجا و لاف آزادی کجا</p></div>
<div class="m2"><p>سایه آزادگان دارد تغافل بر زمین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق امانت دار معشوق است، ازان رو گل گذاشت</p></div>
<div class="m2"><p>نقد و جنس خویش را در پیش بلبل بر زمین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حال دست من جدا از دامنش داند که چیست</p></div>
<div class="m2"><p>هر که از دستش رها شد دامن گل بر زمین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قطره خونی که صد نقش هوس می زد بر آب</p></div>
<div class="m2"><p>می چکد امروز از تیغ تغافل بر زمین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قوت سر پنجه بیداد نتواند رساند</p></div>
<div class="m2"><p>با همه زور آوری پشت تحمل بر زمین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بود تا در قبضه من اختیار گلستان</p></div>
<div class="m2"><p>غیرتم نگذاشت افتد سایه گل بر زمین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دشت پیمای جنون پیشانیی دارد که شیر</p></div>
<div class="m2"><p>می گذارد پیش او روی تنزل بر زمین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بال خود چون سبزه بلبل فرش گلشن ساخته است</p></div>
<div class="m2"><p>تا مباد از گلبن افتد سایه گل بر زمین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حسن عالمسوز از اقبال عشق آمد پدید</p></div>
<div class="m2"><p>رنگ گل را ریختند از خون بلبل بر زمین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خامه صائب صفیری غالبا از دل کشید</p></div>
<div class="m2"><p>کز کنار آشیان افتاد بلبل بر زمین</p></div></div>