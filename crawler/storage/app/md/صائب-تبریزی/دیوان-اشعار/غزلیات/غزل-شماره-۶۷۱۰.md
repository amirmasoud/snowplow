---
title: >-
    غزل شمارهٔ ۶۷۱۰
---
# غزل شمارهٔ ۶۷۱۰

<div class="b" id="bn1"><div class="m1"><p>گر چه خالی کردم از خون صد ایاغ از تشنگی</p></div>
<div class="m2"><p>دل همان در سینه سوزد چون چراغ از تشنگی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساغر خون خوردنم چون لاله نم بیرون نداد</p></div>
<div class="m2"><p>بس که دل در سینه من بود داغ از تشنگی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بحر اگر در کاسه ام ریزند می گردد سراب</p></div>
<div class="m2"><p>خشک شد از بس مرا مغز و دماغ از تشنگی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم احسان دارم از آهن دلان روزگار</p></div>
<div class="m2"><p>آب را از تیغ می گیرم سراغ از تشنگی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حال من دور از لب جان بخش او داند که چیست</p></div>
<div class="m2"><p>چون سکندر هر که گردیده است داغ از تشنگی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شهپر طاوس می باید که باشد سبز و تر</p></div>
<div class="m2"><p>نیست صائب هیچ (غم) گر سوخت داغ از تشنگی</p></div></div>