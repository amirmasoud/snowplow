---
title: >-
    غزل شمارهٔ ۹۲
---
# غزل شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>کی نیام پوچ می سازد به تمکین تیغ را؟</p></div>
<div class="m2"><p>آستین زندان بود چون دست گلچین تیغ را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیل بی زنهار را هر موج بال دیگرست</p></div>
<div class="m2"><p>کثرت جوهر نمی سازد به تمکین تیغ را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غمزه اش از کشتن عشاق شد در خون دلیر</p></div>
<div class="m2"><p>تشنه خون می کند جان های شیرین تیغ را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می کند آهن دلی، کار فسان با کج نهاد</p></div>
<div class="m2"><p>نیست در خون ریختن حاجت به تلقین تیغ را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست پروا برق را از تلخرویی های ابر</p></div>
<div class="m2"><p>چون سپر مانع شود ز ابروی پرچین تیغ را؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست گلچین شد دراز از چهره خندان گل</p></div>
<div class="m2"><p>کرد زخم خنده روی من شلایین تیغ را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کرد عشق آهنین بازو ز مومش نرمتر</p></div>
<div class="m2"><p>آن که کرد از سخت جانی اره چندین تیغ را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می رساند محضر بی رحمی خود را به مهر</p></div>
<div class="m2"><p>نیست از راه ترحم اشک خونین تیغ را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می برد دل از نگاه زیر چشمی بیش، حسن</p></div>
<div class="m2"><p>جوهر دیگر بود زیر سپر این تیغ را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواب آسایش به گرد دیده جوهر نگشت</p></div>
<div class="m2"><p>خون گرم من نشد تا شمع بالین تیغ را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چشم رحم از قاتلی دارم که از بهر شگون</p></div>
<div class="m2"><p>اول از صید حرم کرده است رنگین تیغ را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شد ز آه بی شمار من فلک بی دست و پا</p></div>
<div class="m2"><p>چون برآید یک سپر از عهده چندین تیغ را؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر من از شکر شهادت لب ز حیرت بسته ام</p></div>
<div class="m2"><p>می کند صائب دهان زخم، تحسین تیغ را</p></div></div>