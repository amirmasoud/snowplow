---
title: >-
    غزل شمارهٔ ۶۵۴۵
---
# غزل شمارهٔ ۶۵۴۵

<div class="b" id="bn1"><div class="m1"><p>ای دل گشاد کار خود از آن و این مجو</p></div>
<div class="m2"><p>این قفل را کلید ز هر آستین مجو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی دل از خسیس نهادان طلب مکن</p></div>
<div class="m2"><p>از خار و خس ملایمت یاسمین مجو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حال دل گرفته به هر بی بصر مگوی</p></div>
<div class="m2"><p>از دوزخی کلید بهشت برین مجو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زنبور کافرند سراسر ستارگان</p></div>
<div class="m2"><p>زنهار ازین سیاه دلان انگبین مجو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواهی که بر تو آتش سوزان شود بهشت</p></div>
<div class="m2"><p>امداد چون خلیل ز روح الامین مجو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بشناس استخوان و طباشیر را ز هم</p></div>
<div class="m2"><p>از صبح اولین، نفس راستین مجو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در هر کس آنچه هست همان را ازو طلب</p></div>
<div class="m2"><p>لنگر ز آسمان، حرکت از زمین مجو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آرامش دل تو برون است از آب و گل</p></div>
<div class="m2"><p>در دامن آنچه گم شود از آستین مجو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شایستگی کلید بود قفل بسته را</p></div>
<div class="m2"><p>از سنگ، آب بی جگر آتشین مجو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نتوان به بال عاریه بیرون شدن ز خویش</p></div>
<div class="m2"><p>در وادی طلب مدد از آن و این مجو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گم کرده تو از تو برون نیست، زینهار</p></div>
<div class="m2"><p>گاهی ز آسمان و گهی از زمین مجو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از دست رعشه دار پریشان شود رقم</p></div>
<div class="m2"><p>از دل رمیدگان سخن دلنشین مجو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از دیده می دهند خبر پاک دیدگان</p></div>
<div class="m2"><p>خار گمان ز نرگس عین الیقین مجو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هرگز ز قفل، قفل گشایش ندیده است</p></div>
<div class="m2"><p>صائب گشایش از دل اندوهگین مجو</p></div></div>