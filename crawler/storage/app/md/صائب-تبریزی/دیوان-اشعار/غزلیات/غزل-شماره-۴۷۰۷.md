---
title: >-
    غزل شمارهٔ ۴۷۰۷
---
# غزل شمارهٔ ۴۷۰۷

<div class="b" id="bn1"><div class="m1"><p>دل راز سینه درنظر دلستان برآر</p></div>
<div class="m2"><p>آیینه پیش یوسف از آیینه دان برآر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کار غیور عشق شراکت پذیر نیست</p></div>
<div class="m2"><p>دل رابه نقد ازهمه کار جهان برآر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگذار رنگ جسم پذیرد روان پاک</p></div>
<div class="m2"><p>این مغز رابه نرمی ازین استخوان برآر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با برق همرکاب بودجلوه بهار</p></div>
<div class="m2"><p>خود را به زخم خار درین گلستان برآر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آزادگی و بی ثمری کن شعار خویش</p></div>
<div class="m2"><p>دامان خود چو سرو ز دست خزان برآر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گلزار خود ز سبزه بیگانه پاک کن</p></div>
<div class="m2"><p>آنگاه درملامت مردم زبان برآر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در شوره زار تخم نکویی ثمر دهد</p></div>
<div class="m2"><p>چون دوستان مراد دل دشمنان برآر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در بند خارزار علایق چه مانده ای؟</p></div>
<div class="m2"><p>دستی به جمع کردن دامان جان برآر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون پای قطع راه نداری ز کاهلی</p></div>
<div class="m2"><p>خاری به دست از قدم رهروان برآر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شاید دچار دامن اهل دلی شوی</p></div>
<div class="m2"><p>چون آفتاب دست به گرد جهان برآر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب حریف سیل حوادث نمی شوی</p></div>
<div class="m2"><p>مردانه رخت خویش ازین خاکدان برآر</p></div></div>