---
title: >-
    غزل شمارهٔ ۶۶۷۸
---
# غزل شمارهٔ ۶۶۷۸

<div class="b" id="bn1"><div class="m1"><p>یاد دارد تخت شاهان قلزم خضرا بسی</p></div>
<div class="m2"><p>سرنگون گردیده زین کشتی درین دریا بسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک ها در کاسه سرکرده چون موج سراب</p></div>
<div class="m2"><p>رهروان تشنه لب را جلوه دنیا بسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترک دنیا پیش دنیادوستان باشد عظیم</p></div>
<div class="m2"><p>ورنه در قاف قناعت هست ازین عنقا بسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه همین قارون فرو رفته است در خاک سیاه</p></div>
<div class="m2"><p>خویش را گم کرده اند از جستن دنیا بسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاکساری چون سرافرازی نمی دارد زوال</p></div>
<div class="m2"><p>کوهها را پشت سر دیده است این صحرا بسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شیشه پر زهر گردون چیست در دیر مغان</p></div>
<div class="m2"><p>هر تنک ظرفی تهی کرده است ازین مینا بسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آسمان سنگدل از گریه ما فارغ است</p></div>
<div class="m2"><p>یاد دارد پل ازین سیلاب بی پروا بسی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از هزاران کس که می بینی یکی صاحبدل است</p></div>
<div class="m2"><p>آهوی مشکین ندارد دامن صحرا بسی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دست بردار از خم آن زلف چون چوگان که کرد</p></div>
<div class="m2"><p>سروران را گوی میدان صائب این سودا بسی</p></div></div>