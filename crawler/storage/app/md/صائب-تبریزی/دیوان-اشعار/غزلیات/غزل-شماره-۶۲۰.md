---
title: >-
    غزل شمارهٔ ۶۲۰
---
# غزل شمارهٔ ۶۲۰

<div class="b" id="bn1"><div class="m1"><p>ارگ چه سیل فنا برد هر چه بود مرا</p></div>
<div class="m2"><p>ز بحر کرد کرم خلعت وجود مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بند وصل لباسی مرا برون آورد</p></div>
<div class="m2"><p>اگر چه مه چو کتان سوخت تار و پود مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ستاره سوخته ای بود چون شرر جانم</p></div>
<div class="m2"><p>ز قرب سوختگان روشنی فزود مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز عمر رفته نصیبم جز آه حسرت نیست</p></div>
<div class="m2"><p>به جا نمانده ازان شمع غیر دود مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنین که روی مرا کرده بی حیایی سخت</p></div>
<div class="m2"><p>عجب که چهره ز سیلی شود کبود مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز خوش عیاری من سنگ امتحان داغ است</p></div>
<div class="m2"><p>ز خجلت آب شد آن کس که آزمود مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فغان که همچو قلم نیست از نگون بختی</p></div>
<div class="m2"><p>به غیر روسیهی حاصل از سجود مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به بینوایی ازین باغ پر ثمر صائب</p></div>
<div class="m2"><p>خوشم، که نیست محابایی از حسود مرا</p></div></div>