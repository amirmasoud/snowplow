---
title: >-
    غزل شمارهٔ ۱۵۶۸
---
# غزل شمارهٔ ۱۵۶۸

<div class="b" id="bn1"><div class="m1"><p>در غم و شادی ایام مرا حال یکی است</p></div>
<div class="m2"><p>فصل هر چند کند جامه بدل سال یکی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حرص دایم ز برای دگران در گردست</p></div>
<div class="m2"><p>حال این بی بصر و دیده غربال یکی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عرق سعی برای دگران می ریزد</p></div>
<div class="m2"><p>حاصل خواجه ز مال خود و حمال یکی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر نفس اهل هوس نیت دیگر دارند</p></div>
<div class="m2"><p>دل این طایفه و قرعه رمال یکی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش سوزن که به یک چشم جهان را بیند</p></div>
<div class="m2"><p>گوهر عیسی و خرمهره دجال یکی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش جمعی که ازین نشأه به تنگ آمده اند</p></div>
<div class="m2"><p>شادی مردن و آزادی اطفال یکی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل اگر نرم شود کار جهان آسان است</p></div>
<div class="m2"><p>گره سخت به سررشته آمال یکی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ادب پیر خرابات نگهداشتنی است</p></div>
<div class="m2"><p>طبع پیران و دل نازک اطفال یکی است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا رسیدم به پریخانه وحدت صائب</p></div>
<div class="m2"><p>پای طاوس مرا در نظر و بال یکی است</p></div></div>