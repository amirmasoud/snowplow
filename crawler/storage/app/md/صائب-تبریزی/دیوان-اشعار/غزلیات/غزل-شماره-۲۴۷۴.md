---
title: >-
    غزل شمارهٔ ۲۴۷۴
---
# غزل شمارهٔ ۲۴۷۴

<div class="b" id="bn1"><div class="m1"><p>ناله ای گیراتر از چنگ عقابم داده اند</p></div>
<div class="m2"><p>گریه ای سوزانتر از اشک کبابم داده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از زر و سیم جهان گر دست و دامانم تهی است</p></div>
<div class="m2"><p>شکر لله درد و داغ بی حسابم داده اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سالها ته کرده ام زانو در آتش همچو زلف</p></div>
<div class="m2"><p>تا به کف سر رشته ای از پیچ و تابم داده اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورده ام صد کاسه خون از دست گلهای چمن</p></div>
<div class="m2"><p>تا چو شبنم ره به بزم آفتابم داده اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گومکن نازک به من بالین مخمل پشت چشم</p></div>
<div class="m2"><p>کز سر زانوی خود بالین خوابم داده اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شیوه من نیست چون گردنکشان استادگی</p></div>
<div class="m2"><p>سر چو موج از خوش عنانی در سرابم داده اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک جهان لب تشنه را بر من حوالت کرده اند</p></div>
<div class="m2"><p>مشت آبی گر زدریا چون سحابم داده اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سایل مغرورم، از قسمت ندارم شکوه ای</p></div>
<div class="m2"><p>گشته ام ممنون به تلخی گر جوابم داده اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گریه خونین زخرسندی شراب من شده است</p></div>
<div class="m2"><p>تکیه گر بر روی آتش چون کبابم داده اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پرده شرم است سد راه، ورنه گلرخان</p></div>
<div class="m2"><p>رخصت نظاره زان روی نقابم داده اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کرده اند ایمن زبیداد غلط خوانان مرا</p></div>
<div class="m2"><p>جا اگر بر طاق نسیان چون کتابم داده اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با دهان خشک قانع شو که من مانند تیغ</p></div>
<div class="m2"><p>غوطه در خون خورده ام تا یک دم آبم داده اند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا قیامت پایم از شادی نیاید بر زمین</p></div>
<div class="m2"><p>رخصت پابوس تا همچون رکابم داده اند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آب حیوان را کند همکاسه بد ناگوار</p></div>
<div class="m2"><p>تنگ ظرفان توبه صائب از شرابم داده اند</p></div></div>