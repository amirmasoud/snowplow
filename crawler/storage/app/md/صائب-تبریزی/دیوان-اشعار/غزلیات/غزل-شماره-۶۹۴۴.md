---
title: >-
    غزل شمارهٔ ۶۹۴۴
---
# غزل شمارهٔ ۶۹۴۴

<div class="b" id="bn1"><div class="m1"><p>چندان به خضر ساز که از خود بدر شوی</p></div>
<div class="m2"><p>کز خود برون چو خیمه زدی راهبر شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندان تلاش کن که ترا بی خبر کنند</p></div>
<div class="m2"><p>چون بی خبر شدی ز جهان باخبر شوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبنم به آفتاب رسید از فروتنی</p></div>
<div class="m2"><p>افتاده شو مگر تو هم از خاک بر شوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد آب تلخ گوهر شهوار در صدف</p></div>
<div class="m2"><p>از خود تو هم سفر کن، شاید گهر شوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از قلزمی که نوح مسلم بدر نرفت</p></div>
<div class="m2"><p>تو خشک مغز در غم آنی که تر شوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همت بلنددار، چه چیزست این جهان؟</p></div>
<div class="m2"><p>تا قانع از خدای به این مختصر شوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون سوزن از لباس تعلق برهنه شو</p></div>
<div class="m2"><p>تا با مسیح پاک نفس همسفر شوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب جواب آن غزل است این که خواجه گفت</p></div>
<div class="m2"><p>ای بیخبر بکوش که صاحب خبر شوی</p></div></div>