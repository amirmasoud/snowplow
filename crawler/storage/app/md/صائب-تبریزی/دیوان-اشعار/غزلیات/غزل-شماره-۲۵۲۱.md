---
title: >-
    غزل شمارهٔ ۲۵۲۱
---
# غزل شمارهٔ ۲۵۲۱

<div class="b" id="bn1"><div class="m1"><p>جلوه ای سرکن که خون از چشم بلبل سر کند</p></div>
<div class="m2"><p>اشک شبنم بی حجاب از دیده گل سر کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می توان بر تیرباران ملامت صبر کرد</p></div>
<div class="m2"><p>کو جگرداری که با تیغ تغافل سر کند؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرده خود پیش هر ناشسته رو نتوان درید</p></div>
<div class="m2"><p>بلبل ما گریه را در دامن گل سر کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می دهد یاد از سواد هند فیل مست را</p></div>
<div class="m2"><p>پیش دل هر کس حدیث زلف و کاکل سرکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لنگر بیتابی دریا نمی گردد گهر</p></div>
<div class="m2"><p>عشق هیهات است با صبر و تحمل سر کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیره چشمی بین که پیش عارض گلرنگ او</p></div>
<div class="m2"><p>شبنم نادیده، حرف از دفتر گل سرکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می توان در پرده شب حال خود بی پرده گفت</p></div>
<div class="m2"><p>صبر آن دارم که خط زان روی چون گل سر کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فتنه ها زیر سر تیغ زبان خوابیده است</p></div>
<div class="m2"><p>وای بر آن کس که حرفی بی تأمل سر کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خضر را از لوح دل چون زنگ می باید زدود</p></div>
<div class="m2"><p>هر که خواهد راه صحرای توکل سر کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیش دیوانهای صائب بلبل رنگین سخن</p></div>
<div class="m2"><p>شرم بادش گر سخن از دفتر گل سر کند</p></div></div>