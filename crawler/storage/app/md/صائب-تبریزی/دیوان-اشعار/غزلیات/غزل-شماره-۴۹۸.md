---
title: >-
    غزل شمارهٔ ۴۹۸
---
# غزل شمارهٔ ۴۹۸

<div class="b" id="bn1"><div class="m1"><p>نمک خال بود داغ تمنای ترا</p></div>
<div class="m2"><p>شور لیلی است سیه خانه سودای ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر جبین همچو گهر گرد یتیمی دارد</p></div>
<div class="m2"><p>دید تا شبنم گل، چهره زیبای ترا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خضر از دامن یک عمر ابد دست نداشت</p></div>
<div class="m2"><p>کیست از دست دهد زلف دلارای ترا؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طوق هر فاخته ای حلقه ماتم می شد</p></div>
<div class="m2"><p>سرو می دید اگر قامت رعنای ترا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دو جهان در نظرش دست نگارین گردد</p></div>
<div class="m2"><p>هر که در چشم کشد خاک کف پای ترا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که گل از شمع تو چیند، که گرفته است به بر</p></div>
<div class="m2"><p>پرده شرم چو فانوس سراپای ترا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پر مقید به تماشای خود ای ماه مباش</p></div>
<div class="m2"><p>آفتابی نکند آینه، سیمای ترا!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما که داریم ز دل، دیدن روی تو دریغ</p></div>
<div class="m2"><p>چون به آیینه پسندیم تماشای ترا؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مانده در عقده حیرت نفس موی شکاف</p></div>
<div class="m2"><p>بوسه چون راه برد لعل شکرخای ترا؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چشم صائب به کجای تو نظرباز شود؟</p></div>
<div class="m2"><p>شوخی چشم غزال است سراپای ترا</p></div></div>