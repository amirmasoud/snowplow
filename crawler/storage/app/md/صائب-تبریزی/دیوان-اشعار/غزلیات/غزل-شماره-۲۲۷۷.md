---
title: >-
    غزل شمارهٔ ۲۲۷۷
---
# غزل شمارهٔ ۲۲۷۷

<div class="b" id="bn1"><div class="m1"><p>لب هیچ و دهان هیچ و کمر هیچ و میان هیچ</p></div>
<div class="m2"><p>چون بید ندارد ثمر آن سرو روان هیچ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندیشه جمعیت دل فکر محال است</p></div>
<div class="m2"><p>شیرازه نگیرد به خود اوراق خزان هیچ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در چشم جهان ریخت نمک صبح قیامت</p></div>
<div class="m2"><p>چشم تو نشد سیر ازین خواب گران هیچ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون تاک درین باغچه چندان که گرستم</p></div>
<div class="m2"><p>نگشود مرا عقده ای از رشته جان هیچ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چند که دندان تو از خوردن نان ریخت</p></div>
<div class="m2"><p>حرص تو نشد سیر ز اندیشه نان هیچ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچشم حبابم که ازین بحر گهرخیز</p></div>
<div class="m2"><p>غیر از سخن پوچ ندارم به دهان هیچ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز گریه بیحاصلی و جز ناله افسوس</p></div>
<div class="m2"><p>نگشود مرا از دل و چشم نگران هیچ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با خصم زبون پنجه زدن نیست ز مردی</p></div>
<div class="m2"><p>صائب سخن چرخ میاور به زبان هیچ</p></div></div>