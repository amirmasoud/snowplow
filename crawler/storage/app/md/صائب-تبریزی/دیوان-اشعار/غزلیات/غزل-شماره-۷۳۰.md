---
title: >-
    غزل شمارهٔ ۷۳۰
---
# غزل شمارهٔ ۷۳۰

<div class="b" id="bn1"><div class="m1"><p>گر قابل ملال نیم، شاد کن مرا</p></div>
<div class="m2"><p>ویران اگر نمی کنی، آباد کن مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز افتادگی مباد شوم بار خاطرت</p></div>
<div class="m2"><p>تا هست پای رفتنی آزاد کن مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواری کشیدگان به عزیزی رسند زود</p></div>
<div class="m2"><p>زان پیشتر که یاد کنی یاد کن مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر داد من نمی دهی ای پادشاه حسن</p></div>
<div class="m2"><p>یکباره پایمال ز بیداد کن مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حیف است اگر چه کذب رود بر زبان تو</p></div>
<div class="m2"><p>از وعده دروغ، دلی شاد کن مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیوسته است سلسله خاکیان به هم</p></div>
<div class="m2"><p>بر هر زمین که سایه کنی، یاد کن مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاید به گرد قافله بیخودان رسم</p></div>
<div class="m2"><p>ای پیر دیر، همتی امداد کن مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پر کن ز باده تا خط بغداد جام من</p></div>
<div class="m2"><p>فرمانروای خطه بغداد کن مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درمانده ام به دست دل همچو سنگ خود</p></div>
<div class="m2"><p>سرپنجه تصرف فرهاد کن مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گشته است خون مرده جهان ز آرمیدگی</p></div>
<div class="m2"><p>دیوانه قلمرو ایجاد کن مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بی حاصلی ز سنگ ملامت بود حصار</p></div>
<div class="m2"><p>چون سرو و بید ز ثمر آزاد کن مرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دارد به فکر صائب من گوش، عالمی</p></div>
<div class="m2"><p>یک ره تو نیز گوش به فریاد کن مرا</p></div></div>