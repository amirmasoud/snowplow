---
title: >-
    غزل شمارهٔ ۲۲۹۸
---
# غزل شمارهٔ ۲۲۹۸

<div class="b" id="bn1"><div class="m1"><p>روشندلان به هر که رسیدند همچو صبح</p></div>
<div class="m2"><p>دادند جان، نفس نکشیدند همچو صبح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکر خدا که عاقبت کار، عاشقان</p></div>
<div class="m2"><p>پیراهنی به صدق دریدند همچو صبح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جمعی که پی به داغ مکافات برده اند</p></div>
<div class="m2"><p>یک گل فزون ز باغ نچیدند همچو صبح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از گرد کینه صاف بود آبگینه ام</p></div>
<div class="m2"><p>ناف مرا به مهر بریدند همچو صبح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا شیشه گردن از سر دیوار خم کشید</p></div>
<div class="m2"><p>مستان بغل گشاده دویدند همچو صبح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صائب خموش باش که خورشید طلعتان</p></div>
<div class="m2"><p>بر ما رقم به صدق کشیدند همچو صبح</p></div></div>