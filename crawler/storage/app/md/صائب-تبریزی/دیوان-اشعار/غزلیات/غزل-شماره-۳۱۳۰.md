---
title: >-
    غزل شمارهٔ ۳۱۳۰
---
# غزل شمارهٔ ۳۱۳۰

<div class="b" id="bn1"><div class="m1"><p>دل ظالم از آب چشم مظلومان نیندیشد</p></div>
<div class="m2"><p>ز اشک هیزم تر آتش سوزان نیندیشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه پروا دارد از سنگ ملامت هر که مجنون شد؟</p></div>
<div class="m2"><p>که از کوه گران سیل سبک جولان نیندیشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دهد در دامن خود لاله و گل جای شبنم را</p></div>
<div class="m2"><p>عذار شرمناک از دیده حیران نیندیشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خط پاکی است از موج حوادث چرب نرمیها</p></div>
<div class="m2"><p>که چون هموار گردد تیغ از سوهان نیندیشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیانت بر تو دارد تلخ یاد روز محشر را</p></div>
<div class="m2"><p>که هر کس خود حساب افتاد از دیوان نیندیشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شود فرمانروا در مصر عزت پاکدامانی</p></div>
<div class="m2"><p>که همچون ماه کنعان از چه و زندان نیندیشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به ابراهیم ادهم فقر از شاهی گوارا شد</p></div>
<div class="m2"><p>که طوفان دیده از تردستی باران نیندیشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زبی برگان خزان سنگدل رنگی نمی دارد</p></div>
<div class="m2"><p>زرهزن هر که چون شمشیر شد عریان نیندیشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فروغ عاریت از هر نسیمی می شود لرزان</p></div>
<div class="m2"><p>چراغ لاله از افشاندن دامان نیندیشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زدست ناتوانان هیچ دستی نیست بالاتر</p></div>
<div class="m2"><p>درین پیکار زال از رستم دستان نیندیشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شود رطل گران دریاکشان را لنگر تمکین</p></div>
<div class="m2"><p>نهنگ پر دل از بدمستی طوفان نیندیشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شب مهتاب پای دزد را کوتاه می سازد</p></div>
<div class="m2"><p>دل روشن زمکر و حیله شیطان نیندیشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نباشد بیضه فولاد را اندیشه از دندان</p></div>
<div class="m2"><p>دل سخت بتان از ناله و افغان نیندیشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دلم از خط به لعل روح بخش یار می لرزد</p></div>
<div class="m2"><p>اگرچه از سیاهی چشمه حیوان نیندیشد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در نگشاده سختی می کشد از هر سبکدستی</p></div>
<div class="m2"><p>ز زخم سنگ، صائب پسته خندان نیندیشد</p></div></div>