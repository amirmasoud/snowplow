---
title: >-
    غزل شمارهٔ ۳۹۶۶
---
# غزل شمارهٔ ۳۹۶۶

<div class="b" id="bn1"><div class="m1"><p>به گریه نقطه خال تو از نظر نرود</p></div>
<div class="m2"><p>که داغ لاله به خونابه جگر نرود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز چاه خوبی یوسف نمی شودخس پوش</p></div>
<div class="m2"><p>به بند حسن گلوسوز از شکر نرود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه سود دولت دنیا خسیس طبعان را</p></div>
<div class="m2"><p>که حرص از آتش سوزان به تاج زر نرود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دل به باده روشن نمی رود غم عشق</p></div>
<div class="m2"><p>به آفتاب کلف از رخ قمر نرود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تمام روی زمین بی نزاع وجنگ وجدل</p></div>
<div class="m2"><p>ازان کس است که از حد خود بدر نرود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که می برد خبر کشتن مرا بیرون</p></div>
<div class="m2"><p>ز محفلی که ز دلبستگی خبر نرود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به زیر برگ خزیده است میوه ام جایی</p></div>
<div class="m2"><p>کز آفتاب رگ خامی از ثمر نرود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به خاص وعام بزرگانه می دهد پهلو</p></div>
<div class="m2"><p>چرا به پای خم می کسی به سرنرود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تسلی دل صائب به وصل ممکن نیست</p></div>
<div class="m2"><p>که تلخکامی بادام از شکر نرود</p></div></div>