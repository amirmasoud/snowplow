---
title: >-
    غزل شمارهٔ ۹۵۰
---
# غزل شمارهٔ ۹۵۰

<div class="b" id="bn1"><div class="m1"><p>از تهیدستی ز بی برگان خجالت کار ماست</p></div>
<div class="m2"><p>سر به زیر انداختن چون بید مجنون بار ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش ما جز بیخودی دیگر متاعی باب نیست</p></div>
<div class="m2"><p>خودفروشی بنده بی صاحب بازار ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این که از ما دست سیلاب حوادث کوته است</p></div>
<div class="m2"><p>نیست از گردنکشی، از پستی دیوار ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پنبه برمی گیرد از مینا می پر زور ما</p></div>
<div class="m2"><p>مهر خاموشی سپند گرمی بازار ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش ازین گر زنگ از دل می زدودند، این زمان</p></div>
<div class="m2"><p>دیدن آیینه رویان جهان زنگار ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گلشن آرا را سواد نامه سربسته نیست</p></div>
<div class="m2"><p>ورنه آن گل پیرهن در غنچه منقار ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نقش پای ما نگردد بار بردوش زمین</p></div>
<div class="m2"><p>خار را خون در دل از شوق سبکرفتار ماست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شب به چشم ما نسازد روز روشن را سیاه</p></div>
<div class="m2"><p>کلبه ما را چراغ از دیده بیدار ماست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوشه گیری را به چشم خلق شیرین کرده است</p></div>
<div class="m2"><p>خال مشکینی که در کنج دهان یار ماست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون سبو در آشنایی ها گرانجان نیستیم</p></div>
<div class="m2"><p>زود می گردد سبک، دوشی که زیر بار ماست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر چه ما از چرب نرمی مومیایی گشته ایم</p></div>
<div class="m2"><p>هر که را دیدیم صائب در شکست کار ماست</p></div></div>