---
title: >-
    غزل شمارهٔ ۲۷۳۱
---
# غزل شمارهٔ ۲۷۳۱

<div class="b" id="bn1"><div class="m1"><p>عشق فارغبالم از اندیشه دنیا نمود</p></div>
<div class="m2"><p>وقت آن کس خوش که شغل عشق را پیدا نمود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسن شوخ از پرده پوشی می شود بی پرده تر</p></div>
<div class="m2"><p>دختر رز خویش را در چادر مینا نمود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر بسر چشم غزالان چشم قربانی شده است</p></div>
<div class="m2"><p>محمل لیلی مگر جولان درین صحرا نمود؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حسن بالادست را مشاطه ای چون عشق نیست</p></div>
<div class="m2"><p>تنگی آغوش قمری سرو را رعنا نمود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهربان شد آسمان از چرب نرمیهای من</p></div>
<div class="m2"><p>نخل مومین ریشه محکم در دل خارا نمود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاک نیلی می شود از سایه دیوانه ام</p></div>
<div class="m2"><p>بس که سنگ کودکان در پیکر من جا نمود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برده است از کار دستم را جدایی، ورنه من</p></div>
<div class="m2"><p>می توانستم شکایت نامه ها انشا نمود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آشنا سوزست برق گوهر نایاب عشق</p></div>
<div class="m2"><p>برنیاید هر که غواصی درین دریا نمود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از سبک مغزی است سودای اقامت در جهان</p></div>
<div class="m2"><p>کوه نتوانست پا قایم درین صحرا نمود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تازه شد از سوده الماس داغ کهنه ام</p></div>
<div class="m2"><p>این جواهر سرمه صائب چشم من بینا نمود</p></div></div>