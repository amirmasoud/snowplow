---
title: >-
    غزل شمارهٔ ۵۷۸۳
---
# غزل شمارهٔ ۵۷۸۳

<div class="b" id="bn1"><div class="m1"><p>از آفتاب رنگ نبازد ستاره ام</p></div>
<div class="m2"><p>دل زنده از محیط برآید شراره ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید محشرست دل آتشین من</p></div>
<div class="m2"><p>صبح قیامت است گریبان پاره ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نور نگاه چشم غزالان وحشیم</p></div>
<div class="m2"><p>هم در میان مردم و هم بر کناره ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن بیدلم که کشتی طوفان رسیده بود</p></div>
<div class="m2"><p>در طفلی از تپیدن دل گاهواره ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رطل گران خاک بود نقش پای من</p></div>
<div class="m2"><p>تا از شراب عشق تو مست گذاره ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا قامت تو سایه نیفکند بر سرم</p></div>
<div class="m2"><p>روشن نگشت معنی عمر دوباره ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد برگ زرد و رنگ نگرداند میوه ام</p></div>
<div class="m2"><p>عمرم تمام گشت و همان نیمکاره ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون موج از تردد خاطر درین محیط</p></div>
<div class="m2"><p>صائب یکی شده است میان و کناره ام</p></div></div>