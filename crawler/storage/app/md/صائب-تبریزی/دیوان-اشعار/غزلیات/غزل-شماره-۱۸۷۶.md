---
title: >-
    غزل شمارهٔ ۱۸۷۶
---
# غزل شمارهٔ ۱۸۷۶

<div class="b" id="bn1"><div class="m1"><p>با چهره شکفته گلستان چه حاجت است؟</p></div>
<div class="m2"><p>با خط و زلف، سنبل و ریحان چه حاجت است؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی ترا به زلف پریشان چه حاجت است؟</p></div>
<div class="m2"><p>آتش چو سرکش است به دامان چه حاجت است؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دریای آرمیده به آشوب تشنه است</p></div>
<div class="m2"><p>شور مرا به سلسله جنبان چه حاجت است؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دامن است شعله جواله بی نیاز</p></div>
<div class="m2"><p>گرداب را به شورش طوفان چه حاجت است؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آتش گل همیشه بهارست عشق را</p></div>
<div class="m2"><p>پروانه را به سیر گلستان چه حاجت است؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زندان بود به مردم خودبین سواد شهر</p></div>
<div class="m2"><p>از خود رمیده را به بیابان چه حاجت است؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عالم به چشم آینه گردد سیه ز آب</p></div>
<div class="m2"><p>دل زنده را به چشمه حیوان چه حاجت است؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باشد ز چوب منع دربسته بی نیاز</p></div>
<div class="m2"><p>با جبهه گرفته به دربان چه حاجت است؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از سینه های چاک بود فتح باب دل</p></div>
<div class="m2"><p>این در چو باز شد به گریبان چه حاجت است؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ریزش چه کار با دل بی آرزو کند؟</p></div>
<div class="m2"><p>آن را که تخم سوخت به باران چه حاجت است؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گلچین چه گل ز گلشن دربسته می برد؟</p></div>
<div class="m2"><p>با روی شرمناک، نگهبان چه حاجت است؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اکنون که سوخت گرمی پرواز بال من</p></div>
<div class="m2"><p>دیگر مرا به شمع شبستان چه حاجت است؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از دل، گرفتگی به تماشا نمی رود</p></div>
<div class="m2"><p>نقش و نگار بر در زندان چه حاجت است؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ما خون خود حلال به تیغ تو کرده ایم</p></div>
<div class="m2"><p>از خاک ما کشیدن دامان چه حاجت است؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پیری ز میل سیب زنخدان حجاب نیست</p></div>
<div class="m2"><p>در میوه بهشتی به دندان چه حاجت است؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شد رهنما به حق چو مرا درد بی دوا</p></div>
<div class="m2"><p>صائب دگر به ناز طبیبان چه حاجت است؟</p></div></div>