---
title: >-
    غزل شمارهٔ ۱۷۸۴
---
# غزل شمارهٔ ۱۷۸۴

<div class="b" id="bn1"><div class="m1"><p>ز خط غبار بر آن لعل آتشین ننشست</p></div>
<div class="m2"><p>ز برق حسن، سیاهی بر این نگین ننشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گرد راه تو بیباک، چشم بد مرساد!</p></div>
<div class="m2"><p>که همچو گرد یتیمی به هر جبین ننشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به محفل تو کسی داد بیقراری داد</p></div>
<div class="m2"><p>که تا نسوخت چو پروانه، بر زمین ننشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز ترکتاز قیامت نکرد قامت راست</p></div>
<div class="m2"><p>به هیچ سینه غبار غم این چنین ننشست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه نقش دید ندانم دل رمیده من؟</p></div>
<div class="m2"><p>که یک نفس به نگین خانه، این نگین ننشست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حدیث کوه غم عاشقان نسیم صباست</p></div>
<div class="m2"><p>ترا که قطره شبنم به یاسمین ننشست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نماند بوته خاری جهان امکان را</p></div>
<div class="m2"><p>که بر امید تو صیاد در کمین ننشست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنین که سنگ ملامت نشست بر سر من</p></div>
<div class="m2"><p>به تاج پادشهان گوهر این چنین ننشست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قدم ز غمکده اختیار بیرون نه</p></div>
<div class="m2"><p>که در بهشت رضا هیچ کس غمین ننشست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به نوشخند قناعت کجا شوی خرسند؟</p></div>
<div class="m2"><p>ترا که حرص به صد خانه انگبین ننشست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو زلف و خط کس ازان روی کامیاب نشد</p></div>
<div class="m2"><p>به دور حسن تو نقش کسی چنین ننشست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دلم به حلقه زلف تو تا نظر انداخت</p></div>
<div class="m2"><p>دگر به هیچ نگین خانه، این نگین ننشست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همین نه روز من از خط سیاه شد صائب</p></div>
<div class="m2"><p>که نقش یار هم از خط عنبرین ننشست</p></div></div>