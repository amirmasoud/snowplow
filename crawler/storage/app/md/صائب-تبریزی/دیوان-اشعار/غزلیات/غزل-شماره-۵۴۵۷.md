---
title: >-
    غزل شمارهٔ ۵۴۵۷
---
# غزل شمارهٔ ۵۴۵۷

<div class="b" id="bn1"><div class="m1"><p>روزگاری در رگ جان پیچ و تاب افکنده‌ایم</p></div>
<div class="m2"><p>تا ز روی شاهد معنی نقاب افکنده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم‌خیالان را به همت دستگیری می‌کنیم</p></div>
<div class="m2"><p>نیست از غفلت اگر خود را به خواب افکنده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همره کاهل گرانی می‌برد از پای سعی</p></div>
<div class="m2"><p>سیل را در ره مکرر از شتاب افکنده‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما ز روشن گوهری از پله افتادگی</p></div>
<div class="m2"><p>سر چو شبنم در کنار آفتاب افکنده‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از لب میگون او قانع به می گردیده‌ایم</p></div>
<div class="m2"><p>مهر گل از دوربینی بر گلاب افکنده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچ کس در خاکساری نیست چون ما خوش‌عنان</p></div>
<div class="m2"><p>چشم پیش پای مردم چون رکاب افکنده‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر دیدارت نظر را شست‌و‌شویی می‌دهیم</p></div>
<div class="m2"><p>بی‌تو گر چشمی به روی آفتاب افکنده‌ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عارفان دل ساده می‌سازند از نقش و نگار</p></div>
<div class="m2"><p>ما نظر از دل‌سیاهی بر کتاب افکنده‌ایم</p></div></div>