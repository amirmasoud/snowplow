---
title: >-
    غزل شمارهٔ ۷۴۴
---
# غزل شمارهٔ ۷۴۴

<div class="b" id="bn1"><div class="m1"><p>مگذار بر زمین دل شبها پیاله را</p></div>
<div class="m2"><p>از باده برگ لاله کن این داغ لاله را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نتوان ز من گرفت به عمر دراز خضر</p></div>
<div class="m2"><p>کیفیت بلند شراب دو ساله را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقی چنان خوش است که گر می کمی کند</p></div>
<div class="m2"><p>پر می کند به گردش چشمی پیاله را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اشک است غمگسار دل داغدیدگان</p></div>
<div class="m2"><p>شبنم کند خنک جگر گرم لاله را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تأثیر ناله در دل سنگین فزونترست</p></div>
<div class="m2"><p>در کوه، جلوه های دوبالاست ناله را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پروانه نجات بود درد و داغ عشق</p></div>
<div class="m2"><p>شیرازه کن به رشته جان این رساله را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رویی کز او ستاره من سوخت چون سپند</p></div>
<div class="m2"><p>در خون کشید مردمک چشم هاله را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نتوان به چشم یار ز شوخی نگاه کرد</p></div>
<div class="m2"><p>وحشت بود ز سایه خود این غزاله را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رخسار او ز گریه من خط سبز یافت</p></div>
<div class="m2"><p>خون مشک می شود به جگر برگ لاله را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صائب توان به زور شراب کهن کشید</p></div>
<div class="m2"><p>از سینه ریشه های غم دیرساله را</p></div></div>