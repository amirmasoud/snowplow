---
title: >-
    غزل شمارهٔ ۱۵۵۹
---
# غزل شمارهٔ ۱۵۵۹

<div class="b" id="bn1"><div class="m1"><p>دیدن تازه خطان شاهد بالغ نظری است</p></div>
<div class="m2"><p>واله آیه رحمت نشدن بی بصری است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر خود از خجلت آن موی میان می پیچد</p></div>
<div class="m2"><p>مور هر چند که مشهور به نازک کمری است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناله من چه کند با تو که شور محشر</p></div>
<div class="m2"><p>کوه تمکین ترا قهقهه کبک دری است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون دل از دامن صحرای جنون بردارم؟</p></div>
<div class="m2"><p>من که هر موج سرابم به نظر بال پری است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر دل من که زبی همنفسی غنچه شده است</p></div>
<div class="m2"><p>نفس سوخته عشق نسیم سحری است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو خورشید به یک چشم جهان را دیدن</p></div>
<div class="m2"><p>نیست از نقص بصیرت که ز روشن گهری است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از بصیرت نبود خرج تماشا گشتن</p></div>
<div class="m2"><p>چشم پوشیدن از اوضاع جهان دیده وری است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ساده لوحان حریصش به گره می بندند</p></div>
<div class="m2"><p>گر چه چون ریگ روان خرده جانها سفری است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر کمالی است در اینجا به زوال آبستن</p></div>
<div class="m2"><p>تیغ خود را چو سپر کرد مه نو سپری است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست ممکن که نلرزد ز شکستن بر خویش</p></div>
<div class="m2"><p>شیشه هر چند که در کارگه شیشه گری است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این که از شهپر طاوس مگس ران سازند</p></div>
<div class="m2"><p>در زمین سیه هند، گل جلوه گری است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صائب از داغ غریبی به وطن می سوزد</p></div>
<div class="m2"><p>همچو یعقوب مقیمی که عزیزش سفری است</p></div></div>