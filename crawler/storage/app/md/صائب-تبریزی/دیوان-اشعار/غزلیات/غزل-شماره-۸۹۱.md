---
title: >-
    غزل شمارهٔ ۸۹۱
---
# غزل شمارهٔ ۸۹۱

<div class="b" id="bn1"><div class="m1"><p>نکند باده شب، سوختگان را سیراب</p></div>
<div class="m2"><p>تشنه در خواب شود تشنه تر از خوردن آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش از آن دم که کند خون شفق را شب مشک</p></div>
<div class="m2"><p>همچو خورشید برافروز رخ از باده ناب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در بهاران مشو از باده گلگون غافل</p></div>
<div class="m2"><p>که ز هر لاله در آتش بودش نعل شتاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به جوانی نتوانی چو رسیدن، باری</p></div>
<div class="m2"><p>جهد کن عهد جوانی جهان را دریاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جوانی نتوانی چو رسیدن، باری</p></div>
<div class="m2"><p>جهد کن عهد جوانی جهان را دریاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا توان نغمه سیراب شنید از بلبل</p></div>
<div class="m2"><p>مشنو زمزمه خشک نی و چنگ و رباب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر چه داری گرو باده کن ایام بهار</p></div>
<div class="m2"><p>در خزان از گرو باده برآور اسباب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا بود نغمه بلبل، مشنو ساز دگر</p></div>
<div class="m2"><p>تا بود دفتر گل، روی میاور به کتاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با نفس سوختگی لاله برآمد از سنگ</p></div>
<div class="m2"><p>به کدورت چه فرو رفته ای، ای خانه خراب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شور بلبل نمکی نیست که دایم باشد</p></div>
<div class="m2"><p>نمکی چند ازین شور بیفشان به کباب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عارفان غافل از افسانه دنیا نشوند</p></div>
<div class="m2"><p>بلبلان را نکند صبح بهاران در خواب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نوبت خواب به شب های دی افکن صائب</p></div>
<div class="m2"><p>که حرام است درین فصل به بیداران خواب</p></div></div>