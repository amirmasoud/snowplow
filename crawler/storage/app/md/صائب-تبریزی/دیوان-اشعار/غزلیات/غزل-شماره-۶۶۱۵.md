---
title: >-
    غزل شمارهٔ ۶۶۱۵
---
# غزل شمارهٔ ۶۶۱۵

<div class="b" id="bn1"><div class="m1"><p>لاله است این که از جگر خاک سرزده؟</p></div>
<div class="m2"><p>یا لیلی است سر ز سیه خانه برزده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عنبر به جام باده گلگون فکنده اند؟</p></div>
<div class="m2"><p>یا بخت ماست غوطه به خون جگر زده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در چادر شکوفه نهفته است برگ سبز؟</p></div>
<div class="m2"><p>یا طوطی است غوطه به تنگ شکر زده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از اشتیاق روی تو ای نوبهار حسن</p></div>
<div class="m2"><p>دستی است شاخ گل که گلستان به سرزده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون وا نمی کند گره از دل، چه حاصل است</p></div>
<div class="m2"><p>زان دستها که سرو به طرف کمر زده؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صائب چو زخم سینه گل بخیه گیر نیست</p></div>
<div class="m2"><p>زخمی که روزگار مرا بر جگر زده</p></div></div>