---
title: >-
    غزل شمارهٔ ۵۷۳۹
---
# غزل شمارهٔ ۵۷۳۹

<div class="b" id="bn1"><div class="m1"><p>بس است روی دلی مشت استخوان مرا</p></div>
<div class="m2"><p>ز چشم شیر فتد برق در نیستانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز شرم ناله ام از بس به خاک ریخته است</p></div>
<div class="m2"><p>زبان چو برگ توان رفت از گلستانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه ذوق بودن و نه روی باز گردیدن</p></div>
<div class="m2"><p>چو خنده بر لب ماتم رسیده حیرانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همین بس است که در آستانه عشقم</p></div>
<div class="m2"><p>اگر چه سوختنی همچو چوب دربانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا به کنج قفس بر ز بوستان صائب</p></div>
<div class="m2"><p>که مغز می شود از بوی گل پریشانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر چه نیک نیم، خاک پای نیکانم</p></div>
<div class="m2"><p>عجب که تشنه بمانم، سفال ریحانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو رشته قیمتم از پهلوی گهر باشد</p></div>
<div class="m2"><p>اگر گهر نبود من به خاک یکسانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شوم به خانه مردم نخوانده چون مهمان</p></div>
<div class="m2"><p>که من به خانه خود چون نخوانده مهمانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز ابر آب گرفتن وظیفه صدف است</p></div>
<div class="m2"><p>من آن نیم که به هر سفله لب بجنبانم</p></div></div>