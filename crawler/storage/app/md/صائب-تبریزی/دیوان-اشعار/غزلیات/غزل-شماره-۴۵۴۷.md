---
title: >-
    غزل شمارهٔ ۴۵۴۷
---
# غزل شمارهٔ ۴۵۴۷

<div class="b" id="bn1"><div class="m1"><p>ای زیاد لعل میگون تو کام جان لذیذ</p></div>
<div class="m2"><p>در فراقت در دل شبهای تار افغان لذیذ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه در شیرینی و لذت مثل آمد نبات</p></div>
<div class="m2"><p>حاش لله کان بود همچون لب جانان لذیذ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از در و دیوار جانان حسن می ریزد مدام</p></div>
<div class="m2"><p>زان زلیخا را بود نظاره زندان لذیذ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بردن نام خدنگت کام جان شیرین کند</p></div>
<div class="m2"><p>تیر مژگان ترااز بس بود پیکان لذیذ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چه آب تیغ او باشد گوارادر مذاق</p></div>
<div class="m2"><p>لیک صائب راست آب خنجر مژگان لذیذ</p></div></div>