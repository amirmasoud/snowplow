---
title: >-
    غزل شمارهٔ ۵۵۶۵
---
# غزل شمارهٔ ۵۵۶۵

<div class="b" id="bn1"><div class="m1"><p>دو عالم شد ز یاد آن سمن سیما فراموشم</p></div>
<div class="m2"><p>به خاطر آنچه می گردید شد یکجا فراموشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمی گردد ز خاطر محو چون مصرع بلند افتد</p></div>
<div class="m2"><p>شدم خاک و نشد آن قامت رعنا فراموشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه فارغبال می گشتم درین عالم اگر می شد</p></div>
<div class="m2"><p>غم امروز چون اندیشه فردا فراموشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم آن کس که دور افتاد گردد از فراموشان</p></div>
<div class="m2"><p>من از خواری به پیش چشم از دلها فراموشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سپند او شدم تا از خودی آسان برون آیم</p></div>
<div class="m2"><p>ندانستم شود برخاستن از جا فراموشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو گوهر گر چه در مهد صدف عمری است در خوابم</p></div>
<div class="m2"><p>نشد زین خواب سنگین رغبت دریا فراموشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز من یک ذره تا در سنگ باشد چون شرر باقی</p></div>
<div class="m2"><p>نخواهد شد هوای عالم بالا فراموشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه از منزل نه از ره، نه ز همراهان خبر دارم</p></div>
<div class="m2"><p>من آن کورم که رهبر کرده در صحرا فراموشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به یا دمن که پیش آن فرامشکار می افتد</p></div>
<div class="m2"><p>که با صد رشته کرد آن زلف بی پروا فراموشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به استغنا توان خون در جگر کردن نکویان را</p></div>
<div class="m2"><p>ولی از دیدنش می گردد استغنا فراموشم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا این سرافرازی در میان دور گردان بس</p></div>
<div class="m2"><p>که کرد آن سنگدل از دوستان تنها فراموشم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به اشکی می توان شستن ز خاکم دعوی خون را</p></div>
<div class="m2"><p>پس از گشتن مکن ای شمع بی پروا فراموشم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نیم من دانه ای صائب بساط آفرینش را</p></div>
<div class="m2"><p>که در خاک فراموشان کند دنیا فراموشم</p></div></div>