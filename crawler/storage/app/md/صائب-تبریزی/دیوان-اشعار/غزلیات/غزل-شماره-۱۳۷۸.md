---
title: >-
    غزل شمارهٔ ۱۳۷۸
---
# غزل شمارهٔ ۱۳۷۸

<div class="b" id="bn1"><div class="m1"><p>قطره خونی شد از دست نگارینش چکید</p></div>
<div class="m2"><p>بس که از دستم به ناز آن نازنین دل را گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست کوتاه مرا شد تخته مشق امید</p></div>
<div class="m2"><p>شانه تا دامان آن مشکین سلاسل را گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش عاشق جان ندارد قدر، ورنه می توان</p></div>
<div class="m2"><p>از نگاه عجز تیغ از دست قاتل را گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوه تمکین برنمی آید به دست انداز شوق</p></div>
<div class="m2"><p>جذبه مجنون عنان از دست محمل را گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرکشان را عشق می سازد به افسون چرب نرم</p></div>
<div class="m2"><p>زیر پر، پروانه آخر شمع محفل را گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مفت شیطانند غفلت پیشگان روزگار</p></div>
<div class="m2"><p>سگ به آسانی تواند صید غافل را گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طاعتی بالاتر از دلجویی درویش نیست</p></div>
<div class="m2"><p>دست خود بوسید هر کس دست سایل را گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اینقدر تمهید در تسخیر ما در کار نیست</p></div>
<div class="m2"><p>از نگاهی می توان از دست ما دل را گرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در طلب سستی مکن صائب که از صدق طلب</p></div>
<div class="m2"><p>دست من در آستین دامان منزل را گرفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر که در دریای هستی دامن دل را گرفت</p></div>
<div class="m2"><p>بی تردد موجه اش دامان ساحل را گرفت</p></div></div>