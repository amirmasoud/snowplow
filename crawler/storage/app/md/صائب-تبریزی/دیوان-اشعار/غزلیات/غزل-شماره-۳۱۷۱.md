---
title: >-
    غزل شمارهٔ ۳۱۷۱
---
# غزل شمارهٔ ۳۱۷۱

<div class="b" id="bn1"><div class="m1"><p>دهان تنگ آن شیرین پسر پنهان نمی ماند</p></div>
<div class="m2"><p>ندارد گرچه اصلی این خبر پنهان نمی ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر عریان شود، ورنه چو گل صد جامه گر پوشد</p></div>
<div class="m2"><p>صفای پیکر آن سیمبر پنهان نمی ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فروغ عشق از سیمای عاشق می شود پیدا</p></div>
<div class="m2"><p>درین ابر تنک نور قمر پنهان نمی ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز زیر دامن مجمر شمیم عود رسوا شد</p></div>
<div class="m2"><p>هنرور گر شود پنهان، هنر پنهان نمی ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لب از اظهار راز عشق بستم، گرچه می دانم</p></div>
<div class="m2"><p>زشوخی در دل سنگ این شرر پنهان نمی ماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندارد آتش سوزنده ظرف بو نهان کردن</p></div>
<div class="m2"><p>عیار خلق مردم در سفر پنهان نمی ماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همانا تخم ما امیدواران رزق قارون شد</p></div>
<div class="m2"><p>وگرنه دانه در خاک اینقدر پنهان نمی ماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حدیث اهل دل مشهور عالم می شود صائب</p></div>
<div class="m2"><p>زدریا چون برون آید گهر پنهان نمی ماند</p></div></div>