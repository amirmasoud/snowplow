---
title: >-
    غزل شمارهٔ ۱۷۹۰
---
# غزل شمارهٔ ۱۷۹۰

<div class="b" id="bn1"><div class="m1"><p>هزار بار درآیم اگر به خانه دوست</p></div>
<div class="m2"><p>به کوچه غلط اندازدم بهانه دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین که شوق مرا بیقرار ساخته است</p></div>
<div class="m2"><p>عجب که دل بنشیند مرا به خانه دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فسانه ای است که افسانه خواب می آرد</p></div>
<div class="m2"><p>به چشم خواب نمک می زند فسانه دوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز باده طبع ستم دوست مهربان نشود</p></div>
<div class="m2"><p>ز آب، رنگ نبازد گل بهانه دوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فغان که شرم محبت امان نداد مرا</p></div>
<div class="m2"><p>که بوسه ای بربایم ز آستانه دوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به خال، چشم سیه ساختم ندانستم</p></div>
<div class="m2"><p>که دام مکر نهفته است زیر دانه دوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تلاش بیهده ای می کند سر خورشید</p></div>
<div class="m2"><p>فتاده است بلند، آستان خانه دوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به صبر خویش مکن تکیه از غرور که طور</p></div>
<div class="m2"><p>سپندوار به رقص آمد از ترانه دوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به چشم همت سرشار چون دو دست تهی است</p></div>
<div class="m2"><p>متاع هر دو جهان در قمارخانه دوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا به خاک در دوست آشنایی نیست</p></div>
<div class="m2"><p>به آشنایی دل می روم به خانه دوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز شغل عشق چه اندیشه می کنی صائب؟</p></div>
<div class="m2"><p>خمار صبح ندارد می شبانه دوست</p></div></div>