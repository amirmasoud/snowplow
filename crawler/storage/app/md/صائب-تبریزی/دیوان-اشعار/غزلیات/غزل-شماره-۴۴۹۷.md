---
title: >-
    غزل شمارهٔ ۴۴۹۷
---
# غزل شمارهٔ ۴۴۹۷

<div class="b" id="bn1"><div class="m1"><p>قد ترا سرواعتدال ندارد</p></div>
<div class="m2"><p>این خم وچم ابروی هلال ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رتبه درویش را به شاه چه نسبت</p></div>
<div class="m2"><p>دولت آزادگی زوال ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیچ دلی نیست بی غبار کدورت</p></div>
<div class="m2"><p>روی زمین چشمه زلال ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در سر این چارسو که سنگ عقیق است</p></div>
<div class="m2"><p>گوهر ما قیمت سفال ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شبنم من از رخ زوال چکیده است</p></div>
<div class="m2"><p>طاقت خورشیدبی زوال ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راستی قول سروگلشن جان است</p></div>
<div class="m2"><p>حیف که باغ تو این نهال ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب پشمینه پوش را که شناسد</p></div>
<div class="m2"><p>مهر طلا بر قبای آل ندارد</p></div></div>