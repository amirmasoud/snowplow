---
title: >-
    غزل شمارهٔ ۴۶۷۳
---
# غزل شمارهٔ ۴۶۷۳

<div class="b" id="bn1"><div class="m1"><p>این نه هاله است نمایان شده از دور قمر</p></div>
<div class="m2"><p>پیش رخسار منیر تو مه افکنده سپر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل به مضمون خط پشت لب او نرسید</p></div>
<div class="m2"><p>آه کاین حاشیه از متن بود مشکلتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهر صید دل عشاق، که چشمش مرساد</p></div>
<div class="m2"><p>گشت هر حلقه ای از خط تو گلدام دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود چون زلف پریشان دل صدپاره من</p></div>
<div class="m2"><p>گشت شیرازه اوراق دل آن موی کمر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه چنان ریشه دوانده است مرا دردل وچشم</p></div>
<div class="m2"><p>که رود سرو خرامان تو از مد نظر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در وطن دل چه خیال است گشاید صائب؟</p></div>
<div class="m2"><p>در صدف چشم محال است کند باز گهر</p></div></div>