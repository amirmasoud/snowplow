---
title: >-
    غزل شمارهٔ ۳۹۱
---
# غزل شمارهٔ ۳۹۱

<div class="b" id="bn1"><div class="m1"><p>به دنیا ساختم مشغول چشم روشن دل را</p></div>
<div class="m2"><p>به این یک مشت گل مسدود کردم روزن دل را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندانستم که خواهد رفت چندین خار در پایم</p></div>
<div class="m2"><p>شکستم بی سبب در خرقه تن سوزن دل را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فریب جسم خوردم، کشتیم در گل نشست آخر</p></div>
<div class="m2"><p>نمی ماندم به جا، گر می گرفتم دامن دل را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا گر هیزم دوزخ کند افسوس، جا دارد</p></div>
<div class="m2"><p>که بی برگ از ثمر کردم نهال ایمن دل را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلی از سنگ خارا، گوشی از آهن به دست آور</p></div>
<div class="m2"><p>که با این گوش و دل نتوان شنیدن شیون دل را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندانستم که خواهد شد سیه عالم به چشم من</p></div>
<div class="m2"><p>عبث بر باد دادم نکهت پیراهن دل را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حیات جاودانی از خدا چون خضر می خواهم</p></div>
<div class="m2"><p>که پاک از سبزه بیگانه سازم گلشن دل را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خرد را شهپر پرواز از رطل گران باشد</p></div>
<div class="m2"><p>نگیرد کوه غم دامان از خود رفتن دل را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نمی شد خشک چون دست بخیلان پرده چشمت</p></div>
<div class="m2"><p>اگر می دید یک بار آفتاب روشن دل را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نظرپرداز شد چون سرمه مغز استخوان من</p></div>
<div class="m2"><p>به آه آتشین تا نرم کردم آهن دل را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز آتش طلعتان باغ و بهاری داشتم صائب</p></div>
<div class="m2"><p>ندیدم روز خوش تا سرد کردم گلخن دل را</p></div></div>