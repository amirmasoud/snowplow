---
title: >-
    غزل شمارهٔ ۸۷۳
---
# غزل شمارهٔ ۸۷۳

<div class="b" id="bn1"><div class="m1"><p>بس که از رخسار او در پیچ و تاب است آفتاب</p></div>
<div class="m2"><p>تشنه ابرست و جویای نقاب است آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون چراغ روز می میرد برای خامشی</p></div>
<div class="m2"><p>بس کز آن رخسار روشن در حجاب است آفتاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود اگر سر دفتر مه طلعتان زین پیشتر</p></div>
<div class="m2"><p>در زمان حسن او کی در حساب است آفتاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از شفق هر صبح چون رخسار می شوید به خون؟</p></div>
<div class="m2"><p>گرنه از رخسار او داغ و کباب است آفتاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من دهم چون دیده خود آب از نظاره اش؟</p></div>
<div class="m2"><p>کز تماشای رخش چشم پر آب است آفتاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برنیارد جرعه ای دریاکشان را از خمار</p></div>
<div class="m2"><p>تشنه دیدار را موج سراب است آفتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از فتادن خویش را نتواند از مستی گرفت</p></div>
<div class="m2"><p>از کدامین می چنین مست و خراب است آفتاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون شود از مشرق زین طالع آن رشک قمر</p></div>
<div class="m2"><p>بیشتر از ماه نو پا در رکاب است آفتاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دور باشی نیست حاجت، روی آتشناک را</p></div>
<div class="m2"><p>بی نیاز از ابر و فارغ از نقاب است آفتاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا تو از خلوت صبوحی کرده بیرون آمدی</p></div>
<div class="m2"><p>چون چراغ صبحدم در اضطراب است آفتاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مه ز نور عاریت، گه لاغر و گه فربه است</p></div>
<div class="m2"><p>ایمن از تشویش و فارغ ز انقلاب است آفتاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>روی گرم از دیده شبنم نمی دارد دریغ</p></div>
<div class="m2"><p>گر چه از گردنکشی گردون جناب است آفتاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نعل ماه نو در آتش ز اشتیاق روی کیست؟</p></div>
<div class="m2"><p>در تمنای که سر گرم شتاب است آفتاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ریزش اهل کرم در پرده صائب خوشترست</p></div>
<div class="m2"><p>بیشتر فصل بهاران در سحاب است آفتاب</p></div></div>