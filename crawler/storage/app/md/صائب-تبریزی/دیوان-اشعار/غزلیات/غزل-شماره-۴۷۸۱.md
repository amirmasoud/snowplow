---
title: >-
    غزل شمارهٔ ۴۷۸۱
---
# غزل شمارهٔ ۴۷۸۱

<div class="b" id="bn1"><div class="m1"><p>عشق گرد دل فرزانه نگردد هرگز</p></div>
<div class="m2"><p>خانه دیو، پریخانه نگردد هرگز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شهپر عشق سبکسیر، شکست دل ماست</p></div>
<div class="m2"><p>آسیا بی مدد دانه نگردد هرگز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق از کوی خرابات به جایی نرود</p></div>
<div class="m2"><p>گنج دلگیر ز ویرانه نگردد هرگز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر چه در دایره چشم غزالان باشد</p></div>
<div class="m2"><p>روی مجنون ز سیه خانه نگردد هرگز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که ترجیح دهد عقل و خرد را به جنون</p></div>
<div class="m2"><p>دارم امید که دیوانه نگردد هرگز!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق با عقل محال است شود در دل جمع</p></div>
<div class="m2"><p>این دو تیغ است که همخانه نگردد هرگز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل غفلت زدگان زنده نگردد به سخن</p></div>
<div class="m2"><p>پرده خواب به افسانه نگردد هرگز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر صبا با خبر از درد غریبی باشد</p></div>
<div class="m2"><p>گرد خاکستر پروانه نگردد هرگز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آشنایی به سخن کن که پریزاد سخن</p></div>
<div class="m2"><p>آشنایی است که بیگانه نگردد هرگز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کجرویهای فلک علت کج بینی توست</p></div>
<div class="m2"><p>تا نگردد سرت،این خانه نگردد هرگز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر رخ هر که گشودند در دل صائب</p></div>
<div class="m2"><p>طالب کعبه و بتخانه نگردد هرگز</p></div></div>