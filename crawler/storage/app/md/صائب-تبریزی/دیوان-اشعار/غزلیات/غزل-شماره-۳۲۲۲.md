---
title: >-
    غزل شمارهٔ ۳۲۲۲
---
# غزل شمارهٔ ۳۲۲۲

<div class="b" id="bn1"><div class="m1"><p>نبیند زیر پای خویش، رعنا این چنین باید</p></div>
<div class="m2"><p>نپردازد به کس، آیینه سیما این چنین باید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زشکر خنده اش هر چشم موری تنگ شکر شد</p></div>
<div class="m2"><p>تکلف برطرف، لعل شکر خا این چنین باید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فلک را سبزه خوابیده داند قد رعنایش</p></div>
<div class="m2"><p>قیامت جلوگان را قد و بالا این چنین باید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زگردش ماند دور آسمان چون چشم قربانی</p></div>
<div class="m2"><p>عیار جلوه های حیرت افزا این چنین باید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به نیل چشم زخمش نیست چرخ نیلگون کافی</p></div>
<div class="m2"><p>عزیز مصر را رخسار زیبا این چنین باید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشد از دیده فرهاد غایب صورت شیرین</p></div>
<div class="m2"><p>بنای بیستون را کارفرما این چنین باید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیالش را دل سودایی من غیر می داند</p></div>
<div class="m2"><p>زمردم عاشق شوریده تنها این چنین باید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زنقش پای من روی زمین دریای آتش شد</p></div>
<div class="m2"><p>طلبکار ترا آتش نه پا این چنین باید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ندارد وادی ما لاله زاری غیر بوی خون</p></div>
<div class="m2"><p>زخود رم کرده را دامان صحرا این چنین باید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زنقش بال کوه قاف دارد بر دل وحشی</p></div>
<div class="m2"><p>گریزان از نشان خویش عنقا این چنین باید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نهادم دست تا بر دل جنون من یکی صد شد</p></div>
<div class="m2"><p>زلنگر می شود شوریده، دریا این چنین باید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نکرد از خواب حیرت جوش دل بیدار صائب را</p></div>
<div class="m2"><p>نگاه عاشقان محو تماشا این چنین باید</p></div></div>