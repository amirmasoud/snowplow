---
title: >-
    غزل شمارهٔ ۶۸۹۰
---
# غزل شمارهٔ ۶۸۹۰

<div class="b" id="bn1"><div class="m1"><p>ای آن که دل به دولت بیدار بسته ای</p></div>
<div class="m2"><p>در راه برق، سد خس و خار بسته ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای بی خبر که تقویت نفس می کنی</p></div>
<div class="m2"><p>غافل مشو که گرگ به پروار بسته ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در پیش هر که غیر خدا بسته ای کمر</p></div>
<div class="m2"><p>زنهار پاره ساز که زنار بسته ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک سو فکنده ای ز نظر پرده حیا</p></div>
<div class="m2"><p>بر دل هزار پرده زنگار بسته ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سازی روان ز هر مژه صد کاروان اشک</p></div>
<div class="m2"><p>گر وا کنند آنچه تو در بار بسته ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیلاب حادثات ترا می کند ز جا</p></div>
<div class="m2"><p>دامن اگر به دامن کهسار بسته ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تاج زرست جای تو کوتاه بین و تو</p></div>
<div class="m2"><p>دل بر صدف چو گوهر شهوار بسته ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواهی به باد داد سر سبز خود چو شمع</p></div>
<div class="m2"><p>زینسان که دل به طره طرار بسته ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جز شکوه حرفی از تو تراوش نمی کند</p></div>
<div class="m2"><p>از شکر یک قلم لب اظهار بسته ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نقد حیات داده ای از دست رایگان</p></div>
<div class="m2"><p>چون سکه دل به درهم و دینار بسته ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غیر از سیاه کردن اوراق عمر خویش</p></div>
<div class="m2"><p>صائب دگر چه طرف ز گفتار بسته ای؟</p></div></div>