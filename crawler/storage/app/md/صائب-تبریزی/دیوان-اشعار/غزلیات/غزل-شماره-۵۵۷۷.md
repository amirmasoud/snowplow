---
title: >-
    غزل شمارهٔ ۵۵۷۷
---
# غزل شمارهٔ ۵۵۷۷

<div class="b" id="bn1"><div class="m1"><p>خط شبرنگ را خوشتر ز زلف و خال می دانم</p></div>
<div class="m2"><p>من این تقویم پارین را به از امسال می دانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو کز کیفیت حسن چمن بی بهره ای، می خور</p></div>
<div class="m2"><p>که من هر شبنمی را رطل مالامال می دانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرنج از من اگر جان را به استقبال نفرستم</p></div>
<div class="m2"><p>که از جا رفتن دل را من استقبال می دانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بغیر از زیر بار عشق در زیر فلک هر کس</p></div>
<div class="m2"><p>که زیر بار دیگر می رود، حمال می دانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جنونی کز حصار شهر نتواند برون آمد</p></div>
<div class="m2"><p>من صحرانشین بازیچه اطفال می دانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غبار کلفت از معموره جسم آنقدر دارم</p></div>
<div class="m2"><p>که جغد مرگ را مرغ همایون فال می دانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز دست انداز دوران گر چه یک مشت استخوان گشتم</p></div>
<div class="m2"><p>ضمیر خلق را چون قرعه رمال می دانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو کز خط بی نصیبی عیش کن بانقطه خالش</p></div>
<div class="m2"><p>که بی خط، خال را من چشم بی دنبال می دانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ندارد فال بد راه سخن در بزم خرسندی</p></div>
<div class="m2"><p>اگر ادبار رود آرد به من، اقبال می دانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سری از پیچ و تاب زلف او بیرون نمی آرم</p></div>
<div class="m2"><p>وگرنه موبموی رشته آمال می دانم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نمی دانم چه حال است این که پیچیده است درجانم</p></div>
<div class="m2"><p>که اهل قال را صائب ز اهل حال می دانم</p></div></div>