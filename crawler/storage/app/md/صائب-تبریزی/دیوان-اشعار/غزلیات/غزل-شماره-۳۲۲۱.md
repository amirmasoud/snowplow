---
title: >-
    غزل شمارهٔ ۳۲۲۱
---
# غزل شمارهٔ ۳۲۲۱

<div class="b" id="bn1"><div class="m1"><p>تمنا از دل اهل هوس بیرون نمی آید</p></div>
<div class="m2"><p>که حرص شهد از جان مگس بیرون نمی آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به مرگ از قید تن، تن پروران را نیست آزادی</p></div>
<div class="m2"><p>که مرغ بی پر و بال از قفس بیرون نمی آیی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زخط کوتاه شد از کوی او پای هوسناکان</p></div>
<div class="m2"><p>که شب تاریک چون گردد مگس بیرون نمی آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آن وادی که من چون نقش پا از کاروان ماندم</p></div>
<div class="m2"><p>زبیم راهزن بانگ جرس بیرون نمی آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آن محفل که من بردارم از لب مهر خاموشی</p></div>
<div class="m2"><p>صدا غیر از سپند از هیچ کس بیرون نمی آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پریشان کرد سیر باغ اوراق حواسم را</p></div>
<div class="m2"><p>خوشا مرغی که از کنج قفس بیرون نمی آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگر بال و پر موجی به فریادش رسد، ورنه</p></div>
<div class="m2"><p>به دست و پا زدن از بحر خس بیرون نمی آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زعاجزنالی خود یافتم آن گنج پنهان را</p></div>
<div class="m2"><p>که از دل ناله بی فریادرس بیرون نمی آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حرامش باد رسوایی، حلالش باد مستوری</p></div>
<div class="m2"><p>می آشامی که از بیم عسس بیرون نمی آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو افتادی به بحر عشق دست و پا مزن صائب</p></div>
<div class="m2"><p>که از دریای آتش خار و خس بیرون نمی آید</p></div></div>