---
title: >-
    غزل شمارهٔ ۴۰۳
---
# غزل شمارهٔ ۴۰۳

<div class="b" id="bn1"><div class="m1"><p>ز روی لاله گون متراش خط عنبرافشان را</p></div>
<div class="m2"><p>مکن زنهار بی شیرازه دلهای پریشان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دهان شکوه ما را به حرفی می توان بستن</p></div>
<div class="m2"><p>به مویی می توان زد بخیه این زخم نمایان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز نقصان گهر باشد تکبر با فرودستان</p></div>
<div class="m2"><p>که خودداری میسر نیست گوهرهای غلطان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل از مردان رباید دام زلف شیرگیر او</p></div>
<div class="m2"><p>چراغ از چشم حیران است دایم این شبستان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر زلف پریشان را دلی چون شانه می باید</p></div>
<div class="m2"><p>که بر سر جا تواند داد صد زخم نمایان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محبت با ضعیفان گوشه چشم دگر دارد</p></div>
<div class="m2"><p>به مهر کوچک خود لطف دیگر هست شاهان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو دست از آستین بیرون کند بازیچه گردون</p></div>
<div class="m2"><p>کند دیوی برون از دست، انگشتر سلیمان را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کند چون دام زیر خاک طوق خویش را قمری</p></div>
<div class="m2"><p>به هر گلشن که افتد راه آن سرو خرامان را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برون رو از فلک تا دامن مطلب به دست آری</p></div>
<div class="m2"><p>چو طفلان چند سازی مرکب خود طرف دامان را؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به همت جسم را همرنگ جان کن در سبکروحی</p></div>
<div class="m2"><p>ببر زین فرش با خود این غبار عرش جولان را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قناعت کن به نان خشک تا بی آرزو گردی</p></div>
<div class="m2"><p>که خواهش های الوان هست نعمت های الوان را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>درین ماتم سرا تا یک نفس چون صبح مهمانی</p></div>
<div class="m2"><p>به شکر خنده شیرین دار کام تلخکامان را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ندارد عالم تجرید چون من خانه پردازی</p></div>
<div class="m2"><p>ز عریانی به تار اشک می دوزم گریبان را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>غم عالم فراوان است و من یک غنچه دل دارم</p></div>
<div class="m2"><p>چسان در شیشه ساعت کنم ریگ بیابان را؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>درین دی ماه بی برگی، که غیر از خامه صائب</p></div>
<div class="m2"><p>به فکر تازه دارد زنده دل خاک صفاهان را؟</p></div></div>