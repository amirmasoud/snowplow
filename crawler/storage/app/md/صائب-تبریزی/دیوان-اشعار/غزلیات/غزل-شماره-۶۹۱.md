---
title: >-
    غزل شمارهٔ ۶۹۱
---
# غزل شمارهٔ ۶۹۱

<div class="b" id="bn1"><div class="m1"><p>در جوش گل شراب ننوشد کسی چرا؟</p></div>
<div class="m2"><p>با رحمت خدای نجوشد کسی چرا؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا ابر نوبهار پریشان نگشته است</p></div>
<div class="m2"><p>چون رعد هر نفس نخروشد کسی چرا؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در موسم بهار، می لاله رنگ را</p></div>
<div class="m2"><p>چون لاله کاسه کاسه ننوشد کسی چرا؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرم است تا ز آتش گل سینه بهار</p></div>
<div class="m2"><p>از سنگ همچو چشمه نجوشد کسی چرا؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون دامن وصال به کوشش گرفته اند</p></div>
<div class="m2"><p>چندان که ممکن است نکوشد کسی چرا؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این شیشه ها چو ابر تنک بی طراوتند</p></div>
<div class="m2"><p>در پای خم شراب ننوشد کسی چرا؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دریا ز موج دست ستم چون برآورد</p></div>
<div class="m2"><p>پیراهن حباب نپوشد کسی چرا؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون خوردنی است کاسه زهری که قسمت است</p></div>
<div class="m2"><p>با جبهه گشاده ننوشد کسی چرا؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یاقوت یافت در جگر سنگ آب و رنگ</p></div>
<div class="m2"><p>دیگر برای رزق بکوشد کسی چرا؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غافل مشو ز حق به امید قبول خلق</p></div>
<div class="m2"><p>یوسف به سیم قلب فروشد کسی چرا؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب به شکر سینه گرمی که داده اند</p></div>
<div class="m2"><p>چون گل به خار، گرم نجوشد کسی چرا؟</p></div></div>