---
title: >-
    غزل شمارهٔ ۵۶۷
---
# غزل شمارهٔ ۵۶۷

<div class="b" id="bn1"><div class="m1"><p>روشن است از دل بی کینه ما سینه ما</p></div>
<div class="m2"><p>گوهر ماست چراغ دل گنجینه ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه در نافه ما جز جگر سوخته نیست</p></div>
<div class="m2"><p>جگر نافه بود داغ ز پشمینه ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>(صبح شنبه به نظر جلوه کند مستان را</p></div>
<div class="m2"><p>از فروغ می گلگون شب آدینه ما)</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر شود موجه دریای حوادث صیقل</p></div>
<div class="m2"><p>نیست ممکن که شود صیقلی آیینه ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل ما را بشکن گوهر اگر می خواهی</p></div>
<div class="m2"><p>که شکست است کلید در گنجینه ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جای شکرست که امسال شد از گردش چرخ</p></div>
<div class="m2"><p>چون می کهنه گوارا غم دیرینه ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم زخمی ز گرانان جهان گر نرسد</p></div>
<div class="m2"><p>خطر از سنگ ندارد دل آیینه ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر کماندار که از دست قضا قبضه گرفت</p></div>
<div class="m2"><p>می کند دست روان بر ورق سینه ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کار فانوس کند در دل شبها صائب</p></div>
<div class="m2"><p>خانه ما ز صفای دل بی کینه ما</p></div></div>