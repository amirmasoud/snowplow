---
title: >-
    غزل شمارهٔ ۲۹۹۰
---
# غزل شمارهٔ ۲۹۹۰

<div class="b" id="bn1"><div class="m1"><p>دل آزاده را هرگز غم عالم نمی‌گیرد</p></div>
<div class="m2"><p>مسیحا را کمند رشتهٔ مریم نمی‌گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگردد دام ره زیب جهان دل‌های روشن را</p></div>
<div class="m2"><p>که رنگ و بوی گلشن دامن شبنم نمی‌گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برو ناصح به کار غیر کن این چرب‌نرمی را</p></div>
<div class="m2"><p>که داغ شوخ چشم ما به خود مرهم نمی‌گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گهر بر آبروی خویش می‌لرزد، نمی‌داند</p></div>
<div class="m2"><p>که ابر بی‌نیاز ما ز دریا نم نمی‌گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمی‌چسبد به دل تن‌پروران را حرف اهل دل</p></div>
<div class="m2"><p>چو کاغذ چرب باشد نقش از خاتم نمی‌گیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی کز تنگدستی هر دم آویزد به دامانی</p></div>
<div class="m2"><p>ندانم دامن شب را چرا محکم نمی‌گیرد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مزن دست تأسف بر هم از مرگ سیه‌کاران</p></div>
<div class="m2"><p>که خون مرده را هرگز کسی ماتم نمی‌گیرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه مطلب خوشتر از پاس نفس اهل بصیرت را؟</p></div>
<div class="m2"><p>سخن را عیسی ما از لب مریم نمی‌گیرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پر کاهی است کوه درد در میزان آزادان</p></div>
<div class="m2"><p>ز بار دل قد سرو و صنوبر خم نمی‌گیرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سر هرکس که گرم از کاسه زانوی خود گردد</p></div>
<div class="m2"><p>به منت جام را صائب ز دست جم نمی‌گیرد</p></div></div>