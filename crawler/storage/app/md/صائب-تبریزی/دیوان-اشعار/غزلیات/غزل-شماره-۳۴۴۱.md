---
title: >-
    غزل شمارهٔ ۳۴۴۱
---
# غزل شمارهٔ ۳۴۴۱

<div class="b" id="bn1"><div class="m1"><p>من و راهی که ز سر سنگ نشانش باشد</p></div>
<div class="m2"><p>برق خنجر بلد راهروانش باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی عنانداری بیتابی ما خواهد کرد؟</p></div>
<div class="m2"><p>آن که از رفتن دل آب روانش باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از عقیقی است مرا بوسه توقع که سهیل</p></div>
<div class="m2"><p>یکی از جمله خونابه کشانش باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نتوان یافت ز پیچیدگی افکار مرا</p></div>
<div class="m2"><p>راه فکر من اگر موی میانش باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که چون جام درین بزم تهی چشم افتاد</p></div>
<div class="m2"><p>چشم پیوسته به دست دگرانش باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرد مهری چه کند با دل آزاده ما؟</p></div>
<div class="m2"><p>این نه سروی است که پروای خزانش باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تیر آهش ز دل سنگ ترازو گردد</p></div>
<div class="m2"><p>هر که از قامت خم گشته کمانش باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می برد تربتش از نوحه گران گویایی</p></div>
<div class="m2"><p>هر که گنجینه اسرار نهانش باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از ته دل چقدر خنده تواند کردن؟</p></div>
<div class="m2"><p>نوبهاری که به دنبال، خزانش باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حسن غافل نشود از دل عاشق صائب</p></div>
<div class="m2"><p>که کماندار توجه به نشانش باشد</p></div></div>