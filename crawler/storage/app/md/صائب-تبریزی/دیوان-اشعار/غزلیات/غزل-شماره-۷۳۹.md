---
title: >-
    غزل شمارهٔ ۷۳۹
---
# غزل شمارهٔ ۷۳۹

<div class="b" id="bn1"><div class="m1"><p>طاقت کجاست روی عرقناک دیده را؟</p></div>
<div class="m2"><p>آرام نیست کشتی طوفان رسیده را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شبنم ز باغبان نکشد منت وصال</p></div>
<div class="m2"><p>معشوق در کنار بود پاک دیده را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با هیچ بد گهر نشود چرخ سینه صاف</p></div>
<div class="m2"><p>خون است شیر، کودک پستان گزیده را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بس شنیده ام سخن ناشنیدنی</p></div>
<div class="m2"><p>گویم شنیده ام سخن ناشنیده را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی شور عشق چاشنیی با حیات نیست</p></div>
<div class="m2"><p>تلخ است زندگی ثمر نارسیده را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یاد بهشت، حلقه بیرون در بود</p></div>
<div class="m2"><p>در تنگنای گوشه دل آرمیده را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون سگ گزیده ای که نیارد در آب دید</p></div>
<div class="m2"><p>آیینه می گزد من آدم گزیده را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در پرده ماند شور من از سردی سپهر</p></div>
<div class="m2"><p>آب است شیشه جوش می نارسیده را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خونی که می خورند به از شیر مادرست</p></div>
<div class="m2"><p>مردان از محبت دنیا بریده را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شوخی که دارد از دل سنگین به کوه پشت</p></div>
<div class="m2"><p>می دید کاش صائب در خون تپیده را</p></div></div>