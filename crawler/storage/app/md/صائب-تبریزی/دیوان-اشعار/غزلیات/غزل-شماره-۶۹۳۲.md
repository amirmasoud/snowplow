---
title: >-
    غزل شمارهٔ ۶۹۳۲
---
# غزل شمارهٔ ۶۹۳۲

<div class="b" id="bn1"><div class="m1"><p>گر فکر زاد آخرت ای دوربین کنی</p></div>
<div class="m2"><p>در زیر خاک عشرت روی زمین کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر رزق خود ز بوی گل و یاسمین کنی</p></div>
<div class="m2"><p>زنبوروار خانه پر از انگبین کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون تا به چند در دلم ای نازنین کنی؟</p></div>
<div class="m2"><p>بسمل مرا به اره چین جبین کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پر زر شود چو غنچه ترا کیسه تهی</p></div>
<div class="m2"><p>دست طمع حصاری اگر ز آستین کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>انگشت هیچ کس نگذارد به حرف تو</p></div>
<div class="m2"><p>با نقش راست صلح اگر چون نگین کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>واصل شوی چو شمع به دریای نور صبح</p></div>
<div class="m2"><p>گر در گداز جسم نفس آتشین کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز آتش شود حصار تو زنبوروار موم</p></div>
<div class="m2"><p>شیرین دهان خلق اگر از انگبین کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روشن بود همیشه سیه خانه دلت</p></div>
<div class="m2"><p>صلح از چراغ اگر به چراغ آفرین کنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از چارپای جسم فرودآی چون مسیح</p></div>
<div class="m2"><p>تا چار بالش از فلک چارمین کنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در دوزخ افکنند ترا گر ز سوز عشق</p></div>
<div class="m2"><p>در هر شرار سیر بهشت برین کنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون آدم از بهشت برونت نمی کنند</p></div>
<div class="m2"><p>گر اکتفا ز رزق به نان جوین کنی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفتار را به خوبی کردار کن بدل</p></div>
<div class="m2"><p>تا چند جهد در سخن دلنشین کنی؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا کی به دست نفس دهی اختیار خویش؟</p></div>
<div class="m2"><p>در دست دیو تا به کی انگشترین کنی؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون می توان به خنده ز من جان ستد، چرا</p></div>
<div class="m2"><p>بسمل مرا به اره چین جبین کنی؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نان تو پخته است به هر جا که می روی</p></div>
<div class="m2"><p>صائب زبان خویش اگر گندمین کنی</p></div></div>