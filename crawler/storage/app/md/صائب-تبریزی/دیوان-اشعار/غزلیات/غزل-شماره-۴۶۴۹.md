---
title: >-
    غزل شمارهٔ ۴۶۴۹
---
# غزل شمارهٔ ۴۶۴۹

<div class="b" id="bn1"><div class="m1"><p>زمهرخامشی شد خرده رازم پریشانتر</p></div>
<div class="m2"><p>که زخم صبح گشت ازبخیه انجم نمایانتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه از گل نکهتی بردم نه زخمی خوردم ازخاری</p></div>
<div class="m2"><p>نسیمی زین چمن نگذشت ازمن دامن افشانتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درآغوش گلم ازغنچه خسبان برون در</p></div>
<div class="m2"><p>ندارداین گلستان شبنم ازمن پاکدامانتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به ذوقی سینه پیش تیر دلدوزش هدف سازم</p></div>
<div class="m2"><p>که گردد غنچه پیکانش از سوفار خندانتر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشد سنگ ملامت از دویدن مانع مجنون</p></div>
<div class="m2"><p>که در کهسارها سیلال می باشد شتابانتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مبین گستاخ در رخسار شرم آلوده خوبان</p></div>
<div class="m2"><p>که باشد درنیام این تیغ بی زنهار عریانتر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به گرمی گرچه اخگر به کباب تازه می چسبد</p></div>
<div class="m2"><p>به دل می چسبد آن لبهای چون یاقوت چسبانتر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگربیند غزال آن گوشه چشمی که من دیدم</p></div>
<div class="m2"><p>شود ازدیده قربانیان صدپرده حیرانتر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تمنا در دل مرغ قفس بسیار می باشد</p></div>
<div class="m2"><p>نسازد تنگدستی آرزوراتنگ میدانتر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ثمر را می کند پیوند درچشم جهان شیرین</p></div>
<div class="m2"><p>سرمنصوراز دار فنا گرددبه سامانتر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زگنج اندوختن گفتم شود طول امل ساکن</p></div>
<div class="m2"><p>ندانستم که در گوهر شود این رشته پیچانتر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شد ازموی سفید آسودگی از رشته جانم</p></div>
<div class="m2"><p>که وقت صبحدم شمع از دل شبهاست لرزانتر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بغیراز سنگ ،دندان طمع را نیست درمانی</p></div>
<div class="m2"><p>که گردد اره از چوب ملایم تیز دندانتر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنان کز برگ گل گردید رسوابوی گل صائب</p></div>
<div class="m2"><p>زمهر خامشی راز نهانم گشت عریانتر</p></div></div>