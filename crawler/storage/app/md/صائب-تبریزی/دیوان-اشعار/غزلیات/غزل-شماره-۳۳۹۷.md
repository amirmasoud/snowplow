---
title: >-
    غزل شمارهٔ ۳۳۹۷
---
# غزل شمارهٔ ۳۳۹۷

<div class="b" id="bn1"><div class="m1"><p>در حریمی که گل روی ایاغ افروزد</p></div>
<div class="m2"><p>خار در دیده آن کس که چراغ افروزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لاله تربتش آتش به ته پا دارد</p></div>
<div class="m2"><p>در دل هر که طلب شمع سراغ افروزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می شود فاخته ای جامه مینایی سرو</p></div>
<div class="m2"><p>گر چنین ناله گرمم رخ باغ افروزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روزگاری است که در ساغر خورشید، شراب</p></div>
<div class="m2"><p>آنقدر نیست که یک ذره دماغ افروزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن که ترساندم از داغ، به آن می ماند</p></div>
<div class="m2"><p>که کسی کوری پروانه چراغ افروزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که در مذهب ما غیرت مشرب دارد</p></div>
<div class="m2"><p>شب آدینه به میخانه چراغ افروزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>انفعالی که ز داغ دل من لاله کشید</p></div>
<div class="m2"><p>شرم بادش که دگر چهره باغ افروزد</p></div></div>