---
title: >-
    غزل شمارهٔ ۳۵۶۰
---
# غزل شمارهٔ ۳۵۶۰

<div class="b" id="bn1"><div class="m1"><p>یاد آن عهد که دل در خم گیسوی تو بود</p></div>
<div class="m2"><p>شب من موی تو و روز خوشم روی تو بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نور چون چشم ز پیشانی من می بارید</p></div>
<div class="m2"><p>تا مرا قبله طاعت خم ابروی تو بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از گهر بود اگر رشته من آبی داشت</p></div>
<div class="m2"><p>پرده لاغریم چربی پهلوی تو بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن که می برد مرا از خود و از راه کرم</p></div>
<div class="m2"><p>باز می داد به خود هر نفسی، بوی تو بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غمگساری که به رویم گه بیهوشی آب</p></div>
<div class="m2"><p>می زد از راه مروت، عرق روی تو بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تخم امید من آن روز برومندی داشت</p></div>
<div class="m2"><p>که سویدای دلم خال لب جوی تو بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همزبانی که غمی از دل من برمی داشت</p></div>
<div class="m2"><p>در سراپرده دل چشم سخنگوی تو بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خال رخسار جهان بود سیه رویی من</p></div>
<div class="m2"><p>دل سودازده آن روز که هندوی تو بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل کافر به تهیدستی رضوان می سوخت</p></div>
<div class="m2"><p>روزگاری که بهشتم گل خودروی تو بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بود بر خون گل آن روز شرف خاک مرا</p></div>
<div class="m2"><p>که دل خونشده ام نافه آهوی تو بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پرده ای بود به چشم من گستاخ نگاه</p></div>
<div class="m2"><p>هیکل شرم و حیایی که به بازوی تو بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خار در پیرهن شبنم گل بود از رشک</p></div>
<div class="m2"><p>تا مرا تکیه گه از خاک سر کوی تو بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عشرت روی زمین بود سراسر از من</p></div>
<div class="m2"><p>تا سرم در خم چوگان تو چون گوی تو بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا تو رفتی ز نظر، دیده من شد تاریک</p></div>
<div class="m2"><p>صیقل دیده من آینه روی تو بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دل یوسف هوس حلقه زنجیر تو داشت</p></div>
<div class="m2"><p>صائب آن روز که در سلسله موی تو بود</p></div></div>