---
title: >-
    غزل شمارهٔ ۴۶۴۵
---
# غزل شمارهٔ ۴۶۴۵

<div class="b" id="bn1"><div class="m1"><p>رخ گلرنگش از مژگان خونخوارست گیراتر</p></div>
<div class="m2"><p>گل بی خار این گلزارازخارست گیراتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمستی گر چه نتواند گرفتن چشم او خود را</p></div>
<div class="m2"><p>زخون ناحق آن روی چو گلنارست گیراتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نباشد دانه را هرچند همچون دام گیرایی</p></div>
<div class="m2"><p>ز زلف پرشکن آن خال طرارست گیراتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگرچه می نماید خویش رابیماردر ظاهر</p></div>
<div class="m2"><p>ز خواب صبحدم آن چشم عیارست گیراتر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خلاصی نیست هردل را که افتد درکمنداو</p></div>
<div class="m2"><p>زقلاب آن سرزلف سیه کارست گیراتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نباشد کبک را هرچند چون شهباز گیرایی</p></div>
<div class="m2"><p>زشاهین جلوه آن کبک رفتارست گیراتر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی صد می شود در پرده شب دزدراجرائت</p></div>
<div class="m2"><p>به دور خط مشکین ،خال طرارست گیراتر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به جرأت دررخ نورانی مه طلعتان منگر</p></div>
<div class="m2"><p>که این نورجهان افروز ازنارست گیراتر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به آسانی زجسم عنصری جان چون برون آید؟</p></div>
<div class="m2"><p>که از قید فرنگ این چار دیوارست گیراتر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به دام زلف حاجت نیست صیاد مراصائب</p></div>
<div class="m2"><p>زدام آن حلقه های چشم پرکارست گیراتر</p></div></div>