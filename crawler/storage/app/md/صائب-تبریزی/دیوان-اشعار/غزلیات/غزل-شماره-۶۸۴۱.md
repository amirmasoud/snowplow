---
title: >-
    غزل شمارهٔ ۶۸۴۱
---
# غزل شمارهٔ ۶۸۴۱

<div class="b" id="bn1"><div class="m1"><p>بی حجابانه به آغوش کجا می آیی؟</p></div>
<div class="m2"><p>که به صد ناز در اندیشه ما می آیی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر از سیر خود ای ماه لقا می آیی؟</p></div>
<div class="m2"><p>که عجب در نظر من به صفا می آیی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می چکد خون ز دم تیغ نگاهت امروز</p></div>
<div class="m2"><p>مگر از طوف مزار شهدا می آیی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کیست زان جلوه مستانه نگردد بیهوش؟</p></div>
<div class="m2"><p>که ز سر تا به قدم هوش ربا می آیی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می توان یافت ز رخساره گندم گونت</p></div>
<div class="m2"><p>کز بهشت ای صنم حورلقا می آیی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که به رخسار تو گستاخ نظر کرده، که باز</p></div>
<div class="m2"><p>شسته رو از عرق شرم و حیا می آیی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر خونریز که داری، که ز خلوتگه ناز</p></div>
<div class="m2"><p>مست و خنجر به کف و لعل قبا می آیی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون نثار قدمت خرده جان را نکنم؟</p></div>
<div class="m2"><p>که روان بخش تر از آب بقا می آیی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون کنند از تو نهان راز دل خود عشاق؟</p></div>
<div class="m2"><p>که به رخساره اندیشه نما می آیی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کرده ام هاله صفت قالب خود را خالی</p></div>
<div class="m2"><p>گر به آغوش من ای ماه لقا می آیی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در بساطم نگه بازپسینی مانده است</p></div>
<div class="m2"><p>گر به سر وقت من ای سست وفا می آیی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می کنی خون به دل باغ بهشت از تمکین</p></div>
<div class="m2"><p>تو به غمخانه عشاق کجا می آیی؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گشت خوشید جهانتاب ز مغرب طالع</p></div>
<div class="m2"><p>کی ز مشرق بدر ای ماه لقا می آیی؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>روی چون آینه پنهان مکن از سوختگان</p></div>
<div class="m2"><p>که ز خاکستر دلها به جلا می آیی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مشکبو گشت ز جولان غزال تو زمین</p></div>
<div class="m2"><p>می توان یافت که از ناف ختا می آیی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برخوری چون گل ازان چهره خندان یارب!</p></div>
<div class="m2"><p>که به آغوش من آغوش گشا می آیی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>باش چندان که دل رفته به جا باز آید</p></div>
<div class="m2"><p>گر به دلجویی این بی سر و پا می آیی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بی سبب خضر خط سبز دلیل تو شده است</p></div>
<div class="m2"><p>کی تو سرکش به ره از راهنما می آیی؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر بدانی که چه خون می خورم از دوری تو</p></div>
<div class="m2"><p>تا به غمخانه من پا به حنا می آیی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر بدانی چه قدر تشنه دیدار توایم</p></div>
<div class="m2"><p>عرق آلود به سر منزل ما می آیی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آنقدرها ننشینی که به گردت گردیم</p></div>
<div class="m2"><p>بعد عمری که به ویرانه ما می آیی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>رو به خلوتگه اغیار جلوریز روی</p></div>
<div class="m2"><p>به سر وعده ما رو به قفا می آیی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نیست چون فاصله درآمدن و رفتن تو</p></div>
<div class="m2"><p>ای جگر خون کن عشاق چرا می آیی؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای صبا بوی تو امروز جنون می آرد</p></div>
<div class="m2"><p>مگر از سلسله زلف دو تا می آیی؟</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نکشی پای ز ویرانه صائب هرگز</p></div>
<div class="m2"><p>گر بدانی که چه مقدار بجا می آیی</p></div></div>