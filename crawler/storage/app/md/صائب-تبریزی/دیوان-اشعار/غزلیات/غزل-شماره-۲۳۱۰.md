---
title: >-
    غزل شمارهٔ ۲۳۱۰
---
# غزل شمارهٔ ۲۳۱۰

<div class="b" id="bn1"><div class="m1"><p>شد زسر گردانی من بس که حیران گردباد</p></div>
<div class="m2"><p>کرد گردش را فرامش در بیابان گردباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون ندارد ریشه در صحرای امکان گردباد</p></div>
<div class="m2"><p>می برد آوارگی زود از بیابان گردباد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ریشه در خاک تعلق نیست اهل شوق را</p></div>
<div class="m2"><p>می رود بیرون ز دنیا پایکوبان گردباد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست با تن جان وحشت دیده را دلبستگی</p></div>
<div class="m2"><p>می فشاند گرد هستی از خود آسان گردباد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خار خار شوق دارد جنگ با آسودگی</p></div>
<div class="m2"><p>تا نفس دارد نیاساید زجولان گردباد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر نیاید تخم امید من مجنون ز خاک</p></div>
<div class="m2"><p>گرچه شد از گریه ام سرو خرامان گردباد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خار خار شوق در دل کار بال و پر کند</p></div>
<div class="m2"><p>طی به یک پا می کند چندین بیابان گردباد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تیره بختی می کند کوته زبان لاف را</p></div>
<div class="m2"><p>در دل شبها نمی باشد نمایان گردباد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دولت سر در هوایان را نمی باشد دوام</p></div>
<div class="m2"><p>می شود در جلوه ای از دیده پنهان گردباد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تنگنای شهر زندان است بر سر گشتگان</p></div>
<div class="m2"><p>راست می سازد نفس را در بیابان گردباد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از ره صحرانوردان تا توان برچید خار</p></div>
<div class="m2"><p>نیست ممکن پای خود پیچد به دامان گردباد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چشم خونبارم چنین در گریه گر طوفان کند</p></div>
<div class="m2"><p>می شود فواره خون در بیابان گردباد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>می کند زخم زبان شوریدگان را گرمتر</p></div>
<div class="m2"><p>خار و خس را بال و پر سازد زجولان گردباد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از جنون دوری من بس که دارد پیچ و تاب</p></div>
<div class="m2"><p>برنمی آرد سر لاف از گریبان گردباد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون به جولان گرم گردد شوق آتش پای من</p></div>
<div class="m2"><p>می شود انگشت زنهار بیابان گردباد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر زمد آه من در دل ندارد خارها</p></div>
<div class="m2"><p>از چه می باشد غبارآلود و پیچان گردباد؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>من به سر طی می کنم صائب ره باریک تیغ</p></div>
<div class="m2"><p>گربه یک پا می کند قطع بیابان گردباد</p></div></div>