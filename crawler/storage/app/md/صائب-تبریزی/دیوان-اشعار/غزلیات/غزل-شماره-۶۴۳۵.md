---
title: >-
    غزل شمارهٔ ۶۴۳۵
---
# غزل شمارهٔ ۶۴۳۵

<div class="b" id="bn1"><div class="m1"><p>دارد متاع یوسف در هر گذر صفاهان</p></div>
<div class="m2"><p>امروز خوش قماشی ختم است بر صفاهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دلبران نوخط هر روز می فزاید</p></div>
<div class="m2"><p>بر دستگاه خوبی حسن دگر صفاهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روشن شود نظرها از دیدن سوادش</p></div>
<div class="m2"><p>دارد چو سرمه حقی بر هر نظر صفاهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچون درخت طوبی از اعتدال موسم</p></div>
<div class="m2"><p>در چار فصل باشد صاحب ثمر صفاهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون چشم خوبرویان از گرد سرمه دارد</p></div>
<div class="m2"><p>تشریف خاکساری دایم به بر صفاهان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از گرد کلفت و غم شستند دست دلها</p></div>
<div class="m2"><p>روزی که بر میان بست از پل کمر صفاهان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در ظلمت سوادش آب حیات درج است</p></div>
<div class="m2"><p>در وقت شام دارد فیض سحر صفاهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر جلوه ظهورش تنگ است آسمان ها</p></div>
<div class="m2"><p>در هر صدف نگنجد همچون گهر صفاهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پهلو زد به همت خاکش ز سربلندی</p></div>
<div class="m2"><p>زان سوی آسمانها دارد خبر صفاهان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از صحت هوایش صائب نمی توان یافت</p></div>
<div class="m2"><p>جز چشم خوبرویان بیمار در صفاهان</p></div></div>