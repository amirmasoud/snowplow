---
title: >-
    غزل شمارهٔ ۳۸۶۵
---
# غزل شمارهٔ ۳۸۶۵

<div class="b" id="bn1"><div class="m1"><p>همیشه صاحب طول امل غمین باشد</p></div>
<div class="m2"><p>که چین به قدر بلندی در آستین باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر چه بر یدبیضا بود صباحت ختم</p></div>
<div class="m2"><p>نظر به ساعد او صبح اولین باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به روشنایی دل هرکه صفحه ای خوانده است</p></div>
<div class="m2"><p>چراغ در نظرش میل آتشین باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حضور می طلبی، تن به خاکساری ده</p></div>
<div class="m2"><p>که عیش روی زمین در ته زمین باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جان همیشه ز آسیب گاز می لرزد</p></div>
<div class="m2"><p>چو شمع، تیغ زبانی که آتشین باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به اختصارحلاوت توان ز منزل یافت</p></div>
<div class="m2"><p>که فرش خانه زنبور انگبین باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز گریه روشنی دیده می شودافزون</p></div>
<div class="m2"><p>چراغ خانه چشم اشک آتشین باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ازان به دیده دهم جای اشک لعلی را</p></div>
<div class="m2"><p>که چشم خشک نگین دان بی نگین باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به دوری از نظر من نمی توان شد دور</p></div>
<div class="m2"><p>که عینک دل عشاق دوربین باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شود ز طول امل تنگ دستگاه نشاط</p></div>
<div class="m2"><p>که چین به قدر بلندی در آستین باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز خرج بیش شود دخل باددستان را</p></div>
<div class="m2"><p>که سربلندی خرمن ز خوشه چین باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برای لقمه حریص از حیات می گذرد</p></div>
<div class="m2"><p>که مرگ مور مهیا در انگبین باشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یکی است مرگ وحیاتش به چشم زنده دلان</p></div>
<div class="m2"><p>دلی که زنده به گورازغبارکین باشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مگوی حرف نصیحت به غافلان صائب</p></div>
<div class="m2"><p>مزن تپانچه به رویی که آهنین باشد</p></div></div>