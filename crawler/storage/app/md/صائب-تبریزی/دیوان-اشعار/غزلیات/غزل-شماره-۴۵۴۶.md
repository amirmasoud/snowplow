---
title: >-
    غزل شمارهٔ ۴۵۴۶
---
# غزل شمارهٔ ۴۵۴۶

<div class="b" id="bn1"><div class="m1"><p>عشق تو ز دل بدر نمی آید</p></div>
<div class="m2"><p>سیمرغ ز قاف برنمی آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مأوای دل از دو کون بیرون است</p></div>
<div class="m2"><p>این بیضه به زیر پرنمی آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تر دامنی وصفای دل هیهات</p></div>
<div class="m2"><p>آیینه ز آب برنمی آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شبنم ز کنار آفتاب آمد</p></div>
<div class="m2"><p>از یوسف ما خبر نمی آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل پخته چو شد به خاک می افتد</p></div>
<div class="m2"><p>خودداری ازین ثمر نمی آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا پای ترا رکاب بوسیده است</p></div>
<div class="m2"><p>پایش به زمین دگر نمی آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>امید نگاه دارم از چشمش</p></div>
<div class="m2"><p>از بیخبران خبر نمی آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از کوچه نی کدام شب صائب</p></div>
<div class="m2"><p>صد قافله شکر نمی آید</p></div></div>