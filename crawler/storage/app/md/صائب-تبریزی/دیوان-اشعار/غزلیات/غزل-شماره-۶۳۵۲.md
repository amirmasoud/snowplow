---
title: >-
    غزل شمارهٔ ۶۳۵۲
---
# غزل شمارهٔ ۶۳۵۲

<div class="b" id="bn1"><div class="m1"><p>رحیم شد دل دشمن ز ناتوانی من</p></div>
<div class="m2"><p>حصار آهن من گشت شیشه جانی من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز خار سبز به رهرو نمی رسد آسیب</p></div>
<div class="m2"><p>ز کامرانی خصم است کامرانی من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیارمید چو موج سراب نیم نفس</p></div>
<div class="m2"><p>درین قلمرو وحشت سبک عنانی من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هیچ تشنه جگر روی تلخ ننمودم</p></div>
<div class="m2"><p>همیشه بود سبیل آب زندگانی من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به حسن عاقبت خود امیدها دارم</p></div>
<div class="m2"><p>که صرف پیر مغان گشت نوجوانی من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که را فتاد به رویم نظر ز سنگدلان؟</p></div>
<div class="m2"><p>که خونچکان نشد از چهره خزانی من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رسید بر لب بام زوال خورشیدم</p></div>
<div class="m2"><p>نکرده راست نفس صبح شادمانی من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا شکایتی از آستین فشانان نیست</p></div>
<div class="m2"><p>چو شمع سوخت مرا آتشین زبانی من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منم چو شبنم گل آبروی گلزارش</p></div>
<div class="m2"><p>نمی شود نکند حسن دیده بانی من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مخور چو غنچه مرا بر دل ای چمن پیرا</p></div>
<div class="m2"><p>که رنگ گل پرد از بال و پر فشانی من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل شکفته نماند درین جهان صائب</p></div>
<div class="m2"><p>اگر ز پرده برآید غم نهانی من</p></div></div>