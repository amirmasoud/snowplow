---
title: >-
    غزل شمارهٔ ۵۹۹۷
---
# غزل شمارهٔ ۵۹۹۷

<div class="b" id="bn1"><div class="m1"><p>چند بزم باده پنهان از حریفان ساختن؟</p></div>
<div class="m2"><p>خویش را آراستن، آیینه پنهان ساختن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پنجه خورشید را در آستین دزدیدن است</p></div>
<div class="m2"><p>عشق را در پرده ناموس پنهان ساختن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کار بر شیرازه زلف تو مشکل می شود</p></div>
<div class="m2"><p>ورنه آسان است دلها را پریشان ساختن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می تواند مور، اگر بخت سخن یاری کند</p></div>
<div class="m2"><p>پایتخت خویش از دست سلیمان ساختن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می چکد جای عرق خون از جبین آفتاب</p></div>
<div class="m2"><p>نیست آسان سنگ را لعل بدخشان ساختن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از جوانمردی است با یک قرص همچون آفتاب</p></div>
<div class="m2"><p>عالمی را بی نیاز از خوان احسان ساختن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون صدف پیش ترشرویان برای قطره ای</p></div>
<div class="m2"><p>دست خود را کاسه دریوزه نتوان ساختن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیش دانا از تمام علم ها بالاترست</p></div>
<div class="m2"><p>خویش را با دانش سرشار نادان ساختن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یا ز سیلاب حوادث رو نباید تافتن</p></div>
<div class="m2"><p>یا نباید خانه در صحرای امکان ساختن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر سر گفتار صائب را قدح می آورد</p></div>
<div class="m2"><p>کار آیینه است طوطی را سخندان ساختن</p></div></div>