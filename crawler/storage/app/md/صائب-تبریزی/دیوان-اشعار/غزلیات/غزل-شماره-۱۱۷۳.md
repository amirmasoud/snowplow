---
title: >-
    غزل شمارهٔ ۱۱۷۳
---
# غزل شمارهٔ ۱۱۷۳

<div class="b" id="bn1"><div class="m1"><p>هر نظر بازی که آن لبهای خندان دیده است</p></div>
<div class="m2"><p>برگ عیش عالمی در غنچه پنهان دیده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از گریبان لعل را چون اخگر اندازد برون</p></div>
<div class="m2"><p>تا لب لعل تراکان بدخشان دیده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون نسازد ناله گرمش جگرها را کباب؟</p></div>
<div class="m2"><p>بلبل ما بارها داغ گلستان دیده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زنگ ظلمت از دل تاریک ما نتوان زدود</p></div>
<div class="m2"><p>داغ چندین شمع روشن این شبستان دیده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عقل کوته بین ز بیم حشر می لرزد به خود</p></div>
<div class="m2"><p>عشق در بیداری این خواب پریشان دیده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سواد شهر اگر رم می کند عذرش بجاست</p></div>
<div class="m2"><p>گوشه چشمی که مجنون از غزالان دیده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون ز نسیان یاد کنعان را نیندازد به چاه؟</p></div>
<div class="m2"><p>این نوازشها که ماه مصر از اخوان دیده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آیه رحمت شمارد پیچ و تاب مار را</p></div>
<div class="m2"><p>هر که چین منع از ابروی دربان دیده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حال جان پاک را در قید تن داند که چیست</p></div>
<div class="m2"><p>هر که ماه مصر را در چاه و زندان دیده است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر که صائب آب زد بر آتش خشم و غضب</p></div>
<div class="m2"><p>چون خلیل الله در آتش گلستان دیده است</p></div></div>