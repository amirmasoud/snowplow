---
title: >-
    غزل شمارهٔ ۲۱۷۸
---
# غزل شمارهٔ ۲۱۷۸

<div class="b" id="bn1"><div class="m1"><p>در زیر فلک نیست اگر همنفسی هست</p></div>
<div class="m2"><p>در پرده غیب است اگر دادرسی هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیرون چه کشی دلو تهی از چه کنعان؟</p></div>
<div class="m2"><p>غافل مشو از یاد خدا تا نفسی هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زخمی است که الماس در او ریشه دوانده است</p></div>
<div class="m2"><p>تا در دل مجروح، هوا و هوسی هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زنهار چو صید حرم از کوی خرابات</p></div>
<div class="m2"><p>مگذار برون پای طلب تا عسسی هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر مرغ گرفتار، فضای قفس تنگ</p></div>
<div class="m2"><p>گلزار بهشت است اگر هم قفسی هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر سیل سبکسیر شود خار پر و بال</p></div>
<div class="m2"><p>سهل است اگر در ره ما خار و خسی هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در ترک تمنا بود آسودگی دل</p></div>
<div class="m2"><p>تلخ است شکر خواب به هر جا مگسی هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی جذبه محال است ز دل ناله برآید</p></div>
<div class="m2"><p>فریاد، دلیل است که فریادرسی هست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون مهلت اوراق خزان دیده دو روزی است</p></div>
<div class="m2"><p>در قافله عمر اگر پیش و پسی هست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این است که گاهی به دعا یاد نمایند</p></div>
<div class="m2"><p>از مستمعان صائب اگر ملتمسی هست</p></div></div>