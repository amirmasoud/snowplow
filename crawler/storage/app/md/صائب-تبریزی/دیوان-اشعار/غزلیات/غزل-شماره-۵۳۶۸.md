---
title: >-
    غزل شمارهٔ ۵۳۶۸
---
# غزل شمارهٔ ۵۳۶۸

<div class="b" id="bn1"><div class="m1"><p>شست نقش انجم از افلاک مژگان ترم</p></div>
<div class="m2"><p>ابر شد مستغنی از دریا ز آب گوهرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آتشین جانی ندارد همچو من این خاکدان</p></div>
<div class="m2"><p>پیچ وتاب برق دارد استخوان در پیکرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلومن در ساعت سنگین به چاه افتاده است</p></div>
<div class="m2"><p>شور محشر از گریبان بر نمی آرد سرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از رخ چون آفتاب اوست روز من سیاه</p></div>
<div class="m2"><p>در لباس زنگ از تردستی روشنگرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوختن برآتش من آب نتواند زدن</p></div>
<div class="m2"><p>می توان رنگ قیامت ریخت از خاکسترم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شمع بر بالین من انگشت زنهاری شود</p></div>
<div class="m2"><p>برگ گل چون لاله گردد داغدار بسترم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوری او بس که بیرحمانه می سوزد مرا</p></div>
<div class="m2"><p>شمع بالین می شود گر دشمن آید برسرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من که بودم از سبک مغزان دریا چون حباب</p></div>
<div class="m2"><p>از گرانی غوطه زد در کاسه زانو سرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرچه می دارم به سیلی سرخ روی خویش را</p></div>
<div class="m2"><p>می شود چون لاله خون مرده می در ساغرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می گذارد همچو مجنون شیر پیشم پشت دست</p></div>
<div class="m2"><p>صیدگاه عشق را هر چند صید لاغرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کرد چنبر دست بیداد فلک را صبر من</p></div>
<div class="m2"><p>پای خواب آلود شد موج خطر از لنگرم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شبنم محجوب از گلچین بود گستاختر</p></div>
<div class="m2"><p>در گلستانی که من چون حلقه بیرون درم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این گل روی عرقناکی که من دیدم ازو</p></div>
<div class="m2"><p>نیست ممکن در نظر آید بهشت و کوثرم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مانع پرواز من صائب نمی گردد قفس</p></div>
<div class="m2"><p>می جهد چون سنگ و آهن آتش از بال و پرم</p></div></div>