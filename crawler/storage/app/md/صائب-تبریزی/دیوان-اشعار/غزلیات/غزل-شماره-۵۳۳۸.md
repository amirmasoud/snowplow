---
title: >-
    غزل شمارهٔ ۵۳۳۸
---
# غزل شمارهٔ ۵۳۳۸

<div class="b" id="bn1"><div class="m1"><p>در غریبی گشت صرف نامجویی چون عقیق</p></div>
<div class="m2"><p>مشت خونی کز جگر گاه یمن برداشتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با زبردستی سپهراهنین پی برنداشت</p></div>
<div class="m2"><p>از امانت بار سنگین که من برداشتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکوه از راه درشت عشق کافر نعمتی است</p></div>
<div class="m2"><p>من ز خارش فیض بوی پیرهن برداشتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرغ بی بال و پر شب آشیان گم کرده ام</p></div>
<div class="m2"><p>تا دل خود را ز زلف پرشکن برداشتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر دو عالم چون صف مژگان فتاد از چشم من</p></div>
<div class="m2"><p>خار راهت تا به چشم خویشتن برداشتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز ندامت حاصل دیگر سوال من نداشت</p></div>
<div class="m2"><p>رزق دندان گشت دستی کز دهن برداشتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشت خون کشته من قابل دعوی نبود</p></div>
<div class="m2"><p>دست از دامان آن سیمین بدن برداشتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حاصل صورت پرستی غوطه در خون خوردن است</p></div>
<div class="m2"><p>عبرت از انجام کار کوهکن برداشتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون صراحی نشاه می می دهد گفتار من</p></div>
<div class="m2"><p>بزم شد پرشور تا مهر از دهن برداشتم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صائب از نظاره فردوس گشتم بی نیاز</p></div>
<div class="m2"><p>پرده تا از روی گلرنگ سخن برداشتم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا لب لعل ترا مهر از دهن بر داشتم</p></div>
<div class="m2"><p>فیض داغ تازه از داغ کهن برداشتم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>روی تلخ بحر بر من راه خواهش بسته داشت</p></div>
<div class="m2"><p>پیش نیسان چون صدف دست از دهن برداشتم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>منفعل از آزادگانم گرچه غیر از دل نبود</p></div>
<div class="m2"><p>توشه ای کز عالم ایجاد من برداشتم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از چمن پیرا چه افتاده است ناز گل کشم</p></div>
<div class="m2"><p>من که از کنج قفس فیض چمن برداشتم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گرچه می دانم نخواهد آمد از بیطاقتی</p></div>
<div class="m2"><p>وعده آن یار جانی را به تن برداشتم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از ندامت می زنم در روزگار خط به سر</p></div>
<div class="m2"><p>دست گستاخی کز آن سیب ذقن برداشتم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فکر داغ تازه ای می گشت گرد دل مرا</p></div>
<div class="m2"><p>پنبه ای گاهی گر از داغ کهن برداشتم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون نظر بردارم از زلفی که ازهر حلقه اش</p></div>
<div class="m2"><p>بهره ناف غزالان ختن برداشتم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر سپند من ز اهل بزم کس را دل نسوخت</p></div>
<div class="m2"><p>گرچه من آفات چشم از انجمن برداشتم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یوسفستان گشت زندان غریبی در نظر</p></div>
<div class="m2"><p>تا ز دل یاد زمین گیر وطن برداشتم</p></div></div>