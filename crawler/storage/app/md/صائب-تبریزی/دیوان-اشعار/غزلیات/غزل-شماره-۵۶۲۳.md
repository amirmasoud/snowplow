---
title: >-
    غزل شمارهٔ ۵۶۲۳
---
# غزل شمارهٔ ۵۶۲۳

<div class="b" id="bn1"><div class="m1"><p>چند امید به خوی تو ستمگر بندم؟</p></div>
<div class="m2"><p>نخل مومین به هواداری اخگر بندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لب ز اظهار محبت نتوانستم بست</p></div>
<div class="m2"><p>من که با موم دو صد روزن مجمر بندم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچنان سبزه من گرد یتیمی دارد</p></div>
<div class="m2"><p>جگر تشنه اگر بر لب کوثر بندم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زخم من چون گل صد برگ ز ناخن شده است</p></div>
<div class="m2"><p>ننگ صد بوسه چرا بر لب خنجر بندم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز فرهادی من چند بود پرده نشین؟</p></div>
<div class="m2"><p>تیغ کوهی به کف آرم کمری بر بندم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیگر از هیچ رخی نشأه می گل نکند</p></div>
<div class="m2"><p>شوری بخت اگر بر لب ساغر بندم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم بر ابر ندارد صدف قانع من</p></div>
<div class="m2"><p>آب شور از مژه افشانم و گوهر بندم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مهر کردم روش نامه فرستادن را</p></div>
<div class="m2"><p>دوزخی را ز چه بر بال کبوتر بندم؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب از خنده او تا نظری یافته ام</p></div>
<div class="m2"><p>تهمت تلخی گفتار به شکر بندم</p></div></div>