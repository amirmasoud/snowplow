---
title: >-
    غزل شمارهٔ ۵۲۹۶
---
# غزل شمارهٔ ۵۲۹۶

<div class="b" id="bn1"><div class="m1"><p>تا شده است از دوربینی عاقبت‌بین دیده‌ام</p></div>
<div class="m2"><p>در ترازوی قیامت خویش را سنجیده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منت دست نوازش می‌کشم از دست رد</p></div>
<div class="m2"><p>از قبول خلق از بس بی‌تمیزی دیده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی نظر بندم به صحرا می‌کند چشم غزال</p></div>
<div class="m2"><p>این نوازش‌ها که من از سنگ طفلان دیده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌کند در پیش پا دیدن نگاهم کوتهی</p></div>
<div class="m2"><p>بس که از شرم غدار او نظر دزدیده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بوده ذوق پاره گردیدن گریبان‌گیر من</p></div>
<div class="m2"><p>جامه‌ای چون کعبه در سالی اگر پوشیده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر زمین ناید ز شادی پای من چون گردباد</p></div>
<div class="m2"><p>تا خس و خاشاک هستی را به هم پیچیده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زینهار از کامجویی دست خود کوتاه دار</p></div>
<div class="m2"><p>کز گل ناچیده من صد دامن گل چیده‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کرده‌ام از بهر کاهش خویش را گردآوری</p></div>
<div class="m2"><p>چون مه نو ز التفات مهر اگر بالیده‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مد احسان می‌شمارم پیچ و تاب مار را</p></div>
<div class="m2"><p>چین ابرویی که من از اهل دولت دیده‌ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آیهٔ رحمت شمارم سبزهٔ زنگار را</p></div>
<div class="m2"><p>از جهان نادیدنی از بس که صائب دیده‌ام</p></div></div>