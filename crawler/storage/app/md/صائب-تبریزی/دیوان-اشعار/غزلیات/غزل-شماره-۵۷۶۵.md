---
title: >-
    غزل شمارهٔ ۵۷۶۵
---
# غزل شمارهٔ ۵۷۶۵

<div class="b" id="bn1"><div class="m1"><p>نه می به جام و نه گل در کنار می‌خواهم</p></div>
<div class="m2"><p>تبسمی ز لب لعل یار می‌خواهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیم ز رفتن گل‌های بوستان غمگین</p></div>
<div class="m2"><p>زمان حسن ترا پایدار می‌خواهم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو گل برای تماشاییان دلتنگ است</p></div>
<div class="m2"><p>گشایشی اگر از نوبهار می‌خواهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به ساده‌لوحی من شیشه‌ای ندارد چرخ</p></div>
<div class="m2"><p>که رحم از دل سنگین یار می‌خواهم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>متاع هستی من هرچه هست باختنی است</p></div>
<div class="m2"><p>ز عشق دست و دلی در قمار می‌خواهم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نرفت یک قدم از پیش، کارم از ماندن</p></div>
<div class="m2"><p>هنوز مهلت ازین روزگار می‌خواهم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمی‌توان خمش از سینه‌های گرم گذشت</p></div>
<div class="m2"><p>چراغ داغی ازین لاله‌زار می‌خواهم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رسیده مشق جنونم چنان که نتوان گفت</p></div>
<div class="m2"><p>نوازشی ز نسیم بهار می‌خواهم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی است محرم و بیگانه پیش غیرت من</p></div>
<div class="m2"><p>ترا نهفته ز خود در کنار می‌خواهم!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ازان لب شکرین گر طمع کنم صائب</p></div>
<div class="m2"><p>هزار بوسه، یکی از هزار می‌خواهم</p></div></div>