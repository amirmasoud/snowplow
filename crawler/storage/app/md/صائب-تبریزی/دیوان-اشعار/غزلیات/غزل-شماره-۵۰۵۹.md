---
title: >-
    غزل شمارهٔ ۵۰۵۹
---
# غزل شمارهٔ ۵۰۵۹

<div class="b" id="bn1"><div class="m1"><p>هرچند خط باطلم از تار و پود خویش</p></div>
<div class="m2"><p>خجلت کشم چو موج سراب از نمود خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون ابر غوطه در عرق شرم می‌زنم</p></div>
<div class="m2"><p>بندم لب هزار صدف گر به جود خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تیغ دم دم شود چو برون آیی از غلاف</p></div>
<div class="m2"><p>هردم که صوف عشق کنی از وجود خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد اشک ابر گوهر شهوار در صدف</p></div>
<div class="m2"><p>از پاک‌گوهران مکن امساک جود خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ابر زکام، پرده مغز جهان شده است</p></div>
<div class="m2"><p>در آتش افکنیم چه بیهوده عود خویش؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از فیض بوی سوختگی خلق غافلند</p></div>
<div class="m2"><p>در سینه همچو لاله گره ساز دود خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون هرچه وقف گشت به زودی شود خراب</p></div>
<div class="m2"><p>کردیم وقف عشق تو ملک وجود خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاک سیه به کاسه چشم غزاله کرد</p></div>
<div class="m2"><p>ای سنگدل بناز به چشم کبود خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرچند تاجریم فرومایه نیستیم</p></div>
<div class="m2"><p>تا بر زیان خلق گزینیم سود خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من کیستم که سجده بر آن آستان کنم ؟</p></div>
<div class="m2"><p>در خاک می‌کنم ز خجالت سجود خویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جای ترحم است بر آتش نشسته را</p></div>
<div class="m2"><p>صائب چه انتقام کشم از حسود خویش ؟</p></div></div>