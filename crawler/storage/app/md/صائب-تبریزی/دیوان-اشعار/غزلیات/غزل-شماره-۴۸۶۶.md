---
title: >-
    غزل شمارهٔ ۴۸۶۶
---
# غزل شمارهٔ ۴۸۶۶

<div class="b" id="bn1"><div class="m1"><p>درگلستان بلبل و در انجمن پروانه باش</p></div>
<div class="m2"><p>هرکجا دام تماشایی که بینی دانه باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کفر و دین را پرده دار جلوه معشوق دان</p></div>
<div class="m2"><p>گاه دربیت الحرام و گاه دربتخانه باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نور حسن لاابالی تا کجا سر برزند</p></div>
<div class="m2"><p>بلبل هر بوستان و جغد هر ویرانه باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جلوه مردان راه از خویش بیرون رفتن است</p></div>
<div class="m2"><p>جوهر مردی نداری، چون زنان در خانه باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دامن هرگل مگیر و گرد هر شمعی مگرد</p></div>
<div class="m2"><p>طالب حسن غریب و معنی بیگانه باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خضر راه رستگاری دل به دست آوردن است</p></div>
<div class="m2"><p>در مذاق کودکان شیرینی افسانه باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دست تا از توست، دست از دامن ریزش مدار</p></div>
<div class="m2"><p>تا نمی در شیشه داری تشنه پیمانه باش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا شوی چشم و چراغ این جهان چون آفتاب</p></div>
<div class="m2"><p>پوشش هر تنگدست و فرش هر ویرانه باش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی محبت مگذران عمر عزیز خویش را</p></div>
<div class="m2"><p>در بهاران عندلیب و در خزان پروانه باش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سنگ طفلان می دهد کیفیت رطل گران</p></div>
<div class="m2"><p>نشأه سرشار می خواهی برو دیوانه باش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صحبت شبهای میخواران ندارد بازگو</p></div>
<div class="m2"><p>چون ز مجلس می روی بیرون لب پیمانه باش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ما زبان شکوه را در سرمه خوابانیده ایم</p></div>
<div class="m2"><p>ای سپهر بی مروت، درجفا مردانه باش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا مگر صائب چراغ کشته ات روشن شود</p></div>
<div class="m2"><p>هر دل گرمی که یابی گرد او پروانه باش</p></div></div>