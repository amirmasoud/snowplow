---
title: >-
    غزل شمارهٔ ۲۲۴۰
---
# غزل شمارهٔ ۲۲۴۰

<div class="b" id="bn1"><div class="m1"><p>به چشم من فلک یک چشمخانه است</p></div>
<div class="m2"><p>که انسان مردمک، نور آن یگانه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نباشد چون سبکرو توسن عمر؟</p></div>
<div class="m2"><p>که هر موج نفس چون تازیانه است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود در زیر لب، جان عاشقان را</p></div>
<div class="m2"><p>که جای رفتنی بر آستانه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گناهان را ز خردی سهل مشمار</p></div>
<div class="m2"><p>که خرمنهای عالم دانه دانه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان غفلت ترا مدهوش کرده است</p></div>
<div class="m2"><p>که خواب مرگ در گوشت فسانه است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به غیر از آه، مکتوبی ندارم</p></div>
<div class="m2"><p>چو آتش ترجمان من زبانه است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مکن بر عشق، آه بوالهوس حمل</p></div>
<div class="m2"><p>که چون تیر هوایی بی نشانه است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ازان خورشید شد صائب جهانگیر</p></div>
<div class="m2"><p>که از رخسار زرینش خزانه است</p></div></div>