---
title: >-
    غزل شمارهٔ ۳۸۱۳
---
# غزل شمارهٔ ۳۸۱۳

<div class="b" id="bn1"><div class="m1"><p>به روی خوب تو هرکس ز خواب برخیزد</p></div>
<div class="m2"><p>اگر ستاره بود آفتاب برخیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین که چشم ترا خواب ناز سنگین است</p></div>
<div class="m2"><p>عجب که صبح قیامت ز خواب برخیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همان قدر مرو ای مست ناز از سر من</p></div>
<div class="m2"><p>که بوی سوختگی زین کباب برخیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غبار هستی من تا به جاست ممکن نیست</p></div>
<div class="m2"><p>که از میان من و او حجاب برخیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز فیض عشق به رخسار گریه پرور من</p></div>
<div class="m2"><p>اگر غبار نشیند سحاب برخیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبرد روشنی می سیاهی از دل ما</p></div>
<div class="m2"><p>مگر ز عارض ساقی نقاب برخیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنین که اختر اهل سخن زمین گیرست</p></div>
<div class="m2"><p>عجب که گرد ز روی کتاب برخیزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر به تربت مخمور، تاک دست نهد</p></div>
<div class="m2"><p>ز خواب مرگ به بوی شراب برخیزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فغان که قافله نوبهار کم فرصت</p></div>
<div class="m2"><p>امان نداد که نرگس ز خواب برخیزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غبار هستی من آنقدر گران خیزست</p></div>
<div class="m2"><p>که از عذار تو طرف نقاب برخیزد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ازان خطی که روی تو خاست، نزدیک است</p></div>
<div class="m2"><p>که آه از جگر آفتاب برخیزد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نخاست گوهر شادابی از جهان صائب</p></div>
<div class="m2"><p>چگونه ابر ز بحر سراب برخیزد؟</p></div></div>