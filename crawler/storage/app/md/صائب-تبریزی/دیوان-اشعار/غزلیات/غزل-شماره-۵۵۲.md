---
title: >-
    غزل شمارهٔ ۵۵۲
---
# غزل شمارهٔ ۵۵۲

<div class="b" id="bn1"><div class="m1"><p>صلح در پرده بود یار به جنگ آمده را</p></div>
<div class="m2"><p>مده از دست، گریبان به چنگ آمده را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آشنایی ز نگاهش چه توقع دارید؟</p></div>
<div class="m2"><p>نور اسلام نباشد ز فرنگ آمده را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماجرای دل و آن غمزه بدمست مپرس</p></div>
<div class="m2"><p>می چکد خون ز سخن، شیشه به سنگ آمده را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست جز بی خودی و بی خبری درمانی</p></div>
<div class="m2"><p>از شب و روز و مه و سال به تنگ آمده را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کوشش افکنده ترا دور ز منزل، ورنه</p></div>
<div class="m2"><p>راه نزدیک بود پای به سنگ آمده را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در سفر ریگ روان راحت منزل دارد</p></div>
<div class="m2"><p>ماندگی کم بود از راه درنگ آمده را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می کند کلفت یک خانه به شهری تأثیر</p></div>
<div class="m2"><p>شهر زندان بود از خانه به تنگ آمده را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با قد همچو کمان، تنگ به بر چون گیرم؟</p></div>
<div class="m2"><p>صائب آن قامت چون تیر خدنگ آمده را</p></div></div>