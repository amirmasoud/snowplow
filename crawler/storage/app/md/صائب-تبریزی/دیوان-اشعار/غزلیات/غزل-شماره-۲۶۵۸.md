---
title: >-
    غزل شمارهٔ ۲۶۵۸
---
# غزل شمارهٔ ۲۶۵۸

<div class="b" id="bn1"><div class="m1"><p>دل ز قید جسم چون آزاد گردد وا شود</p></div>
<div class="m2"><p>چون حباب از خود کند قالب تهی دریا شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قفل دل را نیست مفتاحی بغیر از دست سعی</p></div>
<div class="m2"><p>سنگ زن بر سینه تا این در به رویت وا شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گربه سنگ و آهن از چشم بدان گیرم پناه</p></div>
<div class="m2"><p>چشم بد از سنگ و آهن چون شرر پیدا شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می توان روز سیاه از خصم داد خود گرفت</p></div>
<div class="m2"><p>صبر آن دارم که خط گرد رخش پیدا شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در مقام حیرت دیدار، حرف وصوت نیست</p></div>
<div class="m2"><p>طوطی ازآیینه حیرانم که چون گویا شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون نیفتد دل به حال مرگ بی شور جنون؟</p></div>
<div class="m2"><p>خلوت گورست خم چون خالی از صهبا شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شور عشق است این که بی سر کرد صد منصور را</p></div>
<div class="m2"><p>از سر ما کی به دستار پریشان وا شود؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر طلسمی را به نام باددستی بسته اند</p></div>
<div class="m2"><p>غنچه دل از نسیم صبح محشر وا شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آبرو صائب به گوهر دادن از دون همتی است</p></div>
<div class="m2"><p>وقت ابری خوش که دست خالی از دریا شود</p></div></div>