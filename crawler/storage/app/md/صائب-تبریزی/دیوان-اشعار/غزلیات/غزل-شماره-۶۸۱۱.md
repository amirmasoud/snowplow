---
title: >-
    غزل شمارهٔ ۶۸۱۱
---
# غزل شمارهٔ ۶۸۱۱

<div class="b" id="bn1"><div class="m1"><p>رخصت بوسه اگر از لب جامی داری</p></div>
<div class="m2"><p>تلخ منشین که عجب عیش مدامی داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرفرازان جهان جمله سجود تو کنند</p></div>
<div class="m2"><p>در حریم دل اگر راه سلامی داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر از داغ جنون یافته ای مهر قبول</p></div>
<div class="m2"><p>چشم بد دور که خوش ماه تمامی داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوشه ای گر به کف آورده ای از ملک رضا</p></div>
<div class="m2"><p>باش آسوده که شایسته مقامی داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای عقیق از من لب تشنه فراموش مکن</p></div>
<div class="m2"><p>که درین دایره امروز تو نامی داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرو از دایره حکم تو بیرون نرود</p></div>
<div class="m2"><p>تا تو چون فاختگان حلقه دامی داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسته ای در گره از ساده دلی دوزخ را</p></div>
<div class="m2"><p>در سر خود اگر اندیشه خامی داری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون گره شد به گلو لقمه غم باده طلب</p></div>
<div class="m2"><p>به حلالی خور اگر آب حرامی داری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای صبا چشم من از آمدنت روشن شد</p></div>
<div class="m2"><p>مگر از یوسف گم کرده پیامی داری؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برخوری زان لب میگون که چو صهبای صبوح</p></div>
<div class="m2"><p>در رگ و ریشه جان طرفه خرامی داری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب این آن غزل حافظ مشکین نفس است</p></div>
<div class="m2"><p>بشنو ای خواجه اگر زان که مشامی داری</p></div></div>