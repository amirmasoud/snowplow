---
title: >-
    غزل شمارهٔ ۳۶۷۳
---
# غزل شمارهٔ ۳۶۷۳

<div class="b" id="bn1"><div class="m1"><p>اگر نقاب ازان روی دلپسند افتد</p></div>
<div class="m2"><p>به شهر سوختگان قحطی سپند افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هوای قامت او فکر را بلندی داد</p></div>
<div class="m2"><p>سخن بلند شود عشق چون بلند افتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به چشم کم نظران میل آتشین مکشید</p></div>
<div class="m2"><p>که آتشی به هواداری سپند افتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به صد چراغ گل و لاله ره برون نبرد</p></div>
<div class="m2"><p>اگر نسیم در آن طره بلند افتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مناز پر به سخنهای آتشین صائب</p></div>
<div class="m2"><p>زبان شمع درین انجمن بلند افتد</p></div></div>