---
title: >-
    غزل شمارهٔ ۴۶۶۸
---
# غزل شمارهٔ ۴۶۶۸

<div class="b" id="bn1"><div class="m1"><p>ای رخت شسته تر از دامن مهتاب بهار</p></div>
<div class="m2"><p>چشم مخمور تو گیرنده تر از خواب بهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ابر خشکی است که در شوره زمین می گردد</p></div>
<div class="m2"><p>باگل روی تو شادابی مهتاب بهار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مستی چشم تورا رطل گران حاجت نیست</p></div>
<div class="m2"><p>بی نیازست ز افسانه شکر خواب بهار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برق خاروخس تقوی است شکرخنده گل</p></div>
<div class="m2"><p>سیل ناموس بود چهره شاداب بهار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لازم عهد جوانی است سیه کاریها</p></div>
<div class="m2"><p>روشن است این سخن از تیرگی آب بهار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش ازان دم که خزان زرد کند رخسارش</p></div>
<div class="m2"><p>آب ده چشم ز خورشید جهانتاب بهار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقل پیری ز من ایام جوانی مطلب</p></div>
<div class="m2"><p>که در ایام خزان صاف شود آب بهار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جگر سوخته لاله خبر می بخشد</p></div>
<div class="m2"><p>صائب ازشعله دیدار جگر تاب بهار</p></div></div>