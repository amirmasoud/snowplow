---
title: >-
    غزل شمارهٔ ۳۸۰۵
---
# غزل شمارهٔ ۳۸۰۵

<div class="b" id="bn1"><div class="m1"><p>کسی که خرده خود صرف باده می سازد</p></div>
<div class="m2"><p>ز زنگ آینه خویش ساده می سازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حضور روی زمین فرش آستان کسی است</p></div>
<div class="m2"><p>که لوح خویش چو آیینه ساده می سازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عنان به دست قضا ده که موج را دریا</p></div>
<div class="m2"><p>به یک تپانچه کف بی اراده می سازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز چوب منع چه پرواست خیره چشمان را؟</p></div>
<div class="m2"><p>که برق ره به نیستان گشاده می سازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکوه حسن تو خورشید را ز توسن چرخ</p></div>
<div class="m2"><p>به یک اشاره ابرو پیاده می سازد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل پری است مرا از جهان که سایه من</p></div>
<div class="m2"><p>اگر به سیل فتد ایستاده می سازد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به آه گرم تواند کسی که زور آورد</p></div>
<div class="m2"><p>کمان سخت فلک را کباده می سازد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به برق و باد نیاید ز شوق همراهی</p></div>
<div class="m2"><p>کجا سوار به پای پیاده می سازد؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به قسمت ازلی هرکه خواهد افزاید</p></div>
<div class="m2"><p>به کاوش آب گهر را زیاده می سازد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عنان نفس به دست هوا مده کاین سگ</p></div>
<div class="m2"><p>نگشته هرزه مرس با قلاده می سازد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل گرفته ما را ز همرهان صائب</p></div>
<div class="m2"><p>که غیر ناله و افغان گشاده می سازد؟</p></div></div>