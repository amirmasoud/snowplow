---
title: >-
    غزل شمارهٔ ۵۰۲
---
# غزل شمارهٔ ۵۰۲

<div class="b" id="bn1"><div class="m1"><p>حسن کی در دل چون سنگ نماید خود را؟</p></div>
<div class="m2"><p>باده در شیشه بیرنگ نماید خود را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق ازان شوختر افتاده که پنهان گردد</p></div>
<div class="m2"><p>این شرر در جگر سنگ نماید خود را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در شب تار کند جلوه دیگر آتش</p></div>
<div class="m2"><p>چهره زیر خط شبرنگ نماید خود را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تکیه بر دوستی چرخ مکن کاین مکار</p></div>
<div class="m2"><p>به تو یکرنگ ز نیرنگ نماید خود را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زود باشد که زند غوطه به خون چون طاوس</p></div>
<div class="m2"><p>خودنمایی که به صد رنگ نماید خود را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سنگ دندان پریشان سخنان است وقار</p></div>
<div class="m2"><p>وای بر آن که سبک سنگ نماید خود را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شود از بخت سیه، جوهر ذاتی ظاهر</p></div>
<div class="m2"><p>جوهر تیغ اگر از زنگ نماید خود را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عندلیبی که شد از نغمه شناسان نومید</p></div>
<div class="m2"><p>به چه امید به آهنگ نماید خود را؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خون کند در دل صیاد ز پرکاری ها</p></div>
<div class="m2"><p>آهوی وحشی اگر لنگ نماید خود را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صائب آن حسن به سامان که نگنجد به خیال</p></div>
<div class="m2"><p>چه قدر در نظر تنگ نماید خود را؟</p></div></div>