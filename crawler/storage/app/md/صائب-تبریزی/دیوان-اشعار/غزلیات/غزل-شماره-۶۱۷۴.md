---
title: >-
    غزل شمارهٔ ۶۱۷۴
---
# غزل شمارهٔ ۶۱۷۴

<div class="b" id="bn1"><div class="m1"><p>پیش هر ناشسته رویی وا مکن لب بیش ازین</p></div>
<div class="m2"><p>آبروی خود مبر در عرض مطلب بیش ازین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر شب خود را نمی سازی ز برق آه روز</p></div>
<div class="m2"><p>روز خود را از سیه کاری مکن شب بیش ازین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می شود زلف حواس از باد پیما تار و مار</p></div>
<div class="m2"><p>پوچ گویان را مزن انگشت بر لب بیش ازین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کاهلی خوابیده می سازد ره نزدیک را</p></div>
<div class="m2"><p>خواب آسایش مکن بر پشت مرکب بیش ازین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برنمی آید نفس از واصلان بحر عشق</p></div>
<div class="m2"><p>دعوی عرفان مکن ای نامؤدب بیش ازین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر چراغان تجلی آستین افشان مشو</p></div>
<div class="m2"><p>شکوه بیجا مکن از سیر کوکب بیش ازین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برگریز ناخن تدبیر شد، دل وا نشد</p></div>
<div class="m2"><p>این گره در کار ما مپسند یارب بیش ازین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کعبه و بتخانه یکسان است صائب پیش سیل</p></div>
<div class="m2"><p>حرف کفر و دین مگو با اهل مشرب بیش ازین</p></div></div>