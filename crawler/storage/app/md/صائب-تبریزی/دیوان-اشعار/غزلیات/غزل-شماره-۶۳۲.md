---
title: >-
    غزل شمارهٔ ۶۳۲
---
# غزل شمارهٔ ۶۳۲

<div class="b" id="bn1"><div class="m1"><p>احاطه کرد خط آن آفتاب تابان را</p></div>
<div class="m2"><p>گرفت خیل پری در میان سلیمان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکار هاله بود ماه و آن خط مشکین</p></div>
<div class="m2"><p>به دام هاله کشید آفتاب تابان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تن لطیف ترا عطر، خار پیرهن است</p></div>
<div class="m2"><p>به بوی گل مگشا چاک آن گریبان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشو ز حال دل ای یار تازه خط غافل</p></div>
<div class="m2"><p>که نیست جز دل ما شمعی این شبستان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به حکمت از لب خود مهر خامشی بردار</p></div>
<div class="m2"><p>به دست دیو مده خاتم سلیمان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز جان درین تن خاکی مجوی جوش نشاط</p></div>
<div class="m2"><p>که در تنور، نفس سوخته است طوفان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به ما حرارت دوزخ چه می تواند کرد؟</p></div>
<div class="m2"><p>اگر ز ما نستانند چشم گریان را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز حال راهروان غافلم، همین دانم</p></div>
<div class="m2"><p>که هست توشه ز دل خضر این بیابان را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز دود آه، لب تازه خط او صائب</p></div>
<div class="m2"><p>سیاه خانه نشین کرد آب حیوان را</p></div></div>