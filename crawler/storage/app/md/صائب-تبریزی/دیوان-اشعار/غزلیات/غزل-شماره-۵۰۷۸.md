---
title: >-
    غزل شمارهٔ ۵۰۷۸
---
# غزل شمارهٔ ۵۰۷۸

<div class="b" id="bn1"><div class="m1"><p>فارغ ز تمنای جهان گذران باش</p></div>
<div class="m2"><p>بی داعیه چون دیده حیرت زدگان باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از راه تواضع به فلک رفت مسیحا</p></div>
<div class="m2"><p>باذره تنزل کن و خورشید مکان باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان پیش که ایام بهاران بسر آید</p></div>
<div class="m2"><p>آماده پرواز چو اوراق خزان باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در حقه سربسته گذارند گهر را</p></div>
<div class="m2"><p>خاموش نشین، محرم اسرار نهان باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آیینه خورشید شود دیده بیدار</p></div>
<div class="m2"><p>چون شبنم گل تادم آخر نگران باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد مخزن گوهر صدف از پاک دهانی</p></div>
<div class="m2"><p>یک چند درین بحر تو هم پاک دهان باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سررشته میزان عدالت مده از دست</p></div>
<div class="m2"><p>زنهار که با هرکه گران است گران باش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جایی که به کردار بود قیمت مردم</p></div>
<div class="m2"><p>صائب که ترا گفت که چون تیغ،زبان باش ؟</p></div></div>