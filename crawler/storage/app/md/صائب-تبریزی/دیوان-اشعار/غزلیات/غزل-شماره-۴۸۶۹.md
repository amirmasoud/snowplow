---
title: >-
    غزل شمارهٔ ۴۸۶۹
---
# غزل شمارهٔ ۴۸۶۹

<div class="b" id="bn1"><div class="m1"><p>می کشی چون با حریفان باده لایعقل مباش</p></div>
<div class="m2"><p>از خداچون غافلی باری ز خود غافل مباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دعوی خون را همین جا بانگاهی صلح کن</p></div>
<div class="m2"><p>روز محشر درکمین دامن قاتل مباش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در میان شبنم ما و فروغ آفتاب</p></div>
<div class="m2"><p>ای سحاب بی مروت بیش ازین حایل مباش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سبحه تزویر را در گردن زاهد فکن</p></div>
<div class="m2"><p>روزو شب محشور با صد عقده مشکل مباش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می دهد خارو خس اندیشه را حیرت به آب</p></div>
<div class="m2"><p>محو حق شو، پیرو اندیشه باطل مباش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رو نمی بیند خدنگ مو شکاف انتقام</p></div>
<div class="m2"><p>گر شکست خود نخواهی در شکست دل مباش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوشه ای گیر از محیط بیکنار آرزو</p></div>
<div class="m2"><p>بیش ازین خاشاک این دریای بی ساحل مباش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>داغ محرومی منه برجبهه اهل سؤال</p></div>
<div class="m2"><p>نور استحقاق گو درجبهه سایل مباش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ریزش خود راچو ابر نوبهاران عام کن</p></div>
<div class="m2"><p>چون توداری قابلیت گو طرف قابل مباش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر چه از خرمن نصیب مور شد آن رزق توست</p></div>
<div class="m2"><p>گرنه از بی حاصلانی درغم حاصل مباش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طی عرض راه، طول راه را سازد زیاد</p></div>
<div class="m2"><p>در ره حق همچو مستان هر طرف مایل مباش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صائب اوراق هوس رادر گذار باد ریز</p></div>
<div class="m2"><p>بیش ازین شیرازه این دفتر باطل مباش</p></div></div>