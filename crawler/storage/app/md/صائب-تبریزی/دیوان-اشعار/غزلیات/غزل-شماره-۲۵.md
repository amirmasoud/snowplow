---
title: >-
    غزل شمارهٔ ۲۵
---
# غزل شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>نیست سنگ کم اگر در پله میزان ترا</p></div>
<div class="m2"><p>کعبه و بتخانه باشد در نظر یکسان ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا نبندی رخنه چشم و دهان و گوش را</p></div>
<div class="m2"><p>از درون دل نجوشد چشمه حیوان ترا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همرهان سست در راه طلب سنگ رهند</p></div>
<div class="m2"><p>دل مخور، افتاد در پیری اگر دندان ترا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر چه نگذارد کمان از خانه خود پا برون</p></div>
<div class="m2"><p>قامت خم ساخت در پیری سبک جولان ترا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوشمال آخر شود دست نوازش ساز را</p></div>
<div class="m2"><p>سر مکش گر گوشمالی می دهد دوران ترا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست بی جمعیت خاطر تلاوت را ثمر</p></div>
<div class="m2"><p>می شود سی پاره دل در خواندن قرآن ترا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از خجالت می شود هر دم به رنگی چهره ات</p></div>
<div class="m2"><p>بس کز الوان گنه، آلوده شد دامان ترا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صبح زد از خنده رویی غوطه در خون شفق</p></div>
<div class="m2"><p>تا چه گلها بشکفد از چهره خندان ترا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سوده شد از خوردن نان گر چه دندان های تو</p></div>
<div class="m2"><p>چشم کوته بین پرد باز از برای نان ترا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون به زیر خاک خواهی خفت، کز بس سرکشی</p></div>
<div class="m2"><p>می فشانی گر نشیند گرد بر دامان ترا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر نشویی صائب از اشک ندامت روی خویش</p></div>
<div class="m2"><p>جز سیه رویی نباشد حاصل از دیوان ترا</p></div></div>