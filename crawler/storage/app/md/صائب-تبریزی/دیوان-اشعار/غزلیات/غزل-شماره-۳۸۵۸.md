---
title: >-
    غزل شمارهٔ ۳۸۵۸
---
# غزل شمارهٔ ۳۸۵۸

<div class="b" id="bn1"><div class="m1"><p>فروغ گوهر چرخ از جلای دل باشد</p></div>
<div class="m2"><p>صفای روی زمین در صفای دل باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مه تمام ز پهلوی خود خورد روزی</p></div>
<div class="m2"><p>ز خوان خویش مهیا غذای دل باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به درد و داغ درین بوته گداز بساز</p></div>
<div class="m2"><p>که دل چو آب شد آب بقای دل باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز عقده های فلک کیست سربرون آرد</p></div>
<div class="m2"><p>اگر نه ناخن مشکل گشای دل باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صباح عید بود از ستاره سوختگان</p></div>
<div class="m2"><p>در آن مقام که نوروصفای دل باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به ساق عرش تواند رساند خوشه خویش</p></div>
<div class="m2"><p>ز اشک و آه اگر آب وهوای دل باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نفس چگونه کشد جان درین نشیمن پست</p></div>
<div class="m2"><p>اگر نه عالم بی منتهای دل باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چوداغ لاله در آغوش اوست کعبه مقیم</p></div>
<div class="m2"><p>کسی که در قدم رهنمای دل باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گداییی که به آن فخر می توان کردن</p></div>
<div class="m2"><p>گدایی در دولتسرای دل باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زکوه قاف پریزاد را به دام آرد</p></div>
<div class="m2"><p>به دست هر که کمند رسای دل باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سعادتی که ندارد شقاوت از دنبال</p></div>
<div class="m2"><p>به زیر سایه بال همای دل باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بود سپهر برین حلقه برون درش</p></div>
<div class="m2"><p>کسی که در حرم کبریای دل باشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فغان که مردم کوته نظر نمی دانند</p></div>
<div class="m2"><p>که نه سپهر به زیر لوای دل باشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مکن به قبله دل پشت خود که کعبه دل</p></div>
<div class="m2"><p>قفای آینه خوش جلای دل باشد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به بیخودی گذرد روزگار اهل بهشت</p></div>
<div class="m2"><p>بهشت اگر به صفای لقای دل باشد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کلید قفل اجابت درین بلند ایوان</p></div>
<div class="m2"><p>به دست ناله مشکل گشای دل باشد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز بیدلی نبود شکوه عشقبازان را</p></div>
<div class="m2"><p>چه دولتی است که دلبر به جای دل باشد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ازان ز انجمن عشق بوی جان آید</p></div>
<div class="m2"><p>که عود مجمرش از پاره های دل باشد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کمال مغز بود مطلب از رعایت پوست</p></div>
<div class="m2"><p>وجود هر دو جهان از برای دل باشد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به آشنایی دل صائب از جهان جان برد</p></div>
<div class="m2"><p>خوشا کسی که به جان آشنای دل باشد</p></div></div>