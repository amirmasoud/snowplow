---
title: >-
    غزل شمارهٔ ۳۱۹۵
---
# غزل شمارهٔ ۳۱۹۵

<div class="b" id="bn1"><div class="m1"><p>غم عالم به دل از دیده خونبار می‌آید</p></div>
<div class="m2"><p>به این گلشن خزان از رخنه دیوار می‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تسلی در دل آزرده عاشق نمی‌باشد</p></div>
<div class="m2"><p>ازین ویرانه دایم ناله بیمار می‌آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به سختی‌های دوران صبر کن ای تشنه راحت</p></div>
<div class="m2"><p>که آب گریه شادی ازین کهسار می‌آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فشاند آستین بی‌نیازی چون غنای حق</p></div>
<div class="m2"><p>چه از گفتار می‌خیزد؟ چه از کردار می‌آید؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس از مردن به من شد مهربان جانان، ندانستم</p></div>
<div class="m2"><p>ز خواب مرگ کار دولت بیدار می‌آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چراغ گل ز بی‌تابی به شمع صبح می‌ماند</p></div>
<div class="m2"><p>کدامین سنگدل یارب به این گلزار می‌آید؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آن وادی که قطع ره به همت می‌توان کردن</p></div>
<div class="m2"><p>ز پای خفته کار تیغ لنگردار می‌آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز حبس پیله، کرم پیله هم آزاد می‌گردد</p></div>
<div class="m2"><p>اگر زاهد برون از پرده پندار می‌آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر در دل نباشد غصه دوران گره صائب</p></div>
<div class="m2"><p>سخن یکدست می‌خیزد، نفس هموار می‌آید</p></div></div>