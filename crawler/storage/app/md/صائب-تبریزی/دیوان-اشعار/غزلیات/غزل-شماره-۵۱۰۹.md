---
title: >-
    غزل شمارهٔ ۵۱۰۹
---
# غزل شمارهٔ ۵۱۰۹

<div class="b" id="bn1"><div class="m1"><p>ز اضطراب دل کند آن زلف عنبرفام رقص</p></div>
<div class="m2"><p>می‌کند آری به بال مرغ وحشی دام رقص</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرتو خورشید را آیینه در وجد آورد</p></div>
<div class="m2"><p>در دل روشن کند آن یار سیم‌اندام رقص</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش عاقل در بلا بودن به از بیم بلاست</p></div>
<div class="m2"><p>مرغ زیرک می‌کند در حلقه‌های دام رقص</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شوق در هر دل که باشد مطربی در کار نیست</p></div>
<div class="m2"><p>بی دف و نی می‌کند گردون مینافام رقص</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در محیط عشق بی‌تابی بود باد مراد</p></div>
<div class="m2"><p>برد کف را بر کران زین بحر خون‌آشام رقص</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ذره را نظاره خورشید در رقص آورد</p></div>
<div class="m2"><p>آتشین‌رویی چو باشد نیست بی‌هنگام رقص</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا رگ خامی بود در باده ننشیند ز جوش</p></div>
<div class="m2"><p>می‌کنند از نارسایی صوفیان خام رقص</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اوج دولت جای بازی و نشاط و لهو نیست</p></div>
<div class="m2"><p>از بصیرت نیست کردن بر کنار بام رقص</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرکجا آن مطرب خورشید و طالع شود</p></div>
<div class="m2"><p>خرده جان را کند چون ذره بی‌آرام رقص</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شمع می‌سازد قبا پیراهن فانوس را</p></div>
<div class="m2"><p>چون کند در انجمن آن یار سیم‌اندام رقص</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طعمه دریا نگردد هرکه از خود شد تهی</p></div>
<div class="m2"><p>تا بود خالی، بر روی صهبا جام رقص</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فتنه‌سازان جهان را نیست در فرمان زبان</p></div>
<div class="m2"><p>می‌کند بی‌خواست آتش را زبان در کام رقص</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از سیه‌مستان نمی‌آید تمیز درد و صاف</p></div>
<div class="m2"><p>می‌کنم یکسان به ذوق بوسه و دشنام رقص</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پایکوبان می‌رود سیلاب صائب تا محیط</p></div>
<div class="m2"><p>هرکه را شوق است در سر می‌کند هر گام رقص</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اختیاری نیست صائب بی‌قراری‌های ما</p></div>
<div class="m2"><p>ذره چون خورشید بیند می‌کند ناکام رقص</p></div></div>