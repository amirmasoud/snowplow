---
title: >-
    غزل شمارهٔ ۵۹۱۶
---
# غزل شمارهٔ ۵۹۱۶

<div class="b" id="bn1"><div class="m1"><p>ما تخم درین مزرعه جز اشک نکشتیم</p></div>
<div class="m2"><p>یک رشته درین غمکده جز آه نرشتیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون آبله در زیر قدم راهروان را</p></div>
<div class="m2"><p>بردیم بسر عمری و هموار نگشتیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با گرمروی چون جرس از ناله شبگیر</p></div>
<div class="m2"><p>یک خفته درین بادیه بر جای نهشتیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دانه ناگشته چه امید توان داشت؟</p></div>
<div class="m2"><p>افسوس بود حاصل تخمی که نکشتیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقصان نکند هیچ کس از جود و سخاوت</p></div>
<div class="m2"><p>در خوشه رسیدیم گر از دانه گذشتیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از بوته به سیم و زر خالص نرسد نقص</p></div>
<div class="m2"><p>با ما چه کند دوزخ اگر پاک سرشتیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر چند ز بی بال و پری خانه نشینیم</p></div>
<div class="m2"><p>چون آهوی وحشت زده در دامن دشتیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در مشق جنون گر چه سر آمد همه عمر</p></div>
<div class="m2"><p>سطری که توان داد به دستی ننوشتیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این آن غزل سعدی شیراز که فرمود</p></div>
<div class="m2"><p>خرما نتوان خورد ازین خار که کشتیم</p></div></div>