---
title: >-
    غزل شمارهٔ ۵۶۶۳
---
# غزل شمارهٔ ۵۶۶۳

<div class="b" id="bn1"><div class="m1"><p>ما به ناسازی ابنای زمان ساخته ایم</p></div>
<div class="m2"><p>وسعت حوصله را مهر دهان ساخته ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وقت ما را نکند موج حوادث ناصاف</p></div>
<div class="m2"><p>ما به این سلسله چون آب روان ساخته ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه سر گفتن و نه ذوق شنیدن داریم</p></div>
<div class="m2"><p>با لب خامش و با گوش گران ساخته ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقش امید جهان روی به ما آورده است</p></div>
<div class="m2"><p>تا چون آیینه به چشم نگران ساخته ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون مه عید عزیزم به چشم همه کس</p></div>
<div class="m2"><p>تا ز الوان نعم با لب نان ساخته ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با گل روی تو در پرده نظر می بازیم</p></div>
<div class="m2"><p>ما از آن آینه با آینه دان ساخته ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از مروت نبود روی ز ما پوشیدن</p></div>
<div class="m2"><p>ما که با داغی از آن لاله ستان ساخته ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هوس بوسه زیاد از دهن ساغر ماست</p></div>
<div class="m2"><p>ما به پیغامی از آن غنچه دهان ساخته ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از هم آغوشی آن قامت چون تیر خدنگ</p></div>
<div class="m2"><p>ما به خمیازه خشکی چو کمان ساخته ایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>داغ سودای ترا در دل سی پاره خود</p></div>
<div class="m2"><p>چون شب قدر نهان در رمضان ساخته ایم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غوطه در آتش سوزنده چو پیکان زده ایم</p></div>
<div class="m2"><p>تا دل خویش موافق به زبان ساخته ایم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صائب از کلک سخنور چو عصای موسی</p></div>
<div class="m2"><p>چشمه ها از جگر سنگ روان ساخته ایم</p></div></div>