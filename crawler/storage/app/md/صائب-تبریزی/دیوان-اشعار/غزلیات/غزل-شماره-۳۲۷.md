---
title: >-
    غزل شمارهٔ ۳۲۷
---
# غزل شمارهٔ ۳۲۷

<div class="b" id="bn1"><div class="m1"><p>عتاب و لطف می گردد ز ابروی بتان پیدا</p></div>
<div class="m2"><p>که باشد قوت بازوی هر کس از کمان پیدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو تار از گوهر و جوهر ز تیغ و موجه از ساغر</p></div>
<div class="m2"><p>بود از پیکر سیمین او رگ‌های جان پیدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نسازد حسن را چون مضطرب نادیدن عاشق؟</p></div>
<div class="m2"><p>که گل بر خویش لرزد چون نباشد باغبان پیدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نسیم پیرهن را در کنار مصر می گیرم</p></div>
<div class="m2"><p>که دارد صبر، تا گردد غبار کاروان پیدا؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمی آید به چشم از پرتو دل، داغهای من</p></div>
<div class="m2"><p>ستاره روز روشن چون شود از آسمان پیدا؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز آه سرد من خورشید تابان رنگ می بازد</p></div>
<div class="m2"><p>بلرزد برگ بر خود چون شود باد خزان پیدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیامد آفتاب بی مروت بر سر احسان</p></div>
<div class="m2"><p>چو ماه نو ز پهلویم نشد تا استخوان پیدا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه باشد شعله غیرت، چراغ زیر دامن را؟</p></div>
<div class="m2"><p>نگردد همت عالی به زیر آسمان پیدا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درین موسم که صائب می کند هنگامه آرایی</p></div>
<div class="m2"><p>چه خوش باشد اگر بلبل شود در بوستان پیدا</p></div></div>