---
title: >-
    غزل شمارهٔ ۳۲۲۷
---
# غزل شمارهٔ ۳۲۲۷

<div class="b" id="bn1"><div class="m1"><p>قبول عشق سرکش را دل دیوانه می باید</p></div>
<div class="m2"><p>که تاج خسروان را گوهر یکدانه می باید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کنم در سینه و دل درد و داغ عشق را پنهان</p></div>
<div class="m2"><p>که مه در زیر ابر و گنج در ویرانه می باید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشو با مهلت دنیا زتمهید سفر غافل</p></div>
<div class="m2"><p>که یک پا در برون در، یکی در خانه می باید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زآتش چون سیاوش می توان سالم برون آمد</p></div>
<div class="m2"><p>دعای جوشنی از همت مردانه می باید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هر کس قسمت خود می رساند چرخ مینایی</p></div>
<div class="m2"><p>نماند در صراحی آنچه در پیمانه می باید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلا از پای منشین گر هوای زلف او داری</p></div>
<div class="m2"><p>که صد پا کوچه گرد زلف را چون شانه می باید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حجاب و شرم را بگذار در بیرون در صائب</p></div>
<div class="m2"><p>که آتش طلعتان را جرأت پروانه می باید</p></div></div>