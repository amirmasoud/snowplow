---
title: >-
    غزل شمارهٔ ۵۶۹
---
# غزل شمارهٔ ۵۶۹

<div class="b" id="bn1"><div class="m1"><p>مشرق مهر بود سینه بی کینه ما</p></div>
<div class="m2"><p>صاف چون صبح به آفاق بود سینه ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون اگر در جگر نافه آهو شد مشک</p></div>
<div class="m2"><p>مشک خون می شود از خرقه پشمینه ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ریشه سبزه زنگار رسیده است به آب</p></div>
<div class="m2"><p>چه خیال است که روشن شود آیینه ما؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن که بر نعمت الوان جهان دارد دست</p></div>
<div class="m2"><p>می برد رشک به نان جو و کشکینه ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا ز مستی جهالت به خمار افتادیم</p></div>
<div class="m2"><p>به کدورت گذرد شنبه و آدینه ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در پریخانه ما جغد هما می گردد</p></div>
<div class="m2"><p>صبح شنبه خجل است از شب آدینه ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شسته رو می شود از گرد یتیمی صائب</p></div>
<div class="m2"><p>گوهری را که فتد راه به گنجینه ما</p></div></div>