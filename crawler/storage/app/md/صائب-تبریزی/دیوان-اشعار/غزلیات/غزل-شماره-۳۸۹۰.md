---
title: >-
    غزل شمارهٔ ۳۸۹۰
---
# غزل شمارهٔ ۳۸۹۰

<div class="b" id="bn1"><div class="m1"><p>اثر ز همت مستانه در شراب نماند</p></div>
<div class="m2"><p>فغان که در گهر شاهوارآب نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زبس که شیرمراکرداین ستمگر خون</p></div>
<div class="m2"><p>ز روزگار امیدم به انقلاب نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منم که از دل خود نیست قسمتم ورنه</p></div>
<div class="m2"><p>به دست کیست که فردی ازین کتاب نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دلنوازی ما بست روزگارکمر</p></div>
<div class="m2"><p>کنون که هیچ اثر از دل خراب نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زفیض پیر مغان ناامید چون باشم</p></div>
<div class="m2"><p>که لعل در جگر سنگ بی شراب نماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نداشت چاشنی بوسه پیش ازین دشنام</p></div>
<div class="m2"><p>ز اشک ما رگ تلخی درین گلاب نماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر نمی چکدم خون ز دل غفلت نیست</p></div>
<div class="m2"><p>که نم ز تندی آتش درین کباب نماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که رو به وادی دنیا پرفریب نهاد</p></div>
<div class="m2"><p>که در کشاکش ایام چون سراب نماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هزار شکر که جز دل درین جهان صائب</p></div>
<div class="m2"><p>مرا امید گشایش به هیچ باب نماند</p></div></div>