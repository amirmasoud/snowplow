---
title: >-
    غزل شمارهٔ ۳۳۶۹
---
# غزل شمارهٔ ۳۳۶۹

<div class="b" id="bn1"><div class="m1"><p>از خموشی دل روشن گهران آب خورد</p></div>
<div class="m2"><p>کوزه سر بسته چو گردید می ناب خورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرد غربت ز رخش بحر کند پاک آخر</p></div>
<div class="m2"><p>هر که در راه طلب گرد چو سیلاب خورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می فزاید گرهی بر گره مشکل دل</p></div>
<div class="m2"><p>رشته جان اگر از چرخ چنین تاب خورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست یک جرعه درین میکده بی خون جگر</p></div>
<div class="m2"><p>باده در جام کند عاشق و خوناب خورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دایم از خانه برون دشمن من می آید</p></div>
<div class="m2"><p>سنگ بر شیشه ام از زور می ناب خورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نفسش نکهت پیراهن یوسف دارد</p></div>
<div class="m2"><p>دل هرکس که ازان چاه ذقن آب خورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به زبان صحبت اشراق ندارد حاجت</p></div>
<div class="m2"><p>شمع روشن دل خود در شب مهتاب خورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عمر جاوید شود در نظرش موج سراب</p></div>
<div class="m2"><p>خضر اگر زخمی ازان خنجر سیراب خورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نرود حسرت شمشیر تو از دل به هلاک</p></div>
<div class="m2"><p>گر چه در خواب بود تشنه همان آب خورد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حسن بر عاشق صادق نکند رحم که صبح</p></div>
<div class="m2"><p>خون ز پیمانه خورشید جهانتاب خورد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در جهانی که تهیدست برون باید رفت</p></div>
<div class="m2"><p>ساده لوح آن که غم رفتن اسباب خورد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کرد دخل کج احباب ز جان سیر مرا</p></div>
<div class="m2"><p>تا به کی ماهی من طعمه ز قلاب خورد؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ندهد لعل تو از سنگدلی نم بیرون</p></div>
<div class="m2"><p>مگر از چاه زنخدان تو دل آب خورد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چند در شیشه سر بسته گردون صائب</p></div>
<div class="m2"><p>خون خود را دل بیتاب چو سیماب خورد</p></div></div>