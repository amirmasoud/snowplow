---
title: >-
    غزل شمارهٔ ۶۳۰۷
---
# غزل شمارهٔ ۶۳۰۷

<div class="b" id="bn1"><div class="m1"><p>صدای پا نبود در خرام درویشان</p></div>
<div class="m2"><p>زمین به خواب رود زیر گام درویشان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو نی اگر چه بود خشک، کام درویشان</p></div>
<div class="m2"><p>شکر به تنگ بود در کلام درویشان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه لذت است که بادست پخت بی برگی است</p></div>
<div class="m2"><p>نمکچش است سراسر طعام درویشان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر ز سنگ حوادث شود هلال هلال</p></div>
<div class="m2"><p>صدا بلند نگردد ز جام درویشان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که می تواند وصف تمام ایشان کرد؟</p></div>
<div class="m2"><p>به از تمام جهان، ناتمام درویشان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز روستای طمع رخت برده اند برون</p></div>
<div class="m2"><p>مساز روی ترش از سلام درویشان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میان مور و سلیمان تفاوتی ننهد</p></div>
<div class="m2"><p>چو سفره باز کند فیض عام درویشان</p></div></div>