---
title: >-
    غزل شمارهٔ ۵۶۴۰
---
# غزل شمارهٔ ۵۶۴۰

<div class="b" id="bn1"><div class="m1"><p>تاخت از سینه به مژگان دل بازیگوشم</p></div>
<div class="m2"><p>گشت بال و پر طوفان دل بازیگوشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من که در صومعه سر حلقه پیران بودم</p></div>
<div class="m2"><p>کرد بازیچه طفلان دل بازیگوشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من کجا، خنده زدن چون گل بیدرد کجا؟</p></div>
<div class="m2"><p>کرد چون غنچه پریشان دل بازیگوشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر چه در کنج قفس بال و پرم غنچه شده است</p></div>
<div class="m2"><p>می کند سیر گلستان دل بازیگوشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیقرارست چو مو بر سر آتش، تا شد</p></div>
<div class="m2"><p>زلف را سلسله جنبان دل بازیگوشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه شب در تن مجروح ز بی آرامی</p></div>
<div class="m2"><p>می کند سیر چو پیکان دل بازیگوشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هست چون برق نمایان ز رگ ابر بهار</p></div>
<div class="m2"><p>از سر زلف پریشان دل بازیگوشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست از ترکش پر تیر خطر پیکان را</p></div>
<div class="m2"><p>می زند بر صف مژگان دل بازیگوشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بس که زد قطره به هر کوچه بآورد آخر</p></div>
<div class="m2"><p>گرد از عالم امکان دل بازیگوشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همچو طوطی که ز آیینه به گفتار آید</p></div>
<div class="m2"><p>شد از آن چهره سخندان دل بازیگوشم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیست ممکن که به فکر من بیدل افتد</p></div>
<div class="m2"><p>صائب از حلقه طفلان دل بازیگوشم</p></div></div>