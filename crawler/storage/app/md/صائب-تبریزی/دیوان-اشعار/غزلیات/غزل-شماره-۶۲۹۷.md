---
title: >-
    غزل شمارهٔ ۶۲۹۷
---
# غزل شمارهٔ ۶۲۹۷

<div class="b" id="bn1"><div class="m1"><p>عقل سالم ز می ناب نیاید بیرون</p></div>
<div class="m2"><p>کشتی کاغذی از آب نیاید بیرون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست ممکن، نشود دل ز می ناب سیاه</p></div>
<div class="m2"><p>زنده اخگر ز ته آب نیاید بیرون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا به روشنگر دریا نرساند خود را</p></div>
<div class="m2"><p>تیرگی از دل سیلاب نیاید بیرون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پای خوابیده بود در ته دامن بیدار</p></div>
<div class="m2"><p>زاهد آن به که ز محراب نیاید بیرون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می برد عزت غربت وطن از یاد غریب</p></div>
<div class="m2"><p>آب از گوهر سیراب نیاید بیرون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رزق کج بحث ز تحصیل بود دست تهی</p></div>
<div class="m2"><p>گوهر از بحر به قلاب نیاید بیرون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لازم قامت خم گشته بود طول امل</p></div>
<div class="m2"><p>موج از حلقه گرداب نیاید بیرون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک جهت شو که ز صد زاهد شیاد یکی</p></div>
<div class="m2"><p>خالص از بوته محراب نیاید بیرون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رو نهان می کند از روشنی دل شیطان</p></div>
<div class="m2"><p>دزد بیدل شب مهتاب نیاید بیرون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مکن ای سنگدل از شکوه مرا منع که زخم</p></div>
<div class="m2"><p>می کشد زود چو خوناب نیاید بیرون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در گرانجان نبود زخم زبان را تأثیر</p></div>
<div class="m2"><p>خون به نشتر ز رگ خواب نیاید بیرون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خودنمایی نبود شیوه واصل شدگان</p></div>
<div class="m2"><p>زنده ماهی ز ته آب نیاید بیرون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به صد امید دل شبنم ما آب شده است</p></div>
<div class="m2"><p>آه اگر مهر جهانتاب نیاید بیرون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نزند دست به دامان اجابت صائب</p></div>
<div class="m2"><p>ناله ای کز دل بی تاب نیاید بیرون</p></div></div>