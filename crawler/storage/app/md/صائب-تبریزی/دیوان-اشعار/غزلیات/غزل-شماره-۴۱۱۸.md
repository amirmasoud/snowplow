---
title: >-
    غزل شمارهٔ ۴۱۱۸
---
# غزل شمارهٔ ۴۱۱۸

<div class="b" id="bn1"><div class="m1"><p>شرم از نگاه آن گل سیراب می چکد</p></div>
<div class="m2"><p>زان تیغ الحذر که ازو آب می چکد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان چشم پر خمار می ناب می چکد</p></div>
<div class="m2"><p>زان خانه الحذر که ازو آب می چکد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از سروبوستانی اگر آب می چکد</p></div>
<div class="m2"><p>ناز از خرام آن گل سیراب می چکد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از روی تازه گل اگر آب می چکد</p></div>
<div class="m2"><p>زان روی لاله رنگ می ناب می چکد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا خون آرزو نشود خشک در جگر</p></div>
<div class="m2"><p>خامی از این کباب چو خوناب می چکد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیری زآب نیست جگرهای تشنه را</p></div>
<div class="m2"><p>کی خون ما ز خنجر سیراب می چکد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوراخ میکند جگر سنگ خاره را</p></div>
<div class="m2"><p>خونابه ای که از دل بیتاب میچکد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیداری من است که چون چشم مست یار</p></div>
<div class="m2"><p>گاهی ازوخمار وگهی خواب می چکد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>امروز نیست چشم مرا اشک لاله گون</p></div>
<div class="m2"><p>زین زخم عمرهاست که خوناب می چکد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در کوی میکشان نبود راه بخل را</p></div>
<div class="m2"><p>اینجا زدست خشک سبو آب می چکد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نسبت به صبح عارض او سیل تیره ای است</p></div>
<div class="m2"><p>آب صباحتی که زمهتاب می چکد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بی چشم زخم مصرع رنگین صائب است</p></div>
<div class="m2"><p>تیغ برهنه ای که ازو آب می چکد</p></div></div>