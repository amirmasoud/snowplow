---
title: >-
    غزل شمارهٔ ۶۸۱
---
# غزل شمارهٔ ۶۸۱

<div class="b" id="bn1"><div class="m1"><p>هر کس نکرده در گرو می کتاب را</p></div>
<div class="m2"><p>نگرفته است از گل کاغذ گلاب را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل را ز درد و داغ به تدریج پخته کن</p></div>
<div class="m2"><p>هشدار خامسوز نسازی کباب را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شرمی بدار از جگر آتشین ما</p></div>
<div class="m2"><p>تا چند چون گهر به گره بندی آب را؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روشندلان ز مرگ محابا نمی کنند</p></div>
<div class="m2"><p>نور از زوال کم نشود آفتاب را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عمر دوباره مسئلت آنها که می کنند</p></div>
<div class="m2"><p>گویا ندیده اند جهان خراب را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست از هوا بشوی که ترک هوای پوچ</p></div>
<div class="m2"><p>در یک نفس رساند به دریا حباب را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روی سیه به اشک ندامت شود سفید</p></div>
<div class="m2"><p>باران برآورد ز سیاهی سحاب را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از خط ملاحت لب میگون او فزود</p></div>
<div class="m2"><p>شور از نمک زیاده شود این شراب را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر صبح و شام، غیرت آن حسن بی زوال</p></div>
<div class="m2"><p>خون از شفق کند به جگر آفتاب را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بس نیست چشم نرم ترا پرده های خواب؟</p></div>
<div class="m2"><p>کز مخمل دو خوابه کنی رخت خواب را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آرد برون چو تیر خدنگ از کمان سخت</p></div>
<div class="m2"><p>امید خاکبوس نهال تو آب را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صائب کجا به ذره ما رحم می کند؟</p></div>
<div class="m2"><p>گردون که خاکمال دهد آفتاب را</p></div></div>