---
title: >-
    غزل شمارهٔ ۲۸۵۰
---
# غزل شمارهٔ ۲۸۵۰

<div class="b" id="bn1"><div class="m1"><p>گرانی می‌کند بر تن چو سر بی‌جوش می‌گردد</p></div>
<div class="m2"><p>سبو چون خالی از می گشت بار دوش می‌گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز نور عاریت بگذر که شمع ماه تابان را</p></div>
<div class="m2"><p>اگر صدبار روشن می‌کنی خاموش می‌گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آن محفل گل از کیفیت می می‌توان چیدن</p></div>
<div class="m2"><p>که ساقی پیشتر از دیگران مدهوش می‌گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خطر بسیار دارد در کمین همواری دشمن</p></div>
<div class="m2"><p>ز سگ غافل مشو زنهار چون خاموش می‌گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آن گلشن که می در جام ریزد مست ناز من</p></div>
<div class="m2"><p>فغان بلبلان گلبانگ نوشانوش می‌گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندارد خاکساری با بزرگی جنگ در مشرب</p></div>
<div class="m2"><p>که در کوی مغان گردون سبو بر دوش می‌گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز خجلت طوق قمری دام زیر خاک خواهد شد</p></div>
<div class="m2"><p>اگر سرو چمن با قامتش هم‌دوش می‌گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه بی‌دردی است گر اشکم به چشم تر نمی‌آید</p></div>
<div class="m2"><p>چو آتش تند افتد، آب صرف جوش می‌گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قناعت کن، کز این گلشن به بویی هرکه قانع شد</p></div>
<div class="m2"><p>چو زنبور عسل کاشانه‌اش پرنوش می‌گردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از ان ماه از تمامی می‌گذارد روی در نقصان</p></div>
<div class="m2"><p>که دایم خرمن او صرف یک آغوش می‌گردد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا باغ و بهاری نیست غیر از بوی درویشی</p></div>
<div class="m2"><p>دل بیمار من از کاهگل بیهوش می‌گردد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مشو با پردلی ایمن ز خصم ناتوان صائب</p></div>
<div class="m2"><p>که از اندک نسیمی بحر جوشن‌پوش می‌گردد</p></div></div>