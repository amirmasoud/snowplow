---
title: >-
    غزل شمارهٔ ۳۴۵۷
---
# غزل شمارهٔ ۳۴۵۷

<div class="b" id="bn1"><div class="m1"><p>گرچه آن سرو روان در همه جا می باشد</p></div>
<div class="m2"><p>نیست ممکن که توان یافت کجا می باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلق را داروی بیهوشی حیرت برده است</p></div>
<div class="m2"><p>ورنه او با همه کس در همه جا می باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست ممکن که ز من دور توانی گردید</p></div>
<div class="m2"><p>عینک صافدلان دورنما می باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سر کوی تو هرکس که کند عزم سفر</p></div>
<div class="m2"><p>گر به فردوس رود رو به قفا می باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دل ماست خیال تو و از ما دورست</p></div>
<div class="m2"><p>عکس از آیینه در آیینه جدا می باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خضر در دامن صحرای طلب کمیاب است</p></div>
<div class="m2"><p>ورنه در هر سیهی آب بقا می باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جگر سوخته صاحب نظران می دارند</p></div>
<div class="m2"><p>مشک در ناف غزالان ختا می باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل سنگ تو ز بیتابی ما آسوده است</p></div>
<div class="m2"><p>قبله را کی خبر از قبله نما می باشد؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست ممکن که به رویش نگشایند دری</p></div>
<div class="m2"><p>هرکه در حلقه مردان خدا می باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سر آزاده و درد سر دولت، هیهات</p></div>
<div class="m2"><p>تیغ بر فرق من از بال هما می باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رهنوردی که سبکبار ز دنیا گذرد</p></div>
<div class="m2"><p>خار در رهگذرش دست دعا می باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هرکه جان داده درین راه، رسیده است به جان</p></div>
<div class="m2"><p>دل هرکس که ز جا رفت بجا می باشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از دم گرم تو صائب دل افسرده نماند</p></div>
<div class="m2"><p>نفس سوختگان عقده گشا می باشد</p></div></div>