---
title: >-
    غزل شمارهٔ ۴۸۸۴
---
# غزل شمارهٔ ۴۸۸۴

<div class="b" id="bn1"><div class="m1"><p>چهره زرین چو باشد مخزن زر گومباش</p></div>
<div class="m2"><p>هست چون سد رمق سد سکندر گو مباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازخشن پوشی چه پروا عارف دل زنده را؟</p></div>
<div class="m2"><p>پشت این آیینه روشن گهر زر گومباش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در گلستان بی پر و بالی است تشریف وصال</p></div>
<div class="m2"><p>بلبلان را قوت پرواز در پرگو مباش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با دل روشن چراغ روز باشد آفتاب</p></div>
<div class="m2"><p>دربساط آسمان خورشید انورگو مباش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساده لوحی خار پیراهن شمارد نقش را</p></div>
<div class="m2"><p>خانه آیینه روشن مصور گو مباش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همت عالی است مستغنی ازین دنیای پوچ</p></div>
<div class="m2"><p>بیضه عنقا هما رادر ته پرگو مباش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از گل ابری چه شوکت می فزاید بحر را</p></div>
<div class="m2"><p>دل چو شد از عشق پرخون دیده تر گو مباش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غنچه خسبان راچو هست از کاسه زانو شراب</p></div>
<div class="m2"><p>باده گلرنگ در مینا و ساغر گو مباش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون خودآرایان دنیا خرج چشم بد شوند</p></div>
<div class="m2"><p>جامه و دستار مارا برسر و برگو مباش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من که صائب لعل سازم سنگ را ازخون دل</p></div>
<div class="m2"><p>در زمین چون تنگ چشمان گنج گوهر گو مباش</p></div></div>