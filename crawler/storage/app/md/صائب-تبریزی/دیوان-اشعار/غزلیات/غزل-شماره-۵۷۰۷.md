---
title: >-
    غزل شمارهٔ ۵۷۰۷
---
# غزل شمارهٔ ۵۷۰۷

<div class="b" id="bn1"><div class="m1"><p>ز فکر پوچ درین شوره زار بی حاصل</p></div>
<div class="m2"><p>عنان گسسته تر از موجه سیراب شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو از نظاره رخسار خود مشو غافل</p></div>
<div class="m2"><p>که من ز هوش ز نظاره نقاب شدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز پشت پا که براین خاکدان زدم صائب</p></div>
<div class="m2"><p>به یک نفس چو مسیح آسمان رکاب شدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درین ریاض چو شبنم اگر چه آب شدم</p></div>
<div class="m2"><p>خوشم که محو تماشای آفتاب شدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عجب که صبح قیامت مرا کند بیدار</p></div>
<div class="m2"><p>که از نظاره آن چشم مست خواب شدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امید گنج گهر آب در گلم دارد</p></div>
<div class="m2"><p>ز ترکتاز محبت اگر خراب شدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز پیچ و تاب اگر رشته می شود کوتاه</p></div>
<div class="m2"><p>یکی هزار من از مشق پیچ و تاب شدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وبال دامن گل نیست خون بلبل من</p></div>
<div class="m2"><p>که من به شعله آواز خود کباب شدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به سیر چشمی من گوهری نداشت محیط</p></div>
<div class="m2"><p>ز چشم شور تهی چشم چون حباب شدم</p></div></div>