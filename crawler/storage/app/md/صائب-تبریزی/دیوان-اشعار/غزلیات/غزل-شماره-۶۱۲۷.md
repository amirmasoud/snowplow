---
title: >-
    غزل شمارهٔ ۶۱۲۷
---
# غزل شمارهٔ ۶۱۲۷

<div class="b" id="bn1"><div class="m1"><p>آن که ننشیند کنون از ناز در پهلوی من</p></div>
<div class="m2"><p>تکیه گاهش بود در مستی سر زانوی من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این زمان بی اعتبارم، ورنه آن سیب ذقن</p></div>
<div class="m2"><p>در سر مستی مکرر بود دستنبوی من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل نمی زد بر قفس مرغ گرفتار مرا</p></div>
<div class="m2"><p>آن که حرف سخت می گوید کنون بر روی من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من که در دستم کمان آسمان ها بود نرم</p></div>
<div class="m2"><p>سست گردید از کمان سخت او بازوی من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون هدف آغوش رغبت عالمی وا کرده اند</p></div>
<div class="m2"><p>تا که را از خاک بردارد کمان ابروی من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم پاک من بود از خاک دامنگیرتر</p></div>
<div class="m2"><p>سرو نتواند گذشتن از کنار جوی من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کلک من دارد در انشای سخن دست دگر</p></div>
<div class="m2"><p>آب صائب می شود چون تاک می در جوی من</p></div></div>