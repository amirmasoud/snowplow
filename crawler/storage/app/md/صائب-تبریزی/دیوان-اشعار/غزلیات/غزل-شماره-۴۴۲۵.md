---
title: >-
    غزل شمارهٔ ۴۴۲۵
---
# غزل شمارهٔ ۴۴۲۵

<div class="b" id="bn1"><div class="m1"><p>سرمست چو آن شاخ گل از باغ برآید</p></div>
<div class="m2"><p>باغش چو نفس سوختگان بر اثر آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر سو که کند شاخ گلش میل ز مستی</p></div>
<div class="m2"><p>آغوش گشا بلبلی از خاک برآید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حسن تو ز بسیاری سامان لطافت</p></div>
<div class="m2"><p>در دیده هر کس به لباس دگر آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از شوق تماشای جمال تو گل از شاخ</p></div>
<div class="m2"><p>چون لاله نفس سوخته از خاک برآید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان در ره شیرین دهنان باز که تا حشر</p></div>
<div class="m2"><p>آوازه فرهاد ز کوه و کمر آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از عشق به کوشش نتوان کامروا شد</p></div>
<div class="m2"><p>در آتش سوزنده چه از بال و پر آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشمی که در او آب حیاپرده نشین است</p></div>
<div class="m2"><p>از پوست برون زود چو بادام ترآید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در ذکر خدا به که شود صرف چو تسبیح</p></div>
<div class="m2"><p>ایام حیاتی که به صدسال سرآید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب نشود لاله صفت شسته به باران</p></div>
<div class="m2"><p>رنگی که به رخسار به خون جگر آید</p></div></div>