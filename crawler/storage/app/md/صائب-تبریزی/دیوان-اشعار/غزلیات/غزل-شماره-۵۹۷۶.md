---
title: >-
    غزل شمارهٔ ۵۹۷۶
---
# غزل شمارهٔ ۵۹۷۶

<div class="b" id="bn1"><div class="m1"><p>باده گلگون نمی آید به کار عاشقان</p></div>
<div class="m2"><p>از لب میگون خود بشکن خمار عاشقان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شعله نتواند لباس رنگ را تغییر داد</p></div>
<div class="m2"><p>چون برد زردی برون می از عذار عاشقان؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خانه تن را به خاک تیره یکسان کرده اند</p></div>
<div class="m2"><p>دست خالی می رود سیل از دیار عاشقان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مردم کوته نظر در انتظار محشرند</p></div>
<div class="m2"><p>نقد خود را نسیه کردن نیست کار عاشقان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کوه طورست آن که می آید ز هر پرتو به رقص</p></div>
<div class="m2"><p>نیست سنگ کم به میزان وقار عاشقان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبح محشر را نمکدان در گریبان بشکند</p></div>
<div class="m2"><p>شورش مغز پریشان روزگار عاشقان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در دل هر نقطه داغی سواد اعظمی است</p></div>
<div class="m2"><p>تند مگذر این چنین از لاله زار عاشقان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ساده از کوه گرانجانی بود صحرای عشق</p></div>
<div class="m2"><p>نقد جان در آستین دارد شرار عاشقان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که خود را باخت اینجا می زند نقش مراد</p></div>
<div class="m2"><p>پاکبازست از پشیمانی قمار عاشقان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در سراپای وجودم ذره ای بی عشق نیست</p></div>
<div class="m2"><p>محمل لیلی است هر کف از غبار عاشقان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آفتاب از دیده شبنم نمی پوشد عذار</p></div>
<div class="m2"><p>رخ مپوش از دیده شب زنده دار عاشقان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیش الماس حوادث با کمال سرکشی</p></div>
<div class="m2"><p>خواب مخمل می شود در رهگذار عاشقان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خار صحرای ادب را دست دامنگیر نیست</p></div>
<div class="m2"><p>زینهار ای گل مکش دامن ز خار عاشقان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دامن برق تجلی خار نتواند گرفت</p></div>
<div class="m2"><p>دست کوته کن ز نبض بی قرار عاشقان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خاک بیدردان به شمع دیگران دارد نظر</p></div>
<div class="m2"><p>آتش از خود می دهد بیرون مزار عاشقان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر که می داند شمار داغهای خویش را</p></div>
<div class="m2"><p>نیست روز حشر صائب در شمار عاشقان</p></div></div>