---
title: >-
    غزل شمارهٔ ۲۱۳۴
---
# غزل شمارهٔ ۲۱۳۴

<div class="b" id="bn1"><div class="m1"><p>شیرازه جمعیت مستان خط جام است</p></div>
<div class="m2"><p>آزاد بود هر که درین حلقه دام است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گردون که ازو صبح امید همه شد شام</p></div>
<div class="m2"><p>از زلف گرهگیر تو یک حلقه دام است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چندان که نظر بر دل و دلدار فکندم</p></div>
<div class="m2"><p>معلوم نشد دل چه و دلدار کدام است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با قرب، گل از تیغ شهادت نتوان چید</p></div>
<div class="m2"><p>بر صید حرم، آب دم تیغ حرام است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خودداری سیماب بر آیینه محال است</p></div>
<div class="m2"><p>چشمی که به رویش ندویده است کدام است؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن اره که از تیزی دندان چکدش زهر</p></div>
<div class="m2"><p>در مشرب وحشت زدگان سین سلام است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر خاک نهادان در امید نبسته است</p></div>
<div class="m2"><p>تا بوسه خورشید نصیب لب بام است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>داغی که بود زیر سیاهی همه عمر</p></div>
<div class="m2"><p>بر جان عقیقی است که جوینده نام است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون ریگ روان، تشنگی حرص نداریم</p></div>
<div class="m2"><p>ما را چو گهر کار به یک قطره تمام است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گلزار ز گل پرده گوش است سراپا</p></div>
<div class="m2"><p>دربار نسیم سحری تا چه پیام است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فریاد که بر روی من آن رهزن امید</p></div>
<div class="m2"><p>راهی که نبسته است همین راه سلام است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صائب شود آن کس که نسنجیده سخنساز</p></div>
<div class="m2"><p>طفلی است که بازیگه او بر لب بام است</p></div></div>