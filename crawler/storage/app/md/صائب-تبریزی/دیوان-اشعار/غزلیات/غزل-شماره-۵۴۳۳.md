---
title: >-
    غزل شمارهٔ ۵۴۳۳
---
# غزل شمارهٔ ۵۴۳۳

<div class="b" id="bn1"><div class="m1"><p>از حجاب عشق محروم از گل روی توایم</p></div>
<div class="m2"><p>ورنه ما صد پیرهن محرم تر از بوی توایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر به ظاهر چون نگاه از چشم دور افتاده ایم</p></div>
<div class="m2"><p>هرکجا باشیم در محراب ابروی توایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه در چون غنچه بر روی دو عالم بسته ایم</p></div>
<div class="m2"><p>چشم بر راه نسیم آشنا روی توایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما ز چشم پاک، چون آیینه بر بزم حضور</p></div>
<div class="m2"><p>بر سر دست تو و بر روی زانوی توایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست ما را در وفاداری به مردم نسبتی</p></div>
<div class="m2"><p>دیگران آبند و ما ریگ ته جوی توایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سنگ را هرچند با گوهر نمی سنجد کسی</p></div>
<div class="m2"><p>قدر ما این بس که گاهی در ترازوی توایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کیست تا چون سایه ما را جز تو برگیرد ز خاک؟</p></div>
<div class="m2"><p>بر زمین افتاده بالای دلجوی توایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرکشان عشق را در کاسه سر خاک کن</p></div>
<div class="m2"><p>ورنه ما پیوسته زیر تیغ ابروی توایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما که چون شبنم ز گل بالین و بستر داشتیم</p></div>
<div class="m2"><p>این زمان از غنچه خسبان سر کوی توایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما که صائب ره به حرف آشنایان بسته ایم</p></div>
<div class="m2"><p>گوش بر آواز حرف آشنا روی توایم</p></div></div>