---
title: >-
    غزل شمارهٔ ۵۳۴۷
---
# غزل شمارهٔ ۵۳۴۷

<div class="b" id="bn1"><div class="m1"><p>گر ز خود بیرون کسی فریادرس می‌داشتم</p></div>
<div class="m2"><p>می‌کشیدم ناله از دل تا نفس می‌داشتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سود من در پله نقصان ز بی‌سرمایگی است</p></div>
<div class="m2"><p>می‌شدم سیمرغ اگر بال مگس می‌داشتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صحبت همدرد زندانم را گلستان می‌کند</p></div>
<div class="m2"><p>هم‌نوایی کاش در کنج قفس می‌داشتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وحشت ذاتی گوارا کرد عزلت را به من</p></div>
<div class="m2"><p>می‌شدم دیوانه گر الفت به کس می‌داشتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این زمان شد سینه‌ام تاریک ورنه پیش ازین</p></div>
<div class="m2"><p>صبح را آیینه در پیش نفس می‌داشتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد ز قحط آه از مطلب کمندم نارسا</p></div>
<div class="m2"><p>کاش دودی در جگر چون خار و خس می‌داشتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پله دوری ز محمل بود منظور ادب</p></div>
<div class="m2"><p>گوش اگر گاهی به آواز جرس می‌داشتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حرف تلخی از دهان او به من هم می‌رسید</p></div>
<div class="m2"><p>گر زبان شکوه چون اهل هوس می‌داشتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی‌کسی‌ها ناله را بر من گوارا کرده است</p></div>
<div class="m2"><p>مُهر بر لب می‌زدم گر دادرس می‌داشتم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پختگی دریای پرشور مرا خاموش کرد</p></div>
<div class="m2"><p>کاش جوشی چون شراب نیمرس می‌داشتم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا دل از ذوق گرفتاری به آزادی رسید</p></div>
<div class="m2"><p>شد تمام امید بیمی کز عسس می‌داشتم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از نفس‌های پریشان تیره شد آیینه‌ام</p></div>
<div class="m2"><p>صبح می‌گشتم اگر پاس نفس می‌داشتم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر نمی‌گردید در عالم کس من بی‌کسی</p></div>
<div class="m2"><p>از کسان صائب من بی‌کس چه کس می‌داشتم</p></div></div>