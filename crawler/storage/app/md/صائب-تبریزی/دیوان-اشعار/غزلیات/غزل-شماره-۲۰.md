---
title: >-
    غزل شمارهٔ ۲۰
---
# غزل شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>از جهان تا رشته تابی دسترس باشد ترا</p></div>
<div class="m2"><p>هر سر خاری درین وادی عسس باشد ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند از آمیزش دریای وحدت چون حباب</p></div>
<div class="m2"><p>پرده دار چشم کوته بین، نفس باشد ترا؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا تو می لرزی به تار و پود هستی همچو موج</p></div>
<div class="m2"><p>قسمت از دریای گوهر خار و خس باشد ترا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم بی شرم تو سیری را نمی داند که چیست</p></div>
<div class="m2"><p>در تلاش رزق تا حرص مگس باشد ترا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون شرر در سنگ، بی برگی ترا دارد ضعیف</p></div>
<div class="m2"><p>می شوی سرکش اگر یک مشت خس باشد ترا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می شوی افتاده تر، هر چند برخیزی ز جا</p></div>
<div class="m2"><p>تا ز مردم دستگیری ملتمس باشد ترا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شرم دار از حق، منال از بی کسی چون ناکسان</p></div>
<div class="m2"><p>کیست آخر عالم ناکس که کس باشد ترا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از گرفتاران خود، صیاد می گیرد خبر</p></div>
<div class="m2"><p>فکر روزی، چند در کنج قفس باشد ترا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آرزو کرده است آبستن ترا همچو زنان</p></div>
<div class="m2"><p>زان ز دنیا هر زمان چیزی هوس باش ترا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صرف در پرداز دل کن قوت بازوی خویش</p></div>
<div class="m2"><p>در جهان تیره صائب تا نفس باشد ترا</p></div></div>