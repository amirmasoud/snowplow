---
title: >-
    غزل شمارهٔ ۲۲۶۵
---
# غزل شمارهٔ ۲۲۶۵

<div class="b" id="bn1"><div class="m1"><p>چون گذارد خشت اول بر زمین معمار کج</p></div>
<div class="m2"><p>گر رساند بر فلک، باشد همان دیوار کج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می کند یک جانب از خوان تهی سرپوش را</p></div>
<div class="m2"><p>هر سبک مغزی که بر سر می نهد دستار کج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلف کج بر چهره خوبان قیامت می کند</p></div>
<div class="m2"><p>در مقام خود بود از راست به، بسیار کج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راستی در سرو و خم در شاخ گل زیبنده است</p></div>
<div class="m2"><p>قد خوبان راست باید، زلف عنبر بار کج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست جز بیرون در جای اقامت حلقه را</p></div>
<div class="m2"><p>راه در دلها نیابد چون بود گفتار کج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فقر سازد نفس را عاجز، که چون شد تنگ راه</p></div>
<div class="m2"><p>راست سازد خویش را هر چند باشد مار کج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قامت خم بر نیاورد از خسیسی نفس را</p></div>
<div class="m2"><p>بیش آویزد به دامن ها چو گردد خار کج</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هست چون بر نقطه فرمان مدار کاینات</p></div>
<div class="m2"><p>عیب نتوان کرد اگر باشد خط پرگار کج</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در نیام کج نسازد تیغ قد خویش راست</p></div>
<div class="m2"><p>زیر گردون هر که باشد، می شود ناچار کج</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می تراود از سراپای دل آزاران کجی</p></div>
<div class="m2"><p>باشد از مرغ شکاری ناخن و منقار کج</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از تواضع کم نگردد رتبه گردنکشان</p></div>
<div class="m2"><p>نیست عیبی گر بود شمشیر جوهردار کج</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وسعت مشرب، عنان عقل می پیچد ز راه</p></div>
<div class="m2"><p>موج را بر صفحه دریا بود رفتار کج</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گریه مستانه خواهد سرخ رویش ساختن</p></div>
<div class="m2"><p>از درختان تاک را باشد اگر رفتار کج</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>راست شو صائب نخواهی کج اگر آثار خویش</p></div>
<div class="m2"><p>سایه افتد بر زمین کج، چون بود دیوار کج</p></div></div>