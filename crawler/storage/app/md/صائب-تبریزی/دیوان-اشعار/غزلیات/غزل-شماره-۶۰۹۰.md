---
title: >-
    غزل شمارهٔ ۶۰۹۰
---
# غزل شمارهٔ ۶۰۹۰

<div class="b" id="bn1"><div class="m1"><p>تا نگردد چهره نوخط زلف را کوته مکن</p></div>
<div class="m2"><p>ای ستمگر رشته امید ما کوته مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می شود جان تازه از آواز پای آشنا</p></div>
<div class="m2"><p>از مزار کشتگان خویش پا کوته مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کشتی بی بادبان کمتر به ساحل می رسد</p></div>
<div class="m2"><p>دست از دامان مردان خدا کوته مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرسری از فیض صحرای طلب نتوان گذشت</p></div>
<div class="m2"><p>از شتاب این راه را ای رهنما کوته مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی کشش نتوان به پای آهن این ره را برید</p></div>
<div class="m2"><p>دست خود ای سوزن از آهن ربا کوته مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می گشاید از دعا هر عقده مشکل که هست</p></div>
<div class="m2"><p>مشکلی چون رودهد دست از دعا کوته مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شمع را فانوس از آفات می دارد نگاه</p></div>
<div class="m2"><p>دست از دامان آن گلگون قبا کوته مکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکر این معنی که داری در حریم غنچه راه</p></div>
<div class="m2"><p>از قفس پای خود ای باد صبا کوته مکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می شود صائب دعا در دامن شب مستجاب</p></div>
<div class="m2"><p>دست خود زنهار ازان زلف دوتا کوتاه مکن</p></div></div>