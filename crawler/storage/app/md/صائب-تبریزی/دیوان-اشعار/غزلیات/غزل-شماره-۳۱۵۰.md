---
title: >-
    غزل شمارهٔ ۳۱۵۰
---
# غزل شمارهٔ ۳۱۵۰

<div class="b" id="bn1"><div class="m1"><p>زخود بیگانگی را آشنایی عشق می داند</p></div>
<div class="m2"><p>به خود مشغول بودن را جدایی عشق می ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همان با زلف لیلی روح مجنون می کند بازی</p></div>
<div class="m2"><p>ز زنجیر محبت کی رهایی عشق می داند؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگو چون بلبل و قمری سخن از سرو و گل اینجا</p></div>
<div class="m2"><p>که این افسانه ها را ژاژ خایی عشق می داند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به بزم عشق مهر بی نیازی بر مدار از لب</p></div>
<div class="m2"><p>که همت خواستن را هم گدایی عشق می داند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل خوش مشرب و پیشانی وا کرده ای دارد</p></div>
<div class="m2"><p>که سنگ کودکان را مومیایی عشق می داند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو دیدی دست و تیغ عشق را از دور بسمل شو</p></div>
<div class="m2"><p>که بال و پر زدن را بد ادایی عشق می داند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زسختی رو نمی تابد، زکوه غم نمی نالد</p></div>
<div class="m2"><p>نژاد از سنگ دارد مومیایی، عشق می داند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نمی دانم چه سازم تا فنای مطلقم داند</p></div>
<div class="m2"><p>که در خود گم شدن را خودنمایی عشق می داند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو عشق آمد به دل صائب مکن اندیشه سامان</p></div>
<div class="m2"><p>کجا چون عقل ناقص کدخدایی عشق می داند؟</p></div></div>