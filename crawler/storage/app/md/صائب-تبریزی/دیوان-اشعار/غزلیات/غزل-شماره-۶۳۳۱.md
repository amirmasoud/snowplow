---
title: >-
    غزل شمارهٔ ۶۳۳۱
---
# غزل شمارهٔ ۶۳۳۱

<div class="b" id="bn1"><div class="m1"><p>رواق چرخ شد از شمع کلک من روشن</p></div>
<div class="m2"><p>که دیده است ز یک شمع نه لگن روشن؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر چراغ سهیل از نسیم کشته شود</p></div>
<div class="m2"><p>توان نمودن از سیب آن ذقن روشن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عجب نباشد اگر سرنوشت خوان شده ام</p></div>
<div class="m2"><p>که گشت از خط ساغر سواد من روشن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ستاره سوختگی خال چهره سخن است</p></div>
<div class="m2"><p>ز نقطه ریزی کلک است این سخن روشن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>توان ز زخم گرفتن عیار جوهر تیغ</p></div>
<div class="m2"><p>ز جوی شیر بود حال کوهکن روشن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که ره برون ز نهانخانه عدم می برد؟</p></div>
<div class="m2"><p>نگشته صبح شکرخند ازان دهن روشن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چراغ دل ز جگر گوشه روشنی گیرد</p></div>
<div class="m2"><p>شد از عقیق لبت دیده یمن روشن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حدیث راست منور کند جهان صائب</p></div>
<div class="m2"><p>ز روی صبح بود صدق این سخن روشن</p></div></div>