---
title: >-
    غزل شمارهٔ ۲۴۸۴
---
# غزل شمارهٔ ۲۴۸۴

<div class="b" id="bn1"><div class="m1"><p>وقت جمعی خوش که دفتر را در آب افکنده اند</p></div>
<div class="m2"><p>مهر گل از دوربینی بر گلاب افکنده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرده اند آنها که خود را در چمن گردآوری</p></div>
<div class="m2"><p>سر چو شبنم در کنار آفتاب افکنده اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساده لوحانی که دارند از جهان راحت طمع</p></div>
<div class="m2"><p>دام بهر صید در بحر سراب افکنده اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رهبر عشق حقیقی می شود عشق مجاز</p></div>
<div class="m2"><p>زین سرپل تشنگان خود را در آب افکنده اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شستشوی دیده منظورست بهر دیدنش</p></div>
<div class="m2"><p>عشقبازان گر نظر بر آفتاب افکنده اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا زخود پهلو تهی روشن ضمیران کرده اند</p></div>
<div class="m2"><p>آسمان را چون مه نو در رکاب افکنده اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غافل از افسانه نتوان کرد اهل عشق را</p></div>
<div class="m2"><p>کز دل روشن، نمک در چشم خواب افکنده اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>موی آتش دیده گردیده است مژگانها تمام</p></div>
<div class="m2"><p>تا زروی آتشین او نقاب افکنده اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کوته اندیشان که دل بر قصر دولت بسته اند</p></div>
<div class="m2"><p>دست خود چون موج بر دوش حباب افکنده اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هوشیاران صائب از دوزخ محابا می کنند</p></div>
<div class="m2"><p>میکشان صدره بر این آتش کباب افکنده اند</p></div></div>