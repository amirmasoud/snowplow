---
title: >-
    غزل شمارهٔ ۲۰۴۶
---
# غزل شمارهٔ ۲۰۴۶

<div class="b" id="bn1"><div class="m1"><p>بی عشق، آه در جگر روزگار نیست</p></div>
<div class="m2"><p>بی درد، تاب در کمر روزگار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حیرانیان روی عرقناک یار را</p></div>
<div class="m2"><p>پروای بحر پر خطر روزگار نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل زبون، رعیت این بی مروت است</p></div>
<div class="m2"><p>در ملک بیخودی خبر روزگار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی چشم زخم، روی به خون شسته من است</p></div>
<div class="m2"><p>رویی که زخمی نظر روزگار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در زیر پوست نیست جهان وجود را</p></div>
<div class="m2"><p>خونی که رزق نیشتر روزگار نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خط مسلمی ز علایق گرفته ام</p></div>
<div class="m2"><p>ما را دماغ دردسر روزگار نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از چشم مور حرص، شکر خواب برده است</p></div>
<div class="m2"><p>شیرینیی که در شکر روزگار نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا نبض آرمیدگی دل نجسته است</p></div>
<div class="m2"><p>اندیشه ای ز شور و شر روزگار نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آب مروتی که جگر سینه چاک اوست</p></div>
<div class="m2"><p>زحمت مکش که در گهر روزگار نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آزادگان به ملک جهان دل نبسته اند</p></div>
<div class="m2"><p>این بیضه زیر بال و پر روزگار نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن را که عشق لنگر حیرت به دست داد</p></div>
<div class="m2"><p>پروای بحر پر خطر روزگار نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صائب به خاک راه مریز آبروی خویش</p></div>
<div class="m2"><p>چون آب رحم در جگر روزگار نیست</p></div></div>