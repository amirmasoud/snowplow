---
title: >-
    غزل شمارهٔ ۴۰۶
---
# غزل شمارهٔ ۴۰۶

<div class="b" id="bn1"><div class="m1"><p>مسخر کرد خط عنبرین رخسار جانان را</p></div>
<div class="m2"><p>پری آورد در زیر نگین ملک سلیمان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لب جان بخش او را نیست پروای خط مشکین</p></div>
<div class="m2"><p>سیاهی نیل چشم زخم باشد آب حیوان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی صد شد ز خط عنبرین آن حسن روزافزون</p></div>
<div class="m2"><p>شبستان سرمه روشندلی شد شمع تابان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو شد نومید ازان رخسار نازک قطره شبنم</p></div>
<div class="m2"><p>ز برگ لاله و گل بر جگر افشرد دندان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به طفل نی سواری داده ام دل را ز بی باکی</p></div>
<div class="m2"><p>که بر شیران کند انگشت زنهاری نیستان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مکن تعجیل در قطع علایق چون سبکساران</p></div>
<div class="m2"><p>به همواری برون آور ز چنگ خار دامان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر از اوضاع دنیا درهمی صائب نظر وا کن</p></div>
<div class="m2"><p>که بیداری بود لاحول، این خواب پریشان را</p></div></div>