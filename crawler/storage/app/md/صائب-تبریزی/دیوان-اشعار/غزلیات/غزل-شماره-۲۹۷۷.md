---
title: >-
    غزل شمارهٔ ۲۹۷۷
---
# غزل شمارهٔ ۲۹۷۷

<div class="b" id="bn1"><div class="m1"><p>سخن رنگ اثر از سینه افگار می گیرد</p></div>
<div class="m2"><p>نسیم ساده دل بوی گل از گلزار می گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تماشای رخش در پرده می کردم، ندانستم</p></div>
<div class="m2"><p>که این آیینه از آب گهر زنگار می گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که در بیرون در مانده است کامشب بوستان پیرا</p></div>
<div class="m2"><p>به جوش لاله و گل رخنه دیوار می گیرد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فریب عقل خوردم، دامن مستی رها کردم</p></div>
<div class="m2"><p>ندانستم که اینجا محتسب هشیار می گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درخت بی ثمر بارست بر دل، سرو اگر باشد</p></div>
<div class="m2"><p>جهان را زود دل از مردم بیکار می گیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به آه و ناله گفتم دل تهی سازم، ندانستم</p></div>
<div class="m2"><p>که عشق اول زبان زین لشکر خونخوار می گیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگرچه شبنم این بوستانم در عزیزیها</p></div>
<div class="m2"><p>غبار خاطر من رخنه دیوار می گیرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رگ خوابی که می داند کمند عیش بیدردش</p></div>
<div class="m2"><p>دل بیدار عاشق رشته زنار می گیرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پذیرای نصیحت نیست دل اهل تنعم را</p></div>
<div class="m2"><p>چو کاغذ چرب باشد نقش را دشوار می گیرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خیانتهای پنهان می کشد آخر به رسوایی</p></div>
<div class="m2"><p>که دزد خانگی را شحنه در بازار می گیرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زجوش لاله پروا نیست سیل نوبهاران را</p></div>
<div class="m2"><p>کجا خون دامن آن سرو خوش رفتار می گیرد؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چه آتش بود عشق افکند در خرمن مرا صائب؟</p></div>
<div class="m2"><p>که جوش مغز هر دم از سرم دستار می گیرد</p></div></div>