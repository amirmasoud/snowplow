---
title: >-
    غزل شمارهٔ ۴۱۵۲
---
# غزل شمارهٔ ۴۱۵۲

<div class="b" id="bn1"><div class="m1"><p>عشاق سر به جیب نه آسان کشیده اند</p></div>
<div class="m2"><p>جان داده اند و سر به گریبان کشیده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر خدا ز خلق شکایت نکرده اند</p></div>
<div class="m2"><p>در راه کعبه ناز مغیلان کشیده اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در حلقه نظارگیان با کمال قرب</p></div>
<div class="m2"><p>خط بر زمین ز سایه مژگان کشیده اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون بوریا شکسته دلان حریم عشق</p></div>
<div class="m2"><p>مشق شکستگی به دبستان کشیده اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در هیچ ذره نیست که شوری ز عشق نیست</p></div>
<div class="m2"><p>هر جا سری است در خم چوگان کشیده اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنها که کار را به درستی بنا کنند</p></div>
<div class="m2"><p>پا را شکسته اند و به دامان کشیده اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از نقطه یک کتاب سخن اخذ کرده اند</p></div>
<div class="m2"><p>مضمون نامه از لب عنوان کشیده اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اهل نظر به دیده مردم چو مردمک</p></div>
<div class="m2"><p>در گردشند و پای به دامان کشیده اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای حشر خلق را به شکر خواب نیستی</p></div>
<div class="m2"><p>بگذار یک دو روز که طوفان کشیده اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از تاب آفتاب رخ یار فتنه ها</p></div>
<div class="m2"><p>خود را به زیر سایه مژگان کشیده اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب جواب آن غزل سید ست این</p></div>
<div class="m2"><p>کاین نقش بین که برورق جان کشیده اند</p></div></div>