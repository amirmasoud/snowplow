---
title: >-
    غزل شمارهٔ ۶۰۹۷
---
# غزل شمارهٔ ۶۰۹۷

<div class="b" id="bn1"><div class="m1"><p>چون زند موج حلاوت کلک شکر بار من</p></div>
<div class="m2"><p>پسته خندان شود لب بسته از گفتار من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دامن فکر من است از دامن گل پاکتر</p></div>
<div class="m2"><p>چشم شبنم می پرد در حسرت گلزار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون صدف دریادلان را باز می ماند دهن</p></div>
<div class="m2"><p>گوهرافشانی کند چون کلک گوهربار من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در پس آیینه از خجلت نهان گردیده اند</p></div>
<div class="m2"><p>طوطیان در روزگار کلک شکر بار من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عالمی بیدار شد از ناله ام، گویا شده است</p></div>
<div class="m2"><p>مشرق صبح قیامت رخنه منقار من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرو و شمشاد و صنوبر پایکوبان می شوند</p></div>
<div class="m2"><p>هر که خواند در چمن یک مصرع از افکار من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حلقه بیرون در کرده است خلط و زلف را</p></div>
<div class="m2"><p>بر بیاض گردن سیمین بران اشعار من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شیشه گردون خطر دارد ز زور باده ام</p></div>
<div class="m2"><p>کیست تا بر لب گذارد ساغر سرشار من؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سینه افسرده گلشن در ایام خزان</p></div>
<div class="m2"><p>می زند جوش بهار از گرمی گفتار من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر رگ سنگی شود انگشت زنهار دگر</p></div>
<div class="m2"><p>کوه را گر دل فشارد ناله های زار من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همچو کوه قاف در موج پری پنهان شده است</p></div>
<div class="m2"><p>بیستون عشق از فرهاد شیرین کار من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دست گلچین غنچه از جوش بهاران می شود</p></div>
<div class="m2"><p>ورنه چوب منع را ره نیست در گلزار من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مزد کار من ز ذوق کار من آماده است</p></div>
<div class="m2"><p>کارفرما فارغ است از اهتمام کار من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رشته موج سراب از جوش گوهر بگسلد</p></div>
<div class="m2"><p>آستین چون برفشاند ابر گوهربار من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بحر نتواند نفس دیگر ز جزر و مد کشید</p></div>
<div class="m2"><p>گر چنین بر خود ببالد گوهر شهوار من</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر دل آزاده خود بار خود را بسته ام</p></div>
<div class="m2"><p>نیست دوش هیچ کس چو سرو زیر بار من</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون نفس در دل نگردد عندلیبان را گره؟</p></div>
<div class="m2"><p>غنچه می خسبد نسیم صبح در گلزار من</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از پشیمانی لب خود را به دندان می گزد</p></div>
<div class="m2"><p>هر که اندازد ز نادانی گره در کار من</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>روی در آیینه زانوی خود آورده ام</p></div>
<div class="m2"><p>نیست چون طوطی وبال دیگران زنگار من</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جلوه دست حمایت می کند ز آهستگی</p></div>
<div class="m2"><p>بر سر موران ره، پای سبکرفتار من</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>درد بر من صائب از درمان گواراتر شده است</p></div>
<div class="m2"><p>دست از دست مسیحا می کشد بیمار من</p></div></div>