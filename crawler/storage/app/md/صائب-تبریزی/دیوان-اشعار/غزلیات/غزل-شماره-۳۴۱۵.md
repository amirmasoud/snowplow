---
title: >-
    غزل شمارهٔ ۳۴۱۵
---
# غزل شمارهٔ ۳۴۱۵

<div class="b" id="bn1"><div class="m1"><p>چه خیال است به تیغش دل بیتاب رسد؟</p></div>
<div class="m2"><p>بیخبر بر سر این تشنه مگر آب رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم به بال و پر خورشید مگر شبنم ما</p></div>
<div class="m2"><p>به سراپرده خورشید جهانتاب رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رشته عمر ازان چاه ذقن کوتاه است</p></div>
<div class="m2"><p>به گسستن مگر این رشته به آن آب رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفس هر دو جهان سوخت درین غواصی</p></div>
<div class="m2"><p>تا که را دست به آن گوهر نایاب رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سبب کوش که بی ابر بهار از دریا</p></div>
<div class="m2"><p>نیست ممکن به لب خشک صدف آب رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آسمانش یکی از حلقه بگوشان باشد</p></div>
<div class="m2"><p>هر که را دست به آن زلف سیه تاب رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساقی از گردش آن چشم به فریادم رس</p></div>
<div class="m2"><p>که من آن صبر ندارم که می ناب رسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر چه از ثابت و سیار بهشتی است فلک</p></div>
<div class="m2"><p>حاش لله که به هنگامه احباب رسد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دامن تیغ ترا خون دو عالم نگرفت</p></div>
<div class="m2"><p>چه گرانی ز خس و خار به سیلاب رسد؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روزی هر کسی از راه نصیب آماده است</p></div>
<div class="m2"><p>قسمت گرگ محال است به قصاب رسد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هست تا مجلس می روشنی آنجا فرش است</p></div>
<div class="m2"><p>شب آدینه مگر شمع به محراب رسد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پیش کج بحث خمش باش که سرگردانی است</p></div>
<div class="m2"><p>آنچه از ماهی لب بسته به قلاب رسد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نیست جز زخم زبان قسمت سرگشته عشق</p></div>
<div class="m2"><p>خس و خاری مگر از بحر به گرداب رسد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که به ویرانه من پرتو مهتاب رسد</p></div>
<div class="m2"><p>صائب از کوتهی بخت ندارم امید</p></div></div>