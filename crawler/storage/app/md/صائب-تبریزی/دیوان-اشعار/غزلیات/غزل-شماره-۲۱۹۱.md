---
title: >-
    غزل شمارهٔ ۲۱۹۱
---
# غزل شمارهٔ ۲۱۹۱

<div class="b" id="bn1"><div class="m1"><p>در معرکه عشق ز جرأت خبری نیست</p></div>
<div class="m2"><p>غیر از سپر انداختن اینجا سپری نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در قافله فرد روان بار ندارم</p></div>
<div class="m2"><p>هر چند به جز سایه مرا همسفری نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در پله سنگ است گهر بی نظر پاک</p></div>
<div class="m2"><p>بیزارم ازان شهر که صاحب نظری نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خود را بشکن تا شکنی قلب جهان را</p></div>
<div class="m2"><p>این فتح میسر به شکست دگری نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون شیشه بی می، نبود قابل اقبال</p></div>
<div class="m2"><p>باغی که در او بلبل خونین جگری نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شب نیست که بر گرد تو تا روز نگردم</p></div>
<div class="m2"><p>هر چند من سوخته را بال و پری نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرگشتگی ما همه از عقل فضول است</p></div>
<div class="m2"><p>صحرا همه راه است اگر راهبری نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب چه کند گر نکند روی به دیوار؟</p></div>
<div class="m2"><p>جایی که لب خشکی و مژگان تری نیست</p></div></div>