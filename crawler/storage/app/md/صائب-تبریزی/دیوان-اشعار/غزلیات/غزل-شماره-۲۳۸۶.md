---
title: >-
    غزل شمارهٔ ۲۳۸۶
---
# غزل شمارهٔ ۲۳۸۶

<div class="b" id="bn1"><div class="m1"><p>دیده چون تاب صفای آن بناگوش آورد؟</p></div>
<div class="m2"><p>شبنمی چون خرمن گل را در آغوش آورد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گلستانی که شمشاد تو آید در خرام</p></div>
<div class="m2"><p>بهر سرو از طوق قمری حلقه گوش آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم ما بازیچه هر روی آتشناک نیست</p></div>
<div class="m2"><p>دیگ در یارا مگر خورشید در جوش آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>موج اگر گاهی به ساحل می کشاند خویش را</p></div>
<div class="m2"><p>می کشد میدان که دریا را در آغوش آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غنچه تصویر از مستی گریبان پاره کرد</p></div>
<div class="m2"><p>تا دل افسرده ما را که در جوش آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از گلاب صبح محشر هم نمی آید به هوش</p></div>
<div class="m2"><p>هر که در آغوش یک شب آن برو دوش آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب از ما ذوق ایام جوانی را مپرس</p></div>
<div class="m2"><p>کیست تا در خاطر آن خواب فراموش آورد</p></div></div>