---
title: >-
    غزل شمارهٔ ۴۲۳
---
# غزل شمارهٔ ۴۲۳

<div class="b" id="bn1"><div class="m1"><p>نبردم زیر خاک از عجز با خود دعوی خون را</p></div>
<div class="m2"><p>به دست زخم دندان دادم آن لبهای میگون را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز چشم شوخ لیلی آهوان دارند فرمانی</p></div>
<div class="m2"><p>که هر جا می رود، از چشم نگذارند مجنون را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رمیدن جست ازخاطر غزالان را ز بی جایی</p></div>
<div class="m2"><p>شکوه عشق مجنون تنگ کرد از بس که هامون را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نکرد از دیده پنهان باده گلرنگ را مینا</p></div>
<div class="m2"><p>نقاب از دیده چون پنهان کند آن روی گلگون را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مزن زنهار در کوی مغان لاف زبردستی</p></div>
<div class="m2"><p>که زور می حصاری می کند در خم فلاطون را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نگردد ترک جست و جو حجاب روزی قانع</p></div>
<div class="m2"><p>گره در بال گردد دانه این مرغ همایون را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز زندان نیست پروا عشق را، معشوق اگر باشد</p></div>
<div class="m2"><p>به بوی گنج در خاک است استقرار، قارون را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به خاکش نور بارد تا به دامان جزا صائب</p></div>
<div class="m2"><p>کسی کآرد به خاک کشتگان آن جامه گلگون را</p></div></div>