---
title: >-
    غزل شمارهٔ ۲۰۳۵
---
# غزل شمارهٔ ۲۰۳۵

<div class="b" id="bn1"><div class="m1"><p>مجروح عشق را سر و برگ علاج نیست</p></div>
<div class="m2"><p>این خون گرفته را به طبیب احتیاج نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برق از زمین سوخته نومید می رود</p></div>
<div class="m2"><p>تاراج دیده را غم باج و خراج نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در وادیی که قطع امیدست چاره ساز</p></div>
<div class="m2"><p>دردی که بی دوا نشد آن را علاج نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مجنون چه خون که در دل لیلی نمی کند</p></div>
<div class="m2"><p>از خود رمیده را به وصال احتیاج نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راضی نمی شوند به گنج از دل خراب</p></div>
<div class="m2"><p>در ملک عشق برده معمور باج نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر تخت دار، شوکت منصور را ببین</p></div>
<div class="m2"><p>کیفیت بلند کم از هیچ تاج نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این آن غزل که اهلی شیراز گفته است</p></div>
<div class="m2"><p>آن را که عقل نیست به هیچ احتیاج نیست</p></div></div>