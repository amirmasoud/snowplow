---
title: >-
    غزل شمارهٔ ۱۷۰۹
---
# غزل شمارهٔ ۱۷۰۹

<div class="b" id="bn1"><div class="m1"><p>همیشه دیده سوزن ازان به دنبال است</p></div>
<div class="m2"><p>که قبله نظرش رشته های آمال است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خرمن دگران هر که می پرد چشمش</p></div>
<div class="m2"><p>هزار رخنه فزون در دلش چو غربال است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غبار کوچه عشق است کیمیای مراد</p></div>
<div class="m2"><p>خوشا سری که درین رهگذار پامال است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به ظلمتی که ز دوران رسد گرفته مباش</p></div>
<div class="m2"><p>که خنده شب ادبار، صبح اقبال است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز طعن بیخردان اهل دل نیندیشند</p></div>
<div class="m2"><p>که نقل مجلس دیوانه سنگ اطفال است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل و زبان چو یکی شد، سخن بلند شود</p></div>
<div class="m2"><p>به هیچ جا نرسد طایری که یک بال است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هوای عالم آزادگی است بر یک حال</p></div>
<div class="m2"><p>ز برگریز خزان سرو فارغ البال است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر به چشم بصیرت نظر کنی صائب</p></div>
<div class="m2"><p>چه نیشها که نهان در پرند اقبال است</p></div></div>