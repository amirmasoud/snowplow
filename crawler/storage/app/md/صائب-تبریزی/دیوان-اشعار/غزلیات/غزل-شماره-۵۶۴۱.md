---
title: >-
    غزل شمارهٔ ۵۶۴۱
---
# غزل شمارهٔ ۵۶۴۱

<div class="b" id="bn1"><div class="m1"><p>بس که چون برگ خزان دیده پریشان حالم</p></div>
<div class="m2"><p>سایه خود را به زمین می کشد از دنبالم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جگر پاره ولی نعمت سی روز من است</p></div>
<div class="m2"><p>نکند دغدغه رزق پریشان حالم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کیست جز آینه و آب درین قحط آباد</p></div>
<div class="m2"><p>که کند گریه به روز سفر از دنبالم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که را درد دلی هست به من شرح دهد</p></div>
<div class="m2"><p>هر که را بار گرانی است منش حمالم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گه به خاکم کشد و گاه به خون غلطاند</p></div>
<div class="m2"><p>چون پر تیره و بال تن من شد بالم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گریه سنگدل از بس که فشرده است مرا</p></div>
<div class="m2"><p>خار در دیده آیینه زند تمثالم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باده صاف بود آینه طوطی من</p></div>
<div class="m2"><p>در حریمی که لب جام نباشد لالم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آب در دیده آتش ز ترحم گردد</p></div>
<div class="m2"><p>صائب آن شمع اگر شعله زند در بالم</p></div></div>