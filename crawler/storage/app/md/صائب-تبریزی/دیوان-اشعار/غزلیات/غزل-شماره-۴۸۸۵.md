---
title: >-
    غزل شمارهٔ ۴۸۸۵
---
# غزل شمارهٔ ۴۸۸۵

<div class="b" id="bn1"><div class="m1"><p>چون بود گلچهره ساقی باده رنگین گو مباش</p></div>
<div class="m2"><p>ساعد سیمین چو باشد جام سیمین گو مباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست رنگین می کند کار شراب لعل فام</p></div>
<div class="m2"><p>باده گلرنگ در دست نگارین گو مباش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر سفالی رافروغ می بلورین می کند</p></div>
<div class="m2"><p>باده چون روشن بود جام بلورین گو مباش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داغ ما خون رابه همت می تواند مشک کرد</p></div>
<div class="m2"><p>کاکل عنبرفشان و زلف مشکین گو مباش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صحبت دلدار، ما را فارغ از دل کرده است</p></div>
<div class="m2"><p>چون سوارازماست هرگز خانه زین گو مباش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خامه صائب معطر می کند آفاق را</p></div>
<div class="m2"><p>در بیابان ختا آهوی مشکین گو مباش</p></div></div>