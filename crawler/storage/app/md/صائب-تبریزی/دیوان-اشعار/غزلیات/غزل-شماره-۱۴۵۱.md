---
title: >-
    غزل شمارهٔ ۱۴۵۱
---
# غزل شمارهٔ ۱۴۵۱

<div class="b" id="bn1"><div class="m1"><p>دوری راه طلب بر دل کاهل بارست</p></div>
<div class="m2"><p>بر دل گرمروان، دیدن منزل بارست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیش ازین بردل دریا نتوان بار نهاد</p></div>
<div class="m2"><p>ورنه بر کشتی ما لنگر ساحل بارست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غم آواره صحرای طلب منظورست</p></div>
<div class="m2"><p>ور نه گلبانگ جرس بر دل محمل بارست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همت آن است که در پرده شب جود کنند</p></div>
<div class="m2"><p>سایه دست کرم بر سر سایل بارست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غنچه خسبان سراپرده دلتنگی را</p></div>
<div class="m2"><p>گر همه برگ حیات است، که بر دل بارست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در مقامی که سر زلف سخن شانه زنند</p></div>
<div class="m2"><p>باد اگر باد بهشت است، که بر دل بارست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب آنجا که کند حسن و محبت خلوت</p></div>
<div class="m2"><p>پرتو شمع سبکروح به محفل بارست</p></div></div>