---
title: >-
    غزل شمارهٔ ۳۱۵۶
---
# غزل شمارهٔ ۳۱۵۶

<div class="b" id="bn1"><div class="m1"><p>چه شد گر خصم بداختر بهای من نمی داند؟</p></div>
<div class="m2"><p>کمال عیسوی را دیده سوزن نمی داند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگو واعظ حدیث دوزخ و جنت به اهل دل</p></div>
<div class="m2"><p>که سرگرم محبت گلشن از گلخن نمی داند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو بی پروا زبان خلق را کوتاه کن از خود</p></div>
<div class="m2"><p>وگرنه آه مظلومان ره روزن نمی داند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زکافر نعمتی دل شکوه از داغ و جنون دارد</p></div>
<div class="m2"><p>که بلبل قدر گل تا هست در گلشن نمی داند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل بیدار را خواب اجل بیدارتر سازد</p></div>
<div class="m2"><p>چراغ ما زدامان کفن مردن نمی داند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشو از قتل ما ایمن که چون فرهاد خون ما</p></div>
<div class="m2"><p>نخواباند به خون تا خصم را، خفتن نمی داند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرآدم گشته ام چون سرمه در علم نظر بازی</p></div>
<div class="m2"><p>زبان چشم خوبان را کسی چون من نمی داند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>توان کردن به ابرام از نکویان کام دل حاصل</p></div>
<div class="m2"><p>دم این تیغ بی زنهار، برگشتن نمی داند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نداری رحم اگر بر غیر، برخود رحم کن صائب</p></div>
<div class="m2"><p>که آتش گرم چون شد دوست از دشمن نمی داند</p></div></div>