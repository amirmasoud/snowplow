---
title: >-
    غزل شمارهٔ ۶۵۴۱
---
# غزل شمارهٔ ۶۵۴۱

<div class="b" id="bn1"><div class="m1"><p>در خون نشست لاله ز چشم سیاه تو</p></div>
<div class="m2"><p>گل گوشه گیر گشت ز طرف کلاه تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز به زیر پای نمی بینی از غرور</p></div>
<div class="m2"><p>بیچاره عاشقی که شود خاک راه تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلف این چنین ز دست تو گر می کشد عنان</p></div>
<div class="m2"><p>خواهد گرفت روی زمین را سپاه تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم غزال، داغ سیاهی فکنده ای است</p></div>
<div class="m2"><p>در معرض سیاهی چشم سیاه تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آگاه نیستی که چه دلها شکسته است</p></div>
<div class="m2"><p>مشاطه در شکستن طرف کلاه تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چند دست و پای زند بسته تر شود</p></div>
<div class="m2"><p>هر دل که شد مقید زلف سیاه تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از هاله زود حلقه کند نام ماه را</p></div>
<div class="m2"><p>خطی که گشت گرد رخ همچو ماه تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از بیم چشم زخم، ز مژگان آبدار</p></div>
<div class="m2"><p>صد تیغ کرده است حمایل نگاه تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صد پیرهن عرق کند از شرم، بوی گل</p></div>
<div class="m2"><p>از برگ گل کنند اگر خوابگاه تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از بس که در ربودن دل تیزچنگ بود</p></div>
<div class="m2"><p>شد تیر روی ترکش مژگان نگاه تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نقش دگر در او نتواند گرفت جای</p></div>
<div class="m2"><p>آیینه دلی که شود جلوه گاه تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در خون آهوان حرم کاسه می زنی</p></div>
<div class="m2"><p>صائب چگونه امن شود در پناه تو؟</p></div></div>