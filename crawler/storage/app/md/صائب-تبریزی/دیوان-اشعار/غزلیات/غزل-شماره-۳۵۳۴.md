---
title: >-
    غزل شمارهٔ ۳۵۳۴
---
# غزل شمارهٔ ۳۵۳۴

<div class="b" id="bn1"><div class="m1"><p>دل نازک به زبان بازی مژگان چه کند؟</p></div>
<div class="m2"><p>سپر آبله با خار مغیلان چه کند؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیش ازین با من سودازده دوران چه کند؟</p></div>
<div class="m2"><p>شومی جغد به این خانه ویران چه کند؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سبکی کشتی نوح است گرانباران را</p></div>
<div class="m2"><p>کف دریا حذر از موجه طوفان چه کند؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده شوخ درین باغ ز شبنم بیش است</p></div>
<div class="m2"><p>در دل خود نخزد غنچه خندان چه کند؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پنجه شوخی خورشید بلند افتاده است</p></div>
<div class="m2"><p>نکند پاره گل صبح گریبان چه کند؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست پرورد بلا را ز بلا باکی نیست</p></div>
<div class="m2"><p>رعشه بحر به سرپنجه مرجان چه کند؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل سودایی من نیست سزاوار وصال</p></div>
<div class="m2"><p>دانه سوخته احسان بهاران چه کند؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاکساران ز حوادث خط پاکی دارند</p></div>
<div class="m2"><p>شومی جغد به این خانه ویران چه کند؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نتوان دید ز بیداد خجل دشمن را</p></div>
<div class="m2"><p>ورنه سیلاب به ما خانه بدوشان چه کند؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شبنم از دیدن گل سیر نشد با صد چشم</p></div>
<div class="m2"><p>با گل روی تو یک دیده حیران چه کند؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نکند خنده سوفار به پیکان تأثیر</p></div>
<div class="m2"><p>با دل غمزده صائب لب خندان چه کند؟</p></div></div>