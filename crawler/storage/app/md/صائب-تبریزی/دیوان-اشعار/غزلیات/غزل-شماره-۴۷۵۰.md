---
title: >-
    غزل شمارهٔ ۴۷۵۰
---
# غزل شمارهٔ ۴۷۵۰

<div class="b" id="bn1"><div class="m1"><p>ای هر ورق گل زتو آیینه دیگر</p></div>
<div class="m2"><p>هرغنچه زاسرارتوگنجینه دیگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی باده گلرنگ ،بود در نظرمن</p></div>
<div class="m2"><p>هر ابرسیاهی شب آدینه دیگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از سینه من گرچه اثرداغ تونگذاشت</p></div>
<div class="m2"><p>می بودمراکاش دو صد سینه دیگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درآرزوی داغ چوخورشید تو هرروز</p></div>
<div class="m2"><p>از صدق دهد صبح صفا سینه دیگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برخرقه صد پاره ارباب توکل</p></div>
<div class="m2"><p>جز رقعه حاجت نبود پینه دیگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خورشید نمی سوخت نفس درطلب صبح</p></div>
<div class="m2"><p>می بود گر سینه بی کینه دیگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیناست درین بحرحبابی که ندارد</p></div>
<div class="m2"><p>غیراز سر زانوی خودآیینه دیگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن دلبربیباک چه میکردبه عاشق</p></div>
<div class="m2"><p>می داشت اگر غیردل آیینه دیگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درپاره دل گم شود صائب گهرمن</p></div>
<div class="m2"><p>چون حلقه زنم بردرگنجینه دیگر؟</p></div></div>