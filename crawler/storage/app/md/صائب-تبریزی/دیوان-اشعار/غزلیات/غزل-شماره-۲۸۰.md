---
title: >-
    غزل شمارهٔ ۲۸۰
---
# غزل شمارهٔ ۲۸۰

<div class="b" id="bn1"><div class="m1"><p>چشم مست یار شد مخمور و مدهوشیم ما</p></div>
<div class="m2"><p>باده از جوش نشاط افتاد و در جوشیم ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناله ما حلقه در گوش اجابت می کشد</p></div>
<div class="m2"><p>کز سحرخیزان آن صبح بناگوشیم ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قطره اشکیم با آوارگی هم کاروان</p></div>
<div class="m2"><p>در کنار چشم از خاطر فراموشیم ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فتنه صد انجمن، آشوب صد هنگامه ایم</p></div>
<div class="m2"><p>گر به ظاهر چون شراب کهنه خاموشیم ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی تأمل چون عرق بر روی خوبان می دویم</p></div>
<div class="m2"><p>چون کمند زلف، گستاخ برو دوشیم ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیکر ما می کند شمشیر را دندانه دار</p></div>
<div class="m2"><p>در لباس از جوهر ذاتی زره پوشیم ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کار روغن می کند بر آتش ما آب تیغ</p></div>
<div class="m2"><p>خون منصوریم، دایم بر سر جوشیم ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خرقه درویشی ما چون زره زیر قباست</p></div>
<div class="m2"><p>پیش چشم خلق ظاهربین قباپوشیم ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نامه پیچیده را چون آب خواندن حق ماست</p></div>
<div class="m2"><p>کز سخن فهمان آن لبهای خاموشیم ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از شراب ما رگ خامی است صائب موج زن</p></div>
<div class="m2"><p>گر چه عمری شد درین میخانه در جوشیم ما</p></div></div>