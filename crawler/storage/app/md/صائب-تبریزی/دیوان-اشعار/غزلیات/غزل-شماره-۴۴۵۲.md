---
title: >-
    غزل شمارهٔ ۴۴۵۲
---
# غزل شمارهٔ ۴۴۵۲

<div class="b" id="bn1"><div class="m1"><p>طوفان گل و جوش بهارست ببینید</p></div>
<div class="m2"><p>اکنون که جهان بر سر کارست ببینید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سبزه و گل آب روان پرده نشین است</p></div>
<div class="m2"><p>ماهی که درین سبزحصارست ببینید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قانع مشوید از خط استاد به خواندن</p></div>
<div class="m2"><p>حسنی که نهان در خط یارست ببینید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن گرد که بر عرش کله گوشه شکسته است</p></div>
<div class="m2"><p>از جلوه آن شاهسوارست ببینید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این آینه هایی که نظر خیره نماید</p></div>
<div class="m2"><p>در دست کدام آینه دارست ببینید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان آتش پنهان که جهان سوخته اوست</p></div>
<div class="m2"><p>افلاک پر از دود و شرارست ببینید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در مغز بهاراین چه نسیم است ببویید</p></div>
<div class="m2"><p>در دست جهان این چه نگارست ببینید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون نیست شما را نظر دیدن آتش</p></div>
<div class="m2"><p>این جوش که در مغز بهارست ببینید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مژگان بگشایید و ببندید زبان را</p></div>
<div class="m2"><p>آفاق پراز جلوه یارست ببینید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در پله اعداد اقامت منمایید</p></div>
<div class="m2"><p>آن حسن که بیرون ز شمارست ببینید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از شوق هم آغوشی آن قامت موزون</p></div>
<div class="m2"><p>گلها همه آغوش وکنارست ببینید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از دیدن صیاد اگر رنگ ندارید</p></div>
<div class="m2"><p>این دشت که پرخون شکارست ببینید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در دامن دشتی که ز جوش گل بی خار</p></div>
<div class="m2"><p>خورشید کم از بوته خارست ببینید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن نوش که در نیش نهان است بجویید</p></div>
<div class="m2"><p>آن گنج که در کسوت مارست ببینید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زان پیش که از چهره جان گردفشانید</p></div>
<div class="m2"><p>آن ماه که در زیر غبارست ببینید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون بال فلک سیر ز اندیشه ندارید</p></div>
<div class="m2"><p>آن را که در اندیشه یارست ببینید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زان پیش که هردوجهان گردبرآرد</p></div>
<div class="m2"><p>ای بیخبران این چه سوارست ببینید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در جامه خودچاک زدن بی سببی نیست</p></div>
<div class="m2"><p>در پیرهن غنچه چه خارست ببینید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از چشمه کوثر نرود تیرگی بخت</p></div>
<div class="m2"><p>خالی که به کنج لب یارست ببینید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>این آن غزل اوحدی ماست که فرمود</p></div>
<div class="m2"><p>ای بی بصران این چه بهارست ببینید</p></div></div>