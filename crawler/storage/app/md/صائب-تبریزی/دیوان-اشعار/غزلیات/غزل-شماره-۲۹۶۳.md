---
title: >-
    غزل شمارهٔ ۲۹۶۳
---
# غزل شمارهٔ ۲۹۶۳

<div class="b" id="bn1"><div class="m1"><p>دل رم کرده ناخوش آستین افشاندنی دارد</p></div>
<div class="m2"><p>نسیم سرد مهری بدورق گراندنی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گل یکباره نتوان زد در امیدواری را</p></div>
<div class="m2"><p>اگر ما را نخوانی، نامه ما خواندنی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زبس دنبال دل رفتم به حال مرگ افتادم</p></div>
<div class="m2"><p>دویدنهای بی تدبیر ناخوش ماندنی دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مکن عیبم اگر در عشق بر یک حال کم باشم</p></div>
<div class="m2"><p>کباب نازک دل هر نفس گراندنی دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به حسن بی زوال خویش چون خورشید می نازی</p></div>
<div class="m2"><p>نمی دانی که آه ما چه دست افشاندنی دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به قرب ساحل از طوفان آه من مشو غافل</p></div>
<div class="m2"><p>که این باد مخالف بدعنان پیچاندنی دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکایت گرچه بر هم می زند اوراق خاطر را</p></div>
<div class="m2"><p>پریشان نامه افکار صائب خواندنی دارد</p></div></div>