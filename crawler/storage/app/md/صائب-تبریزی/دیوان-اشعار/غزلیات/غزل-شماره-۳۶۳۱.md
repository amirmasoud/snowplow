---
title: >-
    غزل شمارهٔ ۳۶۳۱
---
# غزل شمارهٔ ۳۶۳۱

<div class="b" id="bn1"><div class="m1"><p>ناله ای کز دل بیدرد برون می آید</p></div>
<div class="m2"><p>تیغی از پنجه نامرد برون می آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم دنیا نه حریفی است که مغلوب شود</p></div>
<div class="m2"><p>مرد ازین معرکه نامرد برون می آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رنگ در آب و گلم گریه خونین نگذاشت</p></div>
<div class="m2"><p>لاله از تربت من زرد برون می آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون گهر گرچه جگرگوشه این دریایم</p></div>
<div class="m2"><p>از یتیمی ز دلم گرد برون می آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق را از نظر گرم کند حسن ایجاد</p></div>
<div class="m2"><p>ذره با مهر جهانگرد برون می آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر فتد ره به خرابات مغان قارون را</p></div>
<div class="m2"><p>به دو پیمانه جوانمرد برون می آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ماه در زیر سپر می شود از هاله نهان</p></div>
<div class="m2"><p>هر شبی کان مه شبگرد برون می آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرچه از زیر و زبر کردن غمخانه ما</p></div>
<div class="m2"><p>سالها رفت، همان گرد برون می آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سببش تنگی خانه است نه بیدردیها</p></div>
<div class="m2"><p>از دل صائب اگر درد برون می آید</p></div></div>