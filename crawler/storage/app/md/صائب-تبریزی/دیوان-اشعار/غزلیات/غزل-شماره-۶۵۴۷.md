---
title: >-
    غزل شمارهٔ ۶۵۴۷
---
# غزل شمارهٔ ۶۵۴۷

<div class="b" id="bn1"><div class="m1"><p>یک صافدل در انجمن روزگار کو؟</p></div>
<div class="m2"><p>عالم گرفت تیرگی آیینه دار کو؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر جا که هست صاف ضمیری شکسته است</p></div>
<div class="m2"><p>آیینه درست درین زنگبار کو؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون ریگ، تشنه اند حریفان به خون هم</p></div>
<div class="m2"><p>در قلزم فلک گهر آبدار کو؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خونین دلی چو نافه درین دشت پرشکار</p></div>
<div class="m2"><p>کاآفاق را کند به نفس مشکبار کو؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا تیغ کهکشان بدر آرد ز دست چرخ</p></div>
<div class="m2"><p>یک مرد سرگذشته درین روزگار کو؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی خون دل ز چرخ فراغت طمع مدار</p></div>
<div class="m2"><p>بر خوان سفله نعمت بی انتظار کو؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پروانه تا به شمع رسید آرمیده شد</p></div>
<div class="m2"><p>دریای بی قراری ما را کنار کو؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای آن که دم ز رهروی عشق می زنی</p></div>
<div class="m2"><p>در پرده نظر، اثر زخم خار کو؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون شمع اگر ترا به جگر هست آتشی</p></div>
<div class="m2"><p>رنگ شکسته و مژه اشکبار کو؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا صبر هست درد به درمان نمی رسد</p></div>
<div class="m2"><p>دردی که از شکیب برآرد دمار کو؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تب لرزه آفتاب جهان را گرفته است</p></div>
<div class="m2"><p>هنگامه گرم ساز درین روزگار کو؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در آتش است نعل سفر کوه طور را</p></div>
<div class="m2"><p>در زیر بار عشق تن بردبار کو؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون شمع زیر دامن صحرای روزگار</p></div>
<div class="m2"><p>مانند لاله یک جگر داغدار کو؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ناصح عبث ز ریگ روان سبحه می زند</p></div>
<div class="m2"><p>داغ درون سوختگان را شمار کو؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دولت بود به پای تو مردن به اختیار</p></div>
<div class="m2"><p>اما نیازمند ترا اختیار کو؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>این آن غزل که حضرت عطار گفته است</p></div>
<div class="m2"><p>از آتش سماع دلی بی قرار کو؟</p></div></div>