---
title: >-
    غزل شمارهٔ ۴۳۵۱
---
# غزل شمارهٔ ۴۳۵۱

<div class="b" id="bn1"><div class="m1"><p>دل راه در آن زلف گرهگیر ندارد</p></div>
<div class="m2"><p>دیوانه ما طالع زنجیر ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مژگان بلند تو رساتر ز نگاه است</p></div>
<div class="m2"><p>حاجت به پر عاریه این تیر ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دیده آن کس که به معنی نبرد راه</p></div>
<div class="m2"><p>زندان بود آن خانه که تصویر ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیری نه شکستی است که اصلاح توان کرد</p></div>
<div class="m2"><p>بر در زدن ازان خانه که تعمیر ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از هرزه درایی اثر از بانگ جرس خاست</p></div>
<div class="m2"><p>بسیار چو شد زمزمه تأثیر ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پرشور شد آفاق ز بانگ قلم من</p></div>
<div class="m2"><p>فریاد نیستان مرا شیر ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در سینه هر کس که نباشد الف آه</p></div>
<div class="m2"><p>صائب چو نیامی است که شمشیر ندارد</p></div></div>