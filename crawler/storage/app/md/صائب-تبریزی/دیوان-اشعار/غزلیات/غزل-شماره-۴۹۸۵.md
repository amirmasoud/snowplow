---
title: >-
    غزل شمارهٔ ۴۹۸۵
---
# غزل شمارهٔ ۴۹۸۵

<div class="b" id="bn1"><div class="m1"><p>لب خمیازه ما شد ز می ناب خموش</p></div>
<div class="m2"><p>که صدف می شود ازگوهر سیراب خموش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بحر از پنجه مرجان نپذیرد آرام</p></div>
<div class="m2"><p>نشد از دست نوازش دل بیتاب خموش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گریه برآتش بیتابی ماآب نزد</p></div>
<div class="m2"><p>که ز شبنم نشود مهر جهانتاب خموش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست در صحبت اشراق زبانی درکار</p></div>
<div class="m2"><p>شمع آن به که بود در شب مهتاب خموش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه عجب درگل اگر دیده ماحیران شد</p></div>
<div class="m2"><p>که چو آیینه درین باغ شود آب خموش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست بی داغ ملامت جگر چاک مرا</p></div>
<div class="m2"><p>نشود شمع درین گوشه محراب خموش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شمع در پرده فانوس نیفتد ز زبان</p></div>
<div class="m2"><p>نشود چشم سخنگوی تو در خواب خموش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه کند مهر خموشی به لب شکوه ما؟</p></div>
<div class="m2"><p>نشود سیل گرانسنگ به گرداب خموش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گریه و ناله بود لازم سرگردانی</p></div>
<div class="m2"><p>نیست ممکن شود از زمزمه دولاب خموش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شد غبار خط او باعث تسکین دل را</p></div>
<div class="m2"><p>چاره خاک است چو آتش نشدازآب خموش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روز روشن شب تاریک شود درنظرش</p></div>
<div class="m2"><p>هرکه را گشت چراغ دل بیتاب خموش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شور من بیش شد ازسنگ ملامت صائب</p></div>
<div class="m2"><p>چه خیال است به کهسار شود آب خموش</p></div></div>