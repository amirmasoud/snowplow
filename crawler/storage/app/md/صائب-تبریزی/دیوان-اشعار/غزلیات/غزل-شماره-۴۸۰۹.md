---
title: >-
    غزل شمارهٔ ۴۸۰۹
---
# غزل شمارهٔ ۴۸۰۹

<div class="b" id="bn1"><div class="m1"><p>با عشق او ز هر دو جهانیم پاکباز</p></div>
<div class="m2"><p>ما از دو خانه همچو کمانیم پاکباز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خاک منت گل بی خار می کشیم</p></div>
<div class="m2"><p>با آن که همچو آب روانیم پاکباز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از زخم خار شکوه نفهمیده ایم چیست</p></div>
<div class="m2"><p>چون ماهیان ز تیغ زبانیم پاکباز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آهی است سرد در جگر آتشین ما</p></div>
<div class="m2"><p>از برگ عیش همچو خزانیم پاکباز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آیینه ایم لیک ز حیرت درین بساط</p></div>
<div class="m2"><p>از نقش دلفریب جهانیم پاکباز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون سینه گشاده مستان ساده لوح</p></div>
<div class="m2"><p>از خرده های راز نهانیم پاکباز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زان همچو شبنمیم درین بوستان عزیز</p></div>
<div class="m2"><p>کز رنگ و بوی باغ جهانیم پاکباز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان بی نشان به نام قناعت نموده ایم</p></div>
<div class="m2"><p>هرچند ما ز نام ونشانیم پاکباز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب اگر چه هیچ نداریم در بساط</p></div>
<div class="m2"><p>از چشم و خاطر نگرانیم پاکباز</p></div></div>