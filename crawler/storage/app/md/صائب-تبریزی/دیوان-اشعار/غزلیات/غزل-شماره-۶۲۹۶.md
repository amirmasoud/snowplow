---
title: >-
    غزل شمارهٔ ۶۲۹۶
---
# غزل شمارهٔ ۶۲۹۶

<div class="b" id="bn1"><div class="m1"><p>خار غم از دل عشاق کم آید بیرون</p></div>
<div class="m2"><p>چون ازین شعه ستان خار غم آید بیرون؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوهر از تیغ برد سینه گرمی که مراست</p></div>
<div class="m2"><p>ماهی از قلزم ما بی درم آید بیرون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صدق در سینه هر کس که چراغ افروزد</p></div>
<div class="m2"><p>از دهانش نفس صبحدم آید بیرون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر سیه بختی ارباب سخن می گرید</p></div>
<div class="m2"><p>ناله ای کز دل چاک قلم آید بیرون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زنده شد عالمی از خنده جان پرور او</p></div>
<div class="m2"><p>که گمان داشت وجود از عدم آید بیرون؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی اگر در حرم کعبه کند غمزه او</p></div>
<div class="m2"><p>صید با تیغ و کفن از حرم آید بیرون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سینه چاک، ره قافله غم بوده است</p></div>
<div class="m2"><p>دل ما خوش که ازین رخنه غم آید بیرون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در کنعان نگشایند به رویش اخوان</p></div>
<div class="m2"><p>یوسف از مصر اگر بی درم آید بیرون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حرص دایم چو سگ هرزه مرس در سفرست</p></div>
<div class="m2"><p>صبر شیری است که از بیشه کم آید بیرون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باددستان گره از کیسه کان نگشایند</p></div>
<div class="m2"><p>زر چو گل از کف اهل کرم آید بیرون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب آن شوخ به خوبی شود انگشت نما</p></div>
<div class="m2"><p>چون مه نو اگر از خانه کم آید بیرون</p></div></div>