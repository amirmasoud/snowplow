---
title: >-
    غزل شمارهٔ ۵۵۵۳
---
# غزل شمارهٔ ۵۵۵۳

<div class="b" id="bn1"><div class="m1"><p>خوشا روزی که منزل در سواد اصفهان سازم</p></div>
<div class="m2"><p>ز وصف زنده رودش خامه را رطب اللسان سازم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نسیم آسا به گرد سر بگردم چار باغش را</p></div>
<div class="m2"><p>به هر شاخی که بنشیند دل من آشیان سازم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شود روشن دو چشمم از سواد سرمه خیز او</p></div>
<div class="m2"><p>ز مژگان زنده رود گریه شادی روان سازم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز شکر بشکنم خسرو صفت بازار شیرین را</p></div>
<div class="m2"><p>به ملک اصفهان شبدیز را آتش عنان سازم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صلای آب حیوان می زند تیغ جوانمردش</p></div>
<div class="m2"><p>چرا چون خضر کم همت به عمر جاودان سازم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به مژگان خواب مخمل می دهد جا جسم زارم را</p></div>
<div class="m2"><p>چرا آرامگاه خویش از تیغ و سنان سازم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به این گرمی که من رو از غریبی در وطن دارم</p></div>
<div class="m2"><p>اگر بر سنگ بگذارم قدم ریگ روان سازم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میان آب و آتش طرح صلح انداختم اما</p></div>
<div class="m2"><p>نمی دانم ترا بر خویشتن چون مهربان سازم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بلند افتاده صائب آنقدر طبع خدادادم</p></div>
<div class="m2"><p>که شمع طور را خاموش از تیغ زبان سازم</p></div></div>