---
title: >-
    غزل شمارهٔ ۵۹۴۰
---
# غزل شمارهٔ ۵۹۴۰

<div class="b" id="bn1"><div class="m1"><p>برون نمی برد از فکر دوست عالم آبم</p></div>
<div class="m2"><p>نقاب دولت بیدار نیست پرده خوابم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جگر گداز محیط است داغ تشنگی من</p></div>
<div class="m2"><p>کلاه گوشه به دریا شکسته موج سرابم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خوان چرخ نکردم دراز دست تهی را</p></div>
<div class="m2"><p>نداشت کاسه در یوزه پیش بحر حبابم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر چه روی مرا داشت روزگار بر آتش</p></div>
<div class="m2"><p>چه خون که در دل آتش نکرد اشک کبابم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیم چو آینه مه رهین پرتو منت</p></div>
<div class="m2"><p>چو مهر باهمه آفاق روشن است حسابم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو ماه عید نشد راست قامتم ز تواضع</p></div>
<div class="m2"><p>همان سپهر دهد خاکمال همچو رکابم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بی تهی نرسیدم به غور بحر حقیقت</p></div>
<div class="m2"><p>به فکر پوچ بسر رفت روزگار حبابم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز خشک مغزی میناچه خون که در جگرم نیست</p></div>
<div class="m2"><p>خوش آن زمان که به مینای غنچه بود گلابم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خم سپهر برین می کند تلاش شکستن</p></div>
<div class="m2"><p>مگر به خانه زور آمده است باده نابم؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همان ز طعن خطا نیستم خلاص چو صائب</p></div>
<div class="m2"><p>گرفت روی زمین را اگر چه فکر صوابم</p></div></div>