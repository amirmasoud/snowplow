---
title: >-
    غزل شمارهٔ ۴۲۵۷
---
# غزل شمارهٔ ۴۲۵۷

<div class="b" id="bn1"><div class="m1"><p>در گلشنی که بند قبای تو وا شود</p></div>
<div class="m2"><p>چندین هزار پیرهن گل قبا شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ریزند اگر به دیده من بیغمان نمک</p></div>
<div class="m2"><p>در چشم قدردانی من توتیا شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بخت سیه نبرد روانی ز طبع من</p></div>
<div class="m2"><p>از سنگ سرمه آب کجا بی صدا شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می بایدش به تیغ سر خود به طرح داد</p></div>
<div class="m2"><p>هر کس که چون قلم به سخن آشنا شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طغیان نفس بیش شود در توانگری</p></div>
<div class="m2"><p>این مار چون به گنج رسد اژدها شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>احسان چرخ سفله نباشد به جای خویش</p></div>
<div class="m2"><p>نعمت نصیب مردم بی اشتها شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گردد به چار موجه کثرت کجا حریف</p></div>
<div class="m2"><p>آیینه ای کز آب گهر بی صفا شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیوانگی به سنگ ملامت شود تمام</p></div>
<div class="m2"><p>خوش وقت دانه ای که به این آسیا شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب گزیده می شود از میوه بهشت</p></div>
<div class="m2"><p>دستی که با ترنج ذقن آشنا شود</p></div></div>