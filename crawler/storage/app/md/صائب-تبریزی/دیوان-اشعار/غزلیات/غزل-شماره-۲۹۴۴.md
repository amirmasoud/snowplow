---
title: >-
    غزل شمارهٔ ۲۹۴۴
---
# غزل شمارهٔ ۲۹۴۴

<div class="b" id="bn1"><div class="m1"><p>مرا پاس ادب زان آستان مهجور می دارد</p></div>
<div class="m2"><p>ترا تمکین و ناز از صحبت من دور می دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نباشد حسن را مشاطه ای چون پاکدامانی</p></div>
<div class="m2"><p>به قدر شرم، رخسار نکویان نور می دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لب میگون و چشم مست او را هر که می بیند</p></div>
<div class="m2"><p>مرا در مستی و دیوانگی معذور می دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگردید از ملاحت نشأه آن لعل میگون کم</p></div>
<div class="m2"><p>چه پروا از نمک آن باده پرزور می دارد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمی بینم از ان دزدیده در رخساره جانان</p></div>
<div class="m2"><p>که دیدنهای رسوا عشق را مستور می دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا بیهوشی از پاس ادب غافل نمی سازد</p></div>
<div class="m2"><p>نمی دانم چرا ساقی مرا مخمور می دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جرأت چون طبیب بیجگر نبض مرا گیرد؟</p></div>
<div class="m2"><p>سمندر دست بر آتش مرا از دور می دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگرچه شوربخت افتاده ام اما به این شادم</p></div>
<div class="m2"><p>که باشد ایمن از چشم آن که بخت شور می دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ملایم طینتی هموار سازد تندخویان را</p></div>
<div class="m2"><p>کدو اندیشه کی از باده پرزور می دارد؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به ریزش می توان از گوهر مقصود بر خوردن</p></div>
<div class="m2"><p>به قدر قطره های اشک، تاک انگور می دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به شیرینی توان بستن زبان تندخویان را</p></div>
<div class="m2"><p>که شهد از آتش ایمن خانه زنبور می دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مشو غمگین، زگردون بر نیاید گر تمنایت</p></div>
<div class="m2"><p>که بی بال و پری پاس حیات مور می دارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زبیدردان چه درد از دل شود کم دردمندان را؟</p></div>
<div class="m2"><p>عیادت بیش بیمار مرا رنجور می دارد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زعیسی درد خود از ساده لوحی می کند پنهان</p></div>
<div class="m2"><p>زهمدرد آن که راز خویش را مستور می دارد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نیارد حد شرعی مست بیحد را به خود صائب</p></div>
<div class="m2"><p>زچوب دار کی اندیشه ای منصور می دارد؟</p></div></div>