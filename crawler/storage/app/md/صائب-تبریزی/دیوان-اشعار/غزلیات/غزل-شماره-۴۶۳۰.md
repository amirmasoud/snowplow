---
title: >-
    غزل شمارهٔ ۴۶۳۰
---
# غزل شمارهٔ ۴۶۳۰

<div class="b" id="bn1"><div class="m1"><p>چند روزی می دهم دل رابه دلجوی دگر</p></div>
<div class="m2"><p>می کنم محراب خود از طاق ابروی دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه اوراق دل من قابل شیرازه نیست</p></div>
<div class="m2"><p>می کنم شیرازه اش از تار گیسوی دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چه از ریحان جنت می چکد آب حیات</p></div>
<div class="m2"><p>سبزه پشت لب او راست نیروی دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر زمین باغ مشک وخاک او عنبر شود</p></div>
<div class="m2"><p>نشنودبلبل بغیر از بوی گل بوی دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا به چشمم نور وحدت سرمه حیرت کشید</p></div>
<div class="m2"><p>گشت هر داغ پلنگم چشم آهوی دگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا ز سیر گلشن آن سرو خرامان پا کشید</p></div>
<div class="m2"><p>شد نسیم صبح را هر غنچه زانوی دگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وای بر من کز غرور حسن شد خط غبار</p></div>
<div class="m2"><p>مستی چشم ترا بیهوشداروی دگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا به گرد شمع اوگردیده ام پروانه وار</p></div>
<div class="m2"><p>می کشم از هر پری ناز پریروی دگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست از دنیا بریدن کار هر بیجوهری</p></div>
<div class="m2"><p>دست دیگر خواهد این شمشیر وبازوی دگر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بعد عمری داد گردون گر لب نانی مرا</p></div>
<div class="m2"><p>بهر آزار دلم شد چین ابروی دگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روز وشب آورده ام در معنی بیگانه روی</p></div>
<div class="m2"><p>چون کنم صائب، ندارم آشنا روی دگر</p></div></div>