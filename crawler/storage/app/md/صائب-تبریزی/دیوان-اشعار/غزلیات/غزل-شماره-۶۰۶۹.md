---
title: >-
    غزل شمارهٔ ۶۰۶۹
---
# غزل شمارهٔ ۶۰۶۹

<div class="b" id="bn1"><div class="m1"><p>بهر معنی های رنگین لفظ را پرداز کن</p></div>
<div class="m2"><p>باده شیراز را در شیشه شیراز کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوی گل در غنچه سربسته ایمن از صباست</p></div>
<div class="m2"><p>لب ببند از گفتگو، خون در دل غماز کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آبرو را در عوض دریادلان گوهر دهند</p></div>
<div class="m2"><p>پیش ابر نوبهاران چون صدف لب باز کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بصیرت ترک دنیا سهل و آسان می شود</p></div>
<div class="m2"><p>بهر پوشیدن، درین هنگامه چشمی باز کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از هوس ها قالب خود را تهی چون ساختی</p></div>
<div class="m2"><p>بر میان گلرخان چون بهله دست انداز کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زود دلگیر از تماشای چمن خواهی شدن</p></div>
<div class="m2"><p>در حریم بیضه سامان پر پرواز کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راه بی پایان خود تا کنی یک نعره وار</p></div>
<div class="m2"><p>خرده جان را سپند شعله آواز کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از نیاز پست فطرت ناز مردم می کشی</p></div>
<div class="m2"><p>بی نیازی پیشه خود کن، به عالم ناز کن</p></div></div>