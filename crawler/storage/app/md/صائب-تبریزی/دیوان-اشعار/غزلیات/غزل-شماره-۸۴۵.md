---
title: >-
    غزل شمارهٔ ۸۴۵
---
# غزل شمارهٔ ۸۴۵

<div class="b" id="bn1"><div class="m1"><p>منه بر دل زار بار جهان را</p></div>
<div class="m2"><p>سبک ساز بر شاخ گل آشیان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نفس آتشین کن به تسخیر گردون</p></div>
<div class="m2"><p>که آتش کند نرم، پشت کمان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو شد زهر عادت، مضرت نبخشد</p></div>
<div class="m2"><p>به مرگ آشنا کن به تدریج جان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همین است پیغام گلهای رعنا</p></div>
<div class="m2"><p>که یک کاسه کن نوبهار و خزان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل صاف در بند دنیا نماند</p></div>
<div class="m2"><p>به تدریج گوهر خورد ریسمان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود کیمیا قرب اهل سعادت</p></div>
<div class="m2"><p>هما مغز دولت کند استخوان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برآور ز دل آه گردون نوردی</p></div>
<div class="m2"><p>ز سر باز کن این شرار و دخان را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز تن دست بردار و جان را صفا ده</p></div>
<div class="m2"><p>که آیینه چشم است آیینه دان را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به غیر از زیان نیست در خودفروشی</p></div>
<div class="m2"><p>اگر سود خواهی ببند این دکان را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز معراج منصب مجو پایداری</p></div>
<div class="m2"><p>که بر یخ بود پای این نردبان را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بود غیبت خلق، مردارخواری</p></div>
<div class="m2"><p>بپرداز ازین لقمه کام و زبان را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز گوهر دهد لقمه ات ابر نیسان</p></div>
<div class="m2"><p>اگر چون صدف پاک سازی دهان را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نکرد آسمان راست قامت در اینجا</p></div>
<div class="m2"><p>تو خواهی کنی راست، کار جهان را؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تکلف مکن در سلوکی که داری</p></div>
<div class="m2"><p>چو خواهی که از خود کنی میهمان را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به زنجیر، دیوانه ننشیند از پا</p></div>
<div class="m2"><p>چه پروای موج است آب روان را؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فلک را مترسان به آه دروغین</p></div>
<div class="m2"><p>که از تیر کج نیست پروا نشان را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به اشکی توان کند بنیاد غفلت</p></div>
<div class="m2"><p>که یک قطره، سیل است خواب گران را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جهان استخوانی است بی مغز صائب</p></div>
<div class="m2"><p>به پیش سگ انداز این استخوان را</p></div></div>