---
title: >-
    غزل شمارهٔ ۵۱۹۹
---
# غزل شمارهٔ ۵۱۹۹

<div class="b" id="bn1"><div class="m1"><p>کیست آرد پشت گردون ستمگر را به خاک</p></div>
<div class="m2"><p>می زند این کهنه کشتی گیر یکسر را به خاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غوطه زن دربحر سیل از کدورت پاک شو</p></div>
<div class="m2"><p>تابه کی خواهی کشیدن دامن تررا به خاک ؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزی ماشد چو موران عشرت روی زمین</p></div>
<div class="m2"><p>از قناعت تا بدل کردیم شکر را به خاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاک راهند این خسیسان، آبرو و آب گهر</p></div>
<div class="m2"><p>چند ریزی ای ستمگر آب گوهر را به خاک؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از پشیمانی زپشت دست خود سازد گزک</p></div>
<div class="m2"><p>کوته اندیشی که ریزد درد ساغر را به خاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسن عالمسوز را پروای آه سرد نیست</p></div>
<div class="m2"><p>می کشد این شعله بیباک صرصررا به خاک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در تلاش نعمت دنیا عرق ریزی مکن</p></div>
<div class="m2"><p>ای بهشتی رو چه ریزی آب کوثر را به خاک؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می توان تا تشنه ای را چون صدف سیراب کرد</p></div>
<div class="m2"><p>نیست ازهمت فشاندن آب گوهر را به خاک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیل از ویرانه با رخسار گرد آلود رفت</p></div>
<div class="m2"><p>زود می مالد فلک روی ستمگررا به خاک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر که نقش خویش را در خاکساری دیده است</p></div>
<div class="m2"><p>می نهد چون بوریا پهلوی لاغررا به خاک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سعی دارد در زوال آفتاب عمر خود</p></div>
<div class="m2"><p>هرکه اندازد درخت سایه گستر را به خاک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مور گویا را سلیمان پایتخت ازدست داد</p></div>
<div class="m2"><p>می کشند اکنون سبک مغزان سخنور را به خاک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با سیه بختی شدم خرسند، تادیدم که چرخ</p></div>
<div class="m2"><p>می کشد گیسو کشان خورشید انور را به خاک</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نقد خود را نسیه کردن صائب ازعقل است دور</p></div>
<div class="m2"><p>بهر زر تاچند مالی روی چون زر را به خاک؟</p></div></div>