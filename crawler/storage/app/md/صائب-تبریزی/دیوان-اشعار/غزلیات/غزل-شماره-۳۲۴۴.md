---
title: >-
    غزل شمارهٔ ۳۲۴۴
---
# غزل شمارهٔ ۳۲۴۴

<div class="b" id="bn1"><div class="m1"><p>آن که از عمر سبکسیر وفا می طلبد</p></div>
<div class="m2"><p>لنگر از سیل و اقامت ز هوا می طلبد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکه دارد طمع عافیت از آخر عمر</p></div>
<div class="m2"><p>ساده لوحی است که از درد صفا می طلبد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کشتیی را که شود کوه غم من لنگر</p></div>
<div class="m2"><p>ناخدا موج خطر را ز خدا می طلبد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به گواهان لباسی نشود خون ثابت</p></div>
<div class="m2"><p>خون ما را که ازان لعل قبا می طلبد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هوس دیدن رویی است مرا در خاطر</p></div>
<div class="m2"><p>که نقابش دو جهان روی نما می طلبد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صدف پوچ گران است به دل دریا را</p></div>
<div class="m2"><p>دامن دشت جنون آبله پا می طلبد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست از سایه دیوار قناعت خبرش</p></div>
<div class="m2"><p>آن که دولت ز پر و بال هما می طلبد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حرص بی شرم به آداب نمی پردازد</p></div>
<div class="m2"><p>همه چیز از همه کس در همه جا می طلبد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشم بر دست فقیرست غنی را صائب</p></div>
<div class="m2"><p>شاه پیوسته ز درویش دعا می طلبد</p></div></div>