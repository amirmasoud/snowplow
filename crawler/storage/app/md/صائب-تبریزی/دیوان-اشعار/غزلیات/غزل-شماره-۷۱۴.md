---
title: >-
    غزل شمارهٔ ۷۱۴
---
# غزل شمارهٔ ۷۱۴

<div class="b" id="bn1"><div class="m1"><p>نتوان به خواب کرد مسخر خیال را</p></div>
<div class="m2"><p>جز پیچ و تاب نیست کمند این غزال را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عالم خیال، بهارست چار فصل</p></div>
<div class="m2"><p>بلبل به چتر گل ندهد زیر بال را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چند حسن را خطر از چشم پاک نیست</p></div>
<div class="m2"><p>پنهان ز آب و آینه کن آن جمال را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رحمی به شیشه خانه دلهای خلق کن</p></div>
<div class="m2"><p>از می مکن دو آتشه آن رنگ آل را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از گلشنی که سرو تو دامن کشان رود</p></div>
<div class="m2"><p>بی طاقتی ز ریشه برآرد نهال را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برگ نشاط نیست درین تیره خاکدان</p></div>
<div class="m2"><p>ریحان ز آه سرد بود این سفال را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ده در شود گشاده، شود بسته چون دری</p></div>
<div class="m2"><p>انگشت، ترجمان زبان است لال را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با تیرگی بساز که ابروی عنبرین</p></div>
<div class="m2"><p>یکشب سفید گشت ز منت هلال را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر جرم من ببخش که آورده ام شفیع</p></div>
<div class="m2"><p>اشک ندامت و عرق انفعال را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در ملک خویش رخنه فکندن ز عقل نیست</p></div>
<div class="m2"><p>زنهار بسته دار زبان سؤال را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب کشید سر به گریبان نیستی</p></div>
<div class="m2"><p>تسخیر کرد مملکت بی زوال را</p></div></div>