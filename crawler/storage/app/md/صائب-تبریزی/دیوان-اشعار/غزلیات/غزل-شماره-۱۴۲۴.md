---
title: >-
    غزل شمارهٔ ۱۴۲۴
---
# غزل شمارهٔ ۱۴۲۴

<div class="b" id="bn1"><div class="m1"><p>در کمین این فلک سخت کمانی که تراست</p></div>
<div class="m2"><p>عاقبت گرد برآرد ز نشانی که تراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نعمت روی زمین چشم ترا سیر نکرد</p></div>
<div class="m2"><p>چه کند خاک به چشم نگرانی که تراست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ریخت دندان تو چون اختر صبح از پیری</p></div>
<div class="m2"><p>مشرق شکر نگردید دهانی که تراست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قامتت بید موله شد و چون سرو کشد</p></div>
<div class="m2"><p>سر به عیوق، تمنای جوانی که تراست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ریاضی که بود دولت گل پا به رکاب</p></div>
<div class="m2"><p>چه اقامت کند این برگ خزانی که تراست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>استخوانهای ترا پیشتر از خاک شدن</p></div>
<div class="m2"><p>توتیا می کند این خواب گرانی که تراست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صرف کن چون مه نو توشه خود را زنهار</p></div>
<div class="m2"><p>تا شود قرص تمام این لب نانی که تراست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قامتت خم شد و هموار نگشتی صائب</p></div>
<div class="m2"><p>دم شمشیر بود پشت کمانی که تراست</p></div></div>