---
title: >-
    غزل شمارهٔ ۲۳۱۹
---
# غزل شمارهٔ ۲۳۱۹

<div class="b" id="bn1"><div class="m1"><p>کشتی دریایی‌ای دیدم دلم آمد به یاد</p></div>
<div class="m2"><p>حال دور افتادگان ساحلم آمد به یاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برق را دست و گریبان گیاهی یافتم</p></div>
<div class="m2"><p>گرم‌خونی‌های تیغ قاتلم آمد به یاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوهری افتاده دیدم در میان خاک راه</p></div>
<div class="m2"><p>حال جان در ورطه آب و گلم آمد به یاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نشاط بی‌ثبات غافلان روزگار</p></div>
<div class="m2"><p>شوخی پرواز مرغ بسملم آمد به یاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرنگون دیدم در آن چاه زنخدان زلف را</p></div>
<div class="m2"><p>قصه هاروت و چاه بابلم آمد به یاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سربه‌هم‌آورده دیدم برگ‌های غنچه را</p></div>
<div class="m2"><p>اجتماع دوستان یک‌دلم آمد به یاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست صائب کمتر از منزل حضور راه عشق</p></div>
<div class="m2"><p>کافرم در راه اگر از منزلم آمد به یاد</p></div></div>