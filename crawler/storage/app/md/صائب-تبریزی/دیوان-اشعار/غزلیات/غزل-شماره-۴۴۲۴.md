---
title: >-
    غزل شمارهٔ ۴۴۲۴
---
# غزل شمارهٔ ۴۴۲۴

<div class="b" id="bn1"><div class="m1"><p>داغ از جگر سوختگان دیر برآید</p></div>
<div class="m2"><p>خورشید ز مغرب به قیامت بدر آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هرنظر آن چهره به رنگ دگر آید</p></div>
<div class="m2"><p>چون عاشق رخسار تو یکرنگ برآید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن چشم نه خوابی است که تعبیر توان کرد</p></div>
<div class="m2"><p>آن زلف شبی نیست به افسانه سرآید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در هر شکنی دام تماشاست مهیا</p></div>
<div class="m2"><p>از زلف گرهگیر تو چون دل بدرآید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از پرتو آن صبح بناگوش عجب نیست</p></div>
<div class="m2"><p>گرآب شود رنگ وز چشم گهرآید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد آب در گوش تو زان صبح بناگوش</p></div>
<div class="m2"><p>خشک از قدح شیر برون چون شکر آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از خود خبر آمدنش بیخبرم کرد</p></div>
<div class="m2"><p>ای وای اگر از در من بیخبر آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برسینه ما قدرشناسان جراحت</p></div>
<div class="m2"><p>تیری که خطا گشت فزون کارگر آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ذوقی که ز پیغام تو دل یافت نباید</p></div>
<div class="m2"><p>آن کس که عزیزش ز سفر بیخبر آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در عالم افسرده چو دلسوخته ای نیست</p></div>
<div class="m2"><p>از سنگ به امید چه بیرون شرر آید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با صد دل بیغم چه کندیک دل محزون</p></div>
<div class="m2"><p>دیوانه محال است به اطفال برآید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر برگ درین باغ شود دامن پرسنگ</p></div>
<div class="m2"><p>روزی که مرا نخل تمنا به برآید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون لاله محال است شود شسته به باران</p></div>
<div class="m2"><p>رنگی که به رخسار به خون جگر آید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>روشن شود از نقش قدم شمع امیدش</p></div>
<div class="m2"><p>هر راهنوردی که مرا بر اثر آید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>باریک چو خط شد نگه موی شکافان</p></div>
<div class="m2"><p>تا آن دهن تنگ که را در نظر آید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سوزد دل سنگ از ناله ناقوس</p></div>
<div class="m2"><p>صائب اگر از گوشه بتخانه برآید</p></div></div>