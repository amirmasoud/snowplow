---
title: >-
    غزل شمارهٔ ۳۱۸۱
---
# غزل شمارهٔ ۳۱۸۱

<div class="b" id="bn1"><div class="m1"><p>تو از نام بلند ای نوجوان بردار کام خود</p></div>
<div class="m2"><p>که پیران می کنند از قامت خم حلقه نام خود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زفیض راستی از محتسب بر خود نمی لرزم</p></div>
<div class="m2"><p>به کوه قاف دارم پشت از سنگ تمام خود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر از بیطاقتی خود قاصد پیغام خود گردم</p></div>
<div class="m2"><p>فرامش می کنم در راه از غیرت پیام خود!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حذر کن از می سرکش که تاکش با زمین گیری</p></div>
<div class="m2"><p>به چندین دست نتواند نگه دارد زمام خود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا از بوته خجلت بر آر ای شعله سرکش</p></div>
<div class="m2"><p>که خونها می خورم چون لاله از سودای خام خود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه افتاده است بر دل بار گردم عندلیبان را؟</p></div>
<div class="m2"><p>چو من از بوی گل چون غنچه می گیرم مشا خود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز آواز شکست من دل احباب می ریزد</p></div>
<div class="m2"><p>وگرنه من نمی دارم دریغ از سنگ جام خود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکاری چون به بخت ما نمی افتد همان بهتر</p></div>
<div class="m2"><p>که در خاک فراموشان نهان سازیم دام خود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به شور من ندارد بلبلی این بوستان صائب</p></div>
<div class="m2"><p>روان گردد، به خون مرده گر خوانم کلام خود</p></div></div>