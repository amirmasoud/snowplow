---
title: >-
    غزل شمارهٔ ۲۱۵۲
---
# غزل شمارهٔ ۲۱۵۲

<div class="b" id="bn1"><div class="m1"><p>گفتار تو شهدی است که جانها مگس اوست</p></div>
<div class="m2"><p>رفتار تو سیلی است که دل خار و خس اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر ناله که از دل ز سر صدق برآید</p></div>
<div class="m2"><p>صبحی است که تسخیر جهان در نفس اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نخلی که برآرنده خود را نشناسد</p></div>
<div class="m2"><p>سر پیش فکندن ثمر پیشرس اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر چند که از محمل لیلی اثری نیست</p></div>
<div class="m2"><p>صد بادیه پر شور ز بانگ جرس اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با هر که کسی نیست به جز بیکسی او را</p></div>
<div class="m2"><p>صائب به ادب باش که بی گفت، کس اوست</p></div></div>