---
title: >-
    غزل شمارهٔ ۴۸۳۵
---
# غزل شمارهٔ ۴۸۳۵

<div class="b" id="bn1"><div class="m1"><p>هیچ کار از ما نمی آید ز کار ما مپرس</p></div>
<div class="m2"><p>رفته ایم از خویش بیرون از دیار ما مپرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کوه تمکین حبابیم از شکیب مامگوی</p></div>
<div class="m2"><p>جلوه موج سرابیم از قرار ما مپرس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بید مجنونیم برگ ما زبان خامشی است</p></div>
<div class="m2"><p>گل بچین از برگ ما، احوال بار ما مپرس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دامن آرام بر دامان صرصر بسته ایم</p></div>
<div class="m2"><p>از پریشان حالی مشت غبار ما مپرس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیده خورشید، فتراک سحر خیزان بود</p></div>
<div class="m2"><p>حلقه فتراک ما بین از شکار ما مپرس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازدیار حسن خیز عشق می آییم ما</p></div>
<div class="m2"><p>می شوی آواره احوال دیار ما مپرس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نقل ارباب جنون دیوانگی می آورد</p></div>
<div class="m2"><p>رحم کن بر خویش از جوش بهار ما مپرس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سبحه ریگ روان انگشت حیرت می گزد</p></div>
<div class="m2"><p>از شمار داغهای بی شمار ما مپرس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شرح حال دردمندان دردسر می آورد</p></div>
<div class="m2"><p>میل دردسر نداری از خمار ما مپرس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حلقه تأدیب در گوش معلم می کشند</p></div>
<div class="m2"><p>از فضولیهای اطفال دیار ما مپرس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یک نگاه گرم در سرچشمه خورشید کن</p></div>
<div class="m2"><p>پیش رویش حال چشم اشکبار ما مپرس</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کار ما چون زلف خوبان در گره افتاده است</p></div>
<div class="m2"><p>می کنی سر رشته گم صائب ز کار ما مپرس</p></div></div>