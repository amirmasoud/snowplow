---
title: >-
    غزل شمارهٔ ۶۰۸۸
---
# غزل شمارهٔ ۶۰۸۸

<div class="b" id="bn1"><div class="m1"><p>شکوه بیهوده از ناسازی گردون مکن</p></div>
<div class="m2"><p>این جراحت را به شمشیر زبان افزون مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تلخی ایام را بر خود گوارا کن به صبر</p></div>
<div class="m2"><p>تا ز می پر توان کرد این قدح پر خون مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست افسوس است بار سرو موزون، زینهار</p></div>
<div class="m2"><p>تا تو هم بی بر نگردی مصرعی موزون مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبح پیری نیست چون شام جوانی پرده پوش</p></div>
<div class="m2"><p>آنچه ممکن بود کردی پیش ازین، اکنون مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از شکست خصم خوشحالی، ندامت بردهد</p></div>
<div class="m2"><p>زینهار این ریزه الماس در معجون مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می نشیند زود در گل کشتی سنگین رکاب</p></div>
<div class="m2"><p>تکیه بر سیم و زر بسیار چون قارون مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تاج دریای گهر شد از سبکروحی حباب</p></div>
<div class="m2"><p>چون ز خود گشتی تهی اندیشه از جیحون مکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زردرو از برگریزان ندامت می شوی</p></div>
<div class="m2"><p>روی خود را از شراب بی غمی گلگون مکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چاره بیماری دل را ز افلاطون مجوی</p></div>
<div class="m2"><p>زین طبیب خام درد خویش را افزون مکن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حسن شرم آلود لیلی دامن از خود می کشد</p></div>
<div class="m2"><p>از غزالان گرد خود هنگامه چون مجنون مکن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون مسیحا پای همت بر سر گردون گذار</p></div>
<div class="m2"><p>خویش را در خم حصاری همچو افلاطون مکن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می شود سنگ ملامت در کف طفلان غریب</p></div>
<div class="m2"><p>از سواد شهر صائب روی در هامون مکن</p></div></div>