---
title: >-
    غزل شمارهٔ ۲۴۲۵
---
# غزل شمارهٔ ۲۴۲۵

<div class="b" id="bn1"><div class="m1"><p>جان به تنگ آمد زکلفت غمگساران را چه شد؟</p></div>
<div class="m2"><p>دل به جان آمد ز وحشت دل شکاران را چه شد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاهدان سنگدل بستند اگر کشتی به خشک</p></div>
<div class="m2"><p>همت دریا رکاب میگساران را چه شد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر نمی آرند خاری همرهان از پای هم</p></div>
<div class="m2"><p>گردل سوزن ز آهن گشت، یاران را چه شد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست گلها را اگر بسته است غفلت در نگار</p></div>
<div class="m2"><p>پنجه مشکل گشای نوبهاران را چه شد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عافیت در روزگار و روشنی در روز نیست</p></div>
<div class="m2"><p>کس نمی داند که روز و روزگاران را چه شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سردی از حد می برد باد خزان با گلستان</p></div>
<div class="m2"><p>ناله های سینه گرم هزاران را چه شد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عمرها شد خاک از ته جرعه ای لب تر نکرد</p></div>
<div class="m2"><p>همت بی اختیار باده خواران را چه شد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست گر آب مروت در نظر احباب را</p></div>
<div class="m2"><p>گریه مستانه ابر بهاران را چه شد؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه زدل مانده است در عالم اثر، نه زاهل دل</p></div>
<div class="m2"><p>یارب این آیینه و آیینه داران را چه شد؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کاروانی را اگر دل می رود دنبال بار</p></div>
<div class="m2"><p>خاطر آسوده چابک سواران را چه شد؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صحبت گردنکشان کرده است دل را سیم قلب</p></div>
<div class="m2"><p>کیمیای دلپذیر خاکساران را چه شد؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بخت چون برگشت، بر گردند یاران سربسر</p></div>
<div class="m2"><p>تا به کی صائب خبرپرسی که یاران را چه شد؟</p></div></div>