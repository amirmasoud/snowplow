---
title: >-
    غزل شمارهٔ ۴۰۹۲
---
# غزل شمارهٔ ۴۰۹۲

<div class="b" id="bn1"><div class="m1"><p>عمری گذشت و نامهٔ جانان نمی‌رسد</p></div>
<div class="m2"><p>دیری است پیک مصر به کنعان نمی‌رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمی به داغ لاله عبث سرخ کرده‌ایم</p></div>
<div class="m2"><p>فیض سیاه کاسه به مهمان نمی‌رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیروز سیل گریه ز طوفان گذشته بود</p></div>
<div class="m2"><p>امروز پیک اشک به مژگان نمی‌رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در هیچ نشئه نیست که سیری نکرده‌ایم</p></div>
<div class="m2"><p>کیفیتی به صحبت مستان نمی‌رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با آن که حق اوست ادافهمی سخن</p></div>
<div class="m2"><p>صائب به شعر همچو ظفرخان نمی‌رسد</p></div></div>