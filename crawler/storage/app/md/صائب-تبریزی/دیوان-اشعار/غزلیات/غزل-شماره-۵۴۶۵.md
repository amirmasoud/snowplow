---
title: >-
    غزل شمارهٔ ۵۴۶۵
---
# غزل شمارهٔ ۵۴۶۵

<div class="b" id="bn1"><div class="m1"><p>از شکست آرزو قند مکرر می خوریم</p></div>
<div class="m2"><p>بر لب خود خاک می مالیم و شکر می خوریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با سپهر تلخ سیما خنده رو بر می خوریم</p></div>
<div class="m2"><p>زهر اگر در جام ما ریزند شکر می خوریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از تو تا دوریم از ما دور می گردد حیات</p></div>
<div class="m2"><p>با تو چون بر می خوریم از زندگی برمی خوریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شیوه ما نیست از بیداد روگردان شدن</p></div>
<div class="m2"><p>سیلی دریا ز خلق خوش چو عنبر می خوریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از عزیز مصر و شکرزار او آسوده ایم</p></div>
<div class="m2"><p>ما که گرد کاروان را همچو شکر می خوریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر چه تبخال خون داریم ظاهر در قدح</p></div>
<div class="m2"><p>بی گزند دیده بد آب کوثر می خوریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الوان عالم را کند خون در جگر</p></div>
<div class="m2"><p>کاسه خونی که ما از دست دلبر می خوریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می کند از روزی ما کم سپهر تنگ چشم</p></div>
<div class="m2"><p>گاهی از بی دست و پایی گر سکندر می خوریم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میوه های خام انجم پخته شد بر خاک ریخت</p></div>
<div class="m2"><p>ما زخامی همچنان گرمای محشر می خوریم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خودنمایی نیست در زیر فلک آیین ما</p></div>
<div class="m2"><p>زیر خاکستر دل خود همچو اخگر می خوریم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برنمی داریم دست از زلف مشکین سخن</p></div>
<div class="m2"><p>چون قلم چندان که زخم تیغ بر سر می خوریم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در تلافی میوه شیرین به دامن می دهیم</p></div>
<div class="m2"><p>همچو نخل پر ثمر سنگی که بر سر می خوریم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صائب از فیض خموشی در دل دریای تلخ</p></div>
<div class="m2"><p>آب شیرین چون صدف از جام گوهر می خوریم</p></div></div>