---
title: >-
    غزل شمارهٔ ۱۷۲۶
---
# غزل شمارهٔ ۱۷۲۶

<div class="b" id="bn1"><div class="m1"><p>صفای حسن تو از خط به جای خویشتن است</p></div>
<div class="m2"><p>درین غبار همان بر صفای خویشتن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هنوز می چکد از چهره تو آب حیات</p></div>
<div class="m2"><p>هنوز سرو قدت در هوای خویشتن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر چه حسن تو از خط شده است پا به رکاب</p></div>
<div class="m2"><p>سبک عنانی زلفت به جای خویشتن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هنوز گردش چشم تر است دور بجا</p></div>
<div class="m2"><p>هنوز گوش تو مست نوای خویشتن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هنوز آن صف مژگان ز هم نپاشیده است</p></div>
<div class="m2"><p>هنوز چشم تو محو لقای خویشتن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هنوز مرکز حسن است خال مشکینت</p></div>
<div class="m2"><p>هنوز زلف تو زنجیر پای خویشتن است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هنوز لطف بجا صرف می شود بیجا</p></div>
<div class="m2"><p>هنوز رنجش بیجا به جای خویشتن است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نبسته است به زنجیر پای ما را عشق</p></div>
<div class="m2"><p>قلاده سگ ما از وفای خویشتن است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز تنگنای صدف بی حجاب بیرون آی</p></div>
<div class="m2"><p>که گوهر تو نهان در صفای خویشتن است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دماغ بنده نوازی نمانده است ترا</p></div>
<div class="m2"><p>وگرنه بندگی ما به جای خویشتن است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز آشنایی مردم حذر کند صائب</p></div>
<div class="m2"><p>کسی که از ته دل آشنای خویشتن است</p></div></div>