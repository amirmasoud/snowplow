---
title: >-
    غزل شمارهٔ ۶۱۰۱
---
# غزل شمارهٔ ۶۱۰۱

<div class="b" id="bn1"><div class="m1"><p>آه می دزدد نفس در سینه افگار من</p></div>
<div class="m2"><p>غنچه می خسبد نسیم صبح در گلزار من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرده گنج است ویرانی، که تا محشر مباد</p></div>
<div class="m2"><p>سایه افتادگی کم از سر دیوار من!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلبل تصویر، گلبانگ نشاط از دل کشید</p></div>
<div class="m2"><p>چند باشد غنچه زیر بال و پر منقار من؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با دم جان پرور شمشیر عادت کرده است</p></div>
<div class="m2"><p>از دم عیسی هوا یابد دل بیمار من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آسیا را دانه جان سخت من دندان شکست</p></div>
<div class="m2"><p>آسمان بیهوده می کوشد پی آزار من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر چه از مژگان کلکم آب حیوان می چکد</p></div>
<div class="m2"><p>می توان گرد کسادی رفت از بازار من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هیچ گه دست حوادث از سرم کوته نبود</p></div>
<div class="m2"><p>قطره می زد در رکاب سیل دایم خار من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب از بس چرخ در کارم گره افکنده است</p></div>
<div class="m2"><p>رشته تسبیح در تاب است از زنار من</p></div></div>