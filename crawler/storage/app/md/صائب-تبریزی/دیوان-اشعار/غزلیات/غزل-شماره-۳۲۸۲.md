---
title: >-
    غزل شمارهٔ ۳۲۸۲
---
# غزل شمارهٔ ۳۲۸۲

<div class="b" id="bn1"><div class="m1"><p>راست آزرده کی از زخم زبان می‌گردد؟</p></div>
<div class="m2"><p>تیر کج باعث آرام نشان می‌گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برق اگر پای درین وادی خونخوار نهد</p></div>
<div class="m2"><p>از نفس سوختگی سنگ نشان می‌گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نفس کج‌رو ز نصیحت ننهد پای به راه</p></div>
<div class="m2"><p>تیر کج راست کی از زور کمان می‌گردد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دولت سنگدلان را نبود استقرار</p></div>
<div class="m2"><p>سیل از کوه به تعجیل روان می‌گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شمع در جامه فانوس نماند پنهان</p></div>
<div class="m2"><p>هرچه در دل بود از جبهه عیان می‌گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در طلب باش که از گرمی صحرای طلب</p></div>
<div class="m2"><p>پای پر آبله از دیده‌‌وران می‌گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روزگاری است که از رهگذر ناسازی</p></div>
<div class="m2"><p>سنگ اطفال به دیوانه گران می‌گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بس که خون می‌خورد از خار درین سبز چمن</p></div>
<div class="m2"><p>زر به کف گل ز پی باد خزان می‌گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می‌شود حرص هم از جمع زر و سیم غنی</p></div>
<div class="m2"><p>تشنه سیراب اگر از ریگ روان می‌گردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می‌نمایند به انگشت چو ماه عیدش</p></div>
<div class="m2"><p>قسمت هرکه ز گردون لب نان می‌گردد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرد کلفت ز دل صاف کشان می‌چیند</p></div>
<div class="m2"><p>هرکه در میکده از دُردکشان می‌گردد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سرخ‌رو سر زند از خاک به محشر صائب</p></div>
<div class="m2"><p>هرکه از جمله خونابه‌کشان می‌گردد</p></div></div>