---
title: >-
    غزل شمارهٔ ۱۷۳۰
---
# غزل شمارهٔ ۱۷۳۰

<div class="b" id="bn1"><div class="m1"><p>زبان شکوه من چشم خونفشان من است</p></div>
<div class="m2"><p>چو طفل بسته زبان گریه ترجمان من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا به حرف کجا روز حشر بگذارد؟</p></div>
<div class="m2"><p>ز شرم حسن تو بندی که بر زبان من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به داستان سر زلف کوتهی مرساد!</p></div>
<div class="m2"><p>که تا به کعبه مقصود نردبان من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز من بود سخن راست هر که می گوید</p></div>
<div class="m2"><p>خدنگ راست رو از خانه کمان من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حذر نمی کنم از تیغ زهرداده سرو</p></div>
<div class="m2"><p>که طوق عشق چو قمری خط امان من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به بال سایه پریدن ز کوته اندیشی است</p></div>
<div class="m2"><p>وگرنه بال هما فرش آستان من است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هما ز سایه من غوطه می خورد در نیش</p></div>
<div class="m2"><p>ز بس که نیش ملامت در استخوان من است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به اوج عرش سخن را رسانده ام صائب</p></div>
<div class="m2"><p>بلند نام شود هر که در زمان من است</p></div></div>