---
title: >-
    غزل شمارهٔ ۴۵۵۷
---
# غزل شمارهٔ ۴۵۵۷

<div class="b" id="bn1"><div class="m1"><p>مردمک را سیر کن در حلقه چشم نگار</p></div>
<div class="m2"><p>گر ندیدی درمیان جرگه آهوی تتار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جام لبریزی است در گردش میان میکشان</p></div>
<div class="m2"><p>مردمک در حلقه آن چشمهای پر خمار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نور و ظلمت راکه از سحر آفرینان کرده است</p></div>
<div class="m2"><p>جمع در یک کاسه،غیر از مردمک درچشم یار؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مردمک چون خانه کعبه است و مژگان حاجیان</p></div>
<div class="m2"><p>کز برای سجده اش صف بسته اند از هر کنار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیمه لیلی است در دشت بیاض آن مردمک ؟</p></div>
<div class="m2"><p>یاز ناف روز روشن، شد دل شب آشکار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مردمک راکن نظر در چشم شرم آلود او</p></div>
<div class="m2"><p>گر ندیدی مریم آورده عیسی در کنار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مردمک هرچند باشد مرکز پرگار چشم</p></div>
<div class="m2"><p>مرکز اینجا بیش از پرگار باشد بیقرار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناف مشکین غزال چشم باشد مردمک</p></div>
<div class="m2"><p>دوربادا چشم بد زین آهوی مردم شکار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سینه چاکان دارد از مژگان به گرد خویشتن</p></div>
<div class="m2"><p>مردم آن چشم،مستغنی است از عشاق زار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بود اگر چتر سلیمان از پروبال پری</p></div>
<div class="m2"><p>مردمک دارد ز نور خویش چتر زرنگار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر سیه کاسه است در چشمش به ظاهرمردمک</p></div>
<div class="m2"><p>عالمی را دارد از مردم نوازی شرمسار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حوریان از روزن جنت برون آرند سر</p></div>
<div class="m2"><p>چون نگه زان مردمان چشم گردد آشکار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چند روزی دور خوبی زلف و خط رابیش نیست</p></div>
<div class="m2"><p>دورحسن مردمک هرگز نیفتد از مدار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>می شود نرگس به هر رنگی که باشد آب او</p></div>
<div class="m2"><p>سرخ ازان شد مردمک در نرگس خونخواریار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>می کند هر دم کمندی حلقه از تارنگاه</p></div>
<div class="m2"><p>نیست سیری مردمان چشم او را از شکار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر چه دارد مهر خاموشی به لب از مردمک</p></div>
<div class="m2"><p>چشم مست او بود در گفتگو بی اختیار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ازحیا گر مردم چشمش به ظاهر ننگرد</p></div>
<div class="m2"><p>می برد در پرده دل از مردمان بی اختیار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دامن لیلی، سر سودایی مجنون بود</p></div>
<div class="m2"><p>مردمک در پرده چشم حجاب آلود یار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در سواد چشم او بنگر نگاه گرم را</p></div>
<div class="m2"><p>گر ندیدی برق در ابر سیاه نوبهار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دل ز دست مردم چشمش گرفتن مشکل است</p></div>
<div class="m2"><p>کشتی از گرداب ممکن نیست آیدبرکنار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کاسه اش هر چند در ظاهر نگون افتاده است</p></div>
<div class="m2"><p>تر نمی سازدلبی را از شراب خوشگوار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>می برد در بردن دلها ز مژگان بلند</p></div>
<div class="m2"><p>مردم چشم سیه مستش ید طولی به کار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>می رساند خانه چشم نظر بازان به آب</p></div>
<div class="m2"><p>مردم چشمش زمژگان سیه عیار وار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گرزمستیهاصف مژگان رگ خوابش شود</p></div>
<div class="m2"><p>مردم آن چشم از شوخی نمی گیرد قرار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>درزمان مردم آن چشم،چشم آهوان</p></div>
<div class="m2"><p>در نظر چون نقطه های سهو شد بی اعتبار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مردم خونریز چشم او به قصد عاشقان</p></div>
<div class="m2"><p>دارد از مژگان حمایل تیغهای آبدار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>می کند نام غزالان ختن را حلقه زود</p></div>
<div class="m2"><p>مردم آن چشم از مد نگاه مشکبار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چشم شرم آلود او رامردمک چون مهر شرم</p></div>
<div class="m2"><p>از پریشان گردی نظاره دارد در حصار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آن که دلهای پریشان راکند گرد آوری</p></div>
<div class="m2"><p>نیست غیر از مردمک در دور چشم آن نگار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در بیاض چشم او تا مردمک را دیده است</p></div>
<div class="m2"><p>بر عذار خود نقاب افکنده عنبر از بهار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کرده از یک آستین صددست مژگانش برون</p></div>
<div class="m2"><p>تا نیفتد چشم مستش هر طرف بی اختیار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خضر اگر تیری به تاریکی فکند از ره مرو</p></div>
<div class="m2"><p>در سواد چشم او بین آب حیوان آشکار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>این غزل صائب به فرمان سلیمان زمان</p></div>
<div class="m2"><p>از زبان خامه سحر آفرین شد آشکار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تا بود از مردمک روشن چراغ دیده ها</p></div>
<div class="m2"><p>دور بادا چشم بد زین خسرو عالم مدار</p></div></div>