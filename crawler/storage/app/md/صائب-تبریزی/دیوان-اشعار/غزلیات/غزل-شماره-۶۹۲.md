---
title: >-
    غزل شمارهٔ ۶۹۲
---
# غزل شمارهٔ ۶۹۲

<div class="b" id="bn1"><div class="m1"><p>ترجیح می دهد به پدر اوستاد را</p></div>
<div class="m2"><p>هر کس شناخته است بیاض و سواد را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با نیک و بد چو شیر و شکر جوش می زند</p></div>
<div class="m2"><p>دریافت هر که چاشنی اتحاد را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در زیر آسمان نبود صبح بی شفق</p></div>
<div class="m2"><p>خون در پیاله است جبین گشاد را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زخم زبان چه کار به سرگشتگان کند؟</p></div>
<div class="m2"><p>پروای خار و خس نبود گردباد را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تلخی کشان عشق نگیرند جام زهر</p></div>
<div class="m2"><p>در محفلی که راه بود نوش باد را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از ابر بی نیاز بود تیغ آبدار</p></div>
<div class="m2"><p>حاجت به باده نیست روان های شاد را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زهرست شکری که مکرر نمی شود</p></div>
<div class="m2"><p>بدخو مکن به وصل، دل نامراد را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شایسته خدا و رسول است اعتقاد</p></div>
<div class="m2"><p>ضایع مکن به اهل جهان اعتقاد را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زنهار در درستی خط سعی کن که هست</p></div>
<div class="m2"><p>خط شکسته، خواب پریشان سواد را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صائب امید هست که آن خط عنبرین</p></div>
<div class="m2"><p>روشن کند سواد من بی سواد را</p></div></div>