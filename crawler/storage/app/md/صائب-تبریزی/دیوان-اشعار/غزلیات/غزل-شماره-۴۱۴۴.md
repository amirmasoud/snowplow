---
title: >-
    غزل شمارهٔ ۴۱۴۴
---
# غزل شمارهٔ ۴۱۴۴

<div class="b" id="bn1"><div class="m1"><p>خال ترا ز دیده تر سبز کرده اند</p></div>
<div class="m2"><p>این دانه را به خون جگر سبز کرده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ریحان به خط پشت لب او کجا رسد</p></div>
<div class="m2"><p>کاین سبزه رابه آب گهرسبزکرده اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سنگین دلی تو ورنه اسیران به آب چشم</p></div>
<div class="m2"><p>در مغز سنگ تخم شرر سبز کرده اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مانند طوطیان پروبال مرا به زهر</p></div>
<div class="m2"><p>در آرزوی تنگ شکر سبز کرده اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسیار تخم سوخته را درزمین شور</p></div>
<div class="m2"><p>صاحبدلان ز فیض نظر سبز کرده اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون بس کنم ز گریه که نخل مرا چو شمع</p></div>
<div class="m2"><p>از بهر اشک پاک گهر سبز کرده اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خشکی مکن که نخل ترا با دو صد امید</p></div>
<div class="m2"><p>خونین دلان برای ثمر سبز کرده اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ایمن نیم ز سرزنش پای رهروان</p></div>
<div class="m2"><p>کشت مرا به راهگذر سبز کرده اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل در جهان مبند که این نونهال را</p></div>
<div class="m2"><p>از بهر سرزمین دگر سبز کرده اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرگز نمی شود علف تیغ حادثات</p></div>
<div class="m2"><p>کشتی که از دو دیده تر سبز کرده اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب هزار کاسه پر زهر خورده اند</p></div>
<div class="m2"><p>تا نام خویش اهل هنر سبز کرده اند</p></div></div>