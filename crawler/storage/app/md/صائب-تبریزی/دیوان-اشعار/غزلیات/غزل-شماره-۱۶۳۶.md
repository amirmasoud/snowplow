---
title: >-
    غزل شمارهٔ ۱۶۳۶
---
# غزل شمارهٔ ۱۶۳۶

<div class="b" id="bn1"><div class="m1"><p>اوست سرور که کلاه و کمر از یادش رفت</p></div>
<div class="m2"><p>آن توانگر بود اینجا که زر از یادش رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان به این غمکده آمد که سبک برگردد</p></div>
<div class="m2"><p>از گرانخوابی منزل سفر از یادش رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جای رحم است بر آن طوطی کوتاه اندیش</p></div>
<div class="m2"><p>که ز شیرین سخنیها شکر از یادش رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان وحشی چه خیال است به تن برگردد؟</p></div>
<div class="m2"><p>رشته از زود گسستن گهر از یادش رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با تعلق نتوان سر به سلامت بردن</p></div>
<div class="m2"><p>آن سرآمد شود اینجا که سر از یادش رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قاصد سنگدل از کوی تو در برگشتن</p></div>
<div class="m2"><p>بس که آمد به تأنی خبر از یادش رفت؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم مست تو اگر هوش ز نقاش نبرد</p></div>
<div class="m2"><p>از چه تصویر دهان و کمر از یادش رفت؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای بسا سر که به دیوار زند از غفلت</p></div>
<div class="m2"><p>آن که در خانه تاریک در از یادش رفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل ز تنگی چه خیال است برآید بی آه؟</p></div>
<div class="m2"><p>غنچه ماند آن که نسیم سحر از یادش رفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست ممکن که به اندازه خورد می صائب</p></div>
<div class="m2"><p>می پرستی که خمار سحر از یادش رفت</p></div></div>