---
title: >-
    غزل شمارهٔ ۱۲۴۹
---
# غزل شمارهٔ ۱۲۴۹

<div class="b" id="bn1"><div class="m1"><p>در حقیقت پرتو منت کم از سیلاب نیست</p></div>
<div class="m2"><p>کلبه تاریک ما را حاجت مهتاب نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تهمت آسودگی بر دیده عاشق خطاست</p></div>
<div class="m2"><p>خانه ای کز خود برآرد آب، جای خواب نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آب عیش خویش را نتوان به گردش صاف کرد</p></div>
<div class="m2"><p>هیچ جا خاشاک بیش از دیده گرداب نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داغ حرمان لازم تن پروری افتاده است</p></div>
<div class="m2"><p>جای این اخگر به جز خاکستر سنجاب نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کیمیا ساز وجود خاکساران است فقر</p></div>
<div class="m2"><p>نافه را در پوست خونی غیر مشک ناب نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در گلستانی که زاغان نغمه پردازی کنند</p></div>
<div class="m2"><p>گوش گل را گوشواری بهتر از سیماب نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از خیال یار محرومند غفلت پیشگان</p></div>
<div class="m2"><p>ساغر این می به غیر از دیده بیخواب نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرگ را نتوان به رشوت از سر خود دور کرد</p></div>
<div class="m2"><p>این نهنگ جانستان را چشم بر اسباب نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در دیار ما که مذهب پرده دار مشرب است</p></div>
<div class="m2"><p>گوشه رندی ندارد هر که در محراب نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تشنه چشمان را ز نعمت سیر کردن مشکل است</p></div>
<div class="m2"><p>دشت اگر دریا شود ریگ روان سیراب نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سر برآورده است صائب دانه امید را</p></div>
<div class="m2"><p>در چنین عهدی که در چشم مروت آب نیست</p></div></div>