---
title: >-
    غزل شمارهٔ ۶۵۵۶
---
# غزل شمارهٔ ۶۵۵۶

<div class="b" id="bn1"><div class="m1"><p>بوالهوس از خط نظر پوشید زان روی چو ماه</p></div>
<div class="m2"><p>خط به چشم بی سوادان می کند عالم سیاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم از خط خارخار عشق من کمتر شود</p></div>
<div class="m2"><p>شد خطش دام تماشای دگر بهر نگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از غبار خط یکی صد گشت پیچ و تاب زلف</p></div>
<div class="m2"><p>شد علم انگشت زنهاری ز گرد این سپاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وصف کردم تا به ماه آن چهره را از سادگی</p></div>
<div class="m2"><p>از زمین تا آسمان ممنون من گردید ماه!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر گواهان لباسی گر چه نبود اعتماد</p></div>
<div class="m2"><p>ماه کنعان را بود بس چاک پیراهن گواه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تن به امداد خسیسان درمده چون ماه مصر</p></div>
<div class="m2"><p>کافکنند از قیمت نازل ترا دیگر به چاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سبکروحان گرانی می کند اندک غمی</p></div>
<div class="m2"><p>لنگر پرواز گردد دیده ها را برگ کاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که بر حرفم نهد انگشت، ریزد خون خویش</p></div>
<div class="m2"><p>کشته گردد مار کجرو چون گذارد پا به راه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دست بی ریزش فقیران را وبال گردن است</p></div>
<div class="m2"><p>ابر بی باران کند دلهای روشن را سیاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در بلا بودن بود صائب به از بیم بلا</p></div>
<div class="m2"><p>از هوا گیرد خموشی را چراغ صبحگاه</p></div></div>