---
title: >-
    غزل شمارهٔ ۶۵۵۵
---
# غزل شمارهٔ ۶۵۵۵

<div class="b" id="bn1"><div class="m1"><p>خامش گویا بود چشم سخنگوی تو</p></div>
<div class="m2"><p>نقطه بسم الله است خال بر ابروی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خال سیه فام تو مرکز وحدت بود</p></div>
<div class="m2"><p>دایره کثرت است سلسله موی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نعل در آتش نهد بر ورق برگ گل</p></div>
<div class="m2"><p>شبنم آسوده را شوق گل روی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پنجه مرجان کند شانه شمشاد را</p></div>
<div class="m2"><p>از دل خون گشتگان سلسله موی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عطسه پریشان کند مغز غزالان چین</p></div>
<div class="m2"><p>گر به ختا بگذرد نکهت گیسوی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پرده گوش مرا چون ورق لاله کرد</p></div>
<div class="m2"><p>از سخن آتشین لعل سخنگوی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نبرد شمع پیش پرتو رخساره ات</p></div>
<div class="m2"><p>شانه کند راه گم در خم گیسوی تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پرده بیگانگی چند بود در میان؟</p></div>
<div class="m2"><p>سوختم، از جیب گل چند کشم بوی تو؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا اثر از ماه نو بر ورق چرخ هست</p></div>
<div class="m2"><p>قبله صائب بود گوشه ابروی تو</p></div></div>