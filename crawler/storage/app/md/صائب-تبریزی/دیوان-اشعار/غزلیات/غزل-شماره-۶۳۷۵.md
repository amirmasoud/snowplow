---
title: >-
    غزل شمارهٔ ۶۳۷۵
---
# غزل شمارهٔ ۶۳۷۵

<div class="b" id="bn1"><div class="m1"><p>چون آفتاب و ماه نظر را بلند کن</p></div>
<div class="m2"><p>راهی که مشکل است ز همت سمند کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این راه دور بیش ز یک نعره وار نیست</p></div>
<div class="m2"><p>ای کمتر از سپند، صدایی بلند کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون می خورد ز شوق لقای تو جوی شیر</p></div>
<div class="m2"><p>از تنگنای نی سفری همچو قند کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این کارخانه ای است که خون شیر می شود</p></div>
<div class="m2"><p>هر چیز ناپسند تو باشد پسند کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این ناخنی که بر جگر ما فشرده ای</p></div>
<div class="m2"><p>از بهر امتحان به دل سنگ بندکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای آن که سنگ را به نظر لعل می کنی</p></div>
<div class="m2"><p>بخت مرا به نیم نظر ارجمند کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از دست خود مده طرف احتیاط را</p></div>
<div class="m2"><p>از گرگ بیشتر، حذر از گوسفند کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نقد دو کون در گره آستین توست</p></div>
<div class="m2"><p>بخت بلند خواهی، دستی بلند کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر کس به قدر همت خود کرد ریزشی</p></div>
<div class="m2"><p>صائب تو نیز دانه دل را سپند کن</p></div></div>