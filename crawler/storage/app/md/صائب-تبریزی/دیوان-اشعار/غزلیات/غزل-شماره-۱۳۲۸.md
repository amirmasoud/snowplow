---
title: >-
    غزل شمارهٔ ۱۳۲۸
---
# غزل شمارهٔ ۱۳۲۸

<div class="b" id="bn1"><div class="m1"><p>لنگر تن روح را نتواند از پرواز داشت</p></div>
<div class="m2"><p>موج دریا دیده را نتوان به ساحل بازداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقی ما در مروت هیچ خودداری نکرد</p></div>
<div class="m2"><p>نشأه انجام را در ساغر آغاز داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در جهان آب و گل، ویرانه ای از من نماند</p></div>
<div class="m2"><p>شغل خودسازی مرا از خانه سازی باز داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساعد سیمین او را تا کلیم الله دید</p></div>
<div class="m2"><p>نسخه افسوس شد دستی که در اعجاز داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من چه دارم در نظر تا جان به آسانی دهم؟</p></div>
<div class="m2"><p>کبک، باغ دلگشا از سینه شهباز داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عندلیب مست ما روزی که فارغبال بود</p></div>
<div class="m2"><p>هر طرف چندین کباب شعله آواز داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زنگ بر آیینه ام از قحط روشنگر نماند</p></div>
<div class="m2"><p>منت صیقل مرا محروم از پرداز داشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یاد ایامی که در دریای بی پایان عشق</p></div>
<div class="m2"><p>کشتی ما بادبان از پرده های راز داشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در غبار خط نهان چون دام زیر خاک شد</p></div>
<div class="m2"><p>زلف مشکینی که در هر موی چندین ناز داشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیش ازین صائب نمی آید ز من اخفای عشق</p></div>
<div class="m2"><p>شد مشبک پرده دل بس که پاس راز داشت</p></div></div>