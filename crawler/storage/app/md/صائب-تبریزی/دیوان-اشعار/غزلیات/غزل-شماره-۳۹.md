---
title: >-
    غزل شمارهٔ ۳۹
---
# غزل شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>در طلب سستی چو ارباب هوس کردن چرا؟</p></div>
<div class="m2"><p>راه دوری پیش داری، رو به پس کردن چرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکر دولت سایه بر بی سایگان افکندن است</p></div>
<div class="m2"><p>این همای خوش نشین را در قفس کردن چرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خراب آباد دنیای دنی چون عنکبوت</p></div>
<div class="m2"><p>تار و پود زندگی دام مگس کردن چرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در ره دوری که می باید نفس دریوزه کرد</p></div>
<div class="m2"><p>عمر صرف پوچ گویی چون جرس کردن چرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جستجوی گوهری کز دست بیرون می رود</p></div>
<div class="m2"><p>همچو غواصان به جان بی نفس کردن چرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پاس شان خویش بر اهل بصیرت لازم است</p></div>
<div class="m2"><p>چشمه سار شهد را دام مگس کردن چرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می شود فریادرس فریاد چون گردد تمام</p></div>
<div class="m2"><p>بخل در فریاد با فریادرس کردن چرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می توان تا مد آهی از پشیمانی کشید</p></div>
<div class="m2"><p>لوح دل را تخته مشق هوس کردن چرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جوش گل هر غنچه را منقار بلبل می کند</p></div>
<div class="m2"><p>در بهار زندگی از ناله بس کردن چرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همچو طفل خام در بستانسرای روزگار</p></div>
<div class="m2"><p>کام تلخ از میوه های نیمرس کردن چرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وحشت آباد جهان را منزلی در کار نیست</p></div>
<div class="m2"><p>آشیان آماده در کنج قفس کردن چرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر که پاک است از گناه، آسوده است از گیر و دار</p></div>
<div class="m2"><p>گر نه ای خائن، مدارا با عسس کردن چرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زندگانی با خسیسان می کند دل را سیاه</p></div>
<div class="m2"><p>آب حیوان را سبیل خار وخس کردن چرا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ترکش پر تیر از رنگین لباسی شد هدف</p></div>
<div class="m2"><p>همچو طفلان جامه رنگین هوس کردن چرا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در ره دوری که برق و باد را سوزد نفس</p></div>
<div class="m2"><p>خواب آسایش به امید جرس کردن چرا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در تجلی زار چون آیینه کوتاه بین</p></div>
<div class="m2"><p>اقتباس روشنایی از قبس کردن چرا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نفس بد کردار صائب قابل تعلیم نیست</p></div>
<div class="m2"><p>این سگ دیوانه را چندین مرس کردن چرا؟</p></div></div>