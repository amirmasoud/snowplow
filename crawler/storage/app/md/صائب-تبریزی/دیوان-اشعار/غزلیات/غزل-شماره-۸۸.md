---
title: >-
    غزل شمارهٔ ۸۸
---
# غزل شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>حسن چون آرد به جنگ دل سپاه خویش را</p></div>
<div class="m2"><p>بشکند بهر شگون اول کلاه خویش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوختم، چند از حجاب عشق دارم زیر لب</p></div>
<div class="m2"><p>چون الف در بسم پنهان مد آه خویش را؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا کی از تر دامنی در پرده باشی چون حباب؟</p></div>
<div class="m2"><p>می توان کردن به آهی پاک، راه خویش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می برد غم ره به سر وقت دل ما بی دلیل</p></div>
<div class="m2"><p>ابر نیسان می شناسد خانه خواه خویش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا قد موزون او را در خرام ناز دید</p></div>
<div class="m2"><p>کبک از حیرت فرامش کرد راه خویش را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رو نمی آرد به مهر و ماه تا آیینه هست</p></div>
<div class="m2"><p>می شناسد یار ما قدر نگاه خویش را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رهروی کز راه و رسم دردمندی آگه است</p></div>
<div class="m2"><p>گرد سر چون کعبه گردد سنگ راه خویش را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که نیش منت از ارباب همت خورده است</p></div>
<div class="m2"><p>به شمارد از گل مردم گیاه خویش را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این جواب آن غزل صائب که اهلی گفته است</p></div>
<div class="m2"><p>بر فلک هر شب رسانم برق آه خویش را</p></div></div>