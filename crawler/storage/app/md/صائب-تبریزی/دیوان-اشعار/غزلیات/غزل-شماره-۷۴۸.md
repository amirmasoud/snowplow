---
title: >-
    غزل شمارهٔ ۷۴۸
---
# غزل شمارهٔ ۷۴۸

<div class="b" id="bn1"><div class="m1"><p>پروای مرگ نیست گدای برهنه را</p></div>
<div class="m2"><p>سیل آب زندگی است سرای برهنه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایمن مشو به فقر ز اهل حسد که هست</p></div>
<div class="m2"><p>صد چشم بد ز آبله، پای برهنه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پوشیده دار فقر که سگ سیرتان دهر</p></div>
<div class="m2"><p>در پوست می فتند گدای برهنه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حسن از لباس شرم برآید گشاده روی</p></div>
<div class="m2"><p>در پرده نیست صبر، نوای برهنه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عریان شو از لباس که از بوی پیرهن</p></div>
<div class="m2"><p>تشریف می دهند صبای برهنه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی پاره جگر نبود آه را اثر</p></div>
<div class="m2"><p>از لشکرست فتح، لوای برهنه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگشا گره ز جبهه که هرگز نمی شود</p></div>
<div class="m2"><p>جوشن حجاب، تیغ قضای برهنه را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خورشید و مه به روز و شب از حله های نور</p></div>
<div class="m2"><p>آماده می کنند قبای برهنه را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دست از طمع بشوی که در آستین بود</p></div>
<div class="m2"><p>پیرایه قبول، دعای برهنه را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیداست با لباس پرستان چها کنند</p></div>
<div class="m2"><p>جمعی که می کنند قبای برهنه را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در آفتاب حشر نبیند برهنگی</p></div>
<div class="m2"><p>پوشیده است هر که گدای برهنه را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از عیب خلق چشم بپوشان که اهل شرم</p></div>
<div class="m2"><p>از چشم خود کنند قبای برهنه را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صائب برون ز سینه مده داغ عشق را</p></div>
<div class="m2"><p>ستار باش سوخته های برهنه را</p></div></div>