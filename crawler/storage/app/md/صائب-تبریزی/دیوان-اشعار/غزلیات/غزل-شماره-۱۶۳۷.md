---
title: >-
    غزل شمارهٔ ۱۶۳۷
---
# غزل شمارهٔ ۱۶۳۷

<div class="b" id="bn1"><div class="m1"><p>هر که آمد به جهان دست به دامان زد و رفت</p></div>
<div class="m2"><p>بر سر خشت عناصر دو سه جولان زد و رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخت کاری است برون آمدن از عهده رسم</p></div>
<div class="m2"><p>زین سبب بود که مجنون به بیابان زد و رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وقت آن خوش که درین راه نگردید گره</p></div>
<div class="m2"><p>سینه چون آبله بر خار مغیلان زد و رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم از رفتن ایام جوانی داغ است</p></div>
<div class="m2"><p>آه ازین برق که آتش به نیستان زد و رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که چون شبنم گل پاک شد از آلایش</p></div>
<div class="m2"><p>غوطه در چشمه خورشید درخشان زد و رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل من آب شد از غیرت اقبال حباب</p></div>
<div class="m2"><p>که به یک چشم زدن غوطه به عمان زد و رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داغ ما چشم به الماس نگرداند سیاه</p></div>
<div class="m2"><p>زخم ما تیغ تغافل به نمکدان زد و رفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که از چشمه تیغ تو دمی آب کشید</p></div>
<div class="m2"><p>خاک در دیده سرچشمه حیوان زد و رفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بلبل ما به دل نازک گل رحم نکرد</p></div>
<div class="m2"><p>آتش از شعله آواز به بستان زد و رفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مژه بر هم نزد از خواب اجل دیده ما</p></div>
<div class="m2"><p>این نمک را که به این زخم نمایان زد و رفت؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از پریشانی ما یاد کجا خواهد کرد؟</p></div>
<div class="m2"><p>دل که بر کوچه آن زلف پریشان زد و رفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وقت آن راهروی خوش که ازین خارستان</p></div>
<div class="m2"><p>دست چون برق جهانسوز به دامان زد و رفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>غم لشکر خور اگر پادشهی می خواهی</p></div>
<div class="m2"><p>مور این زمزمه بر گوش سلیمان زد و رفت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر نسیمی که برآورد سر از جیب عدم</p></div>
<div class="m2"><p>بر دل سوخته ما دو سه دامان زد و رفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جگر اهل سخن از نفس صائب سوخت</p></div>
<div class="m2"><p>آه ازین شمع که آتش به شبستان زد و رفت</p></div></div>