---
title: >-
    غزل شمارهٔ ۱۱۸۶
---
# غزل شمارهٔ ۱۱۸۶

<div class="b" id="bn1"><div class="m1"><p>بی اجابت آه مرغ آشیان گم کرده ای است</p></div>
<div class="m2"><p>ناله بی فریادرس تیر نشان گم کرده ای است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر عزیزی را که می بینم درین آشوبگاه</p></div>
<div class="m2"><p>یوسف خود در میان کاروان گم کرده ای است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در نیابد هر که چون پروانه ذوق سوختن</p></div>
<div class="m2"><p>در دل دوزخ بهشت جاودان گم کرده ای است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که در آغاز خط از گلرخان غافل شود</p></div>
<div class="m2"><p>در بهاران عندلیب گلستان گم کرده ای است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این علف خواران که هر یک جانبی دارند روی</p></div>
<div class="m2"><p>گله در دامن صحرا شبان گم کرده ای است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی نصیبان جستجوی رزق بیجا می کنند</p></div>
<div class="m2"><p>نیست چون قسمت، طلب دست دهان گم کرده ای است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل که در زلف پریشان تو می جوید قرار</p></div>
<div class="m2"><p>در شب تاریک مرغ آشیان گم کرده ای است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رهنوردی را که نبود رهبر ثابت قدم</p></div>
<div class="m2"><p>در بیابان طلب سنگ نشان گم کرده ای است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زیر گردون هر که را می بینم از صاحبدلان</p></div>
<div class="m2"><p>دست و پا در زیر تیغ بی امان گم کرده ای است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در صراط المستقیم عشق، عقل خرده بین</p></div>
<div class="m2"><p>در دل شب راه در ریگ روان گم کرده ای است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر دل بی درد کز درد طلب افتاد دور</p></div>
<div class="m2"><p>از نفس گیری درای کاروان گم کرده ای است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر که بهر دانه گردد گرد این سنگین دلان</p></div>
<div class="m2"><p>شاهراه آسیای آسمان گم کرده ای است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر جوانمردی که سر می پیچد از فرمان پیر</p></div>
<div class="m2"><p>در صف مردان کماندار کمان گم کرده ای است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن که دارد موی آتش دیده را در پیچ و تاب</p></div>
<div class="m2"><p>خویش را چون تاب در موی میان گم کرده ای است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر که غافل از ظهور حق بود در ممکنات</p></div>
<div class="m2"><p>بوی یوسف در میان کاروان گم کرده ای است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هست در گم کردن خود، هست اگر آرامشی</p></div>
<div class="m2"><p>هر که خود را یافت مرغ آشیان گم کرده ای است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نیست هر کس را که صائب نفس در فرمان عقل</p></div>
<div class="m2"><p>بر فراز توسن سرکش عنان گم کرده ای است</p></div></div>