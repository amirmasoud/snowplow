---
title: >-
    غزل شمارهٔ ۱۱۰۱
---
# غزل شمارهٔ ۱۱۰۱

<div class="b" id="bn1"><div class="m1"><p>تا ز رخ زلف آن بهشتی روی دور انداخته است</p></div>
<div class="m2"><p>دست رضوان پرده بر رخسار حور انداخته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پنجه مومین حریف پنجه خورشید نیست</p></div>
<div class="m2"><p>عقل بیجا پنجه با عشق غیور انداخته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می برد خواهی نخواهی دل ز دست مردمان</p></div>
<div class="m2"><p>کار خود را آن کمان ابرو به زور انداخته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساعد او بارها در معرض عرض صفا</p></div>
<div class="m2"><p>رعشه غیرت بر اندام بلور انداخته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در حریم عشق، خواهش ناامیدی بردهد</p></div>
<div class="m2"><p>زان تجلی پرتو خود را به طور انداخته است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راه نزدیک است اگر بر گرد دل گردد کسی</p></div>
<div class="m2"><p>دوربینی ها مرا از کعبه دور انداخته است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ابر تر دامن چه باشد، کز حجاب اشک من</p></div>
<div class="m2"><p>خویش را طوفان مکرر در تنور انداخته است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تیره بختی های ما از پستی اقبال نیست</p></div>
<div class="m2"><p>از بلندی شمع ما پرتو به دور انداخته است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه همین در شهر اصفاهان قیامت می کند</p></div>
<div class="m2"><p>فکر صائب در همه آفاق شور انداخته است</p></div></div>