---
title: >-
    غزل شمارهٔ ۶۴۳۲
---
# غزل شمارهٔ ۶۴۳۲

<div class="b" id="bn1"><div class="m1"><p>از داغ بود چهره افروخته من</p></div>
<div class="m2"><p>گردد ز شرر زنده دل سوخته من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون آتش سوزان ز طرب نیست، که باشد</p></div>
<div class="m2"><p>از سیلی صرصر رخ افروخته من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون لاله ز می نیست مرا سرخی رخسار</p></div>
<div class="m2"><p>خون است شراب جگرسوخته من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پوشیدن چشم است مرا خانه صیاد</p></div>
<div class="m2"><p>غافل مشو از باز نظردوخته من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا چشم کند کار، سیه خانه لیلی است</p></div>
<div class="m2"><p>در دامن صحرای دل سوخته من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فریاد که چون غنچه پی سوختن دل</p></div>
<div class="m2"><p>شد مشت شراری زراندوخته من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب کند از سایه خود وحشت صیاد</p></div>
<div class="m2"><p>رم کرده غزالی که شد آموخته من</p></div></div>