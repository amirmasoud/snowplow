---
title: >-
    غزل شمارهٔ ۷۵۱
---
# غزل شمارهٔ ۷۵۱

<div class="b" id="bn1"><div class="m1"><p>چون پای خم به دست فتادت کمر گشا</p></div>
<div class="m2"><p>چون گرم شد سرت ز می ناب، سر گشا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از هر که دل گشوده نگردد کناره گیر</p></div>
<div class="m2"><p>چون غنچه در به روی نسیم سحر گشا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از مردمان سرد نفس تیره می شوی</p></div>
<div class="m2"><p>آیینه پیش مردم صاحب نظر گشا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قانع به رنگ و بوی گل بی وفا مشو</p></div>
<div class="m2"><p>بر روی آفتاب چو شبنم نظر گشا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از سر هوای پوچ برون چون حباب کن</p></div>
<div class="m2"><p>چون موج در میانه دریا کمر گشا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان پیشتر که بر دل مردم گران شوی</p></div>
<div class="m2"><p>استادگی مکن، پر و بال سفر گشا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون موج، پشت دست به کف زن درین محیط</p></div>
<div class="m2"><p>آغوش چون صدف به هوای گهر گشا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زخم گشاده رو به بغل تیغ را کشید</p></div>
<div class="m2"><p>آغوش رغبتی تو هم ای بی جگر گشا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا بر تو خوشگوار شود بستن نظر</p></div>
<div class="m2"><p>یک ره نظر به عالم پر شور و شر گشا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باطل مکن به سیر و تماشا نگاه خویش</p></div>
<div class="m2"><p>زنهار صائب از سر عبرت نظر گشا</p></div></div>