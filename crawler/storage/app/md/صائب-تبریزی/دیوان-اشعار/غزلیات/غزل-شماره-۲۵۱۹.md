---
title: >-
    غزل شمارهٔ ۲۵۱۹
---
# غزل شمارهٔ ۲۵۱۹

<div class="b" id="bn1"><div class="m1"><p>رخنه سیل اشک من در سد اسکندر کند</p></div>
<div class="m2"><p>خون گرمم ریشه در فولاد چون جوهر کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مهر خاموشی چه سازد با دل بیتاب من؟</p></div>
<div class="m2"><p>سنگ خارا را شرار شوخ من مجمر کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از حدیث تلخ ناصح شد گرانتر خواب من</p></div>
<div class="m2"><p>بادبان را کشتی پربار من لنگر کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حاصل تن پروری غیر از گداز روح نیست</p></div>
<div class="m2"><p>چربی پهلوی گوهر رشته را لاغر کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مبتلای شش جهت را چاره جز تسلیم نیست</p></div>
<div class="m2"><p>رخنه زور نقش هیهات است در ششدر کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سینه چون بی آرزو شد روضه جنت شود</p></div>
<div class="m2"><p>دل چو گردد آب، کار چشمه کوثر کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناقصان را خلق خوش سازد ز ارباب کمال</p></div>
<div class="m2"><p>در نظرها عیب خامی را هنر عنبر کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آرزوی آب حیوانش شود صورت پذیر</p></div>
<div class="m2"><p>روی در آیینه زانو گر اسکندر کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حرص صائب تلخ دارد زندگانی را به مور</p></div>
<div class="m2"><p>ورنه اکسیر قناعت خاک را شکر کند</p></div></div>