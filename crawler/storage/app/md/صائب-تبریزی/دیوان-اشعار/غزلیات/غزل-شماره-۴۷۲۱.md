---
title: >-
    غزل شمارهٔ ۴۷۲۱
---
# غزل شمارهٔ ۴۷۲۱

<div class="b" id="bn1"><div class="m1"><p>ای زلفت از کمند تمنابلند تر</p></div>
<div class="m2"><p>مژگان ز زلف و زلف ز بالا بلندتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند عمر رشته شود کوته از گره</p></div>
<div class="m2"><p>زلف تو شد ز عقده دلها بلندتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از سر هوای عشق به سعی سفر نرفت</p></div>
<div class="m2"><p>این شعله شد ز دامن صحرا بلندتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرگز گلی به سر نزدم از ریاض وصل</p></div>
<div class="m2"><p>پیوسته بود دست من از پا بلندتر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در تنگنای قطره بسر چون برد کسی؟</p></div>
<div class="m2"><p>با مشربی ز گردن مینا بلندتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چند نارسایی طالع فزون شود</p></div>
<div class="m2"><p>گردد زبان عرض تمنا بلندتر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پوشیده است یوسف ما از فریب ناز</p></div>
<div class="m2"><p>پیراهنی ز دست زلیخا بلندتر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد دام زیر خاک ز گرد و غبار خط</p></div>
<div class="m2"><p>زلفی که بود از شب یلدا بلندتر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا چند خاکمال اسیران دهی، بس است</p></div>
<div class="m2"><p>خطت شد از غبار دل ما بلندتر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صائب شکست اگر چه بود سرمه صدا</p></div>
<div class="m2"><p>شد از شکست دل سخن ما بلندتر</p></div></div>