---
title: >-
    غزل شمارهٔ ۵۰۸
---
# غزل شمارهٔ ۵۰۸

<div class="b" id="bn1"><div class="m1"><p>تا به حدی است لطافت رخ پرتابش را</p></div>
<div class="m2"><p>که عرق داغ کند لاله سیرابش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا به دامان قیامت نشود چشمش خشک</p></div>
<div class="m2"><p>یک نظر هر که ببیند گل سیرابش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وحشت از صحبت مجنون نکند چشم غزال</p></div>
<div class="m2"><p>می توان یافت گرفته است رگ خوابش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر فتد راه به دریای دلم طوفان را</p></div>
<div class="m2"><p>حلقه گوش کند حلقه گردابش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کعبه و بتکده بی جلوه مستانه یار</p></div>
<div class="m2"><p>آسیایی است که انداخته اند آبش را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جوهر آن مژه صائب زره زیر قباست</p></div>
<div class="m2"><p>این چنین ساده مبین تیغ سیه تابش را</p></div></div>