---
title: >-
    غزل شمارهٔ ۳۹۷۷
---
# غزل شمارهٔ ۳۹۷۷

<div class="b" id="bn1"><div class="m1"><p>گرسنه چشم کجا سیر از نوال شود</p></div>
<div class="m2"><p>که بر حریص لب نان لب سؤال شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خار و خس نتوان سیر کرد آتش را</p></div>
<div class="m2"><p>که حرص خواجه یکی صد ز جمع مال شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز خون صید حرم رنگ تیغ او نگرفت</p></div>
<div class="m2"><p>کجا به گردن او خون من وبال شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مریز آب رخ خود که در کنار محیط</p></div>
<div class="m2"><p>صدف ز بی گهریها کف سؤال شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نرفت زنگ ملال از دلم به باده ناب</p></div>
<div class="m2"><p>ز آب سبزه محال است پایمال شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امیدها به خطش داشتم ندانستم</p></div>
<div class="m2"><p>که روز من شب ازان عنبرین هلال شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غرور حسن ز خط بیش شد که دارد یاد</p></div>
<div class="m2"><p>که حاکم از رقم عزل مستمال شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی که خیمه برون زد ز خویش چون مجنون</p></div>
<div class="m2"><p>سیاه خیمه اش از دیده غزال شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تأمل آینه فکر را کند روشن</p></div>
<div class="m2"><p>که آب صائب از استادگی زلال شود</p></div></div>