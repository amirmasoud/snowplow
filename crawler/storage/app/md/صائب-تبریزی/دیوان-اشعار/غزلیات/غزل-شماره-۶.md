---
title: >-
    غزل شمارهٔ ۶
---
# غزل شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>خط نمی سازد مرا زان لعل جان پرور جدا</p></div>
<div class="m2"><p>تشنه کی گردد به تیغ موج از کوثر جدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سبزه خط لعل سیراب ترا بی آب کرد</p></div>
<div class="m2"><p>آب را هر چند نتوان کرد از گوهر جدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دل خونگرم ما پیکان کشیدن مشکل است</p></div>
<div class="m2"><p>چون توان کردن دو یکدل را ز یکدیگر جدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می کند روز سیه بیگانه یاران را ز هم</p></div>
<div class="m2"><p>خضر در ظلمات می گردد ز اسکندر جدا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا نسوزد آرزو در دل، نگردد سینه صاف</p></div>
<div class="m2"><p>زنگ از آیینه می گردد به خاکستر جدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زندگی را بی حلاوت می کند موی سفید</p></div>
<div class="m2"><p>شیر در یک کاسه اینجا باشد از شکر جدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چاره من مرهم کافوری صبح است و بس</p></div>
<div class="m2"><p>من که دارم بر جگر داغی ز هر اختر جدا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مهر زر هم از دل دنیاپرستان می رود</p></div>
<div class="m2"><p>سکته می گردد به زور دست اگر از زر جدا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهره از آمیزش نیکان ندارد بد که هست</p></div>
<div class="m2"><p>در میان جمع، فرد باطل از دفتر جدا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برنیارد کثرت مردم ز تنهایی مرا</p></div>
<div class="m2"><p>در میان لشکرم چون رایت از لشکرجدا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بعد عمری گر برآرم سر ز کنج آشیان</p></div>
<div class="m2"><p>می شود تیغ دودم در کشتنم هر پر جدا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گوی چوگان حوادث گردد از بی لنگری</p></div>
<div class="m2"><p>از سر زانوی فکر آن را که باشد سر جدا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آتشی از شوق هر کس را که باشد زیر پا</p></div>
<div class="m2"><p>چون سپند از ناله ای گردد ازین مجمر جدا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قطره در اندیشه دریا چو باشد، عین اوست</p></div>
<div class="m2"><p>نیست مسکن دل به دوری گردد از دلبر جدا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حال دل دور از عقیق آتشین او مپرس</p></div>
<div class="m2"><p>این کباب تر به خون دل شد از اخگر جدا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ریشه غم برنیاورد از دلم جام شراب</p></div>
<div class="m2"><p>صیقل از آیینه صائب چون کند جوهر جدا؟</p></div></div>