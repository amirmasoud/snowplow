---
title: >-
    غزل شمارهٔ ۴۳۹
---
# غزل شمارهٔ ۴۳۹

<div class="b" id="bn1"><div class="m1"><p>ز اسرار حقیقت بهره ور کن عشقبازی را</p></div>
<div class="m2"><p>به طفلان واگذار این ابجد عشق مجازی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به استغنای مجنون حسن لیلی برنمی آید</p></div>
<div class="m2"><p>که ناز نازنینان است در سر، بی نیازی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر داری دل پاکی درآ در حلقه مستان</p></div>
<div class="m2"><p>که اینجا آبرویی نیست دامان نمازی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خمار درد نوشان را می ناصاف می باید</p></div>
<div class="m2"><p>توان در خاکساری یافت ذوق خاکبازی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به چشم دور گردان جلوه دیگر کند منزل</p></div>
<div class="m2"><p>شکوه کعبه باشد در نظر کمتر حجازی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به صد افسانه عمر ابد کوته نمی گردد</p></div>
<div class="m2"><p>مگر از زلف او دارد شب هجران درازی را؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گل روی بتان از آه من شد آتشین صائب</p></div>
<div class="m2"><p>ز من دارد نسیم صبح این گلشن طرازی را</p></div></div>