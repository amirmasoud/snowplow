---
title: >-
    غزل شمارهٔ ۶۵۵۷
---
# غزل شمارهٔ ۶۵۵۷

<div class="b" id="bn1"><div class="m1"><p>نفس ظلمانی نمی دارد محابا از گناه</p></div>
<div class="m2"><p>نیست پروا طفل زنگی را ز پستان سیاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از هوا گیرند بی مغزان حدیث پوچ را</p></div>
<div class="m2"><p>کهربا را می پرد چشم از برای برگ کاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می کند دل را سیه نور چراغ عاریت</p></div>
<div class="m2"><p>نیست ممکن شستن داغ کلف از روی ماه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زینهار از کنج عزلت پای خود بیرون منه</p></div>
<div class="m2"><p>کز بها افتاد یوسف تا برون آمد ز چاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست در پایان عمر از رعشه پیران را گزیر</p></div>
<div class="m2"><p>بر فروغ خویش می لرزد چراغ صبحگاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سجده شکرش به دامان قیامت می کشد</p></div>
<div class="m2"><p>هر که را صائب شود آن طاق ابرو قبله گاه</p></div></div>