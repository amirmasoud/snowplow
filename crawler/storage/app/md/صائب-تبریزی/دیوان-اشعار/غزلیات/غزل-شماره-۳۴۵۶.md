---
title: >-
    غزل شمارهٔ ۳۴۵۶
---
# غزل شمارهٔ ۳۴۵۶

<div class="b" id="bn1"><div class="m1"><p>نیستم گل که مرا برگ نثاری باشد</p></div>
<div class="m2"><p>تحفه سوختگان مشت شراری باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باغ من دامن دشت است و حصارم سر کوه</p></div>
<div class="m2"><p>من نه آنم که مرا باغ و حصاری باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غنچه آبله ام، برگ قناعت دارم</p></div>
<div class="m2"><p>روزی من ز دو عالم سر خاری باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تیره روزان جهان را به چراغی دریاب</p></div>
<div class="m2"><p>تا پس از مرگ ترا شمع مزاری باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل داغی که ازو سینه ندزدی امروز</p></div>
<div class="m2"><p>در شبستان کفن لاله عذاری باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خس و خاری که ز راه دگران برداری</p></div>
<div class="m2"><p>در دل خاک ترا باغ و بهاری باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به شمار نفس افتاد ترا کار و ز حرص</p></div>
<div class="m2"><p>هر سر موی تو مشغول به کاری باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زنده در گور کند حشر مکافات ترا</p></div>
<div class="m2"><p>بر دل موری اگر از تو غباری باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عشق بیهوده سر تربیت او دارد</p></div>
<div class="m2"><p>صائب آن نیست که شایسته کاری باشد</p></div></div>