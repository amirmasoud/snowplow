---
title: >-
    غزل شمارهٔ ۴۴۶۲
---
# غزل شمارهٔ ۴۴۶۲

<div class="b" id="bn1"><div class="m1"><p>دل را به زلف پرچین تسخیر می توان کرد</p></div>
<div class="m2"><p>این شیر را به مویی زنجیر می توان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خط نرسته پیداست از چهره نکویان</p></div>
<div class="m2"><p>مو را چگونه پنهان در شیر می توان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چند صد بیابان وحشی تر از غزالیم</p></div>
<div class="m2"><p>ما را به گوشه چشم تسخیر می توان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بحر تشنه چشمان لب خشک بازگردند</p></div>
<div class="m2"><p>آیینه را ز دیدار کی سیر می توان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را خراب حالی از رعشه خمارست</p></div>
<div class="m2"><p>از درد باده ما را تعمیر می توان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در چشم خرده بینان هر نقطه صد کتاب است</p></div>
<div class="m2"><p>آن خال را به صد وجه تفسیر می توان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بوته ریاضت یک چند اگر گذاری</p></div>
<div class="m2"><p>قلب وجود خودرا اکسیر می توان کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر گوش هوش باشد در پرده خموشی</p></div>
<div class="m2"><p>صد داستان شکایت تقریر می توان کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فریاد کاهل دولت از نخوتند غافل</p></div>
<div class="m2"><p>کز خلق خوش چه دلها تسخیر می توان کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی منصبی ز تغییر ایمن بود وگرنه</p></div>
<div class="m2"><p>هر منصب دگر هست تغییر می توان کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اوضاع خوش خیالان سامان پذیر گردد</p></div>
<div class="m2"><p>گر خواب شاعران را تعبیر می توان کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از درد عشق اگر هست صائب ترا نصیبی</p></div>
<div class="m2"><p>از ناله در دل سنگ تأثیر می توان کرد</p></div></div>