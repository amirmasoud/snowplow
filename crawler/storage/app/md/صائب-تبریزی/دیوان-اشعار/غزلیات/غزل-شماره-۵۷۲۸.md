---
title: >-
    غزل شمارهٔ ۵۷۲۸
---
# غزل شمارهٔ ۵۷۲۸

<div class="b" id="bn1"><div class="m1"><p>منم که فیض شراب از کتاب می گیرم</p></div>
<div class="m2"><p>به همت از گل کاغذ گلاب می گیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هوای عالم آب است سازگار مرا</p></div>
<div class="m2"><p>دل از پیاله و جان از شراب می گیرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آتش است صراحی ز صبح خیزی من</p></div>
<div class="m2"><p>همیشه چشم قدح را به خواب می گیرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز چشم مست بتان چشم مردمی دارم</p></div>
<div class="m2"><p>چه ظالمم که خراج از خراب می گیرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشاط رفته ز عمر گذشته می خواهم</p></div>
<div class="m2"><p>حساب موج از بحر سراب می گیرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به زور بازوی همت چو لعل در دل سنگ</p></div>
<div class="m2"><p>فروغ تربیت از آفتاب می گیرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر به گوش صدف صائب این غزل خوانم</p></div>
<div class="m2"><p>هزار دامن در خوشاب می گیرم</p></div></div>