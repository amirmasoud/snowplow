---
title: >-
    غزل شمارهٔ ۴۹۲۰
---
# غزل شمارهٔ ۴۹۲۰

<div class="b" id="bn1"><div class="m1"><p>می تراشم رزق خود چون تیر از پهلوی خویش</p></div>
<div class="m2"><p>می کنم تا هست ممکن حفظ آب روی خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بار منت برنمی تابد تن آزادگان</p></div>
<div class="m2"><p>بید مجنون را لباسی نیست غیر از موی خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزی بی رنج گردد تخم رنج بی شمار</p></div>
<div class="m2"><p>وقت آن کس خوش که باشد رزقش از بازوی خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون مگس ناخوانده هر کس برسرخوانی رود</p></div>
<div class="m2"><p>ای بسا سیلی به دست خود زند بر روی خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرکه راچون تیغ باشد آب باریکی به جوی</p></div>
<div class="m2"><p>می کند چون موج از دریا تهی پهلوی خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکوه خونین تراوش می کند بی اختیار</p></div>
<div class="m2"><p>نیست ممکن در گره چون نافه بستن بوی خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می تواند چهره مقصود رابی پرده دید</p></div>
<div class="m2"><p>هرکه رو آورد در آیینه زانوی خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرکه چین تنگ خلقی از جبین بیرون نکرد</p></div>
<div class="m2"><p>متصل در زیر شمشیرست از ابروی خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نامه اش چون نامه صبح است در محشر سفید</p></div>
<div class="m2"><p>هر که از اشک ندامت دادشست و شوی خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می کشم هر لحظه صائب آه افسوسی زدل</p></div>
<div class="m2"><p>یک نفس فارغ نمی گردم ز رفت و روی خویش</p></div></div>