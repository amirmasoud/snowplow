---
title: >-
    غزل شمارهٔ ۱۳۲۰
---
# غزل شمارهٔ ۱۳۲۰

<div class="b" id="bn1"><div class="m1"><p>در دل پر خون غبار لشکر اندیشه نیست</p></div>
<div class="m2"><p>گرد را دست تصرف بر درون شیشه نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کار چون گویاست، بیکارست اظهار کمال</p></div>
<div class="m2"><p>کوهکن را ترجمانی چون زبان تیشه نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>محنت دنیا نمی گردد به گرد بیخودان</p></div>
<div class="m2"><p>هست سهم شیر حاضر، شیر اگر در بیشه نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می کند گرد یتیمی آب گوهر را زیاد</p></div>
<div class="m2"><p>حسن بالا دست را از گرد خط اندیشه نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که خواهد گو برآرد گرد از بنیاد ما</p></div>
<div class="m2"><p>این درخت خشک را دلبستگی با ریشه نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دل ما ره ندارد عقل و تدبیرات او</p></div>
<div class="m2"><p>عاشقان را جز پری در شیشه اندیشه نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به که صائب از خرابات فلک بیرون رویم</p></div>
<div class="m2"><p>در خور این باده پر زور، اینجا شیشه نیست</p></div></div>