---
title: >-
    غزل شمارهٔ ۲۹۵۲
---
# غزل شمارهٔ ۲۹۵۲

<div class="b" id="bn1"><div class="m1"><p>هنرور را هنر گرد غم از دل برنمی دارد</p></div>
<div class="m2"><p>که پروای لب خشک صدف گوهر نمی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلیل جوهر ذاتی است دلجویی ضعیفان را</p></div>
<div class="m2"><p>که هر تیغی که باشد کند، سوزن بر نمی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر خواهی دل روشن به آه گرم زور آور</p></div>
<div class="m2"><p>که این آیینه غیر از آه، روشنگر نمی دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برآ از خویشتن گر شهپر پرواز می خواهی</p></div>
<div class="m2"><p>که تا در بیضه باشد مرغ بال و پر نمی دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زنقش آرزو ساده است لوح سینه عاشق</p></div>
<div class="m2"><p>که چون آیینه گردد صیقلی جوهر نمی دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لب میگون او را نیل چشم زخم باشد خط</p></div>
<div class="m2"><p>وگرنه آتش یاقوت خاکستر نمی دارد</p></div></div>