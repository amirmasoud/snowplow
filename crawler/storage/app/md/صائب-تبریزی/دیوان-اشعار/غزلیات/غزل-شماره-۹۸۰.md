---
title: >-
    غزل شمارهٔ ۹۸۰
---
# غزل شمارهٔ ۹۸۰

<div class="b" id="bn1"><div class="m1"><p>از نسیم آن زلف مشک افشان سبک جولانترست</p></div>
<div class="m2"><p>از صدف آن غنچه سیراب خوش دندانترست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه زلف عنبرین پر پیچ و تاب افتاده است</p></div>
<div class="m2"><p>پیش ما نازک خیالان آن کمر پیچانترست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست هر چند از لباس گل جدایی رنگ را</p></div>
<div class="m2"><p>جامه گلرنگ بر اندام او چسبانترست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لطف معنی را لباس لفظ رسوا می کند</p></div>
<div class="m2"><p>در ته پیراهن آن سیمین بدن عریانترست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پرده داری می کند شرم از عرق آن چهره را</p></div>
<div class="m2"><p>ورنه صد پیراهن از گل روی او خندانترست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر چه از آیینه آتش زیر پا دارد گهر</p></div>
<div class="m2"><p>بر جبین او عرق بسیار خوش جولانترست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست زیر حلقه های زلف غیر از خال یار</p></div>
<div class="m2"><p>مرکز شوخی که از پرگار سرگردانترست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرد میدان نیست طوطی، ورنه از صد رهگذر</p></div>
<div class="m2"><p>صفحه آن روی از آیینه خوش میدانترست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قوت گیرایی شهباز در سرپنجه است</p></div>
<div class="m2"><p>زود می چسبد به دل چشمی که خوش مژگانترست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پرده شرم و نقاب عصمتی در کار نیست</p></div>
<div class="m2"><p>چشم ما صد پرده از قربانیان حیرانترست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون ز آتش می شود پشت کمان سخت نرم</p></div>
<div class="m2"><p>در سر مستی چرا آن شوخ نافرمانترست؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ناله صاحبدلان را بیشتر باشد اثر</p></div>
<div class="m2"><p>رخنه در خارا کند تیری که خوش پیکانترست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در طلب ما بی زبانان امت پروانه ایم</p></div>
<div class="m2"><p>سوختن از عرض مطلب پیش ما آسانترست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از تهیدستی شود امید صاحب دستگاه</p></div>
<div class="m2"><p>حرص نان بیش است پیری را که بی دندانترست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا زبان حال را فهمیده ایم از فیض عشق</p></div>
<div class="m2"><p>غنچه از منقار بلبل پیش ما نالانترست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از سر منصور شور عشق کی بیرون رود؟</p></div>
<div class="m2"><p>از سر دار فنا بسیار بی سامانترست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ما رگ ابر بهاران را مکرر دیده ایم</p></div>
<div class="m2"><p>خامه صائب به صد معنی گهر افشانترست</p></div></div>