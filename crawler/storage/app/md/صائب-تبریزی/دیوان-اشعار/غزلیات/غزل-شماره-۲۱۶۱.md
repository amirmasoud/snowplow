---
title: >-
    غزل شمارهٔ ۲۱۶۱
---
# غزل شمارهٔ ۲۱۶۱

<div class="b" id="bn1"><div class="m1"><p>در روی زمین یک سر پر شور نمانده است</p></div>
<div class="m2"><p>ته جرعه ای از کاسه منصور نمانده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنگار گرفته است دل اهل جهان را</p></div>
<div class="m2"><p>در آینه هیچ نظر نور نمانده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان مصر حلاوت که شکر بود غبارش</p></div>
<div class="m2"><p>امروز به جز نقش پی مور نمانده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیمانه ارباب تنعم شده لبریز</p></div>
<div class="m2"><p>آوازه ای از کاسه فغفور نمانده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تلخی دشنام برون رفته حلاوت</p></div>
<div class="m2"><p>نزدیکی دل با نگه دور نمانده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان شهد که سرمایه شیرینی جان بود</p></div>
<div class="m2"><p>صائب به جز از نشتر زنبور نمانده است</p></div></div>