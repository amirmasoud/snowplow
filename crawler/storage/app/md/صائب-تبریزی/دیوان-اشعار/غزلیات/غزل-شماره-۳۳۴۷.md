---
title: >-
    غزل شمارهٔ ۳۳۴۷
---
# غزل شمارهٔ ۳۳۴۷

<div class="b" id="bn1"><div class="m1"><p>ای دل از دشمن خاموش حذر باید کرد</p></div>
<div class="m2"><p>از گزند می بی جوش حذر باید کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیشتر کار کند تیغ چو لنگر دارست</p></div>
<div class="m2"><p>از دعای لب خاموش حذر باید کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر جگرداری دزدست شب ماه گواه</p></div>
<div class="m2"><p>از خط و خال بناگوش حذر باید کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تیر از بحر کمان می کند انشای سفر</p></div>
<div class="m2"><p>چون رسد کار به آغوش حذر باید کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فقر پوشیده و شمشیر برهنه است یکی</p></div>
<div class="m2"><p>از فقیران قباپوش حذر باید کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صائب از جوش سخن داد به طوفان عالم</p></div>
<div class="m2"><p>از محیطی که زند جوش حذر باید کرد</p></div></div>