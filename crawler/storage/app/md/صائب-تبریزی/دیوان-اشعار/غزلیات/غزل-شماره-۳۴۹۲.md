---
title: >-
    غزل شمارهٔ ۳۴۹۲
---
# غزل شمارهٔ ۳۴۹۲

<div class="b" id="bn1"><div class="m1"><p>دردمندان که به ناخن جگر خود خستند</p></div>
<div class="m2"><p>چشمه خویش به دریای بقا پیوستند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود حسابان که کشیدند به دیوان خود را</p></div>
<div class="m2"><p>در همین نشأه ز آشوب قیامت رستند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاکیانی که به معماری تن کوشیدند</p></div>
<div class="m2"><p>در ره آب بقا سد سکندر بستند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه بغیر از نفس سوخته حاصل دارند؟</p></div>
<div class="m2"><p>دانه هایی که درین شوره زمین پا بستند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عمر در ماتم احباب به افسوس مبر</p></div>
<div class="m2"><p>شکر کن شکر کز این خواب پریشان جستند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سنگ بر کعبه زنان شیشه خود می شکنند</p></div>
<div class="m2"><p>وای بر سنگدلانی که دلی را خستند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عرق چهره خورشید جهانتاب شوند</p></div>
<div class="m2"><p>شبنمی چند که در دامن گل ننشستند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می توانند به یک حمله دو صد قلب شکست</p></div>
<div class="m2"><p>همچو ابرو دو سر آمد چو بهم پیوستند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دامن وصل شکر در کف جمعی افتاد</p></div>
<div class="m2"><p>که چو نی در جگر خاک کمر را بستند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای خوش آن مایه درستان که ز بی آزاری</p></div>
<div class="m2"><p>هیچ دل غیر دل خسته خود نشکستند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب از خلق جدا باش که موران ضعیف</p></div>
<div class="m2"><p>مار گشتند به ظاهر چو به هم پیوستند</p></div></div>