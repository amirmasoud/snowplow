---
title: >-
    غزل شمارهٔ ۶۰۱۹
---
# غزل شمارهٔ ۶۰۱۹

<div class="b" id="bn1"><div class="m1"><p>پیش هر تلخی نریزم آبروی خویشتن</p></div>
<div class="m2"><p>می خورم قند از شکست آرزوی خویشتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رشته این تنگ چشمان رنج باریک آورد</p></div>
<div class="m2"><p>می کنم از جسم زار خود رفوی خویشتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می فشارم، گر به حرف شکوه بگشاید دهن</p></div>
<div class="m2"><p>چون سبو دستی که دارم بر گلوی خویشتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در کف آیینه چون سیماب می لرزد به خویش؟</p></div>
<div class="m2"><p>آنچنان لرزد دلم بر آبروی خویشتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فارغم چون طوطی از حسن گلوسوز شکر</p></div>
<div class="m2"><p>من که شکر می خورم از گفتگوی خویشتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در غریبی چاره گرد یتیمی چون کنم؟</p></div>
<div class="m2"><p>من که در دریا ندارم شستشوی خویشتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیش آن پاکیزه دامن، خانه نارفته ام</p></div>
<div class="m2"><p>گر چه عمرم صرف شد در رفت و روی خویشتن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست ممکن این کشاکش از رگ جانم رود</p></div>
<div class="m2"><p>تا نپیوندم به دریا آب جوی خویشتن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا شدم چون نافه دور از ناف آهوی ختن</p></div>
<div class="m2"><p>می فرستم قاصدی هر دم ز بوی خویشتن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون به رنگ زرد من بر می خورد برگ خزان</p></div>
<div class="m2"><p>زعفران می مالد از خجلت به روی خویشتن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بی خبر از پیچ و تاب هم سیه روزان نیند</p></div>
<div class="m2"><p>می توان پرسید حال ما ز موی خویشتن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بارها نومید برگشتم ز دکان مسیح</p></div>
<div class="m2"><p>به که خود باشم به همت چاره جوی خویشتن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون غبارآلود می گردم ز خواب بی غمی</p></div>
<div class="m2"><p>تازه می سازم به خون دل وضوی خویشتن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بس که صائب خویش را در عشق او گم کرده ام</p></div>
<div class="m2"><p>می کنم از همنشینان جستجوی خویشتن</p></div></div>