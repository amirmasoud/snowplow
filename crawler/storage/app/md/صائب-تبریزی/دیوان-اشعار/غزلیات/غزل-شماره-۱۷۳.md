---
title: >-
    غزل شمارهٔ ۱۷۳
---
# غزل شمارهٔ ۱۷۳

<div class="b" id="bn1"><div class="m1"><p>در بهشت افکند آن رخسار گندم گون مرا</p></div>
<div class="m2"><p>شست یاد کوثر از دل آن لب میگون مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تماشای رخش چون چشم بردارم، که هست</p></div>
<div class="m2"><p>چهره گلرنگ او گیرنده تر از خون مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خط آزادی طمع زان روی نوخط داشتم</p></div>
<div class="m2"><p>سرخط مشق جنون شد آن خط شبگون مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خشک می آید به چشم سرو چون سوهان روح</p></div>
<div class="m2"><p>ریشه در دل کرد تا آن قامت موزون مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک سیه خانه است چشم لیلی از صحرای او</p></div>
<div class="m2"><p>سر به صحرا داده است آن کس که چون مجنون مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شیشه گو گردنکشی کن، جام گو ناساز باش</p></div>
<div class="m2"><p>کز خمار آورد بیرون آن لب میگون مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرد کلفت گر به خاطر این چنین زور آورد</p></div>
<div class="m2"><p>می دهد در خاک آخر غوطه چون قارون مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از جهان آب و گل عمری است بیرون رفته ام</p></div>
<div class="m2"><p>خم نمی سازد حصاری همچو افلاطون مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تنگنای شهر نتواند مرا دلتنگ داشت</p></div>
<div class="m2"><p>وسعت مشرب بود پیشانی هامون مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست احسان، بنده کردن مردم آزاده را</p></div>
<div class="m2"><p>بخل منعم می کند بیش از کرم ممنون مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سرمه دان دریاکشان را برنیارد از خمار</p></div>
<div class="m2"><p>شد خمار لیلی از چشم غزال افزون مرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صید وحشت دیده ام صائب به تنهایی خوشم</p></div>
<div class="m2"><p>می توان کردن به رو گرداندنی ممنون مرا</p></div></div>