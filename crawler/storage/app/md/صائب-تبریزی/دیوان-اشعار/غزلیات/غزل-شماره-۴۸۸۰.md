---
title: >-
    غزل شمارهٔ ۴۸۸۰
---
# غزل شمارهٔ ۴۸۸۰

<div class="b" id="bn1"><div class="m1"><p>شمع بر خاک شهیدان گر نباشد گو مباش</p></div>
<div class="m2"><p>لاله در کوه بدخشان گر نباشد گو مباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سبزه تیغ تو می‌باید که باشد تازه‌روی</p></div>
<div class="m2"><p>باغ ما را شبنم جان گر نباشد گو مباش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرش ما افتادگی، اسباب ما آزادگی</p></div>
<div class="m2"><p>خانه ما را نگهبان گر نباشد گو مباش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اشتها چون سوخت، دارد لذت مرغ کباب</p></div>
<div class="m2"><p>خوان ما را مرغ بریان گر نباشد گو مباش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شوربختی وقت حاجت می‌کند کار نمک</p></div>
<div class="m2"><p>سفره ما را نمکدان گر نباشد گو مباش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما که چون دل گوشه‌ای داریم از گلزار قدس</p></div>
<div class="m2"><p>دامن صحرای امکان گر نباشد گو مباش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی‌سرانجامی غبار لشکر جمعیت است</p></div>
<div class="m2"><p>روزگار ما به سامان گر نباشد گو مباش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرکب آزادگان تخت روان بی‌خودی است</p></div>
<div class="m2"><p>توسن گردون به فرمان گر نباشد گو مباش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زینب ظاهر چه کار آید دل افسرده را؟</p></div>
<div class="m2"><p>نقش بر دیوار زندان گر نباشد گو مباش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این قدر دل‌بستگی صائب به زلف یار چیست؟</p></div>
<div class="m2"><p>نسخه خواب پریشان گر نباشد گو مباش</p></div></div>