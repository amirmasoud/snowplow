---
title: >-
    غزل شمارهٔ ۲۱۵۱
---
# غزل شمارهٔ ۲۱۵۱

<div class="b" id="bn1"><div class="m1"><p>عشق است که اکسیر بقا خاک در اوست</p></div>
<div class="m2"><p>از هر دو جهان سیر شدن ماحضر اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق است همایی که سعادت نظر اوست</p></div>
<div class="m2"><p>افشاندن بال از دو جهان بال و پر اوت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چند ندارد صدف آن گوهر نایاب</p></div>
<div class="m2"><p>هر دل که شود آب، محیط گهر اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر چند که در رخنه دل گوشه نشین است</p></div>
<div class="m2"><p>گردون یکی از حلقه به گوشان در اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چند که چون سرو روان میوه ندارد</p></div>
<div class="m2"><p>امید جهان سایه نشین شجر اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چند که دل قطره خونی است ازین بحر</p></div>
<div class="m2"><p>سرسبزی افلاک ز آب گهر اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دستی که در آغوش هوس حلقه نگردد</p></div>
<div class="m2"><p>گستاخ تر از زلف به موی کمر اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از سینه هر کس شنوی ناله زاری</p></div>
<div class="m2"><p>از خویش برون آی که آواز در اوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی عشق، دل از هر دو جهان سرد نگردد</p></div>
<div class="m2"><p>این فیض ز تأثیر نسیم سحر اوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از حوصله هر دو جهان، گرد برآرد</p></div>
<div class="m2"><p>این نشأه که در ساغر اول نظر اوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مویی که شود سلسله گردن شیران</p></div>
<div class="m2"><p>در حلقه زنار میانان کمر اوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر تار ز پیراهن فانوس کمندی است</p></div>
<div class="m2"><p>گستاخی پروانه نه از بال و پر اوست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در بیخودی آویز که در عالم هستی</p></div>
<div class="m2"><p>سود دو جهان در سفر بی خطر اوست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صائب خبر یوسف گم کرده خود را</p></div>
<div class="m2"><p>از بی خبری پرس که صاحب خبر اوست</p></div></div>