---
title: >-
    غزل شمارهٔ ۶۳۰۸
---
# غزل شمارهٔ ۶۳۰۸

<div class="b" id="bn1"><div class="m1"><p>مخور ز حرف خنک بر دماغ سوختگان</p></div>
<div class="m2"><p>حذر کن از دل پر درد و دماغ سوختگان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمی شود ز سیه خانه لیلی ما دور</p></div>
<div class="m2"><p>همیشه زیر سیاهی است داغ سوختگان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز جام لاله سیراب شد مرا روشن</p></div>
<div class="m2"><p>که می ز خویش برآرد ایاغ سوختگان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهار تازه کند داغ تخم سوخته را</p></div>
<div class="m2"><p>ز می شکفته نگردد دماغ سوختگان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز لاله زار دلم تا شکفت دانستم</p></div>
<div class="m2"><p>که مرهم دگران است داغ سوختگان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز نور زنده دلی آب زندگی خورده است</p></div>
<div class="m2"><p>ز باد صبح نمیرد چراغ سوختگان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه نعمتی است ندارند بیغمان صائب</p></div>
<div class="m2"><p>خبر ز چاشنی درد و داغ سوختگان</p></div></div>