---
title: >-
    غزل شمارهٔ ۵۸۳۶
---
# غزل شمارهٔ ۵۸۳۶

<div class="b" id="bn1"><div class="m1"><p>خندان به زیر تیغ تغافل نشسته ایم</p></div>
<div class="m2"><p>در خارزار بر ورق گل نشسته ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از انفعال،خار بیابان وحشتیم</p></div>
<div class="m2"><p>چون خار اگر چه در قدم گل نشسته ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زیر سپهر پای به دامن کشیده ایم</p></div>
<div class="m2"><p>در فصل نوبهار ته پل نشسته ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون نوبهار جلوه ما یک دو هفته است</p></div>
<div class="m2"><p>بر دامن گل و پر بلبل نشسته ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون شبنم گداخته بر روی دست گل</p></div>
<div class="m2"><p>آماده هزار تزلزل نشسته ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از شرم ناکسی جگر خویش می خوریم</p></div>
<div class="m2"><p>بر شاخ گل اگر چه چو بلبل نشسته ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از چشم نرم خلق ز بس زخم خورده ایم</p></div>
<div class="m2"><p>بر روی برگ گل به تأمل نشسته ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب ز فکر زلف پریشان آن نگار</p></div>
<div class="m2"><p>آشفته تر ز طره سنبل نشسته ایم</p></div></div>