---
title: >-
    غزل شمارهٔ ۵۹۱۱
---
# غزل شمارهٔ ۵۹۱۱

<div class="b" id="bn1"><div class="m1"><p>کو بخت که در میکده با یار نشینم؟</p></div>
<div class="m2"><p>در ماتم غمهای جگر خوار نشینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مانند حباب از دل می سر بدر آرم</p></div>
<div class="m2"><p>با نغمه به یک پرده و یک تار نشینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر مصلحت عقل کم از کوه غمی نیست</p></div>
<div class="m2"><p>کو رطل گرانی که سبکبار نشینم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حسن رخ گل چشم به راه نگه ماست</p></div>
<div class="m2"><p>از همت پست است که با خار نشینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آه این چه حجاب است که از شرم رخ تو</p></div>
<div class="m2"><p>در خانه خود روی به دیوار نشینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صائب چه کنی منع من از عاشقی و شعر؟</p></div>
<div class="m2"><p>اینها به ازان نیست که بیکار نشینم؟</p></div></div>