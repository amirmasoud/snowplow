---
title: >-
    غزل شمارهٔ ۶۷۳۷
---
# غزل شمارهٔ ۶۷۳۷

<div class="b" id="bn1"><div class="m1"><p>جام هیهات است از صهبا کند پهلو تهی</p></div>
<div class="m2"><p>این حبابی نیست کز دریا کند پهلو تهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آستانش سجده گاه سرفرازان می شود</p></div>
<div class="m2"><p>هر که چون محراب از دنیا کند پهلو تهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رفت بر باد فنا در یک نفس عمر حباب</p></div>
<div class="m2"><p>این سزای آن که از دریا کند پهلو تهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دامن نقصان مده از کف که چون گردد تمام</p></div>
<div class="m2"><p>مه ز خورشید جهان آرا کند پهلو تهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهل بینش (را) گزیر از سختی ایام نیست</p></div>
<div class="m2"><p>این شراری نیست کز خارا کند پهلو تهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زین گرانجانان که کوه قاف ازیشان شد سبک</p></div>
<div class="m2"><p>وقت آن کس خوش که چون عنقا کند پهلو تهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در گلستانی که اشک ما سراسر می رود</p></div>
<div class="m2"><p>لاله از شبنم ز استغنا کند پهلو تهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از شبیخون اجل صائب نگردد مضطرب</p></div>
<div class="m2"><p>هر که پیش از مرگ از دنیا کند پهلو تهی</p></div></div>