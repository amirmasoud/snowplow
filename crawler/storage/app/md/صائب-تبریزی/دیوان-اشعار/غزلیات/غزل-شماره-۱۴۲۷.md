---
title: >-
    غزل شمارهٔ ۱۴۲۷
---
# غزل شمارهٔ ۱۴۲۷

<div class="b" id="bn1"><div class="m1"><p>پرده شب بود ایام شبابی که مراست</p></div>
<div class="m2"><p>رگ سنگ است ز غفلت رگ خوابی که مراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارد از کوی خرابات مرا مستغنی</p></div>
<div class="m2"><p>از دل و دیده شرابی و کبابی که مراست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست در جستن درمان دل کم حوصله را</p></div>
<div class="m2"><p>در طلبکاری درد تو شتابی که مراست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با لب خشک کند شکر تراوش از من</p></div>
<div class="m2"><p>پرده آب حیات است سرابی که مراست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون نبندم کمر خصمی این هستی پوچ؟</p></div>
<div class="m2"><p>گره خاطر بحرست حبابی که مراست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برده است از دل من وحشت تنهایی را</p></div>
<div class="m2"><p>با خیال تو سؤالی و جوابی که مراست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست زان طرف بناگوش، در گوش ترا</p></div>
<div class="m2"><p>از تماشای رخت چشم پر آبی که مراست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که افتاد، ز افتادگی ایمن گردد</p></div>
<div class="m2"><p>چه کند سیل به دیوار خرابی که مراست؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست با دیده بیدار تن آسانان را</p></div>
<div class="m2"><p>با شکرخواب فراغت شکرابی که مراست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خضر را می کند از چشمه حیوان دلسرد</p></div>
<div class="m2"><p>از دم تیغ شهادت دم آبی که مراست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می کند زود حساب من و هستی را پاک</p></div>
<div class="m2"><p>همچو صبح این نفس پا به رکابی که مراست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نکند آتش خونگرم اگر دلسوزی</p></div>
<div class="m2"><p>کیست تا خشک کند اشک کبابی که مراست؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عشرت نسیه روشن گهران نقد من است</p></div>
<div class="m2"><p>در رگ تاک زند جوش، شرابی که مراست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>روزی مرغ چمن از گل شبنم زده نیست</p></div>
<div class="m2"><p>زان عذار عرق آلود گلابی که مراست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چه ضرورست بر اوراق جهان گردیدن؟</p></div>
<div class="m2"><p>در نظر از دل صد پاره کتابی که مراست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از شمار نفس خویش نگردم غافل</p></div>
<div class="m2"><p>هر نفس نقد بود روز حسابی که مراست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نیست ممکن که نشوید ز دلم گرد ملال</p></div>
<div class="m2"><p>صائب از طبع روان این لب آبی که مراست</p></div></div>