---
title: >-
    غزل شمارهٔ ۵۰
---
# غزل شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>دل چسان پیچد عنان آه دردآلود را؟</p></div>
<div class="m2"><p>ز آتش سوزان عنانداری نیاید دود را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تشنگی در خواب ممکن نیست کم گردد ز آب</p></div>
<div class="m2"><p>نیست سیرابی ز خون آن چشم خواب آلود را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از نصیحت خشکی سودا نگردد بر طرف</p></div>
<div class="m2"><p>برنیارد آتش سوزان ز خامی عود را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مردم کم مایه را اسراف برق خرمن است</p></div>
<div class="m2"><p>حفظ کن از پوچ گویی ها دم معدود را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وقت بی برگی شود گوهرفشان از اشک، تاک</p></div>
<div class="m2"><p>تنگدستی مانع ریزش نگردد جود را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حلقه خط، چشم حیران شد به دور عارضش</p></div>
<div class="m2"><p>آه ازین آتش که در زنجیر دارد دود را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سبز گردد از بناگوش بتان پیش از ذقن</p></div>
<div class="m2"><p>خط عنبرفام، حسن عاقبت محمود را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شیشه خشک است صائب در مذاقش آب خضر</p></div>
<div class="m2"><p>هر که بوسیده است لبهای شراب آلود را</p></div></div>