---
title: >-
    غزل شمارهٔ ۱۶۸
---
# غزل شمارهٔ ۱۶۸

<div class="b" id="bn1"><div class="m1"><p>نیست از دشمن محابا یک سر سوزن مرا</p></div>
<div class="m2"><p>کز دل سخت است در زیر قبا جوشن مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چه را خورشید سوزد، برنیاید دود ازو</p></div>
<div class="m2"><p>نه ز بی دردی بود از آه لب بستن مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با دل روشن، ز نور عاریت مستغنیم</p></div>
<div class="m2"><p>گل فتد از مهر و مه در دیده روزن مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داشتم چندین گل بی خار چشم از سادگی</p></div>
<div class="m2"><p>زخم خاری هم نشد روزی ازین گلشن مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کمین دارد پریشان خاطری جمعیتم</p></div>
<div class="m2"><p>پر برون آرد چو موران، دانه در خرمن مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با تهیدستی درین دریای گوهر چون صدف</p></div>
<div class="m2"><p>صد یتیم از اشک افتاده است در دامن مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از هوای تر شود آیینه ام تاریک تر</p></div>
<div class="m2"><p>هیچ باغ دلگشایی نیست چون گلخن مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از نسیم شکر، ناف آهوی مشکین کنم</p></div>
<div class="m2"><p>از دهان شیر سازد چرخ اگر مسکن مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از نفس هر چند چون عیسی روان بخشم به خلق</p></div>
<div class="m2"><p>آب می باید گرفت از چشمه سوزن مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از زبان آتشینم گر چه محفل روشن است</p></div>
<div class="m2"><p>نیست چون شمع از تهیدستی دو پیراهن مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر چه دارم تازه، روی باغ را در بر گریز</p></div>
<div class="m2"><p>نیست چون سرو از تهیدستی دو پیراهن مرا</p></div></div>