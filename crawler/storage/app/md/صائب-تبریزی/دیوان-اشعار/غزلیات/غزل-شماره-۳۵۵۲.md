---
title: >-
    غزل شمارهٔ ۳۵۵۲
---
# غزل شمارهٔ ۳۵۵۲

<div class="b" id="bn1"><div class="m1"><p>عشق لب تشنه بدمستی اظهار بود</p></div>
<div class="m2"><p>گل این باغچه شیدایی دستار بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پاس دام و قفس خویش بدار ای صیاد</p></div>
<div class="m2"><p>ناله سوختگان خونی منقار بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عزت غنچه این باغ به گلچین فرض است</p></div>
<div class="m2"><p>که نظر کرده آن گوشه دستار بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل غبار غم او را ز هوا می گیرد</p></div>
<div class="m2"><p>آب آیینه ما تشنه زنگار بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جنس اگر یوسف مصری است، که ارزان گردد</p></div>
<div class="m2"><p>ناز اگر از طرف میل خریدار بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صائب از لطف سخن گل به سر شهرت زد</p></div>
<div class="m2"><p>مپسندید که در پیرهنش خار بود</p></div></div>