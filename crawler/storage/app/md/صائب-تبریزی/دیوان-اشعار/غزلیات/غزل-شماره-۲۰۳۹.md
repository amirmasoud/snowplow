---
title: >-
    غزل شمارهٔ ۲۰۳۹
---
# غزل شمارهٔ ۲۰۳۹

<div class="b" id="bn1"><div class="m1"><p>در چار باغ دهر نسیم مراد نیست</p></div>
<div class="m2"><p>از ششدر جهات، امید گشاد نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در راه ابر، تخم تمنا نکشته ام</p></div>
<div class="m2"><p>کشت مرا ملاحظه از برق و باد نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آسودگی ز عمر سبکرو طمع مدار</p></div>
<div class="m2"><p>بر گردباد و سایه او اعتماد نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن را که جذب عشق برون آرد از وطن</p></div>
<div class="m2"><p>چون ماه مصر در گرو خیرباد نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در مکتبی که ساده دلان مشق می کنند</p></div>
<div class="m2"><p>رخسار صفحه نقش پذیر مداد نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در عهد شیب، شکوه نسیان چرا کنم؟</p></div>
<div class="m2"><p>کم نعمتی است این که جوانی به یاد نیست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب تلاش صحبت پروانه می کند</p></div>
<div class="m2"><p>بیتابی چراغ ز سیلی باد نیست</p></div></div>