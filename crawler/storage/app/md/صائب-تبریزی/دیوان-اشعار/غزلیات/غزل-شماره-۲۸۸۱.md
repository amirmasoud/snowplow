---
title: >-
    غزل شمارهٔ ۲۸۸۱
---
# غزل شمارهٔ ۲۸۸۱

<div class="b" id="bn1"><div class="m1"><p>بجز چشمش که چشم از دیدن من از حیا بندد</p></div>
<div class="m2"><p>کدامین آشنا دیدی که در بر آشنا بندد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبندد دسته گل در گلستانها کمر دیگر</p></div>
<div class="m2"><p>میان خویش را چون تنگ آن گلگون قبا بندد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بیداری نمی آید زشوخی بر زمین پایش</p></div>
<div class="m2"><p>مگر مشاطه در خواب آن پریرورا حنا بندد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به روی تازه چون گل تازه رو داریم گلشن را</p></div>
<div class="m2"><p>نمی بندد کمر هر کس کمر در خون ما بندد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لباس فقر بر خاکی نهادان زود می چسبد</p></div>
<div class="m2"><p>که آسان بر زمین نرم نقش بوریا بندد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زخواری و مذلت نیست پرواکامجویان را</p></div>
<div class="m2"><p>که چندین عیب بر خود از طمعکاری گدا بندد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دهان خود زحرف نیک و بد می بایدش بستن</p></div>
<div class="m2"><p>به خود هر کس که می خواهد دهان خلق را بندد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به زودی زان نمی گردد مزلف ساده روی من</p></div>
<div class="m2"><p>که حیرت سبزه خط را ره نشو و نما بندد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بغیر از ناله افسوس حاصل نیست از عمرم</p></div>
<div class="m2"><p>سزای آن که دل بر کاروانی چون درا بندد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شود رزق هما گر استخوان من، زبیتابی</p></div>
<div class="m2"><p>عجب دارم دگر در استخوان مغز هما بندد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زتیغ غمزه دل در سینه افگار، صائب را</p></div>
<div class="m2"><p>دو نیم از بهر آن شد تا در آن زلف دو تا بندد</p></div></div>