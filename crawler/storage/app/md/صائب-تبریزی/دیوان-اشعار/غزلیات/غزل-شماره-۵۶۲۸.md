---
title: >-
    غزل شمارهٔ ۵۶۲۸
---
# غزل شمارهٔ ۵۶۲۸

<div class="b" id="bn1"><div class="m1"><p>چند از ساده دلی زخم هوس بردارم؟</p></div>
<div class="m2"><p>به که این آینه از پیش نفس بردارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من که چون برگ خزان بام و پرم ریخته است</p></div>
<div class="m2"><p>به چه امید دل از کنج قفس بردارم؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آفت حرص ز شمشیر دو دم بیشترست</p></div>
<div class="m2"><p>چون دو دست از سر خود همچو مگس بردارم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل به دشمن نزنند اهل مروت، ورنه</p></div>
<div class="m2"><p>من نه آنم که زبونی ز عسس بردارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راه خوابیده ز بیدار دلان می گردد</p></div>
<div class="m2"><p>دست اگر از دهن خود چو جرس بردارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق خواهد ز هوس کرد سبکبار مرا</p></div>
<div class="m2"><p>از ره سیل چه افتاده که خس بردارم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنقدر مهلت از ایام توقع دارم</p></div>
<div class="m2"><p>که از آیینه دل زنگ هوس بردارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان بود بستر و بالین من از گل چو نسیم</p></div>
<div class="m2"><p>که خس و خار ز راه همه کس بردارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در شبستان جهان روشن از آنم چون صبح</p></div>
<div class="m2"><p>که غبار از دل عالم به نفس بردارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بلبلی نیست درین باغ ز من قانعتر</p></div>
<div class="m2"><p>فیض آغوش گل از چاک قفس بردارم</p></div></div>