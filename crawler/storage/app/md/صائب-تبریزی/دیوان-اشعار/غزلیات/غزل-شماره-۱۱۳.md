---
title: >-
    غزل شمارهٔ ۱۱۳
---
# غزل شمارهٔ ۱۱۳

<div class="b" id="bn1"><div class="m1"><p>نیست از روی زمین سیری دل خود کام را</p></div>
<div class="m2"><p>حرص می گردد زیاد از خاک، چشم دام را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داغ دارد میکشان را تشنه چشمی های من</p></div>
<div class="m2"><p>می کنم خالی ز می در دست ساقی جام را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزگار عیش را دود سپندی لازم است</p></div>
<div class="m2"><p>شد شب آدینه نیل چشم زخم ایام را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل به کوشش آرزو را پخته نتوانست کرد</p></div>
<div class="m2"><p>در بغل نتوان رساندن میوه های خام را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که را از درد و صاف می نظر بر نشأه است</p></div>
<div class="m2"><p>باده یک جام داند بوسه و دشنام را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جسم رنگ جان گرفت از بی قراری های دل</p></div>
<div class="m2"><p>می برد چون سایه با خود صید وحشی دام را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در دل خود کعبه مقصود را هر کس که یافت</p></div>
<div class="m2"><p>بستن زنار داند بستن احرام را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بوسه را در نامه می پیچد برای دیگران</p></div>
<div class="m2"><p>آن که می دارد دریغ از عاشقان پیغام را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل چو شد افسرده، از جسم گرانجان پاره ای است</p></div>
<div class="m2"><p>رنگ برگ خویش باشد میوه های خام را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست صائب شنبه و آدینه در کوی مغان</p></div>
<div class="m2"><p>می کند یکرنگ، مشرب سر به سر ایام را</p></div></div>