---
title: >-
    غزل شمارهٔ ۷۱۷
---
# غزل شمارهٔ ۷۱۷

<div class="b" id="bn1"><div class="m1"><p>در گردش آورید می لعل فام را</p></div>
<div class="m2"><p>زین بیش خشک لب مپسندید جام را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تاچون شفق مدام رخت لاله گون بود</p></div>
<div class="m2"><p>بی باده مگذران چو فلک صبح و شام را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غافل مشو که وقت شناسان نوبهار</p></div>
<div class="m2"><p>چون لاله بر زمین ننهادند جام را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کس به خون دل ز می ناب صلح کرد</p></div>
<div class="m2"><p>محکم گرفت دامن عیش مدام را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آمد ز زیر سنگ برون هر دلی که ریخت</p></div>
<div class="m2"><p>بر خاک، میوه های تمنای خام را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دادیم عارفانه چو منصور تن به دار</p></div>
<div class="m2"><p>کردیم نقد، روضه دارالسلام را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر تیغ کوه سینه فشارد ز انفعال</p></div>
<div class="m2"><p>کبکی که آورد به نظر آن خرام را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنجا که دوربینی رشک است، عاشقان</p></div>
<div class="m2"><p>امساک می کنند ز جانان پیام را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل را به زور عشق رهاندیم از بدن</p></div>
<div class="m2"><p>با خود به زیر خاک نبردیم دام را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عیب من از شمار برون است و از حساب</p></div>
<div class="m2"><p>صائب ز چشم خلق بپوشم کدام را؟</p></div></div>