---
title: >-
    غزل شمارهٔ ۶۰۲۸
---
# غزل شمارهٔ ۶۰۲۸

<div class="b" id="bn1"><div class="m1"><p>می شود نقل مجالس چون شود شیرین سخن</p></div>
<div class="m2"><p>همچو خون پنهان نمی ماند چو شد رنگین سخن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تأمل می شود شایسته تحسین سخن</p></div>
<div class="m2"><p>پیچ و تاب فکر سازد غنچه را رنگین سخن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که را آن غمزه خونریز در دل بگذرد</p></div>
<div class="m2"><p>تا قیامت می تراود از لبش خونین سخن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سبزه نورس چسان آید برون از زیر سنگ؟</p></div>
<div class="m2"><p>از لب لعلش برون آید به آن تمکین سخن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طوطیان را زنگ خواهد بست در منقار حرف</p></div>
<div class="m2"><p>خوار گردد در نظرها گر به این آیین سخن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آهوی چین کاسه دریوزه سازد ناف را</p></div>
<div class="m2"><p>گر ز زلف و کاکل او بگذرد در چین سخن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با کمال تیره روزی می شود عالم فروز</p></div>
<div class="m2"><p>آه اگر می داشت شمعی بر سر بالین سخن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کوته اندیشی است میل چشم بینایی مرا</p></div>
<div class="m2"><p>ورنه دارد در گره هر نقطه ای چندین سخن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تلخی ایام را صائب شکر در چاشنی است</p></div>
<div class="m2"><p>طفل چون از شیر لب شوید، شود شیرین سخن</p></div></div>