---
title: >-
    غزل شمارهٔ ۱۵۲۸
---
# غزل شمارهٔ ۱۵۲۸

<div class="b" id="bn1"><div class="m1"><p>هر که از قافله کعبه جدا افتاده است</p></div>
<div class="m2"><p>کارش از راهنمایان به خدا افتاده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رهبر حق طلبان روشنی راه بس است</p></div>
<div class="m2"><p>ساده لوح آن که پی راهنما افتاده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دلیل غلط آن کس که زند لاف وصول</p></div>
<div class="m2"><p>بسته چشمی است که در چه به عصا افتاده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرنوشت دو جهان ابجد طفلانه اوست</p></div>
<div class="m2"><p>هر که را آینه دل به صفا افتاده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن حبابم که درین بحر ز بی مغزی ها</p></div>
<div class="m2"><p>عقده در کار من از کسب هوا افتاده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حذر از سایه خود می کنم از بیم زوال</p></div>
<div class="m2"><p>سایه تا بر سرم از بال هما افتاده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من نه آنم که کنم راز محبت را فاش</p></div>
<div class="m2"><p>صفحه روی تو اندیشه نما افتاده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل معنی بود از نازکی لفظم خون</p></div>
<div class="m2"><p>همچو می، شیشه من هوش ربا افتاده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر کند عار ز نزدیکی ما حسن غیور</p></div>
<div class="m2"><p>عینک دیده ما دورنما افتاده است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا به خشک و تر ازین دایره قانع شده ایم</p></div>
<div class="m2"><p>بحر و کان از دل و از دیده ما افتاده است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سبزی بخت بود شمع سر بالینش</p></div>
<div class="m2"><p>هر که در سایه سرو تو ز پا افتاده است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ادب عشق مرا مهر دهن گردیده است</p></div>
<div class="m2"><p>ورنه لعل لب تو بوسه ربا افتاده است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حلقه در گوش کشد شیردلان را صائب</p></div>
<div class="m2"><p>هر که در حلقه مردان خدا افتاده است</p></div></div>