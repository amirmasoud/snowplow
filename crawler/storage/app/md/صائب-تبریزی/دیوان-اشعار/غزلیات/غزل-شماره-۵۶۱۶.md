---
title: >-
    غزل شمارهٔ ۵۶۱۶
---
# غزل شمارهٔ ۵۶۱۶

<div class="b" id="bn1"><div class="m1"><p>عمرها تربیت دیده بینا کردم</p></div>
<div class="m2"><p>تا ترا یک نظر از دور تماشا کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخنه از آه در آن دل نتوانستم کرد</p></div>
<div class="m2"><p>من که صد غنچه پیکان به نفس وا کردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر قدر خون که به دلها طلب دنیا کرد</p></div>
<div class="m2"><p>من ز گرداندن رو در دل دنیا کردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشد از ابر گهر بار صدف را روزی</p></div>
<div class="m2"><p>آنچه من جمع ز دریوزه دلها کردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زور سیلاب به همواری صحرا چه کند؟</p></div>
<div class="m2"><p>خاک در کاسه دشمن به مدارا کردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منم آن غنچه غافل ز بی حوصلگی</p></div>
<div class="m2"><p>سر خود در سر یک خنده بیجا کردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از گل آتش به ته پای چو شبنم دارم</p></div>
<div class="m2"><p>تا هوای سفر عالم بالا کردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نفرت از دیدن مکروه یکی صد گردد</p></div>
<div class="m2"><p>نیست از رغبت اگر روی به دنیا کردم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نفس از موج خطر راست نکردم صائب</p></div>
<div class="m2"><p>سر برون تا چو حباب از دل دریا کردم</p></div></div>