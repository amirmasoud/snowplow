---
title: >-
    غزل شمارهٔ ۱۰۷۲
---
# غزل شمارهٔ ۱۰۷۲

<div class="b" id="bn1"><div class="m1"><p>خرقه آزادگان چشم از جهان پوشیدن است</p></div>
<div class="m2"><p>کسوت این قوم از دستار سر پیچیدن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخت رویی می شود سنگ فسان شمشیر را</p></div>
<div class="m2"><p>خاکساری روی دشمن بر زمین مالیدن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از تهی مغزی است امید گشاد از ماه عید</p></div>
<div class="m2"><p>ناخن تنها برای پشت سر خاریدن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما حجاب آلودگان را جرأت پروانه نیست</p></div>
<div class="m2"><p>گرد سر گردیدن ما، گرد دل گردیدن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرفرازی چشم بد بسیار دارد در کمین</p></div>
<div class="m2"><p>تا بود روشن، مدار شمع بر لرزیدن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عرض دادن جنس خود بر مردم بالغ نظر</p></div>
<div class="m2"><p>در ترازوی قیامت خویش را سنجیدن است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صرف کردن زندگی در خدمت آزادگان</p></div>
<div class="m2"><p>زیر پای سرو چون آب روان غلطیدن است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از گداز شمع روشن شد که در بزم وجود</p></div>
<div class="m2"><p>روزی روشندلان انگشت خود خاییدن است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در محرم تا چه خونها در دل مردم کند</p></div>
<div class="m2"><p>محنت آبادی که عیدش دربدر گردیدن است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برگ جمعیت به از ریزش ندارد حاصلی</p></div>
<div class="m2"><p>گر گلابی هست این گل را، ز هم پاشیدن است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سنگ طفلان می کند خوش وقت مجنون مرا</p></div>
<div class="m2"><p>کار کبک مست در کوه و کمر خندیدن است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از خموشی می توان صائب به معنی راه برد</p></div>
<div class="m2"><p>مایه غواص گوهرجو نفس دزدیدن است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خواب را صائب مکن بر دیده از شبگیر تلخ</p></div>
<div class="m2"><p>چاره کوتاهی این ره به خود پیچیدن است</p></div></div>