---
title: >-
    غزل شمارهٔ ۳۸۱۷
---
# غزل شمارهٔ ۳۸۱۷

<div class="b" id="bn1"><div class="m1"><p>ازین بساط کسی شادمانه برخیزد</p></div>
<div class="m2"><p>که از سر دو جهان عارفانه برخیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مدار دست ز دامان آه نیمشبی</p></div>
<div class="m2"><p>که دل ز جای به این تازیانه برخیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اثر ز عاشق صادق درین جهان مطلب</p></div>
<div class="m2"><p>که گرد راست روان از نشانه برخیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مآل تفرقه جمعیت است آخر کار</p></div>
<div class="m2"><p>دل دو نیم به محشر یگانه برخیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قدم برون منه از شارع میانه روی</p></div>
<div class="m2"><p>که از کنار غم بیکرانه برخیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز درد هر سر مو بر تنم زبانی شد</p></div>
<div class="m2"><p>به قدر سوز ز آتش زبانه برخیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز طرف دامن گل آستین فشان گذرد</p></div>
<div class="m2"><p>غبار هرکه ازین آستانه برخیزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ملاحت تو برآورد گرد از دلها</p></div>
<div class="m2"><p>ز خاک شور محال است دانه برخیزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر به گل گذری، با کمال بیدردی</p></div>
<div class="m2"><p>ز سینه اش نفس عاشقانه برخیزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز شعله بال سمندر نمی کند پروا</p></div>
<div class="m2"><p>به می چه پرده شرم از میانه برخیزد؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نفس شمرده زن ای بلبل نواپرداز</p></div>
<div class="m2"><p>که رنگ گل به نسیم بهانه برخیزد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو لاله مرهم داغش ز خون بود صائب</p></div>
<div class="m2"><p>سیاه بختی هرکس ز خانه برخیزد</p></div></div>