---
title: >-
    غزل شمارهٔ ۳۷۹۰
---
# غزل شمارهٔ ۳۷۹۰

<div class="b" id="bn1"><div class="m1"><p>دم مسیح دل دردمند ما نخورد</p></div>
<div class="m2"><p>اگر هلاک شود بازی دوا نخورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو ای که از دم عیسی فسانه پردازی</p></div>
<div class="m2"><p>بهوش باش که بیمار ما هوا نخورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهشت در قدم مرد عاقبت بین است</p></div>
<div class="m2"><p>کسی که رو به قفا می رود قفا نخورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به لاله طعنه مستی چه می زنی صائب؟</p></div>
<div class="m2"><p>میسرست قدح خوردنش، چرا نخورد؟</p></div></div>