---
title: >-
    غزل شمارهٔ ۳۳۱۶
---
# غزل شمارهٔ ۳۳۱۶

<div class="b" id="bn1"><div class="m1"><p>هر که چون زانوی خود آینه داری دارد</p></div>
<div class="m2"><p>روز و شب پیش نظر باغ و بهاری دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می کند جام علاجش به پف کاسه گری</p></div>
<div class="m2"><p>هر سری کز خرد خام غباری دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه شتاب است که در دیده من دارد اشک</p></div>
<div class="m2"><p>باز سیماب بر آیینه قراری دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به تهیدستی من کیست ز ثابت قدمان؟</p></div>
<div class="m2"><p>سر دیوار به کف دامن خاری دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به سیه روزی من کیست درین سبز چمن؟</p></div>
<div class="m2"><p>داغ در مد نظر لاله عذاری دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خضر اگر راه به سرچشمه حیوان برده است</p></div>
<div class="m2"><p>مست هم در دل شب آب خماری دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چهره صائب اگر رنگ فشان شد چه غم است؟</p></div>
<div class="m2"><p>شکر لله مژه لاله نگاری دارد</p></div></div>