---
title: >-
    غزل شمارهٔ ۲۰۳۴
---
# غزل شمارهٔ ۲۰۳۴

<div class="b" id="bn1"><div class="m1"><p>می سنگ اگر زند به ایاغم شگفت نیست</p></div>
<div class="m2"><p>گر بوی گل خورد به دماغم شگفت نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سودای زلف ریشه به مغزم دوانده است</p></div>
<div class="m2"><p>خون مشک اگر شود به دماغم شگفت نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پروانه داغ گرمی هنگامه من است</p></div>
<div class="m2"><p>دامن اگر زند به چراغم شگفت نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از کاوکاو ناخن الماس اگر جهد</p></div>
<div class="m2"><p>برق از سیاه خانه داغم شگفت نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با عندلیب هم سبق ناله بوده ام</p></div>
<div class="m2"><p>دلتنگ اگر ز صحبت زاغم شگفت نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صائب ز سوز سینه آتش فشان اگر</p></div>
<div class="m2"><p>آتش چکد ز پنبه داغم شگفت نیست</p></div></div>