---
title: >-
    غزل شمارهٔ ۶۴۲۶
---
# غزل شمارهٔ ۶۴۲۶

<div class="b" id="bn1"><div class="m1"><p>دزدیده در آن ابروی پیوسته نظر کن</p></div>
<div class="m2"><p>زنهار ازین دزد کمربسته حذر کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در رشته بی طاقت جان تاب نمانده است</p></div>
<div class="m2"><p>شیرازه اوراق دل آن موی کمر کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دزدان دل شب دست به تاراج برآرند</p></div>
<div class="m2"><p>در دور خط از خال رخ یار حذر کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دامن خواهش بفشان گرد تعلق</p></div>
<div class="m2"><p>چون موج میان باز به دریای خطر کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا افسر شاهان جهان تخت تو گردد</p></div>
<div class="m2"><p>از بحر به یک قطره قناعت چو گهر کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در قبضه خاک آن گهر پاک نگنجد</p></div>
<div class="m2"><p>گر عارفی از کعبه و بتخانه گذر کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمتر نتوان بود درین باغ ز شبنم</p></div>
<div class="m2"><p>صائب سری از روزن خورشید بدر کن</p></div></div>