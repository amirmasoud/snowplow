---
title: >-
    غزل شمارهٔ ۱۷۹۸
---
# غزل شمارهٔ ۱۷۹۸

<div class="b" id="bn1"><div class="m1"><p>بیان شوق به تیغ زبان میسر نیست</p></div>
<div class="m2"><p>محیط را گذر از ناودان میسر نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین که قافله عمر می رود به شتاب</p></div>
<div class="m2"><p>خبر گرفتن ازین کاروان میسر نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز جوش گل نفس غنچه پردگی شده است</p></div>
<div class="m2"><p>فراغ بال درین گلستان میسر نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به زیر چرخ اقامت زراستان مطلب</p></div>
<div class="m2"><p>سفر نکردن تیر از کمان میسر نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قدم برون منه از راه همچو سنگ نشان</p></div>
<div class="m2"><p>ترا که همرهی کاروان میسر نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ثبات عمر به پیری مجو که در پستی</p></div>
<div class="m2"><p>عنان کشیدن آب روان میسر نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز سیل خانه نگهداشت نمی آید</p></div>
<div class="m2"><p>قرار روح درین خاکدان میسر نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز نام نیک اثر جاودانه ای بگذار</p></div>
<div class="m2"><p>ترا که زندگی جاودان میسر نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به زهد خشک به معراج قرب نتوان رفت</p></div>
<div class="m2"><p>به نردبان سفر آسمان میسر نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر نه لنگر رطل گران به دست افتد</p></div>
<div class="m2"><p>برآمدن ز محیط جهان میسر نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سعادت ازلی مغز جمله نعمتهاست</p></div>
<div class="m2"><p>چه شد همای مرا استخوان میسر نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز گلستان چه تمنای برگ عیش کنم؟</p></div>
<div class="m2"><p>مرا که خار و خس آشیان میسر نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به غیر گرسنگی در میان نعمتها</p></div>
<div class="m2"><p>چه نعمت است که سیری ازان میسر نیست؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به عشق کوش که با شهپر خرد صائب</p></div>
<div class="m2"><p>گذشتن از سر کون و مکان میسر نیست</p></div></div>