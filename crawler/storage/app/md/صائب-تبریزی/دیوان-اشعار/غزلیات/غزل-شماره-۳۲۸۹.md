---
title: >-
    غزل شمارهٔ ۳۲۸۹
---
# غزل شمارهٔ ۳۲۸۹

<div class="b" id="bn1"><div class="m1"><p>دل سنگین ترا هر که به انصاف آرد</p></div>
<div class="m2"><p>می تواند به توجه پری از قاف آرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که در پرده خورد خون جگر همچو غزال</p></div>
<div class="m2"><p>ای بسا نافه سربسته که از ناف آرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکه چون بحر تواند گهر از لب ریزد</p></div>
<div class="m2"><p>به لب خود چه ضرورست کف لاف آرد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق پاک آینه چهره معشوق بود</p></div>
<div class="m2"><p>مهر را صبح برون از نفس صاف آرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی اجل یاد کسی خلق به نیکی نکنند</p></div>
<div class="m2"><p>مرگ این طایفه را بر سر انصاف آرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غنچه می داشت اگر درد سخن، می بایست</p></div>
<div class="m2"><p>بلبلان را به سراپرده الطاف آرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب از کلک شکربار دل عالم برد</p></div>
<div class="m2"><p>طوطیان را نتوانست به انصاف آرد</p></div></div>