---
title: >-
    غزل شمارهٔ ۹۷۱
---
# غزل شمارهٔ ۹۷۱

<div class="b" id="bn1"><div class="m1"><p>در شب مهتاب می را آب و تاب دیگرست</p></div>
<div class="m2"><p>باده و مهتاب با هم همچو شیر و شکرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون به شیرینی نگردد باده های تلخ صرف؟</p></div>
<div class="m2"><p>کز فروغ ماه، شکر در دهان ساغرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مطرب و می چون به دست افتاد، شاهد گو مباش</p></div>
<div class="m2"><p>کز فروغ مه، زمین و آسمان سیمین برست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر چه زور باده می آرد به جولان شیشه را</p></div>
<div class="m2"><p>پرتو مهتاب این طاوس را بال و پرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باده روشن گهر را نسبتی با ماه نیست</p></div>
<div class="m2"><p>نیست مهتاب با می همچو کف با گوهرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آسیای جام را آب از می روشن بود</p></div>
<div class="m2"><p>موجه صهبا پریزاد قدح را شهپرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از می لعلی، چراغ جام روشن می شود</p></div>
<div class="m2"><p>چربی پهلوی ماه از آفتاب انورست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از طراوت می چکد هر چند آب از ماهتاب</p></div>
<div class="m2"><p>از بیاض گردن مینا ز شادابی، ترست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از طلوع ماه، عالم گر چه روشن می شود</p></div>
<div class="m2"><p>آفتاب نشأه می را طلوع دیگرست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فارغند از مهر تابان، تیره روزان خمار</p></div>
<div class="m2"><p>می پرستان را دهان شیشه می خاورست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون کف دریای رحمت، پرتو مهتاب را</p></div>
<div class="m2"><p>شاهدان سیمبر، پوشیده زیر چادرست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در بلورین جام، می جولان دیگر می کند</p></div>
<div class="m2"><p>در شب مهتاب، می را آب و تاب دیگرست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نور مهتاب پریشان در بساط باغ ها</p></div>
<div class="m2"><p>آهوی مشکین شب را نافه های اذفرست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>می گشاید عقده سر در گم افلاک را</p></div>
<div class="m2"><p>میکشان را چون سبو دستی که در زیر سرست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یوسف سیمین بدن در نیل عریان گشته است؟</p></div>
<div class="m2"><p>یا مه تابان نمایان بر سپهر اخضرست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>این که صائب در کهنسالی جوانی می کند</p></div>
<div class="m2"><p>از نسیم التفات شاه والا گوهرست</p></div></div>