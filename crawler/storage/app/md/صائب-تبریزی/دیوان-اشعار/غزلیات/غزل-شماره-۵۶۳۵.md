---
title: >-
    غزل شمارهٔ ۵۶۳۵
---
# غزل شمارهٔ ۵۶۳۵

<div class="b" id="bn1"><div class="m1"><p>تا به کی افتم و تا چند بپا برخیزم؟</p></div>
<div class="m2"><p>من که افتادنی ام چند زجا برخیزم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من که تا خاستم از خاک، به خون افتادم</p></div>
<div class="m2"><p>در قیامت دگر از خاک چرا برخیزم؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون لب خشک صدف تشنه آب گهرم</p></div>
<div class="m2"><p>نه حبابم که پی کسب هوا برخیزم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من که از پستی طالع به زمین بستم نقش</p></div>
<div class="m2"><p>نیست امید که در روز جزا برخیزم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه سرو برگ غریبی، نه سرانجام وطن</p></div>
<div class="m2"><p>به چه طاقت بنشینم، به چه پا خیزم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چاره غفلت من صور قیامت نکند</p></div>
<div class="m2"><p>این نه خوابی است که از وی به صدا برخیزم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاک بادا به سر همت مردانه من</p></div>
<div class="m2"><p>اگر از خاک به اقبال هما برخیزم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قدمی بر سر خاکم بر زیارت بگذار</p></div>
<div class="m2"><p>تا ز خواب عدم آغوش گشا برخیزم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من که گم کرده خود یافتم اینجا صائب</p></div>
<div class="m2"><p>دیگر از کوی خرابات چرا برخیزم؟</p></div></div>