---
title: >-
    غزل شمارهٔ ۴۲۹۶
---
# غزل شمارهٔ ۴۲۹۶

<div class="b" id="bn1"><div class="m1"><p>آزاده رو مقید عالم نمی شود</p></div>
<div class="m2"><p>عیسی شکار رشته مریم نمی شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سجده خداست تنومندی بقا</p></div>
<div class="m2"><p>تا حلقه است زور کمان کم نمی شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لب بسته در محیط، صدف کرد زندگی</p></div>
<div class="m2"><p>قانع رهین منت حاتم نمی شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آمیزش کجان نشود طبع راست کج</p></div>
<div class="m2"><p>از اتصال حرف، الف خم نمی شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از قصر اعتبار تویک خشت تا بجاست</p></div>
<div class="m2"><p>هرگز بنای عشق تو محکم نمی شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برخیز تا به چشمه خورشید رو کنیم</p></div>
<div class="m2"><p>کز گل گشاد عقده شبنم نمی شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ابر تنک نهان نکند آفتاب را</p></div>
<div class="m2"><p>پوشیده داغ عشق به مرهم نمی شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عذر گناه بی ادبان جرم دیگرست</p></div>
<div class="m2"><p>زخم درون به بخیه فراهم نمی شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاتم برون ز دست سلیمان وقت کرد</p></div>
<div class="m2"><p>دیو هوا مسخر آدم نمی شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صائب سزای پنجه خونین تهمت است</p></div>
<div class="m2"><p>هر کس به رنگ مردم عالم نمی شود</p></div></div>