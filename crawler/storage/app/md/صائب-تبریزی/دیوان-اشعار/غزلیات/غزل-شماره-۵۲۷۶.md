---
title: >-
    غزل شمارهٔ ۵۲۷۶
---
# غزل شمارهٔ ۵۲۷۶

<div class="b" id="bn1"><div class="m1"><p>عاشق صادق نمی دارد تمناهای خام</p></div>
<div class="m2"><p>تخم انجم در زمین صبح می سوزد تمام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کام و ناکامی درین گلشن هم آغوش همند</p></div>
<div class="m2"><p>بیشتر از فصلها در فصل گل باشد زُکام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فسق پیش من زطاعات ریایی بهترست</p></div>
<div class="m2"><p>استخوان صد پیرهن باشد به از مغز حرام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تیرگی بیرون نرفت از دل به علم ظاهری</p></div>
<div class="m2"><p>خانه را روشن نمی سازد چراغ پشت بام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز انتقام حق کند ایمن عدوی خویش را</p></div>
<div class="m2"><p>می کشد هرکوته اندیشی که از خصم انتقام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می توان آسان گسستن دامهای سست را</p></div>
<div class="m2"><p>دل مخور گر کار دنیای تو باشد بی نظام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سالکی کز نور وحدت صیقلی شد دیده اش</p></div>
<div class="m2"><p>می کند چون کعبه هر سنگ نشان را احترام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عالم روشن به چشم خویش می سازد سیاه</p></div>
<div class="m2"><p>چون عقیق از سادگی هرکس کند تحصیل نام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از گرانسنگی پرستاران مودب می شوند</p></div>
<div class="m2"><p>سجده پیش بت برهمن می کند جای سلام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چشم بد را ناتمامیهاست نیل چشم زخم</p></div>
<div class="m2"><p>روی در نقصان گذارد ماه چون گردد تمام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از طوع پیش خسان مگشا لب خواهش که نیست</p></div>
<div class="m2"><p>تا لب گور این جراحت را امید التیام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اره با آهن دلی با نخل بارآور نکرد</p></div>
<div class="m2"><p>انچه با عزلت گزینان می کند سین سلام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نفس چون مطلق عنان گردید طغیان می کند</p></div>
<div class="m2"><p>سرکشی بارآورد چون نخل آب بی لجام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>می شود کام سخنورتر ز شعر آبدار</p></div>
<div class="m2"><p>سبز سازد تیغ اگراز آب خود صائب نیام</p></div></div>