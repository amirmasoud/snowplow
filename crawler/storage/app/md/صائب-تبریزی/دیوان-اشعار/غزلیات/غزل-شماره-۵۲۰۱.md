---
title: >-
    غزل شمارهٔ ۵۲۰۱
---
# غزل شمارهٔ ۵۲۰۱

<div class="b" id="bn1"><div class="m1"><p>زخمی تیغ شهادت زنده می خیزد ز خاک</p></div>
<div class="m2"><p>همچو گل با جبهه پرخنده می خیزد ز خاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از حجاب حسن شرم آلوده لیلی هنوز</p></div>
<div class="m2"><p>بید مجنون سر به پیش افکنده می خیزد ز خاک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می شود مرغابی دریای خجلت عندلیب</p></div>
<div class="m2"><p>بس که گل در عهد او شرمنده می خیزد ز خاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که دارد آگهی از چاه خس پوش جهان</p></div>
<div class="m2"><p>با عصا چون نرگس بیننده می خیزد ز خاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر گه چون طاوس عمرش رفت در پرداز بال</p></div>
<div class="m2"><p>در قیامت طایر پرکنده می خیزد ز خاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که اینجا خنده را چون غنچه دارد زیر لب</p></div>
<div class="m2"><p>صبح محشر با لب پرخنده می خیزد ز خاک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شرمساری می برد صائب به خلدش بی حساب</p></div>
<div class="m2"><p>هرکه در محشر ز خود شرمنده می خیزد ز خاک</p></div></div>