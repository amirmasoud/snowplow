---
title: >-
    غزل شمارهٔ ۵۵۸۵
---
# غزل شمارهٔ ۵۵۸۵

<div class="b" id="bn1"><div class="m1"><p>فروغ مهر در پیشانی دیوار می‌بینم</p></div>
<div class="m2"><p>صفای طلعت آیینه از زنگار می‌بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر در چاه، اگر در گوشه زندان بود یوسف</p></div>
<div class="m2"><p>ز چشم دوربین من بر سر بازار می‌بینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمی‌گردد حجاب بینش من پرده ظاهر</p></div>
<div class="m2"><p>که در سر هر چه هر کس دارد از دستار می‌بینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فریب دانه نتواند مرا در دام آوردن</p></div>
<div class="m2"><p>که از آغاز هر کار آخر آن کار می‌بینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرانجام دل سرگشته حیرانم چه خواهد شد</p></div>
<div class="m2"><p>که من این نقطه را بسیار بی‌پرگار می‌بینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو کز اسرار وحدت غافلی اوراق برهم زدن</p></div>
<div class="m2"><p>که من هر غنچه را گنجینه اسرار می‌بینم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز چشم اهل غفلت موبه‌مو خواب پریشان را</p></div>
<div class="m2"><p>دل شب‌ها به نور دیده بیدار می‌بینم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز لوح دیده صائب شسته‌ام تا گرد خودبینی</p></div>
<div class="m2"><p>به هر جانب که رو می‌آورم دیدار می‌بینم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کدامین شاخ گل صائب هوای گلستان دارد؟</p></div>
<div class="m2"><p>که گل را در کمین رخنه دیوار می‌بینم</p></div></div>