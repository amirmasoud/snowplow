---
title: >-
    غزل شمارهٔ ۳۰۹۳
---
# غزل شمارهٔ ۳۰۹۳

<div class="b" id="bn1"><div class="m1"><p>خوشا دردی که از چشم بداندیشان نهان باشد</p></div>
<div class="m2"><p>خوشا چاکی که چون خرما به جیب استخوان باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همیشه کاروان را گرد از دنبال می آید</p></div>
<div class="m2"><p>مرا گرد کسادی پیش پیش کاروان باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلش از شکوه من چون چراغ طور می سوزد</p></div>
<div class="m2"><p>چرا کس در شکایت اینقدر آتش زبان باشد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حصار خویش کردم سخت جانی را، ندانستم</p></div>
<div class="m2"><p>که شمشیر قضا را جان سخت من فسان باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به یک تقصیر سهل از مردم آگاه می رنجم</p></div>
<div class="m2"><p>نظر پوشیدن از بیدار دل خواب گران باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تراوش می کند این نکته از بیهوشی مجنون</p></div>
<div class="m2"><p>که سنگ کودکان دیوانه را رطل گران باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خزان از دور می بوسد زمین و باز می گردد</p></div>
<div class="m2"><p>در آن گلشن که بلبل صائب آتش زبان باشد</p></div></div>