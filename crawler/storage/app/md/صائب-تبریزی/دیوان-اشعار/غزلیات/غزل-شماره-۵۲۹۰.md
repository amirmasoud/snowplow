---
title: >-
    غزل شمارهٔ ۵۲۹۰
---
# غزل شمارهٔ ۵۲۹۰

<div class="b" id="bn1"><div class="m1"><p>از جنون این عالم بیگانه را گم کرده ام</p></div>
<div class="m2"><p>آسمان سیرم زمین خانه را گم کرده ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه من از خود نه کسی از حال من دارد خبر</p></div>
<div class="m2"><p>دل مرا و من دل دیوانه را گم کرده ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون سلیمانم که از کف داده ام تاج و نگین</p></div>
<div class="m2"><p>تا زمستی شیشه و پیمانه را گم کرده ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از من بی عاقبت آغاز هستی را مپرس</p></div>
<div class="m2"><p>کز گرانخوابی سرافسانه را گم کرده ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در چنین وقتی که بی پرواز شد زلف سخن</p></div>
<div class="m2"><p>از پریشان خاطریها شانه را گم کرده ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس که در یک جا ز غلطانی نمی گیرد قرار</p></div>
<div class="m2"><p>در نظر آن گوهر یکدانه را گم کرده ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طفل می گرید چو راه خانه را گم می کند</p></div>
<div class="m2"><p>چون نگریم من که صاحبخانه را گم کرده ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به که در دنبال دل باشم به هر جا می رود</p></div>
<div class="m2"><p>من که صائب کعبه و بتخانه را گم کرده ام</p></div></div>