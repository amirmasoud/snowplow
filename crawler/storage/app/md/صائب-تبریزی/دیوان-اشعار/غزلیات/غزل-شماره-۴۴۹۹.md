---
title: >-
    غزل شمارهٔ ۴۴۹۹
---
# غزل شمارهٔ ۴۴۹۹

<div class="b" id="bn1"><div class="m1"><p>دامن دشت عدم گیاه ندارد</p></div>
<div class="m2"><p>وای بر آن کس که زاد راه ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راز دل عاشقان ز سینه عیان است</p></div>
<div class="m2"><p>عرصه محشر گریزگاه ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیخبرست از بهار عالم بالا</p></div>
<div class="m2"><p>باغ وجودی که سرو آه ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روشنی سینه ها ز روزن داغ است</p></div>
<div class="m2"><p>تیره بود هر شبی که ماه ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر سر موی تو تیغ ملک گشایی است</p></div>
<div class="m2"><p>هیچ شهی این چنین سپاه ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دل خرسند آه سردنباشد</p></div>
<div class="m2"><p>بادخزان در بهشت راه ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیر نشد تشنه ای ازان لب نوخط</p></div>
<div class="m2"><p>آب حیات این دل سیاه ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اشک مرا چون صدف دلی نپذیرفت</p></div>
<div class="m2"><p>وای به ابری که خانه خواه ندارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رنگ برون می زند ز شیشه صافی</p></div>
<div class="m2"><p>چرخ عنان مرا نگاه ندارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر که برآید ز سردسیر تعین</p></div>
<div class="m2"><p>فکر لباس وغم کلاه ندارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا نشوی آشنای عالم مشرب</p></div>
<div class="m2"><p>قصر وجود تو پیشگاه ندارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عذر پسندیده است لیک ز نادان</p></div>
<div class="m2"><p>جرم خردمند عذر خواه ندارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در نظر اعتبار عشق عزیزست</p></div>
<div class="m2"><p>صائب اگر قدر خاک راه ندارد</p></div></div>