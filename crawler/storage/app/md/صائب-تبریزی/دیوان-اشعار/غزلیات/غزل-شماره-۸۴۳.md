---
title: >-
    غزل شمارهٔ ۸۴۳
---
# غزل شمارهٔ ۸۴۳

<div class="b" id="bn1"><div class="m1"><p>ای دفتر حسن ترا فهرست خط و خال‌ها</p></div>
<div class="m2"><p>تفصیل‌ها پنهان شده در پرده اجمال‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آتش‌فروز قهر تو، آیینه‌دار لطف تو</p></div>
<div class="m2"><p>هم مغرب ادبارها، هم مشرق اقبال‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیشانی عفو ترا پرچین نسازد جرم ما</p></div>
<div class="m2"><p>آیینه کی بر هم خورد از زشتی تمثال‌ها؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سهل است اگر بال و پری نقصان این پروانه شد</p></div>
<div class="m2"><p>کان شمع سامان می‌دهد از شعله زرین بال‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با عقل گشتم هم‌سفر یک کوچه راه از بی‌کسی</p></div>
<div class="m2"><p>شد ریشه‌ریشه دامنم از خار استدلال‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرشب کواکب کم کنند از روزی ما پاره‌ای</p></div>
<div class="m2"><p>هرروز گردد تنگ‌تر سوراخ این غربال‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حیران اطوار خودم، درمانده کار خودم</p></div>
<div class="m2"><p>هر لحظه دارم نیتی چون قرعه رمال‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرچند صائب می‌روم سامان نومیدی کنم</p></div>
<div class="m2"><p>زلفش به دستم می‌دهد سررشته آمال‌ها</p></div></div>