---
title: >-
    غزل شمارهٔ ۶۷۸
---
# غزل شمارهٔ ۶۷۸

<div class="b" id="bn1"><div class="m1"><p>چشمی که شد ز دیدن حسن آفرین جدا</p></div>
<div class="m2"><p>خون می خورد ز جلوه هر نازنین جدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب کار من گداختن و روز مردن است</p></div>
<div class="m2"><p>تا همچو موم گشته ام از انگبین جدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون رفت دل ز دست، نیاید به جای خویش</p></div>
<div class="m2"><p>چون نافه ای که گشت ز آهوی چین جدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیچیده همچو گرد یتیمی به گوهریم</p></div>
<div class="m2"><p>ما را ز یکدگر نکند آستین جدا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر جا کنند نقل، شود نقل انجمن</p></div>
<div class="m2"><p>حرفی که شد ازان دو لب شکرین جدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون پرده های دیده یعقوب شد سفید</p></div>
<div class="m2"><p>تا شد صدف ز صحبت در ثمین جدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گریند خون به روز من و روزگار من</p></div>
<div class="m2"><p>جان حزین جدا، دل اندوهگین جدا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دامان سایلان، سپر برق آفت است</p></div>
<div class="m2"><p>از هیچ خرمنی نشود خوشه چین جدا!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون برخوری به سنگدلان نرم شو که موم</p></div>
<div class="m2"><p>از روی نرم، نقش کند از نگین جدا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صائب در آفتاب جهانتاب محو شد</p></div>
<div class="m2"><p>هر شبنمی که شد ز گل و یاسمین جدا</p></div></div>