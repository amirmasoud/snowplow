---
title: >-
    غزل شمارهٔ ۴۹۳۲
---
# غزل شمارهٔ ۴۹۳۲

<div class="b" id="bn1"><div class="m1"><p>حساب دین و دل راپاک کن باچشم عیارش</p></div>
<div class="m2"><p>که شب رانیمه خواهد کرد از خط حسن طرارش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو عالم چون صف مژگان اگر زیرو زبر گردد</p></div>
<div class="m2"><p>من و آن چشم کافر کز رگ خواب است زنارش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هرجاسرو او در جلوه آید، کبک می سازد</p></div>
<div class="m2"><p>به تیغ کوه خون خود حلال از شرم رفتارش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شود گرداب دریای حلاوت دیده روزن</p></div>
<div class="m2"><p>درآن محفل که آید درسخن لعل شکربارش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فروغ عارض او ذره را خورشید می سازد</p></div>
<div class="m2"><p>خوشا انجام آن شبنم که گردد محو دیدارش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل اندامی که درپیراهن من خار می ریزد</p></div>
<div class="m2"><p>ز جوش گل رگ لعل است هر خاری ز دیوارش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به اشک شبنم خونین جگر صائب که پردازد؟</p></div>
<div class="m2"><p>که می داند عرق راشبنم بیگانه گلزارش</p></div></div>