---
title: >-
    غزل شمارهٔ ۲۰۲۱
---
# غزل شمارهٔ ۲۰۲۱

<div class="b" id="bn1"><div class="m1"><p>نابسته رخنه نظر از هر عیان که هست</p></div>
<div class="m2"><p>از پرده جلوه گر نشود هر نهان که هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر مو زبان نکته سرایی نمی شود</p></div>
<div class="m2"><p>تا ترک گفتگو نکند این زبان که هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چندین هزار جامه بدل کرد هر حباب</p></div>
<div class="m2"><p>دریای بیکران حقیقت همان که هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باور که می کند، که ازان گنج سر به مهر</p></div>
<div class="m2"><p>آفاق پر گهر شد و او همچنان که هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از سرنوشت هر دو جهان سربرآورد</p></div>
<div class="m2"><p>خود را اگر کسی بشناسد چنان که هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سنگ نشان به کعبه رسانید حاج را</p></div>
<div class="m2"><p>حق را نیافتی تو به چندین نشان که هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کار جهان چنان که تو خواهی اگر شود</p></div>
<div class="m2"><p>ایمان نیاوری به خدای جهان که هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب چسان به حمد تو رطب اللسان شود؟</p></div>
<div class="m2"><p>ای عاجز از ثنای تو هر نکته دان که هست</p></div></div>