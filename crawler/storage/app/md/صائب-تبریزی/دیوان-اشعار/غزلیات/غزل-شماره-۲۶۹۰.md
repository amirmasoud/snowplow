---
title: >-
    غزل شمارهٔ ۲۶۹۰
---
# غزل شمارهٔ ۲۶۹۰

<div class="b" id="bn1"><div class="m1"><p>کی به ناخن از دل غمگین گره وا می شود؟</p></div>
<div class="m2"><p>دست چون افتاد از کار این گره وا می شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر گشاد دل بود موقوف هر مشکل که هست</p></div>
<div class="m2"><p>این گره چون باز شد چندین گره وا می شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتگوی عشق با افسردگان بی حاصل است</p></div>
<div class="m2"><p>کی ز خون مرده از تلقین گره وا می شود؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشقبازان گر به آه آتشین زورآورند</p></div>
<div class="m2"><p>دلبران را از دل سنگین گره وا می شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رشته عمرم ز پیچ و تاب می گردد گره</p></div>
<div class="m2"><p>تا مرا زان جبهه پرچین گره وا می شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در گشاد دل نفس بیهوده می سوزد نسیم</p></div>
<div class="m2"><p>چون سپند از آتش آخر این گره وا می شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قرب زر چون سکه نگشاید ز ابرویش گره</p></div>
<div class="m2"><p>هر که را از چهره زرین گره وا می شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هیچ تحسینی سخن را نیست چون فهمیدگی</p></div>
<div class="m2"><p>از دل ما کی به هر تحسین گره وا می شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غنچه خسبی فیضها دارد درین بستانسرا</p></div>
<div class="m2"><p>صدهزاران عقده صائب زین گره وا می شود</p></div></div>