---
title: >-
    غزل شمارهٔ ۲۳۷۵
---
# غزل شمارهٔ ۲۳۷۵

<div class="b" id="bn1"><div class="m1"><p>تا عبیر افشانی زلف ترا نظاره کرد</p></div>
<div class="m2"><p>نکهت پیراهن یوسف گریبان پاره کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نو نیاز عشق چون فرهاد و مجنون نیستیم</p></div>
<div class="m2"><p>طفل ما مشق جنون بر تخته گهواره کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زخم من چون ماه نو تا گوشه ابرو نمود</p></div>
<div class="m2"><p>تیغ چون دیوانگان زنجیر جوهر پاره کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو شبنم غوطه در سرچشمه خورشید زد</p></div>
<div class="m2"><p>در عرق هر کس گل روی ترا نظاره کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کار ما اکنون به لطف بی گمانت بسته است</p></div>
<div class="m2"><p>کآنچه می بایست کردن، سعی ما یکباره کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچ کافر را الهی کودک بدخو مباد!</p></div>
<div class="m2"><p>چاره جوییهای دل صائب مرا بیچاره کرد</p></div></div>