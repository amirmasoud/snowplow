---
title: >-
    غزل شمارهٔ ۴۲۵۸
---
# غزل شمارهٔ ۴۲۵۸

<div class="b" id="bn1"><div class="m1"><p>از خط فروغ روی تو پنهان کجا شود</p></div>
<div class="m2"><p>خامش چراغ ماه به دامان کجا شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آب شور تشنه شود بیقرارتر</p></div>
<div class="m2"><p>سیراب بوسه از لب جانان کجا شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلوت دعای جوشن حسن برهنه روست</p></div>
<div class="m2"><p>دلگیر یوسف از چه و زندان کجا شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر جوش عکس، خانه آیینه تنگ نیست</p></div>
<div class="m2"><p>خلق کریم تنگ ز مهمان کجا شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طوطی به معنی سخن خود نمی رسد</p></div>
<div class="m2"><p>هر کس سخنورست سخندان کجا شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرگز نمی شود سگ دیوانه پاسبان</p></div>
<div class="m2"><p>نفسی که سرکش است بفرمان کجا شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صیقل ز جوهر آینه را پاک می کند</p></div>
<div class="m2"><p>مژگان حجاب دیده حیران کجا شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بال وپر تلاطم بحرست بادبان</p></div>
<div class="m2"><p>دامن حریف دیده گریان کجا شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بخت سیاه، لازم طبع روان بود</p></div>
<div class="m2"><p>ظلمت جدا ز چشمه حیوان کجا شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>موری که حکم اوست به روی زمین روان</p></div>
<div class="m2"><p>قانع به روی دست سلیمان کجا شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون طبع شد فسرده، غزلخوان کجا شود</p></div>
<div class="m2"><p>صائب ز خون مرده روانی مدار چشم</p></div></div>