---
title: >-
    غزل شمارهٔ ۳۳۱
---
# غزل شمارهٔ ۳۳۱

<div class="b" id="bn1"><div class="m1"><p>ز سرسبزی حیات جاودان بخشد تماشا را</p></div>
<div class="m2"><p>به آب زندگی پرورده اند آن سرو بالا را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رسانیده است حسن او به جایی دلفریبی را</p></div>
<div class="m2"><p>که خالش حلقه بیرون در سازد سویدا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز چشم پر خمارش نیستم آگه، همین دانم</p></div>
<div class="m2"><p>که خون در دل کند لبهای میگونش تمنا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز چشم پاک شبنم، می شود گل زهره پیشانی</p></div>
<div class="m2"><p>مپوش از دیده من زینهار آن روی زیبا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غبار لشکر بیگانه خط تند می آید</p></div>
<div class="m2"><p>ز خواب ناز کن بیدار چشم باده پیما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نسازی دست اگر آلوده خونم ز بی قدری</p></div>
<div class="m2"><p>به خون من نگارین ساز باری آن کف پا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز رحمش بیشتر می ترسم از بیداد او، ورنه</p></div>
<div class="m2"><p>مسلمان می توانم ساختن آن شوخ ترسا را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>محبت کهنه چون شد، تازه گردد زور بازویش</p></div>
<div class="m2"><p>به مطلب می رساند عاقبت یوسف زلیخا را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز طوق قمریان می شد سراپا دیده حیران</p></div>
<div class="m2"><p>اگر می دید سرو بوستان آن قد رعنا را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز شیرین کاری فرهاد، بی آرام شد شیرین</p></div>
<div class="m2"><p>خوشا کاری که سازد تلخ، خواب کارفرما را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل از نازک خیالان می رباید معنی نازک</p></div>
<div class="m2"><p>میفکن بر کمر زنهار آن زلف چلیپا را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نصیب مور بی زنهار خط سنگدل سازد</p></div>
<div class="m2"><p>دریغ از تلخکامان داشتن لعل شکرخا را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>علاج دردمندان را کند دیگر به بیماری</p></div>
<div class="m2"><p>اگر افتد نظر بر چشم بیمار تو عیسی را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر بر من نداری رحم، بر خود رحم کن ظالم</p></div>
<div class="m2"><p>عنانداری کنم تا چند آه بی محابا را؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر بیرون دهم خونی که پنهان در جگر دارم</p></div>
<div class="m2"><p>ز حیرت چشم قربانی شود گرداب، دریا را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سر گرمی که مجنون من از سودای او دارد</p></div>
<div class="m2"><p>ز نقش پا چراغان می کند دامان صحرا را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به چشمش تیغ زهرآلود می گردید هر سروی</p></div>
<div class="m2"><p>چمن پیرا اگر می دید آن شمشاد بالا را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ازان زلف مسلسل داغ ها دارم به دل صائب</p></div>
<div class="m2"><p>که می بیند به چندین چشم حیران آن سراپا را</p></div></div>