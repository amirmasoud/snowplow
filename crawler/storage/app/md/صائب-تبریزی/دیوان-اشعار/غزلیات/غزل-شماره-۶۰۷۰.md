---
title: >-
    غزل شمارهٔ ۶۰۷۰
---
# غزل شمارهٔ ۶۰۷۰

<div class="b" id="bn1"><div class="m1"><p>مطربا صبح است، قانون صبوحی ساز کن</p></div>
<div class="m2"><p>دانه دل را سپند شعله آواز کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ته دل چون سحر برکش نوای ساده ای</p></div>
<div class="m2"><p>نقش بر بال تذروان چنگل شهباز کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سهل باشد پرده قانون خود را ساختن</p></div>
<div class="m2"><p>می توانی، طالع ناساز ما را ساز کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناقه را ذوق حدی (بر) دارد از دل فکر بار</p></div>
<div class="m2"><p>زیر بار غم منه دل را، حدی آغاز کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در رکاب برق دارد پای، فیض صبحگاه</p></div>
<div class="m2"><p>ای کم از شبنم، درین گلزار چشمی باز کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زیر کوه آهنین منت صیقل مرو</p></div>
<div class="m2"><p>خانه آیینه دل را به می پرداز کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناخنی بر پرده های چنگ خواب آلود زن</p></div>
<div class="m2"><p>عیش های شب پریشان گشته را آواز کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غنچه باغ خموشی ایمن است از برگریز</p></div>
<div class="m2"><p>لب ببند از گفتگو، خون در دل غماز کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ماه صائب از نیاز خویش دایم زردروست</p></div>
<div class="m2"><p>بی نیازی شیوه خود کن، به عالم ناز کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این جواب آن که می گوید حکیم غزنوی</p></div>
<div class="m2"><p>پادشاه امروز گشتی در جهان آواز کن</p></div></div>