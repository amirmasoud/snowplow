---
title: >-
    غزل شمارهٔ ۶۶۸
---
# غزل شمارهٔ ۶۶۸

<div class="b" id="bn1"><div class="m1"><p>چنان که از نمک افزون شود جراحت‌ها</p></div>
<div class="m2"><p>یکی هزار ز پرسش شود مصیبت‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مپوش چشم ز نظاره غبار ملال</p></div>
<div class="m2"><p>که پیش خیز نشاط است گرد کلفت‌‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مده ز جهل مرکب به نام تن چو عقیق</p></div>
<div class="m2"><p>که هست لازم تحصیل نام، ظلمت‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مدار چشم اقامت ز اعتبار جهان</p></div>
<div class="m2"><p>که همچو سایه بال هماست دولت‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمی‌توان به خس و خار کشت آتش را</p></div>
<div class="m2"><p>یکی هزار شود عشق از نصیحت‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبود حوصله چشم شور، ظرف مرا</p></div>
<div class="m2"><p>به خوردن دل خود ساختم ز نعمت‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز اشک، دیده یک آفریده رنگین نیست</p></div>
<div class="m2"><p>فسرده است درین عهد خون غیرت‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نکرده ساده دل خود ز فکر، گوشه مگیر</p></div>
<div class="m2"><p>که انجمن شود از فکر پوچ، خلوت‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نفس ز دل به لبم می‌رسد به دشواری</p></div>
<div class="m2"><p>ز بس گره شده در سینه‌ام شکایت‌ها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کسی ز خاک لحد شسته رو برون آید</p></div>
<div class="m2"><p>که شوید از عرق شرم، گرد خجلت‌ها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گران شدن سبکی را در آستین دارد</p></div>
<div class="m2"><p>که هست لازم ثقل ثقیل خفت‌ها</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جریده شو که ندارد به عهد ما صائب</p></div>
<div class="m2"><p>به غیر خوردن دل دانه دام صحبت‌ها</p></div></div>