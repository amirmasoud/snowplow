---
title: >-
    غزل شمارهٔ ۳۵۲۶
---
# غزل شمارهٔ ۳۵۲۶

<div class="b" id="bn1"><div class="m1"><p>ساقی از جامی اگر خاطر ما شاد کند</p></div>
<div class="m2"><p>به ازان است که صد میکده آباد کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم خفته است غزالی که ندارد شوخی</p></div>
<div class="m2"><p>من و آن صید که خون در دل صیاد کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آخر ای پادشه حسن چه انصاف است این؟</p></div>
<div class="m2"><p>که در ایام تو عشق اینهمه بیداد کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یاد ایام جنون بر سر من بارد سنگ</p></div>
<div class="m2"><p>کودکان را چو ز مکتب کسی آزاد کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز خط سبز که فرمان سلیمان دارد</p></div>
<div class="m2"><p>آدمی را که تواند که پریزاد کند؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل رخسار ترا اینهمه عاشق بس نیست؟</p></div>
<div class="m2"><p>که نظر باز دگر از عرق ایجاد کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نایتیمانه ز دیوانه ام آن طفل گذشت</p></div>
<div class="m2"><p>می توانست به سنگی دل من شاد کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ماتم واقعه لیلی و مجنون دارد</p></div>
<div class="m2"><p>هر درایی که درین بادیه فریاد کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون رسد وقت، دهد جان به دم تیشه خویش</p></div>
<div class="m2"><p>بیستون گر چه سپرداری فرهاد کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر از سختی ایام شود آدم نرم</p></div>
<div class="m2"><p>روی من تربیت سیلی استاد کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بخل بهتر ز سخایی که به آوازه بود</p></div>
<div class="m2"><p>تیرگی به ز چراغی است که فریاد کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خنده کبک شود ناله خونین صائب</p></div>
<div class="m2"><p>بیستون یاد چون از رفتن فرهاد کند</p></div></div>