---
title: >-
    غزل شمارهٔ ۵۴۳۸
---
# غزل شمارهٔ ۵۴۳۸

<div class="b" id="bn1"><div class="m1"><p>در ره باطل ز پا چون نقش پا افتاده‌ایم</p></div>
<div class="m2"><p>کعبه مقصد کجا و ما کجا افتاده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خجلت روی زمین داریم از بحر کمان</p></div>
<div class="m2"><p>از هدف تا دور چون تیر خطا افتاده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون نگاه دیده وحشی‌غزالان از حجاب</p></div>
<div class="m2"><p>در میان مردمان ناآشنا افتاده‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاک می‌لیسید زبان موج ما در جویبار</p></div>
<div class="m2"><p>تا از آن دریای بی‌ساحل جدا افتاده‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دم سرد فلک آیینه ما تیره نیست</p></div>
<div class="m2"><p>ما ز دامان تر خود از جلا افتاده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عذر نامقبول ما را کی پذیرند اهل دید؟</p></div>
<div class="m2"><p>ما که در چاه ضلالت با عصا افتاده‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از سعادت مشرق خورشید دولت گشته است</p></div>
<div class="m2"><p>هرکجا چون سایه بال هما افتاده‌ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون کمان و تیر در وحشت‌سرای روزگار</p></div>
<div class="m2"><p>تا به هم پیوسته‌ایم از هم جدا افتاده‌ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این جواب آن غزل صائب که والی گفته است</p></div>
<div class="m2"><p>«هرکه را از پیش پا رفته است ما افتاده‌ایم»</p></div></div>