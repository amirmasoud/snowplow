---
title: >-
    غزل شمارهٔ ۳۳۱۲
---
# غزل شمارهٔ ۳۳۱۲

<div class="b" id="bn1"><div class="m1"><p>تشنگان حال جگر سوختگان می دانند</p></div>
<div class="m2"><p>خبر از تشنه ما ریگ بیابان دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شورش عشق و جنون را ز دل صائب پرس</p></div>
<div class="m2"><p>روی دریا خبر از سیلی طوفان دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرکز از دایره انگشتر فرمان دارد</p></div>
<div class="m2"><p>مور در خانه خود حکم سلیمان دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می توان یافت ز عنوان که چه در مکتوب است</p></div>
<div class="m2"><p>پا منه بر در آن خانه که دربان دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می کند خنده گل جلوه آغوش وداع</p></div>
<div class="m2"><p>تا که دیگر سر تاراج گلستان دارد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر افتادن ما خاستنی خواهد داشت</p></div>
<div class="m2"><p>سقف افلاک خطرهای نمایان دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پایه ناز دو بالا شود از خودبینی</p></div>
<div class="m2"><p>شبنم آن به که ز گل آینه پنهان دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نکند زخم زبان بیخبران را بیدار</p></div>
<div class="m2"><p>پای خوابیده چه پروای مغیلان دارد؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرکه را گوشه ای از وسعت مشرب دادند</p></div>
<div class="m2"><p>گر همه مور بود ملک سلیمان دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خبر از خنده سوفار ندارد پیکان</p></div>
<div class="m2"><p>چه اثر در دل غمگین لب خندان دارد؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روز ما تیره ز اندیشه فردا شده است</p></div>
<div class="m2"><p>خواب ما را غم تعبیر پریشان دارد</p></div></div>