---
title: >-
    غزل شمارهٔ ۴۵۱۳
---
# غزل شمارهٔ ۴۵۱۳

<div class="b" id="bn1"><div class="m1"><p>مرا ناله از پرده دل برآید</p></div>
<div class="m2"><p>به نازی که لیلی ز محمل برآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین باغ چون سرو آزادگان را</p></div>
<div class="m2"><p>به جای ثمر عقده دل برآید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر مزرع هستی این رنگ دارد</p></div>
<div class="m2"><p>بر آن دانه رحم است کز گل برآید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوشا کعبه دل که در آستانش</p></div>
<div class="m2"><p>به یک آه صدکار مشکل برآید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز صحرای فردوس دلگیر گردد</p></div>
<div class="m2"><p>غریبی که با گوشه دل برآید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آن حلقه چشم دل ماندحیران</p></div>
<div class="m2"><p>که کشتی ز گرداب مشکل برآید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پروبال طوفان بودموج دریا</p></div>
<div class="m2"><p>به مجنون ما کی سلاسل برآید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به صد لب اگر زخم گویا نگردد</p></div>
<div class="m2"><p>که از عهده شکر قاتل برآید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز آگاهی خویش در زیر تیغم</p></div>
<div class="m2"><p>خوشا حال صیدی که غافل برآید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جگر تشنگان محیط فنا را</p></div>
<div class="m2"><p>چه کام از لب خشک ساحل برآید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر آن خال شد دلبری ختم صائب</p></div>
<div class="m2"><p>ز صد بنده یک بنده مقبل برآید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به دردی بنالم درین راه صائب</p></div>
<div class="m2"><p>که فریاد از راه ومنزل برآید</p></div></div>