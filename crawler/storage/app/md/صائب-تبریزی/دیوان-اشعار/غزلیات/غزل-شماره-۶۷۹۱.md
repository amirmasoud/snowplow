---
title: >-
    غزل شمارهٔ ۶۷۹۱
---
# غزل شمارهٔ ۶۷۹۱

<div class="b" id="bn1"><div class="m1"><p>که غیر از سنگ طفلان می کند دیوانه آرایی؟</p></div>
<div class="m2"><p>که غیر از گنج گوهر می کند ویرانه آرایی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تمام عمر اگر با کعبه در یک پیرهن باشم</p></div>
<div class="m2"><p>همان در کعبه دل می کنم بتخانه آرایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عنان کجروی پیچیدن از گردون نمی آید</p></div>
<div class="m2"><p>مکن در راه سیلاب فنا کاشانه آرایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مهیای تپیدن شو که آن صیاد سنگین دل</p></div>
<div class="m2"><p>ندارد هیچ کاری غیر دام و دانه آرایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا بر غفلت سرشار بلبل خنده می آید</p></div>
<div class="m2"><p>که در ایام گل دارد دماغ خانه آرایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی تا چند مزدور هوای نفس خود باشد؟</p></div>
<div class="m2"><p>نگشتی بی دماغ از خانه طفلانه آرایی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به حرف عقل گوش انداختم دیوانه گردیدم</p></div>
<div class="m2"><p>مرا در خواب غفلت کرد این افسانه آرایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه ناقوس دلی نالان، نه زنار تنی پیچان</p></div>
<div class="m2"><p>مکن با ان تجمل دعوی بتخانه آرایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر از اهل شوقی مگذر از اندیشه صائب</p></div>
<div class="m2"><p>که چون باد باران می کند دیوانه آرایی</p></div></div>