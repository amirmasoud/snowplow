---
title: >-
    غزل شمارهٔ ۷۹۷
---
# غزل شمارهٔ ۷۹۷

<div class="b" id="bn1"><div class="m1"><p>نتوان به بی‌مثال رسید از مثال‌ها</p></div>
<div class="m2"><p>از ره مرو به موج سراب خیال‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بانگ جرس ز خوبی یوسف چه آگه است؟</p></div>
<div class="m2"><p>در کنه ذات حق نرسد قیل و قال‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زرین چو برگ‌های خزان دیده گشته‌اند</p></div>
<div class="m2"><p>از باد دستی تو، زبان سؤال‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما چون قلم تمام زبان شکایتیم</p></div>
<div class="m2"><p>در خلوتی که قال شمارند حال‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از اشتیاق دام تو مرغان دوربین</p></div>
<div class="m2"><p>در بیضه می‌دهند سرانجام بال‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در روزگار چشم تو جام تهی نماند</p></div>
<div class="m2"><p>یکسر شدند ماه تمام این هلال‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داغی که بود بر دل مجنون دورگرد</p></div>
<div class="m2"><p>شد تازه از سیاهی چشم غزال‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ده در شود گشاده، شود بسته چون دری</p></div>
<div class="m2"><p>دارند ده زبان ز ده انگشت، لال‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در عهد پاکدامنی او نمی‌رود</p></div>
<div class="m2"><p>دل‌های بدگمان به ره احتمال‌ها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صائب ز خواب‌های پریشان خلاص شد</p></div>
<div class="m2"><p>هرکس که ساده کرد دل از خط و خال‌ها</p></div></div>