---
title: >-
    غزل شمارهٔ ۱۵۰۴
---
# غزل شمارهٔ ۱۵۰۴

<div class="b" id="bn1"><div class="m1"><p>موج سنبل ز پریشانی پرواز من است</p></div>
<div class="m2"><p>گل برافروخته شعله آواز من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سینه ای کز گل صد برگ ز هم نشناسند</p></div>
<div class="m2"><p>مخزن درد نهان و صدف راز من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لامکان سیرتر از عشق بود همت من</p></div>
<div class="m2"><p>چرخ کبکی است که در چنگل شهباز من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منم آن سلسله جنبان نواهای غریب</p></div>
<div class="m2"><p>که ز گل مرغ چمن گوش بر آواز من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می توان خواند ز پیشانی من راز جهان</p></div>
<div class="m2"><p>جام جم داغ دل آینه پرداز من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زهره شوخ که سر حلقه نه دایره است</p></div>
<div class="m2"><p>در شبستان حیا پردگی از ساز من است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون به آیینه رسم طوطی شیرین سخنم</p></div>
<div class="m2"><p>صحبت تیره دلان سرمه آواز من است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیشکر را ز خموشی به زبان چندین بند</p></div>
<div class="m2"><p>همه از رهگذر کلک سخنساز من است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حرف مردم ز بدونیک نیارم به زبان</p></div>
<div class="m2"><p>جای رحم است بر آن خصم که غماز من است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیستم چشم درین دایره، لیکن چون چشم</p></div>
<div class="m2"><p>گر پر کاه بود، مانع پرواز من است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عندلیبی که به آتش نفسی مشهورست</p></div>
<div class="m2"><p>کف خاکستری از شعله آواز من است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شبنم بیجگر آن زهره ندارد صائب</p></div>
<div class="m2"><p>داغ دامان گل از گریه غماز من است</p></div></div>