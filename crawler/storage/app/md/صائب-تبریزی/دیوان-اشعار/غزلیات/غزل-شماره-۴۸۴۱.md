---
title: >-
    غزل شمارهٔ ۴۸۴۱
---
# غزل شمارهٔ ۴۸۴۱

<div class="b" id="bn1"><div class="m1"><p>شد ز خط لعل تو ایمن ز شبیخون هوس</p></div>
<div class="m2"><p>در شب تار بود شهد مسلم ز مگس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حرامی است اگر باده نشاطی دارد</p></div>
<div class="m2"><p>دختر رز به حلالی نشود قسمت کس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نفس را غفلت دل باعث جرأت گردد</p></div>
<div class="m2"><p>دزد را سرمه توفیق بود خواب عسس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نصیحت دل مغرور نگردد بیدار</p></div>
<div class="m2"><p>رهرو مانده کند خواب به آواز جرس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از هوس سینه عشاق مکدر گردد</p></div>
<div class="m2"><p>صفحه آینه راتیره کند مشق نفس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه کند زخم زبان با دل عاشق صائب ؟</p></div>
<div class="m2"><p>شعله اندیشه ندارد ز زبان بازی خس</p></div></div>