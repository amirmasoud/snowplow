---
title: >-
    غزل شمارهٔ ۱۴۸
---
# غزل شمارهٔ ۱۴۸

<div class="b" id="bn1"><div class="m1"><p>نیست تاب درد غربت جان افگار مرا</p></div>
<div class="m2"><p>با قفس آزاد کن مرغ گرفتار مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارد از تار نفس زنار، نفس کافرم</p></div>
<div class="m2"><p>تا دم آخر گسستن نیست زنار مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست می شوید ز کار گل به آب زندگی</p></div>
<div class="m2"><p>چون خضر هر کس کند تعمیر دیوار مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درد را بیچارگی بر من گوارا کرده بود</p></div>
<div class="m2"><p>شربت عیسی به جان آورد بیمار مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فارغ از سیر گلستانم که فکر دوربین</p></div>
<div class="m2"><p>می کند در زیر بال آماده گلزار مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سر و سامان من بگذر که جوش مغز ساخت</p></div>
<div class="m2"><p>چون کف دریا پریشانگرد دستار مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گریه بیرون برد از دستم عنان اختیار</p></div>
<div class="m2"><p>سر به صحرا داد جوش لاله کهسار مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز ملامت از جنون دیوانه ام طرفی نبست</p></div>
<div class="m2"><p>سنگ طفلان بود حاصل نخل پربار مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عالمی می آمد از گفتار من صائب به راه</p></div>
<div class="m2"><p>بهره از کردار اگر می بود گفتار مرا</p></div></div>