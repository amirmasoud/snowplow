---
title: >-
    غزل شمارهٔ ۶۶۹۶
---
# غزل شمارهٔ ۶۶۹۶

<div class="b" id="bn1"><div class="m1"><p>سرفرازی را نباشد جنگ با افتادگی</p></div>
<div class="m2"><p>دولت خورشید را دارد بپا افتادگی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تواضع دولت افزاید سعادتمند را</p></div>
<div class="m2"><p>خوش بود چون سایه از بال هما افتادگی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از عزیزی می گذارد پا به چشم آفتاب</p></div>
<div class="m2"><p>هر که گیرد همچو شبنم از هوا افتادگی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرکز بر گرد سرگردیدن افلاک کرد</p></div>
<div class="m2"><p>نقطه بی دست و پای خاک را افتادگی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفت در بیهوده گردی عمر ما چون گردباد</p></div>
<div class="m2"><p>ما سبک مغزان کجاییم و کجا افتادگی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مردم بی دست و پا را مرکبی در کار نیست</p></div>
<div class="m2"><p>می رود منزل به منزل جاده با افتادگی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جا به کنج گلخن و صحن گلستان داده است</p></div>
<div class="m2"><p>شعله را گردن فرازی، آب را افتادگی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دانه را سرسبز سازد، قطره را گوهر کند</p></div>
<div class="m2"><p>در جهان خاک باشد کیمیا افتادگی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی پر و بالی کند نزدیک راه دور را</p></div>
<div class="m2"><p>برد بر افلاک چون شبنم مرا افتادگی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر چه باشد بر زمین پست جاری حکم آب</p></div>
<div class="m2"><p>می شود سد ره سیل بلا افتادگی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شعله شد مغلوب خاکستر به اندک فرصتی</p></div>
<div class="m2"><p>سرکشان را زود آرد زیر پا، افتادگی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دیگران گر از خدا خواهند اوج اعتبار</p></div>
<div class="m2"><p>می کند دریوزه صائب از خدا افتادگی</p></div></div>