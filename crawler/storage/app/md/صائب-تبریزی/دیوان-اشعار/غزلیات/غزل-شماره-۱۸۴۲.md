---
title: >-
    غزل شمارهٔ ۱۸۴۲
---
# غزل شمارهٔ ۱۸۴۲

<div class="b" id="bn1"><div class="m1"><p>غرور حسن به خط از دماغ یار نرفت</p></div>
<div class="m2"><p>ز ترکتاز خزان زین چمن بهار نرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر چه کرد قیامت نسیم نومیدی</p></div>
<div class="m2"><p>امید من ز سر راه انتظار نرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز خون فاخته دیوار بوستان غلطید</p></div>
<div class="m2"><p>ز جای خویشتن آن سرو پایدار نرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز ترکتاز خزان باخت رنگ هستی را</p></div>
<div class="m2"><p>گلی که در قدم باد نوبهار نرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوش است وصل که بی پرده جلوه گر گردد</p></div>
<div class="m2"><p>به بوی پیرهن از چشم ما غبار نرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز خاکمال اجل داد جان به صد خواری</p></div>
<div class="m2"><p>به زیر تیغ تو هر کس به اختیار نرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فریب جلوه ساحل مخور چه نوسفران</p></div>
<div class="m2"><p>که هیچ کشتی ازین بحر بر کنار نرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کدام شاخ گل آم پیاده در بستان؟</p></div>
<div class="m2"><p>که آخر از دم سرد خزان سوار نرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رسیده ای به لب گور، کجروی بگذار</p></div>
<div class="m2"><p>نگشته راست، به سوراخ هیچ مار نرفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر چه باد خزان رفت پاک گلشن را</p></div>
<div class="m2"><p>ز آشیانه ما بوی نوبهار نرفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به یک دو هفته گل از شاخ اعتبار افتاد</p></div>
<div class="m2"><p>خوشا کسی که به دنبال اعتبار نرفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به فکرهای پریشان گذشت ایامش</p></div>
<div class="m2"><p>کسی که همچو تو صائب به فکر یار نرفت</p></div></div>