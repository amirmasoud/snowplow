---
title: >-
    غزل شمارهٔ ۳۷۹
---
# غزل شمارهٔ ۳۷۹

<div class="b" id="bn1"><div class="m1"><p>چه خوش باشد در آغوش آورم سرو روانش را</p></div>
<div class="m2"><p>کنم شیرازه اوراق دل، موی میانش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کیم من تا وصال گل به گرد خاطرم گردد؟</p></div>
<div class="m2"><p>مرا این بس که گرد سر بگردم باغبانش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کنار حسرتی از طوق قمری تنگتر دارم</p></div>
<div class="m2"><p>نمی دانم که چون در بر کشم سرو روانش را؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر بر آسمان ناز رفته است آن هلال ابرو</p></div>
<div class="m2"><p>به زور چرب نرمی می کشم آخر کمانش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که حد دارد نظر بازی کند با چین ابرویش؟</p></div>
<div class="m2"><p>دهانم تلخ شد تا چاشنی کردم کمانش را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آن وادی که مغزم سرمه چشم غزالان شد</p></div>
<div class="m2"><p>ز دست موج، روغن می چکد ریگ روانش را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر خصم قوی بنیاد، کوه بیستون گردد</p></div>
<div class="m2"><p>ز برق تیشه جوی شیر سازم استخوانش را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چسان معلوم گردد رتبه حسن سخن صائب؟</p></div>
<div class="m2"><p>که دارد در میان گرد کسادی کاروانش را</p></div></div>