---
title: >-
    غزل شمارهٔ ۵۱۴۶
---
# غزل شمارهٔ ۵۱۴۶

<div class="b" id="bn1"><div class="m1"><p>عالم بالا ندارد فیض از پاکان دریغ</p></div>
<div class="m2"><p>قطره خود را ندارد از صدف نیسان دریغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زر که صرف کیمیا گردد یکی صد می شود</p></div>
<div class="m2"><p>خرده جان را چرا کس دارد از جانان دریغ؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رزق می آید به پای میهمان از خوان غیب</p></div>
<div class="m2"><p>بی نصیب آن کس که نعمت دارد از مهمان دریغ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صاف کن دل تا ازان رخسار صافی برخوری</p></div>
<div class="m2"><p>تا به چند آیینه داری از مه کنعان دریغ ؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دامن دولت چو هر ساعت به دست دیگری است</p></div>
<div class="m2"><p>دست تا از توست از سایل مدار احسان دریغ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن که از دندان دهانت پر ز گوهر ساخته</p></div>
<div class="m2"><p>نیست ممکن تا لب گور از تو دارد نان دریغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بس که شد امساک صائب عام در دوران ما</p></div>
<div class="m2"><p>سنگ می دارند از دیوانگان طفلان دریغ</p></div></div>