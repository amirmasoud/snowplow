---
title: >-
    غزل شمارهٔ ۵۹۳۸
---
# غزل شمارهٔ ۵۹۳۸

<div class="b" id="bn1"><div class="m1"><p>تا در خم این کارگه شیشه گرانیم</p></div>
<div class="m2"><p>چون طفل در آیینه به حیرت نگرانیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از رهگذر گوش، صدف کان گهر شد</p></div>
<div class="m2"><p>ما هرزه در ایان همه چون موج زبانیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در بتکده بیگانه تر از قبله نماییم</p></div>
<div class="m2"><p>در کعبه سبک قدرتر از سنگ نشانیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا ترکش افلاک پر از تیر شهاب است</p></div>
<div class="m2"><p>ما بی سر و پایان همه نارنج نشانیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما اخگر عشقیم که تا دامن محشر</p></div>
<div class="m2"><p>در توده خاکستر افلاک نهانیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسیار سبکروح تر از شبنم صبحیم</p></div>
<div class="m2"><p>هر چند که در چشم تو چون خواب گرانیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوشی نخراشیده صدای جرس ما</p></div>
<div class="m2"><p>ما نرم روان قافله ریگ روانیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون مور اگر امروز به خاکیم فتاده</p></div>
<div class="m2"><p>فرداست که در دست سلیمان زمانیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نقش پی ما خضر ره پیشروان است</p></div>
<div class="m2"><p>هر چند که چون گرد ز دنباله روانیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خونابه دل آتش یاقوت گدازست</p></div>
<div class="m2"><p>مگذار به این آبله ناخن برسانیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در دیده کامل نظران نور یقینیم</p></div>
<div class="m2"><p>هر چند که در چشم خسان خار گمانیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رحمت ز سیه کاری ما روی سفیدست</p></div>
<div class="m2"><p>ما سوختگان خال رخ لاله ستانیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این آن غزل مرشد روم است که فرمود</p></div>
<div class="m2"><p>ما پیله عشقیم که بی برگ جهانیم</p></div></div>