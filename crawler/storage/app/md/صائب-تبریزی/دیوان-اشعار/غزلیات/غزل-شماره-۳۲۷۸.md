---
title: >-
    غزل شمارهٔ ۳۲۷۸
---
# غزل شمارهٔ ۳۲۷۸

<div class="b" id="bn1"><div class="m1"><p>هرکه تسلیم به فرمان قضا می‌گردد</p></div>
<div class="m2"><p>بر سرش ابر بلا بال هما می‌گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه ضرور است کشیدن ز مسیحا منّت؟</p></div>
<div class="m2"><p>کامرانی چو کند درد، دوا می‌گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی‌ریاضت نتوان شهره آفاق شدن</p></div>
<div class="m2"><p>مه چو لاغر شود انگشت‌نما می‌گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>واصلان گوش ندارند به افسانه عقل</p></div>
<div class="m2"><p>راه گم کرده پی بانگ‌درا می‌گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در تمنای تو ای قافله‌سالار بهار</p></div>
<div class="m2"><p>گل جدا، رنگ جدا، بوی جدا می‌گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صائب از منت صیقل جگرم گشت کباب</p></div>
<div class="m2"><p>ای خوش آن آینه کز خود به صفا می‌گردد</p></div></div>