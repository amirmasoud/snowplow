---
title: >-
    غزل شمارهٔ ۵۲۷
---
# غزل شمارهٔ ۵۲۷

<div class="b" id="bn1"><div class="m1"><p>نخل قد تو هم آغوش بلا کرد مرا</p></div>
<div class="m2"><p>هوس زلف تو همدست صبا کرد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک در دیده مقراض جدایی بادا!</p></div>
<div class="m2"><p>که ازان حاشیه بزم جدا کرد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عکس من خاک به چشم آینه را می پاشید</p></div>
<div class="m2"><p>پرتو روی تو آیینه نما کرد مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بعد عمری که فلک بر سر انصاف آمد</p></div>
<div class="m2"><p>همچو یوسف به لب چاه بها کرد مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>(چه عجب گر جگر نی بخراشد نفسم</p></div>
<div class="m2"><p>بند از بند، فراق تو جدا کرد مرا)</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>(داشتم شکوه ز ایران، به تلافی گردون</p></div>
<div class="m2"><p>در فرامشکده هند رها کرد مرا)</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون به بستر بنهم پهلوی راحت صائب؟</p></div>
<div class="m2"><p>غنچه خسبی، گره بند قبا کرد مرا</p></div></div>