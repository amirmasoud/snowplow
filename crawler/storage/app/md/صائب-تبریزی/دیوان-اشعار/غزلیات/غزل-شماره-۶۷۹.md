---
title: >-
    غزل شمارهٔ ۶۷۹
---
# غزل شمارهٔ ۶۷۹

<div class="b" id="bn1"><div class="m1"><p>می می کند خیال تنک ظرف آب را</p></div>
<div class="m2"><p>ویرانه سیل می شمرد ماهتاب را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل می تپد به خون ز تمنای خویشتن</p></div>
<div class="m2"><p>بر سیخ می کشد رگ خامی کباب را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجنون کمند طره لیلی کند خیال</p></div>
<div class="m2"><p>بر روی دشت، جلوه موج سراب را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلمرده ای که سر به گریبان خواب برد</p></div>
<div class="m2"><p>کافور ساخت یاسمن ماهتاب را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق است ترجمان نفس های سوخته</p></div>
<div class="m2"><p>آتش کند ترنم مرغ کباب را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عنبر به رخ فکنده نقاب از بهار خویش</p></div>
<div class="m2"><p>تا دیده است آن خط چون مشک ناب را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زنار چشم از رگ خواب است، زینهار</p></div>
<div class="m2"><p>مژگان صفت به چشم مده جای، خواب را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تن ده به بخت شور که خوابانده است چرخ</p></div>
<div class="m2"><p>از صبح در نمک جگر آفتاب را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از پختگی است عاشق اگر گریه کم کند</p></div>
<div class="m2"><p>خونابه است شاهد خامی کباب را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای گل که موج خنده ات از سر گذشته است</p></div>
<div class="m2"><p>آماده باش گریه تلخ گلاب را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من چون نفس کشم، که فراموش می کند</p></div>
<div class="m2"><p>بر آتش عذار تو مو پیچ و تاب را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در بزم قرب، پاس نفس داشتن بلاست</p></div>
<div class="m2"><p>زان دور عمر زود سرآید حباب را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صائب چها به چشم تماشاییان کند</p></div>
<div class="m2"><p>رویی که ساخت صبح قیامت نقاب را</p></div></div>