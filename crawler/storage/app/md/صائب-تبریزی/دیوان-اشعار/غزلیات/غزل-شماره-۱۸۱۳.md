---
title: >-
    غزل شمارهٔ ۱۸۱۳
---
# غزل شمارهٔ ۱۸۱۳

<div class="b" id="bn1"><div class="m1"><p>شب فراق ز روز حساب خالی نیست</p></div>
<div class="m2"><p>که از بیاض، سواد کتاب خالی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نظر به هر چه کنم تازه می شود داغم</p></div>
<div class="m2"><p>که هیچ ذره ازان آفتاب خالی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به چشم کم منگر، هیچ خاکساری را</p></div>
<div class="m2"><p>که هیچ روزن ازان ماهتاب خالی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه موج مگذر ازین بحر سرسری زنهار</p></div>
<div class="m2"><p>که چون صدف ز گهر یک حباب خالی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوانده در همه جا ریشه بیقراری عشق</p></div>
<div class="m2"><p>که نبض سنگ هم از اضطراب خالی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز من گشودن لب چون صدف نمی آید</p></div>
<div class="m2"><p>وگرنه ابر مروت ز آب خالی نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زبان لاف بود لازم تهیدستی</p></div>
<div class="m2"><p>زمین شور ز موج سراب خالی نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگر به فکر سواری است آن سبک جولان؟</p></div>
<div class="m2"><p>که هیچ ملک دل از انقلاب خالی نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همین نه موی میان تراست این خم و پیچ</p></div>
<div class="m2"><p>که هیچ موی تو از پیچ و تاب خالی نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز گل تهی نشود بوستان دربسته</p></div>
<div class="m2"><p>ز حسن، پرده شرم و حجاب خالی نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نمی توان دل بی داغ یافت در عالم</p></div>
<div class="m2"><p>که از سیاهی جغد این خراب خالی نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز قرب و بعد شود کار سالکان دشوار</p></div>
<div class="m2"><p>وگرنه هیچ زمینی ز آب خالی نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به اشک تلخ ازان گلعذار قانع شو</p></div>
<div class="m2"><p>که گل نهفته چو گردد گلاب خالی نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز پست فطرتی از فیض عشق محرومی</p></div>
<div class="m2"><p>وگرنه کوه بلند از عقاب خالی نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سؤال ماست کز آن لب نمی رسد به جواب</p></div>
<div class="m2"><p>وگرنه هیچ سؤال از جو خالی نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هنوز ازان لب نوخط توان به کام رسید</p></div>
<div class="m2"><p>ز نشأه این می پا در رکاب خالی نیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز هر چه چشم توان آب داد مغتنم است</p></div>
<div class="m2"><p>چه قحط حسن شود آفتاب خالی نیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صواب محض بود رزق خامشان صواب</p></div>
<div class="m2"><p>که گفتگو ز خطا و صواب خالی نیست</p></div></div>