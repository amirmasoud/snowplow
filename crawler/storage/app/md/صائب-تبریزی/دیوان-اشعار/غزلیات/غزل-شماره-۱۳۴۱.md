---
title: >-
    غزل شمارهٔ ۱۳۴۱
---
# غزل شمارهٔ ۱۳۴۱

<div class="b" id="bn1"><div class="m1"><p>حاصلی غیر از تهیدستی دل روشن نداشت</p></div>
<div class="m2"><p>شمع با آن منزلت هرگز دو پیراهن نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم ما را حسرت پرواز در دل شد گره</p></div>
<div class="m2"><p>بس که امید پر کاهی ازین خرمن نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر قدر پایم به سنگ آمد درین ظلمت سرا</p></div>
<div class="m2"><p>هیچ دلسوزی چراغی پیش پای من نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می شود از تنگ چشمان دستگاه عیش تنگ</p></div>
<div class="m2"><p>وقت زخمی خوش که چشم بخیه از سوزن نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عمر خود بیجا تلف کردم به دست و پا زدن</p></div>
<div class="m2"><p>ساحلی این بحر خونین جز فرو رفتن نداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست جز بیگانگی از آشنایان روزیم</p></div>
<div class="m2"><p>گر چه پاس آشنایی هیچ کس چون من نداشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رشته تابی تا بجا بود از لباس هستیم</p></div>
<div class="m2"><p>خار صحرای ملامت دستم از دامن نداشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد جهان تنگ از ترک خودی بر من وسیع</p></div>
<div class="m2"><p>این قبای تنگ، صائب چاره جز کندن نداشت</p></div></div>