---
title: >-
    غزل شمارهٔ ۳۴۸۱
---
# غزل شمارهٔ ۳۴۸۱

<div class="b" id="bn1"><div class="m1"><p>حسرت عمر، مرا در دل افگار بماند</p></div>
<div class="m2"><p>رفت سیلاب به دریا و خس و خار بماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بساط من سودازده زان باغ و بهار</p></div>
<div class="m2"><p>خار خاری است که در سینه افگار بماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرکز از دایره پروانه آزادی یافت</p></div>
<div class="m2"><p>دل ما بود که در حلقه زنار بماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بال پرواز ز هر موج سرابش دادند</p></div>
<div class="m2"><p>هرکه در بادیه عشق ز رفتار بماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عنکبوتی است که در فکر شکار مگس است</p></div>
<div class="m2"><p>زاهد خشک که در پرده پندار بماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زیر گردون خبر از حال دل من دارد</p></div>
<div class="m2"><p>هرکه را آینه در پرده زنگار بماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل به نظاره او شد که دگر باز آید</p></div>
<div class="m2"><p>آب گردید و در آن لعل گهر بماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان نمی خواست درین غمکده ساکن گردد</p></div>
<div class="m2"><p>از غبار دل ما در ته دیوار بماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شست از خون شفق صبح قیامت دامن</p></div>
<div class="m2"><p>خون ما بود که بر گردن دلدار بماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می تواند گره از کار دو عالم وا کرد</p></div>
<div class="m2"><p>دست هرکس ز تماشای تو از کار بماند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دانه سوخته از خاک برآمد صائب</p></div>
<div class="m2"><p>دل بی حاصل ما در ته دیوار بماند</p></div></div>