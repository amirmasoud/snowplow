---
title: >-
    غزل شمارهٔ ۱۸۷۳
---
# غزل شمارهٔ ۱۸۷۳

<div class="b" id="bn1"><div class="m1"><p>پوشیدن نظر ز جهان عین حکمت است</p></div>
<div class="m2"><p>قطع نظر ز خلق کمال بصیرت است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمی که باز کردن آن به ز (بستن است)</p></div>
<div class="m2"><p>در عالم مشاهده آن چشم عبرت است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم صفا مدار ز گردون ( )</p></div>
<div class="m2"><p>کاین آسیا همیشه پر از گرد کلفت است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بینایی نظر به مقامی نمی رسد</p></div>
<div class="m2"><p>دارالامان مردم آگاه، حیرت است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی پاس شرع، وضع جهان مستقیم نیست</p></div>
<div class="m2"><p>قانون حفظ صحت عالم، شریعت است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرمان پذیر شرع چو گشتی به امر و نهی</p></div>
<div class="m2"><p>ناکردنی است هر چه خلاف مروت است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون هست بر جناح سفر، بهر اعتبار</p></div>
<div class="m2"><p>بال هما صحیفه عنوان دولت است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آگاه را سفیدی مو تازیانه ای است</p></div>
<div class="m2"><p>در دیده های نرم، رگ خواب غفلت است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روی زمین ز سجده اخلاص ساده است</p></div>
<div class="m2"><p>طاعات خلق بیشتر از روی عادت است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر صبر خود مناز، که دارد فلاخنی</p></div>
<div class="m2"><p>زلفش به کف، که سنگ کمش کوه طاقت است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چرخ وسیع، چشمه سوزن بود (بر او)</p></div>
<div class="m2"><p>در دیده کسی که مقید به ساعت است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رزقش رسید ز عالم بالا به پای خویش</p></div>
<div class="m2"><p>صائب کسی که همچو صدف پاک طینت است</p></div></div>