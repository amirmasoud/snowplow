---
title: >-
    غزل شمارهٔ ۵۵۴۹
---
# غزل شمارهٔ ۵۵۴۹

<div class="b" id="bn1"><div class="m1"><p>نیم بیدرد دایم پیچ و تاب ساکنی دارم</p></div>
<div class="m2"><p>چو نبض ناتوانان اضطراب ساکنی دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز سوز عشق ازین پهلو به آن پهلو نمی گردم</p></div>
<div class="m2"><p>درین دریای پر آتش کباب ساکنی دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشو ای آهنین دل از کمند جذبه ام غافل</p></div>
<div class="m2"><p>که چون آهن ربا در دل شتاب ساکنی دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ربایم هر که را از گلرخان بر من گذار افتد</p></div>
<div class="m2"><p>ز حیرت گر چه چون آیینه آب ساکنی دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندارم در گره چیزی که ارزد بیقراری را</p></div>
<div class="m2"><p>درین دریای پرشورش حباب ساکنی دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گره گردیده ام چون کهربا هر چند در ظاهر</p></div>
<div class="m2"><p>نهان در پرده دل اضطراب ساکنی دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به آواز جرس نتوان مرا بیدار گرداندن</p></div>
<div class="m2"><p>که من همچون ره خوابیده خواب ساکنی دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>علاج شعله سرکش ز آب نرم می آید</p></div>
<div class="m2"><p>سوال تند دشمن را جواب ساکنی دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز سرعت گر چه روی سیل را بر خاک می مالم</p></div>
<div class="m2"><p>به قدر آرزومندی شتاب ساکنی دارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر چه دور گردان می شمارندم ز بیدردان</p></div>
<div class="m2"><p>به هر رگ چون ره خوابیده خواب ساکنی دارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو ای سیل سبکرو وصل دریا را غنیمت دان</p></div>
<div class="m2"><p>که من چون آهن و فولاد آب ساکنی دارم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>امید صلح هر جا هست از خود می دوم بیرون</p></div>
<div class="m2"><p>به هر جا تند باید شد عتاب ساکنی دارم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به ظاهر چون کتان در پرتو مهتاب اگر محوم</p></div>
<div class="m2"><p>به تار و پود هستی پیچ و تاب ساکنی دارم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ندارم با کمال ناامیدی موج بیتابی</p></div>
<div class="m2"><p>درین دریای پر وحشت سراب ساکنی دارم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به جای دعوی از حرفم تراوش می کند معنی</p></div>
<div class="m2"><p>خم سربسته ام بوی شراب ساکنی دارم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عجب دارم نسوزد دانه ام را برق نومیدی</p></div>
<div class="m2"><p>که چشم آبیاری از سحاب ساکنی دارم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز من صائب درین بستانسرا تندی نمی آید</p></div>
<div class="m2"><p>گل نشکفته ام، بوی گلاب ساکنی دارم</p></div></div>