---
title: >-
    غزل شمارهٔ ۸۴۸
---
# غزل شمارهٔ ۸۴۸

<div class="b" id="bn1"><div class="m1"><p>به ادب نوش، جام دولت را</p></div>
<div class="m2"><p>مده از کف زمام دولت را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در چراگاه آرزو مگذار</p></div>
<div class="m2"><p>توسن بی لجام دولت را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به نفس های آتشین چون شمع</p></div>
<div class="m2"><p>زنده دل دار، شام دولت را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزل در دیده ها سبک سازد</p></div>
<div class="m2"><p>پله احتشام دولت را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به قوام آورد گران حلمی</p></div>
<div class="m2"><p>باده کم قوام دولت را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست جودست شمسه زرین</p></div>
<div class="m2"><p>قصر عالی مقام دولت را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نتوان جز به دست حزم گرفت</p></div>
<div class="m2"><p>آستین دوام دولت را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جوی شیرست چرب نرمی خلق</p></div>
<div class="m2"><p>صحن دارالسلام دولت را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مد انعام، تار شیرازه است</p></div>
<div class="m2"><p>دفتر انتظام دولت را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می کند تار و مار، باد غرور</p></div>
<div class="m2"><p>سر زلف نظام دولت را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به دو دست دعا نگه دارند</p></div>
<div class="m2"><p>شهسواران زمام دولت را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در فلاخن نهد سبکساری</p></div>
<div class="m2"><p>لنگر احتشام دولت را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از بلندی پایه همت</p></div>
<div class="m2"><p>نردبان ساز، بام دولت را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صبح محشر نمی کند بیدار</p></div>
<div class="m2"><p>باده نوشان جام دولت را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اهل معنی به فکرت صائب</p></div>
<div class="m2"><p>زنده دارند نام دولت را</p></div></div>