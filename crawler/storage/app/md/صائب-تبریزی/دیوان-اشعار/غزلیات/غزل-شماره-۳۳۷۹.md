---
title: >-
    غزل شمارهٔ ۳۳۷۹
---
# غزل شمارهٔ ۳۳۷۹

<div class="b" id="bn1"><div class="m1"><p>تا دلی از کف ارباب وفا می گیرد</p></div>
<div class="m2"><p>بارها فال ز دیوان حیا می گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گره ناز به ابروی تبسم بستن</p></div>
<div class="m2"><p>غنچه تعلیم ازان بند قبا می گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن که چندین قفس از نغمه سرایان دارد</p></div>
<div class="m2"><p>کار بر بلبل ما تنگ چرا می گیرد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناوکی کز دل الماس ترازو گردد</p></div>
<div class="m2"><p>زان کمانخانه ابروی هوا می گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز قلم کز سر خود قطع تعلق کرده است</p></div>
<div class="m2"><p>که به تقریب سخن دست ترا می گیرد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صائب از فیض هواداری اشک سحری</p></div>
<div class="m2"><p>لاله باغ سخن رنگ ز ما می گیرد</p></div></div>