---
title: >-
    غزل شمارهٔ ۴۸۵۹
---
# غزل شمارهٔ ۴۸۵۹

<div class="b" id="bn1"><div class="m1"><p>چون صدف در حلقه دریادلان خاموش باش</p></div>
<div class="m2"><p>با دهان گوهرافشان پای تا سرگوش باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صرف استغفار کن انفاس رادر خانقاه</p></div>
<div class="m2"><p>در حریم میکشان گلبانگ نوشانوش باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نغمه عشاق را شرط است حسن استماع</p></div>
<div class="m2"><p>در حضور بلبلان چون گل سراپا گوش باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا شود گلگونه مینای گردون خون تو</p></div>
<div class="m2"><p>همچو صهبا تا درین خمخانه ای در جوش باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با کمال هوشیاری چون به مستان برخوری</p></div>
<div class="m2"><p>زینهار اظهار هشیاری مکن،مدهوش باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می کند میخواره را گفتار بیش از باده مست</p></div>
<div class="m2"><p>چون نهادی لب به لب پیمانه را خاموش باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هدیه ما تنگدستان را به چشم کم مبین</p></div>
<div class="m2"><p>ازمروت بر سر خوان تهی سرپوش باش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پرده نیش است هر نوشی که دارد این جهان</p></div>
<div class="m2"><p>برنمی آیی به زخم نیش، دور از نوش باش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا شود چون شمع از روی تو روشن دیده ها</p></div>
<div class="m2"><p>بازبان آتشین در انجمن خاموش باش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی زر از سیمین بران داری اگرامید وصل</p></div>
<div class="m2"><p>مستعد صد بغل خمیازه آغوش باش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از زبان نرم دشمن احتیاط از کف مده</p></div>
<div class="m2"><p>بر حذر زنهار صائب زین چه خس پوش باش</p></div></div>