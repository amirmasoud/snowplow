---
title: >-
    غزل شمارهٔ ۲۶۷۷
---
# غزل شمارهٔ ۲۶۷۷

<div class="b" id="bn1"><div class="m1"><p>چند قرب یار از غفلت حجاب من شود؟</p></div>
<div class="m2"><p>آب دریا پرده چشم حباب من شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نصیب آتشین رویی کباب من شود</p></div>
<div class="m2"><p>گریه خونین زخوشحالی شراب من شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شورش من پرده افلاک را بر هم درید</p></div>
<div class="m2"><p>من نه آن بحرم که این کفها نقاب من شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن که دارد اعتماد خیرگی بر چشم خویش</p></div>
<div class="m2"><p>سخت می خواهم دچار آفتاب من شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن گرانخوابم که نتوانم ز جا برخاستن</p></div>
<div class="m2"><p>دامن محشر اگر بالین خواب من شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زور بازوی حوادث در بساط روزگار</p></div>
<div class="m2"><p>آنقدر باشد که صرف پیچ و تاب من شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شور عشق از پرده دل عاقبت بیرون فتاد</p></div>
<div class="m2"><p>این نمک تا چند پنهان در کباب من شود؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیقراری در فلاخن می گذارد کوه را</p></div>
<div class="m2"><p>کیست طاقت تا حریف اضطراب من شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جلوه شبنم کند در دیده اش طوفان نوح</p></div>
<div class="m2"><p>هر گلستانی که سیراب از سحاب من شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از کباب خامسوز لاله می گیرد دماغ</p></div>
<div class="m2"><p>مغز هر کس تازه از بوی کباب من شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من نه آن پروانه ام کز شعله دارم جان دریغ</p></div>
<div class="m2"><p>آتش روی تو می ترسم کباب من شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در کتاب هستی من نقطه ای بی سهو نیست</p></div>
<div class="m2"><p>حیف از اوقاتی که صرف انتخاب من شود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر دم آبی که موجش از رگ تلخی بود</p></div>
<div class="m2"><p>در بهارستان خرسندی گلاب من شود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیست با خورشید نسبت سوز پنهان مرا</p></div>
<div class="m2"><p>موی آتش دیده نبض از اضطراب من شود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با تهیدستی به سایل تازه رو برمی خورم</p></div>
<div class="m2"><p>تشنه هیهات است نومید از سراب من شود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برق نتوانست با من گشت صائب همعنان</p></div>
<div class="m2"><p>کیست مجنون تا تواند همرکاب من شود</p></div></div>