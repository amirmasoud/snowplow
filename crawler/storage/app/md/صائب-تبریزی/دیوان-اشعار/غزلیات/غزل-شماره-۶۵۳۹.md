---
title: >-
    غزل شمارهٔ ۶۵۳۹
---
# غزل شمارهٔ ۶۵۳۹

<div class="b" id="bn1"><div class="m1"><p>تا کرد تیغ غمزه حمایل نگاه تو</p></div>
<div class="m2"><p>شد سرمه گوشه گیر ز چشم سیاه تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما امتحان دشنه الماس کرده ایم</p></div>
<div class="m2"><p>از یک سرست با مژه کینه خواه تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یادم ز جلوه های قد یار داده ای</p></div>
<div class="m2"><p>ای کبک خوش خرام سر ما و راه تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در چشم شور حشر نمک کرد انتظار</p></div>
<div class="m2"><p>در پرده تا به چند نشیند نگاه تو؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای شاخ گل ببال که با این غرور حسن</p></div>
<div class="m2"><p>گل کج ندیده است به طرف کلاه تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صائب به هوش باش مبادا دل شبی</p></div>
<div class="m2"><p>آتش به خرمنی بزند برق آه تو</p></div></div>