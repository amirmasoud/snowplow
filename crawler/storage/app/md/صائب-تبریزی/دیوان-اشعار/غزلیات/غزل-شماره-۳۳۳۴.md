---
title: >-
    غزل شمارهٔ ۳۳۳۴
---
# غزل شمارهٔ ۳۳۳۴

<div class="b" id="bn1"><div class="m1"><p>از سری جوی سعادت که ز دولت گذرد</p></div>
<div class="m2"><p>تن به خواری دهد از افسر عزت گذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که دامن کشد از خار علایق اینجا</p></div>
<div class="m2"><p>سالم از دامن صحرای قیامت گذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنچه دارند ز شب زنده همان از عمرست</p></div>
<div class="m2"><p>خون مرده است دگر هرچه به غفلت گذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خانه هرکه به اندازه بود چون زنبور</p></div>
<div class="m2"><p>همه ایام حیاتش به حلاوت گذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دامن برق به خاشاک علایق شد بند</p></div>
<div class="m2"><p>تا که زین وادی خونخوار سلامت گذرد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می توان رست به همواری ازین عالم تنگ</p></div>
<div class="m2"><p>رشته از چشمه سوزن به فراغت گذرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون زمین پاک بود تخم مدارید دریغ</p></div>
<div class="m2"><p>صبح حیف است که بی اشک ندامت گذرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاشق از زخم زبان باک ندارد، ورنه</p></div>
<div class="m2"><p>آتش از سایه این خار به دهشت گذرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شور عشق است نمک مایده هستی را</p></div>
<div class="m2"><p>ای خوش آن عمر که در شغل محبت گذرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می توان گفت نصیبی ز سخاوت دارد</p></div>
<div class="m2"><p>باد دستی که ز آوازه همت گذرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دولت سنگدلان زود بسر می آید</p></div>
<div class="m2"><p>سیل از سینه کهسار به سرعت گذرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مردم آزار محال است خجالت نکشد</p></div>
<div class="m2"><p>که نمک آب شود چون به جراحت گذرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>درد خود را نکند پیش مسیحا اظهار</p></div>
<div class="m2"><p>هرکه سالم ز دم تیغ شهادت گذرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دامن هر که بود پاک ز عصیان صائب</p></div>
<div class="m2"><p>چون سیاووش ز آتش به سلامت گذرد</p></div></div>