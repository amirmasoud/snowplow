---
title: >-
    غزل شمارهٔ ۶۳۸۶
---
# غزل شمارهٔ ۶۳۸۶

<div class="b" id="bn1"><div class="m1"><p>عرض صفا به اهل هنر می کنی مکن</p></div>
<div class="m2"><p>پیش کلیم دست بدر می کنی مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صدق عزیمت است دلیل ره طلب</p></div>
<div class="m2"><p>تو سست عزم، عزم دگر می کنی مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قطع ره طلب به تأمل نمی شود</p></div>
<div class="m2"><p>در پیش پای خویش نظر می کنی مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون سیل، بی ملاحظگی خضر این ره است</p></div>
<div class="m2"><p>این راه را به قاعده سر می کنی مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فکر و خیال محرم این شاهراه نیست</p></div>
<div class="m2"><p>هر دم خیال (و) فکر دگر می کنی مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی جذبه آفتاب دلیلت اگر شود</p></div>
<div class="m2"><p>از خود سفر به نور شرر می کنی مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در ره شکنجه ای بتر از کفش تنگ نیست</p></div>
<div class="m2"><p>با خوی بد هوای سفر می کنی مکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در قلزمی که یکجهتان دم نمی زنند</p></div>
<div class="m2"><p>هر دم زدن هوای دگر می کنی مکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آزادگان چو سرو به یک جامه قانعند</p></div>
<div class="m2"><p>هر روز یک لباس به بر می کنی مکن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از رشک عشق، غیرت حسن است بیشتر</p></div>
<div class="m2"><p>در ماه و آفتاب نظر می کنی مکن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اکنون که برد بی خبری هر چه داشتیم</p></div>
<div class="m2"><p>ما را ز حال خویش خبر می کنی مکن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پاس شکوه فقر و قناعت نگاه دار</p></div>
<div class="m2"><p>در پیش گنج، دست به زر می کنی مکن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صائب یکی ز حلقه به گوشان زلف توست</p></div>
<div class="m2"><p>او را نظر به چشم دگر می کنی مکن</p></div></div>