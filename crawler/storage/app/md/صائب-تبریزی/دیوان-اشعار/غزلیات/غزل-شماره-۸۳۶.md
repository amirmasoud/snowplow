---
title: >-
    غزل شمارهٔ ۸۳۶
---
# غزل شمارهٔ ۸۳۶

<div class="b" id="bn1"><div class="m1"><p>برق سبک عنان را پروای خار و خس نیست</p></div>
<div class="m2"><p>دام و قفس چه سازد با دل رمیده ما؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست گرهگشایی است از کار هر دو عالم</p></div>
<div class="m2"><p>در دامن توکل پای کشیده ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چند دیده ها را نادیده می شماری</p></div>
<div class="m2"><p>هر جا که پا گذاری فرش است دیده ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نوبهار صائب رنگش به رو نیاید</p></div>
<div class="m2"><p>بر گلشنی که بگذشت رنگ پریده ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نتوان به مرگ پوشید چشم ندیده ما</p></div>
<div class="m2"><p>سیری ندارد از خاک، چون دام، دیده ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتیم وقت پیری در گوشه ای نشینیم</p></div>
<div class="m2"><p>شد تازیانه حرص قد خمیده ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با دل رمیده عشق زخم زبان چه سازد؟</p></div>
<div class="m2"><p>گلها ز خار چیند دامان چیده ما</p></div></div>