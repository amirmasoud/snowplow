---
title: >-
    غزل شمارهٔ ۶۹۴۱
---
# غزل شمارهٔ ۶۹۴۱

<div class="b" id="bn1"><div class="m1"><p>پنهان رخ چو ماه برای چه می کنی؟</p></div>
<div class="m2"><p>خون در دل نگاه برای چه می کنی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ابرام در شکستن دلهای بیگناه</p></div>
<div class="m2"><p>ای ترک کج کلاه برای چه می کنی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگذر ز کاوش دل ما خون گرفتگان</p></div>
<div class="m2"><p>در بحر خون، شناه برای چه می کنی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با چهره ای که آب کند آفتاب را</p></div>
<div class="m2"><p>اندیشه از نگاه برای چه می کنی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهر خراب کردن ما جلوه ای بس است</p></div>
<div class="m2"><p>صد جلوه سر به راه برای چه می کنی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای برق جلوه ای که دو عالم کباب توست</p></div>
<div class="m2"><p>سر در سر گیاه برای چه می کنی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تسخیر ملک دل به نگاهی میسرست</p></div>
<div class="m2"><p>جمعیت سپاه برای چه می کنی؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون بی گناه کشتن عاشق گناه نیست</p></div>
<div class="m2"><p>عذر مرا گناه برای چه می کنی؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رخسار همچو روز ترا زلف شب بس است</p></div>
<div class="m2"><p>روز مرا سیاه برای چه می کنی؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صائب چو رحم در دل سنگین یار نیست</p></div>
<div class="m2"><p>سامان اشک و آه برای چه می کنی؟</p></div></div>