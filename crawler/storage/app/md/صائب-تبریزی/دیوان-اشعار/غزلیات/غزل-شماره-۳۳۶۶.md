---
title: >-
    غزل شمارهٔ ۳۳۶۶
---
# غزل شمارهٔ ۳۳۶۶

<div class="b" id="bn1"><div class="m1"><p>حسن آن روز که آیینه مصفا می‌کرد</p></div>
<div class="m2"><p>عشق در پرده زنگار تماشا می‌کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از نفس سوختگی خال لب ساحل شد</p></div>
<div class="m2"><p>گوهر ما که تلاش دل دریا می‌کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شوق هر چاک که در پرده دل می‌افکند</p></div>
<div class="m2"><p>رخنه‌ای بود که در گنبد مینا می‌کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برق آن حسن جهان‌سوز به یک دم می‌سوخت</p></div>
<div class="m2"><p>شوق چندان که پر و بال مهیا می‌کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سنگ اطفال مرا لنگر بی‌تابی شد</p></div>
<div class="m2"><p>ور نه دیوانه من روی به صحرا می‌کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن که شد گوهر جان دو جهان پامالش</p></div>
<div class="m2"><p>کاش یک بار نگاهی به ته پا می‌کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر طرف نافه دل بود که می‌ریخت به خاک</p></div>
<div class="m2"><p>هر گره کز سر زلف تو صبا وا می‌کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به تو می‌داد خط بندگی یوسف را</p></div>
<div class="m2"><p>گر ترا دیده یعقوب تماشا می‌کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مردم از عشق مراد دو جهان می‌جستند</p></div>
<div class="m2"><p>صائب از عشق همان عشق تمنا می‌کرد</p></div></div>