---
title: >-
    غزل شمارهٔ ۶۵۲۴
---
# غزل شمارهٔ ۶۵۲۴

<div class="b" id="bn1"><div class="m1"><p>چو از تو دیده و دل کامیاب شد هر دو</p></div>
<div class="m2"><p>مرا ازین چه که عالم خراب شد هر دو؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو مصرع است دو زلف که از بیاض عارض او</p></div>
<div class="m2"><p>که بهر مشق جنون انتخاب شد هر دو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فغان که جوهر شمشیر یار و موی میان</p></div>
<div class="m2"><p>یکی به کشتنم از پیچ و تاب شد هر دو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مدار دست ز دامان دل که کعبه و دیر</p></div>
<div class="m2"><p>ز سیل عشق مکرر خراب شد هر دو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا ز دیده و دل بود چشم بیداری</p></div>
<div class="m2"><p>به یک فسانه غفلت به خواب شد هر دو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دریغ و درد که عصیان ما و طاعت ما</p></div>
<div class="m2"><p>یکی شمرده به روز حساب شد هر دو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مکن ملاحظه از مردمان که دیده من</p></div>
<div class="m2"><p>تهی چو حلقه چشم رکاب شد هر دو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه طرف بستم ازان روی آتشین صائب؟</p></div>
<div class="m2"><p>جز این که چشم و دل من پر آب شد هر دو</p></div></div>