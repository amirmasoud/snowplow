---
title: >-
    غزل شمارهٔ ۴۰۱۸
---
# غزل شمارهٔ ۴۰۱۸

<div class="b" id="bn1"><div class="m1"><p>دل گرفته کی از لاله زار بگشاید</p></div>
<div class="m2"><p>ز دستهای نگارین چه کار بگشاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فغان که شاهد گل را بهار کم فرصت</p></div>
<div class="m2"><p>امان نداد که از پانگار بگشاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل پر آبله من به خاک وخون غلطد</p></div>
<div class="m2"><p>گره ز آبله هر که خار بگشاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهان فروزی ماه وستاره چندان است</p></div>
<div class="m2"><p>که مهر پرده صبح از عذار بگشاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنین که دایره راتنگ کرده است سپهر</p></div>
<div class="m2"><p>عجب که غنچه ز باد بهار بگشاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به هیچ چیز جهان دل نمی نهد صائب</p></div>
<div class="m2"><p>اگر کسی نظر اعتبار بگشاید</p></div></div>