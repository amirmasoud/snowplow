---
title: >-
    غزل شمارهٔ ۱۴۶۷
---
# غزل شمارهٔ ۱۴۶۷

<div class="b" id="bn1"><div class="m1"><p>ساحل بحر پر آشوب فنا شمشیرست</p></div>
<div class="m2"><p>مد بسم الله دیوان بقا شمشیرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دم تیغ فنا بیجگران می ترسند</p></div>
<div class="m2"><p>ورنه روشنگر آیینه ما شمشیرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لب پیمانه بود در نظر جرأت ما</p></div>
<div class="m2"><p>گر به چشم تو دم صبح فنا شمشیرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رگ ابری که به احسان چو گهربار شود</p></div>
<div class="m2"><p>عرق خون کند از شرم سخا شمشیرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نفس عیسوی اینجا گرهی بر بادست</p></div>
<div class="m2"><p>دم جان بخش درین معرکه با شمشیرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا رسیدم ز خم تیغ شهادت به مراد</p></div>
<div class="m2"><p>روشنم گشت که محراب دعا شمشیرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نازکان از سخن سرد ز هم می پاشند</p></div>
<div class="m2"><p>بر دل غنچه، دم باد صبا شمشیرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نازکان از سخن سرد ز هم می پاشند</p></div>
<div class="m2"><p>بر دل غنچه، دم باد صبا شمشیرست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون شجاعت نبود، تیغ کند کار نیام</p></div>
<div class="m2"><p>جوهری مردی اگر هست، عصا شمشیرست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ضعف پیری فکند بیجگران را از پای</p></div>
<div class="m2"><p>دل چو افتاد قوی، پشت دو تا شمشیرست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر که دارد سر پرخاش به ما، خوش باشد</p></div>
<div class="m2"><p>خاکساری ز ره و دست دعا شمشیرست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صائب امروز کریمی که به ارباب سئوال</p></div>
<div class="m2"><p>دم آبی دهد از روی سخا شمشیرست</p></div></div>