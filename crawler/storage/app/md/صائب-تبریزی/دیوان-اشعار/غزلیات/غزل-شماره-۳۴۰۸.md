---
title: >-
    غزل شمارهٔ ۳۴۰۸
---
# غزل شمارهٔ ۳۴۰۸

<div class="b" id="bn1"><div class="m1"><p>گریه ابری است که از دامن دل می خیزد</p></div>
<div class="m2"><p>آه گردی است که از رفتن دل می خیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دم جان بخش به هر تیره درونی ندهند</p></div>
<div class="m2"><p>این نسیمی است که از گلشن دل می خیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زاهد خشک کجا، نغمه توحید کجا؟</p></div>
<div class="m2"><p>این نوا از شجر ایمن دل می خیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در حریم دل اگر ماهرخی مهمان نیست</p></div>
<div class="m2"><p>این چه نورست که از روزن دل می خیزد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر حجابی که به علم نظر از پیش نخاست</p></div>
<div class="m2"><p>به دو پیمانه می روشن دل می خیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق درمان گرانجانی ما خواهد کرد</p></div>
<div class="m2"><p>آخر این کوه غم از دامن دل می خیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم بد دور ازان سلسله زلف دراز</p></div>
<div class="m2"><p>که ز هر حلقه او شیون دل می خیزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منع صائب نتوان کرد ز فریاد و فغان</p></div>
<div class="m2"><p>کاین نوایی است که از رفتن دل می خیزد</p></div></div>