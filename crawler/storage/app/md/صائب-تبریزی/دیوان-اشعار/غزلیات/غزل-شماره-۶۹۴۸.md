---
title: >-
    غزل شمارهٔ ۶۹۴۸
---
# غزل شمارهٔ ۶۹۴۸

<div class="b" id="bn1"><div class="m1"><p>یک روز گل از یاسمن صبح نچیدی</p></div>
<div class="m2"><p>پستان سحر خشک شد از بس نمکیدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تبخال زد از آه جگرسوز لب صبح</p></div>
<div class="m2"><p>وز دل تو ستمگر دم سردی نکشیدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد بار فلک پیرهن خویش قبا کرد</p></div>
<div class="m2"><p>یک بار تو بی درد گریبان ندریدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون بلبل تصویر به یک شاخ نشستی</p></div>
<div class="m2"><p>زافسردگی از شاخ به شاخی نپریدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از جذبه آهن شرر از سنگ برآمد</p></div>
<div class="m2"><p>از مستی غفلت تو گرانجان نرهیدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این لنگر تمکین تو چون صورت دیوار</p></div>
<div class="m2"><p>زان است که از غیب ندایی نشنیدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک صبحدم از دیده سرشکی نفشاندی</p></div>
<div class="m2"><p>از برگ گل خویش گلابی نکشیدی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون صورت دیوار درین خانه شدی محو</p></div>
<div class="m2"><p>دنباله یوسف چو زلیخا ندویدی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گردید ز دندان تو دندانه لب جام</p></div>
<div class="m2"><p>یک بار لب خود ز ندامت نگزیدی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زان سنگدل و بی مزه چون میوه خامی</p></div>
<div class="m2"><p>کز عشق به خورشید قیامت نرسیدی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ایام خزان چون شوی ای دانه برومند؟</p></div>
<div class="m2"><p>از خاک چو در فصل بهاران ندمیدی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نگذشته ز آتش، نخورد آب خردمند</p></div>
<div class="m2"><p>تو در پی سامان کبابی و نبیدی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در پختن سودا شب و روز تو سر آمد</p></div>
<div class="m2"><p>زین دیگ به جز زهر ندامت چه چشیدی؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پیوسته چراگاه تو از چون و چرا بود</p></div>
<div class="m2"><p>از گلشن بی چون و چرا رنگ ندیدی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از زنگ قساوت دل خود را نزدودی</p></div>
<div class="m2"><p>جز سبزه بیگانه ازین باغ نچیدی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از بار تواضع قد افلاک دوتا ماند</p></div>
<div class="m2"><p>وز کبر تو یک ره چو مه نو نخمیدی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از شوق شکر مور برآورد پر و بال</p></div>
<div class="m2"><p>صائب تو درین عالم خاکی چه خزیدی؟</p></div></div>