---
title: >-
    غزل شمارهٔ ۵۱۸۹
---
# غزل شمارهٔ ۵۱۸۹

<div class="b" id="bn1"><div class="m1"><p>گردی است صبح ازنفس راستین عشق</p></div>
<div class="m2"><p>داغی است مهر از جگر آتشین عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قفل درنشاط و سرورست قاف عقل</p></div>
<div class="m2"><p>دندانه کلید بهشت است شین عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در چشم آفتاب کشد میل خوشه اش</p></div>
<div class="m2"><p>هر دانه ای که غوطه خورد در زمین عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون گل تمام پرده گوش است آسمان</p></div>
<div class="m2"><p>از اشتیاق زمزمه دلنشین عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نزدیک گشته است که چون نار شق شود</p></div>
<div class="m2"><p>این نه صدف ز شوکت در سمین عشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسن آنچنان که هست تماشای خود نکرد</p></div>
<div class="m2"><p>آیینه دار حسن نشد تا جبین عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب هوای گلشن جنت نمی کند</p></div>
<div class="m2"><p>در مغز هر که ریشه کند یاسمین عشق</p></div></div>