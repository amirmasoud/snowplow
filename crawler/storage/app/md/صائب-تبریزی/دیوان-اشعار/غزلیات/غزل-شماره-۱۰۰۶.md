---
title: >-
    غزل شمارهٔ ۱۰۰۶
---
# غزل شمارهٔ ۱۰۰۶

<div class="b" id="bn1"><div class="m1"><p>گردش پرگار ما را حلقه مویی بس است</p></div>
<div class="m2"><p>مرکز سرگشتگی ها خال دلجویی بس است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست با آیینه روی حرف ما چون طوطیان</p></div>
<div class="m2"><p>باعث گفتار ما چشم سخنگویی بس است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بند آهن بر سبکروحان گرانی می کند</p></div>
<div class="m2"><p>گردن باریک ما را حلقه مویی بس است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر به صحرا می دهد شوریدگان را ناله ای</p></div>
<div class="m2"><p>یک جهان آهوی وحشت دیده را هویی بس است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مطلب آزادگان دست از دو عالم شستن است</p></div>
<div class="m2"><p>همچو سرو از گلستان ما را لب جویی بس است</p></div></div>