---
title: >-
    غزل شمارهٔ ۲۵۸۸
---
# غزل شمارهٔ ۲۵۸۸

<div class="b" id="bn1"><div class="m1"><p>خام دستانی که پشت پا به دنیا می زنند</p></div>
<div class="m2"><p>در حقیقت دست رد بر زاد عقبی می زنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بندگی را می کنند از خط آزادی سجل</p></div>
<div class="m2"><p>ساده لوحانی که حرف ترک دنیا می زنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زود خواهد طشتشان افتادن از بام زوال</p></div>
<div class="m2"><p>مهر خود چون آفتاب آنها که بالا می زند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چاره جویان را غم بیچارگان بار دل است</p></div>
<div class="m2"><p>ناتوانان تکیه بر دوش مسیحا می زند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رهنوردانی که بردارند بار از دوش خلق</p></div>
<div class="m2"><p>سینه چون کشتی به دریا بی محابا می زنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می کنند آماده اول در جگر جای خراش</p></div>
<div class="m2"><p>دوربینان تیشه گر بر سنگ خارا می زنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یافتند از ذوق کار آنها که مزد کار خویش</p></div>
<div class="m2"><p>خنده ها بر اهتمام کارفرما می زنند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در گداز انتظار روز محشر نیستند</p></div>
<div class="m2"><p>دلخراشان سکه بر نقد خود اینجا می زنند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خانه بر دوشان زطوف کعبه برگردیده اند</p></div>
<div class="m2"><p>خیمه خود تا گرانباران به صحرا می زنند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اهل وحدت را نباشد جنگ با خصم برون</p></div>
<div class="m2"><p>از شکست خویشتن بر قلب اعدا می زنند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عاشقان در عین وصل، از بیقراریهای شوق</p></div>
<div class="m2"><p>پیچ و تاب موج در آغوش دریا می زنند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دردمندان صائب از پا گر برون آرند خار</p></div>
<div class="m2"><p>غوطه در خونابه دل نیزه بالا می زنند</p></div></div>