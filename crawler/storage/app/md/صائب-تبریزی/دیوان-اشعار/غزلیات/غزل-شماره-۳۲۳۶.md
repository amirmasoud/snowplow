---
title: >-
    غزل شمارهٔ ۳۲۳۶
---
# غزل شمارهٔ ۳۲۳۶

<div class="b" id="bn1"><div class="m1"><p>به می آن کس که کلفت از دل پرشور من شوید</p></div>
<div class="m2"><p>به شبنم رنگ خون از لاله خونین کفن شوید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر دریای رحمت این سبکدستی نفرماید</p></div>
<div class="m2"><p>که را دارم غبار از چهره سیلاب من شوید؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زبان آتشین در آستین دارد گرستن را</p></div>
<div class="m2"><p>به اشک گرم روی خویش شمع انجمن شوید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر بر شوره زار افتد رهش گلزار می سازد</p></div>
<div class="m2"><p>به هر آبی که روی خویش آن سیمین بدن شوید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ید بیضاست در روشنگری لطف عزیزان را</p></div>
<div class="m2"><p>غبار از دیده یعقوب بوی پیرهن شوید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زمی گلگونه شرم و حیای یار افزون شد</p></div>
<div class="m2"><p>عرق کی می تواند سرخی از سیب ذقن شوید؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زغیرت نیست اشک بلبلان را بهره ای، ورنه</p></div>
<div class="m2"><p>چرا هر صبح شبنم روی گلهای چمن شوید؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تواند از سر من هر که بیرون برد سودا را</p></div>
<div class="m2"><p>به آسانی سیاهی از پر و بال زغن شوید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زخجلت گر نگردد هر سر مویم رگ ابری</p></div>
<div class="m2"><p>که از آلودگی روز جزا دامن من شوید؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هنرمندی ندارد عاقبت، زحمت مکش صائب</p></div>
<div class="m2"><p>که دست خود به خون از کار شیرین کوهکن شوید</p></div></div>