---
title: >-
    غزل شمارهٔ ۳۸۷۹
---
# غزل شمارهٔ ۳۸۷۹

<div class="b" id="bn1"><div class="m1"><p>کریم اوست که خود را بخیل می داند</p></div>
<div class="m2"><p>عزیز اوست که خود را ذلیل می داند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین محیط چو غواص هر که محرم شد</p></div>
<div class="m2"><p>نفس کشیدن خود قال وقیل می داند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی که آتش خشم و غضب فرو خورده است</p></div>
<div class="m2"><p>میان شعله حضور خلیل می داند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوشم به گریه خونین که آن بهشتی روی</p></div>
<div class="m2"><p>سرشک تلخ مرا سلسبیل می داند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازین سیاه درونان به اهل دل بگریز</p></div>
<div class="m2"><p>که کعبه چاره اصحاب فیل می داند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کبودی رخ خود را زسیلی اخوان</p></div>
<div class="m2"><p>عزیز مصر به از رود نیل می داند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سفینه ای که به گل در کنار ننشسته است</p></div>
<div class="m2"><p>چه قدر بادمراد رحیل می داند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز چرخ کام به شکر دروغ نتوان یافت</p></div>
<div class="m2"><p>که راه حیله سایل بخیل می داند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زبان راه بیابان اگر چه پیچیده است</p></div>
<div class="m2"><p>به صد هزار روایت دلیل می داند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>امید رحم ز خورشید طلعتی است مرا</p></div>
<div class="m2"><p>که خون شبنم گل را سبیل می داند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دلی که محرم اسرار غیب شد صائب</p></div>
<div class="m2"><p>نسیم را نفس جبرئیل می داند</p></div></div>