---
title: >-
    غزل شمارهٔ ۳۸۳۶
---
# غزل شمارهٔ ۳۸۳۶

<div class="b" id="bn1"><div class="m1"><p>زخط عذار تو بی آب وتاب خواهد شد</p></div>
<div class="m2"><p>زهاله ماه تو پا در در رکاب خواهد شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخی که در جگر لاله خون ازومی سوخت</p></div>
<div class="m2"><p>سیاه روزتر از مشک ناب خواهد شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لبی که از سخنش می چکید آب حیات</p></div>
<div class="m2"><p>جگر گداز چو موج سراب خواهد شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلی که پشت به کوه گران زسختی داشت</p></div>
<div class="m2"><p>زسیل اشک ندامت خراب خواهد شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زبرگزیر خزان آفتاب طلعت تو</p></div>
<div class="m2"><p>شکسته رنگتر از ماهتاب خواهد شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زخط ستاره خال تو می رودبه وبال</p></div>
<div class="m2"><p>خمار چشم مبدل به خواب خواهد شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمند زلف تو با آن درازدستها</p></div>
<div class="m2"><p>چو خال یک گره از پیچ وتاب خواهد شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز دستبرد خط سبز تیغ غمزه تو</p></div>
<div class="m2"><p>به زیر پرده نگار آب خواهد شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل سیاه تو چون داغ لاله سیراب</p></div>
<div class="m2"><p>به آتش جگر خود کباب خواهد شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زخط زمانه ترا می کشد به پای حساب</p></div>
<div class="m2"><p>تلافی ستم بی حساب خواهد شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رخی که صائب ازودیده شد نگارستان</p></div>
<div class="m2"><p>سیاه روز چو پر غراب خواهد شد</p></div></div>