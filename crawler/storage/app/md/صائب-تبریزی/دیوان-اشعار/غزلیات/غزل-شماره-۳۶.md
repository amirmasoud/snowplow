---
title: >-
    غزل شمارهٔ ۳۶
---
# غزل شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>چشم می پوشی ازان رخسار جان پرور چرا؟</p></div>
<div class="m2"><p>می کنی آیینه را پنهان ز روشنگر چرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غیرتی کن چون گهر جیب صدف را چاک کن</p></div>
<div class="m2"><p>می خوری سیلی درین دریای بی لنگر چرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خرده جان می جهد از سنگ بیرون چون شرار</p></div>
<div class="m2"><p>می زنی چندین گره بر روی یکدیگر چرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صیقلی کن سینه خود را به آه آتشین</p></div>
<div class="m2"><p>می کنی دریوزهٔ نور از مه و اختر چرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پاره کن زنار جوهر از میان خویشتن</p></div>
<div class="m2"><p>خون مردم می خوری ای تیغ بدگوهر چرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست جای پر فشانی چار دیوار قفس</p></div>
<div class="m2"><p>مانده ای در تنگنای طارم اخضر چرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سپند شوخ، مجمر تنگنای دوزخ است</p></div>
<div class="m2"><p>بر نمی آیی چو بوی عود ازین مجمر چرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می توانی شد چراغ خلوت روحانیان</p></div>
<div class="m2"><p>می کنی ضبط نفس در زیر خاکستر چرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آفتاب دولت بیدار بر بالین توست</p></div>
<div class="m2"><p>می شوی با خواب ای بی درد همبستر چرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیستی صائب حریف تلخی ایام هجر</p></div>
<div class="m2"><p>جان نمی سازی نثار صحبت شکر چرا؟</p></div></div>