---
title: >-
    غزل شمارهٔ ۶۲۴۲
---
# غزل شمارهٔ ۶۲۴۲

<div class="b" id="bn1"><div class="m1"><p>ز آه من ندارد هیچ پروا کج کلاه من</p></div>
<div class="m2"><p>ز شوخی می کند چون زلف خود بازی به آه من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به استغنا دل از عاشق ستاند کم نگاه من</p></div>
<div class="m2"><p>به شمشیر تغافل ملک گیرد پادشاه من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خدا زین برق عالمسوز جانان را نگه دارد!</p></div>
<div class="m2"><p>که مژگان می شود انگشت زنهار از نگاه من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمی داند خس و خاشاک بال شعله می گردد</p></div>
<div class="m2"><p>رقیب از ساده لوحی خار می ریزد به راه من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غرور یار از اظهار عجز من یکی صد شد</p></div>
<div class="m2"><p>به کار مدعی آمد درین دعوی گواه من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پریشان کرد خط یار اوراق حواسم را</p></div>
<div class="m2"><p>که را گویم که از گردی پریشان شد سپاه من؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محبت جمع با تن پروری صائب نمی گردد</p></div>
<div class="m2"><p>وگرنه می شود هر سایه خاری پناه من</p></div></div>