---
title: >-
    غزل شمارهٔ ۲۱۲۸
---
# غزل شمارهٔ ۲۱۲۸

<div class="b" id="bn1"><div class="m1"><p>پیچیدن سر از دو جهان افسر عشق است</p></div>
<div class="m2"><p>برخاستن از جان، علم لشکر عشق است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلگونه رخسار گهر گرد یتیمی است</p></div>
<div class="m2"><p>خواری و غریبی پدر و مادر عشق است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر دفتر عقل است ز جمعیت اوراق</p></div>
<div class="m2"><p>از هر دو جهان فرد شدن دفتر عشق است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تنها نگرفته است همین روی زمین را</p></div>
<div class="m2"><p>چون بیضه فلک در ته بال و پر عشق است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر بر خط حکمش ننهد خاک، چه سازد؟</p></div>
<div class="m2"><p>جایی که فلک بنده و فرمانبر عشق است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حرفش ز دل سوخته ام دود برآورد</p></div>
<div class="m2"><p>آتش بود آن آب که در گوهر عشق است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر حلقه در، در حرم وصل برد رشک</p></div>
<div class="m2"><p>هر حلقه چشمی که ادب پرور عشق است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون نار کند شق دل مینای فلک را</p></div>
<div class="m2"><p>این باده پر زور که در ساغر عشق است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از عشق بود هر که رسیده است به جایی</p></div>
<div class="m2"><p>پرواز کمالات به بال و پر عشق است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شیرین سخن افتاده اگر خامه صائب</p></div>
<div class="m2"><p>زان است که نیشکر بوم و بر عشق است</p></div></div>