---
title: >-
    غزل شمارهٔ ۳۶۶۱
---
# غزل شمارهٔ ۳۶۶۱

<div class="b" id="bn1"><div class="m1"><p>گل عذار تو از درد نیمرنگ مباد!</p></div>
<div class="m2"><p>به خنده تو ز تبخاله جای تنگ مباد!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مباد نبض تو چون موج مضطرب هرگز</p></div>
<div class="m2"><p>میان طبع تو و اعتدال جنگ مباد!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پی علاج تو کز تب چو آفتاب شدی</p></div>
<div class="m2"><p>مسیح را به فلک فرصت درنگ مباد!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سبک، گرانی خود درد از سرت ببرد</p></div>
<div class="m2"><p>چو آفتاب بر آیینه تو زنگ مباد!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به چهره ات عرق سرد و گرم و تر بدود</p></div>
<div class="m2"><p>غبار عارضه را بر رخت درنگ مباد!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز جام صحت جاوید لاله گون باشی</p></div>
<div class="m2"><p>بهار عافیتت در خمار رنگ مباد!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>امیدوار چنانم به لطف حق صائب</p></div>
<div class="m2"><p>که آه من به فلک بیش ازین به جنگ مباد!</p></div></div>