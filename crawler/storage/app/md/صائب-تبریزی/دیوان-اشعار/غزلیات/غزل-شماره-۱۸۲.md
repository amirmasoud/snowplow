---
title: >-
    غزل شمارهٔ ۱۸۲
---
# غزل شمارهٔ ۱۸۲

<div class="b" id="bn1"><div class="m1"><p>جلوه برقی است در میخانه هشیاری مرا</p></div>
<div class="m2"><p>از پی تغییر بالین است بیداری مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون فلاخن کز وصال سنگ دست افشان شود</p></div>
<div class="m2"><p>می دهد رطل گران از غم سبکباری مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نیابم در سخن میدان، نمی آیم به حرف</p></div>
<div class="m2"><p>همچو طوطی لوح تعلیم است همواری مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست چون ریگ روانم در سفر واماندگی</p></div>
<div class="m2"><p>راحت منزل بود از نرم رفتاری مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس که چون آیینه دیدم از جهان نادیدنی</p></div>
<div class="m2"><p>نیست بر خاطر غبار از چرخ زنگاری مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرد بی برگ و نوا را کاروان در کار نیست</p></div>
<div class="m2"><p>می کند چون تیغ، عریانی سپر داری مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوسفندی از دهان گرگ می آرد برون</p></div>
<div class="m2"><p>هر که چون یوسف کند ز اخوان خریداری مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بس که می سوزد دلش بر بی قراری های من</p></div>
<div class="m2"><p>شمع بالین می شود انگشت زنهاری مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست غم از تیر باران جوشن داود را</p></div>
<div class="m2"><p>می کند عشق از غم عالم نگهداری مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نسبت من با گنه، آیینه و خاکسترست</p></div>
<div class="m2"><p>روسفیدی هاست حاصل از سیه کاری مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیست صائب چاه و زندان بر دل من ناگوار</p></div>
<div class="m2"><p>همچو یوسف می فزاید عزت از خواری مرا</p></div></div>