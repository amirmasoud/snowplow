---
title: >-
    غزل شمارهٔ ۱۳۸۴
---
# غزل شمارهٔ ۱۳۸۴

<div class="b" id="bn1"><div class="m1"><p>آه سرم در تو ای آتش عنان خواهد گرفت</p></div>
<div class="m2"><p>خون بلبل را خوان از گلستان خواهد گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شعله حسن جهانسوزت فرو خواهد نشست</p></div>
<div class="m2"><p>لاله ات را داغ حسرت در میان خواهد گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سنبل زلفت ز یکدیگر پریشان می شود</p></div>
<div class="m2"><p>هر گرفتاری به شاخی آشیان خواهد گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرمه آشوب خواهد ریخت از مژگان تو</p></div>
<div class="m2"><p>ماه نو از دست ابرویت کمان خواهد گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ملک حسنت را کز آن صبح تجلی گوشه ای است</p></div>
<div class="m2"><p>لشکر خط قیروان تا قیروان خواهد گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شعله واسوختن از سینه ها سر می کشد</p></div>
<div class="m2"><p>آتش بیتابیت در مغز جان خواهد گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بوسه دندان تغافل بر جگر خواهد نهاد</p></div>
<div class="m2"><p>سجده احرام سفر زان آستان خواهد گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رنگ ریزی های پی در پی تماشاکردنی است</p></div>
<div class="m2"><p>در گلستانت دم سرد خزان خواهد گرفت</p></div></div>