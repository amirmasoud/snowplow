---
title: >-
    غزل شمارهٔ ۱۱۶۱
---
# غزل شمارهٔ ۱۱۶۱

<div class="b" id="bn1"><div class="m1"><p>آسمان از شور دلهای کباب آسوده است</p></div>
<div class="m2"><p>کوه تمکین خم از جوش شراب آسوده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبح محشر بی سبب ما را به دیوان می کشد</p></div>
<div class="m2"><p>خود حساب از پرسش روز حساب آسوده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل نسوزد عشق را بر گریه های آتشین</p></div>
<div class="m2"><p>آتش از اشک جگر سوز کباب آسوده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق را پروای چشم عیبجوی نقل نیست</p></div>
<div class="m2"><p>از گزند چشم خفاش، آفتاب آسوده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با تهی چشمان ندارد اضطراب عشق کار</p></div>
<div class="m2"><p>از پریدن حلقه چشم رکاب آسوده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خیال آسوده گردد دیده ارباب فکر</p></div>
<div class="m2"><p>دیده اصحاب غفلت گر ز خواب آسوده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کهنه اوراق دل ما قابل ترتیب نیست</p></div>
<div class="m2"><p>از غم شیرازه کردن این کتاب آسوده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که ما را ایمن از بیداد گردون دید، گفت</p></div>
<div class="m2"><p>این هوا را بین که در قصر حباب آسوده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در شبستانی که من محو تجلی گشته ام</p></div>
<div class="m2"><p>نبض سیمابش ز موج اضطراب آسوده است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کار خار پا کند زر در کف دریا دلان</p></div>
<div class="m2"><p>چون ندارد گوهری در کف سحاب آسوده است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیست حاجت حسن ذاتی را به خال عارضی</p></div>
<div class="m2"><p>این غزل صائب ز داغ انتخاب آسوده است</p></div></div>