---
title: >-
    غزل شمارهٔ ۸۸۰
---
# غزل شمارهٔ ۸۸۰

<div class="b" id="bn1"><div class="m1"><p>در هوای ابر لازم نیست در مینا شراب</p></div>
<div class="m2"><p>می کند هر قطره باران کار صد دریا شراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب نشین با دختر رز عمر جاوید آورد</p></div>
<div class="m2"><p>فیض آب خضر دارد در دل شبها شراب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تنگنای شهر جای نشأه سرشار نیست</p></div>
<div class="m2"><p>داد جولان می دهد در دامن صحرا شراب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لاله در خارا خمار از باده لعلی شکست</p></div>
<div class="m2"><p>می شود از سنگ بهر میکشان پیدا شراب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می زنم جوشی به زور باده در دیر مغان</p></div>
<div class="m2"><p>سالها شد تا چو خم دارد مرا بر پا شراب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسن سعی نوبهار از سرو و گل ظاهر شود</p></div>
<div class="m2"><p>می نماید خویش را در ساغر و مینا شراب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تیغ کوه از چشمه سار ابر گردد آبدار</p></div>
<div class="m2"><p>سربلندان را رسد از عالم بالا شراب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست از تدبیر، می دادن به ما دیوانگان</p></div>
<div class="m2"><p>کار روغن می کند بر آتش سودا شراب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما خمارآلودگان را بی شرابی می کشد</p></div>
<div class="m2"><p>عمر ما باقی است، تا باقی است در مینا شراب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست پای شمع را از شمع جز ظلمت نصیب</p></div>
<div class="m2"><p>زنگ نتوانست بردن از دل مینا شراب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون پر پروانه سوزد پرده ناموس را</p></div>
<div class="m2"><p>چون شود از عکس ساقی آتشین سیما شراب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برق عالمسوز نتواند به گرد ما رسید</p></div>
<div class="m2"><p>این چنین کز خویش بیرون می برد ما را شراب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>باده می باید که باشد، عقل گو هرگز مباش</p></div>
<div class="m2"><p>در کدوی سر خرد کم به که در مینا شراب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زاهدان خشک را چون توبه می در هم شکست</p></div>
<div class="m2"><p>این سزای آن که می گیرد به استغنا شراب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دست چون از دامن مینای می کوته کنیم؟</p></div>
<div class="m2"><p>می دهد ما را خبر از عالم بالا شراب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا نمی در جویبار همت سرشار هست</p></div>
<div class="m2"><p>کی کند صائب گدایی از در دلها شراب؟</p></div></div>