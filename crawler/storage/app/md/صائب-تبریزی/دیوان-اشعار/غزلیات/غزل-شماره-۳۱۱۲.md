---
title: >-
    غزل شمارهٔ ۳۱۱۲
---
# غزل شمارهٔ ۳۱۱۲

<div class="b" id="bn1"><div class="m1"><p>حضور قلب کی در سینه پرشور می باشد؟</p></div>
<div class="m2"><p>کجا آسودگی در خانه زنبور می باشد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زکشتن زنده جاوید می گردند اهل حق</p></div>
<div class="m2"><p>که از دار سیاست رایت منصور می باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیاید در حیات آن کس که بیرون از تن خاکی</p></div>
<div class="m2"><p>اگرچه هست بر روی زمین، در گور می باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگردد شوق رهرو بیش از نزدیکی منزل</p></div>
<div class="m2"><p>در آن وادی که بیش از راه، منزل دور می باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا نگذاشت در سر هوش لعل آبدار او</p></div>
<div class="m2"><p>می ممزوج اینجا بیشتر پرزور می باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زچشم خوش نگاهان بر ندارد چشم وقت خط</p></div>
<div class="m2"><p>حقوق آشنایی هر که را منظور می باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به چشم کم مبین زنهار در دولت ضعیفان را</p></div>
<div class="m2"><p>که خال چهره ملک سلیمان مور می باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کند از سنگ پیدا حسن عالمسوز عاشق را</p></div>
<div class="m2"><p>چراغ طور را پروانه کوه طور می باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زخوان رزق اگر صائب به دل خوردن شوم قانع</p></div>
<div class="m2"><p>نمکدانم همان از دیده های شور می باشد</p></div></div>