---
title: >-
    غزل شمارهٔ ۴۲۱۸
---
# غزل شمارهٔ ۴۲۱۸

<div class="b" id="bn1"><div class="m1"><p>حاشا که خلق کار برای خدا کنند</p></div>
<div class="m2"><p>تعظیم مصحف از پی مهر طلا کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این جامه حریر که مخصوص کعبه است</p></div>
<div class="m2"><p>پوشند اگر به دیر به او اقتدا کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکر به کام زاغ فشانند بی دریغ</p></div>
<div class="m2"><p>در استخوان مضایقه هابا هما کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون اژدها کلید در گنج گوهرند</p></div>
<div class="m2"><p>وز بهر نیم حبه جدل با گدا کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گردند گرد دفتر اعمال خویشتن</p></div>
<div class="m2"><p>هر طاعتی که نیست ریایی قضاکنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر جا که بگذرد سخن از سوزن مسیح</p></div>
<div class="m2"><p>خود را به زور جاذبه آهن ربا کنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مصحف به زیر پای گذارند از غرور</p></div>
<div class="m2"><p>دستار عقل از سر جبریل وا کنند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دنبال زردرویی حرص اوفتاده اند</p></div>
<div class="m2"><p>چون برگ کاه پیروی کهربا کنند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر هر طرف که روی نهند این سیه دلان</p></div>
<div class="m2"><p>در آبروی ریخته خود شنا کنند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شرم وحیا چو لازمه چشم روشن است</p></div>
<div class="m2"><p>این کورباطنان ز چه شرم وحیا کنند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب بگیر گوشه عزلت که اهل دل</p></div>
<div class="m2"><p>این درد را به گوشه نشینی دوا کنند</p></div></div>