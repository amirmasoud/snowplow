---
title: >-
    غزل شمارهٔ ۲۶۲۷
---
# غزل شمارهٔ ۲۶۲۷

<div class="b" id="bn1"><div class="m1"><p>آبروی کعبه گر از چشمه زمزم بود</p></div>
<div class="m2"><p>کعبه دل را صفا از دیده پرنم بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خودآرا، دست بر دنیا فشاندن مشکل است</p></div>
<div class="m2"><p>در ته سنگ است هر دستی که با خاتم بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می کند عالم به چشم سوزن عیسی سیاه</p></div>
<div class="m2"><p>تار و پود این جهان گر رشته مریم بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که نتواند زدوش خلق باری برگرفت</p></div>
<div class="m2"><p>از گرانجانی حیاتش بار بر عالم بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صبح، وصل مهر تابان از دم جان بخش یافت</p></div>
<div class="m2"><p>می شود روشن چراغش هر که صاحب دم بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غنچه خسبان بیخبر از راز عالم نیستند</p></div>
<div class="m2"><p>کاسه زانوی اهل فکر، جام جم بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن که اول شعر گفت آدم صفی الله بود</p></div>
<div class="m2"><p>طبع موزون حجت فرزندی آدم بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که صائب نفس سرکش را نسازد زیردست</p></div>
<div class="m2"><p>در حقیقت کمتر از زال است اگر رستم بود</p></div></div>