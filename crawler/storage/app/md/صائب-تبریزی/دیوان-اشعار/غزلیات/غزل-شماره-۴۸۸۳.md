---
title: >-
    غزل شمارهٔ ۴۸۸۳
---
# غزل شمارهٔ ۴۸۸۳

<div class="b" id="bn1"><div class="m1"><p>دیده ما گرز خون رنگین نباشد گومباش</p></div>
<div class="m2"><p>حلقه بیرون در زرین نباشد گو مباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیل چشم زخم باشد باده را جام سفال</p></div>
<div class="m2"><p>اهل دل را جامه (گر) رنگین نباشد گو مباش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می شود از وسعت مشرب گوارا تلخ و شور</p></div>
<div class="m2"><p>نقل میخواران اگرشیرین نباشد گو مباش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون بنای زندگی نقش بر آبی بیش نیست</p></div>
<div class="m2"><p>ساحت منزل اگر سنگین نباشد گو مباش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لذت ادراک معنی دلگشای ما بس است</p></div>
<div class="m2"><p>رزق ما گر از سخن تحسین نباشد گو مباش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی آتشناک را پیرایه ای در کار نیست</p></div>
<div class="m2"><p>شمع را فانوس اگر رنگین نباشد گومباش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواب سنگین می کند هموار خشت و خاک را</p></div>
<div class="m2"><p>گر ز مخمل بستر و بالین نباشد گو مباش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست از قحط سخن سنجان سخنور راملال</p></div>
<div class="m2"><p>گردرین بستانسراگلچین نباشد گومباش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیده آلودگان شایسته دیدار نیست</p></div>
<div class="m2"><p>خلق را گر دیده حق بین نباشد گو مباش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می کند منقار طوطی راسخن تنگ شکر</p></div>
<div class="m2"><p>عیش ما صائب اگر شیرین نباشد گومباش</p></div></div>