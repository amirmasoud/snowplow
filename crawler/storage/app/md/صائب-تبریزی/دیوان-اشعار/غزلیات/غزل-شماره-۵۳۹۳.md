---
title: >-
    غزل شمارهٔ ۵۳۹۳
---
# غزل شمارهٔ ۵۳۹۳

<div class="b" id="bn1"><div class="m1"><p>یوسفستان گشت دنیا از نظر پوشیدنم</p></div>
<div class="m2"><p>یک گل بی خار شد عالم زدامن چیدنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرد دل می گشت بر گرد جهان گردیدنی</p></div>
<div class="m2"><p>کرد مستغنی ز عالم گرد دل گردیدنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوری ره سرمه می کرد استخوانهای مرا</p></div>
<div class="m2"><p>گر نمی آورد پایی در میان، لغزیدنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داغ دارد شعله سرگرمیم خورشید را</p></div>
<div class="m2"><p>هر سر ناخن هلالی شد ز سر خاریدنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می گشایم در هوای رفتن آغوش وداع</p></div>
<div class="m2"><p>نیست از غفلت چو گل در بوستان خندیدنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر به این تمکین مرا از خاک خواهی بر گرفت</p></div>
<div class="m2"><p>بیقراریهای دل خواهد ز هم پاشیدنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیچ و تاب دل مرا آخر به زلف او رساند</p></div>
<div class="m2"><p>این ره خوابیده شد کوتاه از پیچیدنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می زنم برهم ز شوق نیستی بال نشاط</p></div>
<div class="m2"><p>نیست بهر خرده جان چون شرر لرزیدنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ذره نا چیزم اما از فروغ داغ عشق</p></div>
<div class="m2"><p>آب می گردد به چشم آفتاب از دیدنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی دماغی باعث بیماری من گشته است</p></div>
<div class="m2"><p>بیشتر سنگین شود بیماری از پرسیدنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ظرف وصل او که دارد، کز نسیم مژده ای</p></div>
<div class="m2"><p>تنگ شد بر آسمانها جای از بالیدنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ان گرامی گوهرم صائب که در مصر وجود</p></div>
<div class="m2"><p>پله میزان ید بیضا شد از سنجیدنم</p></div></div>