---
title: >-
    غزل شمارهٔ ۵۸۵۶
---
# غزل شمارهٔ ۵۸۵۶

<div class="b" id="bn1"><div class="m1"><p>ما در محیط حادثه لنگر فکنده ایم</p></div>
<div class="m2"><p>در آب تیغ، دام چو جوهر فکنده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دستی است کهکشان که به عالم فشانده ایم</p></div>
<div class="m2"><p>خورشید افسری است که از سر فکنده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دیده ستاره نمکدان شکسته است</p></div>
<div class="m2"><p>شوری که ما به قلزم اخضر فکنده ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مانند عود خام، هوسهای خام را</p></div>
<div class="m2"><p>بر یکدگر شکسته به مجمر فکنده ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از ما مجوی گریه ظاهر که چون صدف</p></div>
<div class="m2"><p>در صحن دل بساط ز گوهر فکنده ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر تلخیی که قسمت ما کرده است چرخ</p></div>
<div class="m2"><p>می نام کرده ایم و به ساغر فکنده ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زان آستین که بر رخ عالم فشانده ایم</p></div>
<div class="m2"><p>دیهیم نخوت از سر قیصر فکنده ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از عالم جهات به همت گذشته ایم</p></div>
<div class="m2"><p>از زور نقش رخنه به ششدر فکنده ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما از شکوه خصم محابا نمی کنیم</p></div>
<div class="m2"><p>دایم به فیل پشه لاغر فکنده ایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در سنگلاخ دهر ز پیشانی گشاد</p></div>
<div class="m2"><p>آیینه را ز چشم سکندر فکنده ایم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر آتش که دست کلیم است داغ آن</p></div>
<div class="m2"><p>در بی خودی کباب مکرر فکنده ایم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صائب ز هر پیاله که بر لب نهاده ایم</p></div>
<div class="m2"><p>در سینه طرح عالم دیگر فکنده ایم</p></div></div>