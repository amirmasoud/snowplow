---
title: >-
    غزل شمارهٔ ۵۲۸۷
---
# غزل شمارهٔ ۵۲۸۷

<div class="b" id="bn1"><div class="m1"><p>در ته یک پیرهن از یار دور افتاده ام</p></div>
<div class="m2"><p>آه کز نزدیکی بسیار دورافتاده ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می کشم خمیازه بر آغوش در آغوش یار</p></div>
<div class="m2"><p>همچو مرکز از خط پرگار دور افتاده ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست تدبیری به جز دوری ز نزدیکی مرا</p></div>
<div class="m2"><p>من که از نزدیکی بسیار دورافتاده ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بهشت افتاد بیرون آدم و خندان نشد</p></div>
<div class="m2"><p>چون نگریم من که از دلدار دورافتاده ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تیشه فرهاد گردیده است هرمو برتنم</p></div>
<div class="m2"><p>تاازان معشوق شیرین کاردورافتاده ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد نفس انگشت زنهار از دهان تلخ من</p></div>
<div class="m2"><p>تاازان لبهای شکر بار دورافتاده ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست ممکن بازگشت من به عمر جاودان</p></div>
<div class="m2"><p>این چنین کز بزم او این بار دور افتاده ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیرکنعان چون به من در گریه همچشمی کند</p></div>
<div class="m2"><p>او ز یوسف من ز یوسف زار دورافتاده ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون توانم عمر صرف جستجوی یار کرد</p></div>
<div class="m2"><p>من که از خود بیشتر از یار دور افتاده ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می پرد چشمم به خواب نیستی همچون شرار</p></div>
<div class="m2"><p>از تو ای آتشین رخسار دورافتاده ام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گاه می خندم ز شادی گاه می گریم ز درد</p></div>
<div class="m2"><p>زان که هم از یارو هم از اغیار دورافتاده ام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کیست صائب تا زحال او خبر بخشد مرا</p></div>
<div class="m2"><p>مدتی شد کز دل افگار دورافتاده ام</p></div></div>