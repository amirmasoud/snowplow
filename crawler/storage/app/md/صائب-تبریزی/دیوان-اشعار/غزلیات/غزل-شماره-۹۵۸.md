---
title: >-
    غزل شمارهٔ ۹۵۸
---
# غزل شمارهٔ ۹۵۸

<div class="b" id="bn1"><div class="m1"><p>صیقل آیینه ما گوشه ابروی ماست</p></div>
<div class="m2"><p>عینک ما چون حباب از کاسه زانوی ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه در صحرای امکان پای خواب آلوده ایم</p></div>
<div class="m2"><p>لامکان پر گرد وحشت از رم آهوی ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از شبیخون اجل منصور ما را باک نیست</p></div>
<div class="m2"><p>دار مانند کمان حلقه بر بازوی ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از کمینگاه حوادث طبل وحشت خورده ایم</p></div>
<div class="m2"><p>کار پیکان می کند هر کس که در پهلوی ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غنچه سان هر چند سر در جیب خود دزدیده ایم</p></div>
<div class="m2"><p>عطسه بی اختیار صبحدم از بوی ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فکر رنگین از بهار خاطر ما لاله ای است</p></div>
<div class="m2"><p>مصرع برجسته سروی از کنار جوی ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه ما صائب زبان لاف را پیچیده ایم</p></div>
<div class="m2"><p>گوش بر هر جا که اندازند گفت و گوی ماست</p></div></div>