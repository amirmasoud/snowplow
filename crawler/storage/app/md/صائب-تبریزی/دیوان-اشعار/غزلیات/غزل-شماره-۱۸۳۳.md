---
title: >-
    غزل شمارهٔ ۱۸۳۳
---
# غزل شمارهٔ ۱۸۳۳

<div class="b" id="bn1"><div class="m1"><p>فغان که گرد سر او نمی توانم گشت</p></div>
<div class="m2"><p>چو زلف بر کمر او نمی توانم گشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همیشه گرد دلش بی حجاب می گردم</p></div>
<div class="m2"><p>اگر چه گرد سر او نمی توانم گشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بس که تیر نگاهش بلند پروازست</p></div>
<div class="m2"><p>ز دور در نظر او او نمی توانم گشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا ز بی پر و بالی غمی که هست این است</p></div>
<div class="m2"><p>که گرد بام و در او نمی توانم گشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازان ز هر دو جهان بیخبر شدم صائب</p></div>
<div class="m2"><p>که غافل از خبر او نمی توانم گشت</p></div></div>