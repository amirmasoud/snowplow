---
title: >-
    غزل شمارهٔ ۱۴۹۵
---
# غزل شمارهٔ ۱۴۹۵

<div class="b" id="bn1"><div class="m1"><p>صدف بحر بقا سینه درویشان است</p></div>
<div class="m2"><p>گوهر آن، دل بی کینه درویشان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چه دارد فلک از بهر فقیران دارد</p></div>
<div class="m2"><p>ماه نو صیقل آیینه درویشان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشت خونی که دل نافه ازو پر خون است</p></div>
<div class="m2"><p>در ته خرقه پشمینه درویشان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چهره نعمت الوان شهان چون لاله</p></div>
<div class="m2"><p>داغ نان جو و کشکینه درویشان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست در هفته ارباب توقع تعطیل</p></div>
<div class="m2"><p>صبح شنبه شب آدینه درویشان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می شود دل ز قبول نظر خلق سیاه</p></div>
<div class="m2"><p>دست رد صیقل آیینه درویشان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل آسوده ز گنجینه شاهان مطلب</p></div>
<div class="m2"><p>این گهر در صدف سینه درویشان است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست امروز هواخواه فقیران صائب</p></div>
<div class="m2"><p>مخلص و بنده دیرینه درویشان است</p></div></div>