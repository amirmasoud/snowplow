---
title: >-
    غزل شمارهٔ ۳۴۳
---
# غزل شمارهٔ ۳۴۳

<div class="b" id="bn1"><div class="m1"><p>نمی باشد ز بی برگی چراغی خانه ما را</p></div>
<div class="m2"><p>ز چشم جغد باشد روشنی ویرانه ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرانی می کند بر گوشه گیران پرتو منت</p></div>
<div class="m2"><p>نگه دارد خدا از چشم روزن خانه ما را!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در و دیوار نتواند عنان سیل پیچیدن</p></div>
<div class="m2"><p>که منع از کوچه گردی می کند دیوانه ما را؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز برق تیشه ما سنگ خارا آب می گردد</p></div>
<div class="m2"><p>که حد دارد گذارد لب به لب پیمانه ما را؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سپند شوخ ما بار دل مجمر نمی گردد</p></div>
<div class="m2"><p>به خرمن می رساند بی قراری دانه ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پر پروانه سازد پرده خواب فراغت را</p></div>
<div class="m2"><p>مده در گوش خود راه آتشین افسانه ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به چوب گل دهد تهدید ما ناصح، ازین غافل</p></div>
<div class="m2"><p>که گردد خامه مشق جنون دیوانه ما را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نفس دزدیده، پا در خلوت وحشی خیالان نه</p></div>
<div class="m2"><p>که هست از چشم آهو حلقه در خانه ما را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر درد سخن می داشت صائب صید بند ما</p></div>
<div class="m2"><p>ز گوهر چون صدف می کرد آب و دانه ما را</p></div></div>