---
title: >-
    غزل شمارهٔ ۱۰۱۵
---
# غزل شمارهٔ ۱۰۱۵

<div class="b" id="bn1"><div class="m1"><p>عاجزی از عاشق، از معشوق طنازی خوش است</p></div>
<div class="m2"><p>از سپند افتادن، از آتش سرافرازی خوش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کوهکن حیف است فارغبال دارد تیشه را</p></div>
<div class="m2"><p>ناخنی تا هست در کف، سینه پردازی خوش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون دل در ساغر روشندلان زیبنده است</p></div>
<div class="m2"><p>باده شیراز در مینای شیرازی خوش است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خانه آرایی گرانجانی است با موی سفید</p></div>
<div class="m2"><p>صبح چون روشن شود، از شمع سربازی خوش است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر به پیش انداختن در زندگانی خوشنماست</p></div>
<div class="m2"><p>زیر شمشیر شهادت گردن افرازی خوش است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خانه سازی، در به روی دل برآوردن بود</p></div>
<div class="m2"><p>از عمارت، در جهان خاک، خودسازی خوش است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا نسوزد آرزو، پرداز دل بی حاصل است</p></div>
<div class="m2"><p>چون به خاکستر رسی، آیینه پردازی خوش است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتگو با دل سیاهان می کند دل را سیاه</p></div>
<div class="m2"><p>شمع اگر باشد طرف صائب زبان بازی خوش است</p></div></div>