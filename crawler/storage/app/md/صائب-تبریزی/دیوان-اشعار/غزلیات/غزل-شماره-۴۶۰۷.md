---
title: >-
    غزل شمارهٔ ۴۶۰۷
---
# غزل شمارهٔ ۴۶۰۷

<div class="b" id="bn1"><div class="m1"><p>یارنو خط زنگ از دل می زداید بیشتر</p></div>
<div class="m2"><p>برگ عیش از نوبهاران می فزاید بیشتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مستی چشمش یکی صدگشت دردوران خط</p></div>
<div class="m2"><p>دربهاران آب ازسر چشمه زاید بیشتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درلباس لطف، دل را قهر افزون می گزد</p></div>
<div class="m2"><p>تلخی بادام در شکر نماید بیشتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لب گشودن رخنه در ناموس همت کردن است</p></div>
<div class="m2"><p>ازکریمان بی طلب حاجت برآیدبیشتر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زخم خصم ناتوان است از قوی جانکاهتر</p></div>
<div class="m2"><p>نیشتر از تیغ خون رامی گشاید بیشتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آرزو را صبح بیداری بود موی سفید</p></div>
<div class="m2"><p>حرص درایام پیری می فزایدبیشتر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه لبهای شکر گفتار می چسبد به دل</p></div>
<div class="m2"><p>دل ز من چشم سخنگو می رباید بیشتر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قسمت تن پروران از تنگدستی کاهش است</p></div>
<div class="m2"><p>آسیا بی دانه چون گردید ساید بیشتر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می کند صائب زبان عیبجویان را دراز</p></div>
<div class="m2"><p>کوته اندیشی که خود را می ستاید بیشتر</p></div></div>