---
title: >-
    غزل شمارهٔ ۴۷۱
---
# غزل شمارهٔ ۴۷۱

<div class="b" id="bn1"><div class="m1"><p>ترا پر چون صدف شد گوش از سیماب در دریا</p></div>
<div class="m2"><p>وگرنه حلقه ذکری است هر گرداب در دریا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز عادت پرده غفلت شود اسباب آگاهی</p></div>
<div class="m2"><p>که ماهی بستر و بالین کند از آب در دریا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیال یار را در دیده عاشق تماشا کن</p></div>
<div class="m2"><p>که دارد شور دیگر پرتو مهتاب در دریا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حریم وصل را حیرانیی در پرده می باشد</p></div>
<div class="m2"><p>که شوق آب، ماهی را کند قلاب در دریا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به قسمت می توان برخورد از روزی، نه جمعیت</p></div>
<div class="m2"><p>که از جای دگر گردد صدف سیراب در دریا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غریق عشق بر گرد سر هر قطره می گردد</p></div>
<div class="m2"><p>که ماهی را بود هر موجه ای محراب در دریا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنین کز گرد عصیان تیره گردیده است جان من</p></div>
<div class="m2"><p>عجب دارم که گردد روشن این سیلاب در دریا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو دل شد آب، از دل سربرآرد آرزوی دل</p></div>
<div class="m2"><p>که از دریا زند سر مهر عالمتاب در دریا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نگردد آب تا صائب دلت از داغ نومیدی</p></div>
<div class="m2"><p>نخواهی دید روی گوهر نایاب در دریا</p></div></div>