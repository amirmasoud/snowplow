---
title: >-
    غزل شمارهٔ ۶۵۷۱
---
# غزل شمارهٔ ۶۵۷۱

<div class="b" id="bn1"><div class="m1"><p>در گلستان برگ عیش اندوختم بی فایده</p></div>
<div class="m2"><p>چون گل از جمعیت خود سوختم بی فایده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کیمیای رستگاری بود در دست تهی</p></div>
<div class="m2"><p>من ز غفلت سیم و زر اندوختم بی فایده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشت از ترک ادب هر بی حیایی کامیاب</p></div>
<div class="m2"><p>من درین محفل ادب آموختم بی فایده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوهر مقصود در گنجینه دل فرش بود</p></div>
<div class="m2"><p>من درین دریا نفس را سوختم بی فایده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست جز تسلیم ساحل عالم پرشور را</p></div>
<div class="m2"><p>من درین دریا شنا آموختم بی فایده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساده می بایست کردن دل ز هر نقشی که هست</p></div>
<div class="m2"><p>من دماغ از علم رسمی سوختم بی فایده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست رقت در دل سر در هوایان یک شرر</p></div>
<div class="m2"><p>در حضور شمع خود را سوختم بی فایده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از جواهر سرمه من دیده ای بینا نشد</p></div>
<div class="m2"><p>در ره کوران چراغ افروختم بی فایده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست در آهن دلان پیوند نیکان را اثر</p></div>
<div class="m2"><p>سوزن خود را به عیسی دوختم بی فایده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این جواب آن غزل صائب که می گوید حکیم</p></div>
<div class="m2"><p>عمرها علم و ادب آموختم بی فایده</p></div></div>