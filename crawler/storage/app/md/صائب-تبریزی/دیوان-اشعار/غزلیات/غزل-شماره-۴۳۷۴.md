---
title: >-
    غزل شمارهٔ ۴۳۷۴
---
# غزل شمارهٔ ۴۳۷۴

<div class="b" id="bn1"><div class="m1"><p>شوریده تر از سیل بهارم چه توان کرد</p></div>
<div class="m2"><p>در هیچ زمین نیست قرارم چه توان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون آبله در ظاهر اگر رنگ ندارم</p></div>
<div class="m2"><p>در پرده غیب است بهارم چه توان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شیرازه نگیرد به خود اوراق حواسم</p></div>
<div class="m2"><p>برهم زده زلف نگارم چه توان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون گرد ز من نیست اگر پست وبلندم</p></div>
<div class="m2"><p>خاک ره آن شاهسوارم چه توان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برهم نزنم دیده ز خورشید قیامت</p></div>
<div class="m2"><p>حیرت زده جلوه یارم چه توان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در بیضه چه پرواز کند مرغ چمن گرد</p></div>
<div class="m2"><p>زندانی این سبز حصارم چه توان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کاری به مرادم نشد از نقش موافق</p></div>
<div class="m2"><p>امروز که برگشت قمارم چه توان کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون ماه درین دایره هر چند تمامم</p></div>
<div class="m2"><p>از پهلوی خویش است مدارم چه توان کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از مشغله مهر ومحبت که فزون باد</p></div>
<div class="m2"><p>صائب سرکونین ندارم چه توان کرد</p></div></div>