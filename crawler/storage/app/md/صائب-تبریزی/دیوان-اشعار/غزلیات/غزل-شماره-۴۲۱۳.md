---
title: >-
    غزل شمارهٔ ۴۲۱۳
---
# غزل شمارهٔ ۴۲۱۳

<div class="b" id="bn1"><div class="m1"><p>بعد از فنا ز هستی ما شور شد بلند</p></div>
<div class="m2"><p>از چوب دار رایت منصور شد بلند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نتوان به خاک خون مرا پایمال کرد</p></div>
<div class="m2"><p>شور قیامت زلب گور شد بلند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دور خط دهان توشیرین کلام شد</p></div>
<div class="m2"><p>گرد شکر ز قافله مور شد بلند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از ترک خانمان به طلبکاری کلیم</p></div>
<div class="m2"><p>دست نوازش شجر طور شد بلند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رازی که سر به مهر ادب بود عمرها</p></div>
<div class="m2"><p>آخر ز کاسه سر منصور شد بلند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پروانه نجات به دست آورد چو شمع</p></div>
<div class="m2"><p>دستی که در دل شب دیجور شد بلند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلبل نبرد راه ز مستی به وصل گل</p></div>
<div class="m2"><p>چندان که دست شاخ گل از دور شد بلند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فریاد از درازی شبهاست خسته را</p></div>
<div class="m2"><p>از زلف ناله دل رنجور شد بلند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در دیده ستاره نمک ریخت خواب تلخ</p></div>
<div class="m2"><p>از خنده نهان که این شور شد بلند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در هیچ تربتی نبود شمع خانه زاد</p></div>
<div class="m2"><p>از خاک کشتگان تو این نور شد بلند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آزار خلق اگر نبود برق خانمان</p></div>
<div class="m2"><p>آتش چرا ز خانه زنبور شد بلند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون زلفهای عاریه کوتاه گرد نیست</p></div>
<div class="m2"><p>هر همتی که از می انگور شد بلند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گلبانگ عشق پرده نشین بود سالها</p></div>
<div class="m2"><p>از صائب این ترانه مستور شدبلند</p></div></div>