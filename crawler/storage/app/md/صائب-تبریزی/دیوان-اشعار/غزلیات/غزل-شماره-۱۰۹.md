---
title: >-
    غزل شمارهٔ ۱۰۹
---
# غزل شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>جذبه مجنون سبک سازد ز تمکین سنگ را</p></div>
<div class="m2"><p>در کف طفلان دهد پرواز شاهین سنگ را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می توان دل را به آهی کرد از غم ها سبک</p></div>
<div class="m2"><p>یک فلاخن می کند آواره چندین سنگ را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر گرانخوابان غفلت مهربان است آسمان</p></div>
<div class="m2"><p>کز فروغ لعل باشد شمع بالین سنگ را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خیال یار، دل شد کعبه حاجت مرا</p></div>
<div class="m2"><p>نقش شیرین در نظرها ساخت شیرین سنگ را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کم نشد از گریه مستانه خواب غفلتم</p></div>
<div class="m2"><p>سیل نتوانست کند از جای خود این سنگ را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از کمان نرم بر من زور چندین می رود</p></div>
<div class="m2"><p>شیشه جانی های من دارد شلایین سنگ را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غوطه در خون می دهد دل را فروغ داغ عشق</p></div>
<div class="m2"><p>می کند خورشید عالمتاب رنگین سنگ را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک دل افسرده بی داغ از دم گرمم نماند</p></div>
<div class="m2"><p>در بهار از لاله گردد چهره رنگین سنگ را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون نباشد عید طفلان صحبت رنگین من؟</p></div>
<div class="m2"><p>می کند مجنون من دست نگارین سنگ را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر خمار سنگ طفلان صبر کردن مشکل است</p></div>
<div class="m2"><p>می کنم بالین خود شب بهر تسکین سنگ را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از بدآموزان بود مستغنی آن پیمان شکن</p></div>
<div class="m2"><p>نیست در سنگین دلی حاجت به تلقین سنگ را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر دل بی رحم جانان بوی گل باشد گران</p></div>
<div class="m2"><p>شیشه در بارست از نازکدلی این سنگ را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سختی ایام باشد بر سبک عقلان گران</p></div>
<div class="m2"><p>کی کند دیوانه سرشار، تمکین سنگ را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بود اگر زین پیش صائب در گرانخوابی مثل</p></div>
<div class="m2"><p>شد سبک از غفلت من خواب سنگین سنگ را</p></div></div>