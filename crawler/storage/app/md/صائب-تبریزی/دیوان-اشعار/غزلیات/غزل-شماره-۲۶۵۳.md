---
title: >-
    غزل شمارهٔ ۲۶۵۳
---
# غزل شمارهٔ ۲۶۵۳

<div class="b" id="bn1"><div class="m1"><p>دستگاه شور من از دامن هامون فزود</p></div>
<div class="m2"><p>چشم آهو پرده‌ها بر وحشت مجنون فزود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌نماید گوهر شب‌تاب در شب خویش را</p></div>
<div class="m2"><p>از خط شبگون فروغ آن لب میگون فزود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دوصد قانون نگردد کشف بر حکمت‌شناس</p></div>
<div class="m2"><p>آنچه از یک خشت خم بر علم افلاطون فزود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گاه باشد خرمنی از دانه‌ای فاسد شود</p></div>
<div class="m2"><p>بخل خاک خشک مغز از صحبت قارون فزود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زود عالمگیر گردد چون دو مصرع شد بلند</p></div>
<div class="m2"><p>فتنه صبح قیامت زان قد موزون فزود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از فریب نعل وارون فلک غافل شدند</p></div>
<div class="m2"><p>حرص روزی‌خوارگان زین کاسه وارون فزود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جای حیرت نیست، خرمن‌ها تمام از دانه‌ای است</p></div>
<div class="m2"><p>تخم مهر ما اگر زان خال گندمگون فزود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بود راز آن دهن پوشیده صائب، از چه روی</p></div>
<div class="m2"><p>خط ظالم پرده دیگر بر آن مضمون فزود</p></div></div>