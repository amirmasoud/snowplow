---
title: >-
    غزل شمارهٔ ۳۴۶۹
---
# غزل شمارهٔ ۳۴۶۹

<div class="b" id="bn1"><div class="m1"><p>کی ز تن کار دل خسته به آرام کشد؟</p></div>
<div class="m2"><p>مرغ وحشی چه نفس در قفس و دم کشد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخت گستاخ شد از وصل دلم، می ترسم</p></div>
<div class="m2"><p>عاقبت کار من از بوسه به پیغام کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از زبان لعل لبش تلخی گفتار نبرد</p></div>
<div class="m2"><p>نمک سنگ کجا تلخی بادام کشد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غم مرغان گرفتار ندارد صیاد</p></div>
<div class="m2"><p>مور از رحم مگر دانه به این دام کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نکشد پای پر از آبله از خارستان</p></div>
<div class="m2"><p>آنچه پهلوی من از بسرت آرام کشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دانه اش از گره دام مهیا باشد</p></div>
<div class="m2"><p>هرکه را زلف گرهگیر تو در دام کشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دست کوتاه، گل از وصل فزون می چیند</p></div>
<div class="m2"><p>شانه گستاخ سر زلف دلارام کشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکوه ای کز سر زلف تو مرا هست این است</p></div>
<div class="m2"><p>که دل عاشق و اغیار به یک دام کشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این چه کیفیت حسن است که مخمور وصال</p></div>
<div class="m2"><p>از لب بام تو می همچو لب جام کشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آب را دست درین باغ ز حیرت شد خشک</p></div>
<div class="m2"><p>کیست تا دامن آن سرو گل اندام کشد؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پله ناز تو سنگین تر ازان افتاده است</p></div>
<div class="m2"><p>که ترا جذبه صائب به لب بام کشد</p></div></div>