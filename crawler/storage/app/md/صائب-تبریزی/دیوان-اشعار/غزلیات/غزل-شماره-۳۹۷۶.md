---
title: >-
    غزل شمارهٔ ۳۹۷۶
---
# غزل شمارهٔ ۳۹۷۶

<div class="b" id="bn1"><div class="m1"><p>غبار معصیت از عفو پایمال شود</p></div>
<div class="m2"><p>چو سیل واصل دریا شود زلال شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین بساط که نعمت ز هم نمی گسلد</p></div>
<div class="m2"><p>ادای شکر کسی می کندکه لال شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو شمع خودسرخودمی خورم ز غیرت عشق</p></div>
<div class="m2"><p>چرا به گردن او خون من وبال شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلی چو نافه پر از خون گرم می باید</p></div>
<div class="m2"><p>کجا به خرقه پشمین کس اهل حال شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مباش غره به خوبی که دور چون برگشت</p></div>
<div class="m2"><p>به یک دوهفته مه چارده هلال شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سبکروان به فتادن ز پای ننشینند</p></div>
<div class="m2"><p>شکست شهپر موج شکسته بال شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جدا ازان لب میگون اگر شراب خورم</p></div>
<div class="m2"><p>حباب در قدحم عقده ملال شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غبار خاطرآن تیغ می شود صائب</p></div>
<div class="m2"><p>اگر چوآب گهرخون من زلال شود</p></div></div>