---
title: >-
    غزل شمارهٔ ۳۳۸۹
---
# غزل شمارهٔ ۳۳۸۹

<div class="b" id="bn1"><div class="m1"><p>باد اگر پرده ز رخساره یار اندازد</p></div>
<div class="m2"><p>لرزه بر دست نگارین بهار اندازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آب آیینه ز شرم خط او چون خس و خار</p></div>
<div class="m2"><p>هر نفس جوهر خود را به کنار اندازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شرم رخسار تو صد پرده خونین از گل</p></div>
<div class="m2"><p>هر سر سال به رخسار بهار اندازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلف رخساره خورشید شود شبگیرم</p></div>
<div class="m2"><p>شوق اگر سایه بر این مشت غبار اندازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیشگاه حرم و دیر گریبان چاک است</p></div>
<div class="m2"><p>تا کجا غمزه او طرح شکار اندازد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر مرا حوصله فقر نباشد چه عجب</p></div>
<div class="m2"><p>که تهیدستی، آتش به چنار اندازد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کلک صائب چو گشاید گره از زلف سخن</p></div>
<div class="m2"><p>تاب در نافه آهوی تتار اندازد</p></div></div>