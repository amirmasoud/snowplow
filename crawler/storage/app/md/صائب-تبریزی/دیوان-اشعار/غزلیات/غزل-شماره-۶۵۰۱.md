---
title: >-
    غزل شمارهٔ ۶۵۰۱
---
# غزل شمارهٔ ۶۵۰۱

<div class="b" id="bn1"><div class="m1"><p>ندارد اختیار در گشودن باغبان تو</p></div>
<div class="m2"><p>که در را می گشاید جوش گل در گلستان تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز ابروی تو دارد هر سر مو شوخی مژگان</p></div>
<div class="m2"><p>پر سیمرغ بخشد تیر بی پر را کمان تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز منعم کاسه همسایه خالی برنمی گردد</p></div>
<div class="m2"><p>قدح لبریز برگردد ز لعل می چکان تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهد از طوق قمری سرو چون تیر از کمان بیرون</p></div>
<div class="m2"><p>به هر گلشن که گردد جلوه گر سرو روان تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخن چندان که خود را چون الف باریک می سازد</p></div>
<div class="m2"><p>به دشواری برون آید از تنگ دهان تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمی دارد به زودی رشته همتاب دست از هم</p></div>
<div class="m2"><p>رگ جان را گسستن نیست از موی میان تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ازان از دیدن خورشید در چشم آب می گردد</p></div>
<div class="m2"><p>که مالیده است روی زرد خود بر آستان تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به بی برگان چنان ای شاخ گل مستانه می خندی</p></div>
<div class="m2"><p>که در خواب بهاران است پنداری خزان تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نمی گردد زبان جرأت من، ورنه می گفتم</p></div>
<div class="m2"><p>که جای بوسه پر خالی است در کنج دهان تو!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو جولان می کنی از سرکشی در اوج استغنا</p></div>
<div class="m2"><p>کجا افتد به دست کوته صائب عنان تو؟</p></div></div>