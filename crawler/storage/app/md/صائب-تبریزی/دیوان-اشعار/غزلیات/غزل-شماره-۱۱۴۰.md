---
title: >-
    غزل شمارهٔ ۱۱۴۰
---
# غزل شمارهٔ ۱۱۴۰

<div class="b" id="bn1"><div class="m1"><p>نه همین سرگشته ما را دور گردون کرده است</p></div>
<div class="m2"><p>خضر را خون در جگر این نعل وارون کرده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مهره مومی است در سر پنجه او آسمان</p></div>
<div class="m2"><p>آن که حال ما اسیران را دگرگون کرده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قمری ما از پریشان ناله های دلفریب</p></div>
<div class="m2"><p>سرو را آشفته تر از بید مجنون کرده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر چه ما چون سرو آزادیم از قید لباس</p></div>
<div class="m2"><p>همت ما دست ازین نه خرقه بیرون کرده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دامن معنی به آسانی نمی آید به دست</p></div>
<div class="m2"><p>سرو یک مصرع تمام عمر موزون کرده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در ته گرد کسادی، گوهر شهوار من</p></div>
<div class="m2"><p>خاک عالم را سبک در چشم قارون کرده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می کنم در کوچه گردی سیر صحرای جنون</p></div>
<div class="m2"><p>وسعت مشرب مرا فارغ ز هامون کرده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برنمی آرند سر از زیر بال بلبلان</p></div>
<div class="m2"><p>بس که گلها را خجل آن روی گلگون کرده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر چه با ما می کند، تدبیر ناقص می کند</p></div>
<div class="m2"><p>درد ما را این طبیب خام افزون کرده است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بس که تشریف بهاران نارسا افتاده است</p></div>
<div class="m2"><p>تاک از یک آستین، صد دست بیرون کرده است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنچه در دامان کهسارست صائب لاله نیست</p></div>
<div class="m2"><p>سنگ را محرومی فرهاد دلخون کرده است</p></div></div>