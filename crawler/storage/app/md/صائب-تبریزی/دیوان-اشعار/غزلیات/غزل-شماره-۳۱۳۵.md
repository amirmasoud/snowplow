---
title: >-
    غزل شمارهٔ ۳۱۳۵
---
# غزل شمارهٔ ۳۱۳۵

<div class="b" id="bn1"><div class="m1"><p>به ناز افراختی قامت فلکها در سجود آمد</p></div>
<div class="m2"><p>زمی افروختی رخسار، آتش در وجود آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمود این جهان بودی ندارد، بارها دیدم</p></div>
<div class="m2"><p>من و تنگ دهان او که بود بی نمود آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که باور می کند از ما اگر مژگان تر نبود؟</p></div>
<div class="m2"><p>که از حلوای بی دود تو ما را رزق دود آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه تنها سیلی عشق تو ما را رو سیه داد</p></div>
<div class="m2"><p>مکرر چهره یاقوت ازین آتش کبود آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندانم چیست مضمون خط ساغر، همین دانم</p></div>
<div class="m2"><p>که تا از زیر چشمش دید مینا در سجود آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمی دانم که بود این آتشین جولان، همین دانم</p></div>
<div class="m2"><p>که تا پا در رکاب آورد، در خاطر فرود آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگر بر بال دارد نامه قتل مرا صائب؟</p></div>
<div class="m2"><p>که مرغ نامه بر از کوی او این بار زود آمد</p></div></div>