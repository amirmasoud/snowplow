---
title: >-
    غزل شمارهٔ ۴۹۸۰
---
# غزل شمارهٔ ۴۹۸۰

<div class="b" id="bn1"><div class="m1"><p>دلپذیرست چنان پسته شکرشکنش</p></div>
<div class="m2"><p>که رسد پیشتر از گوش به دلها سخنش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خط شبرنگ دمیده است ز لعل لب او؟</p></div>
<div class="m2"><p>یا به خون چشم سیه کرده عقیق یمنش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرکز دایره عشرت جاوید شود</p></div>
<div class="m2"><p>بوسه ای راکه فتد راه به کنج دهنش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفتابی است که از آب نماید دیدار</p></div>
<div class="m2"><p>تن لرزنده سیمین ز ته پیرهنش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در حریم صدفش گوهر بینایی نیست</p></div>
<div class="m2"><p>دل هر کس که نیفتاده به چاه ذقنش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اشک وآهش گهر و عنبر سارا گردد</p></div>
<div class="m2"><p>هرکه چون شمع بودراه درآن انجمنش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرو قدی که من از جلوه او پامالم</p></div>
<div class="m2"><p>آسمان سبزه خوابیده بود در چمنش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مغز هرکس که ز فکر تو پریشان گردد</p></div>
<div class="m2"><p>سنبل باغ بهشت است پریشان سخنش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عاشقان منت آمد قاصد نکشند</p></div>
<div class="m2"><p>که دهد رفتن دلها خبر ازآمدنش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کی درآید به نظر آن تن سیمین، که شده است</p></div>
<div class="m2"><p>پیرهن بال پریزاد ز لطف بدنش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا فتاده است به فکر سرکویش صائب</p></div>
<div class="m2"><p>هست دلگیرتر از شام غریبان وطنش</p></div></div>