---
title: >-
    غزل شمارهٔ ۵۹۶۷
---
# غزل شمارهٔ ۵۹۶۷

<div class="b" id="bn1"><div class="m1"><p>سالکان را کی دل از اسباب می گردد گران؟</p></div>
<div class="m2"><p>خار و خس کی بر دل سیلاب می گردد گران؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرط طاعت چشم گریان و دل سوزان بود</p></div>
<div class="m2"><p>شمع خامش بر دل محراب می گردد گران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می شکافد جوشن ابر سیه را تیغ برق</p></div>
<div class="m2"><p>کوه غم کی بر دل بی تاب می گردد گران؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست خون بی گناهان بار بر دل حسن را</p></div>
<div class="m2"><p>از شفق کی مهر عالمتاب می گردد گران؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قلقل مینا مرا در چشم می ریزد نمک</p></div>
<div class="m2"><p>خواب هر چند از صدای آب می گردد گران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میکشان را می شود بر خاطر نازک سبک</p></div>
<div class="m2"><p>جام هر چند از شراب ناب می گردد گران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حرف تلخ ناصحان افسانه خوابش شود</p></div>
<div class="m2"><p>هر که را سر از شراب ناب می گردد گران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بستر نرم است خار پیرهن دل زنده را</p></div>
<div class="m2"><p>گرز مخمل دیگران را خواب می گردد گران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روشنایی رهروان شوق را بال و پرست</p></div>
<div class="m2"><p>غافلان را خواب در مهتاب می گردد گران</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از سبکروحان دل روشن گرانی می کشد</p></div>
<div class="m2"><p>بر دل آیینه ام سیماب می گردد گران</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پیش روشن گوهران هر کس لب خود وا کند</p></div>
<div class="m2"><p>چون صدف از گوهر سیراب می گردد گران</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سالک از کلفت نیندیشد که گردد تندتر</p></div>
<div class="m2"><p>هر قدر از گرد ره سیلاب می گردد گران</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دولت سنگین دلان را نعل در آتش بود</p></div>
<div class="m2"><p>در زمین نرم پای آب می گردد گران</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صائب از زخم زبان سرگشتگان را باک نیست</p></div>
<div class="m2"><p>خار و خس کی بر دل گرداب می گردد گران؟</p></div></div>