---
title: >-
    غزل شمارهٔ ۹۳۸
---
# غزل شمارهٔ ۹۳۸

<div class="b" id="bn1"><div class="m1"><p>در غبار خط صفای آن پری طلعت بجاست</p></div>
<div class="m2"><p>گر چه شد درد این شراب صاف، کیفیت بجاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفتن فصل بهار، از خواب سنگینی نبرد</p></div>
<div class="m2"><p>طی شد ایام جوانی و همان غفلت بجاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>توبه خواهش به سایل می دهد از روی تلخ</p></div>
<div class="m2"><p>خواجه ممسک کند گر دعوی همت بجاست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بحر نتواند فرو بردن کف بی مغز را</p></div>
<div class="m2"><p>غرقه شد در آب یونان و همان حکمت بجاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در چنین عهدی که مردم خون هم را می خورند</p></div>
<div class="m2"><p>می کشد هر کس که پا در دامن عزلت بجاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داد جا در دست چون خاتم سلیمان مور را</p></div>
<div class="m2"><p>عزت افتادگان از صاحب دولت بجاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می فشاند گوهر و آب از خجالت می شود</p></div>
<div class="m2"><p>گر کند ابر بهاران دعوی همت بجاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب از مینا به کنه باده مستان می رسند</p></div>
<div class="m2"><p>اهل معنی را نظر بر عالم صورت بجاست</p></div></div>