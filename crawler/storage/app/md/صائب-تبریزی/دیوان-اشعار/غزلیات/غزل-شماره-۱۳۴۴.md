---
title: >-
    غزل شمارهٔ ۱۳۴۴
---
# غزل شمارهٔ ۱۳۴۴

<div class="b" id="bn1"><div class="m1"><p>ای ستمگر از نگاه دور رنجیدن نداشت</p></div>
<div class="m2"><p>این گناه سهل، بر انگشت پیچیدن نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکوه ننوشتن مکتوب را طی می کنیم</p></div>
<div class="m2"><p>نامه ما ای فرامشکار نشنیدن نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از برای کشتن من کم نبود اسباب قتل</p></div>
<div class="m2"><p>حال بیمار من از اغیار پرسیدن نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرکشی چون مانع است از دیدن عاشق ترا</p></div>
<div class="m2"><p>از من ای بی رحم، راه خانه پرسیدن نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قهرمان غیرت عشاق، بی جاسوس نیست</p></div>
<div class="m2"><p>روی خود در خلوت آیینه بوسیدن نداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گریه ابر و نشاط برق، با هم خوشنماست</p></div>
<div class="m2"><p>پیش چشم ما به روی غیر خندیدن نداشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شور بختی شوریی در چشم ما نگذاشته است</p></div>
<div class="m2"><p>از حضور ما بساط باده برچیدن نداشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کی کند در منتهای حسن، زیر پا نگاه؟</p></div>
<div class="m2"><p>آن که در طفلی ز تمکین ذوق گل چیدن نداشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می توان از یک ورق، خواندن کتابی را تمام</p></div>
<div class="m2"><p>اینقدر بر دفتر ایام، گردیدن نداشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در چنین وقتی که صائب خاک ره گردیده است</p></div>
<div class="m2"><p>زیر پای خویش ای بی رحم، نادیدن نداشت</p></div></div>