---
title: >-
    غزل شمارهٔ ۶۸۹۱
---
# غزل شمارهٔ ۶۸۹۱

<div class="b" id="bn1"><div class="m1"><p>از زهر چشم، چشم من زار بسته ای</p></div>
<div class="m2"><p>راه عیادت از چه به بیمار بسته ای؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راه هزار قافله دل می زند به مکر</p></div>
<div class="m2"><p>از شرم پرده ای که به رخسار بسته ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قانع به یک نظاره خشکیم ما ز دور</p></div>
<div class="m2"><p>بر روی ما چرا در گلزار بسته ای؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه حرف می زنی، نه نگه می کنی، نه ناز</p></div>
<div class="m2"><p>بر من در امید به یکبار بسته ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شبنم ز گلشن تو نظر آب چون دهد؟</p></div>
<div class="m2"><p>کز شرم، چشم رخنه دیوار بسته ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون قیمت تو در گره روزگار نیست</p></div>
<div class="m2"><p>از روی لطف راه خریدار بسته ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب ز یار از ته دل نیست شکوه ات</p></div>
<div class="m2"><p>این نغمه را به زور بر این تار بسته ای</p></div></div>