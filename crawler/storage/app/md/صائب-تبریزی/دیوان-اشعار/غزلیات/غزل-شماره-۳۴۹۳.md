---
title: >-
    غزل شمارهٔ ۳۴۹۳
---
# غزل شمارهٔ ۳۴۹۳

<div class="b" id="bn1"><div class="m1"><p>بال پرواز خود آن مردم غافل بستند</p></div>
<div class="m2"><p>که به زنار علایق کمر دل بستند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جلوه موج سراب است جهان در نظرش</p></div>
<div class="m2"><p>چشم حق بین کسی را که ز باطل بستند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در جنت نگشایند به رویش فردا</p></div>
<div class="m2"><p>بر رخ هرکه درین نشأه در دل بستند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکوه اهل دل از عشق ندارد انجام</p></div>
<div class="m2"><p>چون ننالد جرسی را که به محمل بستند؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشود پنجه تدبیر عنانگیر قضا</p></div>
<div class="m2"><p>خار و خس کی ره امواج به ساحل بستند؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رو مگردان ز دم تیغ که بسمل شدگان</p></div>
<div class="m2"><p>دام از دیده خود در ره قاتل بستند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زود باشد که گشایند دهن را به سؤال</p></div>
<div class="m2"><p>صائب آنان که در فیض به سایل بستند</p></div></div>