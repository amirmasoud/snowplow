---
title: >-
    غزل شمارهٔ ۱۲۳۷
---
# غزل شمارهٔ ۱۲۳۷

<div class="b" id="bn1"><div class="m1"><p>بادپیمایی مسلسل همچو آب از بهر چیست؟</p></div>
<div class="m2"><p>می رود عمر سبکرو، این شتاب از بهر چیست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی گرداندن ز ما ای آفتاب از بهر چیست؟</p></div>
<div class="m2"><p>اینقدر از سایه خود اجتناب از بهر چیست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما اگر شایسته لطف نمایان نیستیم</p></div>
<div class="m2"><p>خشم و ناز و رنجش بیجا، عتاب از بهر چیست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قسمت ما از تو چون چشم پر آبی بیش نیست</p></div>
<div class="m2"><p>روی پوشیدن ز ما ای آفتاب از بهر چیست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان لب میگون به خامی کام دل نتوان گرفت</p></div>
<div class="m2"><p>آه گرم و اشک خونین ای کباب از بهر چیست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در تماشا، دیده قربانیان گستاخ نیست</p></div>
<div class="m2"><p>پیش ما حیرانیان چندین حجاب از بهر چیست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با رخ گلگون چرا باید به سیر باغ رفت؟</p></div>
<div class="m2"><p>با لب میگون تمنای شراب از بهر چیست؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون نمی آیی به خواب عاشقان از سرکشی</p></div>
<div class="m2"><p>ریختن چندین نمک در چشم خواب از بهر چیست؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیش دریا از صدف گوهر سراپا گوش شد</p></div>
<div class="m2"><p>گفتگوی پوچ چندین ای حباب از بهر چیست؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رشته گوهر شود موجی که واصل شد به بحر</p></div>
<div class="m2"><p>گفتگوی پوچ چندین ای حباب از بهر چیست؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رشته گوهر شود موجی که واصل شد به بحر</p></div>
<div class="m2"><p>زیر تیغ یار ای جان پیچ و تاب از بهر چیست؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در دل گل ناله بلبل ندارد گر اثر</p></div>
<div class="m2"><p>اشک شبنم، گریه تلخ گلاب از بهر چیست؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کرده ای گر پاک با مردم حساب خویش را</p></div>
<div class="m2"><p>اینقدر اندیشه از روز حساب از بهر چیست؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون به ریو و رنگ نتوان از جوانی طرف بست</p></div>
<div class="m2"><p>حیرتی دارم که پیران را خضاب از بهر چیست؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در بهاران هیچ عاقل توبه از می می کند؟</p></div>
<div class="m2"><p>صائب این اندیشه های ناصواب از بهر چیست؟</p></div></div>