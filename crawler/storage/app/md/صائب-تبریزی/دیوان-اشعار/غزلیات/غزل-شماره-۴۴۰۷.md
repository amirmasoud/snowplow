---
title: >-
    غزل شمارهٔ ۴۴۰۷
---
# غزل شمارهٔ ۴۴۰۷

<div class="b" id="bn1"><div class="m1"><p>خون بهتر ازان می که چشیدن نگذارند</p></div>
<div class="m2"><p>پیکان به ازان غنچه که چیدن نگذارند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غیر از لب افسوس گزیدن چه علاج است</p></div>
<div class="m2"><p>آن راکه لب یار گزیدن نگذارند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوسیدن کنج لب ساقی چه خیال است</p></div>
<div class="m2"><p>آن را که لب جام مکیدن نگذارند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بال وپر ارباب هوس غنچه نگردد</p></div>
<div class="m2"><p>جایی که مرا چشم پریدن نگذارند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چند شد از گریه ما خط بتان سبز</p></div>
<div class="m2"><p>نظاره ما را به چریدن نگذارند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فریاد که چون شوره زمین دانه ما را</p></div>
<div class="m2"><p>این شورنگاهان به دمیدن نگذارند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرچند شود خون دل عشاق ز غیرت</p></div>
<div class="m2"><p>خونابه دل را به چکیدن نگذارند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون سیل سبکسیر درین بادیه ما را</p></div>
<div class="m2"><p>از پای طلب خار کشیدن نگذارند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اندیشه پابوس خیالی است زمین گیر</p></div>
<div class="m2"><p>ما را که به گرد تو رسیدن نگذارند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فریاد که چون غنچه مرا هرزه درایان</p></div>
<div class="m2"><p>در کنج دل خویش خزیدن نگذارند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب چه خیال است که در دست من افتد</p></div>
<div class="m2"><p>از دور گلی را که به دیدن نگذارند</p></div></div>