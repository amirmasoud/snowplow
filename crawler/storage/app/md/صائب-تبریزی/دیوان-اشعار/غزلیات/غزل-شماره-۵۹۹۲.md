---
title: >-
    غزل شمارهٔ ۵۹۹۲
---
# غزل شمارهٔ ۵۹۹۲

<div class="b" id="bn1"><div class="m1"><p>چیست جان تا زیر تیغ یار نتوان باختن؟</p></div>
<div class="m2"><p>سهل باشد پیش آب زندگی جان باختن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قطره را گوهر، گهر را بحر عمان کردن است</p></div>
<div class="m2"><p>سر چو شبنم در ره خورشید تابان باختن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در شبستانی که اشک شمع آب زندگی است</p></div>
<div class="m2"><p>نیست بر پروانه مشکل خرده جان باختن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش تیغی کز رنگ جان است زلف جوهرش</p></div>
<div class="m2"><p>چیست این استادگی در خرده جان باختن؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خودنمایی چیست تا از بیخودی غافل شوند؟</p></div>
<div class="m2"><p>بهر این آیینه نتوان آب حیوان باختن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فارغ است از سرمه ابر سیه آواز رعد</p></div>
<div class="m2"><p>عشق را در پرده ناموس نتوان باختن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سبز کن چون مور در ملک قناعت گوشه ای</p></div>
<div class="m2"><p>تا شود آسان ترا ملک سلیمان باختن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خون رحمت در دل ابر بهار آرد به جوش</p></div>
<div class="m2"><p>بر امید نسیه نقد خود چو دهقان باختن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با کمال علم، ملزم گشتن از نادان خوش است</p></div>
<div class="m2"><p>این قمار برده را شرط است آسان باختن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل ز شبنم می برد خواهی نخواهی آفتاب</p></div>
<div class="m2"><p>اختیاری نیست عاشق را دل و جان باختن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زود سر را گوی چوگان ملامت می کند</p></div>
<div class="m2"><p>با قد خم گشته با اطفال چوگان باختن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دارم این یک چشمه کار از پیر کنعان یادگار</p></div>
<div class="m2"><p>چشم را از گریه در راه عزیزان باختن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صائب از اوضاع عالم دیده بینش بپوش</p></div>
<div class="m2"><p>چند عمر خویش در خواب پریشان باختن؟</p></div></div>