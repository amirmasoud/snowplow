---
title: >-
    غزل شمارهٔ ۳۹۱۷
---
# غزل شمارهٔ ۳۹۱۷

<div class="b" id="bn1"><div class="m1"><p>درین ریاض دلی را که آب می‌سازند</p></div>
<div class="m2"><p>چو شبنم آینه آفتاب می‌سازند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلی که داغ و کباب از فروغ عشق نشد</p></div>
<div class="m2"><p>در آفتاب قیامت کباب می‌سازند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه ساده‌اند گروهی که از هواجویی</p></div>
<div class="m2"><p>ز بحر خانه جدا چون حباب می‌سازند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مده ز دست درین تنگنا عنان زنهار</p></div>
<div class="m2"><p>که رشته را گره از پیچ و تاب می‌سازند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیاض گردن او را بتان آهوچشم</p></div>
<div class="m2"><p>ز مردمک نقط انتخاب می‌سازند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر آن گروه حلال است لاف خوش‌نفسی</p></div>
<div class="m2"><p>که خون سوخته را مشک ناب می‌سازند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز انقلاب خزان و بهار آزادند</p></div>
<div class="m2"><p>جماعتی که ز گل با گلاب می‌سازند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خبر ز نشئه می نیست تن‌پرستان را</p></div>
<div class="m2"><p>چو خم همین شکمی پرشراب می‌سازند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جماعتی که ز اسرار حکمت آگاهند</p></div>
<div class="m2"><p>ز خشت خم چو فلاطون کتاب می‌سازند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به گریه صلح کن از گل‌رخان که دیده‌وران</p></div>
<div class="m2"><p>ز آفتاب به چشم پرآب می‌سازند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جماعتی که نیند از حساب خود غافل</p></div>
<div class="m2"><p>علی‌الحساب به روز حساب می‌سازند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خرابه‌ای است که خوش‌تر ز بیت معمور است</p></div>
<div class="m2"><p>تنی که از تپش دل خراب می‌سازند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به رنگ و بوی منه دل که عاقبت‌بینان</p></div>
<div class="m2"><p>به آه گرم گل خود گلاب می‌سازند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فتاده است ره من به وادیی صائب</p></div>
<div class="m2"><p>که دام خضر ز موج سراب می‌سازند</p></div></div>