---
title: >-
    غزل شمارهٔ ۲۹۴۶
---
# غزل شمارهٔ ۲۹۴۶

<div class="b" id="bn1"><div class="m1"><p>چه باک از عاشق بی باک آن طناز می دارد؟</p></div>
<div class="m2"><p>زکبک مست کی اندیشه ای شهباز می دارد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به تسخیر تو دستم نیست، ورنه جذبه ای دارم</p></div>
<div class="m2"><p>که از رفتار سیل تندرو را باز می دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر بلبل زند مهر خموشی بر لب گویا</p></div>
<div class="m2"><p>که روی باغ، سرخ از شعله آواز می دارد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه آسان است اخگر در گریبان ساختن پنهان</p></div>
<div class="m2"><p>نبیند روی راحت هر که پاس راز می دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زشور عندلیبان است شاخ گل چنین سرکش</p></div>
<div class="m2"><p>نیاز عاشقان معشوق را بر ناز می دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زفیض صبح نور از جبهه خورشید می بارد</p></div>
<div class="m2"><p>که بزم خوبرویان رونق از دمساز می دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زبیم دخل باشد خامشی آتش زبانان را</p></div>
<div class="m2"><p>زبان شمع را کوته دهان گاز می دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ترا گر گوشمالی می دهد دوران مپیچان سر</p></div>
<div class="m2"><p>به قدر گوشمال آهنگ دایم ساز می دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مجو انجام این افسانه دور و دراز از من</p></div>
<div class="m2"><p>که حرف زلف مهرویان همین آغاز می دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نپردازد به دنیای محقر همت عالی</p></div>
<div class="m2"><p>کجا پروا زصید کبک این شهباز می دارد؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بغیر از معنی رنگین که ریزد صائب از کلکت</p></div>
<div class="m2"><p>کدامین سحر دیگر رتبه اعجاز می دارد؟</p></div></div>