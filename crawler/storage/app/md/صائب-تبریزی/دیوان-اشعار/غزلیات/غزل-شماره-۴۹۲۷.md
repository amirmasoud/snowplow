---
title: >-
    غزل شمارهٔ ۴۹۲۷
---
# غزل شمارهٔ ۴۹۲۷

<div class="b" id="bn1"><div class="m1"><p>نشد روشن چراغم از عذار آتش اندودش</p></div>
<div class="m2"><p>مگر چشمی دهم درموسم خط آب ازدودش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اجابتهاست درطالع دعای دامن شب را</p></div>
<div class="m2"><p>یکی صد شد امید من زخط عنبر آلودش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل سنگش کجا برتشنه دیدار می سوزد؟</p></div>
<div class="m2"><p>سبکدستی که برمی آید از آیینه مقصودش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دوری از حریم اونشد قطع امید من</p></div>
<div class="m2"><p>که برگردد به محفل شمع، چون خامش کنی زودش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز احسان نهانی جان سایل تازه می گردد</p></div>
<div class="m2"><p>خوشا زخمی که سازد خنده پنهان نمکسودش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مدان چون تنگدستان جهان محتاج عاشق را</p></div>
<div class="m2"><p>که یاد از بی نیازی می دهد روی زر اندودش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مزن مهر خموشی بر دهن آتش زبانان را</p></div>
<div class="m2"><p>کز این روزن برآید دود چون سازند مسدودش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر همن ازحضور بت دل آسوده ای دارد</p></div>
<div class="m2"><p>نباشد دل به جان آن راکه درغیب است معبودش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه بگشاید ز خلق سفله صائب، ما و درگاهی</p></div>
<div class="m2"><p>که هر موری سلیمان می شود از سفره جودش</p></div></div>