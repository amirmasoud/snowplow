---
title: >-
    غزل شمارهٔ ۴۶۸۹
---
# غزل شمارهٔ ۴۶۸۹

<div class="b" id="bn1"><div class="m1"><p>سخن دریغ مداراز سخن کشان زنهار</p></div>
<div class="m2"><p>به تشنگان برسان آب را روان زنهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آستانه دل جوی آنچه می جویی</p></div>
<div class="m2"><p>مبر نیاز به هر خاک آستان زنهار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشست و خاست درین بوستان چوشبنم کن</p></div>
<div class="m2"><p>مشو به خاطر نازکدلان گران زنهار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پاره دل ولخت جگر قناعت کن</p></div>
<div class="m2"><p>مریز آب رخ خود برای نان زنهار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مباد خواب گرانت کند بیابان مرگ</p></div>
<div class="m2"><p>مده زدست سر راه کاروان زنهار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به عیب خویش به پردازتاشوی بی عیب</p></div>
<div class="m2"><p>مباش آینه عیب دیگران زنهار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمی توانی اگر تن به بی نیازی داد</p></div>
<div class="m2"><p>مگیر هیچ به جز عبرت از جهان زنهار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نظر به عاقبت حرف کن دهن بگشا</p></div>
<div class="m2"><p>نشان ندیده منه تیر در کمان زنهار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر چه صدق به خون شست صبح را رخسار</p></div>
<div class="m2"><p>میار جز سخن راست بر زبان زنهار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر به ساحل مقصد رسیدنت هوس است</p></div>
<div class="m2"><p>چو موج باش درین بحر خوش عنان زنهار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جهان رباط خراب وجهانیان سفری</p></div>
<div class="m2"><p>مخواه خاطر جمع از مسافران زنهار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کجی وراستی آ ب روشن است ازجوی</p></div>
<div class="m2"><p>مبند کجروی خود به آسمان زنهار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تراکه هست پریزاد معنوی صائب</p></div>
<div class="m2"><p>مبین به صورت بی معنی بتان زنهار</p></div></div>