---
title: >-
    غزل شمارهٔ ۳۷۸۳
---
# غزل شمارهٔ ۳۷۸۳

<div class="b" id="bn1"><div class="m1"><p>ترا به یوسف مصر اشتباه نتوان کرد</p></div>
<div class="m2"><p>قیاس آب روان را به چاه نتوان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آفتاب قیامت توان به جرأت دید</p></div>
<div class="m2"><p>نظر دلیر در آن روی ماه نتوان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به رشته گوهر شهوار می توان سفتن</p></div>
<div class="m2"><p>به گریه در دل سخت تو راه نتوان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میسرست چو از روی یار گل چیدن</p></div>
<div class="m2"><p>ستم ز دیدن گل بر نگاه نتوان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گشاره روی محال است تنگدل گردد</p></div>
<div class="m2"><p>زمین میکده را خانقاه نتوان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خموش باش که چون خامه پریشان گوی</p></div>
<div class="m2"><p>به حرف و صوت دل خود سیاه نتوان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بس که حسن غیور تو سرکش افتاده است</p></div>
<div class="m2"><p>ترا نگاه به طرف کلاه نتوان کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو مو سفید شود بر مدار سر ز سجود</p></div>
<div class="m2"><p>نماز فوت درین صبحگاه نتوان کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>توان کشید ز فولاد ریشه جوهر</p></div>
<div class="m2"><p>ز دل به سعی برون حب جان نتوان کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خوش است داغ که از لخت دل برآرد دود</p></div>
<div class="m2"><p>همین چو لاله ورق را سیاه نتوان کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خدا به لطف کند چاره دل صائب</p></div>
<div class="m2"><p>که مبتلاست به دردی که آه نتوان کرد</p></div></div>