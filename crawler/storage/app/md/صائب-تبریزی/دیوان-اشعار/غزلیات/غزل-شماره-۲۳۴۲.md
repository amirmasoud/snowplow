---
title: >-
    غزل شمارهٔ ۲۳۴۲
---
# غزل شمارهٔ ۲۳۴۲

<div class="b" id="bn1"><div class="m1"><p>جان مشتاقان زکوی دلستان چون بگذرد؟</p></div>
<div class="m2"><p>کاروان شبنم از ریگ روان چون بگذرد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقطه ها طوطی شوند و حرفها تنگ شکر</p></div>
<div class="m2"><p>بر زبان خامه نام آن دهان چون بگذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خار در راه نسیم بی ادب نگذاشته است</p></div>
<div class="m2"><p>غیرت بلبل زخون باغبان چون بگذرد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پر زند تا روز محشر در فضای لامکان</p></div>
<div class="m2"><p>تیر آهم از ترنج آسمان چون بگذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون صدف تبخاله ای هر گوشه لب وا کرده است</p></div>
<div class="m2"><p>از لب من گریه آتش عنان چون بگذرد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگسل از کج بحث تا از صد کشاکش وارهی</p></div>
<div class="m2"><p>بر نشان یابد ظفر تیر از کمان چون بگذرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همرهان رفتند اما داغشان از دل نرفت</p></div>
<div class="m2"><p>آتشی بر جای ماند کاروان چون بگذرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشم را با سرمه پیوندی است از روز ازل</p></div>
<div class="m2"><p>صائب از گلگشت سیر اصفهان چون بگذرد؟</p></div></div>