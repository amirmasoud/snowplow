---
title: >-
    غزل شمارهٔ ۳۶۶۹
---
# غزل شمارهٔ ۳۶۶۹

<div class="b" id="bn1"><div class="m1"><p>به درد و داغ دل بیقرار می چسبد</p></div>
<div class="m2"><p>شرر به سوخته بی اختیار می چسبد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نصیب صافدلان از جهان تماشایی است</p></div>
<div class="m2"><p>کجا به آینه نقش و نگار می چسبد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازان ز باغ برون سرو من نمی آید</p></div>
<div class="m2"><p>که گل به دامن او همچو خار می چسبد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به روی آب بود نعل نقش در آتش</p></div>
<div class="m2"><p>چسان به دست بلورین نگار می چسبد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لباس فقر به بالای اهل دل صائب</p></div>
<div class="m2"><p>چو داغ بر جگر لاله زار می چسبد</p></div></div>