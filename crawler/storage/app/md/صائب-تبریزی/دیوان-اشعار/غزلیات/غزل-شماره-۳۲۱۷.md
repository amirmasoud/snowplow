---
title: >-
    غزل شمارهٔ ۳۲۱۷
---
# غزل شمارهٔ ۳۲۱۷

<div class="b" id="bn1"><div class="m1"><p>زمن راز دل صدچاک پوشیدن نمی آید</p></div>
<div class="m2"><p>زبوی گل، نفس در سینه دزدیدن نمی آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگرچه خار این بستانم، اما خار دیوارم</p></div>
<div class="m2"><p>زدست کوته من دل خراشیدن نمی آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر دریای گوهر زیر دامن چون حباب آرم</p></div>
<div class="m2"><p>زچشم سیر من بر خویش بالیدن نمی آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زخواب نیستی برجسته ام از شورش هستی</p></div>
<div class="m2"><p>زدست من بغیر از چشم مالیدن نمی آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فلکها سینه می دزدند از داغ جنون من</p></div>
<div class="m2"><p>زهر کم ظرف رطل عشق نوشیدن نمی آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا مست لقا سر در بیابان جهان دادی</p></div>
<div class="m2"><p>ندانستی ز مستان غیر لغزیدن نمی آید؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به هر سروی که پیچم نگسلم پیوند از و هرگز</p></div>
<div class="m2"><p>زمن چون تاک بر هر نخل پیچیده نمی آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بشو دست از جهان گر چشم فیض از صبحدم داری</p></div>
<div class="m2"><p>که از دست نگارین شیر دوشیدن نمی آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به یک نیت تمام عمر می آرم بسر صائب</p></div>
<div class="m2"><p>به هر نیت زمن چون قرعه غلطیدن نمی آید</p></div></div>