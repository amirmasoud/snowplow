---
title: >-
    غزل شمارهٔ ۱۹۸۵
---
# غزل شمارهٔ ۱۹۸۵

<div class="b" id="bn1"><div class="m1"><p>هر کس بیاض گردن او را ندیده است</p></div>
<div class="m2"><p>افسانه ای ز صبح قیامت شنیده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفاق محو قد قیامت خرام اوست</p></div>
<div class="m2"><p>این مصرع بلند به عالم دویده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آب حیات، خشک بود در مذاق او</p></div>
<div class="m2"><p>هر کس به مستی آن لب میگون مکیده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز سبز تلخ من که برآورده است خط</p></div>
<div class="m2"><p>تیغ سیاه تاب به جوهر که دیده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خونی که مشک گشت دلش می شود سیاه</p></div>
<div class="m2"><p>زان سفله کن حذر که به دولت رسیده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>معیار آرمیدگی مجلس است شمع</p></div>
<div class="m2"><p>تا دل بجاست وضع جهان آرمیده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب ز برگریز برد فیض نوبهار</p></div>
<div class="m2"><p>چون غنچه هر که سر به گریبان کشیده است</p></div></div>