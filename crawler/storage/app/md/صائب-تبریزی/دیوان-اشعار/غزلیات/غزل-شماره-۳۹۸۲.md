---
title: >-
    غزل شمارهٔ ۳۹۸۲
---
# غزل شمارهٔ ۳۹۸۲

<div class="b" id="bn1"><div class="m1"><p>اسیر عشق تو دلتنگ از الم نشود</p></div>
<div class="m2"><p>حجاب خنده این کبک کوه غم نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کجا به درهم ودینار می شود معمور</p></div>
<div class="m2"><p>به درد وداغ تو هر دل که محتشم نشود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز حرف مردم عالم کشیده دار انگشت</p></div>
<div class="m2"><p>که زود عمر تو کوتاه چون قلم نشود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کراست زهره تواند به گرد ما گردید</p></div>
<div class="m2"><p>اگر کبوتر ما دور از حرم نشود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به زیر بار ستم روزگار خم سازد</p></div>
<div class="m2"><p>ز بار طاعت حق قامتی که خم نشود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به سنگ کم نکند التفات مرد تمام</p></div>
<div class="m2"><p>خداپرست مقید به یک صنم نشود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که رو نهاد به هستی که از پشیمانی</p></div>
<div class="m2"><p>نفس گسسته به معموره عدم نشود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز انقلاب توان برد جان به همواری</p></div>
<div class="m2"><p>که آب آینه هرگز زیاد وکم نشود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شود ز گردگنه پاک سینه ای صائب</p></div>
<div class="m2"><p>که غافل از نفس پاک صبحدم نشود</p></div></div>