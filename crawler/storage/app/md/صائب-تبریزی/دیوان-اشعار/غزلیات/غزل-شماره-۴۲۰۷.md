---
title: >-
    غزل شمارهٔ ۴۲۰۷
---
# غزل شمارهٔ ۴۲۰۷

<div class="b" id="bn1"><div class="m1"><p>عاشق حذر ز دیده اختر نمی کند</p></div>
<div class="m2"><p>از آتش احتراز سمندر نمی کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عارف ز شاهدان مجازست بی نیاز</p></div>
<div class="m2"><p>دریاکش التفات به ساغر نمی کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نتوان فریفت تشنه دیدار را به آب</p></div>
<div class="m2"><p>عاشق نظر به چشمه کوثر نمی کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داغی که هست در جگر قدردان عشق</p></div>
<div class="m2"><p>با آفتاب وماه برابر نمی کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقصان کمال می شود از کیمیای خلق</p></div>
<div class="m2"><p>خامی خلل به قیمت عنبرنمی کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون تیرگی نمی رود از داغ لاله زار</p></div>
<div class="m2"><p>دل را سیاه اگر می احمر نمی کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن را که همچو برق بود تیغ آتشین</p></div>
<div class="m2"><p>اندیشه از سیاهی لشکر نمی کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با یک دل این فغان که من زار می کنم</p></div>
<div class="m2"><p>با صد دل شکسته صنوبر نمی کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در کندن بنای گرانسنگ ظالمان</p></div>
<div class="m2"><p>سیلاب کار یک مژه تر نمی کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر سنگ آبگینه خود تا نمی زند</p></div>
<div class="m2"><p>لب تر ز آب خضر سکندر نمی کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از آه روشنایی دل بیش می شود</p></div>
<div class="m2"><p>اندیشه این چراغ ز صرصر نمی کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سخت است پاک ساختن دل ز آرزو</p></div>
<div class="m2"><p>صیقل علاج ریشه جوهر نمی کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صائب به روی هر که در دل گشوده شد</p></div>
<div class="m2"><p>چشم امید حلقه هر در نمی کند</p></div></div>