---
title: >-
    غزل شمارهٔ ۳۲۱۴
---
# غزل شمارهٔ ۳۲۱۴

<div class="b" id="bn1"><div class="m1"><p>زدل کاری که آید از لب خندان نمی آید</p></div>
<div class="m2"><p>گشاد تیر از سوفار چون پیکان نمی آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندارد اختیاری چشم من در محو گردیدن</p></div>
<div class="m2"><p>نظر پوشیدن از آیینه حیران نمی آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر آن رخسار نازک از نگاه تند می لرزم</p></div>
<div class="m2"><p>که طفل شوخ دست خالی از بستان نمی آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفس در پرده داری صبح می سوزد، نمی داند</p></div>
<div class="m2"><p>که مستوری زخورشید سبک جولان نمی آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کناری گیر ای مژگان زچشم خونفشان من</p></div>
<div class="m2"><p>که با دریا زدن سرپنجه از مرجان نمی آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل غمگین زنقل و جام هیهات است بگشاید</p></div>
<div class="m2"><p>گشاد این گره از ناخن و دندان نمی آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به یک کس دل نبندد دولت هر جایی دنیا</p></div>
<div class="m2"><p>سکندر کامیاب از چشمه حیوان نمی آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ندارد، هر که دارد پیچ و تابی، وحشت از خلوت</p></div>
<div class="m2"><p>به پای خود برون زنجیر از زندان نمی آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مجو آرامش از جان مقدس در تن خاکی</p></div>
<div class="m2"><p>که خودداری زدست گوهر غلطان نمی آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر روشندلی بر تیره بختی صبر کن صائب</p></div>
<div class="m2"><p>که بیرون از سیاهی چشمه حیوان نمی آید</p></div></div>