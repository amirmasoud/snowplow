---
title: >-
    غزل شمارهٔ ۴۵۹۷
---
# غزل شمارهٔ ۴۵۹۷

<div class="b" id="bn1"><div class="m1"><p>از فروغ ماه می گردد به آب وتاب ابر</p></div>
<div class="m2"><p>جلوه شکر کند باشیر، در مهتاب ابر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چنین بندد به خشکی کشتی احسان محیط</p></div>
<div class="m2"><p>یکقلم چون کاغذ ابری شود بی آب ابر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در گره بسته است دریا آب خود راچون گهر</p></div>
<div class="m2"><p>خشک می آید برون از بحر چون قلاب ابر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش ازین می ریخت ازدستش گهر بی اختیار</p></div>
<div class="m2"><p>درزمان کشت ما شد گوهر نایاب ابر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خازن گوهر ندارد از ترشرویی گزیر</p></div>
<div class="m2"><p>بر سیاهی می زند چون می شود شاداب ابر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می کند دلهای شب در گریه طوفان، دیده ام</p></div>
<div class="m2"><p>می شود گویا به چشمم پرده های خواب ابر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در زمان تنگدستی دل به حق روی آورد</p></div>
<div class="m2"><p>روبه دریا می رود چون می شود بی آب ابر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زیر بارمنت احسان، نمی ماند کریم</p></div>
<div class="m2"><p>وام دریا راکند تسلیم از سیلاب ابر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در کف دست کریمان نیست گوهر راقرار</p></div>
<div class="m2"><p>قطره را از بیقراری می کند سیماب ابر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آبرو صائب نریزد پیش دریا بعد ازین</p></div>
<div class="m2"><p>گر شود از دیده خونبار من سیراب ابر</p></div></div>