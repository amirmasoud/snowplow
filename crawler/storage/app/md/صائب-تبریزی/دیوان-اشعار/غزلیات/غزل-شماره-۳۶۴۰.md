---
title: >-
    غزل شمارهٔ ۳۶۴۰
---
# غزل شمارهٔ ۳۶۴۰

<div class="b" id="bn1"><div class="m1"><p>از تماشا دل افسرده ما نگشاید</p></div>
<div class="m2"><p>گره از غنچه پیکان به صبا نگشاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن که در خانه اغیار کمر باز کند</p></div>
<div class="m2"><p>از کمر تیغ به کاشانه ما نگشاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با حریفان همه شب در ته یک پیرهن است</p></div>
<div class="m2"><p>آن که در خانه ما بند قبا نگشاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زور می رحم به نومیدی ما خواهد کرد</p></div>
<div class="m2"><p>محتسب گر در میخانه به ما نگشاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صائب امید به ستاری یزدان داریم</p></div>
<div class="m2"><p>که سر نامه ما روز جزا نگشاید</p></div></div>