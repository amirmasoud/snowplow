---
title: >-
    غزل شمارهٔ ۶۱۶۳
---
# غزل شمارهٔ ۶۱۶۳

<div class="b" id="bn1"><div class="m1"><p>سرو گلزار ارم یا قامت دلجوست این؟</p></div>
<div class="m2"><p>زلف مشکین یا کمند گردن آهوست این؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اختر صبح سعادت، مرکز پرگار عشق</p></div>
<div class="m2"><p>تخم آه آتشین یا خال عنبربوست این؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بال شاهین نظر، طغرای شاهنشاه حسن</p></div>
<div class="m2"><p>طاق آتشگاه عارض یا خم ابروست این؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرده دار آب حیوان، ابر گلزار بهشت</p></div>
<div class="m2"><p>تار و پود جامه کعبه است یا گیسوست این؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موج آب زندگی یا جوهر تیغ قضا</p></div>
<div class="m2"><p>سرنوشت عاشقان یا پیچ و تاب موست این؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز آفتاب عارضش خط شعاعی سوخته است</p></div>
<div class="m2"><p>یا به دور ماه رویش زلف عنبربوست این؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حسنش از خط می کند منشور زیبایی درست</p></div>
<div class="m2"><p>یا دعای چشم زخم آن بهشتی روست این؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فتنه ها از یک گریبان سر برون آورده اند</p></div>
<div class="m2"><p>یا صف مژگان به گرد نرگس جادوست این؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خضر می روید به جای سبزه از جولانگهش</p></div>
<div class="m2"><p>آب حیوان یا خرام قامت دلجوست این؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چرب می سازد علم از خون آهوی حرم</p></div>
<div class="m2"><p>رحم در خاطر ندارد، غمزه جادوست این</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اینقدر وحشی نمی باشد ز مردم آدمی</p></div>
<div class="m2"><p>یا پریزاد قباپوش است، یا آهوست این</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از نگاه دیده قربانیان رم می کند</p></div>
<div class="m2"><p>سخت وحشی طینت و بسیار نازک خوست این</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سربرآورده است صائب زان گریبان آفتاب</p></div>
<div class="m2"><p>یا غلط کرده است مشرق را قمر، یاروست این؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیست بزم شاه جای دم زدن جبریل را</p></div>
<div class="m2"><p>پیش شاه نکته دان صائب چه گفت و گوست این؟</p></div></div>