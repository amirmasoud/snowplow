---
title: >-
    غزل شمارهٔ ۳۳۸
---
# غزل شمارهٔ ۳۳۸

<div class="b" id="bn1"><div class="m1"><p>اگر غفلت نهان در سنگ خارا می‌کند ما را</p></div>
<div class="m2"><p>جوانمردست درد عشق، پیدا می‌کند ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندارد صرفه‌ای آیینه ما را جلا دادن</p></div>
<div class="m2"><p>شود رسوای عالم هر که رسوا می‌کند ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تماشای بساط زودسیر عالم امکان</p></div>
<div class="m2"><p>نظر پوشیدنی دارد که بینا می‌کند ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر روشنگر حیرت به حال ما نپردازد</p></div>
<div class="m2"><p>که دیگر ساده از نقش تمنا می‌کند ما را؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چون قطره در دریای کثرت راه ما افتد</p></div>
<div class="m2"><p>خیال دور گرد یار، تنها می‌کند ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به تلخی قطره ما را ز دریا ابر اگر گیرد</p></div>
<div class="m2"><p>به شیرینی دگر در کار دریا می‌کند ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کدامین غبن ازین افزون بود ما بی‌نیازان را؟</p></div>
<div class="m2"><p>که چرخ بی‌بصیرت خرج دنیا می‌کند ما را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز چشم بد خدا آن چشم میگون را نگه دارد!</p></div>
<div class="m2"><p>که در هر گردشی مست تماشا می‌کند ما را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همین عشقی که روز ما ازو شب شد، اگر خواهد</p></div>
<div class="m2"><p>به داغی آفتاب عالم‌آرا می‌کند ما را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر چون شانه از هر چاک، دل راهی کند پیدا</p></div>
<div class="m2"><p>همان زلف سبک‌دستش ز سر وامی‌کند ما را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنین معلوم شد از گوشمال آسمان صائب</p></div>
<div class="m2"><p>که بهر محفل دیگر مهیا می‌کند ما را</p></div></div>