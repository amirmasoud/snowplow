---
title: >-
    غزل شمارهٔ ۶۴۹۸
---
# غزل شمارهٔ ۶۴۹۸

<div class="b" id="bn1"><div class="m1"><p>چه باشد حاصل مرغ چمن ای گلعذار از تو؟</p></div>
<div class="m2"><p>که از گل می خورد صد کاسه خون هردم بهار از تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکرر بر سر بالین شبنم آفتاب آمد</p></div>
<div class="m2"><p>نشد روشن شود یک بار چشم اشکبار از تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا شرمنده کردی از دل امیدوار خود</p></div>
<div class="m2"><p>مبادا هیچ کافر در جهان امیدوار از تو!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد از سنگینی بیماریم دل نرم دشمن را</p></div>
<div class="m2"><p>به پنهان دیدنی هرگز نگشتم شرمسار از تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به تلخی می توانی شکرستان کرد عالم را</p></div>
<div class="m2"><p>که دارد آرزوی بوس و امید کنار از تو؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به جامی دستگیری کن خمارآلوده خود را</p></div>
<div class="m2"><p>چرا ای ابر رحمت بر دلی ماند غبار از تو؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا خود نیست یارای سؤال، آخر چه می گویی</p></div>
<div class="m2"><p>اگر پرسد گناه من کسی روز شمار از تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به قسمت راضیم ای سنگدل دیگر چه می خواهی؟</p></div>
<div class="m2"><p>خمار بی شراب از من، شراب بی خمار از تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نمی شد زخمی تیغ تغافل اینقدر صائب</p></div>
<div class="m2"><p>اگر می بود ممکن قطع امید ای نگار از تو</p></div></div>