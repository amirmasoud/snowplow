---
title: >-
    غزل شمارهٔ ۴۱۰۰
---
# غزل شمارهٔ ۴۱۰۰

<div class="b" id="bn1"><div class="m1"><p>لعل لبش ز سبزه خط دلنواز شد</p></div>
<div class="m2"><p>زین قفل زنگ بسته در عیش باز شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوران بی نیازی خوبی به سر رسید</p></div>
<div class="m2"><p>هر حلقه ای ز خط تو چشم نیاز شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چین از کمند وحشت نخجیر می برد</p></div>
<div class="m2"><p>زلف تو از کشاکش دلها دراز شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون غنچه خون دل ز شکر خنده اش چکد</p></div>
<div class="m2"><p>از منت نسیم دهانی که باز شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طومار زندگی ز طمع می شود تمام</p></div>
<div class="m2"><p>کوتاه عمر شمع ز دست دراز شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آفتاب پاک شود دامن نگاه</p></div>
<div class="m2"><p>چشمی که دید روی ترا پاکبازشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از گوهرش غبار یتیمی نمی رود</p></div>
<div class="m2"><p>آن راکه چون صدف لب خواهش فراز شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>محمود اگر چه زیروزبرکردهند را</p></div>
<div class="m2"><p>آخر شکسته از سر زلف ایاز شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از طفل مشربی همه اوقات عمر ما</p></div>
<div class="m2"><p>در گفتگوی ابجد عشق مجاز شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آزاده ای که پای به دامان خود کشید</p></div>
<div class="m2"><p>چون سرو در ریاض جهان سرفراز شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سودای ما ز سرزنش ناصحان فزود</p></div>
<div class="m2"><p>روشن چراغ ما ز دم سردگاز شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>طفلان تمام روی به صحرا نهاده اند</p></div>
<div class="m2"><p>در دشت تاجنون که هنگامه ساز شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صائب نمی شود خمش از سرمه خزان</p></div>
<div class="m2"><p>هرکس به ذوق بلبل ما نغمه ساز شد</p></div></div>