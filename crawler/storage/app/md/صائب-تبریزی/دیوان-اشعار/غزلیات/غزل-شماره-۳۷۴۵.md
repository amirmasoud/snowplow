---
title: >-
    غزل شمارهٔ ۳۷۴۵
---
# غزل شمارهٔ ۳۷۴۵

<div class="b" id="bn1"><div class="m1"><p>صراحیی که دم صبح قلقلی دارد</p></div>
<div class="m2"><p>چو بلبلی است که مد نظر گلی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زبان شانه ز وصفش به یکدیگر پیچید</p></div>
<div class="m2"><p>کجا بهشت چو آن زلف سنبلی دارد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرم ز شعله سودا چو دود می نگرد</p></div>
<div class="m2"><p>مگر دلم سر پیوند کاکلی دارد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز حال صائب و نومیدیش چه می پرسی</p></div>
<div class="m2"><p>نمی رسد به تو دستش، توکلی دارد</p></div></div>