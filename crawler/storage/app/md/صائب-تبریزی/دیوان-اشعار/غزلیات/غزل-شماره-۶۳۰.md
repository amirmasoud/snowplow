---
title: >-
    غزل شمارهٔ ۶۳۰
---
# غزل شمارهٔ ۶۳۰

<div class="b" id="bn1"><div class="m1"><p>شکست نقش مرادست بوریای مرا</p></div>
<div class="m2"><p>نسیم فتح، قلم می کند لوای مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بیم دوزخ اگر فارغم ز غفلت نیست</p></div>
<div class="m2"><p>که می دهد عمل من همان سزای مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نظر به دانه کس نیست سیر چشمان را</p></div>
<div class="m2"><p>به آب خشک بود گردش آسیای مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی هزار شد از وصل بی قراری دل</p></div>
<div class="m2"><p>نکرد سرمه منزل خمش درای مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رسیده است به جایی گران رکابی خواب</p></div>
<div class="m2"><p>که توتیای قلم ساخته است پای مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنان به پیکر من ضعف زور آورده است</p></div>
<div class="m2"><p>که فرق نیست ز قد دوتا، عصای مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بس که نور بصیرت نمانده در مردم</p></div>
<div class="m2"><p>به نرخ خاک نگیرند توتیای مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز گرمی طلب از بس که داغدار شده است</p></div>
<div class="m2"><p>زمین ز خویش کند دور، نقش پای مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شود ز آب وضو تازه، داغ های ریا</p></div>
<div class="m2"><p>مگر شراب نمازی کند ردای مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هلال عید شود حلقه برون درم</p></div>
<div class="m2"><p>شبی که روی تو روشن کند سرای مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا ز نعمت دیدار سیر نتوان کرد</p></div>
<div class="m2"><p>که ساختند نگون کاسه گدای مرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نظر به صیقل مردم ندارد آینه ام</p></div>
<div class="m2"><p>چو بحر، موجه من می دهد جلای مرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به سنگلاخ اگر راه سیل من افتد</p></div>
<div class="m2"><p>چنان روم که کسی نشنود صدای مرا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نهشت سبزه خوابیده در سراسر باغ</p></div>
<div class="m2"><p>به عندلیب چه نسبت بود نوای مرا؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>قدم شمرده نهم بر بساط گل صائب</p></div>
<div class="m2"><p>ز بس که خار ملامت گزیده پای مرا</p></div></div>