---
title: >-
    غزل شمارهٔ ۳۵۸۰
---
# غزل شمارهٔ ۳۵۸۰

<div class="b" id="bn1"><div class="m1"><p>رفت در خلوت مینا گل و بلبل آسود</p></div>
<div class="m2"><p>بلبل از ناله فرو بست لب و گل آسود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شعله گرمی هنگامه گلزار نشست</p></div>
<div class="m2"><p>غنچه شد شهرت گل، دیده بلبل آسود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دم شمشیر تغافل همه را با من بود</p></div>
<div class="m2"><p>من چو رفتم ز میان، تیغ تغافل آسود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرتو صبح بناگوش گران بود بر او</p></div>
<div class="m2"><p>چون گل روی تو در سایه سنبل آسود؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون به زیر فلک از تفرقه ایمن باشم؟</p></div>
<div class="m2"><p>نتوان فصل بهاران به تو پل آسود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم جادو گرش آن روز که آمد به سخن</p></div>
<div class="m2"><p>در دل چه دل جادوگر بابل آسود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>توسنی نیست توکل که سکندر بخورد</p></div>
<div class="m2"><p>هرکه بسپرد عنان را به توکل آسود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با چنان شوخی طبعی که به گل خنده زدی</p></div>
<div class="m2"><p>در دل خاک چسان طالب آمل آسود؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حیرتی دارم ازان شبنم غافل صائب</p></div>
<div class="m2"><p>که به دور رخ او بر ورق گل آسود</p></div></div>