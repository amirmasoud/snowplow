---
title: >-
    غزل شمارهٔ ۱۸
---
# غزل شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>من ملایم کردم از آه آسمان سخت را</p></div>
<div class="m2"><p>نرم از آتش می توان کردن کمان سخت را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سختی ایام را مردن تلافی می کند</p></div>
<div class="m2"><p>عذرخواهی هست چون مغز استخوان سخت را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر نمی گردید پیدا، مصرفی چون بیستون</p></div>
<div class="m2"><p>ما چه می کردیم چون فرهاد، جان سخت را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سختیی کان نیست ذاتی، زود زایل می شو</p></div>
<div class="m2"><p>می توان کردن به آبی نرم، نان سخت را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نرمتر از مغز گردانید در کام هما</p></div>
<div class="m2"><p>زور بازوی قناعت، استخوان سخت را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست حرف نرم را تأثیر در آهن دلان</p></div>
<div class="m2"><p>ناوک از فولاد می باید نشان سخت را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قسمت منصور از دار فنا خمیازه بود</p></div>
<div class="m2"><p>من کشیدم گوش تا گوش این کمان سخت را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناله گرمی اگر صائب به فریادم رسد</p></div>
<div class="m2"><p>می کنم نرم آن دل نامهربان سخت را</p></div></div>