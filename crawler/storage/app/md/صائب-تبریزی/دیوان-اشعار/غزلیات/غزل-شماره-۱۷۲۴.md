---
title: >-
    غزل شمارهٔ ۱۷۲۴
---
# غزل شمارهٔ ۱۷۲۴

<div class="b" id="bn1"><div class="m1"><p>سیاه روی عقیق از جدایی یمن است</p></div>
<div class="m2"><p>کبود چهره یوسف ز دوری وطن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز پرتو دل روشن چو شمع در فانوس</p></div>
<div class="m2"><p>همیشه خلوت من در میان انجمن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز تهمت است چه اندیشه پاکدامن را؟</p></div>
<div class="m2"><p>که صبح صادق یوسف ز چاک پیرهن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر حیات ابد خواهی از سخن مگذر</p></div>
<div class="m2"><p>که آب خضر نهان در سیاهی سخن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز مرگ، روز هنرور نمی شود تاریک</p></div>
<div class="m2"><p>که برق تیشه چراغ مزار کوهکن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برون میار سر از کنج خامشی زنهار</p></div>
<div class="m2"><p>که در گداز بود شمع تا در انجمن است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سفینه اش به سلامت نمی رسد به کنار</p></div>
<div class="m2"><p>به چار موجه صحبت دلی که ممتحن است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز بس که مرده دل افتاده ای نمی بینی</p></div>
<div class="m2"><p>که چهره تو ز موی سفید در کفن است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به آب خضر تسلی نمی شود صائب</p></div>
<div class="m2"><p>دهان سوخته جانی که تشنه سخن است</p></div></div>