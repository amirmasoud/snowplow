---
title: >-
    غزل شمارهٔ ۵۱۳۴
---
# غزل شمارهٔ ۵۱۳۴

<div class="b" id="bn1"><div class="m1"><p>منم به گوشه چشمی ز آشنا قانع</p></div>
<div class="m2"><p>به خاک پای قناعت ز توتیا قانع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازان شده است به چشم جهانیان شیرین</p></div>
<div class="m2"><p>که ازلباس، شکر شد به بوریا قانع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زمال خویش به احسان تمتعی بردار</p></div>
<div class="m2"><p>مشو ز گنج به نامی چو اژدها قانع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دامن عرق انفعال دست زنید</p></div>
<div class="m2"><p>به عذر خشک مگردید از خطا قانع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همیشه راه به آب بقا نمی افتد</p></div>
<div class="m2"><p>مشو به دیدن ازان لعل جانفزا قانع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خطر زچشم بد چه ندارد آن رهرو</p></div>
<div class="m2"><p>که شد به راستی خویش از عصا قانع</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>(نظر به عاقبت کارکن قدم بردار</p></div>
<div class="m2"><p>مشو ز دیده بینا به پیش پا قانع)</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز لاله زار شهادت گلی بچین صائب</p></div>
<div class="m2"><p>به بوی خون مشو ازخاک کربلا قانع</p></div></div>