---
title: >-
    غزل شمارهٔ ۶۹۹۰
---
# غزل شمارهٔ ۶۹۹۰

<div class="b" id="bn1"><div class="m1"><p>پا به ادب نه که زخم خار نیابی</p></div>
<div class="m2"><p>بار به دلها منه که بار نیابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا به جگر نشکنی هزار تمنا</p></div>
<div class="m2"><p>سینه ریش و دل فگار نیابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نفس خویش را شمرده نسازی</p></div>
<div class="m2"><p>در دل خود عیش بی شمار نیابی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا نکنی از غذا به خاک قناعت</p></div>
<div class="m2"><p>ره به سر گنج همچو مار نیابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا نخورد کشتی تو سیلی طوفان</p></div>
<div class="m2"><p>ذوق هم آغوشی کنار نیابی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا نکنی چون کلیم داغ زبان را</p></div>
<div class="m2"><p>راه سخن پیش کردگار نیابی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا به سر بی کسان چو شمع نسوزی</p></div>
<div class="m2"><p>شمع پس از مرگ بر مزار نیابی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا نرسانی به آب، خانه تن را</p></div>
<div class="m2"><p>راه برون شد ازین حصار نیابی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرد تعلق ز خویش تا نفشانی</p></div>
<div class="m2"><p>آینه روح بی غبار نیابی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>راه فنا طی نمی شود به رعونت</p></div>
<div class="m2"><p>پایه منصور را ز دار نیابی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روی چو زر کار را چو زر کند اینجا</p></div>
<div class="m2"><p>برگ نگردیده زرد، بار نیابی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پرتو قهر حق است طاعت بی ذوق</p></div>
<div class="m2"><p>کار مکن تا نشاط کار نیابی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مشت غباری است جسم، روح سوارش</p></div>
<div class="m2"><p>آه درین گرد اگر سوار نیابی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کشتی عزم تو سخت سست عنان است</p></div>
<div class="m2"><p>ترسم ازین بحر خون گذار نیابی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سایه بال هماست دولت دنیا</p></div>
<div class="m2"><p>سایه به یک جای پایدار نیابی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خیز و شکاری بکن که در دو سه جولان</p></div>
<div class="m2"><p>گردی ازین دشت پرشکار نیابی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا نکنی ترک اعتبار چو صائب</p></div>
<div class="m2"><p>در نظر عشق اعتبار نیابی</p></div></div>