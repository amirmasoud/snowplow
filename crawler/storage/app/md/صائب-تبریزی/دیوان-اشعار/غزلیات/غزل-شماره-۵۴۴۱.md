---
title: >-
    غزل شمارهٔ ۵۴۴۱
---
# غزل شمارهٔ ۵۴۴۱

<div class="b" id="bn1"><div class="m1"><p>ما وفاداری به اسباب جهان نسپرده ایم</p></div>
<div class="m2"><p>لنگر تمکین به این ریگ روان نسپرده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ورق گردانی باد خزان آسوده ایم</p></div>
<div class="m2"><p>دل به رنگ و بوی باغ و بوستان نسپرده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از شتاب عمر ما را نیست بر خاطر غبار</p></div>
<div class="m2"><p>ایستادن ما به این آب روان نسپرده ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از عزیزی شاد و از خواری مکدر نیستیم</p></div>
<div class="m2"><p>امتیازی ما به ابنای زمان نسپرده ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قفل ما چون غنچه دارد از درون خود کلید</p></div>
<div class="m2"><p>ما گشاد دل به دست دیگران نسپرده ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می کشد از خانه ما را جذبه طفلان برون</p></div>
<div class="m2"><p>از جنون زوری به خود ما چون کمان نسپرده ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خودنمایی شیوه ما نیست چون نادیدگان</p></div>
<div class="m2"><p>جوهر دل را به شمشیر زبان نسپرده ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با بزرگی از زمین صد پله ایم افتاده تر</p></div>
<div class="m2"><p>شوکت و شانی به خود چون آسمان نسپرده ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرچه از دولت به آگاهی سرآید نعمت است</p></div>
<div class="m2"><p>هوشیاری ما به این رطل گران نسپرده ایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر لباس عاریت چون بخیه چسبیدن خطاست</p></div>
<div class="m2"><p>ما به دولت دل چو این نودولتان نسپرده ایم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر به سیم قلب می گیرند ما را مفت ماست</p></div>
<div class="m2"><p>نقد انصافی به اهل کاروان نسپرده ایم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قانعیم از سرو و بید این چمن با سایه ای</p></div>
<div class="m2"><p>ما برومندی به این بی حاصلان نسپرده ایم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با جنون ساده دل بوده است صائب کار ما</p></div>
<div class="m2"><p>اختیار خود به عقل کاردان نسپرده ایم</p></div></div>