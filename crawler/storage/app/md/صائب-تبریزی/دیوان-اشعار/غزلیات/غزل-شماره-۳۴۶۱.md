---
title: >-
    غزل شمارهٔ ۳۴۶۱
---
# غزل شمارهٔ ۳۴۶۱

<div class="b" id="bn1"><div class="m1"><p>دیده زنده دلان اشک فشان می باشد</p></div>
<div class="m2"><p>آب از قوت سرچشمه روان می باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست در انجمن وصل اشارت محرم</p></div>
<div class="m2"><p>در حرم صورت محراب نهان می باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صیقل سینه روشن گهران گفتارست</p></div>
<div class="m2"><p>طوطی لال بر آیینه گران می باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طفل را هر سر انگشت بود پستانی</p></div>
<div class="m2"><p>روزی بیخبران دست و دهان می باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشق از عشق به معشوق نمی پردازد</p></div>
<div class="m2"><p>کعبه اهل ادب سنگ نشان می باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دل پیر تمنای جوان بسیارست</p></div>
<div class="m2"><p>این بهاری است که در فصل خزان می باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می شود زندگی از قامت خم پا به رکاب</p></div>
<div class="m2"><p>تیر را شهپر پرواز کمان می باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه کند قرب به عشاق، که در دامن گل</p></div>
<div class="m2"><p>همچنان دیده شبنم نگران می باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر حذر باش ز خصمی که شود روگردان</p></div>
<div class="m2"><p>که هدف را خطر از پشت کمان می باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مشو از صحبت بی برگ و نوایان غافل</p></div>
<div class="m2"><p>که شب قدر نهان در رمضان می باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نشأة باده نگردد به کهنسالی کم</p></div>
<div class="m2"><p>ساکن کوی خرابات جوان می باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زندگانی به ته تیغ سرآرد صائب</p></div>
<div class="m2"><p>دل هرکس که به فرمان زبان می باشد</p></div></div>