---
title: >-
    غزل شمارهٔ ۶۱۷۶
---
# غزل شمارهٔ ۶۱۷۶

<div class="b" id="bn1"><div class="m1"><p>سایه تا افتاد ازان شمشاد بالا بر زمین</p></div>
<div class="m2"><p>آسمان رنگ قیامت ریخت گویا بر زمین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محو شد در روی او هر چشم بینایی که بود</p></div>
<div class="m2"><p>شبنمی نگذاشت آن خورشید سیما بر زمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سایه شمشاد جان بخش تو ای آب حیات</p></div>
<div class="m2"><p>کرد چون می خاکساری را گوارا بر زمین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خط مشکین کرد کوته، دست آن زلف دراز</p></div>
<div class="m2"><p>این سزای آن که مالد روی دلها بر زمین!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دل و دین پاک می سازد بساط خاک را</p></div>
<div class="m2"><p>چون کشد دامان ناز آن سرو بالا بر زمین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر سر ما خاکساران سایه کردن عیب نیست</p></div>
<div class="m2"><p>کآیه رحمت شود نازل ز بالا بر زمین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روز محشر پرده بر می دارد از اعمال تو</p></div>
<div class="m2"><p>می شود در نوبهاران دانه رسوا بر زمین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قمریی بر خاک صورت بندد از نقش قدم</p></div>
<div class="m2"><p>چون گذارد پای خود آن سرو بالا بر زمین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاکساری از سرافرازان عالم عیب نیست</p></div>
<div class="m2"><p>می نشیند آفتاب عالم آرا بر زمین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پرده دام است هر خاکی درین وحشت سرا</p></div>
<div class="m2"><p>تا نبینی پیش پای خود منه پا بر زمین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر که کم کم خرده خود صرف درویشان نکرد</p></div>
<div class="m2"><p>می گذارد همچو قارون جمله یکجا بر زمین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر کجا گوهر فزون تر، تشنه چشمی بیشتر</p></div>
<div class="m2"><p>می تپد چون ماهی بی آب، دریا بر زمین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سیل از افتادگی دیوار را از پا فکند</p></div>
<div class="m2"><p>سرکشان را روی می مالد مدارا بر زمین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیستم پرگار و چون پرگار از سرگشتگی</p></div>
<div class="m2"><p>هست در گردش مرا یک پا و یک پا بر زمین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همت سرشار بی ریزش نمی گیرد قرار</p></div>
<div class="m2"><p>داشت تا یک قطره می، ننشست مینا بر زمین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در بیابان راهش از موی کمر نازکترست</p></div>
<div class="m2"><p>هر که داند نوک خاری نیست بیجا بر زمین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن سبکدستم که آورده است در میدان لاف</p></div>
<div class="m2"><p>پشت پای من مکرر پشت دنیا بر زمین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از گرانجانی تو در بازار امکان مانده ای</p></div>
<div class="m2"><p>ورنه هیهات است ماند جنس عیسی بر زمین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شمع امیدش ز باد صبح روشنتر شود</p></div>
<div class="m2"><p>هر که چون خورشید مالد روی خود را بر زمین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خامه معجز رقم گر خضر وقت خویش نیست</p></div>
<div class="m2"><p>سبز چون گردد به هر جا می نهد پا بر زمین؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ثبت می سازد به خط سبز در هر نوبهار</p></div>
<div class="m2"><p>منشی رحمت برات روزی ما بر زمین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>قسمت آدم شد از روز ازل سر جوش فیض</p></div>
<div class="m2"><p>ریخت ساقی جرعه اول ز مینا بر زمین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عقل هیهات است مجنون را شکار خود کند</p></div>
<div class="m2"><p>می گذارد شیر پشت دست اینجا بر زمین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از سکندر صفحه آیینه ای بر جای ماند</p></div>
<div class="m2"><p>تا چه خواهد ماند از مجموعه ما بر زمین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گل چه صورت دارد از اجزای خود غافل شود؟</p></div>
<div class="m2"><p>دام صید آدمیزادست رگها بر زمین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>می دهد داغ عزیزان را فشار تازه ای</p></div>
<div class="m2"><p>لاله ای هر جا که می گردد هویدا بر زمین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سفره اهل قناعت صائب از نعمت پرست</p></div>
<div class="m2"><p>روزی موران بود دایم مهیا بر زمین</p></div></div>