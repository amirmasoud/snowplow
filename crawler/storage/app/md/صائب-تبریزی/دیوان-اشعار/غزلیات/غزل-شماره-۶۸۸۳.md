---
title: >-
    غزل شمارهٔ ۶۸۸۳
---
# غزل شمارهٔ ۶۸۸۳

<div class="b" id="bn1"><div class="m1"><p>دل عزیز به این تیره خاکدان چه دهی؟</p></div>
<div class="m2"><p>به مفت یوسف خود را به کاروان چه دهی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عنان به طول امل دادن از بصیرت نیست</p></div>
<div class="m2"><p>گهر ز دست به امید ریسمان چه دهی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترا گذر به غزالان قدس خواهد بود</p></div>
<div class="m2"><p>به هر شکار سگ نفس را عنان چه دهی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز عقل نیست به دریای خون شنا کردن</p></div>
<div class="m2"><p>شعور خود به می همچون ارغوان چه دهی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دم فسرده به تاراج می دهد دل را</p></div>
<div class="m2"><p>کلید با به غارتگر خزان چه دهی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بساط اطلس گردون تراست پای انداز</p></div>
<div class="m2"><p>چو کفش تن ز مذلت به آستان چه دهی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حیات ضامن روزی است دل قوی می دار</p></div>
<div class="m2"><p>به هرزه آب رخ خویش را به نان چه دهی؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ملایمت به حریفان سفله بی ثمرست</p></div>
<div class="m2"><p>زلال خضر به خار سیه زبان چه دهی؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز اشتیاق تو فردوس می خورد دل خویش</p></div>
<div class="m2"><p>چو غنچه دل به تماشای بوستان چه دهی؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو کز گنه دل خود همچو شب سیه کردی</p></div>
<div class="m2"><p>جواب ماه جبینان آسمان چه دهی؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به جوی شیر تسلی نمی شود عاشق</p></div>
<div class="m2"><p>به من به جای طباشیر استخوان چه دهی؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جواب آن غزل است این که اوحدی فرمود</p></div>
<div class="m2"><p>مراد دشمن و تشویش دوستان چه دهی؟</p></div></div>