---
title: >-
    غزل شمارهٔ ۶۷۹۴
---
# غزل شمارهٔ ۶۷۹۴

<div class="b" id="bn1"><div class="m1"><p>چهره را صیقلی از آتشِ می ساخته‌ای</p></div>
<div class="m2"><p>خبر از خویش نداری که چه پرداخته‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای بسا خانهٔ تقوی که رسیده‌ست به آب</p></div>
<div class="m2"><p>تا ز منزل عرق‌آلود برون تاخته‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در سرِ کوی تو چندان که نظر کار کند</p></div>
<div class="m2"><p>دل و دین است که بر یکدگر انداخته‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر از آب کنی آینه دیگر، ورنه</p></div>
<div class="m2"><p>هیچ آیینه نمانده‌ست که نگداخته‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون ز حالِ دلِ صاحب‌نظرانی غافل؟</p></div>
<div class="m2"><p>تو که در آینه با خویش نظر باخته‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو که از ناز به عشاق نمی‌پردازی</p></div>
<div class="m2"><p>صد هزار آینه هر سوی چه پرداخته‌ای؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست یک سرو درین باغ به رعناییِ تو</p></div>
<div class="m2"><p>بس که گردن به تماشای خود افراخته‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آتشی را که ازان طور به زنهار آید</p></div>
<div class="m2"><p>در دلِ صائبِ خونین‌جگر انداخته‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برخوری چون رهی از ساغرِ معنی صائب</p></div>
<div class="m2"><p>که درین تازه‌غزل شیشه تهی ساخته‌ای</p></div></div>