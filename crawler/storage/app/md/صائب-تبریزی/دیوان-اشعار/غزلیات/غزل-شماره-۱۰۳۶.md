---
title: >-
    غزل شمارهٔ ۱۰۳۶
---
# غزل شمارهٔ ۱۰۳۶

<div class="b" id="bn1"><div class="m1"><p>دیدن روی تو ظلم است و ندیدن کردن مشکل است</p></div>
<div class="m2"><p>چیدن این گل گناه است و نچیدن مشکل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چه جز معشوق باشد پرده بیگانگی است</p></div>
<div class="m2"><p>بوی یوسف را ز پیراهن شنیدن مشکل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست از جوش شهیدان تیغ را میدان زخم</p></div>
<div class="m2"><p>در سر کویش به کام دل تپیدن مشکل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لامکان بر وحشیان عشق تنگی می کند</p></div>
<div class="m2"><p>در فضای آسمان از خود رمیدن مشکل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی چراغان تجلی طور سنگ تفرقه است</p></div>
<div class="m2"><p>کعبه و بتخانه را بی یار دیدن مشکل است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غنچه را باد صبا از پوست می آرد برون</p></div>
<div class="m2"><p>بی نسیم شوق، پیراهن دریدن مشکل است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر ندارد میوه تا خام است دست از شاخسار</p></div>
<div class="m2"><p>زاهد ناپخته را از خود بریدن مشکل است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که در قید خودآرایی گره گردید، ماند</p></div>
<div class="m2"><p>آب را از پنجه گوهر چکیدن مشکل است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عقل و دین و دل درین سودا کم از بیعانه است</p></div>
<div class="m2"><p>با چنین سرمایه یوسف را خریدن مشکل است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی قراران هر نفس در عالمی جولان کنند</p></div>
<div class="m2"><p>همچو بوی گل به یک جا آرمیدن مشکل است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ماتم فرهاد کوه بیستون را سرمه داد</p></div>
<div class="m2"><p>بی هم آوازی نفس از دل کشیدن مشکل است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در گلستانی که بوی گل گرانی می کند</p></div>
<div class="m2"><p>با قفس بر عندلیب ما پریدن مشکل است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چشم خودبینی به هر ناکرده کاری داده اند</p></div>
<div class="m2"><p>کار عالم کردن و خود را ندیدن مشکل است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بازوی همت ضعیف و تیغ جرأت شیشه دل</p></div>
<div class="m2"><p>با سلاحی این چنین از خود بریدن مشکل است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا گمان نیش خاری هست در دشت وجود</p></div>
<div class="m2"><p>همچو خون مرده یک جا آرمیدن مشکل است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سایه بال هما در قبضه تسخیر نیست</p></div>
<div class="m2"><p>دامن دولت به سوی خود کشیدن مشکل است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر سر موی ترا با زندگی پیوندهاست</p></div>
<div class="m2"><p>با چنین دلبستگی از خود بریدن مشکل است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>با قیامت پاک کن اینجا حساب خویش را</p></div>
<div class="m2"><p>بر زمین از شرم عصیان خط کشیدن مشکل است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در جوانی توبه کن تا از ندامت برخوری</p></div>
<div class="m2"><p>نیست چون دندان، لب خود را گزیدن مشکل است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>منزل نقل مکان ماست اوج لامکان</p></div>
<div class="m2"><p>آسمانها را به گرد ما رسیدن مشکل است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون سلیمان را نباشد رشک بر احوال مور؟</p></div>
<div class="m2"><p>بار عالم را به دوش خود کشیدن مشکل است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>می توان راز دهان یار را تفسیر کرد</p></div>
<div class="m2"><p>در نزاکت های فکر ما رسیدن مشکل است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا نگردد جذبه توفیق صائب دستگیر</p></div>
<div class="m2"><p>از گل تعمیر، پای خود کشیدن مشکل است</p></div></div>