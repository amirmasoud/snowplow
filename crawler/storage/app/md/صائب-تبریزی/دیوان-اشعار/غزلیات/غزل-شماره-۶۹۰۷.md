---
title: >-
    غزل شمارهٔ ۶۹۰۷
---
# غزل شمارهٔ ۶۹۰۷

<div class="b" id="bn1"><div class="m1"><p>ای غنچه‌لب که سر به گریبان کشیده‌ای</p></div>
<div class="m2"><p>در پرده‌ای و پرده عالم دریده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برق سبک‌عنانی و کوه گران رکاب</p></div>
<div class="m2"><p>در هیچ جا نه و همه جا آرمیده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تمکین لفظ و شوخی معنی است در تو جمع</p></div>
<div class="m2"><p>در جلوه‌ای و پای به دامن کشیده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صد پیرهن غریب‌تر از یوسفی به حسن</p></div>
<div class="m2"><p>در مصر ساکنی و به کنعان رسیده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم بد از تو دور که چون طفل اشک من</p></div>
<div class="m2"><p>هر کوچه‌ای که هست به عالم، دویده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در پله غرور تو دل گرچه بی‌بهاست</p></div>
<div class="m2"><p>ارزان مده ز دست که یوسف خریده‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غیر از نگاه عجز که از دور می‌کند</p></div>
<div class="m2"><p>ای سنگدل ز صائب مسکین چه دیده‌ای؟</p></div></div>