---
title: >-
    غزل شمارهٔ ۳۵۲۲
---
# غزل شمارهٔ ۳۵۲۲

<div class="b" id="bn1"><div class="m1"><p>جرم یوسف به چه تقریب عزیزان بخشند؟</p></div>
<div class="m2"><p>بیگناهی گنهی نیست که آسان بخشند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست در طینت بیرحم تو چون بخشایش</p></div>
<div class="m2"><p>کاش صبری به من بی سر و سامان بخشند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مورم اما عوض گوشه بی توشه خویش</p></div>
<div class="m2"><p>نپذیرم اگرم ملک سلیمان بخشند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل بی خار به خار سر دیوار رسد</p></div>
<div class="m2"><p>چون زکات رخ او را به گلستان بخشند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آبرویی که بود چهره یوسف صدفش</p></div>
<div class="m2"><p>حیف باشد که به گوهرنشناسان بخشند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست کار در و دیوار عنانداری سیل</p></div>
<div class="m2"><p>کاش دیوانه ما را به بیابان بخشند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه بهشتی است اگر آینه رویان صائب</p></div>
<div class="m2"><p>تاب نظاره به چشم من حیران بخشند</p></div></div>