---
title: >-
    غزل شمارهٔ ۴۳۹۴
---
# غزل شمارهٔ ۴۳۹۴

<div class="b" id="bn1"><div class="m1"><p>زان سفله حذرکن که توانگرشده باشد</p></div>
<div class="m2"><p>زان موم بیندیش که عنبر شده باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امید گشایش نبود در گره بخل</p></div>
<div class="m2"><p>زان قطره مجوآب که گوهرشده باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنشین که چوپروانه به گرد توزند بال</p></div>
<div class="m2"><p>از روز ازل آنچه مقدر شده باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ایام حیاتش همه ایام بهارست</p></div>
<div class="m2"><p>روز و شب هرکس که برابرشده باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موقوف به یک جلوه مستانه ساقی است</p></div>
<div class="m2"><p>گر توبه من سد سکندر شده باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جایی که چکد باده ز سجاده تقوی</p></div>
<div class="m2"><p>سهل است اگر دامن ما تر شده باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در دامن محشر رگ ابری است گهربار</p></div>
<div class="m2"><p>مژگان تواز گریه اگرترشده باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از سنبل فردوس پریشان شودش مغز</p></div>
<div class="m2"><p>از زلف دماغی که معطرشده باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواهند سبک ساخت به سرگوشی تیغش</p></div>
<div class="m2"><p>از گوهر اگر گوش صدف کر شده باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آزاده نخوانند گرفتارهوا را</p></div>
<div class="m2"><p>گرصاحب صددل چو صنوبر شده باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از گریه شادی مژه اش خشک نگردد</p></div>
<div class="m2"><p>چشمی که در او یار مصور شده باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زندان غریبی شمرد دوش پدر را</p></div>
<div class="m2"><p>طفلی که بدآموزبه مادر شده باشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برباد دهد همچو حباب افسرخودرا</p></div>
<div class="m2"><p>بی مغزاگرصاحب افسر شده باشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عشق است درین عالم اگربال وپری هست</p></div>
<div class="m2"><p>رحم است برآن مرغ که بی پرشده باشد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لبهای می آلودبلای دل وجان است</p></div>
<div class="m2"><p>زان تیغ حذر کن که به خون ترشده باشد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر جا نبود شرم به تاراج رود حسن</p></div>
<div class="m2"><p>ویران شود آن باغ که بی در شده باشد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در غنچه بوددامن صحرای بهشتش</p></div>
<div class="m2"><p>آن را که دل تنگ میسر شده باشد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دانی که چه می بینم ازان قدورخ وزلف</p></div>
<div class="m2"><p>چشم توگرآیینه محشرشده باشد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نسبت به رخ تازه اودیده شورست</p></div>
<div class="m2"><p>آیینه اگرچشمه کوثرشده باشد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تکرار حلاوت برد از چاشنی جان</p></div>
<div class="m2"><p>پرهیز ز قندی که مکرر شده باشد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در دیده ارباب قناعت مه عیدست</p></div>
<div class="m2"><p>صائب لب نانی که به خون ترشده باشد</p></div></div>