---
title: >-
    غزل شمارهٔ ۲۳۷۹
---
# غزل شمارهٔ ۲۳۷۹

<div class="b" id="bn1"><div class="m1"><p>چاره سودای ما پند نصیحتگر نکرد</p></div>
<div class="m2"><p>تلخی دریا علاج خامی عنبر نکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حیرت رویش به مژگان فرصت جنبش نداد</p></div>
<div class="m2"><p>موج دست و پا درین بحر گران لنگر نکرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نزد مهر خموشی بر دهن با صد زبان</p></div>
<div class="m2"><p>بوستان پیرا دهان غنچه را پر زد نکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه چشم انتظار ما ید بیضا نمود</p></div>
<div class="m2"><p>بوی پیراهن سر از جیب مروت بر نکرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه عمری خویش را هموار کرد از پیچ و تاب</p></div>
<div class="m2"><p>رشته ما را کسی شیرازه گوهر نکرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همت ما پست ماند از پستی سقف فلک</p></div>
<div class="m2"><p>شعله ما راست قد خود درین محمر نکرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ریخت اشک آتشین در ماتم پروانه شمع</p></div>
<div class="m2"><p>عالمی را سوخت آن بیرحم و چشمی تر نکرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عالم پرشور گلزاری است بر روشندلان</p></div>
<div class="m2"><p>تلخی دریا اثر در طینت گوهر نکرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل فراموش کرد در زلف سیاه او مرا</p></div>
<div class="m2"><p>خضر در روز سیه پروای اسکندر نکرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا غبارم سرمه چشم تماشایی نشد</p></div>
<div class="m2"><p>درد سنگین مرا آن سنگدل باور نکرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تنگی گردون پر و بال مرا درهم شکست</p></div>
<div class="m2"><p>بیضه فولاد این بیداد بر جوهر نکرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صائب از تعجیل ایام بهاران غافل است</p></div>
<div class="m2"><p>هر که صاف و درد را چون لاله یک ساغر نکرد</p></div></div>