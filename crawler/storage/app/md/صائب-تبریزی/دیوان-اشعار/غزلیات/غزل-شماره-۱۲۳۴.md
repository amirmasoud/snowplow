---
title: >-
    غزل شمارهٔ ۱۲۳۴
---
# غزل شمارهٔ ۱۲۳۴

<div class="b" id="bn1"><div class="m1"><p>توبه نتوان کرد از می تا شراب ناب هست</p></div>
<div class="m2"><p>از تیمم دست باید شست هر جا آب هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صحبت اشراق را تیغ زبان در کار نیست</p></div>
<div class="m2"><p>شمع را خاموش باید کرد تا مهتاب هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالم آب از تنک ظرفان شود پر شور و شر</p></div>
<div class="m2"><p>آفت از دریا فزون در حلقه گرداب هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده خفاش طبعان محرم این راز نیست</p></div>
<div class="m2"><p>ورنه در هر ذره آن خورشید عالمتاب هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست ممکن از عبادت گرم گردد سینه ای</p></div>
<div class="m2"><p>زاهد افسرده تا در گوشه محراب هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می تواند حلقه بر در زد حریم حسن را</p></div>
<div class="m2"><p>در رگ جان هر که را چون زلف پیچ و تاب هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر توانی همچو مردان از سبب پوشید چشم</p></div>
<div class="m2"><p>عالمی دیگر به غیر از عالم اسباب هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواب آسایش نباشد خاطر آگاه را</p></div>
<div class="m2"><p>در بساط خاک تا یک دیده بیخواب هست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روزی بی خون دل کم جو که در بحر وجود</p></div>
<div class="m2"><p>بی کشاکش طعمه ای گر هست، در قلاب هست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست ممکن یک نفس صائب به کام دل کشد</p></div>
<div class="m2"><p>هر که را در سر هوای گوهر نایاب هست</p></div></div>