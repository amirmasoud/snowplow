---
title: >-
    غزل شمارهٔ ۳۶۹
---
# غزل شمارهٔ ۳۶۹

<div class="b" id="bn1"><div class="m1"><p>ز آه سرد پروا نیست عشاق بلاکش را</p></div>
<div class="m2"><p>کند بر دود صبر آن کس که می افروزد آتش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فلک با مردم ممتاز خصمی بیشتر دارد</p></div>
<div class="m2"><p>کمان اول کند آواره تیر روی ترکش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به فریاد سپند ما درین محفل که پردازد؟</p></div>
<div class="m2"><p>که اخگر در گریبان است از خوی تو آتش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز ابراهیم ادهم شهسواری پیش می افتد</p></div>
<div class="m2"><p>که در دولت نگه دارد عنان نفس سرکش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوام عشق اگر خواهی، مکن با وصل آمیزش</p></div>
<div class="m2"><p>که آب زندگی هم می کند خاموش آتش را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر روشندلی، راه از تو چندان نیست تا گردون</p></div>
<div class="m2"><p>که چون شبنم سفر آسان بود جان های بی غش را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خرد را پیروی از راه حاجت می کند نادان</p></div>
<div class="m2"><p>وگرنه کور از خود کورتر خواهد عصاکش را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به نور دل توان از ظلمت هستی برون آمد</p></div>
<div class="m2"><p>علاجی نیست جز بیداری این خواب مشوش را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ازان با وسعت مشرب ز مذهب ساختم صائب</p></div>
<div class="m2"><p>که یک آهوی وحشی نیست آن صحرای دلکش را</p></div></div>