---
title: >-
    غزل شمارهٔ ۲۱۹۶
---
# غزل شمارهٔ ۲۱۹۶

<div class="b" id="bn1"><div class="m1"><p>در خاک وطن چند توان ره به عصا رفت؟</p></div>
<div class="m2"><p>کو وادی غربت که توان رو به قفا رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بس قدح تلخ مکافات کشیدم</p></div>
<div class="m2"><p>از خاطر من دغدغه روز جزا رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خضر ره ارباب طلب، عزم درست است</p></div>
<div class="m2"><p>آواره شد آن کس که پی راهنما رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا چند توان دست دعا داشت بر افلاک؟</p></div>
<div class="m2"><p>این زور در ایام که بر دست دعا رفت؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن روز که خورشید قدح چهره برافروخت</p></div>
<div class="m2"><p>رنگ ادب از چهره گلزار حیا رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر حاصل ما چون جگر برق نسوزد؟</p></div>
<div class="m2"><p>از روی خزان رنگ ز بی برگی ما رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون از لب پیمانه من زهر نریزد؟</p></div>
<div class="m2"><p>صائب به من از گردش ایام چها رفت</p></div></div>