---
title: >-
    غزل شمارهٔ ۴۹۰۳
---
# غزل شمارهٔ ۴۹۰۳

<div class="b" id="bn1"><div class="m1"><p>پوچ شد از دعوی بیهوده مغز خود فروش</p></div>
<div class="m2"><p>آب را کف می کند دیگی که ننشیند ز جوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می کنند از سود، مردم خرج و ازبی حاصلی</p></div>
<div class="m2"><p>می کند از مایه خود خرج دایم خود فروش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از هزار آهو یکی راناف مشکین داده اند</p></div>
<div class="m2"><p>صوفی صافی نگردد هرکه شد پشمینه پوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرچه می گویند بامن ناصحان شایسته ام</p></div>
<div class="m2"><p>بی تأمل پنبه غفلت بر آوردیم ز گوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می کند مستی گوارا تلخی ایام را</p></div>
<div class="m2"><p>وای برآن کس که می آید درین محفل به هوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می زند حرفی برای خویش واعظ، می بکش</p></div>
<div class="m2"><p>نیست پشمی در کلاه محتسب، ساغر بنوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خرقه آلوده ما را بهای می گرفت</p></div>
<div class="m2"><p>نیست در اندک پذیری کس چو پیر میفروش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاشقان رااز گرستن دل نمی گردد خنک</p></div>
<div class="m2"><p>چشمه خورشید را شبنم نیندازد ز جوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست گر پیوسته با هم تاوپود حسن و عشق</p></div>
<div class="m2"><p>چون شود پروانه ساکن؟ شمع چون گردد خموش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دست بردل می نهم چون شوق غالب می شود</p></div>
<div class="m2"><p>می کنم با خاک آتش را زبی آبی خموش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرچه ازنطق است صائب جوهر تیغ زبان</p></div>
<div class="m2"><p>درنظر دارد شکوه تیغ، لبهای خموش</p></div></div>