---
title: >-
    غزل شمارهٔ ۲۸۹
---
# غزل شمارهٔ ۲۸۹

<div class="b" id="bn1"><div class="m1"><p>تا شد از صدق طلب چون صبح، روشن جان ما</p></div>
<div class="m2"><p>از تنور سرد، آید گرم بیرون نان ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خزف ناز گهر از بردباری می کشیم</p></div>
<div class="m2"><p>سنگ کم گردد تمام از پله میزان ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رزق ما آید به پای میهمان از خوان غیب</p></div>
<div class="m2"><p>میزبان ماست هر کس می شود مهمان ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما به تردستی زبان خصم کوته می کنیم</p></div>
<div class="m2"><p>سبز سازد خار دامنگیر را دامان ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشأه رطل گران از سنگ می یابیم ما</p></div>
<div class="m2"><p>هست در آزادی اطفال گلریزان ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می کنیم از ترزبانی دشمنان را مهربان</p></div>
<div class="m2"><p>می کند شیرین زمین شور را باران ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست چون آیینه تصویر، امید نجات</p></div>
<div class="m2"><p>عکس روی یار را از دیده حیران ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غافلان را شهپر طاوس می آید به چشم</p></div>
<div class="m2"><p>بس که رنگین شد ز الوان گنه دامان ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در گرفتاری ز بس ثابت قدم افتاده ایم</p></div>
<div class="m2"><p>برنخیزد ناله از زنجیر در زندان ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما ز گل پیراهنان صائب به بویی قانعیم</p></div>
<div class="m2"><p>از نسیمی یوسفستان می شود زندان ما</p></div></div>