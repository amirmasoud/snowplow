---
title: >-
    غزل شمارهٔ ۲۵۲۹
---
# غزل شمارهٔ ۲۵۲۹

<div class="b" id="bn1"><div class="m1"><p>صحبت روشن ضمیران جسمها را جان کند</p></div>
<div class="m2"><p>کوه را برق تجلی آتشین جولان کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حیرت روشندلان را نقشبند دیگرست</p></div>
<div class="m2"><p>نقش هیهات است این آیینه را حیران کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فیض مردان در زمان بیخودی افزونترست</p></div>
<div class="m2"><p>تیغ چون گردید عریان بیشتر طوفان کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می شود خار ملامت شهپر پرواز او</p></div>
<div class="m2"><p>گردبادی را که شور عشق سر گردان کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق سیل گوهر رازست در هر جا که هست</p></div>
<div class="m2"><p>شمع نتوانست اشک خویش را پنهان کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون زند جوش زبردستی محیط اشک من</p></div>
<div class="m2"><p>پنجه خورشید را سرپنجه مرجان کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دامن شادی چو غم آسان نمی آید به دست</p></div>
<div class="m2"><p>پسته را دل می شود خون تالبی خندان کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برنتابد قهرمان عشق استغنای حسن</p></div>
<div class="m2"><p>ماه کنعان را به جرم ناز در زندان کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باد دستان را به احسان دستگیری کن که بحر</p></div>
<div class="m2"><p>در سخای ابر با روی زمین احسان کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غیرت پروانه چون صائب بر آید از لباس</p></div>
<div class="m2"><p>شمع را از جامه فانوس در زندان کند</p></div></div>