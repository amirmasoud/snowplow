---
title: >-
    غزل شمارهٔ ۴۵۹۱
---
# غزل شمارهٔ ۴۵۹۱

<div class="b" id="bn1"><div class="m1"><p>گلعذار من برون از پرده بوی خود میار</p></div>
<div class="m2"><p>بیقراران رابجان از آرزوی خود میار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست ممکن چون رسیدن درتوای جان جهان</p></div>
<div class="m2"><p>عالم آسوده رادرجستجوی خود میار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست چون پروای دلجویی ترااز سرکشی</p></div>
<div class="m2"><p>از کمند جذبه دلها رابه سوی خود میار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دردسر خواهی کشیدن از هجوم بلبلان</p></div>
<div class="m2"><p>از لباس غنچه بیرون رنگ وبوی خود میار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می گدازد آه محرومان دل فولاد را</p></div>
<div class="m2"><p>بی سبب آیینه را در پیش روی خود میار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دو زلف خویش دست شانه راکوتاه کن</p></div>
<div class="m2"><p>صد دل آشفته را بیرون ز موی خود میار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا به اشک گرم بتوان دست ورویی تازه کرد</p></div>
<div class="m2"><p>از دگر سرچشمه ای آب وضوی خود میار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دارد آتش زیر پااین رنگهای عارضی</p></div>
<div class="m2"><p>غیر بیرنگی دگر رنگی به روی خودمیار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از ته دل گفتگوی اهل حق راگوش کن</p></div>
<div class="m2"><p>خالی از سرچشمه حیوان سبوی خودمیار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر به جرم سینه صافی سنگبارانت کنند</p></div>
<div class="m2"><p>همچو آب از بردباریهابه روی خود میار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رزق فرزندان حوالت کن به خیرالرازقین</p></div>
<div class="m2"><p>چون کبوتر طعمه بیرون از گلوی خود میار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیست ظرف باده پرزور هرکم ظرف را</p></div>
<div class="m2"><p>سیل بی زنهار را صائب به جوی خود میار</p></div></div>