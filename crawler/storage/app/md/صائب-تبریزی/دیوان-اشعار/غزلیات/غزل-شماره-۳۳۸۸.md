---
title: >-
    غزل شمارهٔ ۳۳۸۸
---
# غزل شمارهٔ ۳۳۸۸

<div class="b" id="bn1"><div class="m1"><p>گوهر عکس لبش گر به شراب اندازد</p></div>
<div class="m2"><p>کله عیش، می از جوش حباب اندازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جدل شبنم و خورشید بود مشت و درفش</p></div>
<div class="m2"><p>خرد آن به که سپر پیش شراب اندازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوس شهرت زند از خم چو فلاطون هرکس</p></div>
<div class="m2"><p>خشت برگیرد و از دست کتاب اندازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که خواهد که کند مشکل عالم را حل</p></div>
<div class="m2"><p>دفتر عقل همان به که در آب اندازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه ز مستی است نمک گر به شراب افکندم</p></div>
<div class="m2"><p>چشم تا چند به روی تو حباب اندازد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غلط اندازی حسن است که آب حیوان</p></div>
<div class="m2"><p>پرده بر روی خود از موج سراب اندازد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواب مخمل نبود در گرو افسانه</p></div>
<div class="m2"><p>بخت کی گوش به افسانه خواب اندازد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگرفته است کس آیینه خورشید به موم</p></div>
<div class="m2"><p>چون به رخسار تو مشاطه نقاب اندازد؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب از بحر به یک جرعه برآوردی گرد</p></div>
<div class="m2"><p>در خور ظرف تو، ساقی چه شراب اندازد؟</p></div></div>