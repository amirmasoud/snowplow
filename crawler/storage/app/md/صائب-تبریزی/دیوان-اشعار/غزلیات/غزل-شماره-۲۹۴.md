---
title: >-
    غزل شمارهٔ ۲۹۴
---
# غزل شمارهٔ ۲۹۴

<div class="b" id="bn1"><div class="m1"><p>نیست بر سبزان گلشن، دیده پر خون ما</p></div>
<div class="m2"><p>تیغ خونخوار تو باشد سبز ته گلگون ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دور گردی می کند نزدیک، راه دور را</p></div>
<div class="m2"><p>ناز لیلی شد نیاز از وحشت مجنون ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قطره شبنم چه باشد کز هوا باید گرفت؟</p></div>
<div class="m2"><p>شرم دار ای شاخ گل از دیده پر خون ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما به خون خود چو داغ لاله از بس تشنه ایم</p></div>
<div class="m2"><p>خاک را رنگین نسازد کاسه وارون ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سینه بی کینه ما را گشاد دیگرست</p></div>
<div class="m2"><p>برق را سوزد نفس چون لاله در هامون ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا رسیدن، باده را با خم مدارا لازم است</p></div>
<div class="m2"><p>ورنه بیزار از تن خاکی است افلاطون ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با هوسناکان دلیر از خاک ما نتوان گذشت</p></div>
<div class="m2"><p>پوست بر تن می درد گر مرده باشد خون ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حسن او از هاله خواهد حلقه کردن نام ماه</p></div>
<div class="m2"><p>گر چنین خواهد فزود از عشق روزافزون ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پای جوهر از دم شمشیر می پیچد به هم</p></div>
<div class="m2"><p>تند مگذر زینهار از مصرع موزون ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر چه دارد بلبل ما تازه روی باغ را</p></div>
<div class="m2"><p>برگ سبزی نیست صائب زین چمن ممنون ما</p></div></div>