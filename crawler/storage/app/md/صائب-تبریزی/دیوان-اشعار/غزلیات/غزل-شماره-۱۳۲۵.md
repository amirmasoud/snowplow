---
title: >-
    غزل شمارهٔ ۱۳۲۵
---
# غزل شمارهٔ ۱۳۲۵

<div class="b" id="bn1"><div class="m1"><p>بی تو امشب هر سر مویم جدا فریاد داشت</p></div>
<div class="m2"><p>هر رگم در آستین صد نشتر فولاد داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ذوق خاموشی زبانم را به حرف آورده بود</p></div>
<div class="m2"><p>این جرس را اشتیاق پنبه بر فریاد داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من که دارم سنگ بردارد ز پیش راه من؟</p></div>
<div class="m2"><p>یار غاری کوهکن چون تیشه فولاد داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کیست تا شوید غبار از صفحه خاطر مرا؟</p></div>
<div class="m2"><p>جوی شیری پیش دست خویشتن فرهاد داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا سپند آن آتشین رخسار را در بزم دید</p></div>
<div class="m2"><p>آنچنان جست از سر آتش که صد فریاد داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یاد ایامی که صائب در حریم زلف او</p></div>
<div class="m2"><p>پنجه من اعتبار شانه شمشاد داشت</p></div></div>