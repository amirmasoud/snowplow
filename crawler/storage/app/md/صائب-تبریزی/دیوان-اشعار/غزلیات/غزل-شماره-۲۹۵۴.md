---
title: >-
    غزل شمارهٔ ۲۹۵۴
---
# غزل شمارهٔ ۲۹۵۴

<div class="b" id="bn1"><div class="m1"><p>تمنای فروغ آن ماه سیما برنمی دارد</p></div>
<div class="m2"><p>کرم بی خواست چون افتد تقاضا بر نمی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چوکار افتاد شیرین بی سخن انجام می یابد</p></div>
<div class="m2"><p>که فرهاد اهتمام کارفرما برنمی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درین وادی مرا بر رهنوردی رشک می آید</p></div>
<div class="m2"><p>که تا خاری نیارد در نظر پا بر نمی دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی کان قامت بی سایه را دیده است در جولان</p></div>
<div class="m2"><p>زسرو بوستان ناز دو بالا بر نمی دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دشمن هر که یکرنگ است دل را تیره می سازد</p></div>
<div class="m2"><p>مثال طوطیان آیینه ما برنمی دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر در پرده دل با خیال او نظر بازم</p></div>
<div class="m2"><p>وگرنه آن رخ نازک تماشا برنمی دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوارا می شود از جبهه واکرده سختیها</p></div>
<div class="m2"><p>که بار کوه جز دامان صحرا برنمی دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زسنگ کودکان شد مومیایی استخوان ما</p></div>
<div class="m2"><p>همان صائب جنون دست از سر ما برنمی دارد</p></div></div>