---
title: >-
    غزل شمارهٔ ۲۰۹۱
---
# غزل شمارهٔ ۲۰۹۱

<div class="b" id="bn1"><div class="m1"><p>صبح شکوفه چون کف سیل بهار رفت</p></div>
<div class="m2"><p>خوش موسمی ز کیسه لیل و نهار رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون می چکد ز غنچه منقار بلبلان</p></div>
<div class="m2"><p>زین نقد تازه کز گره روزگار رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آمد به موج لاله و گل بحر نوبهار</p></div>
<div class="m2"><p>مانند کف، شکوفه سبک بر کنار رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دفتر شکوفه، بجا یک ورق نماند</p></div>
<div class="m2"><p>ایام مد کشیدن ابر بهار رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گنجی که از شکوفه برون داده بود خاک</p></div>
<div class="m2"><p>در یک نفس به باد چو زر نثار رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقدی که از شکوفه چمن جمع کرده بود</p></div>
<div class="m2"><p>یکسر به هرزه خرجی باد بهار رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی سکه خرج کرد زر خویش را تمام</p></div>
<div class="m2"><p>زین بوستان شکوفه عجب نامدار رفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوران اعتدال نسیم چمن گذشت</p></div>
<div class="m2"><p>از سینه جهان، نفس بی غبار رفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناسور شد جراحت منقار بلبلان</p></div>
<div class="m2"><p>از بس که خون ناله ازو در بهار رفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خط بنفشه ری به پژمردگی گذاشت</p></div>
<div class="m2"><p>ریحان و گل به سرعت دود و شرار رفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا گشت تازیانه قوس قزح بلند</p></div>
<div class="m2"><p>چون کاروان برق، سبک لاله زار رفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قسمت چو نیست، فایده برگ عیش چیست؟</p></div>
<div class="m2"><p>نرگس پیاله داشت به کف، در خمار رفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا با گل شکفته شبی را به روز کرد</p></div>
<div class="m2"><p>خونها ز چشم شبنم شب زنده دار یافت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رو باز پس ز شور قیامت نمی کند</p></div>
<div class="m2"><p>هوشی که در رکاب نسیم بهار رفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ساقی، ترا که دست و دلی هست می بنوش</p></div>
<div class="m2"><p>کز بوی باده دست و دل من ز کار رفت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خوش وقت رهروی که درین باغ چون نسیم</p></div>
<div class="m2"><p>بی اختیار آمد و بی اختیار رفت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>واشو چو غنچه، ای گره دل به زور خود</p></div>
<div class="m2"><p>اکنون که دست عقده گشایان ز کار رفت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صائب مپرس حال دل عندلیب را</p></div>
<div class="m2"><p>جایی که لاله با جگر داغدار رفت</p></div></div>