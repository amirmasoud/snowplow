---
title: >-
    غزل شمارهٔ ۱۸۲۱
---
# غزل شمارهٔ ۱۸۲۱

<div class="b" id="bn1"><div class="m1"><p>ز دام سوختگان عشق را رهایی نیست</p></div>
<div class="m2"><p>ز لفظ، معنی بیگانه را جدایی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین زمانه چنان راه فیض مسدوست</p></div>
<div class="m2"><p>که از شکاف دل امید روشنایی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش است دردل شب دستگیری محتاج</p></div>
<div class="m2"><p>عبادتی که نهانی بود ریایی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بیقراری دراست تیغ بازی من</p></div>
<div class="m2"><p>وگرنه موج مرا میل خودنمایی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل من و تو ز همصحبتان دیرینند</p></div>
<div class="m2"><p>مرا به ظاهر اگر با تو آشنایی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز فیض بی ثمری فارغ از خزان شده ام</p></div>
<div class="m2"><p>مرا چو سرو شکایت ز بینوایی نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فغان که آبله در پرده می کند اظهار</p></div>
<div class="m2"><p>شکایتی که مرا از برهنه پایی نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خمش ز دعوی دانش، که جهل را صائب</p></div>
<div class="m2"><p>هزار حجت ناطق چو خودستایی نیست</p></div></div>