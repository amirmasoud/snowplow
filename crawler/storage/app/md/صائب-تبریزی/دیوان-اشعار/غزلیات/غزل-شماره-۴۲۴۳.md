---
title: >-
    غزل شمارهٔ ۴۲۴۳
---
# غزل شمارهٔ ۴۲۴۳

<div class="b" id="bn1"><div class="m1"><p>بیرون ز خود کسی که پی مدعا رود</p></div>
<div class="m2"><p>بر پشت بام کعبه به کسب هوا رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از محفلی که آینه رو بر قفا رود</p></div>
<div class="m2"><p>چشم و دل ندیده عشق کجا رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشق ز مومیایی تدبیر فارغ است</p></div>
<div class="m2"><p>در سوختن شکستگی از بوریا رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کس کند نماز برای قبول خلق</p></div>
<div class="m2"><p>بر پشت بام کعبه به کسب هوا رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حیرت بهم نمی خورد از نقش خوب وزشت</p></div>
<div class="m2"><p>آیینه رو گشاده بود هر کجا رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شبنم به آفتاب رسانید خویش را</p></div>
<div class="m2"><p>دلهای آب گشته ما تا کجا رود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عام است فیض صحبت دلهای پاکباز</p></div>
<div class="m2"><p>ز آیینه خانه هر که رود با صفا رود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دارد کسی که سر به ته بال خویشتن</p></div>
<div class="m2"><p>هر کجا رود به سایه بال هما رود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سختی پذیر باش که گرددسفیدروی</p></div>
<div class="m2"><p>هر دانه ای که در دهن آسیا رود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می نیست جوهری که نریزند زر بر او</p></div>
<div class="m2"><p>قارون اگر به میکده آید گدا رود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل چون ز جای رفت نیاید به جای خویش</p></div>
<div class="m2"><p>این عضو رفته نیست که دیگر به جارود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در وادیی که رو به قفا قطع ره کنند</p></div>
<div class="m2"><p>بینا کسی بود که در او با عصارود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صائب سخنوری که خیالش غریب شد</p></div>
<div class="m2"><p>زیر فلک غریب بود هر کجا رود</p></div></div>