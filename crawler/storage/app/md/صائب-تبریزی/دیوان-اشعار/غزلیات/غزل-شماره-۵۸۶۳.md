---
title: >-
    غزل شمارهٔ ۵۸۶۳
---
# غزل شمارهٔ ۵۸۶۳

<div class="b" id="bn1"><div class="m1"><p>در محفلی که تیغ زبان بر کشیده ایم</p></div>
<div class="m2"><p>در گوش تیغ حلقه جوهر کشیده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر حنظل سپهر به ساغر فشرده اند</p></div>
<div class="m2"><p>ابرو ترش نساخته بر سر کشیده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در پرده دل است شب و روز عیش ما</p></div>
<div class="m2"><p>دایم چو غنچه سر به ته پر کشیده ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاخ شکوفه ایم، سبیل است سیم ما</p></div>
<div class="m2"><p>چون نوبهار رشته ز گوهر کشیده ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از داغ لاله نامه ما دل سیه ترست</p></div>
<div class="m2"><p>چون لاله بس که باده احمر کشیده ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرگز ز پیش چشم چو مژگان نمی رود</p></div>
<div class="m2"><p>خاری که در ره تو ز پا بر کشیده ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از نقش بوریای قناعت، که سبز باد!</p></div>
<div class="m2"><p>صائب همیشه صفحه مسطر کشیده ایم</p></div></div>