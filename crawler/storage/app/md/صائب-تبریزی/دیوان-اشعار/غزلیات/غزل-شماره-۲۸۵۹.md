---
title: >-
    غزل شمارهٔ ۲۸۵۹
---
# غزل شمارهٔ ۲۸۵۹

<div class="b" id="bn1"><div class="m1"><p>دل از گفتار ناسنجیده بی آرام می گردد</p></div>
<div class="m2"><p>که شکر خواب، تلخ از مرغ بی هنگام می گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تلافی را مکافات عمل در آستین دارد</p></div>
<div class="m2"><p>دهن گوینده را تلخ اول از دشنام می گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندارد نامداری حاصلی غیر از سیه رویی</p></div>
<div class="m2"><p>عقیق از ساده لوحیها به گرد نام می گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوامی نیست رنگ آمیزی میهای لعلی را</p></div>
<div class="m2"><p>نبیند زردرویی هر که خون آشام می گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر خورشید تابان پخته می سازد ثمرها را</p></div>
<div class="m2"><p>زروی آتشین چون آرزوها خام می گردد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کند هر کس که در دولت فرامش دوستداران را</p></div>
<div class="m2"><p>زدولت کام دل نادیده، دشمنکام می گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مروت نیست خندیدن به حال ما سیه روزان</p></div>
<div class="m2"><p>زخط صبح بناگوش تو آخر شام می گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شود چون از شراب لاله گون گلگل رخ ساقی</p></div>
<div class="m2"><p>پی تسخیر دل، گیرنده چون گلدام می گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به حسن استماع از شکوه خالی می شود دلها</p></div>
<div class="m2"><p>دل مینا تهی از گوش پهن جام می گردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مه تابان کجا مستور از ابر تنک گردد؟</p></div>
<div class="m2"><p>نهان در جامه کی آن سروسیم اندام می گردد؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زعاشق دار و گیر حسن سرکش می شود افزون</p></div>
<div class="m2"><p>که بهر سرو، طوق قمریان گلدام می گردد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مگر از التفات خاص تسخیرش کنی، ورنه</p></div>
<div class="m2"><p>تسلی کی دل صائب به لطف عام می گردد؟</p></div></div>