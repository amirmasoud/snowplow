---
title: >-
    غزل شمارهٔ ۳۲۰۸
---
# غزل شمارهٔ ۳۲۰۸

<div class="b" id="bn1"><div class="m1"><p>گران گشتم به چشمش بس که رفتم بی‌طلب سویش</p></div>
<div class="m2"><p>مرا از پای نافرمان چه‌ها بر سر نمی‌آید!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه بگشاید ز ماه عید بی‌همدستی طالع؟</p></div>
<div class="m2"><p>ز صیقل کار بی‌امداد روشنگر نمی‌آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به گردون جنگ دارد چشم کوته‌بین، نمی‌داند</p></div>
<div class="m2"><p>که بی‌تحریک ساقی باده در ساغر نمی‌آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکوه عشق هیهات است مغلوب نظر گردد</p></div>
<div class="m2"><p>که کوه قاف عنقا را به زیر پر نمی‌آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به آهی خرمن افلاک را بر هم زدم صائب</p></div>
<div class="m2"><p>ز یک دل آنچه می‌آید ز صد لشکر نمی‌آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به عشق لاابالی کوه طاقت برنمی‌آید</p></div>
<div class="m2"><p>علاج شورش این بحر از لنگر نمی‌آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگر یاقوت سیرابش به داد ما رسد، ورنه</p></div>
<div class="m2"><p>علاج تشنه ما از لب ساغر نمی‌آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل گردون نمی‌سوزد به آه آتشین ما</p></div>
<div class="m2"><p>به دود تلخ، آب از دیده مجمر نمی‌آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به دشواری توان دل از لباس فقر برکندن</p></div>
<div class="m2"><p>به پای خود برون از بند نی، شکّر نمی‌آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شکوه حسن او مهری به لب زد بی‌قراران را</p></div>
<div class="m2"><p>که آواز سپند از هیچ مجمر برنمی‌آید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به داغ عشق دارد محرم و بیگانه یک نسبت</p></div>
<div class="m2"><p>ازین آتش خلیل الله سالم بر نمی‌آید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خموشی پیشه کن تا دامن مطلب به دست آری</p></div>
<div class="m2"><p>که بی‌پاس نفس از بحر گوهر برنمی‌آید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کدامین عنبرین‌مو می‌کند در سینه‌ام جولان؟</p></div>
<div class="m2"><p>که از دریای دل یک موج بی‌عنبر نمی‌آید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به منزل می‌برد قطع تعلق کاروانی را</p></div>
<div class="m2"><p>ز رهزن آنچه می‌آید ز صد رهبر نمی‌آید</p></div></div>