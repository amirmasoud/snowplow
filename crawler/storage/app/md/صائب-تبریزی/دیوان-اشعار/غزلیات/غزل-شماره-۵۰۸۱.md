---
title: >-
    غزل شمارهٔ ۵۰۸۱
---
# غزل شمارهٔ ۵۰۸۱

<div class="b" id="bn1"><div class="m1"><p>شوخی که مرا هست تمنای وصالش</p></div>
<div class="m2"><p>وحشی تر از آهوی رمیده است خیالش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم و دل من تشنه حسنی است که از لطف</p></div>
<div class="m2"><p>در آینه و آب نیفتاده مثالش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چند که گیرنده بود خون شهیدان</p></div>
<div class="m2"><p>چون لاله بود داغدل از چهره آلش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گردید مه عید مرا ناخنه چشم</p></div>
<div class="m2"><p>تا چشم من افتاد به ابروی هلالش</p></div></div>