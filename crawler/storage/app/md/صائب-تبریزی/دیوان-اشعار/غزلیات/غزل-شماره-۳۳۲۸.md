---
title: >-
    غزل شمارهٔ ۳۳۲۸
---
# غزل شمارهٔ ۳۳۲۸

<div class="b" id="bn1"><div class="m1"><p>سرو را شیوه رفتار تو از جا ببرد</p></div>
<div class="m2"><p>کبک را با همه شوخی روش از پا ببرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خانه چشم تو پرداخت مرا از دل و دین</p></div>
<div class="m2"><p>رخت را خانه ندیدیم به یغما ببرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راه باریک فنا راه گرانباران نیست</p></div>
<div class="m2"><p>سوزنی را نتوانست که عیسی ببرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکوه عفو ز گرد گنه ما بیجاست</p></div>
<div class="m2"><p>سیل جز خار چه دارد که به دریا ببرد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حیف و صد حیف که در مجمع خوبان صائب</p></div>
<div class="m2"><p>نیست امروز حریفی که دل از ما ببرد</p></div></div>