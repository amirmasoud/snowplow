---
title: >-
    غزل شمارهٔ ۳۹۲۸
---
# غزل شمارهٔ ۳۹۲۸

<div class="b" id="bn1"><div class="m1"><p>ز درد چهره محال است مرد زرد کند</p></div>
<div class="m2"><p>چه لایق است که اظهار درد مردکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز درد نیست اگر زیر تیغ آه کشم</p></div>
<div class="m2"><p>که هر کجا که فشانند آب گرد کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>علاج خصم زبردست نیست جزتسلیم</p></div>
<div class="m2"><p>به آسمان چه ضرورست کس نبرد کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شود ز رفتن روشندلان جهان غمگین</p></div>
<div class="m2"><p>که زرد روی زمین آفتاب زرد کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>توان به خون جگر سرخ داشت تا رخسار</p></div>
<div class="m2"><p>کسی چرا ز طمع روی خویش زرد کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز حرف سخت شدن رنجه فرع هشیاری است</p></div>
<div class="m2"><p>ترا که نیست شعوری سخن چه درد کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنین که ریشه دوانده است در تو بیدردی</p></div>
<div class="m2"><p>عجب عجب که ترا عشق اهل درد کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کند چو صبح کسی آفتاب را تسخیر</p></div>
<div class="m2"><p>که زندگانی خودصرف آه سرد کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شود به رنگ طلا ناقصی تمام عیار</p></div>
<div class="m2"><p>که رخ ز سیلی استاد لاجورد کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مپرس حال من ای سنگدل که هیهات است</p></div>
<div class="m2"><p>که عرض حال به بیدرد اهل درد کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طمع ز اختر دولت مدار یکرنگی</p></div>
<div class="m2"><p>که هر چه سبز کند آفتاب زرد کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نسیم فتح چو پروانه گرد آن گردد</p></div>
<div class="m2"><p>که پای همچو علم سخت درنبرد کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نفس شمرده زند هرکه چون سحر صائب</p></div>
<div class="m2"><p>کلام روشن خودرا جهان نورد کند</p></div></div>