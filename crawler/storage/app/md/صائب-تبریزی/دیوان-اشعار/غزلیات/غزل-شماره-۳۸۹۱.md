---
title: >-
    غزل شمارهٔ ۳۸۹۱
---
# غزل شمارهٔ ۳۸۹۱

<div class="b" id="bn1"><div class="m1"><p>خزان رسید وگل افشانی بهار نماند</p></div>
<div class="m2"><p>به دست بوسه فریب چمن نگار نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان غبار خط آن صفحه عذار گرفت</p></div>
<div class="m2"><p>که جای حاشیه زلف برکنار نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز خوشه چینی این چهره های گندم گون</p></div>
<div class="m2"><p>سفید را به نظر یک جو اعتبار نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز نغمه سنجی داود گوش می گیرند</p></div>
<div class="m2"><p>فغان که نغمه شناسی درین دیار نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز پیش آتش خویش چگونه بگریزم</p></div>
<div class="m2"><p>مرا که قوت پرواز یک شرار نماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خموشیم اثر شکر نیست چون صائب</p></div>
<div class="m2"><p>دماغ شکوه ام از اهل روزگار نماند</p></div></div>