---
title: >-
    غزل شمارهٔ ۲۸۸۳
---
# غزل شمارهٔ ۲۸۸۳

<div class="b" id="bn1"><div class="m1"><p>چمن‌پیرا نه گل را دسته در گلزار می‌بندد</p></div>
<div class="m2"><p>که گل در روزگار حسن او زنار می‌بندد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو عشق بی‌تکلف دست بردار از خودآرایی</p></div>
<div class="m2"><p>که بتوان زیج بستن عقل تا دستار می‌بندد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو کز سر طریقت غافلی از شرع در مگذر</p></div>
<div class="m2"><p>که بر عارف شود احرام اگر زنار می‌بندد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نبیند داغ غربت وقت رحلت عاقبت‌بینی</p></div>
<div class="m2"><p>که پیش از مرگ چشم از عالم غدار می‌بندد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز عاجزنالی ما مهربان شد چرخ سنگین‌دل</p></div>
<div class="m2"><p>گیاه ما زبان برق بی‌زنهار می‌بندد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خزان را غنچه این بوستان در آستین دارد</p></div>
<div class="m2"><p>چمن‌پیرا ز غفلت رخنه دیوار می‌بندد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دردش می‌رسد دانای اسرار نهان صائب</p></div>
<div class="m2"><p>ز عرض حال خود هرکس لب اظهار می‌بندد</p></div></div>