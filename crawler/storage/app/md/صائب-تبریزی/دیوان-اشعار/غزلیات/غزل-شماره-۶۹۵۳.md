---
title: >-
    غزل شمارهٔ ۶۹۵۳
---
# غزل شمارهٔ ۶۹۵۳

<div class="b" id="bn1"><div class="m1"><p>خون می چکد از تیغ نگاهی که تو داری</p></div>
<div class="m2"><p>فریاد ازان چشم سیاهی که تو داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حمله اول ز جهان گرد برآرد</p></div>
<div class="m2"><p>از خال و خط و زلف، سپاهی که تو داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چند گلی نیست به خوش چشمی نرگس</p></div>
<div class="m2"><p>در خواب ندیده است نگاهی که تو داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر در دهن تیغ درآیی ظفر از توست</p></div>
<div class="m2"><p>از دست دعا پشت و پناهی که تو داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهر تو محال است جهانگیر نگردد</p></div>
<div class="m2"><p>از سبزه خط مهر گیاهی که تو داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آهو نتواند ز سر تیر تو جستن</p></div>
<div class="m2"><p>دل چون جهد از تیر نگاهی که تو داری؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برقی است که ابرش ز سیه خانه لیلی است</p></div>
<div class="m2"><p>در زلف سیه روی چو ماهی که تو داری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با خودشکنی، داعیه سرکشی و ناز</p></div>
<div class="m2"><p>می بارد ازان طرف کلاهی که تو داری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب کمی از گلشن فردوس ندارد</p></div>
<div class="m2"><p>در عالم معنی سر راهی که تو داری</p></div></div>