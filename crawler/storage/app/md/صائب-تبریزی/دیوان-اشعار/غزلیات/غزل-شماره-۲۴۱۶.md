---
title: >-
    غزل شمارهٔ ۲۴۱۶
---
# غزل شمارهٔ ۲۴۱۶

<div class="b" id="bn1"><div class="m1"><p>بر من از روشندلی وضع جهان هموار شد</p></div>
<div class="m2"><p>خار در پیراهن آتش گل بی خار شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خودبخود چون غنچه واشد عقده ها از کار من</p></div>
<div class="m2"><p>تا درین بستانسرا دست و دلم از کار شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دور گردان را وصال پرده داران هم خوش است</p></div>
<div class="m2"><p>طوطی ما از ادب یکرنگ با زنگار شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر شود مرکز، بسوزد شهپر پرگار را</p></div>
<div class="m2"><p>نقطه بی طالع من بس که بی پرگار شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که را بیماری چشم تو بر بستر فکند</p></div>
<div class="m2"><p>هر پرستاری که آمد بر سرش بیمار شد!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ننگ قیمت بر ندارد گوهر روشندلان</p></div>
<div class="m2"><p>ای خوش آن گوهر که آب از گرمی بازار شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مستی غفلت میان دیده و دل شد حجاب</p></div>
<div class="m2"><p>پرده خوابم نقاب دولت بیدار شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در شبستان فنا صبح امیدی می شود</p></div>
<div class="m2"><p>هر نفس کز زندگانی صرف استغفار شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وحشت ارباب بینش را فزاید رنگ و بو</p></div>
<div class="m2"><p>شبنم از نزدیکی گل آتشین رفتار شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عالم پرشور بر روشن ضمیران دوزخ است</p></div>
<div class="m2"><p>در بهشت افتاد تا آیینه ما تار شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شبنم گستاخ گردد حلقه بیرون در</p></div>
<div class="m2"><p>هر کجا مژگان من خار سر دیوار شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیش ازین صائب نمی باشد عبادت را اثر</p></div>
<div class="m2"><p>رفته رفته رشته تسبیح من زنار شد</p></div></div>