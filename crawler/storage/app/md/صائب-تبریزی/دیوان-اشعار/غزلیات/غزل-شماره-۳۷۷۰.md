---
title: >-
    غزل شمارهٔ ۳۷۷۰
---
# غزل شمارهٔ ۳۷۷۰

<div class="b" id="bn1"><div class="m1"><p>به اهل عشق نصیحت چه می تواند کرد؟</p></div>
<div class="m2"><p>نمک به شور قیامت چه می تواند کرد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به آفتاب جهانسوز اوج یکتایی</p></div>
<div class="m2"><p>هجوم شبنم کثرت چه می تواند کرد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمی شود دل روشن سیه ز گرد گناه</p></div>
<div class="m2"><p>به آب حیوان ظلمت چه می تواند کرد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزار پیرهن از گرد خاکسار ترم</p></div>
<div class="m2"><p>به من غبار مذلت چه می تواند کرد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درازدستی حرص و فراخ گامی سعی</p></div>
<div class="m2"><p>به تنگ گیری قسمت چه می تواند کرد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>متاع خوب به هر جا رود عزیز بود</p></div>
<div class="m2"><p>به ماه کنعان غربت چه می تواند کرد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز سخت جانی من سنگ پا به کوه نهاد</p></div>
<div class="m2"><p>به من زبان ملامت چه می تواند کرد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز یار شکوه بیجا چه می کنی صائب؟</p></div>
<div class="m2"><p>به آن غرور شکایت چه می تواند کرد؟</p></div></div>