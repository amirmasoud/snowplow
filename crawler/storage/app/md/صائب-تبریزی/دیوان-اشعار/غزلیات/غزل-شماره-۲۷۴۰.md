---
title: >-
    غزل شمارهٔ ۲۷۴۰
---
# غزل شمارهٔ ۲۷۴۰

<div class="b" id="bn1"><div class="m1"><p>عارفان را نکهت سیب ذقن جان می‌دهد</p></div>
<div class="m2"><p>طفل مشرب جان برای نارپستان می‌دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با سبک‌روحان به نقد دل گرانی چون کنم؟</p></div>
<div class="m2"><p>شمع در راه نسیم صبحدم جان می‌دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر به دنبال جنون عشق نه، کاین باد دست</p></div>
<div class="m2"><p>وسعت خاطر بیابان در بیابان می‌دهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل ز فکر پوچ خواهد باخت خود را چون حباب</p></div>
<div class="m2"><p>کشتی ما را سبکباری به طوفان می‌دهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش دریا آبروی خود چرا ریزد سد؟</p></div>
<div class="m2"><p>قطره‌ای دارد گدایی، ابر نیسان می‌دهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سهل باشد بند کردن ناخنی بر بیستون</p></div>
<div class="m2"><p>پیش برق تیشه من کوه میدان می‌دهد</p></div></div>