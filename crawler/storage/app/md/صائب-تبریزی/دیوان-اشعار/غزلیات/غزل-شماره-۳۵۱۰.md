---
title: >-
    غزل شمارهٔ ۳۵۱۰
---
# غزل شمارهٔ ۳۵۱۰

<div class="b" id="bn1"><div class="m1"><p>نه همین اهل خرد آینه اسرارند</p></div>
<div class="m2"><p>که ز خود بیخبران نیز خبرها دارند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقطه هایی که درین دایره فرد آمده اند</p></div>
<div class="m2"><p>همه حیرت زده گردش این پرگارند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی گره شو که به یک چشم زدن می گذرند</p></div>
<div class="m2"><p>رشته ها از نظر سوزن، اگر هموارند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به ادب باش درین بزم که این پست و بلند</p></div>
<div class="m2"><p>همه در کار، پی رونق موسیقارند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قانعانی که فشردند به دل دندان را</p></div>
<div class="m2"><p>خبر از چاشنی میوه جنت دارند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنچه از مایده فیض بر این نه طبق است</p></div>
<div class="m2"><p>رزق جمعی است که در پرده شب بیدارند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سالکانی که دل روشن از اینجا بردند</p></div>
<div class="m2"><p>در ته خاک چو خورشید همان سیارند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می رسد زود به معراج فنا دست بدست</p></div>
<div class="m2"><p>هرکه را خانه برانداختگان معمارند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیش جمعی که رسیدند ز ساحل به محیط</p></div>
<div class="m2"><p>کوهها کبک صفت جمله سبکرفتارند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست ممکن که تراوش کند از ما سخنی</p></div>
<div class="m2"><p>در نهانخانه ما آینه ها ستارند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خاکساری نه بنایی است که ویران گردد</p></div>
<div class="m2"><p>سیلها عاجز کوتاهی این دیوارند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سر درین معرکه بیقدرتر از دستارست</p></div>
<div class="m2"><p>این رفیقان سبکسر به غم دستارند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خوبرویان که ندارند رگ تندی خوی</p></div>
<div class="m2"><p>مفت طفلان هوس همچو گل بی خارند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خودفروشان جهان راست غم رد و قبول</p></div>
<div class="m2"><p>عارفان فارغ از اقرار و غم انکارند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>من گرفتم چمن آرا ز چمن بیرون رفت</p></div>
<div class="m2"><p>سر بسر شبنم این باغ اولوالابصارند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صائب آنان که درین عهد سخن خرج کنند</p></div>
<div class="m2"><p>دانه سوخته در شوره زمین می کارند</p></div></div>