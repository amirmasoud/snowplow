---
title: >-
    غزل شمارهٔ ۳۳۹۲
---
# غزل شمارهٔ ۳۳۹۲

<div class="b" id="bn1"><div class="m1"><p>زهر را صبر جوانمرد شکر می سازد</p></div>
<div class="m2"><p>خار را نخل برومند ثمر می سازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر ما گرد سر دار فنا می گردد</p></div>
<div class="m2"><p>میوه چون پخته شود برگ سفر می سازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو نداری سر آمیزش عاشق، ورنه</p></div>
<div class="m2"><p>با لب خشک صدف آب گهر می سازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناوک غمزه ز الماس ترازو شده است</p></div>
<div class="m2"><p>طاقت ساده دل از موم سپر می سازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون برم پی به سر خویش، که نیرنگ قضا</p></div>
<div class="m2"><p>هر گل صبح، مرا رنگ دگر می سازد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر سبکسیر درین دشت کمندی دارد</p></div>
<div class="m2"><p>دل بیتاب مرا موی کمر می سازد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب از پرتو خورشید تواند گل چید</p></div>
<div class="m2"><p>هر که چون شبنم گل، دیده سپر می سازد</p></div></div>