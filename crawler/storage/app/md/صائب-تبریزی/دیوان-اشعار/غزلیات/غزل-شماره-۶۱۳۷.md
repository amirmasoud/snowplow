---
title: >-
    غزل شمارهٔ ۶۱۳۷
---
# غزل شمارهٔ ۶۱۳۷

<div class="b" id="bn1"><div class="m1"><p>شمع را شب تیغ روشن از نیام آید برون</p></div>
<div class="m2"><p>از سیاهی اختر پروانه شام آید برون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسن کامل می شود در پرده شرم و حیا</p></div>
<div class="m2"><p>از ته این ابر ماه نو تمام آید برون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سبزه می آید به دشواری برون از زیر سنگ</p></div>
<div class="m2"><p>خط به تمکین زان لب یاقوت فام آید برون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می شود از آفتاب تند محشر خامسوز</p></div>
<div class="m2"><p>از تنور خاک، نان هر که خام آید برون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از رهایی بیشتر باشد ز زندانش خطر</p></div>
<div class="m2"><p>از تن خاکی چو جانی ناتمام آید برون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیدن پنهان او نگذاشت در من زندگی</p></div>
<div class="m2"><p>آه ازان روزی که این تیغ از نیام آید برون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نزل خاصان است صائب حرف شورانگیز عشق</p></div>
<div class="m2"><p>از دو صد طوطی یکی شیرین کلام آید برون</p></div></div>