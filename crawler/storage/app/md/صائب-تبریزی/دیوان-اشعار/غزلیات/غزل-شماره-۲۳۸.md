---
title: >-
    غزل شمارهٔ ۲۳۸
---
# غزل شمارهٔ ۲۳۸

<div class="b" id="bn1"><div class="m1"><p>یک نفس گر دور سازی از کنار آیینه را</p></div>
<div class="m2"><p>می کند بی تابی دل سنگسار آیینه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا خط سبز تو آمد در کنار آیینه را</p></div>
<div class="m2"><p>می رود آب خضر در جویبار آیینه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر شکستی تا ز روی ناز دامان نقاب</p></div>
<div class="m2"><p>آب شد دل از گداز انتظار آیینه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا به حسن هرزه گرد او شود جایی دچار</p></div>
<div class="m2"><p>نیست چون آب روان یک جا قرار آیینه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می کند زنجیر جوهر پاره چون دیوانگان</p></div>
<div class="m2"><p>بس که دارد شوق رویت بی قرار آیینه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جوی خونی از رگ هر جوهرش وا کرده است</p></div>
<div class="m2"><p>با همه رویین تنی، مژگان یار آیینه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در تماشاگاه حسن دین و دل پرداز او</p></div>
<div class="m2"><p>آه می خیزد ز دل بی اختیار آیینه را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق بی تاب است، ورنه طوطی گستاخ ما</p></div>
<div class="m2"><p>همچو موم سبز دارد در کنار آیینه را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سینه صافان را ز چشم بد حصاری لازم است</p></div>
<div class="m2"><p>چشم زخم روست، پشت زرنگار آیینه را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رفته رفته حسن پرکار ترا تسخیر کرد</p></div>
<div class="m2"><p>ساده لوحی عاقبت آمد به کار آیینه را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دیده روشن ضمیران جلوه گاه عبرت است</p></div>
<div class="m2"><p>هیچ نقشی نیست در دل پایدار آیینه را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در تماشای جمال خویش بی تاب است حسن</p></div>
<div class="m2"><p>می گذارد گل ز شبنم در کنار آیینه را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بی تکلف بر سر بالینش آید آفتاب</p></div>
<div class="m2"><p>هر که سازد همچو شبنم بی غبار آیینه را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اهل صورت از نزاکت های معنی غافلند</p></div>
<div class="m2"><p>ره مده در خلوت خود زینهار آیینه را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با دل نازک ملایم ساز خلق خویش را</p></div>
<div class="m2"><p>بیشتر از موم می باشد حصار آیینه را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در نزاکت خانه دل ها نفس را پاس دار</p></div>
<div class="m2"><p>تیره می سازد دم سردی هزار آیینه را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چشم حیران مرا مژگان نمی پوشد به هم</p></div>
<div class="m2"><p>بخیه جوهر نمی آید به کار آیینه را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خاطر روشندلان بسیار صائب نازک است</p></div>
<div class="m2"><p>می توان کردن به آهی زنگبار آیینه را</p></div></div>