---
title: >-
    غزل شمارهٔ ۲۱۲
---
# غزل شمارهٔ ۲۱۲

<div class="b" id="bn1"><div class="m1"><p>در شکایت ریختی دندان نعمت خواره را</p></div>
<div class="m2"><p>کهنه کردی در ورق گردانی این سی پاره را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوهر دل شد عیان از گرم و سرد روزگار</p></div>
<div class="m2"><p>آب و آتش ذوالفقاری کرد این انگاره را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اهل دل را گفتگوی عشق آب زندگی است</p></div>
<div class="m2"><p>نیست نقلی به ز اخگر مرغ آتشخواره را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل نهاد درد تا بودم، فراغت داشتم</p></div>
<div class="m2"><p>چاره جویی کرد سرگردان من بیچاره را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من که در صحرای خودکامی سراسر می روم</p></div>
<div class="m2"><p>چون توانم جمع کردن این دل صد پاره را؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشرت روی زمین بسته است در آرام دل</p></div>
<div class="m2"><p>خواب طفلان لنگر تمکین بود گهواره را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر دل خود زنده خواهی خاکساری پیشه کن</p></div>
<div class="m2"><p>به ز خاکستر لباسی نیست آتشپاره را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گوشه چشمی اگر صائب به حال من کنند</p></div>
<div class="m2"><p>سرمه می سازم ز برق تیشه سنگ خاره را</p></div></div>