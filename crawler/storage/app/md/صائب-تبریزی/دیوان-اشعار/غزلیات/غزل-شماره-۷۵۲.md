---
title: >-
    غزل شمارهٔ ۷۵۲
---
# غزل شمارهٔ ۷۵۲

<div class="b" id="bn1"><div class="m1"><p>شد بی صفا ز خاک سیه کاسه آب ما</p></div>
<div class="m2"><p>آخر به رنگ ظرف برآمد شراب ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از اشک تلخ ما کف خاکی نگشت سبز</p></div>
<div class="m2"><p>نگرفت دست هیچ سبویی شراب ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما با خیال روی تو در خواب رفته ایم</p></div>
<div class="m2"><p>یوسف نقاب بسته درآید به خواب ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در قلزمی که موج بود تیغ آبدار</p></div>
<div class="m2"><p>از سرگذشت خویش چه گوید حباب ما؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا چند زیر خرقه قدح را نهان کنیم؟</p></div>
<div class="m2"><p>در پشت کوه چند بود آفتاب ما؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما را اگر چه دست تصرف نداده اند</p></div>
<div class="m2"><p>گیراتر از کمند بود پیچ و تاب ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما گل به جای صید به فتراک بسته ایم</p></div>
<div class="m2"><p>بلبل نفس گسسته رود در رکاب ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز خط یار بر قلم ما نمی رود</p></div>
<div class="m2"><p>داروی بیهشی است غبار کتاب ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زنهار خنده بر دل مجروح ما مکن</p></div>
<div class="m2"><p>خونابه می کند نمکت را کباب ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در کام شعله، دم به شمار اوفتاده است</p></div>
<div class="m2"><p>پر می زند هنوز ز خامی کباب ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای شور حشر، از جگر ما بدار دست</p></div>
<div class="m2"><p>خونابه می کند نمکت را کباب ما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای خم ز پرده پوشی ما در گذر که تاک</p></div>
<div class="m2"><p>زنجیر پاره کرد ز زور شراب ما</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صائب اگر چه بال و پر ما شکسته است</p></div>
<div class="m2"><p>سیمرغ را به چشم نیارد عقاب ما</p></div></div>