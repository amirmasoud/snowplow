---
title: >-
    غزل شمارهٔ ۲۵۰۶
---
# غزل شمارهٔ ۲۵۰۶

<div class="b" id="bn1"><div class="m1"><p>چون ز تاب می رخت بر لاله پهلو می‌زند</p></div>
<div class="m2"><p>غنچه در پیش گل روی تو زانو می‌زند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون شود از عارضش آب طراوت موج زن</p></div>
<div class="m2"><p>از خجالت دست خود آیینه بر رو می‌زند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از شبیخون خزان سنگش به مینا می‌خورد</p></div>
<div class="m2"><p>باغ کز بادام خواب چارپهلو می‌زند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خلاف وعده ابرویت سرآمد گشته است</p></div>
<div class="m2"><p>در کجی‌ها این ترازو راستی مو می‌زند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کیست تا همچشم یار شوخ چشم ما شود؟</p></div>
<div class="m2"><p>هر نگاهش بر صف صد دشت آهو می‌زند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پاک‌طینت از حدیث سرد از جا کی رود؟</p></div>
<div class="m2"><p>آب گوهر از صبا کی چین بر ابرو می‌زند؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب از بس سینه را از مهر صیقل داده‌ایم</p></div>
<div class="m2"><p>پیش ما صافی‌دلان آیینه زانو می‌زند</p></div></div>