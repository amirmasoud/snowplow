---
title: >-
    غزل شمارهٔ ۶۶۰۸
---
# غزل شمارهٔ ۶۶۰۸

<div class="b" id="bn1"><div class="m1"><p>ز خط عذار تو تا عنبرین نقاب شده</p></div>
<div class="m2"><p>ز هاله خوبی مه پای در رکاب شده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خطی که روی بتان را برآورد ز حجاب</p></div>
<div class="m2"><p>مه عذار ترا پرده حجاب شده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز زلف چون به خط افتاد کار خوشدل باش</p></div>
<div class="m2"><p>که خط سبز دعایی است مستجاب شده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نچیده است گل از روی دولت بیدار</p></div>
<div class="m2"><p>کسی که غافل ازان چشم نیمخواب شده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیاض گردن او را چه نسبت است با صبح</p></div>
<div class="m2"><p>که از قلمرو ایجاد انتخاب شده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تمام عمر نیاید به هم ز خنده لبش</p></div>
<div class="m2"><p>پیاله ای که ز لعل تو کامیاب شده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی هزار کند شور عندلیبان را</p></div>
<div class="m2"><p>چنین که عارض او گلگل از شراب شده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به حسن عاقبت قطره ای است رشک مرا</p></div>
<div class="m2"><p>که همچو شبنم گل خرج آفتاب شده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکسته است به دریا کلاه گوشه فخر</p></div>
<div class="m2"><p>سری که پر ز هوای تو چون حباب شده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گلاب پیرهن آفتاب گردیده است</p></div>
<div class="m2"><p>درین ریاض چو شبنم دلی که آب شده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حریف نخوت نودولتان نمی گردید</p></div>
<div class="m2"><p>حذر کنید ز خونی که مشک ناب شده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مگر مرا ز خجالت برآورد در حشر</p></div>
<div class="m2"><p>ز زندگانی من آنچه صرف خواب شده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فکنده است ز هم دور آشنایان را</p></div>
<div class="m2"><p>تکلفی که درین روزگار باب شده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زمین قلمرو سیلاب حادثات بود</p></div>
<div class="m2"><p>مکن بنای عمارت درین خراب شده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مریز رنگ اقامت درین تماشاگاه</p></div>
<div class="m2"><p>که گل ز گرمروی های خود گلاب شده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به غیر بلبل آتش نوای من صائب</p></div>
<div class="m2"><p>دگر که از نفس گرم خود کباب شده؟</p></div></div>