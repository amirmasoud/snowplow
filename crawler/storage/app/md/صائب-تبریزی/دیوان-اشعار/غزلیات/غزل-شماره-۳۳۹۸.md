---
title: >-
    غزل شمارهٔ ۳۳۹۸
---
# غزل شمارهٔ ۳۳۹۸

<div class="b" id="bn1"><div class="m1"><p>عشق در سینه خس و خار تمنا سوزد</p></div>
<div class="m2"><p>آرزو را به رگ و ریشه دلها سوزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل بیدار ازین گوشه نشینان مطلب</p></div>
<div class="m2"><p>کاین چراغی است که در خلوت عنقا سوزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون سیاووش زآتش به سلامت گذرد</p></div>
<div class="m2"><p>هر که امروز در اندیشه فردا سوزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل چراغی است که روشن شود از باد سحر</p></div>
<div class="m2"><p>لاله شمعی است که در دامن صحرا سوزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جلوه ساحل اگر سلسله جنبان گردد</p></div>
<div class="m2"><p>کشتی از گرمروی در دل دریا سوزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آتشین چون شود از می گل رخسار ترا</p></div>
<div class="m2"><p>در شبستان تو پروانه دو بالا سوزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در جگر آه مرا سردی دوران نگذاشت</p></div>
<div class="m2"><p>نکند دود درختی که ز سرما سوزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کشش عشق ز معشوق نمی دارد دست</p></div>
<div class="m2"><p>شمع بر تربت پروانه دو بالا سوزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آتش از صحبت همدرد گلستان گردد</p></div>
<div class="m2"><p>جای رحم است بر آن شمع که تنها سوزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هست در شرع محبت کسی امروز تمام</p></div>
<div class="m2"><p>که ز احباب دلش بیش ز اعدا سوزد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب ایمن شود از وحشت تاریکی قبر</p></div>
<div class="m2"><p>هرکه با دیده گریان دل شبها سوزد</p></div></div>