---
title: >-
    غزل شمارهٔ ۱۹۴۹
---
# غزل شمارهٔ ۱۹۴۹

<div class="b" id="bn1"><div class="m1"><p>جامی ز خون بی غش منصورم آرزوست</p></div>
<div class="m2"><p>لعل تجلی از کمر طورم آرزوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر من جهان ز دیده مورست تنگتر</p></div>
<div class="m2"><p>یک چند تنگنا دل مورم آرزوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با طاقتی که پنجه ازو برده است موم</p></div>
<div class="m2"><p>رفتن به سیر انجمن طورم آرزوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساغر حریف عقل گرانجان نمی شود</p></div>
<div class="m2"><p>رطلی گرانتر از سر مخمورم آرزوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مردم ز اشتیاق شکر خواب نیستی</p></div>
<div class="m2"><p>بالین دار، چون سر منصورم آرزوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شیرینی حیات دلم را گزیده است</p></div>
<div class="m2"><p>عریان شدن به خانه زنبورم آرزوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نازک شدم چنان که گمان می برند خلق</p></div>
<div class="m2"><p>در بر کشیدن کمر مورم آرزوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب درین زمان که رسیده است مشق فکر</p></div>
<div class="m2"><p>هم نغمگی به شاعر مشهورم آرزوست</p></div></div>