---
title: >-
    غزل شمارهٔ ۴۳۴۸
---
# غزل شمارهٔ ۴۳۴۸

<div class="b" id="bn1"><div class="m1"><p>پروای خط آن غنچه مستور ندارد</p></div>
<div class="m2"><p>شکرخبر از قافله مور ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گردید نهان درخط سبز آن لب میگون</p></div>
<div class="m2"><p>این شیشه خطر از می پرزور ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیمارگران را نبود تاب عیادت</p></div>
<div class="m2"><p>تاب نظر آن نرگس مخمور ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر چند شکسته است سفالین قدح ما</p></div>
<div class="m2"><p>آوازه ما کاسه فغفور ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون محضر بی مهر بود باد به دستش</p></div>
<div class="m2"><p>از داغ تو هر کس دل معمور ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آوازه محال است ز یک دست برآید</p></div>
<div class="m2"><p>رحم است برآن پنجه که همزور ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در شعله بود نور به اندازه روغن</p></div>
<div class="m2"><p>هر دیده که بی اشک بود نور ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صائب همه بی نمکان بر سر شورند</p></div>
<div class="m2"><p>امروز که دیوانه ما شور ندارد</p></div></div>