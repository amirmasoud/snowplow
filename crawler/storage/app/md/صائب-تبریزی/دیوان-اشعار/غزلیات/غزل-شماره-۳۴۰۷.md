---
title: >-
    غزل شمارهٔ ۳۴۰۷
---
# غزل شمارهٔ ۳۴۰۷

<div class="b" id="bn1"><div class="m1"><p>می توان گرد خط از آینه حسن زدود</p></div>
<div class="m2"><p>از گهر گرد یتیمی اگر آسان خیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لعل چون لاله به دور لب رنگین سخنت</p></div>
<div class="m2"><p>داغدار از جگر کان بدخشان خیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی بر تافتن از ما ز مروت دورست</p></div>
<div class="m2"><p>غیر تسلیم چه از دیده حیران خیزد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرد پاپوش به ویرانه ما افشاند</p></div>
<div class="m2"><p>هر کجا سیلی ازین کوه و بیابان خیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرگ عیدست ز افلاس به تنگ آمده را</p></div>
<div class="m2"><p>همچو آن خفته که از خواب پریشان خیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پای در دامن تسلیم و رضاکش صائب</p></div>
<div class="m2"><p>تا ترا نکهت یوسف ز گریبان خیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خط سبزی که ز پشت لب جانان خیزد</p></div>
<div class="m2"><p>رگ ابری است که از چشمه حیوان خیزد</p></div></div>