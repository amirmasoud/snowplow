---
title: >-
    غزل شمارهٔ ۶۱۵۱
---
# غزل شمارهٔ ۶۱۵۱

<div class="b" id="bn1"><div class="m1"><p>گر بنالم خون ز چشم سنگ می‌آید برون</p></div>
<div class="m2"><p>ور بگریم خار و گل یکرنگ می‌آید برون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر طرف دیوانه خوش طالع من می‌رود</p></div>
<div class="m2"><p>کودکی با دامن پر سنگ می‌آید برون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فارغ است از خودنمایی جوهر اقبال ما</p></div>
<div class="m2"><p>تیغ ما از زخم‌ها بی‌رنگ می‌آید برون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عمرها باید که آن یاقوت لب نوخط شود</p></div>
<div class="m2"><p>سبزه با تمکین ز زیر سنگ می‌آید برون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زخم پیکان می‌شود در سینه دلگیر من</p></div>
<div class="m2"><p>گل ز باغم غنچه دلتنگ می‌آید برون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طوطیان را دل چو مغز پسته خون خواهد شدن</p></div>
<div class="m2"><p>گر به این تمکین شکر از تنگ می‌آید برون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکه چون آیینه لوح سینه خود صاف کرد</p></div>
<div class="m2"><p>ساده از دنیای پر نیرنگ می‌آید برون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک گل بی‌رنگ دارد عالم پر رنگ و بو</p></div>
<div class="m2"><p>کز لطافت هر زمان صد رنگ می‌آید برون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیستون را در فلاخن می‌گذارد تیشه‌ام</p></div>
<div class="m2"><p>کوهکن با من کجا همسنگ می‌آید برون؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر چه در هر حمله‌ای می‌افکند صد سر به خاک</p></div>
<div class="m2"><p>دست خالی نیزه‌ام از جنگ می‌آید برون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر به این دستور خواهد باده من جوش زد</p></div>
<div class="m2"><p>از طلسم چرخ مینا رنگ می‌آید برون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از دل ما حرص را دست تصرف کوته است</p></div>
<div class="m2"><p>این سبو خشک از می گلرنگ می‌آید برون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نیست چون فرهاد اگر در جذب عاشق کوتهی</p></div>
<div class="m2"><p>نقش شیرین بی‌حجاب از سنگ می‌آید برون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از ریاضت هرکه را بر پشت می‌چسبد شکم</p></div>
<div class="m2"><p>ناله‌اش چون چنگ، سیر آهنگ می‌آید برون</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صبح پیری از دلم زنگار غفلت را نبرد</p></div>
<div class="m2"><p>دیگر این آیینه کی از زنگ می‌آید برون؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ما درین گلزار صائب مرغ آتشخواره‌ایم</p></div>
<div class="m2"><p>دانه ما چون شرار از سنگ می‌آید برون</p></div></div>