---
title: >-
    غزل شمارهٔ ۲۲۱۹
---
# غزل شمارهٔ ۲۲۱۹

<div class="b" id="bn1"><div class="m1"><p>لطف و قهر زمانه هر دو یکی است</p></div>
<div class="m2"><p>گره دام و دانه هر دو یکی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پشت و رو نیست کار دنیا را</p></div>
<div class="m2"><p>شب و روز زمانه هر دو یکی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده خوابناک غفلت را</p></div>
<div class="m2"><p>صور حشر و فسانه هر دو یکی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خنده برق در سبکسیری</p></div>
<div class="m2"><p>با نشاط زمانه هر دو یکی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاکساران بی تعین را</p></div>
<div class="m2"><p>مسند و آستانه هر دو یکی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقد باشد قیامت عاشق</p></div>
<div class="m2"><p>پیش ما گور و خانه هر دو یکی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست از هم جدا دل و دلدار</p></div>
<div class="m2"><p>چون دو لب، این دو دانه هر دو یکی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جلوه آب خضر در ظلمات</p></div>
<div class="m2"><p>با شراب شبانه هر دو یکی است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خانه با یار خانگی زیباست</p></div>
<div class="m2"><p>ورنه زندان و خانه هر دو یکی است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما که از بال خویش در قفسیم</p></div>
<div class="m2"><p>بیضه و آشیانه هر دو یکی است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نسبت کشتی شکسته ما</p></div>
<div class="m2"><p>با کنار و میانه هر دو یکی است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صائب این آن غزل که تنها گفت</p></div>
<div class="m2"><p>بد و نیک زمانه هر دو یکی است</p></div></div>