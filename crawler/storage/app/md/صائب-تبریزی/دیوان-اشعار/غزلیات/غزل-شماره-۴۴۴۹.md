---
title: >-
    غزل شمارهٔ ۴۴۴۹
---
# غزل شمارهٔ ۴۴۴۹

<div class="b" id="bn1"><div class="m1"><p>صحبت به حریفان سیه کار مدارید</p></div>
<div class="m2"><p>بر روی سخن آینه تار مدارید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ظاهر نشود در دل نادان اثر حرف</p></div>
<div class="m2"><p>در پیش نفس آینه تار مدارید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون خامه قدم جفت نمایید درین راه</p></div>
<div class="m2"><p>در سیر وسفر عادت پرگار مدارید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خون می چکد ازغنچه لب بسته درین باغ</p></div>
<div class="m2"><p>کاری به سراپرده اسرار مدارید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شیرازه اوراق دل آن موی میان است</p></div>
<div class="m2"><p>زنهار که دست از کمر یار مدارید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون شمع اگر سوز شما عاریتی نیست</p></div>
<div class="m2"><p>پروای دم سرد خریدار مدارید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر آینه جان شما ساده ز نقش است</p></div>
<div class="m2"><p>اندیشه گردوغم زنگار مدارید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون سایه سبکسیر بود دولت دنیا</p></div>
<div class="m2"><p>با سایه اقبال هماکار مدارید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با تاج زر از گریه نیاسود دمی شمع</p></div>
<div class="m2"><p>راحت طمع از دولت بیدار مدارید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مفتاح نهانخانه دل قفل خموشی است</p></div>
<div class="m2"><p>اوقات خود آشفته به گفتار مدارید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سیلاب حواس است نظر های پریشان</p></div>
<div class="m2"><p>آیینه خود بر سر بازار مدارید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بازیچه امواج بود کشتی خالی</p></div>
<div class="m2"><p>دل را ز غم و درد سبکبار مدارید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر سرو تهیدست خزان دست ندارد</p></div>
<div class="m2"><p>از بی ثمری بر دل خود بار مدارید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در گوشه چشم است نهان فتنه دوران</p></div>
<div class="m2"><p>با گوشه نشینان جهان کار مدارید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کوهی که بلندست نگردد کم ازو برف</p></div>
<div class="m2"><p>با همت عالی غم دستار مدارید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر هست هوای گل بی خار شما را</p></div>
<div class="m2"><p>خاری که درین راه بود خوار مدارید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون صائب اگر موی شکافید درین بزم</p></div>
<div class="m2"><p>دست از کمر رشته زنار مدارید</p></div></div>