---
title: >-
    غزل شمارهٔ ۵۳۹۲
---
# غزل شمارهٔ ۵۳۹۲

<div class="b" id="bn1"><div class="m1"><p>شمع فانوسم که در پرده است اشک افشاندنم</p></div>
<div class="m2"><p>از گرستن تر نگردد دامن پیراهنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست شمعی در سرای من، ولی از سوز دل</p></div>
<div class="m2"><p>می درخشد همچو چشم شیر شبها روزنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دشمنان را می کنم از چرب نرمی سازگار</p></div>
<div class="m2"><p>خار می گردد گل بی خار در پیراهنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خون رحم آید به جوش از چشم شرم الود من</p></div>
<div class="m2"><p>دست خالی می رود گلچین برون از گلشنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا گسستم رشته پیوند از زال جهان</p></div>
<div class="m2"><p>سر برآورد از گریبان مسیحا سوزنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بعد ایامی که گلها از سفر باز آمدند</p></div>
<div class="m2"><p>چون نسیم صبحدم می باید از خود رفتنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شوق اگر صائب چنین گردد گریبانگیرمن</p></div>
<div class="m2"><p>می کند کوتاه دست خار را از دامنم</p></div></div>