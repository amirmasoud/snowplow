---
title: >-
    غزل شمارهٔ ۳۵۱۲
---
# غزل شمارهٔ ۳۵۱۲

<div class="b" id="bn1"><div class="m1"><p>همه از تاب کمر در خم ایمان دارند</p></div>
<div class="m2"><p>چه خرام است که این سرو نژادان دارند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون به نیرنگ دل از موی شکافان نبرند؟</p></div>
<div class="m2"><p>صد زبان در دهن این غنچه دهانان دارند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شعله ای هست ز خونگرمی باطن همه را</p></div>
<div class="m2"><p>همچو فانوس چراغی ته دامان دارند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوسه شان چاشنی عمر ابد می بخشد</p></div>
<div class="m2"><p>آب حیوان همه در چاه زنخدان دارند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خرمن کهنه گل چند توان داد به باد؟</p></div>
<div class="m2"><p>خرمن آن است که این مور میانان دارند</p></div></div>