---
title: >-
    غزل شمارهٔ ۴۱۷۲
---
# غزل شمارهٔ ۴۱۷۲

<div class="b" id="bn1"><div class="m1"><p>عاشق کجا به کعبه و دیر التجا کند</p></div>
<div class="m2"><p>حاشا که خضر پیروی نقش پا کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی ناز خشک می کشد از موجه سراب</p></div>
<div class="m2"><p>لب تشنه ای که ناز به آب بقا کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زنجیر بندخانه تقدیر محکم است</p></div>
<div class="m2"><p>چون مور از میان کمر حرص وا کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی جذبه مشکل است برون آمدن ز خویش</p></div>
<div class="m2"><p>این کاه را ز دانه جداکهربا کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تخمی که سوخت ناز بهاران نمی کشد</p></div>
<div class="m2"><p>بخل فلک چه با دل بی مدعا کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناف غزال کاسه دریوزه می شود</p></div>
<div class="m2"><p>چون زلف مشکبار ترا شانه وا کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این خط سبز کز لب لعل تو سرزده است</p></div>
<div class="m2"><p>عالم سیه به دیده آب بقا کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طوطی ز وصل آینه شیرین کلام شد</p></div>
<div class="m2"><p>گر برخورد به آینه رویی چها کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسته است صف ز سرو وصنوبر حریم باغ</p></div>
<div class="m2"><p>تا اقتدا به قامت آن دلربا کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پوشیده چشم می گذرد از عزیز مصر</p></div>
<div class="m2"><p>آیینه ای که چشم به روی تو وا کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با آه عاشقان چه بود خرده نجوم</p></div>
<div class="m2"><p>مشکل به خرج باد زر گل وفا کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خالی نگردد از گل بی خار دامنش</p></div>
<div class="m2"><p>خاری اگر وطن به مقام رضا کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>افتاده است تا ره صائب به خانقاه</p></div>
<div class="m2"><p>وقت است خاک میکده را توتیا کند</p></div></div>