---
title: >-
    غزل شمارهٔ ۱۲۰۲
---
# غزل شمارهٔ ۱۲۰۲

<div class="b" id="bn1"><div class="m1"><p>هر دلی کز زلف جانان سر برآرد کشتنی است</p></div>
<div class="m2"><p>از حرم صیدی که پا بیرون گذارد کشتنی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قطره از دریا چرا دارد سر خود را دریغ؟</p></div>
<div class="m2"><p>زیر تیغ یار هر کس سر بخارد کشتنی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صاحب اقبالی که پای خود به وقت اقتدار</p></div>
<div class="m2"><p>بر گلوی دشمن عاجز فشارد کشتنی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روزگار بیغمی را، هر که از ارباب درد</p></div>
<div class="m2"><p>از حساب زندگانی بر شمارد کشتنی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که باری از دل مردم تواند بر گرفت</p></div>
<div class="m2"><p>دست خود بر روی یکدیگر گذارد کشتنی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طاعت خالص بود از خودنمایی بی نیاز</p></div>
<div class="m2"><p>آشکارا هر که این ره را سپارد کشتنی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر سبکدستی که در فصل بهار زندگی</p></div>
<div class="m2"><p>تخم نیکی در دل مردم نکارد کشتنی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که بعد از عفو کردن، آشکارا و نهان</p></div>
<div class="m2"><p>جرم دشمن را به روی دشمن آرد کشتنی است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حد هر کس چون حرم صائب حصار جان اوست</p></div>
<div class="m2"><p>هر که پا از حد خود بیرون گذارد کشتنی است</p></div></div>