---
title: >-
    غزل شمارهٔ ۴۲۴۷
---
# غزل شمارهٔ ۴۲۴۷

<div class="b" id="bn1"><div class="m1"><p>زاهد به کعبه با سر و دستار می‌رود</p></div>
<div class="m2"><p>این مست بین که روی به دیوار می‌رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان شاخ گل شکیب من زار می‌رود</p></div>
<div class="m2"><p>زین دست و تازیانه دل از کار می‌رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آسوده‌اند مرده‌دلان از سؤال حشر</p></div>
<div class="m2"><p>این اعتراض با دل بیدار می‌رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منصور سر گذاشت درین راه، برنگشت</p></div>
<div class="m2"><p>زاهد درین غم است که دستار می‌رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کاهش وجود به جان سعی می‌کند</p></div>
<div class="m2"><p>چون خامه هرکه از پی گفتار می‌رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کاری به ذوق بوسه‌ربایی نمی‌رسد</p></div>
<div class="m2"><p>دل‌های شب نسیم به گلزار می‌رود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کار خوشی است شغل محبت ولی چه سود</p></div>
<div class="m2"><p>کز حسن کار دست و دل از کار می‌رود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ترسانده است چشم ترا و هم بی‌جگر</p></div>
<div class="m2"><p>ورنه برهنه گل به سر خار می‌رود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روشنگر وجود بود آرمیدگی</p></div>
<div class="m2"><p>آیینه است آب چو هموار می‌رود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این آن غزل که مولوی روم گفته است</p></div>
<div class="m2"><p>«این نفس ناطقه پی گفتار می‌رود»</p></div></div>