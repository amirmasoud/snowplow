---
title: >-
    غزل شمارهٔ ۶۸۰
---
# غزل شمارهٔ ۶۸۰

<div class="b" id="bn1"><div class="m1"><p>روی تو غنچه ساخت گل آفتاب را</p></div>
<div class="m2"><p>در هم شکست کوکبه ماهتاب را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوری مکن ز صحبت نیکان که می کند</p></div>
<div class="m2"><p>گوهر عزیز در نظر خلق آب را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پروای رستخیز ندارند راستان</p></div>
<div class="m2"><p>روز حساب، عید بود خودحساب را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی عشق خون مرده بود دل به زیر پوست</p></div>
<div class="m2"><p>از آتش است گریه خونین کباب را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روشندلان ز تیغ زبانند بی نیاز</p></div>
<div class="m2"><p>حاجت به شمع نیست شب ماهتاب را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صورت پرست فیض ز معنی نمی برد</p></div>
<div class="m2"><p>بلبل به جای گل نپرستد گلاب را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>معنی شود ز نازکی لفظ، دلپذیر</p></div>
<div class="m2"><p>در شیشه است جلوه دیگر شراب را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر کس که از خسیس کند مردمی طمع</p></div>
<div class="m2"><p>دارد توقع از گل کاغذ گلاب را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب ز مد عمر اقامت طمع مدار</p></div>
<div class="m2"><p>آرام نیست جلوه موج سراب را</p></div></div>