---
title: >-
    غزل شمارهٔ ۵۸۸۸
---
# غزل شمارهٔ ۵۸۸۸

<div class="b" id="bn1"><div class="m1"><p>خیزید تا ز عالم صورت سفر کنیم</p></div>
<div class="m2"><p>تا روشن است راه خرابات سر کنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند نیست قافله در کار شوق را</p></div>
<div class="m2"><p>هویی کشیم و همسفران را خبر کنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نقش پای گرمروان پیش راه ما</p></div>
<div class="m2"><p>دارد چراغ، این ره تاریک سر کنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون مور در هوای شکر پر برآوریم</p></div>
<div class="m2"><p>بر هم زنیم بال و ز گردون گذر کنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شبرنگ روزگار اگر توسنی کند</p></div>
<div class="m2"><p>رامش به تازیانه آه سحر کنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیرون زنیم خیمه ز دارالغرور مصر</p></div>
<div class="m2"><p>چون بوی پیرهن سوی کنعان سفر کنیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از دودمان شعله بگیریم همتی</p></div>
<div class="m2"><p>پرواز تا به اوج فنا چون شرر کنیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر چند رهروان سخن و راه گفته اند</p></div>
<div class="m2"><p>ما راه طی کنیم و سخن مختصر کنیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کسوت ز آفتاب بگیریم چون مسیح</p></div>
<div class="m2"><p>از خرقه کبود فلک سر بدر کنیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باد مراد زود نفس گیر می شود</p></div>
<div class="m2"><p>دامن گره به دامن موج خطر کنیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یا همچو موج بر لب ساحل شویم محو</p></div>
<div class="m2"><p>یا چون حباب سر ز دل بحر بر کنیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا می توان به عالم معنی سفر نمود</p></div>
<div class="m2"><p>صائب چرا به عالم صورت سفر کنیم؟</p></div></div>