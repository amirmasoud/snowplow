---
title: >-
    غزل شمارهٔ ۳۳۵۳
---
# غزل شمارهٔ ۳۳۵۳

<div class="b" id="bn1"><div class="m1"><p>رخنه هایی که مرا در جگر آن مژگان کرد</p></div>
<div class="m2"><p>زرهی نیست که بتوان به قبا پنهان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این طراوت که گل روی ترا داده خدا</p></div>
<div class="m2"><p>می تواند نفس سوخته را ریحان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوی خورشید به خون دست ز آسایش شست</p></div>
<div class="m2"><p>حسن روزی که سر زلف ترا چوگان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرمه خامشی طوطی گویا گردید</p></div>
<div class="m2"><p>بس که نظاره او آینه را حیران کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تخم امید من از سعی فلک سبز نشد</p></div>
<div class="m2"><p>دانه سوخته خون در جگر دهقان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچ جا زیر فلک قابل آرام نبود</p></div>
<div class="m2"><p>این صدف گوهر محبوس مرا غلطان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غوطه در خون شفق زد ز ندامت صائب</p></div>
<div class="m2"><p>هر که چون صبح لبی زیر فلک خندان کرد</p></div></div>