---
title: >-
    غزل شمارهٔ ۵۱۳۱
---
# غزل شمارهٔ ۵۱۳۱

<div class="b" id="bn1"><div class="m1"><p>ز سیر باغ نگردد دل پریشان جمع</p></div>
<div class="m2"><p>که خویش را نکند آب در گلستان جمع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرابه غنچه درین باغ رشک می آید</p></div>
<div class="m2"><p>که بهر پاره شدن می کند گریبان جمع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کمند طول امل درکشاکش است مدام</p></div>
<div class="m2"><p>ز صید دل نشود طره پریشان جمع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به روشنایی فهم از چراغ قانع شو</p></div>
<div class="m2"><p>که این دوشمع نگردد به یک شبستان جمع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا که بحر گهر ازکنار می گذرد</p></div>
<div class="m2"><p>چرا کنم چو صدف آب چشم نیسان جمع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مجو بلندی اگر رحمت آرزو داری</p></div>
<div class="m2"><p>که می شود به زمینهای پست باران جمع</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تمام شب ز برای ذخیره فردا</p></div>
<div class="m2"><p>کنم ز کوچه وبازار ،سنگ طفلان جمع</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو گل شکفت محال است غنچه گردد باز</p></div>
<div class="m2"><p>به هیچ حیله نگردد دل پریشان جمع</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز موج حادثه مردان نمی روند از جا</p></div>
<div class="m2"><p>که زیر تیغ کند کوه پابه دامان جمع</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کجا ز سیر پریشان ما خبر داری ؟</p></div>
<div class="m2"><p>ترا که هست دل آهنین چوپیکان جمع</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بلاست دایره خلق چون وسیع افتاد</p></div>
<div class="m2"><p>که دام و دد همه باشند دربیابان جمع</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به آفتاب جهانتاب می رسد صائب</p></div>
<div class="m2"><p>چو شبنم آن که کند دل درین گلستان جمع</p></div></div>