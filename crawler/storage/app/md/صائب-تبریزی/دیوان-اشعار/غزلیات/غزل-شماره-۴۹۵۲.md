---
title: >-
    غزل شمارهٔ ۴۹۵۲
---
# غزل شمارهٔ ۴۹۵۲

<div class="b" id="bn1"><div class="m1"><p>رگ ابری است آن لبهای نوخط، بوسه بارانش</p></div>
<div class="m2"><p>که عمر جاودان بخشد به عاشق مد احسانش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرانگشت سهیل از زخم دندان جوی خون گردد</p></div>
<div class="m2"><p>ز می گر این چنین رنگین شود سیب زنخدانش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کشد در هر قدم جای قدح مینای می برسر</p></div>
<div class="m2"><p>زمین از جلوه مستانه سرو خرامانش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هر گلشن که آن سرو خرامان جلوه گر گردد</p></div>
<div class="m2"><p>نمی آید بهم تا حشر آغوش خیابانش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زبان العطش گویی است هر گردی کز او خیزد</p></div>
<div class="m2"><p>به خون عاشقان تشنه است از بس خاک میدانش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه بال و پر گشاید در دل چون چشم مور من؟</p></div>
<div class="m2"><p>پریزادی که باشد چون قفس ملک سلیمانش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به آزادان کسی را می رسد پیوند چون قمری</p></div>
<div class="m2"><p>که باشد حلقه فتراک ازطوق گریبانش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کجا آن نوش لب دارد غم اهل سخن صائب؟</p></div>
<div class="m2"><p>که از خود می کند ایجاد طوطی شکرستانش</p></div></div>