---
title: >-
    غزل شمارهٔ ۵۰۳۵
---
# غزل شمارهٔ ۵۰۳۵

<div class="b" id="bn1"><div class="m1"><p>فارغ ز دار و گیر جهان خراب باش</p></div>
<div class="m2"><p>مردانه ترک کام بگو،کامیاب باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این شعله های عاریتی نیست پایدار</p></div>
<div class="m2"><p>چون لاله زآتش جگر خود کباب باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خود را چو آفتاب نکردی به نور عشق</p></div>
<div class="m2"><p>باری چو سایه در قدم آفتاب باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قدر تو کم چرا بود از قدر دیگران؟</p></div>
<div class="m2"><p>از خود زیاده از همه کس در حجاب باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمت اگر به دولت بیدار می پرد</p></div>
<div class="m2"><p>ازشورش درون، نمک چشم خواب باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواهی که بی حساب به جنت ترا برند</p></div>
<div class="m2"><p>صائب نفس شمرده زن و خود حساب باش</p></div></div>