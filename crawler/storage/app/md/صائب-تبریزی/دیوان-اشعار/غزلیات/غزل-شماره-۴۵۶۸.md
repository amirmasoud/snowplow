---
title: >-
    غزل شمارهٔ ۴۵۶۸
---
# غزل شمارهٔ ۴۵۶۸

<div class="b" id="bn1"><div class="m1"><p>آرزو در دل بسوزان، عود در مجمر گذار</p></div>
<div class="m2"><p>خاک بر لب مال، لب را بر لب کوثر گذار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قطره خود را درین دریا چو گوهر ساختی</p></div>
<div class="m2"><p>دست خود را چون صدف بر روی یکدیگر گذار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا رسد وقتی که سازی تختگاه از تاج زر</p></div>
<div class="m2"><p>سر به زانوی صدف یک چند چون گوهر گذار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در سرای مردم بی برگ چون مهمان شوی</p></div>
<div class="m2"><p>مهر بر لب زن، فضولی را برون در گذار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می شود جان تازه از آمیزش سیمین بران</p></div>
<div class="m2"><p>سینه تفسیده را بر سینه خنجر گذار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می توانی حرف حق بر دار اگر بی پرده گفت</p></div>
<div class="m2"><p>پای چون منصور بر بالای این منبر گذار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بیابان طلب گر سر نخواهی باختن</p></div>
<div class="m2"><p>از نشان پای خود مهری براین محضر گذار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گوهر دل را چو آوردی سلامت بر کنار</p></div>
<div class="m2"><p>کشتن تن را به این دریای بی لنگر گذار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از دم آتش فشان، آیینه تاریک خود</p></div>
<div class="m2"><p>گر نسازی آب، باری پیش روشنگر گذار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مزد طاعات ریایی دیدن خلق است و بس</p></div>
<div class="m2"><p>پشت بر محراب کن، پا بر سر منبر گذار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا نپیچد آسمانها گردن از فرمان تو</p></div>
<div class="m2"><p>پا مکش از راه حق، بر خط فرمان سرگذار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وصل آتش طلعتان چون برق باشد در گذر</p></div>
<div class="m2"><p>تیزدستی کن سپند خود درین مجمر گذار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شکوه و شکر از شکست و بست، کوته دیدگی است</p></div>
<div class="m2"><p>شیشه را با شیشه گر، دل را به آن دلبر گذار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جنگ دارد دولت و آسودگی با یکدگر</p></div>
<div class="m2"><p>بر نمی آیی به دردسر، ز سر افسر گذار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از می جان بخش زنگ تیرگی از دل بشوی</p></div>
<div class="m2"><p>آرزوی آب حیوان را به اسکندر گذار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آب گوهر ترجمان حالت گوهر بس است</p></div>
<div class="m2"><p>عرض حال خویش را صائب به چشم تر گذار</p></div></div>