---
title: >-
    غزل شمارهٔ ۶۳۸۸
---
# غزل شمارهٔ ۶۳۸۸

<div class="b" id="bn1"><div class="m1"><p>بارست خنده بر دل کلفت پرست من</p></div>
<div class="m2"><p>پر خون بود دهان گل از پشت دست من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مینا زبان مار شود در شکستگی</p></div>
<div class="m2"><p>رحم است بر کسی که بود در شکست من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قمری بود ز حلقه به گوشان سرو و من</p></div>
<div class="m2"><p>آن قمریم که سرو بود پای بست من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گیرنده تر ز دست شده است آستین من</p></div>
<div class="m2"><p>اکنون که رفته دامن فرصت ز دست من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در بزم وصل از من بی دل اثر مجو</p></div>
<div class="m2"><p>کز خود تمام برده مرا نیم مست من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا خط عنبرین نکند نامه اش سیاه</p></div>
<div class="m2"><p>ایمان نیاورد به خدا خودپرست من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آتش به زیر پاست چو شبنم مرا ز گل</p></div>
<div class="m2"><p>شوید اگر چه گرد ز دلها نشست من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک بار تیر من به غلط بر هدف نخورد</p></div>
<div class="m2"><p>با آن که می برد کجی از تیر، شست من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر نخل سرکشی که درین سبز طارم است</p></div>
<div class="m2"><p>از زور می چو تاک بود زیردست من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیگر غبار دامن هیچ آشنا نشد</p></div>
<div class="m2"><p>تا آشنا به دامن شب گشت دست من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون موج در خم خس و خاشاک نیستم</p></div>
<div class="m2"><p>صائب نهنگ می کشد از بحر شست من</p></div></div>