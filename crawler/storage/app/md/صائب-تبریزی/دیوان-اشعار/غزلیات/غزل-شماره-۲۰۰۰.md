---
title: >-
    غزل شمارهٔ ۲۰۰۰
---
# غزل شمارهٔ ۲۰۰۰

<div class="b" id="bn1"><div class="m1"><p>هر غنچه زین چمن دل در خون نشانده ای است</p></div>
<div class="m2"><p>هر شاخ نرگسی نظر بازمانده ای است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر شاخ گل که فصل خزان جلوه می کند</p></div>
<div class="m2"><p>از رنگ و بوی عاریه، دست فشانده ای است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از عقل درگذر، که چراغی است بی فروغ</p></div>
<div class="m2"><p>دست از جنون بدار، که نخل فشانده ای است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دور تیغ غمزه او نقطه زمین</p></div>
<div class="m2"><p>چون داغ لاله، دیده در خون نشانده ای است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مجنون که بود قافله سالار وحشیان</p></div>
<div class="m2"><p>در عهد ما پیاده دنبال مانده ای است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صائب به دور عارض عالم فروز او</p></div>
<div class="m2"><p>از لاله دم مزن، که چراغ نشانده ای است</p></div></div>