---
title: >-
    غزل شمارهٔ ۵۸۸۷
---
# غزل شمارهٔ ۵۸۸۷

<div class="b" id="bn1"><div class="m1"><p>چون صبح خنده با جگر چاک می زنیم</p></div>
<div class="m2"><p>در موج خیز خون نفس پاک می زنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر جا که موج حادثه ابرو بلند کرد</p></div>
<div class="m2"><p>ما چون حباب پیرهنی چاک می زنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همت به هیچ مرتبه راضی نمی شود</p></div>
<div class="m2"><p>در دام، فال حلقه فتراک می زنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در سردسیر خاک که یک روی گرم نیست</p></div>
<div class="m2"><p>جوشی به زور شعله ادراک می زنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون کاروان ریگ به منزل نمی رسیم</p></div>
<div class="m2"><p>چندان که قطره بر ورق خاک می زنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناخن حریف آبله دل نمی شود</p></div>
<div class="m2"><p>بر قلب شیشه خانه افلاک می زنیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب کدام غبن به این می رسد که ما</p></div>
<div class="m2"><p>داریم می به ساغر و تریاک می زنیم</p></div></div>