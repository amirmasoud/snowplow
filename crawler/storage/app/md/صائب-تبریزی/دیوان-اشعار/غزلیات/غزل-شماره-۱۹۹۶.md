---
title: >-
    غزل شمارهٔ ۱۹۹۶
---
# غزل شمارهٔ ۱۹۹۶

<div class="b" id="bn1"><div class="m1"><p>ابر بهار گلشن رخسار، آینه است</p></div>
<div class="m2"><p>آتش فروز شعله دیدار، آینه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دل توان به انجمن حسن راه برد</p></div>
<div class="m2"><p>سنگ نشان کعبه دیدار آینه است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنجا توان به زور نفس کار پیش برد</p></div>
<div class="m2"><p>افسانه ای است این که دل یار آینه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نتوان به کنه چرخ رسیدن به سعی فکر</p></div>
<div class="m2"><p>اندیشه مور و این در و دیوار آینه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با روی یار چهره شدن نیست کار گل</p></div>
<div class="m2"><p>دارد کسی که جوهر این کار، آینه است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر دل بجاست، وضع جهان آرمیده است</p></div>
<div class="m2"><p>گر چشم روشن است، گل و خار آینه است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشق چو محو گشت، دو عالم دو عینک است</p></div>
<div class="m2"><p>طوطی چو مست شد، در و دیوار آینه است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>امروز دیده ای که نرفته است آب ازو</p></div>
<div class="m2"><p>صائب درین زمانه غدار آینه است</p></div></div>