---
title: >-
    غزل شمارهٔ ۵۸۶
---
# غزل شمارهٔ ۵۸۶

<div class="b" id="bn1"><div class="m1"><p>کجا نظر به گل و یاسمن بود ما را؟</p></div>
<div class="m2"><p>که خارخار، گل پیرهن بود ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به ماه مصر ز یک پیرهن مضایقه کرد</p></div>
<div class="m2"><p>چه چشمداشت دگر از وطن بود ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خون نشست عقیق از فروغ عاریتی</p></div>
<div class="m2"><p>چه دلخوشی ز سهیل یمن بود ما را؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ببین چه ساده دل افتاده ایم با این بخت</p></div>
<div class="m2"><p>که چشم آب ز چاه ذقن بود ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز نوبهار قناعت به داغ حسرت کرد</p></div>
<div class="m2"><p>حسد به لاله خونین کفن بود ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به هر لباس که خواهیم جلوه گر گردیم</p></div>
<div class="m2"><p>نه ایم سرو که یک پیرهن بود ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرا کنیم تنزل به آسمان صائب؟</p></div>
<div class="m2"><p>چنین که خامه، سوار سخن بود ما را</p></div></div>