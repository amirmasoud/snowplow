---
title: >-
    غزل شمارهٔ ۲۴۴۸
---
# غزل شمارهٔ ۲۴۴۸

<div class="b" id="bn1"><div class="m1"><p>کی سر از تیغ شهادت جان روشن می کشد؟</p></div>
<div class="m2"><p>شمع در راه نسیم صبح گردن می کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست مانع حسن را مستوری از خون ریختن</p></div>
<div class="m2"><p>گل به خون بلبلان در غنچه دامن می کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یار را از اوج استغنا فرود آورد عجز</p></div>
<div class="m2"><p>از گریبان مسیح این خار سوزن می کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیدلی ما را گریزان دارد از تیغ اجل</p></div>
<div class="m2"><p>ورنه خار از انتظار برق گردن می کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از رفیقان گرانجان تا چها خواهد کشید</p></div>
<div class="m2"><p>رهروی کز سایه خود کوه آهن می کشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق اگر آشفته گردید از دل پرداغ ماست</p></div>
<div class="m2"><p>نور خورشید این پریشانی زروزن می کشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منعمان را حرص روزی گرچنین خواهد فزود</p></div>
<div class="m2"><p>مور را گر دانه ای باشد به خرمن می کشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست غیر از آه غمخواری دل تنگ مرا</p></div>
<div class="m2"><p>رشته گاهی آستین بر چشم سوزن می کشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حسن از آیینه های پاک نتواند گذشت</p></div>
<div class="m2"><p>چشم حیران ماه را در دام روزن می کشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست غافل آفتاب از حال دورافتادگان</p></div>
<div class="m2"><p>شبنم افتاده را بیرون زگلشن می کشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا به فکر حسن عالمسوز گل افتاده است</p></div>
<div class="m2"><p>صائب از هر خار ناز نخل ایمن می کشد</p></div></div>