---
title: >-
    غزل شمارهٔ ۴۷۰۳
---
# غزل شمارهٔ ۴۷۰۳

<div class="b" id="bn1"><div class="m1"><p>بهار می گذرد ساغر چو لاله بگیر</p></div>
<div class="m2"><p>هزار بوسه ز کنج لب پیاله بگیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز نشأه پر طاوسی ار نداری رنگ</p></div>
<div class="m2"><p>به طاق ابروی قوس قزح پیاله بگیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهار عمر سبکتر ز برق می گذرد</p></div>
<div class="m2"><p>چو لاله کام دل از باده دو ساله بگیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنوش باده گلرنگ و رو به بستان کن</p></div>
<div class="m2"><p>هزار نکته رنگین به برگ لاله بگیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مزاج ساغر گل نازک است ای بلبل</p></div>
<div class="m2"><p>خدای را پر خود پیش سنگ ژاله بگیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نصیحتی است ز پیر مغان به یاد مرا</p></div>
<div class="m2"><p>غمی فرو چو بگیرد ترا پیاله بگیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به عمر خضر چه خمیازه می کشی صائب؟</p></div>
<div class="m2"><p>تو نیز کام دل از باده دو ساله بگیر</p></div></div>