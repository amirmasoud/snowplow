---
title: >-
    غزل شمارهٔ ۱۱۰۴
---
# غزل شمارهٔ ۱۱۰۴

<div class="b" id="bn1"><div class="m1"><p>زلف گرد عارض او رشته گلدسته است</p></div>
<div class="m2"><p>کز لب و رخ غنچه و گل را به هم پیوسته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوی عالمسوز او بی زینهار افتاده است</p></div>
<div class="m2"><p>ورنه از آتش سپند ما مکرر جسته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سبزه خوابیده باشد با قد رعنای او</p></div>
<div class="m2"><p>سرو اگر در پیش قمری مصرع برجسته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سالها شد پشت بر دیوار حیرت داده ایم</p></div>
<div class="m2"><p>دیده آیینه را نقشی چنین ننشسته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلبلان در بیضه با گل زیر یک پیراهنند</p></div>
<div class="m2"><p>غم ز دوری نیست چون دلها به هم پیوسته است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در لباس تلخ دارد جا ز بیم چشم شور</p></div>
<div class="m2"><p>ورنه طوطی در شکر پنهان چو مغز پسته است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون در آیینه، روی سخت این آهن دلان</p></div>
<div class="m2"><p>می نماید باز در ظاهر، ولیکن بسته است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگسلد چون موج صائب رشته امید ما</p></div>
<div class="m2"><p>جویبار ما به دریای کرم پیوسته است</p></div></div>