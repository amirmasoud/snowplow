---
title: >-
    غزل شمارهٔ ۴۳۱
---
# غزل شمارهٔ ۴۳۱

<div class="b" id="bn1"><div class="m1"><p>به هر تردامنی منمای آن آیینه رو را</p></div>
<div class="m2"><p>مبادا زنگ خجلت سبز سازد حرف بدگو را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترا صدبار اگر بینم، همان مشتاق دیدارم</p></div>
<div class="m2"><p>تهی چشمی به گوهر کم نمی گردد ترازو را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگارین می شود از خون دلها دست سیمینش</p></div>
<div class="m2"><p>دهد پرداز اگر با دست، زلف عنبرین بو را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به این شوقی که من رو در گلستان تو آوردم</p></div>
<div class="m2"><p>نگه دارد خدا از بوسه گرمم لب جو را!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز رشک شانه در تابم که با کوتاه دستی ها</p></div>
<div class="m2"><p>به صد آغوش در بر می کشد آن عنبرین مو را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عزایم خوان اگر خود را بسوزد جای آن دارد</p></div>
<div class="m2"><p>که از یک شیشه می تسخیر کردم صد پریرو را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شراب چشم لیلی بدخمار ظالمی دارد</p></div>
<div class="m2"><p>ازان پیوسته مجنون در نظر می داشت آهو را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همان در پیش چشمش گرد خجلت بر جبین دارد</p></div>
<div class="m2"><p>اگر در سرمه خوابانند صد شب چشم آهو را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز صائب پرس احوال غزال وحشی معنی</p></div>
<div class="m2"><p>که مجنون خوب می داند زبان چشم آهو را</p></div></div>