---
title: >-
    غزل شمارهٔ ۳۰۸۸
---
# غزل شمارهٔ ۳۰۸۸

<div class="b" id="bn1"><div class="m1"><p>دل آزاد طبعان فارغ از قید هوس باشد</p></div>
<div class="m2"><p>قبای بی گریبان را چه پروای عسس باشد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حصار خرمن خود ساز دست خوشه چینان را</p></div>
<div class="m2"><p>که اینجا جامه فتح شکربال مگس باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مه محمل نشینی را که من دیوانه اویم</p></div>
<div class="m2"><p>به گوشش ناله زنجیر فریاد جرس باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندارد چشم تحسین ناله بلبل زبیدردان</p></div>
<div class="m2"><p>چه حاجت شعله آواز را با خار و خس باشد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخن سنجیده گفتن بی نیازی بار می آرد</p></div>
<div class="m2"><p>گهر در دامن غواص از پاس نفس باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حبابی را که در بحر حقیقت چشم بگشاید</p></div>
<div class="m2"><p>سپهر آبگون چون پرده روی نفس باشد</p></div></div>