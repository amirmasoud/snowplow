---
title: >-
    غزل شمارهٔ ۶۷۵۲
---
# غزل شمارهٔ ۶۷۵۲

<div class="b" id="bn1"><div class="m1"><p>گر اندک نیکیی از دستت آید در نظر داری</p></div>
<div class="m2"><p>بت خود می کنی سنگی اگر از راه برداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو روزی نیست افزون عمر ایام برومندی</p></div>
<div class="m2"><p>مشو غافل ز حال تلخکامان تا ثمر داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز ریزش کشتی اسباب خود را کن گران لنگر</p></div>
<div class="m2"><p>درین دریا اگر اندیشه از موج خطر داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به ظاهر گرچه بالین کرده ای چون خم ز خشت، اما</p></div>
<div class="m2"><p>هزاران فتنه خوابیده پنهان زیر سر داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز عیب پیش پا افتاده خود نیستی واقف</p></div>
<div class="m2"><p>که چون طاوس از غفلت نظر بر بال و پر داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز طوق بندگی راه نفس شد تنگ بر قمری</p></div>
<div class="m2"><p>تو چون سرو از رعونت دست خود را بر کمر داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به بیداری سرآور روزگار زندگانی را</p></div>
<div class="m2"><p>به زیر خاک اگر خواب فراغت در نظر داری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل مردم به ظاهر می بری از نوشخند، اما</p></div>
<div class="m2"><p>چو گل از خار چندین نیشتر زیر سپر داری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز آب زندگی ظلمت بود رزقت چو اسکندر</p></div>
<div class="m2"><p>ز خودبینی تو تا آیینه در پیش نظر داری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه ای یک مشت گل افزون و از اندیشه روزی</p></div>
<div class="m2"><p>دل پررخنه ای چون سبحه از صد رهگذر داری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به جان بی نفس جان می توان بردن ازین وادی</p></div>
<div class="m2"><p>نه ای از پاکبازان ناله ای تا در جگر داری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز سیر سرسری چون موج خار و خس به دست آید</p></div>
<div class="m2"><p>ز سر پاکن چو غواصان اگر میل گهرداری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مدار از دامن دل دست اگر از کعبه جویانی</p></div>
<div class="m2"><p>که در دنبال چندین رهزن و یک راهبر داری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مبر با خود به زیر خاک این مار سیه صائب</p></div>
<div class="m2"><p>همین جا نامه خود را بشو تا چشم تر داری</p></div></div>