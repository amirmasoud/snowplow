---
title: >-
    غزل شمارهٔ ۴۰۸۹
---
# غزل شمارهٔ ۴۰۸۹

<div class="b" id="bn1"><div class="m1"><p>از شعر بهره ای به سخنور نمی رسد</p></div>
<div class="m2"><p>از بوی عود فیض به مجمرن می رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلبر چنان خوش است که دل را کند کباب</p></div>
<div class="m2"><p>آتش به داد عشق سمندرن می رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا شمع در سرای حضور تو محرم است</p></div>
<div class="m2"><p>از غیب روشنایی دیگ نمی رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حسن از نیازمندی عشاق فارغ است</p></div>
<div class="m2"><p>تلخی ز عیش مور به شکر نمی رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جمعیت حواس بود مال اهل فقر</p></div>
<div class="m2"><p>این منزلت به هیچ توانگر نمی رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صائب وصال خضر به بخت است واتفاق</p></div>
<div class="m2"><p>آواره هر که گشت به رهبر نمی رسد</p></div></div>