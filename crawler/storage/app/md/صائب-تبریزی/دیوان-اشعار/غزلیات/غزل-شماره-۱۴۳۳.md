---
title: >-
    غزل شمارهٔ ۱۴۳۳
---
# غزل شمارهٔ ۱۴۳۳

<div class="b" id="bn1"><div class="m1"><p>دل بی صبر به طوفان بلا رهبر ماست</p></div>
<div class="m2"><p>بال موج خطر از کشتی بی لنگر ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوسه آن لب میگون و لب ما، هیهات</p></div>
<div class="m2"><p>این می لعل، زیاد از دهن ساغر ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشرت روی زمین، قالب بی جانی ازوست</p></div>
<div class="m2"><p>از سر کوی تو خشتی که به زیر سر ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راه عشق است که از سر بودش سنگ نشان</p></div>
<div class="m2"><p>هر که سر در سر این کار کند رهبر ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو اوراق خزان هر ورقش در جایی است</p></div>
<div class="m2"><p>گر به ظاهر دل صد پاره ما در بر ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل ما از نفس سوختگان تازه شود</p></div>
<div class="m2"><p>هر کجا هست جگر سوخته ای عنبر ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نور خورشید در آیینه ما مستورست</p></div>
<div class="m2"><p>جای رحم است بر آن دیده که روشنگر ماست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشم ما پردگی از سرمه حیرت شده است</p></div>
<div class="m2"><p>ورنه آن آینه رو در ته خاکستر ماست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر دلی را سخن ما نپذیرد صائب</p></div>
<div class="m2"><p>سینه پاک دهانان، صدف گوهر ماست</p></div></div>