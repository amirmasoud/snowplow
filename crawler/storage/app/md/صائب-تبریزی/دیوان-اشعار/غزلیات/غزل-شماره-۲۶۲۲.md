---
title: >-
    غزل شمارهٔ ۲۶۲۲
---
# غزل شمارهٔ ۲۶۲۲

<div class="b" id="bn1"><div class="m1"><p>ریزش اشک ندامت غافلان را بس بود</p></div>
<div class="m2"><p>مشت آبی لشکر خواب گران را بس بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می شود پشت کمان از آتش سوزنده نرم</p></div>
<div class="m2"><p>آه گرمی روی سخت آسمان را بس بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زود می پاشد زهم در پیری اوراق حواس</p></div>
<div class="m2"><p>آه سردی ریزش برگ خزان را بس بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما سیه روزان به اندک روی گرمی قانعیم</p></div>
<div class="m2"><p>کرم شب تابی چراغ این دودمان را بس بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون هوا مغلوب شد، در دست خاتم گو مباش</p></div>
<div class="m2"><p>باد در فرمان سلیمان زمان را بس بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کار تیغ از دست آید چون قوی افتاد دل</p></div>
<div class="m2"><p>پنجه مردانگی شیر ژیان را بس بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حسن سرکش را دعای جوشنی چون عشق نیست</p></div>
<div class="m2"><p>طوق قمری دیده بان سروروان را بس بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هست بی زحمت مهیا آنچه می باید ترا</p></div>
<div class="m2"><p>مهر خاموشی سپر تیغ زبان را بس بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیل بی رهبر به دریا می رساند خویش را</p></div>
<div class="m2"><p>جذبه منزل دلیل این کاروان را بس بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می توان بردن زسیما ره به کنه هر کسی</p></div>
<div class="m2"><p>صائب از مکتوب، عنوان نکته دان را بس بود</p></div></div>