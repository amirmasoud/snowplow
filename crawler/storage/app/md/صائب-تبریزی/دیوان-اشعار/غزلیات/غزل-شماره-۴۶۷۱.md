---
title: >-
    غزل شمارهٔ ۴۶۷۱
---
# غزل شمارهٔ ۴۶۷۱

<div class="b" id="bn1"><div class="m1"><p>ای صبا برگی ازان گلشن بی خار بیار</p></div>
<div class="m2"><p>حرف رنگینی ازان لعل گهر بار بیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بهاران بر سان قصه بی برگی من</p></div>
<div class="m2"><p>برگ سبزی پی آرایش دستار بیار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به کف خاکی ازان راهگذر خرسندم</p></div>
<div class="m2"><p>توتیایی پی این دیده خونبار بیار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر چه می گویی ازان لعل شکر بار بگو</p></div>
<div class="m2"><p>هرچه می آوری از مژده دیدار بیار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چه از دوست رسد روشنی چشم من است</p></div>
<div class="m2"><p>گل اگر لایق من نیست خس وخار بیار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وعده آمدنی، گر همه باشد به دروغ</p></div>
<div class="m2"><p>به من ساده دل از یار جفا کار بیار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خبری داری اگر از دهن یار،بگو</p></div>
<div class="m2"><p>حرف سربسته ای از عالم اسرار بیار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چند زنجیر کند پاره دل بیتابم ؟</p></div>
<div class="m2"><p>تار پیچانی ازان طره طرار بیار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خون چشمم ز گرستن به سفیدی زده است</p></div>
<div class="m2"><p>بوی پیراهن یوسف به من زار بیار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حرف آن طره طرار در افکن به میان</p></div>
<div class="m2"><p>موکشان راز مرا بر سر بازار بیار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طوطی از صحبت آیینه سخنساز شود</p></div>
<div class="m2"><p>روی بنما و مرا بر سر گفتار بیار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل بپرداز ز هر نقش که در عالم هست</p></div>
<div class="m2"><p>بعد ازان آینه پیش نظر یار بیار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بی گل روی تو ذرات جهان درخوابند</p></div>
<div class="m2"><p>رخ برافروز وجهان رابه سر کار بیار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیست بر همنفسان زندگی من روشن</p></div>
<div class="m2"><p>روی چون آینه پیش من بیمار بیار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صائب این آن غزل حافظ شیرین سخن است</p></div>
<div class="m2"><p>کای صبا نکهتی از خاک ره یار بیار</p></div></div>