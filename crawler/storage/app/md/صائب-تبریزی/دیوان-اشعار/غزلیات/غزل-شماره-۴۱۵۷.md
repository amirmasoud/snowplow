---
title: >-
    غزل شمارهٔ ۴۱۵۷
---
# غزل شمارهٔ ۴۱۵۷

<div class="b" id="bn1"><div class="m1"><p>قربانیان شکفته به قصاب برخورند</p></div>
<div class="m2"><p>چون پل بغل گشاده به سیلاب برخورند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جمعی که ره به چاشنی فقر برده اند</p></div>
<div class="m2"><p>بر روی بوریا ز شکر خواب برخورند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صاف جهان به مردم خاموش می رسد</p></div>
<div class="m2"><p>لب بسته کوزه ها ز می ناب برخورند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اقبال دیدگان به گنهکار و بیگناه</p></div>
<div class="m2"><p>با جبهه گشاده چو محراب برخورند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون ذره می دوند به هر کوچه عاشقان</p></div>
<div class="m2"><p>شاید به آفتاب جهانتاب برخورند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر گشتگی به طالع جمعی که آمده است</p></div>
<div class="m2"><p>در چشمه سیراب به گرداب برخورند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر کس دعا کند به اجابت قرین شود</p></div>
<div class="m2"><p>در هر کجا به یکدگر احباب برخورند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جمعی که از یگانگی نور آگهند</p></div>
<div class="m2"><p>هر شب که شمع نیست ز مهتاب برخورند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صائب سراغ بحر کنند وروان شوند</p></div>
<div class="m2"><p>از سر گذشتگان چو به سیلاب برخورند</p></div></div>