---
title: >-
    غزل شمارهٔ ۴۵۸
---
# غزل شمارهٔ ۴۵۸

<div class="b" id="bn1"><div class="m1"><p>ز تأثیر دل بیدار، چشم تر شود بینا</p></div>
<div class="m2"><p>که ماه از نور خورشید بلند اختر شود بینا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبرد از چشم سوزن قرب عیسی عیب کوری را</p></div>
<div class="m2"><p>محال است از جواهر سرمه بد گوهر شود بینا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به چشم کم مبین ای ساده دل ما تیره روزان را</p></div>
<div class="m2"><p>که صد آیینه از یک مشت خاکستر شود بینا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ببر زین خاکدان زنهار با خود سرمه بینش</p></div>
<div class="m2"><p>وگرنه کور هیهات است در محشر شود بینا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمی گردد هلال و بدر چون مه، مهر روشندل</p></div>
<div class="m2"><p>محال است از حوادث فربه و لاغر شود بینا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمی آید به کار پاک طینت بینش ظاهر</p></div>
<div class="m2"><p>که افتد از بهای خویش چون گوهر شود بینا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عزیزان نیستند از پرده اسباب مستغنی</p></div>
<div class="m2"><p>ز بوی پیرهن یعقوب پیغمبر شود بینا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بلند و پست عالم می کند افزون بصیرت را</p></div>
<div class="m2"><p>معلم بیش در دریای بی لنگر شود بینا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز سیل تیره حسن سعی دریا می شود ظاهر</p></div>
<div class="m2"><p>که از آیینه تاریک، روشنگر شود بینا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مقیم آستان فیض بخش عشق شو صائب</p></div>
<div class="m2"><p>که نابینا شود گر حلقه این در، شود بینا</p></div></div>