---
title: >-
    غزل شمارهٔ ۵۶۰۷
---
# غزل شمارهٔ ۵۶۰۷

<div class="b" id="bn1"><div class="m1"><p>دست در دامن آن زلف معنبر زده ام</p></div>
<div class="m2"><p>باز بر آتش خود دامن محشر زده ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمع بیدار دلان روشنی از من دارد</p></div>
<div class="m2"><p>آب حیوان به رخ خضر مکرر زده ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برق عشقم که به بال و پر پرواز بلند</p></div>
<div class="m2"><p>قدسیان را سر مقراض به شهپر زده ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسته ام از سخن عشق به خاموشی لب</p></div>
<div class="m2"><p>مهر از موم به منقار سمندر زده ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست یک سرو که پهلو به نهال تو زند</p></div>
<div class="m2"><p>بارها در چمن خلد سراسر زده ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر خط راست روی جاده از من دارد</p></div>
<div class="m2"><p>صفحه دشت جنون را همه مسطر زده ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صفحه خرقه ام از بخیه هستی ساده است</p></div>
<div class="m2"><p>همچو سوزن ز گریبان فنا سر زده ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر چه زاهد نیم، آداب وضو می دانم</p></div>
<div class="m2"><p>شسته ام دست ز سجاده و ساغر زده ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون صدف کاسه در یوزه به نیسان نبرم</p></div>
<div class="m2"><p>به گره آب رخ خویش چو گوهر زده ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من و اندیشه آزادی از آن حلقه زلف؟</p></div>
<div class="m2"><p>رزق پرواز شود بالم اگر پر زده ام!</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صائب آن بلبل مستم که ز شیرین سخنی</p></div>
<div class="m2"><p>نمک سوده به داغ دل محشر زده ام</p></div></div>