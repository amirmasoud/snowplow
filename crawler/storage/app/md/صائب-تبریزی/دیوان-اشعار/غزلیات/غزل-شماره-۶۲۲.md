---
title: >-
    غزل شمارهٔ ۶۲۲
---
# غزل شمارهٔ ۶۲۲

<div class="b" id="bn1"><div class="m1"><p>ز خود برآمده ام، با سفر چه کار مرا؟</p></div>
<div class="m2"><p>بریده ام ز جهان، با ثمر چه کار مرا؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین جهان به مرادی کز آن جهان طلبند</p></div>
<div class="m2"><p>رسیده ام، به جهان دگر چه کار مرا؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو خاک شد شکرستان به مور قانع من</p></div>
<div class="m2"><p>به تنگ گیری شهد و شکر چه کار مرا؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود ز گریه خود فتح باب من چون تاک</p></div>
<div class="m2"><p>به ابرهای پریشان سفر چه کار مرا؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوشم چو غنچه پیکان به کار بسته خود</p></div>
<div class="m2"><p>به انتظار نسیم سحر چه کار مرا؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو هست باده بی دردسر مر از خون</p></div>
<div class="m2"><p>به جام باده پر دردسر چه کار مرا؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمال آینه ساده است حیرانی</p></div>
<div class="m2"><p>به حرف بی اثر و با اثر چه کار مرا؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنین که سنگ ملامت گرفت اطرافم</p></div>
<div class="m2"><p>دگر چو کبک به کوه و کمر چه کار مرا؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>علاج رخنه ملک است کار پادشهان</p></div>
<div class="m2"><p>به رخنه دل و چاک جگر چه کار مرا؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بس است عرفی، همداستان من صائب</p></div>
<div class="m2"><p>به نغمه سنجی مرغ سحر چه کار مرا؟</p></div></div>