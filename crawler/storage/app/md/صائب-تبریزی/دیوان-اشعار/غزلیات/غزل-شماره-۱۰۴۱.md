---
title: >-
    غزل شمارهٔ ۱۰۴۱
---
# غزل شمارهٔ ۱۰۴۱

<div class="b" id="bn1"><div class="m1"><p>از خسیسان منت احسان کشیدن مشکل است</p></div>
<div class="m2"><p>ناز ماه مصر از اخوان کشیدن مشکل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ته دیوار آسان بیرون آمدن</p></div>
<div class="m2"><p>دامن از دست گرانجانان کشیدن مشکل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان لب میگون چه حاصل چون امید بوسه نیست؟</p></div>
<div class="m2"><p>ناز خشک از چشمه حیوان کشیدن مشکل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درد بی درمان به مرگ تلخ شیرین می شود</p></div>
<div class="m2"><p>از طبیبان منت درمان کشیدن مشکل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست محرومی به دل در پله دوری گران</p></div>
<div class="m2"><p>در ته یک پیرهن هجران کشیدن مشکل است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از پریشانی دل از هم گر بریزد گو بریز</p></div>
<div class="m2"><p>منت شیرازه احسان کشیدن مشکل است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دم برآوردن بود بی یاد حق بر دل گران</p></div>
<div class="m2"><p>دلو خالی از چه کنعان کشیدن مشکل است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل به آسانی ز مژگان بتان نتوان گرفت</p></div>
<div class="m2"><p>طعمه از سرپنجه شیران کشیدن مشکل است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی تواضع نیست ممکن سرفرازی یافتن</p></div>
<div class="m2"><p>سوی خود این گوی بی چوگان کشیدن مشکل است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من گرفتم شد قیامت در صف آرایی علم</p></div>
<div class="m2"><p>صف برابر با صف مژگان کشیدن مشکل است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آب از آهن می توان کردن به آسانی جدا</p></div>
<div class="m2"><p>از دل خونگرم ما پیکان کشیدن مشکل است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می توان از سست پیوندان به آسانی برید</p></div>
<div class="m2"><p>در جوانی از دهن دندان کشیدن مشکل است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برق را خاشاک در زنجیر نتواند کشید</p></div>
<div class="m2"><p>دامن عمر سبک جولان کشیدن مشکل است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>می توان چون غنچه صائب خون دل در پرده خورد</p></div>
<div class="m2"><p>باده گلرنگ را پنهان کشیدن مشکل است</p></div></div>