---
title: >-
    غزل شمارهٔ ۶۷۱۹
---
# غزل شمارهٔ ۶۷۱۹

<div class="b" id="bn1"><div class="m1"><p>برد شبنم را برون از باغ، چشم روشنی</p></div>
<div class="m2"><p>با دل روشن تو محو آب و رنگ گلشنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طور از برق تجلی شهر پرواز یافت</p></div>
<div class="m2"><p>از گرانجانی تو پا بر جا چو کوه آهنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تلخ می شد زندگی از نوحه دلمردگان</p></div>
<div class="m2"><p>مرده دل را اگر می بود رسم شیونی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی دل بینا فزاید پرده ای بر غفلتت</p></div>
<div class="m2"><p>با مه کنعان اگر در زیر یک پیراهنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غنچه با دست نگارین پوست را بر تن شکافت</p></div>
<div class="m2"><p>تو ز سستی همچنان زندانی پیراهنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نمی سازی خراب این خانه را چون عاشقان</p></div>
<div class="m2"><p>باز کن چون عاقلان از چشم عبرت روزنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وادی خونخوار سودا را چو مجنون دیده ام</p></div>
<div class="m2"><p>جز دهان شیر در وی نیست دیگر مائمنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حسن عالمسوز را مشاطه ای در کار نیست</p></div>
<div class="m2"><p>می زند هر برگ گل بر آتش گل دامنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر نداری گوشه ای صائب در اقلیم رضا</p></div>
<div class="m2"><p>از تو باشد گر همه روی زمین، بی مائمنی</p></div></div>