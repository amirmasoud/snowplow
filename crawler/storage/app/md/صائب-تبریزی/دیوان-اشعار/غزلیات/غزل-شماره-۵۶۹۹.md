---
title: >-
    غزل شمارهٔ ۵۶۹۹
---
# غزل شمارهٔ ۵۶۹۹

<div class="b" id="bn1"><div class="m1"><p>برون نیامده از برگ بی ثمر شده ام</p></div>
<div class="m2"><p>خبر نیافته از خویش بیخبر شده ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا ز سنگ ملامت چو نیست آزادی</p></div>
<div class="m2"><p>ازین چه سود که چون سرو بی ثمر شده ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به گرد کعبه مقصود اگر نگردیدم</p></div>
<div class="m2"><p>به این خوشم که درین راه پی سپر شده ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر رسد به سرم بی خبر چه خواهم شد</p></div>
<div class="m2"><p>که از رسیدن پیغام بیخبر شده ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قناعت از گل این باغ کرده ام به گلاب</p></div>
<div class="m2"><p>ز آفتاب تسلی به چشم تر شده ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز نارسایی پرواز گشته ام طاوس</p></div>
<div class="m2"><p>زبس فریفته نقش بال و پر شده ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو قطره گر چه فتادم ز چشم ابر بهار</p></div>
<div class="m2"><p>سرم به ابر رسیده است تا گهر شده ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بود ز آهوی رم کرده نافه ای بسیار</p></div>
<div class="m2"><p>ز چشم شوخ تو قانع به یک نظر شده ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نبود ناله من بی اثر چنین صائب</p></div>
<div class="m2"><p>ز هرزه نالی بسیار، بی اثر شده ام</p></div></div>