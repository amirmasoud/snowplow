---
title: >-
    غزل شمارهٔ ۴۸۷۴
---
# غزل شمارهٔ ۴۸۷۴

<div class="b" id="bn1"><div class="m1"><p>چون ترا مسکن میسر شد پی تزیین مباش</p></div>
<div class="m2"><p>تخته کز دریا ترابیرون برد رنگین مباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون سبکروحان لباس از اطلس افلاک کن</p></div>
<div class="m2"><p>همچو صوفی زیر بار خرقه پشمین مباش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راهرو را بستر و بالین بود خواب گران</p></div>
<div class="m2"><p>چون تن آسانان به فکر بستر و بالین مباش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رنگ بیرنگی زآسیب شکستن ایمن است</p></div>
<div class="m2"><p>چون پرطاوس، فرد دفتر تلوین مباش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>علم یکتایی حق بر نمی دارد دویی</p></div>
<div class="m2"><p>نیستی گر اهل شرک ای بی بصر خود بین مباش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا چو شکر نی به ناخن نشکند دوران ترا</p></div>
<div class="m2"><p>صبر کن برتلخکامی، یکقلم شیرین مباش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیستی صائب حریف چشم شور روزگار</p></div>
<div class="m2"><p>گر نگردد بر مرادت آسمان غمگین مباش</p></div></div>