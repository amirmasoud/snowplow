---
title: >-
    غزل شمارهٔ ۵۵۷۳
---
# غزل شمارهٔ ۵۵۷۳

<div class="b" id="bn1"><div class="m1"><p>کجا مایل به هر دل گردد ابرویی که من دانم؟</p></div>
<div class="m2"><p>که سر می‌پیچد از یوسف ترازویی که من دانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمارد موج دریای سراب از بی‌نیازی‌ها</p></div>
<div class="m2"><p>سجود نه فلک را طاق ابرویی که من دانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز خاشاک جگر دوز علایق پاک می‌سازد</p></div>
<div class="m2"><p>زمین سینه سینه‌ها را آتشین‌رویی که من دانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فلک را می‌کشد چون قمریان در حلقه فرمان</p></div>
<div class="m2"><p>به گیسوی مسلسل سر و دلجویی که من دانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کند هم سیر با تخت سلیمان در جهانگردی</p></div>
<div class="m2"><p>ز شوخی شیشه دل را پری‌رویی که من دانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به یک دیدن کند روشن‌تر از رخساره یوسف</p></div>
<div class="m2"><p>چراغ دیده یعقوب را رویی که من دانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جگرگاه زمین کان بدخشان زود می‌گردد</p></div>
<div class="m2"><p>چنین گر تیغ راند دست و بازویی که من دانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو مغز پسته می‌گیرد به شکر تلخ‌کامان را</p></div>
<div class="m2"><p>به حرف شکرین لعل سخنگویی که من دانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گذارد مهر بر لب سحرپردازان بابل را</p></div>
<div class="m2"><p>ز مژگان سخنگو چشم جادویی که من دانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز حرف آرزوی خام می‌بندد به هر جنبش</p></div>
<div class="m2"><p>زبان عاشقان را چین ابرویی که من دانم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به زودی حلقه بیرون در سازد سویدا را</p></div>
<div class="m2"><p>ز حسن دلپذیر آن خال هندویی که من دانم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر در پرده شرم و حیا رویش نهان گردد</p></div>
<div class="m2"><p>به فکر دور گردان می‌فتد بویی که من دانم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به فکر عندلیب بی‌نوای ما کجا افتد؟</p></div>
<div class="m2"><p>که گل از غنچه خسبان است در کویی که من دانم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مشو نومید اگر یک چند خون در دل کند چشمش</p></div>
<div class="m2"><p>که خون را مشک می‌گرداند آهویی که من دانم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز حیرت طوطیان آسمانی را خمش دارد</p></div>
<div class="m2"><p>زبس نور و صفا، آیینه‌رویی که من دانم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر خون دو عالم را کند در شیشه بیدادش</p></div>
<div class="m2"><p>پشیمانی نمی‌داند جفاجویی که من دانم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چرا سازم بیابان مرگ ناکامی دل خود را؟</p></div>
<div class="m2"><p>نمی‌گردد به مجنونم رام، آهویی که من دانم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پریشان می‌کند مغز نسیم صبح را صائب</p></div>
<div class="m2"><p>ز شوخی‌های نکهت عنبرین‌مویی که من دانم</p></div></div>