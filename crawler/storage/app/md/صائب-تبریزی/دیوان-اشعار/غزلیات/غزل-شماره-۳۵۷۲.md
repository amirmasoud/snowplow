---
title: >-
    غزل شمارهٔ ۳۵۷۲
---
# غزل شمارهٔ ۳۵۷۲

<div class="b" id="bn1"><div class="m1"><p>در خیالم اگر آن زلف پریشان می‌بود</p></div>
<div class="m2"><p>نفس سوخته‌ام سنبل و ریحان می‌بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دردمندان چه قدر خون جگر می‌خوردند</p></div>
<div class="m2"><p>درد بی‌دردی اگر قابل درمان می‌بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌شد از حبس دل دولت هرجایی خون</p></div>
<div class="m2"><p>رزق اسکندر اگر چشمه حیوان می‌بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر گلوگیر نمی‌شد غم نان مردم را</p></div>
<div class="m2"><p>همه روی زمین یک لب خندان می‌بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دارد از خرمن من برق دریغ ابر بهار</p></div>
<div class="m2"><p>آه اگر مزرع من تشنه باران می‌بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داده بودند عنان تو به دست تو، اگر</p></div>
<div class="m2"><p>جنبش نبض تو در قبضه فرمان می‌بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صائب اسرار جهان جمله به من می‌شد فاش</p></div>
<div class="m2"><p>اگر آیینه من دیده حیران می‌بود</p></div></div>