---
title: >-
    غزل شمارهٔ ۴۹۰
---
# غزل شمارهٔ ۴۹۰

<div class="b" id="bn1"><div class="m1"><p>خون ما گر سبب چهره آل است ترا</p></div>
<div class="m2"><p>در قدح ریز که چون شیر حلال است ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر مدار از سر ما سایه که چون مهر بلند</p></div>
<div class="m2"><p>سایه چون کم شود، آغاز زوال است ترا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک در دیده آیینه خودبینی زن</p></div>
<div class="m2"><p>تا بدانی که چه مقدار جمال است ترا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جوهر از صافی آیینه حجاب تو شده است</p></div>
<div class="m2"><p>ای که از حسن، نظر بر خط و خال است ترا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی زبانی نکند آب گهر را خس پوش</p></div>
<div class="m2"><p>می شود ظاهر اگر زان که کمال است ترا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خم چوگان ترا گوی سعادت نقدست</p></div>
<div class="m2"><p>سر اندیشه اگر در ته بال است ترا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست جز خشم و تو از جهل برون می فکنی</p></div>
<div class="m2"><p>لقمه تلخ نمایی که حلال است ترا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون لب کاسه دریوزه ز کوته نظری</p></div>
<div class="m2"><p>حاصل از نطق همین حرف سؤال است ترا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از تواضع قد خم گشته خود راست کنی</p></div>
<div class="m2"><p>گر تمنای تمامی چو هلال است ترا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در گذر صائب از اسباب، کز این عبرتگاه</p></div>
<div class="m2"><p>هر چه با خود نتوان برد، وبال است ترا</p></div></div>