---
title: >-
    غزل شمارهٔ ۳۹۹۱
---
# غزل شمارهٔ ۳۹۹۱

<div class="b" id="bn1"><div class="m1"><p>به صبر مشکل عالم تمام بگشاید</p></div>
<div class="m2"><p>که این کلید به هر قفل راست می آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به قسمت ازلی باش از جهان خرسند</p></div>
<div class="m2"><p>که آب بحر به آب گهر نیفزاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من از کجا وبهشت برین مگر رضوان</p></div>
<div class="m2"><p>به درد وداغ تو فردوس را بیاراید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آن چمن که من از گل گلاب می گیرم</p></div>
<div class="m2"><p>ز دور باد صبا پشت دست می خاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آب تیغ جگرگاه خاک شد سیراب</p></div>
<div class="m2"><p>هنوز از شب زلف تو فتنه می زاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشو به سنگدلی از سرشک من ایمن</p></div>
<div class="m2"><p>که رشته مغز گهر رفته رفته فرساید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دو چشم دوخته ای برزمین ازین غافل</p></div>
<div class="m2"><p>که چرخ راه تو از هر ستاره می پاید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه تشنه است به خونریز خلق ابرویش</p></div>
<div class="m2"><p>که در مصاف دو شمشیر کار فرماید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نعیم خلد حلال است بر کسی صائب</p></div>
<div class="m2"><p>که دست ولب به نعیم جهان نیالاید</p></div></div>