---
title: >-
    غزل شمارهٔ ۲۳۲۳
---
# غزل شمارهٔ ۲۳۲۳

<div class="b" id="bn1"><div class="m1"><p>غم ز دل بیرون مرا کی باده احمر برد؟</p></div>
<div class="m2"><p>زردی از آیینه هیهات است روشنگر برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تلخ گویان را دهن شیرین کنم از نوشخند</p></div>
<div class="m2"><p>بشکند چون نیشکر هر کس مرا، شکر برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر سبکباران بود موج خطر باد مراد</p></div>
<div class="m2"><p>کف سلامت کشتی از دریای بی لنگر برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که سازد همچون غواصان نفس در دل گره</p></div>
<div class="m2"><p>از محیط تلخرو دامان پر گوهر برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهل دوست نیست ممکن ترک خودبینی کنند</p></div>
<div class="m2"><p>زنگ ازین آیینه نتوانست اسکندر برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خزان بی برگ دیدن گلستان را مشکل است</p></div>
<div class="m2"><p>مرغ زیرک در بهاران سر به زیر پر برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با دل پرخون من ای تندخو کاوش مکن</p></div>
<div class="m2"><p>صرفه هیهات است آتش زین کباب تر برد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاکیان اکثر گرفتارند در بند جهات</p></div>
<div class="m2"><p>تا که بیرون مهره خود را ازین ششدر برد؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست کار مرغ صائب سینه بر آتش زدن</p></div>
<div class="m2"><p>نامه ما را به آن بدخو مگر صرصر برد</p></div></div>