---
title: >-
    غزل شمارهٔ ۳۷۰۷
---
# غزل شمارهٔ ۳۷۰۷

<div class="b" id="bn1"><div class="m1"><p>ز شکوه گر لبم آن گلعذار می‌بندد</p></div>
<div class="m2"><p>که ره به گریهٔ بی‌اختیار می‌بندد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر تو در نگشایی به روی من از ناز</p></div>
<div class="m2"><p>به آه من که در این حصار می‌بندد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درین ریاض دل جمع غنچه‌ای دارد</p></div>
<div class="m2"><p>که در به روی نسیم بهار می‌بندد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به رنگ و بوی جهان دل منه که وقت رحیل</p></div>
<div class="m2"><p>خزان نگار به دست چنار می‌بندد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز رشک آبله پا دلم پر از خون است</p></div>
<div class="m2"><p>که آب در گره از بهر خار می‌بندد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی هزار شود نقد عمر دیده‌وری</p></div>
<div class="m2"><p>که دل به سوختگان چون شرار می‌بندد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مکن چو خضر درین تیره خاکدان لنگر</p></div>
<div class="m2"><p>که آب زنگ درین جویبار می‌بندد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی که بر سخن اهل حق نهد انشگت</p></div>
<div class="m2"><p>به خون خود کمر ذوالفقار می‌بندد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلیر بر صف افتادگان عشق متاز</p></div>
<div class="m2"><p>که هر پیاده ره صد سوار می‌بندد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کند به زخم زبان هرکه منع من ز جنون</p></div>
<div class="m2"><p>به خار و خس ره سیل بهار می‌بندد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل از سپهر عبث روی دل طمع دارد</p></div>
<div class="m2"><p>چه طرف آینه از زنگبار می‌بندد؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کند ز دولت دنیا ثبات هرکه طمع</p></div>
<div class="m2"><p>به پای برق سبکرو نگار می‌بندد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خوشا کسی که درین میهمان‌سرا صائب</p></div>
<div class="m2"><p>گران نگشته بر احباب، بار می‌بندد</p></div></div>