---
title: >-
    غزل شمارهٔ ۴۴۸۳
---
# غزل شمارهٔ ۴۴۸۳

<div class="b" id="bn1"><div class="m1"><p>ستاره سوخته پروای اعتبار ندارد</p></div>
<div class="m2"><p>که تخم سوخته حاجت به نوبهار ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>توان ز بیخودی ایمن شد از حوادث دوران</p></div>
<div class="m2"><p>خطر سفینه ز دریای بیکنار ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بس گزیده شد از روی تلخ مردم عالم</p></div>
<div class="m2"><p>ز زنگ آینه من به دل غبار ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جنان بساط جهان شد تهی زسوخته جانان</p></div>
<div class="m2"><p>که هیچ سنگ به دل خرده شرار ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی که بیخبر از بحروحدت است که داند</p></div>
<div class="m2"><p>که در کشاکش خود موج اختیار ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رگی ز تندی خو لازم است سیم بران را</p></div>
<div class="m2"><p>که زودچیده شودهرگلی که خار ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اسیر عشق نیندیشداز زبان ملامت</p></div>
<div class="m2"><p>که کبک مست غم از تیغ کوهسار ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو از سیاه دلی روی خود ز خلق نتابی</p></div>
<div class="m2"><p>که پشت آینه وحشت ز زنگ بار ندارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همیشه حلقه ذکر خفی است مهر دهانش</p></div>
<div class="m2"><p>لبی که شکوه ز اوضاع روزگار ندارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به دوش و دامن مریم مسیح بارنگردد</p></div>
<div class="m2"><p>صدف گرانیی ای از در شاهوار ندارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حذر نمی کند از درد و داغ سینه صائب</p></div>
<div class="m2"><p>زمین سوخته پروایی از شرار ندارد</p></div></div>