---
title: >-
    غزل شمارهٔ ۳۴۳۶
---
# غزل شمارهٔ ۳۴۳۶

<div class="b" id="bn1"><div class="m1"><p>شمع با مضطرب از دست حمایت باشد</p></div>
<div class="m2"><p>مرگ بهتر ز حیاتی که به منت باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ثمر از بید و گل از شوره زمین می چیند</p></div>
<div class="m2"><p>از تماشا نظر آن را که به عبرت باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک را چاشنی شهد مصفا دادن</p></div>
<div class="m2"><p>چشمه کاری است که در شان قناعت باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خط که پروانه قتل است هوسناکان را</p></div>
<div class="m2"><p>پیش بالغ نظران آیه رحمت باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باش بر روی زمین بر سر آتش چو سپند</p></div>
<div class="m2"><p>تا ترا زیر زمین خواب فراغت باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون صدف بخیه لب می شودش عقد گهر</p></div>
<div class="m2"><p>هر که گنجینه اسرار حقیقت باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حیف و صد حیف که در حلقه این سنگدلان</p></div>
<div class="m2"><p>نیست گوشی که پذیرای نصیحت باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرچه پستیم، نیاریم فلک را به نظر</p></div>
<div class="m2"><p>پایه مرد به اندازه همت باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دولت آن است که پیوسته و جاوید بود</p></div>
<div class="m2"><p>دولت آن نیست که موقوف به نوبت باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیولاخی است جهان در نظر او صائب</p></div>
<div class="m2"><p>هر که را ره به پریخانه عزلت باشد</p></div></div>