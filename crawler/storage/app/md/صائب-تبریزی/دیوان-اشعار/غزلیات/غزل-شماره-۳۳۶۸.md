---
title: >-
    غزل شمارهٔ ۳۳۶۸
---
# غزل شمارهٔ ۳۳۶۸

<div class="b" id="bn1"><div class="m1"><p>خضر اگر چاشنی تیغ شهادت می کرد</p></div>
<div class="m2"><p>ز آب حیوان به لب خشک قناعت می کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشتی حوصله طوفانی شبنم می شد</p></div>
<div class="m2"><p>گل اگر از رخ او کسب طراوت می کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می شد از غیرت آیینه دل عاشق آب</p></div>
<div class="m2"><p>خوبی معنی اگر جلوه به صورت می کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این زمان ناز هما می کشد از سایه جغد</p></div>
<div class="m2"><p>بی نیازی که دو صد ناز به دولت می کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این که در کوی تو دل رنگ اقامت می ریخت</p></div>
<div class="m2"><p>کاش بر ریگ روان طرح عمارت می کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صفحه روی ترا دید و ورق برگرداند</p></div>
<div class="m2"><p>ساده لوحی که به من دوش نصیحت می کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیشتر زان که دهد خامه به دستش استاد</p></div>
<div class="m2"><p>الف قامت او مشق قیامت می کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی لب لعل تو صائب المی داشت که گل</p></div>
<div class="m2"><p>در نظر جلوه خمیازه حسرت می کرد</p></div></div>