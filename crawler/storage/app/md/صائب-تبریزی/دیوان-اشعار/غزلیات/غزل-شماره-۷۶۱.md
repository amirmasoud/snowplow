---
title: >-
    غزل شمارهٔ ۷۶۱
---
# غزل شمارهٔ ۷۶۱

<div class="b" id="bn1"><div class="m1"><p>دود از نهاد خلق برآرد گزند ما</p></div>
<div class="m2"><p>افتد به کار شعله گره از سپند ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل بر نگارخانه صورت نبسته ایم</p></div>
<div class="m2"><p>از شیر ماهتاب شود آب، قند ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرگز چنان نشد که درین دشت پر شکار</p></div>
<div class="m2"><p>دست افکند به گردن صیدی کمند ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک گام بر مراد دل خود نرفته ایم</p></div>
<div class="m2"><p>در دست گمرهی است عنان سمند ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی طاقتان هلاک نسیم بهانه اند</p></div>
<div class="m2"><p>از ماهتاب سوخته گردد سپند ما</p></div></div>