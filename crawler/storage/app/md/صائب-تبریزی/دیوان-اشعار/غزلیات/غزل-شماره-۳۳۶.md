---
title: >-
    غزل شمارهٔ ۳۳۶
---
# غزل شمارهٔ ۳۳۶

<div class="b" id="bn1"><div class="m1"><p>چنان دانسته می باید درین دنیا نهی پا را</p></div>
<div class="m2"><p>که بر موی میان مور در صحرا نهی پا را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قدم بیجا نهادن در قفا دارد پشیمانی</p></div>
<div class="m2"><p>ادا کن سجده سهوی اگر بی جا نهی پا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حضور کنج عزلت گر ترا از خاک بردارد</p></div>
<div class="m2"><p>اگر در خلد خوانندت به استغنا نهی پا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دامان تجرد گر سبکروحانه آویزی</p></div>
<div class="m2"><p>چو عیسی از زمین بر عالم بالا نهی پا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نریزی گر به خاک راه آب روی درویشی</p></div>
<div class="m2"><p>کنی سبز از طراوت چون خضر هر جا نهی پا را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>توانی گر ز خود چون بوی پیراهن برون آمد</p></div>
<div class="m2"><p>شود بینا، اگر بر چشم نابینا نهی پا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگهبان بی شمارست از یمین و از یسار تو</p></div>
<div class="m2"><p>مبادا هر طرف چون مست، بی پروا نهی پا را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به کوه قاف پشت خود دهی از روی آسایش</p></div>
<div class="m2"><p>برون گر از میان خلق چون عنقا نهی پا را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر خود را به جوش از پستی خامی برون آری</p></div>
<div class="m2"><p>به فرق عقل، بی باکانه چون صهبا نهی پا را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مجرد گر توانی گشت چون نور نظر از خود</p></div>
<div class="m2"><p>به چشم روشن خورشید چون عیسی نهی پا را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به سوهان ریاضت خویشتن را گر سبک سازی</p></div>
<div class="m2"><p>به جرأت چون کف سرمست بر دریا نهی پا را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سبک چون پنبه از سر وا کنی گردانه تن را</p></div>
<div class="m2"><p>چو مستان بی محابا بر سر مینا نهی پا را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بود هر ذره زین خاک سیه، خورشید رخساری</p></div>
<div class="m2"><p>مبادا بر زمین از روی استغنا نهی پا را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به سرعت آنچنان زین خاکدان تیره راهی شو</p></div>
<div class="m2"><p>که گردد سرمه از گرمی، چو بر خارا نهی پا را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز مشرق تا به مغرب طی کنی یک روز بی زحمت</p></div>
<div class="m2"><p>اگر چون مهر در راه طلب تنها نهی پا را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گذشتن از صراط آسان شود روز جزا بر تو</p></div>
<div class="m2"><p>اگر صائب ز روی احتیاط اینجا نهی پا را</p></div></div>