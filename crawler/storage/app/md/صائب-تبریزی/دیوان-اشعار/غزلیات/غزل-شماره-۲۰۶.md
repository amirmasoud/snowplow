---
title: >-
    غزل شمارهٔ ۲۰۶
---
# غزل شمارهٔ ۲۰۶

<div class="b" id="bn1"><div class="m1"><p>دل سیه سازد در و دیوار، سودا کرده را</p></div>
<div class="m2"><p>شهر زندان است روی دل به صحرا کرده را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کوس رحلت نغمه داود می آید به گوش</p></div>
<div class="m2"><p>پیشتر از کوچ، زاد ره مهیا کرده را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهپر پرواز چشم است از تمناهای خام</p></div>
<div class="m2"><p>چشم قربانی است دل ترک تمنا کرده را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قطره گردد گوهر غلطان در آغوش صدف</p></div>
<div class="m2"><p>دل تپد در سینه دایم سیر دریا کرده را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لب به روشن گوهران وا کن که ابر نوبهار</p></div>
<div class="m2"><p>مهر گوهر می زند بر لب، دهن وا کرده را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پرده ناموس از زخم زبان لرزد به خود</p></div>
<div class="m2"><p>هیچ پروا از ملامت نیست رسوا کرده را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از دل تارست در چشم تو دنیا بی صفا</p></div>
<div class="m2"><p>یوسفستان است عالم دل مصفا کرده را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشم پوشیدن بود مشاطه رخسار زشت</p></div>
<div class="m2"><p>جنت نقدست دنیا، رو به عقبی کرده را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ابر نیسان از صدف احسان نمی دارد دریغ</p></div>
<div class="m2"><p>مخزن گوهر شود دل دست بالا کرده را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شبنم گلزار جنت در نمی آید به چشم</p></div>
<div class="m2"><p>گریه همچون شمع در دامان شب ها کرده را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گل به شبنم روی خود را پاک نتوانست کرد</p></div>
<div class="m2"><p>چهره خونین است دایم خنده بی جا کرده را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از سواد شهر بر مجنون شود عالم سیاه</p></div>
<div class="m2"><p>خانه گور تنگ باشد سیر صحرا کرده را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زندگی بر من شد از تیغ شهادت ناگوار</p></div>
<div class="m2"><p>می شود باطل تیمم آب پیدا کرده را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عالم پر شور صائب وحشت آبادی بود</p></div>
<div class="m2"><p>سیر کوه قاف عزلت همچو عنقا کرده را</p></div></div>