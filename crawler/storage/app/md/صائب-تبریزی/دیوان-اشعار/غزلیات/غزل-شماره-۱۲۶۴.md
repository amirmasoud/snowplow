---
title: >-
    غزل شمارهٔ ۱۲۶۴
---
# غزل شمارهٔ ۱۲۶۴

<div class="b" id="bn1"><div class="m1"><p>رحمت ایزد نصیب مردم هشیار نیست</p></div>
<div class="m2"><p>پیش ارباب کرم جرمی چو استغفار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ریشه کرده است آشیان ما چو سنبل در چمن</p></div>
<div class="m2"><p>بلبل ما را هوای رفتن از گلزار نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوته خاری چو مجنون افسر خود می کنند</p></div>
<div class="m2"><p>شعله مغزان را سری با پیچش دستار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلف از بی رویی خط دست ازان رخسار داشت</p></div>
<div class="m2"><p>هیچ شمشیری بتر از حرف پهلودار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غیر صائب کز نوا در پیش دارد چرخ را (کذا)</p></div>
<div class="m2"><p>بلبل خوش نغمه ای امروز در گلزار نیست</p></div></div>