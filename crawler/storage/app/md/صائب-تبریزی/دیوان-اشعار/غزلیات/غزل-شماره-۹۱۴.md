---
title: >-
    غزل شمارهٔ ۹۱۴
---
# غزل شمارهٔ ۹۱۴

<div class="b" id="bn1"><div class="m1"><p>ای خوشه چین سنبل زلف تو مشک ناب</p></div>
<div class="m2"><p>شبنم گدای گلشن حسن تو آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در محفل تو ناله فرامش کند سپند</p></div>
<div class="m2"><p>در آتش تو گریه شادی کند کباب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از وصل گشت گریه من جانگدازتر</p></div>
<div class="m2"><p>از آفتاب، تلخ شود بیشتر گلاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیوانه قلمرو صحرای وحشتیم</p></div>
<div class="m2"><p>ما را سواد شهر بود آیه عذاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر دیده های پاک، روان است حکم عشق</p></div>
<div class="m2"><p>هر شبنمی که هست، بود خرج آفتاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیوسته از هوای خود آزار می کشم</p></div>
<div class="m2"><p>در خانه است دشمن من فرش چون حباب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دست از طمع بشوی که از شومی طمع</p></div>
<div class="m2"><p>در حق خود دعای گدا نیست مستجاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از عیب می فتد به هنر چشم ها پاک</p></div>
<div class="m2"><p>از بحر تلخ، آب گهر می برد سحاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد غفلتم ز عمر سبکسیر بیشتر</p></div>
<div class="m2"><p>سنگین نمود خواب مرا این صدای آب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شاهی که بر رعیت خود می کند ستم</p></div>
<div class="m2"><p>مستی بود که می کند از ران خود کباب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زان دم که دید گوشه ابروی یار را</p></div>
<div class="m2"><p>شد ماه عید ناخنه چشم آفتاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صائب مکن توقع آسایش از جهان</p></div>
<div class="m2"><p>دلهای آب کرده بود موج این سراب</p></div></div>