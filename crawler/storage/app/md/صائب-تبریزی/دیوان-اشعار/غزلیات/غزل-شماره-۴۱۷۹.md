---
title: >-
    غزل شمارهٔ ۴۱۷۹
---
# غزل شمارهٔ ۴۱۷۹

<div class="b" id="bn1"><div class="m1"><p>هر بلبلی که زمزمه بنیاد می‌کند</p></div>
<div class="m2"><p>اول مرا به برگ گلی یاد می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از درد رو متاب که یک قطره خون گرم</p></div>
<div class="m2"><p>در دل هزار میکده ایجاد می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آهی که زیر لب شکند دردمند عشق</p></div>
<div class="m2"><p>در سینه کار تیشهٔ فولاد می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این ظلم دیگرست که عاشق شکار من</p></div>
<div class="m2"><p>چون مرغ پر شکسته شد آزاد می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ناف حسن سعی شود مشک عاقبت</p></div>
<div class="m2"><p>خونی که صید در دل صیاد می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیوان عاشقان به قیامت نمی‌کشد</p></div>
<div class="m2"><p>ایام خط تلافی بیداد می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاجز چو سبزه ته سنگ است در دلت</p></div>
<div class="m2"><p>آهم که ریشه در دل فولاد می‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواهد ثواب بت‌شکنان یافت روز حشر</p></div>
<div class="m2"><p>هرکس که در شکست من امداد می‌کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رنگی که از خزان خجالت شکسته شد</p></div>
<div class="m2"><p>بر چهره کار سیلی استاد می‌کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیوسته سرخ رو بود از پاک گوهری</p></div>
<div class="m2"><p>هرکس که چون شراب دلی شاد می‌کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از پیچ و تاب اهل سخن صائب آگه است</p></div>
<div class="m2"><p>چون سرو هرکه مصرعی ایجاد می‌کند</p></div></div>