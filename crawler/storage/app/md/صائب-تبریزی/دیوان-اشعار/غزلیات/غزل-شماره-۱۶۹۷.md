---
title: >-
    غزل شمارهٔ ۱۶۹۷
---
# غزل شمارهٔ ۱۶۹۷

<div class="b" id="bn1"><div class="m1"><p>نه انجم است که زینت فروز نه فلک است</p></div>
<div class="m2"><p>که بر صحیفه افلاک، نقطه های شک است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تغافلی که به حال کسی بود مخصوص</p></div>
<div class="m2"><p>هزار بار به از التفات مشترک است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حریف ناله نه ای، در گذر ز صحبت من</p></div>
<div class="m2"><p>که ماجرای من و وصل، آتش و نمک است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هوش باش نسازی طعام خود را شور</p></div>
<div class="m2"><p>که شعر همچو طعام، استعاره چون نمک است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همین خط است که باطل ز حق جدا سازد</p></div>
<div class="m2"><p>وگرنه حسن زن و مرد، هر دو مشترک است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کلام خویش به هر بیخرد مخوان صائب</p></div>
<div class="m2"><p>سخن وظیفه جان است و روزی ملک است</p></div></div>