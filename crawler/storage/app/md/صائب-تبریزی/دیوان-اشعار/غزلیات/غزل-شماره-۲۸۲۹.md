---
title: >-
    غزل شمارهٔ ۲۸۲۹
---
# غزل شمارهٔ ۲۸۲۹

<div class="b" id="bn1"><div class="m1"><p>چنین از خون اگر دامان آن گل لاله گون گردد</p></div>
<div class="m2"><p>زدامنگیری او آستینها جوی خون گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زهم پاشید دلها تا بریدی زلف مشکین را</p></div>
<div class="m2"><p>پریشان می شود لشکر علم چون سرنگون گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به عمر نوح نتوان از گرستن داد بیرونش</p></div>
<div class="m2"><p>دلی کز کاوش مژگان او دریای خون گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفس در سینه خاکستر شود صحرانوردان را</p></div>
<div class="m2"><p>غبار خاطرم گر دامن دشت جنون گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل خورشید دارد غنچه نیلوفرش در بر</p></div>
<div class="m2"><p>چو گردون هر تنی کز سنگ طفلان نیلگون گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زنقش خوبرویان می رود کوه گران از جا</p></div>
<div class="m2"><p>مگر تمکین شیرین بند پای بیستون گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مکن صائب پریشان همت خود را به هر کاری</p></div>
<div class="m2"><p>که صاحب فن نگردد هر که خواهد ذوفنون گردد</p></div></div>