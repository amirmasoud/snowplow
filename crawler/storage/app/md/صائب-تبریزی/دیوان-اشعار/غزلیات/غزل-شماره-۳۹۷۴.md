---
title: >-
    غزل شمارهٔ ۳۹۷۴
---
# غزل شمارهٔ ۳۹۷۴

<div class="b" id="bn1"><div class="m1"><p>اگر کسی متوسل به چاره ساز شود</p></div>
<div class="m2"><p>هم از طبیب و هم از چاره بی نیاز شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هلال سعی کند در کمال خود غافل</p></div>
<div class="m2"><p>که چون تمام شود بوته گداز شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دهن به ابر گهربار باز کن که صدف</p></div>
<div class="m2"><p>به لب گشودنی از بحر بی نیاز شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمار آه به مقدار عقده های دل است</p></div>
<div class="m2"><p>به قدر دانه زبان خوشه را دراز شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا دلی است ز صبح وصال روشنتر</p></div>
<div class="m2"><p>چگونه سینه من پرده پوش راز شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشیده دار عنان سخن که همچون شمع</p></div>
<div class="m2"><p>زبان دراز چو گردید خرج گاز شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به طوف کعبه رود بت در آستین صائب</p></div>
<div class="m2"><p>کسی که با خودی خویش در نماز شود</p></div></div>