---
title: >-
    غزل شمارهٔ ۵۹۰۶
---
# غزل شمارهٔ ۵۹۰۶

<div class="b" id="bn1"><div class="m1"><p>دستی که به جامی نشود رهزن هوشم</p></div>
<div class="m2"><p>چون پایه تابوت گران است به دوشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دستی که به احسان نکند حلقه بگوشم</p></div>
<div class="m2"><p>چون پایه تابوت گران است به دوشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فریاد من از سوختگیهاست چو آتش</p></div>
<div class="m2"><p>چون باده ز خامی نبود جوش و خروشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نتوان چو لب جام کشید از لب من حرف</p></div>
<div class="m2"><p>هر چند ز رنگین سخنی رهزن هوشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با شعله خورشید چه سازد نفس صبح؟</p></div>
<div class="m2"><p>روشنتر ازانم که توان کرد خموشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دل شکند شیشه مرا خنده گلها</p></div>
<div class="m2"><p>آواز تو زان دم که رسیده است به گوشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر باده سر جوش نباشد نظر من</p></div>
<div class="m2"><p>کز درد توان گرد برآورد ز هوشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در عالم ایجاد من آن طفل یتیمم</p></div>
<div class="m2"><p>کز شیر به دشنام کند دایه خموشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون کعبه، برازندگیم در نظر خلق</p></div>
<div class="m2"><p>زان است که من جامه پوشیده نپوشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صائب منم آن نغمه را کز دل پر جوش</p></div>
<div class="m2"><p>موقوف بهاران نبود جوش و خروشم</p></div></div>