---
title: >-
    غزل شمارهٔ ۳۱۳۴
---
# غزل شمارهٔ ۳۱۳۴

<div class="b" id="bn1"><div class="m1"><p>نسیم مصر با صد کاروان یوسف لقا آمد</p></div>
<div class="m2"><p>پریزاد بهار از کوه قاف کبریا آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دامن می رسد چاک گریبان گلعذاران را</p></div>
<div class="m2"><p>زگلزار که یارب این نسیم آشنا آمد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سواد مصر از نظارگی یک چشم بینا شد</p></div>
<div class="m2"><p>که از زندان به روی تخت آن یوسف لقا آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هم پیوست چون بال پری ابر سبک جولان</p></div>
<div class="m2"><p>سلیمان وار تا گل بر سر تخت هوا آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زحی رو در بیابان کرد گویا محمل لیلی</p></div>
<div class="m2"><p>که دل در سینه مجنون به جنبش چون درا آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهار نوجوان تاب و توانی داد پیران را</p></div>
<div class="m2"><p>که نرگس از ضمیر خاک بیرون بی عصا آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زفیض نوبهاران شد هوا چندان به کیفیت</p></div>
<div class="m2"><p>که از خلوت برون زاهد پی کسب هوا آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زلطف نوبهاران سنگها نوعی ملایم شد</p></div>
<div class="m2"><p>که سالم دانه بیرون از دهان آسیا آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سپهر گرم جولان چشم قربانی شد از حیرت</p></div>
<div class="m2"><p>زمین چون آسمان در جنبش از نشو و نما آمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سلامت باد ساقی گر بهاران روی گردان شد</p></div>
<div class="m2"><p>می گلرنگ باقی باد اگر گل بیوفا آمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زشکر خنده برق آسمان شد یک لب خندان</p></div>
<div class="m2"><p>ز ابرتر هوا یک روی پرشرم و حیا آمد</p></div></div>