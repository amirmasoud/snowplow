---
title: >-
    غزل شمارهٔ ۵۸۸۱
---
# غزل شمارهٔ ۵۸۸۱

<div class="b" id="bn1"><div class="m1"><p>تا کی به ذوق نشأه می دردسر کشیم؟</p></div>
<div class="m2"><p>تلخی ز بحر چند برای گهر کشیم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیر ترا ز سینه کشیدن نه کار ماست</p></div>
<div class="m2"><p>آهی مگر به قوت عجز از جگر کشیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قانع ز گل نه ایم به بویی چو عندلیب</p></div>
<div class="m2"><p>ما سرو را چو فاخته در زیر پر کشیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان پیش کافتاب حوادث شود بلند</p></div>
<div class="m2"><p>خود را ز خم به سایه کوه و کمر کشیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کو بخت تا لباس گل آلود جسم را</p></div>
<div class="m2"><p>در چشمه سار تیغ به آب گهر کشیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خون مرده است در تن ما از فسردگی</p></div>
<div class="m2"><p>منت چه لازم است که از نیشتر کشیم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از اشک شمع، دامن فانوس تر شود</p></div>
<div class="m2"><p>در محفلی که رشته ز عقد گهر کشیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تنگ است جا بر آن سگ کو از وجود ما</p></div>
<div class="m2"><p>صائب بیا که رخت به جای دگر کشیم</p></div></div>