---
title: >-
    غزل شمارهٔ ۷۷۱
---
# غزل شمارهٔ ۷۷۱

<div class="b" id="bn1"><div class="m1"><p>در سیر و دور می گذرد ماه و سال ما</p></div>
<div class="m2"><p>چون گردباد ریشه ندارد نهال ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ذاتی است روشنایی ما همچو آفتاب</p></div>
<div class="m2"><p>نقصی نمی رسد به کمال از زوال ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در حفظ آبرو ز حبابیم تشنه تر</p></div>
<div class="m2"><p>از آب خضر خشک برآید سفال ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خون می کند ز دیده روان نیش انتقام</p></div>
<div class="m2"><p>خاری اگر به سهو شود پایمال ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوهر فشاند گرد یتیمی ز روی خویش</p></div>
<div class="m2"><p>از دل برون نرفت غبار ملال ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پشت فتادگی بود ایمن ز خاکمال</p></div>
<div class="m2"><p>دشمن چگونه صرفه برد از جدال ما؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صد پیرهن بود به از آماس، لاغری</p></div>
<div class="m2"><p>از آفتاب نور نگیرد هلال ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عمری است تا ز خویش برون رفته ایم ما</p></div>
<div class="m2"><p>چون می شود غریب نباشد خیال ما؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از بیم چشم چهره به خوناب شسته ایم</p></div>
<div class="m2"><p>چون گل ز بی غمی نبود رنگ آل ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>داریم چشم آن که برآرد ز تشنگی</p></div>
<div class="m2"><p>صحرای حشر را عرق انفعال ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>افغان که چون حنای شفق، صبح طلعتی</p></div>
<div class="m2"><p>رنگین نکرد دست ز خون حلال ما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سر جوش عمر را گذراندیم در گناه</p></div>
<div class="m2"><p>شد صرف شوره زار سراسر زلال ما</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از قرب مردمان ز حق افتاده ایم دور</p></div>
<div class="m2"><p>در انقطاع خلق بود اتصال ما</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برگشتنی است گر چه ز کوه گران، صدا</p></div>
<div class="m2"><p>تمکین او نداد جواب سؤال ما</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از گوشمال، دست معلم کبود شد</p></div>
<div class="m2"><p>شوخی ز سر نهشت دل خردسال ما</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پیش رخ گشاده دلدار، می شود</p></div>
<div class="m2"><p>پیچیده تر ز زلف زبان سؤال ما</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صائب فغان که گشت درین بوستانسرا</p></div>
<div class="m2"><p>طاوس وار بال و پر ما و بال ما</p></div></div>