---
title: >-
    غزل شمارهٔ ۳۴۵۳
---
# غزل شمارهٔ ۳۴۵۳

<div class="b" id="bn1"><div class="m1"><p>داغ هر لاله که بر سینه هامون باشد</p></div>
<div class="m2"><p>مهری از محضر رسوایی مجنون باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دورگردی نشود مانع یکتایی دل</p></div>
<div class="m2"><p>قطره در ابر همان در دل جیحون باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناز گرداند ورق، حسن به انصاف آمد</p></div>
<div class="m2"><p>یارب آن خط دلاویز چه مضمون باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه دست ستم خار بلند افتاده است</p></div>
<div class="m2"><p>کوته از دامن عریانی مجنون باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوشدلی نیست درین دایره گوژ و کبود</p></div>
<div class="m2"><p>وقت آن خوش که ازین دایره بیرون باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرچه رنگین به نظر جلوه کند عالم خاک</p></div>
<div class="m2"><p>نیک چون در نگری یک دل پرخون باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کیست با او طرف بحث تواند گشتن؟</p></div>
<div class="m2"><p>هرکه را پشت به خم همچو فلاطون باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنچه از چرخ به ارباب سخن می گذرد</p></div>
<div class="m2"><p>جای رحم است بر آن سرو که موزون باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکوه از داغ ندارد جگر ما صائب</p></div>
<div class="m2"><p>جغد در گوشه ویرانه همایون باشد</p></div></div>