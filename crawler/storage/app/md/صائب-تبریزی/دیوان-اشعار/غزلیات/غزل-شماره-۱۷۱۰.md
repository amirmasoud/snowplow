---
title: >-
    غزل شمارهٔ ۱۷۱۰
---
# غزل شمارهٔ ۱۷۱۰

<div class="b" id="bn1"><div class="m1"><p>سرود مجلس ما جوش مستی ازل است</p></div>
<div class="m2"><p>بط شراب در اینجا خروس بی محل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسا شکست کز او کارها درست شود</p></div>
<div class="m2"><p>کلید رزق گدا، پای لنگ و دست شل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز حال سوختگان بو کجا توانی برد؟</p></div>
<div class="m2"><p>ترا که گل به گریبان و مشک در بغل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهان چو دیده سوزن بود بر آن غافل</p></div>
<div class="m2"><p>که تار و پود حیاتش ز رشته امل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حدیث مرده دلان را به گوش راه مده</p></div>
<div class="m2"><p>که رخنه لب این قوم، رخنه اجل است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به من که پاکتر از چشم عشقبازانم</p></div>
<div class="m2"><p>مدار چرخ مشعبد به مهره دغل است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به غیر سایه دیوار خاکساری نیست</p></div>
<div class="m2"><p>عمارتی که درین روزگار بی خلل نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جنون طرازی ما نیست صائب امروزی</p></div>
<div class="m2"><p>میان ما و جنون آشنایی ازل است</p></div></div>