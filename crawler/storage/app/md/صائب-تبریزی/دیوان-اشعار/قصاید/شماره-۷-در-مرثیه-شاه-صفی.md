---
title: >-
    شمارهٔ ۷ - در مرثیه شاه صفی
---
# شمارهٔ ۷ - در مرثیه شاه صفی

<div class="b" id="bn1"><div class="m1"><p>پادشاهی و جوانی سد راه او نشد</p></div>
<div class="m2"><p>کرد چون ادهم ز ملک عالم فانی کنار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خور اقبال روزافزون خود جایی نیافت</p></div>
<div class="m2"><p>بال بر هم زد برون رفت از جهان بی مدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در محرم کرد عزم قندهار و در صفر</p></div>
<div class="m2"><p>کرد در کاشان سفر از عالم آن کوه وقار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رفت سال «غبن » از عالم، زهی غبن تمام</p></div>
<div class="m2"><p>سوخت عالم را به داغ غبن آن عالم مدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چارده سال هلالی مذهب اثناعشر</p></div>
<div class="m2"><p>بود از شمشیر گردون صولت او پایدار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهره از عمر گرامی یافت یک قرن تمام</p></div>
<div class="m2"><p>اول قرن دوم رفت از جهان بی مدار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو ذوالقرنین عالمگیر می شد دولتش</p></div>
<div class="m2"><p>مهلت قرن دوم می یافت گر از روزگار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>«ظل حق » چون بود سال شاهیش، سال رحیل</p></div>
<div class="m2"><p>گشت «آه از ظل حق » تاریخ آن عالی تبار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیده خونبار شد هر حلقه زنجیر عدل</p></div>
<div class="m2"><p>کاین چنین نوشیروانی کرد از عالم گذار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چهره او بود باغ دلگشای عالمی</p></div>
<div class="m2"><p>دیدنش می برد از آیینه بینش غبار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بود بر فرق سلیمان سایه بال پری</p></div>
<div class="m2"><p>بر سر تاج زر او جیغه های زرنگار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفتگویش وحشیان را بند بر پا می نهاد</p></div>
<div class="m2"><p>طایران قدس را می کرد خلق او شکار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صبح نوروز جهان بود از رخ چون آفتاب</p></div>
<div class="m2"><p>مایه عیش جهانی بود چون فصل بهار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در بهشت خلق او منع تماشایی نبود</p></div>
<div class="m2"><p>جنت بی پاسبانی بود در هنگام بار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بود با خلق جهان چون صبح صادق خنده رو</p></div>
<div class="m2"><p>چین نمی گردید هرگز از جبینش آشکار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>غنچه سربسته پیشش نامه واکرده بود</p></div>
<div class="m2"><p>در دل خارا خبر می داد از عقد شرار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ماه عید فتح و نصرت بود از شمشیر کج</p></div>
<div class="m2"><p>محور چرخ ظفر بود از سنان آبدار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ذره تا خورشید را در پایه خود می شناخت</p></div>
<div class="m2"><p>بود در مردم شناسی بی نظیر روزگار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پیش چشم خرده بین او رموز کاینات</p></div>
<div class="m2"><p>در دل شب همچو انجم بود یکسر آشکار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هیچ رازی بر ضمیر روشنش پنهان نبود</p></div>
<div class="m2"><p>ابجد او بود خط سرنوشت روزگار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>باطنش درویش و ظاهر پادشاه وقت بود</p></div>
<div class="m2"><p>داشت پنهان خرقه در زیر لباس زرنگار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آب می شد از گناه دیگران آزرم او</p></div>
<div class="m2"><p>آیتی از رحمت حق بود و عفو کردگار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>حفظ ناموس جهان را هیچ کس چون او نکرد</p></div>
<div class="m2"><p>با کمال اقتدار از خسروان نامدار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بی نیاز از شهرت (و) مستغنی از تدبیر بود</p></div>
<div class="m2"><p>داشت دایم لوح تعلیم از دل خود در کنار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تاج فرق پادشاهان بود از راه نسب</p></div>
<div class="m2"><p>در حسب ممتاز بود از خسروان روزگار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>با همه فرماندهی فرمان پذیر شرع بود</p></div>
<div class="m2"><p>سرنمی پیچید از فرمان حق در هیچ کار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آفتابی بود از نور ولایت جبهه اش</p></div>
<div class="m2"><p>بود کار او رواج دین حق لیل و نهار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سعی در تسخیر دلها داشت بیش از آب و گل</p></div>
<div class="m2"><p>بود یک دل پیش او بهتر ز صد شهر و دیار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون جواب تلخ، بی منت به سایل بحر را</p></div>
<div class="m2"><p>همتش می داد و می شد جبهه اش گوهرنثار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بزم را خورشید تابان، رزم را مریخ بود</p></div>
<div class="m2"><p>وقت پیمان بود چون سد سکندر استوار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بر رعیت مهربان بود و به دشمن قهرمان</p></div>
<div class="m2"><p>در مقام خویش قهر و لطف را می برد کار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>لطف عالمگیر او چون رحمت حق عام بود</p></div>
<div class="m2"><p>داشت یک نسبت به خار و گل چو ابر نوبهار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نقره انجم روان می شد ز جوی کهکشان</p></div>
<div class="m2"><p>چرخ را می داد اگر سرپنجه قهرش فشار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جوهر تیغ شجاعت بود از چین جبین</p></div>
<div class="m2"><p>ذوالفقاری بود عالمسوز روز کارزار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در زمان او که بود اضداد با هم متفق</p></div>
<div class="m2"><p>چشم شیران بود شمع بزم آهوی تتار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خار از بیم سیاست در زمان عدل او</p></div>
<div class="m2"><p>دامن گل را به مژگان پاک می کرد از غبار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مسند اقبالش از دست دعای خلق بود</p></div>
<div class="m2"><p>بود چتر دولت او سایه پروردگار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هر سر مویش جهانی بود از تدبیر عقل</p></div>
<div class="m2"><p>آه چون گویم، جهانی رفت ازین نیلی حصار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ماه مصری بود هر خلقش ز اخلاق جمیل</p></div>
<div class="m2"><p>کاروانی پر ز یوسف رفت بیرون زین دیار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بی سخن در هیچ عصر و هیچ دورانی نداشت</p></div>
<div class="m2"><p>شاه بیتی این چنین مجموعه لیل و نهار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>در جوانی داد دولت را به فرزند جوان</p></div>
<div class="m2"><p>تا به کام دل شود از عمر و دولت کامکار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کرد پاک از خصم، بیرون و درون ملک را</p></div>
<div class="m2"><p>شمه ای نگذاشت باقی از رسوم گیرودار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کرد کوته از خراسان پای ازبک را به تیغ</p></div>
<div class="m2"><p>صلح کرد از یک جهت با رومیان نابکار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>کرد از تدبیر محکم رخنه های ملک را</p></div>
<div class="m2"><p>بعد ازان فرمود رحلت از جهان بی مدار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>داد دولت را به فرزند جوان عباس شاه</p></div>
<div class="m2"><p>تا بماند نام او در هر دو عالم پایدار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>یارب این شاه جوان بخت بلند اقبال را</p></div>
<div class="m2"><p>تا دم صبح قیامت در جهان پاینده دار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>آه کز سنگین دلی های سپهر بی مدار</p></div>
<div class="m2"><p>روشنی بخش جهان را روز عشرت گشت تار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>در بهار نوجوانی کرد عالم را وداع</p></div>
<div class="m2"><p>آسمان تختی که تاجش بود مهر زرنگار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>آن که چون طوبی جهانی بود زیر سایه اش</p></div>
<div class="m2"><p>ناگهان از تندباد مرگ شد بی برگ و بار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>از قضای آسمانی بر زمین پهلو نهاد</p></div>
<div class="m2"><p>آن که می کرد از زمین بوسش جهانی افتخار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>آن که چون شبنم به روی بستر گل تکیه داشت</p></div>
<div class="m2"><p>کرد از خاک سیه بالین و بستر اختیار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>آن که رویش بود عالم را بهار ارغوان</p></div>
<div class="m2"><p>شد ز بیماری چو شاخ زعفران زرد و نزار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>آن که آب دست او می داد جان بیمار را</p></div>
<div class="m2"><p>کرد در یک آب خوردن جان شیرین را نثار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>آنقدر فرصت که حرفی آید از دل بر زبان</p></div>
<div class="m2"><p>رفت از عالم برون آن شهریار نامدار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>آن که از قربانیانش بود آهوی حرم</p></div>
<div class="m2"><p>پنجه شاهین مرگ سنگدل کردش شکار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>کرد آخر از جهان با مرکب چوبین سفر</p></div>
<div class="m2"><p>آن که می شد لشکر عالم بر اسب او سوار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>دفتر عمرش مجزا شد ز دست انداز مرگ</p></div>
<div class="m2"><p>آن که می شد از خط او دیده ها عنبرنگار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>رفت در ابر کفن چون ماه و سر بیرون نکرد</p></div>
<div class="m2"><p>برق جولانی که در یک جا نمی بودش قرار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>زیر زلف شام پنهان گشت همچون آفتاب</p></div>
<div class="m2"><p>صبح سیمایی که بود آفاق ازو آیینه زار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>داغ جانسوز شهید کربلا را تازه کرد</p></div>
<div class="m2"><p>مرگ این شاه حسینی نسبت حیدر تبار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ورد عالم غیر افسوس و دریغ و آه نیست</p></div>
<div class="m2"><p>تا سفر کرد آن جهان جان سوی دارالقرار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>لوح خاک از جوی خود چون صفحه تقویم شد</p></div>
<div class="m2"><p>بس که شد چشم خلایق زین مصیبت اشکبار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>خون به جای آب می آمد برون از چشمه ها</p></div>
<div class="m2"><p>این مصیبت سایه می افکند اگر بر کوهسار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>رفت تا آن شاخ گل در نوبهار از بوستان</p></div>
<div class="m2"><p>دست افسوس آورد گلشن به جای برگ، بار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چون نگردد تلخ بر اولاد آدم زندگی؟</p></div>
<div class="m2"><p>شهریاری چون صفی الله گذشت از روزگار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>سازگار او نشد آب و هوای این جهان</p></div>
<div class="m2"><p>داشت دایم گوشه بیماریی چون چشم یار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چون سرشک عاشقان در هیچ جا لنگر نکرد</p></div>
<div class="m2"><p>بود در رفتن چو آه از جان عاشق بی قرار</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چون تقدس بود غالب بر مزاج اشرفش</p></div>
<div class="m2"><p>داشت دایم خاطرش از عالم خاکی غبار</p></div></div>