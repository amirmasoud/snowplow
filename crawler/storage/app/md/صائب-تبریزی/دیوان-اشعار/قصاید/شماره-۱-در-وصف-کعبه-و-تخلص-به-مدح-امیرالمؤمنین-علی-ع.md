---
title: >-
    شمارهٔ ۱ - در وصف کعبه و تخلص به مدح امیرالمؤمنین علی (ع)
---
# شمارهٔ ۱ - در وصف کعبه و تخلص به مدح امیرالمؤمنین علی (ع)

<div class="b" id="bn1"><div class="m1"><p>ای سواد عنبرین فامت سویدای زمین</p></div>
<div class="m2"><p>مغز خاک از نهکت مشکین لباست خوشه چین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>موجه ای از ریگ صحرایت صراط المستقیم</p></div>
<div class="m2"><p>رشته ای از تار و پود جامه ات حبل المتین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غنچه پژمرده ای از لاله زارت شمع طور</p></div>
<div class="m2"><p>قطره افسرده ای از زمزمت در ثمین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بیابان طلب یک العطش گوی تو خضر</p></div>
<div class="m2"><p>در حریم قدس یک پروانه ات روح الامین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مصرع برجسته ای دیوان موجودات را</p></div>
<div class="m2"><p>از حجر اینک نشان انتخابت بر جبین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میهمانداری به الوانهای نعمت خلق را</p></div>
<div class="m2"><p>چون خلیل الله داری هر طرف صد خوشه چین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طاق ابروی ترا تا دست قدرت نقش بست</p></div>
<div class="m2"><p>قامت افلاک خم شد، راست شد پشت زمین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مردم چشم جهان بین سپهر اخضری</p></div>
<div class="m2"><p>جای حیرت نیست گر باشد لباست عنبرین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شش جهت چون خانه زنبور پرغوغای توست</p></div>
<div class="m2"><p>کهکشان از نوشخند توست جوی انگبین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عالم اسباب را از طاق دل افکنده ای</p></div>
<div class="m2"><p>نیست نقش بوریا در خانه ات مسندنشین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا به کف نگرفته بود از سایه ات رطل گران</p></div>
<div class="m2"><p>در کشاکش بود از خمیازه رگهای زمین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با صفای جبهه صاف تو از کم مایگی</p></div>
<div class="m2"><p>چون دروغ راست مانندست صبح راستین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آب شوری در قدح داری و از جوش سخا</p></div>
<div class="m2"><p>می کنی تکلیف خلق اولین و آخرین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از ثبات مقدم خود عذرخواهی می کنی</p></div>
<div class="m2"><p>پای عصیان هر که را لغزید از اهل زمین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>روی عالم را ز برگ لاله داری سرختر</p></div>
<div class="m2"><p>گر چه خود چون داغ می پوشی لباس عنبرین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بوسه در یاقوت خوبان دارد آتش زیر پا</p></div>
<div class="m2"><p>بر امید آن که خدام ترا بوسد زمین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گرد فانوس تو گشتن کار هر پروانه نیست</p></div>
<div class="m2"><p>نقش دیوارست اینجا شهپر روح الامین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا ز دامنگیریت کوته نماند هیچ دست</p></div>
<div class="m2"><p>می کشی چون پرتو خورشید دامن بر زمین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر گنهکاری که زد بر دامن پاک تو دست</p></div>
<div class="m2"><p>گرد عصیان پا کردی از رخش با آستین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ساغر لبریز رحمت را تو زمزم کرده ای</p></div>
<div class="m2"><p>چون به رحمت ننگری در سینه های آتشین؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا به روی خاک تردامن نیفتد سایه ات</p></div>
<div class="m2"><p>پهن سازد هر سحر خورشید دامن بر زمین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا شبستان فنا جایی ناستد چون شرر</p></div>
<div class="m2"><p>گر به روی آتش دوزخ فشانی آستین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>انبیا چندین چه می کوشند در تعمیر تو؟</p></div>
<div class="m2"><p>گنج رحمت نیست گر در زیر دیوارت دفین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در هوای حسن شورانگیز آب زمزمت</p></div>
<div class="m2"><p>جمله از سر رفت دیگ مغزهای آتشین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نیستی گر مهردار رحمت پروردگار</p></div>
<div class="m2"><p>چون نگین بهر چه داری این سیاهی بر جبین؟</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هست اسماعیل یک قربانی لاغر ترا</p></div>
<div class="m2"><p>کز نم خونش نکردی لاله گون روی زمین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر زبان ناودانت چون قلم می داشت شق</p></div>
<div class="m2"><p>پاک می شد از غبار معصیت روی زمین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا در تکلیف بر روی جهان واکرده ای</p></div>
<div class="m2"><p>در پس درمانده است از شرم، فردوس برین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در حریم جنت آسای تو اهل دید را</p></div>
<div class="m2"><p>در نظر می آید از هر شمع جوی انگبین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ناودان گوهر افشانت ز رحمت آیه ای است</p></div>
<div class="m2"><p>از حریم لطف نازل گشته در شان زمین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر نه ای روشنگر آیینه دلها، چرا</p></div>
<div class="m2"><p>جامه و دست و رخت پیوسته باشد عنبرین؟</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ایمنند از آتش دوزخ پرستاران تو</p></div>
<div class="m2"><p>حق گزاری شیوه توست ای بهشت هشتمین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>غفلت و نسیان ندارد بر مقیمان تو دست</p></div>
<div class="m2"><p>برنچیند دانه ای بی ذکر مرغی از زمین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هیچ کس ناخوانده نتواند به بزمت آمدن</p></div>
<div class="m2"><p>چون در رحمت نداری گر چه دربان در کمین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>می زنی یک ماه دامن بر میان در عرض سال</p></div>
<div class="m2"><p>می دهی سامان کار اولین و آخرین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هیچ تعریفی ترا زین به نمی دانم که شد</p></div>
<div class="m2"><p>در تو پیدا گوهر پاک امیرالمؤمنین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بهترین خلق بعد از بهترین انبیا</p></div>
<div class="m2"><p>ابن عم مصطفی داماد خیرالمرسلین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا ابد چون طفل بی مادر به خاک افتاده بود</p></div>
<div class="m2"><p>ذوالفقار او نمی برید اگر ناف زمین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خانه زنبور دل بی شهد ایمان مانده بود</p></div>
<div class="m2"><p>گر نمی شد باعث تعمیر او یعسوب دین</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تا نگرداند نظر حیدر، نگردد آسمان</p></div>
<div class="m2"><p>تا نگوید یا علی، گردون نخیزد از زمین</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>در زمان رحمت سرشار عصیان سوز تو</p></div>
<div class="m2"><p>مد آهی می کشد گاهی کرام الکاتبین</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نقطه بسم اللهی فرقان موجودات را</p></div>
<div class="m2"><p>در سواد توست علم اولین و آخرین</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شهپر رحمت بود هر حرفی از نام علی</p></div>
<div class="m2"><p>این دو شهپر برد عیسی را به چرخ چارمین</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سرفراز از اول نام تو عرش ذوالجلال</p></div>
<div class="m2"><p>روشن از خورشید رویت نرگس عین الیقین</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چون لباس کعبه بر اندام بت، زیبنده نیست</p></div>
<div class="m2"><p>جز تو بر شخص دگر نام امیرالمؤمنین</p></div></div>