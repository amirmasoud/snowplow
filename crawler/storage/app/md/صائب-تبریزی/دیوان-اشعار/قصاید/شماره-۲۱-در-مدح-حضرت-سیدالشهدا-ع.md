---
title: >-
    شمارهٔ ۲۱ - در مدح حضرت سیدالشهداء (ع)
---
# شمارهٔ ۲۱ - در مدح حضرت سیدالشهداء (ع)

<div class="b" id="bn1"><div class="m1"><p>خاکیان را از فلک امید آسایش خطاست</p></div>
<div class="m2"><p>آسمان با این جلالت گوی چوگان قضاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرده خارست اگر دارد گلی این بوستان</p></div>
<div class="m2"><p>نوش این غمخانه را چاشنی زهر فناست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساحلی گر دارد این دریا لب گورست و بس</p></div>
<div class="m2"><p>هست اگر کامی درین ویرانه کام اژدهاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داغ ناسورست هست این خانه را گر روزنی</p></div>
<div class="m2"><p>آه جانسوزست اگر شمعی درین ماتم سراست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سختی دوران به ارباب سعادت می رسد</p></div>
<div class="m2"><p>استخوان از سفره این سنگدل رزق هماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست سالم دامن پاکان ز دست انداز او</p></div>
<div class="m2"><p>گرگ تهمت یوسف گل پیرهن را در قفاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سنگ می بارد به نخل میوه دار از شش جهت</p></div>
<div class="m2"><p>سرو از بی حاصلی پیوسته در نشو و نماست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قرص مهر و ماه گردون را کسی نشکسته است</p></div>
<div class="m2"><p>از دل خود روزی مهمان درین مهمانسراست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر زبانی کز فروغ صدق دارد روشنی</p></div>
<div class="m2"><p>زنده زیر خاک دایم چون چراغ آسیاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تیرباران قضا نازل به مردان می شود</p></div>
<div class="m2"><p>از نیستان شیر را آرامگاه و متکاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هست اگر آسایشی در زیر تیغ و خنجرست</p></div>
<div class="m2"><p>دیده حیران قربانی بر این معنی گواست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با قضای آسمان سودی ندارد احتیاط</p></div>
<div class="m2"><p>بیشتر افتد به چه هر کس درین ره با عصاست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کی مسلم می گذارد زندگان را روزگار؟</p></div>
<div class="m2"><p>کز سیه روزان این ماتم سرا آب بقاست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیست غیر از نامرادی در جهان خاک مراد</p></div>
<div class="m2"><p>مدعای هر دو عالم در دل بی مدعاست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عارفانی را که سر در جیب فکرت برده اند</p></div>
<div class="m2"><p>چون ز ره صد چشم عبرت بین نهان زیر قباست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>لب گشودن می شود موج خطر را بال و پر</p></div>
<div class="m2"><p>لنگر این بحر پرآشوب، تسلیم و رضاست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زیر گردون ما ز غفلت شادمانی می کنیم</p></div>
<div class="m2"><p>ورنه گندم سینه چاک از بیم زخم آسیاست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر گدا چشمی نباشد مستحق این نوال</p></div>
<div class="m2"><p>درد و محنت نزل خاص انبیا و اولیاست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زخم دندان ندامت می رسد سبابه را</p></div>
<div class="m2"><p>از میان جمله انگشتان، که ایمان را گواست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در خور ظرف است اینجا هر دهان را لقمه ای</p></div>
<div class="m2"><p>ضربت تیغ شهادت طعمه شیر خداست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نیست هر نخجیر لاغر لایق فتراک عشق</p></div>
<div class="m2"><p>آل تمغای شهادت خاصه آل عباست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کی دلش سوزد به داغ دردمندان دگر؟</p></div>
<div class="m2"><p>چرخ کز لب تشنگان او شهید کربلاست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آنچه از ظلم و ستم بر قرة العین رسول</p></div>
<div class="m2"><p>رفت از سنگین دلان، بر صدق این معنی گواست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مظهر انوار ربانی، حسین بن علی</p></div>
<div class="m2"><p>آن که خاک آستانش دردمندان را شفاست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ابر رحمت سایبان قبه پر نور او</p></div>
<div class="m2"><p>روضه اش را از پر و بال ملایک بوریاست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دست خالی برنمی گردد دعا از روضه اش</p></div>
<div class="m2"><p>سایلان را آستانش کعبه حاجت رواست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در رجب هر کس موفق شد به طوف مرقدش</p></div>
<div class="m2"><p>بی تردد جای او در مقعد صدق خداست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در ره او زایران را هر چه از نقد حیات</p></div>
<div class="m2"><p>صرف گردد، با وجود صرف گردیدن بجاست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون فتاده است این مصیبت زائران را عمر کاه</p></div>
<div class="m2"><p>در تلافی زان طوافش روح بخش و جانفزاست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نیست اهل بیت را رنگین تر از وی مصرعی</p></div>
<div class="m2"><p>گر بود بر صدر نه معصوم جای او، بجاست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کور اگر روشن شود در روضه اش نبود عجب</p></div>
<div class="m2"><p>کان حریم خاص مالامال از نور خداست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>با لب خشک از جهان تا رفت آن سلطان دین</p></div>
<div class="m2"><p>آب را خاک مذلت در دهان زین ماجراست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زین مصیبت می کند خون گریه چرخ سنگدل</p></div>
<div class="m2"><p>این شفق نبود که صبح و شام ظاهر برسماست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>عقده ها از ماتمش روی زمین را در دل است</p></div>
<div class="m2"><p>دانه تسبیح، اشک خاک پاک کربلاست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در ره دین هر که جان خویش را سازد فدا</p></div>
<div class="m2"><p>در گلوی تشنه او آب تیغ آب بقاست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تا بدخشان شد جگرگاه زمین از خون او</p></div>
<div class="m2"><p>هر گیاهی کز زمین سر برزند لعلی قباست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نیست یک دل کز وقوع این مصیبت داغ نیست</p></div>
<div class="m2"><p>گریه فرض عین هفتاد و دو ملت زین عزاست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>می دهد غسل زیارت خلق را در آب چشم</p></div>
<div class="m2"><p>این چنین خاک جگرسوزی ز مظلومان کراست؟</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بهر زوارش که می آیند با چندین امید</p></div>
<div class="m2"><p>هر کف خاک از زمین کربلا دست دعاست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مردگان با اسب چوبین قطع این ره می کنند</p></div>
<div class="m2"><p>زندگان را طاقت دوری ز درگاهش کجاست؟</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از سیاهی داغ این ماتم نمی آید برون</p></div>
<div class="m2"><p>این مصیبت هست بر جا تا بجا ارض و سماست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از جگرها می کشد این نخل ماتم آب خویش</p></div>
<div class="m2"><p>تا قیامت زین سبب پیوسته در نشو و نماست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گر چه از حجت بود حلم الهی بی نیاز</p></div>
<div class="m2"><p>این مصیبت حجت حلم گرانسنگ خداست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>قطره اشکی که آید در عزای او به چشم</p></div>
<div class="m2"><p>گوشوار عرش را از پاکی گوهر سزاست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زائران را چون نسازد پاک از گرد گناه؟</p></div>
<div class="m2"><p>شهپر روح الامین جاروب این جنت سراست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سبحه ای کز خاک پاک کربلا سامان دهند</p></div>
<div class="m2"><p>بی تذکر بر زبان رشته اش ذکر خداست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چند روزی بود اگر مهر سلیمان معتبر</p></div>
<div class="m2"><p>تا قیامت سجده گاه خلق مهر کربلاست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>خاک این در شو که پیش همت دریا دلش</p></div>
<div class="m2"><p>زایران را پاک کردن از گنه کمتر سخاست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مغز ایمان تازه می گردد ز بوی خاک او</p></div>
<div class="m2"><p>این شمیم جانفزا با مشک و با عنبر کجاست؟</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>زیر سقف آسمان، خاکی که از روی نیاز</p></div>
<div class="m2"><p>می توان مرد از برایش، خاک پاک کربلاست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تا شد از قهر الهی طعمه دوزخ یزید</p></div>
<div class="m2"><p>نعره هل من مزید از آتش دوزخ نخاست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تکیه گاهش بود از دوش رسول هاشمی</p></div>
<div class="m2"><p>آن سری کز تیغ بیداد یزید از تن جداست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>آن که می شد پیکرش از برگ گل نیلوفری</p></div>
<div class="m2"><p>چاک چاک امروز مانند گل از تیغ جفاست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>آن که بود آرامگاهش از کنار مصطفی</p></div>
<div class="m2"><p>پیکر سیمین او افتاده زیر دست و پاست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چرخ از انجم در عزایش دامن پر اشک شد</p></div>
<div class="m2"><p>تا به دامان جزا گر ابر خون گرید رواست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>مدحش از ما عاجزان صائب بود ترک ادب</p></div>
<div class="m2"><p>آن که ممدوح خدا و مصطفی و مرتضاست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>سال تاریخ مدیح این امام المتقین</p></div>
<div class="m2"><p>چون نهد «جان » سر به پایش «مدح شاه کربلاست »</p></div></div>