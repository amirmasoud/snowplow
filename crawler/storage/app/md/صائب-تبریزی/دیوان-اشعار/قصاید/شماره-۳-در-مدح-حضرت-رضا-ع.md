---
title: >-
    شمارهٔ ۳ - در مدح حضرت رضا(ع)
---
# شمارهٔ ۳ - در مدح حضرت رضا(ع)

<div class="b" id="bn1"><div class="m1"><p>این حریم کیست کز جوش ملایک روزبار</p></div>
<div class="m2"><p>نیست در وی پرتو خورشید را راه گذار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کیست یارب شمع این فانوس کز نظاره اش</p></div>
<div class="m2"><p>آب می گردد به گرد دیده ها پروانه وار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این شبستان خوابگاه کیست کز موج صفا</p></div>
<div class="m2"><p>دود شمعش می رباید دل چو زلف مشکبار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یارب این خاک گرامی مغرب خورشید کیست</p></div>
<div class="m2"><p>کز فروغش می شود چشم ملایک اشکبار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این مقام کیست کز هر بیضه قندیل او</p></div>
<div class="m2"><p>سر برآرد طایری چون جبرئیل نامدار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کیست یارب در پس این پرده کز انفاس خوش</p></div>
<div class="m2"><p>می برد از چشم ها چون بوی پیراهن غبار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این مزار کیست یارب کز هجوم زایران</p></div>
<div class="m2"><p>غنچه می گردد در او بال ملایک در مطار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جلوه گاه کیست یارب این زمین مشک خیز</p></div>
<div class="m2"><p>کز شمیمش می خورد خون ناف آهوی تتار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ساکن این مهد زرین کیست کز شوق لبش</p></div>
<div class="m2"><p>شیر می جوشد ز پستان صبح را بی اختیار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این همایون بقعه یارب از کدامین سرورست</p></div>
<div class="m2"><p>کز شرافت می زند پهلو به عرش کردگار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سرور دنیا و دین سلطان علی موسی الرضا</p></div>
<div class="m2"><p>آن که دارد همچو دل در سینه عالم قرار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جدول بحر رسالت کز وجود فایضش</p></div>
<div class="m2"><p>خاک پاک طوس شد از بحر رحمت مایه دار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گوهر بحر ولایت کز ضمیر انورش</p></div>
<div class="m2"><p>هر چه در نه پرده پنهان بود گردید آشکار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن که گر اوراق فضلش را به روی هم نهند</p></div>
<div class="m2"><p>چون لباس غنچه گردد چاک این نیلی حصار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آسمان از باغ قدرش غنچه نیلوفری است</p></div>
<div class="m2"><p>یک گل رعناست از گلزار او لیل و نهار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مهره مومی است در سرپنجه او آسمان</p></div>
<div class="m2"><p>می دهد او را به هر شکلی که می خواهد قرار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حاصل دریا و کان را گر به محتاجی دهد</p></div>
<div class="m2"><p>شق شود از جوش گوهر آسمان ها چون انار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>می شود گوهر جواهر سرمه در جیب صدف</p></div>
<div class="m2"><p>در دل دریا شکوه او نماید گر گذار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>راز سرپوشیدگان غیب بر صحرا فتد</p></div>
<div class="m2"><p>پرده بردارد اگر از روی خورشید اشتهار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آنچه تا روز جزا در پرده شب مختفی است</p></div>
<div class="m2"><p>پیش عالم او بود چون روز روشن آشکار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر سپر از موم باشد در دیار حفظ او</p></div>
<div class="m2"><p>تیغ خورشید قیامت را کند دندانه دار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بوی گل در غنچه از خجلت حصاری گشته است</p></div>
<div class="m2"><p>تا نسیم خلق او پیچیده در مغز بهار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تیغ او چون سربرآرد از نیام مشکفام</p></div>
<div class="m2"><p>می شود صبح قیامت از دل شب آشکار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آن که تیغ کهکشان در قبضه فرمان اوست</p></div>
<div class="m2"><p>چون تواند خصم با او تیغ شد در کارزار؟</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تیغ جوهردار او را گو به چشم خود ببین</p></div>
<div class="m2"><p>آن که گوید برنمی خیزد نهنگ از چشمه سار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون تواند خصم رو به باز با او پنجه کرد؟</p></div>
<div class="m2"><p>آن که شیر پرده را فرمانش آرد در شکار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همچو معنی در ضمیر لفظ پنهان گشته است</p></div>
<div class="m2"><p>در رضای او رضای حضرت پروردگار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شکوه غربت غریبان را ز خاطر بار بست</p></div>
<div class="m2"><p>در غریبی تا اقامت کرد آن کوه وقار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زهر در انگور تا دادند او را دشمنان</p></div>
<div class="m2"><p>ماند چشم تاک تا روز قیامت اشکبار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تاک را چون مار هر جا سبز شد سر می زنند</p></div>
<div class="m2"><p>تا شد از انگور کام شکرینش زهربار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>وه چه گویم از صفای روضه پرنور او</p></div>
<div class="m2"><p>کز فروغش کور روشن می شود بی اختیار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گوشوار خود به رشوت می دهد عرش برین</p></div>
<div class="m2"><p>تا مگر یابد در او یک لحظه چون قندیل بار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>می توان خواند از صفای کاشی دیوار او</p></div>
<div class="m2"><p>عکس خط سرنوشت خلق را شبهای تار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>روضه پر نور او را زینتی در کار نیست</p></div>
<div class="m2"><p>پنجه خورشید مستغنی است از نقش و نگار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خیره می شد دیده ها از دیدنش چون آفتاب</p></div>
<div class="m2"><p>گر نمی شد قبه نورانی او زرنگار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>می توان دیدن چو روی دلبران از زیر زلف</p></div>
<div class="m2"><p>از محجرهای او خلد برین را آشکار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همچو اوراق خزان بال ملایک ریخته است</p></div>
<div class="m2"><p>هر کجا پا می نهی در روضه آن شهریار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>می توان رفتن به آسانی به بال قدسیان</p></div>
<div class="m2"><p>از حریم روضه او تا به عرش کردگار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>قلزم رحمت حبابی چند بیرون داده است</p></div>
<div class="m2"><p>نیست قندیل این که می بینی به سقفش بی شمار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>زیر بال قدسیان چون بیضه پنهان گشته است</p></div>
<div class="m2"><p>قبه نورانی آن سرو عرش اقتدار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از محجرهای زرینش که دام رحمت است</p></div>
<div class="m2"><p>می توان آمرزش جاوید را کردن شکار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تا غبار آستانش جلوه گر شد، حوریان</p></div>
<div class="m2"><p>از عبیر خلد افشانند زلف مشکبار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هر شب از گردون ز شوق سجده خاک درش</p></div>
<div class="m2"><p>قدسیان ریزند چون برگ خزان از شاخسار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>کشتی نوح است صندوقش که از طوفان غم</p></div>
<div class="m2"><p>هر که در وی دست زد آمد سلامت بر کنار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>خادمان صندوق پوش مرقدش می ساختند</p></div>
<div class="m2"><p>گر نمی بود اطلس گردون ز انجم داغدار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>با کمال بی نیازی مرقد زرین او</p></div>
<div class="m2"><p>می کند با دام سیمین مرغ دلها را شکار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>اشک شمع روضه او را ز دست یکدگر</p></div>
<div class="m2"><p>حور و غلمان می ربایند از برای گوشوار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نقد می سازد بهشت نسیه را بر زایران</p></div>
<div class="m2"><p>روضه جنت مثالش در دل شبهای تار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>می توان خواند از جبین رحل مصحف های او</p></div>
<div class="m2"><p>رازهای غیب را چون لوح محفوظ آشکار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بس که قرآن در حریم او تلاوت می کنند</p></div>
<div class="m2"><p>صفحه بال ملایک می شود قرآن نگار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هر شب از جوش ملک در روضه پرنور او</p></div>
<div class="m2"><p>شمعها انگشت بردارند بهر زینهار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تا دم صبح از فروغ قبه زرین او</p></div>
<div class="m2"><p>آب می گردد به چشم اختران بی اختیار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>هر شبی صد بار از موج صفا در روضه اش</p></div>
<div class="m2"><p>در غلط از صبح افتد زاهد شب زنده دار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>حسن خلقش دل نمی بخشید اگر زوار را</p></div>
<div class="m2"><p>آب می شد از شکوهش زهره ها بی اختیار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>اختیار خدمت خدام این در می کند</p></div>
<div class="m2"><p>هر که می خواهد شود مخدوم اهل روزگار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>از صفای جبهه خدام او دلهای شب</p></div>
<div class="m2"><p>می توان کردن مصحف خط غبار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>از سر گلدسته اش چون نخل ایمن تا سحر</p></div>
<div class="m2"><p>بر خداجویان شود برق تجلی آشکار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>از نوای عندلیبان سر گلدسته اش</p></div>
<div class="m2"><p>قدسیان در وجد و حال آیند ازین نیلی حصار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>داغ دارد چلچراغ او درخت طور را</p></div>
<div class="m2"><p>این چنین نخلی ندارد یاد چشم روزگار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>از سر دربانی فردوس، رضوان بگذرد</p></div>
<div class="m2"><p>گر بداند می کنندش کفشدار این مزار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>خضر تردستی که میراب زلال زندگی است</p></div>
<div class="m2"><p>می کند سقایی این آستان را اختیار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>می فتد در دست و پای خادمانش آفتاب</p></div>
<div class="m2"><p>تا مگر چون عودسوز آنجا تواند یافت بار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>مطلب کونین آنجا بر سر هم ریخته است</p></div>
<div class="m2"><p>چون برآید ناامید از حضرتش امیدوار؟</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>روز محشر سر برآرد از گریبان بهشت</p></div>
<div class="m2"><p>هر که اینجا طوق بر گردن گذارد بنده وار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>می کند با اسب چوب از آتش دوزخ گذر</p></div>
<div class="m2"><p>هر که را تابوت گردانند گرد این مزار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چشمه کوثر به استقبالش آید روز حشر</p></div>
<div class="m2"><p>هر که را زین آستان بر جبهه بنشیند غبار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>از فشار قبر تا روز جزا آسوده است</p></div>
<div class="m2"><p>هر که اینجا از هجوم زایران یابد فشار</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>می رود فردا سراسر در خیابان بهشت</p></div>
<div class="m2"><p>هر که را امروز افتد در خیابانش گذار</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>هر که باشد در شمار زایران درگهش</p></div>
<div class="m2"><p>می تواند شد شفیع عالمی روز شمار</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>آتش دوزخ نمی گردد به گردش روز حشر</p></div>
<div class="m2"><p>از سر اخلاص هر کس گشت گرد این مزار</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بر جبین هر که باشد سکه اخلاص او</p></div>
<div class="m2"><p>از لحد بیرون خرامد چون زر کامل عیار</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>می شود همسایه دیوار بر دیوار خلد</p></div>
<div class="m2"><p>در جوار روضه او هر که را باشد مزار</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>هر که شمع نیمسوزی برد با خود زین حریم</p></div>
<div class="m2"><p>ایمن از تاریکی قبرست تا روز شمار</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>می گشاید چشم زیر خاک بر روی بهشت</p></div>
<div class="m2"><p>هر که از خاک درش با خود برد یک سرمه وار</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بر جبین هر که بنشیند غبار درگهش</p></div>
<div class="m2"><p>داخل جنت شود از گرد ره بی انتظار</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>هر که را چون مهر در پا خار راهش بشکند</p></div>
<div class="m2"><p>سوزن عیسی برون آرد ز پایش نوک خار</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>آن که باشد یک طواف مرقدش هفتاد حج</p></div>
<div class="m2"><p>فکر صائب چون تواند کرد فضلش را شمار؟</p></div></div>