---
title: >-
    شمارهٔ ۳۶ - در توصیف صفی آباد
---
# شمارهٔ ۳۶ - در توصیف صفی آباد

<div class="b" id="bn1"><div class="m1"><p>می می چکد از آب و هوای صفی آباد</p></div>
<div class="m2"><p>جامی است پر از باده بنای صفی آباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اشرف که بهشت است ازو در عرق شرم</p></div>
<div class="m2"><p>بالا نکند سر ز هوای صفی آباد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چند به اشرف نرسد هیچ مقامی</p></div>
<div class="m2"><p>بالاتر ازان است صفای صفی آباد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>الحق که نگین خانه معموره اشرف</p></div>
<div class="m2"><p>می خواست نگینی چو بنای صفی آباد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اشرف که نکردی به ته پا نگه از ناز</p></div>
<div class="m2"><p>چون سایه فتاده است به پای صفی آباد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیمار شود از دم جان بخش مسیحا</p></div>
<div class="m2"><p>سروی که برآید به هوای صفی آباد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی پرده شود راز نهانخانه دلها</p></div>
<div class="m2"><p>از مرمر اندیشه نمای صفی آباد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون پنجه خورشید کند خیره نظر را</p></div>
<div class="m2"><p>دستی که شود چهره گشای صفی آباد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر چند به معراج رسانند سخن را</p></div>
<div class="m2"><p>بالاتر ازان است بنای صفی آباد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از هیچ طرف مانع نظاره ندارد</p></div>
<div class="m2"><p>چون عالم اندیشه، فضای صفی آباد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پیمانه ز خود باده گلرنگ برآرد</p></div>
<div class="m2"><p>در انجمن نشأه فزای صفی آباد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فواره دریای گهرخیز معانی است</p></div>
<div class="m2"><p>هر خامه که تر شد به ثنای صفی آباد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شد مشرق پروین، نظر شوخ کواکب</p></div>
<div class="m2"><p>از شمسه خورشید لقای صفی آباد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از تخت جم و چتر پریزاد دهد یاد</p></div>
<div class="m2"><p>اشرف که فتاده است به پای صفی آباد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ممتاز به خوبی است ز مجموعه اشرف</p></div>
<div class="m2"><p>چون مصرع برجسته، بنای صفی آباد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از سرو گذشته است کله گوشه مینا</p></div>
<div class="m2"><p>از تربیت آب و هوای صفی آباد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اشرف به ته پای پریزاد کند خواب</p></div>
<div class="m2"><p>از ابر پریشان فضای صفی آباد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون غنچه گل باز شود غنچه پیکان</p></div>
<div class="m2"><p>در بوم و بر عقده گشای صفی آباد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>داغ سیهی می برد از نامه اعمال</p></div>
<div class="m2"><p>از بس که تر افتاده هوای صفی آباد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون سایه شد اشرف یکی از خاک نشینان</p></div>
<div class="m2"><p>روزی که قد افراخت لوای صفی آباد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بر سینه زند سنگ ازو شیشه تقوی</p></div>
<div class="m2"><p>هر چند لطیف است هوای صفی آباد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>طاوس بهشت است که از بال زند چتر</p></div>
<div class="m2"><p>تالار زراندود بنای صفی آباد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زاهد که ز دستش نچکد آب ز خشکی</p></div>
<div class="m2"><p>مستانه دهد جان به لقای صفی آباد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اشرف که در آراستگی باغ بهشت است</p></div>
<div class="m2"><p>یک گوشه ندارد به صفای صفی آباد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون روی عرقناک نماید ز ته زلف</p></div>
<div class="m2"><p>در زیر رگ ابر، لقای صفی آباد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کشمیر که خال رخ هندست ز سبزی</p></div>
<div class="m2"><p>حاشا که شود روی نمای صفی آباد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از فیض قدوم شه دین، ثانی عباس</p></div>
<div class="m2"><p>برخلد کند ناز، بنای صفی آباد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جایی که زبان قاصر از اوصاف بهشت است</p></div>
<div class="m2"><p>صائب چه توان گفت صفای صفی آباد؟</p></div></div>