---
title: >-
    شمارهٔ ۱۹ - در مدح شاه عباس دوم
---
# شمارهٔ ۱۹ - در مدح شاه عباس دوم

<div class="b" id="bn1"><div class="m1"><p>هوا را کند پر ز اختر شکوفه</p></div>
<div class="m2"><p>زمین را کند بحر گوهر شکوفه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز پیراهن یوسفی مغزها را</p></div>
<div class="m2"><p>به هر جلوه سازد معطر شکوفه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کف تازه رویی است از بحر رحمت</p></div>
<div class="m2"><p>که باشد به گوهر برابر شکوفه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز گلزار غیبی به ما دور گردان</p></div>
<div class="m2"><p>بود نامه و نامه آور شکوفه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به صنع الهی است هر تیره دل را</p></div>
<div class="m2"><p>به صد شمع کافور رهبر شکوفه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز چتر پریزاد و تخت سلیمان</p></div>
<div class="m2"><p>دهد یاد بر شاخ اخضر شکوفه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز هر غنچه چون محمل لیلی آرد</p></div>
<div class="m2"><p>برون ماه سیمای دیگر شکوفه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در آیینه آب از عکس سازد</p></div>
<div class="m2"><p>پری را به شیشه مصور شکوفه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز احیای اشجار روشندلان را</p></div>
<div class="m2"><p>دهد یاد از صبح محشر شکوفه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو بال پری بر بساط سلیمان</p></div>
<div class="m2"><p>بر آفاق شد سایه گستر شکوفه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نهان ساخت چون رشته در عقد گوهر</p></div>
<div class="m2"><p>رگ شاخها را سراسر شکوفه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ندیدی به وادی اگر محرمان را</p></div>
<div class="m2"><p>ببین پهن در خاک اغبر شکوفه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو شیر از رگ شاخها زهردی را</p></div>
<div class="m2"><p>به نرمی برآورد یکسر شکوفه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شب و روز را کرد با هم برابر</p></div>
<div class="m2"><p>ز نور جبین منور شکوفه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ازان همچو صبح است خندان و روشن</p></div>
<div class="m2"><p>که خورشید گل راست خاور شکوفه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو راهی که از برف پوشیده گردد</p></div>
<div class="m2"><p>نهان شد چنان شاخ ها در شکوفه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مگر نامه عاشق بی قرارست؟</p></div>
<div class="m2"><p>که گیرد هوا چون کبوتر شکوفه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز افشاندن فلس، آب روان را</p></div>
<div class="m2"><p>چو ماهی کند سیم پیکر شکوفه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز دستار آشفته اش می کند گل</p></div>
<div class="m2"><p>که در پرده خورده است ساغر شکوفه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هوای که برده است از دل قرارش؟</p></div>
<div class="m2"><p>که در بیضه آرد برون پر شکوفه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز حفظش به صد دست شاخ است عاجز</p></div>
<div class="m2"><p>ز بس شیر مست است دیگر شکوفه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>توانگر کند مفلسان طرب را</p></div>
<div class="m2"><p>براتی است از نقد خوشتر شکوفه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بگیر از گلستان برات نشاطی</p></div>
<div class="m2"><p>نبسته است چندان که دفتر شکوفه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز دست گهر ریز هر کف زمین را</p></div>
<div class="m2"><p>کند چون صدف پر ز گوهر شکوفه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز آب گهر خاک سیراب گردد</p></div>
<div class="m2"><p>چنین گر کند خنده تر شکوفه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نقاب لطیفی است کز خوش قماشی</p></div>
<div class="m2"><p>شود چهره با روی دلبر شکوفه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نماند نهان حسن در زیر چادر</p></div>
<div class="m2"><p>به یک جانب انداخت معجر شکوفه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شود خون به تدریج شیر، از چه رو شد</p></div>
<div class="m2"><p>بدل با گل و لاله یکسر شکوفه؟</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کند شمع کافور در روز روشن</p></div>
<div class="m2"><p>ز سیم است از بس توانگر شکوفه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز کم فرصتی های فصل بهاران</p></div>
<div class="m2"><p>بود بر جناح سفر هر شکوفه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گشود از دل خاکیان عقده ها را</p></div>
<div class="m2"><p>به دندان چون عقد گوهر شکوفه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز آیینه گیرند اگر پشت در زر</p></div>
<div class="m2"><p>گرفت آب را روی در زر شکوفه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو شیری که از مهر فرزند زاید</p></div>
<div class="m2"><p>زند جوش زان گونه از بر شکوفه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ازان خواب فصل بهارست شیرین</p></div>
<div class="m2"><p>که جامی است پر شیر و شکر شکوفه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کند بر عصا تکیه در عهد طفلی</p></div>
<div class="m2"><p>ز مستی چو پیر معمر شکوفه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>زمین را لباس و هوا راست معجر</p></div>
<div class="m2"><p>کتان است و مهتاب انور شکوفه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بلورین شود ساق سرو و صنوبر</p></div>
<div class="m2"><p>زند این چنین غوطه گر در شکوفه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شود شاخها سر به سر سیم ساعد</p></div>
<div class="m2"><p>کند باغ را چون سمنبر شکوفه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>که دیده قلم کاغذ از خود برآرد؟</p></div>
<div class="m2"><p>به هر شاخ بنگر مصور شکوفه</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو مریم که عیسی بود در کنارش</p></div>
<div class="m2"><p>گرفته چنان میوه در بر شکوفه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز بس چرب نرمی، به خاکی نهادان</p></div>
<div class="m2"><p>گواراست چون شیر مادر شکوفه</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو صوفی نهان در ته خرقه دارد</p></div>
<div class="m2"><p>ز هر برگ، مینای اخضر شکوفه</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو شیری کز انگشت اطفال زاید</p></div>
<div class="m2"><p>برآرد ز شاخ آنچنان سر شکوفه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ازان در نظرهاست شیرین که دارد</p></div>
<div class="m2"><p>ز هر غنچه ای تنگ شکر شکوفه</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ندیدی بر آیینه سیماب لرزان</p></div>
<div class="m2"><p>به هر صفحه آب بنگر شکوفه</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>توان یافت فیض صبوحی دل شب</p></div>
<div class="m2"><p>ز بس کرد شب را منور شکوفه</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>توان یافت فیض صبوحی دل شب</p></div>
<div class="m2"><p>ز بس کرد شب را منور شکوفه</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گرفته است در نقره خام یکسر</p></div>
<div class="m2"><p>زمین را چو مهتاب انور شکوفه</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>اگر سیر مهتاب در روز خواهی</p></div>
<div class="m2"><p>گذر کن به بستان و بنگر شکوفه</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز خون جام اهل نظر نیست خالی</p></div>
<div class="m2"><p>که بادام را باشد احمر شکوفه</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به لوح زمین می کند نقطه ریزی</p></div>
<div class="m2"><p>که از فال نیکو خورد بر شکوفه</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چه تقصیر ازو گشت صادر چو آدم؟</p></div>
<div class="m2"><p>که عریان شد از حلیه یکسر شکوفه</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>پراندند بهر چه ناخن به چوبش؟</p></div>
<div class="m2"><p>اگر نیست نقدش مزور شکوفه</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>شکستند ازان بیضه ها در کلاهش</p></div>
<div class="m2"><p>که نخوت به سر داشت از زر شکوفه</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>نمی گشت بازیچه هر نسیمی</p></div>
<div class="m2"><p>اگر مغز می داشت در سر شکوفه</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>سبکسار و پوچ است، ازان هر زمانی</p></div>
<div class="m2"><p>زند دست در شاخ دیگر شکوفه</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چو پیر خرابات از تازه رویی</p></div>
<div class="m2"><p>کند ملک دلها مسخر شکوفه</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>کم از کهکشان نیست هر کوچه باغی</p></div>
<div class="m2"><p>ز بس ریخت بر خاک اختر شکوفه</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>به هر جا رسی می توان واکشیدن</p></div>
<div class="m2"><p>که شد بستر و بالش پر شکوفه</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ندیدی اگر روز روشن ستاره</p></div>
<div class="m2"><p>فروزان ز هر شاخ بنگر شکوفه</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بیفشان زر و سیم کز باد دستی</p></div>
<div class="m2"><p>برومند گردید از بر شکوفه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>به ریزش ز اقران سرآمد توان شد</p></div>
<div class="m2"><p>ازان شد بر اشجار سرور شکوفه</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>تو هم شیشه را پنبه بردار از سر</p></div>
<div class="m2"><p>چو ریزان شد از شاخ اخضر شکوفه</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>گرو کن به می هر چه داری ز پوشش</p></div>
<div class="m2"><p>چو انداخت دستار از سر شکوفه</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>در این موسم از کشتی باده مگذر</p></div>
<div class="m2"><p>که سامان دهد بادبان هر شکوفه</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چو سیماب کز شعله گردد سبکپا</p></div>
<div class="m2"><p>شد از آتش گل سبک پر شکوفه</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>به ناخن خراشد زمین چمن را</p></div>
<div class="m2"><p>ز شرم نثار محقر شکوفه</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چو عیسی به گهواره گردید گویا</p></div>
<div class="m2"><p>به مدح شه دادگستر شکوفه</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بهار جهان، شاه عباس ثانی</p></div>
<div class="m2"><p>که بر نام او می زند زر شکوفه</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چنان آرمیده است عالم به عهدش</p></div>
<div class="m2"><p>که بر خود نلرزد ز صرصر شکوفه</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>زمین عنبر تر شد از بوی خلقش</p></div>
<div class="m2"><p>بهاری است زان عنبر تر شکوفه</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>به باغی که افتد به دولت گذارش</p></div>
<div class="m2"><p>شود اختر سعد یکسر شکوفه</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>هوا مشکبو گردد از عطسه گل</p></div>
<div class="m2"><p>شود گر ز خلقش معطر شکوفه</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>شود عاجز از ثبت یکروزه جودش</p></div>
<div class="m2"><p>شجر گر شود کلک و دفتر شکوفه</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ز حفظش به بال و پر کاغذ آید</p></div>
<div class="m2"><p>به ساحل ز دریای آذر شکوفه</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ز صبح جبینش بود فتح لامع</p></div>
<div class="m2"><p>ز میوه بود مژده آور شکوفه</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بود فتح ها در لوای سفیدش</p></div>
<div class="m2"><p>چو رنگین ثمرها نهان در شکوفه</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>زمین بوس شه می کند هر بهاری</p></div>
<div class="m2"><p>ازان رو بود نیک اختر شکوفه</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>نگه دار سررشته حرف صائب</p></div>
<div class="m2"><p>اگر چه بود در و گوهر شکوفه</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>سخن مختصر ساز، هر چند گردد</p></div>
<div class="m2"><p>ز تکرار قند مکرر شکوفه</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>مکن دست کوته ز دامن دعا را</p></div>
<div class="m2"><p>بود در گذر تا چو اختر شکوفه</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>همی تا ز تأثیر باد بهاران</p></div>
<div class="m2"><p>شود از درختان مصور شکوفه</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>نهال برومند اقبال او را</p></div>
<div class="m2"><p>ثمر کام دل باد و گوهر شکوفه</p></div></div>