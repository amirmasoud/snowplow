---
title: >-
    شمارهٔ ۸ - در مدح شاه عباس دوم
---
# شمارهٔ ۸ - در مدح شاه عباس دوم

<div class="b" id="bn1"><div class="m1"><p>ای زمان دلگشایت نوبهار روزگار</p></div>
<div class="m2"><p>صبح نوروز از جبین بخت سبزت آشکار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طینت پاک تو از خاک شریف بوتراب</p></div>
<div class="m2"><p>گوهر تیغ تو از صلب متین ذوالفقار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صولت شیر خدا از بازوی اقبال تو</p></div>
<div class="m2"><p>می شود چون نور خورشید از مه نو آشکار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفتاب سایه پرور را تماشا می کند</p></div>
<div class="m2"><p>هر که می بیند ترا در سایه پروردگار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا به لوح آفرینش نقش ایجاد تو بست</p></div>
<div class="m2"><p>بوسه زد بر دست خود کلک قضا بی اختیار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرشد کامل تویی سجاده ارشاد را</p></div>
<div class="m2"><p>تا شود نور ظهور صاحب الامر آشکار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه بر فرمانروایان جهان فرماندهی</p></div>
<div class="m2"><p>سر نمی پیچی ز فرمان خدا در هیچ کار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دین و دولت را تویی فرمانروای راستین</p></div>
<div class="m2"><p>گر چه در روی زمین هستند شاهان بی شمار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همچو سبابه کز انگشتان شهادت حق اوست</p></div>
<div class="m2"><p>دین حق قایم به توست از خسروان روزگار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از رسوخ اعتقادات آسمان بنیاد شد</p></div>
<div class="m2"><p>چون بروج آسمانی مذهب هشت و چهار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیضه اسلام از سنگ حوادث ایمن است</p></div>
<div class="m2"><p>عصمت ذات تو تا شد آفرینش را حصار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در حسب ممتازی از فرمانروایان جهان</p></div>
<div class="m2"><p>در نسب داری شرف بر خسروان نامدار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پادشاهان دگر دارند تاجی سر به سر</p></div>
<div class="m2"><p>جز تو از شاهان که دارد بندگان تاجدار؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سر به سر پاکیزه اخلاقند نزدیکان تو</p></div>
<div class="m2"><p>در صفا و لطف رنگ چشمه دارد جویبار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در جوانی یافتی دولت ز شاه نوجوان</p></div>
<div class="m2"><p>زود خواهی شد به کام دل ز دولت کامکار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زنده کردی نام جد سامی خود را ز عدل</p></div>
<div class="m2"><p>چون تو فرزندی ندارد یاد دور روزگار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شمع بالین مزارش عمر جاویدان بود</p></div>
<div class="m2"><p>شهریاری را که باشد چون تو شاهی یادگار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>داری اخلاق صفی با شوکت عباس شاه</p></div>
<div class="m2"><p>دیده بد دور بادا از تو ای عالی تبار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر چه باید با خود آورده است ذات کاملت</p></div>
<div class="m2"><p>بی نیاز از مایه دریاست در شاهوار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نیست صیقل احتیاج آیینه خورشید را</p></div>
<div class="m2"><p>جوهر ذات تو مستغنی است از آموزگار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سایه چتر تو تا افتاد بر روی زمین</p></div>
<div class="m2"><p>آسمان دیگر از روی زمین شد آشکار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گرچه شمشیر تو نوخط است از جوهر هنوز</p></div>
<div class="m2"><p>می نویسد قطعه از خون عدو در کارزار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گرگ در ایام عدلت چون سگ اصحاب کهف</p></div>
<div class="m2"><p>از تهیدستی دل خود می خورد در کنج غار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از خودآرایی نگردد خواب گرد دیده اش</p></div>
<div class="m2"><p>تا عروس فتح را تیغ تو شد آیینه دار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سفته بیرون آید از کان چون لب خوبان عقیق</p></div>
<div class="m2"><p>گر کند سهم خدنگت در دل خارا گذار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خانه پیمان که دیوار و درش زآیینه بود</p></div>
<div class="m2"><p>شد به دوران تو چون سد سکندر استوار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>فتنه در چشم پریرویان حصاری گشته است</p></div>
<div class="m2"><p>تا چو ماه عید شد ابروی تیغت آشکار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شد قوی دست ضعیفان بس که در ایام تو</p></div>
<div class="m2"><p>می گریزد از نهیب مور در سوراخ، مار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نیست در عهد تو از ظالم کسی مظلومتر</p></div>
<div class="m2"><p>بس که گردیده است در چشم جهان بی اعتبار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نافه در چین می گذارد ناف غیرت بر زمین</p></div>
<div class="m2"><p>عنبر خلق تو خواهد کرد اگر زینسان بهار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از حریر شعله جای خواب می سازد سپند</p></div>
<div class="m2"><p>بس که شد در روزگارت وضع عالم برقرار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نیست هر صید زبون شایسته نخجیر تو</p></div>
<div class="m2"><p>می کند شاهین اقبال تو دلها را شکار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بر رعیت مهربانی و به ظالم قهرمان</p></div>
<div class="m2"><p>می بری هر جا که باید لطف و قهر خویش کار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>رم به چشم آهوان خواب فراموشی شود</p></div>
<div class="m2"><p>در رکاب دولت آری پا چو بر عزم شکار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>می رباید حلقه از چشم غزالان نیزه ات</p></div>
<div class="m2"><p>می کند چون آه، تیرت در دل نخجیر کار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>پایه تخت فلک قدر تو از دست دعاست</p></div>
<div class="m2"><p>می شوی از تاج و تخت و عمر و دولت کامکار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هست تأیید الهی شامل احوال تو</p></div>
<div class="m2"><p>می کنی تسخیر عالم را به تیغ آبدار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کارپردازان نصرت منتظر ایستاده اند</p></div>
<div class="m2"><p>تا ترا سازند بر رخش جهانگیری سوار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از سیاهی نیست پروا برق شمشیر ترا</p></div>
<div class="m2"><p>اولین فتح تو خواهد بود ملک قندهار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>آستانت سجده گاه سرفرازان می شود</p></div>
<div class="m2"><p>رو به درگاه تو می آرند شاهان کبار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>می شوی فرمانروا بر هفت اقلیم جهان</p></div>
<div class="m2"><p>چون تویی از تاجداران شاه هفتم در شمار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>می شود عباس، سابع چون کند در خویش دور</p></div>
<div class="m2"><p>هفتم شاهان دینداری تو ای عالم مدار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>می نوازی هر کسی را در خور اخلاص خویش</p></div>
<div class="m2"><p>حق نگهدار تو باد ای پادشاه حق گزار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>جاودان باشی که چون صید حرم آسوده اند</p></div>
<div class="m2"><p>در پناه دولتت خلق جهان از گیرودار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>می کند تا اقتباس نور، ماه از آفتاب</p></div>
<div class="m2"><p>باد از شمع وجودت روشن این نیلی حصار</p></div></div>