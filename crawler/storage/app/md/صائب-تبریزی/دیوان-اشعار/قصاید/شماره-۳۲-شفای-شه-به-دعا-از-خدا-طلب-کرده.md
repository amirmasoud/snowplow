---
title: >-
    شمارهٔ ۳۲ - شفای شه به دعا از خدا طلب کرده
---
# شمارهٔ ۳۲ - شفای شه به دعا از خدا طلب کرده

<div class="b" id="bn1"><div class="m1"><p>خدایا شاه ما را صحت کامل کرامت کن</p></div>
<div class="m2"><p>به غیر از درد دین از دردها او را حمایت کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز نبض مستقیم او بود شیرازه عالم را</p></div>
<div class="m2"><p>جهان را مستقیم از صحت آن پاک طینت کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به حول و قوت خود رفع کن ضعف مزاجش را</p></div>
<div class="m2"><p>مبدل رنج آن جان بخش عالم را به صحت کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرانی بیش ازین آن جان عالم برنمی دارد</p></div>
<div class="m2"><p>به جان دوستان درد و غم او را حوالت کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چه نیست لایق جان ما بهر نثار او</p></div>
<div class="m2"><p>نثار مقدم آن شهسوار دین و دولت کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به لطف خویش بردار انحراف از طبع او یارب</p></div>
<div class="m2"><p>ز شمشیر کج او ملک را با استقامت کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دعای صحبت او می کنند از جان و دل عالم</p></div>
<div class="m2"><p>دعای خلق را در حق او یارب اجابت کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درین موسم که عدل از مهر عالمتاب شد میزان</p></div>
<div class="m2"><p>مزاجش معتدل یارب به میزان عدالت کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز نوبت چون گزیری نیست فرمان بخش عالم را</p></div>
<div class="m2"><p>غم و تشویش او را منحصر در پنج نوبت کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز نور جبهه او چشم عالم روشنی دارد</p></div>
<div class="m2"><p>چراغ عالمی روشن ازان خورشید طلعت کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ندارد غیر ازین وردی زبان خامه صائب</p></div>
<div class="m2"><p>که یارب شاه ما را صحت کامل کرامت کن</p></div></div>