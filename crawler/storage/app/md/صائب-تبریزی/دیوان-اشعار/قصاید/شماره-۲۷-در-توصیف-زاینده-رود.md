---
title: >-
    شمارهٔ ۲۷ - در توصیف زاینده رود
---
# شمارهٔ ۲۷ - در توصیف زاینده رود

<div class="b" id="bn1"><div class="m1"><p>چشمه حیوان ندارد آب و تاب زنده رود</p></div>
<div class="m2"><p>خضر و آب زندگانی، ما و آب زنده رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست آب زندگی را حسن آب زنده رود</p></div>
<div class="m2"><p>صد پری در شیشه دارد هر حباب زنده رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که بتواند سفیدی از سیاهی فرق کرد</p></div>
<div class="m2"><p>می شمارد به ز آب خضر، آب زنده رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سینه بر شمشیر بی زنهار ابرو می زند</p></div>
<div class="m2"><p>چین ابروی بتان از پیچ و تاب زنده رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می ستاند توبه را از کف عنان اختیار</p></div>
<div class="m2"><p>جلوه مستانه دریا رکاب زنده رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خرابی های او چون می عمارتهاست فرش</p></div>
<div class="m2"><p>وقت آن کس خوش که می گردد خراب زنده رود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون ز ظلمت در لباس دود پنهان گشته است؟</p></div>
<div class="m2"><p>نیست آب زندگانی گر کباب زنده رود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می دهد چون خضر، تشریف حیات جاودان</p></div>
<div class="m2"><p>خاکهای مرده دل را فیض آب زنده رود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برق در پیراهن اندازد کتان توبه را</p></div>
<div class="m2"><p>چون می روشن، فروغ ماهتاب زنده رود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در نقاب کف دل از روشن ضمیران می برد</p></div>
<div class="m2"><p>آه اگر افتد به یک جانب نقاب زنده رود!</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>موج، مجنون عنان از دست بیرون رفته ای است</p></div>
<div class="m2"><p>خیمه لیلی است پنداری حباب زنده رود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چشم بیدار و دل زنده است صائب گوهرش</p></div>
<div class="m2"><p>هر رگ باری که برخیزد ز آب زنده رود</p></div></div>