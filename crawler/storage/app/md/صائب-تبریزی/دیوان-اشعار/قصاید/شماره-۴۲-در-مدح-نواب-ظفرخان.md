---
title: >-
    شمارهٔ ۴۲ - در مدح نواب ظفرخان
---
# شمارهٔ ۴۲ - در مدح نواب ظفرخان

<div class="b" id="bn1"><div class="m1"><p>زهی ز نرگس خوش سرمه آهوی مشکین</p></div>
<div class="m2"><p>ز طاق بندی ابرو نگارخانه چین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خوش قماشی ساعد طلای دست افشار</p></div>
<div class="m2"><p>ز بوسه های شکرریز غیرت شیرین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل سر سبد آسمان که خورشیدست</p></div>
<div class="m2"><p>ز شرم روی تو گردید مشرق پروین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز سنبل تو شود زخم غنچه ها تازه</p></div>
<div class="m2"><p>ز خنده تو شود داغ لاله ها تمکین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کمان زند به سر ماه عید، ابرویت</p></div>
<div class="m2"><p>به روی مهر کشد غمزه تو خنجر کین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دو سنبلند که پهلو به یک چمن زده اند</p></div>
<div class="m2"><p>کدام مصرع زلف ترا کنم تحسین؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دوش خود فکنی چون کمان حلقه زلف</p></div>
<div class="m2"><p>به تیر رشک شوی ناف سوز آهوی چین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکست پشت صدف تا لبت به حرف آمد</p></div>
<div class="m2"><p>یتیم کرده گفتار توست در ثمین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز بندخانه شرم و حجاب بیرون آی</p></div>
<div class="m2"><p>که بست از عرق شرم زنگ، قفل جبین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز روی ناز قدم چون نهی به خانه زین</p></div>
<div class="m2"><p>ز خرمی نرسد پای مرکبت به زمین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به غیر حسرت آغوش من حدیثی نیست</p></div>
<div class="m2"><p>کتابه ای که مناسب بود به خانه زین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کیم که آب نگردم ز تاب رخسارت؟</p></div>
<div class="m2"><p>فروغ روی تو زد کوه طور را به زمین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دلم چگونه به پیغام بوسه تازه شود؟</p></div>
<div class="m2"><p>چسان به آب گهر تشنگی دهم تسکین؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز تیره روزی شبهای ما چه غم داری؟</p></div>
<div class="m2"><p>ترا که لاله طورست بر سر بالین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو کم ز غنچه و ما کم ز عندلیب نه ایم</p></div>
<div class="m2"><p>چرا به صحبت ما وا نمی شوی به ازین؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به شکر این که ز گلزار حسن سیرابی</p></div>
<div class="m2"><p>مباش تشنه به خونریز عاشقان چندین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وگرنه راه سخن پیش صاحبی دارم</p></div>
<div class="m2"><p>که انتقام کبوتر گرفته از شاهین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بهار عدل، ظفرخان که می کند لطفش</p></div>
<div class="m2"><p>شکسته بندی دلهای مستمند حزین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زهی رسیده به جایی (ز) سربلندی قدر</p></div>
<div class="m2"><p>که پشت دست نهاده است آسمان به زمین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شود چو غنچه نیلوفر از حرارت مهر</p></div>
<div class="m2"><p>اگر به خشم نظر افکنی به چرخ برین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به گرد بالش خورشید سر فرو نارد</p></div>
<div class="m2"><p>ز دود مجمر خلق تو زلف حورالعین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ستاره تو چو گل بر سر سپهر زند</p></div>
<div class="m2"><p>شود به دیده خفاش مهر گوشه نشین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اگر نه کوه وقار تو پافشرده بر او</p></div>
<div class="m2"><p>چرا شده است چنین میخ دوز جرم زمین؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو برق ابر نیام تو چهره افروزد</p></div>
<div class="m2"><p>فتد به رعشه چو سیماب خصم بی تمکین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عدو زبان بدر آرد چو مار زنهاری</p></div>
<div class="m2"><p>چو از نیام کشی روز رزم خنجر کین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چنان ز بیم تو تلخ است زندگی بر خصم</p></div>
<div class="m2"><p>که چشم می پردش بر نگاه بازپسین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به چشم اهل یقین آیه آیه سوره فتح</p></div>
<div class="m2"><p>ز جبهه تو نمایان بود به خط مبین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر چه قلعه دوران شکوه کابل را</p></div>
<div class="m2"><p>گرفته بود عدو در میانه همچو نگین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شدی چو پیشرو لشکر از جلال آباد</p></div>
<div class="m2"><p>سپاه نصرت و اقبال از یسار و یمین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هنوز عرصه سرخاب بود منزل تو</p></div>
<div class="m2"><p>که جوی خون عدو راست رفت تا غزنین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عجب نباشد اگر از سنان خونخوارت</p></div>
<div class="m2"><p>گریخت تا به خارا و بلخ خصم لعین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بلی شهاب چو گردد ز چرخ نیزه گذار</p></div>
<div class="m2"><p>کنند فوج شیاطین گریختن آیین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چنان ز جنگ تو بگریخت خصم روبه باز</p></div>
<div class="m2"><p>که وحشیان سبکرو ز پیش شیر عرین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بلند بختا! خود گو که چون تواند گفت</p></div>
<div class="m2"><p>زبان کوته ما شکر فتح های چنین؟</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو آفتاب، دهانی به صد زبان باید</p></div>
<div class="m2"><p>که مصرعی ز ظفرنامه ات کند تضمین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بهار طبعا! بلبل شناس گلزارا!</p></div>
<div class="m2"><p>که هست در کف کلک تو نبض فکر متین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اگر چه حالت هر کس به چشم فکرت تو</p></div>
<div class="m2"><p>مبرهن است، که داری سواد خط جبین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به سنت شعرا در مدیح خود غزلی</p></div>
<div class="m2"><p>درین قصیده به تقریب می کنم تضمین:</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز بس که ریخت ز کلکم معانی رنگین</p></div>
<div class="m2"><p>خمیرمایه قوس قزح شده است زمین</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هزار شاعر شیرین سخن به گرد رود</p></div>
<div class="m2"><p>نهد چو خسرو طبعم به پشت گلگون زین</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز پاک طینتی اشعار من بلندی یافت</p></div>
<div class="m2"><p>ز تازگی سخنانم گرفت روی زمین</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز فیض پاکی دامان مریم صدف است</p></div>
<div class="m2"><p>که گوشوار نکویان شده است در ثمین</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به دوش عرش نهم کرسی بلندی قدر</p></div>
<div class="m2"><p>به وقت فکر چو از دست خود کنم بالین</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>درین هوس که مرا لیقه دوات شود</p></div>
<div class="m2"><p>پرید از چمن خلد زلف حورالعین</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز نامداری خود در حصار گردونم</p></div>
<div class="m2"><p>ز بندخانه نگردد خلاص نقش نگین</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>تتبع سخن کس نکرده ام هرگز</p></div>
<div class="m2"><p>کسی نکرده به من فن شعر را تلقین</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به زور فکر بر این طرز دست یافته ام</p></div>
<div class="m2"><p>صدف ز آبله دست یافت در ثمین</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز روی آینه طبعان حجاب کن صائب</p></div>
<div class="m2"><p>مده به طوطی گستاخ کلک، رو چندین</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نگار کن به دعا دست خالی خود را</p></div>
<div class="m2"><p>که روح قدس ستاده است لب پر از آمین</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>همیشه تا ز نسیم شکفته روی بهار</p></div>
<div class="m2"><p>جبین غنچه برون آید از شکنجه چین</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>موافقان ترا دل ز مژدگانی فتح</p></div>
<div class="m2"><p>شکفته باد چو گل در هوای فروردین</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مخالفان ترا همچو غنچه تصویر</p></div>
<div class="m2"><p>مباد هیچ نسیمی گرهگشای جبین</p></div></div>