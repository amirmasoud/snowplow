---
title: >-
    شمارهٔ ۹ - در مدح شاه عباس دوم
---
# شمارهٔ ۹ - در مدح شاه عباس دوم

<div class="b" id="bn1"><div class="m1"><p>هزار شکر که گوهر فروز جاه و جلال</p></div>
<div class="m2"><p>به خانه شرف آمد به دولت و اقبال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز درد سال غباری که داشت جام سپهر</p></div>
<div class="m2"><p>به صاف کرد مبدل محول الاحوال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو زلف رو به درازی نهاد روز نشاط</p></div>
<div class="m2"><p>چو خال پای به دامن کشید شام ملال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ایاز شب را، ز اقبال عاقبت محمود</p></div>
<div class="m2"><p>برید زلف به شمشیر ذوالفقار مثال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رسید قافله بوی پیرهن از مصر</p></div>
<div class="m2"><p>نماند دیده پوشیده غیر عین کمال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هوا چو دست کریمان گهرفشان گردید</p></div>
<div class="m2"><p>کنار خاک شد از برگ عیش مالامال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو ماهیی که در آب حیات، خضر افکند</p></div>
<div class="m2"><p>حیات یافت ز ابر بهار سنگ و سفال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هوا بساط سلیمان فکند بر رخ خاک</p></div>
<div class="m2"><p>گشود همچو پری ابر نوبهاران بال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گشود ابر بهار از شکوفه دفتر و ریخت</p></div>
<div class="m2"><p>برات عیش به دامان هر شکسته نهال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رسید دور شکفتن به غنچه تصویر</p></div>
<div class="m2"><p>گره ز کار جهان باز کرد باد شمال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز بس که خاک ز شادی به خویشتن بالید</p></div>
<div class="m2"><p>رسید موج گل و لاله تا رکاب هلال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به مومیایی ابر بهار گشت درست</p></div>
<div class="m2"><p>اگر شکستگیی داشت چند روز نهال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صدا چو تیر ز کف رفته برنمی گردد</p></div>
<div class="m2"><p>ز بس ز لاله و گل دلپذیر گشت جبال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز خاک ریشه اشجار را توان دیدن</p></div>
<div class="m2"><p>چنان که سنبل سیراب را ز آب زلال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>توان به آب کشید از نسیم دست و دهن</p></div>
<div class="m2"><p>اگر ز ابر هوا تر شود به این منوال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به آن شکوه به برج حمل درآمد مهر</p></div>
<div class="m2"><p>که شهریار جوان بخت بر سپهر جلال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نتیجه اسدالله، شاه دین عباس</p></div>
<div class="m2"><p>که هست تیغ کجش شیر فتح را چنگال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به امر حق بود آن سایه خدا دایم</p></div>
<div class="m2"><p>چنان که تابع شخص است سایه در افعال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو آفتاب نمایان بود ز سینه صبح</p></div>
<div class="m2"><p>ز طرف جبهه او نور اختر اقبال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنان که هست ز سبابه رایت ایمان</p></div>
<div class="m2"><p>ازوست نوبت صاحبقرانی از امثال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کراست زهره شود راست چون الف پیشش؟</p></div>
<div class="m2"><p>که هست تیغ کج او به فتح و نصرت دال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سبک رکاب شود همچو دود ظلمت کفر</p></div>
<div class="m2"><p>چو برکشد ز میان تیغ ذوالفقار مثال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سپاه اوست چو مژگان خدنگ یک ترکش</p></div>
<div class="m2"><p>کراست زهره که با او طرف شود به قتال؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگر به قلعه رویین چرخ رو آرد</p></div>
<div class="m2"><p>کلید ماه نو آرد قضا به استقبال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چنان که خامه به خط سطر را کند باطل</p></div>
<div class="m2"><p>شود شکسته ز یک تیر او صف ابطال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز برق تیغش اگر پرتوی به بحر افتد</p></div>
<div class="m2"><p>صدف چو مجمر سوزان شود، سپندلآل</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نفس به کامش ابریشم بریده شود</p></div>
<div class="m2"><p>کسی که خنجر او را درآورد به خیال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دلیر بر سر گردنکشان رود چون ابر</p></div>
<div class="m2"><p>پلنگ را چه محابا بود ز تیغ جبال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کفن ز شهپر کرکس کند دلیران را</p></div>
<div class="m2"><p>همای ناوک او باز چون کند پر و بال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>رسد به رستم اگر در رحم صلابت او</p></div>
<div class="m2"><p>سفیدموی برون آید از رحم چون زال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کند چو زخم زبان کار در دل آهن</p></div>
<div class="m2"><p>ز غنچه ناوک او را اگر کنند نصال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تهمتنی که دهد جان به تیغ خونریزش</p></div>
<div class="m2"><p>به روز حشر زبانش بود زدهشت لال</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اگر شود کجک روزگار تیغ کجش</p></div>
<div class="m2"><p>شود چو ابر بهاران سبک رکاب جبال</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز آتش غضب او در آستین نیام</p></div>
<div class="m2"><p>ز پیچ و تاب بود تیغ دشمنان چون نال</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در آن مقام که گردد به نیزه حلقه ربا</p></div>
<div class="m2"><p>فتد به حلقه گردون ز هیبتش زلزال</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هلال نیست نمایان بر این رواق بلند</p></div>
<div class="m2"><p>که شیر چرخ فکند از نهیب او چنگال</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>که دیده جز خم چوگان و گوی زرینش؟</p></div>
<div class="m2"><p>که آفتاب زند قطره در رکاب هلال</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زره چه کار کند پیش تیغ خونریزش؟</p></div>
<div class="m2"><p>نمی توان ره سیلاب بست با غربال</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اگر به چرخ کند کوه حلم او سایه</p></div>
<div class="m2"><p>چو صبح آرد شود استخوان او در حال</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز ابر معدلت او کمین نموداری است</p></div>
<div class="m2"><p>که تخم، سبز در آتش بود چو دانه خال</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>رسیده است به جایی عروج همت او</p></div>
<div class="m2"><p>که آسمان بلندست سبزه پامال</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو بحر تا به قیامت نمی شودخالی</p></div>
<div class="m2"><p>شود ز ابر کفش دامنی که مالامال</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو سایلان به کف، بحر پیش ابر کفش</p></div>
<div class="m2"><p>دراز کرده ز مرجان کف از برای سؤال</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بر آسمان جلالش هلال عید، بود</p></div>
<div class="m2"><p>خجل ز کوشش خود همچو طایر یک بال</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به عهد معدلتش وقت خواب آسایش</p></div>
<div class="m2"><p>ز چشم شیر به بالین نهد چراغ، غزال</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>شده است مایده لطف او به نوعی عام</p></div>
<div class="m2"><p>که در رحم عوض خون خورند می اطفال</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز خلق اوست برومند خاکدان جهان</p></div>
<div class="m2"><p>که تازه روی ز ریحان بود همیشه سفال</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به غیر جام که برگردد از کفش خالی</p></div>
<div class="m2"><p>دگر که از کرم او نمی رسد به نوال</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چگونه سوی شکر کاروان مور رود؟</p></div>
<div class="m2"><p>چنان به خاک درش رو نهاده اند آمال</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>اگر به زاغ شب افتد ز رای او پرتو</p></div>
<div class="m2"><p>چو آفتاب برآید ز بیضه زرین بال</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>شود ز نور گهرخیز دیده روزن</p></div>
<div class="m2"><p>در آن حریم که گردد گهرفشان ز مقال</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به وام گیرد اشهب ز عنبر خلقش</p></div>
<div class="m2"><p>زند چو دور به گرد جهان نسیم شمال</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به آفتاب روان است امر نافذ او</p></div>
<div class="m2"><p>چنان که بر جگر تشنه حکم آب زلال</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نه لاله است، که حلمش فکنده سایه به کوه</p></div>
<div class="m2"><p>نشسته در عرق خون ز انفعال جبال</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چو رود نیل دهد کوچه پیش دست کلیم</p></div>
<div class="m2"><p>اگر به کوه کند عزم راسخش اقبال</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>نظر به عزمش، گردون چو مرغ نوپرواز</p></div>
<div class="m2"><p>در آشیانه نشسته است و می زند پر و بال</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چنان به عهدش کسب کمال شیرین است</p></div>
<div class="m2"><p>که روز جمعه ز مکتوب نمی روند اطفال</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به دور او که برافتاده است خانه نزول</p></div>
<div class="m2"><p>ز آبگینه اجازت طلب کند تمثال</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>اگر نه خلق بود بر شکوه او غالب</p></div>
<div class="m2"><p>کراست زهره که لب واکند برش به مقال؟</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چو رشته کاهکشان در گهر شود پنهان</p></div>
<div class="m2"><p>ز آستین بدر آرد چو دست بحر نوال</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>جهان پناه خدیو! بلند اقبالا!</p></div>
<div class="m2"><p>که ختم بر تو شد از خسروان صفات کمال</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>اگر به مدح تو اطناب می کنم چون موج</p></div>
<div class="m2"><p>به خلق خویش ببخش ای محیط جاه و جلال</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>که مدحت تو و اجداد پاک طینت تو</p></div>
<div class="m2"><p>کلید خلد برین است ای فرشته خصال</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>برای حسن مآل است مدح سنجی من</p></div>
<div class="m2"><p>نه از برای زر و سیم و ملک و مال و منال</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>تلاش قرب تو با این کلام بی سر و بن</p></div>
<div class="m2"><p>همان معامله یوسف است و قصه زال</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چنان که کرد سلیمان قبول گفته مور</p></div>
<div class="m2"><p>قبول کن ز من عاجز این شکسته مقال</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>که هیچ کم نشود شوکت سلیمانی</p></div>
<div class="m2"><p>به حرف مور ضعیفی اگر کند اقبال</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چه کم ز پرتو مهر بلند می گردد؟</p></div>
<div class="m2"><p>اگر به شبنم افتاده ای دهد پر و بال</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>همیشه تا چمن افروز چرخ مینا رنگ</p></div>
<div class="m2"><p>ز برج حوت به برج حمل کند اقبال</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چو خاتمی که به فرمان دست می گردد</p></div>
<div class="m2"><p>به مدعای تو گردد مدام گردش سال</p></div></div>