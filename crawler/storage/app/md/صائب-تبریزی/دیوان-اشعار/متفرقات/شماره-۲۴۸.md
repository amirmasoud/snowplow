---
title: >-
    شمارهٔ ۲۴۸
---
# شمارهٔ ۲۴۸

<div class="b" id="bn1"><div class="m1"><p>یکه تازان جنون چون روی در هامون کنند</p></div>
<div class="m2"><p>خاکها در کاسه بی ظرفی مجنون کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلبلان سوگند بر سی پاره گل خورده اند</p></div>
<div class="m2"><p>کز گلستان شبنم گستاخ را بیرون کنند!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حیرتی دارم که چون در روزگار زلف او</p></div>
<div class="m2"><p>رسم گردیده است مردم شکوه از گردون کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرخ رویی لازم دیبای شرم افتاده است</p></div>
<div class="m2"><p>زین سبب پیراهن فانوس را گلگون کنند</p></div></div>