---
title: >-
    شمارهٔ ۵۲۳
---
# شمارهٔ ۵۲۳

<div class="b" id="bn1"><div class="m1"><p>کمند زلف ترا چون به خویش رام کنیم؟</p></div>
<div class="m2"><p>به راه دام تو در خاک چند دام کنیم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپهر تیغ مکافات بر کف استاده است</p></div>
<div class="m2"><p>چه لازم است که ما فکر انتقام کنیم؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر چه پختگیی نیست کارفرما را</p></div>
<div class="m2"><p>چه لازم است که ما کار خویش خام کنیم؟</p></div></div>