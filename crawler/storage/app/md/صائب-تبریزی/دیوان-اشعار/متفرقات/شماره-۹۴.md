---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>به این عنوان اگر قامت کشد سرو دلارایت</p></div>
<div class="m2"><p>نماید طوق قمری جلوه خلخال در پایت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نخواهی از گزیدن مانع دندان من گشتن</p></div>
<div class="m2"><p>اگر دانی چه خون‌ها در جگر دارم ز لب‌هایت</p></div></div>