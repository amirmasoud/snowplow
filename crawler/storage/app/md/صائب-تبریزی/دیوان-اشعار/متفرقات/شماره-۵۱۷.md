---
title: >-
    شمارهٔ ۵۱۷
---
# شمارهٔ ۵۱۷

<div class="b" id="bn1"><div class="m1"><p>اگر سیاه دلم داغ لاله زار توام</p></div>
<div class="m2"><p>اگر گشاده جبینم گل بهار توام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر چه چون ورق لاله نامه ام سیه است</p></div>
<div class="m2"><p>به این خوشم که جگرگوشه بهار توام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چرا عزیز نباشم، نه خار این چمنم؟</p></div>
<div class="m2"><p>سرم به عرش نساید چرا، غبار توام</p></div></div>