---
title: >-
    شمارهٔ ۸۰
---
# شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>گل ز تیغ غمزه‌اش در خاک و خون غلتیده است</p></div>
<div class="m2"><p>چشم خورشید از غبار خط او ترسیده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اشک ما در چشم دارد گرد غربت بر جبین</p></div>
<div class="m2"><p>گوهر ما در صدف داغ یتیمی دیده است</p></div></div>