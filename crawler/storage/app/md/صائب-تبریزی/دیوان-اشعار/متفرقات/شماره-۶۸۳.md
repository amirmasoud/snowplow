---
title: >-
    شمارهٔ ۶۸۳
---
# شمارهٔ ۶۸۳

<div class="b" id="bn1"><div class="m1"><p>اگر ز تیغ کند روزگار افسر تو</p></div>
<div class="m2"><p>برون نمی رود این باد نخوت از سر تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز سرکشی تو نبینی به زیر پا، ورنه</p></div>
<div class="m2"><p>به لای نفی بنا کرده اند پیکر تو</p></div></div>