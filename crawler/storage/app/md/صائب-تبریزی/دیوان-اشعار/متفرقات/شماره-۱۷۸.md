---
title: >-
    شمارهٔ ۱۷۸
---
# شمارهٔ ۱۷۸

<div class="b" id="bn1"><div class="m1"><p>رخساره گلرنگ تو گلزار بهشت است</p></div>
<div class="m2"><p>خط گرد گل روی تو دیوار بهشت است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طاعات ریایی است کلید در دوزخ</p></div>
<div class="m2"><p>زاهد به چه سرمایه خریدار بهشت است</p></div></div>