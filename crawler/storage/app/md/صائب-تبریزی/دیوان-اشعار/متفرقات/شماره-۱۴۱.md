---
title: >-
    شمارهٔ ۱۴۱
---
# شمارهٔ ۱۴۱

<div class="b" id="bn1"><div class="m1"><p>ای که گفتی نگاه خیره تو</p></div>
<div class="m2"><p>پرده شرم از میان برداشت:</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی ازان روی می توان گرداند؟</p></div>
<div class="m2"><p>چشم ازان چشم می توان برداشت؟</p></div></div>