---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>(از رحم بر زمین نزد آسمان مرا</p></div>
<div class="m2"><p>دارد بپا برای نشان این کمان مرا)</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>(چون سرو و بید سایه من دام عشرت است</p></div>
<div class="m2"><p>هر چند میوه نیست درین بوستان مرا)</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>(از وصل گل مرا چه تمتع، که شرم عشق</p></div>
<div class="m2"><p>دارد چو بیضه در بغل آشیان مرا)</p></div></div>