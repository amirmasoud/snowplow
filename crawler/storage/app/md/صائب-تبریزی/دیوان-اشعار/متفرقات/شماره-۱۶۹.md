---
title: >-
    شمارهٔ ۱۶۹
---
# شمارهٔ ۱۶۹

<div class="b" id="bn1"><div class="m1"><p>آتش ز شرم خوی تو تا سر کشیده است</p></div>
<div class="m2"><p>خود را به زیر بال سمندر کشیده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خودبین مباش تا به حیات ابد رسی</p></div>
<div class="m2"><p>آیینه سد به راه سکندر کشیده است</p></div></div>