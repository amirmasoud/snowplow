---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>جان من رفتن ازین سینه بی کینه چرا؟</p></div>
<div class="m2"><p>روی گردان شدن از صحبت آیینه چرا؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میفروشان به خدا عالم درویشی هاست</p></div>
<div class="m2"><p>نگرفتن به گرو خرقه پشمینه چرا؟</p></div></div>