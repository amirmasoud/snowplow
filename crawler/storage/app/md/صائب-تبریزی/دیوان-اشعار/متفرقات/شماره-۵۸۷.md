---
title: >-
    شمارهٔ ۵۸۷
---
# شمارهٔ ۵۸۷

<div class="b" id="bn1"><div class="m1"><p>ای فتنه از سپاه تو تیری ز ترکشی</p></div>
<div class="m2"><p>از خنده تو شور قیامت نمکچشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میدان بی قراری ما را کنار نیست</p></div>
<div class="m2"><p>دل یک سپندو عشق تو دریای آتشی</p></div></div>