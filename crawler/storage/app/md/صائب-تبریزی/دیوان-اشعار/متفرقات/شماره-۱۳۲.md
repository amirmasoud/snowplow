---
title: >-
    شمارهٔ ۱۳۲
---
# شمارهٔ ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>جز نام تو بر لوح دلم هیچ رقم نیست</p></div>
<div class="m2"><p>در نامه ما یک سر مو سهو قلم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما خود سر طومار شکایت نگشاییم</p></div>
<div class="m2"><p>خودگوی، فراموشی احباب ستم نیست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر نامه سودازدگان نکته نگیرند</p></div>
<div class="m2"><p>داریم جوابی که به دیوانه قلم نیست</p></div></div>