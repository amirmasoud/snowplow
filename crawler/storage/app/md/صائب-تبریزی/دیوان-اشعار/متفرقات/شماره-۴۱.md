---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>لعل از کان بدخشان، گوهر از عمان طلب</p></div>
<div class="m2"><p>گنج از ویران، حضور دل ز درویشان طلب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست نعمت را درین دریای بی پایان حساب</p></div>
<div class="m2"><p>چون صدف از عالم بالا همین دندان طلب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می کند کار نمک درمی، تکلف در سلوک</p></div>
<div class="m2"><p>خانه را پاک از تکلف کن دگر مهمان طلب</p></div></div>