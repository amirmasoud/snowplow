---
title: >-
    شمارهٔ ۲۹۰
---
# شمارهٔ ۲۹۰

<div class="b" id="bn1"><div class="m1"><p>جهان را تار و پود هستی از موج خطر باشد</p></div>
<div class="m2"><p>کف این بحر خون آشام از مغز گهر باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منه دل بر وفای چرخ کجرو هوش اگر داری</p></div>
<div class="m2"><p>که دستش هر زمان چون تاک بر دوش دگر باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برآی از خود، جهان را زیر دست خویش اگر خواهی</p></div>
<div class="m2"><p>که در پرواز، عالم مرغ را در زیر پر باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>توان سیر پر طاوس کرد از هر پر زاغی</p></div>
<div class="m2"><p>اگر آیینه انصاف در پیش نظر باشد</p></div></div>