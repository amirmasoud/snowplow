---
title: >-
    شمارهٔ ۵۹۸
---
# شمارهٔ ۵۹۸

<div class="b" id="bn1"><div class="m1"><p>اول علاج ما به نگاه کشند کن</p></div>
<div class="m2"><p>آنگاه غیر را هدف نوشخند کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از صد یکی به سوز نهانی نمی رسد</p></div>
<div class="m2"><p>ای کمتر از سپند، صدایی بلند کن</p></div></div>