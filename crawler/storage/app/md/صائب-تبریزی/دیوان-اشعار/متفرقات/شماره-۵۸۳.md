---
title: >-
    شمارهٔ ۵۸۳
---
# شمارهٔ ۵۸۳

<div class="b" id="bn1"><div class="m1"><p>غافل ز سیر عالم انوار مانده ای</p></div>
<div class="m2"><p>در عقده بزرگی دستار مانده ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گم کرده ای چو شعله ره بازگشت خویش</p></div>
<div class="m2"><p>در زیر دست و پای خس و خار مانده ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبنم به آفتاب رسانید خویش را</p></div>
<div class="m2"><p>در دام رنگ و بو چه گرفتار مانده ای؟</p></div></div>