---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>آتش افروز جنون شد دامن صحرا مرا</p></div>
<div class="m2"><p>طشت آتش ریخت بر سر لاله حمرا مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر سر راهی به آگاهی حوالت کرده اند</p></div>
<div class="m2"><p>ناله نی شد دلیل عالم بالا مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست در بزم تو جایم، ورنه در هر محفلی</p></div>
<div class="m2"><p>می جهد از جا سپند و می نماید جا مرا</p></div></div>