---
title: >-
    شمارهٔ ۲۴۵
---
# شمارهٔ ۲۴۵

<div class="b" id="bn1"><div class="m1"><p>چند حرف بوسه او بر لب جان بشکند؟</p></div>
<div class="m2"><p>چند جامم در کنار آب حیوان بشکند؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از شکست دل نشد کم هیچ شور گریه ام</p></div>
<div class="m2"><p>کی به یک کشتی شکستن خشم طوفان بشکند؟</p></div></div>