---
title: >-
    شمارهٔ ۲۰۸
---
# شمارهٔ ۲۰۸

<div class="b" id="bn1"><div class="m1"><p>یادگار عشق داغی در دل دیوانه ماند</p></div>
<div class="m2"><p>شمع رفت از انجمن، خاکستر پروانه ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گریه ام در دل گره شد، ناله ام بر لب شکست</p></div>
<div class="m2"><p>وای بر قفلی که مفتاحش درون خانه ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زیر سقف آسمان نتوان نفس را راست کرد</p></div>
<div class="m2"><p>در دل ما آرزوی نعره مستانه ماند</p></div></div>