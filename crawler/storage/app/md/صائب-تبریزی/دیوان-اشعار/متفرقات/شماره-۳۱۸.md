---
title: >-
    شمارهٔ ۳۱۸
---
# شمارهٔ ۳۱۸

<div class="b" id="bn1"><div class="m1"><p>شبنم از روی لطیف تو نظر می دزدد</p></div>
<div class="m2"><p>غنچه از شرم تو سر در ته پر می دزدد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می کند بیهده دل عیب خود از عشق نهان</p></div>
<div class="m2"><p>گل ز خورشید عبث دامن تر می دزدد</p></div></div>