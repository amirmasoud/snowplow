---
title: >-
    شمارهٔ ۵۸
---
# شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>بهار مایه غفلت بود گرانان را</p></div>
<div class="m2"><p>شکوفه پنبه گوش است باغبانان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چراغ گل به نسیم بهانه ای بندست</p></div>
<div class="m2"><p>مبر به سیر چمن آستین فشانان را</p></div></div>