---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>رنگ بر روی گل آید ز وفاداری ما</p></div>
<div class="m2"><p>سرو بر خویش ببالد ز هواداری ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دم عیسی اگر ناز کند جا دارد</p></div>
<div class="m2"><p>نسخه از چشم تو برداشته بیماری ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>(آنقدر در دهن تیغ تغافل باشیم</p></div>
<div class="m2"><p>کآورد خوی تو ایمان به وفاداری ما)</p></div></div>