---
title: >-
    شمارهٔ ۲۳۱
---
# شمارهٔ ۲۳۱

<div class="b" id="bn1"><div class="m1"><p>(غبارم را نسیم ناتوانی دربدر دارد</p></div>
<div class="m2"><p>غریب کشور طالع چه پروای سفر دارد؟)</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>(غلط کردم ز بزم او جدا گشتم، ندانستم</p></div>
<div class="m2"><p>خمار باده لعلش چه عالم دردسر دارد)</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز نخوت تاج شاهان فتنه ها در زیر سر دارد</p></div>
<div class="m2"><p>ازین باد مخالف کشتی دولت خطر دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مخور زنهار از همواری وضع جهان بازی</p></div>
<div class="m2"><p>که این بیدادگر در موم پنهان نیشتر دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مده سررشته کوچکدلی از دست در دولت</p></div>
<div class="m2"><p>که گر از دیده سوزن فتد عیسی خطر دارد</p></div></div>