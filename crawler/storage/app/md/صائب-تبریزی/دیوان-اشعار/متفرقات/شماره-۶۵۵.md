---
title: >-
    شمارهٔ ۶۵۵
---
# شمارهٔ ۶۵۵

<div class="b" id="bn1"><div class="m1"><p>آسوده ای که لطف نمایان ندیده ای</p></div>
<div class="m2"><p>آن سینه را ز چاک گریبان ندیده ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از شوخی نگاه در آن چشم غافلی</p></div>
<div class="m2"><p>در قطره چار موجه طوفان ندیده ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از شست غمزه ناوک مشکین نخورده ای</p></div>
<div class="m2"><p>در گرد سرمه جنبش مژگان ندیده ای</p></div></div>