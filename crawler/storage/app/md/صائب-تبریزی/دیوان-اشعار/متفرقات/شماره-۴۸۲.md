---
title: >-
    شمارهٔ ۴۸۲
---
# شمارهٔ ۴۸۲

<div class="b" id="bn1"><div class="m1"><p>خطی که دمید گرد رخسارش</p></div>
<div class="m2"><p>شد پرده گلیم چشم عیارش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر خاطر نازکش گران آید</p></div>
<div class="m2"><p>گل تکیه زند اگر به دیوارش</p></div></div>