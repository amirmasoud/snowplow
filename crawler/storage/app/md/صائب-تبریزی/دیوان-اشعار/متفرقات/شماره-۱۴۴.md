---
title: >-
    شمارهٔ ۱۴۴
---
# شمارهٔ ۱۴۴

<div class="b" id="bn1"><div class="m1"><p>زلف مشکین تو سر در دامن محشر نهاد</p></div>
<div class="m2"><p>خط گستاخ تو لب را بر لب کوثر نهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برنمی خیزد ز شور حشر، یارب بخت من</p></div>
<div class="m2"><p>در کدامین ساعت سنگین به بالین سر نهاد</p></div></div>