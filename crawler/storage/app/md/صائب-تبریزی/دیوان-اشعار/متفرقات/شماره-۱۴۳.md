---
title: >-
    شمارهٔ ۱۴۳
---
# شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>شنیدم آنقدر از دوستان تلخ</p></div>
<div class="m2"><p>که شد شیرینی جان در دهان تلخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نباشد چشم او بی زهر چشمی</p></div>
<div class="m2"><p>بود بیمار را دایم دهان تلخ</p></div></div>