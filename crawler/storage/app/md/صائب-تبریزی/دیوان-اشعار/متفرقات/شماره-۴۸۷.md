---
title: >-
    شمارهٔ ۴۸۷
---
# شمارهٔ ۴۸۷

<div class="b" id="bn1"><div class="m1"><p>هر کس که چون صدف دهن خویش کرد پاک</p></div>
<div class="m2"><p>لبریز می شود ز گهرهای تابناک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی سجده می کنند نماز جنازه را</p></div>
<div class="m2"><p>مگذار پیش مرده دلان روی خود به خاک</p></div></div>