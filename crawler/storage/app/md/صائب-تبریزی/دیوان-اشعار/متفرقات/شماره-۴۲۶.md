---
title: >-
    شمارهٔ ۴۲۶
---
# شمارهٔ ۴۲۶

<div class="b" id="bn1"><div class="m1"><p>بال و پر محیط ز موج آشکاره شد</p></div>
<div class="m2"><p>گردد یکی هزار چو دل پاره پاره شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بالیدگی است لازمه التفات خلق</p></div>
<div class="m2"><p>فربه به یک دو هفته هلال از اشاره شد</p></div></div>