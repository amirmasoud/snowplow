---
title: >-
    شمارهٔ ۴۵۱
---
# شمارهٔ ۴۵۱

<div class="b" id="bn1"><div class="m1"><p>دور قدح را مه تمام ندارد</p></div>
<div class="m2"><p>روشنی ماه این دوام ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر لب کوثر شکسته است سبویش</p></div>
<div class="m2"><p>هر که به کف وقت صبح جام ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با همه دلها یکی است نسبت آن زلف</p></div>
<div class="m2"><p>دیده آهوشناس دام ندارد</p></div></div>