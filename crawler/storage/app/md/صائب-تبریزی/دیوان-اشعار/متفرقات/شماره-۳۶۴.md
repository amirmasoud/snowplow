---
title: >-
    شمارهٔ ۳۶۴
---
# شمارهٔ ۳۶۴

<div class="b" id="bn1"><div class="m1"><p>سحر که پرده ز رخ گلرخان براندازند</p></div>
<div class="m2"><p>( ) زلزله در ملک خاور اندازند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حذر ز گرمی این ره مکن که آبله ها</p></div>
<div class="m2"><p>به هر قدم که نهی فرش گوهر اندازند</p></div></div>