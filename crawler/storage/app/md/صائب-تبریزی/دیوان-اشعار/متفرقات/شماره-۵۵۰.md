---
title: >-
    شمارهٔ ۵۵۰
---
# شمارهٔ ۵۵۰

<div class="b" id="bn1"><div class="m1"><p>نیست غیری در حریم دیده نمناک من</p></div>
<div class="m2"><p>نام لیلی نقش می بندد ز اشک پاک من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اینقدر بی طاقتی در مشت خاری بوده است؟</p></div>
<div class="m2"><p>روی دریا شد کبود از سیلی خاشاک من</p></div></div>