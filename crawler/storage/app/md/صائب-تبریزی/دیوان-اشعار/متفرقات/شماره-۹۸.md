---
title: >-
    شمارهٔ ۹۸
---
# شمارهٔ ۹۸

<div class="b" id="bn1"><div class="m1"><p>پیش اشکم که خروشنده تر از سیلاب است</p></div>
<div class="m2"><p>بحر را مهر خموشی به لب از گرداب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا ازان حسن رباینده نظر یافته است</p></div>
<div class="m2"><p>آب آیینه رباینده تر از سیلاب است</p></div></div>