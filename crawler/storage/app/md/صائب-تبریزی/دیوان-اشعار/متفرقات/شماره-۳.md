---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>گر چنین ابروی او ره می زند اصحاب را</p></div>
<div class="m2"><p>رفته رفته طاق نسیان می کند محراب را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از شبیخون حوادث عشقبازان غافلند</p></div>
<div class="m2"><p>می کند خون در جگر صید حرم قصاب را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیر چشمان خیال از فکر وصل آسوده اند</p></div>
<div class="m2"><p>می گزد می شیرمست پرتو مهتاب را</p></div></div>