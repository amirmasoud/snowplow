---
title: >-
    شمارهٔ ۳۰۸
---
# شمارهٔ ۳۰۸

<div class="b" id="bn1"><div class="m1"><p>اگر خواهی سر مویی نپیچد سر ز فرمانت</p></div>
<div class="m2"><p>به خاموشی زبانی چون زبان شانه می باید</p></div></div>