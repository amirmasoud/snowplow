---
title: >-
    شمارهٔ ۴۰۶
---
# شمارهٔ ۴۰۶

<div class="b" id="bn1"><div class="m1"><p>هرکس که از رخ تو نظر آب می‌دهد</p></div>
<div class="m2"><p>خرمن به برق و خانه به سیلاب می‌دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خون یک جهان دل بی‌تاب می‌رود</p></div>
<div class="m2"><p>مشاطه‌ای که زلف ترا تاب می‌دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صیدی که بی‌قراری وحشت کشیده است</p></div>
<div class="m2"><p>در چشم دام، داد شکرخواب می‌دهد</p></div></div>