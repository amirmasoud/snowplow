---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>چند دارم در بغل پنهان دل افسرده را؟</p></div>
<div class="m2"><p>چند در فانوس دارم این چراغ مرده را؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیر گلشن بی دماغان را نمی آرد به حال</p></div>
<div class="m2"><p>سایه گل می کشد در خون دل آزرده را</p></div></div>