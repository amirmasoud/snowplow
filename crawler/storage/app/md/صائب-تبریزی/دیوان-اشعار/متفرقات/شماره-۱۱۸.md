---
title: >-
    شمارهٔ ۱۱۸
---
# شمارهٔ ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>زبان شانه درازست بر سر عالم</p></div>
<div class="m2"><p>به این که خدمت زلف تو حق شمشادست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جفای چرخ فلک را هم از تو می دانم</p></div>
<div class="m2"><p>که شوخ چشمی طفلان گناه استادست</p></div></div>