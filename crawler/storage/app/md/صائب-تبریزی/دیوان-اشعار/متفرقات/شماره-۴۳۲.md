---
title: >-
    شمارهٔ ۴۳۲
---
# شمارهٔ ۴۳۲

<div class="b" id="bn1"><div class="m1"><p>جوش سخن من بود از جذبه مردان</p></div>
<div class="m2"><p>هر خام مرا بر سر گفتار نیارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلوی که چهل کس نتوانند کشیدن</p></div>
<div class="m2"><p>یک کس چه خیال است که از چاه برآرد؟</p></div></div>