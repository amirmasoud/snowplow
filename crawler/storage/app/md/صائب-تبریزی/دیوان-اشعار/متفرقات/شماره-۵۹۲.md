---
title: >-
    شمارهٔ ۵۹۲
---
# شمارهٔ ۵۹۲

<div class="b" id="bn1"><div class="m1"><p>تا چو سرزلف صد شکست نیابی</p></div>
<div class="m2"><p>دامن معشوق را به دست نیابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا ندهی خویش را تمام به علمی</p></div>
<div class="m2"><p>بعضی ازان را چنان که هست نیابی</p></div></div>