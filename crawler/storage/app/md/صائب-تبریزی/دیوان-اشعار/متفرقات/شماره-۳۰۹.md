---
title: >-
    شمارهٔ ۳۰۹
---
# شمارهٔ ۳۰۹

<div class="b" id="bn1"><div class="m1"><p>مگر پروانه حرفی از کنار و بوس می گوید؟</p></div>
<div class="m2"><p>که شمع امشب سخن از پرده فانوس می گوید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مزن حرف سبکباری که پیوند تعلق را</p></div>
<div class="m2"><p>یکایک بخیه های خرقه سالوس می گوید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل رنگین لباسی هاست خون خود هدر کردن</p></div>
<div class="m2"><p>پر زاغ این سخن را با پر طاوس می گوید</p></div></div>