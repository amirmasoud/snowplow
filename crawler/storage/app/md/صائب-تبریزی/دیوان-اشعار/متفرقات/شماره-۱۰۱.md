---
title: >-
    شمارهٔ ۱۰۱
---
# شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>فکر دنیای دنی کار خدانشناس است</p></div>
<div class="m2"><p>هر چه در دل گذرد غیر خدا وسواس است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لب ببند از سخن پوچ که صد پیراهن</p></div>
<div class="m2"><p>لاغری خوبتر از فربهی آماس است</p></div></div>