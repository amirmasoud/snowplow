---
title: >-
    شمارهٔ ۶۱۵
---
# شمارهٔ ۶۱۵

<div class="b" id="bn1"><div class="m1"><p>به کویش مشت خاکی می فرستم</p></div>
<div class="m2"><p>پیام دردناکی می فرستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دماغ نامه پردازی ندارم</p></div>
<div class="m2"><p>به مستان برگ تاکی می فرستم</p></div></div>