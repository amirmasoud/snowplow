---
title: >-
    شمارهٔ ۶۶۷
---
# شمارهٔ ۶۶۷

<div class="b" id="bn1"><div class="m1"><p>ختم است بر خرام تو راه نظر زدن</p></div>
<div class="m2"><p>چون آه صبحگاه به قلب جگر زدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خامشی گریز ز گفتار، زان که هست</p></div>
<div class="m2"><p>آن گل به جیب ریختن، این گل به سر زدن</p></div></div>