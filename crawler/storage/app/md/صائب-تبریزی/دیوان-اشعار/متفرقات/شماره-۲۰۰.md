---
title: >-
    شمارهٔ ۲۰۰
---
# شمارهٔ ۲۰۰

<div class="b" id="bn1"><div class="m1"><p>هر قدر نقاش نقش او به دقت می‌کشد</p></div>
<div class="m2"><p>چون نظر بر رویش اندازد خجالت می‌کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شعله جواله یک نقطه است چون ساکن شود</p></div>
<div class="m2"><p>سیر و دور سالکان آخر به وحدت می‌کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آبروی جرم اگر این است، در دیوان عفو</p></div>
<div class="m2"><p>عاصی از ناکرده بیش از کرده خجلت می‌کشد</p></div></div>