---
title: >-
    شمارهٔ ۶۵۹
---
# شمارهٔ ۶۵۹

<div class="b" id="bn1"><div class="m1"><p>محبت چون کند زورآزمایی</p></div>
<div class="m2"><p>شکست آرد به قلب مومیایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به رنگ و بو مناز ای گل که دارد</p></div>
<div class="m2"><p>خزان در آستین دست حنایی</p></div></div>