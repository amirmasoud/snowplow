---
title: >-
    شمارهٔ ۴۷۶
---
# شمارهٔ ۴۷۶

<div class="b" id="bn1"><div class="m1"><p>آن که از چاک دل خویش بود محرابش</p></div>
<div class="m2"><p>نشود پرده نسیان عبادت خوابش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوش عشق از لب من مهر خموشی برداشت</p></div>
<div class="m2"><p>این نه بحری است که در حقه کند گردابش</p></div></div>