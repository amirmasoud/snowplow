---
title: >-
    شمارهٔ ۲۵۵
---
# شمارهٔ ۲۵۵

<div class="b" id="bn1"><div class="m1"><p>دل سیاه ارباب غیرت را ز منت می‌شود</p></div>
<div class="m2"><p>شمع ما خاموش از دست حمایت می‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌شود شیطان پا بر جای دیگر بهر نفس</p></div>
<div class="m2"><p>در جهان آفرینش هرچه عادت می‌شود</p></div></div>