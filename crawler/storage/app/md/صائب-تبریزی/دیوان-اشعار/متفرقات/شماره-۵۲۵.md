---
title: >-
    شمارهٔ ۵۲۵
---
# شمارهٔ ۵۲۵

<div class="b" id="bn1"><div class="m1"><p>فرهادم و ثبات قدم هست پیشه ام</p></div>
<div class="m2"><p>ناخن دوانده در جگر سنگ تیشه ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بخت شور در نمک آبم چو مغز تلخ</p></div>
<div class="m2"><p>می روترش کند چو درآید به شیشه ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از ناخن آب دشنه الماس برده ام</p></div>
<div class="m2"><p>فرهاد را به کوه جهانده است تیشه ام</p></div></div>