---
title: >-
    شمارهٔ ۳۵۰
---
# شمارهٔ ۳۵۰

<div class="b" id="bn1"><div class="m1"><p>به حسن، خیرگی ما چه می تواند کرد؟</p></div>
<div class="m2"><p>به آفتاب، تماشا چه می تواند کرد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر دو یار موافق زبان یکی سازند</p></div>
<div class="m2"><p>فلک به یک تن تنها چه می تواند کرد؟</p></div></div>