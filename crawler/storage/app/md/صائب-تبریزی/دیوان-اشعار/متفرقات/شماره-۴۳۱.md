---
title: >-
    شمارهٔ ۴۳۱
---
# شمارهٔ ۴۳۱

<div class="b" id="bn1"><div class="m1"><p>این ریش پروران که گرفتار شانه اند</p></div>
<div class="m2"><p>غافل که صد خدنگ بلا را نشانه اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خانمان خرابی دل سعی می کنند</p></div>
<div class="m2"><p>این غافلان که در پی تعمیر خانه اند</p></div></div>