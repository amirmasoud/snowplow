---
title: >-
    شمارهٔ ۳۳۶
---
# شمارهٔ ۳۳۶

<div class="b" id="bn1"><div class="m1"><p>مصر روشن ز جمال مه کنعان نشود</p></div>
<div class="m2"><p>تا برافروخته از سیلی اخوان نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواریی هست به دنبال خودآرایی را</p></div>
<div class="m2"><p>پرطاوس محال است مگس ران نشود</p></div></div>