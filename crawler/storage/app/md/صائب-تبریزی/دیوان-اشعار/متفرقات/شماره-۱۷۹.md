---
title: >-
    شمارهٔ ۱۷۹
---
# شمارهٔ ۱۷۹

<div class="b" id="bn1"><div class="m1"><p>رخسار تو شاداب تر از لاله طورست</p></div>
<div class="m2"><p>شبنم گل سیراب ترا دیده شورست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسیار به از صحبت ابنای زمان است</p></div>
<div class="m2"><p>در مشرب من، خلوت اگر خلوت گورست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواری ز طمع دور نگردد که عصاکش</p></div>
<div class="m2"><p>هر چند که در پیش بود پیر و کورست</p></div></div>