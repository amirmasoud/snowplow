---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>(ای لعل تو جان بخش ترا ز عیسی مشرب</p></div>
<div class="m2"><p>چشم تو فریبنده تر از لولی مشرب)</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>(در خار و گل دهر به یک چشم نظر کن</p></div>
<div class="m2"><p>سرچشمه خورشید شو از معنی مشرب)</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>(چون ابر شب جمعه گران است به خاطر</p></div>
<div class="m2"><p>از خشک مزاجان ریا دعوی مشرب)</p></div></div>