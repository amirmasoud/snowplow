---
title: >-
    شمارهٔ ۳۱۶
---
# شمارهٔ ۳۱۶

<div class="b" id="bn1"><div class="m1"><p>حسن خط پرده فهمیدن مضمون گردد</p></div>
<div class="m2"><p>کسی آگاه ز مضمون خطش چون گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مصرع سرو به تقطیع چه حاجت دارد؟</p></div>
<div class="m2"><p>الف از صنعت مشاطه چه موزون گردد؟</p></div></div>