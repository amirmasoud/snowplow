---
title: >-
    شمارهٔ ۴۲۵
---
# شمارهٔ ۴۲۵

<div class="b" id="bn1"><div class="m1"><p>خالش بلای جان ز خط مشکبار شد</p></div>
<div class="m2"><p>پرهیز ازان ستاره که دنباله دار شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انصاف نیست سوختن از تشنگی مرا</p></div>
<div class="m2"><p>کز خون من عقیق لبت آبدار شد</p></div></div>