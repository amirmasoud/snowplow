---
title: >-
    شمارهٔ ۱۰۳
---
# شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>تا به کام است فلک، خار گل پیرهن است</p></div>
<div class="m2"><p>بخت تا سبز بود ساحت گلخن چمن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یوسف از خواری اخوان سر کنعانش نیست</p></div>
<div class="m2"><p>هر کجا بخت عزیزی دهد آنجا وطن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جوی شیر از جگر سنگ بریدن سهل است</p></div>
<div class="m2"><p>هر که بر پای هوس تیشه زند کوهکن است</p></div></div>