---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>چنان ز ساده دلی ها رمیده ام ز کتاب</p></div>
<div class="m2"><p>که بوی خون به مشامم رسید ز سرخی باب!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تمام شب به خیال تو عشق می بازم</p></div>
<div class="m2"><p>ز سادگی به کتان صاف می کنم مهتاب</p></div></div>