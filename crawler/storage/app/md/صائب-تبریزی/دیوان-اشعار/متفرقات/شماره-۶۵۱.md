---
title: >-
    شمارهٔ ۶۵۱
---
# شمارهٔ ۶۵۱

<div class="b" id="bn1"><div class="m1"><p>زهی نگاه تو با فتنه گرم همدوشی</p></div>
<div class="m2"><p>به دور خط تو خورشید در سیه پوشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز قرب زلف دل آشفته بود، غافل ازین</p></div>
<div class="m2"><p>که در دو روز کشد کار خط به سرگوشی</p></div></div>