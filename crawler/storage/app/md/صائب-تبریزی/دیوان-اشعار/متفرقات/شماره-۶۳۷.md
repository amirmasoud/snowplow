---
title: >-
    شمارهٔ ۶۳۷
---
# شمارهٔ ۶۳۷

<div class="b" id="bn1"><div class="m1"><p>می بده ساقی که از فیض شراب صبحگاه</p></div>
<div class="m2"><p>نور می بارد ز روی آفتاب صبحگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقد انجم را به یک جام صبوحی می دهد</p></div>
<div class="m2"><p>خوب می داند فلک، قدر شراب صبحگاه</p></div></div>