---
title: >-
    شمارهٔ ۱۷۴
---
# شمارهٔ ۱۷۴

<div class="b" id="bn1"><div class="m1"><p>در گلشن وجود ره بوالفضول نیست</p></div>
<div class="m2"><p>برگ خزان رسیده او بی اصول نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داغ است عشق از دل بی آرزوی من</p></div>
<div class="m2"><p>خون می خورد کریم چو مهمان فضول نیست</p></div></div>