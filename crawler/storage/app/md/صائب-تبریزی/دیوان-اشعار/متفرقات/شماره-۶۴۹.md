---
title: >-
    شمارهٔ ۶۴۹
---
# شمارهٔ ۶۴۹

<div class="b" id="bn1"><div class="m1"><p>از سخن چند چو سی پاره پریشان گردی؟</p></div>
<div class="m2"><p>مهر زن بر لب گفتار که قرآن گردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جگر خود مخور از حسرت گلزار خلیل</p></div>
<div class="m2"><p>آتش خشم فروخور که گلستان گردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باش واله که درین دایره بی سر و پا</p></div>
<div class="m2"><p>می شوی مرکز اگر دیده حیران گردی</p></div></div>