---
title: >-
    شمارهٔ ۶۴۱
---
# شمارهٔ ۶۴۱

<div class="b" id="bn1"><div class="m1"><p>پا چو مجنون جمع اگر در دامن صحرا کنی</p></div>
<div class="m2"><p>می توانی رام لیلی را ز استغنا کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی تأمل می کنی در کار باطل عمر صرف</p></div>
<div class="m2"><p>چون به کار حق رسی امروز را فردا کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کار خود را راست کن با قامت همچون الف</p></div>
<div class="m2"><p>با قد خم چون میسر نیست سر بالا کنی</p></div></div>