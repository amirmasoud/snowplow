---
title: >-
    شمارهٔ ۵۴۱
---
# شمارهٔ ۵۴۱

<div class="b" id="bn1"><div class="m1"><p>ما حسرت دیدار تو در سینه شکستیم</p></div>
<div class="m2"><p>این خار هوس در دل آیینه شکستیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زهاد شکستند اگر شیشه ما را</p></div>
<div class="m2"><p>ما نیز طلسم شب آدینه شکستیم</p></div></div>