---
title: >-
    شمارهٔ ۳۸۶
---
# شمارهٔ ۳۸۶

<div class="b" id="bn1"><div class="m1"><p>می خرامید و صبوح از می بی غش زده بود</p></div>
<div class="m2"><p>لاله ای بود که سر از دل آتش زده بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای مه عید کجا گم شده بودی دیروز؟</p></div>
<div class="m2"><p>که کمان ابروی من دست به ترکش زده بود</p></div></div>