---
title: >-
    شمارهٔ ۳۴۲
---
# شمارهٔ ۳۴۲

<div class="b" id="bn1"><div class="m1"><p>دو دل شوم چو به زلفش مرا نگاه افتد</p></div>
<div class="m2"><p>چو رهروی که رهش بر سر دو راه افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فروتنی است برازنده از سرافرازان</p></div>
<div class="m2"><p>که خوشنماست شکستی که بر کلاه افتد</p></div></div>