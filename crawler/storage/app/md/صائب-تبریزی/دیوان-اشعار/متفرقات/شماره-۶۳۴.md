---
title: >-
    شمارهٔ ۶۳۴
---
# شمارهٔ ۶۳۴

<div class="b" id="bn1"><div class="m1"><p>خوش است چاشنی سود در زیان دیدن</p></div>
<div class="m2"><p>رخ بهار در آیینه خزان دیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه خوب کرد که بلبل خزان ز گلشن رفت</p></div>
<div class="m2"><p>شکسته رنگی گل را نمی توان دیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رویت که شست چهره به آتش نقاب ازو</p></div>
<div class="m2"><p>شد مشرق ستاره و مه آفتاب ازو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم حیا مدار ز خوبان، که آینه است</p></div>
<div class="m2"><p>امروز دیده ای که نرفته است آب ازو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جرات نگر که در قدح موم کرده ایم</p></div>
<div class="m2"><p>آن باده ای که هست بط می کباب ازو</p></div></div>