---
title: >-
    شمارهٔ ۲۰۳
---
# شمارهٔ ۲۰۳

<div class="b" id="bn1"><div class="m1"><p>داستان عمر طی شد حرف او آخر نشد</p></div>
<div class="m2"><p>برگریزان زبان شد، گفتگو آخر نشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد به هم پیچیده طومار حیات جاودان</p></div>
<div class="m2"><p>داستان زلف بی پایان او آخر نشد</p></div></div>