---
title: >-
    شمارهٔ ۲۵۷
---
# شمارهٔ ۲۵۷

<div class="b" id="bn1"><div class="m1"><p>کی دلِ دیوانهٔ من رام آهو می‌شود؟</p></div>
<div class="m2"><p>صحبت من قال از چشم سخنگو می‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر فرو نارد به اسباب دو عالم همتش</p></div>
<div class="m2"><p>از دل هرکس که تیر او ترازو می‌شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیرت بد، صورت نیکو نمی‌گیرد به خود</p></div>
<div class="m2"><p>خشم چون صورت پذیرد چین ابرو می‌شود</p></div></div>