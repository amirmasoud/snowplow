---
title: >-
    شمارهٔ ۴۵۴
---
# شمارهٔ ۴۵۴

<div class="b" id="bn1"><div class="m1"><p>گریهٔ ما دل به طوفان می‌دهد</p></div>
<div class="m2"><p>نالهٔ ما جان به افغان می‌دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلبلان را داغ دارد باغبان</p></div>
<div class="m2"><p>کاو سزای هرزه‌گویان می‌دهد</p></div></div>