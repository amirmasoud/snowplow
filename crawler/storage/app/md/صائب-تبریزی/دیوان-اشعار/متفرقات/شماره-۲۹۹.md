---
title: >-
    شمارهٔ ۲۹۹
---
# شمارهٔ ۲۹۹

<div class="b" id="bn1"><div class="m1"><p>دل سودایی من یار را اغیار می داند</p></div>
<div class="m2"><p>سر زانوی وحدت را سر بازار می داند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز روشن گوهران عیب نمایان است غمازی</p></div>
<div class="m2"><p>وگرنه سینه ام آیینه را ستار می داند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کند شاخ بلند از کودکان گل را سپرداری</p></div>
<div class="m2"><p>سر سودایی منصور قدر دار می داند</p></div></div>