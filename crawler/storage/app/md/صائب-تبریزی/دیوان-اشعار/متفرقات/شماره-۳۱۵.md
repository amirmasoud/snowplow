---
title: >-
    شمارهٔ ۳۱۵
---
# شمارهٔ ۳۱۵

<div class="b" id="bn1"><div class="m1"><p>اگر ابروی تو محراب نمازم گردد</p></div>
<div class="m2"><p>کعبه پروانه صفت گرد نیازم گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گریبان نرسد نکهت دامن دارش</p></div>
<div class="m2"><p>جامه یوسف اگر پرده رازم گردد</p></div></div>