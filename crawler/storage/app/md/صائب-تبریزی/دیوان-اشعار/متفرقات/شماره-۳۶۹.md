---
title: >-
    شمارهٔ ۳۶۹
---
# شمارهٔ ۳۶۹

<div class="b" id="bn1"><div class="m1"><p>به تیغ هر که شود کشته پایدار شود</p></div>
<div class="m2"><p>رسد چو قطره به دریا یکی هزار شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز چارپای عناصر پیاده هر کس شد</p></div>
<div class="m2"><p>به یک نفس چو مسیحا فلک سوار شود</p></div></div>