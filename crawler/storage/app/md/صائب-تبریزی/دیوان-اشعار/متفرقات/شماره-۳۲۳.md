---
title: >-
    شمارهٔ ۳۲۳
---
# شمارهٔ ۳۲۳

<div class="b" id="bn1"><div class="m1"><p>نمک لعل تو کی چشمه حیوان دارد؟</p></div>
<div class="m2"><p>یوسف مصر کی این چاه زنخدان دارد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راز این سینه صد چاک چرا گل نکند؟</p></div>
<div class="m2"><p>که دریده دهنی همچو گریبان دارد</p></div></div>