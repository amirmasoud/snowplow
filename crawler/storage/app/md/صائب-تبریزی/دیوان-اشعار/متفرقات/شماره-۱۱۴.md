---
title: >-
    شمارهٔ ۱۱۴
---
# شمارهٔ ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>(دلم ز سینه به آن زلف تابدار گریخت</p></div>
<div class="m2"><p>ز چارموجه غم در دهان مار گریخت)</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>(خوشا کسی که ازین سایه های پا به رکاب</p></div>
<div class="m2"><p>به زیر سایه آن سرو پایدار گریخت)</p></div></div>