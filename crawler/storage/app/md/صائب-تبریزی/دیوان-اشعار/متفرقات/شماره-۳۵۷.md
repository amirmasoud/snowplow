---
title: >-
    شمارهٔ ۳۵۷
---
# شمارهٔ ۳۵۷

<div class="b" id="bn1"><div class="m1"><p>دل رمیده به این خاکدان نمی سازد</p></div>
<div class="m2"><p>به هیچ وجه شرر با دخان نمی سازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خارخار قفس بلبلی که خوی گرفت</p></div>
<div class="m2"><p>دگر به خار و خس آشیان نمی سازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فغان من که دل سنگ را به درد آرد</p></div>
<div class="m2"><p>غنیمت است ترا مهربان نمی سازد</p></div></div>