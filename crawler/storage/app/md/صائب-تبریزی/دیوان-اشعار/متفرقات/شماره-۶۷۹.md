---
title: >-
    شمارهٔ ۶۷۹
---
# شمارهٔ ۶۷۹

<div class="b" id="bn1"><div class="m1"><p>ای صبا احوال ما با پاسبان او بگو</p></div>
<div class="m2"><p>اشتیاق سجده را با آستان او بگو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کجا آن شاخ گل را ناز بگشاید کمر</p></div>
<div class="m2"><p>شکوه آغوش ما را با میان او بگو</p></div></div>