---
title: >-
    شمارهٔ ۴۶۱
---
# شمارهٔ ۴۶۱

<div class="b" id="bn1"><div class="m1"><p>ای ز رویت هر نظر محو تماشای دگر</p></div>
<div class="m2"><p>در دل هر ذره ای خورشید سیمای دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون رود بیرون ز باغ آن یوسف گل پیرهن</p></div>
<div class="m2"><p>می شود هر شاخ گل دست زلیخای دگر</p></div></div>