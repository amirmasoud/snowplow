---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>باقی به حق، ز خویش فنا می کند ترا</p></div>
<div class="m2"><p>از عشق غافلی که چها می کند ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این گردنی که همچو هدف برکشیده ای</p></div>
<div class="m2"><p>آماجگاه تیر قضا می کند ترا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگذر ز فکر پوچ تعین که این خیال</p></div>
<div class="m2"><p>از بحر چون حباب جدا می کند ترا</p></div></div>