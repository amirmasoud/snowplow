---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>کو باده تا به سنگ زنم جام عقل را؟</p></div>
<div class="m2"><p>از خط جام، حلقه کنم نام عقل را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمری که در ملال رود در حساب نیست</p></div>
<div class="m2"><p>چون بشمرم ز عمر خود ایام عقل را؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تفسیده تر ز ریگ روان است مغز ما</p></div>
<div class="m2"><p>ضایع مساز روغن بادام عقل را</p></div></div>