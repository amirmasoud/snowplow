---
title: >-
    شمارهٔ ۴۸۴
---
# شمارهٔ ۴۸۴

<div class="b" id="bn1"><div class="m1"><p>در گرد خط نهان شد روی عرق فشانش</p></div>
<div class="m2"><p>خط غبار گردید دیوار گلستانش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کوتاه بود دستم تا داشت اختیاری</p></div>
<div class="m2"><p>قالب چو کرد خالی شد بهله میانش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن شوخ پاکدامن تا لب ز باده تر ساخت</p></div>
<div class="m2"><p>بوی امیدواری می آید از دهانش</p></div></div>