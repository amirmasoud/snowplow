---
title: >-
    شمارهٔ ۱۷۳
---
# شمارهٔ ۱۷۳

<div class="b" id="bn1"><div class="m1"><p>بر حسن زود سیر بهار اعتماد نیست</p></div>
<div class="m2"><p>بر اعتدال لیل و نهار اعتماد نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در چارسوی جسم مزن خیمه ثبات</p></div>
<div class="m2"><p>بر ابر و برق و باد و غبار اعتماد نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ایمن مشو ز فتنه آن حسن در نقاب</p></div>
<div class="m2"><p>بر ابر و آفتاب بهار اعتماد نیست</p></div></div>