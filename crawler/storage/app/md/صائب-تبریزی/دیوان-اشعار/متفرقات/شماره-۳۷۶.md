---
title: >-
    شمارهٔ ۳۷۶
---
# شمارهٔ ۳۷۶

<div class="b" id="bn1"><div class="m1"><p>نی اگر از دل پررخنه صدایی نزند</p></div>
<div class="m2"><p>راه عشاق ترا هیچ نوایی نزند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می زند مار به هر عضو، ولی نی ماری است</p></div>
<div class="m2"><p>که به غیر از دل آگاه به جایی نزند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون ما را که دل آهن ازو جوش کند</p></div>
<div class="m2"><p>جوهر تیغ محال است که خس پوش کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل بی طاقت ما صبر ندارد، ورنه</p></div>
<div class="m2"><p>حسن از آیینه محال است فراموش کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیچ نفرین به ازین نیست که عاقل گردد</p></div>
<div class="m2"><p>هر که حق نمک عشق فراموش کند</p></div></div>