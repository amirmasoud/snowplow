---
title: >-
    شمارهٔ ۳۹۹
---
# شمارهٔ ۳۹۹

<div class="b" id="bn1"><div class="m1"><p>خطت ز پیش چشم سوادم نمی رود</p></div>
<div class="m2"><p>دلسوزی رخ تو ز یادم نمی رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر جلوه ای که دیده ام از سروقامتی</p></div>
<div class="m2"><p>چون مصرع بلند ز یادم نمی رود</p></div></div>