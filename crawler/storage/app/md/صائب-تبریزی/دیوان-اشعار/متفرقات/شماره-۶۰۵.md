---
title: >-
    شمارهٔ ۶۰۵
---
# شمارهٔ ۶۰۵

<div class="b" id="bn1"><div class="m1"><p>نیست هر آیینه را تاب رخ گلرنگ او</p></div>
<div class="m2"><p>هم مگر آیینه سازند از دل چون سنگ او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در شب تاریک نتوان دزد را دنبال رفت</p></div>
<div class="m2"><p>دل گرفتن مشکل است از طره شبرنگ او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از لطافت نسبت رخسار او با گل خطاست</p></div>
<div class="m2"><p>کز نگاه گرم گردد آفتابی، رنگ او</p></div></div>