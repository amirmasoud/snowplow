---
title: >-
    شمارهٔ ۳۳۴
---
# شمارهٔ ۳۳۴

<div class="b" id="bn1"><div class="m1"><p>مرغ من در بغل بیضه هم آزاد نبود</p></div>
<div class="m2"><p>آشیان هیچ کم از خانه صیاد نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل بی درد من از خواب فراموشی جست</p></div>
<div class="m2"><p>نامه دوست کم از سیلی استاد نبود</p></div></div>