---
title: >-
    شمارهٔ ۱۴۲
---
# شمارهٔ ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>زهرست بی تبسم شیرین شراب تلخ</p></div>
<div class="m2"><p>با بخت شور چند توان خورد آب تلخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی می نیم شکفته، همانا بریده اند</p></div>
<div class="m2"><p>همچون پیاله ناف مرا با شراب تلخ</p></div></div>