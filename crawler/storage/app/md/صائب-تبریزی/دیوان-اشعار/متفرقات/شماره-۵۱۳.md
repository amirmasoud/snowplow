---
title: >-
    شمارهٔ ۵۱۳
---
# شمارهٔ ۵۱۳

<div class="b" id="bn1"><div class="m1"><p>بر سر خوان امل دست هوس می بندم</p></div>
<div class="m2"><p>در شکرزار، پر و بال مگس می بندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوق در هیچ مقامی نکند آسایش</p></div>
<div class="m2"><p>در گلستانم و احرام قفس می بندم</p></div></div>