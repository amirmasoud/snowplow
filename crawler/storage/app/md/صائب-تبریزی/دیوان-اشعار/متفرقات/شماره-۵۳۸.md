---
title: >-
    شمارهٔ ۵۳۸
---
# شمارهٔ ۵۳۸

<div class="b" id="bn1"><div class="m1"><p>گرم نصیحت دل دیوانه خودیم</p></div>
<div class="m2"><p>سنگیم و در کمینگه پیمانه خودیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همت بلند مرتبه از آبروی ماست</p></div>
<div class="m2"><p>ما خوشه چین خرمن بی دانه خودیم</p></div></div>