---
title: >-
    شمارهٔ ۱۵۲
---
# شمارهٔ ۱۵۲

<div class="b" id="bn1"><div class="m1"><p>ترخنده از عرق به می ناب زد رخت</p></div>
<div class="m2"><p>باز این چه نقش بود که بر آب زد رخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاقوت های راز نهان رنگ باختند</p></div>
<div class="m2"><p>زین آتشی که در دل احباب زد رخت</p></div></div>