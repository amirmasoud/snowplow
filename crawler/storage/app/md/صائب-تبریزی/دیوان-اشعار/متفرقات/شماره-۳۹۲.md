---
title: >-
    شمارهٔ ۳۹۲
---
# شمارهٔ ۳۹۲

<div class="b" id="bn1"><div class="m1"><p>تا چند رخ ز باده کسی لاله گون کند؟</p></div>
<div class="m2"><p>تا کی کسی شناه به دریای خون کند؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کس ز عشق سایه دستی گرفته است</p></div>
<div class="m2"><p>چون تیشه دست در کمر بیستون کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزی که چرخ پنبه گذارد به داغ ما</p></div>
<div class="m2"><p>خورشید سر ز روزن مغرب برون کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اول شکار لاغر آن صید پیشه ایم</p></div>
<div class="m2"><p>ما را مگر شهید برای شگون کند</p></div></div>