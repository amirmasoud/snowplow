---
title: >-
    شمارهٔ ۴۴۴
---
# شمارهٔ ۴۴۴

<div class="b" id="bn1"><div class="m1"><p>تا صدق طلب خضر من آبله پا بود</p></div>
<div class="m2"><p>هر موجه ای از ریگ روان قبله نما بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از کشمکش عشق رسیدیم به دولت</p></div>
<div class="m2"><p>این اره به فرق سر ما بال هما بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر داد به صحرا دل دیوانه ما را</p></div>
<div class="m2"><p>آن گنج که در گوشه ویرانه ما بود</p></div></div>