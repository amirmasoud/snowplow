---
title: >-
    شمارهٔ ۶۰۱
---
# شمارهٔ ۶۰۱

<div class="b" id="bn1"><div class="m1"><p>هر نغمه طرازی نرباید دل مستان</p></div>
<div class="m2"><p>از ناله نی وجد کند محمل مستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از طاق فرود آید و در پای خم افتد</p></div>
<div class="m2"><p>خشتی که سرانجام کنند از گل مستان</p></div></div>