---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>در زیر بار مهره گل نیست دست ما</p></div>
<div class="m2"><p>ز اشک تاک سبحه کند می پرست ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه گوشه کلاه و نه زلف و نه توبه ایم</p></div>
<div class="m2"><p>خوبان چه بسته اند کمر در شکست ما؟</p></div></div>