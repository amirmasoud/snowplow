---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>تا نگیرم بوسه از لب گلعذار خویش را</p></div>
<div class="m2"><p>نشکنم از چشمه کوثر خمار خویش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون کند برق تجلی پای شوخی در رکاب</p></div>
<div class="m2"><p>کوه نتواند نگه دارد وقار خویش را</p></div></div>