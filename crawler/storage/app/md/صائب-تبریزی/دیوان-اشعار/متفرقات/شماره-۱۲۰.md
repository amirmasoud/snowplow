---
title: >-
    شمارهٔ ۱۲۰
---
# شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>ستاره سحر عشق چشم بیدارست</p></div>
<div class="m2"><p>غبار لشکر غم ناله شرربارست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلی که نیست در او شور عشق، ناقوس است</p></div>
<div class="m2"><p>رگی که نیست در او پیچ تاب، زنارست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدم ز دایره خود برون منه صائب</p></div>
<div class="m2"><p>که حصن عافیت نقطه خط پرگارست</p></div></div>