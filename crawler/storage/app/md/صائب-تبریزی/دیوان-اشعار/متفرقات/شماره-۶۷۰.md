---
title: >-
    شمارهٔ ۶۷۰
---
# شمارهٔ ۶۷۰

<div class="b" id="bn1"><div class="m1"><p>تیغ دو دم بود لب خندان به چشم من</p></div>
<div class="m2"><p>شاخ گل است زخم نمایان به چشم من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گشته است از نظاره آن سرو جامه زیب</p></div>
<div class="m2"><p>سوهان روح، سرو گلستان به چشم من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبح دگر شود ز پی خواب غفلتم</p></div>
<div class="m2"><p>خالی اگر کنند نمکدان به چشم من</p></div></div>