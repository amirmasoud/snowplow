---
title: >-
    شمارهٔ ۳۴۰
---
# شمارهٔ ۳۴۰

<div class="b" id="bn1"><div class="m1"><p>مگر از ناله بلبل دل ما بگشاید</p></div>
<div class="m2"><p>ورنه پیداست چه از باد صبا بگشاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می تواند گره از غنچه پیکان وا کرد</p></div>
<div class="m2"><p>هر نسیمی که دل تنگ مرا بگشاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبح را بخیه انجم نشود مهر دهن</p></div>
<div class="m2"><p>نتوان بست دری را که خدا بگشاید</p></div></div>