---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>چه نسبت است به روی تو روی آینه را؟</p></div>
<div class="m2"><p>که خشک کرد فروغ تو جوی آینه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به یاد روی تو با گل خوشم که طوطی مست</p></div>
<div class="m2"><p>به یک نظر نگرد پشت و روی آینه را</p></div></div>