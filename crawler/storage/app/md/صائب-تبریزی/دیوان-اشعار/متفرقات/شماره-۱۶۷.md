---
title: >-
    شمارهٔ ۱۶۷
---
# شمارهٔ ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>دل بی خیال طایر شهپر بریده است</p></div>
<div class="m2"><p>بی فکر روح پای به دامن کشیده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معیار آرمیدگی مجلس است شمع</p></div>
<div class="m2"><p>تا دل بجاست وضع جهان آرمیده است</p></div></div>