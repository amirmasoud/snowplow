---
title: >-
    شمارهٔ ۵۲۲
---
# شمارهٔ ۵۲۲

<div class="b" id="bn1"><div class="m1"><p>برون ز شیشه افلاک همچو رنگ زدیم</p></div>
<div class="m2"><p>به عالمی که در او رنگ نیست چنگ زدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکست بر دل ما آن زمان گوارا شد</p></div>
<div class="m2"><p>که مومیایی احباب را به سنگ زدیم</p></div></div>