---
title: >-
    شمارهٔ ۳۶۲
---
# شمارهٔ ۳۶۲

<div class="b" id="bn1"><div class="m1"><p>کجا به سیر چمن با رخ گشاده نشد</p></div>
<div class="m2"><p>که گل ز شاخ به تعظیم او پیاده نشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار ناخن الماس ریشه کرد و هنوز</p></div>
<div class="m2"><p>ز زلف طالع ما یک گره گشاده نشد</p></div></div>