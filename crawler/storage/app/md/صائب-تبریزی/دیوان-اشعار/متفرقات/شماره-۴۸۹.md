---
title: >-
    شمارهٔ ۴۸۹
---
# شمارهٔ ۴۸۹

<div class="b" id="bn1"><div class="m1"><p>بازآ که بی تو رنگ نیاید به روی گل</p></div>
<div class="m2"><p>در جیب غنچه زنگ برآورد بوی گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گلستان حسن تو از جوش عندلیب</p></div>
<div class="m2"><p>تنگ است جای بال فشانی به بوی گل</p></div></div>