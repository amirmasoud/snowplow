---
title: >-
    شمارهٔ ۶۵۰
---
# شمارهٔ ۶۵۰

<div class="b" id="bn1"><div class="m1"><p>کند چه نشو و نما دانه زمین گیری</p></div>
<div class="m2"><p>که در گذار ندیده است ابر تصویری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مبرهن است ز شبنم ربایی خورشید</p></div>
<div class="m2"><p>که در بساط فلک نیست دیده سیری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هر که نیست به حق آشنا، ندارد کار</p></div>
<div class="m2"><p>ندیده ام چو سگ نفس آشنا گیری</p></div></div>