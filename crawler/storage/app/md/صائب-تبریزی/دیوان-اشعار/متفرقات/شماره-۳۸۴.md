---
title: >-
    شمارهٔ ۳۸۴
---
# شمارهٔ ۳۸۴

<div class="b" id="bn1"><div class="m1"><p>داغ سودای ترا بر دل بی کینه نهند</p></div>
<div class="m2"><p>گوهری را که عزیزست به گنجینه نهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی تو جمعی که نظر آب دهند از گلزار</p></div>
<div class="m2"><p>تشنگانند که بر ریگ روان سینه نهند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قسمت مردم هموار نگردد سختی</p></div>
<div class="m2"><p>بالش از موم به زیر سر آیینه نهند</p></div></div>