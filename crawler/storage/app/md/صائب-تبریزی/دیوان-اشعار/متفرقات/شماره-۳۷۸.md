---
title: >-
    شمارهٔ ۳۷۸
---
# شمارهٔ ۳۷۸

<div class="b" id="bn1"><div class="m1"><p>عشق چون شعله کشد اشک دمادم چه کند؟</p></div>
<div class="m2"><p>پیش خورشید صف آرایی شبنم چه کند؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از جگر تشنگیم ریگ روان سیراب است</p></div>
<div class="m2"><p>با چو من سوخته ای چشمه زمزم چه کند؟</p></div></div>