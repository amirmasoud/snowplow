---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>بلبل به ثنای تو گشوده است زبان را</p></div>
<div class="m2"><p>گل غنچه به پابوس تو کرده است دهان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بندگی قامت موزون تو بسته است</p></div>
<div class="m2"><p>هر فاخته از طوق کمر سرو روان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر شاخ گل آماده به نظاره رویت</p></div>
<div class="m2"><p>از غنچه و شبنم دل و چشم نگران را</p></div></div>