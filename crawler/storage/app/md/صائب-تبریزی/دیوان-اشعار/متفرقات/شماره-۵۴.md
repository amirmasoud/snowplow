---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>ز ناز بوسه لب دلستان نداد مرا</p></div>
<div class="m2"><p>به لب رسید مرا جان و جان نداد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به صبر گفتم ازان لب، دهن شود شیرین</p></div>
<div class="m2"><p>خط از کمین بدر آمد امان نداد مرا</p></div></div>