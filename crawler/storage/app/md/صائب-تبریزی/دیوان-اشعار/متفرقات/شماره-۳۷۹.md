---
title: >-
    شمارهٔ ۳۷۹
---
# شمارهٔ ۳۷۹

<div class="b" id="bn1"><div class="m1"><p>آه را یاد سر زلف تو پیچیده کند</p></div>
<div class="m2"><p>فکر را شیوه رفتار تو سنجیده کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل محال است که با داغ هوس جوش زند</p></div>
<div class="m2"><p>کعبه حاشا که به بر جامه پوشیده کند</p></div></div>