---
title: >-
    شمارهٔ ۱۹۴
---
# شمارهٔ ۱۹۴

<div class="b" id="bn1"><div class="m1"><p>من کیم تا دست امیدم به آن دامن رسد؟</p></div>
<div class="m2"><p>این مرا بس کز رهش گردی به چشم من رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست هر گوشی حریف ناله جانسوز من</p></div>
<div class="m2"><p>آتشی کو تا به فریاد سپند من رسد؟</p></div></div>