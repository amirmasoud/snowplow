---
title: >-
    شمارهٔ ۵۹۵
---
# شمارهٔ ۵۹۵

<div class="b" id="bn1"><div class="m1"><p>به هرزه ناله و فریاد ای سپند مکن</p></div>
<div class="m2"><p>اگر ز سوختگانی صدا بلند مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مباد ز هر ندامت گزد زبان ترا</p></div>
<div class="m2"><p>به تلخکامی عشاق نوشخند مکن</p></div></div>