---
title: >-
    شمارهٔ ۶۷۴
---
# شمارهٔ ۶۷۴

<div class="b" id="bn1"><div class="m1"><p>برد تا رنگ حیا را باده از رخسار او</p></div>
<div class="m2"><p>آنچه بسیارست گلچین است در گلزار او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طره دستار او همسایه بال هماست</p></div>
<div class="m2"><p>پادشاهی کرد گل بر گوشه دستار او</p></div></div>