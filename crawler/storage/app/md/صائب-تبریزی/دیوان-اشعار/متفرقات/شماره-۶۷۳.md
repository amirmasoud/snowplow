---
title: >-
    شمارهٔ ۶۷۳
---
# شمارهٔ ۶۷۳

<div class="b" id="bn1"><div class="m1"><p>دل ز مهر بوالهوس آزاد کن</p></div>
<div class="m2"><p>شعله را از قید خس آزاد کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما حریف درد غربت نیستیم</p></div>
<div class="m2"><p>مرغ ما را با قفس آزاد کن!</p></div></div>