---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>ز هجران که دارد لاله داغی دلسیاهی را؟</p></div>
<div class="m2"><p>غزال دشت از چشم که دارد خوش نگاهی را؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکن در عشق منع دیده بیدار ما ناصح</p></div>
<div class="m2"><p>به خواب از دست نتوان داد ذوق پادشاهی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز شوق خال مشکینش به گرد کعبه می گردم</p></div>
<div class="m2"><p>که ره گم کرده خضری می شمارد هر سیاهی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر فردای محشر عفو میر عدل خواهد شد</p></div>
<div class="m2"><p>که ثابت می کند بر خود گناه بی گناهی را</p></div></div>