---
title: >-
    شمارهٔ ۴۲۹
---
# شمارهٔ ۴۲۹

<div class="b" id="bn1"><div class="m1"><p>مردم ز حد خویش برون پا نهاده‌اند</p></div>
<div class="m2"><p>راه هزار تفرقه بر خود گشاده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسته است روزگار جهان را به کار گل</p></div>
<div class="m2"><p>یکسر به فکر باغ و عمارت فتاده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواهند عاقبت ز ندامت به سر زدن</p></div>
<div class="m2"><p>دستی که ظالمان به تعدی گشاده‌اند</p></div></div>