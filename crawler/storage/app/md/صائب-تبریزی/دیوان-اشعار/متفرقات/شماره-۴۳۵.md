---
title: >-
    شمارهٔ ۴۳۵
---
# شمارهٔ ۴۳۵

<div class="b" id="bn1"><div class="m1"><p>از دیده حیرت زده چون آب نریزد؟</p></div>
<div class="m2"><p>از دست چسان آینه سیماب نریزد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کلبه من از پی آسایش (من) چرخ</p></div>
<div class="m2"><p>گورنگ ز خاکستر سنجاب نریزد</p></div></div>