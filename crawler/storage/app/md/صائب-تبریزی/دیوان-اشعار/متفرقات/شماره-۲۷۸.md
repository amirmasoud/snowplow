---
title: >-
    شمارهٔ ۲۷۸
---
# شمارهٔ ۲۷۸

<div class="b" id="bn1"><div class="m1"><p>نشد قسمت که چندان چشم شوخ او به ما سازد</p></div>
<div class="m2"><p>که مرغ زیرکی منقار با آب آشنا سازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگاه آن که بر آیینه روی تو می غلطد</p></div>
<div class="m2"><p>دمش آیینه آب گهر را بی صفا سازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخ مقصود از آیینه وقتی جلوه گر گردد</p></div>
<div class="m2"><p>که مالش استخوان پیکرت را رونما سازد</p></div></div>