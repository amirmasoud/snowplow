---
title: >-
    شمارهٔ ۳۳۰
---
# شمارهٔ ۳۳۰

<div class="b" id="bn1"><div class="m1"><p>قطع امید ازان زلف دوتا نتوان کرد</p></div>
<div class="m2"><p>دامن دولت جاوید رها نتوان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قاصد و نامه و پیغام نمی خواهد عشق</p></div>
<div class="m2"><p>در حرم پیروی قبله نما نتوان کرد</p></div></div>