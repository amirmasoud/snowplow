---
title: >-
    شمارهٔ ۲۶۳
---
# شمارهٔ ۲۶۳

<div class="b" id="bn1"><div class="m1"><p>خیال زلف او در دیده خونبار می زیبد</p></div>
<div class="m2"><p>خرام موج در دامان دریا بار می زیبد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز پیش چشم دل بردن، به زیر چشم دل دادن</p></div>
<div class="m2"><p>به خال گوشه چشم تو ای پرکار می زیبد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به کار گل نبندد اهل دل را هیچ کس زاهد</p></div>
<div class="m2"><p>ترا تسبیح بر گردن، مرا زنار می زیبد</p></div></div>