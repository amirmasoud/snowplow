---
title: >-
    شمارهٔ ۲۴۱
---
# شمارهٔ ۲۴۱

<div class="b" id="bn1"><div class="m1"><p>داغ عاشق سازگاری کی به مرهم می‌کند؟</p></div>
<div class="m2"><p>لاله این باغ خون در چشم شبنم می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دولت گردنده دنیا به استحقاق نیست</p></div>
<div class="m2"><p>دور گردون دیو را در دست، خاتم می‌کند</p></div></div>