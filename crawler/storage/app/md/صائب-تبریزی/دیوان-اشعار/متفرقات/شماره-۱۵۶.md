---
title: >-
    شمارهٔ ۱۵۶
---
# شمارهٔ ۱۵۶

<div class="b" id="bn1"><div class="m1"><p>چون غنچه گر زبان تو با دل موافق است</p></div>
<div class="m2"><p>بر کاینات از ته ل خنده لایق است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در وقت صبح چرخ نفس راست می کند</p></div>
<div class="m2"><p>یعنی که غمگسار جهان یار صادق است</p></div></div>