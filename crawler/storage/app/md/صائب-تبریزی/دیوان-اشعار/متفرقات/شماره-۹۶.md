---
title: >-
    شمارهٔ ۹۶
---
# شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>تار و پود فلک از ناله پیچیده ماست</p></div>
<div class="m2"><p>پیله اطلس گردون دل غم دیده ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نظر همت ما وسعت دیگر دارد</p></div>
<div class="m2"><p>آسمان مردمک چشم جهان دیده ماست</p></div></div>