---
title: >-
    شمارهٔ ۵۷۷
---
# شمارهٔ ۵۷۷

<div class="b" id="bn1"><div class="m1"><p>بی سبب بر سرم ای عربده ساز آمده ای</p></div>
<div class="m2"><p>از دل من چه به جا مانده که باز آمده ای؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز ره صبر چه پوشم، که (ز) الماس نگاه</p></div>
<div class="m2"><p>سینه پردازتر از چنگل باز آمده ای</p></div></div>