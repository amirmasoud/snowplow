---
title: >-
    شمارهٔ ۲۰۴
---
# شمارهٔ ۲۰۴

<div class="b" id="bn1"><div class="m1"><p>هیچ کس بی گوشمال روزگار آدم نشد</p></div>
<div class="m2"><p>غوطه تا در خون نزد شمشیر صاحب دم نشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست گوهر را به از گرد یتیمی کسوتی</p></div>
<div class="m2"><p>از غبار خط صفای چهره او کم نشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از شکست خویش بالاتر نباشد هیچ فتح</p></div>
<div class="m2"><p>نیست استاد آن که از شاگرد خود ملزم نشد</p></div></div>