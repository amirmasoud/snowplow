---
title: >-
    شمارهٔ ۴۲۴
---
# شمارهٔ ۴۲۴

<div class="b" id="bn1"><div class="m1"><p>آخر غبار خط تو گرد کساد شد</p></div>
<div class="m2"><p>جوهر به چشم آینه موی زیاد شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر عاشقی که شیوه وارستگی شناخت</p></div>
<div class="m2"><p>چون بنده گریخته بی اعتماد شد</p></div></div>