---
title: >-
    شمارهٔ ۳۲۴
---
# شمارهٔ ۳۲۴

<div class="b" id="bn1"><div class="m1"><p>اشک ما آتش حل کرده به دامن دارد</p></div>
<div class="m2"><p>دانه سوختگان برق به خرمن دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با کلاه نمد خویش بسازید که شمع</p></div>
<div class="m2"><p>تاج بر طرف سر و اشک به دامن دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن به سرچشمه مقصود تواند ره برد</p></div>
<div class="m2"><p>که دلی تنگ تر از چشمه سوزن دارد</p></div></div>