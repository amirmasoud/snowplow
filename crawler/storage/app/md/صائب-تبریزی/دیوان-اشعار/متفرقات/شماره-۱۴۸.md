---
title: >-
    شمارهٔ ۱۴۸
---
# شمارهٔ ۱۴۸

<div class="b" id="bn1"><div class="m1"><p>قسم به شمع تجلی که پرتوش ازلی است</p></div>
<div class="m2"><p>که عشق تازه عذاران بهار زنده دلی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عزیزدار دل پاره پاره ما را</p></div>
<div class="m2"><p>که شمع را پر پروانه مصحف بغلی است</p></div></div>