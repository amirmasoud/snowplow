---
title: >-
    شمارهٔ ۴۷۵
---
# شمارهٔ ۴۷۵

<div class="b" id="bn1"><div class="m1"><p>فصل گل می گذرد بی قدح و جام مباش</p></div>
<div class="m2"><p>غنچه منشین، گره خاطر ایام مباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل و سنبل به از اسباب گرفتاری نیست</p></div>
<div class="m2"><p>گر به گلزار روی بی قفس و دام مباش</p></div></div>