---
title: >-
    شمارهٔ ۶۶۶
---
# شمارهٔ ۶۶۶

<div class="b" id="bn1"><div class="m1"><p>یکی هزار شد از عشق ناتمامی من</p></div>
<div class="m2"><p>ز آفتاب قیامت فزود خامی من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کمال چون مه نو بوته گدازم شد</p></div>
<div class="m2"><p>به از تمامی من بود ناتمامی من</p></div></div>