---
title: >-
    شمارهٔ ۵۷۸
---
# شمارهٔ ۵۷۸

<div class="b" id="bn1"><div class="m1"><p>از کجی ناخنه دیده انور گردی</p></div>
<div class="m2"><p>راست شو راست که سرو لب کوثر گردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نعل نان است در آتش ز پی گرسنگان</p></div>
<div class="m2"><p>چه ضرورست پی رزق به هر در گردی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خم نمودند قدت را که زمین گیر شوی</p></div>
<div class="m2"><p>نه که از بی بصری حلقه هر در گردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوالهوس نیست به لطف تو سزاوار، ولی</p></div>
<div class="m2"><p>شرمت آید ز غلط کرده خود برگردی</p></div></div>