---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>گذشت عمر و نگردید پخته طینت ما</p></div>
<div class="m2"><p>به آفتاب قیامت فتاد نوبت ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صدای آب روان خواب را گران سازد</p></div>
<div class="m2"><p>ز خوش عنانی عمرست خواب غفلت ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مقام نشو و نما نیست این نشیمن پست</p></div>
<div class="m2"><p>مگر به ریشه کند زور، نخل همت ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کباب پرتو منت نمی توان گردید</p></div>
<div class="m2"><p>بس است دیده بیدار، شمع خلوت ما</p></div></div>