---
title: >-
    شمارهٔ ۱۲۹
---
# شمارهٔ ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>اندیشه ز مستی نکند هر که شرابی است</p></div>
<div class="m2"><p>کآبادی این طایفه موقوف خرابی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن را که به انگشت توان عیب شمردن</p></div>
<div class="m2"><p>در عالم انصاف ز مردان حسابی است!</p></div></div>