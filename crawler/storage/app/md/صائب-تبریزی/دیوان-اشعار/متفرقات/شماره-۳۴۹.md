---
title: >-
    شمارهٔ ۳۴۹
---
# شمارهٔ ۳۴۹

<div class="b" id="bn1"><div class="m1"><p>ز خط صفا لب میگون یار پیدا کرد</p></div>
<div class="m2"><p>بهار نشأه این باده را دوبالا کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا به دست تهی همچو شانه می باید</p></div>
<div class="m2"><p>گره ز کار پریشان عالمی وا کرد</p></div></div>