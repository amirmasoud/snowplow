---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>لعل سیراب تو ترخنده به صهبا زده است</p></div>
<div class="m2"><p>نگهت زهر به سرچشمه مینا زده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوهر جرات من در صدف طوفان نیست</p></div>
<div class="m2"><p>بارها قطره من بر صف دریا زده است</p></div></div>