---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>(مده ز دست درین فصل جام صهبا را</p></div>
<div class="m2"><p>که موج لاله به می شست روی صحرا را)</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>(جنون ما به نسیم بهانه ای بندست</p></div>
<div class="m2"><p>بس است آتش گل دیگجوش سودا را)</p></div></div>