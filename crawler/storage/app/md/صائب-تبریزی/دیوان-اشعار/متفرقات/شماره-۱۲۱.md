---
title: >-
    شمارهٔ ۱۲۱
---
# شمارهٔ ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>ترا که خط بناگوش ابجد نازست</p></div>
<div class="m2"><p>چه وقت توبه حسن کرشمه پردازست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو چشم واله قربانیان پس از تسلیم</p></div>
<div class="m2"><p>دو شاهدست که انجام به ز آغازست</p></div></div>