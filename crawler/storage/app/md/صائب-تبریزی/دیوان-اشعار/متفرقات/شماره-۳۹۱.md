---
title: >-
    شمارهٔ ۳۹۱
---
# شمارهٔ ۳۹۱

<div class="b" id="bn1"><div class="m1"><p>ماه از حجاب روی تو پروین عرق کند</p></div>
<div class="m2"><p>آیینه را شکوه جمال تو شق کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اسکندرست اگر چه بود در لباس فقر</p></div>
<div class="m2"><p>هر کس که اختصار به سد رمق کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون با رخ تو چهره شود مه، که آفتاب</p></div>
<div class="m2"><p>از شرم عارضت ز شفق خون عرق کند</p></div></div>