---
title: >-
    شمارهٔ ۶۱۹
---
# شمارهٔ ۶۱۹

<div class="b" id="bn1"><div class="m1"><p>از نصیحت دم مزن با خاطر افگار من</p></div>
<div class="m2"><p>کز دوای تلخ بدخوتر شود بیمار من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوش یکرنگی ز من نام و نشان برداشته است</p></div>
<div class="m2"><p>می دهد آزار خود، هر کس دهد آزار من</p></div></div>