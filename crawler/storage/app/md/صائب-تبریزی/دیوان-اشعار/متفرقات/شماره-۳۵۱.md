---
title: >-
    شمارهٔ ۳۵۱
---
# شمارهٔ ۳۵۱

<div class="b" id="bn1"><div class="m1"><p>به آه سرد دل خود دو نیم باید کرد</p></div>
<div class="m2"><p>چو غنچه خنده به روی نسیم باید کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندا کند به زبان بریده زلف ایاز</p></div>
<div class="m2"><p>که پا دراز به حد گلیم باید کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلی که جمع ز ذکر خفی چو غنچه شود</p></div>
<div class="m2"><p>ز ذکر اره چه لازم دو نیم باید کرد؟</p></div></div>