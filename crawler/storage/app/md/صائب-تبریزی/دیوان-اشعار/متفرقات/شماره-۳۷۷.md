---
title: >-
    شمارهٔ ۳۷۷
---
# شمارهٔ ۳۷۷

<div class="b" id="bn1"><div class="m1"><p>دل چو آرایش مژگان تر خویش کند</p></div>
<div class="m2"><p>داغ را آینه دار جگر خویش کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می برد بخت به ظلمت کده هند مرا</p></div>
<div class="m2"><p>تا چه خاک (سیه) آنجا به سر خویش کند</p></div></div>