---
title: >-
    شمارهٔ ۶۴۶
---
# شمارهٔ ۶۴۶

<div class="b" id="bn1"><div class="m1"><p>عرق آلود ز می طرف جبین ساخته ای</p></div>
<div class="m2"><p>دیده ها را صدف در ثمین ساخته ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساده لوحی بود آیینه صد نقش مراد</p></div>
<div class="m2"><p>تو ز صد نقش به نامی چو نگین ساخته ای</p></div></div>