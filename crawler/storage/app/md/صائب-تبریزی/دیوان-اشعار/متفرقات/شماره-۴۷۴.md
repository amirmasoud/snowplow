---
title: >-
    شمارهٔ ۴۷۴
---
# شمارهٔ ۴۷۴

<div class="b" id="bn1"><div class="m1"><p>کاکل او درهم است از شورش سودای خویش</p></div>
<div class="m2"><p>از پریشانی ندارد زلف او پروای خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشأه مستی ز عمر جاودانی خوشترست</p></div>
<div class="m2"><p>خضر و آب زندگانی، ما و ته مینای خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در میان هر دو موزون آشنایی معنوی است</p></div>
<div class="m2"><p>سرو تا بالای او را دید جست از جای خویش!</p></div></div>