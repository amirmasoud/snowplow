---
title: >-
    شمارهٔ ۵۳۳
---
# شمارهٔ ۵۳۳

<div class="b" id="bn1"><div class="m1"><p>پیش قضا طلسم ز تدبیر بسته ایم</p></div>
<div class="m2"><p>سد شکر به رهگذر شیر بسته ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجنون خانه زاد بیابان وحشتیم</p></div>
<div class="m2"><p>دیوانگی به خود نه به زنجیر بسته ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حاشا که زخم ما دهن شکوه وا کند</p></div>
<div class="m2"><p>خود بر میان ناز تو شمشیر بسته ایم</p></div></div>