---
title: >-
    شمارهٔ ۳۹۳
---
# شمارهٔ ۳۹۳

<div class="b" id="bn1"><div class="m1"><p>چندان که ممکن است کسی مردمی کند</p></div>
<div class="m2"><p>دم را چرا علم کند و کژدمی کند؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نان جوی به سفره هر کس که هست ازوست</p></div>
<div class="m2"><p>آدم زبان خویش اگر گندمی کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طوفان نوح، کودک دریا ندیده ای است</p></div>
<div class="m2"><p>جایی که آب دیده ما قلزمی کند</p></div></div>