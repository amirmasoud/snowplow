---
title: >-
    شمارهٔ ۵۸۲
---
# شمارهٔ ۵۸۲

<div class="b" id="bn1"><div class="m1"><p>ز چهره تو چو خوشید نور می بارد</p></div>
<div class="m2"><p>اگر تو در دل شبها ستاره بار شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به اعتبار جهان التفات اگر نکنی</p></div>
<div class="m2"><p>به دیده همه کس ز اهل اعتبار شوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر ز نعمت الوان به خون شوی قانع</p></div>
<div class="m2"><p>چو نافه از نفس گرم مشکبار شوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فریب وعده بی حاصلان مخور صائب</p></div>
<div class="m2"><p>که همچو ساده دلان خرج انتظار شوی</p></div></div>