---
title: >-
    شمارهٔ ۳۱۳
---
# شمارهٔ ۳۱۳

<div class="b" id="bn1"><div class="m1"><p>زلف او کی به خیال من غمناک افتد؟</p></div>
<div class="m2"><p>مگر از شانه به فکر دل صد چاک افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرتو حسن غریب تو ازان شوخترست</p></div>
<div class="m2"><p>که ازو عکس بر آیینه ادراک افتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رشته گوهر سیراب شود مژگانش</p></div>
<div class="m2"><p>هر که را چشم بر آن روی عرقناک افتد</p></div></div>