---
title: >-
    شمارهٔ ۲۵۴
---
# شمارهٔ ۲۵۴

<div class="b" id="bn1"><div class="m1"><p>بی تو گر ساغر زنم خون در رگم نشتر شود</p></div>
<div class="m2"><p>بی دم تیغت اگر آبی خورم خنجر شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غیرت ما ناز از معشوق نتواند کشید</p></div>
<div class="m2"><p>بلبل مغرور ما از خنده گل، تر شود</p></div></div>