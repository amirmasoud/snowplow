---
title: >-
    شمارهٔ ۴۵۹
---
# شمارهٔ ۴۵۹

<div class="b" id="bn1"><div class="m1"><p>جان دگر ز بوسه دلدار یافتم</p></div>
<div class="m2"><p>عمر دوباره از دو لب یار یافتم</p></div></div>