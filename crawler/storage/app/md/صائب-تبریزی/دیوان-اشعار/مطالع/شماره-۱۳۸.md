---
title: >-
    شمارهٔ ۱۳۸
---
# شمارهٔ ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>باز سرمشق جنونم خط نازک رقمی است</p></div>
<div class="m2"><p>که دو نیم است ز عشقش دل هر جا قلمی است</p></div></div>