---
title: >-
    شمارهٔ ۴۶۴
---
# شمارهٔ ۴۶۴

<div class="b" id="bn1"><div class="m1"><p>ما آبروی فقر به گوهر نمی دهیم</p></div>
<div class="m2"><p>سد رمق به ملک سکندر نمی دهیم</p></div></div>