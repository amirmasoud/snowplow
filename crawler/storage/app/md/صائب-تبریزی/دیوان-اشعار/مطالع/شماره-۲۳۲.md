---
title: >-
    شمارهٔ ۲۳۲
---
# شمارهٔ ۲۳۲

<div class="b" id="bn1"><div class="m1"><p>سیه مستان غفلت را فلک هشیار می سازد</p></div>
<div class="m2"><p>درشتان را به گردش آسیا هموار می سازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مژگان زرد، خانه برانداز سینه است</p></div>
<div class="m2"><p>الماس در خراش جگر بی قرینه است</p></div></div>