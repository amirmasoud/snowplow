---
title: >-
    شمارهٔ ۴۲۰
---
# شمارهٔ ۴۲۰

<div class="b" id="bn1"><div class="m1"><p>ای خط سبز کز لب جانان دمیده ای</p></div>
<div class="m2"><p>بر آب زندگی خط باطل کشیده ای</p></div></div>