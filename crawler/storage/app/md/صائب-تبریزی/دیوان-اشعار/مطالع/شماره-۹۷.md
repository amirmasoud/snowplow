---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>کم کم کن آشنا به لب زخم دشنه را</p></div>
<div class="m2"><p>سیراب می کنند به تدریج تشنه را</p></div></div>