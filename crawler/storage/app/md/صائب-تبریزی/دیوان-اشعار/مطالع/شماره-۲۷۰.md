---
title: >-
    شمارهٔ ۲۷۰
---
# شمارهٔ ۲۷۰

<div class="b" id="bn1"><div class="m1"><p>ز ماه نوگشاد عقده دلها نمی آید</p></div>
<div class="m2"><p>گره وا کردن از یک ناخن تنها نمی آید</p></div></div>