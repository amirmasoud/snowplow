---
title: >-
    شمارهٔ ۳۴۰
---
# شمارهٔ ۳۴۰

<div class="b" id="bn1"><div class="m1"><p>در صیدگاه دنیا هر کس که هوش دارد</p></div>
<div class="m2"><p>جز عبرت آنچه باشد صید حرم شمارد</p></div></div>