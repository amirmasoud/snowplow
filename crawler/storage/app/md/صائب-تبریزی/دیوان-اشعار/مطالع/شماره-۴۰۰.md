---
title: >-
    شمارهٔ ۴۰۰
---
# شمارهٔ ۴۰۰

<div class="b" id="bn1"><div class="m1"><p>ز احسان بنای دولت خود باثبات کن</p></div>
<div class="m2"><p>دست گشاده را سپر حادثات کن</p></div></div>