---
title: >-
    شمارهٔ ۳۹۷
---
# شمارهٔ ۳۹۷

<div class="b" id="bn1"><div class="m1"><p>به طوق غبغب سیمین او نظر واکن</p></div>
<div class="m2"><p>هلال ماه در آغوش را تماشا کن</p></div></div>