---
title: >-
    شمارهٔ ۲۹۸
---
# شمارهٔ ۲۹۸

<div class="b" id="bn1"><div class="m1"><p>ز آفتاب شود خشک خط چو تر باشد</p></div>
<div class="m2"><p>خط عذار تو هر روز تازه تر باشد</p></div></div>