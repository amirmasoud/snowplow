---
title: >-
    شمارهٔ ۱۶۴
---
# شمارهٔ ۱۶۴

<div class="b" id="bn1"><div class="m1"><p>دیدار یار در گره چشم بستن است</p></div>
<div class="m2"><p>بند نقاب او ز دو عالم گسستن است</p></div></div>