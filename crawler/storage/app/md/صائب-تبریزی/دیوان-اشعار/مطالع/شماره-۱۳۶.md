---
title: >-
    شمارهٔ ۱۳۶
---
# شمارهٔ ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>دل دگربار در آن زلف دو تا افتاده است</p></div>
<div class="m2"><p>چشم بد دور که بسیار بجا افتاده است</p></div></div>