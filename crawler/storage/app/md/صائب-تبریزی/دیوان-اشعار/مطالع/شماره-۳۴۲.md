---
title: >-
    شمارهٔ ۳۴۲
---
# شمارهٔ ۳۴۲

<div class="b" id="bn1"><div class="m1"><p>دل ز من خال یار می گیرد</p></div>
<div class="m2"><p>حق به مرکز قرار می گیرد</p></div></div>