---
title: >-
    شمارهٔ ۱۷۵
---
# شمارهٔ ۱۷۵

<div class="b" id="bn1"><div class="m1"><p>حسن در دوستی یگانه خوش است</p></div>
<div class="m2"><p>رنگ معشوق، عاشقانه خوش است</p></div></div>