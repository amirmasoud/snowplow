---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>از دست کار رفته بود پیش، کار ما</p></div>
<div class="m2"><p>در برگریز جوش زند نوبهار ما</p></div></div>