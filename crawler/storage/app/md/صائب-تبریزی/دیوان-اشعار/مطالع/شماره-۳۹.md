---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>خط مشکین خواست عذر آن عذار ساده را</p></div>
<div class="m2"><p>سرمه ای در کار بود این چشم برف افتاده را</p></div></div>