---
title: >-
    شمارهٔ ۳۶۰
---
# شمارهٔ ۳۶۰

<div class="b" id="bn1"><div class="m1"><p>مخور فریب محبت ز ناله همه کس</p></div>
<div class="m2"><p>مشو چو شیشه می هم پیاله همه کس</p></div></div>