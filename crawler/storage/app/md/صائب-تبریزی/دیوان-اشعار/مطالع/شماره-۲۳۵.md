---
title: >-
    شمارهٔ ۲۳۵
---
# شمارهٔ ۲۳۵

<div class="b" id="bn1"><div class="m1"><p>رخسار او مقید زلف بلند نیست</p></div>
<div class="m2"><p>این صید پیشه را نظری با کمند نیست</p></div></div>