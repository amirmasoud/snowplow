---
title: >-
    شمارهٔ ۳۴۴
---
# شمارهٔ ۳۴۴

<div class="b" id="bn1"><div class="m1"><p>خسیس از هنرپیشگان عیب بیند</p></div>
<div class="m2"><p>مگس بیشتر بر جراحت نشیند</p></div></div>