---
title: >-
    شمارهٔ ۸۷
---
# شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>چرخ خونخوار دلیرست به خونریزی ما</p></div>
<div class="m2"><p>شش جهت پنجه شیرست به خونریزی ما</p></div></div>