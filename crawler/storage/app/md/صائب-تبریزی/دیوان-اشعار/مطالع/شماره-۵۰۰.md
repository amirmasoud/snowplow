---
title: >-
    شمارهٔ ۵۰۰
---
# شمارهٔ ۵۰۰

<div class="b" id="bn1"><div class="m1"><p>اگر چه لاله طورست روی روشن او</p></div>
<div class="m2"><p>چراغ روز بود با بیاض گردن او</p></div></div>