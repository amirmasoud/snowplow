---
title: >-
    شمارهٔ ۳۱۱
---
# شمارهٔ ۳۱۱

<div class="b" id="bn1"><div class="m1"><p>با جسم کس به عالم بالا نمی رسد</p></div>
<div class="m2"><p>دجال خرسوار به عیسی نمی رسد</p></div></div>