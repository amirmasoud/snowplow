---
title: >-
    شمارهٔ ۲۲۷
---
# شمارهٔ ۲۲۷

<div class="b" id="bn1"><div class="m1"><p>نهان در پرده هر موی من آه آتشین دارد</p></div>
<div class="m2"><p>رگ ابر بهاران برق را در آستین دارد</p></div></div>