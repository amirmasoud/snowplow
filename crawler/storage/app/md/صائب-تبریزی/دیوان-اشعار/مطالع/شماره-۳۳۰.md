---
title: >-
    شمارهٔ ۳۳۰
---
# شمارهٔ ۳۳۰

<div class="b" id="bn1"><div class="m1"><p>جز سرکشی از آدم بی درد چه خیزد؟</p></div>
<div class="m2"><p>از خاک فرومایه به جز گرد چه خیزد؟</p></div></div>