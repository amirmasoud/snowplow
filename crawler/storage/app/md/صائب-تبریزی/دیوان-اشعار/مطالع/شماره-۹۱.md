---
title: >-
    شمارهٔ ۹۱
---
# شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>حذر ز ناخن الماس نیست داغ مرا</p></div>
<div class="m2"><p>که برگریز بود برگ عیش باغ مرا</p></div></div>