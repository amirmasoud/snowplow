---
title: >-
    شمارهٔ ۲۹۶
---
# شمارهٔ ۲۹۶

<div class="b" id="bn1"><div class="m1"><p>اگر نه اشک مرادست بر گلو گیرد</p></div>
<div class="m2"><p>غبار خاطر من ماه را فرو گیرد</p></div></div>