---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>زشت‌رو چون سازد از خود دور خوی زشت را؟</p></div>
<div class="m2"><p>لازم افتاده است خوی زشت، روی زشت را</p></div></div>