---
title: >-
    شمارهٔ ۳۷۳
---
# شمارهٔ ۳۷۳

<div class="b" id="bn1"><div class="m1"><p>سلیمانی است حسن، انگشتری از حلقه مویش</p></div>
<div class="m2"><p>پریزادی است دست آموز، زلف آشنارویش</p></div></div>