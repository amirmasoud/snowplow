---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>تلاش بیهده ای می کند سر خورشید</p></div>
<div class="m2"><p>ستاده (فتاده؟) است بلند، آستان حضرت دوست</p></div></div>