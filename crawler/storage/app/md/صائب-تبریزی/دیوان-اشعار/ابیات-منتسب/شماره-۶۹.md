---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>افزود شوق بوسه مرا از لبان تو</p></div>
<div class="m2"><p>صفرای من زیاده شد از ناردان تو</p></div></div>