---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>به فریب کسی ز راه مرو</p></div>
<div class="m2"><p>یوسف من، اگر برادر توست</p></div></div>