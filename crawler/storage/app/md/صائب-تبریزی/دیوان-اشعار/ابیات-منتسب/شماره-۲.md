---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>خضر نتواند به آب زندگی از ما خرید</p></div>
<div class="m2"><p>منصب میرابی سرچشمه آیینه را</p></div></div>