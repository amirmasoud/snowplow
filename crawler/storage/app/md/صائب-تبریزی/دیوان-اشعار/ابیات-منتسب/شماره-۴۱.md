---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>تا شرم داشت منصب آیینه داریت</p></div>
<div class="m2"><p>گرداندن لباس تو تغییر رنگ بود</p></div></div>