---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>کرده ام از غیر خیال دوست خالی سینه را</p></div>
<div class="m2"><p>از غبار آرزو شستم دل بی کینه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسمان را دل ز رشک عشرتم خالی نشد</p></div>
<div class="m2"><p>تا نزد بر شیشه ام سنگ شب آدینه را</p></div></div>