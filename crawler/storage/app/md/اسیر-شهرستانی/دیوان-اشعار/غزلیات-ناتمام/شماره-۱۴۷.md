---
title: >-
    شمارهٔ ۱۴۷
---
# شمارهٔ ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>چرایی سرگران تقصیر ما چیست</p></div>
<div class="m2"><p>بکش، در آتش افکن، مدعا چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گذشت از یک تغافل روزگارم</p></div>
<div class="m2"><p>نمی دانم نگاه آشنا چیست</p></div></div>