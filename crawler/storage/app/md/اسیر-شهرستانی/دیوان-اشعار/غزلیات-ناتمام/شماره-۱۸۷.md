---
title: >-
    شمارهٔ ۱۸۷
---
# شمارهٔ ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>می‌گریزم سایه الفت‌شکاران کم مباد</p></div>
<div class="m2"><p>می‌سپارم جان نوازش‌های یاران کم مباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌کشان را بر سر خاکم بساطی چیده‌اند</p></div>
<div class="m2"><p>در غبار من گل ابر بهاران کم مباد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌روم از خود گلاب چاره‌ای در کار نیست</p></div>
<div class="m2"><p>از سر من سایه الفت‌شکاران کم مباد</p></div></div>