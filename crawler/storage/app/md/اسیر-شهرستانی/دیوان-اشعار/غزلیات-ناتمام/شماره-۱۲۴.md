---
title: >-
    شمارهٔ ۱۲۴
---
# شمارهٔ ۱۲۴

<div class="b" id="bn1"><div class="m1"><p>سربلندیها غباری بوده است</p></div>
<div class="m2"><p>آسمان هم خاکساری بوده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جلوه تا کرد آزمودم خویش را</p></div>
<div class="m2"><p>جانفشانی سهل کاری بوده است</p></div></div>