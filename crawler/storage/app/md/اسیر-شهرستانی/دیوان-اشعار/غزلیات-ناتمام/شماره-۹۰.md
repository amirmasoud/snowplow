---
title: >-
    شمارهٔ ۹۰
---
# شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>با تماشای تو سیر گل و گلشن ستم است</p></div>
<div class="m2"><p>تا توان رفت به قربان تو مردن ستم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر دل ما که ز خونگرمی یاران شکند</p></div>
<div class="m2"><p>آتش افروختن کینه دشمن ستم است</p></div></div>