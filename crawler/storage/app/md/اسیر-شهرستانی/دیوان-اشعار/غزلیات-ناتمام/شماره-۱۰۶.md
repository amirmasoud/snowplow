---
title: >-
    شمارهٔ ۱۰۶
---
# شمارهٔ ۱۰۶

<div class="b" id="bn1"><div class="m1"><p>دل به راحت ندهم پاس محبت این است</p></div>
<div class="m2"><p>مژه بر هم نزنم خواب فراغت این است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سینه صافی است غباری که ز راهم برخاست</p></div>
<div class="m2"><p>اثر ساده دلیهای عداوت این است</p></div></div>