---
title: >-
    شمارهٔ ۳۰۶
---
# شمارهٔ ۳۰۶

<div class="b" id="bn1"><div class="m1"><p>که دید از غم شناسان خاطر شادی که من دیدم</p></div>
<div class="m2"><p>سراسر مهربانی بودم بیدادی که من دیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوادش خواب حیرانی غبارش آب ویرانی</p></div>
<div class="m2"><p>نبیند چشم محشر حیرت آبادی که من دیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گهی با سایه در شوخی گهی با جلوه در مستی</p></div>
<div class="m2"><p>چها در سر ندارد سرو آزادی که من دیدم</p></div></div>