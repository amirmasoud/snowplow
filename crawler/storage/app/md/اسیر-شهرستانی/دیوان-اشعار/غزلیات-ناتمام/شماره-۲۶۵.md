---
title: >-
    شمارهٔ ۲۶۵
---
# شمارهٔ ۲۶۵

<div class="b" id="bn1"><div class="m1"><p>نو بهار آمد دلم فال شکفتن می زند</p></div>
<div class="m2"><p>بوی گل بر آتش افسرده دامن می زند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست آسان خاطر جمعی پریشان ساختن</p></div>
<div class="m2"><p>می گذارد برق تا خود را به خرمن می زند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اشک طوفان کوششی از بهر تعمیرم فرست</p></div>
<div class="m2"><p>سیل بی پرواست طبل آرمیدن می زند</p></div></div>