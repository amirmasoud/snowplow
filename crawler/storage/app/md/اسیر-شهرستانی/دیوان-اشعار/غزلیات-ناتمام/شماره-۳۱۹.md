---
title: >-
    شمارهٔ ۳۱۹
---
# شمارهٔ ۳۱۹

<div class="b" id="bn1"><div class="m1"><p>دردیم و از دیار هوس کم گذشته ایم</p></div>
<div class="m2"><p>برقیم و از قلمرو خس کم گذشته ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما خامشیم و زمزمه دل نوای ماست</p></div>
<div class="m2"><p>در کوچه فغان جرس کم گذاشته ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون راز خویش آفت اقلیم شکوه ایم</p></div>
<div class="m2"><p>از تنگنای راه نفس کم گذشته ایم</p></div></div>