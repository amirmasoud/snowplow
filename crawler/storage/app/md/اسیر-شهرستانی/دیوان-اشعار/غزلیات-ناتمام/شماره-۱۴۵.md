---
title: >-
    شمارهٔ ۱۴۵
---
# شمارهٔ ۱۴۵

<div class="b" id="bn1"><div class="m1"><p>عالم شکار فتنه چشم سیاه اوست</p></div>
<div class="m2"><p>هر جا که می روم سر تیر نگاه اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرهای غنچه بسته به فتراک نو بهار</p></div>
<div class="m2"><p>صیاد ما که سایه گل دامگاه اوست</p></div></div>