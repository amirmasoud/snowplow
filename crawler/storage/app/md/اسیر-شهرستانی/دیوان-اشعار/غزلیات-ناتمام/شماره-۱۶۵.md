---
title: >-
    شمارهٔ ۱۶۵
---
# شمارهٔ ۱۶۵

<div class="b" id="bn1"><div class="m1"><p>کار اشکم بی تو از صحرا گذشت</p></div>
<div class="m2"><p>قطره زد چندان که از دریا گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داشتیم از بس در این ره اعتبار</p></div>
<div class="m2"><p>هر که آمد همچو برق از ما گذشت</p></div></div>