---
title: >-
    شمارهٔ ۲۵۰
---
# شمارهٔ ۲۵۰

<div class="b" id="bn1"><div class="m1"><p>دلم در آتش رشک چراغ می سوزد</p></div>
<div class="m2"><p>که با خیال که شبها دماغ می سوزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین بهار کسی را مسلم است جنون</p></div>
<div class="m2"><p>که از فتیله زنجیر داغ می سوزد</p></div></div>