---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>دلیل بادیه دیوانگی بس است مرا</p></div>
<div class="m2"><p>همین نشانه فرزانگی بس است مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز خویشتن به دیار جنون گریزانم</p></div>
<div class="m2"><p>که آشنایی بیگانگی بس است مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کجاست غم که کشد رخت من به کوی جنون</p></div>
<div class="m2"><p>به عقل نسبت همخانگی بس است مرا</p></div></div>