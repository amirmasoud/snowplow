---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>گرد فتادگی شده بال همای ما</p></div>
<div class="m2"><p>منت نمی کشد ز کسی مدعای ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با چاکهای سینه به محشر نمی رویم</p></div>
<div class="m2"><p>تا رنگ و بوی گل نشود خونبهای ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حیرت ندیدگی گل گلزار وحشت است</p></div>
<div class="m2"><p>ای غافل از نگاه تغافل نمای ما</p></div></div>