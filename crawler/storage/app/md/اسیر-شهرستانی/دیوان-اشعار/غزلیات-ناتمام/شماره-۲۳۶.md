---
title: >-
    شمارهٔ ۲۳۶
---
# شمارهٔ ۲۳۶

<div class="b" id="bn1"><div class="m1"><p>چو گل از اختلاطم سنگ طفلان تازگی دارد</p></div>
<div class="m2"><p>در این حالم نصیحت‌های یاران تازگی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خاک کشته‌ای کز ابر رحمت باج می‌گیرد</p></div>
<div class="m2"><p>تغافل‌های ابر نوبهاران تازگی دارد</p></div></div>