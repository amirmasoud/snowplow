---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>صبح شد ساقی بده جام می دیرینه را</p></div>
<div class="m2"><p>تا بر افروزیم از این آتش چراغ سینه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فصل گل تا از لب ساغر نگیری کام دل</p></div>
<div class="m2"><p>از میان هفته بیرون کن شب آدینه را</p></div></div>