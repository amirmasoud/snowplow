---
title: >-
    شمارهٔ ۳۰۲
---
# شمارهٔ ۳۰۲

<div class="b" id="bn1"><div class="m1"><p>رفته ام از خود ندانم بیقرار کیستم</p></div>
<div class="m2"><p>می تپم در خون شهید انتظار کیستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گاه خونم می خورد گه خاکمالم می دهد</p></div>
<div class="m2"><p>آسمان گویا نمی داند شکار کیستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در حساب است از من سرگشته هر جا سرمه ای است</p></div>
<div class="m2"><p>دیده خورشید می داند غبار کیستم</p></div></div>