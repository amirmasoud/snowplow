---
title: >-
    شمارهٔ ۸۱
---
# شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>سرنوشتم چون تمنا چین ابرویی بس است</p></div>
<div class="m2"><p>بازگشتم چون تماشا با گل رویی بس است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در محبت دل به حرف آشنایی بسته ایم</p></div>
<div class="m2"><p>حلقه زنجیر ما دیوانگان هویی بس است</p></div></div>