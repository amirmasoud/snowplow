---
title: >-
    شمارهٔ ۷۴
---
# شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>نشان زخم که جویی سوار بسیار است</p></div>
<div class="m2"><p>سر سراغ که داری غبار بسیار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی است صید تغافل اگر نمی دانی</p></div>
<div class="m2"><p>به امتحان نظری کن شکار بسیار است</p></div></div>