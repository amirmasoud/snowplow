---
title: >-
    شمارهٔ ۲۹۷
---
# شمارهٔ ۲۹۷

<div class="b" id="bn1"><div class="m1"><p>در حقیقت قرب و بعد مردم دنیا غلط</p></div>
<div class="m2"><p>آشناییها غلط نا آشناییها غلط</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نسخه آشفته دیوان عمر ما مپرس</p></div>
<div class="m2"><p>خط غلط معنی غلط انشا غلط املا غلط!</p></div></div>