---
title: >-
    شمارهٔ ۹۵
---
# شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>محبت خانه زاد سینه ماست</p></div>
<div class="m2"><p>خموشی همدم دیرینه ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز جوی سینه صافی می خورد آب</p></div>
<div class="m2"><p>فراموشی که تیغ کینه ماست</p></div></div>