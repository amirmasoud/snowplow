---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>از می تجربه میخانه افلاک پر است</p></div>
<div class="m2"><p>جام مستی شده سرشار که از خاک پر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سنگ طفلان چه که هر نقش قدم جام جم است</p></div>
<div class="m2"><p>درخرابات جنون آینه پاک پر است</p></div></div>