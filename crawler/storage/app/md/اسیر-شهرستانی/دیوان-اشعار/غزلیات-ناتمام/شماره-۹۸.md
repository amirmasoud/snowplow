---
title: >-
    شمارهٔ ۹۸
---
# شمارهٔ ۹۸

<div class="b" id="bn1"><div class="m1"><p>قفل کتابخانه حیرت دل من است</p></div>
<div class="m2"><p>صندوق رازهای محبت دل من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صید نگاه گرم ز دام رمیدگی است</p></div>
<div class="m2"><p>آیینه دار وحشت و الفت من است</p></div></div>