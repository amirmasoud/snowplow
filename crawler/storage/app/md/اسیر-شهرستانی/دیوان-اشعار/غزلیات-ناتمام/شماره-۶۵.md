---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>معنی غالب حریفی بی شماتت همت است</p></div>
<div class="m2"><p>پیش من (بر) دشمن عاجز مروت همت است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می تواند حاتم طایی شدن راه عرب</p></div>
<div class="m2"><p>گر همین افسانه ارباب همت همت است</p></div></div>