---
title: >-
    شمارهٔ ۱۳۸
---
# شمارهٔ ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>در حقیقت عاشق و معشوق را دل‌ها یکی است</p></div>
<div class="m2"><p>شیشه را از روی نسبت اصل با خارا یکی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست شام هجر ما آیینه صبح وصال</p></div>
<div class="m2"><p>نور و ظلمت پیش چشم مردم دانا یکی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناوکش چون بر دلی آید ز صد دل خون چکد</p></div>
<div class="m2"><p>عالمی را در گرفتاری ز بس دل‌ها یکی است</p></div></div>