---
title: >-
    شمارهٔ ۱۸۸
---
# شمارهٔ ۱۸۸

<div class="b" id="bn1"><div class="m1"><p>رنگ شکسته از گل رویش عیان مباد</p></div>
<div class="m2"><p>پژمردگی شکفته از آن گلستان مباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیغام خویش جز به خیالش نمی دهم</p></div>
<div class="m2"><p>غیری به این وسیله به او همزبان مباد</p></div></div>