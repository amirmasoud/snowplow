---
title: >-
    شمارهٔ ۲۶۳
---
# شمارهٔ ۲۶۳

<div class="b" id="bn1"><div class="m1"><p>در این مکتب کتاب عقل را دیوانه می‌خواند</p></div>
<div class="m2"><p>خط دیوانی زنجیر را فرزانه می‌خواند</p></div></div>