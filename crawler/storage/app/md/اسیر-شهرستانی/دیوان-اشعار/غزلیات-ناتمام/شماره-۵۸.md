---
title: >-
    شمارهٔ ۵۸
---
# شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>به یاد روی تو از تاب ناله ام گل سوخت</p></div>
<div class="m2"><p>چه داغها که چمن ز آشیان بلبل سوخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به قحط سال نگاه تو حاصل عمرم</p></div>
<div class="m2"><p>چو دانه شرر از آتش تغافل سوخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زمن رسید به جایی رواج ناله که باغ</p></div>
<div class="m2"><p>سپند غنچه پی چشم زخم بلبل سوخت</p></div></div>