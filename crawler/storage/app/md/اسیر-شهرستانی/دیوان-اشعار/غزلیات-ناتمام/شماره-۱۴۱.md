---
title: >-
    شمارهٔ ۱۴۱
---
# شمارهٔ ۱۴۱

<div class="b" id="bn1"><div class="m1"><p>دامان فتنه گل به میان سخن شکست</p></div>
<div class="m2"><p>دریاب توبه را که خمار چمن شکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بوی نافه خون دلم جوش می زند</p></div>
<div class="m2"><p>از خوی طره که دماغ ختن شکست</p></div></div>