---
title: >-
    شمارهٔ ۲۸۶
---
# شمارهٔ ۲۸۶

<div class="b" id="bn1"><div class="m1"><p>نگاهش آشنا شد آشناتر</p></div>
<div class="m2"><p>دعای بی اثر حاجت رواتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اثرها دیده ام از دوستداری</p></div>
<div class="m2"><p>دلش از بیوفایی بیوفاتر</p></div></div>