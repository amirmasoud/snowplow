---
title: >-
    شمارهٔ ۱۶۹
---
# شمارهٔ ۱۶۹

<div class="b" id="bn1"><div class="m1"><p>شوخی حسن تو گه نقش و گهی نقاش گشت</p></div>
<div class="m2"><p>راز پنهان دو عالم از دل ما فاش گشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دخل عالم نیست خرج چشم گریان را کفاف</p></div>
<div class="m2"><p>بارها در دور اشکم آسمان قلاش گشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اعتبارم کمتر از ویرانه ای شد آه آه</p></div>
<div class="m2"><p>زانکه نتوانم گهی بر گرد آن سرفاش گشت</p></div></div>