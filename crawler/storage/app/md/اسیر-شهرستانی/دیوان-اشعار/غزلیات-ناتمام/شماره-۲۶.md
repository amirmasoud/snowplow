---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>کرده ام مرغی سبکروحی امید و بیم را</p></div>
<div class="m2"><p>اضطرابم در بغل دارد گل تسلیم را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوخی پرواز او بال گرفتاری خوش است</p></div>
<div class="m2"><p>می کنم در کنج عزلت سیر هفت اقلیم را</p></div></div>