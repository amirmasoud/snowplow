---
title: >-
    شمارهٔ ۱۱۸
---
# شمارهٔ ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>دل با خیال لعل تو پیوند کرده است</p></div>
<div class="m2"><p>در جام زهر چاشنی قند کرده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاهد بگیر ساغر و مستانه برفشان</p></div>
<div class="m2"><p>این دست را که سبحه عسس بند کرده است</p></div></div>