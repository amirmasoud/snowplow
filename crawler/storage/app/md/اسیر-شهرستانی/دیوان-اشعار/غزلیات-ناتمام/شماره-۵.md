---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>نظاره خطش از هوش می برد ما را</p></div>
<div class="m2"><p>به سیر باغ بناگوش می برد ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه اوجها که گرفتیم تا غبار شدیم</p></div>
<div class="m2"><p>نسیم کوی تو بر دوش می برد ما را</p></div></div>