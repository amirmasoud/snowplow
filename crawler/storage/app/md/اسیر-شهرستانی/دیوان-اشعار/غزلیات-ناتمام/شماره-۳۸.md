---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>عهد تمکین با دل دیوانه بستن کار ما</p></div>
<div class="m2"><p>خاطر خود را ز هر اندیشه خستن کار ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر نفس بست و گشادی هست در دست خیال</p></div>
<div class="m2"><p>کار دل افتادن اندر دام و جستن کار ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل اگر در پیرهن باشد جنون را نشتر است</p></div>
<div class="m2"><p>نیست خار حرف در خاطر شکستن کار ما</p></div></div>