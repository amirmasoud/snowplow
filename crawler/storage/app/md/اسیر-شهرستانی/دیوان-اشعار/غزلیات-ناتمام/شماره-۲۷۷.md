---
title: >-
    شمارهٔ ۲۷۷
---
# شمارهٔ ۲۷۷

<div class="b" id="bn1"><div class="m1"><p>عشق یک پرده از آن عاشق پر نور شود</p></div>
<div class="m2"><p>تا به چشم بد اگر کس نگرد کور شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حلقه دام گرفتاری ما آزادی است</p></div>
<div class="m2"><p>سایه سرو مباد از سر ما دور شود</p></div></div>