---
title: >-
    شمارهٔ ۲۰۵
---
# شمارهٔ ۲۰۵

<div class="b" id="bn1"><div class="m1"><p>با عشق توام ناله فراموش نگردد</p></div>
<div class="m2"><p>این دود چراغی است که خاموش نگردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داده است فریب نگهت خام دلان را</p></div>
<div class="m2"><p>آن باده که خمیازه کش جوش نگردد</p></div></div>