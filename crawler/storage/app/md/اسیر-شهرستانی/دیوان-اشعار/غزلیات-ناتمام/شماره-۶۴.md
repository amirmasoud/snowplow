---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>دلم در اضطراب اضطراب است</p></div>
<div class="m2"><p>که هر گردش طلسم فتح باب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنازم همت دست تهی را</p></div>
<div class="m2"><p>هر انگشتش کلید فتح باب است</p></div></div>