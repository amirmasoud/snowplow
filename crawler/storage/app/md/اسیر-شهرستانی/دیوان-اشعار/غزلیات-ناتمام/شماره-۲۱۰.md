---
title: >-
    شمارهٔ ۲۱۰
---
# شمارهٔ ۲۱۰

<div class="b" id="bn1"><div class="m1"><p>وطنم بی تو سفر می گردد</p></div>
<div class="m2"><p>طاقتم زیر و زبر می گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بعد عمری که به برآمده ای</p></div>
<div class="m2"><p>تیر مژگان تو بر می گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چقدر نام خدا خوش چشمی</p></div>
<div class="m2"><p>نگهت نور نظر می گردد</p></div></div>