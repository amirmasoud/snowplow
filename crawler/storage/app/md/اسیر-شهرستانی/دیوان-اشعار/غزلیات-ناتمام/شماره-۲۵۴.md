---
title: >-
    شمارهٔ ۲۵۴
---
# شمارهٔ ۲۵۴

<div class="b" id="bn1"><div class="m1"><p>دل ز تاراج نگاه شعله بازش می‌رسد</p></div>
<div class="m2"><p>گفتن افسانه سوز و گدازش می‌رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طفل و محجوب است و بی‌باک است و معشوق رسا</p></div>
<div class="m2"><p>هرچه خواهد می‌تواند کرد نازش می‌رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم و زلفش اختیار عالمی دارد اسیر</p></div>
<div class="m2"><p>کشتن ما بستن اهل نیازش می‌رسد</p></div></div>