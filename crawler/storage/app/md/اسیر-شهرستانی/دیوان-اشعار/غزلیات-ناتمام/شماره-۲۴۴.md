---
title: >-
    شمارهٔ ۲۴۴
---
# شمارهٔ ۲۴۴

<div class="b" id="bn1"><div class="m1"><p>آنکه لعلش بیشتر از نیشتر دل می‌برد</p></div>
<div class="m2"><p>گر به خاطر بگذرد شیرین‌شکر دل می‌برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غیر حرف او نمی‌دانم چه گویم در جواب</p></div>
<div class="m2"><p>هرکه می‌پرسد ز حال من خبر دل می‌برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاهد رنگین دنیا جلوه‌ای دارد مپرس</p></div>
<div class="m2"><p>می‌خورد خون جگر تا در نظر دل می‌برد</p></div></div>