---
title: >-
    شمارهٔ ۲۲۰
---
# شمارهٔ ۲۲۰

<div class="b" id="bn1"><div class="m1"><p>لب از تبسم و چشم از ادا خبر دارد</p></div>
<div class="m2"><p>کسی نگفت که دل از کجا خبر دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به شکوه لب چه گدازم عجب که راز مرا</p></div>
<div class="m2"><p>غریب بیشتر از آشنا خبر دارد</p></div></div>