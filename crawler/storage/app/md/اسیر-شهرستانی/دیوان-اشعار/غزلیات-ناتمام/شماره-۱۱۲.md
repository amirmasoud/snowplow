---
title: >-
    شمارهٔ ۱۱۲
---
# شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>بس که در بزمش نگه سرمشق حیرت گشته است</p></div>
<div class="m2"><p>راز دل ممنون تأثیر محبت گشته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون تسلی بخشدم زخمی که از مژگان اوست</p></div>
<div class="m2"><p>تا دل آیینه بیتاب جراحت گشته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوانده نور چشم خود آیینه ما را غبار</p></div>
<div class="m2"><p>گنج باد آورد ما گرد کدروت گشته است</p></div></div>