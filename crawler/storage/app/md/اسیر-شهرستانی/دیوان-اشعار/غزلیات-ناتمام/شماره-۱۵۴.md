---
title: >-
    شمارهٔ ۱۵۴
---
# شمارهٔ ۱۵۴

<div class="b" id="bn1"><div class="m1"><p>دوری گل بال مرغان قفس را عار نیست</p></div>
<div class="m2"><p>بینوایی همت بی دسترسی را عار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل کمان زور مطلب های عالم را شکست</p></div>
<div class="m2"><p>گر کشد آهسته تر گاهی نفس را عار نیست</p></div></div>