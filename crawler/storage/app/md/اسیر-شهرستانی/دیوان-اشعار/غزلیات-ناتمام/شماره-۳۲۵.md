---
title: >-
    شمارهٔ ۳۲۵
---
# شمارهٔ ۳۲۵

<div class="b" id="bn1"><div class="m1"><p>از بسکه سوخت بی تو دل بیقرار من</p></div>
<div class="m2"><p>آتش سلم خرید محبت ز خار من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساغر بنوش و چهره برافروز و گل بچین</p></div>
<div class="m2"><p>بدمستی دماغ مبین در بهار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گردی که رفته است به باد اعتبار تو</p></div>
<div class="m2"><p>خاکستری که مانده به جا یادگار من</p></div></div>