---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>ز بی سرمایگی دادم سرانجامی سر خود را</p></div>
<div class="m2"><p>به دست صد شکست دل سپردم ساغر خود را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان سیر چمن شد در گرفتاری فراموشم</p></div>
<div class="m2"><p>که هرگز از قفس نشناختم بال و پر خود را</p></div></div>