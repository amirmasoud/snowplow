---
title: >-
    شمارهٔ ۲۹۲
---
# شمارهٔ ۲۹۲

<div class="b" id="bn1"><div class="m1"><p>خیال لعل لبش عیش تنگدستان بس</p></div>
<div class="m2"><p>گل پیاله نمکدان می پرستان بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیم نسیم که درد سر بهار دهم</p></div>
<div class="m2"><p>چو شعله ام گل خاری از این گلستان بس</p></div></div>