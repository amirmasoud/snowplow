---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>هر طرف فتنه ای از گرد سواری برخاست</p></div>
<div class="m2"><p>مژده ای دیده که از دور غباری برخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو به خونریزی و ما از پی تسلیم شدن</p></div>
<div class="m2"><p>هر که را دست و دلی بود به کاری برخاست</p></div></div>