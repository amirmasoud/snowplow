---
title: >-
    شمارهٔ ۱۲۵
---
# شمارهٔ ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>چون دور ز وصل یار می بوده است</p></div>
<div class="m2"><p>دل در بر من چه کار می بوده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر روز به رنگ تازه می خندد</p></div>
<div class="m2"><p>این داغ چه خوش بهار می بوده است</p></div></div>