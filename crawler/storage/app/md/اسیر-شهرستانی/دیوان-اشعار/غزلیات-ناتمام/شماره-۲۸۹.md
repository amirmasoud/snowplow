---
title: >-
    شمارهٔ ۲۸۹
---
# شمارهٔ ۲۸۹

<div class="b" id="bn1"><div class="m1"><p>دل ندارم بیقراری بیشتر</p></div>
<div class="m2"><p>صیدگاهی را شکاری بیشتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ذره بر دوش غبارم در سماع</p></div>
<div class="m2"><p>بیشتر چابک سواری بیشتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند تنها وعده قتل اسیر</p></div>
<div class="m2"><p>بیوفا امیدواری بیشتر</p></div></div>