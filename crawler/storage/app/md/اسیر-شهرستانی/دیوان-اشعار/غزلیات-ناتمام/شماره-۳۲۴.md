---
title: >-
    شمارهٔ ۳۲۴
---
# شمارهٔ ۳۲۴

<div class="b" id="bn1"><div class="m1"><p>ای آنکه دل ربوده ای از دلربای ما</p></div>
<div class="m2"><p>تا حال ما نداند از او احتراز کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز موج کس گره نگشود از دل حباب</p></div>
<div class="m2"><p>زان تیغ آبدار مرا سرفراز کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون صید چشم او شدی ای بینوا اسیر</p></div>
<div class="m2"><p>فکری به حال جان تغافل گداز کن</p></div></div>