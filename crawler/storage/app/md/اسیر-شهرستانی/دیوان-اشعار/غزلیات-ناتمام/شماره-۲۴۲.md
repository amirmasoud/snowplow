---
title: >-
    شمارهٔ ۲۴۲
---
# شمارهٔ ۲۴۲

<div class="b" id="bn1"><div class="m1"><p>کو قاصدی که نامه به باد صبا برد</p></div>
<div class="m2"><p>تا گردم از قلمرو بال هما برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکس به روی ما در چاک قفس گشود</p></div>
<div class="m2"><p>پیش درش تبسم گل التجا برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکچند هم به رغم فلک بیوفا شدم</p></div>
<div class="m2"><p>شاید به این وسیله کسی نام ما برد</p></div></div>