---
title: >-
    شمارهٔ ۱۵۵
---
# شمارهٔ ۱۵۵

<div class="b" id="bn1"><div class="m1"><p>هر کس از خیال نگاه تو دور نیست</p></div>
<div class="m2"><p>گر حور آیدش به نظر بی قصور نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داد دل از نهفتن رازی توان گرفت</p></div>
<div class="m2"><p>در کار عشق ناله و آهی ضرور نیست</p></div></div>