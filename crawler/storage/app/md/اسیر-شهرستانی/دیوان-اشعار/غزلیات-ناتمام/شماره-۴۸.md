---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>گر به ما فاش شود معنی نادانی ما</p></div>
<div class="m2"><p>دشت را بحر کند اشک پشیمانی ما</p></div></div>