---
title: >-
    شمارهٔ ۲۱۷
---
# شمارهٔ ۲۱۷

<div class="b" id="bn1"><div class="m1"><p>عیشم از دولت بیدار اثرها دارد</p></div>
<div class="m2"><p>شبم از گردش پیمانه سحرها دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو گوهر نظر از پاکی دل یافته ایم</p></div>
<div class="m2"><p>اشک ما در جگر سنگ اثرها دارد</p></div></div>