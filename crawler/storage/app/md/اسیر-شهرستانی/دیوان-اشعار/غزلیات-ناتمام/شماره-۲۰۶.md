---
title: >-
    شمارهٔ ۲۰۶
---
# شمارهٔ ۲۰۶

<div class="b" id="bn1"><div class="m1"><p>عشق تو چراغ دل هر خام نگردد</p></div>
<div class="m2"><p>صیدی است خیالت که به کس رام نگردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهم که بر افتد ز جهان رسم وفا هم</p></div>
<div class="m2"><p>تا هیچ زبان محرم آن نام نگردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیدل شد اسیر و اثر عشق تو باقی است</p></div>
<div class="m2"><p>کیفیت می در گرو جام نگردد</p></div></div>