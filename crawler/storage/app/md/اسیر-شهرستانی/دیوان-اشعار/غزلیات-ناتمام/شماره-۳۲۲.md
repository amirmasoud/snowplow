---
title: >-
    شمارهٔ ۳۲۲
---
# شمارهٔ ۳۲۲

<div class="b" id="bn1"><div class="m1"><p>مجنون دشت گریه مستانه خودیم</p></div>
<div class="m2"><p>آزاد کرده دل دیوانه خودیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آیینه خاطریم ز تأثیر عشق پاک</p></div>
<div class="m2"><p>جویای وصل روی تو در خانه خودیم</p></div></div>