---
title: >-
    شمارهٔ ۳۱۸
---
# شمارهٔ ۳۱۸

<div class="b" id="bn1"><div class="m1"><p>دل به زلف او چو بندم بر سر دل چون شوم</p></div>
<div class="m2"><p>همره خضر پریشانی به منزل چون شوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ذوق آشوب خطر دیوانه ام دارد چو موج</p></div>
<div class="m2"><p>نیستم خس بسته زنجیر ساحل چون شوم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من که از گرم اختلاطی های آتش زنده ام</p></div>
<div class="m2"><p>گر شعوری دارم از یاد تو غافل چون شوم</p></div></div>