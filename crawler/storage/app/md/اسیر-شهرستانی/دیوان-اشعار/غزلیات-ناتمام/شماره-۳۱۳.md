---
title: >-
    شمارهٔ ۳۱۳
---
# شمارهٔ ۳۱۳

<div class="b" id="bn1"><div class="m1"><p>دل نیستم که منتی از آرزو کشم</p></div>
<div class="m2"><p>چون شعله می ز میکده آبرو کشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داغم ز دست غیر و کشم از دل انتقام</p></div>
<div class="m2"><p>بر جام لب گذارم و خون سبو کشم</p></div></div>