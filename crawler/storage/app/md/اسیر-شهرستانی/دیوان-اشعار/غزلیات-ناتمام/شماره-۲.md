---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>دیده پاک حباب می حال است اینجا</p></div>
<div class="m2"><p>لب خاموش دم صبح خیال است اینجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمک صید نکردن فره صیاد است</p></div>
<div class="m2"><p>دام صد پاره به از بستن بال است اینجا</p></div></div>