---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>الفت نمی‌کنند به کس دل دویده‌ها</p></div>
<div class="m2"><p>گلچین نمی‌شوند جراحت گزیده‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ممنون خصم غالب خویشم که خضر اوست</p></div>
<div class="m2"><p>پای کم است گام به منزل رسیده‌ها</p></div></div>