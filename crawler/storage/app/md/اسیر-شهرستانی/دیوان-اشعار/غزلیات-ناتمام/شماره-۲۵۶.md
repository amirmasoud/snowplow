---
title: >-
    شمارهٔ ۲۵۶
---
# شمارهٔ ۲۵۶

<div class="b" id="bn1"><div class="m1"><p>از کدوی باده کی باشد که گلها بشکفد</p></div>
<div class="m2"><p>در گل ساغر بهار خاطر ما بشکفد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کو بهار نکهت پیراهنی کز جلوه اش</p></div>
<div class="m2"><p>همچو نرگس غنچه چشم تر ما بشکفد</p></div></div>