---
title: >-
    شمارهٔ ۳۰۵
---
# شمارهٔ ۳۰۵

<div class="b" id="bn1"><div class="m1"><p>از تمنای تو سرگرم تمنای خودم</p></div>
<div class="m2"><p>می کنم یاد تو و محو تماشای خودم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با خیالت جای غم در سینه خالی می کنم</p></div>
<div class="m2"><p>منفعل دارد تصرفهای بیجای خودم</p></div></div>