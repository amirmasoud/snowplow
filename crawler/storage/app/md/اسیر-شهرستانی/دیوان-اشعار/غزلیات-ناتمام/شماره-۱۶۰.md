---
title: >-
    شمارهٔ ۱۶۰
---
# شمارهٔ ۱۶۰

<div class="b" id="bn1"><div class="m1"><p>به نکهت چمنم بی‌تو آشنایی نیست</p></div>
<div class="m2"><p>به رنگ غنچه دلتنگ من هوایی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هر کدام غمش لطف می‌کند خوب است</p></div>
<div class="m2"><p>میان ما و دل از درد او جدایی نیست</p></div></div>