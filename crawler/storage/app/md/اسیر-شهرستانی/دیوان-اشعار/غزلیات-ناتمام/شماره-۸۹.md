---
title: >-
    شمارهٔ ۸۹
---
# شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>در بزم وفا یاد لب یار حرام است</p></div>
<div class="m2"><p>درکیش ادب مستی سرشار حرام است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چاک دل ما وقف گریبان غبار است</p></div>
<div class="m2"><p>جمعیت دیوانه به گلزار حرام است</p></div></div>