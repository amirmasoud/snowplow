---
title: >-
    شمارهٔ ۳۰۷
---
# شمارهٔ ۳۰۷

<div class="b" id="bn1"><div class="m1"><p>پرورده عشقم دل بی ریش ندارم</p></div>
<div class="m2"><p>شرمندگی از عشق ستم کیش ندارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارم غم رسوا دل شیدا سر سودا</p></div>
<div class="m2"><p>چیزی که ندارم خبر از خویش ندارم</p></div></div>