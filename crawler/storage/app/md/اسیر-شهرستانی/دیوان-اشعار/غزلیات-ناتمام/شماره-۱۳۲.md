---
title: >-
    شمارهٔ ۱۳۲
---
# شمارهٔ ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>آهم ز لخت دل فلک گل ستاره ای است</p></div>
<div class="m2"><p>حیرانیم بهار بهشت نظاره ای است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیوانه دلی که به صحرا دویده باز</p></div>
<div class="m2"><p>هر اختری چو دانه زنجیر پاره ای است</p></div></div>