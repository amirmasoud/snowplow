---
title: >-
    شمارهٔ ۳۰۴
---
# شمارهٔ ۳۰۴

<div class="b" id="bn1"><div class="m1"><p>یاد او کردم دل اندوهگینی یافتم</p></div>
<div class="m2"><p>در خیال مصرعی بودم زمینی یافتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاکساری آنقدر کردم که نامم شد بلند</p></div>
<div class="m2"><p>از شکست دل عجب نقش نگینی یافتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باده کم جوش لطفم در خمار افکنده بود</p></div>
<div class="m2"><p>نشئه سرشاری از چین جبینی یافتم</p></div></div>