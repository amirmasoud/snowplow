---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>حرف عشق دوست را افسانه می دانیم ما</p></div>
<div class="m2"><p>سایه های بید را دیوانه می دانیم ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قصه شیرین مجنون یک حدیث درد ماست</p></div>
<div class="m2"><p>عاشقی را در پر پروانه می خوانیم ما</p></div></div>