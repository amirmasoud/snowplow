---
title: >-
    شمارهٔ ۱۹۷
---
# شمارهٔ ۱۹۷

<div class="b" id="bn1"><div class="m1"><p>حرفی است محبت که به گفتار نگنجد</p></div>
<div class="m2"><p>این راز در آیینه اظهار نگنجد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون ذره سپردیم سراپا به نگاهی</p></div>
<div class="m2"><p>در دیده مشتاق تو دیدار نگنجد</p></div></div>