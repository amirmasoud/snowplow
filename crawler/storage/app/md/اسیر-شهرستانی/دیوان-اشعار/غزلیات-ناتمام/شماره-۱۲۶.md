---
title: >-
    شمارهٔ ۱۲۶
---
# شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>خط او دام هوش گردیده است</p></div>
<div class="m2"><p>لاله ریحان فروش گردیده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از نگاه فسرده زاهد</p></div>
<div class="m2"><p>شعله سنجاب پوش گردیده است</p></div></div>