---
title: >-
    شمارهٔ ۲۲۹
---
# شمارهٔ ۲۲۹

<div class="b" id="bn1"><div class="m1"><p>تغافل در نگه پنهان که دارد</p></div>
<div class="m2"><p>تبسم زیر لب خندان که دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز مژگان می نویسم سطر اشکی</p></div>
<div class="m2"><p>سواد نسخه طوفان که دارد</p></div></div>