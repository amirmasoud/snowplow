---
title: >-
    شمارهٔ ۱۶۸
---
# شمارهٔ ۱۶۸

<div class="b" id="bn1"><div class="m1"><p>حرف شمع رخ او دوش در این خانه گذشت</p></div>
<div class="m2"><p>آتش رشک چو آب از سر پروانه گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکوه کفر است از او گرنه بیان می کردم</p></div>
<div class="m2"><p>کاشنایی زمن امروز چو بیگانه گذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تیره آن بزم که بی شمع رخ ساقی بود</p></div>
<div class="m2"><p>تلخ آن عمر که بی گردش پیمانه گذشت</p></div></div>