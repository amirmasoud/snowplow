---
title: >-
    شمارهٔ ۲۳۷
---
# شمارهٔ ۲۳۷

<div class="b" id="bn1"><div class="m1"><p>زبان ناصح اگر زهر قاتلی دارد</p></div>
<div class="m2"><p>جنون کامل ما دست قابلی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دیر و صومعه پیش از اراده می گذریم</p></div>
<div class="m2"><p>وجود ناقص ما شوق کاملی دارد</p></div></div>