---
title: >-
    شمارهٔ ۱۳۶
---
# شمارهٔ ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>گلی که رنگ فروشد به شعله می نوشی است</p></div>
<div class="m2"><p>میی که حوصله بخشد به شیشه بیهوشی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز چهره مه و خورشید می توان فهمید</p></div>
<div class="m2"><p>جهان مسخر شمع زبان خاموشی است</p></div></div>