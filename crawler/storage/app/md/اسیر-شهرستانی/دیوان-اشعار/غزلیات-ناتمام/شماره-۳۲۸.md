---
title: >-
    شمارهٔ ۳۲۸
---
# شمارهٔ ۳۲۸

<div class="b" id="bn1"><div class="m1"><p>باده در میکده گشت آیینه</p></div>
<div class="m2"><p>خم سکندر شد و خشت آیینه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باده نوشید گلستان ساغر</p></div>
<div class="m2"><p>روی خود دید بهشت آیینه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیدلی کم سخنی کم نگهی</p></div>
<div class="m2"><p>هر چه دید از تو نوشت آیینه</p></div></div>