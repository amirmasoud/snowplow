---
title: >-
    شمارهٔ ۲۵۵
---
# شمارهٔ ۲۵۵

<div class="b" id="bn1"><div class="m1"><p>هستیم از بس غبار خاطر احباب شد</p></div>
<div class="m2"><p>هر سر مژگان من سرچشمه سیلاب شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پاره ابری شد از باران اشک من قفس</p></div>
<div class="m2"><p>چشمه دام از سرشکم حلقه گرداب شد</p></div></div>