---
title: >-
    شمارهٔ ۱۷۵
---
# شمارهٔ ۱۷۵

<div class="b" id="bn1"><div class="m1"><p>عقل از دیوانگی ارشاد می باید گرفت</p></div>
<div class="m2"><p>بی تکلف مشربی را یاد می باید گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قاتلی دارد که نامش را نمی داند هنوز</p></div>
<div class="m2"><p>بیدماغی از دل ما یاد می باید گرفت</p></div></div>