---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>صبح بیدار ندارد نظر پاک مرا</p></div>
<div class="m2"><p>آب در شیر کند دیده نمناک مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راز او خجلت رسوایی محشر نکشد</p></div>
<div class="m2"><p>نتوان جست به صحرای عدم خاک مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اختیارش تر صاف است چراغش روشن</p></div>
<div class="m2"><p>شعله محراب دعا کرده دل پاک مرا</p></div></div>