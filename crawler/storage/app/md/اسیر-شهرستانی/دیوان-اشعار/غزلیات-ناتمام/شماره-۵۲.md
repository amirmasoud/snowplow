---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>ناله از تاثیر هویی در کمین دارم بیا</p></div>
<div class="m2"><p>جان برای مقدمت در آستین دارم بیا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرده تیرت مغز را در استخوان من شرار</p></div>
<div class="m2"><p>خوش چراغانی ز شوق‌آفرین دارم بیا</p></div></div>