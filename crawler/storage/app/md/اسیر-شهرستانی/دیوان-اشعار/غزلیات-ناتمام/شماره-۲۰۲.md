---
title: >-
    شمارهٔ ۲۰۲
---
# شمارهٔ ۲۰۲

<div class="b" id="bn1"><div class="m1"><p>دل اگر شیفته موجه ساغر گردد</p></div>
<div class="m2"><p>رویش از قبله ابروی بتان برگردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست خورده است قدح گر همه جمشید بود</p></div>
<div class="m2"><p>خانه زاد است گر آیینه سکندر گردد</p></div></div>