---
title: >-
    شمارهٔ ۱۸۲
---
# شمارهٔ ۱۸۲

<div class="b" id="bn1"><div class="m1"><p>ساقی مرا به باده بیغش چه احتیاج</p></div>
<div class="m2"><p>مهر توام نواخت به آتش چه احتیاج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کار نیست قوت بازو به صید ما</p></div>
<div class="m2"><p>آنجا که جذبه هست کشاکش چه احتیاج</p></div></div>