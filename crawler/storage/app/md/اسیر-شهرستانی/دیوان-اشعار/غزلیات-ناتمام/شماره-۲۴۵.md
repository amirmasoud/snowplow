---
title: >-
    شمارهٔ ۲۴۵
---
# شمارهٔ ۲۴۵

<div class="b" id="bn1"><div class="m1"><p>بی هوش تر ز خوابم و خوابم نمی برد</p></div>
<div class="m2"><p>طوفان گریه گشتم و آبم نمی برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با آنکه غیر دیده تنک ظرفی مرا</p></div>
<div class="m2"><p>با توبه هم به بزم شرابم نمی برد</p></div></div>