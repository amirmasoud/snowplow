---
title: >-
    شمارهٔ ۶۹۲
---
# شمارهٔ ۶۹۲

<div class="b" id="bn1"><div class="m1"><p>ای چشم و چراغ آفرینش</p></div>
<div class="m2"><p>رنگین گل باغ آفرینش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از رایحه بهار خلقت</p></div>
<div class="m2"><p>گلدسته دماغ آفرینش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از باده حقشناسی تو</p></div>
<div class="m2"><p>دل گشته ایاغ آفرینش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مست است اسیر در ره تو</p></div>
<div class="m2"><p>از جام سراغ آفرینش</p></div></div>