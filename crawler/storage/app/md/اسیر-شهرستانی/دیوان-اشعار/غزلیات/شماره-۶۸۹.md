---
title: >-
    شمارهٔ ۶۸۹
---
# شمارهٔ ۶۸۹

<div class="b" id="bn1"><div class="m1"><p>خداوندا مکن کس را شهید خنجر نازش</p></div>
<div class="m2"><p>مگردان جز دل من مرغ دیگر صید شهبازش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>الهی طاقت بیداد رشکم نیست ننمایی</p></div>
<div class="m2"><p>بجز اندیشه قتلم کسی را محرم رازش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم در دام صیاد است یارب آرزو دارم</p></div>
<div class="m2"><p>که بندد دست در خون تپیدن بال پروازش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر گوید سخن نتوان شنیدن گفتگویش را</p></div>
<div class="m2"><p>چو بوی غنچه بس در پرده شرم است آوازش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غزالی رام الفت بود فهمیدم ز استغنا</p></div>
<div class="m2"><p>نگاهی فکر قتلم داشت فهمیدم ز اندازش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اسیر از من حدیث درد دل هر دم چه می پرسی</p></div>
<div class="m2"><p>عسس گر نیستی هر بیدلی می داند و رازش</p></div></div>