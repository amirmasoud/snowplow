---
title: >-
    شمارهٔ ۵۲۰
---
# شمارهٔ ۵۲۰

<div class="b" id="bn1"><div class="m1"><p>کی دل کلید راز به دست زبان سپرد</p></div>
<div class="m2"><p>بحر گهر به موج کجا می توان سپرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دود است گرد حمله ما در نبرد خصم</p></div>
<div class="m2"><p>آتش زند به معرکه چون دل عنان سپرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان می توان سپرد به یک روی دل ولی</p></div>
<div class="m2"><p>کی راز دوستان به کسی می توان سپرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صحرا ز پاره دل بی اعتبار ما</p></div>
<div class="m2"><p>گوهر به کیسه کرد و به ریگ روان سپرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حیرت به دیده داد محبت به دل اسیر</p></div>
<div class="m2"><p>گوهر به بحر داد و جواهر به کان سپرد</p></div></div>