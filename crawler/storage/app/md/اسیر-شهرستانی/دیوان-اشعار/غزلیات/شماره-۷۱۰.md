---
title: >-
    شمارهٔ ۷۱۰
---
# شمارهٔ ۷۱۰

<div class="b" id="bn1"><div class="m1"><p>بیا ای هجر و وصلت مایه شیب و شباب دل</p></div>
<div class="m2"><p>مهیا کرده ام بهرت شراب جان کباب دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به نوعی سخت جانم کرده بیدادش که هر ساعت</p></div>
<div class="m2"><p>صدای تیشه می آید به گوش از اضطراب دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه خونها خورده از اشکم چه جانها داده از آهم</p></div>
<div class="m2"><p>نمی دانم چه خواهم گفت در محشر جواب دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد از زلف تو حیرت سرمه چشم پریشانی</p></div>
<div class="m2"><p>که خواهد کرد آیا بعد از این تعبیر خواب دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه گل گل می شکفتی از می حیرت به این شوخی</p></div>
<div class="m2"><p>پریشان مصرعی گر می شنیدی از کتاب دل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مبین مژگان غمازش مبین چشم فسونبازش</p></div>
<div class="m2"><p>همین است انتخاب دل همین است انتخاب دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شب و روز از خم زلفی گرفتاری گرفتاری</p></div>
<div class="m2"><p>سؤال او سؤال من جواب او جواب دل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اسیر از چشم مستی ترک سودای دو عالم کرد</p></div>
<div class="m2"><p>به این سودا کسی تا کی نگردد بر جناب دل</p></div></div>