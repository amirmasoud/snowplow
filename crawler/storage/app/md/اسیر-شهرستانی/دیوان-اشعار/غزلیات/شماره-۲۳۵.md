---
title: >-
    شمارهٔ ۲۳۵
---
# شمارهٔ ۲۳۵

<div class="b" id="bn1"><div class="m1"><p>به رنگ باده صراحی طلسم هستی ماست</p></div>
<div class="m2"><p>قدح مصاحب ایام تنگدستی ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همینقدر به سرکویش اعتبار بس است</p></div>
<div class="m2"><p>که چرخ داغ ز بخت بلند و پستی ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز جام باده شراب نظاره می نوشیم</p></div>
<div class="m2"><p>خیال چشم تو سرمشق می پرستی ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به یاد نرگس ساقی کشیده ایم شراب</p></div>
<div class="m2"><p>پیاله تشنه توفیق پیشدستی ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به کعبه تحفه برد سجده ای اسیر از دیر</p></div>
<div class="m2"><p>دلیل راه طلب شوق بت پرستی ماست</p></div></div>