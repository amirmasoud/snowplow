---
title: >-
    شمارهٔ ۴۵۱
---
# شمارهٔ ۴۵۱

<div class="b" id="bn1"><div class="m1"><p>مالک رقاب فتح و ظفر یا علی مدد</p></div>
<div class="m2"><p>از پافتاده ایم دگر یا علی مدد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روشن سواد دیده آیینه گشته ایم</p></div>
<div class="m2"><p>یکبار گفته ایم اگر یا علی مدد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آتش فروز چهره شرمندگی مکن</p></div>
<div class="m2"><p>دل بسته است از تو کمر یا علی مدد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش پایمال خصمی افلاک گشته ام</p></div>
<div class="m2"><p>می خواهم از دل تو جگر یا علی مدد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آیینه خانه دو جهان است مهر تو</p></div>
<div class="m2"><p>چشم دل و چراغ نظر یا علی مدد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خورشید ذره ای شده از خاک راه من</p></div>
<div class="m2"><p>تا سایه ات فتاده به سر یا علی مدد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همت ز دستیاری خود بود روشناس</p></div>
<div class="m2"><p>ابر محیط قطره گهر یا علی مدد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روشنگر است رای تو عالم به دست تو</p></div>
<div class="m2"><p>آیینه ساز شام و سحر یا علی مدد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون غیر درگه تو ندارد پناه اسیر</p></div>
<div class="m2"><p>می خواهد از تو فتح و ظفر یا علی مدد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از کلک هر نفس که کشم در ریاض جان</p></div>
<div class="m2"><p>انشا کنم به رنگ دگر یا علی مدد</p></div></div>