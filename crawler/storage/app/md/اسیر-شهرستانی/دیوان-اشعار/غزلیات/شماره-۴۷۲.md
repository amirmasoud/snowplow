---
title: >-
    شمارهٔ ۴۷۲
---
# شمارهٔ ۴۷۲

<div class="b" id="bn1"><div class="m1"><p>جدا هر جلوه ای بینی ز سودای نظر دارد</p></div>
<div class="m2"><p>گل از جایی دل از جایی می از جایی نظر دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نفس فرسوده آتش دل دیوانه ای دارم</p></div>
<div class="m2"><p>که هر آشوبش از طوفان دریایی نظر دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گلستان هوس را رنگ و بویی غیر وحشت نیست</p></div>
<div class="m2"><p>گلش از جایی و شمشادش از جایی نظر دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آن میخانه وحدت می دهد داد دل عارف</p></div>
<div class="m2"><p>که هر ساغر ز چشم باده پیمایی نظر دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نظر تا از کجا دارد دل دیوانه عاشق</p></div>
<div class="m2"><p>به هر شمع و گلی می بینم از جایی نظر دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چراغ خلوت ما خواب خاموشی نمی داند</p></div>
<div class="m2"><p>دل بیدار می داند کز ایمایی نظر دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زند لاف گرفتاری اسیر ما جهانگرد است</p></div>
<div class="m2"><p>سر زنجیرش از زلف چلیپایی نظر دارد</p></div></div>