---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>رخصت کشتنم بده نرگس کم نگاه را</p></div>
<div class="m2"><p>یا مکن آشنای دل گرمی گاه گاه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می کنم اضطراب را پیش تو پاسبان دل</p></div>
<div class="m2"><p>تا نبرد ز دیده ام چاشنی نگاه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب که خیال چشم او خواب رباید از نظر</p></div>
<div class="m2"><p>سرمه کشم ز دود دل چشم سفید ماه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهر شکایتم به دل شکر شکر می شود</p></div>
<div class="m2"><p>چون به لب آشنا کند خنده عذرخواه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دشمن خویش را کسی راه به خانه چون دهد</p></div>
<div class="m2"><p>کی کنم آشنای دل طاقت عمر کاه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکه زپاکی نفس همچو اسیر دم زند</p></div>
<div class="m2"><p>آینه اثرکند گریه صبحگاه را</p></div></div>