---
title: >-
    شمارهٔ ۷۷۲
---
# شمارهٔ ۷۷۲

<div class="b" id="bn1"><div class="m1"><p>صد زبان گر بهر عذر مدعا پیدا کنم</p></div>
<div class="m2"><p>مدعایی را که نشناسم کجا پیدا کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می کنم پیوند با بیداد و خویشی با ستم</p></div>
<div class="m2"><p>در دلت شاید به این تقریر جا پیدا کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در محبت خضر راهم گشته بخت واژگون</p></div>
<div class="m2"><p>خویش را گم می کنم باشد تو را پیدا کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آشناییهای رسمی را ثمر بیگانگی است</p></div>
<div class="m2"><p>می شوم بیگانه شاید آشنا پیدا کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همتم دست طلب را بشکند در آستین</p></div>
<div class="m2"><p>گر ید بیضا به تأثیر دعا پیدا کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در سرکویش به مژگان خاک می روبم ا سیر</p></div>
<div class="m2"><p>تا چو مهر آیینه از آن نقش پا پیدا کنم</p></div></div>