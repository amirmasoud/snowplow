---
title: >-
    شمارهٔ ۵۷۹
---
# شمارهٔ ۵۷۹

<div class="b" id="bn1"><div class="m1"><p>دوستان فکری به حالم کرده اند</p></div>
<div class="m2"><p>خون خواهشها حلالم کرده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معنیم را عین صورت دیده اند</p></div>
<div class="m2"><p>دشمن جان حلالم کرده اند؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبنم هوشی به جوش آورده اند</p></div>
<div class="m2"><p>غرق بحر انفعالم کرده اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خانه زاد خط و خالم دیده اند</p></div>
<div class="m2"><p>محرم بزم وصالم کرده اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آه از این ترکان کشمیری نسب</p></div>
<div class="m2"><p>طوطی بلبل مقالم کرده اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خنده شادی نمی سازد مرا</p></div>
<div class="m2"><p>سرخوش از جام ملالم کرده اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دور گردی بیش می سازد به من</p></div>
<div class="m2"><p>خوش نشین بزم حالم کرده اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ساغر صورت به دستم داده اند</p></div>
<div class="m2"><p>هر نفس معنی مآلم کرده اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اخترم را چشم بد بادا سپند</p></div>
<div class="m2"><p>پاره ای دور از وبالم کرده اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دختر رز تا حرامت کرده ام</p></div>
<div class="m2"><p>هر دو عالم را حلالم کرده اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کرده ام تا ترک ملتها اسیر</p></div>
<div class="m2"><p>مذهب و مشرب حلالم کرده اند</p></div></div>