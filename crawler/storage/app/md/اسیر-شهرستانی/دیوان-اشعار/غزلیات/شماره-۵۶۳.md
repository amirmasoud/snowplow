---
title: >-
    شمارهٔ ۵۶۳
---
# شمارهٔ ۵۶۳

<div class="b" id="bn1"><div class="m1"><p>عالم از جلوه تو خرم شد</p></div>
<div class="m2"><p>سایه گل آفتاب شبنم شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دورگردم به قرب می نازم</p></div>
<div class="m2"><p>هر که بیگانه گشت محرم شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زهر بیگانگی چشید و نمرد</p></div>
<div class="m2"><p>بوالهوس رفته رفته آدم شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق آیینه مآل نماست</p></div>
<div class="m2"><p>شادی از اختلاط ما غم شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در زمان من و تو مهر و وفا</p></div>
<div class="m2"><p>بیش از بیش شد کم از کم شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خونبهای اسیر تشنه لب است</p></div>
<div class="m2"><p>ساغری کز لب تو زمزم شد</p></div></div>