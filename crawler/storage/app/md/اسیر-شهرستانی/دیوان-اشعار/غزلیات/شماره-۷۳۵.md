---
title: >-
    شمارهٔ ۷۳۵
---
# شمارهٔ ۷۳۵

<div class="b" id="bn1"><div class="m1"><p>با لبش همزبانیی کردم</p></div>
<div class="m2"><p>مردم و زندگانیی کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک صبوحی زدم به یاد کسی</p></div>
<div class="m2"><p>صبح را باغبانیی کردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرد نسیان به خاطر افشاندم</p></div>
<div class="m2"><p>صید راز نهانیی کردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آرزوها به دل گره شده بود</p></div>
<div class="m2"><p>دیدمش جانفشانیی کردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفته بودم ز خاطر همه کس</p></div>
<div class="m2"><p>بر دل خود گرانیی کردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حرفی از دفتر جنون خواندم</p></div>
<div class="m2"><p>دعوی نکته دانیی کردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرد راه فتادگی گشتم</p></div>
<div class="m2"><p>با فلک هم عنانیی کردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی محابا زدم بر آتش اسیر</p></div>
<div class="m2"><p>در صف دل جوانیی کردم</p></div></div>