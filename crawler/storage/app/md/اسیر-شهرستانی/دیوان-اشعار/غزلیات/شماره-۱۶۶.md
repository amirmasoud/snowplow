---
title: >-
    شمارهٔ ۱۶۶
---
# شمارهٔ ۱۶۶

<div class="b" id="bn1"><div class="m1"><p>زعشق مرتبه حسن دلنشین پیداست</p></div>
<div class="m2"><p>زشیشه جوهر این آب آتشین پیداست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مخورفریب زشیرین لبان زیرنقاب</p></div>
<div class="m2"><p>نشان آبله روی انگبین پیداست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غمت نهان زکه دارم که همچو قبله نما</p></div>
<div class="m2"><p>تپیدن دل ازآیینه جبین پیداست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نهفته درلب خاموش حرص طول امل</p></div>
<div class="m2"><p>نشان جاده زهمواری زمین پیداست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کند چو شوخیت انگشتری ز حلقه زلف</p></div>
<div class="m2"><p>فروغ دست تو چون آب در نگین پیداست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نهان به باغ خیالت صبوحیی زده اند</p></div>
<div class="m2"><p>ز چهره گل و سیمای یاسمین پیداست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برای دعوی جوهر چه احتیاج گواه</p></div>
<div class="m2"><p>چو تیغ دستی اگر هست از آستین پیداست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلی نباخته باشی به آشنایی خویش</p></div>
<div class="m2"><p>زچهره بندی آیینه اینچنین پیداست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برای حسرت من باده خورده پنداری</p></div>
<div class="m2"><p>دلم گداخته زان روی آتشین پیداست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فکنده شور شنیدم کسی به قلب ریا</p></div>
<div class="m2"><p>اسیر توبه شکن بوده اینچنین پیداست</p></div></div>