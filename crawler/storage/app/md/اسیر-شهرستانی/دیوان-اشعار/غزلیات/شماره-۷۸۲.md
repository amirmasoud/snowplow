---
title: >-
    شمارهٔ ۷۸۲
---
# شمارهٔ ۷۸۲

<div class="b" id="bn1"><div class="m1"><p>چه سرخوشم که ندانم دل و کباب از هم</p></div>
<div class="m2"><p>چه بیخودم که ندانم گل و شراب از هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهار جلوه شوخ که گشته عالمگیر</p></div>
<div class="m2"><p>بتان کرشمه نمایند انتخاب از هم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دامن مژه دستی زدم چه دانستم</p></div>
<div class="m2"><p>که تیغ او نکند فرق خون و آب از هم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمیم برگ گل و گوشه نقاب از من</p></div>
<div class="m2"><p>فروغ آینه ماه و آفتاب از هم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سواد شوخی طفلان نمی شود روشن</p></div>
<div class="m2"><p>به رنگ گل بربایند اگر کتاب از هم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو آب می گذرند اهل دل ز یکدیگر</p></div>
<div class="m2"><p>نه همچو کینه پرستان به آب و تاب از هم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اسیر دلکده بی تکلفی از تو</p></div>
<div class="m2"><p>رسوم ساختگیهای شیخ و شاب از هم</p></div></div>