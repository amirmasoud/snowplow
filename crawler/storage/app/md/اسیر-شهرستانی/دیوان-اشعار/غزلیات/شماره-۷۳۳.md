---
title: >-
    شمارهٔ ۷۳۳
---
# شمارهٔ ۷۳۳

<div class="b" id="bn1"><div class="m1"><p>آتش از آن گرمی نگاه گرفتم</p></div>
<div class="m2"><p>تا عرق فتنه ای ز آه گرفتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوخت سراپای ما ز آتش پنهان</p></div>
<div class="m2"><p>بسکه چو مژگان به گریه راه گرفتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رنگ تپیدن نداشت خون شهیدان</p></div>
<div class="m2"><p>دامن پاک تو را گواه گرفتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم ودل ما بس است جلوه گه او</p></div>
<div class="m2"><p>گل به در دیر و خانقاه گرفتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسکه تپیدم به زیرپای سمندش</p></div>
<div class="m2"><p>خون خود از خاک صیدگاه گرفتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر سر راهش اسیر بسکه نشستم</p></div>
<div class="m2"><p>کام دل و دیده از نگاه گرفتم</p></div></div>