---
title: >-
    شمارهٔ ۷۱۸
---
# شمارهٔ ۷۱۸

<div class="b" id="bn1"><div class="m1"><p>لبت به سهو نوازد گرم به یک دشنام</p></div>
<div class="m2"><p>به من ز سستی طالع نمی رسد پیغام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دوست شکوه ندارم چه شد که عمر گذشت</p></div>
<div class="m2"><p>وفای ما بقرار و جفای او بدوام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به جلوه آمدی و سوختم مبارک باد</p></div>
<div class="m2"><p>مرا خزان حجاب و تو را بهار خرام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بهشت به رویم گشاده پنداری</p></div>
<div class="m2"><p>گهی که داده ندانسته ام جواب سلام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان تغافل صیاد کرده خاموشم</p></div>
<div class="m2"><p>که ناله ام نشنیده است گوش حلقه دام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کناره جوست ز من مهر یار و خرسندم</p></div>
<div class="m2"><p>که نیکنام گریزد ز صحبت بد نام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رهش به کلبه تاریک ما نمی افتد</p></div>
<div class="m2"><p>طلوع صبح نبود است در قلمرو شام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اسیر سلسله دام عشق می داند</p></div>
<div class="m2"><p>که دور از او به من خسته زندگی است حرام</p></div></div>