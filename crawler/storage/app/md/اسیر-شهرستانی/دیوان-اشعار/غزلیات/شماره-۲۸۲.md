---
title: >-
    شمارهٔ ۲۸۲
---
# شمارهٔ ۲۸۲

<div class="b" id="bn1"><div class="m1"><p>موجه دریا ز شورم مصرع پیچیده است</p></div>
<div class="m2"><p>در صدف گوهر ز اشکم معنی دزدیده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از هوای گلشن چاک گریبان کسی</p></div>
<div class="m2"><p>نکهت یوسف گل در پیرهن بالیده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناله خاموشی است تکرار فراموشی خوش است</p></div>
<div class="m2"><p>گفتگوی آشنایی گوش کس نشنیده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گریه ما دشت را مهمان دریا می کند</p></div>
<div class="m2"><p>صفحه بحر ازحباب و موج بزم چیده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ریاض آرزو گر خار بلبل سبز شد</p></div>
<div class="m2"><p>آنچه حاصل کرده ام عمری گل ناچیده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شرمساری ها عصیان خرمن اندوزی کند</p></div>
<div class="m2"><p>عضو عضو ما ز دهشت دانه پاشیده است</p></div></div>