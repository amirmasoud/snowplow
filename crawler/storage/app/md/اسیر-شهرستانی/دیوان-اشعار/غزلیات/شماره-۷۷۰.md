---
title: >-
    شمارهٔ ۷۷۰
---
# شمارهٔ ۷۷۰

<div class="b" id="bn1"><div class="m1"><p>صلحم نساخت با تو در جنگ می زنم</p></div>
<div class="m2"><p>یک شیشه خانه حوصله بر سنگ می زنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلهای رازگوش برآواز بلبلند</p></div>
<div class="m2"><p>خاموشیم رساست بر آهنگ می زنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عکس تو نازپرور و چشم زمانه شور</p></div>
<div class="m2"><p>هر دم به رنگی آینه در سنگ می زنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بسکه رشک قافله ام پایمال کرد</p></div>
<div class="m2"><p>منزل اگر شوم ره فرسنگ می زنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مستم اسیر از آن سرکویم چه می بری</p></div>
<div class="m2"><p>از خاک راه تکیه بر اورنگ می زنم</p></div></div>