---
title: >-
    شمارهٔ ۶۳۰
---
# شمارهٔ ۶۳۰

<div class="b" id="bn1"><div class="m1"><p>گر نگه نکهت گلزار حیا می باید</p></div>
<div class="m2"><p>جور هم قاصد پیغام وفا می باید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی جنون گم شده عشق به منزل نرسد</p></div>
<div class="m2"><p>خضر این بادیه زنجیر به پا می باید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خانه پرداز هوس نام محبت نبرد</p></div>
<div class="m2"><p>گر بداند که در این راه چها می باید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا دلم همسفر عشق شد آرام گرفت</p></div>
<div class="m2"><p>مست را بستر سنجاب هوا می باید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر تسلیم به جای قدمی دارد اسیر</p></div>
<div class="m2"><p>تا شنیده است که هر چیز بجا می باید</p></div></div>