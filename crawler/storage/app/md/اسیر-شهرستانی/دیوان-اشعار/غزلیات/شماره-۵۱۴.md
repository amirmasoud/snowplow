---
title: >-
    شمارهٔ ۵۱۴
---
# شمارهٔ ۵۱۴

<div class="b" id="bn1"><div class="m1"><p>سودای تو ما را به تمنای دگر برد</p></div>
<div class="m2"><p>آیینه حیرت به تماشای دگر برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند که احرام سرکوی تو بستم</p></div>
<div class="m2"><p>بیگانگی خوی توام جای دگر برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این زمزمه در گوش که گویم که بفهمد</p></div>
<div class="m2"><p>خاموشی من نسخه ز انشای دگر برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک شمه ز آوارگی خویش بگویم</p></div>
<div class="m2"><p>در هر قدمم شوق به صحرای دگر برد</p></div></div>