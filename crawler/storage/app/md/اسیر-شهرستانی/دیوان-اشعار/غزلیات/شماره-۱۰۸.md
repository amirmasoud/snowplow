---
title: >-
    شمارهٔ ۱۰۸
---
# شمارهٔ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>ای نام تو قبله زبان‌ها</p></div>
<div class="m2"><p>چشم دل‌ها چراغ جان‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آیینه راز توست عالم</p></div>
<div class="m2"><p>پیداست ز نام‌ها نشان‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ویران شده نسیم شوقت</p></div>
<div class="m2"><p>مانند حباب خانمان‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرمنزل توست بی‌نشانی</p></div>
<div class="m2"><p>گرد ره کیست کاروان‌ها؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شوق تو به هر دلی که افتاد</p></div>
<div class="m2"><p>پر کرد ز خاک‌ها دکان‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرسوده سجده در تو</p></div>
<div class="m2"><p>تا در فرق جبین آسمان‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گردید اسیر از دل تو</p></div>
<div class="m2"><p>آیینه گر یقین گمان‌ها</p></div></div>