---
title: >-
    شمارهٔ ۳۲۲
---
# شمارهٔ ۳۲۲

<div class="b" id="bn1"><div class="m1"><p>آنچه دل را می نوازد درد بیدرمان اوست</p></div>
<div class="m2"><p>آنکه جان را زنده دارد آتش پنهان اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده از مکتوب زخم تازه ای روشن نکرد</p></div>
<div class="m2"><p>دل شهید انتظار قاصد پیکان اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا غبار غیر ننشیند به طرف دامنش</p></div>
<div class="m2"><p>سرزمین دیده من عرصه جولان اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرم شد بازار چاک سینه در فصل بهار</p></div>
<div class="m2"><p>نکهت پیراهن گل گردی از دامان اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که از یاد تو شد در مصر تنهایی عزیز</p></div>
<div class="m2"><p>گر به بزم وصل یوسف جا کند زندان اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دلم چون غنچه می روید شکستن هر نفس</p></div>
<div class="m2"><p>شیشه من خانه زاد ساغر پیمان اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق هر جا از کمال خود سخن گوید اسیر</p></div>
<div class="m2"><p>شاه بیت آفرینش حرفی از دیوان اوست</p></div></div>