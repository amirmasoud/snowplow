---
title: >-
    شمارهٔ ۳۴۵
---
# شمارهٔ ۳۴۵

<div class="b" id="bn1"><div class="m1"><p>گرفتار کمند عشق را صیاد حاجت نیست</p></div>
<div class="m2"><p>شهید خنجر بیداد را جلاد حاجت نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلند آوازگی نخلی است از گلزار خاموشی</p></div>
<div class="m2"><p>گر از من بشنود مرغ چمن فریاد حاجت نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا خود شیشه دل می خورد بر سنگ ناکامی</p></div>
<div class="m2"><p>تو را در مکتب سنگین دلی استاد حاجت نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز تأثیر توکل گشته ام از خلق مستغنی</p></div>
<div class="m2"><p>مرا در بی نیازی از کسی امداد حاجت نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو دل در هجر میرد جان نخواهد تهنیت در وصل</p></div>
<div class="m2"><p>که در عید اهل ماتم را مبارکباد حاجت نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهار آمد که از فیض جنون گلها زنم بر سر</p></div>
<div class="m2"><p>اسیر این شور را سیر گل و شمشاد حاجت نیست</p></div></div>