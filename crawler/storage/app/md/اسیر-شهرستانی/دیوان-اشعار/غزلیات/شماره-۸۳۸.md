---
title: >-
    شمارهٔ ۸۳۸
---
# شمارهٔ ۸۳۸

<div class="b" id="bn1"><div class="m1"><p>همه تن آینه ای دست بر آیینه منه</p></div>
<div class="m2"><p>سوی خود بین و عبث در نظر آیینه منه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خبر از خویش نداری خبری می شنوی</p></div>
<div class="m2"><p>پر به کف ای ز خدا بی خبر آیینه منه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در تماشای رخت شش جهت آیینه گرند</p></div>
<div class="m2"><p>بیش از این منت دیدار بر آیینه منه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لذت وصل مپرس از دل ظاهر بینان</p></div>
<div class="m2"><p>نام کوته نظر بدگهر آیینه منه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سعی کن تا دل بیدار به دست آید اسیر</p></div>
<div class="m2"><p>دیده گر هست به دیوار و در آیینه منه</p></div></div>