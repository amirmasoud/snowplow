---
title: >-
    شمارهٔ ۱۴۳
---
# شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>خط بر سرهر حرف چو تقویم کشندت</p></div>
<div class="m2"><p>زان به که خطی بر سر تسلیم کشندت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای خاک نشین رتبه ات از دولت خواری است</p></div>
<div class="m2"><p>مگذار که در پله تعظیم کشندت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون گرد از این جاده بکش دامن همت</p></div>
<div class="m2"><p>حیف است که اقلیم به اقلیم کشندت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر صفحه رحمت رقم حرف امید است</p></div>
<div class="m2"><p>تیری که بر هر مو به تن از بیم کشندت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی شان ادب صورت دیوار وجودی</p></div>
<div class="m2"><p>هر چند که با خانه تعلیم کشندت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چند اسیر غمی از حرف میندیش</p></div>
<div class="m2"><p>در چشم کند سرمه گر از بیم کشندت؟</p></div></div>