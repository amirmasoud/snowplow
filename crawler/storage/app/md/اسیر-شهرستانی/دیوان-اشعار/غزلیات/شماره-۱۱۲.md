---
title: >-
    شمارهٔ ۱۱۲
---
# شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>ای گلشن از بهار خیال تو سینه‌ها</p></div>
<div class="m2"><p>برگ گل از طراوت نامت سفینه‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرجا غمت رواج دهد گوهر شکست</p></div>
<div class="m2"><p>بر سنگ خاره رشک برند آبگینه‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر از نسیم راز تو عالم چمن شود</p></div>
<div class="m2"><p>بوی گل صفا دمد از گرد کینه‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در جستجوی گوهر ذاتت فکنده چرخ</p></div>
<div class="m2"><p>از روز و شب به قلزم غیرت سفینه‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخشیده حشمتت به سلیمان ملک فقر</p></div>
<div class="m2"><p>از نقش پای مور کلید خزینه‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دنیاپرست حسرت جاوید می‌برد</p></div>
<div class="m2"><p>در خاک مانده از دل قارون دفینه‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در جلوه‌گاه سنگدلان شو غبار اسیر</p></div>
<div class="m2"><p>این است پاس خاطر آیینه سینه‌ها</p></div></div>