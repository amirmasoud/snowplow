---
title: >-
    شمارهٔ ۷۳۰
---
# شمارهٔ ۷۳۰

<div class="b" id="bn1"><div class="m1"><p>اگر عاقل اگر دیوانه مستم</p></div>
<div class="m2"><p>اگر بلبل اگر پروانه مستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دماغ خنده عیشم رساتر</p></div>
<div class="m2"><p>زجام گریه مستانه مستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شراب بیخودی ساغر ندارد</p></div>
<div class="m2"><p>همینم بس که بی پیمانه مستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل بی شیشه و پیمانه شوخی</p></div>
<div class="m2"><p>دل بی ساقی و میخانه مستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اسیرم بیدلم عاشق تماشا</p></div>
<div class="m2"><p>به یاد جلوه مستانه مستم</p></div></div>