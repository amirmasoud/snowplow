---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>چمن جلوه کن غبار مرا</p></div>
<div class="m2"><p>سبز کن باغ انتظار مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل و یادش خدا نگهدارد</p></div>
<div class="m2"><p>در طلسم خزان بهار مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق دیوانه خوش تماشایی است</p></div>
<div class="m2"><p>سیر کن سیر کار و بار مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خنده می آیدم چو می پرسی</p></div>
<div class="m2"><p>سبب گریه های زار مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سبق نازخوان چه وقت خط است</p></div>
<div class="m2"><p>مکن آشفته روزگار مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکه یک صید دامش آزادی است</p></div>
<div class="m2"><p>کی رها می کند شکار مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تاب دوری بس است اسیر امان</p></div>
<div class="m2"><p>سوختی سوختی قرار مرا</p></div></div>