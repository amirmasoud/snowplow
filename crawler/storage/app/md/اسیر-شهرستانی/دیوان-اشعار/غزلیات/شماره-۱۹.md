---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>زوالی نیست دست پیشتر را</p></div>
<div class="m2"><p>زدم در پرده راه هر نظر را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر سرد است اگر گرم است خونم</p></div>
<div class="m2"><p>به هر رگ بسته دست نیشتر را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برآید گرد اگر دریا و ازکان</p></div>
<div class="m2"><p>شکستی کی رسد آب گهر را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جنون نقش نگین خویش دارد</p></div>
<div class="m2"><p>نهان لوح طلسم خیر و شر را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پشیمانی مبارک باد تحسین</p></div>
<div class="m2"><p>بکش این مست از خود بی خبر را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسی در دیده دل سیرکردم</p></div>
<div class="m2"><p>یکی دیدم سواد خیر و شر را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اسیر از موجه شوقی به ساحل</p></div>
<div class="m2"><p>شکستم کشتی بحر خطر را</p></div></div>