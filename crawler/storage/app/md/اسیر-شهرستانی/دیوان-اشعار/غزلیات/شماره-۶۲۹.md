---
title: >-
    شمارهٔ ۶۲۹
---
# شمارهٔ ۶۲۹

<div class="b" id="bn1"><div class="m1"><p>اجر جفا و مزد وفا را که می دهد</p></div>
<div class="m2"><p>تاوان عمر رفته ما را که می دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سامان کشتیم زشکستن گذشت و رفت</p></div>
<div class="m2"><p>ای ناخدا جواب خدا را که می دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خندید صبح و کام دل نوبهار داد</p></div>
<div class="m2"><p>انعام ابر و مزد هوا را که می دهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما را ز بند حوصله آزاد کرده اند</p></div>
<div class="m2"><p>زنجیربانی دل ما را که می دهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان نظاره عمر وفا قبله حیا</p></div>
<div class="m2"><p>فردا جواب حسرت ما را که می دهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کام نگاه حوصله سیماب پیشکش</p></div>
<div class="m2"><p>داد دل شهید ادا را که می دهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای دورگرد کم نگه بی سخن بگو</p></div>
<div class="m2"><p>ما پیشکش جواب خدا را که می دهد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما را به جرم عشق شما می کشند های!</p></div>
<div class="m2"><p>ما از شما جواب شما را که می دهد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما بنده شما دل ما را که می کشد</p></div>
<div class="m2"><p>روز جزا جواب شما را که می دهد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اجر دل جفا کش ما کشته شما</p></div>
<div class="m2"><p>مزد کرشمه های شما را که می دهد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرمی غلام لطف تو الفت اسیر تو</p></div>
<div class="m2"><p>کام دل اسیر وفا را که می دهد</p></div></div>