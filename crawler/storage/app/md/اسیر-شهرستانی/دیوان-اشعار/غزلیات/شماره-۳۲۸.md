---
title: >-
    شمارهٔ ۳۲۸
---
# شمارهٔ ۳۲۸

<div class="b" id="bn1"><div class="m1"><p>اگر ستم کند امید احترامی هست</p></div>
<div class="m2"><p>جواب اگر دهد اندیشه سلامی هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز خاک رهگذرت صیدگاه می جوشد</p></div>
<div class="m2"><p>به هر طرف که سمند تو رفته دامی هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلل پذیر نگردد بنای ناز و نیاز</p></div>
<div class="m2"><p>جفای یار و وفای مرا دوامی هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نظاره محرم راز نهان عاشق نیست</p></div>
<div class="m2"><p>اگر خیال لبش می برد پیامی هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اسیر عشقم و صیاد وحشی سخنم</p></div>
<div class="m2"><p>به یاد چشم توام الفت تمامی هست</p></div></div>