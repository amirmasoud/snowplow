---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>چه دردسر دهم دیوانگی با سخت‌جانی را</p></div>
<div class="m2"><p>غرورش طعن الفت می‌زند شیرین و لیلی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لباس عاریت را اختیار از دیگران باشد</p></div>
<div class="m2"><p>ز منصب‌های گوناگون چه حاصل اهل دنیی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز عکس شبنمی طوفان بحر آتشی بیند</p></div>
<div class="m2"><p>اگر گلچین کند آیینهٔ دل پیش‌بینی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جزایی می‌دهد هرکس به رنگی در مکافاتش</p></div>
<div class="m2"><p>بود هر مجمعی حشر خجالت دزد معنی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غبارم نقش بالین می‌شود خواب قیامت را</p></div>
<div class="m2"><p>چنین گرمی دهد نازت شراب سرگرانی را</p></div></div>