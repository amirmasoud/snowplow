---
title: >-
    شمارهٔ ۵۷۶
---
# شمارهٔ ۵۷۶

<div class="b" id="bn1"><div class="m1"><p>دل شکاران که مرا بی سر و پا ساخته اند</p></div>
<div class="m2"><p>حلقه دام ز محراب دعا ساخته اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر همه سایه خاری است طوافی دارد</p></div>
<div class="m2"><p>کعبه شوق مرا در همه جا ساخته اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل ناکام نمیرد که نظر کرده اوست</p></div>
<div class="m2"><p>استخوان بندی قالب ز هما ساخته اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چاره از کوشش بی صرفه پشیمان شدن است</p></div>
<div class="m2"><p>برده اند از دل ما درد و دوا ساخته اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شش جهت چیست که آهی بکند بنیادش</p></div>
<div class="m2"><p>چار دیوار جهان را ز هوا ساخته اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل فولاد هم از آتش عشق آب شود</p></div>
<div class="m2"><p>گر بداند که در این کوره چه ها ساخته اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غم پنهانیم از دیده توان نقش زدن</p></div>
<div class="m2"><p>خلوت راز تو را پرده نما ساخته اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زنده بادا دلت از حسرت جاوید اسیر</p></div>
<div class="m2"><p>خاک این کوزه گل از آب بقا ساخته اند</p></div></div>