---
title: >-
    شمارهٔ ۱۳۹
---
# شمارهٔ ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>خون بود دل که لذت درد نهان شناخت</p></div>
<div class="m2"><p>این غنچه قطره بود که رنگ جنان شناخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آیینه است پرتو شمع مزار من</p></div>
<div class="m2"><p>در خواب هم خیال تو را می توان شناخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دارد نقیض گیری عاشق سرایتی؟</p></div>
<div class="m2"><p>حسن یقین من ز دل بدگمان شناخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیداست از جبین عدم عشق پرده سوز</p></div>
<div class="m2"><p>این باده را ز شیشه خارا توان شناخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شب خوابش از فسانه قتلم ربوده بود</p></div>
<div class="m2"><p>روزم ز اظطراب دل پاسبان شناخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روزی کتابخانه غفلت گشود دل</p></div>
<div class="m2"><p>تعبیر خواب الفت اهل جهان شناخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در پیش پای پرتو خورشید برنخاست</p></div>
<div class="m2"><p>گردی که جای خویش در آن آستان شناخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رنگ گل و فروغ می و لعل یار شد</p></div>
<div class="m2"><p>هر کس که قدر خویش چو آب روان شناخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گردی که شبنم گل این سرزمین نشد</p></div>
<div class="m2"><p>کی قرب مهر و منزلت آسمان شناخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خوابی که می برد به ره شوق راحت است</p></div>
<div class="m2"><p>دیوانه قدر بستر ریگ روان شناخت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حرز یقین به وسوسه دیدم که شد اسیر</p></div>
<div class="m2"><p>موری که گرد بیدلی کاروان شناخت؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پرواز هرزه راه به منزل نمی برد</p></div>
<div class="m2"><p>کی تیر بی سراغ محبت نشان شناخت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر دل که در ریاض وفا مست خواب شد</p></div>
<div class="m2"><p>کی لذت صبوحی این گلستان شناخت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از سیر باغ و بادیه حاصل نمی برد</p></div>
<div class="m2"><p>هرکس که گردباد ز سرو روان شناخت؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در خواب دید آینه عکس مراد من</p></div>
<div class="m2"><p>خود را اسیر محرم راز نهان شناخت</p></div></div>