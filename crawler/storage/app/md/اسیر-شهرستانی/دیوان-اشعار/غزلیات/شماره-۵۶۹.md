---
title: >-
    شمارهٔ ۵۶۹
---
# شمارهٔ ۵۶۹

<div class="b" id="bn1"><div class="m1"><p>تیغ بر کفش دیدم خون من به جوش آمد</p></div>
<div class="m2"><p>خنده زد گل زخمی ناله در خروش آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم او نگاهی کرد لعل او حدیثی گفت</p></div>
<div class="m2"><p>هوش مست و بیخود شد بیخودی به هوش آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نکهت بهار آمد ساغر طرب برکف</p></div>
<div class="m2"><p>مژده می پرستان را پیر می فروش آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیر دیر را دیدم سرنوشت پرسیدم</p></div>
<div class="m2"><p>گفت آیت رحمت بهر باده نوش آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که دید خندانش در قبای گلگون گفت</p></div>
<div class="m2"><p>سرو گل فروش آمد شمع شعله پوش آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در چمن گل و غنچه داد میکشی دادند</p></div>
<div class="m2"><p>این پیاله نوش آمد آن سبو به دوش آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون اسیر دیوانه توبه از ریا کردم</p></div>
<div class="m2"><p>حرف ناصحان ما را اینقدر به گوش آمد</p></div></div>