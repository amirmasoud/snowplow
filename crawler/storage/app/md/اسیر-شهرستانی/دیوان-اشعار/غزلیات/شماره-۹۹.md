---
title: >-
    شمارهٔ ۹۹
---
# شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>ز عندلیب چه پرسی نشان خانه ما</p></div>
<div class="m2"><p>که پی نبرده صبا هم به آشیانه ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهاده بر لب ما عشق مهر خاموشی</p></div>
<div class="m2"><p>که گوش کس نکند نوبر ترانه ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهار رفت و نچیدیم جز گل حسرت</p></div>
<div class="m2"><p>ز آب گریه مگر سبز گشت دانه ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بهر گمشدگان دگر صبا نگذاشت</p></div>
<div class="m2"><p>دلیل بادیه خاکستر نشانه ما</p></div></div>