---
title: >-
    شمارهٔ ۱۱۶
---
# شمارهٔ ۱۱۶

<div class="b" id="bn1"><div class="m1"><p>بس که می‌ترسم از جدایی‌ها</p></div>
<div class="m2"><p>می‌گریزم از آشنایی‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناله‌خیز است متصل چون نی</p></div>
<div class="m2"><p>بند بند من از جدایی‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل منت گزیده می‌داند</p></div>
<div class="m2"><p>که چه درد است با دوایی‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تربتم را بهار آبله کرد</p></div>
<div class="m2"><p>گل باغ برهنه‌پایی‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عالم آیینه‌خانه راز است</p></div>
<div class="m2"><p>هست در پرده خودنمایی‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرم از تیغ هم جدا نشود</p></div>
<div class="m2"><p>بس که می‌ترسم از جدایی‌ها</p></div></div>