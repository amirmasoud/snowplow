---
title: >-
    شمارهٔ ۱۴۹
---
# شمارهٔ ۱۴۹

<div class="b" id="bn1"><div class="m1"><p>تا روغن چراغ دلم درد غربت است</p></div>
<div class="m2"><p>اسباب خانه وطنم گرد غربت است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی می شود شکنجه کش دامن وطن</p></div>
<div class="m2"><p>پای طلب که آبله پرورد غربت است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر جا که می روم سفر کعبه من است</p></div>
<div class="m2"><p>از فیض عشق خضر رهم گرد غربت است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آزاد کرده سفر بی تکلفم</p></div>
<div class="m2"><p>در دل مرا خیال وطن درد غربت است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آتش دلیل،داغ جگر توشه،گریه آب</p></div>
<div class="m2"><p>با عشق هرکه کرد سفر مرد غربت است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مانند گردباد غریب است در وطن</p></div>
<div class="m2"><p>هرکس که در میانه جهانگرد غربت است؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گردد جهان نورد اگر بشنود اسیر</p></div>
<div class="m2"><p>این سرگذشت ما که ره آورد غربت است</p></div></div>