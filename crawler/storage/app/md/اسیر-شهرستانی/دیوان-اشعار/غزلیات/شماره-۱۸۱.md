---
title: >-
    شمارهٔ ۱۸۱
---
# شمارهٔ ۱۸۱

<div class="b" id="bn1"><div class="m1"><p>سینه صافیهای من روشنگر است</p></div>
<div class="m2"><p>کعبه هم آیینه اسکندر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سنبل خط جانشین زلف شد</p></div>
<div class="m2"><p>باغ حسنش را صفای دیگر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نایب ابروست مژگان دراز</p></div>
<div class="m2"><p>سبزه باغ کسی آهنگر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گریه ما را داده عمر جاودان</p></div>
<div class="m2"><p>چشمه حیوان ما چشم تر است</p></div></div>