---
title: >-
    شمارهٔ ۶۵۲
---
# شمارهٔ ۶۵۲

<div class="b" id="bn1"><div class="m1"><p>پیداست عکس چهره گل در هوای ابر</p></div>
<div class="m2"><p>آیینه خانه کرده چمن را صفای ابر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مستند شاهدان بهار از می هوا</p></div>
<div class="m2"><p>سرها برهنه کرده غزلخوان به پای ابر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل می کند دماغ تر از خاک خشک زهد</p></div>
<div class="m2"><p>ساقی است تا هوای رطوبت فزای ابر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیوانه باده می کشد و می کند سماع</p></div>
<div class="m2"><p>بزم پری است در نظر جلوه های ابر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از مکتب گرفتگی آزاد می شود</p></div>
<div class="m2"><p>طفلی است روز جمعه خمار هوای ابر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گلهای ابر خیل غزالان وحشیند</p></div>
<div class="m2"><p>شوخ است صید کشت هوا روزهای ابر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر سبزه رشک مصرع شوخی است در نظر</p></div>
<div class="m2"><p>طرح غزل نموده بهار از برای ابر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما خویش را به ساقی کوثر سپرده ایم</p></div>
<div class="m2"><p>تکلیف باده می چکد از شیوه های ابر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دام پری کشیده بهار از شکفتگی</p></div>
<div class="m2"><p>دیوانه تو چون نشود آشنای ابر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیداست عکس چهره ساقی ز برگ گل</p></div>
<div class="m2"><p>از بسکه شست روی چمن را صفای ابر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هریک اسیر از دگری خوشنماتر است</p></div>
<div class="m2"><p>هم خنده های شیشه و هم گریه های ابر</p></div></div>