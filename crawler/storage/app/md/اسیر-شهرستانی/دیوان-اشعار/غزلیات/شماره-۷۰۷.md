---
title: >-
    شمارهٔ ۷۰۷
---
# شمارهٔ ۷۰۷

<div class="b" id="bn1"><div class="m1"><p>یکدلم گر چه پریشان نظرم ساخته عشق</p></div>
<div class="m2"><p>گوشه گیرم که چنین در به درم ساخته عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از قفس ماندم و پرواز به دردم نرسید</p></div>
<div class="m2"><p>مگر از پرده دل بال و پرم ساخته عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا در آیینه دیگر نشناسم خود را</p></div>
<div class="m2"><p>بی تو هر لحظه به رنگ دگرم ساخته عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا دلیرانه از شعله زنم بر صف داغ</p></div>
<div class="m2"><p>چون دل خویش سراپا جگرم ساخته عشق</p></div></div>