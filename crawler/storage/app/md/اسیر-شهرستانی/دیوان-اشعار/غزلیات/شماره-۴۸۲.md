---
title: >-
    شمارهٔ ۴۸۲
---
# شمارهٔ ۴۸۲

<div class="b" id="bn1"><div class="m1"><p>دل ما زخمی از مرهم ندارد</p></div>
<div class="m2"><p>اگر شادی ندارد غم ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز من مجنون گریزد دشت در دشت</p></div>
<div class="m2"><p>چه شد زنجیر پای کم دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم آن غنچه بی آب و رنگ است</p></div>
<div class="m2"><p>که در زیر نگین شبنم ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشو آزرده دل از مردن من</p></div>
<div class="m2"><p>که مرگ چون منی ماتم ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اسیرت را فلک بخشیده دردی</p></div>
<div class="m2"><p>که در خاک و گل آدم ندارد</p></div></div>