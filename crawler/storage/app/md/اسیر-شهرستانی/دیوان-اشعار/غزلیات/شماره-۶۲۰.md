---
title: >-
    شمارهٔ ۶۲۰
---
# شمارهٔ ۶۲۰

<div class="b" id="bn1"><div class="m1"><p>دشمن هوشی به رویت چشم تر نتوان گشود</p></div>
<div class="m2"><p>آفت نظاره ای سویت نظر نتوان گشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا سپردم دل به نومیدی چها دیدم مپرس</p></div>
<div class="m2"><p>کو دری کز فیض آه بی اثر نتوان گشود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذوق راحت را به درگاه محبت بار نیست</p></div>
<div class="m2"><p>دست خدمت بر میان داری کمر نتوان گشود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گریه تا کردم به کارم عقده دیگر فتاد</p></div>
<div class="m2"><p>چون گره در رشته ای گردید تر نتوان گشود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسکه در اظهار شوق طره ات پیچیده ام</p></div>
<div class="m2"><p>نامه ام از بال مرغ نامه بر نتوان گشود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرزه گردم چشم سرگردانی از من روشن است</p></div>
<div class="m2"><p>بیدلم یک عقده از کار سفر نتوان گشود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل ز چاک سینه وصل او تمنا کرد و سوخت</p></div>
<div class="m2"><p>خس چه می داند به روی شعله در نتوان گشود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رسم و آیین گرفتاری ندانم چون اسیر</p></div>
<div class="m2"><p>اینقدر دانم که در دام تو پر نتوان گشود</p></div></div>