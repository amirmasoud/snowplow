---
title: >-
    شمارهٔ ۳۸۵
---
# شمارهٔ ۳۸۵

<div class="b" id="bn1"><div class="m1"><p>تا غمش در دلم قرار گرفت</p></div>
<div class="m2"><p>برگ گل شعله در کنار گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خویش آتش ز گل نمی دانست</p></div>
<div class="m2"><p>دل ما را ز ما چکار گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل یکرنگ خویش را نازم</p></div>
<div class="m2"><p>خویش را تنگ در کنار گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی تو دیگر چه می توان گفت</p></div>
<div class="m2"><p>چشم آیینه ها غبار گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرو رفتار و غنچه گفتار</p></div>
<div class="m2"><p>چقدر از تو اعتبار گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوی بیگانگی چنین افروخت</p></div>
<div class="m2"><p>نقد دلبستگی عیار گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به وفای سرشگ خود نازم</p></div>
<div class="m2"><p>که گلاب از گل مزار گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>الحذر الحذر ز ساختگی</p></div>
<div class="m2"><p>نتوان خوی روزگار گرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا شدم خاک راه یار اسیر</p></div>
<div class="m2"><p>اعتبار من اعتبار گرفت</p></div></div>