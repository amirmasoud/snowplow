---
title: >-
    شمارهٔ ۲۳۶
---
# شمارهٔ ۲۳۶

<div class="b" id="bn1"><div class="m1"><p>فتادگی ثمر نخل سرفرازی ماست</p></div>
<div class="m2"><p>خزان یاس گل باغ بی نیازی ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به زیر تیغ تو بی اختیار می رقصیم</p></div>
<div class="m2"><p>ز سرگذشتگی ما کمینه بازی ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نظر به دیده پاک است ابر رحمت را</p></div>
<div class="m2"><p>چو قطره دامن تر جامه نمازی ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگاه گرم تو در عالم آرزو نگذاشت</p></div>
<div class="m2"><p>تغافل است که در فکر کارسازی ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو ذره همسفر آفتاب خود شده ایم</p></div>
<div class="m2"><p>اسیر باد صبا داغ برق تازی ماست</p></div></div>