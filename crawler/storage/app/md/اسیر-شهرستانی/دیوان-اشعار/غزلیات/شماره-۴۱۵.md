---
title: >-
    شمارهٔ ۴۱۵
---
# شمارهٔ ۴۱۵

<div class="b" id="bn1"><div class="m1"><p>که گفته سنگ دلت کفه محبت باد</p></div>
<div class="m2"><p>دعای کیست که صبرم رمیده طاقت باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا به عاشق دیوانه رحم می آید</p></div>
<div class="m2"><p>غریب کیست دلش آشنای طاقت باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آتش دل الفت ندیده خویشم</p></div>
<div class="m2"><p>که آرمیدگیش صید دام وحشت باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزار رنگ شکفتند میکشان ساقی</p></div>
<div class="m2"><p>گل همیشه بهار تو ابر رحمت باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهار آمده رنگین تر از نگار اسیر</p></div>
<div class="m2"><p>به عزم توبه شکستن رسیده فرصت باد</p></div></div>