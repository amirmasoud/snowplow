---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>چه گویم با کسی راز دل دیوانه خود را</p></div>
<div class="m2"><p>که خوابم می برد گر سرکنم افسانه خود را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرانجام خیال توتیای غیرتی دارم</p></div>
<div class="m2"><p>به چشم خود کشم خاکستر پروانه خود را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غبار خاطرم خوش گریه آلود است می خواهم</p></div>
<div class="m2"><p>به سیل اضطراب دل دهم ویرانه خود را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندارم سجده ای کز عهده خجلت برون آیم</p></div>
<div class="m2"><p>سرکوی وفا یعنی عبادتخانه خود را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کجا صد روزگار از عهده موجی برون آید</p></div>
<div class="m2"><p>جلو ریزی دهم گر گریه مستانه خود را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمی دانم کجا پیدا کنم چندان دل دعوی</p></div>
<div class="m2"><p>بیارایم اگر از بهر او کاشانه خود را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اسیر امشب نمی دانم چه گفتم یا چه ها کردم</p></div>
<div class="m2"><p>دل دیوانه خود را دل دیوانه خود را</p></div></div>