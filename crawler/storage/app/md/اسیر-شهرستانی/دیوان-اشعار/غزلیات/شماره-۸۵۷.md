---
title: >-
    شمارهٔ ۸۵۷
---
# شمارهٔ ۸۵۷

<div class="b" id="bn1"><div class="m1"><p>داریم بر تو احترامی</p></div>
<div class="m2"><p>دیدیم جواب از سلامی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر باده سبو سبو نباشد</p></div>
<div class="m2"><p>دریاب مرا به نیم جانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناکامی دهر قسمت ماست</p></div>
<div class="m2"><p>ماییم که دیده ایم کامی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای دل ز دلش وفا چه پرسی</p></div>
<div class="m2"><p>در سوختگی هنوز خامی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صد درس جنون اسیر خواندی</p></div>
<div class="m2"><p>داغم که هنوز ناتمامی</p></div></div>