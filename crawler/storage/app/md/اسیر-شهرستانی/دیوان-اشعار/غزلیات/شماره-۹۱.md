---
title: >-
    شمارهٔ ۹۱
---
# شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>بهار تنگدلی سبز کرد حاصل ما</p></div>
<div class="m2"><p>عبیر غنچه غبار خرابه دل ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زموج چون نشناسد جوهر تیغش</p></div>
<div class="m2"><p>هنوز شوق ندانسته گشت قاتل ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حباب چشمه نزدیک راه تفرقه است</p></div>
<div class="m2"><p>خراب سیل غبار است خانه دل ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دمید دانه و در تنگنای خوشه خزید</p></div>
<div class="m2"><p>به غیر عقده چه دید از گشاد مشگل ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به یاد روی تو در آتشیم همچو اسیر</p></div>
<div class="m2"><p>دل گداخته ما چراغ محفل ما</p></div></div>