---
title: >-
    شمارهٔ ۶۵۸
---
# شمارهٔ ۶۵۸

<div class="b" id="bn1"><div class="m1"><p>بیتابم ز حوصله صاحب کمال تر</p></div>
<div class="m2"><p>بد مستیم ز توبه بیجا وبال تر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آهم غبار جلوه و اشکم شرر نسب</p></div>
<div class="m2"><p>آیینه از جمال تو پر خط و خال تر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ابر غبار تربت ما ذره بار شد</p></div>
<div class="m2"><p>خورشید جلوه های رسا بی زوال تر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قامت رسا و جلوه مستانه خوش نما</p></div>
<div class="m2"><p>باغ قیامت است تماشا نهال تر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تعبیر خواب فتنه پریشانی دل است</p></div>
<div class="m2"><p>آشفتگی زلف تو فرخنده فال تر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهتر ز خون بی رگ ما خاک خفتگان</p></div>
<div class="m2"><p>درد فنا ز کوثر هستی زلال تر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرواز می کنم که اسیر تو گشته ام</p></div>
<div class="m2"><p>در دام بیقراریم آسوده بال تر</p></div></div>