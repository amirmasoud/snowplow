---
title: >-
    شمارهٔ ۱۱۹
---
# شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>عارضت گلدسته باغ نظر دارم بیا</p></div>
<div class="m2"><p>انتظارت بیشتر از بیشتر دارم بیا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی تماشای رخت گلدسته بند حسرتم</p></div>
<div class="m2"><p>جان به لب خون در جگر گل در نظر دارم بیا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبح محشر را نمکسود جراحت می کنم</p></div>
<div class="m2"><p>با تو امشب یک دو حرف مختصر دارم بیا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بازگشتن گر چه هست اما وداع تربت است</p></div>
<div class="m2"><p>می روم از خویشتن عزم سفر دارم بیا</p></div></div>