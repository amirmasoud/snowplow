---
title: >-
    شمارهٔ ۵۴۷
---
# شمارهٔ ۵۴۷

<div class="b" id="bn1"><div class="m1"><p>گریختم که به گرد ره تو کس نرسد</p></div>
<div class="m2"><p>گداختم که به آیینه ات نفس نرسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به غنچه از گل همراهی صبا چه رسید؟</p></div>
<div class="m2"><p>کسی به کام دل از سعی هیچ کس نرسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهار سوختگی را طراوت دگر است</p></div>
<div class="m2"><p>اگر چه گل چمن آرا بود به خس نرسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به شاهراه وفا نام شکوه کس نشنید</p></div>
<div class="m2"><p>چه شد که ناله به درد دل جرس نرسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چه صحن گلستان طلسم آب و هواست</p></div>
<div class="m2"><p>به گرد گوشه بی توشه قفس نرسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سپند اشک به آتش فشان به گلشن دل</p></div>
<div class="m2"><p>که آفتی به ثمرهای پیشرس نرسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اسیر جذبه عشق از هوا مدار طمع</p></div>
<div class="m2"><p>که فیض بال هما از پر مگس نرسد</p></div></div>