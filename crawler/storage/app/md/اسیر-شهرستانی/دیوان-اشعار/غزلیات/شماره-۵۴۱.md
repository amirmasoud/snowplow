---
title: >-
    شمارهٔ ۵۴۱
---
# شمارهٔ ۵۴۱

<div class="b" id="bn1"><div class="m1"><p>دل رمیده به صد آب و تاب می سوزد</p></div>
<div class="m2"><p>گهی ز صبر و گه از اضطراب می سوزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خوابم آمد و پنهان زد آتشی به دلم</p></div>
<div class="m2"><p>چراغ بخت اسیران به خواب می سوزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نهفته در بغل موج عکس روی تو را</p></div>
<div class="m2"><p>دلم به ساده دلیهای آب می سوزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر جمال تو مشاطه بهار شود</p></div>
<div class="m2"><p>ز رشک سایه گل آفتاب می سوزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سیاه بختی زاهد نگر به بزم شراب</p></div>
<div class="m2"><p>که در بهشت چو اهل عذاب می سوزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز شعله گرمی بی اختیار می بیند</p></div>
<div class="m2"><p>دلم بر آتش رشک کباب می سوزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوای مرغ چمن گر شود کلام اسیر</p></div>
<div class="m2"><p>گل از خجالت نظمش کتاب می سوزد</p></div></div>