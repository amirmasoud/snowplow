---
title: >-
    شمارهٔ ۵۱۳
---
# شمارهٔ ۵۱۳

<div class="b" id="bn1"><div class="m1"><p>پروانه چراغ وفا پر کجا برد</p></div>
<div class="m2"><p>هرکس که گشت گرد سری سرکجا برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنجیر موج باده شوریدگی بس است</p></div>
<div class="m2"><p>دیوانه تو شیشه و ساغر کجا برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عهد تو هرزه درد سر ناز می دهد</p></div>
<div class="m2"><p>آیینه شکسته سکندر کجا برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خار ره تو منت دریا نمی کشد</p></div>
<div class="m2"><p>جوهر شناس آبله گوهر کجا برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دام فنا قلمرو آسودگی بس است</p></div>
<div class="m2"><p>چون گل شکار زخم تو بستر کجا برد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک نامه برد ناله و گردم به باد داد</p></div>
<div class="m2"><p>در حیرتم که نامه دیگر کجا برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ممنون چاره نیست دل بیقرار ما</p></div>
<div class="m2"><p>دریای اضطراب شناور کجا برد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>راه از تو منزل از تو و آوارگی ز تو</p></div>
<div class="m2"><p>گم گشته دیار تو ره بر کجا برد؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پرواز شوق گرمتر از بال آتش است</p></div>
<div class="m2"><p>غمنامه اسیر کبوتر کجا برد</p></div></div>