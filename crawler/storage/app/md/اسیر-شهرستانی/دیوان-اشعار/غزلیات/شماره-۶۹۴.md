---
title: >-
    شمارهٔ ۶۹۴
---
# شمارهٔ ۶۹۴

<div class="b" id="bn1"><div class="m1"><p>از قدش جلوه انتخاب فروش</p></div>
<div class="m2"><p>از رخش باده آفتاب فروش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق در پرده جلوه گر شده باز</p></div>
<div class="m2"><p>دیده حیرتم نقاب فروش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غفلت آباد سرنوشت من است</p></div>
<div class="m2"><p>عمرم افسانه ای است خواب فروش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دهر میخانه نگاه تو شد</p></div>
<div class="m2"><p>سوخت هنگامه شراب فروش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غنچه بادام تلخ شد در باغ</p></div>
<div class="m2"><p>زهر خند که شد عتاب فروش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غنچه گردد گل شکفته عیش</p></div>
<div class="m2"><p>گر دل ما شود شراب فروش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گریه را رنگ و بوی معشوق است</p></div>
<div class="m2"><p>دیده ما نشد گلاب فروش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شوخی گلشنش ز مکتب برد</p></div>
<div class="m2"><p>گل صد برگ شد کتاب فروش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کام دل تر نشد ز اشک اسیر</p></div>
<div class="m2"><p>لب دریا که دیده آب فروش</p></div></div>