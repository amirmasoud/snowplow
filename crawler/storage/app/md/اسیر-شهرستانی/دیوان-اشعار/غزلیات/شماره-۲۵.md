---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>در دل گداختیم تمنای خویش را</p></div>
<div class="m2"><p>شاید که ناله گرم کند جای خویش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرصت سلم خریده بازار محنتم</p></div>
<div class="m2"><p>امروز می خورم غم فردای خویش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان پیشتر که گریه شود رو شناس ما</p></div>
<div class="m2"><p>شستیم سرنوشت مداوای خویش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آخر دچار کوی تو شد گرد تربتم</p></div>
<div class="m2"><p>دیدم بهار آبله پای خویش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یاد قدی ز بسکه به دل داشتم اسیر</p></div>
<div class="m2"><p>بگداختم چو شمع سراپای خویش را</p></div></div>