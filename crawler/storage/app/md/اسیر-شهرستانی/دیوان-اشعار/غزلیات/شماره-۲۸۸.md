---
title: >-
    شمارهٔ ۲۸۸
---
# شمارهٔ ۲۸۸

<div class="b" id="bn1"><div class="m1"><p>خلوت جان را تجلی از چراغ سینه است</p></div>
<div class="m2"><p>پنبه داغ دل خون گشته داغ سینه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بهارش هر قدر حیرت تماشا می کند</p></div>
<div class="m2"><p>گلشن سیر بنا گوشت دماغ سینه است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبح و شامم از گل داغ محبت گلشن است</p></div>
<div class="m2"><p>روز خورشید دل است و شب چراغ سینه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر سرمویش به هم بد مستی خون می کنند</p></div>
<div class="m2"><p>عاشق دیوانه را رحمت سراغ سینه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از خیالت باغبان گلشن خویش است اسیر</p></div>
<div class="m2"><p>سیرگاهش از گل زخم تو داغ سینه است</p></div></div>