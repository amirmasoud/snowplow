---
title: >-
    شمارهٔ ۶۷۵
---
# شمارهٔ ۶۷۵

<div class="b" id="bn1"><div class="m1"><p>دست اگر داری به غیر از دامن صحرا مگیر</p></div>
<div class="m2"><p>پرنیان خار تا باشد به مسند جا مگیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرد خاطرها مگرد و راه بر گلها مگیر</p></div>
<div class="m2"><p>در دل یاریم از این خوشتر سراغ ما مگیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناتوانی قوتی دارد که خارا موم اوست</p></div>
<div class="m2"><p>کوهکن را در حساب مردم دانا مگیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زور بازوی نزاکت عرض عزت می برد</p></div>
<div class="m2"><p>پنجه با خورشید اگر گیری به استغنا مگیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر در افتد قطره با دریا که نقصان می کند</p></div>
<div class="m2"><p>نکته بیجا به حرف مردم دانا مگیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی خبر دارد درون خانه از بیرون در</p></div>
<div class="m2"><p>دیده ای داری سراغ ره ز نابینا مگیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سایه خار تعلق آتش افروز دل است</p></div>
<div class="m2"><p>الفت آسان نیست با دنیا و مافیها مگیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جذبه کامل نداری دل به سختی سوختی</p></div>
<div class="m2"><p>نیستی آتش گلاب از شیشه دلها مگیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زحمت میدان مده گر نیستی غالب حریف</p></div>
<div class="m2"><p>مرد کشتی چون نه ای جز در کناری جا مگیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست آسان قبضه اهل قناعت خواستن</p></div>
<div class="m2"><p>یا بگیر و بر سر صد آرزو زن یا مگیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گوشه گیری خویش را رسوای عالم کردن است</p></div>
<div class="m2"><p>گر سر شهرت نداری شیوه عنقا مگیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قدر نشناس دل دیوانه خویشی اسیر</p></div>
<div class="m2"><p>حیف از این آیینه زنگ مردم دنیا مگیر</p></div></div>