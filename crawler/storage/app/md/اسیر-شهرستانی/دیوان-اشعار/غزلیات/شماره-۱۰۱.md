---
title: >-
    شمارهٔ ۱۰۱
---
# شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>با شکوه همزبان نشود گفتگوی ما</p></div>
<div class="m2"><p>پیچیده همچو گریه نفس در گلوی ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما آبروی خویش به عالم نمی دهیم</p></div>
<div class="m2"><p>بیهوده گریه ریخت به خاک آبروی ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دل غمین مباش که در وادی طلب</p></div>
<div class="m2"><p>آوارگی دو اسبه کند جستجوی ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما را دلیل بادیه سرگشتگی بس است</p></div>
<div class="m2"><p>منت ز خضر هم نکشد آروزی ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آسوده ایم با غم شوخی که تا به حشر</p></div>
<div class="m2"><p>آسودگی سراغ نیابد زکوی ما</p></div></div>