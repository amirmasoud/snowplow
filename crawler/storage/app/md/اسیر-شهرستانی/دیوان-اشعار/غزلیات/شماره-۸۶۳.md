---
title: >-
    شمارهٔ ۸۶۳
---
# شمارهٔ ۸۶۳

<div class="b" id="bn1"><div class="m1"><p>نمی دانم زبان دادخواهی</p></div>
<div class="m2"><p>گرفتار توام خواهی نخواهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر صیقل گر لطفت نباشد</p></div>
<div class="m2"><p>چه خواهم کرد با این رو سیاهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قناعت می دهد داد دل ما</p></div>
<div class="m2"><p>در این کشور گدایی پادشاهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دماغ دشمنی با کس ندارم</p></div>
<div class="m2"><p>خجالت می کشم از کینه خواهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان چشمم به رویش گشته حیران</p></div>
<div class="m2"><p>که نشناسد سفیدی از سیاهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سراپا انتخاب خاطر ماست</p></div>
<div class="m2"><p>برای ما نگه دارش الهی</p></div></div>