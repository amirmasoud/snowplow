---
title: >-
    شمارهٔ ۸۲۳
---
# شمارهٔ ۸۲۳

<div class="b" id="bn1"><div class="m1"><p>گر به محشر لطف ساقی عذرخواه آید برون</p></div>
<div class="m2"><p>کو دلی کز عهده شرم گناه آید برون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده ام خوابی که تعبیرش سراسر حیرت است</p></div>
<div class="m2"><p>تا کجا با جلوه محشر پناه آید برون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرسش دیوانم از جوش گناهان دور باد</p></div>
<div class="m2"><p>دوزخ آن روزی که عاشق بی گناه آید برون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گریه می کردم چه دانستم که صیاد مرا</p></div>
<div class="m2"><p>سبزه محشر ز خاک صیدگاه آید برون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خنده بر خاکستر حسرت شرار ما مزن</p></div>
<div class="m2"><p>آنقدر بنشین که آن مژگان سیاه آید برون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می تپد در خون خود از خجلت قاتل اسیر</p></div>
<div class="m2"><p>کرده صید تقصیر و ترسد بی‌گناه آید برون</p></div></div>