---
title: >-
    شمارهٔ ۴۵۵
---
# شمارهٔ ۴۵۵

<div class="b" id="bn1"><div class="m1"><p>ندیدم آن لب شیرین بخندد</p></div>
<div class="m2"><p>ندیدم غنچه پروین بخندد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه غم دارد دلم از زخم و از چاک</p></div>
<div class="m2"><p>بگو بر دامن گلچین بخندد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز شوقت هر کسی در انتظاری</p></div>
<div class="m2"><p>بیا تا آن بگرید و این بخندد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر این تأثیر دارد عشقبازی</p></div>
<div class="m2"><p>به روز مهربانی کین بخندد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر گرید کسی برحال فرهاد</p></div>
<div class="m2"><p>بگو بر خجلت شیرین بخندد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اسیر از گریه عالم گل فشان کرد</p></div>
<div class="m2"><p>اگر گرید بگو رنگین بخندد</p></div></div>