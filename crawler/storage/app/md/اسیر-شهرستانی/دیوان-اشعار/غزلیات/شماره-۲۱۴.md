---
title: >-
    شمارهٔ ۲۱۴
---
# شمارهٔ ۲۱۴

<div class="b" id="bn1"><div class="m1"><p>هر کجا موج هیاهوی دل است</p></div>
<div class="m2"><p>تا نفس پر می کشد بوی دل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می توان کردن دل خود را نگاه</p></div>
<div class="m2"><p>خوی من نازکتر از خوی دل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر محبت گلشن آرایی کند</p></div>
<div class="m2"><p>سلسبیلش موجه جوی دل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پایمال فوج مژگان گشتم آه</p></div>
<div class="m2"><p>گرد من محو تکاپوی دل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرکجا می می کشد بیخود اسیر</p></div>
<div class="m2"><p>ساغرش لبریز از هوی دل است</p></div></div>