---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>چو شمع سوختگی تر کند دماغ مرا</p></div>
<div class="m2"><p>نگاه گرم دهد روشنی چراغ مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهار تشنه خونم شود اگر داند</p></div>
<div class="m2"><p>که آب تیغ تو سرسبز کرده باغ مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به عزم کوی تو آواره چمن شده ام</p></div>
<div class="m2"><p>ز بوی گل نکند تا کسی سراغ مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به کار سوختنم شعله چون کند تقصیر</p></div>
<div class="m2"><p>نخوانده است مگر سرنوشت داغ مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرم اسیر زسودای ساقیی گرم است</p></div>
<div class="m2"><p>که از شکستن دل پرکند ایاغ مرا</p></div></div>