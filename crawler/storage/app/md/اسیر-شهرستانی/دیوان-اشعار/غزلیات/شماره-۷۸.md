---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>چه حرف مهر و وفا گوش کرده ای از ما</p></div>
<div class="m2"><p>چه دیده ای که فراموش کرده ای از ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهار سوختگی چاک دلق عریانی است</p></div>
<div class="m2"><p>چه شعله ها که قصب پوش کرده ای از ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مباد دردسر قیل و قالت ای غفلت</p></div>
<div class="m2"><p>چراغ مدرسه خاموش کرده ای از ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تبسمی که نمکپوش کرده ای از دل</p></div>
<div class="m2"><p>ترحمی که جفا کوش کرده ای از ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جان مشرب ما می خورد ورع سوگند</p></div>
<div class="m2"><p>چه توبه ها که قدح نوش کرده ای از ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خمار حوصله سوز است نشئه رنگین تر</p></div>
<div class="m2"><p>غمی که باده سرجوش کرده ای از ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اسیر منفعل از آرزو نمی گردی</p></div>
<div class="m2"><p>چه حلقه ها که نه در گوش کرده ای از ما</p></div></div>