---
title: >-
    شمارهٔ ۵۸۲
---
# شمارهٔ ۵۸۲

<div class="b" id="bn1"><div class="m1"><p>عشق نگشوده طلسمی است که بر دل بستند</p></div>
<div class="m2"><p>آه از این عقده آسان که چه مشکل بستند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه صید قفسم کی روم از خاطر دام</p></div>
<div class="m2"><p>در هواداری من عهد به یک دل بستند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق موجی است که ساغر کش گرداب فناست</p></div>
<div class="m2"><p>لب این بحر ز خمیازه ساحل بستند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جگر صید حرم سوز شهیدان وفا</p></div>
<div class="m2"><p>اول احرام به نقش پی قاتل بستند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شدم آواره و بی دام ندیدم طرفی</p></div>
<div class="m2"><p>پایم از رشته صد راه به منزل بستند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رخصت گفت و شنید از نگهت داشت اسیر</p></div>
<div class="m2"><p>دل و جان راهش از اندیشه باطل بستند</p></div></div>