---
title: >-
    شمارهٔ ۶۱۱
---
# شمارهٔ ۶۱۱

<div class="b" id="bn1"><div class="m1"><p>سرکنم آوارگی منزل همان گیرم نبود</p></div>
<div class="m2"><p>دل به دریا افکنم ساحل همان گیرم نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سینه ای بر تیر باران تغافل می دهم</p></div>
<div class="m2"><p>التفات خنجر قاتل همان گیرم نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه دیدم عمری از آیینه دل روی او</p></div>
<div class="m2"><p>یک نفس از حال من غافل همان گیرم نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترک دل کردم چه پروا دارم از تاراج عمر</p></div>
<div class="m2"><p>برقها مطلب روا حاصل همان گیرم نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل گره می زد به کارم ترک او کردم اسیر</p></div>
<div class="m2"><p>برخود آسان کردم این مشکل همان گیرم نبود</p></div></div>