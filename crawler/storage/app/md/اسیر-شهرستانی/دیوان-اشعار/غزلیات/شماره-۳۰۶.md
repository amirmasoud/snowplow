---
title: >-
    شمارهٔ ۳۰۶
---
# شمارهٔ ۳۰۶

<div class="b" id="bn1"><div class="m1"><p>هر سبزه این باغ نشان خم دامی است</p></div>
<div class="m2"><p>هر سرو و گلی نغمه سرای جم و جامی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آمیخته با چشم ترم شور تماشا</p></div>
<div class="m2"><p>هر قطره سرشکم نمک فتنه عامی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وارستگی ما وگرفتاری عالم</p></div>
<div class="m2"><p>هر وحشی رم کرده نظر کرده دامی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شبنم به شرر حوصله داغ فروشد</p></div>
<div class="m2"><p>در گلشن و گلخن ز تمنای تو دامی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آتشکده سینه نظر یافته کیست</p></div>
<div class="m2"><p>در کلبه ام از شور نفس شرب مدامی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>احوال غریبان ز فراموشی خود پرس</p></div>
<div class="m2"><p>هر ناله که از خاطر ما رفته پیامی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیگانگیم سوخت ز بیداد تو فریاد</p></div>
<div class="m2"><p>مستانه جوابی که غریبانه سلامی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شبگرد تو کارش نه به شمع است و نه با دل</p></div>
<div class="m2"><p>نقش قدم گرمروان ماه تمامی است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیتابیش افکنده به آسایش جاوید</p></div>
<div class="m2"><p>یکبار نظر کن که اسیر تو چه خامی است</p></div></div>