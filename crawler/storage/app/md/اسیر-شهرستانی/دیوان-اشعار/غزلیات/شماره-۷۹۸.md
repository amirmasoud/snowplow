---
title: >-
    شمارهٔ ۷۹۸
---
# شمارهٔ ۷۹۸

<div class="b" id="bn1"><div class="m1"><p>آشفته دلی دارم چون ساغر بد مستان</p></div>
<div class="m2"><p>بی صرفه منه زاهد سر در سر بد مستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رندانه گذر کردم بر غفلت و آگاهی</p></div>
<div class="m2"><p>آن نسخه هشیاری این دفتر بد مستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زاهد به خدا بگذر از کرده ما بگذر</p></div>
<div class="m2"><p>پرواز جنون دارد بال و پر بد مستان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن بار ندامت را این بار ملامت را</p></div>
<div class="m2"><p>ناصح سگ هشیاران زاهد خر بد مستان</p></div></div>