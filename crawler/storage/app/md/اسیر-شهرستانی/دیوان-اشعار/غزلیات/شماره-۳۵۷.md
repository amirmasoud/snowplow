---
title: >-
    شمارهٔ ۳۵۷
---
# شمارهٔ ۳۵۷

<div class="b" id="bn1"><div class="m1"><p>به دور چشم توام التجا به دوران نیست</p></div>
<div class="m2"><p>نگاه گرم کم از خاتم سلیمان نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فریب خورده دام نگاه می داند</p></div>
<div class="m2"><p>که زیر تیغ تغافل نشستن آسان نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز گرد راه تو روشن کنم سواد نظر</p></div>
<div class="m2"><p>غبار بینشم از سرمه صفاهان نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا که یاد رخ دوست گلشن نظر است</p></div>
<div class="m2"><p>هجوم گریه کم از جوش عندلیبان نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جنون پرستم و آشفتگی بهار من است</p></div>
<div class="m2"><p>دماغم از گل باغ کسی پریشان نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندیده روی خریدار گفتگوی اسیر</p></div>
<div class="m2"><p>در آن دیار که خیل کسادی ارزان نیست</p></div></div>