---
title: >-
    شمارهٔ ۲۷۳
---
# شمارهٔ ۲۷۳

<div class="b" id="bn1"><div class="m1"><p>دیوانه گر به دل غم دنیا شمرده است</p></div>
<div class="m2"><p>از موج کیسه زر دنیا شمرده است؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرد حسابی از دل ما می توان گرفت</p></div>
<div class="m2"><p>بی دخل و خرج ترک تمنا شمرده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ننگ حساب دفتر دانش چرا کشد</p></div>
<div class="m2"><p>دیوانه ای که تا پر عنقا شمرده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اشک از غبار خاطر من در کنار بحر</p></div>
<div class="m2"><p>ریگ روان به دامن صحرا شمرده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیوانه هرزه در پی رسوایی خود است</p></div>
<div class="m2"><p>دل از نصیحتم چه بدیها شمرده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شب انجم و صباح گل و لاله بهر تو</p></div>
<div class="m2"><p>هر چیز هر که داشته یکجا شمرده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دریا ز جوش گوهر رازت لبالب است</p></div>
<div class="m2"><p>هر جا که موج چون نفس ما شمرده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیوانه قلمرو سرگشتگی اسیر</p></div>
<div class="m2"><p>از سنگ ریزه عقد ثریا شمرده است</p></div></div>