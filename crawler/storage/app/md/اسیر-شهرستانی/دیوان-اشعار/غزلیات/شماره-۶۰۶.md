---
title: >-
    شمارهٔ ۶۰۶
---
# شمارهٔ ۶۰۶

<div class="b" id="bn1"><div class="m1"><p>امشب که خیال رخ او شمع نظر بود</p></div>
<div class="m2"><p>با دل نمک لعل لبش داغ جگر بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کلبه تاریک من از فیض محبت</p></div>
<div class="m2"><p>شمعی که شب هجر تو می سوخت سحر بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بستیم چو رخت سفر از کوی فراغت</p></div>
<div class="m2"><p>چیزی که فراموش شد اول غم سر بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد ترک وطن خضر ره وادی وصلش</p></div>
<div class="m2"><p>طوف حرم اول قدم شوق سفر بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دل بر او نامه به یک چشم زدن برد</p></div>
<div class="m2"><p>با مرغ نظر جرأت پرواز دگر بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در کاسه ز خشم دلم از سوز محبت</p></div>
<div class="m2"><p>آب دم شمشیر و نمک شیر و شکر بود؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرگز غم پرواز ندانست اسیرت</p></div>
<div class="m2"><p>چاک قفس مرغ دلش چاک جگر بود</p></div></div>