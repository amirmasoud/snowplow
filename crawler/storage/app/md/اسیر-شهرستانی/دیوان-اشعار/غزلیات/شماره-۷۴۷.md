---
title: >-
    شمارهٔ ۷۴۷
---
# شمارهٔ ۷۴۷

<div class="b" id="bn1"><div class="m1"><p>نمی گنجد به محشر فوج عصیانی که من دارم</p></div>
<div class="m2"><p>اجل شرمندگیها دارد از جانی که من دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگهدارد خدا از چشم بد از چشم خوبش هم</p></div>
<div class="m2"><p>کماندار آفتی برگشته مژگانی که من دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا درد تو می سازد به آیینی که می باید</p></div>
<div class="m2"><p>بسوزد روزگار از درد پنهانی که من دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمی پرسی، نمی خواهی، نمی جویی، نمی آیی</p></div>
<div class="m2"><p>کجا دانسته ای حال پریشانی که من دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمی دانم چه خواهد داد در محشر جواب من</p></div>
<div class="m2"><p>ستمگر اول و آخر پشیمانی که من دارم</p></div></div>