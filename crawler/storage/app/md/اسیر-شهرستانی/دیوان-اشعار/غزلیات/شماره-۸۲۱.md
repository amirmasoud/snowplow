---
title: >-
    شمارهٔ ۸۲۱
---
# شمارهٔ ۸۲۱

<div class="b" id="bn1"><div class="m1"><p>چون شعله آبروی نیاز است خون من</p></div>
<div class="m2"><p>چون بیخودی چکیده راز است خون من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از کشتن وجود و عدم صرفه می برند</p></div>
<div class="m2"><p>دشمن پرست دوست نواز است خون من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صیاد هرزه منت صیدم نمی کشد</p></div>
<div class="m2"><p>گلدسته بند چنگل باز است خون من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بند یک ترانه گرفتم که جان دهم</p></div>
<div class="m2"><p>مانند نغمه در رگ ساز است خون من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از سوختن چراغ دلم زنده شد اسیر</p></div>
<div class="m2"><p>چون خس بهار سوز و گداز است خون من</p></div></div>