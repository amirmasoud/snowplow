---
title: >-
    شمارهٔ ۶۷۷
---
# شمارهٔ ۶۷۷

<div class="b" id="bn1"><div class="m1"><p>ز فیض ابر شد زانسان جهان سبز</p></div>
<div class="m2"><p>که از عکس زمین شد آسمان سبز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به صحرا گردباد تشنه لب را</p></div>
<div class="m2"><p>رطوبت کرد چون سرو روان سبز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رقم زد خامه ای گر وصف گلشن</p></div>
<div class="m2"><p>چو برگ بید گردیدش زبان سبز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز فیض لطف عام ابر نیسان</p></div>
<div class="m2"><p>چو موج سبزه شد ریگ روان سبز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اسیر عشق را چون جوهر تیغ</p></div>
<div class="m2"><p>شود در سینه غم‌های نهان سبز</p></div></div>