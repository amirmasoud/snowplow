---
title: >-
    شمارهٔ ۵۲۱
---
# شمارهٔ ۵۲۱

<div class="b" id="bn1"><div class="m1"><p>بسکه دارم به دل محبت درد</p></div>
<div class="m2"><p>درد جویم ز یار و طاقت درد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاد از آنم که آشنا شده است</p></div>
<div class="m2"><p>با لب زخم من شکایت درد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می ربایند مو به مو از هم</p></div>
<div class="m2"><p>عضو عضو مرا ز لذت درد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما کجا تلخی دوا ز کجا</p></div>
<div class="m2"><p>می گریزیم در حمایت درد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناله می روید از نی تیرش</p></div>
<div class="m2"><p>در دل ما به ذوق عشرت درد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>استخوانم به خویش می بالد</p></div>
<div class="m2"><p>هر نفس زیر بار منت درد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بند بندم طلسم شور نی است</p></div>
<div class="m2"><p>تا نمکسود شد ز لذت درد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زده عشقت صلای مهمانی</p></div>
<div class="m2"><p>داغ ما را به خوان قسمت درد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می کنم جان فدای گرمی عشق</p></div>
<div class="m2"><p>دل اسیر وفای راحت درد</p></div></div>