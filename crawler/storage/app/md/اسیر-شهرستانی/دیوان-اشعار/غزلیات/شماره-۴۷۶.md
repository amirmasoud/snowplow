---
title: >-
    شمارهٔ ۴۷۶
---
# شمارهٔ ۴۷۶

<div class="b" id="bn1"><div class="m1"><p>ز استخوان ساخته صنعتگر قدرت درمی</p></div>
<div class="m2"><p>که وطن در دلش آن گوهر تابان دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با وجودی که ندید است کس از درج او را</p></div>
<div class="m2"><p>مشتری بیحد و جوینده فراوان دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکه دیدش به هوا تا ننماید او را</p></div>
<div class="m2"><p>از شعف در دهنش گیرد و پنهان دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شبچراغی است که شمع همه کس روشن از اوست</p></div>
<div class="m2"><p>با گدا رابطه و قرب به شاهان دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیستش منتی از تربیت مهر و سحاب</p></div>
<div class="m2"><p>زر خورشید توقع نه ز باران دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خون و شیر است که جوشیده و او گشته عیان</p></div>
<div class="m2"><p>نسبتی لیک نه با این و نه با آن دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه عقیق است و نه الماس و نه یاقوت و نه لعل</p></div>
<div class="m2"><p>آشنایی نه به سیلانی و مرجان دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه زمرد نه زبرجد نه خزف نه لؤلؤ</p></div>
<div class="m2"><p>نه ز دریا خبر و نه اثر از کان دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ضعف پیری اگرش مایده روح کند</p></div>
<div class="m2"><p>همچو گل خنده رنگین به جوانان دارد</p></div></div>