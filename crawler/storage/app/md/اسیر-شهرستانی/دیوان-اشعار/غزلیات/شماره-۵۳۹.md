---
title: >-
    شمارهٔ ۵۳۹
---
# شمارهٔ ۵۳۹

<div class="b" id="bn1"><div class="m1"><p>سررشته جنون ره اهل هوس نزد</p></div>
<div class="m2"><p>بند گران به پای مگس هیچ کس نزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روشندلی ز پرتو افتادگی بود</p></div>
<div class="m2"><p>بیهوده شعله دست به دامان خس نزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون شد درون سینه دل و شکوه سر نکرد</p></div>
<div class="m2"><p>بحری است اینکه غیر خموشی نفس نزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آیینه در غبار کدورت نشست اسیر</p></div>
<div class="m2"><p>روشندل آنکه تکیه به این یک نفس نزد</p></div></div>