---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>مو به مو مژگان تر باید شکار عشق را</p></div>
<div class="m2"><p>گریه بسیار است ابر نوبهار عشق را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از نیاز ما رخت باغ تماشا گشته است</p></div>
<div class="m2"><p>گل بخندد از گریبان خارخار عشق را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل به امیدی غبار راه حسرت گشته است</p></div>
<div class="m2"><p>وعده می سوزد چراغ انتظار عشق را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نسیم جلوه ای پرواز رنگین می کنم</p></div>
<div class="m2"><p>نکهت گل می برد از جا غبار عشق را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون اسیر آیینه ام از تیره بختی روشن است</p></div>
<div class="m2"><p>صبح ما شام غریبان شد دیار عشق را</p></div></div>