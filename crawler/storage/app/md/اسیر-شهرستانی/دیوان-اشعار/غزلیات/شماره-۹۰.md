---
title: >-
    شمارهٔ ۹۰
---
# شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>درد عشق آشیانه دل ما</p></div>
<div class="m2"><p>راز مجنون فسانه دل ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نفسی از تو کی شود غافل</p></div>
<div class="m2"><p>بیخودیها بهانه دل ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رنگ از روی آه می دزدد</p></div>
<div class="m2"><p>گریه بیخودانه دل ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناله شوخ ما چرا نشود</p></div>
<div class="m2"><p>بلبل آشیانه دل ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آه تعمیر جلوه اشکی</p></div>
<div class="m2"><p>پرخراب است خانه دل ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چاکهای جگر به گل خندید</p></div>
<div class="m2"><p>بلبلی شد ترانه دل ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گردش چشم مست را نازیم</p></div>
<div class="m2"><p>یاد او شیره خانه دل ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سجده شکر می کنیم اسیر</p></div>
<div class="m2"><p>دل ما آستانه دل ما</p></div></div>