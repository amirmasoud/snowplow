---
title: >-
    شمارهٔ ۸۲۹
---
# شمارهٔ ۸۲۹

<div class="b" id="bn1"><div class="m1"><p>سرو رعنا غبار جلوه او</p></div>
<div class="m2"><p>گل خودرو شکار جلوه او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خرمن اشک و خوشه آه است</p></div>
<div class="m2"><p>حاصل انتظار جلوه او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل طاقت رمیده می رقصد</p></div>
<div class="m2"><p>چون شرر در غبار جلوه او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون زنم لاف حسرت جاوید</p></div>
<div class="m2"><p>گر کنم جان نثار جلوه او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نبودی اسیر جسم تو خاک</p></div>
<div class="m2"><p>چون گذشتی مدار جلوه او</p></div></div>