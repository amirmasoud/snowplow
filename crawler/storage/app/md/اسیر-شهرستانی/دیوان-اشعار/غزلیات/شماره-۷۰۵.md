---
title: >-
    شمارهٔ ۷۰۵
---
# شمارهٔ ۷۰۵

<div class="b" id="bn1"><div class="m1"><p>همین دانسته چشمش جای می خون خوردن عاشق</p></div>
<div class="m2"><p>نمی داند نگاهش دیدن از نا دیدن عاشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فلاطون را به جوش آورد در خم همچو جوش می</p></div>
<div class="m2"><p>چه شد طفل است (و) خوابش می برد در دامن عاشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو عالم گر رود بر باد ایمایی نمی داند</p></div>
<div class="m2"><p>چه پروا می کند از زیستن یا مردن عاشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر بویی ز یاری دارد آن بیگانه می گیرد</p></div>
<div class="m2"><p>ز چاک سینه صحرا سراغ گلشن عاشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به مکتب راه می گیرد ز شوخی باز می دارد</p></div>
<div class="m2"><p>نمی باشند بیجا خردسالان دشمن عاشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خزان رنگ زرد اوست یاران حاصل عمرم</p></div>
<div class="m2"><p>تپیدنهای دل در کشت عاشق خرمن عاشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز آغوش سحر محشر گریبان چاک برخیزد</p></div>
<div class="m2"><p>حمایل گر کند دستی شبی در گردن عاشق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پدر نامحرم است ای غنچه نورسته نامحرم</p></div>
<div class="m2"><p>به طفلی گر نداری دست را در گردن عاشق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اسیر آیینه ای داری خموشی پیشه خود کن</p></div>
<div class="m2"><p>نمی گردد بغیر از راز عاشق رهزن عاشق</p></div></div>