---
title: >-
    شمارهٔ ۲۳۱
---
# شمارهٔ ۲۳۱

<div class="b" id="bn1"><div class="m1"><p>چشم بدخو چون هجوم آورد طاقت خوشنماست</p></div>
<div class="m2"><p>چون غضب شمشیرکین بندد مروت خوشنماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیر خود بار اطاعت برده بر دوش غرور</p></div>
<div class="m2"><p>از جوانان خجالت پیشه طاعت خوشنماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رستمی در گفتگو با خصم عاجز کیش نیست</p></div>
<div class="m2"><p>دست داری حرف عجز آمیز جرأت خوشنماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون شود بیدار کارش بیش می آید ز دست</p></div>
<div class="m2"><p>جوهر شمشیر را خواب فراغت خوشنماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صبح چون شد هرکسی و جرأت بازوی خویش</p></div>
<div class="m2"><p>شام هیجا خصم را با خصم الفت خوشنماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبح صادق خضر این عصر است و موسی آفتاب</p></div>
<div class="m2"><p>در صف روشندلان عهد اخوت خوشنماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوستانی را که با هم سینه صافی کرده اند</p></div>
<div class="m2"><p>در میان جنگ ایمای محبت خوشنماست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از تغافل پیشه ای کی شکوه می ورزد اسیر</p></div>
<div class="m2"><p>هر چه می آید از آن خصم مروت خوشنماست</p></div></div>