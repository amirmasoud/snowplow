---
title: >-
    شمارهٔ ۴۸۱
---
# شمارهٔ ۴۸۱

<div class="b" id="bn1"><div class="m1"><p>نزاکت اینقدر نی برگ گل نی یاسمن دارد</p></div>
<div class="m2"><p>به هر عضو تو خوبی یوسفی در پیرهن دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز تاراج غمش راهی به دلها کرده ام پیدا</p></div>
<div class="m2"><p>نگاهش سرگران از هر که گردد رو به من دارد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چمن پیرا به رنگی امتحانم می کند هر دم</p></div>
<div class="m2"><p>بهار تازه ای در سایه هر نسترن دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به بزمش بلبل و پروانه بسیارند می دانم</p></div>
<div class="m2"><p>زبان آتشین دارم که تاب یک سخن دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گهی از نکهت گل گه به بوی پیرهن شادم</p></div>
<div class="m2"><p>دلم را مهربانی های غربت در وطن دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل نظم اسیر از آفت پژمردن آزاد است</p></div>
<div class="m2"><p>که رنگ و بوی فیض از تربت پاک حسن دارد</p></div></div>