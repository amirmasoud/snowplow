---
title: >-
    شمارهٔ ۲۴۴
---
# شمارهٔ ۲۴۴

<div class="b" id="bn1"><div class="m1"><p>کی در صفا چو تیغ تواش سینه روشن است</p></div>
<div class="m2"><p>بی جوهری ز چهره آیینه روشن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از فیض آب گوهر پیکان تیر او</p></div>
<div class="m2"><p>در بحر عشق چون صدفم سینه روشن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غم نیست گر به روی تو گاهی کند نگاه</p></div>
<div class="m2"><p>چون آفتاب کوری آیینه روشن ا ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاهد خیال شیشه می کرد وکور شد</p></div>
<div class="m2"><p>آه که مرا که در شب آدینه روشن است؟</p></div></div>