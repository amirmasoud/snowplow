---
title: >-
    شمارهٔ ۱۰۵
---
# شمارهٔ ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>گردون ز بسکه برد غمت در دیارها</p></div>
<div class="m2"><p>از روز من گرفت سبق روزگارها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمر ابد به خاک درت جان سپرد و گفت</p></div>
<div class="m2"><p>مشت غبار رهگذر انتظارها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کردم زیاد روی تو روشن چراغ فال</p></div>
<div class="m2"><p>از پرتو دلم شررستان غبارها؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جوش بهار بگسلد از هم که از رخت</p></div>
<div class="m2"><p>گل می زنیم بر سر لیل و نهارها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر تربتم فشانی اگر آب زندگی</p></div>
<div class="m2"><p>خیزد به جای گرد زخاکم شرارها</p></div></div>