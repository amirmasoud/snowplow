---
title: >-
    شمارهٔ ۶۱۳
---
# شمارهٔ ۶۱۳

<div class="b" id="bn1"><div class="m1"><p>دیدیم زهر چشم تو عمر دوباره بود</p></div>
<div class="m2"><p>معلوم ما نشد که تبسم چکاره بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر غم که آمد از دل ما بینوا برفت</p></div>
<div class="m2"><p>خوان خلیل ما جگر پاره پاره بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش از جنون در آتش دل می گداختیم</p></div>
<div class="m2"><p>شغل غم تو پیشرو استخاره بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پستی ندید مرتبه عشق من اسیر</p></div>
<div class="m2"><p>داغم همیشه آینه دار ستاره بود</p></div></div>