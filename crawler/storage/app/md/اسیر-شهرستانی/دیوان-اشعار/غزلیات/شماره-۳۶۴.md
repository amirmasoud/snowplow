---
title: >-
    شمارهٔ ۳۶۴
---
# شمارهٔ ۳۶۴

<div class="b" id="bn1"><div class="m1"><p>رواج ساختگی های روزگار نداشت</p></div>
<div class="m2"><p>زر شکسته دل بیش از این عیار نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غبار سوخته ما به لاله زار گریخت</p></div>
<div class="m2"><p>تحمل نفس سرد روزگار نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وجود عرض سپه دید در مصاف عدم</p></div>
<div class="m2"><p>به سرگذشتگی شوق یک سوار نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آن زمانه مدار و معاشم از دل بود</p></div>
<div class="m2"><p>که روزگار معاش و فلک مدار نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گداز ساختگی های روزگارم سوخت</p></div>
<div class="m2"><p>ز روی گرم و خنک جلوه شرار نداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم به ملک وجود آبروی مشرب ریخت</p></div>
<div class="m2"><p>یک آشنای موافق در این دیار نداشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گل همیشه بهار است آمد اقبال</p></div>
<div class="m2"><p>ز صد نگار یکی حسن روزگار نداشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خزان ساختگی پرچمن فروشی کرد</p></div>
<div class="m2"><p>به دلخراشی مهر فسرده خار نداشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زمانه دفتر ایام را اگر می دید</p></div>
<div class="m2"><p>گرفته گوشه تر از فرد افتخار نداشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بهار خانه به دوشی چه خنده ها که نکرد</p></div>
<div class="m2"><p>به شوخ چشمی مجنون گلی به بار نداشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خرابی از گل صدبرگ باج می گیرد</p></div>
<div class="m2"><p>هوای گوشه ویرانه را بهار نداشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به صبر بیکس مطلب شکار خنده چرا</p></div>
<div class="m2"><p>کسی چو شکر خداوند کردگار نداشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مپرس باعث کام دل اسیر مپرس</p></div>
<div class="m2"><p>نداشت روی توقع نکرده کار نداشت</p></div></div>