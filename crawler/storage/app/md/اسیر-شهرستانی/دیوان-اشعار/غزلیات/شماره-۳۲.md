---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>خواب، پرواز حرام است مرا</p></div>
<div class="m2"><p>آشیان حلقه دام است مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاد زلفت گل شب بیداری</p></div>
<div class="m2"><p>فیض صبح اول شام است مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمر سودایی زلف تو دراز</p></div>
<div class="m2"><p>تا ابد کار به کام است مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرآن جلوه سلامت باشد</p></div>
<div class="m2"><p>هر نفس عیش مدام است مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اضطراب و لب خاموش و ادب</p></div>
<div class="m2"><p>قاصد و نامه و نام است مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی خزان باغ دل از بیدردی</p></div>
<div class="m2"><p>سوختن میوه خام است مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل زهر چاک هلالی دارد</p></div>
<div class="m2"><p>سر به سر ماه تمام است مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شهد منت ز تکبر نوشم</p></div>
<div class="m2"><p>از جوابش که سلام است مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نو خطان پیش شما غیر اسیر</p></div>
<div class="m2"><p>نه بگویید چه نام است مرا</p></div></div>