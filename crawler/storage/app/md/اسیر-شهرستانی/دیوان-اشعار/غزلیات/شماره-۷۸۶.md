---
title: >-
    شمارهٔ ۷۸۶
---
# شمارهٔ ۷۸۶

<div class="b" id="bn1"><div class="m1"><p>سیماب توشه سفر خواب کرده ایم</p></div>
<div class="m2"><p>این رسم تازه ای است که ما باب کرده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صید اثر هلاک خدنگ دعای ماست</p></div>
<div class="m2"><p>یا رب کمان کیست که محراب کرده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبها به یاد روی تو سیماب اشک را</p></div>
<div class="m2"><p>از گریه شبنم گل مهتاب کرده ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دور از تو غیر طعنه آرام می زند</p></div>
<div class="m2"><p>غافل که ما چه با دل بیتاب کرده ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما صید امتحان جفاییم چون اسیر</p></div>
<div class="m2"><p>شمشیر را ز شرم شکست آب کرده ایم</p></div></div>