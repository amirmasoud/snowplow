---
title: >-
    شمارهٔ ۶۳۲
---
# شمارهٔ ۶۳۲

<div class="b" id="bn1"><div class="m1"><p>از آتش ما گر نفسی دود برآید</p></div>
<div class="m2"><p>از شرم نگاهش عرق آلود برآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لبریز چه شور است دگر انجمن ما</p></div>
<div class="m2"><p>کز هر جگری ناله نمکسود برآید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از غیرت آهم گل عشقی نتوان چید</p></div>
<div class="m2"><p>آتش بود آن لاله کز او دود برآید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گلچین فریبش نشود زخم دل ما</p></div>
<div class="m2"><p>الماس گر از مرهم بهبود برآید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جایی که نگاهت سخن از حوصله پرسد</p></div>
<div class="m2"><p>دل گر همه بود است که نابود برآید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از غم چه گریزی که ز همراهی عشقت</p></div>
<div class="m2"><p>گر بود دل از تربت محمود برآید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جایی که اسیر تو کند نغمه سرایی</p></div>
<div class="m2"><p>دود از جگر زمزمه عود برآید</p></div></div>