---
title: >-
    شمارهٔ ۷۲۵
---
# شمارهٔ ۷۲۵

<div class="b" id="bn1"><div class="m1"><p>لاله داغم ز گلزار جگر جوشیده‌ام</p></div>
<div class="m2"><p>آتشین موجم ز بحر چشم تر جوشیده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قطره باران نیسانم که در دریای عشق</p></div>
<div class="m2"><p>گاه با طوفان و گاهی با خطر جوشیده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر رگ جان خورده‌ام زین کینه‌جویان نیشتر</p></div>
<div class="m2"><p>همچو خون خود به هرکس بیشتر جوشیده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نخل امیدم ندارد حاصلی جز سوختن</p></div>
<div class="m2"><p>از نهال شعله مانند شرر جوشیده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی شناسد این خزف بی‌مایگان قدر اسیر</p></div>
<div class="m2"><p>گوهرم در سینه بحر هنر جوشیده‌ام</p></div></div>