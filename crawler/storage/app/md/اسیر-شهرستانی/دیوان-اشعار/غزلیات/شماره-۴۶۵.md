---
title: >-
    شمارهٔ ۴۶۵
---
# شمارهٔ ۴۶۵

<div class="b" id="bn1"><div class="m1"><p>صدف گوهر چمن گل ابر باران دل وفا دارد</p></div>
<div class="m2"><p>ندارد هرکه امیدی به کس چون من تو را دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به نومیدی چها دل بسته بودم خوش گلی وا شد</p></div>
<div class="m2"><p>چه دانستم که مشکل در بغل مشکل گشا دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندیدم خواب مطلب بسکه از تعبیر ترسیدم</p></div>
<div class="m2"><p>خوشا حال دل هرکس دماغ مدعا دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نسیمی کز شمیمت پر زند دود از چمن خیزد</p></div>
<div class="m2"><p>دلش خوش باغبان باغ خوش آب و هوا دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نسوزد گر حجاب روی او در پرده دلها را</p></div>
<div class="m2"><p>شرار سنگ هم پروانه از موج هوا دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به موری خرمنی از حاصل دل می توان دادن</p></div>
<div class="m2"><p>به قدر انتظار این دانه گر نشو و نما دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اسیر از گردش چشم کسی مخمور کی ماند</p></div>
<div class="m2"><p>اگر بیگانگی دارد نگاه آشنا دارد</p></div></div>