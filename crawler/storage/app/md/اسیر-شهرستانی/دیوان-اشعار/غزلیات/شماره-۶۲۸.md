---
title: >-
    شمارهٔ ۶۲۸
---
# شمارهٔ ۶۲۸

<div class="b" id="bn1"><div class="m1"><p>عاشق از شوق نگاه واپسین جان می دهد</p></div>
<div class="m2"><p>چشم و دل را حسرت جاوید تاوان می دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اشک دریا دل کجا و ساحل دامان کجا</p></div>
<div class="m2"><p>گریه ایم عرض تجمل در بیابان می دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اضطراب دل نشان جلوه مژگان کیست</p></div>
<div class="m2"><p>مو به مویم وعده زخم نمایان می دهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی نیازی را زدست انداز آفت باک نیست</p></div>
<div class="m2"><p>شبنم این باغ طوفان را به طوفان می دهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک شکار از منت صیاد ما آزاد نیست</p></div>
<div class="m2"><p>خاک راهش سر خط کبک خرامان می دهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مو به مو آگاهم از راز دل زلفش اسیر</p></div>
<div class="m2"><p>نامه ام را قاصد خواب پریشان می دهد</p></div></div>