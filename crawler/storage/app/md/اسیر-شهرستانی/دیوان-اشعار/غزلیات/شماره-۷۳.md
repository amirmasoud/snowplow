---
title: >-
    شمارهٔ ۷۳
---
# شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>شد شیشه خانه باغ دل از جان سخت ما</p></div>
<div class="m2"><p>جز سنگ فتنه یار نیارد درخت ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیگانه الفتیم چه دنیا چه آخرت</p></div>
<div class="m2"><p>در خانه وجود و عدم نیست رخت ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ابر بهار گریه مستانه خودیم</p></div>
<div class="m2"><p>گلهای باغ ما جگر لخت لخت ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زیر نگین ماست دو عالم گذشتگی</p></div>
<div class="m2"><p>بیزاری کلاه و نمد تاج و تخت ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوهر چکد ز شبنم گلزار فقر اسیر</p></div>
<div class="m2"><p>ابر بهار چون نشود پوست تخت ما</p></div></div>