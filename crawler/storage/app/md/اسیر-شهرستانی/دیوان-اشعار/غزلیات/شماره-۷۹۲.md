---
title: >-
    شمارهٔ ۷۹۲
---
# شمارهٔ ۷۹۲

<div class="b" id="bn1"><div class="m1"><p>ماییم و یاد دوست غنیمت کجا بریم</p></div>
<div class="m2"><p>عالم تمام اوست شکایت کجا بریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عین رضا شده است دل خودشناس ما</p></div>
<div class="m2"><p>فکر زیاده جویی قسمت کجا بریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>محو توایم آینه دیگر چکاره است</p></div>
<div class="m2"><p>خوار توایم دولت و عزت کجا بریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای سر بسر رضای دل ما رضای تو</p></div>
<div class="m2"><p>اندیشه جفا و فراغت کجا بریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ماییم و بیزبانی مطلب تمام کن</p></div>
<div class="m2"><p>دل هم زبان شده است عبارت کجا بریم</p></div></div>