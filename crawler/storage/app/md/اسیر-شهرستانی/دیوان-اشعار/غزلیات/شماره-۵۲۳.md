---
title: >-
    شمارهٔ ۵۲۳
---
# شمارهٔ ۵۲۳

<div class="b" id="bn1"><div class="m1"><p>نیست درویش آنکه از تاراج عزت بگذرد</p></div>
<div class="m2"><p>گر گذشتی دارد از ملک قناعت بگذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زندگی بی عشق یعنی دانه ای در زیر خاک</p></div>
<div class="m2"><p>حیف از اوقاتی که بی شغل محبت بگذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بینوایی ذوق بخشیدن نمی داند که چیست</p></div>
<div class="m2"><p>آنکه دارد بیشتر عمرش به خجلت بگذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرنیان صبحدم یک چاک پیراهن شود</p></div>
<div class="m2"><p>گر ز گلگشت چمن با این نزاکت بگذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از فریب گردش چشم فسونسازش به بزم</p></div>
<div class="m2"><p>ساغر از می ناله از نی دل ز الفت بگذرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سبزه ای کز گریه ام روید پرقمری شود</p></div>
<div class="m2"><p>هرگه از باغ نظر آن سرو قامت بگذرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خودنمایی نیست چون امروز فردا گر به من</p></div>
<div class="m2"><p>سر ز محشر بر ندارم تا قیامت بگذرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از خیال محشر انصاف می گردد غبار</p></div>
<div class="m2"><p>حضم اگر مرد است از یادش مروت بگذرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در چمن دارم خیال می پرستی با اسیر</p></div>
<div class="m2"><p>پاره اوقات مجنون هم به عشرت بگذرد</p></div></div>