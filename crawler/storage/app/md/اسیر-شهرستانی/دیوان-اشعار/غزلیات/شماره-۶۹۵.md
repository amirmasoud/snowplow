---
title: >-
    شمارهٔ ۶۹۵
---
# شمارهٔ ۶۹۵

<div class="b" id="bn1"><div class="m1"><p>ای کرده ز یاد ما فراموش</p></div>
<div class="m2"><p>حیف است مکن وفا فراموش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با حرف تو حرفها اراجیف</p></div>
<div class="m2"><p>با یاد تو یادها فراموش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکبار به سوی خود نگاهی</p></div>
<div class="m2"><p>پرکرده ای از خدا فراموش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شرمندگی از وفا نداریم</p></div>
<div class="m2"><p>داریم دلی جفا فراموش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای کرده به رغم دوستداری</p></div>
<div class="m2"><p>یکباره ز حال ما فراموش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کردیم برای خاطر تو</p></div>
<div class="m2"><p>بیگانه و آشنا فراموش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در میکده ها اسیر سرمست</p></div>
<div class="m2"><p>ما را مکن از دعا فراموش</p></div></div>