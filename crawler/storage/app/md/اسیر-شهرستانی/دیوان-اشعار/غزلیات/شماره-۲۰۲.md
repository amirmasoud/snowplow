---
title: >-
    شمارهٔ ۲۰۲
---
# شمارهٔ ۲۰۲

<div class="b" id="bn1"><div class="m1"><p>به گلشنی دلم از دست باغبان تنگ است</p></div>
<div class="m2"><p>که جای نکهت گل هم به گلستان تنگ است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخن به لعل که شد آشنا نمی دانم</p></div>
<div class="m2"><p>که دستگاه سخن بر سخنوران تنگ است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگشته است کسی از فغان من دلتنگ</p></div>
<div class="m2"><p>ز میزبانی صبرم دل فغان تنگ است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل شکفته چه جویم که غربتم وطن است</p></div>
<div class="m2"><p>چگونه بال گشایم که آسمان تنگ است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز دست تیغ تو بس کار بر جهان شد تنگ</p></div>
<div class="m2"><p>لباس زخم بر اندام کشتگان تنگ است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بسکه پر شده از کینه ام دل عالم</p></div>
<div class="m2"><p>ز التفات بتان خلق عاشقان تنگ است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنین که پر شده از آه من زمانه اسیر</p></div>
<div class="m2"><p>خیال عکس در آیینه گمان تنگ است</p></div></div>