---
title: >-
    شمارهٔ ۷۷۸
---
# شمارهٔ ۷۷۸

<div class="b" id="bn1"><div class="m1"><p>یاد چشمی را به افسون رام الفت می‌کنم</p></div>
<div class="m2"><p>می‌نشینم گوشه‌ای تنها فراغت می‌کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ننگ سربازی است امید ترحم داشتن</p></div>
<div class="m2"><p>جان فدای جور یار بی‌مروت می‌کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاطر من مفلس و گنج روان عشق نیست</p></div>
<div class="m2"><p>تکیه بر جمعیت گرد کدورت می‌کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محشر صد زخم ناسور است چاک سینه‌ام</p></div>
<div class="m2"><p>خواب خوش در بستر شور قیامت می‌کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تغافل صد رهم گر خون بریزی چون اسیر</p></div>
<div class="m2"><p>از دم تیغ تو احیای شهادت می‌کنم</p></div></div>