---
title: >-
    شمارهٔ ۴۱۸
---
# شمارهٔ ۴۱۸

<div class="b" id="bn1"><div class="m1"><p>بی یاد قامتش دل بیتاب من مباد</p></div>
<div class="m2"><p>چون سرو خوشخرام نباشد چمن مباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معشوق دیگران گل بر باد رفته است</p></div>
<div class="m2"><p>شوخ است نغمه گوشزد کوهکن مباد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وقت اجابت است و دل شب دعا کنم</p></div>
<div class="m2"><p>جز خار گلستان تو در پیرهن مباد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اشکم رساست از ته دل می کنم دعا</p></div>
<div class="m2"><p>در خلوت وصال تو راه سخن مباد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آتش فروز دل نشود گر خیال او</p></div>
<div class="m2"><p>یک برگ شعله در چمن سوختن مباد</p></div></div>