---
title: >-
    شمارهٔ ۵۶۱
---
# شمارهٔ ۵۶۱

<div class="b" id="bn1"><div class="m1"><p>بسکه دامان حجاب از الفت من می کشد</p></div>
<div class="m2"><p>گر شود گلشن ز خونم رنگ دامن می کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نا امیدی حاصل کشت امید ما بس است</p></div>
<div class="m2"><p>زود کار دانه عاشق به خرمن می کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زشت را خجلت گذاری بهتر از آیینه نیست</p></div>
<div class="m2"><p>سینه صافی انتقام ما ز دشمن می کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پاس رازت لازم است از بزم بیرون می روم</p></div>
<div class="m2"><p>مستم و پایان خاموشی به گفتن می کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه از رازش دل یک قطره بی آشوب نیست</p></div>
<div class="m2"><p>محرم او همچو موج از خویش دامن می کشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوی حسن از عشق می داند گناه خود اسیر</p></div>
<div class="m2"><p>انتقام فتنه بیباکی از من می کشد</p></div></div>