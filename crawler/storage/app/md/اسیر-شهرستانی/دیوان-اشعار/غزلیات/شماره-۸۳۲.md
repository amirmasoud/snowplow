---
title: >-
    شمارهٔ ۸۳۲
---
# شمارهٔ ۸۳۲

<div class="b" id="bn1"><div class="m1"><p>خونم به جوش آمده تیغ نگاه کو</p></div>
<div class="m2"><p>مو تیر می کشد به تنم صیدگاه کو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل وصف عیش زخم خدنگت به سینه داشت</p></div>
<div class="m2"><p>هرمو جدا به ناله درآمد که آه کو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نزدیک شد که وحشی آوارگی شوم</p></div>
<div class="m2"><p>تدبیرهای چشم تغافل پناه کو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورشید عیش از افق مدعا دمید</p></div>
<div class="m2"><p>فرقی دگر میان سفید و سیاه کو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شرمنده داردم ز گنه ترک می اسیر</p></div>
<div class="m2"><p>آن گریه های نیمشب عذرخواه کو</p></div></div>