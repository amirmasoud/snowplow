---
title: >-
    شمارهٔ ۳۲۳
---
# شمارهٔ ۳۲۳

<div class="b" id="bn1"><div class="m1"><p>کرده خونم را صف مژگان چراغان زیر پوست</p></div>
<div class="m2"><p>موج نشتر می زند نبض شهیدان زیر پوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیش از این با حسرت سرشار بازی چون کنم</p></div>
<div class="m2"><p>بند بندم تا به کی رقصد چو طفلان زیر پوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می توان از پوست پوشی ملک دارایی گرفت</p></div>
<div class="m2"><p>داد شهرت می زند طبل سلیمان زیر پوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رشک گو خونم بریز و شوق گو نامم مبر</p></div>
<div class="m2"><p>عضو عضوم می تپد از دل چه پنهان زیر پوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیچ و تابم بیش از این شبهای بیتابی مپرس</p></div>
<div class="m2"><p>گشته مغز استخوانم سنبلستان زیر پوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وصف رویت می کنم چون غنچه رسوا زیر لب</p></div>
<div class="m2"><p>بوی زلفت می کنم چون نافه پنهان زیر پوست</p></div></div>