---
title: >-
    شمارهٔ ۶۱۸
---
# شمارهٔ ۶۱۸

<div class="b" id="bn1"><div class="m1"><p>گر آفتاب مهر تو از سینه می رود</p></div>
<div class="m2"><p>آب صفا ز چشمه آیینه می رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر خاک روزگار به باد فنا رود</p></div>
<div class="m2"><p>باور مکن که از دل او کینه می رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دارد سری به زهد چه شد مست مشربم</p></div>
<div class="m2"><p>گاهی به سیر مسجد آدینه می رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مطرب ترانه ای به اصولش نواخت آن</p></div>
<div class="m2"><p>صوفی به زیر خرقه پشمینه می رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بد خواه را به دشمنی خویش واگذار</p></div>
<div class="m2"><p>گردش به باد کینه دیرینه می رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در ملک تن دل است که منظور محنت است</p></div>
<div class="m2"><p>اول نگاه درد به گنجینه می رود</p></div></div>