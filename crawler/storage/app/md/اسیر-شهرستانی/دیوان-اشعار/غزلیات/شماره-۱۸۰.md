---
title: >-
    شمارهٔ ۱۸۰
---
# شمارهٔ ۱۸۰

<div class="b" id="bn1"><div class="m1"><p>بسکه سودای تماشای تو پنهان در سر است</p></div>
<div class="m2"><p>هر سر موی مرا پرواز مژگان در سر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دل آرام سیماب جنون پرداز من</p></div>
<div class="m2"><p>چشمه آیینه را آشوب توفان در سر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جذبه بی اختیارم تا کجا خواهد کشید</p></div>
<div class="m2"><p>پای در زنجیر و سودای بیابان در سر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در به روی شام من صبح از شفق وا می کند</p></div>
<div class="m2"><p>مشت خاکم را هوای ابر نیسان در سر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رنگ گل بال شکفتن می زند از خار من</p></div>
<div class="m2"><p>بخت آلوده را شوق گلستان در سر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>استخوانم را هما تعویذ عزت می کند</p></div>
<div class="m2"><p>نشئه محرومیم از خام یاران در سر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سایه خاری به خاکم نشکند طرف کلاه</p></div>
<div class="m2"><p>داغ بی اقبالیم زین کج کلاهان در سر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شعله رنگ سوختن می بارد از خاشاک من</p></div>
<div class="m2"><p>بر زمین افتاده او را نگهبان در سر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می توانم ساخت از لخت جگر عمری کباب</p></div>
<div class="m2"><p>چون اسیرم تا می گلرنگ احسان در سر است</p></div></div>