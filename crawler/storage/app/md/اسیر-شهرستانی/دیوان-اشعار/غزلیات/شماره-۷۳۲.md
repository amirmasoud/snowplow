---
title: >-
    شمارهٔ ۷۳۲
---
# شمارهٔ ۷۳۲

<div class="b" id="bn1"><div class="m1"><p>گر گمان وصل آن نامهربان می داشتم</p></div>
<div class="m2"><p>می سپردم جان و صد منت به جان می داشتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اینقدر تأثیر می بوده است عشق پاک را</p></div>
<div class="m2"><p>کافرم گر لطف او برخود گمان می داشتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر ز حیرت مهر خاموشی نبودی بر لبم</p></div>
<div class="m2"><p>می زدم آتش به عالم تا زبان می داشتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آستانش پاک می گردد غبار غم اسیر</p></div>
<div class="m2"><p>گر در آن کو اختیار پاسبان می داشتم</p></div></div>