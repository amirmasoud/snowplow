---
title: >-
    شمارهٔ ۳۸۰
---
# شمارهٔ ۳۸۰

<div class="b" id="bn1"><div class="m1"><p>گشته آیینه بیقرار خطت</p></div>
<div class="m2"><p>شده طوطی مگر شکار خطت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر مشق دل شکسته نویس</p></div>
<div class="m2"><p>می کشیدیم انتظار خطت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می توان خواند شرح گلشن راز</p></div>
<div class="m2"><p>از تماشای نوبهار خطت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که روزش سیه شود بیند</p></div>
<div class="m2"><p>سر خورشید در کنار خطت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرد دامن پر از گل شب بو</p></div>
<div class="m2"><p>صبح آییه شد دچار خطت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برد یکباره عقل و هوش از من</p></div>
<div class="m2"><p>رنگ و بوی بنفشه زار خطت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آرزوی دگر نمانده مرا</p></div>
<div class="m2"><p>دل و جان می کنم نثار خطت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رحم کن رحم بر اسیر که باز</p></div>
<div class="m2"><p>شده دیوانه بهار خطت</p></div></div>