---
title: >-
    شمارهٔ ۶۸۳
---
# شمارهٔ ۶۸۳

<div class="b" id="bn1"><div class="m1"><p>گفت‌وگویی شنیده‌ام که مپرس</p></div>
<div class="m2"><p>نگهی واکشیده‌ام که مپرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در محبت ز آه بی‌تأثیر</p></div>
<div class="m2"><p>اثری باز دیده‌ام که مپرس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منم آن هرزه‌گرد کز پی دل</p></div>
<div class="m2"><p>آنقدرها دویده‌ام که مپرس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون کنم شکر بی‌نوایی خویش</p></div>
<div class="m2"><p>به نوایی رسیده‌ام که مپرس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زیر پای سمند ناز کسی</p></div>
<div class="m2"><p>به هوایی تپیده‌ام که مپرس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اضطرابی تمام دارم اسیر</p></div>
<div class="m2"><p>آنچنان آرمیده‌ام که مپرس</p></div></div>