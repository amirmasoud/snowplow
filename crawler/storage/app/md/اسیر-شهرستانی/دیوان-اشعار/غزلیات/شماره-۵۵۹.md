---
title: >-
    شمارهٔ ۵۵۹
---
# شمارهٔ ۵۵۹

<div class="b" id="bn1"><div class="m1"><p>دمی که از نگهت سرمه ستم نکشد</p></div>
<div class="m2"><p>عجب که آینه چون گل جبین به هم نکشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فدای بیکسی کعبه گرفتاری</p></div>
<div class="m2"><p>که صید منت خونگرمی حرم نکشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز اختلاط پریشان عیش در قفس است</p></div>
<div class="m2"><p>دلم که ناز سبکروحی الم نکشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به جستجوی تو ممنون شوق خویشتنم</p></div>
<div class="m2"><p>که ننگ تهمت همراهی قدم نکشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به برگ شعله نوشتیم گفتگوی اسیر</p></div>
<div class="m2"><p>که از طراوت نامش سفینه نم نکشد</p></div></div>