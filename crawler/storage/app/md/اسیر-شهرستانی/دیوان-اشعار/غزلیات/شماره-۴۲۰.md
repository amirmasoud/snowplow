---
title: >-
    شمارهٔ ۴۲۰
---
# شمارهٔ ۴۲۰

<div class="b" id="bn1"><div class="m1"><p>غمی که درد نیفزود ننگ سلسله باد</p></div>
<div class="m2"><p>لبی که شکر نفرسود ساغر گله باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آشنایی صیاد در شکنجه دام</p></div>
<div class="m2"><p>دل رمیده عاشق شکار آبله باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هجوم گریه ما گرد ما به جا نگذاشت</p></div>
<div class="m2"><p>غبار خاطر یاران غبار قافله باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگاه گرم تحمل گداز و شوق رسا</p></div>
<div class="m2"><p>دلی که خون نشود خونبهای حوصله باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اسیر عشق کجا اختلاط عشق کجا</p></div>
<div class="m2"><p>جنون در آتش انجام این معامله باد</p></div></div>