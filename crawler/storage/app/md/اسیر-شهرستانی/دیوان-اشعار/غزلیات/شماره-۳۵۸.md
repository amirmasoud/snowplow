---
title: >-
    شمارهٔ ۳۵۸
---
# شمارهٔ ۳۵۸

<div class="b" id="bn1"><div class="m1"><p>در تن بیمار لعل روح پرورد تو نیست</p></div>
<div class="m2"><p>استخوانی کو نمک پرورده درد تو نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غیر مژگانی که باشد خانه زاد چشم تر</p></div>
<div class="m2"><p>کس در این ره محرم نظاره گرد تو نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لذت عشقت نه تنها با دل ریش است و بس</p></div>
<div class="m2"><p>مرهم آسودگی هم خالی از درد تو نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر چه از دستت کسی را نیست رنگی جز حنا</p></div>
<div class="m2"><p>نیست یک گل در چمن کو دستپرورد تو نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با غم او شکوه در افسردگی کم کن اسیر</p></div>
<div class="m2"><p>آتش پروانه چون خاکستر سرد تو نیست</p></div></div>