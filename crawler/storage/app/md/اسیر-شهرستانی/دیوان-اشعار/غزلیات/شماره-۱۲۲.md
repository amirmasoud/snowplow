---
title: >-
    شمارهٔ ۱۲۲
---
# شمارهٔ ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>موجها برده دل ز باغ در آب</p></div>
<div class="m2"><p>لاله ها شسته روی داغ در آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه صنمخانه ای است عالم عکس</p></div>
<div class="m2"><p>خوش نماید گل چراغ در آب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی سبب دل نمی کند ز سحاب</p></div>
<div class="m2"><p>قطره دارد گهر سراغ در آب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خط مشکین و عارض گلرنگ</p></div>
<div class="m2"><p>سایه افکنده بال زاغ در آب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در چمن باده ریخت اشک اسیر</p></div>
<div class="m2"><p>موج گل تر کند دماغ در آب</p></div></div>