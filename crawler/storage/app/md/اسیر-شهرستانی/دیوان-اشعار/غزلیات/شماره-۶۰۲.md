---
title: >-
    شمارهٔ ۶۰۲
---
# شمارهٔ ۶۰۲

<div class="b" id="bn1"><div class="m1"><p>نیرنگ باده رفع حجابم نمی کند</p></div>
<div class="m2"><p>آتش جدا چو گوهر از آبم نمی کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا غافل از خیال تو شد داغ می شوم</p></div>
<div class="m2"><p>کس چون دل فسرده کبابم نمی کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در هجر و وصل منفعل هستی خودم</p></div>
<div class="m2"><p>خجلت کدام روز خرابم نمی کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گردیده موج ساغر می سرنوشت من</p></div>
<div class="m2"><p>افسون توبه منع شرابم نمی کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیگانگی است خضر بیابان عشق اسیر</p></div>
<div class="m2"><p>لب تشنه فریب سرابم نمی کند</p></div></div>