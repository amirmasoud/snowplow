---
title: >-
    شمارهٔ ۴۵۶
---
# شمارهٔ ۴۵۶

<div class="b" id="bn1"><div class="m1"><p>گل ویرانه عاشق به روی آب می خندد</p></div>
<div class="m2"><p>به سعی تیشه بیجوهر سیلاب می خندد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهار آرزو را عندلیب از گرد پرواز است</p></div>
<div class="m2"><p>گل امید از باغ دل بیتاب می خندد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبوحی می زند برگ گل از شبنم چه می داند</p></div>
<div class="m2"><p>که صبح بی ثبات از مشرق سیماب می خندد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به مسجد چون روم بی او نمی دانم چه می گویم</p></div>
<div class="m2"><p>نمازم می رود از خاطر و محراب می خندد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سحاب وی شود هر برگ شبنم دیده گلشن</p></div>
<div class="m2"><p>به بیداری بگرید چون کسی در خواب می خندد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه رنگین باده ای دارد دلم از یاد او شبها</p></div>
<div class="m2"><p>گل پیمانه اش بر گلشن مهتاب می خندد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به بحری کز سبکروحی کند کشتی اسیر آنجا</p></div>
<div class="m2"><p>لب هر موجه ای بر لنگر گرداب می خندد</p></div></div>