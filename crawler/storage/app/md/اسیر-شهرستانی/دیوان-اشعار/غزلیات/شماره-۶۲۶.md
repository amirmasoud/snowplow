---
title: >-
    شمارهٔ ۶۲۶
---
# شمارهٔ ۶۲۶

<div class="b" id="bn1"><div class="m1"><p>مست است و عرض آتش رخسار می دهد</p></div>
<div class="m2"><p>خورشید را گداخت که را بار می دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خط از رخش دمید و هنوز آه می کشیم</p></div>
<div class="m2"><p>داریم خضر و تشنگی آزار می دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد موج خیز شعله غبارم چو درد می</p></div>
<div class="m2"><p>ساقی هنوز باده سرشار می دهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا دیده مایل است به تماشای گلستان</p></div>
<div class="m2"><p>آیینه گل به دست تو بسیار می دهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگز به نا امیدی امشب نبود اسیر</p></div>
<div class="m2"><p>این نخل خشک ما چه شبی بار می دهد</p></div></div>