---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>مکن در کار گلشن جلوه‌های انتخابی را</p></div>
<div class="m2"><p>مده بر باد رنگ و بوی گل‌های نقابی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم در سینه تا پر می زند چشمش خبر دارد</p></div>
<div class="m2"><p>نمی دانم کجا آورده این حاضر جوابی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه شد طفل است و خوابش می برد در دامن عاشق</p></div>
<div class="m2"><p>ز چشمش یاد گیرد گفتگوی نیم خوابی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مده دردسر ساقی برای امتحان من</p></div>
<div class="m2"><p>نیارد بر رخم صد جام رنگ بی حجابی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اسیرم هر چه هستم قایلم ناصح برو بنشین</p></div>
<div class="m2"><p>به تعمیر دلم تا کی دهی زحمت خرابی را</p></div></div>