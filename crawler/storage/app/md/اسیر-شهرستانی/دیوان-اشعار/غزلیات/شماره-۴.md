---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>بحر عشق است و وفا گوهر پاک است اینجا</p></div>
<div class="m2"><p>کشتی چاره گران سینه چاک است اینجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیر بازار وفا محشر ارباب دل است</p></div>
<div class="m2"><p>عالم تفرقه یک دامن پاک است اینجا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالم امن و امان گوشه میخانه بس است</p></div>
<div class="m2"><p>که نه بیم است و نه ترس است و نه باک است اینجا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگذر از تربت مستان که نظرگاه وفاست</p></div>
<div class="m2"><p>ذره خاک دل حوصله ناک است اینجا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل دیوانه چرا غیرت مستان نکشد</p></div>
<div class="m2"><p>رگ زنجیر جنون ریشه تاک است اینجا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رفت آنها که دلت صید چمن بود اسیر</p></div>
<div class="m2"><p>بلبلی در قفس سینه چاک است اینجا</p></div></div>