---
title: >-
    شمارهٔ ۱۶۱
---
# شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>هر دل از یاد تو مرغ چمن را زخود است</p></div>
<div class="m2"><p>حسن دمساز خود و عشق نظریاز خود است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کفر و دین عاقل و مجنون همه رسوای دلند</p></div>
<div class="m2"><p>هرکه دیدیم در این آیینه غماز خود است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاید افسانه خویت ز تپیدن شنود</p></div>
<div class="m2"><p>چون خموشی دل ما گوش بر آواز خود است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می رسد از چمن آینه آشفته چو گل</p></div>
<div class="m2"><p>می توان دید که غارتزده ناز خود است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل ما فال تپیدن زد و در خون غلطید</p></div>
<div class="m2"><p>برق بسمل زده شوخی پرواز خود است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برق حسن است اگر پرده اگر پرده دراست</p></div>
<div class="m2"><p>نور این آینه زنگ خود و پرواز خود است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فکر معماری آتشکده ای دارد اسیر</p></div>
<div class="m2"><p>از خیال ستمت خانه برانداز خود است</p></div></div>