---
title: >-
    شمارهٔ ۲۶۷
---
# شمارهٔ ۲۶۷

<div class="b" id="bn1"><div class="m1"><p>بهر پابوس تو از خاکم غباری برنخاست</p></div>
<div class="m2"><p>از گل بخت من اقبال زمین هم کوته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست تنها دست من کوتاه از دامان داغ</p></div>
<div class="m2"><p>بهر پنهان کردن داغ آستین هم کوته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر پریشان شد ز پیچ و تاب این درهم مباش</p></div>
<div class="m2"><p>از خم آن جعد مشکین دست چین هم کوته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون عنان کی دست او بوسم که مانند رکاب</p></div>
<div class="m2"><p>دست امید من از دامان زین هم کوته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون کنم دریوزه آتش برای سوختن</p></div>
<div class="m2"><p>دستم از دامان خاکستر نشین هم کوته است</p></div></div>