---
title: >-
    شمارهٔ ۷۲۶
---
# شمارهٔ ۷۲۶

<div class="b" id="bn1"><div class="m1"><p>مشغول یاد اوست دل پاره پاره ام</p></div>
<div class="m2"><p>رقصد ز شوق بر سر مژگان نظاره ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی که فال منصب دیوانگی زدم</p></div>
<div class="m2"><p>زنجیر سبحه گشت پی استخاره ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا شد زگریه ام شفقی رنگ آسمان</p></div>
<div class="m2"><p>چون داغ لاله غوطه به خون زد ستاره ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا از خیال روی تو دیوانه گشته ام</p></div>
<div class="m2"><p>گل رشک می برد به گریبان پاره ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا در طلسم شیشه فتادم چو بوی می</p></div>
<div class="m2"><p>خوشتر می دو ساله ز عمر دوباره ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خورشید را چو عارض او گفته ام اسیر</p></div>
<div class="m2"><p>شرمنده کرد دوری آن استعاره ام</p></div></div>