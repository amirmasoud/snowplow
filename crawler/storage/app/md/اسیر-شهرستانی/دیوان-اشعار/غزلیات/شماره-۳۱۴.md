---
title: >-
    شمارهٔ ۳۱۴
---
# شمارهٔ ۳۱۴

<div class="b" id="bn1"><div class="m1"><p>ز حرف دوست اگر کار من نرفت از دست</p></div>
<div class="m2"><p>ز دست رفتم و دست سخن نرفت از دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل بهار وفا ساخت زخم تیشه خویش</p></div>
<div class="m2"><p>به حیرتم که چرا کوهکن نرفت از دست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ثبات کامل دیر وفای کو این است</p></div>
<div class="m2"><p>غبار شد صنم و برهمن نرفت از دست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شراب عشق بتان را کباب می بایست</p></div>
<div class="m2"><p>کسی زبیم جگر سوختن نرفت از دست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حسنا نبسته مگر گل تعجبی دارم</p></div>
<div class="m2"><p>که دید دشمن و رنگ چمن نرفت از دست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیال گرد رهت گرد در چمن شده است</p></div>
<div class="m2"><p>اسیر از سمن و یاسمن نرفت از دست</p></div></div>