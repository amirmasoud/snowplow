---
title: >-
    شمارهٔ ۳۳۷
---
# شمارهٔ ۳۳۷

<div class="b" id="bn1"><div class="m1"><p>گلزارها ز اشک جگرتاب چشم کیست</p></div>
<div class="m2"><p>ویرانه ها نمونه ای از آب چشم کیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز خیالت از نظر ما نمی رود</p></div>
<div class="m2"><p>دانسته ای که سیر رخت باب چشم کیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبنم چکد ز گلبن شوخ شرارها</p></div>
<div class="m2"><p>صحرای جستجوی تو سیراب چشم کیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر آسمان نشان دهد از تخته پاره ای</p></div>
<div class="m2"><p>عالم شکسته کشتی سیلاب چشم کیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از ترکتاز شوخ نگاهان نشد غبار</p></div>
<div class="m2"><p>تا سرزمین آینه سیراب چشم کیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بلبل ترانه لب خاموش بیدلی است</p></div>
<div class="m2"><p>پروانه تا نظاره بیتاب چشم کیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>امیدوار باش چو دانسته ای اسیر</p></div>
<div class="m2"><p>بیداری دل از اثر خواب چشم کیست</p></div></div>