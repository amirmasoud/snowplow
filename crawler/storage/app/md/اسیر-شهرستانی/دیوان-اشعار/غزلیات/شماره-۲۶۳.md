---
title: >-
    شمارهٔ ۲۶۳
---
# شمارهٔ ۲۶۳

<div class="b" id="bn1"><div class="m1"><p>کار نفس ز جلوه رنگین گذشته است</p></div>
<div class="m2"><p>تا دیگر از دلم به چه آیین گذشته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رشکم برای عرض تجمل برد به باغ</p></div>
<div class="m2"><p>چاک دلم ز دامن گلچین گذشته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از سرگذشتگی هنر بسمل تو نیست</p></div>
<div class="m2"><p>کاری که کرده از سر تحسین گذشته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا رب مباد راز دلم نقل مجلسی</p></div>
<div class="m2"><p>در خاطر آن تبسم شیرین گذشته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا بیخودم شراب جنون می کشم اسیر</p></div>
<div class="m2"><p>کارم ز عقل و هوش و دل و دین گذشته است</p></div></div>