---
title: >-
    شمارهٔ ۱۱۱
---
# شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>قبله عالم میخانه خم ابروها</p></div>
<div class="m2"><p>گردش نرگس مستانه رم آهوها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیر گلشن کن اگر تشنه دیدار خودی</p></div>
<div class="m2"><p>آب از چشمه آیینه رود در جوها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالم آواره شوقند چه خورشید و چه ماه</p></div>
<div class="m2"><p>سوده پای فلک از شوق تو تا زانوها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دعوی این بس که ز کوشش همه رسوا شده ایم</p></div>
<div class="m2"><p>حلقه در گوش کمان تو خم ابروها</p></div></div>