---
title: >-
    شمارهٔ ۵۳۶
---
# شمارهٔ ۵۳۶

<div class="b" id="bn1"><div class="m1"><p>زبان ناله پر از شوخی تحریر می گیرد</p></div>
<div class="m2"><p>دعا پنداری امشب دامن تأثیر می گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شبنم آیینه دارد در بغل از صبح سرشاری</p></div>
<div class="m2"><p>فروغ روشنی دارد چو شمعی دیر می گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه گلها می توان چید از دل دیوانه مستی</p></div>
<div class="m2"><p>که دستی جام و دستی حلقه زنجیر می گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز عکس آیینه لافی با دل من می تواند زد</p></div>
<div class="m2"><p>اگر مرد است جا در بزم بی تصویر می گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اسیر از گوشه چشم تو با افلاک می گردد</p></div>
<div class="m2"><p>غبار دشت مجنون تو باج از شیر می گیرد</p></div></div>