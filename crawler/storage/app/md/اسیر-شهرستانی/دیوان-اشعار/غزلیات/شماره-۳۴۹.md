---
title: >-
    شمارهٔ ۳۴۹
---
# شمارهٔ ۳۴۹

<div class="b" id="bn1"><div class="m1"><p>همتم را ترک عالم کمتر از تسخیر نیست</p></div>
<div class="m2"><p>عشق بی طالع کم از اقبال عالمگیر نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حالتی داریم با کفر جنون آمیز خویش</p></div>
<div class="m2"><p>ناله ناقوس ما بی شورش زنجیر نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کعبه جو گردیده ام می خواهم از سر طی کنم</p></div>
<div class="m2"><p>راه صحرایی که کمتر از دم شمشیر نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دبستانی که عاشق درس حیرت خوانده است</p></div>
<div class="m2"><p>گفتگویی هست اما معنی و تفسیر نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترکتازی کو دو عالم را براندازد ز جا</p></div>
<div class="m2"><p>صف شکن تر از سپاه آه بی تأثیر نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر گناهی را به امید عطایی می کنیم</p></div>
<div class="m2"><p>بی گمان لطف بی اندازه یک تقصیر نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون اثر تقصیر دارد نیست نقص آه ما</p></div>
<div class="m2"><p>گر نباشد کارگر پیکان گناه تیر نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر دل بینشگری داری تماشا می کنی</p></div>
<div class="m2"><p>هر دو عالم بهتر از یک ناله شبگیر نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر دلت ویران شد از تعمیر معمور است اسیر</p></div>
<div class="m2"><p>عالم آباد کسی بی دهشت تعمیر نیست</p></div></div>