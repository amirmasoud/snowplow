---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>آسودگی کجا دل بیتاب من کجا</p></div>
<div class="m2"><p>شوق سفر کجا و قرار وطن کجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در پرده جذبه گر نشود رهنمای شوق</p></div>
<div class="m2"><p>یوسف کجا و رایحه پیرهن کجا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ابر است و گل شکفته و گلزار تازه روی</p></div>
<div class="m2"><p>ساقی کجا پیاله کجا انجمن کجا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیوانه نیستیم که پیمانه بشکنیم</p></div>
<div class="m2"><p>ما از کجا و توبه پیمان شکن کجا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باور نمی کنم سخن التفات او</p></div>
<div class="m2"><p>دیده است چشم او به غلط سوی من کجا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>افسانه ای است گفت و شنید لبش به ما</p></div>
<div class="m2"><p>راه سخن کجا ومجال سخن کجا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر عاشقی اسیر چرا دل شکسته ای</p></div>
<div class="m2"><p>آشفتگی کجا و هوای چمن کجا</p></div></div>