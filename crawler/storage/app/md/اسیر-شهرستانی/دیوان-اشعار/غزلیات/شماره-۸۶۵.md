---
title: >-
    شمارهٔ ۸۶۵
---
# شمارهٔ ۸۶۵

<div class="b" id="bn1"><div class="m1"><p>به استقبال مژگان سیاهی</p></div>
<div class="m2"><p>نگاهم می رود هر دم به راهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه می کردیم با چندین خجالت</p></div>
<div class="m2"><p>اگر بودی زبان عذرخواهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل است آیینه روز و شب ما</p></div>
<div class="m2"><p>نمی دانیم خورشیدی و ماهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شود گر خاکساریها صف آرا</p></div>
<div class="m2"><p>غباری بشکند قلب سپاهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به ذوقی صید فتراک تو گشتم</p></div>
<div class="m2"><p>که شد هر قطره خونم عیدگاهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فروشم دامن پاک دو عالم</p></div>
<div class="m2"><p>خرم چشم و دل عاشق نگاهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به محشر چون برآرم سر ز خجلت</p></div>
<div class="m2"><p>ندارم در خور بخشش گناهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو عمدا پرسد از نامم بگویی</p></div>
<div class="m2"><p>اسیر از بیزبانی بی گناهی</p></div></div>