---
title: >-
    شمارهٔ ۲۰۳
---
# شمارهٔ ۲۰۳

<div class="b" id="bn1"><div class="m1"><p>دل بی غم گل بی آب و رنگ است</p></div>
<div class="m2"><p>بهار گلشن آیینه زنگ است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر بد مستیی دارم به گردون</p></div>
<div class="m2"><p>میم در ساغر داغ پلنگ است؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هلاک شوخ پرکاری که صلحش</p></div>
<div class="m2"><p>گره در گوشه ابروی جنگ است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهارستان ما در دست ساقی است</p></div>
<div class="m2"><p>گل دیوانگی را باده رنگ است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمی دانم صف آرا جلوه گرکیست</p></div>
<div class="m2"><p>میان کعبه و بتخانه جنگ است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرشکم می کند طوفان الفت</p></div>
<div class="m2"><p>به گلزاری که یکرنگی دو رنگ است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غبارم در سرکویی زمینگیر</p></div>
<div class="m2"><p>شتابم مصلحت بین درنگ است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اسیر از اضطراب دل چه گویم</p></div>
<div class="m2"><p>فضای گفتگو بسیار تنگ است</p></div></div>