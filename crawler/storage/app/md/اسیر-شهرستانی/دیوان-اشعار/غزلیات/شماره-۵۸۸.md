---
title: >-
    شمارهٔ ۵۸۸
---
# شمارهٔ ۵۸۸

<div class="b" id="bn1"><div class="m1"><p>بیدلان ملک وفا را نه به خاتم گیرند</p></div>
<div class="m2"><p>رقم گریه نویسند و دو عالم گیرند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آه پنهان جگر سوختگان رسوایی است</p></div>
<div class="m2"><p>پرده شعله به روی دل بیغم گیرند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تنم از داغ جنون آینه شعله نماست</p></div>
<div class="m2"><p>عضو عضوم سبق سوختن از هم گیرند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راز داران خیال رخت از دیده پاک</p></div>
<div class="m2"><p>سینه را چون گل آیینه به شبنم گیرند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چاره درد دل گریه پرستان وفا</p></div>
<div class="m2"><p>با گلابی است که از اشک دمادم گیرند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با خیالت نکنم عیش ابد می ترسم</p></div>
<div class="m2"><p>که نشان غمت از خاطر خرم گیرند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در حسابند ز من عاقل و دیوانه اسیر</p></div>
<div class="m2"><p>بیش از آن درد تو دارم که مرا کم گیرند</p></div></div>