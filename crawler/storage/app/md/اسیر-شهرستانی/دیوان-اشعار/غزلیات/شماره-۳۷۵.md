---
title: >-
    شمارهٔ ۳۷۵
---
# شمارهٔ ۳۷۵

<div class="b" id="bn1"><div class="m1"><p>شوق روزی که پی چاک گریبان می‌گشت</p></div>
<div class="m2"><p>عمرها بود که مجنون تو عریان می‌گشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاد معماری مجنون که ز خاکستر دل</p></div>
<div class="m2"><p>رنگ هر خانه که می‌ریخت بیابان می‌گشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گردش چشم تو روزی که شکارم می‌کرد</p></div>
<div class="m2"><p>تا چه‌ها در دلت ای زود پشیمان می‌گشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناله را با نفس سوخته بنواخته بود</p></div>
<div class="m2"><p>که نیستان چقدر بی‌سر و سامان می‌گشت</p></div></div>