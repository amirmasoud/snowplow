---
title: >-
    شمارهٔ ۶۷۳
---
# شمارهٔ ۶۷۳

<div class="b" id="bn1"><div class="m1"><p>بحر وجود محشر موج و حباب گیر</p></div>
<div class="m2"><p>قطع نظر ز دل کن و عالم خراب گیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تشنگی گداخته دلها نظاره کن</p></div>
<div class="m2"><p>عطر گلاب چاره ز موج سراب گیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از ضعف دل بمیر و مکش منت کسی</p></div>
<div class="m2"><p>از دود آه قوت بوی گلاب گیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رنگ شکست دل ز رخت می شود عیان</p></div>
<div class="m2"><p>مگشا لب شکایت و جام شراب گیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا جمع و خرج دفتر عالم در آب ریز</p></div>
<div class="m2"><p>یا هر نفس که می کشی از دل حساب گیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صید دل رمیده به افسون نمی شود</p></div>
<div class="m2"><p>فتراک برق ساز(و) کمند از شهاب گیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کس از شکست خویش ز دام فنا نجست</p></div>
<div class="m2"><p>خود را مگیر ذره فزون ز آفتاب گیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر صدق نیت از نفست جوش می زند</p></div>
<div class="m2"><p>از دل کن استخاره و بر کف کتاب گیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همچون اسیر خنده زنی تا به مهر و ماه</p></div>
<div class="m2"><p>ساغر ز نقش پای سگ بوتراب گیر</p></div></div>