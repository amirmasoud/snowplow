---
title: >-
    شمارهٔ ۱۹۱
---
# شمارهٔ ۱۹۱

<div class="b" id="bn1"><div class="m1"><p>ساقی همین نه از تو دل ما در آتش است</p></div>
<div class="m2"><p>ساغر به خون نشسته و مینا در آتش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روشن چراغ دیده ز یاد تو کرده ایم</p></div>
<div class="m2"><p>بر هر چه بنگریم تماشا در آتش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بسکه داغ جلوه او گشته درچمن</p></div>
<div class="m2"><p>مانند شعله سرو سراپا در آتش است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از پای یک خمند گل و شمع تردماغ</p></div>
<div class="m2"><p>هر کس به رنگ دیگر از آنجا در آتش است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یاد نگاه گرم تو شد برق خرمنم</p></div>
<div class="m2"><p>چون آه خود مرا همه اعضا در آتش است</p></div></div>