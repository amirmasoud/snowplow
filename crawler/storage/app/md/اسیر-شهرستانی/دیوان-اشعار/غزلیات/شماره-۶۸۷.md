---
title: >-
    شمارهٔ ۶۸۷
---
# شمارهٔ ۶۸۷

<div class="b" id="bn1"><div class="m1"><p>شکار آینه گردیده آه می رسدش</p></div>
<div class="m2"><p>پری کشیده به دام نگاه می رسدش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه طعنه ها که به خورشید می تواند زد</p></div>
<div class="m2"><p>شکسته از مژه طرف کلاه می رسدش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپند طاقت سیماب دستگاه مرا</p></div>
<div class="m2"><p>چهار چشم تغافل پناه می رسدش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکسته آینه زنگ بسته ای دارم</p></div>
<div class="m2"><p>که دعوی نسب مهر و ماه می رسدش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نظاره در چمن آرزو چه کم دارد</p></div>
<div class="m2"><p>چو گل شکستن طرف کلاه می رسدش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گداختیم و مروت خجل نمی گردد</p></div>
<div class="m2"><p>ز دعوی که قسم بر گواه می رسدش</p></div></div>