---
title: >-
    شمارهٔ ۴۰۸
---
# شمارهٔ ۴۰۸

<div class="b" id="bn1"><div class="m1"><p>گل گل شکفته از شفق فیض باغ صبح</p></div>
<div class="m2"><p>پر گشته از شراب تماشا ایاغ صبح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کافر به حسرت دل بیتاب من مباد</p></div>
<div class="m2"><p>پر سوختم به ناحیه شام داغ صبح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب را کسی به خواب نبیند چو آفتاب</p></div>
<div class="m2"><p>روشن اگر ز روی تو گردد چراغ صبح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بسکه انتظار کشیدیم سوختیم</p></div>
<div class="m2"><p>در سینه دل گداخته ام چون چراغ صبح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر شب که عزم روی تو کردم اسیروار</p></div>
<div class="m2"><p>از گرد راه خویش گرفتم سراغ صبح</p></div></div>