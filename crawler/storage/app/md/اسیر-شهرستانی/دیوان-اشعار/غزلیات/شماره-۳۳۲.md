---
title: >-
    شمارهٔ ۳۳۲
---
# شمارهٔ ۳۳۲

<div class="b" id="bn1"><div class="m1"><p>فرزانگی در آتش غفلت سپند کیست</p></div>
<div class="m2"><p>دیوانگی شکار و رهایی کمند کیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاکم به باد رفت و غبارم ز جا نخاست</p></div>
<div class="m2"><p>معمار من طبیعت مشگل پسند کیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رشک شبم در آتش پروانگی گداخت</p></div>
<div class="m2"><p>تا صبح عندلیب گل غنچه خندکیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گردم ز بوی گل به هوا دام می کشد</p></div>
<div class="m2"><p>رعنا تذرو جلوه رعنا سمند کیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مستان طلسم توبه شکستن به نام من</p></div>
<div class="m2"><p>تکلیف آشنائی ساقی به پند کیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گشتم ز برق تازی آن جستجو غبار</p></div>
<div class="m2"><p>شب سرگذشته که سحر تیغ بند کیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل مشق ناله پیش رهایی کند اسیر</p></div>
<div class="m2"><p>این غنچه سایه پرور سرو بلند کیست</p></div></div>