---
title: >-
    شمارهٔ ۲۷۲
---
# شمارهٔ ۲۷۲

<div class="b" id="bn1"><div class="m1"><p>آنکه صید عالم از چشم خماری کرده است</p></div>
<div class="m2"><p>یاد خود کرده است پنداری شکاری کرده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده در آیینه روی خویش و بیخود گشته است</p></div>
<div class="m2"><p>حیرتستان را بهارش نوبهاری کرده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل به سر ساغر به کف پا در حنا می آید آه</p></div>
<div class="m2"><p>بیقراریهای ما جوش قراری کرده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می دهد بر باد هر ساعت غبار وعده ای</p></div>
<div class="m2"><p>هر نسیمی را فریب انتظاری کرده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل به نومیدی سپردن صید مطلب کردن است</p></div>
<div class="m2"><p>بی نیازی خار خشکی را بهاری کرده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق دیرین پرتوی دارد که بعد از سالها</p></div>
<div class="m2"><p>داغهای کهنه را خورشید زاری کرده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خنده خورشید می جوشد ز شام تار من</p></div>
<div class="m2"><p>سنبلستان خیالت خوش بهاری کرده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کی نگاهش یاد همچون من اسیری می کند</p></div>
<div class="m2"><p>آنکه از هر سایه مژگان شکاری کرده است</p></div></div>