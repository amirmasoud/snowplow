---
title: >-
    شمارهٔ ۶۵۳
---
# شمارهٔ ۶۵۳

<div class="b" id="bn1"><div class="m1"><p>شبنم باغ وفا از خار دندان سخت تر</p></div>
<div class="m2"><p>شوخی خورشید از الماس پیکان سخت تر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می رمد زنجیرش از زنجیر دیگر همچو موج</p></div>
<div class="m2"><p>نیست کس از سبزه زار سنگ دندان سخت تر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عرض سرعت می برد مجنون در آن وادی که هست</p></div>
<div class="m2"><p>گرد از افتادگی از کوه دامان سخت تر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تیغ بی جوهر خجالت دارد از بی قیمتی</p></div>
<div class="m2"><p>جود بی سرمایه از فولاد هم جان سخت تر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در جنون بر ناله ام مشق سرایت می کند</p></div>
<div class="m2"><p>بیزبانی هست از زنجیر افغان سخت تر</p></div></div>