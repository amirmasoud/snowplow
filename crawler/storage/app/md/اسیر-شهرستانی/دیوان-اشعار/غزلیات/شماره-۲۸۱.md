---
title: >-
    شمارهٔ ۲۸۱
---
# شمارهٔ ۲۸۱

<div class="b" id="bn1"><div class="m1"><p>تا غم نبوده خاطر خرم نبوده است</p></div>
<div class="m2"><p>زخم آنقدر نبوده که مرهم نبوده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از باغبان چه دیده گل آرزوی ما</p></div>
<div class="m2"><p>درگلشنی شکفته که شبنم نبوده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی توشه از قلمرو احسان گذشته ایم</p></div>
<div class="m2"><p>این سایه هرگز از سر ما کم نبوده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با دانه بهشت چه سازد ندیده دام</p></div>
<div class="m2"><p>جایی بهشت بوده که آدم نبوده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رشکم گداخت در چمن وصل او اسیر</p></div>
<div class="m2"><p>گویا دلی شکفته که بی غم نبوده است</p></div></div>