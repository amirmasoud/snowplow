---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>بی رخت شکوه ز بخت سیهی نیست مرا</p></div>
<div class="m2"><p>لاف طاقت زده ام کم گنهی نیست مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده گر جلوه گه گلشن امید شود</p></div>
<div class="m2"><p>همچو نرگس سر و برگ نگهی نیست مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حلقه دام در این سلسله محراب دعاست</p></div>
<div class="m2"><p>ورنه در هر دو جهان سجده گهی نیست مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می زند سوز دلم طعنه به آرام سپند</p></div>
<div class="m2"><p>به ز آتشکده آرامگهی نیست مرا</p></div></div>