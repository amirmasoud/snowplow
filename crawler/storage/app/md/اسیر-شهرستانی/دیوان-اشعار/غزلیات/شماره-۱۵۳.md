---
title: >-
    شمارهٔ ۱۵۳
---
# شمارهٔ ۱۵۳

<div class="b" id="bn1"><div class="m1"><p>صفحه افلاک سرلوح کتاب غفلت است</p></div>
<div class="m2"><p>نسخه ایام طومار حساب غفلت است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می توان گلهای رنگین چید از بزم جهان</p></div>
<div class="m2"><p>ذره تا خورشید سرمست شراب غفلت است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرسش بیدار دل توفیق آگاهی بس است</p></div>
<div class="m2"><p>محشر صاحبدلان تعبیر خواب غفلت است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محو دنیا اتحاد صورت و معنی ندید</p></div>
<div class="m2"><p>در نظر آیینه اعمی را کتاب غفلت است</p></div></div>