---
title: >-
    شمارهٔ ۶۵۴
---
# شمارهٔ ۶۵۴

<div class="b" id="bn1"><div class="m1"><p>حسنش از گلزارها گلزارتر</p></div>
<div class="m2"><p>دیده با حیرت پرستی یار تر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لطف پنهان از تغافل تازه رو</p></div>
<div class="m2"><p>دوستی از دشمنی خونخوار تر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کشته نازم به مژگانم چه کار</p></div>
<div class="m2"><p>گلبن آسودگی بیخارتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق ساقی باده خون میخانه دل</p></div>
<div class="m2"><p>هر که آنجا سست تر هشیارتر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شیشه دل تیشه سختی بیستون</p></div>
<div class="m2"><p>کار ما از کوهکن دشوارتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دشمنان از دوستان محرمترند</p></div>
<div class="m2"><p>دوستان از دشمنان اغیارتر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بوالهوس کور است و عاشق دوربین</p></div>
<div class="m2"><p>پاره ای این راه ناهموارتر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روشناس سنگ طفلان است اسیر</p></div>
<div class="m2"><p>ناصحا پند تو گوهربارتر</p></div></div>