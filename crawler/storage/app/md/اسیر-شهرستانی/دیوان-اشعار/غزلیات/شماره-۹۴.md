---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>خار و گل را جوش یک پیمانه می‌دانیم ما</p></div>
<div class="m2"><p>سبزه بیگانه را افسانه می‌دانیم ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خو به الفت کرده را بیگانه می‌دانیم ما</p></div>
<div class="m2"><p>سایه دیوانه را دیوانه می‌دانیم ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ریاضی کز خرامت شمع مینا روشن است</p></div>
<div class="m2"><p>نکهت گل را پر پروانه می‌دانیم ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش مجنون سربلندی‌ها خیالی بیش نیست</p></div>
<div class="m2"><p>آسمان را سایه ویرانه می‌دانیم ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شعله جواله از هر ترکتازش دیده‌ایم</p></div>
<div class="m2"><p>هر غباری را پر پروانه می‌دانیم ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حرفی از لوح جبین دوستی‌ها خوانده‌ایم</p></div>
<div class="m2"><p>آشنایان را ز هم بیگانه می‌دانیم ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از خرابی‌های دل گردیده نام ما بلند</p></div>
<div class="m2"><p>صبحدم را گرد این ویرانه می‌دانیم ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی‌قراری‌های پنهان گفت‌و‌گوی ما بس است</p></div>
<div class="m2"><p>از تپیدن‌های دل افسانه می‌دانیم ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا سواد خط سودای تو روشن کرده‌ایم</p></div>
<div class="m2"><p>نوخطان را سبزه بیگانه می‌دانیم ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جلوه ایجاد در نور چراغ خود گم است</p></div>
<div class="m2"><p>آفرینش را پر پروانه می‌دانیم ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کس نمی‌فهمد زبان گفت‌و‌گوی ما اسیر</p></div>
<div class="m2"><p>هرچه می‌دانیم ما بیگانه می‌دانیم ما</p></div></div>