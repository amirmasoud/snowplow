---
title: >-
    شمارهٔ ۷۶۴
---
# شمارهٔ ۷۶۴

<div class="b" id="bn1"><div class="m1"><p>در نظر نقش و نگارت می کشم</p></div>
<div class="m2"><p>تا نفس دارم خمارت می کشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می شوم خاک و گلستان می شوم</p></div>
<div class="m2"><p>حلقه در گوش بهارت می کشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سینه از داغت بر آتش می زنم</p></div>
<div class="m2"><p>درد یاری در دیارت می کشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در سر مستی ز ساغر بیخبر</p></div>
<div class="m2"><p>حرف لعل میگسارت می کشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بزم یکرنگی است مشق دوستی</p></div>
<div class="m2"><p>از دل خود انتظارت می کشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاک می گردم به راه وعده ای</p></div>
<div class="m2"><p>انتقام از انتظارت می کشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پر به صبر خویش می نازی اسیر</p></div>
<div class="m2"><p>حلقه در گوش قرارت می کشم</p></div></div>