---
title: >-
    شمارهٔ ۳۱۰
---
# شمارهٔ ۳۱۰

<div class="b" id="bn1"><div class="m1"><p>هر غنچه نشکفته نظر باز نگاهی است</p></div>
<div class="m2"><p>هر لاله دلسوخته سرچشمه آهی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بتخانه به رنگینی صحرای جنون نیست</p></div>
<div class="m2"><p>هر سایه خاری شکن طرف کلاهی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سامان وطن در گرو برگ سفر کن</p></div>
<div class="m2"><p>مگذار به جا شعله صفت گر همه آهی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از همرهی گریه چه خونها که نخوردیم</p></div>
<div class="m2"><p>هر نقش قدم در ره این قافله چاهی است</p></div></div>