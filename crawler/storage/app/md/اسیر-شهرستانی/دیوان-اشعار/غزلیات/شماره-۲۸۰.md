---
title: >-
    شمارهٔ ۲۸۰
---
# شمارهٔ ۲۸۰

<div class="b" id="bn1"><div class="m1"><p>بی جام و شیشه چشم تو خمار بوده است</p></div>
<div class="m2"><p>بی نوبهار روی تو گلزار بوده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان کشته شهادت و دل تشنه وصال</p></div>
<div class="m2"><p>در خون تپیدنی چقدر کار بوده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خواب پای خم شده با کعبه همسفر</p></div>
<div class="m2"><p>آن را که جذبه تو طلبکار بوده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روزی که جام شور به منصور داده اند</p></div>
<div class="m2"><p>هر کس به قدر خویش گرفتار بوده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیهوشیم نگر به چه آگاهیی رسید</p></div>
<div class="m2"><p>توفیق در پیاله سرشار بوده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان جنگجو شکایت بیجا مکن اسیر</p></div>
<div class="m2"><p>دایم جفا عزیز و وفا خوار بوده است</p></div></div>