---
title: >-
    شمارهٔ ۱۵۱
---
# شمارهٔ ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>گر آن دیر آشنا بیگانه مست است</p></div>
<div class="m2"><p>خوشم کاین بیخبر دیوانه مست است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به یاد چشم او جامی کشیدم</p></div>
<div class="m2"><p>در و دیوار این کاشانه مست است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل دیوانه از چشم تو شد مست</p></div>
<div class="m2"><p>که بد مست از گل پیمانه مست است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگاه گرم او در خواب دیده</p></div>
<div class="m2"><p>اگر گوید اسیر افسانه مست است</p></div></div>