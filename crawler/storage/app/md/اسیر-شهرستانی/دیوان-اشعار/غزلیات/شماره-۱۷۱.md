---
title: >-
    شمارهٔ ۱۷۱
---
# شمارهٔ ۱۷۱

<div class="b" id="bn1"><div class="m1"><p>تنگ غم تو خانه دل از هوا پر است</p></div>
<div class="m2"><p>لب از ترانه خالی و گوش از نوا پر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دام شکوه ناله شکستن چه لازم است</p></div>
<div class="m2"><p>دنیا فراخ و سلسله بسیار و جا پر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خاطرش اراده نظاره برده ایم</p></div>
<div class="m2"><p>آیینه داغ شو که دل او ز ما پر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جوش بهار گردش چشم سیاه کیست</p></div>
<div class="m2"><p>صد شیشه گشت خالی و جام هوا پر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی خون من بهار ستم گل نمی کند</p></div>
<div class="m2"><p>پیمانه ام ز شبنم باغ وفا پر است</p></div></div>