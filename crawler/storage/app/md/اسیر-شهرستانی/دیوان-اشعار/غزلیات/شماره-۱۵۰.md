---
title: >-
    شمارهٔ ۱۵۰
---
# شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>در آتش عشق تو خرد خام نخست است</p></div>
<div class="m2"><p>امید زسودای تو ناکام نخست است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجنون تو را در طلب کعبه مقصود</p></div>
<div class="m2"><p>انجام بیابان فنا گام نخست است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از یک نگه گرم فتادیم در آتش</p></div>
<div class="m2"><p>آیینه بیهوشی ما جام نخست است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان نیست مرا برگ تعلق که در این باغ</p></div>
<div class="m2"><p>ناکامی آخر ثمر گام نخست است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در عشق تو قطع نظر از خویش نمودن</p></div>
<div class="m2"><p>رمزی است که در پرده پیغام نخست است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جایی که تجلی ز رخت شمع فروزد</p></div>
<div class="m2"><p>شوق ار همه پروانه شود جام نخست است</p></div></div>