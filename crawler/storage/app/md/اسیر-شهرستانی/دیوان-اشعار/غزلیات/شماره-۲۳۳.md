---
title: >-
    شمارهٔ ۲۳۳
---
# شمارهٔ ۲۳۳

<div class="b" id="bn1"><div class="m1"><p>ز فیض گریه چمن یک بساط چیده ماست</p></div>
<div class="m2"><p>بهار نشئه می حاصل رسیده ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چراغ حسن بود روشن از فروغ حجاب</p></div>
<div class="m2"><p>گلی که خنده ندانسته نور دیده ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گلی زگلشن عیش گذشته می چینم</p></div>
<div class="m2"><p>بهار رفته نشان دل رمیده ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به رنگ و بوی گل از یاد خویشتن رفتم</p></div>
<div class="m2"><p>وداع اول شوق سفر ندیده ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز پرتو گل روی تو صبح و شام یکی است</p></div>
<div class="m2"><p>نقاب جلوه حسنت حجاب دیده ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شراب حسرت سرشار ساغری دارد</p></div>
<div class="m2"><p>دل گداخته پیمانه کشیده ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اسیر سر زگریبان آسمان نکشد</p></div>
<div class="m2"><p>جنون که درد شراب به سر دویده ماست</p></div></div>