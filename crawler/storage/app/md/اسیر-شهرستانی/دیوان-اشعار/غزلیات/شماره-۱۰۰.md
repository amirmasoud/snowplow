---
title: >-
    شمارهٔ ۱۰۰
---
# شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>از می دیگر است مستی ما</p></div>
<div class="m2"><p>سر ساغر به گردن مینا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>واژگون است کار اهل جنون</p></div>
<div class="m2"><p>خار بر سر زنیم و گل بر پا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیچش زلف موج زنجیر است</p></div>
<div class="m2"><p>خط سبز است نسخه سودا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکدم از خون نمی شود خالی</p></div>
<div class="m2"><p>بی تو هم چشم ماست ساغر ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در جنون همچو گردباد آخر</p></div>
<div class="m2"><p>زدم از آه خیمه بر صحرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دل تنگ دیده پر خون است</p></div>
<div class="m2"><p>مایه از قطره دارد این دریا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز آتش دوری تو می سوزد</p></div>
<div class="m2"><p>دل جدا جان جدا اسیر جدا</p></div></div>