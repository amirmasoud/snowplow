---
title: >-
    شمارهٔ ۲۹۰
---
# شمارهٔ ۲۹۰

<div class="b" id="bn1"><div class="m1"><p>دشت جنون ز فتنه چشم تو دیده ای است</p></div>
<div class="m2"><p>هر موج اشک خیل غزال رمیده ای است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک عمر در شکنجه امید بوده ایم</p></div>
<div class="m2"><p>هر ذره خاک ما دل منت گزیده ای است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون خامه سر به راه طلب را دلیل نیست</p></div>
<div class="m2"><p>صحرا زجاده صفحه مسطر کشیده ای است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شور شراب پیر ز دنیا گذشته است</p></div>
<div class="m2"><p>گرد جنون جوان به منزل رسیده ای است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در صیدگاه فتنه مجال گریز نیست</p></div>
<div class="m2"><p>تیر قضا به بند کمان کشیده ای است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صیاد عشق را که چمن دامگاه اوست</p></div>
<div class="m2"><p>بوی بهار وحشی در خون تپیده ای است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ممنون التفات گرانجانی خودیم</p></div>
<div class="m2"><p>مشت غبار ما نفس آرمیده ای است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما را ز دل به کعبه مقصود برد اسیر</p></div>
<div class="m2"><p>توفیق ما که خضر بیابان ندیده ای است</p></div></div>