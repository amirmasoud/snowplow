---
title: >-
    شمارهٔ ۲۰۰
---
# شمارهٔ ۲۰۰

<div class="b" id="bn1"><div class="m1"><p>بهار شوخی او جشن تازه فلک است</p></div>
<div class="m2"><p>شراب خوش مزه است و کباب (خوش نمک) است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز یمن همت احباب مطلبی داریم</p></div>
<div class="m2"><p>که گر به هیچ نسنجد (دو) صد هزار یک است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به حال ما نزند خنده گر کسی داند</p></div>
<div class="m2"><p>که در قلمرو دیوانه صبرکمترک است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حریف منت احباب نیستم ساقی</p></div>
<div class="m2"><p>شکست توبه من با تو آشنا ترک است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>توان زصافی دل دید حال دشمن و دوست</p></div>
<div class="m2"><p>همین که پاک شد از کینه خاطرت محک است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز زهر خند دلم می شود نهان در پیش؟</p></div>
<div class="m2"><p>اگر تصورحالم کند فلک فلک است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمی شود اثر ناله کار خود نکند</p></div>
<div class="m2"><p>گداز آتش دل می خورد مگر خنک است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کباب از آتش دل ساختیم اسیر بیا</p></div>
<div class="m2"><p>شراب شوق مهیا شده است دل نمک است</p></div></div>