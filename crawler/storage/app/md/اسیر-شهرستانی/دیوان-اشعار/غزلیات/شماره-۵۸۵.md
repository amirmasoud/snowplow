---
title: >-
    شمارهٔ ۵۸۵
---
# شمارهٔ ۵۸۵

<div class="b" id="bn1"><div class="m1"><p>تا دل مست تو را داغ وفا بخشیدند</p></div>
<div class="m2"><p>جرم صد میکده از نیم دعا بخشیدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیکسی قرعه اقبال سلیمانی زد</p></div>
<div class="m2"><p>جای خاتم دل شوریده به ما بخشیدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر سر شمع زند دسته گف فیض سحر</p></div>
<div class="m2"><p>تا به پروانه ما بال هما بخشیدند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هستی و نیستی اقلیم تبهکاری بود</p></div>
<div class="m2"><p>جرم ما را زکجا تا به کجا بخشیدند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوستان سینه صاف آینه توفیق است</p></div>
<div class="m2"><p>جرم ناکرده ما را به وفا بخشیدند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شعله خوی تو هر لحظه به رنگی می سوخت</p></div>
<div class="m2"><p>پر طاوس به خاکستر ما بخشیدند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشت خاکستر ما سرمه دل ساز اسیر</p></div>
<div class="m2"><p>روشنایی است که در راه خدا بخشیدند</p></div></div>