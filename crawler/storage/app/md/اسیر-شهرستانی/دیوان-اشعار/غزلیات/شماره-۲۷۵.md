---
title: >-
    شمارهٔ ۲۷۵
---
# شمارهٔ ۲۷۵

<div class="b" id="bn1"><div class="m1"><p>دلم از یاد تو خندان شده است</p></div>
<div class="m2"><p>شبم از صبح چراغان شده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جگر صبر گدازد دندان</p></div>
<div class="m2"><p>جان فشانی است که آسان شده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جلوه ای داد غبارم بر باد</p></div>
<div class="m2"><p>چقدر بوی گل ارزان شده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حسن را بوده زکاتی زین بیش</p></div>
<div class="m2"><p>دل پریشان پریشان شده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه گذشته است دگر در دل ناز</p></div>
<div class="m2"><p>کفر را آینه ایمان شده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکه پیدایی او پنهانی است</p></div>
<div class="m2"><p>از دل و چشم که پنهان شده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی تو جشن است تماشایی کن</p></div>
<div class="m2"><p>شبم از گریه چراغان شده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل پریخانه زخم جگر است</p></div>
<div class="m2"><p>دور از آن سایه مژگان شده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی نظر بازی مژگانش اسیر</p></div>
<div class="m2"><p>محشر زخم نمایان شده است</p></div></div>