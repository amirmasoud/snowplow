---
title: >-
    شمارهٔ ۱۱۳
---
# شمارهٔ ۱۱۳

<div class="b" id="bn1"><div class="m1"><p>خوش بهاری است قدح‌نوشی‌ها</p></div>
<div class="m2"><p>بوی گل نشئه بیهوشی‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گریه کی فرصت حیرت می‌داد</p></div>
<div class="m2"><p>می‌شمردم به تو خاموشی‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لب گشودم سخن از یادم رفت</p></div>
<div class="m2"><p>چه بهشتی است فراموشی‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چقدر درد دل از یادم رفت</p></div>
<div class="m2"><p>حاصلی داشت فراموشی‌ها</p></div></div>