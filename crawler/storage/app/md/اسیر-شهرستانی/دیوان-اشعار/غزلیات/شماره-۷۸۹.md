---
title: >-
    شمارهٔ ۷۸۹
---
# شمارهٔ ۷۸۹

<div class="b" id="bn1"><div class="m1"><p>تا از دل و دین جدا نگشتیم</p></div>
<div class="m2"><p>با درد تو آشنا نگشتیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بتخانه و مسجد و خرابات</p></div>
<div class="m2"><p>ما در طلبت کجا نگشتیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آیینه آفتاب بودیم</p></div>
<div class="m2"><p>تا ذره خودنما نگشتیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ممنون شراب یا دواییم</p></div>
<div class="m2"><p>شرمنده از این هوا نگشتیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان صبر چکیده است ما را</p></div>
<div class="m2"><p>خجلت زده جفا نگشتیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیهوشی دایم پیاله</p></div>
<div class="m2"><p>غافل ز تو بیوفا نگشتیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در چشم کسی نمی توان رفت</p></div>
<div class="m2"><p>شادیم که توتیا نگشتیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مانند اسیر در دو عالم</p></div>
<div class="m2"><p>از سایه او جدا نگشتیم</p></div></div>