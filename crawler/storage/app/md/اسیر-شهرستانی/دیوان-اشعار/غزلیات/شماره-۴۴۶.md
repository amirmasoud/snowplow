---
title: >-
    شمارهٔ ۴۴۶
---
# شمارهٔ ۴۴۶

<div class="b" id="bn1"><div class="m1"><p>اگر خاکستر پروانه ما توده می گردد</p></div>
<div class="m2"><p>پر از گل می شود گر دامنی فرسوده می گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جلایی می دهد آیینه او را غبار ما</p></div>
<div class="m2"><p>علاج ضعف دل باشد گهر چون سوده می گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دعایی می کند پنهان زبانش لکنتی دارد</p></div>
<div class="m2"><p>دلم گاهی میان اضطراب آسوده می گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز گردشهای چشمش می توان دیدن چه دل دارد</p></div>
<div class="m2"><p>که ساقی سرگران از ساغر پیموده می گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هشیاری چشانی گر شراب سرگرانی را</p></div>
<div class="m2"><p>دل شب زنده داران چشم خواب آلوده می گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نگه تا می توانی عرض مطلب می توان کردن</p></div>
<div class="m2"><p>نفس تا می کشی راه سخن پیموده می گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمی دانم سراغ صیدگاهش اینقدر دانم</p></div>
<div class="m2"><p>که در محشر در پناه صید زخم آلوده می گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر گستاخ می بودم اسیر اظهار می کردم</p></div>
<div class="m2"><p>که پایان نیست راهش را فلک بیهوده می گردد</p></div></div>