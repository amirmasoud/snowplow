---
title: >-
    شمارهٔ ۶۵۹
---
# شمارهٔ ۶۵۹

<div class="b" id="bn1"><div class="m1"><p>شهید راز پنهان را سرافرازی نهان بهتر</p></div>
<div class="m2"><p>خموشی در طریق ما رفیق همزبان بهتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ره بی مطلبی دور است سامان بیشتر باید</p></div>
<div class="m2"><p>غبار بی نشان کاروان در کاروان بهتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برای خاطر ما شب نشینان هرکه بیدار است</p></div>
<div class="m2"><p>ز فیض صبحدم بیخوابی آن پاسبان بهتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ادب رنگین بهار گلشن حسن دل آشوب است</p></div>
<div class="m2"><p>ز باغ بی‌صفا افسردگی‌های خزان بهتر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اسیر از دفتر کین عزت آیین مصرعی خواندم</p></div>
<div class="m2"><p>نسیم عمر یکرنگی ز عمر جاودان بهتر</p></div></div>