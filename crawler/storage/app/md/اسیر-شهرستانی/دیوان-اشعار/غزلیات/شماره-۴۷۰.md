---
title: >-
    شمارهٔ ۴۷۰
---
# شمارهٔ ۴۷۰

<div class="b" id="bn1"><div class="m1"><p>به جایی می‌رسد شوقی که الفت راهبر دارد</p></div>
<div class="m2"><p>پر پرواز طوطی رنگ برگ نیشکر دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلی از کین مردم پاک می بینی چه می دانی</p></div>
<div class="m2"><p>که از بیجوهری این تیغ جوهر بیشتر دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سری در جیب کش چون قطره سیر بحر هستی کن</p></div>
<div class="m2"><p>حباب از خود نمایی سر به بالین خطر دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به این سرعت کدامین درد دل را مختصر سازم</p></div>
<div class="m2"><p>تپد در سینه چون دل بال مرغ نامه بر دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز داغ لاله کاران سبزه الماس می جوشد</p></div>
<div class="m2"><p>بهار عافیت سرچشمه از خون جگر دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نگه دزدینش بیند دو عالم طرفه تر این است</p></div>
<div class="m2"><p>نظرباز وفا بیتابی بحر دگر دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگردد رنج پاس خاطر روشندلان ضایع</p></div>
<div class="m2"><p>صدف در جوش طوفان تکیه بر آب گهر دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اسیر از دشت دل مجنون دماغی می برد بویی</p></div>
<div class="m2"><p>که از هر سایه خاری بهاری در نظر دارد</p></div></div>