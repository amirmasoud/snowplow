---
title: >-
    شمارهٔ ۴۱۱
---
# شمارهٔ ۴۱۱

<div class="b" id="bn1"><div class="m1"><p>بسکه از مهر او گداخته صبح</p></div>
<div class="m2"><p>علم صدق بر فراخته صبح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جلوه ای کن عجب تماشایی است</p></div>
<div class="m2"><p>برت آیینه خانه ساخته صبح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نرد با آفتاب می بازد</p></div>
<div class="m2"><p>رنگ خود را چرا نباخته صبح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه بساطی به خویش چیده ز رنگ</p></div>
<div class="m2"><p>تا که را مشتری شناخته صبح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در هوای غبار جولانی</p></div>
<div class="m2"><p>رفته از کار بسکه تاخته صبح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داو کن نقد آفتابش را</p></div>
<div class="m2"><p>زر انجم نبسته باخته صبح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سنبل شام و یاسمین سحر</p></div>
<div class="m2"><p>به هوای تو دسته ساخته صبح</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شب وصل خیال قامت اوست</p></div>
<div class="m2"><p>آه من گشته سرو و فاخته صبح</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می توان دید اسیر از رویش</p></div>
<div class="m2"><p>دل آیینه را نواخته صبح</p></div></div>