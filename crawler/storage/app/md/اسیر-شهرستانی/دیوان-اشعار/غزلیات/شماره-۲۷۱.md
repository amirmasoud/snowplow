---
title: >-
    شمارهٔ ۲۷۱
---
# شمارهٔ ۲۷۱

<div class="b" id="bn1"><div class="m1"><p>نه همین از دوری احباب داغم کرده است</p></div>
<div class="m2"><p>آسمان زهر هلاهل در ایاغم کرده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرده شمع مجلس افروزی که از هجران او</p></div>
<div class="m2"><p>جای روغن خون دل غم در چراغم کرده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسکه الفت داشت با من حسرت دیدار او</p></div>
<div class="m2"><p>در دل خود هر نفس گرد سراغم کرده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نو بهاری رفته است از گلشن عالم برون</p></div>
<div class="m2"><p>گریه یاران در و دیوار باغم کرده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غنچه باغ فراقش هر دم از شوخی اسیر</p></div>
<div class="m2"><p>جای بو بیهوشدارو در دماغم کرده است</p></div></div>