---
title: >-
    شمارهٔ ۴۰۰
---
# شمارهٔ ۴۰۰

<div class="b" id="bn1"><div class="m1"><p>پرده چشم سمن پیراهنت</p></div>
<div class="m2"><p>جای بوی پیرهن پیراهنت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تنت جای عرق دل می چکد</p></div>
<div class="m2"><p>محشر آشوب من پیراهنت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماه از شوق کتانی جان دهد</p></div>
<div class="m2"><p>گشته لبریز بدن پیراهنت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش خیالی جلوه اندام تو</p></div>
<div class="m2"><p>نازکی مضمون سخن پیراهنت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عضو عضوت یوسف مصر بهار</p></div>
<div class="m2"><p>پیرکنعان چمن پیراهنت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اشک بلبل چشمه سار جان شود</p></div>
<div class="m2"><p>گر کند گل پیرهن پیراهنت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می کند افسردگی داغت اسیر</p></div>
<div class="m2"><p>گر نباشد سوختن پیراهنت</p></div></div>