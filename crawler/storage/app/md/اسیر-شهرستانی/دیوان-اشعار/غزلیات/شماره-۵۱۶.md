---
title: >-
    شمارهٔ ۵۱۶
---
# شمارهٔ ۵۱۶

<div class="b" id="bn1"><div class="m1"><p>کلفت زخاطرم دل بیدار می برد</p></div>
<div class="m2"><p>زنگ از دلم پیاله سرشار می برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی گریه دست و پای تو موجی است در سراب</p></div>
<div class="m2"><p>بیهوده عرض کوشش بسیار می برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناصح ز منع باده اگر نوش می کند</p></div>
<div class="m2"><p>دیوانه را به دیدن خمار می برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آواره گل ز آبله خون نچیده است</p></div>
<div class="m2"><p>پایی که ره به کوچه زنار می برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نازکدلان برای شگون می خرند اسیر</p></div>
<div class="m2"><p>مفت دلی که حیرتش از کار می برد</p></div></div>