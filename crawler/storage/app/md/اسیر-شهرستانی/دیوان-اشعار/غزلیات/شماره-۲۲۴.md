---
title: >-
    شمارهٔ ۲۲۴
---
# شمارهٔ ۲۲۴

<div class="b" id="bn1"><div class="m1"><p>گر شود همسایه زلفت صبا نامحرم است</p></div>
<div class="m2"><p>ور شود همخوابه چشمت حیا نامحرم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد چراغم روشن از خاکستر بخت سیاه</p></div>
<div class="m2"><p>بعد از این آیینه دل را صفا نامحرم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا دعای دوست شد ورد دل خلوت نشین</p></div>
<div class="m2"><p>گر اثر باشد زبان وقت دعا نامحرم است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آشنای عشق را با روشنایی کار نیست</p></div>
<div class="m2"><p>کلبه تاریک عاشق را ضیا نامحرم است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حسن را هر چند حیرت پاسبان باشد اسیر</p></div>
<div class="m2"><p>گر شود آیینه او چشم ما نامحرم است</p></div></div>