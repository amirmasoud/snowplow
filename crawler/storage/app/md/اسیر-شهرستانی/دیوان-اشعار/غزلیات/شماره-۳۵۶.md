---
title: >-
    شمارهٔ ۳۵۶
---
# شمارهٔ ۳۵۶

<div class="b" id="bn1"><div class="m1"><p>کدام صبح که سرمشق انتظارم نیست</p></div>
<div class="m2"><p>کدام شب که سر گریه در کنارم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بیزبانی خود شکر بر زبان دارم</p></div>
<div class="m2"><p>که سهو شکوه در اوراق روزگارم نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز ابر سایه برگ خزان سرشته گلم</p></div>
<div class="m2"><p>شکفتگی است که در خاطر بهارم نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکفتگی نپذیرد اگر غبار شوم</p></div>
<div class="m2"><p>ز سخت رویی دل عقده ای به کارم نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم به یاد تو سرگرم شعله آرایی</p></div>
<div class="m2"><p>کدام شب که چراغان انتظارم نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا در آتش افسردگی گداخته اند</p></div>
<div class="m2"><p>شراب صندل(و) درد سر خمارم نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به گوش حلقه زنجیر راز می گویم</p></div>
<div class="m2"><p>اسیر بر دل دیوانه اعتبارم نیست</p></div></div>