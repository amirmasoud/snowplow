---
title: >-
    شمارهٔ ۳۶۳
---
# شمارهٔ ۳۶۳

<div class="b" id="bn1"><div class="m1"><p>فلک ز کام من سفله کیش عار نداشت</p></div>
<div class="m2"><p>دلم دماغ سرانجام اعتبار نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به کوه و دشت جنون سوده گشت پای طلب</p></div>
<div class="m2"><p>به بیزبانی من عشق خاکسار نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهار عنبر خاکستر شهید وفا</p></div>
<div class="m2"><p>به گرمخونی پروانه یک شرار نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نماند رنگ به خونم ز مشق دام و قفس</p></div>
<div class="m2"><p>شکارگاه محبت چو من شکار نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شتاب بوی گل و اضطراب برق نگاه</p></div>
<div class="m2"><p>سبک عنانی شوقم گه بهار نداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فلک شد آبله پای سوده ره دل</p></div>
<div class="m2"><p>ولی چه سود که پیش تو اعتبار نداشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شب از خیال تو محشر به خواب می دیدم</p></div>
<div class="m2"><p>کسی به پرسش عمر گذشته کار نداشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گل نزاکت از این شوختر نمی باشد</p></div>
<div class="m2"><p>زمین وعده گهش تاب انتظار نداشت</p></div></div>