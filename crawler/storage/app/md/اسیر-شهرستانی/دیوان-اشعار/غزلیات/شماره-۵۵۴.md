---
title: >-
    شمارهٔ ۵۵۴
---
# شمارهٔ ۵۵۴

<div class="b" id="bn1"><div class="m1"><p>نگه یار گریبان تو باشد</p></div>
<div class="m2"><p>اگر بار گریبان تو باشد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگاه من چو بوی غنچه پنهان</p></div>
<div class="m2"><p>گرفتار گریبان تو باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدم با بوی پیراهن که ترسم</p></div>
<div class="m2"><p>خریدار گریبان تو باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به مژگانت سپردم رشته جان</p></div>
<div class="m2"><p>که در کار گریبان تو باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دعای بی سر و سامان اسیر است</p></div>
<div class="m2"><p>نگهدار گریبان تو باشد</p></div></div>