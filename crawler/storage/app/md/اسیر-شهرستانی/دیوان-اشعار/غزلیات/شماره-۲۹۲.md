---
title: >-
    شمارهٔ ۲۹۲
---
# شمارهٔ ۲۹۲

<div class="b" id="bn1"><div class="m1"><p>نظر به جوهر اصلی نه زشت مکتسبی است</p></div>
<div class="m2"><p>که عیب زاده تصحیف باده عنبی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فزود کام دل ما ز نا امیدی ما</p></div>
<div class="m2"><p>فروغ گوهر عاشق زلال تشنه لبی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکست خاطر ما خانه زاد خاطر ماست</p></div>
<div class="m2"><p>گواه نسبت خارا و شیشه حلبی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز جوش آینه باده صاف می گردد</p></div>
<div class="m2"><p>کمال رتبه عاشق ز اوج بی ادبی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غبار آیینه ما بهار خاطر ماست</p></div>
<div class="m2"><p>گل شکفته گل باغ آرزو طلبی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار زخم نمایان به حسرت ارزانی</p></div>
<div class="m2"><p>علاج زخم نهان خنده های زیر لبی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشو زدولت بیدار ناامید اسیر</p></div>
<div class="m2"><p>کلید قفل اثر با دعای نیمشبی است</p></div></div>