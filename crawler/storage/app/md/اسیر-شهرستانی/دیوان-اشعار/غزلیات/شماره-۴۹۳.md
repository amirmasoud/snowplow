---
title: >-
    شمارهٔ ۴۹۳
---
# شمارهٔ ۴۹۳

<div class="b" id="bn1"><div class="m1"><p>محبت در غم بیدردی آزادم نگه دارد</p></div>
<div class="m2"><p>جنون در ماتم آسودگی شادم نگه دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صفیری گر کشد مرغ قفس را ذوق پرواز است</p></div>
<div class="m2"><p>خموشی در گرفتاری ز فریادم نگه دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دل از ترکتاز عشق بیباکی غمی دارم</p></div>
<div class="m2"><p>خدا از چشم زخم خاطر شادم نگه دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سواد اعظم ویرانیم نامم غم آباد است</p></div>
<div class="m2"><p>نبینم روی معموری غم آبادم نگه دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حباب بحر آتش با لب فرسوده ای دارم</p></div>
<div class="m2"><p>نبینم آفتی گر عشق بنیادم نگه دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرداش گر ز خون صید دیگر زیب فتراک است</p></div>
<div class="m2"><p>بگو تا کشته در فتراک بیدادم نگه دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من آن صیدم که با دام تغافل کرده ام الفت</p></div>
<div class="m2"><p>خدا از مهربانیهای صیادم نگه دارد</p></div></div>