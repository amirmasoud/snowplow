---
title: >-
    شمارهٔ ۴۴۰
---
# شمارهٔ ۴۴۰

<div class="b" id="bn1"><div class="m1"><p>دل شکسته درستی پذیر می گردد</p></div>
<div class="m2"><p>ز پا فتادگیم دستگیر می گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که دیده مشت غباری به آن زمینگیری</p></div>
<div class="m2"><p>عبیر پیرهن چرخ پیر می گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ضعیف نالی از آن بیشتر نمی باشد</p></div>
<div class="m2"><p>خموشی از دم سردم صفیر می گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه مدعا چقدر باب انتظار است این</p></div>
<div class="m2"><p>به کام خویشم و نالم که دیر می گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنای خانه الفت ز موم می سازد</p></div>
<div class="m2"><p>اگر زمانه به کام اسیر می گردد</p></div></div>