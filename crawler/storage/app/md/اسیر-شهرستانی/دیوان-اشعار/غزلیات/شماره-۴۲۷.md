---
title: >-
    شمارهٔ ۴۲۷
---
# شمارهٔ ۴۲۷

<div class="b" id="bn1"><div class="m1"><p>قطره چون موج اضطراب افتد</p></div>
<div class="m2"><p>بر سرش خانه حباب افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست ویرانه تر دماغ از سیل</p></div>
<div class="m2"><p>همچو آن گل که در شراب افتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هوس آلوده صید دام هواست</p></div>
<div class="m2"><p>مست در سایه سحاب افتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل ما را عبث خراب مکن</p></div>
<div class="m2"><p>گل صدبرگ بی گلاب افتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از چراغان صبح گرد رهی</p></div>
<div class="m2"><p>ذره از چشم آفتاب افتد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل ما کرده جشن بیداری</p></div>
<div class="m2"><p>چشم پیمانه مست خواب افتد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کوه و دشت جنون سبکروح است</p></div>
<div class="m2"><p>از نسیمی در اضطراب افتد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرگشایم بساط صافدلی</p></div>
<div class="m2"><p>چه کتانها به ماهتاب افتد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تشنه تر می شود سراب عدم</p></div>
<div class="m2"><p>گر دو عالم به روی آب افتد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ناله مور محشری دارد</p></div>
<div class="m2"><p>خانه شیر از آن خراب افتد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گل شکار است تار پیرهنت</p></div>
<div class="m2"><p>همچو موجی که بر گلاب افتد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیست از تاب می اسیر عجب</p></div>
<div class="m2"><p>گل چو پروانه در شراب افتد</p></div></div>