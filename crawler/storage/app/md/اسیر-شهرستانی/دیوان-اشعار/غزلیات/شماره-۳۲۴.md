---
title: >-
    شمارهٔ ۳۲۴
---
# شمارهٔ ۳۲۴

<div class="b" id="bn1"><div class="m1"><p>بیقراریهای عشق آیینه دار خوی دوست</p></div>
<div class="m2"><p>همچو گل می خندد از سیمای عاشق روی دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوخی جوهر ندارد خواب در شمشیر ناز</p></div>
<div class="m2"><p>می نماید راز عاشق از خم ابروی دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده بر روی شکفتن همچو گل وا می کند</p></div>
<div class="m2"><p>کاش دل هم یک گره می بود از گیسوی دوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روز روشن از پر پروانه می سازد چراغ</p></div>
<div class="m2"><p>گر نباشد غیرت عاشق نقاب روی دوست</p></div></div>