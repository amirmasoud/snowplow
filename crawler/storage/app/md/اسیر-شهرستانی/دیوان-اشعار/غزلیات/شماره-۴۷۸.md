---
title: >-
    شمارهٔ ۴۷۸
---
# شمارهٔ ۴۷۸

<div class="b" id="bn1"><div class="m1"><p>هر دل خبر از آینه دید ندارد</p></div>
<div class="m2"><p>هر ذره نسب نامه خورشید ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناکامی جاوید رسانید به کامم</p></div>
<div class="m2"><p>نومیدیم از وصل تو نومید ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تنهایی تنها نشود رهبر موری</p></div>
<div class="m2"><p>کثرت خبر از عالم توحید ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تنها نه غم خود غم یک قافله دارم</p></div>
<div class="m2"><p>غارتزده جز حسرت جاوید ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صاحبنظران چشم به راه دگرانند</p></div>
<div class="m2"><p>صد قبله نما هست و یکی دید ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در حیرتم از پرورش ابر مکافات</p></div>
<div class="m2"><p>بر تاک خطا رفت و ثمر بید ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همدرد اسیرم به تمنای تو عمری است</p></div>
<div class="m2"><p>دارم ز تو روزی که شب عید ندارد</p></div></div>