---
title: >-
    شمارهٔ ۷۷۳
---
# شمارهٔ ۷۷۳

<div class="b" id="bn1"><div class="m1"><p>کو جنون کز سنگ طفلان خانه‌ای پیدا کنم</p></div>
<div class="m2"><p>خواب راحت چون شرر در بستر خارا کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌روم تا از غبار خاطر و سیلاب اشک</p></div>
<div class="m2"><p>خنده بر سامان دشت و مایه دریا کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آرزو دارم که از اعجاز بخت واژگون</p></div>
<div class="m2"><p>او نماید لطف و من دانسته استغنا کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نمانده باده مستی غم ساقی کجاست</p></div>
<div class="m2"><p>آن نیم کز بهر جامی ترک این سودا کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهر بلبل منع گلبن می کنم از پای سرو</p></div>
<div class="m2"><p>گر چو قمری با گرفتاری سری پیدا کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی گشاید گوشه چشمی به سوی من اسیر</p></div>
<div class="m2"><p>گر چو بی مهری به چشم اختر خود جا کنم</p></div></div>