---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>لعلت زجام شیر و شکر می دهد مرا</p></div>
<div class="m2"><p>ساغر ز آبروی گوهر می دهد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساغر به طاق ابروی وحشت کشیده ام</p></div>
<div class="m2"><p>بیگانگی ز خویش خبر می دهد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقی ستم ظریف و می از شعله شوخ تر</p></div>
<div class="m2"><p>جامی نداده جامی دگر می دهد مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گردم به جستجوی تو پرواز می کند</p></div>
<div class="m2"><p>در خاک هم هوای تو پر می دهد مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر دیده باغبان بهار خیال خویش</p></div>
<div class="m2"><p>شبنم به جای خون جگر می دهد مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر ناله ای که کرد فراموش سینه ام</p></div>
<div class="m2"><p>پیغامی از زبان اثر می دهد مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیغام من شکنجه کش انتظار نیست</p></div>
<div class="m2"><p>قاصد نرفته شوق خبر می دهد مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر دیده بحر گوهر مقصود دامنم</p></div>
<div class="m2"><p>تا ناخدا نوید خبر می دهد مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد گرد عزلتم گل آوارگی اسیر</p></div>
<div class="m2"><p>حب وطن نوید سفر می دهد مرا</p></div></div>