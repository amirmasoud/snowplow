---
title: >-
    شمارهٔ ۳۱۲
---
# شمارهٔ ۳۱۲

<div class="b" id="bn1"><div class="m1"><p>نگاهش دورگرد آشنایی است</p></div>
<div class="m2"><p>تغافل بر سر صبرآزمایی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خون افتاده مرغ دام او را</p></div>
<div class="m2"><p>تپیدن بال پرواز رهایی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو دل باشد تسلی از خیالی</p></div>
<div class="m2"><p>امید وصل کافر ماجرایی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کند در غنچه پنهان نقد خود را</p></div>
<div class="m2"><p>میان بلبل و گل هم جدایی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اسیر از من چه می پرسی غم دل</p></div>
<div class="m2"><p>عنانم در کف آشفته رایی است</p></div></div>