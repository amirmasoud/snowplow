---
title: >-
    شمارهٔ ۳۸۴
---
# شمارهٔ ۳۸۴

<div class="b" id="bn1"><div class="m1"><p>گلستانی که هوای دل نومید گرفت</p></div>
<div class="m2"><p>باغبانش ثمر پیشرس از بید گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باده الفت سرشار قوامی دارد</p></div>
<div class="m2"><p>شبنم گریه ما دامن خورشید گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حلقه دام گرفتاری ما چشم غزال</p></div>
<div class="m2"><p>صید وحشت که به او الفت جاوید گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساغر از ساقی دوران چه گرفتن دارد</p></div>
<div class="m2"><p>این تنک حوصله جام از کف جمشید گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوخت در عشق بتان خجلت بیحاصلیم</p></div>
<div class="m2"><p>آتشین گریه ام از گل عرق بید گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سوخت پروانه ام از دوری و اجری می خواست</p></div>
<div class="m2"><p>جای در بزم چراغان شب عید گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل کجا بود که خاکستر نومیدی ما</p></div>
<div class="m2"><p>راه بر سرمه پرکاری امید گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر ز صحرای جنون رست چه غم دارد اسیر</p></div>
<div class="m2"><p>خار این بادیه تیغ از کف خورشید گرفت</p></div></div>