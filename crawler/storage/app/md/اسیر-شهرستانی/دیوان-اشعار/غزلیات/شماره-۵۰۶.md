---
title: >-
    شمارهٔ ۵۰۶
---
# شمارهٔ ۵۰۶

<div class="b" id="bn1"><div class="m1"><p>نه تنها صبر با کم آرزوی بیش نگذارد</p></div>
<div class="m2"><p>چو کامل شد جنون دل را به جای خویش نگذارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آتش یک نفس تا مانده سامان شرر دارد</p></div>
<div class="m2"><p>دلم را سرنوشت سوختن درویش نگذارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه رنگین صرفه ها بردند خلق از مردم آزاری</p></div>
<div class="m2"><p>نرنجاند گزیدن خاطری پا پیش نگذارد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محبت خون دلی گم کردنی گم کردنی دارد</p></div>
<div class="m2"><p>سری بی داغ سودا سینه ای بی ریش نگذارد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو وابینی بود هر خار گلشن هر شرر گلخن</p></div>
<div class="m2"><p>قناعت هیچ کس را در جهان درویش نگذارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نداری اعتمادی بر دل ما امتحان بهتر</p></div>
<div class="m2"><p>مگو تاراج عشقت خویش را درویش نگذارد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که می دارد نگه دیوانه صحرایی ما را</p></div>
<div class="m2"><p>اگر سودای زنجیر تو پایی پیش نگذارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اسیر از جاده زنجیر چشم حیرتی دارم</p></div>
<div class="m2"><p>که شوقم را به راه عقل دور اندیش نگذارد</p></div></div>