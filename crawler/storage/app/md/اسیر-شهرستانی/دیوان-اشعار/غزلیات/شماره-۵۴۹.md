---
title: >-
    شمارهٔ ۵۴۹
---
# شمارهٔ ۵۴۹

<div class="b" id="bn1"><div class="m1"><p>جایی که حکم ناله شبگیر می رسد</p></div>
<div class="m2"><p>غافل مشو که تیر پی تیر می رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی او نمی شود سر و برگ جنون درست</p></div>
<div class="m2"><p>دل را نسب به حلقه زنجیر می رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد بیستون حوصله جان سختی خمار</p></div>
<div class="m2"><p>فرهاد برق تیشه می دیر می رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گردم بهانه جو شده پرواز می کند</p></div>
<div class="m2"><p>گر زود می رسد به سرم دیر می رسد</p></div></div>