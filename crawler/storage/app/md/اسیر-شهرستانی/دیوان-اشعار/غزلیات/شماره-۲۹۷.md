---
title: >-
    شمارهٔ ۲۹۷
---
# شمارهٔ ۲۹۷

<div class="b" id="bn1"><div class="m1"><p>هر عارض افروخته مشاطه نازی است</p></div>
<div class="m2"><p>هر جنبش مژگان چمن آرای نیازی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلزار نسب نامه یاران عزیز است</p></div>
<div class="m2"><p>هر فاخته محمودی و هر سرو ایازی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک یک ورق صفحه ایجاد گشودیم</p></div>
<div class="m2"><p>هر صفحه بیکارتر آیینه رازی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آتش و آبم نفریبد گل و شبنم</p></div>
<div class="m2"><p>خضر ره من جلوه نظاره گدازی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل برد به صد رنگ و خبردار نگشتیم</p></div>
<div class="m2"><p>دلدار اسیر تو عجب شعبده بازی است</p></div></div>