---
title: >-
    شمارهٔ ۴۵۳
---
# شمارهٔ ۴۵۳

<div class="b" id="bn1"><div class="m1"><p>چو نیرنگ محبت چشم ارباب نظر بندد</p></div>
<div class="m2"><p>رگ مژگان گشاید سیل آتش در جگر بندد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان از تاب رشک دوستان بر خویش می پیچم</p></div>
<div class="m2"><p>که خون غیرتم چون رشته دست نیشتر بندد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو گل خاک شهید لعل او شادابیی دارد</p></div>
<div class="m2"><p>غبار تربت ما راه بر آب گهر بندد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>(بود) شمع بساط خاطرم بازیچه طفلی</p></div>
<div class="m2"><p>که بال بلبل و پروانه را بر یکدگر بندد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گدازد جلوه گلزار بویان چون قبا پوشد</p></div>
<div class="m2"><p>گشاید خاطر زنار مویان چون کمر بندد</p></div></div>