---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>روشنگر چشم و دل ما کن شب ما را</p></div>
<div class="m2"><p>صیقل نزند تیره دلی مطلب ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل شکر تو را از قلم شکوه نویسد</p></div>
<div class="m2"><p>باور نکند ساده دلی یا رب ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاید که تو یکبار ندانسته بخوانی</p></div>
<div class="m2"><p>آیینه کند نقش نگین مطلب ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتند سویدای دل صبح امید است</p></div>
<div class="m2"><p>دیدند چو در سوختگی کوکب ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاکستر پروانه سرشتند ز شبنم</p></div>
<div class="m2"><p>کردند بنا در دل ما مکتب ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مستی گل تقوی و ورع موج شراب است</p></div>
<div class="m2"><p>نشناخته تا مشرب ما مشرب ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از گلبن کثرت گل توحید بخندد</p></div>
<div class="m2"><p>گر آب کند نقش جبین مذهب ما را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیوانه اسیر از ته دل شکر خدا کن</p></div>
<div class="m2"><p>افزود ز صدق تو جنون مذهب ما را</p></div></div>