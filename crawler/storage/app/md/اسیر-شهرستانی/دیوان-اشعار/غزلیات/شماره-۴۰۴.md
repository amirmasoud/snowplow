---
title: >-
    شمارهٔ ۴۰۴
---
# شمارهٔ ۴۰۴

<div class="b" id="bn1"><div class="m1"><p>گه به درد و گه به داغ ما مپیچ</p></div>
<div class="m2"><p>اینقدرها در سراغ ما مپیچ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشئه گر سر جوش سودا نیستی</p></div>
<div class="m2"><p>پا به دامان ایاغ ما مپیچ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شعله را سرکار شبنم کرده ایم</p></div>
<div class="m2"><p>بوی گل در سیر باغ ما مپیچ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر سمندر گشته ای پروانه ای</p></div>
<div class="m2"><p>هرزه بر دود چراغ ما مپیچ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل نمی گنجد در این آشفته باغ</p></div>
<div class="m2"><p>نکهت گل بر دماغ ما مپیچ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درد می خیزد از این صحرای خشک</p></div>
<div class="m2"><p>هیچکس گو در سراغ ما مپیچ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتمت گفتم اسیر خام سوز</p></div>
<div class="m2"><p>رشته سودا به داغ ما مپیچ</p></div></div>