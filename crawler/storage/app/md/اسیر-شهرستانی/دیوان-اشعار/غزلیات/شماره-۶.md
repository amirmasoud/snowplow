---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>جنون نمی کند از خویشتن جدا ما را</p></div>
<div class="m2"><p>چه احتیاج به یاران آشنا ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر چه ساده خیالیم ساده لوح نه ایم</p></div>
<div class="m2"><p>کدام وعده چه دل دیده ای کجا ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کشیده تیغ و تغافل گرفته دامانش</p></div>
<div class="m2"><p>کجا شناخته آن ترک میرزا ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خجل ز همرهی مستی و خمار شدیم</p></div>
<div class="m2"><p>بگو چه رنگ برآرد دگر وفا ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر شویم نهان در غبار ساختگی</p></div>
<div class="m2"><p>سراغ می دهد از رنگ ما حیا ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی نداشت که سررشته را نگه دارد</p></div>
<div class="m2"><p>کرایه کرد زدیوانگی وفا ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنان به عربده سر می کند که پنداری</p></div>
<div class="m2"><p>خریده است جنون برهنه پا ما را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر اسیر دیار فرنگ هم گردیم</p></div>
<div class="m2"><p>نمی خرد کسی از دولت حیا ما را</p></div></div>