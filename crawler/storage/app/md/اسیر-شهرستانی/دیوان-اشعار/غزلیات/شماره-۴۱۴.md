---
title: >-
    شمارهٔ ۴۱۴
---
# شمارهٔ ۴۱۴

<div class="b" id="bn1"><div class="m1"><p>گشته تا صیاد ما مژگان شوخ</p></div>
<div class="m2"><p>کرده صید مدعا مژگان شوخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر نمی آید فلک با تیغ ناز</p></div>
<div class="m2"><p>دارد اقبال رسا مژگان شوخ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل عبادت می کند چشم مرا</p></div>
<div class="m2"><p>دیده ام نام خدا مژگان شوخ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نگاه آشنا برگشته های</p></div>
<div class="m2"><p>تا به سر دارد چها مژگان شوخ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می چکد بر شش جهت خون دلم</p></div>
<div class="m2"><p>دیده باشد تا کجا مژگان شوخ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داردم سرگشته بیداد اسیر</p></div>
<div class="m2"><p>آه از آن سر در هوا مژگان شوخ</p></div></div>