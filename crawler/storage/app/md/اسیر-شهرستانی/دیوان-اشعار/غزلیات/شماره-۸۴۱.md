---
title: >-
    شمارهٔ ۸۴۱
---
# شمارهٔ ۸۴۱

<div class="b" id="bn1"><div class="m1"><p>دارم ز کاوش مژه ات جان تازه ای</p></div>
<div class="m2"><p>وز چاک سینه طرح گریبان تازه ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد حشر و از غرور به دادم نمی رسی</p></div>
<div class="m2"><p>این ماجرا فتاده به دیوان تازه ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیرینه عندلیب گل داغ کهنه ام</p></div>
<div class="m2"><p>کی می خورم فریب گلستان تازه ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آتش پرست عشقم و از رغم کفر و دین</p></div>
<div class="m2"><p>دارم ز هر نگاه تو ایمان تازه ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در باغ دل اسیر ز تیغ نگاه او</p></div>
<div class="m2"><p>گل کرده است زخم نمایان تازه ای</p></div></div>