---
title: >-
    شمارهٔ ۹۸
---
# شمارهٔ ۹۸

<div class="b" id="bn1"><div class="m1"><p>چه غم دارد دل از اندیشه ما</p></div>
<div class="m2"><p>نظر از سنگ دارد شیشه ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه شد گر بیستون الماس باشد</p></div>
<div class="m2"><p>بود لخت دل ما تیشه ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمی دانیم رمز لن ترانی</p></div>
<div class="m2"><p>چه می گوید تغافل پیشه ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به صورت مور (و) در معنی چو شیریم</p></div>
<div class="m2"><p>نیستان ناله ها دل بیشه ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه شد نازکدلی اما غیوری</p></div>
<div class="m2"><p>گدازد سنگها را شیشه ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه شد گر صورت از معنی ندانیم</p></div>
<div class="m2"><p>سراسر حیرت است اندیشه ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمی ترسم اسیر از صرصر یأس</p></div>
<div class="m2"><p>سرشک آمیخت در خون ریشه ما</p></div></div>