---
title: >-
    شمارهٔ ۴۰۳
---
# شمارهٔ ۴۰۳

<div class="b" id="bn1"><div class="m1"><p>مگشای پیش هر کس بار فسانه چون موج</p></div>
<div class="m2"><p>بند زبان ندارند اهل زمانه چون موج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی بیمی از شکستن دارد به بحر هستی</p></div>
<div class="m2"><p>تا کشتیی نگردد صید کرانه چون موج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خویش اگر گذشتیم راه برون شدن هست</p></div>
<div class="m2"><p>بحر شهادت است این ما در میانه چون موج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما را غبار خاطر در بحر گریه سر داد</p></div>
<div class="m2"><p>او در کرانه چون دشت ما در میانه چون موج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در بر و بحر عالم چون دل مسافری کو</p></div>
<div class="m2"><p>بی آب و دانه چون ما بی آشیانه چون موج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در بحر آشنایی تا دیده می گشایم</p></div>
<div class="m2"><p>اشکم کند به راهی هر دم روانه چون موج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا کی به بحر هستی در بند هیچ بودن</p></div>
<div class="m2"><p>بشکن به زور بازو زنجیر خانه چون موج</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دریا اسیر گردد از بیقراری من</p></div>
<div class="m2"><p>تیغش همان به خونم دارد زبانه چون موج</p></div></div>