---
title: >-
    شمارهٔ ۴۵۷
---
# شمارهٔ ۴۵۷

<div class="b" id="bn1"><div class="m1"><p>از تماشای رخت شام و سحر می خندد</p></div>
<div class="m2"><p>در تمنای لبت پسته شکر می خندد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد غبارم ز شکر خندهای اکسیر نفس</p></div>
<div class="m2"><p>می شوم زنده اگر بار دگر می خندد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جور بینی اگر از وصل نشان می خواهی</p></div>
<div class="m2"><p>گل مقصود به گلزار خطر می خندد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گریه ام خنده نما خنده ام اندوه فزا</p></div>
<div class="m2"><p>هر گل باغ جنون رنگ دگر می خندد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خنده خوشدلی ارزانی ناقص طربان</p></div>
<div class="m2"><p>خرم آن است که با دیده تر می خندد</p></div></div>