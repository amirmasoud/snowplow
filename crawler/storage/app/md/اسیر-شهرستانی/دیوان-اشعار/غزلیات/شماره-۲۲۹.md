---
title: >-
    شمارهٔ ۲۲۹
---
# شمارهٔ ۲۲۹

<div class="b" id="bn1"><div class="m1"><p>حرف بی صرفه و بیتابی اظهار کم است</p></div>
<div class="m2"><p>بوی این باده پر و ساغر سرشار کم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاطر چاره گران زحمت درمان می شد</p></div>
<div class="m2"><p>من و آن درد که در عهد تو بسیار کم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دل از دست تو آخر به جلا خواهم زد</p></div>
<div class="m2"><p>چه بگویم که در اقلیم سخن عار کم است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من هم از شوخی پرواز گلی می چیدم</p></div>
<div class="m2"><p>چه کنم خنده بیدردی گلزار کم است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شیشه ام بوی گلاب از گل سنگی نکشید</p></div>
<div class="m2"><p>دل چه منت کشد از عیش چو آزار کم است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منت از غیرت بی مطلبیش می سوزد</p></div>
<div class="m2"><p>به گرانقدری دیوانه سبکسار کم است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شده آیینه این بی خبران عیش جهان</p></div>
<div class="m2"><p>باده آن زور ندارد دل هشیار کم است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گشت پامال تماشا دل و حسرت باقی است</p></div>
<div class="m2"><p>سوخت بازار و همان گرمی بازار کم است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سرو از تربیت سایه گل خاست اسیر</p></div>
<div class="m2"><p>سفر نشو و نما بی تعب خار کم است</p></div></div>