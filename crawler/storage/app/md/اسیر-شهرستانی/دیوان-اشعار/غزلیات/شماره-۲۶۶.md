---
title: >-
    شمارهٔ ۲۶۶
---
# شمارهٔ ۲۶۶

<div class="b" id="bn1"><div class="m1"><p>از اشک و آه من گل و سنبل شکفته است</p></div>
<div class="m2"><p>از ناله ام ترانه بلبل شکفته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیر خزان تنگدلی کرده ایم ما</p></div>
<div class="m2"><p>آن کس بهار کرده که گل گل شکفته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکم نگاهیش ز دلم برده حیرتی</p></div>
<div class="m2"><p>این غنچه در بهار تغافل شکفته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشتر خلد به دیده خصم از غبار ما</p></div>
<div class="m2"><p>گلها ز فیض خار تحمل شکفته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در نوبهار گریه ما بی رخش اسیر</p></div>
<div class="m2"><p>نه غنچه خنده کرد و نه گل شکفته است</p></div></div>