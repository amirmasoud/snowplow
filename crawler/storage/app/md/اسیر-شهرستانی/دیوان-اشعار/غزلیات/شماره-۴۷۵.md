---
title: >-
    شمارهٔ ۴۷۵
---
# شمارهٔ ۴۷۵

<div class="b" id="bn1"><div class="m1"><p>گل خیال که در آب و آتشم دارد</p></div>
<div class="m2"><p>که یاد خنده غفلت مشوشم دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر غبار نگردم غبار خواهم شد</p></div>
<div class="m2"><p>به دام شوخی جولان ابرشم دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهید جلوه شمشیر گشتنم بس نیست</p></div>
<div class="m2"><p>هلاک شوخی پرواز ترکشم دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عداوتم گل خجلت به بار می آرد</p></div>
<div class="m2"><p>ز درد کینه دل پاک بی غشم دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز کینه ام دل شمشیر آرمید و هنوز</p></div>
<div class="m2"><p>خیال ابروی او درکشاکشم دارد</p></div></div>