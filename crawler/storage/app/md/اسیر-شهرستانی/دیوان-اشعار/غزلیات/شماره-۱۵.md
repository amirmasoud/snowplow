---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>به دل دوزد نگاهت سینه‌ها را</p></div>
<div class="m2"><p>به گل گیرد رخت آیینه‌ها را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهار سینه‌صافی بی‌خزان‌تر</p></div>
<div class="m2"><p>ز دل روبد غبار کینه‌ها را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیا زاهد که مست سجده یابی</p></div>
<div class="m2"><p>به پای خُم شب آدینه‌ها را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نخستین گام سر باید نهادن</p></div>
<div class="m2"><p>که بالا می‌رود این زینه‌ها را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اسیر دور گردم می‌توانم</p></div>
<div class="m2"><p>که نامحرم کنم دیرینه‌ها را</p></div></div>