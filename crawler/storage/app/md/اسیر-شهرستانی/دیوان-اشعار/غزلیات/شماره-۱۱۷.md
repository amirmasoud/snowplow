---
title: >-
    شمارهٔ ۱۱۷
---
# شمارهٔ ۱۱۷

<div class="b" id="bn1"><div class="m1"><p>دوزخ اقلیم خودنمایی‌ها</p></div>
<div class="m2"><p>جان مرحله برهنه‌پایی‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسیار ز جانب وفاکیشان</p></div>
<div class="m2"><p>شرمنده شدم ز آشنایی‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوابی است تمام عمر در عالم</p></div>
<div class="m2"><p>تعبیرش داغ آشنایی‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از عالم راه و رسم بیزارند</p></div>
<div class="m2"><p>هم شهری و طرز روستایی‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بخت سیه امیدها دارم</p></div>
<div class="m2"><p>در تاریکی است روشنایی‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاجز شده (ام) ز شکر نومیدی</p></div>
<div class="m2"><p>رابح گشتم ز ناروایی‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شایسته امتیاز گردیدم</p></div>
<div class="m2"><p>دیدم از بس که خودستایی‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیدیم اسیر در گرفتاری</p></div>
<div class="m2"><p>فارغ نخورد غم رهایی‌ها</p></div></div>