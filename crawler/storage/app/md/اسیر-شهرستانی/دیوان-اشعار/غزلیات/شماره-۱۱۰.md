---
title: >-
    شمارهٔ ۱۱۰
---
# شمارهٔ ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>زنجیری طره‌ات ختن‌ها</p></div>
<div class="m2"><p>سودایی جلوه‌ات چمن‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فریاد که دم نمی‌توان زد</p></div>
<div class="m2"><p>بی‌یاد تو هیچ ما و من‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دارم ز تو لعل پاره داغ</p></div>
<div class="m2"><p>بیرون ز شمار انجمن‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاموشی‌ها زبان‌درازی</p></div>
<div class="m2"><p>در پرده نگنجد این سخن‌ها</p></div></div>