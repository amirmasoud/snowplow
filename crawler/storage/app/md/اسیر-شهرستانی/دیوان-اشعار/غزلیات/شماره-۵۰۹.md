---
title: >-
    شمارهٔ ۵۰۹
---
# شمارهٔ ۵۰۹

<div class="b" id="bn1"><div class="m1"><p>خواب اگر پی به سرگریه شبگیر آرد</p></div>
<div class="m2"><p>صبح را بهر شفاعت به چه تدبیر آرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سبزه شد دود چراغ دل و بیداد هنوز</p></div>
<div class="m2"><p>بر سرخاک منش دست به شمشیر آرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون سراسیمه نباشم که به هر گردش چشم</p></div>
<div class="m2"><p>صیدی از سایه مژگان به سر تیر آرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیرگاهش لب جوی است و گلش سایه ابر</p></div>
<div class="m2"><p>جز جنون آب و هوا را که به زنجیر آرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر کند نشتر فصاد خیال مژه ات</p></div>
<div class="m2"><p>خون افسرده برون از رگ تصویر آرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشربم بین که به بزم عسس توبه اسیر</p></div>
<div class="m2"><p>می خورم خون قدحی ساقی اگر دیر آرد</p></div></div>