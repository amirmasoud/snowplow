---
title: >-
    شمارهٔ ۶۱۲
---
# شمارهٔ ۶۱۲

<div class="b" id="bn1"><div class="m1"><p>از جان که می شنید اگر حرف غم نبود</p></div>
<div class="m2"><p>از دل چه می کشید کسی گر ستم نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرمنده تغافل و ناز و کرشمه ایم</p></div>
<div class="m2"><p>چشم تو آنچه در حق ما کرد کم نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تقریب شکوه ای چو فراق تو داشتیم</p></div>
<div class="m2"><p>ممنون خامه ام که شکایت رقم نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای توبه خون خوری که کدوی شراب ما</p></div>
<div class="m2"><p>خاکش ز خون ساغر جمشید کم نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرهم طراوت گل باغ جراحت است</p></div>
<div class="m2"><p>بی خنده شکفتگی امید غم نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ته جرعه بهار بود زحمت خزان</p></div>
<div class="m2"><p>شادی اگر نبود نشان الم نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از سر نمود قطع بیابان غم اسیر</p></div>
<div class="m2"><p>این سرزمین قلمرو نقش قدم نبود</p></div></div>