---
title: >-
    شمارهٔ ۶۴۲
---
# شمارهٔ ۶۴۲

<div class="b" id="bn1"><div class="m1"><p>بوالهوس لاف محبت زد و آزار کشید</p></div>
<div class="m2"><p>کور دل صورت آیینه به دیوار کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شور دیوانگیم درس محبت آموخت</p></div>
<div class="m2"><p>کارم از خدمت زنجیر به زنار کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قطره خون شد و در دیده حیرت جا کرد</p></div>
<div class="m2"><p>هر گلابی که دلم زان گل رخسار کشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواب شیرین اجل هم نکند مخمورش</p></div>
<div class="m2"><p>از لبت هر که می تلخ به گفتار کشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل چو در سینه تپد آفت راز است اسیر</p></div>
<div class="m2"><p>لب خاموشی تو بدنامی گفتار کشید</p></div></div>