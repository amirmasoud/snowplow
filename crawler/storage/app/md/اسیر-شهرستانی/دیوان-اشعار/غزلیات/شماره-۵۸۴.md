---
title: >-
    شمارهٔ ۵۸۴
---
# شمارهٔ ۵۸۴

<div class="b" id="bn1"><div class="m1"><p>فریب جلوه مستانه دادند</p></div>
<div class="m2"><p>مرا از نقش پا پیمانه دادند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در صحرا به روی ما گشادند</p></div>
<div class="m2"><p>کلید خانه دیوانه دادند؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چرا آواره عالم نباشم</p></div>
<div class="m2"><p>مرا در سایه دل خانه دادند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهشت ارزانی مطلب پرستان</p></div>
<div class="m2"><p>به رندان گوشه میخانه دادند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حریفان با خودش یکدل ندیدند</p></div>
<div class="m2"><p>به زاهد سبحه صد دانه دادند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دو عالم را به یک آتش نشاندند</p></div>
<div class="m2"><p>به بلبل هم پر پروانه دادند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سجود آشنایی کن دلی را</p></div>
<div class="m2"><p>که ذوق صحبت بیگانه دادند</p></div></div>