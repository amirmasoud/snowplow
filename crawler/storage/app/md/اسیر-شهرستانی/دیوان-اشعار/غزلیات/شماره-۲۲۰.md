---
title: >-
    شمارهٔ ۲۲۰
---
# شمارهٔ ۲۲۰

<div class="b" id="bn1"><div class="m1"><p>آن چهره که خورشید غلام است کدام است</p></div>
<div class="m2"><p>آن صبح که مشاطه شام است کدام است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دریوزه یک مسئله مشکلم این است</p></div>
<div class="m2"><p>طفلی که در ارشاد تمام است کدام است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای باده کشان مطلب دیوانه سؤالی است</p></div>
<div class="m2"><p>آن می که حلال است و حرام است کدام است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سروی که بود سایه او تاج سرما</p></div>
<div class="m2"><p>تا چند بپرسید کدام است کدام است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیوانه اسیر تو سراسیمه حیرت</p></div>
<div class="m2"><p>آن جلوه کز او کار به کام است کدام است</p></div></div>