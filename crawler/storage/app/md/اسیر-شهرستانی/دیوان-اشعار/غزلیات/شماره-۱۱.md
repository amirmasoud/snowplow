---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>هوا گلشن به گلشن می کشد دیوانه ما را</p></div>
<div class="m2"><p>زخون توبه موج گل کند پیمانه ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرابت دام می چیند تغافل جام می بخشد</p></div>
<div class="m2"><p>به ما گر واگذارد دل وفا بیگانه ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپند چشم بدکام دو عالم می توان کردن</p></div>
<div class="m2"><p>شرابت می پرستد گریه مستانه ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز رویت شعله ها گلشن ز خویت شمع ها روشن</p></div>
<div class="m2"><p>تماشا برگ گل سازد پر پروانه ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خرابی صندل درد سر تعمیر عالم شد</p></div>
<div class="m2"><p>به سیل امتحان تا کی دهی ویرانه ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز وحشت باج می گیرد به الفت تاج می بخشد</p></div>
<div class="m2"><p>بیا شرمنده حسرت مکن دیوانه ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اسیر آن طفل بدخو رام آسایش نمی گردد</p></div>
<div class="m2"><p>مبادا بشنود در خواب هم افسانه ما را</p></div></div>