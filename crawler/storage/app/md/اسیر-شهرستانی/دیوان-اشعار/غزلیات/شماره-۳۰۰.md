---
title: >-
    شمارهٔ ۳۰۰
---
# شمارهٔ ۳۰۰

<div class="b" id="bn1"><div class="m1"><p>دو جهان از کتاب دل ورقی است</p></div>
<div class="m2"><p>زندگی سطری و فنا سبقی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تب رشک که سوخت گلشن را</p></div>
<div class="m2"><p>غنچه تبخاله ای و گل عرقی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل خورشید غنچه می خندد</p></div>
<div class="m2"><p>صبح بی باده شام بی شفقی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هنر از عیب می توان دانست</p></div>
<div class="m2"><p>عالم آیینه خانه سبقی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می گریزم اسیر از آزادی</p></div>
<div class="m2"><p>دل بی داغ لکه بهقی است؟</p></div></div>