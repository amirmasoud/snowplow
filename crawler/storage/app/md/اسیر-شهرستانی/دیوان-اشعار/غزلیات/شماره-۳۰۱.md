---
title: >-
    شمارهٔ ۳۰۱
---
# شمارهٔ ۳۰۱

<div class="b" id="bn1"><div class="m1"><p>بازم از مژگان شوخی خارخار عاشقی است</p></div>
<div class="m2"><p>دل درون سینه ام از انتظار عاشقی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سبزه خط گر دمید از گلشن حسنت چه غم</p></div>
<div class="m2"><p>بیقراران رخت را نوبهار عاشقی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوشدلی ارزانی یاران فارغبال باد</p></div>
<div class="m2"><p>من دل خرم نمی خواهم که عار عاشقی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان وفا دشمن جفا چندانکه بینی صبرکن</p></div>
<div class="m2"><p>اینچنین می باشد ای دل کار و بار عاشقی است</p></div></div>