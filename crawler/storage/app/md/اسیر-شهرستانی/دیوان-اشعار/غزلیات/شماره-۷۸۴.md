---
title: >-
    شمارهٔ ۷۸۴
---
# شمارهٔ ۷۸۴

<div class="b" id="bn1"><div class="m1"><p>پیمان توبه در صف مستان شکسته ایم</p></div>
<div class="m2"><p>پیمانه ای بیار که پیمان شکسته ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حیرت دلیل و کعبه مقصود دور و ما</p></div>
<div class="m2"><p>در پای سعی خار مغیلان شکسته ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن نخل تازه ایم که از تندباد غم</p></div>
<div class="m2"><p>سر تا قدم چو زلف پریشان شکسته ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از ضعف طالع است که بر روی روزگار</p></div>
<div class="m2"><p>پیوسته همچو رنگ اسیران شکسته ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای عندلیب از چه شدی خصم جان ما</p></div>
<div class="m2"><p>شاخ گلی مگر زگلستان شکسته ایم</p></div></div>