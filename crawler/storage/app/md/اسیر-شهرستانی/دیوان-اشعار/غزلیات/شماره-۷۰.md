---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>جنون کو تا نثار دل کنم آشفته رایی را</p></div>
<div class="m2"><p>زعریانی لباس تازه بخشم خود نمایی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورد نیش آنکه تأثیر محبت از هوس جوید</p></div>
<div class="m2"><p>به شهد موم کی بخشند نفع مومیایی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شوم نومیدتر چندانکه بینم بیشتر سویش</p></div>
<div class="m2"><p>تماشا پرده پوشد جلوه حسن خدایی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر ننمود عارض دیده بیدار می باید</p></div>
<div class="m2"><p>درون پرده دارد حسن شوخش خود نمایی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به بازار وفا گر خود فروشان را گذار افتد</p></div>
<div class="m2"><p>به نرخ کیمیا گیرند جنس ناروایی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اجل هم جان به منت می گرفت از کشته نازت</p></div>
<div class="m2"><p>گر از چشم تو می آموخت کافر ماجرایی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تغافلهای چشمش از شراب لطف خالی نیست</p></div>
<div class="m2"><p>به مستی می دهد پیمانه صبر آزمایی را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به دام عشق هر نقش پر من چشم بیداری است</p></div>
<div class="m2"><p>نبینم تا ابد خواب پریشان رهایی را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اسیر از رغم زاهد ساغر سرشار می خواهد</p></div>
<div class="m2"><p>که موج باده شوید سرنوشت پارسایی را</p></div></div>