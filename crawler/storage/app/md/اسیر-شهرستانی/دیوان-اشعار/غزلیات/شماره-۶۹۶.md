---
title: >-
    شمارهٔ ۶۹۶
---
# شمارهٔ ۶۹۶

<div class="b" id="bn1"><div class="m1"><p>تا به کس روشن نسازم کفر ایمان بار خویش</p></div>
<div class="m2"><p>همچو شمع از خلق پنهان کرده ام زنار خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فیض دست آموز دارد ناخن موج سرشک</p></div>
<div class="m2"><p>هر گره کز دل گشایم می زنم در کار خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرنوشت طالعم تا گشته بخت واژگون</p></div>
<div class="m2"><p>گر کنم آزار دشمن می کنم آزار خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وصل جاوید خیال از آفت هجر ایمن است</p></div>
<div class="m2"><p>کی توان سوخت ما را بی گل رخسار خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نگاه گرم او گردد خریدار نیاز</p></div>
<div class="m2"><p>عشق می سوزد سپند از گرمی بازار خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باغبان گلشن انصاف را نازم اسیر</p></div>
<div class="m2"><p>کز پر بلبل کند خار سر دیوار خویش</p></div></div>