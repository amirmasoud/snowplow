---
title: >-
    شمارهٔ ۶۷۶
---
# شمارهٔ ۶۷۶

<div class="b" id="bn1"><div class="m1"><p>ای برای دلم سراپا ناز</p></div>
<div class="m2"><p>نیم ناز تو را هزار نیاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رشته های امید زود گسل</p></div>
<div class="m2"><p>عمر کوته کند امید دراز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیر در عالم گرفتاری است</p></div>
<div class="m2"><p>با قفس می کنیم ما پرواز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل من با خیال رخسارت</p></div>
<div class="m2"><p>گلشن راز و معنی اعجاز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با دل ما چه مدعا دارد</p></div>
<div class="m2"><p>چون نپرسی زغمزه غماز</p></div></div>