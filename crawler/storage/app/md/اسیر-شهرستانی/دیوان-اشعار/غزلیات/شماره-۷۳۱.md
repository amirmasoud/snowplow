---
title: >-
    شمارهٔ ۷۳۱
---
# شمارهٔ ۷۳۱

<div class="b" id="bn1"><div class="m1"><p>ز شور عشق هرگز تلخ از شیرین ندانستم</p></div>
<div class="m2"><p>دل آسوده را از خاطر غمگین ندانستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دمی با خواب راحت دیده من آشنا گردید</p></div>
<div class="m2"><p>که چون جوهر به غیر تیغ او بالین ندانستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا آیینه دل خضر راه سینه صافی شد</p></div>
<div class="m2"><p>که هرگز در محبت جبهه پر چین ندانستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نکردم فرق زخم تیغ و مرهم در گرفتاری</p></div>
<div class="m2"><p>ز فیض ساده لوحی مهر را از کین ندانستم</p></div></div>