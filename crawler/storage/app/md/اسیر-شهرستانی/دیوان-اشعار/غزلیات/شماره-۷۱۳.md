---
title: >-
    شمارهٔ ۷۱۳
---
# شمارهٔ ۷۱۳

<div class="b" id="bn1"><div class="m1"><p>جلوه حسنت چمن پرداز گل</p></div>
<div class="m2"><p>خنده گل شوخی گل ناز گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رتبه شوخی ز رعنایی گذشت</p></div>
<div class="m2"><p>جلوه شمشاد پا انداز گل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با خیالت سینه ها گلزارها</p></div>
<div class="m2"><p>می توان از دل شنید آواز گل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گریه ها در خنده پنهان کرده است</p></div>
<div class="m2"><p>از لب ساغر کشیدم راز گل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گریه می آید مرا بر عندلیب</p></div>
<div class="m2"><p>دیده ام تا خنده غماز گل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما و گلزار اطاعت پیشگی</p></div>
<div class="m2"><p>می کشم از خار دست انداز گل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسته ام دل بر تماشای اسیر</p></div>
<div class="m2"><p>داده ام آیینه را پرداز گل</p></div></div>