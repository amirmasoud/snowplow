---
title: >-
    شمارهٔ ۵۶۶
---
# شمارهٔ ۵۶۶

<div class="b" id="bn1"><div class="m1"><p>در محبت اشک و آه بی محابا را چه شد</p></div>
<div class="m2"><p>دل اگر گم گشت سامان دل ما را چه شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکزبانی سینه صافیهای دنیا پیشکش</p></div>
<div class="m2"><p>زود گرمیهای دیر اهل دنیا را چه شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هستی ما بیشتر از نیستی مجذوب بود</p></div>
<div class="m2"><p>عشق دنیا را چه شد افسوس عقبا را چه شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشت ما را آفت یک مور برق خرمن است</p></div>
<div class="m2"><p>دانه ما سبزه ما حاصل ما را چه شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غم ندارد غنچه های خاطر آشفتگی</p></div>
<div class="m2"><p>اشک رنگین بهار حسرت آرا چه شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عمرها تعبیر یک خواب پریشان دل است</p></div>
<div class="m2"><p>کس نمی پرسد که آشوب سراپا را چه شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاطر از خونگرمی افسرده یاران گداخت</p></div>
<div class="m2"><p>بیکسی ها بیکسی ها بیکسی ها را چه شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیوفایی برطرف نامهربانی برطرف</p></div>
<div class="m2"><p>ای دعا محو تأثیر دعاها را چه شد</p></div></div>