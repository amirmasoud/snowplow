---
title: >-
    شمارهٔ ۱۹۴
---
# شمارهٔ ۱۹۴

<div class="b" id="bn1"><div class="m1"><p>فیض نومیدی از امید مروت بیش است</p></div>
<div class="m2"><p>اجر ناکامی از اندازه حسرت بیش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جرم ناکرده ما را به سلامی بخشند</p></div>
<div class="m2"><p>گل این باغ ز دامان شفاعت بیش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شوخی می، سر رسوایی مستان دارد</p></div>
<div class="m2"><p>هر که را حوصله بیش است خجالت بیش است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر چه ننوشته ام از کوتهی مضمون پرس</p></div>
<div class="m2"><p>شکوه هجر ز طومار شکایت بیش است</p></div></div>