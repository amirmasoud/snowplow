---
title: >-
    شمارهٔ ۶۹۷
---
# شمارهٔ ۶۹۷

<div class="b" id="bn1"><div class="m1"><p>بیکسیم ساخت خریدار خویش</p></div>
<div class="m2"><p>سوختم از گرمی بازار خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکجهتی حلقه دام وفاست</p></div>
<div class="m2"><p>صید بتانیم و گرفتار خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک رهم سیر جهان می کنم</p></div>
<div class="m2"><p>همسفر شوق سبکبار خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جلوه غربت ز وطن دیده ام</p></div>
<div class="m2"><p>آینه ام سایه دیوار خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر دو جهان خواب فراموشیم</p></div>
<div class="m2"><p>منتم از دیده بیدار خویش</p></div></div>