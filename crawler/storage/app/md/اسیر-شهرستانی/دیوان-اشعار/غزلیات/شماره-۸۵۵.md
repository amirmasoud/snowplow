---
title: >-
    شمارهٔ ۸۵۵
---
# شمارهٔ ۸۵۵

<div class="b" id="bn1"><div class="m1"><p>و ما فیهما غیر جذب الوفا شیء</p></div>
<div class="m2"><p>خوشا حال مجنون خوشا وادی حی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گذشتیم شبهای آدینه از جام</p></div>
<div class="m2"><p>نصیحت تراشان دیرینه هی هی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه قاصد که تا خامه ای می کنم سر</p></div>
<div class="m2"><p>دلم صد خبر می رساند پیاپی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل تازه و سرو گلزار روحند</p></div>
<div class="m2"><p>خوشا نغمه دف خوشا ناله نی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلای شب جمعه واگشته از سر</p></div>
<div class="m2"><p>خماریم ساقی بده می بده می</p></div></div>