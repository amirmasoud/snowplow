---
title: >-
    شمارهٔ ۳۶۸
---
# شمارهٔ ۳۶۸

<div class="b" id="bn1"><div class="m1"><p>سبزه ما کی ز برق خرمنی اندیشه داشت</p></div>
<div class="m2"><p>گر دمید از آب حیوان آتشی در ریشه داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در شکست توبه کار عشرت ما شد درست</p></div>
<div class="m2"><p>آسمان ما را گرفتار طلسم شیشه داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوهکن در زیر بار ننگ مزدوری نبود</p></div>
<div class="m2"><p>خونبهای صد چو خسرو از شرار تیشه داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوش زاهد را چو ساغر اختیار از دست رفت</p></div>
<div class="m2"><p>زان ید بیضا که می در آستین شیشه داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از نگاه گرم فهمیدم که با من چشم او</p></div>
<div class="m2"><p>باده بد مستیی در ساغر اندیشه داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون کنم با طعنه دشمن که کوه سخت جان</p></div>
<div class="m2"><p>صد جراحت بر دل از تیغ زبان تیشه داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهره ور می شد ز گوهرهای نظم خود اسیر</p></div>
<div class="m2"><p>گر طریق این سخنور زان شاعر پیشه داشت</p></div></div>