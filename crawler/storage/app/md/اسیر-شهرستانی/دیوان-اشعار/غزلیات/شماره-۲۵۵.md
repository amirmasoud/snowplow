---
title: >-
    شمارهٔ ۲۵۵
---
# شمارهٔ ۲۵۵

<div class="b" id="bn1"><div class="m1"><p>آهم از بس که آتشین است</p></div>
<div class="m2"><p>با ناله من اثر قرین است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه عقل به من گذاشت نه دین</p></div>
<div class="m2"><p>چشمت که بلای عقل و دین است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در مصحف عارضت به خوبی</p></div>
<div class="m2"><p>ابروی تو آیت مبین است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن خال سیه نگر که چون مور</p></div>
<div class="m2"><p>در مزرع حسن خوشه چین است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>افسوس که وصل دلبران را</p></div>
<div class="m2"><p>خصمی چون هجر در کمین است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان می کنمت نثار امشب</p></div>
<div class="m2"><p>آن نقد که حاضر است این است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیگانه ننگ و نام گشتم</p></div>
<div class="m2"><p>خاصیت عشق اسیر این است</p></div></div>