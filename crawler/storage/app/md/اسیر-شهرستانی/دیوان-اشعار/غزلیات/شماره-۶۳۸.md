---
title: >-
    شمارهٔ ۶۳۸
---
# شمارهٔ ۶۳۸

<div class="b" id="bn1"><div class="m1"><p>یاد چشمت چو پی غارت جان می آید</p></div>
<div class="m2"><p>خواب و آرام به تاراج فغان می آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امتحان دل خود کردم و حالش دیدم</p></div>
<div class="m2"><p>می رود هر که ز کوی تو به جان می آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نیامد به سرخاک من آن گل نشکفت</p></div>
<div class="m2"><p>که بهاری به تماشای خزان می آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محرم شرح جدایی نبود هستی ما</p></div>
<div class="m2"><p>نامه ام سوی تو با قاصد جان می آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا ز جولان تو برخاست غبار از خاکم</p></div>
<div class="m2"><p>از گریبان صبا بوی فغان می آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسکه از نسبت آن رخ به نزاکت آمیخت</p></div>
<div class="m2"><p>عکس بر خاطر آیینه گران می آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کس گل از غنچه تصویر نچیده است اسیر</p></div>
<div class="m2"><p>راز بیگانه دل کی به زبان می آید</p></div></div>