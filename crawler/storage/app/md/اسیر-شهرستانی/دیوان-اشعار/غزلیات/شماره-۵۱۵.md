---
title: >-
    شمارهٔ ۵۱۵
---
# شمارهٔ ۵۱۵

<div class="b" id="bn1"><div class="m1"><p>رنگی که حسن از دل نومید می برد</p></div>
<div class="m2"><p>عشق از برای آینه دید می برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاهد که دوزخ از دمش افسرده خاطر است</p></div>
<div class="m2"><p>نام بهشت را به چه امید می برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکس به قدر بار سبکبار می شود</p></div>
<div class="m2"><p>دنیا پرست حسرت جاوید می برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سامان نشتر از دل ما کم نمی شود</p></div>
<div class="m2"><p>این قطره نم ز چشمه خورشید می برد</p></div></div>