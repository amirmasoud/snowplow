---
title: >-
    شمارهٔ ۶۲۲
---
# شمارهٔ ۶۲۲

<div class="b" id="bn1"><div class="m1"><p>از شکستن دل ما رام تظلم نشود</p></div>
<div class="m2"><p>هر چه خواهد بشود صید ترحم نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من و آن همت سرشار که گر خاک خورد</p></div>
<div class="m2"><p>تشنه خون دل مرده مردم نشود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر انصاف سلامت که جگر گوشه ماست</p></div>
<div class="m2"><p>در هم از خصمی دانسته مردم نشود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کرده ام ترک فراموشی دیرینه خویش</p></div>
<div class="m2"><p>کز دلم دشمنی اهل وفا گم نشود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر ز سر چشمه سیراب قناعت جوشد</p></div>
<div class="m2"><p>گریه ای نیست که سرمایه انجم نشود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خون افسردگی از برگ و برش جوش زند</p></div>
<div class="m2"><p>تاک اگر برق سوار سفر خم نشود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در دیاری که از او نشو و نما یافت اسیر</p></div>
<div class="m2"><p>حاکمی نیست که محکوم تحکم نشود</p></div></div>