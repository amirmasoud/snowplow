---
title: >-
    شمارهٔ ۶۶۶
---
# شمارهٔ ۶۶۶

<div class="b" id="bn1"><div class="m1"><p>زنجیر شوق ما شده روز و شب دگر</p></div>
<div class="m2"><p>بخت دگر سپهر دگر کوکب دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاد دعا نکرده ریاض اثر شکفت</p></div>
<div class="m2"><p>از فیض دل نماند مرا مطلب دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حرفی به گوش سیلی استاد می کشم</p></div>
<div class="m2"><p>پر کرده ام کتاب دل از مکتب دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یاران طبیب ساقی و ساقی طبیب ما</p></div>
<div class="m2"><p>جان در خمار دیگر و دل در تب دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صید اثر شکاری وحشی نگاه کیست</p></div>
<div class="m2"><p>هر لحظه بیخودانه کنم یا رب دگر</p></div></div>