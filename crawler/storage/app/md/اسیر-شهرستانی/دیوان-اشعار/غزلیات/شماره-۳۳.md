---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>سرمه حیرتش اکسیر نگاه است مرا</p></div>
<div class="m2"><p>سایه گل به نظر چشم سیاه است مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسکه گشتم به چمن محو خرام تو چو آب</p></div>
<div class="m2"><p>سبزه هر لب جو طرف کلاه است مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دارم از همت داغ تو جهان زیر نگین</p></div>
<div class="m2"><p>سرمه سوختگی گرد سپاه است مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تربیت یافته دود دلم همچو شرار</p></div>
<div class="m2"><p>گلستان جلوه این ابرسیاه است مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل بدآموز شکایت شده بیهوده اسیر</p></div>
<div class="m2"><p>هیچ کس نیست که پرسد چه گناه است مرا</p></div></div>