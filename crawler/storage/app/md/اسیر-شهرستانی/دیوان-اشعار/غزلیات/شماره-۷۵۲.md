---
title: >-
    شمارهٔ ۷۵۲
---
# شمارهٔ ۷۵۲

<div class="b" id="bn1"><div class="m1"><p>سراپا یکدلم درد تمنای کسی دارم</p></div>
<div class="m2"><p>همه تن دیده ام شغل تماشای کسی دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر گستاخم ای فرزانگان معذور داریدم</p></div>
<div class="m2"><p>ز بی پروایی آن مه چه پروای کسی دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگاهم گرده گلزارها در آستین دارد</p></div>
<div class="m2"><p>سواد بینش از خاک کف پای کسی دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تمنا کشته تیرش تغافل زخم شمشیرش</p></div>
<div class="m2"><p>ندانم اینقدر دانم که سودای کسی دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غبار من اسیر از سرکشی برخاک ننشیند</p></div>
<div class="m2"><p>مگر در سر هوای سرو بالای کسی دارم</p></div></div>