---
title: >-
    شمارهٔ ۳۸۲
---
# شمارهٔ ۳۸۲

<div class="b" id="bn1"><div class="m1"><p>قرب شاهان بی صدف کی گوهر شهوار یافت</p></div>
<div class="m2"><p>از ریاضتهای زندان دولت بیدار یافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل چو طفلان از شعف با توبه بازی می کند</p></div>
<div class="m2"><p>غنچه رنگینی از گلزار استغفار یافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرد عالم گشت مجنون جای آسایش ندید</p></div>
<div class="m2"><p>سایه خاری در این صحرای ناهموار یافت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کرد از پرواز ابر تندرو خود را سبک</p></div>
<div class="m2"><p>بحر از تمکین لنگر عزت سرشار یافت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شعله هوشانند استعداد رنگین می خرند</p></div>
<div class="m2"><p>بی سواد دل به درگاه جنون کی بار یافت؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست بی اجر اضطراب غوص و زندان صدف</p></div>
<div class="m2"><p>تا گهر را آب شد دل دیده بیدار یافت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیده یعقوب را بر روی یوسف بازدید</p></div>
<div class="m2"><p>روزگار از همت عاشق نظر بسیار یافت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زلف او غافل عنانگیر دل دیوانه شد</p></div>
<div class="m2"><p>خواست زنجیری شبی پیدا کند زنار یافت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در ره آوارگی دیوانه نقصانی نکرد</p></div>
<div class="m2"><p>سرزمین دلکشی از سایه هر خار یافت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می رود در سایه اقبال خیز گردباد</p></div>
<div class="m2"><p>هر که در صحرا دل از شوق سبکرفتار یافت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از غبار خاطر عاشق زمین اندوخت گنج</p></div>
<div class="m2"><p>آسمان از اشک سرشار که این مقدار یافت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیست بی رخصت غبار ما چمن پیرا اسیر</p></div>
<div class="m2"><p>جنبش مژگانی از خار سر دیوار یافت</p></div></div>