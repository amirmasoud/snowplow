---
title: >-
    شمارهٔ ۳۰۵
---
# شمارهٔ ۳۰۵

<div class="b" id="bn1"><div class="m1"><p>در تماشای تو هر چشمی دلی است</p></div>
<div class="m2"><p>پاکی بینش قمار مشکلی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست برما بی بری چون بید بار</p></div>
<div class="m2"><p>شهرت بیحاصلی هم حاصلی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آرزوی منصب دنیا بلاست</p></div>
<div class="m2"><p>قطره را هر موج امید ساحلی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با نظر تنگی بود دنیا فراخ</p></div>
<div class="m2"><p>مور را هر نقش پا سرمنزلی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تشنه را هر سایه ای سرچشمه ای</p></div>
<div class="m2"><p>شوق را هر رهزنی صاحبدلی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اعتقادت نیست گر ناقص اسیر</p></div>
<div class="m2"><p>خواب هر گمراه سعی کاملی است</p></div></div>