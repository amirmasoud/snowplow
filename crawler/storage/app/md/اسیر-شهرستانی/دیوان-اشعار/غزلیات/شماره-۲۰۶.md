---
title: >-
    شمارهٔ ۲۰۶
---
# شمارهٔ ۲۰۶

<div class="b" id="bn1"><div class="m1"><p>ممنون خویش بودن دل قوت دل است</p></div>
<div class="m2"><p>درخاک و خون تپیدن دل عادت دل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بادام تلخ چاشنی عیش دیده است</p></div>
<div class="m2"><p>درکام زهر غوطه زدن لذت دل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشو و نما چو خار بن از پیش دیده است</p></div>
<div class="m2"><p>در شهره مشقت دل راحت دل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رنگ شکسته زینت قلب سیاه نیست</p></div>
<div class="m2"><p>بی دست و پا شدن اثر جرأت دل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی اختیار خویش به دشمن نداد اسیر</p></div>
<div class="m2"><p>هر جا رسید کار من از دولت دل است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا روز عیش حشر پریشان صبحدم</p></div>
<div class="m2"><p>تعبیر خواب یکشبه جمعیت دل است</p></div></div>