---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>جذبه‌ها زین کوشش بی‌بال و پر دیدیم ما</p></div>
<div class="m2"><p>کعبه و بتخانه را در یک سفر دیدیم ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هجر ما آیینه وصل است و بعد آیین قُرب</p></div>
<div class="m2"><p>هر قدر شد دور آن را بیشتر دیدیم ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از غبار ما بهار چشم حیران می‌چکد</p></div>
<div class="m2"><p>فیض‌ها از همت اهل نظر دیدیم ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وسعت جولان موری نیست در زیر فلک</p></div>
<div class="m2"><p>عرصه کون و مکان را مختصر دیدیم ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اشک ما آیینه می‌کارد به هرجا می‌رود</p></div>
<div class="m2"><p>هرچه دیدیم از دل صاحب‌نظر دیدیم ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر قدر در پرده بودی هر قدر پنهان شدی</p></div>
<div class="m2"><p>بیشتر از بیشتر از بیشتر دیدیم ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون توکل خضر ره شد کاهلی‌ها سرعت است</p></div>
<div class="m2"><p>منزل مقصود بی‌عزم سفر دیدیم ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>امتیاز قدر بی‌قدری فزون است از قیاس</p></div>
<div class="m2"><p>ذره را از آسمان‌ها بیشتر دیدیم ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرکجا وحدت بهار جوش یکرنگی نمود</p></div>
<div class="m2"><p>آتش یاقوت در آب گهر دیدیم ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ناامیدی سر به سر امید شد آخر اسیر</p></div>
<div class="m2"><p>عاقبت زین نخل بی‌حاصل ثمر دیدیم ما</p></div></div>