---
title: >-
    شمارهٔ ۲۱۰
---
# شمارهٔ ۲۱۰

<div class="b" id="bn1"><div class="m1"><p>جلوه باغ نظر و چهره گلستان دل است</p></div>
<div class="m2"><p>طره بی سر و سامان سرو سامان دل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اینقدر حیله ندانم ز که آموخته است</p></div>
<div class="m2"><p>می خورد خون دل اما قسمش جان دل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده در پرده کند شمع تماشا روشن</p></div>
<div class="m2"><p>شب قدری که تماشای تو مهمان دل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جلوه هر نفسم شعله آتشبار است</p></div>
<div class="m2"><p>سینه پروانه شود جوش چراغان دل است</p></div></div>