---
title: >-
    شمارهٔ ۳۴۱
---
# شمارهٔ ۳۴۱

<div class="b" id="bn1"><div class="m1"><p>رفت از پیر و جوان طاقت و کس پیدا نیست</p></div>
<div class="m2"><p>عالمی گشته گرفتار و قفس پیدا نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو آن شعله که از دود نگردد پیدا</p></div>
<div class="m2"><p>بسکه پیچیده به دل آه نفس پیدا نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می کشد زارم و از شوق به خود می بالم</p></div>
<div class="m2"><p>که در این معرکه یک اهل هوس پیدا نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دلم غیر خیال تو تجلی نکند</p></div>
<div class="m2"><p>که در این آینه عکس همه کس پیدا نیست</p></div></div>