---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>کرده ای جیقه جیقه ابرو را</p></div>
<div class="m2"><p>داده ای عرض جوهر مو را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخت روتر زسنگ خاره نه ای</p></div>
<div class="m2"><p>روی دل دیده هم ترازو را؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خور جور صبر خواهد بود</p></div>
<div class="m2"><p>تا ببینیم دست و بازو را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صلحش از تیغ جنگ می سازد</p></div>
<div class="m2"><p>آزمودیم طاقت او را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امتحان سنج صبرکیست اسیر</p></div>
<div class="m2"><p>سه گره کرده هر دو ابرو را</p></div></div>