---
title: >-
    شمارهٔ ۵۴۲
---
# شمارهٔ ۵۴۲

<div class="b" id="bn1"><div class="m1"><p>نه همین نامه چو شمع از خبرم می سوزد</p></div>
<div class="m2"><p>که چو پروانه پر نامه برم می سوزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پر طاوس کشد سایه آن جلوه به خاک</p></div>
<div class="m2"><p>هر قدم شوق به رنگ دگرم می سوزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>انتظارت نشود سرمه کش دیده کس</p></div>
<div class="m2"><p>این چراغی است که از چشم ترم می سوزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عضو عضوم سبق سوختن از هم گیرند</p></div>
<div class="m2"><p>جگر از سینه ودل از جگرم می سوزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هست در هر دو جهان داغ تو سرمایه من</p></div>
<div class="m2"><p>هر کجا می روم آتش به سرم می سوزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شمع بالین من امشب قد دلجوی کسی است</p></div>
<div class="m2"><p>دل خورشید ز رشک جگرم می سوزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نکند خواب اجل تیره سرانجام اسیر</p></div>
<div class="m2"><p>شمع دیدار کسی در نظرم می سوزد</p></div></div>