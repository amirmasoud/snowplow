---
title: >-
    شمارهٔ ۴۳۴
---
# شمارهٔ ۴۳۴

<div class="b" id="bn1"><div class="m1"><p>دلم تا چند از شرم نگاهی مضطرب گردد</p></div>
<div class="m2"><p>همان بهتر که پیش دادخواهی مضطرب گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوشا بزمی که از جوش دل آهی مضطرب گردد</p></div>
<div class="m2"><p>نگاهی مضطرب گردد نگاهی مضطرب گردد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فسردن سوخت خون نا امیدی در رگ جانم</p></div>
<div class="m2"><p>خوشا آن دل کز امید نگاهی مضطرب گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نسب از کوره سیماب دارد خاطر عاشق</p></div>
<div class="m2"><p>بسوزد هر دو عالم را چو آهی مضطرب گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طبیبم گر تو باشی روز و شب از درد می خواهم</p></div>
<div class="m2"><p>که نبض ناتوان من الهی مضطرب گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پشیمانی بدل کرد آنکه کوه صبر مستان را</p></div>
<div class="m2"><p>الهی مضطرب گردد الهی مضطرب گردد</p></div></div>