---
title: >-
    شمارهٔ ۲۶۵
---
# شمارهٔ ۲۶۵

<div class="b" id="bn1"><div class="m1"><p>گلشن ز جلوه تو پریخانه گشته است</p></div>
<div class="m2"><p>بوی گل از هوای تو دیوانه گشته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آبادی دو کون غباری است در رهش</p></div>
<div class="m2"><p>هر دل که یک سراسر ویرانه گشته است؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>الفت به چشم مست تو بسیار مشکل است</p></div>
<div class="m2"><p>بدخوی آشنایی بیگانه گشته است؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمع از رخت به سرزده گلدسته فروغ</p></div>
<div class="m2"><p>رشکم نقاب بلبل و پروانه گشته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقی شکار جلوه طاقت گداز کیست</p></div>
<div class="m2"><p>تا عکس جام و شیشه پریخانه گشته است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از بس کشیده از دل بی اضطراب من</p></div>
<div class="m2"><p>گردم غبار خاطر ویرانه گشته است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر ناله ای که از دل من سرکشیده است</p></div>
<div class="m2"><p>شمع مزار بلبل و پروانه گشته است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رنگین بساط توبه گه دیگر است اسیر</p></div>
<div class="m2"><p>گلشن طراز گریه مستانه گشته است</p></div></div>