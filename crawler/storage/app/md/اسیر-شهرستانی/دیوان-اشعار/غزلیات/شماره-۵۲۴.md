---
title: >-
    شمارهٔ ۵۲۴
---
# شمارهٔ ۵۲۴

<div class="b" id="bn1"><div class="m1"><p>یاد چشمت سرگران تا چند از دل بگذرد</p></div>
<div class="m2"><p>تا کی این صیاد مست از صید غافل بگذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سبزه زنجیر می روید ز صحرای جنون</p></div>
<div class="m2"><p>سیر دارد گر نسیمی بی سلاسل بگذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>موج را بیدست و پا تر می نماید اضطراب</p></div>
<div class="m2"><p>کشتی بی لنگران از بحر مشکل بگذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی نیازیهای دهقان خانه سوز آفت است</p></div>
<div class="m2"><p>میگدازد برق را هرکس ز حاصل بگذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشتی دریای عاشق را حباب وحشت است</p></div>
<div class="m2"><p>آب می گردد اگر از یاد ساحل بگذرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پاک شو ز آلودگیها در ره تنگ عدم</p></div>
<div class="m2"><p>از گهر چون رشته ای تر گشت مشکل بگذرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کشت بی برگی است برقش دشتبانی می کند</p></div>
<div class="m2"><p>آفت از دست توکل دست بر دل بگذرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شوق را نازم چه پروا دارم از واماندگی</p></div>
<div class="m2"><p>منزل از همراهی کامم ز منزل بگذرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صبح خندان می شود بر روی تیغ آفتاب</p></div>
<div class="m2"><p>کاملی باید که از تقصیر جاهل بگذرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قطره ما از خیال بحر طوفان است اسیر</p></div>
<div class="m2"><p>مایه سیل است هر اشکی که از دل بگذرد</p></div></div>