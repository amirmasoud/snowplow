---
title: >-
    شمارهٔ ۴۹۸
---
# شمارهٔ ۴۹۸

<div class="b" id="bn1"><div class="m1"><p>ز خود هم می گریزد راز پنهان کسی دارد</p></div>
<div class="m2"><p>غبار وحشتم بوی گلستان کسی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل دیوانه ام شبها پری در خواب می بیند</p></div>
<div class="m2"><p>سری با سایه سرو خرامان کسی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه نقاشانه می آید صبا از گلشن کویش</p></div>
<div class="m2"><p>سرانجامی برای چشم حیران کسی دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به رنگی می خرامد سرو استغنا شعار من</p></div>
<div class="m2"><p>که پنداری به زیر هر قدم جان کسی دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز غیرت غنچه می خندد گل زخم نمایانم</p></div>
<div class="m2"><p>مگر دل نسبت دوری به پیکان کسی دارد</p></div></div>