---
title: >-
    غزل شمارهٔ ۱۴۷۱
---
# غزل شمارهٔ ۱۴۷۱

<div class="b" id="bn1"><div class="m1"><p>بهار رنگ عبرت جز دل روشن نمی‌بیند</p></div>
<div class="m2"><p>صفا آیینه دارد در بغل آهن نمی‌بیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گریبان چاک زن شاید تمیزی واکند چشمت</p></div>
<div class="m2"><p>که یوسف محو آغوش‌است و پیراهن‌نمی‌بیند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مزاج همت آزاد حکم آسمان دارد</p></div>
<div class="m2"><p>ز خود هرگاه دل برخاست افتادن نمی‌بیند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تحیر توام خورشید می‌بالد درین گلشن</p></div>
<div class="m2"><p>گل داغی که ما داریم افسردن نمی‌بیند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مقلد از تجرد برنیاید با سبکروحان</p></div>
<div class="m2"><p>کمالات مسیحا دیدهٔ سوزن نمی‌بیند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهان عبرت نمی‌خواهد به حکم ناز خودبینی</p></div>
<div class="m2"><p>چه سازد شخص فطرت زندگی مردن نمی‌بیند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پر افشان‌ست موهومی ولی چشم تأمل‌کو</p></div>
<div class="m2"><p>تلاش ذرهٔ ما هیچ جا روزن نمی‌بیند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به سیر این بهار از عیش مهجوران چه می‌پرسی</p></div>
<div class="m2"><p>جدایی جز به چشم زخم خندیدن نمی‌بیند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درین محفل هزار آیینه‌ام آمد به پیش اما</p></div>
<div class="m2"><p>کسی‌ جز عکس ‌خود دیدم‌ که سوی ‌من نمی‌بیند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه سازم‌ کز گریبان شعله‌واری سر برون آرم</p></div>
<div class="m2"><p>ز همت آتش افسرده‌ام دامن نمی‌بیند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رعونت خاک لیسد تاکنی فهم مآل خود</p></div>
<div class="m2"><p>که پیش پا،‌کس اینجا بی‌خم‌گردن نمی‌بیند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فلک هم از نصیب ما ندارد آگهی بیدل</p></div>
<div class="m2"><p>تلاش روزی‌ کس چشم پرویزن نمی‌بیند</p></div></div>