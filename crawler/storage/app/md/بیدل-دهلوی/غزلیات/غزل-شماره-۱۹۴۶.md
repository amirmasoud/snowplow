---
title: >-
    غزل شمارهٔ ۱۹۴۶
---
# غزل شمارهٔ ۱۹۴۶

<div class="b" id="bn1"><div class="m1"><p>اگر آن نازنین رود به تماشای رنگ‌گل</p></div>
<div class="m2"><p>چمن از شرم عارضش ندهد گل به چنگ‌گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خرامی‌که‌گل‌کند ز نهال جنون‌گلش</p></div>
<div class="m2"><p>الم خار می‌کشد قدم عذر لنگ‌گل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می مینای این چمن ز شکست است موجزن</p></div>
<div class="m2"><p>پی بوگیر و درشکن به خیال ترنگ‌گل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز نشاط عرق ثمر به‌گلاب آب ده نظر</p></div>
<div class="m2"><p>مگشای بالت آنقدر که‌ کشند غنچه بنگ‌گل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه به رنگ الفت بقا، نه ز بوی جلوه پرگشا</p></div>
<div class="m2"><p>مگر این نقد پوچ را تو بسنجی به سنگ‌گل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طرب باغ رنگ اگر زند ازخنده‌گل به سر</p></div>
<div class="m2"><p>تو هم این زخم تازه‌کن دو سه روزی به رنگ‌گل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به چنین وضع ناتوان نستیزی به این وآن</p></div>
<div class="m2"><p>نبرد صرفه‌ای حیا به خس و خار چنگ‌گل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سحرجام فرصتم رمق شمع وحشتم</p></div>
<div class="m2"><p>نفسی چند می‌کشم به شتاب درنگ‌گل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من بیدل درین چمن ز چه تشریف بشکفم</p></div>
<div class="m2"><p>به فشار است رنگ هم زقباهای تنگ‌گل</p></div></div>