---
title: >-
    غزل شمارهٔ ۱۵۲۹
---
# غزل شمارهٔ ۱۵۲۹

<div class="b" id="bn1"><div class="m1"><p>آه نومیدم‌ کجا تأثیر من پیدا شود</p></div>
<div class="m2"><p>خاک‌گردم تا نشان تیر من پیدا شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صدگلو بندد جنون چون حلقه در پهلوی هم</p></div>
<div class="m2"><p>تا صدای بسمل از زنجیر من پیدا شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رنگها گم‌ کرده‌ام در خامهٔ نقاش عجز</p></div>
<div class="m2"><p>خارپایی‌ گر کشی تصویر من پیدا شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون حیا شوخی ندارد جوهر ایجاد من</p></div>
<div class="m2"><p>بر عرق زن تا گل تعمیر من پیدا شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست جز قطع تعلق حسرت عریانی‌ام</p></div>
<div class="m2"><p>جوهری‌ می‌خواهم‌ از شمشیر من پیدا شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در کتاب اعتبارم یکقلم حرف مگوست</p></div>
<div class="m2"><p>گر نفس دزدد کسی تقریر من پیدا شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌گذارد بر دماغ یک جهان معنی قدم</p></div>
<div class="m2"><p>لغزشی ‌کز خامهٔ تحریر من پیدا شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صفحهٔ‌ کاغذ ندارد تاب جولان شرار</p></div>
<div class="m2"><p>آه از آن دشتی‌کزو نخجیر من پیدا شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بوتهٔ دیگر نمی‌خواهدگداز وهم و ظن</p></div>
<div class="m2"><p>می به ساغر ریز تا اکسیر من پیدا شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در خیال او بهار افسانه‌ای سر کرده‌ام</p></div>
<div class="m2"><p>با‌ش تا خواب ‌گل از تعبیر من پیدا شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عمرها شد بیدل احرام صبوحی بسته‌ام</p></div>
<div class="m2"><p>کو خط پیمانه تا شبگیر من پیدا شود</p></div></div>