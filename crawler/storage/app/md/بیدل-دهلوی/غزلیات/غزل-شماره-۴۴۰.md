---
title: >-
    غزل شمارهٔ ۴۴۰
---
# غزل شمارهٔ ۴۴۰

<div class="b" id="bn1"><div class="m1"><p>ما و من شور گرفتاریهاست</p></div>
<div class="m2"><p>ربشهٔ دانهٔ زنجیر صداست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازگل و سبزه این باغ مپرس</p></div>
<div class="m2"><p>عالمی پا به‌ گل و سر به هواست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>. قید ما شاهد آزادی اوست</p></div>
<div class="m2"><p>طوق قمری همه دم سرونماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محرمان غنچهٔ باغ ادبند</p></div>
<div class="m2"><p>چشم واکردن ما ترک حیاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عجز در هیچ مکان پنهان نیست</p></div>
<div class="m2"><p>آبله زیر قدم هم رسواست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خلق در حسرت بیکاری مرد</p></div>
<div class="m2"><p>دست و پای همه مشتاق حناست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه ستم بود که دل صورت بست</p></div>
<div class="m2"><p>عمرها شد گهر از بحر جداست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>معنی از لفظ‌، صفا می‌خواهد</p></div>
<div class="m2"><p>آتش سنگ به فکر میناست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برق معنی به سیاهی نزند</p></div>
<div class="m2"><p>خط اگر جلوه دهد دورنماست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کعبه و دیر تسلیکده نیست</p></div>
<div class="m2"><p>درد نایابی مطلب همه جاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>منکر قد دو تا نتوان بود</p></div>
<div class="m2"><p>آنچه برداردت از خویش عصاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فکر جمعیت دل چند کنید</p></div>
<div class="m2"><p>رشتهٔ حسرت این عقد رساست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن قیامت‌که اجل می‌گوبند</p></div>
<div class="m2"><p>اگر امرور نباشد فرداست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کاش چون شمع نخندَد سحرم</p></div>
<div class="m2"><p>سوختن باز در این بزم ‌کجاست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بی دل از یاس ندارپم گریز</p></div>
<div class="m2"><p>جز دل ما دو جهان در بر ماست</p></div></div>