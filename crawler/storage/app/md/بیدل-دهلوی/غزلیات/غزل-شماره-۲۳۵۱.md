---
title: >-
    غزل شمارهٔ ۲۳۵۱
---
# غزل شمارهٔ ۲۳۵۱

<div class="b" id="bn1"><div class="m1"><p>قابل بار امانتها مگو آسان شدیم</p></div>
<div class="m2"><p>سرکشیها خاک شد تا صورت انسان شدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عدم جنس محبت قیمت‌ کونین داشت</p></div>
<div class="m2"><p>تا نفس واکرد دکان همچو باد ارزان شدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای بسا نقشی‌که آگاهی به یاد ما شنید</p></div>
<div class="m2"><p>تاکنون زیب تغافلخانهٔ نسیان شدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتگو عمری نفسها سوخت تا انجام کار</p></div>
<div class="m2"><p>همچو شمع ‌کشته در زیر زبان پنهان شدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سود اگر در پرده ‌خون می‌شد زیانی هم نبود</p></div>
<div class="m2"><p>چون مه از عرض ‌کمال آیینهٔ نقصان شدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیکر ما را چوگردون بی سبب خم‌کرده‌اند</p></div>
<div class="m2"><p>در میان گویی نبود آندم ‌که ‌ما چوگان شدیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غنچهٔ ما عرض چندین برگ گل در بار داشت</p></div>
<div class="m2"><p>یک گرببان چاک اگر کردیم صد دامان شدیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرکسی‌ ویرانهٔ خود را عمارت می‌کند</p></div>
<div class="m2"><p>ما به تعمیر دل بی پا و سر ویران شدیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آینه‌ در زنگ‌ مژگانی‌ بهم‌ آورده‌ بود</p></div>
<div class="m2"><p>چشم تا وا شد به روی نیک‌ و بد حیران‌ شدیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی تمیزی داشت ما را نازپرورد غنا</p></div>
<div class="m2"><p>آخر از آدم شدن محتاج آب و نان شدیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زین لباس سایگی کز شرم هستی تیره است</p></div>
<div class="m2"><p>نور او پوشید ما را هر قدر عریان شدیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اینقدرها حسرت آغوش هم می‌بوده است</p></div>
<div class="m2"><p>هرکه شد چشم تماشای تو ما مژگان شدیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هیچ نتوان بست نقش خجلت ازکمفرصتی</p></div>
<div class="m2"><p>رنگ ما پیش از وفا بشکست‌ اگر پیمان شدیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پشت دستی هم نشد ریش از ندامتهای خلق</p></div>
<div class="m2"><p>طبع ما وقتی پشیمان شد که بی‌دندان شدیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بیدل از ما عالمی با درس معنی اشناست</p></div>
<div class="m2"><p>ما به فهم خود چرا چون حرف و خط نادان شدیم</p></div></div>