---
title: >-
    غزل شمارهٔ ۱۰۴۲
---
# غزل شمارهٔ ۱۰۴۲

<div class="b" id="bn1"><div class="m1"><p>عالم گرفتاری‌، خوش تسلسلی دارد</p></div>
<div class="m2"><p>جوش نالهٔ زنجیر، باغ سنبلی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو کوزهٔ دولاب هر چه زیر گردون است</p></div>
<div class="m2"><p>یا ترقی آهنگ است یا تنزلی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرفشانی عشق است رنگ و بوی این‌گلشن</p></div>
<div class="m2"><p>هر گلی که می‌بینی بال بلبلی دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر تعلق اسباب‌، عرض صد جنون‌نازست</p></div>
<div class="m2"><p>بی‌نیازی ما هم یک تغافلی دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بار شکوه‌پیمایی بر دل پر افتاده‌ست</p></div>
<div class="m2"><p>تا تهی نمی‌گردد شیشه قلقلی دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواه برتأمل زن خواه لب به حرف افکن</p></div>
<div class="m2"><p>سیر این بهارستان غنچه و گلی دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز انفعال مخموری سرخوش تسلی باش</p></div>
<div class="m2"><p>جبهه تا عرق‌پیماست ساغر مُلی دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رنج زندگی بر ما نیستی‌ گوارا کرد</p></div>
<div class="m2"><p>زین محیط بگذشتن در نطر پلی دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می‌کشد اسیران را از قیامت آنسوتر</p></div>
<div class="m2"><p>شاهد امل بیدل طرفه کاکلی دارد</p></div></div>