---
title: >-
    غزل شمارهٔ ۱۸۹۹
---
# غزل شمارهٔ ۱۸۹۹

<div class="b" id="bn1"><div class="m1"><p>بسکه بی‌لعل تو رفت از بزم عیش ما نمک</p></div>
<div class="m2"><p>می‌زند بر ساغر می‌خندهٔ مینا نمک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داغ شوقت زِیرمشق منت هرپنبه نیست</p></div>
<div class="m2"><p>اشک خودکافیست‌گر خواهدکباب ما نمک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جسم راحت خواه و دل جمعیت و عمر امتداد</p></div>
<div class="m2"><p>با چنین توفان حاجت دارد استغنا نمک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای‌خرد خمخانهٔ نازی بجوش آورده‌ای</p></div>
<div class="m2"><p>باش تا شور جنون ما کند پیدا نمک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پشت برگل دادن از آثارکافر نعمتی است</p></div>
<div class="m2"><p>جای آن دارد که‌ گیرد چشم شبنم را نمک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اضطراب شعله تسکینش همان خاکستر است</p></div>
<div class="m2"><p>کوشش ما می‌برد داغی‌که دارد با نمک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی‌تبسم نیست با آن جوش شیرینی لبش</p></div>
<div class="m2"><p>تا تو دریابی‌ که درکار است در هر جا نمک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آفت هستی به اسبابی دگرموقوف نیست</p></div>
<div class="m2"><p>زخم صبح از خندهٔ خود می‌کند انشا نمک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با همه ابرام باید تشنه‌کام یاس مرد</p></div>
<div class="m2"><p>حرص مستسقی و دارد آبروی ما نمک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل از حسن ملیحش چند غافل زیستن</p></div>
<div class="m2"><p>دیده‌های زخم را هم‌می‌کند بینا نمک</p></div></div>