---
title: >-
    غزل شمارهٔ ۱۴۵۱
---
# غزل شمارهٔ ۱۴۵۱

<div class="b" id="bn1"><div class="m1"><p>اینکه طاقت‌ها جوانی می‌کند</p></div>
<div class="m2"><p>ناتوانی‌، ناتوانی می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر همه خاک از زمین‌گردد بلند</p></div>
<div class="m2"><p>بر سر ما آسمانی می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسکه فطرتها ضعیف‌ افتاده است</p></div>
<div class="m2"><p>تکیه بر دنیای فانی می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست‌کس اینجاکفیل هیچکس</p></div>
<div class="m2"><p>زندگی روزی‌رسانی می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عصمت از تشویش دنیا جستن است</p></div>
<div class="m2"><p>نفس را این قحبه، زانی می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در تب و تاب نفس پرواز نیست</p></div>
<div class="m2"><p>سعی بسمل پرفشانی می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قید هستی پاس ناموس دل است</p></div>
<div class="m2"><p>بیضه‌داری آشیانی می‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از چه خجلت صفحه‌ام آتش زند</p></div>
<div class="m2"><p>چون عرق داغم روانی می‌کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرکه را دیدم درین عبرت‌سرا</p></div>
<div class="m2"><p>بهر مردن زندگانی می‌کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی دماغم‌، غیر دل زین انجمن</p></div>
<div class="m2"><p>هرچه بردارم گرانی می‌کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنقدر از خود به یادش رفته‌ام</p></div>
<div class="m2"><p>کاین جهانم آنجهانی می‌کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هیچ می‌دانی که‌ام ای بی‌خبر</p></div>
<div class="m2"><p>شاه ما را پاسبانی می‌کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کلک بیدل هرکجا دارد خرام</p></div>
<div class="m2"><p>سکته هم ناز روانی می‌کند</p></div></div>