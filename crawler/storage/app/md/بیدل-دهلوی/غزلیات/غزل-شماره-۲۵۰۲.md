---
title: >-
    غزل شمارهٔ ۲۵۰۲
---
# غزل شمارهٔ ۲۵۰۲

<div class="b" id="bn1"><div class="m1"><p>دل پیش نظر گیر سر و برگ نمو ‌کن</p></div>
<div class="m2"><p>گر مایل نازی سوی این آینه روکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شایستهٔ تسلیم یقین سجدهٔ‌کس نیست</p></div>
<div class="m2"><p>ای ننگ عبادت عرقی چند وضوکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا چشم هوس هرزه نخندد مژه بربند</p></div>
<div class="m2"><p>در جوهر این آینه چاکی‌ست‌، رفوکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منظور وفا گر بود امداد ضعیفان</p></div>
<div class="m2"><p>با سبزه خطابی‌که‌کنی از لب جو کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صد طبلهٔ عطار شکسته‌ست در این دشت</p></div>
<div class="m2"><p>هر خاک‌که بینی نم آبی زن وبوکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تحقیق خیالات مقابل نپسندد</p></div>
<div class="m2"><p>تمثال پرستی سر آیینه فرو کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برچینی دل غیر شکستن چه توان‌کرد</p></div>
<div class="m2"><p>ابریشم این ساز نوا باخته مو کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زین ورطه نرسته‌ست کسی بی‌سر تسلیم</p></div>
<div class="m2"><p>زان پیش که‌کشتی شکند فکر کدو کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از قطرهٔ ‌گمگشته همان بحر سراغ‌ست</p></div>
<div class="m2"><p>هرگاه که یادم کنی اندیشهٔ اوکن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی مطبی از شبهه و تحقیق مبراست</p></div>
<div class="m2"><p>آن روی امیدی که نداری همه سو کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل طلب راحت اگر مقصد جهد است</p></div>
<div class="m2"><p>چون موج گهر بر دل ناکام غلو کن</p></div></div>