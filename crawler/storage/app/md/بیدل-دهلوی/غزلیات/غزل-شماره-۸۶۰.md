---
title: >-
    غزل شمارهٔ ۸۶۰
---
# غزل شمارهٔ ۸۶۰

<div class="b" id="bn1"><div class="m1"><p>دی به شبنم گریهٔ ما نوگلی خندید و رفت</p></div>
<div class="m2"><p>از زبان اشک هم درد دلی نشنید و رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تماشاگاه هستی مدعا سیر دل است</p></div>
<div class="m2"><p>چون نفس باید بر این آیینه هم پیچید و رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمع‌ محفل‌ بر خموشی‌ بست‌ و مینا بر شکست</p></div>
<div class="m2"><p>هر کسی زین انجمن طرز دگر نالید و رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین بیابان هر قدم خار دگر دارد کمین</p></div>
<div class="m2"><p>رهروان را پیش‌پای خویش باید دید و رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عزم چون افتاد صادق راه مقصد بسته نیست</p></div>
<div class="m2"><p>اشک در بی‌دست و پایی ها به سر غلتید و رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کوشش واماندگان هم ره به جایی می‌برد</p></div>
<div class="m2"><p>سر به پایی می‌توان چون آبله دزدید و رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عالمی صد ناله پیش‌آهنگی امید داشت</p></div>
<div class="m2"><p>یک نگاه واپسین ناگاه برگردید و رفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای‌ سحر در اشک شبنم غوطه می‌باید زدن</p></div>
<div class="m2"><p>کز شکست رنگ بر ما عافیت خندید و رفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هیچ شبنم برنیارد سر ز جیب نیستی</p></div>
<div class="m2"><p>گر بداند کز چه ‌گل خواهد نظر پوشید و رفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زان دهان بی‌نشان بوی سراغی برده‌ام</p></div>
<div class="m2"><p>تا قیامت بایدم راه عدم پرسید و رفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صبحدم بیدل خیال نوبهار آیینه‌ای</p></div>
<div class="m2"><p>از تبسم بر گل زخمم نمک پاشید و رفت</p></div></div>