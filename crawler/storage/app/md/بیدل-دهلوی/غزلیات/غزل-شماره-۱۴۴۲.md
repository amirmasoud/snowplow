---
title: >-
    غزل شمارهٔ ۱۴۴۲
---
# غزل شمارهٔ ۱۴۴۲

<div class="b" id="bn1"><div class="m1"><p>بولهوس از سبک سری حفظ سخن نمی‌کند</p></div>
<div class="m2"><p>در قفس حبابها، باد وطن نمی‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لب مگشای چون صدف تا گهر آوری به‌ کف</p></div>
<div class="m2"><p>گوش طلب‌که‌کارگوش هیچ دهن نمی‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قطره محیط می‌شود چون ز سحاب شد جدا</p></div>
<div class="m2"><p>روح ز وهم خود عبث ترک بدن نمی‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هستی خود گداز من شمع شرر بهانه‌ای‌ست</p></div>
<div class="m2"><p>لیک‌کسی نگاه‌گرم جانب من نمی‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خون امید می‌خورد بی‌تو دل شکسته‌ام</p></div>
<div class="m2"><p>طرهٔ سرکشت چرا یاد شکن نمی‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسکه هوای غربتم چون نفس است دلنشین</p></div>
<div class="m2"><p>جوهر من در آینه فکر وطن نمی‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست به عالم جنون‌گردش رنگ عافیت</p></div>
<div class="m2"><p>هیچکس از برهنگی جامه‌کهن نمی‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پنبهٔ داغ عاشقان نیست به غیر سوختن</p></div>
<div class="m2"><p>مرده‌صفت چراغ ما سر به‌ کفن نمی‌کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیده به صدهزار اشک محو نثار مقدمی‌ست</p></div>
<div class="m2"><p>آه که آن سهیل ناز یاد یمن نمی‌کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>منع غنای دلبران نیست به جهد عاشقان</p></div>
<div class="m2"><p>بلبل اگربه خون تپد غنچه سخن نمی‌کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از عزبی به طبع خود جمع مکن مواد ننگ</p></div>
<div class="m2"><p>شوهر خویش می‌شود مرد که زن نمی‌کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ناله به شعله می‌تپد حلقهٔ داغ‌گو مباش</p></div>
<div class="m2"><p>شمع بساط بیکسان ساز لگن نمی‌کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زخم تو آنچه می‌کند با دل خستگان عشق</p></div>
<div class="m2"><p>صبح نکرده با هوا،‌ گل به چمن نمی‌کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سایهٔ دور ازآفتاب مغتنم خود است و بس</p></div>
<div class="m2"><p>طالب وصل او شدن صرفهٔ من نمی‌کند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نیست دمی‌که شانه‌وار در خم فکر زلف یار</p></div>
<div class="m2"><p>بیدل سینه‌چاک من سیر ختن نمی‌کند</p></div></div>