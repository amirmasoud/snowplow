---
title: >-
    غزل شمارهٔ ۲۴۳۹
---
# غزل شمارهٔ ۲۴۳۹

<div class="b" id="bn1"><div class="m1"><p>از نالهٔ دل ما تا کی رمیده رفتن</p></div>
<div class="m2"><p>زین دردمند حرفی باید شنیده رفتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی نشئه زندگانی چندان نمک ندارد</p></div>
<div class="m2"><p>حیف ست از این خرابات می ناکشیده رفتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آهنگ بی‌نشانی زین‌ گلستان ضرور است</p></div>
<div class="m2"><p>راه فنا چو شبنم باید به دیده رفتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جرأتگر طلب نیست بی دست و پایی ما</p></div>
<div class="m2"><p>دارد به سعی‌ قاتل خون چکیده رفتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون شعله‌ای که آخر پامال داغ‌ گردد</p></div>
<div class="m2"><p>در زیر پا نشستیم از سر کشیده رفتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زین باغ محمل ما بر دوش ناامیدیست</p></div>
<div class="m2"><p>بر آمدن نبندد رنگ پریده رفتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از وحشت نفسها کو فرصت تأمل</p></div>
<div class="m2"><p>چون صبح باید از خویش دامن نچیده رفتن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر خلق بی‌بصیرت‌ تا چند عرض‌ جوهر</p></div>
<div class="m2"><p>باید ز شهر کوران چون نور دیده رفتن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همدوش آرزوها دل‌ می‌رود نفس نیست</p></div>
<div class="m2"><p>در رنگ ریشه دارد تخم دمیده رفتن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قطع نفس نمودیم جولان مدعا کو</p></div>
<div class="m2"><p>در خواب هم نبیند پای بریده رفتن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رفتار سایه هرگز واماندگی ندارد</p></div>
<div class="m2"><p>در منزل است پرواز از آرمیده رفتن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قد دو تای پیریست ابروی این اشارت</p></div>
<div class="m2"><p>کز تنگنای هستی باید خمیده رفتن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بال فشاندهٔ آه بی‌گرد حسرتی نیست</p></div>
<div class="m2"><p>با عالمی ز خود برد ما را جریده رفتن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تعجیل طفل خویان مشق خطاست بیدل</p></div>
<div class="m2"><p>لغزش به پیش دارد اشک از دویده رفتن</p></div></div>