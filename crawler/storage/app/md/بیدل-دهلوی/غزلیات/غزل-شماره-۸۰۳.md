---
title: >-
    غزل شمارهٔ ۸۰۳
---
# غزل شمارهٔ ۸۰۳

<div class="b" id="bn1"><div class="m1"><p>ز خویش‌ مگذر اگر جوهرت‌ شناسایی‌ ست</p></div>
<div class="m2"><p>که خو‌‌دپرستی عالم‌، بهار یکتایی‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه‌ گلشنی‌ست به پیش نظر، نه دشت و نه در</p></div>
<div class="m2"><p>بلندی مژه ا‌ت منظر خودآرایی‌ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهار رمز ازل تا چه وقت گیرد رنگ</p></div>
<div class="m2"><p>هنوز نغمهٔ نی تشنهٔ لب نایی‌ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگیر ز غیب برآییم‌ تا عیان گردیم</p></div>
<div class="m2"><p>ز خود نشان چه دهد قطره‌ای‌ که دریایی‌ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز ذات محض چه اسما که برنمی‌آییم</p></div>
<div class="m2"><p>جهان وهم و گمان فطرت معمایی‌ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل از تکلف هستی جنون‌نمایی کرد</p></div>
<div class="m2"><p>نفس در آینه رنگ بهار سودایی ‌ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به بلزم وصل جنون ناگزبر عشه افتاد</p></div>
<div class="m2"><p>ز منع بلبل ادب کن بهار سودایی ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کس به ستر عیوب نفس چه چار‌ند</p></div>
<div class="m2"><p>غبار نیستی آیینه‌ایم و رسوایی‌ست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لطافتی‌ست به طبع درشتی آفاق</p></div>
<div class="m2"><p>مقیم پرده سنگ انتظار مینایی‌ست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شکست بام و دری چند می‌کند فریاد</p></div>
<div class="m2"><p>که از هوا به‌در آیید خانه صحرایی‌ست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به عرض نیم نفس‌ کس چه‌ گردن افرازد</p></div>
<div class="m2"><p>حباب ما عرق انفعال پیدایی ‌ست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو هم دری چو شرر واکن و ببند، بس است</p></div>
<div class="m2"><p>به‌ کارخانهٔ فرصت‌، عدم تماشایی ‌ست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فتاده‌ایم به راهت چو سایه جبهه به خاک</p></div>
<div class="m2"><p>ز پش ما به تغافل زدن چه رعنایی‌ست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رعونتی به طبعت‌ که چون غبار سحر</p></div>
<div class="m2"><p>اگر به باد روی پیشت اوج‌پیمایی‌ست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تلاش‌ کعبه و دیرت نمی‌رود بیدل</p></div>
<div class="m2"><p>بهشت ‌و دوزخ‌خویشی خیال هرجایی ‌ست</p></div></div>