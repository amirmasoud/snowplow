---
title: >-
    غزل شمارهٔ ۳۶۷
---
# غزل شمارهٔ ۳۶۷

<div class="b" id="bn1"><div class="m1"><p>از سر مستی نبود امشب خطابم با شراب</p></div>
<div class="m2"><p>بی‌دماغی شیشه زد بر سنگ‌گفتم تا شراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزم امکان را بود غوغای مستی تا به‌کی</p></div>
<div class="m2"><p>چند خواهد بود آخرجوش یک مینا شراب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دور وهمی می‌توان طی‌کرد چون اوراق‌گل</p></div>
<div class="m2"><p>ساغر این بزم رنگ است و شکستنها شراب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مست تا مخمور این میخانه محتاجند و بس</p></div>
<div class="m2"><p>وهم بنگ‌است اینکه‌گویی‌دارد استغنا شراب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عمرها بودیم مخمور سمندر مشربی</p></div>
<div class="m2"><p>نیست از انصاف اگرریزی به خاک ما شراب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیقراران طلب سر تا قدم‌کیفیتند</p></div>
<div class="m2"><p>می‌کند ایجاد از هر عضو خود دریا شراب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساغر بزم خیالم نرگس مخمورکیست</p></div>
<div class="m2"><p>می‌روم مستانه از خود خورده‌ام‌گویا شراب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صبح ز خمیازه آخر جام شبنم می‌کشد</p></div>
<div class="m2"><p>حسرت مخمور از خود می‌کند پیدا شراب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خون‌شدن سر منزلیم‌، از جستجوی ما مپرس</p></div>
<div class="m2"><p>تاک می‌داند چها در پیش دارد تا شراب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بهرمنع می‌کشیها محتسب درکارنیست</p></div>
<div class="m2"><p>بیدل آخر رعشه می‌بندد به دست ما شراب</p></div></div>