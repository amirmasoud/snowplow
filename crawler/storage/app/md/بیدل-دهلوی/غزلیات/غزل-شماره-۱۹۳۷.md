---
title: >-
    غزل شمارهٔ ۱۹۳۷
---
# غزل شمارهٔ ۱۹۳۷

<div class="b" id="bn1"><div class="m1"><p>ای فرش خرامت همه‌جا چون سر ما گل</p></div>
<div class="m2"><p>در راه تو صد رنگ جبین ریخته تا گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلشن چقدر حیرت دیدار تو دارد</p></div>
<div class="m2"><p>در شیشهٔ هر رنگ شکسته‌ست صدا گل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبنم صفت از عجز نظر هیچ نچیدیم</p></div>
<div class="m2"><p>غیر از عرقی چند درین باغ حیا گل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای بیخبران غرهٔ اقبال مباشید</p></div>
<div class="m2"><p>از خاک چه مقدار کشد سر به هوا گل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نعل همه در آتش تحصیل نشاط است</p></div>
<div class="m2"><p>دریاب‌ که از رنگ چه دارد ته پا گل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عالم همه یک بست و گشاد مژه دارد</p></div>
<div class="m2"><p>ای باغ هوس غنچه چه رنگ است و کجا گل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آشفتگی وضع جنون بی‌چمنی نیست</p></div>
<div class="m2"><p>گر ذوق تماشاست به این رنگ برآ گل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلدار سر نامه و پیغام که دارد</p></div>
<div class="m2"><p>آیینه تو آنجا ببر از حیرت ما گل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیر چمن بیخودی آرایش ناز است</p></div>
<div class="m2"><p>گر می‌روی از خویش برو رنگ و بیا گل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل سر احرام تماشای که دارد</p></div>
<div class="m2"><p>آیینه‌ گرفته‌ست به صد دست دعا گل</p></div></div>