---
title: >-
    غزل شمارهٔ ۱۰۸۱
---
# غزل شمارهٔ ۱۰۸۱

<div class="b" id="bn1"><div class="m1"><p>دل‌گداخته بر شش جهت بغل واکرد</p></div>
<div class="m2"><p>جهان به شیشه‌گرفت این پری چه انشاکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ستم نصیب دلم من کجا و درد کجا</p></div>
<div class="m2"><p>نفس به‌ کوچهٔ نی رفت و ناله پیدا کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز شرم چشم تو دارد خیالم انجمنی</p></div>
<div class="m2"><p>که باید از عرقم سیر جام و مینا کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه سحر بود که افسون بی‌نیازی عشق</p></div>
<div class="m2"><p>مرا به خاک نشاند و ترا تماشاکرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به فکر کار دل افتادم از چکیدن اشک</p></div>
<div class="m2"><p>شکست شیشه به روبم در حلب واکرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازین بساط گذشتم‌.ولی نفهمیدم</p></div>
<div class="m2"><p>که وضع پیکر خم با که این مدارا کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو شمع صورت بیداری‌ام چه امکان داشت</p></div>
<div class="m2"><p>سری‌ که رفت ز دوشم اشارت پا کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نهفت معنی مکشوف بی‌تاملی‌ام</p></div>
<div class="m2"><p>نبستن مژه آفاق را معما کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جنون بیخودیی پیش برد سعی امل</p></div>
<div class="m2"><p>که کار عالم امروز نذر فردا کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فسردنی است سرانجام عافیت‌طلبان</p></div>
<div class="m2"><p>محیط این‌کره از رشتهٔ‌گهر واکرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خیال اگر همه فردوس در بغل دارد</p></div>
<div class="m2"><p>قفای زانوی حسرت نمی‌توان جا کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دلیل الفت اسباب غیر عسجز نبود</p></div>
<div class="m2"><p>پر شکستهٔ ما سیر این قفسها کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نداشت ظاهر و مظهر جهان یکتایی</p></div>
<div class="m2"><p>جنون آینه در دست خنده بر ما کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>درین هوسکده از من چه دیده‌ای بیدل</p></div>
<div class="m2"><p>به عالمی که نی‌ام بایدم تماشا کرد</p></div></div>