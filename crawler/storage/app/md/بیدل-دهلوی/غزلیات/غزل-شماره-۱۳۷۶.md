---
title: >-
    غزل شمارهٔ ۱۳۷۶
---
# غزل شمارهٔ ۱۳۷۶

<div class="b" id="bn1"><div class="m1"><p>خوش‌خرامان اگر اندیشهٔ جولان کردند</p></div>
<div class="m2"><p>گردش رنگ مرا جنبش دامان‌ کردند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دام من در گره حلقهٔ افلاک نبود</p></div>
<div class="m2"><p>چون نگاهم قفس از دیده حیران‌ کردند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به سراغم نتوان جز مژه برهم چیدن</p></div>
<div class="m2"><p>داشتم مشت غباری‌ که پریشان کردند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به چه امید درین دشت توان آسودن</p></div>
<div class="m2"><p>وحشتی بود که تسلیم غزالان‌ کردند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زین‌ چمن حاصل‌ عشاق همین‌بس‌ که‌ چو رنگ</p></div>
<div class="m2"><p>چینی از خود شکنی زینت دامان کردند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی قراران ادب‌پرور صحرای جنون</p></div>
<div class="m2"><p>سیلها درگره آبله پنهان‌کردند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سعی واماندهٔ خلق آن سوی خود راه نبرد</p></div>
<div class="m2"><p>بسکه دامن ته پا ماند گریبان ‌کردند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نقش بند چمن وحشت ما بی رنگی است</p></div>
<div class="m2"><p>شد هوا آینه تا ناله نمایان‌ کردند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بحر امکان چوگهر شوخی‌یک‌موج نداشت</p></div>
<div class="m2"><p>از پریشان‌نظری اینهمه توفان کردند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جنس بازار وفا رنگ نمی‌گرداند</p></div>
<div class="m2"><p>دل چه مقدارگران‌کشت‌که ارزان کردند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا ز یادم نگرانی نکشد خاطر کس</p></div>
<div class="m2"><p>سرنوشت من بیدل خط نسیان ‌کردند</p></div></div>