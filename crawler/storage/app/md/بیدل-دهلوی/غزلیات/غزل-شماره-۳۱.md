---
title: >-
    غزل شمارهٔ ۳۱
---
# غزل شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>برآن سرم‌که ز دامن برون‌کشم پا را</p></div>
<div class="m2"><p>به جیب آبله ریزم غبار صحرا را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به سعی دیده حیران دل از تپش ننشست</p></div>
<div class="m2"><p>گهرکند چه‌قدر خشک آب دریا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اثرگم است به گرد کساد این بازار</p></div>
<div class="m2"><p>همان به ناله فروشید درد دلها را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خویش‌گم شدنم‌کنج عزلتی دارد</p></div>
<div class="m2"><p>که بار نیست در آن پرده وهم عنقا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زبان درد دل آسان نمی‌توان فهمید</p></div>
<div class="m2"><p>شکسته‌اند به صد رنگ شیشهٔ ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فضای خلوت دل جلوه‌گاه غیری نیست</p></div>
<div class="m2"><p>شکافتیم به نام تو این معما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگاه یار ز پهلوی ناز می‌بالد</p></div>
<div class="m2"><p>به قدرنشئه بلند است موج صهبا را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مخور فریب غنا از هوس‌گدازی یأس</p></div>
<div class="m2"><p>مباد آب دهد مزرع تمنا را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز جوش صافی دل‌، جسم‌، جان تواند شد</p></div>
<div class="m2"><p>به سعی شیشه پری کرده‌اند خارا را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به غیر عکس ندانم دگر چه خواهی دید</p></div>
<div class="m2"><p>اگر در آینه بینی جمال یکتا را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به ففر تکیه زدی بگذر از تملق خلق</p></div>
<div class="m2"><p>به مرگ ریشه دواندی درازکن پا را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چه سان به عشرت واماندگان رسی بیدل</p></div>
<div class="m2"><p>به چشم آبلهٔ پا ندیده‌ای ما را</p></div></div>