---
title: >-
    غزل شمارهٔ ۱۹۹۰
---
# غزل شمارهٔ ۱۹۹۰

<div class="b" id="bn1"><div class="m1"><p>برگ خودداری مجویید از دل دیوانه‌ام</p></div>
<div class="m2"><p>ربشه‌ها دارد چو اشک از بیقراری دانه‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قامت خم‌گشته بیش از حلقهٔ زنجیر نیست</p></div>
<div class="m2"><p>غیر جنبش ناله نتوان یافتن در خانه‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک دامنگیر دارد سرزمین بیخودی</p></div>
<div class="m2"><p>سیل بی تشویش دامی نیست از ویرانه‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل ز دست شوخی وضع نفس خون می‌خورد</p></div>
<div class="m2"><p>شمع دارد لرزه از یاد پر پروانه‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>التفات زندگی تشویش اسبابست و بس</p></div>
<div class="m2"><p>آنقدر کز خویش دورم از هوس بیگانه‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دستگاه عاریت خجلت کمین‌ کس مباد</p></div>
<div class="m2"><p>صد شبیخون ریخت نور شمع برکاشانه‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوستان را بس که افسون تغافل ننگ داشت</p></div>
<div class="m2"><p>گوشها در چشم خواباندند از افسانه‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مزرع آفاق آفت خرمن نشو و نماست</p></div>
<div class="m2"><p>همچو راز ریشه ترسم پر برآرد دانه‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسکه بر هم می‌زند بی‌جوهری اجزای من</p></div>
<div class="m2"><p>چون دم شمشیر مژگان سر به سر دندانه‌ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا شود روشن تر اسبابی‌ که باید سوختن</p></div>
<div class="m2"><p>احتیاج شمع دارد خانهٔ پروانه‌ام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زخمی ایجادم از تدبیر من آسوده باش</p></div>
<div class="m2"><p>در شکستن‌گشت‌گم چون موی چینی شانه‌ام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل از کیفیت شوق‌ گرفتاری مپرس</p></div>
<div class="m2"><p>نالهٔ زنجیر هر جا گل کند دیوانه‌ام</p></div></div>