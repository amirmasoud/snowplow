---
title: >-
    غزل شمارهٔ ۷۱
---
# غزل شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>ای غافل از رنج هوس آیینه‌پردازی چرا</p></div>
<div class="m2"><p>چون شمع بار سوختن از سر نیندازی چرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p> نگشوده‌مژگان چون شرر از خویش‌کن قطع نظر</p></div>
<div class="m2"><p>زین یک دو دم زحمتکش جام و آغازی چرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p> تاکی دماغت خون‌کند تعمیر بنیاد جسد</p></div>
<div class="m2"><p>طفلی‌گذشت ای بیخرد با خاک وگل بازی چرا </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آزادی‌ات ساز نفس آنگه غم دام و قفس</p></div>
<div class="m2"><p>با این غبار پرفشان گم کرده پروازی چرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p> گردی به جا ننشسته‌ای دل در چه عالم بسته‌ای</p></div>
<div class="m2"><p>از پرده بیرون جسته‌ای واماندة سازی چرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p> حیف است با سازغنا مغلوب خسّت زیستن</p></div>
<div class="m2"><p>تیغ ظفر در پنجه‌ات دستی نمی‌یازی چرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p> گر جوهر شرم و ادب پرواز مستوری دهد</p></div>
<div class="m2"><p>آیینه‌گردد از صفا رسوای غمازی چرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p> تاب و تب‌کبر و حسد بر حق‌پرستان‌کم زند</p></div>
<div class="m2"><p>گر نیستی آتش‌پرست آخر به این سازی چرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p> هرگز ندارد هیچکس پروای فهم خویشتن</p></div>
<div class="m2"><p>رازی وگرنه این قدر نامحرم رازی چرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p> از وادی این ما و من خاموش باید تاختن</p></div>
<div class="m2"><p>ای‌کاروانت بی‌جرس در بند آوازی چرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p> محکوم فرمان قضا مشکل‌کشد سر بر هوا</p></div>
<div class="m2"><p>از تیغ گر غافل نه‌ای گردن برافرازی چرا </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل مخواه آزار دل از طاقت راحت گسل</p></div>
<div class="m2"><p> ای پا به دوش آبله بر خار می‌تازی چرا</p></div></div>