---
title: >-
    غزل شمارهٔ ۲۷۲۲
---
# غزل شمارهٔ ۲۷۲۲

<div class="b" id="bn1"><div class="m1"><p>که دم زند ز من و مادمی‌ که ما تو نباشی</p></div>
<div class="m2"><p>به این غرور که ماییم از کجا تو نباشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نفس چو صبح زدن بی‌حضور مهر نشاید</p></div>
<div class="m2"><p>چه زندگیست ‌کسی را که آشنا تو نباشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازل به یاد که باشد، ابد دل‌ که خراشد</p></div>
<div class="m2"><p>که بود و کیست‌ گر آغاز و انتها تو نباشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غنای موج تلافیگرش بقای محیط است</p></div>
<div class="m2"><p>نکشت عشق ‌کسی را که خونبها تو نباشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>محیط عشق به‌ گوشم جز این خطاب ندارد</p></div>
<div class="m2"><p>که ای حباب چه شد جامه‌ات فنا تو نباشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مکش خجالت محرومی از غرور تعین</p></div>
<div class="m2"><p>چه من چه او همه‌ با توست اگر تو با تو نباشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان پر است ز گرد عدم سراغی عنقا</p></div>
<div class="m2"><p>تو نیز باش به رنگی ‌که هیچ جا تو نباشی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طمع به ششجهتت بسته راه حاصل مطلب</p></div>
<div class="m2"><p>جهان همه در باز است اگر گدا تو نباشی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر این بهار چو شبنم خوش‌ست چشم‌ گشو‌دن</p></div>
<div class="m2"><p>دمی‌که غیر عرق چیزی از حیا تو نباشی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنین ‌که قافلهٔ رنگ بر هواست خرامش</p></div>
<div class="m2"><p>به رنگ شمع نگاهی ‌که زیر پا تو نباشی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من و تو بیدل ‌ما را به وهم‌ چند فریبد</p></div>
<div class="m2"><p>منی جز از تو نزیبد تویی چرا تو نباشی</p></div></div>