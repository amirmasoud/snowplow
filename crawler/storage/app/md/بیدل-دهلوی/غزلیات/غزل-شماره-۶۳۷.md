---
title: >-
    غزل شمارهٔ ۶۳۷
---
# غزل شمارهٔ ۶۳۷

<div class="b" id="bn1"><div class="m1"><p>خم مکن در عرض حاجت تا توانی پشت دست</p></div>
<div class="m2"><p>اینقدرها برنمی‌دارد گرانی پشت دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوکت ملک و ملک تا اوج اقبال فلک</p></div>
<div class="m2"><p>جمله پامال است هرگه می‌فشانی پشت دست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا کی از ترک کلاه آرایش اندیشیدنت</p></div>
<div class="m2"><p>معنیی دارد نه صورت آنچه خوانی پشت دست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عمرها شد انتظار ضعف پیری می‌کشم</p></div>
<div class="m2"><p>تا زنم ازپیکر خم برجوانی پشت دست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دعوی قدرت جهانی را زپا افکنده است</p></div>
<div class="m2"><p>پهلوانی‌، بر زمین‌گر می‌رسانی پشت دست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از بیاض چشم قربانی چه استغنا دمید</p></div>
<div class="m2"><p>کاین ورق افشاند برلفظ ومعانی پشت دست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سعی آزادی حریف دامگاه وهم نیست</p></div>
<div class="m2"><p>تاکجاگیرد عیار پرفشانی پشت دست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عهدهٔ کار ندامت بار دوشم کرده‌اند</p></div>
<div class="m2"><p>عمرها شد می‌گزم از ناتوانی پشت دست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قطع آثار ندامت نیست ممکن زین بساط</p></div>
<div class="m2"><p>حرص دندان دارد و دنیای فانی پشت دست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غیر استغنا علاج زحمت اسباب نیست</p></div>
<div class="m2"><p>پشت پایی‌گر نباشد، تا توانی پشت دست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ازکفم بیدل نمی‌دانم چه‌گل دامن‌کشید</p></div>
<div class="m2"><p>کز ندامت‌کردم آخر ارغوانی پشت دست</p></div></div>