---
title: >-
    غزل شمارهٔ ۷۴۳
---
# غزل شمارهٔ ۷۴۳

<div class="b" id="bn1"><div class="m1"><p>برگ و سازم جز هجوم‌گریهٔ بیتاب نیست</p></div>
<div class="m2"><p>خانهٔ چشمی‌که من دارم‌کم ازگرداب نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رشتهٔ قانون یأسم از نواهایم مپرس</p></div>
<div class="m2"><p>درگسستن عالمی دارم‌که در مضراب نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا به ذوق‌گوهر مقصد توان زد چشمکی</p></div>
<div class="m2"><p>در محیط آرزو یک حلقهٔ‌گرداب نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست و پا از آستین و دامن آن‌سو می‌زنیم</p></div>
<div class="m2"><p>مشرب دیوانگان زندانی آداب نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در شبستان سیه بختی ز بس‌گمگشته‌ایم</p></div>
<div class="m2"><p>سایه‌‌ی ما نیز بار خاطر مهتاب نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زاهدا لاف محبت سزنی هشیار باش</p></div>
<div class="m2"><p>زخم شمشیراست این خمیازهٔ محراب نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خار خار بوریا و دلق فقر از دل برآر</p></div>
<div class="m2"><p>آتش‌است‌ای‌خواجه‌اینهامخمل‌وسنجاب‌نیست‌</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیده‌ها باز است و اسباب تماشا مغتنم</p></div>
<div class="m2"><p>لیک‌درملک خرد جز جنس غفلت یاب نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز اخلاط سخت‌رویان‌کینه جولان می‌کند</p></div>
<div class="m2"><p>سنگ وآهن تا به هم ناید شرربیتاب نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حال دل پرسیده‌ای بیطاقتی آماده باش</p></div>
<div class="m2"><p>شوخی افسانهٔ ما دستگاه‌خواب‌نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مدعا تحقیق و دل جنس امید، آه از شعور</p></div>
<div class="m2"><p>ما چنان آیینه‌ای داریم‌کانجا باب نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنچه‌می‌گویند عنقا ای زخود غافل تویی</p></div>
<div class="m2"><p>گرتوانی‌یافت خودرا مطلبی‌نایاب نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شوخی تمثال هستی برنیابد پیکرم</p></div>
<div class="m2"><p>آنقدر خاکم‌که در آیینهٔ من آب نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیدل آن برق‌نظرها آنچنان در پرده ماند</p></div>
<div class="m2"><p>غافلان‌گرم انتظار و محرمان را تاب نیست</p></div></div>