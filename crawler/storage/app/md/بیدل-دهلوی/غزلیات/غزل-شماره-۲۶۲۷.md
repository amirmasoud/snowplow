---
title: >-
    غزل شمارهٔ ۲۶۲۷
---
# غزل شمارهٔ ۲۶۲۷

<div class="b" id="bn1"><div class="m1"><p>زد عرق پیمانه حسنی ساغر اندر آینه</p></div>
<div class="m2"><p>کرد توفانها بهشت و کوثر اندر آینه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جلوهٔ او هرکجا تیغ تغافل آب داد</p></div>
<div class="m2"><p>خون حیرت ریخت جوش جوهر اندر آینه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالم آب است امشب دل بهٔاد نرگسش</p></div>
<div class="m2"><p>شیشه‌ها دارد خیال ساغر اندر آینه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل به نیرنگ خیالی بسته‌ایم و چاره نیست</p></div>
<div class="m2"><p>ما کباب دلبریم و دلبر اندر آینه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنچه از اسباب امکان دیده‌ای وهمست و بس</p></div>
<div class="m2"><p>نیست جز تمثال چیزی دیگر اندر آینه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دامن دل‌ گرد کلفت بر نتابد بیش ازین</p></div>
<div class="m2"><p>ای نفس تا چند می‌دزدی سر اندر آینه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طبع روشن فارغ است از فکر غفلتهای خلق</p></div>
<div class="m2"><p>نیست ظاهر معنی گوش کر اندر آینه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در خیال آباد دل از هر طرف خواهی درآ</p></div>
<div class="m2"><p>ره ندارد نسبت بام و در اندر آینه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرد تمثالم ولی از سرگرانیهای وهم</p></div>
<div class="m2"><p>بایدم کردن چو حیرت لنگر اندر آینه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صحبت روشندلان اکسیر اقبال است و بس</p></div>
<div class="m2"><p>آب پیدا می‌کند خاکستر اندر آینه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جبهه‌ای داری جدا مپسند از ان نقش قدم</p></div>
<div class="m2"><p>جای این عکس است بیدل خوشتر اندر آینه</p></div></div>