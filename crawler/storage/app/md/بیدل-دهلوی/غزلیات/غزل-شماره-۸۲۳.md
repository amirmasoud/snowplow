---
title: >-
    غزل شمارهٔ ۸۲۳
---
# غزل شمارهٔ ۸۲۳

<div class="b" id="bn1"><div class="m1"><p>سادگی دل را اسیر فکرهای خام داشت</p></div>
<div class="m2"><p>تا تحیر بود در آیینه عکس آرام داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نمی‌بود آرزو تشویش جانکاهی نبود</p></div>
<div class="m2"><p>ماهیان را تشنهٔ قلاب حرص کام داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از ادای ابرویت لطف نگه فهمیده‌ایم</p></div>
<div class="m2"><p>این کمان رنگ فریب از روغن بادام داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل نه امروز از صفا فال صبوحی می‌زند</p></div>
<div class="m2"><p>در کدورت نیز این آیینه عیش شام داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما ز خودداری عبث خون طلب‌ها ریختیم</p></div>
<div class="m2"><p>در صدای بال بسمل عافیت پیغام داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل مصفاکردن از بشم به طوف جلوه برد</p></div>
<div class="m2"><p>آینه بر دوش حیرت جامهٔ احرام داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی‌پر و بالی تپش فرسوده پرواز نیست</p></div>
<div class="m2"><p>هرکسی ایجا به قدر عاجزی آرام داشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در نقاب اشکم آخرحسرت دل قطره زد</p></div>
<div class="m2"><p>رنگ صهبا پای گردیدن به طبع‌ جام داشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون‌ عرق زین ‌نقد ایثاری‌ که‌ آب ‌است‌ از حیا</p></div>
<div class="m2"><p>ما اداکردیم هرکس از خجالت وام داشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بس که بیدل بر طبایع حرص شهرت غالب است</p></div>
<div class="m2"><p>جان‌کَنی‌ها سنگ هم در آرزوی نام داشت</p></div></div>