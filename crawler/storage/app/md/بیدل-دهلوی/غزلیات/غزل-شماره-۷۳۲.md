---
title: >-
    غزل شمارهٔ ۷۳۲
---
# غزل شمارهٔ ۷۳۲

<div class="b" id="bn1"><div class="m1"><p>چارهٔ دردسر دیر محبت جلی‌ست</p></div>
<div class="m2"><p>شمع صفت عمرهاست قشقهٔ ما صندلی‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رابط اجزای وهم یک مژه بربستن است</p></div>
<div class="m2"><p>تا به دوچشم است‌کار علم و عیان احولی‌ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آینهٔ راز دل آن همه روشن نشد</p></div>
<div class="m2"><p>چاک‌گریبان همین یک دو الف صیقلی‌ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به‌که ؛ لب نگذرد زمزمهٔ احتیاج</p></div>
<div class="m2"><p>خون قناعت مریز ناله رگ ممتلی‌ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نام تکلف مباد ننگ تک و‌تاز مرد</p></div>
<div class="m2"><p>ششجهتت خواب پاست‌کفش اگر مخملی‌ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کلفت فردا همان دی شمر آزاد باش</p></div>
<div class="m2"><p>آنچه به تفصیل آن منتظری مجملی‌ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مطرب دل‌گر زند زخمه به قانون شوق</p></div>
<div class="m2"><p>صور به صد شور حشر زمزمهٔ یللی‌ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لمعهٔ مهر ازل تا نفرازد علم</p></div>
<div class="m2"><p>ای به دلایل مثل نور شبت مشعلی‌ست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر خط تحریرعشق شورحواشی مبند</p></div>
<div class="m2"><p>متن رموز ادب از لب ما جدولی‌ست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل از اسرارعشق‌گوش ولب آگاه نیست</p></div>
<div class="m2"><p>فهم‌کن ودم مزن حرف نبی یا ولی‌ست</p></div></div>