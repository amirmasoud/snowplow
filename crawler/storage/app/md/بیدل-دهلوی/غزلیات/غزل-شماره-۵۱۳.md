---
title: >-
    غزل شمارهٔ ۵۱۳
---
# غزل شمارهٔ ۵۱۳

<div class="b" id="bn1"><div class="m1"><p>احتیاجی با مزاج سبزه وگل شامل است</p></div>
<div class="m2"><p>هرچه می‌روید ازین صحرا زبان سایل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اعتبارات غنا و فقر ما پیداست چیست</p></div>
<div class="m2"><p>خاک از آشفتن غبارست و به جمعیت‌گل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وحشت بحر از شکست موج ظاهر می‌شود</p></div>
<div class="m2"><p>رنگ روی عشقبازان‌گرد پرواز دل است‌</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی‌گداز خویش باید دست شست از اعتبار</p></div>
<div class="m2"><p>هرکه درخود می‌زند آتش چراغ محفل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صیدگاه‌کیست این‌گلشن‌که هر سو بنگری</p></div>
<div class="m2"><p>آب و رنگ‌گل پرافشانتر ز خون بسمل است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرچه می‌بینم سراغی از خیالش می‌دهد</p></div>
<div class="m2"><p>پیش مجنون وادی امکان غبار محمل است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیل بنیاد تحیر حسرت دیدارکیست‌</p></div>
<div class="m2"><p>جوهرآیینه چون اشکم‌چکیدن مایل است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیستی شاید به داد اضطراب ما رسد</p></div>
<div class="m2"><p>شعله را بی‌سعی خاکسترتسلی مشکل است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا نگردید آفت آسایشم نیرنگ هوش</p></div>
<div class="m2"><p>زین معما بیخبر بودم‌که مجنون عاقل است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ازتلاش عافیت بگذرکه در دریای عشق</p></div>
<div class="m2"><p>هرکجا بی‌دست‌و پایی‌جلوه‌گر شدساحل است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کوشش ما مانع سرمنزل مقصود ماست</p></div>
<div class="m2"><p>در میان بسمل و راحت تپیدن حایل است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باطن آسوده ازیک حرف بر هم می‌خورد</p></div>
<div class="m2"><p>غنچه تا خواهد نفس‌بر لب‌رساند بیدل است</p></div></div>