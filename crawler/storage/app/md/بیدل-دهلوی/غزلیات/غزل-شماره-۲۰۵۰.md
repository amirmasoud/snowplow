---
title: >-
    غزل شمارهٔ ۲۰۵۰
---
# غزل شمارهٔ ۲۰۵۰

<div class="b" id="bn1"><div class="m1"><p>ای دلت حسرت‌ کمین انتخاب صبحدم</p></div>
<div class="m2"><p>نقطه‌ای از اشک کن اندرکتاب صبحدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمر در اظهار شوخی پر تنک سرمایه است</p></div>
<div class="m2"><p>یک نفس تاکی فروشد پیچ و تاب صبحدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تیره‌روزان جنون‌ را هست بی‌انداز چرخ</p></div>
<div class="m2"><p>چاک دل‌، صبح طرب‌؛ داغ‌، آفتاب صبحدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر دل افسرده داغ انتظار فیض نیست</p></div>
<div class="m2"><p>آفتابست آنکه می‌بینی لباب صبحدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وحشت ما بر تعلق دامنی افشانده است</p></div>
<div class="m2"><p>تکمه نتوان یافت در بند نقاب صبحدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عالم فرصت ندارد از غبار ما سراغ</p></div>
<div class="m2"><p>می‌دود این ریشه یکسر در رکاب صبحدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آسمان‌گر بی‌حسد می‌بود در ایثار فیض</p></div>
<div class="m2"><p>دیده‌های اخترش می‌داشت تاب صبحدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رنج الفت را علاج از غیر جستن آفت است</p></div>
<div class="m2"><p>رعشه بر مخمور می می‌بندد آب صبحدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نشئهٔ غفلت به هر رنگی که باشد مفت ماست</p></div>
<div class="m2"><p>کاش ما را واگذارد دل به خواب صبحدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از توهم چند خواهی زیست مغرور امل</p></div>
<div class="m2"><p>ای نفس گم‌کرده درگرد سراب صبحدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر قدت خم‌کرد پیری راستی مفت صفاست</p></div>
<div class="m2"><p>در دم صدق است بیدل فتح باب صبحدم</p></div></div>