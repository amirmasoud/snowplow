---
title: >-
    غزل شمارهٔ ۱۹۴۰
---
# غزل شمارهٔ ۱۹۴۰

<div class="b" id="bn1"><div class="m1"><p>ای بهار جلوه‌ات را شش جهت دربار گل</p></div>
<div class="m2"><p>بی‌ رخت در دیدهٔ من می‌خلد چون خار گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک نگه نظاره‌ات سر جوش صد میخانه می</p></div>
<div class="m2"><p>یک تبسم‌ کردنت آغوش صد گلزار گل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درگلستانی که بوی وعدهٔ دیدار توست</p></div>
<div class="m2"><p>می‌کند جای نگه چون برگ از اشجارگل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اینقدر در پردهٔ رنگ حنا شوخی‌ کجاست</p></div>
<div class="m2"><p>می‌زند جوش ازکف پایت به این هنجارگل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا به ‌کی پوشد تغافل بر سراپایت نقاب</p></div>
<div class="m2"><p>در دل یک غنچه نتوان یافت این مقدارگل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر رخ هر گلبن از شبنم نقاب افکنده‌اند</p></div>
<div class="m2"><p>تا ز خواب نازگردد بر رخت بیدارگل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست ممکن‌ گر کند در عرض شوخی‌های ناز</p></div>
<div class="m2"><p>لاله‌رویان را عرق بی‌رنگ از رخسارگل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می‌زند در جمع احباب از تقاضای بهار</p></div>
<div class="m2"><p>سایهٔ دست کرم بر گوشهٔ دستار گل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ساز عیش از قلقل مینا قیامت غلغل است</p></div>
<div class="m2"><p>ابر رنگ نغمه می‌بندد به روی تار گل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ریشه‌ها را گر به این سامان نمو بخشد هوا</p></div>
<div class="m2"><p>موی سر چون خامهٔ تصویر آرد بارگل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نوبهارست و طراوت شوخیی دارد به چنگ</p></div>
<div class="m2"><p>بوی‌گل از غنچه‌کرده نغمه از منقارگل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل از اندیشهٔ لعلش به عجزم معترف</p></div>
<div class="m2"><p>می‌کند در عرض جرأت رنگ استغفار گل</p></div></div>