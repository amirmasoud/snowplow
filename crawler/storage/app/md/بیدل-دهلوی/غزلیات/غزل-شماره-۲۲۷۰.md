---
title: >-
    غزل شمارهٔ ۲۲۷۰
---
# غزل شمارهٔ ۲۲۷۰

<div class="b" id="bn1"><div class="m1"><p>دل را به یاد روی کسی یاد می‌کنم</p></div>
<div class="m2"><p>آیینه کرده‌ام گم و فریاد می‌کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوی پیامی از چمن جلوه می‌رسد</p></div>
<div class="m2"><p>از دیده تا دل آینه ایجاد می‌کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاکم به باد می‌رود و آتشم به آب</p></div>
<div class="m2"><p>انشای صلحنامهٔ اضداد کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون صبح بسکه فرصت پرواز نارساست</p></div>
<div class="m2"><p>رنگ پریده را نفس امداد می‌کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>علم و عمل فسانهٔ تمهید خواب کیست</p></div>
<div class="m2"><p>عمریست هر چه می‌شنوم یاد می‌کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قد خمیده نسخهٔ تدبیر جانکنی است</p></div>
<div class="m2"><p>سر گوشیی به تیشهٔ فرهاد می‌کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در ضمن ناله‌ای که دل از یاس می‌کشد</p></div>
<div class="m2"><p>پروازهاست کز پرش آزاد می‌کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>افسانهٔ تظلم حیرت شنیدنی است</p></div>
<div class="m2"><p>دست بلندی از مژه ایجاد می‌کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل آب گشت و خجلت جان سختی‌ام نرفت</p></div>
<div class="m2"><p>آیینه می‌گدازم و فولاد می‌کنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مینای دل به ذوق خیالی شکسته‌ام</p></div>
<div class="m2"><p>آرایش جهان پریزاد می‌کنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کیفیت میان تو باغ تصور است</p></div>
<div class="m2"><p>مو در دماغ خامهٔ بهزاد می‌کنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل خرابی‌ام نفس وحشتست و بس</p></div>
<div class="m2"><p>دل نام عالمی‌که من آباد می‌کنم</p></div></div>