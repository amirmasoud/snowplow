---
title: >-
    غزل شمارهٔ ۵۲۴
---
# غزل شمارهٔ ۵۲۴

<div class="b" id="bn1"><div class="m1"><p>در خیال‌آباد راحت آگهی نامحرم است</p></div>
<div class="m2"><p>جلوه‌ننماید بهشت آنجاکه جنس‌آدم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در نظرهاگرد حیرت در نفسها شور عجز</p></div>
<div class="m2"><p>سازبزم زندگانی را همین زبر وبم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پادشاهی در طلسم سیر چشمی بسته‌اند</p></div>
<div class="m2"><p>کاسهٔ چشم‌گداگرپر شود جام جم است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دو تاگشتن ندارد چاره نخل میوه‌دار</p></div>
<div class="m2"><p>قامت هرکس به زیربار می‌آید خم است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یأس تمهید است این امیدها هشیار باش</p></div>
<div class="m2"><p>هرقدر عرض اهلها بیش‌، فرصتهاکم است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با فروغ جلوه‌ات نظارگی را تاب‌کو</p></div>
<div class="m2"><p>رنک‌چون‌آتش‌افروزد سپندش‌شبنم است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بنای حیرت ازحسن تو می‌بینم خلل</p></div>
<div class="m2"><p>خانهٔ آیینه هم برپا به دیوار نم است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درس‌عبرتهای ما را نسخه‌ای درکار نیست</p></div>
<div class="m2"><p>چشم‌آهو را سواد خویش‌سرمشق‌رم است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا نفس باقی‌ست ظالم نیست بی‌فکر فساد</p></div>
<div class="m2"><p>گوشه‌گیر فتنه می‌باشدکمان را تا دم است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شعله هرجا می‌شود سرگرم تعمیرغرور</p></div>
<div class="m2"><p>داغ‌می‌خنددکه همواری‌بنایی‌محکم است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دوستان حاشاکه ربط الفت هم بگسلند</p></div>
<div class="m2"><p>موجها را رفتن‌از خود هم در آغوش‌هم است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نامداریها گرفتاری‌ست در دام بلا</p></div>
<div class="m2"><p>بیدل انگشت شهان را طوق‌گردن خاتم است</p></div></div>