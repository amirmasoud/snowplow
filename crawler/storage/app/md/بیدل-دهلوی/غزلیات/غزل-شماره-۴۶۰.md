---
title: >-
    غزل شمارهٔ ۴۶۰
---
# غزل شمارهٔ ۴۶۰

<div class="b" id="bn1"><div class="m1"><p>سوخت دل در محفل تسلیم و از جا برنخاست</p></div>
<div class="m2"><p>شمع را آتش ز سر برخاست ازپا برنخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در تماشاگاه عبرت پر ضعیف افتاده ایم</p></div>
<div class="m2"><p>بی‌عصا هرچند مژگان بود از ما برنخاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌رود خلق از خود و برجاست آثار قدم</p></div>
<div class="m2"><p>عالمی عنقا شد وگردی ز عنقا برنخاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا به قصرکبریا چندین فلک طی‌کردن‌ست</p></div>
<div class="m2"><p>نردبانی چند بیش آنجا مسیحا برنخاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آسمان هم اعتباری دارد از آزادگی</p></div>
<div class="m2"><p>کرکسی برخاست از دنیا ز دنیا برنخاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیدماغی دیگر است و عرض همتها دگر</p></div>
<div class="m2"><p>از جهان ‌زینسان که‌ دل ‌برخاست‌ گویا برنخاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پا به سنگ و دعوی پرواز ننگ اگهی‌ ست</p></div>
<div class="m2"><p>نام هرگز جز در افواه از نگینها برنخاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما و من از صاف‌طبعان انفعال فطرت است</p></div>
<div class="m2"><p>تا فرو ناورد سر، قلقل ز مینا برنخاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تهمت وضع غرور از ناتوانی می‌کشیم</p></div>
<div class="m2"><p>ناله تعظیم غم دل بود از ما برنخاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دامن دل از غبار آه چین پیدا نکرد</p></div>
<div class="m2"><p>از تلاش ‌گربادی چند صحرا برنخاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل از نشو و نمای ما کسی آگاه نیست</p></div>
<div class="m2"><p>آبله نبر قدم فرسوده شد پا برنخاست</p></div></div>