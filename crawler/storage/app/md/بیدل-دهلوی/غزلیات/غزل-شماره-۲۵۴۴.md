---
title: >-
    غزل شمارهٔ ۲۵۴۴
---
# غزل شمارهٔ ۲۵۴۴

<div class="b" id="bn1"><div class="m1"><p>در خور گل‌ کردن فقرست استغنای من</p></div>
<div class="m2"><p>نیست جز دست تهی صفر غرورافزای من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از مراد هر دو عالم بسکه بیرون جسته‌ام</p></div>
<div class="m2"><p>در غبار وحشت دی می‌تپد فردای من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سایهٔ مویی زکلک خود تصورکرد وبس</p></div>
<div class="m2"><p>نقشبند وهم در صنع ضعیفیهای من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترک دنیا هم دماغ همت من بر نداشت</p></div>
<div class="m2"><p>رنجه‌کرد افشاندن این‌گرد پشت پای من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشت خاکم لیک در عرض بهار رنگ و بو</p></div>
<div class="m2"><p>عالمی آیینه می‌پردازد از سیمای من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقش مهرخامشی چون موج برخود می‌تپد</p></div>
<div class="m2"><p>در محیط حسرت طبع سخن پیرای من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پردهٔ ناموس بیرنگی‌ست شوخیهای رنگ</p></div>
<div class="m2"><p>می‌دری جیب پری‌گر بشکنی مینای من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از سبکروحی درون خانه بیرونم ز خوبش</p></div>
<div class="m2"><p>چون نگه در دیده‌ها خالیست از من جای من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اینقدرها لالهٔ گلزار سودای کی‌ام</p></div>
<div class="m2"><p>بی‌چراغان نیست دشت و در ز نقش پای من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عمرها شد حسرتم خون گشتهٔ پابوس اوست</p></div>
<div class="m2"><p>صفحه می‌باید حنایی‌کردن از انشای من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یاد ایامی‌ که از آهنگ زنجیر جنون</p></div>
<div class="m2"><p>کوچهٔ نی بود یکسر جاده در صحرای من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شمع این محفل نی‌ام لیک از هجوم بیخودی</p></div>
<div class="m2"><p>در رکاب رنگ از جا رفته است اجزای من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هیچکس خجلت نقاب ربط‌ کمظرفان مباد</p></div>
<div class="m2"><p>نشئه عمری شد عرق می‌چیند از صهبای من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کرد بیدل سرخون جمعیتم آخر چوشمع</p></div>
<div class="m2"><p>داغ جانکاهی همان ته جرعهٔ مینای من</p></div></div>