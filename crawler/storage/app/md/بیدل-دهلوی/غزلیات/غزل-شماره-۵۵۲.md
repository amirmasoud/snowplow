---
title: >-
    غزل شمارهٔ ۵۵۲
---
# غزل شمارهٔ ۵۵۲

<div class="b" id="bn1"><div class="m1"><p>بزم‌گردون صبح‌خیز ازگرد بیتاب من است</p></div>
<div class="m2"><p>نور این آیینهٔ مینا ز سیماب من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک‌جهان ضبط نفس دارد به خود پیچیدنم</p></div>
<div class="m2"><p>رشتهٔ‌موهوم هستی تشنهٔ‌ناب‌من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا تغافل دارم از وضع جهان آسوده‌ام</p></div>
<div class="m2"><p>چشم‌پوشیدن بساط‌آرایی خواب‌من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درخور وارستگی مسندطراز عزتم</p></div>
<div class="m2"><p>بال پروازم چو قمری فرش سنجاب من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موبه مویم چشمهٔ برق تجلیهای اوست</p></div>
<div class="m2"><p>طور اگر آتش فروزدکرم شبتاب من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از مزاج‌گوهرم شوخی نمی‌بالد به خویش</p></div>
<div class="m2"><p>موج عمری‌شد به توفان بردهٔ آب من است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جوش دردی‌کوکه هنگ اثر پیداکنم</p></div>
<div class="m2"><p>رشتهٔ قانون آهم‌، یأس مضراب من است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>محو شوقم از غم اسباب راحت فارغم</p></div>
<div class="m2"><p>صافی آیینه حیرت شکر خواب من است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می‌برد جذب خرامت چون غبار از جا مرا</p></div>
<div class="m2"><p>جلوه‌ای از چین دامان تو قلاب من است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عمرها شد زین شبستان انتخابی می‌زنم</p></div>
<div class="m2"><p>هرکجا حیرانیی‌گل‌کرد مهتاب من است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر طرف پر می‌زند نظاره حیرت خفته است</p></div>
<div class="m2"><p>عالم آیینه‌ام‌، همواری اسباب من است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از قماش خامشی بیدل دکانی چیدم</p></div>
<div class="m2"><p>هرچه غیر از خودفروشیها بود باب من است</p></div></div>