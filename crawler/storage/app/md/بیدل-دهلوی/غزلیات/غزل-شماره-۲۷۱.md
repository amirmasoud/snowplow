---
title: >-
    غزل شمارهٔ ۲۷۱
---
# غزل شمارهٔ ۲۷۱

<div class="b" id="bn1"><div class="m1"><p>شفق در خون حسرت می‌تپد از دیدن مینا</p></div>
<div class="m2"><p>عقیق آب روان می‌گردد از خندیدن مینا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جگرها بر زمین می‌ریزد ازکف رفتن ساغر</p></div>
<div class="m2"><p>دلی در زیر پا دارد به سر غلتیدن مینا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنال از درد غفلت آنقدرکز خود برون آیی</p></div>
<div class="m2"><p>به قدرقلقل است ازخویش دامن چیدن مینا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سراغ عیش ازین محفل مجوکز جوش‌دلتنگی</p></div>
<div class="m2"><p>صدای‌گریه پیچیده‌ست بر خندیدن مینا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تنک‌سرمایه است‌آن‌دل‌که‌شد آسودگی‌سازش</p></div>
<div class="m2"><p>به بی‌مغزی دلیلی نیست جز خوابیدن مینا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به سعی بیخودی قلقل نوای ساز نیرنگم</p></div>
<div class="m2"><p>شکست رنگ دارد اینقدر نالیدن مینا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رعونت در مزاج می‌پرستان ره نمی‌یابد</p></div>
<div class="m2"><p>چه امکان است از تسلیم سر پیچیدن مینا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نزاکت هم درتن محفل به‌کف آسان نمی‌آید</p></div>
<div class="m2"><p>گداز سنگ می‌خواهد به خود بالیدن مینا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بساط ناز چیدم هرقدرکز خود برون رفتم</p></div>
<div class="m2"><p>پری بالید در خورد تهی‌گردیدن مینا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خموشی چند، طبع اهل معنی تازه‌کن بیدل</p></div>
<div class="m2"><p>به مخموران ستم دارد نفس دزدیدن مینا</p></div></div>