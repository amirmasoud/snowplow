---
title: >-
    غزل شمارهٔ ۱۱۹۸
---
# غزل شمارهٔ ۱۱۹۸

<div class="b" id="bn1"><div class="m1"><p>بپرهیز از حسد تا فضل یزدانت قرین باشد</p></div>
<div class="m2"><p>که مرحوم است آدم هرقدر شیطان لعین باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگو در جوش خط افزونی حسن‌است خوبان را</p></div>
<div class="m2"><p>زبان‌کفر هرجا شد دراز از نقص دین باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>محبت محوکرد از دل غبار وهم اسبابم</p></div>
<div class="m2"><p>به‌پیش شعله‌کی از چهرهٔ خاشاک چین باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمایانم به رنگ سایه از جیب سیه‌روزی</p></div>
<div class="m2"><p>چه باشد رنگ من یارب اگر آیینه ین باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به صد مژگان فشاندن گرد اشکی رفته‌ام از دل</p></div>
<div class="m2"><p>من و نقدی ‌که بیرون راندهٔ صد آستین باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به لوح حیرتم ثبت است رمز پردهٔ امکان</p></div>
<div class="m2"><p>مثال خوب و زشت‌ آبینه را نقش نگین باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آن مزرع‌که حسنت خرمن‌آرای عرق‌گردد</p></div>
<div class="m2"><p>به‌ پروین می‌رساند ریشه ‌هر کس خوشه‌چین باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نسیم از خاک‌کویت‌گر غباری بر سرم ریزد</p></div>
<div class="m2"><p>به‌کام آرزویم حاصل روی زمین باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ندارد دامن دشت جنون از گرد پروایی</p></div>
<div class="m2"><p>دل عاشق چرا از طعنهٔ مردم حزین باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دو روزی از هوس تاریکی دنیا گواراکن</p></div>
<div class="m2"><p>چراغ خانهٔ زنبور ذوق انگبین باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کف دست توانایی به سودنها نمی‌ارزد</p></div>
<div class="m2"><p>مکن کاری که انجامش ندامت‌آفرین باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز سیر آف و رنگ این چمن دل جمع‌ کن بیدل</p></div>
<div class="m2"><p>که هر جا غنچه ‌گردیدی ‌گلت در آستین باشد</p></div></div>