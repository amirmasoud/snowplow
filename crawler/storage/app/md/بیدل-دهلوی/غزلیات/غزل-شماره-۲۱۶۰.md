---
title: >-
    غزل شمارهٔ ۲۱۶۰
---
# غزل شمارهٔ ۲۱۶۰

<div class="b" id="bn1"><div class="m1"><p>عمری‌ست ز اسباب غنا هیچ ندارم</p></div>
<div class="m2"><p>چون دست تهی غیر دعا هیچ‌ ندارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تحریک لبی بود اثر مایهٔ ایجاد</p></div>
<div class="m2"><p>معذورم اگر جز من و ما هیچ ندارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تشویق خیالات وجود و عدمم نیست</p></div>
<div class="m2"><p>چون رمز دهانت همه جا هیچ ندارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا رب چقدر گرم‌ کنم مجلس تصویر</p></div>
<div class="m2"><p>سازم همه ‌کوک است و صدا هیچ ندارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون شمع اگر شش جهتم پی سپر افتد</p></div>
<div class="m2"><p>غیر از سر خود در ته پا هیچ ندارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وامانده یأسم که از این انجمن آخر</p></div>
<div class="m2"><p>برخاستنی هست و عصا هیچ ندارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مغرور هوس می‌زیم از هستی موهوم</p></div>
<div class="m2"><p>فریاد که من شرم و حیا هیچ ندارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همکسوت اسباب حبابم چه توان‌ کرد</p></div>
<div class="m2"><p>گر باز کنم بند قبا هیچ ندارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شخص عدم از زحمت تمثال مبراست</p></div>
<div class="m2"><p>آیینه‌! تو هیچم منما هیچ ندارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل اگر آفاق بود زیر نگینم</p></div>
<div class="m2"><p>جز نام خدا نام خدا هیچ ندارم</p></div></div>