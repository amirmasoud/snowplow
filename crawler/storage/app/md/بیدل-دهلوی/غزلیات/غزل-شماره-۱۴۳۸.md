---
title: >-
    غزل شمارهٔ ۱۴۳۸
---
# غزل شمارهٔ ۱۴۳۸

<div class="b" id="bn1"><div class="m1"><p>بس که زخم‌ کشتهٔ نازش تلاطم می‌کند</p></div>
<div class="m2"><p>هرچه را دیدم درین مشهد تبسم می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم بگشا بر حصول جستجو کاینجا چو شمع</p></div>
<div class="m2"><p>نقد خود هرکس به قدر یافتن ‌گم می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پختگان دامن ز قید تن‌پرستی چیده‌اند</p></div>
<div class="m2"><p>باده‌ات از خام‌‌جوشی خدمت خم می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچکس از بی‌تکلف زیستن آگاه نیست</p></div>
<div class="m2"><p>آدمی بودن خلل در عیش مردم می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زین ‌نفس سوزی ‌که دارد خلق ‌بر طاق‌ و سرا</p></div>
<div class="m2"><p>سعی عبرت‌بافی‌ کرم بریشم می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش‌بینی‌ کن ز ننگ حسرت ماضی برآ</p></div>
<div class="m2"><p>بر قفا نظاره‌ کردن ریش را دم می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دهر لبریز مکافات‌ست اما کو تمیز</p></div>
<div class="m2"><p>کم‌کسی اینجا به حال خود ترحم می‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از ادبگاه خموشی گوش باید وام کرد</p></div>
<div class="m2"><p>سرمه‌گون‌ چشمی درین ‌مخمل‌ تکلم می‌کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرکجا باشد قناعت آبیار اتفاق</p></div>
<div class="m2"><p>پهلوی از نان تهی ایجاد گندم می‌کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رحم بر بی‌مغزی ما کن ‌که این نقش حباب</p></div>
<div class="m2"><p>خویش را آیینهٔ دریا توهم می‌کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل از بس بی‌نم افتاده است بحر اعتبار</p></div>
<div class="m2"><p>گوهر از گرد یتیمی‌ها تیمم می‌کند</p></div></div>