---
title: >-
    غزل شمارهٔ ۶۷۰
---
# غزل شمارهٔ ۶۷۰

<div class="b" id="bn1"><div class="m1"><p>فردوس دل‌، اسیر خیال تو بودنست</p></div>
<div class="m2"><p>عید نگاه‌، چشم به رویت‌ گشودنست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شادم به هجر هم ‌که به این یک دم انتظار</p></div>
<div class="m2"><p>حرف لب توام ز تمنا شنودنست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معراج آرزوی دو عالم حضور من</p></div>
<div class="m2"><p>یک سجده‌وار جبهه به پای تو سودنست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یاد فنا مرا به خیال تو داغ کرد</p></div>
<div class="m2"><p>آه از پری ‌که شیشه به سنگ آزمودنست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آسان مگیر، دیدن تمثال ما و من</p></div>
<div class="m2"><p>زنگ نفس‌ ز آینهٔ دل زدودنست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرها فتاده است دین ره به هر قدم</p></div>
<div class="m2"><p>از شرم پیش پا مژه‌ای خم نمودنست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داغ فشار غفلت ما هیچکس مباد</p></div>
<div class="m2"><p>چشمی گشوده‌ایم‌که ننگ غنودنست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این است اگر حقیقت اقبال ناکسی</p></div>
<div class="m2"><p>درحق ما عقوبت نفرین ستودنست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در دفتر محاسبهٔ اعتبار ما</p></div>
<div class="m2"><p>بر هیچ یک دو صفر دگر هم فزودنست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل غبار ما ز چه دامن جدا فتاد</p></div>
<div class="m2"><p>بر باد رفته‌ایم و همان دست سودنست</p></div></div>