---
title: >-
    غزل شمارهٔ ۱۱۸۱
---
# غزل شمارهٔ ۱۱۸۱

<div class="b" id="bn1"><div class="m1"><p>تسلی کو اگر منظورت اسباب هوس باشد</p></div>
<div class="m2"><p>ندارد برگ ‌راحت هر که را در دیده خس باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز هستی هرچه اندیشی غبار دل مهیا کن</p></div>
<div class="m2"><p>کسوف آفتاب آیینهٔ عرض نفس باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درین محفل حیا کن تا گلوی ناله نخراشی</p></div>
<div class="m2"><p>نفس‌ هم ‌کم‌ خروشی‌ نیست‌ گر فریادرس باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمی‌گیرد به غیر از دست و تیغ و دامن قاتل</p></div>
<div class="m2"><p>مرا درکوچه‌های‌زخم رنگ‌خون ‌عسس باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه امکانست ما و جرات پرواز گلزارت</p></div>
<div class="m2"><p>نگاه عاجزان را سایهٔ مژگان قفس باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبالیدیم بر خود ذره‌ای در عرض پیدایی</p></div>
<div class="m2"><p>غبار ما مباد افشانده ی بال مگس باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دل وامانده ای از لاف ما و من تبرا کن</p></div>
<div class="m2"><p>مقیم خانهٔ آیینه باید بی‌نفس باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه لازم تنگ ‌گیرد آسمان ارباب معنی را</p></div>
<div class="m2"><p>شکخ‌ما همان مضمورن‌که نتوان بست بس باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مکن ساز اقامت تا غبار خویش بشکافی</p></div>
<div class="m2"><p>نفس پر می‌فشاند شاید آواز جرس باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شکست‌ رنگ امیدی‌ست ‌سر تا پای‌ ما بیدل</p></div>
<div class="m2"><p>ز سیر ما مشو غافل اگر عبرت هوس باشد</p></div></div>