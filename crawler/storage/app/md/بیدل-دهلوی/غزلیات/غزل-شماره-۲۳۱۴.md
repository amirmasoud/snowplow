---
title: >-
    غزل شمارهٔ ۲۳۱۴
---
# غزل شمارهٔ ۲۳۱۴

<div class="b" id="bn1"><div class="m1"><p>به رنگ خامه ز بس ناتوانی اجزایم</p></div>
<div class="m2"><p>به سودن مژه فرسوده شد سراپایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در این محیط مقیم تغافلم چو حباب</p></div>
<div class="m2"><p>غبار چشم گشودن تهی کند جایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حریف مطلب اشک چکیده نتوان شد</p></div>
<div class="m2"><p>صدا شکست نفس در شکست مینایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شرار مرده‌ام از حشر من مگوی و مپرس</p></div>
<div class="m2"><p>چنان گذشته‌ام از خود که نیست فردایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سحر طرازی گلزار حیرتست امروز</p></div>
<div class="m2"><p>شکسته رنگی آیینهٔ تماشایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیال هستی موهوم سرخوشم دارد</p></div>
<div class="m2"><p>وگرنه در رگ تاکست موج صهبایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو عمر رفته ندارم امید برگشتن</p></div>
<div class="m2"><p>غنیمت است ‌که‌ گاهی به یاد می‌آیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی خیال چه هستی‌کند ز وضع حباب</p></div>
<div class="m2"><p>شکافته است به نام عدم معمایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هزار رنگ ز من پر فشان بیرنگی‌ست</p></div>
<div class="m2"><p>اگر غلط نکنم آشیان عنقایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غرور خودسری آیینهٔ نمودم نیست</p></div>
<div class="m2"><p>چو انفعال عرق کرده است پیدایم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طواف دشت جنون ذوق سجده‌ای دارد</p></div>
<div class="m2"><p>که جای آبله دل می‌کشد سر از پایم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نگاه چاره ندارد ز مردمک بیدل</p></div>
<div class="m2"><p>نشانده است جنون در دل سویدایم</p></div></div>