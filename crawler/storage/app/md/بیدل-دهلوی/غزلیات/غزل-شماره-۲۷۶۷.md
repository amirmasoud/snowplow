---
title: >-
    غزل شمارهٔ ۲۷۶۷
---
# غزل شمارهٔ ۲۷۶۷

<div class="b" id="bn1"><div class="m1"><p>درین حدیقه‌ نه‌ای قدردان حیرانی</p></div>
<div class="m2"><p>به شوخی مژه ترسم ورق بگردانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به‌کار عشق نظرکن شکست دل درباب</p></div>
<div class="m2"><p>ز موج سیل عیانست حسن حیرانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صداع هستی ما را علاج تسلیم است</p></div>
<div class="m2"><p>بس است صندل اگر سوده‌ایم پیشانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خویش رفتن ما محملی نمی‌خواهد</p></div>
<div class="m2"><p>سحر به دوش نفس بسته است آسانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به عالمی‌که خیال تو نقش می‌بندد</p></div>
<div class="m2"><p>نفس نمی‌کشد از شرم خامهٔ مانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جماعتی‌که به بزم خیال محو تواند</p></div>
<div class="m2"><p>هزار آینه دارتد غیر حیرانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیال حلقهٔ زلف تو ساغری دارد</p></div>
<div class="m2"><p>که رنگ نشئهٔ آن نیست‌جز پریشانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خراب آینه رنگ بنای مجنونم</p></div>
<div class="m2"><p>فلک در آب وگلم صرف‌کرده ویرانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کدام عرصه که لبریز اضطرابم نیست</p></div>
<div class="m2"><p>جهان گرفت غبار من از پر افشانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو ناله سخت نهان‌ست صورت حالم</p></div>
<div class="m2"><p>برون ز خویش روم ‌تا رسم به عریانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ندامتم ز تردد چو موج باز نداشت</p></div>
<div class="m2"><p>کفی نسوده‌ام الا به ناپشیمانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به عافیت نتوان نقش این بساط شدن</p></div>
<div class="m2"><p>مگر به سعی فنا گرد خویش بنشانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نیرزد آینه بودن به آنهمه تشویش</p></div>
<div class="m2"><p>که هرکه جلوه فروشد تو رنگ‌گردانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گل است خاک بیابان آرزو بیدل</p></div>
<div class="m2"><p>چو گرد باد مگر ناقه بر هوا رانی</p></div></div>