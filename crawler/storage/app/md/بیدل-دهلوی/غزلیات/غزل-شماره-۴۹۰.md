---
title: >-
    غزل شمارهٔ ۴۹۰
---
# غزل شمارهٔ ۴۹۰

<div class="b" id="bn1"><div class="m1"><p>ما را به راه عشق طلب رهنما بس است</p></div>
<div class="m2"><p>جایی ‌که نیست قبله‌نما نقش پا بس است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جنس نگه زهرکه بود جلوه سود ما</p></div>
<div class="m2"><p>سرمایه بهرآینه‌کسب صفا بس است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ننشست اگر به پهلوی‌، ما تیر او، ز ناز</p></div>
<div class="m2"><p>نقشی به حسرتش‌، ز نی بوربا بس است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرگشته‌ای‌ که دامن همت کشد ز دهر</p></div>
<div class="m2"><p>بر دوش عمر چون فلکش یک ردا بس است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گو سرمه عبرت آینهٔ دیده‌ها مباش</p></div>
<div class="m2"><p>ما را خیال خاک شدن توتیا بس است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک دم زدن به خاک نشاند سپند را</p></div>
<div class="m2"><p>هرچند ناله هیچ ندارد مرا بس است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر مدعا ز جادهٔ اوهام جستن است</p></div>
<div class="m2"><p>یک اشک لغزش ‌تو فنا تا بقا بس است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منت‌کش نسیم نشد غنچهٔ حباب</p></div>
<div class="m2"><p>ما را همان شکسته‌ دلی دلگشا بس است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آخرسری به منزل مقصود می‌کشیم</p></div>
<div class="m2"><p>افتادگی چو جاده در این ره عصا بس است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یارب مکن به بار دگر امتحان ما</p></div>
<div class="m2"><p>برداشتیم پیش تو دست دعا بس است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عرض شکست دل به زبان احتیاج نیست</p></div>
<div class="m2"><p>رنگ شکسته آینهٔ حال ما بس است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل دماغ دردسر این و آن کراست</p></div>
<div class="m2"><p>با خویش هم ا‌گر شده‌ایم آشنا بس است</p></div></div>