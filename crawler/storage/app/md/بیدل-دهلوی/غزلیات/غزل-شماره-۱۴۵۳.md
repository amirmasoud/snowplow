---
title: >-
    غزل شمارهٔ ۱۴۵۳
---
# غزل شمارهٔ ۱۴۵۳

<div class="b" id="bn1"><div class="m1"><p>رفته رفته عافیت هم‌کینه‌خواهی می‌کند</p></div>
<div class="m2"><p>ساحل آخر کشتی ما را تباهی می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوستان بر موی پیری اعتماد عیش چند</p></div>
<div class="m2"><p>خانه‌ها روشن چراغ صبحگاهی می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آسمان زین دور مفعولی‌ که ننگ دورهاست</p></div>
<div class="m2"><p>اختلاط خلق را معجون باهی می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرزه‌گویی بسکه در اهل تعین غالب‌ست</p></div>
<div class="m2"><p>لطف معنی را به لب نگذشته واهی ‌می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاختلاط خشک‌طبعان محو مژگان می‌شود</p></div>
<div class="m2"><p>خامه هم هرچند اشک از د‌یده راهی می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیر گردیدیم حکم ضعف باید پیش برد</p></div>
<div class="m2"><p>قامت خم‌ گشته بر ما کجکلاهی می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست بی‌جوهر نیام از پهلوی اقبال تیغ</p></div>
<div class="m2"><p>صحبت مردان محنت را سپاهی می‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حسن می‌داند تقاضای جنون عاشقان</p></div>
<div class="m2"><p>گر تغافل می‌نماید عذرخواهی می‌کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بس که پیشیم از گروتازان میدان امل</p></div>
<div class="m2"><p>باد محشر هم قفای ما سیاهی می کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در گلستانی‌ که حرف سرو او گردد بلند</p></div>
<div class="m2"><p>گر همه طوبی سر افرازد گیاهی می‌کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون حیا غالب شود از لاف نتوان ‌دم زدن</p></div>
<div class="m2"><p>هرکه باشد زیر آب آواز ماهی می‌کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیست ممکن بیدل اصلاح طبایع جز به فقر</p></div>
<div class="m2"><p>خلق را آدم همین بیدستگاهی می‌کند</p></div></div>