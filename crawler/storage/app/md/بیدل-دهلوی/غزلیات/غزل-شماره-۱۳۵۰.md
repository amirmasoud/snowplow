---
title: >-
    غزل شمارهٔ ۱۳۵۰
---
# غزل شمارهٔ ۱۳۵۰

<div class="b" id="bn1"><div class="m1"><p>چشم چون آیینه برنیرنگ عرض نازبند</p></div>
<div class="m2"><p>ساغر بزم تحیر شو لب از آواز بند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>موج آب‌ گوهر از ننگ تپیدن فارغ است</p></div>
<div class="m2"><p>لاف عزلت می‌زنی بال و پر پرواز بند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غنچه دیوان در بغل از سر به زانو بستن است</p></div>
<div class="m2"><p>ای بهار فکر مضمونی به ابن انداز بند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خارج آهنگ بساط‌ کفر و ایمانت ‌که‌ کرد</p></div>
<div class="m2"><p>بی‌تکلف خویش را چون نغمه برهرسازبند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خرده‌گیران ‌تیغ‌برکف پیش‌ و پس استاده‌اند</p></div>
<div class="m2"><p>یک‌نفس چون‌شمع خامش‌شو زبان‌گاز بند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برطلسم غنچه تمهید شکفتن آفت است</p></div>
<div class="m2"><p>عقده‌ای از دل اگر واکرده باشی باز بند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نام هم معراج شوخیهاست پرواز ترا</p></div>
<div class="m2"><p>همچو عنقا آشیان در عالم آواز بند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی‌نیازی از خم و پیچ تعلق رستن است</p></div>
<div class="m2"><p>از سر خود هرچه واگردی به دوش ناز بند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>موج از بی‌طاقتیها کرد ایجاد حباب</p></div>
<div class="m2"><p>بسمل ما را تپش زد بر پر پرواز بند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وصل حق بیدل نظر بربستن است از ماسوا</p></div>
<div class="m2"><p>قرب‌شه خواهی ز عالم‌چشم چون شهباز بند</p></div></div>