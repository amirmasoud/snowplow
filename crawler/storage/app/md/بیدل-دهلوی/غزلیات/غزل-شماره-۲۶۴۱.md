---
title: >-
    غزل شمارهٔ ۲۶۴۱
---
# غزل شمارهٔ ۲۶۴۱

<div class="b" id="bn1"><div class="m1"><p>گر همه رفتی چو ماه از چرخ برتر سجده‌ای</p></div>
<div class="m2"><p>تا ز پیشانی اثر داری برآن در سجده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بندگی را در عدم هم چاره نتوان یافتن</p></div>
<div class="m2"><p>خاک اگرکشتی همان از پای تا سر سجده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لوح اظهار اینقدر تهمت نقوش عاجزی ست</p></div>
<div class="m2"><p>ای همه معنی به جرم خط مسطر سجده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دام تکلیف نیاز توست هرجا منزلی‌ست</p></div>
<div class="m2"><p>یعنی از دیر و حرم تاکوی دلبر سجده ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا نگردد جبهه فرش آشیان نیستی</p></div>
<div class="m2"><p>چون نماز غافلان سیلی خور هر سجده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناله داری سرکشی کن از طلسم خود برآ</p></div>
<div class="m2"><p>ای نمازت ننگ غفلت بر مکرر سجده‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاک‌گردیدی و از وضعت پریشانی نرفت</p></div>
<div class="m2"><p>جمع شو از آب‌گردیدن که ابتر سجده‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در ضعیفی رشتهٔ ساز رعونت بیصداست</p></div>
<div class="m2"><p>از رگ‌گردن غباری نیست تا در سجده‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اوج عزت زیردست پایهٔ عجز است و بس</p></div>
<div class="m2"><p>سرنوشت جبههٔ نیکان شدی‌گر سجده‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی‌نیازیها جبین می‌مالد اینجا بر زمین</p></div>
<div class="m2"><p>ای ز خود غافل نگاهی تا چه جوهر سجده ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هم ز وضع اشک خود بیدل غبار خویش‌گیر</p></div>
<div class="m2"><p>کزگریبان تا برون آورده‌ای سر سجده‌ای</p></div></div>