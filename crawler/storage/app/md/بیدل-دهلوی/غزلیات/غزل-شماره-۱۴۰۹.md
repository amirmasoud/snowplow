---
title: >-
    غزل شمارهٔ ۱۴۰۹
---
# غزل شمارهٔ ۱۴۰۹

<div class="b" id="bn1"><div class="m1"><p>کو جنون تا عقدهٔ هوش از سر ما واکند</p></div>
<div class="m2"><p>وهم هستی را سپند آتش سودا کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بساط خاکدان دهر نتوان یافتن</p></div>
<div class="m2"><p>آن قدر گردی‌ که تعمیر شکست ما کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بعد از این آن به‌ که خاموشی دهد داد سخن</p></div>
<div class="m2"><p>گوهر معنی‌ کسی تا کی زبان‌فرسا کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عجز ما را ترجمان غفلت ما کرده‌اند</p></div>
<div class="m2"><p>تا همان واماندگی تعبیر خواب پا کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برنیاید تا ابد از حیرت شکر نگاه</p></div>
<div class="m2"><p>هرکه چون تصویر بر نقّاش چشمی واکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بادپیمای سبک‌مغزی‌ست هرکس چون حباب</p></div>
<div class="m2"><p>ساغر خود را نگون در مجلس دریا کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بعد عمری آن پری‌ گرم التفات دلبری‌ست</p></div>
<div class="m2"><p>می‌روم از خود مبادا یاد استغنا کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قیمت وصلش ندارد دستگاه کاینات</p></div>
<div class="m2"><p>نقد ما هیچ است شاید هم به ما سودا کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی‌تکلّف صنعت معمار عشقم داغ ‌کرد</p></div>
<div class="m2"><p>کز شکست هر دو عالم ناله‌ای برپا کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی‌بریها را علاجی نیست شاید چون چنار</p></div>
<div class="m2"><p>دست برهم سودن ما آتشی پیدا کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عبرت من چاشنی ‌گیر از شکست عالمی‌ست</p></div>
<div class="m2"><p>هرچه‌ گردد توتیا، چشم مرا بینا کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چاره دشوار است بیدل شوخی نظاره را</p></div>
<div class="m2"><p>شرم حسن او مگر در دیدهٔ ما جا کند</p></div></div>