---
title: >-
    غزل شمارهٔ ۲۰۱۷
---
# غزل شمارهٔ ۲۰۱۷

<div class="b" id="bn1"><div class="m1"><p>به عشقت‌ گر همه یک داغ سامان بود در دستم</p></div>
<div class="m2"><p>همان انگشتر ملک سلیمان بود در دستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین‌گلشن نه‌ گل دیدم نه رمز غنچه فهمیدم</p></div>
<div class="m2"><p>ز دل تا عقده وا شد چشم حیران بود در دستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز غفلت ره نبردم در نزاکت‌خانهٔ هستی</p></div>
<div class="m2"><p>ز نبضم رشته‌واری زلف جانان بود در دستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هر بی‌دستگاهی گر به قسمت می‌شدم قانع</p></div>
<div class="m2"><p>کف خود دامن صحرای امکان بود در دستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندامت داشت یکسر رونق گلزار پیدایی</p></div>
<div class="m2"><p>چوگل آثار شبنم زخم دندان بود در دستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به بالیدن نهال محنتم فرصت نمی‌خواهد</p></div>
<div class="m2"><p>ز پا تا می‌کشیدم خار پیکان بود در دستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پی تحصیل روزی بسکه دیدم سختی دوران</p></div>
<div class="m2"><p>به چشمم آسیا گردید اگر نان بود در دستم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جنون آوارهٔ دیر و حرم عمری‌ست می‌گردم</p></div>
<div class="m2"><p>مکاتیب نفس پر هرزه عنوان بود در دستم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کفی صیقل نزد سودن دین هنگامهٔ عبرت</p></div>
<div class="m2"><p>به حسرت مردم و آیینه پنهان بود در دستم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درین مدت‌که سعی نارسایم بال زد بیدل</p></div>
<div class="m2"><p>همین لغزیدن پایی چو مژگان بود در دستم</p></div></div>