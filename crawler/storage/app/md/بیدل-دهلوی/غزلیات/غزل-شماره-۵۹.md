---
title: >-
    غزل شمارهٔ ۵۹
---
# غزل شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>فال حباب زن‌، بشمر موج آب را</p></div>
<div class="m2"><p>چشمی به صفر گیر و نظر کن حساب را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق از مزاج ما به هوس گشت متهم</p></div>
<div class="m2"><p>در شک گرفت نقطهٔ وهم انتخاب را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر نیست زین قلمرو اوهام عبرتت</p></div>
<div class="m2"><p>آب حیات تشنه لبی‌ کن سیراب را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشمم تحیر آینهٔ نقش پای تست</p></div>
<div class="m2"><p>مپسند خالی از قدمت این رکاب را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عالم تصرف ید بیضا گرفته است</p></div>
<div class="m2"><p>اعجاز دیگر است ز رویت نقاب را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امروز در قلمرو نظاره نور نیست</p></div>
<div class="m2"><p>از بس خطت به سایه نشاند آفتاب را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فیض بهار لغزش مستانه بردنی‌ست</p></div>
<div class="m2"><p>در شیشه‌های آبله میکن گلاب را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اجزای ما چو صبح نفس‌پرور است و بس</p></div>
<div class="m2"><p>شیرازه کرده‌اند به باد این کتاب را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما بیخودان به غفلت خود پی‌ نبرده‌ایم</p></div>
<div class="m2"><p>چشم آشنا نشد که چه رنگ است خواب را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در طینت فسرده‌صفاها کدورت است</p></div>
<div class="m2"><p>آیینه می‌کند همه زنگار آب را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جوش خزانم آینه‌دار بهار اوست</p></div>
<div class="m2"><p>نظاره کن ز چاک کتان ماهتاب را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل به گیرودار نفس آنقدر مناز</p></div>
<div class="m2"><p>آیینه کن شکست کلاه حباب را</p></div></div>