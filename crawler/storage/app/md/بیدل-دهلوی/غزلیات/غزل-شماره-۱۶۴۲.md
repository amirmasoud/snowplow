---
title: >-
    غزل شمارهٔ ۱۶۴۲
---
# غزل شمارهٔ ۱۶۴۲

<div class="b" id="bn1"><div class="m1"><p>کو رنگ‌، چه بو؟ جلوهٔ یارست ببینید</p></div>
<div class="m2"><p>گل نیست همان لاله‌عذارست ببینید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زبن برگ ‌گلی چند که آیینهٔ رنگند</p></div>
<div class="m2"><p>آن دست‌ که بیرون نگارست ببینید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آفاق به عرض اثر خویش اسیرست</p></div>
<div class="m2"><p>صیّاد همین گرد شکارست ببینید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر صفحهٔ آتش‌زدهٔ عمر منازبد</p></div>
<div class="m2"><p>فرصت چقدر سبحه‌ شمارست ببینید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این دشت که جولانگه صد رنگ تمناست</p></div>
<div class="m2"><p>ای آبله‌پایان همه خارست ببینید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خونگرمی عشق آینه‌پرداز ‌بهارست</p></div>
<div class="m2"><p>کو غنچه چه‌گل‌بوس و کنارست ببینید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک سجده نپیمود طلب بی‌عرق شرم</p></div>
<div class="m2"><p>پیشانی ما آبله‌دارست ببینید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن رنگ کز اندیشه برون است خیالش</p></div>
<div class="m2"><p>دیگر نتوان دید بهارست ببینید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عمری‌ست تماشاکدهٔ شوخی نازیم</p></div>
<div class="m2"><p>آیینهٔ ما با که دچارست ببینید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل ز نفس آینه‌ام یأس خروش است</p></div>
<div class="m2"><p>کای دیده‌وران این چه غبارست ببینید</p></div></div>