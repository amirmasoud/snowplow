---
title: >-
    غزل شمارهٔ ۱۸۶۶
---
# غزل شمارهٔ ۱۸۶۶

<div class="b" id="bn1"><div class="m1"><p>نشسته‌ای ز دل تنگ بر در تصدیع</p></div>
<div class="m2"><p>دمی‌که واشود این قفل عالمیست وسیع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خویش گر نرسی آنقدر غرابت نیست</p></div>
<div class="m2"><p>که سرکشیده‌ای از کارگاه صنع بدیع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طلب ز هرچه تسلی شود غنیمت‌گیر</p></div>
<div class="m2"><p>به جوع می‌مکد انگشت خوبش طفل رضیع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قیامت است طمع ز امتلا نمی‌میرد</p></div>
<div class="m2"><p>که تا به حلق رسیده است می‌خورد تشنیع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه غفلت است که چون شمع گل به سر باشی</p></div>
<div class="m2"><p>به زیر تیغ نشستن ندارد این تقطیع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به‌گرد قاصد همت رسیدن آسان نیست</p></div>
<div class="m2"><p>ز مقصد آنطرفش برده گامهای وسیع</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدون خاک حضور یقین نشد روشن</p></div>
<div class="m2"><p>چراغ نقش قدم داشت این بساط رفیع</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بقا فنا به‌ کنار و فنا بقا به بغل</p></div>
<div class="m2"><p>همین ربیع و خریفست هم خریف وربیع</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز شرم چشم‌ گشودن به ‌بارگاه حضور</p></div>
<div class="m2"><p>عرق تو آینه پرداز تا بریم شفیع</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پس از تأمل بسیار شد عیان بیدل</p></div>
<div class="m2"><p>که علت است تفاوتگر مطاع و مطیع</p></div></div>