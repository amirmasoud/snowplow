---
title: >-
    غزل شمارهٔ ۸۰۱
---
# غزل شمارهٔ ۸۰۱

<div class="b" id="bn1"><div class="m1"><p>برگ طربم عشرت بی‌برگ و نوایی‌ست</p></div>
<div class="m2"><p>چون آبله بالیدنم از تنگ‌قبایی‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در قافلهٔ بی جرس مقصد تسلیم</p></div>
<div class="m2"><p>بی‌طاقتی نبض طلب هرزه‌درایی‌ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کو شور جنونی‌که اسیران ادب را</p></div>
<div class="m2"><p>در دام و قفس حسرت یک ناله رهایی‌ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرش در دل باش‌کزین‌گوشهٔ الفت</p></div>
<div class="m2"><p>هرجا روی از آبلهٔ پاکف پایی‌ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آرایش‌گل منت مشاطه ندارد</p></div>
<div class="m2"><p>بی‌ساختگی‌های چمن حسن خدایی‌ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خلوتگه وصل انجمن‌آرای‌ دویی نیست</p></div>
<div class="m2"><p>هشدارکه اندیشهٔ آغوش جدایی‌ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا رنگ قبولی به دل از نقش تمناست</p></div>
<div class="m2"><p>گر خود همه آیینه شوی‌کارگدایی‌ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای خاک‌نشین‌کسب ادب مفت سفالت</p></div>
<div class="m2"><p>اندیشهٔ چینی مکن این جنس خطایی‌ست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنجاکه‌گل حسن حیاپرور نازست</p></div>
<div class="m2"><p>سیر چمن آینه هم دیده‌درایی‌ست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فریادکه یک عمر غبارنفس ما</p></div>
<div class="m2"><p>زد بال و ندانست که پروازکجایی‌ست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کو صبروچه طاقت‌که به صحرای محبت</p></div>
<div class="m2"><p>در آبله پاداری و در ناله رسایی‌ست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اندیشه چمن طرح‌کن سجدهٔ شوقی‌ست</p></div>
<div class="m2"><p>امروز ندانم کف پای که حنایی‌ست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون اشک من و دوش چکیدن چه توان‌کرد</p></div>
<div class="m2"><p>سرمایهٔ اول قدمم آبله‌پایی‌ست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مجموعهٔ امکان سخنی بیش ندارد</p></div>
<div class="m2"><p>بیدل مرو از راه‌که این ساز نوایی‌ست</p></div></div>