---
title: >-
    غزل شمارهٔ ۱۱۲۷
---
# غزل شمارهٔ ۱۱۲۷

<div class="b" id="bn1"><div class="m1"><p>چو دندان ریخت نعمت حرص را مأیوس می‌سازد</p></div>
<div class="m2"><p>صدف را بی‌گهرگشتن‌کف افسوس می‌سازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تعلقهای هستی با دلت چندان نمی‌پاید</p></div>
<div class="m2"><p>نفس را یک دو دم این آینه محبوس می‌سازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه سازد خلق عاجز تا نسازد با گرفتاری</p></div>
<div class="m2"><p>قفس را بی‌پریها عالم مانوس می‌سازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فلک بر شش جهت واکرده است آغوش رسوایی</p></div>
<div class="m2"><p>خیال بی‌خبر با پرده ناموس می‌سازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به ‌گمنامی قناعت ‌کن ‌که جاف بی‌حیا طینت</p></div>
<div class="m2"><p>به سرها چرم گاوی می‌کشد تا کوس می‌سازد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو خواهی شور عالم گیر و خواهی غلغل محشر</p></div>
<div class="m2"><p>فلک زین‌ رنگ چندین نغمه‌ها محسو‌س می سازد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نفس زیر عرق می‌پرورد شرم حباب اینجا</p></div>
<div class="m2"><p>به پاس آبرو هر شمع با فانوس می‌سازد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خموشی ختم‌گفت‌وگوست لب بربند و فارغ شو</p></div>
<div class="m2"><p>همین یک نقطه کار درس صد قاموس می‌سازد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه‌سحر است این‌که افسونکاری‌مشاطهٔ حیرت</p></div>
<div class="m2"><p>به دستت می‌دهد آیینه و طاووس می‌سازد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به یاد آستانت‌ گر همه چین بر جبین بندم</p></div>
<div class="m2"><p>ادب لب می‌کند ایجاد و وقف بوس می سازد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فغان بی‌وجد نازی نیست کز دل برکشد بیدل</p></div>
<div class="m2"><p>برهمن‌زاده‌ای در ‌دیر ما ناقوس می‌سازد</p></div></div>