---
title: >-
    غزل شمارهٔ ۱۴۵۵
---
# غزل شمارهٔ ۱۴۵۵

<div class="b" id="bn1"><div class="m1"><p>بلا‌کشان محبت‌ گل چه نیرنگند</p></div>
<div class="m2"><p>شکسته‌اند به رنگی‌ که عالم رنگند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه شیشه و چه پری خانه‌زاد حیرت ماست</p></div>
<div class="m2"><p>به آرمیدگی دل‌که بیخودان سنگند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز عیب‌پوی ابنای روزگار مپرس</p></div>
<div class="m2"><p>یکی‌گر آینه پرداخت دیگران زنگند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فریب صلح مخور ازگشاده‌رویی خلق</p></div>
<div class="m2"><p>که تنگ حوصلیگیهای عرصهٔ جنگند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به وادیی ‌که طلب نارسای مفصد اوست</p></div>
<div class="m2"><p>بهوش باش ‌که منزل‌ رسیدن لنگند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نوای پرده ی بیتابی نفس این است</p></div>
<div class="m2"><p>که عافیت‌طلبان سخت غفلت آهنگند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو هر شکست‌ که خواهی به دوش ما بربند</p></div>
<div class="m2"><p>وفا سرشته حریفان طبیعت رنگند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز وهم بر سر مینای خود چه می‌لرزی</p></div>
<div class="m2"><p>شنو ز شیشه‌گران در شکستن سنگند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به بستن مژه انجام‌کار شد معلوم</p></div>
<div class="m2"><p>که آب آینه‌ها جمله طعمهٔ زنگند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حباب نیم‌نفس با نفس نمی‌سازد</p></div>
<div class="m2"><p>ز خود تهی‌شدگان بر خود اینقدر تنگند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز خلق آنهمه بیگانه نیستی بیدل</p></div>
<div class="m2"><p>تو هرزه‌فکری و این قوم عالم بنگند</p></div></div>