---
title: >-
    غزل شمارهٔ ۲۱۰۱
---
# غزل شمارهٔ ۲۱۰۱

<div class="b" id="bn1"><div class="m1"><p>دور از آن در چند در هر دشت و در گرداندم</p></div>
<div class="m2"><p>بخت برگردیده برگردد که برگرداندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طالعی دارم ‌که چرخ بی‌مروت همچو شمع</p></div>
<div class="m2"><p>شام پیش از دیگر آگه از سحر گرداندم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آگهی در کارگاه مخملم خون می‌خورد</p></div>
<div class="m2"><p>خواب پا برجاست صد پهلو اگر گرداندم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهره‌ام از نام عشق آبست لیک اقبال شوق</p></div>
<div class="m2"><p>می‌تواند کوه یاقوت جگر گرداندم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاک هم‌ گاهی به رنگ صبح‌ گردی می‌کند</p></div>
<div class="m2"><p>فقر می‌ترسم به استغنا سپر گرداندم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای قناعت پا به دامن‌ کش‌ که چشم حرص دون</p></div>
<div class="m2"><p>کاسه‌ای دارد مبادا دربه‌در گرداندم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم به زیر پایم آب و دانه خرمن می‌کند</p></div>
<div class="m2"><p>آنکه بیرون قفس بی‌بال و پر گرداندم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شیشه‌ها کردم تهی اما تنک ظرفی بجاست</p></div>
<div class="m2"><p>بشکند دل تا خراباتی دگر گرداندم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از ضعیفی سوده می‌گردد چو شمع انگشت من</p></div>
<div class="m2"><p>گر ورقهای شکست رنگ تر گرداندم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چیزی از ایثار می‌خواهم نیاز دوستان</p></div>
<div class="m2"><p>تا مبادا این سلام خشک تر گرداندم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون حنا بیدل ز گلزار عدم آورده‌ام</p></div>
<div class="m2"><p>رنگ امیدی که پایش گرد سر گرداندم</p></div></div>