---
title: >-
    غزل شمارهٔ ۱۲۶۳
---
# غزل شمارهٔ ۱۲۶۳

<div class="b" id="bn1"><div class="m1"><p>آهی به هوا چتر زد و چرخ برین‌ شد</p></div>
<div class="m2"><p>داغی به غبار الم آسود و زمین شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بشکست طلسم دل و زد کوس محبت</p></div>
<div class="m2"><p>پاشید غبار نفس و آه حزین شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نظاره به صورت زد و نیرنگ‌ کمان ریخت</p></div>
<div class="m2"><p>اندیشه به معنی نظری‌ کرد و یقین شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن آینه ‌کز عرض صفا نیز حیا داشت</p></div>
<div class="m2"><p>تا چشم‌گشودیم پریخانهٔ چین شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غفلت چه فسون خواند که در خلوت تحقیق</p></div>
<div class="m2"><p>برگشت نگاهم ز خود و آینه‌بین شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل‌کرد ز مسجودی من سجده‌ فروشی</p></div>
<div class="m2"><p>یعنی چو هلالم خم محراب جبین شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عنقایی‌ام از شهرت خودگشت فزون تر</p></div>
<div class="m2"><p>آخر پی‌گمنامی من نقش‌ نگین شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل خواست به ‌گردون نگرد زیر قدم دید</p></div>
<div class="m2"><p>آن بود که در یک نظر انداختن این شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر لحظه هوایی‌ست عنان‌تاب دماغم</p></div>
<div class="m2"><p>رخشی که ندارم به خیال اینهمه زین شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از عالم حیرانی من هیچ مپرسید</p></div>
<div class="m2"><p>آیینه ‌کمند نگهی بود که چین شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وقت است‌که بر بی‌کسی عشق بگرییم</p></div>
<div class="m2"><p>کاین شعله ز خار و خس ما خاک‌نشین شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در غیب و شهادت من و معشوق همانیم</p></div>
<div class="m2"><p>بیدل تو بر آنی ‌که چنان بود و چنین شد</p></div></div>