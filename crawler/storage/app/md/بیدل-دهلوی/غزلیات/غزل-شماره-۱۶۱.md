---
title: >-
    غزل شمارهٔ ۱۶۱
---
# غزل شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>ساختم قانع دل از عافیت بیگانه را</p></div>
<div class="m2"><p>برگ بیدی فرش‌کردم خانهٔ دیوانه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مطلبم از می‌پرستی تر دماغیها نبود</p></div>
<div class="m2"><p>یک دو ساغر آب دادم‌گریهٔ مستانه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل سپندگردش چشمی‌که یاد مستی‌ش</p></div>
<div class="m2"><p>شعلهٔ جواله می‌سازد خط پیمانه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>التفات عشق آتش ریخت در بنیاد دل</p></div>
<div class="m2"><p>سیل شد تردستی معمار این ویرانه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تاکنم تمهید آغوشی دل از جا رفته است</p></div>
<div class="m2"><p>درگشودن شهپر پرواز بود این خانه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عالمی را انفعال وضع بیکاری گداخت</p></div>
<div class="m2"><p>ناخن سرخاری دلها مگردان شانه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر سیندی‌گوش چندین بزم می‌مالد به‌هم</p></div>
<div class="m2"><p>خوابناکان کاش از ما بشنوند افسانه را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حایل آن شمع یکتایی فضولیهای تست</p></div>
<div class="m2"><p>از نظر بردار چون مژگان پر پروانه را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آگهی گر ریشه‌پرداز جهانی می‌شود</p></div>
<div class="m2"><p>سیر این مزرع یکی صد می‌نماید دانه را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حق زنار وفا بیدل نمی‌گردد ادا</p></div>
<div class="m2"><p>تا سلیمانی نسازی سنگ این بتخانه را</p></div></div>