---
title: >-
    غزل شمارهٔ ۸۶۶
---
# غزل شمارهٔ ۸۶۶

<div class="b" id="bn1"><div class="m1"><p>به حیرتم چه فسون داشت بزم نیرنگت</p></div>
<div class="m2"><p>زدم به دامن خود دست و یافتم چنگت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دماغ زمزمهٔ بی‌نیازی‌ات نازم</p></div>
<div class="m2"><p>که تا دمید برآهنگ ما زد آهنگت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقاب بر نزدن هم قیامت‌آرایی‌ست</p></div>
<div class="m2"><p>فتاده در همه آفاق آتش سنگت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به غیر چاک‌گریبان‌گلی نرست اینجا</p></div>
<div class="m2"><p>درین چمن چه جنون‌کرد شوخی رنگت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه ممکن است جهان را ز فتنه آسودن</p></div>
<div class="m2"><p>فتاده بر صف برگشتهٔ مژه جنگت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حیا نبود کفیل برون خرامی ناز</p></div>
<div class="m2"><p>دل‌گرفتهٔ ما کرد اینقدر ننگت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>براین ترانه‌که ما رنگ نوبهار توایم</p></div>
<div class="m2"><p>رسیده‌ایم به گلهای تهمت ننگت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهان وهم چه مقدار منفعل تک وپوست</p></div>
<div class="m2"><p>که جستجوکند آنگه به عالم بنگت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>علاج دوری‌غفلت به جهد ناید راست</p></div>
<div class="m2"><p>نشسته‌ایم به منزل هزار فرسنگت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه دیده قابل دیدن نه لب حریف بیان</p></div>
<div class="m2"><p>نگ‌ه ما متحیر زبان ما دنگت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کراست زهرهٔ جهدی‌که دامنت گیرد</p></div>
<div class="m2"><p>چودست ما همه شلت چوپای ما لنگت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زبان آینه‌، پرداز می‌دهم بیدل</p></div>
<div class="m2"><p>بهارکرد مرا پرفشانی رنگت</p></div></div>