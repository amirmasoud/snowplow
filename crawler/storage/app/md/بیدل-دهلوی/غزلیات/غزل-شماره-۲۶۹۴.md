---
title: >-
    غزل شمارهٔ ۲۶۹۴
---
# غزل شمارهٔ ۲۶۹۴

<div class="b" id="bn1"><div class="m1"><p>خطاپرست مباش‌ ای ز راستی عاری</p></div>
<div class="m2"><p>که ‌گر سپهر شوی می‌کشی نگو نساری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان ز شوخی نظّارهٔ تو کهسارست</p></div>
<div class="m2"><p>به چشم بسته نظر کن بهار همواری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قبول آفت هرکس بقدر حوصله است</p></div>
<div class="m2"><p>به تیغ می‌کند اینجا طرف جگر داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو گل درین چمن از بحر عبرتت ‌کافیست</p></div>
<div class="m2"><p>تبسمی‌ که همان چین دامن انگاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به رنگ و بو دل خود بسته‌ای و زین غافل</p></div>
<div class="m2"><p>که غنچه سان ‌گل پرواز در بغل داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گره ز کار فروبستهٔ تو بگشاید</p></div>
<div class="m2"><p>اگر چو غنچه دل شبنمی به دست آری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غبار دامن این دشت ناله اندود است</p></div>
<div class="m2"><p>قدم دلیر منه تا دلی نیفشاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به غیر طبع تو کز سجده‌است معراجش</p></div>
<div class="m2"><p>کدام شعله که خاکش بکرد همواری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنان ز دهر سبکبار بایدت رفتن</p></div>
<div class="m2"><p>که بار نقش قدم هم به خاک نگذاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گواه عاقبت‌ کار ظلم پیشه بس است</p></div>
<div class="m2"><p>به خون نشستن نشتر ز مردم آزاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز خواب صبح سر غنچه می‌رود بر باد</p></div>
<div class="m2"><p>مده ز دست چو شبنم عنان بیداری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به مزرعی‌ که دلش برگ خرمن آرایی‌ست</p></div>
<div class="m2"><p>شکست می‌دروی آبگینه می‌کاری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به دوش عمر کشی بار این و آن تا چند</p></div>
<div class="m2"><p>خوش آن ‌زمان که‌ ز اسباب ‌دست برداری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر ز جادهٔ تسلیم نگذری بیدل</p></div>
<div class="m2"><p>کند به‌ کسوت موجت شکست معماری</p></div></div>