---
title: >-
    غزل شمارهٔ ۱۴۲۳
---
# غزل شمارهٔ ۱۴۲۳

<div class="b" id="bn1"><div class="m1"><p>طبع دانا الم دهر مکدر نکند</p></div>
<div class="m2"><p>گرد بر روی ‌گهر آن همه لنگر نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خیالی نتوان غرهٔ تحقیق شدن</p></div>
<div class="m2"><p>گر همه حسن دمد آینه باور نکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌دهد عاقبت‌ کار حسد سینه به زخم</p></div>
<div class="m2"><p>بدرگی تا به‌کجا تکیه به نشتر نکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خرابات‌، شیاطین نسبان بسیارند</p></div>
<div class="m2"><p>دختر رز جلبی نیست ‌که شوهر نکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی‌زری ممتحن جوهر انسانی نیست</p></div>
<div class="m2"><p>آدم آنست‌ که مال و حشمش خر نکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شیشهٔ حرص به صهبای قناعت پرکن</p></div>
<div class="m2"><p>کز تنگ‌حوصلگی ناله به ساغر نکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مجلس‌آرای هوس با تو حسابی دارد</p></div>
<div class="m2"><p>تا نسوزد دلت آرایش مجمر نکند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به نگاهی چو شرر قانع پیدایی باش</p></div>
<div class="m2"><p>تا ترا در نظر خلق مکرر نکند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شبنم‌ گلشن ایجاد خجالت دارد</p></div>
<div class="m2"><p>صبح تصویر بر آ تا نفست تر نکند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شوق دل حسرت‌ گلزار حضوری دارد</p></div>
<div class="m2"><p>همچو طاووس چرا آینه دفتر نکند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خاک درگاه مذلت ز چه اکسیرکم است</p></div>
<div class="m2"><p>کیمیا گو مس بیقدر مرا زر نکند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عشوهٔ الفت دنیا نخرد بیدل ما</p></div>
<div class="m2"><p>نقد دل باخته سودای محقر نکند</p></div></div>