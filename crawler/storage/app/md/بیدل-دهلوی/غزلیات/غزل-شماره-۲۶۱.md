---
title: >-
    غزل شمارهٔ ۲۶۱
---
# غزل شمارهٔ ۲۶۱

<div class="b" id="bn1"><div class="m1"><p>بر سنگ زد زمانه ز بس ساز آشنا </p></div>
<div class="m2"><p> آه از فسون غول به آواز آشنا </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز نیست قابل تفریق و امتیاز </p></div>
<div class="m2"><p>در سرمه‌گرد می‌کند آواز آشنا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر صیقلی به کار برد سعی اتفاق </p></div>
<div class="m2"><p> انجام‌کار دشمن و آغاز آشنا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا کی درین بساط ز افسون التفات </p></div>
<div class="m2"><p> دل می‌خراشد آینه‌پرداز آشنا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داد گشاد کار تظلم کجا برد </p></div>
<div class="m2"><p> برروی شمع خنده زندگاز آشنا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر مدعای مرغ نفس آرمیدن است </p></div>
<div class="m2"><p> زد حلقه بستگی به در باز آشنا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بشنو نوای نیک و بد از دور و دم مزن </p></div>
<div class="m2"><p> دام و قفس خوش است ز پرواز آشنا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنگ قضاست دهر، امان‌گاه خلق نیست </p></div>
<div class="m2"><p> نی ناله داشته‌ست ز دمساز آشنا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منت‌کش تکلف اخلاق‌کس مباد </p></div>
<div class="m2"><p> گنجشک را چه سود زشهبازآشنا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از هرچه دم زنی به خموشی حواله‌کن</p></div>
<div class="m2"><p> بیگانه‌ام ز خویش هم از ناز آشنا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p> عشق قابل انشاکسی نیافت </p></div>
<div class="m2"><p> این انجمن پر است ز غماز آشنا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل به‌حرف وصوت هم‌آواره‌گشت‌خلق</p></div>
<div class="m2"><p> بردیم سر به مهر عدم راز آشنا </p></div></div>