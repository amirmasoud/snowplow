---
title: >-
    غزل شمارهٔ ۴۵۲
---
# غزل شمارهٔ ۴۵۲

<div class="b" id="bn1"><div class="m1"><p>گل‌کردن هوس ز دل صاف تهمت است</p></div>
<div class="m2"><p>موج و حباب چشمهٔ آیینه حیرت است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را که بستن مژه باشد دلیل هوش</p></div>
<div class="m2"><p>چشم‌گشاده آینهٔ خواب غفلت است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این است اگر حقیقت اسباب اعتبار</p></div>
<div class="m2"><p>نگذشتنت ز هستی موهوم همت است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زبن عبرتی ‌که زندگیش نام‌ کرده‌اند</p></div>
<div class="m2"><p>تا سر به زیر خاک ندزدی خجالت است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر دوش عمر چندکشی محمل امل</p></div>
<div class="m2"><p>ای بیخبر شرر چقدر رام فرصت است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عام است بسکه نسبت بی‌ربطی جهان</p></div>
<div class="m2"><p>مژگان به خواب اگر به هم آری غنیمت است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زنهار از التفات عزیزان حذر کنید</p></div>
<div class="m2"><p>بیمار ظلم کشتهٔ اهل عیادت است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشکن به شوخی نفس‌، آیینهٔ نمود</p></div>
<div class="m2"><p>خاموشی حباب طلسم سلامت است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فرش است فیض هر دو جهان در صفای دل</p></div>
<div class="m2"><p>آیینه از قلمرو صبح سعادت است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرد بلند و پست نفس‌ گر رود به باد</p></div>
<div class="m2"><p>بام و در بنای هوس جمله رفعت است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عمری‌ست دل به غفلت خودگریه می‌کند</p></div>
<div class="m2"><p>این نامهٔ سیه چقدر ابر رحمت است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل به یاد محشراگرخون شوم بجاست</p></div>
<div class="m2"><p>بازم دل شکسته دمیدن قیامت است</p></div></div>