---
title: >-
    غزل شمارهٔ ۳۵۰
---
# غزل شمارهٔ ۳۵۰

<div class="b" id="bn1"><div class="m1"><p>پیام داشت به عنقا خط جبین حباب</p></div>
<div class="m2"><p>که‌گرد نام نشسته است بر نگین حباب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نفس‌شمار زمانیم تا نفس نزدن</p></div>
<div class="m2"><p>همین شهور حباب و همین سنین حباب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز ششجهت مژه بندید و سیرخویش‌کنید</p></div>
<div class="m2"><p>نگه‌کجاست به چشم خیال بین حباب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز عمر هرچه رود، آمدن نمی‌داند</p></div>
<div class="m2"><p>مخور فریب نفسهای واپسین حباب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به فرصتی‌که نداری‌کدام عشوه چه ناز</p></div>
<div class="m2"><p>ز فربهی نکنی تکیه برسرین حباب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مقیم پردهٔ ناموس فقر باید بود</p></div>
<div class="m2"><p>کجاست دست‌که برداری آستین حباب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه نشئه داشت می ساغر سبکروحی</p></div>
<div class="m2"><p>که‌گشت موج‌گهر درد ته‌نشین حباب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سحاب مزرعهٔ اعتبار منفعلی‌ست</p></div>
<div class="m2"><p>تو هم نمی زعرق ریزبرزمین حباب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دماغ‌کسب وقارم نشدکفیل وفا</p></div>
<div class="m2"><p>جهان به‌کیش‌گهر ساخت من به دین حباب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کراست ضبط عنان‌، عرصهٔ گروتازی‌ست</p></div>
<div class="m2"><p>برآمده‌ست سوار نفس به زین حباب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زمان پر زدن زندگی معین نیست</p></div>
<div class="m2"><p>تو محو باش ته دامن است جین حباب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شکست دل به چه تدبیرکم شود بیدل</p></div>
<div class="m2"><p>هزار موج‌کمر بسته درکمین حباب</p></div></div>