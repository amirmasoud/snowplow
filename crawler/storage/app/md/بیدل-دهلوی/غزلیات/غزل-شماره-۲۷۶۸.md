---
title: >-
    غزل شمارهٔ ۲۷۶۸
---
# غزل شمارهٔ ۲۷۶۸

<div class="b" id="bn1"><div class="m1"><p>ز بسکه‌کرد قصور نگاه مژگانی</p></div>
<div class="m2"><p>به خود شناسی ما ختم شد خدا دانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرر گل است خزان و بهار امکانی</p></div>
<div class="m2"><p>ندارد آنهمه فرصت که رنگ گردانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز خود بر آمدگان شوکتی دگر دارند</p></div>
<div class="m2"><p>غبار هم به هوا نیست بی‌سلیمانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به عجز کوش‌ گر از شرم جوهری داری</p></div>
<div class="m2"><p>مباد دعوی کاری کنی که نتوانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لباس بر تن آزادگان نمی‌زیبد</p></div>
<div class="m2"><p>بس است جوهر شمشیر موج، عریانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گشاده رویی ارباب دستگاه مخواه</p></div>
<div class="m2"><p>فلک به چین مه نو نهفته پیشانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فراغ دارد از اسلام و کفر غرهٔ جاه</p></div>
<div class="m2"><p>یکی‌ست سبحه و زنار در سلیمانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سواد مطلع ما نیست آنقدر روشن</p></div>
<div class="m2"><p>که انتظار نویسی به چشم قربانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کجاست گرد امیدی که دامنم گیرد</p></div>
<div class="m2"><p>چو صبح می‌دمد از پیکرم خود افشانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز ابر گریهٔ دیده گر ایمنی می‌داشت</p></div>
<div class="m2"><p>نمی‌کشید ز مژگان کلاه بارانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو خون بسملم ‌از دستگاه شوق مپرس</p></div>
<div class="m2"><p>بهار کرد طواف من از پریشانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>درین هوسکده تا ممکنست بیدل باش</p></div>
<div class="m2"><p>مکار آینه تا حیرتی نرویانی</p></div></div>