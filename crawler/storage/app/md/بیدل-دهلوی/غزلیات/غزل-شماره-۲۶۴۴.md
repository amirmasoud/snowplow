---
title: >-
    غزل شمارهٔ ۲۶۴۴
---
# غزل شمارهٔ ۲۶۴۴

<div class="b" id="bn1"><div class="m1"><p>خشم را آیینه پرداز ترحم کرده‌ای</p></div>
<div class="m2"><p>در نقاب چین پیشانی تبسم کرده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر سر مویت زبان التفاتی دیگر است</p></div>
<div class="m2"><p>بسکه شوخی در خموشی هم تکلم‌ کرده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا عرق از چهره‌ات خورشید ریز عبرت‌ست</p></div>
<div class="m2"><p>چرخ را یک دشت نقش پای انجم کرده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عقده‌های غنچهٔ دل بی‌گلاب اشک نیست</p></div>
<div class="m2"><p>می به ساغر کن کزین انگور در خم کرده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوهر از تسلیم شد ایمن ز موج انقلاب</p></div>
<div class="m2"><p>ساحل جمعیتی گر دست و پا گم کرده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر حدیث مدعی کافسانهٔ دردسر است</p></div>
<div class="m2"><p>گر تغافل کرده‌ای بر خود ترحم کرده‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای خیالت غرق سودای جهان مختصر</p></div>
<div class="m2"><p>قطره‌ای را برده‌ای جایی‌که قلزم کرده‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>موج اقبال تو در گرد عدم پر می‌زند</p></div>
<div class="m2"><p>قلزمی اما برون از خود تلاطم کرده‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی‌تکلف گر همین‌ست اعتبارات جهان</p></div>
<div class="m2"><p>کم ز حیوانی اگر تقلید مردم ‌کرده‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>معرفت کز اصطلاح ما و من جوشیده است</p></div>
<div class="m2"><p>غفلت‌ست اما تو آگاهی توهّم کرده‌ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این زمان عرض کمالت فکر آب و نان بس است</p></div>
<div class="m2"><p>آدمیت داشتی در کار گندم کرده‌ای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بحر امکان شوخی موج سرابی بیش نیست</p></div>
<div class="m2"><p>دست از آبش تا نمی‌شویی تیمم ‌کرده‌ای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بسته‌ای بیدل اگر بر خود زبان مدعی</p></div>
<div class="m2"><p>عقربی را می‌توانم‌ گفت بی دم کرده‌ای</p></div></div>