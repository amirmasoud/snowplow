---
title: >-
    غزل شمارهٔ ۲۴۹۵
---
# غزل شمارهٔ ۲۴۹۵

<div class="b" id="bn1"><div class="m1"><p>به تماشای این چمن در مژگان فراز کن</p></div>
<div class="m2"><p>ز خمستان عافیت قدحی‌ گیر و ناز کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشکن جام آبرو به تپشهای آرزو</p></div>
<div class="m2"><p>عرق احتیاج را می مینای راز کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مپسند آنقدر ستم ‌که به خست شوی علم</p></div>
<div class="m2"><p>گره دست و دل ز هم مژه بگشا و باز کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به چه افسانه مایلی‌که ز تحقیق غافلی</p></div>
<div class="m2"><p>تو تماشا مقابلی ز خیال احترازکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه ظهوری‌ست نی خفا نه بقایی‌ست نی فنا</p></div>
<div class="m2"><p>به تخیل حقیقتی‌ که نداری مجاز کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو غبار شکسته در سر راهت نشسته‌ام</p></div>
<div class="m2"><p>قدمی برزمین‌گذار و مرا سرفرازکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به ادای تکلمی‌، به فسون تبسمی</p></div>
<div class="m2"><p>شکری را قوام ده‌، نمکی راگدازکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عطش حرص‌ یکقلم زجهان برده رنگ نم</p></div>
<div class="m2"><p>همه خاکست آب هم به تیمم نمازکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نکند رشته کوتهی‌، اگر از عقده وارهی</p></div>
<div class="m2"><p>سرت از آرزو تهی‌، چه شود پا درازکن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز فسردن چو بگذری سوی آیینهٔ پری</p></div>
<div class="m2"><p>دل سنگین گداز و کارگه شیشه ساز کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بنشین بیدل از حیا پس زانوی خامشی</p></div>
<div class="m2"><p>نفسی چند حرص را ز طلب بی‌نیاز کن</p></div></div>