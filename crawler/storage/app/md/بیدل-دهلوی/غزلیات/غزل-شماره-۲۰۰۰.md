---
title: >-
    غزل شمارهٔ ۲۰۰۰
---
# غزل شمارهٔ ۲۰۰۰

<div class="b" id="bn1"><div class="m1"><p>رفتم ز خویش و یاد نگاهیست حالی‌ام</p></div>
<div class="m2"><p>مستی نماست آینهٔ جام خالی‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک روی و یک دلم به بد و نیک روزگار</p></div>
<div class="m2"><p>آیینه کرد جوهر بی انفعالی‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر برگ ‌گل به عرض من آیینه است و من</p></div>
<div class="m2"><p>چون بو هنوز در چمن بی مثالی‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عمریست در ادبکدهٔ بوریای فقر</p></div>
<div class="m2"><p>آسوده‌تر ز نکهت گلهای قالی‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در پرده کوس سلطنت فقر می‌زند</p></div>
<div class="m2"><p>حیرت صداست چینی ناز سفالی‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخت سیاه ‌کو که ز ضعفم نشان دهد</p></div>
<div class="m2"><p>بر شب نوشته‌اند برات هلالی‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد خاک از انتظار تو چشم تر و هنوز</p></div>
<div class="m2"><p>قد می‌کشد غبار نگه از حوالی‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر جزوم از شکسته دلی موج می‌زند</p></div>
<div class="m2"><p>من شیشه ریزه‌ام حذر از پای‌مالی‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در هر سری به نشئهٔ دیگر دویده است</p></div>
<div class="m2"><p>چون موج باده ریشهٔ بی‌اعتدالی‌ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>موج از گهر ندامت دوری نمی‌کند</p></div>
<div class="m2"><p>اندیشهٔ فراق ندارم وصالی‌ام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل به ناتوانی خود ناز می‌کنم</p></div>
<div class="m2"><p>پرواز آشیانی افسرده بالی‌ام</p></div></div>