---
title: >-
    غزل شمارهٔ ۱۵۰۵
---
# غزل شمارهٔ ۱۵۰۵

<div class="b" id="bn1"><div class="m1"><p>یکدو دم هنگامهٔ تشویش مهر و کینه بود</p></div>
<div class="m2"><p>هرچه دیدم میهمان خانهٔ آیینه بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ابتذال باغ امکان رنگ گردیدن نداشت</p></div>
<div class="m2"><p>هرگلی ‌کامسالم آمد در نظر پارینه بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منفعل می‌شد ز دنیا هوش اگر می‌داشت خلق</p></div>
<div class="m2"><p>صبر و حنظل در مذاق‌گاو و خر لوزینه بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچ شکلی بی‌هیولا قابل صورت نشد</p></div>
<div class="m2"><p>آدمی هم پیش از آن‌ کادم شود بوزینه بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امتحان اجناس بازار ریا می‌داد عرض</p></div>
<div class="m2"><p>ریشها دیدیم با قیمت‌تر از پشمینه بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکجا دیدیم صحبتهای گرم زاهدان</p></div>
<div class="m2"><p>چون نکاح دختر رز در شب آدینه بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاک‌شد فطرت‌ز پستی لیک‌مژگان برنداشت</p></div>
<div class="m2"><p>ورنه از ما تا به بام آسمان یک زینه بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تختهٔ مشق حوادث‌کرد ما را عاجزی</p></div>
<div class="m2"><p>زخم دندان بیشتر وقف لب زیرینه بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در جهان بی‌تمیزی چاره از تشویش نیست</p></div>
<div class="m2"><p>ما به صد جا منقسم‌کردیم و دل در سینه بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آرزوها ماند محو ناز در بزم وصال</p></div>
<div class="m2"><p>پاس ناموس تحیّر مهر این ‌گنجینه بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرکجا رفتیم بیدل درد ما پنهان نماند</p></div>
<div class="m2"><p>خرقهٔ دروبشی ما لختی از دل پنبه بود</p></div></div>