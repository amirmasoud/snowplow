---
title: >-
    غزل شمارهٔ ۴۷۷
---
# غزل شمارهٔ ۴۷۷

<div class="b" id="bn1"><div class="m1"><p>خواب در چشم و نفس بر دل محزون بار است</p></div>
<div class="m2"><p>ازکه دورم‌که به خود ساختنم دشوار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عرق شرم تو، ازچشم جهان‌، شست نگاه</p></div>
<div class="m2"><p>گرتو خجلت نکشی‌، آینه‌ها بسیار است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوشهٔ چشم تو محرومی‌کس نپسندد</p></div>
<div class="m2"><p>گر تغافل مژه خواباند نگه بیدار است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نرود حق وفای ادب ازگردن ما</p></div>
<div class="m2"><p>موج را بستن‌گوهرگره زنار است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در مقامی‌که جنون نشئهٔ عزت دارد</p></div>
<div class="m2"><p>پای بی‌آبله یکسر، سر بی‌دستار است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آبرو تا به‌کجا، خاک مذلت نشود</p></div>
<div class="m2"><p>حرص در سعی طلب‌، آنچه ندارد، عار است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زر و سیمی‌که‌کنی جمع وبه درویش دهی</p></div>
<div class="m2"><p>طبع‌گر ننگ فضولی نکشد ایثار است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواجه تا چند نبندد به تغافل درگوش</p></div>
<div class="m2"><p>شور هنگامهٔ محتاج دماغ افشار است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تاکی‌اندوه‌کج و راست ز دنیا بردن</p></div>
<div class="m2"><p>مهرهٔ عرصهٔ شطرنج به صد رفتار است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غافلان‌، چند هوا تاز جنون باید بود</p></div>
<div class="m2"><p>کسوت سرکشی شمع‌گریبان‌وار است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل آخر به سر خویش قدم باید زد</p></div>
<div class="m2"><p>جادهٔ منزل تحقیق خط پرگار است</p></div></div>