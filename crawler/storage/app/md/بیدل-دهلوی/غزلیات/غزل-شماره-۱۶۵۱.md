---
title: >-
    غزل شمارهٔ ۱۶۵۱
---
# غزل شمارهٔ ۱۶۵۱

<div class="b" id="bn1"><div class="m1"><p>از غبار جلوه غیر تو تا بستم نظر</p></div>
<div class="m2"><p>چون صف مژگان دو عالم محو شد در یکدگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسته‌ام محمل به دوش یأس و از خود می‌روم</p></div>
<div class="m2"><p>بال پروازی ندارد صبح جز چاک جگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خدمت موی میانت تاکه را باشد نصیب</p></div>
<div class="m2"><p>گلرخان را زین هوس زنار می‌بندد کمر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون‌گهر زین پیش سامان سرشکی داشتم</p></div>
<div class="m2"><p>این زمانم نیست جزحیرت سراغ چشم تر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وحشت حسرت به این کمفرصتی مخمورکیست</p></div>
<div class="m2"><p>صورت خمیازه دارد چین دامان سحر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عالمی را از تغافل ربط الفت داده‌ایم</p></div>
<div class="m2"><p>نیست مژگان قابل شیرازه بی‌ضبط نظر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این تن‌آسانی دلیل وحشت سرشار نیست</p></div>
<div class="m2"><p>هرقدر افسرده گردد سنگ می‌بندد کمر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر فلک بی‌اعتبارت‌ کرد جای شکوه نیست</p></div>
<div class="m2"><p>بر حلاوت بسته‌ای دل چون‌ گره در نیشکر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فکر فردا چند از این خاک غبار آماده است</p></div>
<div class="m2"><p>هم‌ تو خواهی بود صبح خویش یا صبح دگر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سیر رنگ و بو هوس داری زگل غافل مباش</p></div>
<div class="m2"><p>شوخی پرواز نتوان دید جز در بال و پر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چند باید شد هوس‌فرسود کسب اعتبار</p></div>
<div class="m2"><p>سر هم ای غافل نمی‌ارزد به چندین دردسر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>منزل سرگشتگان راه عجز افتادگی‌ست</p></div>
<div class="m2"><p>تا دل خاک است بیدل اشک را حد سفر</p></div></div>