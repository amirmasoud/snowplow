---
title: >-
    غزل شمارهٔ ۱۲۰۳
---
# غزل شمارهٔ ۱۲۰۳

<div class="b" id="bn1"><div class="m1"><p>محو طلبت گردی اگر داشته باشد</p></div>
<div class="m2"><p>آن سوی جهان عرض سحر داشته باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل آیهٔ فتحی است ز قرآن محبت</p></div>
<div class="m2"><p>زیر و زبر زخمی اگر داشته باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از شعلهٔ هم نسبتی لعل تو آب است</p></div>
<div class="m2"><p>هر چند که یاقوت جگر داشته باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما و من وحدت‌نگهان غیرتویی نیست</p></div>
<div class="m2"><p>این رشته محالست دو سر داشته باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن راکه زکیفیت چشمت نظری نیست</p></div>
<div class="m2"><p>از بیخبریها چه خبر داشته باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم تر ما نیز همان مرکز حسن است</p></div>
<div class="m2"><p>چون آینه‌گر پاس نظر داشته باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از طینت ظالم نتوان خواست مروت</p></div>
<div class="m2"><p>شمشیر کجا آب گهر داشته باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>امروز دم کر و فر خواجه بلند است</p></div>
<div class="m2"><p>البته که این سگ دو سه خر داشته باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سوز دلم از گریه چرا محو نگردید</p></div>
<div class="m2"><p>بر آتش اگر آب ظفر داشته باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سیلاب سرشکم همه گر یک مژه بالد</p></div>
<div class="m2"><p>تا خانهٔ خورشید خطر داشته باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>افسانهٔ هنگامهٔ اوهام مپرسید</p></div>
<div class="m2"><p>شامی‌که ندارم چه سحر داشته باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل من و آن ناله از عجز رسایی</p></div>
<div class="m2"><p>در نقش قدم‌گرد اثر داشته باشد</p></div></div>