---
title: >-
    غزل شمارهٔ ۱۷۵۵
---
# غزل شمارهٔ ۱۷۵۵

<div class="b" id="bn1"><div class="m1"><p>گر شود از خواب من خیال تو محبوس</p></div>
<div class="m2"><p>حسرت بالین من برد پر طاووس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساز حجابی نداشت محفل هستی</p></div>
<div class="m2"><p>سوخت دل شمع ما به حسرت فانوس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل نفسی بیش نیست مرکز الفت</p></div>
<div class="m2"><p>چند نشیند نفس در آینه محبوس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دامن بیحاصلی غبار ندارد</p></div>
<div class="m2"><p>رنگ حنا تهمتیست بر کف افسوس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا نکشد فطرت انفعال تریها</p></div>
<div class="m2"><p>شبنم ما را هواست پردهٔ ناموس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر ز گریبان مکش ‌که ریخته گردون</p></div>
<div class="m2"><p>شمع در این انجمن ز دیدهٔ جاسوس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منکر قدرت مشو که جغد ندارد</p></div>
<div class="m2"><p>جز به سر گنج پا ز طینت منحوس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گل به ‌کف و در غم بهار فسردن</p></div>
<div class="m2"><p>مزد تخیل پر است جلوهٔ محسوس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوشت اگر نیست نغمه‌سنج مخالف</p></div>
<div class="m2"><p>صوت موذن بس‌ است نالهٔ ناقوس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ریشه دوانده‌ست در بهار جنونم</p></div>
<div class="m2"><p>پیچش هر گردباد تا پر طاووس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل از این مزرع آنچه در نظر آمد</p></div>
<div class="m2"><p>دانه امل بود و آسیا کف افسوس</p></div></div>