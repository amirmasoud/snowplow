---
title: >-
    غزل شمارهٔ ۳۰۱
---
# غزل شمارهٔ ۳۰۱

<div class="b" id="bn1"><div class="m1"><p>ای آینهٔ حسن تمنای تو جان‌ها</p></div>
<div class="m2"><p>اوراق گلستان ثنای تو زبان‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌زمزمهٔ حمد تو قانون سخن را</p></div>
<div class="m2"><p>افسرده چو خون رگ تار است بیان‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از حسرت گلزار تماشای تو آبست</p></div>
<div class="m2"><p>چون شبنم گل آینه در آینه‌دان‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی‌تاب وصال است دل اما چه توان کرد</p></div>
<div class="m2"><p>جسم است به راهت گره رشتهٔ جان‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنجا که بود جلوه‌گه حسن کمالت</p></div>
<div class="m2"><p>چون آینه محو است یقین‌ها و گمان‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از مرحمت عام تو در کوی اجابت</p></div>
<div class="m2"><p>گم‌گشته اثرها به تک و پوی فغان ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از قوت تأیید تو تحریک نسیمی</p></div>
<div class="m2"><p>بر بحر کشد از شکن موج کمان‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در چارسوی دهر گذر کرد خیالت</p></div>
<div class="m2"><p>لبریز شد از حیرت آیینه دکان‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در پرده دل غیر خیالت نتوان یافت</p></div>
<div class="m2"><p>جولانکده پرتو ماهند کتان‌ها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در دیده بیدل نبود یک دل پر خون</p></div>
<div class="m2"><p>بی‌داغ هوای تو در تن لاله‌ستان‌ها</p></div></div>