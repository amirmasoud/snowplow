---
title: >-
    غزل شمارهٔ ۲۶۵۷
---
# غزل شمارهٔ ۲۶۵۷

<div class="b" id="bn1"><div class="m1"><p>در پردهٔ هر رنگ کمین کرده شکستی</p></div>
<div class="m2"><p>داده است قضا کارگه شیشه به مستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر نقش خیال تو و من بسته شکستی</p></div>
<div class="m2"><p>از هر دو جهان آن طرف آینه بستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمری‌ست بهار دل فردوس خیال است</p></div>
<div class="m2"><p>گل تخت چمن بارگه غنچه نشستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خجلت‌کش نومیدی‌ام از هستی موهوم</p></div>
<div class="m2"><p>کو آنقدرم رنگ ‌که آرد به شکستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فطرت چقدر گل ‌کند از پیکر خاکی</p></div>
<div class="m2"><p>کردند بلند آتشم از خانهٔ پستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چند که اقبال‌ کلاهم به فلک سود</p></div>
<div class="m2"><p>بی‌خاک شدن نقش مرا نیست نشستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کاری دگر است آنچه دلش حاصل جهد است</p></div>
<div class="m2"><p>این مزد مدان وعدهٔ هر آبله دستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از معبد نیرنگ مگویید و مپرسید</p></div>
<div class="m2"><p>ماییم همان سایهٔ خورشید پرستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گل کن به نم جبهه غباری ‌که نداری</p></div>
<div class="m2"><p>درکشو‌ر اوهام چه بندی و چه بستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هشدار که در عرصهٔ همت نتوان یافت</p></div>
<div class="m2"><p>چون سعی‌ گذشتن ز نشان صافی شستی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل اثر سعی ندامت اگر این است</p></div>
<div class="m2"><p>آتش به دو عالم فکن از سودن دستی</p></div></div>