---
title: >-
    غزل شمارهٔ ۱۶۵۲
---
# غزل شمارهٔ ۱۶۵۲

<div class="b" id="bn1"><div class="m1"><p>بر تماشای فنایم دوخت پیریها نظر</p></div>
<div class="m2"><p>یافتم در حلقه‌گشتن حلقهٔ چشم دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از هجوم حیرتم راه تپیدن وانشد</p></div>
<div class="m2"><p>پیکرم سر تا قدم اشکی‌ست در چشم‌گهر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رفت آن سامان‌که در هر چشم سیلی داشتم</p></div>
<div class="m2"><p>این زمانم آب باید شد به یاد چشم تر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون سپند آخر نمی‌دانم کجا خواهم رسید</p></div>
<div class="m2"><p>می‌روم از خود به دوش ناله‌های خود اثر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>معنی دل در خم و پیچ امل‌ گم‌ کرده‌ام</p></div>
<div class="m2"><p>یک گره تا کی به چندین رشته باشد جلوه‌گر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسکه سامان بهار عیش امکان وحشت است</p></div>
<div class="m2"><p>می‌زند گل از نفس چون صبح دامن بر کمر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شبنمی در کار دارد گلشن عرض قبول</p></div>
<div class="m2"><p>جز خجالت هرچه آن‌جا می‌توان بردن مبر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جوهر اصلی ندامت می‌کشد از اعتبار</p></div>
<div class="m2"><p>رو به ناخن می‌کند چون سکه پیدا کرد زر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لب گشودنهای ظالم بی‌ غبار کینه نیست</p></div>
<div class="m2"><p>می‌شمارد عقده‌های سنگ پرواز شرر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عافیت مخمور شد تا ساغر جرأت زدیم</p></div>
<div class="m2"><p>آشیان خمیازه‌ گشت از دستگاه بال و پر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دود سودای تنزه از دماغ خود برآر</p></div>
<div class="m2"><p>گر پری خواهی تماشاکن دکان شیشه‌گر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در دکان وهم و ظن بیدل قماش غیر نیست</p></div>
<div class="m2"><p>خودفروشیهاست آنجا غیر ما از ما مخر</p></div></div>