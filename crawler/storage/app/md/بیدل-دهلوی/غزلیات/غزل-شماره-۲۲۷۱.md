---
title: >-
    غزل شمارهٔ ۲۲۷۱
---
# غزل شمارهٔ ۲۲۷۱

<div class="b" id="bn1"><div class="m1"><p>آمدم طرح بهار تازه‌ای انشا کنم</p></div>
<div class="m2"><p>یک دوگلشن بشکفم چشمی به رویت واکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از فسردن هر بن مویم مزار حیرتست</p></div>
<div class="m2"><p>زان تبسمها جهانی مرده را احیا کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خمارآباد امکان ساغر دیگر کجاست</p></div>
<div class="m2"><p>التفاتی واکشم زان چشم و مستیها کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غنچه خرمن می‌کند شوقم زمین تا آسمان</p></div>
<div class="m2"><p>بوسه‌واری گر به خاک آستانت جا کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فکر آن قامت جهانی را بلند آوازه‌ کرد</p></div>
<div class="m2"><p>رخصت نازی‌ که من هم مصرعی رعنا کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شرم حسنم ساغر تکلیف چندین بیخودیست</p></div>
<div class="m2"><p>بر قفا افتم چو مژگان گر مژه بالا کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در شکایت‌ نامه‌ام چون کاغذ آتش زده</p></div>
<div class="m2"><p>نقطه پر پیدا کند تا نامه‌بر پیدا کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناز پرورد تغافل خانهٔ یکتایی‌ام</p></div>
<div class="m2"><p>هر کجا آیینه‌ای را بینم استغنا کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قطرهٔ اشکی به توفان آورم‌ کز حسرتش</p></div>
<div class="m2"><p>تشنه‌کامی را صدای ساغر دریا کنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عشق بیدل گر بساط نازم آراید چو شمع</p></div>
<div class="m2"><p>آنقدر گردن ‌کشم از خود که سر را پا کنم</p></div></div>