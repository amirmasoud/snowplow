---
title: >-
    غزل شمارهٔ ۳۶۸
---
# غزل شمارهٔ ۳۶۸

<div class="b" id="bn1"><div class="m1"><p>بزم ما را نیست غیر از شهرت عنقا شراب</p></div>
<div class="m2"><p>کز صدای جام نتوان فرق ‌کردن تا شراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ظرف‌و مظروف توهم‌گاه هستی حیرت است</p></div>
<div class="m2"><p>کس چه ‌بندد طرف ‌مستی زین پری مینا شراب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مقصد حیرت خرام اشک بیتابم مپرس</p></div>
<div class="m2"><p>نشئه بیرون‌تاز ادراک است و خون‌پیما شراب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما به امید گداز دل به خود بالیده‌ایم</p></div>
<div class="m2"><p>یعنی این انگور هم خواهد شدن فردا شراب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ره ما از شکست شیشه‌های آبله</p></div>
<div class="m2"><p>می‌فروشد همچو جام باده نقش پا شراب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در سیهکاری سواد گریه روشن کرده‌ایم</p></div>
<div class="m2"><p>صاف می‌آید برون از پردهٔ شبها شراب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیچ وتاب موج زلف جوهر انشا می‌کند</p></div>
<div class="m2"><p>گر نماید چهر در آینهٔ مینا شراب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خار و خس را می‌نشاند شعله در خاک سیاه</p></div>
<div class="m2"><p>عاقبت هول هوس را می‌کند رسوا شراب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون لب ‌ساحل نصیب ما همان خمیازه است</p></div>
<div class="m2"><p>گر همه در کام ما ریزند یک دریا شراب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>امتیازی در میان آمد دورنگی نقش بست</p></div>
<div class="m2"><p>کرد بیدل ساغر ما را گل رعنا شراب</p></div></div>