---
title: >-
    غزل شمارهٔ ۲۴۲۱
---
# غزل شمارهٔ ۲۴۲۱

<div class="b" id="bn1"><div class="m1"><p>آینهٔ وصل چیست‌، حیرتی آراستن</p></div>
<div class="m2"><p>وز اثر ما و من یک دو نفس کاستن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مفت تماشاست حسن لیک به شکر نگاه</p></div>
<div class="m2"><p>از سر خود بایدت چون مژه برخاستن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جلوهٔ رنگ دویی خون حیا می‌خورد</p></div>
<div class="m2"><p>سخت ادب دشمنی‌ست آینه آراستن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به ‌که به پیش ‌کریم نازکنی وقت جرم</p></div>
<div class="m2"><p>ورنه ز کم همتی‌ست عذر گنه خواستن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عیش و غم روزگار طعمهٔ یکدیگرند</p></div>
<div class="m2"><p>حاصل روز و شب است در بر هم ‌کاستن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست‌کف خاک ما قابل عرض غبار</p></div>
<div class="m2"><p>پیشتر از ما نشست جرأت برخاستن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیدل اگر محرمی جلوهٔ بیرنگ باش</p></div>
<div class="m2"><p>دام تماشا مکن‌ کلفت پیراستن</p></div></div>