---
title: >-
    غزل شمارهٔ ۲۷۳۶
---
# غزل شمارهٔ ۲۷۳۶

<div class="b" id="bn1"><div class="m1"><p>ای شیخ به تدبیر امل بیهده حرفی</p></div>
<div class="m2"><p>دستار به کهسار میفکن تل برفی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همنسبتی جوهر رازت چه خیال است</p></div>
<div class="m2"><p>از وهم برون ‌آ،‌ کف این قلزم ژرفی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دون فطرتیت غیر جنون هیچ ندارد</p></div>
<div class="m2"><p>بر حوصلهٔ پوچ مناز آبله ظرفی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در عالم برق و شرر امید وفا نیست</p></div>
<div class="m2"><p>هستی رم نازست و تو حسرت‌کش طرفی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با نقش خیال این همه رعنا نتوان زیست</p></div>
<div class="m2"><p>چون پیکر طاووس ز نیرنگ شگرفی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بحث من و ما برده‌ای آن سوی قیامت</p></div>
<div class="m2"><p>ای مدٌ نفس با همه فرصت دو سه حرفی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیدل ادب علم و فن از دور بجا آر</p></div>
<div class="m2"><p>جزخجلت تقریرنه نحوی و نه صرفی</p></div></div>