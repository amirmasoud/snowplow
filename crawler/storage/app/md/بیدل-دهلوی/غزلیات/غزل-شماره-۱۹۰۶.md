---
title: >-
    غزل شمارهٔ ۱۹۰۶
---
# غزل شمارهٔ ۱۹۰۶

<div class="b" id="bn1"><div class="m1"><p>ز خودفروشی پرواز بسکه دارم ننگ</p></div>
<div class="m2"><p>چو اشک شمع چکیده‌ست خونم آنسوی رنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به قدرآگهی اسباب وحشت است اینجا</p></div>
<div class="m2"><p>سواد دیدهٔ آهو بس است داغ پلنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمی‌شود طرف نرمخو درشتی دهر</p></div>
<div class="m2"><p>به روی آب محالست ایستادن سنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو ناخدای محیط غرور باش‌که من</p></div>
<div class="m2"><p>ز جیب خوبش فرورفته‌ام به‌کام نهنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به نیم چشم‌زدن وصل مقصد است اینجا</p></div>
<div class="m2"><p>شرارما نکشد زحمت ره و فرسنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به اعتبار اگر وارسی نمی‌ارزد</p></div>
<div class="m2"><p>گشاده‌رویی‌گوهر به خجلت دل تنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به ذوق‌کینه ستم پیشه زندگی دارد</p></div>
<div class="m2"><p>کمان همین نفسی می‌کشد به زورخدنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به قدر عجز ازین دامگاهت آزادیست</p></div>
<div class="m2"><p>که دل شکاف قفس دارد از شکستن رنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جز این‌که کلفت بیجا کشد چه سازد کس</p></div>
<div class="m2"><p>جهان‌المکده و آرزو نشاط آهنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز صورت ارهمه معنی شوی رهایی نیست</p></div>
<div class="m2"><p>فتاده است جهانی به قیدگاه فرنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به‌کسب نی نفسی زن صفای دل درباب</p></div>
<div class="m2"><p>گشودن مژه آیینه راست رفتن رنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وبال دوش‌کسان بودن از حیا دور است</p></div>
<div class="m2"><p>نبسته است‌کسی پا به‌گردنت چو تفنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>درین محیط ز مضمون اعتبار مپرس</p></div>
<div class="m2"><p>حباب بست نفس بسکه دید قافیه تنگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو نام تکیه به نقش نگین مکن بیدل</p></div>
<div class="m2"><p>که جز شکست چه دارد سر رسیده به سنگ</p></div></div>