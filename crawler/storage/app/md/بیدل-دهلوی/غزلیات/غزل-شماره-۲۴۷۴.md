---
title: >-
    غزل شمارهٔ ۲۴۷۴
---
# غزل شمارهٔ ۲۴۷۴

<div class="b" id="bn1"><div class="m1"><p>درس کمال خود گیر از ناله سر کشیدن</p></div>
<div class="m2"><p>تا برنیایی از خویش نتوان به خود رسیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوبی یکی هزار است از شیوهٔ تواضع</p></div>
<div class="m2"><p>ابروی نازگردد شاخ گل از خمیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا گوش می‌توان شد نتوان همه زبان شد</p></div>
<div class="m2"><p>نقصان نمی‌فروشد سرمایهٔ شنیدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای هرزه جلوه فهمان غافل ز دل مباشید</p></div>
<div class="m2"><p>کوری درشت رو‌یی آیینه را بدیدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز عجز سعی ناقص چیزی نمی‌برد پیش</p></div>
<div class="m2"><p>افتادن است چون اشک اطفال را دویدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فقر و حضور تمکین جاه و هزار خفت</p></div>
<div class="m2"><p>از بحر بیقراری از ساحل آرمیدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حیفست محرم دل ‌گردد فسانه مایل</p></div>
<div class="m2"><p>آیینه در مقابل آنگه نفس‌ کشیدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از تیغ مرگ عشاق رنگ بقا نبازند</p></div>
<div class="m2"><p>عمر دوباره گیرند چون ناخن از بریدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا جلوه ‌کرد شوخی حسن تو در عرق زد</p></div>
<div class="m2"><p>دارد حیا به این رنگ آیینه آفریدن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صید کمند عجزم سامان وحشتم‌ کو</p></div>
<div class="m2"><p>رنگ شکسته دارد صد رنگ دام چیدن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طاووس این بهارم ساغرکش خمارم</p></div>
<div class="m2"><p>در راه انتظارم صد چشم و یک پریدن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر هستی‌ام به این رنگ محجوب خودنماییست</p></div>
<div class="m2"><p>آیینه برنیارد تصویر از کشیدن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون تخم اشک بیدل نومیدی آبیارم</p></div>
<div class="m2"><p>بی‌برگ ازین گلستان می‌بایدم دمیدن</p></div></div>