---
title: >-
    غزل شمارهٔ ۱۳۲۱
---
# غزل شمارهٔ ۱۳۲۱

<div class="b" id="bn1"><div class="m1"><p>ذره تا خورشید امکان جمله حیرت زاده‌اند</p></div>
<div class="m2"><p>جز به دیدار تو چشم هیچکس نگشاده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلق آنسوی فلک پر می‌زند اما هنوز</p></div>
<div class="m2"><p>چون نفس از خلوت دل پا برون ننهاده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکدل اینجا فارغ از تشویش نتوان یافتن</p></div>
<div class="m2"><p>این منازل یکسر از آشفتگیها جاده‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون حباب آزاداصعان هم دپن دریای وهم</p></div>
<div class="m2"><p>در ته باری‌که بر دل نیست دوشی داده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جلوهٔ او عالمی را خودپرست وهم‌ کرد</p></div>
<div class="m2"><p>حسن پرکار است و این آیینه‌ها پر ساده اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شمع‌سان داغ وگداز و اشک و آه و سوختن</p></div>
<div class="m2"><p>هم به پایت تا ز پا ننشسته‌ای استاده اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این طربهایی که احرام امیدش بسته‌ای</p></div>
<div class="m2"><p>چون طلسم رنگ گل یکسر شکست آماده‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مطلب عشاق نافهمیده روشن می‌شود</p></div>
<div class="m2"><p>در پر عنقاست مکتوبی که نفرستاده اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>راز مستان‌ کیست تا پوشد که این حق‌مشربال</p></div>
<div class="m2"><p>خون منصوری دو بال جوش چندین باده اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پرسش احوال ما وصف خرام ناز تست</p></div>
<div class="m2"><p>عاجزان چون سایه هرجا پا نهی افتاده‌اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بی‌سیاهی نیست بیدل صورت ایجاد خط</p></div>
<div class="m2"><p>یک قلم معنی‌طرازان تیره‌بختی زاده‌اند</p></div></div>