---
title: >-
    غزل شمارهٔ ۲۵۹۵
---
# غزل شمارهٔ ۲۵۹۵

<div class="b" id="bn1"><div class="m1"><p>ای فکر نازکت را شبهت ‌کمینی از مو</p></div>
<div class="m2"><p>تشویش عطسه تا کی مانند بینی از مو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کارگاه فطرت نام شکست ننگست</p></div>
<div class="m2"><p>باید قلم نبندد نقاش چینی از مو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل آتش تو دارد ضبط نفس چه حرفست</p></div>
<div class="m2"><p>اخگر نمی‌پسندد نقش نگینی از مو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیرنگ التفاتت مغرور کرد ما را</p></div>
<div class="m2"><p>افسون آفتاب است مار آفرینی از مو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تعظیم ناتوانان دشواریی ندارد</p></div>
<div class="m2"><p>بر عضوها گران نیست بالا نشینی از مو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کم نیست شخص ما را در کسوت ضعیفی</p></div>
<div class="m2"><p>از رشته دامنیها یا آستینی از مو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بالیدم از تخیل سرکوب آسمانها</p></div>
<div class="m2"><p>بر خود نچیدم اما فرق یقینی از مو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عمریست ناتوانان ممنون آن نگاهند</p></div>
<div class="m2"><p>ای دیدهٔ مروت زحمت نبینی از مو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما را شکیب دل برد آنسوی خود فروشی</p></div>
<div class="m2"><p>شبگیر کرد بیدل آواز چینی از مو</p></div></div>