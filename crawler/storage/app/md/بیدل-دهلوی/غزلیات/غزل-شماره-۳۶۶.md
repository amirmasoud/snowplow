---
title: >-
    غزل شمارهٔ ۳۶۶
---
# غزل شمارهٔ ۳۶۶

<div class="b" id="bn1"><div class="m1"><p>سایه اندازد اگر بخت سیاه من در آب</p></div>
<div class="m2"><p>فلس ماهی دیدهٔ آهوکند خرمن در آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر نگه در دیدهٔ من ناله‌است اما چه سود</p></div>
<div class="m2"><p>حلقهٔ زنجیر نومید است از شیون درآب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی توانم در دل سنگین خوبان جاکنم</p></div>
<div class="m2"><p>من‌که نتوانم فروبردن سر سوزن درآب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راه غربت عارفان را در وطن پوشیده نیست</p></div>
<div class="m2"><p>گوهر ازگرداب دارد هر طرف روزن در آب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ظاهر و باطن به‌گرد عرض یکدیگرگم است</p></div>
<div class="m2"><p>آب درگلشن نمایان است چون‌گلشن در آب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پوچ می‌آیی برون ازلاف هستی دم مزن</p></div>
<div class="m2"><p>نیست‌بی‌عرض‌حباب‌از قطره‌خندیدن در آب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما ضعیفان شبنم واماندهٔ این‌گلشنیم</p></div>
<div class="m2"><p>از نم اشکی‌ست ما را دیده تا دامن در آب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر چنین جوشد عرق از هرزه‌تازیهای فکر</p></div>
<div class="m2"><p>نسخهٔ ما را خجالت خواهد افکندن درآب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غرق دنیاییم‌کو ساز منزه زیستن</p></div>
<div class="m2"><p>جبههٔ‌فطرت تر است‌از دامن‌افشردن‌در آب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نرمی گفتار ظالم بی‌فسون‌کینه نیست</p></div>
<div class="m2"><p>صنعتی دارد حسد از شعله پروردن درآب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هوش می‌باید قوی با چشم بیناکار نیست</p></div>
<div class="m2"><p>جز به پا ممکن نباشد پیش پا دیدن در آب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یک نگه نادیده رخسار عرق‌آلوده‌اش</p></div>
<div class="m2"><p>چون تری عمری‌ست بیدل‌کرده‌ام مسکن درآب</p></div></div>