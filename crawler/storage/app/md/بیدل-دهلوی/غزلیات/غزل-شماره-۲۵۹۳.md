---
title: >-
    غزل شمارهٔ ۲۵۹۳
---
# غزل شمارهٔ ۲۵۹۳

<div class="b" id="bn1"><div class="m1"><p>کجایی ای جنون ویرانه ات کو</p></div>
<div class="m2"><p>خس و خاریم آتشخانه‌ات کو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>الم پیمایم از کم ظرفی هوش</p></div>
<div class="m2"><p>شراب عافیت پیمانه‌ات کو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو شمع بی‌نیازبها بر افروز</p></div>
<div class="m2"><p>مگو خاکستر پروانه‌ات کو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر اشکی چه شد رنگ‌گدازت</p></div>
<div class="m2"><p>و گر آهی رم دیوانه‌ات‌ کو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر ساغر پرست خواب نازی</p></div>
<div class="m2"><p>چو مژگان لغزش مستانه‌ات‌کو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرفتم موشکاف زلف رازی</p></div>
<div class="m2"><p>زبان بینوای شانه‌ات کو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز هستی تا عدم یک نعره واری</p></div>
<div class="m2"><p>ولیکن همت مردانه‌ات کو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کمان قبضهٔ آفاقی اما</p></div>
<div class="m2"><p>برون از خود سراغ خانه‌ات‌کو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بساط و هم واچیدن ندارد</p></div>
<div class="m2"><p>نوا افسانه‌ای افسانه‌ات کو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حجاب آشنایی قید خویش است</p></div>
<div class="m2"><p>زخود گر بگذری بیگانه‌ات کو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ندارد این قفس سامان دیگر</p></div>
<div class="m2"><p>گرفتم آب شد دل دانه‌ات‌ کو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سرت بیدل هوا فرسود راهیست</p></div>
<div class="m2"><p>دماغ کعبه و بتخانه‌ات کو</p></div></div>