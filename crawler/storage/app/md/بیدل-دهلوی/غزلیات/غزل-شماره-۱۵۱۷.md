---
title: >-
    غزل شمارهٔ ۱۵۱۷
---
# غزل شمارهٔ ۱۵۱۷

<div class="b" id="bn1"><div class="m1"><p>به نظم عمرکه سر تا سرش روانی بود</p></div>
<div class="m2"><p>خیال هستی موهوم سکته‌خوانی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه رنگها که ندادم به بادپیمایی</p></div>
<div class="m2"><p>بهار شمع در این انجمن خزانی بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیافت عشق جفاپیشه قابل ستمی</p></div>
<div class="m2"><p>همیشه بسمل این تیغ امتحانی بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هنوزآن پری ازسنگ فرق شیشه نداشت</p></div>
<div class="m2"><p>که دل شررکده ی چشمک نهانی بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به‌کام دل نگشودیم بال پروازی</p></div>
<div class="m2"><p>چو رنگ‌، هستی ما گرد پرفشانی بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پس از غبار شدن گشت اینقدر معلوم</p></div>
<div class="m2"><p>که بار ما همه بر دوش ناتوانی بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به خاک راه تو یکسان شدیم و منفعلیم</p></div>
<div class="m2"><p>که سجده نیز درین راه سرگردانی بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طراوت گل اظهار شبنمی می‌خواست</p></div>
<div class="m2"><p>ز خجلت آب نگشتن چه زندگانی‌ بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>علم به هرزه‌درایی شدیم ازین غافل</p></div>
<div class="m2"><p>که صد کتاب سخن محو بیزبانی بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تلاش موج درتن بحر هیچ پیش نرفت</p></div>
<div class="m2"><p>گهر دمیدن ما پاس بیکرانی بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جهان ‌گذرگه آیینه است و ما نفسیم</p></div>
<div class="m2"><p>تو هم چو ما نفسی باش اگر ‌توانی بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فریب معرفتی خورده بود بیدل ما</p></div>
<div class="m2"><p>چو وارسید یقینها همه‌ گمانی بود</p></div></div>