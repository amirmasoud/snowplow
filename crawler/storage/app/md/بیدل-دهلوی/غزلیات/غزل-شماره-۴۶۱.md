---
title: >-
    غزل شمارهٔ ۴۶۱
---
# غزل شمارهٔ ۴۶۱

<div class="b" id="bn1"><div class="m1"><p>بی‌شکست از پردهٔ سازم نوایی برنخاست</p></div>
<div class="m2"><p>ناامیدی داشتم دست دعایی برنخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخت بی‌رنگ است نقش وحدت عنقایی‌ام</p></div>
<div class="m2"><p>جستجوها خاک شد گردی ز جایی برنخاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اشک مجنونم‌که تا یأسم ره دامان‌گرفت</p></div>
<div class="m2"><p>جز همان چاک‌گریبان رهنمایی برنخاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرکه‌ازخودمی‌رودمحمل به‌دوش‌حسرت‌است</p></div>
<div class="m2"><p>گرد ما واماندگان هم بی‌هوایی برنخاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جزنفس در ماتم دل هیچ‌کس دستی نسود</p></div>
<div class="m2"><p>از چراغ‌کشته غیر از دوده‌هایی برنخاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قطع اوهام تعلق آنقدر مشکل نبود</p></div>
<div class="m2"><p>آه از دل نالهٔ تیغ آزمایی برنخاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عجز و طاقت جوهرکیفیت یکدیگرند</p></div>
<div class="m2"><p>برکرم ظلم است اگر دست‌گدایی برنخاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیگر از یاران این محفل چه باید داشت چشم</p></div>
<div class="m2"><p>صد جفا بردیم و زینها مرحبایی برنخاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ساز ما عاجزنوایان دست برهم سوده بود</p></div>
<div class="m2"><p>عمر در شغل تأسف رفت و وایی برنخاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خاک شد امید پیش از نقش بستنهای ما</p></div>
<div class="m2"><p>شعله تا ننشست داغ از هیچ جایی برنخاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جلوه درکار است اما جرأت نظاره‌کو</p></div>
<div class="m2"><p>از بساط عجز ما مژگان عصایی برنخاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در زمین آرزو بیدل املها کاشتیم</p></div>
<div class="m2"><p>لیک غیر از حسرت نشو و نمایی برنخاست</p></div></div>