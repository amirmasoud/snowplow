---
title: >-
    غزل شمارهٔ ۲۴۱۷
---
# غزل شمارهٔ ۲۴۱۷

<div class="b" id="bn1"><div class="m1"><p>تا تب عشق آتشم را داد سر در سوختن</p></div>
<div class="m2"><p>پنبه شد خاکستر از شور مکرر سوختن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هستی عشّاق از آیین جهان دیگر است</p></div>
<div class="m2"><p>بسته جز آتش دو عالم بر سمندر سوختن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روشن است اقبال ما چون شمع در ملک جنون</p></div>
<div class="m2"><p>تخت داغ و لشکر آه و اشک افسر سوختن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دل افسرده خون‌ها می‌خورد ناموس عشق</p></div>
<div class="m2"><p>آتش یاقوت دارد تا به محشر سوختن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند بیند آرزو در دیر نیرنگ خیال</p></div>
<div class="m2"><p>چون خیال بی‌تمیزان می به ساغر سوختن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با وجود وصل در بزم حضورم بار نیست</p></div>
<div class="m2"><p>بشنو از پروانه دیگر قصهٔ پر سوختن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل به دست آور تلاش دیگرت آوارگی‌ست</p></div>
<div class="m2"><p>موج را باید نفس در سعی ‌گوهر سوختن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی‌ندامت نیست عشق از نسبت طبع فضول</p></div>
<div class="m2"><p>گریه‌ها دارد ز دست هیزم تر سوختن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همچو اخگر خواب راحت خواهدت بیدار کرد</p></div>
<div class="m2"><p>نیست غافل‌ گرمی پهلو ز بستر سوختن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شب به دل‌ گفتم چه باشد آبروی زندگی</p></div>
<div class="m2"><p>گفت چون پروانه در آغوش دلبر سوختن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نقطه‌ای چند از شرار کاغذم ‌کرده‌ست داغ</p></div>
<div class="m2"><p>بی‌تکلف انتخابی داشت دفتر سوختن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>میهمان عبرتی ای شمع ‌پُر بر خود مبال</p></div>
<div class="m2"><p>تا بود پهلوی چربت نیست‌ لاغرسوختن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با دل مأیوس عهدی بسته‌ایم و چاره نیست</p></div>
<div class="m2"><p>کس چه سازد نیست بیدل جای دیگر سوختن</p></div></div>