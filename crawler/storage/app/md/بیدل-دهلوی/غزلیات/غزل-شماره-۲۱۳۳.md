---
title: >-
    غزل شمارهٔ ۲۱۳۳
---
# غزل شمارهٔ ۲۱۳۳

<div class="b" id="bn1"><div class="m1"><p>همچو شمع از خویش برانداز وحشت برترم</p></div>
<div class="m2"><p>بسکه دامن چیدم از خود زیر پا آمد سرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناامیدیهای مطلب پر نزاکت نشئه بود</p></div>
<div class="m2"><p>از شکست آبرو لبریز دل شد ساغرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر بن موی مرا با آه حسرت چشمکی است</p></div>
<div class="m2"><p>سرمه‌ها دارد ز دود خویش چشم مجمرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در غبار نیستی هم آتشم افسرده نیست</p></div>
<div class="m2"><p>داغ چون اخگر نمکسود است از خاکسترم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌گشایم سر به مُهر اشک طومار نگاه</p></div>
<div class="m2"><p>نیست بیرون‌گره یک رشته موج‌ گوهرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو آن‌کلکی‌که فرساید به‌تحریر نیاز</p></div>
<div class="m2"><p>نگذرم از سجده‌ات چندانکه از خود بگذرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صفحهٔ آیینه محتاج حک و اصلاح نیست</p></div>
<div class="m2"><p>بسکه بی‌نقش است شستن شسته‌ام از دفترم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عالم یکتایی از وضع تصنع برتر است</p></div>
<div class="m2"><p>من تو گردم یا تو من اینها نیاید باورم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دعوی دل دارم و دل نیست در ضبط نفس</p></div>
<div class="m2"><p>عمر ها شد ناخدای کشتی بی‌لنگرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرگ هم در زندگی آسان نمی‌آید به دست</p></div>
<div class="m2"><p>تا ز هستی جان برم عمریست زحمت می‌برم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مستی طاووس من تا صد قدح مخمور ماند</p></div>
<div class="m2"><p>ظلمت من بر نمی‌دارد چراغان پرم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیکسی بیدل چه دارد غیر تدبیر جنون</p></div>
<div class="m2"><p>طرف دامانی نمی‌یابم گریبان می‌درم</p></div></div>