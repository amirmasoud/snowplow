---
title: >-
    غزل شمارهٔ ۴۸۷
---
# غزل شمارهٔ ۴۸۷

<div class="b" id="bn1"><div class="m1"><p>سفله با جاه نیزهیچکس است</p></div>
<div class="m2"><p>مور اگر پر برآورد مگس است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نفس را بی‌شکنجه مگذارید</p></div>
<div class="m2"><p>سگ‌ دیوانه مصلحش مرس است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خفت اهل شرم بیباکی‌ست</p></div>
<div class="m2"><p>چون پرد چشم پایمال خس است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منفعل نیست خلق هرزه معاش</p></div>
<div class="m2"><p>دو جهان یک دماغ بوالهوس است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر امید گشاد عقدهٔ کار</p></div>
<div class="m2"><p>چشم اگر باز کرده‌ایم بس است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خون افسرده‌ایم باقی هیچ</p></div>
<div class="m2"><p>خرقهٔ‌ ما چو پوست بر عدس است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرصت رفته نیست باب سراغ</p></div>
<div class="m2"><p>کاروان خیال بی‌جرس است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آینه نسبتی به دل دارد</p></div>
<div class="m2"><p>که مقام تأمل نفس است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مفلسان را، ز عالم اسباب</p></div>
<div class="m2"><p>تاگریبان تمام دسترس است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرکه جست از عدم به‌هستی ساخت</p></div>
<div class="m2"><p>یک قدم پیش آشیان نفس است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل از خاک می‌رویم به باد</p></div>
<div class="m2"><p>غیر ازین‌نیست‌آنچه پیش‌و پس است</p></div></div>