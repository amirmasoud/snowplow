---
title: >-
    غزل شمارهٔ ۲۶۰۶
---
# غزل شمارهٔ ۲۶۰۶

<div class="b" id="bn1"><div class="m1"><p>تا به شوخی نکشد زمزمهٔ ساز نگاه</p></div>
<div class="m2"><p>مردمک شد ز ازل سرمهٔ آواز نگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در تماشای توام رنگ اثر باختن است</p></div>
<div class="m2"><p>همچو چشمم همه تن ‌گرد تک و تاز نگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر همه آب بود آینه بینایی‌کو</p></div>
<div class="m2"><p>نرسد اشک به‌کیفیت انداز نگاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیگر از عاقبت تشنهٔ دیدار مپرس</p></div>
<div class="m2"><p>هست از خویش برون تاختن ناز نگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو شمعی‌که‌کند دود پس از خاموشی</p></div>
<div class="m2"><p>حسرتت زمزمه‌ای می‌کشد از ساز نگاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طوبی از سایهٔ ناز مژه‌ام می‌بالد</p></div>
<div class="m2"><p>چقدر سرو توام کرده سرافراز نگاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشق جمعیت دل قدرت دیگر دارد</p></div>
<div class="m2"><p>بر فسلک نیز نلغزید رسن باز نگاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غم اسباب تعلق نکشد صاحب دل</p></div>
<div class="m2"><p>مژه صیقل نزند آینه پرداز نگاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرد غفلت مشکافید که در عرصهٔ رنگ</p></div>
<div class="m2"><p>بی‌نشانی‌ست خطای قدرانداز نگاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون شرارم چقدر محمل ناز آراید</p></div>
<div class="m2"><p>یک تپش‌گرد دل و یک مژه پرواز نگاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل از نور نظر صافی دل مستغنی‌ست</p></div>
<div class="m2"><p>کسب بینش نکند آینهٔ ناز نگاه</p></div></div>