---
title: >-
    غزل شمارهٔ ۱۷۹۰
---
# غزل شمارهٔ ۱۷۹۰

<div class="b" id="bn1"><div class="m1"><p>سخن‌سنجی‌که مدح خلق نفریبد به وسواسش</p></div>
<div class="m2"><p>مسیحای جهان مرده گردد صبح انفاسش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نفس محمل‌کش چندین غنا و فقر می‌باشد</p></div>
<div class="m2"><p>که در هر آمد و رفتی است‌ گرد جاه افلاسش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز تار و پود اضداد است عبرت بافی‌ گردون</p></div>
<div class="m2"><p>کجی و راستی شد جمع تا گل‌ کرد کرباسش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فسردن هم ‌کمالش پاس آب روست در معنی</p></div>
<div class="m2"><p>نگین از کندن آزاد است اگر سازی ز الماسش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فلک سازیست مستغنی ز وضع هرزه آهنگی</p></div>
<div class="m2"><p>.من و مای تو می‌باشد گر آوازی است در طاسش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا بر بی‌نیازیهای مجنون رشک می‌آید</p></div>
<div class="m2"><p>که گم کرده‌ست راه و نیست یاد از خضر و الیاسش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکوه عزت از اقبال دونان ننگ می‌دارد</p></div>
<div class="m2"><p>بلندی تاکجا بر آبله خندد ز آماسش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو زین مزرع نموهای درو آماده‌ای داری</p></div>
<div class="m2"><p>که در هر ماه چون ناخن زگردون می‌دمد داسش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به اقلیم عدم گم کرد انسان ذوق سلطانی</p></div>
<div class="m2"><p>که وهم هستی افکند این زمان در دست کناسش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حباب بیدل ما را غم دیگر نمی‌باشد</p></div>
<div class="m2"><p>نفس زندانی شرم است باید داشتن پاسش</p></div></div>