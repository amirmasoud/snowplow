---
title: >-
    غزل شمارهٔ ۱۱۰۱
---
# غزل شمارهٔ ۱۱۰۱

<div class="b" id="bn1"><div class="m1"><p>اگر نظّاره‌ گل می‌توان کرد</p></div>
<div class="m2"><p>وطن در چشم بلبل می‌توان‌ کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین محفل ز یک مینا بضاعت</p></div>
<div class="m2"><p>به چندین نغمه قلقل می‌توان‌کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عرق‌واری گر از شرم آب گردم</p></div>
<div class="m2"><p>به جام عالمی مل می‌توان‌کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نظر بر خویش واکردن محال ا‌ست</p></div>
<div class="m2"><p>اگرگویی تغافل می‌توان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو صبح این یک نفس ‌گردی ‌که داریم</p></div>
<div class="m2"><p>اگر بالد تجمل می‌توان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به هر محفل‌که زلفش سایه افکند</p></div>
<div class="m2"><p>ز دود شمع ‌کاکل می‌توان ‌کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شهید حسرت آن ‌گلعذارم</p></div>
<div class="m2"><p>ز زخم خنده برگل می‌توان کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به هر جا سطری از زلفش نوبسند</p></div>
<div class="m2"><p>قلم از شاخ سنبل می‌توان ‌کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درین ‌گلشن اگر رنگست و گر بوست</p></div>
<div class="m2"><p>قیاس بال بلبل می‌توان کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر این است عیش خاکساری</p></div>
<div class="m2"><p>ز پستی هم تنزل می‌توان‌کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>محیط بیخودی منصور جوش است</p></div>
<div class="m2"><p>به مستی جزو را کل می‌توان ‌کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ازین بی‌دانشان جان بردنی هست</p></div>
<div class="m2"><p>اگر اندک تجاهل می‌توان کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تردد مایهٔ بازار هستی‌ست</p></div>
<div class="m2"><p>اگر نبود توکل می‌توان کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پر آسان است ازین دریا گذشتن</p></div>
<div class="m2"><p>ز پشت پا اگر پل می‌توان ‌کرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دهان یار ناپیداست بیدل</p></div>
<div class="m2"><p>به فهم خود تأمل می‌توان‌ کرد</p></div></div>