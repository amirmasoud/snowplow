---
title: >-
    غزل شمارهٔ ۴۷۸
---
# غزل شمارهٔ ۴۷۸

<div class="b" id="bn1"><div class="m1"><p>رزق‌، خلوتگه اندیشهٔ روزی‌خوار است</p></div>
<div class="m2"><p>دانه هرگاه مژه بازکند منقار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قطرهٔ ما نشد آگاه تامل‌، ورنه</p></div>
<div class="m2"><p>موج این بحر گهرخیز گریبان زار است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>الفت جسم صفای دل ما داد به زنگ</p></div>
<div class="m2"><p>آب این آینه یکسر عرق‌ گلکار است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طرف دامان تعلق ز خراش ایمن نیست</p></div>
<div class="m2"><p>مفت ‌دیوانه‌ که صحرای ‌جنون بی‌خار است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از کج‌اندیشی ‌دل وضع جهان دلکش نیست</p></div>
<div class="m2"><p>غم تمثال مخور آینه ناهموار است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر تعین زده‌ای زحمت تحقیق مده</p></div>
<div class="m2"><p>سر سودایی سامان به گریبان بار است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بهاری‌که سر و برگ طرب رنگ فناست</p></div>
<div class="m2"><p>دست بر سر زدنت به زگل دستار است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ادب آموز هوستازی غفلت پیری‌ست</p></div>
<div class="m2"><p>سایه را پای به دامن‌، ز خم دیوار است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رنگها بال‌فشان می‌رود و می‌آید</p></div>
<div class="m2"><p>این چمن عالم تجدید کهن تکرار است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای ندامت مد‌د‌ی کز غم اسباب جهان</p></div>
<div class="m2"><p>دست سودن هوسی دارد و پُر بیکار است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل از زندگی آخر نتوان جان بردن</p></div>
<div class="m2"><p>رنگ این باغ هوس آتش بی‌زنهار است</p></div></div>