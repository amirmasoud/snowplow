---
title: >-
    غزل شمارهٔ ۴۷۱
---
# غزل شمارهٔ ۴۷۱

<div class="b" id="bn1"><div class="m1"><p>در تپش‌آباد دهر حیرت دل لنگر است</p></div>
<div class="m2"><p>مرکز دور محیط آب رخ‌گوهر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرخ ز سرگشتگی‌گرد سحر سازکرد</p></div>
<div class="m2"><p>سودن صندل همان شاهد دردسر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لاف هنر بیهده‌ست تا ننمایی عمل</p></div>
<div class="m2"><p>تیغ نگردد چنارگر همه تن جوهر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست غبار اثر محرم جولان ما</p></div>
<div class="m2"><p>کز عرق شرم عجز راه فضولی تر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رشتهٔ ساز امید درگره عجز سوخت</p></div>
<div class="m2"><p>شوق چه شوخی‌کند ناله نفس‌پرور است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رهرو تسلیم را، راحله افتادگی</p></div>
<div class="m2"><p>قافلهٔ عجز را خاک شدن رهبر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا به قبولی رسی دامن ایثارگیر</p></div>
<div class="m2"><p>شامهٔ آفاق را صیت‌کرم عنبر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بحث عدو را مده جز به تغافل جواب</p></div>
<div class="m2"><p>زانکه حدیث درشت درخورگوش کر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دام تپشهای دل حسرت سیر فناست</p></div>
<div class="m2"><p>شعلهٔ بیتاب ما بسمل خاکستر است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روی‌که‌دارد عرق‌، دیده‌سرشک آشناست</p></div>
<div class="m2"><p>زلف‌که در تاب‌رفت نسخهٔ‌دل ابتر است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چاک‌گریبان ما سینه به صحراگشود</p></div>
<div class="m2"><p>تنگی خلق جنون این همه وسعتگراست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل از این انجمن سرخوش دردیم و بس</p></div>
<div class="m2"><p>بزم چو باشد شراب آبله‌اش ساغر است</p></div></div>