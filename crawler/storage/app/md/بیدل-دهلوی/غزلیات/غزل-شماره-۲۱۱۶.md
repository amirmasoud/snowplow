---
title: >-
    غزل شمارهٔ ۲۱۱۶
---
# غزل شمارهٔ ۲۱۱۶

<div class="b" id="bn1"><div class="m1"><p>درین‌ گلشن نه بویی دیدم و نی‌ رنگ فهمیدم</p></div>
<div class="m2"><p>چو شبنم حیرتی گل کردم و آیینه خندیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گشود از نفی خویشم پردهٔ اثبات بی‌رنگی</p></div>
<div class="m2"><p>پری در جلوه آمد تا شکست شیشه نالیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز موهومی به دل راهی نبردم آه محرومی</p></div>
<div class="m2"><p>شدم عکس و برون خانهٔ آیینه خوابیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تحیر پیشم آمد ای سرشک از یاد دیداری</p></div>
<div class="m2"><p>تو راهی باش من بر جوهر آیینه پیچیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو صبح از برگ ساز بی‌کسی‌هایم چه می‌پرسی</p></div>
<div class="m2"><p>غباری داشتم بر روی زخم خویش پاشیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوشا آیینه داربهای عرض ناز معشوقان</p></div>
<div class="m2"><p>بهارش گل نشان بود و من از خود رنگ پیچیدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درین محفل که خجلت مایه است اسباب پیدایی</p></div>
<div class="m2"><p>چو اشک از چهرهٔ هستی عرق‌واری تراویدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غبارم داشت سطری چند تحریر پریشانی</p></div>
<div class="m2"><p>به مهر گردباد امروز مکتوبش رسانیدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز چندین پیرهن بر قامت موزون عریانی</p></div>
<div class="m2"><p>لباس عافیت چسبان ندیدم چشم پوشیدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا از وهم عقبا سخت می‌ترسانی ای واعظ</p></div>
<div class="m2"><p>به این تمهید اگر مردی برآر از ملک امیدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز فرق و امتیاز و کعبه و دیرم چه می‌پرسی</p></div>
<div class="m2"><p>اسیر عشق بودم هر چه پیش آمد پرستیدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خموشی در فضای دل صفا می‌پرورد بیدل</p></div>
<div class="m2"><p>غباری داشت گفت‌وگو نفس در خویش دزدیدم</p></div></div>