---
title: >-
    غزل شمارهٔ ۱۹۵۹
---
# غزل شمارهٔ ۱۹۵۹

<div class="b" id="bn1"><div class="m1"><p>دست و پا گم کردهٔ شوق تماشای توام</p></div>
<div class="m2"><p>افکند یارب سر افتاده در پای توام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اینکه رنگم می‌پرد هر دم به ناز بیخودی</p></div>
<div class="m2"><p>انجمن پرداز خالی کردن جای توام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خانمان پرداز الفت را چه هستی‌کو عدم</p></div>
<div class="m2"><p>هر کجا مژگان گشایم‌ گرد صحرای توام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچکس آواره گرد وادی همت مباد</p></div>
<div class="m2"><p>مطلب نایاب خویشم بسکه جویای توام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقد موهوم حباب آنگه به بازار محیط</p></div>
<div class="m2"><p>زبن بضاعت آب سازد کاش سودای توام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواه درد آرم به شوخی خواه صاف آیم به جوش</p></div>
<div class="m2"><p>همچو می از قلقل آهنگان مینای توام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کیست‌ گردد مانع مطلق عنانیهای من</p></div>
<div class="m2"><p>موج بی‌پروای توفان خیز دریای توام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سجده‌ها دارم به ناز هستی موهوم خویش</p></div>
<div class="m2"><p>کاین غبار سرمه جوهر گرد مینای توام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در محبت فرق تمییز نیاز و ناز کو</p></div>
<div class="m2"><p>هر قدر مجنون خویشم محو لیلای توام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می‌شکافم پردهٔ هستی تو می‌آیی برون</p></div>
<div class="m2"><p>نقش نامت بسته‌ام یعنی معمای توام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرمی هنگامهٔ موج و محیط امروز نیست</p></div>
<div class="m2"><p>تا تو افشای منی من ساز اخفای توام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می‌شنیدم پیش ازین بیدل نوای قدسیان</p></div>
<div class="m2"><p>این زمان محو کلام حیرت انشای توام</p></div></div>