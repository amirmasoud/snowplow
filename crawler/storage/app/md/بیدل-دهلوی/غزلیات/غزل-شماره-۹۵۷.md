---
title: >-
    غزل شمارهٔ ۹۵۷
---
# غزل شمارهٔ ۹۵۷

<div class="b" id="bn1"><div class="m1"><p>هوس تا چند بر دل تهمت هر خشک و تر بندد</p></div>
<div class="m2"><p>بدزدم در خود آغوشی که بر آفاق دربندد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به این یک رشته زناری که در رهن نفس دارم</p></div>
<div class="m2"><p>گسستن تا به کی چون سبحه صد جایم کمر بندد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به آزادی شوم چون شمع تا ممتاز این محفل</p></div>
<div class="m2"><p>گشایم رشتهٔ پایی که دستارم به سر بندد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هم چشمان خیال امتیازم آب می‌سازد</p></div>
<div class="m2"><p>خدایا قطره‌ام بیرون این دریا گهر بندد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز حاصل قطع خواهش کن که این نخل گلستان را</p></div>
<div class="m2"><p>به طومار نمو مهر است در هرجا ثمر بندد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهان افشاگر راز است بر غفلت متن چندان</p></div>
<div class="m2"><p>که ناهنجاریت در خانهٔ آیینه خر بندد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جنون گل عیانست از گریبان‌چاکی اجزا</p></div>
<div class="m2"><p>که وحشت برکشد از سنگ و خفت بر شرر بندد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهانی در غبار ما و من ماند از عدم غافل</p></div>
<div class="m2"><p>حذر از سیر صحرایی که راه خانه بربندد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به بزم عشق پر بی‌جرأت تمهید زنهارم</p></div>
<div class="m2"><p>مگر اشکی چو مژگان بر سرانگشتم جگر بندد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وفا تا از حلاوت نگسلاند ربط چسبانم</p></div>
<div class="m2"><p>حضور بوریا یارب به پهلویم شکر بندد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز بس وارستگی می‌جوشد از بنیاد من بیدل</p></div>
<div class="m2"><p>پرنگ‌، الفت نگیرد نقش من نقاش گر بندد</p></div></div>