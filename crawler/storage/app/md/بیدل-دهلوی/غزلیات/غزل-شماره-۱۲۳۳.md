---
title: >-
    غزل شمارهٔ ۱۲۳۳
---
# غزل شمارهٔ ۱۲۳۳

<div class="b" id="bn1"><div class="m1"><p>آگاهی دل انجمن اختلاف شد</p></div>
<div class="m2"><p>عکسش فروگرفت چو آیینه صاف شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کام و زبان به سرمه‌اش از خاک پرکند</p></div>
<div class="m2"><p>گویاییی‌که تشنهٔ لاف وگزاف شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر چینی‌ات مناز که خاقان به آن غرور</p></div>
<div class="m2"><p>چندی به سر نیامده مویینه‌باف شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میل غذاست مرکز بنیاد زندگی</p></div>
<div class="m2"><p>پیچید معده بر هوس جوع و ناف شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مستغنی‌ام ز دیر و حرم کرد بیخودی</p></div>
<div class="m2"><p>برگرد خویش گردش رنگم طواف شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آخر به ناله دعوی طاقت نرفت پیش</p></div>
<div class="m2"><p>لب بستنم به عجز دوام اعتراف شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیری‌گره ز رشتهٔ جان سختی‌ام ‌گشود</p></div>
<div class="m2"><p>قد خمیده تیشیهٔ خاراشکاف شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مردان به شرم جوهر غیرت نهفته‌اند</p></div>
<div class="m2"><p>تیغ از حجاب زنگ مقیم غلاف شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فهمیده نِه قدم که‌کمالات راستی</p></div>
<div class="m2"><p>ننگ هزار جاده ز یک انحراف شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با خامشی بساز که خواهد گشاد لب</p></div>
<div class="m2"><p>میدان هم‌کشیدن اهل مصاف شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل به چارسوی برودت رواج دهر</p></div>
<div class="m2"><p>گردکساد، جنس وفا را لحاف شد</p></div></div>