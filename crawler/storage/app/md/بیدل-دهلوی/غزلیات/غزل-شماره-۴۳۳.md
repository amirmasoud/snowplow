---
title: >-
    غزل شمارهٔ ۴۳۳
---
# غزل شمارهٔ ۴۳۳

<div class="b" id="bn1"><div class="m1"><p>صد هنر در پرده دل فرش اقبال صفاست</p></div>
<div class="m2"><p>بیشتر در خانهٔ آیینه جوهر بوریاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سجده تعلیم است عجز نارساییهای شوق</p></div>
<div class="m2"><p>چین‌ کلفت بر جبینم نقش محراب دعاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمع دیدی عبرت از هنگامهٔ آفاق گیر</p></div>
<div class="m2"><p>گرد بال شعله فرسودی فروغ بزمهاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دولت شاهی ندارد بیش از این رنگ ثبات</p></div>
<div class="m2"><p>کز هواپروردگان سایهٔ بال هماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرهم ایجاد است‌گر طبع از درشتی بگذرد</p></div>
<div class="m2"><p>سنگ این‌‌ کهسار چون‌ گردد ملایم مومیاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از هجوم اشک در گرد ستم خوابیده‌ام</p></div>
<div class="m2"><p>جیب ‌و دامانم ز جوش این شهیدان‌ کربلاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناله‌ها در پردهٔ ساز نگه گم کرده‌ایم</p></div>
<div class="m2"><p>مردمک مُهر خموشی بر زبان چشم ماست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از حیا نبود اگر آیینه‌ات پوشد نمد</p></div>
<div class="m2"><p>چشم پوشیدن ‌ز خوب ‌و زشت تشریف حیاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غافلان عافیت را هر قدم مانند شمع</p></div>
<div class="m2"><p>خفته یک پا بر زمین و پای دیگر در هواست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عاقبت نقش دو عالم پاک خواهد کرد عشق</p></div>
<div class="m2"><p>شعله ‌بهر خوردن ‌خاشاک یکسر اشتهاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دهر خلقی را به مرگ اغنیا می‌پرورد</p></div>
<div class="m2"><p>یک نهنگ مرده اینجا بهر صد ماهی غذاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نغمهٔ ما در غبار عجز توفان می‌کند</p></div>
<div class="m2"><p>موجها را در شکست خویش تحریر صداست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قامت پیری ز حرصت شد کمینگاه امل</p></div>
<div class="m2"><p>ورنه خم‌گردیدنت بر هر دو عالم پشت پاست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شیوهٔ خوبان عجب نازک ادا افتاده است</p></div>
<div class="m2"><p>شوخی آنجا تا عرق‌آلود می‌ گردد حیاست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شانه‌ها چون ‌صبح بیدل یک جهان خمیازه‌اند</p></div>
<div class="m2"><p>با دل چاک ‌که امشب طرهٔ او آشناست</p></div></div>