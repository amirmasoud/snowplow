---
title: >-
    غزل شمارهٔ ۲۶۳۹
---
# غزل شمارهٔ ۲۶۳۹

<div class="b" id="bn1"><div class="m1"><p>کیسه پرداز خیال شادی و غم رفته‌ای</p></div>
<div class="m2"><p>چون نفس چندانکه می‌آیی فراهم رفته‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیدماغی رخصت آگاهی خویشت نداد</p></div>
<div class="m2"><p>کز چه محفل آمدی و از چه عالم رفته‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواه گردون جلوه‌گر شو خواه دریا موج زن</p></div>
<div class="m2"><p>هر چه باشی تا نهادی چشم برهم رفته‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با همه لاف من و ما رو نهفتی در کفن</p></div>
<div class="m2"><p>دعویت بی‌پرده شد آخر که ملزم رفته‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای خیال آواره اکنون جای آرامت کجاست</p></div>
<div class="m2"><p>از بهشت آخر تو هم با صلب آدم رفته‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عیش و غم آن به‌ که از تمییز آن‌کس بگذرد</p></div>
<div class="m2"><p>تا بهشت آمد به یادت در جهنم رفته‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آمدن فهم نشان تیر آفت بودن است</p></div>
<div class="m2"><p>گر بدانی رفته‌ای در حصن محکم رفته‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هیچکس ‌در عرصهٔ وحشت گرو تاز تو نیست</p></div>
<div class="m2"><p>تا عدم از عالم هستی به یکدم رفته‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سعی جولان تو یک سیر گریبان بود و بس</p></div>
<div class="m2"><p>چون خط پرگار هر جا رفته‌ای خم رفته‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دوستان محمل به دوش اتفاق عبرتند</p></div>
<div class="m2"><p>پیش و پس چون ‌دست بر هم سود‌ه با هم رفته‌ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قطع راه زندگی بیدل نمی‌خواهد تلاش</p></div>
<div class="m2"><p>بی‌قدم زین انجمن چون شمع‌ کم‌کم رفته‌ای</p></div></div>