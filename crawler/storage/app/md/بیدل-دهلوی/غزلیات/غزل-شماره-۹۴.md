---
title: >-
    غزل شمارهٔ ۹۴
---
# غزل شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>جوش زخمم دادسر در صبح محشرتیغ را</p></div>
<div class="m2"><p>کرد خون‌گرم من بال سمندرتیغ را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از گزیدنهای رشک ابروی چین‌پرورت</p></div>
<div class="m2"><p>بر زبان پیداست دندانهای جوهر تیغ را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسمل نازتو چون مشق تپیدن می‌کند</p></div>
<div class="m2"><p>می‌کشد چون مدّ بسم‌الله بر سرتیغ را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جمع با زینت نگردد جوهر مردانگی</p></div>
<div class="m2"><p>از برش عاری بود گر سازی از زرتیغ را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زینت هرکس به قدر اقتضای وضع اوست</p></div>
<div class="m2"><p>قبضه داند بر سر خود به ز افسر تیغ را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرخوش‌تسلیم ازتهدید دوران‌ایمن است</p></div>
<div class="m2"><p>کس نراند برسر بسمل مکررتیغ را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در هجوم عاجزی آفت گوارا می‌شود</p></div>
<div class="m2"><p>می‌شمارد مرغ بی‌پرواز شهر تیغ را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کوه اندوهیم از سنگینی پای طلب</p></div>
<div class="m2"><p>نالهٔ خوابیده می‌دانیم بر سر تیغ را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طبع سرکش ناکجا تقلید همواری‌کند</p></div>
<div class="m2"><p>سخت‌دشوار است دادن آب‌گوهر تیغ را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از هنر آیینهٔ مقدار هرکس روشن است</p></div>
<div class="m2"><p>رشتهٔ شمع‌است بیدل موج جوهرتیغ را</p></div></div>