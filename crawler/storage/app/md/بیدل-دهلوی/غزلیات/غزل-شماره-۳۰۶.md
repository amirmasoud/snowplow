---
title: >-
    غزل شمارهٔ ۳۰۶
---
# غزل شمارهٔ ۳۰۶

<div class="b" id="bn1"><div class="m1"><p>چو سایه چند به هر خاک جبهه سودن‌ها</p></div>
<div class="m2"><p>که زنگ بخت نگردد کم از زدودن‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غبار غفلت و روشندلی نگردد جمع</p></div>
<div class="m2"><p>کجاست دیدهٔ آیینه را غنودن‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز امتحان محبت در آتشیم همه</p></div>
<div class="m2"><p>چو عود سوختن ماست آزمودن‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دمی که جلوه ادا فهم مدعا باشد</p></div>
<div class="m2"><p>گشودن مژه هم مفت لب گشودن‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مخواه زآینهٔ حسن رفع جوهر خط</p></div>
<div class="m2"><p>که بیش می‌شود این زنگ از زدودن‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر آبرو بود از حادثات کاهش نیست</p></div>
<div class="m2"><p>زیان نمی‌رسد الماس را ز سودن‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کجاست عشرت اندوختن به راحت ترک</p></div>
<div class="m2"><p>مجو چو کاشتن آسانی از درودن‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مباش هرزه‌نوای بساط کج‌فهمان</p></div>
<div class="m2"><p>که ترسم آفت نفرین کشد ستودن‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تغافل از بد و نیک اعتبار اهل حیاست</p></div>
<div class="m2"><p>که سرخ‌رویی چشم آورد غنودن‌ها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نی‌ام چو ماه نو از آفت کمال ایمن</p></div>
<div class="m2"><p>همان به کاستنم می‌برد فزودن‌ها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فریب فرصت هستی مخور که همچو شرار</p></div>
<div class="m2"><p>نهفتنی‌ست اگر هست وانمودن‌ها</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>درین محیط که نقد فسوس گوهر اوست</p></div>
<div class="m2"><p>کفی پُرآبله کن چون صدف ز سودن‌ها</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سراغ جیب سلامت نمی‌توان دریافت</p></div>
<div class="m2"><p>مگر ز کسوت بی‌رنگ هیچ بودن‌ها</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گره‌گشای سخنور سخن بود بیدل</p></div>
<div class="m2"><p>به ناخنی نفتد کار لب گشودن‌ها</p></div></div>