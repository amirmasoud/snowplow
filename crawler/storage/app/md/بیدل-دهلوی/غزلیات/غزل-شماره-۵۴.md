---
title: >-
    غزل شمارهٔ ۵۴
---
# غزل شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>نقاب عارض گلجوش کرده‌ای ما را</p></div>
<div class="m2"><p>تو جلوه داری و روپوش‌کرده‌ای ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز خود تهی‌شدگان‌گر نه از تو لبریزند</p></div>
<div class="m2"><p>دگر برای چه آغوش‌کرده‌ای ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خراب میکدهٔ عالم خیال توایم</p></div>
<div class="m2"><p>چه مشربی‌که قدح نوش‌کرده‌ای ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمود ذره طلسم حضور خورشید است</p></div>
<div class="m2"><p>که‌گفته است فراموش کرده‌ای ما را؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز طبع قطره نمی جزمحیط نتوان‌یافت</p></div>
<div class="m2"><p>تو می‌تراوی اگر جوش‌کرده‌ای ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به رنگ آتش یاقوت ما و خاموشی</p></div>
<div class="m2"><p>که حکم خون شو و مخروش‌کرده‌ای ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر به ناله نیرزیم‌، رخصت آهی</p></div>
<div class="m2"><p>نه‌ایم شعله که خاموش‌کرده‌ای ما را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه بارکلفتی‌ای زندگی‌که همچو حباب</p></div>
<div class="m2"><p>تمام آبله بر دوش‌کرده‌ای ما را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چوچشم چشمهٔ خورشید حیرتی داریم</p></div>
<div class="m2"><p>تو ای مژه ز چه خس‌پوش‌کرده‌ای ما را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نوای پردهٔ خاکیم یک قلم بیدل</p></div>
<div class="m2"><p>کجاست عبرت اگرگوش‌کرده‌ای ما را</p></div></div>