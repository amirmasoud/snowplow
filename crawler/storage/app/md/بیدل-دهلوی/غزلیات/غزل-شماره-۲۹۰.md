---
title: >-
    غزل شمارهٔ ۲۹۰
---
# غزل شمارهٔ ۲۹۰

<div class="b" id="bn1"><div class="m1"><p>بر قماش پوچ هستی تا به‌کی وسواس‌ها</p></div>
<div class="m2"><p>پنبه‌ها خواهد دمید آخر ازین کرباس‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیشهٔ ساعت خبر ز ساز فرصت می‌دهد</p></div>
<div class="m2"><p>خودسران غافل مباشید از صدای طاس‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عبرت آنجا کز مکافات عمل گیرد عیار</p></div>
<div class="m2"><p>ناخنی دارند در جنگ درودن داس‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اهل دنیا را به نهضت‌گاه آزادی چه‌کار</p></div>
<div class="m2"><p>در مزابل فارغند از بوی گل کناس‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عالمی بالیده است از دستگاه خودسری</p></div>
<div class="m2"><p>نشتری می‌خواهد این جمعیت آماس‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا بود ممکن به وضع خلق باید ساختن</p></div>
<div class="m2"><p>آدمیت پیش نتوان برد با نسناس‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حیرت دیدار با دنیا و عقبا شد طرف</p></div>
<div class="m2"><p>بوی امیدی گوارا کرد چندین یاس‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی‌نوایی چون به سامان جنون پوشیده نیست</p></div>
<div class="m2"><p>صبح خندد بر گریبان‌چاکی افلاس‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شرم می‌دارد درشتی از ملایم‌طینتان</p></div>
<div class="m2"><p>غالب افتاده‌ست بیدل سرب بر الماس‌ها</p></div></div>