---
title: >-
    غزل شمارهٔ ۶۷۲
---
# غزل شمارهٔ ۶۷۲

<div class="b" id="bn1"><div class="m1"><p>پیوستگی به حق‌، ز دو عالم بریدنست</p></div>
<div class="m2"><p>دیدار دوست هستی خود را ندیدنست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آزادگی کزوست مباهات عافیت</p></div>
<div class="m2"><p>دل را زحکم حرص وهوا واخریدنست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرواز سایه جز به سر بام مهر نیست</p></div>
<div class="m2"><p>از خود رمیدن تو، به حق آرمیدنست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون موج‌کوشش نفس ما درتن محیط</p></div>
<div class="m2"><p>رخت شکست خویش به ساحل‌کشیدنست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پامال غارت نفس سرد یأس نیست</p></div>
<div class="m2"><p>صبح مراد ما که‌گلش نادمیدنست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر هرچه دیده واکنی از خویش رفته‌گیر</p></div>
<div class="m2"><p>افسانه‌وار دیدن عالم شنیدنست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا حرص آب و دانه به دامت نیفکند</p></div>
<div class="m2"><p>عنقا صفت به قاف قناعت خزیدنست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر بوالهوس به بزم خموشان نفش‌کشد</p></div>
<div class="m2"><p>همچون خروس بی‌محلش سر بریدنست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>امشب ز بس‌که هرزه زبانست شمع آه</p></div>
<div class="m2"><p>کارم چوگاز تا به سحر لب‌گزیدنست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آرام در طریقت ما نیست غیرمرگ</p></div>
<div class="m2"><p>هنگامه گرم‌ساز نفسها تپیدنست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ما را به رنگ شمع درعافیت زدن</p></div>
<div class="m2"><p>از چشم خود همین دو سه اشکی چکیدنست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سعی قدم‌کجا وطریق فناکجا</p></div>
<div class="m2"><p>بیدل به خنجرنفس این ره بریدنست</p></div></div>