---
title: >-
    غزل شمارهٔ ۵۲۰
---
# غزل شمارهٔ ۵۲۰

<div class="b" id="bn1"><div class="m1"><p>صبح این بادیه آشوب تپشهای دل است</p></div>
<div class="m2"><p>شام‌گردی ز جنون‌تازی سودای دل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجمر اینجا همه‌ گوش است بر آواز سپند</p></div>
<div class="m2"><p>آسمان خانهٔ زنبور ز غوغای دل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گه تپشگاه فغان‌، گاه جنون می‌خندد</p></div>
<div class="m2"><p>برق ‌تازی که در آیینهٔ اخفای دل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست حرفی‌ که ازین نقطه نیاید بیرون</p></div>
<div class="m2"><p>شور ساز دو جهان اسم معمای دل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه همین اشک به توفان تپش می‌غلتد</p></div>
<div class="m2"><p>داغ هم زورق توفانی دریای دل است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شیشه بی‌خون جگر کی‌ گذرد از سر جام</p></div>
<div class="m2"><p>چشم حیرت‌زده‌ام آبلهٔ پای دل است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حسن بی‌پرده و من سر به‌ گریبان خیال</p></div>
<div class="m2"><p>اینکه منع نگهم می‌کند ایمای دل است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نوبهاری عجب از وهم خزن باخته‌ام</p></div>
<div class="m2"><p>غم امروز من اندیشهٔ فردای دل است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ظرف و مظروف خیال آینهٔ یکدگرند</p></div>
<div class="m2"><p>هرکجا از تو تهی نیست همان جای دل است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست جز بیخری راحلهٔ ریگ روان</p></div>
<div class="m2"><p>رفتن از دست به ذوق طلبت پای دل است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کس به تسخیر نفس صرفهٔ تدبیر ندید</p></div>
<div class="m2"><p>به هوس دام مچین وحشی صحرای دل است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل احیای معانی به خموشی کردم</p></div>
<div class="m2"><p>نفس سوخته اعجاز مسیحای دل است</p></div></div>