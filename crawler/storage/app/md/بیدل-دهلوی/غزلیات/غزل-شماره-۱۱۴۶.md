---
title: >-
    غزل شمارهٔ ۱۱۴۶
---
# غزل شمارهٔ ۱۱۴۶

<div class="b" id="bn1"><div class="m1"><p>مباش غره به سامان این بنا که نریزد</p></div>
<div class="m2"><p>جهان طلسم غبارست ازکجا که نریزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکش ز جرات اظهار شرم تهمت شوخی</p></div>
<div class="m2"><p>عرق دمی شود آیینهٔ حیا که نریزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به جدگرفتن تدبیر انتقام چه لازم</p></div>
<div class="m2"><p>همانقدر دم تیغت تنک‌نما که نریزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قدح به خاک زدیم از تلاش صحبت دونان</p></div>
<div class="m2"><p>نداشت آن همه موج آبروی ما که نریزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به‌گوش منتظران ترانهٔ غم عشقت</p></div>
<div class="m2"><p>فسانهٔ شبخون دارد آن صدا که نریزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل ستمکش بیحاصلی چو آبله دارم</p></div>
<div class="m2"><p>کسی‌کجا برد این دانه زیر پا که نریزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به باد رفتم و بر طبع‌کس نخورد غبارم</p></div>
<div class="m2"><p>دگر چه سحرکند خاک بی‌عصا که نریزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نثار راه تو دیدم چکیدن آینه اشکی</p></div>
<div class="m2"><p>گرفتم از مژه‌اش برکف دعا که نریزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خمید پیکرم از انتظار و جان به لب آمد</p></div>
<div class="m2"><p>قدح به یاد توکج کرده‌ام بیا که نریزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به این حنا که‌ گرفته است خون خلق به ‌گردن</p></div>
<div class="m2"><p>اگر تو دست فشانی چه رنگها که نریزد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غم مروت قاتل‌گداخت پیکر بیدل</p></div>
<div class="m2"><p>مباد خون ‌کس ارزد به این بها که نریزد</p></div></div>