---
title: >-
    غزل شمارهٔ ۲۱۸۷
---
# غزل شمارهٔ ۲۱۸۷

<div class="b" id="bn1"><div class="m1"><p>ز بال نارسا بر خویش پیچیده است پروازم</p></div>
<div class="m2"><p>لب خاموش دایم در قفس دارد چو آوازم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو تمثالم نهان از دیده‌های اعتبار اما</p></div>
<div class="m2"><p>همان آیینهٔ بی اعتباربهاست غمازم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نفس‌ گر می‌کشم قانون حالم می‌خورد بر هم</p></div>
<div class="m2"><p>چو ساز خامشی با هیچ آهنگی نمی‌سازم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خیالی می‌کشد مخمل‌ کدامین راه و کو منزل</p></div>
<div class="m2"><p>سوار حیرتم در عرصهٔ آیینه می‌تازم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درین گلشن‌ که سامان من و ما باختن دارد</p></div>
<div class="m2"><p>چو گل سرمایه‌ای دیگر ندارم رنگ می‌بازم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز شمع‌ کشته داغی هم اگر یابی غنیمت دان</p></div>
<div class="m2"><p>نگاه حیرت انجامم تماشا داشت آغازم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ندارد ذرهٔ موهوم بی‌خورشید رسوایی</p></div>
<div class="m2"><p>تو کردی جلوه و افتاد بر رو تختهٔ رازم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شدم خاک و فرو ننشست توفان غبار من</p></div>
<div class="m2"><p>هنوز از پردهٔ ساز عدم می‌جوشد آوازم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز درد سعی ناپیدای تصویرم چه می‌پرسی</p></div>
<div class="m2"><p>سرا پا رنگم اما سخت بیرنگ است پروازم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بنازم خرمی های بهارستان غفلت را</p></div>
<div class="m2"><p>شکستن فتنه توفانست و من بر رنگ می‌نازم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به رنگ چشم مشتاقان ز حیرت بر نمی‌آیم</p></div>
<div class="m2"><p>همان یک عقده دارم تا قیامت‌ گر کنی بازم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ند‌انم عذر این غفلت چه خواهم خواستن بیدل</p></div>
<div class="m2"><p>که حسنش خصم تمثالست و من آیینه پردازم</p></div></div>