---
title: >-
    غزل شمارهٔ ۱۹۹۷
---
# غزل شمارهٔ ۱۹۹۷

<div class="b" id="bn1"><div class="m1"><p>مرده‌ام اما همان خجلت طراز هستی‌ام</p></div>
<div class="m2"><p>با عرق چون شمع می‌جوشد گداز هستی‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رنگ این پرواز حیرانم‌ کجا خواهد شکست</p></div>
<div class="m2"><p>چون نفس عمری‌ست ‌گرد ترکتاز هستی‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کاش چشمم وانمی‌گردید از خواب عدم</p></div>
<div class="m2"><p>منفعل شد نیستی از امتیاز هستی‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حاصل چندین امل چشمی بهم آوردن است</p></div>
<div class="m2"><p>بگذر از افسانهٔ دور و دراز هستی‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر هوا چند افکنم سجادهٔ ناز غبار</p></div>
<div class="m2"><p>سجده‌ای می‌خواهد ارکان نماز هستی‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقش من چون اشک شوخی ‌کرد و از خجلت‌ گداخت</p></div>
<div class="m2"><p>کاش هم در پرده خون می‌گشت راز هستی‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون حبابم یک نفس پرواز و آن هم در قفس</p></div>
<div class="m2"><p>ای ز من غافل چه می‌پرسی ز ساز هستی‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صبح پیری می‌دمد ای شمع ما و من خموش</p></div>
<div class="m2"><p>جز نفس مشکل که گیرد شاهباز هستی‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشمکم را چون شرر دنبالهٔ تکرار نیست</p></div>
<div class="m2"><p>پر تغافل پیشه است ابروی ناز هستی‌ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سرنگونیهای خجلت تحفهٔ بیحاصلی‌ست</p></div>
<div class="m2"><p>کیست غیر از یأس بیند بر نیاز هستی‌ام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل از منصوبهٔ عنقایی‌ام غافل مباش</p></div>
<div class="m2"><p>نقد اظهاری ندارم پاکباز هستی‌ام</p></div></div>