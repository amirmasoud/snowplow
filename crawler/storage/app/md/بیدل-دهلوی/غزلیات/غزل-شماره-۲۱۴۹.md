---
title: >-
    غزل شمارهٔ ۲۱۴۹
---
# غزل شمارهٔ ۲۱۴۹

<div class="b" id="bn1"><div class="m1"><p>عروج همتی در کار دارم</p></div>
<div class="m2"><p>همه گر سایه‌ام دیوار دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غبارم آشیان حسرت اوست</p></div>
<div class="m2"><p>چمن درگوشهٔ دستار دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نفس بیتابی دل می‌شمارد</p></div>
<div class="m2"><p>هجوم سبحه در زنار دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگاهی تا به مژگان می‌رسانم</p></div>
<div class="m2"><p>ز خود رفتن همین مقدار دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مپرس از انفعال ساز غفلت</p></div>
<div class="m2"><p>ز هستی آنچه دارم عار دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو شمعم چاره غیر سوختن نیست</p></div>
<div class="m2"><p>به سر آتش‌، ته پا خار دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به خود می‌لرزم از تمهید آرام</p></div>
<div class="m2"><p>چوگردون سقف بی دیوار دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تظلم قابل فریادرس نیست</p></div>
<div class="m2"><p>طنین پشه در کهسار دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ازین یک مشت خاک باد برده</p></div>
<div class="m2"><p>به دوش هر دو عالم بار دارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دگر ای نامه پهلویم مگردان</p></div>
<div class="m2"><p>که پهلوی دل بیمار دارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به حیرت می‌روم آیینه بر دوش</p></div>
<div class="m2"><p>سفارش نامهٔ دیدار دارم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به چشمم توتیا مفروش بیدل</p></div>
<div class="m2"><p>که من با خاک پایی کار دارم</p></div></div>