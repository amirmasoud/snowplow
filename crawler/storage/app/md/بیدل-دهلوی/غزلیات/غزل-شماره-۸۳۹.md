---
title: >-
    غزل شمارهٔ ۸۳۹
---
# غزل شمارهٔ ۸۳۹

<div class="b" id="bn1"><div class="m1"><p>چنین‌که عمر تأملگر شتاب‌گذشت</p></div>
<div class="m2"><p>هوای آبله‌ای از سر حباب گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به چشم‌بند جهان این چه سحرپردازی‌ست</p></div>
<div class="m2"><p>که بی‌حجابی آن جلوه از نقاب‌گذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هر طرف نگرم دود دل پرافشان است</p></div>
<div class="m2"><p>کدام سوخته زین وادی خراب گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جنون‌پرستی اغراض ننگ طبع مباد</p></div>
<div class="m2"><p>حیا نماند چو انصاف از حساب‌گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی به چارهٔ تسکین ما چه پردازد</p></div>
<div class="m2"><p>که تا به داغ رسیدیم ماهتاب‌گذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز مصرع نفس واپسین عیان‌گردید</p></div>
<div class="m2"><p>که ما ز هر چه‌گذشتیم انتخاب‌گذشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیاهکار فضولی مخواه موی سفید</p></div>
<div class="m2"><p>کفن چوپرده د‌رد باید از خضاب‌گذشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صفا کدورت زنگار چشم نزداید</p></div>
<div class="m2"><p>ز سایه کس نتواند در آفتاب گذشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز خود تهی شو و از ورطهٔ خیال برآی</p></div>
<div class="m2"><p>به آن‌کنار همین‌کشتی ز سراب‌گذشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به عیش غفلت عمری‌که نیست‌کس نرسد</p></div>
<div class="m2"><p>فغان‌که فرصت تعبیر هم به خواب‌گذشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز سوز سینه‌ام آگه‌که‌کرد محفل را</p></div>
<div class="m2"><p>که اشک دود شد و از سرکباب‌گذشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ندانم از چه غرض بال فرصت افشاندم</p></div>
<div class="m2"><p>شرر بیانی‌ام از حاصل جواب گذشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به وادیی‌که نفس بود رهبربیدل</p></div>
<div class="m2"><p>همین تأمل رفتن‌گران رکاب‌گذشت</p></div></div>