---
title: >-
    غزل شمارهٔ ۸۸۰
---
# غزل شمارهٔ ۸۸۰

<div class="b" id="bn1"><div class="m1"><p>کار به نقش پا رساند جهد سر هواییت</p></div>
<div class="m2"><p>شمع صفت به داغ برد آینه خودنماییت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل به غبار وهم و وظن رفت زشغل ما و من</p></div>
<div class="m2"><p>آینه ‌ها به باد داد زنگ نفس‌ زداییت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فقر نداشت این‌قدر رنج خیال پا و سر</p></div>
<div class="m2"><p>خانهٔ کفشدوز کرد فکر برهنه‌پاییت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آینه‌داری خیال شخص تو را مثال‌ کرد</p></div>
<div class="m2"><p>خاک‌چه‌ره‌به‌سر فشاند خاک‌به‌سر جداییت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیأت چرخ دیده‌ای محرم احتیاج باش</p></div>
<div class="m2"><p>کاسه بلند چیده است دستگه گداییت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از نفس هواپرست رنگ غنای دل شکست</p></div>
<div class="m2"><p>بر سر آشیان فتاد آفت پرگشاییت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گربه فلک روی‌که نیست بند هواگسیختن</p></div>
<div class="m2"><p>همچو سحر گرفته‌اند در قفس رهاییت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دامن خود به دست‌ گیر شکر حقوق عجز کن</p></div>
<div class="m2"><p>قاصد رمز مدعاست خجلت نارساییت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سجده فسون قدرت است پایهٔ همت بلند</p></div>
<div class="m2"><p>ربط زمین و آسمان داده به هم دوتاییت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خشک و تر بهار رنگ سر به ره امید ماند</p></div>
<div class="m2"><p>لیک به فرق ‌گل فکند سایه ‌کف حناییت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چشم تأمل حباب‌، تا کف و موج وارسید</p></div>
<div class="m2"><p>با همه‌ام دچارکرد یک نگه آشناییت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل اگر نه شرم عشق‌، لب ‌گزد از جنون‌ تو</p></div>
<div class="m2"><p>تا به سپهر می رسد چاک سحر قباییت</p></div></div>