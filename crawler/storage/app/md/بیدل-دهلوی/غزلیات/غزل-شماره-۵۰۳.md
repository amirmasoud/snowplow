---
title: >-
    غزل شمارهٔ ۵۰۳
---
# غزل شمارهٔ ۵۰۳

<div class="b" id="bn1"><div class="m1"><p>حذر ز راه محبت که پر خطرناک است</p></div>
<div class="m2"><p>تو مشت خار ضعیفی و شعله بی‌باک است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>توان به بی‌کسی ایمن شد از مضرت دهر</p></div>
<div class="m2"><p>سموم حادثه را بخت تیره تریاک است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به اختیار نرفتیم هرکجا رفتیم</p></div>
<div class="m2"><p>غبار ما و نفس‌، حکم صید و فتراک است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بس زمانه هجوم کساد بازاری‌ست</p></div>
<div class="m2"><p>چو اشک گوهر ما وقف دامن خاک است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چگونه کم شود از ما ملامت زاهد</p></div>
<div class="m2"><p>که صد زبان درازش به چوب مسواک است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازین محیط که در بی‌نمی‌ست توفانش</p></div>
<div class="m2"><p>کسی که آب رخی برد گوهرش پاک است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غبار حادثه حصنی است ناتوانان را</p></div>
<div class="m2"><p>کمند موج خطر ناخدای خاشاک است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز خویش رفتن ما رهبری نمی‌خواهد</p></div>
<div class="m2"><p>دلیل قافلهٔ صبح سینهٔ چاک است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیامده‌ست شرابی به عرض شوخی رنگ</p></div>
<div class="m2"><p>جهان هنوز سیه‌مست سایهٔ تاک است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه وانمایمت از چشم‌بند عالم وهم</p></div>
<div class="m2"><p>که خودنمایی آیینه در دل خاک است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زمانه کج‌منشان را به برکشد بیدل</p></div>
<div class="m2"><p>کسی که راست بود خار چشم افلاک است</p></div></div>