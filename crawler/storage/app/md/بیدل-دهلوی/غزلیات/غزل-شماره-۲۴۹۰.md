---
title: >-
    غزل شمارهٔ ۲۴۹۰
---
# غزل شمارهٔ ۲۴۹۰

<div class="b" id="bn1"><div class="m1"><p>هوس ها می‌دمد زین باغ جوش گل تماشا کن</p></div>
<div class="m2"><p>امل آشفته است آرایش سنبل تماشا کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تعلقهاست یکسر حلقهٔ زنجیر سودایت</p></div>
<div class="m2"><p>دو روزی گر هوس دیوانه‌ای غلغل تماشا کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر آگاهی ز زخم دل مباش از ناله هم غافل</p></div>
<div class="m2"><p>به عرض خندهٔ گل شیون بلبل تماشا کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سواد نسخهٔ تحقیق اگر چشمت کند روشن</p></div>
<div class="m2"><p>ز هر جزو محقر انتخاب گل تماشا کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جیب هر بن مو جلوهٔ خاصی‌ست خوبی را</p></div>
<div class="m2"><p>اگر چشم است وگر رخسار وگر کاکل تماشا کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بال و پر چه حاصل گر ندیدی عرض پروازی</p></div>
<div class="m2"><p>در آب و رنگ این‌گلزار بوی گل تماشا کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تپیدنهای دل صد رنگ شور بیخودی دارد</p></div>
<div class="m2"><p>دهان شیشه‌ای واکرده‌ای قلقل تماشا کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کهن شد سیر گل در عالم نیرنگ خودداری</p></div>
<div class="m2"><p>کنون از خود برآ، آشفتن سنبل تماشاکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه حسرت‌ها که دارد نردبان قامت پیری</p></div>
<div class="m2"><p>عروج موج سیلاب از سر این پل تماشا کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به هشیاری ندارد هیچکس آسودگی بیدل</p></div>
<div class="m2"><p>دمی بیخود شو و کیفیت این مل تماشا کن</p></div></div>