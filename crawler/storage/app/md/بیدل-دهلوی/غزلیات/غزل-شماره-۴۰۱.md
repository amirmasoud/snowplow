---
title: >-
    غزل شمارهٔ ۴۰۱
---
# غزل شمارهٔ ۴۰۱

<div class="b" id="bn1"><div class="m1"><p>گر همه در سنگ بود آتش جدایی دید و سوخت</p></div>
<div class="m2"><p>وقت آن‌کس خوش که از مرکز جدا گردید و سوخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دی من و دلدار ربط آب وگوهر داشتیم</p></div>
<div class="m2"><p>این زمان باید ز قاصد نام او پرسید و سوخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک عاشق جامهٔ احرام صد دردسر است</p></div>
<div class="m2"><p>برهمن زین داغ صندل برجبین مالید و سوخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ازتب و تاب سپند این بساط آگه نی‌ام</p></div>
<div class="m2"><p>اینقدر دانم که در یاد کسی نالید و سوخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حلقهٔ صحبت دماغ شعلهٔ جواله داشت</p></div>
<div class="m2"><p>تا به خود پیچید تامل رنگ‌گردانید و سوخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوزخی نقد است وضع خودسری هشیار باش</p></div>
<div class="m2"><p>شمع اینجا یک رگ گردن به خود بالید و سوخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>انفعال عالم بیحاصلی برق است و بس</p></div>
<div class="m2"><p>چون نفس خلقی دکان سعی بیجا چید و سوخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شبنم از خورشید تابان صرفه نتوانست برد</p></div>
<div class="m2"><p>عالمی آیینه با رویت مقابل دید و سوخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وصف لعلت از سخن پرداخت افکار مرا</p></div>
<div class="m2"><p>بال موجی داشتم در گوهر آرامید و سوخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برده بودم تا سر مژگان نگاه حسرتی</p></div>
<div class="m2"><p>یاد خویت‌ کرد جرأت آتش اندیشید و سوخت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نخل من زین باغ حرمان نوبر رنگی نکرد</p></div>
<div class="m2"><p>چون چنار آخر کف دستی به هم سایید و سوخت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اینقدر کز گرم و سرد دهر داغ عبرتم</p></div>
<div class="m2"><p>شعله را باید به حالم تا ابد لرزید وسوخت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دوستان آخر هوای باغ امکانم نساخت</p></div>
<div class="m2"><p>همچو داغ لاله دربرگ ‌گلم پیچید و سوخت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از جنون جولانی تحقیق این بیدل مپرس</p></div>
<div class="m2"><p>شعلهٔ جواله‌ای بر گرد خود گردید و سوخت</p></div></div>