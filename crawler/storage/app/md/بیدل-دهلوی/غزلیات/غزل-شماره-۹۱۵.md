---
title: >-
    غزل شمارهٔ ۹۱۵
---
# غزل شمارهٔ ۹۱۵

<div class="b" id="bn1"><div class="m1"><p>تا عرق‌،‌گلبرگ حسنت یک دوشبنم آب داد</p></div>
<div class="m2"><p>خانهٔ خورشید رخت ناز بر سیلاب داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کس به ضبط دل چه پردازد که عرض جلوه ات</p></div>
<div class="m2"><p>حیرت آیینه را هم جوهر سیماب داد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در محبت غافل از آداب نتوان زبستن</p></div>
<div class="m2"><p>حسن‌ گوش حلقه‌های زلف را هم تاب داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نرگس مست بتان را وانکرد از خواب ناز</p></div>
<div class="m2"><p>آنکه عاشق را چو شبنم دیده بیخواب داد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرزه جولان بود سعی جستجوهای امید</p></div>
<div class="m2"><p>یاس گل‌کرد و سراغ مطلب نایاب داد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌تپد خلقی به خون از یاد استغنای ناز</p></div>
<div class="m2"><p>بیش ازین نتوان دم تیغ تغافل آب داد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواب امنی در جهان بی‌تمیزی داشتم</p></div>
<div class="m2"><p>چشم واکردن سرم در عالم اسباب داد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>داشت غافل سرکشیهای شباب از طاعتم</p></div>
<div class="m2"><p>قامت خم‌گشته یاد ازگوشهٔ محراب داد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اضطراب‌شعله عرض مسند خاکستر است</p></div>
<div class="m2"><p>هرکه رفت ازخویش عبرت بر من بیتاب داد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>استقامت در مزاج عافیت خون کرده‌ام</p></div>
<div class="m2"><p>رشتهٔ امید من نگسسته نتوان تاب داد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بی‌طراوت بود بیدل‌ کوچه‌باغ انتظار</p></div>
<div class="m2"><p>گریهٔ نومیدی آخر چشم ما را آب داد</p></div></div>