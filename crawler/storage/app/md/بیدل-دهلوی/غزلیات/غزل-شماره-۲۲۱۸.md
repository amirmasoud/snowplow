---
title: >-
    غزل شمارهٔ ۲۲۱۸
---
# غزل شمارهٔ ۲۲۱۸

<div class="b" id="bn1"><div class="m1"><p>گهی در شعله می‌غلتم گهی با آب می‌جوشم</p></div>
<div class="m2"><p>وطن آوارهٔ شوقم نگاه خانه بر دوشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درپن محفل امید و یأس هر یک نشئه‌ای دارد</p></div>
<div class="m2"><p>خوشم کز درد بی‌کیفیتی کردند مدهوشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سراغم کرده‌ای آمادهٔ ساز تحیر باش</p></div>
<div class="m2"><p>غبار گردش رنگم دلیل غارت هوشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه سازد گر به حیرانی نپردازد حباب من</p></div>
<div class="m2"><p>ز بس عریانم از خودکسوت آیینه می‌پوشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به رنگی ناتوانم در خیال سرمه‌گون چشمی</p></div>
<div class="m2"><p>که چون تار نظر آواز نتوان بست بر دوشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندارد ساز هستی غیر آهنگ گرفتاری</p></div>
<div class="m2"><p>ز تحریک نفسها شور زنجیر است درگوشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به آن نامهربان‌، یارب که خواهد گفت حال من</p></div>
<div class="m2"><p>ز یادش رفته‌ام چندانکه از هر دل فراموشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خمستان وفا رنگ فسردن بر نمی‌دارد</p></div>
<div class="m2"><p>جنون شوق او دارم مباد از خود برون جوشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز خوبان سود نتوان برد بی سرمایهٔ حیرت</p></div>
<div class="m2"><p>خریداری ندارد دل مگر آیینه بفروشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز گل تا غنچه هر یک ظرف استعداد خود ‌دارد</p></div>
<div class="m2"><p>درین ‌گلشن بقدر جلوهٔ خود من هم آغوشم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نفس عمری تپید و مدعای دل نشد روشن</p></div>
<div class="m2"><p>چراغی داشتم بی مطلبیها کرد خاموشم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به ‌کنج عالم نسیان دل‌ گمگشته‌ام بیدل</p></div>
<div class="m2"><p>ز یادم نیست غافل هرکه می‌سازد فراموشم</p></div></div>