---
title: >-
    غزل شمارهٔ ۹۷۰
---
# غزل شمارهٔ ۹۷۰

<div class="b" id="bn1"><div class="m1"><p>نه فخر می‌دمد اینجا نه ننگ می‌بارد</p></div>
<div class="m2"><p>بر این نشان‌که تو داری خدنگ می‌بارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فریب ابر کرم خورده‌ای از این غافل</p></div>
<div class="m2"><p>که قطره قطره همان چشم تنگ می‌بارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دگر چه چاره به جز خامشی که همچو حباب</p></div>
<div class="m2"><p>بر آبگینهٔ ما آه سنگ می‌بارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وداع فرصت برق و شرار خرمن کن</p></div>
<div class="m2"><p>به مزرعی که شتاب از درنگ می‌بارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهار این چمن از بسکه وحشت‌اندودست</p></div>
<div class="m2"><p>ز داغ لاله جنون پلنگ می‌بارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به پرسش دل چاک که سوده‌ای ناخن</p></div>
<div class="m2"><p>که رنگ خون بهارت ز چنگ می‌بارد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به حیرتم که نگاه از چه حیرت آب دهم</p></div>
<div class="m2"><p>ز خار وگل همه حسن فرنگ می‌بارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل شکسته خمستان یاد نرگس کیست</p></div>
<div class="m2"><p>که اشکم از مژه ساغر به چنگ می بارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مخور فریب مروت ز چرخ مینارنگ</p></div>
<div class="m2"><p>که جای باده از این شیشه سنگ می‌بارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز آبیاری کشت حسد تبرا کن</p></div>
<div class="m2"><p>که خون عافیت از ساز جنگ می‌بارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خطاست تهمت جرات به عجز ما بستن</p></div>
<div class="m2"><p>هزار آبله بر پای لنگ می‌بارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مخواه غیر توهم ز اغنیا بیدل</p></div>
<div class="m2"><p>که ابر مزرع این قوم بنگ می‌بارد</p></div></div>