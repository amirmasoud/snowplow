---
title: >-
    غزل شمارهٔ ۷۹۲
---
# غزل شمارهٔ ۷۹۲

<div class="b" id="bn1"><div class="m1"><p>این‌زمان یک طالب‌مستی درین میخانه نیست</p></div>
<div class="m2"><p>آنکه‌گرد باده‌گردد جز خط پیمانه نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از نشاط‌دل چه می‌پرسی‌که مانند سپند</p></div>
<div class="m2"><p>غیر دود آه حسرت ریشهٔ این دانه نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اضطراب دل چو موج ازپیکر ما روشن است</p></div>
<div class="m2"><p>طرهٔ آشفتگی را احتیاج شانه نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرقدر خواهد دلت اسباب حسرت جمع‌کن</p></div>
<div class="m2"><p>چون‌کمان اینجا به‌جز خمیازه‌رخت‌خانه نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حسنش از جوش نظرها دارد ایجاد نقاب</p></div>
<div class="m2"><p>دامن فانوس شمعش جزپرپروانه نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون‌گل از دور فریب زندگی غافل مباش</p></div>
<div class="m2"><p>رنگ‌می‌گردد درین‌اینجا ساغر و پیمانه‌نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرچه از چشم بتان افتد غبار عاشق ست</p></div>
<div class="m2"><p>اشک‌گرم شمع جز خاکستر پروانه نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهر نسیان غفلت ذاتی نمی‌خواهد سبب</p></div>
<div class="m2"><p>از برای خواب مخمل حاجت افسانه نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر امید الفت از وحشت دلی خوش می‌کنیم</p></div>
<div class="m2"><p>آشنای ماکسی جز معنی بیگانه نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جان پاک از قید تن بیدل ندامت می‌کشد</p></div>
<div class="m2"><p>گنج را جز خاک بر سرکردن از ویرانه نیست</p></div></div>