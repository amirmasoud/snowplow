---
title: >-
    غزل شمارهٔ ۱۴۵۸
---
# غزل شمارهٔ ۱۴۵۸

<div class="b" id="bn1"><div class="m1"><p>پیری آمد ماند عشرتها ز انداز بلند</p></div>
<div class="m2"><p>سرنگون شد شیشه‌، قلقل‌کرد پرواز بلند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دستگاه اصل فطرت جز تنزل هیچ نیست</p></div>
<div class="m2"><p>می‌کندگل پست پست انجام آغاز بلند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرد امکان عمرها شد می‌رود بر باد صبح</p></div>
<div class="m2"><p>تا کجا چیند نفس این دامن ناز بلند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>معنی‌ صوری ‌که‌ گوش ‌کس به ‌فهمش‌ باز نیست</p></div>
<div class="m2"><p>ازسپند بزم ما بشنو به آوازبلند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غافلان تا بر خط شق‌القمر گردن نهند</p></div>
<div class="m2"><p>حکم انگست شهادت داشت اعجاز بلند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زیر گردون هرچه شور انگیخت محو سرمه شد</p></div>
<div class="m2"><p>نغمه‌ها در خاک خوابانید این ساز بلند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین چمن بیدل کسی را شرم دامنگیر نیست</p></div>
<div class="m2"><p>سرو تاگل‌، پا به‌ گل دارد تک و تاز بلند</p></div></div>