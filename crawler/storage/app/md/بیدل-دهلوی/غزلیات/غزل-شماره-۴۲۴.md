---
title: >-
    غزل شمارهٔ ۴۲۴
---
# غزل شمارهٔ ۴۲۴

<div class="b" id="bn1"><div class="m1"><p>تهمت‌افسردگی بر طینت عاشق خطاست</p></div>
<div class="m2"><p>ناله هرجا آینه گردید آزادی‌نماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌فنا مشکل‌که‌گردد دل به عبرت آشنا</p></div>
<div class="m2"><p>چشم این آیینه را خاکستر خود توتیاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شرم باید داشتن از شوخی آثار شرم</p></div>
<div class="m2"><p>چون عرق بی‌پرده‌گردد لغزش پای حیاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا توان آزاد بودن دامن عزلت مگیر</p></div>
<div class="m2"><p>موج را در هر تپش بر وضع‌گوهر خنده‌هاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جام آب زندگی تنها به‌کام خضر نیست</p></div>
<div class="m2"><p>درگداز آرزو هم جوش دریای بقاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>معنی دود ازکتاب شعله انشا کرده‌اند</p></div>
<div class="m2"><p>هرکجا او جلوه دارد ناز هستی مفت ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکه را از نشئهٔ معنی‌ست سیری خامش است</p></div>
<div class="m2"><p>ساغر لبریز اگر صدلب‌گشاید بی‌صداست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عالمی سرگشته است از اضطراب گریه‌ام</p></div>
<div class="m2"><p>اشک من سرچشمهٔ د‌وران چندین آسیاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می‌کند هر جزوم از شوق توکار آینه</p></div>
<div class="m2"><p>خامهٔ تصویرم و هر موی من صورت‌نماست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر برآید ازصدف‌گوهر اسیر رشته است</p></div>
<div class="m2"><p>خانه و غربت دل آگاه را دام بلاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کی پریشان می‌کند باد غرور اجزای من</p></div>
<div class="m2"><p>نسخهٔ خاک مراشیرازه نقش بوریاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اینقدر چون شمع از شوق فنا جان می‌کنم</p></div>
<div class="m2"><p>باکمال سرکشی سعی نگاهم زیرپاست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نقش چندین عبرت از عنوان حالم روشن است</p></div>
<div class="m2"><p>شعلهٔ جوالهٔ من مهر طومار فناست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیدل از مشت غبار ما دل خود جمع‌کن</p></div>
<div class="m2"><p>شانه‌‌ی این طرهٔ آشفته در دست هواست</p></div></div>