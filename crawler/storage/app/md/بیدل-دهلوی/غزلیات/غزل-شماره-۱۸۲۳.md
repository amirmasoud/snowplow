---
title: >-
    غزل شمارهٔ ۱۸۲۳
---
# غزل شمارهٔ ۱۸۲۳

<div class="b" id="bn1"><div class="m1"><p>مپرسید از نگین شاه و اقبال نفس‌ کاهش</p></div>
<div class="m2"><p>به چندین کوچه افکنده‌ست سعی نام در چاهش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خودآرایی به دیهیم زر و یاقوت می‌نازد</p></div>
<div class="m2"><p>ز ماتم ‌کرده غافل خاک رنگین بر سر جاهش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر شخص طلب قدر جنون مفلسی داند</p></div>
<div class="m2"><p>گریبان دامن آراید به طوف دست ‌کوتاهش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ره امن از که پرسم در جنون سامان بیابانی</p></div>
<div class="m2"><p>که محشر چشم می‌پوشد به مژگان پر کاهش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو آن‌ گل ‌کز سر و دستار مستی بر زمین افتد</p></div>
<div class="m2"><p>به لغزیدن من از خود رفتم و دل ماند درراهش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عنان‌گیر غبار سینه چاکان نیست‌گردون هم</p></div>
<div class="m2"><p>سحر هر سو خرامد کوچه‌ها پیداست در راهش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سراپای ‌گهر موج است اگر آغوش بگشاید</p></div>
<div class="m2"><p>گره تاریست کز پیچیدگی کردند کوتاهش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هلال آیینه‌دار است ای ز سامان طلب غافل</p></div>
<div class="m2"><p>که از خمیازهٔ یک ریشه بالد خرمن ماهش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قناعت در مزاج خلق دون فطرت نمی‌باشد</p></div>
<div class="m2"><p>پریشان‌ کرد عالم را زمین آسمان خواهش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه امکانست رمز پردهٔ این وهم بشکافی</p></div>
<div class="m2"><p>که عنقا غفلتست و سعی دانش نیست آگاهش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زبان درکام دزدد هرکه درس عشق می‌خواند</p></div>
<div class="m2"><p>برون لفظ و خط راهی ندارد در ادبگاهش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر اسقاط اضافات است منظور یقین بیدل</p></div>
<div class="m2"><p>بس است الله الله از من‌الله و الی‌اللهش</p></div></div>