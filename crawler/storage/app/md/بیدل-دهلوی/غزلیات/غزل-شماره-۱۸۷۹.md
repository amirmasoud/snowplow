---
title: >-
    غزل شمارهٔ ۱۸۷۹
---
# غزل شمارهٔ ۱۸۷۹

<div class="b" id="bn1"><div class="m1"><p>ساز تبختر است اگر مایهٔ شرف</p></div>
<div class="m2"><p>این خواجه بوق می‌زند اقبال چنگ و دف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیری کجاست تا نگری اقتدار خلق</p></div>
<div class="m2"><p>بالیدگی مخواه ز گاوان ‌کم علف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از رونق ‌کمال تعین حذر کنید</p></div>
<div class="m2"><p>دکان مه پُر است ز آرایش‌ کلف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلقی ز فکر هرزه بیان پیش می‌برد</p></div>
<div class="m2"><p>نازد پدر به شهرت فرزند ناخلف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد بی‌صفا دلی که به نقش و نگار ساخت</p></div>
<div class="m2"><p>گم کردن گهر فکند رنگ بر صدف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عارف ز اعتبار تعین منزه است</p></div>
<div class="m2"><p>دریا حباب نیست‌ که بالد ز موج و کف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وهم فضول دشمن یکتایی است و بس</p></div>
<div class="m2"><p>آیینه تا کجا نکند با خودت طرف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اسرار دل ز هرچه درد پرده مفت‌گیر</p></div>
<div class="m2"><p>مشتاق یک ‌صداست بهم خوردن دو کف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در دشت آتشی‌ که شرر پر نمی‌زند</p></div>
<div class="m2"><p>ما پنبه می‌بریم به امید «‌لاتخف‌»</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تمثال نقش پا هم ازین دشت‌ گل نکرد</p></div>
<div class="m2"><p>از بس شکست و خاک شد آیینهٔ‌ سلف</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نایاب گوهری به کف دل فتاده است</p></div>
<div class="m2"><p>می‌لرزدم نفس که مبادا شود تلف</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل ز حکم غالب تقدیر چاره نیست</p></div>
<div class="m2"><p>صف‌ها گشاده تیر و به یک نقطه دل هدف</p></div></div>