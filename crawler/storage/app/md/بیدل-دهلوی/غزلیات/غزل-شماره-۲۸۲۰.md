---
title: >-
    غزل شمارهٔ ۲۸۲۰
---
# غزل شمارهٔ ۲۸۲۰

<div class="b" id="bn1"><div class="m1"><p>بر هرگلی دمیده‌ست افسون آرزویی</p></div>
<div class="m2"><p>بوی شکسته رنگی رنگ پریده بویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناموس ناتوانی افتاده بر سر هم</p></div>
<div class="m2"><p>رنگ شکسته دارد بر ششجهت غلویی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سازی که چینی دل ناز ترنمش داشت</p></div>
<div class="m2"><p>روشن شد آخر کار از پرده تار مویی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درکاروان هستی یک جنس نیستی بود</p></div>
<div class="m2"><p>زین چار سو گزیدیم دکان چارسویی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تدبیر خانمانت در عشق خنده دارد</p></div>
<div class="m2"><p>کشتی شکسته آنگه غمخواری سبویی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از هر سری درین بحر ناز حباب ‌گل ‌کرد</p></div>
<div class="m2"><p>مست شناست اینجا بیمغزی کدویی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا چشم باز کردیم با تو چه ساز کردیم</p></div>
<div class="m2"><p>بر ما چو نی ستم کرد آوازی و گلویی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون‌گرد باد زین دشت صد نخل بی‌ثمر رست</p></div>
<div class="m2"><p>ما نیزکرده باشیم بی ‌پا و سر نمویی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جوش و خروش عشقیم زیر و بم هوس چیست</p></div>
<div class="m2"><p>هر پشه در طنینش دارد نهنگ هویی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هستی همان عدم بود، نی کیفی و نه کم بود</p></div>
<div class="m2"><p>در هر لب و دهانی من داشته‌ست اویی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در معبدی که پاکان از شرم آب گشتند</p></div>
<div class="m2"><p>ما را نخواست غفلت تر دامن وضویی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون شمع تا رسیدیم در بزمگاه قسمت</p></div>
<div class="m2"><p>یاران نشاط بردند ما داغ شعله خویی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دل بر چه داغ مالیم سر بر چه سنگ ساییم</p></div>
<div class="m2"><p>ما را نمی‌دهد بار آیینه پیش رویی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیدل گذشت خلقی ماْیوس تشنه‌کامی</p></div>
<div class="m2"><p>غیر از نفس درین باغ آبی نداشت جویی</p></div></div>