---
title: >-
    غزل شمارهٔ ۱۷۲۰
---
# غزل شمارهٔ ۱۷۲۰

<div class="b" id="bn1"><div class="m1"><p>هر کجا آیینهٔ ما گردد از زنگار سبز</p></div>
<div class="m2"><p>گر همه طوطی شوی نتوان شد آن مقدار سبز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این چمن الفت‌پرست سایهٔ ‌گیسوی کیست</p></div>
<div class="m2"><p>سبزه می‌جوشد به ‌گردن رشتهٔ زنٌار سبز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برگ عیش قانعان بی‌گفتگو آماده است</p></div>
<div class="m2"><p>شد زبان بسته از خاموشی اظهار سبز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر مزاج خام ظالم پخته‌کار افتد بلاست</p></div>
<div class="m2"><p>ورنه دارد طبع‌گل چندان‌ که باشد خار سبز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسوت ما هرچه باشد ناله خون‌آلوده است</p></div>
<div class="m2"><p>طوطیان را کم شود چون بال و پر منقار سبز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از لب شاداب او چون سنبل اندر چشمه‌سار</p></div>
<div class="m2"><p>موج می‌خواهد شدن در ساغر خمّار سبز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر سحاب آرد نوید سایهٔ نخل قدش</p></div>
<div class="m2"><p>نالهٔ بلبل دهد چون سرو از این‌ گلزار سبز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برق حسن نو خطی در گل ‌گرفت آیینه را</p></div>
<div class="m2"><p>جلوه‌گر این است‌ کشت تشنهٔ دیدار سبز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ریشهٔ ‌گل بی‌طراوت نیست از ابر بهار</p></div>
<div class="m2"><p>می‌کند تردستی مطرب زبان تار سبز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هیچ زشتی در مقام خویش نامرغوب نیست</p></div>
<div class="m2"><p>خار را دارد همان چون گل سر دیوار سبز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رنگ می‌بندد لب خندان به عزلت خو مکن</p></div>
<div class="m2"><p>آب هم می‌گردد از آسودن بسیار سبز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آبروی مرد بیدل با هنر جوشیدنست</p></div>
<div class="m2"><p>نیست در شمشیرها جز تیغ جوهردار سبز</p></div></div>