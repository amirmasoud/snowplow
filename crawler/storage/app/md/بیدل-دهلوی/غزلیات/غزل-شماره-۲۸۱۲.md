---
title: >-
    غزل شمارهٔ ۲۸۱۲
---
# غزل شمارهٔ ۲۸۱۲

<div class="b" id="bn1"><div class="m1"><p>نشد آیینه‌ کیفیت ما ظاهر آرایی</p></div>
<div class="m2"><p>نهان ماندیم چون معنی به چندین لفظ پیدایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به غفلت ساخت دل تا وارهید از غیرت امکان</p></div>
<div class="m2"><p>چه‌ها می‌سوخت این آیینه‌ گر می‌داشت بینایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مزاج عافیت یکسر شکست آماده است اینجا</p></div>
<div class="m2"><p>همه‌ گر سنگ باشد نیست بی‌اندوه مینایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلد عشق است از سر منزل مجنون چه می‌پرسی</p></div>
<div class="m2"><p>که اینجا خانه‌ها چون دیدهٔ آهوست صحرایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیال زندگی پختن دماغ هرزه می‌خواهد</p></div>
<div class="m2"><p>همه ‌گر دل شود آیینه‌ات آن به که ننمایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>علف خواری نباید سر کشد از حکم‌ گردونت</p></div>
<div class="m2"><p>که دوش از بار اگر دزدی به زیر چوب می‌آیی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز ننگ اعتبار پوچ هستی بر نمی‌آید</p></div>
<div class="m2"><p>عدم‌ کرد از ترحم پیکر ما را هیولایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نوایی از صدف‌ گل می‌کند کای غافل از قسمت</p></div>
<div class="m2"><p>لب خشکی که ما داریم دربایی‌ست دریایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به خاموشی مباش از نالهٔ بی‌رنگ دل غافل</p></div>
<div class="m2"><p>نفس چندین نیستان ریشه دارد از لب نایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به خواب ناز هم زان چشم جادو می‌کشد قامت</p></div>
<div class="m2"><p>به انداز بلندیهای مژگان فتنه بالایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نهان می‌دارد از شرم تکلم لعل خاموشش</p></div>
<div class="m2"><p>چو بند نیشکر در بوس هم ذوق شکرخایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هلال اوج قدر از وضع تسلیم تو می‌بالد</p></div>
<div class="m2"><p>فلک فرشی‌ گر از خود یک خم ابرو فرود آیی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ندانم با که می‌باید درین ویرانه جوشیدن</p></div>
<div class="m2"><p>به هرمحفل‌که ره بردم چو شمعم سوخت تنهایی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هوای دامن او گر نباشد شهپر همت</p></div>
<div class="m2"><p>که بر می‌دارد از مشت غبارم ناتوانایی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چه سان از سستی طالع ز پا افتاده‌ام بیدل</p></div>
<div class="m2"><p>که تمثال ضعیفم را کند آیینه دیبایی</p></div></div>