---
title: >-
    غزل شمارهٔ ۶۵۳
---
# غزل شمارهٔ ۶۵۳

<div class="b" id="bn1"><div class="m1"><p>عاقبت چون شعله خاکستر به فرق ما نشست</p></div>
<div class="m2"><p>درد صهبا پنبه ‌گشت و بر سر مینا نشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌توام‌ گرد ضعیفی بس که بر اعضا نشست</p></div>
<div class="m2"><p>ناله‌ام درکوچهٔ نی چون‌ گره صدجا نشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کس نمی‌فهمد زبان سوختن تقریر شمع</p></div>
<div class="m2"><p>در میان انجمن می‌بایدم تنها نشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌توان در خاکساری یافت اوج اعتبار</p></div>
<div class="m2"><p>آبله شد صاحب افسر، بسکه زیر پا نشست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که را سررشتهٔ وضع حیا باشد به دست</p></div>
<div class="m2"><p>می‌تواند چون نگه در دیدهٔ بینا نشست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شعلهٔ شوقت نشد پنهان به فانوس خیال</p></div>
<div class="m2"><p>همچو رنگ‌ این‌ می برون‌ از خلوت‌ مینا نشست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سعی پرواز فنا را، اعتبار دیگر است</p></div>
<div class="m2"><p>رفت ‌گرد ما به جایی‌ کز فلک بالا نشست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تیره‌باطن را چه سود از صحبت روشندلان</p></div>
<div class="m2"><p>صاف نبود زنگ با آیینه‌گر یک جا نشست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ننگ وضع هم بساطیهای مجنون برنداشت</p></div>
<div class="m2"><p>گرد ما شد آب تا در دامن صحرا نشست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شعلهٔ ما را درین بزم آرمیدن مفت نیست</p></div>
<div class="m2"><p>صد تپیدن سوخت تا یک داغ نقش یا نشست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آبرو ذاتی‌ست بیدل ورنه مانند گهر</p></div>
<div class="m2"><p>مهرهٔ ‌گل هم تواند در دل دریا نشست</p></div></div>