---
title: >-
    غزل شمارهٔ ۱۹۶۶
---
# غزل شمارهٔ ۱۹۶۶

<div class="b" id="bn1"><div class="m1"><p>هستی نیاز دیده نمناک کرده‌ام</p></div>
<div class="m2"><p>تا شمع سان جبین زعرق پاک کرده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راهم به کوچهٔ دگر است از رم نفس</p></div>
<div class="m2"><p>زبن موج می سراغ رگ تاک‌کرده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تیغی به جادهٔ دم الفت نمی‌رسد</p></div>
<div class="m2"><p>سیر هزار راه خطرناک کرده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل از نفس نمی‌گسلد ربط آرزو</p></div>
<div class="m2"><p>این رشته را خیال چه فتراک کرده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طاقت به دوش‌ کس ننهد بار احتیاج</p></div>
<div class="m2"><p>وامانده‌ام که تکیه بر افلاک کرده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از ضعف پیریی ‌که سرانجام زندگی‌ست</p></div>
<div class="m2"><p>دندان غلط به ریشهٔ مسواک کرده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پر بیدماغ فطرتم از سجده‌ام مپرس</p></div>
<div class="m2"><p>سر بود گوهری که کنون خاک کرده‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرد شکستم از چه نخندد به روی‌کار</p></div>
<div class="m2"><p>مزدوری قلمرو ادراک کرده‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیدل حنایی از چه نگردد بیاض چشم</p></div>
<div class="m2"><p>خطها به‌خون نوشته‌ام و پاک کرده‌ام</p></div></div>