---
title: >-
    غزل شمارهٔ ۲۱۹۸
---
# غزل شمارهٔ ۲۱۹۸

<div class="b" id="bn1"><div class="m1"><p>واکرد صبح آهی بر دل در تبسم</p></div>
<div class="m2"><p>تا آسمان فشاندم بال و پر تبسم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل بی تو زین گلستان یاد شکفتنی کرد</p></div>
<div class="m2"><p>بردم ز جوش زخمش تا محشر تبسم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما را به رمز اعجاز لعل تو آشنا کرد</p></div>
<div class="m2"><p>شاید مسیح باشد پیغمبر تبسم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر حسن در خور ناز عرض بهار دارد</p></div>
<div class="m2"><p>من هم بقدر حیرت دارم سر تبسم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا چشم باز کردم صد زخم ساز کردم</p></div>
<div class="m2"><p>در حیرتم چو می‌خواند افسونگر تبسم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امید ما بهار است از چین ابروی ناز</p></div>
<div class="m2"><p>یارب مباد تیغش بی‌جوهر تبسم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نتوان ز لعل خوبان قانع شدن به بوسی</p></div>
<div class="m2"><p>گردیدن‌ست چون خط‌گرد سر تبسم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای هوش بی‌تأمل از لعل یار بگذر</p></div>
<div class="m2"><p>بی‌شوخی خطی نیست آن مسطر تبسم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از صبح هستی ما شبنم نکرد اشکی</p></div>
<div class="m2"><p>پر بی‌نمک دمیدیم از منظر تبسم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای صبح رنگ عشرت تا کی بقا فروشد</p></div>
<div class="m2"><p>مالیده‌ گیر بر لب خاکستر تبسم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل ز معنی دل خوش بیخبر گذشتی</p></div>
<div class="m2"><p>این غنچه بود مهری بر دفتر تبسم</p></div></div>