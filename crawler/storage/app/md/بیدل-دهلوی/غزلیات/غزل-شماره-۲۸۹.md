---
title: >-
    غزل شمارهٔ ۲۸۹
---
# غزل شمارهٔ ۲۸۹

<div class="b" id="bn1"><div class="m1"><p>از پا نشیند ای کاش محمل‌کش هوس‌ها</p></div>
<div class="m2"><p>زین کاروان شنیدیم نالیدن جرس‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بازار ظلم گرم است از پهلوی ضعیفان</p></div>
<div class="m2"><p>آتش به عزم اقبال دارد شگون ز خس‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در طبع خودسر جاه سعی گزند خلق است</p></div>
<div class="m2"><p>دیوانه‌اند سگ‌ها از کندن مرس‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای مزرعی است کآنجا دهقان صنع پوشید</p></div>
<div class="m2"><p>خون‌های زخم گندم در پرده عدس‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از حرص منفعل شد خوان‌گستر قناعت</p></div>
<div class="m2"><p>برد از شکر حلاوت جوشیدن مگس‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در عرصه گاه تسلیم از یکدگر گذشته‌ست</p></div>
<div class="m2"><p>مانند موج گوهر جولان پیش و پس‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>افغان به سرمه خوابید کس مدعا نفهمید</p></div>
<div class="m2"><p>آخر به خاک بردیم ابرام ملتمس‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون ناله زین نیستان رستن چه احتمال است</p></div>
<div class="m2"><p>خط می‌کشیم عمری‌ست بر مسطر قفس‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مجنون شدیم اما داد جنون ندادیم</p></div>
<div class="m2"><p>تا دامن و گریبان کم بود دسترس‌ها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل به مشق اوهام دل را سیاه کردیم</p></div>
<div class="m2"><p>تا کی طرف برآید آیینه با نفس‌ها</p></div></div>