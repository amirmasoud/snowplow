---
title: >-
    غزل شمارهٔ ۲۰۸۱
---
# غزل شمارهٔ ۲۰۸۱

<div class="b" id="bn1"><div class="m1"><p>نفسی چند جدا از نظرت می‌گردم</p></div>
<div class="m2"><p>باز می‌آیم و برگرد سرت می‌گردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هستی‌ام‌ گرد خرام است چه صحرا و چه باغ</p></div>
<div class="m2"><p>هرکجا مهر تو تابد سحرت می‌گردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی‌تو با عالم اسباب چه کار است مرا</p></div>
<div class="m2"><p>موج این بحر به ذوق‌ گهرت می‌گردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست معراج دگر مقصد تسلیم وفا</p></div>
<div class="m2"><p>خاک این مرحله‌ام پی سپرت می‌گردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نفس خون شده در خلوت دل بار نیافت</p></div>
<div class="m2"><p>محرم رازم و بیرون درت می‌گردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در میان هیچ نمی‌یابم ازین مجمع وهم</p></div>
<div class="m2"><p>لیک بر هر چه بپیچم‌کمرت می‌گردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وهم دوری چقدر سحر طراز است‌که من</p></div>
<div class="m2"><p>همعنان تو به‌ذوق خبرت می‌گردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وصل بیتاب پیام است چه سازم یا رب</p></div>
<div class="m2"><p>پیش خود درهمه‌جا نامه برت می‌گردم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به نمی از عرق شرم غبارم بنشان</p></div>
<div class="m2"><p>که من گم شده دل دربه‌درت می‌گردم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل ازسعی مکن شکوه‌که یک‌گام دگر</p></div>
<div class="m2"><p>پای خوابیدهٔ بی درد سرت می‌گردم</p></div></div>