---
title: >-
    غزل شمارهٔ ۳۹۱
---
# غزل شمارهٔ ۳۹۱

<div class="b" id="bn1"><div class="m1"><p>چه دارد این صفات حاجت آیات</p></div>
<div class="m2"><p>به جز ورد دعای حضرت ذات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غنا و فقرهستی لا والاست</p></div>
<div class="m2"><p>گدایی نفی و شاهنشاهی اثبات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فسون ظاهر و مظهر مخوانید</p></div>
<div class="m2"><p>خیال است این چه تمثال و چه مرآت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهان گل کردهٔ یکتایی اوست</p></div>
<div class="m2"><p>ندارد شخص تنها جز خیالات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نباشد مهر اگر صبح تبسم</p></div>
<div class="m2"><p>که خندد جز عدم بر روی ذرات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مه وسال وشب وروزت مجازیست</p></div>
<div class="m2"><p>حقیقت نه زمان دارد نه ساعات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشاط و رنج ما تبدیل اوضاع</p></div>
<div class="m2"><p>بلند وپست ما تغییر حالات</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همین غیب و شهادت فرق دارد</p></div>
<div class="m2"><p>معانی در دل و برلب عبارات</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فروغی بسته بر مرآت اعیان</p></div>
<div class="m2"><p>چراغان شبستان محالات</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه او را جزتقدس میل آثار</p></div>
<div class="m2"><p>نه ما را غیر معدومی علامات</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو و غافل ز من‌، افسوس‌، افسوس</p></div>
<div class="m2"><p>من و دور از درت‌، هیهات‌، هیهات</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زبان شرم اگر باشد به کامت</p></div>
<div class="m2"><p>خموشی نیست بیدل جز مناجات</p></div></div>