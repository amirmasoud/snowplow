---
title: >-
    غزل شمارهٔ ۲۳۲۸
---
# غزل شمارهٔ ۲۳۲۸

<div class="b" id="bn1"><div class="m1"><p>با کف خاکستری سودای اخگر کرده‌ایم</p></div>
<div class="m2"><p>سر به تسلیم ادب گم در ته پر کرده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آرزوها در مزاج ما نفس دزدید و سوخت</p></div>
<div class="m2"><p>خویش را چون قطرهٔ بی‌موج گوهر کرده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اشک غلتانیم کز دیوانگیهای طلب</p></div>
<div class="m2"><p>لغزش پا را خیال گردش سر کرده‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی‌زبانی دارد ابرامی‌ که در صد کوس نیست</p></div>
<div class="m2"><p>هر کجا گوش است ما از خامشی کر کرده‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از شکوه اقتدار هیچ بودنها مپرس</p></div>
<div class="m2"><p>ذره‌ایم اقلیم معدومی مسخر کرده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنقدر وسعت ندارد ملک هستی تا عدم</p></div>
<div class="m2"><p>چون نفس پر آمد و رفت مکرر کرده‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاقبت خط غبار از نسخهٔ ما خواندنی است</p></div>
<div class="m2"><p>باد می‌گرداند آوازی که دفتر کرده‌ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خامشی در علم جمعیت رباضتخانه است</p></div>
<div class="m2"><p>فربهی‌های زمان لاف لاغر کرده‌ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آستان خلوت‌ کنج عدم‌ کمفرصتی است</p></div>
<div class="m2"><p>شعلهٔ جواله‌ای را حلقهٔ در کرده‌ایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مقصد ما زین چمن ‌بر هیچکس ‌روشن ‌نشد</p></div>
<div class="m2"><p>رنگ گل بوده‌ست پروازی که بی‌پر کرده‌ایم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زحمت فهم از سواد سرنوشت ما مخواه</p></div>
<div class="m2"><p>خط موهومی عیان بود از عرق ترکرده‌ایم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یک دو دم بیدل به ذوق دل درین وحشت‌سرا</p></div>
<div class="m2"><p>چون نفس در خانهٔ آیینه لنگر کرده‌ایم</p></div></div>