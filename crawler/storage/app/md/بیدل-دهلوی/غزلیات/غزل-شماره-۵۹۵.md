---
title: >-
    غزل شمارهٔ ۵۹۵
---
# غزل شمارهٔ ۵۹۵

<div class="b" id="bn1"><div class="m1"><p>شور استغنای عشق از حسرت دل بوده است</p></div>
<div class="m2"><p>کوس ارباب‌کرم فریاد سایل بوده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم غفلت‌پیشه را افسردگی امروزنیست</p></div>
<div class="m2"><p>مشت خاک ما به هرجا بود کامل بوده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در گرفتاری رسا شد نشئهٔ پرواز من</p></div>
<div class="m2"><p>بال آزادی چو سروم پای در گل بوده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>موج تا در جنبش آید می‌رود از خود حباب</p></div>
<div class="m2"><p>گرد بال‌افشانی رنگم همین دل بوده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد تپیدن جاده سرمنزل آسایشم</p></div>
<div class="m2"><p>آشیان عیش زیر بال بسمل بوده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غافلم دارد، ز دریا لاف بینش چون حباب</p></div>
<div class="m2"><p>پرده چشمی به چندین جلوه حایل بوده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کرد آخر واصل بزم تو از خود رفتنم</p></div>
<div class="m2"><p>سایه را در خانهٔ خورشید منرل بوده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قالب افسرده ما را در غبار وهم سوخت</p></div>
<div class="m2"><p>غرقهٔ بحری که ما بودیم ساحل بوده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دفتر امکان ز بیکاری ندارد صفحه‌ای</p></div>
<div class="m2"><p>پردهٔ چشم غلط بین فرد باطل بوده است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر فنا خواهم غم قطع امیدم می‌کشد</p></div>
<div class="m2"><p>مرگ هم چون زندگانی بی‌تو مشکل بو‌ده است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون نفس آیینهٔ دل هم ثبات ما نداد</p></div>
<div class="m2"><p>حیف‌نقش ماکه در هر صفحه‌زایل‌بوده است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیخودی‌کرد از حضور لیلی دل غافلم</p></div>
<div class="m2"><p>ورنه هر اشکی که رفت از دیده محمل بوده است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نیست نیرنگی‌که نقش اعتبار خاک نیست</p></div>
<div class="m2"><p>نیست‌گردیدن به‌صد هستی مقابل بوده است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>امتداد عمر بیدل سختی از طبعم ربود</p></div>
<div class="m2"><p>گردش سال آسیای دانهٔ دل بوده است</p></div></div>