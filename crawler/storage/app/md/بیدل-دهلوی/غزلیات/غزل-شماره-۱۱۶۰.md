---
title: >-
    غزل شمارهٔ ۱۱۶۰
---
# غزل شمارهٔ ۱۱۶۰

<div class="b" id="bn1"><div class="m1"><p>تا ز عبرت سر مژگان به خمیدن نرسد</p></div>
<div class="m2"><p>آنجه زیر قدم تست به دیدن نرسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش از انجام تماشا همه افسانه شمار</p></div>
<div class="m2"><p>دیدنی نیست که آخر به شنیدن نرسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای طرب در قفس غنچه پرافشان می‌باش</p></div>
<div class="m2"><p>صبح ما رفت به جایی ‌که دمیدن نرسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نخل یأسیم‌ که در باغ طرب‌خیز هوس</p></div>
<div class="m2"><p>ثمر ما به تمنای رسیدن نرسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی‌طلب برگ دو عالم همه ساز است اما</p></div>
<div class="m2"><p>حرص مشکل‌که به رنج طلبیدن نرسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شرر کاغذت آمادهٔ صد پرواز است</p></div>
<div class="m2"><p>صفحه آتش زن اگر مشق پریدن نرسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشود حکم قضا تابع تدبیرکسی</p></div>
<div class="m2"><p>به‌گمان فلک افسون‌کشیدن نرسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جوهری لازم آیینهٔ عریانی نیست</p></div>
<div class="m2"><p>دامن ‌کسوت دیوانه به چیدن نرسد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مطلب بوی ثبات از چمن عشرت دهر</p></div>
<div class="m2"><p>هر چه بر رنگ تند جز به پریدن نرسد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شرح چاک جگر از عالم تحریر جدست</p></div>
<div class="m2"><p>آه اگر نامهٔ عاشق به دریدن نرسد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل افسانهٔ راحت ز نفس چشم مدار</p></div>
<div class="m2"><p>این نسیمی است‌ که هرگز به وزیدن نرسد</p></div></div>