---
title: >-
    غزل شمارهٔ ۴۵۰
---
# غزل شمارهٔ ۴۵۰

<div class="b" id="bn1"><div class="m1"><p>بسکه سودای توام سرتا به پا زنجیر پاست</p></div>
<div class="m2"><p>موی‌سر چون‌دود شمعم‌جمع‌با زنجیر پاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اشکم و بر انتظار جلوه‌ای پیچیده‌ام</p></div>
<div class="m2"><p>یاد آن‌گل شبنم شوق‌مرا زنجیرپاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همتی ای ناله تا دام تعلق بگسلیم</p></div>
<div class="m2"><p>یعنی از خود می‌رویم و رهنما زنجیر پاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عالم تسخیر الفت هم تماشاکردنی‌ست</p></div>
<div class="m2"><p>جلوه‌اش را حلقه‌های چشم ما زنجیر پاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما سبکروحان اسیر سادگیهای دلیم</p></div>
<div class="m2"><p>عکس را درآینه موج صفا زنجیرپاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کو خروشی تا پر افشانیم و از خود بگذریم</p></div>
<div class="m2"><p>چون سپند اینجا همین ضبط صدا زنجیرپاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از شکست دل چه می‌پرسی‌که مجنون مرا</p></div>
<div class="m2"><p>نقش پا هم ناله‌فرسود است تا زنجیرپاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با همه آزادی از جیب تعلق رسته‌ایم</p></div>
<div class="m2"><p>سرو را سررشتهٔ نشوو نما زنجیرپاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا نفس باقی است باید با علایق ساختن</p></div>
<div class="m2"><p>خضررا هم الفت آب بقا زنجیرپاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیشتر در طبع‌پیران آشیان دارد امل</p></div>
<div class="m2"><p>حرص سوداپیشه را قد ‌دوتا زنجیر پاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنقدر وسعت‌مچین‌کز خویش‌نتوانی‌گذشت</p></div>
<div class="m2"><p>ای هوس پیرایه دامان رسا زنجیر پاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غافل از قید هوس دارد به جا افسردنت</p></div>
<div class="m2"><p>اندکی برخیزتا بینی چها زنجیرپاست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آشیان ساز تماشاخانهٔ بیرنگی‌ام</p></div>
<div class="m2"><p>شبنم ما را همان طبع هوا زنجیرپاست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اینقدر بی‌اختیار از اختیار افتاده‌ایم</p></div>
<div class="m2"><p>دست‌ما بر دست‌ماسنگ‌است‌و پا زنجیر پاست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بیدل ازکیفیت ذوق گرفتاری مپرس</p></div>
<div class="m2"><p>من سری دزدیده‌ام در هرکجا زنجیر پاست</p></div></div>