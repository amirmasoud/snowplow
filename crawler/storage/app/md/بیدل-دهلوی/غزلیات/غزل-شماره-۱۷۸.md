---
title: >-
    غزل شمارهٔ ۱۷۸
---
# غزل شمارهٔ ۱۷۸

<div class="b" id="bn1"><div class="m1"><p>اگر مردی در تسلیم زن راه طلب مگشا</p></div>
<div class="m2"><p>ز هر مو احتیاجت‌گرکند فریاد لب مگشا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p> خم شمشیر جرأت صرف ایجاد تواضع‌کن</p></div>
<div class="m2"><p>به‌این‌ناخن همان جزعقدة چین‌غضب‌مگشا </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خریداران همه سنگند معنیهای نازک را</p></div>
<div class="m2"><p>زبان خواهی‌کشید اجناس بازار حلب مگشا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p> ز علم عزت و خواری به مجهولی قناعت‌کن</p></div>
<div class="m2"><p>تسلی برنمی‌آید معمای سبب مگشا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p> به ننگ انفعالت رغبت دنیا نمی‌ارزد</p></div>
<div class="m2"><p>زه بند قبایت بر فسون این جلب مگشا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p> عدم گفتن‌کفایت می‌کند تا آدم و حوا</p></div>
<div class="m2"><p>دگر ای هرزه درس وهم طو‌مار نسب مگشا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p> بنای سرکشی چون اشک سرتا پا خلل دارد</p></div>
<div class="m2"><p>علاج سیل آفت‌کن سربند ادب مگشا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p> ستم می‌پرورد آغوش گل از خار پروردن</p></div>
<div class="m2"><p>زبانی راکزوکار درود آید به سب مگشا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p> حضور نورت از دقت نگاهی ننگ می‌دارد</p></div>
<div class="m2"><p>به رنگ‌چشم خفاش این‌گره‌جز پیش شب مگشا </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سبکروحی نیاید راست با وهم جسد بیدل</p></div>
<div class="m2"><p> طلسم بیضه تا نشکسته‌ای بال طرب مگشا</p></div></div>