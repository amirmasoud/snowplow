---
title: >-
    غزل شمارهٔ ۲۲۴۰
---
# غزل شمارهٔ ۲۲۴۰

<div class="b" id="bn1"><div class="m1"><p>ز بس گرد وحشت گرفته است تنگم</p></div>
<div class="m2"><p>به یک پا چو شمع ایستاده است رنگم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلی دارم آزادی امکان ندارد</p></div>
<div class="m2"><p>ز مینا چو دست پری زیر سنگم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نفس دستگاهم مپرس از کدورت</p></div>
<div class="m2"><p>چوآیینه آبیست تکلیف رنگم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه سازم به افسون فرصت شماری</p></div>
<div class="m2"><p>چو عزم شرر در فشار درنگم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشم تاکجا خجلت نارسایی</p></div>
<div class="m2"><p>به پا تیشه زن چون سراپای لنگم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز موهومیم تا به آثار عنقا</p></div>
<div class="m2"><p>تفاوت همین بس‌که نام است ننگم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به تحقیق ره بردم از وهم هستی</p></div>
<div class="m2"><p>به‌کیفیت می رسانید بنگم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهاری ‌کز آن جلوه رنگی ندارد</p></div>
<div class="m2"><p>گلش می‌دهد می به داغ پلنگم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به دریوزهٔ‌ گرد دامان نازش</p></div>
<div class="m2"><p>اگرکف‌گشایم دمد گل ز چنگم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زگیسو نیاید فسون نگاهش</p></div>
<div class="m2"><p>تو از هند مگذر که من در فرنگم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دلم کارگاه چه میناست بیدل</p></div>
<div class="m2"><p>جرس بسته عبرت به دوش ترنگم</p></div></div>