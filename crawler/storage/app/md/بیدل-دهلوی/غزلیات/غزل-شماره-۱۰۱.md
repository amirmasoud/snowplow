---
title: >-
    غزل شمارهٔ ۱۰۱
---
# غزل شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>به گلشن گر برافشاند ز روی ناز کاکل را</p></div>
<div class="m2"><p>هجوم ناله‌ام آشفته سازد زلف سنبل را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p> چرا عاشق نگیرد ازخطش درس ز خود رفتن </p></div>
<div class="m2"><p> که‌بلبل موج جام‌باده می‌خواند رگ‌گل‌را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نفس دزدیدنم توفان خون در آستین دارد </p></div>
<div class="m2"><p> گلوی شیشه‌ام بامی فروبرده‌ست قلقل را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز جیب‌ریشه اسرار چمن‌گل می‌کند آخر </p></div>
<div class="m2"><p> کمال جزو دارد دستگاه معنی‌کل را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چراغ پیری‌ام آخربه‌اشک یأس شد روشن</p></div>
<div class="m2"><p> زگردسیل دادم سرمه‌چشم حلقهٔ پل را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p> درین‌گلشن اگر از ساز یکرنگی خبر داری </p></div>
<div class="m2"><p> ز بوی‌گل توانی درکشید آوز بلبل را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فنا مشکل‌کند منع تپش از طینت عاشق </p></div>
<div class="m2"><p> به‌ساحل نیز درد موج‌این دریا تسلسل‌را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز فرق قرب و بعد نازمشتاقان چه‌می‌پرسی</p></div>
<div class="m2"><p> توان ازگردش چشمی نگه‌کردن تغافل‌را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p> به‌فکر خودگره‌گشتیم‌وبیرون ریخت‌اسرارش </p></div>
<div class="m2"><p> فشار طرفه‌ای بوده‌ست آغوش تأمل را </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز دل در هر تپیدن عالم دیگر تماشا کن</p></div>
<div class="m2"><p>مکررنیست گرصدبار گویدشیشه‌قلقل‌را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p> تمنا حسرت الفت خمارچشم میگونت</p></div>
<div class="m2"><p> سراغ‌کوچهٔ ناسور داند شیشهٔ مل را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>علاج زخم‌دل ازگریه‌کی ممکن‌بود بیدل</p></div>
<div class="m2"><p> به شبنم بخیه نتوان‌کرد چاک‌دامن‌گل را</p></div></div>