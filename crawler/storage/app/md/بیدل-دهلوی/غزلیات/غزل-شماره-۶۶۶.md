---
title: >-
    غزل شمارهٔ ۶۶۶
---
# غزل شمارهٔ ۶۶۶

<div class="b" id="bn1"><div class="m1"><p>با کمال بی‌نقابی پرده‌دارم شیونست</p></div>
<div class="m2"><p>همچو درد از دل برون جوشیدنم پیراهنست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سجده ریزی دانه را آرایش نشو ونماست</p></div>
<div class="m2"><p>درطریق سرکشها خاک‌گشتن هم فنست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عافیت‌گم‌کردهٔ تا چند خواهی تاختن</p></div>
<div class="m2"><p>هوش اگرداری دماغ جستجویت رهزنست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رهنورد عجز را سعی قدم درکار نیست</p></div>
<div class="m2"><p>شمع را سیرگریبان نیز از خود رفتنست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لاله‌زار دل سراسر موج عبرت می‌زند</p></div>
<div class="m2"><p>هرگل داغی‌که می‌بینی شکافت‌گلخنست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اختیاری نیست‌گردش از نظرها نگذرد</p></div>
<div class="m2"><p>در تماشاگه عبرت چشم ما پرویزنست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وحشتی می‌باید اسباب جنون آماده است</p></div>
<div class="m2"><p>صد گریبان‌چاکی‌ات موقوف چین دامنست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشم برهم نه اگرآسوده خواهی زیستن</p></div>
<div class="m2"><p>در هلاکتگاه امکان ربط مژگان جوشنست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوشه‌پردازی نمی‌ارزد به تشویش درو</p></div>
<div class="m2"><p>زندگی نذر عزیزان‌،‌گر دماغ مردنست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل از بس در شکنج لاغری فرسوده‌ایم</p></div>
<div class="m2"><p>ناله و داغ دل خون‌گشته طوق وگردنست</p></div></div>