---
title: >-
    غزل شمارهٔ ۱۱۹۲
---
# غزل شمارهٔ ۱۱۹۲

<div class="b" id="bn1"><div class="m1"><p>وضع فلک آنجا که به یک حال نباشد</p></div>
<div class="m2"><p>رنگ من و تو چند سبکبال نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا وانگری رفته‌ای از دیدهٔ احباب</p></div>
<div class="m2"><p>آب آن همه زندانی غربال نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گردن نفرازی‌ که در این مزرع عبرت</p></div>
<div class="m2"><p>چون دانه سری نیست که پامال نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل را نفریبی به فسونهای تعین</p></div>
<div class="m2"><p>آرایش این آینه تمثال نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عیبی بتر از لاف کمالات ندیدیم</p></div>
<div class="m2"><p>شرمی که لبت تشنهٔ تبخال نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از شکر محبت دل ما بیخبر افتاد</p></div>
<div class="m2"><p>در قحط وفا جرم مه و سال نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>امروز گر انصاف دهد داد طبایع</p></div>
<div class="m2"><p>کس منتظر مهدی و دجال نباشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای آینه هر سو گذری مفت تماشاست</p></div>
<div class="m2"><p>امید که آهیت به دنبال نباشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دامان کری گیر و نوای همه بشنو</p></div>
<div class="m2"><p>تا پیش تو صاحب غرضی لال نباشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خفت مکش از خلق و به اظهار غناکوش</p></div>
<div class="m2"><p>هرچند به دست تو زر و مال نباشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در هرکف خاکی که فتادیم‌، فتادیم</p></div>
<div class="m2"><p>پهلوی ادب قرعهٔ رمال نباشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تر می‌کند اندیشهٔ خشکی مژه‌ام را</p></div>
<div class="m2"><p>مغز قلم نرگس من نال نباشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آزادگی و سیرگریبان چه خیال است</p></div>
<div class="m2"><p>بیدل سر پرواز ته بال نباشد</p></div></div>