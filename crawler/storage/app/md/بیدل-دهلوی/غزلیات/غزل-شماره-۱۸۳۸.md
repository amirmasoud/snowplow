---
title: >-
    غزل شمارهٔ ۱۸۳۸
---
# غزل شمارهٔ ۱۸۳۸

<div class="b" id="bn1"><div class="m1"><p>عمرها شد بی‌نصیب راحتم از چشم خویش</p></div>
<div class="m2"><p>چون نگه پا در رکاب وحشتم از چشم خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین چمن صد رنگ عریانی تماشا کرده‌ام</p></div>
<div class="m2"><p>همچو شبنم درگداز خجلتم از چشم خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس که در یاد نگاهت سرمه شد اجزای من</p></div>
<div class="m2"><p>کس نمی‌خواهد جدا یک ساعتم از چشم خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شوق دیدارم به هر آیینه توفان می‌کند</p></div>
<div class="m2"><p>عالمی دارد سراغ حیرتم‌ از چشم خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جوهر بینش خسک ریز بساط‌ کس مباد</p></div>
<div class="m2"><p>می‌پرد چول شمع رنگ طاقتم از چشم خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نسخهٔ موهوم امکان نقش نیرنگی نداشت</p></div>
<div class="m2"><p>اینقدر روشن سواد عبرتم از چشم خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست ایمن خانهٔ آیینه از آفات زنگ</p></div>
<div class="m2"><p>دستگاه خواب چندین غفلتم از چشم خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غیر موهومی دلیل مرکز آرام نیست</p></div>
<div class="m2"><p>می‌گشاید ذره راه خلوتم ازچشم خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نُه فلک را یک قفس می‌بیند انداز نگاه</p></div>
<div class="m2"><p>تا کجاها در فشار وسعتم از چشم خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون شرر هر گه درین محفل نظر وا می‌کنم</p></div>
<div class="m2"><p>می‌زند چشمک وداع فرصتم از چشم خویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ناز هستی در نیاز آباد حسن آسوده است</p></div>
<div class="m2"><p>نیست بی‌سیر نگاهت فطرتم از چشم خویش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یا رب این ‌گلشن تماشاخانهٔ نیرنگ کیست</p></div>
<div class="m2"><p>کرد چون آیینه پنهان حیرتم از چشم خویش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خواه دریا نقش بندم خواه شبنم‌ گل ‌کنم</p></div>
<div class="m2"><p>رفتنی پیداست در هر صورتم از چشم خویش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>امتحان آگهی بیدل سراپایم گداخت</p></div>
<div class="m2"><p>همچو شمع‌ افکند آخر همتم از چشم خویش</p></div></div>