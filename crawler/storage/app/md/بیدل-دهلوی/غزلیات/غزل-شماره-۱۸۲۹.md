---
title: >-
    غزل شمارهٔ ۱۸۲۹
---
# غزل شمارهٔ ۱۸۲۹

<div class="b" id="bn1"><div class="m1"><p>اشکم قدم آبله فرسا ننهد پیش</p></div>
<div class="m2"><p>تا رفتن دل پای تقاضا ننهد پیش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل سجده فروش سرکویی است کز آن جا</p></div>
<div class="m2"><p>خاکم همه‌گر آب شود پا ننهد پیش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کیفیت یادت ز خودم می‌برد آخر</p></div>
<div class="m2"><p>این جرعه محال است‌ که مینا ننهد پیش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حیرانی ما صفحهٔ صد رنگ بیان است</p></div>
<div class="m2"><p>آیینه بساط لب‌ گویا ننهد پیش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما و نم اشکی و سجود سر راهی</p></div>
<div class="m2"><p>تسلیم وفا تحفه به هرجا ننهد پیش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روشن نتوان ‌کرد سواد خط هستی</p></div>
<div class="m2"><p>تا نسخهٔ عبرت پر عنقا ننهد پیش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما بیخبران سر به‌گریبان جنونیم</p></div>
<div class="m2"><p>مجنون قدم از دامن صحرا ننهد پیش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پروانهٔ نیرنگ سحرگاه ندارد</p></div>
<div class="m2"><p>مشتاق تو آینهٔ فردا ننهد پیش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جز سوختن از داغ‌، حضوری نتوان یافت</p></div>
<div class="m2"><p>آن به‌که‌کسی آینهٔ ما ننهد پیش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در راه تو دل را ز پرافشانی رنگم</p></div>
<div class="m2"><p>ساز قدمی هست مبادا ننهد پیش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن جاکه بود تیغ تو خضر ره تسلیم</p></div>
<div class="m2"><p>آن‌ کیست ‌که چون شمع سر از پا ننهد پیش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همت خجل است از هوس دست فشاندن</p></div>
<div class="m2"><p>کز چرخ ثری تا به ثریا ننهد پیش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حرصت همه‌گر قطره تقاضاست حذرکن</p></div>
<div class="m2"><p>تاکاسهٔ در یوزهٔ دریا ننهد پیش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مفت است غنا چشمی اگر سیر توان کرد</p></div>
<div class="m2"><p>زین بیش‌ کسی نعمت دنیا ننهد پیش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بیدل‌، شمرد بند گریبان ندامت</p></div>
<div class="m2"><p>آن دست‌ که در خدمت دلها ننهد پیش</p></div></div>