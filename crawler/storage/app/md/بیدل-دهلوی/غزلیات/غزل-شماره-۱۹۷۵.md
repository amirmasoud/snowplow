---
title: >-
    غزل شمارهٔ ۱۹۷۵
---
# غزل شمارهٔ ۱۹۷۵

<div class="b" id="bn1"><div class="m1"><p>بی ‌تو در هر جا جنون جوش ندامت بوده‌ام</p></div>
<div class="m2"><p>همچو دریا عضو عضو خویش بر هم سوده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون زمین زبن بیش نتوان بردبار وهم بود</p></div>
<div class="m2"><p>دوش هرکس زیر باری رفت من فرسوده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خیالت حسرتی دارم به روی‌ کاروبس</p></div>
<div class="m2"><p>همچو دل یک صفحهٔ رنگ امید اندوده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روزگار بی تمیزی خوش‌ که مانند نگاه</p></div>
<div class="m2"><p>می‌روم از خویش و می‌دانم همان آسوده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سودها مزد زیان من‌ که چون مینای می</p></div>
<div class="m2"><p>هر چه از خودکاستم بر بیخودی افزوده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسته‌ام چشم از خود و سیر دو عالم می‌کنم</p></div>
<div class="m2"><p>این چه پرواز است یارب در پر نگشوده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه قطع وادی امیدگامی هم نداشت</p></div>
<div class="m2"><p>حسرت آگاهست از راهی‌ که من پیموده‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسکه دارد پاس بیرنگی بهار هستی‌ام</p></div>
<div class="m2"><p>عمرها شد در لباس رنگم و ننموده‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیستم آگه چه دارد خلوت یکتایی‌اش</p></div>
<div class="m2"><p>اینقدر دانم‌که آنجا هم همین من بوده‌ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست بیدل باکم از درد خمار عافیت</p></div>
<div class="m2"><p>صندلی در پرده دارد دست بر هم سوده‌ام</p></div></div>