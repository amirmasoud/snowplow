---
title: >-
    غزل شمارهٔ ۸۱۴
---
# غزل شمارهٔ ۸۱۴

<div class="b" id="bn1"><div class="m1"><p>شب‌که شور بلبل ما ریشه درگلزار داشت</p></div>
<div class="m2"><p>بوی‌ گل در غنچه رنگ ناله در منقار داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نغمه‌ جولا‌ن صید نیرنگ‌ که‌ زین‌ صحرا گذشت</p></div>
<div class="m2"><p>ترکش تیر بتان فریاد موسیقار داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخصت یک جنبش مژگان نداد آگاهی‌ام</p></div>
<div class="m2"><p>حیرت اینحا خواب یا از دیدهای بیدار داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عقدهٔ محرومیِ کس فکر جمعیت مباد</p></div>
<div class="m2"><p>تا پریشان بود دل‌، بویی ز زلف یار داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داغ بی‌دردی نشاند، آخر به خاک تیره‌ام</p></div>
<div class="m2"><p>بود پر چتر گل‌، تا شمع در پا خار داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر همه‌ کفر است نتوان سر ز همواری‌ کشید</p></div>
<div class="m2"><p>سبحه را دیدیم طوف حلقهٔ زنار داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عجز هم‌کافی‌ست‌ هرجا مقصد از خود رفتن‌ است</p></div>
<div class="m2"><p>سایه ‌هستی تا عدم یک لغزشی هموار داشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صفحه‌ای آتش زدیم آیینه‌ها پرداختیم</p></div>
<div class="m2"><p>سوختن ‌چندین ‌چراغان چشمک ‌دیدار داشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ب‌ی‌گل صد انجمن بی‌پرده بود اما چه سود</p></div>
<div class="m2"><p>التفات رنگ ما را درپس دیوار داشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نارسابی صد خیال هرزه انشا کند</p></div>
<div class="m2"><p>طینت بیکار، ما را بیشتر در کار داشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عمرها شد چون‌گهر تهمت‌کش بی‌دردی‌ام</p></div>
<div class="m2"><p>یاد ایامی‌ که چشمم یک دو شبنم‌وار داشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آسمانی از کف خاک اختراع غفلت است</p></div>
<div class="m2"><p>بیدل از فخری ‌که ما دارپم باید عار داشت</p></div></div>