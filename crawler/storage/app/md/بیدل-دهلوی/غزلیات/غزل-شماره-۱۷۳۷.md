---
title: >-
    غزل شمارهٔ ۱۷۳۷
---
# غزل شمارهٔ ۱۷۳۷

<div class="b" id="bn1"><div class="m1"><p>از لب خامش زبان واماندهٔ کام است و بس</p></div>
<div class="m2"><p>بال از پرواز چون ماند آشیان دام است و بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرکز تسخیر دل جز دیده نتوان یافتن</p></div>
<div class="m2"><p>گوش‌مینا حلقه‌ای گر دارد آن جام است وبس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نفس باقی‌ست نتوان بست بال احتیاج</p></div>
<div class="m2"><p>این غناهایی‌که ما داربم ابرام است و بس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نشان ‌کعبهٔ مقصود آگه نیستم</p></div>
<div class="m2"><p>اینقدر دانم که هستی‌ساز احرام است و بس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وادی امکان ندارد دستگاه وحشتم</p></div>
<div class="m2"><p>هر طرف جولان‌ کند نظاره یک گام است و بس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسته است از موی چینی صورتم نقاش صنع</p></div>
<div class="m2"><p>صبح ایجادم همان ‌گل‌ کردن شام است و بس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دستگاه ما و من چون صبح برباد فناست</p></div>
<div class="m2"><p>صحن این‌ کاشانه‌ها یکسر لب بام است و بس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کاش از خجلت شرارم برنمی‌آمد ز سنگ</p></div>
<div class="m2"><p>سوختم از شرم آغازی که انجام است و بس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برپر عنقا تو هر رنگی‌که می‌خواهی ببند</p></div>
<div class="m2"><p>صورت آیینهٔ هستی همین نام است و بس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیش از این نتوان به افسون محبت زیستن</p></div>
<div class="m2"><p>داغم از اندیشهٔ وصلی‌ که پیغام است و بس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پختگی دیگ سخن را باز می‌دارد ز جوش</p></div>
<div class="m2"><p>تا خموشی ‌نیست بیدل مدعا خام‌ است و بس</p></div></div>