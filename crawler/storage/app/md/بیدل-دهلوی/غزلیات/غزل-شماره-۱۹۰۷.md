---
title: >-
    غزل شمارهٔ ۱۹۰۷
---
# غزل شمارهٔ ۱۹۰۷

<div class="b" id="bn1"><div class="m1"><p>نام شاهان کز نگین گل کرده کر و فر به چنگ</p></div>
<div class="m2"><p>عبرتی بیرون چکیده‌ست از فشار چشم تنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صدر استغنای یار آمادهٔ تعظیم ماست</p></div>
<div class="m2"><p>یک قدم‌ گر بگذرپم از چوب دربانان ننگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دهر بی‌باک‌ست اما قابل بیداد کیست</p></div>
<div class="m2"><p>همت از مینا طلب درکوه بسیار است سنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فضل اگر رهبر بود اوهام انوار هداست</p></div>
<div class="m2"><p>ابر رحمت خضر می‌رویاند از صحرای بنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا اثر چون ناله از صید اجابت نگذرد</p></div>
<div class="m2"><p>پر برون می‌آرد اینجا سعی منقار خدنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از هوس عمریست چون آیینه مژگان بسته‌ایم</p></div>
<div class="m2"><p>کم نگردد از سر ما سایهٔ دیوار زنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاک می‌لیسد دم بیدستگاهی لاف مرد</p></div>
<div class="m2"><p>سرمه آهنگ است در آب تنک هوی نهنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرمی آغوش بیرنگی برودت مایه نیست</p></div>
<div class="m2"><p>همچو بوی‌گل چه شد زیر پرم نگرفت رنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشم بدمست‌ که زد بر سنگ مینای مرا</p></div>
<div class="m2"><p>کز غبارم تا قیامت صوت خیزاند ترنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>امتحان هستی از دل رونق تحقیق برد</p></div>
<div class="m2"><p>از نفس‌کردیم آخر خانهٔ آیینه تنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آسمان بیدل ندانم تا کجا می‌راندم</p></div>
<div class="m2"><p>این فلاخن می‌زند عمریست از دورم به سنگ</p></div></div>