---
title: >-
    غزل شمارهٔ ۲۷۹۶
---
# غزل شمارهٔ ۲۷۹۶

<div class="b" id="bn1"><div class="m1"><p>ما را نه غروری‌ست نه فرّی نه ‌کلاهی</p></div>
<div class="m2"><p>خاکیم به ‌زیر قدم خویش نگاهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنجا که قناعت‌ کند ایجاد تسلی</p></div>
<div class="m2"><p>گرم است سرکوه به زیر پرکاهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر دولت بیدار ننازم چه خیال‌ست</p></div>
<div class="m2"><p>خوابیده بهم بخت من و چشم سیاهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر صد چمن هستی‌ام افسانهٔ نازست</p></div>
<div class="m2"><p>خواب عدم و سایهٔ مژگان گیاهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بردهٔ دل تا چه ‌کشد سعی تأمل</p></div>
<div class="m2"><p>چون خامه زنالم رسنی هشته‌ به چاهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا رب تو تن آسانی جهدم نپسندی</p></div>
<div class="m2"><p>می‌خواندم افسون نفس سوخته گاهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زبن دشت سبکتازی فرصت ندمانید</p></div>
<div class="m2"><p>گردی‌ که توان بست به پیشانی آهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آخر چو غبار نفس از هرزه دویها</p></div>
<div class="m2"><p>رفتیم به باد و ننشستیم به راهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرد تری از جبههٔ شبنم نتوان برد</p></div>
<div class="m2"><p>در آینهٔ ما عرقی ‌کرده نگاهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بید‌ل شدم و رَستم از اوهام تعین</p></div>
<div class="m2"><p>آیینه شکستن به بغل داشت کلاهی</p></div></div>