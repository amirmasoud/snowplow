---
title: >-
    غزل شمارهٔ ۲۰۰۱
---
# غزل شمارهٔ ۲۰۰۱

<div class="b" id="bn1"><div class="m1"><p>تا کجا بوس کف پایت شود ارزانی‌ام</p></div>
<div class="m2"><p>همچو موج آواره می‌گردد خط پیشانی‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بال و پر گم کرده‌ام در آشیان بیخودی</p></div>
<div class="m2"><p>چون دماغ عندلیب از بوی گل توفانی‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در عدم هم داشت استغنای حسن بی نشان</p></div>
<div class="m2"><p>چون شرار سنگ داغ چشمکی پنهانی‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عالمی گم کرده‌ام در گرد تکرار نفس</p></div>
<div class="m2"><p>نسخه‌ها بر باد داد این یک ورق گردانی‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چار سوی دهر جنس جلوه‌ها بسیار داشت</p></div>
<div class="m2"><p>تخته شد هر جا دکانی بود از حیرانی‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شبههٔ هستی به چندین رنگ داغم می‌کند</p></div>
<div class="m2"><p>وانما تا کیستم جز خاک اگر می‌دانی‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هیچ کس یارب گرفتار کمال خود مباد</p></div>
<div class="m2"><p>چون گهر بر سر فتاد از شش جهت غلتانی‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دامن تشریف اقبال نگه کوتاه نیست</p></div>
<div class="m2"><p>نه فلک پوشد قبا گر یک مژه پوشانی‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فقرم از تشویش چندین آرزوها باز داشت</p></div>
<div class="m2"><p>بی تکلف هیچ گنجی نیست در ویرانی‌ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>داشتم با خار خار طبع مجنون نسبتی</p></div>
<div class="m2"><p>بر سر راهی که لیلی پا نهد بنشانی‌ام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جان فدای خنجر نازی که در اندیشه‌اش</p></div>
<div class="m2"><p>هر کجا باشم شهیدم‌، بسملم‌، قربانی‌ام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هیچ کس نشکافت بیدل پردهٔ تحقیق من</p></div>
<div class="m2"><p>چون فلک پوشیده چشم عالم عریانی‌ام</p></div></div>