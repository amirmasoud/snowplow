---
title: >-
    غزل شمارهٔ ۱۷۵۹
---
# غزل شمارهٔ ۱۷۵۹

<div class="b" id="bn1"><div class="m1"><p>دل به‌کام تست چندی خرمی اظهار باش</p></div>
<div class="m2"><p>ساغری داری شکست رنگ را معمار باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فیضها دارد سخن بر معنی باریک پیچ</p></div>
<div class="m2"><p>گر دل آسوده خواهی عقدهٔ این نار باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر چه از وصلش به یکرنگی نیامیزد دلت</p></div>
<div class="m2"><p>گر همه جان باشد از اندیشه‌اش بیزار باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا حضور چشم و مژگان یابی از هر خار وگل</p></div>
<div class="m2"><p>چون نگه درهرکجا پا می‌نهی هموار باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیچکس تهمت نشان داغ بی‌نفعی مباد</p></div>
<div class="m2"><p>چتر شاهی‌گر نباشی سایهٔ دیوار باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ننگ تعطیل از غم بیحاصلی نتوان‌ کشید</p></div>
<div class="m2"><p>سودن دستی نبازی جهدکن درکار باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نقش پای رفتگان مخمور می‌آید به چشم</p></div>
<div class="m2"><p>یعنی ای وامانده در خمیازهٔ رفتار باش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مانع آزادگان پست و بلند دهر نیست</p></div>
<div class="m2"><p>ناله از خود می‌رود، گو ششجهت ‌کهسار باش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر تسلسل ختم شد دور غرور سبحه‌ات</p></div>
<div class="m2"><p>یک دو ساغر محو عشرتخانهٔ خمّار باش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرزه تازی تا به‌کی‌ گامی به‌گرد خویش ‌گرد</p></div>
<div class="m2"><p>جهد بر مشق تو خطی می‌کشد پرگار باش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر قدر مژگان‌گشایی جلوه در آغوش تست</p></div>
<div class="m2"><p>ای نگاهت مفت فرصت طالب دیدار باش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عاقبت بیدل ز چشم خویش باید رفتنت</p></div>
<div class="m2"><p>ذره هم کم نیست‌، تا باشی همین مقدار باش</p></div></div>