---
title: >-
    غزل شمارهٔ ۶۵۲
---
# غزل شمارهٔ ۶۵۲

<div class="b" id="bn1"><div class="m1"><p>تا غبارخط برآن حسن صفا پیرا نشست</p></div>
<div class="m2"><p>یک جهان امید در خاکستر سودا نشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داغ سودای تو دود انگیخت از بنیاد دل</p></div>
<div class="m2"><p>گرد برمی‌خیزد از جایی‌که نقش‌پا نشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حیرت ما دستگاه انتظار عالمی‌ست</p></div>
<div class="m2"><p>هرکه شد خاک سر راهت به چشم ما نشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حسن در جوش عرق خفت از ترددهای ناز</p></div>
<div class="m2"><p>آب این‌گوهرز شوخی بر رخ دریا نشست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پرگران خیزیم از سعی ضعیفیها مپرس</p></div>
<div class="m2"><p>نقش‌‌‌سنگی‌کردگل تمثال ما هرجا نشست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فیض عزلت عالمی را در بغل می‌پرورد</p></div>
<div class="m2"><p>مردمک در سایهٔ مژگان فلک‌پیما نشست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سربلندی خواهی از وضع ادب غافل مباش</p></div>
<div class="m2"><p>نشئه برمی‌خیزد از جوشی‌که در صهبا نشست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیرگردیدی دگر با دل‌گرانجانی مکن</p></div>
<div class="m2"><p>پنبه‌ات تا چند خواهد بر سر مینانشست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در دل ما چون شرارکاغذ آتش زده</p></div>
<div class="m2"><p>داغ هم یک‌لحظه نتوانست بی‌پروا نشست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یک جهان موهومی از آثار ما پر می‌زند</p></div>
<div class="m2"><p>ای فنا مشتاق باید در خیال ما نشست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حسرت دل را زمینگیری نمی‌گردد علاج</p></div>
<div class="m2"><p>ناله‌در سیر است بیدل‌کوه‌اگر ازپانشست</p></div></div>