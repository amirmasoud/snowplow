---
title: >-
    غزل شمارهٔ ۱۲۰۱
---
# غزل شمارهٔ ۱۲۰۱

<div class="b" id="bn1"><div class="m1"><p>ما راکه نفس آینه پرداخته باشد</p></div>
<div class="m2"><p>تدبیر صفا حیرت بی‌ساخته باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرداست که زیر سپر خاک نهانیم</p></div>
<div class="m2"><p>گو تیغ تو هم به سپهر آخته باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تسلیم سرشتیم رعونت چه خیال است</p></div>
<div class="m2"><p>مو تا به ‌کجا گردنش افراخته باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با طینت ظالم چه‌ کند ساز تجرّد</p></div>
<div class="m2"><p>ماری به هوس پوستی انداخته باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شور طلب از ما به فنا هم نتوان برد</p></div>
<div class="m2"><p>خاکستر عاشق قفس فاخته باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی‌ بوی‌ گلی نیست غبار نفس امروز</p></div>
<div class="m2"><p>یاد که در اندیشهٔ ما تاخته باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلدار گذشت و خبر از دل نگرفتیم</p></div>
<div class="m2"><p>این آینه‌ای نیست که نگداخته باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از شرم نثار تو به این هستی موهوم</p></div>
<div class="m2"><p>رنگی که ندارم چقدر باخته باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیدل به هوس دامنت ازکف نتوان داد</p></div>
<div class="m2"><p>ای کاش کسی قدر تو نشناخته باشد</p></div></div>