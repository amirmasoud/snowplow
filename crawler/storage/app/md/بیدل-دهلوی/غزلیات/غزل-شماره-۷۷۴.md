---
title: >-
    غزل شمارهٔ ۷۷۴
---
# غزل شمارهٔ ۷۷۴

<div class="b" id="bn1"><div class="m1"><p>پیش چشمی‌که نورعرفان نیست</p></div>
<div class="m2"><p>گر بود آسمان نمایان نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمرها شد، دمیده است آفاق</p></div>
<div class="m2"><p>بی‌لباسی هنوز عریان نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمع راگر به فکرخویش سری‌ست</p></div>
<div class="m2"><p>تاکف پاش جزگریبان نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقشبند خیال دور مباش</p></div>
<div class="m2"><p>گل چه داردکزین‌گلستان نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باید از نقد اعتبارگذشت</p></div>
<div class="m2"><p>جنس بازار عبرت ارزان نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برفلک هم خم است دوش هلال</p></div>
<div class="m2"><p>ناتوانی کشیدن آسان نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نرگستان عبرتیم همه</p></div>
<div class="m2"><p>چشم ا‌زخود بپوش مژگان نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاجزی خضر وادی ادب است</p></div>
<div class="m2"><p>پای خوابیده جز به دامان نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا نفس از تپش نیاساید</p></div>
<div class="m2"><p>جمع‌گردیدن دل امکان نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خجلتی چیده‌اید برچینید</p></div>
<div class="m2"><p>خودفروشان‌! زمانه دکان نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سجده را مفت عافیت شمرید</p></div>
<div class="m2"><p>جبهه‌سایی کف پشیمان نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کام عیش از صفای دل طلبید</p></div>
<div class="m2"><p>خانه آتش زدن چراغان نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شرم‌دار از طلب‌که بر در خلق</p></div>
<div class="m2"><p>سیلیی هست اگر خوری نان نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گه بخور ای طمع‌که نان خسان</p></div>
<div class="m2"><p>هضم ناگشته باب دندان نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بیدل امروز در مسلمانان</p></div>
<div class="m2"><p>همه‌چیز است لیک ایمان نیست</p></div></div>