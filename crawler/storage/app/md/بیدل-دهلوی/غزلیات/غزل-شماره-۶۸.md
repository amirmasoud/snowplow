---
title: >-
    غزل شمارهٔ ۶۸
---
# غزل شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>پرتو آهی ز جیبت‌گل نکرد ای دل چرا</p></div>
<div class="m2"><p>همچو شمع‌کشته‌بی‌نوری درین‌محفل چرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشت‌خون خود چوگل باید به‌روی خویش ریخت</p></div>
<div class="m2"><p>بی‌ادب آلوده‌سازی دامن قاتل چرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک صد صحرا زدی آب از عرقهای تلاش</p></div>
<div class="m2"><p>راه جولان هوس‌کامی نکردی‌گل چرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منزلت عرض حضوراست ومقامت اوج قرب</p></div>
<div class="m2"><p>نور خورشیدی به خاک تیره ای مایل چرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سعی آرامت قفس فرسودة ابرام‌کرد</p></div>
<div class="m2"><p>سر نمی‌دزدی زمانی در پر بسمل چرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون سلیمان هم‌گره بر باد نتوانست زد</p></div>
<div class="m2"><p>ای حباب این سرکشی بر عمر مستعجل چرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست از جیب تو بیرون‌گوهر مقصود تو</p></div>
<div class="m2"><p>بی‌خبر سر می‌زنی چون موج بر ساحل چرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جلوه‌گاه حسن معنی خلوت لفظ است و بس</p></div>
<div class="m2"><p>طالب لیلی نشیند غافل ازمحمل چرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا به‌کی بی‌مدعا چون شمع باید رفتنت</p></div>
<div class="m2"><p>جادهٔ خود را نسازی محو در منزل چرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر دو عالم هر مژه برهم زدن خط می‌کشی</p></div>
<div class="m2"><p>نیست یک‌دم نقش خویش از صفحه‌ات زایل چرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جود اگر در معرض احسان تغافل پیشه نیست</p></div>
<div class="m2"><p>می‌درد حاجت گریبان از لب سایل چرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گوهر عرض حباب آیینه‌دار حیرت است</p></div>
<div class="m2"><p>ای طلسم دل عبث‌گل‌کرده‌ای بیدل چرا</p></div></div>