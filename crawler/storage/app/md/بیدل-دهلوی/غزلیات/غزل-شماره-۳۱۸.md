---
title: >-
    غزل شمارهٔ ۳۱۸
---
# غزل شمارهٔ ۳۱۸

<div class="b" id="bn1"><div class="m1"><p>باز آب شمشیرت از بهار جوشی‌ها</p></div>
<div class="m2"><p>داد مشت خونم را یاد گل‌فروشی‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناله تا نفس دزدید من به سرمه خوابیدم</p></div>
<div class="m2"><p>کرد شمع این محفل داغم از خموشی‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا تغافل از عالم یا ز خود نظر بستن</p></div>
<div class="m2"><p>زین دوپرده بیرون نیست ساز عیب‌پوشی‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مایه‌دار هستی را لاف ما و من ننگ است</p></div>
<div class="m2"><p>بی‌بضاعتان دارند عرض خودفروشی‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاهدی نمی‌دانم تقویی نمی‌خواهم</p></div>
<div class="m2"><p>سینه صافیی دارم نذر دُردنوشی‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساز محفل هستی پر گسستن آهنگ است</p></div>
<div class="m2"><p>از نفس که می‌خواهد عافیت سروشی‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محرم فنا بیدل زیر بار کسوت نیست</p></div>
<div class="m2"><p>شعله جامه‌ای دارد از برهنه‌دوشی‌ها</p></div></div>