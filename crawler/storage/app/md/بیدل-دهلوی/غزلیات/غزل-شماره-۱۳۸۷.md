---
title: >-
    غزل شمارهٔ ۱۳۸۷
---
# غزل شمارهٔ ۱۳۸۷

<div class="b" id="bn1"><div class="m1"><p>عاقبت شرم امل بر غفلت ما می‌زند</p></div>
<div class="m2"><p>ربشه‌پردازی به خواب دانه‌ها پا می‌زند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شش جهت کیفیت اسرار دل‌گل‌کرده است</p></div>
<div class="m2"><p>رنگ می جام دگر بیرون مینا می‌زند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خانمان تنگی ندارد گر جنون دزد نفس</p></div>
<div class="m2"><p>خودسری بر آتشت دامان صحرا می‌زند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا کجا جمعیت دل نقش بندد آسمان</p></div>
<div class="m2"><p>عمرها شد خجلت‌ گوهر به دریا می‌زند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دماغ خاکساری هیچکس آگاه نیست</p></div>
<div class="m2"><p>آبله در زیر پا جام ثریا می‌زند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همنوای عبرتی درکار دارد درد دل</p></div>
<div class="m2"><p>ناله درکهسار بر هر سنگ خود را می‌زند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی‌گداز از طبع ما رفع‌کدورت مشکل است</p></div>
<div class="m2"><p>در حقیقت شیشه‌گر صیقل به خارا می‌زند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>احتیاجی نیست‌گر شرم طلب افتد به دست</p></div>
<div class="m2"><p>بی‌حیاییها در چندین تقاضا می‌زند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جست‌وجوی خلق مقصد در قدم دارد تلاش</p></div>
<div class="m2"><p>هرچه رفتار است بر نقش‌ کف پا می‌زند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صانع اسراری از تحقیق خود غافل مباش</p></div>
<div class="m2"><p>جز زبانت نیست آن بالی‌ که عنقا می‌زند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر نوا کز انجمن بالد ز دل باید شنید</p></div>
<div class="m2"><p>ساز دیگر نیست مطرب زخمه بر ما می‌زند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شوخی تقدیر تمهید شکست رنگ ماست</p></div>
<div class="m2"><p>قلقل خود سنگ بر سامان مینا می‌زند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زین هوس‌هایی که بیدل در تخیل چیده‌ایم</p></div>
<div class="m2"><p>یأس اگر بر دل نزد امروز، فردا می‌زند</p></div></div>