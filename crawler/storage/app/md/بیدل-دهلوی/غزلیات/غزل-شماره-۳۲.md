---
title: >-
    غزل شمارهٔ ۳۲
---
# غزل شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>به رنگ غنچه سودای خطت پیچیده دلها را</p></div>
<div class="m2"><p>رک‌گل رشتهٔ شیرازه شد جمعیت ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p> خرامت بال شوقم داد در پرواز حیرانی</p></div>
<div class="m2"><p>که‌چون قمری قدح در چشم‌دارم سرو مینا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p> نگه شد شمع فانوس خیال از چشم پوشیدن</p></div>
<div class="m2"><p>فنا مشکل که از عاشق برد ذوق تماشا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p> درین محفل سراغ گوشهٔ امنی نمی‌یابم</p></div>
<div class="m2"><p>چو شمع آخرگریبان می‌کنم نقش‌کف پا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p> کف خاکی ندارم قابل تعمیر خودداری</p></div>
<div class="m2"><p>جنون افشاند بر ویرانه‌ام دامان صحرا را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p> به غیر از نیستی لوح عدم نقشی نمی‌بندد</p></div>
<div class="m2"><p>اگر خواهی نگردی جلوه‌گر آیینه‌کن ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p> ندارد حال ما اندیشهٔ مستقبل دیگر</p></div>
<div class="m2"><p>که‌گم‌کردیم در آغوش دی‌، امروز و فردا را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p> نه از موج نسیم است اینقدرها جوش بیتابی</p></div>
<div class="m2"><p>تب شوق‌کسی در رقص دارد نبض دریا را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p> خموشی غیر افسودن چه‌گل ریزد به دامانت</p></div>
<div class="m2"><p>اگر آزاده‌ای با ناله کن پیوند اعضا را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p> اقامت تهمتی در محفل‌کم فرصت هستی</p></div>
<div class="m2"><p>چو عکس ازخانهٔ آیینه بیرون‌گرم‌کن جا را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p> مآل شعله هم‌داغ است گرآسودگی خواهی</p></div>
<div class="m2"><p>به‌صدگردن مده ازکف جبین سجده فرسا را </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نشانها نیست غیراز نام آن هم تا بی بیدل</p></div>
<div class="m2"><p> جهانی دیده‌ای‌، بشمار نقش بال عنقا را</p></div></div>