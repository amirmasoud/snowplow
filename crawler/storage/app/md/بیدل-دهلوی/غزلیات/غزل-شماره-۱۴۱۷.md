---
title: >-
    غزل شمارهٔ ۱۴۱۷
---
# غزل شمارهٔ ۱۴۱۷

<div class="b" id="bn1"><div class="m1"><p>اگر معنی خامشی ‌گل کند</p></div>
<div class="m2"><p>لب غنچه تعلیم بلبل ‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بساط جهان جای آرام نیست</p></div>
<div class="m2"><p>چرا کس وطن بر سر پل‌ کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درین انجمن مفلسان خامشند</p></div>
<div class="m2"><p>صراحی خالی چه قلقل ‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قبا کن در بن باغ‌،جیب طرب</p></div>
<div class="m2"><p>که از لخت دل غنچه فرگل‌ کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زبان را مکن پر فشان طلب</p></div>
<div class="m2"><p>مبادا چراغ حیا گل‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مکش سر ز پستی‌ که آواز آب</p></div>
<div class="m2"><p>ترقی بقدر تنزل‌ کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه سیل است یارب دم تیغ او</p></div>
<div class="m2"><p>که چون بگذرد از سرم پل‌ کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من‌ و یاد حسنی که در حسرتش</p></div>
<div class="m2"><p>جگر دامن ناله پرگل کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز رمز دهانش نباید اثر</p></div>
<div class="m2"><p>عدم هم به خود گر تامل ‌کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز بیداد آن چشم نتوان‌ گذشت</p></div>
<div class="m2"><p>دلی را که او خون کند مل کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز بس قهر و لطفش همه خوش‌اداست</p></div>
<div class="m2"><p>نگه می‌کند گر تغافل ‌کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دلت بی‌دماغ‌ست بیدل مباد</p></div>
<div class="m2"><p>به تعطیل‌، حکم توکل‌کند</p></div></div>