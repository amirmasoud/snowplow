---
title: >-
    غزل شمارهٔ ۲۹۲
---
# غزل شمارهٔ ۲۹۲

<div class="b" id="bn1"><div class="m1"><p>بی‌دماغی با نشاط از بس که دارد جنگ‌ها</p></div>
<div class="m2"><p>باده گردانده‌ست بر روی حریفان رنگ‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غافلند ارباب جاه از پستی اقبال خویش</p></div>
<div class="m2"><p>زیر پا بوده‌ست صدرآرایی اورنگ‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وادی عشق است اینجا منزل دیگر کجاست</p></div>
<div class="m2"><p>جز نفس در آبله دزدیدن فرسنگ‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی‌نیازی از تمیز کفر و دین آزاد بود</p></div>
<div class="m2"><p>از کجا جوشید یارب اختراع ننگ‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاهدان‌، از شانه پاس ریش باید داشتن‌!</p></div>
<div class="m2"><p>داء ثعلب بی‌پیامی نیست زین سر چنگ‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا نفس باقی‌ست باید با کدورت ساختن</p></div>
<div class="m2"><p>در کمین آینه آبی‌ست وقف زنگ‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرب و نرمی هرچه باشد مغتنم باید شمرد</p></div>
<div class="m2"><p>آب و روغن چون پر طاووس دارد رنگ‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرچه از تحقیق خوانی بشنو و خاموش باش</p></div>
<div class="m2"><p>ساز ما بیرون تار افکنده است آهنگ‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آخر این کهسار یک آیینه دل خواهد شدن</p></div>
<div class="m2"><p>شیشه افتاده‌ست در فکر شکست سنگ‌ها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل اسباب طرب تنبیه آگاهی‌ست‌، لیک</p></div>
<div class="m2"><p>انجمن پر غافل است از گوشمال چنگ‌ها</p></div></div>