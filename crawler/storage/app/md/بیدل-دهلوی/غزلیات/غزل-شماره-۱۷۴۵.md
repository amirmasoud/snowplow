---
title: >-
    غزل شمارهٔ ۱۷۴۵
---
# غزل شمارهٔ ۱۷۴۵

<div class="b" id="bn1"><div class="m1"><p>دل قیامت می کند از طبع ناشادم مپرس</p></div>
<div class="m2"><p>بیستون یک ناله می‌گردد ز فرهادم مپرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نام هم مفت است‌، عنقا بشنو و خاموش باش</p></div>
<div class="m2"><p>صد عدم از هستی آن سویم ز ایجادم مپرس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>محفل‌آرای حضورم خلوت نسیان اوست</p></div>
<div class="m2"><p>گو فراموشم نخواهی هیچش از یادم مپرس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پهلوی‌خودمی‌خورم چون شمع‌و ازخود می‌روم</p></div>
<div class="m2"><p>رهنورد وادی تسلیمم از زادم مپرس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تهمت تشویش نتوان بر مزاج سایه بست</p></div>
<div class="m2"><p>خواب امنی دارم از عجز خدادادم مپرس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا مژه در جنبش آید عافیت خاکستر است</p></div>
<div class="m2"><p>شمع بزم یأسم از اشک شررزادم مپرس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو طاووسم به‌ چندین رنگ محو جلوه‌ای</p></div>
<div class="m2"><p>نقش دامم دیدی از نیرنگ صیادم مپرس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کس در این محفل زبان‌دان چراغ‌کشته نیست</p></div>
<div class="m2"><p>از خموشی سرمه گردیدم ز فریادم مپرس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آب‌ در آیینه بیدل حرف زنگار است و بس</p></div>
<div class="m2"><p>سیل اگر گردی سراغ کلفت‌آبادم مپرس</p></div></div>