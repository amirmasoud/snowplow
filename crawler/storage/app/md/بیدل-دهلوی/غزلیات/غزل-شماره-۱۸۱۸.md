---
title: >-
    غزل شمارهٔ ۱۸۱۸
---
# غزل شمارهٔ ۱۸۱۸

<div class="b" id="bn1"><div class="m1"><p>ای خیال آوارهٔ نیرنگ هوش</p></div>
<div class="m2"><p>تا توانی در شکست رنگ ‌کوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا نفس باقیست ما و من بجاست</p></div>
<div class="m2"><p>شمع بی‌کشتن نمی‌گردد خموش‌</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زندگی در ننگ هستی مردنست</p></div>
<div class="m2"><p>خاک‌گرد و، عیب ما و من بپوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زبن خمستان گرمی دل برده‌اند</p></div>
<div class="m2"><p>همچو می با خون خود چندی بجوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از جراحت‌زار دل غافل مباش</p></div>
<div class="m2"><p>رنگها دارد دکان گلفروش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق اگر نبود هوس هم عالمی‌ست</p></div>
<div class="m2"><p>نیست خون دل ‌گوارا، می بنوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاک من بر باد رفت و خامشم</p></div>
<div class="m2"><p>همچو صبحم در نفس خون شد خروش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تر دماغان از مخالف ایمنند</p></div>
<div class="m2"><p>گاه خشکی باد می‌پیچد به ‌گوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یارب از مستی نلغزد پای من</p></div>
<div class="m2"><p>اشک مینا خانه‌ای دارد به دوش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زندگانی نشئهٔ وهمش رساست</p></div>
<div class="m2"><p>تا نمی‌میری نمی‌آیی به هوش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر لباس سایه از دوش افکنی</p></div>
<div class="m2"><p>می‌کند عریانیت خورشید پوش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یأس بر جا ماند و فرصت ها گذشت</p></div>
<div class="m2"><p>امشب ما نیست جز اندوه دوش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا مگر بیدل دلی آری به دست</p></div>
<div class="m2"><p>در تواضع همچو زلف یار کوش</p></div></div>