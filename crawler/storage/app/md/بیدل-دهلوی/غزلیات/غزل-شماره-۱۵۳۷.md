---
title: >-
    غزل شمارهٔ ۱۵۳۷
---
# غزل شمارهٔ ۱۵۳۷

<div class="b" id="bn1"><div class="m1"><p>خواهش از ضبط نفس‌ گر قدمی پیش شود</p></div>
<div class="m2"><p>ساغر همت جم ‌کاسهٔ درویش شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکه قدر پس زانو نشناسد چون اشک</p></div>
<div class="m2"><p>پایمال قدم هرزه‌دو خویش شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌کشد خون امید از دل حسرت‌کش ما</p></div>
<div class="m2"><p>سینهٔ هر که ز تیغ ستمی ریش شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لذت وصل تو از کام تمنا نرود</p></div>
<div class="m2"><p>هر سر مو به تنم‌ گر به مثل نیش شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست دور از اثر غیرت ابروی‌کجت</p></div>
<div class="m2"><p>جوهرآینه درتیغ ستمکیش شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم ما حلقه به ‌گوش است به نقش قدمی</p></div>
<div class="m2"><p>که به راه تو ز ما یک دو قدم پیش شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرصت ناز غنیمت شمر ای شوخ‌، مباد</p></div>
<div class="m2"><p>حسن تابد سرالفت ز خط و ریش شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آب یاقوت زآتش نتوان فرق نمود</p></div>
<div class="m2"><p>اختلاط ار همه بیگانه بود خویش شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>راحت‌اندیش مباشید که در وادی عشق</p></div>
<div class="m2"><p>وحشت آرام شود آهو اگر میش شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتگو کم‌ کن اگر عافیتت منظور است</p></div>
<div class="m2"><p>بحر هم می‌رود از خود چو هوا بیش‌شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نکشی پای ز دامان تغافل‌ که شرار</p></div>
<div class="m2"><p>رفته باشد ز نظر تا قدم‌اندیش شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رشتهٔ سازکرم نغمه ندارد بیدل</p></div>
<div class="m2"><p>گرنه مضراب قبولش لب درویش شود</p></div></div>