---
title: >-
    غزل شمارهٔ ۶۹۸
---
# غزل شمارهٔ ۶۹۸

<div class="b" id="bn1"><div class="m1"><p>دل از ندامت هستی‌، مکدر ا‌فتاده‌ست</p></div>
<div class="m2"><p>دگر ز یاس مگو خاک بر سر افتاده‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین بساط‌، تنزه کجا، تقدس‌کو</p></div>
<div class="m2"><p>مسیح رفته و نقش سم خر افتاده‌ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرو به باغ‌که از خنده‌کاری‌گلها</p></div>
<div class="m2"><p>درین هوسکده رسم حیا برافتاده‌ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فلک شکوه برآ، از فروتنی مگذر</p></div>
<div class="m2"><p>بلندی سر این بام بر در افتاده‌ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هرطرف نگری خودسری جنون دارد</p></div>
<div class="m2"><p>جهان‌خطی‌ست که بیرون‌مسطر افتاده‌ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به غیر چوب زمینگیری از خران نرود</p></div>
<div class="m2"><p>عصاکجاست‌که واعظ ز منبر افتاده‌ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نرفت شغل‌گرفتاری از طبیعت خلق</p></div>
<div class="m2"><p>قفس شکسته به آرایش پر افتاده‌ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی به منع خودآرایی‌ات ندارد کار</p></div>
<div class="m2"><p>بیا که خانهٔ آیینه بی‌در افتاده‌ست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سرشک آینه نگذاشت در مقابل آه</p></div>
<div class="m2"><p>ز بی‌نمی چقدر چشم ما تر افتاده‌ست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به عافیت چه خیال است طرف بسش ما</p></div>
<div class="m2"><p>مریض عشق چوآتش به بسترافتاده‌ست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فسانهٔ دل جمع از چه عالم افسون بود</p></div>
<div class="m2"><p>محیط در عرق سعی گوهر افتاده‌ست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>توهم به حیرت ازبن بزم صلح‌کن بیدل</p></div>
<div class="m2"><p>جنون حسن به آیینه‌ها درافتاده‌ست</p></div></div>