---
title: >-
    غزل شمارهٔ ۲۵۰۰
---
# غزل شمارهٔ ۲۵۰۰

<div class="b" id="bn1"><div class="m1"><p>ترشح مایه‌ای ناز دلی را محو احسان‌کن</p></div>
<div class="m2"><p>تبسم می‌کند آیینه برگیر و نمکدان کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طربگاه جهان رنگ استعداد می‌خواهد</p></div>
<div class="m2"><p>در اینجا هر قدر آغوش‌گردی گل به دامان کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکست خودسری تسخیر صد حرص و هوس دارد</p></div>
<div class="m2"><p>جهانی‌گبر از یک‌کشتن آتش مسلمان‌کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهار جلوه‌ای‌ گر اندکی از خود برون آیی</p></div>
<div class="m2"><p>چو تخم از ربشه بیرون دادنی تحریک مژگان‌ کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گوشم از شبستان عدم آواز می‌آید</p></div>
<div class="m2"><p>که چون طاووس اگر از بیضه وارستی چراغان کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نگاه یار هر مژگان زدن درس رمی دارد</p></div>
<div class="m2"><p>تو هم ای بیخبر از خود رو و گرد غزالان‌ کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر در سایهٔ مژگان مورت جا دهد فرصت</p></div>
<div class="m2"><p>به ‌راحت واکش و آرایش چتر سلیمان ‌کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به دریا قطره ی گمگشته از هر موج می‌جوشد</p></div>
<div class="m2"><p>فرو رو در گداز دل جهانی را گریبان‌ کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به جرم بی‌گناهی سوختن هم حیرتی دارد</p></div>
<div class="m2"><p>به رنگ شمع از هر عضو خویش آیینه عریان کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نفس دزدیدنت کیفیت دل نقش می‌بندد</p></div>
<div class="m2"><p>گهر انگاره‌ای داری به ضبط موج سوهان کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز خاک رفتگان بر دیده مشتی آب زن بیدل</p></div>
<div class="m2"><p>بدین تدبیر دشوار دو عالم بر خود آسان کن</p></div></div>