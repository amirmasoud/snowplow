---
title: >-
    غزل شمارهٔ ۱۱۷۶
---
# غزل شمارهٔ ۱۱۷۶

<div class="b" id="bn1"><div class="m1"><p>آن فتنه ‌که آفاقش شور من و ما باشد</p></div>
<div class="m2"><p>دل نام بلایی هست یارب به‌کجا باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بابد به سراب اینجا از بحر تسلی بود</p></div>
<div class="m2"><p>نزدیک خود انگارید گر دورنما باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راحت‌طلبی ما را چون‌ شمع به خاک افکند</p></div>
<div class="m2"><p>این آرزوی نایاب شاید تنه پا باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویند ندارد دهر جزگرد عدم چیزی</p></div>
<div class="m2"><p>آن جلوه که ناپیداست باید همه جا باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی‌پیرهن از یوسف بویی نتوان بردن</p></div>
<div class="m2"><p>عریانی اگر باشد در زبر قبا باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبر وبم جرات نیست درساز حباب اینجا</p></div>
<div class="m2"><p>غرق عرق شرمیم ما را چه صدا باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کم نیست کمال فقر ز دام هوس رستن</p></div>
<div class="m2"><p>بگذار که این پرواز در بال هما باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اندیشهٔ‌خودبینی از وضع ادب دور است</p></div>
<div class="m2"><p>آیینه نمی‌باشد آنجا که حیا باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با طبع رعونت‌کیش زنهار نخواهی ساخت</p></div>
<div class="m2"><p>باید سرگردن خواه از دوش جدا باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اشکی‌که دمید از شمع غیرت ته‌پایش ریخت</p></div>
<div class="m2"><p>کاش آب رخ ما هم خاک ذر ما باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تحقیق ندارد کار با شبهه‌ تراشیها</p></div>
<div class="m2"><p>در آینهٔ خورشید تمثال خطا باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اجزای جهان کل کیفیت‌ کل دارد</p></div>
<div class="m2"><p>هر قطره که در دریاست باشد همه تا باشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هرچند قبولت نیست بیدل زطلب مگسل</p></div>
<div class="m2"><p>بالقوهٔ حاجتها در دست دعا باشد</p></div></div>