---
title: >-
    غزل شمارهٔ ۱۲۰۰
---
# غزل شمارهٔ ۱۲۰۰

<div class="b" id="bn1"><div class="m1"><p>جمعیت از آن دل‌که پریشان تو باشد</p></div>
<div class="m2"><p>معموری آن شوق که ویران تو باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمری‌ست دل خون شده بی‌تاب ‌گدازی‌ست</p></div>
<div class="m2"><p>یارب شود آیینه و حیران تو باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد چرخ توان ریخت ز پرواز غبارم</p></div>
<div class="m2"><p>آن روزکه در سایهٔ دامان تو باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داغم‌که چرا پیکر من سایه نگردید</p></div>
<div class="m2"><p>تا در قدم سرو خرامان تو باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشاق بهار چمنستان خیالند</p></div>
<div class="m2"><p>پوشیدگی آیینه عریان تو باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر نقش قدم خمکده عالم نازیست</p></div>
<div class="m2"><p>هرجا اثر لغزش مستان تو باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نظاره ز کونین به کونین نپرداخت</p></div>
<div class="m2"><p>پیداست که حیران تو حیران تو باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مپسند که دل در تپش یأس بمیرد</p></div>
<div class="m2"><p>قربان تو قربان تو قربان تو باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر جوش تبسمکده ناز بهار است</p></div>
<div class="m2"><p>چینی‌که شکن‌پرور دامان تو باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در دل تپشی می خلد از شبههٔ هستی</p></div>
<div class="m2"><p>یارب‌که نفس جنبش مژگان تو باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل سخنت نیست جز انشای تحیر</p></div>
<div class="m2"><p>کو آینه تا صفحهٔ دیوان تو باشد</p></div></div>