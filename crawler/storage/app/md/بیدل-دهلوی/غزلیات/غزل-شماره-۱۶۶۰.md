---
title: >-
    غزل شمارهٔ ۱۶۶۰
---
# غزل شمارهٔ ۱۶۶۰

<div class="b" id="bn1"><div class="m1"><p>نه جام باده‌ شناسم نه کاسهٔ طنبور</p></div>
<div class="m2"><p>جز آنقدرکه جهان یکسر است و چندین شرر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندانم آنهمه‌ کوشش برای چیست‌که چرخ</p></div>
<div class="m2"><p>ز انجم آبله‌دار است چون کف مزدور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هجوم آبلهٔ اشک پر به سامان است</p></div>
<div class="m2"><p>درین حدیقه همین خوشه می‌دهد انگور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خرده‌بینی غماز عشق می‌نازیم</p></div>
<div class="m2"><p>که تا به دست سلیمان رسانده‌ام پی مور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو غنچه‌گلشن پوشیده حالتی دارم</p></div>
<div class="m2"><p>به بیضه شوخی عنقاست در پر عُصفور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز اهل قال توان بوی درد دل بردن</p></div>
<div class="m2"><p>به جای نغمه اگر خون‌ کشد رگ طنبور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان طربگه دیدار و ما جنون‌نظران</p></div>
<div class="m2"><p>پی غبار خیالی رسانده‌ایم به طور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کشیده‌اند در این معرض پشیمانی</p></div>
<div class="m2"><p>عسل تلافی نیش از طبیعت زنبور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز موج درخور جهدش شکست می‌بالد</p></div>
<div class="m2"><p>به عجز پیش نرفته‌ست اعتبار غرور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>توان معاینه کرد از فتیله‌سازی موج</p></div>
<div class="m2"><p>که بحر راست چه مقدار در جگر ناسور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو شمع موم به جز سوختن چه اندوزد</p></div>
<div class="m2"><p>کسی‌ که ماند ز شهد حقیقتی مهجور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز یار دورم و صبری ندارم ای ناصح</p></div>
<div class="m2"><p>دل شکسته همین ناله می‌کند مغرور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز سردمهری ایام دم مزن بیدل</p></div>
<div class="m2"><p>مباد.چون سحرت از نفس دمد کافور</p></div></div>