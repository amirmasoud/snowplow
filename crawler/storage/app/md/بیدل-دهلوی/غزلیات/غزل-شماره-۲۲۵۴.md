---
title: >-
    غزل شمارهٔ ۲۲۵۴
---
# غزل شمارهٔ ۲۲۵۴

<div class="b" id="bn1"><div class="m1"><p>از عزت و خواری نه امید است نه بیمم</p></div>
<div class="m2"><p>من‌گوهر غلتان خودم اشک یتیمم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل نیست بساطی که فضولی رسد آنجا</p></div>
<div class="m2"><p>طور ادبم سرمهٔ آوازکلیمم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرچند سر و برک متاع دگرم نیست</p></div>
<div class="m2"><p>زین گرد نفس قافلهٔ ملک عظیمم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نعمت بی‌خواست به کفران نتوان زد</p></div>
<div class="m2"><p>محتاج نی‌ام لیک‌کریم است‌کریمم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از سایهٔ گم گشته مجویید سیاهی</p></div>
<div class="m2"><p>شستندبه سر چشمهٔ‌خورشید گلیمم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بالیدن من تا ندرد جامهٔ آفاق</p></div>
<div class="m2"><p>باریکتر از ریشهٔ تحقیق جسیمم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشمی نگشودم‌که به زخمی نتپیدم</p></div>
<div class="m2"><p>عمریست چو عبرت به همین کوچه مقیمم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با تیغ طرف گشته‌ام از دست سلامت</p></div>
<div class="m2"><p>چون شمع به هر جا سر خویش است غنیمم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی‌درد سری نیست سحر نیز درین باغ</p></div>
<div class="m2"><p>صندل به جبین می‌وزد از دور نسیمم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون خوشهٔ‌گندم‌چه‌دهم‌عرض تبسم</p></div>
<div class="m2"><p>از خاک پیام‌آور دلهای دو نیمم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل نی‌ام امروز خجالت‌کش هستی</p></div>
<div class="m2"><p>چون چرخ سر افکندهٔ ادوار قدیمم</p></div></div>