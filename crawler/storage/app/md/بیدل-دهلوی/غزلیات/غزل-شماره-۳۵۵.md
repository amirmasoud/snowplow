---
title: >-
    غزل شمارهٔ ۳۵۵
---
# غزل شمارهٔ ۳۵۵

<div class="b" id="bn1"><div class="m1"><p>ای جلوهٔ تو سرشکن شان آفتاب</p></div>
<div class="m2"><p>خندیده مطلع تو به دیوان آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیغام عجز من ز غرورت شنیدنی‌ست</p></div>
<div class="m2"><p>مکتوب سایه دارم و عنوان آفتاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در هرکجا نگاه پر افشان روز بود</p></div>
<div class="m2"><p>شوق تو د‌اشت اینهمه سامان آفتاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب محو انتظارتو بودم دمید صبح</p></div>
<div class="m2"><p>گشتم به یاد روی تو قربان آفتاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون سایه پایمال خس و خار بهتر است</p></div>
<div class="m2"><p>آن سرکه نیست‌گرم ز احسان آفتاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از چرخ سفله‌کام چه جویم‌که این خسیس</p></div>
<div class="m2"><p>هر شب نهان‌کند به بغل نان آفتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همت به جهد شبنم ما نازمی‌کند</p></div>
<div class="m2"><p>بستیم اشک خویش به مژگان آفتاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای لعل یار ضبط تبسم مروت است</p></div>
<div class="m2"><p>تا نشکنی به خنده نمکدان آفتاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون ماه نو ز شهرت رسوایی‌ام مپرس</p></div>
<div class="m2"><p>چاکی کشیده‌ام زگریبان آفتاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل به حسن مطلع نازش چسان رسیم</p></div>
<div class="m2"><p>ما راکه ذره ساخته حیران آفتاب</p></div></div>