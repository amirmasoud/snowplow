---
title: >-
    غزل شمارهٔ ۱۵۹۱
---
# غزل شمارهٔ ۱۵۹۱

<div class="b" id="bn1"><div class="m1"><p>چه شمع امشب در این محفل چمن‌پرداز می‌آید</p></div>
<div class="m2"><p>که آواز پر پروانه هم گلباز می‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نسیمی گویی از گلزار الفت باز می‌آید</p></div>
<div class="m2"><p>که مشت خاک من چون چشم در پرواز می‌آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من و نظاره حسنی که از بیگانه‌خویی‌ها</p></div>
<div class="m2"><p>در آغوش است و دور از یک نگاه انداز می‌آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز پیش‌آهنگی قانون حسرت‌ها چه می‌پرسی</p></div>
<div class="m2"><p>شکست از هرچه باشد از دلم آواز می‌آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پرافشان هوای کیستم یارب که در یادش</p></div>
<div class="m2"><p>نفس در پردهٔ اندیشه‌ام گل باز می‌آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز دریا، بازگشت قطره، ‌گوهر در گره دارد</p></div>
<div class="m2"><p>نیاز من ز طوف جلوهٔ او ناز می‌آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه حاجت مطرب دیگر طربگاه ‌محبت را</p></div>
<div class="m2"><p>که از یک دل تپیدن کار چندین ساز می‌آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز خود رفتن اگر مقصود باشد شعله ما را</p></div>
<div class="m2"><p>فسردن نیز دارد آنچه از پرواز می‌آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نفس دزدیده‌ام چون شمع و پنهان نیست داغ دل</p></div>
<div class="m2"><p>هنوز از خامشی بوی لب غماز می‌آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به اشکی فکر استقبال آهم می‌توان کردن</p></div>
<div class="m2"><p>که گردآلوده از فتح طلسم راز می‌آید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هنوز از سخت‌جانی این قدر طاقت گمان دارم</p></div>
<div class="m2"><p>که از خود می‌توانم رفت ا‌گر او باز می‌آید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فسون‌ساز غفلت گر نگردد پنبهٔ گوشت</p></div>
<div class="m2"><p>چو تار از دست برهم سوده هم آواز می‌آید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دل هر ذره خورشیدی‌ست اما جهد کو بیدل</p></div>
<div class="m2"><p>منم آیینه از دستت اگر پرداز می‌آید</p></div></div>