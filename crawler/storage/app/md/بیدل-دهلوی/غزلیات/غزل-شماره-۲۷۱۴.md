---
title: >-
    غزل شمارهٔ ۲۷۱۴
---
# غزل شمارهٔ ۲۷۱۴

<div class="b" id="bn1"><div class="m1"><p>نگه از مستی چشم تو با ساغر کند بازی</p></div>
<div class="m2"><p>حیا از رنگ تمکین تو با گوهر کند بازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر بیند هجوم خط به دور شکّر لعلش</p></div>
<div class="m2"><p>ز حسرت مور جوهر در دم خنجر کند بازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دوران تو گردون مهرهٔ سیاره می‌چیند</p></div>
<div class="m2"><p>بفرما چشم فتان را که تا ابتر کند بازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به بزم بیقراری مشرب عیش شرر دارم</p></div>
<div class="m2"><p>من و اشکی ‌که چون اطفال با اخگر کند بازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر تحریر خط دلفریبش سر کنم بیدل</p></div>
<div class="m2"><p>زبان ‌کلک خشک من به مشک تر کند بازی</p></div></div>