---
title: >-
    غزل شمارهٔ ۱۹۸۹
---
# غزل شمارهٔ ۱۹۸۹

<div class="b" id="bn1"><div class="m1"><p>اشک شمعی بود یک عمر آبیار دانه‌ام</p></div>
<div class="m2"><p>سوختن خرمن کنید از حاصل پروانه‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیره‌بختی فرش من آشفتگی اسباب من</p></div>
<div class="m2"><p>حلقهٔ زلف سیاه کیست یارب خانه‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خرمن بیحاصلان را برق حاصل می‌شود</p></div>
<div class="m2"><p>سیل هم از بیکسی‌ گنجیست در وبرانه‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ذوق چتر شاهی و بال هما منظورکیست‌؟</p></div>
<div class="m2"><p>کم نگردد سایهٔ مو از سر دیوانه‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفته‌ام عمریست زین‌ گلشن به یاد جلوه‌ای</p></div>
<div class="m2"><p>گوش نه بر بوی‌ گل تا بشنوی افسانه‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در زراعتگاه چرخ مجمری همچون سپند</p></div>
<div class="m2"><p>برگ دود آرد برون گر سبز گردد دانه‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روزگاری شد که چون چشم ندامت پیشگان</p></div>
<div class="m2"><p>باده‌ها ازگردش خود می‌کشد پیمانه‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سیل را تا بحر ساز محملی در کار نیست</p></div>
<div class="m2"><p>می‌برد شوقت به دوش لغزش مستانه‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قبله خوانم یا پیمبر یا خدا یا کعبه است</p></div>
<div class="m2"><p>اصطلاح عشق بسیار است و من دیوانه‌ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عمرها شد دست من دامان زلفی می‌کشد</p></div>
<div class="m2"><p>جای آن دارد که از انگشت روید شانه‌ام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شوخی‌اش از طرز پروازم تماشا کردنی‌ست</p></div>
<div class="m2"><p>شمع رنگ بسته در بال و پر پروانه‌ام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون حباب از نشئهٔ سودای تحقیقم مپرس</p></div>
<div class="m2"><p>بسکه می‌بالم به خود پر می‌شود پیمانه‌ام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عافیتها در نظر دارم ز وضع نیستی</p></div>
<div class="m2"><p>چشم بر هم بسته واکرده‌ست راه خانه‌ام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون نفس بیدل کلید آرزوها داشتم</p></div>
<div class="m2"><p>قفل وسواس دل آخر کرد بی‌دندانه‌ام</p></div></div>