---
title: >-
    غزل شمارهٔ ۴۸
---
# غزل شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>حسابی نیست با وحشت جنون‌کامل ما را</p></div>
<div class="m2"><p>مگرلیلی به دوش جلوه بندد محمل ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محبت بسکه بوداز جلوه مشتاقان این محفل</p></div>
<div class="m2"><p>به‌تعمیرنگه چون شمع برد آب وگل ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندارد گردن تسلیم بیش از سایهٔ مویی</p></div>
<div class="m2"><p>عبث بر ما تنک‌کردند تیغ قاتل ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غبار احتیاج امواج دریا خشک می‌سازد</p></div>
<div class="m2"><p>عیارکم مگیرید آبروی سایل ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صفای دل به حیرت بست نقش پردهٔ هستی</p></div>
<div class="m2"><p>فروغ شمع‌کام اژدها شد محفل ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ادبگاه وفا آنگه برافشانی، چه ننگ است این</p></div>
<div class="m2"><p>تپیدن خاک بر سرکرد آخر بسمل ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل از سعی امل بر وضع آرامیده می‌لرزد</p></div>
<div class="m2"><p>مبادا دوربینی جاده سازد منزل ما را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکست آرزو زین بیش نتوان درگره بستن</p></div>
<div class="m2"><p>گرانجانی ز هر سو بر دل ما زد دل ما را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز خشکیهای وضع عافیت تر می‌شود همت</p></div>
<div class="m2"><p>عرق ای‌کاش در دریا نشاند ساحل ما را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تمیز از سایه ممکن نیست فرق دود بردارد</p></div>
<div class="m2"><p>به روی شعله‌گر پاشی غبارکاهل ما را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حباب پوچ از آب گهر امیدها دارد</p></div>
<div class="m2"><p>خداوندا به حق دل ببخشا بیدل ما را</p></div></div>