---
title: >-
    غزل شمارهٔ ۲۲۰
---
# غزل شمارهٔ ۲۲۰

<div class="b" id="bn1"><div class="m1"><p>عمری‌ست‌گردگردش رنگ خودیم ما</p></div>
<div class="m2"><p>چون آسیا فلاخن سنگ خودیم ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دریاد زندگی به عدم ناز کنیم</p></div>
<div class="m2"><p>رنگ حنای رفته زچنگ خودیم ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرصت‌کجاست تا به تظلم جنون‌کنیم</p></div>
<div class="m2"><p>دنباله‌ای زگرد ترنگ خودیم ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فکر و وقار و خفت‌کس در خیال‌کیست</p></div>
<div class="m2"><p>کم نیست‌گرترازوی سنگ خودیم ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کو دور آسمان وکجا گردش زمان</p></div>
<div class="m2"><p>سرگشته‌های عالم بنگ خودیم ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از هم‌گذشته است پی‌کاروان عمر</p></div>
<div class="m2"><p>واماندهٔ شتاب و درنگ خودیم ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نخجیرگاه عجز رهایی‌کمند نیست</p></div>
<div class="m2"><p>هم خود ز رنگ جسته پلنگ خودیم ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای شمع‌، عافیتکده‌، تسلیم نیستی‌ست</p></div>
<div class="m2"><p>کشتی‌نشین‌کام نهنگ خودیم ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رسواییی به فطرت ناقص نمی‌رسد</p></div>
<div class="m2"><p>مجنون قبا ز جامهٔ تنگ خودیم ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از صنعت مصوررنگ حنا مپرس</p></div>
<div class="m2"><p>دلدارگل به دست فرنگ خودیم ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کس محرم ادبگه ناموس دل مباد</p></div>
<div class="m2"><p>جایی رسیده‌ایم‌که ننگ خودیم ما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا زنده‌ایم تاب وتب از ما نمی‌رود</p></div>
<div class="m2"><p>بیدل به دل خلیده خدنگ خودیم ما</p></div></div>