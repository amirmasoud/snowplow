---
title: >-
    غزل شمارهٔ ۱۵۳۸
---
# غزل شمارهٔ ۱۵۳۸

<div class="b" id="bn1"><div class="m1"><p>بر آستان تو تا جبهه نقش پا نشود</p></div>
<div class="m2"><p>حق نیاز به این سجده‌ها ادا نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز تیر‌ه بختی خود میل در نظر دارد</p></div>
<div class="m2"><p>به خاک پای تو هر دیده‌ای که وانشود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه ممکن است که در بوتهٔ گداز وفا</p></div>
<div class="m2"><p>د‌ل آب گردد و جام جهان‌نما نشود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برون سایهٔ‌گل خوابگاه شبنم نیست</p></div>
<div class="m2"><p>سرم به پای بتان خاک شد چرا نشود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>توان شد آینهٔ بحر عافیت چو حباب</p></div>
<div class="m2"><p>اگر غبار نفس سد راه ما نشود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا ز مرگ به خاطر غمی‌که هست این است</p></div>
<div class="m2"><p>که خاک‌گردم و دل محرم فنا نشود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز یار دوری و آسایش ای فلک مپسند</p></div>
<div class="m2"><p>که شبنم از برگل‌ خیزد و هوا نشود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل از غبار تعلق نمی‌توان برداشت</p></div>
<div class="m2"><p>نسیم وادی عبرت اگر عصا نشود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به داغ می‌کند آخر جنون خرامیها</p></div>
<div class="m2"><p>چو شمع به که کسی سربرهنه پا نشود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز چشم حرص یقین دارم اینقدر بیدل</p></div>
<div class="m2"><p>که خاک گور هم این زخم را دوا نشود</p></div></div>