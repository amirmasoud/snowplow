---
title: >-
    غزل شمارهٔ ۲۱۵۹
---
# غزل شمارهٔ ۲۱۵۹

<div class="b" id="bn1"><div class="m1"><p>در آن محفل‌ که‌ام من تا بگویم این و آن دارم</p></div>
<div class="m2"><p>جبین سجده فرسودی نیاز آستان دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طلسم ذرهٔ من بسته‌اند از نیستی اما</p></div>
<div class="m2"><p>به خورشیدیست کارم اینقدر بر خود گمان دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنای عجز تعمیرم چو نقش پا‌ زمینگیرم</p></div>
<div class="m2"><p>سرم بر خاک راهی بود اکنون هم همان دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نی‌ام محتاج عرض مدعا در بیزبانیها</p></div>
<div class="m2"><p>تحیر دارد اظهاری که پنداری زبان دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه خواهم جز دل صد پاره برگ ماحضر کردن</p></div>
<div class="m2"><p>غم او میهمان و من همین یک بیره‌پان دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرو کار شفق‌ با آفتاب آخر چه انجامد</p></div>
<div class="m2"><p>تو تیغی داری و من مشت خونی در میان دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلندیهای قصر نیستی را نیست پایانی</p></div>
<div class="m2"><p>که من چندانکه برمی آیم از خود نردبان دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگردی ای فسردن از کمین شعله‌ام غافل</p></div>
<div class="m2"><p>که درگرد شکست رنگ ذوق آشیان دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شرارم در زمین بی‌یقینی ریشه‌ها دارد</p></div>
<div class="m2"><p>اگر گویی ‌گلم هستم و گر خواهی خزان دارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گه از امید دلتنگم گهی با یأ‌س در جنگم</p></div>
<div class="m2"><p>خیال عالم بنگم نه این دارم نه آن دارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جناب‌کبریا آیینه است و خلق تمثالش</p></div>
<div class="m2"><p>من بیدل چه دارم تا از آن حضرت نهان دارم</p></div></div>