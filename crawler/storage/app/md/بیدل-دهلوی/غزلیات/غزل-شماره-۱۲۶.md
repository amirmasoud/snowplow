---
title: >-
    غزل شمارهٔ ۱۲۶
---
# غزل شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>گدازگوهر دل باده ناب است شبنم را</p></div>
<div class="m2"><p>نم چشم تحیرعالم آب است شبنم را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگردد جمع نوراگهی با ظلمت غفلت</p></div>
<div class="m2"><p>صفای دل نمک در دیدهٔ خواب است شبنم را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان آیینهٔ دلدار و حیرانی حجاب من</p></div>
<div class="m2"><p>چمن صد جلوه و نظاره نایاب است شبنم را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هرجا می‌روم در اشک نومیدی وطن دارم</p></div>
<div class="m2"><p>ز چشم خود جهان یک دشت سیلاب است شبنم را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگردی غافل ای اشک نیاز از ترک خودداری</p></div>
<div class="m2"><p>که بر دوش چکیدن سیر مهتاب است شبنم را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تماشا نیست کم، چشم هوس گر شرمناک افتد</p></div>
<div class="m2"><p>حیا آیینهٔ گلهای سیراب است شبنم را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گل اشکم اگر منظور جانان شد عجب نبود</p></div>
<div class="m2"><p>گذر در چشم خورشید جهانتاب است شبنم را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خط خوبان‌کمند غفلت اهل نظر باشد</p></div>
<div class="m2"><p>رگ‌گلهای این‌گلشن رگ خواب است شبنم را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فضولی می‌کنم در انتظار مهر تابانش</p></div>
<div class="m2"><p>گرفتم پرده بردارد،‌کجا تاب است شبنم را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به وصل گلرخان نتوان کنار عافیت جستن</p></div>
<div class="m2"><p>که درآغوش‌گل‌، خون جگرآب است شبنم را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ضعیفی تهمت چندین تعلق بست بر حالم</p></div>
<div class="m2"><p>ز پا افتادگی یک عالم اسباب است شبنم را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حیا بال هوس را مانع پرواز می‌گردد</p></div>
<div class="m2"><p>نگه در دیده بیدل موجهٔ آب است شبنم را</p></div></div>