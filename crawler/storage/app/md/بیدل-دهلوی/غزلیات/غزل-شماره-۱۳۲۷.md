---
title: >-
    غزل شمارهٔ ۱۳۲۷
---
# غزل شمارهٔ ۱۳۲۷

<div class="b" id="bn1"><div class="m1"><p>آن سخا کیشان ‌که بر احسان نظر واکرده‌اند</p></div>
<div class="m2"><p>ازگشاد دست و دل چشمی دگر واکرده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیر این‌ گلزار غیر از ماتم نظاره چیست</p></div>
<div class="m2"><p>دیده‌ها یکسر ز مژگان موی سر واکرده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد مژه پا خورد ربطش تا ترا بیدار کرد</p></div>
<div class="m2"><p>یک رگ‌خوابت به چندین نیشتر واکرده ا‌ند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وضع‌ مخمور ادب خفّت‌کش‌ خمیازه نیست</p></div>
<div class="m2"><p>یاد آغوشی که در موج گهر واکرده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیدلان را هرزه نفریبد غم دستار پوچ</p></div>
<div class="m2"><p>چون‌حباب این قوم سر راهم ز سر واکرده اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساز موجیم از رم و آرام ما غافل مباش</p></div>
<div class="m2"><p>این‌ کمرها جمله دامن بر کمر وا کرده‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نالهٔ ما زین چمن تمهید پرواز است و بس</p></div>
<div class="m2"><p>بلبلان منقار پیش از بال و پر واکرده‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عرض جوهر بر صفای آینه در بستن است</p></div>
<div class="m2"><p>غافل آن قومی‌‌که دکان هنر واکرده‌اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پرتو شمع حقیقت خارج‌ فانوس نیست</p></div>
<div class="m2"><p>شوخ‌چشمان ‌روزن‌سنگ از شرر واکرده اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>موی پیری ‌عبرت روز سیاه کس مباد</p></div>
<div class="m2"><p>آه از آن‌ شمعی‌ که چشمش ‌بر سحر وا کرده اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا نگردیدم دو تا قرب فنا روشن نشد</p></div>
<div class="m2"><p>از تلاش پیری‌ام یک حلقهٔ در واکرده اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ناتوانی بیدل از تشویش قدرت فارغ است</p></div>
<div class="m2"><p>عقده در بی‌ناخنیها بیشتر واکرده‌اند</p></div></div>