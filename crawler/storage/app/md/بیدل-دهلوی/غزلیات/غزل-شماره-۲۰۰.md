---
title: >-
    غزل شمارهٔ ۲۰۰
---
# غزل شمارهٔ ۲۰۰

<div class="b" id="bn1"><div class="m1"><p>هرجا روی ای ناله سلامی ببر ازما</p></div>
<div class="m2"><p>یادش دل ما برد به جای دگر از ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امید حریف نفس سست عنان نیست</p></div>
<div class="m2"><p>ما را برسانید به او پیشتر از ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل را فلک آخر به‌گدازی نپسندید</p></div>
<div class="m2"><p>هیهات چه برسنگ زد این شیشه‌گراز ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تاکی هوس آوارهٔ پرواز توان زیست</p></div>
<div class="m2"><p>یارب‌که جداکرد سر زیر پر از ما؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آیینه به بر غافل از آن جلوه دمیدیم</p></div>
<div class="m2"><p>جز ما نتوان یافت‌کسی را بتر از ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی‌پردگی آیینهٔ آثار غنا نیست</p></div>
<div class="m2"><p>عریانی ما برد کلا‌ه وکمر از ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوهر ز قناعت‌گره طبع محیط است</p></div>
<div class="m2"><p>ازکس دل پر نیست فلک را مگر از ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کس آینه بر طاق تغافل نپسندد</p></div>
<div class="m2"><p>از خود نگرفتی خبر ای بیخبر از ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما را ز درت جرأت دوری چه خیال است</p></div>
<div class="m2"><p>صد مرحله دوراست درین ره جگراز ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا حشر درین بزم محال است توان برد</p></div>
<div class="m2"><p>خلوت زتو و عالم بیرون در از ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عمری‌ست وفا ممتحن ناز و نیاز است</p></div>
<div class="m2"><p>نی تیغ ز دست تو جدا شد نه سر از ما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زحمتکش وهمیم چه ادبار و چه اقبال</p></div>
<div class="m2"><p>بیدل نتوان‌گفت شب از ما سحر از ما</p></div></div>