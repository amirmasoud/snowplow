---
title: >-
    غزل شمارهٔ ۱۷۷۹
---
# غزل شمارهٔ ۱۷۷۹

<div class="b" id="bn1"><div class="m1"><p>به ساز نیستی بسته‌ست شور ما و من بارش</p></div>
<div class="m2"><p>بهارت بلبلی دارد که شکل لاست منقارش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خجالت با دماغ بید مجنون بر نمی‌آید</p></div>
<div class="m2"><p>جهانی زحمت خم می‌کشد از دوش بی‌بارش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز آشوب غبار دهر یکسر سنگ می‌بارد</p></div>
<div class="m2"><p>تو ضبط شیشهٔ خودکن‌، پری خیز است‌ کهسارش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زحرف پوچ نتوان جز به بیمغزی علم‌ گشتن</p></div>
<div class="m2"><p>سر منصور باید پنبه بندد بر سر دارش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کمند حب جاه از خلق واگشتن نمی‌خواهد</p></div>
<div class="m2"><p>سلیمانی سری دارد که زنار است دستارش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صفا هم دام پا لغزی‌ست از عبرت مباش ایمن</p></div>
<div class="m2"><p>به سر غلتاند گوهر را غرور طبع هموارش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به میدانی‌که رخش عزم همت می‌کند جولان</p></div>
<div class="m2"><p>حیا از هر دو عالم می‌کشد دست عنان‌دارش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جفا با طینت مسرور عاشق بر نمی‌آید</p></div>
<div class="m2"><p>مگراز درد محرومی زپا بیرون خلد خارش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به رفع‌ کلفت غفلت غبار خود زپا بنشان</p></div>
<div class="m2"><p>شکست سایه دارد هر چه می‌افتد ز دیوارش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خیال بحر چندین موج گوهر در نظر دارد</p></div>
<div class="m2"><p>که می‌داند چه‌ها دیدند مشتاقان دیدارش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مجاز پوچ ما را از حقیقت باز می‌دارد</p></div>
<div class="m2"><p>به سیر نرگسستان غافلیم از چشم بیمارش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کبابم کرد اندوه جدایی هر چه را دیدم</p></div>
<div class="m2"><p>کسی یارب در این محفل نیفتد با نگه‌ کارش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به تعمیر دل تنگم‌ کسی دیگر چه پردازد</p></div>
<div class="m2"><p>طناب وسع همت پرگره بسته است معمارش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در این غفلتسرا بی عبرت آگاهی نمی‌باشد</p></div>
<div class="m2"><p>مژه تا پا نزد بر چشم ننمودند بیدارش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو تصویر هلال آخر به خجلت خاک شد بیدل</p></div>
<div class="m2"><p>ز ننگ ناتمامی بر نیامد خط پرگارش</p></div></div>