---
title: >-
    غزل شمارهٔ ۲۳۱۵
---
# غزل شمارهٔ ۲۳۱۵

<div class="b" id="bn1"><div class="m1"><p>پروانه شوم یا پر طاووس گشایم</p></div>
<div class="m2"><p>از عالم عنقا چه خیالست برآیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آب و گلم از جوهر نظاره سرشتند</p></div>
<div class="m2"><p>در چشم خیالست به چشم همه جایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سعی طلبم بیش شد از هر چه نه بنشست</p></div>
<div class="m2"><p>زین بعد مگر شوق برد رو به قفایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دامن دشتی‌ که نه راه است نه منزل</p></div>
<div class="m2"><p>عمریست‌ که محمل‌کش آواز درایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جوشیده‌ام از انجمن عبرت معشوق</p></div>
<div class="m2"><p>مشکل‌ که در آیینهٔ ‌کس جلوه نمایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ذرات جهان چشمک اسرار وصال است</p></div>
<div class="m2"><p>آغوش من اینست‌ که چشمی بگشایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سازم ادب آهنگ خیال نگه‌ کیست</p></div>
<div class="m2"><p>در انجمن سرمه نشسته‌ست صدایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با موج‌ گهر باخته‌ام دست و گریبان</p></div>
<div class="m2"><p>از دامن خود نیست برون لغزش پایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی‌پردگی معنی از آیینهٔ لفظ است</p></div>
<div class="m2"><p>فریاد که در ساز نگنجید نوایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>امید اجابت چقدر منفعلم‌ کرد</p></div>
<div class="m2"><p>امشب عرق آینهٔ دست دعایم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا غرهٔ افسون سعادت نتوان زیست</p></div>
<div class="m2"><p>بر سایهٔ خود بال فشانده است همایم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ساقی قدحی چند مشو مانع تکلیف</p></div>
<div class="m2"><p>شاید روم از یاد خود و باز نیایم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بیدل مکن آرام تمنا که در ایجاد</p></div>
<div class="m2"><p>بر باد نهادند چو پرواز بنایم</p></div></div>