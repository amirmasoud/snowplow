---
title: >-
    غزل شمارهٔ ۸۴
---
# غزل شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>گرکماندار خیالت در زه آرد تیر را</p></div>
<div class="m2"><p>هر بن مو چشم امیدی شود نخجیر را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاد رخسارت جبین فکر را آیینه ساخت</p></div>
<div class="m2"><p>حرف زلفت‌کرد سنبل رشتهٔ تقریر را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برنمی‌دارد عمارت خاک صحرای جنون</p></div>
<div class="m2"><p>خواهی آبادم‌کنی بر باد ده تعمیر را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مانع بیتابی آزادگان فولاد نیست</p></div>
<div class="m2"><p>ناله در وحشت گریبان می‌درّد زنجیر را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخت دشوارست پرداز شکست رنگ‌من</p></div>
<div class="m2"><p>بشکن ای نقاش اینجا خامهٔ تصویر را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>موج‌خون‌من‌که‌آتش داغ‌گرمیهای‌اوست</p></div>
<div class="m2"><p>می‌کند بال سمندر جوهر شمشیر را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون‌ره‌خوابیده زین‌خوابی‌که فیضش‌کم مباد</p></div>
<div class="m2"><p>تا به منزل برده‌ام سررشتهٔ تعبیر را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گربه‌این وجدست شور وحشت دیوانه‌ام</p></div>
<div class="m2"><p>داغ‌حیرت می‌کند چون‌نقش‌پا زنجیررا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پای تا سر دردم اما زحمت کس نیستم</p></div>
<div class="m2"><p>ناله‌ام در سینه خرمن می‌کند تأثیر را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تاکی ازغفلت به قید جسم فرساید دلت</p></div>
<div class="m2"><p>یک نفس بر باد ده این خاک دامنگیر را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صبح عشرتگاه هستی ازشفق آبستن است</p></div>
<div class="m2"><p>نیست جز خون‌گر بپالایدکسی این شیر را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دست از دنیا بدار و دامن آهی بگیر</p></div>
<div class="m2"><p>تا بدانی همچو بیدل قدر دار وگیر را</p></div></div>