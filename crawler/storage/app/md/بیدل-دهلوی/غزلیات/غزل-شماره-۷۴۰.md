---
title: >-
    غزل شمارهٔ ۷۴۰
---
# غزل شمارهٔ ۷۴۰

<div class="b" id="bn1"><div class="m1"><p>قانون ادب پرده در صورت و صدا نیست</p></div>
<div class="m2"><p>زین ساز مگو تا نفست سرمه نوا نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از هرچه اثر واکشی افسانه دلیل است</p></div>
<div class="m2"><p>سرمایهٔ این قافله جز بانگ درا نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر حرف ‌که آمد به زبان منفعلم ‌کرد</p></div>
<div class="m2"><p>کم جست ازین کیش خدنگی که خطا نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همت چقدر زیر فلک بال‌ گشاید</p></div>
<div class="m2"><p>پست است به حدی‌ که درین خانه هوا نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عمری‌ست که از ساز بد اندامی آفاق</p></div>
<div class="m2"><p>گر رشته و تابی‌ست به هم تنگ قبا نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما را تری جبهه به عبرت نرسانید</p></div>
<div class="m2"><p>جنس عرق سعی زدگان حیا نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی‌عجز رسا قابل رحمت نتوان شد</p></div>
<div class="m2"><p>دستی که بلندی رسدش باب دعا نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هشدار که در سایه‌ دیوار قناعت</p></div>
<div class="m2"><p>خوابی‌ست‌ که در خواب پر و بال هما نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>واماندهٔ عجزیم ز افسون تعلق</p></div>
<div class="m2"><p>گر دل نکشد رشته‌، نفس آبله‌پا نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ازجهل وخردتا هوس وعشق ومحبت</p></div>
<div class="m2"><p>جز ما چه متاعی‌ست‌ که در خانه‌ ما نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ما را کرم عام تو محتاج غنا کرد</p></div>
<div class="m2"><p>گر جلوه تغافل زند آیینه گدا نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جز معنی از آثار عبارت نتوان خواند</p></div>
<div class="m2"><p>گر غیر خدا فهم کنی غیر خدا نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر بی‌بصری را نکند محرم تحقیق</p></div>
<div class="m2"><p>آن دست حنا بسته ‌که جز رنگ حنا نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیدل رم فرصت چمن‌آراست در اینجا</p></div>
<div class="m2"><p>گل فکر اقامت چه‌ کند رنگ بجا نیست</p></div></div>