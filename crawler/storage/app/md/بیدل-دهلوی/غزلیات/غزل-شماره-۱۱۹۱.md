---
title: >-
    غزل شمارهٔ ۱۱۹۱
---
# غزل شمارهٔ ۱۱۹۱

<div class="b" id="bn1"><div class="m1"><p>هرچند دل از وصل قدح‌نوش نباشد</p></div>
<div class="m2"><p>رحمی ‌که زیاد تو فراموش نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حرفی که بود بی‌اثر ساز دعایت</p></div>
<div class="m2"><p>یارب به زبان ناید و در گوش نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جایی‌که به‌گردش زند انداز نگاهت</p></div>
<div class="m2"><p>چندان ‌که نظرکار کند هوش نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنجا که ادب قابل دیدارپرستی‌ست</p></div>
<div class="m2"><p>واکردن مژگان کم از آغوش نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دیر محبت که ادب آینه‌دارست</p></div>
<div class="m2"><p>خاموش به آن شعله‌ که خاموش نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گویند به صحرای قیامت سحری هست</p></div>
<div class="m2"><p>یارب ‌که جز آن صبح بناگوش نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلقی‌ست خجالت‌کش مخموری و مستی</p></div>
<div class="m2"><p>این خمکده را غیر عرق جوش نباشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سر تا قدم وضع حباب است خمیدن</p></div>
<div class="m2"><p>حمال نفس جز به چنین دوش نباشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیدل چه خیال است‌ کمال تو نهفتن</p></div>
<div class="m2"><p>آیینهٔ خورشید نمد پوش نباشد</p></div></div>