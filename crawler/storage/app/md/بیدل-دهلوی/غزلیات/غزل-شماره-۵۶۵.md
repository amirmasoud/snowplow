---
title: >-
    غزل شمارهٔ ۵۶۵
---
# غزل شمارهٔ ۵۶۵

<div class="b" id="bn1"><div class="m1"><p>عرق‌فشانی شبنم در این حدیقه‌ گواه است</p></div>
<div class="m2"><p>که هر طرف نگرد دیده انفعال نگاه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حساب سایه و خورشید هیچ راست نیاید</p></div>
<div class="m2"><p>متاع منتظران زنگ و حسن آینه‌خواه است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غبار دشت عدم را کدام فعل و چه طاعت</p></div>
<div class="m2"><p>ز ما اگر همه آهنگ سجده است ‌گناه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هرکجا اثر جلوه‌ات نقاب گشاید</p></div>
<div class="m2"><p>حقیقت دو جهان ماجرای برق و گیاه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز حال مردم چشمم توان معاینه کردن</p></div>
<div class="m2"><p>که در محیط غمت خانهٔ حباب سیاه است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سراغ عافیتی نیست در قلمرو امکان</p></div>
<div class="m2"><p>برای شعلهٔ ما درگذار خویش پناه است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طریق عالم عجزی سپرده‌ایم‌ که آنجا</p></div>
<div class="m2"><p>سر غرور چو نقش قدم گل سر راه است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز فقر شیفتهٔ جاه غیر مرگ چه فهمد</p></div>
<div class="m2"><p>که شمع را سر و برگ نفس به بند کلاه است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کتان نه‌ایم ولیکن ز بار منت عشرت</p></div>
<div class="m2"><p>بر آبگینهٔ ما سنگ به ز پرتو ماه است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>توان ز گردش رنگم به درد عشق رسیدن</p></div>
<div class="m2"><p>دل گداخته آبی به زیر این پر کاه است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو صبح در قفس زخم آرزوی تو دارم</p></div>
<div class="m2"><p>تبسمی‌ که غبار هزار قافله آه است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به‌محفلی‌که دهد سرمه‌ات صلای خموشی</p></div>
<div class="m2"><p>خروش ساز قیامت صدای تار نگاه است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به خانمان نکشد آرزوی الفت بیدل</p></div>
<div class="m2"><p>مثال وحشی ما را خیال آینه چاه است</p></div></div>