---
title: >-
    غزل شمارهٔ ۱۸
---
# غزل شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>نه طرح باغ و نه‌گلشن فکنده‌اند اینجا</p></div>
<div class="m2"><p>در آب آینه روغن فکنده‌اند اینجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غبار قافلهٔ عبرتی که پیدا نیست</p></div>
<div class="m2"><p>همه به دیدهٔ روشن فکناه‌اند اینجا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رسیده‌گیر به معراج امتیاز چو شمع</p></div>
<div class="m2"><p>همان سری که زگردن فکنده‌اند اینجا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جنون مکن‌که دلیران عرصهٔ تحقیق</p></div>
<div class="m2"><p>سپر ز خجلت جوشن فکنده‌اند اینجا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی‌ست حاصل و آفت به مزرعی‌که شبی</p></div>
<div class="m2"><p>ز دانه مور به خرمن فکنده‌اند اینجا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به صید خواهش دنیای دون دلیر متاز</p></div>
<div class="m2"><p>هزار مرد ز یک زن فکنده‌اند اینجا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر فسانه سلامت‌که خوابناکی چند</p></div>
<div class="m2"><p>غبار وادی ایمن فکنده‌اند این‌جا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نهفته است‌تلاش محیط موج‌گوهر</p></div>
<div class="m2"><p>یه روی آبله دامن فکنده‌اند اینجا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رموز دل نشود فاش بی‌چراغ یقین</p></div>
<div class="m2"><p>نظر به خانه ز روزن فکنده‌ا‌ند اینجا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مقیم زاویهٔ اتفاق تسلیمم</p></div>
<div class="m2"><p>بساط عافیت من فکنده‌اند اینجا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو شمع‌گردن دعوی چسان‌کشم بیدل</p></div>
<div class="m2"><p>سرم به دوش فکندن فکند‌ه‌اند اینجا</p></div></div>