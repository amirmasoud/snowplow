---
title: >-
    غزل شمارهٔ ۶
---
# غزل شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>جولان ما فسرد به زنجیر خواب پا</p></div>
<div class="m2"><p>واماندگی‌ست حاصل تعبیر خواب پا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ممنون غفلتیم‌که بی‌منت طلب</p></div>
<div class="m2"><p>ما را به ما رساند به شبگیر خواب پا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>واماندگی ز سلسلهٔ ما نمی‌رود</p></div>
<div class="m2"><p>چون جاده‌ایم یک رگ زنجیر خواب پا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در هر صفت تلافی غفلت غنیمت است</p></div>
<div class="m2"><p>تاوان ز چشم‌گیر به تقصیر خواب پا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نتوان به سعی آبله افسردگی‌کشید</p></div>
<div class="m2"><p>خشتی نچیده‌ایم به تعمیر خواب پا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اظهار غفلت طلبم‌کار عقل نیست</p></div>
<div class="m2"><p>نقاش عاجزست به تصویر خواب پا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آخر سری به عالم نورم کشیدن است</p></div>
<div class="m2"><p>غافل نی‌ام چو سایه ز شبگیر خواب پا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سامان آرمیدگی موج‌گوهریم</p></div>
<div class="m2"><p>ما را سری‌ست برخط تسخیرخواب پا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیدل دلت اگرهوس آهنگ منزل است</p></div>
<div class="m2"><p>ما و شکست‌کوشش وتدبیر خواب پا</p></div></div>