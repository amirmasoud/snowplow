---
title: >-
    غزل شمارهٔ ۱۱۶۹
---
# غزل شمارهٔ ۱۱۶۹

<div class="b" id="bn1"><div class="m1"><p>صبح شو ای‌شب‌که خورشید من‌اکنون می‌رسد</p></div>
<div class="m2"><p>عید مردم‌ گو برو عید من اکنون می‌رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بعد از اینم بی‌دماغ یاس نتوان زیستن</p></div>
<div class="m2"><p>دستگاه عیش جاوید من اکنون می‌رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌روم در سایه‌اش بنشینم و ساغرکشم</p></div>
<div class="m2"><p>نونهال باغ امید من اکنون می‌رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آرزو خواهدکلاه ناز برگردون فکند</p></div>
<div class="m2"><p>جام می در دست جمشید من اکنون می‌رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفع خواهدگشت بیدل شبههٔ وهم دویی</p></div>
<div class="m2"><p>صاحب اسرار توحید من اکنون می‌رسد</p></div></div>