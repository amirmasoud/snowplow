---
title: >-
    غزل شمارهٔ ۲۲۶
---
# غزل شمارهٔ ۲۲۶

<div class="b" id="bn1"><div class="m1"><p>باکمال اتحاد ازوصل مهجوریم ما</p></div>
<div class="m2"><p>همچو ساغر می به‌لب داریم و مخموریم ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p> پرتو خورشید جز در خاک نتوان ‌یافتن</p></div>
<div class="m2"><p> یک‌زمین و آسمان از اصل خود دوریم‌ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p> درتجلی سوختیم وچشم بینش وا نشد</p></div>
<div class="m2"><p> سخت پابرخاست جهل مامگرطوریم ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p> با وجود ناتوانی سر به‌گردون سوده‌ایم</p></div>
<div class="m2"><p> چون مه سرخط عجزیم ومغروریم ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p> تهمت حکم قضا را چاره نتوان یافتن</p></div>
<div class="m2"><p> اختیار از ماست چندانی‌که مجبوریم ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p> مفت ساز بندگی‌گر غفلت وگر آگهی</p></div>
<div class="m2"><p> پیش نتوان برد جزکاری‌که مأموریم ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بحر در آغوش و موج ما همان محوکنار</p></div>
<div class="m2"><p> کارها با عشق بی‌پرواست معذوریم ما</p></div></div>