---
title: >-
    غزل شمارهٔ ۶۵۵
---
# غزل شمارهٔ ۶۵۵

<div class="b" id="bn1"><div class="m1"><p>تازمستی غنچه برفرق چمن میناشکست</p></div>
<div class="m2"><p>رنگ ما هم ازترنج جام می صفرا شکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تنگنای شهر، تاب شهرت سودا نداشت</p></div>
<div class="m2"><p>گرد ما دیوانگان در دامن صحرا شکست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌رود بر باد عالم‌گر خموشان دم زنند</p></div>
<div class="m2"><p>رنگ صدگلشن به آه غنچه‌ای تنها شکست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیچ و تاب موج غیر از انقلاب بحر نیست</p></div>
<div class="m2"><p>چرخ رنگ خویش بامینای مایکجا شکست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صافی وحدت مکدرگشت‌کثرت جلوه‌کرد</p></div>
<div class="m2"><p>موج شد تمثال تا آیینهٔ دریا شکست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کیست دریابد عروج دستگاه بیخودی</p></div>
<div class="m2"><p>رنگ ما طرف‌کلاه ناز پر بالا شکست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>موج دریای ندامت امتحان آگهی‌ست</p></div>
<div class="m2"><p>صدمژه یک چشم مالیدن به‌چشم ما شکست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از فریب خاکساریهای خصم ایمن مباش</p></div>
<div class="m2"><p>سنگ تا شد مایل افتادگی مینا شکست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسکه عالم را به حسن خلق ممنون‌کرده‌ایم</p></div>
<div class="m2"><p>رنگ هم نتواند ازجرأت به روی ما شکست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باغ امکان یک‌گل آغوش فضا پیدا نکرد</p></div>
<div class="m2"><p>رنگها بریکدگرازتنگی این جا شکست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عمرها شد از دعاهای سحر شرمنده‌ام</p></div>
<div class="m2"><p>چین آهی داشتم در دامن شبها شکست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هرزه تاکی پیش پیش بحر باید تاختن</p></div>
<div class="m2"><p>موج ما از شرم‌در دامان‌گوهر پا شکست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پیش ازآن بیدل‌که هستی آشیان پیرا شود</p></div>
<div class="m2"><p>نام ما بال هوس در بیضهٔ عنقا شکست</p></div></div>