---
title: >-
    غزل شمارهٔ ۲۵۴۵
---
# غزل شمارهٔ ۲۵۴۵

<div class="b" id="bn1"><div class="m1"><p>دهر، توفان دارد از طبع جنون پیمای من</p></div>
<div class="m2"><p>قلقلی دزدیده است این بحر از مینای من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست خالی یک کف خاک از غبار وحشتم</p></div>
<div class="m2"><p>چون نفس می‌جوشد از هر دل تپیدنهای من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غنچه را جز شوخی رنگ آفتی دربار نیست</p></div>
<div class="m2"><p>خودنمایی می‌دهد آخر به باد اجزای من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر نفس‌ کز دل کشیدم خامشی افشاند بال</p></div>
<div class="m2"><p>می‌زند موج از زبان ماهیان دریای من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسکه افشردم قدم در خاک راه نیستی</p></div>
<div class="m2"><p>همچو شمع آخرسر من‌گشت نقش پای من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صافی دل در غبار عرض استعداد رفت</p></div>
<div class="m2"><p>موج می شد جوهر آیینهٔ مینای من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راه از خود رفتنم از شمع هم روشن‌تر است</p></div>
<div class="m2"><p>جاده پرداز است برق ناله در صحرای من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حسن هرجا جلوه‌گر شد عشق می‌آید برون</p></div>
<div class="m2"><p>عرض مجنون می‌دهد آیینهٔ لیلای من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا قیامت بایدم سرگشتهٔ پرواز بود</p></div>
<div class="m2"><p>دام دارد بر هوا صیاد بی‌پروای من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همچو برق آغوش از وحشت مهیا کرده‌ام</p></div>
<div class="m2"><p>طول صد عقبا امل صرفست بر پهنای من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پردهٔ تحقیق بیدل تا کجا خواهی شکافت</p></div>
<div class="m2"><p>عالمی دارد نهان کیفیت پیدای من</p></div></div>