---
title: >-
    غزل شمارهٔ ۱۵۷۸
---
# غزل شمارهٔ ۱۵۷۸

<div class="b" id="bn1"><div class="m1"><p>آفات از هوس به سرت هاله می‌شود</p></div>
<div class="m2"><p>این شعله‌ها ز دست تو جواله می‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زبن‌کاروان چه سودکه هرکس چونقش پا</p></div>
<div class="m2"><p>از سعی پیش تاخته دنباله می‌شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی‌شغل فتنه نیست چو نفس از فساد ماند</p></div>
<div class="m2"><p>چون قحبهٔ عجوز که دلاله می‌شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از محتسب بترس‌ که این فتنه‌زاده را</p></div>
<div class="m2"><p>چون وارسند دختر رز خاله می‌شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی ‌سحر نیست هیأ‌ت شیخ از رجوع خلق</p></div>
<div class="m2"><p>این خر تناسخی‌ست که گوساله می‌شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سوداییان بخت سیه را ترانه‌هاست</p></div>
<div class="m2"><p>طوطی هزار رنگ به بنگاله می‌شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما را قرینه دولت بیدار داده است</p></div>
<div class="m2"><p>صبحی‌که در شب‌، او شفق لاله می‌شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در وقت احتیاج‌، ز اظهار، شرم دار</p></div>
<div class="m2"><p>چون شد بلند دست دعا ناله می‌شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وامانده‌ام به راه تو چندانکه بر لبم</p></div>
<div class="m2"><p>چون شمع حرف آبله تبخاله می‌شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل به شیب نام حلاوت مبر که نخل</p></div>
<div class="m2"><p>دور اسث از ثمر چوکهن‌ ساله می‌شود</p></div></div>