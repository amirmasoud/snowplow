---
title: >-
    غزل شمارهٔ ۱۲۵۰
---
# غزل شمارهٔ ۱۲۵۰

<div class="b" id="bn1"><div class="m1"><p>تا پری به ‌عرض آمد موج شیشه عریان شد</p></div>
<div class="m2"><p>پیرهن ز بس بالید دهر یوسفستان شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جلوه‌اش جهانی را محو بیخودیها کرد</p></div>
<div class="m2"><p>آینه دکان بر چین جنس حیرت ارزان شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک من به یاد آورد چهره عرقناکش</p></div>
<div class="m2"><p>هچو بیضهٔ طاووس در عدم چراغان شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوشش زمینگیرم برعروج بینش تاخت</p></div>
<div class="m2"><p>خارپای شمعِ آخر دستگاه مژگان شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وحشتم درین محفل شوخی سپندی داشت</p></div>
<div class="m2"><p>تا قفس زدم آتش ناله‌ای پرافشان شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>انفعال هستی را من عیار افسوسم</p></div>
<div class="m2"><p>دست داغ سودن بود طبع اگر پشیمان شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>امتحان آفاتم رنگ طاقت دل ریخت</p></div>
<div class="m2"><p>آبگینه‌ام آخر از شکست سندان شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زین‌ چمن به هر رنگم سیر آگهی مفت‌ است</p></div>
<div class="m2"><p>داغ لاله هم‌کم نیست‌گر بهار نتوان شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سازگردن‌افرازی رنج هرزه‌گردی داشت</p></div>
<div class="m2"><p>سر به جیب دزدیدم پا مقیم دامان شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>داغ درد شو بیدل ‌کز گداز بی حاصل</p></div>
<div class="m2"><p>اشکها درین محفل ریشخند مژگان شد</p></div></div>