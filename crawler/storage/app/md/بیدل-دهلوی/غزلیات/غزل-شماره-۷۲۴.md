---
title: >-
    غزل شمارهٔ ۷۲۴
---
# غزل شمارهٔ ۷۲۴

<div class="b" id="bn1"><div class="m1"><p>سرو بهار جلوه قد دلستان‌ کیست</p></div>
<div class="m2"><p>پیغام فتنه‌، برق نگاه نهان کیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگذشته‌ست اگر ز دلم لشکر غمت</p></div>
<div class="m2"><p>داغ جگر، نشان پی‌ کاروان‌ کیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اندیشه‌ها به حسرت تحقیق آب شد</p></div>
<div class="m2"><p>یارب سخن نزاکت موی میان کیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از تیشه برد سعی نفس‌گوی جان‌کنی</p></div>
<div class="m2"><p>این بیستون اثر دل نامهربان‌کیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عمری به پیچ و تاب سیهروزی‌ام‌ گذشت</p></div>
<div class="m2"><p>بختم غبار طرهٔ عنبر فشان ‌کیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرگرم خوش‌خرامی ناز است ناوکت</p></div>
<div class="m2"><p>این مغز فتنه‌، کوچه‌رو استخوان کیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فریاد ما به چشم سیاهت نمی‌رسد</p></div>
<div class="m2"><p>باب دکان سرمه‌فروشان‌، فغان کیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگذارتا به عجز؟؟ بنالیم وخون شویم</p></div>
<div class="m2"><p>جرأت‌فروش عرض محبت‌، زبان کیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در هر کجا ز مشت خس ما نشان دهند</p></div>
<div class="m2"><p>آتش زن و بسوز، مپرس آشیان کیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صندل‌فروش ناصیهٔ عزتم چو صبح</p></div>
<div class="m2"><p>گرد به باد رفته‌ام از آستان کیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل ا‌گر نه طبع تو مشاطگی ‌کند</p></div>
<div class="m2"><p>آیینه‌دار شاهد معنی بیان کیست</p></div></div>