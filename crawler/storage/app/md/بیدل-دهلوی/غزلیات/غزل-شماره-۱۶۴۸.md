---
title: >-
    غزل شمارهٔ ۱۶۴۸
---
# غزل شمارهٔ ۱۶۴۸

<div class="b" id="bn1"><div class="m1"><p>ستمکش تو به قاصد اگر دهد کاغذ</p></div>
<div class="m2"><p>به سیل اشک زند دست و سر دهدکاغذ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز نقطه تخم امیدم دماند ریشه به خط</p></div>
<div class="m2"><p>چه دولت است که ناگه ثمر دهد کاغذ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چسان صفای بناگوش او کنم تحریر</p></div>
<div class="m2"><p>اگر نه مطلع فیض سحر دهد کاغذ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیاه کرد فلک نامهٔ امید مرا</p></div>
<div class="m2"><p>برای آن‌که به هر بی‌بصر دهد کاغذ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز دود کلفت دل رنگ نامه‌ام ابری‌ست</p></div>
<div class="m2"><p>مگر به او خبر از چشم تر دهد کاغذ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به هر دلی رقم داغ عشق مایل نیست</p></div>
<div class="m2"><p>بگو به لاله که خوش‌رنگ‌تر دهد کاغذ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه دود دل که نپیچیده‌ای به پردهٔ خط</p></div>
<div class="m2"><p>عجب مدار که بوی جگر دهد کاغذ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هزار نقش ز هر پرده روشن است امّا</p></div>
<div class="m2"><p>به بی‌سواد چه عرض هنر دهد کاغذ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نفس مسوز به پرواز لاف ما و منت</p></div>
<div class="m2"><p>به شعله تا چقدر بال و پر دهد کاغذ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به مفلسی نتوان لاف اعتبار گرفت</p></div>
<div class="m2"><p>که عرض قدر به افشان زر دهد کاغذ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تهی ز کینه مدان طینت تنکرویان</p></div>
<div class="m2"><p>ز سنگ عرض شرر بیشتر دهد کاغذ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به دست غیر تو آیینه دادم و خجلم</p></div>
<div class="m2"><p>چو قاصدی که به جای دگر دهد کاغذ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قلم به حسرت دیدار عجز تحریر است</p></div>
<div class="m2"><p>بیاض دیده به مژگان مگر دهد کاغذ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سفینه در دل دریا فکنده‌ام بیدل</p></div>
<div class="m2"><p>مگر ز وصل‌ کناری خبر دهد کاغذ</p></div></div>