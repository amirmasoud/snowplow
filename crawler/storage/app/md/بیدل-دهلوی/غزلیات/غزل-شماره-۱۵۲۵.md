---
title: >-
    غزل شمارهٔ ۱۵۲۵
---
# غزل شمارهٔ ۱۵۲۵

<div class="b" id="bn1"><div class="m1"><p>بعد ازینت سبزه خط در سیاهی می‌رود</p></div>
<div class="m2"><p>ای ز خود غافل زمان خوش نگاهی می‌رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌شود سرسبزی این باغ پامال خزان</p></div>
<div class="m2"><p>خوشدلی‌هایت به گرد رنگ کاهی می‌رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با قد خم‌گشته فکر صید عشرت ابلهی‌ست</p></div>
<div class="m2"><p>همچو موج از چنگ این قلاب ماهی می‌رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چاره دشوار است در تسخیر وحشت‌پیشگان</p></div>
<div class="m2"><p>نکهت گل هر طرف گردید راهی می‌رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان به پیش چشم بیباکت ندارد قیمتی</p></div>
<div class="m2"><p>رایگان این گوهر از دست سپاهی می‌رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرخوش پیمانهٔ ناز محیط جلوه‌ایم</p></div>
<div class="m2"><p>موج ما از خود به دوش‌کج‌کلاهی می‌رود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست صابون کدورتهای دل غیر ازگداز</p></div>
<div class="m2"><p>چون شود خاکستر از آتش سیاهی‌ می‌رود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صیقل زنگار کلفتها همین آه است و بس</p></div>
<div class="m2"><p>ظلمت شب با نسیم صبحگاهی می‌رود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کیست‌ گردد مانع رنگ از طواف برگ ‌گل</p></div>
<div class="m2"><p>خون من تا دامنت خواهی نخواهی می‌رود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از خط او دم مزن بیدل ‌که این حرف غریب</p></div>
<div class="m2"><p>بر زبان خامه ی صنع الاهی می رود</p></div></div>