---
title: >-
    غزل شمارهٔ ۱۶۸۱
---
# غزل شمارهٔ ۱۶۸۱

<div class="b" id="bn1"><div class="m1"><p>شب زندگی سر آمد به نفس‌شماری آخر</p></div>
<div class="m2"><p>به هوا رساند خاکم سحر انتظاری آخر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طرب بهار غفلت عرق خجالت آورد</p></div>
<div class="m2"><p>نگذشت بی‌گلابم‌ گل خنده‌کاری آخر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>الم وداع طفلی به چه درد دل سرایم</p></div>
<div class="m2"><p>به غبار ناله بردم غم نی سواری آخر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تپشی به باد دادم دگر از نمو مپرسید</p></div>
<div class="m2"><p>چو سحر چه ‌گل دماند نفس آبیاری آخر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر راه وحشت رنگ ز غبار منع پاکست</p></div>
<div class="m2"><p>ز چه پر نمی‌فشانی قفسی نداری آخر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل باغ اعتبارت اثر وفا ندارد</p></div>
<div class="m2"><p>بگذار از اول او را که فروگذاری آخر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به غرور تقوی‌، ای شیخ‌، مفروش وعظ بیجا</p></div>
<div class="m2"><p>من اگر ورع ندارم تو به من چه داری آخر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به فسانهٔ تغافل ستم است چشم بستن</p></div>
<div class="m2"><p>نگهی‌کزین‌گلستان به چه‌گل دچاری آخر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عدم و وجود و امکان همه در تو محو و حیران</p></div>
<div class="m2"><p>ز برت‌کجا رودکس‌که تو بی‌کناری آخر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو چراغ‌کشته بیدل ز خیال‌گریه مگذر</p></div>
<div class="m2"><p>مژه‌ات نمی ندارد ز چه می‌فشاری آخر</p></div></div>