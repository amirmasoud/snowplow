---
title: >-
    غزل شمارهٔ ۲۴۶۶
---
# غزل شمارهٔ ۲۴۶۶

<div class="b" id="bn1"><div class="m1"><p>تا چند به عیب من وما چشم‌گشودن</p></div>
<div class="m2"><p>آیینهٔ ما آب شد از شرم نمودن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مانند شرر دانهٔ بیحاصل ما را</p></div>
<div class="m2"><p>نا کاشته دیدند سزاوار درودن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین بیش که‌کاهیدی از اسباب تعین</p></div>
<div class="m2"><p>ای صفر هوس بر تو چه خواهند فزودن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جمعیت دل وقف مقیم پس زانوست</p></div>
<div class="m2"><p>باید به تامل مژه‌ای چند غنودن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نا صافی دل بیخبر از وهم و گمان بود</p></div>
<div class="m2"><p>تمثال بر آیینه ما بست زدودن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>علم و عملی چند که‌افسانهٔ وهم است</p></div>
<div class="m2"><p>می‌جوشد ازین پرده‌ چو گفتن ز شنودن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما را به تصرفکدهٔ عالم اسباب</p></div>
<div class="m2"><p>دستی‌ست‌که باید چو نفس بر همه سودن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خمیازه غنیمت شمرد ذوق وصالم</p></div>
<div class="m2"><p>چشمم به ‌تو وا می‌کند آغوش گشودن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما خاک نشینان چمن عیش دوامیم</p></div>
<div class="m2"><p>گل از سر تسلیم محالست ربودن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جز عجز ز پیدایی ما پرده‌گشا نیست</p></div>
<div class="m2"><p>انداز خمی هست در ابروی نمودن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل ‌رم فرصت سرو برگ نفس‌ توست</p></div>
<div class="m2"><p>جایی‌که تو باشی نتوان آنهمه بودن</p></div></div>