---
title: >-
    غزل شمارهٔ ۱۳۷۴
---
# غزل شمارهٔ ۱۳۷۴

<div class="b" id="bn1"><div class="m1"><p>تا شدم ‌گرم طلب عجز درایم‌ کردند</p></div>
<div class="m2"><p>گام اول چو سرشک آبله پایم ‌کردند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه توان‌کرد زمینگیری تسلیم رساست</p></div>
<div class="m2"><p>خشت فرسودهٔ این کهنه سرایم کردند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ننگ عریانی‌ام از اطلس افلاک نرفت</p></div>
<div class="m2"><p>بی‌تکلف چقدر تنگ قبایم کردند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عمرها شد غم خود می‌خورم و می‌بالم</p></div>
<div class="m2"><p>پهلوی‌ کاسته چون شمع غذایم‌ کردند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخت‌جانی به تلاش غم جاهم فرسود</p></div>
<div class="m2"><p>استخوان داشتم افسون همایم‌ کردند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون یقین منحرف افتاد دلایل بالید</p></div>
<div class="m2"><p>راستی رفت ‌که ممنون عصایم ‌کردند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا ز هر گوشه رسد قسمت شکر دگرم</p></div>
<div class="m2"><p>قابل زله چو کشکول گدایم کردند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سیر دریاست در این دشت تماشای سراب</p></div>
<div class="m2"><p>تا شوم محرم خود دورنمایم ‌کردند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زندگی عاشق مرگ است چه باید کردن</p></div>
<div class="m2"><p>تشنهٔ خون خود از آب بقایم‌کردند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زحمت هستی‌ام از قامت پیری دریاب</p></div>
<div class="m2"><p>چقدر بارکشیدم که درتایم کردند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می‌کند گریه عرق ‌گر مژه بر می‌دارم</p></div>
<div class="m2"><p>ناکجا منفعل از دست دعایم‌کردند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>الم عین وسوا می‌کشم و حیرانم</p></div>
<div class="m2"><p>یارب از خود به چه تقصیر جدایم‌کردند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نقش خمیازهٔ واژون حبابم بیدل</p></div>
<div class="m2"><p>آه ازین ساغر عبرت‌ که بنایم‌ کردند</p></div></div>