---
title: >-
    غزل شمارهٔ ۲۷۵۲
---
# غزل شمارهٔ ۲۷۵۲

<div class="b" id="bn1"><div class="m1"><p>ای هوش‌! سخت داغیست‌، یاد بهار طفلی</p></div>
<div class="m2"><p>تا مرگ بایدت بود شمع مزار طفلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قد دو تا درین بزم آغوش ناامیدیست</p></div>
<div class="m2"><p>خمیازه کرد ما را آخر خمار طفلی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای عافیت تمنا مگذر ز خاکساری</p></div>
<div class="m2"><p>این شیوه یادگارست از روزگار طفلی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای غافل از نهایت تا کی غم بدایت</p></div>
<div class="m2"><p>مو هم سفید کردی در انتظار طفلی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای واقف بزرگی آوارگی مبارک</p></div>
<div class="m2"><p>منزل نماند هر جا بستند بار طفلی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما را ز جام قسمت خون خوردنی است اما</p></div>
<div class="m2"><p>امروز ناگوارست آن خوشگوار طفلی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا روزگار سازد خالی به دیده جایت</p></div>
<div class="m2"><p>چون اشک برنداری سر از کنار طفلی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشمم به پیری آخر محتاج توتیا شد</p></div>
<div class="m2"><p>می‌داشت کاش گردی از رهگذار طفلی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>انجام پختگی بود آغاز خامی من</p></div>
<div class="m2"><p>تا حلقه‌گشت قامت‌کردم شکار طفلی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا خاک یأس بیزم بر فرق اعتبارات</p></div>
<div class="m2"><p>یکبارکاش سازند بازم دچار طفلی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر رغم فرع گاهی بر اصل هم نگاهی</p></div>
<div class="m2"><p>تاکی بزرگ بودن ای شیرخوار طفلی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از مهد غنچه خواندیم اسرار این معما</p></div>
<div class="m2"><p>کاسودگی محال است بی‌اعتبار طفلی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آخر ز جیب پیری قد خمیده گل کرد</p></div>
<div class="m2"><p>رمزکچه نهفتن در روزگار طفلی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر موی پیری افتاد امروز نوبت رنگ</p></div>
<div class="m2"><p>زد خامه در سفیداب صورت نگار طفلی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>امروزکام عشرت از زندگی چه جویم</p></div>
<div class="m2"><p>رفت آن غباربیدل با نی سوارطفلی</p></div></div>