---
title: >-
    غزل شمارهٔ ۱۶۶۹
---
# غزل شمارهٔ ۱۶۶۹

<div class="b" id="bn1"><div class="m1"><p>ای ابر! نی به باغ و نه در لاله‌زار بار</p></div>
<div class="m2"><p>یادی ز اشک من‌ کن و درکوی یار بار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قامت به جهد، حلقه شد، اما چه فایده</p></div>
<div class="m2"><p>ما را نداد دل به در اختیار بار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آیینهٔ وصال ندارد غبار وهم</p></div>
<div class="m2"><p>بندد اگر ز کشور ما انتظار بار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از درد زه برآکه در این انجمن هنوز</p></div>
<div class="m2"><p>ننهاده است حاملهٔ اعتبار بار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای شمع گریهٔ تو دل انجمن گداخت</p></div>
<div class="m2"><p>ای اشک شعله‌بار به خاک مزار بار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درد شکست دل همه را در زمین نشاند</p></div>
<div class="m2"><p>یک شیشه کرده‌اند بر این کوهسار بار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرچند آستان کرم تشنهٔ وفاست</p></div>
<div class="m2"><p>آب رخ طلب نتوان ریخت بار بار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر در مزاج جوش غنا کسب پختگی‌ست</p></div>
<div class="m2"><p>دیگ شعور را نسزد ننگ و عار بار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناموس یک جهان غم از این دشت می‌بریم</p></div>
<div class="m2"><p>پیری تو هم به دوش من از خم‌ گذار بار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گلچینی حدیقهٔ تسلیم آگهی‌ست</p></div>
<div class="m2"><p>باغ بهار خیره‌سری گو میار بار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل ز هر دو کَون فراموشیت خوش است</p></div>
<div class="m2"><p>زین بیش نیست‌ گر همه‌ گویم هزار بار</p></div></div>