---
title: >-
    غزل شمارهٔ ۲۰۵۸
---
# غزل شمارهٔ ۲۰۵۸

<div class="b" id="bn1"><div class="m1"><p>عبث خود را چو آتش تهمت آلود غضب کردم</p></div>
<div class="m2"><p>به هر خاشاک چندان گرم جوشیدم که تب کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو آن طفلی‌که رقص بسملش در اهتزاز آرد</p></div>
<div class="m2"><p>نفسها را پر افشان یافتم ناز طرب‌کردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به داغ صد کلف واسوختم از خامی همت</p></div>
<div class="m2"><p>چو ماه از خانهٔ خورشید اگر آتش طلب‌کردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مخواه از موج ‌گوهر جرآت توفان شکاریها</p></div>
<div class="m2"><p>کمند نارسایی داشتم صید ادب کردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز حسن بی نشان تا وانمایم رنگ تمثالی</p></div>
<div class="m2"><p>در حیرت زدم آیینه‌داری را سبب‌ کردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به مستان می‌نوشتم بیخودی تمهید مکتوبی</p></div>
<div class="m2"><p>مدادش را دوات از سایهٔ برگ عنب کردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چوشمع از خلوت و محفل شدم مرهون داغ دل</p></div>
<div class="m2"><p>ز چندین دفتر آخر نقطه‌ای را منتخب‌کردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو گردون هر چه جوشید از غبارم جوهر دل شد</p></div>
<div class="m2"><p>به این یک شیشه خلقی را دکاندار حلب‌کردم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به مشق عافیت ر‌اهی دگر نگشود این دریا</p></div>
<div class="m2"><p>همین چون موج‌ گوهر گردنی را بی‌عصب‌ کردم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نرفت از طینتم شغل تمنای زمین بوسش</p></div>
<div class="m2"><p>چو ماه نو جبین گر سوده شد ایجاد لب کردم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ندامت داشت بیدل معنی موهوم فهمیدن</p></div>
<div class="m2"><p>به تحقیق نفس روز هزار آیینه شب ‌کردم</p></div></div>