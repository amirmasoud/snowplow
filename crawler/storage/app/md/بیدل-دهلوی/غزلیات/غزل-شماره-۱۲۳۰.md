---
title: >-
    غزل شمارهٔ ۱۲۳۰
---
# غزل شمارهٔ ۱۲۳۰

<div class="b" id="bn1"><div class="m1"><p>هرکجا عشاق را درد طلب منظور شد</p></div>
<div class="m2"><p>رفتن رنگ دو عالم خون یک ناسور شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رنگ منت برنمی‌دارد دل اهل صفا</p></div>
<div class="m2"><p>صبح‌ ، زخم خویش را خود مرهم‌کافور شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسکه دیدم الفت آفاق لبریز گزند</p></div>
<div class="m2"><p>دیدهٔ احباب بر من خانهٔ زنبور شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیقرارانت دماغ حسرتی می‌سوختند</p></div>
<div class="m2"><p>یک شرر ازپرده بیرون‌زد چراغ طور شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل چه سامان‌کز شکست آرزو بر هم نچید</p></div>
<div class="m2"><p>بس که مو آورد این چینی سر فغفور شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود بی‌تعمیریی صرف بنای کاینات</p></div>
<div class="m2"><p>دل خرابی‌کرد کاین ویرانه‌ها معمور شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترک انصاف از رسوم انتظام یمن نیست</p></div>
<div class="m2"><p>بسکه چشم از معنی‌ام پوشید حاسد،‌کور شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گاه توفان غضب از چین ابرو باک نیست</p></div>
<div class="m2"><p>از شکست پل نترسد سیل چون‌ پر زور شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زبن همه حسرت‌که مردم در خمارن مرده‌اند</p></div>
<div class="m2"><p>جمع شد خمیازه‌ای چند و دهان گور شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آبله بی‌سعی پامردی نمی‌آید به دست</p></div>
<div class="m2"><p>ربشهٔ تاک از دویدن صاحب انگور شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>محنت پیری‌ست بیدل حاصل عیش شباب</p></div>
<div class="m2"><p>هرکه ‌شب ‌می خورد خواهد صبحدم‌ مخمور شد</p></div></div>