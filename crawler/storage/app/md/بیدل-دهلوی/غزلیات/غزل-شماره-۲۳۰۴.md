---
title: >-
    غزل شمارهٔ ۲۳۰۴
---
# غزل شمارهٔ ۲۳۰۴

<div class="b" id="bn1"><div class="m1"><p>ز دل چون غنچه یک چاک‌ گریبانگیر می‌خواهم</p></div>
<div class="m2"><p>گشاد کار خود بی‌ناخن تدبیر می‌خواهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی‌ام مخمور می‌ کز قلقل مینا به جوش آیم</p></div>
<div class="m2"><p>سیه مست جنونم غلغل زنجیر می‌خواهم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به‌ کوثر گر زند ساغر ندارد بسملم سیری</p></div>
<div class="m2"><p>دم آبی اگر می‌خواهم از شمشیر می‌خواهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنایم ننگ ویرانی‌ کشید از دست جمعیت</p></div>
<div class="m2"><p>غبار دامن زلفی پی تعمیر می‌خواهم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آتش‌ کاش احرام جنون بندد سپند من</p></div>
<div class="m2"><p>به وحشت جستنی زین خانهٔ دلگیر می‌خواهم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به هر مویم هجوم جلوه خوابانده‌ست مژگانها</p></div>
<div class="m2"><p>ز شوقت جنبشی چون خامهٔ تصویر می‌خواهم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به بوی غنچه نسبت‌ کرده‌ام طرز کلامت را</p></div>
<div class="m2"><p>زبان برگ گل در عذر این تقصیر می‌خواهم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درین صحرا جنون هرزه فکر دامها دارد</p></div>
<div class="m2"><p>دو عالم جسته است از خویش و من نخجیر می‌خواهم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لب سوفارم از خمیازه‌های بی‌پر و بالی</p></div>
<div class="m2"><p>ز گردون مقوس همتی چون تیر می‌خواهم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حصول مطلب از ذوق تمنا می‌کند غافل</p></div>
<div class="m2"><p>زمان انتظار هر چه باشد دیر می‌خواهم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به رنگ من برون آید کسی تا قدر من داند</p></div>
<div class="m2"><p>به این امید طفلی را که خواهم پیر می‌خواهم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز حد بگذشت بیدل مستی شور جنون من</p></div>
<div class="m2"><p>به چوب‌ گل چو بلبل اندکی تعزیر می‌خواهم</p></div></div>