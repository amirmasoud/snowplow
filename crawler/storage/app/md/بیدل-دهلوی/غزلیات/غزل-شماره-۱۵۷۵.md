---
title: >-
    غزل شمارهٔ ۱۵۷۵
---
# غزل شمارهٔ ۱۵۷۵

<div class="b" id="bn1"><div class="m1"><p>خارج ابنای جنس است آنکه موزون می‌شود</p></div>
<div class="m2"><p>قطره چون گردد گهر از بحر بیرون می‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با همه افسردگی گر راه فکری واکنم</p></div>
<div class="m2"><p>جیب ما خمخانهٔ جوش فلاطون می‌شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبنم و گل غیر رسوایی چه دارد زین چمن</p></div>
<div class="m2"><p>گریهٔ بیدردی ما خنده مقرون می‌شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خانه‌داری دیگر و صحرانوردی دیگر است</p></div>
<div class="m2"><p>تاب دلتنگی ندارد آنکه مجنون می‌شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از جنون‌کرر فر بر چرخ مفرازد سر</p></div>
<div class="m2"><p>کاین صدای کوه آخر گرد هامون می‌شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باکفن سازید پاک آلایش ننگ جسد</p></div>
<div class="m2"><p>جامه چون شد شوخگین محتاج صابون می‌شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سعد اگر خوانی چه حاصل طینت منحوس را</p></div>
<div class="m2"><p>همچنان مسخ است اگر بوزینه‌، میمون می‌شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زین غناها آنچه خواهی از صفای دل طلب</p></div>
<div class="m2"><p>چون به صیقل می‌رسد آیینه قارون می‌شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی‌تکلف نیست موقوف دو مصرع وضع بیت</p></div>
<div class="m2"><p>چون دو در مربوط هم‌ شد خانه موزون می‌شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر سرم‌ گر سایه افتد زان حنایی نقش پا</p></div>
<div class="m2"><p>چون بهار از سایهٔ من خاک گلگون می‌شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جهدها باید که جامی زین چمن آری به دست</p></div>
<div class="m2"><p>آب تاگل هرقدم رنگی دگرخون می‌شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا کیت قلقل‌نواییهای آهنگ شباب</p></div>
<div class="m2"><p>ای جنون‌پیمای غفلت شیشه واژون می‌شود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بیدل اشعار من از فهم کسان پوشیده ماند</p></div>
<div class="m2"><p>چون عبارت نازک افتد رنگ مضمون می‌شود</p></div></div>