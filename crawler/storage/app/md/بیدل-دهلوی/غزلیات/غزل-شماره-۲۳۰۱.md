---
title: >-
    غزل شمارهٔ ۲۳۰۱
---
# غزل شمارهٔ ۲۳۰۱

<div class="b" id="bn1"><div class="m1"><p>صید کمند شوقی‌ست از مهر تا به ما هم</p></div>
<div class="m2"><p>جوش بهار حیرت یعنی‌گل نگاهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با هر فسرده رنگی شادم‌ که پیش شمعت</p></div>
<div class="m2"><p>تا بال می‌فشانم پروانه دستگاهم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جولان ناز سر کن اندیشه مختصر کن</p></div>
<div class="m2"><p>ظلم آنقدر ندارد پا مالی‌گیاهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا زنگ پرده برداشت آینه محو صافی‌ست</p></div>
<div class="m2"><p>خوابیده است عفوت در سایهٔ گناهم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زنجیر می نویسد سطری ز حال مجنون</p></div>
<div class="m2"><p>در دعوی اسیران‌، زلف دو تا، گواهم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جوهر ز ضعف پروار آیینه می‌پرستد</p></div>
<div class="m2"><p>نقش نگین داغ است سطری‌که دارد آهم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آمد به یاد شوقم‌کیفیت خرامی</p></div>
<div class="m2"><p>شد موج ساغر می در چشم تر نگاهم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای زلف یار تاکی با شانه همزبانی</p></div>
<div class="m2"><p>ما نیزسینه چاکیم رحمی به حال ما هم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تاری‌ست پیکر من در چنگ ناتوانی</p></div>
<div class="m2"><p>از زخمهٔ نگاهی بنواز گاه گاهم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عرض مثال امکان منظور الفتم نیست</p></div>
<div class="m2"><p>در عالم تحیر آینه بارگاهم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قصرم سری ندارد یاگیر و دار فغفور</p></div>
<div class="m2"><p>یارب چو موی چینی دل بشکندکلاهم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همدوش سایه رفتم تا خاک آستانش</p></div>
<div class="m2"><p>از بخت تیره بیدل زین بیشتر چه خواهم</p></div></div>