---
title: >-
    غزل شمارهٔ ۲۶۵۴
---
# غزل شمارهٔ ۲۶۵۴

<div class="b" id="bn1"><div class="m1"><p>عرق ربز خجالت می‌گدازد سعی بیتابی</p></div>
<div class="m2"><p>ندارم مزرع امید اما می‌دهم آبی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین دریا به‌کام آرزو نتوان رسید آسان</p></div>
<div class="m2"><p>مه اینجا بعد سالی می‌کشد ماهی به قلابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خجالت هم ز ابرام طبیعت برنمی‌آید</p></div>
<div class="m2"><p>حیا را کرد غواص عرق مطلوب نایابی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گهی فکر تعین‌گاه هستی می‌کنم انشا</p></div>
<div class="m2"><p>سر و کارم به تعبیر است ‌گویا دیده‌ام خوابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خم تسلیم‌، قرب راحت جاوید می‌باشد</p></div>
<div class="m2"><p>به ذوق سجده سر دزدیده‌ام در کنج محرابی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قناعت پرور این‌گرد خوانیم از ضعیفیها</p></div>
<div class="m2"><p>غنیمت می‌شمارد رشتهٔ ما خوردن تابی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز فکر خودگریزان رفت خلق نارسا فطرت</p></div>
<div class="m2"><p>بر ناآشنا سیر گریبان بود گردابی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تلاش حرص هم سرمایهٔ مقدور می‌خواهد</p></div>
<div class="m2"><p>دماغ ما ز خشکی داغ شد، ای دردسر خوابی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برو درکربلا دیگر مپرس از رمز استغنا</p></div>
<div class="m2"><p>شهید ناز او از تیغ می‌خواهد دم آبی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نوایی‌ گل نکرد از پردهٔ ساز نفس بیدل</p></div>
<div class="m2"><p>ز هستی بگسلم شاید رسد تاری به مضرابی</p></div></div>