---
title: >-
    غزل شمارهٔ ۱۹۵۸
---
# غزل شمارهٔ ۱۹۵۸

<div class="b" id="bn1"><div class="m1"><p>گهی حجاب وگه آیینهٔ جمال توام</p></div>
<div class="m2"><p>به حیرتم‌ که چها می‌کند خیال توام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مزاج شوقم از آب وگل تسلی نیست</p></div>
<div class="m2"><p>جنون سرشته غبار رم غزال توام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کلاه گوشهٔ پروازم آسمان سایی‌ست</p></div>
<div class="m2"><p>ز بس چو آرزوی خود شکسته بال توام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس است حلقهٔ ‌گوشم خم سجود نیاز</p></div>
<div class="m2"><p>اگر به چرخ برآیم همان هلال توام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز امتیاز فنا و بقا نمی‌دانم</p></div>
<div class="m2"><p>جز اینکه ذرهٔ خورشید بی زوال توام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زمانه‌گر نشناسد مرا به این شادم</p></div>
<div class="m2"><p>که من هم آینهٔ حسن بی مثال توام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سپند من به فسردن چرا نه نازکند</p></div>
<div class="m2"><p>نفس گداختهٔ جستجوی خال توام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مباد هیچکس آفت نصیب همچشمی</p></div>
<div class="m2"><p>حنا گداخت که من نیز پایمال توام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به چشم تر نتوان شبنم بهار تو شد</p></div>
<div class="m2"><p>عرق فروش گلستان انفعال توام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به خود نمی‌رسم از فکر ناقصی‌که مراست</p></div>
<div class="m2"><p>زهی هوس که در اندیشهٔ کمال توام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خیال وحشت و آرام حیرت‌ست اینجا</p></div>
<div class="m2"><p>چه آشیان و چه پرواز زیر بال توام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خبر ز خویش ندارم جز اینکه روزی چند</p></div>
<div class="m2"><p>نگاه شوق تو بودم‌کنون خیال توام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زمین معرفت از ریشهٔ دویی پاک است</p></div>
<div class="m2"><p>چرا زخویش نیایم برون نهال توام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز شرم بیدلی خود گداختم بیدل</p></div>
<div class="m2"><p>دلی ندارم و سودایی وصال توام</p></div></div>