---
title: >-
    غزل شمارهٔ ۲۳۲۵
---
# غزل شمارهٔ ۲۳۲۵

<div class="b" id="bn1"><div class="m1"><p>چون غنچه در خیال تو هرگاه رفته‌ایم</p></div>
<div class="m2"><p>محمل به دوش بیخودی آه رفته‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پاس قدم به دشت جنون حق سعی ماست</p></div>
<div class="m2"><p>عمری به دوش آبله‌ها راه رفته‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راه سفر اگر همه ابروست تا جبین</p></div>
<div class="m2"><p>از ضعف چون هلال به یک ماه رفته‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از ساز منزل و سفر عاجزان مپرس</p></div>
<div class="m2"><p>چون داغ آرمیده و چون آه رفته‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>محمل طراز کشمکش دهر عبرتیست</p></div>
<div class="m2"><p>ماییم خواه آمده و خواه رفته‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امروز سود ما غم فردای زندگی است</p></div>
<div class="m2"><p>اندیشه‌ای که در چه زیانگاه رفته‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عجز و غرور هر دو جنون‌تاز وحشتند</p></div>
<div class="m2"><p>زین باغ اگر گلیم و اگر کاه رفته‌ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لاف صفا ز طبع هوس موج می‌زند</p></div>
<div class="m2"><p>ای هوش غفلتی ‌که پر آگاه رفته‌ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فرصت ز رنگ ماست پرافشان نیستی</p></div>
<div class="m2"><p>غافل ز ما مباش که ناگاه رفته‌ایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عنقا نشان شهرت گمنامی خودیم</p></div>
<div class="m2"><p>کو بازگشتنی که به افواه رفته‌ایم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بانگ دراست قافلهٔ بیقرار ما</p></div>
<div class="m2"><p>یک‌ گام ناگشوده به صد راه رفته‌ایم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل به بند نی‌ گرهی نیست ناله را</p></div>
<div class="m2"><p>آزاده‌ایم اگر همه در چاه رفته‌ایم</p></div></div>