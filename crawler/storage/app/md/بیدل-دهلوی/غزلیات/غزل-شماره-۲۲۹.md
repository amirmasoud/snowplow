---
title: >-
    غزل شمارهٔ ۲۲۹
---
# غزل شمارهٔ ۲۲۹

<div class="b" id="bn1"><div class="m1"><p>چون نگاه از بس به ذوق جلوه همدوشیم ما</p></div>
<div class="m2"><p>یک مژه تا واشود صد دشت آغوشیم ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حیرت ما ازدرشتیهای وضع عالم است</p></div>
<div class="m2"><p>دهرتاکهسار شد آیینه می‌جوشیم ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمع فانوس حباب از ما منورکرده‌اند</p></div>
<div class="m2"><p>روشنی داریم چندانی‌که خاموشیم ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم‌بند غفلت هستی تماشاکردنی‌ست</p></div>
<div class="m2"><p>دهرشور محشرست وپنبه درگوشیم ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساز تشویش عدم از هستی ما می‌دمد</p></div>
<div class="m2"><p>عافیت بی‌اضطرابی نیست تا هوشیم ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شعله‌گر دارد مقام عافیت خاکسترست</p></div>
<div class="m2"><p>به‌که طاقتها به دست عجزبفروشیم ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آمد و رفت نفس پر بی‌سبب افتاده است</p></div>
<div class="m2"><p>کیست تافهمدکه از بهر چه می‌کوشیم‌ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زندگی تنها وبال ما نشد ز اقبال عجز</p></div>
<div class="m2"><p>نیستی هم بارتکلیف است تا دوشیم ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>احتیاط ظاهر امواج عجز باطن است</p></div>
<div class="m2"><p>بسکه می‌بالد شکست دل زره پوشیم ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>راه مقصد جزبه سعی ناله نتوان‌کرد طی</p></div>
<div class="m2"><p>چون جرس‌بیدرد هم ای‌کاش‌بخروشیم‌ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون نگه صدمدعا ازعجز مابی‌پرده است</p></div>
<div class="m2"><p>نیست‌فریادی به‌این شوخی‌که‌خاموشیم‌ما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یاد ما بیدل وداع وهم هستی‌کردن است</p></div>
<div class="m2"><p>تا خیالی در نظر داری فراموشیم ما</p></div></div>