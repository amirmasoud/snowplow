---
title: >-
    غزل شمارهٔ ۶۴۵
---
# غزل شمارهٔ ۶۴۵

<div class="b" id="bn1"><div class="m1"><p>هوس دل را شکست اعتبارست</p></div>
<div class="m2"><p>به یک مو حسن چینی ریش‌دارست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز ننگ تنگ‌چشمیهای احباب</p></div>
<div class="m2"><p>به هم آوردن مژگان فشارست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل بی‌کینه زین محفل مجویید</p></div>
<div class="m2"><p>که هر آیینه چندین زنگبارست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمی‌خواهد حیا تغییر اوضاع</p></div>
<div class="m2"><p>لب خاموش را خمیازه عارست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جضور اهل این گلزار دیدم</p></div>
<div class="m2"><p>همین رنگ جنا شب‌ژنده‌دارست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عصا و ریش شیخ اعجاز شیخ است</p></div>
<div class="m2"><p>که پیر و شیرخوارانی سوارست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نفس را هر نفس رد می‌کند دل</p></div>
<div class="m2"><p>هوای این چمن پر ناگوارست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قناعت‌کن ز نقش این نگینها</p></div>
<div class="m2"><p>به آن نامی‌که بر لوح مزارست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به دوش همتت نه اطلس چرخ</p></div>
<div class="m2"><p>اگر عریان شوی یک جامه‌وارست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به‌چشمت‌گرد مجنول‌سرمه‌کش نیست</p></div>
<div class="m2"><p>وگرنه ششجهت لیلی بهارست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به پیش قامتش از سرو تا نخل</p></div>
<div class="m2"><p>همه انگشتهای زینهارست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جهان ‌می‌نالد از بی‌دست و پایی</p></div>
<div class="m2"><p>صدا عذر خرام کوهسارست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فلک تا دوری از تجدید دارد</p></div>
<div class="m2"><p>بنای گردش رنگ استوارست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو مو چندان‌که بالم سرنگونم</p></div>
<div class="m2"><p>عرق در مزرع شرم آبیارست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سراغ‌خود درین دشت ازکه پرسم</p></div>
<div class="m2"><p>که من تمثالم و آیینه تارست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مپرس از اعتبار پوچ بیدل</p></div>
<div class="m2"><p>احد زین صفرها چندین هزارست</p></div></div>