---
title: >-
    غزل شمارهٔ ۳۹
---
# غزل شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>نزیبد پرده فانوس دیگر شمع سودا را</p></div>
<div class="m2"><p>مگردرآب چون یاقوت‌گیرند آتش ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل آسودهٔ ما شور امکان در قفس دارد</p></div>
<div class="m2"><p>گهر دزدیده‌است اینجاعنان موج‌دریا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهشت عافیت رنگ جهان آبرو باشی</p></div>
<div class="m2"><p>درآغوش نفس‌گر خون‌کنی عرض تمنا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غبار احتیاج آنجاکه دامان طلب‌گیرد</p></div>
<div class="m2"><p>روان است آبرو هرگه به رفتارآوری پا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به‌عرض بیخودیهاگرم‌کن هنگامهٔ مشرب</p></div>
<div class="m2"><p>که می‌نامیده‌اند اینجا شکست رنگ مینا را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فروغ این شبستان جز رم برقی نمی‌باشد</p></div>
<div class="m2"><p>چراغان‌کرده‌اند از چشم آهوکوه و صحرا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دراین‌محفل‌پریشان‌جلوه‌است آن‌حسن یکتایی</p></div>
<div class="m2"><p>شکستی‌کوکه پردازی دهد آیینهٔ ما را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سبکتازاست شوق امامن آن سنگ زمینگیرم</p></div>
<div class="m2"><p>که‌دررنگ شرراز خویش خالی می‌کنم جا را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به‌داغ بی‌نگاهی رفت‌ازین محفل چراغ من</p></div>
<div class="m2"><p>شکست آیینهٔ رنگی‌که‌گم‌کردم تماشا را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هوس چون نارسا شد نسیه نقدحال می‌گردد</p></div>
<div class="m2"><p>امل را رشته‌کوته ساز و عقباگیر دنیا را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز شور بی‌نشانی‌، بی‌نشانی شد نشان بیدل</p></div>
<div class="m2"><p>که‌گم‌گشتن زگم‌گشتن برون آورد عنقا را</p></div></div>