---
title: >-
    غزل شمارهٔ ۱۰۰۰
---
# غزل شمارهٔ ۱۰۰۰

<div class="b" id="bn1"><div class="m1"><p>جایی‌ که جام در دست آن مه خرام دارد</p></div>
<div class="m2"><p>مژگان گشودن آنجا مهتاب و بام دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عام است ذکر عشاق در معبد خیالش</p></div>
<div class="m2"><p>گر برهمن نباشد بت رام رام دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دی آن نگار مخمور در پرده‌گردشی داشت</p></div>
<div class="m2"><p>امروز صد خرابات مینا و جام دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کم‌مایگان به هر رنگ سامان انفعالند</p></div>
<div class="m2"><p>هستی دو روزه‌ عصیان زحمت دوام دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رنگ بهار امکان ازگردش آفریدند</p></div>
<div class="m2"><p>هر صاف در‌ثماست هر صبح شام دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز انفعال ازین بزم ‌کام دگر مجویید</p></div>
<div class="m2"><p>لذات عالم خواب یک احتلام دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیتابی تفسها عمری‌ست دارد آواز</p></div>
<div class="m2"><p>کای صبح پرفشان باش این دشت دام دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ضبط نفس در این بحر جمعیت‌آفرین است</p></div>
<div class="m2"><p>گوهر هزار قلاب مصروف‌کام دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آثار جوهر مرد پنهان نمی‌توان‌کرد</p></div>
<div class="m2"><p>تیغ‌کشیدهٔ‌کوه ننگ از نیام دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل را ودیعت وهم باید ز سر ادا کرد</p></div>
<div class="m2"><p>از خلق آنچه دارد آیینه وام دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قلقل همین ‌دو حرف ‌است ‌ای‌ شیشه دردسر چند</p></div>
<div class="m2"><p>چیزی بگوی و بگذر قاصد پیام دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفتم به دل‌که عمری‌ست ذوق وصال دارم</p></div>
<div class="m2"><p>خندید کاین خیالت سودای خام دارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جوش خطی‌ست بیدل پرگار مرکز حسن</p></div>
<div class="m2"><p>دود چراغ این بزم پروانه نام دارد</p></div></div>