---
title: >-
    غزل شمارهٔ ۲۳۲۶
---
# غزل شمارهٔ ۲۳۲۶

<div class="b" id="bn1"><div class="m1"><p>یکدم آسایش به صد ابرام پیدا کرده‌ایم</p></div>
<div class="m2"><p>سعی‌ها شد خاک تا آرام پیدا کرده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیره بختی نیز مفت دستگاه عجز ماست</p></div>
<div class="m2"><p>روز اگر گم‌ گشت باری شام پیدا کرده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مقصد عشاق رسوایی‌ست ما هم چون سحر</p></div>
<div class="m2"><p>یک گریبان جامهٔ احرام پیدا کرده‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شهره واماندگی‌هاییم چون نقش نگین</p></div>
<div class="m2"><p>پای تا بر سنگ آمد نام پیدا کرده‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قطرهٔ اشکیم ما را جهد کو جولان‌ کدام</p></div>
<div class="m2"><p>از چکیدن تهمت یک گام پیدا کرده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای‌ شرر زین بیش برآیینهٔ‌ فطرت مناز</p></div>
<div class="m2"><p>ما هم از آغاز خویش انجام پیدا کرده‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم حیران درکفیم از نشئهٔ دیدار و بس</p></div>
<div class="m2"><p>بیخودی وقف تماشا جام پیدا کرده‌ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عمرها شد با خیال جلوهٔ او توأم است</p></div>
<div class="m2"><p>بی نگه چشمی که چون بادام پیدا کرده‌ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خامشی‌ خلوتگهٔ وصلست و ما نامحرمان</p></div>
<div class="m2"><p>از لب غفلت نوا پیغام پیدا کرده‌ایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عمر زندانخانهٔ چندین تعلق بوده است</p></div>
<div class="m2"><p>در غبار خود سراغ دام پیدا کرده‌ایم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خاک ما امروزگرم آهنگ پرواز فناست</p></div>
<div class="m2"><p>ای هوس کسب هواها بام پیدا کرده‌ایم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عالم موهومه‌ای اسباب صورت بسته است</p></div>
<div class="m2"><p>آنچه بیدل از خیال خام پیدا کرده‌ایم</p></div></div>