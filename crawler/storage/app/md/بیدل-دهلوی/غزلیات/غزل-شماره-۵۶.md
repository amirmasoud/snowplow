---
title: >-
    غزل شمارهٔ ۵۶
---
# غزل شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>شرر تمهید سازد مطلب ما داستان‌ها را</p></div>
<div class="m2"><p>دهد پرواز بسمل مدعای ما بیان‌ها را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به جرم ما و من دوریم از سرمنزل مقصد</p></div>
<div class="m2"><p>جرس اینجا بیابان مرگ دارد کاروان‌ها را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کدورت چیده‌ای جدی نما تا بی‌نفس گردی</p></div>
<div class="m2"><p>صفای دیگرست از فیض برچیدن دکان‌ها را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندانم جوش توفان خیال کیست این گلشن</p></div>
<div class="m2"><p>که اشک چشم مرغان کرد گرداب‌آشیان‌ها را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به لعل او خط از ما بیشتر دل‌بستگی دارد</p></div>
<div class="m2"><p>طمع افزون‌تر از دزدست اینجا پاسبان‌ها را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نفس سرمایهٔ بیتابی‌ست‌، افسردگی تا کی</p></div>
<div class="m2"><p>مکن شمع مزار زندگانی استخوان‌ها را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جز کشتی شکستن ساحل امنی نمی‌باشد</p></div>
<div class="m2"><p>ز بس وسعت فروبرده‌ست این دریا کران‌ها را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به سعی اشک‌،‌ کام از دهر حاصل می‌کنی روزی</p></div>
<div class="m2"><p>که آهت پرّه گردد آسیای سمان‌ها را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به افسون مدارا از کج‌اندیشان مشو ایمن</p></div>
<div class="m2"><p>تواضع در کمین تیر می‌دارد کمان‌ها را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جهانی آرزوها پخت و سیر آمد ز ناکامی</p></div>
<div class="m2"><p>تنور سرد این مطبخ به خامی سوخت نان‌ها را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من آن عاجز سجودم کز پی طرف جبین من</p></div>
<div class="m2"><p>به دوش باد می‌آرند خاک آستان‌ها را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو هم خاموش شو بیدل که من از یاد دیداری</p></div>
<div class="m2"><p>به دوش حیرت آیینه می‌بندم فغان‌ها را</p></div></div>