---
title: >-
    غزل شمارهٔ ۶۸۵
---
# غزل شمارهٔ ۶۸۵

<div class="b" id="bn1"><div class="m1"><p>بسکه رازعجز ما بالید پنهان زیرپوست</p></div>
<div class="m2"><p>یک قلم چون آبله‌گشتیم عریان زیرپوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرشکست رنگ ما دیدی ز حال مپرس</p></div>
<div class="m2"><p>نامهٔ مجنون ندارد غیر عنوان زیر پوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست ممکن از لباس وهم بیرون آمدن</p></div>
<div class="m2"><p>زندگانی عالمی راکرد زندان زیر پوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا نگردد قاتل ما جز به‌گلچینی سمر</p></div>
<div class="m2"><p>همچوگل خون‌بحل‌گردیم سامانزیر پوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناله‌ها در پردهٔ ساز جنون دزدیده‌ایم</p></div>
<div class="m2"><p>خفته شیر بیشهٔ ما را نیستان زیرپوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جیب ما چون غنچه آخربال صحرا می‌کشد</p></div>
<div class="m2"><p>بر سر ما سایه افکنده است دامان زیرپوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلوت راز است چشمی‌کز تماشا دوختیم</p></div>
<div class="m2"><p>عین یوسف شد نگاه پیرکنعان زیرپوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از نقاب غنچه رنگ شور بلبل می‌چکد</p></div>
<div class="m2"><p>شیشه دارد خون عیش می‌پرستان زیرپوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ساز هستی پرده‌دارد شوخیی در دست و بس</p></div>
<div class="m2"><p>هرکه بینی ناله‌ای‌کرده‌ست پنهان زیر پوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همچو نارم عقده‌ای ازکار دل تا واشود</p></div>
<div class="m2"><p>سرخ‌کردم هم به خو سعی دندان زیر پوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتم آفتهای‌امکان زیرگردون است و بس</p></div>
<div class="m2"><p>زندگی‌نالید وگفت این‌جمله‌توفان‌زیر پوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بسکه مردم جنس ایثار از نظر پوشیده‌اند</p></div>
<div class="m2"><p>درهم ماهی‌ست ایتجا همچو همیان زیر پوست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عضو عضوم حسرت دیدار می‌آرد به بار</p></div>
<div class="m2"><p>نخل بادمم سراپا چشم حیران زیر پوست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هیچ‌کس آتش نزد بر صفحهٔ بیحاصلم</p></div>
<div class="m2"><p>ورنه من‌هم‌داشتم بیدل چراغان‌زیر پوست</p></div></div>