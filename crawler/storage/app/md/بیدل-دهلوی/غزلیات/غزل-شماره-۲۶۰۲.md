---
title: >-
    غزل شمارهٔ ۲۶۰۲
---
# غزل شمارهٔ ۲۶۰۲

<div class="b" id="bn1"><div class="m1"><p>گر نفس چیند به این فرصت بساط دستگاه</p></div>
<div class="m2"><p>چون سحر بر ما شکستن می‌رسد پیش از کلاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سینه صافی می‌شود بی‌پرده تا دم می‌زنم</p></div>
<div class="m2"><p>در دل ما چون حباب آیینه‌پرداز است آه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما و من آخر سواد یأس روشن می‌کند</p></div>
<div class="m2"><p>خلقی از مشق نفس آیینه می‌سازد سیاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صاحب دل کیست حیرانم درین غفلت‌سرا</p></div>
<div class="m2"><p>آینه یک‌ گل زمین است و جهانی خانه خواه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرگشایی دیدهٔ انصاف بر اقبال ظلم</p></div>
<div class="m2"><p>همچوآتش اخگر است و شعله آن تخت وکلاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اوج اقبال شهنشاهی توهم کرده است</p></div>
<div class="m2"><p>بر سر مژگان نم اشکی چکیدن دستگاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>استخوان چرب و خشکی هست‌کز خاصیتش</p></div>
<div class="m2"><p>سگ توجه بر گدا دارد هما بر پادشاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای هوس رسوایی دیبا و اطلس روشن است</p></div>
<div class="m2"><p>بیش ازین از جامهٔ عریانی‌ام عریان مخواه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با شکوه آسمان‌ گردن نیفرازد زمین</p></div>
<div class="m2"><p>خاک باید بود پیش رفعت آن بارگاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>محرم راز کرم نتوان شدن بی‌احتیاج</p></div>
<div class="m2"><p>در پناه رحمت آخر می‌برد ما را گناه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بی‌گداز نیستی صورت نبندد آگهی</p></div>
<div class="m2"><p>شمع این محفل سراپا سرمه است و یک نگاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر به این رنگست بید‌ل رونق بازار دهر</p></div>
<div class="m2"><p>تا قیامت یوسف ما برنمی‌آید زچاه</p></div></div>