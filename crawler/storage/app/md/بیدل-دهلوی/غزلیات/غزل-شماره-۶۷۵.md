---
title: >-
    غزل شمارهٔ ۶۷۵
---
# غزل شمارهٔ ۶۷۵

<div class="b" id="bn1"><div class="m1"><p>نیست ایمن از بلا هر کس به فکر جستجوست</p></div>
<div class="m2"><p>روز و شب گرداب‌ را ازموج‌،‌ خنجر برگلوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در تماشایی ‌که ما را بار جرات داده‌اند</p></div>
<div class="m2"><p>آرزو در سینه خار است و نگه در دیده موست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جادهٔ کج رهروان را سر خط جانکاهی‌ست</p></div>
<div class="m2"><p>باعث آشوب دل ها پیچ و تاب آرزوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنچه نتوان داد جز در دست محبوبان دل است</p></div>
<div class="m2"><p>وانچه نتوان ریخت جز در پای خوبان آبروست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر فریب عرض جوهر گرد پرکاری مگرد</p></div>
<div class="m2"><p>آینه بی‌حسن نتوان یافتن تا ساده‌روست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسن بیرنگیست در هرجا به رنگی جلوه‌گر</p></div>
<div class="m2"><p>در دل سنگ آنچه می‌بینی شرر در غنچه بوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غیر حیرت آبیار مزرع عشاق نیست</p></div>
<div class="m2"><p>چون رگ یاقوت اینجا ریشه درخون نموست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی‌فنا نتوان به کنه معنی اشیا رسید</p></div>
<div class="m2"><p>آینه‌ گر خاک‌کردد با دو عالم روبروست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در عبادتگاه ما کانجا هوس را بار نیست</p></div>
<div class="m2"><p>نقش خویش از لوح هستی گر توان شستن وضوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خار و خس را اعتباری نیست غیر از سوختن</p></div>
<div class="m2"><p>آبروی مزرع ما برق استغنای اوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غفلت ما پرده‌دار عیب بینایی خوشست</p></div>
<div class="m2"><p>چاک دامان نگه را بستن مژگان رفوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون زبان خامه بیدل درکف استاد عشق</p></div>
<div class="m2"><p>باکمال نکته‌سنجی بیخبر از گفتگوست</p></div></div>