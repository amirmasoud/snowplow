---
title: >-
    غزل شمارهٔ ۱۳۲۸
---
# غزل شمارهٔ ۱۳۲۸

<div class="b" id="bn1"><div class="m1"><p>فرصت انشایان هستی‌ گر تکلف ‌کرده‌اند</p></div>
<div class="m2"><p>سکته مقداری در این مصرع توقف کرده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از مآل زندگی جمعی ‌که دارند آگهی</p></div>
<div class="m2"><p>کارهای عالم از دست تأسف کرده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هستی و امید جمعیت جنون وهم ‌کیست</p></div>
<div class="m2"><p>عافیت دارد چراغی کز نفس پف کرده‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در مزاج خلق بیکاری هوس می‌پرورد</p></div>
<div class="m2"><p>غافلان نام فضولی را تصوف کرده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گشته‌اند آنهاکه در هنگامهٔ اغراض پیر</p></div>
<div class="m2"><p>موسفیدی را به روی زندگی‌ تف‌ کرده‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در حقیقت اتحاد کفر و ایمان ثابت است</p></div>
<div class="m2"><p>اندکی از بدگمانی‌ها، تخلف کرده‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حسن یکتا کارگاه شوخی تمثال نیست</p></div>
<div class="m2"><p>اینقدر آیینه‌پردازان تصرف کرده‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیدل از خوبان همین آیین استغنا خوش است</p></div>
<div class="m2"><p>بر حیا ظلم است اگر با کس تلطف ‌کرده‌اند</p></div></div>