---
title: >-
    غزل شمارهٔ ۱۲۲۶
---
# غزل شمارهٔ ۱۲۲۶

<div class="b" id="bn1"><div class="m1"><p>اینقدر نمی‌دانم صیدم از چه لاغر شد</p></div>
<div class="m2"><p>کزتصور خونم آب تیغ اوتر شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حرف شعله خویش را، با محیط سر کردم</p></div>
<div class="m2"><p>فلس ماهیان یکسر دیده سمندر شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کاف‌و نون‌لبی‌ وا کر‌د، حسن‌وعشق شورانگیخت</p></div>
<div class="m2"><p>احوالی ضرور افتاد قند ما مکرر شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در جهان نومیدی محو بود آفتها</p></div>
<div class="m2"><p>آررو فضولی ‌کرد جستجو ستمگر شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گردش فلک دیدی‌، ای‌ جنون تأمل چیست</p></div>
<div class="m2"><p>دور، دور بیباکی‌ست شیشه وقف ساغر شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرچه با جنون‌پیوست زکمین آفت رست</p></div>
<div class="m2"><p>پاسبان خود گردید خانه‌ای که بی در شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواب گل در این گلشن تهمت خیالی بود</p></div>
<div class="m2"><p>رنگ پهلویی‌گرداند تا امید بستر شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>راحت آرزوییها داغ‌ کرد محفل را</p></div>
<div class="m2"><p>رنگ‌ها چو شمع اینجا صرف بالش پر شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کسب عزت دنیا سخت عبرت‌آلودست</p></div>
<div class="m2"><p>خاک‌ گشت سر در جیب‌ قطره‌ای‌ که ‌گوهر شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آه بر در دونان آخر التجا بردیم</p></div>
<div class="m2"><p>تشنه‌کام می‌مردیم آبرو میسر شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیلد‌ل این تغافلها جرم خست‌ کس نیست</p></div>
<div class="m2"><p>احتیاج‌ها شورید گوش دوستان ‌کر شد</p></div></div>