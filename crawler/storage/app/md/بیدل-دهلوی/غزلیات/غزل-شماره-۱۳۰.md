---
title: >-
    غزل شمارهٔ ۱۳۰
---
# غزل شمارهٔ ۱۳۰

<div class="b" id="bn1"><div class="m1"><p>داغ عشقم‌، نیست الفت با تن‌آسانی مرا</p></div>
<div class="m2"><p>پیچ وتاب شعله باشد نقش پیشانی مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌سبب در پردهٔ اوهام لافی داشتم</p></div>
<div class="m2"><p>شد نفس آخربه لب انگشت حیرانی مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از نفس بر خویش می‌لرزد بنای غنچه‌ام</p></div>
<div class="m2"><p>نیست غیر از لب‌گشودن سیل ویرانی مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلعت خونین‌دلان تشریف دردی بیش نیست</p></div>
<div class="m2"><p>بس بود چون غنچه زخم دل‌گریبانی مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رازداریها به معنی‌کوس شهرت بوده است</p></div>
<div class="m2"><p>چون حیا ازپوشش عیب است عریانی مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پر سبکروحم زفکر سخت جانی فارغم</p></div>
<div class="m2"><p>چون شرر در سنگ نتوان‌کرد زندانی مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرد بیتاب از طواف دامنی محروم نیست</p></div>
<div class="m2"><p>زد به صحرای جنون آخرپریشانی مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچو موجم سودن دست ندامت آب‌کرد</p></div>
<div class="m2"><p>بعد ازین هم‌کاش بگدازد پشیمانی مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می‌روم از خویش در اندیشهٔ باز آمدن</p></div>
<div class="m2"><p>همچو عمر رفته یارب برنگردانی مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غیر الفت برنتابد صافی آیینه‌ام</p></div>
<div class="m2"><p>می‌کند تا خار و خس در دیده مژگانی مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این چمن یارب به خون غلتیدهٔ بیدادکیست</p></div>
<div class="m2"><p>کرد حیرانی چوشبنم چشم قربانی مرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جلوه مشتاقم بهشت ودوزخم منظورنیست</p></div>
<div class="m2"><p>می‌روم از خویش در هرجاکه می‌خوانی‌مرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون شرارم ساز پیدایی حیا ارشادکرد</p></div>
<div class="m2"><p>یعنی از خود چشم پوشانید عریانی مرا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>می‌رود از موج بر باد فنا نقش حباب</p></div>
<div class="m2"><p>تیغ خونخوارست بیدل چین پیشانی مرا</p></div></div>