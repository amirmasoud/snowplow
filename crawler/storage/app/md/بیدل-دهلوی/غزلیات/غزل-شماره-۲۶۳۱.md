---
title: >-
    غزل شمارهٔ ۲۶۳۱
---
# غزل شمارهٔ ۲۶۳۱

<div class="b" id="bn1"><div class="m1"><p>امروزکیست مست تماشای آینه</p></div>
<div class="m2"><p>کز ناز موج می‌زند اجزای آینه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیوانهٔ جمال تو گر نیست از چه رو</p></div>
<div class="m2"><p>جوهرکشیده سلسله در پای آینه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در حسرت بهار خطت ‌گرد می‌کند</p></div>
<div class="m2"><p>جوهر به جای سبزه ز صحرای آینه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>موقوف جلوهٔ ‌گل شبنم بهار توست</p></div>
<div class="m2"><p>جوش‌ گهر ز موجهٔ دریای آینه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از شرم آنکه آب نشد از نظاره‌ات</p></div>
<div class="m2"><p>گرداب خجلت است سراپای آینه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد عمر صرف جلوه‌پرستی‌، ولی چه سود</p></div>
<div class="m2"><p>نگرفت بینوا دل ما جای آینه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز حیرت آنچه هست متاع‌ کدورت است</p></div>
<div class="m2"><p>در عشق بعد از این من و سودای آینه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با خوی زشت صحبت روشندلان مخواه</p></div>
<div class="m2"><p>زنگی خجل شود به تماشای آینه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حسن و هزار نسخهٔ نیرنگ در بغل</p></div>
<div class="m2"><p>ما و دلی و یک ورق انشای آینه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روزی‌ که داد عرض نزاکت میان یار</p></div>
<div class="m2"><p>افتاد مو به دیدهٔ بینای آینه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چندان که چشم باز کنی جلوه می‌دهد</p></div>
<div class="m2"><p>اسمی‌ست ششجهت ز مسمای آینه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل به هر دلی ندهند آرزوی داغ</p></div>
<div class="m2"><p>اسکندر است باب تمنای آینه</p></div></div>