---
title: >-
    غزل شمارهٔ ۲۱۶۴
---
# غزل شمارهٔ ۲۱۶۴

<div class="b" id="bn1"><div class="m1"><p>ببین به ساز و مپرس از ترانه‌ای‌ که ندارم</p></div>
<div class="m2"><p>توان به دیده شنیدن فسانه‌ای که ندارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به سعی بازوی تسلیم در محیط توکل</p></div>
<div class="m2"><p>شناورم به امید کرانه‌ای که ندارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به رنگ شعلهٔ تصویر سخت بی پر و بالم</p></div>
<div class="m2"><p>چها نسوخته‌ام از زبانه‌ای که ندارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزار چاک دل آغوش چیده‌ام به تخیل</p></div>
<div class="m2"><p>هواپرست چه گیسوست شانه‌ای که ندارم‌؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به چاره سازی وهم تعلقم متحیر</p></div>
<div class="m2"><p>مگر جنون زند آتش به خانه‌ای‌ که ندارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فسون‌کمند هوس نیست بی‌بضاعتی من</p></div>
<div class="m2"><p>کسی ‌کلاغ نگیرد به دانه‌ای که ندارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به عزم بی‌جهتی‌ گم نکرده‌ام ره مقصد</p></div>
<div class="m2"><p>خطا ندوخته‌ام بر نشانه‌ای که ندارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دگر چه پیش توان برد در ادبگه نازش</p></div>
<div class="m2"><p>به غیر آینه بودن بهانه‌ای ‌که ندارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لوای فتنه‌ کشیده‌ست تا به دامن محشر</p></div>
<div class="m2"><p>نفس شمار دو ساعت زمانه‌ای که ندارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فغان‌ که بست به بالم هزار شعله تپیدن</p></div>
<div class="m2"><p>نشیمنی که نبود آشیانه‌ای که ندارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر به دیر کبابم‌، وگر به‌کعبه خرابم</p></div>
<div class="m2"><p>من کشیده سر از آستانه‌ای که ندارم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز یأس بیدلی‌ا‌م ‌گل نکرد شوخی آهی</p></div>
<div class="m2"><p>نفس چه ریشه دواند ز دانه‌ای که ندارم</p></div></div>