---
title: >-
    غزل شمارهٔ ۱۱۹۳
---
# غزل شمارهٔ ۱۱۹۳

<div class="b" id="bn1"><div class="m1"><p>هرچند خودنمایی تخت و حشم نباشد</p></div>
<div class="m2"><p>در عرض بی‌حیایی آیینه‌ کم نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش از خیال هستی باید در عدم زد</p></div>
<div class="m2"><p>این دستگاه خجلت‌کاو یک دو دم نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>موضوع ‌کسوت جود دامن‌فشانیی هست</p></div>
<div class="m2"><p>در بند آستین‌ها دست کرم نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خوان این بزرگان دستی بشوی و بگذر</p></div>
<div class="m2"><p>کانجا ز خوردنیها غیر از قسم نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حیف است ننگ افلاس دامان مردگیرد</p></div>
<div class="m2"><p>تا ناخنی‌ست در دست کس بی‌درم نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غفلت هزار رنگ است در کارگاه اجسام</p></div>
<div class="m2"><p>چون چشم خواب پا را م‌ژگان بهم نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی‌انتظار نتوان از وصل‌ کام دل برد</p></div>
<div class="m2"><p>شادی چه قدر دارد جایی‌که غم نباشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روزی‌دو، این‌تب و تاب‌باید غنمیت انگاشت</p></div>
<div class="m2"><p>ای راحت انتظاران‌، هستی‌، عدم نباشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل داغ سرنوشت است از انفعال تقدیر</p></div>
<div class="m2"><p>تا سرنگون نگردد خط در قلم نباشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در عرصه‌ای‌ که بالد گرد ضعیفی ما</p></div>
<div class="m2"><p>مژگان بلندکردن کم از علم نباشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از ما سراغ ما کن‌، وهم دویی رها کن</p></div>
<div class="m2"><p>جایی‌که ما نباشیم آیینه هم نباشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر دم زدن در اینجا صدکفر و دین مهیاست</p></div>
<div class="m2"><p>دل معبد تماشاست‌، دیر و حرم نباشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از شاخ بید گیرید معیار بی‌بریها</p></div>
<div class="m2"><p>کاین بار برندارد دوشی که خم نباشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عمری‌ست‌ گوهر ما رفته‌ست از کف ما</p></div>
<div class="m2"><p>این آبله ببینید زیر قدم نباشد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وحشت‌کمین نشسته‌ست ‌گرد هزار مجنون</p></div>
<div class="m2"><p>مگذار پا به خاکم تا دیده نم نباشد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو عمر رفته بیدل پر بی‌نشان سراغم</p></div>
<div class="m2"><p>جز دست سوده ما را نقش قدم نباشد</p></div></div>