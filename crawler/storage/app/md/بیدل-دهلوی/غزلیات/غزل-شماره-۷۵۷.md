---
title: >-
    غزل شمارهٔ ۷۵۷
---
# غزل شمارهٔ ۷۵۷

<div class="b" id="bn1"><div class="m1"><p>زین عبارات جنون تحقیق بی‌ناموس نیست</p></div>
<div class="m2"><p>شیشه ‌گو صد رنگ‌ توفان‌ کن پری طاووس نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اتحاد آیینه‌دار، رنگ اضدادست و بس</p></div>
<div class="m2"><p>هر کجا لبیک وادزدد، نفس ناقوس نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لفظ و معنی‌ گیر خواهی ظاهر و باطن تراش</p></div>
<div class="m2"><p>رشته‌ای جز شمع در پیراهن فانوس نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا تجدد جلوه دارد شبههٔ معنی بجاست</p></div>
<div class="m2"><p>کس چه فهمد این عبارتها یکی مأنوس نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دامن صحرای مطلب بسکه خشک افتاده است</p></div>
<div class="m2"><p>آبروها بر زمین می‌ریزد و محسوس نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سراغ رفتگان دل جمع باید داشتن</p></div>
<div class="m2"><p>کان همه آواز پا، جز در کف افسوس نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در محبت مرگ هم چون زندگی دام وفاست</p></div>
<div class="m2"><p>این‌ورق هرچند برگردد،‌خطش‌معکوس نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تشنه‌لب باید گذشت از وصل معشوقان هند</p></div>
<div class="m2"><p>هیچ ننگی در برهمن‌زادگان چون بوس نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کار پیچ و تاب موجم با گهر افتاده است</p></div>
<div class="m2"><p>آنچه می خواهد تمنا در دل مایوس نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسکه بیدل سازناموس محبت نازک است</p></div>
<div class="m2"><p>شیشهٔ اشکی‌که رنگش بشکنی بی‌کوس نیست</p></div></div>