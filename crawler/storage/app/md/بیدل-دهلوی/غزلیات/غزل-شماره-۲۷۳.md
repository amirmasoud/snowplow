---
title: >-
    غزل شمارهٔ ۲۷۳
---
# غزل شمارهٔ ۲۷۳

<div class="b" id="bn1"><div class="m1"><p>کدامین نشئه بیرون داد راز سینهٔ مینا</p></div>
<div class="m2"><p>که عکس موج می‌شد جوهرآیینهٔ مینا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان صاف ست از زنگ‌کدورت سینهٔ مینا</p></div>
<div class="m2"><p>که می‌تابد چو جوهر نشئه از آیینهٔ مینا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سزدگرگوش ساغر آشنای این نواگردد</p></div>
<div class="m2"><p>که راز میکشان‌گل‌کرده است از سینهٔ مینا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کدورت با صفای مشرب ما برنمی‌آید</p></div>
<div class="m2"><p>نبندد صورت تمثال زنگ آیینهٔ مینا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به تمکینم چسان خفّت رساندکوشش‌گردون</p></div>
<div class="m2"><p>ببازد بیستون رنگ وقار ازکینهٔ مینا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تهی دستیم چون ساغر خدا را ساقیا رحمی</p></div>
<div class="m2"><p>به روی بخت ما بگشا درگنجینهٔ مینا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوشا صبحی‌که شاه ملک عشرت جلوه ریزآید</p></div>
<div class="m2"><p>به زرین تخت جام از قصر زنگارینهٔ مینا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مقیم‌گوشهٔ دل باش‌،‌گر آسودگی خواهی</p></div>
<div class="m2"><p>که حیرت می‌شود سیماب در آیینهٔ مینا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همان خاک سیه اکنون لباس دل به بر دارد</p></div>
<div class="m2"><p>صفا مفت است منگرکسوت پارینهٔ مینا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بهار نشئه‌ام‌، عیش دماغم‌، بادهٔ صافم</p></div>
<div class="m2"><p>مرا باید نشاندن در دل بی‌کینهٔ مینا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ادب‌کوشید در ضبط خود وتعطیل شد نامش</p></div>
<div class="m2"><p>به روز وصل ما ماند شب آدینهٔ مینا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به آفت سخت نزدیکند نازک طینتان بیدل</p></div>
<div class="m2"><p>بود با سنگ و آتش الفت دیرینهٔ مینا</p></div></div>