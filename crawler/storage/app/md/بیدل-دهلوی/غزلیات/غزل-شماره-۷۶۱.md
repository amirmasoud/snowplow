---
title: >-
    غزل شمارهٔ ۷۶۱
---
# غزل شمارهٔ ۷۶۱

<div class="b" id="bn1"><div class="m1"><p>درگلشن هوس‌که سراغ‌گلیش نیست</p></div>
<div class="m2"><p>گریأس نوحه سربکند بلبلیش نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن ساز فتنه‌ای‌که تو محشر شنیده‌ای</p></div>
<div class="m2"><p>زیر و بم توگر نبود غلغلیش نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیدیم حسن ساختهٔ اعتبار جاه</p></div>
<div class="m2"><p>هرگاه بی‌نطاقه شود کاکلیش نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یارب به حال مفلسی خواجه رحم‌کن</p></div>
<div class="m2"><p>بیچاره خربه عرض چه نازد جلیش نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آزادگان ز فکر رعونت منزه‌اند</p></div>
<div class="m2"><p>باگردن آنکه ساز ندارد غلیش نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صیادی هوس‌، چقدر ننگ فطرت است</p></div>
<div class="m2"><p>شاهین حرص می‌پرد وچنگلیش نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر انفعال‌، عشرت این بزم چیده‌اند</p></div>
<div class="m2"><p>تاشیشه‌سرنگون نشودقلقلیش‌نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تدبیر رستگاری جاوید، نیستی‌ست</p></div>
<div class="m2"><p>این بحرغیرکشتی واژون پلیش نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از قطره تا محیط وبال تعلق است</p></div>
<div class="m2"><p>بیدل خوش‌آنکه الفت جزووکلیش نیست</p></div></div>