---
title: >-
    غزل شمارهٔ ۲۷۸۱
---
# غزل شمارهٔ ۲۷۸۱

<div class="b" id="bn1"><div class="m1"><p>زین گلستان نیستم محتاج دامن چیدنی</p></div>
<div class="m2"><p>می‌برد چون رنگم آخر بی‌قدم‌ گردیدنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ندامت‌کاری ذوق طرب غافل نی‌ام</p></div>
<div class="m2"><p>صدگریبان می‌درد بوی گل از بالیدنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمرها بر خوبش بالد شیشه تا خالی شود</p></div>
<div class="m2"><p>گردن بسیار می‌خواهد به‌سر غلتیدنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا به‌کی دزدد تری یارب خط پیشانی‌ام</p></div>
<div class="m2"><p>خشک شد این لب به امید زمین بوسیدنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پنجهٔ بیکار منع خار خار دل نکرد</p></div>
<div class="m2"><p>کاش باشد سینه بر برگ حنا مالیدنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مست و مخموری نمی‌باشد همه محو دلیم</p></div>
<div class="m2"><p>سنگ این‌کهسار و مینا در بغل خوابیدنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون حباب از خامشی مگذر که حسن عافیت</p></div>
<div class="m2"><p>خفته است آیینه در دست قفس دزدیدنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عیب جویی طبع ما را دشمن آرام‌کرد</p></div>
<div class="m2"><p>خواب بسیارست اگر باشد مژه پوشیدنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خودنمایی هر چه باشد خارج آهنگ حیاست</p></div>
<div class="m2"><p>چون‌گره بیرون تاریم از همین بالیدنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیده از نقش تماشاخانهٔ‌گردون مپوش</p></div>
<div class="m2"><p>دستگاه آن پری زین شیشه دارد دیدنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غیر عریانی به هرکسوت‌که می‌دوزیم چشم</p></div>
<div class="m2"><p>دارد از هر رشته بر ما زیر لب خندیدنی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بی‌دلیل عجز بیدل هیچ جا نتوان رسید</p></div>
<div class="m2"><p>سعی‌کن چندانکه آید پیش پا لغزیدنی</p></div></div>