---
title: >-
    غزل شمارهٔ ۱۰۲۹
---
# غزل شمارهٔ ۱۰۲۹

<div class="b" id="bn1"><div class="m1"><p>حرص اگر بر عطش غلو دارد</p></div>
<div class="m2"><p>شرم آبی دگر به جو دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوشهٔ دامن قناعت گیر</p></div>
<div class="m2"><p>خاک این وادی آبرو دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خار خار خیال پوچ بلاست</p></div>
<div class="m2"><p>آه زان دل که آرزو دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست این بحر بی شنای حباب</p></div>
<div class="m2"><p>سر بی‌مغز هم‌کدو دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رنگ گل بی ‌تو بی ‌دماغم کرد</p></div>
<div class="m2"><p>خون این زخم تازه بو دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست می‌باید از جهان شستن</p></div>
<div class="m2"><p>رفع آلایش این وضو دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساز اقبال بی شکستی نیست</p></div>
<div class="m2"><p>چیستی اعتبار مو دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی‌رواج جهان عنصری‌ایم</p></div>
<div class="m2"><p>جنس ما گرد چارسو دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اوج بنیاد ما ، نگونساری‌ست</p></div>
<div class="m2"><p>موی سر، سوی خاک رو، دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از نفس رست و رفت به باد</p></div>
<div class="m2"><p>ریشهٔ ما همین نمو دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برکه نالد نیاز ما یارب</p></div>
<div class="m2"><p>دادرس پر به ناز خو دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خاک ناگشته پاک نتوان شد</p></div>
<div class="m2"><p>زاهدان‌! آب هم وضو دارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هرکجاییم زین چمن دوریم</p></div>
<div class="m2"><p>ما و من رنگ و بوی و دارد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیدل این‌حرف و صوت چیزی‌نیست</p></div>
<div class="m2"><p>خامشی معنی مگو دارد</p></div></div>