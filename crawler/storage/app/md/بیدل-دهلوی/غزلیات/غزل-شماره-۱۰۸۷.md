---
title: >-
    غزل شمارهٔ ۱۰۸۷
---
# غزل شمارهٔ ۱۰۸۷

<div class="b" id="bn1"><div class="m1"><p>از تغافل زدنی ترک سبب بایدکرد</p></div>
<div class="m2"><p>روز خود را به غبار مژه شب باید کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرد وارستگی‌هکوی فنا باید بود</p></div>
<div class="m2"><p>خاک در دیدهٔ اندوه ظرب باید کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو آیینه اگر دست دهد صافی دل</p></div>
<div class="m2"><p>جوهر ناطقه شیرازهٔ لب باید کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کهنه مشق خط امواج سرابیم همه</p></div>
<div class="m2"><p>عینک از آبلهٔ پای طلب باید کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اشک اگر شیشه از این دست بهم برچیند</p></div>
<div class="m2"><p>مژه را روکش بازار حلب باید کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا شودطبع توآیینهٔ تحقیق وفا</p></div>
<div class="m2"><p>خلق را صیقل زنگار غضب باید کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دم صبحی مگر افسون تباشیر دمد</p></div>
<div class="m2"><p>شمع ما را همه شب خدمت تب باید کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیده‌ای را که چمن‌پرور دیدار تو نیست</p></div>
<div class="m2"><p>به تماشای‌گل و لاله ادب باید کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنقدر شیفتهٔ نرگس خمّار توام</p></div>
<div class="m2"><p>که‌.ز خاکم به قدح آب عنب باید کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یک تحیر دو جهان در نظرت می‌سوزد</p></div>
<div class="m2"><p>آتش از خانهٔ آیینه طلب بایدکرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل و دانش همه در عشق بتان باید باخت</p></div>
<div class="m2"><p>خویش را بیدل دیوانه لقب بایدکرد</p></div></div>