---
title: >-
    غزل شمارهٔ ۶۱۰
---
# غزل شمارهٔ ۶۱۰

<div class="b" id="bn1"><div class="m1"><p>چون سپند آرام جسم دردناکم ناله است</p></div>
<div class="m2"><p>برق جولانی‌که خواهد سوخت پاکم ناله است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد گریبان نسخهٔ رسوایی‌ام اما هنوز</p></div>
<div class="m2"><p>یک الف ازانتخاب مشق چاکم ناله است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از علمداران یأسم‌، کار اقبالم بلند</p></div>
<div class="m2"><p>کز سمک تا عالم اوج سماکم ناله است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کس نمی‌فهمد زبان خاکساریهای من</p></div>
<div class="m2"><p>ورنه هرگردی‌که می‌خیزد ز خاکم ناله است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازگداز عافیت؟کی برون جوشیده‌ام</p></div>
<div class="m2"><p>بادهٔ درد دلم رگهای تاکم ناله است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا نفس برخویش بالد یأس عریان می‌شود</p></div>
<div class="m2"><p>بی‌رخت صد پیرهن سامان چاکم ناله است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کس بدآموز نزاکت فهمی الفت مباد</p></div>
<div class="m2"><p>خامشی هم بی‌تواز بهرهلاکم ناله است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گم شدم از خویش تحریک دل آوازم نداد</p></div>
<div class="m2"><p>این جرس بیدل نمی‌دانم چراکم ناله است</p></div></div>