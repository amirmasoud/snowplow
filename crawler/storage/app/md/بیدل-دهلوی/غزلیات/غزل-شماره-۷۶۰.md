---
title: >-
    غزل شمارهٔ ۷۶۰
---
# غزل شمارهٔ ۷۶۰

<div class="b" id="bn1"><div class="m1"><p>برق با شوقم شراری بیش نیست</p></div>
<div class="m2"><p>شعله طفل نی‌سواری بیش نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آرزوهای دو عالم دستگاه</p></div>
<div class="m2"><p>ازکف خاکم غباری بیش نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون شرارم یک نگه عرض است و بس</p></div>
<div class="m2"><p>آینه اینجا دچاری بیش نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لاله وگل زخمی خمیازه‌اند</p></div>
<div class="m2"><p>عیش این‌گلشن خماری بیش نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا به‌کی نازی به حسن عاریت</p></div>
<div class="m2"><p>ما و من آیینه‌داری بیش نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌رود صبح و اشارت می‌کند</p></div>
<div class="m2"><p>کاین‌گلستان خنده‌واری بیش نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا شوی آگاه فرصت رفته است</p></div>
<div class="m2"><p>وعدهٔ وصل انتظاری بیش نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دست از اسباب جهان برداشتن</p></div>
<div class="m2"><p>سعی‌گر مرد است‌کاری بیش نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون سحر نقدی‌که در دامان تست</p></div>
<div class="m2"><p>گربیفشانی غباری بیش نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چند در بند نفس فرسودنست</p></div>
<div class="m2"><p>محوآن دامی‌که تاری بیش نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صد جهان معنی به لفظ ماگم است</p></div>
<div class="m2"><p>این نهانها آشکاری بیش نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غرقهٔ وهمیم ورنه این محیط</p></div>
<div class="m2"><p>از تنک آبی‌کناری بیش نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای شرر از همرهان غافل مباش</p></div>
<div class="m2"><p>فرصت ما نیزباری بیش نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیدل این‌کم‌همتان بر عز و جاه</p></div>
<div class="m2"><p>فخرها دارند و عاری بیش نیست</p></div></div>