---
title: >-
    غزل شمارهٔ ۲۱۷۹
---
# غزل شمارهٔ ۲۱۷۹

<div class="b" id="bn1"><div class="m1"><p>ز بس ضعیف مزاج جهان تدبیرم</p></div>
<div class="m2"><p>چو صبح تا نفس از دل به لب رسد پیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هنوز جلوهٔ من در فضای بیرنگیست</p></div>
<div class="m2"><p>خیالم و به نگه کرده‌اند زنجیرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی به هستی موهوم من چه پردازد</p></div>
<div class="m2"><p>که همچو خواب فراموش ننگ تعبیرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز فرق تا به قدم حیرتم نمی‌دانم</p></div>
<div class="m2"><p>گشوده‌اند به روی‌که چشم تصویرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو اخگرم به‌گره نیست غیر خاکستر</p></div>
<div class="m2"><p>تبم اگر شکند سر به سر تباشیرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه نغمه داشت نی تیر او که در طلبش</p></div>
<div class="m2"><p>چو رنگ می‌رود از خویش خون نخجیرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیاه‌بخت محبت بهارها دارد</p></div>
<div class="m2"><p>به هند نازفروش سوادکشمیرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگاه دیدهٔ آهوست وحشتی ‌که مراست</p></div>
<div class="m2"><p>به روز هم نتوان‌ کرد قطع شبگیرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو جاده رنگ بنای مرا شکستی نیست</p></div>
<div class="m2"><p>به خشت نقش قدم‌کرده‌اند تعمیرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مپرس ز آتش شوق‌ که داغم ای ناصح</p></div>
<div class="m2"><p>که چون سپند مبادا به ناله درگیرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من آن ستمزده طفلم‌که مادر ایام</p></div>
<div class="m2"><p>به جام دیدهٔ قربانی افکند شیرم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنان به ضعف عنان رفته ازکفم بیدل</p></div>
<div class="m2"><p>که من ز خویش روم‌ گر کشند تصویرم</p></div></div>