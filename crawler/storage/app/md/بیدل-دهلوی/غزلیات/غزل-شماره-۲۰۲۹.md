---
title: >-
    غزل شمارهٔ ۲۰۲۹
---
# غزل شمارهٔ ۲۰۲۹

<div class="b" id="bn1"><div class="m1"><p>کو جهد که چون بوی‌ گل از هوش خود افتم</p></div>
<div class="m2"><p>یعنی دو سه‌ گام آنسوی آغوش خود افتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سوختنم شمع صفت عرض نیازیست</p></div>
<div class="m2"><p>مپسندکه در آتش خاموش خود افتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خاک ره افتاده‌ام اما چه خیالست</p></div>
<div class="m2"><p>کز یاد شب وعده فراموش خود افتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر دگران چند کنم وعظ طرازی</p></div>
<div class="m2"><p>ای‌ کاش شوم حرفی‌ و در گوش خود افتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کو لغزش پایی‌ که به ناموس وفایت</p></div>
<div class="m2"><p>بار دو جهان‌ گیرم و بر دوش خود افتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عمریست‌ که دریا به‌کنار است حبابم</p></div>
<div class="m2"><p>آن به‌ که در اندیشهٔ آغوش خود افتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شور طلبم مانع تحقیق وصالست</p></div>
<div class="m2"><p>خمخانهٔ رازم اگر از جوش خود افتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای بخت سیه‌روز چرا سایه نکردی</p></div>
<div class="m2"><p>تا در قدم سرو قباپوش خود افتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیدل همه تن بار خودم چون نفس صبح</p></div>
<div class="m2"><p>بر دوش که افتم اگر از دوش خود افتم</p></div></div>