---
title: >-
    غزل شمارهٔ ۲۳۸
---
# غزل شمارهٔ ۲۳۸

<div class="b" id="bn1"><div class="m1"><p>غیر وحدت برنتابد همت عرفان ما</p></div>
<div class="m2"><p>دامن خویش است چون صحراگل دامان ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوق در بی‌دست‌وپایی نیست‌مأیوس طلب</p></div>
<div class="m2"><p>چون قلم سعل قدم می‌بالد از مژگان ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معنی اظهار صبح از وحشت انشا کرده‌اند</p></div>
<div class="m2"><p>نامهٔ آهیم بیتابی همان عنوان ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین دبستان مصرع زلفی مسلسل خوانده‌ایم</p></div>
<div class="m2"><p>خامشی مشکل که‌گردد مقطع دیوان ‌ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وحشت ما زین چمن محمل‌کش صدعبرت است</p></div>
<div class="m2"><p>نشکند رنگی‌که چینش نیست در دامان ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یار در آغوش و نام او نمی‌دانم‌که چیست</p></div>
<div class="m2"><p>سادگی ختم است چون آیینه‌بر نسیان ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در تپیدن‌گاه امکان شوخی نظاره‌ایم</p></div>
<div class="m2"><p>از غباری می‌توان ره بست بر جولان ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مدعا از دل به لب نگذشته می‌سوزد نفس</p></div>
<div class="m2"><p>اینقدر دارد خموشی آتش پنهان ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مغثنم دار ای شرر جولانگه آغوش سنگ</p></div>
<div class="m2"><p>تنگی فرضت بغل واکرده در میدان ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جلوه درکار است و ما با خود قناعت‌کرده‌ایم</p></div>
<div class="m2"><p>به‌که بر روی توباشد چشم ما حیران ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل از حیرت زبان درد دل فهمیدنی‌ست</p></div>
<div class="m2"><p>آیسنه می‌پوشد امشب نالهٔ عریان ما</p></div></div>