---
title: >-
    غزل شمارهٔ ۴۴۶
---
# غزل شمارهٔ ۴۴۶

<div class="b" id="bn1"><div class="m1"><p>یاد آن جلوه ز چشمم‌ گره اشک‌ گشاست</p></div>
<div class="m2"><p>شوق دیدار پرستان چقدر آینه ‌زاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نذر کویی ‌ست غبار به هوا رفته‌ ی من</p></div>
<div class="m2"><p>باخبر باش که دنبالهٔ این سرمه‌رساست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیری‌ام سر خط تحقیق فنا روشن‌ کرد</p></div>
<div class="m2"><p>حلقهٔ قامت من عینک نقش‌ کف پاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلوت‌آرای خیال ادب دیداریم</p></div>
<div class="m2"><p>هرکجا آینه‌ای هست غبار دل ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنقدرسعی به آبادی ما لازم نیست</p></div>
<div class="m2"><p>خانهٔ چشم به امداد نگاهی برپاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاک هم شوخی‌اندز غباری دارد</p></div>
<div class="m2"><p>شرط افتادگی آن است ‌که نتوان برخاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آتش از چهرهٔ زرین اثر زر ندهد</p></div>
<div class="m2"><p>دین به دنیا مفروشید که دنیا دنیاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غنچه زان پیش که آهنگ نفس ساز کند</p></div>
<div class="m2"><p>جرس قافلهٔ رنگ طرب‌، یأس نو است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شوکت حسن‌ که لشکرکش نازست اینجا</p></div>
<div class="m2"><p>عمرها شد صف مژگان بتان رو به قفاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بینوا نیست دل از جوش‌کدورت بیدل</p></div>
<div class="m2"><p>شیشه را سنگ ستم آینهٔ حسن صداست</p></div></div>