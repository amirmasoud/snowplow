---
title: >-
    غزل شمارهٔ ۷۷۹
---
# غزل شمارهٔ ۷۷۹

<div class="b" id="bn1"><div class="m1"><p>تویی‌که غیر دلم هیچ‌جا مقام تو نیست</p></div>
<div class="m2"><p>اگر نگین دمد آفاق جای نام تو نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهات‌کون و مکان چون نگاه اشک‌آلود</p></div>
<div class="m2"><p>هنوز آبله پایی و نیم‌گام تو نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدم به کسوت ناز حدوث می‌بالد</p></div>
<div class="m2"><p>خمارها همه جز نشئهٔ دوام تو نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خرام قاصد رازت ازآن سوی من وماست</p></div>
<div class="m2"><p>نفس هم آنهمه معنی رس پیام تونیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزار آینه در دل شکست تمکینت</p></div>
<div class="m2"><p>ولی چه سودکه تمثال شوق رام تو نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فضولی هوست ننگ اعتبار مباد</p></div>
<div class="m2"><p>به‌کام تست جهان‌گر جهان به‌کام تو نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیاز‌پروری ناز سحرپردازی‌ست</p></div>
<div class="m2"><p>به خود منازکه جز خواجگی غلام تو نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به پرگشایی عنقا نفس چه رشته‌تند</p></div>
<div class="m2"><p>چه شدکه دانهٔ دل ریشه‌گرد دام تو نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تأملت نشود گر محاسب اعمال</p></div>
<div class="m2"><p>کسی دگر هوس انشای انتقام تو نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو آسمان زتو برتر خیال نتوان بست</p></div>
<div class="m2"><p>چه منظری‌که هوا هم به پشت‌بام تو نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سواد رازتو روشن به نورفطرت توست</p></div>
<div class="m2"><p>چراغ وهم‌کس آیینه‌دار شام تو نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چوآفتاب به هرجا رسی سراغ خودی</p></div>
<div class="m2"><p>نشان پاگل رعنایی خرام‌تو نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو خواه مست‌گمان باش خواه محویقین</p></div>
<div class="m2"><p>شراب جام تو غیر از شراب جام تو نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پیام عشق به‌گوش هوس مخوان بیدل</p></div>
<div class="m2"><p>سخن اگر سخن اوست جزکلام تو نیست</p></div></div>