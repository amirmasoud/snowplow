---
title: >-
    غزل شمارهٔ ۱۶۰۵
---
# غزل شمارهٔ ۱۶۰۵

<div class="b" id="bn1"><div class="m1"><p>سران ز نسخهٔ تسلیم باب بردارید</p></div>
<div class="m2"><p>جبین به خاک نهید انتخاب بردارید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جمال مقصد سعی جهان معاینه است</p></div>
<div class="m2"><p>ز نقش پا نفسی گر نقاب بردارید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمارتی اگر از آب و گل توان برداشت</p></div>
<div class="m2"><p>دل از خیال جهان خراب بردارید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزار موج در این بحر قاصد هوس است</p></div>
<div class="m2"><p>ز نامهٔ همه مهر حباب بردارید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سواد وادی امکان سراب تشنه‌لبی است</p></div>
<div class="m2"><p>ز چشمه‌سار گداز دل آب بردارید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جنون حکم قضا تیغ برکف استاده است</p></div>
<div class="m2"><p>سری‌ که نیست به‌ گردن ز خواب بردارید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا به سایهٔ بخت سیه شکر خوابی‌ست</p></div>
<div class="m2"><p>ز خاک من علم آفتاب بردارید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هجوم خنده نم چشم می‌کند ایجاد</p></div>
<div class="m2"><p>به هرگلی‌که رسید این‌گلاب بردارید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کرشمهٔ نگهش از سوال مستغنی‌ست</p></div>
<div class="m2"><p>نظر به سرمه ‌کنید و جواب بردارید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به جرم‌کج‌نظری دور گرد تحقیقم</p></div>
<div class="m2"><p>خط خطاست گر از تیر تاب بردارید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز هستی‌ام غلطی رفته در حساب عدم</p></div>
<div class="m2"><p>مرا چو نقطهٔ شک زبن‌کتاب بردارید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غباربیدل ما راکه دستگیر شود</p></div>
<div class="m2"><p>اگر نسیم توان شد صواب بردارید</p></div></div>