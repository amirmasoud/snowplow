---
title: >-
    غزل شمارهٔ ۱۳۸۲
---
# غزل شمارهٔ ۱۳۸۲

<div class="b" id="bn1"><div class="m1"><p>برای خاطرم غم آفریدند</p></div>
<div class="m2"><p>طفیل چشم من یم آفریدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو صبح آنجا که من پرواز دارم</p></div>
<div class="m2"><p>قفس با بال توأم آفریدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عرق‌گل کرده‌ام از شرم هستی</p></div>
<div class="m2"><p>مرا از چشم شبنم آفریدند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گهر موج آورد آیینه جوهر</p></div>
<div class="m2"><p>دل بی‌آرزو کم آفریدند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهان خونریز بنیاد است هشدار</p></div>
<div class="m2"><p>سر سال از محرم آفریدند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وداع غنچه را گل نام‌ کردند</p></div>
<div class="m2"><p>طرب را ماتم غم آفریدند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>علاجی نیست داغ بندگی را</p></div>
<div class="m2"><p>اگر بیشم وگرکم آفریدند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کف خاکی که بر بادش توان داد</p></div>
<div class="m2"><p>به خون‌گل‌کرده آدم آفریدند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طلسم زندگی الفت بنا نیست</p></div>
<div class="m2"><p>نفس را یک قلم رم آفریدند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر عالم برای خویش پیداست</p></div>
<div class="m2"><p>برای من مرا هم آفریدند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه سان تابم سر از فرمان تسلیم</p></div>
<div class="m2"><p>که چون ابرویم از خم آفریدند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دلم بیدل ندارد چاره از داغ</p></div>
<div class="m2"><p>نگین را بهر خاتم آفریدند</p></div></div>