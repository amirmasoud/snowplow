---
title: >-
    غزل شمارهٔ ۱۱۴۰
---
# غزل شمارهٔ ۱۱۴۰

<div class="b" id="bn1"><div class="m1"><p>بهار حیرت‌ست اینجا نه‌گل نی جام می‌خیزد</p></div>
<div class="m2"><p>ز هستی تا عدم یک دیدهٔ بادام می‌خیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خروش فتنه زان چشم جنون آشام می‌خیزد</p></div>
<div class="m2"><p>که جوش ‌الامان از جان خاص و عام می‌خیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلیل شوق نیرنگ تماشای که شد یارب</p></div>
<div class="m2"><p>که آب از آینه چون اشک بی‌آرام می‌خیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه امکان‌ست صید خاکساران فنا کردن</p></div>
<div class="m2"><p>به راه انتظار ما غبار از دام می‌خیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به‌طوف مدعا چون ناله عریان شو که عاشق را</p></div>
<div class="m2"><p>فسردنها ز طوف جامهٔ احرام می‌خیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هوای پختگی داری کلاه فقر سامان‌کن</p></div>
<div class="m2"><p>که از تاج سرافرازان خیال خام می‌خیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز نادانی حباب باده می‌نامند بیدردان</p></div>
<div class="m2"><p>به دیدار تو چشم حیرتی‌ کز جام می‌خیزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نفس در دل شکستم شعله زد دود دماغ من</p></div>
<div class="m2"><p>هوا در خانه می‌دزدم غبار از بام می‌خیزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رمیدن برنمی‌تابد هوای عالم الفت</p></div>
<div class="m2"><p>چو جوش سبزه ‌گرد این بیابان رام می‌خیزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درین مزرع که دارد ریشه از ساز گرفتاری</p></div>
<div class="m2"><p>اگر یک دانه افتد بر زمین صد دام می‌خیزد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دماغ جاده‌پیمایی ندارد رهرو شوقت</p></div>
<div class="m2"><p>شرر اول قدم از خود به جای‌گام می‌خیزد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ر بس در آرزوی می سرا ‌پا حسرتم بیدل</p></div>
<div class="m2"><p>نفس تا بر لبم آید صدای جام می‌خیزد</p></div></div>