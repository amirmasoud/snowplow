---
title: >-
    غزل شمارهٔ ۲۳۱۲
---
# غزل شمارهٔ ۲۳۱۲

<div class="b" id="bn1"><div class="m1"><p>منم آن نشئهٔ فطرت‌ که خمستان قدیم</p></div>
<div class="m2"><p>دارد از جوهر من سیر دماغ تعظیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندمیدم ز بهاری‌ که چمن ساز نفس</p></div>
<div class="m2"><p>صبح ایجاد مرا خنده نماید تعلیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیش از آن است در آیینهٔ من مایهٔ نور</p></div>
<div class="m2"><p>که به هر ذره دو خورشید نمایم تقسیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بهاری‌که منش غنچهٔ تمکین بندم</p></div>
<div class="m2"><p>وضع شبنم نکشد تهمت اجزای نسیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شوقم آن دم‌ که پر افشاند به صحرای عقول</p></div>
<div class="m2"><p>گشت یک عالم ارواح در اندیشه جسیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قصر سودای جهان پایهٔ قدری می‌خواست</p></div>
<div class="m2"><p>چترزد دود دماغ من وشد عرش عظیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فطرتم ریخت برون شور وجوب و امکان</p></div>
<div class="m2"><p>این دو تمثال در آیینهٔ من بود مقیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به گشاد مژه‌ام انجمن آرای حدوث</p></div>
<div class="m2"><p>به شکست نفسم آینه پرداز قدیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شعله بودم من و می‌سوخت نفس شمع‌ مسیح‌</p></div>
<div class="m2"><p>من قدح می‌زدم و مست طلب بود کلیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیش ز ایجاد به امید ظهور احمد</p></div>
<div class="m2"><p>داشت نور احدم درکنف حلقهٔ میم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رفت آن نشئه ز یادم به‌ فسون من و تو</p></div>
<div class="m2"><p>برد آن هوش ز مغزم الم خلد و جحیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خاکبوسی‌ست‌ کنون سر خط پیشانی ناز</p></div>
<div class="m2"><p>عشق‌ کرد آخرم این نسخهٔ عبرت تسلیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حلقه‌ام کرد سجود در یکتایی خویش</p></div>
<div class="m2"><p>حیرت آورد بهم دایرهٔ علم و علیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نفس ماهی دریای وفا قلاب است</p></div>
<div class="m2"><p>جیم‌ گل می‌کند از نون چو نمایند دو نیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بحر فطرت به‌گهر سازی من می‌گوید</p></div>
<div class="m2"><p>گرچه صیقل زده‌ام آینهٔ اشک یتیم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خلقی اینجاست به عبرتکدهٔ‌ کعبه و دیر</p></div>
<div class="m2"><p>پیش پا خوردهٔ هر سنگ ز جولان سقیم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زین خطوطی‌ که نفس‌ کوشش باطل دارد</p></div>
<div class="m2"><p>جام جم تا به‌ کجا کهنه نسازد تقویم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زبن شکستی ‌که به مو می‌رسد از چینی دل</p></div>
<div class="m2"><p>سر فغفور چسان شرم نپوشد به گلیم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>طاق نسیانی از این انجمن احداث‌ کنیم</p></div>
<div class="m2"><p>تا دم شیشهٔ دل ماند از آفات سلیم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بیدل افسانه غیرم سبق آهی هست</p></div>
<div class="m2"><p>می‌کند اینقدرم سیر گریبان تعلیم</p></div></div>