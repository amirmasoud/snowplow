---
title: >-
    غزل شمارهٔ ۱۷۲۳
---
# غزل شمارهٔ ۱۷۲۳

<div class="b" id="bn1"><div class="m1"><p>خودسری‌گرد دل تنگ نگردد هرگز</p></div>
<div class="m2"><p>غنچه تا وانشود رنگ نگردد هرگز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرمهٔ چشم ادب‌پرور جمعیت ماست</p></div>
<div class="m2"><p>ساز ما خفت آهنگ نگردد هرگز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی‌سخن عذر ضعیفی همه جا مقبول است</p></div>
<div class="m2"><p>سعی رنج قدم لنگ نگردد هرگز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سایه خفت‌کش اندیشهٔ پامالی نیست</p></div>
<div class="m2"><p>خاکساری سبب ننگ نگردد هرگز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترک هستی کن اگر صافی دل می‌خواهی</p></div>
<div class="m2"><p>از نفس‌، آینه بیرنگ نگردد هرگز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دور وهمی‌ست‌که بر جام سپهر افتاده‌ست</p></div>
<div class="m2"><p>بی‌تکلف سر بی‌ننگ نگردد هرگز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکه دارد تپشی در جگر از شعلهٔ عشق</p></div>
<div class="m2"><p>گر همه سنگ شود دنگ نگردد هرگز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پستی طبع‌که چون آبلهٔ پا ازلی‌ست</p></div>
<div class="m2"><p>گر تناسخ زند اورنگ نگردد هرگز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فکر روزی‌ست‌که پرمی‌کشد از مغز وقار</p></div>
<div class="m2"><p>آسیا تا نشود سنگ نگردد هرگز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کلفت هر دو جهان درگره حسرت ماست</p></div>
<div class="m2"><p>دل اگر جمع شود تنگ نگردد هرگز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل از طورکلامت همه حیرت‌زده‌ایم</p></div>
<div class="m2"><p>در بهاری‌که تویی رنگ نگردد هرگز</p></div></div>