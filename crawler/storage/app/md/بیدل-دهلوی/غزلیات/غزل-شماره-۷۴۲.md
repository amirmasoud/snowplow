---
title: >-
    غزل شمارهٔ ۷۴۲
---
# غزل شمارهٔ ۷۴۲

<div class="b" id="bn1"><div class="m1"><p>برچهرهٔ آثارجهان رنگ سبب نیست</p></div>
<div class="m2"><p>چون آتش یاقوت‌که تب دارد و تب نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وهم‌است‌که در ششجهتش ریشه دویده‌ست</p></div>
<div class="m2"><p>سرسبزی این مزرعه بی‌برگ‌کنب نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشمی به تأمل نگشوده‌ست نگاهت</p></div>
<div class="m2"><p>بروضع جهان‌گر عجبت نیست عجب نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا زنده‌ای امید غنا هرزه خیالی‌ست</p></div>
<div class="m2"><p>این آمد ورفت نفست غیرطلب نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شغل هوس خواجه مگرگم شود از مرگ</p></div>
<div class="m2"><p>این‌حکهٔ هنگامهٔ حرص است جرب نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در هیچ صفت داد فضولی نتوان داد</p></div>
<div class="m2"><p>تا دل هوس‌انشاست جهان جای طلب نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دور است شکست دل از آرایش تعمیر</p></div>
<div class="m2"><p>این‌کارگه شیشهٔ رنگ است حلب نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تسلیم‌وسر وبرگ فضولی چه جنون است</p></div>
<div class="m2"><p>گر ریشه‌کند دانه‌ات ازکشت ادب نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کامل‌ادبان قانع یک سجده جبینند</p></div>
<div class="m2"><p>مشتاق زمین‌بوس هوس تشنهٔ لب نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی‌باده دل از زنگ طبیعت نتوان شست</p></div>
<div class="m2"><p>افسوس‌که در آینه‌ها آب عنب نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل غم روز سیه ازما نتوان برد</p></div>
<div class="m2"><p>چین سحراینجا شکن دامن شب نیست</p></div></div>