---
title: >-
    غزل شمارهٔ ۱۵۸۶
---
# غزل شمارهٔ ۱۵۸۶

<div class="b" id="bn1"><div class="m1"><p>از حقهٔ دهانش هر گه سخن برآید</p></div>
<div class="m2"><p>آپ از عقیق ریزد در از عدن برآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از شوق صبح تیغش مانند موج شبنم</p></div>
<div class="m2"><p>گلهای زخم دل را آب از دهن‌برآید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از روی داغ حسرت‌ گر پنیه باز گیرم</p></div>
<div class="m2"><p>با صد زبانه چون شمع از پیرهن برآید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیند ز بار ‌خجلت چون تیشه سرنگونی</p></div>
<div class="m2"><p>بر بیستون دردم‌ گر کوهکن برآید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وصف بهار حسنش‌گر در چمن بگویم</p></div>
<div class="m2"><p>چون بلبل ازگلستان‌ گل نعره‌زن برآید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تار نگه رساند نظاره را به رویش</p></div>
<div class="m2"><p>هرکس به بام خورشید با این رسن برآید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیدل ‌کلام حافظ شد هادی خیالم</p></div>
<div class="m2"><p>دارم امید آخر مقصود من برآید</p></div></div>