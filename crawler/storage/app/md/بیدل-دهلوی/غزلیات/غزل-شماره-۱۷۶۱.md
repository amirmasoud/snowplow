---
title: >-
    غزل شمارهٔ ۱۷۶۱
---
# غزل شمارهٔ ۱۷۶۱

<div class="b" id="bn1"><div class="m1"><p>چو ابر و بحر ز لاف سخا پشیمان باش</p></div>
<div class="m2"><p>کرم‌ کن و عرق انفعال احسان باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بساط این چمن آیینه‌داری ادب است</p></div>
<div class="m2"><p>چو شبنم آب شو اما به چشم حیران باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حضور آبلهٔ پا اگر به دست افتد</p></div>
<div class="m2"><p>قدم بر افسر شاهی گذار و سلطان باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زخون خود چو حنا رنگ تحفه پردازد</p></div>
<div class="m2"><p>گل وسیلهٔ پابوس خوش خرامان باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه لازم است ‌کشی رنج انتظاریها</p></div>
<div class="m2"><p>جگر چو صبح به چاکی ده و گلستان باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز مشرب خط و خال بتان مشو غافل</p></div>
<div class="m2"><p>به حسن معنی‌ کفر آبروی ایمان باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هوا پرستی جمعیت از فسرده دلی است</p></div>
<div class="m2"><p>چو گرد بر سر این خاکدان پریشان باش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کجاست وسعت دیگر سواد امکان را</p></div>
<div class="m2"><p>چو شعله در جگر سنگ داغ جولان باش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز فکر عقدهٔ دل چون‌ گهر مشو غافل</p></div>
<div class="m2"><p>دمی که ناخن موجت نماند دندان باش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دلیل مطلب عشاق بودن آسان نیست</p></div>
<div class="m2"><p>به نامه‌ای که ندارد سواد عنوان باش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به ساز حادثه هم نغمه بودن آرام است</p></div>
<div class="m2"><p>اگر زمانه قیامت ‌کند تو توفان باش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به جز فنا نمک ساز زندگانی نیست</p></div>
<div class="m2"><p>تمام شیفتهٔ اینی و اندکی آن باش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در این چمن همه عاجز نگاه دیداریم</p></div>
<div class="m2"><p>تو نیز یک دونگه در قطار مژگان باش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چه ننگ دلق و چه فخر کلاه غفلت توست</p></div>
<div class="m2"><p>به هر لباس ‌که باشی ز خویش عریان باش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دلیل وحدت از افسون‌ کثرتی بیدل</p></div>
<div class="m2"><p>همین قدر که به جسم آشنا شدی جان باش</p></div></div>