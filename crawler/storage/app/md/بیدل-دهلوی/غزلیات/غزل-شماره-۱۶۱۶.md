---
title: >-
    غزل شمارهٔ ۱۶۱۶
---
# غزل شمارهٔ ۱۶۱۶

<div class="b" id="bn1"><div class="m1"><p>منتظران بهار بوی شکفتن رسید</p></div>
<div class="m2"><p>مژده به ‌گلها برید یار به‌ گلشن رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لمعهٔ مهر ازل بر در و دیوار تافت</p></div>
<div class="m2"><p>جام تجلی به دست نور ز ایمن رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نامه و پیغام را رسم تکلف نماند</p></div>
<div class="m2"><p>فکر عبارت کراست معنی روشن رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق ز راه خیال‌ گرد الم پاک رفت</p></div>
<div class="m2"><p>خار و خس وهم غیر رفت و به‌ گلخن رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صبر من نارسا باج ز کوشش ‌گرفت</p></div>
<div class="m2"><p>دست به دل داشتم مژدهٔ دامن رسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عیش و غم روزگار مرکز خود واشناخت</p></div>
<div class="m2"><p>نغمه به احباب ساخت نوحه به دشمن رسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مطلع همت بلند مزرع اقبال سبز</p></div>
<div class="m2"><p>ریشه به نخل آب داد دانه به خرمن رسید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زین چمنستان کنون بستن مژگان خطاست</p></div>
<div class="m2"><p>آینه صیقل زنید دیده به دیدن رسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بردم از این نوبهار نشئهٔ عمر دوبار</p></div>
<div class="m2"><p>دیده‌ام از دیده رست دل به دل من رسید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سرو خرامان ناز حشر چه نیرنگ داشت</p></div>
<div class="m2"><p>هر چه ز من رفته بود با به مسکن رسید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل از اسرار عشق هیچکس آگاه نیست</p></div>
<div class="m2"><p>گاه گذشتن گذشت وقت رسیدن رسید</p></div></div>