---
title: >-
    غزل شمارهٔ ۲۳۵۳
---
# غزل شمارهٔ ۲۳۵۳

<div class="b" id="bn1"><div class="m1"><p>فرصت‌کمین پرواز چون نالهٔ سپندیم</p></div>
<div class="m2"><p>چندان که سر به جیبیم چین گشتهٔ کمندیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طاقت به زیر گردون خفت شکار پستی است</p></div>
<div class="m2"><p>هرگاه پر شکستیم زین آشیان بلندیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرواز خاک غافل در دیده‌ها غبار است</p></div>
<div class="m2"><p>عمری‌ست از فضولی ردیم ناپسندیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امروز هیچکس نیست شایستهٔ ستودن</p></div>
<div class="m2"><p>مضمون تهمتی چند با ناقصان چه بندیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بس رواج دارد افسانه‌های باطل</p></div>
<div class="m2"><p>چون حرف حق درین بزم تلخیم گرچه قندیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نامحرمان چه دانند شأن عسل چه دارد</p></div>
<div class="m2"><p>در خانه‌ها حلاوت بیرون در گزندیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ظلم است مرهم لطف از ما دریغ کردن</p></div>
<div class="m2"><p>چون داغ سوزناکیم چون زخم دردمندیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از اشک شمع گیرید معیار عبرت ما</p></div>
<div class="m2"><p>آن سر که می‌کشیدیم آخر به پا فکندیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شیرینی هوسها فرهاد کرد ما را</p></div>
<div class="m2"><p>فرصت به جان کنی رفت دل از جهان نکندیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آفاق کسوت شور تا کی به وهم بافد</p></div>
<div class="m2"><p>ماتم خروش عبرت زین نیلگون پرندیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل درین ستمگاه از درد ناامیدی</p></div>
<div class="m2"><p>بسیار گریه ‌کردیم اکنون بیا بخندیم</p></div></div>