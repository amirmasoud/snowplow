---
title: >-
    غزل شمارهٔ ۲۶۹۲
---
# غزل شمارهٔ ۲۶۹۲

<div class="b" id="bn1"><div class="m1"><p>به جلوهٔ تو نگه را ز حیرت اظهاری</p></div>
<div class="m2"><p>ببالد از مژه انگشتهای زنهاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چوگردباد اسیران حلقهٔ زلفت</p></div>
<div class="m2"><p>کشند محمل پرواز برگرفتاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگه ز پردهٔ آن چشم ناتوان پیداست</p></div>
<div class="m2"><p>به رنگ شخص اجل در لباس بیماری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زبان خار ندانم چه‌گفت درگوشش</p></div>
<div class="m2"><p>که چشم از آبله‌ام برد سیل خونباری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه ممکنست دل ازگریه‌ام بجا ماند</p></div>
<div class="m2"><p>ز سنگ نیز نیاید در آب خودداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلیل عافیت شمع عرض زنهارست</p></div>
<div class="m2"><p>تو نیز جز به سرانگشت ‌گام نشماری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گهر ز سنگدلی بار خاطر دریاست</p></div>
<div class="m2"><p>به روی‌آب‌نشین چون کف از سبکباری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نظر به خاک ره انتظار دوخته‌ام</p></div>
<div class="m2"><p>بس است مردمک چشم دام بیداری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به آن مراتب عجزم‌که همچو نقش قدم</p></div>
<div class="m2"><p>کند بنای مرا سایه سقف و دیواری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در آن بساط که من مرکز فسردگی‌ام</p></div>
<div class="m2"><p>رمد ز شعلهٔ جواله سعی پرگاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غبار هستی‌ام اجزای وحشت عنقاست</p></div>
<div class="m2"><p>چها به باد دهی تا مرا بهم آری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز بسکه ساغر بزم ادب زدم بیدل</p></div>
<div class="m2"><p>چو شمع ناله‌گره‌گشت وکرد منقاری</p></div></div>