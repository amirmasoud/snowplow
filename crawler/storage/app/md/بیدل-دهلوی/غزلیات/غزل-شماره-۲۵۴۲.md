---
title: >-
    غزل شمارهٔ ۲۵۴۲
---
# غزل شمارهٔ ۲۵۴۲

<div class="b" id="bn1"><div class="m1"><p>بعد مردن گر همین داغست وحشت‌زای من</p></div>
<div class="m2"><p>خاک هم خالی در آتش می‌نماید جای من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر به صد چاه جهنم سرنگون غلتم خوش است</p></div>
<div class="m2"><p>در دل مأیوس خود یارب نلغزد پای من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد جنون شور قیامت می‌تپد درگرد یاس</p></div>
<div class="m2"><p>از ادبگاه خموشی تا لب گویای من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آرزوها بسکه در جیب نفس خون‌ کرده‌ام</p></div>
<div class="m2"><p>بال طاووس است اگر موج است در دریای من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کو تأمل تا به کنه نسخهء خاکم رسد</p></div>
<div class="m2"><p>بی‌غباری نیست خط صفحهٔ سیمای من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای هوس چون‌گل فریب عشرت از رنگم مده</p></div>
<div class="m2"><p>خون پروازیست در بال قفس فرسای من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روزگاری چشم مجنون داشت مشق گردشی</p></div>
<div class="m2"><p>گردباد است این زمان در مکتب صحرای من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دستگاه عبرت اینجا جز تعلق هیچ نیست</p></div>
<div class="m2"><p>می‌گشاید چشم من چون شمع خار پای من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کیست رنگ معنی از لفظم تواند کرد فرق</p></div>
<div class="m2"><p>باده چون آب‌ گهر جوشید با مینای من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیدهٔ آهو نگردد تهمت آلود بیاض</p></div>
<div class="m2"><p>صبح یک خواب فراموش‌ست از شبهای من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هستی موهوم عرض بی‌نشانی هم نداد</p></div>
<div class="m2"><p>ازنفس خون شد صدای شهپر عنقای من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می‌کشم چون صبح از اسباب این وحشت‌سرا</p></div>
<div class="m2"><p>تهمت ربطی‌ که نتوان بست بر اجزای من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فرصت ازکف رفت و دل‌کاری نکرد، افسوس عمر</p></div>
<div class="m2"><p>کاروان بگذشت و من در خواب مردم‌، وای من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کارگاه حیرتم بیدل خموشی باف نیست</p></div>
<div class="m2"><p>ناله دارد تار و پود صورت دیبای من</p></div></div>