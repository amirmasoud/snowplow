---
title: >-
    غزل شمارهٔ ۱۲۵
---
# غزل شمارهٔ ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>سوار برق عمرم‌، نیست برگشتن عنانم را</p></div>
<div class="m2"><p>مگر نام توگیرم تا بگرداند زبانم را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عدم کیفیتم خاصیت نقش قدم دارم</p></div>
<div class="m2"><p>خرامی تا به زیرپای خود یابی نشانم را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به رنگ شمع‌گر شوقت عیار طاقتم‌گیرد</p></div>
<div class="m2"><p>کند پرواز رنگ از مغز خالی استخوانم را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به‌مردن نیز از وصف خرامت لب نمی‌بندم</p></div>
<div class="m2"><p>نگیرد سکته طرف دامن اشعار روانم را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غباری می‌فروشم در سر بازار موهومی</p></div>
<div class="m2"><p>مبادا چشم بستن تخته‌گرداند دکانم را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به تدبیر دگر نتوان نشان مدعا جستن</p></div>
<div class="m2"><p>شکست‌دل مگرچون موج زه‌بنددکمانم‌را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مخواه ای مفلسی ذلت‌کش تسلیم دونانم</p></div>
<div class="m2"><p>زمین تا چند زیرپا نشاند آسمانم را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز شرم‌عافیت محرومی‌جهدم چه‌می‌پرسی</p></div>
<div class="m2"><p>عرق بیرون این دریا نمی‌خواهدکرانم را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز درد دل درتن صحرا نبستم بار امیدی</p></div>
<div class="m2"><p>جرس نالید و آتش زد متاع‌کاروانم را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نمی‌دانم ز بیداد دل سنگین کجا نالم</p></div>
<div class="m2"><p>شنیدن نیست آن دوشی‌که بردارد فغانم را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تراوشهای آثارکرم هم موقعی دارد</p></div>
<div class="m2"><p>مباد اسراف سازد منفعل روزی رسانم را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شبی چون شمع حرفی ازگداز عشق سرکردم</p></div>
<div class="m2"><p>مکیدن ازلب هر عضو بوسی زد دهانم را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نفس بودم‌جنون پیمای‌دشت بی‌نشان‌تازی</p></div>
<div class="m2"><p>دل از آیینه گردیدن‌گرفت آخر عنانم را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز اسرار دهانی حرف چندی‌کرده‌ام انشا</p></div>
<div class="m2"><p>به‌جز شخص عدم‌بیدل‌که‌می‌فهمد زبانم‌را</p></div></div>