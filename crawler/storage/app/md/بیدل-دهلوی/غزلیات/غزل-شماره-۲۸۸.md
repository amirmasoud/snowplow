---
title: >-
    غزل شمارهٔ ۲۸۸
---
# غزل شمارهٔ ۲۸۸

<div class="b" id="bn1"><div class="m1"><p>حیرت دل گر نپردازد به ضبط‌کارها</p></div>
<div class="m2"><p>ناله می‌بندد به فتراک تپش‌کهسارها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عالمی بر وهم پیچیده‌ست مانند حباب</p></div>
<div class="m2"><p>جز هوا نبود سری در زیر این دستارها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست زندانگاه امکان سنگ راه وحشتم</p></div>
<div class="m2"><p>چون نگه سامان عینک دارم از دیوارها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عندلیبان را ز شرم ناله‌ام مانند شمع</p></div>
<div class="m2"><p>شعلهٔ آواز بست آیینهٔ منقارها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از خرام‌موج می چشم قدح‌داغ است‌و بس</p></div>
<div class="m2"><p>دارد این نقش قدم خمیازهٔ رفتارها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>موجهای این محیط آخرگهر خواهد شدن</p></div>
<div class="m2"><p>سبحه خوابیده‌ست در پیچ و خم زنارها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسکه درهرگل زمین ذوق تماشا خاک شد</p></div>
<div class="m2"><p>پشه می‌آرد برون نظاره ازگلزارها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فقر در هرجا غرور یأس سامان می‌کند</p></div>
<div class="m2"><p>کجکلاهی می‌زند موج از شکست‌کارها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواب‌راحت بستهٔ مژگان‌به‌هم آورد‌ن‌است</p></div>
<div class="m2"><p>سایه می‌گردند از افتادن این دیوارها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون‌سحر سعی خروشم‌قابل اظهار نیست</p></div>
<div class="m2"><p>به‌که برسازم شکست رنگ بندد تارها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل‌این‌گلشن ز بس‌منظورحسن افتاده‌است</p></div>
<div class="m2"><p>ناز مژگان می‌دمد گر دسته‌بندی خارها</p></div></div>