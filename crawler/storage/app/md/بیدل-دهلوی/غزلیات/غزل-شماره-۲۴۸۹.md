---
title: >-
    غزل شمارهٔ ۲۴۸۹
---
# غزل شمارهٔ ۲۴۸۹

<div class="b" id="bn1"><div class="m1"><p>به سعی بی‌نشانی آنسوی امکان رهی واکن</p></div>
<div class="m2"><p>پر افشانست همت آشیان در چشم عنقاکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازین صحرای وحشت هر چه برداری قدم باشد</p></div>
<div class="m2"><p>سری از خواب اگر برداشتی اندیشهٔ پا کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به یک مژگان زدن از خود چو حیرت می‌توان رفتن</p></div>
<div class="m2"><p>اگرگامی نداری جنبش نظاره پیدا کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز رفع‌ گرد هستی می‌توان صد صبح بالیدن</p></div>
<div class="m2"><p>نسیم امتحان شوگوشه‌ای زبن پرده بالاکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گداز قطره بحری را ز خود لبریز می‌بیند</p></div>
<div class="m2"><p>جو دل صهبا شو و از ذره تا خورشید میناکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درین مزرع چه لازم آب دادن تخم بیکاری</p></div>
<div class="m2"><p>ز حاصل‌گر به استغنا زدی آفت تقاضاکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عمارتهای آب و خاک نتوان بر فلک بردن</p></div>
<div class="m2"><p>اگر خواهی بنای رنگ ریزی ناله بر پاکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرفتم ‌گلشنی ای بیخبر رنگ قبولت ‌کو</p></div>
<div class="m2"><p>همه یک قطرهٔ خون باش اما در دلی جاکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خیال ما شراب بی‌خمار نیستی دارد</p></div>
<div class="m2"><p>اگر از بزم همت ساغری داری پر از ماکن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غرور سرکشی در آفتابت چند بنشاند</p></div>
<div class="m2"><p>فروتن باش یعنی سایهٔ دیواری انشا کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر چشمت ز اسرار محبت سرمهٔ دارد</p></div>
<div class="m2"><p>بببن موی سر مجنون و سیر زلف لیلاکن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کمینگاه تعلق هاست خواب غفلت بید‌ل</p></div>
<div class="m2"><p>به یک واکردن مژگان جهانی را ز سر واکن</p></div></div>