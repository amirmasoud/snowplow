---
title: >-
    غزل شمارهٔ ۱۶۶
---
# غزل شمارهٔ ۱۶۶

<div class="b" id="bn1"><div class="m1"><p>مآل‌کار نقصانهاست هر صاحب‌کمالی را</p></div>
<div class="m2"><p>اگر ماهت‌کنند از دست نگذاری هلالی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رمیدنها ز اوضاع جهان طرز دگر دارد</p></div>
<div class="m2"><p>به‌وحشت پیش باید برد ازین صحرا غزالی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به‌بقش نیک وبد روشندلان رادست رد نبود</p></div>
<div class="m2"><p>کف آیینه می‌چیندگل بی‌انفعالی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بساط گفتگو طی کن که در انجام کار آخر</p></div>
<div class="m2"><p>به حکم خامشی پیچیدن است این فرش قالی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وبال رنج پیری برنتابد صاحب جوهر</p></div>
<div class="m2"><p>چنار آتش زند ناچار دلق کهنه‌سالی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درین وادی‌که‌خاک است اعتبار جهل و دانشها</p></div>
<div class="m2"><p>غباری بر هوادان قصر فطرتهای عالی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به وحدتخانهٔ دل غیر دل چیزی نمی‌گنجد</p></div>
<div class="m2"><p>براین آیینه جز تهمت مدان نقش مثالی را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر خرسندی دل آبیار مزرعت باشد</p></div>
<div class="m2"><p>چوتخم آبله نشو ونماکن پایمالی را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به چنگ اغنیا دامان فقر آسان نمی‌افتد</p></div>
<div class="m2"><p>که چینی خاک‌گردد تا شود قابل سفالی را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه امکان است بیدل منعم از غفلت برون آید</p></div>
<div class="m2"><p>هجوم خواب خرگوش است یکسر شیر قالی را</p></div></div>