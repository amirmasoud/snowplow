---
title: >-
    غزل شمارهٔ ۶۸۱
---
# غزل شمارهٔ ۶۸۱

<div class="b" id="bn1"><div class="m1"><p>قصر غناکه عالم تحقیق نام اوست</p></div>
<div class="m2"><p>دا‌من ز خویش بر زدنی سیر بام اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر برگ این چمن رقمی دارد از بهار</p></div>
<div class="m2"><p>عالم نگین‌تراشی سودای نام اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پر انتظار نامه‌بران هوس مکش</p></div>
<div class="m2"><p>خود را به خود دمی‌ که رساندی پیام اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وحشت ز غیر خاطر ما جمع‌کرده است</p></div>
<div class="m2"><p>از خود رمیدنی‌ که نداریم رام اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آه از ستمکشی ‌که درین صیدگاه وهم</p></div>
<div class="m2"><p>عمری به خود تنید ونفهمید دام اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا چند ناز ا‌نجمن‌آرایی غرور</p></div>
<div class="m2"><p>ای غافل از حیا عرق ما به جام اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز مرگ نیست چاره آفاق زندگی</p></div>
<div class="m2"><p>چون زحم شیشه‌ای که‌گداز التیام‌اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر هرچه واکنی مژه بی‌انفعال نیست</p></div>
<div class="m2"><p>خوابی‌ ست آگهی ‌که جهان احتلام اوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شرع یقین‌، دمی که دهد فتوی حضور</p></div>
<div class="m2"><p>عین سواست آنچه حلال و حرام اوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شرط نماز عشق به ارکان نمی‌کشد</p></div>
<div class="m2"><p>کونین و یک محرف همت سلام اوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای فتنه قامت‌، این چه غرور است در سرت</p></div>
<div class="m2"><p>تیغی کشیده‌ای که قیامت نیام اوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فرداست ‌کز مزار من آیینه می‌دمد</p></div>
<div class="m2"><p>خاکم چمن دماغ‌ کمین خرام اوست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>افسانه‌ خیال به پایان نمی‌رسد</p></div>
<div class="m2"><p>عالم تمام یک سخن ناتمام اوست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیدل زبان پردهٔ تحقیق نازک است</p></div>
<div class="m2"><p>آهسنه‌ گوش نه‌ که خموشی‌ کلام اوست</p></div></div>