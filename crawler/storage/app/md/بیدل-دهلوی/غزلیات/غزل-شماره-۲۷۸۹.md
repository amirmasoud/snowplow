---
title: >-
    غزل شمارهٔ ۲۷۸۹
---
# غزل شمارهٔ ۲۷۸۹

<div class="b" id="bn1"><div class="m1"><p>اگر سیر زمین داری وگر افلاک می‌بینی</p></div>
<div class="m2"><p>دماغ فرصت امروزست فردا خاک می‌بینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پری نفشانده‌ای تا وانماید رنگ این باغت</p></div>
<div class="m2"><p>قفس پرورده‌ای گل ازکمین چاک می‌بینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نخواهی غره ی آرایش علم و عمل گشتن</p></div>
<div class="m2"><p>خیالی چند دور از عالم ادراک می‌بینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نپنداری شود آب وضوی باطنت حاصل</p></div>
<div class="m2"><p>به فالی گر فشاری دامن نمناک می‌بینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هنوز از موج می بویی ندارد جام این محفل</p></div>
<div class="m2"><p>خط پیمانه در اندیشه‌های تاک می‌بینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه دنیا کلفت‌آموزست و نه عقبا غم‌اندوزست</p></div>
<div class="m2"><p>ستم ها از جنون فطرت بی باک می‌بینی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکار وهم گردونی‌، به زنجیر چه افسونی</p></div>
<div class="m2"><p>که هر سو می‌روی یک حلقهٔ فتراک می‌بینی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که برد آن طول و پهنایت‌، چه شد دریادلی‌هایت</p></div>
<div class="m2"><p>که چون گوهر غنا در عقدهٔ امساک می‌بینی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اقامت آرزو، هیهات با اسباب جوشیدن</p></div>
<div class="m2"><p>به قدر آشیان‌، رنج خس و خاشاک می‌بینی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رقم ساز تعلق وقف عبرت سر خطی دارد</p></div>
<div class="m2"><p>که تا لغزید مژگان هر چه دیدی پاک می‌بینی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غم تدبیر لذات از مزاجت‌گم نشد بیدل</p></div>
<div class="m2"><p>به دندان سنگ زن پر زحمت مسواک می‌بینی</p></div></div>