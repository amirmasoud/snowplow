---
title: >-
    غزل شمارهٔ ۱۵۳۱
---
# غزل شمارهٔ ۱۵۳۱

<div class="b" id="bn1"><div class="m1"><p>حسن بی‌شرم ازهجوم بوالهوس محشر شود</p></div>
<div class="m2"><p>ایمن ازگلچین نباشد باغ چون بی‌در شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساده‌لوحیهای دل عمری‌ست سرمشق غناست</p></div>
<div class="m2"><p>آرزویارب مباد این صفحه را مسطر شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک ارباب نظر سامان نور آگهی است</p></div>
<div class="m2"><p>سرمه بایدکرد اگر آیینه خاکستر شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شوخی حرف از زبان شرمسار ما مخواه</p></div>
<div class="m2"><p>طایر از پرواز می‌ماند چو بالش تر شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صفحهٔ دل را به داغی می‌توان آیینه ‌کرد</p></div>
<div class="m2"><p>لفظ ازیک نقطه صاحب معنی دیگرشود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آسمان مشکل به آسانی دهد پرداز دل</p></div>
<div class="m2"><p>بحر توفان‌ها کند تا قطره‌ا‌ی گوهر شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناتوانی سر متاب از جاده تسلیم عشق</p></div>
<div class="m2"><p>خاک چون درسایه ی خورشید خوابد زر شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سایه‌وار از بیکسیها حیله‌جوی غیرتم</p></div>
<div class="m2"><p>بر سرم‌ گر خاک هم دستی‌ کشد افسر شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حسرت مخموری آن چشم میگون برده‌ام</p></div>
<div class="m2"><p>سرنوشت خاک من یارب خط ساغرشود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای جنون تعمیر ازتشویش آسودن برآ</p></div>
<div class="m2"><p>جان سختت چند خشت این‌کهن منظر شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آرمیدن‌کو؟‌گرفتم ساعتی چون‌گردباد</p></div>
<div class="m2"><p>در سر خاکت هوایی پیچد و افسر شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل از سرگشتگانی منزلت آوارگی‌ست</p></div>
<div class="m2"><p>اضطرابت چند چون ریگ روان رهبر شود</p></div></div>