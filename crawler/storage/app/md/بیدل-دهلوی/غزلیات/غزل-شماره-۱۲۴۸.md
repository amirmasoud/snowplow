---
title: >-
    غزل شمارهٔ ۱۲۴۸
---
# غزل شمارهٔ ۱۲۴۸

<div class="b" id="bn1"><div class="m1"><p>دل شهرهٔ تسلیم ز ضبط‌ نفسم شد</p></div>
<div class="m2"><p>قلقل ‌به ‌لب‌شیشه ‌شکستن ‌جرسم شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرواز ضعیفان تب و تاب مژه دارد</p></div>
<div class="m2"><p>بالی نگشودم ‌که نه چاک قفسم شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فریاد زگیرایی قلاب محبت</p></div>
<div class="m2"><p>هر سوکه‌گذشتم مژه او عسسم شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا چاشنی بوسی ازآن لعل‌گرفتم</p></div>
<div class="m2"><p>شیرینی لذات دو عالم مگسم شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم به نوایی رسم از ساز سلامت</p></div>
<div class="m2"><p>دل زمزمه تعلیم نبی بی‌نفسم شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کو خواب عدم ‌کز تب و تابم ‌کند ایمن</p></div>
<div class="m2"><p>چون شمع ‌گشاد مژه در دیده خسم شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر هرخس و خاری‌ که در این باغ رسیدم</p></div>
<div class="m2"><p>شرم نرسیدن ثمرپیشرسم شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرتا قدمم در عرق شمع فرورفت</p></div>
<div class="m2"><p>یارب زکجا سیر گریبان هوسم شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عنقای جهان خودم اما چه توان کرد</p></div>
<div class="m2"><p>این یک دو نفس الفت بیدل قفسم شد</p></div></div>