---
title: >-
    غزل شمارهٔ ۵۵
---
# غزل شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>درین وادی چسان آرام باشد کاروان‌ها را</p></div>
<div class="m2"><p>که همدوشی‌ست با ریگ روان سنگ نشان‌ها را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه دل بندد دل آگاه بر معمورهٔ امکان</p></div>
<div class="m2"><p>که فرصت گردش چشمی‌ست دور آسمان‌ها را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز موج بحر کم‌سامانی عالم تماشا کن</p></div>
<div class="m2"><p>که تیر بی‌پر از آه حباب است این کمان‌ها را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جگر خون مگر بر اعتبار دل بیفزاید</p></div>
<div class="m2"><p>که قیمت نیست غیر از خونبها یاقوت کان‌ها را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به تدبیر از غم کونین ممکن نیست وارستن</p></div>
<div class="m2"><p>مگر سوزد فراموشی متاع این دکان‌ها را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>علاج پیچ وتاب حرص نتوان یافتن ورنه</p></div>
<div class="m2"><p>به جوش آورده فکر حاجت ما بحر و کان‌ها را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به یک پرواز خاکستر شدیم از شعلهٔ غیرت</p></div>
<div class="m2"><p>سلام توتیای ماست چشم آشیان‌ها را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به بال وبر دهد پرواز مرغان رنج بی‌تابی</p></div>
<div class="m2"><p>تپیدن بیش نبود حاصل از گفتن زبان‌ها را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو رنگ رفته‌، یاد آشیان سودی نمی‌بخشد</p></div>
<div class="m2"><p>درین وادی که برگشتن نمی‌باشد عنان‌ها را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرانی کی کشد پای طلب در وادی شوقت</p></div>
<div class="m2"><p>که جسم اینجا سبک‌روحی کند تعلیم جان‌ها را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من و عرض نیاز، از عزت و خواری چه می‌پرسی</p></div>
<div class="m2"><p>که نقش سجده بیش از صدر خواهد آستان‌ها را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنین کز کلک ما رنگ معانی می‌چکد بیدل</p></div>
<div class="m2"><p>توان گفتن رگ ابر بهار این ناودان‌ها را</p></div></div>