---
title: >-
    غزل شمارهٔ ۱۱۲۵
---
# غزل شمارهٔ ۱۱۲۵

<div class="b" id="bn1"><div class="m1"><p>چنین‌گر طیع‌بیدر‌ت‌به‌خورد و خواب‌می‌سازد</p></div>
<div class="m2"><p>به چشمت اشک را هم‌گوهر نایاب می‌سازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ضعیفی دامنت دارد خروش درد پیدا کن</p></div>
<div class="m2"><p>که‌ هرجا رشته‌ٔ سازی‌ست با مضراب می‌سازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درین میخانه فرش سجده باید بود مستان را</p></div>
<div class="m2"><p>که موج باده از خم تا قدح محراب می‌سازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جنون کن در بنای خانمان هوش آتش زن</p></div>
<div class="m2"><p>همین وضعت خلاص از کلفت اسباب می‌سازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نفس را الفت دل نیست جز تکلیف بیتابی</p></div>
<div class="m2"><p>که دود از صحبت آتش به پیچ و تاب می‌سازد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو صبحی‌ کز حضور آفتاب انشا کند شبنم</p></div>
<div class="m2"><p>خیال او نفس در سینهٔ من آب می‌سازد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنین کز سوز دل خاکستر ایجاد است اعضایم</p></div>
<div class="m2"><p>تب پهلوی من از بوریا سنجاب می‌سازد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به برق همت از ابرکرم قطع نظرکردم</p></div>
<div class="m2"><p>تریهای هوس کشت مرا سیراب می‌سازد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به هجران ذوق وصلی د‌ارم و بر خویش می‌بالم</p></div>
<div class="m2"><p>در آتش نیز این ماهی همان با آب می‌سازد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درین محفل ندارد بوی راحت چشم واکردن</p></div>
<div class="m2"><p>نگاه بیدماغان بیشتر با خواب می‌سازد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ندارد بزم امکان چون ضعیفی‌،‌کیمیاسازی</p></div>
<div class="m2"><p>که اجزای غرور خلق را آداب می‌سازد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تواضعهای ظالم مکر صیادی بود بیدل</p></div>
<div class="m2"><p>که میل آهنی را خم شدن قلاب می‌سازد</p></div></div>