---
title: >-
    غزل شمارهٔ ۱۰۲۰
---
# غزل شمارهٔ ۱۰۲۰

<div class="b" id="bn1"><div class="m1"><p>خامش‌نفسی خفت گوینده ندارد</p></div>
<div class="m2"><p>لبهای ز هم واشده جز خنده ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرواز رسایی‌ که بنازیم به جهدش</p></div>
<div class="m2"><p>چون رنگ به غیر از پر برکنده‌ ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواهی به عدم غوطه زن و خواه به هستی</p></div>
<div class="m2"><p>بنباد تو جز غفلت یابنده ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>معیارتک و تاز من و ما ز نفس‌گیر</p></div>
<div class="m2"><p>جز رفتن ازین مرحله آینده ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موج و کف دریای عدم سحرنگاری‌ست</p></div>
<div class="m2"><p>نادار همه دارد و دارنده ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دلق گشودیم معمای قلندر</p></div>
<div class="m2"><p>پوشیدگی این است‌که‌ کس ژنده ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیر خم زانو به هوس جمع نگردد</p></div>
<div class="m2"><p>نامحرم معنی سر افکنده ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همواری و صحرای تعین چه خیال است</p></div>
<div class="m2"><p>این تختهٔ نجار جنون رنده ندارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زین گردش رنگی که جبین ساز تماشاست</p></div>
<div class="m2"><p>آنیست که صد جامهٔ زیبنده ندارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>معشوق مزاجی‌ست ‌که این باغ تجدد</p></div>
<div class="m2"><p>یک ریشه به جز سرو خرامنده ندارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جمعیت دل خواه چه دنیا و چه عقبا</p></div>
<div class="m2"><p>موج گهر اجزا‌ی پراکنده ندارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل سخن این است تأمل کن و تن زن</p></div>
<div class="m2"><p>من خواجه طلب مردم و او بنده ندارد</p></div></div>