---
title: >-
    غزل شمارهٔ ۸۲۷
---
# غزل شمارهٔ ۸۲۷

<div class="b" id="bn1"><div class="m1"><p>هرکه را دستی ز همت بود جز بر دل نداشت</p></div>
<div class="m2"><p>دستگاه پرتو یک شمع این محفل نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل به هرنقشی‌که بستم صورت آیینه بود</p></div>
<div class="m2"><p>نسخهٔ تحقیق امکان جز خط باطل نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاجزیها را غنیمت دان که درباب طلب</p></div>
<div class="m2"><p>دست‌و پایی‌گز می‌کردیم‌گم ساحل نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>انفعالی نیست دل را ورنه درکیش حیا</p></div>
<div class="m2"><p>سنگ ‌هم‌گر آب‌می‌شد عقده ای مشکل نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زندگی در پیچ و تاب سعی بیجا مردن است</p></div>
<div class="m2"><p>از تپیدن عالمی بسمل شد و قاتل نداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیرگیهای نظر محو نقاب‌آرایی‌ست</p></div>
<div class="m2"><p>ورنه هرگز، لیلی آزاد ما، محمل نداشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غنچه‌ها بال نفس در پردهٔ دل سوختند</p></div>
<div class="m2"><p>عیش این باغ امتداد رقص یک بسمل نداشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شوخی موج ‌کرم شد انفعال جرم ما</p></div>
<div class="m2"><p>این محیط آبی برون از جبههٔ سایل نداشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همچو شبنم‌ گریه بر ما راه جولان بسته است</p></div>
<div class="m2"><p>چشم ما تا بود بی‌نم این بیابان‌ گل نداشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سرو گلزار تمنا طوق قمری در بر است</p></div>
<div class="m2"><p>گل نکرد از سینه‌ام آهی‌که داغ دل نداشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اشکم و گم‌ کرده‌ام از ضعف راه اضطراب</p></div>
<div class="m2"><p>ورنه این ره لغزش پا داشت‌گر منزل نداشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نقش او از اضطرابم در نفس صورت نبست</p></div>
<div class="m2"><p>حسن را آیینه می‌بایست و این بیدل نداشت</p></div></div>