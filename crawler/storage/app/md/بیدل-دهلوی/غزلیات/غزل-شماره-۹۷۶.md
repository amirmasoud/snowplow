---
title: >-
    غزل شمارهٔ ۹۷۶
---
# غزل شمارهٔ ۹۷۶

<div class="b" id="bn1"><div class="m1"><p>زمینگیری ز جولانم چه امکانست وادارد</p></div>
<div class="m2"><p>بروب‌رفتن ز خود چون شمع ‌ر هرعضوپا دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خط طومار یاهن آرایش مهر جفا درد</p></div>
<div class="m2"><p>به رنگ شاخ‌ گل آهم سراپا داغها دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آن وادی که من دارم کمین انتظار او</p></div>
<div class="m2"><p>غباری ‌گر تپد آواز پای آشنا دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زگل باید سراغ غنچهٔ‌گمگشته پرسیدن</p></div>
<div class="m2"><p>که از چشم تحیر رفتن دل نقش پا دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فناپروردگانیم از مزاج ما چه می‌پرسی</p></div>
<div class="m2"><p>فضای عالم موهوم هستی یک هوا دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرایت نغمهٔ عجزیم ساز آفرینش را</p></div>
<div class="m2"><p>درپن‌ محفل شکست از هرچه‌ باشد رنگ ما دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قد پیران تواضع می‌کند عیش جوانی را</p></div>
<div class="m2"><p>پل از بهر وداع سیل پشت خود دوتا دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز خوب و زشت امکان صافدل تنگی نمی‌چیند</p></div>
<div class="m2"><p>به بزم آینه عکسی اگر ره برد جا دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز حال‌گوشه‌گیر فقر ای منعم مشو غافل</p></div>
<div class="m2"><p>که خواب مخملی در رهن نقش بوریا دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز عالم نگذری بی‌دستگیریهای آزادی</p></div>
<div class="m2"><p>کسی برخیزد از دنیا که از وحشت عصا دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جهانی سرخوش آگاهی‌ست ازگردش حالم</p></div>
<div class="m2"><p>شکست رن من چون خند مینا صدا دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به رنگ آب سیر برگ برگ این چمن کردم</p></div>
<div class="m2"><p>گل داغ‌ست بیدل آنکه بویی از وفا دارد</p></div></div>