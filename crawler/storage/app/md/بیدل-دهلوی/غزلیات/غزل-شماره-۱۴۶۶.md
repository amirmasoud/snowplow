---
title: >-
    غزل شمارهٔ ۱۴۶۶
---
# غزل شمارهٔ ۱۴۶۶

<div class="b" id="bn1"><div class="m1"><p>روشندلان چو آینه بر هرچه رو کنند</p></div>
<div class="m2"><p>هم در طلسم خویش تماشای او کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پاکی چو بحر موج زند از جبینشان</p></div>
<div class="m2"><p>قومی که از گداز تمنا وضو کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آزادگان نهال گلستان ناله‌اند</p></div>
<div class="m2"><p>بر باد اگر روند نشاط نموکنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پروانه مشربان بساط وفا چو شمع</p></div>
<div class="m2"><p>اجزای خویش را به گداز آبرو کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را به زندگی ز محبت گزیر نیست</p></div>
<div class="m2"><p>نتوان گذشت گر همه با درد خو کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عنقاست در قلمرو امکان بقای عیش</p></div>
<div class="m2"><p>تاکی بهار را قفس از رنگ و بو کنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جیب مرا به نیستی انباشت روزگار</p></div>
<div class="m2"><p>چاکی‌ست صبح را که به هیچش رفو کنند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این موجها که گردن دعوی کشیده‌اند</p></div>
<div class="m2"><p>بحر حقیقتند اگر سر فروکنند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای غفلت آبروی طلب بیش از‌ این مریز</p></div>
<div class="m2"><p>عالم تمام اوست‌که را جستجوکنند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل به این طراوت اگر باشد انفعال</p></div>
<div class="m2"><p>باید جهانیان ز جبینم وضو کنند</p></div></div>