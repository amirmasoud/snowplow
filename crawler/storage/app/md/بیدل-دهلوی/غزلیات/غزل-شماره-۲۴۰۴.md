---
title: >-
    غزل شمارهٔ ۲۴۰۴
---
# غزل شمارهٔ ۲۴۰۴

<div class="b" id="bn1"><div class="m1"><p>کرد حرف بی‌نشانم عالمی را تر زبان</p></div>
<div class="m2"><p>همچو عنقا آشیانی بسته‌ام در هر زبان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وصف آن خط شوخیی داردکه در اندیشه‌اش</p></div>
<div class="m2"><p>می‌دواند ربشه‌ها موج رک گل بر زبان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به‌که عاشق حسرت دیدار در دل بشمرد</p></div>
<div class="m2"><p>موج سیلاب است اگرجوشد ز چشم‌تر زبان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مطلب دیدار حیرانم چسان گردد ادا</p></div>
<div class="m2"><p>خاص آن عالم تحیر، تاب این‌کشور زبان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهل معنی یک قلم در ضبط اسرار خودند</p></div>
<div class="m2"><p>موج ممکن نیست بیرون آرد ازگوهر زبان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی‌خموشی‌کلبهٔ دل عافیت اسباب نیست</p></div>
<div class="m2"><p>کاش ‌گردد شمع این‌ کاشانه را صرصر زبان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عافیت خواهی تبرا کن ز اظهارکمال</p></div>
<div class="m2"><p>رو به ناخن می‌کند آیینهٔ جوهر زبان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>راحت اهل سخن در بی‌سخن گردیدن‌ست</p></div>
<div class="m2"><p>غیر خاموشی ندارد بالش و بستر زبان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بحر برخود می‌تپد از خود فروشیهای موج</p></div>
<div class="m2"><p>عالمی بی‌طاقت است از مردمان تر زبان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رازکمظرفان نمی‌پوشد هجوم احتیاج</p></div>
<div class="m2"><p>می‌کشد در تشنگیها از صدا ساغر زبان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شور دل چون غنچه از رنگم‌گریبان می‌درد</p></div>
<div class="m2"><p>پاس خاموشی چسان دارم به یک دفتر زبان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هرکه دارد قوت روحانی ازکاهش تهی‌ست</p></div>
<div class="m2"><p>بیدل از ضعف بدن‌کم می‌شود لاغر زبان</p></div></div>