---
title: >-
    غزل شمارهٔ ۱۰۶۸
---
# غزل شمارهٔ ۱۰۶۸

<div class="b" id="bn1"><div class="m1"><p>مکتوب من به هرکه برد باد می‌برد</p></div>
<div class="m2"><p>تا یاد کس رسیدنم از یاد می‌برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرواز رنگ من اگر آید به امتحان</p></div>
<div class="m2"><p>مانی شکست خامه به بهزاد می‌برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دیر پا بر آتشم از کعبه سر به سنگ</p></div>
<div class="m2"><p>دیگر کجایم این دل ناشاد می‌برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از حرف و صوت جوهر تحقیق رفته گیر</p></div>
<div class="m2"><p>آیینه تا نفس زده‌ای باد می‌برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این پیکری‌که تیشهٔ تدبیر جانکنی است</p></div>
<div class="m2"><p>ما را همان به تربت فرهاد می‌برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا گردی از خرام تو باغ تصورست</p></div>
<div class="m2"><p>شوق از خودم به سایهٔ شمشاد می‌برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک موج اگر عنان گسلد سیل‌ گریه‌ام</p></div>
<div class="m2"><p>از خاک هند دجله به بغداد می‌برد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرچند دل ز شرم خیال‌ات عرق کند</p></div>
<div class="m2"><p>یک شیشه خانه عرض پریزاد می‌برد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در آتشم فکن که سپند فسرده‌ام</p></div>
<div class="m2"><p>تا سرمه نیست زحمت فریاد می‌برد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل بنال ورنه درین دامگاه یأس</p></div>
<div class="m2"><p>خاموشی‌ات ز خاطر صیاد می‌برد</p></div></div>