---
title: >-
    غزل شمارهٔ ۷۱۱
---
# غزل شمارهٔ ۷۱۱

<div class="b" id="bn1"><div class="m1"><p>ساز تو کمین نغمهٔ بیداد شکستی‌ست</p></div>
<div class="m2"><p>در شیشهٔ این رنگ پریزاد شکستی‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوهر ز حباب آن همه تفریق ندارد</p></div>
<div class="m2"><p>هرجاست سری درگره باد شکستی‌ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تصویر سحر ‌رنگ سلامت نفروشد</p></div>
<div class="m2"><p>صورتگر ما خامهٔ بهزاد شکستی‌ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیچ و خم عجزیم‌، چه ناز و چه تعین‌؟</p></div>
<div class="m2"><p>بالیدن امواج به امداد شکستی‌ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون رنگ چه‌“‌بالم به غباری‌که ندارم</p></div>
<div class="m2"><p>از خویش فراموشی من یاد شکستی‌ ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تنها دل عاشق تپش یأس ندارد</p></div>
<div class="m2"><p>هرشیشه تنک مشرب فریاد شکستی‌ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیدل نخوری عشوهٔ تعمیر سلامت</p></div>
<div class="m2"><p>ویرانی بنیاد تو آباد شکستی ست</p></div></div>