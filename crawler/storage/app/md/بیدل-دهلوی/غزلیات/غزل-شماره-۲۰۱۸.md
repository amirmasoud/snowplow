---
title: >-
    غزل شمارهٔ ۲۰۱۸
---
# غزل شمارهٔ ۲۰۱۸

<div class="b" id="bn1"><div class="m1"><p>شب از یاد خطت سر رشتهٔ جان بود در دستم</p></div>
<div class="m2"><p>ز موج گل رگ خواب گلستان بود در دستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به غارت رفته‌ام تا ازکفم رفته‌ست‌گیرایی</p></div>
<div class="m2"><p>چو بوی‌گل نمی‌دانم چه دامان بود در دستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فراهم تا نمودم تار و پود کسوت هستی</p></div>
<div class="m2"><p>به رنگ غنچه یک چاک‌گریبان بود در دستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کف پایی نیفشاندم به عرض دستگاه خود</p></div>
<div class="m2"><p>وگر نه یک جهان امید سامان بود در دستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نفس در دل‌گره‌کردم به ناموس وفا ور نه</p></div>
<div class="m2"><p>کلید نالهٔ چندین نیستان بود در دستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سواد عجز روشن‌کردم و درس دعا خواندم</p></div>
<div class="m2"><p>درین مکتب همین یک خط شبخوان بود در دستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز جنس‌ گوهر نایاب مطلب هر چه ‌گم‌کردم</p></div>
<div class="m2"><p>کف افسوس فرصت نقد تاوان بود در دستم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پر افشانی ز موج‌ گوهرم صورت نمی‌بندد</p></div>
<div class="m2"><p>سر این رشته تا بودم پریشان بود در دستم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سواد دشت امکان داشت بوی چین‌ گیسویی</p></div>
<div class="m2"><p>اگر نه دامن خود هم چه امکان بود در دستم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به سعی نارسایی قطع امید از جهان‌کردم</p></div>
<div class="m2"><p>تهی دستی همان شمشیر عریان بود در دستم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو صبح ازکسوت هستی نبردم صرفهٔ چاکی</p></div>
<div class="m2"><p>چه سازم جیب فرصت دامن افشان بود در دستم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شبم آمد به‌کف بیدل حضور دامن وصلی</p></div>
<div class="m2"><p>که ناخن هم ز شوقش چشم حیران بود در دستم</p></div></div>