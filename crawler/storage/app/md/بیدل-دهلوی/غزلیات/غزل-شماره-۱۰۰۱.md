---
title: >-
    غزل شمارهٔ ۱۰۰۱
---
# غزل شمارهٔ ۱۰۰۱

<div class="b" id="bn1"><div class="m1"><p>ادب چون ماه نو امشب پی تکلیف من دارد</p></div>
<div class="m2"><p>قدح کج کرده صهبایی که شرم از ریختن دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به وضع غنچه فرصت می‌دهد آواز گل‌ها را</p></div>
<div class="m2"><p>که لب زینهار مگشایید خاموشی چمن دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز ساز و برگ آسایش چه دارد منعم غافل</p></div>
<div class="m2"><p>همه‌ گر نام دارد در زمین آب کن دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنین کز دیده‌ها یوشیده‌اند احوال مجنونم</p></div>
<div class="m2"><p>که گر گردون شوم عریانی من پیرهن دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز انگشت شهادت این نوایم ‌گوش می‌مالد</p></div>
<div class="m2"><p>که سوی او اشارت هم ز خود برخاستن دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازین محفل به جایی رو که در یاد کسان نایی</p></div>
<div class="m2"><p>وگرنه در عدم هم رفتنت باز آمدن دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسوز و محو شو تا عشق‌ گردد فارغ از رنجت</p></div>
<div class="m2"><p>شرار سنگ بت پر انتظار برهمن دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به پیری تا کجا خواب سلامت آرزو کردن</p></div>
<div class="m2"><p>خمیدن سایه بر بنیاد دیوار کهن دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نمی‌دانم کجا دزدم سر از بیداد مژگانش</p></div>
<div class="m2"><p>که دل تا دیده یک تیر تغافل پر زدن دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شکوه ناز می‌بالد ز پهلوی نیاز اینجا</p></div>
<div class="m2"><p>کلاه او شکست آراست تا رنگم شکن دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بغل وامی‌کند گرد چمن ‌خیز خرام او</p></div>
<div class="m2"><p>که امشب انجمن مهتاب و بوی یاسمن دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل‌از ننگ‌آب‌شد بیدل‌که‌پیش‌لعل‌خاموشش</p></div>
<div class="m2"><p>تبسم می‌کند موج‌ گهر گویی دهن دارد</p></div></div>