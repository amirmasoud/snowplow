---
title: >-
    غزل شمارهٔ ۱۸۸۴
---
# غزل شمارهٔ ۱۸۸۴

<div class="b" id="bn1"><div class="m1"><p>بحث و جدل به افت جان می‌کند طرف</p></div>
<div class="m2"><p>سرها به تیغ فتنه زبان می‌کند طرف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طعن خسان مقابل صدق مقال توست</p></div>
<div class="m2"><p>اظهار راستی به سنان می‌کند طرف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از گفت و گو به خاک مزن ‌گوهر وقار</p></div>
<div class="m2"><p>این موج بحر را به‌ کران می‌کند طرف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا کی ز چارسوی تعلق خرد کسی</p></div>
<div class="m2"><p>جنسی ‌که آتشش به دکان می‌کند طرف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تشویش خوب و زشت ز آثار آگهی‌ست</p></div>
<div class="m2"><p>آیینه را صفا به جهان می‌کند طرف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بد نیست با معاملهٔ جاه ساختن</p></div>
<div class="m2"><p>اما دماغ را به خران می‌کند طرف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیدا اگر نباشی از آفات رسته‌ای</p></div>
<div class="m2"><p>با ناوک غرور نشان می‌کند طرف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا آتشی به دل نزند عشق چون سپند</p></div>
<div class="m2"><p>آداب را به ناله چسان می‌کند طرف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همدرس خلق باش‌، تغافل کمال نیست</p></div>
<div class="m2"><p>ای بی خبر کری به فغان می‌کند طرف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آسان مدان تردد روزی که چون هلال</p></div>
<div class="m2"><p>با نُه سپهر یک لب نان می‌کند طرف</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل غرور لاف دلیل سبکسری‌ست</p></div>
<div class="m2"><p>خودسنجی‌ات به سنگ‌کران می‌کند طرف</p></div></div>