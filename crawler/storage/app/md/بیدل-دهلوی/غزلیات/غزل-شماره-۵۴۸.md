---
title: >-
    غزل شمارهٔ ۵۴۸
---
# غزل شمارهٔ ۵۴۸

<div class="b" id="bn1"><div class="m1"><p>عجز بینش با تعلقهای امکان آشناست</p></div>
<div class="m2"><p>اشک ما تا چشم نگشودن به مژگان آشناست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امتحانگاه حوادث بزم افلاس است و بس</p></div>
<div class="m2"><p>سرد و گرم دهر با آغوش عریان آشناست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرد ما ننشست جز در دامن زلف بتان</p></div>
<div class="m2"><p>هر کجا بینی پریشان با پریشان آشناست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچکس ‌کام امید از اهل دنیا برنداشت</p></div>
<div class="m2"><p>طالع ما هم به وضع این عزیزان آشناست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غیر عبرت هیچ نتوان خواند از اوضاع دهر</p></div>
<div class="m2"><p>یارب این ‌طومار حیرت با چه ‌عنوان آشناست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در چنین بزمی که سازش پردهٔ بیگانگی ست</p></div>
<div class="m2"><p>مفت الفتها اگر مژگان به مژگان آشناست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اشکم از مژگان چکید و رنگ اظهاری نبست</p></div>
<div class="m2"><p>این‌ گهر در خاک هم با قعر عمان آشناست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سوختن، خاشاک را هم‌رنگ آتش می‌کند</p></div>
<div class="m2"><p>هرقدر بیگانه‌ایم از خویش جانان آشناست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر کجا بی‌خانمانی هست صید زلف اوست</p></div>
<div class="m2"><p>این‌کمند ناز با شام غریبان آشناست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرد خط در دور حسنش ابر عالمگیرشد</p></div>
<div class="m2"><p>طالع موری ‌که با دست سلیمان آشناست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در رهش پای طلب بیگانهٔ دامان صبر</p></div>
<div class="m2"><p>در غمش دست ندامت با گریبان آشناست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بی‌ندامت نیست اسباب نشاط این چمن</p></div>
<div class="m2"><p>گل هم ازشبنم‌ کف دستی به دندان آشناست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شمع ‌گو در دیده‌ام دکان رعنایی مچین</p></div>
<div class="m2"><p>کاین دل پر داغ با چندین چراغان آشناست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیدل از چشم تحیر مشربم غافل مباش</p></div>
<div class="m2"><p>هرکجا حسنی است با آیینه‌داران آشناست</p></div></div>