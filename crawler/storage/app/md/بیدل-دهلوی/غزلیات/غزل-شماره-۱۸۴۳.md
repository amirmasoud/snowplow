---
title: >-
    غزل شمارهٔ ۱۸۴۳
---
# غزل شمارهٔ ۱۸۴۳

<div class="b" id="bn1"><div class="m1"><p>گرفته اشک مرا دیده تا به دامان رقص</p></div>
<div class="m2"><p>چنین‌ که داد ندانم به یاد مستان رقص</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرار خرمن جمعیت‌ است خود سریت</p></div>
<div class="m2"><p>غبار را چو نفس می‌کند پریشان رقص</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر ز بزم جنون ساغرت به چنگ افتد</p></div>
<div class="m2"><p>چو گرد باد توان کرد در بیابان رقص</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طرب‌ کجاست درین محفل ای خیال ‌پرست</p></div>
<div class="m2"><p>که نغمه غلغلهٔ محشر است و توفان رقص</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درین ستمکده‌ گویی دگر نمی‌باشد</p></div>
<div class="m2"><p>سر بریدهٔ ما می‌کند به میدان رقص</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز اضطراب دل‌، اهل زمانه بی خبرند</p></div>
<div class="m2"><p>بود تپیدن بسمل به پیش طفلان رقص</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فضولی آینهٔ دستگاه‌ کم ظرفیست</p></div>
<div class="m2"><p>به ‌روی بحر کند قطره وقت باران رقص</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز خود تهی شو و شور جنون تماشا کن</p></div>
<div class="m2"><p>به‌ کام دل نکند ناله بی‌ نیستان رقص</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گشاد بال درین تنگنا خجالت داشت</p></div>
<div class="m2"><p>شرار ما به دل سنگ‌ کرد پنهان رقص</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نفس به ذوق رهایی است پر فشان خیال</p></div>
<div class="m2"><p>و گر نه ‌کس نکند در شکنج زندان رقص</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مگر به باد فروشد غبار ما ورنه</p></div>
<div class="m2"><p>ز خاک راست نیاید به‌ هیچ ‌عنوان رقص</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مکن تغافل اگر فرصت نگاهی هست</p></div>
<div class="m2"><p>شرار کاغذ ما کرده است سامان رقص</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به اعتماد نفس اینقدر چه می‌نازی</p></div>
<div class="m2"><p>به اشک صرفه ندارد به ‌دوش مژگان رقص</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به این ترانه صدای سپند می‌بالد</p></div>
<div class="m2"><p>که تا ز خود نتوان رست نیست امکان رقص</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تپش ز موج‌ گهر گل نمی‌کند بیدل</p></div>
<div class="m2"><p>نکرد اشک من ‌آخر به‌ چشم حیران رقص</p></div></div>