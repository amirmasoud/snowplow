---
title: >-
    غزل شمارهٔ ۲۳۸۲
---
# غزل شمارهٔ ۲۳۸۲

<div class="b" id="bn1"><div class="m1"><p>شکوهٔ اسباب چند، دل به رمیدن دهیم</p></div>
<div class="m2"><p>دامن اگر شد بلند گریه به چیدن دهیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درد سر ما و من سخت مکرر شده‌ست</p></div>
<div class="m2"><p>حرف فراموشیی یاد شنیدن دهیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عبرت این انجمن خورد سراپای ما</p></div>
<div class="m2"><p>شمع صفت تا کجا لب به ‌گزیدن دهیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غفلت سرشار خلق نیست‌کفیل شعور</p></div>
<div class="m2"><p>چشمی اگر واشود مژدهٔ دیدن دهیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عبرت پیری شکست شیشهٔ‌گردن‌کشی</p></div>
<div class="m2"><p>حوصله را بعد ازین جام خمیدن دهیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچکس از باغ دهر صرفه‌بر جهد نیست</p></div>
<div class="m2"><p>بی‌ثمری را مگر حکم رسیدن دهیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ربشه ما می‌دود هرزه به باغ خیال</p></div>
<div class="m2"><p>آبله‌کو تا دمی‌ گل به دمیدن دهیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مزرع بیحاصلان وقف حیا پروریست</p></div>
<div class="m2"><p>دانه‌ کجا تا به حرص رخصت چیدن دهیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مایه همین عبرتست درگره اشک وآه</p></div>
<div class="m2"><p>آنچه ز ما وا کند مزد کشیدن دهیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسمل این مشهدیم فرصت دیگر کجاست</p></div>
<div class="m2"><p>یک دو نفس مهلت است داد تپیدن دهیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زحمت مژگان‌کشد اشک جهان تاز چند</p></div>
<div class="m2"><p>کاش به پایی رسد سر به دویدن دهیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شور طلب همچو شمع قطع نگردد ز ما</p></div>
<div class="m2"><p>پاکند ایجاد اگر سر به بریدن دهیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سیر خودش باعثی است ‌کاش به دل رو کند</p></div>
<div class="m2"><p>حسن تغافل اداست آینه دیدن دهیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر همه تن لب شوبم جرأت‌ گفتار کو</p></div>
<div class="m2"><p>قاصد ما بیدل است خط به دریدن دهیم</p></div></div>