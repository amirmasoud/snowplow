---
title: >-
    غزل شمارهٔ ۱۲۲۹
---
# غزل شمارهٔ ۱۲۲۹

<div class="b" id="bn1"><div class="m1"><p>شب که از شور شکست دل اثر پرزور شد</p></div>
<div class="m2"><p>همچو چینی تار مویی ‌کاسهٔ طنبور شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برق آفت‌گر چنین دارد کمین اعتبار</p></div>
<div class="m2"><p>خرمن ما عاقبت خواهد نگاه مور شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عیش صد دانا ز یک نادان منغص می‌شود</p></div>
<div class="m2"><p>ربط مصرع بر هم است آنجا که حرفی کور شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفس را ترک هوا روح مقدس می‌کند</p></div>
<div class="m2"><p>شعله‌ای‌ کز دود فارغ ‌گشت عین نور شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نمکدانت چنین در دیده‌ها دارد اثر</p></div>
<div class="m2"><p>آب در آیینه همچون اشک خواهد شور شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل شکست اماکسی بر نالهٔ ما پی نبرد</p></div>
<div class="m2"><p>موی چینی جوهر آیینهٔ فغفور شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کاش چون نقش قدم با عاجزی می‌ساختم</p></div>
<div class="m2"><p>بسکه سعی ما رسایی‌کرد منزل دورشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ساغر عشق مجازم نشئهٔ تحقیق داد</p></div>
<div class="m2"><p>مشت خونم جون مجنون می‌زد ومنصورشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون سحر کم نیست‌ گر عرض غباری داده‌ایم</p></div>
<div class="m2"><p>بیش ازین نتوان به سامان نفس مغرور شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عمرها شد بیدل احرام خموشی بسته‌ام</p></div>
<div class="m2"><p>آخراین ضبط نفس خواهد خروش صور شد</p></div></div>