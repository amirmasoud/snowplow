---
title: >-
    غزل شمارهٔ ۹۹۰
---
# غزل شمارهٔ ۹۹۰

<div class="b" id="bn1"><div class="m1"><p>هوس‌پیمای فرصت گرد کلفت در قفس دارد</p></div>
<div class="m2"><p>همین خاک است و بس گر شیشهٔ ساعت نفس دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لب از خمیازهٔ صبح قیامت تا نمی‌بندی</p></div>
<div class="m2"><p>خم آسودگی جوش شراب خام‌رس دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در سعی جنون زن‌، از وبال هوش بیرون آی</p></div>
<div class="m2"><p>به زحمت تا نگیرد کوچهٔ دانش عسس دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه‌تنها شامل هستی‌ست عشق بی‌نشان جوهر</p></div>
<div class="m2"><p>عدم هم زآن معیت دستگاه پیش و پس دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جنون الرحیلی شش جهت پیچیده عالم را</p></div>
<div class="m2"><p>مپرس از کاروان منزل هم آهنگ جرس دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برون آر از طبیعت خار خار وهم آسودن</p></div>
<div class="m2"><p>که چشم بی‌نیازان از رگ این خواب خس دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نفس هر پر زدن خون دگر در پرده می‌ریزد</p></div>
<div class="m2"><p>طبیب زندگی شغلی همین نیش مجس دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خراش دامن عزت مخواه از ترک خوشخویی</p></div>
<div class="m2"><p>که راه کوی بدکیشی سگان بی‌مرس دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>محبت عمرها شد رفته می‌جوشد ز خاطرها</p></div>
<div class="m2"><p>ندارد جز فراموشی کسی گر یاد کس دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ندامت نیست غافل از کمین هیچکس بیدل</p></div>
<div class="m2"><p>به هر دستی که عبرت وارسد دست مگس دارد</p></div></div>