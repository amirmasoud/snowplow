---
title: >-
    غزل شمارهٔ ۲۳۳۰
---
# غزل شمارهٔ ۲۳۳۰

<div class="b" id="bn1"><div class="m1"><p>نشنیده حرف چند که ما گوش‌ کرده‌ایم</p></div>
<div class="m2"><p>تا لب گشوده‌ایم فراموش کرده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درد دلیم، شور دو عالم غبار ماست</p></div>
<div class="m2"><p>اما زیارت لب خاموش کرده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تسلیم ما قلمرو جولان ناز کیست</p></div>
<div class="m2"><p>سیر نُه آسمان به خم دوش کرده‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفات دهر چاره‌گرش یک تغافلست</p></div>
<div class="m2"><p>توفان به بستن مژه خس پوش کرده‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شوری دگر نداشت خمستان اعتبار</p></div>
<div class="m2"><p>خود را چو درد می سبب جوش‌کرده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حیرت سحر دماندهٔ طرز نگاه ماست</p></div>
<div class="m2"><p>صد چاک سینه نذر یک آغوش‌کرده‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طاووس رنگ ما ز نگاه ‌که می‌کش است</p></div>
<div class="m2"><p>پرواز را به‌ جلوه قدح نوش کرده‌ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر وضع ما خطای جنونی دگر مبند</p></div>
<div class="m2"><p>کم نیست این که پیروی هوش کرده‌ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مردم به دستگاه بقا ناز می‌کنند</p></div>
<div class="m2"><p>ما تکیه بر فنای خطا پوش کرده‌ایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل حدیث بیخبران ناشنیدنی است</p></div>
<div class="m2"><p>بودیم معنیی که فراموش کرده‌ایم</p></div></div>