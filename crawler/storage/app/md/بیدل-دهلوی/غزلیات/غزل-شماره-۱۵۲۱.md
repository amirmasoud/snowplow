---
title: >-
    غزل شمارهٔ ۱۵۲۱
---
# غزل شمارهٔ ۱۵۲۱

<div class="b" id="bn1"><div class="m1"><p>هر که آمد در جان بیکس‌تر از ما می‌رود</p></div>
<div class="m2"><p>کاروانها زین ره باریک تنها می‌رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از شکست اعتبار آگاه باید زیستن</p></div>
<div class="m2"><p>نیست بی‌گرد پری راهی ‌که مینا می‌رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر خط مضمون زلفش‌ کج رقم افتاده است</p></div>
<div class="m2"><p>شانه‌ گر صد خامه پردازد چلیپا می‌رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر سر رفتن بود سوی ‌گریبان رو کنید</p></div>
<div class="m2"><p>شمع زپن محفل برون بی‌زحمت پا می‌رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی‌وداع جاه نتوان از دنائت وارهید</p></div>
<div class="m2"><p>سایه با آثار این دیوار یک‌جا می‌رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طمطراق عالم عبرت تماشاکردنی‌ست</p></div>
<div class="m2"><p>پیش پیشش بانگ خرگرم است مرزا می‌رود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زاهدان بر خود مچینید اینقدر سودای پوچ</p></div>
<div class="m2"><p>ریش و فش آخر چو پشم از کون دنیا می‌رود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>انتظار صبح محشر عالمی را خاک‌ کرد</p></div>
<div class="m2"><p>عمرها رفت‌و همین امروز و فردا می‌رود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کاش موهومی به فریاد غبار ما رسد</p></div>
<div class="m2"><p>رنگها باید پری افشاند عنقا می‌رود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در کمین صنعت علم و فنون دیوانگی‌ست</p></div>
<div class="m2"><p>بام و در، بی‌جستجو آخر به صحرا می‌رود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ششجهت واماندهٔ یاس سراغ مدعاست</p></div>
<div class="m2"><p>نام فرصت نیست‌کم‌گر بر زبانها می‌رود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حیف دانایی که گردد غافل از آزادگی</p></div>
<div class="m2"><p>در تلاش‌گوهر، آب روی دریا می‌رود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دوستان‌گر مدعا عرض پیام آرزوست</p></div>
<div class="m2"><p>قاصد دیگر چه لازم فرصت ما می‌رود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پی غلط‌ کرده است بیدل آمد و رفت نفس</p></div>
<div class="m2"><p>خلق می‌آید به آیینی‌ که‌ گویا می‌رود</p></div></div>