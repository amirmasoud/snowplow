---
title: >-
    غزل شمارهٔ ۲۴۰۳
---
# غزل شمارهٔ ۲۴۰۳

<div class="b" id="bn1"><div class="m1"><p>گر چه جز ذکرت نمی‌گنجد حدیثی در زبان</p></div>
<div class="m2"><p>چون نگینم جای نام توست خالی‌بر زبان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درد عشق و ساز مستوری زهی فکر محال</p></div>
<div class="m2"><p>خار پا چون آتش اینجا می‌کشد از سر زبان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مزرع اهل سخن شایستهٔ آفات نیست</p></div>
<div class="m2"><p>رشحهٔ معنی نبندد ننگ خشکی بر زبان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نغمهٔ من اضطراب ایجاد ساز عالمی‌ست</p></div>
<div class="m2"><p>عمر‌ها شد چون سخن‌پر می‌زنم در پر زبان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگذر از لاف سخن پروازها پیداست چیست</p></div>
<div class="m2"><p>در قفس تاکی تپد ای بیخبر یک هر زبان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا فنا صورت نبندد زندگی بی‌لاف نیست</p></div>
<div class="m2"><p>شعله دزدیدن ندارد جز به خاکستر زبان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غیر خون آبی ندارد ساغر جانکاه ظلم</p></div>
<div class="m2"><p>گر همه ازکام بیرون افکند خنجر زبان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا به رنگ خانهٔ چشم ایمن از آفت شوی</p></div>
<div class="m2"><p>به‌که باشد همچو مژگانت برون در زبان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لب گشودن داشت آغوش وداع عافیت</p></div>
<div class="m2"><p>چون دهان پسته بستم راه جنبش بر زبان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عجرما بیدل‌به تقریری دگرمحتاج نیست</p></div>
<div class="m2"><p>موج در عرض شکست خود بود یکسر زبان</p></div></div>