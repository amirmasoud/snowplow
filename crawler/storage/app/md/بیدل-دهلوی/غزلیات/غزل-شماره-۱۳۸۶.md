---
title: >-
    غزل شمارهٔ ۱۳۸۶
---
# غزل شمارهٔ ۱۳۸۶

<div class="b" id="bn1"><div class="m1"><p>مصور نگهت ساغر چه رنگ زند</p></div>
<div class="m2"><p>مگر جنون کند و خامه در فرنگ زند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین‌که نرگست از ناز سرگران شده است</p></div>
<div class="m2"><p>ز سایهٔ مژه ترسم به سرمه سنگ زند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به گلشنی که چمن در رکاب بخرامی</p></div>
<div class="m2"><p>حنا ز دست تو گیرد گل و به رنگ زند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز سعی خاک به گردون غبار نتوان برد</p></div>
<div class="m2"><p>به دامن تو همان دامن تو چنگ زند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل گرفتهٔ ما قابل تصرف نیست</p></div>
<div class="m2"><p>کسی چه قفل بر این خانه‌های تنگ زند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گشودن مژه مفت نفس‌شماری ماست</p></div>
<div class="m2"><p>شرر دگر چه‌قدر تکیه بر درنگ زند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان ادبگه‌ دل‌هاست بی‌نفس می‌باش</p></div>
<div class="m2"><p>مباد آینه‌ای زین میانه زنگ زند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل شکسته جنون بهانه‌جو دارد</p></div>
<div class="m2"><p>که رنگ اگر شکنم شیشه بر تُرنگ زند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نموده‌اند ز دست نوازش فلکم</p></div>
<div class="m2"><p>دمی که گاه غضب بر زمین پلنگ زند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز خویش غیر تراشیده‌ای‌، کجاست جنون</p></div>
<div class="m2"><p>که خنده‌ای به شعور جهان بنگ زند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به ساز عجز برآ عذرخواه آفت باش</p></div>
<div class="m2"><p>هجوم آبله کمتر به پای لنگ زند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز بیدلی قدح انفعال سودایم</p></div>
<div class="m2"><p>به شیشه‌ای‌که ندارم‌کسی چه سنگ زند</p></div></div>