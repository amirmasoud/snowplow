---
title: >-
    غزل شمارهٔ ۲۷۲۴
---
# غزل شمارهٔ ۲۷۲۴

<div class="b" id="bn1"><div class="m1"><p>ز چه ناز بال دعوی به فلک گشاده باشی</p></div>
<div class="m2"><p>تو غبار ناتوانی ته پا فتاده باشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می عیش بیخمارت نفسی اگر درین بزم</p></div>
<div class="m2"><p>سر از خیال خالی دل بی‌اراده باشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدمی اگر شماری پی عزم پرفشانی</p></div>
<div class="m2"><p>به هزار چین دامن ز سحر زیاده باشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز تلاش برق تازان گروت گذشته باشد</p></div>
<div class="m2"><p>تو اگر سوار همت دو قدم پیاده باشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زنمو به رنگ شبنم طرب بهار این بس</p></div>
<div class="m2"><p>که ز چشم‌ تر کشی سر به‌ در اوفتاده باشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نسزد به مکتب وهم غم سرنوشت خوردن</p></div>
<div class="m2"><p>خط این جریده پوچ‌ است خوشت آنکه ساده‌ باشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه را ز باغ اعمال نظر او نبست نازش</p></div>
<div class="m2"><p>تو نم جبین نداری چه‌ گل آب داده باشی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شرر پریده رنگت اگر این بهار دارد</p></div>
<div class="m2"><p>ز مشیمهٔ تعین به چه ننگ زاده باشی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گل سرخوش و مستی طلبی است مابقی هیچ</p></div>
<div class="m2"><p>اگر این خمار بشکست‌ نه قدح نه باده باشی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو جوانی و چه پیری به‌ کشاکش است‌ کارت</p></div>
<div class="m2"><p>چو کمان دمی‌ که زورت شکند کباده باشی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نروی به محفل ای شمع‌ که زتنگی دل آنجا</p></div>
<div class="m2"><p>به نشستن تو جا نیست مگر ایستاده باشی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سخنت به طبع مستان اثری نکرد بیدل</p></div>
<div class="m2"><p>سر شیشه‌های خالی چقدر گشاده باشی</p></div></div>