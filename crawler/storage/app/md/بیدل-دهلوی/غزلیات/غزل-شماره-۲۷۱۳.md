---
title: >-
    غزل شمارهٔ ۲۷۱۳
---
# غزل شمارهٔ ۲۷۱۳

<div class="b" id="bn1"><div class="m1"><p>من و دیوانه‌خو طفلی ‌که هر جا سر کند بازی</p></div>
<div class="m2"><p>دو عالم رنگ بر هم چیند و ابتر کند بازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیال چین ابروی تو هر جا بی‌نقاب افتد</p></div>
<div class="m2"><p>نظر ها در دم شمشیر با جوهر کند بازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به توفان خیالت اشک حسرت بسملی دارم</p></div>
<div class="m2"><p>که هر مژگان زدن در عالم دیگر کند بازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به رویت پیچ و تاب طرهٔ مشکین به آن ماند</p></div>
<div class="m2"><p>که شاخ سنبلی بر لالهٔ احمر کند بازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آن محفل‌ که ‌گلچین هوس باشد دم تیغت</p></div>
<div class="m2"><p>مرا چون شمع یک‌ گردن به چندین سر کند بازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود ننگ شکوه مهر محو ذره ‌گردیدن</p></div>
<div class="m2"><p>بگو تا جلوه در آیینه‌ها کمتر کند بازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل عاشق به گلگشت چمن حیف‌ست پردازد</p></div>
<div class="m2"><p>سپند آن به‌ که در جولانگه مجمر کند بازی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طلب سرمایهٔ عشقی به درس لهو کمتر رو</p></div>
<div class="m2"><p>مبادا طفل خواهش را هوس پرور کند بازی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر آیینهٔ عبرت دلیل پیش پا باشد</p></div>
<div class="m2"><p>چرا طاووس ما با نقش بال و پرکند بازی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مزاج خوابناک افسانه را باطل نمی‌داند</p></div>
<div class="m2"><p>جهان بازی‌ست اماکیست تا باورکند بازی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طرب‌کن‌گر نشاط وهم هستی زود طی‌گردد</p></div>
<div class="m2"><p>به کلفت می‌کشد دل هر قدر لنگر کند بازی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هوس در طبع تمکین مشربان شوخی نمی‌داند</p></div>
<div class="m2"><p>چه امکان است بیدل موج در گوهر کند بازی</p></div></div>