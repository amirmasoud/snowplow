---
title: >-
    غزل شمارهٔ ۱۱۹۴
---
# غزل شمارهٔ ۱۱۹۴

<div class="b" id="bn1"><div class="m1"><p>اگر تعین عنقا هوس پیام نباشد</p></div>
<div class="m2"><p>نشان خود به جهانی برم که نام نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه لازم ست به دوشم غم آدا فکند کس</p></div>
<div class="m2"><p>حق بقا دونفس خجلت است و وام نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حیا ز ننگ خموشی ‌کدام نغمه‌ کند سر</p></div>
<div class="m2"><p>به صد فسانه زنم ‌گر سخن تمام نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو دم به‌ وضع تجدد خیال می‌گذرانم</p></div>
<div class="m2"><p>خوشم به نشئه‌ که جمعیت دوام نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حجاب‌جوهر دل نیست‌جزکدورت‌هستی</p></div>
<div class="m2"><p>چراغ آینه روشن به وقت شام نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل ‌است ‌باعت ‌هستی‌،‌ کجاست ‌نشئه‌ چه مستی</p></div>
<div class="m2"><p>دماغِ باده که دارد دمی‌که جام نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هوس تپد به چه راحت‌، نفس دمد ز چه وحشت</p></div>
<div class="m2"><p>در آن مقام‌که صیاد و صید و دام نباشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی ندید ز هستی به غیر دردسر اینجا</p></div>
<div class="m2"><p>شراب این خم وهم ازکجاکه خام نباشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه‌ ممکن است ‌که آغوش حرصها بهم آید</p></div>
<div class="m2"><p>درتن جسراحث خمیازه التیام نباشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل از شکایت افلاس به ‌که جمع نمایی</p></div>
<div class="m2"><p>زبان به ‌کام تو بس ‌گر جهان به ‌کام نباشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جدا ز انجمن نیستی به هرجه رسیدم</p></div>
<div class="m2"><p>نیافتم‌که می ساغرش حرام نباشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کدام عمر و چه فرصت‌که دل دهی به تماشا</p></div>
<div class="m2"><p>به پای اشک نگه می‌دود خرام نباشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه‌گوشه‌ای‌ست معین نه منزلی‌ست مبرهن</p></div>
<div class="m2"><p>کسی ‌کجا رود از عالمی ‌که نام نباشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به اوج عشق چه نسبت تلاش بال هوس را</p></div>
<div class="m2"><p>وداع وهم من و ما هوای بام نباشد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خروش درد شنو مدعای عشق همین بس</p></div>
<div class="m2"><p>در الله الله ما جای حرف لام نباشد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر ز ملک عدم تا وجود فهم ‌گماری</p></div>
<div class="m2"><p>بجزکلام تو بیدل دگرکلام نباشد</p></div></div>