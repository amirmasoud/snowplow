---
title: >-
    غزل شمارهٔ ۹۸
---
# غزل شمارهٔ ۹۸

<div class="b" id="bn1"><div class="m1"><p>گرکنم با این سر پرشور بالین سنگ را</p></div>
<div class="m2"><p>از شررپرواز خواهدگشت تمکین سنگ را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من به درد نارساییها چه‌سان دزدم نفس</p></div>
<div class="m2"><p>می‌کند بی‌دست و پایی ناله تلقین سنگ را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از جسد رنگ گداز دل توان دید آشکار</p></div>
<div class="m2"><p>گرشود دامن به خون لعل رنگین سنگ را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون‌صداهرکس به‌رنگی‌می‌رود زین‌کوهسار</p></div>
<div class="m2"><p>آتشم فهمید آخر خانهٔ زین سنگ را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از شکست ما صدای شکوه نتوان یافتن</p></div>
<div class="m2"><p>شیشه اینجامی‌گشاید لب به‌تحسین سنگ را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیدهٔ بیدار را خواب‌گران زیبنده نیست</p></div>
<div class="m2"><p>ای شررتا چند خواهی‌کرد بالین سنگ را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساز این کهسار غیر از ناله آهنگی نداشت</p></div>
<div class="m2"><p>آرمیدن اینقدرهاکرد سنگین سنگ را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صافی دل مفت عیش است از حسد پرهیزکن</p></div>
<div class="m2"><p>هوش اگر جامت دهد برشیشه مگزین سنگ‌را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فیض سودا مشربان از بس‌که عام فتاده است</p></div>
<div class="m2"><p>خون مجنون می‌کند دامان‌گلچین سنگ را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ظالم از ساز حسد بی‌دستگاه عیش نیست</p></div>
<div class="m2"><p>از شرر دایم چراغان در دل است این سنگ را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا نفس دارد تردد جسم را سرگشتگی‌ست</p></div>
<div class="m2"><p>تا نیاساید فلاخن نیست تسکین سنگ را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گرهمه برخاک پیچید عشق حسن آرد برون</p></div>
<div class="m2"><p>کوشش فرهاد آخرکرد شیرین سنگ را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عافیتها نیست غیر از پردهٔ ساز شکست</p></div>
<div class="m2"><p>شیشه می‌بیند نگاه عاقبت بین سنگ را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خواب‌غفلت می‌شود پادر رکاب از موج اشک</p></div>
<div class="m2"><p>در میان آب بیدل نیست تمکین سنگ را</p></div></div>