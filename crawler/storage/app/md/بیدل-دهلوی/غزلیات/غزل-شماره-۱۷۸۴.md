---
title: >-
    غزل شمارهٔ ۱۷۸۴
---
# غزل شمارهٔ ۱۷۸۴

<div class="b" id="bn1"><div class="m1"><p>چنین تا کی تپد در انتظار زخم نخجیرش</p></div>
<div class="m2"><p>درآغوش‌ کمان بر دل قیامت می‌کند تیرش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر آن جلوه دریابد زبان حیرت ما را</p></div>
<div class="m2"><p>که چون آیینه بی‌حرف است صافیهای تقریرش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر این است برق خانه سوز شعلهٔ حسنت</p></div>
<div class="m2"><p>جهانی می‌توان آتش زدن از رنگ تصویرش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مصور جلوه نتواند دهد نقش میانت را</p></div>
<div class="m2"><p>گر از تار نظر سازند موی‌ کلک تحریرش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سیه‌روزی که یاد طره‌ات آوازه‌اش دارد</p></div>
<div class="m2"><p>به صد خورشید نتوان شد حریف منع شبگیرش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به این نیرنگ اگر حسن بتان آیینه پردازد</p></div>
<div class="m2"><p>برهمن دارد ایمانی‌ که شرم آید ز تکفیرش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به سعی جان‌کنیها کوهکن آوازه‌ای دارد</p></div>
<div class="m2"><p>به غوغا می‌فروشد هرکه باشد آب در شیرش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در این دشت جنون الفت‌گرفتاری نمی‌باشد</p></div>
<div class="m2"><p>که آزادی پر افشان نیست از آواز زنجیرش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نفس می‌بست بر عمر ابد ساز حباب من</p></div>
<div class="m2"><p>به‌ یک بست وگشاد چشم آخر شد بم و زیرش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل جمع آرزو داری بساط‌ گفتگو طی‌ کن</p></div>
<div class="m2"><p>که‌گوهر بر شکست موج موقوف است‌ تعمیرش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به صحرایی که صیادش‌کمند زلف او باشد</p></div>
<div class="m2"><p>اگر معنی شود جستن ندارد گرد نخجیرش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به صد طاقت نکردم راست بیدل قامت آهی</p></div>
<div class="m2"><p>جوانی‌ها اگر این است رحمت باد بر پیرش</p></div></div>