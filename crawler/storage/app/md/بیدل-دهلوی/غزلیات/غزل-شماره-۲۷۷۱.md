---
title: >-
    غزل شمارهٔ ۲۷۷۱
---
# غزل شمارهٔ ۲۷۷۱

<div class="b" id="bn1"><div class="m1"><p>ز عریانی جنون ما نشد مغرور سامانی</p></div>
<div class="m2"><p>توان دست از دو عالم برد اگر باشد گریبانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر از خود روم تا اشکی وآهی به موج آید</p></div>
<div class="m2"><p>که چون شبنم نی‌ام سر تا قدم جز چشم حیرانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه سان زبر فلک عرض بلندیها دهد همت</p></div>
<div class="m2"><p>که از کوتاهی این خیمه نتوان چید دامانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندانم از کدامین‌ کوچه خیزد گرد من یا رب</p></div>
<div class="m2"><p>نوای شوقم و گم کرده‌ام ره در نیستانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تبسم جلوه‌ای چون صبح بگذشت ازکنار من</p></div>
<div class="m2"><p>سراپایم نهان گردید در گرد نمکدانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز سوز دل تجلی منظر برقی‌ست هر عضوم</p></div>
<div class="m2"><p>چو مجمر دارم از یک شعله سامان چراغانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز قرب سایهٔ من می‌گدازد زهرهٔ راحت</p></div>
<div class="m2"><p>تبی در استخوان دارم چو شیری در نیستانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنین‌ کز هر بن مو انتظار چشم یعقوبم</p></div>
<div class="m2"><p>پس از مردن تواند ریخت خاکم رنگ‌ کنعانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به زلف او شکست آمادهٔ حسرت دلی دارم</p></div>
<div class="m2"><p>که عمری شد شکن می‌پرورد در سنبلستانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به اسباب تعلق جمع نتوان یافت آسودن</p></div>
<div class="m2"><p>دو عالم محو گردد تا رسد مژگان به مژگانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هیولی ماند دهر و نقشی از پیکر نبست آخر</p></div>
<div class="m2"><p>ز لفظ این معما برنیامد نام انسانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر بیدل چوگل پایم ز دامن برنمی‌آید</p></div>
<div class="m2"><p>ندارد کوتهی دست من از سیر گریبانی</p></div></div>