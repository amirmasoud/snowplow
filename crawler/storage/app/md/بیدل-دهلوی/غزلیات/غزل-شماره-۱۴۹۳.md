---
title: >-
    غزل شمارهٔ ۱۴۹۳
---
# غزل شمارهٔ ۱۴۹۳

<div class="b" id="bn1"><div class="m1"><p>امشب غبار نالهٔ دل سرمه رنگ بود</p></div>
<div class="m2"><p>یا رب شکست‌شیشهٔ‌ من از چه سنگ بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از کشتنم نشد شفقی طرف دامنی</p></div>
<div class="m2"><p>خونم درپن ستمکده نومید رنگ بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا صاف‌ گشت آینه خود را ندیدم ام</p></div>
<div class="m2"><p>چون سایه نقش هستی من جمله زنگ بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عالم به خون تپیدهٔ نومیدی من است</p></div>
<div class="m2"><p>جستن ز صیدگاه مرادم خدنگ بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حسن از غبار شوخ‌نگاهان رمیده است</p></div>
<div class="m2"><p>اینجا هجوم آینه پشت پلنگ بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همت نمی‌رود به سر ترک اختیار</p></div>
<div class="m2"><p>ازخویش رفتنم به رهت عذر لنگ بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عنقای دیگرم که ز بنیاد هستی‌ام</p></div>
<div class="m2"><p>تا نام‌، شوخی اثری داشت‌، ننگ بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در دل برون دل دو جهان جلوه رنگ ریخت</p></div>
<div class="m2"><p>این جامه بر قد تو چه مقدارتنگ بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از بس که بی‌دماغ تماشای فرصتیم</p></div>
<div class="m2"><p>ما را به خود نیامده رفتن درنگ بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل‌،‌که داشت جلوه‌ که از برق خجلتش</p></div>
<div class="m2"><p>در مجلس بهار چراغان رنگ بود</p></div></div>