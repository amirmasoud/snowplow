---
title: >-
    غزل شمارهٔ ۱۶۶۵
---
# غزل شمارهٔ ۱۶۶۵

<div class="b" id="bn1"><div class="m1"><p>چشم تعظیم ازگران‌جانان این محفل مدار</p></div>
<div class="m2"><p>کوفتن‌ گردد عصا کز سنگ برخیزد شرار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیر این‌گلشن مآلش انفعال خرمی‌ست</p></div>
<div class="m2"><p>عاقبت سر در شکست رنگ می‌دزدد بهار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرچه می‌بالد علم بر دوش‌گرد عاجزی‌ست</p></div>
<div class="m2"><p>نیستان شد عرصه از انگشتهای زینهار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بنای چینی دل‌کیست بردارد شکست</p></div>
<div class="m2"><p>ای فلک گر مردی این مو از خمیر ما برآر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشئهٔ دور و تسلسل تاکه راگردد نصیب</p></div>
<div class="m2"><p>جای ساغر ششجهت خمیازه می‌چیند خمار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل ز ضبط یک نفس جمعیت کلّیش نیست</p></div>
<div class="m2"><p>بحر ز افسون گهر تا کی ز خود گیرد کنار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عالم امکان تماشاخانهٔ آیینه است</p></div>
<div class="m2"><p>هرچه می‌بینم به رنگ رفتهٔ خویشم دچار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با دل افتاده‌ست کار زندگی آگاه باش</p></div>
<div class="m2"><p>آب را ناچار باید گشت درگوهر غبار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرزبان یأس امشب نام فرهاد که بود</p></div>
<div class="m2"><p>کز گرانی شد صدا نقش نگین‌ کوهسار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بوی ‌پیراهن به حسرت کرد خلقی را مثل</p></div>
<div class="m2"><p>می‌کشد یک دیدهٔ یعقوب چندین انتظار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از نفس سعی جنون ناقصم فهمیدنی‌ست</p></div>
<div class="m2"><p>صد گریبان می‌درم اما همین یک رشته‌وار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می‌کشم تا قامت پیری‌ست بار هرچه هست</p></div>
<div class="m2"><p>گو فلک دوش خم خود نیز بر دوشم گذار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بوریای فقرم آخر شهرهٔ آفاق شد</p></div>
<div class="m2"><p>هر سر موی من اینجا چون نفس شد نی‌سوار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زحمت فکر درودن تا کی ای کشت امل</p></div>
<div class="m2"><p>پرکهن شد ریشه اکنون ‌گردن دیگر برآر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بیدل از علم و عمل‌گر مدعا جمعیت است</p></div>
<div class="m2"><p>هیچ کاری غیر بیکاری نمی‌آید به‌کار</p></div></div>