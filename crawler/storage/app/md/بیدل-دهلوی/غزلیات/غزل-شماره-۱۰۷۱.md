---
title: >-
    غزل شمارهٔ ۱۰۷۱
---
# غزل شمارهٔ ۱۰۷۱

<div class="b" id="bn1"><div class="m1"><p>یک سر مو گر هوس از فکر جاهی بگذرد</p></div>
<div class="m2"><p>پشم ما بالد به حدی ‌کز کلاهی بگذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمع محفل داغ می‌گردد کز آهی بگذرد</p></div>
<div class="m2"><p>آه از آن روزی که حرص از دستگاهی بگذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دسترنج سعی آزادی نمی‌گردد تلف</p></div>
<div class="m2"><p>کهکشان بالد اگر از برگ کاهی بگذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در جنون دارد کسی تا کی سر زنجیر اشک</p></div>
<div class="m2"><p>سرده این دیوانه را شاید به راهی بگذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روشن است از جادهٔ انصاف حکم ما ز شمع</p></div>
<div class="m2"><p>داغ نقش پاست‌گر زین ره نگاهی بگذرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شمع بردار از مزار تیره‌روزان وفا</p></div>
<div class="m2"><p>باش تا بر خاک مژگان سیاهی بگذرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از غبار ما سواد عجز روشن‌کردنیست</p></div>
<div class="m2"><p>باید این خط هم به چشمت‌ گاه‌گاهی بگذرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عرض مطلب یک فلک ره دارد از دل تا زبان</p></div>
<div class="m2"><p>چون سحر صد نردبان بندی‌ که آهی بگذرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برنمی‌دارد چوگردون عمر تمکین وحشتم</p></div>
<div class="m2"><p>ننگ آن جولان که از من سال و ماهی بگذرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ترک دنیا هم دلیل پایهٔ دون همتی است</p></div>
<div class="m2"><p>سر به معنی پا شود تا ازکلاهی بگذرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نالهٔ نی می‌کشد از موج آب اواز پا</p></div>
<div class="m2"><p>عمر عاشق‌ گر همه د زیر چاهی بگذرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بی‌فنا ممکن بدان بیدل ‌گذشتن زین محیط</p></div>
<div class="m2"><p>بستن مژگان شود پل تا نگاهی بگذرد</p></div></div>