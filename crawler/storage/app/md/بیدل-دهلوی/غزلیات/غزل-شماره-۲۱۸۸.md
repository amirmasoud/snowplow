---
title: >-
    غزل شمارهٔ ۲۱۸۸
---
# غزل شمارهٔ ۲۱۸۸

<div class="b" id="bn1"><div class="m1"><p>ز فیض‌ ناتوانی مصرعی در خلق ممتازم</p></div>
<div class="m2"><p>چو ماه نو به یک بال آسمان سیر است پروازم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به یاد چشمی از خود می‌روم ای فرصت امدادی</p></div>
<div class="m2"><p>که ازگردش رسد رنگی به آن پیمانهٔ نازم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نوای فرصتم آهنگ عبرت نغمهٔ عمرم</p></div>
<div class="m2"><p>مپرس از نارسایی تا چه دارد رشتهٔ سازم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هجرت‌گر نی‌ام دمساز آه و ناله معذورم</p></div>
<div class="m2"><p>شکست خاطرم در سرمه خوابیده‌ست آوازم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز حیرت در کفم سر رشته‌ای داده‌ست پیدایی</p></div>
<div class="m2"><p>که تا مژگان بهم می‌آید انجام است آغازم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تماشاخانهٔ حسنم بقدر محوگردیدن</p></div>
<div class="m2"><p>تحیر بسکه لنگر می‌کند آیینه می‌سازم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهار آمد جنون از ششجهت سر پنجه می‌بازد</p></div>
<div class="m2"><p>چوگل من هم درین‌ گلشن‌ گریبانی بپردازم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طلسم غنچه توفان بهاری در قفس دارد</p></div>
<div class="m2"><p>دو عالم رنگ و بوی اوست هر جا گل‌ کند رازم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو صبح انکار عجزم نیست از اصناف آگاهی</p></div>
<div class="m2"><p>غباری را به‌گردون برده‌ام کم نیست اعجازم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غرور خودنمایی ها به‌این زحمت نمی‌ارزد</p></div>
<div class="m2"><p>به رنگ شمع چند از سر بریدن‌ گردن افرازم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به آسانی ز بار زندگی رستن نمی‌باشد</p></div>
<div class="m2"><p>مگر پیری خمی پیدا کند کز دوشش اندازم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به هر واماندگی از ساز وحشت نیستم غافل</p></div>
<div class="m2"><p>صدایی هست بیدل در شکست رنگ پروازم</p></div></div>