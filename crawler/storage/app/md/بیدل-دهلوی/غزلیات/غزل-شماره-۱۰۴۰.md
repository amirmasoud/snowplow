---
title: >-
    غزل شمارهٔ ۱۰۴۰
---
# غزل شمارهٔ ۱۰۴۰

<div class="b" id="bn1"><div class="m1"><p>نفس زینسان که بر عزم پرافشانی کدی دارد</p></div>
<div class="m2"><p>غبار رفتنت این دشت آمد آمدی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از این‌گلشن حضوری نیست آغوش تمنا را</p></div>
<div class="m2"><p>نگه بر هرچه مژگان واکند دست ردی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تماشا بسمل آن دست رنگین نیستی ورنه</p></div>
<div class="m2"><p>حضور سایهٔ برگ حنا هم مشهدی دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز سیمای سحر آموز فیض انشایی همت</p></div>
<div class="m2"><p>که دست از آستین بیرون‌کشیدن ساعدی دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیاز باید بایدکرد پیچ و تاب مهلت را</p></div>
<div class="m2"><p>دماغ بیکسان دود چراغ مرقدی دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بساط آفرینش را سر و پایی نمی‌باشد</p></div>
<div class="m2"><p>همین‌آثارکمفرصت جهان سرمدی دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر عجز است اگر طاقت به‌جایی می‌رسیم آخر</p></div>
<div class="m2"><p>ره واماندگان در لغزش پا مقصدی دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی غیر از یکی چیزی نمی‌ آرد به عرض اینجا</p></div>
<div class="m2"><p>احد در عالم تعداد میم احمدی دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز تصویر مزار اهل دل آواز می‌آید</p></div>
<div class="m2"><p>که در راه فنا از پا نشستن مسندی دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بعید است از زمین خاکسار اقبال‌ گردونی</p></div>
<div class="m2"><p>ز وضع سجده مگذر ناز رعنایی قدی دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز انجام بهار زندگی غافل مشو بیدل</p></div>
<div class="m2"><p>گل شمعی‌ که داری در نظر بوی بدی دارد</p></div></div>