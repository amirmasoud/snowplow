---
title: >-
    غزل شمارهٔ ۲۳۳۸
---
# غزل شمارهٔ ۲۳۳۸

<div class="b" id="bn1"><div class="m1"><p>به ذوق سجدهٔ او از عدم گلباز می‌آیم</p></div>
<div class="m2"><p>چه شوق‌ست اینکه یک پیشانی و صد ناز می‌آیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تحیر نامه‌ها دارم‌، هزار آیینه دربارم</p></div>
<div class="m2"><p>خیال آهنگ دیدارم به چندین ساز می‌آیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خمستان در رکاب گردش رنگم چه سحرست این</p></div>
<div class="m2"><p>به یاد نرگسی ساغرکش اعجاز می‌آیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طواف کعبهٔ دل آمد و رفت نفس دارد</p></div>
<div class="m2"><p>اگر صد بار ازین جا رفته باشم باز می‌آیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هر جا پاگذارم شوقت استقبال من دارد</p></div>
<div class="m2"><p>ادب پروردهٔ عشقم به این اعزاز می‌آیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز تجدید بهار انس دارم در نظر رنگی</p></div>
<div class="m2"><p>که‌ گر صد سال پیش آیم همان آغاز می‌آیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوای بوی گل سازم‌، نوید عالم رازم</p></div>
<div class="m2"><p>نسیم گلشن نازم‌، هزار انداز می‌آیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهار آرزو در دل‌،‌ گل امید در دامن</p></div>
<div class="m2"><p>به هر رنگی‌که می‌آیم چمن پرداز می‌آیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به حکم مهر تابان اختیاری نیست شبنم را</p></div>
<div class="m2"><p>پر و بالم تویی چندان‌ که در پرواز می‌آیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواص مرغ دست‌آموز دارد طینت بیدل</p></div>
<div class="m2"><p>به هر جا می‌روم تا می‌دهی آواز می‌آیم</p></div></div>