---
title: >-
    غزل شمارهٔ ۱۴۶۹
---
# غزل شمارهٔ ۱۴۶۹

<div class="b" id="bn1"><div class="m1"><p>هرکه انجام غرور من و ما می‌بیند</p></div>
<div class="m2"><p>بر فلک نیز همان در ته پا می‌بیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شش‌جهت آینهٔ عرض صواب است اما</p></div>
<div class="m2"><p>چشمت از کور دلی سهو خطا می‌بیند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم بر حلقهٔ دروازهٔ رحمت دارد</p></div>
<div class="m2"><p>خویش را هرکه به تسلیم دوتا می‌بیند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نکنی جرأت کاری که نباید کردن</p></div>
<div class="m2"><p>گر شوی اینقدر آگه ‌که خدا می‌بیند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زندگانی چه و آسودگی عمر کدام</p></div>
<div class="m2"><p>صبح ما عرض غباری به هوا می‌بیند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شمع‌وار آینهٔ راستی از دست مده</p></div>
<div class="m2"><p>کور هم ‌پیش و پس خود به عصا می‌بیند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جای رحم است‌ گر آزاده مقید گردد</p></div>
<div class="m2"><p>آب در کسوت آیینه چها می‌بیند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بلبل ما چه‌کندگر نشود محو خروش</p></div>
<div class="m2"><p>از رگ ‌گل همه محراب دعا می‌بیند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به که ما نیز چو شبنم عرقی آب شویم</p></div>
<div class="m2"><p>کان ‌گلستان حیا جانب ما می‌بیند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه ماضی‌ ست کجا حال و کدام استقبال</p></div>
<div class="m2"><p>دیده هر سو نگرد رو به قفا می‌بیند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بس‌که کاهیده‌ام از درد تمنا بیدل</p></div>
<div class="m2"><p>موی دارد به نظر هرکه مرا می‌بیند</p></div></div>