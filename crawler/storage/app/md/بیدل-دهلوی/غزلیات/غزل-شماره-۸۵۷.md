---
title: >-
    غزل شمارهٔ ۸۵۷
---
# غزل شمارهٔ ۸۵۷

<div class="b" id="bn1"><div class="m1"><p>فغان‌ که فرصت دام تلاش چیدن رفت</p></div>
<div class="m2"><p>پی‌گذشتن عمر آنسوی رسیدن رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چوشمع‌سربه هوآ سوخت جوهرتحقیق</p></div>
<div class="m2"><p>چه جلوه‌ها که نه درپیش پا ندیدن رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بس بلند فتاد آشیان خاموشی</p></div>
<div class="m2"><p>رسید ناله به جایی‌که از شنیدن رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه دم زنم زثبات‌بنای خودکه چوصبح</p></div>
<div class="m2"><p>نفس‌کشیدن من تا نفس‌ کشیدن رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طلب فسرد و نگردید محرم تپشی</p></div>
<div class="m2"><p>چو چشم آینه‌ام عمر بی‌پریدن رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جنون به ملک هوس داشت بوی عافیتی</p></div>
<div class="m2"><p>رمید فرصت وآرام تا رمیدن رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به رنگ غنچهٔ تصویر در بغل دارم</p></div>
<div class="m2"><p>شکفتنی ‌که به تاراج نادمیدن رفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی ز معنی چاک جگر چه شرح دهد</p></div>
<div class="m2"><p>خوشم‌که نامهٔ عشاق تا دریدن رفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه جلوه پرتو حیرت درتن بساط افکند</p></div>
<div class="m2"><p>کز آب چشمهٔ آیینه‌ها چکیدن رفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فنا به رفع بلاهای بی‌امان سپر است</p></div>
<div class="m2"><p>به سوختن ز سرشمع سربریدن رفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا به بیکسی اشک‌گریه می‌آید</p></div>
<div class="m2"><p>که در پی تو، به امید نارسیدن رفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گران شد آنقدر از گوهر نصیحت خلق</p></div>
<div class="m2"><p>که ‌گوش من چو صدف بیدل از شنیدن رفت</p></div></div>