---
title: >-
    غزل شمارهٔ ۲۳۷۱
---
# غزل شمارهٔ ۲۳۷۱

<div class="b" id="bn1"><div class="m1"><p>پیمانهٔ غناکدهٔ بی‌مثالیم</p></div>
<div class="m2"><p>پر نیست آنقدر که توان‌ کرد خالیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شادم به‌کنج فقر کز ابنای روزگا‌ر</p></div>
<div class="m2"><p>سیلی خور جواب نشد بی سوالیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک ضعیف مرکز صد شعله رنگ و بوست</p></div>
<div class="m2"><p>غافل مشو ز وحشت افسرده بالیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آغوش مه پر است ز کیفیت هلال</p></div>
<div class="m2"><p>بالیده‌ گیر نقص ز صاحب کمالیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پستی ‌گل بلندی نخلست ربشه را</p></div>
<div class="m2"><p>در خاک خفته اینقدر از طبع عالیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از بس به رنگ نی پرم از انتظار درد</p></div>
<div class="m2"><p>آغوش ناله می‌کند از خویش خالیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عمریست ‌وحشتم ‌نگه چشم ‌حیرتیست</p></div>
<div class="m2"><p>یادت نشانده است غبار غزالیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سامان طراز راحتم از سعی نارسا</p></div>
<div class="m2"><p>افکنده خواب با همه‌ جا فرش قالیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از بسکه ناله داشت نی بوریای فقر</p></div>
<div class="m2"><p>مخمل نبرد صرفهٔ خواب از نهالیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فریاد کز فسردگی باغ اعتبار</p></div>
<div class="m2"><p>هم جوهر چنار نشدکهنه سالیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آغوش حیرتم به چه تنگی ‌گشوده‌اند</p></div>
<div class="m2"><p>در من شکسته است چو گردون حوالیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نتوان به چشم داد سراغ نمود من</p></div>
<div class="m2"><p>بیدل به یمن ضعف چو معنی خیالیم</p></div></div>