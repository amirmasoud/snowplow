---
title: >-
    غزل شمارهٔ ۹۶۶
---
# غزل شمارهٔ ۹۶۶

<div class="b" id="bn1"><div class="m1"><p>جهان‌کجاست‌،‌گلی زان نقاب می‌خندد</p></div>
<div class="m2"><p>سحر تبسمی از آفتاب می‌خندد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فنای ما چمن‌آرای بی‌نقابی اوست</p></div>
<div class="m2"><p>به قدر چاک کتان‌، ماهتاب می‌خندد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تلاش آگهی‌ات ننگ غفلت است اینجا</p></div>
<div class="m2"><p>مژه ز هم نگشایی‌که خواب می‌خندد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تهی ز خویش شدن مفت آگهی باشد</p></div>
<div class="m2"><p>ز صفر بر خط ما انتخاب می‌خندد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کجاست فرصت دیگرکه ما به خود بالیم</p></div>
<div class="m2"><p>محیط نیز در اینجا حباب می‌خندد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زعلم وفضل بجزعبرت آنچه جمع‌کنید</p></div>
<div class="m2"><p>گشاد هر ورقش برکتاب می‌خندد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درنگ راهبرکاروان فرصت نیست</p></div>
<div class="m2"><p>کجا روبم‌که هر سو شتاب می‌خندد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به‌درسگاه‌ ادب حرف‌ و صوف مسخرگی‌ست</p></div>
<div class="m2"><p>ز صد سؤال همین یک جواب می‌خندد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز برق حسن‌ کسی را مجال جرات نیست</p></div>
<div class="m2"><p>بپوش چشم که حکم حجاب می‌خندد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زبان به لاف مده‌، پاس شرم مغتنم است</p></div>
<div class="m2"><p>چو بازگشت لب موج آب می‌خندد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غبار صبح تماشاست هرچه باداباد</p></div>
<div class="m2"><p>تو هم بخند جهان خراب می‌خندد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دلت چو شمع به هجر که داغ شد بیدل</p></div>
<div class="m2"><p>کز اشک گرم تو بوی کباب می‌خندد</p></div></div>