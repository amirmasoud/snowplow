---
title: >-
    غزل شمارهٔ ۸۱۰
---
# غزل شمارهٔ ۸۱۰

<div class="b" id="bn1"><div class="m1"><p>جز خموشی هرکه دل بر ناله و فریاد داشت</p></div>
<div class="m2"><p>شمع خود را همچو نی در رهگذار باد داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای خوش آن عهدی‌که در محراب چشم انتظار</p></div>
<div class="m2"><p>اشک ما هم‌گردشی چون سبحهٔ زهاد داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صید ما را حلقهٔ دام بلا شد عافیت</p></div>
<div class="m2"><p>گوشهٔ چشمی‌که با دل الفت صیاد داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواب اگروحشت گرفت از دیدهٔ من دور نیست</p></div>
<div class="m2"><p>خانهٔ چشمم چوگوهر آب در بنیاد داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیخودی از معنی جمعیتم آگاه‌کرد</p></div>
<div class="m2"><p>گردش رنگ اعتبار سیلی استاد داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کرد تعمیر اینقدرگرد خرابی آشکار</p></div>
<div class="m2"><p>ورنه ویران بودن ما عالمی آباد داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این زمان محو فرامش نغمگی‌های دلیم</p></div>
<div class="m2"><p>جام ما پیش ازشکستنها ترنگی یاد داشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از فنای ما مشو غافل‌که این مشت شرار</p></div>
<div class="m2"><p>چشم زخم نیستی در عالم ایجاد داشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دوش‌کز سازعدم هستی ظهور آهنگ بود</p></div>
<div class="m2"><p>نالهٔ ما هم نوای هرچه باداباد داشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حیف اوقاتی‌که صرف‌کوشش بیجا شود</p></div>
<div class="m2"><p>تیشه عمری نوحه بر جان‌کندن فرهاد داشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بال قمری این زمان بیدل غبار سرو نیست</p></div>
<div class="m2"><p>گردوحشت پیش ازین هم هرکه بود آزاد داشت</p></div></div>