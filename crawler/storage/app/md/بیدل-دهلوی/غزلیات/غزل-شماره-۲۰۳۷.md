---
title: >-
    غزل شمارهٔ ۲۰۳۷
---
# غزل شمارهٔ ۲۰۳۷

<div class="b" id="bn1"><div class="m1"><p>دوش ‌گستاخ به نظارهٔ جانان رفتم</p></div>
<div class="m2"><p>جلوه چندان به عرق زد که به توفان رفتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیر این انجمنم آمد و رفت سحراست</p></div>
<div class="m2"><p>یک نفس نامده صد زخم نمایان رفتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فیض عریان تنی‌ام خلعت صحرا بخشید</p></div>
<div class="m2"><p>جیب شوق آنهمه وا شد که به‌ دامان رفتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی نشانی اثرم آینهٔ بوی ‌گلم</p></div>
<div class="m2"><p>رنگ شد کسوت من ‌کاینهمه عریان رفتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیش ازین سعی زمینگیر خموشی چه ‌کند</p></div>
<div class="m2"><p>تا به جایی ‌که نفس ماند ز جولان رفتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فکر خود بود همان خلوت تحقیق وصال</p></div>
<div class="m2"><p>تا به دامان تو از راه‌ گریبان رفتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چقدر کاغذ آتش زده‌ام داغ تو داشت</p></div>
<div class="m2"><p>که ز خود نیز به سامان چراغان رفتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تپش دل سحری بوی ‌گلی می‌آورد</p></div>
<div class="m2"><p>رفتم از خویش ندانم به چه عنوان رفتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بایدم تا ابد از خود به خیالش رفتن</p></div>
<div class="m2"><p>یارب از بهر چه آنجا من حیران رفتم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نگه‌دیدهٔ قربانی‌ام از شوق مپرس</p></div>
<div class="m2"><p>سر آن جلوه رهی داشت ‌که پنهان رفتم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جرأت پا نپسندید طواف چمنش</p></div>
<div class="m2"><p>حیرتم رنگ ادب ریخت به مژگان رفتم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خجلت نشو و نمایم به عدم یاد آمد</p></div>
<div class="m2"><p>رنگ ناکرده گل از چهرهٔ امکان رفتم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پای پر آبله شد دست تأسف بیدل</p></div>
<div class="m2"><p>بسکه از وادی امید پشیمان رفتم</p></div></div>