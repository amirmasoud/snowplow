---
title: >-
    غزل شمارهٔ ۲۸۱۵
---
# غزل شمارهٔ ۲۸۱۵

<div class="b" id="bn1"><div class="m1"><p>دلت فسرد جنونی ‌کز آشیانه برآیی</p></div>
<div class="m2"><p>چو ناله دامن صحرا به ‌کف ز خانه برآیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به ساز عجز ز سر چنگ خلق نیست‌ گزیرت</p></div>
<div class="m2"><p>چو مو زپرده چه لازم به ذوق شانه برآیی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر التزام جنون نیست سعی‌ گوشهٔ فقری</p></div>
<div class="m2"><p>مگر ز جرگهٔ یاران به این بهانه برآیی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شعار طبع رسا نیست انتظار مواعظ</p></div>
<div class="m2"><p>ز توسنی است‌ که محتاج تازیانه برآیی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو موج ‌گوهر اگر بگذری ز فکر تردد</p></div>
<div class="m2"><p>برون نرفته ازین بحر برکرانه برآیی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زجا درآمدن آنگه به حرف پوچ حیاکن</p></div>
<div class="m2"><p>نه کودکی که به صورت دهل زخانه برآیی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو مور نقب قناعت رسان به ‌کنج غنایی</p></div>
<div class="m2"><p>که پر بر آری و از احتیاج دانه برآیی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زگوشهٔ دل جمع آن زمان دهند سراغت</p></div>
<div class="m2"><p>که همچو فرصت آسودن از زمانه برآیی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به خاک نیز پر افشان فتنه‌ای‌ست غبارت</p></div>
<div class="m2"><p>بخواب آنهمه کز عالم فسانه برآیی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به خود ستایی بیهوده شرم دار ز همت</p></div>
<div class="m2"><p>که لاف دل زنی و بیدل از میانه برآیی</p></div></div>