---
title: >-
    غزل شمارهٔ ۱۸۳۴
---
# غزل شمارهٔ ۱۸۳۴

<div class="b" id="bn1"><div class="m1"><p>طرب خواهی درین محفل برون آ گامی آن سویش</p></div>
<div class="m2"><p>بنالد موج از دریا، تهی ناکرده پهلویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلستانی‌ که حرص احرام عشرت بسته است آنجا</p></div>
<div class="m2"><p>به جای سبزه می‌روید دم تیغ از لب جویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چراغ مطلب نایاب ما روشن نمی‌گردد</p></div>
<div class="m2"><p>نفس تا چند باید سوخت در وهم تک و پویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به آهی می‌توانم ساز تسخیر جهان کردن</p></div>
<div class="m2"><p>به دست آورده‌ام سر رشته‌ای از تار گیسویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غبار یک جهان دل می‌کند توفان نومیدی</p></div>
<div class="m2"><p>مبادا سر بر آرد جوهر از آیینه‌ای رویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به تاراج نگاه ناتوانش داده‌ام طاقت</p></div>
<div class="m2"><p>هنوزم در کمین قامت پیریست ابرویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صبا تا گردی از خاک سر راه تو می‌آرد</p></div>
<div class="m2"><p>چمن در کاسهٔ گل می‌کند در یوزهٔ بویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درین محفل ندارد سایه هم امید آسودن</p></div>
<div class="m2"><p>مگر در خانهٔ خورشید گردد گرم پهلویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جنون را تهمت عجز است بی‌سرمایگی‌هایت</p></div>
<div class="m2"><p>گریبانی نداری تا ببینی زور بازویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هوای‌ گل نمی‌دانم دماغ مل نمی‌فهمم</p></div>
<div class="m2"><p>سری دارم‌ که سامان نیست جز تسلیم زانویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به زلفی بسته‌ام دل از مضامینم چه می‌پرسی</p></div>
<div class="m2"><p>دو عالم معنی باریک قربان سر مویش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کرا تاب عتاب اوست بیدل‌ کاتش سوزان</p></div>
<div class="m2"><p>به خاکستر نفس می دزدد از اندیشه ی خویش</p></div></div>