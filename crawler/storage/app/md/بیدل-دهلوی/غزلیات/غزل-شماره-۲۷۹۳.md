---
title: >-
    غزل شمارهٔ ۲۷۹۳
---
# غزل شمارهٔ ۲۷۹۳

<div class="b" id="bn1"><div class="m1"><p>به طبع مقبلان یارب‌کدورت را مده راهی</p></div>
<div class="m2"><p>براین ‌آیینه‌ها مپسند زنگ تهمت آهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چراغ ابلهان عمری‌ست می‌سوزد درین محفل</p></div>
<div class="m2"><p>چه باشد یک شرر بالد فروغ طبع آگاهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان آیینهٔ وهم است و این طوطی سرشتانش</p></div>
<div class="m2"><p>نفس پرداز تقلیدند و می‌گویند اللهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پر است آفاق از غولان آدم رو چه سازست این</p></div>
<div class="m2"><p>به این بی‌حاصلان یا دانشی یا مرگ ناگاهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به حیرتگاه وصل افسون هجران عالمی دارد</p></div>
<div class="m2"><p>فراموشی نصیبم کن مگر یادت کنم گاهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تپش‌ها دارم و از آشیان بیرون نمی‌آیم</p></div>
<div class="m2"><p>به این انداز مژگان هم ندارد بال‌ کوتاهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به خاک آستانت چون هلال از بس‌که‌ گم‌ گشتم</p></div>
<div class="m2"><p>جبینی یافتم در نقش پیشانی پس از ماهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ندانم مژدهٔ وصل که دارد انتظار من</p></div>
<div class="m2"><p>که حسرت سخت گلبازست با گرد سر راهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چراغ عبرت من از گداز شمع شد روشن</p></div>
<div class="m2"><p>بغیر از زندگانی نیست اینجا داغ جانکاهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به تنگیهای دل یک غنچه نتوان نقش بست اینجا</p></div>
<div class="m2"><p>شکستم رنگ تا تغییر دادم بستر آهی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ببینم تا کجاها می‌برد فکر خودم بیدل</p></div>
<div class="m2"><p>به رنگ شمع امشب در گریبان کنده‌ام چاهی</p></div></div>