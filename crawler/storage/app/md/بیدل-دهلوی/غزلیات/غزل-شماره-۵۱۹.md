---
title: >-
    غزل شمارهٔ ۵۱۹
---
# غزل شمارهٔ ۵۱۹

<div class="b" id="bn1"><div class="m1"><p>داغ اگر حلقه زند ساغر صهبای دل است</p></div>
<div class="m2"><p>ناله گر بال کشد گردن مینای دل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست بی‌شور جنون، مشت غباری زین دشت</p></div>
<div class="m2"><p>ششجهت‌، عرض پریشانی اجزای دل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دهرگو تنگتر از قطرهٔ خونم گیرد</p></div>
<div class="m2"><p>گره آبله میدان تپشهای دل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مسطر صفحهٔ آیینه همان جوهر اوست</p></div>
<div class="m2"><p>نفس سوخته هم جادهٔ صحرای دل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشرت خانهٔ تاریک، ز روزن باشد</p></div>
<div class="m2"><p>زخم پیکان توام چشم تماشای دل است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پشه تخم است‌، به هرجا، ز دویدن واماند</p></div>
<div class="m2"><p>نفس از ضبط من و ماگهرآرای دل است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راحت شیشه در آغوش شکست است اینجا</p></div>
<div class="m2"><p>صدف‌گوهر ما زخم طربزای دل است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به‌که جزبرورق‌گل ننشیند شبنم</p></div>
<div class="m2"><p>بیشتر دست نگارین بتان جای دل است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون طلب سوخت نفس‌،‌گریه روان می‌گردد</p></div>
<div class="m2"><p>اشک یکسر قدم آبله‌فرسای دل است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بحر، بر موج‌گهر، حکم روانی می‌کرد</p></div>
<div class="m2"><p>گفت‌: معذور،‌که در دامن من‌، پای دل است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درد، مشکل‌که ازین دایره بیرون تازد</p></div>
<div class="m2"><p>آنچه در ای شکست آمده مینای دل است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل ازگرد هوس در قفس یاس مباش</p></div>
<div class="m2"><p>زنگ آیینه‌ات افسون تمنای دل است</p></div></div>