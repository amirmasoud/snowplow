---
title: >-
    غزل شمارهٔ ۱۱۸۸
---
# غزل شمارهٔ ۱۱۸۸

<div class="b" id="bn1"><div class="m1"><p>مکتوب شوق هرگز بی‌نامه‌بر نباشد</p></div>
<div class="m2"><p>ما و ز خویش رفتن قاصد اگر نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرجا تنید فطرت یک حلقه داشت‌ گردون</p></div>
<div class="m2"><p>در فهم پرگار حکم دو سر نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاشاک را در آتش تاکی خیال پختن</p></div>
<div class="m2"><p>آنجاکه جلوهٔ اوست از ما اثر نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مغرور فرصت دهر زین بیشتر مباشید</p></div>
<div class="m2"><p>بست وگشاد مژگان شام و سحر نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برقی ز دور داردهنگامهٔ تجلی</p></div>
<div class="m2"><p>ای بیخودان ببینید دل جلوه‌گر نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما را به رنگ شبنم تا آشیان خورشید</p></div>
<div class="m2"><p>باید به دیده رفتن‌گر بال و پر نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرچندکار فرداست امروز مفت خودگیر</p></div>
<div class="m2"><p>شاید دماغ وطاقت وقت دگر نباشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زاهد ز وضع خلوت نازکمال مفروش</p></div>
<div class="m2"><p>افسردن ازکف خاک چندان هنر نباشد.</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آیینه خانهٔ دل آخر به زنگ دادیم</p></div>
<div class="m2"><p>زین بیش آه ما را رنگ اثر نباشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواهی به خلق روکن خواهی خیال او کن</p></div>
<div class="m2"><p>در عالم تماشا بر خود نظر نباشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آسودگی مجویید از وضع اشک بیدل</p></div>
<div class="m2"><p>این جوهر چکیدن آب‌گهر نباشد</p></div></div>