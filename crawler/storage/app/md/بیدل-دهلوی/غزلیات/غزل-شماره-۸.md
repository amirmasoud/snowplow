---
title: >-
    غزل شمارهٔ ۸
---
# غزل شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>روزی‌که زد به خواب شعورم ایاغ پا</p></div>
<div class="m2"><p>من هم زدم ز نشئه به چندین دماغ پا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رنگ حنا زطبع چمن موج می‌زند</p></div>
<div class="m2"><p>شسه‌ست گویی آن گل خودرو به باغ پا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیر بهار رنگ ندارد گل ثبات</p></div>
<div class="m2"><p>لغزد مگر چو لاله کسی را به داغ پا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنجا که نقش پای تو مقصود جستجوست</p></div>
<div class="m2"><p>سر جای موکشد به هوای سراغ پا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز خاک تیره نیست بنای جهان رنگ</p></div>
<div class="m2"><p>طاووس سوده است به منقار زاغ پا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با طبع سرکش اینهمه رنج وفا مبر</p></div>
<div class="m2"><p>روز سوار، شب کند اسب چراغ پا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک گام اگر ز وهم تعلق گذشته‌ای‌</p></div>
<div class="m2"><p>بیدل درازکن به بساط فراغ پا</p></div></div>