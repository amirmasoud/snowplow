---
title: >-
    غزل شمارهٔ ۲۲۴۲
---
# غزل شمارهٔ ۲۲۴۲

<div class="b" id="bn1"><div class="m1"><p>مزرع تسلیم ادب حاصلم</p></div>
<div class="m2"><p>سر نکشد گردن آب و گلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>موج ‌گهر نیستم اما ز ضعف</p></div>
<div class="m2"><p>آبله‌ گل‌کرده ره منزلم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک ندامت به سر عاجزی</p></div>
<div class="m2"><p>صبحم اگر تار نفس بگسلم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفی من آیینهٔ اثبات اوست</p></div>
<div class="m2"><p>حق دمد آندم‌ که‌ کنی باطلم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بار نفس می‌کشم و چاره نیست</p></div>
<div class="m2"><p>بی‌تو فتاده‌ست الم بر دلم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>الفت دل سدّ ره‌ کس مباد</p></div>
<div class="m2"><p>کرد همین آبله پا در گلم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عافیتم داد به توفان شرم</p></div>
<div class="m2"><p>راند به دریا عرق ساحلم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خامشی اسباب غنا بود و بس</p></div>
<div class="m2"><p>تا به زبان آمده‌ام سایلم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر تپشم تهمت راحت مبند</p></div>
<div class="m2"><p>بیضه منه زبر پر بسملم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرد من از قافلهٔ رنگ نیست</p></div>
<div class="m2"><p>کلک مصور چه‌کشد محملم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نامه برید از چمن خون من</p></div>
<div class="m2"><p>برگ حنایی به کف قاتلم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آبم ازین درد که آن مست ناز</p></div>
<div class="m2"><p>آینه می‌خواهد و من بیدلم</p></div></div>