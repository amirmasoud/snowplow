---
title: >-
    غزل شمارهٔ ۲۶۷۱
---
# غزل شمارهٔ ۲۶۷۱

<div class="b" id="bn1"><div class="m1"><p>نه نفس تربیتم ‌کرد و نه دامان مددی</p></div>
<div class="m2"><p>آتشم خاک شد ای سوخته جانان مددی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوق دیدارم و یک جلوه ندارم طاقت</p></div>
<div class="m2"><p>مگر آیینه‌ کند بر من حیران مددی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آرزو می‌کشدم بر در ابرام طلب</p></div>
<div class="m2"><p>کو حیا تاکند از وضع پشیمان مددی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یاد چشم تو ز آوارگیم غافل نیست</p></div>
<div class="m2"><p>گرد این دشتم و دارم ز غزالان مددی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسملم‌ گرم طواف چمن عافیتی است</p></div>
<div class="m2"><p>ای تپیدن به تغافل نزنی هان مددی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راحت از قافلهٔ هوش برون تاخته است</p></div>
<div class="m2"><p>ای جنون تا شودم بار دل آسان مددی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کیست بار تپش از دوش هوس بردارد</p></div>
<div class="m2"><p>بی‌عصایی نکند گر به ضعیفان مددی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با همه ظلم رها نیست‌کس ازمنت چرخ</p></div>
<div class="m2"><p>آه از آن روز که می‌کرد به احسان مددی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حیله جوی نم اشکیم درین وادی خشک</p></div>
<div class="m2"><p>کاش از آبله بخشند به مژگان مددی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل از غنچه‌گرفتم سبق زانوی فکر</p></div>
<div class="m2"><p>بود کوتاهی دامن به گریبان مددی</p></div></div>