---
title: >-
    غزل شمارهٔ ۲۵۴۳
---
# غزل شمارهٔ ۲۵۴۳

<div class="b" id="bn1"><div class="m1"><p>چون‌گهر هر چند بر دریا تند غوغای من</p></div>
<div class="m2"><p>در نم یک چشم سر غرق‌ست سرتا پای من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناتوانی همچو من در عالم تسلیم نیست</p></div>
<div class="m2"><p>بیشتر از سایه می‌بوسد زمین اعضای من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مسند آتش همان تسلیم خاکستر خوشست</p></div>
<div class="m2"><p>جز غبار خوبش‌ ننشیندکسی بر جای من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اینقدر چون شمع محو انتظار کیستم</p></div>
<div class="m2"><p>بر سر مژگان وطن‌ کرده‌ست دیدنهای من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منع در سعی طلب ترغیب سالک می‌شود</p></div>
<div class="m2"><p>«‌لن‌ترانی‌» داشت درس همت موسای من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زندگی پر بیخبر بود از اشارات فنا</p></div>
<div class="m2"><p>قامت خم‌گشته‌گردید ابروی ایمای من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لفظ ممکن نیست برمعنی نچیند دقتی</p></div>
<div class="m2"><p>باده بر دل سنگ بست از الفت مینای من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نالهٔ محو خیالت قابل تحریر نیست</p></div>
<div class="m2"><p>هر قدر ننوشته‌ام بی‌پرده است انشای من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در جنون عریانی‌ام تشریف ‌امنی دیگر است</p></div>
<div class="m2"><p>یا رب این خلعت نگردد تنگ‌بر بالای من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از غبار شیشهٔ ساعت قدح پر می‌کنم</p></div>
<div class="m2"><p>خشکی این بزم نم نگذاشت در صهبای من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سایه‌ام بیدل ز نیرنگ غم و عیشم مپرس</p></div>
<div class="m2"><p>نیست ممتاز آنقدر روز من از شبهای من</p></div></div>