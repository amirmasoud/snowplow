---
title: >-
    غزل شمارهٔ ۲۴۱۴
---
# غزل شمارهٔ ۲۴۱۴

<div class="b" id="bn1"><div class="m1"><p>سر به زیر تیغ و پا بر خار باید تاختن</p></div>
<div class="m2"><p>چون به عرض آمد برون تار باید تاختن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نغمهٔ تحقیق محو پردهٔ اخفا خوش است</p></div>
<div class="m2"><p>یکقدم ره چون نفس صد بار باید تاختن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منت هستی قبول اختیارکس مباد</p></div>
<div class="m2"><p>دوش‌ مزدوریم و زیر بار باید تاختن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون بهارم کوشش بیجا ندارد انقطاع</p></div>
<div class="m2"><p>رنگ امسال مرا تا پار باید تاختن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهد منصوری‌ کمینگاه سوار همت است</p></div>
<div class="m2"><p>گر تو هم زین عرصه‌ای تا دار باید تاختن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دشت آتشبار و دل بیچارهٔ ضبط عنان</p></div>
<div class="m2"><p>نی‌سواران نفس ناچار باید تاختن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پاس دل تا چند دارد کس درین آشوبگاه</p></div>
<div class="m2"><p>شیشه در باریم و برکهسار باید تاختن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرکزپرگار غفلت ما همین جسم است وبس</p></div>
<div class="m2"><p>سایه را پیش و پس دیوار باید تاختن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون‌ گلم در غنچه چندین چشم زخم آسوده است</p></div>
<div class="m2"><p>آه از آن روزی‌که در بازار باید تاختن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عرصهٔ شوق عدم پر بی‌کنار افتاده است</p></div>
<div class="m2"><p>هر چه باشی چون شرر یکبار باید تاختن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سعی مردی خاک شد هرگاه همت باخت رنگ</p></div>
<div class="m2"><p>مرکب پی‌کرده را دشوار باید تاختن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سر به‌گردون تازیت چون شمع پر بیصرفه است</p></div>
<div class="m2"><p>چاه پیش است اندکی هشیار باید تاختن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پیش پای سایه تشویش بلند و پست نیست</p></div>
<div class="m2"><p>گر جبین رهبر شود هموار باید تاختن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>موج ما تاگوهر دل ره به آسانی نبرد</p></div>
<div class="m2"><p>در پی این آبله بسیار باید تاختن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای سحر زین یک تبسم‌وار جولان نفس</p></div>
<div class="m2"><p>تا کجا گل بر سر دستار باید تاختن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شرم‌دار از دعوی هستی که در میدان لاف</p></div>
<div class="m2"><p>یکقدم ره چون نفس صد بار باید تاختن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از خط تسلیم بیدل تا توانی سر متاب</p></div>
<div class="m2"><p>سبحه را بر جاده زنار باید تاختن</p></div></div>