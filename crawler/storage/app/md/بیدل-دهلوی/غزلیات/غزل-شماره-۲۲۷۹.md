---
title: >-
    غزل شمارهٔ ۲۲۷۹
---
# غزل شمارهٔ ۲۲۷۹

<div class="b" id="bn1"><div class="m1"><p>ای طرب وجدی که باز آغوش گل وامی‌کنم</p></div>
<div class="m2"><p>بعد سالی چون بهار این رنگ پیدا می‌کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چار دیوار توّهم سدّ راه شوق چند</p></div>
<div class="m2"><p>کعبه‌ای دارم به پیش‌، آهنگ صحرا می‌کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقی بزم نشاط امروز شرم نرگسی است</p></div>
<div class="m2"><p>از عرق چون ابر طرح جام و مینا می کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حسن خلقی در نظر دارم‌ که افسون هوس</p></div>
<div class="m2"><p>گر همه آیینه بینم در دلش جا می‌کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون شفق هرچند بر چرخم برد پرواز رنگ</p></div>
<div class="m2"><p>همچنان سیر حنای آن کف پا می‌کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در طربگاه حضورم بار فرصت داده‌اند</p></div>
<div class="m2"><p>روزکی چند انتخاب آرزوها می‌کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک نگه دیدار می‌خواهم دو عالم حوصله</p></div>
<div class="m2"><p>می‌گدازم کاینقدر طاقت مهیا می‌کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زین کلامم معنی خاصیت سود اتفاق</p></div>
<div class="m2"><p>غیر پندارد به حرف و صوت سودا می‌کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در دبستان محبت طور دانش دیگر است</p></div>
<div class="m2"><p>سجده می‌خوانم خط‌ پیشانی انشا می‌کنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حیرتم بیدل سفارشنامهٔ آیینه است</p></div>
<div class="m2"><p>می‌روم جایی که خود را او تماشا می‌کنم</p></div></div>