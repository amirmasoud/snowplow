---
title: >-
    غزل شمارهٔ ۸۶
---
# غزل شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>هرکجا تسلیم بندد بر میان شمشیر را</p></div>
<div class="m2"><p>می‌کندچون موج‌گوهر بی‌زبان شمشیر را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرکشی وقف تواضع‌کن‌که برگردون هلال</p></div>
<div class="m2"><p>می‌کندگاهی سپرگاهی‌کمان شمشیر را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا به خود جنبی سپر افکندهٔ خاکی و بس</p></div>
<div class="m2"><p>گو بیاویزد غرور از آسمان شمشیر را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسمل آهنگان‌، تسلیمت مهیا کرده‌اند</p></div>
<div class="m2"><p>جبههٔ شوقی‌که داند آستان شمشیر را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حسن تا سر داد ابرو را به قتل عاشقان</p></div>
<div class="m2"><p>قبضه شدانگشت حیرت در دهان شمشیر را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گشت از خواب‌گر‌ان چشمت به خون ما دلیر</p></div>
<div class="m2"><p>می‌کند بیباکتر سنگ فسان شمشیر را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زایل از زینت نگردد جوهر مردانگی</p></div>
<div class="m2"><p>قبضهٔ زر از برش مانع مدان شمشیر را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر شجاعت پیشه ننگ است از تهور دم مزن</p></div>
<div class="m2"><p>حرف جوهر برنیاید از زبان شمشیر را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسمل موج می‌ام زخمم همان خمیازه است</p></div>
<div class="m2"><p>در لب ساغرکن ای قاتل نهان شمشیر را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نوبهار عشرتم بیدل‌که با این لاغری</p></div>
<div class="m2"><p>خون صیدم‌کرد شاخ ارغوان شمشیر را</p></div></div>