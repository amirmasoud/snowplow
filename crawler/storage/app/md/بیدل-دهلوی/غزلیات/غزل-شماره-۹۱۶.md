---
title: >-
    غزل شمارهٔ ۹۱۶
---
# غزل شمارهٔ ۹۱۶

<div class="b" id="bn1"><div class="m1"><p>حسنی که یادش آینهٔ حیرت آب داد</p></div>
<div class="m2"><p>زان رنگ جلوه کرد که داد نقاب داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرجا بهار جلوه او در نظر گذشت</p></div>
<div class="m2"><p>شکی‌که سر زد از مژه بوی‌گلاب داد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک -جلوه داشت عاشق ومعشوق پیش این</p></div>
<div class="m2"><p>خون ‌گردد امتیاز که عرض حجاب داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرواز شوق از عرق شرم‌گل نکرد</p></div>
<div class="m2"><p>خاکم غبارهای تپیدن به آب داد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از حرص این قدر غم سباب می کشم</p></div>
<div class="m2"><p>لب‌تشنگی سرم به محیط سراب داد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آخر ز گریه نشئهٔ شوقم بلند شد</p></div>
<div class="m2"><p>اشک آنقدر چکید که جام شراب داد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زان گلستان که رنگ گلش داغ لاله است</p></div>
<div class="m2"><p>نشکفت غنچه‌ای‌که نه بوی‌کباب داد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کم‌فرصتی به عرض تماشای این محیط</p></div>
<div class="m2"><p>آیینهٔ خیال به دست حباب داد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از بس که معنی‌ام رقمی جز هوا نداشت</p></div>
<div class="m2"><p>گردون به نقطهٔ شررم انتخاب داد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>داغم ز رشک منتظری کز هجوم شوق</p></div>
<div class="m2"><p>جان داد اگر به قاصد جانان جواب داد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون صبح در معاملهٔ‌ گیر و دار عمر</p></div>
<div class="m2"><p>چندان نه‌ایم ساده که باید حساب‌ داد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل ز آبروطلبی دست شسته‌ایم</p></div>
<div class="m2"><p>کاین آرزو بنای دو عالم به آب داد</p></div></div>