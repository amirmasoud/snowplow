---
title: >-
    غزل شمارهٔ ۲۲۰۰
---
# غزل شمارهٔ ۲۲۰۰

<div class="b" id="bn1"><div class="m1"><p>از ضعف بسکه در همه جا دیر می‌رسم</p></div>
<div class="m2"><p>تا پای خود چو شمع‌ به شبگیر می‌رسم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وهم علایق از همه سو رهزن دل است</p></div>
<div class="m2"><p>پا درگل خیال به صد قیر می‌رسم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برنقش پای شمع تصور حنا مبند</p></div>
<div class="m2"><p>من رنگها شکسته به تصویر می‌رسم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رنگ بنای صبح ز آب وگل فناست</p></div>
<div class="m2"><p>بر باد می‌روم ‌که به تعمیر می‌رسم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از کام حرص لذت طفلی نمی‌رود</p></div>
<div class="m2"><p>دندان شکسته باز پی شیر می‌رسم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگذار چون سحر فکنم طرح فرصتی</p></div>
<div class="m2"><p>گرد رمی ز دور نفس‌گیر می‌رسم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواب عدم فسانهٔ هستی‌شنیده است</p></div>
<div class="m2"><p>شادم‌کزین بهانه به تعبیر می‌رسم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون شمع رنگم از چه بهارآفریده است</p></div>
<div class="m2"><p>کز هر نگه به صد گل تغییر می‌رسم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از نارسایی ثمر خام من مپرس</p></div>
<div class="m2"><p>تا رنگ زرد نیز همان دیر می‌رسم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آسان نمی‌رسد به تسلی جنون من</p></div>
<div class="m2"><p>چون ناله رفته رفته به زنجیر می‌رسم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای قامت خمیده دو گام آرمیده رو</p></div>
<div class="m2"><p>من هم به تو همین که شدم پیر می‌رسم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همدم چو فرصت از دو جهان قطع‌ الفت است</p></div>
<div class="m2"><p>بر هر چه می‌رسم دم شمشیر می‌رسم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بیدل همین قدر اثرم بس که گاهگاه</p></div>
<div class="m2"><p>بر گوش ناسخن شنوان تیر می‌رسم</p></div></div>