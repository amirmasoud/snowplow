---
title: >-
    غزل شمارهٔ ۲۴۸۵
---
# غزل شمارهٔ ۲۴۸۵

<div class="b" id="bn1"><div class="m1"><p>بر شیشه خانهٔ دل افسرده سنگ زن</p></div>
<div class="m2"><p>کم نیستی زگل قدحی را به رنگ زن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمی به وحشت آب ده از باغ اعتبار</p></div>
<div class="m2"><p>مهری تو هم به محضر داغ پلنگ زن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رنج دگر مکش به کمانخانهٔ سپهر</p></div>
<div class="m2"><p>جای نفس همین پر و بال خدنگ زن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تسلیم حکم عشق نشاید کم از سپند</p></div>
<div class="m2"><p>گر خود در آتشت بنشاند شلنگ زن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امن است هرکجا سر تسلیم رهبر است</p></div>
<div class="m2"><p>زین وضع فال‌گیر و به‌ کام نهنگ زن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تاکی نفس به خون‌کشی از انتقام خصم</p></div>
<div class="m2"><p>تیغی‌که می‌زنی به فسانش به رنگ زن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرغنچه زین بهارطلسم شکفتنی است</p></div>
<div class="m2"><p>ای غافل از طرب در دلهای تنگ زن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خلد و جحیم چند کند غافل از خودت</p></div>
<div class="m2"><p>آتش به‌ کارگاه خیالات بنگ زن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همت زمین مشرب تغییر خجلت است</p></div>
<div class="m2"><p>در دامنی‌ که چین نزند دست چنگ زن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خمخانه‌ها به ‌گردش چشمت نمی‌رسد</p></div>
<div class="m2"><p>امشب محرفی به دماغ فرنگ زن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل شکست شیشهٔ دل نیز عالمی‌ست</p></div>
<div class="m2"><p>ساز جنون‌کن و قدحی در ترنگ زن</p></div></div>