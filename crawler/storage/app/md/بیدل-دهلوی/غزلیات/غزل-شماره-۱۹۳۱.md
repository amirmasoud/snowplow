---
title: >-
    غزل شمارهٔ ۱۹۳۱
---
# غزل شمارهٔ ۱۹۳۱

<div class="b" id="bn1"><div class="m1"><p>محو جنون ساکنم شور بیابان در بغل</p></div>
<div class="m2"><p>چون چشم خوبان خفته‌ام ناز غزالان در بغل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی غنچه دیدم نی چمن نی شمع خواندم نی لگن</p></div>
<div class="m2"><p>گل‌ کرده‌ام زین انجمن دل نام حرمان در بغل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمریست از آسودگی پا در رکاب وحشتم</p></div>
<div class="m2"><p>چون شمع دارم در وطن شام غریبان در بغل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلقست زین گرد هوس یعنی ز افسون نفس</p></div>
<div class="m2"><p>شور قیامت در قفس‌ آشوب توفان در بغل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تنها نه‌ خلق بیخرد بر حرص‌ محمل ‌می‌کشد</p></div>
<div class="m2"><p>خورشید هم تک می‌زند زر درکمر نان در بغل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دارد گدا از غفلتت بر خود نظر واکردنی</p></div>
<div class="m2"><p>ای سنگ تاکی داشتن آیینه پنهان در بغل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از بسکه با خاک درت می‌جوشد آب زندگی</p></div>
<div class="m2"><p>دارد نسیم از طوف او همچون نفس جان در بغل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از خار خار جلوه‌ات در عرض حیرت خاک شد</p></div>
<div class="m2"><p>چون جوهر آیینه چندین چشم مژگان در بغل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مشکل دماغ یوسفت پیمانهٔ شرکت‌ کشد</p></div>
<div class="m2"><p>گیرد زلیخایش به بر یا پیر کنعان در بغل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این درد صاف‌ کفر و دین محو است در دیر یقین</p></div>
<div class="m2"><p>بی‌رنگ صهبا شیشه‌ای دارند مستان در بغل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل به این علم و فنون تاکی به بازار جنون</p></div>
<div class="m2"><p>خواهی دویدن هر طرف اجناس ارزان در بغل</p></div></div>