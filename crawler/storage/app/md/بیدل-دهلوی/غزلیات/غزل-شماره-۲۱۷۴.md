---
title: >-
    غزل شمارهٔ ۲۱۷۴
---
# غزل شمارهٔ ۲۱۷۴

<div class="b" id="bn1"><div class="m1"><p>ز دشت بیخودی می‌آیم از وضع ادب دورم</p></div>
<div class="m2"><p>جنونی‌ گر کنم ای شهریان هوش معذورم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز قدر عاجزیها غافلم لیک اینقدر دانم</p></div>
<div class="m2"><p>که تا دست سلیمان می‌رسد نقش پی مورم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان در عالم بیگانگی شد آشنای من</p></div>
<div class="m2"><p>سراب‌ آیینه‌ام گل‌ می‌کند نزدیکی از دورم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همان بهترکه خاکستر شوم در پردهٔ عبرت</p></div>
<div class="m2"><p>نقاب از روی‌ کارم بر نداری خون منصورم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برو زاهد برای خویش هر کس مطلبی دارد</p></div>
<div class="m2"><p>تو محو و من تغافل اشتیاق‌ جنت و حورم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به اقبال تپیدن نازها دارد غبار من</p></div>
<div class="m2"><p>کلاه آرای عجزم بر شکست خویش معذورم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سجودی بست بار هستی آخر بر جبین من</p></div>
<div class="m2"><p>چه‌سان سر تابم از حکم خمیدن دوش مزدورم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر صدق طلب دست ز پا افتادگان‌ گیرد</p></div>
<div class="m2"><p>به‌ مستی می‌رساند لغزش مژگان مخمورم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به خون پیچیده می‌بالم نفس دزدیده می‌نالم</p></div>
<div class="m2"><p>دمیدنهای تبخالم چکیدنهای ناسورم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مکش ای ناله دامانم مدر ای غم‌ گریبانم</p></div>
<div class="m2"><p>سرشکی محو مژگانم چکیدن نیست مقدورم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خلل تعمیر سیلاب حوادث نیستم بیدل</p></div>
<div class="m2"><p>بنای حسرتی در عالم امید معمورم</p></div></div>