---
title: >-
    غزل شمارهٔ ۱۸۵۱
---
# غزل شمارهٔ ۱۸۵۱

<div class="b" id="bn1"><div class="m1"><p>گشتم از بی‌دست و پاییها به خشک و تر محیط</p></div>
<div class="m2"><p>کشتی از تسلیم پیدا کرد ساحل در محیط</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قاصدان شوق یکسر ناخدایی می‌کنند</p></div>
<div class="m2"><p>موجها دارد ز چشمم تا در دلبر محیط</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل به هر اندیشه فال انقلابی می‌زند</p></div>
<div class="m2"><p>می‌کند از هر نسیمی نسخهٔ ابتر محیط</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر چنین افسردگی جوشد زطبع روزگار</p></div>
<div class="m2"><p>رفته رفته می‌خزد در دیدهٔ‌ گوهر محیط</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شوخی برگ نگه در دیدهٔ آیینه نیست</p></div>
<div class="m2"><p>همچو گوهر موج ما را گشت چشم‌ تر محیط</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طبع چون ممتاز اعیان شد وطن هم غربتست</p></div>
<div class="m2"><p>می‌کند حاصل ‌گهر گرد یتیمی در محیط</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر قدر ساز تعلق بیش‌، وحشت بیشتر</p></div>
<div class="m2"><p>می‌گشاید در خور امواج بال و پر محیط</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شفقت حال ضعیفان بر بزرگان ننگ نیست</p></div>
<div class="m2"><p>خار و خس را همچو گل جا می‌هد بر سر محیط</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون به عزلت خو گرفتی فکر آزادی خطاست</p></div>
<div class="m2"><p>آب‌ گوهر گشته نتواند شدن دیگر محیط</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چشم حیران مرا آیینه‌ای فهمیده است</p></div>
<div class="m2"><p>در طلسم‌ گوهر من نیست بی‌لنگر محیط</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>محرم او کیست‌،‌ گرد خویش می‌گردیده باش</p></div>
<div class="m2"><p>حلقه‌ای دارد ز گردابت برون در محیط</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دستگاه مستی ارباب معنی باده نیست</p></div>
<div class="m2"><p>بیدل از چشم تر خود می‌کشد ساغر محیط</p></div></div>