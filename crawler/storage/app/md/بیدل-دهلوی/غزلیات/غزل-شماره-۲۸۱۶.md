---
title: >-
    غزل شمارهٔ ۲۸۱۶
---
# غزل شمارهٔ ۲۸۱۶

<div class="b" id="bn1"><div class="m1"><p>سبکساری‌ست هرگه در نظرها بیدرنگ آیی</p></div>
<div class="m2"><p>به این جرات مبادا چون شرر مینا به‌ سنگ آیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به انداز تغافل نیم رخ هم عالمی دارد</p></div>
<div class="m2"><p>چرا مستقبل مردم چو تصویر فرنگ آیی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز ما و من جهانی شیشه زد بر سنگ نومیدی</p></div>
<div class="m2"><p>در قلقل مزن چندان که در پای ترنگ آیی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه‌ گر جبن باشد از طریق صلح‌ کل مگذر</p></div>
<div class="m2"><p>چو غیرت تا کجا با هر که پیش آیی به جنگ آیی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حیا سامانی این ‌مقدار رسوایی نمی‌خواهد</p></div>
<div class="m2"><p>که چون فواره هر چند آب‌گردی درشلنگ آیی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خمار، آفت‌کشیها دارد از ساغرکشی بگذر</p></div>
<div class="m2"><p>که می‌اندیشم از خمیازه در کام نهنگ آیی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بساط لاف چندین انفعالی درکمین دارد</p></div>
<div class="m2"><p>حذر زان وسعت دامن ‌که زیر پای لنگ آیی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی با برق بی ‌زنهار فرصت برنمی‌آید</p></div>
<div class="m2"><p>به افسون نفس تا چند در باد تفنگ آیی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سخن دردسر است اما متن بر خامشی چندان</p></div>
<div class="m2"><p>که چون آیینه از ضبط نفس در زیر زنگ آیی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درآن محفل به ظرف وهم ‌وظن ‌کم می‌رسد فطرت</p></div>
<div class="m2"><p>مگر گردون شوی تا قابل یک‌ کاسه بنگ آیی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همین در کسوت وهم است سیر باغ امکانت</p></div>
<div class="m2"><p>بپوش از هر دو عالم چشم اگر زین جامه تنگ آیی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به سامانست بیدل عشرتت در خورد همواری</p></div>
<div class="m2"><p>به سیر این چمن باید روی آیی‌ که رنگ آیی</p></div></div>