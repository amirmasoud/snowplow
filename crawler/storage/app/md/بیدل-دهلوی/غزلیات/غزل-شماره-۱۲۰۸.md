---
title: >-
    غزل شمارهٔ ۱۲۰۸
---
# غزل شمارهٔ ۱۲۰۸

<div class="b" id="bn1"><div class="m1"><p>تغافل‌چه‌خجلت‌به‌خود چیده‌باشد</p></div>
<div class="m2"><p>که آن نازنین سوی ما دیده باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حنابی‌ست رنگ بهار سرشکم</p></div>
<div class="m2"><p>بدانم به پای که غلتیده باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طرب مفت دل‌گرهمه صبح شبنم</p></div>
<div class="m2"><p>زگل کردن گریه خندیده باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به اظهار هستی مشو داغ خجلت</p></div>
<div class="m2"><p>همان به‌ که این عیب پوشیده باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندانم دل از درس موهوم هستی</p></div>
<div class="m2"><p>چه فهمیده باشدکه فهمیده باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو موج گهر به‌ که از شرم دریا</p></div>
<div class="m2"><p>نگاه تو در دیده پیچیده باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بجوشد دل گرم با جسم خاکی</p></div>
<div class="m2"><p>اگر باده با شیشه جوشیده باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من و یأس مطلب‌، دل و آه حسرت</p></div>
<div class="m2"><p>دعا گو اثر می‌پرستیده باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نفس‌ساز‌ی آهنگ جمعیتت‌کو</p></div>
<div class="m2"><p>سحر گرد اجزای پاشیده باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درین ‌دشت وحشت من آن‌ گردبادم</p></div>
<div class="m2"><p>که سر تا قدم دامن چیده باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حیاپرور آستان نیازت</p></div>
<div class="m2"><p>دلی داشتم آب گردیده باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر بیدل ما دهد عرض هستی</p></div>
<div class="m2"><p>به خواب عدم حیرتی دیده باشد</p></div></div>