---
title: >-
    غزل شمارهٔ ۱۳۶۷
---
# غزل شمارهٔ ۱۳۶۷

<div class="b" id="bn1"><div class="m1"><p>هرجا تپش شمع درین خانه نهفتند</p></div>
<div class="m2"><p>ناموس پر افشانی پروانه نهفتند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آشفتگیی داشت خم طرهٔ لیلی</p></div>
<div class="m2"><p>در پیچش موی سر دیوانه نهفتند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همواری از اندیشهٔ اضداد بهم خورد</p></div>
<div class="m2"><p>چون اره دم تیغ به دندانه نهفتند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سلسلهٔ خط خبر نقطه مپرسید</p></div>
<div class="m2"><p>تا ریشه قدم زد به جنون دانه نهفتند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد هستی بی‌ پرده حجاب عدم ما</p></div>
<div class="m2"><p>در گنج عیان صورت ویرانه نهفتند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در چاک گریبان نفس معنی رازیست</p></div>
<div class="m2"><p>باریکی آن مو به همین شانه نهفتند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نا محرم دل ماند جهانی چه توان‌ کرد</p></div>
<div class="m2"><p>هر چند که بود آینه در خانه نهفتند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی‌ سیر خط جام محال است توان یافت</p></div>
<div class="m2"><p>آن جاده ‌که در لغزش مستانه نهفتند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در پردهٔ آن خواب که چشم همه پوشید</p></div>
<div class="m2"><p>کس نیست بفهمد که چه افسانه نهفتند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کار همه با مبتذل یکدگر افتاد</p></div>
<div class="m2"><p>فریاد که آن معنی بیگانه نهفتند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حسرت به دل از مطلب نایاب جنون‌ کرد</p></div>
<div class="m2"><p>خمیازه عنان‌ گشت چو پیمانه نهفتند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل به تقاضای تعین چه توان‌کرد</p></div>
<div class="m2"><p>پوشیدگیی بود که در ما نه نهفتند</p></div></div>