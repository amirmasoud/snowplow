---
title: >-
    غزل شمارهٔ ۲۷۶۱
---
# غزل شمارهٔ ۲۷۶۱

<div class="b" id="bn1"><div class="m1"><p>به دل دارم چو شمع از شعله‌های آه سامانی</p></div>
<div class="m2"><p>مرتب کرده‌ام از مصرع برجسته دیوانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خراش تازه‌ای در طالع نظاره می‌بینم</p></div>
<div class="m2"><p>درین گلشن ز شوخی هر سر خاریست مژگانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به داغ حسرتم تا چند سوزد شمع این محفل</p></div>
<div class="m2"><p>تو آتش زن به من تا من هم آرایم شبستانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز وصلت انبساط دل هوس کردم ندانستم</p></div>
<div class="m2"><p>که گردد این گره از باز گشتن چشم حیرانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو صبح از وحشت هستی ندارم آنقدر فرصت</p></div>
<div class="m2"><p>که گرد اضطراب من زند دستی به دامانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندارد سعی تشویش آنقدر آشفتگیهایم</p></div>
<div class="m2"><p>نگه بیخانمان می‌گردد از تحریک مژگانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز خود گر بگذری دیگر ره و منزل نمی‌ماند</p></div>
<div class="m2"><p>صدا بر شش جهت می‌پیچد از گام پریشانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تماشا فرش راه توست از آزادگی بگذر</p></div>
<div class="m2"><p>گشاد بال چون طاووس دارد نرگسستانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز خود بینیت عیب دیگران بی‌پردگی دارد</p></div>
<div class="m2"><p>اگر پوشیده گردد چشمت از خود نیست عریانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز سامان تأمل نیست خالی سیر تحقیقت</p></div>
<div class="m2"><p>به خود چون شمع هر جا وارسی دارد گریبانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فضای عشرتی‌ کو وادی خونریز امکان را</p></div>
<div class="m2"><p>زمین تا آسمان خفته‌ست در زخم نمایانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به افسون نفس روشن نگردید آتش مهرت</p></div>
<div class="m2"><p>به هستی چون سحر می‌بایدم افشاند دامانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دو همجنسی که با هم متفق یابی به عالم کو</p></div>
<div class="m2"><p>ز مژگان هم مگر در خواب بینی ربط جسمانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ازین ‌گلشن جنون حیرتی ‌گل کرده‌ام بیدل</p></div>
<div class="m2"><p>نهان چون بوی گل در رشتهٔ چاک گریبانی</p></div></div>