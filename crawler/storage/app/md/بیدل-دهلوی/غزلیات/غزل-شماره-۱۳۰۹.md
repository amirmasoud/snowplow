---
title: >-
    غزل شمارهٔ ۱۳۰۹
---
# غزل شمارهٔ ۱۳۰۹

<div class="b" id="bn1"><div class="m1"><p>گر نالهٔ من پرتو اندیشه دواند</p></div>
<div class="m2"><p>توفان قیامت به فلک ریشه دواند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوق تو به سامان خراش دل عشاق</p></div>
<div class="m2"><p>ناخن چه خیال است مگر تیشه دواند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دور از مژه اشک است و همان بی‌سر و پایی</p></div>
<div class="m2"><p>غربت همه‌ کس را به چنین بیشه دواند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شوری‌ست در این بزم‌ کز افسون شکستن</p></div>
<div class="m2"><p>چندان که پری بال کشد شیشه دواند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صد کوچه خیال‌ست غبار نفس اینجا</p></div>
<div class="m2"><p>تا سیر گریبان به چه اندیشه دواند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مجنون تو راگر همه تن‌بند خموشی‌ست</p></div>
<div class="m2"><p>چون نی هوس ناله به صد بیشه دواند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وقت است‌که چون غنچه به افسون خموشی</p></div>
<div class="m2"><p>در نالهٔ بلبل نفسم ریشه دواند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سعی امل از قد دوتا چاره ندارد</p></div>
<div class="m2"><p>بیدل به ره‌کوهکنی تیشه دواند</p></div></div>