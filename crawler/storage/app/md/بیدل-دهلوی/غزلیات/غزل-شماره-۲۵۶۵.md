---
title: >-
    غزل شمارهٔ ۲۵۶۵
---
# غزل شمارهٔ ۲۵۶۵

<div class="b" id="bn1"><div class="m1"><p>دست جرأت دیدم آخر مغتنم در آستین</p></div>
<div class="m2"><p>همچو شمع کشته خواباندم علم در استین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با همه الفت چو موج از یکدگر پهلو تهی‌ست</p></div>
<div class="m2"><p>عالمی زین بحر جوشیده‌ست رم در استین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باطن این خلق کافر‌کیش با ظاهر مسنج</p></div>
<div class="m2"><p>جمله قرآن در کنارند و صنم در آستین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دامن‌افشان بایدت چون موج از این ‌دریا گذشت</p></div>
<div class="m2"><p>چند چون گرداب بندی پیچ و خم در آستین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شوق بیتابیم ما را رهبری در کار نیست</p></div>
<div class="m2"><p>اشک هر جا سر کشد دارد قدم در آستین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر تأمل پرده بردارد ز روی این بساط</p></div>
<div class="m2"><p>هر کف خاکی‌ست چندین جام جم در آستین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دم زدن شور قیامت خامشی حشر خیال</p></div>
<div class="m2"><p>یک نفس ساز دو عالم زیر و بم در آستین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پنجهٔ قدرت رهین باد دستیها خوش است</p></div>
<div class="m2"><p>تا به افسردن نگردد متهم در آستین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در جنون هم دستگاه‌ کلفت ما کم نشد</p></div>
<div class="m2"><p>ناله عریان است و دارد صد الم در آستین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دعوی ‌کاذب ‌گواه از خویش پیدا می‌کند</p></div>
<div class="m2"><p>چون زبان شد هرزه گو دارد قسم در آستین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سرکشی در تنگدستیها مدارا می‌شود</p></div>
<div class="m2"><p>سودن‌ست انگشتها را سر بهم در آستین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بسکه بیدل عام شد افلاس در ایام ما</p></div>
<div class="m2"><p>نقش ناخن هم نمی‌بندد درم در آستین</p></div></div>