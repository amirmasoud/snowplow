---
title: >-
    غزل شمارهٔ ۲۰۰۷
---
# غزل شمارهٔ ۲۰۰۷

<div class="b" id="bn1"><div class="m1"><p>ندارد آنقدر قطع از جهان غفلت اسبابم</p></div>
<div class="m2"><p>به جنبش تا رسد مژگان محرف می‌خورد خوابم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نفس در دل‌ گره دارم نگه در دیده معذورم</p></div>
<div class="m2"><p>خطی از نقطه بیرون نیست در دیوان آدابم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر ترک طلب گیرد درین ره دست من ورنه</p></div>
<div class="m2"><p>چو آتش دور می‌افتم ز خود چندانکه بشتابم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خزان پیش از دمیدن بود منظور بهار من</p></div>
<div class="m2"><p>کتان در پنبگی می‌داد عرض سیر مهتابم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به امید قد خم گشته محمل می‌کشد فرصت</p></div>
<div class="m2"><p>مگر پیری ازین دریا برون آرد به قلابم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به فکر خود فتادم معبد تحقیق پیدا شد</p></div>
<div class="m2"><p>خم سیر گریبان رفت و پیش آورد محرابم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو آتش‌ گرمی پهلو ندیدم جز به خاکستر</p></div>
<div class="m2"><p>درین دیر هوس دامن زدند آخر به‌ سنجابم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به ‌سعی بیخودی هم از عرق بیرون نمی‌آیم</p></div>
<div class="m2"><p>زطبع منفعل تاگردش رنگست گردابم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خدا از انفعال می‌کشیهایم نگهدارد</p></div>
<div class="m2"><p>مزاج شرم مینایم‌، در آتش خفته است آبم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من بیدل نبودم اینقدر پروانهٔ جرأت</p></div>
<div class="m2"><p>دم تیغ تو دیدم ذوق‌ کشتن‌ کرد سیمابم</p></div></div>