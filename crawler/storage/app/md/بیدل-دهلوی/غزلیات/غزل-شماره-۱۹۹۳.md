---
title: >-
    غزل شمارهٔ ۱۹۹۳
---
# غزل شمارهٔ ۱۹۹۳

<div class="b" id="bn1"><div class="m1"><p>عمری‌ست چون نفس به تپیدن فسانه‌ام</p></div>
<div class="m2"><p>از عافیت مپرس دل است آشیانه‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در قلزمی که اوج و حضیضش تحیر است</p></div>
<div class="m2"><p>موج خیالم و به خیالی روانه‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آهم چو دود آتش یاقوت گل نکرد</p></div>
<div class="m2"><p>وا سوخته‌ست در گره دل زبانه‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خط غبار آفت نظاره است و بس</p></div>
<div class="m2"><p>بی‌صرفه نیست این که شناسد زمانه‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیش حسد به وضع ملایم چه می‌کند</p></div>
<div class="m2"><p>چون موم آرمیده به زنبور خانه‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای چرخ بیش ازین اثر زحمتم مخواه</p></div>
<div class="m2"><p>چون دل بس است تیر نفس را نشانه‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اشکی به صد گداز جگر جمع می‌کنم</p></div>
<div class="m2"><p>چون شمع زندگی‌ست به این آب و دانه‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خجلت به عرض جوهر من خنده می‌کند</p></div>
<div class="m2"><p>مویی ز چشم رستهٔ مغرور شانه‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن شور طالعم که در این بزم خواب عیش</p></div>
<div class="m2"><p>در چشم عالمی نمک است از فسانه‌ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی‌اختیار می‌روم از خویش و چاره نیست</p></div>
<div class="m2"><p>تا کی کشد عنان نفس از تازیانه‌ام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خاکم به باد رفت و نرفت از جبین شوق</p></div>
<div class="m2"><p>یک‌سجده وار حسرت آن آستانه‌ام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آسوده‌تر ز آب گهر خاک می‌شوم</p></div>
<div class="m2"><p>پرواز در کنار فسردن بهانه‌ام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>موج فضول‌، محرم وصل محیط نیست</p></div>
<div class="m2"><p>بی‌طاقتی مباد زند بر کرانه‌ام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیدل اسیر حسرت از آنم‌که همچو چشم</p></div>
<div class="m2"><p>در رهگذار سیل فتاده‌ست خانه‌ام</p></div></div>