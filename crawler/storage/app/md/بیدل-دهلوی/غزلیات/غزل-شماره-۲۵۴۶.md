---
title: >-
    غزل شمارهٔ ۲۵۴۶
---
# غزل شمارهٔ ۲۵۴۶

<div class="b" id="bn1"><div class="m1"><p>شمع صفت دیدنی‌ست عجز جنون زای من</p></div>
<div class="m2"><p>سر به هوا می‌دود آبلهٔ پای من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بال فشان می‌روم لیک ندانم کجا</p></div>
<div class="m2"><p>بر پر من بسته‌اند نامهٔ عنقای من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسکه به رویم عرق آینهٔ شرم بست</p></div>
<div class="m2"><p>ماند نهان از نظر صورت پیدای من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همقدم‌گرد باد تاختم از بیخودی</p></div>
<div class="m2"><p>گردش ساغر شکست گردن مینای من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خجلت اعمال پوچ نامه به فردا فکند</p></div>
<div class="m2"><p>روی ورق پشت‌ کرد مشق چلیپای من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا ز نم انفعال صورتی آرم به‌عرض</p></div>
<div class="m2"><p>دام نکرد از حباب آینه دریای من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با همه آزادگی منفعل هستی‌ام</p></div>
<div class="m2"><p>حیف‌ که چین‌وار نیست دامن صحرای من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غیر فسوس از نفس یک سخنم‌ گل نکرد</p></div>
<div class="m2"><p>هر چه شنیدم زدل بود همین وای من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ضعف به صد دشت و در می‌کشدم سایه‌وار</p></div>
<div class="m2"><p>تا به‌ کجایم برد لغزش بی پای من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چند نفس خون کنم تا به‌ خود افسون‌ کنم</p></div>
<div class="m2"><p>سوختم و وا نشد در دل من جای من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خواه ادب پروریم خواه گریبان دریم</p></div>
<div class="m2"><p>غیردرین خیمه نیست جز من و لیلای من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>داغ شو ای عاجزی نوحه‌ کن ای بیکسی</p></div>
<div class="m2"><p>با دو جهان شد طرف‌ بیدل تنهای من</p></div></div>