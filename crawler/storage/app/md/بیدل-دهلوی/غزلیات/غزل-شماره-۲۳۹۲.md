---
title: >-
    غزل شمارهٔ ۲۳۹۲
---
# غزل شمارهٔ ۲۳۹۲

<div class="b" id="bn1"><div class="m1"><p>آخر از بار تعلق‌های اسباب جهان</p></div>
<div class="m2"><p>عبرتی بستیم بر دوش نگاه ناتوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خم‌ گردون مهیا شو به ایمای بلا</p></div>
<div class="m2"><p>تیر می‌باشد اشارتهای ابروی کمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از تأمل چند باید آبروی شوق ریخت</p></div>
<div class="m2"><p>خامشی تا کی‌ گره در رشتهٔ ساز فغان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زحمت بسیار دارد از عدم گل ‌کردنت</p></div>
<div class="m2"><p>نقب در خارا زنی ‌کز نام خود یابی نشان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چنین حیرت عنان جستجوها می‌کشد</p></div>
<div class="m2"><p>جوهر آیینه می‌گردد غبار کاروان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر فروغ دل هوس داری خموشی ساز کن</p></div>
<div class="m2"><p>می‌شود این شمع را افشاندن دامن زبان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از سواد چشم پی بر معنی دل برده‌‌ام</p></div>
<div class="m2"><p>در همین خاک سیه آیینه‌ای دارم‌ گمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عرض جوهر در غبار خجلتم پوشیده است</p></div>
<div class="m2"><p>این زمانه آیینه‌ام چشمی است در مژگان نهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همچو آن طفلی ‌که بستانش‌ کند خمیازه سنج</p></div>
<div class="m2"><p>زخم دل از شوق پیکانت نمی‌بندد دهان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شب به وصل طره‌ات فکر مسلسل داشتیم</p></div>
<div class="m2"><p>یک سخن چون شانه‌ام نگذشت جز مو بر زبان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مشت خاک‌ِ من نیاز سجدهٔ تسلیم اوست</p></div>
<div class="m2"><p>آب اگر گردم زکوی او نمی‌گردم روان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رفت بیدل عمرها چون رنگ بر باد امید</p></div>
<div class="m2"><p>غنچه واری هم در این ‌گلشن نبستم آشیان</p></div></div>