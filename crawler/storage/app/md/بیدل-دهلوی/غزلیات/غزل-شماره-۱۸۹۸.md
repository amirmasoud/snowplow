---
title: >-
    غزل شمارهٔ ۱۸۹۸
---
# غزل شمارهٔ ۱۸۹۸

<div class="b" id="bn1"><div class="m1"><p>نشد از حسرت داغت جگرم تنها خشک</p></div>
<div class="m2"><p>لاله را نیز دماغیست درین سودا خشک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منت چشمهٔ خضر آینه‌پردازی‌تریست</p></div>
<div class="m2"><p>دم شمشیرتو یارب نشود با ما خشک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برق حسن تو در ابروی اشارت دارد</p></div>
<div class="m2"><p>خم موجی‌که‌کند خون دل دریا خشک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در تماشاکدهٔ جلوه که چشمش مرساد</p></div>
<div class="m2"><p>موج آیینه زند هرکه شود برجا خشک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون حیا آب رخ‌ گوهر ما وقف‌تریست</p></div>
<div class="m2"><p>عرقی چند مبادا شود از سیما خشک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زین بضاعت نتوان دیگ فضولی پختن</p></div>
<div class="m2"><p>تا رسد نان به تری می‌شود آب ما خشک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وقت آن شدکه ز بی آبی ابر احسان</p></div>
<div class="m2"><p>برگ گل روید ازین باغ چو نقش پا خشک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسکه افسردگی افسون تحیر دارد</p></div>
<div class="m2"><p>سیل چون جاده فتاده است درین صحرا خشک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ترک اسباب لب شکوهٔ نایابی دوخت</p></div>
<div class="m2"><p>کرد افشاندن این گرد جراحتها خشک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ماند از حیرت رفتار بلاانگیزت</p></div>
<div class="m2"><p>ناله در سینهٔ بیدل چو رگ خارا خشک</p></div></div>