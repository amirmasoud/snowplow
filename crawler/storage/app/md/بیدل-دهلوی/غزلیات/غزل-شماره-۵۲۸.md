---
title: >-
    غزل شمارهٔ ۵۲۸
---
# غزل شمارهٔ ۵۲۸

<div class="b" id="bn1"><div class="m1"><p>اگر می نیست جمعیت‌کدام است</p></div>
<div class="m2"><p>کمند وحدت اینجا دور جام است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو ساغر در محیط می‌کشیها</p></div>
<div class="m2"><p>ز موج باده قلابم به‌کام است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو عالم در نمک خفت از غبارم</p></div>
<div class="m2"><p>هنوزم شور مستی ناتمام است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر بی‌دستگاهم غم ندارم</p></div>
<div class="m2"><p>چو هندویم سیه‌بختی غلام است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بال‌افشانی‌ام قطع نظرکن</p></div>
<div class="m2"><p>که صید من نگاه چشم دام است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من و میخانهٔ دیدارکانجا</p></div>
<div class="m2"><p>مژه تا بازگردد خط جام است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل از هستی نمی‌چیند فروغی</p></div>
<div class="m2"><p>نفس درکشور آیینه شام است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهان زندان نومیدی‌ست اما</p></div>
<div class="m2"><p>دمی‌کز خود برآیی سیر بام است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درتن محفل به حکم شرع‌تسلیم</p></div>
<div class="m2"><p>نفس گر می‌کشی‌چون‌می حرام است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به طبع اهل دنیا پختگی نیست</p></div>
<div class="m2"><p>ثمر چندان‌که‌سرسبز است‌خام است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اسیری شهپر آزادی ماست</p></div>
<div class="m2"><p>نگین دام ما را صید نام است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز هستی تا عدم جهدی ندارد</p></div>
<div class="m2"><p>ز مژگان تا به مژگان نیم‌گام است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به غفلت آنقدر دوریم از دوست</p></div>
<div class="m2"><p>که تا وصلش رسد اینجا پیام است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زبیدل‌جرأت جولان مجوبید</p></div>
<div class="m2"><p>چو موج این ناتوان پهلو خرام است</p></div></div>