---
title: >-
    غزل شمارهٔ ۳۷۵
---
# غزل شمارهٔ ۳۷۵

<div class="b" id="bn1"><div class="m1"><p>فال تسلیم زن و شوکت شاهی دریاب</p></div>
<div class="m2"><p>گردنی خم کن و معراج‌کلاهی دریاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دام تسخیر دو عالم نفس نومیدی‌ست</p></div>
<div class="m2"><p>ای ندامت‌زده سررشتهٔ آهی دریاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرصت صحبت‌گل پا به رکاب رنگ است</p></div>
<div class="m2"><p>آرزو چند اگر هست نگاهی دریاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از شبیخون خط یار نگردی غافل</p></div>
<div class="m2"><p>هرکجا شوخی گردی‌ست سپاهی دریاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دود پیچیدهٔ دل گرد سراغی دارد</p></div>
<div class="m2"><p>از سویدا اثر چشم سیاهی دریاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تاکی ای پای طلب زحمت جولان دادن</p></div>
<div class="m2"><p>طوف آسودگی آبله‌گاهی دریاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یوسفی‌کن‌گرت اسباب مسیحایی نیست</p></div>
<div class="m2"><p>به فلک‌گر نرسیدی بن چاهی دریاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نامرادی صدف گوهر اقبال رساست</p></div>
<div class="m2"><p>غوطه در جیب‌گدایی زن و شاهی دریاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سعل بنیاد دو عالم شدی ای آتش عشق</p></div>
<div class="m2"><p>ماگیاهیم ز ما هم پرکاهی دریاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه وجود وچه عدم بست وگشاد مژه است</p></div>
<div class="m2"><p>چون شرر هر دو جهان را به نگاهی دریاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خلوت عافیت شمع‌گداز است اینجا</p></div>
<div class="m2"><p>پی خاکستر خودگیر و پناهی دریاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دامن دیده به هر سرمه میالا بیدل</p></div>
<div class="m2"><p>انتظاری شو وگرد سر راهی دریاب</p></div></div>