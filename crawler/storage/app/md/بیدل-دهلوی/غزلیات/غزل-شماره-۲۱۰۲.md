---
title: >-
    غزل شمارهٔ ۲۱۰۲
---
# غزل شمارهٔ ۲۱۰۲

<div class="b" id="bn1"><div class="m1"><p>سحر ز شرم رخت مطلعی به تاب رساندم</p></div>
<div class="m2"><p>زمین خانهٔ خورشید را به آب رساندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به یک قدح به در آوردم از هزار حجابش</p></div>
<div class="m2"><p>تبسم سحری‌ گفتم آفتاب رساندم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رهی به نقطهٔ موهوم بردم از خط هستی</p></div>
<div class="m2"><p>جریده‌ای که ندارم به انتخاب رساندم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تلاش راحتم این بس که با کمال ضعیفی</p></div>
<div class="m2"><p>چو شمع‌ یک مژه تا نقش پا به خواب رساندم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیام ملک یقینم نداشت قاصد دیگر</p></div>
<div class="m2"><p>چو عکس از آینه برگشتم و جواب رساندم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به یک حدیث‌ که خواندم ز شبهه‌زار تعین</p></div>
<div class="m2"><p>به گوش هر دو جهان آیه ی عذاب رساندم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صفای جوهر معنی نداشت غیر ندامت</p></div>
<div class="m2"><p>مرا نشاند در آتش به هر مآب رساندم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو شمع‌ آن سوی خاکسترم نبود تسلی</p></div>
<div class="m2"><p>دماغ سوخته آخر به ماهتاب رساندم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به سعی فطرت معذور بیش اپن چه‌ گشاید</p></div>
<div class="m2"><p>نگاهی از مژهٔ بسته تا نقاب رساندم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شب چراغ خموش انتظار صبح ندارد</p></div>
<div class="m2"><p>دعای خود به دعاهای مستجاب رساندم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به عشق نسبت عجزم درست کرد تخیل</p></div>
<div class="m2"><p>سری نداشتم اما به آن رکاب رساندم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خطی ز مشق یقین‌ گل نکرد از من بیدل</p></div>
<div class="m2"><p>چو حرف شبهه‌، خراشی به هر کتاب رساندم</p></div></div>