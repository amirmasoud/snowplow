---
title: >-
    غزل شمارهٔ ۱۱۳۹
---
# غزل شمارهٔ ۱۱۳۹

<div class="b" id="bn1"><div class="m1"><p>چه غفلت یارب از تقریر یأس انجام می‌خیزد</p></div>
<div class="m2"><p>که دل تا وصل می‌گوید ز لب پیغام می‌خیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیال چشم او داری طمع بگسل ز هشیاری</p></div>
<div class="m2"><p>که اینجا صد جنون از روغن بادام می‌خیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چسان بیتابی عاشق نگیرد دامن حیرت</p></div>
<div class="m2"><p>که از طرز خرامش ‌گردش ایام می‌خیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز جوش خون دل بر حلقهٔ آن زلف می‌لرزم</p></div>
<div class="m2"><p>که توفان شفق آخر ز قعر شام می‌خیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بزم می‌پرستان بی‌توقف بگذر ای زاهد</p></div>
<div class="m2"><p>که آنجا هرکه بنشبند ز ننگ و نام می‌خیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کرم درکار تست ای بی‌خبر ترک فضولی‌کن</p></div>
<div class="m2"><p>که از دست دعا برداشتن ابرام می‌خیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه اشک اینجا زمین‌فرساست نی آهی هوا پیما</p></div>
<div class="m2"><p>غبار بی‌عصاییها به این اندام می‌خیزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سخن در پرده خون‌سازی به است از عرض اظهارش</p></div>
<div class="m2"><p>که از تحسین این بی‌دانشان دشنام می‌خیزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جنون آهنگ صید کیست یارب مست بیتابی</p></div>
<div class="m2"><p>که چون زنجیر، شور از حلقه‌های دام می‌خیزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عروج عشرت است امشب ز جوش خم مشو غافل</p></div>
<div class="m2"><p>که صحن خانهٔ مستان به سیر بام می‌خیزد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نفس سرمایه‌ای بیدل ز سودای هوس بگذر</p></div>
<div class="m2"><p>سحر هم از سر این خاکدان ناکام می‌خیزد</p></div></div>