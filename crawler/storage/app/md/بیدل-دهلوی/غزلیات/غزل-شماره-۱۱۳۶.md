---
title: >-
    غزل شمارهٔ ۱۱۳۶
---
# غزل شمارهٔ ۱۱۳۶

<div class="b" id="bn1"><div class="m1"><p>به این عجزم چه ز خاک حیاپرورد برخیزد</p></div>
<div class="m2"><p>مگر مشتی عرق از من به‌جای ‌گرد برخیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگو سهل‌است عاشق را به نومیدی علم‌گشتن</p></div>
<div class="m2"><p>چها زپا نشیند تا یک آه سرد برخیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به‌مقصد برد شور یک‌جرس صد کاروان محمل</p></div>
<div class="m2"><p>مباش از ناله غافل گر همه بی‌ درد بر خیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خیال آوارهٔ دشت هوای اوست اجزایم</p></div>
<div class="m2"><p>مبادا حسرتی زین خاک بادآورد برخیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آن وادی‌ که دامان تصرف بشکند رنگم</p></div>
<div class="m2"><p>چو اوراق خزان نقش قدم هم زرد برخیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازین دام تعلق بسکه دشوار است وارستن</p></div>
<div class="m2"><p>تحیر نقش بندد گر نگاهی فرد برخیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر این است نیرنگ اثر زخم محبت را</p></div>
<div class="m2"><p>نفس از سینه چون صبحم قفس‌پرورد برخیزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بقدر اعتبار آیینه دارد جوهر هرکس</p></div>
<div class="m2"><p>ز جرات‌ گیر اگر مو بر تن نامرد برخیزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز املاک هوس‌، دل نام کلفت مزرعی دارم</p></div>
<div class="m2"><p>چو زخم آنجا همه‌گر خنده‌کارم درد برخیزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز سامان جنون جوش سحر خواهم زدن بیدل</p></div>
<div class="m2"><p>گریبان می‌درم چندان که از من گرد برخیزد</p></div></div>