---
title: >-
    غزل شمارهٔ ۱۹۰۴
---
# غزل شمارهٔ ۱۹۰۴

<div class="b" id="bn1"><div class="m1"><p>رسانده‌ایم درین عرصهٔ خیال آهنگ</p></div>
<div class="m2"><p>چو شمع ناوک آهی به شوخی پر رنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز ناامیدی دلها دلت چه غم دارد</p></div>
<div class="m2"><p>شکست ساغر و میناست طبل عشرت سنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شرابخانهٔ هستی که عشق ساقی اوست</p></div>
<div class="m2"><p>بجز خیال حدوث و قدم ندارد بنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درین چمن همه با جیب خویش ساخته‌ایم</p></div>
<div class="m2"><p>کسی ندید که ‌گل دامن ‌که داشت به چنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سواد الفت این دشت عبرت‌اندوز است</p></div>
<div class="m2"><p>نگاهی آب ده از سرمه‌دان داغ پلنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آرزوی شکستی‌ که چشم بد مرساد</p></div>
<div class="m2"><p>درین ستمکده ما هم رسیده‌ایم به رنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیال اینهمه داغ غرور غفلت ماست</p></div>
<div class="m2"><p>صفا ودیعت نازبست در طبیعت رنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به قلزمی ‌که فتد سایهٔ بناگوشت</p></div>
<div class="m2"><p>گهر به رشته ‌کشد خارهای پشت نهنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه آفتی تو که نقاش فتنهٔ نگهت</p></div>
<div class="m2"><p>به رنگ رفته‌ کشد مخمل غبار فرنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو گل جز این ‌که گریبان درم علاجی نیست</p></div>
<div class="m2"><p>فشرده است به صد رنگ ‌کلفتم دل تنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هنوز شیشه نه‌ای‌، نشئه عالم دگر است</p></div>
<div class="m2"><p>تفاوت عدم و کم‌، مدان پری تا سنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به دوش برق ‌کشیدیم بار خود بیدل</p></div>
<div class="m2"><p>ز خویش رفتن ما اینقدر نداشت درنگ</p></div></div>