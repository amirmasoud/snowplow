---
title: >-
    غزل شمارهٔ ۳۵۴
---
# غزل شمارهٔ ۳۵۴

<div class="b" id="bn1"><div class="m1"><p>تاب زلفت سایه آویزد به طرف آفتاب</p></div>
<div class="m2"><p>خط مشکینت شکست آرد به حرف آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده در ادراک آغوش خیالت عاجز است</p></div>
<div class="m2"><p>ذره کی یابدکنار بحر ژرف آفتاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بینی‌ات آن مصرع عالی است کز انداز حسن</p></div>
<div class="m2"><p>دخل نازش دارد انگشتی به حرف آفتاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ظلمت ما را فروغ نور وحدت جاذب است</p></div>
<div class="m2"><p>سایه آخر می‌رود ازخود به طرف آفتاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسکه اقبال جنون ما بلند افتاده است</p></div>
<div class="m2"><p>می‌توان عریانی ماکرد صرف آفتاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در عرق اعجاز حسن او تماشا کردنی‌ست</p></div>
<div class="m2"><p>شبنم‌گل می‌چکد آنجا ز ظرف آفتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکجا با مهر رخسارتو لاف حسن زد</p></div>
<div class="m2"><p>هم زپرتو بر زمین افتاد حرف آفتاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما عدم‌سرمایگان را لاف هستی نادر است</p></div>
<div class="m2"><p>ذره حیران است در وضع شگرف آفتاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسکه در نظّارهٔ مهر جمال اوگداخت</p></div>
<div class="m2"><p>موج شبنم می‌زند امروز برف آفتاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جانفشانیهاست بیدل در تماشای رخش</p></div>
<div class="m2"><p>چون سحرکن نقد عمرخویش صرف آفتاب</p></div></div>