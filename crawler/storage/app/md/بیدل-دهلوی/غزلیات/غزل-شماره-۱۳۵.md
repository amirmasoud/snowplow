---
title: >-
    غزل شمارهٔ ۱۳۵
---
# غزل شمارهٔ ۱۳۵

<div class="b" id="bn1"><div class="m1"><p>هرچند گرانی بود اسباب جهان را</p></div>
<div class="m2"><p>تحریک زبان نیشتر است این رگ جان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیتاب جنون در غم اسباب نباشد</p></div>
<div class="m2"><p>چون نی به خمیدن نکشد ناله‌کشان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیداری من شمع صفت لاف زبانی‌ست</p></div>
<div class="m2"><p>دل زاد ره شوق بود ر‌یگ روان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفاق فسون انجمن شور خموشی‌ست</p></div>
<div class="m2"><p>دارم ز خموشی به‌کمین خواب‌گران را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ایمن نتوان بود ز همواری ظالم</p></div>
<div class="m2"><p>حیرت لگن شمع زبان ساز دهان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنیاد کج‌اندیش شود سخت ز تهدید</p></div>
<div class="m2"><p>در راستی افزونی زخم است سنان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ممسک نشود قابل ایمان خساست</p></div>
<div class="m2"><p>از بند قوی مهره مکن پشت‌کان را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما را به غم عشق همان عشق علاج است</p></div>
<div class="m2"><p>تا نشمرد انگشت شهادت لب نان را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خط فیض بهار دگر از حسن تو دارد</p></div>
<div class="m2"><p>مهتاب بود پنبهٔ ناسورکتان را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وقت است‌کنون‌کز اثر خون شهیدان</p></div>
<div class="m2"><p>جوش رگ‌گل می‌کند این شعله دخان را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عشرت هوس رفتن رنگم چه توان‌کرد</p></div>
<div class="m2"><p>شمشیر تو یاقوت‌کند سنگ فسان را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باشدکه سراز منزل مقصود برآریم</p></div>
<div class="m2"><p>کردند بهار چمن شمع خزان را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بیدل نفست خون مکن از هرزه درایی</p></div>
<div class="m2"><p>چون جاده درین دشت فکندیم عنان را</p></div></div>