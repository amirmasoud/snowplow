---
title: >-
    غزل شمارهٔ ۲۲۸۳
---
# غزل شمارهٔ ۲۲۸۳

<div class="b" id="bn1"><div class="m1"><p>چیزی از خود هر قدم زیر قدم‌ گم می‌کنم</p></div>
<div class="m2"><p>رفته رفته هر چه دارم چون قلم‌گم می‌کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌نصیب معنی‌ام کز لفظ می‌جویم مراد</p></div>
<div class="m2"><p>دل اگر پیدا شود دیر و حرم‌گم می‌کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای هوس دود تعین بر دماغ من مپیچ</p></div>
<div class="m2"><p>زیر این پرچم چو شمع آخر علم‌ گم می‌کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تشنه‌کام حرص می‌میرد قناعت تا ابد</p></div>
<div class="m2"><p>یک عرق‌ گر از جبین شرم نم‌ گم می‌کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دعوی خضر طریقت بودنم آواره‌کرد</p></div>
<div class="m2"><p>اندکی‌ گر کم شود این راه‌کم ‌گم می‌کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا غبار وادی مجنون به یادم می‌رسد</p></div>
<div class="m2"><p>آسمان بر سر، زمین‌، زیر قدم‌ گم می‌کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رنگ و بو چیزی ندارد غیر استغنا بهار</p></div>
<div class="m2"><p>هر چه از خود گم‌ کنم با او بهم‌ گم می‌کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل نمی‌ماند به دستم طاقت دیدارکو</p></div>
<div class="m2"><p>تا تو می‌آیی به پیش آیینه هم ‌گم می‌کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عالم صورت برون از عالم تنزیه نیست</p></div>
<div class="m2"><p>در صمد دارم تماشا گر صنم گم می‌کنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قاصد ملک فراموشی‌کسی چون من مباد</p></div>
<div class="m2"><p>نامه‌ای دارم ‌که هر جا می‌برم گم می‌کنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دم مزن از جستجوی شوق بی‌پروای من</p></div>
<div class="m2"><p>هر چه می‌یابم ز هستی تا عدم‌ گم می‌کنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر رفیقان بیدل از مقصد چه‌سان آرم خبر</p></div>
<div class="m2"><p>من‌که خود را نیز تا آنجا رسم ‌گم می‌کنم</p></div></div>