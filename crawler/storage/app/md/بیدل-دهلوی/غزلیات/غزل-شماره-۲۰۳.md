---
title: >-
    غزل شمارهٔ ۲۰۳
---
# غزل شمارهٔ ۲۰۳

<div class="b" id="bn1"><div class="m1"><p>تا بوی‌گل به رنگ ندوزد لباس ما</p></div>
<div class="m2"><p>عریان‌گذشت زین چمن امید ویاس ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل داشت دستگاه دو عالم ولی چه سود</p></div>
<div class="m2"><p>با ما نساخت آینهٔ خودشناس ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاکی و سایه‌ای همه‌جا فرش کرده‌ایم</p></div>
<div class="m2"><p>در خانه‌ای که نیست همین بس پلاس ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آیینهٔ سراب خیالیم چاره نیست</p></div>
<div class="m2"><p>چسزی نموده‌اند به چشم قیاس ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یاران غنیمتیم به‌هم زین دو دم وقاق</p></div>
<div class="m2"><p>ما شخص فرصتیم بدارند پاس ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پهلو زدن ز پنبه برآتش قیامت است</p></div>
<div class="m2"><p>هرخشک مغزنیست حریف مساس ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غیرت نشان پلنگ سواد تجردیم</p></div>
<div class="m2"><p>دل هم رمیده است ز ما از هراس ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تکلیف بی‌نشانی عشق از هوس جداست</p></div>
<div class="m2"><p>یارب قبول کس نشود التماس ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از ششجهت ترانهٔ عنقا شنیدنی‌ست</p></div>
<div class="m2"><p>کز بام و منظر دگر افتاد طاس ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از شبنم سحر سبق شرم برده‌ایم</p></div>
<div class="m2"><p>هستی عرق شد از نفس ناسپاس ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آیینهٔ دلیم‌کدورت نصیب ماست</p></div>
<div class="m2"><p>کز تاب فرصت نفس است اقتباس ما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مردیم وخاک ما به هواگرد می‌کند</p></div>
<div class="m2"><p>بی‌ربطیی که داشت نرفت از حواس ما</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جز زیر پا چو آبله خشتی نچید‌ه‌ایم</p></div>
<div class="m2"><p>دیگرکدام قصر و چه طاق و اساس ما</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خال زیاد فرض‌کن و نرد وهم باز</p></div>
<div class="m2"><p>بر هیچ تخته‌ای نفتاده‌ست طاس ما</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صد سال رفت تا به قد خم رسیده‌ایم</p></div>
<div class="m2"><p>بیدل چه خوشه‌هاکه نشد نذر داس ما</p></div></div>