---
title: >-
    غزل شمارهٔ ۳۸۸
---
# غزل شمارهٔ ۳۸۸

<div class="b" id="bn1"><div class="m1"><p>از خامشی مپرس و زگفتار عندلیب</p></div>
<div class="m2"><p>صد غنچه وگل است به منقار عندلیب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارم دلی به سینه ز داغ خیال دوست</p></div>
<div class="m2"><p>طراح آشیانهٔ گلزار عندلیب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نامحرمی‌که از ادب عشق غافل است</p></div>
<div class="m2"><p>دارد اهانت گل از انکسار عندلیب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی‌یار جای یار نشان قیامت است</p></div>
<div class="m2"><p>با باغ در خزان نفتد کار عندلیب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دردسر تظلم الفت کجا برد</p></div>
<div class="m2"><p>گر زبر بال هم ندهد بار عندلیب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دورباش غیرت خوبان حذرکنید</p></div>
<div class="m2"><p>گل خارها نشانده به آزار عندلیب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آیین دلبری به چه رنگش نشان دهند</p></div>
<div class="m2"><p>شاخ‌گلی‌که نیست قفس‌وار عندلیب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بوی‌گلم برون چمن داغ می‌کند</p></div>
<div class="m2"><p>از ناله‌های در پس‌ دیوار عندلیب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من نیز بی‌هوس نی‌ام اما نداد عشق</p></div>
<div class="m2"><p>پروانه را دماغ سر وکار عندلیب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شاید نصیب دردی از اهل وفا برم</p></div>
<div class="m2"><p>بستم دل دو نیم به‌منقار عندلیب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بالین خواب‌گل همه رنگ شکسته بود</p></div>
<div class="m2"><p>آه از ندامت پر بیکار عندلیب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل بهار عشرت عشاق ناله است</p></div>
<div class="m2"><p>امسال نیز می‌گذرد پار عندلیب</p></div></div>