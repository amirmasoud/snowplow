---
title: >-
    غزل شمارهٔ ۲۳۳۴
---
# غزل شمارهٔ ۲۳۳۴

<div class="b" id="bn1"><div class="m1"><p>از زندگی به جز غم فردا نمانده‌ایم</p></div>
<div class="m2"><p>چیزی که مانده‌ایم درینجا نمانده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی دو چون حواس به وحشت سرای عمر</p></div>
<div class="m2"><p>بی‌سعی التفات و مدارا نمانده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون سایه خضر مقصد ما شوق نیستی است</p></div>
<div class="m2"><p>از پا فتاده‌ایم ولی وا نمانده‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر بر زمین فرصت هستی درین بساط</p></div>
<div class="m2"><p>زان رنگ مانده‌ایم که گویا نمانده‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زین خاکدان برون نتوان برد رخت خویش</p></div>
<div class="m2"><p>حرفیست بعد مرگ به دنیا نمانده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مجبور اختبار تعین‌ کسی مباد</p></div>
<div class="m2"><p>گوهر شدیم لیک به دریا نمانده‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرگشتگی هم از سر مجنون ما گذشت</p></div>
<div class="m2"><p>جز نام گردباد به صحرا نمانده‌ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>محو سراغ خویش برآمد غبار ما</p></div>
<div class="m2"><p>بودیم بی‌نشان ازل یا نمانده‌ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دود چراغ بود غبار بنای یأس</p></div>
<div class="m2"><p>بر سر چه افکنیم ته پا نمانده‌ایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر شرم کن حواله جواب سلام ما</p></div>
<div class="m2"><p>تا قاصدت رسد بر ما، ما نمانده‌ایم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون مهره‌ای ‌که شش درش افسون حیرت است</p></div>
<div class="m2"><p>ما هم برون شش‌در این خانه مانده‌ایم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل به فکر نقطهٔ موهوم آن دهن</p></div>
<div class="m2"><p>جزوی به غیر لایتجزا نمانده‌ایم</p></div></div>