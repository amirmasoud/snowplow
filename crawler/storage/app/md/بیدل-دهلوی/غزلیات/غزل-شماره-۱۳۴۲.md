---
title: >-
    غزل شمارهٔ ۱۳۴۲
---
# غزل شمارهٔ ۱۳۴۲

<div class="b" id="bn1"><div class="m1"><p>حاضران از دور چون محشر خروشم دیده‌اند</p></div>
<div class="m2"><p>دیده‌ها باز است لیک از راه گوشم دیده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با خم شوقم چه نسبت زاهد افسرده را</p></div>
<div class="m2"><p>میکشان هم یک دو ساغروار جوشم دیده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سابه زنگ‌کلفت آیینهٔ خورشید نیست</p></div>
<div class="m2"><p>نشئهٔ صافم چه شد گر درد نوشم دیده اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صورت پا در رکابی همچو شمع استاده‌ام</p></div>
<div class="m2"><p>رفته خواهد بود سر هم‌گر به دوشم دیده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خراباتی که حرف نرگس مخمور اوست</p></div>
<div class="m2"><p>کم جنونی نیست یاران‌ گر به هوشم دیده‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تهمت‌آلود نفس چندین گریبان می‌درد</p></div>
<div class="m2"><p>چون سحر عریانم اما خرقه‌پوشم دیده‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کنج فقرم چون شرار سنگ بزم ایمنی‌ست</p></div>
<div class="m2"><p>مصلحتها در چراغان خموشم دیده‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فرصت نازگلم پر بیدماغ رنگ و بوست</p></div>
<div class="m2"><p>خنده بر لب در دکان گلفروشم دید‌ه اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حال می‌پندارم و ماضی است استقبال من</p></div>
<div class="m2"><p>در نظر می‌آیم امروزی که دوشم دیده‌اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شبنم‌آرایی‌ست بیدل شوخی آثار صبح</p></div>
<div class="m2"><p>هرکجا گل کرده باشم شرم‌کوشم دیده‌اند</p></div></div>