---
title: >-
    غزل شمارهٔ ۱۷۹۷
---
# غزل شمارهٔ ۱۷۹۷

<div class="b" id="bn1"><div class="m1"><p>کشت عاشق‌ که دهد داد گیاه خشکش</p></div>
<div class="m2"><p>موی چینی‌ست رگ ابر سیاه خشکش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌سخا گردن منعم چه کمال افرازد</p></div>
<div class="m2"><p>سر خشکی‌ست که آتش به‌ کلاه خشکش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر به غفلت مفرازید ز آه مظلوم</p></div>
<div class="m2"><p>برق خفته‌ست به فوارهٔ آه خشکش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاه اگر دامن انعام به‌ خسّت چیند</p></div>
<div class="m2"><p>نیست جز مهرهٔ شطرنج سپاه خشکش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غفلت بیدل ما تا به‌ کجا گرد کند</p></div>
<div class="m2"><p>ابر رحمت نشود تر به ‌گناه خشکش</p></div></div>