---
title: >-
    غزل شمارهٔ ۷۱۸
---
# غزل شمارهٔ ۷۱۸

<div class="b" id="bn1"><div class="m1"><p>درین‌گلشن دو روزت خنده‌کاریست</p></div>
<div class="m2"><p>مبادا غره‌گردی گل بهاری‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برافشان بر هوس دامان‌و بگذر</p></div>
<div class="m2"><p>که در جیب نفس نقد نثاری‌ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم از بست وگشاد چشم دریاب</p></div>
<div class="m2"><p>که اجزای جهان لیل و نهاری‌ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ودیعتها ز سر باید اداکرد</p></div>
<div class="m2"><p>به ره‌گر پاگذاری حقگزاری‌ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حریف پاکبازان وفا باش</p></div>
<div class="m2"><p>که جز سر هرچه بازی بدقماری‌ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به‌صد دست حمایت بایدت سوخت</p></div>
<div class="m2"><p>چراغ زندگی یک سر چناری‌ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز خاکستر امان می‌جوید آتش</p></div>
<div class="m2"><p>چوهستی‌باکفن‌جوشد حصاری‌ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هنوزت دیده کم دارد سفیدی</p></div>
<div class="m2"><p>زمان وصل یوسف انتظاری‌ست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حذر، ای شمع از این محفل‌که اینجا</p></div>
<div class="m2"><p>بقدر سر بریدن سرشماری‌ست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من و ما نسخهٔ تحقیق هستی</p></div>
<div class="m2"><p>خطی داردکه آن لوح مزاری‌ست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جهان مجنون سودای نقاب است</p></div>
<div class="m2"><p>ازین غافل که لیلی بی‌عماری‌ست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مباشید از خواص جاه غافل</p></div>
<div class="m2"><p>بجنگید ای خروس‌آن‌، تاجداری‌ست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وقار پیری ازگردون مجویید</p></div>
<div class="m2"><p>که طفلی عاشق دامن‌سواری‌ست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چه فقر وکو غنا عام است رحمت</p></div>
<div class="m2"><p>ز خشک‌وتر مگو‌چشمه‌ساری‌ست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>غبارت چون سحرگر اوج‌گیرد</p></div>
<div class="m2"><p>فلکها پایمال خاکساری‌ست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به هستی‌بیدل‌مفلس‌چه‌لافد</p></div>
<div class="m2"><p>ز قلقل شیشهٔ بی‌باده عاری‌ست</p></div></div>