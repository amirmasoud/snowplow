---
title: >-
    غزل شمارهٔ ۸۵۴
---
# غزل شمارهٔ ۸۵۴

<div class="b" id="bn1"><div class="m1"><p>دل ماند بی‌حس و غمت افشانده بال رفت</p></div>
<div class="m2"><p>این ناوک وفا همه جا پوست‌مال رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلقی ازین بساط به وهم ‌گذشتگی</p></div>
<div class="m2"><p>بی‌نقش پا چو قافلهٔ ماه و سال رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین دشت‌ گرد ناقهٔ دیگر نشد بلند</p></div>
<div class="m2"><p>هرمحملی‌که رفت به دوش خیال رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زردوستان تهیهٔ راه عدم کنید</p></div>
<div class="m2"><p>قارون به زیر خاک پی جمع مال رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناایمنی نبرد زگوهر حصار موج</p></div>
<div class="m2"><p>سرها به زانوی عدم از زیر بال رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر شرم داری از هوس جاه شرم دار</p></div>
<div class="m2"><p>تا قطره شد گهر عرق انفعال رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی‌دستگاهی‌، آفت آثار مرد نیست</p></div>
<div class="m2"><p>نارفتنی است خط اگر از خامه نال رفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>موج‌گهر، چه واکشد از معنی محیط</p></div>
<div class="m2"><p>حرفی که داشتم به زبانهای لال رفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اشکم به دیده محمل‌انداز برق داشت</p></div>
<div class="m2"><p>گفتم نگاهی آب دهم بر شکال رفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تصویر تیره‌بختی من می‌کشید عشق</p></div>
<div class="m2"><p>از هند تا فرنگ‌، قلم بر زگال رفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای چینی اینقدر به طنین موی سر مکن</p></div>
<div class="m2"><p>فغفور در اعادهٔ ساز سفال رفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل دلیل مقصد عزت تواضع است</p></div>
<div class="m2"><p>زبن جاده ماه نو به جهان‌کمال رفت</p></div></div>