---
title: >-
    غزل شمارهٔ ۲۷۰۳
---
# غزل شمارهٔ ۲۷۰۳

<div class="b" id="bn1"><div class="m1"><p>بیحاصلی‌ام بست به‌ گردن خم پیری</p></div>
<div class="m2"><p>چون بید ز سر تا قدمم عالم پیری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عالم فرصت چقدر قافیه تنگ است</p></div>
<div class="m2"><p>مو،‌ رست سیه‌، پیش‌تر از ماتم پیری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا پنبه نهد کس به سر داغ جوانی</p></div>
<div class="m2"><p>کافور ندارد اثر مرهم پیری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>موقوف فراموشی ایام شبابست</p></div>
<div class="m2"><p>خلدی اگر ایجاد کند عالم پیری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیهات به این حلقه در دل نگشودند</p></div>
<div class="m2"><p>رفتند جوانان همه نامحرم پیری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آزادگی آن نیست ‌که از مرگ هراسد</p></div>
<div class="m2"><p>بر سرو نبسته‌ست خمیدن غم پیری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل خورد فشاری‌ که ز هم ریخت نگینش</p></div>
<div class="m2"><p>زبن بیش چه تنگی دمد از حاتم پیری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تأثیر نفس سوخت به سامان فسردن</p></div>
<div class="m2"><p>رو آتش یاقوت فروز از دم پیری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>انگشت‌نمای عدم از موی سپیدم</p></div>
<div class="m2"><p>کردند چو صبحم علم از پرچم پیری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون موی سپیدی زند از لاف حیا کن</p></div>
<div class="m2"><p>هشدار که زال است همان رستم پیری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل تو جوانی به تک و تاز قدم زن</p></div>
<div class="m2"><p>من سایهٔ دیوار خودم از خم پیری</p></div></div>