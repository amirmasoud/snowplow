---
title: >-
    غزل شمارهٔ ۲۳۳۲
---
# غزل شمارهٔ ۲۳۳۲

<div class="b" id="bn1"><div class="m1"><p>نسخهٔ هیچیم‌، وهمی از عدم آورده‌ایم</p></div>
<div class="m2"><p>ما و من حرفی‌ که می‌گردد رقم آورده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خامشی بی آه و گفت‌وگوی باب ناله نیست</p></div>
<div class="m2"><p>یک نفس سازیم و چندین زیر و بم آورده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیچ نقش از پردهٔ معدومی ما گل نکرد</p></div>
<div class="m2"><p>یک‌قلم خاکستریم‌، آیینه کم آورده‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای فلک از ما ضعیفان بیش از این طاقت مخواه</p></div>
<div class="m2"><p>چون مه نو خویش را بر پشت خم آورده‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آفتابی‌ کرد رنگ طاقت ما احتیاج</p></div>
<div class="m2"><p>تا به‌خاطر سایهٔ دست کرم آورده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر درت پیشانی خجلت شفیع ما بس است</p></div>
<div class="m2"><p>سجده‌ای در بار ما گر نیست نم آورده‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عمرها نامحرم جیب تأمل تاختیم</p></div>
<div class="m2"><p>تاکنون ما و خیالت سر بهم آورده‌ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کو تنزه سجده‌ای تا آبرو بندیم نقش</p></div>
<div class="m2"><p>زحمتی بر خاک پایت از قسم آورده‌ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صبح ما روشن سواد نسخهٔ آرام نیست</p></div>
<div class="m2"><p>سطر گردی در خیال از مشق رم آورده‌ایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دست عجز ما صلای جلوه‌ای دارد بلند</p></div>
<div class="m2"><p>عرصه حیرانی است از مژگان علم آورده‌ایم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اینقدر رقص سپند ما به‌امید فناست</p></div>
<div class="m2"><p>ناله در باریم اما سرمه هم آورده‌ایم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سعی ما واماندگان سر منزلی دیگر نداشت</p></div>
<div class="m2"><p>همچو لغزش زور بر نقش قدم آورده‌ایم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همت ما چون سحر منت‌کش اسباب نیست</p></div>
<div class="m2"><p>اینقدر هستی که داریم از عدم آورده‌ایم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حاصل جمعیت اسباب جز عبرت نبود</p></div>
<div class="m2"><p>مفت ما بیدل‌ که مژگانی بهم آورده ایم</p></div></div>