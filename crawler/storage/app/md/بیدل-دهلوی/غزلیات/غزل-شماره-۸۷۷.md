---
title: >-
    غزل شمارهٔ ۸۷۷
---
# غزل شمارهٔ ۸۷۷

<div class="b" id="bn1"><div class="m1"><p>بیا ای جام و مینای طرب نقش‌کف پایت</p></div>
<div class="m2"><p>خرام موج می مخمور طرز آمدن‌هایت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نفس در سینه‌، نکهت آشیان خلد توصیفت</p></div>
<div class="m2"><p>نگه در دیده شبنم پرور باغ تماشایت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکوه جلوه‌ات جز در فضای دل نمی‌گنجد</p></div>
<div class="m2"><p>جهان پرگردد از آیینه تا خالی شود جایت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پر آسان است اگر توفیق بخشد نور بی‌تابی</p></div>
<div class="m2"><p>تماشای بهشت از گوشهٔ چشم تمنایت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>توان در موج ساغر غوطه زد از نقش پیشانی</p></div>
<div class="m2"><p>به مستی گر دهد فرمان نگاه نشئه‌پیمایت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فروغ شمع هم مشکل تواند رنگ گرداندن</p></div>
<div class="m2"><p>در آن محفل که منع دور ساغر باشد ایمایت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مروت صرف ایجادت کرم فیض خدا دادت</p></div>
<div class="m2"><p>ادب تعمیر بنیادت حیا آثار سیمایت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نظراندیشی وهمم به داغ غیر می‌سوزد</p></div>
<div class="m2"><p>دلی آیینه سازم کز تو ریزم رنگ همتایت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هواخوه تو اکسیر سعادت در بغل دارد</p></div>
<div class="m2"><p>نفس بودم سحر گل کردم از فیض دعاهایت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تهی از سجدهٔ شوقت سر مویی نمی‌یابم</p></div>
<div class="m2"><p>سراپا در جبین می‌غلتم از یاد سراپایت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اثر محو دعای بیدل است امید آن دارد</p></div>
<div class="m2"><p>که بالد دین و دنیا در پناه دین و دنیایت</p></div></div>