---
title: >-
    غزل شمارهٔ ۱۰۱۵
---
# غزل شمارهٔ ۱۰۱۵

<div class="b" id="bn1"><div class="m1"><p>اسرار در طبایع ضبط نفس ندارد</p></div>
<div class="m2"><p>درپردهٔ خس و خار، آتش قفس ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گو وهم سوده باشد بر چرخ تاج شاهان</p></div>
<div class="m2"><p>سعی هما بلندی پیش مگس ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خرد و بزرگ دنیا یکدست خودسرانند</p></div>
<div class="m2"><p>خر گر فسار گم ‌کرد سگ هم مرس ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای برک‌گل بلند است اقبال پایبوسش</p></div>
<div class="m2"><p>رنگ حناست آنجا، کس دسترس ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درگلشنی‌که ما را دادند بار تحقیق</p></div>
<div class="m2"><p>صبح بهار هستی بوی نفس ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا ناله‌وار گاهی‌زین تنگنا برآییم</p></div>
<div class="m2"><p>افسوس دامن ما، چین ففس ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر حال رفتگان‌ کیست تا نوحه‌ای ‌کند سر</p></div>
<div class="m2"><p>این کاروان شفیقی غیر از جرس ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تدبیر عالم وهم بر وهم واگذارید</p></div>
<div class="m2"><p>اینجا پریدن چشم پروای خس ندارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گردون خرام شوقیم پرگار دور ذوقیم</p></div>
<div class="m2"><p>بی‌وهم تحت‌ و فوقیم‌، دل پیش‌ و پس ندارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سودا ز سر بینداز، نرد خیال کم باز</p></div>
<div class="m2"><p>تشویق بیدماغان عشق و هوس ندارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر فرصتی که نامش هستی‌ست دامن‌افشان</p></div>
<div class="m2"><p>بیدل نفس مدارا با هیچکس ندارد</p></div></div>