---
title: >-
    غزل شمارهٔ ۲۵۷۴
---
# غزل شمارهٔ ۲۵۷۴

<div class="b" id="bn1"><div class="m1"><p>گر از موج‌ گهر نشنیده‌ای رمز خروش او</p></div>
<div class="m2"><p>بیا شور تبسم بشنو از لعل خموش او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حیا ساقی‌ست چندانی‌ که حسنش رنگ ‌گرداند</p></div>
<div class="m2"><p>ز شبنم می‌زند ساغر بهار گلفروش او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چمن جام طرب در جلوهٔ شاخ‌ گلی دارد</p></div>
<div class="m2"><p>که خم ‌گردید از بار سبوی غنچه دوش او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندانم بوالهوس از گردش ساغر چه پیماید</p></div>
<div class="m2"><p>که شد پا در رکاب از صورت پیمانه هوش او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خروشی می‌کند توفان چه از دانا چه از نادان</p></div>
<div class="m2"><p>جهان ‌خمخانه‌ای د‌ارد که این ‌رنگ است‌ جوش او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نباید بودن از پشت و رخ‌ کار جهان غافل</p></div>
<div class="m2"><p>چو زنبور عسل نیشی است در دنبال نوش او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غرور خود سری را چارهٔ دیگر نمی‌باشد</p></div>
<div class="m2"><p>مگر گردد خیال خاک‌ گشتن عیب پوش او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نوای صور هم مشکل ‌گشاید گوش استغنا</p></div>
<div class="m2"><p>چه نازم بر دل افسرده و ساز خروش او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زبان بو‌ی‌ گل جز غنچه بیدل ‌کس نمی‌فهمد</p></div>
<div class="m2"><p>فغان نازکی دارم اگر افتد به‌ گوش‌ او</p></div></div>