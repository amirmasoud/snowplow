---
title: >-
    غزل شمارهٔ ۲۷۷۶
---
# غزل شمارهٔ ۲۷۷۶

<div class="b" id="bn1"><div class="m1"><p>نشد حجاب خیالم غبار جسمانی</p></div>
<div class="m2"><p>حباب رانه ز پیراهن است عریانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز اینقدر نشد از سرنوشت من ظاهر</p></div>
<div class="m2"><p>که سجده می‌چکدم چون نگین ز پیشانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو شمع دام امید است سعی پروازم</p></div>
<div class="m2"><p>سزد که رنگ قفس ریزم از پر افشانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خاک تا نشود ساز ما و من هموار</p></div>
<div class="m2"><p>نفس نمی‌گذرد از تلاش سوهانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز پیچ و تاب نفس عالمی جبون قفس است</p></div>
<div class="m2"><p>چوگرد باد تو هم‌ دسته کن پریشانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سفر گزیده به فکر وطن چه پردازد</p></div>
<div class="m2"><p>دوباره مرغ نگردد به بیضه زندانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوای عیش تو تا رشتهٔ نفس دارد</p></div>
<div class="m2"><p>ز سطر نسخهٔ زنجیر ناله می‌خوانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به مرگ نیز همان حب جاه خلق بجاست</p></div>
<div class="m2"><p>مگر همابرد از استخوان گرانجانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گداز ما چونگه آنسوی نم افتاده است</p></div>
<div class="m2"><p>دل و دماغ چکیدن به اشک ارزانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غبارکثرت امکان حجاب وحدت نیست</p></div>
<div class="m2"><p>شکوه شعله به خاشاک چند پوشانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جنون به‌ کسوت ناموس جلوه‌ها دارد</p></div>
<div class="m2"><p>چو اشک، آینه صیقل مزن ز عریانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو خامه ‌گر به خموشی به سر بری بیدل</p></div>
<div class="m2"><p>تو نیز راز دل خلق بر زبان رانی</p></div></div>