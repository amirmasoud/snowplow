---
title: >-
    غزل شمارهٔ ۴۰۴
---
# غزل شمارهٔ ۴۰۴

<div class="b" id="bn1"><div class="m1"><p>یاد وصلی‌ کردم آغوش من دیوانه ‌سوخت</p></div>
<div class="m2"><p>لاله‌سان از گرمی این می دل پیمانه سوخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناله‌ها رفت از دل و احرام آزادی نبست</p></div>
<div class="m2"><p>پرتو خود را در اول شمع این‌ کاشانه سوخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وقت رندی خوش که در ماتمسرای اعتبار</p></div>
<div class="m2"><p>خرمن ‌هستی چو برق ‌از خنده ی مستانه سوخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دور دار از زلفش ای مشاطهٔ‌گستاخ‌، دست</p></div>
<div class="m2"><p>آتش این‌ دود نزدیک است خواهد شانه‌ سوخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق هرجا در خیال مجلس ‌آرایی نشست</p></div>
<div class="m2"><p>هر دو عالم در چراغ کلبهٔ دیوانه سوخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما نه ‌تنها در شکنج جسم‌ گردیدیم خاک</p></div>
<div class="m2"><p>ای بسا گنجی ‌که نقد خویش در ویرانه سوخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اضطراب حال دل‌، ما را به حیرت داغ ‌کرد</p></div>
<div class="m2"><p>آتش این خانه رخت ما برون خانه سوخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دود هم دستی به دامان شرار ما نزد</p></div>
<div class="m2"><p>آخر از بی ریشگی در مزرع ما دانه سوخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا سواد سطری از رمز وفا روشن شود</p></div>
<div class="m2"><p>صد نفس باید به تحقیق پر پروانه سوخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عالمی بیدل به‌ حرف یکدگر آرام باخت</p></div>
<div class="m2"><p>غفلت ما هم دماغ خواب در افسانه سوخت</p></div></div>