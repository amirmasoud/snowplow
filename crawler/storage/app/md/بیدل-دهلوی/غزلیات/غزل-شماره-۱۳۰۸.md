---
title: >-
    غزل شمارهٔ ۱۳۰۸
---
# غزل شمارهٔ ۱۳۰۸

<div class="b" id="bn1"><div class="m1"><p>زان نشئه‌ که قلقل به لب شیشه دواند</p></div>
<div class="m2"><p>صد رنگ صریر قلمم ریشه دواند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون شمع اگر سوخت سر و برگ نگاهم</p></div>
<div class="m2"><p>خاکستر من شعله در اندیشه دواند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از عشق و هوس چاره ندارم چه توان‌ کرد</p></div>
<div class="m2"><p>سعی نفس است این‌که به هرپیشه دواند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خار و خس اوهام ‌گرفته‌ست جهان را</p></div>
<div class="m2"><p>کو برق ‌که یک ریشه درین بیشه دواند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ساز وفا ناخن تدبیر دگر نیست</p></div>
<div class="m2"><p>فرهاد همان بر سر خود تیشه دواند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنجا که خیالت چمن‌آرای حضور است</p></div>
<div class="m2"><p>مژگان به صد انداز نگه ریشه دواند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بزم تو شمعی به‌ گداز آمده وقت است</p></div>
<div class="m2"><p>رنگی به رخم غیرت هم پیشه دواند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>محو است به خاموشی مستان نگاهت</p></div>
<div class="m2"><p>شوری ‌که نفس در نفس شیشه دواند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیدل‌ گهر نظم‌ کسی راست‌ که امروز</p></div>
<div class="m2"><p>در بحر غزل زورق اندیشه دواند</p></div></div>