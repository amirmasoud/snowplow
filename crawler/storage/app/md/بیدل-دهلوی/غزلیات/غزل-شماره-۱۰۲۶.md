---
title: >-
    غزل شمارهٔ ۱۰۲۶
---
# غزل شمارهٔ ۱۰۲۶

<div class="b" id="bn1"><div class="m1"><p>نهال زندگی بالیدنی وحشت‌کمین دارد</p></div>
<div class="m2"><p>نفس‌گر ریشه پیدا می‌کند ننگ از زمین دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عدم سرمایه‌ایم از دستگاه ما چه می‌پرسی</p></div>
<div class="m2"><p>شرار از نقد هستی یک نگاه واپسین دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمی‌خواهد کسی خود را غبارآلود بی‌دردی</p></div>
<div class="m2"><p>اگر ما درد دل داریم زاهد درد دین دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فسردن نیست دل را بی تو در کنج گرانجانی</p></div>
<div class="m2"><p>که در هر جزو این سنگ آتش دیگرکمین دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تصرف نیست ممکن در دل ما عیش امکان را</p></div>
<div class="m2"><p>که این اقلیم را داغ غمت زیر نگین دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو هر رنگی‌که خواهی جلوه‌کن در تنگنای دل</p></div>
<div class="m2"><p>سراسر خانهٔ آیینه‌ام یک‌گل زمین دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به‌هر بی‌دست‌وپایی شمع‌از خودمی‌برد خود را</p></div>
<div class="m2"><p>نبیند واپسی هرکس نگاه پیش‌بین دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکنج چهرهٔ اقبال باشد درخور دولت</p></div>
<div class="m2"><p>به قدر نردبان قصر شهان چین جبین دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ندارد چاره از بی‌دستگاهی طینت موزون</p></div>
<div class="m2"><p>که سرو این چمن صد دست در یک آستین دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به احرام محبّت از گداز دل مشو ایمن</p></div>
<div class="m2"><p>هوای وادی مجنون مزاج آتشین دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کمال دانش ماگر فراموشی‌ست از عالم</p></div>
<div class="m2"><p>مشو مغرور آگاهی که غفلت هم همین دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به رنج یک تپیدن صد جهان عشرت نمی‌ارزد</p></div>
<div class="m2"><p>نمی‌دانم کدامین آرزو دل را برین دارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به همّت یک قدم زین عرصه نتوان تاختن بیدل</p></div>
<div class="m2"><p>وگر نه هر که بینی رخش صد دعوی به زین دارد</p></div></div>