---
title: >-
    غزل شمارهٔ ۵۰
---
# غزل شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>محبت بس که پر کرد از وفا جان و تن ما را</p></div>
<div class="m2"><p>کند یوسف صدا گر بو کنی پیراهن ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو صحرا مشرب ما ننگ وحشت برنمی‌تابد</p></div>
<div class="m2"><p>نگه دارد خدا از تنگی چین دامن ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان مطلق عنان‌تاز است شمع ما ازین محفل</p></div>
<div class="m2"><p>که رنگ رفته دارد پاس از خود رفتن ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خرامش در دل هر ذره صد توفان جنون دارد</p></div>
<div class="m2"><p>عنان گیرید این آتش به عالم افکن ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گهر دارد حصار آبرو در ضبط امواجش</p></div>
<div class="m2"><p>میندازید ز آغوش ادب پیراهن ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فلک در خاک می‌غلتید از شرم سرافرازی</p></div>
<div class="m2"><p>اگر می‌دید معراج ز پا افتادن ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به اشک افتاد کار آه ما از پیش پا دیدن</p></div>
<div class="m2"><p>ز شبنم بال تر گردید صبح گلشن ما را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هوس هرسو بساط ناز دیگر پهن می‌چیند</p></div>
<div class="m2"><p>ندید این بی‌خبر مژگان به هم آوردن ما را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ازین خاشاک اوهامی که دارد مزرع هستی</p></div>
<div class="m2"><p>به گاو چرخ نتوان پاک کردن خرمن ما را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو ماهی خارخار طبع در کار است و ما غافل</p></div>
<div class="m2"><p>که بر امواج پوشانده‌ست گردون جوشن ما را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز آب زندگی تا بگذرد تشویش رعنایی</p></div>
<div class="m2"><p>خم وضع ادب پل کرد دوش و گردن ما را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به حرف و صوت تا کی تیره سازی وقت ما بیدل</p></div>
<div class="m2"><p>چراغ چارسو مپسند طبع روشن ما را</p></div></div>