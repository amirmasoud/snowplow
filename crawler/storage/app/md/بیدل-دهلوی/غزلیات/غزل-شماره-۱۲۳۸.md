---
title: >-
    غزل شمارهٔ ۱۲۳۸
---
# غزل شمارهٔ ۱۲۳۸

<div class="b" id="bn1"><div class="m1"><p>شوق دیداری ‌که از دل بال حسرت می‌کشد</p></div>
<div class="m2"><p>تا به مژگان می‌رسد آغوش حیرت می‌کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌رخت تمهید خوابم خجلت ارام نیست</p></div>
<div class="m2"><p>لغزش مژگان من خط بر فراغت می‌کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از عرق پیمایی شبنم پر است آغوش صبح</p></div>
<div class="m2"><p>همت مخمورم از خمیازه خجلت می‌کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرکجاگل می‌کند نقش ضعیفیهای من</p></div>
<div class="m2"><p>خامهٔ نقاش‌، موی چشم صنعت می‌کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای نهال ‌گلشن عبرت به رعنایی مناز</p></div>
<div class="m2"><p>شمع پستی می‌کشد چندانکه قامت می‌کشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غفلت نشو و نمایت صرفهٔ جمعیت است</p></div>
<div class="m2"><p>تخم این مزرع ‌به‌ جای پشه آفت می‌کشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زور بازویی که داری انفعالی بیش نیست</p></div>
<div class="m2"><p>ناتوانی انتقام آخر ز طاقت می‌کشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگذر از حرص ریاستها کز افسون هوس</p></div>
<div class="m2"><p>گرهمه قاضی شوی کارت به رشوت می‌کشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بندگی‌، شاهی‌، گدایی‌، مفلسی‌، گردن‌کشی</p></div>
<div class="m2"><p>خاک عبرت‌خیز ما صد رنگ تهمت می‌کشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چرخ را از سفله‌پرورخواندن‌کس ننگ نیست</p></div>
<div class="m2"><p>تهمت کم‌همتیها تیر همت می‌کشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پیرگردیدی ز تکلیف تعلقها برآ</p></div>
<div class="m2"><p>دوش خم از هرچه برداری ندامت می‌کشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کوه هم دارد به قدر ناله دامن چیدنی</p></div>
<div class="m2"><p>محمل تمکین هربنیاد خفت می‌کشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بی‌خبر از آفت اقبال نتوان زیستن</p></div>
<div class="m2"><p>عالمی را دار از چاه مذلت می‌کشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای شرر تا چند خواهی غافل ازخود تاختن</p></div>
<div class="m2"><p>گردش‌ چشم است میدانی‌که فرصت می‌کشد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نوحه بر تدبیرکن بیدل که در صحرای عشق</p></div>
<div class="m2"><p>پا به دفع خار زآتش بار منت می‌کشد</p></div></div>