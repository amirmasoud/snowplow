---
title: >-
    غزل شمارهٔ ۲۴۳۰
---
# غزل شمارهٔ ۲۴۳۰

<div class="b" id="bn1"><div class="m1"><p>کار آسانی مدان تاج کمر برداشتن</p></div>
<div class="m2"><p>همچو خورشید آتشی باید به سر برداشتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غفلت ذاتی به جهد ازدل نگردد مرتفع</p></div>
<div class="m2"><p>تیرگی نتوان به صیقل از سپر برداشتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سعی بیمغزان به عزم خفت ما باطل است</p></div>
<div class="m2"><p>نیست ممکن پنبه را آب ازگهر برداشتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برندارد دوش آزادی خم باری دگر</p></div>
<div class="m2"><p>یک نگه‌کم نیست‌گر خواهد شرر برداشتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سایهٔ مو نیز می‌چربد بر آثار نفس</p></div>
<div class="m2"><p>اینقدر گردن نمی‌ارزد به سر برداشتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حایلی دیگر ندارد منزل مقصود ما</p></div>
<div class="m2"><p>گرد خود می‌باید از ره چون سحر برداشتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همتت در ترک اسباب اینقدر عاجز چراست</p></div>
<div class="m2"><p>می شود افکندن بارت مگر برداشتن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون نگه تاکی ز مژگان زحمتت باید کشید</p></div>
<div class="m2"><p>یک تپش پرواز و چندین بال و پر برداشتن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست عذر ناتوانی باب اقلیم وفا</p></div>
<div class="m2"><p>زخم بسیار است می‌باید جگر برداشتن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شرم‌دار از سعی خوه ای حرص‌کوش بیخبر</p></div>
<div class="m2"><p>عزم مقصدگور و آنگه‌ کرّ و فر برداشتن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کر چنین نیرن حرصت دشمن آسودکی‌ست</p></div>
<div class="m2"><p>خاک شو در منزل ازگرد سفر برداشتن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دانه را بیدل ز فیض سجده‌ریزیهای عجز</p></div>
<div class="m2"><p>نیست بی نشو و نما از خاک سر برداشتن</p></div></div>