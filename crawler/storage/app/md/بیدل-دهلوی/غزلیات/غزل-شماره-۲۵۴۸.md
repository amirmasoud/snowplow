---
title: >-
    غزل شمارهٔ ۲۵۴۸
---
# غزل شمارهٔ ۲۵۴۸

<div class="b" id="bn1"><div class="m1"><p>دوری مقصد دمید از سرکشیدنهای من</p></div>
<div class="m2"><p>نقش پاگم‌کرد پیش پا ندیدنهای من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نفس از هستی خود در غبار خجلتم</p></div>
<div class="m2"><p>کز جهانی برد آسایش تپیدنهای من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>الفت هستی چو صبحم نردبان وحشت است</p></div>
<div class="m2"><p>چین دامن نیست جز بر خویش چیدنهای من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شور محشر گوش خلقی وانکرد اما چه سود</p></div>
<div class="m2"><p>اندکی نزدیک می‌خواهد شنیدنهای من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شمع ماتمخانهٔ یاسم زاحولم مپرس</p></div>
<div class="m2"><p>بی‌تو در آغوش مژگان سوخت دیدنهای من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاکساری آبیارم چون نهال‌گرد باد</p></div>
<div class="m2"><p>گرد می‌گردد بلند از قدکشیدنهای من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیر جیب امن امکان بود بی‌سعی‌ گداز</p></div>
<div class="m2"><p>همچو شمع آمد به‌کار از هم چکیدنهای من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پا به دامن دارم و جولان حرص آسوده نیست</p></div>
<div class="m2"><p>خاک افسردن به فرق آرمیدنهای من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ریشهٔ وامانده‌م‌، رنگ نمو گم کرده‌ام</p></div>
<div class="m2"><p>با رگ یاقوت می‌جوشد دوبدنهای من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون ثمر بیدل به چندین ریشه جولان امید</p></div>
<div class="m2"><p>تا شکست خود رسید آخر رسیدنهای من</p></div></div>