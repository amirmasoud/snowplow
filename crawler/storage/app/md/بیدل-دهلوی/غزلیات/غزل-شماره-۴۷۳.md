---
title: >-
    غزل شمارهٔ ۴۷۳
---
# غزل شمارهٔ ۴۷۳

<div class="b" id="bn1"><div class="m1"><p>شعلهٔ بی‌بال وپر سجده گر اخگر است</p></div>
<div class="m2"><p>سعی چو پستی گرفت، آبله ی پا، سر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باعث لاف غرور نیست جز اسباب جاه</p></div>
<div class="m2"><p>دعوی پروازها در خور بال و پر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عرض هنر می‌دهد دل ز خم و پیچ آه</p></div>
<div class="m2"><p>آینهٔ داغ اگر دود کشد جوهر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواری دیوان دهر عزت ما بیش‌کرد</p></div>
<div class="m2"><p>فرد چو باطل شود سر ورق دفتراست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند زند همتم فال بنای امل</p></div>
<div class="m2"><p>رشتهٔ نومیدیی دارم و محکم ‌تر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناله ز هر جا دمد، بی‌خلش درد نیست</p></div>
<div class="m2"><p>زخمه رگ ساز را تیزتر !ز نشتر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهل دل آتش دم‌اند، بین که به روی محیط</p></div>
<div class="m2"><p>آبله‌های حباب از نفس‌گوهر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یار در آغوش تست هرزه به هرسو متاز</p></div>
<div class="m2"><p>دیده ی بینا طلب جلوه نگه‌ پرور است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست بساط جهان‌، قابل دلبستگی</p></div>
<div class="m2"><p>ریشهٔ ما چون نفس در چمن دیگر است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شیوه‌تغافل خوش‌است ورنه به‌این‌برق حسز</p></div>
<div class="m2"><p>تا تو نظرکرده‌ای آینه خاکستر است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غیرفنا نگسلد بند غرور نفس</p></div>
<div class="m2"><p>رشتهٔ این شمع را عقده‌کشا صرصر است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل از آشوب دهر سرن کشیدی به جیب</p></div>
<div class="m2"><p>زورق توفانی‌ات بیخبر از لنگر است</p></div></div>