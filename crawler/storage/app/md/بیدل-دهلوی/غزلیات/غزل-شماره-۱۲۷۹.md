---
title: >-
    غزل شمارهٔ ۱۲۷۹
---
# غزل شمارهٔ ۱۲۷۹

<div class="b" id="bn1"><div class="m1"><p>نتوان به تلاش از غم اسباب برآمد</p></div>
<div class="m2"><p>گوهر چه نفس سوخت ‌که از آب برآمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غافل نتوان بود به خمخانهٔ توفیق</p></div>
<div class="m2"><p>ز آن جوش‌ که دردی ز می ناب برآمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواه انجمن‌آرا شد و خواه آینه پرداخت</p></div>
<div class="m2"><p>از خانهٔ خورشید همین تاب برآمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیرنگ نفس شور دو عالم به عدم بست</p></div>
<div class="m2"><p>در ساز نبود اینکه ز مضراب برآمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای دیده‌وران چارهٔ حیرت چه خیال است</p></div>
<div class="m2"><p>آیینه عبث طالب سیماب برآمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از ساحل این بحر زبان می‌کشد آتش</p></div>
<div class="m2"><p>کشتی به چه امید ز گرداب برآمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیش از همه در عالم غیرت خجلم‌ کرد</p></div>
<div class="m2"><p>آن‌ کار که بی‌منت احباب برآمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این دشت ز بس منفعل‌ کوشش ما بود</p></div>
<div class="m2"><p>خاکی‌ که بر آن دست زدیم آب برآمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زین باغ به‌ کیفین رنگی نرسیدیم</p></div>
<div class="m2"><p>دریا همه یک ‌گوهر نایاب برآمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیدایی او صرفهٔ موهومی ما نیست</p></div>
<div class="m2"><p>با سایه مگوییدکه مهتاب برآمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زان گرمی نازی که دمید ازکف پایش</p></div>
<div class="m2"><p>مخمل عرقی‌کردکه از خواب برآمد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل چو مه نو به سجودکه خمیدی</p></div>
<div class="m2"><p>کامروز چراغ تو ز محراب برآمد</p></div></div>