---
title: >-
    غزل شمارهٔ ۱۹۳۲
---
# غزل شمارهٔ ۱۹۳۲

<div class="b" id="bn1"><div class="m1"><p>می‌آید از دشت جنون گردم بیابان در بغل</p></div>
<div class="m2"><p>توفان وحشت در قدم فوج غزالان در بغل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سودایی داغ ترا از شام نومیدی چه غم</p></div>
<div class="m2"><p>پروانهٔ بزم وفا دارد چراغان در بغل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از وحشت این تنگنا هرکس به رنگی می‌رود</p></div>
<div class="m2"><p>دریا و مینایی به‌ کف صحرا و دامان در بغل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از چشم خویش ایمن نی‌ام ‌کاین قطرهٔ دریا نسب</p></div>
<div class="m2"><p>دارد به وضع شبنمی صد رنگ توفان در بغل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رسوای آفاقم چو صبح از شوخی داغ جنون</p></div>
<div class="m2"><p>چون آفتاب آیینه‌ای پوشید نتوان در بغل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرید به حال آگهی ‌کز غفلت نامحرمی</p></div>
<div class="m2"><p>چون چشم اعمی‌ کرده‌ام آیینه پنهان در بغل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاک من بنیاد سر در حسر ت چاک جگر</p></div>
<div class="m2"><p>وقتست چون‌ گرد سحر خیزد گریبان در بغل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کام دل حسرت‌ گدا حاصل نشد از ما سوا</p></div>
<div class="m2"><p>عمریست می‌خواهد ترا این خانه ویران در بغل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای‌ کارگاه وهم و ظن نشکافتی رمز سخن</p></div>
<div class="m2"><p>اینجا ندارد پیرهن جز شخص عریان در بغل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دکان غفلت وا مکن با زندگی سودا مکن</p></div>
<div class="m2"><p>خود را عبث رسوا مکن زین سود نقصان دربغل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل ندارد بزم ما از دستگاه عافیت</p></div>
<div class="m2"><p>چشمی‌که‌گیرد یک دمش چون شمع مژگان در بغل</p></div></div>