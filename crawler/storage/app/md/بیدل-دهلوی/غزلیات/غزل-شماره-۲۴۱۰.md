---
title: >-
    غزل شمارهٔ ۲۴۱۰
---
# غزل شمارهٔ ۲۴۱۰

<div class="b" id="bn1"><div class="m1"><p>عرقها دارد آن شمع حیا لیک از نظر پنهان</p></div>
<div class="m2"><p>به تمکینی که آتش نیست در سنگ آنقدر پنهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو آن اشکی که گردد خشک در آغوش مژگانها</p></div>
<div class="m2"><p>به عشقت در طلسم نیشتر دارم جگر پنهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زدم از آفت امکان به برق سایهٔ تیغت</p></div>
<div class="m2"><p>به ذوق عافیت کردم به زیر بال‌، سر پنهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکست رنگ هم شوخی نکرد از ضعف احوالم</p></div>
<div class="m2"><p>در این ویرانه ماند آخر نشان گنج زر پنهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه امکانست گرد وحشتم از دل برون جوشد</p></div>
<div class="m2"><p>تحیر رشته‌ای چون موج دارم در گهر پنهان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز موی خود خروش چینی از شرم صفیر من</p></div>
<div class="m2"><p>صدای کاسهٔ چشم است در تار نظر پنهان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تماشاگاه جمعیت‌، تحیر خانه‌ای دارم</p></div>
<div class="m2"><p>که چون آیینه در دیوار دارد نام در پنهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مکن تکلیف گلگشت چمن مجروح الفت را</p></div>
<div class="m2"><p>که بو در برگ گل تیغی‌ست در زیر سر پنهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سراغ هیچکس از هیچکس بیرون نمی‌آید</p></div>
<div class="m2"><p>جهانی می‌رود در نقش پای یکدگر پنهان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سراپا وحشتم اما به ناموس سبکروحی</p></div>
<div class="m2"><p>ز چشم نقش پا چون رنگ می‌دارم سفر پنهان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ندارد لب گشودن صرفهٔ جمعیتم بیدل</p></div>
<div class="m2"><p>که من چون غنچه در منقار دارم بال و پر پنهان</p></div></div>