---
title: >-
    غزل شمارهٔ ۸۸۴
---
# غزل شمارهٔ ۸۸۴

<div class="b" id="bn1"><div class="m1"><p>تأمل عارفان چه دارد به کارگاه جهان حادث</p></div>
<div class="m2"><p>نوای ساز قدم شنیدن ز زخمه‌های زبان حادث</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکست‌و بستی که‌موج دارد کسی‌چه مقدار واشمارد</p></div>
<div class="m2"><p>به ‌یک ‌وتیره است تا قیامت حساب سود و زیان حادث</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز فکر سودای پوچ هستی به شرم باید تنید و پا زد</p></div>
<div class="m2"><p>به دستگاه چه جنس نازد سقط فروش دکان حادث</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ازبن بساط خیال رونق نقاب رمز ظهور کن شق‌</p></div>
<div class="m2"><p>خزان ندارد بهار مطلق بهار دارد خزان حادث</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فسانه‌ای ناتمام دارد حقیقت عالم تعین</p></div>
<div class="m2"><p>تو درخور فرصتی‌ که داری تمام‌ کن داستان حادث</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی در‌ین دشت بی‌سر و پا برون منزل نمی‌خرامد</p></div>
<div class="m2"><p>به خط پرگار جاده دارد تردد کاروان حادث</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غم و طرب نعمت است اما نصیب لذت‌ که راست اینجا</p></div>
<div class="m2"><p>تجدد الوان ناز دارد نیاز مهمان خوان حادث</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگرشکستیم وگر سلامت‌که دارد اندیشهٔ ندامت</p></div>
<div class="m2"><p>بر اوستاد قدم فتاده است رنج میناگران حادث</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رموز فطرت بر این سخن کرد ختم صد معنی و عبارت</p></div>
<div class="m2"><p>که آشکار و نهان ندارد جز آشکار و نهان حادث</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به پستی اعتبار بیدل ‌عبث فسردی و خاک گشتی</p></div>
<div class="m2"><p>نمی‌ توان‌ کرد بیش از اینها زمینی و آسمان حادث</p></div></div>