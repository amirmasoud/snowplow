---
title: >-
    غزل شمارهٔ ۲۱۸۲
---
# غزل شمارهٔ ۲۱۸۲

<div class="b" id="bn1"><div class="m1"><p>ز سودای چشم تو تا کام ‌گیرم</p></div>
<div class="m2"><p>دو عالم فروشم دو بادام گیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شهید وفایم ز راحت جدایم</p></div>
<div class="m2"><p>نه مردم به ذوقی که آرام گیرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیه مست شهرت نی‌ام ورنه من هم</p></div>
<div class="m2"><p>چو نقش نگین صبح در شام گیرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بس همتم ننگ تزویر دارد</p></div>
<div class="m2"><p>محالست اگر دانه در دام گیرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنین‌ کز طلب بی‌نیاز است طبعم</p></div>
<div class="m2"><p>گدا گر شوم ترک ابرام گیرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چوشبنم چه لافم به سامان هستی</p></div>
<div class="m2"><p>مگر از عرق صورتی وام گیرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درین انجمن مشرب غنچه دارم</p></div>
<div class="m2"><p>زنم شیشه بر سنگ تا جام‌گیرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زمانی شود خواب عیشم میسر</p></div>
<div class="m2"><p>که چون نقش پا سایه بر بام گیرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کمند نفس حرص صیاد عنقاست</p></div>
<div class="m2"><p>به ‌این نارسایی مگر نام گیرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جهان نیست جز اعتبار من و تو</p></div>
<div class="m2"><p>تو تحقیق دان‌ گر من اوهام‌ گیرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تجاهل سر و برگ هستی است بیدل</p></div>
<div class="m2"><p>همه گر وصالست پیغام گیرم</p></div></div>