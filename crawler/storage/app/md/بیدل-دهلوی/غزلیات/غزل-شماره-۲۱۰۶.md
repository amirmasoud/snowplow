---
title: >-
    غزل شمارهٔ ۲۱۰۶
---
# غزل شمارهٔ ۲۱۰۶

<div class="b" id="bn1"><div class="m1"><p>چو بوی‌گل به نظرها نقاب نگشودم</p></div>
<div class="m2"><p>بهار آینه پرداخت لیک ننمودم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیال پوچ دو روزم غنیمت سوداست</p></div>
<div class="m2"><p>به این متاع ‌که در پیش وهم موجودم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار خلد طرب داشته‌ست وضع خموش</p></div>
<div class="m2"><p>چها گشود به رویم لبی‌ که نگشودم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به رنگ سایه ز جمعیتم مگوی و مپرس</p></div>
<div class="m2"><p>گذشت عمر به خواب و دمی نیاسودم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو زخم صبح ندارم لب شکایت غیر</p></div>
<div class="m2"><p>همان تبسم خود می‌کند نمکسودم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز همرهان مدد پا نیافتم چو جرس</p></div>
<div class="m2"><p>هزار دشت به اقبال ناله پیمودم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هوس بضاعت سعی از دماغ می‌خواهد</p></div>
<div class="m2"><p>ز یأس دست و دلی داشتم به هم سودم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز زندگی چه نشاط آرزو کنم یارب</p></div>
<div class="m2"><p>چو عمر رفته سراپا زیان بی‌سودم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز عرض جسم‌ که ننگ شعور هستی بود</p></div>
<div class="m2"><p>به غیر خاک دگر بر عدم چه افزودم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو خواه شخص عدم‌ گوی خواه بیدل‌گیر</p></div>
<div class="m2"><p>در آن بساط‌ که چیزی نبود من بودم</p></div></div>