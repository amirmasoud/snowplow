---
title: >-
    غزل شمارهٔ ۱۴۰۱
---
# غزل شمارهٔ ۱۴۰۱

<div class="b" id="bn1"><div class="m1"><p>چه بوربا و چه مخمل حجاب می‌بافند</p></div>
<div class="m2"><p>به هر چه دیده گشادیم خواب می‌بافند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قماش ‌کسوت هستی نمی‌توان دریافت</p></div>
<div class="m2"><p>حریر وهم به موج سراب می‌بافند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نفس چه سحر طرازد به عرض راحت ما</p></div>
<div class="m2"><p>درین طلسم همین پیچ وتاب می‌بافند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز لاف ما و من ای بیخودان پوچ قماش</p></div>
<div class="m2"><p>کتان به کارگه ماهتاب می‌بافند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز تار و پود هجوم خطش مشو غافل</p></div>
<div class="m2"><p>که بهر فتنه‌‌ی آن چشم‌، خواب می بافند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به کارگاه نفس ره نبرده‌ای کانجا</p></div>
<div class="m2"><p>هزار ناله به یک رشته تاب می‌بافند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمند سعی جهان جز نفس درازی نیست</p></div>
<div class="m2"><p>چو عنکبوت سراسر لعاب می‌بافند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عبث به فکر قماش ثبات جامه مدر</p></div>
<div class="m2"><p>به عالمی‌که تویی انقلاب می‌بافند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به وهم خون شدهٔ‌کو چمن‌،‌کجاست بهار</p></div>
<div class="m2"><p>هنوز رنگ به طبع سحاب می‌بافند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز تیغ یار سر ما بلند شد بیدل</p></div>
<div class="m2"><p>به موج خیمهٔ ناز حباب می‌بافند</p></div></div>