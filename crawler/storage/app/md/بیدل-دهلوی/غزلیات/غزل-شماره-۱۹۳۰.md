---
title: >-
    غزل شمارهٔ ۱۹۳۰
---
# غزل شمارهٔ ۱۹۳۰

<div class="b" id="bn1"><div class="m1"><p>عمریست چون‌ گل می‌روم زین باغ حرمان در بغل</p></div>
<div class="m2"><p>از رنگ دامن برکمر، از بو گریبان در بغل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجنون و ساز بلبلان‌، لیلی و ناز گلستان</p></div>
<div class="m2"><p>من با دل داغ آشیان طاووس نالان در بغل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای اشکریزان عرق تدبیر عرض خلوتی</p></div>
<div class="m2"><p>مشت غبارم می‌رسد وضع پریشان در بغل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تنها نه من از حیرتش دارم نفس در دل‌ گره</p></div>
<div class="m2"><p>آیینه هم دزدیده است آشوب توفان در بغل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌آید آن لیلی نسب سرشار یک عالم طرب</p></div>
<div class="m2"><p>می در قدح تا کنج لب‌ گل تا گریبان در بغل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آه قیامت قامتم آسان نمی‌افتد ز پا</p></div>
<div class="m2"><p>این شعله هر جا سرکشد دارد نیستان در بغل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از غنچهٔ خاموش او ایمن مباش ای زخم دل</p></div>
<div class="m2"><p>کان فتنهٔ طوفان‌کمین دارد نمکدان در بغل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنیاد شمع از سوختن در خرمن‌ گل غوطه زد</p></div>
<div class="m2"><p>گر هست داغی در نظر داری گلستان در بغل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون صبح شور هستی‌ات کوک است با ساز عدم</p></div>
<div class="m2"><p>تا چندگردی از نفس اجزای بهتان در بغل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دارد زیانگاه جسد تشویش «‌حبل من مسد»</p></div>
<div class="m2"><p>زین کافرستان جسد بگریز ایمان در بغل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل ز ضبط گریه‌ام مژگان به خون دارد وطن</p></div>
<div class="m2"><p>تا چند باشد دیده‌ام از اشک پیکان در بغل</p></div></div>