---
title: >-
    غزل شمارهٔ ۶۲۶
---
# غزل شمارهٔ ۶۲۶

<div class="b" id="bn1"><div class="m1"><p>هرسو نگرم دیده به دیدار حجابست</p></div>
<div class="m2"><p>ای تار نظر پیرهنت این چه نقابست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خمیازهٔ شوق تو به می کم نتوان ‌کرد</p></div>
<div class="m2"><p>ما را به ‌قدح نسبت گرداب و حبابست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آستان نتوان چشم به پای تو نهادن</p></div>
<div class="m2"><p>این گل ثمر دیدهٔ بی‌خواب رکابست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای شمع حیا رنگ‌، عتاب آن همه مفروز</p></div>
<div class="m2"><p>هرجا شرر آیینه شود جلوه کبابست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غافل ز شکست دل عاشق نتوان بود</p></div>
<div class="m2"><p>معموری امکان به همین خانه خرابست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گیرم نشدم قابل پیمانهٔ رحمت</p></div>
<div class="m2"><p>آیینه یاسم چه کم از عالم آبست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرواز نیاید ز پرافشانی مژگان</p></div>
<div class="m2"><p>ای هیچ به کاری که نداری چه شتابست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما هیچکسان‌، بیهوده مغرور کمالیم</p></div>
<div class="m2"><p>گر ذره به افلاک پرد در چه حسابست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این میکده کیفیت دیدار که دارد</p></div>
<div class="m2"><p>هرجا مژه آغوش‌ کشد جام شرابست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>منعم دلش از بستر مخمل نشکیبد</p></div>
<div class="m2"><p>این سبزه خوابیده سراپا رگ خوابست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صد آبله پیمانه ده ریگ روانم</p></div>
<div class="m2"><p>پای طلبم ساقی مستان شرابست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یارب هوس شانهٔ گیسوی که دارد</p></div>
<div class="m2"><p>عمری‌ست ‌که شمشاد به خون خفتهٔ آبست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خاموشی آن لب به حیا داشت سوالی</p></div>
<div class="m2"><p>دادیم دل از دست و نگفتیم جوابست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیدل ز دویی چاره محال است درین بزم</p></div>
<div class="m2"><p>پرداز تو هم آینه چندان که نقابست</p></div></div>