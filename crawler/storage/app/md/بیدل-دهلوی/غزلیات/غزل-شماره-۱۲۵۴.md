---
title: >-
    غزل شمارهٔ ۱۲۵۴
---
# غزل شمارهٔ ۱۲۵۴

<div class="b" id="bn1"><div class="m1"><p>مخمل و دیبا حجاب هستی رسوا نشد</p></div>
<div class="m2"><p>چشم می‌پوشم‌ کنون پیراهنی پیدا نشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در فرامشخانهٔ امکان چه علم و کو عمل</p></div>
<div class="m2"><p>سعی باطل بود اینجا هر چه شد گویا نشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زآن حلاوتها که آداب محبت داشته‌ست</p></div>
<div class="m2"><p>خواستم نام لبش‌ گیرم لب از هم وانشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر وفا می‌کرد فرصتهای‌ کسب اعتبار</p></div>
<div class="m2"><p>از هوس من نیز چیزی می‌شدم اما نشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>انتظار مرگ شمع آسان نمی‌باید شمرد</p></div>
<div class="m2"><p>سر بریدن منفعل‌ گردید و یار ما نشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل به رنگ داغ ما را رخصت وحشت نداد</p></div>
<div class="m2"><p>شکر کن ای ناله پروازت قفس‌فرسا نشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر صید خلق در زهد ریایی جان مکن</p></div>
<div class="m2"><p>زین تکلف عالمی بی‌دین شد و دنیا نشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قانعان از خفت امداد یاران فارغند</p></div>
<div class="m2"><p>موج هرگز دستش از آب‌ گهر بالا نشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از دل دیوانهٔ ما مجلس‌آرایی مخواه</p></div>
<div class="m2"><p>سنگ سودا سوخت اما قابل مینا نشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آتش فکر قیامت در قفا افتاده است</p></div>
<div class="m2"><p>صد هزار امروز دی گردید و دی فردا نشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خاک ناگردیده رستن از شکست دل‌ کراست</p></div>
<div class="m2"><p>موی چینی بود این مو کز سر ما وانشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با زبان خلق‌ کار افتاد بیدل چاره چیست</p></div>
<div class="m2"><p>گوشه‌‌گیری‌های ما عنقا شد و تنها نشد</p></div></div>