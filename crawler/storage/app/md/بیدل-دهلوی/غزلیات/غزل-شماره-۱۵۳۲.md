---
title: >-
    غزل شمارهٔ ۱۵۳۲
---
# غزل شمارهٔ ۱۵۳۲

<div class="b" id="bn1"><div class="m1"><p>در بیابانی‌ که سعی بیخودی رهبر شود</p></div>
<div class="m2"><p>راه صد مطلب به یک لغزیدن پا، سر شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جزوها در عقده ی خودداری‌کل غافلند</p></div>
<div class="m2"><p>نقطه از ضبط عنان ‌گر بگذرد دفتر شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خشکی از طبع جهان آلودگی هم محوکرد</p></div>
<div class="m2"><p>لاف چشم تر توان زد دامنی گر تر شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر همه گوهر بود نومیدیست افسردگی</p></div>
<div class="m2"><p>از گرانباری مبادا کشتی‌ام لنگر شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فال آسودن ندارد خودگدازیهای من</p></div>
<div class="m2"><p>جمله پرواز است آن آتش که خاکستر شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عقدهٔ ‌کارت دلیل اعتبار دیگر است</p></div>
<div class="m2"><p>شاخ‌ گل چون غنچه آرد رشتهٔ‌ گوهر شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر شکست هر زیان تعمیر سودی بسته‌اند</p></div>
<div class="m2"><p>فربهی وقف غناگر آرزو لاغر شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چاره نتواند نهفتن راز ما خونین‌دلان</p></div>
<div class="m2"><p>زخم‌ گل از بخیهٔ شبنم نمایان‌تر شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاک حسرت برده ای دارم‌که مانند جرس</p></div>
<div class="m2"><p>ناله پیماید به‌جای باده، گر ساغر شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صاحب آیینه نتوان گشت بی‌قطع نفس‌</p></div>
<div class="m2"><p>بگذرد از زندگی تا .خضر، اسکندر شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وضع همواری ز ابنای زمان مطلوب ماست</p></div>
<div class="m2"><p>آدمیت‌گر نباشد هر که خواهد خر شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل آسان نیست کسب اعتبارات جهان</p></div>
<div class="m2"><p>سخت افسردن به‌خود بنددکه خاکی زر شود</p></div></div>