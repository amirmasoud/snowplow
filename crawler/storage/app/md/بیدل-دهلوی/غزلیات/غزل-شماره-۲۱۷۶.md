---
title: >-
    غزل شمارهٔ ۲۱۷۶
---
# غزل شمارهٔ ۲۱۷۶

<div class="b" id="bn1"><div class="m1"><p>نی سر تعمیر دل دارم نه تن می‌پرورم</p></div>
<div class="m2"><p>مشت خاکی را به ذوق خون شدن می‌پرورم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با نگاه دیدهٔ قربانیانم توأمی است</p></div>
<div class="m2"><p>بی‌نفس عمری‌ست خود را درکفن می‌پرورم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبر دارم تا کجا آتش به فریادم رسد</p></div>
<div class="m2"><p>تخم نومیدی سپندم سوختن می‌پرورم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سایه وار آسودگیهایم همان آوارگی‌ست</p></div>
<div class="m2"><p>تیره روزم شام غربت در وطن می‌پرورم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیرم و شرمم نمی‌آید ز افسون امل</p></div>
<div class="m2"><p>عبرتی در سایهٔ نخل‌کهن می‌پرورم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسته‌ام دل را به یاد چین‌ گیسوی‌ کسی</p></div>
<div class="m2"><p>در دماغ نافه‌ای فکر ختن می‌پرورم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اختیار گوشهٔ خاموشیم بیهوده نیست</p></div>
<div class="m2"><p>قدردان معنی‌ام ربط سخن می‌پرورم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی‌تماشایی نمی‌باشد تعلق زار جسم</p></div>
<div class="m2"><p>در قفس زین مشت پرگل در چمن می‌پرورم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اشک مجنون آبیار انتظار عبرتی‌ست</p></div>
<div class="m2"><p>می‌دمد لیلی نهالی را که من می‌پرورم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل این رنگی ‌که عریانی ز سازش‌ کم نبود</p></div>
<div class="m2"><p>در قیاس ناز آن‌ گل پیرهن می‌پرورم</p></div></div>