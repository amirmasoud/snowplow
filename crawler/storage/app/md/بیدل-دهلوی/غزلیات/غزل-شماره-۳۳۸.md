---
title: >-
    غزل شمارهٔ ۳۳۸
---
# غزل شمارهٔ ۳۳۸

<div class="b" id="bn1"><div class="m1"><p>تا از آن پای نگارین بوسه‌ای‌کرد انتخاب</p></div>
<div class="m2"><p>جام در موج شفق زد حلقهٔ چشم رکاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا به بحر شوق چون گرداب دارم اضطراب</p></div>
<div class="m2"><p>نیست نقش خاتم من جز نگین پیچ و تاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دهان بی‌نشانت هیچ نتوان دم زدن</p></div>
<div class="m2"><p>سوختم زین معنی موهوم خاموش جواب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جام گل را از می رنگت جگر چون لاله داغ</p></div>
<div class="m2"><p>وز نگاهت شیشهٔ می را نفس چو شبنم آب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صفحهٔ گلشن نبندد نقش رنگت در خیال</p></div>
<div class="m2"><p>ساغر نرگس نبیند نشئهٔ چشمت به خواب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خنده لبریز ملاحت‌، جلوه مالامال حسن</p></div>
<div class="m2"><p>ناز سرشار جفاها، غمزه مخمور عتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سایه‌پردازی تغافل‌های خورشید است و بس</p></div>
<div class="m2"><p>گر تو از رخ پرده برگیری که می‌گردد نقاب‌؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناله را آسوده نتوان دید در کیش وفا</p></div>
<div class="m2"><p>به که کم گردد دعای دردمندان مستجاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در گلستانی که رنگ از چهرهٔ من می‌ریختند</p></div>
<div class="m2"><p>گشت هر برگ خزان آیینه‌دار آفتاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا هوایی در سرم پیچید از خود می‌روم</p></div>
<div class="m2"><p>گردبادم دارم از سرگشتگی پا در رکاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شبنم لطف کریمان جهان برق است و بس</p></div>
<div class="m2"><p>غیر آتش نیست در سرچشمهٔ خورشید آب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عالم امن است حیرانی مژه بر هم زدن</p></div>
<div class="m2"><p>خانه‌ها ز افتادن دیوار می‌گردد خراب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>معجز خوبی نگر بیدل که هنگام سخن</p></div>
<div class="m2"><p>لعل خاموشش کشید از غنچهٔ گوهر گلاب</p></div></div>