---
title: >-
    غزل شمارهٔ ۱۶۸۷
---
# غزل شمارهٔ ۱۶۸۷

<div class="b" id="bn1"><div class="m1"><p>دام ز سیر گلشن اسباب در نظر</p></div>
<div class="m2"><p>رنگی‌ که شعله می‌زندم آب در نظر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون شد دل ازتکلف اسباب زندگی</p></div>
<div class="m2"><p>یک لفظ پوچ و آن همه اعراب در نظر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مخمل نه‌ایم ولیک ز غفلت نصیب ماست</p></div>
<div class="m2"><p>بیداریی‌که نیست به جز خواب در نظر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در وادی طلب که سراب است چشمه‌اش</p></div>
<div class="m2"><p>اشکی مگر نشان دهدم آب در نظر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همواری از طبیعت روشن نمی‌رود</p></div>
<div class="m2"><p>تار نگاه را نبود تاب در نظر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گلها چوشبنمت به سروچشم جا دهند</p></div>
<div class="m2"><p>گر باشدت رعایت آداب در نظر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر خویش هم در حسدت باز می‌شود</p></div>
<div class="m2"><p>گرگل کند حقیقت احباب در نظر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یارب صداع غفلت ما را علاج چیست</p></div>
<div class="m2"><p>مخموری خیال و می ناب در نظر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>موهومی حقیقت ما را نموده‌اند</p></div>
<div class="m2"><p>چون نقطهٔ دهان تو نایاب در نظر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیگر ز سایهٔ دم تیغت‌کجا رویم</p></div>
<div class="m2"><p>سرها سجود مایل محراب در نظر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غافل مشو که انجمن اعتبارها</p></div>
<div class="m2"><p>ویرانه‌ای‌ست وحشت سیلاب در نظر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آسوده‌ایم درکف خاکستر امید</p></div>
<div class="m2"><p>بیدل‌کراست بستر سنجاب در نظر</p></div></div>