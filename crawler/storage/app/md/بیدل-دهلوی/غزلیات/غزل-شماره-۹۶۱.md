---
title: >-
    غزل شمارهٔ ۹۶۱
---
# غزل شمارهٔ ۹۶۱

<div class="b" id="bn1"><div class="m1"><p>قضا تا نقش بنیاد من بیکار می‌بندد</p></div>
<div class="m2"><p>حنا می‌آرد و در پنجهٔ معمار می‌بندد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز چاک سینه بی‌روی تو هرجا می‌کشم آهی</p></div>
<div class="m2"><p>سحر شور قیامت بر سرم دستار می‌بندد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر شرم خیالت نقش بر آبی تواند زد</p></div>
<div class="m2"><p>سراپایم عرق آیینهٔ دیدار می‌بندد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بساط عبرت این انجمن آیینه‌ای دارد</p></div>
<div class="m2"><p>که تا مژگان بهم آورده‌ای زنگار می‌بندد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمی‌دانم به یاد او چسان از خود برون آیم</p></div>
<div class="m2"><p>دل سنگین به دوش ناله‌ام کهسار می‌بندد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آن محفل‌که من حیرت‌کمین جلوهٔ اویم</p></div>
<div class="m2"><p>فروغ شمع هم آیینه بر دیوار می‌بندد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به رعنایی چو شمع‌ ازآفت شهرت مباش ایمن</p></div>
<div class="m2"><p>رگ ‌گردن ز هر عضوت سری بر دار می‌بندد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه دارد قابلیت جز می تکلیف پیمودن</p></div>
<div class="m2"><p>در این محفل همین دوشم به دوشم بار می‌بندد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زمان فرصت ربط نفس با دل غنیمت دان</p></div>
<div class="m2"><p>کزین تار این‌ گره چون باز شد دشوار می‌بندد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اسیر مشرب موجم‌ کزان مطلق عنانیها</p></div>
<div class="m2"><p>گرش تکلیف برگشتن کنی زنّار می‌بندد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به ‌مخموری ‌ز سیر این ‌چمن غافل‌ مشو بیدل</p></div>
<div class="m2"><p>که خجلت در به روی هر که شد مختار می‌بندد</p></div></div>