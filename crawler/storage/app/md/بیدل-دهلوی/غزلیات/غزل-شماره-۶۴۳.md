---
title: >-
    غزل شمارهٔ ۶۴۳
---
# غزل شمارهٔ ۶۴۳

<div class="b" id="bn1"><div class="m1"><p>نسخهٔ آرام دل در عرض آهی ابترست</p></div>
<div class="m2"><p>غنچه‌ها را خامشی شیرازهٔ بال و پرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچکس را حاصل جمعیت ازاسباب نیست</p></div>
<div class="m2"><p>بحر را هم موج بیتابی زجوش‌ گوهرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باید از هستی به تمثالی قناعت‌کردنت</p></div>
<div class="m2"><p>میهمان خانهٔ آیینه بیرون درست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس‌که دارد شور آهنگ مخالف روزگار</p></div>
<div class="m2"><p>هرکه می‌آید در اینجا طالب‌گوش‌کرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اعتبار ما به خود واماندگان آشفتگی‌ست</p></div>
<div class="m2"><p>خاک اگر آیینه می‌گردد غبارش جوهرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آفتاب طالع ما داغ حرمان است و بس</p></div>
<div class="m2"><p>آسمان تیره‌بختی ها سویدا اخترست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بعد مرگ، اجزای ما، توفانی موج هواست</p></div>
<div class="m2"><p>تا نپنداری‌که ما را خاک‌گشتن لنگرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشرت آهنگی ز بزم میکشان غافل مباش</p></div>
<div class="m2"><p>آشیان رنگ اگر بی‌پرده گردد ساغرست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاک اگر باشم به راهت جوهر آیینه‌ام</p></div>
<div class="m2"><p>ور همه آیینه ‌گردم بی‌تو خاکم بر سرست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسکه شد خشک ازتب‌ گرم محبت پیکرم</p></div>
<div class="m2"><p>همچو اخگر بر جبین من عرق خاکسترست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عمرها شد می‌روم از خویش و بر جایم هنوز</p></div>
<div class="m2"><p>گرد تمکین خرامت موج آب‌ گوهرست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شور عشقت آنقدر راحت فروش افتاده است</p></div>
<div class="m2"><p>کز تپش تا نالهٔ بیمار صاحب بسترست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آب تیغت تا نگردد صندل آرامها</p></div>
<div class="m2"><p>کی‌ شود این‌ نکته‌ات‌ روشن‌ که سر دردسرست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چشم و گوشی را که بیدل نیست فیض عبرتی</p></div>
<div class="m2"><p>در تماشاگاه معنی روزن بام و درست</p></div></div>