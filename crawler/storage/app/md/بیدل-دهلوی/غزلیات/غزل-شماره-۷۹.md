---
title: >-
    غزل شمارهٔ ۷۹
---
# غزل شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>شوق اگر بی‌پرده سازد حسرت مستور را</p></div>
<div class="m2"><p>عرض یک‌خمیازه صحرا می‌کند مخمور را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درد دل در پردهٔ محویتم خون می‌خورد</p></div>
<div class="m2"><p>از تحیر خشک بندی‌کرده‌ام ناسور را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چاره‌سازان در صلاح کار خود بیچاره‌اند</p></div>
<div class="m2"><p>به نسازد موم، زخم خانهٔ زنبور را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما ضعیفان را ملایم طینتی دام بلاست</p></div>
<div class="m2"><p>مشکل است از روی خاکسترگذشتن مور را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زندگانی شیوهٔ عجز است باید پیش برد</p></div>
<div class="m2"><p>نیست سر دزدیدن ازپشت دوتا مزدور را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشرتی‌گر نیست می‌باید به کلفت ساختن</p></div>
<div class="m2"><p>درد هم‌صاف است بهرسرخوشی‌مخموررا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غفلت سرشار مستغنی‌ست از اسباب جهل</p></div>
<div class="m2"><p>خواب گو مژگان نبندد دیده‌های کور را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در نظر داریم مرگ و از امل فارغ نه‌ایم</p></div>
<div class="m2"><p>پیش پا دیدن نشد مانع خیال دور را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اعتبار درد عشق از وصل برهم می‌خورد</p></div>
<div class="m2"><p>زنگ باشد التیام آیینهٔ ناسور را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زندگی وحشی‌ست از ضبط نفس غافل مباش</p></div>
<div class="m2"><p>بوی‌، آرامیده دارد در قفس‌کافور را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در تنعم ذکر احسانها بلند آوازه نیست</p></div>
<div class="m2"><p>چینی خالی مگر یادی‌کند فغفور را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل از اندیشهٔ اوهام باطل سوختم</p></div>
<div class="m2"><p>بر سر داغم فشان خاکستر منصور را</p></div></div>