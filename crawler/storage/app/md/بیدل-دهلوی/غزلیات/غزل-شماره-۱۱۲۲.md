---
title: >-
    غزل شمارهٔ ۱۱۲۲
---
# غزل شمارهٔ ۱۱۲۲

<div class="b" id="bn1"><div class="m1"><p>غنا مفت هوس‌گر نام آسودن نمی‌گیرد</p></div>
<div class="m2"><p>غبار دامن‌افشان سحر دامن نمی‌گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فسردن خوشترست از منت شوراندن آتش</p></div>
<div class="m2"><p>حنا بوسدکف دستی‌که دست من نمی‌گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلی دارم ادب‌ پروردهٔ ناموس یکتایی</p></div>
<div class="m2"><p>که از شرم محبت خرده بر دشمن نمی‌گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز تشویش علایق رسته‌گیر آزادطبعان را</p></div>
<div class="m2"><p>عنان آب‌، دام سعی پرویزن نمی‌گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ره فهم تجرد، فطرت باریک می‌خواهد</p></div>
<div class="m2"><p>کسی جز رشته آب از چشمهٔ سوزن نمی‌گیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حضور عافیت‌گر مقصد سعی طلب باشد</p></div>
<div class="m2"><p>چرا همّت‌، ره از پا درافتادن نمی‌گیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ضعیفی در چه خاک افکنده باشد دام من یارب</p></div>
<div class="m2"><p>که صیٌاد از حیا، عمریست نام من نمی‌گیرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تواضع‌کیش همّت را چه امکان است رعنایی</p></div>
<div class="m2"><p>خم دوش فلک‌، بار سر و گردن نمی‌گیرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دم پیری ز فیض گریه خلقی می‌رود غافل</p></div>
<div class="m2"><p>در این‌ مهتاب‌ شیری‌ هست‌ و کس ‌روغن نمی‌گیرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قماشی از حیا دارد قبای نازک‌اندامی</p></div>
<div class="m2"><p>که بوی یوسف ازشوخی به پیراهن نمی‌گیرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر شمع رخش صد انجمن روشن‌ کند بیدل</p></div>
<div class="m2"><p>تحیر آتشی دارد که جز در من نمی‌گیرد</p></div></div>