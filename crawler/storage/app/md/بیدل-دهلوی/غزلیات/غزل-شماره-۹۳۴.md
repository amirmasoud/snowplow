---
title: >-
    غزل شمارهٔ ۹۳۴
---
# غزل شمارهٔ ۹۳۴

<div class="b" id="bn1"><div class="m1"><p>نه با سازهوس جوشد نه برکسب هنرپیچد</p></div>
<div class="m2"><p>طبیعت چون رسا افتد به معنی بیشتر پیچد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به این آشفتگی ما را کجا راحت چه جمعیت</p></div>
<div class="m2"><p>هوای طره‌ات جای نفس بر دل مگر پیچد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گمان حلقهٔ دام است آن صید نزاکت را</p></div>
<div class="m2"><p>گر از چشم منش تار نگاهی بر کمر پیچد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز اسباب هوس بر هر چه پیچی فال کلفت زن</p></div>
<div class="m2"><p>گره پیدا کند در هر کجا نی بر شکر پیچد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شب امید طی شد وقت آن آمد که نومیدی</p></div>
<div class="m2"><p>غبار ما ضعیفان هم به دامان سحر پیچد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جنونم داغ شد در کسوت ناموس خودداری</p></div>
<div class="m2"><p>گریبانی چو گل دامن کنم تا بر کمر پیچد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>امید عافیت گر هست از تیغ است بسمل را</p></div>
<div class="m2"><p>غریق بحر الفت به که بر موج خطر پیچد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز سامان تعلقها پریشانی غنیمت دان</p></div>
<div class="m2"><p>همه دام است اگر این رشته‌ها بر یکدگر پیچد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نزاکت‌گاه نازکیست یارب کلک تصویرم</p></div>
<div class="m2"><p>دو عالم رنگ‌ گرداند سر مویی اگر پیچد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به رنگ شمع مجنون‌ گرفتار دلی دارم</p></div>
<div class="m2"><p>که زنجیرش گر از پا واکنی چون مو به سر پیچد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به انداز خرام او مباد از خودروی بیدل</p></div>
<div class="m2"><p>که ترسم‌ گردش رنگت عنان ناز درپیچد</p></div></div>