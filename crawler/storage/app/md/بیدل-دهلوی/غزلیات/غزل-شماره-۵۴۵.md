---
title: >-
    غزل شمارهٔ ۵۴۵
---
# غزل شمارهٔ ۵۴۵

<div class="b" id="bn1"><div class="m1"><p>ای‌کعبه جو یقینی اگرکار بستن است</p></div>
<div class="m2"><p>احرام بستنت همه زنار بستن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر محرمی علم نفرازی یه حرف پوچ</p></div>
<div class="m2"><p>این پنبه پرچمی‌ست‌که بر دار بستن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باید به خون هر دو جهان دست شستنت</p></div>
<div class="m2"><p>مشاطه‌گر حنا به‌کف یار بستن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون سایه عالمی‌ست به زیر نگین ما</p></div>
<div class="m2"><p>گر سر به دوش جبههٔ هموار بستن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عبرت زکارگاه عمل موج می‌زد</p></div>
<div class="m2"><p>ساز شکسته را چقدر تار بستن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منگر به لفظ و معنی‌ام ازکم‌بضاعتی</p></div>
<div class="m2"><p>تنگی برای قیافه‌تکرار بستن است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای صرصر انتظار چراغان اعتبار</p></div>
<div class="m2"><p>درهاگشوده‌ای‌که به یک بار بستن است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سست است بار قافلهٔ عافیت هنوز</p></div>
<div class="m2"><p>پر بسته‌ایم نوبت منقار بستن است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پر نامجو مباش‌که نقش نگین عجز</p></div>
<div class="m2"><p>پیشانی شکسته به دیوار بستن است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در خاکدان دهر مچین دستگاه ناز</p></div>
<div class="m2"><p>گر بر سر مزار چه دستار بستن است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل مباش‌ غرهٔ تحصیل مدعا</p></div>
<div class="m2"><p>در مزرعی‌که خوشه همان بار بستن است</p></div></div>