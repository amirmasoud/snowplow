---
title: >-
    غزل شمارهٔ ۳۰۷
---
# غزل شمارهٔ ۳۰۷

<div class="b" id="bn1"><div class="m1"><p>چو اشک آن کس که می‌چیند گل عیش از تپیدن‌ها</p></div>
<div class="m2"><p>بود دلتنگ اگر گوهر شود از آرمیدن‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بس عام است در وحشت‌سرای دهر بی‌تابی</p></div>
<div class="m2"><p>دل هر ذره دارد در قفس چندین تپیدن‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجو آوازهٔ شهرت ز آهنگ سبک‌روحان</p></div>
<div class="m2"><p>صدای بال مرغ رنگ نبود در پریدن‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگه در دیدهٔ حیران ما شوخی نمی‌داند</p></div>
<div class="m2"><p>به رنگ چشم شبنم درد این میناست دیدن‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوتا کردیم آخر خوبش را در خدمت ببری</p></div>
<div class="m2"><p>رسانیدیم بار زندگانی تا خمیدن‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز رونق باز می‌ماند چو مینا شد ز می خالی</p></div>
<div class="m2"><p>شکست رنگ ظاهر می‌شود در خون کشیدن‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا از پیچ وتاب گردباد این نکته شد روشن</p></div>
<div class="m2"><p>که در را طلب معراج دامان است چیدن‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز قطع الفت دل‌ها حسود آسوده ننشیند</p></div>
<div class="m2"><p>شود خمیازهٔ مقراض افزون در بریدن‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گداز درد نومیدی تماشای دگر دارد</p></div>
<div class="m2"><p>به رنگ اشک ناسورم نظرباز چکیدن‌ها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حباب از موج هرگز صرفهٔ طاقت نمی‌بیند</p></div>
<div class="m2"><p>ز بال ما گره وامی‌کند آخر تپیدن‌ها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز هستی گر برون تازی عدم در پیش می‌آید</p></div>
<div class="m2"><p>درین وادی مقامی نیست غیر از نارسیدن‌ها</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مجو از طفل‌خویان‌، فطرت آزادگان بیدل</p></div>
<div class="m2"><p>به پرواز نگه کی سرسا اشک از دویدن‌ها</p></div></div>