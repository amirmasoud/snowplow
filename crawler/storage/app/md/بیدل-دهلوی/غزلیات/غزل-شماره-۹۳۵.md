---
title: >-
    غزل شمارهٔ ۹۳۵
---
# غزل شمارهٔ ۹۳۵

<div class="b" id="bn1"><div class="m1"><p>حسرتی در دل از آن لاله قبا می‌پیچد</p></div>
<div class="m2"><p>که چودستار چمن بر سر ما می‌پیچد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبض هستی چقدرگرم تپش پیمایی‌ست</p></div>
<div class="m2"><p>موی آتش زده بر خویش چها می‌پیچد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نفس هست حباب من و جولان هوس</p></div>
<div class="m2"><p>نیست آرام سری را که هوا می‌پیچد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه زمین و چه فلک ‌گوشهٔ زندان دل است</p></div>
<div class="m2"><p>ششجهت ‌کلفت این تنگ فضا می‌پیچد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نالهٔ ما به چه تدبیر تواند برخاست</p></div>
<div class="m2"><p>همچو نی صد گره اینجا به عصا می‌پیچد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناتوانی ‌که به جز مرگ ندارد سپری</p></div>
<div class="m2"><p>به چه امید سر از تیغ قضا می‌پیچد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>استخوان‌بندی اوهام ز بس بی‌مغز است</p></div>
<div class="m2"><p>آرزوها همه بر بال هما می‌پیچد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صورخیزست ندامت ز شکست دل ما</p></div>
<div class="m2"><p>که بساط دو جهان را به صدا می‌پیچد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عبرت مرگ کسان سلسلهٔ خجلت ماست</p></div>
<div class="m2"><p>رشته از هرکه شود باز به ما می‌پیچد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قدرت افسانهٔ ابرام نخواهد بیدل</p></div>
<div class="m2"><p>نفس ازبی‌اثریها به دعا می‌پیچد</p></div></div>