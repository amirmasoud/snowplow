---
title: >-
    غزل شمارهٔ ۲۳۸۱
---
# غزل شمارهٔ ۲۳۸۱

<div class="b" id="bn1"><div class="m1"><p>حرفم همه از مغز است از پوست نمی‌گویم</p></div>
<div class="m2"><p>آن را که به جز من نیست من اوست نمی‌گویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اسرار کماهی را تأویل نمی‌باشد</p></div>
<div class="m2"><p>سر را سر و پا را پا، زانوست نمی‌گویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ظرفست به هر صورت آیینهٔ استعداد</p></div>
<div class="m2"><p>درکوزه اگر آبست در جوست نمی‌گویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>معنی نظران دورند از وهم غلط فهمی</p></div>
<div class="m2"><p>نارنج ذقن سیب است لیموست نمی‌گویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عیب و هنر این بزم افشاگر اسرار است</p></div>
<div class="m2"><p>هر چندگل چشم است بی‌بوست نمی‌گویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من در به ‌در انصاف از فعل خود آگاهم</p></div>
<div class="m2"><p>گر غیر بدم ‌گوید بدگوست نمی‌گویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر صفحهٔ آفاقست یا آینهٔ فلاک</p></div>
<div class="m2"><p>تا پشت و رخی دارد یکروست نمی گویم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جاه و حشم دنیا ننگ است ز سر تا پا</p></div>
<div class="m2"><p>چینی چو سر فغفور بیموست نمی‌گویم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لبریز فنا باید تا دل همه را شاید</p></div>
<div class="m2"><p>ناگشته تهی از خود مملوست نمی‌گویم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر شبههٔ تحقیقم زین دشت سیاهی‌کرد</p></div>
<div class="m2"><p>لیلی به نظر دارم آهوست نمی‌گویم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آیین محبت نیست سودای دویی پختن</p></div>
<div class="m2"><p>من بیدل خود را هم جز دوست نمی‌گویم</p></div></div>