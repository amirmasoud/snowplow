---
title: >-
    غزل شمارهٔ ۲۶۲۳
---
# غزل شمارهٔ ۲۶۲۳

<div class="b" id="bn1"><div class="m1"><p>برآرد گَرَم آتش‌ دل زبانه</p></div>
<div class="m2"><p>شودگرد بال سمندر زمانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گشایم‌گر از بیخودی شست آهی</p></div>
<div class="m2"><p>کنم قبهٔ چرخ زنبور خانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به صد لاف وارستگی صید خویشم</p></div>
<div class="m2"><p>نبرده‌ست پروازم از آشیانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چراغ ادبگاه بزم خیالم</p></div>
<div class="m2"><p>نمی‌بالد از آتش من زبانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درین دشت خلقی زخود رفت اما</p></div>
<div class="m2"><p>ندانست سر منزلی هست یا نه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فلک نقش نام که خواهد نشاندن</p></div>
<div class="m2"><p>به این خ‌اتم صد نگین در میانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صدف‌وار تا یک گهر اشک داری</p></div>
<div class="m2"><p>ازبن آسیاها مجو آب و دانه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دو روزی‌کزین ما و من مست نازی</p></div>
<div class="m2"><p>به خواب عدم ‌گفته باشی فسانه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کف پوچ مغزی مکن فکردریا</p></div>
<div class="m2"><p>که هر جا تویی نیست غیر از کرانه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قیامت خرو شست بنیاد امکان</p></div>
<div class="m2"><p>ازین ساز نیرنگ انسان ترانه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دمیده‌ست از آب منی مشت خاکی</p></div>
<div class="m2"><p>به صد سخت جانی چو سنگ از مثانه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>محال است پروازت از دام زلفش</p></div>
<div class="m2"><p>اگر جمله تن بال‌گردی چو شانه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به پی‌ری‌کشیدیم رنج جوانی</p></div>
<div class="m2"><p>سحر می‌کند گل خمار شبانه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر گشت باغ است و گر سیر صحرا</p></div>
<div class="m2"><p>روانیم از خود به چندین بها نه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>غبار جسد چشم بند است بیدل</p></div>
<div class="m2"><p>چو دیوارت افتاد صحراست خانه</p></div></div>