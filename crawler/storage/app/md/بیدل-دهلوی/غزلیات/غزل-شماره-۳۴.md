---
title: >-
    غزل شمارهٔ ۳۴
---
# غزل شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>جز پیش ما مخوانید افسانهٔ فنا را</p></div>
<div class="m2"><p>هرکس نمی‌شناسد آواز آشنا را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از طاق و قصر دنیاکز خاک وخشت چینید</p></div>
<div class="m2"><p>حیف‌است پست‌گیرید معراج پشت پا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم طمع مدوزید برکیسهٔ خسیسان</p></div>
<div class="m2"><p>باورنمی‌توان داشت سگ نان دهد گدا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روزی‌دو زین بضاعت‌مردن کفیل‌هستی‌ست</p></div>
<div class="m2"><p>برگ معاش ماکرد تقدیرخونبها را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در چشم‌کس‌ نمانده‌ست‌گنجایش مروت</p></div>
<div class="m2"><p>زین خانه‌ها چه مقدار تنگی‌گرفت جا را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دستبرد حاجت نم در جبین نداریم</p></div>
<div class="m2"><p>آخرهجوم مطلب شست ازعرق حیا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز نشئهٔ تجرد شایستهٔ جنون نیست</p></div>
<div class="m2"><p>صرف بهار ماکن رنگی زگل جدا را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا زنده‌ایم باید در فکر خویش مردن</p></div>
<div class="m2"><p>گردون بی‌مروت برماگماشت ما را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آهم‌ز نارسایی شد اشک و با عرق ساخت</p></div>
<div class="m2"><p>پستی‌ست‌گر خجالت شبنم‌کند هوا را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیکاری آخرکار دست مرا به خون بست</p></div>
<div class="m2"><p>رنگین نمی‌توان‌کرد زین بیشتر حنا را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دست در آستینم بی‌دامن غنا نیست</p></div>
<div class="m2"><p>صبح است با اجابت نامحرم دعا را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از هرکه خواهی امداد اول تلافی‌اش‌کن</p></div>
<div class="m2"><p>دستی گر نداری زحمت مده عصا را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خاک زمین آداب‌گر پی سپر توان‌کرد</p></div>
<div class="m2"><p>ای تخم آدمیت بر سرگذار پا را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هنگام شیب بیدل‌کفر است شعله‌خویی</p></div>
<div class="m2"><p>محراب‌کبر نتوان‌کردن قد دوتا را</p></div></div>