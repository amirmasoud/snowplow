---
title: >-
    غزل شمارهٔ ۱۵۳۰
---
# غزل شمارهٔ ۱۵۳۰

<div class="b" id="bn1"><div class="m1"><p>گر چنین بخت نگون عبرت ‌کمین پیدا شود</p></div>
<div class="m2"><p>هر قدر سر بر فلک سایم زمین پیدا شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچکس محرم نوای سرنوشت شمع نیست</p></div>
<div class="m2"><p>جای خط یارب زبانم از جبین پیدا شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در گلستانی‌ که خواند اشک من سطر نمی</p></div>
<div class="m2"><p>سایهٔ‌ گل تا ابد ابرآفرین پیدا شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دامن وحشت ز سیر این چمن نتوان شکست</p></div>
<div class="m2"><p>دیده مژگان برهم افشارد که چین پیدا شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن‌سوی خویشت چه عقبا و چه دنیا هیچ نیست</p></div>
<div class="m2"><p>بگذر از خود تا نگاهی پیش‌بین پیدا شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بازگرداند عنان جهد عیش رفته را</p></div>
<div class="m2"><p>موم اگر از آب‌گشتن انگبین پیدا شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسکه بی رویت در این‌کهسار جانهاکنده‌ام</p></div>
<div class="m2"><p>هرکجا نامم بری نقش نگین پیدا شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناله تا دستی‌ کند در یاد دامانت بلند</p></div>
<div class="m2"><p>چون نیستانم ز هر عضو آستین پیدا شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عالم آب است دشت و در ز شرم سجده‌ام</p></div>
<div class="m2"><p>بی‌عرق‌ گردد جبینم تا زمین پیدا شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در تماشاگاه امکان آنچه ما گم کرده‌ایم</p></div>
<div class="m2"><p>بیدل آخر از نگاه واپسین پیدا شود</p></div></div>