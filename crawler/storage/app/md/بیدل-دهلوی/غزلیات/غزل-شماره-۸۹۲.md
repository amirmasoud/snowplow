---
title: >-
    غزل شمارهٔ ۸۹۲
---
# غزل شمارهٔ ۸۹۲

<div class="b" id="bn1"><div class="m1"><p>از بس‌که خورده‌ام به خم زلف یار پیچ</p></div>
<div class="m2"><p>طومار ناله‌ام همه جا رفته مارپیچ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زال فلک طلسم امل خیز هستی‌ام</p></div>
<div class="m2"><p>بسته ‌است چون ‌کلاوه به چندین هزار پیچ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای غافل از خجالت صیادی هوس</p></div>
<div class="m2"><p>رو عنکبوت‌وار هوا را به تار پیچ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش ازتو ذوق جانکنیی داشت‌کوهکن</p></div>
<div class="m2"><p>چندی تو هم چو ناله درین ‌کوهسار پیچ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امید در قلمرو بیحاصلی رساست</p></div>
<div class="m2"><p>از هر چه هست بگسل و در انتظار پیچ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رنج جهان به همت مردانه راحت است</p></div>
<div class="m2"><p>گر بار می‌کشی ‌کمرت استوار پیچ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر یک جهان امل دم پیری چه می‌تنی</p></div>
<div class="m2"><p>دستار صبح به‌ که بود اختصار پیچ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>افسرده‌ گیر شعلهٔ موهومی نفس</p></div>
<div class="m2"><p>دود دلی‌ که نیست به شمع مزار پیچ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>موجی‌که صرف‌ کار گهر گشت‌گوهر است</p></div>
<div class="m2"><p>سرتا به پای خود به سراپای یار پیچ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صد خواب‌ناز تشنهٔ ضبط‌حواس توست</p></div>
<div class="m2"><p>بر خویش غنچه‌ گرد و لحاف بهار پیچ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل مباش منفعل جهد نارسا</p></div>
<div class="m2"><p>این یک نفس عنان ز ره اختیار پیچ</p></div></div>