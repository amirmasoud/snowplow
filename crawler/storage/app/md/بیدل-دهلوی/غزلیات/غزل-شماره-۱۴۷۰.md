---
title: >-
    غزل شمارهٔ ۱۴۷۰
---
# غزل شمارهٔ ۱۴۷۰

<div class="b" id="bn1"><div class="m1"><p>هرکه زین انجمن آثار صفا می‌بیند</p></div>
<div class="m2"><p>نشئه از باده و از تار صدا می‌بیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روغن از پردهٔ بادام تواند دیدن</p></div>
<div class="m2"><p>هرکه از نرگس مست تو ادا می‌بیند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست رنگین ز حنا ناخن پایت‌که بهار</p></div>
<div class="m2"><p>طلعت خویش در این آینه‌ها می‌بیند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه خطاها که ندارد اثر کج‌نظری</p></div>
<div class="m2"><p>سرو را احول معذور دوتا می‌بیند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در مقامی که تماشا اثر بیرنگی‌ست</p></div>
<div class="m2"><p>چشم پوشیده به معنی همه را می‌بیند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این غروری‌ که به خلوتگه یکتایی اوست</p></div>
<div class="m2"><p>گر همه آینه‌ گردیم‌ کجا می‌بیند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از خم کاکل او فکر رهایی غلط است</p></div>
<div class="m2"><p>شانه هم دست خود آنجا به قفا می‌بیند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جلوهٔ شخص ز تمثال عیان‌ست اینجا</p></div>
<div class="m2"><p>از تو غافل نبود هرکه مرا می‌بیند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شش‌جهت آب شد و آینه‌ای ساز نکرد</p></div>
<div class="m2"><p>حسن یارب چقدر عرض حیا می‌بیند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غیر در عالم تحقیق ندارد اثری</p></div>
<div class="m2"><p>بیدل آیینهٔ ما صورت ما می‌بیند</p></div></div>