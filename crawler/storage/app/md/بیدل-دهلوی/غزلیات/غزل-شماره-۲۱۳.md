---
title: >-
    غزل شمارهٔ ۲۱۳
---
# غزل شمارهٔ ۲۱۳

<div class="b" id="bn1"><div class="m1"><p>هم آبله هم چشم پر آب است دل ما</p></div>
<div class="m2"><p>پیمانهٔ صد رنگ شراب است دل ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غافل نتوان بود ازین منتخب راز</p></div>
<div class="m2"><p>هشدارکه یک نقطه‌کتاب است دل ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باغی‌که بهارش همه سنگ است دل اوست</p></div>
<div class="m2"><p>دشتی‌که غبارش همه آب است دل ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما خاک ز جا بردهٔ سیلاب جنونیم</p></div>
<div class="m2"><p>سرمایهٔ صدخانه خراب است دل ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیراهن ما کسوت عریانی دریاست</p></div>
<div class="m2"><p>یک پرده تنکتر ز حباب است دل ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در بزم‌وصالت‌که حیا جام به‌دست است</p></div>
<div class="m2"><p>گر آب شود بادهٔ ناب است دل ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منظوربتان هرکه‌شود حسرتش از ماست</p></div>
<div class="m2"><p>یار آینه می‌بیند وآب است دل ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا آینه باقی‌ست همان‌عکس جمال است</p></div>
<div class="m2"><p>ای یأس خروشی‌که نقاب است دل ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا چشم‌گشودیم به خویش آینه دیدیم</p></div>
<div class="m2"><p>دریاب‌که تعبیر چه خواب است دل ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای آه اثر باخته آتش نفسی چند</p></div>
<div class="m2"><p>خون شوکه زدست توکباب است دل ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یارب نکشد خجلت محرومی دیدار</p></div>
<div class="m2"><p>عمری‌ست‌که آیینه خطاب است دل ما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آیینه همان چشمهٔ توفان خیالی‌ست</p></div>
<div class="m2"><p>بیدل چه توان‌کرد سراب است دل ما</p></div></div>