---
title: >-
    غزل شمارهٔ ۱۴۶۰
---
# غزل شمارهٔ ۱۴۶۰

<div class="b" id="bn1"><div class="m1"><p>حسرت دل‌کرد بر ما پنجهٔ قاتل بلند</p></div>
<div class="m2"><p>می‌شود دست‌کرم با نالهٔ سایل بلند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما نه‌ تنها نیستی را دادرس فهمیده‌ایم</p></div>
<div class="m2"><p>بحر هم از موج دارد دست‌ بر ساحل بلند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چین ابروی تو هرجا بحث جوهر می‌کند</p></div>
<div class="m2"><p>تیغ از جوهر رگ ‌گردن ‌کند مشکل بلند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سایهٔ تمکین نازت هر کجا افتاده است</p></div>
<div class="m2"><p>سبزه چون مژگان شود از خاک آن منزل بلند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>‌نه فلک در جلوه آمد از تپیدنهای دل</p></div>
<div class="m2"><p>تاکجا رفته‌ست یارب‌ گرد این بسمل بلند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کاروان یاس امکان را غبار حسرتم</p></div>
<div class="m2"><p>هرکه رفت از خویشتن‌، کرد آتشم در دل بلند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حرز امنی نیست جز محرومی از نشو و نما</p></div>
<div class="m2"><p>خوشه‌سان گردن مکش زین کشت بیحاصل بلند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حیرت ‌آهنگیم دل از شکوه ما جمع دار</p></div>
<div class="m2"><p>دود نتواند شدن از شمع این محفل بلند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با غرور نازاو مشکل برآید عجز ما</p></div>
<div class="m2"><p>گرد مجنون نارسا و دامن محمل بلند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سدّ راه توست بیدل گر کنی تعمیر جسم</p></div>
<div class="m2"><p>می‌شود دیوار چون شد قدری آب وگل بلند</p></div></div>