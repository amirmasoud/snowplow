---
title: >-
    غزل شمارهٔ ۲۵۰۱
---
# غزل شمارهٔ ۲۵۰۱

<div class="b" id="bn1"><div class="m1"><p>ز پابوسش بهار عشرت جاوید سامان‌کن</p></div>
<div class="m2"><p>چمن تا در برت غلتد حنایی را گریبان ‌کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اثر پروردهٔ یاد نگاه اوست اجزایم</p></div>
<div class="m2"><p>ز خاکم سرمه‌کش در دیده و عریان غزالان ‌کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به‌ تمثال حباب از بحر تا کی منفعل باشی</p></div>
<div class="m2"><p>دویی‌تا محو گردد خانهٔ آیینه ویران‌کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درین گلشن‌ که بال افشانی رنگست بنیادش</p></div>
<div class="m2"><p>توهم آشیانی در نوای عندلیبان کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غبارت چون سحر در بال عنقا آشیان دارد</p></div>
<div class="m2"><p>به ذوق امتحان رنگی اگر داری پر افشان‌کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به ‌شور ما و من تا چند جوشد شوخی موجت</p></div>
<div class="m2"><p>دمی در جیب خاموشی نفس دزیده توفان‌ کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صفای عافیت تشویش صیقل برنمی‌ دارد</p></div>
<div class="m2"><p>اگر آسودگی خواهی چو سنگ آیینه پنهان‌ کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تحیر می‌زند موج از غبار عرصهٔ امکان</p></div>
<div class="m2"><p>نم اشکی اگر در لغزش آیی ناز جولان ‌کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکوه همتت آیینه در ضبط نفس دارد</p></div>
<div class="m2"><p>هوا را گر مسخر کرده‌ای تخت سلیمان‌ کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ندارد قدردانی جز ندامت‌ کوشش همت</p></div>
<div class="m2"><p>به دست سوده چندی خدمت طبع‌ پشیمان ‌کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بهار هستی‌، انداز پر طاووس می‌خواهد</p></div>
<div class="m2"><p>به یک مژگان گشودن سیر چندین چشم حیران ‌کن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو صبح‌ از صنعت وارستگی غافل مشو بیدل</p></div>
<div class="m2"><p>به‌چین دامنی طرح شکست رنگ امکان‌کن</p></div></div>