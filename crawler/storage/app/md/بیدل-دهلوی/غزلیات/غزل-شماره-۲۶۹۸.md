---
title: >-
    غزل شمارهٔ ۲۶۹۸
---
# غزل شمارهٔ ۲۶۹۸

<div class="b" id="bn1"><div class="m1"><p>ای گشاد و بست مژگانت معمای پری</p></div>
<div class="m2"><p>جام در دستست از چشم تو مینای پری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تغافل تا نگاهت فرق نتوان یافتن</p></div>
<div class="m2"><p>یک جنون می‌پرورد پنهان و پیدای پری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین تمیزی چند کز ساز حواست ظاهر است</p></div>
<div class="m2"><p>گر بفهمی بی‌مساسی نیست اعضای پری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عالمی را حرف و صوت بی‌اثر دیوانه‌کرد</p></div>
<div class="m2"><p>طرف افسون داشت بی اسم مسمای پری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آخر آغوش خیال از خویش خالی‌کردنست</p></div>
<div class="m2"><p>شیشه‌ای داری دو روزی گرم کن جای پری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا کجا گردد غبار وحشت اسباب‌، جمع</p></div>
<div class="m2"><p>بگذر از شیرازه‌ بندیهای اجزای پری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای بهشت آگهی تا کی جنون وهم و ظن</p></div>
<div class="m2"><p>آدمی‌، آدم چه می‌خواهی ز صحرای پری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کارگاه حسن تحقیق از تکلف ساده است</p></div>
<div class="m2"><p>بیشتر بی‌نقش می بافند دیبای پری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آخر از وهم دو رنگی قدر خود نشناختم</p></div>
<div class="m2"><p>شیشه‌ها بر سنگ زد فطرت ز سودای پری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سخت محجوب است حسن‌، آیینه‌دار شرم باش</p></div>
<div class="m2"><p>ازتو چشم بسته می‌خواهد تماشای پری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر کجا زین انجمن یابی سراغ شیشه‌ای</p></div>
<div class="m2"><p>بی ‌ادب مگذر عرق ‌کرده‌ست سیمای پری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل از آثار نیرنگ فلک غافل مباش</p></div>
<div class="m2"><p>وضع این نه حلقه خلخالی‌ست در پای پری</p></div></div>