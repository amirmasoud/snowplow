---
title: >-
    غزل شمارهٔ ۹۵۰
---
# غزل شمارهٔ ۹۵۰

<div class="b" id="bn1"><div class="m1"><p>چو شمع از عضو عضوم آگهی سرشار می‌گردد</p></div>
<div class="m2"><p>به هرجا پا زنم آیینه‌ای بیدار می‌گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندارد نالهٔ من احتیاج لب گشودن‌ها</p></div>
<div class="m2"><p>دو انگشتی که از هم واکنم منقار می‌گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو موج‌گوهر از جمعیت حالم چه می‌یرسی</p></div>
<div class="m2"><p>جنونها می کنم تا لغزشی هموار می‌گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به رنگ شعلهٔ جواله ربطی با وفا دارم</p></div>
<div class="m2"><p>که گر رنگی به گردش آورم زنار می‌گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کف پای حنابند که شورانید خاکم را</p></div>
<div class="m2"><p>که دست قدرت از تخمیر آن بیکار می‌گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل رنگی که من می‌پرورم در جیب امیدش</p></div>
<div class="m2"><p>چمن می‌بالد و برگرد آن دستار می‌گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دماغ باده از سیر چمن مستغنی‌اش دارد</p></div>
<div class="m2"><p>ز یک ساغرکه بر سر می‌کشدگلزار می‌گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز اقبال جهان بگذر مباد از شوق وامانی</p></div>
<div class="m2"><p>درین عبرت‌سرا پیش آمدن دیوار می‌گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مجین‌بر خویش‌چندانی‌که‌فطرت‌باجون‌جوشد</p></div>
<div class="m2"><p>بنا چون پر بلند افتد سر معمار می‌گردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فلک کز نارساییها گم است آغاز و انجامش</p></div>
<div class="m2"><p>به یک پاگرد پای خفته چون پرگار می‌گردد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تلاش رزق داری دست بر هم سوده سامان کن</p></div>
<div class="m2"><p>در این ویرانه زین دست آسیا بسیار می‌گردد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به عرض احتیاج آزار طبع‌کس مده بیدل</p></div>
<div class="m2"><p>نفس چون با غرض جوشید گفتن بار می‌گردد</p></div></div>