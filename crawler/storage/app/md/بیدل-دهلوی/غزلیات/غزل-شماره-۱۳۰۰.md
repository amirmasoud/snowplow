---
title: >-
    غزل شمارهٔ ۱۳۰۰
---
# غزل شمارهٔ ۱۳۰۰

<div class="b" id="bn1"><div class="m1"><p>گر آیینه‌ات در مقابل نماند</p></div>
<div class="m2"><p>خیال حق و فکر باطل نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه ‌صبحی‌ست اینجا نه بامی‌ست پیدا</p></div>
<div class="m2"><p>کجا عرش وکو فرش اگر دل نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همین‌پوست مغز است اگر واشکافی</p></div>
<div class="m2"><p>خیال است لیلی چو محمل نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نم خون عشاق اگر شسته‌گردد</p></div>
<div class="m2"><p>حنا نیز در دست قاتل نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز دانش به صد عقده افتاده‌ کارت</p></div>
<div class="m2"><p>جنون‌گرکنی هیچ مشکل نماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نخواهی به تاب نفس غره بودن</p></div>
<div class="m2"><p>که این شمع آخر به محفل نماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشان گیر ازگرد عنقا سراغم</p></div>
<div class="m2"><p>به آن نقش پایی‌که درگل نماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برد شوق اگر لذت نارسیدن</p></div>
<div class="m2"><p>اقامت در آغوش منزل نماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مجازآفرین است میل حقیقت</p></div>
<div class="m2"><p>کرم گرکند ناز سایل نماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نفس عالمی دارد امّا چه حاصل</p></div>
<div class="m2"><p>دو دم بیش پرواز بسمل نماند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جهان جمله فرش خیال است امّا</p></div>
<div class="m2"><p>ز صیقل گر آیینه غافل نماند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل جمع دارد چه دنیا چه عقبا</p></div>
<div class="m2"><p>چوگوهر شدی بحر و ساحل نماند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در این بزم ز آثار اسرارسنجان</p></div>
<div class="m2"><p>چه ماند اگر شعر بیدل نماند</p></div></div>