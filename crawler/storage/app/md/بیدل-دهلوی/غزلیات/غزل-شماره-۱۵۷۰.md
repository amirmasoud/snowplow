---
title: >-
    غزل شمارهٔ ۱۵۷۰
---
# غزل شمارهٔ ۱۵۷۰

<div class="b" id="bn1"><div class="m1"><p>فرصت ناز کر و فر ضامن ‌کس نمی‌شود</p></div>
<div class="m2"><p>باد و بروت خودسری مد نفس نمی‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل به تلاش خون‌کنی تا برسی به ‌کوی عجز</p></div>
<div class="m2"><p>پای مقیم دامنت آبله‌رس نمی‌شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عین و سوا فضولی فطرت بی‌تمیز توست</p></div>
<div class="m2"><p>زحمت‌آگهی مبر، عشق هوس نمی‌شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قدرشناس داغ عشق حوصله جوهر فناست</p></div>
<div class="m2"><p>وقف ودیعت چنار آتش خس نمی‌شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ذوق ز خویش رفتنی در پی‌ات اوفتاده است</p></div>
<div class="m2"><p>تا به ابد اگر دوی‌، پیش تو پس نمی‌شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قافله‌های درد دل‌ گشته نهان به زبر خاک</p></div>
<div class="m2"><p>حیف‌که‌گرد این بساط شور جرس نمی‌شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست مزاج بوالهوس مایل راز عاشقان</p></div>
<div class="m2"><p>قاصد ما سمندر است عزم مگس نمی‌شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>راه خیال زندگی یک دو قدم جریده رو</p></div>
<div class="m2"><p>خانهٔ زبن پی فراغ جای دو کس نمی‌شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چند دهد فریب امن‌، سر، ته بال بردنت</p></div>
<div class="m2"><p>گر همه فکر نیستی است‌، غیر قفس نمی‌شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دست به خود فشانده را با غم دیگران چه‌کار</p></div>
<div class="m2"><p>لب به فشاراگر رسد رنج نفس نمی‌شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل از انفعال جرم دشمن هوش را چه باک</p></div>
<div class="m2"><p>دزد شراب خورده را فکر عسس نمی‌شود</p></div></div>