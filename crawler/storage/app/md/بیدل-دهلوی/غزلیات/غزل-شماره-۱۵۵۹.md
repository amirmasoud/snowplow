---
title: >-
    غزل شمارهٔ ۱۵۵۹
---
# غزل شمارهٔ ۱۵۵۹

<div class="b" id="bn1"><div class="m1"><p>جزو موزون اعتدال جوهر کل می‌شود</p></div>
<div class="m2"><p>چون شود مینا صدای‌ کوه قلقل می‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جام الفت بسکه بر طاق نزاکت چیده‌اند</p></div>
<div class="m2"><p>دور لطف از باد برگشتن تغافل می‌شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درخور رفع تعلق عیش خرمن ‌کن‌ که شمع</p></div>
<div class="m2"><p>خار پا چندان‌ که می‌آرد برون‌ گل می‌شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عجز طاقت ‌کرد ما را محرم امداد غیب</p></div>
<div class="m2"><p>اختیار آنجا که درماند توکل می‌شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امشبم در دل خیالت مست جام شرم بود</p></div>
<div class="m2"><p>کز نم پیشانی من شیشه ‌پُر مُل می‌شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جرأت رفتار شمعم گر به این واماندگی‌ست</p></div>
<div class="m2"><p>رفته رفته نقش پا درگردنم غل می‌شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرچه‌شد منسوب‌ مجنون ‌بی‌خروش‌عشق‌نیست</p></div>
<div class="m2"><p>آهن ازگل‌ کردن زنجیر بلبل می‌شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عافیت خواهی درین‌ بزم از من و ما دم مزن</p></div>
<div class="m2"><p>زبن هوای تند شمع عالمی‌گل می‌شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرزه‌تاز گفتگو تا چند خواهی زیستن</p></div>
<div class="m2"><p>گر نفس دزدی دو عالم یک تامل می‌شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زین‌ترقیهاکه دونان سر به‌گردون سوده‌اند</p></div>
<div class="m2"><p>گاو و خر را آدمی‌گفتن تنزل می‌شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از تبختر بر قفا مفکن وفاق حاضران</p></div>
<div class="m2"><p>هر سخن‌کاینجا سر زلف‌است‌کاکل می‌شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با قد خم‌ گشته بیدل مگذر از طوف ادب</p></div>
<div class="m2"><p>آه از آن جنگی ‌که میدانش سر پل می‌شود</p></div></div>