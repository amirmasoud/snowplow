---
title: >-
    غزل شمارهٔ ۲۱۱۱
---
# غزل شمارهٔ ۲۱۱۱

<div class="b" id="bn1"><div class="m1"><p>نالهٔ عجز نوای لب خاموش خودم</p></div>
<div class="m2"><p>نشئهٔ شوقم و درد می بیجوش خودم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بحر جولانگه بیباکی و من همچو حباب</p></div>
<div class="m2"><p>در شکنج قفس از وضع ادب‌ کوش خودم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گریه توفانکدهٔ عالم آبی دگر است</p></div>
<div class="m2"><p>بی‌رخت درخور هر اشک قدح نوش خودم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم پوشیده به خود همچو حبابم نظری‌ست</p></div>
<div class="m2"><p>مژه ‌گر باز کنم خواب فراموش خودم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خجلت غیرت ازین بیش چه خواهد بودن</p></div>
<div class="m2"><p>عالم افسانه و من پنبه کش ‌گوش خودم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای بسا سعی عروجی‌ که دلیل پستی است</p></div>
<div class="m2"><p>همچو صهبا به زمین ریخته ی جوش خودم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درخور حفظ ادب خلوت وصلست اینجا</p></div>
<div class="m2"><p>من جنون حوصله از وسعت آغوش خودم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه خیالست‌ کشم حسرت دیگر چو حباب</p></div>
<div class="m2"><p>من‌ که از بار نفس آبلهٔ دوش خودم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیدل از فکر غم و عیش ‌گذشتن دارد</p></div>
<div class="m2"><p>امشبی دارم و فرصت شمر دوش خودم</p></div></div>