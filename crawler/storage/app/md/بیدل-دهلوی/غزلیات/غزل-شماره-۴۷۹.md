---
title: >-
    غزل شمارهٔ ۴۷۹
---
# غزل شمارهٔ ۴۷۹

<div class="b" id="bn1"><div class="m1"><p>ز دهر نقد تو جز پیچ وتاب دشوار است</p></div>
<div class="m2"><p>خیال‌،‌گو مژه بربند، خواب دشوار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل گداخته دعوتسرای جلوهٔ اوست</p></div>
<div class="m2"><p>فروغ مهر نیفتد در آب‌، دشوار است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر به قدر شکستن توان به خود بالید</p></div>
<div class="m2"><p>وگرنه وسعت ظرف حباب دشوار است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز اهل حال مجویید غیر ضبط نفس</p></div>
<div class="m2"><p>که لاف دانش و فهم ازکتاب دشوار است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز حیرت آینهٔ ما به هم نزد مژه‌ ای</p></div>
<div class="m2"><p>به‌ خانه‌ای‌ که ‌پر آب‌ است خواب دشوار است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی برآینهٔ مهر، زنگ سایه نبست</p></div>
<div class="m2"><p>به عالمی‌که تو باشی‌، نقاب دشوار است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سراغ جلوهٔ یار است هر کجا رنگی‌ست</p></div>
<div class="m2"><p>دربن بهار، گل انتخاب دشوار است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز دستگاه دل است اینقدر غرور نفس</p></div>
<div class="m2"><p>وقار و قدر هوا، بی‌حباب‌، دشوار است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه به وهم فرو رفته‌اند و آبی نیست</p></div>
<div class="m2"><p>مگو که غوطه زدن در سراب دشوار است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز انفعال سرشتند نقش ما بیدل</p></div>
<div class="m2"><p>تری برون رود از طبع آب دشوار است</p></div></div>