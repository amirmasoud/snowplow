---
title: >-
    غزل شمارهٔ ۱۸۷۵
---
# غزل شمارهٔ ۱۸۷۵

<div class="b" id="bn1"><div class="m1"><p>شمع من‌ گرم حیا کرد مگر سوی چراغ</p></div>
<div class="m2"><p>می‌توان‌ کرد شنا در عرق روی چراغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل اگر جوش طراوت نزند، سوختنی</p></div>
<div class="m2"><p>شعله‌ کافی‌ست همان سرو لب جوی چراغ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوختیم از هوس اما مژه واری نکشید</p></div>
<div class="m2"><p>بال پروانهٔ ما شانه به‌گیسوی چراغ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نتوان بود ز نیرنگ عتابش غافل</p></div>
<div class="m2"><p>بزم‌گرم است به افروختن روی چراغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بالش عافیتی نیست درین شعله‌ بساط</p></div>
<div class="m2"><p>نفس سوخته دارد سر زانوی چراغ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیری و عشرت ایام جوانی غلط است</p></div>
<div class="m2"><p>صبحدم رنگ نبنددگل شب‌بوی چراغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قرب این شعله مزاجان به‌خود آتش زده است</p></div>
<div class="m2"><p>نیست پروانهٔ ما بیخبر از خوی چراغ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عجز ما رنگ اشارتکدهٔ ناز تو ریخت</p></div>
<div class="m2"><p>بال پروانه شد آخر خم ابروی چراغ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آب‌گردید دل و ناله همان عجز تو است</p></div>
<div class="m2"><p>رشته فربه نشد از خوردن پهلوی چراغ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرکجاگردکند شمع خیالم بیدل</p></div>
<div class="m2"><p>شعله از شرم نشیند پس زانوی چراغ</p></div></div>