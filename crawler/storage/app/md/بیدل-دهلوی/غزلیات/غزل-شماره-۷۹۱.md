---
title: >-
    غزل شمارهٔ ۷۹۱
---
# غزل شمارهٔ ۷۹۱

<div class="b" id="bn1"><div class="m1"><p>آزادگی‌، غبار در و بام خانه نیست</p></div>
<div class="m2"><p>پرواز طایری‌ست که در آشیانه نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرجا سراغ کعبهٔ مقصود داده‌اند</p></div>
<div class="m2"><p>سرها فتاده بر سر هم آستانه نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمع و چراغ مجلس تصویر، حیرت است</p></div>
<div class="m2"><p>درآتشیم و آتش ما را زبانه نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داد شکست دل‌که دهد تا فغان‌کنیم</p></div>
<div class="m2"><p>پرداز موی چینی ما کار شانه نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>واماندهٔ تعلق رزق مقدریم</p></div>
<div class="m2"><p>دام و قفس به غیر همین آب و دانه نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طبع فسرده شکوهٔ همت‌کجا برد</p></div>
<div class="m2"><p>در خانه آتشی‌که توان زد به خانه نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>امشب به وعده‌ای که ز فردا شنیده‌ای</p></div>
<div class="m2"><p>گرآگهی مخسب قیامت فسانه نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جایی که خامشان‌، ادب انشای صحبت‌اند</p></div>
<div class="m2"><p>آیینه باش‌! پای نفس در میانه نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مردان‌، نفس به یاد دم تیغ می‌زنند</p></div>
<div class="m2"><p>میدان عشق، مجلس حیز و زنانه نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما را به هستی و عدم وهم چون‌‌شرار</p></div>
<div class="m2"><p>فرصت بسی‌ست لیک دماغ بهانه نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خفته‌ست‌گرد مطلب خاک شهید عشق</p></div>
<div class="m2"><p>گر خون شودکه قاصد از این‌جا، روانه نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل اگر هوس ندرد پردهٔ حیا</p></div>
<div class="m2"><p>وحدتسرای معنی‌ات آیینه خانه نیست</p></div></div>