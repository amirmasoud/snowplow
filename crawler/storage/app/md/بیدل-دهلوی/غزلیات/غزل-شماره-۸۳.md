---
title: >-
    غزل شمارهٔ ۸۳
---
# غزل شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>ز آهم مجویید تأثیر را</p></div>
<div class="m2"><p>پر از بال عنقاست این تیر را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مصوربه هرجاکشد نقش من</p></div>
<div class="m2"><p>ز تمثال رنگی‌ست تصویر را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درین دشت و در، دم صیاد نیست</p></div>
<div class="m2"><p>رمیدن گرفته‌ست نخجیر را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنای نفس بر هوا بسته‌اند</p></div>
<div class="m2"><p>زتسکین‌گلی نیست تعمیر را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گهی دیر تازیم وگه‌کعبه جو</p></div>
<div class="m2"><p>جنونهاست مجبور تقدیر را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به خواب عدم هستیی دیده‌ایم</p></div>
<div class="m2"><p>ز هذیان مده رنج‌، تعبیر را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرفتار وهم است آزادی‌ات</p></div>
<div class="m2"><p>صدا می‌کشد بار ‌زنجیر را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به وهم اینقدر چند خوابیدنت</p></div>
<div class="m2"><p>برآر از بغل پای در قیر را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زروی‌ترش عرض‌پیری مبر</p></div>
<div class="m2"><p>تبه می‌کند سرکه ین شیر را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خم قامتت این صلا می‌زند</p></div>
<div class="m2"><p>که بر طاق نه ذوق شبگیر را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به هرجا مخاطب ادا فهم نیست</p></div>
<div class="m2"><p>برین ساز بشکن بم وزیر را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به تهدید ازین همدمان امن خواه</p></div>
<div class="m2"><p>تسلسل وبال است تقریر را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر مرجع زندگی خاک نیست</p></div>
<div class="m2"><p>کلک زن خناق‌گلوگیر را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زمین تا فلک نغمهٔ بیدل ست</p></div>
<div class="m2"><p>خمیدن‌کجا می‌برد پیر را</p></div></div>