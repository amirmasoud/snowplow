---
title: >-
    غزل شمارهٔ ۲۱۳۶
---
# غزل شمارهٔ ۲۱۳۶

<div class="b" id="bn1"><div class="m1"><p>به زور شعلهٔ آواز حسرت‌ گرم رفتارم</p></div>
<div class="m2"><p>چو شمع از ناتوانی بال پرواز است منقارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر چه بوی‌ گل دارد ز من درس سبکروحی</p></div>
<div class="m2"><p>همان چون آه بر آیینهٔ دلها گرانبارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز ترک هرزه‌ گردی محو شد پست و بلند من</p></div>
<div class="m2"><p>به رنگ موج‌ گوهر آرمیدن‌ کرد هموارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه مقدار انجمن پرداز خجلت بایدم بودن</p></div>
<div class="m2"><p>که عالم خانهٔ آیینه است و من نفس وارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکست از سیل نپذیرد بنای خانهٔ حیرت</p></div>
<div class="m2"><p>نمی‌افتد به زور آب چون آیینه دیوارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی جز منتهی مضمون عنوانم نمی‌فهمد</p></div>
<div class="m2"><p>به سر دارد ز منزل مهر همچون جاده طومارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دل هر دانه‌ای از ریشهٔ خود دامها دارد</p></div>
<div class="m2"><p>مبادا سر برون آرد ز جیب سبحه زنارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنای نقش پایم در زمین خاکساریها</p></div>
<div class="m2"><p>که از افتادگی با سایه همدوش است دیوارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز حال رفتگان شد غفلتم آیینهٔ بینش</p></div>
<div class="m2"><p>به چشم نقش همچون جادهٔ خوابیده بیدارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز شرم عیب خود چشم از هنر برداشتم بپدل</p></div>
<div class="m2"><p>که چون طاووس پای خوبش باشد خار گلزارم</p></div></div>