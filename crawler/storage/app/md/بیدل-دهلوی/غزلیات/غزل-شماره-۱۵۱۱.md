---
title: >-
    غزل شمارهٔ ۱۵۱۱
---
# غزل شمارهٔ ۱۵۱۱

<div class="b" id="bn1"><div class="m1"><p>شب‌که وصل آغوش‌پرداز دل دیوانه بود</p></div>
<div class="m2"><p>از هجوم زخم شوق آیینهٔ ما شانه بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق‌می‌جوشید هرجاگرد شوخی‌داشت‌حسن</p></div>
<div class="m2"><p>رنگ شمع از پرفشانی عالم پروانه بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یاد آن عیشی‌که از رنگینی بیداد عشق</p></div>
<div class="m2"><p>سیل در ویرانهٔ من باده در پیمانه بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از محیط ما و من توفان‌کثرت اعتبار</p></div>
<div class="m2"><p>نه صدف‌گل‌کرد اماگوهر یکدانه بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تپیدنهای دل رنگ دو عالم ربختند</p></div>
<div class="m2"><p>هر کجا دیدم بنایی گرد این ویرانه بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راز دل از وسعت مشرب به رسوایی‌کشید</p></div>
<div class="m2"><p>دامن صحرا گریبان چاکی دیوانه بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خانه وبرانی به روی آتش من آب ریخت</p></div>
<div class="m2"><p>سوختنها داشتم چون شمع با‌ کاشانه بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جرم آزادیست‌کر نشناخت ما را هیچکس</p></div>
<div class="m2"><p>معنی بیرنگ ما از لفظ پر بیگانه بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عالمی را سعی ما و من به خاموشی رساند</p></div>
<div class="m2"><p>بهر خواب مرگ شور زندگی افسانه بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اختلاط خلق جز ژولیدگی صورت نبست</p></div>
<div class="m2"><p>هر دو عالم پیچش یک گیسوی بی‌شانه بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چشم‌لطف‌از سخت‌رویان‌داشتن بی‌دانشی‌ست</p></div>
<div class="m2"><p>سنگ در هرجا نمایان‌گشت آتشخانه بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دوش حیرانم چه می‌پیمود اشک از بیخودی</p></div>
<div class="m2"><p>کز مژه تا خاک کویش لغزش مستانه بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مفت سامان ادب کز جلوه غافل می‌روبم</p></div>
<div class="m2"><p>چشم واکردن دلیل وضع گستاخانه بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هرکجا رفتیم سیر خلوت دل داشتیم</p></div>
<div class="m2"><p>بیدل‌آ‌غوش فلک هم روزنی زین خانه بود</p></div></div>