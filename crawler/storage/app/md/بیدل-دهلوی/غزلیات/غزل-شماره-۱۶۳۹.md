---
title: >-
    غزل شمارهٔ ۱۶۳۹
---
# غزل شمارهٔ ۱۶۳۹

<div class="b" id="bn1"><div class="m1"><p>چو فقر دست دهد ترک عز و جاه‌کنید</p></div>
<div class="m2"><p>سر برهنه همان آسمان‌ کلاه‌ کنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر گل هوس‌ کهکشان زند به دماغ</p></div>
<div class="m2"><p>اتاقهٔ سر تسلیم برگ کاه‌کنید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سراغ یوسف مطلب درین بیابان نیست</p></div>
<div class="m2"><p>مگر ز چاک ‌گریبان نظر به چاه ‌کنید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خضاب ماتم موی سفید داشتن است</p></div>
<div class="m2"><p>ز مرگ پیش دو روزی ‌کفن سیاه ‌کنید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حریف سرو بلندش نمی‌توان گردید</p></div>
<div class="m2"><p>به هر نهال‌کز این باغ رست آه‌کنید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به برق جلوهٔ حسنش کراست تاب نگاه</p></div>
<div class="m2"><p>غنیمت است اگر سیر مهر و ماه‌ کنید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درین قلمرو عبرت ‌کجا امید و چه یاس</p></div>
<div class="m2"><p>ز هر رهی‌که بجایی رسید راه‌کنید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به یک قسم که ز ضبط دو لب بجا آید</p></div>
<div class="m2"><p>زبان دعوی صد بحث بی‌گوا‌ه کنید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زساز معبد رحمت همین نواست بلند</p></div>
<div class="m2"><p>که ای عدم صفتان کاشکی گناه کنید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ندیده‌اید سرانجام این تماشاگه</p></div>
<div class="m2"><p>به چشم نقش قدم سوی هم نگاه‌کنید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سواد آینهٔ شمع روشن است اینجا</p></div>
<div class="m2"><p>چوخط به نقطه رسد نامه را سیاه ‌کنید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به عالمی‌که همین عمرو و زید جلوه‌گرست</p></div>
<div class="m2"><p>خیال بیدل ما نیز گاه‌گاه ‌کنید</p></div></div>