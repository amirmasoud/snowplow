---
title: >-
    غزل شمارهٔ ۳۴۲
---
# غزل شمارهٔ ۳۴۲

<div class="b" id="bn1"><div class="m1"><p>ممسک اگربه عرض سخا جوشد ازشراب</p></div>
<div class="m2"><p>دستی بلند می‌کند اما به زیرآب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طبع‌کرم فسردهٔ دست تهی مباد</p></div>
<div class="m2"><p>برگشت عالمی‌ست ستم خشکی سحاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این است اگر سماجت ارباب احتیاج</p></div>
<div class="m2"><p>رحم است بر مزاج دعاهای مستجاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غارت نصیب حسرت درد محبتم</p></div>
<div class="m2"><p>نگریست بیدلی‌که زچشمم نبرد آب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل آنقدرگریست‌که غم هم به سیل رفت</p></div>
<div class="m2"><p>آتش درآب غوطه زد از اشک این‌کباب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>افسانه‌سازی شرر و برق تا به‌کی</p></div>
<div class="m2"><p>گر مرد این رهی توهم از خود برون شتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یاران عبث به وهم تعلق فسرده‌اند</p></div>
<div class="m2"><p>اینجاست چون نگه قدم از خانه در رکاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صبح از نفس‌دو مصرع‌برجسته خواند و رفت</p></div>
<div class="m2"><p>دیوان اعتبار و همین بیتش انتخاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواهی نفس خیال‌کن و خواه‌گرد وهم</p></div>
<div class="m2"><p>چیزی نموده‌ایم در آیینهٔ حباب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>محویم و باعثی زتحیرپدید نیست</p></div>
<div class="m2"><p>ای فطرت آب‌کرد وزما رفع‌کن حجاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>معنی چه وانماید از این لفظهای پوچ</p></div>
<div class="m2"><p>پرتشنه است جلوه وآیینه‌ها سراب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در بزم عشق‌، علم چه و معرفت‌کدام</p></div>
<div class="m2"><p>تا عقل گفته‌ایم جنون می‌درد نقاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در عالمی‌که یاد تو با ما مقابل است</p></div>
<div class="m2"><p>آیینه می‌کشد به رخ سایه آفتاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیدل ز جوش سبزه در این ره فتاده است</p></div>
<div class="m2"><p>بی‌چشم یک جهان مژه تهمت‌پرست خواب</p></div></div>