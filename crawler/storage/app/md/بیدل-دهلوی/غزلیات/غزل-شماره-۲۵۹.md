---
title: >-
    غزل شمارهٔ ۲۵۹
---
# غزل شمارهٔ ۲۵۹

<div class="b" id="bn1"><div class="m1"><p>وصف لب توگر دمد ازگفتگوی ما</p></div>
<div class="m2"><p>گردد چوگوهر آب‌گره درگلوی ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دربهار و باغ به سوی توروی ما</p></div>
<div class="m2"><p>نام تو سکهٔ درم‌گفتگوی ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بحریم ونیست قسمت ما آرمیدنی</p></div>
<div class="m2"><p>چون موج خفته است‌تپش موبه موی ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از اختراع مطلب نایاب ما مپرس</p></div>
<div class="m2"><p>با رنگ و بو نساخت‌گل آرزوی ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما وحباب آب زیک بحرمی‌کشیم</p></div>
<div class="m2"><p>خالی شدن نبرد پری از سبوی ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون صبح چاک سینهٔ ما بخیه‌ای نداشت</p></div>
<div class="m2"><p>پاشیدن غبار نفس شد رفوی ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عمری‌ست باگداز دل خود مقابلیم</p></div>
<div class="m2"><p>ای آینه عبث نشوی روبروی ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناگشته خاک دست نشستیم از غرور</p></div>
<div class="m2"><p>چون شعله بود وقف‌تیمم وضوی ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نقاش زحمت خط و خال آنقدر مکش</p></div>
<div class="m2"><p>خط می‌کشد به سایهٔ موآب جوی ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا چند پروری به نفس مزرع امید</p></div>
<div class="m2"><p>بایدکشید خاطر او را به سوی ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غماز ناتوانی ما هیچکس نبود</p></div>
<div class="m2"><p>بیدل شکست رنگ برون داد بوی ما</p></div></div>