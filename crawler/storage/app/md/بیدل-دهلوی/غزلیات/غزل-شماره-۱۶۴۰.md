---
title: >-
    غزل شمارهٔ ۱۶۴۰
---
# غزل شمارهٔ ۱۶۴۰

<div class="b" id="bn1"><div class="m1"><p>ای بیخردان طور تعین نگزینید</p></div>
<div class="m2"><p>با سجده بسازید که اجزای زمینید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درکارگه شیوه تسلیم‌، عروجی‌ست</p></div>
<div class="m2"><p>چندانکه نشان‌ کف پایید جبینید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اینجا طرب وهم اقامت چه جنون است</p></div>
<div class="m2"><p>در خانه نیرنگ حنابندی زینید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امروز پی نام و نشان چند دویدن</p></div>
<div class="m2"><p>فردا که ‌گذشتید نه آنید نه اینید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندیشهٔ هستی‌ کلف همت مردست</p></div>
<div class="m2"><p>دامن ز غباری که نداربد بچینید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون شمع هوس سر به هوا چند فرازید</p></div>
<div class="m2"><p>گاهی زتکلف ته پا نیز ببینید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین نسبت دوری که به هستی‌ست عدم را</p></div>
<div class="m2"><p>کم نیست‌که چون ذره به خورشید قرینید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در عالم تجرید چه فرصت‌ شمریهاست</p></div>
<div class="m2"><p>تا صبح قیامت نفس باز پسینید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رفتید و نکردید تماشای گذشتن</p></div>
<div class="m2"><p>ای ‌کامن دمی چند به یکجا بنشینید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرچند نفس ساز کند صور قیامت</p></div>
<div class="m2"><p>در حوصله‌های مگس و پشه طنینید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عنقا چه نشان می‌دهد از شهرت موهوم</p></div>
<div class="m2"><p>چشمی بگشایید که نام چه نگینید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تمثال غبار من و مایید چو بیدل</p></div>
<div class="m2"><p>صد سال‌ گر آیینه زدایید همینید</p></div></div>