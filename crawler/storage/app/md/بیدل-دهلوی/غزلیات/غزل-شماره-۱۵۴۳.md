---
title: >-
    غزل شمارهٔ ۱۵۴۳
---
# غزل شمارهٔ ۱۵۴۳

<div class="b" id="bn1"><div class="m1"><p>جهدکن‌که دل ز هوس پایمال شک نشود</p></div>
<div class="m2"><p>این‌کتاب علم‌یقین نقطه‌ای‌ست حک نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رنگ مهرگیتی اگر دیدی از هوس بگذر</p></div>
<div class="m2"><p>این جلب گلی که زند غیر آتشک نشود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آب‌و رنگ‌حسن جهان می‌دهد ز قبح نشان</p></div>
<div class="m2"><p>کم دمید گل‌ که به رخ شبنمش ‌کلک نشود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از مزاج اهل دول رسم اتحاد مجو</p></div>
<div class="m2"><p>در زمین تیره‌دلان سایه مشترک نشود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلبل ار رسی به چمن طرح خامشی مفکن</p></div>
<div class="m2"><p>ناله‌کن‌که برلب‌گل خنده بی‌نمک نشود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست شامی و سحری‌ کز حجاب جلوه او</p></div>
<div class="m2"><p>غنچه شبنمی نکند شمع شبپرک نشود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رنگ عشق و داغ طلب نور شمع و مایل شب</p></div>
<div class="m2"><p>هرکجا زری‌ست چرا طالب محک نشود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مانع تنزه ما گشت شغل حرص و هوا</p></div>
<div class="m2"><p>تا بود شراب وغذا آدمی ملک نشود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زحمت محال مبر جیب انفعال مدر</p></div>
<div class="m2"><p>ما نمی‌رسیم به او تا زمین فلک نشود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتگوی‌.عین وسوا قطع‌کن زشبهه بزآ</p></div>
<div class="m2"><p>تا به لب‌گره نزنی اینکه دوست یک نشود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل اقتضای جشد می‌کشد به‌حرص‌و حسد</p></div>
<div class="m2"><p>خواب امنی داری اگر پیرهن خسک نشود</p></div></div>