---
title: >-
    غزل شمارهٔ ۱۶۸۵
---
# غزل شمارهٔ ۱۶۸۵

<div class="b" id="bn1"><div class="m1"><p>تیغ در دست است یار از جیب بیرون آر سر</p></div>
<div class="m2"><p>صبح شد بی‌پرده از خواب ‌گران بردار سر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فال آهنگ شهادت زن که در میدان عشق</p></div>
<div class="m2"><p>هست بی‌سعی بریدن پای بی رفتار سر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در محیط عشق‌کافسون شهادت موج اوست</p></div>
<div class="m2"><p>چون حباب از الفت تن بایدت بیزار سر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از زبان بینوای شمع می‌آید به‌گوش</p></div>
<div class="m2"><p>کای حریفان نیست اینجا عافیت دربار سر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای فلک در دور چشم و ابروی آن فتنه‌جوی</p></div>
<div class="m2"><p>از مه نو ناخنی پیداکن و میخار سر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌نشاند بال قمری سرو را در زیر تیغ</p></div>
<div class="m2"><p>گر کند با قامت او دعوی رفتار سر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دهر اگرگلخن شود سامان عیش من‌کجاست</p></div>
<div class="m2"><p>یاد رخسار توام داده‌ست در گلزار سر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از گزند خلق دل فارغ ‌کن و آسوده باش</p></div>
<div class="m2"><p>چند باید داشت باب‌ کوفتن چون مار سر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وضع همواری مده از دست اگر صاحب‌دلی</p></div>
<div class="m2"><p>نیست اینجا سبحه را جز بر خط زنار سر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر نتابد وادی تسلیم ما گردنکشی</p></div>
<div class="m2"><p>همچو نقش پا در این ره می‌شود هموار سر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اهل دنیا را ز جست‌وجوی دنیا چاره نیست</p></div>
<div class="m2"><p>می‌کشد ناچار کرکس جانب مردار سر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در جهان بی‌نیازی جز شهادت باب نیست</p></div>
<div class="m2"><p>شمع‌سان چندان که مقدورت بود بردار سر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حاصل ‌کار شکفتنهای ما آشفتگی است</p></div>
<div class="m2"><p>غنچه را بعد از دمیدن می‌شود دستار سر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>با کدامین آبرو گردن توان افراختن</p></div>
<div class="m2"><p>همچو شمعم‌ کاش باشد یک بریدن وار سر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جوش بحر بی‌نیازی تشنهٔ اسباب نیست</p></div>
<div class="m2"><p>چون‌ گهر بی‌گردن اینجا می‌دهد بسیار سر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اشک مژگان است بیدل برگ ساز این چمن</p></div>
<div class="m2"><p>می‌نهد هر غنچه بر بالین چندین خار سر</p></div></div>