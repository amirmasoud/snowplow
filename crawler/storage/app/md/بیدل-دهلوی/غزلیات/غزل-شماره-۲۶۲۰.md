---
title: >-
    غزل شمارهٔ ۲۶۲۰
---
# غزل شمارهٔ ۲۶۲۰

<div class="b" id="bn1"><div class="m1"><p>زین چمن درکف ندارد غنچهٔ دل جز گره</p></div>
<div class="m2"><p>دانهٔ ما را چو گوهر نیست حاصل جز گره</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از امل محمل‌کش صدکاروان نومیدی‌ام</p></div>
<div class="m2"><p>سبحه درگردن نمی‌بندد حمایل جزگره</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از تعلق‌، حاصل آزادگان خون‌خوردن است</p></div>
<div class="m2"><p>سروکم آرد به‌بار از پای درگل جزگره</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از فسون عافیت بر خود در کوشش مبند</p></div>
<div class="m2"><p>رشتهٔ راهت نمی‌بیند ز منزل جزگره</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از حیا بر روی خود درهای نعمت بسته‌ای</p></div>
<div class="m2"><p>بی‌زبانی نفکند در کار سایل جز گره</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غافل از تردستی مطرب درین محفل مباش</p></div>
<div class="m2"><p>زخمه جز ناخن ندارد درکف و دل جزگره</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همتی ای شعله‌خویان‌! کاین سپند بینوا</p></div>
<div class="m2"><p>تحفه‌ای دیگر ندارد نذر محفل جزگره</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک دل تنگ است عالم بی‌حصول مدعا</p></div>
<div class="m2"><p>تابود در پرده لیلی نیست محمل جزگره</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر اسیران دل از فقر و غنا افسون مخوان</p></div>
<div class="m2"><p>نیست در چشم ‌گهر دریا و ساحل جز گره</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صاف طبعان بیدل از هستی‌کدورت می‌کشند</p></div>
<div class="m2"><p>از نفس آیینه‌ها را نیست در دل جزگره</p></div></div>