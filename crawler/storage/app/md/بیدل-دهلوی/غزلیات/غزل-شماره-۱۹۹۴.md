---
title: >-
    غزل شمارهٔ ۱۹۹۴
---
# غزل شمارهٔ ۱۹۹۴

<div class="b" id="bn1"><div class="m1"><p>فهم حقیقت من و ما را بهانه‌ام</p></div>
<div class="m2"><p>خوابیده است هر دو جهان در فسانه‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بوی غنچه‌ای‌ که فتد در نقاب رنگ</p></div>
<div class="m2"><p>خون می‌خورد به پردهٔ حسرت ترانه‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پاک است نامهٔ سحر ازگرد انتطار</p></div>
<div class="m2"><p>قاصد اگر درنگ کند من روانه‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر دوش آه محمل دل بسته است شوق</p></div>
<div class="m2"><p>چون سبحه می‌دود به سر ریشه دانه‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زبن بزم غیر شمع کسی را نسوختند</p></div>
<div class="m2"><p>دنیاست آتشی که منش در میانه‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چندی تپید شعلهٔ امید و داغ شد</p></div>
<div class="m2"><p>چون شمع بال سوخته بود آشیانه‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عجزم چو سایه بر در دیر و حرم نشاند</p></div>
<div class="m2"><p>یک جبههٔ نیاز و هزار آستانه‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آشفته نیست طرهٔ وضع تحیرم</p></div>
<div class="m2"><p>یارب به جنبش مژه مپسند شانه‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در موج حیرتی چو گهر غوطه خورده‌ام</p></div>
<div class="m2"><p>محو است امتیاز کران و میانه‌ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عنقا به بی‌نشانی من می‌خورد قسم</p></div>
<div class="m2"><p>نامی به عالم نشنیدن فسانه‌ام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لبریزم آنقدر ز تمنای جلوه‌ای</p></div>
<div class="m2"><p>کز شرم ‌گر عرق‌ کنم آیینه خانه‌ام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا پر فشانده‌ام قفس و آشیان گم است</p></div>
<div class="m2"><p>بیدل چو بوی‌گل به‌کمین بهانه‌ام</p></div></div>