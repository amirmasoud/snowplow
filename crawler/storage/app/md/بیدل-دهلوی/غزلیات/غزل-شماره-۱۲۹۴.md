---
title: >-
    غزل شمارهٔ ۱۲۹۴
---
# غزل شمارهٔ ۱۲۹۴

<div class="b" id="bn1"><div class="m1"><p>رفتیم و داغ ما به دل روزگار ماند</p></div>
<div class="m2"><p>خاکستری ز قافلهٔ اعتبار ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ما به خاک وادی الفت سواد عشق</p></div>
<div class="m2"><p>هرجا شکست آبله دل یادگار ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل را تپیدن از سرکوی تو برنداشت</p></div>
<div class="m2"><p>این‌گوهر آب‌گشت و همان خاکسار ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وضع حیاست دامن فانوس عافیت</p></div>
<div class="m2"><p>از ضبط خود چراغ ‌گهر در حصار ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مفت نشاط هیچ اگر فقر و گر غنا</p></div>
<div class="m2"><p>دستی نداشتم که بگویم ز کار ماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زنهار خو مکن به‌گرانجانی آنقدر</p></div>
<div class="m2"><p>شد سنگ ناله‌ای که درین ‌کوهسار ماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرصت نماند و دل به تپش همعنان هنوز</p></div>
<div class="m2"><p>آهو گذشت و شوخی رقص غبار ماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرجا نفس به شعلهٔ تحقیق سوختیم</p></div>
<div class="m2"><p>کهسار بر صدا زد و مشتی شرار ماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیری سراغ وحشت عمر گذشته بود</p></div>
<div class="m2"><p>مزدور رفت دوش هوس زیر بار ماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نگذاشت حیرتم که گلی چینم از وصال</p></div>
<div class="m2"><p>از جلوه تا نگاه یک آغوش‌وار ماند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خودداری‌ام به عقدهٔ محرومی آرمید</p></div>
<div class="m2"><p>در بحر نیز گوهر من برکنار ماند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مژگان ز دیده قطع تعلق نمی کند</p></div>
<div class="m2"><p>مشت غبار من به ره انتظار ماند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بیدل ز شعله‌ای که نفس برق ناز داشت</p></div>
<div class="m2"><p>داغی چو شمع کشته به لوح مزار ماند</p></div></div>