---
title: >-
    غزل شمارهٔ ۲۷۳۱
---
# غزل شمارهٔ ۲۷۳۱

<div class="b" id="bn1"><div class="m1"><p>ازین ‌نه منظر نیرنگ تا برتر زنم جوشی</p></div>
<div class="m2"><p>نفس بودم سحر گل کردم از یاد بناگوشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تپشها در هجوم حیرت دیدار گم دارم</p></div>
<div class="m2"><p>نگاه ناتوانم غرقهٔ توفان خاموشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زتمکین رگ یاقوت بست ابریشم سازم</p></div>
<div class="m2"><p>اشارات ادب آهنگی خون گرد و مخروشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز درس نسخهٔ هستی چه خواهم سخت حیرانم</p></div>
<div class="m2"><p>به صد تعبیرم ایما می‌کند خواب فراموشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به غارت رفته گرد جلوه‌گا‌ه ‌کیستم یارب</p></div>
<div class="m2"><p>که از هر ذره‌ای بالم نگاه خانه بر دوشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نوای آتشینی دارم و از شرم بیباکی</p></div>
<div class="m2"><p>نفس دزدیده‌ام تا در نگیرد پنبه درگوشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکستن تا چه‌ها ریزد به دامان حباب من</p></div>
<div class="m2"><p>نگاهی رفته است از خویش و گل‌ کرده‌ست آغوشی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز مستان هوس‌پیمای این محفل نمی‌بینم</p></div>
<div class="m2"><p>چو مینا شیشه در دستی و چون ساغر قدح نوشی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز صد آیینه اینجا یک نگه صورت نمی‌بندد</p></div>
<div class="m2"><p>تو بر خود جلوه‌ کن ما را کجا چشمی کجا هوشی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل داغ آشیانی در قفس پرورده‌ام بیدل</p></div>
<div class="m2"><p>به زیر بال دارم سیر طاووس چمن پوشی</p></div></div>