---
title: >-
    غزل شمارهٔ ۵۰۱
---
# غزل شمارهٔ ۵۰۱

<div class="b" id="bn1"><div class="m1"><p>نفس بوالهوسان بر دل ر‌وشن تیغ است</p></div>
<div class="m2"><p>شمع افروخته را جنبش دامن تیغ است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیشه‌ را سرکشی‌ خویش نشانده ست به خون</p></div>
<div class="m2"><p>گردن بی‌ادبان را رگ گردن تیغ است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منت سایهٔ اقبال ز آتش کم نیست</p></div>
<div class="m2"><p>گر هما بال ‌گشاید به سر من تیغ است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاک تسلیم به سر کن که درین دشت هلاک</p></div>
<div class="m2"><p>تو نداری سپر و در کف دشمن تیغ است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نتوان از نفس سوختگان ایمن بود</p></div>
<div class="m2"><p>دود این خانه چو برجست ز روزن تیغ است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عکس خونی‌ست فرویخته از پیکر شخص</p></div>
<div class="m2"><p>گر همه آینه سازند ز آهن تیغ است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا مخالف ز موافق قدمی فاصله نیست</p></div>
<div class="m2"><p>در گلو آب چو استاد ز رفتن تیغ است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کوه از ناله و فریاد نمک ‌آساید</p></div>
<div class="m2"><p>چه کند بر سر این پای به دامن تیغ است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ذوالفقار دگر است آنکه‌ کند قلع امل</p></div>
<div class="m2"><p>و‌رنه مقراض هم از بهر بریدن تیغ است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کلفت ز‌ندگی از مرگ بتر می‌باشد</p></div>
<div class="m2"><p>شمع ما را ز ‌سر خو‌د نگذشتن تیغ است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سطر خونی ز پرافشانی بسمل خواندیم</p></div>
<div class="m2"><p>که گر از خویش روی جادهٔ روشن تیغ است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زین ندامت که به وصلی نرسیدم بیدل</p></div>
<div class="m2"><p>هر نفس در جگرم تا دم مردن تیغ است</p></div></div>