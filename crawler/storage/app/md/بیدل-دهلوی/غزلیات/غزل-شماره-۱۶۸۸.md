---
title: >-
    غزل شمارهٔ ۱۶۸۸
---
# غزل شمارهٔ ۱۶۸۸

<div class="b" id="bn1"><div class="m1"><p>ز صبح طلعتش آیینهٔ دل را صفا بنگر</p></div>
<div class="m2"><p>ز شام طره‌اش چون شب دلیل بخت ما بنگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به ‌کشت صبر ما برق نگاهش را تماشا کن</p></div>
<div class="m2"><p>ز چین ابرویش دندانهٔ داس بلا بنگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به پای زلف از هر حلقه خلخالی تماشاکن</p></div>
<div class="m2"><p>به دست نرگس بیمارش از مژگان عصا بنگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غبار خاطر خورشید از خطش برون آمد</p></div>
<div class="m2"><p>به باغ دلفریبی شوخی این سبزه را بنگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جای خنده‌های غفلت ‌گل درگلستانها</p></div>
<div class="m2"><p>ز موج اشک بلبل در گلستان حیا بنگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشان مردمی بیدل چه جویی از سیه‌چشمان</p></div>
<div class="m2"><p>وفا کن پیشه و زین قوم آیین جفا بنگر</p></div></div>