---
title: >-
    غزل شمارهٔ ۲۳۷۸
---
# غزل شمارهٔ ۲۳۷۸

<div class="b" id="bn1"><div class="m1"><p>نه لفظ از پرده می‌جوشد نه معنی می‌دهد رویم</p></div>
<div class="m2"><p>همان یک رفتن دل می‌کند گرد آنچه می‌گویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مپرس ازمزرع بیحاصل نشو و نمای من</p></div>
<div class="m2"><p>چو تخم اشک می‌کارم گداز ناله می‌رویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به چندین ناز خونم می‌چکد در پردهٔ حسرت</p></div>
<div class="m2"><p>تغافل بسملم یعنی شهید تیغ ابرویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندارم از هجوم ناتوانی رنگ گرداندن</p></div>
<div class="m2"><p>به رنگ سایه گر آتش نهی در زیر پهلویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بس شخص نمودم آب شد از شرم پیدایی</p></div>
<div class="m2"><p>عرق می‌چینم از آیینه گر تمثال می‌جویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو فرصت وانما تا من کنم تدبیر آرایش</p></div>
<div class="m2"><p>به رنگ دود شمع‌ از شانه دارد شرم‌ گیسویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جا وامانده‌ام چون شمع لیک از ننگ افسردن</p></div>
<div class="m2"><p>به دوش شعله محمل می‌کشد عجز تک و پویم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نی‌ام گوهر که هر یکقطره آبم بگذرد از سر</p></div>
<div class="m2"><p>اگرتوفان مدّ چون موج بوسد پای زانویم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غرور هستی‌ام با تیغ نازش بر نمی‌آید</p></div>
<div class="m2"><p>به این گردن که می‌بینی به صد باریکی مویم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز عدل ناتوانی ناله را با کوه می‌سنجم</p></div>
<div class="m2"><p>درین بازار سنگ کم نمی‌گردد ترازویم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو شبنم تا درین گلزار عبرت چشم وا کردم</p></div>
<div class="m2"><p>حیا غیر از عرق رنگی دگر نگذاشت بر رویم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نگردی غافل از فیض سواد معنی‌ام بیدل</p></div>
<div class="m2"><p>تماشا بر سحر می‌خندد ازگلهای شببویم</p></div></div>