---
title: >-
    غزل شمارهٔ ۲۴۲۵
---
# غزل شمارهٔ ۲۴۲۵

<div class="b" id="bn1"><div class="m1"><p>داغم ز ابر دیده به ‌شبنم گریستن</p></div>
<div class="m2"><p>یعنی ‌که بیش اپن نتوان‌ کم ‌گریستن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دیده با لباس سیه‌گریه‌ات خوش است</p></div>
<div class="m2"><p>دارد گلاب جامهٔ ماتم گریستن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر ساز زندگانی خود نیز خنده‌ای</p></div>
<div class="m2"><p>تا چند در وفات اب و عم ‌گریستن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو ابن آدمی‌ گرت امید رحمتی است</p></div>
<div class="m2"><p>میراث دیده گیر ز آدم گریستن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر شد دل از نشاط و لب از خنده بی‌نصیب</p></div>
<div class="m2"><p>یارب ز چشم ما نشود کم گریستن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ضعف اینچنین‌که خصم توانایی منست</p></div>
<div class="m2"><p>مشکل که بی‌رخ تو توانم گریستن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شبنم ز وصل گل چه نشاط آرزو کند</p></div>
<div class="m2"><p>اینجاست بر نگاه مقدم گریستن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کس اینقدر ادب قفس درد دل مباد</p></div>
<div class="m2"><p>اشکم نبست طاقت یکدم گریستن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تاکی درین بهار طرب خنده‌های صبح</p></div>
<div class="m2"><p>این خنده توام است به شبنم گریستن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شیرازهٔ موافقت آخر گسستنی است</p></div>
<div class="m2"><p>باید دو روز چون مژه با هم گریستن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خجلت رضا به شوخی اشکم نمی‌دهد</p></div>
<div class="m2"><p>می‌بایدم به سعی جبین نم گریستن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل ز شیشه‌های نگون باده می‌کشد</p></div>
<div class="m2"><p>زیباست از قدی که بود خم گریستن</p></div></div>