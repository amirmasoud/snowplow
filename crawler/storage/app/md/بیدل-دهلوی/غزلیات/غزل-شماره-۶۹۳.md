---
title: >-
    غزل شمارهٔ ۶۹۳
---
# غزل شمارهٔ ۶۹۳

<div class="b" id="bn1"><div class="m1"><p>تنها نه ذره دقت اظهار داشته‌ست</p></div>
<div class="m2"><p>خورشید نیز آینه درکار داشته‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل غرهٔ چه عیش نشیندکه زیرچرخ</p></div>
<div class="m2"><p>گوهر شکست و آینه زنگار داشته‌ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تنزیه در صنایع آثار دهر نیست</p></div>
<div class="m2"><p>این شیشه‌گر حقیقت گل کار داشته‌ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در ششجهت تنیدن آهنگ حیرتی‌ست</p></div>
<div class="m2"><p>قانون درد دل چقدر تار داشته‌ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آگاه نیست هیچ‌کس از نشئهٔ حضور</p></div>
<div class="m2"><p>حیرت هزار ساغر سرشار داشته‌ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقش نگار خانهٔ دل جز خیال نیست</p></div>
<div class="m2"><p>آیینه هرچه دارد از آن عار داشته‌ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای از جنون جهل تن آسانی آرزوست</p></div>
<div class="m2"><p>هوشی که‌سایه را که‌نگونسار داشته‌ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قد دو تاست حلقهٔ چندین سجود ناز</p></div>
<div class="m2"><p>گویا سراغی از در دلدار داشته‌ست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرچند داغ‌گشت دل و دیده خون‌گریست</p></div>
<div class="m2"><p>آگه نشدکه عشق چه آزار داشته‌ست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل تو اندکی گره دل گشاده کن</p></div>
<div class="m2"><p>کاین نوغزل چه‌صنعت اسرار داشته‌ست</p></div></div>