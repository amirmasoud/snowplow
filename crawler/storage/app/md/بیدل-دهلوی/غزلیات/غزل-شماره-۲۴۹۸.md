---
title: >-
    غزل شمارهٔ ۲۴۹۸
---
# غزل شمارهٔ ۲۴۹۸

<div class="b" id="bn1"><div class="m1"><p>از خودآرایی به‌جنس جاودان لنگر مکن</p></div>
<div class="m2"><p>آبرو را سنگسار صنعت‌ گوهر مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خار جوهر زحمت‌گلبرک تمثالت مباد</p></div>
<div class="m2"><p>پردهٔ چشم تر آیینه را بستر مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا توان درکسوت همواری آیینه زیست</p></div>
<div class="m2"><p>دامن ابروی خود چون تیغ پر جوهر مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای ادب‌، بگذار مژگانی به رویش واکنم</p></div>
<div class="m2"><p>جوهر پرواز ما را چین بال و پر مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>انفعال معصیت فردوس تعمیر است و بس</p></div>
<div class="m2"><p>گر جبین دارد عرق اندیشهٔ ‌کوثر مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آب‌ورنگ حسن معنی‌نشکند بیجوهری</p></div>
<div class="m2"><p>آسمان گو نسخه‌ام را جدولی از زر مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از محیط رحمتم اشک ندامت مژده‌ای‌ست</p></div>
<div class="m2"><p>یا رب این نومید را محروم چشم ترمکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای سپند از سرمه هم اینجا صدا وا می‌کشد</p></div>
<div class="m2"><p>نا توان بر باد رفتن سعی خاکستر مکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا به‌کی چون خامه موی حسرتت بایدکشید</p></div>
<div class="m2"><p>اینقدر خود را به ذوق فربهی لاغر مکن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درد سر بسیار دارد نسخهٔ تحقیق خویش</p></div>
<div class="m2"><p>جز فراموشی اگر درسی‌ست هیچ از بر مکن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خامشی دل را همان شیرازهٔ جمعیت‌ست</p></div>
<div class="m2"><p>نسخهٔ آیینه از باد نفش ابتر مکن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حیف اوقاتی‌که صرف حسرت جاهش‌کنند</p></div>
<div class="m2"><p>آدمی‌، آدم‌! وطن در فکرگاو و خر مکن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تاکجا بیدل به افسون امل خواهی تنید</p></div>
<div class="m2"><p>قصهٔ ما داستان مار دارد سر مکن</p></div></div>