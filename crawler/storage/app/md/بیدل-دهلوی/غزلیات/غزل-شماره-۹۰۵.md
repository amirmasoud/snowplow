---
title: >-
    غزل شمارهٔ ۹۰۵
---
# غزل شمارهٔ ۹۰۵

<div class="b" id="bn1"><div class="m1"><p>مگو طاق و سرایی‌ کرده‌ام طرح</p></div>
<div class="m2"><p>دل عبرت بنایی کرد‌ه‌ام طرح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز نیرنگ تعلقها مپرسید</p></div>
<div class="m2"><p>برای خود بلایی کرده‌ام طرح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ببینم تا چها می‌بایدم دید</p></div>
<div class="m2"><p>چو هستی خودنمایی‌ کرده‌ام طرح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگارستان رنگ انفعال است</p></div>
<div class="m2"><p>اگر چون و چرایی کرده‌ام طرح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آثار بلندیهای طاقت</p></div>
<div class="m2"><p>همین دست دعایی کرده‌ام طرح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکست رنگ باید جمع‌کردن</p></div>
<div class="m2"><p>که تصویر فنایی کرده‌ام طرح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو صبحم نقشبند طاق اوهام</p></div>
<div class="m2"><p>نفس‌واری هوایی کرده‌ام طرح</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سراسر تازه گلزار خیالم</p></div>
<div class="m2"><p>خیابان رسایی کرده‌ام طرح</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هوای وعدهٔ دیدار گرم است</p></div>
<div class="m2"><p>قیامت مدعایی کرده‌ام طرح</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ندارم شکوه نذر خویش اما</p></div>
<div class="m2"><p>نیاز افسون‌نوایی کرده‌ام طر‌ح</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چرا چون آبله بر خود نبالم</p></div>
<div class="m2"><p>سری در زیر پایی کرده‌ام طر‌ح</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه ‌گلزاری‌ست منظورم نه فردوس</p></div>
<div class="m2"><p>برای خنده جایی کرده‌ام طرح</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به این طارم مناز ای اوج اقبال</p></div>
<div class="m2"><p>که من یک پشت پایی‌کرده‌ام‌.طرح</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیا بیدل‌ که درگلزار معنی</p></div>
<div class="m2"><p>زمین دلگشایی کرده‌ام طرح</p></div></div>