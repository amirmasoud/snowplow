---
title: >-
    غزل شمارهٔ ۲۴۵۶
---
# غزل شمارهٔ ۲۴۵۶

<div class="b" id="bn1"><div class="m1"><p>از خود سری مچینید ادبار تا به‌گردن</p></div>
<div class="m2"><p>خلقی‌ست زین چنین سر بیزار تا به‌گردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای غافلان گر این است آثار سربلندی</p></div>
<div class="m2"><p>فرقی نمی‌توان یافت از دار تا به گردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تسلیم تیغ تقدیر زین بیشتر چه بالد</p></div>
<div class="m2"><p>چون موست پیکر ما یک تار تا به‌گردن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین سرکشی چه دارد طبع جنون سرشتت</p></div>
<div class="m2"><p>آفات همچو سیل‌ست درکار تابه گردن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تمکین نمی‌پسندد هنگامهٔ رعونت</p></div>
<div class="m2"><p>زین وضع زیر تیغ‌ست کهسار تا به گردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرداست خاک این‌دشت پا بر سر شکسته‌ست</p></div>
<div class="m2"><p>امروز در ته پاش انگار تا به گردن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلقی‌ست زین جنونزار عریان بی‌تمیزی</p></div>
<div class="m2"><p>دستار تا به زانو شلوار تا به گردن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رنج خلاب دنیا مست بهار خوبی‌ست</p></div>
<div class="m2"><p>تا پا نهی که رفتی یک بار تا به گردن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مینای این خرابات بی می نمی‌توان یافت</p></div>
<div class="m2"><p>در خون نشستگانند بسیار تا به گردن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از حرص ما تعلق دارد سر تملق</p></div>
<div class="m2"><p>چندیش پای در گل بگذار تا به گردن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>موج‌گهر چه مقدار از آب سر برآرد</p></div>
<div class="m2"><p>دارد بنای اقبال دیوار تا به گردن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا بند بندت از هم چون سبحه وا نگردد</p></div>
<div class="m2"><p>عقد انامل یأس بشمار تا به گردن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا زندگی ست چون شمع‌ ایمن نمی‌توان زیست</p></div>
<div class="m2"><p>یک‌کوچه آتش از پاست این خار تا به‌گردن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در خلق اگر به این بعد بی ربطی وفاق‌ست</p></div>
<div class="m2"><p>پیغام سر توان برد دشوار تا به گردن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کو سیلی ضروری یا تیغ امتحانی</p></div>
<div class="m2"><p>خلقی نشسته اینجا بیکار تا به گردن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کو طاعتی‌که ما را تاکوی او رساند</p></div>
<div class="m2"><p>تسبیح تا زبان‌ست زنار تا به‌ گردن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بید بهار یأسیم از بی‌بری مپرسید</p></div>
<div class="m2"><p>اعضا به خم شکستیم زین بار تا به‌ گردن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رنگ حنایش امشب سیر بهار نازست</p></div>
<div class="m2"><p>پابوس و منت خون بردار تا به گردن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زان جرأتی که سودم دستی به تیغ نازش</p></div>
<div class="m2"><p>بردم ز هر سر انگشت زنهار تا به‌گردن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون شعله برده بودم بر چرخ بار طاقت</p></div>
<div class="m2"><p>رنگ شکسته‌ام کرد هموار تا به گردن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سودایی هوس را کم نیست موی سر هم</p></div>
<div class="m2"><p>بپدل مپیچ ازین بیش دستار تا به ‌گردن</p></div></div>