---
title: >-
    غزل شمارهٔ ۲۱۰۴
---
# غزل شمارهٔ ۲۱۰۴

<div class="b" id="bn1"><div class="m1"><p>ندارم رشتهٔ دیگر که آیین طلب بندم</p></div>
<div class="m2"><p>شب تاری مگر برساز آهنگ طرب بندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز گفت‌وگو دهم تا کی به توفان زورق دل را</p></div>
<div class="m2"><p>حیا کو کز لب خاموش پل بحر طلب بندم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به این ترتیب الفاظی ‌که دارد ننگ موزونی</p></div>
<div class="m2"><p>دو مصرع ربط پیدا می‌کند گر لب به لب بندم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خیر و شر چه پردازم ‌که تسلیم حیا مشرب</p></div>
<div class="m2"><p>به ‌کفرم می‌کند منسوب‌ گر دل بر سبب بندم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مزاج خاکسارم با رعونت بر نمی‌آید</p></div>
<div class="m2"><p>جبین بر سجده مشتاقست احرام ادب بندم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز طبع موج ‌گوهر غیر همواری نمی‌جوشد</p></div>
<div class="m2"><p>مروت جوهرم‌ گر تیغ بندم بر غضب بندم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل بیدرد تا کی مجلس آرای هوس باشد</p></div>
<div class="m2"><p>جنونی بشکند این شیشه تا راه حلب بندم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ندارد چون تامل شاهد نظم دقیق اینجا</p></div>
<div class="m2"><p>نقاط سکته من هم بر کلام منتخب بندم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هلاک‌ گریه‌های مستی‌ام ای اشک امدادی</p></div>
<div class="m2"><p>که بر مژگان بی نم خوشه‌ای چند از عنب بندم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به ستر حال چندان مایلم‌ کز پردهٔ اخفا</p></div>
<div class="m2"><p>اگر صبح قیامت‌ گل‌ کنم خود را به شب بندم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز مضمون دگر بیدل دماغم تر نمی‌گردد</p></div>
<div class="m2"><p>مگر در وصف مینا حرف تبخالی به‌ لب بندم</p></div></div>