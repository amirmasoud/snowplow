---
title: >-
    غزل شمارهٔ ۱۴۷۵
---
# غزل شمارهٔ ۱۴۷۵

<div class="b" id="bn1"><div class="m1"><p>اتفاق است آنکه هردشوار را آسان نمود</p></div>
<div class="m2"><p>ورنه از تدبیر یک ناخن ‌گره نتوان‌ گشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر به شهرت مایلی با بی‌نشانی ساز کن</p></div>
<div class="m2"><p>دهر نتواند نمودن آنچه عنقا وانمود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آرزو از نفی ما اثبا یار ایجاد کرد</p></div>
<div class="m2"><p>هرچه ازآثار مجنون‌ کاست بر لیلی فزود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صافی دل تهمت‌آلودکلف شد از حسد</p></div>
<div class="m2"><p>رنگ آب از سیلی امواج می‌گردد کبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حیف طبعی‌کز وبال‌کبر وکین آگاه نیست</p></div>
<div class="m2"><p>خاک ریزید از مزاری چند در چشم حسود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راحت این بزم برترک طمع موقوف بود</p></div>
<div class="m2"><p>دستها بر هم نهادیم از طلب‌، مژگان غنود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حسن یکتا بیدل از تمثال دارد انفعال</p></div>
<div class="m2"><p>جای زنگارت همین آیینه می‌باید زدود</p></div></div>