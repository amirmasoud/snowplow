---
title: >-
    غزل شمارهٔ ۱۶۲۰
---
# غزل شمارهٔ ۱۶۲۰

<div class="b" id="bn1"><div class="m1"><p>از کشمکش‌ کف تو می لاله‌گون ‌کشید</p></div>
<div class="m2"><p>دامن کشیدن تو ز دستم به خون کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پر منفعل دمید حبابم درین محیط</p></div>
<div class="m2"><p>جیبم سری نداشت ‌که باید برون‌ کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیش ازدمی به همت هستی نساخت صبح</p></div>
<div class="m2"><p>باری‌ست انفعال که نتوان فزون کشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیک و بد جهان هوس آهنگ جان‌کنی‌ست</p></div>
<div class="m2"><p>ما را صدای تیشه به این بیستون کشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قد خمیده ضامن رفع خمار کیست</p></div>
<div class="m2"><p>تا کی توان می از قدح‌ سرنگون‌ کشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشمت به عالم دگر افکند طرح ناز</p></div>
<div class="m2"><p>از ساغری‌ که می‌کشد آخر جنون ‌کشید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عریان تنی رسید به داد جنون من</p></div>
<div class="m2"><p>تا دامنم ز زحمت چندین فنون‌ کشید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>موهومی ام ز تهمت ایجاد بازداشت</p></div>
<div class="m2"><p>مشق عدم قلم به خط کاف و نون کشید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آخر شکست چینی دل بر ترنگ زد</p></div>
<div class="m2"><p>موی نهفته سر ز خمیرم ‌کنون ‌کشید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دست شکسته‌ام گل دامان یار کرد</p></div>
<div class="m2"><p>نقاشم انتقام ز بخت نگون‌کشید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل سواد نامه سیاهی نداشتم</p></div>
<div class="m2"><p>خطی چو سایه بر ورقم طبع دون ‌کشید</p></div></div>