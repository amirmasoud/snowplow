---
title: >-
    غزل شمارهٔ ۱۳۱۹
---
# غزل شمارهٔ ۱۳۱۹

<div class="b" id="bn1"><div class="m1"><p>شمعها زبن‌انجمن بی‌صرفه‌تازان رفته‌اند</p></div>
<div class="m2"><p>هر طرف سر بر هوا سوی‌گریبان رفته‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آشنایی با قماش بوی پیراهن‌کراست</p></div>
<div class="m2"><p>کاروانها با نگاه پیر کنعان رفته اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حسن یکتایی تو از وحشی‌نگاهان دم مزن</p></div>
<div class="m2"><p>از سواد غیرت لیلی غزالان رفته‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاک صحرای محبت نر‌گسستان نقش پاست</p></div>
<div class="m2"><p>مفت چشم ماکزین ده خو‌ش‌نگاهان رفته‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پان رفتار نفس جز دست بر هم سوده نیست</p></div>
<div class="m2"><p>رفته‌ها یکسر ازین وادی پشیمان رفته‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبح محشر کی دمد تا چشم عبرت واکنیم</p></div>
<div class="m2"><p>خوابناکان در خم دیوار مژگان رفته‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ابله شاید به داد هرزه‌ جولانی رسد</p></div>
<div class="m2"><p>تاگهر این موجها افتان و خیزان رفته ا‌ند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کیست با پیکان دلدوز قضاگردد طرف</p></div>
<div class="m2"><p>چون سخن تا رفته‌اند از لب پریشان رفته‌اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بزم امکان یک سحر پروانهٔ فرصت نداشت</p></div>
<div class="m2"><p>شمعها در داغ خوابیدند و یاران رفته‌اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کس ازین حرمان‌سرا با ساز جمعیت نرفت</p></div>
<div class="m2"><p>چون سخن تا رفته‌اند از لب پریشان رفته‌اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حرص راگفتم به پری قطع کن تارامید</p></div>
<div class="m2"><p>گفت دندانها پی آوردن نان رفته‌اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خامهٔ مژگان تر بیدل نکرد ایجاد خلق</p></div>
<div class="m2"><p>رنگها از کلک نقاش اشک ریزان رفته‌اند</p></div></div>