---
title: >-
    غزل شمارهٔ ۱۹۵۲
---
# غزل شمارهٔ ۱۹۵۲

<div class="b" id="bn1"><div class="m1"><p>داغم از کیفت آگاهی و اوهام هم</p></div>
<div class="m2"><p>جنس بسیار است و نقد فرصت ناکام کم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنقدر از شهرت هستی خجالت مایه‌ام</p></div>
<div class="m2"><p>کز نگین من چو شبنم می فروشد نام نم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کور شد چشمش ز سوزن‌کاری دست قضا</p></div>
<div class="m2"><p>پیش از آن ‌کز نرگس شوخت زند بادام دم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خجالت در لب‌ گل خنده شبنم می‌شود</p></div>
<div class="m2"><p>با تبسم آشنا گر سازد آن ‌گلفام فم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مژده ای لب تشنگان دشت بی‌آب جنون</p></div>
<div class="m2"><p>گریه‌ای دارم‌ که خواهد شد درین ایام یم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسکه فرصتها پر افشان هوای وحشت است</p></div>
<div class="m2"><p>از وصالم داغ دل می‌جوشد از پیغام غم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شوق کامل در تسلیها کم از جبریل نیست</p></div>
<div class="m2"><p>دل تپیدن ناز وحیی دارد و الهام هم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنچه ما در حلقهٔ داغ محبت دیده‌ایم</p></div>
<div class="m2"><p>نی سکندر دید در آیینه نی در جام جم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>محو دیدار تو دست از بحر امکان شسته است</p></div>
<div class="m2"><p>در سواد دیدهٔ حیران ندارد نام نم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>محمل موج نفس دوش تپیدن می‌کشد</p></div>
<div class="m2"><p>عافیت درکشور ما دارد از آرام رم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زین نشیمن نغمه‌ ی شوقی به سامان‌ کرده گیر</p></div>
<div class="m2"><p>سایهٔ دیوار دارد زیر و پشت بام بم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اهل دنیا را مطیع خویش‌ کردن‌ کار نیست</p></div>
<div class="m2"><p>پر به آسانی توان دادن به چوب خام خم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وعظ را نتوان به نیرنگ غرض بد نام‌ کرد</p></div>
<div class="m2"><p>این فسون بر هر که می‌خواهی برون دام دم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بی لب نوشین او بیدل به بزم عیش ما</p></div>
<div class="m2"><p>گشت مینا و قدح را باده در اجسام سم</p></div></div>