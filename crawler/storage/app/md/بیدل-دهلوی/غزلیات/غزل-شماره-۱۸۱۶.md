---
title: >-
    غزل شمارهٔ ۱۸۱۶
---
# غزل شمارهٔ ۱۸۱۶

<div class="b" id="bn1"><div class="m1"><p>ز بس دامان ناز افشاند زلف عنبر افشانش</p></div>
<div class="m2"><p>خط مشکین دمید آخر ز موج‌ گرد دامانش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز جوش شوخی چشم تماشا می‌کند پنهان</p></div>
<div class="m2"><p>به طوق قمریان نقش قدم سرو خرامانش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آن محفل‌که شوق آیینهٔ اسرار می‌گردد</p></div>
<div class="m2"><p>ندارد دل تپیدن غیر چشمکهای پنهانش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دل یکباره دشوار است قطع التفات او</p></div>
<div class="m2"><p>نگاهش بر نمی‌گردد اگر برگشت مژگانش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکست موج دارد عرض بی‌پروایی دریا</p></div>
<div class="m2"><p>من و آرایش رنگی‌کزو بستند پیمانش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به این رنگست اگر حیرت حضور قاتل ما را</p></div>
<div class="m2"><p>نیاراید روانی محمل خون شهیدانش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز فیض عشق دارد محو آن دیدار سامانی</p></div>
<div class="m2"><p>که صد آیینه باید ریخت از یک چشم حیرانش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فلک‌گر نسخهٔ جمعیت امکان زند بر هم</p></div>
<div class="m2"><p>تو روشن‌کن سواد سطری از زلف پریشانش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل بیمدعا یعنی بیاض ساده‌ای دارم</p></div>
<div class="m2"><p>به آتش می‌برم تا صفحه‌ای سازم زرافشانش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وجودم در عدم شاید به فکر خویش پردازد</p></div>
<div class="m2"><p>که آتش غیر خاکستر نمی‌باشدگریبانش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درین گلزار حیرت هرکه بسمل می‌شود بیدل</p></div>
<div class="m2"><p>چو اشک دیدهٔ شبنم تپیدن نیست امکانش</p></div></div>