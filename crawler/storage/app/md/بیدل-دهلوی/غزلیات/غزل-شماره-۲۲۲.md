---
title: >-
    غزل شمارهٔ ۲۲۲
---
# غزل شمارهٔ ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>تا دربن‌گلزار چون شبنم‌گذر داریم ما</p></div>
<div class="m2"><p>باده‌ای در جام عیش از چشم تر داریم ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سهل نبود در محیط دهر پاس اعتبار</p></div>
<div class="m2"><p>آبرویی چون‌گهر همراه سر داریم ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون صداهرچند در دام‌قس وامانده‌ایم</p></div>
<div class="m2"><p>از شکست خاطر خود بال وپر داریم ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کی به سیل‌گفتگو بنیاد ماگیرد خلل</p></div>
<div class="m2"><p>کوه تمکین خانه‌ای ازگوش‌کر داریم ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کس به‌تیغ سرکشی باما نمی‌گردد طرف</p></div>
<div class="m2"><p>اززمینگیری چو نقش پا سپر داریم ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شعلهٔ ما فال خاکستر زد و آسوده شد</p></div>
<div class="m2"><p>ای هوس بگذر، سری درزیرپر داریم‌ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رنگ‌ما از خاکساری برنمی‌دارد شکست</p></div>
<div class="m2"><p>چون علم‌،‌گردی ز میدان ظفر داریم ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از دل گرمی توان درکاینات آتش زدن</p></div>
<div class="m2"><p>ساز چندین‌گلخنیم ویک شرر داریم ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناله‌را ای دل به‌بادغم مده‌این رشته‌ای‌ست</p></div>
<div class="m2"><p>کزپی شیرازهٔ لخت جگر داریم ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فتنه‌ها از دستگاه زندگی‌گل کردنی‌ست</p></div>
<div class="m2"><p>از نفس‌، صبح قیامت در نظر داریم ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می‌رسیم آخر همان تا نقش پای خود چوشمع</p></div>
<div class="m2"><p>گر سراغ رنگهای رفته برداریم ما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل اندر جلوه‌گاه چین ابروی کسی</p></div>
<div class="m2"><p>کشتی نظاره در موج خطر داریم ما</p></div></div>