---
title: >-
    غزل شمارهٔ ۹۳۲
---
# غزل شمارهٔ ۹۳۲

<div class="b" id="bn1"><div class="m1"><p>جنون اندیشه‌ای بگذار تا دل بر هنر پیچد</p></div>
<div class="m2"><p>به‌دانش نازکن چندانکه سودایی به سر پیچد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حصول ‌کام با سعی املها برنمی‌آید</p></div>
<div class="m2"><p>عنان ریشه دشوار است تحصیل ثمر پیچد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگه محو جمال اوست اما چشم آن دارم</p></div>
<div class="m2"><p>که ‌دل ‌هم قطره‌ اشکی‌ گردد و بر چشم تر پیچد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آغوش نقابش تا قیامت‌ گل توان چیدن</p></div>
<div class="m2"><p>اگر بر عارض رنگین شبی از ناز درپیچد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تواند در تکلم شکرستان ریزد از گوهر</p></div>
<div class="m2"><p>لبی‌کز خامشی موج‌گهر را در شکر پیچد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صدای تیغ او می‌آید از هر موج این دریا</p></div>
<div class="m2"><p>در این اندیشه حیرانست دل تا از که سرپیچد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نفس هم برنمی‌دارد دماغ صبح نومیدی</p></div>
<div class="m2"><p>دعای ما کنون خود را به طومار دگر پیچد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوشا قطع امید و پرفشانیهای اندازش</p></div>
<div class="m2"><p>که صد عمر ابد در فرصت رقص شرر پیچد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به رنگ‌گردباد آن به‌که وحشت‌پرور شوقت</p></div>
<div class="m2"><p>بجای دامن پیچیده خود را برکمر پیچد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه امکان‌ست طی‌گردد بساط‌حسرت عاشق</p></div>
<div class="m2"><p>چو مژگان هر دو عالم را مگربریکدگرپیچد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تعین هرچه باشد خجلت دون‌همتی دارد</p></div>
<div class="m2"><p>به‌ کوتاهی‌ست‌ میل ‌رشته‌بر خود هر قدر پیچد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کسی بیدل به سعی‌وحشت از خود برنمی‌آید</p></div>
<div class="m2"><p>ز غفلت تاکجا گرداب ما از بحر سر پیچد</p></div></div>