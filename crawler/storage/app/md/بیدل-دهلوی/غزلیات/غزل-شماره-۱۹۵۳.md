---
title: >-
    غزل شمارهٔ ۱۹۵۳
---
# غزل شمارهٔ ۱۹۵۳

<div class="b" id="bn1"><div class="m1"><p>رفت فرصت ز کف اما من حیرت‌زده هم</p></div>
<div class="m2"><p>آنقدر دست ندارم‌که توان سود بهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حیرتم ‌گشت قفس ورنه درین عبرتگاه</p></div>
<div class="m2"><p>چون نگاهم همه تن جوهر آیینهٔ رم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمع عبرتگه دل نالهٔ داغ آلودست</p></div>
<div class="m2"><p>بایدم شاخ‌ گلی‌ کرد درین باغ علم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر خورشید به فتراک هوا می‌بندد</p></div>
<div class="m2"><p>گردنی‌ کز ادب تیغ تو می‌گردد خم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیخودی ‌گر ببرد خامه‌ام از چنگ شعور</p></div>
<div class="m2"><p>وصف چشمت به خط جام توان‌ کرد رقم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صافی دل مده از دست به اظهار کمال</p></div>
<div class="m2"><p>نسخهٔ آینه مپسند ز جوهر بر هم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشمهٔ فیض قناعت غم خشکی نکشد</p></div>
<div class="m2"><p>آب یاقوت به صد سال نمی‌گردد کم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آبرویی‌ که بود عاریتی روسیهی است</p></div>
<div class="m2"><p>جمله زنگ‌ست اگر آینه بردارد نم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غنچهٔ وا شده آغوش وداع رنگ‌ست</p></div>
<div class="m2"><p>به فسون دل خرم نتوان شد خرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حرف ناصح ز خیال تو نشد مانع ما</p></div>
<div class="m2"><p>آرزو نیست چراغی‌ که توان ‌کشت به‌ دم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عجز رفتار همان مرکز جمعیت ماست</p></div>
<div class="m2"><p>قدم از آبله آن به‌ که ندزدد شبنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کو مقامی ‌که توان مرکز هستی فهمید</p></div>
<div class="m2"><p>از زمین تا فلک آغوش ‌گشوده‌ست عدم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نامداری هوسی بیش ندارد بیدل</p></div>
<div class="m2"><p>به نگین راست نگردد خم پشت خاتم</p></div></div>