---
title: >-
    غزل شمارهٔ ۲۵۱۳
---
# غزل شمارهٔ ۲۵۱۳

<div class="b" id="bn1"><div class="m1"><p>ز شوخی تا قدح می‌گیرد آن بیدار مست من</p></div>
<div class="m2"><p>به چینی خانهٔ افلاک می‌خندد شکست من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیالش نقش امکان محو کرد از صفحهٔ شوقم</p></div>
<div class="m2"><p>به صورت‌ پی نبرد آیینهٔ معنی‌پرست من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو آن آتش که دود خویش داغ حسرتش دارد</p></div>
<div class="m2"><p>نگردید از ضعیفی سایهٔ من زیر دست من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به نظم عافیت در فتنه‌زار کشور هستی</p></div>
<div class="m2"><p>لب و چشمی‌ست‌ گر مقدور باشد بند و بست من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به تحقیق عدم افتادم و در خود نظر کردم</p></div>
<div class="m2"><p>گرفت آیینه نیز از امتیاز نیست هست من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به هر جا پا بیفشردم ز وحشت صرفه‌ کم بردم</p></div>
<div class="m2"><p>نگین نقشم‌،‌ گشاد بال و پر دارد نشست من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به رنگ غنچه لبریز بهار آفتم بیدل</p></div>
<div class="m2"><p>نفس‌ گر می‌کشم می‌آید آواز شکست من</p></div></div>