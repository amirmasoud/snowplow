---
title: >-
    غزل شمارهٔ ۱۸۰۸
---
# غزل شمارهٔ ۱۸۰۸

<div class="b" id="bn1"><div class="m1"><p>چو دریابد کسی رنگ ادای چشم خود کامش</p></div>
<div class="m2"><p>نهانتر از رگ خواب است موج باده در جامش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رساییها به فکر طرهٔ او خاک می‌بوسد</p></div>
<div class="m2"><p>مپرس از شانهٔ‌ کوتاه دست آغاز و انجامش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیال او مقیم چشم حیران است‌، می‌ترسم</p></div>
<div class="m2"><p>که آسیبی رساند جنبش مژگان بر اندامش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به ذوق شوخی آن جلوه چون آیینهٔ شبنم</p></div>
<div class="m2"><p>نگاهی نیست در چشمم ‌که حیرانی‌ کند رامش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تبسم ساغر صبح تمنای ‌که می‌گردد</p></div>
<div class="m2"><p>اگر یابی به صد دست دعا بردار دشنامش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر این باشد غرور شیوهٔ نازی‌که من دیدم</p></div>
<div class="m2"><p>به‌کام خویش هم مشکل‌که باشد لعل خودکامش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه امکان است دل را در خرامش ضبط خودکردن</p></div>
<div class="m2"><p>همه‌گر سنگ باشد بر شرر می‌بندد آرامش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر در خانهٔ آیینه حسنش پرتو اندازد</p></div>
<div class="m2"><p>چو جوهر لعمهٔ خورشید جوشد از در و بامش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه تنها در دل آیینه رنگ جلوه می‌خندد</p></div>
<div class="m2"><p>در آغوش نگینها هم تبسم می‌کند نامش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طواف خاک‌کویش آنقدر جهد طرب دارد</p></div>
<div class="m2"><p>که رنگ و بوی‌گل در غنچه‌ها می‌بندد احرامش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در آن محفل‌که حسن عالم آرایش بود ساقی</p></div>
<div class="m2"><p>فلک میناست می عیش ابد خورشید ومه جامش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز نخل آن قد دلجو نزاکت را تماشا کن</p></div>
<div class="m2"><p>که خم‌ گردیده شاخ ابرو از بار دو بادامش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>امید از وصل او مشکل که گردد داغ محرومی</p></div>
<div class="m2"><p>نفس تا می‌تپد بر خویش درکار است پیغامش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سر انگشت اشارات خطش با دیده می‌گوید</p></div>
<div class="m2"><p>حذر باید ز صیادی که خورشید است در دامش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مریض شوق بیدل هرگز آسودن نمی‌خواهد</p></div>
<div class="m2"><p>که ‌همچون نبض موج آخر کفن می‌گردد آرامش</p></div></div>