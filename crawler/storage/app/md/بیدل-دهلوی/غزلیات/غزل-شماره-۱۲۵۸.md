---
title: >-
    غزل شمارهٔ ۱۲۵۸
---
# غزل شمارهٔ ۱۲۵۸

<div class="b" id="bn1"><div class="m1"><p>گل نکرد آهی‌که بر ما خنجر قاتل نشد</p></div>
<div class="m2"><p>آرزو برهم نزد بالی‌که دل بسمل نشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دام محرومی درین دشت احتیاط آگهی‌ست</p></div>
<div class="m2"><p>وای بر صیدی‌که از صیاد خود غافل نشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل به راحت‌ گر نسازد با گدازش واگذار</p></div>
<div class="m2"><p>گوهر ما بحر خواهد گشت اگر ساحل نشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بیابانی که ما را سر به کوشش داده‌اند</p></div>
<div class="m2"><p>جاده هم از خویش رفت و محرم منزل نشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شعله را خاموش گشتن پای از خود رفتن است</p></div>
<div class="m2"><p>داغ هم گردیدم و آسودگی حاصل نشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرچه رنگ این دو آتشخانه از من ریختند</p></div>
<div class="m2"><p>از جبینم چون شرر داغ فنا زایل نشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اعتبار اندیشگان آفت‌پرست کاهشند</p></div>
<div class="m2"><p>هیچکس‌بی‌خودگدازی شمع این محفل نشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عافیت گر هست نقش پردهٔ واماندگی‌ست</p></div>
<div class="m2"><p>حیف پروازی‌که آگاه از پر بسمل نشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ذوق آغوش دویی در وصل نتوان یافتن</p></div>
<div class="m2"><p>بیخبرمجنون ما لیلی شد ومحمل نشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نی‌گداز دل به‌کار آمد نه ریزشهای اشک</p></div>
<div class="m2"><p>بی‌تومشت خاک من برباد رفت وگل نشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در لباس قطره نتوان تلخی دریاکشید</p></div>
<div class="m2"><p>مفت آن خونی‌که خاکستر شد امّا دل نشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غیرمن زین قلزم حیرت حبابی‌گل نکرد</p></div>
<div class="m2"><p>عالمی صاحبدل‌است امّاکسی بید‌ل نشد</p></div></div>