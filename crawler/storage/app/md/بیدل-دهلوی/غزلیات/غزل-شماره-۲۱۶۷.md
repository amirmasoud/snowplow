---
title: >-
    غزل شمارهٔ ۲۱۶۷
---
# غزل شمارهٔ ۲۱۶۷

<div class="b" id="bn1"><div class="m1"><p>خاموشم و بیتابی فریاد تو دارم</p></div>
<div class="m2"><p>چندانکه فراموش توام یاد تو دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این ناله‌ که قد می‌کشد از سینهٔ تنگم</p></div>
<div class="m2"><p>تصویر نهال ز غم آزاد تو دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تمثال‌ گل و رنگ بهارم چه فریبد</p></div>
<div class="m2"><p>من آینهٔ حسن خداداد تو دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرچند به صد رنگ زنم دست تصنع</p></div>
<div class="m2"><p>چون وانگرم خامهٔ بهزاد تو دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا زنده‌ام از جان‌کنی‌ام نیست رهایی</p></div>
<div class="m2"><p>شیرینی و من خدمت فرهاد تو دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گو شیشهٔ امکان شکند سنگ حوادث</p></div>
<div class="m2"><p>من طاقی از ابروی پریزاد تو دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرواز نفس یاد گرفتاری شوق است</p></div>
<div class="m2"><p>این یک دو پر از خانهٔ صیاد تو دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشمت به نگاهی ز جهان منتخبم‌کرد</p></div>
<div class="m2"><p>تمغای قبول از اثر صاد تو دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مطرب چه تراود ز نی‌بی‌نفس من</p></div>
<div class="m2"><p>هر ناله که من دارم از ارشاد تو دارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل تو به من هیچ مدارا ننمودی</p></div>
<div class="m2"><p>عمریست‌که پاس دل ناشاد تو دارم</p></div></div>