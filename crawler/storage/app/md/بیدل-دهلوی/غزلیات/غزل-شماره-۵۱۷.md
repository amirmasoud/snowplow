---
title: >-
    غزل شمارهٔ ۵۱۷
---
# غزل شمارهٔ ۵۱۷

<div class="b" id="bn1"><div class="m1"><p>آگاهی و افسردگی دل چه خیال است</p></div>
<div class="m2"><p>تا دانه به خود چشم‌گشوده‌ست نهال است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آیینهٔ‌گل از بغل غنچه برون نیست</p></div>
<div class="m2"><p>دل‌گر شکند سربسر آغوش وصال است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حیرتکدهٔ دهر جز اوهام چه دارد</p></div>
<div class="m2"><p>آبادکن خانهٔ آیینه خیال است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برفکربلند آن همه مغرورمباشید</p></div>
<div class="m2"><p>این جامهٔ نو، ناخنهٔ چشم‌کمال است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی فرصت عیش است درتن باغ‌که‌گل را</p></div>
<div class="m2"><p>گرگردش رنگی‌ست همان‌گردش سال است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از ریشهٔ نظاره دماندیم تحیر</p></div>
<div class="m2"><p>بالیدگی داغ مه از جسم هلال است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در خلوت دل ازتو تسلی نتوان شد</p></div>
<div class="m2"><p>چیزی‌که در آیینه توان دید مثال است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرگام به راه طلبت رفته‌ام از خویش</p></div>
<div class="m2"><p>نقش قدمم آینهٔ گردش حال است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرجا روم از روز سیه چاره ندارم</p></div>
<div class="m2"><p>بی‌روی تو عالم همه یک چشم غزال است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن مشت غبارم‌که به آهنگ تپیدن</p></div>
<div class="m2"><p>در حسرت دامان نسیمش پر و بال است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای ذره مفرسای بپرداز توهم</p></div>
<div class="m2"><p>خورشید هم از آینه‌داران زوال است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل من و آن دولت بی‌دردسر فقر</p></div>
<div class="m2"><p>کز نسبت او چینی خاموش سفال است</p></div></div>