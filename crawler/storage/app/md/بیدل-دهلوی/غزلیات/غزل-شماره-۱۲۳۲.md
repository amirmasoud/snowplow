---
title: >-
    غزل شمارهٔ ۱۲۳۲
---
# غزل شمارهٔ ۱۲۳۲

<div class="b" id="bn1"><div class="m1"><p>تا دل دیوانه واماند از تپیدن داغ شد</p></div>
<div class="m2"><p>اضطراب این سپند از آرمیدن داغ شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچکس‌چون نقش‌پا از خاک‌راهم برنداشت</p></div>
<div class="m2"><p>این‌گل محرومی از درد نچیدن داغ شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می دهد سعی طلب عرض سراغ منزلم</p></div>
<div class="m2"><p>نادویدنها ز درد نارسیدن داغ شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غافلم از حسنش اما اینقدر دانم‌که دوش</p></div>
<div class="m2"><p>برق‌حیرت جلوه‌ای دیدم‌که دیدن داغ شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برق بردل ریخت آخر حسرت نشو و نما</p></div>
<div class="m2"><p>چون شرر این دانه از شوق دمیدن داغ شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از جنون‌پیمایی طاووس بیتابم‌- مپرس</p></div>
<div class="m2"><p>پر زدم چندان‌ که در بالم پریدن داغ شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محو دیدارکه‌ام کز دورباش جلوه‌اش</p></div>
<div class="m2"><p>برمژه هرقطره اشکم تا چکیدن داغ شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاقبت ‌گردنکشان ‌را طو‌ق‌ گردن نقش‌ پاست</p></div>
<div class="m2"><p>شعله هم اینجا به‌ جرم سر کشیدن داغ شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آب درآیینه آخر فال حیرت می‌زند</p></div>
<div class="m2"><p>آنقدر از پا نشستم کارمیدن داغ شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غیر عبرت شمع من زین انجمن حاصل نکرد</p></div>
<div class="m2"><p>انچه در دیدن گلش بود از ندیدن داغ شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ناله‌ای ‌کردم به‌ گلشن بیدل از شوق گلی</p></div>
<div class="m2"><p>لاله‌ها را پنبهٔ ‌گوش از شنیدن داغ شد</p></div></div>