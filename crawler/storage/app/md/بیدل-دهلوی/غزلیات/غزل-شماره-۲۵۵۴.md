---
title: >-
    غزل شمارهٔ ۲۵۵۴
---
# غزل شمارهٔ ۲۵۵۴

<div class="b" id="bn1"><div class="m1"><p>ز پرده آیی اگر از قبای تنگ برون</p></div>
<div class="m2"><p>به‌روی ‌گل ننشیند ز شرم رنگ برون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیال آن مژه خون می‌کند چه چاره‌ کنم</p></div>
<div class="m2"><p>دل آب‌ گشت و نمی‌آید این خدنگ برون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زمانه مجمع آیینه‌های ناصاف است</p></div>
<div class="m2"><p>درون صفا ز کدورت نشسته زنگ برون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حذر کنید ز کینی که از دو دل خیزد</p></div>
<div class="m2"><p>شرار کوفته می‌آید از دو سنگ برون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بساط صلح‌ گر از عافیت نگردد تنگ</p></div>
<div class="m2"><p>کسی ز خانه نیاید به‌عزم جنگ برون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهار عالم انصاف گر به‌ این رنگست</p></div>
<div class="m2"><p>نرفته است مسلمانی از فرنگ برون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به لاف پیش مبر دعوی توانایی</p></div>
<div class="m2"><p>که خارتنگ نیاید ز پای لنگ برون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز طعن تیره درونان خدا نگهدارد</p></div>
<div class="m2"><p>نفس جنون زده می‌آید از تفنگ برون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دریغ محرمی دل نصیب فطرت نیست</p></div>
<div class="m2"><p>نشسته‌ایم ز آیینه همچو زنگ برون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تعلقات جهان حکم نیستان دارد</p></div>
<div class="m2"><p>نشد صدا هم ازین کوچه‌های تنگ برون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هزار سنگ به دل‌ کوفتیم لیک چه سود</p></div>
<div class="m2"><p>میی نیامد ازین شیشه جز ترنگ برون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نفس نیاز خرام که می‌کنی بیدل</p></div>
<div class="m2"><p>که سنگ سبزه نیارد به‌این درنگ برون</p></div></div>