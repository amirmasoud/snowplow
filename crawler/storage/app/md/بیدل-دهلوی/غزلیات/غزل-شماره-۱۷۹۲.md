---
title: >-
    غزل شمارهٔ ۱۷۹۲
---
# غزل شمارهٔ ۱۷۹۲

<div class="b" id="bn1"><div class="m1"><p>دل بی‌مدعا رنگی ندارد تا کنم فاشش</p></div>
<div class="m2"><p>صدف در حیرت آیینه گم کرده‌ست نقاشش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین محفل نیاوردند از تاریکی دلها</p></div>
<div class="m2"><p>چراغی را که باشد امتیاز از چشم خفاشش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان رنگ با تغییر وضع خود جدل دارد</p></div>
<div class="m2"><p>به هر جا شیشه و سنگی است با وهم است پرخاشش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به تشویش دل مأیوس رنجی نیست مفلس را</p></div>
<div class="m2"><p>شکست کاسه در بزم کرم کرده‌ست بی‌آشش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به این شرمی که می‌بیند کریم از جبههٔ سایل</p></div>
<div class="m2"><p>گهر هم سرنگون می‌افتد از دست گهرپاشش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به ملک بی‌نیازی رو که‌ گاه احتیاج آنجا</p></div>
<div class="m2"><p>چوناخن می‌کشد درهم به پشت دست قلاشش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خط لوح امل جز حک زدن چیزی نمی‌ارزد</p></div>
<div class="m2"><p>همه‌گر ریش زاهد در خیال آید که بتراشش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شئون هر صفت مستوری عاشق نمی‌خواهد</p></div>
<div class="m2"><p>کفن هر چند پوشد ذوق عریانیست نباشش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بساط زندگی مفت حضور اما به دل جاکو</p></div>
<div class="m2"><p>نفس می‌گسترد در خانهٔ آیینه فراشش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ندارد کاوش دل صرفهٔ امن ‌کسی بیدل</p></div>
<div class="m2"><p>در این ناسور توفانهای خون خفته‌ست مخراشش</p></div></div>