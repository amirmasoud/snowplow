---
title: >-
    غزل شمارهٔ ۸۳۱
---
# غزل شمارهٔ ۸۳۱

<div class="b" id="bn1"><div class="m1"><p>آغاز نگاهم به قیامت نظری داشت</p></div>
<div class="m2"><p>واکردن مژگان چراغم سحری داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوابم چه خیال است به گرد مژه گردد</p></div>
<div class="m2"><p>بالین من‌گم شده آرام پری داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشمی به تحیرکدهٔ دل نگشودیم</p></div>
<div class="m2"><p>آیینه همین خانهٔ بیرون دری داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما بیخبران بیهده بر ناله تنیدیم</p></div>
<div class="m2"><p>تا دل نفس سوخته هم نامه‌بری داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قاصد، ز رموز جگر چاک چه‌گوید</p></div>
<div class="m2"><p>درنامهٔ عشاق دریدن خبری داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آخرگروه حیرت ما باز نگردید</p></div>
<div class="m2"><p>او بودکه هر چشم‌گشودن دگری داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کردیم تماشای ترقی و تنزل</p></div>
<div class="m2"><p>آیینهٔ ما هر نفس از ما بتری داشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زین بحر، عیارطلب موج‌گرفتیم</p></div>
<div class="m2"><p>آن پای که فرسود به دامن گهری داشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آگاه نشد هیچ‌کس از رمز حلاوت</p></div>
<div class="m2"><p>ورنه لب خاموش گره نیشکری داشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی‌شعله نبود آنچه تو دیدی‌گل داغش</p></div>
<div class="m2"><p>هر نقش قدم یک دو نفس پیش سری داشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با لفظ نپرداختی ای غافل معنی</p></div>
<div class="m2"><p>تحقیق پری در نفس شیشه‌گری داشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آسان نرسیدیم به هنگامهٔ دیدار</p></div>
<div class="m2"><p>ای بیخبران آینه دیدن جگری داشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عریانی‌ام ازکسوت تشویش برآورد</p></div>
<div class="m2"><p>رفت آنکه جنونم هوس جامه‌دری داشت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیدل چقدر غافل‌کیفیت خویشم</p></div>
<div class="m2"><p>من آینه در دست وتماشا دگری داشت</p></div></div>