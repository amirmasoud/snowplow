---
title: >-
    غزل شمارهٔ ۳۵۹
---
# غزل شمارهٔ ۳۵۹

<div class="b" id="bn1"><div class="m1"><p>بی‌کمالی‌نیست دل از شرم چون می‌گردد آب</p></div>
<div class="m2"><p>از عرق آیینهٔ ما را فزون می‌گردد آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دم گرم مراقب‌طینتان غافل مباش</p></div>
<div class="m2"><p>کزشرارتیشه اینجا بیستون می‌گردد آب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تاب خودداری ندارد صاف طبع از انفعال</p></div>
<div class="m2"><p>می‌شودمطلق‌عنان‌چون‌سرنگون‌می‌گردد آب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کیست‌کز مرکز جداگردیدنش زنگی نباخت</p></div>
<div class="m2"><p>خون دل از دیده تا گردد برون می‌گردد آب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در محبت گریه تدبیرکدورتها بس است</p></div>
<div class="m2"><p>گرغشی‌داری به صافی رهنمون می‌گردد آب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سوز دل چون شمعم از افسردگیها شد عرق</p></div>
<div class="m2"><p>آنچه آتش بود در چشمم‌کنون می‌گردد آب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیل آفت می‌کند معماری بنیاد شرم</p></div>
<div class="m2"><p>خانه‌آرایان گوهر را ستون می‌گردد آب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منتهای‌کار سالک می‌شود همرنگ درد</p></div>
<div class="m2"><p>چون‌زشاخ‌وبرگ‌درگل‌رفت‌خون‌می‌گردد آب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همچو شبنم سیر اشک ما به دامان هواست</p></div>
<div class="m2"><p>درگلستان محبت واژگون می‌گردد آب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دام سودا می‌کند دل را هجوم احتیاج</p></div>
<div class="m2"><p>ازفسون موج زنجیر جنون می‌گردد آب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل چه باشد تا نگردد خون به یاد طره‌اش</p></div>
<div class="m2"><p>گر همه‌سنگ‌است بیدل زین‌فسون می‌گردد آب</p></div></div>