---
title: >-
    غزل شمارهٔ ۲۲۳۰
---
# غزل شمارهٔ ۲۲۳۰

<div class="b" id="bn1"><div class="m1"><p>به صدگردون تسلسل بست دور ساغر عشقم</p></div>
<div class="m2"><p>که گردانید یارب اینقدر گرد سر عشقم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیاهی می‌کنم اما برون از رنگ پیدایی</p></div>
<div class="m2"><p>غبار عالم رازم سواد کشور عشقم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه دنیا عبرت آموزم نه عقبا حسرت اندوزم</p></div>
<div class="m2"><p>به هیچ آتش نمی‌سوزم سپند مجمر عشقم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به صیقل‌کم نمی‌گردد غرور زنگ خودبینی</p></div>
<div class="m2"><p>مگر آیینه بر سنگی زند روشنگر عشقم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عنان بگسست عمر و من همان خاک درش ماندم</p></div>
<div class="m2"><p>نشد این بادبان آخر حریف لنگر عشقم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غمم‌، دردم‌، سرشکم‌، ناله‌ام‌، خون دلم‌، داغم</p></div>
<div class="m2"><p>نمی‌دانم عرض ‌گل ‌کرده‌ام یا جوهر عشقم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گهی‌صلحم ،‌گهی‌جنگم‌،‌گهی‌مینا ،‌گهی‌سنگم</p></div>
<div class="m2"><p>دو عالم‌گردش رنگم جنون ساغر عشقم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو شمع ازگردنم حق وفا ساقط نمی‌گردد</p></div>
<div class="m2"><p>درآتش هم عرق دارم خجالت پرور عشقم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نی‌ام نومید اگر روزی دو احرام هوس دارم</p></div>
<div class="m2"><p>که من چون داغ هر جا حلقه ‌گشتم بردر عشقم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه فخرکعبه دلخواهم نه ننگ دیر اکراهم</p></div>
<div class="m2"><p>سر تسلیم و فرش هر چه خواهی چاکر عشقم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ندارد موی مجنون شانه‌ای غیر از پریشانی</p></div>
<div class="m2"><p>چه امکانست بیدل جمع‌ گردم دفتر عشقم</p></div></div>