---
title: >-
    غزل شمارهٔ ۲۵۳۹
---
# غزل شمارهٔ ۲۵۳۹

<div class="b" id="bn1"><div class="m1"><p>تا فلک بر باد ناکامی دهد تسکین من</p></div>
<div class="m2"><p>همچو اخگر پنبه بیرون ریخت از بالین من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیخودی را رونق بزم حضورم‌ کرده‌اند</p></div>
<div class="m2"><p>رنگهای رفته می‌بندد چو شمع آیین من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرد رفتارت پری افشاند در چشم ترم</p></div>
<div class="m2"><p>دهر شد طاووس خیز ازگریهٔ رنگین من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین‌ گلستان دامنی بر چیده‌ام مانند صبح</p></div>
<div class="m2"><p>کز گریبان فلک دارد تبسم چین من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موج این بحر جنون هنگام توفان مشربی‌ست</p></div>
<div class="m2"><p>نیست بی‌تجدید وحشت الفت دیرین من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ذوق آگاهی به چندین شبهه‌ام پامال‌ کرد</p></div>
<div class="m2"><p>عالم تمثال شد آیینهٔ خود بین من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسکه چون‌ گوهر قناعت در مزاجم پا فشرد</p></div>
<div class="m2"><p>موج زد ابرام و نگذشت از پل تمکین من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بستن چشمی‌ست تسخیر جهات امّا چه سود</p></div>
<div class="m2"><p>داد گیرایی به حیرت چنگل شاهین من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناروایی معنی‌ام را بسکه در پستی نشاند</p></div>
<div class="m2"><p>خاک می‌لیسد زبان عبرت از تحسین من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از شکست دل خیال نازکی گل کرده‌ام</p></div>
<div class="m2"><p>واکشید از موی چینی مصر‌ع تضمین من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شخص عبرت بی‌ندامت قابل ارشاد نیست</p></div>
<div class="m2"><p>از صدای دست بر هم سوده‌ کن تلقین من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شکوهٔ افسردگی بیدل‌ کجا باید شمرد</p></div>
<div class="m2"><p>ناله در نقش نگین خفت از دل سنگین من</p></div></div>