---
title: >-
    غزل شمارهٔ ۱۹۹
---
# غزل شمارهٔ ۱۹۹

<div class="b" id="bn1"><div class="m1"><p>چه‌ممکن است‌که راحت سری برآورد از ما</p></div>
<div class="m2"><p>مگر نفس رود و دیگری برآورد از ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به عرصهٔ دو نفس انقلاب فرصت هستی</p></div>
<div class="m2"><p>گمان نبودکه دل لشکری برآورد از ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو رنگ عهدهٔ ناموس وحشتیم به‌گردن</p></div>
<div class="m2"><p>ز خوبش هرکه برآید پری برآورد از ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شرارکاغذ اگر در خیال بال گشاید</p></div>
<div class="m2"><p>جنون به حکم وفا مجمری برآورد ازما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دماغ ما سر غواصی محیط ندارد</p></div>
<div class="m2"><p>بس است ضبط نفس‌گوهری برآورد از ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فلک ز صبح قیامت فکنده شور به عالم</p></div>
<div class="m2"><p>مباد پنبهٔ گوش‌کری برآورد از ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فسرده‌ایم به زندان عقل چاره محال است</p></div>
<div class="m2"><p>جنون مگرکه قیامت‌گری برآورد از ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به رنگ غنچه نداریم برگ عشرت دیگر</p></div>
<div class="m2"><p>شکست شیشه مگر ساغری برآورد از ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهار بیخودی افسوس گل نکرد زمانی</p></div>
<div class="m2"><p>که رنگ رفته چمن پیکری برآورد از ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در انتظار رهایی نشسته‌ایم که شاید</p></div>
<div class="m2"><p>به روی ما مژه بستن دری برآورد ازما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو بیدلیم همه ناگزیر نامه سیاهی</p></div>
<div class="m2"><p>جبین مگربه عرق‌کوثری برآورد ازما</p></div></div>