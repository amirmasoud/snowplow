---
title: >-
    غزل شمارهٔ ۷۴۴
---
# غزل شمارهٔ ۷۴۴

<div class="b" id="bn1"><div class="m1"><p>بی‌رخت در چشمهٔ آیینه خاک است آب نیست</p></div>
<div class="m2"><p>چشم مخمل را ز شوق پای‌بوست خواب نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بعد کشتن خون ما رنگ است در پرواز شوق</p></div>
<div class="m2"><p>آب و خاک بسملت از عالم سیماب نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شوخی مهتاب و تمکین کتان پر ظاهر است</p></div>
<div class="m2"><p>بر بنای صبر ما شوقت کم از سیلاب نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کی تواند آینه عکس ترا در دل نهفت</p></div>
<div class="m2"><p>ضبط این گوهر به چنگ سعی هر گرداب نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سایه را آیینهٔ خورشید بودن مشکل است‌</p></div>
<div class="m2"><p>خود به خود در جلوه باش اینجا کسی را تاب نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خرقه از لخت جگر چون غنچه در بر کرده‌ایم</p></div>
<div class="m2"><p>در دیار ما قماش دل درستی‌ باب نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای حباب از سادگی دست دعا بالا مکن</p></div>
<div class="m2"><p>در محیط عشق جز موج خطر محراب نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برگ‌برگ این گلستان پرده‌دار غفلت است</p></div>
<div class="m2"><p>غنچهٔ بیدار اگر گل گشت گل بی‌خواب نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دور نبود گر فلک پیچد به خویش از ناله‌ام</p></div>
<div class="m2"><p>دود را از شعله حاصل غیر پیچ و تاب نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا توانی چون نسیم آزادگی از کف مده</p></div>
<div class="m2"><p>آشنای رنگ جمعیت گل اسباب نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از فروغ این شبستان دست باید شست و بس</p></div>
<div class="m2"><p>آب گردیده‌ست سامان طرب مهتاب نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل از احباب دنیا چشم سرسبزی مدار</p></div>
<div class="m2"><p>کشت این شطرنج‌بازان دغل سیراب نیست</p></div></div>