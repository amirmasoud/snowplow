---
title: >-
    غزل شمارهٔ ۲۳۱۳
---
# غزل شمارهٔ ۲۳۱۳

<div class="b" id="bn1"><div class="m1"><p>نه خط شناس امیدم نه درس محرم بیم</p></div>
<div class="m2"><p>به ‌حیرتم‌ که محبت چه می‌کند تعلیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیاکه منتظرانت چو دیدهٔ یعقوب</p></div>
<div class="m2"><p>فضای کلبهٔ احزان گرفته‌اند نسیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز نسبت دهنت بسکه لذت اندود است</p></div>
<div class="m2"><p>بهم دو بوسه زند لب دم تکلم میم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بغیر سجده ز سیمای عجز ما مطلب</p></div>
<div class="m2"><p>جبین سایه و آیینه داری تسلیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه شد زبان تمنا خموش آهنگست</p></div>
<div class="m2"><p>نگاه نامهٔ سایل بس است سوی‌کریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به یاس گرد هوسهایم از نظر برخاست</p></div>
<div class="m2"><p>نفس‌گداخته را رنگ می‌کند تعظیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به رنگ پسته لب از جوش خون ندوخته‌ام</p></div>
<div class="m2"><p>حذر که صورت منقار من دلی‌ست دو نیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فتادگی همه جا خضر مقصد ضعفاست</p></div>
<div class="m2"><p>عصای جاده همان می‌کشد خط تسلیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عبث متاز که خونت به‌ خاک می‌ریزد</p></div>
<div class="m2"><p>سرشک را قدم جرات خودست غنیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پی حقیقت نیک و بد گذشته مگیر</p></div>
<div class="m2"><p>خطوط وهم مپیما که‌ کهنه شد تقویم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز شور وحدت و کثرت به درد سر نروی</p></div>
<div class="m2"><p>حدیث ذره و خورشید مبحثی است قدیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرو به صومعه کانجا نمی‌توان دیدن</p></div>
<div class="m2"><p>به وهم خلد، جهانی گرفته کنج جحیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در آن بساط که کهسار ناله پرداز است</p></div>
<div class="m2"><p>غبار ماست هوس مردهٔ امید نسیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>غبار شمع به تاراج رنگ باخته رفت</p></div>
<div class="m2"><p>متاع عاریت ما به هیچ شد تقسیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>درون پردهٔ هستی تردد انفاس</p></div>
<div class="m2"><p>اشاره‌ای‌ست که اینجا مسافر است مقیم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دل گداخته مضمون گوهر دگر است</p></div>
<div class="m2"><p>محیط آب شد اما نبست اشک یتیم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو ابر دست به دامان اشک زن بیدل</p></div>
<div class="m2"><p>مگر به ‌گریه برآید سیاهی‌ات ز گلیم</p></div></div>