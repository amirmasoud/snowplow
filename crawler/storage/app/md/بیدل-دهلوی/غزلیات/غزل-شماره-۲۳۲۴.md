---
title: >-
    غزل شمارهٔ ۲۳۲۴
---
# غزل شمارهٔ ۲۳۲۴

<div class="b" id="bn1"><div class="m1"><p>گر در هوای او قدمی پیش رفته‌ایم</p></div>
<div class="m2"><p>مانند شبنم از گره خویش رفته‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قید جهات مانع پرواز رنگ نیست</p></div>
<div class="m2"><p>از حیرت اینقدر قفس اندیش رفته‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنجاکه نقش جبههٔ تسلیم جاده است</p></div>
<div class="m2"><p>آسوده‌ایم اگر همه در نیش رفته‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا لب‌گشوده‌ایم به دریوزهٔ امید</p></div>
<div class="m2"><p>چون آبرو ز کیسهٔ درویش رفته‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاهد فسون زهد رها کن‌ که عمرهاست</p></div>
<div class="m2"><p>ما هم چو شانه از ته این ریش رفته‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دنیا و صد معامله عقبا و صد خیال</p></div>
<div class="m2"><p>ما بیخودان به چنگ چه تشویش رفته‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غواص درد را به محیط‌گهر چه‌کار</p></div>
<div class="m2"><p>اخگر صفت فرو به دل ریش رفته‌ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در آفتاب سایه سراغ چه می‌کند</p></div>
<div class="m2"><p>از خویش تا تو آمده‌ای پیش رفته‌ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با هیچ ذره راست نیاید حساب ما</p></div>
<div class="m2"><p>از بس که در شمار کم و بیش رفته ایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل نشاط دهر مآلش ندامت‌ست</p></div>
<div class="m2"><p>چون‌گل ازبن چمن همه تن ریش رفته‌ایم</p></div></div>