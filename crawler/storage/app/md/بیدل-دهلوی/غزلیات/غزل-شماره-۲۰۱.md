---
title: >-
    غزل شمارهٔ ۲۰۱
---
# غزل شمارهٔ ۲۰۱

<div class="b" id="bn1"><div class="m1"><p>چون صبح مجو طاقت آزارکس از ما</p></div>
<div class="m2"><p>کم نیست‌که ما را به درآرد نفس ازما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما قافلهٔ بی‌نفس موج سرابیم</p></div>
<div class="m2"><p>چندین عدم آن‌سوست صدای جرس ازما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردیم به ضبط نفس ولب نگشودیم</p></div>
<div class="m2"><p>تا بوی تظلم نبرد دادرس از ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عمری‌ست دراین انجمن ازضعف دوتاییم</p></div>
<div class="m2"><p>خلخال رسانید به پای مگس از ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همت نزندگل به سر ناز فضولی</p></div>
<div class="m2"><p>رنگ آینه بشکست به روی هوس ازما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پر ناکس ازین مزرعهٔ یأس دمیدیم</p></div>
<div class="m2"><p>بر چشم توقع مگذارید خس از ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درگرد خیال تو سراغی است وگرنه</p></div>
<div class="m2"><p>چیزی دگر از ما نتوان یافت پس از ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رنگ آینهٔ الفت گل هیچ نپرداخت</p></div>
<div class="m2"><p>قانع به دل چاک شد آخر قفس از ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما را ننشانیدکسی بر سر رهش</p></div>
<div class="m2"><p>بیدل تو پذیری مگر این ملتمس از ما</p></div></div>