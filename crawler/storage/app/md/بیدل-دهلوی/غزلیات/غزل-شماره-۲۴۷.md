---
title: >-
    غزل شمارهٔ ۲۴۷
---
# غزل شمارهٔ ۲۴۷

<div class="b" id="bn1"><div class="m1"><p>می‌خورد خون نفس اندر دل غم پیشهٔ ما</p></div>
<div class="m2"><p>جوهرتیغ بود خارو خس بیشهٔ ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس که چون شمع به غم نشوونما یافته‌ایم</p></div>
<div class="m2"><p>شعله را موج طراوت شمرد ریشهٔ ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سختی دهر ز صبر دل ما زنهاری‌ست</p></div>
<div class="m2"><p>آب شد طاقت سنگ ازجگر شیشهٔ ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قد خم‌گشه همان ناخن فرهاد غم است</p></div>
<div class="m2"><p>سعی بیجاست به جز جان‌کنی ازتیشهٔ ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شغل رسوایی و مستوری احوال بلاست</p></div>
<div class="m2"><p>کاش آرایش بازار دهد پیشهٔ ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شور زنجیر جنون از نفس ما پیداست</p></div>
<div class="m2"><p>نکهت زلف‌که پیچیده بر اندیشهٔ ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم امید نداریم زکشت دگران</p></div>
<div class="m2"><p>دل ما دانهٔ ما، نالهٔ ما، ریشهٔ ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خامشیها سبق مکتب بیتابی نیست</p></div>
<div class="m2"><p>یک قلم ناله بود مشق نی پیشهٔ ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نشئهٔ مشرب بیرنگی ازآن صافترست</p></div>
<div class="m2"><p>که شود موج پری درّد ته شیشهٔ ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل از فطرت ما قصر معانی‌ست بلند</p></div>
<div class="m2"><p>پایه دارد سخن ازکرسی اندیشهٔ ما</p></div></div>