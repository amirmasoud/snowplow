---
title: >-
    غزل شمارهٔ ۱۸۶۰
---
# غزل شمارهٔ ۱۸۶۰

<div class="b" id="bn1"><div class="m1"><p>نی در پرواز زد، نی‌ سعی جولان کرد شمع</p></div>
<div class="m2"><p>تا به نقش پا همین سیر گریبان‌ کرد شمع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خودگدازی محرم اسرار امکان گشتن است</p></div>
<div class="m2"><p>هر قدر در آب خفت آیینه سامان‌ کرد شمع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل اگر روشن نمی‌شد داغ آگاهی ‌که داشت</p></div>
<div class="m2"><p>اینقدر ما را درین هنگامه حیران‌ کرد شمع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غفلت این انجمن درخورد اغماض دل است</p></div>
<div class="m2"><p>عالمی را چشم پوشانید و عریان ‌کرد شمع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیخودی‌ کن از بهار عافیت غافل مباش</p></div>
<div class="m2"><p>رنگ ها پرواز داد و گل به دامان ‌کرد شمع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر رخ ما ناز مشتاقان در مژگان مبند</p></div>
<div class="m2"><p>کز تغافل خانهٔ پروانه ویران‌ کرد شمع</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل نه قدر آه فهمید و نه پاس اشک داشت</p></div>
<div class="m2"><p>سبحه و زنار را با خاک یکسان‌ کرد شمع</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درگشاد عقدهٔ هستی که دندانگیر نیست</p></div>
<div class="m2"><p>از بن هر قطره اشک ایجاد دندان‌ کرد شمع</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا کجا زبن انجمن چشم هوس پوشد کسی</p></div>
<div class="m2"><p>عضو عضو خویش اینجا صرف مژگان ‌کرد شمع</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نور دل در ترک لذات جهان خوابیده است</p></div>
<div class="m2"><p>موم تا آلودهٔ شهد است نتوان‌ کرد شمع</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیستی بیدل به داد خود نمایی می‌رسد</p></div>
<div class="m2"><p>عاقبت خود را به رنگ رفته پنهان‌ کرد شمع</p></div></div>