---
title: >-
    غزل شمارهٔ ۱۵۸۰
---
# غزل شمارهٔ ۱۵۸۰

<div class="b" id="bn1"><div class="m1"><p>بیخودی امشب پر و بال فغانی می‌شود</p></div>
<div class="m2"><p>گر ندارد مدعا باری بیانی می‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچ وضعی درطریق جستجوبیکارنیست</p></div>
<div class="m2"><p>پای خواب‌آلود هم سنگ نشانی می‌شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشئهٔ تسلیم حاصل‌کن‌که مشتی خاک را</p></div>
<div class="m2"><p>باد هم‌ گر می برد تخت روانی می‌ شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>موج این دریا به سعی ناخدا محتاج نیست</p></div>
<div class="m2"><p>کشتی ما را شکستن بادبانی می‌شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون لطافت تهمت‌آلود کدورت شد بلاست</p></div>
<div class="m2"><p>سایهٔ بال پری کوه گرانی می‌شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رخ مپوش از من که چشم حسرت آهنگ مرا</p></div>
<div class="m2"><p>هر سر مژگان پر و بال فغانی می‌شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاجزم چندانکه در عرض ضعیفیهای من</p></div>
<div class="m2"><p>ناله ‌گر باشد نگاه ناتوانی می‌شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر چنین باشد فشار حسرت بال هما</p></div>
<div class="m2"><p>مغزها آخر ز خشکی استخوانی می‌شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسکه گرمیهای صحبت پرفشان وحشت است</p></div>
<div class="m2"><p>آتش این کاروان هم کاروانی می‌شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>راحت جاوید در ضبط عنان آرزوست</p></div>
<div class="m2"><p>بال و پرگر جمع ‌گردد آشیانی می‌شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سیر حق بیدل بقدر ترک اسباب است و بس</p></div>
<div class="m2"><p>سوی او از هرچه برگردی عنانی می‌شود</p></div></div>