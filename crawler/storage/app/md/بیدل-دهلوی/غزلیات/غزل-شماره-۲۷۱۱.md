---
title: >-
    غزل شمارهٔ ۲۷۱۱
---
# غزل شمارهٔ ۲۷۱۱

<div class="b" id="bn1"><div class="m1"><p>درین مکتب‌ که باز آن طفل بازیگر کند بازی</p></div>
<div class="m2"><p>که از علم آنچه تعلیمش دهی از برکند بازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به قانون ادب سازان بزم دل چه پردازد</p></div>
<div class="m2"><p>هوس مستی‌ که جای باده در ساغر کند بازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشاط طبع در ترک تکلف بیش می‌باشد</p></div>
<div class="m2"><p>به خاک از فرش زرین طفل رنگین تر کند بازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اسیر چرخم و شد عمرها کز شوق می‌خواهم</p></div>
<div class="m2"><p>سپندم یک تپش بیرون این مجمرکند بازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمی‌دانم چه پردازد هوس در خانهٔ گردون</p></div>
<div class="m2"><p>مگر باگردکانی چند ازین اخترکند بازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به غیر از سوختن چیزی ندارد فرصت‌ کارت</p></div>
<div class="m2"><p>شرر اول به دود آخر به خاکستر کند بازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به خاک ز لهو مفکن جوهر پرداز همت را</p></div>
<div class="m2"><p>کبوتر مایل پستی‌ست هرگه سرکند بازی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدو نیک جهان رقاص وهم هستی است اما</p></div>
<div class="m2"><p>کجا رندی‌کزین بازیچه بیرونترکند بازی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نگه‌گر نیستی اشکی شو و از خویش بیرون آ</p></div>
<div class="m2"><p>چو مژگان چند پروازت به بال و پرکند بازی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قد پیری نمودارست طفلی تا به‌کی بیدل</p></div>
<div class="m2"><p>کچه در خاک پنهان‌کن مبادت ترکند بازی</p></div></div>