---
title: >-
    غزل شمارهٔ ۱۵۲۰
---
# غزل شمارهٔ ۱۵۲۰

<div class="b" id="bn1"><div class="m1"><p>تا مه نوبر فلک بال‌گشا می‌رود</p></div>
<div class="m2"><p>در نظرم رخش عمر نعل‌‌نما می‌‌رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواه نفس فرض‌کن خواه غبار هوس</p></div>
<div class="m2"><p>نی سحراست ونه شام سیل فنا می‌رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قطع نفس تا بجاست خاک همین منزلیم</p></div>
<div class="m2"><p>شمع رهش زیر پاست سعی کجا می‌رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشو و نماگفتگوست در چمن احتیاج</p></div>
<div class="m2"><p>رو به فلک یکقلم دست دعا می‌رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قافلهٔ عجز و باز حکم به هر سو بتاز</p></div>
<div class="m2"><p>عالم واماندگی‌ست آبله‌ها می‌رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سجده نمی‌خواهدت زحمت جهد قدم</p></div>
<div class="m2"><p>چون سرت افتاد پیش نوبت پا می‌رود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زبن همه باغ و بهاردست بهم سوده‌گیر</p></div>
<div class="m2"><p>فرصت رنگ حنا از کف ما می‌رود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در چمن اعتبارگر همه سیر دل است</p></div>
<div class="m2"><p>چشم نخواهی‌ گشود عرض حیا می‌رود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرزه‌خرام است و هم بیهده‌تازست فکر</p></div>
<div class="m2"><p>هیچ‌کس آگاه نیست آمده یا می‌رود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>موسم ییری رسید آنهمه بر خود مبال</p></div>
<div class="m2"><p>روزبه فصل شتا غنچه قبا می‌رود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هیأت شمعند خلق ساز اقامت کراست</p></div>
<div class="m2"><p>پا اگر فشرد‌ه‌اند سر به هوا می‌رود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا به‌کجا بایدم ماتم خود داشتن</p></div>
<div class="m2"><p>با نفسم عمرهاست آب بقا می‌رود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مقصد و مختار شوق کعبه و بتخانه نیست</p></div>
<div class="m2"><p>بی‌سبب و بی‌طلب دل همه جا می‌رود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اینک به خود چیده‌ایم فرصت ناز و نیاز</p></div>
<div class="m2"><p>دلبر ما یک دوگام پا به حنا می‌رود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هرچه‌گذشت از نظر نیست برون از خیال</p></div>
<div class="m2"><p>بیدل ازین دامگاه رفته ‌کجا می‌رود</p></div></div>