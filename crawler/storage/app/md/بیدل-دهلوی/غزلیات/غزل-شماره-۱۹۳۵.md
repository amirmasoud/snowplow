---
title: >-
    غزل شمارهٔ ۱۹۳۵
---
# غزل شمارهٔ ۱۹۳۵

<div class="b" id="bn1"><div class="m1"><p>ای جوش بهارت چمن‌آرای تغافل</p></div>
<div class="m2"><p>چون چشم تو سر تا قدمت جای تغافل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمریست‌ که آوارهٔ امید نگاهیم</p></div>
<div class="m2"><p>ازگوشهٔ چشم تو به صحرای تغافل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از شور دل خسته چه مینا که نچیده‌ست</p></div>
<div class="m2"><p>ابروی تو بر طاق معلای تغافل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ازنقطهٔ‌خالی‌که‌برآن‌گوشهٔ‌ابروست‌</p></div>
<div class="m2"><p>مهری زده‌ای بر لب گویای تغافل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سربازی عشاق به بزم تو تماشاست</p></div>
<div class="m2"><p>هرچند نباشد به میان پای تغافل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کو هوش ادا فهمی نازی که توان خواند</p></div>
<div class="m2"><p>سطر نگه از صفحهٔ سیمای تغافل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرچند نگاه تو حیات دو جهان است</p></div>
<div class="m2"><p>من‌کشتهٔ تمکینم و رسوای تغافل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فریاد که از لعل تو حرفی نشنیدیم</p></div>
<div class="m2"><p>موجی نزد این گوهر دریای تغافل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلها به تپش خون شد و ناز تو همان است</p></div>
<div class="m2"><p>مپسند به این حوصله مینای تغافل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از حسن در این بزم امید نگهی نیست</p></div>
<div class="m2"><p>ای آینه خون شو به تماشای تغافل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل نکشیدیم زکس جام مدارا</p></div>
<div class="m2"><p>مردیم به مخموری صهبای تغافل</p></div></div>