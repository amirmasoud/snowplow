---
title: >-
    غزل شمارهٔ ۳۳۹
---
# غزل شمارهٔ ۳۳۹

<div class="b" id="bn1"><div class="m1"><p>تا نمی‌دزدد غبار غفلت هستی خطاب</p></div>
<div class="m2"><p>بایدم از شرم این خاک پریشان‌گشت آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در طلسم حیرت این بحریک وارسته نیست</p></div>
<div class="m2"><p>موج هم داردگره بر بال پرواز از حباب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نالهٔ عشاق و آه بوالهوس با هم مسنج</p></div>
<div class="m2"><p>فرقها دارد شکوه برق تا مد شهاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ازتلاش آسود دل چون بر هوس دامن فشاند</p></div>
<div class="m2"><p>شعلهٔ بی‌دود را چندان نباشد پیچ و تاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آه از آن روزی‌که عرض مدعا سایل شود</p></div>
<div class="m2"><p>بی‌صدا زین کوهسارم سنگ می‌آید جواب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر به مخموران نگاهم هم نپردازد بلاست</p></div>
<div class="m2"><p>ای به دور نرگست رم‌کرده مستی از شراب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی‌بلایی نیست شمشیر مژه خواباندنت</p></div>
<div class="m2"><p>فتنهٔ‌چشم سیاهت را چه بیداری چه خواب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرکه را دیدم چو مژگان بال بسمل می‌زند</p></div>
<div class="m2"><p>عالمی‌راکشت چشمت خانهٔ مستی خراب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرگشادکار خواهی از طلسم خود برآ</p></div>
<div class="m2"><p>هست برخاک پریشان ششجهت یک فتح باب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از فریب و مکر دنیا اهل ترک آسوده‌اند</p></div>
<div class="m2"><p>دام راه تشنگان می‌باشد امواج سراب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هستی ما پردهٔ‌ساز تغافلهای اوست</p></div>
<div class="m2"><p>سایه مژگان بود هرجا چشم پوشید آفتاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ذره تا خورشید اسباب جهان سوزنده است</p></div>
<div class="m2"><p>بیدل ازگلخن شراری‌کرده باشی انتخاب</p></div></div>