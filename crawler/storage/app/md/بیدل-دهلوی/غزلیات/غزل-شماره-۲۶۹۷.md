---
title: >-
    غزل شمارهٔ ۲۶۹۷
---
# غزل شمارهٔ ۲۶۹۷

<div class="b" id="bn1"><div class="m1"><p>قدح از شوق لعلت چشم بی‌خوابست پنداری</p></div>
<div class="m2"><p>گل از شرم رخت آیینهٔ آبست پنداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیال‌ کیست یا رب شمع نیرنگ شبستانم</p></div>
<div class="m2"><p>هجوم حیرتی دارم‌که مهتابست پنداری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شدم خاکستر و از جوش بیتابی نیاسودم</p></div>
<div class="m2"><p>رگ خوابی ‌که دارم نبض سیمابست پنداری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تعلقهای هستی محو چندین حیرتم دارد</p></div>
<div class="m2"><p>به خود پیجیدنم در زلف او تابست پنداری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به چندین پیچ و تاب از دام حیرت برنمی‌آیم</p></div>
<div class="m2"><p>سراپایم نگاه چشم‌ گردابست پنداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهانی سیر مستی دارد از وضع جنون من</p></div>
<div class="m2"><p>گریبان چاکی‌ام موج می نابست پنداری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به نیک و بد مدارا سرکن و مسجود عالم شو</p></div>
<div class="m2"><p>تواضع هم خمی دارد که محرابست پنداری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>امل از چنگ فرصت می‌رباید نقد عمرت را</p></div>
<div class="m2"><p>توان را رشتهٔ تسخیر اسبابست پنداری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به ملک نیستی راه یقینت اینقدر واکن</p></div>
<div class="m2"><p>که هر کس هر چه آنجا می‌برد بابست پنداری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز هستی جز تن آسانی ندارم در نظر بیدل</p></div>
<div class="m2"><p>چو محمل هر سر مویم رگ خوابست پنداری</p></div></div>