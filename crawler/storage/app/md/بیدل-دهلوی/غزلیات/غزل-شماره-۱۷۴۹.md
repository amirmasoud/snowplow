---
title: >-
    غزل شمارهٔ ۱۷۴۹
---
# غزل شمارهٔ ۱۷۴۹

<div class="b" id="bn1"><div class="m1"><p>گره چو غنچه نباید زدن به تار نفس</p></div>
<div class="m2"><p>فکندنی است ز سر چون حباب بار نفس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمانه صد سحر از هر کنار می‌خندد</p></div>
<div class="m2"><p>به ضبط کار تو و وضع استوار نفس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش آن زمان ‌که شوی در غبار کسوت عجز</p></div>
<div class="m2"><p>چو شعله بر رگ گردن بلند بار نفس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اشاره‌ایست به اهل یقین ز چشم حباب</p></div>
<div class="m2"><p>که دیده وانشود تا بود غبار نفس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به سوی خویش‌ کشد صید را خموشی دام</p></div>
<div class="m2"><p>سخن ز فیض تامل شود شکار نفس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز موج بحر مجویید جهد خودداری</p></div>
<div class="m2"><p>چه ممکن است درآمد شد اختیار نفس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>متن چو صبح در انکار هستی ای موهوم</p></div>
<div class="m2"><p>گرفته است جهان را هوا سوار نفس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در این محیط‌ که هر قطره صد جنون تپش است</p></div>
<div class="m2"><p>شناخت موج‌ گهر قیمت وقار نفس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شب فراق توام زندگی چه امکان است</p></div>
<div class="m2"><p>مگر چو شمع ‌کند سعی اشک‌،‌ کار نفس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به چاک پیرهن عمر بخیه ممکن نیست</p></div>
<div class="m2"><p>متاب رشتهٔ وهم امل به تار نفس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فلک به ساغر خمیازه سرخوشم دارد</p></div>
<div class="m2"><p>چو صبح می‌کشم از زندگی خمار نفس</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تأملی نکشیده‌ست دامنت ورنه</p></div>
<div class="m2"><p>برون هر دو جهانی به یک فشار نفس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فروغ دل طلبی خامشی ‌گزین بیدل</p></div>
<div class="m2"><p>که شمع صرفه ندارد به رهگذار نفس</p></div></div>