---
title: >-
    غزل شمارهٔ ۵۸۴
---
# غزل شمارهٔ ۵۸۴

<div class="b" id="bn1"><div class="m1"><p>آرزوی دل‌، چو اشک از چشم ما افتاده است</p></div>
<div class="m2"><p>مدعا چون سایه‌ای در پیش پا افتاده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوهر امید ما قعر توکل‌کرده ساز</p></div>
<div class="m2"><p>کشتی تدبیر در موج رضا افتاده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جادهٔ سرمنزل عشاق سعی نارساست</p></div>
<div class="m2"><p>یا ز دست خضر این وادی‌، عصا افتاده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا قیامت برنمی‌خیزد چوداغ ازروی دل</p></div>
<div class="m2"><p>سایهٔ ما ناتوانان هرکجا افتاده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موی آتش دیده راکوتاه می‌باشد امل</p></div>
<div class="m2"><p>چشم ما عمری‌ست بر روز جزا افتاده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسکه‌کردم مشق وحشت در دبستان جنون</p></div>
<div class="m2"><p>شخصم‌از سایه‌چوکلک‌از خط‌جدا افتاده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیکرم‌خم گشته‌است ازضعف‌و دل‌خون می‌خورد</p></div>
<div class="m2"><p>بار این‌کشتی به دوش ناخدا افتاده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شبنم‌گلزار حیرت را نشست و خاست نیست</p></div>
<div class="m2"><p>اشک من در هرکجا افتاد وا افتاده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست در دشت طلب‌، باکعبه ما را احتیاج</p></div>
<div class="m2"><p>سجده‌گاه ماست هرجا نقش پا افتاده است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سایهٔ ما می‌زند پهلو به نورآفتاب</p></div>
<div class="m2"><p>ناتوانی اینقدرها خودنما افتاده است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون خط پرگارعمری شدکه سرتاپا خمیم</p></div>
<div class="m2"><p>ابتدای ما به فکر انتها افتاده است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سرمه این مقدار باب التفات ناز نیست</p></div>
<div class="m2"><p>چشم او بر خاکساریهای ما افتاده است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در حقیقت بیدل ما صاحب‌گنج بقاست</p></div>
<div class="m2"><p>گر به صورت در ره فقروفنا افتاده است</p></div></div>