---
title: >-
    غزل شمارهٔ ۲۰۹۳
---
# غزل شمارهٔ ۲۰۹۳

<div class="b" id="bn1"><div class="m1"><p>چون حباب آن دم که سیر آهنگ این دریا شدم</p></div>
<div class="m2"><p>درگشاد پردهٔ چشم از سر خود وا شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عرصهٔ آزادی از جوش غبارم تنگ بود</p></div>
<div class="m2"><p>بر سر خود دامنی افشاندم و صحرا شدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معنیم از شوخی اظهار آخر لفظ توست</p></div>
<div class="m2"><p>بسکه رنگ باده‌ام بی‌پردهٔ مینا شدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در فضای بیخودیها پی به حالم بردنست</p></div>
<div class="m2"><p>هر کجا سرگشته‌ای گم گشت من پیدا شدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر بن مویم تماشاخانهٔ دیدار بود</p></div>
<div class="m2"><p>عاقبت صرف نگه چون شمع سرتا پا شدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خامشیهایم جهانی را به شور دل‌ گرفت</p></div>
<div class="m2"><p>آخر از ضبط نفس صبح قیامت زا شدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای خوش آن وحدت‌ کزو نتوان عبارت باختن</p></div>
<div class="m2"><p>می‌زند کثرت ز نامم جوش تا تنها شدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>داغ نیرنگم‌، مپرس از مطلب نایاب من</p></div>
<div class="m2"><p>جست‌وجوی هر چه کردم محرم عنقا شدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شمع سیر انجمن‌ها در گداز خویش داشت</p></div>
<div class="m2"><p>هر قدر از پیکر من سرمه شد بینا شدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ماضی و مستقبل من حال‌ گشت از بیخودی</p></div>
<div class="m2"><p>رفتم امروز آنقدر از خود که چون فردا شدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فقر آخر سر ز جیب بی‌نیازی‌ها کشید</p></div>
<div class="m2"><p>احتیاجم جوش زد چندانکه استغنا شدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گرچه بیدل شیشهٔ من ازفلک آمد به سنگ</p></div>
<div class="m2"><p>اینقدر شد کز شکستن یک دهن‌گویا شدم</p></div></div>