---
title: >-
    غزل شمارهٔ ۶۶۱
---
# غزل شمارهٔ ۶۶۱

<div class="b" id="bn1"><div class="m1"><p>شیخ تا عزم بر نماز شکست</p></div>
<div class="m2"><p>صد وضو تازه‌ کرد و باز شکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صوفی افکند بر زمین مسواک</p></div>
<div class="m2"><p>وجد دندان این گراز شکست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبهه درس تامل من و تست</p></div>
<div class="m2"><p>رنگ تحقیق از امتیاز شکست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عیش سربسته داشت خاموشی</p></div>
<div class="m2"><p>لب‌گشودن طلسم راز شکست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر زمین تاخت حادثات فلک</p></div>
<div class="m2"><p>به نشیب آمد از فراز شکست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ادب‌آموز بود وضع سپهر</p></div>
<div class="m2"><p>گردن ما خم نیاز شکست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل خراب اعاده درد است</p></div>
<div class="m2"><p>شیشه را حسرت‌گداز شکست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناامیدی کلید مطلبهاست</p></div>
<div class="m2"><p>ای بسا در که ‌کرد باز شکست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دستگاه آنقدر نباید چید</p></div>
<div class="m2"><p>آستینی ‌که شد دراز شکست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مطرب این ندامت انجمنیم</p></div>
<div class="m2"><p>نغمهٔ‌ ماست عجز و ساز شکست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل از پیکر خمیده ما</p></div>
<div class="m2"><p>ناتوانی ‌کلاه ناز شکست</p></div></div>