---
title: >-
    غزل شمارهٔ ۱۴۵۴
---
# غزل شمارهٔ ۱۴۵۴

<div class="b" id="bn1"><div class="m1"><p>مفلسی دست تهی بر سودن ارزانی کند</p></div>
<div class="m2"><p>پنجهٔ بیکار بیعت با پشیمانی کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم‌ من از درد بیخوابی‌ در این‌ وادی‌ گداخت</p></div>
<div class="m2"><p>سایهٔ خاری نشد پیدا که مژگانی کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از حیا هم شرم می‌دارم ز ننگ اشتهار</p></div>
<div class="m2"><p>جامهٔ پوشندکی حیف است عریانی کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل به غفلت نه‌ که دردفع تمیز خوب و زشت</p></div>
<div class="m2"><p>خانهٔ آیینه را زنگار دربانی کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز به موقع آبروریزی‌ست عرض هر کمال</p></div>
<div class="m2"><p>غیر موسم ابر بر دریا چه نیسانی‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا به همواری رسد دور درشتیهای طبع</p></div>
<div class="m2"><p>هرکه را رنگی‌ست باید آسیابانی کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سبحه را گردآوری چون حلقهٔ زنار نیست</p></div>
<div class="m2"><p>کفر چون هموار شدکار مسلمانی کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نامه‌ای دارم بهار انشا که طبع بلبلش</p></div>
<div class="m2"><p>چون صریر خامه پیش از خط غزلخوانی‌ کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی‌تامل هرزه‌نالیهایم از خود می‌برد</p></div>
<div class="m2"><p>کاش چون بند نی‌ام خجلت‌ گریبانی‌ کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شرم بیدردی عرق می‌خواهد ای بیدل مباد</p></div>
<div class="m2"><p>بی‌نمی‌ها دیده را محتاج پیشانی کند</p></div></div>