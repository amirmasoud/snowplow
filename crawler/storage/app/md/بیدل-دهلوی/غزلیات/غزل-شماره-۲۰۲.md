---
title: >-
    غزل شمارهٔ ۲۰۲
---
# غزل شمارهٔ ۲۰۲

<div class="b" id="bn1"><div class="m1"><p>دل می‌رود و نیست کسی دادرس ما</p></div>
<div class="m2"><p>از قافله دور است خروش جرس ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم مشرب اوضاع گرفتاری صبحیم</p></div>
<div class="m2"><p>پرواز به منظر نرسد از قفس ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر هیچ‌کس افسانهٔ امید نخواندیم</p></div>
<div class="m2"><p>عمری‌ست همان بیکسی ماست‌کس ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما هیچکسان ناز چه اقبال فروشیم</p></div>
<div class="m2"><p>تقدیر عرق‌کرد به حشر مگس ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاریم ولی در هوس آباد تعین</p></div>
<div class="m2"><p>بر دیدهٔ دریا مژه چیده‌ست خس ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما و سخن ازکینه‌فروزی‌، چه خیال است</p></div>
<div class="m2"><p>آیینه نداده‌ست به آتش نفس ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر فرصت خام آن همه دکان نتوان چید</p></div>
<div class="m2"><p>مهمان دماغ است می زودرس ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مکتوب وفا مشعر امید نگاهی‌ست</p></div>
<div class="m2"><p>واکن مژه تا خوانده شود ملتمس ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیدل به جنون امل ازپا ننشستیم</p></div>
<div class="m2"><p>کاش آبله‌گیرد سر راه هوس ما</p></div></div>