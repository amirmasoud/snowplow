---
title: >-
    غزل شمارهٔ ۴۷۵
---
# غزل شمارهٔ ۴۷۵

<div class="b" id="bn1"><div class="m1"><p>به خوان لذت دنیاگزند بسیار است</p></div>
<div class="m2"><p>ترنجبینی اگر هست بر سر خار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به باد رفتهٔ ذوق فضولییم همه</p></div>
<div class="m2"><p>سر هوا طلبیها حباب دستار است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عنان وحشت مجنون ماکه می‌گیرد</p></div>
<div class="m2"><p>ز فرق تا به قدم‌گردباد چین‌دار است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پاس راحت دل این‌قدر زمینگیریم</p></div>
<div class="m2"><p>خیال آبله ضبط عنان رفتار است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به محفلی‌که دل احیای معرفت دارد</p></div>
<div class="m2"><p>لب خموش چراغ مزار اظهار است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غم تحیرحسن قبول باید خورد</p></div>
<div class="m2"><p>نه هرکه آینه پرداخت باب دیدار است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به وادیی‌که مرا داغ انتظار تو سوخت</p></div>
<div class="m2"><p>به چشم نقش قدم خاک نیز بیدار است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگاه اگر به خیال توگردن افرازد</p></div>
<div class="m2"><p>مژه بلندی انگشتهای زنهار است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وفا ستمکش ناموس ناتوانایی‌ست</p></div>
<div class="m2"><p>به پای هرکه خورد سنگ بر سرم باراست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کشیده سعی هوس رنج دشت ودرورنه</p></div>
<div class="m2"><p>رهی‌که پای تو نسپرده است هموار است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حیاکنید به پیری زوانمود طرب</p></div>
<div class="m2"><p>سحر چوآینه‌گیرد نفس شب تار است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چه ممکن است ز افتادگی‌گذشتن ما</p></div>
<div class="m2"><p>که خوابناک ضعیفیم و سایه دیوار است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به این‌گرانی دل بیدل از من مأیوس</p></div>
<div class="m2"><p>صدا اگر همه گردد بلندکهسار است</p></div></div>