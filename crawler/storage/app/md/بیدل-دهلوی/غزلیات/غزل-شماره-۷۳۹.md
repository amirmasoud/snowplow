---
title: >-
    غزل شمارهٔ ۷۳۹
---
# غزل شمارهٔ ۷۳۹

<div class="b" id="bn1"><div class="m1"><p>تومست وهم ودرین بزم بوی صهبا نیست</p></div>
<div class="m2"><p>هنوزجزبه دل سنگ جای مینا نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیال عالم بیرنگ رنگها دارد</p></div>
<div class="m2"><p>کدام نقش‌که تصویر بال عنقا نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بمیر وشهره شوای دل‌کزین مزار هوس</p></div>
<div class="m2"><p>چراغ مرده عیان است و زنده پیدا نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به چشم بسته خیال حضور حق پختن</p></div>
<div class="m2"><p>اشاره‌ای‌ست‌که اینجا نگاه بینا نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلت به عشوهٔ عقبا خوش است ازین غافل</p></div>
<div class="m2"><p>که هرکجا، تویی، آنجا به غیر دنیا نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به هرچه وارسی از خودگذشتنی دارد</p></div>
<div class="m2"><p>به‌هوش باش‌که امروز رفت وفردا نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به نامیدی ما، رحمی‌، ای دلیل فنا!</p></div>
<div class="m2"><p>که آشیان هوسیم ودرین چمن جا نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حریرکارگه وهم را چه تار و چه پود</p></div>
<div class="m2"><p>قماش ما ز لطافت تمیزفرسا نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو جلوه سازکن و مدعای دل دریاب</p></div>
<div class="m2"><p>زبان حیرت آیینه بی‌تقاضا نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غریق بحر ز فکر حباب مستغنی‌ست</p></div>
<div class="m2"><p>رسیده‌ایم به جایی‌که بیدل آنجا نیست</p></div></div>