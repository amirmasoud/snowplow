---
title: >-
    غزل شمارهٔ ۱۵۴۵
---
# غزل شمارهٔ ۱۵۴۵

<div class="b" id="bn1"><div class="m1"><p>هوش تا عافیت آیینهٔ مستی نشود</p></div>
<div class="m2"><p>نیست ممکن‌که‌کندکاری و عاصی نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باخبر باش که نگذشته‌ای از عالم وهم</p></div>
<div class="m2"><p>نقش فردای تو تا آینهٔ دی نشود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون عشاق‌، وطن در رگ بسمل دارد</p></div>
<div class="m2"><p>نیست این آب از آن چشمه‌ که جاری نشود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا به‌ کی شبهه‌پرس حق و باطل بودن</p></div>
<div class="m2"><p>مرد این محکمه آن است‌ که قاضی نشود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هوس راحت جاوید زکف باخته‌ایم</p></div>
<div class="m2"><p>شعله داغ است اگر مست ترقی نشود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی‌تو بر لاله و گل چشم هوس نگشادم</p></div>
<div class="m2"><p>که به رویم مژه برگردد و سیلی نشود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از بدآموزی تنهایی دل می‌ترسم</p></div>
<div class="m2"><p>که دهی منصب آیینه و راضی نشود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آه از آن داغ‌ که خاکستر شوق‌آلودم</p></div>
<div class="m2"><p>در غم سرو تو واسوزد و قمری نشود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا به سیلاب فنا وانگذاری بیدل</p></div>
<div class="m2"><p>باخبر باش‌که رخت تو نمازی نشود</p></div></div>