---
title: >-
    غزل شمارهٔ ۵۲۹
---
# غزل شمارهٔ ۵۲۹

<div class="b" id="bn1"><div class="m1"><p>چشمی‌که ندارد نظری حلقهٔ دام است</p></div>
<div class="m2"><p>هرلب‌که سخن سنج نباشد لب بام است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌جوهری از هرزه درایی‌ست زبان را</p></div>
<div class="m2"><p>تیغی‌که به زنگار فرورفت نیام است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مغرورکمالی ز فلک شکوه چه لازم</p></div>
<div class="m2"><p>کار تو هم از پختگی طبع تو خام است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای شعلهٔ امید نفس سوخته تا چند</p></div>
<div class="m2"><p>فرداست که پرواز تو فرسودهٔ دام است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نومیدی‌ام از قید جهان شکوه ندارد</p></div>
<div class="m2"><p>با دام و قفس طایر پرریخته رام است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی صبح نقاب افکند از چهره‌که امشب</p></div>
<div class="m2"><p>آیینهٔ بخت سیهم درکف شام است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نی صبربه دل ماند ونه حیرت به نظرها</p></div>
<div class="m2"><p>ای سیل دل وبرق نظراین چه خرام است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مستند اسیران خم وپیچ محبت</p></div>
<div class="m2"><p>در حلقهٔ‌گیسوی تو ذکر خط جام است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگذر ز غنا تا نشوی دشمن احباب</p></div>
<div class="m2"><p>اول سبق حاصل زرترک سلام است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گویند بهشت است همان راحت جاوید</p></div>
<div class="m2"><p>جایی‌که به داغی نتپد دل چه مقام است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چشم تو نبسته است مگرگفت و شنودت</p></div>
<div class="m2"><p>محو خودی ای بیخبر افسانه‌کدام است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل به‌گمان محو یقینم چه توان‌کرد</p></div>
<div class="m2"><p>کم فرصتی از وصل‌پرستان چه پیام است</p></div></div>