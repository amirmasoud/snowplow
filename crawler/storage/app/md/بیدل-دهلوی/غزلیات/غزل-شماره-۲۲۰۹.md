---
title: >-
    غزل شمارهٔ ۲۲۰۹
---
# غزل شمارهٔ ۲۲۰۹

<div class="b" id="bn1"><div class="m1"><p>چون شمع زحمتی که به شبگیر می‌کشم</p></div>
<div class="m2"><p>از داغ پنبه می‌کشم و دیر می‌کشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طفلی شد و شباب شد و شیب سرکشید</p></div>
<div class="m2"><p>لیکن یقین نشد که چه تصویر می‌کشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرصت امید و سعی هوسها همان بجاست</p></div>
<div class="m2"><p>سیماب رفت و زحمت اکسیر می‌کشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عجزم به زعم خویش رگ از سنگ می‌کشد</p></div>
<div class="m2"><p>هر چند موی از قدح شیر می‌کشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی خم شدن ز دوش نیفتاد بار کش</p></div>
<div class="m2"><p>رنج شباب تا نشوم پیر می‌کشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مزدوری بنای جسد بار گردن است</p></div>
<div class="m2"><p>تا زنده‌ام همین گل تعمیر می‌کشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین ناله‌ای که هرزه دو نارسایی است</p></div>
<div class="m2"><p>روزی دو انتقام ز تأثیر می‌کشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنیاد اعتبار بر این صورت است و بس</p></div>
<div class="m2"><p>وهم ثبات دارم و تغییر می‌کشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در دل هزار ناله به تحسین من کم است</p></div>
<div class="m2"><p>نقاش صنعت المم تیر می‌کشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ضعفم نشانده است به روز سیاه شمع</p></div>
<div class="m2"><p>پایی که می‌کشم ز گل قیر می‌کشم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا همچو اخگرم تب جانکاه کم شود</p></div>
<div class="m2"><p>می‌سایم استخوان و تباشیر می‌کشم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پیری اشاره‌ای ز خم ابروی فناست</p></div>
<div class="m2"><p>ای سر مچین بلند که شمشیر می‌کشم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بیدل سخن صدای گرفتاری دل است</p></div>
<div class="m2"><p>این ریشه‌ها ز دانهٔ زنجیر می‌کشم</p></div></div>