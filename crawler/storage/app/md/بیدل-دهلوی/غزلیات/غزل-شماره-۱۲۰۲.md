---
title: >-
    غزل شمارهٔ ۱۲۰۲
---
# غزل شمارهٔ ۱۲۰۲

<div class="b" id="bn1"><div class="m1"><p>چشمی‌ که بر آن جلوه نظر داشته باشد</p></div>
<div class="m2"><p>یارب به چه جرات مژه برداشته باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دل‌که ز زخم تو اثر داشته باشد</p></div>
<div class="m2"><p>صد صبح‌گل فیض به بر داشته باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمری‌ست دکان نفس سوخته‌گرم است</p></div>
<div class="m2"><p>ازآه من آیینه خبر داشته باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با پرتو خورشید کرم سهل حسابی‌ست</p></div>
<div class="m2"><p>گر شبنم ما دامن تر داشته باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل توشه‌کش وهم حباب‌ست درین بحر</p></div>
<div class="m2"><p>امید که آهی به جگر داشته باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جا بر سر دوش است‌کسی راکه درین بزم</p></div>
<div class="m2"><p>با ما چو سبو دست به سر داشته باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ازتیغ نگاهت دل آیینه دو نیم است</p></div>
<div class="m2"><p>هرچند ز فولاد سپر داشته باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما را به ادبگاه حضورت چه پیام است</p></div>
<div class="m2"><p>قاصد مگر از خویش خبر داشته باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از وحشت ما بر دل کس نیست غباری</p></div>
<div class="m2"><p>یک ذره تپیدن چقدر داشته باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای بیخبر از عشق مجو ساز سلامت</p></div>
<div class="m2"><p>جز سوختن آتش چه هنر داشته باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ناکام فسردیم چو خون در رگ یاقوت</p></div>
<div class="m2"><p>رنگی ندمیدیم‌که پر داشته باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل خلف سلسلهٔ عبرت امکان</p></div>
<div class="m2"><p>جز مرگ چه از ارث پدر داشته باشد</p></div></div>