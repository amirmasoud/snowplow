---
title: >-
    غزل شمارهٔ ۶۴
---
# غزل شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>شور جنون درقفسی با همه بیگانه برآ</p></div>
<div class="m2"><p>یک دو نفس ناله شو و از دل دیوانه برآ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تاب وتب سبحه بهل‌، رشتهٔ زنارگسل</p></div>
<div class="m2"><p>قطرهٔ می! جوش زن و برخط پیمانه‌برآ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اشک‌کشد تا به‌کجا ساغر ناموس حیا</p></div>
<div class="m2"><p>شیشه به بازار شکن‌، اندکی از خانه برآ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون نفس از الفت دل پای توفرسود به‌گل</p></div>
<div class="m2"><p>ریشهٔ وحشت ثمری از قفس دانه برآ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرخ‌کلید در دل وقف جهادت نکند</p></div>
<div class="m2"><p>اره صفت‌گو دم تیغت همه دندانه برآ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست خرابات‌جنون عرصهٔ جولان فنون</p></div>
<div class="m2"><p>لغزش مستانه خوش است آبله پیمانه برآ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کرده فسون نفست‌، غرهٔ عشق وهوست</p></div>
<div class="m2"><p>دود چراغی‌که نه‌ای از دل پروانه برآ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا ز خودت‌نیست خبر درته‌خاکست نظر</p></div>
<div class="m2"><p>یک مژه برخویش‌گشاگنج زویرانه برآ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما ومن عالم‌دون جمله‌فریب است وفسون</p></div>
<div class="m2"><p>رو به در خواب زن ازکلفت افسانه برآ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل ازافسونگری‌ات خرس‌وبز آدم‌نشود</p></div>
<div class="m2"><p>چنگ به هرریش مزن ازهوس شانه برآ</p></div></div>