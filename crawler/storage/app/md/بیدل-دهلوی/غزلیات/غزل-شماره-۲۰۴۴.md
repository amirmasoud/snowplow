---
title: >-
    غزل شمارهٔ ۲۰۴۴
---
# غزل شمارهٔ ۲۰۴۴

<div class="b" id="bn1"><div class="m1"><p>کند هر جا عرق ز آن ماه تابان‌ گلفشان انجم</p></div>
<div class="m2"><p>شکست رنگ سازد جمع چون برگ خزان انجم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جبین و عارضش از دور دیدم در عرق‌ گفتم</p></div>
<div class="m2"><p>که این ماه است و آن خورشید تابان است و آن انجم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو بر خاک درش یک نقش پا کسب سعادت ‌کن</p></div>
<div class="m2"><p>به اظهار اثرگو داغ شو بر آسمان انجم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آن وادی که یاد اوست شمع راه امیدم</p></div>
<div class="m2"><p>توان خرمن نمودن از غبار کاروان انجم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عرق جوش است حسن ای شوق چشم حیرتی وا کن</p></div>
<div class="m2"><p>قدح باید گرفت آندم‌ که آمد در میان انجم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به هرجا شکوه‌ای گل کرده است از بخت ناسازم</p></div>
<div class="m2"><p>ز خجلت چون شرر در سنگ می‌باشد نهان انجم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به غیر از سوختن تخمی ندارد مزرع امکان</p></div>
<div class="m2"><p>به این حاصل مگر در خاک‌ کارد آسمان انجم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شراری چند سامان‌ کن اگر در خود زدی آتش</p></div>
<div class="m2"><p>نمی‌تابد به‌ کام بینوایان رایگان انجم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چراغ این شبستان قابل پرتو نمی‌باشد</p></div>
<div class="m2"><p>نتابد کرم شبتابی مگر در آشیان انجم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو از غفلت به صد امید سودا کرده‌ای ورنه</p></div>
<div class="m2"><p>به غیر از چشمک خشکی ندارد در دکان انجم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درین حسرت‌ که مهر طلعتش‌ کی پرده برگیرد</p></div>
<div class="m2"><p>چو بیدل می‌تپد هر شب به چشم خون فشان انجم</p></div></div>