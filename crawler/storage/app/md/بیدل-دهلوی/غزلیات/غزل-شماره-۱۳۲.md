---
title: >-
    غزل شمارهٔ ۱۳۲
---
# غزل شمارهٔ ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>حیف است‌کشد سعی دگر باده‌کشان را</p></div>
<div class="m2"><p>یاران به خط جام ببندید میان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما صافدلان سرشکن طبع درشتیم</p></div>
<div class="m2"><p>بر سنگ ترحم نبود شیشه‌گران را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حسرت همه دم صید خم قامت پیری‌ست</p></div>
<div class="m2"><p>گل در بر خمیازه بود شاخ‌کمان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غفلت ز سرم باز نگردید چوگوهر</p></div>
<div class="m2"><p>با دیده گره ساخته‌ام خواب گران را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عالم همه یار است تو محجوب خیالی</p></div>
<div class="m2"><p>بند از مژه بردار یقین سازگمان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آسوده روان جاده تشویش ندارند</p></div>
<div class="m2"><p>منزل طلبی ترک مکن ضبط عنان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما و سحر از یک جگر چاک دمیدیم</p></div>
<div class="m2"><p>آهی نکشیدیم‌که نگرفت جهان را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیدار پرستیم مپرس از رم و آرام</p></div>
<div class="m2"><p>پرواز نگاه است تحیر قفسان را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل جمع‌کن ازکشمکش دهر برون آ</p></div>
<div class="m2"><p>کاین بحر در آغوش گهر ریخت کران را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گردون همه پرواز و زمین جمله غبار است</p></div>
<div class="m2"><p>منزل بنمایید اقامت‌طلبان را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سرمایه چو صبح از دو نفس بیش ندارید</p></div>
<div class="m2"><p>بیهوده براین جنس مچینید دکان را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل ز نفسها روش عمر عیان است</p></div>
<div class="m2"><p>نقش قدم از موج بود آب روان را</p></div></div>