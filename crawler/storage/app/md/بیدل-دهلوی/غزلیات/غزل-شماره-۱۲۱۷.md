---
title: >-
    غزل شمارهٔ ۱۲۱۷
---
# غزل شمارهٔ ۱۲۱۷

<div class="b" id="bn1"><div class="m1"><p>دماغ وحشت‌آهنگان خیال‌آور نمی‌باشد</p></div>
<div class="m2"><p>سر ما طایران رنگ زبر پر نمی‌باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیال ثابت و سیار تا کی خواند افسونت</p></div>
<div class="m2"><p>سلامت نقشبند طاق این منظر نمی‌باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیالش در دل است اما چه حاصل غیر نومیدی</p></div>
<div class="m2"><p>پری در شیشه جز در عالم دیگر نمی‌باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به سامان جهان پوچ تسکین چیده‌ایم اما</p></div>
<div class="m2"><p>به این صندل ‌که ما داریم دردسر نمی‌باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حواس آواره افتاده است از خلوتسرای دل</p></div>
<div class="m2"><p>وگرنه حلقهٔ صحبت برون در نمی‌باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بلد از عجز طاقت‌گیر و هر راهی‌ که خواهی رو</p></div>
<div class="m2"><p>خط پیشانی تسلیم بی‌مسطر نمی‌باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زترک مطلب نایاب صید بی‌نیازی ‌کن</p></div>
<div class="m2"><p>دل جمعی ‌که می‌خواهی درین ‌کشور نمی‌باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کدورت‌گر همه باد است بر دل بار می‌چیند</p></div>
<div class="m2"><p>نفس در خانهٔ آیینه بی‌لنگر نمی‌باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سواد هر دو عالم شسته ‌است اشکی ‌که من دارم</p></div>
<div class="m2"><p>رواج سرمه در اقلیم چشم تر نمی‌باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مروت‌سخت‌مخمور است در خمخانهٔ مطلب</p></div>
<div class="m2"><p>جبین هیچکس اینجا عرق ساغر نمی‌باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جنون فطرتی در رقص دارد نبض امکان را</p></div>
<div class="m2"><p>همه گر پا به‌ گردش آوری بی‌سر نمی‌باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تأمل بی‌کمالی نیست در ساز نفس بیدل</p></div>
<div class="m2"><p>اگر شد رشته‌ات لاغر گره لاغر نمی‌باشد</p></div></div>