---
title: >-
    غزل شمارهٔ ۱۷۵۶
---
# غزل شمارهٔ ۱۷۵۶

<div class="b" id="bn1"><div class="m1"><p>نفس ثبات ندارد به شست ‌کار نویس</p></div>
<div class="m2"><p>شکسته است قلم نسخه اعتبار نویس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جریدهٔ رقم اعتبارها خاک است</p></div>
<div class="m2"><p>تو هم خطی به سر لوح این مزار نویس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زمان وصل به صبح قیامت افتاده‌ست</p></div>
<div class="m2"><p>سیاهی از شب ما گیر و انتظار نویس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوار مطلب عشاق دقتی دارد</p></div>
<div class="m2"><p>برای خاطر ما اندکی غبار نویس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شقی‌ که ‌گل ‌کند از خامه بی ‌صریری نیست</p></div>
<div class="m2"><p>برات ناله تو هم بر دل فگار نویس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خط جنون‌ سبقان مسطری نمی‌خواهد</p></div>
<div class="m2"><p>چو نغمه هرچه نویسی برون تار نویس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شگون یمن ندارد برات عشرت دهر</p></div>
<div class="m2"><p>زبان خامه سیاه است‌ گو بهار نویس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هزار مرتبه دارد شهید تیغ وفا</p></div>
<div class="m2"><p>قلم به خون زن و بیتی به یادگار نویس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز نقش هستی من هر کجا اثر یابی</p></div>
<div class="m2"><p>خط جبین کن و بر خاک راه یار نویس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیاض دیدهٔ یعقوب اشارتی دارد</p></div>
<div class="m2"><p>که سیر ما کن و تفسیر نقره‌کار نویس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به نامه‌ای‌ که در او نام عشق ثبت ‌کنند</p></div>
<div class="m2"><p>به جای هر الف انگشت زینهار نویس</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز خود تهی شدن آغوش بی‌نشانی اوست</p></div>
<div class="m2"><p>چو صفر اگر ز میان رفته‌ای‌کنار نویس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به مشق حسرت ازآن جلوه قانعم بیدل</p></div>
<div class="m2"><p>بر او سفیدی مکتوب انتظار نویس</p></div></div>