---
title: >-
    غزل شمارهٔ ۲۴۱۸
---
# غزل شمارهٔ ۲۴۱۸

<div class="b" id="bn1"><div class="m1"><p>کس چو شمع من نبوده‌ست آشنای سوختن</p></div>
<div class="m2"><p>گرد داغم داغ شد سر تا به پای سوختن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشقان بالی به ذوق نیستی افشانده‌اند</p></div>
<div class="m2"><p>کیست از پروانه پرسد ماجرای سوختن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیر فرصت دود خاکستر ندارد آتشش</p></div>
<div class="m2"><p>از شرر پرس ابتدا و انتهای سوختن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمع آداب وفا عمریست روشن کرده‌ام</p></div>
<div class="m2"><p>تا نفس دارم سرتسلیم و پای سوختن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زندگی چندان‌ گوارا نیست اما عمرهاست</p></div>
<div class="m2"><p>با طبایع گرمیی دارد هوای سوختن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی‌تو ما را چون چراغ‌ کشته هستی داغ‌ کرد</p></div>
<div class="m2"><p>هرکجا رفتیم خالی بود جای سوختن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از وبال بی‌پریها چون غبار آسوده‌ایم</p></div>
<div class="m2"><p>در پناه سایهٔ دست دعای سوختن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نعل در آتش نمی‌باشد سپند بزم ما</p></div>
<div class="m2"><p>لیک اندک وجد می‌خواهد نوای سوختن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا نفس باقیست اجزای نفس می‌پروریم</p></div>
<div class="m2"><p>مشت خاشاکیم مصروف غذای سوختن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طول و عرض حرص کوته کن که خطها می‌کشد</p></div>
<div class="m2"><p>از طناب برق معمار بنای سوختن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لالهٔ این گلستان چندان نشاط آماده نیست</p></div>
<div class="m2"><p>کاسهٔ داغیست در دست گدای سوختن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کم عیارانیم دارالامتحان عشق کو</p></div>
<div class="m2"><p>نیست هرکس قدردان کیمیای سوختن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خواه دور چرخ‌، خواهی شعلهٔ جواله گیر</p></div>
<div class="m2"><p>روز و شب می‌گردد اینجا آسیای سوختن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صبح شد چون شمعم اکنون داغ نقد زندگی‌ست</p></div>
<div class="m2"><p>هر قدر سر داشتم‌ کردم فدای سوختن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شمع دل گفتم درین محفل چرا آورده‌اند</p></div>
<div class="m2"><p>داغ شد نومیدی و گفت از برای سوختن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بیدل امشب چون شرار کاغذ آتش زده</p></div>
<div class="m2"><p>چیده‌ام گلها ز باغ دلگشای سوختن</p></div></div>