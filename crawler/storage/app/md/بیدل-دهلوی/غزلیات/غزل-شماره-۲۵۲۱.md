---
title: >-
    غزل شمارهٔ ۲۵۲۱
---
# غزل شمارهٔ ۲۵۲۱

<div class="b" id="bn1"><div class="m1"><p>سوخته لاله‌زار من رفته گل از کنار من</p></div>
<div class="m2"><p>بی‌تو نه رنگم و نه بو ای ‌قدمت بهار من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوش نسیم مژده‌ای گل به سر امید زد</p></div>
<div class="m2"><p>کز ره دور می‌رسد سرو چمن سوار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر به تبسمی رسد صبح بهار وعده‌ات</p></div>
<div class="m2"><p>آینه موج‌گل زند تا ابد از غبار من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر همه زخم خورده‌ام گل زکف تو برده‌ام</p></div>
<div class="m2"><p>باغ حناست هر کجا خون چکد از شکار من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرصت دیگرم‌ کجاست تا کنم آرزوی وصل</p></div>
<div class="m2"><p>راه عدم سپید کرد شش جهت انتظار من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عکس تحیر آب و رنگ منفعل است از آینه</p></div>
<div class="m2"><p>گرد نفس نمی‌کند هستی من ز عار من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آه سپند حسرتم ‌گرمی مجمری ندید</p></div>
<div class="m2"><p>سوختنم همان بجاست ناله نکرد کار من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کاش به‌ وامی از عرق حق وفا ادا شود</p></div>
<div class="m2"><p>نم نگذاشت در جبین گریهٔ شرمسار من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاک تپیدنم ‌که برد گرد مرا به‌کوی تو</p></div>
<div class="m2"><p>بنده حیرتم که کرد آینه‌ات دچار من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ظاهر و باطن دگر نیست به ‌ساز این نشاط</p></div>
<div class="m2"><p>تا من و تو اثر نواست نغمهٔ توست تار من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گربه سپهرم التجاست ورمه و مهرم آشناست</p></div>
<div class="m2"><p>بیدل بیکس توام غیر تو کیست یار من</p></div></div>