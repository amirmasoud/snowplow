---
title: >-
    غزل شمارهٔ ۴۶۷
---
# غزل شمارهٔ ۴۶۷

<div class="b" id="bn1"><div class="m1"><p>نیک و بدم از بخت بدانجام سفید است</p></div>
<div class="m2"><p>چندان‌ که سیاه است نگین نام سفید است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سطری ننوشتم که نکردم عرق از شرم</p></div>
<div class="m2"><p>مکتوب من از خجلت پیغام سفید است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر منتظران صرفه ندارد مژه بستن</p></div>
<div class="m2"><p>در پرده همان دیدهٔ بادام سفید است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای غره ی جاه این همه اظهار کمالت</p></div>
<div class="m2"><p>حرفی چو مه نو ز لب بام سفید است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر اهل صفا ننگ ‌کدورت نتوان بست</p></div>
<div class="m2"><p>این شیر اگر پخته وگر خام سفید است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناصافی دل آینه‌ ی وصل نشاید</p></div>
<div class="m2"><p>ای بیخردان جامهٔ احرام سفید است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پوچ است تعلق چو ز مو رفت سیاهی</p></div>
<div class="m2"><p>در پینه‌ کنون رشتهٔ این دام سفید است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صبحی به سیاهی نزد از دامن این دشت</p></div>
<div class="m2"><p>چندان که نظر کار کند شام سفید است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از چرخ کهن درگذر و کاهکشانش</p></div>
<div class="m2"><p>فرسودگیی از خط این جام سفید است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از خویش برآ منزل تحقیق نهان نیست</p></div>
<div class="m2"><p>صد جاده درین‌دشت به یک گام سفید است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون دیدهٔ قربانی‌ات از ترک تماشا</p></div>
<div class="m2"><p>بیدل همه جا بستر آرام سفید است</p></div></div>