---
title: >-
    غزل شمارهٔ ۹۶۷
---
# غزل شمارهٔ ۹۶۷

<div class="b" id="bn1"><div class="m1"><p>رنگم نقاب غیرت آن جلوه می‌درد</p></div>
<div class="m2"><p>فطرت جنون کند که ز بویم اثر برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شادم ‌که بی‌ نشانی آثار رنگ و بو</p></div>
<div class="m2"><p>بیرونم از قلمروتحقیق پرورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این چار سو ادبگه سودای نازکیست</p></div>
<div class="m2"><p>عمری‌ست ضبط آه من آیینه می‌خرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلقی در امل زد و با داغ یأس رفت</p></div>
<div class="m2"><p>آتش به‌کارگاه فسون خانهٔ خرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داغم ز جلوه‌ای که غرور تغافلش</p></div>
<div class="m2"><p>آیینه‌خانه‌ها کند ایجاد و ننگرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هنگامهٔ قبول نفس بسکه تنگ بود</p></div>
<div class="m2"><p>پا تا سرم چو شمع ز هم خورد دست رد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نقاش شرم دار ز پرداز انفعال</p></div>
<div class="m2"><p>تصویرم آن ‌کشد که ز رنگم برآورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آیینهٔ خرام بهار است‌ گرد رنگ</p></div>
<div class="m2"><p>من نقش پا خیال تو هرجا که بگذرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طاووس من بهار کمین چه مژده است</p></div>
<div class="m2"><p>عمری‌ست بال می‌زنم و چشم می‌پرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل جواب مطلب عشاق حیرت‌ست</p></div>
<div class="m2"><p>آنکس ‌که نامه‌ام برد آیینه آورد</p></div></div>