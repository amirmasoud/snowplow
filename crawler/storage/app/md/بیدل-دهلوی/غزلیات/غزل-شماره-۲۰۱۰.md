---
title: >-
    غزل شمارهٔ ۲۰۱۰
---
# غزل شمارهٔ ۲۰۱۰

<div class="b" id="bn1"><div class="m1"><p>تأخیر ندارد خط فرمان نجاتم</p></div>
<div class="m2"><p>در کاغذ آتش زده ثبت است براتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آثار بقایم عرق روی حبابست</p></div>
<div class="m2"><p>شرم آینه دارد به‌کف از موت و حیاتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هستی به هوس تک زدن‌ گرد فسوس است</p></div>
<div class="m2"><p>مانند نفس سخت ندامت حرکاتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عجزم ز نم جبهه ‌گذشتن نپسندید</p></div>
<div class="m2"><p>زین یکدو عرق شد پل جیحون و فراتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرد نفس و فال اقامت چه خیالست</p></div>
<div class="m2"><p>پرواز گرفته‌ست سر راه ثباتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خطی به هوا می‌کشم از فطرت مجهول</p></div>
<div class="m2"><p>در مشق جنون خامه نوا کرده دواتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون نشئه ندانم به‌ کجا می ‌روم از خویش</p></div>
<div class="m2"><p>دارد خط پیمانه شمار درجاتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هیهات نبردم اثر از نشئهٔ تحقیق</p></div>
<div class="m2"><p>دین رفت به باد هوس صوم و صلاتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>محتاج نی‌ام لیک چو آیینه ز حیرت</p></div>
<div class="m2"><p>هر جلوه‌ که آمد به نظر داد زکاتم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خاموشی‌ام آن نیست‌که جوشم به تکلم</p></div>
<div class="m2"><p>از حرف تو بر لب شکری بست نباتم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل نفسم‌ کارگه حشر معانی‌ست</p></div>
<div class="m2"><p>چون غلغلهٔ صور قیامت کلماتم</p></div></div>