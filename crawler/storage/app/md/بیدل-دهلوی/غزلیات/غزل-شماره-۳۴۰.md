---
title: >-
    غزل شمارهٔ ۳۴۰
---
# غزل شمارهٔ ۳۴۰

<div class="b" id="bn1"><div class="m1"><p>چو شمع تا سحر افسانه می‌شود تب وتاب</p></div>
<div class="m2"><p>نگاه برق خرام است جلوه‌ای دریاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر غنا طلبی مشق خاکساری‌کن</p></div>
<div class="m2"><p>حضورگنج براتی‌ست سرنوشت خراب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به فیض‌کاهلی آماده است راحت ما</p></div>
<div class="m2"><p>که سایه راست ز پهلوی عجز بستر خواب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فریب جلوهٔ نیرنگ زندگی نخوری</p></div>
<div class="m2"><p>که شسته‌اند ازین صفحه غیر نقش سراب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آن بساط‌که از رنگ آرزو پرسند</p></div>
<div class="m2"><p>چو یاس در نفس ما شکسته است جواب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دل اگر برسی جستجو نمی‌ماند</p></div>
<div class="m2"><p>تحیر است درآیینه شوخی سیماب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نماند در دل ما خونی از فشار غمت</p></div>
<div class="m2"><p>به غنچگی زگل ماگرفته‌اندگلاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز شرم حلقهٔ آن زلف حیرتی دارم</p></div>
<div class="m2"><p>که ناف آهوی مشکین چرا نشدگرد‌اب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عجب‌که رشتهٔ پروین زهم نمی‌گسلد</p></div>
<div class="m2"><p>چنین‌که از عرق روی اوست درتب و تاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زموج رنگ به دوران نشئهٔ نگهت</p></div>
<div class="m2"><p>خط شکسته توان خواند از جبین شراب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غرور هستی او را فنای ماست دلیل</p></div>
<div class="m2"><p>خم‌کلاه محیط است در شکست حباب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کسی چه چاره‌کند سرنوشت را بیدل</p></div>
<div class="m2"><p>نشست سرخط موج ازجبین دریا آب</p></div></div>