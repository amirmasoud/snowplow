---
title: >-
    غزل شمارهٔ ۱۴۰۰
---
# غزل شمارهٔ ۱۴۰۰

<div class="b" id="bn1"><div class="m1"><p>به‌ گفتگوی کسان مردمی که می‌لافند</p></div>
<div class="m2"><p>چو خط به معنی خود نارسیده حرافند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مباش غره انصاف کاین نفس‌بافان</p></div>
<div class="m2"><p>به پنبه‌کاری مغز خیال ندافند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>توانگری‌که دم از فقر می‌زند غلط است</p></div>
<div class="m2"><p>به موی‌کاسهٔ چینی نمد نمی‌بافند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تهیهٔ سپر از احتراز کن کامروز</p></div>
<div class="m2"><p>به قطع هم‌، بد ونیک زمانه سیافند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخن چه عرض نجابت دهد در آن محفل</p></div>
<div class="m2"><p>که سیم و ‌‌زر نسبان همچو جدول اشرافند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غرض ز صحبت اگر پاس آبرو باشد</p></div>
<div class="m2"><p>حذر کنید که ابنای جاه اجلافند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بهشت معانی به رویشان مگشا</p></div>
<div class="m2"><p>که این جهنمی چند ننگ اعرافند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به‌علم‌پوچ چوجهل مرکبند بسیط</p></div>
<div class="m2"><p>به فطرت کشفی درسگاه ‌کشافند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز وضعشان مطلب نیم نقطه همواری</p></div>
<div class="m2"><p>که‌ یک قلم به خم و پیچ سرکشی ‌کافند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تمام بیهوده گویند و نازکی این است</p></div>
<div class="m2"><p>که چشم بر طمع ریشخند انصافند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ازین خران مطلب مردمی‌ که چون‌گرداب</p></div>
<div class="m2"><p>به موج آب منی غرق ‌تا لب نافند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به خاک تیره مزن نقد ابرو بیدل</p></div>
<div class="m2"><p>درین دیارکه کوران چند صرافند</p></div></div>