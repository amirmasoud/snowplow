---
title: >-
    غزل شمارهٔ ۴۳۱
---
# غزل شمارهٔ ۴۳۱

<div class="b" id="bn1"><div class="m1"><p>شوخی‌انداز جرأتها ضعیفان را بلاست</p></div>
<div class="m2"><p>جنبش خویش از برای اشک سیلاب فناست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر از سَرو ِ تو شور ِقمری ما شد بلند</p></div>
<div class="m2"><p>جلوِهٔ بالابلندان خاکساران را عصاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اینقدر کز بیکسی ممنون احسان غمیم</p></div>
<div class="m2"><p>بر سر ما خاک اگر دستی کشد بال هماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عرض حال بیدلان راگفتگو درکار نیست</p></div>
<div class="m2"><p>گردش چشم تحیر هم ادای مدعاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وصل می‌خواهی وداع شوخی نظاره‌ کن</p></div>
<div class="m2"><p>جلوه اینجا محو آغوش نگاه نارساست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی‌ادب نتوان به روی نازنینان تاختن</p></div>
<div class="m2"><p>پای خط عنبرینش سر به دامن حیاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اعتبار ما، ز رنگ چهره ی ما روشن است</p></div>
<div class="m2"><p>سرخرو بودن به بزم‌ گلرخان کار حناست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از ورق‌گردانی وضع جهان غافل مباش</p></div>
<div class="m2"><p>صبح و شام این‌گلستان انقلاب رنگهاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وهم هستی را رواج از سادگیهای دل است</p></div>
<div class="m2"><p>عکس را آیینه عشرتخانهٔ‌ نشو و نماست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بهره‌ای از ساز درد بینوایی برده‌ام</p></div>
<div class="m2"><p>چون صدای نی، شکست استخوانم خوش نواست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در ضعیفی‌گر همه عجز است نتوان پیش برد</p></div>
<div class="m2"><p>چون مژه دست دعای ناتوانان بر قفاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل امشب نیست دست آهم از افغان تهی</p></div>
<div class="m2"><p>روزگاری شد که این تار از ضعیفی بیصداست</p></div></div>