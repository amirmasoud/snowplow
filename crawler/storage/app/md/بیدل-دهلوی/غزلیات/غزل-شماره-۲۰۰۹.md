---
title: >-
    غزل شمارهٔ ۲۰۰۹
---
# غزل شمارهٔ ۲۰۰۹

<div class="b" id="bn1"><div class="m1"><p>یک چشم حیرت است زسرتا به پا لبم</p></div>
<div class="m2"><p>یارب به روی نام‌که‌گردید وا لبم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چند پرسی از من آشفته حال دل</p></div>
<div class="m2"><p>چون ساغر شکسته ندارد صدا لبم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بال هوس ز موج‌ گهر سر نمی‌کشد</p></div>
<div class="m2"><p>چسبیده است بر دل بی مدعا لبم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لبریز حیرتم به‌کمالی‌که روزگار</p></div>
<div class="m2"><p>خشت بنای آینه ریزد ز قالبم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواهی محیط فرض‌کن و خواه قطره‌گیر</p></div>
<div class="m2"><p>دارد همین یک آبله از سینه تا لبم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آسان به شکر تیغ تو نتوان بر آمدن</p></div>
<div class="m2"><p>جوشد مگر چو زخم ز سر تا به‌ پا لبم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌ترسم از فراق بحدی گه گاه حرف</p></div>
<div class="m2"><p>در خون تپم اگر شود از هم جدا لبم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>افسون شوق زمزمه آهنگ جرات‌ست</p></div>
<div class="m2"><p>ور نه‌ کجا حدیث وصال و کجا لبم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عمری‌ست عافیت کف افسوس می‌زند</p></div>
<div class="m2"><p>من در گمان‌ که با سخن است آشنا لبم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غیر از تری چه نغمه ‌کشد ساز احتیاج</p></div>
<div class="m2"><p>موجی در آب ریخته است از حیا لبم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>احرام پایبوس تو اقبال ناز کیست</p></div>
<div class="m2"><p>روید مگر ز پردهٔ برگ حنا لبم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گردون به مهر خامشی‌ام داغ می‌کند</p></div>
<div class="m2"><p>چون ماه نو مباد فتد کار با لبم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خمیازه هم غنیمت صهبای زندگی است</p></div>
<div class="m2"><p>یا رب چو گل کشد قدحی از هوا لبم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیدل زبان موج‌گهر باب شکوه نیست</p></div>
<div class="m2"><p>گر مرد قدرتی تو به ناخن‌گشا لبم</p></div></div>