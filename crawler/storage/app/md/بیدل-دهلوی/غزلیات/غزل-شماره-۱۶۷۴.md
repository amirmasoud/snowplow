---
title: >-
    غزل شمارهٔ ۱۶۷۴
---
# غزل شمارهٔ ۱۶۷۴

<div class="b" id="bn1"><div class="m1"><p>تا کی خیال هستی موهوم‌، سر برآر</p></div>
<div class="m2"><p>عنقایی‌، ای حباب‌، از این بیضه پر برآر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حیف از دلی‌ که رنج فسون نفس ‌کشد</p></div>
<div class="m2"><p>از قید رشته‌ای که نداری گهر برآر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهدی‌ که شعله‌ات نکشد ننگ اخگری</p></div>
<div class="m2"><p>خاکستری برون ده و رخت سفر برآر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل جمع ‌کن ز آمد و رفت خیال پوچ</p></div>
<div class="m2"><p>بر روی خلق از مژهٔ بسته در برآر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سامان دهر نیست حریف قناعتت</p></div>
<div class="m2"><p>این بحر را به قدر لب خشک تر برآر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیماب رو در آتش و روغن در آب باش</p></div>
<div class="m2"><p>خود را ز جرگهٔ بد و نیک این قدر برآر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پشت دوتا تدارک او بار سرکشی‌ست</p></div>
<div class="m2"><p>تیغ آن زمان‌ که ریخت دم از هم به سر برآر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آهی به لب رسان ‌که نیفسرده‌ای هنوز</p></div>
<div class="m2"><p>زان‌ پیشتر که سنگ برآری شرر برآر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سامان تازه‌رو‌یی‌ات از شمع نیست کم</p></div>
<div class="m2"><p>خار شکسته را ز قدم گل به سر برآر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فکر شکست چینی دل مفت جهد گیر</p></div>
<div class="m2"><p>مویی‌ست در خمیر تو ای بی‌خبر برآر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در خون نشسته است غبار شهید عشق</p></div>
<div class="m2"><p>ای خاک تشنه مرده زبان دگر برآر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل نفس به یاد خدنگت گرفته است</p></div>
<div class="m2"><p>تا زندگی‌ست خون خور و تیر از جگر برآر</p></div></div>