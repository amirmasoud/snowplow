---
title: >-
    غزل شمارهٔ ۲۲۱۷
---
# غزل شمارهٔ ۲۲۱۷

<div class="b" id="bn1"><div class="m1"><p>زبن سجدهٔ خود دار تفاخر چه فروشم</p></div>
<div class="m2"><p>در راه تو افتاده سرم لیک به دوشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون موج‌گهر پای من و دامن حیرت</p></div>
<div class="m2"><p>سعی طلبی بود که‌ کرد آبله پوشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تغییر خیالی دهم و بگذرم از خویش</p></div>
<div class="m2"><p>بر رنگ سواد است جنون تازی هوشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خرسندی اوهام ز اسرار چه فهمد</p></div>
<div class="m2"><p>آنسوی یقین مژده رسانده‌ست سروشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مجبور ترددکدهٔ وهم چه سازد</p></div>
<div class="m2"><p>روزی دو نفس بال فشان است به‌گوشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چیزی ز من و ما بنمایم چه توان‌ کرد</p></div>
<div class="m2"><p>گرم است دکان آینه داری بفروشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زبن بزم به جز زحمت عبرت چه ‌کشد کس</p></div>
<div class="m2"><p>طنبور تقاضای همین مالش گوشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون دیدهٔ آهو رمی افروخت چراغم</p></div>
<div class="m2"><p>کز دامن صحرا نتوان کرد خموشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دور است به مژگان بلند تو رسیدن</p></div>
<div class="m2"><p>من سرمه نگشتم چه‌کنم‌گر نخروشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل چو خم می چقدر دل به هم آید</p></div>
<div class="m2"><p>تا من به ‌گداز آیم و با خویش بجوشم</p></div></div>