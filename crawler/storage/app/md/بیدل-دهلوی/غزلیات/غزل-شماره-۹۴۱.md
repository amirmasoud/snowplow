---
title: >-
    غزل شمارهٔ ۹۴۱
---
# غزل شمارهٔ ۹۴۱

<div class="b" id="bn1"><div class="m1"><p>هرچه آنجاست چو آنجا روی‌اینجاگردد</p></div>
<div class="m2"><p>چه خیال است‌ که امروز تو فردا گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مقامی‌ که بود ترک و طلب امکانی</p></div>
<div class="m2"><p>رو به دنیاست همان گرچه ز دنیا گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جمع شو ، مرکز نه دایرهٔ چرخ برآ</p></div>
<div class="m2"><p>قطره چون فال‌ گهر زد دل دریا گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رستن از پیچ و خم رشتهٔ آمال‌ کراست</p></div>
<div class="m2"><p>بگسلی از دو جهان تا گرهی وا گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نور دل درگرو کسب قبول سخن است</p></div>
<div class="m2"><p>به نفس‌ گو چه دهد سنگ‌ که مینا گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سن بی‌ سر و پا تفرقهٔ ساز حیاست</p></div>
<div class="m2"><p>آب چون بر در فواره زد اجزا گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طور مستان نکشد تهمت تغییر وفا</p></div>
<div class="m2"><p>خط ساغر چه خیال است چلیپا گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عجز تقریر من آخر به اشارات کشید</p></div>
<div class="m2"><p>ناله چون راه نفس‌ گم کند ایما گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نامهٔ رمز نفس در پر عنقا بربند</p></div>
<div class="m2"><p>سر این رشته نه جایی‌ست‌که پیداگردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کعبه و دیر مگو گرد تو گشتیم بس است</p></div>
<div class="m2"><p>آسیا نیست سر شوق‌ که هر جا گردد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گوهر آزادگی موج نخواهد بیدل</p></div>
<div class="m2"><p>سر چو گردید گران آبلهٔ پا گردد</p></div></div>