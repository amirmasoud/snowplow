---
title: >-
    غزل شمارهٔ ۱۷۴۱
---
# غزل شمارهٔ ۱۷۴۱

<div class="b" id="bn1"><div class="m1"><p>خودسر ز عافیت به تکلف برید و بس</p></div>
<div class="m2"><p>آهی‌ که قد کشید به دل خط‌ کشید و بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راه تلاش دیر و حرم طی نمی‌شود</p></div>
<div class="m2"><p>باید به طوف آبلهٔ پا رسید و بس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جمعی‌ که در بهشت فراغ آرمیده‌اند</p></div>
<div class="m2"><p>طی‌ کرده‌اند جادهٔ دشت امید و بس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل با همه شهود ز تحقیق پی نبرد</p></div>
<div class="m2"><p>آیینه آنچه دید همین عکس دید و بس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناز سجود قبلهٔ توفیق می‌کشیم</p></div>
<div class="m2"><p>زین‌ گردنی‌ که تا سر زانو خمید و بس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محمل‌کشان عجز، فلکتاز قدرتند</p></div>
<div class="m2"><p>تا آفتاب سایه به پهلو دوید و بس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عیش بهار عشق ز پهلوی عجز نیست</p></div>
<div class="m2"><p>در باغ نیز، شمع‌ گل از خویش چید و بس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما را درین ستمکده تدبیر عافیت</p></div>
<div class="m2"><p>ارشاد بسمل است‌ که باید تپید و بس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هیهات راه مقصد ما وانموده‌اند</p></div>
<div class="m2"><p>بر جاده‌ای‌ که هیچ نگردد پدید و بس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواندیم بی‌تمیز رقمهای خیر و شر</p></div>
<div class="m2"><p>از نامه‌ای که بود سراسر سفید و بس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رفع تظلم دم پیری چه ممکن است</p></div>
<div class="m2"><p>هرجا رسید صبح‌ گریبان د‌رید و بس</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل پیام وصل به حرمان رساندنی‌است</p></div>
<div class="m2"><p>موسی برون پرده ندیدن شنید و بس</p></div></div>