---
title: >-
    غزل شمارهٔ ۲۳۵۶
---
# غزل شمارهٔ ۲۳۵۶

<div class="b" id="bn1"><div class="m1"><p>خاک نمیم امروز دی محو یاد بودیم</p></div>
<div class="m2"><p>در عالمی‌که هستیم شادیم و شاد بودیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درکوه آتش سنگ‌، در باغ جوهر رنگ</p></div>
<div class="m2"><p>با این متاع موهوم در هر مزاد بودیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چاک جگرکجا بود مژگان تر کرا بود</p></div>
<div class="m2"><p>با داغ این هوسها در اتحاد بودیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اجزای ما ز شوخی ناکام رفت بر باد</p></div>
<div class="m2"><p>گر می‌نشست این‌گرد نقش مراد بودیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق مقام ما را با خود خیالها بود</p></div>
<div class="m2"><p>در نرد اعتبارات خال زیاد بودیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رسم حضور و غیبت‌کم داشت محفل انس</p></div>
<div class="m2"><p>فارغ ز خیر مقدم تأخیر باد بودیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بستیم ازتعلق بر دوش فطرت آخر</p></div>
<div class="m2"><p>افسردنی که گویی یکسر جماد بودیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فطرت ز ما جنون خواند تحقیق چشم خواباند</p></div>
<div class="m2"><p>چون نقش بال عنقا پر بی‌سواد بودیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر از فرامشانیم امروز شکوه ازکیست</p></div>
<div class="m2"><p>زین پیش هم کسی را ما کی به یاد بودیم</p></div></div>