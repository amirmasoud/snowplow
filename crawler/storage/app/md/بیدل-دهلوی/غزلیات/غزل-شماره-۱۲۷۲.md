---
title: >-
    غزل شمارهٔ ۱۲۷۲
---
# غزل شمارهٔ ۱۲۷۲

<div class="b" id="bn1"><div class="m1"><p>باغ نیرنگ جنونم نیست آسان بشکفد</p></div>
<div class="m2"><p>خون خورد صد شعله تا داغی به سامان بشکفد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آببار ما ادبکاران گداز جرأت است</p></div>
<div class="m2"><p>چشم ما مشکل‌ که بر رخسار جانان بشکفد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیدماغی فرصت‌اندیش شکست رنگ نیست</p></div>
<div class="m2"><p>گل به رنگ صبح بابد دامن‌افشان بشکفد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تنگنای عرصهٔ موهوم امکان را کجاست</p></div>
<div class="m2"><p>اتفدر وسعت‌که یک زخم نمایان بشکفد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در شکست من طلسم عیش امکان بسته‌اند</p></div>
<div class="m2"><p>رنگ آغوشی‌کشد تا این‌گلستان بشکفد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مهرورزی نیست اینجا کم ز باد مهرگان</p></div>
<div class="m2"><p>چاک زن جیب وفا تا طبع یاران بشکفد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وضع مستوری غبار مشرب مجنون مباد</p></div>
<div class="m2"><p>داغ دل یارب به رنگ ناله عریان بشکفد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قابل نظارِِهٔ آن جلوه‌گشتن مشکل است</p></div>
<div class="m2"><p>گرهمه صد نرگسستان چشم حیران بشکفد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هیچ تخمی قابل سرسبزی امید نیست</p></div>
<div class="m2"><p>اشک بایدکاشتن چندان که توفان بشکفد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زبن‌ چمن محروم دارد چشم خواب‌آلوده‌ام</p></div>
<div class="m2"><p>بی‌بهاری‌نیست حیرت‌کاش مژگان بشکفد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در گلستانی‌ که دارد اشک بیدل شبنمی</p></div>
<div class="m2"><p>برگ برگش نالهٔ بلبل به دامان بشکفد</p></div></div>