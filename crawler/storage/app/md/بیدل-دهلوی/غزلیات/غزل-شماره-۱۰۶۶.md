---
title: >-
    غزل شمارهٔ ۱۰۶۶
---
# غزل شمارهٔ ۱۰۶۶

<div class="b" id="bn1"><div class="m1"><p>پیری‌ام آخر می و پیمانه برد</p></div>
<div class="m2"><p>باد سحر شمع ز کاشانه برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده سیاهی ز گل و لاله چید</p></div>
<div class="m2"><p>کوش‌گرانی ز هر افسانه برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمع جنون آبله‌پا کرده‌ گم</p></div>
<div class="m2"><p>سر به هوا لغزش مستانه برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشمکش ازسعی نفس قطع شد</p></div>
<div class="m2"><p>اره خودآرایی دندانه برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یاد خطش‌کردم و دل باختم</p></div>
<div class="m2"><p>سایهٔ مور از کف من دانه برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکه در این انجمن حرص وگد</p></div>
<div class="m2"><p>ساخت به خودگنج به ویرانه برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حسرت دیدار گریبان درید</p></div>
<div class="m2"><p>آینهٔ ما همه جا شانه برد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواندن اسرار وفا مشکل است</p></div>
<div class="m2"><p>مهر شد آن نامه‌که پروانه برد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در دل ما ذوق تماشا نماند</p></div>
<div class="m2"><p>آه‌کسی آینه زین خانه برد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قاصد دلبر جگرم داغ‌ کرد</p></div>
<div class="m2"><p>نامهٔ من ناله شد اما نه برد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وقت جنون خوش‌که غم خانمان</p></div>
<div class="m2"><p>یک دو دم از بیدل دیوانه برد</p></div></div>