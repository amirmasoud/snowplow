---
title: >-
    غزل شمارهٔ ۲۷۶۰
---
# غزل شمارهٔ ۲۷۶۰

<div class="b" id="bn1"><div class="m1"><p>افتاده‌ام به راهت چون اشک بی‌روانی</p></div>
<div class="m2"><p>مکتوب انتظارم شاید مرا بخوانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ساز حیرت من مضمون ناله درباب</p></div>
<div class="m2"><p>گرد نگاه دارد فریاد ناتوانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنجا که عشق ریزد آیینهٔ تحیر</p></div>
<div class="m2"><p>روشنتر از بیانها مضمون بیزبانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا اضطراب اشکی یا وحشت نگاهی</p></div>
<div class="m2"><p>تاکی به رنگ مژگان پرواز آشیانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از رفتن نفسها آثار نیست پیدا</p></div>
<div class="m2"><p>نقش قدم ندارد صحرای زندگانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دریای عشق و ساحل ای بیخبر چه حرفست</p></div>
<div class="m2"><p>تا قطره دارد اینجا توفان بیکرانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا چند سنگ راهت باشد غبار هستی</p></div>
<div class="m2"><p>از وحشت شرر کن نقش سبک‌عنانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در عالمی که نقدش مصروف احتیاجست</p></div>
<div class="m2"><p>ابرام می‌فروشی چند‌ان که زنده مانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا طبع دون نسازد مغرور اختیارت</p></div>
<div class="m2"><p>ناکردن است اولی کاری که می‌توانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی صید دیدهٔ دام مخمور می‌نماید</p></div>
<div class="m2"><p>قد دو تاست اینجا خمیازهٔ جوانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خمخانهٔ تمنا جامی دگر ندارد</p></div>
<div class="m2"><p>مفتست بیدماغی گر نشئه می‌رسانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل غبار آهی تا رنگ اوج گیرد</p></div>
<div class="m2"><p>از چاک سینه دارم چون صبح نردبانی</p></div></div>