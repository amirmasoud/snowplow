---
title: >-
    غزل شمارهٔ ۲۶۹۳
---
# غزل شمارهٔ ۲۶۹۳

<div class="b" id="bn1"><div class="m1"><p>به یأس هم نپسندید ننگ بیکاری</p></div>
<div class="m2"><p>دل شکستهٔ ماکرد ناله معماری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آن بساط ‌که موجود بودن‌ست غرض</p></div>
<div class="m2"><p>چو ذره اندکی ما بس است بسیاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به رنگ غنچه درین باغ بیدماغان را</p></div>
<div class="m2"><p>نسیم درد سر و شبنم است سر باری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خدنگ ناله که از جوش نه فلک گذرد</p></div>
<div class="m2"><p>منش به داغ جگر می‌کنم سپرداری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرم به خدمت هستی فرو نمی‌آید</p></div>
<div class="m2"><p>نفس به گردنم افتاد و کرد زناری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه سحر کرد ندانم نگاه جادویت</p></div>
<div class="m2"><p>که مرده است جهانی به ذوق بیماری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آرزوی دهان تو بسکه دلتنگم</p></div>
<div class="m2"><p>نفس به سینهٔ من ره برد به دشواری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهانی از نم چشمم مگر به توفان رفت</p></div>
<div class="m2"><p>به بحرش ای مژه‌ام بیش ازبن نیفشاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دگر چو سایه‌ام از خانمان چه می‌پرسی</p></div>
<div class="m2"><p>نشسته‌ام به غبار شکسته دیواری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نگاه اگر نشود صرف تار و پود تمیز</p></div>
<div class="m2"><p>سر برهنه ‌کند چون حباب دستاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز هرزه تازی اگر بگذرد سرشک خوش است</p></div>
<div class="m2"><p>گهر شود چو نشیند ز قطره سیاری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کجاست‌ گوهر دیگر محیط عرفان را</p></div>
<div class="m2"><p>مگر ز جیب تامل سری برون آری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طلسم غنچه هجوم بهار در قفس است</p></div>
<div class="m2"><p>به خون نشین و طرب‌کن اگر دلی داری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چه جلوه‌ها که نشد فرش حیرتم بیدل</p></div>
<div class="m2"><p>صفای خانهٔ آیینه داشت همواری</p></div></div>