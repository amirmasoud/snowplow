---
title: >-
    غزل شمارهٔ ۲۱۳۱
---
# غزل شمارهٔ ۲۱۳۱

<div class="b" id="bn1"><div class="m1"><p>محو دلم مپرس ز تحقیق عنصرم</p></div>
<div class="m2"><p>آیینه خنده است دماغ تحیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن ناله‌ام‌ که با همه پرواز نارسا</p></div>
<div class="m2"><p>تا دل توان رسید ز نقب تاثرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پستی درین محیط‌ گهر کرد قطره را</p></div>
<div class="m2"><p>کسب فروتنی است عروج تفاخرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دانش ز پیکرم عرق انفعال ریخت</p></div>
<div class="m2"><p>گل کرد از گداز خجالت تحیرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زین‌گلشنم چه برگ نشاط و چه ساز عیش</p></div>
<div class="m2"><p>خون می‌شود چو گل دم آبی‌ که می‌خورم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جرات به ناتوانی من ناز می‌کند</p></div>
<div class="m2"><p>رنگی شکسته‌ام چقدرها بهادرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرد هزار جاده به منزل شکسته است</p></div>
<div class="m2"><p>چون موج‌ گوهر آبله پای تحیرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمع خموشم از سر زانوی من مپرس</p></div>
<div class="m2"><p>آیینه زنگ بست به جیب تفکرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درد دلم‌، گداز غمم‌، داغ حیرتم</p></div>
<div class="m2"><p>فریاد از خیالم و آه از تصورم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نقدی دگر نمی‌شمرد کیسهٔ حباب</p></div>
<div class="m2"><p>بیدل من از تهی شدن خویشتن پرم</p></div></div>