---
title: >-
    غزل شمارهٔ ۲۳۴۰
---
# غزل شمارهٔ ۲۳۴۰

<div class="b" id="bn1"><div class="m1"><p>سایه‌وار از نارسایان جهان غربتیم</p></div>
<div class="m2"><p>شخص طاقت رفته وما نقش پای طاقتیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عجزبینش جوهر ما را به خاک افکنده است</p></div>
<div class="m2"><p>یک مژه‌ گر چشم برداریم‌ گرد فطرتیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دامن افشاندن ز اسباب جهان بی‌مدار</p></div>
<div class="m2"><p>آنقدرها نیست اما اندکی بی‌جرأتیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچکس چون شمع داغ بی‌تمیزیها مباد</p></div>
<div class="m2"><p>سر به جیب و پا به دامن درتلاش راحتیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حرص بر خوان قناعت هم همان خون می‌خورد</p></div>
<div class="m2"><p>میهمانان غناییم و فضولی قسمتیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زبن وبالی‌ کز وفاق حاضران‌ گل می‌کند</p></div>
<div class="m2"><p>همچو یاد رفتگان آیینه‌دار عبرتیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفت ایامی‌ که عزلت آبروی ناز داشت</p></div>
<div class="m2"><p>این زمان از اختلاط این و آن بی‌حرمتیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچو مینایی نمی از جبههٔ ما کم نشد</p></div>
<div class="m2"><p>آب می‌گردیم اما انفعال خجلتیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با همه نومیدی اقبال سیه‌بختان رساست</p></div>
<div class="m2"><p>چون شب عصیان ز مشتاقان صبح رحمتیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواه عالم نقش بند و خواه عنقاکن خیال</p></div>
<div class="m2"><p>در دماغ خامهٔ نقاش موی صورتیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیم چشمک خانه روشن‌کردنی داریم و هیچ</p></div>
<div class="m2"><p>چون شرر بیدل چراغ دودمان فرصتیم</p></div></div>