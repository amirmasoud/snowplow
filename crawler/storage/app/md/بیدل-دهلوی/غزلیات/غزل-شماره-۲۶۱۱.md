---
title: >-
    غزل شمارهٔ ۲۶۱۱
---
# غزل شمارهٔ ۲۶۱۱

<div class="b" id="bn1"><div class="m1"><p>به دست‌ تیغ تو تا خون من حنا بسته</p></div>
<div class="m2"><p>به حیرتم که عجب خویش را بجا بسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه سان به روی تو مرغ نظر کند پرواز</p></div>
<div class="m2"><p>که حیرت از مژه‌اش رشته‌ها به پا بسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دل ز شوق وصالت صد آرزو دارم</p></div>
<div class="m2"><p>ولی ادب ره تقریر مدعا بسته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فراق بیگنهم‌کشت و نقد داغ خطا</p></div>
<div class="m2"><p>به‌گردن دل خون‌گشته خون‌بها بسته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز جیب ناز خطش سر برون نمی آرد</p></div>
<div class="m2"><p>که عقد عهد به خلوتگهٔ حیا بسته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو شمع تا به فنا هیچ جا نیاسایم</p></div>
<div class="m2"><p>مرا سریست‌ که احرام بوریا بسته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تن از بساط حریرم چگونه بندد طرف</p></div>
<div class="m2"><p>که دل به سلسلهٔ نقش بوربا بسته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهار بوسه به پای تو داد و خون‌ گردید</p></div>
<div class="m2"><p>نگه‌ تصور رنگینی حنا بسته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به وادی طلب نارسایی عجزیم</p></div>
<div class="m2"><p>که هرکه رفته زخود خویش را به ما بسته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کدام نقش که گردون نبست بی‌ستمش</p></div>
<div class="m2"><p>دلی شکسته اگر صورت صدا بسته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مگر ز زلف تو دارد طریق بست و گشاد</p></div>
<div class="m2"><p>که بیدل اینهمه مضمون دلگشا بسته</p></div></div>