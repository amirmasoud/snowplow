---
title: >-
    غزل شمارهٔ ۱۷۴۲
---
# غزل شمارهٔ ۱۷۴۲

<div class="b" id="bn1"><div class="m1"><p>غم نه‌تنها بر دلم نالید و بس</p></div>
<div class="m2"><p>عیش هم بر فرصتم خندید و بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر طواف‌ کعبهٔ درد آرزوست</p></div>
<div class="m2"><p>می‌توان گرد دلم گردید و بس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون ‌گلم زین باغ عبرت داده‌اند</p></div>
<div class="m2"><p>آنقدر دامن که باید چید و بس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جاده چون طی شد حضور منزل است</p></div>
<div class="m2"><p>رشته می‌باید به پا پیچید و بس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>علم دانش یک قلم هیچ است و پوچ</p></div>
<div class="m2"><p>اینقدر می‌بایدت فهمید و بس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صحبت دل با نفس معکوس بود</p></div>
<div class="m2"><p>سبحه اینجا رشته‌ گردانید و بس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل حرم تا دیر در خون می‌تپید</p></div>
<div class="m2"><p>خانه راه خانه می‌پرسید و بس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون شرر در راه ‌کس ‌گردی نبود</p></div>
<div class="m2"><p>شرم فرصت چشم ما پوشید و بس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر بهار عیش می‌نازد غنا</p></div>
<div class="m2"><p>بیخبرکاین‌گل قناعت چید و بس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیقرارم داشت درد احتیاج</p></div>
<div class="m2"><p>ناله‌ای‌ کردم‌ که‌ کس نشنید و بس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>منزل مقصود پرسیدم ز اشک</p></div>
<div class="m2"><p>گفت باید یک مژه لغزید و بس</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل اسباب جهان چیزی نبود</p></div>
<div class="m2"><p>زندگی خواب پریشان دید و بس</p></div></div>