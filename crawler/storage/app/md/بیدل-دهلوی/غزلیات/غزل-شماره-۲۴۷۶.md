---
title: >-
    غزل شمارهٔ ۲۴۷۶
---
# غزل شمارهٔ ۲۴۷۶

<div class="b" id="bn1"><div class="m1"><p>ما را ز بار هستی تاکی غم خمیدن</p></div>
<div class="m2"><p>آیینه هم سیه‌ کرد دوش‌ از نفس‌کشیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندین‌ گهر درین بحر افسرد و خاک ‌گردید</p></div>
<div class="m2"><p>یمن آنقدر ندارد بر عافیت تنیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رنگ شکسته دارد اقبال سرخ رویی</p></div>
<div class="m2"><p>این لعاب بی‌بها را نتوان به زر خریدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ارباب رنگ یکسر زندانی لباسند</p></div>
<div class="m2"><p>بی‌دام نیست طاووس در عالم پریدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک نخل از ین‌ گلستان از اصل باخبر نیست</p></div>
<div class="m2"><p>سر بر هواست خلقی از پیش پا ندیدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در قید جسم تا کی افسرده بایدت زیست</p></div>
<div class="m2"><p>ای دانه سبز بختی‌ست از خاک سرکشیدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>افسانهٔ حلاوت با ساز انگبین رفت</p></div>
<div class="m2"><p>ای شمع چند خواهی انگشت خود مکیدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا وصل جلوه‌ گر شد دل قطع آرزو کرد</p></div>
<div class="m2"><p>آنسوی رنگ و بوبرد این میوه را رسیدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درکاروان شوقم دل بر دل جرس سوخت</p></div>
<div class="m2"><p>این اشک بی‌فغان نیست از درد ناچکیدن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای‌ کاش قطع‌ گردد سر رشتهٔ تعلق</p></div>
<div class="m2"><p>مقراض وار عمرم شد صرف لب‌گزیدن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جز خاک‌گشتنم نیست عرص نیاز دیگر</p></div>
<div class="m2"><p>باید به پیش چشمت از سرمه خط‌کشیدن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رنگی به پردهٔ شوق آرایش هوس داشت</p></div>
<div class="m2"><p>چون‌گل زدیم آخر گل بر سر دمیدن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بیدل ز دست مگذار دامان بیقراری</p></div>
<div class="m2"><p>چون آب تیغ نتوان خون خورد از آرمیدن</p></div></div>