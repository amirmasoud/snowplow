---
title: >-
    غزل شمارهٔ ۴۲۲
---
# غزل شمارهٔ ۴۲۲

<div class="b" id="bn1"><div class="m1"><p>اضطراب نبض دل تمهید آهنگ فناست</p></div>
<div class="m2"><p>شعله‌در هر پر فشاندن‌اندکی‌از خود جداست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شخص پیری نفی هستی می‌کند هشیار باش</p></div>
<div class="m2"><p>صورت قد دوتا آیینهٔ ترکیب لاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین‌چمن بر دستگاه‌رنگ نتوان دوخت چشم</p></div>
<div class="m2"><p>غنچه تا ناخن به خون دل نشوید بی‌حناست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچ‌کس چون ما اسیر بی‌تمیزیها مباد</p></div>
<div class="m2"><p>مشت خاکی درگره داریم‌کاین آب بقاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاک‌گشتیم و غبار ما هوایی درنیافت</p></div>
<div class="m2"><p>آنکه بر خمیازه حسرت می‌کشد آغوش ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حاصل کونین پامال ندامت کردنی‌ست</p></div>
<div class="m2"><p>دانهٔ کشت امل را سودن دست آسیاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رشحهٔ ابر نیازم غافل از عجزم مباش</p></div>
<div class="m2"><p>سجدهٔ‌من ریشه‌دارد هرکجا مشتی گیاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شوق‌درکار است‌وضع‌این و آن منظور نیست</p></div>
<div class="m2"><p>با نگه هر برگ این‌گلشن به رنگی آشناست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بند بندم فکرآن موی میان درهم شکست</p></div>
<div class="m2"><p>ناتوانی هرکجا زور آورد زورآزماست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>داغ می‌بالدکه دل خلوتگه جمعیت است</p></div>
<div class="m2"><p>ناله می‌نالدکه اینجا جای آسایش‌کجاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رهروان تمهید پروازی‌که می‌آید اجل</p></div>
<div class="m2"><p>دودها از خود برون تازی‌که آتش در قفاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل از نیرنگ اسباب من و ما غافلی</p></div>
<div class="m2"><p>اینگه صبح زندگی فهمیده‌ای روز جزاست</p></div></div>