---
title: >-
    غزل شمارهٔ ۱۲۱۶
---
# غزل شمارهٔ ۱۲۱۶

<div class="b" id="bn1"><div class="m1"><p>راحت دل ز نفس بال‌فشان می‌باشد</p></div>
<div class="m2"><p>آب این آینه چون باد روان می‌باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شعله‌ها رنگ به خاکستر ما باخته است</p></div>
<div class="m2"><p>شور پرواز درن سرمه نهان می‌باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سادگی جنس چو آیینه دکانی داریم</p></div>
<div class="m2"><p>زینت ما به متاع دگران می‌باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به زبان راز دل خویش سپردیم چو شمع</p></div>
<div class="m2"><p>موج این‌گوهر خون‌گشته زبان می‌باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حایلی نیست به جولانگه معنی هشدار</p></div>
<div class="m2"><p>خواب پا در ره ما سنگ‌نشان می‌باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی‌گهر نشئهٔ تمکین صدف ممکن نیست</p></div>
<div class="m2"><p>تا نم آب بگو شست‌گران می‌باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کینهٔ خصم بداندیش ملایم‌گفتار</p></div>
<div class="m2"><p>نیش خاری است ‌که در آب نهان می‌باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ایمن از فتنه نگردی به مدارای حسود</p></div>
<div class="m2"><p>آب تیغ آفت قعرش به‌کران می‌باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تیره‌بختی نفسی از طلبم غافل نیست</p></div>
<div class="m2"><p>سایه دایم ز پی شخص روان می‌باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ذوق خود بینی ما تا نشود محو فنا</p></div>
<div class="m2"><p>نتوان یافت که آیینه چسان می‌باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شرر از سنگ دهد عرضهٔ شوخی بیدل</p></div>
<div class="m2"><p>تیغ‌ کین را سخن سخت فسان می‌باشد</p></div></div>