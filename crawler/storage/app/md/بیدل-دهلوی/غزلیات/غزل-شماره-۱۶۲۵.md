---
title: >-
    غزل شمارهٔ ۱۶۲۵
---
# غزل شمارهٔ ۱۶۲۵

<div class="b" id="bn1"><div class="m1"><p>زندگی افسرد فال شوخی سودا زنید</p></div>
<div class="m2"><p>انتخاب عالم آشوبی ازین اجزا زنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند چون‌ گرداب باید بود محو پیچ و تاب</p></div>
<div class="m2"><p>بر امید ساحلی چون موج دست و پا زنید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر فروغ شمع بیداد نفس تیغ است و بس</p></div>
<div class="m2"><p>چند چون زنگار بر آیینهٔ دلها زنید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شورتوفان حوادث بر محیط افتاده است</p></div>
<div class="m2"><p>بعد ازین چون موج می بر کشتی صهبا زنید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باز آغوش دم تیغی مهیاکرده‌ایم</p></div>
<div class="m2"><p>خنده‌ ای از بخیه می‌ باید به زخم ما زنید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جلوه در کار است غفلت چند ای بیحاصلان</p></div>
<div class="m2"><p>چشم خواب‌آلود خود را یک دو مژگان پا زنید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راحتی ‌گر هست در آغوش ترک مدعاست</p></div>
<div class="m2"><p>احتیاج آشوبها دارد به استغنا زنید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سیر نیرنگ جهان وقف تغافل خوشتر است</p></div>
<div class="m2"><p>نعل واژونی به پای دیدهٔ بینا زنید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شعله‌سان چند از رک‌گردن علم افراشتن</p></div>
<div class="m2"><p>سکهٔ افتادگی بک ره چو تقش پا زنید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بستن مژگان به چندین شمع دامن می‌زند</p></div>
<div class="m2"><p>یک شبیخون برصف اندیشهٔ دنیا زنید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از پر عنقا صدایی می‌رسد کای غافلان</p></div>
<div class="m2"><p>موج بسیار است اگر بیرون این درم‌با زنید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>معنی آرام بیدل می‌توان معلوم‌کرد</p></div>
<div class="m2"><p>گر به رنگ موج بر قلب ‌تپیدن‌ها زنید</p></div></div>