---
title: >-
    غزل شمارهٔ ۱۶۹۷
---
# غزل شمارهٔ ۱۶۹۷

<div class="b" id="bn1"><div class="m1"><p>غبار فرصت از این خاکدان وهم مگیر</p></div>
<div class="m2"><p>که پیرگشت سحرتا دهن‌گشود به شیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امل به صبح قیامت رساند گرد نفس</p></div>
<div class="m2"><p>گذشت فرصت تقدیمت آن سوی تاخیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همین‌کشاکش اوهام تا ابد باقی‌ست</p></div>
<div class="m2"><p>فنا بجاست توخواهی بزی و خواه بمیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در این چمن نفسی می‌کشیم و می‌گذربم</p></div>
<div class="m2"><p>گمان مبر به‌کمانخانه آرمیدن تیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نفس درازی اظهار جرأت آهنگ است</p></div>
<div class="m2"><p>به سرمه تا نرسد ناله‌، عذر ما بپذیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هنوز دامن صحرا ز گردباد پُر است</p></div>
<div class="m2"><p>غبار عالم دیوانه نیست بی‌زنجیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در این ستمکده سود و زیان من این است</p></div>
<div class="m2"><p>که از شکستن دل ناله می‌کنم تعمیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سیاه‌بختی‌ام آرایشی نمی‌خواهد</p></div>
<div class="m2"><p>ز خاک پیرهن سایه را بس است عبیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صفای دل به نفس عمرهاست می‌بازم</p></div>
<div class="m2"><p>چو صبح آینه در زنگ می‌کنم شبگیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به ناتوانی من یاس می‌خورد سوگند</p></div>
<div class="m2"><p>که ناله‌ای نکشیدم چو خامهٔ تصویر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز ساز عجز به هرجا نفس زدم بیدل</p></div>
<div class="m2"><p>به قدر جوهر آیینه شد بلند صفیر</p></div></div>