---
title: >-
    غزل شمارهٔ ۲۳۹۱
---
# غزل شمارهٔ ۲۳۹۱

<div class="b" id="bn1"><div class="m1"><p>عمرها در پرده بود اسرار وهم ما و من</p></div>
<div class="m2"><p>صیقل زنگار این آیینه شد آخر کفن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با اقامت ما نفس سرمایگان بی‌نسبتیم</p></div>
<div class="m2"><p>دامنی دارد غبار صبح در آهن شکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قید جسمانی گوارا کرد افسون معاش</p></div>
<div class="m2"><p>بهر آب و دانه خلقی در قفس دارد وطن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن هوس منزل که باغ جنتش نامیده‌اند</p></div>
<div class="m2"><p>رنگها چیده‌ست لیکن در غبار وهم و ظن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر طرف جام خیالی کجکلاه بیخودی‌ست</p></div>
<div class="m2"><p>گردش چشمی که دارد این فرنگی انجمن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند باشی انفعال آمادهٔ افراط عیش</p></div>
<div class="m2"><p>خندهٔ سرشار دارد گریه از آب دهن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غافل از تقدیر بر تدبیر می‌چینی دکان</p></div>
<div class="m2"><p>کارگاه بی‌نیازی نیست جای علم و فن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از عمارت خشت غفلت تا لحد چیده‌ست خلق</p></div>
<div class="m2"><p>ای ز خود غافل تو هم خشتی براین ویرانه زن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هیچکس از انفعال زندگی آگاه نیست</p></div>
<div class="m2"><p>شمع ازشرم آب می‌گردد تو زربن‌کن لگن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آنقدرها رفتن از خویشت نمی‌خواهد تلاش</p></div>
<div class="m2"><p>شمع را یک‌گردش رنگست و صد دامن زدن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سعی خاموشی ثبات طبع انشا کردن است</p></div>
<div class="m2"><p>آتش یاقوت می‌گردد نفس از سوختن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قالب فرسوده زحمت انتظار مرگ نیست</p></div>
<div class="m2"><p>می‌کند ایجاد سیل از خوبش دیوار کهن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>غازه ی حسن ادا آسان نمی‌آید به دست</p></div>
<div class="m2"><p>فکر خونها می‌ خو‌رد تا رنگ می گیرد سخن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کارگاه انتظار ما تسلی باف بود</p></div>
<div class="m2"><p>پنبهٔ چشم سپید آورد بوی پیرهن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خون پامالی که چون رنگ حنایت داده‌اند</p></div>
<div class="m2"><p>آبرو گردد اگر بر جا توانی ریختن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زندگی بیدل جهانی را ز مرگ آگاه‌ کرد</p></div>
<div class="m2"><p>محو بود اندوه رفتن‌ گر نمی‌بود آمدن</p></div></div>