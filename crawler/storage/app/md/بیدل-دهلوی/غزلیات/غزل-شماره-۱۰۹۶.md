---
title: >-
    غزل شمارهٔ ۱۰۹۶
---
# غزل شمارهٔ ۱۰۹۶

<div class="b" id="bn1"><div class="m1"><p>شب‌ که دل از یأس مطلب باده‌ای در جام‌ کرد</p></div>
<div class="m2"><p>یک جهان حسرت به توفان داد و آهش نام‌کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برنمی‌آید سپند من به استیلای شوق</p></div>
<div class="m2"><p>از جرس باید دل بی‌انفعالم وام‌ کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم من شد پرده ی زنبور و بیداری ندید</p></div>
<div class="m2"><p>غفلت آخر حشر من درکسوت با دام‌کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آبم از شرم عدم‌ کز هستی بیحاصلم</p></div>
<div class="m2"><p>آرمیدن‌کوشش و بیمطلبی ابرام‌کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شعله‌ای بودم‌کنون خاکسترم مفت طلب</p></div>
<div class="m2"><p>سوختن عریانی‌ام راجامهٔ احرام کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در پریشانی کشیدیم انتقام از روزگار</p></div>
<div class="m2"><p>خاک ما باری طواف دیدهٔ ایام کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قرب هم در خلوت تحقیق‌گنجایش نداشت</p></div>
<div class="m2"><p>دوربین افتاد شوق و وصل را پیغام‌ کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از تعلق سنگسار شهرت آزادی‌ام</p></div>
<div class="m2"><p>الفت نقش نگین آخر ستم بر نام کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اینقدر در بند خویش از ناتوانی مانده‌ایم</p></div>
<div class="m2"><p>عشق رنگ ما شکست و اختراع دام کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل به یاد مستی چشم حجاب‌آلوده‌ای</p></div>
<div class="m2"><p>آب‌گردید از حیا چندانکه می در جام‌ کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جادهٔ سرمنزل ما صد بیابان سعی داشت</p></div>
<div class="m2"><p>بیدماغیهای فرصت چون شرر یک گام کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عشرت‌ما چون نگه از بس تنک‌سرمایه است</p></div>
<div class="m2"><p>سایهٔ مژگان تواند صبح ما را شام‌ کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>می‌رود صبح و اشارت می‌کندکای غافلان</p></div>
<div class="m2"><p>تا نفس باقیست نتوان هیچ جا آرام‌کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یک قلم بیدل غبار وحشت نظاره‌ایم</p></div>
<div class="m2"><p>عشق نتوانست ما را بی‌تحیر رام‌ کرد</p></div></div>