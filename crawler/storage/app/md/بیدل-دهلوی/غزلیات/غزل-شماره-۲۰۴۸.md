---
title: >-
    غزل شمارهٔ ۲۰۴۸
---
# غزل شمارهٔ ۲۰۴۸

<div class="b" id="bn1"><div class="m1"><p>چشمش افکنده طرح بیدادم</p></div>
<div class="m2"><p>سرمه ‌کو تا رسد به فریادم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرو تهمت قفس چه چاره ‌کند</p></div>
<div class="m2"><p>پا به ‌گل کرده‌اند آزادم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبنم انفعال خاصیتم</p></div>
<div class="m2"><p>همه آب است و خاک بنیادم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از فسون نفس مگوی و مپرس</p></div>
<div class="m2"><p>خاک نا گشته می‌برد بادم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درد عشق امتحان راحت داشت</p></div>
<div class="m2"><p>همچو آتش به بستر افتادم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلش آزادی‌ام نمی‌خواهد</p></div>
<div class="m2"><p>قفس است آرزوی صیادم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>او دلم داد تا به خود نگرم</p></div>
<div class="m2"><p>من هم آیینه در کفش دادم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خالی‌ام از خود و پر از یادش</p></div>
<div class="m2"><p>شیشهٔ مجلس پری زادم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی‌دماغانه نشکند چه کند</p></div>
<div class="m2"><p>شیشه می‌خواست دل فرستادم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نفسی هست جان ‌کنی مفت است</p></div>
<div class="m2"><p>تیشه دارم هنوز فرهادم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نظم و نثری ‌که می کنم ‌تحریر</p></div>
<div class="m2"><p>به‌ که در زندگی ‌کند شادم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ورنه حیفست نقشم از پس مرگ</p></div>
<div class="m2"><p>گل زند بر مزار بهزادم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این زمان هرچه دارم از من نیست</p></div>
<div class="m2"><p>داشتم آنچه رفت از یادم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیستی هم به داد من نرسید</p></div>
<div class="m2"><p>مرگ مرد آن زمان ‌که من زادم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یأس من امتحان نمی‌خواهد</p></div>
<div class="m2"><p>بیدلم عبرت خدا دادم</p></div></div>