---
title: >-
    غزل شمارهٔ ۱۰۵۶
---
# غزل شمارهٔ ۱۰۵۶

<div class="b" id="bn1"><div class="m1"><p>نگاهت جوش صد میخانه از ساغر برون آرد</p></div>
<div class="m2"><p>تبسم شور چندین محشر از کوثر برون آرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز ریحان خطت بالد بهار سبزهٔ جنّت</p></div>
<div class="m2"><p>وز آن زلف‌ دو تا روح‌الامین شهپر برون آرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به گلشن گر ز پا افتد غبار راه جولانت</p></div>
<div class="m2"><p>بهار از غنچه و گل بالش و بستر برون آرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لبت در خنده‌ گوهر ریزد از آغوش برگ گل</p></div>
<div class="m2"><p>رخت‌گاه عرق از آفتاب اختر برون آرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رم دیوانهٔ شوق تو گر جولان دهد گردی</p></div>
<div class="m2"><p>به چندین‌ گردباد آه از دل محشر برون آرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرفتم بی‌نقابی رخصت نظّاره است اینجا</p></div>
<div class="m2"><p>نگاهی‌کو که مژگان‌واری از خود، سر برون آرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فسون نوخطیهای لبت بر سنگ اگر خوانم</p></div>
<div class="m2"><p>گداز حسرتش صد آینه جوهر برون آرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نمی‌ارزد به رنگ خوش عیار چهرهٔ عاشق</p></div>
<div class="m2"><p>خزان از بوته‌های گل گرفتم زر برون آرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همان پیرایهٔ وهم است اگر کامل شود زاهد</p></div>
<div class="m2"><p>هیولا چون در سامان زند پیکر برون آرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کهن شد سیر این گلشن کنون فال تحیر زن</p></div>
<div class="m2"><p>مگر آیینه گردیدن ‌گل دیگر برون آرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در این دریا، طلب آیینهٔ مطلوب می‌باشد</p></div>
<div class="m2"><p>گره سازد نفس‌، غواص‌، تاگوهر برون آرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قفس فرسودهٔ‌ گرد هوسهایم خوشا روزی</p></div>
<div class="m2"><p>که پروازم چو بوی ‌گل ز بال و پر برون آرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر صد بار آید موج تیغش بر سرم بیدل</p></div>
<div class="m2"><p>حباب من ز جیب دل سر دیگر برون آرد</p></div></div>