---
title: >-
    غزل شمارهٔ ۱۰۰۸
---
# غزل شمارهٔ ۱۰۰۸

<div class="b" id="bn1"><div class="m1"><p>کام دل از لب خاموش گرفتن دارد</p></div>
<div class="m2"><p>نشئه‌ای زین می بی جوش گرفتن دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا نوا های جهان ساز کدورت نشود</p></div>
<div class="m2"><p>چون کری رهگذر گوش گرفتن دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست دیوانه ز کیفیت صحرا غافل</p></div>
<div class="m2"><p>از جنون هم سبق هوش ‌گرفتن دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاهدا کس ز سبوی می ‌ات آگاه نکرد</p></div>
<div class="m2"><p>این صوابی ‌ست‌ که بر دوش گرفتن دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوب و زشت آنچه در این بزم درّد طرف نقاب</p></div>
<div class="m2"><p>همچو آیینه در آغوش ‌گرفتن دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر نگه دیده به توفان دگر می‌جوشد</p></div>
<div class="m2"><p>سر این چشمه‌ خس‌ پوش گرفتن دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فیض آزادی اگر پرده گشاید چون صبح</p></div>
<div class="m2"><p>یک دمیدن به صد آغوش‌ گرفتن دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درد دل صور قیامت شد و نشنید کسی</p></div>
<div class="m2"><p>پیش این بیخبران‌ گوش گرفتن دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مفت فرصت اگر آگه شوی از ساز نفس</p></div>
<div class="m2"><p>این رگ خواب فراموش گرفتن دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در دل غنچه ز اسرار چمن بویی هست</p></div>
<div class="m2"><p>خبر از مردم خاموش گرفتن دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چشم تا باز نمائی مژه ‌ها رو به قفاست</p></div>
<div class="m2"><p>خبر امشبت از دوش‌ گرفتن دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به سخن قانعم از نعمت الوان بیدل</p></div>
<div class="m2"><p>رزق خود چون صدف از گوش ‌گرفتن دارد</p></div></div>