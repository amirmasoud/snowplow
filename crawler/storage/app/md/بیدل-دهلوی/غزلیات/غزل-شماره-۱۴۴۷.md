---
title: >-
    غزل شمارهٔ ۱۴۴۷
---
# غزل شمارهٔ ۱۴۴۷

<div class="b" id="bn1"><div class="m1"><p>بر اهل فضل دانش و فن‌گریه می‌کند</p></div>
<div class="m2"><p>تا خامه لب گشود سخن گریه می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پر بیکسیم ‌کز نم چشم مسامها</p></div>
<div class="m2"><p>هرچند مو دمد ز بدن ‌گریه می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درپیری ازتلاش سخن ضبط لب‌کنید</p></div>
<div class="m2"><p>دندان دمی که ریخت دهن گریه می‌ کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عقل از فسون نفس ندارد برآمدن</p></div>
<div class="m2"><p>بیچاره است مرد چون زن گریه می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اشکی‌ که مهر پروردش در کنار چشم</p></div>
<div class="m2"><p>چون طفل بر زمین مفکن‌ گریه می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای قطره غفلت از نم چشم محیط چند</p></div>
<div class="m2"><p>از درد غربت تو وطن‌ گریه می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تیمار جسم چند عرق ریز انفعال</p></div>
<div class="m2"><p>تعمیر بر بنای کهن گریه می‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هنگامهٔ چه عیش فروزم‌که همچو شمع</p></div>
<div class="m2"><p>گل نیز بی‌ تو بر سر من ‌گریه می‌ کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شبنم درین بهار دلیل نشاط نیست</p></div>
<div class="m2"><p>صبحی‌ست‌ کز وداع چمن‌ گریه می‌کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل به هرکجا رگ ابری نشان دهند</p></div>
<div class="m2"><p>در ماتم حسین و حسن‌گریه می‌کند</p></div></div>