---
title: >-
    غزل شمارهٔ ۲۷۹۷
---
# غزل شمارهٔ ۲۷۹۷

<div class="b" id="bn1"><div class="m1"><p>اگر جانی وگر جسمی سراب مطلب مایی</p></div>
<div class="m2"><p>به هر جا جلوه‌ گر کردی همان جز دور ننمایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه لفظ آیینهٔ انشا، نه‌معنی قابل ایما</p></div>
<div class="m2"><p>به این سازست پنهانی‌، به این رنگست پیدایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهار وحدت است اینجا دویی صورت نمی‌بندد</p></div>
<div class="m2"><p>خیال آیینه‌ دارد لیک بر روی تماشایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به سامان نگاهت جلوه آغوش اثر دارد</p></div>
<div class="m2"><p>دو عالم سر بهم سوده‌ست تا مژگان بهم سایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلی خون کردم و در آب دیدم نقش امکان را</p></div>
<div class="m2"><p>گداز قطرهٔ من عالمی را کرد دریایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هجوم‌گریه برد از جا دل دیوانهٔ ما را</p></div>
<div class="m2"><p>به آب از سنگ سودا محو شد تمکین خارایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهارستان شوق بی‌نیازی رنگ ها دارد</p></div>
<div class="m2"><p>گلی مست خود آرایی‌ست یعنی عالم آرایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به وهم غیر ممکن نیست انداز برون جستن</p></div>
<div class="m2"><p>چوگردون شش جهت آغوش واکرده‌ست یکتایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قصور و حور گو آنسوی وهم آیینه بردارد</p></div>
<div class="m2"><p>زمان فرصت آگاهان وصلت نیست فردایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بنازم نشئهٔ یکرنگی جام محبت را</p></div>
<div class="m2"><p>دل از خود رفتنی دارد که پندارم تو می‌آیی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هزار آیینه حیرت در قفس‌کرده‌ست طاووست</p></div>
<div class="m2"><p>جهانی چشم بگشاید تو گر یک بال بگشایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز تحریک نفس عمری‌ست بیدل در نظر دارم</p></div>
<div class="m2"><p>پر پروانهٔ چندی جنون پرواز عنقایی</p></div></div>