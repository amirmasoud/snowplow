---
title: >-
    غزل شمارهٔ ۱۲۷۷
---
# غزل شمارهٔ ۱۲۷۷

<div class="b" id="bn1"><div class="m1"><p>دل باز به جوش یارب آمد</p></div>
<div class="m2"><p>شب‌ رفت و سحرنشد شب‌ آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اشک از مژه بسکه بی‌اثر پخت</p></div>
<div class="m2"><p>رحمم به زوال‌ کوکب آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی‌ روی تو یاد خلد کردم</p></div>
<div class="m2"><p>مرگی به عیادت تب آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شرمندهٔ رسم انتظارم</p></div>
<div class="m2"><p>جانی‌ که نبود بر لب آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مستان خبریست در خط جام</p></div>
<div class="m2"><p>قاصد ز دیار مشرب آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وضع عقلای عصر دیدم</p></div>
<div class="m2"><p>دیوانهٔ ما مؤدب آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از اهل دول حیا مجویید</p></div>
<div class="m2"><p>اخلاق کجاست‌، منصب آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از رفتن آبرو خبر گیر</p></div>
<div class="m2"><p>هرجا اظهار مطلب آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتم چو سخن‌، رسم‌ به‌ گوشی</p></div>
<div class="m2"><p>هرگام به پیش من لب آمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>راجت در کسب نیستی بود</p></div>
<div class="m2"><p>از هر عمل این مجرب آمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل نشدم دچار تحقیق</p></div>
<div class="m2"><p>آیینه به دست من شب آمد</p></div></div>