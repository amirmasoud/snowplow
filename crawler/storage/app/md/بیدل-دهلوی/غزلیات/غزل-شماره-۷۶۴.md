---
title: >-
    غزل شمارهٔ ۷۶۴
---
# غزل شمارهٔ ۷۶۴

<div class="b" id="bn1"><div class="m1"><p>آستان عشق جولانگاه هر بیباک نیست</p></div>
<div class="m2"><p>هیچکس‌غیر از جبین‌آنجا قدم‌بر خاک نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گریه‌کو، تا عذر غفلت خواهد از ابرکرم</p></div>
<div class="m2"><p>می‌کشد رحمت‌تری‌تا چشم ما نمناک نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک می‌باید شدن در معبد تسلیم عشق</p></div>
<div class="m2"><p>گر همه آب است اینجا بی‌تیمم پاک نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ریش‌گاوی‌، شرمی ای زاهد ز دندان طمع</p></div>
<div class="m2"><p>شاخ طوبی ریشه‌دار شانه و مسواک نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گردن تسلیم در هر عضو ما آماده است</p></div>
<div class="m2"><p>شمع این کاشانه را از سر بریدن باک نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تهمت وضع تظلم برجنون ما خطاست</p></div>
<div class="m2"><p>صبح پوشیده‌ست عریانی‌گریبان‌چاک نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرکز پرگار اسراری‌، به ضبط خویش کوش</p></div>
<div class="m2"><p>ورنه تا گردید رنگت گردش افلاک نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشم بر احسان‌گردون دوختن دیوانگی‌ست</p></div>
<div class="m2"><p>دانه‌ها، هشیار باشید، آسیا دلاک نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کامجویان‌! دست در دامان نومیدی زنید</p></div>
<div class="m2"><p>صید ما صدسال‌اگر در خون‌تپد فتراک نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غیر مستی هرچه دارد این چمن دردسر است</p></div>
<div class="m2"><p>خواب‌راحت جز به زیر سایه‌های تاک نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با که بایدگفت بیدل ماجرای آرزو</p></div>
<div class="m2"><p>آنچه دلخواه من است از عالم ادراک نیست</p></div></div>