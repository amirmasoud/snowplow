---
title: >-
    غزل شمارهٔ ۱۴۹۴
---
# غزل شمارهٔ ۱۴۹۴

<div class="b" id="bn1"><div class="m1"><p>روزی که بی تو دامن ضعفم به چنگ بود</p></div>
<div class="m2"><p>عکسم ز آب آینه در زیر زنگ بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون لاله زین بهار نچیدیم غیر داغ</p></div>
<div class="m2"><p>آیینه‌داری نفس اظهار رنگ بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پروازها به زیر فلک محو بال ماند</p></div>
<div class="m2"><p>گردی نشد بلند ز بس عرصه تنگ بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوس کفش تبسم صبح امید کیست</p></div>
<div class="m2"><p>اینجا همین بهار حنا گل به چنگ بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در عالمی ‌که بیخبر از خود گذشتن است</p></div>
<div class="m2"><p>اندیشهٔ شتاب طلسم درنگ بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبری مگر تلافی آزار ما کند</p></div>
<div class="m2"><p>مینا شکسته ‌آنچه ‌به دل بست سنگ بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زنجیر ما چو زلف بتان ماند بی‌صدا</p></div>
<div class="m2"><p>از بس غبار دشت جنون سرمه رنگ بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حیرت ‌کفیل یکمژه تمهید خواب نیست</p></div>
<div class="m2"><p>آینه داغ سایهٔ دیوار زنگ بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آهی نکرد گل ‌که دمی از خودم نبرد</p></div>
<div class="m2"><p>رنگ شکسته‌ام پر چندین خدنگ بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل به‌ جیب خویش فرو برد حیرتم</p></div>
<div class="m2"><p>چشم به هم نیامده ‌کام نهنگ بود</p></div></div>