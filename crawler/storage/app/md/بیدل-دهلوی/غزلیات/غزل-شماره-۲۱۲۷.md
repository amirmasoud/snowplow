---
title: >-
    غزل شمارهٔ ۲۱۲۷
---
# غزل شمارهٔ ۲۱۲۷

<div class="b" id="bn1"><div class="m1"><p>بس که در هجر تو فرسود از ضعیفی پیکرم</p></div>
<div class="m2"><p>می‌توان از موی چینی سایه ‌کردن بر سرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد عدم از جلوه زار هستی آن سو می‌پرم</p></div>
<div class="m2"><p>گر پری از شیشه بیرونست من بیرون‌ترم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مستی حیرت خروشم آنقدر بی پرده نیست</p></div>
<div class="m2"><p>موج می دارد رگ خوابی به چشم ساغرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جوهر آیینه در مژگان نگه می‌پرورد</p></div>
<div class="m2"><p>حیرتی دارم‌ که توفان جنون را لنگرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون سپندم آرزوها به که در دل خون شود</p></div>
<div class="m2"><p>ورنه تا پر می‌فشاند ناله من خاکسترم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچکس آیینه‌دار ناتوانیها مباد</p></div>
<div class="m2"><p>انفعال شخص پیدایی‌ست جسم لاغرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هستی من بر عدم می‌چربد از بی‌حاصلی</p></div>
<div class="m2"><p>خاک را تر کرد خشکیهای آب گوهرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کس ندارد زین چمن سامان یک شبنم تمیز</p></div>
<div class="m2"><p>چون بهار از رنگ هر گل صد گریبان می‌درم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاک من صد درد دل توفان غبار تنگی است</p></div>
<div class="m2"><p>حسرت بیمار عشقم ناله دارد بسترم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>واعظ هنگامهٔ این عبرت آبادم چو صبح</p></div>
<div class="m2"><p>زخم دل تا چرخ دارد نردبان منبرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کاش بیدل پیش از آهنگ غرور خودسری</p></div>
<div class="m2"><p>خجلت پرواز چون ابر از عرق ریزد پرم</p></div></div>