---
title: >-
    غزل شمارهٔ ۲۰۳۶
---
# غزل شمارهٔ ۲۰۳۶

<div class="b" id="bn1"><div class="m1"><p>تحیر مطلعی سرزد چو صبح‌ از خویشتن رفتم</p></div>
<div class="m2"><p>نمی‌دانم‌ که آمد در خیال من‌ که من رفتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صدای ساغر الفت جنون کیفیت‌ست اینجا</p></div>
<div class="m2"><p>لب او تا به حرف آمد من از خود چون سخن رفتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبم بر بستر گل یاد او گرداند پهلویی</p></div>
<div class="m2"><p>تپیدم آنقدر بر خود که بیرون از چمن رفتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بزم او چه امکانست چون شمعم برون رفتن</p></div>
<div class="m2"><p>اگر از خویش هم رفتم به دوش سوختن رفتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برون لفظ ممکن نیست سیر عالم معنی</p></div>
<div class="m2"><p>به عریانی رسیدم تا درون پیرهن رفتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تمیز وحدتم از گرد کثرت بر نمی‌آرد</p></div>
<div class="m2"><p>به خلوت هم همان پنداشتم در انجمن رفتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درین گلشن که سیر رنگ و بوی خودسری دارد</p></div>
<div class="m2"><p>جهانی آمد اما من ز یاد آمدن رفتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ندارم جز فضولیهای راحت داغ محرومی</p></div>
<div class="m2"><p>به خاک تیره چون شمع از مژه بر هم زدن رفتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به قدر لاف هستی بود سامان فنا اینجا</p></div>
<div class="m2"><p>نفس یک عمر بر هم یافتم تا در کفن رفتم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به اثباتش جگر خوردم به نفی خود دل افشردم</p></div>
<div class="m2"><p>ز معنی چون اثر بردم نه او آمد نه من رفتم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو گردون عمرها شد بال وحشت می‌زنم بیدل</p></div>
<div class="m2"><p>نرفتم آخر از خود هر قدر از خویشتن رفتم</p></div></div>