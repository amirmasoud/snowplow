---
title: >-
    غزل شمارهٔ ۲۲۴۶
---
# غزل شمارهٔ ۲۲۴۶

<div class="b" id="bn1"><div class="m1"><p>ز بس صرف ادب پیمایی عجز است احوالم</p></div>
<div class="m2"><p>به رنگ خامه لغزشهای مژگان ‌کرده پامالم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کف خاکم‌، غبار است آبروی دستگاه من</p></div>
<div class="m2"><p>به توفان می‌روم تا گل ‌کند آثار اقبالم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نظرها محرم نشو و نمای من نمی‌باشد</p></div>
<div class="m2"><p>نهال ناله‌ام آن سوی عرض رنگ می‌بالم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همان بهتر که پیش از خاک‌گشتن بی‌نشان باشم</p></div>
<div class="m2"><p>دماغ شهرت عنقا ندارد ریزش بالم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به رنگی آب می‌گردم ز شرم خودنماییها</p></div>
<div class="m2"><p>که سیلابی‌ کند در خانهٔ آیینه تمثالم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو گل تا زبن چمن دوری به ‌کام ساغرم خندد</p></div>
<div class="m2"><p>به زیر خاک باید رنگها گرداند یک سالم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلی ‌کو تا به ‌درد آید ز عجز مدعای من</p></div>
<div class="m2"><p>نفس شور قیامت می‌کند انشا و من لالم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز اوضاعم چه می‌پرسی ز اطوارم چه می‌خواهی</p></div>
<div class="m2"><p>به حسرت می‌تپم جان می‌کنم این است اعمالم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز تأثیر فسونهای محبت نیستم غافل</p></div>
<div class="m2"><p>به‌ گوشم می‌رسد آ‌وازها چندانکه می‌نالم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شرار کاغذم عمریست بال افشاند و عنقا شد</p></div>
<div class="m2"><p>تمنا همچنان پرواز می‌بیزد به غربالم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز سازم چون نفس غیر از تپش صورت نمی‌بندد</p></div>
<div class="m2"><p>چه امکان دارد آسودن دل افتاد‌ست دنبالم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ندامت توأم آگاهی‌ام گل می‌کند بیدل</p></div>
<div class="m2"><p>چو مژگان دست بر هم سوده‌ام تا چشم می‌مالم</p></div></div>