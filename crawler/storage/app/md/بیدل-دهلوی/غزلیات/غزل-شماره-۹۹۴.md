---
title: >-
    غزل شمارهٔ ۹۹۴
---
# غزل شمارهٔ ۹۹۴

<div class="b" id="bn1"><div class="m1"><p>بی‌نمک از نمک غیر توهم دارد</p></div>
<div class="m2"><p>لب بام است ‌که اظهار تکلم دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جای اشک از مژهٔ تیغ حیا جوهر ریخت</p></div>
<div class="m2"><p>چقدر حسرت زخم تو تبسم دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی‌تو اظهار اثر خجلت معدومی ماست</p></div>
<div class="m2"><p>قطرهٔ دور ز دریا چه تلاطم دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاهد از گنبد دستار به خود می‌نازد</p></div>
<div class="m2"><p>نکنی عیب که خر فخر به توقم دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر به دادت نرسد شور قیامت ستم است</p></div>
<div class="m2"><p>درد هستی است‌ که فریاد تظلم دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فیض خورشید به عالم ز کواکب نرسد</p></div>
<div class="m2"><p>شیشهٔ تنگ کجا حوصلهٔ خم دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مفت غواص تامل‌گهرمعنی بکر</p></div>
<div class="m2"><p>دفتر بیدل ما خصلت قلزم دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیدل از فیض قناعت چمن عافیت است</p></div>
<div class="m2"><p>تکیه عمری‌ست‌که بر بستر قاقم دارد</p></div></div>