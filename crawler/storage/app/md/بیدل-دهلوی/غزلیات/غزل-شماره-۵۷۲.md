---
title: >-
    غزل شمارهٔ ۵۷۲
---
# غزل شمارهٔ ۵۷۲

<div class="b" id="bn1"><div class="m1"><p>الفت دل عمرها شد دست وپایم بسته است</p></div>
<div class="m2"><p>قطرهٔ خونی ز سرتا پا حنایم بسته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آرزو نگذشت حیف از قلزم نیرنگ حرص</p></div>
<div class="m2"><p>ورنه عمری‌شد پلش دست دعایم بسته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو صحرا با همه عریانی وآزادگی</p></div>
<div class="m2"><p>نقد چندین‌گنج درگنج ردایم بسته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رفته‌ام‌زین‌انجمن چون‌شمع‌و داغ‌دل بجاست</p></div>
<div class="m2"><p>حسرت دیدار چشمی بر قفایم بسته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عبرتم محمل‌کش صد آبله واماندگی</p></div>
<div class="m2"><p>هرکه رفتاری ندارد پا به پایم بسته است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زیر‌گردون برکدامین آرزو نازدکسی</p></div>
<div class="m2"><p>تنگی این خانه درها بر هوایم بسته است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کاش ابرامی درین محمل به فریادم رسد</p></div>
<div class="m2"><p>بی‌زبانیها در رزق گدایم بسته است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کو عرق تا تکمه‌ای چند ازگریبان واکنم</p></div>
<div class="m2"><p>خجلت عریان تنی بند قبایم بسته است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>الرحیل زندگی دیگرکه برگوشم زند</p></div>
<div class="m2"><p>موی پیری پنبه بر ساز درایم بسته است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>معنی موج‌گهر از حیرتم فهمیدنی‌ست</p></div>
<div class="m2"><p>رفته‌ام از خویش‌ ویادت دل به جایم بسته است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مصرع فکربلند بیدلم‌اما چه سود</p></div>
<div class="m2"><p>بی‌دماغیهای فرصت نارسایم بسته است</p></div></div>