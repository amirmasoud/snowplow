---
title: >-
    غزل شمارهٔ ۱۲۸۵
---
# غزل شمارهٔ ۱۲۸۵

<div class="b" id="bn1"><div class="m1"><p>گل به سر، جام به کف‌، آن چمن‌ آیین آمد</p></div>
<div class="m2"><p>میکشان مژده‌، بهار آمد و رنگین آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طبعم از دست زبان‌سوز تبی داشت چو شمع</p></div>
<div class="m2"><p>عاقبت خامشی‌ام بر سر بالین آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نخل‌ گلزار محبت ثمر عیش نداد</p></div>
<div class="m2"><p>مصرع آه همان یأس مضامین آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حیرتم بی‌اثر از انجمن عالم رنگ</p></div>
<div class="m2"><p>همچو آیینه ز صورتکدهٔ چین آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حاصل این چمن از سودن دستم‌ گل‌ کرد</p></div>
<div class="m2"><p>به‌ کف از آبله‌ام دامن‌ گلچین آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچکس از غم اسباب نیامد بیرون</p></div>
<div class="m2"><p>بار نابستهٔ این قافله سنگین آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه خیالست سر از خواب گران برداریم</p></div>
<div class="m2"><p>پهلوی ما چو گهر در ته‌‌ی بالین آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون نفس سر به خط وحشت دل می‌تازیم</p></div>
<div class="m2"><p>جاده در دامن این دشت همان چین آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باز بی‌روی تو در فصل جنون جوش بهار</p></div>
<div class="m2"><p>سایهٔ گل به سرم پنجهٔ شاهین آمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خون به‌ دل‌، خاک‌ به‌ سر، آه به‌ لب‌، اشک به چشم</p></div>
<div class="m2"><p>بی‌جمال تو چه‌ها بر من مسکین آمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل آسوده‌تر از موج گهر خاک شدیم</p></div>
<div class="m2"><p>رفتن از خویش چه مقدار به تمکین آمد</p></div></div>