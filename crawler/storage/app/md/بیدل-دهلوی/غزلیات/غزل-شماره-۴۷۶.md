---
title: >-
    غزل شمارهٔ ۴۷۶
---
# غزل شمارهٔ ۴۷۶

<div class="b" id="bn1"><div class="m1"><p>اشک یک لحظه به مژگان بار است</p></div>
<div class="m2"><p>فرصت عمر همین مقدار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زندگی عالم آسایش نیست</p></div>
<div class="m2"><p>نفس آیینهٔ این اسرار است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسکه‌گرم است هوای گلشن</p></div>
<div class="m2"><p>غنچه اینجا سر بی‌دستار است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شیشه‌ساز نم اشکی نشوی</p></div>
<div class="m2"><p>عالم از سنگدلان‌، کهسار است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خشت داغی‌ست عمارتگر دل</p></div>
<div class="m2"><p>خانهٔ آینه یک دیوار است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میکشی سرمهٔ عرفان نشود</p></div>
<div class="m2"><p>بینش از چشم قدح دشوار است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو آیینه اگر صاف شوی</p></div>
<div class="m2"><p>همه جا انجمن دیدار است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گوش‌کو تا شود آیینهٔ راز</p></div>
<div class="m2"><p>نالهٔ ما نفس بیمار است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دردگل‌کرد زکفر و دین شد</p></div>
<div class="m2"><p>سبحه اشک مژه‌، زنار است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست گرداب‌صفت آرامم</p></div>
<div class="m2"><p>سرنوشتم به خط پرگار است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از نزاکت سخنم نیست بلند</p></div>
<div class="m2"><p>از صدا ساغرگل را عار است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غافل از عجز نگه نتوان بود</p></div>
<div class="m2"><p>آسمانها گره این تار است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نکشد شعله سر از خاکستر</p></div>
<div class="m2"><p>نفس سوختگان هموار است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیدل از زخم بود رونق دل</p></div>
<div class="m2"><p>خندهٔ‌گل نمک گلزار است</p></div></div>