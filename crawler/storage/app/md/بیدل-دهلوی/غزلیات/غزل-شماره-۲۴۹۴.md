---
title: >-
    غزل شمارهٔ ۲۴۹۴
---
# غزل شمارهٔ ۲۴۹۴

<div class="b" id="bn1"><div class="m1"><p>قد خم‌ گشته را تا می‌توانی وقف طاعت ‌کن</p></div>
<div class="m2"><p>به این قلاب صید ماهی دریای رحمت‌ کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه‌ای‌ گردن ‌که همچون ‌شعله باید سر کشت بودن</p></div>
<div class="m2"><p>تو با خود جبهه‌ای آورده‌ای ساز عبادت کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به‌ رنگ موج تا کی پیش پای یکدگر خوردن</p></div>
<div class="m2"><p>به فرش آبروی خویش یک‌ گوهر فراغت‌ کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تماشا وحشت آهنگست ای آیینه تدبیری</p></div>
<div class="m2"><p>به پیچ و تاب جوهر چاره‌پردازیی حیرت‌ کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز دستت هر چه آید مفت قدرتهای موهومی</p></div>
<div class="m2"><p>دماغ جهد صرف قدردانیهای فرصت‌ کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درین محفل سپندی نیست شوری برنینگیزد</p></div>
<div class="m2"><p>تو هم ای بیخبر با خود دلی داری قیامت ‌کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دماغ‌ گلشنت‌ گر نیست سیر نرگسستانی</p></div>
<div class="m2"><p>زگل قطع نظر بیمار چندی را عیادت‌کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به چینی از اشارت آب ده انداز ابرویی</p></div>
<div class="m2"><p>مه نو را به‌گردون موج دریای خجالت‌کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گذشتن از جهان پوچ دارد ننگ استغنا</p></div>
<div class="m2"><p>همینت‌گر بود معراج همت ترک همت‌کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز مینا خانهٔ‌ گردون اگر نتوان برون جستن</p></div>
<div class="m2"><p>تهی شو از خیال و طاق نسیانی عمارت‌کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کس از باغ طمع بیدل ندارد حاصل عزت</p></div>
<div class="m2"><p>چو شبنم زین چمن با سیر چشمیها قناعت‌کن</p></div></div>