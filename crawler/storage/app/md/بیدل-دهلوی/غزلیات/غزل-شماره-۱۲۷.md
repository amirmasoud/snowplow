---
title: >-
    غزل شمارهٔ ۱۲۷
---
# غزل شمارهٔ ۱۲۷

<div class="b" id="bn1"><div class="m1"><p>وهم راحت صید الفت‌کرد مجنون مرا</p></div>
<div class="m2"><p>مشق تمکین لفظ‌گردانید مضمون مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گریه توفان‌کرد چندانی‌که دل هم آب شد</p></div>
<div class="m2"><p>موج سیل آخر به دریا برد هامون مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داده‌ام ازکف عنان و سخت حیرانم‌که باز</p></div>
<div class="m2"><p>ناکجا راند محبت اشک‌گلگون مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین عبارتهاکه حیرت صفحهٔ تحریر اوست</p></div>
<div class="m2"><p>گر نفهمی می‌توان فهمید مضمون مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناخن تدبیر را بر عقدگوهر دست نیست</p></div>
<div class="m2"><p>موج می مشکل‌گشاید طبع محزون مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون‌شرر روزو شبم‌کرد رم کم‌فرضیی‌است</p></div>
<div class="m2"><p>گردشی در عالم رنگ است گردون مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل هم از مضمون اسرارم عبارت‌ساز ماند</p></div>
<div class="m2"><p>آینه ننمود الا نقش بیرون مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکقدم‌وارم‌چواشک‌ازخودروانی‌مشکل‌است</p></div>
<div class="m2"><p>ای تپیدن‌گر توانی آب کن خون مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زیردست التفات چتر شاهی نیستم</p></div>
<div class="m2"><p>موی سر در سایه پرورده است مجنون مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا فلک یک مدّ آهم نارسا آهنگ نیست</p></div>
<div class="m2"><p>سکته معدوم‌است مصرعهای‌موزون مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تارگیسو نیست بیدل رشتهٔ تسخیر من</p></div>
<div class="m2"><p>از زبان‌ مار باید جست فسون مرا</p></div></div>