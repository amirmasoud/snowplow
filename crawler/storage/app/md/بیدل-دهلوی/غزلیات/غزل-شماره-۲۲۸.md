---
title: >-
    غزل شمارهٔ ۲۲۸
---
# غزل شمارهٔ ۲۲۸

<div class="b" id="bn1"><div class="m1"><p>عمری‌ست ناز دیدهٔ تر می‌کشیم ما</p></div>
<div class="m2"><p>از اشک‌، انتظارگهر می‌کشیم ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تسخیرحسن درخور حیرت‌نگاهی است</p></div>
<div class="m2"><p>صید عجب به دام نظرمی‌کشیم ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دامن‌کشان ز ناز به هر سوگذرکنی</p></div>
<div class="m2"><p>چون سایه زیرپای توسرمی‌کشیم ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خلق اگرکناره‌گرفتیم مفت ماست</p></div>
<div class="m2"><p>کشتی زچارموج خطرمی‌کشیم ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پروازما سری نکشید ازشکست بال</p></div>
<div class="m2"><p>امروزناله هم ته پر می‌کشیم ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای چرخ پاس آه دل خسته لازم است</p></div>
<div class="m2"><p>این رشته را ز پای‌گوهر می‌کشیم ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عمری‌ست درادبکدهٔ وضع خامشی</p></div>
<div class="m2"><p>از ناله انتقام اثر می‌کشیم ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمع خموش انجمن داغ حیرتیم</p></div>
<div class="m2"><p>خمیازهٔ خمار نظر می‌کشیم ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>داغ سپهر مرهم کافور می‌برد</p></div>
<div class="m2"><p>زین آه‌کزجگر چوسحرمی‌کشیم ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همچون‌نفس بنای‌جهان برتردداست</p></div>
<div class="m2"><p>درمنزلیم ورنج سفرمی‌کشیم ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فرصت‌کفیل این‌همه شوخی نمی‌شود</p></div>
<div class="m2"><p>آیینه‌ای به روی شرر می‌کشیم ما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل به جرم آنکه چو آیینه ساده‌ایم</p></div>
<div class="m2"><p>خاکسترست آنچه به بر می‌کشیم ما</p></div></div>