---
title: >-
    غزل شمارهٔ ۲۸۲۷
---
# غزل شمارهٔ ۲۸۲۷

<div class="b" id="bn1"><div class="m1"><p>چو محو عشق شدی رهنما چه می‌جویی</p></div>
<div class="m2"><p>به بحر غوطه زدی ناخدا چه می‌جویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>متاع خانه آیینه حیرت است اینجا</p></div>
<div class="m2"><p>تو دیگر از دل بیمدعا چه می‌جویی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عصا ز دست تو انگشت رهنما دارد</p></div>
<div class="m2"><p>توگرنه‌کوردلی از عصا چه می‌جویی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز این‌که خرد کند حرص استخوان ترا</p></div>
<div class="m2"><p>دگر ز سایهٔ بال هما چه می‌جویی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به سینه تانفسی هست‌دل پریشان است</p></div>
<div class="m2"><p>رفوی جیب سحر از هوا چه می‌جویی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر نیاز ضعیفان غرور سامان نیست</p></div>
<div class="m2"><p>به غیر سجده ز مشتی گیا چه می‌جویی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صفای دل نپسندد غبار آرایش</p></div>
<div class="m2"><p>به دست آینه رنگ حنا چه می‌جویی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز حرص‌، دیدهٔ احباب حلقهٔ دام است</p></div>
<div class="m2"><p>نم مروت ازین چشمها چه می‌جویی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو شمع خاک شدم در سراغ خویش اما</p></div>
<div class="m2"><p>کسی نگفت‌ که در زیر پا چه می‌جویی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز آفتاب طلب شبنم هوا شده‌ایم</p></div>
<div class="m2"><p>دل رمیدهٔ ما را زما چه می‌جویی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بجز غبار ندارد تپیدن نفست</p></div>
<div class="m2"><p>ز تار سوخته بیدل صدا چه می‌جویی</p></div></div>