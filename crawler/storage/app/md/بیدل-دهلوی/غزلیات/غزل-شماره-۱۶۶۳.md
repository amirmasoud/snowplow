---
title: >-
    غزل شمارهٔ ۱۶۶۳
---
# غزل شمارهٔ ۱۶۶۳

<div class="b" id="bn1"><div class="m1"><p>تا کنم از هر بن مو رنگ هستی آشکار</p></div>
<div class="m2"><p>جام می‌خواهم در این میخانه یک طاووس‌دار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوختن می‌بالد آخر از کف افسوس من</p></div>
<div class="m2"><p>دامنی بر آتش خود می‌زند برگ چنار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تیره‌بختی چون سیاهی ناله‌ام را زیر کرد</p></div>
<div class="m2"><p>سوخت آخر همچو سنگ سرمه در طبعم شرار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آهم از خاکستر دل سرمه‌آلود حیاست</p></div>
<div class="m2"><p>نالهٔ خاموش داغم چون نسیم لاله‌زار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سعی بیتابم‌ کمند جذبهٔ آسودگی‌ست</p></div>
<div class="m2"><p>از تپیدن می‌رسد هر جزو دریا درکنار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آتش رنگی که دارد این چمن بی‌دود نیست</p></div>
<div class="m2"><p>آب می‌گردد به چشم شبنم از بوی بهار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای که هوشت نغمه از بال و پری وامی‌کشد</p></div>
<div class="m2"><p>بر شکست شیشهٔ ما هم زمانی ‌گوش دار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیده‌ها در جلوه‌کاهت زخمی خمیازه‌اند</p></div>
<div class="m2"><p>بادهٔ جام تحیر نیست جز رنگ خمار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عمرها شد در خیال آفتاب و آینه</p></div>
<div class="m2"><p>سایه‌وار از الفت زنگار می‌دزدم کنار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با تن‌آسانی ز ما کم فرصتان نتوان گذشت</p></div>
<div class="m2"><p>برق هم دارد حسابی با خس آتش سوار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>انتقام از دشمن عاجز کشیدن کار نیست</p></div>
<div class="m2"><p>گر تو مردی این خیال پوچ از خاطر بدار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از نفس چون صبح نتوان بخیه زد در جیب عمر</p></div>
<div class="m2"><p>روزن این خانه بیدل تا کجا بندد غبار</p></div></div>