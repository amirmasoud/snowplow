---
title: >-
    غزل شمارهٔ ۲۴۱۶
---
# غزل شمارهٔ ۲۴۱۶

<div class="b" id="bn1"><div class="m1"><p>ما و نگاه شرمگین از تک و تاز دوختن</p></div>
<div class="m2"><p>آبله سا به پای عجز چشم نیاز دوختن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ضبط نفس زکف مده فرصت چاره نازک است</p></div>
<div class="m2"><p>غنچه قبا به خاک داد در غم باز دوختن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق جنون ترانه است‌، ناله نفس بهانه است</p></div>
<div class="m2"><p>بی لب بسته مشکل است پردهٔ راز دوختن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شهرت خودنمایی‌ات رونق شرم می‌برد</p></div>
<div class="m2"><p>پرده‌دری و آنگهت جامهٔ ساز دوختن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در همه حال نیستی است چاره‌گر شکست دل</p></div>
<div class="m2"><p>قابل زخم شیشه نیست غیر گداز دوختن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرد تردد حدوث بخیه به روی ما فکند</p></div>
<div class="m2"><p>خرقه درید پردهٔ شرم مجاز دوختن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر مژه بسته‌ای ز خلق هر دو جهان شکار توست</p></div>
<div class="m2"><p>قوت بال می‌دهد دیدهٔ باز دوختن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عمر به تاب وتب‌گذشت محرم عافیت نگشت</p></div>
<div class="m2"><p>رشتهٔ سعی نارسا کرد دراز دوختن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عجز نفس حباب راکرد به خامشی‌گرو</p></div>
<div class="m2"><p>رشته کجاست تا توان نغمهٔ ساز دوختن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل از ین دو روز عمر ننگ بقای‌کس مباد</p></div>
<div class="m2"><p>دل پی حرص باختن چشم به آز دوختن</p></div></div>