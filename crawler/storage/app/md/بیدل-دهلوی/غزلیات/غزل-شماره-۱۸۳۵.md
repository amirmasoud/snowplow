---
title: >-
    غزل شمارهٔ ۱۸۳۵
---
# غزل شمارهٔ ۱۸۳۵

<div class="b" id="bn1"><div class="m1"><p>بی تو مشکل ‌کنم از خلق نهان جوهر خویش</p></div>
<div class="m2"><p>اشک آیینهٔ یاس است ز چشم تر خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساکنان سرکویت ز هوس ممتازند</p></div>
<div class="m2"><p>خلد خواهد به عرق غوطه زد ازکوثر خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فطرت پست به‌کیفیت عالی نرسد</p></div>
<div class="m2"><p>کس چو گل‌، آبله را جا ندهد بر سر خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق و یاد رخ دوست‌که چشمش مرساد</p></div>
<div class="m2"><p>خواجه و حسرت مال و غم‌گاو و خر خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا نجوشد عرق خجلت تمثال ز شخص</p></div>
<div class="m2"><p>عالمی آینه‌ کرده‌ست نهان در بر خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چه خواهی همه در خانهٔ خود می‌یابی</p></div>
<div class="m2"><p>همچو آیینه اگر حلقه زنی بر در خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عجز رفتار من آخر در بیباکی زد</p></div>
<div class="m2"><p>اشک تا آبله پاگشت‌،‌گذشت از سر خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صبح جمعیت ما سوخته‌جانان دگر است</p></div>
<div class="m2"><p>ختم شبگیر کن ای شعله به خاکستر خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سعی وابستگی آخر در فیضی نگشود</p></div>
<div class="m2"><p>عقده درکار من افتاد چو قفل از پر خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سایل از حادثه آب رخ خود می‌ریزد</p></div>
<div class="m2"><p>بی ‌شکستن ندهد هیچ صدف ‌گوهر خویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فکر لذات جهان کلفت دل می‌آرد</p></div>
<div class="m2"><p>نی به صد عقده فشرده‌ست لب از شکر خویش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سفله را منصب جاه است ندامت بیدل</p></div>
<div class="m2"><p>چون مگس سیر شود دست زند بر سر خویش</p></div></div>