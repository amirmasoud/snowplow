---
title: >-
    غزل شمارهٔ ۲۵۸۷
---
# غزل شمارهٔ ۲۵۸۷

<div class="b" id="bn1"><div class="m1"><p>ای بسمل طلب پی خون چکیده رو</p></div>
<div class="m2"><p>چون اشک هر قدر روی از خود دویده رو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرصت در این‌ بهار پر افشان وحشت است</p></div>
<div class="m2"><p>همچون نگه به هر گل و خاری رسیده رو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا چند هرزه از در هر کوچه تاختن</p></div>
<div class="m2"><p>یک قطره خون شو و ز گلوی بریده رو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امروزت از امل پی فردا گرفته است</p></div>
<div class="m2"><p>ای غافل از غزل به خیال قصیده رو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سعی شرار اینهمه فرصت شمار نیست</p></div>
<div class="m2"><p>یک پر زدن به همت رنگ پریده رو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای بیخبر ز قامت پیری چه شکوه است</p></div>
<div class="m2"><p>عمری‌ست بار می‌کشی اکنون خمیده رو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زبن گرد تهمتتی که نفس نام کرده‌اند</p></div>
<div class="m2"><p>چون صبح دامنی که نداری کشیده رو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کورانه چند در پی عصیان قدم زدن</p></div>
<div class="m2"><p>شایدکه بازگردی از این راه دیده رو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی‌وحشتی رهایی ازین باغ مشکل است</p></div>
<div class="m2"><p>از بوی‌ گل به خویش فسونها دمیده رو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زین خاکدان عروج تو در خورد وحشت است</p></div>
<div class="m2"><p>بر نردبان صبح ز دامان چیده رو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قاصد پیام ما نفس واپسین ماست</p></div>
<div class="m2"><p>گر محرمی ز آینه چیزی شنیده رو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل به هر طرف کشدت کاتب قضا</p></div>
<div class="m2"><p>مانند خامه یک خط بینی‌کشیده رو</p></div></div>