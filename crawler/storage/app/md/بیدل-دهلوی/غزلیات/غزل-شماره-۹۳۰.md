---
title: >-
    غزل شمارهٔ ۹۳۰
---
# غزل شمارهٔ ۹۳۰

<div class="b" id="bn1"><div class="m1"><p>نفس درازی‌ کس تا به چون و چند نیفتد</p></div>
<div class="m2"><p>گره خوش است‌ که بیرون این‌ کمند نیفتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حیاست آینه‌پرداز اختیار تعلق</p></div>
<div class="m2"><p>اگر دل آب نگردد نفس به بند نیفتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رعونت است که چون شمع می‌کشد ته پایت</p></div>
<div class="m2"><p>به سر نیفتی اگر گردنت بلند نیفتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مروت آن همه از چشم زخم نیست‌ گزندش</p></div>
<div class="m2"><p>اگر به گوش حیا نالهٔ سپند نیفتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سفاهت است کرم بی‌تمیز موقع احسان</p></div>
<div class="m2"><p>گشاده دست و دل آن به که هرزه‌خند نیفتد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز فکر کینه ندارد گزیر طینت ظالم</p></div>
<div class="m2"><p>چه ممکن است حسد در چهی که کند نیفتد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو صبح‌ گرد من از دامنت رسیده به اوجی</p></div>
<div class="m2"><p>که تا ابد اگرش بر زمین زنند نیفتد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مباد کام کسی بی‌نصیب لذت معنی</p></div>
<div class="m2"><p>تو لب ‌گشا که جهان چون مگس به قند نیفتد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به خاک راه تو افکنده‌ام دلی که ندارم</p></div>
<div class="m2"><p>نیاز شرم کن این جنس اگر پسند نیفتد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر احتیاج به توفان دهد غبار تو بیدل</p></div>
<div class="m2"><p>چو صبح به‌ که صدا از نفس بلند نیفتد</p></div></div>