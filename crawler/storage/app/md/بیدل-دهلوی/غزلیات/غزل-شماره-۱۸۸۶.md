---
title: >-
    غزل شمارهٔ ۱۸۸۶
---
# غزل شمارهٔ ۱۸۸۶

<div class="b" id="bn1"><div class="m1"><p>عقل را مپسند با عشق جنون‌پرور طرف</p></div>
<div class="m2"><p>بیخبرتا چند سازی پنبه با اخگر طرف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کلفت جاوید پستی‌های فطرت توأم‌اند</p></div>
<div class="m2"><p>از جبین سایه کم گردد سیاهی برطرف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دل تنها توان بر قلب محشر تاختن</p></div>
<div class="m2"><p>لیک نتوان ‌گشت با یک دل ز صد لشکر طرف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرزه‌گو را قابل صحبت نگیری زینهار</p></div>
<div class="m2"><p>عاقبت خون گشت اگر گشتی به دردسر طرف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناتوانان ایمنند از رنج آفت‌های دهر</p></div>
<div class="m2"><p>تیغ کمتر می‌شود با پیکر لاغر طرف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا نفس باقیست ممکن نیست ایمن زیستن</p></div>
<div class="m2"><p>چون گلوی شمع باید بود با خنجر طرف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نالهٔ ما بر نیاید با تغافلهای ناز</p></div>
<div class="m2"><p>سعی خاموشی مگر باشد به‌ گوش ‌کر طرف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز تبسم با لب او هیچکس را تاب نیست</p></div>
<div class="m2"><p>موج می‌باید که ‌گردد با خط ساغر طرف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای بهشت آرزو بر چشم گریان رحمتی</p></div>
<div class="m2"><p>کرده‌اند این قطرهٔ خون را به صد گوهر طرف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سایه را از هیچکس اندیشهٔ تعظیم نیست</p></div>
<div class="m2"><p>ناتوانی عالمی دارد تکلف بر طرف</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بوی گل با نالهٔ بلبل وداع آماده است</p></div>
<div class="m2"><p>خیر باد دوستانم داغ کرد از هر طرف</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هیچکس سودی نبرد از انتظار مدعا</p></div>
<div class="m2"><p>تا نشد چشم طمع با حلقه‌های در طرف</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شور امکان بر نیاید با دل آسودگان</p></div>
<div class="m2"><p>جوش دریا نیست با جمعیت گوهر طرف</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا توانی بیدل از وهم تعلق قطع کن</p></div>
<div class="m2"><p>یک قلم نور است چون شد دود آتش بر طرف</p></div></div>