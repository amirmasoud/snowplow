---
title: >-
    غزل شمارهٔ ۲۵۰۶
---
# غزل شمارهٔ ۲۵۰۶

<div class="b" id="bn1"><div class="m1"><p>در جنون جوش سویدا تنگ دارد جای من</p></div>
<div class="m2"><p>چشم آهو سایه افکنده‌ست بر صحرای من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از هوا پروردگان نوبهار وحشتم</p></div>
<div class="m2"><p>چون سحر از یکدگر پاشیدن است اجزای من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناتوانیهای موجم کم نمی‌باید گرفت</p></div>
<div class="m2"><p>رو به ناخن می‌کند بحر از تپیدنهای من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکسر مویم تهی ازگریه نتوان یافتن</p></div>
<div class="m2"><p>چشمی و ‌اشکی است همچون شمع‌ سر تا پای من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاه اشک یأس وگاهی ناله عریان می‌شود</p></div>
<div class="m2"><p>خلعت دل در چه‌کوتاهی ‌ست بر بالای من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شبنم وحشت‌کمین الفت‌پرست رنگ نیست</p></div>
<div class="m2"><p>چشمکی دارد پری درکسوت مینای من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسکه جولانگاه شوقم اضطراب آلوده است</p></div>
<div class="m2"><p>جاده یکسر موج سیلابست در صحرای من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سایه در دشتی‌که صد محمل تمنا می‌کشد</p></div>
<div class="m2"><p>می‌روم از خویش و امیدی ندارم وای من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیر دیر و کعبه جز آوارگیهایم نخواست</p></div>
<div class="m2"><p>شد هواگیر از فشار این مکانها جای من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی‌رخت آیینهٔ نشو و نماگم‌کرد و سوخت</p></div>
<div class="m2"><p>چون نگه در پردهٔ شب‌، روز ناپیدای من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سرکشیدنهای اشکم‌، غافل از عجزم مباش</p></div>
<div class="m2"><p>آستان سجده می‌آراید استغنای من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غوطه درآتش زدم چون شمع و داغی یافتم</p></div>
<div class="m2"><p>این‌ گهر بوده‌ست بیدل حاصل دریای من</p></div></div>