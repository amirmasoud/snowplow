---
title: >-
    غزل شمارهٔ ۵۰۲
---
# غزل شمارهٔ ۵۰۲

<div class="b" id="bn1"><div class="m1"><p>دل از غبارِ نفس زخمِ خفته در نمک است</p></div>
<div class="m2"><p>ز موجِ پیرهن این محیط پر خسک است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهارِ رنگِ جهان جلوهٔ خزان دارد</p></div>
<div class="m2"><p>بقم درین چمن حادثاتِ اسپرک است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز اهلِ صومعه اکراه نیست مستان را</p></div>
<div class="m2"><p>که ترش‌روییِ زاهد به بزمِ می نمک است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز عرضِ شیشه تهی نیست نسخهٔ تحقیق</p></div>
<div class="m2"><p>تو آنچه کرده‌ای از خویش انتخاب شک است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به عالمِ بشری غیرِ خودنمایی نیست</p></div>
<div class="m2"><p>کسی که بگذرد از وهمِ خویشتن ملک است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قدِ خمیده کند تن‌پرست را هموار</p></div>
<div class="m2"><p>مدارِ راست‌روی‌های فیل بر کجک است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فزوده‌ایم به وحدت ز شوخ‌چشمی‌ها</p></div>
<div class="m2"><p>دمی که محو شد این صفر هر چه هست یک است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نظر به گردِ رهِ انتظار دوخته‌ایم</p></div>
<div class="m2"><p>به چشم دام سیاهی صید مردمک است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خطی به صفحهٔ دل بی‌خراشِ شوقِ تو نیست</p></div>
<div class="m2"><p>ز روی بحر به‌جز موج هر چه هست حک است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می‌ام به ساغرِ دل نقلِ یاس می‌گردد</p></div>
<div class="m2"><p>چو زخم‌ قطرهٔ آبی که می‌خورم گزک است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دویی کجاست‌، ز نیرنگِ احولی بگذر</p></div>
<div class="m2"><p>که یک نگاه میانِ دو چشم مشترک است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به اوجِ آگهی‌ات نردبان نمی‌باید</p></div>
<div class="m2"><p>نگاه تا مژه برداشته‌ست بر فلک است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر ز سوختگانی‌ سوادِ فقر گزین</p></div>
<div class="m2"><p>که شام چهرهٔ زرینِ شمع را محک است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دگر مپرس ز سامانِ بزمِ ما بیدل</p></div>
<div class="m2"><p>ز شور اشک خود اینجا کباب را نمک است</p></div></div>