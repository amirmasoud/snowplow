---
title: >-
    غزل شمارهٔ ۲۷۸۳
---
# غزل شمارهٔ ۲۷۸۳

<div class="b" id="bn1"><div class="m1"><p>در دلی اما به قصد اشکم افسون می‌کنی</p></div>
<div class="m2"><p>سر ز جیب صد هزار آیینه بیرون می‌کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز تغافلهای نازت دستگاه ناله چیست</p></div>
<div class="m2"><p>مصرع چندی‌ که من دارم تو موزون می‌کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با حنا ربطی ندارد اشک استغنای ناز</p></div>
<div class="m2"><p>می‌نهی پا بر دل پرخون و گلگون می‌کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاک اگر صد رنگ گرداند همان خاک است و بس</p></div>
<div class="m2"><p>یک زمانم ‌کرد سرگردان‌ که ‌گردون می‌کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر به این ساز است آهنگ تغافلهای ناز</p></div>
<div class="m2"><p>جوهر آیینه را زنجیر مجنون می‌کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فطرت از تاب سر مویی محرف می‌خورد</p></div>
<div class="m2"><p>در وفا گر یک قدم کج می‌روی خون می‌کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر ‌قد‌ر سعی زبانت پرفشان گفت‌وگوست</p></div>
<div class="m2"><p>عافیت می‌روبی و از خانه بیرون می‌کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ماهی بحر حقیقت تشنهٔ قلاب نیست</p></div>
<div class="m2"><p>هرزه بر زانو سرت را نقطهٔ نون می‌کنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دعوی نازک‌خیالی‌، چشم‌زخم فطرت‌ست</p></div>
<div class="m2"><p>بیخبر خاموش موی چینی افزون می‌کنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل از فهم کلامت عالمی دیوانه شد</p></div>
<div class="m2"><p>ای جنون انشا دگر فکر چه مضمون می‌کنی</p></div></div>