---
title: >-
    غزل شمارهٔ ۱۹۴۲
---
# غزل شمارهٔ ۱۹۴۲

<div class="b" id="bn1"><div class="m1"><p>در چمن‌ گر جلوه‌ات آرد به روی‌ کار گل</p></div>
<div class="m2"><p>رنگها چون شمع بندد تا به نوک خارگل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رازداران محبت پرتنک سرمایه‌اند</p></div>
<div class="m2"><p>کز جنون چیدند یک چاک‌ گریبان‌وار گل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم حیران شاهد دلهای از خود رفته است</p></div>
<div class="m2"><p>نقش پایی هست در هر جا کند رفتار گل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از رگ تاکم لب امید بی‌خمیازه نیست</p></div>
<div class="m2"><p>می‌کند زین‌ ریشه آخر نشئه‌ای سرشار گل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سبحه ریزد غنچهٔ ‌کیفیت این ‌شاخسار</p></div>
<div class="m2"><p>گر کند در باغ‌ کفرم رشتهٔ زنار گل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>الفت دلها بهار انبساط دیگر است</p></div>
<div class="m2"><p>شاخ این‌ گلبن ز پیوند آورد بسیار گل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناله از انداز جرأت در عرق گم می‌شود</p></div>
<div class="m2"><p>بلبل ما را که چون شمعست در منقار گل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درگلستانی‌ که رنگ و بوی می‌سازد بهم</p></div>
<div class="m2"><p>عالمی را از تکلف گشت ربط‌ دارگل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای شرر در سنگ رنگ آرزو گردانده‌ گیر</p></div>
<div class="m2"><p>چشم واکردن نمی‌ارزد به این مقدار گل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در بهارم داغ‌ کرد آخر به چندین رنگ یأس</p></div>
<div class="m2"><p>ساغر بی‌باده یعنی بی‌جمال یار گل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برنفس بسته‌ست فرصت محمل فیض سحر</p></div>
<div class="m2"><p>ناله شو ای رنگ تا چشمی‌ کند بیدار گل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رشتهٔ شمع‌ است مژگانم‌ که‌ گوهرهای اشک</p></div>
<div class="m2"><p>بسکه چیدم بیدل امشب‌ کرد دیگر بار گل</p></div></div>