---
title: >-
    غزل شمارهٔ ۹۴۴
---
# غزل شمارهٔ ۹۴۴

<div class="b" id="bn1"><div class="m1"><p>دل تا به‌کی‌ام جز پی آزار نگردد</p></div>
<div class="m2"><p>ظلم است گر این آبله هموار نگردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمری‌ست به تسلیم دوتایم چه توان‌کرد</p></div>
<div class="m2"><p>بر دوش ‌کسی نام نفس بار نگردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بند لب عاشق نشود مهرخموشی</p></div>
<div class="m2"><p>در نی‌ گرهی نیست ‌که منقار نگردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حیف از قدم مردکه در عرصهٔ همت</p></div>
<div class="m2"><p>سربازی شمعش گل دستار نگردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مطلوب جگرسوختگان سوز و گدازی‌ست</p></div>
<div class="m2"><p>پروانه به گرد گل و گلزار نگردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برگشتن از آن انجمن انس محال است</p></div>
<div class="m2"><p>هشدار که قاصد ز بر یار نگردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر نقطهٔ دل یک خط تحقیق تمام است</p></div>
<div class="m2"><p>پرگار بر این دایره هر بار نگردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیرون‌ نتوان رفت به هرکلفت آنتن بزم</p></div>
<div class="m2"><p>گر تنگی اخلاق دل افشار نگردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی‌باکی سعی تو به عجز است دلیلت</p></div>
<div class="m2"><p>گر پا نزنی آبله بیدار نگردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگذار دو روزی ز هوس‌ گرد برآریم</p></div>
<div class="m2"><p>هستی سر وهمی‌ست‌که بسیار نگردد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرچند حیا باب ادبگاه وصالست</p></div>
<div class="m2"><p>یارب مژه پیش تو نگونسار نگردد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل به سر ازپرتو خورشید تو دارد</p></div>
<div class="m2"><p>آن سایه‌که پیش و پس دیوار نگردد</p></div></div>