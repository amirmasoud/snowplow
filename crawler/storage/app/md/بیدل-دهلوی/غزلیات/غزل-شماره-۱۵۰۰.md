---
title: >-
    غزل شمارهٔ ۱۵۰۰
---
# غزل شمارهٔ ۱۵۰۰

<div class="b" id="bn1"><div class="m1"><p>تا نفس ما ومن غبارنبود</p></div>
<div class="m2"><p>همه بودیم و غیر یار نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نخل این باغ را به‌کسوت شمع</p></div>
<div class="m2"><p>جز گداز خود آبیار نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سعی پرواز آشیان گم کرد</p></div>
<div class="m2"><p>بی‌پر و بالی آشکار نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عالم آیینه خانهٔ سوداست</p></div>
<div class="m2"><p>جز به خود هیچکس دچار نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر حبابی‌که بازکرد آغوش</p></div>
<div class="m2"><p>غیر درباب بی‌کنار نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه حنا رنگ ناز بیرون داد</p></div>
<div class="m2"><p>دست ما نیز بی‌نگار نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وهم بی‌پردگی قیامت‌کرد</p></div>
<div class="m2"><p>نغمهٔ کس برون تار نبود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عثثبق‌از هرچه‌خواست‌شور انگیخت</p></div>
<div class="m2"><p>خاک ما قابل غبار نبود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>انتظار گل دگر داریم</p></div>
<div class="m2"><p>اینقدر رنگ و بو بهار نبود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سیر بام سپهر هم کردیم</p></div>
<div class="m2"><p>این هواها و هوای یار نبود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سیر بام سپهر هم‌کردیم</p></div>
<div class="m2"><p>این هواها هوای یار نبود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حلقه‌گشتیم لیک بر در یاس</p></div>
<div class="m2"><p>خلوتی داشتیم و بار نبود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>محرمی چشم ما ز ما پوشید</p></div>
<div class="m2"><p>چه توان کرد پرده‌دار نبود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نشنیدیم بوی زنده‌دلی</p></div>
<div class="m2"><p>ششجهت غیریک مزار نبود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>غم تیمار جسم باید خورد</p></div>
<div class="m2"><p>رنج ما ناقه بود بار نبود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عجز جز زیر پاکجا تازد</p></div>
<div class="m2"><p>سایه آخر شترسوار نبود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هیچکس قدر زندگی نشناخت</p></div>
<div class="m2"><p>وصل ما مردن انتظار نبود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عالمی در خیال عشق و هوس</p></div>
<div class="m2"><p>کارها کرد و هیچ ‌کار نبود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اینکه مختار فعل نیک و بدیم</p></div>
<div class="m2"><p>بیدل آیین اختیار نبود</p></div></div>