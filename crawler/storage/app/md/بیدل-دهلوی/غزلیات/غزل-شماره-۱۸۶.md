---
title: >-
    غزل شمارهٔ ۱۸۶
---
# غزل شمارهٔ ۱۸۶

<div class="b" id="bn1"><div class="m1"><p>بحرمی‌پیچد به‌موج از اشک غم‌پرورد ما</p></div>
<div class="m2"><p>چرخ می‌گردد دوتا در فکر بار درد ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p> گر به میدان ریاضت‌کهربا دعوی‌کند</p></div>
<div class="m2"><p> کاه‌گیرد در دهن از شرم رنگ زرد ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p> دور نبود گرکمان صید دلها زه‌کند</p></div>
<div class="m2"><p> هم ادای ابروی نازی‌ست بیت فرد ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p> می‌دهد بوی گریبان سحر موج نسیم</p></div>
<div class="m2"><p> می‌توان دانست حال دل ز آه سرد ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p> هم‌چو نی در هر نفست دایم نقد ناله‌ای</p></div>
<div class="m2"><p> ای هوس غافل مباش ازگنج باد آورد ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p> ما سبکر‌وحان ز قید ششدر تن فارغیم</p></div>
<div class="m2"><p> مهرة آزاد دل دارد بساط نرد ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p> گر دهد صدبارگردون خاک عالم را به‌باد</p></div>
<div class="m2"><p> بشکندآشفتگی رنگی به روی‌گرد ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p> دوش‌ با تیغ تبسم رفتی از بزم و هنوز</p></div>
<div class="m2"><p> شور بیرون می‌دهد زخم نمک‌پرورد ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p> در سواد حیرت از یاد جمالت بیخودیم</p></div>
<div class="m2"><p> روز وشب خواب سحر دارد دل شبگرد ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست بیدل جزنوای قلقل مینای من</p></div>
<div class="m2"><p> هیچکس درمحفل خونین‌دلان همدرد ما</p></div></div>