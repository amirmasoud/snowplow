---
title: >-
    غزل شمارهٔ ۱۶۱۲
---
# غزل شمارهٔ ۱۶۱۲

<div class="b" id="bn1"><div class="m1"><p>صبحی به‌گوش عبرتم از دل صدا رسید</p></div>
<div class="m2"><p>کای بیخبربه ما نرسید آنکه وارسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دریاست قطره‌ای که به دریا رسیده است</p></div>
<div class="m2"><p>جز ما کسی دگر نتواند به ما رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سعی نفس ز دل سر مویی نرفت پیش</p></div>
<div class="m2"><p>جایی‌ که کس نمی‌رسد این نارسا رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مزد فسردنی‌که به خاکم قدم زند</p></div>
<div class="m2"><p>یاد قدت به سیر بهارم عصا رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آسودگی به خاک‌نشینان مسلم است</p></div>
<div class="m2"><p>این حرفم از صدای نی بوربا رسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دنیا که تاج کج‌کلهان نقش پای اوست</p></div>
<div class="m2"><p>بر ما غبار ریخت‌که تا پشت پا رسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طبع ترا مباد فضول هوس‌کند</p></div>
<div class="m2"><p>میراث سایه‌ای‌ که ز بال هما رسید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشاق دیگر از که وفا آرزو کنند</p></div>
<div class="m2"><p>دل نیز رفته رفته به آن بی‌وفا رسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون ناله‌ای که بگذرد از بندبندنی</p></div>
<div class="m2"><p>صد جا نشست حسرت دل تا به ما رسید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا وادی غبار نفس طی نمی‌شود</p></div>
<div class="m2"><p>نتوان به مقصد دل بی مدعا رسید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر غفلت انفعال و به آگاهی انبساط</p></div>
<div class="m2"><p>برهرکه هرچه می‌رسد ازمصطفا رسید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از خود گذشتنی‌ست فلک ‌تازی نگاه</p></div>
<div class="m2"><p>تا نگذری زخود نتوان هیچ جا رسید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خون دلی به دیده بیدل مگر نماند</p></div>
<div class="m2"><p>کز بهر پای‌بوس تو رنگ حنا رسید</p></div></div>