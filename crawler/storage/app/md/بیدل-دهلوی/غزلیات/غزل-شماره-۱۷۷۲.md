---
title: >-
    غزل شمارهٔ ۱۷۷۲
---
# غزل شمارهٔ ۱۷۷۲

<div class="b" id="bn1"><div class="m1"><p>فریاد جهان سوخت نفس سعی ‌کمندش</p></div>
<div class="m2"><p>تا سرمه رسانید به مژگان بلندش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از حیرت راه طلبش انجم و افلاک</p></div>
<div class="m2"><p>گم‌ کرد صدا قافلهٔ زنگله بندش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ننمود سحر نیز درین معرض ناموس</p></div>
<div class="m2"><p>بیش از دو نفس رشته به صد چاک پرندش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر گرد که برخاست ازین دشت پری بود</p></div>
<div class="m2"><p>یارب به چه رفتار جنون‌ کرد سمندش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صد مصر شکر آب شد از شرم حلاوت</p></div>
<div class="m2"><p>پیش دو لب او که مکرر شده قندش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کو تحفهٔ دیگر که بیرزد به قبولی</p></div>
<div class="m2"><p>دل پیشکشی بود که در خاک فکندش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز در چمن شرم جمالش نتوان دید</p></div>
<div class="m2"><p>ای آیینه‌سازان عرق افتاد پسندش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تسلیم به غارتکدهٔ یأس ندارد</p></div>
<div class="m2"><p>جز سجده ‌که ترسم ز جبینم ببرندش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون من ز دل خاک‌ کمربسته جهانی</p></div>
<div class="m2"><p>تا زور چه همت‌گسلد اینهمه بندش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تشویش دل کس نتوان سهل شمردن</p></div>
<div class="m2"><p>زان شیشه حذر کن‌ که به راهت شکنندش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل فتنهٔ شورافکن هنگامهٔ هستی است</p></div>
<div class="m2"><p>نُه مجمر گردون و یک آواز سپندش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل به‌ که ‌گویم غم بیداد محبت</p></div>
<div class="m2"><p>این تیر نه آهی‌ست ‌که از دل شکنندش</p></div></div>