---
title: >-
    غزل شمارهٔ ۲۸۲۲
---
# غزل شمارهٔ ۲۸۲۲

<div class="b" id="bn1"><div class="m1"><p>به وحشت برنمی‌آیم ز فکر چشم جادویی</p></div>
<div class="m2"><p>چو رم دارم وطن در سایهٔ مژگان آهویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بزمت نیست ممکن جرأت تحریک مژگانم</p></div>
<div class="m2"><p>نی‌ام آیینه اما از تحیر برده‌ام بویی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگردی ای صبا بر هم زن هنگامهٔ عهدم</p></div>
<div class="m2"><p>که من مشت غباری‌ کرده‌ام نذر سر کویی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پیری هم ز قلاب محبت نیستم ایمن</p></div>
<div class="m2"><p>قد خم‌گشته جیبم می‌کشد تا ناز ابرویی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهانی نقد فطرت در تلاش شبهه می‌بازد</p></div>
<div class="m2"><p>یقین مزد تو،‌گر پیدا نمایی همچو من رویی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر تسلیم می‌دزدم به بالین پر عنقا</p></div>
<div class="m2"><p>چه سازم در خم نُه چرخ پیدا نیست زانویی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سراغ از حیرت من کن رم لیلی نگاهان را</p></div>
<div class="m2"><p>برون از چشم مجنون نیست نقش پای آهویی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دو عالم معنی آشفته حالی در گره دارم</p></div>
<div class="m2"><p>دل افسرده‌ام مهریست بر طومار گیسویی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دماغ آشفتگان را مهرهٔ سودا اثر دارد</p></div>
<div class="m2"><p>برای زلف سازید از دلم تعویذ بازویی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به رنگی ناتوانم در تمنای میان او</p></div>
<div class="m2"><p>که گرداند عیان مانند تصویرم سر مویی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>محال است آنچه می‌خواهم‌، خیالست اینکه می‌بینم</p></div>
<div class="m2"><p>مقابل کرده‌اند آیینهٔ من با پریرویی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خیال نیست. سیر شبستانی دگر دارد</p></div>
<div class="m2"><p>چو شمع کشته سر دزدیده‌ام درکنج زانویی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>درین‌گلشن چو بوی گل مریض وحشتی دارم</p></div>
<div class="m2"><p>که خالی می‌کند صد بستر از تغییر پهلویی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بهار راحت از پاس نفس گل می‌کند بیدل</p></div>
<div class="m2"><p>به رنگ گل ندارم زین چمن سررشتهٔ بویی</p></div></div>