---
title: >-
    غزل شمارهٔ ۱۱۶۶
---
# غزل شمارهٔ ۱۱۶۶

<div class="b" id="bn1"><div class="m1"><p>کار دلها باز از آن مژگان به سامان می‌رسد</p></div>
<div class="m2"><p>ریشهٔ تاکی به استقبال مستان می‌رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اشک امشب بسمل حسن عرق توفان‌ کیست</p></div>
<div class="m2"><p>زبن پر پروانه پیغام چراغان می‌رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بهار آن خط نو رسته غافل نیستم</p></div>
<div class="m2"><p>مدتی شد در دماغم بوی ریحان می‌رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آب می‌گردد دل از بی‌دست‌وپایی‌های اشک</p></div>
<div class="m2"><p>در کنارم از کجا این طفل‌ گریان می‌رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سطر چاکی از خط طومار مجنون خواندنیست</p></div>
<div class="m2"><p>قاصد ما نامه در دست از گریبان می‌رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی‌محبت در وطن هم ناشناسایی‌ست عام</p></div>
<div class="m2"><p>بهر یک دل بوی پیراهن به ‌کنعان می‌رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بس که بر تنگی بساط عشق امکان چیده‌اند</p></div>
<div class="m2"><p>صد گریبان می‌درّد تا گل به دامان می‌رسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فرصت تمهید آسایش در این محفل ‌کجاست</p></div>
<div class="m2"><p>خواب ها رفته‌ ست تا مژگان به مژگان می‌رسد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل به آفت واگذار و ایمن از توفان برآ</p></div>
<div class="m2"><p>بر کنار این ‌کشتی از هول نهنگان می‌رسد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قطع ‌کن از نعمت الوان‌ که اینجا چرخ هم</p></div>
<div class="m2"><p>می‌نهد صد ریزه برهم تا به یک نان می‌رسد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حاصل غواص این دریا پشیمانی بس است</p></div>
<div class="m2"><p>وصل ‌گوهر گیر اگر دستت به دامان می‌رسد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در کمند سعی نیکی چین ‌کوتاهی خطاست</p></div>
<div class="m2"><p>تا به هر دامن‌ که خواهی دست احسان می‌رسد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خاکساری در مذاق هیچکس مکروه نیست</p></div>
<div class="m2"><p>منّت این وضع بر گبر و مسلمان می‌رسد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پیشه بسیار است بیدل بر خموشی ختم‌ کن</p></div>
<div class="m2"><p>سعی در علم و عمل اینجا به پایان می‌رسد</p></div></div>