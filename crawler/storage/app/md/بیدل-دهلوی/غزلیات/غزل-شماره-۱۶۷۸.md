---
title: >-
    غزل شمارهٔ ۱۶۷۸
---
# غزل شمارهٔ ۱۶۷۸

<div class="b" id="bn1"><div class="m1"><p>چشم واکن رنگ اسرار دگر دارد بهار</p></div>
<div class="m2"><p>آنچه در وهمت نگنجد جلوه‌گر دارد بهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساعتی چون بوی‌ گل از قید پیراهن برآ</p></div>
<div class="m2"><p>از تو چشم آشنایی آنقدر دارد بهار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کهکشان هم پایمال موج توفان گل است</p></div>
<div class="m2"><p>سبزه را از خواب غفلت چند بردارد بهار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از صلای رنگ عیش انجمن غافل مباش</p></div>
<div class="m2"><p>پاره‌هایی چند بر خون جگر دارد بهار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم تا واکرده‌ای رنگ از نظرها رفته است</p></div>
<div class="m2"><p>از نسیم صبح دامن بر کمر دارد بهار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی فنا نتوان گلی زین هستی موهوم چید</p></div>
<div class="m2"><p>صفحهٔ ما گر زنی آتش شرر دارد بهار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از خزان آیینه دارد صبح تا گل می‌کند</p></div>
<div class="m2"><p>جز شکستن نیست رنگ ما اگر دارد بهار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ابر می‌نالد کز اسباب نشاط این چمن</p></div>
<div class="m2"><p>هرچه دارد در فشار چشم تر دارد بهار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ازگل و سنبل به نظم و نثر سعدی قانعم</p></div>
<div class="m2"><p>این معانی درگلستان بیشتر دارد بهار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مو به مویم حسرت زخمت تبسم می‌کند</p></div>
<div class="m2"><p>هرکه گردد بسملت بر من نظر دارد بهار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زین چمن بیدل نه سروی جست و نه شمشاد رست</p></div>
<div class="m2"><p>از خیال قامتش دودی به سر دارد بهار</p></div></div>