---
title: >-
    غزل شمارهٔ ۸۸۶
---
# غزل شمارهٔ ۸۸۶

<div class="b" id="bn1"><div class="m1"><p>تا ز پیدایی به‌گوشم خواند افسون احتیاج</p></div>
<div class="m2"><p>روز اول چون دلم خواباند در خون احتیاج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نغمهٔ‌قانون این‌محفل صلای جودکیست</p></div>
<div class="m2"><p>عالمی را از عدم آورد بیرون احتیاج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حسن ‌و عشقی‌نیست جز اقبال‌ و ادبار ظهور</p></div>
<div class="m2"><p>لیلی این بزم استغناست‌، مجنون احتیاج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا نشد خاکستر از آتش سیاهی‌گم نشد</p></div>
<div class="m2"><p>تیره‌بختیها مرا هم‌کرد صابون احتیاج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صید نیرنگ توهم را چه هستی‌کوعدم</p></div>
<div class="m2"><p>پیش ‌ازین خونم غنا می‌خورد اکنون احتیاج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درخور جا هست ابرام فضولیهای طبع</p></div>
<div class="m2"><p>سیم‌و زر چون‌بیش‌شد می‌گردد افزون ا‌حتیاج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با لئیمان‌ گر چنین حرص‌ گدا طبعت خوش است</p></div>
<div class="m2"><p>بایدت زیر زمین بردن به قارون احتیاج</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر لب از اظهار بندی اشک مژگان می‌درد</p></div>
<div class="m2"><p>تا کجا باید نهفت این ناله مضمون احتیاج</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صبح این ویرانه با آن بی‌تعلق زیستن</p></div>
<div class="m2"><p>می‌برد از یک ‌نفس هستی به ‌گردون احتیاج‌</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عرض مطلب نرمی گفتار انشا می‌کند</p></div>
<div class="m2"><p>حرف ناموزون ما راکرد موزون احتیاج</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همچو اهل‌ قبر بیدل بی‌نفس‌ باشی ‌خوش ‌است</p></div>
<div class="m2"><p>تا نبندد رشته‌ات بر سازگردون احتیاج</p></div></div>