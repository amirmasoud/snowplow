---
title: >-
    غزل شمارهٔ ۵۳
---
# غزل شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>خدا چو شمع دهد جرأت آب دیدهٔ ما را</p></div>
<div class="m2"><p>که افکند ته پاگردن‌کشیدهٔ ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شهید تیغ تغافل بر آستان‌که نالد</p></div>
<div class="m2"><p>تظلمی‌ست چو اشک از نظر چکیدهٔ ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه دشت و درکه نکردیم قطع درپی فرصت</p></div>
<div class="m2"><p>کسی نداد سراغ آهوی رمیدهٔ ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نداشتیم به وهم آنقدر دماغ تپیدن</p></div>
<div class="m2"><p>به باد داد نفس خاک آرمیدهٔ ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به انفعال رسیدیم از فسون تعلق</p></div>
<div class="m2"><p>به رخ فکند حیا دامن نچیدهٔ ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر به محکمهٔ دل یقین شود حق وباطل</p></div>
<div class="m2"><p>گواه‌کیست حدیث ز خود شنیدهٔ ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نبرد همت‌کس از تلاش‌گوی تسلی</p></div>
<div class="m2"><p>بیفکنید درتن ره شر بریدهٔ ما را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زربشه تا به ثمر صدهزارمرحله طی شد</p></div>
<div class="m2"><p>که‌کرد این همه قاصد به خود رسیدهٔ ما را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مژه زهم نگشودیم تا چکد نم اشکی</p></div>
<div class="m2"><p>گداخت شرم رقم‌کلک شق ندیدهٔ ما را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مباد تا به ابد نالد و خموش نگردد</p></div>
<div class="m2"><p>به یاد شمع مده صبح نادمیدهٔ ما را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مقیم‌گوشهٔ نقش قدم شویم وگرنه</p></div>
<div class="m2"><p>درکه حلقه‌کند پیکر خمیدهٔ ما را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نهفته است قضا سرنوشت معنی بیدل</p></div>
<div class="m2"><p>رقم‌کجاست مگر خط‌کشی جریدهٔ ما را</p></div></div>