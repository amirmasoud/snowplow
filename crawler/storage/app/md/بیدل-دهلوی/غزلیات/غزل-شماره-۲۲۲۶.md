---
title: >-
    غزل شمارهٔ ۲۲۲۶
---
# غزل شمارهٔ ۲۲۲۶

<div class="b" id="bn1"><div class="m1"><p>چنین آفت نصیب از طبع راحت دشمن خویشم</p></div>
<div class="m2"><p>اگر یک دانهٔ دل جمع‌ کردم خرمن خویشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو گل از پیکرم یک غنچه جمعیت نمی‌خندد</p></div>
<div class="m2"><p>به صد آغوش حیرانی بهم آوردن خویشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به وحشت سخت محکم‌ کرده‌ام سر رشتهٔ الفت</p></div>
<div class="m2"><p>به رنگ موج در قلاب چین دامن خویشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلیلی در سواد وحشت امکان نمی‌باشد</p></div>
<div class="m2"><p>همان چون برق شمع راه از خود رفتن خویشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فروغ خویش سیلاب بنای شمع می‌باشد</p></div>
<div class="m2"><p>به غارت رفتهٔ توفان طبع روشن خویشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیه بختی به رنگ سایه مفت ساز جمعیت</p></div>
<div class="m2"><p>عبیری دارم و آرایش پیراهن خویشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمی‌دانم خیالم نقش پیمان‌ که می‌بندد</p></div>
<div class="m2"><p>که چون رنگ ضعیفان بست بشکن‌ بشکن خویشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تعلق صرفهٔ جمعیت خاطر نمی‌خواهد</p></div>
<div class="m2"><p>خیال دوستی با هر که بندم دشمن خویشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تمیزی گر نمی‌بود آنقدر عبرت نبود اینجا</p></div>
<div class="m2"><p>تحیر نامه ‌در دست از مژه وا کردن خویشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پر افشانم پری تا وارهم از چنگ خود داری</p></div>
<div class="m2"><p>به این کلفت چه لازم در قفس پروردن خویشم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کف خاکستر من نیست بی سیر سمن زاری</p></div>
<div class="m2"><p>چو آتش از شکست رنگ‌ گل در دامن خویشم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به خاک افتاده‌ام تا در زمین عاریت بیدل</p></div>
<div class="m2"><p>مگر بر باد رفتن وا نماید مسکن خویشم</p></div></div>