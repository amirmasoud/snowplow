---
title: >-
    غزل شمارهٔ ۴۹۴
---
# غزل شمارهٔ ۴۹۴

<div class="b" id="bn1"><div class="m1"><p>بروت تافتنت‌گربه شانی هوس است</p></div>
<div class="m2"><p>به ریش مرد شدن بزگمانی هوس است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به حرف و صوت پلنگی نیاید از روباه</p></div>
<div class="m2"><p>فسون غرشت افسانه‌خوانی هوس است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز آدمی چه معاش است هم‌جوالی خرس</p></div>
<div class="m2"><p>تلاش صوف ونمد زندگانی هوس است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به وهم وانگذارد خرد زمام حواس</p></div>
<div class="m2"><p>رمه به‌گرگ سپردن شبانی هوس است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه لازم است به شیخی علاقهٔ دستار</p></div>
<div class="m2"><p>خری به شاخ رساندن جوانی هوس است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دستگاه شترمرغ انفعال مکش</p></div>
<div class="m2"><p>که محملت همه برپرفشانی هوس است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غبار عبرت سر چنگهای خرس بگیر</p></div>
<div class="m2"><p>که ریش‌گاوی واین شانه‌رانی هوس است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز تازیانه و چوب آنچه مایهٔ اثر است</p></div>
<div class="m2"><p>برای‌کون خران میهمانی هوس است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تنیده است به دم لابگی جنون هوس</p></div>
<div class="m2"><p>بدین سگان چقدر میزبانی هوس است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به سحرپوچ ز اعجاز دم زدن بیدل</p></div>
<div class="m2"><p>در این حیاکده گوساله‌بانی هوس است</p></div></div>