---
title: >-
    غزل شمارهٔ ۲۱۴۶
---
# غزل شمارهٔ ۲۱۴۶

<div class="b" id="bn1"><div class="m1"><p>خیال آن مژه عمریست در نظر دارم</p></div>
<div class="m2"><p>درین چمن قلم نرگسی به سر دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیاز من همه ناز، احتیاجم استغنا</p></div>
<div class="m2"><p>گل بهار توام رنگ از که بردارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وصال اگر ثمر دیده‌ها‌ی بی‌خوابست</p></div>
<div class="m2"><p>من این امید ز آیینه بیشتر دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل و دماغ تماشای فرصتم‌ کم نیست</p></div>
<div class="m2"><p>هزار آینه در چشمک شرر دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به یاد نرگس مستش‌گرفته‌ام قدحی</p></div>
<div class="m2"><p>دگر مپرس ز من عالمی دگر دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خمار عیش ندارد مقیم دیر وفا</p></div>
<div class="m2"><p>دلی گداخته‌ام شیشه در نظر دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حضور دولت بی‌اعتباریم چه کم است</p></div>
<div class="m2"><p>گره ندارم اگر رشته بی‌گهر دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غم فضولی وحشت‌ کجا برم یارب</p></div>
<div class="m2"><p>که شش جهت چو نگه یک قدم سفر دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جنون شکست به بیکار‌ی‌ام ز عریانی</p></div>
<div class="m2"><p>به دست جای گریبان همین کمر دارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کسی به فهم‌ کمالم دگر چه پردازد</p></div>
<div class="m2"><p>ز فرق تا به قدم عیبم این هنر دارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دلیر عرصهٔ لافم ز انفعال مپرس</p></div>
<div class="m2"><p>همین قدرکه نفس خون کنم جگر دارم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کجاست مشتری لفظ و معنی‌ام بیدل</p></div>
<div class="m2"><p>پری متاعم و دکان شیشه‌گر دارم</p></div></div>