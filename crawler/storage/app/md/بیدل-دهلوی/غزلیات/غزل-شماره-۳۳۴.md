---
title: >-
    غزل شمارهٔ ۳۳۴
---
# غزل شمارهٔ ۳۳۴

<div class="b" id="bn1"><div class="m1"><p>همیشه سنگدلانند نامدار طرب</p></div>
<div class="m2"><p>ز خنده نقش نگین را به هم نیاید لب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زبان حاسد وتمهید راستی غلط است</p></div>
<div class="m2"><p>کجی به در نتوان برد از دم عقرب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سواد فقر اثر مایهٔ صفای دل است</p></div>
<div class="m2"><p>چو صبح پاک‌نما چهره‌ای به دامن شب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به غیر عشق نداریم هیچ آیینی</p></div>
<div class="m2"><p>گزیده‌ایم چو پروانه سوختن مذهب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هنر به اهل حسد می‌دهد نتیجهٔ عیب</p></div>
<div class="m2"><p>ز جوهرست در ابروی تیغ چین‌غضب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هوس چگونه‌کند شوخی از دل قانع</p></div>
<div class="m2"><p>به دامن‌گهر آسوده است موج‌طلب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دشت عجز تحیر متاع قافله‌ایم</p></div>
<div class="m2"><p>اگر بر آینه محمل‌کشیم نیست‌عجب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو چشمه زندگی ما به‌اشک موقوف‌است</p></div>
<div class="m2"><p>دگر زگریهٔ ما بیخودان مپرس سبب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بساط زلف شود چیده در دمیدن خط</p></div>
<div class="m2"><p>به چاک سینهٔ صبح است‌چین دامن شب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جهان قلمرو اظهار بی‌نیازیهاست</p></div>
<div class="m2"><p>کدام ذره‌که او نپست آفتاب نسب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سر از ره تو چسان واکشم‌که بی‌قدمت</p></div>
<div class="m2"><p>رکاب با دل سنگین تهی‌کند قالب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز بسکه دشمن آسودگی‌ست طینت من</p></div>
<div class="m2"><p>چو شعله می‌شکند رنگ؟ از شکستن تب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قدح‌پرستی از اسباب فارغم دارد</p></div>
<div class="m2"><p>کتاب دردسری شسته‌ام به آب غضب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به خامشی طلب از لعل یارکام امید</p></div>
<div class="m2"><p>که بوسه روندهدتا به هم نیاری لب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به پیش جلوهٔ طاقت‌گداز او بیدل</p></div>
<div class="m2"><p>گزید جوهر آیینه پشت دست ادب</p></div></div>