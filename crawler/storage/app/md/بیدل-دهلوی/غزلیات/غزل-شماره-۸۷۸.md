---
title: >-
    غزل شمارهٔ ۸۷۸
---
# غزل شمارهٔ ۸۷۸

<div class="b" id="bn1"><div class="m1"><p>همه‌کس‌ کشیده محمل به جناب‌ کبریایت</p></div>
<div class="m2"><p>من و خجلت سجودی‌که نریخت‌گل به پایت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه به خاک دربسودم نه به سنگش آزمودم</p></div>
<div class="m2"><p>به‌کجا برم سری راکه نکرده‌ام فدایت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشود خمار شبنم می جام انفعالم</p></div>
<div class="m2"><p>چو سحر چه مغز چیند سر خالی از هوایت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طرب بهار امکان به چه حسرتم فریبد</p></div>
<div class="m2"><p>به بر خیال دارم‌ گل رنگی از قبایت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هوس دماغ شاهی چه خیال دارد اینجا</p></div>
<div class="m2"><p>به فلک فرو نیاید سرکاسهٔ گدایت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به بهار نکته سازم ز بهشت بی‌نیازم</p></div>
<div class="m2"><p>چمن‌آفرین نازم به تصور لقایت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نتوان کشید دامن ز غبار مستمندان</p></div>
<div class="m2"><p>بخرام و نازها کن سر ما و نقش پایت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نفس از تو صبح خرمن نگه از تو گل به دامن</p></div>
<div class="m2"><p>تویی آنکه در بر من تهی از من است جایت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز وصال بی‌حضورم به پیام ناصبورم</p></div>
<div class="m2"><p>چقدر ز خویش دورم ‌که به من رسد صدایت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نفس هوس‌خیالان به هزار نغمه صرف است</p></div>
<div class="m2"><p>سر دردسر ندارم من بیدل و دعایت</p></div></div>