---
title: >-
    غزل شمارهٔ ۷۹۸
---
# غزل شمارهٔ ۷۹۸

<div class="b" id="bn1"><div class="m1"><p>سرو چمن دل الف شعلهٔ آهیست</p></div>
<div class="m2"><p>سرسبزی این مزرعه را برق‌ گیاهیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌جرأت بینش نتوان محو تو گشتن</p></div>
<div class="m2"><p>سررشتهٔ حیرانی ما، مدّ نگاهیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی سد ره اشک شود، دامن‌ رنگم</p></div>
<div class="m2"><p>گر کوه بود در دم سیلش پر کاهیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز صیقلی آیینهٔ آب ندارد</p></div>
<div class="m2"><p>هرچندکه سرو لب جو، مصرع آهیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عزت‌طلبی‌، جوهر تسلیم به ‌دست آر</p></div>
<div class="m2"><p>اینجا خم طاعت‌، شکن طرف ‌کلاهیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا چند زند لاف بلندی‌، سرگردون</p></div>
<div class="m2"><p>این بیضه به زبر پر پرواز نگاهیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر حاصل دنیا چقدر ناز توان کرد</p></div>
<div class="m2"><p>سرتاسر این مزرعه یک مشت ‌گیاهیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فرش در دل شو،‌ که ‌درین عرصه نفس را</p></div>
<div class="m2"><p>از هرزه‌دوی خانهٔ آیینه پناهیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زین‌ هستی بیهوده صوابی‌ که تو داری</p></div>
<div class="m2"><p>گر جرم تصور نکنی سخت ‌گناهیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فال سر تسلیم زن و ساز قدم ‌کن</p></div>
<div class="m2"><p>تا منزل رحت ز گریان نو راهیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل پی آن جلوه که من رفته‌‌ام از خویش</p></div>
<div class="m2"><p>هر نفش قدم، صورت خمیازه آهیست</p></div></div>