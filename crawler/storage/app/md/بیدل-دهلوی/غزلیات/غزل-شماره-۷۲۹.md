---
title: >-
    غزل شمارهٔ ۷۲۹
---
# غزل شمارهٔ ۷۲۹

<div class="b" id="bn1"><div class="m1"><p>دل را به خیال خط او سیر فرنگیست</p></div>
<div class="m2"><p>این آینه صاحب‌نظر از سرمهٔ زنگیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غافل مشو از سیر تماشاگه داغم</p></div>
<div class="m2"><p>هر برگ گلی زین چمن آیینهٔ زنگیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در گلخن وحشتکدهٔ فرصت امکان</p></div>
<div class="m2"><p>دودی‌، شرری چند شتابی و درنگیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون بشکند این ساز، چه خشم و چه مدارا</p></div>
<div class="m2"><p>زیر و بم تار نفست صلحی و جنگیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از اهل تکبر مطلب ساز شکفتن</p></div>
<div class="m2"><p>چین ‌بر رخ ‌این ‌شعله ‌مزاجان ‌رگ سنگیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محمل کش صف قافله بیتابی شوقیم</p></div>
<div class="m2"><p>چاک دل ما هم جرس ناله به چنگیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهدی که برآیی زکمانخانهٔ آفاق</p></div>
<div class="m2"><p>نخجیر مراد دو جهان صید خدنگیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حیرت مگر از دل ‌کند ایجاد فضایی</p></div>
<div class="m2"><p>ورنه چو نگه خانهٔ ما گوشهٔ تنگیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون لاله ز بس‌گرمرو حسرت داغم</p></div>
<div class="m2"><p>صحرا ز نشان قدمم پشت پلنگیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آزادگی موج‌، زگوهر چه خیالی‌ست</p></div>
<div class="m2"><p>تمکین به ره قطرهٔ ما پشتهٔ سنگیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون شمع ز بس آینه سامان بهارم</p></div>
<div class="m2"><p>تا ناوک آهم‌ سر و برگش پر رنگیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل‌گهر عشق به بحری است‌که آنجا</p></div>
<div class="m2"><p>آیینهٔ هر قطره‌گریبان نهنگیست</p></div></div>