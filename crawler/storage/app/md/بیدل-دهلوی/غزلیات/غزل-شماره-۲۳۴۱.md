---
title: >-
    غزل شمارهٔ ۲۳۴۱
---
# غزل شمارهٔ ۲۳۴۱

<div class="b" id="bn1"><div class="m1"><p>هیچ می‌دانی مآل خود چرا نشناختیم</p></div>
<div class="m2"><p>سر به پیش پا نکردیم از حیا نشناختیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غیرت یکتاییش از خودشناسی ننگ داشت</p></div>
<div class="m2"><p>قدر ما این بس که ما هم خویش را نشناختیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالمی را معرفت شرمندهٔ جاوید کرد</p></div>
<div class="m2"><p>خودشناسی ننگ ‌کوری شد ترا نشناختیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل اگربا خلق‌کم جوشید جای شکوه نیست</p></div>
<div class="m2"><p>از همه بیگانه بودیم آشنا نشناختیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم پوشیدن جهان عافیت ایجاد کرد</p></div>
<div class="m2"><p>غیر کنج دل برای امن جا نشناختیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در گلستانی که رنگش پایمال ناز بود</p></div>
<div class="m2"><p>خون ما هم داشت رنگی از حنا نشناختیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم‌بندی بی‌تمیزی را نمی‌باشد علاج</p></div>
<div class="m2"><p>حسن عریان بود ما غیر از فنا نشناختیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهل موج و کف به فهم راز دریا روشن است</p></div>
<div class="m2"><p>عشق مستغنی است ‌گر ما و شما نشناختیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عالم از کیفیت رد و قبول آگاه نیست</p></div>
<div class="m2"><p>چون نفس یکسر برو را از بیا نشناختیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فهم واجب نیست ممکن تا ابد از ممکنات</p></div>
<div class="m2"><p>اینکه ما نشناختیمت از کجا نشناختیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بی‌نیازی از تمیز عین و غیر آزاده است</p></div>
<div class="m2"><p>جرم غفلت نیست بی‌بود که ما نشناختیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صبر اگر می‌بود ابرام طلب خجلت نداشت</p></div>
<div class="m2"><p>ما اجابت را دو دم پیش از دعا نشناختیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زین تماشا بیدل از وحشت عنانیهای عمر</p></div>
<div class="m2"><p>دیده و دانسته بگذشتیم یا نشناختیم</p></div></div>