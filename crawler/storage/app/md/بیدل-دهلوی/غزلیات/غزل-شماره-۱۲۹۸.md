---
title: >-
    غزل شمارهٔ ۱۲۹۸
---
# غزل شمارهٔ ۱۲۹۸

<div class="b" id="bn1"><div class="m1"><p>از هجوم کلفت دل ناله بی‌آهنگ ماند</p></div>
<div class="m2"><p>بوی این‌ گل از ضعیفی در طلسم رنگ ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوختیم و مشت خاشاکی ز ما روشن نشد</p></div>
<div class="m2"><p>شعلهٔ ما چون نفس در دام این نیرنگ ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از حیا موجی نزد هر چند دل از هم‌ گداخت</p></div>
<div class="m2"><p>آب شد آیینه اما حیرتش در چنگ ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سنگ راه هیچکس تحصیل جمعیت مباد</p></div>
<div class="m2"><p>قطرهٔ بیتاب ما گوهر شد و دلتنگ ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خرابات هوس تا دور جام ما رسید</p></div>
<div class="m2"><p>بیدماغی از شراب و نکبتی از بنگ ماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عجز طاقت در طلب ما را دلیل عذر نیست</p></div>
<div class="m2"><p>منزلی‌کوتا نباید سر به پای لنگ ماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منت سیقل مکش‌، دردسر اوهام چند</p></div>
<div class="m2"><p>عکس معدوم است اگر آیینه ا‌ت در زنگ ماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آخر از سعی ضعیفی پیکر فرسوده‌ام</p></div>
<div class="m2"><p>همچو اخگر زیر دیوار شکست رنگ ماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست تکلیف تپیدنهای هستی در عدم</p></div>
<div class="m2"><p>آرمیدن مفت آن سازی‌که بی‌آهنگ ماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نام را نقش نگینها بال پرواز رساست</p></div>
<div class="m2"><p>ما ز خود رفتیم اگر پای طلب در سنگ ماند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکقدم ناکرده بیدل قطع راه آرزو</p></div>
<div class="m2"><p>منزل آسودگی ازما به صد فرسنگ ماند</p></div></div>