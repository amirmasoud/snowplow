---
title: >-
    غزل شمارهٔ ۹۷۵
---
# غزل شمارهٔ ۹۷۵

<div class="b" id="bn1"><div class="m1"><p>رگ گل آستین شوخی کمین صید ما دارد</p></div>
<div class="m2"><p>که زیر سنگ دست از سایهٔ برگ حنا دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر در عرض خویش آیینه‌ام عاریست معذورم</p></div>
<div class="m2"><p>که عمری شد خیال او مرا از من جدا دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگردد سابهٔ بال هما دام فریب من</p></div>
<div class="m2"><p>هنوزم استخوان جوهر ز نقش بوریا دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به رنگ سایه‌ام عبرت‌نمای چشم مغروران</p></div>
<div class="m2"><p>مرا هر کس ‌که می‌بیند نگاهی زیر پا دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمی‌باشد ز هم ممتاز نقصان و کمال اینجا</p></div>
<div class="m2"><p>خط پرگار در هر ابتدایی انتها دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حیات جاودان خواهی‌گداز عشق حاصل‌کن</p></div>
<div class="m2"><p>که دل در خون شدن خاصیت آب بقا دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به عبرت چشم خواهی واکنی نظارهٔ ما کن</p></div>
<div class="m2"><p>غبار خاکساران آبروی توتیا دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به دل تا گرد امیدی‌ست از ذوق طلب مگسل</p></div>
<div class="m2"><p>جهانی را گدا در سایهٔ دست دعا دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر موجیم یا بحریم اگر آبیم یا گوهر</p></div>
<div class="m2"><p>دویی نقشی نمی‌بندد که ما را از تو وا دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به فکر اضطراب موج کم می‌باید افتادن</p></div>
<div class="m2"><p>تپش در طینت ما خیر باد مدعا دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من ‌و تاب‌ وصال و طاقت دوری چه حرفست این</p></div>
<div class="m2"><p>اسیری‌راکه عشقت خواند بیدل دل‌کجا دارد</p></div></div>