---
title: >-
    غزل شمارهٔ ۶۲۲
---
# غزل شمارهٔ ۶۲۲

<div class="b" id="bn1"><div class="m1"><p>به‌گلزاری‌که حسنت بی‌نقابست</p></div>
<div class="m2"><p>خزان در برگریز آفتابست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زشرم یک عرق‌گل‌کردن حسن</p></div>
<div class="m2"><p>چو شبنم صد هزار آیینه آبست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جنون ساغرپرست نرگس‌کیست</p></div>
<div class="m2"><p>گریبان چاکی‌ام موج شرابست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دود سینه‌ام دریاب کامشب</p></div>
<div class="m2"><p>نفس بال و پر مرغ‌کبابست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که دارد جوهر عرض اقامت</p></div>
<div class="m2"><p>فلک تا ماه نوپا در رکابست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>توهم مردهٔ نام است ورنه</p></div>
<div class="m2"><p>چویاقوت آتش وآبم سرابست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درین دنیا چه دیبا و چه مخمل</p></div>
<div class="m2"><p>همین وضع ملایم فرش خوابست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به چشم خلق بی (‌لاحول‌) مگذر</p></div>
<div class="m2"><p>نظرها یک قلم مد شهابست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طرب خواهی دل از مطلب بپرداز</p></div>
<div class="m2"><p>کتان چون شسته‌گردد ماهتابست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برو ای سایه در خورشیدگم شو</p></div>
<div class="m2"><p>سیاهی‌کردنت داغ حجابست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نظر واکرده‌ای محو ادب باش</p></div>
<div class="m2"><p>سؤال جلوه حیرانی جوابست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به هر سو بگذری سیر نفس‌کن</p></div>
<div class="m2"><p>همین سطر از پریشانی کتابست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نگه باید به چشم بسته خواباند</p></div>
<div class="m2"><p>گر این خط نقطه گردد انتخابست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خیال اندیش دیداریم بیدل</p></div>
<div class="m2"><p>شب ما دلنشین آفتابست</p></div></div>