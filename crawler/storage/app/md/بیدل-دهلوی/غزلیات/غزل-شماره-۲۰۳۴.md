---
title: >-
    غزل شمارهٔ ۲۰۳۴
---
# غزل شمارهٔ ۲۰۳۴

<div class="b" id="bn1"><div class="m1"><p>چون آینه چندان به برش تنگ گرفتم</p></div>
<div class="m2"><p>کز خویش برون آمدم و رنگ گرفتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نامی که ندارم هوس نقش نگین داشت</p></div>
<div class="m2"><p>دامان خیالی به ته سنگ‌ گرفتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عجز طلبم گشت عنان تاب نگاهش</p></div>
<div class="m2"><p>ره بر رم آهو ز تک لنگ گرفتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون غنچه شبم لخت دلی در نظر آمد</p></div>
<div class="m2"><p>دامان تو پنداشتم و تنگ گرفتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خلقی در ناموس زد و داغ جنون برد</p></div>
<div class="m2"><p>من نیزگرفتم‌که ره ننگ‌گرفتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خجلت‌کش خودسازی‌ام از خودشکنیها</p></div>
<div class="m2"><p>نگشوده در صلح و ره جنگ‌ گرفتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چرخ نسنجید به میزان وقارم</p></div>
<div class="m2"><p>من نیز به همت‌ کم این سنگ‌ گرفتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در ترک تعلق چقدر ناز و غنا بود</p></div>
<div class="m2"><p>بر هر چه هوس پای زد اورنگ‌ گرفتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تاگرم‌کنم بستر امنی‌که ندارم</p></div>
<div class="m2"><p>چون صبح نفس زیر پررنگ‌گرفتم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل نفس آخر ورق آینه‌ گرداند</p></div>
<div class="m2"><p>سیلی به تجرد زدم و رنگ‌ گرفتم</p></div></div>