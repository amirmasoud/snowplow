---
title: >-
    غزل شمارهٔ ۱۳۵۳
---
# غزل شمارهٔ ۱۳۵۳

<div class="b" id="bn1"><div class="m1"><p>ای بهار پرفشان دل برگل و سنبل مبند</p></div>
<div class="m2"><p>آشیان جز در فضای نالهٔ بلبل مبند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوق آزادی تعلق اختراع وهم تست</p></div>
<div class="m2"><p>از خیال پوچ چون قمری به‌گردن غل مبند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجمع دلها تغافلخانهٔ ابرو بس است</p></div>
<div class="m2"><p>غافل از شور قیامت بر قفا کاکل مبند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بزم‌خاموشی‌ست از پاس‌نفس غافل مباش</p></div>
<div class="m2"><p>بر پر پروانه تشویش چراغ گل مبند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دورگردونت صلاها سزندکای بیخبر</p></div>
<div class="m2"><p>تا نفس داری ز گردش پای جام مل مبند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرگذشت‌عبرت‌مجنون‌هنوز افسانه‌نیست</p></div>
<div class="m2"><p>محشر آسوده‌ست بر زنجیر ما غلغل مبند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زندگی تاکی کشد رنج تک و تاز هوس</p></div>
<div class="m2"><p>پشت‌ خر ریش ‌است ای ‌گاو از تکلف جل ‌مبند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از شکست موج آزاد است استغنای بحر</p></div>
<div class="m2"><p>تهمت نقصان اجزا بر کمال ‌کل مبند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست بی‌آرایش عشاق استعداد شوق</p></div>
<div class="m2"><p>موی سرکافیست بر دستار مجنون‌ گل مبند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا دم حاجت مبادا بگذری از آبرو</p></div>
<div class="m2"><p>اندکی آگاه باش از چشم بستن پل مبند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پیری و لاف جوانی بیدل آخر شرم دار</p></div>
<div class="m2"><p>شیشه چون‌ شد سرنگون‌ جز بر عرق‌ قلقل‌ مبند</p></div></div>