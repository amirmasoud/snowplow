---
title: >-
    غزل شمارهٔ ۶۲۹
---
# غزل شمارهٔ ۶۲۹

<div class="b" id="bn1"><div class="m1"><p>نقاش ازل تا کمر مو کمران بست</p></div>
<div class="m2"><p>تصویر میانت به همان موی میان بست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از غیرت نازست ‌که آن حسن جهانتاب</p></div>
<div class="m2"><p>واگرد نقاب ازرخ و برچشم جهان بست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهرت‌طلبان‌! غرهٔ اقبال مباشید</p></div>
<div class="m2"><p>سرهاست در اینجا که بلندی به سنان بست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سامان ‌کمال آن همه بر خویش مچینید</p></div>
<div class="m2"><p>انبوهی هر جنس‌که دیدیم دکان بست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منسوب ‌کجان معتمد امن نشاید</p></div>
<div class="m2"><p>زآن تیر بیندیش‌که خود را به‌کمان ‌بست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترک طلب روزی از آدم چه خیال است</p></div>
<div class="m2"><p>گندم نتوانست لب از حسرت نان بست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مردیم وزتشویش تعلق نگسستیم</p></div>
<div class="m2"><p>بر آدم بیچاره که افسار خران بست‌؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون سبحه جهانی‌به نفس‌کلفت دل چید</p></div>
<div class="m2"><p>هرجاگرهی بود براین رشته میان بست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر موج در این بحر هوسگاه حبابی‌ست</p></div>
<div class="m2"><p>پنسان همه‌کس دل به جهان‌گذران بست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کس محرم فریاد نفس‌سوختگان نیست</p></div>
<div class="m2"><p>شمع از چه درین بزم به هر عضو زبان بست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عمری‌ست ز هر کوچه بلند است غبارم</p></div>
<div class="m2"><p>بیداد نگاه‌ که بر این سرمه فغان بست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل همه تن عبرتم از کلفت هستی</p></div>
<div class="m2"><p>جز چشم ز تصویر غبارم نتوان بست</p></div></div>