---
title: >-
    غزل شمارهٔ ۲۲۳۳
---
# غزل شمارهٔ ۲۲۳۳

<div class="b" id="bn1"><div class="m1"><p>دو روزی گو به خون گل کرده باشد چشم نمناکم</p></div>
<div class="m2"><p>تری تا گم شد از خاکم ز هر آلودگی پاکم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گزند هستی باطل علاجی نیست جز مرگش</p></div>
<div class="m2"><p>ز بی‌تاثیری اقبال سم گل کرده تریاکم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هوا تازی به خاک ذلتم پامال می‌دارد</p></div>
<div class="m2"><p>اگر سوی‌گریبان روکنم سرکوب افلاکم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز صد مستی قناعت کرده‌ام با یاد مژگانی</p></div>
<div class="m2"><p>دماغ‌گردن مینا بلند است از رگ تاکم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مزار کشتهٔ تیغ تبسم عالمی دارد</p></div>
<div class="m2"><p>سحر خندد غباری هم اگر برخیزد از خاکم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پرافشان می‌روم چون صبح ممکن نیست آزادی</p></div>
<div class="m2"><p>چه سازم ار قفس فرسوده‌های سینهٔ چاکم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بی‌دندانی ایام پیری نعمتم این بس</p></div>
<div class="m2"><p>که فارغ دارد از فکر و خیال رنج مسواکم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طلسمی بسته‌ام چول شمع‌ کو خلوت کجا محفل</p></div>
<div class="m2"><p>ز رویم رنگ اگر شویند هستی تا عدم پاکم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کمند کس حریف صید آزادم نمی‌گردد</p></div>
<div class="m2"><p>امل‌ها رشته درگردن‌ کم است از سعی فتراکم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر رنگم پرافشانم اگر بومست جولانم</p></div>
<div class="m2"><p>به هر صورت فضولی دستگاه طبع بیباکم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نمی‌سوزم نفس بیهوده در تدبیر جمعیت</p></div>
<div class="m2"><p>دم فرصت‌کسل دارم منش ناچار دلاکم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به حرف و صوت این محمل ندارم نسبتی بیدل</p></div>
<div class="m2"><p>خموشی‌کرده‌ام روشن چراغ‌ کنج ادراکم</p></div></div>