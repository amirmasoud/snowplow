---
title: >-
    غزل شمارهٔ ۵۶۱
---
# غزل شمارهٔ ۵۶۱

<div class="b" id="bn1"><div class="m1"><p>آفت سر و برگ هوس آرایی جاه است</p></div>
<div class="m2"><p>سر باختن شمع ز سامان‌کلاه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غافل مشو از فیض سیه‌روزی عشاق</p></div>
<div class="m2"><p>نیل شب ما غازه‌کش چهرهٔ ماه است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با حسن تو آسان نتوان‌گشت مقابل</p></div>
<div class="m2"><p>حیرت چقدر آینه را پشت و پناه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک چشم تر آورده‌ام از قلزم حیرت</p></div>
<div class="m2"><p>این‌کشتی آیینه پر از جنس نگاه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>افسوس‌که در غنچه و بو فرق نکردم</p></div>
<div class="m2"><p>دل رفت و من دلشده پنداشتم آه است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا هست نفس رنگ به رویم نتوان‌یافت</p></div>
<div class="m2"><p>تحریک هوا بال و پر وحشت کاه است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کو خجلت عصیان‌که محیط‌کرمش را</p></div>
<div class="m2"><p>آرایش موج، از عرق شرم‌گناه است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان جلوه به خود ساخت جهانی چه توان‌کرد</p></div>
<div class="m2"><p>شب پرتو خورشید در آیینهٔ ماه است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جزسازنفس غفلت دل را سببی نیست</p></div>
<div class="m2"><p>این خانه چو داغ از اثر دود سیاه است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آنجاکه تکبرمنشان ناز فروشند</p></div>
<div class="m2"><p>ماییم و شکستی که سزاوارکلاه است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرچند جهان وسعت یک‌گام ندارد</p></div>
<div class="m2"><p>اما اگر از خویش برآیی همه راه است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زندان جسد منظر قرب صمدی نیست</p></div>
<div class="m2"><p>معراج خیالی تو و ره در بن چاه است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از جلوه‌کسی ننگ تغافل نپسندد</p></div>
<div class="m2"><p>بیدل مژه بر هم زدنت عجز نگاه است</p></div></div>