---
title: >-
    غزل شمارهٔ ۱۳۴۱
---
# غزل شمارهٔ ۱۳۴۱

<div class="b" id="bn1"><div class="m1"><p>لاله و گل چشمک رمز خوان فهمیده‌اند</p></div>
<div class="m2"><p>زعفرانی هست کاین‌ها بر وفا خندیده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین گلستانم به گوش آواز دردی می رسد</p></div>
<div class="m2"><p>رنگ و بویی نیست اینجا بلبلان نالیده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برغرور فرصت ما تا کجا خندد شباب</p></div>
<div class="m2"><p>آسیاها نیز اینجا رنگ گردانیده‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرنگونی با همه نشو و نما از ما برفت</p></div>
<div class="m2"><p>ناتوانان همچو مو پر منفعل بالیده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به که غلتانی نخواند برگهر افسون ناز</p></div>
<div class="m2"><p>موجها بیتاب بودند این دم آرامیده‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواه برگردون سحر شو خواه در دریا حباب</p></div>
<div class="m2"><p>در ترازوی نفس جز باد کم سنجیده‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منکر وضع ندامت غافلست از ساز عیش</p></div>
<div class="m2"><p>دست‌ها اینجا دو برگ ‌گل به هم ساییده‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست تدبیر وداع درد سر کار کمی</p></div>
<div class="m2"><p>بی‌تمیزان عقل‌کامل را جنون نامیده‌اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کل شوی تا دورگردون محرم عدلت‌ کند</p></div>
<div class="m2"><p>جزوها یکسر خط پرگار را کج دیده‌اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از ادب تا یاد آن نرگس نچیند انفعال</p></div>
<div class="m2"><p>خانهٔ بیمار را دارالشفا نامیده‌اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حیرتی را مغتنم‌گیریدو عشرتها کنید</p></div>
<div class="m2"><p>محرمان از صد بهار رنگ یک‌ گل چیده‌اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پیش هر نقش قدم ما را سجودی بردن است</p></div>
<div class="m2"><p>کاین به خاک‌افتادگان پای کسی بوسیده‌اند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بی‌ادب بی دل به خاک نرگسستان نگذری</p></div>
<div class="m2"><p>شرمناکان با هم آنجا یک مژه خوابیده‌اند</p></div></div>