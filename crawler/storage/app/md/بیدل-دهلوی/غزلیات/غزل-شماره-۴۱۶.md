---
title: >-
    غزل شمارهٔ ۴۱۶
---
# غزل شمارهٔ ۴۱۶

<div class="b" id="bn1"><div class="m1"><p>چه خوش است اگر بود آنقدر هوس بلندی منظرت</p></div>
<div class="m2"><p>که برآن‌مکان چو قدم نهی خم‌گردشی نخورد سرت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دو روزه مهلت این قفس دلت آشیانهٔ صد هوس</p></div>
<div class="m2"><p>نه‌ای آگه از تپش نفس‌که چه بیضه می‌شکند پرت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه‌راست جادهٔ پیچشی همه راست خجلت‌گردشی</p></div>
<div class="m2"><p>توچنان مروکه ز لغزشی به‌کجی زند خط مسطرت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چوگل از طبیعت بی‌نشان به خیال دشتی آشیان</p></div>
<div class="m2"><p>به برهنگی زدی این زمان‌که دمید پیرهن از برت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو حباب غیرلباس تو چه توقع وچه هراس تو</p></div>
<div class="m2"><p>نه تو مانی و نه قیاس تو، چوکشند جامه زپیکرت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه عروج نغمهٔ قدرتی‌، نه دماغ نشئهٔ فطرتی</p></div>
<div class="m2"><p>چو غباز واعظ عبرتی و هواست پایهٔ منبرت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دماغ افشرهٔ عنب مپسند این همه تاب وتب</p></div>
<div class="m2"><p>که ز سیر انجمن ادب فکند به عالم دیگرت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زفسون مطرب و چنگ آن‌، مکش آنقدر اثر فغان</p></div>
<div class="m2"><p>که به فهم نالهٔ عاجزان‌کند التفات هوس‌گرت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غم قدر بیهده خوردنی همه سکته دارد و مردنی</p></div>
<div class="m2"><p>حذر از بلای فسردنی‌که رسد ز منصب‌گوهرت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طلبی‌گرازتوبه جا رسد، به سر اوفتد چو به پا رسد</p></div>
<div class="m2"><p>سرآرزوبه‌کجا رسد زدماغ آبله ساغرت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز سواد نسخهٔ خشک وتربه‌کلام بیدل ما نگر</p></div>
<div class="m2"><p>که به حیرت چمن اثر، شود آب آینه رهبرت</p></div></div>