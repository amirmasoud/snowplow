---
title: >-
    غزل شمارهٔ ۵۱۸
---
# غزل شمارهٔ ۵۱۸

<div class="b" id="bn1"><div class="m1"><p>در وصلم و سیرم به‌گریبان خیال است</p></div>
<div class="m2"><p>چون آینه پرواز نگاهم ته بال است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیقدری دل نیست جزآهنگ غرورش</p></div>
<div class="m2"><p>تا چینی ما خاک نگشته‌ست سفال است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سایل به‌کف اهل‌کرم‌گر به غلط هم</p></div>
<div class="m2"><p>چشمی بگشاید لب صد رنگ سوال است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بیخبری چندکنی فخر لباسی</p></div>
<div class="m2"><p>پشمی‌ست‌که‌بر دوش‌تو درکسوت‌شال‌است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از مایدهٔ بی‌نمک حرص مپرسید</p></div>
<div class="m2"><p>چیزی‌که به‌جز غصه توان خورد محال است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهدی‌که زکلفتکدهٔ جسم برآیی</p></div>
<div class="m2"><p>هر دانه‌که ازخاک برون جست نهال است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگداز به رنگی‌که پری داغ توگردد</p></div>
<div class="m2"><p>چون‌سنگ اگر شیشه‌برآیی چه‌کمال است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر جلوهٔ اسباب توهم نفروشی</p></div>
<div class="m2"><p>دیوار و در خانهٔ خورشید خیال است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لعل توبه بزمی‌که دهد عرض تبسم</p></div>
<div class="m2"><p>موج‌گهر آنجا شکن چهرهٔ زال است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زین مایده یک لقمه‌گوارا نتوان یافت</p></div>
<div class="m2"><p>نعمت همه دندان زدهٔ رنج خلال است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل دل ما با چه شهود است مقابل</p></div>
<div class="m2"><p>نقشی‌که درین پرده ببستیم خیال است</p></div></div>