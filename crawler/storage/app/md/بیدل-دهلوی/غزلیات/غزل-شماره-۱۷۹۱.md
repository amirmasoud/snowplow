---
title: >-
    غزل شمارهٔ ۱۷۹۱
---
# غزل شمارهٔ ۱۷۹۱

<div class="b" id="bn1"><div class="m1"><p>که دارد جوهر تحقیق حسرتگاه ناموسش</p></div>
<div class="m2"><p>جهانتاب است شمع و بیضهٔ عنقاست فانوسش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تبسم ریز صبحی رفت از گلشن‌ که تا محشر</p></div>
<div class="m2"><p>به هر سو غنچه‌ها لب می‌کند از حسرت بوسش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیال عشق چندان شست اوراق دلایل را</p></div>
<div class="m2"><p>که در آیینه نتوان یافتن تمثال جاسوسش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نوید وصل آهنگی‌ست وقف ساز نومیدی</p></div>
<div class="m2"><p>اگر دل بشکند زین نغمه نگذارند مأیوسش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درین محفل به هر جا شیشهٔ ما سرنگون‌ گردد</p></div>
<div class="m2"><p>خم طاق شکست دل نماید جای پا بوسش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکستم در تمنای بهارت شیشهٔ رنگی</p></div>
<div class="m2"><p>که هر جا می‌رسم پر می‌زند آواز طاووسش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان یکسر حقست‌، آری مقیّد مطلق است اینجا</p></div>
<div class="m2"><p>ز مینا هر که آگه شد پری ‌گردید محسوسش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز دیرستان عشقت در جگر جوش تبی دارم</p></div>
<div class="m2"><p>که از تبخاله می‌باید شنیدن بانگ ناقوسش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دگر می‌تاختم با ناز در جولانگه فطرت</p></div>
<div class="m2"><p>به این خجلت عرق ‌کردم ‌که نم زد پوست بر کوسش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زمان فرصت دیدار رفت اما من غافل</p></div>
<div class="m2"><p>به وهم آیینه صیقل می‌زنم از دست افسوسش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به آزادی پری می‌زد نفس در باغ ما بیدل</p></div>
<div class="m2"><p>تخیل‌ گشت زندانش توهُم‌ کرد محبوسش</p></div></div>