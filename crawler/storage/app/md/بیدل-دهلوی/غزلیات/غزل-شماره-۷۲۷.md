---
title: >-
    غزل شمارهٔ ۷۲۷
---
# غزل شمارهٔ ۷۲۷

<div class="b" id="bn1"><div class="m1"><p>دل گرم من آتشخانهٔ‌کیست</p></div>
<div class="m2"><p>نگاه حسرتم پروانهٔ کیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خط جام است امشب رهزن هوش</p></div>
<div class="m2"><p>خیال نرگس م‌ستانهٔ ‌کیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار آیینه روز خویش شب کرد</p></div>
<div class="m2"><p>صفا مهتاب فرش خانهٔ‌کیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امل در مزرع ما ره ندارد</p></div>
<div class="m2"><p>فسون ریشه‌، دام و دانهٔ ‌کیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر تیغت ند‌‌ارد می‌پرستی</p></div>
<div class="m2"><p>لب زخم خط پیمانهٔ‌کیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز چاک دل نواها می‌تراود</p></div>
<div class="m2"><p>که می‌فهمد زبان شانهٔ‌کیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیرزیدم به تعمیر خیالی</p></div>
<div class="m2"><p>غبارم یارب از ف‌برانهٔ کیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رک گا نالهٔ زنجیر درد</p></div>
<div class="m2"><p>چمن جولانگه دیوانهٔ کیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سپند آهی ‌کشید و چشم پوشید</p></div>
<div class="m2"><p>به‌ا‌ین تکلیف خواب افسانهٔ ‌کیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شرارم ناز خواهد کرد خرمن</p></div>
<div class="m2"><p>برون از ریشه جستن دانهٔ ‌کیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به ذوق بیخودی مردیم بیدل</p></div>
<div class="m2"><p>شکست‌رنگ، ‌صورت‌خانهٔ کیست</p></div></div>