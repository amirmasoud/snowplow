---
title: >-
    غزل شمارهٔ ۴۵۷
---
# غزل شمارهٔ ۴۵۷

<div class="b" id="bn1"><div class="m1"><p>یا رب امشب آن جنون آشوب جان و دل ‌کجاست</p></div>
<div class="m2"><p>آن خرام نازکو، آن عمر مستعجل کجاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زورقی دارم‌، به غارت رفتهٔ توفان‌ یاس</p></div>
<div class="m2"><p>جز کنار الفت آغوشش دگر ساحل کجاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا به‌س تهمت نصیب داغ حرمان زیستن</p></div>
<div class="m2"><p>آن شررخویی‌که می‌زد آتشم در دل‌کجاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جنس آثار قدم آنگه به بازار حدوث</p></div>
<div class="m2"><p>پرتو شمعی‌cکه من دارم درین محفل ‌کجاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تپیدن های دل عمریست می آید به ‌گوش</p></div>
<div class="m2"><p>کای حریفان آشیان راحت بسمل‌کجاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غیر جو افتاده‌ای‌، ای غافل از خود شرم دار</p></div>
<div class="m2"><p>جز فضولیهای تو ‌در ملک حق باطل کجاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آبیاریهای حرص اوهام خرمن می‌کند</p></div>
<div class="m2"><p>هرکجاکشتی نباشد جلوه‌گر حاصل‌کجاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون نفس عمریست در لغزش قدم افشرده‌ایم</p></div>
<div class="m2"><p>دل اگر دامن نگیرد در ره ما گل‌کجاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی‌نقابی برنمی‌دارد ادبگاه وفا</p></div>
<div class="m2"><p>شرم لیلی گر نپوشد چشم ما محمل کجاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>احتیاج ما تماشاخانهٔ اکرام اوست</p></div>
<div class="m2"><p>رمز استغنا تبسم می‌کند سایل‌ کجاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>معنی ایجادیم از نیرنگ مشتاقان مپرس</p></div>
<div class="m2"><p>خون ما رنگ حنا داردکف قاتل‌کجاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شب به ذوق جستجوی خود در دل می‌زدم</p></div>
<div class="m2"><p>عشق‌گفت‌: این جا همین‌ ماییم‌ و بس بیدل کجاست</p></div></div>