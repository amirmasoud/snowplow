---
title: >-
    غزل شمارهٔ ۸۵۹
---
# غزل شمارهٔ ۸۵۹

<div class="b" id="bn1"><div class="m1"><p>عمرگذشته بر مژه‌ام اشک بست و رفت</p></div>
<div class="m2"><p>پرواز صبح‌، بیضهٔ شبنم شکست و رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خود تهی شوید و ز اوهام بگذرید</p></div>
<div class="m2"><p>خلقی درین محیط به‌ کشتی نشست و رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از نقد و جنس حاصل این‌کارگاه وهم</p></div>
<div class="m2"><p>دیدیم باد بودکه آمد به دست و رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رفتن قیامتی‌ست‌که پا لغز کس مباد</p></div>
<div class="m2"><p>هرچند حق‌پرست‌، شد اتش‌پرست و رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پوشیده نیست رسم خرابات ما و من</p></div>
<div class="m2"><p>هرکس بهٔک‌دو جام نفس‌گشت‌مست و رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در سینه داشتم دلکی عاقبت نماند</p></div>
<div class="m2"><p>آه این سپند سوخته با ناله جست و رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بند کشاکش نفس آخر گسیخت عمر</p></div>
<div class="m2"><p>با خویش برد ماهی پر زور شست و رفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشم گشوده وحشت دل را بهانه بود</p></div>
<div class="m2"><p>شاهین بی‌تماغه رها شد ز دست و ‌رفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کس محرم پیام دم واپسین نشد</p></div>
<div class="m2"><p>کز دل چه مژده داد به دل پست پسب‌ورفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شمعی زبان موعظت بزم‌ گرم داشت</p></div>
<div class="m2"><p>گفتم چسان روم ز در دل نشست و رفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل غبار قافلهٔ اعتبار ما</p></div>
<div class="m2"><p>باری دگر نداشت همین چشم بست و رفت</p></div></div>