---
title: >-
    غزل شمارهٔ ۱۵۲۳
---
# غزل شمارهٔ ۱۵۲۳

<div class="b" id="bn1"><div class="m1"><p>شبنم صبح از چمن آبله دل می‌رود</p></div>
<div class="m2"><p>عیش عرق می‌کند خنده خجل می‌رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مخمصهٔ زندگی فرصت ماکرد تنگ</p></div>
<div class="m2"><p>عیش والم هیچ نیست عمر مخل می‌رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زبن همه نشو و نما منفعل است اصل ما</p></div>
<div class="m2"><p>درخور شاخ بلند ربشه به‌گل می‌رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تک به هوا می زند خلق زحرص بگیر</p></div>
<div class="m2"><p>گرجه به دوش نفس‌*‌رد بهل می‌رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرچه دمد زین بهار نشئهٔ آفت شمار</p></div>
<div class="m2"><p>در رگ گل آب نیست خون بحل می‌رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رنج و الم هم نداد داد ثباتی‌که نیست</p></div>
<div class="m2"><p>زین مرض‌آباد یأس دق شد و سل می‌رود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرصت‌کار نفس مغتنم غفلت است</p></div>
<div class="m2"><p>آمده در یاد نیست رفته ز دل می‌رود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیدل ازین رنگ وبو غنچهٔ دل جمع نیست</p></div>
<div class="m2"><p>قافلهٔ اتفاق ربط گسل می‌رود</p></div></div>