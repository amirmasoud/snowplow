---
title: >-
    غزل شمارهٔ ۱۳۵۱
---
# غزل شمارهٔ ۱۳۵۱

<div class="b" id="bn1"><div class="m1"><p>محرم آهنگ دل شو سرمه بر آواز بند</p></div>
<div class="m2"><p>یک نفس از خامشی هم رشته‌ای بر ساز بند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود گدازی‌ کعبهٔ مقصود دارد در بغل</p></div>
<div class="m2"><p>کم ز آتش نیستی احرام این انداز بند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاقبت بینی نظر پوشیدن است از عیب خلق</p></div>
<div class="m2"><p>آنچه در انجام خواهی بستن از آغاز بند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست غیر از خاکساری پرده‌دار راز عشق</p></div>
<div class="m2"><p>گرتوانی مشت خاکی شو لب غماز بند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با خراش قلب‌، ممنون صفا نتوان شدن</p></div>
<div class="m2"><p>خون شو ای آیینه راه منت‌پرداز بند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>موج می‌باشدکلید قفل وسواس حباب</p></div>
<div class="m2"><p>عقدهٔ دل وانمی‌گردد به تار ساز بند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ننگ آزادی‌ست بر وهم نفس دل بستنت</p></div>
<div class="m2"><p>این‌گره را همچو اشک از رشته بیرون تاز بند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان لب خاموش شور دل‌گریبان می‌درد</p></div>
<div class="m2"><p>حیف باشد غنچه‌ها را بر قبای ناز بند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناله می‌گویند پروازش به جایی می‌رسد</p></div>
<div class="m2"><p>ای اثر مکتوب ما بر شعلهٔ آواز بند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دستگاه ما و من بر باد حسرت رفته‌ گیر</p></div>
<div class="m2"><p>هرچه می‌بندی به خود چون رنگ بر پرواز بند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل این‌جا یأس مطلب فتح باب مدعاست</p></div>
<div class="m2"><p>از شکست دل‌گشادی بر طلسم راز بند</p></div></div>