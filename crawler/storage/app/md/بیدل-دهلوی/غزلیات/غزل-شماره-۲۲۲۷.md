---
title: >-
    غزل شمارهٔ ۲۲۲۷
---
# غزل شمارهٔ ۲۲۲۷

<div class="b" id="bn1"><div class="m1"><p>غبار عجز پروازی مقیم دامن خویشم</p></div>
<div class="m2"><p>شکست خویش چون موج ‌است هم بر گردن خویشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین مزرع‌ که جز بیحاصلی تخمی نمی‌بندد</p></div>
<div class="m2"><p>نمی‌دانم هجوم آفتم یا خرمن خویشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سراغ رنگ هستی در طلسم خود نمی‌یابم</p></div>
<div class="m2"><p>درین محفل چو شمع کشته داغ رفتن خویشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شبستان دارد از پرواز رنگ شمع طاووسم</p></div>
<div class="m2"><p>بهار این بساطم کز خزان گلشن خویشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو رنگ گل به شاخ برگ تحقیقم که می‌پیچد</p></div>
<div class="m2"><p>که من صد پیرهن عریانتر از پیراهن خویشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درتن وادی ندارد عافیت‌گرد «‌اناالعشقی‌».</p></div>
<div class="m2"><p>اگر آتش زنم در خویش نخل ایمن خویشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو مژگانم ز وضع خویش باید سرنگون بودن</p></div>
<div class="m2"><p>بضاعت هیچ و من مغرور دست افشاندن خویشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه مقدار آب گردد صبح تا شبنم به عرض آید</p></div>
<div class="m2"><p>به این عجز نفس حیران مضمون بستن خویشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو شمع از ضعف آغوش وداعم در قفس دارد</p></div>
<div class="m2"><p>شکست رنگ بر هم چیدهٔ پیراهن خویشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تظلم هرزه تازی داشت در صحرای نومیدی</p></div>
<div class="m2"><p>ضعیفی داد آخر یاد دست و دامن خویشم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جهان را صید حیرت کرد جوش ناله‌ام بیدل</p></div>
<div class="m2"><p>همه زنجیرم اما در نقاب شیون خویشم</p></div></div>