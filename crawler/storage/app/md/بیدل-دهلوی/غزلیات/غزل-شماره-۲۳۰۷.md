---
title: >-
    غزل شمارهٔ ۲۳۰۷
---
# غزل شمارهٔ ۲۳۰۷

<div class="b" id="bn1"><div class="m1"><p>ای نرگست حیاکدهٔ صلح و جنگ هم</p></div>
<div class="m2"><p>ساز غزال رام تو خشم پلنگ هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دنباله‌های ابروت از دل گذشته است</p></div>
<div class="m2"><p>می‌آید ازکمان توکار خدنگ هم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تنها نه دف ز حلقه به‌گوشان بزم تست</p></div>
<div class="m2"><p>دارد سری به فکر سجود تو چنگ هم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رنگینی لباس چه مقدار دلکش است</p></div>
<div class="m2"><p>گل‌کرده است این هوس از طبع سنگ هم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آگهی به مغز خرد جمع‌کرده‌ایم</p></div>
<div class="m2"><p>کیفیتی‌ که نیست در اوهام ننگ هم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زانو زدن ز خصم مپندار عاجزیست</p></div>
<div class="m2"><p>پیداست این ادا دم‌ کین از تفنگ هم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای خستت عقوبت جاوید، هوش‌دار</p></div>
<div class="m2"><p>بدتر ز قبر می‌فشرد جسم تنگ هم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>راهیست راه عمرکه خود قطع می‌شود</p></div>
<div class="m2"><p>وصل فنا شتاب ندارد درنگ هم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عجزیست در مزاج تحیر سرشت من</p></div>
<div class="m2"><p>کز خویش رفتنم نشکسته‌ست رنگ هم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درکارگاه عشق سلامت چه می‌کند</p></div>
<div class="m2"><p>اینجا به طبع شیشه خزیده‌ست سنگ هم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بی‌الفت لباس ز عریان تنی چه باک</p></div>
<div class="m2"><p>جنس دکان فخرپرستی‌ست ننگ هم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل مباد منکر جام تهی شوی</p></div>
<div class="m2"><p>دارد حضور قلقل مینا ترنگ هم</p></div></div>