---
title: >-
    غزل شمارهٔ ۱۹۴۸
---
# غزل شمارهٔ ۱۹۴۸

<div class="b" id="bn1"><div class="m1"><p>تا بست ادب نامهٔ من در پر بسمل</p></div>
<div class="m2"><p>پرواز گرفته‌ست شکن در پر بسمل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاد تب شوقی ‌که ز سامان تپیدن</p></div>
<div class="m2"><p>آسودگیم داشت سخن در پر بسمل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرصت هوس افتاد رم آهنگ شرارم</p></div>
<div class="m2"><p>طرز نو من گشت‌ کهن در پر بسمل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل محو شهادتگه نازیست که اینجا</p></div>
<div class="m2"><p>خون در رگ موجست و کفن در پر بسمل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای شوق ‌کرا نیست تپشهای محبت</p></div>
<div class="m2"><p>سرتا قدم من بشکن در پر بسمل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیتابی ساز نفس از دود خموشیست</p></div>
<div class="m2"><p>ای عافیت آتش مفکن در پر بسمل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شبگیر فنا هم چقدر داشت رسایی</p></div>
<div class="m2"><p>عمریست‌ که داریم وطن در پر بسمل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر جا دم تیغ تو گل افشان خیالیست</p></div>
<div class="m2"><p>فرشست چو طاووس چمن در پر بسمل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای راهروان منزل تحقیق بلندست</p></div>
<div class="m2"><p>باید قدمی چند زدن در پر بسمل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل هوس ‌آرایی پرواز که دارد</p></div>
<div class="m2"><p>محو است غبار تو و من در پر بسمل</p></div></div>