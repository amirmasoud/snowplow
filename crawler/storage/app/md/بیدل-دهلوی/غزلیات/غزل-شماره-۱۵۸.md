---
title: >-
    غزل شمارهٔ ۱۵۸
---
# غزل شمارهٔ ۱۵۸

<div class="b" id="bn1"><div class="m1"><p>بیاکه جام مروت دهیم حوصله را</p></div>
<div class="m2"><p>به سایهٔ کف پا پروریم آبله را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به وادیی که تعلق دلیل کوشش‌هاست</p></div>
<div class="m2"><p>ز بار دل به زمین خفته‌گیر قافله را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز صاحب امل آزادگی چه مکان است</p></div>
<div class="m2"><p>درین بساط‌گرانخیزی است حامله را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز انقلاب حوادث بزرگی ایمن نیست</p></div>
<div class="m2"><p>به طبع‌کوه اثر افزونتر است زلزله را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>محبت از من و تو رنگ امیتازگداخت</p></div>
<div class="m2"><p>تری و آب سزاوار نیست فاصله را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به‌کج ادایی حسن تغافلت نازم</p></div>
<div class="m2"><p>که یاد اوگلهٔ ناز می‌کندگله را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چوصبح یک دونفس مغتنم شمربیدل</p></div>
<div class="m2"><p>مکن دلیل اقامت چو زاهدان چله را</p></div></div>