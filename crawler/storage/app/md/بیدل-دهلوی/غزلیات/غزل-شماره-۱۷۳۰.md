---
title: >-
    غزل شمارهٔ ۱۷۳۰
---
# غزل شمارهٔ ۱۷۳۰

<div class="b" id="bn1"><div class="m1"><p>غبار ره شو و سرکوب صد حشم برخیز</p></div>
<div class="m2"><p>شه قلمرو فقری به این علم برخیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به فیض عام ز امید قطع نتوان ‌کرد</p></div>
<div class="m2"><p>زبخت خفته میندیش و صبحدم برخیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غبار دل به زمین نقش خواهدت بستن</p></div>
<div class="m2"><p>کنون که بار سر و دوش توست کم برخیز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرونشسته‌تر از جسم مرده است جهان</p></div>
<div class="m2"><p>دو روز گو به جنون جوشی ورم برخیز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز اغنیا به تواضع مباش غرهٔ امن</p></div>
<div class="m2"><p>چو اعتماد ز دیوارهای خم برخیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حریف معنی تحقیق بودن آسان نیست</p></div>
<div class="m2"><p>به سرنگونی جاوید چون قلم برخیز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شریک غفلت و آگاهی رفیقان باش</p></div>
<div class="m2"><p>به خواب چون مژه‌ها با هم و به هم برخیز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غبار هرزه‌دو دشت آفتی چه بلاست</p></div>
<div class="m2"><p>تو راکه گفت ز خاک ره عدم برخیز؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درای قافلهٔ صبح می‌دهد آواز</p></div>
<div class="m2"><p>که ای ستم‌زده رفتیم ما، تو هم برخیز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو شمع سیرگریبان عصای همت تست</p></div>
<div class="m2"><p>به خود فرو رو و از فرق تا قدم برخیز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در این ستمکده نومید خفته‌ای بیدل</p></div>
<div class="m2"><p>به آرزوی دلت می‌دهم قسم برخیز</p></div></div>