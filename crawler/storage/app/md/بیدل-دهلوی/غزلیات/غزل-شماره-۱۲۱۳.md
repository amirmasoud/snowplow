---
title: >-
    غزل شمارهٔ ۱۲۱۳
---
# غزل شمارهٔ ۱۲۱۳

<div class="b" id="bn1"><div class="m1"><p>لب بی‌صرفه نوا جهل سبق می‌باشد</p></div>
<div class="m2"><p>خامه شایان عرق در خور شق می‌باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با ادب باش که در انجمن یکتایی</p></div>
<div class="m2"><p>دعوی باطلت اندیشهٔ حق می‌باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلبلان قصه مخوانید که در مکتب عشق</p></div>
<div class="m2"><p>دفترگل پر پروانه ورق می‌باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرکجا غیرت حسن انجمن‌آرای حیاست</p></div>
<div class="m2"><p>خجلت از آینه‌داران عرق می‌باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در قناعت اگر ابرام نجوشد چو حباب</p></div>
<div class="m2"><p>سکتهٔ وضع رضا سد رمق می‌باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جوع و شهوت همه جا پرده در دلکوبی‌ست</p></div>
<div class="m2"><p>نغمهٔ دهر ز قانون نهق می‌باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خون ما مغتنم ‌گرد سر تمکین‌ گیر</p></div>
<div class="m2"><p>چترکوه از پر طاووس شفق می‌باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سنگ هم درکف اطفال ندارد آرام</p></div>
<div class="m2"><p>دور مجنون چقدر سست نسق می‌باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ورق جود کریمان جهان برگردید</p></div>
<div class="m2"><p>نان محتاج ‌کنون پشت طبق می‌باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بید‌ل از خلق جهان عشوهٔ خوبی نخوری</p></div>
<div class="m2"><p>غازهٔ چهرهٔ این قوم به حق می‌باشد</p></div></div>