---
title: >-
    غزل شمارهٔ ۳۳۷
---
# غزل شمارهٔ ۳۳۷

<div class="b" id="bn1"><div class="m1"><p>بس‌که دارد برق تیغت درگذشتنها شتاب</p></div>
<div class="m2"><p>رنگ نخجیر تو می‌گردد ز پهلوی‌کباب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناز اگرافسون نخواند مانع آن جلوه‌کیست</p></div>
<div class="m2"><p>در بنای وهم غیرآتش زن وبرخود بتاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جام نرگس‌گرمی شبنم به شوخی آورد</p></div>
<div class="m2"><p>پیش‌چشمت‌نیست‌غیر از حلقهٔ‌چشم‌پر آب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در مقامی کز تماشایت گدازد هستی‌ام</p></div>
<div class="m2"><p>عرض خجلت دارد ایجاد عرق از آفتاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>واصلان را سودها باشد ز اسباب زیان</p></div>
<div class="m2"><p>قوت پرواز می‌گیرد پر ماهی از آب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از نشان و نام ما بگذر، خیالی پخته‌ایم</p></div>
<div class="m2"><p>خاتم‌گرداب نقشی نیست غیر از پیچ و تاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در عدم بیکاری ما شغل هستی پیش برد</p></div>
<div class="m2"><p>صنعت اوهام‌کشتی راند در موج سراب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رفتم از خود آنقدرکان جلوه استقبال‌کرد</p></div>
<div class="m2"><p>گردش رنگم فکند آخر ز روی او نقاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ازگداز من عیار عشق می‌باید گرفت</p></div>
<div class="m2"><p>درعرق دارد جبین تا چشمهٔ خورشید، آب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حسن‌و عشقی نیست اینجا با چه‌پردازدکسی</p></div>
<div class="m2"><p>خانهٔ لیلی سیاه و وادی مجنون خراب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زندگی در قدر جمعیت نفهمیدن‌گذشت</p></div>
<div class="m2"><p>ای شعورت دورباش عافیت لختی بخواب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عالم معنی شدیم وداغ جهل ازما نرفت</p></div>
<div class="m2"><p>ساخت بیدل علمهای بی‌عمل ما راکتاب</p></div></div>