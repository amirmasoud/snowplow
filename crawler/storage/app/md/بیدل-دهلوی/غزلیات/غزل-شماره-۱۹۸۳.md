---
title: >-
    غزل شمارهٔ ۱۹۸۳
---
# غزل شمارهٔ ۱۹۸۳

<div class="b" id="bn1"><div class="m1"><p>بیخودی ننهفت اسرار دل غم پیشه‌ام</p></div>
<div class="m2"><p>بوی می آخر صدا شد از شکست شیشه‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیگ بحر از جوش ننشیند به سرپوش حباب</p></div>
<div class="m2"><p>مهر خاموشیست داغ شورش اندیشه‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در بن هر موی من چندین امل پر می‌زند</p></div>
<div class="m2"><p>همچو تخم عنکبوت از پای تا سر ریشه‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست تا آبی زند بر آتش بنیاد من</p></div>
<div class="m2"><p>گر نباشد خجلت شغل محبت پیشه‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عمرها شد در جنون زار طلب برده است پیش</p></div>
<div class="m2"><p>ناز چشم آهو از داغ پلنگان بیشه‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نفس در سینه می‌دزدم صلای جلوه‌ایست</p></div>
<div class="m2"><p>نیست غافل صورت شیرین ز عجز تیشه‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رنگ شمعی‌ کرده‌ام‌ گل از خرابات هوس</p></div>
<div class="m2"><p>باده می‌باید کشیدن در گداز شیشه‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با همه کمفرصتی از لنگر غفلت مپرس</p></div>
<div class="m2"><p>سنگ در طبع شرر می‌پرورد اندیشه‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناله‌ها ارکلفت دل در نقاب خاک ماند</p></div>
<div class="m2"><p>سوخت بیدل در غبار دانه سعی ریشه‌ام</p></div></div>