---
title: >-
    غزل شمارهٔ ۱۶۷۷
---
# غزل شمارهٔ ۱۶۷۷

<div class="b" id="bn1"><div class="m1"><p>بر خیالی چیده‌ایم از دیده تا دل انتظار</p></div>
<div class="m2"><p>لیلی این انجمن وهم است و محمل انتظار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا دل از امید غافل بود تشویشی نبود</p></div>
<div class="m2"><p>ساز استغنای ما را کرد باطل انتظار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکه را دیدیم فکری آنسوی تحقیق داشت</p></div>
<div class="m2"><p>بیکرانی رفت از این دریای ساحل انتظار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از هوس جز ناامیدی با چه پردازدکسی</p></div>
<div class="m2"><p>جست‌وجو آواره است و پای در گل انتظار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقش پا هر گامت آغوش دگر وامی‌کند</p></div>
<div class="m2"><p>ای طلب شرمی‌ که دارد چشم منزل انتظار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قطره‌ات دریاست گر از وهم گوهر بگذری</p></div>
<div class="m2"><p>عالمی را کرده است از وصل غافل انتظار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم واکردیم اما فرصت دیدار کو</p></div>
<div class="m2"><p>بر شرارکاغذ ما بست محمل انتظار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عمرها شد از توقع آبیار عبرتیم</p></div>
<div class="m2"><p>ریشهٔ‌ کشت امل خاک است و حاصل انتظار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر شبستان خیال وهم و ظن آتش زنید</p></div>
<div class="m2"><p>شمع خاموش است و می‌سوزد به محفل انتظار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وعدهٔ احسان به معنی ازگدایی نیست کم</p></div>
<div class="m2"><p>از کرم ظلم است اگر خواهد ز سایل انتظار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرده‌ایم اما همان صبح قیامت در نظر</p></div>
<div class="m2"><p>این‌کفن می‌پرورد در چشم بسمل انتظار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در محبت آرزو را اعتبار دیگر است</p></div>
<div class="m2"><p>این حریفان وصل می‌خواهند و بیدل انتظار</p></div></div>