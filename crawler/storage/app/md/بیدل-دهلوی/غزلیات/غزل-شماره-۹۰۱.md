---
title: >-
    غزل شمارهٔ ۹۰۱
---
# غزل شمارهٔ ۹۰۱

<div class="b" id="bn1"><div class="m1"><p>دل فتح و دست فتح و نظر فتح و کار فتح</p></div>
<div class="m2"><p>گلجوش هر نفس زدنت صدهزار فتح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دستت به بازوی نسب مرتضی قوی</p></div>
<div class="m2"><p>تیغ تو را همین حسب ذوالفقار فتح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک غنچه غیر گل نتوان یافت تا ابد</p></div>
<div class="m2"><p>در گلشنی که کرد حقش آبیار فتح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گردون چو زخم کهنه کند چارپاره‌اش</p></div>
<div class="m2"><p>گر با دل عدوی‌ تو سازد دچار فتح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرجا به عزم رزم ببالد اراده‌ات</p></div>
<div class="m2"><p>مژگان گشودنی نکشد انتظار فتح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یارب چو آفتاب به هرجا قدم زنی</p></div>
<div class="m2"><p>گردد رهت چو صبح کند آشکار فتح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چندانکه چشم کار کند گل دمیده گیر</p></div>
<div class="m2"><p>چون آسمان گرفته جهان در کنار فتح</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آغوش خرمی چقدر باز کرده‌ای</p></div>
<div class="m2"><p>کآفاق از تو باغ گل است ای بهار فتح</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکبار اگر رسد به زبان نام نصرتت</p></div>
<div class="m2"><p>هشتاد و هشت و چارصد ارد شمار فتح</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا حشر ای سحاب چمن‌ساز بیدلان</p></div>
<div class="m2"><p>بر مزرع امید دو عالم ببار فتح</p></div></div>