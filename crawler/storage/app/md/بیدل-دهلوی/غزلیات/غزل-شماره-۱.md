---
title: >-
    غزل شمارهٔ ۱
---
# غزل شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>آیینه بر خاک زد صُنعِ یکتا</p></div>
<div class="m2"><p>تا وانمودند کیفیتِ ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنیادِ اظهار بر رنگ چیدیم</p></div>
<div class="m2"><p>خود را به هر رنگ کردیم رسوا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در پرده پختیم سودایِ خامی</p></div>
<div class="m2"><p>چندان که خندید آیینه بر ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از عالمِ فاش بی‌پرده گشتیم</p></div>
<div class="m2"><p>پنهان نبودن‌، کردیم پیدا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما و رُعونت‌، افسانهٔ کیست</p></div>
<div class="m2"><p>نازِ پری بست گردن به مینا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آیینه‌واریم محرومِ عبرت</p></div>
<div class="m2"><p>دادند ما را چشمی که مگشا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درهایِ فرد‌وس وا بود امروز</p></div>
<div class="m2"><p>از بی‌دماغی گفتیم فردا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گو‌هر گره بست از بی‌نیازی</p></div>
<div class="m2"><p>دستی که شستیم از آبِ دریا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر جیبِ ناموس تنگت نگیرد</p></div>
<div class="m2"><p>در چینِ د‌امن خفته‌ست صحرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حیرت‌طرازی‌ست، نیرنگ‌سازی‌ست</p></div>
<div class="m2"><p>تمثالِ اوهام آیینه دنیا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کثرت نشد محو از سازِ وحدت</p></div>
<div class="m2"><p>هم‌چون خیالات از شخصِ تنها</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وهمِ تعلّق بر خود مچینید</p></div>
<div class="m2"><p>صحرانشین‌اند این خانمان‌ها</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>موجود نامی است، باقی توهّم</p></div>
<div class="m2"><p>از عالمِ خضر رو تا مسیحا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زین یأسِ مُنزَل ما را چه حاصل</p></div>
<div class="m2"><p>هم‌خانه بیدل، هم‌سایه عَنقا</p></div></div>