---
title: >-
    غزل شمارهٔ ۲۳۹۵
---
# غزل شمارهٔ ۲۳۹۵

<div class="b" id="bn1"><div class="m1"><p>بعد مردن از غبارم‌ کیست تا یابد نشان</p></div>
<div class="m2"><p>نقش پای موج هم با موج می‌باشد روان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خامشی مهری‌ست بر طومار عرض مدعا</p></div>
<div class="m2"><p>همچو شمع‌کشته دارم داغ بر روی زبان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک‌ گردیدن حصول صد گهر جمعیت است</p></div>
<div class="m2"><p>کاش موج من ز ساحل برنگرداند عنان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کو خموشی تا نفس تمکین دل انشا کند</p></div>
<div class="m2"><p>گوهر است اما اگر پیچد به خویش این ریسمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست غیر از احتیاط آگهی دشواربم</p></div>
<div class="m2"><p>زیرکوه از بار مژگان همچو خواب پاسبان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تن به سختی داده را آفت‌ گوارا می‌شود</p></div>
<div class="m2"><p>نیست دشواری دم شمشیر خوردن از فسان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در فضای شعله خاکستر هم از خود می‌رود</p></div>
<div class="m2"><p>عالمی در جستجوی بی نشان شد بی‌نشان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غفلت ساز امل را چاره نتوان یافتن</p></div>
<div class="m2"><p>ما به فکر آشیانیم و نفسها پرفشان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرمیی در مجمر هنگامهٔ آفاق نپست</p></div>
<div class="m2"><p>آتش این کاروانها رفت پیش از کاروان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زینهمه نقشی‌ که توفان دارد از آیینه‌ات</p></div>
<div class="m2"><p>گر بجویی غیر حیرت نیست چیزی در میان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون گهر اشک دبستان پرور حیرانی ام</p></div>
<div class="m2"><p>تا قیامت درس طفل ما نمی‌گردد روان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همچو هستی در عدم هم مشکلست آزادگی</p></div>
<div class="m2"><p>مدعا پرواز اگر باشد قفس‌گیر آشیان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خانهٔ نیرنگ هستی حسرت اسبابست و بس</p></div>
<div class="m2"><p>روزن بام و در از خمیازه می‌بندد گمان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>با همه پرواز شوق از ما زمینگیری نرفت</p></div>
<div class="m2"><p>جز به‌حیرت بر نمی‌آید نگاه ناتوان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بسکه بار زندگی بیدل به پیری می‌کشم</p></div>
<div class="m2"><p>موی من از سخت جانی برد رنگ ستخوان</p></div></div>