---
title: >-
    غزل شمارهٔ ۵۲۱
---
# غزل شمارهٔ ۵۲۱

<div class="b" id="bn1"><div class="m1"><p>چشم بیدار طرب مایهٔ سامان‌گل است</p></div>
<div class="m2"><p>در نظر خوابت اگر سوخت چراغان‌گل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آب و رنگ دگر از فیض جنون یافته‌ایم</p></div>
<div class="m2"><p>عرض رسوایی ما چاک‌گریبان‌گل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشرت رفته درین باغ تماشا دارد</p></div>
<div class="m2"><p>خنده‌های سحر آغوش پریشان‌گل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک‌نگه مشق تماشای طرب مفت هوس</p></div>
<div class="m2"><p>غنچه در مهد به‌پرداز دبستان گل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داغ بیطاقتی کاغذ آتش زده‌ایم</p></div>
<div class="m2"><p>رفتن از خود چقدر سیر خیابان‌گل است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اشک ما موج تبسمکدهٔ شوخی اوست</p></div>
<div class="m2"><p>شور شبنم نمکی از لب خندان‌گل است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرصت عیش‌‌درین باغ نچیده‌ست بساط</p></div>
<div class="m2"><p>رنگ گردیست ز پایی‌که به دامان‌گل است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نشوی بیهوده تهمت‌کش جمعیت دل</p></div>
<div class="m2"><p>غنچه هم در شکن ببستن پیمان‌گل است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو هم از نالهٔ بلبل نشستن آموز</p></div>
<div class="m2"><p>صحن این باغ پر از خانه به‌دوشان گل است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رنگ و بو در نظرت چند نقاب آراید</p></div>
<div class="m2"><p>با خبر باش همین صورت عریان‌گل است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یاد ما حسن تو را آینهٔ استغناست</p></div>
<div class="m2"><p>نالهٔ بلبل بیدل علم‌شان‌گل است</p></div></div>