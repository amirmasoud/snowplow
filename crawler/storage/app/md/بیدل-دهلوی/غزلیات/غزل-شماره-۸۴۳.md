---
title: >-
    غزل شمارهٔ ۸۴۳
---
# غزل شمارهٔ ۸۴۳

<div class="b" id="bn1"><div class="m1"><p>دوش از نظر خیال تو دامن‌کشان‌گذشت</p></div>
<div class="m2"><p>اشک آنقدر دوید ز پی کز فغان ‌گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا پر فشانده‌ایم ز خود هم گذشته‌ایم</p></div>
<div class="m2"><p>دنیا غم تو نیست‌که نتوان از آن‌گذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دارد غبار قافلهٔ ناامیدی‌ام</p></div>
<div class="m2"><p>از پا نشستنی‌که ز عالم توان‌گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برق و شرار محمل فرصت نمی‌کشد</p></div>
<div class="m2"><p>عمری نداشتم‌که بگویم چسان‌گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا غنچه دم زند ز شکفتن بهار رفت</p></div>
<div class="m2"><p>تا ناله گل کند ز جرمن کاروان گذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیرون نتاخته‌ست ازین عرصه هیچ کس</p></div>
<div class="m2"><p>واماندنی‌ست اینکه توگو.بی فلان‌گذشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای معنی آب شو که ز ننگ شعور خلق</p></div>
<div class="m2"><p>انصاف نیز آب شد و از جهان‌گذشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک نقطه پل ز آبلهٔ پا کفایت است</p></div>
<div class="m2"><p>زین بحر همچو موج ‌گهر می‌توان ‌گذشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر بگذری ز کشمکش چرخ واصلی</p></div>
<div class="m2"><p>محو نشانه است چو تیر از کمان‌ گذشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>واماندگی ز عافیتم بی‌نیاز کرد</p></div>
<div class="m2"><p>بال آنقدر شکست که از آشیان‌ گذشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طی شد بساط عمر به پای شکست رنگ</p></div>
<div class="m2"><p>بر شمع یک بهار گل زعفران ‌گذشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دلدار رفت و من را بی وداعی سوخت</p></div>
<div class="m2"><p>یارب چه برق بر من آتش به جان‌گذشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تمکین ‌کجا به سعی خرامت رضا دهد</p></div>
<div class="m2"><p>کم نیست اینکه نام توام بر زبان ‌گذشت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیدل چه مشکل است ز دنیاگذشتنم</p></div>
<div class="m2"><p>یک ناله داشتم‌ که ز هفت آسمان‌ گذشت</p></div></div>