---
title: >-
    غزل شمارهٔ ۶۹۷
---
# غزل شمارهٔ ۶۹۷

<div class="b" id="bn1"><div class="m1"><p>چون شمع اگر خلق پس و پیش‌گذشته‌ست</p></div>
<div class="m2"><p>تا نقش قدم پا به سر خویش‌گذشته‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هیچ مکان رام تسلی نتوان شد</p></div>
<div class="m2"><p>زین بادیه خلقی به دل ریش‌گذشته‌ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر راهروی براثر اشک قدم زن</p></div>
<div class="m2"><p>هستی‌ست خدنگی که ز هرکیش گذشته‌ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاید ز عدم گل کند آثار سراغی</p></div>
<div class="m2"><p>ز دشت غبار همه‌کس پیش‌گذشته‌ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر اشک‌که‌گل‌کرد ز ما و تو به راهی‌ست</p></div>
<div class="m2"><p>این آبله‌ها بر سر یک نیش‌گذشته‌ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روز دو دگر نیز به‌کلفت سپری‌گیر</p></div>
<div class="m2"><p>زین پیش هم اوقاف به تشویش‌گذشته‌ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیخان همه آداب خرامند ولیکن</p></div>
<div class="m2"><p>زین قافله‌ها یکدو قدم ریش‌گذشته‌ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آدمگری از ریش بیاموزکه امروز</p></div>
<div class="m2"><p>هر پشم ز صد خرس‌و بز و میش‌گذشته‌ست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ی پیر خرف شرم‌کن از دعوی شوخی</p></div>
<div class="m2"><p>عمری که‌کمش می‌شمری بیش‌گذشته‌ست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زین بحرکه دور است سلامت زکنارش</p></div>
<div class="m2"><p>آسوده همین کشتی درویش گذشته‌ست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سرمایه هوایی‌ست چه دنیا و چه عقبا</p></div>
<div class="m2"><p>از هرچه نفس بگذرد از خویش‌گذشته‌ست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل به جهان‌گذران تا دم محشر</p></div>
<div class="m2"><p>یک قافله آینده میندیش گذشته‌ست</p></div></div>