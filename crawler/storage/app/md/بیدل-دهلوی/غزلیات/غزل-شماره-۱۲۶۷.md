---
title: >-
    غزل شمارهٔ ۱۲۶۷
---
# غزل شمارهٔ ۱۲۶۷

<div class="b" id="bn1"><div class="m1"><p>رضاعت از برم چندانکه گردم پیر می‌جوشد</p></div>
<div class="m2"><p>چو آتش می‌شوم خا کستر اما شیر می‌جوشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندارد مزرع دیوانگان بی‌ناله سیرابی</p></div>
<div class="m2"><p>همین یک ریشه از صد دانهٔ زنجیر می‌جوشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم مشکن مبادا نقش بندد شکل بیدادت</p></div>
<div class="m2"><p>زموی چینی اینجا خامهٔ تصویر می‌جوشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه دارد انفعال طبع ظالم جز سیه ‌رویی</p></div>
<div class="m2"><p>عرق از سنگ اگربی‌پرده‌گردد قیر می‌جوشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تبرا از شلایینی ندارد طینت مبرم</p></div>
<div class="m2"><p>ز هرجایی که جوشد خار دامنگیر می جوشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نفس‌سوز دماغ شرح و بسط زندگی تاکی</p></div>
<div class="m2"><p>به این خوابی‌ که دارم پا زدن تعبیر می‌جوشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سراغ عافیت خواهی به میدان شهادت رو</p></div>
<div class="m2"><p>که صد بالین راحت از پر یک تیر می‌جوشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در این صحرا شکارافکن خیال کیست حیرانم</p></div>
<div class="m2"><p>که رقص موج‌ گل با خون هر نخجیر می جوشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز صبح مقصد آگه نیستم لیک اینقدر دانم</p></div>
<div class="m2"><p>که سرتاپای ‌من چون سایه یک شبگیر می‌جوشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مگر از جوهر یاقوت رنگ‌ است‌ این‌ گلستان را</p></div>
<div class="m2"><p>که آب و آتش‌گل پر ادب تاثیر می‌جوشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دماغ آشفتهٔ خاصیت، ‌پنجاب وکشمیرم</p></div>
<div class="m2"><p>که بوی هر گل آنجا با پیاز و سیر می‌جوشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به‌ربط ناقصان بیدل مده زحمت ریاضت را</p></div>
<div class="m2"><p>بهم انگورهای خام در خم دیر می‌جوشد</p></div></div>