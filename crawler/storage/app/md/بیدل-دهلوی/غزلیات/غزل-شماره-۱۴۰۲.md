---
title: >-
    غزل شمارهٔ ۱۴۰۲
---
# غزل شمارهٔ ۱۴۰۲

<div class="b" id="bn1"><div class="m1"><p>قماش رنگ ز بس بی‌حجاب می‌بافند</p></div>
<div class="m2"><p>به روی ‌گل ز دریدن نقاب می‌بافند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مباش منکر اسرار سینه‌ چاکی ما</p></div>
<div class="m2"><p>به‌ کارگاه سحر آفتاب می‌بافند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز زخم تیغ حوادث توان شدن ایمن</p></div>
<div class="m2"><p>به جوشنی‌ که ز موج شراب می‌بافند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به یک نفس سر بی مغز می‌خورد بر سنگ</p></div>
<div class="m2"><p>جدا ز پشم‌ کلاه حباب می‌بافند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درین چمن ‌که هوا داغ شبنم‌ آراییست</p></div>
<div class="m2"><p>تسلّیی به هزار اضطراب می‌بافند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو خواه مرگ شمر خواه زندگی اندیش</p></div>
<div class="m2"><p>همین به طبع ‌کتان ماهتاب می‌بافند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کراست تاب رسایی بحث فرصت عمر</p></div>
<div class="m2"><p>گسسته است نفس تا جواب می‌بافند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>توان شناخت ز باریک‌ریشی انفاس</p></div>
<div class="m2"><p>که در قلمرو هستی چه باب می‌بافند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کباب شد عدم ما ز تهمت هستی</p></div>
<div class="m2"><p>بر آتشی‌ که نداریم آب می‌بافند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز گفت‌وگو به غبارم نظر متن بیدل</p></div>
<div class="m2"><p>که بهر چشم ز افسانه خواب می‌بافند</p></div></div>