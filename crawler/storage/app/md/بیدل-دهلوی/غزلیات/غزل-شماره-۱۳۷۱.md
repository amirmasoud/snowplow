---
title: >-
    غزل شمارهٔ ۱۳۷۱
---
# غزل شمارهٔ ۱۳۷۱

<div class="b" id="bn1"><div class="m1"><p>ای بی‌نصیب عشق به ‌کار هوس بخند</p></div>
<div class="m2"><p>بر بال هرزه پر دو سه چاک قفس بخند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل جمع‌کن به یک دو قدح ازهزار وهم</p></div>
<div class="m2"><p>برمحتسب بتیز و به ریش عسس بخند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اوقات زندگی ز فسردن به باد رفت</p></div>
<div class="m2"><p>برگریه‌ات اگر نبود دسترس بخند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین جمع مال مسخرگی موج می زند</p></div>
<div class="m2"><p>خلقی‌ست درکمند فسار و مرس بخند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شور ترانه‌سنجی عنقایی‌ات رساست</p></div>
<div class="m2"><p>چندی به قاه‌قاه طنین مگس بخند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از شرم چون شرر مژه‌ای واکن و بپوش</p></div>
<div class="m2"><p>سامان این بهار همین است و بس بخند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین‌کشت‌خون به‌دل چه‌ضرور است رستنت</p></div>
<div class="m2"><p>لب گندمین کن و به تلاش عدس بخند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در آتش است شمع و همان خنده می‌کند</p></div>
<div class="m2"><p>ای خامشی به غفلت این بوالهوس بخند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تاکی‌کند فسون نفس داغ فرصتت</p></div>
<div class="m2"><p>ای آتش فسرده به سامان خس بخند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خاموش رفته‌اند رفیقانت از نظر</p></div>
<div class="m2"><p>اشکی به درد قافلهٔ بی‌جرس بخند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر زندگی چو صبح‌ گمان بقاکبراست</p></div>
<div class="m2"><p>گو این غبار رفته به‌گردون نفس بخند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل چو گل اگر فکنی طرح انبساط</p></div>
<div class="m2"><p>چشمی به خویش واکن و بر پیش و پس بخند</p></div></div>