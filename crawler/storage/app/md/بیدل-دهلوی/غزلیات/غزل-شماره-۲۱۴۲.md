---
title: >-
    غزل شمارهٔ ۲۱۴۲
---
# غزل شمارهٔ ۲۱۴۲

<div class="b" id="bn1"><div class="m1"><p>ز بس لبریز حسرت دارد امشب شوق دیدارم</p></div>
<div class="m2"><p>چکد آیینه‌ها بر خاک اگر مژگان بیفشارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تغافل زبن شبستان نیست بی‌عبرت چراغانی</p></div>
<div class="m2"><p>مژه خوابیدنی دارد به چندین چشم بیدارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنای نقش پایم در زمین نارساییها</p></div>
<div class="m2"><p>به دوش سایه هم نتوان رساندن دست دیوارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غبار عالم کثرت نفس دزدیدنی دارد</p></div>
<div class="m2"><p>وگرنه همچو بو از اختلاط رنگ بیزارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زبان حالم از انصاف عذر ناله می‌خواهد</p></div>
<div class="m2"><p>گران جانتر ز چندین‌کوهم و دل می‌کشد بارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ضعیفی شوخی نشو و نمایم برنمی‌دارد</p></div>
<div class="m2"><p>مگر از روی بستر ناله خیزد جای بیمارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو خاشاکم نگاهی در رگ خواب آشیان دارد</p></div>
<div class="m2"><p>خدایا آتشین رویی‌کند یک چشم بیدارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگر آهی‌کندگل تا به پرواز آیدم رنگی</p></div>
<div class="m2"><p>که چون شمع از ضعیفی رنگ دزدیده‌ست منقارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وفا سر رشته‌اش صد عقد الفت درکمین دارد</p></div>
<div class="m2"><p>ز بس درهم‌گسستم سبحه پیداکرد زنارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جنون صبحم از آشفتگیهایم مشو غافل</p></div>
<div class="m2"><p>جهانی را ز سر وا می‌توان‌کردن به دستارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز شرم عیب خود چشم از هنر برداشتم بیدل</p></div>
<div class="m2"><p>به درد خار پا داغست چون طاووس‌ گلزارم</p></div></div>