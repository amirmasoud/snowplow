---
title: >-
    غزل شمارهٔ ۱۰۹۱
---
# غزل شمارهٔ ۱۰۹۱

<div class="b" id="bn1"><div class="m1"><p>آگاهی از خیال خودم بی‌نیاز کرد</p></div>
<div class="m2"><p>خود را ندید آینه تا چشم باز کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نعل جهان در آتش فکر سلامت است</p></div>
<div class="m2"><p>آن شعله آرمید که مشق گداز کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون آه کرد رهگذر ناامیدی‌ام</p></div>
<div class="m2"><p>هرکس ز پا نشست مرا سرفراز کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کو زحمت فراق و کدام انبساط وصل</p></div>
<div class="m2"><p>زین جور آنچه کرد به ما امتیاز کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کلفت‌زدای کینهٔ دل‌ها تواضع است</p></div>
<div class="m2"><p>زین تیشه می‌توان‌ گره سنگ باز کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حیرت مقیم خانهٔ آیینه است و بس</p></div>
<div class="m2"><p>نتوان به روی ما در دل‌ها فراز کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داغم ز سایه‌‌ای ‌که به طوف سجود او</p></div>
<div class="m2"><p>پای طلب ز نقش جبین نیاز کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شابت قیام و شیب رکوع و فنا سجود</p></div>
<div class="m2"><p>در هستی و عدم نتوان جز نماز کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زین گلستان به حیرت شبنم رسیده‌ام</p></div>
<div class="m2"><p>باید دری به خانهٔ خورشید باز کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در پرده بود صورت موهوم هستی‌ام</p></div>
<div class="m2"><p>آیینهٔ خیال تو افشای رازکرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر زندگی‌ست بار گرانجانی‌ام هنوز</p></div>
<div class="m2"><p>قد دو تا مرا خم ابروی ناز کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گامی نبود بیش ره مقصد فنا</p></div>
<div class="m2"><p>این رشته را نفس به کشاکش دراز کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>معنی نمای چهره مقصود نیستی ست</p></div>
<div class="m2"><p>بیدل مرا گداختن آیینه‌سازکرد</p></div></div>