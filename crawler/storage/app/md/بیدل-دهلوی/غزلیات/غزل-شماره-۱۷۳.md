---
title: >-
    غزل شمارهٔ ۱۷۳
---
# غزل شمارهٔ ۱۷۳

<div class="b" id="bn1"><div class="m1"><p>ربود از بس خیال ساعد او هوش ماهی را</p></div>
<div class="m2"><p>نمی‌باشد خبر از شور دریاگوش ماهی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نفس دزدیدنم در شور امکان ریشه‌ها دارد</p></div>
<div class="m2"><p>زبان با موج می‌جوشد لب خاموش ماهی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز دمسردی دوران کم نگرددگرمی دلها</p></div>
<div class="m2"><p>فسردن‌مشکل است از آب‌دریا جوش ماهی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حریصان را نباشد محنت از حمالی دنیا</p></div>
<div class="m2"><p>گرانی‌کم رسد از بار درهم دوش ماهی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جای استخوان از پیکر اینجا تیر می‌ روید</p></div>
<div class="m2"><p>سراغ عافیت‌کو وضع جوشن‌پوش ماهی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غریق وصلم و شوق‌کنار آواره‌ام دارد</p></div>
<div class="m2"><p>تپیدن ناکجا وسعت دهد آغوش ماهی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نصیحت‌کارگر نبود غریق عشق را بیدل</p></div>
<div class="m2"><p>به دریا احتیاج در نباشدگوش ماهی را</p></div></div>