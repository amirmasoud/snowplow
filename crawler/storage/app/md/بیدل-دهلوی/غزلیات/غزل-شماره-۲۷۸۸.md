---
title: >-
    غزل شمارهٔ ۲۷۸۸
---
# غزل شمارهٔ ۲۷۸۸

<div class="b" id="bn1"><div class="m1"><p>صد رنگ نقش بستیم دریاد گل جبینی</p></div>
<div class="m2"><p>طاووس‌ کرد ما را تصویر نازنینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرواز شوق امروز محمل‌کش تپش نیست</p></div>
<div class="m2"><p>در بیضه‌ام جنون داشت بی‌بال و پرکمینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وهم برهنه پایی‌گر دامنت نگیرد</p></div>
<div class="m2"><p>هر خار این بیابان دارد ترنجبینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صور و خروش محشر درگوش عاشقانت</p></div>
<div class="m2"><p>کم نیست‌ گر رساند از پشه‌ای طنینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را غرور دانش شد دور باش تحقیق</p></div>
<div class="m2"><p>می‌خواست این تماشا چشم به خود نبینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در مکتب تعین چندین ورق سیه‌ کرد</p></div>
<div class="m2"><p>مشق خیال هستی از سر خط جبینی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زنن دشت و در ندیدیم جایی‌ که دل ‌گشاید</p></div>
<div class="m2"><p>در بحر نظم شاید پیدا شود زمینی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شهرت ‌کمین عنقا مردیم و خاک‌ گشتیم</p></div>
<div class="m2"><p>بر نام ما نخندید زین انجمن نگینی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از ذره تا مه و مهر آمادهٔ رحیل است</p></div>
<div class="m2"><p>هر پای بر رکابی هر توسنی و زینی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل مپیچ چندین بر دستگاه اقبال</p></div>
<div class="m2"><p>در دامن بلندت چین دارد آستینی</p></div></div>