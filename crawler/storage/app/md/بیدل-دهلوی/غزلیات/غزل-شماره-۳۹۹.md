---
title: >-
    غزل شمارهٔ ۳۹۹
---
# غزل شمارهٔ ۳۹۹

<div class="b" id="bn1"><div class="m1"><p>چولاله بی‌تو ز بس رنگ اعتبارم سوخت</p></div>
<div class="m2"><p>خزان به باد فنا داد و نوبهارم سوخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمردمک نگهم داغ شد چوشمع خموش</p></div>
<div class="m2"><p>در انتظار تو سامان انتظارم سوخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هجوم حیرت آن جلوه چون پرطاووس</p></div>
<div class="m2"><p>هزار رنگ تپش در دل غبارم سوخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غبار تربت پروانه می‌دهد آواز</p></div>
<div class="m2"><p>که می‌توان نفسی بر سر مزارم سوخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشدکه شعلهٔ من نیز بی‌غبار شود</p></div>
<div class="m2"><p>صفای آینهٔ وحشت شرارم سوخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به عشق نیز اثرکرد شرم ناکسی‌ام</p></div>
<div class="m2"><p>عرق‌فشانی این شعله خامکارم سوخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صبا مزن به غبار فسرده‌ام دامن</p></div>
<div class="m2"><p>دماغ حسرت رقصی‌که من ندارم سوخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو برق آینهٔ امنیاز هستی من</p></div>
<div class="m2"><p>ز خوابگاه عدم تا سری برآرم سوخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز تخته پاره‌ام ناخدا چه می‌پرسی</p></div>
<div class="m2"><p>فلک‌کشید زگرداب و برکنارم سوخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هزار برق ز خاکسترم پرافشانست</p></div>
<div class="m2"><p>کدام شعله به این رنگ بیقرارم سوخت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شهید ناز تو پروانه کرد عالم را</p></div>
<div class="m2"><p>چها نسوخت چراغی‌که بر مزارم سوخت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فلک نیافت علاج‌کدورتم بیدل</p></div>
<div class="m2"><p>نفس به‌سینهٔ این دشت از غبارم سوخت</p></div></div>