---
title: >-
    غزل شمارهٔ ۱۰۳۲
---
# غزل شمارهٔ ۱۰۳۲

<div class="b" id="bn1"><div class="m1"><p>این دور، دور حیز است‌، وضع متین که دارد</p></div>
<div class="m2"><p>باد و بروت مردی غیر از سرین که دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آثار حق‌پرستی ختم است بر مخنث</p></div>
<div class="m2"><p>غیر از دبر سرشتان سر بر زمین‌ که دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرسو به حرکت نفس مطلق عنان بتازید</p></div>
<div class="m2"><p>ای زیر خرسواران پالان و زین‌ که دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاهد ز پهلوی ریش پشمینه می‌فروشی</p></div>
<div class="m2"><p>بازار نوره‌ گرم است این پوستین‌ که دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رنگ بنای طاعت بر خدمت سرین نه</p></div>
<div class="m2"><p>امروز طرح محراب جز گنبدین که دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر کیسهٔ کریمان چشم طمع ندوزی</p></div>
<div class="m2"><p>جز دست خر در این عصر در آستین ‌که دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از منعمان ‌گدا را دیگر چه می‌توان خواست</p></div>
<div class="m2"><p>تن داده‌اند بر فحش داد اینچنین که دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خلقی وسیع‌ ‌خفته‌ست در تنگی سرین‌ها</p></div>
<div class="m2"><p>جز کام این حواصل دامن به چین‌ که دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یک غنچه صد گلستان آغوش می‌گشابد</p></div>
<div class="m2"><p>مقعد به خنده باز است طبع حزین که دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از بس که دور گردون گرداند طور مردم</p></div>
<div class="m2"><p>تا پشت برنتابد بر زن یقین که دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ادبار مرد و زن را نگذاشت نام اقبال</p></div>
<div class="m2"><p>یک کاف و واو و نون است تا کاف و سین که دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن خرقه‌ای که جیبش باب رفو نباشد</p></div>
<div class="m2"><p>بردار دامنی چند آنگه ببین که دارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در چارسوی آفاق بالفعل این منادی‌ست</p></div>
<div class="m2"><p>لعل خوشاب با کیست در ثمین که دارد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جز جوهر گران‌سنگ مطلوب مشتری نیست</p></div>
<div class="m2"><p>ساق بلور بنما جنس گزین که دارد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سرد است بی‌تکلف هنگامهٔ تهور</p></div>
<div class="m2"><p>کر کن تفنگ و خوش باش جز مهر کین که دارد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بیدل به تیغ و خنجر نتوان شدن بهادر</p></div>
<div class="m2"><p>لشکر عمود خواهد تا آهنین‌ که دارد</p></div></div>