---
title: >-
    غزل شمارهٔ ۱۲۹۶
---
# غزل شمارهٔ ۱۲۹۶

<div class="b" id="bn1"><div class="m1"><p>در گلستانی‌ که چشمم محو آن طناز ماند</p></div>
<div class="m2"><p>نکهت‌گل نیز چون برگ گل از پرواز ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسکه فطرتها به‌گرد نارسایی بازماند</p></div>
<div class="m2"><p>یک جهان انجام‌، خجلت‌پرور آغاز ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نغمه‌ها بسیار بود اما ز جهل مستمع</p></div>
<div class="m2"><p>هرقدر بی‌پرده شد در پرده‌های ساز ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حسن‌در اظهار شوخی رنگ‌تقصیری ند!شت</p></div>
<div class="m2"><p>چشمها غفلت نگه شد جلوه محو باز ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این زمان، حسرت‌، تسلی‌خانهٔ جمعیت است</p></div>
<div class="m2"><p>بی‌خیالی نیست آن آیینه ‌کز پرداز ماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقش نیرنگ حقیقت ثبت لوح دل بس است</p></div>
<div class="m2"><p>شوق غافل نیست گر چشم تماشا باز ماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جوهر آیینهٔ من سوخت شرم جلوه‌اش</p></div>
<div class="m2"><p>حیرتی ‌گل ‌کرده بودم لیک محو ناز ماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عمرها شد خاک بر سر می‌کند اجزای من</p></div>
<div class="m2"><p>یارب این‌گرد پریشان از چه دامن باز ماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شعلهٔ ما دعوی افسردن آخر پیش برد</p></div>
<div class="m2"><p>برشکست رنگ بستم آنچه ازپرواز ماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صافی د‌ل شبههٔ هستی به عرض آوردن است</p></div>
<div class="m2"><p>عکس هرجا محو شد آیینه از پرداز ماند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جاده سرمنزل مقصد خط پرگار داشت</p></div>
<div class="m2"><p>عالمی انجامها طی‌کرد و در آغاز ماند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یار رفت از دیده اما از هجوم حیرتش</p></div>
<div class="m2"><p>با من از هر جلوه‌ای آیینه‌داری باز ماند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خامشی روشنگر آیینهٔ دیدار بود</p></div>
<div class="m2"><p>با سواد سرمه پیوست آنچه از آواز ماند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ازگداز صد جگر اشکی به عرض آورده‌ام</p></div>
<div class="m2"><p>بخیه‌ای آخر ز چاک پرده‌های راز ماند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بیدل از برگ و نوای ما سیه‌بختان مپرس</p></div>
<div class="m2"><p>روزگار وصل رفت و طالع ناساز ماند</p></div></div>