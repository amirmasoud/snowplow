---
title: >-
    غزل شمارهٔ ۲۲۳۱
---
# غزل شمارهٔ ۲۲۳۱

<div class="b" id="bn1"><div class="m1"><p>از شوق تو ای شمع طرب بعد هلاکم</p></div>
<div class="m2"><p>جوشد پر پروانه ز هر ذرهٔ خاکم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیتابی من عرض نسب‌نامهٔ مستی است</p></div>
<div class="m2"><p>چون موج می از سلسلهٔ ریشهٔ تاکم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دود نفس سوخته‌ام طرهٔ یار است</p></div>
<div class="m2"><p>کآن را نبود شانه به جز سینهٔ چاکم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تهمت‌کش آلایش هستی نتوان شد</p></div>
<div class="m2"><p>چون عکس ز تردامنی آینه پاکم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آهم‌، شررم‌، اشکم و داغم‌، چه توان کرد</p></div>
<div class="m2"><p>چون شمع درین بزم به صد رنگ هلاکم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای همت عالی‌نظران دست نگاهی</p></div>
<div class="m2"><p>تا چند برد پستی طالع به مغاکم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گردم چمن رنگ نبالد چه خیال است</p></div>
<div class="m2"><p>عمری‌ست که در راه تمنای تو خاکم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون غنچه ز شوق من دیوانه مپرسید</p></div>
<div class="m2"><p>گل نیز گریبان شده از حسرت چاکم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاشاک به ساحل رسد از دست رد موج</p></div>
<div class="m2"><p>از تیغ اجل نیست درین معرکه باکم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از بال هما کیست‌ کشد ننگ سعادت</p></div>
<div class="m2"><p>بیدل ز سر ما نشود سایهٔ ما کم</p></div></div>