---
title: >-
    غزل شمارهٔ ۲۷۴۰
---
# غزل شمارهٔ ۲۷۴۰

<div class="b" id="bn1"><div class="m1"><p>سجده بنیادی بساز ای جبهه با افتادگی</p></div>
<div class="m2"><p>سایه را نتوان ز خود کردن جدا افتادگی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از شعاع مهر یکسر خاکساری می‌چکد</p></div>
<div class="m2"><p>بر جبین چرخ هم خطی‌ست با افتادگی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سجده را در خاک راهش ‌گر عروج آبروست</p></div>
<div class="m2"><p>می‌شود چون دانه‌ام آخر عصا افتادگی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست راحت جز به وضع خاکساری ساختن</p></div>
<div class="m2"><p>با زمین سرکن چو نقش بوریا افتادگی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>استقامت نیست ساز کهنهٔ دیوار جهد</p></div>
<div class="m2"><p>عضو عضوت می‌زند موج زپا افتادگی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی ‌عرق یک ‌سجده از پیشانی من‌ گل نکرد</p></div>
<div class="m2"><p>می‌کند بر عجز حالم گریه‌ها افتادگی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون غبار رفته از خود دست و پایی می‌زنم</p></div>
<div class="m2"><p>تا به فریادم رسد آخر کجا افتادگی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آستانش از سجودم بسکه ننگ آلوده است</p></div>
<div class="m2"><p>آب می‌گردد چو شبنم ازحیا افتادگی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا به ‌چشم نقش پایی راه عبرت ‌واکنم</p></div>
<div class="m2"><p>پیکرم را کاش سازد توتیا افتادگی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با کمال سرکشی بیدل تواضع طینتم</p></div>
<div class="m2"><p>همچو زلف یار می‌نازد به ما افتادگی</p></div></div>