---
title: >-
    غزل شمارهٔ ۱۳۵۶
---
# غزل شمارهٔ ۱۳۵۶

<div class="b" id="bn1"><div class="m1"><p>کار دنیا بس که مهمل‌ گشت عقبا ریختند</p></div>
<div class="m2"><p>فرصت امروز خون شد رنگ فردا ریختند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوی یوسف از فسردن پیرهن آمد به عرض</p></div>
<div class="m2"><p>شد پری بی‌ بال و پر چندان‌ که مینا ریختند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سینه‌چاکان را دماغ سخت‌جانی‌ها نبود</p></div>
<div class="m2"><p>از شکست رنگ همچون ‌گل سراپا ریختند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترک خودداریست عرض مشرب دیوانگی</p></div>
<div class="m2"><p>رفت ‌گرد ما ز خود جایی‌ که صحرا ریختند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در غبار عشق دارد حسن دام سرکشی</p></div>
<div class="m2"><p>طرح آن زلف از شکست خاطر ما ریختند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچکس از گریهٔ من در جهان هشیار نیست</p></div>
<div class="m2"><p>بیخودی فرش است هرجا رنگ صهبا ریختند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی‌دماغی محفل‌‌آرای جنون شوق بود</p></div>
<div class="m2"><p>سوخت‌ حسرت ها نفس تا شمع سودا ریختند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رنگ تحقیقی نبستم زان‌ حنای نقش پا</p></div>
<div class="m2"><p>این قدر دانم ‌که خونم را همین جا ریختند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ریزش ابر کرم در خورد استعداد ماست</p></div>
<div class="m2"><p>کشت بسمل تا شود سیراب ‌، خون ها ریختند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عاقبت بویی نبردیم از سراغ عافیت</p></div>
<div class="m2"><p>ساحل ‌گم گشتهٔ ما را به دریا ریختند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا نفس باقیست همچون شمع باید سوختن</p></div>
<div class="m2"><p>کز فسون هستی آتش بر سر ما ریختند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اشک ما بیدل ز درد نارسایی خاک شد</p></div>
<div class="m2"><p>ریشه‌ای پیدا نکرد این تخم هر جا ریختند</p></div></div>