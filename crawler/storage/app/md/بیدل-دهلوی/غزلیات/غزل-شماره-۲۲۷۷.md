---
title: >-
    غزل شمارهٔ ۲۲۷۷
---
# غزل شمارهٔ ۲۲۷۷

<div class="b" id="bn1"><div class="m1"><p>بعد ازین از صحبت این دیو مردم رم کنم</p></div>
<div class="m2"><p>غول چندی در بیابان پرورم آدم‌کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مزاج بدرگان جز فحش کم دارد اثر</p></div>
<div class="m2"><p>زخم سگ را بی لعاب سگ چسان مرهم‌کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالمی رنج توقعهای بیجا می‌کشد</p></div>
<div class="m2"><p>کوس شهرت انتظاران بشکنم یا نم‌کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون خبیث افتاد طبع از طینت ناپاک او</p></div>
<div class="m2"><p>خوک را حلواکشم در پیش تا ملزم‌کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با فساد جوهر ذاتی چه پردازد صلاح</p></div>
<div class="m2"><p>آدمیت کو اگر از خرس مویی کم کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرزه‌کاریها درین دل مردگان از حد گذشت</p></div>
<div class="m2"><p>بعد ازین آن به که گر کاری کنم ماتم کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هیچم اما در طلسم قدرت نیرنگ دهر</p></div>
<div class="m2"><p>چون عدم‌کاری‌که نتوان‌کرد اگر خواهم‌کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صنعتی دارد خیال من ‌که در یک دم زدن</p></div>
<div class="m2"><p>عالمی را ذره سازم ذره را عالم‌کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حکم تقدیر دگر در پردهٔ‌ کلک منست</p></div>
<div class="m2"><p>هر لئیمی راکه خواهم‌بی‌کرم حاتم‌کنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ننگ همت‌گر نباشد پوچ بافیهای وهم</p></div>
<div class="m2"><p>بر سماروغی نویسم جاه و چتر جم‌کنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا خجالت بشکند باد بروت سرکشی</p></div>
<div class="m2"><p>موی چینی بر علمهای شهان پرچم‌کنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از صفا آیینه‌دار یک جهان دل می‌شود</p></div>
<div class="m2"><p>سنگ خشتی راکه من با نقش خود محرم‌کنم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بسکه در ساز کلامم فیض آگاهی است عام</p></div>
<div class="m2"><p>محرم انصاف گرددگرکسی را ذم کنم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عبرت ایجادست بیدل تنگی آغوش شرم</p></div>
<div class="m2"><p>بی‌گریبان نیستم هر چند مژگان خم‌کنم</p></div></div>