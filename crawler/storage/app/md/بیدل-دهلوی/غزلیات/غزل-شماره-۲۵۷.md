---
title: >-
    غزل شمارهٔ ۲۵۷
---
# غزل شمارهٔ ۲۵۷

<div class="b" id="bn1"><div class="m1"><p>چون نقش پا ز عجز نگردید روی ما</p></div>
<div class="m2"><p>در سجده خاک شد سر تسلیم خوی ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیهوده همچو موج زبان برنمی‌کشیم</p></div>
<div class="m2"><p>لبریز خامشی‌ست چوگوهر سبوی ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای وهم عقده بر دل آزاد ما مبند</p></div>
<div class="m2"><p>بی‌تخم رسته است چو میناکدوی ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حیرت سجود معبد راز محبتیم</p></div>
<div class="m2"><p>غیر ازگداز نیست چو شبنم وضوی ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حرفی‌که دارد آینه مرهون حیرت است</p></div>
<div class="m2"><p>سیلی‌خور زبان نشودگفتگوی ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون شمع سربلندی عشاق مفت نیست</p></div>
<div class="m2"><p>یعنی به قدر سوختن است آبروی ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشهور عالمیم به نقصان اعتبار</p></div>
<div class="m2"><p>اظهارعیب چون‌گل چشم است بوی ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گمگشتگان وادی حیرت نگاهی‌ایم</p></div>
<div class="m2"><p>درگرد رنگ‌باخته کن جستجوی ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از بس‌که خوگرفتهٔ وضع ملامتیم</p></div>
<div class="m2"><p>جزرنگ نیست‌گرشکندکس به روی ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نتوان کشید هرزه‌تریهای عاریت</p></div>
<div class="m2"><p>بیدل زبحرنظم بس است آب جوی ما</p></div></div>