---
title: >-
    غزل شمارهٔ ۲۶۰۸
---
# غزل شمارهٔ ۲۶۰۸

<div class="b" id="bn1"><div class="m1"><p>در محیطی‌کز فلک طرح حباب انداخته</p></div>
<div class="m2"><p>کشتی ما را تحیر در سراب انداخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با دو عالم شوق بال بسملی آسوده‌ایم</p></div>
<div class="m2"><p>عشق بر چندین تپش از ما نقاب انداخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر شکست شیشهٔ دلهای ما رحمی نداشت</p></div>
<div class="m2"><p>آنکه در طاق خم آن زلف تاب انداخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تاکجاها بایدم صید خموشی زیستن</p></div>
<div class="m2"><p>در غبار سرمه چشمش دام خواب انداخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقشی از آیینهٔ‌ کیفیت ما گل نکرد</p></div>
<div class="m2"><p>دفتر ما را خجالت در چه آب انداخته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هستی ما را سراغ از جلوه دلدار پرس</p></div>
<div class="m2"><p>این کتان آیینه پیش ماهتاب انداخته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غیر شور ما و من بر هم زنی دیگر نداشت</p></div>
<div class="m2"><p>عیش این بزمم نمکها در شراب انداخته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر نباشد حرص عالم بحر مواج غناست</p></div>
<div class="m2"><p>تشنگی ما را به توفان سراب انداخته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رخت همت تا نبیند داغ اندوه تری</p></div>
<div class="m2"><p>سایهٔ ما خویش را در آفتاب انداخته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای خیال اندیش مژگان اندکی مژگان بمال</p></div>
<div class="m2"><p>می‌فشارد چشم من رخت در آب انداخته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ما و عنقا تا کجا خواهیم بحث شبهه‌کرد</p></div>
<div class="m2"><p>لفظ ما بیحاصلی دور از کتاب انداخته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یک نگه‌کم نیست بیدل فرصت عمرشرار</p></div>
<div class="m2"><p>آسمان طرح درنگم در شتاب انداخته</p></div></div>