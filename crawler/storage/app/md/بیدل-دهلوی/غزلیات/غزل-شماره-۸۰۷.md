---
title: >-
    غزل شمارهٔ ۸۰۷
---
# غزل شمارهٔ ۸۰۷

<div class="b" id="bn1"><div class="m1"><p>یک شبم در دل نسیم یاد آن گیسو گذشت</p></div>
<div class="m2"><p>عمر در آشفتگی‌ چون سر به زیر مو گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوخی اندیشهٔ لیلی درین وادی بلاست</p></div>
<div class="m2"><p>بر سر مجنون قیامت از رم آهوگذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیچ‌ کافَر را عذاب مرگ مشتاقان مباد</p></div>
<div class="m2"><p>کز وداع خویش باید از خیال او گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای دل از جور محبت تا توانی دم مزن</p></div>
<div class="m2"><p>ناله بی‌درد است خواهد از سر آن‌ کو گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سیل همو‌اری مباش از عرض افراط‌ کجی</p></div>
<div class="m2"><p>چین پیشانیست هرگه شوخی از ابرو گذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سراغ عافیت بگذر که در دشت جنون</p></div>
<div class="m2"><p>وحشت سنگ نشانها از رم آهو گذشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاقبت نقش قدم‌ گردید بالینم چو شمع</p></div>
<div class="m2"><p>بسکه در فکر خود افتادم سر از زانو گذشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>موج جوهر می‌زند هر قطره خون در زخم من</p></div>
<div class="m2"><p>سبزهٔ تیغ‌که یارب بر لب این جو گذشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی‌تأمل می‌توان طی کرد صد دریای خون</p></div>
<div class="m2"><p>لیک نتوان،‌ از سر یک قطره‌، آب رو گذشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا به خود جنبی نشانها بی‌نشانی ‌گشته ست</p></div>
<div class="m2"><p>ای بسا رنگی‌که در یک پر زدن از بو گذشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بستر ما ناتوانان قابل تغییر نیست</p></div>
<div class="m2"><p>موج گوهر آنقدر آسود کز پهلو گذشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر به این رنگ است بیدل کلفت ویرانه‌ات</p></div>
<div class="m2"><p>رحم‌ کن بر حال سیلی‌ کز بنای او گذشت</p></div></div>