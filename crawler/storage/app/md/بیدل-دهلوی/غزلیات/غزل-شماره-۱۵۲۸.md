---
title: >-
    غزل شمارهٔ ۱۵۲۸
---
# غزل شمارهٔ ۱۵۲۸

<div class="b" id="bn1"><div class="m1"><p>چو دولت درش بر خسان واشود</p></div>
<div class="m2"><p>پر آرد برون مور و عنقا شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بپرهیز از اقبال دون ‌فطرتان</p></div>
<div class="m2"><p>تنک‌روست سنگی که مینا شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سبک‌مغز شایان اسرار نیست</p></div>
<div class="m2"><p>خس از دوری شعله رسوا شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو گردد اقبال علم و عمل</p></div>
<div class="m2"><p>ورق چیست‌، خط هم چلیپا شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر ارباب همت دنائت مبند</p></div>
<div class="m2"><p>فلک خاک گردد که سرپا شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>معمای آفاق نتوان شکافت</p></div>
<div class="m2"><p>مگر اسم عنقا مسما شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز اسباب نتوان به دل زد گره</p></div>
<div class="m2"><p>بروبید تا خانه صحرا شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگین می‌تراشد معمای سنگ</p></div>
<div class="m2"><p>که شاید به نام ‌کسی واشود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به صد خامشی بازدارد سخن</p></div>
<div class="m2"><p>اگریک دمش در دلی جا شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بناگوش دلدارم آمد به یاد</p></div>
<div class="m2"><p>کنم ناله تا صبح‌ گویا شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زکیفیت نسبت آن دهن</p></div>
<div class="m2"><p>عدم تا بگویم من وما شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در ین دشت و در گردی از غیر نیست</p></div>
<div class="m2"><p>ترا گر نجویم که پیدا شود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به هرجا تو باشی زبانها یکیست</p></div>
<div class="m2"><p>نه امروز دی شد نه فردا شود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جهان چشم نگشاید از خواب ناز</p></div>
<div class="m2"><p>اگر بیدل افسانه انشا شود</p></div></div>