---
title: >-
    غزل شمارهٔ ۱۳
---
# غزل شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>آبیار چمن رنگ‌، سراب است اینجا</p></div>
<div class="m2"><p>درگل خندة تصویرگلاب است اینجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p> وهم تاکی شمرد سال و مه فرصت‌کار</p></div>
<div class="m2"><p>شیشهٔ ساعت‌موهوم حباب‌است اینجا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p> چیست گردون‌، هوس‌افزای خیالات عدم</p></div>
<div class="m2"><p>عالمی را به همین صفر حساب است اینجا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p> چه قدر شب رود از خودکه‌کندگرد سحر</p></div>
<div class="m2"><p>مو سفیدی عرق سعی شباب است اینجا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p> قد خم‌گشته‌، نشان می‌دهد از وحشت عمر</p></div>
<div class="m2"><p>بر در خانه از آن حلقه رکاب است اینجا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p> عشق ز اول علم لغزش پاداشت بلند</p></div>
<div class="m2"><p>عذر مستان به لب موج شراب است اینجا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p> بوریا راحت مخمل به فراموشی داد</p></div>
<div class="m2"><p>صد جنون شور نیستان رگ خواب است اینجا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p> لذت‌داغ جگر حق فراموشی نیست</p></div>
<div class="m2"><p>قسمتی در نمک اشک‌کباب است اینجا </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه درسعی فنا پیشترازیکدگریم‌</p></div>
<div class="m2"><p>با شرر سنگ‌گروتاز شتاب است اینجا </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رستن از آفت امکا‌ن تهی از خود شدنست </p></div>
<div class="m2"><p>تو زکشتی مگذر عالم آب است اینجا </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زین همه علم و عمل قدر خموشی دریاب </p></div>
<div class="m2"><p>هرکجا بحث‌سئوالی‌ست جواب است اینجا </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل آن فتنه‌که توفان قیامت دارد</p></div>
<div class="m2"><p> غیردل نیست همین خانه خراب است اینجا </p></div></div>