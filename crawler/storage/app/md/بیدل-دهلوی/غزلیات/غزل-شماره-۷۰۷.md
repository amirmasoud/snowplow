---
title: >-
    غزل شمارهٔ ۷۰۷
---
# غزل شمارهٔ ۷۰۷

<div class="b" id="bn1"><div class="m1"><p>نه ما را صراحی نه پیمانه ‌ایست</p></div>
<div class="m2"><p>دل و دیده غوغای مستانه ایست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دل ششجهت شیشه‌ها چیده‌اند</p></div>
<div class="m2"><p>جهان حلب خوش پریخانه‌ایست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هرگردبادی‌کزین دشت و در</p></div>
<div class="m2"><p>تامل کنی هوی دیوانه ‌ایست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر این است سنگینی خواب ما</p></div>
<div class="m2"><p>خروش قیامت هم افسانه ایست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درین انجمن فرصت ما و من</p></div>
<div class="m2"><p>همان قصهٔ عشق و پروانه‌ایست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قناعت به گوشت نگفت ای صدف</p></div>
<div class="m2"><p>که در جیب لب بستنت دانه‌ایست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفیقان تلاشی‌ که آنجا رسیم</p></div>
<div class="m2"><p>درین دشت دل نام و‌برانه‌ایست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مباشید غافل ز وضع جنون</p></div>
<div class="m2"><p>به هر زلف آشفتگی شانه‌ایست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز تحقیق خود هیچ نشکافتیم</p></div>
<div class="m2"><p>سرم در گریبان بیگانه‌ ایست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو بید‌ل توان از دو عالم‌گذشت</p></div>
<div class="m2"><p>اگر یک قدم جهد مردانه‌ایست</p></div></div>