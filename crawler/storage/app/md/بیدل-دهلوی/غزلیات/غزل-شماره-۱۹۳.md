---
title: >-
    غزل شمارهٔ ۱۹۳
---
# غزل شمارهٔ ۱۹۳

<div class="b" id="bn1"><div class="m1"><p>خارج آهنگی ندارد سبحه و زنار ما</p></div>
<div class="m2"><p>می‌دود مرکز همان سر بر خط پرگار ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ادب‌پروردگان یاد تمکین توایم</p></div>
<div class="m2"><p>موی چینی می‌فروشد ناله درکهسار ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سعی ماچون شمع‌بیتاب هوای نیستی‌ست</p></div>
<div class="m2"><p>تا پر رنگی‌ست ز خود می‌کند منقار ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرهمه‌مخمل شودخواب‌بهار اینجاتراست</p></div>
<div class="m2"><p>سایهٔ گل پر عرق‌ریزست درگلزار ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا نگه رنگ تأمل باخت پروازیم و بس</p></div>
<div class="m2"><p>چون سحر تاکی شودشبنم قفس بردارما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بوی‌گل مفت تأمل‌هاست‌گر وامی‌رسی</p></div>
<div class="m2"><p>نبض‌واری در نفس پر می‌زند بیمار ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ذره‌ایم از خجلت سامان موهومی مپرس</p></div>
<div class="m2"><p>اندک هرچیز دارد خنده بر بسیار ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شهرت‌رسوایی‌ماچون سحرپوشیده‌نیست</p></div>
<div class="m2"><p>گل ز جیب چاک می‌بندند بر دستار ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از ازل آشفتگی بنیاد تعمیر دلیم</p></div>
<div class="m2"><p>موی‌مجنون چیدن است ز سایهٔ دیوارما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یأس پیری قطع‌کرد از ما امید زندگی</p></div>
<div class="m2"><p>بسکه خم‌گشتیم افتاد از سر ما بار ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همچوعکس آب تشویش ازبنای ما نرفت</p></div>
<div class="m2"><p>مرتعش بوده‌ست‌گویی پنجهٔ معمار ما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در خور هرسطر بیدل باید ازخود رفتنی</p></div>
<div class="m2"><p>جاده‌ها بسته‌ست بر سر قاصد از طومار ما</p></div></div>