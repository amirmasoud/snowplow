---
title: >-
    غزل شمارهٔ ۲۳۲۰
---
# غزل شمارهٔ ۲۳۲۰

<div class="b" id="bn1"><div class="m1"><p>چون سبحه یک دو روز که با هم نشسته‌ایم</p></div>
<div class="m2"><p>از یکدگر گسسته فراهم نشسته‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز است چشم ما به رخ انجمن چو شمع</p></div>
<div class="m2"><p>اما در انتظار فنا هم نشسته‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چند طور عجز به غیر از صواب نیست</p></div>
<div class="m2"><p>زحمت‌کشی خیال خطا هم نشسته‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دود سپند مجلس تصویر حیرت است</p></div>
<div class="m2"><p>هر چند گل کنیم صدا هم نشسته‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غافل نه‌ایم از غم درماندگان خاک</p></div>
<div class="m2"><p>چندی چو آبله ته پا هم نشسته‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نا قدردان راحت عریان تنی مباش</p></div>
<div class="m2"><p>گاهی برون بند قبا هم نشسته‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواب غرور مخمل و دیبا ز ما مخواه</p></div>
<div class="m2"><p>بر فرش بوریای گدا هم نشسته‌ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دارد دماغ تخت سلیمان غبار ما</p></div>
<div class="m2"><p>بی پا و سر به روی هوا هم نشسته‌ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دود چراغ محفل امکان بهانه‌جوست</p></div>
<div class="m2"><p>در راه باد ما و شما هم نشسته‌ایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آسایشی به ترک مطالب نمی‌رسد</p></div>
<div class="m2"><p>در سایه‌های دست دعا هم نشسته‌ایم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر التفات نقش قدم شیوهٔ حیاست</p></div>
<div class="m2"><p>بر خاک آستان تو ما هم نشسته‌ایم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل به رنگ توأم بادام ما و تو</p></div>
<div class="m2"><p>هر چند یک دلیم جدا هم نشسته‌ایم</p></div></div>