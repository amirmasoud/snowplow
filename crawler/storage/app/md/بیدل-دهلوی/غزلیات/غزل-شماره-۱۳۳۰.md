---
title: >-
    غزل شمارهٔ ۱۳۳۰
---
# غزل شمارهٔ ۱۳۳۰

<div class="b" id="bn1"><div class="m1"><p>آب و رنگ عبرتی صرف بهارم‌ کرده‌اند</p></div>
<div class="m2"><p>پنجهٔ افسوسم از سودن نگارم ‌کرده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عالم غفلت نگردد پرده تسخیر من</p></div>
<div class="m2"><p>عبرتم در دیده بینا شکارم کرده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرد جولانم برون ازپردهٔ افسردگی‌ست</p></div>
<div class="m2"><p>نالهٔ شوقم چه شدگر نی سوارم‌کرده اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین سرشکی چند کز یادت به مژگان بسته‌ام</p></div>
<div class="m2"><p>دستگاه صد چراغان انتظارم کرده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزگارسوختنها خوش‌ که در دشت جنون</p></div>
<div class="m2"><p>هر کجا برقی‌ست نذر مشت خارم ‌کرده‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا نسیمی می‌وزد عریانی‌ام‌گل‌کرده است</p></div>
<div class="m2"><p>آتشم‌، خاکستری را پرده‌دارم کرده‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر که بندم تهمت دانش‌ که جمعی بیخرد</p></div>
<div class="m2"><p>تردماغیهای مجنون اعتبارم کرده‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سخت‌ دشوار است چون ‌آیینه‌ خود را یافتن</p></div>
<div class="m2"><p>عالمی را در سراغ خود دچارم کرده‌اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پرفشانیهای چندین ناله‌ام اما چه سود</p></div>
<div class="m2"><p>از دل افسرده جزو کوهسارم کرده‌اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>محملم در قطرگی آرایش صد موج داشت</p></div>
<div class="m2"><p>تا شدم ‌گوهر به دوش خویش بارم ‌کرده‌اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیست بید‌ل وضع من افسانه‌ساز دردسر</p></div>
<div class="m2"><p>همچو خاموشی شرات بیخمارم کرده‌اند</p></div></div>