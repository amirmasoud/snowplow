---
title: >-
    غزل شمارهٔ ۲۴۱۹
---
# غزل شمارهٔ ۲۴۱۹

<div class="b" id="bn1"><div class="m1"><p>زان تغافلگر چرا نا شاد باید زبستن</p></div>
<div class="m2"><p>ای فراموشان به ذوق یاد باید زبستن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلبلان نی الفت دام است اینجا نی قفس</p></div>
<div class="m2"><p>بر مراد خاطر صیاد باید زیستن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من نمی‌گویم به‌کلی ازتعلق‌ها برآ</p></div>
<div class="m2"><p>اندکی زبن درد سر آزاد باید زیستن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواه در دوزخ وطن ‌کن خواه با فردوس ساز</p></div>
<div class="m2"><p>عافیت هر جا نباشد شاد باید زیستن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون سپندم عمرها درکسوت افسردگی</p></div>
<div class="m2"><p>بر امید یک تپش فریاد باید زیستن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست زین دشوارترجهدی‌که ما را با فنا</p></div>
<div class="m2"><p>صلح کار عالم اضداد باید زیستن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زندگی برگردن افتاده‌ست یاران چاره چیست</p></div>
<div class="m2"><p>چند روزی هر چه باداباد باید زیستن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>موج‌گوهر در قناعتگاه قسمت خشک نیست</p></div>
<div class="m2"><p>تردماغ شرم استعداد باید زیستن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرسرمویت خم تسلیم چندین جانکنی است</p></div>
<div class="m2"><p>با هزاران تیشه یک فرهاد باید زیستن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل این هستی نمی‌سازد به تشویش نفس</p></div>
<div class="m2"><p>شمع را تاکی به راه باد باید زیستن</p></div></div>