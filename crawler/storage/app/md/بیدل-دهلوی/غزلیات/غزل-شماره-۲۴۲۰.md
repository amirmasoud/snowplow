---
title: >-
    غزل شمارهٔ ۲۴۲۰
---
# غزل شمارهٔ ۲۴۲۰

<div class="b" id="bn1"><div class="m1"><p>گر به این ساز است دور از وصل جانان زبستن</p></div>
<div class="m2"><p>زنده‌ام من‌ هم به آن ننگی ‌که نتوان زبستن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انفعالم می‌کشد از سخت جانیها مپرس</p></div>
<div class="m2"><p>کاش باشد بی‌رخت چون مرگم آسان زیستن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>موج‌گهر نیستم زندانی خویشم چرا</p></div>
<div class="m2"><p>سر به جیبم خاک‌کرد این بامدادان زبستن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم زخم خودنمایی را نمی‌باشد علاج</p></div>
<div class="m2"><p>ای شرر باید همان در سنگ پنهان زیستن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از وطن دوری و غربت هم ‌گوارای تو نیست</p></div>
<div class="m2"><p>چند خواهی ‌این چنین‌ای خانه ویران زیستن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک دودم‌کم نیست خجلت مایگیهای نفس</p></div>
<div class="m2"><p>چون سحر زین بیش نتوان سست پیمان زیستن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم چو شمع از عشرت این انجمن غافل مباش</p></div>
<div class="m2"><p>گل به سر می‌خواهد آتش در گریبان زیستن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرگذشت عالم آیینه از دیدار پرس</p></div>
<div class="m2"><p>جلوه غافل نیست از اسباب حیران زبستن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کسوت مرگم نقاب غفلت دیدار نیست</p></div>
<div class="m2"><p>در کفن دارد نگاه پیر کنعان زیستن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نعمت الوان دنیا نیست در خورد تمیز</p></div>
<div class="m2"><p>بی‌خس جاوید باید جوع دندان زبستن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر قناعت قطره آبی چون‌گهر سامان‌کند</p></div>
<div class="m2"><p>می‌توان صد سال بی‌اندیشهٔ نان زیستن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خواجه‌کاری‌کن‌که درگیرد چراغ شهرتت</p></div>
<div class="m2"><p>حیف دنیا دار و پنهانتر ز شیطان زیستن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سر به پای یکدگر چون سبحه باید بود و بس</p></div>
<div class="m2"><p>اینقدر می‌خواهد آیین مسلمان زیستن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ما وطن آوارگان را غربتی در کار نیست</p></div>
<div class="m2"><p>موج ناچار است در بحر از پریشان زیستن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بزم امکانست بیدل غافل از مردن مباش</p></div>
<div class="m2"><p>خضر اگر باشی در اینجا نیست امکان زیستن</p></div></div>