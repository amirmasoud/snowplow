---
title: >-
    غزل شمارهٔ ۱۷۶۰
---
# غزل شمارهٔ ۱۷۶۰

<div class="b" id="bn1"><div class="m1"><p>گر نه‌ای عین تماشا حیرت سرشار باش</p></div>
<div class="m2"><p>سر به سر دلدار یا آیینهٔ دلدار باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با هجوم عیش شو چون نغمهٔ ذوق وصال</p></div>
<div class="m2"><p>یا سراپا درد دل چون نالهٔ بیمار باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بال و پر فرسودهٔ دام فلک نتوان شدن</p></div>
<div class="m2"><p>گر همه مرکز شوی بیرون این پرگار باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند باید بود پیشاهنگ تحریک نفس</p></div>
<div class="m2"><p>ساز موهومی‌ که ما داریم ‌گویی تار باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صد چمن رنگ طرب در غنچه دارد خامشی</p></div>
<div class="m2"><p>ناله هر جا گل‌ کند کوته‌تر از منقار باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر همه بویی ز افسون حسد دارد دلت</p></div>
<div class="m2"><p>بر دم عقرب نشین یا بر دهان مار باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آگهی آیینه دار احتیاط افتاده است</p></div>
<div class="m2"><p>چشم اگر گردیده باشی اندکی بیدار باش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسمل ما را پر وامانده سیر عالمیست</p></div>
<div class="m2"><p>عرصهٔ کون و مکان گو یک تپیدن‌وار باش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>داغ هم رنگینیی دارد که در گلزار نیست</p></div>
<div class="m2"><p>گر نه‌ای طاووس باری رخت آتشکار باش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سیر چشمی ذره از مهر قناعت بودن‌ست</p></div>
<div class="m2"><p>پیش مردم اندکی‌، در چشم خود بسیار باش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غنچه‌ات از بیخودی فال شکفتن می‌زند</p></div>
<div class="m2"><p>ای ز سر غافل‌، برو بیمغزی دستار باش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا به‌ کی باشد دل از خجلت شماران نفس</p></div>
<div class="m2"><p>سبحه بیکار است چندی گرم استغفار باش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بی‌نیازیهای عشق آخر به هیچت می‌خرد</p></div>
<div class="m2"><p>جنس موهومی دو روزی بر سر بازار باش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یک قدم راهست بیدل از تو تا دامان خاک</p></div>
<div class="m2"><p>بر سر مژگان چو اشک استاده‌ای هشیار باش</p></div></div>