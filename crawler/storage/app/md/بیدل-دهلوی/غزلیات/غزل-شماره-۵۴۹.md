---
title: >-
    غزل شمارهٔ ۵۴۹
---
# غزل شمارهٔ ۵۴۹

<div class="b" id="bn1"><div class="m1"><p>زندگی تمهید اسباب فناست</p></div>
<div class="m2"><p>ما و من افسانهٔ خواب فناست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غافلان تا چند سودای غرور</p></div>
<div class="m2"><p>جنس این دکان همه باب فناست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مست ومخمورخیال ازخود روید</p></div>
<div class="m2"><p>ششجهت یک عالم آب فناست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اینکه امواج نفس نامیدهٔم</p></div>
<div class="m2"><p>چون به‌ خود پیچیده‌ گرداب فناست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاک دیر و کعبه‌ام منظور نیست</p></div>
<div class="m2"><p>اشک ما را سجده محراب فناست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواه هستی واشمر خواهی عدم</p></div>
<div class="m2"><p>نغمه‌ها در رهن مضراب فناست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر چه از دنیا و عقبا بشنوی</p></div>
<div class="m2"><p>حرف نامفهوم القاب فناست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنچه زین دریا نمی‌آید به دست</p></div>
<div class="m2"><p>گوهر تحقیق ناباب فناست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دورگردون یک دو دم میدان‌کشید</p></div>
<div class="m2"><p>عمر، شاگرد رسن‌تاب فناست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما نفس سرمایگان پر بسملیم</p></div>
<div class="m2"><p>پرفشانی عذر بیتاب فناست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا ابد، از نیستی نتوان گذشت</p></div>
<div class="m2"><p>خاک این وادی‌ گل از آب فناست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل از طور جنون غافل مباش</p></div>
<div class="m2"><p>خاک بر سر کردن آداب فناست</p></div></div>