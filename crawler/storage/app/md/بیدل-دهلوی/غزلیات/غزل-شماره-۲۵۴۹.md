---
title: >-
    غزل شمارهٔ ۲۵۴۹
---
# غزل شمارهٔ ۲۵۴۹

<div class="b" id="bn1"><div class="m1"><p>سوخت چون موج گهر بال تپیدنهای من</p></div>
<div class="m2"><p>عقدهٔ دل‌گشت آخر آرمیدنهای من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آبیار مزرعم یارب تب سودای کیست</p></div>
<div class="m2"><p>درد می‌جوشد چو تبخال از دمیدنهای من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد بیابان آرزو بی‌جستجو طی می‌شود</p></div>
<div class="m2"><p>تا به نومیدی اگر باشد رسیدنهای من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آه دردم تهمت آلود رعونت نیستم</p></div>
<div class="m2"><p>رستن است از قید هستی سرکشیدنهای من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از مقیمان بهارستان ضعف پیری‌ام</p></div>
<div class="m2"><p>گل زنقش پا به سر دارد خمیدنهای من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عالمی را کرد حسرت بسمل ناز و نیاز</p></div>
<div class="m2"><p>دور باش غمزه و دزدیده دیدنهای من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از سر کویت غبارم برده اند اما هنوز</p></div>
<div class="m2"><p>می‌تپد هر ذره در یاد تپیدنهای من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جرأت بیحاصلی خجلت گداز کس مباد</p></div>
<div class="m2"><p>اشک شد پرواز چون چشم از پریدنهای من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسکه اجزایم زدرد ناتوانیها گدا‌خت</p></div>
<div class="m2"><p>چون صدا شد عینک دیدن شنیدنهای من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وحشتم غیر از کلاه بی‌نشانی نشکند</p></div>
<div class="m2"><p>دامن رنگم بلند افتاده چیدنهای من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همچو اشک از شرم جرأت بایدم گردید آب</p></div>
<div class="m2"><p>تا یکی لغزش تراود از دویدنهای من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وحشتم فال‌گرفتاریست بیدل همچو موج</p></div>
<div class="m2"><p>نیست بی‌ایجاد دام از خود رمیدنهای من</p></div></div>