---
title: >-
    غزل شمارهٔ ۱۲۴۶
---
# غزل شمارهٔ ۱۲۴۶

<div class="b" id="bn1"><div class="m1"><p>همچو مینا غنچهٔ رازم بهار آهنگ شد</p></div>
<div class="m2"><p>پرتوی از خون دل بیرون دوید و رنگ شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس که ‌در یادت به چندین رنگ حسرت سوختم</p></div>
<div class="m2"><p>چون پر طاووس داغم عالم نیرنگ شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوه تمکینی به این افسردگیها حیرت است</p></div>
<div class="m2"><p>بس که زیر بار دل ماندم صدا هم سنگ شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در طلسم بستن مژگان فضایی داشتم</p></div>
<div class="m2"><p>تا نگه آغوش پیدا کرد عالم تنگ شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیکرم در جست‌وجویت رفت همدوش نفس</p></div>
<div class="m2"><p>رشتهٔ این ساز از فرسودگی آهنگ شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در شکنج پیری‌ام هر مو زبان ناله‌ای است</p></div>
<div class="m2"><p>از خمیدنها سراپایم طرف با چنگ شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن‌قدر وامانده‌ام کز الفتم نتوان گذشت</p></div>
<div class="m2"><p>اشک هم در پای من افتاد و عذر لنگ شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جوهر خط آخر از آیینه‌ات میگون دمید</p></div>
<div class="m2"><p>دود هم از شعلهٔ حسن تو آتش‌رنگ شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کسب آگاهی کدورت‌خانه تعمیر است و بس</p></div>
<div class="m2"><p>هر قدر آیینه شد دل زیر مشق زنگ شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هیچکس حسرتکش بی‌مهری خوبان مباد</p></div>
<div class="m2"><p>آرزو بشکست ما را تا دل او سنگ شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل از درد وطن خون ‌گشت ذوق عبرتم</p></div>
<div class="m2"><p>بس که یاد آشیان کردم قفس هم تنگ شد</p></div></div>