---
title: >-
    غزل شمارهٔ ۶۶۴
---
# غزل شمارهٔ ۶۶۴

<div class="b" id="bn1"><div class="m1"><p>دل انجمن صد طرب ازیاد وصالست</p></div>
<div class="m2"><p>آبادکن خانهٔ آیینه خیالست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی فرصت عیش ست درین باغ‌که‌گل را</p></div>
<div class="m2"><p>گر گردش‌رنگ‌است‌همان‌گردش سالست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای ذره مفرسای به پرواز توهم</p></div>
<div class="m2"><p>خورشید هم از آینه‌داران زوالست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن مشت غبارم‌که به پرواز تپیدن</p></div>
<div class="m2"><p>در حسرت دامان نسیمم پر و بالست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آیینهٔ‌گل از بغل غنچه جدا نیست</p></div>
<div class="m2"><p>دل‌گر شکند سربسر آغوش وصالست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرگام به راه طلبت رفته‌ام از خویش</p></div>
<div class="m2"><p>نقش قدمم آینهٔ‌گردش‌ حالست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در خلوت دل از تو تسلی نتوان شد</p></div>
<div class="m2"><p>چیزی‌که در آیینه توان دید مثالست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد جوهر نظاره‌ام آیینهٔ حیرت</p></div>
<div class="m2"><p>بالیدگی داغ مه از زخم هلالست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیدل من وآن دولت بی‌درد سرفقر</p></div>
<div class="m2"><p>کز نسبت او چینی خاموش سفالست</p></div></div>