---
title: >-
    غزل شمارهٔ ۱۱۰۰
---
# غزل شمارهٔ ۱۱۰۰

<div class="b" id="bn1"><div class="m1"><p>طبع سرکش خاک‌گشت و چشم شرمی وانکرد</p></div>
<div class="m2"><p>شمع سر بر نقش پا سایید و خم پیدا نکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمرها شد آمد و رفت نفس جان می‌کند</p></div>
<div class="m2"><p>ما و من بیرون در فرسود و در دل جا نکرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زندگی بیع و شرای ما و من بی‌سود یافت</p></div>
<div class="m2"><p>کس چه سازد آرمیدن با نفس سودا نکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرکشی گر بر دماغت زد شکست آماده باش</p></div>
<div class="m2"><p>خاک از شغل عمارت عافیت برپا نکرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سعی فطرت دور گرد معنی تحقیق ماند</p></div>
<div class="m2"><p>غیرت او داشت افسونی‌که ما را ما نکرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکجا رفتم نرفتم نیم‌گام از خود برون</p></div>
<div class="m2"><p>صد قیامت رفت وامروز مرا فردا نکرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با خیالت غربتم صد ناز دارد بر وطن</p></div>
<div class="m2"><p>جان فدای بی کسی هاکز توام تنها نکرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دامن خود گیر و از تشویش دهر ‌آزاد باش</p></div>
<div class="m2"><p>قطره را تا جمع شد دل یادی از دریا نکرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فرع را از اصل خویش آگاه باید زیستن</p></div>
<div class="m2"><p>شیشه را سامان مستی غافل از خارا نکرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>انقلاب ساز وحدت‌کثرت موهوم نیست</p></div>
<div class="m2"><p>ربط بی‌اجزاییی ما را خیال اجزا نکرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جود مطلق درکمین سایل‌ست اما چه سود</p></div>
<div class="m2"><p>شرم تکلیف اجابت دست ما بالا نکرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نام عنقا نقشبند پردهٔ ادراک نیست</p></div>
<div class="m2"><p>هیچکس زین بزم فهم آن پری پیدا نکرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بیدل از نقش قدم باید عیار ماگرفت</p></div>
<div class="m2"><p>ناتوانی سایه را هم زیردست ما نکرد</p></div></div>