---
title: >-
    غزل شمارهٔ ۵۷۵
---
# غزل شمارهٔ ۵۷۵

<div class="b" id="bn1"><div class="m1"><p>گر به سیر انجمن یا گشت‌ گلشن رفته است</p></div>
<div class="m2"><p>شمع‌ما هرسو همین یک سرزگردن رفته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مزرعی چون کاغذ آتش‌زده گل کرده ایم</p></div>
<div class="m2"><p>تا نظر بر دانه می‌دوزیم خرمن رفته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کاشکی باکلفت افسردگی می‌ساخیم</p></div>
<div class="m2"><p>بر بهار ما قیامت از شکفتن رفته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>انتظارت رنگ نم نگذاشت در چشم ترم</p></div>
<div class="m2"><p>تا مقشر گشت این بادام روغن رفته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهد صیقل صدهزار آیینه با زنگار برد</p></div>
<div class="m2"><p>خانه‌ها زین خاکدان بر باد رفتن رفته است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غنچه‌واری هیچکس با عافیت سودا نکرد</p></div>
<div class="m2"><p>همچو گل اینجا گریبانها به دامن رفته است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلقی‌از بیدانشی‌تمکین‌به‌حرف‌و صوت باخت</p></div>
<div class="m2"><p>سنگ این‌کهساریکسر در فلاخن رفته است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زندگی زین انجمن یک گام آزادی نخواست</p></div>
<div class="m2"><p>هرکه را دیدیم زاینجا بعد مردن رفته است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نقش پایی چند از عجز تلاش افسرده‌ایم</p></div>
<div class="m2"><p>نام واماندن بجا مانده‌ست رفتن رفته است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خانه را نتوان سیه‌کرد از غرور روشنی</p></div>
<div class="m2"><p>نور می‌پنداری و دودی به روزن رفته است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرچه از خود می‌بریم آنجا فضولی می‌بریم</p></div>
<div class="m2"><p>جای قاصد انفعال نامه بردن رفته است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیستم بیدل حریف انتظار خوشدلی</p></div>
<div class="m2"><p>فرصت از هرکس‌که‌باشد یان از من رفته است</p></div></div>