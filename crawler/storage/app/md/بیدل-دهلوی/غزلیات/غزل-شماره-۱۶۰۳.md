---
title: >-
    غزل شمارهٔ ۱۶۰۳
---
# غزل شمارهٔ ۱۶۰۳

<div class="b" id="bn1"><div class="m1"><p>به سعی یأس نفس خامشی بیان‌ گردید</p></div>
<div class="m2"><p>به خود شکستن دل سرمهٔ فغان ‌گردید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در این زمانه ز بس طبع دون رواج‌ گرفت</p></div>
<div class="m2"><p>عنان کسب کمالات سوی نان گردید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گهر به علت خودداری از محیط جداست</p></div>
<div class="m2"><p>نباید این همه بر طبعها گران ‌گردید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو شعله وحشت ما حیله ‌ساز عافیتی‌ست</p></div>
<div class="m2"><p>به هر کجا پر ما ریخت آشیان ‌گردید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهار چشمک رنگی نیاز وحشت داشت</p></div>
<div class="m2"><p>شرار کاغذ ما نیز گلفشان گردید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آن بساط ‌که دل محمل تپش آراست</p></div>
<div class="m2"><p>شکستن جرس اشک کاروان گردید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو صبح نیم ‌نفس‌ گر ز زندگی باقیست</p></div>
<div class="m2"><p>برون ز گرد کدورت نمی‌توان‌گردید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به روزگار مثل‌گشت بی‌زبانی من</p></div>
<div class="m2"><p>خموشی آنهمه خون شد که داستان‌ گردید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جهان حادثه از وضع من ‌گرفت سبق</p></div>
<div class="m2"><p>بقدر گردش رنگ من آسمان ‌گردید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو طفل اشک مپرس از رسایی طبعم</p></div>
<div class="m2"><p>ز خود گذشتم اگر درس من روان ‌گردید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عدم سراغ جهان تحیرم بیدل</p></div>
<div class="m2"><p>غبار من به هوای که ناتوان‌گردید</p></div></div>