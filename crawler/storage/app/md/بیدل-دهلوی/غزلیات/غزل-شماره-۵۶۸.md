---
title: >-
    غزل شمارهٔ ۵۶۸
---
# غزل شمارهٔ ۵۶۸

<div class="b" id="bn1"><div class="m1"><p>هرکجا وحشتی از آتشم افروخته است</p></div>
<div class="m2"><p>برق در اول پرواز نفس سوخته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه خیال است دل از داغ تسلی‌گردد</p></div>
<div class="m2"><p>اخگری چشم به خاکستر خود دوخته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لاف را آینه‌پرداز محبت مکنید</p></div>
<div class="m2"><p>به نفس هیچکس این شعله نیفروخته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نتوان محرم تحقیق شد از علم و عمل</p></div>
<div class="m2"><p>و ضعها ساخته و ما و من آموخته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پاس اسرار محبت به هوس ناید راست</p></div>
<div class="m2"><p>شمع بر قشقه و زنار چها سوخته است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای نفس ‌مایه دکانداری غلفت تا چند</p></div>
<div class="m2"><p>آسمان جنس سلامت به تو نفروخته است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از قماش بد و نیک دو جهان بیخبریم</p></div>
<div class="m2"><p>چون حیا پیرهن ما نظر دوخته است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ذره‌ایی نیست که خورشید نمایی نکند</p></div>
<div class="m2"><p>گرد راهت چقدر آینه اندوخته است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر نه بیدل سبق از مکتب مجنون دارد</p></div>
<div class="m2"><p>اینقدر چاک گریبان زکه آموخته است</p></div></div>