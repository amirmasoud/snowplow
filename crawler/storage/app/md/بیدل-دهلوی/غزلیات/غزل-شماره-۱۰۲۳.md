---
title: >-
    غزل شمارهٔ ۱۰۲۳
---
# غزل شمارهٔ ۱۰۲۳

<div class="b" id="bn1"><div class="m1"><p>غبار ما به جز این پر شکستنی‌که ندارد</p></div>
<div class="m2"><p>کجا رود به امید نشستنی‌که ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار قافله پا درگل است و می‌رود از خود</p></div>
<div class="m2"><p>به فرصت و نفس بار بستنی‌که ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه زخمهاکه نچیده‌ست دل به فرقت یاران</p></div>
<div class="m2"><p>ز ناخن المی سینه خستنی ‌که ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سپند مجمر تصویرهمچو من به‌که نالد</p></div>
<div class="m2"><p>ز وحشتی‌که فسرده‌ست و جستنی‌که ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گذشته است جهانی ز اوج منتظر عنقا</p></div>
<div class="m2"><p>به بال دعوی از خویش رستنی ‌که ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اسیر حرص چه‌کوشش‌کند به ناز رهایی</p></div>
<div class="m2"><p>بر این دکان هوس دل نبستنی ‌که ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به حیرتم چه فسون است دام حیرت بیدل</p></div>
<div class="m2"><p>تعلقی که نبودش‌، گسستنی که ندارد</p></div></div>