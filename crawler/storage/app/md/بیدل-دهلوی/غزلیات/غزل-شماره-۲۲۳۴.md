---
title: >-
    غزل شمارهٔ ۲۲۳۴
---
# غزل شمارهٔ ۲۲۳۴

<div class="b" id="bn1"><div class="m1"><p>زین ‌گریه اگر باد برد حاصل خاکم</p></div>
<div class="m2"><p>چون صبح چکد شبنم اشک از دل چاکم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست من و دامان تمنای وصالت</p></div>
<div class="m2"><p>نتوان چو نفس‌کردن ازین آینه پاکم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آبله‌ام منع دویدن نتوان کرد</p></div>
<div class="m2"><p>انگور نگردد گره ریشهٔ تاکم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی موج به ساحل نرسد کشتی خاشاک</p></div>
<div class="m2"><p>از تیغ اجل نیست در این معرکه باکم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گردم چمن رنگ نبالد چه خیال‌ست</p></div>
<div class="m2"><p>عمری‌ست که در راه تمنای تو خاکم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دارد نفسم پیچ و خم طرهٔ رازی</p></div>
<div class="m2"><p>کان را نبود شانه مگر سینهٔ چاکم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از بسمل شمشیر جفا هیچ مپرسید</p></div>
<div class="m2"><p>دارم به نظر ذوق هلاکی که هلاکم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای همت عالی نظران دست نگاهی</p></div>
<div class="m2"><p>تا چند کشد پستی طالع به مغاکم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل شمع خیالی‌ست‌ که تا حشر نمیرد</p></div>
<div class="m2"><p>زنهار تکلف مفروزید به خاکم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل به خیال مژهٔ چشم سیاهی</p></div>
<div class="m2"><p>امروز سیه مست‌تر از سایهٔ تاکم</p></div></div>