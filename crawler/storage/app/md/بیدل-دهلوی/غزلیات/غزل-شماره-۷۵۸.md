---
title: >-
    غزل شمارهٔ ۷۵۸
---
# غزل شمارهٔ ۷۵۸

<div class="b" id="bn1"><div class="m1"><p>صنعت نیرنگ دل بر فطرت‌ کس فاش نیست</p></div>
<div class="m2"><p>آینه تصویرها می‌بندد و نقاش نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوش اشیا، اشتباه ذات بی‌همتاش نیست</p></div>
<div class="m2"><p>کثرت صورت غبار وحدت نقاش نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کفر و دین‌، شک و یقین سازی‌ست بی‌آهنگ ربط</p></div>
<div class="m2"><p>هوش اگر دا‌ری بفهم ای بیخبر پرخاش نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عقل‌گو خون شو به دور اندیشی رد و قبول</p></div>
<div class="m2"><p>در حضورآباد استغنا برو، یا باش نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرچه خواهی در غبار نیستی آماده گیر</p></div>
<div class="m2"><p>ای تنک سرمایه، چون هستی‌، عدم قلاش نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون حباب این چیدن و واچیدن افسون هواست</p></div>
<div class="m2"><p>خیمهٔ اوهام را غیر از نفس فراش نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی‌تکلف زی تب و تاب امید و یاس چند</p></div>
<div class="m2"><p>عالم شوق است اینجا جای بوک وکاش نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شوخ چشمی برنمی‌دارد ادبگاه جلال</p></div>
<div class="m2"><p>قدردان آفتاب امروز جز خفاش نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>موج دریای تعین‌گر همین جوش من است</p></div>
<div class="m2"><p>آنچه خلق‌، آب بقا دارد،‌گمان جز شاش نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ربش گاوی چیست‌؟ امید مراد از مردگان</p></div>
<div class="m2"><p>زین مزارات آنکه چیزی یافت جز نباش نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگذر از افسانهٔ تحقیق‌، فهم این است و بس</p></div>
<div class="m2"><p>تا تو آگاهی رموز هیچ چیزت فاش نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نوبهار آیینه در دست از هجوم رنگ و بوست</p></div>
<div class="m2"><p>بید‌ل این الفاظ غیر از صورت معناش نیست</p></div></div>