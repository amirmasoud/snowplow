---
title: >-
    غزل شمارهٔ ۱۴۳۲
---
# غزل شمارهٔ ۱۴۳۲

<div class="b" id="bn1"><div class="m1"><p>گر طمع دست طلب وامی‌کند</p></div>
<div class="m2"><p>بر قناعت خنده لب وامی‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرم می‌جوشی به لذات جهان</p></div>
<div class="m2"><p>این شکر دکان تب وامی‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>موج گوهر باش کارت بسته نیست</p></div>
<div class="m2"><p>ناخنی دارد ادب وامی‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فتح باب عافیت وقف ‌کسی‌ست</p></div>
<div class="m2"><p>کز جبین چین غضب وامی‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شیشه مشکن ورنه دل هم زین بساط</p></div>
<div class="m2"><p>راه کهسار حلب وامی‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سایهٔ طوبی نباشد گو مباش</p></div>
<div class="m2"><p>جای ما برگ عنب وامی‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای چراغ محفل شیب و شباب</p></div>
<div class="m2"><p>صبح ته ‌گیر آنچه شب وامی‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شرم‌کم دارد ز ناموس عدم</p></div>
<div class="m2"><p>هر که طومار نسب وامی‌کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پنبه از مینا به غفلت برمدار</p></div>
<div class="m2"><p>این پری بند قصب وامی‌کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی‌ادب بر غنچه نگشایید دست</p></div>
<div class="m2"><p>این‌ گره را گل به لب وامی‌کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عقده ناپیداست در تار نفس</p></div>
<div class="m2"><p>لیک بیدل روز و شب وامی‌کند</p></div></div>