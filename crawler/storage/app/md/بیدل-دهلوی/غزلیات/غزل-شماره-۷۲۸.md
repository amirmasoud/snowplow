---
title: >-
    غزل شمارهٔ ۷۲۸
---
# غزل شمارهٔ ۷۲۸

<div class="b" id="bn1"><div class="m1"><p>سرشکم نسخهٔ دیوانهٔ کیست</p></div>
<div class="m2"><p>جگر آیینه‌دار شانهٔ کیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جنون می‌جوشد از طرز کلامم</p></div>
<div class="m2"><p>زبانم لغزش مستانهٔ کیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم‌ گر نیست فانوس خیالت</p></div>
<div class="m2"><p>نفس بال و پر پروانهٔ‌ کیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خود رفتم ولی بویی نبردم</p></div>
<div class="m2"><p>که رنگم ‌گردش پیمانهٔ‌ کیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خموشی ناله می‌گردد مپرسید</p></div>
<div class="m2"><p>که آن ناآشنا بیگانهٔ‌ کیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندارد مزرع امکان دمیدن</p></div>
<div class="m2"><p>تبسم آبیار دانهٔ کیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیاوردیم مژگانی فراهم</p></div>
<div class="m2"><p>نمک‌پاش جگر افسانهٔ کیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شعورم رنگ ‌گرداند از که پرسم</p></div>
<div class="m2"><p>ز خود رفتن ره کاشانهٔ ‌کیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گداز دل که سیل خانمانهاست</p></div>
<div class="m2"><p>عرق ‌پروردهٔ دیوانهٔ کیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل عاشق به استغنا نیرزد</p></div>
<div class="m2"><p>خموشی وصع گستاخانهٔ ‌کیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به پیری هم نفهمیدیم افسوس</p></div>
<div class="m2"><p>که دنیا بازی طفلانهٔ ‌کیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به دیر و کعبه‌ کارت چیست بیدل</p></div>
<div class="m2"><p>اگر فهمیده ای دل حانهٔ ‌کیست</p></div></div>