---
title: >-
    غزل شمارهٔ ۲۹۴
---
# غزل شمارهٔ ۲۹۴

<div class="b" id="bn1"><div class="m1"><p>ز برق این تحیر آب شد آیینهٔ دل‌ها</p></div>
<div class="m2"><p>که ره تا محمل لیلی‌ست بیرون‌گرد محمل‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کجا راحت‌، چه آسودن که از نایابی مطلب</p></div>
<div class="m2"><p>به پای جستجو چون آبله خون گشت منزل‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه دنیا و چه عقبا، سد را‌ه تست ای غافل</p></div>
<div class="m2"><p>بیا بگذر که از بهر گذشتن‌هاست حایل‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درین مزرع چه لازم خرمن‌آرای هوس بودن</p></div>
<div class="m2"><p>دلی باید به دست آری همین تخم است حاصل‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دشت انتظارت از بیاض چشم مشتاقان</p></div>
<div class="m2"><p>سفیدی کرد آخر راه از خود رفتن دل‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دماغی می‌رسانم از شکست شیشهٔ رنگی</p></div>
<div class="m2"><p>به خون رفته پرواز دگر دارند بسمل‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز پاس آبروی احتیاج ما مشو غافل</p></div>
<div class="m2"><p>به بازار کرم گوهرفروشانند سایل‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ندارد صید حسن از دامگاه عشق‌، آزادی</p></div>
<div class="m2"><p>همان یک حلقهٔ آغوش مجنون است محمل‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما و من اثبات حق در گوش می‌آید</p></div>
<div class="m2"><p>نوای طرفه‌ای دارد شکست رنگ باطل‌ها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خزان گلشن امکان بهار واجبی دارد</p></div>
<div class="m2"><p>تراوش می‌‌کند حق از شکست رنگ باطل‌ها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زبان شمع فهمیدم‌، ندارد غیر ازین حرفی</p></div>
<div class="m2"><p>که گر در خود توان آتش زدن مفت است محفل‌ها</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تسلسل اینقدر در دور بی‌ربطی نمی‌باشد</p></div>
<div class="m2"><p>گرو از سبحه برد امروز برهم خوردن دل‌ها</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کنار عافیت گم بود در بحر طلب بیدل</p></div>
<div class="m2"><p>شکست از موج ما گل کرد بیرون ریخت ساحل‌ها</p></div></div>