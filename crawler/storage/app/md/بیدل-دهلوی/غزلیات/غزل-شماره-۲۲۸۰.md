---
title: >-
    غزل شمارهٔ ۲۲۸۰
---
# غزل شمارهٔ ۲۲۸۰

<div class="b" id="bn1"><div class="m1"><p>بعد ازین در گوشهٔ دل چون نفس جا می‌کنم</p></div>
<div class="m2"><p>چشم می‌پوشم جهانی را تماشا می‌کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان دهان بی‌نشان هرگاه می‌آیم به حرف</p></div>
<div class="m2"><p>بر لب ذرات امکان مهر عنقا می‌کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا چه پیش آید چو شمعم زین شبستان خیال</p></div>
<div class="m2"><p>صیقل آیینه زان نقش‌ کف پا می‌کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مدعای دل به لب دادن قیامت داشته‌ست</p></div>
<div class="m2"><p>رو به ناخن می‌تراشم‌ کاین‌ گره وا می‌کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی‌تمیزی کفر و اسلامم برون آورده‌اند</p></div>
<div class="m2"><p>هرچه باشد بس که محتاجم تقاضا می‌کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقد فطرت اینقدر مصروف نادانی مباد</p></div>
<div class="m2"><p>خانه بازار است من در پرده سودا می‌کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از چراغ دیدهٔ خفاش می‌گیرم بلد</p></div>
<div class="m2"><p>تا سراغ خانهٔ خورشید پیدا می‌کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون‌ گهر خودداری‌ام تاکی در ساحل زند</p></div>
<div class="m2"><p>دست می‌شویم ز خویش و سیر دریا می‌کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برکه نالم از عقوبت‌های بیداد امل</p></div>
<div class="m2"><p>آه از امروزی‌ که صرف فکر فردا می‌کنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نالهٔ دردی ‌گر از من بشنوی معذور دار</p></div>
<div class="m2"><p>غرقهٔ توفان عجزم دست بالا می‌کنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل از سامان مستی‌های اوهامم مپرس</p></div>
<div class="m2"><p>دل به حسرت می‌گدازم می به مینا می‌کنم</p></div></div>