---
title: >-
    غزل شمارهٔ ۱۸۱
---
# غزل شمارهٔ ۱۸۱

<div class="b" id="bn1"><div class="m1"><p>تجدید سحرکاری‌ست در جلوه‌زار عنقا</p></div>
<div class="m2"><p>صدگردش است و یک‌گل رنگ‌بهار عنقا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچند نوبهاریم یا جوش لاله‌زاریم</p></div>
<div class="m2"><p>باغ دگر نداریم غیر ازکنار عنقا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سطری نخواند فطرت ز درسگاه تحقیق</p></div>
<div class="m2"><p>تقویمها کهن‌کرد امسال و پار عنقا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آیینه جزتحیر اینجا چه نقش بندد</p></div>
<div class="m2"><p>از رنگ شرم دارد صورت‌نگار عنقا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تسلیم‌عشق بودن مفت‌است هرچه باشد</p></div>
<div class="m2"><p>ما را چه‌کار وکو بار درکار و بار عنقا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شهرت‌پرستی وهم تا چند باید اینجا</p></div>
<div class="m2"><p>نقش نگین رهاکن ای نامدار عنقا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم‌صحبتیم و ما را ازیکدگر خبر نیست</p></div>
<div class="m2"><p>عنقا چه وانمایدگر شد دچار عنقا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نایابی مطالب معدوم کرد ما را</p></div>
<div class="m2"><p>دیگرکسی چه یابد در انتظار عنقا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرگ است آخرکار عبرت‌نمای هستی</p></div>
<div class="m2"><p>غیر از عدم‌که خندد بر روزگار عنقا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زیرپرندگردون‌، رسواست خلق مجنون</p></div>
<div class="m2"><p>عریانی‌که پوشد این جامه‌وار عنقا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتیم بی‌نشانی رنگی به جلوه آرد</p></div>
<div class="m2"><p>ما را نمود بر ما آیینه‌دار عنقا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در خاکدان عبرت غیر از نفس چه داریم</p></div>
<div class="m2"><p>پر روشناست بیدل شمع مزار عنقا</p></div></div>