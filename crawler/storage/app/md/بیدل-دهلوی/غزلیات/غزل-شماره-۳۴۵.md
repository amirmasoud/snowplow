---
title: >-
    غزل شمارهٔ ۳۴۵
---
# غزل شمارهٔ ۳۴۵

<div class="b" id="bn1"><div class="m1"><p>وقت پیری شرم دارید از خضاب</p></div>
<div class="m2"><p>مو، سیاهی دیده‌است اینجابه‌خواب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم دقت جوهری پیداکنید</p></div>
<div class="m2"><p>جز به روزن ذره‌کم دید آفتاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اعتبار‌ات آنچه دارد ذلت است</p></div>
<div class="m2"><p>تاگهرگل کرد رفت از قطره آب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم بستن رمز معنی خواندن است</p></div>
<div class="m2"><p>نقطه می‌باشد دلیل انتخاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جمع علم افلاس می‌آرد نه جاه</p></div>
<div class="m2"><p>بیشترها پوست می‌پوشدکتاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زبن بهارت آنچه آید در نظر</p></div>
<div class="m2"><p>عبرتی‌گردیده باشد بی‌نقاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوزعشقی نیست ورنه روشن است</p></div>
<div class="m2"><p>همچو شمعت پای تا سر فتح باب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز روانی نیست در درس نفس</p></div>
<div class="m2"><p>سکته‌می‌خواند ز لکنت‌شیخ و شاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>انفعالم خودنمایی می‌کند</p></div>
<div class="m2"><p>غم ندارد در جبین موج سراب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فرع از بس مایل اصل خود است</p></div>
<div class="m2"><p>شیشه را انگور می‌داند شراب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فرصت از خودگذشتن هم‌کم است</p></div>
<div class="m2"><p>یک عرق پل بر نفس بند ای حباب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از مکافات عمل غافل مباش</p></div>
<div class="m2"><p>آتش ایمن نیست از اشک کباب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ما و من بی‌نسبت است آنجاکه اوست</p></div>
<div class="m2"><p>با کتان ربطی ندارد ماهتاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن شکارافکن به خونم تر نخواست</p></div>
<div class="m2"><p>چشم و مژگان بود فتراک و رکاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بیدل استغنا همین یأس است و بس</p></div>
<div class="m2"><p>دست بردار از دعای مستجاب</p></div></div>