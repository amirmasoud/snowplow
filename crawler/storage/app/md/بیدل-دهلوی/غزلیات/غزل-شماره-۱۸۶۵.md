---
title: >-
    غزل شمارهٔ ۱۸۶۵
---
# غزل شمارهٔ ۱۸۶۵

<div class="b" id="bn1"><div class="m1"><p>هرکجاکردم به یاد سجده‌ات ساز رکوع</p></div>
<div class="m2"><p>چون مه نو تا فلک رفتم به پرواز رکوع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش از آن کز خاک من بالد نهال زندگی</p></div>
<div class="m2"><p>می‌رسد از بار دل در گوشم آواز رکوع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیچ و تاب موجها یکسرگهرگردیدن است</p></div>
<div class="m2"><p>سجده انجام است هر جا دیدی آغاز رکوع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شخص تسلیمی ز پرواز هوسها شرم‌دار</p></div>
<div class="m2"><p>با هوا کاری ندارد سرنگون تاز رکوع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما ضعیفان را به سامان سلیمانی بس است</p></div>
<div class="m2"><p>سجده ایجاد نگین و خاتم انداز رکوع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر منافق از تواضع صاحب دین می‌شود</p></div>
<div class="m2"><p>تیغ هم خواهد نمازی شد به پرواز رکوع</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راست می‌تازم چو اشک از دیده تا دامان خاک</p></div>
<div class="m2"><p>بر نمی‌دارد دماغ سجده‌ام ناز رکوع</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرکشیها زبن ادا آغوش رحمت می‌شود</p></div>
<div class="m2"><p>دیگر ای غافل چه می‌خواهی ز اعجاز رکوع</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیکرت خم‌ کرد پیری از فنا غافل مباش</p></div>
<div class="m2"><p>سخت نزدیکست بیدل سجده با ساز رکوع</p></div></div>