---
title: >-
    غزل شمارهٔ ۲۵۸۰
---
# غزل شمارهٔ ۲۵۸۰

<div class="b" id="bn1"><div class="m1"><p>ای ز عنایت آشکار شخص تو و مثال تو</p></div>
<div class="m2"><p>آینهٔ جمال تو آینهٔ جمال تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تب‌ و تاب آب و گل‌ تا تک و تاز جان و دل</p></div>
<div class="m2"><p>ریشهٔ‌ کس نمی‌دود در چمن خیال تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چرخ به صد کمند چین‌ بوسه زده است‌ بر زمین</p></div>
<div class="m2"><p>بس که بلند جسته است‌ گرد رم غزال تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر در ناز کبریا چند غبار ماسوا</p></div>
<div class="m2"><p>در کف وهم من‌ که داد آینهٔ محال تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این بم و زیر و قیل و قال نیست به ساز لایزال</p></div>
<div class="m2"><p>نقص و کمال فهم ماست بدر تو و هلال تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خلق ز سعی نارسا سوخت جبین به نقش پا</p></div>
<div class="m2"><p>برهمه داغ سایه بست سرکشی نهال تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیشهٔ ساعت فلک از چه حساب دم زند</p></div>
<div class="m2"><p>راه نفس‌ گرفته است غیرت ماه و سال تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیری‌ام از قد دو تا راه نبرد هیچ جا</p></div>
<div class="m2"><p>هم به در تو می‌برم حلقهٔ انفعال تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تشنهٔ بوس آن لبم لیک ز ننگ ناکسی</p></div>
<div class="m2"><p>جرأتم آب می‌کند از تری زلال تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باید از اقتضای شوق بر سر غفلتم‌ گریست</p></div>
<div class="m2"><p>از تو جدا چسان شوم تا طلبم وصال تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طایر آشیان عجز ناز فروش حسرت است</p></div>
<div class="m2"><p>رنگ شکسته می‌پرد بیدل خسته بال تو</p></div></div>