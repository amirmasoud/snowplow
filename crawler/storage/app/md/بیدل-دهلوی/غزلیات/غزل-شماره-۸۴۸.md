---
title: >-
    غزل شمارهٔ ۸۴۸
---
# غزل شمارهٔ ۸۴۸

<div class="b" id="bn1"><div class="m1"><p>شب هجوم جلوه او در خیالم جا گرفت</p></div>
<div class="m2"><p>آنقدر بالید دل‌ کایینه در صحرا گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازدل روشن ملایم طینتی را چاره نیست</p></div>
<div class="m2"><p>پنبه خود رایی تواند از سر میناگرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سعی‌ گردون از زمین مشکل‌ که بردارد مرا</p></div>
<div class="m2"><p>قطر‌‌ه را ازدست خاک تشنه نتوان واگرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در گلستانی ‌که بلبل بود هر برگ گلش</p></div>
<div class="m2"><p>پیکرم را خامشی چون غنچه سرتا پا گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخت‌ نایاب‌ است‌ مطلب‌ ورنه‌ کوشش‌ کم نبود</p></div>
<div class="m2"><p>احتیاج از ناامیدی رنگ استغنا گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تاکی از اندیشهٔ تمکین‌گرانجان زیستن</p></div>
<div class="m2"><p>قراهٔ ما را چوگوهر ‌ل در این دپاکرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر بلند افتد چوگردون نشئهٔ وارستگی</p></div>
<div class="m2"><p>می‌توان دامان همت از سر دنیا گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در ریاض دهر، ما را سبز کرد آزادگی</p></div>
<div class="m2"><p>بی‌بریها اینقدر، چون سرو، دست ماگرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زبن همه اسباب نومیدی چه برگیردکسی</p></div>
<div class="m2"><p>آنچه می‌باید ‌گرفتن دست ناگیرا گرفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عقده‌ای ازکار ما نگشود سعی نارسا</p></div>
<div class="m2"><p>ناخن تدبیر ما آخر دل ما را گرفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چشم بند و زور بر دل‌کن‌که در آفاق نیست</p></div>
<div class="m2"><p>آنقدر اوجی‌که یک مژگان توان بالاگرفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا شود بیدل به نامت سکهٔ آسودگی</p></div>
<div class="m2"><p>خاکساری در نگین باید چو نقش پا گرفت</p></div></div>