---
title: >-
    غزل شمارهٔ ۶۴۹
---
# غزل شمارهٔ ۶۴۹

<div class="b" id="bn1"><div class="m1"><p>تو محو خواب و در سیرکن‌فکان بازست</p></div>
<div class="m2"><p>مبند چشم‌که آغوش امتحان بازست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین طربکده حیف است ساز افسردن</p></div>
<div class="m2"><p>گره مشوکه زمین تا به آسمان بازست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کجا دمید سحرکز چمن جنون نشکفت</p></div>
<div class="m2"><p>تبسمی که گریبان عاشقان بازست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به معبدی‌که خموشان هلاک نام تواند</p></div>
<div class="m2"><p>چو سبحه بر دریک حرف صد دهان بازست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هر طرف‌گذری سیر نرگسستان‌کن</p></div>
<div class="m2"><p>به قدر نقش قدم چشم دوستان بازست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به پیش خلق ز انداز عالم معقول</p></div>
<div class="m2"><p>زبان ببند که افسار این خران بازست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درین هوسکده غافل ز فیض یأس مباش</p></div>
<div class="m2"><p>دری‌که بر رخ ما بسته شد همان بازست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز جا نرفته جنون هزار قافله‌ایم</p></div>
<div class="m2"><p>جرس بنال‌که بر ما ره فغان بازست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به جاده‌های نفس فرصت اقامت عمر</p></div>
<div class="m2"><p>همان تأمل شاگرد ریسمان بازست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به‌کنه سود و زیان‌کیست وارسد بیدل</p></div>
<div class="m2"><p>متاعها همه سربسته و دکان بازست</p></div></div>