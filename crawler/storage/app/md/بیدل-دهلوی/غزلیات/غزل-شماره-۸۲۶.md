---
title: >-
    غزل شمارهٔ ۸۲۶
---
# غزل شمارهٔ ۸۲۶

<div class="b" id="bn1"><div class="m1"><p>وهم هستی هیچکس را ازتپیدن وانداشت</p></div>
<div class="m2"><p>مهر بال و پر همان جز بیضهٔ عنقا نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عالمی زین بزم عبرت مفلس و مایوس رفت</p></div>
<div class="m2"><p>کس‌ نشد آگه‌ که‌ چیزی‌ داشت‌ با خود یا نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیکسی زحمت‌پرست منت احباب نیست</p></div>
<div class="m2"><p>یاد ایامی‌ که کس یاد از غبار ما نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرچه پیش آمد همان رو بر قفاکردیم سیر</p></div>
<div class="m2"><p>یک قلم دی داشتیم امروز ما فردا نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دعوی صاحبدلی از هرزه‌گویان باطل است</p></div>
<div class="m2"><p>تا نفس بی‌ضبط می‌زد شیشه‌گر مینا نداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشق ‌همواری درین مکتب دلیل خامشی‌ست</p></div>
<div class="m2"><p>تا درشتی داشت سنگ ‌سرمه جز غوغا نداشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حرص هر سو، ره برد بر سیم و زر دارد نظر</p></div>
<div class="m2"><p>زاهد از فردوس هم مطلوب جز دنیا نداشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قانعان سیراب تسکین از زلال دیگرند</p></div>
<div class="m2"><p>آب شیربنی ‌که‌ گوهر دارد از در‌با نداشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا ز تمکین نگذرند آداب‌دانان وفا</p></div>
<div class="m2"><p>شمع‌محفل در سرآتش داشت زیر پا نداشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا بیابان مرگ نومیدی نباید زیستن</p></div>
<div class="m2"><p>هر‌کجا رفتیم ما را بیکسی تنها نداشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دوری‌ام زان آستان دیوانه‌ کرد اما چه سود</p></div>
<div class="m2"><p>آنقدر خاکی‌که افشانم به سر صحرا نداشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون نفس بیدل نفسها در تردد سوختم</p></div>
<div class="m2"><p>گوشهٔ دل جای راحت بود اما جا نداشت</p></div></div>