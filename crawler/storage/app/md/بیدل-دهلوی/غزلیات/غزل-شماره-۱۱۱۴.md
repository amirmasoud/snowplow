---
title: >-
    غزل شمارهٔ ۱۱۱۴
---
# غزل شمارهٔ ۱۱۱۴

<div class="b" id="bn1"><div class="m1"><p>تا ساز نفسها کم مضراب نگیرد</p></div>
<div class="m2"><p>آهنگ جنون دامن آداب نگیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشق‌ که بنایش همه بر دوش خرابی‌ ست</p></div>
<div class="m2"><p>چون دیده چرا خانه به سیلاب نگیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر پای توگر باز شود دیده مخمل</p></div>
<div class="m2"><p>چون آینه هرگز خبر از خواب نگیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون ریگ روان در سفر دشت توکل</p></div>
<div class="m2"><p>باید قدح آبله هم آب نگیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی‌کینه‌ام از خلق به رنگی‌که چو یاقوت</p></div>
<div class="m2"><p>مو از اثر آتش من تاب نگیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درویشی‌ من سرخوش صهبای تسلی است</p></div>
<div class="m2"><p>ساحل قدح از گردن‌ گرداب نگیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین خواب گمان وا نشود چشم یقینت</p></div>
<div class="m2"><p>ازتیغ اجل تا به‌گلو آب نگیرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غفلت به‌کمین دم پیری‌ست حذرکن</p></div>
<div class="m2"><p>کزپرتو صحبت به شکر خواب نگیرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آخربه‌گهر محو شود پیچ وخم موج</p></div>
<div class="m2"><p>تا چند دل از عالم اسباب نگیرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل به عبادتکدهٔ عجزپرستی</p></div>
<div class="m2"><p>جز نقش‌کف پای تو محراب نگیرد</p></div></div>