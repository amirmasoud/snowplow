---
title: >-
    غزل شمارهٔ ۱۷۱۴
---
# غزل شمارهٔ ۱۷۱۴

<div class="b" id="bn1"><div class="m1"><p>جامی مگر از بزم حیا در زده‌ای باز</p></div>
<div class="m2"><p>کاتش به دل شیشه و ساغر زده‌ای باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن زلف پریشان زده‌ای شانه ندانم</p></div>
<div class="m2"><p>بر دفتر دلها ز چه مسطر زده‌ا‌ی باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برگوشهٔ دستار تو آن لالهٔ سیراب</p></div>
<div class="m2"><p>لخت جگر کیست‌ که بر سر زده‌ای باز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای ساغر تبخاله از این تشنه سلامی</p></div>
<div class="m2"><p>خوش خیمه بر آن چشمهٔ ‌کوثر زده‌ای باز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مخموری و مستی همه فرش است به راهت</p></div>
<div class="m2"><p>چون چشم خود امروز چه ساغر زده‌ای باز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ابر چه بهار است‌که بر بسمل نازت</p></div>
<div class="m2"><p>تیغ مژه با برق برابر زده‌ای باز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هشدار که پرواز غرورت نرباید</p></div>
<div class="m2"><p>دل بیضهٔ وهم است و ته پر زده‌ای باز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برهستی موهوم مچین خجلت تحقیق</p></div>
<div class="m2"><p>بر کشتی درویش چه لنگر زده‌ای باز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از خاک دمیدن به قبا صرفه ندارد</p></div>
<div class="m2"><p>ای گل زگریبان که سر برزده‌ای باز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل ز فروغ‌ گهر نظم جهانتاب</p></div>
<div class="m2"><p>دامن به چراغ مه و اختر زده‌ای باز</p></div></div>