---
title: >-
    غزل شمارهٔ ۲۶۸۲
---
# غزل شمارهٔ ۲۶۸۲

<div class="b" id="bn1"><div class="m1"><p>مکش رنج تأمل‌ گر زیان خواهی و گر سودی</p></div>
<div class="m2"><p>درنگ عالم فرصت نمی‌باشد کم از دودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان یکسر قماش کارگاه صبح می‌بافد</p></div>
<div class="m2"><p>ندارد این ‌کتان جز خاک حسرت تاری و پودی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیال آباد امکان غیر حیرت بر نمی دارد</p></div>
<div class="m2"><p>بساط خودنماییها مچین بر بود و نابودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درین گلزار کم فرصت کدامین صبح و کو شبنم</p></div>
<div class="m2"><p>عرقها می‌شمارد خجلت انفاس معدودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیال آشیان نوبهار کیست حیرانم</p></div>
<div class="m2"><p>که می‌بالد ز چشمم حیرت بوی‌ گل اندودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکرخند کدامین غنچه یارب بسملم دارد</p></div>
<div class="m2"><p>که چون صبحم سراپا پیکر زخم نمکسودی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از این سودا که من در چارسوی نُه فلک دارم</p></div>
<div class="m2"><p>همین در سودن دست ندامت دیده‌ام سودی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به هر سو بنگری دود کباب یاس می‌آید</p></div>
<div class="m2"><p>به غیر از دل ندارد مجمر کون و مکان عودی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو هم‌در آرزوی سیم و زر زنار می‌بندی</p></div>
<div class="m2"><p>مکن طعن برهمن‌ گر کند از سنگ معبودی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>علاج زندگی بی نیستی صورت نمی‌بندد</p></div>
<div class="m2"><p>چو زخم صبح دارم در عدم امید بهبودی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به چندین داغ آهی از دل ما سر نزد بیدل</p></div>
<div class="m2"><p>چراغ لالهٔ ما نیست تهمت قابل دودی</p></div></div>