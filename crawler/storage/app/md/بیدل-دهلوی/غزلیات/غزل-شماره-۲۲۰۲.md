---
title: >-
    غزل شمارهٔ ۲۲۰۲
---
# غزل شمارهٔ ۲۲۰۲

<div class="b" id="bn1"><div class="m1"><p>چه‌سان با دوست درد و داغ چندین ساله بنویسم</p></div>
<div class="m2"><p>نیستان صفحه‌ای مسطر زند تا ناله بنویسم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به سطری ‌گر رسم از نسخهٔ بخت سیاه خود</p></div>
<div class="m2"><p>خط نسخ سواد هند تا بنگاله بنویسم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز فرصت آنقدر تنگم‌ که‌ گر مقدور من باشد</p></div>
<div class="m2"><p>برات نه فلک بر شعلهٔ جواله بنویسم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زوال اعتبارات جهان فرصت نمی‌خواهد</p></div>
<div class="m2"><p>ز خجلت آب‌گردم تا گهر را ژاله بنویسم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز تحقیق تناسخ نامهٔ زاهد چه می‌پرسی</p></div>
<div class="m2"><p>مگر آدم بر آید تا منش‌ گوساله بنویسم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به خاطر شکوه‌ای زان لعل خاموشم جنون دارد</p></div>
<div class="m2"><p>قلم در موج ‌گوهر بشکنم تبخاله بنویسم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز آن مدّ تغافلها که دارد چین ابرویش</p></div>
<div class="m2"><p>قیامت بگذرد تا یک مژه دنباله بنویسم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از آن مهپاره خلقی برد داغ حسرت آغوشی</p></div>
<div class="m2"><p>کنون من هم تهی‌گردم ز خوبش و هاله بنویسم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهار فرصت مشق جنونم می‌رود بیدل</p></div>
<div class="m2"><p>زمانی صبرکن تا یک دو داغ لاله بنویسم</p></div></div>