---
title: >-
    غزل شمارهٔ ۱۸۹۳
---
# غزل شمارهٔ ۱۸۹۳

<div class="b" id="bn1"><div class="m1"><p>بر خود از ساز شکفتن‌کی‌گمان دارد عقیق</p></div>
<div class="m2"><p>درخور نامت تبسم در دهان دارد عقیق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جای آن داردکه باشد باب دندان طمع</p></div>
<div class="m2"><p>نسبت دوری به لعل دلبران دارد عقیق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسکه بی‌آب است این صحرای شهرت اعتبار</p></div>
<div class="m2"><p>روز و شب نقش نگین زیر زبان دارد عقیق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سادگی دارالامان بی تمیزان بوده است</p></div>
<div class="m2"><p>حلقه‌های دام را خاتم‌گمان دارد عقیق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عیب ما رنگین خیالان معنی باریک ماست</p></div>
<div class="m2"><p>عرض نقصان تا دهد از رگ زبان دارد عقیق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کسی تا خاک‌گردیدن به رنگی بسمل است</p></div>
<div class="m2"><p>خون رنگی در فسردنها روان دارد عقیق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حرص هر جا غالب افتد بر جگر دندان فشار</p></div>
<div class="m2"><p>در هجوم تشنگی‌ها امتحان دارد عقیق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرکه می‌بینی به قدر شهرت از خود رفته است</p></div>
<div class="m2"><p>سودنامی هم به تحصیل زیان دارد عقیق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی‌جگر خوردن میسر نیست پاس اعتبار</p></div>
<div class="m2"><p>آبرو در موج خون دل نهان دارد عقیق</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اعتبارات جهان پر بی‌نسق افتاده است</p></div>
<div class="m2"><p>جانکنیها بهر نام دیگران دارد عقیق</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خون دل را در بساط دیده رنگی دیگر است</p></div>
<div class="m2"><p>آبرو در خاتم افزونتر ز کان دارد عقیق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لعل ها از بهر مشتاقان تبسم‌پرور است</p></div>
<div class="m2"><p>آب باربکی به ذوق تشنگان دارد عقیق</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>محو لعلت را فسردن نیز آب زندگی‌ست</p></div>
<div class="m2"><p>همچو دل تا رنگ خونی هست جان دارد عقیق</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیست بیدل‌ کاهش ایام بر دلخستگان</p></div>
<div class="m2"><p>در شکست خود همان خط امان دارد عقیق</p></div></div>