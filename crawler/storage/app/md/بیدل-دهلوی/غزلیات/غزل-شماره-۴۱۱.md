---
title: >-
    غزل شمارهٔ ۴۱۱
---
# غزل شمارهٔ ۴۱۱

<div class="b" id="bn1"><div class="m1"><p>د‌ی ترنگی از شکست ساغرم ‌کل ‌کرد و ریخت</p></div>
<div class="m2"><p>ششجهت ‌کیفیت چشم ترم‌ گل‌ کرد و ریخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب چو شمعم وعدهٔ دیدار در آتش نشاند</p></div>
<div class="m2"><p>تا سحر آیینه از خاکسترم‌گل‌کرد و ریخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلوت رازم بهشت غیرت طاووس‌ گشت</p></div>
<div class="m2"><p>رنگها چون حلقه بیرون درم‌ گل‌کرد و ریخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا تجرد از اثر پرداخت اجزای مرا</p></div>
<div class="m2"><p>سایه همچون ‌مو، ز جسم‌لاغری ‌گل ‌کرد و ریخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای هوس دیگر چه دکان قیامت چیدنست</p></div>
<div class="m2"><p>برکف خونی‌که چون‌گل در برم‌ گل‌کرد و ریخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیر این باغم‌کفیل یک سحر فرصت نبود</p></div>
<div class="m2"><p>خنده واری تاگریبان بر درم‌گل‌کرد و ریخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرنگون شرم عصیان را چه عزت‌،‌ کو وقار</p></div>
<div class="m2"><p>آبروی من ز دامان ترم ‌گل ‌کرد و ریخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>داغم از اوج و حضیض دستخاه انفعال</p></div>
<div class="m2"><p>بر فلک‌هم یک‌عرق‌وار اخترم گل‌کرد و ریخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سعی مژگان جز ندامت‌ساز پروازی نداشت</p></div>
<div class="m2"><p>بسکه ماندم نارسا اشک از پرم‌ گل‌ کرد و ریخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صفحه‌ام یاد که آتش زد که تا مژگان زدن</p></div>
<div class="m2"><p>صد نگاه واپسین از پیکرم گل کرد و ریخت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هیچ فردوسی به سامان دل خرسند نیست</p></div>
<div class="m2"><p>خاک هم ‌گر خواست ریزد بر سرم گل کرذ و ریخت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا بپوشم بیدل آن‌گنجی‌که در دل داشتم</p></div>
<div class="m2"><p>عالم ویرانی از بام و درم گل کرد و ریخت</p></div></div>