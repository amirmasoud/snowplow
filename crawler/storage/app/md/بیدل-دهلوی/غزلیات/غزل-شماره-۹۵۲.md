---
title: >-
    غزل شمارهٔ ۹۵۲
---
# غزل شمارهٔ ۹۵۲

<div class="b" id="bn1"><div class="m1"><p>به هرجا ساز غیرت انفعال آهنگ می‌گردد</p></div>
<div class="m2"><p>به موج یک عرق صد آسیای رنگ می‌گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگردد ضعف پیری مانع بیتابی شوقت</p></div>
<div class="m2"><p>نوا از پا نیفتد گر نی ما چنگ می‌گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فسردن‌ کسوت ناموس ‌چندین وحشت‌ است اینجا</p></div>
<div class="m2"><p>پری در شیشه دارد خاک ما گر سنگ می‌گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز الفتگاه دل مگذرکه با آن پرفشانیها</p></div>
<div class="m2"><p>نفس اینجا ز لب نگذشته عذر لنگ می‌گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو گیرد خودنمایی دامنت ساز ندامت کن</p></div>
<div class="m2"><p>خموشی می‌تپد بر خویش تا آهنگ می‌گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فریب آب نتوان خوردن از آیینهٔ هستی</p></div>
<div class="m2"><p>گر امروزش صفایی هست فردا زنگ می‌گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دماغ و هم سرشار است در خمخانهٔ امکان</p></div>
<div class="m2"><p>می تحقیق تا در جام ریزی بنگ می‌گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ندانم نبض موجم یا غبار شیشهٔ ساعت</p></div>
<div class="m2"><p>که راحت از مزاج من به صد فرسنگ می‌گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جنونم جامه‌واری دارد از تشریف عریانی</p></div>
<div class="m2"><p>که‌ گر یک رشته بر رویش فزایی تنگ می‌گردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل آن بهتر که چون اشک از تپیدن نگذرد بیدل</p></div>
<div class="m2"><p>که این گوهر به یک دم آرمیدن سنگ می‌گردد</p></div></div>