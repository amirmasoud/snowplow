---
title: >-
    غزل شمارهٔ ۲۴۹۹
---
# غزل شمارهٔ ۲۴۹۹

<div class="b" id="bn1"><div class="m1"><p>ای به عشرت متهم سامان درد سر مکن</p></div>
<div class="m2"><p>صاف و دردی نیست اینجا وهم در ساغر مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمع این محفل وبال گردن خویش است و بس</p></div>
<div class="m2"><p>تا بود ممکن ز جیب خامشی سر بر مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زندگی مفتست اگر بی‌فکر مردن بگذرد</p></div>
<div class="m2"><p>شعلهٔ خود را بیابان مرگ خاکستر مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا توانی درکمین زحمت دلها مباش</p></div>
<div class="m2"><p>همچو سیل از خاک این ویرانه‌ها سر بر مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لب‌ گشودن‌ کشتی عمرت به توفان می‌دهد</p></div>
<div class="m2"><p>در چنین بحر بلای خامشی لنگر مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قسمتت زین گردخوان بی‌انتظار آماده است</p></div>
<div class="m2"><p>خاک کن بر دیده اما حلقه بر هر در مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا کجا خواهی به افسون نفس پرواز کرد</p></div>
<div class="m2"><p>این ورق گردانده گیر آرایش دفتر مکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای هوس فرسای جولان خون جمعیت مریز</p></div>
<div class="m2"><p>بر رگ هر جاده نقش پای خود نشتر مکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرکس اینجا قاصد پیغام اسرار خود است</p></div>
<div class="m2"><p>از زبانم حرف او گر بشنوی باور مکن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دود دل تا خانهٔ خورشید خواهد شد بلند</p></div>
<div class="m2"><p>یا رب این آیینه رو را محرم جوهر مکن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نخل‌ گلزار جنون از ریشه بیرون خوشنماست</p></div>
<div class="m2"><p>ای خموشی نالهٔ ما را نفس پرور مکن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ترک زحمت گیر اگر زنگار خورد آیینه‌ات</p></div>
<div class="m2"><p>انفعال سعی بیجا مزد روشنگر مکن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>احتراز از شور امکان درس هر مجهول نیست</p></div>
<div class="m2"><p>فهم در کار است اگر گوشی نداری‌ کر مکن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا سلامت جان بری بیدل ازین‌ گرداب یأس</p></div>
<div class="m2"><p>تشنه چون‌ گشتی بمیر اما لب خود تر مکن</p></div></div>