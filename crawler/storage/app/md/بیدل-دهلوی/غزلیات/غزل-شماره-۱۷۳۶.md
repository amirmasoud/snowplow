---
title: >-
    غزل شمارهٔ ۱۷۳۶
---
# غزل شمارهٔ ۱۷۳۶

<div class="b" id="bn1"><div class="m1"><p>نیست بی‌شور حوادث آمد و رفت نفس</p></div>
<div class="m2"><p>کاروان موج دارد از شکست خود جرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باغ امکان را شکست رنگ می‌باشد کمال</p></div>
<div class="m2"><p>ای ثمر گر فرصتی داری به کام خویش رس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا توانی پاس آب روی سایل داشتن</p></div>
<div class="m2"><p>خودفروشی های احسان به‌ که ننمایی به کس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای عدم آوازه قید زندگی هم عالمی‌ست</p></div>
<div class="m2"><p>بیضه ‌گر بشکست‌، چون ‌طاووس رنگین ‌کن ‌قفس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشت خونی هرزه‌گرد کوچهٔ زخم دلیم</p></div>
<div class="m2"><p>حسرت است‌اینجا به جز عبرت چه‌می گردد عسس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دستگاه سفله‌خویان مایهٔ شور و شر است</p></div>
<div class="m2"><p>خالی از عرض طنینی نیست پرواز مگس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون به آگاهی رسیدی‌ گفت و گوها محو کن</p></div>
<div class="m2"><p>نیست منزل جز بیابان مرگی شور جرس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی‌غباری نیست هرجا مشت خاکی دیده‌ایم</p></div>
<div class="m2"><p>شد یقین ‌کز بعد مردن هم نمی‌میرد هوس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون‌ حبابم ‌بیدل از وضع‌ خموشی ‌چاره نیست</p></div>
<div class="m2"><p>صاحب آیینه را لازم بود پاس نفس</p></div></div>