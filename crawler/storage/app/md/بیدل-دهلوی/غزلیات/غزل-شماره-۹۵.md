---
title: >-
    غزل شمارهٔ ۹۵
---
# غزل شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>گر، دمی‌، بوس کفت‌گردد میسر تیغ را</p></div>
<div class="m2"><p>تا ابد رگهای‌گل بالد ز جوهر تیغ را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازکدورت برنمی‌آید مزاج کینه‌جو</p></div>
<div class="m2"><p>بیشتر دارد همین زنگار در بر تیغ را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای‌که داری سیرگلزار شهادت در خیال</p></div>
<div class="m2"><p>بایدت‌از شوق زد چون سبزه برسرتیغ‌را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عیش خواهی صید آفت شوکه مانند هلال</p></div>
<div class="m2"><p>چرخ ابرومی‌کند برچشم ساغرتیغ را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پردهٔ نیرنگ توفان بود شوق بسملم</p></div>
<div class="m2"><p>خونم آخرکرد بازوی شناور تیغ را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا مگر یکباره‌گردد قطع راه هستی‌ام</p></div>
<div class="m2"><p>چون دم مقراض می‌خواهم دو پیکر تیغ را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>موج توفان می‌زند جوی به‌دریامتصل</p></div>
<div class="m2"><p>جوهر دیگر بود در دست حیدر تیغ را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرکه را دل از غبارکینه‌جوییها تهی‌ست</p></div>
<div class="m2"><p>می‌کشد همچون نیام آسوده در برتیغ را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل به امید تلافی می‌تپد اماکجاست</p></div>
<div class="m2"><p>آنقدر زخمی‌که خواباند به بسترتیغ را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل از هرمصرعم موج نزاکت می‌چکد</p></div>
<div class="m2"><p>کرده‌ام رنگین به خون صید لاغرتیغ را</p></div></div>