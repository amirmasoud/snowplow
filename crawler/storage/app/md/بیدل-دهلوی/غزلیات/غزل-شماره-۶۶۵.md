---
title: >-
    غزل شمارهٔ ۶۶۵
---
# غزل شمارهٔ ۶۶۵

<div class="b" id="bn1"><div class="m1"><p>صورت راحت نفور از مردمان عالمست</p></div>
<div class="m2"><p>جلوه ننماید بهشت آنجا که جنس آدمست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>د‌ر نظر آهنگ حسرت در نفس شور ظلب</p></div>
<div class="m2"><p>ساز بزم زندگانی را همین زیر و بمست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر دو عالم در غبار وهم توفان می‌کند</p></div>
<div class="m2"><p>از گهر تا موج‌ ، هرجا واشکافی بی‌نمست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سایهٔ خود درس وحشت داده مجنون تو را</p></div>
<div class="m2"><p>چشم اهو را سواد خویش سرمشق رمست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر حیا گیرد هوس آیینه‌دار آبرو است</p></div>
<div class="m2"><p>چون ‌هوا از هرزه گردی ‌منفعل شد، شبنمست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرچه پیرم فارغ از انداز شوخی نیستم</p></div>
<div class="m2"><p>قامت خم‌ گشته‌ ام هم چشم ابروی خمست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پادشاهی در طلسم سیر چشمی بسته‌اند</p></div>
<div class="m2"><p>کاسهٔ چشم‌ گدا گر پر شود جام جمست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با فروغ جعواه‌ات نظارگی را تاب‌کو</p></div>
<div class="m2"><p>رنگ گل چون آتش افروزد سپندش شبنمست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در بنای حیرت از حسن تو می‌بینم خلل</p></div>
<div class="m2"><p>خانهٔ آیینه هم برپا به دیوار نمست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا نفس باقی‌ست‌، ظالم نیست‌، بی‌فکر فساد</p></div>
<div class="m2"><p>گوشه ‌گیر فتنه می‌باشد کمان را تا دمست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شعله هرجا می‌شود سرگرم تعمیر غرور</p></div>
<div class="m2"><p>داغ می‌خندد که همواری بنایی محکمست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نامدا‌ریها گرفتاریست در دام بلا</p></div>
<div class="m2"><p>بیدل انگشت شهان را طوق گردن خاتمست</p></div></div>