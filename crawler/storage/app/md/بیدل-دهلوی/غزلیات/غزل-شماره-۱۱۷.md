---
title: >-
    غزل شمارهٔ ۱۱۷
---
# غزل شمارهٔ ۱۱۷

<div class="b" id="bn1"><div class="m1"><p>کافرم‌گر مخمل و سنجاب می‌باید مرا</p></div>
<div class="m2"><p>سایهٔ بیدی‌کفیل خواب می‌باید مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معبد تسلیم و شغل سرکشی بی‌رونقی‌ست</p></div>
<div class="m2"><p>شمع خاموشی درین محراب می‌باید مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تشنه‌کام عافیت چون شمع‌تاکی سوختن</p></div>
<div class="m2"><p>ازگداز درد، مشتی آب می‌باید مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غافل از جمعیت‌کنج قناعت نیستم</p></div>
<div class="m2"><p>کشتی درویشم این پایاب می‌باید مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آرزوهای هوس نذر حریفان طلب</p></div>
<div class="m2"><p>انفعال مطلب نایاب می‌باید مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در کشاکش‌های نیرنگ خیال افتاده‌ام</p></div>
<div class="m2"><p>دل جنون می‌خواهد و آداب می‌باید مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شرم اگرباشد بنای وهم هستی هیچ نیست</p></div>
<div class="m2"><p>بی‌تکلف یک عرق سیلاب می‌باید مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دامن برچیده‌ای چون صبح کارم می‌کند</p></div>
<div class="m2"><p>اینقدر از عالم اسباب می‌باید مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مشرب داغ وفا منت‌کش تسکین مباد</p></div>
<div class="m2"><p>آب می‌گردم اگر مهتاب می‌باید مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا درین محفل نوای حیرتی انشاکنم</p></div>
<div class="m2"><p>چون‌نگه یک‌تار و صدمضراب می‌باید مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بی‌نیازم از رم و آرام این آشوبگاه</p></div>
<div class="m2"><p>چشم می‌پوشم همه‌گر خواب می‌باید مرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گریه هم‌بیدل لب خشکم چومژگان‌ترنکرد</p></div>
<div class="m2"><p>وحشتی زین وادی بی‌آب می‌باید مرا</p></div></div>