---
title: >-
    غزل شمارهٔ ۱۹۲۲
---
# غزل شمارهٔ ۱۹۲۲

<div class="b" id="bn1"><div class="m1"><p>ز من عمریست می‌گردد جدا دل</p></div>
<div class="m2"><p>ندانم با که گردید آشنا دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز حرف عشق خارا می‌گدازد</p></div>
<div class="m2"><p>من و رازی‌که نتوان‌گفت با دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به فکر ناوک ابروکمانی</p></div>
<div class="m2"><p>چو پیکانم‌ گره از سینه تا دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به امید پری مینا پرستیم</p></div>
<div class="m2"><p>ز شوقت‌ کرد بر ما نازها دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نفس آیینه را زنگار یأس است</p></div>
<div class="m2"><p>ز هستی باخت امید صفا دل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به رنگ لاله نقد دیگرم نیست</p></div>
<div class="m2"><p>مگر از داغ خواهد خونبها دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تپش‌گم‌کرده اشکی ناتوان چشم</p></div>
<div class="m2"><p>گره بالیده آهی نارسا دل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ثباتی نیست بنیاد نفس را</p></div>
<div class="m2"><p>حباب ما چه بندد بر هوا دل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مزن ای بیخبر لاف محبت</p></div>
<div class="m2"><p>مبادا آب‌ گردد از حیا دل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در آن معرض‌ که جوشد شور محشر</p></div>
<div class="m2"><p>قیامت هم تو خواهی بود با دل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حریفان از نشان من مپرسید</p></div>
<div class="m2"><p>خیالی داشتم‌ گم‌ گشت با دل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فسردن بیدل از بیدردی‌ام نیست</p></div>
<div class="m2"><p>چو موج‌ گوهر‌م در زیر پا دل</p></div></div>