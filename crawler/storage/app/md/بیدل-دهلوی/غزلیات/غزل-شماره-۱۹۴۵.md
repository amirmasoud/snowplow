---
title: >-
    غزل شمارهٔ ۱۹۴۵
---
# غزل شمارهٔ ۱۹۴۵

<div class="b" id="bn1"><div class="m1"><p>نوبهار آرد به امداد من بیمارگل</p></div>
<div class="m2"><p>تا به جای رنگ ‌گردانم به‌ گرد یار گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گلستانی که شرم آیینه‌دار ناز اوست</p></div>
<div class="m2"><p>محو شبنم می‌شود از شوخی اظهارگل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باغبان‌! از دورگردان چمن غافل مباش</p></div>
<div class="m2"><p>تا کی‌ام دزدیده باشد رخنهٔ دیوار گل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خموشی پرده ‌دار شوخی حسن است عشق</p></div>
<div class="m2"><p>می‌کند بلبل نهان در غنچهٔ منقار گل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا نفس باقیست باید خصم راحت بود و بس</p></div>
<div class="m2"><p>هم ز بوی خویش دارد در گریبان خار گل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رنگ بو نامحرم فیض بهار نیستی است</p></div>
<div class="m2"><p>خاک راهی باش و از هر نقش پا بردار گل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر ز اسرار بهار عشق بویی برده‌ای</p></div>
<div class="m2"><p>غیر داغ و زخم و اشک و آبله مشمار گل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر بساط غنچه خسبان‌ گر رسی آهسته باش</p></div>
<div class="m2"><p>می‌شود از جنبش نبض نفس بیدار گل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این حدیث از شمع روشن شد که در بزم وقار</p></div>
<div class="m2"><p>داغ دارد زیب دل چون زینت دستار گل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حاصل این باغ بر دامن‌ گرانی می‌کند</p></div>
<div class="m2"><p>چون سپر بر پشت باید بستنت ناچار گل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جلوه در پیش است تشویش دگر انشا مکن</p></div>
<div class="m2"><p>هرکجا باشد همان بر رنگ دارد کار گل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شوخی نشو و نماها بس که شبنم‌پرور است</p></div>
<div class="m2"><p>سبزه چون مژگان بیدل ‌کرده ‌گوهر بارگل</p></div></div>