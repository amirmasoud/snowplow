---
title: >-
    غزل شمارهٔ ۵۳۱
---
# غزل شمارهٔ ۵۳۱

<div class="b" id="bn1"><div class="m1"><p>چون سایه بس‌که‌کلفت غفلت سرشت ماست</p></div>
<div class="m2"><p>بخت سیاه نامهٔ اعمال زشت ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرد‌ون به فکر آفت ماکم فتاده است</p></div>
<div class="m2"><p>مانند خم‌، همیشه‌، سرما و خشت ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون غنچه درکمین بهاری نشسته‌ایم</p></div>
<div class="m2"><p>چاکی اگر دمد زگریبان بهشت ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در سینه دل به ضبط نفس آب‌کرده‌ایم</p></div>
<div class="m2"><p>ناقوس از ستم‌زده‌های کنشت ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سودای طره‌ات ز سر ما نمی‌رود</p></div>
<div class="m2"><p>چون شعله دوددل رقم‌سرنوشت ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تهمت مبند بیهده بر دوش وهم غیر</p></div>
<div class="m2"><p>خار وگل بساط جهان خوب و زشت ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اشکی ز الفت مژه دل برگرفته‌ایم</p></div>
<div class="m2"><p>هر دانه‌ای‌که ریشه ندارد زکشت ماست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پوشیده نیست جوهر نظاره مشربان</p></div>
<div class="m2"><p>آیینه لختی ازدل حیرت سرشت ماست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیدل بنای ریختهٔ درد الفتیم</p></div>
<div class="m2"><p>گرد جفا و داغ الم خاک و خشت ماست</p></div></div>