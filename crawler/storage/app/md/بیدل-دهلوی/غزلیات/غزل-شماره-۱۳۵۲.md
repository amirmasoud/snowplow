---
title: >-
    غزل شمارهٔ ۱۳۵۲
---
# غزل شمارهٔ ۱۳۵۲

<div class="b" id="bn1"><div class="m1"><p>ای ساز قدس دل به جهان نوا مبند</p></div>
<div class="m2"><p>یکتاست رشته‌ات به هر آواز پا مبند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تمثال غیر و آینه‌ات این چه تهمت است</p></div>
<div class="m2"><p>رنگ شکسته بر چمن ‌کبریا مبند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای بی‌نیاز کارگه اتفاق صنع</p></div>
<div class="m2"><p>بار خیال بر دل بی‌مدعا مبند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرکوته است سعی امل با رسایی‌ات</p></div>
<div class="m2"><p>ای نغمهٔ بلند به هر رشته پا مبند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیگانگی ز وضع جهان موج می‌زند</p></div>
<div class="m2"><p>آیینه جز مقابل آن آشنا مبند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بست و گشاد حکم قضا را چه چاره است</p></div>
<div class="m2"><p>نتوان خیال بست‌که مگشای یا مبند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دارد دل شکسته در این دیر بی‌ثبات</p></div>
<div class="m2"><p>مضمون عبرتی‌ که برای خدا مبند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سامان شبنم چمنت آرمیدگی‌ست</p></div>
<div class="m2"><p>این محمل وفاق به دوش هوا مبند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناموس آبروی تنزه نگاه دار</p></div>
<div class="m2"><p>رنگ عرق تری‌ست به سازحیا مبند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زان‌دست بی‌نگارکه در آستین توست</p></div>
<div class="m2"><p>زنهار شرم دار خیال حنا مبند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این‌ عقده امید که دل نقش بسته‌است</p></div>
<div class="m2"><p>بیدل به رشته‌ ای‌ که توان‌ کرد وامبند</p></div></div>