---
title: >-
    غزل شمارهٔ ۵۶۲
---
# غزل شمارهٔ ۵۶۲

<div class="b" id="bn1"><div class="m1"><p>خاک نمیم‌، ما را،‌کی فکر عجز و جاه است</p></div>
<div class="m2"><p>گرد شکستهٔ ما بر فرق ماکلاه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق غیورا‌ز ما چیزی نخواست جزعجز</p></div>
<div class="m2"><p>سازگدایی اینجا منظور پادشاه است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیر و شری‌که دارند بر فضل واگذارید</p></div>
<div class="m2"><p>هرچند امید عفو است درکیش ماگناه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با عشق غیرتسلیم دیگر چه سرکندکس</p></div>
<div class="m2"><p>در آفتاب محشر بی‌سایگی پناه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل‌گر نشان نمی‌داد هستی چه داشت در بار</p></div>
<div class="m2"><p>تمثال بی‌اثر را آیینه دستگاه است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای شمع چند خواهی مغرور ناز بودن</p></div>
<div class="m2"><p>این‌گردن بلندت سر درکنار چاه است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهد ضعیف ما را تسلیم می‌شناسد</p></div>
<div class="m2"><p>هرچند پا نداریم چون سبحه سر به راه است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاک مرا مخواهید پامال ناامیدی</p></div>
<div class="m2"><p>با هر سیاهکاری در سرمه‌ام نگاه است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شستن مگربخواند مضمون سرنوشتم</p></div>
<div class="m2"><p>نامی‌که من ندارم در نامهٔ سیاه است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شادم‌که فطرتم نیست تریاکی تعین</p></div>
<div class="m2"><p>وهمی‌که می‌فروشم بنگ است وگاهگاه است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل دلیل عجز است شبنم طرازی صبح</p></div>
<div class="m2"><p>از سعی بی‌پر و بال اشکم گداز آه است</p></div></div>