---
title: >-
    غزل شمارهٔ ۱۳۸۰
---
# غزل شمارهٔ ۱۳۸۰

<div class="b" id="bn1"><div class="m1"><p>روزگاری‌ که به عشق از هوسم افکندند</p></div>
<div class="m2"><p>بال و پر کنده برون قفسم افکندند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما و من خوش پر و بالی به خیال انشا کرد</p></div>
<div class="m2"><p>مور بودم به غرور مگسم افکندند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا کند عبرتم آگاه ز هنگامهٔ عمر</p></div>
<div class="m2"><p>در تب و تاب شمار نفسم افکندند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خون خشکم جوی از قدر نیرزبد آخر</p></div>
<div class="m2"><p>صد ره از پوست برون چو عدسم افکندند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقش پا کرد تصور به تغافل زد و رفت</p></div>
<div class="m2"><p>در ره هر که خط ملتمسم افکندند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناز دارم به غباری ‌که ز بیداد فلک</p></div>
<div class="m2"><p>سرمه شد تا به ره دادرسم افکندند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه توان ‌کرد سراغ همه زین دشت ‌گم است</p></div>
<div class="m2"><p>در پی قافلهٔ بی‌جرسم افکندند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکوهٔ من ز فراموشی احباب خطاست</p></div>
<div class="m2"><p>از ادب پیش ‌گذشتم ‌که پسم افکندند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سخت زحمتکش اسباب جهانم بیدل</p></div>
<div class="m2"><p>چه نمودند که در دیده خسم افکندند</p></div></div>