---
title: >-
    غزل شمارهٔ ۲۴۲
---
# غزل شمارهٔ ۲۴۲

<div class="b" id="bn1"><div class="m1"><p>پر تشنه است حرص فضولی‌کمین ما </p></div>
<div class="m2"><p>یارب عرق به خاک نریزد جبین ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آه از حلاوت سخن وخلق بی‌تمیز </p></div>
<div class="m2"><p> آتش به خانهٔ که زند انگبین ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمری‌ست با خیال‌گر و تاز پهلویم </p></div>
<div class="m2"><p> گردون به رخش موج‌گهربست زین ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غیراز شکست چینی دل‌کاین زمان دمید </p></div>
<div class="m2"><p> مویی نداشت خامهٔ نقاش چین ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیغام عجز سرمه‌نوا باکه می‌رسد </p></div>
<div class="m2"><p> شاید مگس به پنبه رساند طنین ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حرفی نشدعیان‌که‌توان خواند وفهم‌کرد </p></div>
<div class="m2"><p> بسی‌خامه بود منشی خط جبین ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یارب زمین نرم چه سازد به نقش پا </p></div>
<div class="m2"><p> داغ‌گذشتگان نکنی دلنشین ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بشکسته‌ایم دامن وحشت چوگردباد </p></div>
<div class="m2"><p> دستی بلندکرد زچین آستین ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چندان نمک نداشت به‌خود چشم دوختن </p></div>
<div class="m2"><p> صدآفرین به غفلت غیرآفرین ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در ملک نیستی‌چه تصرف‌کندکسی</p></div>
<div class="m2"><p> عنقاگم است در پی نام نگین ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p> گشتیم‌داغ خلوت محفل‌ولی‌چوشمع</p></div>
<div class="m2"><p> خود را ندید غفلت آیینه‌بین ما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برخاستن ز شرم‌ضعیفی چه‌ممکن است</p></div>
<div class="m2"><p> بیدل غبار نم‌زده دارد زمین ما </p></div></div>