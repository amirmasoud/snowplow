---
title: >-
    غزل شمارهٔ ۲۶۳
---
# غزل شمارهٔ ۲۶۳

<div class="b" id="bn1"><div class="m1"><p>چه‌کدخدایی‌ست ای ستمکش جنون‌کن از دردسر برون‌آ </p></div>
<div class="m2"><p>تو شوق آزاد بی‌غباری زکلفت بام و در برون آ </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به‌کیش آزادگی نشایدکه فکر لذات عقده زاید </p></div>
<div class="m2"><p>ره نفس‌پیچ وخم ندارد چونی زبند شکربرون آ </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر محیط‌گهر برآیی قبول بزم وفا نشایی </p></div>
<div class="m2"><p>دلی به‌ذوق حضور خونین سرشکی از چشم تر برون آ </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دماغ عشاق ننگ دارد علم شدن بی‌جنون داغی </p></div>
<div class="m2"><p>چو شمع‌گر خودنما برآبی ز سوختن‌گل به سربرون آ </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز شعله خاکستر آشیانی ربود تشویش پرفشانی </p></div>
<div class="m2"><p>به ذوق پرواز، بی‌نشانی تو نیز سر زیر پر برون آ </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی درین دشت برنیامد حریف یک لحظه استقامت</p></div>
<div class="m2"><p>توتا نچینی غبار خفت ز عرصهٔ بی‌جگر برون آ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p> ندارد اقبال جوهر مرد در شکنج لباس بودن</p></div>
<div class="m2"><p>چوتیغ‌، و‌هم نیام بگذار و با شکوه ظفر برون آ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p> به صد تب وتاب خلق غافل‌گذشت زین‌تنگنای غربت</p></div>
<div class="m2"><p>چو موج خون ازگلوی بسمل تو نیز باکر و فربرون آ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p> به بارگاه نیاز دارد فروتنی ناز سربلندی</p></div>
<div class="m2"><p>به خاک روزی دوریشگی‌کن دگر ببال و شجربرون آ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p> جهان‌گران خیز نارسایی‌ست اگرنه در عرصه‌گاه عبرت</p></div>
<div class="m2"><p>نفس همین تازیانه داردکزین مکان چون سحر برون آ </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درین بساط خیال بیدل ز سعی بی‌حاصل انفعائی</p></div>
<div class="m2"><p> حیا بس است آبروی همت زعالم خشک تر برون آ </p></div></div>