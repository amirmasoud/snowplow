---
title: >-
    غزل شمارهٔ ۱۳۹۰
---
# غزل شمارهٔ ۱۳۹۰

<div class="b" id="bn1"><div class="m1"><p>برق خطی بر سیاهی می‌زند</p></div>
<div class="m2"><p>هالهٔ مه تا به ماهی می‌زند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سجده مشتاق خم ابروی کیست</p></div>
<div class="m2"><p>بر دماغم کج‌کلاهی می‌زند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معصیت در بارگاه رحمتش</p></div>
<div class="m2"><p>خنده‌ها بر بی‌گناهی می‌زند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای عدم فرصت‌، شرارکاغذت</p></div>
<div class="m2"><p>چشمک عبرت نگاهی می‌زند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهر عبرت فرصتی در کار نیست</p></div>
<div class="m2"><p>یک نگه برهرچه خواهی می‌زند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پُردلیها امتحانگاه بلاست</p></div>
<div class="m2"><p>تیغ بر قلب سپاهی می‌زند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا فسون بادبان دارد نفس</p></div>
<div class="m2"><p>کشتی ما برتباهی می‌زند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی تو گر مژگان بهم می‌آیدم</p></div>
<div class="m2"><p>بر سر خوابم سیاهی می‌زند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیدل از وصلی نویدم داده‌اند</p></div>
<div class="m2"><p>دل تپیدن کوس شاهی می‌زند</p></div></div>