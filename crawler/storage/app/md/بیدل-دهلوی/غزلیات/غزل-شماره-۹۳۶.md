---
title: >-
    غزل شمارهٔ ۹۳۶
---
# غزل شمارهٔ ۹۳۶

<div class="b" id="bn1"><div class="m1"><p>به سرم شور تمنای تو تا می‌پیچد</p></div>
<div class="m2"><p>دود در ساغر داغم چو صدا می‌پیچد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسرت چاک گرببان نشود دام‌ کسی</p></div>
<div class="m2"><p>این کمندی‌ست که در گردن ما می‌پیچد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالم از شکوهٔ نومیدی عشاق پُر است</p></div>
<div class="m2"><p>نارسا نالهٔ ما در همه جا می‌پیچد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نبود هستی اگر دشمن روشن‌گهران</p></div>
<div class="m2"><p>نفس پوچ در آیینه چرا می‌پیچد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیر گردیده‌ام و از خودم آزادی نیست</p></div>
<div class="m2"><p>حلقهٔ زلف ‌که بر قد دو تا می‌پیچد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کس ندانست‌که با این همه بیتابی شوق</p></div>
<div class="m2"><p>رشتهٔ سعی نفسها به ‌کجا می‌پیچد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صید عجز خودم از شبنم من هیچ مپرس</p></div>
<div class="m2"><p>بوی گل نیز مرا رشته به پا می‌پیچد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وحشتی ‌هست درپن ‌دشت ‌که چون ‌رشتهٔ شمع</p></div>
<div class="m2"><p>جاده بر شعلهٔ آواز درا می‌پیچد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل به غفلت نه و از رنج خیالات برآ</p></div>
<div class="m2"><p>عکس برآینه یکسر ز صفا می‌پیچد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می‌کشد هفت فلک درخم یک شاخ غزال</p></div>
<div class="m2"><p>گردبادی‌که به دشت دل ما می‌پیچد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ناله تحریر مضامین تمنای توام</p></div>
<div class="m2"><p>خامشی‌ کیست ‌که مکتوب مرا می‌پیچد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چاره از عربده بیدل نبود مفلس را</p></div>
<div class="m2"><p>سرو از بی‌ثمریها به هوا می‌پیچد</p></div></div>