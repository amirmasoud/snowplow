---
title: >-
    غزل شمارهٔ ۲۴۴۵
---
# غزل شمارهٔ ۲۴۴۵

<div class="b" id="bn1"><div class="m1"><p>جایی ‌که بود پیش بری پیش نبردن</p></div>
<div class="m2"><p>مفت تو اگر پیش بری بیش نبردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چند توان زیست به افسون رعونت</p></div>
<div class="m2"><p>مکروهتر از سجده‌ به هر کیش نبردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای شیخ تو درکشمکشی ورنه بهشتی است</p></div>
<div class="m2"><p>از شانه قیامت به سر ریش نبردن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>انبوهی مو نسبت تنزیه ندارد</p></div>
<div class="m2"><p>حکم‌ست به فردوس بز و میش نبردن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برگشتن مژگان بتان قاصد نازی‌ست</p></div>
<div class="m2"><p>ظلم است نویدی به دل ریش نبردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دردا که دل اگه نشد از لذت دردی</p></div>
<div class="m2"><p>خون می‌خورم از آبله بر نیش نبردن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساقی خط ییمانه نی‌ام حوصله تا چند</p></div>
<div class="m2"><p>حیف است به موج می‌ام از خویش نبردن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز در سخن بی‌غرضی راست نیاید</p></div>
<div class="m2"><p>بر خلق ستمنامهٔ تشویش نبردن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیدل همه دم مزرع اقبال کریمان</p></div>
<div class="m2"><p>سبز است ز آب رخ درویش نبردن</p></div></div>