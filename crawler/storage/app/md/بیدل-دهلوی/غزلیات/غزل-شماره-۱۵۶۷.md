---
title: >-
    غزل شمارهٔ ۱۵۶۷
---
# غزل شمارهٔ ۱۵۶۷

<div class="b" id="bn1"><div class="m1"><p>تا دم تیغت به عرض جلوه عریان می‌شود</p></div>
<div class="m2"><p>خون زخم من چو رنگ ازگل نمایان می‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چمن زین رنگ می‌بالد به یاد مقدمت</p></div>
<div class="m2"><p>شاخ‌گل محمل‌کش پرواز مرغان می‌شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نشاند برلب تیغ تو نقش جوهری</p></div>
<div class="m2"><p>در دهان زخم عاشق بخیه دندان می‌شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترک‌خودداری‌ست‌مشکل ورنه مشت‌خاک‌ما</p></div>
<div class="m2"><p>طرف دامانی ‌گر افشاند بیابان می‌شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرکه رفت از دیده داغی بر دل ما تازه‌کرد</p></div>
<div class="m2"><p>در زمین نرم نقش پا نمایان می‌شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کینه می‌یابد رواج از سردمهری‌های دهر</p></div>
<div class="m2"><p>آبروی آتش افزون در زمستان می‌شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کلفت اسباب رنج، طبع حرص‌اندود نیست</p></div>
<div class="m2"><p>خار و خس در دیده ی گرداب مژگان می‌شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صافی دل را زیارتگاه عبرت کرده‌اند</p></div>
<div class="m2"><p>هرکه میرد خانهٔ آیینه ویران می‌شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حاکم معزول را از بی‌وقاری چاره نیست</p></div>
<div class="m2"><p>زلف در دور هجوم خط مگس‌ ران می‌شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اشک در کار است اگر ما رنگ افغان باختیم</p></div>
<div class="m2"><p>هرچه دل ‌گم ‌می‌کند بر دیده تاوان می‌شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شعلهٔ ما هرقدر خاکستر انشا می‌کند</p></div>
<div class="m2"><p>جامهٔ عریانی ما را گریبان می‌شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دستگاه هستی از وضع سحر ممتاز نیست</p></div>
<div class="m2"><p>گردی از خود می‌فشاند هر که دامان می‌شود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کاهشم چون شمع مفت دستگاه حیرت است</p></div>
<div class="m2"><p>نیست بی‌سود تماشا آنچه نقصان می‌شود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا توانی بیدل از مشق فنا غافل مباش</p></div>
<div class="m2"><p>مشکل هر آرزو زبن شیوه آسان می‌شود</p></div></div>