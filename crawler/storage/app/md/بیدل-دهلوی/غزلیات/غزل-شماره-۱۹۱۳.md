---
title: >-
    غزل شمارهٔ ۱۹۱۳
---
# غزل شمارهٔ ۱۹۱۳

<div class="b" id="bn1"><div class="m1"><p>گر جنون جوشد به این تأثیر احسانش ز سنگ</p></div>
<div class="m2"><p>شیشهٔ نشکسته باید خواست تاوانش ز سنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر مجنون‌ کلاهی گر نباشد گو مباش</p></div>
<div class="m2"><p>عزتی دیگر بود همچون نگیندانش ز سنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناز پرورد خیال جور طفلانیم ما</p></div>
<div class="m2"><p>سایه دارد بر سر خود خانه وبرانش ز سنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با نگاهش بر نیاید شوخی خواب‌ گران</p></div>
<div class="m2"><p>چون شرر بگذشت آخر تیر مژگانش ز سنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر شرار ما به کنج نیستی قانع‌ شود</p></div>
<div class="m2"><p>تا قیامت می‌کشد روغن چراغانش ز سنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مدّ احسانی ‌که‌ گردون بر سر ما می‌کشد</p></div>
<div class="m2"><p>هست طوماری‌که دارد مُهر عنوانش ز سنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچوگندم می‌کشد هرکس درین هفت آسیا</p></div>
<div class="m2"><p>آنقدر رنجی‌که بر می‌آورد نانش ز سنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سخت جانی چنگ اقبالیست با شاهین حرص</p></div>
<div class="m2"><p>تا کشد گوهر ندارد چاره میزانش ز سنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پای خواب‌آلود تمکین ‌کسب مجنون مرا</p></div>
<div class="m2"><p>همچو کوه افتاد آخر گل به دامانش ز سنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حیف دل کز غفلتت باشد غبار اندود جسم</p></div>
<div class="m2"><p>می‌توان‌ کردن به رنگ شیشه عریانش ز سنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شوق من بیدل درین ‌کهسار پرافسرده‌ کیست</p></div>
<div class="m2"><p>ناله‌ای دارم که می‌بالد نیستانش ز سنگ</p></div></div>