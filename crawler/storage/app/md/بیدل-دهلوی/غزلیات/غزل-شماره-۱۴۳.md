---
title: >-
    غزل شمارهٔ ۱۴۳
---
# غزل شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>سرمه سنگین نکند شوخی چشم اورا</p></div>
<div class="m2"><p>درس تمکین ندهد گرد، رم آهورا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زخم تیغش به دل ز داغ‌، مقدم باشد</p></div>
<div class="m2"><p>پایه از چشم‌، بلند است خم ابرو را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جبههٔ ما و همان سجدهٔ تسلیم نیاز</p></div>
<div class="m2"><p>نقش پا،‌کی‌کند از خاک تهی پهلو را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هدف مقصد ما سخت بلند افتاده‌ست</p></div>
<div class="m2"><p>باید از عجزکمان کرد خم بازو را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در مقامی‌که بود جلوه‌گه شاهد فکر</p></div>
<div class="m2"><p>جوهر از موی سر است آینهٔ زانو را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نرمیده‌ست معانی ز صریر قلمم</p></div>
<div class="m2"><p>‌رام دارد نی تیرم به صدا آهو را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نغمهٔ محفل عشاق شکست سازاست</p></div>
<div class="m2"><p>چینی بزم جنون باش و صداکن مو را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهل باشد طمع خلق زسرکش صفتان</p></div>
<div class="m2"><p>هیچ دانا زگل شمع نخواهد بو را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طبع دون از ره تقلید به نیکان نرسد</p></div>
<div class="m2"><p>پای اگر خواب‌کند چشم نخوانند او را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هستی تیره‌دلان جمله به خواری‌گذرد</p></div>
<div class="m2"><p>سایه دایم به سر خاک کشدگیسو را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وحشت‌ما چه خیال ست به‌راحت سازد</p></div>
<div class="m2"><p>ناله آن نیست‌که ساید به زمین پهلورا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل از بال و پر بسته نیاید پرواز</p></div>
<div class="m2"><p>غنچه تا وا نشود جلوه نبخشد بورا</p></div></div>