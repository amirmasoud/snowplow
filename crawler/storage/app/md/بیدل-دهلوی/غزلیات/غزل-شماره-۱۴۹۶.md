---
title: >-
    غزل شمارهٔ ۱۴۹۶
---
# غزل شمارهٔ ۱۴۹۶

<div class="b" id="bn1"><div class="m1"><p>شب‌که از شوق توپروازم بهار آهنگ بود</p></div>
<div class="m2"><p>استخوان هم در تنم چون‌شمع‌ مغز رنگ بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواب راحت باخت دل آخر به افسون صفا</p></div>
<div class="m2"><p>داشت مژگانی بهم آیینه تا در زنگ بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در جهان بی‌تمیزی صلح هم موجود نیست</p></div>
<div class="m2"><p>صبروکوشش را تامل عرصه‌گاه جنگ بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقد راحت می‌شماردگرد از خود رفتنم</p></div>
<div class="m2"><p>همچو آتش بستر نازم شکست رنگ بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اشک از لغزیدنی بر دوش صد مژگان‌گذشت</p></div>
<div class="m2"><p>قطع چندین جاده پا انداز عذر لنگ بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیره‌بختی سرمهٔ کام و زبان کس مباد</p></div>
<div class="m2"><p>چنگ‌گیسو هم به چندین تار بی‌آهنگ بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شوخی مژگانت از خواب‌ گران سر برتداشت</p></div>
<div class="m2"><p>پنجهٔ این ظالم بیباک زبر سنگ بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بلبل ما را همین پرواز عبرت غنچه نیست</p></div>
<div class="m2"><p>ناله هم منقار شد از بسکه‌ گلشن تنگ بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرده‌ام اما خجالت از مزارم می‌دمد</p></div>
<div class="m2"><p>دور از آن در خاک‌گشتن هم غبار ننگ بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قید دل بیدل نفس را هرزه‌سنج وهم‌کرد</p></div>
<div class="m2"><p>شوخی ناز پری در شیشه پر بی‌سنگ بود</p></div></div>