---
title: >-
    غزل شمارهٔ ۱۸۸۹
---
# غزل شمارهٔ ۱۸۸۹

<div class="b" id="bn1"><div class="m1"><p>گاه به رنگ مایلی‌ گاه به بوی بی ‌نسق</p></div>
<div class="m2"><p>دستهٔ باطلت که‌بست ای‌چمن حضور حق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا تو ز حرص بگذری و ز غم جوع وارهی</p></div>
<div class="m2"><p>چیده زمین و آسمان عالم ‌کاسه و طبق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمر شد و همان بجاست غفلت خودنمایی‌ات</p></div>
<div class="m2"><p>از نظر تو دور رفت آینه‌های ماسبق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پوست به تن شکنجه چید هر سر مو به خم رسید</p></div>
<div class="m2"><p>منتخب چه نسخه است اینکه شکسته‌ای ورق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در عمل محال هم همت مرد سرخ‌روست</p></div>
<div class="m2"><p>برد علم بر آسمان پای حنایی شفق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تحفهٔ‌ محفل حضور درکف عرض هیچ نیست</p></div>
<div class="m2"><p>کاش شفیع ما شود آینه‌سازی عرق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قانع قسمت ازل وضع فضولش آفت است</p></div>
<div class="m2"><p>مغز به امتلا سپرد پسته دمی‌ که ‌گشت شق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواه دو روزه عمر گیر خواه هزار سال زی</p></div>
<div class="m2"><p>یک نفس است صد جنون‌، یک رمق است صد قلق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرکس ازین ستمکشان قابل التفات نیست</p></div>
<div class="m2"><p>چشم‌ به هر چه وا کند بیدل ماست مستحق</p></div></div>