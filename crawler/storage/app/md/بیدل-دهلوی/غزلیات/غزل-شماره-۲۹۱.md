---
title: >-
    غزل شمارهٔ ۲۹۱
---
# غزل شمارهٔ ۲۹۱

<div class="b" id="bn1"><div class="m1"><p>شرم از خط پیشانی ما ریخته شق‌ها</p></div>
<div class="m2"><p>زین جاده نرفته‌ست برون نقب عرق‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درس همه در سکتهٔ تدبیر مساوی ست</p></div>
<div class="m2"><p>در موج گوهر نیست پس و پیش سبق‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین خوان تهی مغتنم حرص شمارید</p></div>
<div class="m2"><p>لیسیدن اگر رو دهد از پشت طبق‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی‌ماحصل مشق دبستان وجودیم</p></div>
<div class="m2"><p>باید به خیالات سیه کرد ورق‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فریاد که بستند بر این هستی باطل</p></div>
<div class="m2"><p>یک گردن و صد رنگ ادا کردن حق‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیغت چه فسون داشت که چون بیضهٔ طاووس</p></div>
<div class="m2"><p>گل می‌کند از خاک شهید تو شفق‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیدل ز چه سوداست جنون‌جوشی این بحر</p></div>
<div class="m2"><p>عمری‌ست که دارد تب امواج قلق‌ها</p></div></div>