---
title: >-
    غزل شمارهٔ ۲۵۷۹
---
# غزل شمارهٔ ۲۵۷۹

<div class="b" id="bn1"><div class="m1"><p>منفعلم برکه برم حاجت خوبش از برتو</p></div>
<div class="m2"><p>ای قدمت بر سر من چون سر من بر در تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آینهٔ‌کون و مکان حیرت سیر چمن است</p></div>
<div class="m2"><p>ساغر رنگ دو جهان حسرت گرد سر تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تاب جمال تو ز کس راست نیاید ز هوس</p></div>
<div class="m2"><p>حلقهٔ گیسوی تو بس چشم تماشاگر تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محرم آن لعل نشدکام تمنای‌کسی</p></div>
<div class="m2"><p>غیرتبسم‌که برد چاشنی از شکرتو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رنگ تو آشفته چوگل در چمن آرزویت</p></div>
<div class="m2"><p>موج تو غلتان چوگهر در طلب‌گوهر تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبح برد تا به کجا پایه ز قطع نفسش</p></div>
<div class="m2"><p>وانشود زین هوسی چند ره منظر تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نُه فلک ازگردش سرگشته به خمیازه سمر</p></div>
<div class="m2"><p>همت ظرف که کشد بادهٔ بی‌ساغر تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سعی طلب بی سر و پا جادهٔ تحقیق رسا</p></div>
<div class="m2"><p>سبحه صفت آبله‌ها خفته برون در تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خط حساب من و ما راه گشاید ز کجا</p></div>
<div class="m2"><p>صفر نماید به نظر نقطه‌ای از دفتر تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل از افسون سخن بلبل باغ چه‌گلی</p></div>
<div class="m2"><p>رنگ چمن می‌شکند بوی بهار ازپرتو</p></div></div>