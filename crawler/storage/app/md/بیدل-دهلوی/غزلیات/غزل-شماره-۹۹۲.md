---
title: >-
    غزل شمارهٔ ۹۹۲
---
# غزل شمارهٔ ۹۹۲

<div class="b" id="bn1"><div class="m1"><p>حیا عمری‌ست با صد گردش رنگم طرف دارد</p></div>
<div class="m2"><p>عرق نقاش عبرت از جبین من صدف دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشد روشن صفای سینهٔ اخلاص‌کیشانت</p></div>
<div class="m2"><p>که درباب بهم جوشیدن دلها چه‌ کف دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به شغل لهو چندی رفع سردیهای دوران‌کن</p></div>
<div class="m2"><p>جهان حیز گرمی در خور آواز دف دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل از فکر معیشت جمع کن از علم و فن بگذر</p></div>
<div class="m2"><p>اگر جهل است و گر دانش همین آب و علف دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به توفانگاه آفات استقامت رنگ می‌بازد</p></div>
<div class="m2"><p>درین میدان کسی گر سینه‌ای دارد هدف دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز اقبال عرب غافل مباشید ای عجم‌زادان</p></div>
<div class="m2"><p>سریر اقتدار بلخ هم شاه نجف دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جدا نپسندد از خود هیچکس مشاطهٔ خود را</p></div>
<div class="m2"><p>مه تابان حضور شب در آغوش کلف دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قضا بر سجدهٔ ما بست اوج نشئهٔ عزت</p></div>
<div class="m2"><p>طلسم آبروی خاک در پستی شرف دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به نومیدی چمن سیر نگارستان افسوسم</p></div>
<div class="m2"><p>حنا داغ‌ست از رنگی که سودنهای کف دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به این عجزی‌که می‌بینم شکوه جراتت بیدل</p></div>
<div class="m2"><p>اگر مژگان توانی واکنی فتح دو صف دارد</p></div></div>