---
title: >-
    غزل شمارهٔ ۱۹۱۶
---
# غزل شمارهٔ ۱۹۱۶

<div class="b" id="bn1"><div class="m1"><p>بلبل الم غنچه کشد بیشتر از گل</p></div>
<div class="m2"><p>ظلمست به عاشق چه مدارا چه تغافل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خودداری شبنم چه‌ کند با تف خورشید</p></div>
<div class="m2"><p>ای یاد تو برق دو جهان رخت تحمل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کیفیت لعل تو ز بس نشئه‌گداز است</p></div>
<div class="m2"><p>در چشم حباب آینه دارد قدح مل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان نیش‌ که از اشک خم زلف تو دارد</p></div>
<div class="m2"><p>مشکل‌ که تپیدن نگشاید رگ سنبل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلهای خراب انجمن جلوهٔ یارند</p></div>
<div class="m2"><p>خورشید به ویرانه دهد عرض تجمل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما قمری آن سرو گلستان خرامیم</p></div>
<div class="m2"><p>دارد ز نشان قدمش‌ گردن ما غل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آیینهٔ دردیم چه عجز و چه رسایی</p></div>
<div class="m2"><p>اشک است اگر ناله‌ کند ساز تنزل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر غنچه ازین باغ‌ گره بستهٔ‌ نازیست</p></div>
<div class="m2"><p>اشکی است‌ گریبان‌ در چشم تر بلبل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اسرار سخن جز به خموشی نتوان یافت</p></div>
<div class="m2"><p>مفتاح در گنج معانیست تأمل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روزی دو به فکر قد خم‌ گشته فتادیم</p></div>
<div class="m2"><p>کردیم تماشای‌ گذشتن ز سر پل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خجلت شمر فرصت پرواز شراریم</p></div>
<div class="m2"><p>بیدل به چه امید توان ‌کرد توکّل</p></div></div>