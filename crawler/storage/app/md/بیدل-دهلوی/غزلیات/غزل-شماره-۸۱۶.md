---
title: >-
    غزل شمارهٔ ۸۱۶
---
# غزل شمارهٔ ۸۱۶

<div class="b" id="bn1"><div class="m1"><p>تا جنون نقد بهار عشرتم در چنگ داشت</p></div>
<div class="m2"><p>طفل اشکی هم‌که می‌دیدم به دامن سنگ داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمری از فیض لب خاموش غافل زیستم</p></div>
<div class="m2"><p>نغمهٔ عیش ابد این ساز بی‌آهنگ داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با همه وحشت غبار دامن خاکیم و بس</p></div>
<div class="m2"><p>اشک در عرض‌روانی نیز عذر لنگ داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ازگهر تهمت‌کش افسردن است اجزای بحر</p></div>
<div class="m2"><p>هرکه اینجا فال راحت زد مرا دلتنگ داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پای در دامن شکستم شد ره و منزل یکی</p></div>
<div class="m2"><p>جرأت رفتار در هرگام صد فرسنگ داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>موج لطف از جوهر تیغ عتابش چیده‌ایم</p></div>
<div class="m2"><p>غنچهٔ چین جبینش ازتبسم‌رنگ‌داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سعی هستی هیچ ما را برنیاورد از عدم</p></div>
<div class="m2"><p>آتش ما هرکجا زد شعله جا در سنگ داشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کاش هجران داد من می‌داد اگر وصلی نبود</p></div>
<div class="m2"><p>شمع‌تصویرم‌که از من سوختن هم ننگ‌داشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست جوش لاله وگل غیر افسون بهار</p></div>
<div class="m2"><p>هرقدر ما رنگ گرداندیم اونیرنگ‌داشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شمع را افروختن در داغ دل خواباند و رفت</p></div>
<div class="m2"><p>منت صیقل چه مقدار انفعال زنگ داشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نقش پرتو برنمی‌دارد جبین آفتاب</p></div>
<div class="m2"><p>غیر هم اوبود لیک ازنام بیدل ننگ داشت</p></div></div>