---
title: >-
    غزل شمارهٔ ۷۸۴
---
# غزل شمارهٔ ۷۸۴

<div class="b" id="bn1"><div class="m1"><p>مبتذل صبح و شام تازگی‌ آرنده نیست</p></div>
<div class="m2"><p>مسخرهٔ روزگار آنقدرش خنده نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آینه در پیش‌ گیر محرم تحقیق باش</p></div>
<div class="m2"><p>غیر ز خود رفتنت پیش تو آینده نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وشت طور زمان لمعهٔ برق است وبس</p></div>
<div class="m2"><p>علت‌ کوری‌ست‌ گر چشم تو ترسنده نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صافدلان فارغند شکوه ارهام چند</p></div>
<div class="m2"><p>گر دلت از خود پر است آینه شرمنده نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درکف اخلاق تست رشتهٔ تسخیر خلق</p></div>
<div class="m2"><p>غافل از احسان مباش هیچ کست بنده نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مصدر ایذای خلق در همه جا ناسزاست</p></div>
<div class="m2"><p>گر همه در پرپاست !بله زیبنده نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هیچکس از گل نچید رایحهٔ انفعال</p></div>
<div class="m2"><p>خبث چه بو می‌دهد گر دهنت ‌گنده نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طبع حرون خم نزد جزبه در احتیاج</p></div>
<div class="m2"><p>بی‌طلب‌کاه و جوگاو سرافکنده نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تخت سلیمان جاه پایهٔ قدرش هواست</p></div>
<div class="m2"><p>دود دماغ حباب آن همه پاینده نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فقر به هرجاکشد دامن اقبال ناز</p></div>
<div class="m2"><p>چرخ به صد طلسش پینهٔ یک زنده نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای همه وهم و گمان در الم رفتگان</p></div>
<div class="m2"><p>رشه‌کن و جامه در، یشم‌کسی‌کنده نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خواه دلت چاک زن خواه به سرخاک ریز</p></div>
<div class="m2"><p>دهر ز وضع غرور بهر تو گردنده نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به ‌که دل منفعل از خودت آگه ‌کند</p></div>
<div class="m2"><p>ور نه به پیشت ‌کسی آینه‌ دارنده نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیدل از این چارسو عشوه ی دیگر مخر</p></div>
<div class="m2"><p>غیر فنا هیچ جنس نزد حق ارزنده نیست</p></div></div>