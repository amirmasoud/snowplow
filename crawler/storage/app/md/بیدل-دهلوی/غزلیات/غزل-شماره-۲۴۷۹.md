---
title: >-
    غزل شمارهٔ ۲۴۷۹
---
# غزل شمارهٔ ۲۴۷۹

<div class="b" id="bn1"><div class="m1"><p>پریشان ‌کرد چون خاموشی‌ام آواز گردیدن</p></div>
<div class="m2"><p>ندارد جمع ‌گشتن جز به خویشم بازگردیدن‌</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هوس طرف جنون سیرم‌ ، مپرس ازکعبه و دیرم</p></div>
<div class="m2"><p>سر بی مغز و سامان هزار انداز گردیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگرهستی زجیب ذره صد خورشید بشکافد</p></div>
<div class="m2"><p>ندارد عقدهٔ موهومی من بازگردیدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر گرد سری دارم‌که در جولانگه نازش</p></div>
<div class="m2"><p>چو رنگم می‌شود بال و پر پروازگردیدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس از مردن بقدر ذره می‌باید غبارم را</p></div>
<div class="m2"><p>به ناموس وفا مهر لب غماز گردیدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دو عالم طور می‌خواهدکمین برق دیدارش . ..</p></div>
<div class="m2"><p>به یک آیینه دل نتوان حریف نازگردیدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرفتم گل شدی ای غنچه زین باغت رهایی کو</p></div>
<div class="m2"><p>گره وا کردن ست اینجا قفس پرواز گردیدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شرارت‌گر نگه واری پر افشاند غنیمت‌دان</p></div>
<div class="m2"><p>به رنگ رفته نتوان بیش از این‌گلباز گردیدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فنا هم دستگاه هستی بسیارمی‌خواهد</p></div>
<div class="m2"><p>بقدر سرمه‌گشتن بایدم بسیارگردیدن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خط پرگارنیرنگی‌ست بیدل نقش ایجادم</p></div>
<div class="m2"><p>هزار انجام طی کرده‌ست این آغاز گردیدن</p></div></div>