---
title: >-
    غزل شمارهٔ ۲۳۸۶
---
# غزل شمارهٔ ۲۳۸۶

<div class="b" id="bn1"><div class="m1"><p>دل حیرت آفرین است هر سو نظرگشاییم</p></div>
<div class="m2"><p>در خانه هیچکس نیست آیینه است و ماییم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین بیشتر چه باشد هنگامهٔ توهم</p></div>
<div class="m2"><p>چون‌گرد صبح‌ عمریست هیچیم و خود نماییم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما را چو شمع ازین بزم بیخود گذشتنی هست</p></div>
<div class="m2"><p>گردون چه برفرازبم سر نیستیم پاییم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا چند دانهٔ ما نازد به سخت جانی</p></div>
<div class="m2"><p>در یک دو روز دیگر بیرون آسیاییم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آیینهٔ سعادت اقبال بی‌نشانی است</p></div>
<div class="m2"><p>گر استخوان شود خاک بر فرق خود نماییم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آیینه مشربی‌ها بیگانهٔ وفا نیست</p></div>
<div class="m2"><p>جایش به‌دیده گرم‌است با هرکه آشناییم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عجز طلب در این دشت با ما چو اشک چشم است</p></div>
<div class="m2"><p>هر چند ره به پهلوست محتاج صد عصاییم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شبنم چه جام گیرد از نشئهٔ تعین</p></div>
<div class="m2"><p>در باده آب داریم‌، پیمانهٔ حیاییم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>محتاج زندگی را عزت چه احتمالست</p></div>
<div class="m2"><p>لبریز نقد لذت چون کیسهٔ گداییم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا کی ‌کشد تعین ادبار نسبت ما</p></div>
<div class="m2"><p>ننگی چو بار مردن درگردن بقاییم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ظاهر خروش سازش باطن جهان نازش</p></div>
<div class="m2"><p>ای محرمان بفهمید ما زین میان‌ کجاییم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شخص هوا مثالیم خمیازهٔ خیالیم</p></div>
<div class="m2"><p>گر صد فلک ببالیم صفر عدم فزاییم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رنگ حناست هستی فرصت ‌کمین تغییر</p></div>
<div class="m2"><p>روز سیاه خود را تا کی شفق نماییم‌</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گوش مروتی‌ کو کز ما نظر نپوشد</p></div>
<div class="m2"><p>دست غریق یعنی فریاد بی‌صداییم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر هر چه دیده واکرد آغوش الفت ما</p></div>
<div class="m2"><p>مژگان به خم زد و گفت خوش باش پشت پاییم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دوزخ‌ کجاست بیدل جز انفعال غفلت</p></div>
<div class="m2"><p>آتش حریف ما نیست زبن آب اگر برآییم</p></div></div>