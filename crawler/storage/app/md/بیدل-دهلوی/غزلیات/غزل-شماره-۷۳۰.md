---
title: >-
    غزل شمارهٔ ۷۳۰
---
# غزل شمارهٔ ۷۳۰

<div class="b" id="bn1"><div class="m1"><p>صفای حال ما مغشوش رنگیست</p></div>
<div class="m2"><p>عدم ‌را نام هستی سخت ننگیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز قید سخت جانیها مپرسید</p></div>
<div class="m2"><p>شرار ما قفس فرسوده سنگیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هر جا بال عجز ما گشودند</p></div>
<div class="m2"><p>پر پرواز نقش پای لنگی ‌ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نواهایی که دارد ساز زنجیر</p></div>
<div class="m2"><p>ز شست شهرت‌مجنون خدنگیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهان گرد سویدای که دارد</p></div>
<div class="m2"><p>ز داغ لاله این صحرا پلنگیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سراپا بالم و از عجز طاقت</p></div>
<div class="m2"><p>چوگل پروازم از رنگی به رنگیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو شمع از فکر هستی می‌گدازم</p></div>
<div class="m2"><p>بغل واکردن جیبم نهنگیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکستن شاقی بزم است هشدار</p></div>
<div class="m2"><p>می و‌ مینا و جام اینجا نرنگیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جهان ‌، جنس بد و نیکی ندارد</p></div>
<div class="m2"><p>تویی سرمایه ‌هر جا صلح‌ و جنگیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به یکتایی طرف‌گردیدنت چند</p></div>
<div class="m2"><p>خیال‌اندیشی آیینه زنگیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نواپروردهٔ عجزیم بیدل</p></div>
<div class="m2"><p>درین دریا خم هر موج چنگیست</p></div></div>