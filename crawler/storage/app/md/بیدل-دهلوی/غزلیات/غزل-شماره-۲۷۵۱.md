---
title: >-
    غزل شمارهٔ ۲۷۵۱
---
# غزل شمارهٔ ۲۷۵۱

<div class="b" id="bn1"><div class="m1"><p>رمی‌’ بیتابیی‌، تغییر رنگی‌،‌گردش حالی</p></div>
<div class="m2"><p>فسردی بیخبر، جهدی که شاید واکنی بالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به رنگ غنچه نتوان عافیت مغرور گردیدن</p></div>
<div class="m2"><p>پریشانی بود تفصیل هر جمعیت اجمالی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بغیر از نفی هستی محرم اثبات نتوان شد</p></div>
<div class="m2"><p>همان پرواز رنگت بسته بر آیینه تمثالی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حصول آب و رنگ امتیاز آسان نمی‌باشد</p></div>
<div class="m2"><p>بسوز و داغ شو تا بر رخ هستی نهی خالی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به ذوق سوختن زین انجمن کلفت غنیمت دان</p></div>
<div class="m2"><p>همین شام است و بس گر شمع دارد صبح اقبالی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تحیر زحمت تکلیف دیگر برنمی‌دارد</p></div>
<div class="m2"><p>نگه باش و مژه بردار هر باری و حمالی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من از سود و زبان آگه نی‌ام لیک اینقدر دانم</p></div>
<div class="m2"><p>که جنس عافیت را جز خموشی نیست دلالی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به هر جا رفته‌ایم از خود اثر رفته‌ست پیش از ما</p></div>
<div class="m2"><p>غباری کو که نازد کاروان ما به دنبالی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به رسوایی‌کشید از شوخی چاک‌گریبانت</p></div>
<div class="m2"><p>تبسم از سحر همچون شکنج از چهرهٔ زالی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به هیچ آهنگ عرض مدعا صورت نمی‌بندد</p></div>
<div class="m2"><p>چو مضمون بلند افتاده‌ام در خاطر لالی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مگر خاکستر دل دارد استقبال آهنگم</p></div>
<div class="m2"><p>که از طبع سپند من تپیدن می‌کشد بالی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تپش در طبع امواجست سعی‌گوهر آرایی</p></div>
<div class="m2"><p>تبی دارم که خواهد ریخت آخر رنگ تبخالی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه پردازم به اظهار خط بیمطلب هستی</p></div>
<div class="m2"><p>مگر از خامهٔ تحقیق بیرون افکنم نالی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به ناسور جگر عمری‌ست گرد ناله می‌ریزم</p></div>
<div class="m2"><p>خوشا عرض بضاعتها کف خاکی و غربالی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز تشریف جهان بیدل به عریانی قناعت ‌کن</p></div>
<div class="m2"><p>که ‌گل اینجا همین یک جامه می‌یابد پس از سالی</p></div></div>