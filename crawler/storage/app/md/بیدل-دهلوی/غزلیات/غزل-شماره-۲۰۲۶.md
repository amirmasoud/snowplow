---
title: >-
    غزل شمارهٔ ۲۰۲۶
---
# غزل شمارهٔ ۲۰۲۶

<div class="b" id="bn1"><div class="m1"><p>شبی مشتاق رنگ ‌آمیزی تصویر دل ‌گشتم</p></div>
<div class="m2"><p>زگال مشق این فن بر سیاهی زد خجل ‌گشتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غباری بودم از آشفتگی نومید آسودن</p></div>
<div class="m2"><p>پر افشانی عرقها کرد تا امروز گل‌ گشتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ستم از هیأت تسلیم خوبان شرم می‌دارد</p></div>
<div class="m2"><p>دم تیغ قضا برگشت تا خون بحل‌ گشتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وبال موی پیری در نگیرد هیچ‌ کافر را</p></div>
<div class="m2"><p>شبم این بسکه با صبح قیامت متصل‌گشتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حیا ضبط عنان آتش یاقوت من دارد</p></div>
<div class="m2"><p>شررها آب شد تا اینقدرها مشتعل‌ گشتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز دقت تنگ‌ کردم فطرت ارباب دانش را</p></div>
<div class="m2"><p>چو مو در دیده‌ها از معنی نازک مخل گشتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قناعت هر چه باشد زحمت دلها نمی‌خواهد</p></div>
<div class="m2"><p>در مطلب زدم بر طبع خلقی دق و سل‌گشتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به دل چندان‌ که می‌جویم سراغ خود نمی‌یابم</p></div>
<div class="m2"><p>نمی‌دانم چه بودم در خیالش مضمحل‌گشتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سحر هر سو خرامد شبنم ایجاد عرق دارم</p></div>
<div class="m2"><p>نفس پرواز دادم کاینقدرها منفعل گشتم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بهار رنگم از آسودگی طرفی نبست آخر</p></div>
<div class="m2"><p>چه سازم آشنای فرصت پیمان گسل گشتم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تلاش شوق از محرومی من داغ شد بیدل</p></div>
<div class="m2"><p>که برگرد جهانی چون نفس بیرون دل ‌گشتم</p></div></div>