---
title: >-
    غزل شمارهٔ ۲۳۶۰
---
# غزل شمارهٔ ۲۳۶۰

<div class="b" id="bn1"><div class="m1"><p>خلوت‌پرست گوشهٔ حیرانی خودیم</p></div>
<div class="m2"><p>یعنی نگاه دیدهٔ قربانی خودیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را چو صبح باگل تعمیرکار نیست</p></div>
<div class="m2"><p>مشتی غبار عالم ویرانی خودیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لاف بقا و زندگی رفته نازکیست</p></div>
<div class="m2"><p>لنگر فروش کشتی توفانی خودیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>موگشته‌ایم و نقش خیال تو مشق ماست</p></div>
<div class="m2"><p>حیران صنعت قلم مانی خودیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پر هرزه بود چشم‌گشودن دین بساط</p></div>
<div class="m2"><p>چون شمع جمله اشک پشیمانی خودیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جمعیت از غبار هوای رمیده است</p></div>
<div class="m2"><p>صبح جنون بهار پریشانی خودیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون اشک راز ما به هزار آب شسته‌اند</p></div>
<div class="m2"><p>آیینهٔ خجالت عریانی خودیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاک فسرده خواری جاوید می‌کشد</p></div>
<div class="m2"><p>عمریست پایمال تن‌آسانی خودیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیوار رنگ منع خرام بهار نیست</p></div>
<div class="m2"><p>ای خام فطرتان همه زندانی خودیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل چوگردباد ز آرام ما مپرس</p></div>
<div class="m2"><p>عمریست درکمند پرافشانی خودیم</p></div></div>