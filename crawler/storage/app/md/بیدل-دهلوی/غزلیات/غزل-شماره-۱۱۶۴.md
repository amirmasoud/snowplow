---
title: >-
    غزل شمارهٔ ۱۱۶۴
---
# غزل شمارهٔ ۱۱۶۴

<div class="b" id="bn1"><div class="m1"><p>آه به درد عجز هم‌ کوشش ما نمی‌رسد</p></div>
<div class="m2"><p>آبله‌گریه می‌کند اشک به پا نمی‌رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نغمهٔ‌ساز ما و من تفرقهٔ ‌دل است و بس</p></div>
<div class="m2"><p>تا دو دلش نمی‌کنی لب به صدا نمی‌رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند به فرصت نفس غره ی ناز زیستن</p></div>
<div class="m2"><p>در چمنی ‌که جای ‌ماست بوی ‌هوا نمی‌رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تنگی این‌نه آسیا در پی دورباش ماست</p></div>
<div class="m2"><p>ما دو سه دانه‌ایم لیک نوبت جا نمی‌رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خنده درین چمن خطاست‌ ناز شکفتگی‌ بلاست</p></div>
<div class="m2"><p>تا نگذاردش عرق‌ گل به حیا نمی‌رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخت ز هم‌گذشتهٔم زحمت ناله‌کم دهید</p></div>
<div class="m2"><p>بر پی‌کاروان ما بانگ درا نمی‌رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مقصد بی بر چنار نیست به غیر سوختن</p></div>
<div class="m2"><p>دست به چرخ برده‌ایم لیک دعا نمی‌رسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سایه بهٔمن عاجزی ایمن ازآب و آتش است</p></div>
<div class="m2"><p>سر به زمین فکنده را هیچ بلا نمی‌رسد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در تو هزار جلوه است کز نظرت نهفته‌اند</p></div>
<div class="m2"><p>ترک خیال و وهم کن آینه وانمی‌رسد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قاصد وصل در ره است منتظرپیام باش</p></div>
<div class="m2"><p>آنچه به‌ما رسیدنی‌ست تا به‌کجا نمی‌رسد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کوشش موج و قطره‌ها همقدم است با محیط</p></div>
<div class="m2"><p>هرکه به هر کجا رسد از تو جدا نمی‌رسد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عجز بساط اعتبار از مدد غرور چند</p></div>
<div class="m2"><p>بنده به خود نمی‌رسد تا به خدا نمی‌رسد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ربط وفاق جزوها پاس رعایت‌ کل ‌ست</p></div>
<div class="m2"><p>زخم جدایی دو تار جز به قبا نمی‌رسد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر درکبریای عشق بارگمان و وهم نیست</p></div>
<div class="m2"><p>گر تو رسیده‌ای به او بیدل ما نمی‌رسد</p></div></div>