---
title: >-
    غزل شمارهٔ ۱۷۴۸
---
# غزل شمارهٔ ۱۷۴۸

<div class="b" id="bn1"><div class="m1"><p>در این بساط هوس پیش از اعتبار نفس</p></div>
<div class="m2"><p>همان به دوش هوا بسته‌ گیر بار نفس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صفای آینه در رنگ وهم باخته‌ایم</p></div>
<div class="m2"><p>به زیر سایهٔ‌ کوهیم از غبار نفس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هیچ وضع نبردیم صرفهٔ هستی</p></div>
<div class="m2"><p>چو صبح ضبط خود آید مگر به‌ کار نفس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به رنگ شمع سحرفرصتی نمی‌خواهد</p></div>
<div class="m2"><p>خزان عشرت و رنگینی بهار نفس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در این چمن اثر اشک شبنم آینه است</p></div>
<div class="m2"><p>که آب شد سحر از شرم‌ گیرودار نفس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غرور هستی ما را گر انتقامی هست</p></div>
<div class="m2"><p>بس است اینکه خمیدیم زبر بار نفس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شرار کاغذ آتش زده است فرصت عیش</p></div>
<div class="m2"><p>فشاندن پر ما نیست جز شمار نفس.</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به ساز انجمن هستی آتش افتاده ‌ست</p></div>
<div class="m2"><p>چو نبض تب‌زده مشکل بود قرار نفس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل است آینه‌دار غبار ما و منت</p></div>
<div class="m2"><p>وگرنه عرض نهانی‌ست آشکار نفس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هزار صبح در این باغ بار حسرت بست</p></div>
<div class="m2"><p>گشاده‌گیر تو هم یک دو دم کنار نفس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همان به ذوق تماشاست زندگانی من</p></div>
<div class="m2"><p>به زنگ چشم نگاهم بس است تار نفس</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز ضعف تنگدلیها چو غنچهٔ تصویر</p></div>
<div class="m2"><p>نشسته‌ام به سر راه انتظار نفس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شکست جام حبابم غریب حوصله داشت</p></div>
<div class="m2"><p>محیط می‌کشم امروز از خمار نفس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به عالمی‌که من از دست زندگی داغم</p></div>
<div class="m2"><p>نگردد آتش افسرده هم دچار نفس</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بهار عمر ندارد گلی دگر بیدل</p></div>
<div class="m2"><p>نچید هیچکس اینجا به غیر خار نفس</p></div></div>