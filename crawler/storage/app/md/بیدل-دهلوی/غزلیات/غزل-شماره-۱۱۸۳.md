---
title: >-
    غزل شمارهٔ ۱۱۸۳
---
# غزل شمارهٔ ۱۱۸۳

<div class="b" id="bn1"><div class="m1"><p>صبحی‌ که‌ گلت به باغ باشد</p></div>
<div class="m2"><p>گل در بغل چراغ باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تمثال شریک حسن مپسند</p></div>
<div class="m2"><p>گو آینه بی‌تو داغ باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای سایه نشان خویش‌ گم‌ کن</p></div>
<div class="m2"><p>تا خورشیدت سراغ باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنسوی عدم دو گام واکش</p></div>
<div class="m2"><p>گرآرزوی فراغ باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مردیم به حسرت دل جمع</p></div>
<div class="m2"><p>این غنچه‌گل چه باغ باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گویند بهشت جای خوبی‌ست</p></div>
<div class="m2"><p>آنجا هم اگر دماغ باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیدل به امید وصل شادیم</p></div>
<div class="m2"><p>گو طوطی بخت زاغ باشد</p></div></div>