---
title: >-
    غزل شمارهٔ ۲۷۵۷
---
# غزل شمارهٔ ۲۷۵۷

<div class="b" id="bn1"><div class="m1"><p>گر نیست در این میکدهها دور تمامی</p></div>
<div class="m2"><p>قانع چو هلالیم به نصف خط جامی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ملک قناعت به مه و مهر مپرداز</p></div>
<div class="m2"><p>گر نان شبی هست و چراغ سر شامی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این باغ چه دارد ز سر و برگ تعین</p></div>
<div class="m2"><p>تخم‌، آرزوی پوچ و، ثمر، فطرت خامی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنیاد غرور همه بر دعوی پوچ است</p></div>
<div class="m2"><p>در عرصهٔ ما تیغ‌کشیده‌ست نیامی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاهان به‌نگین غره‌، گدایان به قناعت</p></div>
<div class="m2"><p>هستی همه را ساخته خفت‌کش نامی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عبرت خبری می‌دهد از فرصت اقبال</p></div>
<div class="m2"><p>این وصل نه زانهاست که ارزدبه پیامی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلها همه مجموعهٔ نیرنگ فسونند</p></div>
<div class="m2"><p>هر دانه‌که دیدی ‌گرهی بود به دامی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هستی روش ناز جنون تاز که دارد</p></div>
<div class="m2"><p>می‌آیدم از گرد نفس بوی خرامی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا مهر رخش از چه افق جلوه نماید</p></div>
<div class="m2"><p>گوش همه پرکرده صدای لب بامی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آفاق ز پرواز غبارم مژه پوشید</p></div>
<div class="m2"><p>زین سرمه به هر چشم رسیده‌ست سلامی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل چه ازل‌ کو ابد، از وهم برون آی</p></div>
<div class="m2"><p>درکشور تحقیق نه صبح است و نه شامی</p></div></div>