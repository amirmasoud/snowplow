---
title: >-
    غزل شمارهٔ ۵۵۷
---
# غزل شمارهٔ ۵۵۷

<div class="b" id="bn1"><div class="m1"><p>خامش نفسم شوخی آهنگ من این است</p></div>
<div class="m2"><p>سر جوش بهار ادبم رنگ من این است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمری‌ست گرفتار خم پیکر عجزم</p></div>
<div class="m2"><p>تا بال وپرنغمه شوم چنگ من این است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیتاب هواسنجی عمرم چه توان‌کرد</p></div>
<div class="m2"><p>میزان خیال نفسم سنگ من این است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خمیازه‌ام آرایش پیمانهٔ هستی‌ست</p></div>
<div class="m2"><p>چون صبح خمارم مشکن رنگ من این است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موج می و آرایش‌گوهر چه خیال است</p></div>
<div class="m2"><p>ناموس جهان تپشم ننگ من این است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه ذوق هنر دارم و نه محوکمالم</p></div>
<div class="m2"><p>مجنون توام دانش و فرهنگ من این است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با هرکه طرف‌گشته‌ام آرایش اویم</p></div>
<div class="m2"><p>آیینه‌ام و خاصیت جنگ من این است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ظلم است رفیقان ز دل خسته‌گذشتن</p></div>
<div class="m2"><p>گر آبله دارد قدم لنگ من این است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نامحرم آن جلوه‌ام از بیدلی خویش</p></div>
<div class="m2"><p>آیینه ندارم چه‌کنم زنگ من این است</p></div></div>