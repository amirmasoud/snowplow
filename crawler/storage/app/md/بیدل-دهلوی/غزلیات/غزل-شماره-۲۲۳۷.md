---
title: >-
    غزل شمارهٔ ۲۲۳۷
---
# غزل شمارهٔ ۲۲۳۷

<div class="b" id="bn1"><div class="m1"><p>به رنگ‌گلشن ازفیض حضورت عشرت آهنگم</p></div>
<div class="m2"><p>مشو غایب‌ که چون آیینه از رخ می‌پرد رنگم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حیا را کرده‌ام قفل در دکان رسوایی</p></div>
<div class="m2"><p>به رنگ غنچه پنهانست جیب پاره در چنگم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز مردم بسکه چون آیینه دیدم سخت‌ روییها</p></div>
<div class="m2"><p>نگه در دیده پیچیده است مانند رگ سنگم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوشا روزی‌ که نقاش نگارستان استغنا</p></div>
<div class="m2"><p>کشد تصویر من چندان که بیرون آرد از رنگم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به رنگ سایه از خود غافلم لیک اینقدر دانم</p></div>
<div class="m2"><p>که‌ گر پنهان شوم نورم و گر پیدا همین رنگم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شدم پیر و نی‌ام محرم نوای نالهٔ دردی</p></div>
<div class="m2"><p>محبت‌ کاش بنوازد طفیل قامت چنگم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز خاک آستانت چشم بی نم می‌برم اما</p></div>
<div class="m2"><p>دلی دارم‌ که خواهد آب گردید آخر از ننگم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به ظرف غنچه دشوار است بودن نکهت ‌گل را</p></div>
<div class="m2"><p>نمی‌گنجد نفس در سینهٔ من بسکه دلتنگم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تنک ظرفی چو من در بزم میخواران نمی‌باشد</p></div>
<div class="m2"><p>که دور جام بیهوشی است چون‌ گل ‌گردش رنگم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مگر بر هم توانم زد صف جمعیتت رنگی</p></div>
<div class="m2"><p>به رنگ شمع یکسر تیغم و با خویش در جنگم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به وضع احتراز هر دو عالم باج می‌گیرم</p></div>
<div class="m2"><p>جهانگیر است چون خورشید ناگیرایی چنگم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>طرف در تنگنای عرصهٔ امکان نمی‌گنجد</p></div>
<div class="m2"><p>همان با خوبش دارم‌کار،‌ گر صلح است و گر جنگم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به وهم عافیت چون غنچه محروم از گلم بیدل</p></div>
<div class="m2"><p>شکستی‌ کو که رنگ دامن او ریزد از چنگم</p></div></div>