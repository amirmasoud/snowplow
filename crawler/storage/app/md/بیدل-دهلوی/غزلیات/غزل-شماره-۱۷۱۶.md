---
title: >-
    غزل شمارهٔ ۱۷۱۶
---
# غزل شمارهٔ ۱۷۱۶

<div class="b" id="bn1"><div class="m1"><p>سودای تک و تاز هوسها ز سر انداز</p></div>
<div class="m2"><p>پرواز به جایی نتوان برد پر انداز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرجا تویی آشوب همین دود و غبار است</p></div>
<div class="m2"><p>از خویش برآ طرح جهان دگر انداز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شوری‌که ز زیر و بم این پرده شنیدی</p></div>
<div class="m2"><p>حرف لب‌گنگش‌کن و درگوش‌کر انداز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رسوایی عیب و هنر خلق میندیش</p></div>
<div class="m2"><p>ضبط مژه کن پردهٔ ناموس درانداز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صلح و جدل عالم افسرده مساوی‌ست</p></div>
<div class="m2"><p>رو آتش یاقوت در آب‌گهر انداز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این عرصه اشارتگه ابروی هلالی‌ست</p></div>
<div class="m2"><p>اینجا به دم تیغ برون آ سپر انداز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمفرصتی عمر غبار نفسم را</p></div>
<div class="m2"><p>داده‌ست ردایی که به دوش سحر انداز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر از تو سراغ من‌ گمگشته بپرسند</p></div>
<div class="m2"><p>بردارکفی خاک و به چشم اثر انداز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شیرینی جان نیست‌ گلوسوز چو شمعم</p></div>
<div class="m2"><p>ای صبح تبسم نمکی در شکر انداز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نامحرم عبرتکدهٔ دل نتوان بود</p></div>
<div class="m2"><p>این خانه بروب از خود و بیرون در انداز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ما خود نرسیدیم به تحقیق میانش</p></div>
<div class="m2"><p>گر دست‌رسا هست تو هم درکمر انداز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پرسیدم از آوارگی دربه‌دری چند</p></div>
<div class="m2"><p>گفتند مپرسید از آن خانه برانداز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بیدل ز تو تا من نتوان فرق نمودن</p></div>
<div class="m2"><p>گر آینه خواهی به مزارم نظر انداز</p></div></div>