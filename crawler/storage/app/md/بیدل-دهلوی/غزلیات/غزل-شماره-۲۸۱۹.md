---
title: >-
    غزل شمارهٔ ۲۸۱۹
---
# غزل شمارهٔ ۲۸۱۹

<div class="b" id="bn1"><div class="m1"><p>ای که در دیر و حرم مست‌ کرم می‌آیی</p></div>
<div class="m2"><p>دل چه دارد که درین غمکده کم می‌آیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوهر ناز چه مقدار تری می‌چیند</p></div>
<div class="m2"><p>که به حسرتکدهٔ دیدهٔ نم می‌آیی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اینقدر سلسلهٔ ناز که دیده‌ست رسا؟</p></div>
<div class="m2"><p>عمرها شد که به هر سو نگرم می آیی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صمدی لیک دربن انجمن عجز نگاه</p></div>
<div class="m2"><p>به چمن سازی آثار صنم می آ‌یی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چقدر لطف تو فریادرس‌ بی‌‌بصری‌ست</p></div>
<div class="m2"><p>که به چشم همه‌کس دیر و حرم می‌آیی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عقل و حس غیر تحیر چه ترازد اینجا</p></div>
<div class="m2"><p>کز حدوث آینه‌پرداز قدم می‌‌آیی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عرض تنزیه به تشبیه نمی‌آید راست</p></div>
<div class="m2"><p>سحر کاریست که معنی به رقم می‌آ‌یی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فقر نازد که به تجرید نظر دوخته‌ای</p></div>
<div class="m2"><p>جاه بالد که به سامان حشم می‌آیی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای نفس آمد و رفت هوست داغم‌ کرد</p></div>
<div class="m2"><p>می‌رو‌ی سوی عدم باز عدم می‌آیی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چشم تا بسته‌ای‌، آفاق سواد مژه است</p></div>
<div class="m2"><p>صد شق خامه ز یک نقطه به هم می‌آیی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چینت از دامن آرام به هرجا گل ‌کرد</p></div>
<div class="m2"><p>ذره تا مهر به آرایش هم می‌آیی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>انتظار تو به هر رهگذرم دارد فرش</p></div>
<div class="m2"><p>هرکجا پای نهی پا به سرم می‌آ‌یی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کم آرایش تسلیم نگیری زنهار</p></div>
<div class="m2"><p>ابروی نازی اگر مایل خم می‌آیی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چه ضرور است ‌کشی رنج وداعم بیدل</p></div>
<div class="m2"><p>می‌روم من به مقامی‌ که تو هم می‌آیی</p></div></div>