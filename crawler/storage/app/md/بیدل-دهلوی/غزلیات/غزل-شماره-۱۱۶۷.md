---
title: >-
    غزل شمارهٔ ۱۱۶۷
---
# غزل شمارهٔ ۱۱۶۷

<div class="b" id="bn1"><div class="m1"><p>هرگز به دستگاه نظر پا نمی‌رسد</p></div>
<div class="m2"><p>کور عصاپرست به بینا نمی‌رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر طفل غنچه هم سبق درس صبح نیست</p></div>
<div class="m2"><p>هر صاحب‌نفس به مسیحا نمی‌رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل خاک‌ گشت و شوخی رنگ حنا نیافت</p></div>
<div class="m2"><p>افسوس جبهه‌ای که به آن پا نمی‌رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این است اگر حقیقت نیرنگ وعده‌ات</p></div>
<div class="m2"><p>ماییم و فرصتی که به فردا نمی‌رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از نقش اعتبار جهان سخت ساده‌ایم</p></div>
<div class="m2"><p>تمثال کس به آینهٔ ما نمی‌رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در جستجوی ما نکشی زحمت سراغ</p></div>
<div class="m2"><p>جایی رسیده‌ایم که عنقا نمی‌رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما را چو سیل خاک به سر کردن است و بس</p></div>
<div class="m2"><p>تا آن زمان که دست به دریا نمی‌رسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آسوده‌اند صافدلان از زبان خلق</p></div>
<div class="m2"><p>ازموج می شکست به مینا نمی‌رسد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یک دست می‌دهد سحر و شام روزگار</p></div>
<div class="m2"><p>هیچ آفتی به این گل رعنا نمی‌رسد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در گلشنی که اوست چه شبنم‌، کدام رنگ</p></div>
<div class="m2"><p>یعنی دعای بوی‌گل آنجا نمی‌رسد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رمز دهان یار ز ما بیخودان مپرس</p></div>
<div class="m2"><p>طبع سقیم ما به معما نمی‌رسد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زاهد دماغ توبه به کوثر رسانده‌ای</p></div>
<div class="m2"><p>معذور کاین خیال به صهبا نمی‌رسد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آخر به رنگ نقش قدم خاک ‌گشتن است</p></div>
<div class="m2"><p>آیینه پیش پا وکسی وانمی‌رسد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیدل به عرض جوهر اسرار خوب و زشت</p></div>
<div class="m2"><p>آیینه‌ای به صفحهٔ سیما نمی‌رسد</p></div></div>