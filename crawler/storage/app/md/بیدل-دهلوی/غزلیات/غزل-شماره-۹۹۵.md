---
title: >-
    غزل شمارهٔ ۹۹۵
---
# غزل شمارهٔ ۹۹۵

<div class="b" id="bn1"><div class="m1"><p>جنون از بس شکست آبله در هر قدم دارد</p></div>
<div class="m2"><p>بنای خانهٔ زنجیر ما چون موج نم دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به برقم می‌دهد خرمن خیال موج رفتاری</p></div>
<div class="m2"><p>که اعجاز خرامش آب و آتش را به هم دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز لعل خامشت رمز تبسم‌ کیست بشکافد</p></div>
<div class="m2"><p>خیالی دست بر چاک گریبان عدم دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فضولیهای امید اینقدر جان می‌کند ورنه</p></div>
<div class="m2"><p>دل‌الفت‌پرست یاس از شادی چه غم دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به ترگ جاه زن تا درنگیرد ننگ افلاست</p></div>
<div class="m2"><p>که رنج‌خودفروشی می‌کشد هرکس درم دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به‌ لغزش چون ‌ننالد خامهٔ حسرت صریر من</p></div>
<div class="m2"><p>که زنجیر سیه‌بختی به تحریک قدم دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز تدبیر محبت غافلم لیک اینقدر دانم</p></div>
<div class="m2"><p>که دل تا آتشی در سینه دارد دیده نم دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگه ننگاشت صنع آ‌گهی در دیده اعیان</p></div>
<div class="m2"><p>قلم در نرگسستان یک قلم سه‌ و القلم دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مدار ای ‌زشت‌رو امید تحسین ‌از صفا کیشان</p></div>
<div class="m2"><p>که اسباب خوش‌آمد خانهٔ آیینه‌کم دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نوای‌عیش‌گو خون شو، دمی با درد سوداکن</p></div>
<div class="m2"><p>نفس با این بضاعت هرچه دارد مغتنم دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر دشمن تواضع‌پیشه است ایمن مشو بیدل</p></div>
<div class="m2"><p>به خونریزی بود بی‌باک شمشیری‌ که خم دارد</p></div></div>