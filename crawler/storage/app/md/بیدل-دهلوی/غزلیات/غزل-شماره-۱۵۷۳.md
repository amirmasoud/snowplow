---
title: >-
    غزل شمارهٔ ۱۵۷۳
---
# غزل شمارهٔ ۱۵۷۳

<div class="b" id="bn1"><div class="m1"><p>موی دماغ جاه و حشم حل نمی‌شود</p></div>
<div class="m2"><p>فغفور خاک‌گشت و سرش‌کل نمی‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما و من هوسکدهٔ اعتبار خلق</p></div>
<div class="m2"><p>تقریر مهملی است‌که مهمل نمی‌شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زبن گرد اعتبار مچین دستگاه ناز</p></div>
<div class="m2"><p>بر یکدگر چو سایه فتد تل نمی‌شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آیینه‌دار جوهر مرد استقامت است</p></div>
<div class="m2"><p>پرداز تیغ کوه به صیقل نمی‌شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>افسردگی‌کمینگر تعطیل وقت ماست</p></div>
<div class="m2"><p>تا دست گرم کار بود شل نمی‌شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناقدردان راحت وضع زمانه‌ای</p></div>
<div class="m2"><p>تا دردسر به طبع تو صندل نمی‌شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با این دو چشم کاینه‌دار دو عالم است</p></div>
<div class="m2"><p>انسان تحیر است که احول نمی‌شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زبن آرزوکه سرمه نظرگاه چشم اوست</p></div>
<div class="m2"><p>حیف است اصفهان همه مکحل نمی‌شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای خواجه خواب راحت از اقبال رفته‌گیر</p></div>
<div class="m2"><p>این کار بوریاست ز مخمل نمی‌شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با وهم و ظن معامله طول اوفتاده است</p></div>
<div class="m2"><p>عالم مفصلی‌ست که مجمل نمی‌شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل‌ کسی به عرش حقیقت نمی‌رسد</p></div>
<div class="m2"><p>تا خاک راه احمد مرسل نمی‌شود</p></div></div>