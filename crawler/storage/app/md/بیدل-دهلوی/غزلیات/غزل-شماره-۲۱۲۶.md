---
title: >-
    غزل شمارهٔ ۲۱۲۶
---
# غزل شمارهٔ ۲۱۲۶

<div class="b" id="bn1"><div class="m1"><p>بسکه چون طاوو‌س‌، پیچیده‌ست مستی در سرم</p></div>
<div class="m2"><p>جام‌ها در گردش آید گر به خود جنبد پرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرد بادم‌، مستی‌ام موقوف کوه و دشت نیست</p></div>
<div class="m2"><p>هر کجا گردید سر در گردش آمد ساغرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تازه است از من بهار سنبلستان خیال</p></div>
<div class="m2"><p>جوهر آیینهٔ زانو بود موی سرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>موج بر هم خورده دارد عرض سامان حباب</p></div>
<div class="m2"><p>می‌توان تعمیر دل‌ کرد از شکست پیکرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وحشت آفاق در گرد سحر خوابیده است</p></div>
<div class="m2"><p>می‌کند خلقی جنون تا من‌ گریبان می‌درم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با خیال جلوهٔ خورشید افتاده‌ست‌ کار</p></div>
<div class="m2"><p>همچو شبنم می‌کند بال از نگه چشم ترم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیستم بی‌ سعی وحشت با همه افسردگی</p></div>
<div class="m2"><p>بلبل تصویرم و تا رنگ دارم می‌پرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حیرتم حیرت ز نیرنگ بد و نیکم مپرس</p></div>
<div class="m2"><p>برده است آیینه‌ گشتن در جهان دیگرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نالهٔ عجزم من و بی‌طاقتیهای محال</p></div>
<div class="m2"><p>اینقدر آتش دل بیمار زد در بسترم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صرفه‌ای آرام نتوان برد در تسخیر من</p></div>
<div class="m2"><p>خس به چشم دام می‌افتد ز صید لاغرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا به‌ کی بینم به چشم بسته داغ سوختن</p></div>
<div class="m2"><p>همچو اخگر کاش مژگان واکند خاکسترم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از خط لعل‌ که امشب سرمه خواهد یافتن</p></div>
<div class="m2"><p>می پرد بیدل به بال موج چشم ساغرم</p></div></div>