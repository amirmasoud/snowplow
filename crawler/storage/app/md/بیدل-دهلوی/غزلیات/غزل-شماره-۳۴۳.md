---
title: >-
    غزل شمارهٔ ۳۴۳
---
# غزل شمارهٔ ۳۴۳

<div class="b" id="bn1"><div class="m1"><p>می‌دهد دل را نفس آخر به سیل اضطراب</p></div>
<div class="m2"><p>خانهٔ آیینه‌ای داریم و می‌ گردد خراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در محیط عشق تا سر درگریبان برده‌ایم</p></div>
<div class="m2"><p>نیست چون‌گرداب رزق‌ما به‌غیرازپیچ وتاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کاش با اندیشهٔ هستی نمی‌پرداختیم</p></div>
<div class="m2"><p>خواب دیگر شد غبار بینش از تعبیر خواب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک گره‌وار از تعلق مانع وارستگی‌ست</p></div>
<div class="m2"><p>موج اینجا آبله درپاست از نقش حباب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسمل شوق‌گل‌اندامی‌ست سر تا پای من</p></div>
<div class="m2"><p>می‌توان‌چون‌گل‌گرفت‌از خندهٔ‌زخمم گلاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در محبت چهرهٔ زردی به دست آورده‌ام</p></div>
<div class="m2"><p>زین گلستان کرده‌ام برگ خزانی انتخاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیش روی اوکه آتش رنگ می‌بازد ز شرم</p></div>
<div class="m2"><p>آینه از ساده‌لوحی می‌زند نقشی بر آب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در تماشاگاه بوی‌گل نگه را بار نیست</p></div>
<div class="m2"><p>آب ده چشم هوس ای شبنم از سیر نقاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا به‌کی بیکار باشد جوهر شمشیر ناز</p></div>
<div class="m2"><p>گرچه می‌دانم نگاهت فتنه است ما مخواب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در دبستان تماشای جمالت هر سحر</p></div>
<div class="m2"><p>دارد از خط شعاعی مشق حیرت آفتاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شور حشر انگیخت‌دل از سعی‌خاکستر شدن</p></div>
<div class="m2"><p>سوخت‌چندانی‌که‌سر تا پا نمک شد این‌کباب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ناقصان را بیدل آسان نیست تعلیم‌کمال</p></div>
<div class="m2"><p>تا دمد یک دانه چندین آبرو ریزد سحاب</p></div></div>