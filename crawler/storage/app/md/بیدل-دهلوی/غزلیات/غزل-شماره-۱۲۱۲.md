---
title: >-
    غزل شمارهٔ ۱۲۱۲
---
# غزل شمارهٔ ۱۲۱۲

<div class="b" id="bn1"><div class="m1"><p>نگه در شبههٔ تحقیق من معذور می‌باشد</p></div>
<div class="m2"><p>سراب آیینه‌ام آیینهٔ من دور می‌باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من‌ و ساز دکان خودفروشی‌ها، چه‌حرف‌است این</p></div>
<div class="m2"><p>جنون این فضولی در سرمنصور می‌باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عذابی نیست ‌گر از خانه‌پردازی برون آیی</p></div>
<div class="m2"><p>جهانی از غم طاق و سرا درگور می‌باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه دارد آگهی غیر از قدح‌پیمایی حاجت</p></div>
<div class="m2"><p>به قدر چشم واکردن نگه مخمور می‌باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>معاش جاه بی‌عاجزکشی صورت نمی‌بندد</p></div>
<div class="m2"><p>برات رزق شاهان بر دهان مور می‌باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>علاج خارخار حرص ممکن نیست جز مردن</p></div>
<div class="m2"><p>کفن این زخمها را مرهم‌ کافور می باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حذر از گوشهٔ چشمی ‌کزین یاران طمع‌ داری</p></div>
<div class="m2"><p>نگاه اینجا چراغ خانهٔ زنبور می‌باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سراغ یک نگاه آشنا از کس نمی‌یابم</p></div>
<div class="m2"><p>جهان‌چون‌نرگسستان بی‌تو شهر کور می‌باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در آن وادی که من دارم جنون شعله‌پروازی</p></div>
<div class="m2"><p>اگر عنقاست محتاج پر عصفور می‌باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ترنگی نیست ‌کز شوقت نپیچد در دماغ من</p></div>
<div class="m2"><p>سر عشاق چینی خانهٔ فغفور می‌باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ندارد ساز این کهسار جز خاموشی آهنگی</p></div>
<div class="m2"><p>ز موسی پرس آوازی‌که شمع طور می‌باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خرابات یقین فرقی ندارد ظرف و مظروفش</p></div>
<div class="m2"><p>می و مینا همان یک دانهٔ انگور می‌باشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عبارت چیست غیر از اقتضای شوخی معنی</p></div>
<div class="m2"><p>پری تا نیست پیدا شیشه هم مستور می‌باشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سیاهی ریخت بر آیینهٔ ادراک ما بیدل</p></div>
<div class="m2"><p>چراغ محفل تحقیق را این نور می‌باشد</p></div></div>