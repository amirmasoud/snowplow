---
title: >-
    غزل شمارهٔ ۲۳۳۶
---
# غزل شمارهٔ ۲۳۳۶

<div class="b" id="bn1"><div class="m1"><p>یاران نه در چمن نه به‌باغی رسیده‌ایم</p></div>
<div class="m2"><p>بوی‌گلی به سیر دماغی رسیده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مفت تأمدم اگر وا رسد کسی</p></div>
<div class="m2"><p>از عالم برون ز سراغی رسیده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از سرگذشت عافیت شمع ما مپرس</p></div>
<div class="m2"><p>طی‌گشت شعله‌ها که به داغی رسیده‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پر دور نیست از نفس آثار سوختن</p></div>
<div class="m2"><p>پروانه‌ها به دور چراغی رسیده‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر بیخودان فسانهٔ عیش دگر مخوان</p></div>
<div class="m2"><p>رنگی شکسته‌ایم و به باغی رسیده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اقبال پرگشایی بخت سیاه داشت</p></div>
<div class="m2"><p>از سایهٔ هما به‌کلاغی رسیده‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از ما تلاش لغزش مستان غنیمت است</p></div>
<div class="m2"><p>اشکی به یک دو قطره ایاغی رسیده‌ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون سکته‌ای‌ که‌ گل‌کند از مصرع روان</p></div>
<div class="m2"><p>کم فرصت یقین به فراغی رسیده‌ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیدل درین‌ بهار ثمرهاست گلفشان</p></div>
<div class="m2"><p>ما هم به وهم خویش دماغی رسیده‌ایم</p></div></div>