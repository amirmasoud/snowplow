---
title: >-
    غزل شمارهٔ ۲۲۷
---
# غزل شمارهٔ ۲۲۷

<div class="b" id="bn1"><div class="m1"><p>طرح قیامتی ز جگر می‌کشیم ما</p></div>
<div class="m2"><p>نقاش ناله‌ایم و اثر می‌کشیم ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>توفان نفس نهنگ محیط تحیریم</p></div>
<div class="m2"><p>آفاق راچوآینه در می‌کشیم ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ظالم‌کند به صحبت ما دل زکین تهی</p></div>
<div class="m2"><p>از جیب سنگ نقد ش؟ر می‌کشیم ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین عرض جوهری‌که درآیینه دیده‌ایم</p></div>
<div class="m2"><p>خط بر جریده‌های؟ر می‌کشیم ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا حسن عافیت شود آیینه‌دار ما</p></div>
<div class="m2"><p>از داغ دل چوشعله سپرمی‌کشیم ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در وصل هم‌کنار خیالیم چاره نیست</p></div>
<div class="m2"><p>آیینه‌ایم و عکس به بر می‌کشیم ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اینجا جواب نامهٔ عاشق تغافل است</p></div>
<div class="m2"><p>بیهوده انتظار خبر می‌کشیم ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آیینه نقشبند طلسم خیال نیست</p></div>
<div class="m2"><p>تصویرخود به لوح دگرمی‌کشیم ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وحشت متاع قافلهٔ گرد فرصتیم</p></div>
<div class="m2"><p>محمل به دوش عمرشررمی‌کشیم ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا سجده برده‌ایم خم پیکر نیاز</p></div>
<div class="m2"><p>زین بار زندگی‌که به سر می‌کشیم ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این‌است اگرتصرف عرض شکست رنگ</p></div>
<div class="m2"><p>آیینهٔ خیال به زر می‌کشیم ما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خاک بنای ما به هواگرد می‌کند</p></div>
<div class="m2"><p>بیدل هنوزمنت‌پرمی‌کشیم ما</p></div></div>