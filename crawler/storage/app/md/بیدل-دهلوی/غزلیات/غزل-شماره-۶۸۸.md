---
title: >-
    غزل شمارهٔ ۶۸۸
---
# غزل شمارهٔ ۶۸۸

<div class="b" id="bn1"><div class="m1"><p>به‌دست و تیغ‌کسی خون من حنابسته‌ست</p></div>
<div class="m2"><p>به حیرتم‌که عجب تهمت بجا بسته‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز جیب ناز خطش سر برون نمی‌آرد</p></div>
<div class="m2"><p>ز بسکه عهد به خلوتگه حیا بسته‌ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زه قبای بتی غنچه‌کرد دلها را</p></div>
<div class="m2"><p>که حسنش ازرگ‌گل بند بر قبا بسته‌ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غبار من همه تن بال حسرت است اما</p></div>
<div class="m2"><p>ادب همان ره پرواز مدعا بسته‌ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به وادی طلبت نارسایی عجزیم</p></div>
<div class="m2"><p>که هرکه رفته زخود خویش را به‌ما بسته‌ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امیدهاست که جز سجده‌ام نفرماید</p></div>
<div class="m2"><p>کسی‌که خاصیت عجز برگیا بسته‌ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تن از بساط حریرم چه‌گونه بندد طرف</p></div>
<div class="m2"><p>که دل به سلسلهٔ نقش بوریا بسته‌ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگاه حسرتم و نیست تاب پروازم</p></div>
<div class="m2"><p>که حیرت از مژه‌ام بال بر قفابسته‌ست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گداخت حیرت نقاش رنگ تصویرم</p></div>
<div class="m2"><p>که‌نقش هستی من بی‌نفس چرا بسته‌ست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مگربه آتش دل التجا برم چوسپند</p></div>
<div class="m2"><p>که بی‌زبانم وکارم به ناله وابسته‌ست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو شمع تا به فنا هیچ‌جا نیاسایم</p></div>
<div class="m2"><p>مرا سری‌ست‌که احرام نقش پا بسته‌ست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مگر ز زلف تو دارد طریق بست وگشاد</p></div>
<div class="m2"><p>گه بیدل اینهمه‌مضمون دلگشا بسته‌ست</p></div></div>