---
title: >-
    غزل شمارهٔ ۱۶۷۹
---
# غزل شمارهٔ ۱۶۷۹

<div class="b" id="bn1"><div class="m1"><p>سیر گلزار که یارب در نظر دارد بهار</p></div>
<div class="m2"><p>از پر طاووس دامن بر کمر دارد بهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شبنم ما را به حیرت آب می‌باید شدن</p></div>
<div class="m2"><p>کز دل هر ذره توفانی دگر دارد بهار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رنگ دامن چیدن و بوی گل از خود رفتن‌ست</p></div>
<div class="m2"><p>هر کجا گل می‌کند برگ سفر دارد بهار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جلوه تا دیدی نهان شد رنگ تا دیدی شکست</p></div>
<div class="m2"><p>فرصت عرض تماشا اینقدر دارد بهار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>محرم نبض رم و آرام ما عشق است و بس</p></div>
<div class="m2"><p>از رگ ‌گل تا خط سنبل خبر دارد بهار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای خرد چون بوی گل دیگر سراغ ما مگیر</p></div>
<div class="m2"><p>درجنون سرداد ما را تا چه سر دارد بهار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیر این گلشن غنیمت دان که فرصت بیش نیست</p></div>
<div class="m2"><p>در طلسم خندهٔ‌ گل بال و پر دارد بهار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بوی‌گل عمریست‌ خون‌آلودهٔ‌ رنگست‌ و بس</p></div>
<div class="m2"><p>ناوکی از آه بلبل در جگر دارد بهار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لاله داغ و گل‌ گریبان‌چاک و بلبل نوحه‌گر</p></div>
<div class="m2"><p>غیر عبرت زین چمن دیگر چه بردارد بهار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زندگی می‌باید اسباب طرب معدوم نیست</p></div>
<div class="m2"><p>رنگ هر جا رفته باشد در نظر دارد بهار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زخم دل عمریست درگرد نفس خوابانده‌ام</p></div>
<div class="m2"><p>در گریبانی که من دارم سحر دارد بهار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کهنه درس فطرتیم ای آگهی سرمایگان</p></div>
<div class="m2"><p>چند روزی شد که ما را بی‌خبر دارد بهار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چند باید بود مغرور طراوت های وهم</p></div>
<div class="m2"><p>شبنمستان نیست بیدل چشم تر دارد بهار</p></div></div>