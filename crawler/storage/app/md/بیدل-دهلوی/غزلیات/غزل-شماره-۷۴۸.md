---
title: >-
    غزل شمارهٔ ۷۴۸
---
# غزل شمارهٔ ۷۴۸

<div class="b" id="bn1"><div class="m1"><p>چون سحر طومارچاک سینه‌ام واکردنی‌ست</p></div>
<div class="m2"><p>آرزو مستوریی داردکه رسواکردنی‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون حبابم داغ دارد حیرت تکلیف شوق</p></div>
<div class="m2"><p>دیده محروم نگاه و سیر دریاکردنی‌ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از نفس دزدیدن بوی‌گلم غافل مباش</p></div>
<div class="m2"><p>دامن پیچیده‌ای دارم که صحرا کردنی‌ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیستم بیهوده گرد چارسوی اعتبار</p></div>
<div class="m2"><p>مشت خاکی دارم و با باد سوداکردنی‌ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواهشی کو، تا توانم فال نومیدی زدن</p></div>
<div class="m2"><p>سوختن را نیز خاشاکی مهیاکردنی‌ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جیب نازی می‌درد صبح بهار جلوه‌ای</p></div>
<div class="m2"><p>مژده ای آیینه رنگ رفته پیداکردنی‌ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌کند خاکستری گرد از نقاب اخگرم</p></div>
<div class="m2"><p>قمریی در بیضه می‌نالدتماشاکردنی‌ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قید هستی برنتابد جوش استیلای عشق</p></div>
<div class="m2"><p>چون هواگرمی‌کند بند قبا واکردنی‌ست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کشتی موجی به توفان شکستن داده‌ایم</p></div>
<div class="m2"><p>تا نفس‌باقی‌ست دست عجز بالاکردنی‌ست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیکر خاکی ندارد چاره از عرض غبار</p></div>
<div class="m2"><p>نسخهٔ‌ما بسکه بی‌ربط است اجزاکردنی‌ست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عجز می‌ گوید به آواز حزین درگوش من</p></div>
<div class="m2"><p>کز پر وامانده سیر عافیتها کردنی‌ست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لطف معنی بیش ازین بیدل ندارد اعتبار</p></div>
<div class="m2"><p>از خیال نازکت بوی‌گل انشاکردنی‌ست</p></div></div>