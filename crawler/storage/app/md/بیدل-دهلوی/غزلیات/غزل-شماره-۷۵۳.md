---
title: >-
    غزل شمارهٔ ۷۵۳
---
# غزل شمارهٔ ۷۵۳

<div class="b" id="bn1"><div class="m1"><p>رنگ‌عجزم لیک با وضع‌ خموشم ‌کار نیست</p></div>
<div class="m2"><p>در شکست بال دارم ناله‌گر منقار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در تأمل بیشتر دارد روانی شعر من</p></div>
<div class="m2"><p>مصر عم از سکته جز شمشیر لنگردار نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عجزتجدید هوسها را نفس آیینه است</p></div>
<div class="m2"><p>یک‌ ورق عمری‌ست‌ می‌گردانم و تکرار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اختلاط‌ خودفروشان‌گر به‌این بیحاصلی‌ست</p></div>
<div class="m2"><p>خانهٔ آیینه را قفلی به از زنگار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازکمین عسیبجو آگاه باید دم زدن</p></div>
<div class="m2"><p>گوشهای حاضران جز در پس دیوار نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محوگشتن منتهای مقصد شوق رساست</p></div>
<div class="m2"><p>چون ‌نگه غیر از تحیر مُهر این‌ طومار نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بردباری طلینتم خاک تامل پیشه‌ام</p></div>
<div class="m2"><p>غیر هستی هر چه بر دوشم ببندی بار نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اشک چشم‌ گوهرم‌، برق چراغ حیرتم</p></div>
<div class="m2"><p>کوکبم یک غم اگر در خود تپد سیار نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غافل از سیرگداز دل نباید زیستن</p></div>
<div class="m2"><p>هست در خون گشتنت رنگی که در گلزار نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرکجا او جلوه‌ دارد عرض ‌هستی ‌مفت ماست</p></div>
<div class="m2"><p>عکس را آیینه می‌باید نفس در کار نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر به‌ این رنگ ‌است بیدل انفعال هستی‌ام</p></div>
<div class="m2"><p>سنگ را هم آب‌ گشتن آنقدر دشوار نیست</p></div></div>