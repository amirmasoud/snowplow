---
title: >-
    غزل شمارهٔ ۵۶۰
---
# غزل شمارهٔ ۵۶۰

<div class="b" id="bn1"><div class="m1"><p>تپیدن دل عشاق محوکسوت آه است</p></div>
<div class="m2"><p>به حال شورش دریا زبان موج‌ گواه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز برق حادثه آرام نیست معتبران را</p></div>
<div class="m2"><p>درتن قلمرو شطرنج‌ کشت بر سر شاه است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به حسن قامت رعنا مباد غره برآیی</p></div>
<div class="m2"><p>هزار سدره درین باغ پایمال گیاه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر اهل عجز حصار است پیچ و تاب حوادث</p></div>
<div class="m2"><p>چوگردبادکه تخت روان هر پرکاه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صفای دل نتوان خواست از محبت دنیا</p></div>
<div class="m2"><p>که در شمردن زر، دست زرشمار، سیاه است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به غیر ترک تماشا مخواه نشئهٔ راحت</p></div>
<div class="m2"><p>هجوم‌خواب به‌چشمت شکست‌رنگ نگاه است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قبول خاطر نیک و بد است وضع ملایم</p></div>
<div class="m2"><p>که آب را به‌ دل تیغ و چشم آینه راه است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به درد عشق قناعت ‌کن از تجمل امکان</p></div>
<div class="m2"><p>دل‌شکسته در این انجمن شکست‌کلاه است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مپرس از طلب نارسای سوخته‌جانان</p></div>
<div class="m2"><p>چو شمع منزل‌ما داغ‌و جاده‌، شعلهٔ‌آه است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به دل نهفته نماند خیال شوکت حسنی</p></div>
<div class="m2"><p>که در شکستن رنگ منش غبار سپاه است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز سیر گلشن دل پا مکش‌که داغ تمنا</p></div>
<div class="m2"><p>در انتطار به چندین امید چشم به راه است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به ‌هرطرف چه خیال ‌است سرکشیدن بیدل</p></div>
<div class="m2"><p>پر شکسته همان آشیان عجز پناه است</p></div></div>