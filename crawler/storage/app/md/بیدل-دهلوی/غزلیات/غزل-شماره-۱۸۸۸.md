---
title: >-
    غزل شمارهٔ ۱۸۸۸
---
# غزل شمارهٔ ۱۸۸۸

<div class="b" id="bn1"><div class="m1"><p>ای ز عکس نرگست آیینه جام مل به‌ کف</p></div>
<div class="m2"><p>شانه از زلف تو نبض یک چمن سنبل به‌ کف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا دم تیغت ‌کند گلچینی باغ هوس</p></div>
<div class="m2"><p>گردن خلقی‌ست چون شمع از سر خود گل به کف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون هوا سودایی فکر پریشان می‌شود</p></div>
<div class="m2"><p>هرکه دارد بوی مضمونی از آن ‌کاکل به ‌کف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بزم امکان را که و مه‌ گفتگو سرمایه‌اند</p></div>
<div class="m2"><p>جامها در سر ترنگ و شیشه‌ها قلقل به کف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غنچه واری رنگ جمعیت درین گلزار نیست</p></div>
<div class="m2"><p>از پریشانی‌ گل اینجا می‌دمد سنبل به‌ کف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قامت پیری نشاط رفته را خمیازه‌ایست</p></div>
<div class="m2"><p>چشم حیرانیست‌ گر سیلاب دارد پل به ‌کف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرم دارد اطلس و دیبا دماغ خواجه را</p></div>
<div class="m2"><p>از خری این پشت خر تا کی برآید جل به ‌کف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ریشهٔ آزادگی در خاک این‌ گلشن‌ کجاست</p></div>
<div class="m2"><p>سرو هم چون ‌گردن قمری است اینجا غل به‌ کف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حسن چون شد بی‌نقاب از فکر عاشق فارغ است</p></div>
<div class="m2"><p>گل همان در غنچگی دارد دل بلبل به‌ کف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>محو گشتن می‌کند دریا حباب و موج را</p></div>
<div class="m2"><p>جزو از خود رفته دارد دستگاه‌ کل به ‌کف</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فیض هستی عام شد چندانکه چون ابروی ناز</p></div>
<div class="m2"><p>در نظر می‌آیدم محراب جام مل به‌ کف</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از چمن تا انجمن بی‌تاب تسخیر دل است</p></div>
<div class="m2"><p>بوی ‌گل تا دود مجمر می‌دود کاکل به کف</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یاد رخسار تو سامان چراغان می‌کند</p></div>
<div class="m2"><p>هر سر مویم کنون خواهد دمیدن گل به کف</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیست بیدل در ادبگاه خموشی مشربان</p></div>
<div class="m2"><p>شیشه را جز سرنگون گردیدن از قلقل به کف</p></div></div>