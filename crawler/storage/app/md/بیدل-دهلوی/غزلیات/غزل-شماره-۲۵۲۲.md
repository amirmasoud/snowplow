---
title: >-
    غزل شمارهٔ ۲۵۲۲
---
# غزل شمارهٔ ۲۵۲۲

<div class="b" id="bn1"><div class="m1"><p>نیامد کوشش بیحاصل گردون به کار من</p></div>
<div class="m2"><p>مگر از خاک بردارد مرا سعی غبار من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهال ناله‌ام نشو و نمای طرفه‌ای دارم</p></div>
<div class="m2"><p>دل هرکس گدازی دید گردید آبیار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمی‌دانم چه برق افتاده در بنیاد ادراکم</p></div>
<div class="m2"><p>که داغ دل شرار کاغذی شد درکنار من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به وحشت نالهٔ آزادم از گردون چه غم دارد</p></div>
<div class="m2"><p>اسیر طوق قمری نیست سرو جویبار من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تحیر جوهری گل کرده‌ام نومید پیدایی</p></div>
<div class="m2"><p>مگر آیینه از تمثال خود گیرد عیار من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو اجزای تخیل نامشخص هیاتی دارم</p></div>
<div class="m2"><p>قلم در رنگ تصویری نزد صورت نگار من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بس بی‌انفعال دور باش عبرتم دارد</p></div>
<div class="m2"><p>نمی‌گرید عرق هم بر ندامتهای کار من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رهایی پر فشان و مفت جمعیت گرفتاری</p></div>
<div class="m2"><p>به فتراک نفس عمری‌ست می‌لرزد شکار من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نمی‌دانم هوس بهر چه می‌سوزد نفس یا رب</p></div>
<div class="m2"><p>تو داری عالم نازی که ممکن نیست نار من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز بس در یاد چشم او سراپا مستی‌ام بیدل</p></div>
<div class="m2"><p>قدح بالید اگر خمیازه‌ گل کرد از خمار من</p></div></div>