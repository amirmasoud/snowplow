---
title: >-
    غزل شمارهٔ ۱۲۷۳
---
# غزل شمارهٔ ۱۲۷۳

<div class="b" id="bn1"><div class="m1"><p>وحشتم گر یک تپش در دشت امکان بشکفد</p></div>
<div class="m2"><p>تا به دامان قیامت چین دامان بشکفد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اشک مژگان‌پرورم‌، از حسرتم غافل مباش</p></div>
<div class="m2"><p>ناله‌اندودست آن گل کز نیستان بشکفد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کو نسیم مژده وصلی که از پرواز شوق</p></div>
<div class="m2"><p>غنچهٔ دل در برم تا کوی جانان بشکفد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌توان با صد خیابان بهشتم طرح داد</p></div>
<div class="m2"><p>یک مژه چشمی که بر روی عزیزان بشکفد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا قیامت در کف خاکی که نقش پای اوست</p></div>
<div class="m2"><p>دل تپد، آیینه بالد،‌ گل دمد، جان بشکفد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هستی جاوید ریزد گل به دامان عدم</p></div>
<div class="m2"><p>یک تبسم‌وار اگر آن لعل خندان بشکفد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گل‌فروشان جنون را دستگاهی لازم است</p></div>
<div class="m2"><p>غنچهٔ این باغ ترسم بی‌گریبان بشکفد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناله‌ها از کلفت بی‌دردی دل آب شد</p></div>
<div class="m2"><p>یارب این گلشن به بخت عندلیبان بشکفد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست غیر از شرم حاجت ابر گلزار کرم</p></div>
<div class="m2"><p>می‌کند سایل عرق تا دست احسان بشکفد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر دل مایوس بیدل پشت دستی می‌گزم</p></div>
<div class="m2"><p>غنچهٔ این عقده کاش از سعی دندان بشکفد</p></div></div>