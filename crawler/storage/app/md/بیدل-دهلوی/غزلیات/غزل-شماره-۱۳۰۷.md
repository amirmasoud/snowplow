---
title: >-
    غزل شمارهٔ ۱۳۰۷
---
# غزل شمارهٔ ۱۳۰۷

<div class="b" id="bn1"><div class="m1"><p>نه غنچه سر به گریبان‌ کشیده می‌ماند</p></div>
<div class="m2"><p>ز سایه سرو هم اینجا خمیده می‌ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمین و زلزله‌،‌گردون و صد جنون گردش</p></div>
<div class="m2"><p>در این دو ورطه کسی آرمیده می‌ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بلبل و گل این باغ تا دهند سراغ</p></div>
<div class="m2"><p>پر شکسته و رنگ پریده می‌ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز یأس‌، شیشهٔ رشکی مگر زنیم به سنگ</p></div>
<div class="m2"><p>وگرنه صبح طرب نادمیده می‌ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیال نشتر مژگان کیست در گلشن</p></div>
<div class="m2"><p>که شاخ‌گل به رک خون‌کشیده می‌ماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دور زلف تو گیسوی مهوشان یکسر</p></div>
<div class="m2"><p>به نارسایی تاک بریده می‌ماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو گل به ذوق هوس هرزه‌خند نتوان بود</p></div>
<div class="m2"><p>شکفتگی به دهان دریده می‌ماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خیال کینه به دل گر همه سر مویی‌ست</p></div>
<div class="m2"><p>به صد قیامت خار خلیده می‌ماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طراوت من و مایی که مایه‌اش نفس است</p></div>
<div class="m2"><p>به خونی از رگ بسمل چکیده می‌ماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گداخت حیرتم از نارسایی اشکی</p></div>
<div class="m2"><p>که آب می‌شود و محو دیده می‌ماند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز بسکه رشتهٔ ساز نفس‌ گسیخته است</p></div>
<div class="m2"><p>نشاط دل به نوای رمیده می‌ماند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غنیمت است دمی چند مشق ناله کنیم</p></div>
<div class="m2"><p>قفس به صفحهٔ مسطر کشیده می‌ماند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به هرچه وانگری سربه دامن خاک است</p></div>
<div class="m2"><p>جهان به اشک ز مژگان چکیده می‌ماند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حیا نخواست خیالش به دل نقاب درد</p></div>
<div class="m2"><p>که داغ حسرت بیدل به دیده می‌ماند</p></div></div>