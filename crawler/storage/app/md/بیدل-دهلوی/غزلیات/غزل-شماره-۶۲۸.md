---
title: >-
    غزل شمارهٔ ۶۲۸
---
# غزل شمارهٔ ۶۲۸

<div class="b" id="bn1"><div class="m1"><p>برکمرتا بهله آن‌ترک نزاکت مست بست</p></div>
<div class="m2"><p>نازکی در خدمت موی میانش دست بست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگذر از امید آگاهی‌که در صحرای وهم</p></div>
<div class="m2"><p>چشم‌ماکردی‌که خواهد تا ابد ننشست بست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک بر سرگرد خلقی را غرور بام و در</p></div>
<div class="m2"><p>نقش پا بایست طاق این بنای پست بست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرزه فکر حرص مضمونهای چندین آبله</p></div>
<div class="m2"><p>تا به دامان قناعت پای ما نشکست بست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شمع خاموشیم دیگر ناز رعنایی‌کراست</p></div>
<div class="m2"><p>عهد ما با نقش پارنگی‌که ازرو جست بست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قطره‌واری تا ازین دریا کشی سر بر برکنار</p></div>
<div class="m2"><p>بایدت چون‌موج‌گوهر دل‌به‌چندین‌شست بست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی‌زیان از خجلت اظهار مطلب مرده‌ایم</p></div>
<div class="m2"><p>باید از خاکم لب زخمی‌که نتوان بست بست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یاد چشم او خرابات جنون دیگر است</p></div>
<div class="m2"><p>شیشه بشکن‌تا توانی نقش آن بدمست بست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هیچکس بیدل حریف طرف دامانش نشد</p></div>
<div class="m2"><p>شرم آن پای حنایی عالمی را دست بست</p></div></div>