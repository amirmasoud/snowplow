---
title: >-
    غزل شمارهٔ ۱۳۸۳
---
# غزل شمارهٔ ۱۳۸۳

<div class="b" id="bn1"><div class="m1"><p>به شوخی زد طرب غم آفریدند</p></div>
<div class="m2"><p>مکرر شد عسل سم آفریدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نثار نازی از اندیشه گل کرد</p></div>
<div class="m2"><p>دو عالم جان به یک دم آفریدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به زخم اضطراب بسمل ما</p></div>
<div class="m2"><p>ز خون رفته مرهم آفریدند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکست عافیت آهنگ گردید</p></div>
<div class="m2"><p>به هرجا ساز آدم آفریدند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهان جوش بهار بی‌نیازیست</p></div>
<div class="m2"><p>به یک صورت دو گل‌ کم آفریدند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به هرجا وحشت ما عرضه دادند</p></div>
<div class="m2"><p>شرار و برق بی‌رم آفریدند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گل این بوستان آفت بهار است</p></div>
<div class="m2"><p>شکست و رنگ توأم آفریدند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به تسکین دل مجروح بسمل</p></div>
<div class="m2"><p>پر افشانده مرهم آفریدند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به پیری‌ گریه‌ کن‌ کایینه‌ ی صبح</p></div>
<div class="m2"><p>برای عرض شبنم آفریدند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کریمان‌ خون شوید از خجلت جود</p></div>
<div class="m2"><p>که شهرت خاص حاتم آفریدند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون ماه نو خم وضع سجودم</p></div>
<div class="m2"><p>ز پیشانی مقدم آفریدند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه مخموری نه مستی چیست بیدل</p></div>
<div class="m2"><p>دماغت از چه عالم آفریدند</p></div></div>