---
title: >-
    غزل شمارهٔ ۳۷۳
---
# غزل شمارهٔ ۳۷۳

<div class="b" id="bn1"><div class="m1"><p>باز درگلشن ز خویشم می‌برد افسون آب</p></div>
<div class="m2"><p>در نظر طرز خرامی دارم از مضمون آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شورش امواج این دریا خروش بزم‌کیست</p></div>
<div class="m2"><p>نغمه‌ای تر می‌فشارد مغزم از قانون آب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برنمی‌دارد دورنگی طینت روشندلان</p></div>
<div class="m2"><p>در رگ‌موجش همان‌آب است رنگ خون آب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو شبنم اشک ما آیینهٔ آه است و بس</p></div>
<div class="m2"><p>بر هوا ختم است اینجا وحشت مجنون آب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد عرق شبنم طرازگلستان شرم یار</p></div>
<div class="m2"><p>این‌گهر بود انتخاب نسخهٔ موزون آب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آرزوگر تشنهٔ رفع غبار حسرت است</p></div>
<div class="m2"><p>با وجود تیغ او نتوان شدن ممنون آب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست سیر عالم نیرنگ جای دم زدن</p></div>
<div class="m2"><p>عشق دریاهای آتش دارد و هامون آب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>معنی آسودگی نفس طلسم خامشی‌ست</p></div>
<div class="m2"><p>برمن ازموج‌گهرشد روشن این مضمون آب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طبعم از آشفتگی دام صفای دیگر است</p></div>
<div class="m2"><p>درخور امواج باشد حسن روزافزون آب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قلزم امکان نم موج سرابی هم نداشت</p></div>
<div class="m2"><p>تشنگیهاکرد ما را اینقدر مفتون آب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وحدت‌از خودداری‌ما تهمت‌آلود دویی‌ست</p></div>
<div class="m2"><p>عکس در آب است تا استاده‌ای بیرون آب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صاف‌طبعانند بیدل بسمل شوق بهار</p></div>
<div class="m2"><p>جادهٔ رگهای گل دارد سراغ خون آب</p></div></div>