---
title: >-
    غزل شمارهٔ ۲۱۱
---
# غزل شمارهٔ ۲۱۱

<div class="b" id="bn1"><div class="m1"><p>سلسلهٔ شوق‌کیست سر خط آهنگ ما</p></div>
<div class="m2"><p>رشته به پا می‌پرد از رگ گل رنگ ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقد جهان فسوس سهل نباید شمرد</p></div>
<div class="m2"><p>دل به‌گره بسته است آبله در چنگ ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با همه افسردگی جوش شرار دلیم</p></div>
<div class="m2"><p>خفته پریخانه‌ای در بغل سنگ ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درتپش آباد دل قطع نفس می‌کنیم</p></div>
<div class="m2"><p>نیست ز منزل برون جاده و فرسنگ ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پردهٔ سازنفس سخت‌خموشی نواست</p></div>
<div class="m2"><p>رشته مگر بگسلد تا دهد آهنگ ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در قفس عافیت هرزه فسردیم حیف</p></div>
<div class="m2"><p>شور شکستی نزدگل به سر رنگ ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سعی‌گوهر برگرفت بار دل از دوش موج</p></div>
<div class="m2"><p>آبله چشمی ندوخت بر قدم لنگ ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عالم بی‌مطلبی عرصهٔ پرخاش کیست</p></div>
<div class="m2"><p>نیست روان خون زخم جزعرق ازجنگ ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رشتهٔ چندین امل یک‌گره آمد به‌عرض</p></div>
<div class="m2"><p>بر دو جهان مهر زد یأس دل تنگ ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیدل از اقبال عجز درهمه جا چیده است</p></div>
<div class="m2"><p>آبله و نقش پا افسر واورنگ ما</p></div></div>