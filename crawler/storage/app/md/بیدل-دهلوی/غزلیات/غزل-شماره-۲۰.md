---
title: >-
    غزل شمارهٔ ۲۰
---
# غزل شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>به مهر مادرگیتی مکش رنج امید اینجا</p></div>
<div class="m2"><p>که خونها می‌خورد تا شیر می‌گردد سفید اینجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p> مقیم نارسایی باش پیش از خاک گردیدن</p></div>
<div class="m2"><p>که‌سعی هردوعالم چون عرق خواهد چکید اینجا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p> محیط از جنبش هر قطره‌صد توفان جنون دارد</p></div>
<div class="m2"><p>شکست رنگ امکان بود اگر یکدل تپید اینجا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p> گداز نیستی از انتظارم برنمی‌آرد</p></div>
<div class="m2"><p>ز خاکستر شدن‌گل می‌کند چشم سفید اینجا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p> ز ساز الفت آهنگ عدم در پردة‌گوشم</p></div>
<div class="m2"><p>نوایی می‌رسدکز بیخودی نتوان شنید اینجا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p> درین محنت‌سرا آیینهٔ اشک یتیمانم</p></div>
<div class="m2"><p>که در بی‌دست و پایی هم مرا باید دوید اینجا </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کباب خام سوز آتش حسرت دلی دارم</p></div>
<div class="m2"><p>که هرجا بینوایی سوخت دودش سرکشید اینجا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p> نیاز سرکشان حسن آشوبی دگر دارد</p></div>
<div class="m2"><p>کمینگاه تغافل شد اگر ابرو خمید اینجا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p> تپشهای نفس ز پردة تحقق می‌گوید</p></div>
<div class="m2"><p>که تا از خود اثر داری نخواهی آرمید اینجا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بلندست آنقدرها آشیان عجز ما بیدل</p></div>
<div class="m2"><p> که بی‌سعی شکست بال و پر نتوان رسید اینجا</p></div></div>