---
title: >-
    غزل شمارهٔ ۱۰۶۷
---
# غزل شمارهٔ ۱۰۶۷

<div class="b" id="bn1"><div class="m1"><p>ما را به در دل ادب هیچکسی برد</p></div>
<div class="m2"><p>تمثال در آیینه‌، ره از بی‌نفسی برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین دشت هوس منت سیلی نکشیدیم</p></div>
<div class="m2"><p>خاروخس ما را عرق شرم خسی برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیگانهٔ عشقیم ز شغل هوسی چند</p></div>
<div class="m2"><p>آب رخ عنقایی ما را مگسی برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فریادکه محمل‌کش یک ناله نگشتیم</p></div>
<div class="m2"><p>دل خون شد ودر خاک غبار جرسی برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دور همه چون سبحه یکی‌کرد تسلسل</p></div>
<div class="m2"><p>زین قافله‌ها پیش وپسی‌ ، پیش وپسی برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آخر پی تحقیق به جایی نرساندم</p></div>
<div class="m2"><p>بیرونم از این دشت اقامت هوسی برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل نیز نشد چون نفسم دام تسلی</p></div>
<div class="m2"><p>جمعیت بالم الم بی‌قفسی برد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیدل ثمر باغ‌کمالم چه توان‌کرد</p></div>
<div class="m2"><p>پیش از همه در خاک مرا پیش رسی‌برد</p></div></div>