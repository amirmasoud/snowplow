---
title: >-
    غزل شمارهٔ ۱۷۰۱
---
# غزل شمارهٔ ۱۷۰۱

<div class="b" id="bn1"><div class="m1"><p>این بحر را یک آینه دشت سراب‌ گیر</p></div>
<div class="m2"><p>گر تشنه‌ای چو آبله از خویش آب ‌گیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنیاد چشم در گذر سیل نیستی‌ست</p></div>
<div class="m2"><p>خواهی عمارتش کن و خواهی خراب‌ گیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر زندگی همین نظری بازکردن‌ست</p></div>
<div class="m2"><p>رو بر در عدم زن و چشمی به خواب‌ گیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این استقامتی ‌که تو بر خویش چیده‌ای</p></div>
<div class="m2"><p>چون اشک بر سر مژه پا در رکاب‌ گیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گلچینی خیال به امید واگذار</p></div>
<div class="m2"><p>چون یأس از گداز دو عالم گلاب‌ گیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ممنون چرخ سفله شدن سخت خجلت است</p></div>
<div class="m2"><p>تا از اثر تهی‌ست دعا مستجاب‌گیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کیفیتی به نشئهٔ عرفان نمی‌رسد</p></div>
<div class="m2"><p>چشمی به خویش واکن و جام شراب ‌گیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در خاک هم ز معنی خود بی‌خبر مباش</p></div>
<div class="m2"><p>از هر نشان پا نقط انتخاب‌ گیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیلاب خوش عمارت ویرانه می‌کند</p></div>
<div class="m2"><p>ای چشم تر تو هم گل ما را در آب گیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جز چاک دل، نشیمن عنقای عشق نیست</p></div>
<div class="m2"><p>چون صبح سازکن قفس و آفتاب‌گیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عالم تمام‌، خانهٔ زین اعتبار کن</p></div>
<div class="m2"><p>یعنی قدم به هرچه‌گذاری رکاب‌گیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خاموشیت نظر به یقین بازکردن‌ست</p></div>
<div class="m2"><p>آیینه‌ای به ضبط نفس چون حباب‌گیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قاصد، سوادنامهٔ عشاق نیستی‌ست</p></div>
<div class="m2"><p>بردار مشت خاک ز راه و جواب‌گیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بی دردی از خیانت اعمال رنگ کیست</p></div>
<div class="m2"><p>از هر نفس‌که ناله ندارد حساب‌گیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از نسیه فیض نقد نبرده‌ست هیچکس</p></div>
<div class="m2"><p>بیدل تو می خور و دل زاهد کباب‌ گیر</p></div></div>