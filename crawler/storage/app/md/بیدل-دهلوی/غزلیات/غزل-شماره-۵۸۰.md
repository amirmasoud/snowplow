---
title: >-
    غزل شمارهٔ ۵۸۰
---
# غزل شمارهٔ ۵۸۰

<div class="b" id="bn1"><div class="m1"><p>در بهارگریه عیش بیدلان آماده است</p></div>
<div class="m2"><p>اشک تاگل می‌کند هم شیشه و هم باده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طینت عاشق همین وحشت غبار ناله نیست</p></div>
<div class="m2"><p>چون شرارکاغذ اینجا داغ هم آزاده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیچکس واقف نشد از ختم‌کار رفتگان</p></div>
<div class="m2"><p>در پی این‌کاروان هم آتشی افتاده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پردهٔ ناموس هستی اعتباری بیش نیست</p></div>
<div class="m2"><p>م ما را شیشه‌ای‌گر هست رنگ‌باده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منزل خاصی نمی‌خواهد عبادتگاه شوق</p></div>
<div class="m2"><p>هرکف خاکی‌که آنجا سر نهی سجاده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زاهد ز رشک شرار شوق ما تردامنان</p></div>
<div class="m2"><p>همچو خارخشک بهر سوختن آماده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقل‌کو تا جمع سازد خاطر از اجزای ما</p></div>
<div class="m2"><p>عشق مشت خاک ما را سر به صحرا داده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خار راه اهل بینش جلوهٔ اسباب نیست</p></div>
<div class="m2"><p>ازکمند الفت مژگان نگه آزاده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زنهار! ایمن مباش ز شک دردآلود من</p></div>
<div class="m2"><p>گرهمه یک‌شبنم است‌این طفل توفان‌زاده است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا فنا در هیچ جا آر‌ام نتوان یافتن</p></div>
<div class="m2"><p>هرچه‌جزمنزل درین‌وادی‌ست یکسرجاده‌است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گوهر ماکاش از ننگ فسردن خون شود</p></div>
<div class="m2"><p>می‌رود دریا ز خویش و موج ما استاده است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل به نادانی مده بیدل‌که در ملک یقین</p></div>
<div class="m2"><p>تختهٔ مشق خیال است آینه تاساده است</p></div></div>