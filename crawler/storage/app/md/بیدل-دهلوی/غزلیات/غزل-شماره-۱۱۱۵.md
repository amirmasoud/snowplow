---
title: >-
    غزل شمارهٔ ۱۱۱۵
---
# غزل شمارهٔ ۱۱۱۵

<div class="b" id="bn1"><div class="m1"><p>گر شوق پی مطلب نایاب نگیرد</p></div>
<div class="m2"><p>سرمشق رم از عالم اسباب نگیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با تشنه ‌لبی ساز و مخور آبی از این بحر</p></div>
<div class="m2"><p>تا حلق تو را تنگ چو گرداب نگیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن دل ‌که تپیدن فکند قرعهٔ وصلش</p></div>
<div class="m2"><p>حیف است‌ که آیینه به سیماب نگیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محتاج کریمان نشود مفلس قانع</p></div>
<div class="m2"><p>سرچشمهٔ آیینه زبحرآب نگیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صیّاد اسیران محبّت خم ابروست</p></div>
<div class="m2"><p>کس ماهی این بحر به قلاب نگیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از نور هدایت نبرد بهره سیه‌بخت</p></div>
<div class="m2"><p>چون سایه‌ که رنگ از گل مهتاب نگیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل مست جنون است بگویید خرد را</p></div>
<div class="m2"><p>امروز سراغ من بیتاب نگیرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از بس به مراد دو جهان دست فشاندم</p></div>
<div class="m2"><p>گر زلف شوم دامن من تاب نگیرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منظور حیا ضبط نگاهیست و گر نه</p></div>
<div class="m2"><p>سر پنجهٔ مژگان بتان خواب نگیرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در حلقهٔ خامش ‌نفسان در دل باش</p></div>
<div class="m2"><p>تا هیچکست نکته در این باب نگیرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هنیاد تو تا چند شود سدّ ره عمر</p></div>
<div class="m2"><p>بیدل‌ کف خاکی ره سیلاب نگیرد</p></div></div>