---
title: >-
    غزل شمارهٔ ۴۴۹
---
# غزل شمارهٔ ۴۴۹

<div class="b" id="bn1"><div class="m1"><p>چشم خرد آیینهٔ جام می ناب است</p></div>
<div class="m2"><p>ابروی سخن در شکن موج شراب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آگاهی دل می‌طلبی ترک هنرگیر</p></div>
<div class="m2"><p>کز جوهر خود بر رخ آیینه نقاب است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیتاب فنا آن همه‌کوشش نپسندد</p></div>
<div class="m2"><p>شبگیرشررها همه یک لحظه شتاب است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عارف به خدا می‌رسد ازگردش چشمی</p></div>
<div class="m2"><p>در نیم نفس بحر هماغوش حباب است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کیفیت توفانکدهٔ‌گریه مپرسید</p></div>
<div class="m2"><p>در هر نم اشکم دو جهان عالم آب است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این بحرگداز جگر سوخته دارد</p></div>
<div class="m2"><p>آبی‌که تو داری به نظر اشک‌کباب است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون سیهی دولت به‌کسی نیست مسلم</p></div>
<div class="m2"><p>پیداست‌که هر نقش نگین نقش برآب است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوش باش‌که در میکدهٔ نشئهٔ تحقیق</p></div>
<div class="m2"><p>مینایی اگر هست همان رنگ شراب است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی‌جنبش دل راه به جایی نتوان برد</p></div>
<div class="m2"><p>یکسر جرس قافلهٔ موج حباب است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در محفل قانون نواسنجی عشاق</p></div>
<div class="m2"><p>گوشی‌که ادا فهم نشدگوش رباب است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا سرمه نگشتیم به چشمش نرسیدیم</p></div>
<div class="m2"><p>در بزم خموشان نفس سوخته باب است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل چیست‌که با خاک برابر نتوان‌کرد</p></div>
<div class="m2"><p>بی‌روی تو تا خانهٔ آیینه خراب است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دانش همه غفلت شود از عجز رسایی</p></div>
<div class="m2"><p>چون تار نظرکوتهی آرد رگ خواب‌است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیدل اگر افسرده دلی جمع کتب کرد</p></div>
<div class="m2"><p>در مدرسهٔ دانش ما جلد کتاب است</p></div></div>