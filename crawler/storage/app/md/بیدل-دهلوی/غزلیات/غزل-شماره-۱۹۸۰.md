---
title: >-
    غزل شمارهٔ ۱۹۸۰
---
# غزل شمارهٔ ۱۹۸۰

<div class="b" id="bn1"><div class="m1"><p>سینه چاک یک جهان گرد هوس بالیده‌ام</p></div>
<div class="m2"><p>صبح آزادی چه حرف است این قفس بالیده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طمطراق گفتگوی بی‌اثر فهمیدنی‌ست</p></div>
<div class="m2"><p>کاروانی چند آواز جرس بالیده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>انفعال همتم‌، ننگ جهان فطرتم</p></div>
<div class="m2"><p>آرزویی در دماغ بوالهوس بالیده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خرابات ظهورم نام هستی تهمتی‌ست</p></div>
<div class="m2"><p>چون حباب جرم مینا بی‌نفس بالیده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوختن هم مفت فرصت بود اما مایه‌کو</p></div>
<div class="m2"><p>پهلوی خشکی به قدر یک دو خس بالیده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر غباری در هوای دامنی پر می‌زند</p></div>
<div class="m2"><p>من هم ای حسرت‌کشان زین دسترس بالیده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناله‌ام اما نمی‌گنجم درین نه انجمن</p></div>
<div class="m2"><p>یارب این مقدار در یاد چه کس بالیده‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غیر دریا چیست افسون مایهٔ ناز حباب</p></div>
<div class="m2"><p>می‌درم پیراهنت بر خود ز بس بالیده‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیدل از ساز ضعیفیهای من غافل مباش</p></div>
<div class="m2"><p>صور می‌خندد طنینی کز مگس بالیده‌ام</p></div></div>