---
title: >-
    غزل شمارهٔ ۱۲۱۰
---
# غزل شمارهٔ ۱۲۱۰

<div class="b" id="bn1"><div class="m1"><p>نیام تیغ عالمگیر مستی موج می باشد</p></div>
<div class="m2"><p>خدنگ دلنشین نغمه را قندیل نی باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دل غیر از خیال جلوه‌ات نقشی نمی‌یابم</p></div>
<div class="m2"><p>به جز حیرت‌کسی در خانهٔ آیینه‌ کی باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز باغ عافیت رنگ امیدی نیست عاشق را</p></div>
<div class="m2"><p>محبت غیر خون گشتن نمی‌دانم چه شی باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز الفت چشم نگشایی به رنگ و بوی این گلشن</p></div>
<div class="m2"><p>که می‌ترسم نگاه عبرت‌آلودی ز پی باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گذشتن برنتابد از سر این خاکدان همت</p></div>
<div class="m2"><p>که ننگ پاست طی کردن بساطی را که طی باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به بادی هم نمی‌سنجم نوای عیش امکان را</p></div>
<div class="m2"><p>به گوشم تا شکست استخوان آواز نی باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ندارد از حوادث توسن فرصت عنان‌داری</p></div>
<div class="m2"><p>نواهای شکست خویش بر امواج هی باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>توان از یک تغافل صد دهان هرزه‌گو بستن</p></div>
<div class="m2"><p>چه لازم رغبت طبعت به طشت‌ پر ز قی باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جنون‌جوش است امشب مجلس‌کیفیت مستان</p></div>
<div class="m2"><p>مبادا چشم مستی در قفای جام می باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز شور عجز، ما گردنکشان را لرزه می‌گیرد</p></div>
<div class="m2"><p>هجوم خاروخس بر روی آتش فصل دی باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قفس‌فرسوده این تنگنایم ای هوس خون شو</p></div>
<div class="m2"><p>که می‌داند زمان رخصت پرواز کی باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیابی جز امل شیرازهٔ سختی‌کشان بیدل</p></div>
<div class="m2"><p>مدار ستخوان در بندبند خلق پی باشد</p></div></div>