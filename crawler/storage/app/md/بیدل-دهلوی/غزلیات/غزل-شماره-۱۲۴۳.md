---
title: >-
    غزل شمارهٔ ۱۲۴۳
---
# غزل شمارهٔ ۱۲۴۳

<div class="b" id="bn1"><div class="m1"><p>بار ما عمری‌ست دوش چشم حیران می‌کشد</p></div>
<div class="m2"><p>محمل‌اجزای ما چون شمع مژگان می‌کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناتوانان مغتنم دارید وضع عاجزی</p></div>
<div class="m2"><p>کزغرورطاقت آسودن به جولان می‌کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما ضعیفان آنقدرها زحمت یاران نه‌ا‌یم</p></div>
<div class="m2"><p>سایه باری دارد اما هرکس آسان می‌کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچکس ‌در مزرع امکان قناعت‌پیشه نیست</p></div>
<div class="m2"><p>گر همه گندم بود خمیازهٔ نان می‌کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صلح و جنگ‌ عرصهٔ ‌غفلت تماشاکردنی‌ست</p></div>
<div class="m2"><p>تیر در کیش‌ است و خلق‌ از سینه‌ پیکان‌ می‌کشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوری انس است استعداد لذتهای خلق</p></div>
<div class="m2"><p>طفل می‌برد ز شیر آن‌دم‌که دندان می‌کشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>التفات رنگ امکان یکقلم آلودگی‌ست</p></div>
<div class="m2"><p>مفت نقاشی‌کزین تصویر دامان می‌کشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وحشت‌ آهنگی ز فکر خویش بیرون آ، که شمع‌</p></div>
<div class="m2"><p>پا ز دامن تاکشد سر از گریبان می‌کشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>محو او را هر سر مو یک جهان بالیدن است</p></div>
<div class="m2"><p>گاه حیرت داغم از قدی که مژگان می‌کشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می‌روم از خویش و جز حیرت دلیل‌جهد نیست</p></div>
<div class="m2"><p>وحشتم در خانه ی آیینه میدان می کشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جسم‌گرشد خاک بیدل رفع اوهام دویی‌ست</p></div>
<div class="m2"><p>شخص از آیینه‌گم‌کردن چه نقصان می‌کشد</p></div></div>