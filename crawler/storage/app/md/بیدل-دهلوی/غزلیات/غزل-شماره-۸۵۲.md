---
title: >-
    غزل شمارهٔ ۸۵۲
---
# غزل شمارهٔ ۸۵۲

<div class="b" id="bn1"><div class="m1"><p>ز آتش رخسار که ساغر گرفت</p></div>
<div class="m2"><p>خانهٔ آیینه چو من درگرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کو پر و بالی‌که به آن‌کو رسد</p></div>
<div class="m2"><p>نامه گرفتم که کبوتر گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق‌، وفا می‌طلبد، چاره چیست</p></div>
<div class="m2"><p>بار دل از دل نتوان برگرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نی چقدر رغبت طفلانه داشت</p></div>
<div class="m2"><p>بال و پر ناله به شکر گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناله نخیزد ز نی بورپا</p></div>
<div class="m2"><p>طاقت ما پهلوی لاغر گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بحربه توفان رضا می‌تپید</p></div>
<div class="m2"><p>کشتی ما هم ‌کم لنگر گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چاره به خورشید قیامت کشید</p></div>
<div class="m2"><p>دامن ما خشک شدن‌، تر گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما همه زین باغ برون رفته‌ایم</p></div>
<div class="m2"><p>رنگ که پرواز ته پر گرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیدل از اعجاز ضعیفی مپرس</p></div>
<div class="m2"><p>لغزش من خامه به مسطر گرفت</p></div></div>