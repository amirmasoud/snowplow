---
title: >-
    غزل شمارهٔ ۵۵۵
---
# غزل شمارهٔ ۵۵۵

<div class="b" id="bn1"><div class="m1"><p>نیک و بد این مرحله خاکش به ‌کمین است</p></div>
<div class="m2"><p>چشمی‌ که به پا دوخته باشی همه بین است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌ غنچه ‌گلی سر نزد از گلشن امکان</p></div>
<div class="m2"><p>اینجاست که چین مایهٔ ایجاد جبین است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برخیز ز خاک سیه مزرع هستی</p></div>
<div class="m2"><p>جایی‌ که نفس آینه‌ کارد چه زمین است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون صبح جنونی‌ کن و از خو برون تاز</p></div>
<div class="m2"><p>از چاک گریبان گل دامان تو چین است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر صور مناز از دهل و کوس تجمل</p></div>
<div class="m2"><p>ای پشه بم و زیرکمال تو طنین است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این است اگر کر و فر طاق و سرایت</p></div>
<div class="m2"><p>بنیاد غبار به هوا رفته متین است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای آینه از ما مطلب عرض مکرر</p></div>
<div class="m2"><p>تمثال ضعیفان نفس باز پسین است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای شمع عنان نگه هرزه نگهدار</p></div>
<div class="m2"><p>تا چشم تو باز است جهان خانهٔ زین است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زان جلوه‌گذشتیم و به خود هم نرسیدیم</p></div>
<div class="m2"><p>ما را چه‌ گنه خاصیت عجز همین است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل نیز گره شد به خم ابروی نازش</p></div>
<div class="m2"><p>در طاق تغافل همه نقاشی چین است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در وصل به اظهار مکش ننگ فضولی</p></div>
<div class="m2"><p>با بوسه حضور لب خاموش قرین است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رندان مشکیبید ز معشوقهٔ فربه</p></div>
<div class="m2"><p>کاین شکل دلاوبز سراپاش سرین است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شور تپش از ما به فنا هم نتوان برد</p></div>
<div class="m2"><p>خاکستر منصور مزاجان نمکین است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیدل ‌کم سرمایهٔ عزلت نپسندی</p></div>
<div class="m2"><p>از پای به دامان تو نامت به نگین است</p></div></div>