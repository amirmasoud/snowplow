---
title: >-
    غزل شمارهٔ ۱۱۱۸
---
# غزل شمارهٔ ۱۱۱۸

<div class="b" id="bn1"><div class="m1"><p>تدبیر عنان من پر شور نگیرد</p></div>
<div class="m2"><p>هر پنبه سر شیشهٔ منصور نگیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارد ز سر و برگ غنا دامن فقرم</p></div>
<div class="m2"><p>چینی‌ که به مویی سر فغفور نگیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خلق خجالت‌کش تحصیل‌کمالم</p></div>
<div class="m2"><p>برخرمن من خرده مگر مور نگیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با من چو کلف بخت سیاهی‌ست‌ که صدسال</p></div>
<div class="m2"><p>در ماهش اگر غوطه دهم نور نگیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نزدیکتر آیید سرابم نه محیطم</p></div>
<div class="m2"><p>معیار کمالم کسی ‌‌از دور نگیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محرومی شوق ارنی سخت عذابی‌ست</p></div>
<div class="m2"><p>جهدی‌که خروش تو ره طور نگیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عریانی از اسباب جهان مغتنم انگار</p></div>
<div class="m2"><p>تا بند گریبان تو هر گور نگیرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قطع امل الفت دل عقد محال است</p></div>
<div class="m2"><p>چندان ببر این تاک ‌که انگور نگیرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای مرده دل آرایش مرقد چه تمناست</p></div>
<div class="m2"><p>نام تو همان به‌ که لب ‌گور نگیرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر منتظر وصل مفرما مژه بستن</p></div>
<div class="m2"><p>انصاف‌، قدح از کف مخمور نگیرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل هدف ناوک آفات بزرگی‌ست</p></div>
<div class="m2"><p>مه تا به ‌کمالش نرسد نور نگیرد</p></div></div>