---
title: >-
    غزل شمارهٔ ۲۲۶۶
---
# غزل شمارهٔ ۲۲۶۶

<div class="b" id="bn1"><div class="m1"><p>باز دل مست نوایی‌ست که من می‌دانم</p></div>
<div class="m2"><p>این نوا نیز ز جایی‌ست‌که من می‌دانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محمل و قافله و ناقه درین وحشتگاه</p></div>
<div class="m2"><p>گردی از بانگ درایی‌ست‌که من می‌دانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خونم آخر به‌ کف پای‌ کسی خواهد ریخت</p></div>
<div class="m2"><p>این همان رنگ حنایی‌ست‌که من می‌دانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم واکردم و توفان قیامت دیدم</p></div>
<div class="m2"><p>زندگی روز جزایی‌ ست ‌که من می‌دانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آب‌گردیدن و موجی ز تمنا نزدن</p></div>
<div class="m2"><p>پاس ناموس حیایی‌ست که من می‌دانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست راهی که به‌کاهل‌قدمی‌طی نشود</p></div>
<div class="m2"><p>پای خوابیده عصایی‌ست که من می‌دانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در مقامی که بجایی نرسد کوششها</p></div>
<div class="m2"><p>ناله اقبال رسایی‌ست که من می‌دانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ساز تحقیق ندارد چه نگاه و چه نفس</p></div>
<div class="m2"><p>سر این‌رشته بجایی‌ست که من می‌دانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طلبت یأس تپیدن هوس عشق وفاست</p></div>
<div class="m2"><p>کار دل نام بلایی‌ست که من می‌دانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای غنا شیفته با ‌این دل راحت محتاج</p></div>
<div class="m2"><p>فخر مفروش گدایی‌ست که من می‌دانم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عشق زد شمع‌ که ای سوختگان خوش باشید</p></div>
<div class="m2"><p>شعله هم آب بقایی‌ست که من می‌دانم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حیرتم سوخت ‌که از دفتر عنقایی او</p></div>
<div class="m2"><p>جهل هم نسخه‌ نمایی‌ست‌ که من می‌دانم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بود عمری به برم دلبر نگشوده نقاب</p></div>
<div class="m2"><p>بیدل این نیز ادایی‌ست که من می‌دانم</p></div></div>