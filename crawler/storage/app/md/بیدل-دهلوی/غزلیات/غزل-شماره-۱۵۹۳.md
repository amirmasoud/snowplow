---
title: >-
    غزل شمارهٔ ۱۵۹۳
---
# غزل شمارهٔ ۱۵۹۳

<div class="b" id="bn1"><div class="m1"><p>نفس تا پرفشان است از تو و من برنمی‌آید</p></div>
<div class="m2"><p>کسی زین خجلت در آتش‌افکن برنمی‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زبانم را حیا چون موج‌گوهر لال‌کرد آخر</p></div>
<div class="m2"><p>ز زنجیری‌که درآب است شیون برنمی‌آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حضور دل طمع داری ز تعمیر جسد بگذر</p></div>
<div class="m2"><p>که‌گوهر از صدفها بی‌شکستن برنمی‌آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گدازی از نفس‌گیر انتخاب نسخهٔ هستی</p></div>
<div class="m2"><p>که جز شبنم ز شیر صبح روغن برنمی‌آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غرور خودسریها ابجد نشو و نما باشد</p></div>
<div class="m2"><p>ز تخم اول به جز رگهای‌گردن برنمی‌آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ریاضت تاکجا بار درشتی بندد از طبعت</p></div>
<div class="m2"><p>به صیقل آینه ازننگ آهن برنمی‌آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به رفع تهمت غفلت ‌گداز درد سامان ‌کن</p></div>
<div class="m2"><p>که دل تا خون‌نگردد از فسردن برنمی‌آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هواپروردهٔ شوق بهارستان دیدارم</p></div>
<div class="m2"><p>به‌گلخن هم نگاه من زگلشن برنمی‌آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به عریانی چو گردن بایدم ناچار سرکردن</p></div>
<div class="m2"><p>به این رازی‌که من دارم نهفتن برنمی‌آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بساط مهر باید سایه را از دور بوسیدن</p></div>
<div class="m2"><p>به برق جلوهٔ او هستی من برنمی‌آید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ادب فرسوده‌تر از اشک مژگان‌پرورم بیدل</p></div>
<div class="m2"><p>من و پایی که تا کویش ز دامن برنمی‌آید</p></div></div>