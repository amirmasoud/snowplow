---
title: >-
    غزل شمارهٔ ۲۰۴۶
---
# غزل شمارهٔ ۲۰۴۶

<div class="b" id="bn1"><div class="m1"><p>نیست در میدان عبرت باکی از نیک و بدم</p></div>
<div class="m2"><p>صاحب خفتان شرمم عیب‌پوشی چلقدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منفعل نشو و نمای سر به جیبم داده‌اند</p></div>
<div class="m2"><p>رستن مو می‌کشد نقاش تصویر قدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرچه پیش ‌آید غنیمت مفت سعی ‌بیکسی است</p></div>
<div class="m2"><p>آدمم اما هلاک صحبت دام و ددم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صد امل گر تازد آنسوی قیامت گرد من</p></div>
<div class="m2"><p>انفعالم نیست‌، بیکار جهان سرمدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشرت این انجمن پر انفعال آماده بود</p></div>
<div class="m2"><p>فرصت مستی عرقها کرد تا ساغر زدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تنگی میدان هوشم‌ کرد محکوم جهات</p></div>
<div class="m2"><p>زندگی در بیخودی‌ گر جمع‌ کردم بیحدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رنگ و بوها جمع دارد میزبان نوبهار</p></div>
<div class="m2"><p>هر دو عالم را صلا زد عشق تا من آمدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کعبه و دیری ندیدم غیر الفت‌گاه دل</p></div>
<div class="m2"><p>هرکجا رفتم به پیش آمد همین یک معبدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاکسار عشق را پامال نتوان یافتن</p></div>
<div class="m2"><p>پرتو خورشید بر سرهاست در زیر قدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از بهار من چراغ عبرتی روشن‌ کنید</p></div>
<div class="m2"><p>همچو رنگ خون چمن پرداز چندین مشهدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل از ترک هوس موج‌گهر افسرده نیست</p></div>
<div class="m2"><p>پشتی بنیاد اقبالیست در دست ردم</p></div></div>