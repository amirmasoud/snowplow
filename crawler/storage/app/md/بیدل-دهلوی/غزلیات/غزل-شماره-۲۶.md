---
title: >-
    غزل شمارهٔ ۲۶
---
# غزل شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>چه‌امکان است‌گرد غیرازین محفل‌شود پیدا</p></div>
<div class="m2"><p>همان لیلی شود بی‌پرده تامحمل شود پیدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غناگاه خطاب از احتیاج آگاه می‌گردد</p></div>
<div class="m2"><p>کریم آواز ده کز ششجهت سایل شود پیدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجازاندیشی‌ات فهم حقیقت را نمی‌شاید</p></div>
<div class="m2"><p>محال است اینکه حق ازعالم باطل شود پیدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفس را الفت دل هم ز وحشت برنمی‌آرد</p></div>
<div class="m2"><p>ره ما طی نگردد گر همه منزل شود پیدا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برون دل نفس را پرفشان دیدم ندانستم</p></div>
<div class="m2"><p>که‌عنقا چون شوداز بیضه‌گم‌بسمل شود پیدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به‌گوهر وارسیدن موجها برهم زدن دارد</p></div>
<div class="m2"><p>جهانی را شکافی سینه تا یک دل شود پیدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ره آوارگی عمری‌ست می‌پویم نشد یارب</p></div>
<div class="m2"><p>که چون تمثال یک آیینه‌وارم دل شود پیدا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز محو عشق غیر از عشق نتوان یافت آثاری</p></div>
<div class="m2"><p>به‌دریا قطره خون‌گردیدگم مشکل شود پیدا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شهیدان ادبگاه وفا را خون نمی‌باشد</p></div>
<div class="m2"><p>مگر رنگ حنایی ازکف قاتل شود پیدا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سواد کنج معدومی قیامت عالمی دارد</p></div>
<div class="m2"><p>که هرکس هرکجاگم‌شد ازین منزل شودپیدا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به رنگی موج خلقی ازتپیدن آب می‌گردد</p></div>
<div class="m2"><p>کزین دریا به قدریک‌گهر ساحل شود پیدا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نفس تا هست زین مزرع تلاش دانهٔ دل‌کن</p></div>
<div class="m2"><p>که‌این‌گمگشته‌گر پیداشود حاصل شود پیدا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به قدر آگهی آماده ا ست اسباب تشویشت</p></div>
<div class="m2"><p>طبیعت باید اینجا اندکی غافل شود پیدا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>درین دریا دل هر قطره گوهر در گره دارد</p></div>
<div class="m2"><p>اگر بر روی آب آید همان بیدل شود پیدا</p></div></div>