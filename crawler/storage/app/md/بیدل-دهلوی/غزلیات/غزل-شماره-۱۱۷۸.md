---
title: >-
    غزل شمارهٔ ۱۱۷۸
---
# غزل شمارهٔ ۱۱۷۸

<div class="b" id="bn1"><div class="m1"><p>تا در آیینهٔ دل راه نفس واباشد</p></div>
<div class="m2"><p>کلفت هر دو جهان در گره ما باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبح شبنم ثمر باغچهٔ نیرنگیم</p></div>
<div class="m2"><p>خنده وگریهٔ ما از همه اعضا باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گامها بسکه تر از موج سراب است اینجا</p></div>
<div class="m2"><p>نیست بی‌خشکی لب گر همه دریا باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جلوه مفت است تودرحق نگه ظلم مکن</p></div>
<div class="m2"><p>وهم ‌گو در غم اندیشهٔ فردا باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زین گلستان مگذر بیخبر از کاوش رنگ</p></div>
<div class="m2"><p>شاید این پرده نقاب چمن‌آرا باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پشت و رویی نتوان بست بر آیینهٔ دل</p></div>
<div class="m2"><p>گل این باغ محال است که رعنا باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مژه‌ای ‌گرم توان ‌کرد در این عبرتگاه</p></div>
<div class="m2"><p>بالش خواب کسی‌گر پر عنقا باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سعی واماندگی‌ام ‌کرد به منزل همدوش</p></div>
<div class="m2"><p>گره رشته ره آبلهٔ پا باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به‌ گشاد مژه آغوش یقین انشا کن</p></div>
<div class="m2"><p>جلوه تا چند به چشم‌تومعما باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عشرتی از دل افسرده ما رنگ نبست</p></div>
<div class="m2"><p>خون این شیشه مگر در رگ خارا باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بی ‌زبانی‌ست ندامت‌کش آهنگ ستم</p></div>
<div class="m2"><p>کف افسوس خموشی لب ‌گویا باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل نداریم و همان بارکش صد المیم</p></div>
<div class="m2"><p>زنگ سهل است اگر آینه از ما باشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بیدل آیینه‌ ی مشرب نکشد کلفت زنگ</p></div>
<div class="m2"><p>سینه صافی‌ست در آن بزم که مینا باشد</p></div></div>