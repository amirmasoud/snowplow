---
title: >-
    غزل شمارهٔ ۲۵۹۷
---
# غزل شمارهٔ ۲۵۹۷

<div class="b" id="bn1"><div class="m1"><p>از بسکه ضعف طاقت بوسید روی زانو</p></div>
<div class="m2"><p>خط جبین غلط خورد آخر به موی زانو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آبم در‌ین ادبگاه از شرم غفلت شرم</p></div>
<div class="m2"><p>سر بر هوا نشاید تسلیم خوی زانو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کو معبد حضوری‌ کز ما برد رعونت</p></div>
<div class="m2"><p>صد حیف پیرگشتیم در جستجوی زانو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر جلوه را درین بزم آیینه است منظور</p></div>
<div class="m2"><p>تمثال دل مجو‌بید نادیده روی زانو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکر قد دوتایم امروز فرض گردید</p></div>
<div class="m2"><p>عمریست می‌کشیدم گردن به سوی زانو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشق دبیر اسرار چندین نشست دارد</p></div>
<div class="m2"><p>اما نمی‌توان خواند حرف مگوی زانو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون برگ گل به یادت یک صبح غنچه بودم</p></div>
<div class="m2"><p>شد عمر در جبینم خفته‌ست بوی زانو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زین فکرهای باطل چیزی نمی‌گشاید</p></div>
<div class="m2"><p>گیرم فتاده باشم سر درگلوی زانو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیحاصلان سرا پا اندوه در کمینند</p></div>
<div class="m2"><p>چیزی نروید از بید جز آرزوی زانو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تغییر وضع تسلیم بر غنچه هم ستم کرد</p></div>
<div class="m2"><p>یارب پی چه راحت‌ گشتم عدوی زانو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل چو موج‌ گوهر در فکر خوبش خشکم</p></div>
<div class="m2"><p>پیشانی‌ام قدح زد اما به‌ جوی زانو</p></div></div>