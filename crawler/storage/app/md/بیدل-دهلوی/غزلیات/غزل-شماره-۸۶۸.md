---
title: >-
    غزل شمارهٔ ۸۶۸
---
# غزل شمارهٔ ۸۶۸

<div class="b" id="bn1"><div class="m1"><p>ای ظفر شیفتهٔ همت نصرت فالت</p></div>
<div class="m2"><p>چمن فتح تبسمکدهٔ اقبالت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آیت فضا و سخاشان تو را آینه‌دار</p></div>
<div class="m2"><p>نص تحقیق وفا ترجمهٔ اقوالت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در مقامی‌که شکوهت فشرد پای ثبات</p></div>
<div class="m2"><p>کوه بازد کمر از سایهٔ استقلالت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روح اعدا همه‌گر همسر سیمرغ شود</p></div>
<div class="m2"><p>نیست جز صعوهٔ شاهین قضا چنگالت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرگردن‌شکنان دوختهٔ نقش قدم</p></div>
<div class="m2"><p>تاج شاهان غیور آبلهٔ پامالت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صورت هیچکس آنجا به مقابل نرسد</p></div>
<div class="m2"><p>برهرآیینه‌که غیرت فکند تمثالت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عمرها شدکه به تفهیم شرف می‌نازد</p></div>
<div class="m2"><p>سال و ماه همه در سایهٔ ماه و سالت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر همه عقدهٔ دل بود نگاه توگشود</p></div>
<div class="m2"><p>حق نیفکند سر وکار به هیچ اشکالت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نور ذاتی‌، دلت اندوه کدورت نکند</p></div>
<div class="m2"><p>امر حقی‌، به تغیر نگراید حالت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یارب از ملک اجابت به دعای بیدل</p></div>
<div class="m2"><p>کند اقبال ازل تا ابد استقبالت</p></div></div>