---
title: >-
    غزل شمارهٔ ۲۵۳
---
# غزل شمارهٔ ۲۵۳

<div class="b" id="bn1"><div class="m1"><p>فقر نخواست شکوهٔ مفلسی ازگدای ما</p></div>
<div class="m2"><p>ناله به خواب ناز رفت در نی بوریای ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکرقبول عاجزی تا به‌کجا ادانیم</p></div>
<div class="m2"><p>گشت اجابت از ادب درکف ما دعای ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در چه‌بلافتاده است‌، خلق زکف چه‌داده است</p></div>
<div class="m2"><p>هرکه لبی‌گشاده است آه من است و وای ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جیب ففسن ریبده را بخیهٔ خمی سکجاست</p></div>
<div class="m2"><p>تکمهٔ اشک شبنم ست بند سحر قبای ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرد خیال عاشقان رفت به عالم دگر</p></div>
<div class="m2"><p>پا به فلک نمی‌نهد سر به رهت فدای ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آه‌که همچوسایه رفت عمر به سودن جبین</p></div>
<div class="m2"><p>از سر خاک برنخاست‌کوشش بی‌عصای ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شمع دماغ تک زدن داد به باد سوختن</p></div>
<div class="m2"><p>برتن ما سری نبود آبله داشت پای ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در نفس حباب چیست تاب محیط دم زدن</p></div>
<div class="m2"><p>روبه عرق نهفت ورفت زندگی ازحیای ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در غم جتسجوی رزق سودن دست داشتیم</p></div>
<div class="m2"><p>آبلهرینخت دانه‌ای چند در آسیای ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کاش به نقش پا رسیم تا به‌گذشته‌ها رسیم</p></div>
<div class="m2"><p>هرقدم آه می‌کشد آبله در قفای ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دور بهار لاله‌ایم فرصت عیش ماکم ست</p></div>
<div class="m2"><p>داغ شدیم وداغ هم‌گرم نکرد جای ما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در حرمی‌که آسمان سجده نیارد از ادب</p></div>
<div class="m2"><p>از چه متاع دم زند بیدل بینوای ما</p></div></div>