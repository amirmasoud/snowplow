---
title: >-
    غزل شمارهٔ ۱۱۳۰
---
# غزل شمارهٔ ۱۱۳۰

<div class="b" id="bn1"><div class="m1"><p>زبان به‌کام خموشی‌ کشد بیانش و لرزد</p></div>
<div class="m2"><p>نگه ز دور به حیرت دهد نشانش ولرزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگه نظاره‌ کند از حیا نهانش و لرزد</p></div>
<div class="m2"><p>زبان سخن‌ کند از تنگی دهانش و لرزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه شوکت است ادبگاه حسن را که تبسم</p></div>
<div class="m2"><p>ببوسد از لب موج‌گهر دهانش و لرزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قلم چگونه دهد عرض دستگاه توهم</p></div>
<div class="m2"><p>که فکر مو شود ازحیرت میانش و لرزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دمی‌که آرزوی دل به عرض شوق توکوشد</p></div>
<div class="m2"><p>گره چو شمع شود ناله بر زبانش و لرزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیال ما کند آهنگ سجدهٔ سر راهت</p></div>
<div class="m2"><p>برد تصور از آنسوی آسمانش و لرزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نظربه طینت بیتاب عاشق اینهمه سهل است</p></div>
<div class="m2"><p>که همچو مو ج شود ناله برزبانش ولرزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عجب مدار ز نیرنگ اختراع مروت</p></div>
<div class="m2"><p>که همچوآه زدل بگذرد سنانش ولرزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بود ترحم عشقت به حال ناکسی من</p></div>
<div class="m2"><p>چو مشت خس‌ که ‌کند شعله امتحانش و لرزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به محفل تو که اظهار مدعاست تحیر</p></div>
<div class="m2"><p>نفس در آینه پنهان کند فغانش و لرزد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به وصل وحشتم از دل نمی‌رود چه توان کرد</p></div>
<div class="m2"><p>که سست مشق رسد تیر بر نشانش و لرزد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به عافیت نی‌ام ایمن ز آفتی‌ که ‌کشید</p></div>
<div class="m2"><p>چون آن غریق ‌که آرند بر کرانش و لرزد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز بسکه شرم سجودش گداخت پیکر بیدل</p></div>
<div class="m2"><p>چو عکس آب نهد سر بر آستانش و لرزد</p></div></div>