---
title: >-
    غزل شمارهٔ ۱۵۸۲
---
# غزل شمارهٔ ۱۵۸۲

<div class="b" id="bn1"><div class="m1"><p>گذشت عمر به لرزیدنم ز بیم و امید</p></div>
<div class="m2"><p>قضا نوشت‌ مگر سرخطم‌ به‌ سایهٔ بید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سحر دماندن پیری چه شامهاکه نداشت</p></div>
<div class="m2"><p>سیاه‌کرد جهانم به دیده موی سفید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز دور می‌شنوم‌ گر زبان ما و شماست</p></div>
<div class="m2"><p>جلاجلی‌که صدا بسته بر دف ناهید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز اختراع جنون امل‌ طرازان نیست</p></div>
<div class="m2"><p>قیامت دو نفس عمر و حسرت جاوید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تلاش خلق به جایی نمی‌رسد امّا</p></div>
<div class="m2"><p>همان به دوش نفس ناقه می‌کشد امید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حذر ز نشئهٔ دولت‌ که مستی یک جام</p></div>
<div class="m2"><p>هنوز می‌شکند شیشه برسر جمشید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نماند علم و هنر عشق تا به یاد آمد</p></div>
<div class="m2"><p>چراغها همه‌ گل ‌کرد دامن خورشید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غبار قافلهٔ رفتگان پرافشان‌ست</p></div>
<div class="m2"><p>که ای نفس قدمان شام شد به ما برسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کدورت از دل منعم نمی‌رود بیدل</p></div>
<div class="m2"><p>چه ممکن است ‌که چینی رسد به موی سفید</p></div></div>