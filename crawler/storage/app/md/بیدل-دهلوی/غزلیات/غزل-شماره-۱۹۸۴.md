---
title: >-
    غزل شمارهٔ ۱۹۸۴
---
# غزل شمارهٔ ۱۹۸۴

<div class="b" id="bn1"><div class="m1"><p>از جراحت‌زار دل چیده‌ست دامان ناله‌ام</p></div>
<div class="m2"><p>می‌رسد یعنی ز کوی گل‌فروشان ناله‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده دردآلودهٔ محرومی دیدار کیست</p></div>
<div class="m2"><p>کز شکست اشک می‌جوشد ز مژگان ناله‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همعنان درد دل عمریست از خود می‌روم</p></div>
<div class="m2"><p>نسبتی دارد به آن سرو خرامان ناله‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دید و وادیدم برون پردهٔ رنگ‌ست و بس</p></div>
<div class="m2"><p>هر کجا باشم چه پیدا و چه پنهان ناله‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با ‌دو عالم اضطراب اظهار مطلب خامشی است</p></div>
<div class="m2"><p>صد جرس دل دارم اما نیست امکان ناله‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوش ‌کز بام ازل افتاد طشت ‌کاف و نون</p></div>
<div class="m2"><p>گر تأمل محرم معنی است من آن ناله‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خندهٔ‌گل را نمک از شور بلبل بوده است</p></div>
<div class="m2"><p>حسن او بی‌پرده شد تا گشت عریان ناله‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درد عشقم قصهٔ من بشنو و خاموش باش</p></div>
<div class="m2"><p>تا نهانم‌، داغ‌، چون گشتم نمایان ناله‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از شکست شیشهٔ دل آنقدر غمگین نی‌ام</p></div>
<div class="m2"><p>درد آن دارم‌ که خواهد شد پریشان ناله‌ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون سپندم نیست خاکستر دلیل خامشی</p></div>
<div class="m2"><p>سرمه گشتم تا ببیند چشم یاران ناله‌ام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>راز دل چون موج پوشیدن ندارد ساز من</p></div>
<div class="m2"><p>می‌درد در هر تپیدن صد گریبان ناله‌ام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل از مشت غبار حسرت‌آلودم مپرس</p></div>
<div class="m2"><p>یک بیابان خار خارم‌، یک نیستان ناله‌ام</p></div></div>