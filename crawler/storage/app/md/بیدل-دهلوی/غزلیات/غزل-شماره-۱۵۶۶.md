---
title: >-
    غزل شمارهٔ ۱۵۶۶
---
# غزل شمارهٔ ۱۵۶۶

<div class="b" id="bn1"><div class="m1"><p>باد صحرای جنون هرگه‌ گل‌افشان می‌شود</p></div>
<div class="m2"><p>جیبم از خود می‌رود چندانکه دامان می‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پای تا سر عجز ما آیینه‌ نازکدلی‌ست</p></div>
<div class="m2"><p>خاک را نقش قدم زخم نمایان می‌شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرده ناموس دردم از حجابم چاره نیست</p></div>
<div class="m2"><p>گر گریبان ‌چاک سازم ناله عریان می‌شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غنچهٔ دل به‌ که از فکر شکفتن بگذرد</p></div>
<div class="m2"><p>کاین گره از بازگشتن چشم حیران می‌شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیستی آیینهٔ اقبال عجز ما بس است</p></div>
<div class="m2"><p>خاک را اوج هوا تخت سلیمان می‌شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>معنی دل را حجابی نیست جز طول امل</p></div>
<div class="m2"><p>ریشه چون در جلوه آید د!نه پنهان می‌شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در گشاد عقده دل هیچ‌کس بی‌جهد نیست</p></div>
<div class="m2"><p>موج گوهر ناخنش چون سود دندان می‌شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ماند الفتها به یک سوتا در وحشت زدیم</p></div>
<div class="m2"><p>چن دامن عالمی را طاق نسیان می‌شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زندگانی را نفس سررشته‌ آرام نیست</p></div>
<div class="m2"><p>موج‌ در‌یا را رگ خواب پریشان می‌شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عافیت دور است از نقش بنای محرمی</p></div>
<div class="m2"><p>خون بود رنگی‌کزو تصوبر انسان می‌شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای فضول و هم عقبا آدم از جنت چه دید</p></div>
<div class="m2"><p>عبرت ‌است‌ آنجا که ‌صاحبخانه ‌مهمان می‌شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غنچه‌وار از برگ عیش این چمن بی‌بهره ایم</p></div>
<div class="m2"><p>دامن ماپرگل از چاک گریبان می‌شود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ناله‌ها در پردهٔ دود جگر پیچیده‌ایم</p></div>
<div class="m2"><p>سطر این مکتوب تا خواندن نیستان می‌شود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مست جام‌ مشربم بیدل‌ که از موج می‌اش</p></div>
<div class="m2"><p>جاده‌های دشت یکرنگی نمایان می‌شود</p></div></div>