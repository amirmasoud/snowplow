---
title: >-
    غزل شمارهٔ ۷۷۰
---
# غزل شمارهٔ ۷۷۰

<div class="b" id="bn1"><div class="m1"><p>تعین جز افسون اوهام نیست</p></div>
<div class="m2"><p>نگین خنده‌ای می‌کند نام نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بی‌مقصدی خلق تک می‌زند</p></div>
<div class="m2"><p>همه قاصدانند و پیغام نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان سرخ‌وش پستی فطرت است</p></div>
<div class="m2"><p>هواهاست در هر سر و، بام نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فروغ یقین بر دلکش نتافت</p></div>
<div class="m2"><p>درین خانه‌ها وضع‌گلجام نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی‌تاکجا ناز سبزان‌کشد</p></div>
<div class="m2"><p>به هندوستان یک گل‌اندام نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به هم دوستان را غنودن‌کجاست</p></div>
<div class="m2"><p>دو مغزی به هر جنس بادام نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به غفلت چراغان‌کنید از عرق</p></div>
<div class="m2"><p>که بالیدن سایه بی‌شام نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دماغ حریفان حسرت رساست</p></div>
<div class="m2"><p>به خمیازه ترکن لبت‌، جام نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه اوج سپهر و چه زیرزمین</p></div>
<div class="m2"><p>به هرجا تویی جای آرام نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رعونت اگر نشئهٔ زندگی‌ست</p></div>
<div class="m2"><p>سر زنده باگردنت رام نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غبار عدم باش و آسوده زی</p></div>
<div class="m2"><p>به این جامه تکلیف احرام نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ضروری ندارم سخن می‌کنم</p></div>
<div class="m2"><p>اداهایم از عالم وام نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قناعت کفیل بهار حیاست</p></div>
<div class="m2"><p>گل طینتم بیدل ابرام نیست</p></div></div>