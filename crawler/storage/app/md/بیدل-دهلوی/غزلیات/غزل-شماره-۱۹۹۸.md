---
title: >-
    غزل شمارهٔ ۱۹۹۸
---
# غزل شمارهٔ ۱۹۹۸

<div class="b" id="bn1"><div class="m1"><p>یاد من کردی به سامان‌گشت ناز هستی‌ام</p></div>
<div class="m2"><p>نام دل بردی قیامت کرد ساز هستی‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تخم عجزم پرتنک سرمایهٔ نشو و نماست</p></div>
<div class="m2"><p>سجده‌ای می‌دانم و بس نو نیاز هستی‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تنگ ظرفی احتیاطم ورنه مانند حباب</p></div>
<div class="m2"><p>بحر می‌بالد زآغوش گداز هستی‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو شمعم هر نگه داغی دگر ایجاد کرد</p></div>
<div class="m2"><p>اینقدر یارب که فرمود امتیاز هستی‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من هم از موهومی ساز نفس غافل نی‌ام</p></div>
<div class="m2"><p>تاکجا خواهد دمید افسون طراز هستی‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبحم و در پردهٔ شب زندگانی می کنم</p></div>
<div class="m2"><p>بی‌نفس خوابیده‌است افسانه ساز هستی‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر همه توفان شوم‌ کیفیتم بی‌پرده نیست</p></div>
<div class="m2"><p>عشق درگوش عدم خوانده‌ست راز هستی‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای شرار رفته از خود پر به بیرنگی مناز</p></div>
<div class="m2"><p>دیده‌ام رنگی که من هم بی‌نیاز هستی‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سایه را بر خاک ره پیداست ترجیح عروج</p></div>
<div class="m2"><p>اینقدر من نیز بیدل سر فراز هستی‌ام</p></div></div>