---
title: >-
    غزل شمارهٔ ۱۳۰۲
---
# غزل شمارهٔ ۱۳۰۲

<div class="b" id="bn1"><div class="m1"><p>کم نیست صحبت دل‌گر مرد، زن نماند</p></div>
<div class="m2"><p>آیینه خانه‌ای هست‌، گر انجمن نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر حسرت هوس‌کیش بازآید از فضولی</p></div>
<div class="m2"><p>کلفت‌ کراست هر چند گل در چمن نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>افسون کاهش اینجا تاب و تب نفسهاست</p></div>
<div class="m2"><p>دامن‌ فشان بر این شمع تا سوختن نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عرفان ز فهم‌ دوری‌ست‌،‌ادراک بی‌ حضوری‌ست</p></div>
<div class="m2"><p>جهدی‌ که در خیالت این علم و فن نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون صبح از این بیابان چندان تلاش رم‌ کن</p></div>
<div class="m2"><p>کز دامن بلندت‌ گرد شکن نماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یاد گذشتگان هم آینده است اینجا</p></div>
<div class="m2"><p>در کارگاه تجدید چیزی کهن نماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر وضع خلق ختم است آرایش حقیقت</p></div>
<div class="m2"><p>گلشن ‌کجاست هرگه سرو و سمن نماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مجنون به هر در و دشت محو کنار لیلی‌ست</p></div>
<div class="m2"><p>عاشق به سعی غربت دور از وطن نماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرد خیال تا کی هر سو دهد نشانم</p></div>
<div class="m2"><p>جایی روم‌ که آنجا او هم ز من نماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این مبحث تو و من از نسخهٔ عدم نیست</p></div>
<div class="m2"><p>گر زان دهن بگویم جای سخن نماند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یاران به وسع امکان در ستر حال‌ کوشید</p></div>
<div class="m2"><p>تصویر انفعالیم گر پیرهن نماند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل به دیر اعراض انصاف نیست ورنه</p></div>
<div class="m2"><p>تاوان بت‌پرستی بر برهمن نماند</p></div></div>