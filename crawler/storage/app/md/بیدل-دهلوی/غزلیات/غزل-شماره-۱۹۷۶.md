---
title: >-
    غزل شمارهٔ ۱۹۷۶
---
# غزل شمارهٔ ۱۹۷۶

<div class="b" id="bn1"><div class="m1"><p>برق حسنی در نظر دارم به خود پیچیده‌ام</p></div>
<div class="m2"><p>جوهر آیینه یعنی موی آتش دیده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نادمیدن زین شبستان پاس ناموس حیاست</p></div>
<div class="m2"><p>چون سحر عمریست خود را با نفس دزدیده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر قدر پر می‌زنم پرواز محو بیخودی است</p></div>
<div class="m2"><p>ازکجا یارب عنان رنگ گردانیده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا ابد می‌بایدم خط بر شکست دل‌ کشید</p></div>
<div class="m2"><p>در غبار موی چینی چون صدا لغزیده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز ندامت چارهٔ درد سر اسباب نیست</p></div>
<div class="m2"><p>صندل انشای ‌کف دست به هم ساییده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محو گردد کاش از آیینه‌ام نقش کمال</p></div>
<div class="m2"><p>کز صفا تا جوهرم باقیست دامن چیده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صورت پیدایی و پنهانی سازم یکیست</p></div>
<div class="m2"><p>هرکجایم چون صدا عریانیی پوشیده‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زندگی یارب تماشاخانهٔ دیدار کیست</p></div>
<div class="m2"><p>گل‌فروش صد چمن تعبیر خوابی دیده‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غیررا درخلوت تحقیق معنی بارنیست</p></div>
<div class="m2"><p>جز به ‌گوش ‌گل صدای بوی ‌گل نشنیده‌ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صد قیامت رفته باشد تا ز خود یابم خبر</p></div>
<div class="m2"><p>قاصدم لیک از جهان ناز برگردیده‌ام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پابه خاکم زن‌ که مژگان غبارم وا شود</p></div>
<div class="m2"><p>گر تو بیدارم نسازی تا ابد خوابیده‌ام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیدل از بی دست‌وپاییهای من غافل مباش</p></div>
<div class="m2"><p>چون ضعیفی گوشمال گردن بالیده‌ام</p></div></div>