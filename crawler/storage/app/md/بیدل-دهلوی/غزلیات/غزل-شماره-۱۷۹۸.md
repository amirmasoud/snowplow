---
title: >-
    غزل شمارهٔ ۱۷۹۸
---
# غزل شمارهٔ ۱۷۹۸

<div class="b" id="bn1"><div class="m1"><p>شوق آزادی سر از سامان استغنا مکش</p></div>
<div class="m2"><p>گرکشی بار تعلق جز به پشت پا مکش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای شرر زین مجمرت آخر پری باید فشاند</p></div>
<div class="m2"><p>گر همه در سنگ باشی آنقدرها وامکش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر نمی‌آید خرد با ساز حشرآهنگ دل</p></div>
<div class="m2"><p>مغز مستی ‌گر نداری پنبه از مینا مکش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمع را رعنایی او داغ خجلت می‌کند</p></div>
<div class="m2"><p>سرنگونی می‌کشی ‌گردن به این بالا مکش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صرفهٔ هستی ندارد سایه را ترک ادب</p></div>
<div class="m2"><p>هر طرف خواهی برو لیک ازگلیمت پا مکش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>معنی نازک ندارد تاب تحریک نفس</p></div>
<div class="m2"><p>از ادب مگسل طناب خیمهٔ لیلا مکش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خشکی خمیازه بر یاران پسندیدن تری‌ست</p></div>
<div class="m2"><p>عالم آب است اگر ساغرکشی تنها مکش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کلفت رفع علایق از هر آفت بدتر است</p></div>
<div class="m2"><p>خار اگر داری بیا رنج‌ کشیدنها مکش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتگو هنگامهٔ برهمزن روشن دلی است</p></div>
<div class="m2"><p>این بساط آیینه‌ها دارد نفس اینجا مکش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آب می‌گردد دل از درد وطن آوارگان</p></div>
<div class="m2"><p>ای ترحم صید دام ماهی از دریا مکش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>انفعال فطرتم ای کلک نقاش ‌کرم</p></div>
<div class="m2"><p>رنگ می‌بازد حیا ما را به روی ما مکش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نسبتت بیدل به آزادی ز مجنون نیست کم</p></div>
<div class="m2"><p>رشته‌ای داری تو هم از دامن صحرا مکش</p></div></div>