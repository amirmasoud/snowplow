---
title: >-
    غزل شمارهٔ ۲۰۰۵
---
# غزل شمارهٔ ۲۰۰۵

<div class="b" id="bn1"><div class="m1"><p>بی حوصلگی‌کرد درین بزم ‌کبابم</p></div>
<div class="m2"><p>چون اشک نگون ساغر یک جرعه شرابم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پامال هوسهای جهانم چه توان‌کرد</p></div>
<div class="m2"><p>مخمل نی‌ام اما سر هر موست به خوابم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنیاد من آب و گل تشخیص ندارد</p></div>
<div class="m2"><p>از دور نمایند مگر همچو سرابم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن روزکه چون شعله به خود چشم‌گشودم</p></div>
<div class="m2"><p>برچهره ز خاکستر خود بود گلابم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یار از نظرم رفته و من می‌روم از خویش</p></div>
<div class="m2"><p>ای ناله شتابی که درنگست شتابم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از صفحهٔ من غیر تحیر نتوان خواند</p></div>
<div class="m2"><p>چون آینه شستند ندانم به چه آبم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>انداز غبارم چو سحر بسکه بلند است</p></div>
<div class="m2"><p>با همنفسان از لب بام است خطابم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون ماه نوم بسکه برون دار تعین</p></div>
<div class="m2"><p>شایستهٔ بوس لب خویش است رکابم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای چرخ ز سر تا قدمم رشتهٔ عجزیست</p></div>
<div class="m2"><p>تا نگسلم از خو مده آنهمه تابم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در جلوه‌گه او اثر من چه خیالست</p></div>
<div class="m2"><p>گمگشته تر از سایهٔ خورشید نقابم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا دم زده‌ام ساز طربها همه خشکست</p></div>
<div class="m2"><p>آب تنکی تاخته بر روی حبابم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>واکردن چشم آنقدرم ده دله دارد</p></div>
<div class="m2"><p>بی‌دل به همین صفر فزوده است حسابم</p></div></div>