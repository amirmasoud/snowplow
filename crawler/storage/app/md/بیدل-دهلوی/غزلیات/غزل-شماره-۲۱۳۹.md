---
title: >-
    غزل شمارهٔ ۲۱۳۹
---
# غزل شمارهٔ ۲۱۳۹

<div class="b" id="bn1"><div class="m1"><p>جز سوختن به یادت مشقی دگر ندارم</p></div>
<div class="m2"><p>در پرتو چراغی پروانه می‌نگارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز نشاط شب کرد آخر فراق یارم</p></div>
<div class="m2"><p>خود را اگر نسوزم شمعی دگر ندارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی‌کس شهید عشقم خاک مرا بسوزید</p></div>
<div class="m2"><p>خاکستری زند کاش‌ گل بر سر مزارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین باغ شبنم من دیگر چه طرف بندد</p></div>
<div class="m2"><p>آیینه‌ای شکستم رنگی نشد دچارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز درد دل چه دارد تبخاله آرمیدن</p></div>
<div class="m2"><p>یارب عرق نریزد از خجلت آبیارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شوقی ‌که رنگ دل ریخت در کارگاه امکان</p></div>
<div class="m2"><p>وقف گداز می‌خواست یک آبگینه‌وارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شمع بساط الفت نومید سوختن نیست</p></div>
<div class="m2"><p>در آتشم سراپا تا زیر پاست خارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاکم به باد دادند اما به سعی الفت</p></div>
<div class="m2"><p>در سایهٔ خط او پر می‌زند غبارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صبر آزمای عشقت در خواب بی‌نیازی‌ست</p></div>
<div class="m2"><p>گرداندنم چه حرفست پهلوی کوهسارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی‌فهم معنیی نیست بر دل تنیدن من</p></div>
<div class="m2"><p>تمثال کرده‌ام گم آیینه می‌فشارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل به معبد عشق پروای طاقتم نیست</p></div>
<div class="m2"><p>چندانکه می‌تپد دل من سبحه می‌شمارم</p></div></div>