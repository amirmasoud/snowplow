---
title: >-
    غزل شمارهٔ ۷۷
---
# غزل شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>آنجا که فشارد مژه‌ام دیدهٔ تر را</p></div>
<div class="m2"><p>پرواز هوس پنبه‌کند آب‌گهر را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p> وقت است چوگرداب به سودای خیالت</p></div>
<div class="m2"><p> ثابت قدم نازکنم گردش سر را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p> محوتو ز آغوش تمنا چه‌گشاید</p></div>
<div class="m2"><p> رنگیست تحیرگل تصویر نظر را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p> زین بادیه رفتم‌که به سرچشمهٔ خورشید</p></div>
<div class="m2"><p> چون سایه بشویم ز جبین‌گرد سفر را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p> یارب چه بلا بودکه تردستی ساقی</p></div>
<div class="m2"><p> بر خرمن مخمور فشاند آتش تر را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p> از اشک مجوبید نشان بر مژة من</p></div>
<div class="m2"><p> کاین رشته ز سستی نکشیده‌ست‌گهر را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p> تسلیم همان آینهٔ حسن کمال است</p></div>
<div class="m2"><p> چون ماه نو ایجادکن از تیغ سپر را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p> تاکی چو جرس دل به تپیدن بخراشم</p></div>
<div class="m2"><p> در ناله‌ام آغوش وداعیست اثر را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p> از اشک توان محرم رسوایی ما شد</p></div>
<div class="m2"><p> شبنم همه‌جا آینه‌دارست سحر را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p> چون قافلهٔ عمر به دوش نفسی چند </p></div>
<div class="m2"><p> رفتیم به جایی‌که خبر نیست خبر را </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیدل چو سحر دم مزن از درد محبت</p></div>
<div class="m2"><p> تا آنکه نبندی به نفس چاک جگررا </p></div></div>