---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>چشم تو که فتنه ی جهان خیزد ازو</p></div>
<div class="m2"><p>لعل تو که آب خضر می ریزد ازو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کردند تن مرا چنان خوار که باد</p></div>
<div class="m2"><p>می آید و گرد و خاک می بیزد ازو</p></div></div>