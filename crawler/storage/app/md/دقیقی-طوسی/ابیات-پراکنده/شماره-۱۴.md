---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>که را رودکی گفته باشد مدیح</p></div>
<div class="m2"><p>امام فنون سخن بود ور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دقیقی مدیح آورد نزد او</p></div>
<div class="m2"><p>چو خرما بود برده سوی هَجَر</p></div></div>