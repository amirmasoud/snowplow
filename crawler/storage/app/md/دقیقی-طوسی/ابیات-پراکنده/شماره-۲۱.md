---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>ملک آن یادگار آل دارا</p></div>
<div class="m2"><p>ملک آن قطب دور آل سامان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر بیند بگاه کینش ابلیس</p></div>
<div class="m2"><p>ز بیم تیغ او بپذیرد ایمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بپای لشکرش ناهید و هرمز</p></div>
<div class="m2"><p>به پیش لشکرش مریخ و کیوان</p></div></div>