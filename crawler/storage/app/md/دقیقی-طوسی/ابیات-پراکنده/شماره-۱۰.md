---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>چگونه بلائی که پیوند تو</p></div>
<div class="m2"><p>نجویی بد است و بجویی بتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شبی بیش کردم چگونه شبی</p></div>
<div class="m2"><p>همی از شب داج تاریک تر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درنگی که گفتم که پروین همی</p></div>
<div class="m2"><p>نخواهد شد از تارکم زاستر</p></div></div>