---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>ز دو چیز گیرند مر مملکت را</p></div>
<div class="m2"><p>یکی پرنیانی یکی زعفرانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی زر نام ملک بر نبشته</p></div>
<div class="m2"><p>دگر آهن آب دادهٔ یمانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که را بویهٔ وصلت ملک خیزد</p></div>
<div class="m2"><p>یکی جنبشی بایدش آسمانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زبانی سخنگوی و دستی گشاده</p></div>
<div class="m2"><p>دلی همش کینه همش مهربانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که ملکت شکاریست کو را نگیرد</p></div>
<div class="m2"><p>عقاب پرنده نه شیر ژیانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دو چیزست کو را ببند اندر آرد</p></div>
<div class="m2"><p>یکی تیغ هندی دگر زرّ کانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به شمشیر باید گرفتن مر او را</p></div>
<div class="m2"><p>به دینار بستنش پای ار توانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که را بخت و شمشیر و دینار باشد</p></div>
<div class="m2"><p>به بالا و تن، تهم و نسبت کیانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خرد باید آنجا و جود و شجاعت</p></div>
<div class="m2"><p>فلک مملکت کی دهد رایگانی</p></div></div>