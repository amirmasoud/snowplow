---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>می صافی بیارای بت که صافی است</p></div>
<div class="m2"><p>جهان از ماه تا آنجا که ماهی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو از کاخ آمدی بیرون بصحرا</p></div>
<div class="m2"><p>کجا چشم افکنی دیبای شاهی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیا تا می خوریم و شاد باشیم</p></div>
<div class="m2"><p>که هنگام می و روز مناهی است</p></div></div>