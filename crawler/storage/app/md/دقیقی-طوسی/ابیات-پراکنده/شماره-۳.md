---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>برخیز و بر افروز هلا قبله ی زردشت</p></div>
<div class="m2"><p>بنشین و بر افکن شکم قاقم بر پشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس کس که ز زردشت بگردیده، دگر بار</p></div>
<div class="m2"><p>ناچار کند روی سوی قبله ی زردشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من سرد نیابم که مرا ز آتش هجران</p></div>
<div class="m2"><p>آتشکده گشتست دل و دیده چو چرخشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر دست بدل بر نهم از سوختن دل</p></div>
<div class="m2"><p>انگشت شود بیشک در دست من انگشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای روی تو چون باغ و همه باغ بنفشه</p></div>
<div class="m2"><p>خواهم که بنفشه چنم از زلف تو یک مشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکس که ترا کشت، ترا کشت و مرا زاد</p></div>
<div class="m2"><p>و آنکس که ترا زاد، ترا زاد و مرا کشت</p></div></div>