---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>تو آن ابری که ناساید شب و روز</p></div>
<div class="m2"><p>ز باریدن چنانچون از کمان تیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نباری بر کف دلخواه جز زر</p></div>
<div class="m2"><p>چنانچون بر سر بدخواه جز بیر</p></div></div>