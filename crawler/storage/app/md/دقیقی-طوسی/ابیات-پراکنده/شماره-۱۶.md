---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>بزلف کژّ ولیکن بقدّ و قامت راست</p></div>
<div class="m2"><p>به تن درست و لیکن بچشمکان بیمار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر سر آرد یار آن سنان او نشگفت</p></div>
<div class="m2"><p>هر آینه چو همه خون خورد سر آرد بار</p></div></div>