---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>خداوند ذوالجلال روان‌بخش ذوالکرم</p></div>
<div class="m2"><p>بر آرنده حدوث بر آزنده قدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منزه ز چون و چند مقدس ز کیف و کم</p></div>
<div class="m2"><p>نه در بود او زوال نه در داد او ستم</p></div></div>
<div class="b2" id="bn3"><p>کند هست هر چه را بهستی است مستحق</p></div>
<div class="b" id="bn4"><div class="m1"><p>بوجه کمال کرد تجلی ز عیب ذات</p></div>
<div class="m2"><p>هویدا شد از تمام در اسماء و در صفات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رخ از موهبت نمود بمرآت ممکنات</p></div>
<div class="m2"><p>هر آن ممکنی گرفت زوی خلعت حیات</p></div></div>
<div class="b2" id="bn6"><p>شد آیات رحمتش شئونات ما خلق</p></div>