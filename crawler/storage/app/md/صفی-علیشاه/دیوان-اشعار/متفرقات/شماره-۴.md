---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>خویش مکن زمانهان خیز و بیا سخن بگو</p></div>
<div class="m2"><p>رمزی از آب لب و دهان بی‌لب و بی‌دهن بگو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق ترا چو سرجان از همه کس کنم نهان</p></div>
<div class="m2"><p>نیست منی در این وصف رخت بمن بگو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دل خویش بوی تو می‌شنوم بموی تو</p></div>
<div class="m2"><p>می‌کشدم بسوی تو زلف تو زان‌شکن بگو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر و قدا قیام کن در دل ما خرام کن</p></div>
<div class="m2"><p>رخ بنما کلام کن گرد گل از چمن بگو</p></div></div>