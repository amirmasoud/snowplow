---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>بنیاد جهان چو یافت تأسیس</p></div>
<div class="m2"><p>شد بوالبشر آشکار و ابلیس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باشد مثل اینکه گشت در رمل</p></div>
<div class="m2"><p>شد خانه هشت جای انکیس</p></div></div>