---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>ای بار خدای فرد واحد</p></div>
<div class="m2"><p>هستی تو بحال بنده شاهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با خلق تو نیستم مخاصم</p></div>
<div class="m2"><p>جز نفس که باویم مجاهد</p></div></div>