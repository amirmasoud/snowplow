---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>عجب آمدم که آمدم ز تو مژده وصالی</p></div>
<div class="m2"><p>که بعمر خود ندادم بوصالت احتمالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنما رخ از چه شاهی ر حجاب طره گاهی</p></div>
<div class="m2"><p>که شبی بروی ماهی نگرم ز بعد سالی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بتو زیید ار که خوبان برخت شوندقربان</p></div>
<div class="m2"><p>که ندید چشم دوران ز تو خوبتر جمالی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بفقیه طعنه کم زن بسیه دلی و خامی</p></div>
<div class="m2"><p>که ندیده روی ماهت که نبرده ره بحالی</p></div></div>