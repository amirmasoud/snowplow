---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>ای آنکه با مرتست ایجاد</p></div>
<div class="m2"><p>یادش کنی ار کست کند یاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غیر از تو بهر دم از مکاره</p></div>
<div class="m2"><p>ما را نرسد کسی بفریاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای آنکه توئی بذات موجود</p></div>
<div class="m2"><p>باقی هم فانیند و نابود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ار جود تو گشت عالمی خلق</p></div>
<div class="m2"><p>ما راست امید بر همان جود</p></div></div>