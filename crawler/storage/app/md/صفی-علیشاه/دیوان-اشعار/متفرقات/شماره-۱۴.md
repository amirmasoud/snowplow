---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>کرم‌های ترا هرگز فراموش</p></div>
<div class="m2"><p>نخواهم کرد اگر کردم کفن‌پوش</p></div></div>