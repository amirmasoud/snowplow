---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>کسی که کرد نفی تفسیر من</p></div>
<div class="m2"><p>که این زوست باشد از پیش ازین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخندد ابلیس بر آن بینوا</p></div>
<div class="m2"><p>که این بود ز احمقان اولین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو صد هزار نظم و نثر صفی</p></div>
<div class="m2"><p>جهان نموده چون بهشت برین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو اغشمی که نشنوی بوی گل</p></div>
<div class="m2"><p>چه حاصلت ز سنبل و یاسمین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه حاصل آنکه آفتاب منیر</p></div>
<div class="m2"><p>بتابد آن بهر خفاش و عمین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صفی نرنجد از کلام حسود</p></div>
<div class="m2"><p>که گفته حرفی از ره حقد و کین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ولیک باشدم از اینرو دریغ</p></div>
<div class="m2"><p>که حق نموده لعن بر مفترین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر او دهد خدای توفیق آن</p></div>
<div class="m2"><p>که تا شود به بخردی همنشین</p></div></div>