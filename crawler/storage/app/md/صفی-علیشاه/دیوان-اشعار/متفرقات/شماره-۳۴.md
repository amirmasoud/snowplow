---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>فرو مانده‌ای گر به غم و ابتلائی</p></div>
<div class="m2"><p>نمایم ترا ره به دار الشفائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شفاخانه حق از سبق رحمت</p></div>
<div class="m2"><p>هر آن درد را هست آنجا دوائی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حسین آن خداوند ملک شهادت</p></div>
<div class="m2"><p>که از مهر او نیست برتر ولائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود سایه‌اش ظل ممدود باری</p></div>
<div class="m2"><p>پناهده گر که جوید لوائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اجابت بنام حسین است از حق</p></div>
<div class="m2"><p>که از اضطرار کند کس دعائی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود تا که مفتوح و ملجا جنابش</p></div>
<div class="m2"><p>مبر جز بوی گر بری التجائی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که آسان شود مشکلی بر خلایق</p></div>
<div class="m2"><p>جز از دست و بازوی مشگل گشائی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که باشد حقش خون‌بها جد پیمبر</p></div>
<div class="m2"><p>ولی باب و مادرش خیرالنسائی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به مهرش بدار آدمیزاده یکدل</p></div>
<div class="m2"><p>بر آدم نمی‌رفت هرگز خطائی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر ابلیس از رفعتش بودی آگه</p></div>
<div class="m2"><p>نمی‌کرد از آن سجده هرگز ابائی</p></div></div>