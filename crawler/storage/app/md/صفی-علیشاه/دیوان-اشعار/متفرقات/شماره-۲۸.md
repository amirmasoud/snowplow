---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>ای بار خدای لایزالی</p></div>
<div class="m2"><p>ذات تو عری ز خلق و عالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیوسته کنم گنه که بر من</p></div>
<div class="m2"><p>عفو تو رسد علی‌التوالی</p></div></div>