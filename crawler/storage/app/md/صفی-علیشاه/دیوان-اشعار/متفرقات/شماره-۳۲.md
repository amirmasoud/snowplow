---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>اگر عیان شود از اهل شرع و فقر فجور</p></div>
<div class="m2"><p>ز آدمی نبود هیچ عیب و نقصی دور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تفاوت آنکه بود چشم اهل دل روشن</p></div>
<div class="m2"><p>بطلعتی که ز دیدار اوست زاهد کور</p></div></div>