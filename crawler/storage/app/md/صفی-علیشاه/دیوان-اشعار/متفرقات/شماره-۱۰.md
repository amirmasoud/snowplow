---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>سراغت دارم دارم ای ماه یگانه</p></div>
<div class="m2"><p>حریفان را روی هر شب بخانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون آئی نزد ما ننشسته بر جا</p></div>
<div class="m2"><p>دراندازی پی رفتن بهانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش آنروزی که بودی یار باما</p></div>
<div class="m2"><p>نبودت کار با اهل زمانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بماز ابرو چو می‌گشتی گمانکش</p></div>
<div class="m2"><p>کشیدی تیر مژگان را کمانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نکرد اغنی خدنگت با نشانی</p></div>
<div class="m2"><p>همانا جز دل ما را نشانه</p></div></div>