---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>تا شد دلم شکسته آن زلف عنبرین</p></div>
<div class="m2"><p>برداشتم درست دل از عقل و جان و دین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سودا بد آنچه در سر شوریده شد مکین</p></div>
<div class="m2"><p>در هر کجا ز حلقه دیوانگان بر این</p></div></div>
<div class="b2" id="bn3"><p>افسانه گشت قصه حال من غمین</p></div>
<div class="b" id="bn4"><div class="m1"><p>گمگشته مراست که او را دل است نام</p></div>
<div class="m2"><p>بودش همیشه در شکن طره مقام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزی اسیر سلسله گشت و هین مدام</p></div>
<div class="m2"><p>میجویمش ز نام کجا در هزار دام</p></div></div>
<div class="b2" id="bn6"><p>میپرسمش ز حال کجا در هزار چین</p></div>
<div class="b" id="bn7"><div class="m1"><p>دارد هزار گوشه در آن طره دل درنگ</p></div>
<div class="m2"><p>زین کرده سخت بر من دیوانه کار تنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از چار سو برگردانم افکنده پا لهنگ</p></div>
<div class="m2"><p>گاهی کشد برومم و گاهی کشد بزنگ</p></div></div>
<div class="b2" id="bn9"><p>گاه افکند بهندم و گاه آورد بچین</p></div>
<div class="b" id="bn10"><div class="m1"><p>هر تار موی خم بخمش راست صد شکن</p></div>
<div class="m2"><p>در هر شکن اسیر دل و دین دو صد چو من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در وی گرفته هر دل آواره وطن</p></div>
<div class="m2"><p>فرزانه ساحریست همه رنگ و مکرو فن</p></div></div>
<div class="b2" id="bn12"><p>پیچیده اژدریست همه شید و کید و کین</p></div>
<div class="b" id="bn13"><div class="m1"><p>اندیشه را عبور نه زانموی مدلهم</p></div>
<div class="m2"><p>تا چون نهد زبیم براه غمش قدم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر جا فتد چو مرغ معلق بدام غم</p></div>
<div class="m2"><p>گویا مستر است در او خام خم بخم</p></div></div>
<div class="b2" id="bn15"><p>مانا مکرر است در اودام چین بچین</p></div>
<div class="b" id="bn16"><div class="m1"><p>گفتم برم پناه از این غم بعاقله</p></div>
<div class="m2"><p>او هم چو بنده بودگرفتار سلسله</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صبر و قرار و حسن و هش و عقل و حوصله</p></div>
<div class="m2"><p>بودند جمله با دل دیوانه یکدله</p></div></div>
<div class="b2" id="bn18"><p>یعنی در آن دو طره عقل آزمارهین</p></div>
<div class="b" id="bn19"><div class="m1"><p>دوشم که بود خاطر از آن موی مشک فر</p></div>
<div class="m2"><p>مجنون صفت بوادی اندوه در بدر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ناگه بجان فتاد مرا آتشی دگر</p></div>
<div class="m2"><p>یکباره زد بهستی موهوم من شرر</p></div></div>
<div class="b2" id="bn21"><p>پرداختم وجود ز غوغای آن و این</p></div>
<div class="b" id="bn22"><div class="m1"><p>کردم تهی ز زارغ و زغن آشیانه را</p></div>
<div class="m2"><p>پرداختم تمام ز هستی میانه را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خالی نمودم از خود و اغیار خانه را</p></div>
<div class="m2"><p>دیدم ببزم جمع نگار یگانه را</p></div></div>
<div class="b2" id="bn24"><p>کافکنده پرده بر طرف از روی نازنین</p></div>
<div class="b" id="bn25"><div class="m1"><p>چشمش ز شب نشینی بسیار نیم خواب</p></div>
<div class="m2"><p>جعدش بدلربائی عشاق نیم تاب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بنهفته در دو لعل لبش صد قرابه ناب</p></div>
<div class="m2"><p>بد محتجب ز پرتو رخسارش آفتاب</p></div></div>
<div class="b2" id="bn27"><p>زان رخ شدم حقیقت حق‌‌البقین یقین</p></div>
<div class="b" id="bn28"><div class="m1"><p>لعلش که بود از خم اسرار باده نوش</p></div>
<div class="m2"><p>یکباره برد ازمن سرمست عقل و هوش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز افسانه وجود چو یکجا شدم خموش</p></div>
<div class="m2"><p>بود آنچه می‌رسید در آنحالتم بگوش</p></div></div>
<div class="b2" id="bn30"><p>تمجید پیر عشق از آن لعل گوهرین</p></div>
<div class="b" id="bn31"><div class="m1"><p>ایدل گرت هواست که در عالم نیاز</p></div>
<div class="m2"><p>گردد بنفس عارجه معراجت این نماز</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همت طلب نخست ز مردان پاکباز</p></div>
<div class="m2"><p>تکبیر پس بگوی وز هستی کن احتراز</p></div></div>
<div class="b2" id="bn33"><p>یعنی چهار بار دل از شش جهه گزین</p></div>
<div class="b" id="bn34"><div class="m1"><p>کردی چو قصد وروی نمودی بحق زجان</p></div>
<div class="m2"><p>کونین را بپشت سر انداز آن زمان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سر حضور نیست گرت باور ای جوان</p></div>
<div class="m2"><p>در ضمن گفتگو کنم آن راز را بیان</p></div></div>
<div class="b2" id="bn36"><p>هشدار تا دلت شود از نور حق مبین</p></div>
<div class="b" id="bn37"><div class="m1"><p>اول شناس نقطه بارا توای حکیم</p></div>
<div class="m2"><p>هم فرض دان و لای را در دل ایسلیم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بسم‌الله است آیه آن نقطه عظیم</p></div>
<div class="m2"><p>پس گو بنام دوست تو رحمن والرحیم</p></div></div>
<div class="b2" id="bn39"><p>یعنی بعام و خاص بودر رحمتش قرین</p></div>
<div class="b" id="bn40"><div class="m1"><p>محمود مطلق است چون آن سید مجید</p></div>
<div class="m2"><p>حمدش فتاده فرض بهر عبدی از عبید</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>این حمد قفل رحمت حقل را بود کلید</p></div>
<div class="m2"><p>توفیق حمد پس طلب از حامد حمید</p></div></div>
<div class="b2" id="bn42"><p>آنگاه زن مدام دم از رب عالمین</p></div>
<div class="b" id="bn43"><div class="m1"><p>چون سابق است بر غضبش رحمت ایفلان</p></div>
<div class="m2"><p>تکرار کرده رحمت خود را ببندگان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>آنجا بشرط مغفرت اینجا به شرط جان</p></div>
<div class="m2"><p>رحمن الرحیم باین قصد پس بخوان</p></div></div>
<div class="b2" id="bn45"><p>تا در دوکون بر تو شود رحمتش معین</p></div>
<div class="b" id="bn46"><div class="m1"><p>گر داری اعتقاد به عدل حق ای جواد</p></div>
<div class="m2"><p>بر موقف حساب ترا باید اعتقاد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>کآنجا براستی کند اظهار عدل و داد</p></div>
<div class="m2"><p>تعدیل کفر و دین شود اندر صف معاد</p></div></div>
<div class="b2" id="bn48"><p>پس مال کس بگوی بر اثبات بوم دین</p></div>
<div class="b" id="bn49"><div class="m1"><p>سرّ حضور پیش تراگفتم ای ولی</p></div>
<div class="m2"><p>دل کردمت ز جلوه معبود صیقلی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>آن نکته را تمام کنم بر تو من جلی</p></div>
<div class="m2"><p>تا رو کند بجانت ظهور سینجلی</p></div></div>
<div class="b2" id="bn51"><p>خواند حقت براه عبادت ز مخلصین</p></div>
<div class="b" id="bn52"><div class="m1"><p>این بندگان مقام حضور است ای فقیر</p></div>
<div class="m2"><p>وان جذبه تو چیست تو لا بعون پیر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ایاک نعبد است پس از آن فعل مستجیر</p></div>
<div class="m2"><p>مقصود از این سلوک بود جذبه مجیر</p></div></div>
<div class="b2" id="bn54"><p>جز جذبه نیست زین عمل آمال مؤمنین</p></div>
<div class="b" id="bn55"><div class="m1"><p>اندر سلوک و فعل چو ای سالک شهود</p></div>
<div class="m2"><p>باقی ترا هنوز بود هستی از وجود</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>پس در عمل تو طالب این جذبه باش زود</p></div>
<div class="m2"><p>یعنی بجوی یاری از آن پادشاه جود</p></div></div>
<div class="b2" id="bn57"><p>تا از خودی بجذبه شوی خالص و امین</p></div>
<div class="b" id="bn58"><div class="m1"><p>ظاهر شدت چو سر عبادت بدین نمط</p></div>
<div class="m2"><p>روکن کنون بدرگه معبود بی غلط</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>کاین رهنما ماست زهی جاذب فقط</p></div>
<div class="m2"><p>دادی بقسم حصر چو بر بندگیش خط</p></div></div>
<div class="b2" id="bn60"><p>از صدق دل بگوی پس ایاک نستعین</p></div>
<div class="b" id="bn61"><div class="m1"><p>دل در حضور پیر چو بر کندی از دو کون</p></div>
<div class="m2"><p>زینسان شدی تمام مجرد زکون و لون</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>شد دیده‌ات زهر چه بجز دوست لایرون</p></div>
<div class="m2"><p>یعنی ترا پرستم و خواهم ترا بعون</p></div></div>
<div class="b2" id="bn63"><p>یعنی ترا ستایم و جویم ترا معین</p></div>
<div class="b" id="bn64"><div class="m1"><p>خواهی اگر ز سر صراطت کنم علیم</p></div>
<div class="m2"><p>مرد حق است معنی آن راه مستقیم</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>در اِهدنا نابجوی تو زان یار ای حکیم</p></div>
<div class="m2"><p>پس نه قدم به همت هادی بدون بیم</p></div></div>
<div class="b2" id="bn66"><p>در راه تا شوی تو ز اصحاب راستین</p></div>
<div class="b" id="bn67"><div class="m1"><p>وان نعمتی که کرده حق اتمام در الست</p></div>
<div class="m2"><p>راه ولای سید ما نعمه‌الله است</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بردار دل ز دوستی غیر هرچه هست</p></div>
<div class="m2"><p>بنشین بخوان نعمتش ای مرد حق‌پرست</p></div></div>
<div class="b2" id="bn69"><p>تا در صراط راست شوی ز اهل‌الذین</p></div>
<div class="b" id="bn70"><div class="m1"><p>راز دگر نیوش گرت گوش دل بجاست</p></div>
<div class="m2"><p>از سر غیر ضال که آن نکته رضاست</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>یعنی رضای حق ز تو در مسلک رضاست</p></div>
<div class="m2"><p>پس این صراط راست که گفتم تراکجاست</p></div></div>
<div class="b2" id="bn72"><p>جاریست از هدایت مولای هشتیمن</p></div>
<div class="b" id="bn73"><div class="m1"><p>دارد بحق براستی این راه اتصال</p></div>
<div class="m2"><p>باقی دگر تمام بود غفلت و ضلال</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>آن کو براه راست نزد گام لامجال</p></div>
<div class="m2"><p>گمراه و غافلست تو خوانش مضل و ضال</p></div></div>
<div class="b2" id="bn75"><p>پس دلبری نمای ز مغضوب و ضالین</p></div>
<div class="b" id="bn76"><div class="m1"><p>ای طالب طریق هدایت بلا کلام</p></div>
<div class="m2"><p>جویای اولیا نشود نطفه حرام</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>داری تو چون بجستن راه حق اهتمام</p></div>
<div class="m2"><p>بیشک ز شیر پاک دلت دیده انفطام</p></div></div>
<div class="b2" id="bn78"><p>میکن به مادر و پدر خویش آفرین</p></div>
<div class="b" id="bn79"><div class="m1"><p>شکر خدا که بنده پیران رهبرم</p></div>
<div class="m2"><p>در ملک فقر صاحب اکلیل و افسرم</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>در آستان پیر مغان خاک شد سرم</p></div>
<div class="m2"><p>روشندل از تجلی انوار حیدرم</p></div></div>
<div class="b2" id="bn81"><p>و اعجاز موسویست هزارم در آستین</p></div>
<div class="b" id="bn82"><div class="m1"><p>سری که از کلیم حقش داشت مکتتم</p></div>
<div class="m2"><p>و آندم که بر مسیح نزد زان صریح دم</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>وان یم نداد هیچ کس نیم قطره نم</p></div>
<div class="m2"><p>از ما نداشت پیر طریقت دریغ هم</p></div></div>
<div class="b2" id="bn84"><p>ای مرحبا بغیریت آن غیرت آفرین</p></div>
<div class="b" id="bn85"><div class="m1"><p>ای جان جان عشق که جان جهان ز تست</p></div>
<div class="m2"><p>در جسم ما ز عشق تو گر هست جان ز تست</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>صورت ز ما و معنی روح روان ز تست</p></div>
<div class="m2"><p>گفتم غلط چه باز که این از من آن ز تست</p></div></div>
<div class="b2" id="bn87"><p>بادم زبان بریده هم آن از تو و هم این</p></div>
<div class="b" id="bn88"><div class="m1"><p>من کیستم که دم زنم از نیست یا که هست</p></div>
<div class="m2"><p>معدوم محض ای ز تو عالی هر آنچه پست</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>الطاف خسروانه محمودی تو هست</p></div>
<div class="m2"><p>کز ما فتادگان مذلت گرفته دست.</p></div></div>
<div class="b2" id="bn90"><p>تا نگذرم ز قصه چاروق و پوستین</p></div>
<div class="b" id="bn91"><div class="m1"><p>هیچیم ما و هیچتر از هیچ در بسیچ</p></div>
<div class="m2"><p>آید چه ای کریم از این مشت هیچ هیچ</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>با آنکه تو بتوست ز ما جرم پیچ پیچ</p></div>
<div class="m2"><p>بر جرممان مگیر و بر افعالمان مپیچ</p></div></div>
<div class="b2" id="bn93"><p>یعنی مردان ز درگه احسانمان چنین</p></div>
<div class="b" id="bn94"><div class="m1"><p>بی علتی ز فقر چو دادی تو نعمتی</p></div>
<div class="m2"><p>از ما مگیر داده خود را بعلتی</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>جزجرم گر چه هیچ نکردیم خدمتی</p></div>
<div class="m2"><p>گر ما مقصریم تو دریای رحمتی</p></div></div>
<div class="b2" id="bn96"><p>اغفرلنا بفضلک یا‌رب آمین</p></div>
<div class="b" id="bn97"><div class="m1"><p>از ما مجو حساب که سرمایه گشت چون</p></div>
<div class="m2"><p>ما را بس خجلت بسیار خود کنون</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>داریم گر نه سود زیانستمان فزون</p></div>
<div class="m2"><p>تا چون کند عطای تو ای شاه ذوفنون</p></div></div>
<div class="b2" id="bn99"><p>با جان بندگان زیانکار مستکین</p></div>