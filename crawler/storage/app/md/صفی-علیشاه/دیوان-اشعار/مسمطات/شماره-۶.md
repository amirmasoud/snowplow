---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>ای ترک چه باشد دگرت باز بهانه</p></div>
<div class="m2"><p>کامروز برون آمده‌ای مست ز خانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگشوده هنو چشم ز مستی شبانه</p></div>
<div class="m2"><p>بر رخ نزده آب و به گیسو گل و شانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نابسته کمر گشته‌ای از خشم روانه</p></div>
<div class="m2"><p>تیرت دو سه در مشت و کمان از بر شانه</p></div></div>
<div class="b2" id="bn4"><p>تنگ از چه شدت حوصله با کیست تو را جنگ</p></div>
<div class="b" id="bn5"><div class="m1"><p>بخرامی و شائی بخرامیدن از آداب</p></div>
<div class="m2"><p>افکنده بر ابرو بگیسو کره و تاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشمی که فتد خیره بر آن نرگس پرخواب</p></div>
<div class="m2"><p>وان عارض خوی کرده و آن خال سیه‌ناب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا چیست که جاریستش از هر مژه خوناب</p></div>
<div class="m2"><p>در ره مگر آن خون که توریزی بود از آب</p></div></div>
<div class="b2" id="bn8"><p>در بر مگر آن دل که توداری بود از سنگ</p></div>
<div class="b" id="bn9"><div class="m1"><p>گر هیچ بدل بردن خلقت نبود دل</p></div>
<div class="m2"><p>گیری چه کمند از خم گیسوی با نامل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ور خود بخرابی نبود چشم تو مایل</p></div>
<div class="m2"><p>از چیست که بر جنگ چو مردان مقتال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بندد صف مژگان و بتازد بقبایل</p></div>
<div class="m2"><p>و انخال سیه یکتنه آید بمقابل</p></div></div>
<div class="b2" id="bn12"><p>بر راکب ور اجل همه گیرد سر ره تنگ</p></div>
<div class="b" id="bn13"><div class="m1"><p>چندار که بود لعل روانبخش تو خاموش</p></div>
<div class="m2"><p>بی‌پرده نگوئی بکس اسرار خود ار هوش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کاسرار شود فاش چو از لب شنود گوش</p></div>
<div class="m2"><p>پیداست از آنگردش چشم و علم دوش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کاندر پی تاراجی بی‌پرده و روپوش</p></div>
<div class="m2"><p>وین بس عجب آید که در آن طره بناگوش</p></div></div>
<div class="b2" id="bn16"><p>ماند بشه روم که لشگر کشد از رنگ</p></div>
<div class="b" id="bn17"><div class="m1"><p>بر پای تو ریزیم چو ما جان به ارادت</p></div>
<div class="m2"><p>حاجت چو بخونریزی و آشوب و جلادت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ور زانکه ترا این بود اندیشه و عادت</p></div>
<div class="m2"><p>زی شاد و بزه‌ساز کمانرا بر شادت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نازیم بر آن پنجه و بازو بزیادت</p></div>
<div class="m2"><p>بر آنکه خورد تیر تو بدهیم شهادت</p></div></div>
<div class="b2" id="bn20"><p>کاو بر همه عشاق بود سرور و سرهنگ</p></div>
<div class="b" id="bn21"><div class="m1"><p>آنکس که شود کشته ز تیغ تو گلندام</p></div>
<div class="m2"><p>کارش بود از جمله عشاق تو بر کام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>رو کرده بر او بخت پسندیده ز ایام</p></div>
<div class="m2"><p>روزی رسد آیا بمن از لعل تو پیغام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر قتل صفی باش که فردا کنم اقدام</p></div>
<div class="m2"><p>یابد دلم از وعده فردای تو آرام</p></div></div>
<div class="b2" id="bn24"><p>چندان که بود وعده خوبان همه نیرنگ</p></div>
<div class="b" id="bn25"><div class="m1"><p>هیچت بود این یاد که گفتی تو مرا کی</p></div>
<div class="m2"><p>کآیم بسرای تو شبی یکدل و یک پی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا صبح در آغوش تو چون نشاه که درمی</p></div>
<div class="m2"><p>مانم کنم این قصه هجران تو را طی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>باشد که بود آن شب یلد از مه دی</p></div>
<div class="m2"><p>کاین عده بپایم نه فرامش کنم از وی</p></div></div>
<div class="b2" id="bn28"><p>وین راز مگو هیچ بکس غیر نی و چنگ</p></div>
<div class="b" id="bn29"><div class="m1"><p>یعنی که بدل گوی و بدل‌دار که دلدار</p></div>
<div class="m2"><p>داده است مرا وعده دلداری و دیدار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تابو که مگر خانه بپردازد ز اغیار</p></div>
<div class="m2"><p>وین راز نگوید بکس الا که بود یار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا سر ندهد نیست کسی محرم اسرار</p></div>
<div class="m2"><p>منصور که شد محرم اسرار هم از دار</p></div></div>
<div class="b2" id="bn32"><p>گردید ز گفتن سر ببریده‌اش آونگ</p></div>
<div class="b" id="bn33"><div class="m1"><p>این وعده بدل دادم و او بود خود آگاه</p></div>
<div class="m2"><p>بنشست مراقب همه شب تا بسحرگاه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چون منتظران بود نگاهش سوی درگاه</p></div>
<div class="m2"><p>کاید بورود تو مگر مژده از راه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آن وعده که دادی چه بدی بود بدلخواه</p></div>
<div class="m2"><p>دل داشت حساب دی و تموز بهر ماه</p></div></div>
<div class="b2" id="bn36"><p>تا کی به اسد جای کند شمس چو خرچنگ</p></div>
<div class="b" id="bn37"><div class="m1"><p>بس دی شد و اُردی شد و شعبان شد و شوال</p></div>
<div class="m2"><p>بگذشت بسی هفته بسی ماه بسی سال</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یادت به نیامد یک از آن عده بمنوال</p></div>
<div class="m2"><p>وین نیست شگفتی که ز خوبان بود اهمال</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در وعده و میثاق که بندند با جمال</p></div>
<div class="m2"><p>با آنکه نظیر تو محال است بهر حال</p></div></div>
<div class="b2" id="bn40"><p>در راستی عهد و وفاداری و فرهنگ</p></div>
<div class="b" id="bn41"><div class="m1"><p>گفتم منت آنروز تو خرم زی و خرسند</p></div>
<div class="m2"><p>بادت شجر حسن ثمربخش و برومند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بر وعده خوبان نتوان بست دلی چند</p></div>
<div class="m2"><p>چندان به نباشند چو بر وعده خود بند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>عهدی که نمایند نپایند به پیوند</p></div>
<div class="m2"><p>گفتی تو که بر موی من و مهر تو سوگند</p></div></div>
<div class="b2" id="bn44"><p>کاندیشه دیگر نکنم یارم و یکرنگ</p></div>
<div class="b" id="bn45"><div class="m1"><p>امروز که از خانه برون آمده باز</p></div>
<div class="m2"><p>داری سر غوغا و کنی عربده آغاز</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>وز غایت مستی نکنی چشم بکس باز</p></div>
<div class="m2"><p>با عالم و آدم نشوی همدم و همراز</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>باشد به زمین و به زمانت بروش ناز</p></div>
<div class="m2"><p>زیبد بتو خوانندت اگر خانه برانداز</p></div></div>
<div class="b2" id="bn48"><p>زینرو که بود جمله بخونریزیت آهنگ</p></div>
<div class="b" id="bn49"><div class="m1"><p>اینسان که برون آمده مست و جلوگیر</p></div>
<div class="m2"><p>دانم چه بسر داری از اندیشه و تدبیر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>خواهی که کنی ملک جهان را همه تسخیر</p></div>
<div class="m2"><p>خواهد شدن این زودترا حاصل نی دیر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گیسو چو گشائی کنی از دوش سرازیر</p></div>
<div class="m2"><p>و ابرو بخم آری نبود حاجت شمشیر</p></div></div>
<div class="b2" id="bn52"><p>چین‌گیری و بیرق زنی از روم به افرنگ</p></div>
<div class="b" id="bn53"><div class="m1"><p>بازم بود امید بر الطاف خدائی</p></div>
<div class="m2"><p>مانا که برأفتد ز میان نام جدائی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>با آن همه نازت بفقیران فدائی</p></div>
<div class="m2"><p>باز آئی و از دل کنیم عقده گشائی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>می‌نوشی‌ و سرپوشی و گل بوئی و سائی</p></div>
<div class="m2"><p>غم‌سوزی و جان‌بخشی و دلجوئی و شائی</p></div></div>
<div class="b2" id="bn56"><p>بزدانیم از آئینه دل بوفا زنگ</p></div>
<div class="b" id="bn57"><div class="m1"><p>از دامن آلوده اگر داری پرهیز</p></div>
<div class="m2"><p>لعل تو می‌آلوده و خونخواره بود نیز</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>من دانم و دل نکته آن لعل دلاویز</p></div>
<div class="m2"><p>پیش لب شیرین تو شکر نبود چیز</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>افسانه دگر هیچ بخوانند ز پرویز</p></div>
<div class="m2"><p>کو کرد شبی سیر مهمی با پی‌شبدیز</p></div></div>
<div class="b2" id="bn60"><p>روزی که برانگیزی گرد از سم شبرنگ</p></div>
<div class="b" id="bn61"><div class="m1"><p>گر دیده به آلودگی ار دامن من چاک</p></div>
<div class="m2"><p>جز چاک شدن را نسزد دامن بی‌باک</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>دامان تو المنته لله که بود پاک</p></div>
<div class="m2"><p>گو پاک بو و دامن و دلق و عنب و تاک</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>کز زاده او چونکه به پیوست بادراک</p></div>
<div class="m2"><p>در عشق تو گردد خرد آزاده و چالاک</p></div></div>
<div class="b2" id="bn64"><p>نه نام در اندیشه بجا ماند نه ننگ</p></div>
<div class="b" id="bn65"><div class="m1"><p>ور آنکه بود عارت از اندیشه درویش</p></div>
<div class="m2"><p>ز آمیزش درویش شود فرشهان بیش</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>وین رسم جدیدی نبود‌، بوده است از پیش</p></div>
<div class="m2"><p>شاهان عدالت روش عاقبت اندیش</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بودند به درویشی خود مفتخر از کیش</p></div>
<div class="m2"><p>نازیم بر آن دولت بی‌آفت و تشویش</p></div></div>
<div class="b2" id="bn68"><p>در خاک نشینی نه که بر شاهی واورنگ</p></div>
<div class="b" id="bn69"><div class="m1"><p>و آنکه بد از من بتو گویند خلایق</p></div>
<div class="m2"><p>کو نیست بر آئینی و بر کیشی واثق</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>در طبع من آن نیست بسی غیر موافق</p></div>
<div class="m2"><p>دانی تو خود این حال بتحقیق که عاشق</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>هرگز نبود در خورش آئین و علائق</p></div>
<div class="m2"><p>عشق است مرا کیش و بر این کیشم لایق</p></div></div>
<div class="b2" id="bn72"><p>اینست مرا راه و نیم بند بخرسنگ</p></div>
<div class="b" id="bn73"><div class="m1"><p>با این همه عیبی که مرا هست و تو دانی</p></div>
<div class="m2"><p>پرسم سخنی از تو خدا را بنهانی</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>بر گونه زر و داری و سروی و گرانی</p></div>
<div class="m2"><p>از اهل خرابات ومناجات و معانی</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>داری چو من آیا بحقیقت نه زبانی</p></div>
<div class="m2"><p>دل باخته بر سر کویت بنشانی</p></div></div>
<div class="b2" id="bn76"><p>کش بر زده باشی تبر از روی وفا سنگ</p></div>
<div class="b" id="bn77"><div class="m1"><p>آن سان که تو بی‌مثلی و مانند در آفاق</p></div>
<div class="m2"><p>در حسن و برازندگی و پاکی و اخلاق</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>من نیز بگیتی مثلم در همه عشاق</p></div>
<div class="m2"><p>در رندی و چالاکی و قلاشی‌ و میثاق</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>مانا شده بر عشق من از حسن تو اشراق</p></div>
<div class="m2"><p>در حسن تو بیجفتی و در عشق تو من طاق</p></div></div>
<div class="b2" id="bn80"><p>من بر دل رستم تو بهشیاری هوشنگ</p></div>
<div class="b" id="bn81"><div class="m1"><p>این شکوه ز بخت است نه زان خصلت نیکو</p></div>
<div class="m2"><p>نبود بجهان خلق و خصالی به از آنخو</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>از خوی تو شاکی نتوان بودن یک مو</p></div>
<div class="m2"><p>زخم تو بدل به بود از مرهم و دارو</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>درد تو بجان می‌خرم آرد به من ار رو</p></div>
<div class="m2"><p>شمشیر بمن برکش در هم مکش ابرو</p></div></div>
<div class="b2" id="bn84"><p>آتش بسرم ریز و میفکن برخ آژنگ</p></div>
<div class="b" id="bn85"><div class="m1"><p>ایا تو نگفتی بمن اندر سر پیمان</p></div>
<div class="m2"><p>خواهم که تو برخیزی در عشق من از جان</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>آیا نفکندم سر و جان جمله به میدان</p></div>
<div class="m2"><p>آیا نگذشتم برهت از سر و سامان</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>آیا ننهادم ز غمت سر به بیابان</p></div>
<div class="m2"><p>آیا نرساندم ره عشق تو بپایان</p></div></div>
<div class="b2" id="bn88"><p>آیا نزدم کام بهر منزل و فرسنگ</p></div>
<div class="b" id="bn89"><div class="m1"><p>یک مایه سخن بجا مانده گویم و بر‌جاست</p></div>
<div class="m2"><p>آشفته نگردد دلت آشفتگی از ماست</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>آشفته دل ما تو کنی وین ز تو زیبا است</p></div>
<div class="m2"><p>ما را به دعای تو بود دست و هویداست</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>دانی تو خود این نکته که ما را چه تمناست</p></div>
<div class="m2"><p>زین دست که بر دامن مولی بتولاست</p></div></div>
<div class="b2" id="bn92"><p>بر ذیل و لایش زده‌ایم از دو جهان چنگ</p></div>
<div class="b" id="bn93"><div class="m1"><p>آن صاحب شمشیر دو دم خواجه قنبر</p></div>
<div class="m2"><p>کز تیغ وی آمد ملک و ملک مسخر</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>نامش بستم دیده و افتاده و مضطر</p></div>
<div class="m2"><p>افسون مجرب بود و عون میسر</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>هر مشگلی آسان شود از عونش یکسر</p></div>
<div class="m2"><p>دارند بر این تجربه زندان قلندر</p></div></div>
<div class="b2" id="bn96"><p>نکنند بکاری بجز از نام وی آهنگ</p></div>
<div class="b" id="bn97"><div class="m1"><p>تریاق سموم الم و دافع هر زهر</p></div>
<div class="m2"><p>حلال هر آن مشکل در باطن و در جهر</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>خوانند چو نامش بهر آن وادی و هر شهر</p></div>
<div class="m2"><p>جاری شود از سنگ بهر تشنه دو صد نهر</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>فریاد رس هر که رسیدش ستم دهر</p></div>
<div class="m2"><p>پس گشت پناهنده به آن غالب ذوالقهر</p></div></div>
<div class="b2" id="bn100"><p>کارش نشد از همت او یکسر مولنگ</p></div>
<div class="b" id="bn101"><div class="m1"><p>ای آنکه براهت بود افهام چو خاشاک</p></div>
<div class="m2"><p>تا فهم کمالت چکند دانش و ادراک</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>چندار که حمام است برازنده و چالاک</p></div>
<div class="m2"><p>هرگز نتواند پرد از بام بافلاک</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>هم عقل ز آلایش اگر چند بود پاک</p></div>
<div class="m2"><p>ز ادراک صفات تو بود همسر با خاک</p></div></div>
<div class="b2" id="bn104"><p>زین رتبه خرد خام و بیان پست و روان دنگ</p></div>
<div class="b" id="bn105"><div class="m1"><p>باشد بدل امید مراکز کرم پیر</p></div>
<div class="m2"><p>ترک ار شده اولی و پدید آمده تقصیر</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>بخشی د شود آنچه نبوده است به تقدیر</p></div>
<div class="m2"><p>یعنی که بد از ماست نه زان قدرت وتأثیر</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>خلقند بهر جنبشی ار چند قضاگیر</p></div>
<div class="m2"><p>وانرا که کند کلک قضا نقش ز تغییر</p></div></div>
<div class="b2" id="bn108"><p>در راست گراسپید بود یا که سیه رنگ</p></div>
<div class="b" id="bn109"><div class="m1"><p>ننگاشت یکی کلک نگارنده خطی کج</p></div>
<div class="m2"><p>کوته نظران بینند ار چند که معوج</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>تا باشد از افکار نگارنده چو منتج</p></div>
<div class="m2"><p>حرف آمده بیرون همه بستوده ز مخرج</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>با این همه رفتار کج از ماست به منهج</p></div>
<div class="m2"><p>ز الطاف کریمان بود آمال مروج</p></div></div>
<div class="b2" id="bn112"><p>تا بود که بر افتاده نگیرند دمی تنگ</p></div>