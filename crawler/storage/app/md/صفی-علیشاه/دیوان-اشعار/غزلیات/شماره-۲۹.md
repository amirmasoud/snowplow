---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>در عشق تو آن بهره که حاصل شود آخر</p></div>
<div class="m2"><p>خونیست که جاری برخ از دل شود آخر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مژگان توصف بسته چنین بی‌سببی نیست</p></div>
<div class="m2"><p>بر هم زن صد قوم و قبایل شود آخر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر زلف تو دلبستگی ما بخطا نیست</p></div>
<div class="m2"><p>دیوانه گرفتار سلاسل شود آخر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اول بغمت سهل سپردم دل و غافل</p></div>
<div class="m2"><p>کز زلف تو کارم همه مشکل شود آخر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز فکر تو از خاطر و جز یاد تو از دل</p></div>
<div class="m2"><p>چون نقش بر آبست که زایل شود آخر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر طعنه که زد خال تو بر هستی عشاق</p></div>
<div class="m2"><p>سهلست اگر حل مسائل شود آخر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در راه غمت جان بسپردیم و روا بود</p></div>
<div class="m2"><p>گر در طلبت طی مراحل شود آخر</p></div></div>