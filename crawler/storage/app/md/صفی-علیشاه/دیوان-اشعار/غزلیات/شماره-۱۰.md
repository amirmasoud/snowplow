---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>باذی نفسی نیست که او یکنفسی نیست</p></div>
<div class="m2"><p>شد با همه کس تا که نگوید کسی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر زنده دلی دل ز مسیحا نفسی یافت</p></div>
<div class="m2"><p>آنرا نفسی نیست که عیسی نفسی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالم همگی پرتو آن طلعت زیباست</p></div>
<div class="m2"><p>موسی نظری نیست که روشن قبسی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاهد نبود آگه از اندیشه عشاق</p></div>
<div class="m2"><p>هم فکرت عنقا بمعانی مگسی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد قافله پیدا و از ایشان رسد آواز</p></div>
<div class="m2"><p>کس هیچ نیازش بصدای جرسی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان از باختگان واقف از اندازه عشقند</p></div>
<div class="m2"><p>دریا سپری در خور هر خار و خسی نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در کشمکش عشق بود عقل شهان مات</p></div>
<div class="m2"><p>این بازی و برد از پی فیل و فرسی نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق تو بدان مایه که از دل برود غیر</p></div>
<div class="m2"><p>سوزیست که در سینه هر بوالهوسی نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر دین و دل اندر خم زلف تو شد از دست</p></div>
<div class="m2"><p>ترک دل و دین بر سر آن طره بسی نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باشد که بپای ت نهم سر بارادت</p></div>
<div class="m2"><p>چند ار که بوصل تو مرا دسترسی نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هشیار ز چشم تو در این شهر نمانده است</p></div>
<div class="m2"><p>اینست که اندر پی مستان عسسی نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از خرمن دنیا نخورد گندمی آنمرد</p></div>
<div class="m2"><p>کاین دور فلک در نظرش یکعدسی نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در عشق تو هر کس بتمنائی و حالی است</p></div>
<div class="m2"><p>غیر از تو صفی را بصفا ملتمسی نیست</p></div></div>