---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>دو چشم مست تو برشان یکدگر گوهند</p></div>
<div class="m2"><p>که رهزن دل و دین از اشاره و نگهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کمان کشیده بدل بستی ار که ره چه عجب</p></div>
<div class="m2"><p>که ابروان تو هر یک حریف صد سپهند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگیر خورده خدارا بعقل و دانش من</p></div>
<div class="m2"><p>که ذکر زلف تو چون رفت این و آن تبهند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو لعبتی تو نگارا که گلرخان جهان</p></div>
<div class="m2"><p>به پیش روی اصیلت براستی شبهند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من از غمت نه ببیت‌الحزن نشستم و بس</p></div>
<div class="m2"><p>چه یوسفان که ز عشق رخت اسیر چهند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به آن امید که گیرند دامن تو کف</p></div>
<div class="m2"><p>نشسته بر سر راهت شهان چو خاک رهند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زجان سبوی خراباتیان کشند بدوش</p></div>
<div class="m2"><p>ببوی وصل تو آنان که یار خانقهند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بغمزه تو سپردم روان و دل بلبت</p></div>
<div class="m2"><p>بخون این دو گواهند و خویش بیگنهند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مکن ملامتم ار ره مقصدی نرسید</p></div>
<div class="m2"><p>که دام راهروان آن دو طره سیهند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صفای عشق صفی از حریم میکده جو</p></div>
<div class="m2"><p>که ساکنان درش نور بخش مهر و مهند</p></div></div>