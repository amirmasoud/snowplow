---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>ای صفی معشوقت آخر دیدی اندرخانه بود</p></div>
<div class="m2"><p>بر سراغش گرد عالم گشتنت افسانه بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاهدی کآواز او از کعبه می‌آمد بگوش</p></div>
<div class="m2"><p>عشق بردم بر نشانش مست در میخانه بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان بت بی‌پرده پوشد ار که شیخ شهر چشم</p></div>
<div class="m2"><p>عذر او خواهم من از پیرمغان بیگانه بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاهد ار پنداشت با تسبیح او گردد سپهر</p></div>
<div class="m2"><p>بیخبر زان چشم مست و گردش پیمانه بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز آدم را سیاه آنخال مشکین کرد و عقل</p></div>
<div class="m2"><p>بر گمان افتاد کان دلبردگی از دانه بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دود او در سوختن می‌کرد ظاهر حال شمع</p></div>
<div class="m2"><p>کاین شرر پنهان نه تنها در دل پروانه بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از صفی جو داری ار گمگشته در راه عشق</p></div>
<div class="m2"><p>زانکه در زنجیر زلفش سال‌ها دیوانه بود</p></div></div>