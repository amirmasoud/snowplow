---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>در طواف حرمم گفت بگوش آگاهی</p></div>
<div class="m2"><p>حلقه میکده را هم به ادب زن گاهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بینی از مروه میخانه صفای رخ دوست</p></div>
<div class="m2"><p>گر کنی سعی و در آن حلقه بیابی راهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست در صومعه سودی بخرابات گرای</p></div>
<div class="m2"><p>قلب خود نقد کن از صحبت صاحب جاهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عرفاتیست در دوست که عشاق رسند</p></div>
<div class="m2"><p>اندران کوی نه هر بیخبری خود‌خواهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همت پیر مغان بین که ز رندان همه عیب</p></div>
<div class="m2"><p>دید و پوشید و در اکرام نکرد اکراهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جو ز عشاق کرم زاهد بیچاره گداست</p></div>
<div class="m2"><p>بکف آور گهر از مخزن شاهنشاهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هیچ سائل ز در میکده محروم نرفت</p></div>
<div class="m2"><p>بکجا کرد توان رو ز چنین درگاهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوشه از خرمن صاحب کرمی بر که بقدر</p></div>
<div class="m2"><p>پیش او حاصل کونین کمست از کاهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یوسف مصر معانی توئی آخر ز چه روی</p></div>
<div class="m2"><p>می‌کنی عمر گرانمایه تلف در چاهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روز خود بی می و معشوق مکن شب همه عمر</p></div>
<div class="m2"><p>خردسال است که یک هفته بود بی‌ماهی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قدمی هم بصفا بر در میخانه گذار</p></div>
<div class="m2"><p>تا مگر دست تو گیرند صفی اللهی</p></div></div>