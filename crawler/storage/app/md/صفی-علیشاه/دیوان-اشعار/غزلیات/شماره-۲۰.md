---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>داشتم چشم بعهدی که کند یار بماند</p></div>
<div class="m2"><p>قدر حسن خود و عشق من درویش بداند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه خوبان به نمانند یکی بر سر پیمان</p></div>
<div class="m2"><p>بودم امید که او عهد بآخر برساند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زانکه حسن و ادب و شاهی و درویشی و دانش</p></div>
<div class="m2"><p>نگذارد که بافتاده کند هر چه تواند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غافل از آنکه جوانست و مرا این سر پیری</p></div>
<div class="m2"><p>برسن بندد و هر سو پیِ بازی بدواند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لیک با این همه دانم که بجای من بی‌دل</p></div>
<div class="m2"><p>دیگری را نگزیند که بپهلو بنشاند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زانکه داند چو صفی نیست یکی در همه عالم</p></div>
<div class="m2"><p>که بود قابل مهرش خدائیش بخواند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بخدا خاک رهش را بدو گیتی نفروشم</p></div>
<div class="m2"><p>کودکست آنکه دهد گوهر و جوزی بستاند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه چون من یار پرستی که دهم دست بعهدش</p></div>
<div class="m2"><p>نه چو او دوست نوازی که زبندم برهاند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کند ار دلبری از من بود از راه ارادت</p></div>
<div class="m2"><p>ور نه گردش شود ار چرخ ز دامن بفشاند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غزلی دوش فرستاد وحیدالحق والدین</p></div>
<div class="m2"><p>منش این نام نهادم که بتوحید بماند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بود این نیز جواب غزل حضرت عبدی</p></div>
<div class="m2"><p>بخت با اوست مساعد که بیارش بکشاند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو بهر پر که گشائی دم سیمرغ ببندی</p></div>
<div class="m2"><p>شاهبازی و که شاهت ز پی صید پراند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هیچکس جان خود از تن نپراند پی‌صیدی</p></div>
<div class="m2"><p>فلکش زهر اجل گر بپراند بچشاند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نفسی گر بخدا از بر من دور نشینی</p></div>
<div class="m2"><p>غم هجرن تو این قالب خاکی بدراند</p></div></div>