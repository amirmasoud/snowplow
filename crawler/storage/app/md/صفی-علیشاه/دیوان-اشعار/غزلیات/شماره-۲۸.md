---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>ای حسین ابن علی از باطن پاکت مدد</p></div>
<div class="m2"><p>و ز دم عشاق و جان مست سلاکت مدد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست قلب یار من آنسان که باید بسوی من</p></div>
<div class="m2"><p>بهر جذب قلب او از روح چالاکت مدد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاکساری کوی عشق تست جای جن و انس</p></div>
<div class="m2"><p>تا شود معشوق من خاک من از خاکت مدد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عقلها حیران عشق تست و عقل یار من</p></div>
<div class="m2"><p>تا شود حیران من از عقل و ادراکت مدد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر سری را هست شوری بسته فتراک تست</p></div>
<div class="m2"><p>تافتد شور منش در سر ز فتراکت مدد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا ز غیر من بپوشد چشم امید وصال</p></div>
<div class="m2"><p>از دل حق بین و دست و دیده پاکت مدد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز تو کس در عشق حق امساک از هستی نکرد</p></div>
<div class="m2"><p>تاکند او ترک غیر از من ز امساکت مدد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر نبودی تو نبود از عشق در عالم ثمر</p></div>
<div class="m2"><p>خواهم اندر جذب یار از سر لولاکت مدد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گشت عالی عرش و افلاک از علو همتت</p></div>
<div class="m2"><p>بر مرادی کن مرا از دور افلاکت مدد</p></div></div>