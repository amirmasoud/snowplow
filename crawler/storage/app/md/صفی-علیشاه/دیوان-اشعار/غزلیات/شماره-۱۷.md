---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>دل غمدیده به تنهائی هجران خو کرد</p></div>
<div class="m2"><p>تکیه بر زلف تو و باد و جهان یکرو کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم آنروز که دل نکته ز خال تو شنید</p></div>
<div class="m2"><p>رخنه در کار مسلمانیم این هندو کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه عجب گر شب عاشق بغلط گشت سحر</p></div>
<div class="m2"><p>زان دو رنگی که بنا گوش تو در گیسو کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست جای گله در زلف توأم یکسر مو</p></div>
<div class="m2"><p>کآنچه کرد او بسیه روزی ما نیکو کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همسری چون سر شوریده بعشق تو نیافت</p></div>
<div class="m2"><p>شرح سودای غمت را همه بازانو کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عقل باریک بسی گشت و میان تو ندید</p></div>
<div class="m2"><p>دیده حس بخطا رفت که فهم از مو کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل تنگم ز دهان تو نشان هیچ نیافت</p></div>
<div class="m2"><p>خورده کم گیر که اندیشه موهوم او کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دارد افسون مسیحالب جانبخش تو لیک</p></div>
<div class="m2"><p>حمل این معجزه را چشم تو بر جادو کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عجب آن نیست که زلف تو ز دل دست ببرد</p></div>
<div class="m2"><p>عجب آنست که با زلف تو دل بازو کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دوش میرفتی و ماه رخت از روزن جان</p></div>
<div class="m2"><p>پرتو افکن شد و ویرانه ما مینو کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیک طرزیست که آیدرمت از مردم شهر</p></div>
<div class="m2"><p>زان قیاس نگهت بی‌بصر از آهو کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قامت سرو تو زاندم که روان گشت بچشم</p></div>
<div class="m2"><p>آبیارم نتوان فرق کنار از جو کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر چه مژگان تو در فتنه صف آراست ولی</p></div>
<div class="m2"><p>کارپردازی چشمت همه را ابرو کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا چه دردی بدل از سنبلت ای غالیه موست</p></div>
<div class="m2"><p>گشت دیوانه طبیبی که دهانم بو کرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>روز‌ها رفت ز پهلوی صفی دجله چشم</p></div>
<div class="m2"><p>جرم یک شب که تمنای تو در پهلو کرد</p></div></div>