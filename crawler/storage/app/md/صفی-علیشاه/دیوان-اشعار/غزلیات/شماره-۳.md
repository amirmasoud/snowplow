---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>دل نداد از دست یک مو زلف یار خویش را</p></div>
<div class="m2"><p>تا سیه کرد از کشاکش روزگار خویش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اختیاری بهر عاشق نیست در فرمان عشق</p></div>
<div class="m2"><p>تا قلم بکشیم بر سر اختیار خویش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرفتم تا صبح محشر مست از آن چشم خمار</p></div>
<div class="m2"><p>نشکنم جز با همان ساغر خمار خویش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواهم اندر خیل جانبازان نیازندم به نام</p></div>
<div class="m2"><p>بینم اندک چون به راه او نثار خویش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی‌قرار آن زلف مشکین را هر آن بیند به دوش</p></div>
<div class="m2"><p>می‌دهد بر بی‌قراری‌ها قرار خویش را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زاهدان از یاد جنّت مست و ما از عشق یار</p></div>
<div class="m2"><p>هر کسی در بوته سنجد عیار خویش را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تیر مژگانت صفی را بر نشان افکند و خست</p></div>
<div class="m2"><p>بازگیر از خاک چو افکندی شکار خویش را</p></div></div>