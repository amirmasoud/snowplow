---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>در انجمن بخرام آمدی و رو بستی</p></div>
<div class="m2"><p>سخن بپرده سرودی و لب فرو بستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حدیث حسن تو هر کس بیک زبانی گفت</p></div>
<div class="m2"><p>طلسم دلبری خود بگفتگو بستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هوای عشق بدریا دلی توان بستن</p></div>
<div class="m2"><p>سزد ز گریه مراگر بدیده جو بستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم بکشور حسن تو شد بصید نظر</p></div>
<div class="m2"><p>ره برون شدن از خال و خط بر او بستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نزاع زاهد و صوفی با نتزاع تو بود</p></div>
<div class="m2"><p>که نفس خود همه بر خار و گل نکوبستی</p></div></div>