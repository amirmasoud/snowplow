---
title: >-
    شمارهٔ ۱۰۰
---
# شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>گر طالب ره شدی ز مردان سبل</p></div>
<div class="m2"><p>جو راهروی گذشته از جزو ز کل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کن مغز خرد معطر از طیب رسل</p></div>
<div class="m2"><p>در بزم صفی که او گلابیست ز گل</p></div></div>