---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>گویند گناه چونکه پیوسته شود</p></div>
<div class="m2"><p>بر حق در بازگشت ما بسته شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی صد اگر توبه بشکسته شود</p></div>
<div class="m2"><p>حق این نشود که از عطا خسته شود</p></div></div>