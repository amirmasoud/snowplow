---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>گرباده خوری با صنمی زیبا خور</p></div>
<div class="m2"><p>یا با مردی قوی دلی دانا خور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نیست ترا رفیق و یاری همدم</p></div>
<div class="m2"><p>می‌ هیچ مخور و گر خوری تنها خور</p></div></div>