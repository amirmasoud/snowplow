---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>هستی یم و دین کشتی و حیدر ملاح</p></div>
<div class="m2"><p>زین ورطه بود ولای ملاح فلاح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهی اگر آوری بکف گوهر عشق</p></div>
<div class="m2"><p>در بحر ولایت علی شو سباح</p></div></div>