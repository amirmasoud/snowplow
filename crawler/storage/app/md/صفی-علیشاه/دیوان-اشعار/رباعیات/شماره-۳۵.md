---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>یغمای نگاه بین که آن دلبر شوخ</p></div>
<div class="m2"><p>چیزی نگذاشت دیگر از بهر شیوخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل و دل و دین بجمله شد غارت</p></div>
<div class="m2"><p>علم و عمل از اشاره شد منسوخ</p></div></div>