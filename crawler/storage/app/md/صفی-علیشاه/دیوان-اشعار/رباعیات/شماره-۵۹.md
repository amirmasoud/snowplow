---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>هر لحظه صفی حساب ره باید کرد</p></div>
<div class="m2"><p>چاه است بهر قدم نگه باید کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رحمت پی رحمت آید از رب غفور</p></div>
<div class="m2"><p>اما نه گنه پی گنه باید کرد</p></div></div>