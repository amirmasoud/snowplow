---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>زاهد که مواعظش بجز نیش نبود</p></div>
<div class="m2"><p>صوفی که دمی بحالت خویش نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افسوس که مردان قلندر رفتند</p></div>
<div class="m2"><p>گشتیم بسی اثر ز درویش نبود</p></div></div>