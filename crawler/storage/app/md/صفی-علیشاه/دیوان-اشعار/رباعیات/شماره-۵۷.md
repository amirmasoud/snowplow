---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>خوش آنکه حدیث کفر و ایمان نشنید</p></div>
<div class="m2"><p>افسانه کافر و مسلمان نشیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز جام شراب و دست ساقی نشناخت</p></div>
<div class="m2"><p>جز نان نگار و حرف جانان نشنید</p></div></div>