---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>ای آنکه رود بقالب از امر تو روح</p></div>
<div class="m2"><p>از نور صدر اهل معنی مشروح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر قلب صفی ز فتح بابت چه عجب</p></div>
<div class="m2"><p>کابواب معارف تو گردد مفتوح</p></div></div>