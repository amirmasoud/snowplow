---
title: >-
    شمارهٔ ۸۳
---
# شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>ای ذات تو بر جمیع ذرات فیض</p></div>
<div class="m2"><p>ظل کرمت کشیده بر اوج و حضیض</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانست کسی که کارساز همه کیست</p></div>
<div class="m2"><p>یکجا بتو کرد کار خود را تفویض</p></div></div>