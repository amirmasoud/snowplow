---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>گر سالک عارفی و بی‌عیب و عبوس</p></div>
<div class="m2"><p>بدخواه مباش بر مسلمان و مجوس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر خلق نباشد ار ترا طبع کریم</p></div>
<div class="m2"><p>آزرده مباد کز تو گردند نفوس</p></div></div>