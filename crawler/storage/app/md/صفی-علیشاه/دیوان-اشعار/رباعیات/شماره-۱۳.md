---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>تا دل نشود بریده از دلخواهت</p></div>
<div class="m2"><p>نبود بحریم لی مع‌الله راهت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خلق ببند دیده تا باز شود</p></div>
<div class="m2"><p>بر دل در لا اله الا اللهت</p></div></div>