---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>آنانکه براه عقل و برهان رفتند</p></div>
<div class="m2"><p>و آنان که برسم علم و ایمان رفتند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آگاه نگشتند ز اسرار وجود</p></div>
<div class="m2"><p>حیران بجهان شدند و حیران رفتند</p></div></div>