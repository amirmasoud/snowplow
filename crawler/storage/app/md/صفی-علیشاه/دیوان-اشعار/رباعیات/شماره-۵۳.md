---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>هر کس که رهی گزید رهبر نشود</p></div>
<div class="m2"><p>هر حیه دری بدهر حیدر نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی گام پی صفیعلی شاه نهد</p></div>
<div class="m2"><p>تا مرد مجرد و قلندر نشود</p></div></div>