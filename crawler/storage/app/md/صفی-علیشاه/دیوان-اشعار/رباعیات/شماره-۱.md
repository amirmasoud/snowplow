---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>هستی نبود سزای کس غیر خدا</p></div>
<div class="m2"><p>او هستی محض و ما سوا هست نما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هستی ما شروط هستی نایاب</p></div>
<div class="m2"><p>در هستی حق کمال هستی پیدا</p></div></div>