---
title: >-
    شمارهٔ ۹۱
---
# شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>یکرنگ بخم کن فکان زد صباغ</p></div>
<div class="m2"><p>بر پرده خلق را بخود داد سراغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیرنگی خویش یعنی از این همه رنگ</p></div>
<div class="m2"><p>بنمود چو آب از رخ لال بباغ</p></div></div>