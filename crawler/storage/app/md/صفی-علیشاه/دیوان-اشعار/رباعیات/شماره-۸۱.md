---
title: >-
    شمارهٔ ۸۱
---
# شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>در خانه و شهر و خلوت و انجمنش</p></div>
<div class="m2"><p>می‌جویم و نیست در میان جز سخنش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر جا سخنی است می‌دهم دل که مگر</p></div>
<div class="m2"><p>پی از سخنی برم بسر دهنش</p></div></div>