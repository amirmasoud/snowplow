---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>ا‌رب چو من ار گناهکاری باشد</p></div>
<div class="m2"><p>غفران ترا در انتظاری باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عفوت ز پی گناهکاران گردد</p></div>
<div class="m2"><p>چون یار که در سراغ یاری باشد</p></div></div>