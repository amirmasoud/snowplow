---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>در صرف وجود فرق و تمیزی نیست</p></div>
<div class="m2"><p>وز غیر که منفی است پرهیزی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یعنی نبود خدا یرا مثل و شریک</p></div>
<div class="m2"><p>هستی همه اوست غیر او چیزی نیست</p></div></div>