---
title: >-
    شمارهٔ ۸۰
---
# شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>جز پوست خود صفی بتن خرقه مپوش</p></div>
<div class="m2"><p>وندر طلب روزی مقسوم مکوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز بر سر سفره توکل منشین</p></div>
<div class="m2"><p>می جز ز کدوی حسبی الله منوش</p></div></div>