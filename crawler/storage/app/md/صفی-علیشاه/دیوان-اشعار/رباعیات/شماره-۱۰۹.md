---
title: >-
    شمارهٔ ۱۰۹
---
# شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>با نیک و بد زمانه نزدیک مشو</p></div>
<div class="m2"><p>نیکی کن و در پی بد و نیک مشو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سر وجود زیر کان بس گشتند</p></div>
<div class="m2"><p>سر رشته نیافت کس تو باریک مشو</p></div></div>