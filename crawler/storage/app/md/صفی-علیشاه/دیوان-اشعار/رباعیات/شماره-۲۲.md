---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>یک نکته بگویمت بتحقیق بسنج</p></div>
<div class="m2"><p>گر عاقل و کاملی مرنجان و مرنج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رنجاندن خلق و رنجشت از طمع است</p></div>
<div class="m2"><p>بگذر ز طمع که این به است از صد گنج</p></div></div>