---
title: >-
    شمارهٔ ۱۱۰
---
# شمارهٔ ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>دام است جهان صفی پی‌دانه مرو</p></div>
<div class="m2"><p>قانع بنشین و خانه بر خانه مرو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رزق تو رسد ز غیب بی‌منت خلق</p></div>
<div class="m2"><p>و زخلق که عاجزند و افسانه مرو</p></div></div>