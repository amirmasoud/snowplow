---
title: >-
    شمارهٔ ۱۱۶
---
# شمارهٔ ۱۱۶

<div class="b" id="bn1"><div class="m1"><p>ای آنکه بروز محنتم یار توئی</p></div>
<div class="m2"><p>براین همه عیب و نقص ستار توئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر عمر و عمل تمام شد صرف گناه</p></div>
<div class="m2"><p>امید بر آن بود که غفار توئی</p></div></div>