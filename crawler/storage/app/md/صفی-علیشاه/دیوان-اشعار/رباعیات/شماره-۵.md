---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>اثنی عشری ز زاده بوطالب</p></div>
<div class="m2"><p>تا مهدی منتظر امام غائب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حب همه را بخویش میدان واجب</p></div>
<div class="m2"><p>تا فوز عظیمت رسد از هر جانب</p></div></div>