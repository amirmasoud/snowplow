---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>گر راه روی تجاوز از خط صراط</p></div>
<div class="m2"><p>حاشا که بتفریط کنی یا افراط</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>توحید ره است و شرک اضافات طریق</p></div>
<div class="m2"><p>ره صاف شد ار اضافه‌ات شد اسقاط</p></div></div>