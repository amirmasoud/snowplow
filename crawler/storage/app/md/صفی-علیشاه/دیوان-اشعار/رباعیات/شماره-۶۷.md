---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>ای نار خدای پاک و بیمثل و نظیر</p></div>
<div class="m2"><p>افتاده سرم زبار عصیان برزیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جرمم تو بجمع رحمت خویش ببخش</p></div>
<div class="m2"><p>دستم تو بدست قدرت خویش بگیر</p></div></div>