---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>ای غمت اصل مدعای وجود</p></div>
<div class="m2"><p>وی ز جودت بپا لوای وجود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کو وجودی بجز تو تا که کند</p></div>
<div class="m2"><p>وحدتی ثابت از برای وجود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غیر نقش و نمایشی نبود</p></div>
<div class="m2"><p>با وجود تو ماسوای وجود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز تو یکتائی وجود ترا</p></div>
<div class="m2"><p>کس ندند بمقتضای وجود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غیر ذات یگانه تو کسی</p></div>
<div class="m2"><p>نیست موجود در سرای وجود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون ز سر ازل گرفت قرار</p></div>
<div class="m2"><p>بظهور وجود رای وجود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بحار صفات و اسماء گشت</p></div>
<div class="m2"><p>جاری از کل خویش مای وجود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان در آئینه حدوث نمود</p></div>
<div class="m2"><p>پادشاه قدم لقای وجود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در بر آن ظهور یکتا کرد</p></div>
<div class="m2"><p>راست از کبریا ردای وجود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا تو دانی که بوده بر وحدت</p></div>
<div class="m2"><p>از ازل تا ابد بنای وجود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هستی ما بود چو کوه و در او</p></div>
<div class="m2"><p>می نه پیچیده جز صدای وجود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زنگ ز آئینه دلت بزدای</p></div>
<div class="m2"><p>تا بیابی در او صفای وجود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بی‌لب و کام پس بگوش دلت</p></div>
<div class="m2"><p>دم بدم در رسد ندای وجود</p></div></div>
<div class="b2" id="bn14"><p>که در اشیاء ظهور اوست عیان</p>
<p>غیره کل من علیها فان</p></div>
<div class="b" id="bn15"><div class="m1"><p>پرده از رخ چو آن صنم برداشت</p></div>
<div class="m2"><p>دل براه غمش قدم برداشت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چین بگسیو فکند و قامت دل</p></div>
<div class="m2"><p>ز احتمال بلاش خم برداشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آهوی رام چشم او چون دید</p></div>
<div class="m2"><p>دل بدنبال خویش رم برداشت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صبح کان لعبت یگانه قدم</p></div>
<div class="m2"><p>جانب دیر از حرم برداشت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گفتم ای سرور راستان که قدت</p></div>
<div class="m2"><p>پرده از سر فاستقم برداشت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بوثاق گدای گوشه‌نشین</p></div>
<div class="m2"><p>می‌توان گامی از کرم برداشت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چشم رحمت گشود بر من و خوش</p></div>
<div class="m2"><p>دو لب لعل را ز هم برداشت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که در اول قدم ز خود پرداخت</p></div>
<div class="m2"><p>هر که در راه ما قدم برداشت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گویدش دوست کومنست و من او</p></div>
<div class="m2"><p>عاشق از دست از منهم برداشت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کرد اشارت بساقی اندر دم</p></div>
<div class="m2"><p>تا که مستانه جام‌جم برداشت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کرد لبریز زان مئی که ز دل</p></div>
<div class="m2"><p>چون کشیدم غم و الم برداشت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اندر آن حالتی که ز آینه‌‌ام</p></div>
<div class="m2"><p>صیقل باده رنگ غم برداشت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>می‌شنیدم ز چنگ مطرب عشق</p></div>
<div class="m2"><p>این نوا چون بنغمه دم برداشت</p></div></div>
<div class="b2" id="bn28"><p>که در اشیاء ظهور اوست عیان</p>
<p>غیره کل من علیها فان</p></div>
<div class="b" id="bn29"><div class="m1"><p>دلبر ما که عین ماست همه</p></div>
<div class="m2"><p>ظاهر از نقش ماسواست همه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ساری اندر حباب قطره و یم</p></div>
<div class="m2"><p>بی‌تغییر وجود ماست همه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زان بت بس‌سرا و خانه ما</p></div>
<div class="m2"><p>بین که پرخانه و سراست همه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>قاف هستی ممکنات وجود</p></div>
<div class="m2"><p>سایه پر آن هماست همه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>این ظهورات مختلف که بجای</p></div>
<div class="m2"><p>نقش این پرده جابجاست همه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر هزار است وگر هزار هزار</p></div>
<div class="m2"><p>بوجود یکی بپاست همه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هیچیک را مبین بچشم خطا</p></div>
<div class="m2"><p>کآیت شه ذوالعطاست همه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>غیر خود را چو حق وجود نخواند</p></div>
<div class="m2"><p>از حق ار نگذری خداست همه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خویش را زد صدا بکوه وجود</p></div>
<div class="m2"><p>این هیاهوی آن صداست همه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تو مگو نیست در بنا پیدا</p></div>
<div class="m2"><p>بانیئی کو خود این بناست همه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نقش ذرات را چون بینی نیک</p></div>
<div class="m2"><p>کسوت شمس با ضیاست همه</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سر گنج نهان الا را</p></div>
<div class="m2"><p>جوئی ار در طلسم لاست همه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خود ز نای وجود شاه وجود</p></div>
<div class="m2"><p>دان که نائی این نواست همه</p></div></div>
<div class="b2" id="bn42"><p>که در اشیاء ظهور اوست عیان</p>
<p>غیره کل من علیها فان</p></div>
<div class="b" id="bn43"><div class="m1"><p>طره ترک عنبرین موئی</p></div>
<div class="m2"><p>دلفریبی بتی بلا جوئی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>در ره دل مرا بهر سوئی</p></div>
<div class="m2"><p>هشته دامی ز رشته موئی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>می‌کشد هر دم ببازاری</p></div>
<div class="m2"><p>می‌کشد هر دمم ببازوئی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>دل ز چوگان گویش بر در و بام</p></div>
<div class="m2"><p>می‌دود صبح و شام چون گوئی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>هست بر پا ز دستبرد شبش</p></div>
<div class="m2"><p>در همه انجمن هیاهوئی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>یار پیدا و در تفحص او</p></div>
<div class="m2"><p>هر کس می‌دود بهر سوئی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>دوشم آمد ببزم و گفت ترا</p></div>
<div class="m2"><p>هست با عشق ما اگر روئی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>مغز جان خالی از زکام هوا</p></div>
<div class="m2"><p>کن که یابی ز وصل مابوئی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>باز بنگر که عین ماست همه</p></div>
<div class="m2"><p>آنچه دریا و جوش می‌گوئی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>یار باتست زین عجب که تو خود</p></div>
<div class="m2"><p>عین آبی و آب می‌جوئی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بگذر از جود را ببحر وجود</p></div>
<div class="m2"><p>تا به بینی که جونئی اوئی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بحر گوید که از احاطه ذات</p></div>
<div class="m2"><p>نیست خالی زماء ما جوئی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>رفت و گفت این حدیث کرد بخویش</p></div>
<div class="m2"><p>دنگ و دیوانه‌‌ام ز یک هوئی</p></div></div>
<div class="b2" id="bn56"><p>که در اشیاء ظهور اوست عیان</p>
<p>غیره کل من علیها فان</p></div>
<div class="b" id="bn57"><div class="m1"><p>ای پسر حب حشمت و جاهت</p></div>
<div class="m2"><p>کرده دور از حریم آن شاهت</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بر تو یار از تو اقربست و ترا</p></div>
<div class="m2"><p>کرده دور از تو نفس گمراهت</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>یکدم از خود در اوبین که توئی</p></div>
<div class="m2"><p>آنکه ندهد توئی بر او راهت</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چون حجاب توئی فتاد از تو</p></div>
<div class="m2"><p>جو ز خود هرچه هست دلخواهت</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>کمترین قدرتست اینکه بود</p></div>
<div class="m2"><p>مالکیت بماهی و ماهت</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>صادق آمد چو رفت از تو تویی</p></div>
<div class="m2"><p>لیس فی جبّتی سوی آللهت</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>یوسفا تو عزیز مصر خودی</p></div>
<div class="m2"><p>نفس خود ببین فکنده در چاهت</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>زین خودی در گذر که عشق کند</p></div>
<div class="m2"><p>شاه مصر وجود ناگاهت</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>هست یکسان بوحدت ار نگری</p></div>
<div class="m2"><p>فوق و تحت و بلند و کوتاهت</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>کن بشطرنج عشق جانرا مات</p></div>
<div class="m2"><p>تا که بردارد از دو سوی شاهت</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>دو جهان از گدائی در عشق</p></div>
<div class="m2"><p>کمتر آید ز یک پر کاهت</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>گر کنی جان براه دوست نثار</p></div>
<div class="m2"><p>دوست خواند بنانم آللهت</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>شو ز خود بی‌خبر که غیرت عشق</p></div>
<div class="m2"><p>زین حقیقت نماید آگاهت</p></div></div>
<div class="b2" id="bn70"><p>که در اشیاء ظهور اوست عیان</p>
<p>غیره کل من علیها فان</p></div>
<div class="b" id="bn71"><div class="m1"><p>ار رخت ماه چرخ طنازی</p></div>
<div class="m2"><p>قامت سرو باغ ممتازی</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>آفت عقل و جان بطراری</p></div>
<div class="m2"><p>فتنه دین و دل بطنازی</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>طره‌ات مشک چین دلداری</p></div>
<div class="m2"><p>نرگست ترک شهر غمازی</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>چون تو شاهی و مات تست دو کون</p></div>
<div class="m2"><p>با که شطرنج عشق می‌بازی</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>گرنه عاشق خود از چه سبب</p></div>
<div class="m2"><p>خویش بر حسن خویش می‌نازی</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>زانکه نبود بخانه جز تو کسی</p></div>
<div class="m2"><p>که دلش را بناز بگدازی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>زلف خود را از بهر خودتابی</p></div>
<div class="m2"><p>روی خود را از بهر خود‌سازی</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>نکته خال خود تو دانی و بس</p></div>
<div class="m2"><p>که سخن با لطیفه پردازی</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>تو مسیحا دمی و نادره‌گوی</p></div>
<div class="m2"><p>ترکتازی کلام اعجازی</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>دل که در آتش غم تو گداخت</p></div>
<div class="m2"><p>شایدش گر بحرف بنوازی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>ضعف دل را بیار قند حجاز</p></div>
<div class="m2"><p>کن عجین با گلاب شیرازی</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>فارس را یکی بگوی ملیح</p></div>
<div class="m2"><p>نکته با فصاحت تازی</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>از میان خیزد اختلاف دوئی</p></div>
<div class="m2"><p>پرده زین رازگر براندازی</p></div></div>
<div class="b2" id="bn84"><p>که در اشیاء ظهور اوست عیان</p>
<p>غیره کل من علیها فان</p></div>
<div class="b" id="bn85"><div class="m1"><p>خیز ای دل که تا به همت عشق</p></div>
<div class="m2"><p>رو کنیم از دو سو بحضرت عشق</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>لوح جان را از نقش جرم دهیم</p></div>
<div class="m2"><p>شست و شوئی به آب رحمت عشق</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>سنگ باشد به از دلی که نکرد</p></div>
<div class="m2"><p>خویشتن را نثار حضرت عشق</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>یافت هر ذره وجود چو تافت</p></div>
<div class="m2"><p>در جهان آفتاب طلعت عشق</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>کرد در بر هرآنچه شد موجود</p></div>
<div class="m2"><p>بقبول وجود طلعت عشق</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>گر به وحدت کنی رجوع شود</p></div>
<div class="m2"><p>متساوی بجمله نسبت عشق</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>در حقیقت چو نگری بوجود</p></div>
<div class="m2"><p>وحدتی نیست غیر وحدت عشق</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>این ظهورات مختلف که بود</p></div>
<div class="m2"><p>نقش بر پرده مشیت عشق</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>هست هر یک به اختلاف صُور</p></div>
<div class="m2"><p>متعلق بکلک قدرت عشق</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>فاش گویم کسی بدار وجود</p></div>
<div class="m2"><p>نیست موجود غیر حضرت عشق</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>آری آری بغیر هستی عشق</p></div>
<div class="m2"><p>هستئی کی گذاشت غیرت عشق</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>در ازل کشت زار هستی غیر</p></div>
<div class="m2"><p>سوخت یکباره برق سطوت عشق</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>می‌رسد این ندا بگوش دلم</p></div>
<div class="m2"><p>هر دم از عالم هویت عشق</p></div></div>
<div class="b2" id="bn98"><p>که در اشیاء ظهور اوست عیان</p>
<p>غیره کل من علیها فان</p></div>
<div class="b" id="bn99"><div class="m1"><p>ای دل آهنگ کوی جانان کن</p></div>
<div class="m2"><p>ترک سر اندرین ره از جان کن</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>دفتر صلح و جنگ در هم پیچ</p></div>
<div class="m2"><p>خانه نام و ننگ ویران کن</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>جبهه خویش را در این میدان</p></div>
<div class="m2"><p>میخ نعل سمند سلطان کن</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>عقل در کار عشق نادان است</p></div>
<div class="m2"><p>هر چه کت گوید آن مکن آن کن</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>چند سندان زنی به درگه دوست</p></div>
<div class="m2"><p>باری از کله کار سندان کن</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>در وصل ار بروت نگشایند</p></div>
<div class="m2"><p>دیده مسمار باب هجران کن</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>آب و جاروب آستان روا</p></div>
<div class="m2"><p>ز اشک چشمان و موی مژگان کن</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>دل غمدیده را به مجمع فکر</p></div>
<div class="m2"><p>بند آن طره پریشان کن</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>بنشین بر سمند گردون تاز</p></div>
<div class="m2"><p>چرخ راگرد سم یکران کن</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>خوش ز سم کمیت عرش نورد</p></div>
<div class="m2"><p>منشق ایجان حجاب امکان کن</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>از تکاپوی رخش دریایی</p></div>
<div class="m2"><p>لامکان ار غبار میدان کن</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>عشق از ایمان و کفر بیرونست</p></div>
<div class="m2"><p>دل مبرا ز کفر و ایمان کن</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>تا شوی ایمن از وساوس نفس</p></div>
<div class="m2"><p>این سخن نقش خاتم جان کن</p></div></div>
<div class="b2" id="bn112"><p>که در اشیاء ظهور اوست عیان</p>
<p>غیره کل من علیها فان</p></div>
<div class="b" id="bn113"><div class="m1"><p>حسن یارای حسن یکیست یکی</p></div>
<div class="m2"><p>حرفی افزون سخن یکیست یکی</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>نرد عارف که یافت سر وجود</p></div>
<div class="m2"><p>راحت و هم محن یکیست یکی</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>در بر آنکه دیده جلوه یار</p></div>
<div class="m2"><p>خلوت و انجمن یکیست یکی</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>دلبر و دل بکار دل چه شوی</p></div>
<div class="m2"><p>یکدل ایجان من یکیست یکی</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>جان و جانان اگر که در گذری</p></div>
<div class="m2"><p>یکره از جان و تن یکیست یکی</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>نسبت آب صاف گاه ظهور</p></div>
<div class="m2"><p>با سه برگ و سمن یکیست یکی</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>با گل و خار بی‌معیت رنگ</p></div>
<div class="m2"><p>انبساط چمن یکیست یکی</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>من حجاب من است چونکه افتاد</p></div>
<div class="m2"><p>این حجاب او و من یکیست یکی</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>این من و ماست جمله خواب و خیال</p></div>
<div class="m2"><p>قادر ذوالمنن یکیست یکی</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>آنکه زین ما و من عریست بذات</p></div>
<div class="m2"><p>در نهان و علن یکیست یکی</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>ذات بی‌نقش اندرین همه نقش</p></div>
<div class="m2"><p>نزد اهل فطن یکیست یکی</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>سخن اوست در همه دهنی</p></div>
<div class="m2"><p>این سخن وین دهن یکیست یکی</p></div></div>
<div class="b2" id="bn125"><p>که در اشیاء ظهور اوست عیان</p>
<p>غیره کل من علیها فان</p></div>
<div class="b" id="bn126"><div class="m1"><p>ای رخت آفتاب روشن دل</p></div>
<div class="m2"><p>وی قدرت نو نهال گلشن دل</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>طره‌ات گه به فتنه رهبر عقل</p></div>
<div class="m2"><p>نگرست که بغمزه رهزن دل</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>غم عشقت سرور سینه ریش</p></div>
<div class="m2"><p>خم زلفت کند گردن دل</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>عاشقان را که برق عشق تو سوخت</p></div>
<div class="m2"><p>کشت زار وجود و خرمن دل</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>پرده بردار و طلعتی بنمای</p></div>
<div class="m2"><p>بهر تسکین دل بمأمن دل</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>از پس ظلمت فراق بتاب</p></div>
<div class="m2"><p>آفتابی بتاب ز روزن دل</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>از عنایت به نوبهار وصال</p></div>
<div class="m2"><p>کن مبدل هوای بهمن دل</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>ما که دادیم دل بطره دوست</p></div>
<div class="m2"><p>تا چه با دل کند مهیمن دل</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>دی عبورم پی سراغ بتی</p></div>
<div class="m2"><p>شد به بتخانه معین دل</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>دیدم از شاهدان پرده نشین</p></div>
<div class="m2"><p>محفی در سرای ارمن دال</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>جستم از شاهدی نهفته نشان</p></div>
<div class="m2"><p>زان‌بت بی‌نشان به مسکن دل</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>لب گزیدم که لب ببند و بجوی</p></div>
<div class="m2"><p>سر مکنون دل ز مکمن دل</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>داشت فکرم بر اینکه باز برم</p></div>
<div class="m2"><p>مشکل خویش بر برهمن دل</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>ناگه آمد زبام دیر بگوش</p></div>
<div class="m2"><p>این خروشم ز نای ارغن دل</p></div></div>
<div class="b2" id="bn140"><p>که در اشیاء ظهور اوست عیان</p>
<p>غیره کل من علیها فان</p></div>
<div class="b" id="bn141"><div class="m1"><p>از خرابات رند مستی دوش</p></div>
<div class="m2"><p>شد ز لطفم براه و گفت بگوش</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>کی طلبکار یار با من مست</p></div>
<div class="m2"><p>خیز و رو کن بکوی باده فروش</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>تا ببینی عیان بمحفل عشق</p></div>
<div class="m2"><p>روی دلدار و حسن بی‌روپوش</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>جز در پیر ما ز هیچ درت</p></div>
<div class="m2"><p>نیست فتحی مزن دری و مکوش</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>گشت آن حرفم آتشی و بسوخت</p></div>
<div class="m2"><p>جسم و جان را و دل فتاد بجوش</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>از پی او شدم روانه بشوق</p></div>
<div class="m2"><p>همه جا مست و بیخود و مدهوش</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>تا رسیدم بدرگهی که در آن</p></div>
<div class="m2"><p>بود جبریل عقل حلقه بگوش</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>دیدم از دور میکشان همه را</p></div>
<div class="m2"><p>جمع بر دور پیر باده فروش</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>ار حریفان بزم گوش دلم</p></div>
<div class="m2"><p>می‌نیوشید بانگ نوشانوش</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>ناگه افتد چشم رحمت پیر</p></div>
<div class="m2"><p>بمن زار و بر کشید خروش</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>که تراگر هوای خدمت ماست</p></div>
<div class="m2"><p>در خرابات کش سبو بردوش</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>آنگهم ساقی از اشارت پیر</p></div>
<div class="m2"><p>ساغری داد کاین بگیر و بنوش</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>چون کشیدم می از پیاله عشق</p></div>
<div class="m2"><p>گشتم از گفتگوی عقل خموش</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>اندر آن مستی این حدیث بدیع</p></div>
<div class="m2"><p>گفت خوش خوش بگوش هوش سروش</p></div></div>
<div class="b2" id="bn155"><p>که در اشیاء ظهور اوست عیان</p>
<p>غیره کل من علیها فان</p></div>
<div class="b" id="bn156"><div class="m1"><p>دامن خیمه شه چو بالا زد</p></div>
<div class="m2"><p>حسنش آتش بکوه و صحرا زد</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>شد جهان روشن از فروغ رخش</p></div>
<div class="m2"><p>رایت حسن چون هویدا زد</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>آنشهی کو ز ما بذلت غنی است</p></div>
<div class="m2"><p>آمد و جام فقر با ما زد</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>شد ز تخت شهی بزیر و قدح</p></div>
<div class="m2"><p>با گدایان بی سرو پا زد</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>دید چون حسن دلفریبی او</p></div>
<div class="m2"><p>بر سر عقل شور سودا زد</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>تا نماید که هر چه هست یکیست</p></div>
<div class="m2"><p>سوی صحرا علم به تنها زد</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>تا بگوید که غیر ما همه لاست</p></div>
<div class="m2"><p>کوس وحدت ببام الا زد</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>این همه نقش کلک قدرت او</p></div>
<div class="m2"><p>که بر این پرده است پیدا زد</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>کرد غوغا ز حسن خویش بپا</p></div>
<div class="m2"><p>وانگهی خویش را بغوغا زد</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>بار دیگر نهنگ عشق برون</p></div>
<div class="m2"><p>شد ز دریا و دل بدریا زد</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>پرده زان راز بر فکند عیان</p></div>
<div class="m2"><p>دم ز اسرار ذات یکتا زد</p></div></div>
<div class="b2" id="bn167"><p>که در اشیاء ظهور اوست عیان</p>
<p>غیره کل من علیها فان</p></div>
<div class="b" id="bn168"><div class="m1"><p>ساقیا دور دور رحمت تست</p></div>
<div class="m2"><p>چشم مستان بدست همت تست</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>دور ما گر بسر رسید چه باک</p></div>
<div class="m2"><p>دور چون دور جود و رحمت تست</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>گر بسهو و خطا گذشت گذشت</p></div>
<div class="m2"><p>دور ما نک بعفو نوبت تست</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>ما گر آلوده دانیم چه جرم</p></div>
<div class="m2"><p>دل خود اندر پناه عصمت تست</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>صبح عید است و چشم باده‌کشان</p></div>
<div class="m2"><p>بعطای تو و عنایت تست</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>داروی درد و غم که جام می است</p></div>
<div class="m2"><p>ده بمستان که وقت قدرت تست</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>می‌کشان را کفیل در هر باب</p></div>
<div class="m2"><p>کف پیمانه بخش حضرت تست</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>از تو ما را بجز تو نیست طمع</p></div>
<div class="m2"><p>خود گواهم بعشق غیرت تست</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>گر کنی لطف وگرنه در همه حال</p></div>
<div class="m2"><p>جان رندان رهین منت تست</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>باری آن باده – شبانه کز او</p></div>
<div class="m2"><p>دل دیوانه مست وحدت تست</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>گر بود صاف و گر که دُرد بیار</p></div>
<div class="m2"><p>زانکه درد تو عین صفوت تست</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>خوش کن از باده‌ام سری که مدام</p></div>
<div class="m2"><p>بند اندر کمند بیعت تست</p></div></div>
<div class="b" id="bn180"><div class="m1"><p>سرکشی کرد نفس و چاره او</p></div>
<div class="m2"><p>درد جام شراب سطوت تست</p></div></div>
<div class="b" id="bn181"><div class="m1"><p>کرده این نکته را فسانه خویش</p></div>
<div class="m2"><p>تا دل آئینه‌دار طلعت تست</p></div></div>
<div class="b2" id="bn182"><p>که در اشیاء ظهور اوست عیان</p>
<p>غیره کل من علیها فان</p></div>
<div class="b" id="bn183"><div class="m1"><p>ساقی امشب عنایت افزون کرد</p></div>
<div class="m2"><p>بهر رندان بباده افیون کرد</p></div></div>
<div class="b" id="bn184"><div class="m1"><p>کار یاران بدور اول ساخت</p></div>
<div class="m2"><p>دور ثانی مپرس تا چون کرد</p></div></div>
<div class="b" id="bn185"><div class="m1"><p>در قدح مشک و می بهم آمیخت</p></div>
<div class="m2"><p>باده را با گلاب معجون کرد</p></div></div>
<div class="b" id="bn186"><div class="m1"><p>بیش از پیش دست قدرت را</p></div>
<div class="m2"><p>ز آستین بهر بذل بیرون کرد</p></div></div>
<div class="b" id="bn187"><div class="m1"><p>سوی رندان دور در هر دور</p></div>
<div class="m2"><p>همره جام چشم میگون کرد</p></div></div>
<div class="b" id="bn188"><div class="m1"><p>باده حضار را پیاپی داد</p></div>
<div class="m2"><p>حال عشاق را درگرگون کرد</p></div></div>
<div class="b" id="bn189"><div class="m1"><p>گنج لب بر گشود و گوهر ریخت</p></div>
<div class="m2"><p>مفلسان را بحرف قارون کرد</p></div></div>
<div class="b" id="bn190"><div class="m1"><p>دست بر مو گرفت و ساغر داد</p></div>
<div class="m2"><p>عقل را از دو شیوه مجنون کرد</p></div></div>
<div class="b" id="bn191"><div class="m1"><p>ترک خون ریز غمزه‌اش یکبار</p></div>
<div class="m2"><p>بر سر بیهشان شبیخون کرد</p></div></div>
<div class="b" id="bn192"><div class="m1"><p>خوش خوش آن مطرب مقام‌شناس</p></div>
<div class="m2"><p>آشنا چنگ را به قانون کرد</p></div></div>
<div class="b" id="bn193"><div class="m1"><p>هر دمی زد رهی و مستان را</p></div>
<div class="m2"><p>بسیاقی ز خویش ممنون کرد</p></div></div>
<div class="b" id="bn194"><div class="m1"><p>از بم و زیر نی حریفان را</p></div>
<div class="m2"><p>گاه مسرور و گاه محزون کرد</p></div></div>
<div class="b" id="bn195"><div class="m1"><p>مطرب از چشم عاشقان افشاند</p></div>
<div class="m2"><p>آنچه ساقی ز غمزه‌اش خون کرد</p></div></div>
<div class="b" id="bn196"><div class="m1"><p>مستی بیخودان چون افزون دید</p></div>
<div class="m2"><p>این نوا را بنغمه موزون کرد</p></div></div>
<div class="b2" id="bn197"><p>که در اشیاء ظهور اوست عیان</p>
<p>غیره کل من علیها فان</p></div>
<div class="b" id="bn198"><div class="m1"><p>ما گدایان که نفی با لذاتیم</p></div>
<div class="m2"><p>پادشاهان ملک اثباتیم</p></div></div>
<div class="b" id="bn199"><div class="m1"><p>حامل اسم اعظم شاهیم</p></div>
<div class="m2"><p>مخزن سر حضرت ذاتیم</p></div></div>
<div class="b" id="bn200"><div class="m1"><p>آفتاب سپهر عشق و بحسن</p></div>
<div class="m2"><p>جلوه‌گر در تمام ذراتیم</p></div></div>
<div class="b" id="bn201"><div class="m1"><p>پرتو حسن ذات مطلق را</p></div>
<div class="m2"><p>در تمام صفات مرآتیم</p></div></div>
<div class="b" id="bn202"><div class="m1"><p>جلوه نور شاه معنی را</p></div>
<div class="m2"><p>در مقام حضور مشکواتیم</p></div></div>
<div class="b" id="bn203"><div class="m1"><p>بحر ز خار وحدتیم و ز جوش</p></div>
<div class="m2"><p>گاه در جزر و مد و گه ماتیم</p></div></div>
<div class="b" id="bn204"><div class="m1"><p>گه ثابت به ارض و گه در سیر</p></div>
<div class="m2"><p>همچو سیاره در سماواتیم</p></div></div>
<div class="b" id="bn205"><div class="m1"><p>دایم از جام عشق پیر مغان</p></div>
<div class="m2"><p>مست افتاده در خراباتیم</p></div></div>
<div class="b" id="bn206"><div class="m1"><p>باب فضل آستان میکده است</p></div>
<div class="m2"><p>ما بر آن در کلید حاجانیم</p></div></div>
<div class="b" id="bn207"><div class="m1"><p>رند و قلاش و لاابالی و مست</p></div>
<div class="m2"><p>فارغ از زهد و زرق و طاماتیم</p></div></div>
<div class="b" id="bn208"><div class="m1"><p>خویش غرق گناه از دم پیر</p></div>
<div class="m2"><p>خلق را غافرالخطیئاتیم</p></div></div>
<div class="b" id="bn209"><div class="m1"><p>از دم شاه عیوسی انفاس</p></div>
<div class="m2"><p>روح بخش تمام امواتیم</p></div></div>
<div class="b" id="bn210"><div class="m1"><p>روز و شب با سرود و بر بط و نی</p></div>
<div class="m2"><p>متذکر به این مناجاتیم</p></div></div>
<div class="b2" id="bn211"><p>که در اشیاء ظهور اوست عنان</p>
<p>غیره کل من علیها فان</p></div>
<div class="b" id="bn212"><div class="m1"><p>دوش در خوابم آفتاب آمد</p></div>
<div class="m2"><p>یعنی آن ماه بی‌حجاب آمد</p></div></div>
<div class="b" id="bn213"><div class="m1"><p>طالع از بام طالعم ز قضا</p></div>
<div class="m2"><p>در شب قدر آفتاب آمد</p></div></div>
<div class="b" id="bn214"><div class="m1"><p>شاه بیدار بخت بنده نواز</p></div>
<div class="m2"><p>بر سر خفته نیمی خواب آمد</p></div></div>
<div class="b" id="bn215"><div class="m1"><p>بهر دفع خمار هجر بتم</p></div>
<div class="m2"><p>نیم شب با بط و شراب آمد</p></div></div>
<div class="b" id="bn216"><div class="m1"><p>پا نهادم بخلوت دل و گفت</p></div>
<div class="m2"><p>گنج در خانه خراب آمد</p></div></div>
<div class="b" id="bn217"><div class="m1"><p>دل بیچاره را ز غمزه او</p></div>
<div class="m2"><p>دعوت وصل مستجاب آمد</p></div></div>
<div class="b" id="bn218"><div class="m1"><p>بهر صید دل شکسته ما</p></div>
<div class="m2"><p>با دو گیسوی پر زتاب آمد</p></div></div>
<div class="b" id="bn219"><div class="m1"><p>خوش قراری مرا ز خال لبش</p></div>
<div class="m2"><p>بعد صد گونه اضطراب آمد</p></div></div>
<div class="b" id="bn220"><div class="m1"><p>عاشقان البشاره کز در وصل</p></div>
<div class="m2"><p>شاهد قدس بی‌نقاب آمد</p></div></div>
<div class="b" id="bn221"><div class="m1"><p>واردات عجایب از ره غیب</p></div>
<div class="m2"><p>در دلم باز بی‌حساب آمد</p></div></div>
<div class="b" id="bn222"><div class="m1"><p>نور مهدی عیان به بزم حضور</p></div>
<div class="m2"><p>خوش خوش از پرده غیاب آمد</p></div></div>
<div class="b" id="bn223"><div class="m1"><p>بهر نفس عدو به دشت قتال</p></div>
<div class="m2"><p>نیاب مظهر‌العجاب آمد</p></div></div>
<div class="b" id="bn224"><div class="m1"><p>در کفش ذوالفقار خصم گداز</p></div>
<div class="m2"><p>حامی دین بوتراب آمد</p></div></div>
<div class="b" id="bn225"><div class="m1"><p>ز آستان جلال حضرت او</p></div>
<div class="m2"><p>خوش به گوش دل این خطاب آمد</p></div></div>
<div class="b2" id="bn226"><p>که در اشیاء ظهور اوست عیان</p>
<p>غیره کل من علیها فان</p></div>
<div class="b" id="bn227"><div class="m1"><p>شاه رحمت سریر می‌بینم</p></div>
<div class="m2"><p>پیر دریا ضمیر می‌بینم</p></div></div>
<div class="b" id="bn228"><div class="m1"><p>چشم دل راز نور رحمت او</p></div>
<div class="m2"><p>روشن و مستنیر می‌بینم</p></div></div>
<div class="b" id="bn229"><div class="m1"><p>در دل خاره از حوائج مور</p></div>
<div class="m2"><p>حضرتش را خبیر می‌بینم</p></div></div>
<div class="b" id="bn230"><div class="m1"><p>دو جهان را ز خرمن جودش</p></div>
<div class="m2"><p>کمتر از یک شعیر می‌بینم</p></div></div>
<div class="b" id="bn231"><div class="m1"><p>بر همه ذره‌ها چو مهر منیر</p></div>
<div class="m2"><p>لطف او را مجیر می‌بینم</p></div></div>
<div class="b" id="bn232"><div class="m1"><p>بر در دیری عیسوی پیری</p></div>
<div class="m2"><p>با جمال منیر می‌بینم</p></div></div>
<div class="b" id="bn233"><div class="m1"><p>خوش بچین کمند طره او</p></div>
<div class="m2"><p>دل خلقی اسیر می‌بینم</p></div></div>
<div class="b" id="bn234"><div class="m1"><p>میکشان را به پیره باده فروش</p></div>
<div class="m2"><p>بنده مستجیر می‌بینم</p></div></div>
<div class="b" id="bn235"><div class="m1"><p>زاهدان را ز نور طلعت یار</p></div>
<div class="m2"><p>دیده دل ضریر می‌بینم</p></div></div>
<div class="b" id="bn236"><div class="m1"><p>متحلی بهر چه می‌نگرم</p></div>
<div class="m2"><p>دلبری بی‌نظیر می‌بینم</p></div></div>
<div class="b" id="bn237"><div class="m1"><p>در صف کارزار نفس حرون</p></div>
<div class="m2"><p>رهروان را دلیر می‌بینم</p></div></div>
<div class="b" id="bn238"><div class="m1"><p>بر دو کون از گدائی در دوست</p></div>
<div class="m2"><p>خویشتن را امیر می‌بینم</p></div></div>
<div class="b" id="bn239"><div class="m1"><p>هر دم از بندگی پیر مغان</p></div>
<div class="m2"><p>فیضهای کثیر می‌بینم</p></div></div>
<div class="b" id="bn240"><div class="m1"><p>روز و شب بر نگارش این راز</p></div>
<div class="m2"><p>عقل کل را دبیر می‌بینم</p></div></div>
<div class="b2" id="bn241"><p>که در اشیاء ظهور اوست عیان</p>
<p>غیره کل من علیها فان</p></div>
<div class="b" id="bn242"><div class="m1"><p>چشم از او وام کن که او بینی</p></div>
<div class="m2"><p>وجه هور از چشم هو بینی</p></div></div>
<div class="b" id="bn243"><div class="m1"><p>از دل ما رموز طره یار</p></div>
<div class="m2"><p>جوئی ارباز مو بموبینی</p></div></div>
<div class="b" id="bn244"><div class="m1"><p>دل پیر مغان بجو که نه جوست</p></div>
<div class="m2"><p>گرچه این بحر را تو جو بینی</p></div></div>
<div class="b" id="bn245"><div class="m1"><p>گر بدنیا بچشم ما نگری</p></div>
<div class="m2"><p>قدر او را کم از تسوبینی</p></div></div>
<div class="b" id="bn246"><div class="m1"><p>در خرابات گر نهی قدمی</p></div>
<div class="m2"><p>خوش بسر ظل فضل هو بینی</p></div></div>
<div class="b" id="bn247"><div class="m1"><p>ساکنان حریم میکده را</p></div>
<div class="m2"><p>مست آن چشم فتنه جو بینی</p></div></div>
<div class="b" id="bn248"><div class="m1"><p>هر چه در پرده وجود بود</p></div>
<div class="m2"><p>فاش و بی پرده خوش نکو بینی</p></div></div>
<div class="b" id="bn249"><div class="m1"><p>دامن دلق می‌کشان همه را</p></div>
<div class="m2"><p>پاک از لوث آرزو بینی</p></div></div>
<div class="b" id="bn250"><div class="m1"><p>رهروان طریق صفوت را</p></div>
<div class="m2"><p>سر بزانوی غم فرو بینی</p></div></div>
<div class="b" id="bn251"><div class="m1"><p>راز داران سر وحدت را</p></div>
<div class="m2"><p>بر زبان مهر انصتو بینی</p></div></div>
<div class="b" id="bn252"><div class="m1"><p>ساقی دور را می‌ از خم ذات</p></div>
<div class="m2"><p>بهر عشاق در کدو بینی</p></div></div>
<div class="b" id="bn253"><div class="m1"><p>قطره هر که نوشد از می او</p></div>
<div class="m2"><p>قلزمش غرق در سبو بینی</p></div></div>
<div class="b" id="bn254"><div class="m1"><p>مطرب عشق را در این افسون</p></div>
<div class="m2"><p>با دف و چنگ بذله گو بینی</p></div></div>
<div class="b2" id="bn255"><p>که در اشیاء ظهور اوست عیان</p>
<p>غیره کل من علیها فان</p></div>
<div class="b" id="bn256"><div class="m1"><p>ای دل ار بند زلف یار شوی</p></div>
<div class="m2"><p>مطلق از قید و اختیار شوی</p></div></div>
<div class="b" id="bn257"><div class="m1"><p>مالک ملک جان و دل گردی</p></div>
<div class="m2"><p>قبله اهل افتکار شوی</p></div></div>
<div class="b" id="bn258"><div class="m1"><p>در خرابات عشق رندانه</p></div>
<div class="m2"><p>گر در آئی و میگسار شوی</p></div></div>
<div class="b" id="bn259"><div class="m1"><p>حالی از ته پیاله مستان</p></div>
<div class="m2"><p>مست افتی و هوشیار شوی</p></div></div>
<div class="b" id="bn260"><div class="m1"><p>هوش آئی ز مستی هستی</p></div>
<div class="m2"><p>چه از می‌ نیستی خمار شوی</p></div></div>
<div class="b" id="bn261"><div class="m1"><p>احد‌آسا ز نه فلک گذری</p></div>
<div class="m2"><p>بر براق می ار سوار شوی</p></div></div>
<div class="b" id="bn262"><div class="m1"><p>علم رسمی بود سراب و ازو</p></div>
<div class="m2"><p>بگذر ای تشنه تا بحار شوی</p></div></div>
<div class="b" id="bn263"><div class="m1"><p>نوشی ار می ز جام پیر مغان</p></div>
<div class="m2"><p>عارف نور هشت و چار شوی</p></div></div>
<div class="b" id="bn264"><div class="m1"><p>بندگی گر کنی بحضرت عشق</p></div>
<div class="m2"><p>در دو عالم بزرگوار شوی</p></div></div>
<div class="b" id="bn265"><div class="m1"><p>قنبرآسا بکردگار قسم</p></div>
<div class="m2"><p>زین غلامی تو کردگار شوی</p></div></div>
<div class="b" id="bn266"><div class="m1"><p>عارفان جان عالمت خوانند</p></div>
<div class="m2"><p>در ره او چون جان نثار شوی</p></div></div>
<div class="b" id="bn267"><div class="m1"><p>چون صفی علی بمقدم شاه</p></div>
<div class="m2"><p>ترک سر کن که تاجدار شوی</p></div></div>
<div class="b" id="bn268"><div class="m1"><p>این سخن را بگوی مستانه</p></div>
<div class="m2"><p>تا بفگتن زبان یار شوی</p></div></div>
<div class="b2" id="bn269"><p>که در اشیاء ظهور اوست عیان</p>
<p>غیره کل من علیها فان</p></div>