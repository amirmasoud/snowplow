---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>چونکه در جوش بحر وحدت شد</p></div>
<div class="m2"><p>ظاهر از بحر موج کثرت شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کنز مخفی که غیب مطلق بود</p></div>
<div class="m2"><p>آشکار از حجاب غیبت شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نماند بخانه غیر از خود</p></div>
<div class="m2"><p>عین اشیاء ز فرط غیرت شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گاه گردید دل گهی دلدار</p></div>
<div class="m2"><p>گاه آئینه‌دار طلعت شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاه بنمود روی و از معنی</p></div>
<div class="m2"><p>هر دمی صد هزار صورت شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گاه بگشود روی و از محفل</p></div>
<div class="m2"><p>در سرا پرده هویت شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گاه شمشیر در معارک زد</p></div>
<div class="m2"><p>گاه آماده شهادت شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گاه در خوابگاه احمد خفت</p></div>
<div class="m2"><p>گاه بر مسند امامت شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گاه ترویج شرع احمد کرد</p></div>
<div class="m2"><p>رهنما گاه در طریقت شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ماالحقیقه که از زبان کمیل</p></div>
<div class="m2"><p>گفت و خود عین آن حقیقت شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خلق را گه بخوشی شورانید</p></div>
<div class="m2"><p>وانگه اندر سرای عزلت شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گه بمنبر دم از سلونی زد</p></div>
<div class="m2"><p>گاه لب بست و خود بحیرت شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گاه در طور لن ترانی گفت</p></div>
<div class="m2"><p>یعنی اندر حجاب عزت شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در جهان بی‌حجاب و پرده گهی</p></div>
<div class="m2"><p>جلوه‌گر در هزار کسوت شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گاه بنمود رخ بموسی و گه</p></div>
<div class="m2"><p>بر یهودان رهین خدمت شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گه سه نان داد و خویش حامد خویش</p></div>
<div class="m2"><p>زان کنایت بهفده ایت شد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گاه اندر نماز خاتم داد</p></div>
<div class="m2"><p>ختم بر دست او مروت شد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جود ذاتی او ز سر قدم</p></div>
<div class="m2"><p>بر حدوث دو کون علت شد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جلوه‌گر وحدتش در این کثرت</p></div>
<div class="m2"><p>بهر اظهار جود و قدرت شد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وه چه قدرت که چار عنصر جمع</p></div>
<div class="m2"><p>از دم او بیک طبیعت شد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وه چه قدرت کش از دو حرف جهان</p></div>
<div class="m2"><p>وندران هر چه هست خلقت شد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دوش کاندر حضور پیر مغان</p></div>
<div class="m2"><p>در خرابات عشق صحبت شد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>این سخن بود گوهری و برون</p></div>
<div class="m2"><p>از دهان علی رحمت شد</p></div></div>
<div class="b2" id="bn24"><p>که حقیقت بملک هستی شاه</p>
<p>نیست غیر از علی ولی‌الله</p></div>
<div class="b" id="bn25"><div class="m1"><p>مصطفی شاه ملک امکانی</p></div>
<div class="m2"><p>اولین موج بحر یزدانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در شب قرب واجب از دامان</p></div>
<div class="m2"><p>چون برافشاند گرد امکانی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سم رخشش حجاب نه گردون</p></div>
<div class="m2"><p>کرد منشق ز گرم جولانی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>این عجب بین که آنشب اشیاء را</p></div>
<div class="m2"><p>داد جسمش عروج روحانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هست یعنی حقیقت هر شیی</p></div>
<div class="m2"><p>ظل آن جسم پاک نورانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا بقوسین و قاب پیغمبر</p></div>
<div class="m2"><p>گشت خارج بجسم ربانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سر حد کمان شنو کاینک</p></div>
<div class="m2"><p>مر تراگویم ار سخندانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا بواجب چو دوره پرگار</p></div>
<div class="m2"><p>کن تصور تو دور امکانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جامع دوره را نبوت دان</p></div>
<div class="m2"><p>بر نبوت دو وجه ارزانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وجه ادنی ظهور اوست بر او</p></div>
<div class="m2"><p>در رسالت بنص قرآنی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>وجه اعلی بطون اوست که هست</p></div>
<div class="m2"><p>آن ولایت بصدق عرفانی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>والی آن ولایت است علی</p></div>
<div class="m2"><p>وجه یزدان ولی سبحانی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هستی ممکنات سرتاسر</p></div>
<div class="m2"><p>فرع جسم نبی است تا دانی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چونکه اول بسیط در خود بود</p></div>
<div class="m2"><p>منبسط شد بخویش در ثانی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چون شدش دوره تجلی طی</p></div>
<div class="m2"><p>هشت پا در حریم سلطانی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>عکس وجه ولایتش در دم</p></div>
<div class="m2"><p>تافت آنجا چنانکه میدانی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اندران بزم‌الغرض چون حق</p></div>
<div class="m2"><p>کرده بد دعوتش بمهمانی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خوانی آندم زغیب شد حاضر</p></div>
<div class="m2"><p>از نعیم سرای سبحانی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دستی از آستین غیب برون</p></div>
<div class="m2"><p>آمد او را بر سم همخوانی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دید دستی که داده با او دست</p></div>
<div class="m2"><p>بهر پیمان بامر یزدانی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دید دستی که کنده از خیبر</p></div>
<div class="m2"><p>با دو انگشت در به آسانی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>پیش از ایجاد عالم و آدم</p></div>
<div class="m2"><p>بوده کاخ وجود را بانی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>با پیمبر علی اعلی گفت</p></div>
<div class="m2"><p>در ثنای علی عمرانی</p></div></div>
<div class="b2" id="bn48"><p>که حقیقت بملک هستی شاه</p>
<p>نیست غیر از علی ولی‌الله</p></div>
<div class="b" id="bn49"><div class="m1"><p>مرتضی را بحجت عرفان</p></div>
<div class="m2"><p>معنی و صورتی است با یزدان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>معنیش واقع بمعنیش در ذات</p></div>
<div class="m2"><p>اسم و رسم و شروط و وصف و بیان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>و همها جمله اندر او مبهوت</p></div>
<div class="m2"><p>عقلها جمله اندران حیران</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>او چو دریا و عقلها چون خس</p></div>
<div class="m2"><p>خس چه یابد ز قعر بحرنشان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>صد هزاران هزار کشتی عقل</p></div>
<div class="m2"><p>شد در این بحر غرقه از طوفان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>که نیفتاد تخته بکنار</p></div>
<div class="m2"><p>تا چه جائی که ره برد بکران</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گمشد اوهام بس در این وادی</p></div>
<div class="m2"><p>که یکی راه نبرد بر پایان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>دم نشاید زدن چون زین معنی</p></div>
<div class="m2"><p>که برونست از یقین و گمان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بشنو از من ز صورتش سخنی</p></div>
<div class="m2"><p>تا که عشقم شکسته مهر زبان</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>صورت او که نزد اهل شهود</p></div>
<div class="m2"><p>عین معنی است در مقام عیان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>باشد او را دو وجه بر یک تن</p></div>
<div class="m2"><p>یک بمعنی و اوست جان جهان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>موجود جسم عالم است این جسم</p></div>
<div class="m2"><p>خالق جان آدم است آن جان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>هست زین بحر جنبشی اسماء</p></div>
<div class="m2"><p>هست زان نور تابشی اعیان</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>آنچه گفتند انبیاء بخبر</p></div>
<div class="m2"><p>و آنچه دیدند اولیا بعیان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>شمه بُد ز وصف این تن هین</p></div>
<div class="m2"><p>تا بشی‌ بد ز شمس آنچان هان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>زینره از صلب انبیا این جسم</p></div>
<div class="m2"><p>گشت ظاهر بعالم امکان</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>در دل پاک اولیا این روح</p></div>
<div class="m2"><p>گشت ساکن بصورت انسان</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>سر این صورت ار عیان خواهی</p></div>
<div class="m2"><p>جو تولا بعشق پیر مغان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>وجه او باقی است در اکرام</p></div>
<div class="m2"><p>غیره کل من علیها فان</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>در ره پیش عشق چون دادی</p></div>
<div class="m2"><p>جان و کردی بدست او پیمان</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>درمقام حضور پیر شود</p></div>
<div class="m2"><p>بر نور روشن سکینه ایمان</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چون بتابد بجانت نور حضور</p></div>
<div class="m2"><p>یابی از سر این کلام نشان</p></div></div>
<div class="b2" id="bn71"><p>که حقیقت بملک هستی شاه</p>
<p>نیست غیر از علی ولی‌الله</p></div>
<div class="b" id="bn72"><div class="m1"><p>خانه کعبه در تن عالم</p></div>
<div class="m2"><p>چون دل عالم است ای اعلم</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>لاجرم آن علی جسمانی</p></div>
<div class="m2"><p>زاد در خانه دل عالم</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>فاطمه ابنه الاسد که نمود</p></div>
<div class="m2"><p>افتخار از کنیزیش مریم</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چونکه بگرفت از ابوطالب</p></div>
<div class="m2"><p>حمل بر خالق وجود و عدم</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چون شد آثار وضع حمل عیان</p></div>
<div class="m2"><p>از وی آمد بعجز سوی حرم</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>کرد دیوار خانه را منشق</p></div>
<div class="m2"><p>در زمان رب کعبه و زمزم</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>شد چو داخل بخانه بانوی قدس</p></div>
<div class="m2"><p>هر دو دیوار هشت سر بر هم</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>گشت آنخانه غرق نور سیاه</p></div>
<div class="m2"><p>اندرین نکته ایست هین فافهم</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>آب حیوان درون تاریکی</p></div>
<div class="m2"><p>زد پی روشنی بدهر علم</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>ای پسر شو سیاه روی دوکون</p></div>
<div class="m2"><p>تا دو کونت شود اسیر ظالم</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>زین سیاهی رسی بنور وجود</p></div>
<div class="m2"><p>هل سفیدی و شو سیاه رقم</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>بوتراب آنزمان ز عالم قدس</p></div>
<div class="m2"><p>هشت اندر سرای خاک قدم</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>تا بری از مقدمات ظهور</p></div>
<div class="m2"><p>پی بسر نتیجه معظم</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>کعبه دیگریست ای سالک</p></div>
<div class="m2"><p>در تن عالم صغیر آدم</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>اندر اینجا علی روحانی</p></div>
<div class="m2"><p>زاده از مام نفس قدسی دم</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>روح قدسی مذکر آمد نیز</p></div>
<div class="m2"><p>نفس قدسی مؤنث آمد هم</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>آن چو بوطالبست و ای طالب</p></div>
<div class="m2"><p>وین چون بنت‌الاسد شد ای همدم</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>با هم این هر دو را کند تزویج</p></div>
<div class="m2"><p>نفس پاک پیر روشن دم</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>زاید اندر حریم دل آن نور</p></div>
<div class="m2"><p>چون شد این دو بیکدگر توأم</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>نام او شد سکینه معنی</p></div>
<div class="m2"><p>صورت او چو صورت آدم</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>دل بود کعبه این سکینه صمد</p></div>
<div class="m2"><p>دل چو دیر آمد این سکینه صنم</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>هر که را نیست این سکینه مخوان</p></div>
<div class="m2"><p>تو بنی آدمش هوالاعلم</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>شاهد غیب این سخن می‌گفت</p></div>
<div class="m2"><p>پرده برداشت چون ز سرکتم</p></div></div>
<div class="b2" id="bn95"><p>که حقیقت بملک هستی شاه</p>
<p>نیست غیر از علی ولی‌الله</p></div>
<div class="b" id="bn96"><div class="m1"><p>روز جنگ احد چو پیغمبر</p></div>
<div class="m2"><p>شد زانبوهی عدو مضطر</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>رو نهادند همرهانش تمام</p></div>
<div class="m2"><p>بفرار و نماند کس دیگر</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>موج بجز سیاه کفر غریق</p></div>
<div class="m2"><p>حواست فلک وجود پیغمبر</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>آمد از حق ندا که ای احمد</p></div>
<div class="m2"><p>استعانت بجوی از حیدر</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>تا در آرم بیارییت اینک</p></div>
<div class="m2"><p>ز آستین جلال دست ظفر</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>تارسد عون حقت از چپ و راست</p></div>
<div class="m2"><p>جو اعانت ز حیدر صفدر</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>خواست از شاه اولیا امداد</p></div>
<div class="m2"><p>در زمان احمد ستوده سیر</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>بود بر لب هنوزش ادرکنی</p></div>
<div class="m2"><p>از پس یا علی از که از معبر</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>خاست آواز شیهه دلدل</p></div>
<div class="m2"><p>تافت پس برق ذوالفقار دو سر</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>بود گفتی صدای عزرائیل</p></div>
<div class="m2"><p>بانگ دلدل بنفی آن لشکر</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>رفت خاشاک عمر اعدا را</p></div>
<div class="m2"><p>در زمان تیغ شاه چون صرصر</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>جان بجانان رسید یعنی خوش</p></div>
<div class="m2"><p>مصطفی شاه را کشید ببر</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>چون در این عالم آنچه یافت وقوع</p></div>
<div class="m2"><p>هست در شخص آدمی مضمر</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>در وجود تو نیز دشت احد</p></div>
<div class="m2"><p>هست قلب صنوبری پیکر</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>وان شئونات نفس غدارت</p></div>
<div class="m2"><p>هست انبوه لشکر کافر</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>احمد عقلت اندر این میدان</p></div>
<div class="m2"><p>مانده تنها و بیکس و مضطر</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>حیدرت عشق و ذوالفقارت ذکر</p></div>
<div class="m2"><p>وان ندا جذب خالق اکبر</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>احمد عقل را چو حیدر عشق</p></div>
<div class="m2"><p>گشت از سر جذب حق یاور</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>بر کشد ذوالفقار لا وزند</p></div>
<div class="m2"><p>بر وجود قریش نفس شرر</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>چون بشمشیر ذکر ساحت دل</p></div>
<div class="m2"><p>گشت پاک از سپاه فتنه و شر</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>بکشد آن شاهد یگانه ز رخ</p></div>
<div class="m2"><p>پرده آنگه که بر نشست غبر</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>معنی لا اله الا هو</p></div>
<div class="m2"><p>گوش قلبت نیوشد از دلبر</p></div></div>
<div class="b2" id="bn118"><p>که حقیقت بملک هستی شاه</p>
<p>نیست غیر از علی ولی الله</p></div>
<div class="b" id="bn119"><div class="m1"><p>احمد بت شکن خلیلانه</p></div>
<div class="m2"><p>با علی در حرم شد از خانه</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>آن که بر قفل دل کلید عطاش</p></div>
<div class="m2"><p>زد پی فتح باب دندانه</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>از غم فرقتش چو اهل عقول</p></div>
<div class="m2"><p>گشت نالان ستون حنانه</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>خواست تا دفتر رسالت خویش</p></div>
<div class="m2"><p>برساند به مهر شاهانه</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>بهر تحزیب بت علی را گفت</p></div>
<div class="m2"><p>پا به دوشم گذار مردانه</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>بت شکستن بهانه بود غرض</p></div>
<div class="m2"><p>حیدرش پا نهاد بر شانه</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>بار عشق خدای را بر دوش</p></div>
<div class="m2"><p>اشتر حق کشید مستانه</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>گشت از آن حول و قوه و قدرت</p></div>
<div class="m2"><p>عقل حیران و دنگ و دیوانه</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>پنجه بت شکن گشود و فکند</p></div>
<div class="m2"><p>لات و طاغوت را ز بتخانه</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>کرد واجب چو پاک کرد از بت</p></div>
<div class="m2"><p>بر خود و خلق طوف آن خانه</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>حج صوریست اینکه در اسلام</p></div>
<div class="m2"><p>شد یکی از جهات ششگانه</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>حج معنیست طوف کعبه دل</p></div>
<div class="m2"><p>کان بود فرض عقل فرزانه</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>عشق حیدر چو در دلت پرداخت</p></div>
<div class="m2"><p>لات و عزای نفس بیگانه</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>وز غبار وجود اغیارت</p></div>
<div class="m2"><p>رفت جاروب ذکر کاشانه</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>روکند در دل تو یار شود</p></div>
<div class="m2"><p>شمع جمعت جمال جانانه</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>نعمت الله را چو یافت دلت</p></div>
<div class="m2"><p>هرچه داری بده شکرانه</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>جان بشمع رخش بسوز چنانک</p></div>
<div class="m2"><p>عشق آموزد از تو پروانه</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>گر دهی دل بحرف ما آید</p></div>
<div class="m2"><p>حرف عالم بگوش پروانه</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>خواهی ار وصل گنج باد آور</p></div>
<div class="m2"><p>خانه را کوب و باش ویرانه</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>سبحه بفکن یار یکتا را</p></div>
<div class="m2"><p>یک دل آئی بترک صد دانه</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>در غم دوست پای یکتایی</p></div>
<div class="m2"><p>زن بفرق دو کون رندانه</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>در خرابات عاشقان با ما</p></div>
<div class="m2"><p>پس در آی و بنوش پیمانه</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>تا بجان تو عکس این معنی</p></div>
<div class="m2"><p>افتد از جام پیر میخانه</p></div></div>
<div class="b2" id="bn142"><p>که حقیقت بملک هستی شاه</p>
<p>نیست غیر از علی والی الله</p></div>
<div class="b" id="bn143"><div class="m1"><p>چون بخم غدیر از ایزد</p></div>
<div class="m2"><p>بر نبی شد خطاب کای احمد</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>سر بر آر از گلیم و کن برخلق</p></div>
<div class="m2"><p>فاش اسرار شاه لم یولد</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>همین مترس از خسان و کن ظاهر</p></div>
<div class="m2"><p>آن چه ز اسلام باشد آن مقصد</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>با وجود علی چه داری باک</p></div>
<div class="m2"><p>ای سلیمان ملک جان از دد</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>خیز و برکش بروی یأجوجان</p></div>
<div class="m2"><p>ای سکندر ز نام حیدر سد</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>بود عرفان بخود مرا مقصود</p></div>
<div class="m2"><p>ز آفرینش به جلوه او حد</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>گو بر اسلامیان ندارد سود</p></div>
<div class="m2"><p>بی‌تولای حیدر این اشهد</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>کن تو تبلیغ امر ما بر خلق</p></div>
<div class="m2"><p>خواه گردد قبول و خواهی رد</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>گشت در دم پیمبر راشد</p></div>
<div class="m2"><p>خلق را بر پیام حق ارشد</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>بر خلایق ز عالی و دانی</p></div>
<div class="m2"><p>کرد اتمام حجت سرمد</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>دست حیدر گرفت و گفت این دست</p></div>
<div class="m2"><p>هست دست خدای فرد صمد</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>کرده واجب بخلق تا محشر</p></div>
<div class="m2"><p>بیعت دست خویش را ایزد</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>بشکند هر که بیعت این دست</p></div>
<div class="m2"><p>گردد از باب کبریا مرتد</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>اندر آن روز از صغیر و کبیر</p></div>
<div class="m2"><p>عهد بستند با ید ذوالید</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>لیک بغد از نبی بر آن پیمان</p></div>
<div class="m2"><p>ماند باقی چهار تن بسند</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>دل غیر خم است و عقل نبی</p></div>
<div class="m2"><p>حیدرت عشق مطلق امجد</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>در غدیر دل نو ای عارف</p></div>
<div class="m2"><p>پیر عقلت بعشق چون خواند</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>چنگ بر زن بذیل او محکم</p></div>
<div class="m2"><p>تا رسی در سلوک بر مقصد</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>مادر عقل طفل قلب ترا</p></div>
<div class="m2"><p>چون کند منفظم ز شیر رشد</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>ز اهل منا شوی و سلمان وش</p></div>
<div class="m2"><p>سر این معنیت عیان گردد</p></div></div>
<div class="b2" id="bn163"><p>که حقیقت بملک هستی شاه</p>
<p>نیست غیر از علی ولی الله</p></div>
<div class="b" id="bn164"><div class="m1"><p>یک جهت چون شدند در شب غار</p></div>
<div class="m2"><p>قوم بر قتل سید ابرار</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>امر حق شد بر او که ای احمد</p></div>
<div class="m2"><p>امشب از مکّه بست باید بار</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>جای خود واگذار بر حیدر</p></div>
<div class="m2"><p>رو تو تنها ز شهر ذی کهسار</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>تا من امشب بذات خویش شوم</p></div>
<div class="m2"><p>مر ترا در سرای بستر دار</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>رفت و بگذاشت الغرض آنشاه</p></div>
<div class="m2"><p>خوابگه را بحیدر کرار</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>خفت انجا علی و زان خفتن</p></div>
<div class="m2"><p>بخت عارف ز خواب شد بیدار</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>حسن در وصف عشق شد فانی</p></div>
<div class="m2"><p>عشق بر حسن جان چو کرد ایثار</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>وحدت آمد نماند غیرت عشق</p></div>
<div class="m2"><p>هیچ باقی بخانه جز دلدار</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>نیمشب چون شدند جمع‌آور</p></div>
<div class="m2"><p>بر در حجره نبی کفار</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>کس ندیدند جز علی کان بود</p></div>
<div class="m2"><p>خفته بر جای احمد مختار</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>در زمان سطوت خداوندی</p></div>
<div class="m2"><p>خانه را ماند خالی از اغیار</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>تا تو دانی که در سرای وجود</p></div>
<div class="m2"><p>بیشکی نیتس جز یکی دلدار</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>خود نیوشد بگوش خویش ندا</p></div>
<div class="m2"><p>لمن الملک واحد القهار</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>اوست باقی و مابقی فانی</p></div>
<div class="m2"><p>اوست پیدا و ما سوی پندار</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>شاهد معنوی بخلوت دل</p></div>
<div class="m2"><p>گوید این فرد و می‌کند تکرار</p></div></div>
<div class="b2" id="bn179"><p>که حقیقت بملک هستی شاه</p>
<p>نیست غیر از علی ولی الله</p></div>