---
title: >-
    بخش ۵۰۶ - الوقوف الصادق
---
# بخش ۵۰۶ - الوقوف الصادق

<div class="b" id="bn1"><div class="m1"><p>وقوف صادقت آنست و لایق</p></div>
<div class="m2"><p>که باشد با مراد حق مطابق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بحال فقر و اعمال تصوف</p></div>
<div class="m2"><p>نباشد جز بوفق حق توقف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشانی وانمایم زین مقامت</p></div>
<div class="m2"><p>بود تا در وقوفش اهتمامت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود ایثار در جایش مناسب</p></div>
<div class="m2"><p>ولی ترکش بجائی هست واجب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غرض هر نیتی لله باشد</p></div>
<div class="m2"><p>مراد حق باو همراه باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مراد حق عموما ترک کام است</p></div>
<div class="m2"><p>خصوصا لیک حفظ هر مقام است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باین معنی که بر وضع مناسب</p></div>
<div class="m2"><p>بود بر جای خود حفظ مراتب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وقوف صادقت حفظ حدود است</p></div>
<div class="m2"><p>مراعات مقامات وجود است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بد این سر رشته از بهر احباب</p></div>
<div class="m2"><p>تو باقی را بذوق خویش دریاب</p></div></div>