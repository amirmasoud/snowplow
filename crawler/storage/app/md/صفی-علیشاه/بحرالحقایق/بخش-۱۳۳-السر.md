---
title: >-
    بخش ۱۳۳ - السر
---
# بخش ۱۳۳ - السر

<div class="b" id="bn1"><div class="m1"><p>شنو از سر و سر حق نگهدار</p></div>
<div class="m2"><p>که هر سر دار گردد زود سردار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود سر آنکه در ایجاد از وی</p></div>
<div class="m2"><p>بشیئیت بود مخصوص هر شی‌ء</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن شد نزد اهل حق محقق</p></div>
<div class="m2"><p>که حق را هیچکس نشناخت جز حق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه او را غیر او کس گشت طالب</p></div>
<div class="m2"><p>محب اوست هم او بیشوائب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر آن آگه زعین ما خلق شد</p></div>
<div class="m2"><p>بسر ذات خود عارف بحق شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو او را طالب الا سر او نیست</p></div>
<div class="m2"><p>محب او جز آنوجه نکونیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نبی کو را بحق بد تام حبی</p></div>
<div class="m2"><p>عرفت گفت زان ربی بریی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر این سر یافتی اسرار دانی</p></div>
<div class="m2"><p>بعرفان محرم این آستانی</p></div></div>