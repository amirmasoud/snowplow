---
title: >-
    بخش ۲۸۹ - عبدالروف
---
# بخش ۲۸۹ - عبدالروف

<div class="b" id="bn1"><div class="m1"><p>دگر از اولیا عبدالروف است</p></div>
<div class="m2"><p>که در رأفت چو شمس بی‌کسوفست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخلقش رأفت است افزون که برخود</p></div>
<div class="m2"><p>مساوی رأفتش بر نیک و بر بد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدی هم با بدانش محض جوداست</p></div>
<div class="m2"><p>که از رأفت یکی حفظ حدود است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نماید گر چه نقمت در نظرها</p></div>
<div class="m2"><p>ولی دارد به نیکوئی اثرها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دهد بر کس اگر او گو شمالی</p></div>
<div class="m2"><p>برد نقصی و افزاید کمالی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دو ابهر مریض آن حرز جانست</p></div>
<div class="m2"><p>خورانی گر باو حلوازیان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حدود شرع یکجا این چنین است</p></div>
<div class="m2"><p>ز بهر پاس ملک و جان و دین است</p></div></div>