---
title: >-
    بخش ۱۰ - الاحسان
---
# بخش ۱۰ - الاحسان

<div class="b" id="bn1"><div class="m1"><p>گر از روی خرد باشی مصدق</p></div>
<div class="m2"><p>شنو تحقیق احسان از محقق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عبودیت بود تفسیر احسان</p></div>
<div class="m2"><p>اگر یابد تحقیق بهر انسان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کنی آنسان مراعات ادب را</p></div>
<div class="m2"><p>که می‌بینی تو گوئی وجه رب را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عبودیت اگر گردد محقق</p></div>
<div class="m2"><p>شود مشهود سالک حضرت حق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو آثار عبودیت درین بین</p></div>
<div class="m2"><p>جمال رب خود از چشم یقین بین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبی فرمود اعمال نکو را</p></div>
<div class="m2"><p>چنان میکن که بینی گوئی او را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مر او را از یقین دیدن چنین است</p></div>
<div class="m2"><p>نه از روی حقیقت وین یقین است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بود پیدا بعبد آیات ذاتش</p></div>
<div class="m2"><p>خود از پشت حجابات صفاتش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدون این حجب منع است رؤیت</p></div>
<div class="m2"><p>جز آنرا کوست فنانی در طریقت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حقیقت گر شهودش شد نسق را</p></div>
<div class="m2"><p>نه عبد است او، دگر حق دیده حق را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه احسانست این، بل کشف ذاتست</p></div>
<div class="m2"><p>خود احسان رؤیت او بالصفاتست</p></div></div>