---
title: >-
    بخش ۲۴۷ - عبدالجلیل
---
# بخش ۲۴۷ - عبدالجلیل

<div class="b" id="bn1"><div class="m1"><p>نمایم واقف از عبدالجلیلت</p></div>
<div class="m2"><p>که هست او در جلالتها دلیلت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حق او را ساخت مرآت کمالش</p></div>
<div class="m2"><p>که باشد مظهر وصف جلالش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جلال حق چو او در ما سوا دید</p></div>
<div class="m2"><p>وجود خود ز اجلالش فنا دید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نذلل پیش هر شیئی بحق کرد</p></div>
<div class="m2"><p>جلال حق نشان درما خلق کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو موجود حقیقی ذوالجلال است</p></div>
<div class="m2"><p>ز بهر غیر او هستی محال است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آن صوفی نبیند ما سوائی</p></div>
<div class="m2"><p>سوای اوست معدم اربجایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر آن ز اشیاء جلال کبریا دید</p></div>
<div class="m2"><p>شهنشه را مساوی با گدا دید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو در این هر دو اجلالش عیانست</p></div>
<div class="m2"><p>ز دو لاشیئی باقی مستعانست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تجلی کرد چون اسم جلیلش</p></div>
<div class="m2"><p>هر آن شیئی است از هیبت ذلیلش</p></div></div>