---
title: >-
    بخش ۲۳۱ - عبدالمعز
---
# بخش ۲۳۱ - عبدالمعز

<div class="b" id="bn1"><div class="m1"><p>کند عبدالمعز را در تولی</p></div>
<div class="m2"><p>حق از اسم معز بر دل تجلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فزاید هر زمان بر اعتزازش</p></div>
<div class="m2"><p>بهر شیئی نماید دلنوازش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر اشیاء جمله او عزت پناهست</p></div>
<div class="m2"><p>بهر ذی عزتی خود پادشاه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به بستان عزت گل آشکار است</p></div>
<div class="m2"><p>گیاه تلخ نامطبوع و خوارست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گهی باشد که از بهر علاجی</p></div>
<div class="m2"><p>تو را بر آن گیاهست احتیاجی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در اینجا عزت او بین که چونست</p></div>
<div class="m2"><p>ز صد بستان گل قدرش فزونست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به بخشی دربهای او چمنها</p></div>
<div class="m2"><p>بحفظش پرده پوشی‌ از سمنها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در اینساعت که این اعزاز وحد یافت</p></div>
<div class="m2"><p>از آن عبدالمعزعون و مدد یافت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که او اسم معز را هست مظهر</p></div>
<div class="m2"><p>بود ز و عزت ار زد حلقه بر در</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بعالم پس نباشد خوار چیزی</p></div>
<div class="m2"><p>عزیز است از حق ارداری تمیزی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شود قدرش بجای خویش معلوم</p></div>
<div class="m2"><p>برون از جای خود خوار است و محروم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بعزت پس بهر شیئی او دخیل است</p></div>
<div class="m2"><p>چه عز در هستی اشیاء اصیل است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مبین خوار ار که چیزی زهر ریز است</p></div>
<div class="m2"><p>بجائی ز هر مارت هم عزیز است</p></div></div>