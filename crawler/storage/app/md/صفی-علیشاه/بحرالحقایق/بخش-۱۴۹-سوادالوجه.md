---
title: >-
    بخش ۱۴۹ - سوادالوجه
---
# بخش ۱۴۹ - سوادالوجه

<div class="b" id="bn1"><div class="m1"><p>سوادالوجه فی‌الدارین رازیست</p></div>
<div class="m2"><p>که از بهر فقیران امتیازیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن فقر حقیقی مرتسم شد</p></div>
<div class="m2"><p>رجوع عبد بر اصل عدم شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدنیا و آخرت پنهان و پیدا</p></div>
<div class="m2"><p>فنا فی‌الله گردید او بیکجا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آن کارسیه رویان بکام است</p></div>
<div class="m2"><p>هولله است هر فقری تمام است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوادالوجه چون گشتی زدارین</p></div>
<div class="m2"><p>ز پیشت خاست یکجا گرد کونین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه عالم ماند و نی آثار عالم</p></div>
<div class="m2"><p>نه یاد خلق و نه دیدار عالم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنان ثابت کند در خود فنا را</p></div>
<div class="m2"><p>که نه خود را بداند نه خدا را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز عارف حیرت اندر حیرت اینست</p></div>
<div class="m2"><p>فنای فی‌الفنا در وحدت اینست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون کونینش ز خاطر شد فراموش</p></div>
<div class="m2"><p>نداند هم که یارستش هم آغوش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سوادالوجه دون اینمقام است</p></div>
<div class="m2"><p>در اینجا فقرها تام التمام است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در اینجا جای تقریر و بیان نیست</p></div>
<div class="m2"><p>ز فقر و از فقیر اینجا نشان نیست</p></div></div>