---
title: >-
    بخش ۴۰۸ - المستهلک
---
# بخش ۴۰۸ - المستهلک

<div class="b" id="bn1"><div class="m1"><p>بود مستهلک آن فانی بالذات</p></div>
<div class="m2"><p>که باقی زو نماند رسم و عادات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو در ذات الاحد گردید فانی</p></div>
<div class="m2"><p>نماند ز او نشانی در نهانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همانا بعد استهلاک اشیاء</p></div>
<div class="m2"><p>بود آنوجه هو باقی و برجا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو غیری باوجود او نشاید</p></div>
<div class="m2"><p>از آنرو غیر او مستهلک آید</p></div></div>