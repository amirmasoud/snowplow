---
title: >-
    بخش ۴۵ - بیت‌العزه
---
# بخش ۴۵ - بیت‌العزه

<div class="b" id="bn1"><div class="m1"><p>بیت‌العزه آنک اهل دل آمد</p></div>
<div class="m2"><p>اگر پرسند قلب واصل آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شود حال فنا او را چو حاصل</p></div>
<div class="m2"><p>بود اندر مقام جمع داخل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فنا باشد مقام عز و اکرام</p></div>
<div class="m2"><p>دل از ذلت در آنجا یافت آرام</p></div></div>