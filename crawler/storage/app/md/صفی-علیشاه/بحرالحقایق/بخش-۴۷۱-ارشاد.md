---
title: >-
    بخش ۴۷۱ - ارشاد
---
# بخش ۴۷۱ - ارشاد

<div class="b" id="bn1"><div class="m1"><p>بس این نفس را شأنی عظیم است</p></div>
<div class="m2"><p>تو نشناسی که نفست بس سقیم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود او را با چنان لطف و شرأفت</p></div>
<div class="m2"><p>عجین داری باقسام کثافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر پاکش ز قاذورات سازی</p></div>
<div class="m2"><p>باو بینی که می‌شاید بنازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آر از منجلاب این در صافی</p></div>
<div class="m2"><p>بظلمی کش نمودی کن تلافی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهل معشوق خود را نزد اوباش</p></div>
<div class="m2"><p>مده کالای خود بر دزد و قلاش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بکام اژدها بینی دل آرام</p></div>
<div class="m2"><p>عجب از غیرتت گرمانی آرام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اعلنت از خدا و اولیا جوی</p></div>
<div class="m2"><p>که آری بر نجات نفس خودروی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بتأیید حق است این گر چه موقوف</p></div>
<div class="m2"><p>ولی اوقات باید داشت مصروف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بکوشش آب خود را کن تو صافی</p></div>
<div class="m2"><p>که نبود موهبت راهم منافی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رسد گر موهبت نعم المراد است</p></div>
<div class="m2"><p>وگرنه بر عمل هم اعتماد است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بود در کار امیدت فزونتر</p></div>
<div class="m2"><p>بسا نادر که ماند کشت بی‌بر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صفی نبود امید و اعتمادش</p></div>
<div class="m2"><p>بجز برلطف حق در هر مرادش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو دانی حال محتاجان مسکین</p></div>
<div class="m2"><p>اغثنی یا غیاب‌المستغیثین</p></div></div>