---
title: >-
    بخش ۴۸۸ - الوجود
---
# بخش ۴۸۸ - الوجود

<div class="b" id="bn1"><div class="m1"><p>ز معنای وجود آرم سند را</p></div>
<div class="m2"><p>که وجدان حقست آن ذات خود را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بذات خویش خود را محض خود یافت</p></div>
<div class="m2"><p>بصرف ذت خود را هر چه بد یافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن خوانند ارباب شهودش</p></div>
<div class="m2"><p>خود این را حضرت جمع و وجودش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو او خود را ذات خویشتن یافت</p></div>
<div class="m2"><p>بجمع خویش دور از انجمن یافت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مراد از انجمن اینجا صفات است</p></div>
<div class="m2"><p>که لازم حضرت او را بذاتست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آن دانیم اندر صرف ذاتش</p></div>
<div class="m2"><p>منزه ز اعتبارات صفاتش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آنجا نه صفت نه اسم گنجد</p></div>
<div class="m2"><p>نه در معنای عقلی جسم گنجد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خود اینهم محض مفهومست و ادراک</p></div>
<div class="m2"><p>وگر نه او بود ز ادراکها پاک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه نسبت قطره را با بحر عمان</p></div>
<div class="m2"><p>چه نسبت ذره را با مهر تابان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غرض وجدان او خود را وجود است</p></div>
<div class="m2"><p>هر آن موجودی از بودش نمود است</p></div></div>