---
title: >-
    بخش ۱۸۷ - باب‌الضاء ظاهر الممکنات
---
# بخش ۱۸۷ - باب‌الضاء ظاهر الممکنات

<div class="b" id="bn1"><div class="m1"><p>شنو از ظاهر الممکن که هست آن</p></div>
<div class="m2"><p>ظهور حق بصورتهای اعیان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بصورتهای اعیان ز التفاتش</p></div>
<div class="m2"><p>تجلی کرد و آن شد ممکناتش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مسما بر اضافی آن وجود است</p></div>
<div class="m2"><p>بظاهر چون مقید در نمود است</p></div></div>