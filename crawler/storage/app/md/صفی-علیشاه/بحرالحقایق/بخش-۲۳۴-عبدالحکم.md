---
title: >-
    بخش ۲۳۴ - عبدالحکم
---
# بخش ۲۳۴ - عبدالحکم

<div class="b" id="bn1"><div class="m1"><p>میان بندگان عبدالحکم را</p></div>
<div class="m2"><p>بدان حاکم بحکم الله شیم را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بحکم از حق بود بر خلق مأمور</p></div>
<div class="m2"><p>زریب وظن و اوهام و ریا دور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه تنها حکم او در امر شرعست</p></div>
<div class="m2"><p>که حکمش جاری اندر اصل و فرعست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشان حکم او باشد که بادی</p></div>
<div class="m2"><p>کند در کاه و گندم افتقادی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز حکمش شد شکار گربه عصفور</p></div>
<div class="m2"><p>که کرمی خورده بود از ملک معمور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز حکمش باد برد آرامش از گل</p></div>
<div class="m2"><p>چو او بر باد بر باد داد آرام بلبل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز حکمش خورد قاضی چوب رشوت</p></div>
<div class="m2"><p>بحکم حق چو قاضی کرد حیلت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز حکمش زاهدی شد همدم خلق</p></div>
<div class="m2"><p>که او را بس دغلها بود در دلق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدینسان حکم او جاریست مطلق</p></div>
<div class="m2"><p>شکافد موی در احقاق هر حق</p></div></div>