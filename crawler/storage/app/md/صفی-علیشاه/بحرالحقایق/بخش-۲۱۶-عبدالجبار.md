---
title: >-
    بخش ۲۱۶ - عبدالجبار
---
# بخش ۲۱۶ - عبدالجبار

<div class="b" id="bn1"><div class="m1"><p>دگر عبدی که بر وی اسم جبار</p></div>
<div class="m2"><p>تجلی کرده باشد اندر اطوار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کند پیوند هر کسری زهر شیئی</p></div>
<div class="m2"><p>چه جبر کسرها حق کرده از وی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تجلی کرده جبارش چو در سر</p></div>
<div class="m2"><p>بحال کل شیئش کرده جابر</p></div></div>