---
title: >-
    بخش ۷۳ - حجه‌الحق
---
# بخش ۷۳ - حجه‌الحق

<div class="b" id="bn1"><div class="m1"><p>شود از حجت الحق هر که سایل</p></div>
<div class="m2"><p>باو میگو: بود انسان کامل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو حجت زو بخلق از حق تمامست</p></div>
<div class="m2"><p>وجودش دعوتی بر خاص و عامست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خود آدم بر ملایک گشت حجت</p></div>
<div class="m2"><p>به «انبئهم» از آن ره یافت رخصت</p></div></div>