---
title: >-
    بخش ۴۷۲ - النقباء
---
# بخش ۴۷۲ - النقباء

<div class="b" id="bn1"><div class="m1"><p>نقیبانند ارباب سرائر</p></div>
<div class="m2"><p>که آگاهند ز اسرار و ضمایر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مصادق شد بر ایشان اسم باطن</p></div>
<div class="m2"><p>از آن وجهند مشرف بر بواطن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خفایائی که واجب کشف آنهاست</p></div>
<div class="m2"><p>نمایند آشکارا وین هویداست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز اینشان نیست کاری هیچ در حد</p></div>
<div class="m2"><p>خود آن قومند می‌گویند سیصد</p></div></div>