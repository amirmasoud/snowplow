---
title: >-
    بخش ۱۳۹ - سرالقدر
---
# بخش ۱۳۹ - سرالقدر

<div class="b" id="bn1"><div class="m1"><p>بود سرالقدر علمش بهر عین</p></div>
<div class="m2"><p>کز و شد منطبع احوال کونین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز عین شئی در علم آنچه می‌بود</p></div>
<div class="m2"><p>بظاهر سر بسر گردید موجود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس او را حکم بر مقدار آنست</p></div>
<div class="m2"><p>بهر شیئی که در علمش عیانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود سرالقدر حکمی که مربوط</p></div>
<div class="m2"><p>بعین شیئی و در علم است مضبوط</p></div></div>