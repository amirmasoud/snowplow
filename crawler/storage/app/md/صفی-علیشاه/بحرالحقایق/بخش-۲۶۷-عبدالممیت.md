---
title: >-
    بخش ۲۶۷ - عبدالممیت
---
# بخش ۲۶۷ - عبدالممیت

<div class="b" id="bn1"><div class="m1"><p>بود عبدالممیت آن کز ولایش</p></div>
<div class="m2"><p>بمیراند حق از نفس و هوایش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو مرد از نفس بر حق زنده گردد</p></div>
<div class="m2"><p>باوصاف نکو پاینده گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز شهوت و ز غضب چون مرد بازش</p></div>
<div class="m2"><p>نماید زنده حق غیر از مجازش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدینسان بر نفوسش بی‌توقف</p></div>
<div class="m2"><p>دهد حق باز تأثیر و تصرف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که باشد بر اماته خلق قادر</p></div>
<div class="m2"><p>ز نفس و حال و اخلاق مغایر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تواند پس بحقشان زنده سازد</p></div>
<div class="m2"><p>بنور عقلشان پاینده سازد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه باشد تا که قدر همت آن</p></div>
<div class="m2"><p>که گیرد زین مؤثر روح و ریحان</p></div></div>