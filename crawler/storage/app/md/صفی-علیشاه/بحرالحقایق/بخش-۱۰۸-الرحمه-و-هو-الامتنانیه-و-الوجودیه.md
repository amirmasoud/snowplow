---
title: >-
    بخش ۱۰۸ - الرحمه و هو الامتنانیه و الوجودیه
---
# بخش ۱۰۸ - الرحمه و هو الامتنانیه و الوجودیه

<div class="b" id="bn1"><div class="m1"><p>ز رحمت بشنو ار دانی معانی</p></div>
<div class="m2"><p>که آن باشد: وجودی و امتنانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود آن امتنانی در تقاضا</p></div>
<div class="m2"><p>ز حق عاید باستحقاق اشیاء</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بکلی شیئی وسعت از ازل داشت</p></div>
<div class="m2"><p>بهر تقدیر سبقت بر عمل داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آن داخل تمام ممکناتند</p></div>
<div class="m2"><p>ز حق مستدعی رزق و حیاتند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خود او مستغنی از خلق جهانست</p></div>
<div class="m2"><p>جهانش محض جود و امتنان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کمالش اقتضای رحمتی کرد</p></div>
<div class="m2"><p>بر ایجاد خلایق قدرتی کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همان رحمت بعالم متسع شد</p></div>
<div class="m2"><p>بدون آن تمدن ممتنع شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فرو بگرفت ذرات جهان را</p></div>
<div class="m2"><p>نمود آنگاه راه امتنان را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پس از آن کامتنانی بود و جودی</p></div>
<div class="m2"><p>شنو زان رحمتی کامد وجودی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خود آن مخصوص ارباب یقین است</p></div>
<div class="m2"><p>بآن نزدیک روح محسنین است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خود آنهم در حقیقت امتنانیست</p></div>
<div class="m2"><p>چه فیضش بر خلایق رایگانیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بآن گر اهل دین موعد گشتند</p></div>
<div class="m2"><p>بمحض منت از ذیجود گشتند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عمل در دین ز بهر خلق سوداست</p></div>
<div class="m2"><p>نه بهر آنکه سلطان وجوداست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چه او رحمت بخلق از نیک و بد کرد</p></div>
<div class="m2"><p>غضب را هم نه بهر نفع خود کرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>غضب باشد مثال تازیانه</p></div>
<div class="m2"><p>که سازد خفته شکلانرا روانه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از آنرو خواست آه نیمشب را</p></div>
<div class="m2"><p>که از جوش افکند بحر غضب را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر پر هر دو عالم از گناهست</p></div>
<div class="m2"><p>به پیش رحمتش یک پر کاهست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز جرم خلق رحمت کم نگردد</p></div>
<div class="m2"><p>محیطی غرقه شبنم نگردد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کند بل منجذب دریا نمی را</p></div>
<div class="m2"><p>به بخشد حق بآهی عالمی را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>غضب هم رحمتی باشد بمختص</p></div>
<div class="m2"><p>بذوق این نکته باید یافت نز نص</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>صفی را بحر رحمت منجذوب کرد</p></div>
<div class="m2"><p>بپاکی رجس او را منقلب کرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز ان رحمه‌الله قریبش</p></div>
<div class="m2"><p>من المحسن شد آنرحمت نصیبش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خداوندا بروح پاک سجاد</p></div>
<div class="m2"><p>تو روح پیر ما را کن زما شاد</p></div></div>