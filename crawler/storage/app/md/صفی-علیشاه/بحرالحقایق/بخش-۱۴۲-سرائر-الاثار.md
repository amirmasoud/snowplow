---
title: >-
    بخش ۱۴۲ - سرائر الاثار
---
# بخش ۱۴۲ - سرائر الاثار

<div class="b" id="bn1"><div class="m1"><p>ز آثارت سرائر گر یقین است</p></div>
<div class="m2"><p>ترا در باطن اکوان دفین است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرائر چیست باطنهای اکوان</p></div>
<div class="m2"><p>که شد تعبیر بر اسماء یزدان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خود این اکوان بظاهر چون مرا یاست</p></div>
<div class="m2"><p>در آن اسماء بچشم عقل پیداست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود اسماء حقیقت روح اکوان</p></div>
<div class="m2"><p>چنان کاندر بدن روحیست پنهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نبینی روح را با چشم لیکن</p></div>
<div class="m2"><p>کنی ادراک آن از جنبش تن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرت چشمی بود چون روح از جسم</p></div>
<div class="m2"><p>شناسی کاین اثر هست از فلان اسم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در این هیکل چه اسمی کرده تأثیر</p></div>
<div class="m2"><p>در این خلقت چه وصفی بوده تقدیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وگر باشی ادق در سیر اشیاء</p></div>
<div class="m2"><p>ز اسماء و صور بینی مسما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مسمی کیست عین ذات بیچون</p></div>
<div class="m2"><p>که از هر اسم و رسمی اوست بیرون</p></div></div>