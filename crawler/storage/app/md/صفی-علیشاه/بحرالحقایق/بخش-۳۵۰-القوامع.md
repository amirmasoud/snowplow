---
title: >-
    بخش ۳۵۰ - القوامع
---
# بخش ۳۵۰ - القوامع

<div class="b" id="bn1"><div class="m1"><p>قوامع ترک میل و آروزهاست</p></div>
<div class="m2"><p>که طبع و نفس سرکش را تقاضاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شود مرعبد فارغ از هواها</p></div>
<div class="m2"><p>هم ازوی قمع میل و مدعاها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوداین ز امداد اسماء الهی است</p></div>
<div class="m2"><p>هم از تایید حق و عون شاهیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که بهر عبد در سیر الی الله</p></div>
<div class="m2"><p>رسد پیک عنایت گاه و بیگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس آن امداد اسماء الهی</p></div>
<div class="m2"><p>کند قمع هواها را کماهی</p></div></div>