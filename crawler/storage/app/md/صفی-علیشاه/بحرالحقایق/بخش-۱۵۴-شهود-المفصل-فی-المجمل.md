---
title: >-
    بخش ۱۵۴ - شهود المفصل فی‌المجمل
---
# بخش ۱۵۴ - شهود المفصل فی‌المجمل

<div class="b" id="bn1"><div class="m1"><p>شهودی هم که در مجمل مفصل</p></div>
<div class="m2"><p>مشاهد را بود بروجه اکمل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود آن در احد کثرات دیدن</p></div>
<div class="m2"><p>شئون ذات را در ذات دیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان کاضوا جمع اندر سراجست</p></div>
<div class="m2"><p>بر آن چشمی که دور از اعوجاجست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باین معنی که بینی این ظهورات</p></div>
<div class="m2"><p>از آن ذات‌الاحد باشد در آیات</p></div></div>