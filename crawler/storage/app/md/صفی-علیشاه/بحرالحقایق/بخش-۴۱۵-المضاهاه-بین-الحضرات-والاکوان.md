---
title: >-
    بخش ۴۱۵ - المضاهاه بین‌الحضرات والاکوان
---
# بخش ۴۱۵ - المضاهاه بین‌الحضرات والاکوان

<div class="b" id="bn1"><div class="m1"><p>مضاهات دگر در نص عرفان</p></div>
<div class="m2"><p>بما بین سه حضرت گشت و اکوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود اکوانرا که ترتیب و حسابست</p></div>
<div class="m2"><p>بسوی آن سه حضرت انتسابست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خود امکان و وجوبست آن به نسبت</p></div>
<div class="m2"><p>دگرهم جمع ما بین دو حضرت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس از اکوان هر آنچیزی که نسبت</p></div>
<div class="m2"><p>بود سوی وجوب او را بقوت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود او در وجود اعلی و اشرف</p></div>
<div class="m2"><p>بنزد عامه اندر رتبه الطف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حقیقت علوی و روحیست او را</p></div>
<div class="m2"><p>و یا ملکیه مر تحقیق جو را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دگر نسبت سوی امکانش اقواست</p></div>
<div class="m2"><p>وجود اواخس در رتبه و ادنی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حقیقت عنصری و سفلی او راست</p></div>
<div class="m2"><p>مرکب یا بسیطه در تقاضاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دگر نسبت سوی امکانش اقواست</p></div>
<div class="m2"><p>وجود او اخس در رتبه وادنی است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حقیقت عنصری و سفلی او راست</p></div>
<div class="m2"><p>مرکب یا بسیطه در تقاضاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>و گر نسبت بسوی جمع ما بین</p></div>
<div class="m2"><p>اشدستش ز انسان باشد آن عین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حقیقت باشد انسانیه او را</p></div>
<div class="m2"><p>ز انسان باز بشنو وصف و خورا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر انسان سوی امکانست امیل</p></div>
<div class="m2"><p>در او احکام امکان باشد اغلب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ور انسان امیل او سوی وجوبست</p></div>
<div class="m2"><p>بسوی او همه روی وجوبست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در او حکم وجود اغلب روا شد</p></div>
<div class="m2"><p>خود او از انبیا و اولیا شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>و گر دروی جهات آمد مساوی</p></div>
<div class="m2"><p>مگر از مؤمنینش خوانده راوی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بهر سو میل او زین هر دو جانب</p></div>
<div class="m2"><p>بود در حکم کلی باز غالب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در ایمان شد دلیل ضعف و شدت</p></div>
<div class="m2"><p>مظاهاتت بر این نظم است و رتبت</p></div></div>