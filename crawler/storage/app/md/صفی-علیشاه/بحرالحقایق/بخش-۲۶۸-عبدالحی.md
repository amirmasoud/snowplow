---
title: >-
    بخش ۲۶۸ - عبدالحی
---
# بخش ۲۶۸ - عبدالحی

<div class="b" id="bn1"><div class="m1"><p>شناسی گر که عبدالحی کدامست</p></div>
<div class="m2"><p>حیات سر مدت از حق مقام است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کند بروی تجلی حی دیموم</p></div>
<div class="m2"><p>حیات سر مدش بخشد بمعلوم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حیات ذاتیش چون شد محقق</p></div>
<div class="m2"><p>بیابد سر دیمومیت حق</p></div></div>