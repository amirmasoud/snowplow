---
title: >-
    بخش ۳۱۵ - العید
---
# بخش ۳۱۵ - العید

<div class="b" id="bn1"><div class="m1"><p>خود آن عودی که بر قلبت پدید است</p></div>
<div class="m2"><p>بنزد اهل قلب و دید عید است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود آن از تخلی یا تجلی</p></div>
<div class="m2"><p>تو را یابد دل از هر یک تسلی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن گفتند ارباب معارف</p></div>
<div class="m2"><p>بود هر دم دو عید از بهر عارف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی زان شد تخلی از ذمائم</p></div>
<div class="m2"><p>دگر باشد تجلی ملایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدینان هم در اسلامت مبارک</p></div>
<div class="m2"><p>دو عید آمد که باشد تاج تارک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو را زین هر دو نیکو انبساطیست</p></div>
<div class="m2"><p>که بر عیدین اصلی ارتباطیست</p></div></div>