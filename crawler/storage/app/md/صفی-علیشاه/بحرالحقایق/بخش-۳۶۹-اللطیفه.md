---
title: >-
    بخش ۳۶۹ - اللطیفه
---
# بخش ۳۶۹ - اللطیفه

<div class="b" id="bn1"><div class="m1"><p>لطیفه چیست تدقیق اشارت</p></div>
<div class="m2"><p>که در فهم است و ناید در عبارت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بمعنی بس اشاراتی دقیق است</p></div>
<div class="m2"><p>که لفظ از بهر ضبطش لایلیق است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عبارت باشد از جولان او تنک</p></div>
<div class="m2"><p>سمند لفظ در میدان او لنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخاطر آید اما در بیانی</p></div>
<div class="m2"><p>نگنجد وین بداند نکته دانی</p></div></div>