---
title: >-
    بخش ۲۵۶ - عبدالشهید
---
# بخش ۲۵۶ - عبدالشهید

<div class="b" id="bn1"><div class="m1"><p>بود عبدالشهید آنکس که اشهاد</p></div>
<div class="m2"><p>دهد حقش بهر شیئی در ایجاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نماید آگه از سر وجودش</p></div>
<div class="m2"><p>بود بر نفس خلق الله شهودش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشاهد اوست بر نفس خلایق</p></div>
<div class="m2"><p>که باشد کشف اعیانش موافق</p></div></div>