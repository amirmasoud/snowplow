---
title: >-
    بخش ۵۱۳ - الهو
---
# بخش ۵۱۳ - الهو

<div class="b" id="bn1"><div class="m1"><p>بود هو اعتبار از ذات حضرت</p></div>
<div class="m2"><p>ز حیث اختفا و فقد غیبت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن وضع نفس مانند هوشد</p></div>
<div class="m2"><p>که روح اندر تن از پیوند هوشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو را هستی دلیل تام ذات است</p></div>
<div class="m2"><p>نفس هم دمبدم پیغام ذاتست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز نام خود فرستد بر تو پیغام</p></div>
<div class="m2"><p>که یادآور ز جود صاحب اکرام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تصور کن تو کی ذی روح بودی</p></div>
<div class="m2"><p>کجا بودت نمودی یا که بودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو را حق در وجود آورد و شئی کرد</p></div>
<div class="m2"><p>بهار این گلشنت را بعد دی کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غنای حق و فقر خود بیاد آر</p></div>
<div class="m2"><p>ز نام هو که دارد در تو تکرار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ودیعه هستی است این داری ار هوش</p></div>
<div class="m2"><p>کجا آدم کند ایندم فراموش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غرض هو رتبه غیب‌الغیوبست</p></div>
<div class="m2"><p>نفس نقش وی از بهر وجوبست</p></div></div>