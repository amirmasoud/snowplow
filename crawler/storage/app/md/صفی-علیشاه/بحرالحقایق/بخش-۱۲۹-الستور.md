---
title: >-
    بخش ۱۲۹ - الستور
---
# بخش ۱۲۹ - الستور

<div class="b" id="bn1"><div class="m1"><p>ستور آمد بتحقیق این هیاکل</p></div>
<div class="m2"><p>میان هر دو عالم گشته حایل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدن چون پرده‌ئی باشد بعادت</p></div>
<div class="m2"><p>میان عالم غیب و شهادت</p></div></div>