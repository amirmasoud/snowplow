---
title: >-
    بخش ۳۳۸ - باب القاف القابلیه الاولی
---
# بخش ۳۳۸ - باب القاف القابلیه الاولی

<div class="b" id="bn1"><div class="m1"><p>دگر بشنو ز اول قابلیت</p></div>
<div class="m2"><p>که آن اصل اصول آمد برتبت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود در اصطلاح اول تعین</p></div>
<div class="m2"><p>تعینها ازو یابد تمکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بحر اوست او اولین موج</p></div>
<div class="m2"><p>ز فرد مطلق است او اولین زوج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دگر برخاست امواج مکرر</p></div>
<div class="m2"><p>ز موج اولین تا رشح آخر</p></div></div>