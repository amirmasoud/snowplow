---
title: >-
    بخش ۴۱۹ - المعلم الاول و معلم الملک
---
# بخش ۴۱۹ - المعلم الاول و معلم الملک

<div class="b" id="bn1"><div class="m1"><p>معلم اول آدم بر ملک بود</p></div>
<div class="m2"><p>ملایک را معلم یک بیک بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برایشان کرد او انباء‌ اسماء</p></div>
<div class="m2"><p>بدستوری که دادش حقتعالی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حقیقت آدم او خودعین حق بود</p></div>
<div class="m2"><p>در آن کسوت که دانی جلوه فرمود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ظهور واحدیت بود آدم</p></div>
<div class="m2"><p>که مسجود ملک گردید و اکرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ملک نسبت بآدم بود ناقص</p></div>
<div class="m2"><p>که بود بود او عقل صرف و روح خالص</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صفی را عقل و روح و قلب و تن بود</p></div>
<div class="m2"><p>وی اکمل در ظهور ذولمنن بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر آدم کوشود در عین کثرت</p></div>
<div class="m2"><p>مجرد از شئونات طبیعت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خود او را مظهریت باشد از حق</p></div>
<div class="m2"><p>در او دارد تجلی ذات مطلق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خود آدم را از آن بر صورت خویش</p></div>
<div class="m2"><p>بخوبی کرد خلق از حکمت خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو را گویم بشرط محرمیت</p></div>
<div class="m2"><p>که پوشید او لباس آدمیت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر آنکس سجده او کرد دل دید</p></div>
<div class="m2"><p>نبود ابلیس محرم آب و گل دید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بهر دوری بود قطبی و شاهی</p></div>
<div class="m2"><p>بود هم ضد او ابلیس راهی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دگر گویند ارواح مکرم</p></div>
<div class="m2"><p>که باشد نفس و عقل ابلیس و آدم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مراد از نفس دان اینجا طبیعت</p></div>
<div class="m2"><p>که نبود هیچ با عقلش معیت</p></div></div>