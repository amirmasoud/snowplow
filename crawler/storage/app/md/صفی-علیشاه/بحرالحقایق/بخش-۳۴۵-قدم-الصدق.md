---
title: >-
    بخش ۳۴۵ - قدم‌الصدق
---
# بخش ۳۴۵ - قدم‌الصدق

<div class="b" id="bn1"><div class="m1"><p>قدم کان با اضافه صدق خاست</p></div>
<div class="m2"><p>بنیکی حکم سابق بر خواص است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخوبان حکمش از سابق جمیل است</p></div>
<div class="m2"><p>با خیارش مواهب بس جزیل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنیکی کرد حق حکم متین را</p></div>
<div class="m2"><p>عباد مخلصین و صالحین را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بشارتها برایشان از کرم داد</p></div>
<div class="m2"><p>بخیر سابق و صدق قدم داد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود صدق آنکه ز اشیاء خیر بینی</p></div>
<div class="m2"><p>خلاف است ار بچشم غیر بینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیابد راست بین جز راست هرگز</p></div>
<div class="m2"><p>نه چشمش جز بحق بیناست هرگز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از آن جز نیک ناید در نظرها</p></div>
<div class="m2"><p>شود ز او هر چه ظاهر در سیرها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از آنکه صورت از معنی کند نقل</p></div>
<div class="m2"><p>بودعالم تماما صورت عقل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بود آن عقل هم مانند مرآت</p></div>
<div class="m2"><p>نماید خوب و بد را در علامات</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر آن با عقل کل عقلش کج آید</p></div>
<div class="m2"><p>تمام قول و فعلش معوج آید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>و گر با عقل یک حالش بود راست</p></div>
<div class="m2"><p>بآن مقدار حسن او هیوداست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو در کشتی نشینی جانب بحر</p></div>
<div class="m2"><p>گمانت ساکنی تو می‌رود شهر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از آن خود بین خصال خود نکو دید</p></div>
<div class="m2"><p>عیبو دیگران را موبمو دید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قدم گر صدق باشد اینچنین نیست</p></div>
<div class="m2"><p>فعالش درنظرها جز متین نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هم او از خلق بیند گر خطائی</p></div>
<div class="m2"><p>بپوشاند بدون مدعائی</p></div></div>