---
title: >-
    بخش ۱۲۲ - الزیتون
---
# بخش ۱۲۲ - الزیتون

<div class="b" id="bn1"><div class="m1"><p>بود زیتون همان نفسی که مذکور</p></div>
<div class="m2"><p>بتفصیل آمد اندر آیه نور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هماره مستعد اشتعال است</p></div>
<div class="m2"><p>بنور قدس کان جمع کمال است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن نور است قوه فکر کامل</p></div>
<div class="m2"><p>مطالب با سهولت زوست حاصل</p></div></div>