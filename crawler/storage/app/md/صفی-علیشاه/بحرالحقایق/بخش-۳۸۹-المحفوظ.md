---
title: >-
    بخش ۳۸۹ - المحفوظ
---
# بخش ۳۸۹ - المحفوظ

<div class="b" id="bn1"><div class="m1"><p>بود محفوظ آنعبدی که شاهش</p></div>
<div class="m2"><p>بخود دارد ز لغزشها نگاهش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز حفظ حق مخالفها نهاده</p></div>
<div class="m2"><p>که آن در قول و فعل است و اراده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کند کاری که حق راضی بر آنست</p></div>
<div class="m2"><p>بافعال و اراده حق نشانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مراد و قصد و حالش جز بحق نیست</p></div>
<div class="m2"><p>ز قصد خویش حرفش در ورق نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مقام آمد که از عصیان آدم</p></div>
<div class="m2"><p>تو را گویم گر آن داری مسلم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که با حفظ الهی او بدرگاه</p></div>
<div class="m2"><p>چرا عصیان نمود و گشت گمراه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زأکل گندم از حکم حقیقت</p></div>
<div class="m2"><p>بود افعال آدم بر طبیعت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهشت عقل از حق گشت جایش</p></div>
<div class="m2"><p>طبیعت شد بگندم رهنمایش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز نهی گندم اینمعنی است منظور</p></div>
<div class="m2"><p>که ز آثار طبیعت او شود دور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تقاضای طبیعت لیک آن بود</p></div>
<div class="m2"><p>که بر وی فر و زور خویش بنمود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خود این جبریست کاصل اختیار است</p></div>
<div class="m2"><p>چه هر شیئی بجای خود بکار است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جهانرا بر طبیعت چون مدار است</p></div>
<div class="m2"><p>ز حق شاید گرش این اقتدار است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نباشد گر طبیعت عالمی نیست</p></div>
<div class="m2"><p>به «کرمنا» مخاطب آدمی نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پس آدم خورد گر گندم ز غفلت</p></div>
<div class="m2"><p>منافی نیست آن با عقل و عصمت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چه او در اکل گندم بود مجبور</p></div>
<div class="m2"><p>زوجهی منهی از صد وجه مأمور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز یک ره کرد ترک امر حضرت</p></div>
<div class="m2"><p>بباطن گر چه آنهم بود طاعت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز یک ره اکل گندم شد و بالش</p></div>
<div class="m2"><p>ز صد ره گشت باعث بر کمالش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برون از جنتش انداخت در خاک</p></div>
<div class="m2"><p>که در خاکش کند سلطان لولاک</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نمود از حله‌های جنتش دور</p></div>
<div class="m2"><p>که پوشد حله‌اش از رحمت و نور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>لباس مغفرت از حله بهتر</p></div>
<div class="m2"><p>نگاه رهبر از صد چله بهتر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز گندم یافت آدم ره بعالم</p></div>
<div class="m2"><p>بمعنی حکم حق بود آن بآدم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نبود ار امر حق در عین واقع</p></div>
<div class="m2"><p>کجا آدم بگندم بود طامع</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خود او را بهر دنیا کرد خلق او</p></div>
<div class="m2"><p>از آنرو داد بروی بطن و حلق او</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نبد مقصود ز آدم و ز سرشتش</p></div>
<div class="m2"><p>که جا پیوسته باشد در بهشتش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بدنیا می‌شد او بیشک روانه</p></div>
<div class="m2"><p>ازو اغوای شیطان بد بهانه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نکوتر گویمت از عالم عقل</p></div>
<div class="m2"><p>کند بردار ناسوت آدمی نقل</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که بعد از نظم اقلیم طبیعت</p></div>
<div class="m2"><p>بثانی رخت بندد بر حقیقت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رهد از تیه ظلمت نور گردد</p></div>
<div class="m2"><p>عوالم جمله زو معمور گردد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدون باعثی از ملک تجرید</p></div>
<div class="m2"><p>شود کی نفس کی بند تقیید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بود باعث تقاضای کمالش</p></div>
<div class="m2"><p>که بر اکل شجر آمد مثالش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز مبدء بعد او ظلم است و عصیان</p></div>
<div class="m2"><p>کند این ظلم بر خود نفس انسان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خود این ظلم ار چه از حکم قضا بود</p></div>
<div class="m2"><p>ادب را یک گویم آن ز ما بود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از آنرو آدم اظهار خطا کرد</p></div>
<div class="m2"><p>بحق «انا ظلمنا» را دعا کرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خطا هم جز که در فعل بشر نیست</p></div>
<div class="m2"><p>بکون وحدت از عصیان خیر نیست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در این عصیان هم آدم را کمال است</p></div>
<div class="m2"><p>که غفران حق از پی لامحال است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بحق فتوح گردد راه آدم</p></div>
<div class="m2"><p>ز رحمت کایدش والله اعلم</p></div></div>