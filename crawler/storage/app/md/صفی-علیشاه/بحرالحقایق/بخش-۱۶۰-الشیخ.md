---
title: >-
    بخش ۱۶۰ - الشیخ
---
# بخش ۱۶۰ - الشیخ

<div class="b" id="bn1"><div class="m1"><p>مراد از شیخ باشد مرشد راه</p></div>
<div class="m2"><p>ز حال خلق و عیب نفس آگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز علم شرع و آداب طریقت</p></div>
<div class="m2"><p>بود آگه هم از سر حقیقت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ره تکمیل را پیموده باشد</p></div>
<div class="m2"><p>بفرق بعد جمع آسوده باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود آگاه از امراض و آفات</p></div>
<div class="m2"><p>و را مقدور هم دفع بلیات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بداند هر دوئی را خواصش</p></div>
<div class="m2"><p>که باشد بر چه دردی اختصاصش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنین شیخی در او روی وریانیست</p></div>
<div class="m2"><p>ز ارشادش بجز حق مدعانیست</p></div></div>