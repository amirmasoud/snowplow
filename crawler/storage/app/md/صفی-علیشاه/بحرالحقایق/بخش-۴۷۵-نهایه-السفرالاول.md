---
title: >-
    بخش ۴۷۵ - نهایه‌السفرالاول
---
# بخش ۴۷۵ - نهایه‌السفرالاول

<div class="b" id="bn1"><div class="m1"><p>بود اول سفر را خود نهایت</p></div>
<div class="m2"><p>یقین رفع حجاب از وجه وحدت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حجاب کثرت از وحدت چه برخاست</p></div>
<div class="m2"><p>نهایت این سفر را در تقاضاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تعینهای اکوانی یکایک</p></div>
<div class="m2"><p>شود در این سفر منفی و مندک</p></div></div>