---
title: >-
    بخش ۱۸ - الاعیان الثابته
---
# بخش ۱۸ - الاعیان الثابته

<div class="b" id="bn1"><div class="m1"><p>شنو ز اعیان ثابت اصطلاحی</p></div>
<div class="m2"><p>ز صوفی گر که خود ز اهل صلاحی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود اعیان عالم علم الهی است</p></div>
<div class="m2"><p>که عین ممکنات آنجا کماهی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود مظبوط در بحر الحقایق</p></div>
<div class="m2"><p>بهر جا شرح اعیان با دقایق</p></div></div>