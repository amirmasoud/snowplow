---
title: >-
    بخش ۲۳۷ - عبدالخبیر
---
# بخش ۲۳۷ - عبدالخبیر

<div class="b" id="bn1"><div class="m1"><p>دگر هم زاولیا عبدالخبیر است</p></div>
<div class="m2"><p>که ز اشیاء آگه از لطف ضمیر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز هر چیزی حق او را مطلع کرد</p></div>
<div class="m2"><p>حجاب از چشم قلبش مرتفع کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز قبل و بعد هر شیئی خبر یافت</p></div>
<div class="m2"><p>ز سر و جهر او علم و اثر یافت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خود این باشد دلیل از کشف اعیان</p></div>
<div class="m2"><p>حقایق چون در اعیانست یکسان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در اعیان شد عیان شیئیت شیی</p></div>
<div class="m2"><p>بود ثابت در آن عینیت وی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر اعیان باشد اسماء را تعلق</p></div>
<div class="m2"><p>مبین گشت در بحرالحقایق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بود عبدالخبیر آنکس که این اسم</p></div>
<div class="m2"><p>شود زو جلوه‌گر در عالم جسم</p></div></div>