---
title: >-
    بخش ۵۱۸ - الهیولی
---
# بخش ۵۱۸ - الهیولی

<div class="b" id="bn1"><div class="m1"><p>هیولی باشد آن شیئ که صورت</p></div>
<div class="m2"><p>ازو ظاهر شود در وقت نسبت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدون صورتش شیئیتی نیست</p></div>
<div class="m2"><p>بشیئش جز بصورت نسبتی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود آن ماده یعنی که هر شیئی</p></div>
<div class="m2"><p>کند بیشک قبول صورت از وی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر آن شیئی بود قبل از نمودش</p></div>
<div class="m2"><p>مر اورا رتبه‌ئی کآرد ببودش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیولی گوید آنرا مرد عارف</p></div>
<div class="m2"><p>که از سر صور گردیده واقف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندارد حاجت شرح و بیانی</p></div>
<div class="m2"><p>بفهم آنرا تو خود گر نکته‌دانی</p></div></div>