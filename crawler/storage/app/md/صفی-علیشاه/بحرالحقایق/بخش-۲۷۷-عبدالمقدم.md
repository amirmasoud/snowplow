---
title: >-
    بخش ۲۷۷ - عبدالمقدم
---
# بخش ۲۷۷ - عبدالمقدم

<div class="b" id="bn1"><div class="m1"><p>بود عبدالمقدم را مسلم</p></div>
<div class="m2"><p>تجلی حق از اسم مقدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تمودش حق ز اهل صف اول</p></div>
<div class="m2"><p>که امر حق مقدم داشت وافضل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تقدم زو رسد بر اهل تعظیم</p></div>
<div class="m2"><p>که دارند از حق استحقاق تقدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم افعالی که تقدیمش مسلم</p></div>
<div class="m2"><p>بود واجب خود او دارد مقدم</p></div></div>