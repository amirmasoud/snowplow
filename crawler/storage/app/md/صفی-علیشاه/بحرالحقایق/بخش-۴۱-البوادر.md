---
title: >-
    بخش ۴۱ - البوادر
---
# بخش ۴۱ - البوادر

<div class="b" id="bn1"><div class="m1"><p>بوادر فجئه‌ئی باشد که بر دل</p></div>
<div class="m2"><p>ز غیب آید بقبض و بسط سائل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر باشد بوادر برگشایش</p></div>
<div class="m2"><p>ورودش روشن آید در نمایش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وگر بربستگی باشد ورودش</p></div>
<div class="m2"><p>تو تیره در نظر بینی چو دودش</p></div></div>