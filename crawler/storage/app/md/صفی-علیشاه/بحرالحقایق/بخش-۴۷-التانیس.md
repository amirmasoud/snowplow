---
title: >-
    بخش ۴۷ - التانیس
---
# بخش ۴۷ - التانیس

<div class="b" id="bn1"><div class="m1"><p>بود تانیس بر ظاهر تجلا</p></div>
<div class="m2"><p>مرید مبتدی را در تولا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تجلی فعلی است این نزد اصحاب</p></div>
<div class="m2"><p>شود ظاهر بصورتهای اسباب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تجلیها که سند هر تأنیس</p></div>
<div class="m2"><p>بحس ظاهر از تهلیل و تقدیس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا او را جلوه‌‌‌‌‌ها محسوس گردد</p></div>
<div class="m2"><p>مگر بر تصفیه مانوس گردد</p></div></div>