---
title: >-
    بخش ۳۴۷ - القشر
---
# بخش ۳۴۷ - القشر

<div class="b" id="bn1"><div class="m1"><p>دگر قشرت بظاهر رهنمون است</p></div>
<div class="m2"><p>که علم باطن اندر وی مصونست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شود محفوظ باطنها بظاهر</p></div>
<div class="m2"><p>چو در صندقها لعل و جواهر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رسد تا میوه از قشریم ناچار</p></div>
<div class="m2"><p>نشد بی‌قشر هرگز پخته اثمار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میان نی شود پرورده شکر</p></div>
<div class="m2"><p>دگر هم در صدفها در و گوهر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شریعت قشر و مغز آمد طریقت</p></div>
<div class="m2"><p>بنسبت همچنین باشد حقیقت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز صورت رو بمعنی گر توانی</p></div>
<div class="m2"><p>نما پس جمع صورت با معانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز قشرت قصد لب است این عیانست</p></div>
<div class="m2"><p>ولی در پوست این مغزت نهانست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهم این هر دو ملزومند و لازم</p></div>
<div class="m2"><p>عمل بر هر دو دارد مرد حازم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی گر زین دو شد از عبد مفقود</p></div>
<div class="m2"><p>عجب باشد که ره یابد بمقصود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شود در ترک صورت راه مشکل</p></div>
<div class="m2"><p>بندرت می‌رسد باری بمنزل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بحفظ قشر باید سعی نغزت</p></div>
<div class="m2"><p>مگر وقتی که شد پروده مغزت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رسیدت هم چه مغز از کف منه پوست</p></div>
<div class="m2"><p>که گوهرهای لب محفوظ در اوست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر آنکس را بود لعل و جواهر</p></div>
<div class="m2"><p>بکار افزون بدش صندوق و ساتر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کسی کز زرو گوهر باشد آزاد</p></div>
<div class="m2"><p>نخواهد حجره و صندوق فولاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وگر هم باشدش صندوق وحجره</p></div>
<div class="m2"><p>ز دزد ایمن بود کو کند حفره</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو دزد آمد نخواهد برد چیزی</p></div>
<div class="m2"><p>نیرزد بیت مفلس بر پشیزی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فقیه خشک جز ریش و ورم نیست</p></div>
<div class="m2"><p>گرش معنی نباشد هیچ غم نیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بدست آورده او دنیا و مالی</p></div>
<div class="m2"><p>حروف و نقش خالی از کمالی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بوقت مرگ بادش رفت و تن ماند</p></div>
<div class="m2"><p>همان هیکل که بود اندر کفن ماند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو گودنیا بد او را تا گه موت</p></div>
<div class="m2"><p>نبودش معنیی کز وی شود فوت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نماند از وی بدلها غیر نیشی‌</p></div>
<div class="m2"><p>چه باشد قیمت دستار و ریشی</p></div></div>