---
title: >-
    بخش ۱۴۵ - السفر
---
# بخش ۱۴۵ - السفر

<div class="b" id="bn1"><div class="m1"><p>سفر آمد به پیش ایمرد سیار</p></div>
<div class="m2"><p>رفیقی بایدت چالاک و هشیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سفر باشد زدار تن شدن سلب</p></div>
<div class="m2"><p>بسوی حق نمودن وجهه قلب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنزد سالکی کو رهسپار است</p></div>
<div class="m2"><p>سفرها شد معین آنکه جار است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود اول سفر سیر الی الله</p></div>
<div class="m2"><p>نمودن طی منزلها در این راه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رسیدن بر کمال عالم قلب</p></div>
<div class="m2"><p>شدن در آن تجلی محرم قلب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>افق باشد مبین در رتبه آنجا</p></div>
<div class="m2"><p>تجلیهای اسماء راست مبدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بود دوم سفر در سیر فی الله</p></div>
<div class="m2"><p>که آن اعلی افق شد نزد آگاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بود این واحدیت را نهایت</p></div>
<div class="m2"><p>شدن موصوف بر اوصاف حضرت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دگر سیم سفر آنرا که سمع است</p></div>
<div class="m2"><p>ترقی بر مقام عین جمع است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بود آنجا مقام قاب قوسین</p></div>
<div class="m2"><p>احد نگذاشت باقی نام اثنین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دوئی چون مرتفع شد بر نهایت</p></div>
<div class="m2"><p>بود در عین اوادنی ولایت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ولایت یعنی آنجا جا وحدنیست</p></div>
<div class="m2"><p>دوئیت رفت و باقی جز احد نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دگر باشد سفر در سیر بالله</p></div>
<div class="m2"><p>که فرق بعد جمع آمد بدلخواه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بود چارم سفر را نیک لایق</p></div>
<div class="m2"><p>که باز آید بتکمیل خلایق</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در اینجا فرق و جمع او مساویست</p></div>
<div class="m2"><p>بود در خلق اما غیر حق نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بود دارای کل این مراتب</p></div>
<div class="m2"><p>ولی زونیست پیدا غیر قالب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو دریایی که بیقعر و کرانست</p></div>
<div class="m2"><p>و لیکن زیر کف یکجا نهان است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر آن کف دید خوار و بی‌ادب ماند</p></div>
<div class="m2"><p>لب دریای رحمت تشنه لب ماند</p></div></div>