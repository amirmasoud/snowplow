---
title: >-
    بخش ۴۳۷ - المهیمون
---
# بخش ۴۳۷ - المهیمون

<div class="b" id="bn1"><div class="m1"><p>مهیمون از ملایک نوع خاصند</p></div>
<div class="m2"><p>که از قید عبودیت خلاصند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان محو جلال ذوالجلالند</p></div>
<div class="m2"><p>که فارغ از غم هجر و وصالند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز حق دارند اندر حق شهودی</p></div>
<div class="m2"><p>بر آنها نیست تکلیف سجودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مر آنها را ز استغراق محکم</p></div>
<div class="m2"><p>نه از عالم خبر باشد نه ز آدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آیات و احادیثنند آنها</p></div>
<div class="m2"><p>بعالون و کربوبیون مسما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صفی خواهد ز حق امداد و توفیق</p></div>
<div class="m2"><p>نماید در مهیمون تا که تحقیق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز حق چون عقل اول ما صدر بود</p></div>
<div class="m2"><p>وجودش ز آفرینش پیشتر بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز قبل از کل اشیاء خلق او شد</p></div>
<div class="m2"><p>گرفتار کمندش حلق او شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باو فرمود «اقبل» پیشتر شد</p></div>
<div class="m2"><p>جمالش دید و از خود بیخبر شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هنوز اندر هماندیدار غرق است</p></div>
<div class="m2"><p>بکلی بیخبر از جمع و فرق است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دگر فرمود «ادبر» ورتر گشت</p></div>
<div class="m2"><p>ازو حسن ازل مستورتر گشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از آن فرمود دو راز قرب ذاتش</p></div>
<div class="m2"><p>که باشد رو بنظم ممکناتش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ازو ایجاد پس ملک و ملک شد</p></div>
<div class="m2"><p>کمال آدم و نفس فلک شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نمیشد گر که بروی حکم «أدبر»</p></div>
<div class="m2"><p>کجا می‌گشت بر عالم مدبر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو شد دور از بساط قرب اقدم</p></div>
<div class="m2"><p>بنظم عالم و تکمیل آدم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بعنوان دگر جست او تقرب</p></div>
<div class="m2"><p>اطاعت کرد سجده آدم از حب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>محبت طاعت آرد این یقین است</p></div>
<div class="m2"><p>خود آنهم لامز عقل امین است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز آدم سجده بهر امتحان بود</p></div>
<div class="m2"><p>چه سر عشق در آدم نهان بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر آدم زاده این نکته دریاب</p></div>
<div class="m2"><p>بیاموز از ملایک عقل و آداب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بیان آمد ز مطلب دور ماندیم</p></div>
<div class="m2"><p>ز تحقیق نظر معذور ماندیم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>غرض بگزید عقل ذو فنونرا</p></div>
<div class="m2"><p>به او آراست اقلیم شئونرا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خطاب «أقبل» آمد سوی شه رفت</p></div>
<div class="m2"><p>در «ادبر» جانب نظم سپه رفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بود «اقبل» مقام اتصالش</p></div>
<div class="m2"><p>هو «ادبر» این ظهورات کمالش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در «اقبل» محو آن حسن و لقا شد</p></div>
<div class="m2"><p>در «ادبر» بانی نفس و قوا شد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز «اقبل» جمله رویش سوی حق گشت</p></div>
<div class="m2"><p>در «ادبر» رهنمای ما خلق گشت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در أقبل هیچش از اشیاء خبر نیست</p></div>
<div class="m2"><p>در أدبر شیئی از وی بی‌اثر نیست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در أقبل حسن او را دید و دل باخت</p></div>
<div class="m2"><p>در أدبر حکم او بشنید و جان باخت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بد أقبل اینکه خود را محو ما کن</p></div>
<div class="m2"><p>هم أدبر اینکه رسم ما سوا کن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در أقبل بر جمالش واله گردید</p></div>
<div class="m2"><p>در أدبر گردماهش هاله گردید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در أقبل حق رهش از هر طرف بست</p></div>
<div class="m2"><p>در أدبر فوج امکان گشت و صفت بست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در أقبل عندلیب باغ گل شد</p></div>
<div class="m2"><p>در أدبر عرض و فرش و جزء‌و کل شد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در اقبل عقل برتر از شئونست</p></div>
<div class="m2"><p>مهیمونست و دور از چند و چونست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در اقبل محو انوار جمالند</p></div>
<div class="m2"><p>بعین قرب و جمع اتصالند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بکلی بیخبر از خویش و غیرند</p></div>
<div class="m2"><p>ز حکم بعد و قرب و شر و خیرند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دگر گویند بعضی ز اهل تحقیق</p></div>
<div class="m2"><p>که نبود قولشان خالی ز تعمیق</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>که آن مهیمون برتر ز طولند</p></div>
<div class="m2"><p>برون از سلسله عقل و عقولند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اگر باشد چنین آنهم عجب نیست</p></div>
<div class="m2"><p>بود حرفی و نفیش از ادب نیست</p></div></div>