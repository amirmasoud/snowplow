---
title: >-
    بخش ۳۹۳ - محوالعبودیه و محو عین‌العبد
---
# بخش ۳۹۳ - محوالعبودیه و محو عین‌العبد

<div class="b" id="bn1"><div class="m1"><p>گرت محو عبودیت بود فهم</p></div>
<div class="m2"><p>بری از محوعین عبد هم سهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وی اسقاط اضافت از وجود است</p></div>
<div class="m2"><p>سوی اعیان که بودی بی نمود است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود اعیان شئون ذات حضرت</p></div>
<div class="m2"><p>که ظاهر گشت اندر واحدیت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آنحضرت بحکم عالمیت</p></div>
<div class="m2"><p>هویدا گشت از غیب هویت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خود اعیانند معدومات مطلق</p></div>
<div class="m2"><p>در آن گردید ظاهر هستی حق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وجود حق در آن ظاهر بذاتست</p></div>
<div class="m2"><p>بعلم آثارعین ممکنات است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بصورتها خود اعیانست معلوم</p></div>
<div class="m2"><p>هم اعیان نیست الا کون معدوم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وجود محض الا عین حق نیست</p></div>
<div class="m2"><p>اضافه غیر نسبت در نسق نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مر او را نیست در خارج وجودی</p></div>
<div class="m2"><p>که باشد فعل و تأثیرش ببودی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه افعال و تأثیرات تابع</p></div>
<div class="m2"><p>بود بهر وجود اندر مصانع</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه خود معدوم را فعل است و تأثیر</p></div>
<div class="m2"><p>نه موجود است غیر از حق بتقدیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پس او عابد بود با نسبت عبد</p></div>
<div class="m2"><p>تقید یافت چون در صورت عبد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه آنهم هست شانی از شئونات</p></div>
<div class="m2"><p>شئوناتی که باشد صادر از ذات</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بود معبود هم از حیث اطلاق</p></div>
<div class="m2"><p>بهر عبدی از این حیث است مشتاق</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو عین بد باقی در عدم دان</p></div>
<div class="m2"><p>عدم را کون علمی در رقم دان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بود پس عبد ممحو و عبودت</p></div>
<div class="m2"><p>بذات و وصف در عین حقیقت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بود تند ار در اینمعنی کمیت</p></div>
<div class="m2"><p>دلیل آمد رمیت ما رمیت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سه تن را حق بنحوی نیست ثالث</p></div>
<div class="m2"><p>دو صد شرک از ثلاثه گشت حادث</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سه تن را ور بود رابع خطا نیست</p></div>
<div class="m2"><p>چو او اندر شما ر ماسوا نیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بود پاک از شمار ماسوی الله</p></div>
<div class="m2"><p>ولیکن باشمار جمله همراه</p></div></div>