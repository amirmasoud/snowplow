---
title: >-
    بخش ۱۳۱ - السحق
---
# بخش ۱۳۱ - السحق

<div class="b" id="bn1"><div class="m1"><p>بود سحق از نمود خویش هجرت</p></div>
<div class="m2"><p>بتحت قهر سلطان حقیقت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ذهاب از رتبه و ترکیب و عادات</p></div>
<div class="m2"><p>بنزد کبریا و عظمت ذات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از این ترکیب ناجورت بسایند</p></div>
<div class="m2"><p>بترکیبی که بایستت نمایند</p></div></div>