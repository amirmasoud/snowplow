---
title: >-
    بخش ۳۵۱ - باب‌الکاف الکتاب المبین
---
# بخش ۳۵۱ - باب‌الکاف الکتاب المبین

<div class="b" id="bn1"><div class="m1"><p>کتاب بس مبینت لوح محفوظ</p></div>
<div class="m2"><p>بود کاشیاء در آن ضبط است و ملحوظ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نباشد خارج از وی خود وجودی</p></div>
<div class="m2"><p>ز رطب و یابس و نابود و بودی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود آن رطب و یابس اندر امثال</p></div>
<div class="m2"><p>مراد از عالم تفصیل و اجمال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنسبت آنهم ار باشی مشاهد</p></div>
<div class="m2"><p>مفصل‌تر یکی از دیگر آمد</p></div></div>