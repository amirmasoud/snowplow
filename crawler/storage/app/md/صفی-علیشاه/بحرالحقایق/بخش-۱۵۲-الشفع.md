---
title: >-
    بخش ۱۵۲ - الشفع
---
# بخش ۱۵۲ - الشفع

<div class="b" id="bn1"><div class="m1"><p>اگر گاهت سراغ از شفع باشد</p></div>
<div class="m2"><p>ز استحضار خلقت نفع باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محقق شد که اسماء معظم</p></div>
<div class="m2"><p>تحقق یافت بر خلق منظم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نبود ار آنکه شفع واحدیت</p></div>
<div class="m2"><p>سوی و ترالاحد جستی معیت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نبود اسماء حضرت را ظهوری</p></div>
<div class="m2"><p>بخلق این انضمام افکند شوری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمایشها هم اندر خلق مشهود</p></div>
<div class="m2"><p>شد از تأثیر شفع و وتر موجود</p></div></div>