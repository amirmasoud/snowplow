---
title: >-
    بخش ۲۴۳ - عبدالکبیر
---
# بخش ۲۴۳ - عبدالکبیر

<div class="b" id="bn1"><div class="m1"><p>دگر عبدالکبیر این ناگزیر است</p></div>
<div class="m2"><p>که او از کبریای حق کبیر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زکبر حق نه کبر غیر لایق</p></div>
<div class="m2"><p>نماید کبریائی بر خلایق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز کبر خود فنا شد کبریا یافت</p></div>
<div class="m2"><p>ز حق این کبریائی در فنا یافت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آن فضل و کمالی کش خداداد</p></div>
<div class="m2"><p>بزرگی بر تمام ما سوا داد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تجلی کرد از اسم کبیرش</p></div>
<div class="m2"><p>نمود از کبر خود صاحب سریرش</p></div></div>