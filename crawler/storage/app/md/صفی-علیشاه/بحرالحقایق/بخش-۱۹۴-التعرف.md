---
title: >-
    بخش ۱۹۴ - التعرف
---
# بخش ۱۹۴ - التعرف

<div class="b" id="bn1"><div class="m1"><p>پس از تحقیق عارف کن توقف</p></div>
<div class="m2"><p>یکی در حال ارباب تعرف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تعرف هم اگر نوبد ریائی</p></div>
<div class="m2"><p>رساند مرد را شاید بجائی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تعرف گر که باشد ز اعتقادی</p></div>
<div class="m2"><p>از او حاصل توان کردن مرادی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دگر باشد تعرفها باقسام</p></div>
<div class="m2"><p>ز هر یک گویمت رمزی به الهام</p></div></div>