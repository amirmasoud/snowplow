---
title: >-
    بخش ۳۴۳ - القبض
---
# بخش ۳۴۳ - القبض

<div class="b" id="bn1"><div class="m1"><p>شوداز قبض وقت مر دره تنگ</p></div>
<div class="m2"><p>ز وحشت پای رفتارش شود لنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود آن وحشتش از صد و هجران</p></div>
<div class="m2"><p>خلاف بسط کز انس است و احسان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود اغلب که بعد از بسط آید</p></div>
<div class="m2"><p>از آن سوء ادب کز بسط زاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز قبض و بسط ما را مدعا چیست</p></div>
<div class="m2"><p>خود آنرا فرق با خوف و رجا چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود خوف و رجا خود از بدو خوب</p></div>
<div class="m2"><p>توقع کش بداز مکروه و مرغوب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود خوفش ز مکروهی که آید</p></div>
<div class="m2"><p>رجایش هم ز مرغوبی که خواهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>و لیکن قبض و بسط اندر ظواهر</p></div>
<div class="m2"><p>تعلق دارد آن بر وقت حاضر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باینمعنی که حال نقد چونست</p></div>
<div class="m2"><p>بر او این نیل کوثر یا که خونست</p></div></div>