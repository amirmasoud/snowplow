---
title: >-
    بخش ۳۲۶ - الفتح المبین
---
# بخش ۳۲۶ - الفتح المبین

<div class="b" id="bn1"><div class="m1"><p>گرت فتح مبین باشد مبین</p></div>
<div class="m2"><p>خود آن فتح از ولایت شد معین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شود ز انوار اسماء الهی</p></div>
<div class="m2"><p>تجلیها بمرد ره کما هی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خود از فتح مبین است این عبارت</p></div>
<div class="m2"><p>که در «انا فتحنا» شد اشارت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد اینجا سالکان را محو یکسر</p></div>
<div class="m2"><p>ذنوب ما تقدم ما تاخر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز خود یعنی دو عالم کرده سلب او</p></div>
<div class="m2"><p>گذشته از صفات نفس و قلب او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گناه ما تأخیر ما تقدم</p></div>
<div class="m2"><p>حجاب قلب و نفس آمد مسلم</p></div></div>