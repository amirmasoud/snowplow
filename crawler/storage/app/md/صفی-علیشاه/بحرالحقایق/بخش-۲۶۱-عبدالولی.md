---
title: >-
    بخش ۲۶۱ - عبدالولی
---
# بخش ۲۶۱ - عبدالولی

<div class="b" id="bn1"><div class="m1"><p>مگر عبدلولی دور از تکلف</p></div>
<div class="m2"><p>ز استیلاست اولی بالتصرف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگرداند ز کل ماسوا رو</p></div>
<div class="m2"><p>ز هر سو رو نماید سوی بیسو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگیرد جز خدا را بهر خود دوست</p></div>
<div class="m2"><p>نگنجد از ولای دوست در پوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نبیند غیر آن کو بروی اولی است</p></div>
<div class="m2"><p>کند پس رو بهر سوسوی مولی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حقش زان مظهر اسم ولی کرد</p></div>
<div class="m2"><p>ولایت داد و بر کل معتلی کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خدا باشد ولی آنکه خارج</p></div>
<div class="m2"><p>ز ظلمت شد بنور اندر معارج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو ایمانش بحق شد عین واقع</p></div>
<div class="m2"><p>خود ایمانها بر او گردید راجع</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از آن حق در نبی او را ولی گفت</p></div>
<div class="m2"><p>نبی «من کنت مولاه علی» گفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تلای حقش شمع رسل کرد</p></div>
<div class="m2"><p>بحکم انما مولای کل کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ولایت پس یقین خاص علی بود</p></div>
<div class="m2"><p>که حق را به نص حق ولی بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جز او کس در ولایت نیست منصوص</p></div>
<div class="m2"><p>بر او این اولویت گشت مخصوص</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پس آنها کاولیا و صالحینند</p></div>
<div class="m2"><p>علی را در ولایت جا نشینند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یکی زان جمله را عبدالولی دان</p></div>
<div class="m2"><p>ولیش با تو لای علی دان</p></div></div>