---
title: >-
    بخش ۲۴۲ - عبدالعلی
---
# بخش ۲۴۲ - عبدالعلی

<div class="b" id="bn1"><div class="m1"><p>بود عبدالعلی آنکس که برتر</p></div>
<div class="m2"><p>بود قدر وی از اقران سراسر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیال و همتش پیوسته عالی</p></div>
<div class="m2"><p>بود از بهر تحصیل معالی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز اقران و ز اخوان معلی</p></div>
<div class="m2"><p>بکسب رفعت است ادارکش اعلی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رسد تا بر شئونات علیه</p></div>
<div class="m2"><p>کند درک کمالات سنیه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صفی را صاف‌تر زین مشربی هست</p></div>
<div class="m2"><p>بتحقیق معانی مطلبی هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کند عبدالعلی در رتبه کامل</p></div>
<div class="m2"><p>هر آنچه اعتلا را هست قابل</p></div></div>