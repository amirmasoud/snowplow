---
title: >-
    بخش ۳۱۸ - الغنا
---
# بخش ۳۱۸ - الغنا

<div class="b" id="bn1"><div class="m1"><p>غنا مالکیست بی‌آسیب و مالی</p></div>
<div class="m2"><p>که آنرا نیست از حیثی زوالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غنای تام جز حق را محال است</p></div>
<div class="m2"><p>که مستغنی بذات او در کمال است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غنای عبد از حیث فنا شد</p></div>
<div class="m2"><p>که مستغنی بحق از ماسوا شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو شداز هستی یکذات فائز</p></div>
<div class="m2"><p>بهستیهاست حکمش جمله جایز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر آنکس ره بدریا برد دریاست</p></div>
<div class="m2"><p>حباب و موج و قطره جمله او راست</p></div></div>