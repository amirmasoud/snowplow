---
title: >-
    بخش ۲۳ - الآن الدائم
---
# بخش ۲۳ - الآن الدائم

<div class="b" id="bn1"><div class="m1"><p>ز آن دایم اگر با افتتاحی</p></div>
<div class="m2"><p>ز من کن گوش نیکو اصطلاحی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود آن امتداد پادشاهی</p></div>
<div class="m2"><p>ظهور حضرت باری کماهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آن مدت ازل عین ابددان</p></div>
<div class="m2"><p>بر این معنی کلام ما سند دان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ازل را با ابد با وقت حاضر</p></div>
<div class="m2"><p>یکی دان هم بباطن هم بظاهر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمان سر مد بود در اصل و باطن</p></div>
<div class="m2"><p>هم آنات زمانی را مواطن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود ثابت بحال خویش و قایم</p></div>
<div class="m2"><p>از آن دانیم آن را آن دایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تغیرها در او همچون نقوش است</p></div>
<div class="m2"><p>بظاهر ناطق و باطن خموش است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در آنجا صبح و شام و روز و شب‌نیست</p></div>
<div class="m2"><p>بجز یک آن واحد نز درب نیست</p></div></div>