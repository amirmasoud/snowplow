---
title: >-
    بخش ۱۳ - الاسم
---
# بخش ۱۳ - الاسم

<div class="b" id="bn1"><div class="m1"><p>ز اسم ار اصطلاح ما بدانی</p></div>
<div class="m2"><p>عجب باشد اگر در لفظ مانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مراد از اسم نزد ما مسمی است</p></div>
<div class="m2"><p>که او را اعتبار لفظ و معنی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوجهی اسم گویم عین ذات است</p></div>
<div class="m2"><p>که در وی اعتبارات صفاتست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مسمی را در اسماء مستتردان</p></div>
<div class="m2"><p>اثرها از مسمی مستمردان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود آب آنکه اندر جوی جاریست</p></div>
<div class="m2"><p>مراد از آب این با عوالف نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مسمی غیر از الفاظ مجازیست</p></div>
<div class="m2"><p>که در وی اصطلاح ترک و تازیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عرب گر ما خواند یا عجم آب</p></div>
<div class="m2"><p>تو از وی آنکه مقصود است دریاب</p></div></div>