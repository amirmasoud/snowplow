---
title: >-
    بخش ۹۴ - باب الدال الدبور
---
# بخش ۹۴ - باب الدال الدبور

<div class="b" id="bn1"><div class="m1"><p>بود آن صولت نفس عقورت</p></div>
<div class="m2"><p>باستیلا چون باد دبورت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز غرب طبع جسمانی بمشهور</p></div>
<div class="m2"><p>و زد کش خواند عارف مغرب نور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دگر باد صبا کز مشرق آید</p></div>
<div class="m2"><p>باستیلا چو روح مشفق آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آنرو گفت پیغمبر که نصرت</p></div>
<div class="m2"><p>مرا بود از صبا در صبح رحمت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همان باد دبور آمد هلاکت</p></div>
<div class="m2"><p>گروه عاد را با صد فلاکت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا یعنی از آنمشرق فتوح است</p></div>
<div class="m2"><p>کز و اندر تموج باد روح است</p></div></div>