---
title: >-
    بخش ۴۱۴ - المضاهاه بین‌الشئون و الحقایق
---
# بخش ۴۱۴ - المضاهاه بین‌الشئون و الحقایق

<div class="b" id="bn1"><div class="m1"><p>مضاهات آنکه بر تعیین لایق</p></div>
<div class="m2"><p>بمابین شئونست و حقایق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود آنرتبه حقایقهای کونیست</p></div>
<div class="m2"><p>همان جز بر الهی مبتنی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حقایق کان الهیه بمعناست</p></div>
<div class="m2"><p>شئون ذات مطلق را خود اسماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس این اکوان مر اسماء را ضلالست</p></div>
<div class="m2"><p>شئون را ظل هم اسماء بر کمالست</p></div></div>