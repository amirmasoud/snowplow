---
title: >-
    بخش ۷ - احدیه الجمع
---
# بخش ۷ - احدیه الجمع

<div class="b" id="bn1"><div class="m1"><p>احدیت که جمع اندر بیانست</p></div>
<div class="m2"><p>بلا اسقاط ولا اثبات آنست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلا اثبات ز آن باشد محقق</p></div>
<div class="m2"><p>که از هر نسبتی فرد است و مطلق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلا اسقاط ز آنره که در وی</p></div>
<div class="m2"><p>نسبها مندرج باشد پیاپی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود خود جمع در این پاک حضرت</p></div>
<div class="m2"><p>احدیت دگر هو واحدیت</p></div></div>