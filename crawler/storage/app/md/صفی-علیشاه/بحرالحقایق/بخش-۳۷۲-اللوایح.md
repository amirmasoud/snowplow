---
title: >-
    بخش ۳۷۲ - اللوایح
---
# بخش ۳۷۲ - اللوایح

<div class="b" id="bn1"><div class="m1"><p>لوایح لایحه را لفظ جمع است</p></div>
<div class="m2"><p>که در جمع مکاشف همچو شمع است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شود آن لایحه در حکم اشراق</p></div>
<div class="m2"><p>بکشف معنوی و صوری اطلاق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خود آن صوری مگر کشف مثال است</p></div>
<div class="m2"><p>که برحس لایح اندر انتقال است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دگر آن معنوی آمد هویدا</p></div>
<div class="m2"><p>ز غیب حضرت اقدس اعلی</p></div></div>