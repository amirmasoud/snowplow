---
title: >-
    بخش ۳۹۷ - المحادثه
---
# بخش ۳۹۷ - المحادثه

<div class="b" id="bn1"><div class="m1"><p>محادث با اضافه تا خطاب است</p></div>
<div class="m2"><p>که سوی عبد وارد ز آنجنابست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیوشد آن بصورت نیک‌بختی</p></div>
<div class="m2"><p>چو موسی کان نیوشید از درختی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی کانرا شنید و گفت حق بود</p></div>
<div class="m2"><p>میانچی گر بصورت ما خلق بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حدیثش را یکی از گوش سریافت</p></div>
<div class="m2"><p>یکی از سمع ادراک و نظر یافت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خطابی را که موسی از شجر یافت</p></div>
<div class="m2"><p>صفی آثارش از هر خشک و تر یافت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زبان ماسوا ناطق بر این است</p></div>
<div class="m2"><p>هر آن شیئی خطاب رب دینست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ولی در این مقام از گوش ظاهر</p></div>
<div class="m2"><p>خطابش را نیوشد مرد سایر</p></div></div>