---
title: >-
    بخش ۲۲۳ - عبدالوهاب
---
# بخش ۲۲۳ - عبدالوهاب

<div class="b" id="bn1"><div class="m1"><p>دگر باشد ولیی عبد وهاب</p></div>
<div class="m2"><p>ز حق بر موهبتها دارد القاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ببخشد هر چه هر کس را سزاوار</p></div>
<div class="m2"><p>بود، وان حق او باشد در آثار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون رنگ و بو که باشد لایق گل</p></div>
<div class="m2"><p>دگر فریاد و افغان بهر بلبل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدینسان جمله موجودات عالم</p></div>
<div class="m2"><p>برد زا و بخش خود هر شیئی هر دم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببخشد حق هر کس بی زاغماض</p></div>
<div class="m2"><p>بدون بغض وحب خالی ز اغراض</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود جودش چو ظل بر خلق ممدود</p></div>
<div class="m2"><p>که هست او واسطه بر جودذیجود</p></div></div>