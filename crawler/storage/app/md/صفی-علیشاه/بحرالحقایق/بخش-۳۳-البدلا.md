---
title: >-
    بخش ۳۳ - البدلاء
---
# بخش ۳۳ - البدلاء

<div class="b" id="bn1"><div class="m1"><p>ز بدلا یعنی ابدال مکرم</p></div>
<div class="m2"><p>رجاع سبعه در اطراف عالم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شنو رمزی از ایشان چونسفر کرد</p></div>
<div class="m2"><p>یکی آید بجای او دگر مرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جسد را ور نهد برجای شاید</p></div>
<div class="m2"><p>رود بی‌تن بهر جائیکه باید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باین معنی سخن نیکوتر آید</p></div>
<div class="m2"><p>بمعنای دگر ابعد نماید</p></div></div>