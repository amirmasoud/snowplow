---
title: >-
    بخش ۳۲۹ - الفرق الاول
---
# بخش ۳۲۹ - الفرق الاول

<div class="b" id="bn1"><div class="m1"><p>یکی از فرق اول گویمت راست</p></div>
<div class="m2"><p>بخلق آن احتجاب از حقتعالی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بقای رسم خلقیت در این حال</p></div>
<div class="m2"><p>بحال خود تواند بود بیقال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بقای رسم خلقیت کدام است</p></div>
<div class="m2"><p>بخلق از حق حجاب مستدام است</p></div></div>