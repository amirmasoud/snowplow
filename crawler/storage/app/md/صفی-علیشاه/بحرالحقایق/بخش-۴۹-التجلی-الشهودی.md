---
title: >-
    بخش ۴۹ - التجلی الشهودی
---
# بخش ۴۹ - التجلی الشهودی

<div class="b" id="bn1"><div class="m1"><p>تجلی شهودی آن ظهور است</p></div>
<div class="m2"><p>که از حقل جلوه‌گر بر اسم نور است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در اینجا شد ظهور ذات سبحان</p></div>
<div class="m2"><p>یکی با صورت اسماء بر اکوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دم رحمن که ایجاد خلایق</p></div>
<div class="m2"><p>باو فرمود حق برقدر لایق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تجلی شهودی دان با شهاد</p></div>
<div class="m2"><p>تو گل کل زان نفس گشتند ایجاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رسید اینجا چو سالک سیر راهش</p></div>
<div class="m2"><p>تجلی شهود است از الهش</p></div></div>