---
title: >-
    بخش ۴۷۹ - النوال
---
# بخش ۴۷۹ - النوال

<div class="b" id="bn1"><div class="m1"><p>نوال آن خاص اهل قرب و حال است</p></div>
<div class="m2"><p>و ز آنها خاص افراد رجال است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بمعنی از رضا دان خلعت او را</p></div>
<div class="m2"><p>که بخشد ذوالجلال از رحمت او را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بخلعتها که بر هر فرد خاصش</p></div>
<div class="m2"><p>دهد او بر نوال است اختصاصش</p></div></div>