---
title: >-
    بخش ۳۶۸ - لسان الحق
---
# بخش ۳۶۸ - لسان الحق

<div class="b" id="bn1"><div class="m1"><p>لسان‌الحق بود انسان فایق</p></div>
<div class="m2"><p>که باشد مظهر او بر اسم ناطق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کند بر اهل صورت وصف ظاهر</p></div>
<div class="m2"><p>دهد بر مرد معنی سیر قاهر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بقدر فهم و استعداد مردم</p></div>
<div class="m2"><p>کند هر جا بعنوانی تکلم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لسانش در شریعت گوید از حق</p></div>
<div class="m2"><p>وجودش سازد از حق کشف مطلق</p></div></div>