---
title: >-
    بخش ۲۴۶ - عبدالحسیب
---
# بخش ۲۴۶ - عبدالحسیب

<div class="b" id="bn1"><div class="m1"><p>دگر هم ز اولیا عبدالحسیب است</p></div>
<div class="m2"><p>که فیض حسبی اللهش نصیب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنفس خود بود دایم محاسب</p></div>
<div class="m2"><p>نگر حتی بر انفاسش مراقب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر تا یک نفس نکشد بعفلت</p></div>
<div class="m2"><p>ز حق دارد چنین توفیق و فرصت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بجان دایم در این اندیشه باشد</p></div>
<div class="m2"><p>حساب نفس او را پیشه باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حساب نفس هر کس را نصیب است</p></div>
<div class="m2"><p>بعالم مظهر اسم حسیب است</p></div></div>