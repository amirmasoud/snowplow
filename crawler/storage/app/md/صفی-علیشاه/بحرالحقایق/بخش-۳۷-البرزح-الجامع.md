---
title: >-
    بخش ۳۷ - البرزح الجامع
---
# بخش ۳۷ - البرزح الجامع

<div class="b" id="bn1"><div class="m1"><p>دگر از برزخ جاکع بتفسیر</p></div>
<div class="m2"><p>کنم در واحدیت بر تو تعبیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود آن اول تعین در کلامست</p></div>
<div class="m2"><p>در آن برزخ مرایابی ظلامست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود این برزخ اولی و اعظم</p></div>
<div class="m2"><p>ز عالمهای و برزخها مقدم</p></div></div>