---
title: >-
    بخش ۶۸ - الجنائب
---
# بخش ۶۸ - الجنائب

<div class="b" id="bn1"><div class="m1"><p>جنائب صاحب اعمال خیرند</p></div>
<div class="m2"><p>که دایم در نفوس آنها بسیرند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بحمل زاد تقوی ره نوردند</p></div>
<div class="m2"><p>مقام قلب را واصل نگردند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همی بانشد در سیر الی الله</p></div>
<div class="m2"><p>جدا از سیر فی الهند در راه</p></div></div>