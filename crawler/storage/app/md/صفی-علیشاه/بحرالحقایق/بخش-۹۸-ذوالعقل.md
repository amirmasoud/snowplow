---
title: >-
    بخش ۹۸ - ذوالعقل
---
# بخش ۹۸ - ذوالعقل

<div class="b" id="bn1"><div class="m1"><p>بظاهر خلق و باطن هر که حق دید</p></div>
<div class="m2"><p>ذوی‌العقل است و سرما خلق دید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در اینحال است حق بر خلق مرآت</p></div>
<div class="m2"><p>که محجوب است مطلق در شئونات</p></div></div>