---
title: >-
    بخش ۱۸۶ - الطمس
---
# بخش ۱۸۶ - الطمس

<div class="b" id="bn1"><div class="m1"><p>دگر طمس ار که سیرت در علوم است</p></div>
<div class="m2"><p>ذهاب مرد بسیار از رسوم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در اوصاف و ظهور نور الانوار</p></div>
<div class="m2"><p>که آمدیار و رفت از پیش سیار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شود فانی ز اوصاف و جهاتش</p></div>
<div class="m2"><p>بدل گردد بوصف حق صفاتش</p></div></div>