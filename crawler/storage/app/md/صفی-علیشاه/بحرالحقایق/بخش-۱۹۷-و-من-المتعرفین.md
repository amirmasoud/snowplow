---
title: >-
    بخش ۱۹۷ - و من المتعرفین
---
# بخش ۱۹۷ - و من المتعرفین

<div class="b" id="bn1"><div class="m1"><p>گروهی کاهل علم و اختصاصند</p></div>
<div class="m2"><p>تعرف را بطور و طرز خاصند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به برهان می‌کنند اثبات توحید</p></div>
<div class="m2"><p>ولی خود پیرو ظنند و تقلید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دهند از بیخودی درس معارف</p></div>
<div class="m2"><p>بجای خود ولی چون سنگ واقفند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کفایت کرده او بر لفظ و حرفی</p></div>
<div class="m2"><p>کز و هرگز نشاید بست طرفی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه سود از اینهمه الفاظ عالی</p></div>
<div class="m2"><p>که محی‌الدین نوشته یا غزالی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر گفتند آنها بهر این است</p></div>
<div class="m2"><p>که آید در ره آن کاهل یقین است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه بهر آنکه در لفظش وساوس</p></div>
<div class="m2"><p>کنند ارباب ظاهر در مجالس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در این الفاظ چون گشتند کامل</p></div>
<div class="m2"><p>شوند از کسب بمعنی سخت غافل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نشد زین علمشان حاصل کمالی</p></div>
<div class="m2"><p>بغیر از گفتگو و قیل و قالی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیان فقر کاهلش خاص بودند</p></div>
<div class="m2"><p>به بحر معرفت غواص بودند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باهل قشر و صورت منتسب شد</p></div>
<div class="m2"><p>بلفظ بی حقیقت منقلب شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گرش گوئی معارف گر قبولست</p></div>
<div class="m2"><p>چرا خاطر ترا زاهلش ملول است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بلفظش قائلی بی‌فعل چونست</p></div>
<div class="m2"><p>گر این آبست چون بهره تو خونست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نه تنها نیستی حامل بگفتار</p></div>
<div class="m2"><p>کز اهلش هم کنی پیوسته انکار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بگوید ما ولی را با دلایل</p></div>
<div class="m2"><p>شناسیم اینکه بینی نیست کامل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از آن غافل که فرض او بحث نیست</p></div>
<div class="m2"><p>صفات اولیا بر یک نسق نیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر آن یک را صفات و خلق خاصی است</p></div>
<div class="m2"><p>مگر بر وصف حقشان کاختصاصی است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ولی میزان که کردی فرض وهمست</p></div>
<div class="m2"><p>عجب دارم که پنداری ز فهم است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو آن میزان که کردی فرض و همست</p></div>
<div class="m2"><p>عجب دارم که پنداری ز فهم است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر میزان تو دور از خطا بود</p></div>
<div class="m2"><p>دلت مرات نور اولیا بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو خود گوئی که عالم بیولی نیست</p></div>
<div class="m2"><p>خلافت بی‌عمر حق بی‌علی نیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شناسی هم صفات و خلق و خورا</p></div>
<div class="m2"><p>چرا نشناختی عمری پس او را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اگر گوئی ندیدم ناشناسی</p></div>
<div class="m2"><p>و گر گوئی نباشد ناسپاسی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کسی که آرد دلیل اثبات شی را</p></div>
<div class="m2"><p>شناسد هم بوصف و رسم ویرا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بعمری گرنجست او را دغل بود</p></div>
<div class="m2"><p>دلش بی‌نور و علمش بی‌عمل بود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نه او را ذوق باشد نه بصیرت</p></div>
<div class="m2"><p>نه تحقیقش بکس برهان و حجت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>غرض از لفظ عرفان حاصلی نیست</p></div>
<div class="m2"><p>تو را تا رهنمای کاملی نیست</p></div></div>