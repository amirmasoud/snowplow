---
title: >-
    بخش ۱۸۵ - التفریق بین الکامل و الغافل
---
# بخش ۱۸۵ - التفریق بین الکامل و الغافل

<div class="b" id="bn1"><div class="m1"><p>کسی کش نیست تاب ترک نانی</p></div>
<div class="m2"><p>کجا بر ترک جان دارد توانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نداد از دست مردی گردکانرا</p></div>
<div class="m2"><p>دهد گو چون زمنی و آسمان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نکرده مرد زاهد ترک خشتی</p></div>
<div class="m2"><p>گذشته لیک آسان از بهشتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گذشته از بهشت و حوض کوثر</p></div>
<div class="m2"><p>ولی نگذشته از دانگی قلندر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو را گوید جهانرا اهل که برده است</p></div>
<div class="m2"><p>ولی خود ترک یکجوزی نکرده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرش گوئی چه شد کین بر تو نور است</p></div>
<div class="m2"><p>ولی بر دیگران نقص و قصور است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جوابت راز پیش او کرده حاضر</p></div>
<div class="m2"><p>کزین معنی بود فهم تو قاصر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منم سالم، مزاج تو سقیم است</p></div>
<div class="m2"><p>مرا نارد زیان بهر توبیم است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز بهر غوره سرما بس زیانست</p></div>
<div class="m2"><p>ولی انگور از وی در امانست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ندارد بهر من دنیا زیانی</p></div>
<div class="m2"><p>که بگذشتستم از هر امتحانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تمام خورد و خوابم ذکر و نور است</p></div>
<div class="m2"><p>همیشه قلب و روحم در حضور است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جهان بر دوش مؤمن بار نبود</p></div>
<div class="m2"><p>خورد گر عالمی بسیار نبود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر بینی که بند مال و جاهم</p></div>
<div class="m2"><p>نخواهد گشت اینا سد راهم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جوابش گو که گر تو رهنمایی</p></div>
<div class="m2"><p>چه شد کز همرهان خود جدایی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خود آنمردان که صاحب راه بودند</p></div>
<div class="m2"><p>تو خود گوئی ولی الله بودند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چرا دنیا بر آنها مختصر بود</p></div>
<div class="m2"><p>جهان از چشم سوزن تنگ‌تر بود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو مستثنی شدی زانجمله چون شد</p></div>
<div class="m2"><p>که بر تو آب و براطیاب خون شد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همه عمرت بهر روزی بهر جا</p></div>
<div class="m2"><p>گذشت اندر تمنا و تقاضا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تملق گو ز هر صدر و امیری</p></div>
<div class="m2"><p>اسیران طبیعت را اسیری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که بنویسند از بهرت سجلی</p></div>
<div class="m2"><p>فلانکس مرد و پیدا شد محلی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نباشد انفعالی هیچ از ینت</p></div>
<div class="m2"><p>که بنویسند فخرالعارفینت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فلانی ز اهل عرفان و سلوک است</p></div>
<div class="m2"><p>لهذا لایق جود ملوک است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر او این مبلغ استمرار شاه است</p></div>
<div class="m2"><p>که شیخ راه و پنیر خانقاه است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ندانی خورده بینان در کمینند</p></div>
<div class="m2"><p>همه این قوم گویند این چنیند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مثال غرقه‌بند هر حشیشی</p></div>
<div class="m2"><p>بر آنی خوش که قطب وقت خویشی‌</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زیان گوئی ندارد این فعالم</p></div>
<div class="m2"><p>منم درویش و کامل نی عیالم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بدعوی رهنمای جبرئیلی</p></div>
<div class="m2"><p>بمعنی خود بگنجشکی دخیلی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>غرض نزدیک هر کس کنجکاو است</p></div>
<div class="m2"><p>طریقت نیست اینهاریش گاو است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>طریقت پیشه بودن کار مرد است</p></div>
<div class="m2"><p>نه هر دامن سواری ره نورد است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بود قصد صفی زین نکتها باز</p></div>
<div class="m2"><p>که گردد گوهر از خر مهره ممتاز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نپندارد کسی کاهل طریقت</p></div>
<div class="m2"><p>بدینسانند مغلوب طبیعت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تمام این معانی با دقایق</p></div>
<div class="m2"><p>شود مکشوف از بحرالحقایق</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در این دفترکه اصل جمع و خرج است</p></div>
<div class="m2"><p>حساب جزء و کلت جمله درج است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز اول گر بخوانی تا بآخر</p></div>
<div class="m2"><p>شبه را بازبشناسی ز گوهر</p></div></div>