---
title: >-
    بخش ۴۶۷ - الاخلاص
---
# بخش ۴۶۷ - الاخلاص

<div class="b" id="bn1"><div class="m1"><p>دگر ضد ریا صدق است و اخلاص</p></div>
<div class="m2"><p>که ملحوظت نباشد عام یا خاص</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخن بر میل نااهلان نگفتن</p></div>
<div class="m2"><p>گهر بر طبع بوجهلان نسفتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلوصت خلق باشد نی تخلق</p></div>
<div class="m2"><p>بیانت صدق باشد نی تملق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگوئی مدح دیاری بسالوس</p></div>
<div class="m2"><p>نجوئی ذم موجودی بافسوس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه در رأیت اسف باشد نه اعراض</p></div>
<div class="m2"><p>نه در حالت طرف باشد نه اغماض</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه تهمت بر کسی بندی نه تعریف</p></div>
<div class="m2"><p>نه تعظیم از کسی خواهی نه تشریف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو نفسی گردد اینسان مطمئنه</p></div>
<div class="m2"><p>بدون و هم و تخییل و مظنه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شنید از جان خطاب ارجعی را</p></div>
<div class="m2"><p>بداد از سر جواب ارجعی را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز موضوعات اعراضیه برگشت</p></div>
<div class="m2"><p>برب مرضیه و راضیه برگشت</p></div></div>