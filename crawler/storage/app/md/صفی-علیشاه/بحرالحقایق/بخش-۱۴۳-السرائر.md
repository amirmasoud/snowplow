---
title: >-
    بخش ۱۴۳ - السرائر
---
# بخش ۱۴۳ - السرائر

<div class="b" id="bn1"><div class="m1"><p>شنو باز از سرائر بی ز آثار</p></div>
<div class="m2"><p>بحق است انمحاق مرد سیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوانمردان که راه وصل پویند</p></div>
<div class="m2"><p>و را محق از وصول تام گویند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نبی فرمود زانرو لی مع‌الله</p></div>
<div class="m2"><p>کز آن حالت نباشد غیر آگاه</p></div></div>