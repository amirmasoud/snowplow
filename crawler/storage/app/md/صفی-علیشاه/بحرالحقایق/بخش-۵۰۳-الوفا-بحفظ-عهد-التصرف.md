---
title: >-
    بخش ۵۰۳ - الوفاء بحفظ عهد التصرف
---
# بخش ۵۰۳ - الوفاء بحفظ عهد التصرف

<div class="b" id="bn1"><div class="m1"><p>وفا بر حفظ آن عهد تصرف</p></div>
<div class="m2"><p>چه باشد در مقامات تصوف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که بر عجز و عبودیت اعادت</p></div>
<div class="m2"><p>بود او را بگاه خرق عادت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شود ز او هر چه ظاهر در علامات</p></div>
<div class="m2"><p>تصرفها و اعجاز و کرامات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فزاید هر زمان بر انکسارش</p></div>
<div class="m2"><p>بعجز و بندگی و اضطرارش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تجاوز ورز حد خود نماید</p></div>
<div class="m2"><p>خود او اصلا کرامت را نشاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه حد عبد عجز و انکسار است</p></div>
<div class="m2"><p>برون رفت ارز حد مطرو دو خوار است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه افعال مطرود است مطرود</p></div>
<div class="m2"><p>بود نقص و کمالش جمله نابود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز هم دورست فقر و خود نمائی</p></div>
<div class="m2"><p>عبودیت کجا و کبریائی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز فقر باطل این باشد علامت</p></div>
<div class="m2"><p>که دارد خودنمائی در کرامت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از آن غافل که چشمش و خدا بست</p></div>
<div class="m2"><p>خیال فاسدش بر ادعا بست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که تا دانند اهل فهم و هوشش</p></div>
<div class="m2"><p>در این ره خود نما و دین فروشش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اناالحق گفت فرعون و ریا بود</p></div>
<div class="m2"><p>و گر منصور می‌گفت از فنا بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اناالحق گفتن از عمدت بود دام</p></div>
<div class="m2"><p>خود این دعوی در انسان دارد اقسام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سخن گر بشمرم بسیار گردد</p></div>
<div class="m2"><p>ولی تا خفته‌ئی بیدار گردد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نشانی وانمایم بهر عاقل</p></div>
<div class="m2"><p>همین کافیست اندر حق و باطل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر آن نفیی که از خود باشد اثبات</p></div>
<div class="m2"><p>اناالحق گفتن از ژاژ است و طامات</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فلانی پا بود یعنی سرم من</p></div>
<div class="m2"><p>ازو پیدا و پنهان بهترم من</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کلام بهترم ماند از عزازیل</p></div>
<div class="m2"><p>صفی از خاک و من زاتش بتکمیل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>انا گفت او که دانند اهل افلاک</p></div>
<div class="m2"><p>خود او را چیست قدر و وزن و ادراک</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کسی کاندر وجود او مستقل نیست</p></div>
<div class="m2"><p>عجب دان کز نمود خود خجل نیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بنزد آنکه او را داده هستی</p></div>
<div class="m2"><p>أنا گوید ز کبر و خودپرستی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خود اعجاز انبیارانه از أنا بود</p></div>
<div class="m2"><p>بد او فانی وز او ناطق خدا بود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دعاوی کز نمود و خودنمائی است</p></div>
<div class="m2"><p>بشیطان روئی اظهار خدائیست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کراماتی کز او اندر نمود است</p></div>
<div class="m2"><p>ز بهر و زن و معیار وجود است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کزان گردد عیان اندیشه او</p></div>
<div class="m2"><p>که بر شیداست دانی پیشه او</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بعهد ما که حاجت بر کرامات</p></div>
<div class="m2"><p>نباشد اندر اظهار مقامات</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کند هر کس بر نطق و زبانی</p></div>
<div class="m2"><p>هر آندعوی که آید در گمانی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گذشته است آنکه در دعوی و حالی</p></div>
<div class="m2"><p>بود لازم کرامت یا کمالی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز دعویها غرض فقر و فنا به</p></div>
<div class="m2"><p>ز اعجاز و کرامتها صفا به</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گریزان نی ز دیو و اژدها باش</p></div>
<div class="m2"><p>بدو راز داعیان خودنما باش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>که دعوی ضد فقر است و تصوف</p></div>
<div class="m2"><p>خلاف حفظ آن عهد تصرف</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>وفا بر حفظ آن عهد انکسار است</p></div>
<div class="m2"><p>یکی ز اوصاف صوفی افتقار است</p></div></div>