---
title: >-
    بخش ۳۳۵ - الفرق بین‌الکمال و النقص و الشرف و الخسه
---
# بخش ۳۳۵ - الفرق بین‌الکمال و النقص و الشرف و الخسه

<div class="b" id="bn1"><div class="m1"><p>بسی فرق است بین چار رتبت</p></div>
<div class="m2"><p>کمال و هم شرف هم نقص و خست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کمال آمد عبارت زینکه حاصل</p></div>
<div class="m2"><p>شود جمع حقایق بهر کامل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حقایق‌های کونی والهی</p></div>
<div class="m2"><p>کند جمعیت اندر وی کماهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شود او مظهر اوصاف علیا</p></div>
<div class="m2"><p>در او مجموع گردد کل اسما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ظهور حق در او بر وجه اکمل</p></div>
<div class="m2"><p>شود و اندر حقایق باشد افضل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بجمعیت حظوظ اوست مختص</p></div>
<div class="m2"><p>هر آن حظش اقل است اوست انقص</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هنوزش هست تا منزل مسافت</p></div>
<div class="m2"><p>بود ابعد هم از رتبه خلافت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نقایص باز بر مقدار راهست</p></div>
<div class="m2"><p>از و تا چند منزل براله است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بود اما شرف رفع وسایط</p></div>
<div class="m2"><p>میان عبد و موجد در روابط</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وسایط هر چه کمتر اشرفست او</p></div>
<div class="m2"><p>ز لفطف حق وجودش الطف است او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هم احکام وجوب او را بمشرب</p></div>
<div class="m2"><p>ابراحکام امکان باشد اغلب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دگر بسیار باشد در روابط</p></div>
<div class="m2"><p>میان واجب و ممکن وسایط</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خود او باشد اخس در آفرینش</p></div>
<div class="m2"><p>چنین کردند تحقیق اهل بینش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز انسان پس ملک هم عقل اول</p></div>
<div class="m2"><p>بود اشرف و ز آنها آدم اکمل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز عقل و از ملک انسان کامل</p></div>
<div class="m2"><p>شد اکمل در کمالات و فضایل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ولی عقل و ملک باشند اشرف</p></div>
<div class="m2"><p>که از خلقند اهل اولین صف</p></div></div>