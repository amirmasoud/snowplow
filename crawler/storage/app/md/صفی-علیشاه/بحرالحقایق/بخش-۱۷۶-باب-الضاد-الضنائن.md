---
title: >-
    بخش ۱۷۶ - باب الضاد الضنائن
---
# بخش ۱۷۶ - باب الضاد الضنائن

<div class="b" id="bn1"><div class="m1"><p>ضنائن را اگر خواهی خصایص</p></div>
<div class="m2"><p>ز اهل الله مخصوصند و خالص</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جدا از خلق و هم با حق انیسند</p></div>
<div class="m2"><p>کند ضنت بایشان بس نفسیند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خدا را نیست ضنت بر خلایق</p></div>
<div class="m2"><p>کند بخل او بجای غیر لایق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حقیقت بخل او هم عین جوداست</p></div>
<div class="m2"><p>نداد از گنجت از بهر تو سود است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دهد طفلی چه گوهر بر مویزی</p></div>
<div class="m2"><p>ندارد زانکه بر گوهر تمیزی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پدر ضنت کند بر وی جواهر</p></div>
<div class="m2"><p>که وقت حاجتش باشد ذخایر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو آن بهتر که نشناسی ولی را</p></div>
<div class="m2"><p>نه بینی با رمد شمس جلی را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ضنائن‌الغرض حق را خواصند</p></div>
<div class="m2"><p>که از اعراض خلقیت خلاصند</p></div></div>