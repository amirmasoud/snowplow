---
title: >-
    بخش ۵۰۷ - الولی
---
# بخش ۵۰۷ - الولی

<div class="b" id="bn1"><div class="m1"><p>مهیا بهر تحقیق ولی باش</p></div>
<div class="m2"><p>ز معنای تولی ممتلی باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی باشد که کارش با خدا شد</p></div>
<div class="m2"><p>بهر خیری الهش رهنما شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مهمش را بخود سازد کفایت</p></div>
<div class="m2"><p>بمحض فضل و احسان و عنایت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود خود حافظش از هر خطائی</p></div>
<div class="m2"><p>کز او صادر نگردد ناروائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلغزد ناگه ار پای ثباتش</p></div>
<div class="m2"><p>نگه دارد بعون از سیئاتش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رساند تا بجائی در وصولش</p></div>
<div class="m2"><p>که مردانرا رسانید از قبولش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خود او فرموده گر فهمی بیان را</p></div>
<div class="m2"><p>که دارم دوست بیشک صالحانرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صلاحیت بغیر از موهبت نیست</p></div>
<div class="m2"><p>نگویم موهبت را هم جهت نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جهت راز آنکه فرموده صلاح است</p></div>
<div class="m2"><p>در این پس زاید امید فلاح است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بکف گنج ار چه کس را نارد افتاد</p></div>
<div class="m2"><p>ولی زآنجا طلب کو خود نشانداد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو هر کس هم بدست آورد گنجی</p></div>
<div class="m2"><p>بد از جای نشانی بی‌زرنجی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بسی هم تا بقعر آنرا بریدند</p></div>
<div class="m2"><p>بغیر از رنج و تشویشی ندیدند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مگر کز چشم بندیهای رب بود</p></div>
<div class="m2"><p>که آنرا گنج و اینرا رنج و تب بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نگه دارد یکی را از رذایل</p></div>
<div class="m2"><p>یکی افتد زره با صد فضائل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نداند غیر ذات بی‌روالش</p></div>
<div class="m2"><p>کسی اسرار خلقت یار جالش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خود او داند که علام‌الغیوب است</p></div>
<div class="m2"><p>بحکمت صانع هر زشت و خوبست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بسا باشد که آنرا هم که گنجش</p></div>
<div class="m2"><p>نداد از بعد صد تشویش و رنجش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همان منعش که از وی ماند مهجور</p></div>
<div class="m2"><p>ز حکمتها که در غیب است و مستور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یکی گنجیست مالامال گوهر</p></div>
<div class="m2"><p>دهد وقتی که بهر اوست بهتر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همان کوشش بجز توفیق حق نیست</p></div>
<div class="m2"><p>دلیل است اینکه او باطل ورق نیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>توالی الصالحینت جای گنج است</p></div>
<div class="m2"><p>بکوب اینخانه را تا تاب رنج است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بزغم ما محلا است اینکه محروم</p></div>
<div class="m2"><p>کسی گردد ز جای گنج معلوم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اگر ناید بدست از ضعف حال است</p></div>
<div class="m2"><p>در آن آلات و اسباب اختلالست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>و گر نه وعده حق راست باشد</p></div>
<div class="m2"><p>ز بهر هر که او را خواست باشد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نبیند مر مقام جستجو را</p></div>
<div class="m2"><p>بجز یوسف رخی برهان او را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که از برهان رب بیند اثرها</p></div>
<div class="m2"><p>مگر برهاند او را از خطرها</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ببندندش باغلال و بزنجیر</p></div>
<div class="m2"><p>کز او صادر نگردد ذنب و تقصیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بود اغلال و زنجیرش تولی</p></div>
<div class="m2"><p>ولای عبد یعنی حفظ مولی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بود فرض محبت هم واحد</p></div>
<div class="m2"><p>بقلبی گر شود از غیب وارد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو هم واحد آمد غیر محبوب</p></div>
<div class="m2"><p>نماید در نظر مطرود و معیوب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نیاید هیچ در چشمش بدو نیک</p></div>
<div class="m2"><p>نماید آفتابش قرص تاریک</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بود برهان رب پس حب ساطع</p></div>
<div class="m2"><p>که باشد میلها را جمله قاطع</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هر آن میلی که ترک از خود محب کرد</p></div>
<div class="m2"><p>بخود محبوب بازش منجذب کرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از آن جذبش دگر عشقی فزون گشت</p></div>
<div class="m2"><p>ز عشقش باز جذبی رهنمون گشت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بدینسان تا بکلی منجذب شد</p></div>
<div class="m2"><p>یکی خود حب و محبوب و محب شد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چنین شخصی ولی الله باشد</p></div>
<div class="m2"><p>در آنجا مقصد اینجا راه باشد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خود او هادی و خود مقصود و خود راه</p></div>
<div class="m2"><p>بصورت بنده و در معنی الله</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ولی هم در مراتب دارد اقسام</p></div>
<div class="m2"><p>بود در هر مقام او را یکی نام</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>رجال و غوث و قطب، ابدال و اوتاد</p></div>
<div class="m2"><p>نجیبان و نقیبان و هم افراد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>که ذکر جمله بالطف و دقایق</p></div>
<div class="m2"><p>بود مضبوط در بحرالحقایق</p></div></div>