---
title: >-
    بخش ۱۴۰ - سرالربوبیه
---
# بخش ۱۴۰ - سرالربوبیه

<div class="b" id="bn1"><div class="m1"><p>یک از سر ربوبیت بتحقیق</p></div>
<div class="m2"><p>ز من بنیوش و از دل دار تصدیق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود موقوف تعیینش بمربوب</p></div>
<div class="m2"><p>که لابد برد و نسبت اوست منسوب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی مربوب باشد زان دو نسبت</p></div>
<div class="m2"><p>که در اعیان ثابت داشت غیبت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خود از وی غیر نامی در قلم نیست</p></div>
<div class="m2"><p>بجز اعیان ثابت در عدم نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود موقوف بر معدوم معدوم</p></div>
<div class="m2"><p>خود این بر اهل تحقیق است معلوم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز قول سهل گویند اهل عرفان</p></div>
<div class="m2"><p>ربوبیت ورا سریست پنهان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که گر ظاهر شود آنسر قابل</p></div>
<div class="m2"><p>ربوبیت شود البته باطل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز محیی ‌الدین بود این نکته تعبیر</p></div>
<div class="m2"><p>که ظاهر را بزایل کرده تفسیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شود یعین اگر زایل خود آن سر</p></div>
<div class="m2"><p>ربوبیت شود باطل بظاهر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ببطالان حکم از آنره کرد کامل</p></div>
<div class="m2"><p>که موقوف علیه اوست زایل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دگر گوید صفی بر وجه تحقیق</p></div>
<div class="m2"><p>چو حقش در معانی داده توفیق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بود سر شرط آن مستور بودن</p></div>
<div class="m2"><p>ز اظهار و نمایش دور بودن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر ظاهر شود از اصل سر نیست</p></div>
<div class="m2"><p>بود سر مستتر، وان مستتر نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پس او از معنی خود گشته موضوع</p></div>
<div class="m2"><p>بر او اطلاق سر نهی است و ممنوع</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو اینعالم بترتیب است موجود</p></div>
<div class="m2"><p>وجود جمله هم در علم مشهود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بود سر ربوبیت همان علم</p></div>
<div class="m2"><p>نخواهد گشت ظاهر در عیان علم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر ظاهر شود فرض محال است</p></div>
<div class="m2"><p>بحال خویش ثابت در کمال است</p></div></div>