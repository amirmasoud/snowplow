---
title: >-
    بخش ۸۹ - الخطره
---
# بخش ۸۹ - الخطره

<div class="b" id="bn1"><div class="m1"><p>بود آن خطره کایدپیش سالک</p></div>
<div class="m2"><p>بدفع آن نباشد عبد مالک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی داعیست خواند بر الهش</p></div>
<div class="m2"><p>که تا دارد ز لغرش نگاهش</p></div></div>