---
title: >-
    بخش ۲۱ - الامامان
---
# بخش ۲۱ - الامامان

<div class="b" id="bn1"><div class="m1"><p>امامان قطب را همچون دو دستند</p></div>
<div class="m2"><p>که در جنبین او پیوسته هستند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی کان بریمین است او بملکوت</p></div>
<div class="m2"><p>بود ناظر دگر پر ملک ناسوت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خود این کوناظر ملک است اعلاست</p></div>
<div class="m2"><p>از آن دیگر که بر ملکوت بیناست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلیفه قطب این باشد به تعیین</p></div>
<div class="m2"><p>بنزد اهل ذوق از روی تمکین</p></div></div>