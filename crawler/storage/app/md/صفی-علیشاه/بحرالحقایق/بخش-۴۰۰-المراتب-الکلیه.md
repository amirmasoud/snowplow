---
title: >-
    بخش ۴۰۰ - المراتب الکلیه
---
# بخش ۴۰۰ - المراتب الکلیه

<div class="b" id="bn1"><div class="m1"><p>مراتب نزد صوفی غیر شش نیست</p></div>
<div class="m2"><p>جز این ترتیب بر دل منتقش نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود این شش هست کلی از مراتب</p></div>
<div class="m2"><p>بود بر وصف کلیت مناسب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی ذات الاحد پس واحدیت</p></div>
<div class="m2"><p>که ثانی از مراتب شد بر تبت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیم رتبه است ارواح مجرد</p></div>
<div class="m2"><p>نفوس عامله پس رابع آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که موسوم از عوالم بر مثال است</p></div>
<div class="m2"><p>دگر ملکوتش ار خوانی مجال است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به پنجم عالم ملک و شهادت</p></div>
<div class="m2"><p>که ناسوت است بی‌نقص و زیادت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ششم گشت از مراتب کون جامع</p></div>
<div class="m2"><p>که انسان است و حق در عین واقع</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بمعنی مجلی کل مجالی</p></div>
<div class="m2"><p>بصورت جامع آنذات عالی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مجالی پنج و شش باشد مراتب</p></div>
<div class="m2"><p>یکی از سته باشد ذات واجب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بذات او جلوه‌گر شد در مجالی</p></div>
<div class="m2"><p>بآثار جمالی و جلالی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دگر بعضی بر این گشتند قائل</p></div>
<div class="m2"><p>که هشت است این مراتب نزد کامل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نخستین عالم ملک است از آن</p></div>
<div class="m2"><p>دگر ملکوت و پس جبروت و اعیان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پس اسمای الهیه به برتر</p></div>
<div class="m2"><p>صافت پاک سبحانیه دیگر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کز آن تعبیر شد بر واحدیت</p></div>
<div class="m2"><p>دگر هم بر احد از حیث رتبت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بهفتم وحدت ذات الهی</p></div>
<div class="m2"><p>بهشتم ذات حق بی‌تناهی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صفی خود هفت داند این مراتب</p></div>
<div class="m2"><p>شهادت را بود اول مناسب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که آنملک است و در ثانی مثال است</p></div>
<div class="m2"><p>سیم ملکوت کارواح از کمال است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دگر جبروت و اعیانست و اسما</p></div>
<div class="m2"><p>بهفتم ذات حق بحت یکتا</p></div></div>