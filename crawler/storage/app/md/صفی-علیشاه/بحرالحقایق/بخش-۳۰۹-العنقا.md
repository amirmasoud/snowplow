---
title: >-
    بخش ۳۰۹ - العنقاء
---
# بخش ۳۰۹ - العنقاء

<div class="b" id="bn1"><div class="m1"><p>بود عنقا کنایه از هیولا</p></div>
<div class="m2"><p>که ز انظار است مخفی همچو عنقا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو او را جز که با صورت نیابی</p></div>
<div class="m2"><p>نشان از وی بکونیت نیابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بزیبایی صور را قابل آمد</p></div>
<div class="m2"><p>وجود او بصورت حاصل آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود او را تحرک بین اجسام</p></div>
<div class="m2"><p>که باشد عنصر اعظم در افهام</p></div></div>