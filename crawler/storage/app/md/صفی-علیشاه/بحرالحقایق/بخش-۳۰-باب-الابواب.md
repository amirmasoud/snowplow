---
title: >-
    بخش ۳۰ - باب الابواب
---
# بخش ۳۰ - باب الابواب

<div class="b" id="bn1"><div class="m1"><p>نکو دریاب شرح باب الابواب</p></div>
<div class="m2"><p>که آن توبه است نزد اهل آداب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شود در خانه داخل هر کس از در</p></div>
<div class="m2"><p>ز توبه یافت سالک راه و رهبر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز سر توبه هر کس گشت آگاه</p></div>
<div class="m2"><p>نگردد یکنفس غافل ز درگاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بظاهر تو به از جرم و گناه است</p></div>
<div class="m2"><p>بباطن ز آنچه آن غیر از اله است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود در شرع حق توبت ز دلخواه</p></div>
<div class="m2"><p>بمعنی از تمام ما سوی الله</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود آن توبه در حکم شریعت</p></div>
<div class="m2"><p>بود این توبه اهل حقیقت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آن توبه ز فعل بد بپرهیز</p></div>
<div class="m2"><p>در این توبه ز بود خویش برخیز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهر آنی که راهی را کنی طی</p></div>
<div class="m2"><p>از آن رفتار میکن تو به در پی</p></div></div>