---
title: >-
    بخش ۲۳۲ - عبدالمذل
---
# بخش ۲۳۲ - عبدالمذل

<div class="b" id="bn1"><div class="m1"><p>بود عبدالمذل در علم و احوال</p></div>
<div class="m2"><p>ز یزدان مظهر او بر وصف اذلال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر آنکس را بظاهر یا بباطن</p></div>
<div class="m2"><p>ز حق شد حتم ذلت در مواطن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود او واسطه ضعف و ذلالش</p></div>
<div class="m2"><p>شوند اعدای حق خوار از خیالش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عدو اولیا اعدای حقند</p></div>
<div class="m2"><p>که بر ذلت هم از حق مستحقند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وگر نه ذات مطلق را عدو نیست</p></div>
<div class="m2"><p>بعالم دشمنی از بهر او نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی با خود نگردد هیچ دشمن</p></div>
<div class="m2"><p>زند لیک از طمع شمشیر بر تن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو شد تن چاک مرگش در طلب بود</p></div>
<div class="m2"><p>بجان خویش دشمن زین سبب بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صفی را باز هم تحقیق خاصی است</p></div>
<div class="m2"><p>که آنرا نزد صوفی اختصاصی است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو حق در هستی اشیاء اصیل است</p></div>
<div class="m2"><p>عجب دارم اگر شیئی ذلیل است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بود ذل گر که باشد سیر موضوع</p></div>
<div class="m2"><p>همانا وضع شیی در غیر موضوع</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بود در عزتش بر تاج شاهی</p></div>
<div class="m2"><p>باو هم حاجت است آنجا کماهی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بکهدان ارفتد میدان تو خوارش</p></div>
<div class="m2"><p>ندارد ز آنکه آنجا کس بکارش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سلاح اندر کف مردان عزیز است</p></div>
<div class="m2"><p>ذلیل است ار بدست دزد و هیز است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بنزد مرد مشرک علم توحید</p></div>
<div class="m2"><p>ذلیل است این تو را کافیست دردید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بدینسان هر وجودی در نظامش</p></div>
<div class="m2"><p>ذلیل است ار نباشد در مقامش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خود اینهم در نظام تام عرض است</p></div>
<div class="m2"><p>که ذلت بهر شیئی هست و فرض است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بود عبدالمذل آنکس که از وی</p></div>
<div class="m2"><p>رسد ذلت بجای خویش بر شیی</p></div></div>