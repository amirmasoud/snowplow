---
title: >-
    بخش ۱۶۲ - حکایت ابوحمزه
---
# بخش ۱۶۲ - حکایت ابوحمزه

<div class="b" id="bn1"><div class="m1"><p>ابوحمزه خراسانی که مردیست</p></div>
<div class="m2"><p>ز سلاک طریقت رهنوردیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>براه کعبه اندر چاهی افتاد</p></div>
<div class="m2"><p>نه چاهست آنکه در وی ماهی افتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بغافل قصر و ایوان جمله چاهست</p></div>
<div class="m2"><p>بکامل چاه و زندان بارگاهست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگفتا نفس او فریاد سر کن</p></div>
<div class="m2"><p>ز حال خود خلایق را خبر کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که آرندت مگر زین چاه بیرون</p></div>
<div class="m2"><p>رسی بر ساحلی زین بحر پر خون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگفتا حاضر آنشاه مجید است</p></div>
<div class="m2"><p>که بر ما اقرب ازحبل وریدست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>توکل جز بحق زیبنده نبود</p></div>
<div class="m2"><p>که جز او کار ساز بنده نبود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رسیدند امتحانرا ناگه از راه</p></div>
<div class="m2"><p>دو تن از رهسپاران بر سر چاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در آمد نفس بوحمزه بفریاد</p></div>
<div class="m2"><p>که باید خواست اینک زین دو امداد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وگرنه مرد باید با فلاکت</p></div>
<div class="m2"><p>تو را باشد ز دست خود هلاکت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دگر گفت او توکل باشد انسب</p></div>
<div class="m2"><p>که هست از بنده او بر بنده اقرب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تصور کن هست این وقت آخر</p></div>
<div class="m2"><p>در آندم از که جوئی چاره دیگر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پس از عمری مرا عار آید از این</p></div>
<div class="m2"><p>که خواهم چاره از مخلوق مسکین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بحق باشد چهل سالم توکل</p></div>
<div class="m2"><p>کنون جویم بناداری توسل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مرا صدبار بهتر مرگ ازین مزد</p></div>
<div class="m2"><p>که باشم شاه و جویم یاری از دزد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نشست و داد دل بر مرگ وتن زد</p></div>
<div class="m2"><p>فسون گفت آنچه نفسش بر دهن زد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از این بگذشت یکساعت بناگاه</p></div>
<div class="m2"><p>صدائی دیدکاید از سر چاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سر چه را پلنگی بود و بگشاد</p></div>
<div class="m2"><p>معلق گشت و زودش کرد آزاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بگفتش هاتفی بهر تو اینسان</p></div>
<div class="m2"><p>توکل کرد آتش را گلستان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سبوعی را که عنوان غضب بود</p></div>
<div class="m2"><p>تو را شد فوز و رحمت وین عجب بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کسی کو را توکل در نهاد است</p></div>
<div class="m2"><p>هلاک اقرب اسباب مراد است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مرا نزدیک خواندی چون چنین نیک</p></div>
<div class="m2"><p>ترا دادم نجات از مرگ نزدیک</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فقیرانرا چنین بود است اوصاف</p></div>
<div class="m2"><p>تو بر نفس خود از مردی ده انصاف</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگر باشی چنین، صاحب لوائی</p></div>
<div class="m2"><p>امام و پیر و شیخ و رهنمائی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>وگر نه بگذر از خوان مناعت</p></div>
<div class="m2"><p>بنان بینوائی کن قناعت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو در میدان موشی نیست صبرت</p></div>
<div class="m2"><p>هوس چبود بنزد شیر و ببرت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بآن وصف و کمال و خلق و سیرت</p></div>
<div class="m2"><p>نبود آنقوم را دعوی و دعوت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مقال و حالشان فقر و فنا بود</p></div>
<div class="m2"><p>من و ما در تصوف کی بنا بود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>صد از ظرف خالی هست در خور</p></div>
<div class="m2"><p>نه از بحری که از گوهر بود پر</p></div></div>