---
title: >-
    بخش ۳۹۰ - محوارباب الظاهر
---
# بخش ۳۹۰ - محوارباب الظاهر

<div class="b" id="bn1"><div class="m1"><p>اگر داری تو سیری در ظواهر</p></div>
<div class="m2"><p>شنو محوی زمن زار باب ظاهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود آن رفع هر اوصاف و عادات</p></div>
<div class="m2"><p>پس اثبات اقامت در عبادات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمودن سعی در ترک رذایل</p></div>
<div class="m2"><p>بکسب علم و اخلاق و فضایل</p></div></div>