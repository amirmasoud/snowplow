---
title: >-
    بخش ۲۹۱ - عبدذی اجلال و الاکرام
---
# بخش ۲۹۱ - عبدذی اجلال و الاکرام

<div class="b" id="bn1"><div class="m1"><p>بیان ما ز عبد ذوالجلال است</p></div>
<div class="m2"><p>که اعلی و اجل در هر کمال است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اجل و اکرامش حق کرد و جا داشت</p></div>
<div class="m2"><p>که بر جیا آنچه جز حق بود بگذاشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باوصاف الهی متصف شد</p></div>
<div class="m2"><p>بدل شمس جلالش منکشف شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان کاسمای او باشد معلا</p></div>
<div class="m2"><p>ز توصیف و تنزه نزد دانا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم اینسانند ارباب سرائر</p></div>
<div class="m2"><p>که اسماء را شدند آنها مظاهر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه بیند دشمنی او را برؤیت</p></div>
<div class="m2"><p>مگر کز وی بدل گیرد مهابت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرامی دارد او هم متقین را</p></div>
<div class="m2"><p>نماید خوار و پست اعدادی دین را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز هر وصفی که پنداری اجل است</p></div>
<div class="m2"><p>دو عالم نزد اجلالش اقل است</p></div></div>