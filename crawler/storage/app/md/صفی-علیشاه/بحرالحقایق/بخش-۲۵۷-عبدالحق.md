---
title: >-
    بخش ۲۵۷ - عبدالحق
---
# بخش ۲۵۷ - عبدالحق

<div class="b" id="bn1"><div class="m1"><p>بود عبدالحق آن کش حق ز باطل</p></div>
<div class="m2"><p>نگهدارد هم از رجس و رذائل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز اوصافی که نبود مرضی حق</p></div>
<div class="m2"><p>نماید حفظ او را حق مطلق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ببیند حق در اشیاء جمله ثابت</p></div>
<div class="m2"><p>ذوات کل اشیاء را مذوت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وجودیرا که او قائم بذات است</p></div>
<div class="m2"><p>به بیند کو مذوت بر ذوات است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوایش باطل است و غیر ثابت</p></div>
<div class="m2"><p>کجا زایل تواند شد مذوت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز صورتهای حقی بیند او حق</p></div>
<div class="m2"><p>ز باطل باطل این باشد محقق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بود باطل تو گو صرف تلاشی‌</p></div>
<div class="m2"><p>ازو حقی نگردد هیچ ناشی‌</p></div></div>