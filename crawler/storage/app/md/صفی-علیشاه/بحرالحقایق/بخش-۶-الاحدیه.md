---
title: >-
    بخش ۶ - الاحدیه
---
# بخش ۶ - الاحدیه

<div class="b" id="bn1"><div class="m1"><p>احدیت بود وصفی که ساقط</p></div>
<div class="m2"><p>در او باشد صفتها با روابط</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود وصفی که او خود عین ذاتست</p></div>
<div class="m2"><p>برون از جمع اسماء و صفات است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه اسم و رسم را آنجا مقام است</p></div>
<div class="m2"><p>نسبها و تعینها تمام است</p></div></div>