---
title: >-
    بخش ۴۴۳ - النفس
---
# بخش ۴۴۳ - النفس

<div class="b" id="bn1"><div class="m1"><p>نفس گویند ترویح قلوبست</p></div>
<div class="m2"><p>بامداد لطایف کز غیوب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نفسها مر محبی راست مطلوب</p></div>
<div class="m2"><p>که باشد در تنفس انس محبوب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر این مخصوص باشند اهل انفاس</p></div>
<div class="m2"><p>دم آدم بود غیر از دم ناس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه هر جنبنده‌ئی صاحب نفس شد</p></div>
<div class="m2"><p>کسی قدر نفس داند که کس شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی قدر نفس داند که شبها</p></div>
<div class="m2"><p>کشیده در فراقی رنج و تبها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی قدر نفس داند که حالی</p></div>
<div class="m2"><p>رسیده بعد هجران بر وصالی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کسی کو صرف وقتی کرد هم را</p></div>
<div class="m2"><p>شناسد قدر وقت و قدر دم را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی را می‌توان صاحب نفس گفت</p></div>
<div class="m2"><p>که در عشق بتی ترک هوس گفت</p></div></div>