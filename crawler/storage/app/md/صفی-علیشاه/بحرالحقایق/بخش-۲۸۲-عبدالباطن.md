---
title: >-
    بخش ۲۸۲ - عبدالباطن
---
# بخش ۲۸۲ - عبدالباطن

<div class="b" id="bn1"><div class="m1"><p>ز عبدالباطن ار جوئی نشانی</p></div>
<div class="m2"><p>بود دایم بتکمیل معانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رساند حق بمعلومات قلبش</p></div>
<div class="m2"><p>نماید منکشف آیات قلبش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بحق گردد ز موجودات خلص</p></div>
<div class="m2"><p>کند دل را بعشق دوست مختص</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بکوشد بر صفای قلب و باطن</p></div>
<div class="m2"><p>شود کشف مغیباتش معاین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو روحانیت از حق یافت در سر</p></div>
<div class="m2"><p>از این رو گشت مشرف بر سر ائر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخواند خلق را بر معنویت</p></div>
<div class="m2"><p>که بر معنی بود او را رؤیت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دگر تقدیس قلب و حسن میثاق</p></div>
<div class="m2"><p>دگر تطهیر سر و کسب اخلاق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بتنزیه است او را حکم توحید</p></div>
<div class="m2"><p>بد انسانی که عیسی راست در دید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از اینره چونکه از حق شده مخاطب</p></div>
<div class="m2"><p>تو گفتی ما الهین ایم و غالب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تبرا جست از گفتار تشبیه</p></div>
<div class="m2"><p>گرفت اندر تمسک حبل تنزیه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که پاک است از تشبه ذات سبحان</p></div>
<div class="m2"><p>تو دانی کاین بمن کذبست و بهتان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو دانی آنچه در من هست یا نیست</p></div>
<div class="m2"><p>ندانم لیک من نفس تو بر چیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خود این معنی اگر دانی موارد</p></div>
<div class="m2"><p>بقدس نفس و تنزیه است شاهد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از اینره خلطه‌اش با خلق کم بود</p></div>
<div class="m2"><p>جهان در چشم توحیدش عدم بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هم آدم شد باین تنزیه موسوم</p></div>
<div class="m2"><p>«ز انبئهم باسما» اینست معلوم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نبود آدم سلوکش گر بباطن</p></div>
<div class="m2"><p>کجا دانست اسماء را مواطن</p></div></div>