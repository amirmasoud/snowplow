---
title: >-
    بخش ۴۸۶ - واسطه الفیض و المدد
---
# بخش ۴۸۶ - واسطه الفیض و المدد

<div class="b" id="bn1"><div class="m1"><p>شنو از واسطه فیض و مدد باز</p></div>
<div class="m2"><p>بود انسان کامل با سند باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود او واسطه بر خلق از حق</p></div>
<div class="m2"><p>که نسبت بر دو سو دارد محقق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر بط معنوی ذوجنبتین است</p></div>
<div class="m2"><p>میانجی بین وجهین او بعین است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک از وجهی که مطلق از دو کونست</p></div>
<div class="m2"><p>قریب‌الربط با سلطان عون است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دگر وجهش که با خلق اشتراکست</p></div>
<div class="m2"><p>مقید در لباس آب و خاک است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رساند فیض عالی را بسافل</p></div>
<div class="m2"><p>کشاند فلک دانی را بساحل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو گو خطی است بین‌النقطتین او</p></div>
<div class="m2"><p>ز هر یک دارد آثاری بعین او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک آثارش وجود علم و نور است</p></div>
<div class="m2"><p>یک آثارش همه فقد و فتور است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز آثار وجودی شاه مطلق</p></div>
<div class="m2"><p>بگفتا «من رآنی قدرأی الحق»</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز حیث ممکنیت گفت بر دل</p></div>
<div class="m2"><p>غباری گرد دم پیوسته حایل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دهم دل را باستغفار رجعت</p></div>
<div class="m2"><p>ز مرآت آن زداید زنگ غفلت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باین معنی «عصی آدم» صریح است</p></div>
<div class="m2"><p>که عصیان بهر ممکن بس صحیحست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خود امکانیت آدم راست عصیان</p></div>
<div class="m2"><p>وجوب آمد نشانش عفو و غفران</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نبی معصوم از وجه وجوب است</p></div>
<div class="m2"><p>ز ممکن دزد غفلت خانه روب است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بود هر غفلتی بر قدر ادراک</p></div>
<div class="m2"><p>حضور آنهم بقدر عقل چالاک</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صفی یا رب ز عصیان رو سیاهست</p></div>
<div class="m2"><p>ز آدم یادگار اند گناه است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز ضعف او راست لغزشها پیاپی</p></div>
<div class="m2"><p>ببخش او را که بالاصل است لاشیئی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باصل او محض فقدان و عدم بود</p></div>
<div class="m2"><p>وجود ارشد ز فصل ذوالکرم بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کرم گر باز فرمائی عجب نیست</p></div>
<div class="m2"><p>بمعدومی که در فعلش ادب نیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ابا این جمله فقدان و قصورم</p></div>
<div class="m2"><p>مدار از رحمت ذاتیه دو رم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مرا ذاتیست ضعف و ظلم و ذلت</p></div>
<div class="m2"><p>تو را ذاتیست عفو وجود و رحمت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بخود من ظلم کردم تو کرم کن</p></div>
<div class="m2"><p>نظر آنسان که کردی باز هم کن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همه عمرم بغفلت رفت و بر سهو</p></div>
<div class="m2"><p>تو بخشاینده‌ئی العفو العفو</p></div></div>