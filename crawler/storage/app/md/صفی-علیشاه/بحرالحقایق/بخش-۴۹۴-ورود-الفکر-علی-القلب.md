---
title: >-
    بخش ۴۹۴ - ورود الفکر علی‌القلب
---
# بخش ۴۹۴ - ورود الفکر علی‌القلب

<div class="b" id="bn1"><div class="m1"><p>بود فکر تو از وجهی عبارت</p></div>
<div class="m2"><p>کزان بر نفس قدسی شد اشارت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو گو باد بهشت است آن که آسان</p></div>
<div class="m2"><p>بقلب آید بوجهی همچو انسان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم اینسان شد مجسم بهر مریم</p></div>
<div class="m2"><p>که او را آدمی پنداشت فافهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر این فکر آیدت میدان غنیمت</p></div>
<div class="m2"><p>مگر بردی ز میدان گوی دولت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو این باشد خود آثار قبولش</p></div>
<div class="m2"><p>تو را نبود وصولی بیحصولش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رسد وقتی گرت مهمانی از غیب</p></div>
<div class="m2"><p>گرامی دارو از ریبش مجوعیب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حبیب حق بود اینگونه مهمان</p></div>
<div class="m2"><p>که همراه آورد نعمت فراوان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوشا وقتش که با وقت خوش یافت</p></div>
<div class="m2"><p>دو صد گفتار از آن لعل خمش یافت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شبان تیره با او بی‌لب و کام</p></div>
<div class="m2"><p>سخن گفت و شنید و یافت آرام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرش جستی و گفت او با تو رازی</p></div>
<div class="m2"><p>بپوش آنرا که باشد امتیازی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرفتاران صورت رامنه عیب</p></div>
<div class="m2"><p>مدارش گر چه بر ظن است و بر ریب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مزن بر قشریان خام تسخر</p></div>
<div class="m2"><p>که داری تو خزف ما راست گوهر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خزف هم اندرین مخزن بکار است</p></div>
<div class="m2"><p>بجای خود بسی کامل عیار است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بخلقان بین بلطف و رفق و حرمت</p></div>
<div class="m2"><p>نه با خود بینی و عجب و منیت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ببین هر چیزی اندر جای خود نیک</p></div>
<div class="m2"><p>بهر دوری تو خود را دار نزدیک</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یقینت چون شود کامل بتوحید</p></div>
<div class="m2"><p>مزن طعنه باهل ظن و تقلید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که هر کس را بود نوعی عبادت</p></div>
<div class="m2"><p>یکی از عشق و آن دیگر ز عادت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو تکوینی و توظیفی است طاعات</p></div>
<div class="m2"><p>نماید هر دو را صوفی مراعات</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بجای خویش هر یک را بجا آر</p></div>
<div class="m2"><p>که هر یک را بجا دیدند اخیار</p></div></div>