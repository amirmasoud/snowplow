---
title: >-
    بخش ۵۰ - التحقیق
---
# بخش ۵۰ - التحقیق

<div class="b" id="bn1"><div class="m1"><p>ز حق باشد گرت در علم توفیق</p></div>
<div class="m2"><p>یکی بشنو ز من معنای تحقیق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شهود حق بصورتهای اسماست</p></div>
<div class="m2"><p>که اکونش اگر خوانی بود راست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بخلق از حق محقق نیست محجوب</p></div>
<div class="m2"><p>بحق از خلق هم بروجه مرغوب</p></div></div>