---
title: >-
    بخش ۲۱۵ - عبدالعزیز
---
# بخش ۲۱۵ - عبدالعزیز

<div class="b" id="bn1"><div class="m1"><p>دگر عبدالعزیز آن در تمیز است</p></div>
<div class="m2"><p>که از حق مظهر اسم عزیز است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تجلی کرده حق بروی بعزت</p></div>
<div class="m2"><p>بهر شیئی است غالب در حقیقت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگردد هیچ مغلوب و هراسان</p></div>
<div class="m2"><p>ز دست حادثات و جور اکوان</p></div></div>