---
title: >-
    بخش ۱۶ - الاصطلام
---
# بخش ۱۶ - الاصطلام

<div class="b" id="bn1"><div class="m1"><p>یکی از اصطلاحات اصطلام است</p></div>
<div class="m2"><p>که آنرا گر وله گوئی بنام است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وله بر هیمنه معنا قریب است</p></div>
<div class="m2"><p>کزان پس هیمنه بر دل نصیب است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیمان گر بدل گردید غالب</p></div>
<div class="m2"><p>شود درویش از کونین غایب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نماید هر دو عالم را فراموش</p></div>
<div class="m2"><p>شود از قیل و قال خلق خاموش</p></div></div>