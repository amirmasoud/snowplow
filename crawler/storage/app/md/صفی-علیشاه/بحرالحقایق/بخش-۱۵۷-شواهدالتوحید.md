---
title: >-
    بخش ۱۵۷ - شواهدالتوحید
---
# بخش ۱۵۷ - شواهدالتوحید

<div class="b" id="bn1"><div class="m1"><p>بود دیگر شواهدهای توحید</p></div>
<div class="m2"><p>تعینهای اشیاء خود بتاکید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه اشیاء هر یکی یکتا بذاتند</p></div>
<div class="m2"><p>ز هم ممتاز و مخصوص از جهاتند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز حق باشند هر یک آیتی خاص</p></div>
<div class="m2"><p>بیک هستی تشخص یافت اشخاص</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ازین حیثیت اشیاء را عدد نیست</p></div>
<div class="m2"><p>عیان از این عددها جز احد نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان یکتاست در خود هر نمودی</p></div>
<div class="m2"><p>که پنداری جز او نبود وجودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تعینهای اشیاء پس بتاکید</p></div>
<div class="m2"><p>بود هر یک شواهدهای توحید</p></div></div>