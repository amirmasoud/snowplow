---
title: >-
    بخش ۱۰۳ - الرب
---
# بخش ۱۰۳ - الرب

<div class="b" id="bn1"><div class="m1"><p>بود رب اسمی از اسماء حضرت</p></div>
<div class="m2"><p>که از ذاتش باعیانست نسبت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باسماء ربوبیت مصادق</p></div>
<div class="m2"><p>مثال منعم و رزاق و خالق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دراکون هر چه او گردید ظاهر</p></div>
<div class="m2"><p>بود آن اسم ربانی و باهر</p></div></div>