---
title: >-
    بخش ۹۹ - ذوالعین
---
# بخش ۹۹ - ذوالعین

<div class="b" id="bn1"><div class="m1"><p>ذوی‌العین آنکه حق بیند بظاهر</p></div>
<div class="m2"><p>بباطن وانگهی خلق و مظاهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در اینجا خلق مرآت حق آمد</p></div>
<div class="m2"><p>نهان اندر مقید مطلق آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ظهور حق و اخفای خلایق</p></div>
<div class="m2"><p>بصورت همچون مرآت موافق</p></div></div>