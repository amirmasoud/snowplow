---
title: >-
    بخش ۴۶۲ - الحلم
---
# بخش ۴۶۲ - الحلم

<div class="b" id="bn1"><div class="m1"><p>سکون و حلم جز ضد غضب نیست</p></div>
<div class="m2"><p>غضبها از حرام است و عجب نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کند قوت حلال از غیب یاری</p></div>
<div class="m2"><p>تو را در علم و عقل و بردباری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو آنرارکن اخلاق و عمل دان</p></div>
<div class="m2"><p>محاسن از حلالت ما حصل دان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حرامت مایه ظلم و شرور است</p></div>
<div class="m2"><p>حلالت منشأ خیر الامور است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشان خیر باشد عدل و انصاف</p></div>
<div class="m2"><p>توان از عدل کردن جمع اطراف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شجاعت دست عدل اندر فنونست</p></div>
<div class="m2"><p>از آن بینی که هر ظالم جبونست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو عدل آمد رود ظلم و خیانت</p></div>
<div class="m2"><p>فزاید مر تو را وزن و متانت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نترسی گر جهان جنبد بجنگت</p></div>
<div class="m2"><p>نغلطد از هزاران سیل سنگت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز طوفانها نلرزد شاخ بیدت</p></div>
<div class="m2"><p>که نبود جز بعون حق امیدت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به ننشیند ز کس گردت بدامان</p></div>
<div class="m2"><p>نباشد هم نبردت کس بمیدان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بود پس حلم معیار شجاعت</p></div>
<div class="m2"><p>وقار از حلم و صبر است از قناعت</p></div></div>