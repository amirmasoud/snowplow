---
title: >-
    بخش ۲۸۳ - عبدالوالی
---
# بخش ۲۸۳ - عبدالوالی

<div class="b" id="bn1"><div class="m1"><p>تو عبدالوالی آنرا دان که مطلق</p></div>
<div class="m2"><p>بود والی بکل ما سوی الحق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخلق او را ولایت در ظهور است</p></div>
<div class="m2"><p>ولی ناس در کل امور است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنفس خویش مستولی چنان شد</p></div>
<div class="m2"><p>که والی بر نفوس دیگران شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود زان هفت تن کاهل نیازند</p></div>
<div class="m2"><p>بظل عرش با صد اعتزازند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو گو هم باشد اول زان هیاکل</p></div>
<div class="m2"><p>ظهورش در جهان سلطان عادل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بعالم شاه و ظل الله باشد</p></div>
<div class="m2"><p>معین خلق بر دلخواه باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز خوبیها بنزد با تمیزان</p></div>
<div class="m2"><p>ورا سنگین‌تر از خلقست میزان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو خیرات خلایق و آنچه نیکوست</p></div>
<div class="m2"><p>سراسر وضع او را در ترازوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز حق در ارض و افلاکست مشهور</p></div>
<div class="m2"><p>بحق محفوظ و از حق است منصور</p></div></div>