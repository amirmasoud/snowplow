---
title: >-
    بخش ۳۴۶ - القرب
---
# بخش ۳۴۶ - القرب

<div class="b" id="bn1"><div class="m1"><p>دگر قرب از فنا باشد عبارت</p></div>
<div class="m2"><p>بعهد ماسبق دارد اشارت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همان عهدی که بین عبد و حق بود</p></div>
<div class="m2"><p>بر آن چیزی که اندر ماسبق بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>«الست ربکم» حق در ازل گفت</p></div>
<div class="m2"><p>هم او «قالوابلی» خود در محل گفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>«الست ربکم» حکم وجود است</p></div>
<div class="m2"><p>دگر «قالوابلی» ارسال جود است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نبد غیری خود آن گفت و خود این گفت</p></div>
<div class="m2"><p>خود او بود آنکه در عهد خود سفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بجائی دون غیر از قول حق گفت</p></div>
<div class="m2"><p>بجائی از زبان ما خلق گفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نبود آن ما خلق غیر از نمودش</p></div>
<div class="m2"><p>که گشت از غیب ظاهر در شهودش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خود او بشنود گر گفت او الستی</p></div>
<div class="m2"><p>تو پنداری بلی گفتی که مستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>الست آن سیر صورت در مرایاست</p></div>
<div class="m2"><p>بلی یعنی رخ از آئینه پیداست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>الست آن آفتاب پر ز نور است</p></div>
<div class="m2"><p>بلی یعنی که در ضوئش ظهور است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>الست آن جلوه حسن و جمال است</p></div>
<div class="m2"><p>بلی آن دیدن خود بر کمال است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>الست آن جنبش بحر الخطابست</p></div>
<div class="m2"><p>بلی یعنی که موج و قطر آب است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>الست آمد ظهور فاعلیت</p></div>
<div class="m2"><p>بلی باشد نمود قابلیت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>الست اظهار حسن او بخود بود</p></div>
<div class="m2"><p>بلی تأثیر عشق معتمد بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>الست اعنی که حسنم بی‌نظیر است</p></div>
<div class="m2"><p>بلی یعنی ز کوتم بر فقیر است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>الست اعنی که ثابت در وجودم</p></div>
<div class="m2"><p>بلی یعنی که ساری در حدودم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>الست اعنی که مستغنی بذاتم</p></div>
<div class="m2"><p>بلی یعنی که ظاهر در صفاتم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>الست اعنی که شاه و ذوالجلالم</p></div>
<div class="m2"><p>بلی یعنی که مشهود از جمالم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بود قربت فنا باری ز کونین</p></div>
<div class="m2"><p>هم آن باشد مقام قاب قوسین</p></div></div>