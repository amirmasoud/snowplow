---
title: >-
    بخش ۱۲ - ارائک التوحید
---
# بخش ۱۲ - ارائک التوحید

<div class="b" id="bn1"><div class="m1"><p>شنو زارائک التوحید باری</p></div>
<div class="m2"><p>که آن اسمای ذاتیه است آری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو کون او مظاهر بهر ذاتست</p></div>
<div class="m2"><p>در اول کان تو گو جمع صفاتست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خود آنرا گفته صوفی واحدیت</p></div>
<div class="m2"><p>بر اوصاف است او را جامعیت</p></div></div>