---
title: >-
    بخش ۲۵۴ - عبدالمجید
---
# بخش ۲۵۴ - عبدالمجید

<div class="b" id="bn1"><div class="m1"><p>دگر از اولیا عبدالمجید است</p></div>
<div class="m2"><p>که مجدش بین خلق از حق پدیداست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخلق و وصف حق دارد تخلق</p></div>
<div class="m2"><p>محاسن یافت اندر وی تحقق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کند تمجید او هر کس در اوصاف</p></div>
<div class="m2"><p>دهد بر فضل و حسن خلقش انصاف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه آثار مجد از وی عیانست</p></div>
<div class="m2"><p>بزرگ و دلنواز و مهربانست</p></div></div>