---
title: >-
    بخش ۱۶۱ - علامات الشیخ الکامل
---
# بخش ۱۶۱ - علامات الشیخ الکامل

<div class="b" id="bn1"><div class="m1"><p>بود بهر چنین شیخی علامات</p></div>
<div class="m2"><p>که بشناسند او را در مقامات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نخست آگه ز علم شرع باشد</p></div>
<div class="m2"><p>دگر عامل به اصل و فرع باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اسیر حلق و بند دلق نبود</p></div>
<div class="m2"><p>کلامش جز بخیر خلق نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بی برگیش غم در سینه ناید</p></div>
<div class="m2"><p>ز طعن خلق اندر کینه ناید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نرنجد از جفا و جور مردم</p></div>
<div class="m2"><p>نگردد تنگدل از طور مردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز میل خود بکس دشمن نگردد</p></div>
<div class="m2"><p>به تقدیرات حق ضامن نگردد:</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که حسن دین و دنیای تو با من</p></div>
<div class="m2"><p>منم بر دفع مکروه تو ضامن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خود این از جهل و بعلمی بکار است</p></div>
<div class="m2"><p>فضولی در قضای کردگار است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فضولی اندر سرائی ره ندارد</p></div>
<div class="m2"><p>چنین شیخی دل آگه ندارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز اشیاء جهان چیزی بخیره</p></div>
<div class="m2"><p>نسازد بهر خود هرگز ذخیره</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بهر چیزی که باشد بیش یا کم</p></div>
<div class="m2"><p>بخود سلاک را دارد مقدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بود از مدح و ذم خلق آزاد</p></div>
<div class="m2"><p>نه غمگین از رهی نه از علتی شاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بداند واردات عالم از حق</p></div>
<div class="m2"><p>بشیئی نیست غافل یکدم از حق</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیاید بر ستوه از نا روائی</p></div>
<div class="m2"><p>نیارد چین بر ابرویش بلائی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نیابد راه بر نفسش تزلزل</p></div>
<div class="m2"><p>فزاید در شداید بر توکل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مقام آمد ترا گویم بیانی</p></div>
<div class="m2"><p>که باشد در مقاماتت نشانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شوی واقف ز احوال مشایخ</p></div>
<div class="m2"><p>که چون کوهند در آلام راسخ</p></div></div>