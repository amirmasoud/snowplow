---
title: >-
    بخش ۴۲۶ - مقام‌التنزل الربانی
---
# بخش ۴۲۶ - مقام‌التنزل الربانی

<div class="b" id="bn1"><div class="m1"><p>مقامی کوست ربانی تنزل</p></div>
<div class="m2"><p>دم رحمانی آمد بی‌تأمل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود اعنی ظور آنوجودی</p></div>
<div class="m2"><p>که می‌باشد حقایق ز او نمودی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ظهور او کرد در کل مراتب</p></div>
<div class="m2"><p>تعینهای مبسوط مناسب</p></div></div>