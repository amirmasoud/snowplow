---
title: >-
    بخش ۲۲۲ - عبدالقهار
---
# بخش ۲۲۲ - عبدالقهار

<div class="b" id="bn1"><div class="m1"><p>دگر هم عبدقهار است در کار</p></div>
<div class="m2"><p>که باشد مظهر او بر اسم قهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز حق دارد خود او توفیق و تایید</p></div>
<div class="m2"><p>که از وی نفس یابد قهر و تهدید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بقهر نفس دارد اهتمامی</p></div>
<div class="m2"><p>بقهاریت استش احتشامی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر آن فعلی که معلوم است و مستور</p></div>
<div class="m2"><p>ز نفس از وی شود معدوم و مقهور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنفس خویش و غیر است او مسلط</p></div>
<div class="m2"><p>روانها داده بر دارائیش خط</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر اکوان او تواند کرد تأثیر</p></div>
<div class="m2"><p>نه اکوانش تواند داد تغییر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر آن شیئی تجاوز یابد از حد</p></div>
<div class="m2"><p>ز قهر او بجای خود شود رد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در اکوان فعل و تأثیرش چنین است</p></div>
<div class="m2"><p>فلک در پیش عزمش چون زمین است</p></div></div>