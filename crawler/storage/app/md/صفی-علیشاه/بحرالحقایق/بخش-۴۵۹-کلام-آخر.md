---
title: >-
    بخش ۴۵۹ - کلام آخر
---
# بخش ۴۵۹ - کلام آخر

<div class="b" id="bn1"><div class="m1"><p>ز نفست گر شد این سرها ربوده</p></div>
<div class="m2"><p>نماند در تو وصفی ناستوده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو این اوصاف سبعه جمله اصلند</p></div>
<div class="m2"><p>بر آینها سایر اوصاف وصلند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مثال کینه و ظلم و عداوت</p></div>
<div class="m2"><p>که آنها بر غضب دارند نسبت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدینسان سایر اوصاف دیگر</p></div>
<div class="m2"><p>فروعاتند و می‌رویند از سر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهر جا نسبت هر یک عیانست</p></div>
<div class="m2"><p>نپندارم که محتاج بیان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر یکوصف جائی جلوه‌گر هست</p></div>
<div class="m2"><p>ز خوب و بد ز باقی هم اثر هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خصال بد ولیکن جز عرض نیست</p></div>
<div class="m2"><p>نه چون خق نکو عینی و ذاتی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی نفس تو چون اماره آید</p></div>
<div class="m2"><p>بدیها جمله زان پتاره آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>و گر لوامه شد از قلب نوری</p></div>
<div class="m2"><p>بر او تابد کزو یا حضوری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باندازه تنبه کانتقالش</p></div>
<div class="m2"><p>دهد از نوم غفلت وز ضلالش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مردد در میان نور و ظلمت</p></div>
<div class="m2"><p>که کون حق و خلق است آن بنسبت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کند گاهی طلب از توبه مفتاح</p></div>
<div class="m2"><p>گشاید بلکه‌یابی بهر اصلاح</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از آن حیثی که سجینی صفاتست</p></div>
<div class="m2"><p>همه میلش بسوی سینات است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وزان وجهی که علیین اساس است</p></div>
<div class="m2"><p>تدارک را مهیا بر حواس است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ازو ظاهر شد ار وقتی قباحت</p></div>
<div class="m2"><p>کند خود را ملامت زان وقاحت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دگر تا نفس را معیار چبود</p></div>
<div class="m2"><p>کمال و فهم و استحضار چبود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر پس جاهلی بر کس عطائی</p></div>
<div class="m2"><p>کند بر وی مگو بود این ریائی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که سازی از نوا دادن ملولش</p></div>
<div class="m2"><p>کجا داند ریا نفس جهولش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بود تکلیف هر کس قدر فهمش</p></div>
<div class="m2"><p>باستعداد هم حق داده سهمش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عطای او بمحض خود نمائی است</p></div>
<div class="m2"><p>دگر مشعر بر اخلاص و ریانیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شود ور نفس یکجا مطمئنه</p></div>
<div class="m2"><p>یقینش فارغ از وهم و مظنه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز اخلاق ذمیمه خلع و معذور</p></div>
<div class="m2"><p>باوصاف حمیده جمع و معمور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>توجه جمله باشد سوی قلبش</p></div>
<div class="m2"><p>که بر خود روح قدسی کرده جلبش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کند تشییع قلب اندر ترقی</p></div>
<div class="m2"><p>سوی اقلیم قدس و کون حقی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دو اسبه تازد او تا مالک الله</p></div>
<div class="m2"><p>تو گورو دست مولایت بهمراه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>صفی هم هست چشمش در رهی باز</p></div>
<div class="m2"><p>تو دانی ایکه دانایی بهر راز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو می‌دانی که علام الغیوبی</p></div>
<div class="m2"><p>پناه و خالق هر زشت و خوبی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دعا ما بین رب و عبد غیر است</p></div>
<div class="m2"><p>بهر حالم تو داری باز خیر است</p></div></div>