---
title: >-
    بخش ۲۰۳ - العارالعظیم
---
# بخش ۲۰۳ - العارالعظیم

<div class="b" id="bn1"><div class="m1"><p>دگر عار عظیم اندر ضمیرت</p></div>
<div class="m2"><p>که شد تفسیر بر مقت کبیرت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز نقص عهد می‌باشد اشارت</p></div>
<div class="m2"><p>که هست از قول بیفعلت امارت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر در جمع مردان داخلی خود</p></div>
<div class="m2"><p>همان میگو که بروی عاملی خود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهیچ اندازه‌ئی قابل نباشی</p></div>
<div class="m2"><p>که گوئی آنچه را عامل نباشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز گفتاری که دروی نیست کردار</p></div>
<div class="m2"><p>حذر کن تا نگیرد در گلوخار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز قول بی‌حقیقت نزد معبود</p></div>
<div class="m2"><p>نبد مبغوض‌تر ز اشیاء مردود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز حق بر تک اینحال از متانت</p></div>
<div class="m2"><p>روا باشد که جوئی استعانت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسا لعنت بر آن دستار و ریش است</p></div>
<div class="m2"><p>که کذبش بر خدا و خلق و خویش است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غرض لفظی که او بی‌اعتبار است</p></div>
<div class="m2"><p>به پیش اهل غیرت سخت عار است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر آن قولی که دروی نیست افعال</p></div>
<div class="m2"><p>نباشد صاحبش را رو باقبال</p></div></div>