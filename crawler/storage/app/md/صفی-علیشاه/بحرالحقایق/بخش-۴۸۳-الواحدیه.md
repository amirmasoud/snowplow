---
title: >-
    بخش ۴۸۳ - الواحدیه
---
# بخش ۴۸۳ - الواحدیه

<div class="b" id="bn1"><div class="m1"><p>شنو باز از نشان واحدیت</p></div>
<div class="m2"><p>که هست آن اعتبار ذات حضرت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز حیث انتشاء کل اسماء</p></div>
<div class="m2"><p>از او گر باز دانی سر انشاء</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تجلی حضرت آمد واحدیت</p></div>
<div class="m2"><p>در اسماء و صفات او بر تبت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بصورتهای اسماء کوست اعیان</p></div>
<div class="m2"><p>که لازم شد باسماء و صفات آن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لزومش بی‌تأخر در وجود است</p></div>
<div class="m2"><p>بنز در هر وی کاهل شهود است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خود او موجود بر هستی اسماست</p></div>
<div class="m2"><p>که موجود آن بهستی مسمی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خود این را واحدیت خواند عارف</p></div>
<div class="m2"><p>تجلی دگر نزد مکاشف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که شد بی‌اسم و رسم از ذات بر ذات</p></div>
<div class="m2"><p>احدیت شد آن بروجه اثبات</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تجلییی که از ذات وجود است</p></div>
<div class="m2"><p>در افعالش که در غیب و شهود است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وجود انبساطی شد نشان را</p></div>
<div class="m2"><p>تو میدان رحمت فعلیه آنرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مراد از واحدیت بود واحد</p></div>
<div class="m2"><p>مناسب شرح آن در مطلب آمد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بود آن واحدیت وصف ذاتش</p></div>
<div class="m2"><p>تکثر یافت از حیث صفاتش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دگر واحد که اسماء را مدارا است</p></div>
<div class="m2"><p>هم اسم ذات بر این اعتبار است</p></div></div>