---
title: >-
    بخش ۲۷۹ - عبدالاول
---
# بخش ۲۷۹ - عبدالاول

<div class="b" id="bn1"><div class="m1"><p>تو عبدالاول آنرا دان که رؤیت</p></div>
<div class="m2"><p>در اشیاء کرد وجه اولیت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بکل شی‌ء بیند وجه حق را</p></div>
<div class="m2"><p>که اول گشت منشأ ما خلق را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تحقق یافت او بر اسم اول</p></div>
<div class="m2"><p>پس او خود اول الاشیاءست و افضل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خود او بر اولیت گشت لایق</p></div>
<div class="m2"><p>بطاعات و بخیراتست سابق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دگر بیند ازل را در تعلق</p></div>
<div class="m2"><p>چو بر اسم ازل یابد تحقق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شود واقف همانا بالخلیقه</p></div>
<div class="m2"><p>که سلطان ازل شد بالحقیقه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو خلقیت حدوثش بالعیانست</p></div>
<div class="m2"><p>ازل دور از حدوث کن فکانست</p></div></div>