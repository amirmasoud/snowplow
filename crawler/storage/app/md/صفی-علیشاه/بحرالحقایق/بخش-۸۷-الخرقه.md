---
title: >-
    بخش ۸۷ - الخرقه
---
# بخش ۸۷ - الخرقه

<div class="b" id="bn1"><div class="m1"><p>تو را گر هست ذوقی از تصوف</p></div>
<div class="m2"><p>بیان خرقه بشنو بی‌تکلف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرید صاف دل چون گشت داخل</p></div>
<div class="m2"><p>بزی شیخ صاحب دلق کامل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو کرد از باطن شیخ اقتباسی</p></div>
<div class="m2"><p>بر او پوشند از تقوی لباسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا آنرا خرقه تجرید خوانند</p></div>
<div class="m2"><p>بری ز آلایش تقلید خوانند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود از رجس تزویر و ریا پاک</p></div>
<div class="m2"><p>دگر از لوث استدراج و اشراک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دگر از ننگ و آز و حرص ذمیمه</p></div>
<div class="m2"><p>اگر از نقص اجرام عظیمه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دگر از عجب و رعنایی و مستی</p></div>
<div class="m2"><p>دگر از فخر و دارائی و هستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دگر از فعل و ترک ناموافق</p></div>
<div class="m2"><p>دگر از مدح و ذعم غیر لایق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دگر از هر چه زاید در کلام است</p></div>
<div class="m2"><p>دگر از هر چه زیبا بر عوام است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دگر از هر چه کاندر عقل ننگست</p></div>
<div class="m2"><p>دگر از هر چه کاندر فقر رنگست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دگر از حب دنیا و لوازم</p></div>
<div class="m2"><p>دگر از فکر کونین و مراسم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بود این در حقیقت جان سپردن</p></div>
<div class="m2"><p>کفن پوشیدن و از خویش مردن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نشان خرقه پوشان عیب پوشیست</p></div>
<div class="m2"><p>خودیت هشتن الا خود فروشیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بستاری علی را خرقه دادند</p></div>
<div class="m2"><p>بر او مولائی این فرقه دادند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر آن ستار و از دنیاست تارک</p></div>
<div class="m2"><p>بر او این خرقه بس باشد مبارک</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که از وی نسج صوفت منتسج شد</p></div>
<div class="m2"><p>صفا در تار و پودش مندرج شد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صفی داند که وضع خرقه چونست</p></div>
<div class="m2"><p>بزیر آن نهان دریای خون است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو کاندر آب جوئی غرقه گردی</p></div>
<div class="m2"><p>شناور چون ببحر خرقه گردی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مزن چون پشه دارد در تو تأثیر</p></div>
<div class="m2"><p>دو از چنگال ببر و پنجه شیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چه کردی در شریعت کز تکلف</p></div>
<div class="m2"><p>فتادت بر سر آشوب تصوف</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مپوش اینجامه کز بهر تو تنگست</p></div>
<div class="m2"><p>یم آن خواهد که هم سیر نهنگست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نمیری تا ز خوی خود فروشان</p></div>
<div class="m2"><p>یکی مگذر ز کوی خرقه‌پوشان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بظاهر خرقه باشد جامه فقر</p></div>
<div class="m2"><p>ولی باطن پر از هنگامه فقر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نه هر کس خرقه پوشد ره نور دست</p></div>
<div class="m2"><p>هر آن از دلق هستی رست مرداست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>غرض چون شد ز دست شیخ کامل</p></div>
<div class="m2"><p>مریدی در لباس فقر داخل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همی‌ باید بحسن خرقه کوشد</p></div>
<div class="m2"><p>بهر دم از کمالی خرقه پوشد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بود از باطن شیخ التماسش</p></div>
<div class="m2"><p>کهب اشد تازه‌تر دائم لباسش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همیشه خرقه صوفی بود نو</p></div>
<div class="m2"><p>نگردد کهنه هرگز دلق رهرو</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زنو مقصود تبدیل و ترقی است</p></div>
<div class="m2"><p>که ساکن راهرو در منزلی نیست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نماند هیچ یکدم در مقامی</p></div>
<div class="m2"><p>کند هر صبج استقبال شامی</p></div></div>