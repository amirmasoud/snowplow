---
title: >-
    بخش ۱۳۷ - حکایت
---
# بخش ۱۳۷ - حکایت

<div class="b" id="bn1"><div class="m1"><p>مریدی خواست از پیری اجازت</p></div>
<div class="m2"><p>که در کوهی کشد چندی ریاضت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آن مغازه روی دید ماری</p></div>
<div class="m2"><p>گرفت او را بدست از اختیاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گزیدش مار و بردندش به تدبیر</p></div>
<div class="m2"><p>ز بیم مرگ او را جانب پیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگفتش پیر عقلت از چه شدپست</p></div>
<div class="m2"><p>که بگرفتی تو مار خفته بردست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگفتا از تو بشنیدم که اشیاء</p></div>
<div class="m2"><p>همه حقند در پنهان و پیدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بکف زانرو گرفتم اژدها را</p></div>
<div class="m2"><p>که با خود آشنا دیدم خدا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگفتش پیر حق در صورت قهر</p></div>
<div class="m2"><p>اگر بینی ازو بگریز صد شهر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بصورتهای لطفی سوی او رو</p></div>
<div class="m2"><p>ز صورتهای قهرش بر حذر شو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چوبی هر صورتش دیدی معادل</p></div>
<div class="m2"><p>ترا سر حقیقت گشته حاصل</p></div></div>