---
title: >-
    بخش ۴۴۱ - باب‌النون النبوه
---
# بخش ۴۴۱ - باب‌النون النبوه

<div class="b" id="bn1"><div class="m1"><p>نبوت از حقایق باشد اخبار</p></div>
<div class="m2"><p>که شد عرفان ذات حق و آثار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دگر اسماء و اوصاف الهی</p></div>
<div class="m2"><p>دگر احکام هر نظمی کماهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو قسم است آن نبوت‌گاه توزیع</p></div>
<div class="m2"><p>یکی تعریف و دیگر باز تشریع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود تعریف: انباء او بعرفان</p></div>
<div class="m2"><p>در اسماء و صفات و ذات سبحان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دگر هم آن که تشریعش بود نام</p></div>
<div class="m2"><p>بزاید باشدش تبلیغ احکام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم اخلاق و سیاسات و عبادات</p></div>
<div class="m2"><p>دگر هم علم حکمت در افادات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز تعریفست اعلی در جلالت</p></div>
<div class="m2"><p>محقق خوانده است او را رسالت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ولی آگاه از این علم و اصولست</p></div>
<div class="m2"><p>ولی ز اخبار و انبائش ملول است</p></div></div>