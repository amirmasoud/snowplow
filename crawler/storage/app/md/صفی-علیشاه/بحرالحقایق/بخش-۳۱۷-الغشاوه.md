---
title: >-
    بخش ۳۱۷ - الغشاوه
---
# بخش ۳۱۷ - الغشاوه

<div class="b" id="bn1"><div class="m1"><p>غشاوه پرده‌ئی باشد که حایل</p></div>
<div class="m2"><p>شود مرآت دل را در مشاغل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غشا خوانندش از بر دل نشیند</p></div>
<div class="m2"><p>چو آید بر بصر تاریک بیند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدل آنهم تواندکز صدا شد</p></div>
<div class="m2"><p>صدا آن خاطره‌های ناروا شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود خود این صدا جز آن صدایت</p></div>
<div class="m2"><p>که بر گوش آید آن بیمدعایت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگوش آید اگر در ره صدائی</p></div>
<div class="m2"><p>بود رو بر قفا کردن خطائی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غشاوه هست باری از صدایت</p></div>
<div class="m2"><p>نه بر گوش آنکه آید از قفایت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صدای هر که بر اندازه اوست</p></div>
<div class="m2"><p>ز نفس دون و فکر تازه اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غرض پیدا شود بر دل غشائی</p></div>
<div class="m2"><p>که افتد روی مرآت از جلائی</p></div></div>