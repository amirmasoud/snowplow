---
title: >-
    بخش ۲۴۹ - عبدالرقیب
---
# بخش ۲۴۹ - عبدالرقیب

<div class="b" id="bn1"><div class="m1"><p>بود عبدالرقیب آنکس که در کیش</p></div>
<div class="m2"><p>بخود بیند رقیبش اقرب از خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس از درک فنای خویش بینی</p></div>
<div class="m2"><p>که این اسمش کند برجان تجلی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندارد از حدود حق تجاوز</p></div>
<div class="m2"><p>مراعاتش بحد است از تمایز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بپاس نفس خویش و نظم مولا</p></div>
<div class="m2"><p>مراقب تر کسی زو نیست اصلا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدینسان در مقامات و مراتب</p></div>
<div class="m2"><p>بود بر حال اصحابش مراقب</p></div></div>