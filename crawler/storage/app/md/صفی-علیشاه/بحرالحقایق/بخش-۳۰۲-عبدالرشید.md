---
title: >-
    بخش ۳۰۲ - عبدالرشید
---
# بخش ۳۰۲ - عبدالرشید

<div class="b" id="bn1"><div class="m1"><p>دگر از اولیا عبدالرشید است</p></div>
<div class="m2"><p>که در عالم بهر رشدی وحید است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود ساعی در ارشاد خلایق</p></div>
<div class="m2"><p>بدنیا و بدین بر قدر لایق</p></div></div>