---
title: >-
    بخش ۲۵۱ - عبدالواسع
---
# بخش ۲۵۱ - عبدالواسع

<div class="b" id="bn1"><div class="m1"><p>ز عبدالواسعت گویم بواقع</p></div>
<div class="m2"><p>که باشد مظهر او بر اسم واسع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر آنچه باشد اندرعون و حولش</p></div>
<div class="m2"><p>دهد وسعت بر آن از فضل و طولش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز چیزی وسعت اوما بواجب</p></div>
<div class="m2"><p>نیابد کوست غالب بر مراتب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه بیند مستحقی در مقامش</p></div>
<div class="m2"><p>جز آن کاعطا کند هر صبح و شامش</p></div></div>