---
title: >-
    بخش ۳۷۷ - المبدئیت
---
# بخش ۳۷۷ - المبدئیت

<div class="b" id="bn1"><div class="m1"><p>اضافه محض باشد مبدئیت</p></div>
<div class="m2"><p>احد هم کاقدم است از واحدیت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>احدیت مر او را خود تقدم</p></div>
<div class="m2"><p>بود بر واحدیت بی‌تکلم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو او خود منشأ کل صفاتست</p></div>
<div class="m2"><p>تعینها زوی چون منشئات است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اضافه خود اشاراتیست عقلی</p></div>
<div class="m2"><p>بمعنی اعتباراتیست عقلی</p></div></div>