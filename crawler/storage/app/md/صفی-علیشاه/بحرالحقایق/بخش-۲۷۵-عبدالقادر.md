---
title: >-
    بخش ۲۷۵ - عبدالقادر
---
# بخش ۲۷۵ - عبدالقادر

<div class="b" id="bn1"><div class="m1"><p>ز عبدالقادر ار خواهی شد آگاه</p></div>
<div class="m2"><p>خود او شاهد بود بر قدرت الله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>الا در کل مقدورات صادر</p></div>
<div class="m2"><p>کند بروی تجلی اسم قادر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدان پس صورت دست الهش</p></div>
<div class="m2"><p>که گیرد باوی و دارد نگاهش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نباشد لطف او را امتناعی</p></div>
<div class="m2"><p>بود ز و هر عطا و انتزاعی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عیانش وجه تأثیر مؤثر</p></div>
<div class="m2"><p>شود بر کل بتقدیر مقدر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم ایصال مدد کورا دوام است</p></div>
<div class="m2"><p>بمعدوم از وجود کل که تام است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رسد امداد هستی هر دم از وی</p></div>
<div class="m2"><p>بمعد و مات اعیانی پیاپی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مع‌الکونی که معدم الذواتند</p></div>
<div class="m2"><p>بمعدومیت خود بر ثباتند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به بیند نفس خود معدم بالذات</p></div>
<div class="m2"><p>چنان کاندر عدم بد ذات ذرات</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بعین آنکه در اشیاء مؤثر</p></div>
<div class="m2"><p>بود با قدرت حق بی‌مغایر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بعین آنکه دارد قدرت از حق</p></div>
<div class="m2"><p>در آنقدرت بود معدم مطلق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز حق باشد بقدرت گر چه معلوم</p></div>
<div class="m2"><p>نه بیند ذات خود را جز که معدوم</p></div></div>