---
title: >-
    بخش ۳۲۳ - باب‌الفاء الفتق
---
# بخش ۳۲۳ - باب‌الفاء الفتق

<div class="b" id="bn1"><div class="m1"><p>بود فتق آن بنزد اهل تحقیق</p></div>
<div class="m2"><p>که هست از ماده بالجمله تفصیل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز مطلق ماده تفصیل قابل</p></div>
<div class="m2"><p>که صورتهای نوعی راست حامل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ظهور آنچه اندر واحدیت</p></div>
<div class="m2"><p>ز اسماء بود مستور آن به نسبت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بروز آنچه مخفی بود در ذات</p></div>
<div class="m2"><p>بقدر قابلیت از شئونات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شئوناتی که بر کونست لایق</p></div>
<div class="m2"><p>معین شد بخارج آن حقایق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برون آمد زبحر ذات چون موج</p></div>
<div class="m2"><p>حقایق دسته دسته فوج در فوج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درآمد از حصار افواج وصف بست</p></div>
<div class="m2"><p>صفوف کاینات از هر طرف بست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حروفی کز دوات آمد در اوراق</p></div>
<div class="m2"><p>خود آنرا فتق خوانند اهل اشراق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدینسان فتق تفصیل نبات است</p></div>
<div class="m2"><p>که قبل از فتق مخفی در نوات است</p></div></div>