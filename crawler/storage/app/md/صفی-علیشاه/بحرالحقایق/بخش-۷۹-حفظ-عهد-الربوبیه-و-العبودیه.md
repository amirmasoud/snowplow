---
title: >-
    بخش ۷۹ - حفظ عهد الربوبیه و العبودیه
---
# بخش ۷۹ - حفظ عهد الربوبیه و العبودیه

<div class="b" id="bn1"><div class="m1"><p>یکی دیگر ز حفظ العهد آنست</p></div>
<div class="m2"><p>که در این سیر سالک را نشانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز رب بیند کمال و حسن خالص</p></div>
<div class="m2"><p>ز عهد ناقص اهمال و نقایص</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر آن خیری که بینی از خدادان</p></div>
<div class="m2"><p>شرور از نفس شوم خودنما دان</p></div></div>