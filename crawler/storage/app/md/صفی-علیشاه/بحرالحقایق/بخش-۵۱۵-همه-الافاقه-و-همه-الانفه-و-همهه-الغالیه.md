---
title: >-
    بخش ۵۱۵ - همه‌الافاقه و همه‌الانفه و همهه الغالیه
---
# بخش ۵۱۵ - همه‌الافاقه و همه‌الانفه و همهه الغالیه

<div class="b" id="bn1"><div class="m1"><p>ز همتها یکی باشد افاقت</p></div>
<div class="m2"><p>که اول همت است اندر لیاقت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود آن باعث ارجوئی نشانی</p></div>
<div class="m2"><p>بکسب باقی و هم ترک فانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو ثانی همت آنرا آنفه دان</p></div>
<div class="m2"><p>درجه ثانی از همت بود آن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کراهت باشد آنرا زاجر اعمال</p></div>
<div class="m2"><p>توقع نبودش پاداش افعال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نخواهد در عمل از حق جزائی</p></div>
<div class="m2"><p>ندارد در نظر خوف و رجائی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیم همت بود اندر توالی</p></div>
<div class="m2"><p>خود آن زارباب همتهای عالی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که ایشانرا تعلق جز بحق نیست</p></div>
<div class="m2"><p>نظر هیچ از رهی بر ما خلق نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه هرگز ملتفت بر ممکناتند</p></div>
<div class="m2"><p>نه واقف هم در اسماء و صفاتند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه از مستقبل آگه نی زماضی</p></div>
<div class="m2"><p>نه از حال و مقامی شاد و راضی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه مقصودشان تعظیم ذاتست</p></div>
<div class="m2"><p>فراغت همشانرا از جهاتست</p></div></div>