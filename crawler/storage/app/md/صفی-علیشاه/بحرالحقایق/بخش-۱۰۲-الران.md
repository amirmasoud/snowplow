---
title: >-
    بخش ۱۰۲ - الران
---
# بخش ۱۰۲ - الران

<div class="b" id="bn1"><div class="m1"><p>چه باشد ران‌، حجابی کوست حایل</p></div>
<div class="m2"><p>میان ملک قدس و عالم دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز استیلا نفس و هیئت آن</p></div>
<div class="m2"><p>ز جسمانی جهان و ظلمت آن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که بالکلیه آن آثار و ظلمت</p></div>
<div class="m2"><p>کند محجوب ز انوار حقیقت</p></div></div>