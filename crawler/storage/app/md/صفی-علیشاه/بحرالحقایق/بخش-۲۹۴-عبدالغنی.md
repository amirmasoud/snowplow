---
title: >-
    بخش ۲۹۴ - عبدالغنی
---
# بخش ۲۹۴ - عبدالغنی

<div class="b" id="bn1"><div class="m1"><p>ز حق عبدالغنی دارد غنائی</p></div>
<div class="m2"><p>که ز استغنا نبیند ما سوائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمود از ما سوا حق بی‌نیازش</p></div>
<div class="m2"><p>باستغناست از خلق امتیازش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بمحتاجان عطا بی‌مسئلت کرد</p></div>
<div class="m2"><p>باستعداد لیک آن موهبت کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دهد بی مسئلت الا که قائل</p></div>
<div class="m2"><p>باستعداد باشد نطق سائل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دهد یعنی عطاگر بی‌سوالی</p></div>
<div class="m2"><p>طلب ز او کرده‌اند از نطق حالی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه باشد فقر ذاتی بهر ممکن</p></div>
<div class="m2"><p>بمعنی افتقار اوست بین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوی آن کو بهمت‌هاست جامع</p></div>
<div class="m2"><p>باو فقر و غناها جمله راجع</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غنا چون بحرش اندر آستین است</p></div>
<div class="m2"><p>بذات خود غنی از عالمین است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو ذاتش فانی اندر ذات حق است</p></div>
<div class="m2"><p>غنایش ثابت از اثبات حق است</p></div></div>