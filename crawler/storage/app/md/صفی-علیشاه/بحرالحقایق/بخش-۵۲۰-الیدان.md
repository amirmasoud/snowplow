---
title: >-
    بخش ۵۲۰ - الیدان
---
# بخش ۵۲۰ - الیدان

<div class="b" id="bn1"><div class="m1"><p>یدان دو اسم را دان بالتقابل</p></div>
<div class="m2"><p>بود فعل و قبولت گر تعقل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی اندر مقام فاعلیت</p></div>
<div class="m2"><p>دگر باشد بوصف قابلیت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی باشد از آن دو اسم فاعل</p></div>
<div class="m2"><p>دگر اندر مقابل باز قابل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک اندر وجوب آمد بمیزان</p></div>
<div class="m2"><p>دگر هم در مقابل حیث امکان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بامکان و وجوب اندر تقابل</p></div>
<div class="m2"><p>شد اسماء حضرت او مجمع کل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بشیطان شد ز حق توبیخ هم ذم</p></div>
<div class="m2"><p>که در سجده چه منعت داشت ز آدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو دیدی کامتحان خلق و خورا</p></div>
<div class="m2"><p>بدست خود نمودم خلق او را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو دانستی که جز حق نیست فاعل</p></div>
<div class="m2"><p>هم آدم از قبول ماست قابل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غرض اسمیست اندر فاعلیت</p></div>
<div class="m2"><p>مقابل باز هم در قابلیت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر آن اسمیست ثابت بهر فاعل</p></div>
<div class="m2"><p>بود هم بهر قابل در مقابل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ممیت و محیی اندر فاعلیت</p></div>
<div class="m2"><p>حیات و موت اندر قابلیت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بنافع منتفع را دان مقابل</p></div>
<div class="m2"><p>متضرر باسم ضار قابل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جهان بر پا از این فعل و قبولست</p></div>
<div class="m2"><p>که واضح بالتقابل بر عقولست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تقابل را توان هم عام کردن</p></div>
<div class="m2"><p>بفاعل منتسب در نام کردن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مقابل هم چنانکه نافع و ضار</p></div>
<div class="m2"><p>بود دیگر لطیف و باز قهار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بقابل در تقابل دان مرادف</p></div>
<div class="m2"><p>انیس و هائب و راجی و خائف</p></div></div>