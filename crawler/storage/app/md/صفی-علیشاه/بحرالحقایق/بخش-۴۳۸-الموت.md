---
title: >-
    بخش ۴۳۸ - الموت
---
# بخش ۴۳۸ - الموت

<div class="b" id="bn1"><div class="m1"><p>بود گر معنی موتت به نیت</p></div>
<div class="m2"><p>شد آن قمع هواهای طبیعت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود در اصطلاح قوم مردن</p></div>
<div class="m2"><p>بترک آرزوها جمله کردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گذشتن از همه لذات و شهوات</p></div>
<div class="m2"><p>نگشتن گرد نفس و مقتضیات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نباشد جز که از نفس پرأفت</p></div>
<div class="m2"><p>هواهائی که بینی در طبیعت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون میل نفس گردد سوی اسفل</p></div>
<div class="m2"><p>شود مرقلب را جاذب بأنزل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بمیرد از حیات معنوی قلب</p></div>
<div class="m2"><p>شود ز او نور علم و معرفت سلب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بتیه جهل و نادانی شود گم</p></div>
<div class="m2"><p>بود در زمره انعام و بلهم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بمیرد ور که نفس از آرزوها</p></div>
<div class="m2"><p>هم از میل مجاز و خلق و خوها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نماید قلب رو بر عالم قدس</p></div>
<div class="m2"><p>چه او را هم بآن عالم بود انس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>محبت باشدش بالطبع و بالاصل</p></div>
<div class="m2"><p>بملک قدس و هم بروی شود وصل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رسد او را حیات و نور ذاتی</p></div>
<div class="m2"><p>که نوبد در ردیف او مماتی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فلاطون داده خود فتوی زیاده</p></div>
<div class="m2"><p>بر این موتت ز «موتوابالاراده»</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کزان پس بالطبیعه زنده گردی</p></div>
<div class="m2"><p>در اقلیم بقا پاینده گردی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بود فرموده جعفر موت توبت</p></div>
<div class="m2"><p>که هست از ما سواللهت بنوبت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه تنها توبه‌ات از سیئات است</p></div>
<div class="m2"><p>که هم از هر چیز حتی از وجود است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نه تنها مردنست از دارناسوت</p></div>
<div class="m2"><p>که هم باید گذشت از ملک لاهوت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نه تنها کن تو ممکن را فراموش</p></div>
<div class="m2"><p>که باید شد هم از واجب کفن‌پوش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پس از مردن مشو بند حیاتش</p></div>
<div class="m2"><p>گذر کن هم ز اسماء و صفاتش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بقاها کایدت بعد از فناها</p></div>
<div class="m2"><p>قلندر شو بمیر از آن بقاها</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مراتب راز عشقش زیر پی کن</p></div>
<div class="m2"><p>به پیشت هر چه آید ترک وی کن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>غمی جو کز پیش شادی نخواهی</p></div>
<div class="m2"><p>خرابی شو که آبادی نخواهی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو مردی هم ز ممکن هم ز واجب</p></div>
<div class="m2"><p>گذشتی از مقامات و مراتب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سراغی از صفی گر در مقامی</p></div>
<div class="m2"><p>شدت بر روی رسان از ما سلامی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که او آواره از کون و مکان شد</p></div>
<div class="m2"><p>نه تنها بیکس و بیخانمان شد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>و را هر جا نشست از ضعف راندند</p></div>
<div class="m2"><p>بهر دامی فتار او را رهاندند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نرفت از خود بزنجیرش کشدیند</p></div>
<div class="m2"><p>بقهرش بند هر قیدی بریدند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بمرد او از جمادی و نباتی</p></div>
<div class="m2"><p>دگر از رتبه وصفی و ذاتی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>امید از ممکناتش منقطع گشت</p></div>
<div class="m2"><p>غبار و همش از ره مرتفع گشت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نماند او را غم بیگانه و خویش</p></div>
<div class="m2"><p>شد آزاد از خیال مذهب و کیش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چه در کثرت نماند آثار و نامش</p></div>
<div class="m2"><p>یقین بود اینکه وحدت شد مقامش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از آنجا هم کشیدیدنش که برخیز</p></div>
<div class="m2"><p>یکی خاک فنائی جو بسر ریز</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از این موت و مکانها حاصلی نیست</p></div>
<div class="m2"><p>تو را جز لامکانی منزلی نیست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مراد از لامکانی بسی نشانیست</p></div>
<div class="m2"><p>که آخر موت ارباب معانیست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کند خود را و موتش را فراموش</p></div>
<div class="m2"><p>فتد ز ادراک هستی مست و مدهوش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>صفی را خنده آید کز قساوت</p></div>
<div class="m2"><p>کند هر کس باو نوعی عداوت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>باقسماش کنند از رتبه زایل</p></div>
<div class="m2"><p>بآزارش شوند از کینه مایل</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از آنغافل که با اهل فنا جنگ</p></div>
<div class="m2"><p>بود چون آنکه بر دریا زدن سنگ</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نه دریا پر شود از سنگ اطفال</p></div>
<div class="m2"><p>نه بر خشم آید از غوغای جهال</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>فقیر از طعن کس گرد نه دلتنگ</p></div>
<div class="m2"><p>زنند این غافلان برجام خود سنگ</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>صفی کی بر ستوه آید ز کوران</p></div>
<div class="m2"><p>خورد چندار لگدها زین ستوران</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نمیی تا خود از خلق و خصالت</p></div>
<div class="m2"><p>نباشد درک حال اهل حالت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نیابی سر اینمعنی که درویش</p></div>
<div class="m2"><p>ندارد چون ز جور خلق تشویش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نگیرد در مقامی نقش و رنگی</p></div>
<div class="m2"><p>نباشد با گروهش صلح و جنگی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نباشد یادش از موجود و معدوم</p></div>
<div class="m2"><p>که تا گردد از آن خورسند و مهمور</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چه پروامرده را از ننگ و از نام</p></div>
<div class="m2"><p>بفکرش کی رسد تعریف و دشنام</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>دمی از خود بمیر آنگه توقف</p></div>
<div class="m2"><p>نما درحال ارباب تصوف</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بمیر از خود دمی وانگه نظر کن</p></div>
<div class="m2"><p>در اینعالم یکی سیر دگر کن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به بین تا چیست سر موت احمر</p></div>
<div class="m2"><p>که آن باشد خلاف نفس ابتر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نبی گوید جهاد اکبر اینست</p></div>
<div class="m2"><p>که راجع بر جهاد مشرکین است</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>خود این راموت جامع گفته صوفی</p></div>
<div class="m2"><p>در این بستر براحت خفته صوفی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هر آن مرد از هوای نفس گمراه</p></div>
<div class="m2"><p>حیات ازمعرفت یابد بدلخواه</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بمیرد از جهالت پس شود وی</p></div>
<div class="m2"><p>بنور علم و عرفان هر نفس حی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>«او من کان میتا» در عبارت</p></div>
<div class="m2"><p>«فاحییناه» زین آمد اشارت</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>که باید مرد از نفس و هواها</p></div>
<div class="m2"><p>شدن پس زنده بر نور و نواها</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بمیری گر یکی زین زندگانی</p></div>
<div class="m2"><p>که داری پس بحق باقی بمانی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ره مردن طلب کن گر فقیری</p></div>
<div class="m2"><p>مدد جو اول از روشن ضمیری</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>نگردد بسته نفس از هیچ‌بندی</p></div>
<div class="m2"><p>مگر از حبل عشق آری کمندی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بطاعتها که داری در تعبد</p></div>
<div class="m2"><p>ز ورد و ذکر و قرآن و تهجد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>صلوه وصوم و حج اطعام و ایثار</p></div>
<div class="m2"><p>نگردد خسته زینها نفس غدار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>کند بل همرهی در جمله اعمال</p></div>
<div class="m2"><p>که پنداری تو را یار است و هم حال</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>مگر که آری بکف دامان یاری</p></div>
<div class="m2"><p>شوی در راه عشق او غباری</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بدست او دهی دستی به پیمان</p></div>
<div class="m2"><p>به پیش حکم او برخیزی از جان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>رود سوی نشست از این قبولیت</p></div>
<div class="m2"><p>پیاپی زانوی نفس جهولت</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>تو گوئی نفس برفی در سبد بود</p></div>
<div class="m2"><p>هلاکش نزد خورشد اسد بود</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>از آنرو حمل هر باری بعادت</p></div>
<div class="m2"><p>نماید غیرتمکین واردات</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بآسانی بهر طاعت دهد دل</p></div>
<div class="m2"><p>بجز تمکین کش آید سخت مشکل</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>از آن گوید محقق نفس و ابلیس</p></div>
<div class="m2"><p>یکی بودند اندر کبر و تلبیس</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ز سجده آدم آن سرکش ابا کرد</p></div>
<div class="m2"><p>باظهار منیت ابتدا کرد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>کسی کورا طبیعت گشت غالب</p></div>
<div class="m2"><p>نگردد جز باهل طبع راغب</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>از آن نفس تو جز اماره نبود</p></div>
<div class="m2"><p>کت از میل طبیعت چاره نبود</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>اگر گاهی کنی ترک مرادش</p></div>
<div class="m2"><p>برآید آه تشویش از نهادش</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>وگر لوامه باشد در اقامت</p></div>
<div class="m2"><p>کند خود را ز فعل بد ملامت</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>وگر یکجا نمائی ترک میلش</p></div>
<div class="m2"><p>فتد آتش بخرمنها و خیلش</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ولی این بس تو را دشوار باشد</p></div>
<div class="m2"><p>مگر عون الهت یار باشد</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>که تا نفس تو گردد مطمئنه</p></div>
<div class="m2"><p>شود فارغ ز تلوین و مظنه</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>غرض باشد گر از پیری افاضت</p></div>
<div class="m2"><p>بود به نفس را از هر ریاضت</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چه تمکین واردات خیزداز عشق</p></div>
<div class="m2"><p>خضوع آید هوا بگریزد از عشق</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چو عشق آمد حریف صد سوار است</p></div>
<div class="m2"><p>از و نفس موسوس خوار و زار است</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>چو عشق آمد ز نفس ایمن توان بود</p></div>
<div class="m2"><p>که عشق از فتنها دارالامان بود</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>نماند عشق برجا حرف و نقلی</p></div>
<div class="m2"><p>نشان از قبح نفس وحسن عقلی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>چو عشق آمد وداع کفر و دین است</p></div>
<div class="m2"><p>کجا داند که آن دزد این امین است</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>نه تنها سوخت نازش ما و من را</p></div>
<div class="m2"><p>که سوزد روح و نفس و قلب و تن را</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>بسوزد نفس و نارد در مظنه</p></div>
<div class="m2"><p>که این امار بد یا مطمئنه</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>ولی بیعشق پای نفس باز است</p></div>
<div class="m2"><p>ز هر سو بر تو دست او بد یا مطمئنه</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>ولی بیعشق پای نفس باز است</p></div>
<div class="m2"><p>ز هر سو بر تو دست او دراز است</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>اگر مردی بمیرانش باقسام</p></div>
<div class="m2"><p>مراین خود موت جامع باشدش نام</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>از اینرو جامع‌اند را اصطلاح است</p></div>
<div class="m2"><p>که نفس ار مرد حالت بر صلاح است</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>ز موت نفس هر موتی شود سهل</p></div>
<div class="m2"><p>که بس ترکیبها او راست در جهل</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>اگر شد موت احمر حاصل تو</p></div>
<div class="m2"><p>دگر موتی نباشد مشکل تو</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>مراد از موت احمر موت نفس است</p></div>
<div class="m2"><p>حیات روح و عقل از فوت نفس است</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>اگر نفسی بمیرد حسن بخت است</p></div>
<div class="m2"><p>از آن پس جوع و عریانی نه سخت است</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>بود مر موت ابیض جوع درویش</p></div>
<div class="m2"><p>رجوعش نیست هرگز ضعف و تشویش</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>شود زین موت وجه قلبش ابیض</p></div>
<div class="m2"><p>بحق چون کار قوتش شد مفوض</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>رسد پس فطنتی او را بباطن</p></div>
<div class="m2"><p>حیاتی آیدش بر قلب فاطن</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>نه هر کس را نشاند حق بر این خوان</p></div>
<div class="m2"><p>شکم جز مرد را نبود بفرمان</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>نباشد میل حیوان جز بخوردن</p></div>
<div class="m2"><p>خورد کی آنکه دارد دل بمردن</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>بود آنموت اخضر بی‌مناعت</p></div>
<div class="m2"><p>که پوشی کهنه ثوبی از قناعت</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>گذاری جامه نو را باطفال</p></div>
<div class="m2"><p>سپاری این تجملها بجهال</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>نیرزد جامه‌ات گر قدر کاهی</p></div>
<div class="m2"><p>چه باک او را اگر پوشیده شاهی</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>خوش آن ثوبی که زیرش بحر جانست</p></div>
<div class="m2"><p>چو لیلی کافتابش در میان است</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>بود ایذای خلقت موت اسود</p></div>
<div class="m2"><p>صفی این موت را دیده است بیحد</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>ز خلق از هیچ آزاری نرنجید</p></div>
<div class="m2"><p>که در آزار خود خلقی نسنجید</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>سخنهائی که آن بس دلشکن بود</p></div>
<div class="m2"><p>شنیدم لیک دانستم ز من بود</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>ز طعن خلق خوردم هر چه خنجر</p></div>
<div class="m2"><p>شد آن دلدار با من مهربانتر</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>عیان گشت اینکه هر زخمی که از خلق</p></div>
<div class="m2"><p>رسد بیرون بتی اندازد از دلق</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>دغلهائی که باشد فرقه فرقه</p></div>
<div class="m2"><p>نهان از هر جهت در زیر خرقه</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>بود مر نفس قدسی را علایق</p></div>
<div class="m2"><p>ز وصل یار و الطافش عوایق</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>مرا از خلق خستها شرف شد</p></div>
<div class="m2"><p>که از راهم موانع برطرف شد</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>بهر روزی مرا بود انتظاری</p></div>
<div class="m2"><p>که بر دل آیدم از خلق خاری</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>شبم دلدار می‌گفت آن سخنها</p></div>
<div class="m2"><p>که بشنیدی بیان کن ز انجمنها</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>نپنداری که بود آنها ز خلقت</p></div>
<div class="m2"><p>منت گفتم چه بود آلوده دلقت</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>مگر تا واقف از سالوس باشی</p></div>
<div class="m2"><p>مبادا با ریا مأنوس باشی</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>مدار این غم که کج یار است گفتند</p></div>
<div class="m2"><p>خلایق هر چه امر ماست گفتند</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>بمیر از مدح و دم خلق و خوش باش</p></div>
<div class="m2"><p>بدور از طعم شیرین و ترش باش</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>بتعریف آنچه خوردی طعمه قی کن</p></div>
<div class="m2"><p>بتکذیب آنچه دید خاک پی کن</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>بمان این نیک و بدها جمله برزیر</p></div>
<div class="m2"><p>بموت خویشتن گو چار تکبیر</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>که آن خود کاشف از موت سیاهست</p></div>
<div class="m2"><p>فنای ذات عارف در الهست</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>سوادالوجه فی‌الدارین این است</p></div>
<div class="m2"><p>سیه‌روئی در این صورت یقین است</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>ز دامان تو خیز گرد امکان</p></div>
<div class="m2"><p>نه‌بینی جز حق اندر عین ایمان</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>شود چون با تو محبوبت هم آغوش</p></div>
<div class="m2"><p>زیادت ما سوا گردد فراموش</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>چو خواهد بر تو او جور خلایق</p></div>
<div class="m2"><p>ز جان برجور خلقان باش شایق</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>به پشت سرفکن کون و مکان را</p></div>
<div class="m2"><p>خود و خلق و زمین و آسمانرا</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>تو حسن موت اگر داری مسلم</p></div>
<div class="m2"><p>بمیر از غیر حق و الله اعلم</p></div></div>