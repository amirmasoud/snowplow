---
title: >-
    بخش ۲۵۸ - عبدالوکیل
---
# بخش ۲۵۸ - عبدالوکیل

<div class="b" id="bn1"><div class="m1"><p>بود عبدالوکیل آنکس ز اطیاب</p></div>
<div class="m2"><p>که بیند حق بصورتهای اسباب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود او فاعل اندر کل افعال</p></div>
<div class="m2"><p>محول دست تقدیرش در احوال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امورات جهانرا خلق محجوب</p></div>
<div class="m2"><p>بر اسبابست پندارند منسوب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آن غافل که بی تدبیر فاعل</p></div>
<div class="m2"><p>بود اسبابها بیکار و باطل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چود اعضائی که در جنبش ز جانند</p></div>
<div class="m2"><p>چو روح از تن جدا شد ناتوانند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی کاسباب را نابود بیند</p></div>
<div class="m2"><p>مسبب را در آن موجود بیند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بحق باشد توکل در امورش</p></div>
<div class="m2"><p>نه اسباب از حق اندازد بدورش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بچشم آید سببها بس علیلش</p></div>
<div class="m2"><p>گذارد باز کار او با وکیلش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>توکل بر مسبب جوید از دل</p></div>
<div class="m2"><p>که می‌بیند سببها را معطل</p></div></div>