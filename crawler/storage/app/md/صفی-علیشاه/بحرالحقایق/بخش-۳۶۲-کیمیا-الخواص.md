---
title: >-
    بخش ۳۶۲ - کیمیاء‌الخواص
---
# بخش ۳۶۲ - کیمیاء‌الخواص

<div class="b" id="bn1"><div class="m1"><p>دگر دارند خاصان کیمیائی</p></div>
<div class="m2"><p>که دنیا را کند زر بید وائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود آن اکسیر مخصوص خواص است</p></div>
<div class="m2"><p>زوی هر قلب بیغش در خلاص است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود تخلیص قلب از کون و کائن</p></div>
<div class="m2"><p>باستیثار سلطان مکون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز استیثار مقصود اختیار است</p></div>
<div class="m2"><p>که زر قلب ازو کامل عیار است</p></div></div>