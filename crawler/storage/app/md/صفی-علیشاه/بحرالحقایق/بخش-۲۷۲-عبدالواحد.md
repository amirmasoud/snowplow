---
title: >-
    بخش ۲۷۲ - عبدالواحد
---
# بخش ۲۷۲ - عبدالواحد

<div class="b" id="bn1"><div class="m1"><p>ز عبدالواحد از پرسی و حالش</p></div>
<div class="m2"><p>بود بر واحدیت اتصالش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>احدیت که بر جمع است موصوف</p></div>
<div class="m2"><p>شود او را ز اسماء جمله مکشوف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بمایدرک کند درک حقایق</p></div>
<div class="m2"><p>دگر فعلی که با اسمی است لایق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شود مکشوفش از اسماء وجوهات</p></div>
<div class="m2"><p>دگر هر وجهی نماید فعلی اثبات</p></div></div>