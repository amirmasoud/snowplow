---
title: >-
    بخش ۱۱۶ - الزجاجه
---
# بخش ۱۱۶ - الزجاجه

<div class="b" id="bn1"><div class="m1"><p>زجاجه قلب صاحب سینه باشد</p></div>
<div class="m2"><p>که وجه الله را آئینه باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مناسب باشد اینجا آیه نور</p></div>
<div class="m2"><p>بشرح آن دل از حق یافت دستور</p></div></div>