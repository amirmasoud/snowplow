---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>شنو این نکته از من ای دل آگاه</p></div>
<div class="m2"><p>به جز راه رضای حق مجو راه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی کو بر کند از ما سوا دل</p></div>
<div class="m2"><p>جهان گردد ورا بر وجه دلخواه</p></div></div>