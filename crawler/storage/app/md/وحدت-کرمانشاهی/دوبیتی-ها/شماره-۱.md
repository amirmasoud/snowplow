---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>به صدق گفته‌ام هر دل گواه است</p></div>
<div class="m2"><p>که دل‌ها را به سوی دوست راه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هر سو رو نمایی دوست آن سوست</p></div>
<div class="m2"><p>جز او را گر ببینی اشتباه است</p></div></div>