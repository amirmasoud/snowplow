---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>هرچند که ذات حق نهان است ز دید</p></div>
<div class="m2"><p>در خویش خدای خویش را بتوان دید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر خانه دل تهی شود از اغیار</p></div>
<div class="m2"><p>منزلگه یار می‌شود بی تردید</p></div></div>