---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>بی کسب کمال نقص زایل نشود</p></div>
<div class="m2"><p>الطاف خدا شامل کاهل نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مقسوم بود رزق ولیکن وحدت</p></div>
<div class="m2"><p>بی کوشش و بی تلاش حاصل نشود</p></div></div>