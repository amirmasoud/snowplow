---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>به عقل غره مشو تند پا منه در راه</p></div>
<div class="m2"><p>بگیر دامن صبر و ز عشق همت خواه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عیان در آینه کائنات حق بینید</p></div>
<div class="m2"><p>اگر به چشم حقیقت در او کنید نگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به غیر پیر خرابات و ساکنان درش</p></div>
<div class="m2"><p>ز اصل نکته توحید کس نشد آگاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رسد به مرتبه‌ای خواجه پایه توحید</p></div>
<div class="m2"><p>که عین شرک بود لا اله الا الله</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر آفتاب حقیقت بتابدت در دل</p></div>
<div class="m2"><p>دمد ز مشرق جانت هزار کوکب و ماه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز روی زرد و لب خشک و چشم تر پیداست</p></div>
<div class="m2"><p>نشان عشق چه حاجت به شاهد است و گواه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به کیش اهل حقیقت جز این گناهی نیست</p></div>
<div class="m2"><p>که پیش رحمت عامش برند نام گناه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگر به یاری عشق ای حکیم، ور نه به عقل</p></div>
<div class="m2"><p>کسی نیافته بر حل این معما راه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چرا مقیم حرم گشت شیخ جامه سفید</p></div>
<div class="m2"><p>شد از چه معتکف دیر رند نامه سیاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرت هواست که بر سر نهند افسر عشق</p></div>
<div class="m2"><p>گدائی در میخانه کن چو وحدت شاه</p></div></div>