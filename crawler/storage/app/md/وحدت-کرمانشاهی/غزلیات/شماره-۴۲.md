---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>شکست گر دلت از کس مرنج ای عاقل</p></div>
<div class="m2"><p>از آنکه خانه حق می‌شود شکست چو دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همیشه لازمه زندگی‌ست کوشش و کار</p></div>
<div class="m2"><p>به دهر بهره ز هستی نمی‌برد کاهل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کمال و فضل ز علم است و معرفت ای دوست</p></div>
<div class="m2"><p>کسی کمال و فضیلت نخواهد از جاهل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درخت خشک ز سعی و عمل ثمر ندهد</p></div>
<div class="m2"><p>مکن تلاش و حذر کن ز سعی بی‌حاصل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شود همیشه چو مرآت مقتبس از شمس</p></div>
<div class="m2"><p>اگر غبار کدورت ز دل شود زایل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر به قدرت خود پی برد دمی انسان</p></div>
<div class="m2"><p>زند چو بحر خروشنده موج بر ساحل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شوی مقرب درگاه کبریا وحدت</p></div>
<div class="m2"><p>اگر که پاک شود لوحه دلت از غل</p></div></div>