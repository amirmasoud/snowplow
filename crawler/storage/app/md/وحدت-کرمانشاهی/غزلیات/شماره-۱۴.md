---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>ز دست عقل به رنجم بیار جام شراب</p></div>
<div class="m2"><p>بنای عقل مگر گردد از شراب خراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برو به کوی خرابات و می‌پرستی کن</p></div>
<div class="m2"><p>که این کلید نجات است و آن طریق صواب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لطیفه‌های نهانی رسد به گوش دلم</p></div>
<div class="m2"><p>ز صوت بربط و آهنگ چنگ و بانگ رباب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به یک تجلی حسن ازل ز بحر وجود</p></div>
<div class="m2"><p>شد آشکار هزاران هزار شکل حباب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهان و هرچه در او هست پیش اهل نظر</p></div>
<div class="m2"><p>نظیر خواب و خیال است عکس ظل تراب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عجب مدار که شب تا به صبح بیدارم</p></div>
<div class="m2"><p>عجب بود که در آید به چشم عاشق خواب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قرار و صبر ز عاشق مجو که نتواند</p></div>
<div class="m2"><p>به حکم عقل محال است جمع آتش و آب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیا و این من و ما را تو از میان بردار</p></div>
<div class="m2"><p>که غیر این من و ما نیست در میانه حجاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نبوده بی می و معشوق سال‌ها وحدت</p></div>
<div class="m2"><p>به دور لاله و گل روزگار عهد شباب</p></div></div>