---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>تا زنگ سیه ز آینه دل نزداید</p></div>
<div class="m2"><p>عکس رخ دلدار در او خوش ننماید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در طرف چمن گر نکند جلوه رخ دوست</p></div>
<div class="m2"><p>بر برگ گلی این همه بلبل نسراید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نور ازلی گر ندمد از رخ لیلی</p></div>
<div class="m2"><p>از گردش چشمی دل مجنون نرباید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کو نکند بندگی پیر خرابات</p></div>
<div class="m2"><p>بر روی دلش جان در معنی نگشاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای غمزده تریاق محبت به کف آور</p></div>
<div class="m2"><p>تا زهر غم دهر تو را جان نگزاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آیین طریقت به حقیقت به جز این نیست</p></div>
<div class="m2"><p>کز شادی و غم راحت و رنجت نفزاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این بار امانت که شده قسمت وحدت</p></div>
<div class="m2"><p>بر پشت فلک گر نهد البته خم آید</p></div></div>