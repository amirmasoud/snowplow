---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>شد بر فراز مسند دل باز شاه عشق</p></div>
<div class="m2"><p>یعنی گرفت کشور جان را سپاه عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز در فضای سینه رندان می‌پرست</p></div>
<div class="m2"><p>نتوان زدن به ملک جهان بارگاه عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شوریدگان عشق برابر نمی‌کنند</p></div>
<div class="m2"><p>با صد هزار افسر شاهی کلاه عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در ملک فقر افسر فخرش به سر نهند</p></div>
<div class="m2"><p>هر تن که خاک شد ز دل و جان به راه عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای شیخ روی زرد و لب خشک و چشم تر</p></div>
<div class="m2"><p>در شرع ما به حقیقت بود گواه عشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خودخواهی از خیال برون کن که در جهان</p></div>
<div class="m2"><p>از خود گذشتگی‌ست همی رسم و راه عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرگز نیابد ایمنی از حادثات دهر</p></div>
<div class="m2"><p>وحدت، مگر دمی که بود در پناه عشق</p></div></div>