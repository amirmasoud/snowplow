---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>کردیم عاقبت وطن اندر دیار عشق</p></div>
<div class="m2"><p>خوردیم آب بیخودی از جویبار عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مستان عشق را به صبوحی چه حاجت است</p></div>
<div class="m2"><p>زیرا که درد سر نرساند خمار عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سی سال لاف مهر زدم تا سحرگهی</p></div>
<div class="m2"><p>وا شد دلم چو گل ز نسیم بهار عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فارغ شود ز دردسر عقل فلسفی</p></div>
<div class="m2"><p>یک جرعه گر کشد ز می خوشگوار عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دامن مراد نبینی گل مراد</p></div>
<div class="m2"><p>بی ترک خواب راحت و بی نیش خار عشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای فرخ آن سری که زنندش به تیغ یار</p></div>
<div class="m2"><p>وی خرم آن تنی که کشندش به دار عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روزی ندیده تا به کنون چشم روزگار</p></div>
<div class="m2"><p>از دور روزگار به از روزگار عشق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پروانه گر ز عشق بسوزد عجب مدار</p></div>
<div class="m2"><p>کآتش زند به خرمن هستی شرار عشق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن دم مس وجود تو زر می‌شود که تن</p></div>
<div class="m2"><p>در بوته فراق گدازد به نار عشق</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرکس که یافت آگهی از سر عاشقی</p></div>
<div class="m2"><p>وحدت صفت کند سرو جان را نثار عشق</p></div></div>