---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>بگوی زاهد خودبین بادپیما را</p></div>
<div class="m2"><p>که درد باده رهانید از خودی ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی که پا و سری یافت در دیار فنا</p></div>
<div class="m2"><p>گزید خدمت رندان بی سر و پا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگرچه نقطه ز با یافت رتبه امکان</p></div>
<div class="m2"><p>ولی به نقطه شناسند عارفان با را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مکن ملامتم از عاشقی که نتوان بست</p></div>
<div class="m2"><p>ز دیدن رخ خورشید چشم حربا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز کوی دوست مگر می‌رسد نسیم صبا</p></div>
<div class="m2"><p>که پر ز نافه چین کرده کوه و صحرا را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کمینه چاکری از بندگان پیر مغان</p></div>
<div class="m2"><p>به یک اشاره کند زنده صد مسیحا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روا مدار که هر دم به یاد روی گلی</p></div>
<div class="m2"><p>چو غنچه چاک زنم جامه شکیبا را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به صد فسانه و افسون نمی‌کند بیرون</p></div>
<div class="m2"><p>رقیب از سر مجنون هوای لیلا را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیاله گیر که رندان به نیم جو نخرند</p></div>
<div class="m2"><p>هزار ساله طاعات زهد و تقوا را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برو ز دست مده گر وصال می‌طلبی</p></div>
<div class="m2"><p>فغان و ناله و فریاد و آه شب‌ها را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کسی به کنه کلام تو پی برد وحدت</p></div>
<div class="m2"><p>که یافت در صدف لفظ در معنا را</p></div></div>