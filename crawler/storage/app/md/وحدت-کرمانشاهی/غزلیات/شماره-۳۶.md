---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>هرکه آئین حقیقت نشناسد ز مجاز</p></div>
<div class="m2"><p>در سراپرده رندان نشود محرم راز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا که بیهوده مران نام محبت به زبان</p></div>
<div class="m2"><p>یا چو پروانه بسوز از غم و با درد بساز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگذارید قدم بیهده در وادی عشق</p></div>
<div class="m2"><p>کاندر این مرحله بسیار نشیب است و فراز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنقدر حلقه زنم بر در میخانه عشق</p></div>
<div class="m2"><p>تا کند صاحب میخانه به رویم در باز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دم غنیمت بود ای دوست در این دم زیرا</p></div>
<div class="m2"><p>آنچه از عمر ز کف رفت دگر ناید باز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکه شد معتکف اندر حرم کعبه دل</p></div>
<div class="m2"><p>حاش لله که شود معتکف کوی مجاز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهتر از جنت و حور است همانا وحدت</p></div>
<div class="m2"><p>وصل دلدار و لب جوی و می و نغمه ساز</p></div></div>