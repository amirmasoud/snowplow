---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>به من فرمود پیر راه بینی</p></div>
<div class="m2"><p>مسیح آسا دمی خلوت گزینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که از جهل چهل سالت رهاند</p></div>
<div class="m2"><p>اگر با دل نشینی اربعینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نباشد ای پسر صاحبدلان را</p></div>
<div class="m2"><p>به جز دل در دل شب‌ها قرینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شبان وادی دل صد هزارش</p></div>
<div class="m2"><p>ید بیضا بود در آستینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سلیمان حشمتان ملک عرفان</p></div>
<div class="m2"><p>کجا باشند محتاج نگینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنازم ملک درویشی که آنجا</p></div>
<div class="m2"><p>بود قارون گدای خوشه‌چینی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگو این کافر است و آن مسلمان</p></div>
<div class="m2"><p>که در وحدت نباشد کفر و دینی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عجب نبود اگر با دشمن و دوست</p></div>
<div class="m2"><p>نباشد عاشقان را مهر و کینی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خدا را سر حکمت را مگویید</p></div>
<div class="m2"><p>مگر با چون فلاطون خم نشینی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نروید لاله از هر کوهساری</p></div>
<div class="m2"><p>نخیزد سبزه از هر سرزمینی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برو وحدت اگر ز اهل نیازی</p></div>
<div class="m2"><p>بکش پیوسته ناز نازنینی</p></div></div>