---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>لبریز تا ز باده نگردید جام ما</p></div>
<div class="m2"><p>در نامه عمل ننوشتند نام ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما خود خراب و مست شرابیم و محتسب</p></div>
<div class="m2"><p>نبود خبر ز مستی شرب مدام ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دارم هوای آنکه ز بامش پرم ولیک</p></div>
<div class="m2"><p>بنموده چین زلف کجش پای دام ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون گشته‌ایم حلقه به گوش جناب عشق</p></div>
<div class="m2"><p>زیبد که ماه چارده گردد غلام ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با اینچنین تحقق آمال و وصل یار</p></div>
<div class="m2"><p>بنشسته است مرغ سعادت به بام ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای مدعی اگر بگشایی تو چشم دل</p></div>
<div class="m2"><p>بینی شکوه عزت و جاه و مقام ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این نکته روشن است که در دور روزگار</p></div>
<div class="m2"><p>باشد صفا و صدق و محبت مرام ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وحدت بنوش باده وحدت ز دست دوست</p></div>
<div class="m2"><p>بهتر از این به دهر نباشد گمان ما</p></div></div>