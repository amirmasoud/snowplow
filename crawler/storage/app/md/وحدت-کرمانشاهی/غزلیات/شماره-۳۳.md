---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>می خور که هر که می نخورد فصل نوبهار</p></div>
<div class="m2"><p>پیوسته خون دل خورد از دست روزگار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می در بهار صیقل دل‌های آگه است</p></div>
<div class="m2"><p>از دست یار خاصه به آهنگ چنگ و تار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در عهد گل ز دست مده جام باده را</p></div>
<div class="m2"><p>کاین باشد از حقیقت جمشید یادگار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صحن چمن چو وادی ایمن شد ای عزیز</p></div>
<div class="m2"><p>گل برفروخت آتش موسی ز شاخسار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آموختند مستی و دیوانگی مرا</p></div>
<div class="m2"><p>دیوانگان عاقل و مستان هوشیار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از بندگی به مرتبه خواجگی رسید</p></div>
<div class="m2"><p>هرکس که کرد بندگی دوست بنده‌وار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وحدت بیا و بر در توفیق حلقه زن</p></div>
<div class="m2"><p>توفیق چون رفیق شود گشت بخت یار</p></div></div>