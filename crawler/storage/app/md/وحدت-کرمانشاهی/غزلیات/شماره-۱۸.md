---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>بر آنکه مرید می و معشوقه و جام است</p></div>
<div class="m2"><p>جز دوست نعیم دو جهان جمله حرام است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترک سر و جان گیر پس آنگاه بیاسای</p></div>
<div class="m2"><p>آری سفر عشق همین یک دو سه گام است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از اول این بادیه تا کعبه مقصود</p></div>
<div class="m2"><p>دیدیم و گذشتیم از او چار مقام است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون طالب و مطلوب و طلب هر سه یکی شد</p></div>
<div class="m2"><p>هنگام وصال است دگر سیر تمام است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر خواجه که در بندگی عشق کمر بست</p></div>
<div class="m2"><p>کی دفع کند ننگ و کجا طالب نام است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>معلوم شود عاقبت از رنج ره عشق</p></div>
<div class="m2"><p>کاین همسفران پخته کدام است و که خام است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هشدار که زاهد نزند راه تو ای دوست</p></div>
<div class="m2"><p>تحت الحنک و سبحه او دانه و دام است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وحدت عجبی نیست که در بحر محبت</p></div>
<div class="m2"><p>گر بنده شود خواجه اگر شاه غلام است</p></div></div>