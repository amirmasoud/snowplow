---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>خواجه آن روز که از بندگی آزادم کرد</p></div>
<div class="m2"><p>ساغر می به کفم داد و ز غم شادم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خبر از نیک و بد عاشقیم هیچ نبود</p></div>
<div class="m2"><p>چشم مست تو در این مرحله استادم کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی شیرین‌صفتان در نظر آراست مرا</p></div>
<div class="m2"><p>ریخت طرح هوس اندر سر و فرهادم کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاقبت بیخ و بن هستی ما کرد خراب</p></div>
<div class="m2"><p>از کرم، خانه‌اش آباد که آبادم کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفت بر باد فنا گرد وجودم آخر</p></div>
<div class="m2"><p>دیدی ای دوست که سودای تو بر بادم کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس که فرهاد صفت ناله و فریاد زدم</p></div>
<div class="m2"><p>بیستون ناله و فریاد ز فریادم کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بودم از زمره رندان خرابات ولیک</p></div>
<div class="m2"><p>قسمت از روز ازل همدم زهادم کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وحدت آن ترک کماندار جفاجو آخر</p></div>
<div class="m2"><p>دیده و دل هدف ناوک بیدادم کرد</p></div></div>