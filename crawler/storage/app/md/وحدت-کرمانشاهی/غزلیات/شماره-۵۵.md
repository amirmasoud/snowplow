---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>ز نام بهره نبردیم غیر بدنامی</p></div>
<div class="m2"><p>ز کام صرفه نبردیم غیر ناکامی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکست شیشه تقوی ز سنگ رسوایی</p></div>
<div class="m2"><p>گسست سجه طاعت به دست بدنامی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیار باده که این آتش سلامت‌سوز</p></div>
<div class="m2"><p>برون کند ز تن مرد علت خامی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مپرس جز ز خراباتیان بی سر و پا</p></div>
<div class="m2"><p>رموز عاشقی و مستی و می آشامی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زبان عشق زبانی‌ست که اهل دل دانند</p></div>
<div class="m2"><p>نه تازی است و نه هندی نه فارس نه شامی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز دست عشق روان گیر جام جمشیدی</p></div>
<div class="m2"><p>به پای عقل درافکن کمند بهرامی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گل اناالحق و سبحانی ای عزیز هنوز</p></div>
<div class="m2"><p>دمد ز تربت منصور و شیخ بسطامی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به قصد قتل دلم ترک چشم مخمورش</p></div>
<div class="m2"><p>نمود تکیه بر آن ابروان صمصامی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بپوش چشم دل از غیر دوست وحدت‌وار</p></div>
<div class="m2"><p>به گوش هوش شنو نکته‌های الهامی</p></div></div>