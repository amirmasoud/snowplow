---
title: >-
    شمارهٔ  ۶ - درستایش مشرقی و عذر‌خواهی از او
---
# شمارهٔ  ۶ - درستایش مشرقی و عذر‌خواهی از او

<div class="b" id="bn1"><div class="m1"><p>زهی ز مشرق طبع تو آفتاب خجل</p></div>
<div class="m2"><p>که هر چه زاده این مشرق‌ست بهتر ازوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تواضعی‌ست ز تو مشرقی وگرنه سپهر</p></div>
<div class="m2"><p>فسرده غنچه پر گرد و خاک این مینوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو لب به شعرگشایی ز فیض شادابی</p></div>
<div class="m2"><p>سخن بر آن لب مانند سبزه بر لب جوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز نو‌بهار ضمیر تو گوش را چون گل</p></div>
<div class="m2"><p>هزار گونه طراوت برون ز رنگ و ز بوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به تخم کاری طبع و به آبیاری فکر</p></div>
<div class="m2"><p>چه احتیاج مرا وصف تو گل خودروست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سواد خط دهد از معنی تو نکهت مشک</p></div>
<div class="m2"><p>مگر معانی رنگین تو گل شب‌بوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیال زلف بتان هست در دل همه کس</p></div>
<div class="m2"><p>چرا همین سخن دلکش تو عنبربوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وفا شکوها صافی دلا ملک طبعا</p></div>
<div class="m2"><p>غبار خاطرت از من بگوی تا ز چه روست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر فسرده‌زبانی ز غمز حرفی گفت</p></div>
<div class="m2"><p>تو خود ندانی کان آب خانه‌زاد سبوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زبان بنده و بد گفتن آسمان داند</p></div>
<div class="m2"><p>که در قبیله نطقم هر آنچه هست نکوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به راستی سخنم در زمانه مشهورست</p></div>
<div class="m2"><p>وگر کجی‌ست در آن تاب زلف و پیچش موست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در این حدیث دل پاک تو گواه منست</p></div>
<div class="m2"><p>زهی به پاکدلی شهره نزد دشمن و دوست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فرشته خویا لعن خدا بر آن شیطان</p></div>
<div class="m2"><p>که سرگردانی خوی تو از غوایت اوست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو نقش کینه نفس در دلش گره بادا</p></div>
<div class="m2"><p>که مغز تلخ همان به که پوسد اندرپوست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر احمقی سخنی گفت منحرف چه شوی</p></div>
<div class="m2"><p>شتاب بر اثر بانگ غول نانیکوست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نوای مجلس روحانیان چه می‌داند</p></div>
<div class="m2"><p>شکم‌پرستی که همچو نای جمله گلوست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گرفتم آن که زبانم نوای عصیان زد</p></div>
<div class="m2"><p>کریم را نه که عفو گناه عادت و خوست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دو بیت کلکم ازین پیش کرده بود انشا</p></div>
<div class="m2"><p>برای حال من امروز آن دوبیت نکوست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز دوستان به گناهی نمی‌توان رنجید</p></div>
<div class="m2"><p>کجی ز دوست پسندیده چون خم ابروست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دورنگی گل رعنا گناه گلشن نیست</p></div>
<div class="m2"><p>گناه رنگ رزیهای آسمان دوروست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ولی به عذر تسلی نمی‌شود دل تو</p></div>
<div class="m2"><p>بهل که خشک شوم همچو مشک اندرپوست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به خاکم ار گذری بعد از آن کنی معلوم</p></div>
<div class="m2"><p>که از شمیم محبت‌پرست مرقد دوست</p></div></div>