---
title: >-
    شمارهٔ  ۱۷ - هجویه
---
# شمارهٔ  ۱۷ - هجویه

<div class="b" id="bn1"><div class="m1"><p>خواجه را از زبان بنده بگو</p></div>
<div class="m2"><p>زر امید خویش مس نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خویش را آدمی بنشمارد</p></div>
<div class="m2"><p>هرزه انکار عقل و حس نکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون ناموس خود نگه دارد</p></div>
<div class="m2"><p>تیغ هجو مرا نجس نکند</p></div></div>