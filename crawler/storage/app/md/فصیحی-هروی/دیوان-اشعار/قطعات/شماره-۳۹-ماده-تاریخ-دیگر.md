---
title: >-
    شمارهٔ  ۳۹ - ماده تاریخ دیگر
---
# شمارهٔ  ۳۹ - ماده تاریخ دیگر

<div class="b" id="bn1"><div class="m1"><p>ای نسخه کارگاه مانی</p></div>
<div class="m2"><p>فهرست کتاب کامرانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون کعبه سرآمد زمینی</p></div>
<div class="m2"><p>چون مهر چراغ آسمانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نی شاهدی و چو شاهد شوخ</p></div>
<div class="m2"><p>هر لحظه کرشمه می‌فشانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر پیکر تو ز بس لطافت</p></div>
<div class="m2"><p>ترسم که کند صفا گرانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرزند عزیز آفتابی</p></div>
<div class="m2"><p>او پیر شد و تو نوجوانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌زیبد اگر کنی به خورشید</p></div>
<div class="m2"><p>در کفه روشنی گرانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در ساحت قدرت آسمان را</p></div>
<div class="m2"><p>آورد زمین به میهمانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون دید شکوه حضرت تو</p></div>
<div class="m2"><p>بوسید زمین به پاسبانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در جوف تو خضر کرده پنهان</p></div>
<div class="m2"><p>سرچشمه آب زندگانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا ساحت مسند خراسان</p></div>
<div class="m2"><p>یابد ز تو عمر جاودانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نی ز آب و گلی که روح پاکی</p></div>
<div class="m2"><p>در پیکر خاک تیره‌جانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رضوان گویی به ما فرستاد</p></div>
<div class="m2"><p>قصری ز بهشت ارمغانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نی‌نی که بهشتی و از آن شد</p></div>
<div class="m2"><p>تاریخ بهشت کامرانی</p></div></div>