---
title: >-
    شمارهٔ  ۱۴ - اخوانیه
---
# شمارهٔ  ۱۴ - اخوانیه

<div class="b" id="bn1"><div class="m1"><p>رسید مژده که خورشید آسمان جلال</p></div>
<div class="m2"><p>بر آن سرست کزین ذره آسمان سازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز کلبه‌ای که همی توامان زندان‌ست</p></div>
<div class="m2"><p>سحاب مکرمتش باغ و بوستان سازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خاک غمکده‌ای آب لطف آمیزد</p></div>
<div class="m2"><p>مفرح طربی بهر جسم و جان سازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لب مرا که بجز زهر غم کسی ننواخت</p></div>
<div class="m2"><p>ز نوش بوسه اقبال کامران سازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو بخت از درم آید درون و دایره‌وار</p></div>
<div class="m2"><p>سر نیاز مرا وقف آستان سازد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار چشمه شاداب‌تر ز بحر سخا</p></div>
<div class="m2"><p>به ذره ذره این خاکدان نهان سازد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزار اختر فرخنده‌تر ز کوکب جود</p></div>
<div class="m2"><p>هم از مطالع این سرزمین عیان سازد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا غرابت این مژده چون تب سودا</p></div>
<div class="m2"><p>نفس نفس به صد اندیشه سرگردان سازد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درین خرابه که جغد اندرو مقام نکرد</p></div>
<div class="m2"><p>همای قله دولت کی آشیان سازد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرفتم آن که سلیمان نواخت موری را</p></div>
<div class="m2"><p>فضای دیده او را چه‌سان مکان سازد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نفس فسرده زدم دولتش اگر خواهد</p></div>
<div class="m2"><p>ز جرم دیده موری نه آسمان سازد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز بس درستی عزمش کف مهندس او</p></div>
<div class="m2"><p>ز نیم قطره دو صد بحر رایگان سازد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه صنعت است ندانم که ابر لطفش راست</p></div>
<div class="m2"><p>که شعله را گل سیراب گلستان سازد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همیشه تا که فسون نیاز زخم مرا</p></div>
<div class="m2"><p>ز فیض مرهم الماس کامران سازد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زمانه لطف محبان پناه صاحب را</p></div>
<div class="m2"><p>بدین گروه عدم ریزه مهربان سازد</p></div></div>