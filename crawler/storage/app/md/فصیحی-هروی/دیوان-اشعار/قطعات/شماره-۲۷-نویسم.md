---
title: >-
    شمارهٔ  ۲۷ - نویسم
---
# شمارهٔ  ۲۷ - نویسم

<div class="b" id="bn1"><div class="m1"><p>گفتم که بیاض دوستان را</p></div>
<div class="m2"><p>طغرای سواد جان نویسم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مدحت هر رقم که بینم</p></div>
<div class="m2"><p>ز اعجاز جهان جهان نویسم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاکی که بر آن رقم فشانم</p></div>
<div class="m2"><p>جانداروی آسمان نویسم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشکین رقمی که خود نگارم</p></div>
<div class="m2"><p>نامش به یک آستان نویسم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عقل آمد و گفت فرصتت باد</p></div>
<div class="m2"><p>من نیز چنین چنان نویسم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر صفحه آفتاب اول</p></div>
<div class="m2"><p>حرفی دو به امتحان نویسم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پس بر وزقش که روی حور است</p></div>
<div class="m2"><p>خط چون خط دلستان نویسم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از حضرت عشق شرمم آید</p></div>
<div class="m2"><p>ورنه خوشتر از آن نویسم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ابیات وفای عشق را فاش</p></div>
<div class="m2"><p>بر ناصیه فغان نویسم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آیات ثنای عصمت حسن</p></div>
<div class="m2"><p>هم از قلمم نهان نویسم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تعویذ نظر نظارگی را</p></div>
<div class="m2"><p>بر خاتمه حرز جان نویسم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نی‌نی که ز گفته فصیحی</p></div>
<div class="m2"><p>بیتی دو سه حرز جان نویسم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن به که چو سرنوشت عشق است</p></div>
<div class="m2"><p>بر جبهه قدسیان نویسم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ور جایزه همتش پذیرد</p></div>
<div class="m2"><p>بر دیده خون‌فشان نویسم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ور نپذیرد پی نثارش</p></div>
<div class="m2"><p>گل بر باغ جنان نویسم</p></div></div>