---
title: >-
    شمارهٔ  ۷ - توصیف غار جمشیدی
---
# شمارهٔ  ۷ - توصیف غار جمشیدی

<div class="b" id="bn1"><div class="m1"><p>تعالی الله نه غارست این جهانیست</p></div>
<div class="m2"><p>زمین او ز رفعت آسمانیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقاب آسمان را گر تواند</p></div>
<div class="m2"><p>که اینجا پر زند خوش آشیانیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هر برجی در آن از روح قدسی</p></div>
<div class="m2"><p>خجسته کوکبی صاحب قرانیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خدام در این آستانست</p></div>
<div class="m2"><p>اگر آن آسمان را پاسبانیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز انوار تجلی در فضایش</p></div>
<div class="m2"><p>ببینی هر کجا راز نهانیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو گویی گوهر خورشید و مه را</p></div>
<div class="m2"><p>به هر سنگ اندر او فرخنده کانیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>معاذالله تو و مدحش فصیحی</p></div>
<div class="m2"><p>گرفتم آن که هر مویت زبانیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به جمعی اندر آن مجلس گرفتم</p></div>
<div class="m2"><p>که از اخلاص هر موشان جهانیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه صحرانشین و شهرزاد‌ند</p></div>
<div class="m2"><p>وفا را طبع ایشان ترجمانیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لقب جمشیدی و جمشید فطرت</p></div>
<div class="m2"><p>بنامیزد چه فرخ دودمانیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو دیگ قدرشان در جوش آید</p></div>
<div class="m2"><p>فلک آنجا کلوخ دیگدانیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خداشان دایما فرخنده دار[ا]د</p></div>
<div class="m2"><p>ازین فرخنده منزل تا نشانیست</p></div></div>