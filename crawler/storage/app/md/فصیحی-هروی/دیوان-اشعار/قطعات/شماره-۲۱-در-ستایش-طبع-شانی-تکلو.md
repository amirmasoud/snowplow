---
title: >-
    شمارهٔ  ۲۱ - در ستایش طبع شانی تکلو
---
# شمارهٔ  ۲۱ - در ستایش طبع شانی تکلو

<div class="b" id="bn1"><div class="m1"><p>صبا به کوی دل آشفتگان عشق گذر</p></div>
<div class="m2"><p>زمین ببوس اگر آسمان دهد دستور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگو به مردمک دیده هنر شانی</p></div>
<div class="m2"><p>که ای ضمیر تو چون چشم عقل چشمه نور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو آن مسیح مقالی که ملک معنی راست</p></div>
<div class="m2"><p>بیاض جبهه کلک تو صبحگاه نشور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صریر خامه‌ات ار نفخ صور نیست چرا</p></div>
<div class="m2"><p>کند جهان معانی ز نقطه‌ای محشور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کدام قطره ترا ریخت از سحاب قلم</p></div>
<div class="m2"><p>که روی صفحه نشد پر ترانه منصور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ولی چو این گهر از بحر عقل گشته پدید</p></div>
<div class="m2"><p>نمی‌کند صدف گوش جهل را معمور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو صبح کلک تو از آفتاب حامله است</p></div>
<div class="m2"><p>اگرچه نطفه ستانید از شب دیجور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که هر نقط که از آن خال روی صفحه شود</p></div>
<div class="m2"><p>زمانه را کند از روشنی چو عارض حور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو نای خامه معجزفشان بگیری تنگ</p></div>
<div class="m2"><p>که سر غیب بماند ز ناکسان مستور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رسد ز هر سر انگشت تو به گوش خرد</p></div>
<div class="m2"><p>همان نوا که ز داود در ادای زبور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به باغ طبع تو لفظی که جلوه گر گردد</p></div>
<div class="m2"><p>ز رنگ و بوی شود تو به تو گل معمور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رسید درج دری کامغان فرستادی</p></div>
<div class="m2"><p>زهی محیط کز آن آمد این درر به ظهور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه ز قدر سزای ثنای قلزم و کان</p></div>
<div class="m2"><p>همه ز لطف سزاوار گوش و گردن حور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز جلوه‌های معانی در آن خرد مدهوش</p></div>
<div class="m2"><p>چنان که واله ایمن گه تجلی طور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نظر به جودت هر لفظ او فروماند</p></div>
<div class="m2"><p>چو آن مگس که کند بر عسل هوای مرور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز بس فروغ معانی به روی الفاظش</p></div>
<div class="m2"><p>پی نگاه توان دید در شب دیجور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>محیط طبع تو زین‌سان گر از تموج فیض</p></div>
<div class="m2"><p>در سماع بر‌آرد ز لولو منثور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خرد به درک یکی لفظ برنیاید اگر</p></div>
<div class="m2"><p>هزار گوش ستاند ز قدسیان مزدور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>حسود سحر نسب معجز ترا دیدم</p></div>
<div class="m2"><p>ز ته پیاله بوجهل گشته مست غرور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شفای عالم جهل و وبای کشور عقل</p></div>
<div class="m2"><p>بود درین دو جهان راست بر مثال دو صور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به مشرب جهلا صافتر ز آب زلال</p></div>
<div class="m2"><p>به مذهب عقلا تیره‌تر ز لای قصور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به گوش نغمه مطرب هزار پای شود</p></div>
<div class="m2"><p>ز روی مرتبه گردد اگر خرد طنبور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بود چو دخمه کفار جنگ اشعارش</p></div>
<div class="m2"><p>چو مردگانش معانی و لفظها چو قبور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نگشته آیت رحمت در آن جهان نازل</p></div>
<div class="m2"><p>نکرده فیض ازل اندر آن دیار عبور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ولی نظام ترا از خصومتش چه ضرر</p></div>
<div class="m2"><p>اساس طبع ترا از عداوتش چه فتور</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که غیر روسیهی هیچ صرفه‌ای نبرد</p></div>
<div class="m2"><p>بر آفتاب شود تیره گر شب دیجور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خرد پناها ای لال در ثنات خرد</p></div>
<div class="m2"><p>زهی لالی تو گنج فیض را گنجور</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رسید مژده که چون ابر رحمت ازلی</p></div>
<div class="m2"><p>بر آن سری که کنی سوی این بهشت عبور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بلی ز غایت زهد تو هنم درین دنیا</p></div>
<div class="m2"><p>اگر بهشت نصیبت شود نباشد دور</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ولی ز بخت بدخار و خس بسی دورست</p></div>
<div class="m2"><p>که نور محض کندشان وصال آتش طور</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>وگر ز مغرب بخت سیاهشان خورشید</p></div>
<div class="m2"><p>کند طلوع و شوند از وصال تو مسرور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ثنا طراز شود مو به مو فصیحی را</p></div>
<div class="m2"><p>به دولت تو شوم خواجه عباد شکور</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همیشه تا بود از نور و ظلمت شب و روز</p></div>
<div class="m2"><p>گهی منور و گاهی کدر سنین و شهور</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شکفته طبع تو بادا چو نور در ظلمت</p></div>
<div class="m2"><p>نژند خصم تو بادا چو ظلمت اندر نور</p></div></div>