---
title: >-
    شمارهٔ  ۲۳ - ماده تاریخ
---
# شمارهٔ  ۲۳ - ماده تاریخ

<div class="b" id="bn1"><div class="m1"><p>مرحبا ای مهین نشیمن قدس</p></div>
<div class="m2"><p>کز تو این خلد گشت منزل عیش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاریت نوبهار فراش‌ست</p></div>
<div class="m2"><p>که ز تو پر گل‌ست محفل عیش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رنگ گلهای غصه بشکستی</p></div>
<div class="m2"><p>بس که آراستی شمایل عیش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حیرتم سوخت کز چه آب و گلی</p></div>
<div class="m2"><p>کاین قدر نشئه نیست با گل عیش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک جهان نو‌بهار اگر آید</p></div>
<div class="m2"><p>نشکفد بی تو غنچه دل عیش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در تاریخ تو گشوده شود</p></div>
<div class="m2"><p>چون مکرر شود منازل عیش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رقم نام بانی تو کنند</p></div>
<div class="m2"><p>سر ابیات از انامل عیش</p></div></div>