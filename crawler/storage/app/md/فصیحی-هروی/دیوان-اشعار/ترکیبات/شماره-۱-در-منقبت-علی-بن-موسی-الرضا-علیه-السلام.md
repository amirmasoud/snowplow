---
title: >-
    شمارهٔ  ۱ - در منقبت علی‌بن موسی‌الرضا علیه‌السلام
---
# شمارهٔ  ۱ - در منقبت علی‌بن موسی‌الرضا علیه‌السلام

<div class="b" id="bn1"><div class="m1"><p>هر چند که من شعله افسرده عیارم</p></div>
<div class="m2"><p>در خرمن خود سوخته از باد بهارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌خنده گل بس که تر‌شروی نشستم</p></div>
<div class="m2"><p>صفرای چمن بشکند از ناله زارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر لخت تنم مرثیه بخت جوانیست</p></div>
<div class="m2"><p>گویی که در‌ین دخمه ستان لوح مزارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در پرده دل آتش اندوه کنم صاف</p></div>
<div class="m2"><p>و آنگه کنمش اشک و به مژگان بسپارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان پیش که بارم گهر از گلبن مژگان</p></div>
<div class="m2"><p>چینند گل حوصله دامان و کنارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رنجیده ز هم چشمی داغم گل خورشید</p></div>
<div class="m2"><p>کاندود به صد قیر رخ رونق کارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از داغ جگر شانه‌کش طره آهم</p></div>
<div class="m2"><p>وز بخت سیه پردگی زلف نگارم</p></div></div>
<div class="b2" id="bn8"><p>بی‌پرده اگر جلوه کند بخت سیاهم</p>
<p>بر مردمک از رشک کشد تیغ نگاهم</p></div>
<div class="b" id="bn9"><div class="m1"><p>من کیستم آن بادیه‌پیمای فنایم</p></div>
<div class="m2"><p>کاواره کند قافله را بانگ درایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون باغ غم افسرده شود دیده ابرم</p></div>
<div class="m2"><p>چون داغ جگر غنچه شود باد صبایم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در پیکر ماتم نفس بازپسینم</p></div>
<div class="m2"><p>میرند شهیدان اگر از پای درآیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غم پاشدم از خنده چو در خنده نشینم</p></div>
<div class="m2"><p>خون جوشدم از ناله چو در ناله درآیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طوفانست گره در دل هر قطره اشکم</p></div>
<div class="m2"><p>آه ار گرهی از دل اشکی بگشایم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از روزنه دیده موری ز ضعیفی</p></div>
<div class="m2"><p>آهسته‌تر از پرتو خورشید درآیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کونین دو نقش‌ست که چون در نگشودند</p></div>
<div class="m2"><p>توفیق کلیدانه ستد از کف پایم</p></div></div>
<div class="b2" id="bn16"><p>ما خانه خرابان سر زلف نگاریم</p>
<p>کاری به خرابات و مناجات نداریم</p></div>
<div class="b" id="bn17"><div class="m1"><p>گوش هوسم، پند ز گفتار نگیرم</p></div>
<div class="m2"><p>تعلیم علاج از لب بیمار نگیرم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آورده‌ام از مصر وفا نکهت یوسف</p></div>
<div class="m2"><p>قیمت بجز از ناز خریدار نگیرم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نه بلبل صبحم مزه ناله شناسم</p></div>
<div class="m2"><p>برگ گل خورشید به منقار نگیرم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مضرابم و این طرفه که جوشد ز رگم خون</p></div>
<div class="m2"><p>یک لحظه اگر خون ز رگ تار نگیرم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آیینه نازم ز تب یاس گدازم</p></div>
<div class="m2"><p>یکدم اگر از آهی ژنگار نگیرم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تمثال نیازم گل بختم نشود وا</p></div>
<div class="m2"><p>تا بوس نشاطی ز لب خار نگیرم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نظاره به دل دزدم و خونابه فروشم</p></div>
<div class="m2"><p>آنجا که بها جلوه دیدار نگیرم</p></div></div>
<div class="b2" id="bn24"><p>ور جلوه دیدار شبی بزم فروزد</p>
<p>شمعی به میان آید و نظاره بسوزد</p></div>
<div class="b" id="bn25"><div class="m1"><p>کو حوصله تا بند نهم بر نظر خویش</p></div>
<div class="m2"><p>وین شعله کنم صرفه برای جگر خویش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در راه وفا قافله اشک نیازم</p></div>
<div class="m2"><p>هم پی سپر خویشم و هم راهبر خویش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در بزم وفا سلسله طره یارم</p></div>
<div class="m2"><p>هم پرده خویشم من و هم پرده در خویش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هر شعله جانسوز که از بال فشانم</p></div>
<div class="m2"><p>سازند ملایک همه را حرز پر خویش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زنجیر نهم بر سر و بر پای نهم داغ</p></div>
<div class="m2"><p>از هم نشناسم ز جنون پا و سر خویش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در دیده طوفان بلا سوخته اشکم</p></div>
<div class="m2"><p>بر چهره امید که جویم اثر خویش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در سینه گرداب جنون گم شده موجم</p></div>
<div class="m2"><p>از ساحل دریای که پرسم خبر خویش</p></div></div>
<div class="b2" id="bn32"><p>ما گم شدگانیم و جنون پی سپر ماست</p>
<p>گمراهی جاوید حریف سفر ماست</p></div>
<div class="b" id="bn33"><div class="m1"><p>افسوس که رنگ گلم از باد خزان ریخت</p></div>
<div class="m2"><p>خاکستر شبنم به سر بخت جوان ریخت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شب تا به سحر بر رمه‌ام گرگ کمین داشت</p></div>
<div class="m2"><p>تیغ کرمی تند شد و خون شبان ریخت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>این ابر بهاری ز کجا بود که از لطف</p></div>
<div class="m2"><p>بر آتشم آب از دم شمشیر و سنان ریخت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گل بود ز دل تا لبم از موجه خوناب</p></div>
<div class="m2"><p>پای نفس از جای شد و ساغر جان ریخت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در سینه شکاف افکنم و سیل بریزم</p></div>
<div class="m2"><p>پیداست که چند از مژه‌ای اشک توان ریخت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مشتی نفس سرد دلم داشت ذخیره</p></div>
<div class="m2"><p>شب سینه به تنگ آمد و در پای فغان ریخت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از روشنی دیده غبار در شه را</p></div>
<div class="m2"><p>نشناخته از دیده خونابه فشان ریخت</p></div></div>
<div class="b2" id="bn40"><p>شاهی که از‌و یافته اورنگ شهی زین</p>
<p>سلطان علی موسی جعفر شه کونین</p></div>
<div class="b" id="bn41"><div class="m1"><p>ای ناطقه این جلوه افضال مبارک</p></div>
<div class="m2"><p>وی بحر هنر موجه آمال مبارک</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>این نادره پرواز که بی‌جنبش بالست</p></div>
<div class="m2"><p>مرغان ترا بر حرم این بال مبارک</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>آیات کمال دو جهان منقبت اوست</p></div>
<div class="m2"><p>بر نام تو این خطبه اقبال مبارک</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>معنی نشنیدم که در آن لفظ شود گم</p></div>
<div class="m2"><p>این طرفه گهر بر صدف قال مبارک</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نعتش نمکین نقطه پر‌گار کمالست</p></div>
<div class="m2"><p>بر چهره خورشید تو این خال مبارک</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بر جوهر اول دم مدحش بدمیدی</p></div>
<div class="m2"><p>این روح قدس بر تن تمثال مبارک</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>خورشید ثنا رفت به بیت‌الشرف خویش</p></div>
<div class="m2"><p>بر شهر سخن غره این سال مبارک</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>فال نقطی می‌زند از کلک تو خورشید</p></div>
<div class="m2"><p>خمیازه‌کش جاه ترا فال مبارک</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>هر موی تو عید هنری سایه‌نشین داشت</p></div>
<div class="m2"><p>شد جمله ز نعت شرف آل مبارک</p></div></div>
<div class="b2" id="bn50"><p>ای شانه‌کش طره نعت تو زبانها</p>
<p>وی دانه‌کش خرمن مدح تو بیانها</p></div>
<div class="b" id="bn51"><div class="m1"><p>نعت تو شها ناسخ آیات عذابست</p></div>
<div class="m2"><p>با نعمت تو دوزخ سخن آتش و آبست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ظلم از غضبت دیده به مسمار مژه دوخت</p></div>
<div class="m2"><p>ور ز آنکه گشوده‌ست ز خمیازه خوابست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بر روی هم آراید اگر خصم صف کین</p></div>
<div class="m2"><p>همچون صف مژگان به نگاه تو خرابست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چون نقش کنم بر ورق از پرتو معنی</p></div>
<div class="m2"><p>گویی که رقم کرده به آب زر نابست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>پرواز کند حرف ز کلکم سوی نامه</p></div>
<div class="m2"><p>از بس به ره منقبتت گرم شتابست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>آنجا که شود موجه‌فشان بحر کمالت</p></div>
<div class="m2"><p>مانا که کمال دو جهان عین سرابست</p></div></div>
<div class="b2" id="bn57"><p>ای خطبه سلطانی کونین به نامت</p>
<p>وی عرش برین سایه‌نشین در و بامت</p></div>
<div class="b" id="bn58"><div class="m1"><p>ای طور شبستان ترا تازه ندیمی</p></div>
<div class="m2"><p>نه پرده چرخ از حرمت کهنه گلیمی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>خاک در تو تربیت عرش برین کرد</p></div>
<div class="m2"><p>چونان که کسی تربیت طفل یتیمی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>هر شمع از‌ین وادی ایمن سره نخلیست</p></div>
<div class="m2"><p>هر جلوه شمعی دیت خون کلیمی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>خورشید سراسیمه دود تا در مشرق</p></div>
<div class="m2"><p>گر شعله شمعی طپد از موج نسیمی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>فردوس به عذر آید و لطفت نپذیرد</p></div>
<div class="m2"><p>گستاخ اگر باد رود پیش شمیمی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>خدام تو هر یک چو کلیمند در‌ین طور</p></div>
<div class="m2"><p>ای کعبه در‌ین کوی چو من تیره گلیمی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>کاهند الف‌وار و ز اعجاز نمایند</p></div>
<div class="m2"><p>اسرار جهان را همه در نقطه جیمی</p></div></div>
<div class="b2" id="bn65"><p>شیخ حرم قرب و مرید دل خویشند</p>
<p>فیض دو جهانند و در آب و گل خویشند</p></div>
<div class="b" id="bn66"><div class="m1"><p>امشب شب عیدست و مرا روز سیاهست</p></div>
<div class="m2"><p>عزم سف نعش مرا سوی هراهست</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>امشب ز جگر تا مژه‌ام اشک وداعست</p></div>
<div class="m2"><p>فرداست که این مائده‌ام توشه راهست</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>امشب نگهم را نفس بازپسین‌ست</p></div>
<div class="m2"><p>فرداست که یک یک مژه تابوت نگاهست</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>تا مهره گردون نفس سوخته چون دود</p></div>
<div class="m2"><p>بنشسته از‌ین ماتم در شعله آهست</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ای معتکفان حرم قدس نگاهی</p></div>
<div class="m2"><p>بر کار من خسته که پر حال تباهست</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چون موج مخندید کز‌ین چشمه رحمت</p></div>
<div class="m2"><p>بر‌بست فلان رخت و همان نامه سیاهست</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بر زخم من الماس مپاشید که عذرم</p></div>
<div class="m2"><p>صد مرتبه روشن‌تر از آیینه ماهست</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>من ابر سیه کارم و این مرحله خورشید</p></div>
<div class="m2"><p>زین مرحله‌ام زود شدن عذر گناهست</p></div></div>
<div class="b2" id="bn74"><p>ایمان فصیحی‌ست درت جان فصیحی</p>
<p>رفتم من و بی عیب شد ایمان فصیحی</p></div>