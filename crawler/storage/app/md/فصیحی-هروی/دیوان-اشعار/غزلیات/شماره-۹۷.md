---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>شوق دیدار تو چون چشم مرا باز کند</p></div>
<div class="m2"><p>مژه پیش از نگهم سوی تو پرواز کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قیمت حسن ز بس عشق تو افزود کنون</p></div>
<div class="m2"><p>داغ غم بر جگر سوختگان ناز کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به مسیح ار چمن داغ مرا بفروشند</p></div>
<div class="m2"><p>هر دمش تازه‌تر از شبنم اعجاز کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم از خدمت امید به جایی نرسید</p></div>
<div class="m2"><p>همت یاس مگر زآنکه دری باز کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق در گنج غم ار دست کرم بگشاید</p></div>
<div class="m2"><p>همت آنست که ما را همه تن آز کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ریختی خون فصیحی ز سرش زود مرو</p></div>
<div class="m2"><p>باش تا شکر کرمهای تو آغاز کند</p></div></div>