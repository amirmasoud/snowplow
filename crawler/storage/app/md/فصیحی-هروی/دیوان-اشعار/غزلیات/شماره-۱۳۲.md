---
title: >-
    شمارهٔ ۱۳۲
---
# شمارهٔ ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>تا توانی در ترازوی هوس بی سنگ باش</p></div>
<div class="m2"><p>چون گل آزادگی بیزار از آب و رنگ باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسن اگر در دیده چون نازت دهد جا پا منه</p></div>
<div class="m2"><p>خوش نشین ناله‌های زار چون آهنگ باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون خزان آید در دل چون گل از شش سوگشای</p></div>
<div class="m2"><p>در بهاران غنچه‌سان در بسته و دلتنگ باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در گلستان جنون دستان رسوایی بزن</p></div>
<div class="m2"><p>عقل گو خار سر دیوار نام و ننگ باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نوشداروی جنون در حقه تسلیم نیست</p></div>
<div class="m2"><p>خوی طفلان گیر و دست‌آموز صلح و جنگ باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بال می‌رویاندم از تن چو اخگر شوق دوست</p></div>
<div class="m2"><p>ره چو سوی اوست گو یک گام صد فرسنگ باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مقصد افغانست خوش باشد فصیحی شرم چیست</p></div>
<div class="m2"><p>گو درین ماتم‌سرا یک نوحه بی آهنگ باش</p></div></div>