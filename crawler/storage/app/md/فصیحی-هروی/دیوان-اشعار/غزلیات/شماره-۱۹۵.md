---
title: >-
    شمارهٔ ۱۹۵
---
# شمارهٔ ۱۹۵

<div class="b" id="bn1"><div class="m1"><p>تا کی چو طفل عقل دم از مهر و کین زنیم</p></div>
<div class="m2"><p>کو همتی که پای بر آن و بر این زنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تلخیم در مذاق جهان همچنان اگر</p></div>
<div class="m2"><p>صد بار غوطه در شکر و انگبین زنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاکستریم و باز به صد حیله خویش را</p></div>
<div class="m2"><p>سوزیم تا دمی نفسی آتشین زنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گریند شام ماتم ما دوستان ما</p></div>
<div class="m2"><p>چون صبح خنده در نفس واپسین زنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاکیم و بردبار نه آن نازنین زلال</p></div>
<div class="m2"><p>کز جنبش نسیم گره بر جبین زنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز آن شعله‌ای که سینه آتش کباب اوست</p></div>
<div class="m2"><p>کو یک شرر که در جگر کفر و دین زنیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دستم نماند بس که فصیحی به سر زدیم</p></div>
<div class="m2"><p>زین پس به جای دست مگر آستین زنیم</p></div></div>