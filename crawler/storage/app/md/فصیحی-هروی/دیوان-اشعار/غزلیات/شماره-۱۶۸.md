---
title: >-
    شمارهٔ ۱۶۸
---
# شمارهٔ ۱۶۸

<div class="b" id="bn1"><div class="m1"><p>قرعه کاری به نام سعی باطل می‌زنم</p></div>
<div class="m2"><p>می‌کشم از سینه تیر آه و بر دل می‌زنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر گلو از طوق راه تیغ روشن می‌کنم</p></div>
<div class="m2"><p>قمری این گلستانم فال بسمل می‌زنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>موجم و آشفته دارد ذوق سرگردانیم</p></div>
<div class="m2"><p>غوطه در گرداب خون از بیم ساحل می‌زنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبر قفل بی کلیدم پرگشایش جو نیم</p></div>
<div class="m2"><p>حلقه چشمی به بازی بر در دل می‌زنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشته بگذاریدم امشب اندرین میدان که من</p></div>
<div class="m2"><p>یک شبیخون دگر بر تیغ قاتل می‌زنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همتم وز کبریای عشق در کوی طلب</p></div>
<div class="m2"><p>مهر حسرت بر دهان و دست سائل می‌زنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پای شوقم چند بشتابم فصیحی بی‌رفیق</p></div>
<div class="m2"><p>دست امیدی به دامان سلاسل می‌زنم</p></div></div>