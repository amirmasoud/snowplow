---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>چشم ترا ز مستی ناز آفریده‌اند</p></div>
<div class="m2"><p>زلف ترا ز عمر دراز آفریده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو یوسفی چو همت عشقم بلند بود</p></div>
<div class="m2"><p>زآنت خراب کرده و باز آفریده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشنو نوای ناله ما کاین ترانه را</p></div>
<div class="m2"><p>از شعله‌های گوش گداز آفریده‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ابروی دوست بین و بر آن جان نثار کن</p></div>
<div class="m2"><p>کاین قبله را نه بهر نماز آفریده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داغ دلم چو لاله ز شوخی برون فتاد</p></div>
<div class="m2"><p>ورنه مرا ز جوهر راز آفریده‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کبکم ولیک سینه بی طالع مرا</p></div>
<div class="m2"><p>بعد از شکست چنگل باز آفریده‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رشک سبک عنانی دل می‌کشد مرا</p></div>
<div class="m2"><p>کش چون نسیم بیهده تاز آفریده‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمعیم و خوانده‌ایم خط سرنوشت خویش</p></div>
<div class="m2"><p>ما را برای سوز و گداز آفریده‌اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر سوزن مسیح فصیحی ستم مکن</p></div>
<div class="m2"><p>کاین سینه را زچاک نیاز آفریده‌اند</p></div></div>