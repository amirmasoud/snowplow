---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>دیده امشب ره نظاره به پایان آورد</p></div>
<div class="m2"><p>به صد افسون نگهی تا سر مژگان آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راه آباد بسی بود ولی غمزه دوست</p></div>
<div class="m2"><p>به لب کوثرم از راه بیابان آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داد سرمایه به تاراج دل و آخر کار</p></div>
<div class="m2"><p>خبر یوسف گم‌گشته به کنعان آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تازه کردند ملایک به تو ایمان نیاز</p></div>
<div class="m2"><p>کفر چون دید خطت را به خود ایمان آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سنبل دوست پریشان خودست ارنه بهار</p></div>
<div class="m2"><p>باد را دست هوس بسته به بستان آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نام منصور برد عشق و لب خویش مکد</p></div>
<div class="m2"><p>گر زبان تو نوایی به گلستان آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کفنی جوی فصیحی که سحر چاک غمی</p></div>
<div class="m2"><p>خبر مرگ گریبان سوی دامان آورد</p></div></div>