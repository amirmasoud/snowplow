---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>از فیض گریه‌ام مژه نشو و نما گرفت</p></div>
<div class="m2"><p>سامان صد بهار چمن زین گیا گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیرنگ عشق بین که به یک تار زلف یار</p></div>
<div class="m2"><p>سرتاسر زمانه به تار بلا گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگداختم ز رشک که شمع سحرگهی</p></div>
<div class="m2"><p>جان داد و بوی دوست ز باد صبا گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوی ترا به من که رساند که هر نسیم</p></div>
<div class="m2"><p>کان چین زلف دید در آن حلقه جا گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دوست دشمنی‌ست فصیحی نصیب دل</p></div>
<div class="m2"><p>قسمت نگر که سایه خور از هما گرفت</p></div></div>