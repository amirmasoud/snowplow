---
title: >-
    شمارهٔ ۱۰۴
---
# شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>بیزارم از آن سینه که از جوش نشیند</p></div>
<div class="m2"><p>پوشد کفن شعله و خاموش نشیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بختم شب تاریست که تا صبح قیامت</p></div>
<div class="m2"><p>در ماتم خورشید سیه‌پوش نشیند</p></div></div>