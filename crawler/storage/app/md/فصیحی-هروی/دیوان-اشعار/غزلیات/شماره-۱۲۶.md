---
title: >-
    شمارهٔ ۱۲۶
---
# شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>ای دل نشاط صرف کن و غم نگاه دار</p></div>
<div class="m2"><p>داغی که بسپریم ز مرهم نگاه دار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آه و ناله هر چه بود در حرم بهل</p></div>
<div class="m2"><p>اشکی به یادگاری زمزم نگاه دار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک لاله‌وار داغ دل ار افتدت به دست</p></div>
<div class="m2"><p>شاداب خو به غم کن و خرم نگاه دار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی غم دل مرا نتوان داشتن نگاه</p></div>
<div class="m2"><p>اینک ببر دلم تو و بی غم نگاه دار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غم روید ار ز سینه‌ات آتش بر آن فشان</p></div>
<div class="m2"><p>این سبزه را ز آفت شبنم نگاه دار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جام جمی به میکده چندی تهی نشین</p></div>
<div class="m2"><p>رام کسی مشو ادب جم نگاه دار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مطرب نه‌ای منال فصیحی به بزم عیش</p></div>
<div class="m2"><p>این ناله را ذخیره ماتم نگاه دار</p></div></div>