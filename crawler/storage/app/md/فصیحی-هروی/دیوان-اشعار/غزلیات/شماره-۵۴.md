---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>نخل طلبم هیچ مرا برگ و بری نیست</p></div>
<div class="m2"><p>فرزند خزانم ز بهارم خبری نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از باغ وفا مگذر اگر تشنه کامی</p></div>
<div class="m2"><p>کآنجا همه گر بید بود بی‌ثمری نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر بام و در دوست تجلی نفشاندند</p></div>
<div class="m2"><p>گویا که درین کوچه پریشان‌نظری نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویند که بیگانه پرست است غم دوست</p></div>
<div class="m2"><p>صد شکر که بیگانه‌تر از ما دگری نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزی که در گنج کرم باز کند حسن</p></div>
<div class="m2"><p>سرمایه ما هیچ بجز چشم تری نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لخت دلم از دیده کند سوی تو پرواز</p></div>
<div class="m2"><p>مشتاق ترا شوق کم از بال و پری نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر لذت داغ جگر اینست فصیحی</p></div>
<div class="m2"><p>صد حیف که بر هر سر مویم جگری نیست</p></div></div>