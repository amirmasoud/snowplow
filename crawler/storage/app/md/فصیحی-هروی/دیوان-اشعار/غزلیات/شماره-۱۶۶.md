---
title: >-
    شمارهٔ ۱۶۶
---
# شمارهٔ ۱۶۶

<div class="b" id="bn1"><div class="m1"><p>از ناله لب حوصله بستن نتوانم</p></div>
<div class="m2"><p>همچون مژه بی اشک نشستن نتوانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این تار نفس بگسلم اکنون که توان هست</p></div>
<div class="m2"><p>ترسم دگر از ضعف گسستن نتوانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو عهد‌شکن خواهی و من بس که ضعیفم</p></div>
<div class="m2"><p>یک عهد به صد سال شکستن نتوانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خار طلبم چون گل دل لاله‌مزاجم</p></div>
<div class="m2"><p>بی داغ درین بادیه رستن نتوانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از چهره دل گرد غم دوست فصیحی</p></div>
<div class="m2"><p>صد شکر که دریایم و شستن نتوانم</p></div></div>