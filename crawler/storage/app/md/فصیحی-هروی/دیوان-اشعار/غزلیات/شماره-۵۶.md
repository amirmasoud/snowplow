---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>تب دوش از ملال تو از خود خبر نداشت</p></div>
<div class="m2"><p>ظالم به خود گمان ستم این قدر نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم به دل بگیردت اندر بدن گرفت</p></div>
<div class="m2"><p>آه نکرده کار وقوف اثر نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حسنت هزار شعبده در عرضه داشت لیک</p></div>
<div class="m2"><p>تبخال درد بر لب تنگ شکر نداشت</p></div></div>