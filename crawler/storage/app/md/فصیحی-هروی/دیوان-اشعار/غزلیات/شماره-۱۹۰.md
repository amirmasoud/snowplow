---
title: >-
    شمارهٔ ۱۹۰
---
# شمارهٔ ۱۹۰

<div class="b" id="bn1"><div class="m1"><p>شب که ما تو سن بیهوده روی پی کردیم</p></div>
<div class="m2"><p>صد بیابان سرشک از مژه‌ای طی کردیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق می‌خواست قدح در خور خمیازه خویش</p></div>
<div class="m2"><p>ز اشک حسرت مژه‌واری به فسون می‌کردیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ره ناله ما کوس هوس داشت کمین</p></div>
<div class="m2"><p>ما نهان از نفس این زمزمه با نی کردیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در زه خود سفر دور تو تا آینه‌ است</p></div>
<div class="m2"><p>تو چه دانی که چه‌سان بادیه‌ها طی کردیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکر ناکرده وفاهاش فصیحی تا چند</p></div>
<div class="m2"><p>یاد افسانه طرازی جم و کی کردیم</p></div></div>