---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>مگر بازم دل از زخم جفایی شاد می‌گردد</p></div>
<div class="m2"><p>که مرگم گرد جان بهر مبارک باد می‌گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به فتراک محبت من همان صید گرفتارم</p></div>
<div class="m2"><p>که بسمل گشته و گرد سر صیاد می‌گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به جای باده خون در ساغرست امروز خسرورا</p></div>
<div class="m2"><p>همانا یاد شیرین در دل فرهاد می‌گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فصیحی زار می‌نالد مگر جان می‌دهد از غم</p></div>
<div class="m2"><p>که امشب ماتمی بر گرد این فریاد می‌گردد</p></div></div>