---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>از روز سیاهم شب هجران گله دارد</p></div>
<div class="m2"><p>وز صبحدمم شام غریبان گله دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لب تشنه فتادیم در آن بادیه کآنجا</p></div>
<div class="m2"><p>از خشک لبی چشمه حیوان گله دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دامن هستی به فغان آمده پایم</p></div>
<div class="m2"><p>مسکین مگر از تنگی دامان گله دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صحرای فراق تو که ریگش غم و دردست</p></div>
<div class="m2"><p>چندان که خورد خون شهیدان گله درد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کو نوح که بر زورق افلاک بخندد</p></div>
<div class="m2"><p>امشب که ز دل دیده و دامان گله دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پامال دو صد قافله خونست درین راه</p></div>
<div class="m2"><p>آن دیده که از سایه مژگان گله دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک گام فزون نیست ره کعبه فصیحی</p></div>
<div class="m2"><p>رخش طلب از تنگی میدان گله دارد</p></div></div>