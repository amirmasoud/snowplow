---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>کی ز ماتمخانه ما دود افغان برنخاست</p></div>
<div class="m2"><p>کی غم از بالین ما با چشم گریان برنخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در زمین سیل‌خیز دیده گریان ما</p></div>
<div class="m2"><p>کی نشست اشکی که چون برخاست طوفان برنخاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد نسیم آمد ز مصر و بوی پیراهن رساند</p></div>
<div class="m2"><p>ناتوانی بین که گردی زین بیابان برنخاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من ندانم بود چشم خون‌فشانی یا نبود</p></div>
<div class="m2"><p>این قدر دانم که این طوفان ز دامان برنخاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ذوق بی‌سامانیم هر چند بر بالین نشست</p></div>
<div class="m2"><p>این سر آشفته‌بخت از خواب سامان برنخاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک جهان زخم از دم تیغ تو بر هر مو شکفت</p></div>
<div class="m2"><p>از سر جان کشته تیغ تو آسان برنخاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکر فیض نوبهار غم فصیحی چون کنم</p></div>
<div class="m2"><p>کز گلم یک لاله بی چاک گریبان برنخاست</p></div></div>