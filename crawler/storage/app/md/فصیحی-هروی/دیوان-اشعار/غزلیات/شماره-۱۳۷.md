---
title: >-
    شمارهٔ ۱۳۷
---
# شمارهٔ ۱۳۷

<div class="b" id="bn1"><div class="m1"><p>ز پا افکند ما را آرزوی سرو بالایش</p></div>
<div class="m2"><p>اجل گر دست ما گیرد سر افشانیم در پایش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا دوزخ سزاوارست اما دیده راحت</p></div>
<div class="m2"><p>که من با عشق او خو دارم و او با تماشایش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به نوعی بگذرد آن تند خو گرم عتاب از من</p></div>
<div class="m2"><p>که سوزد پرده‌های چشم از باد کف پایش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که باز از بانگ «ارنی» کرد در تاب آتش ما را</p></div>
<div class="m2"><p>که امشب سوخت مغز هوش را برق تمنایش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فصیحی وعده قتلم به فردا داد و می‌ترسم</p></div>
<div class="m2"><p>که فردای قیامت هم بود امروز [و] فردایش</p></div></div>