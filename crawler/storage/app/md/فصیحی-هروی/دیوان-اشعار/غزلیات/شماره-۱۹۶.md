---
title: >-
    شمارهٔ ۱۹۶
---
# شمارهٔ ۱۹۶

<div class="b" id="bn1"><div class="m1"><p>برخیز تا زیارت ماتم‌سرا کنیم</p></div>
<div class="m2"><p>جان را برای قطره اشکی فدا کنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر مرهم مسیح بخندیم و بگذریم</p></div>
<div class="m2"><p>هر درد را به حسرت دردی دوا کنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داغیم و کار ما سفر ملک سینه‌هاست</p></div>
<div class="m2"><p>تا خویش را به چاک غمی آشنا کنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بستند در به روی تمنای هر دو کون</p></div>
<div class="m2"><p>فرصت غنیمت‌ست نمازی ادا کنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سجده اوفتیم چو بینیم روی دوست</p></div>
<div class="m2"><p>عید شهادتست بیا تا دعا کنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کو یک جهان نظاره که در طور وصل دوست</p></div>
<div class="m2"><p>امروز یک نماز تماشا قضا کنیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پر مهربان حنجره ماست تیغ ناز</p></div>
<div class="m2"><p>دیگر شکایت از غم هجران چرا کنیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر سر به پای غم نفشاندیم بخل نیست</p></div>
<div class="m2"><p>می‌خواستیم در طلب دوست پا کنیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در لرزه اوفتیم فصیحی درین چمن</p></div>
<div class="m2"><p>گستاخ اگر چو شمع طواف صبا کنیم</p></div></div>