---
title: >-
    شمارهٔ ۲۰۰
---
# شمارهٔ ۲۰۰

<div class="b" id="bn1"><div class="m1"><p>رسم عشاقست خندان اشک حرمان ریختن</p></div>
<div class="m2"><p>دیده در طوفان خون و گل به دامان ریختن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گریه را در پرده‌های خون دل پیچم که هست</p></div>
<div class="m2"><p>کفر در کیش حیا یک قطره عریان ریختن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نی دلی دریای خون نه سینه‌ای گرداب غم</p></div>
<div class="m2"><p>چون توانم قطره اشکی به سامان ریختن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رنگ احسان نیست بر سیمای ابر نوبهار</p></div>
<div class="m2"><p>ورنه خون بایست بر خاک شهیدان ریختن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بوی گل انگیزد از هر قطره خونش بلبلی</p></div>
<div class="m2"><p>صرفه نبود خون بلبل در گلستان ریختن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گاه دامن گلشن است و گه گریبان گلستان</p></div>
<div class="m2"><p>دیده دلگیرست ازین اشک پریشان ریخن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لاف حیرت می‌زنی شرمت فصیحی زین گزاف</p></div>
<div class="m2"><p>چشم حیران و سر و برگ گلستان ریختن</p></div></div>