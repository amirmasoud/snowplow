---
title: >-
    شمارهٔ ۱۹۳
---
# شمارهٔ ۱۹۳

<div class="b" id="bn1"><div class="m1"><p>کو دل که رو به میکده مشرب آوریم</p></div>
<div class="m2"><p>شبخون کفر بر سپه مذهب آوریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلفت اگر مدد کند از دست آفتاب</p></div>
<div class="m2"><p>گیریم جیب صبح و به سوی شب آوریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا سالها به خون نطپد ناله در جگر</p></div>
<div class="m2"><p>رخصت نمی‌دهند که سوی لب آوریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نالش خوش است اگر همه از جور دوست است</p></div>
<div class="m2"><p>کو لب که رو به زمزمه یا‌رب آوریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم‌چشم ماست چرخ فصیحی درین چمن</p></div>
<div class="m2"><p>تا هر شب از سرشک به رخ کوکب آوریم</p></div></div>