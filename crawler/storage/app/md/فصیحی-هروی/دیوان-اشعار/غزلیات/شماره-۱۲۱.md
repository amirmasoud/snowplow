---
title: >-
    شمارهٔ ۱۲۱
---
# شمارهٔ ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>دل از ولایت غم بار بسته می‌‌آید</p></div>
<div class="m2"><p>چو موج بر سر طوفان نشسته می‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی که لب به سراغی نسوخت کی‌داند</p></div>
<div class="m2"><p>که کار بال ز پای شکسته می‌آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیا به طور و همه گوش شو که شاهد حسن</p></div>
<div class="m2"><p>ز ناله «ارنی» پرده بسته می‌‌‌‌‌آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شهید رسم دیاری شوم که بعد از مرگ</p></div>
<div class="m2"><p>طبیب بر سر بالین خسته می‌آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه زخمه بود ندانم که چنگ پاس مرا</p></div>
<div class="m2"><p>هنوز ناله ز تار گسسته می‌آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز نخل طور چه حاصل که در مشام مرا</p></div>
<div class="m2"><p>شمیم عافیت از نخل بسته می‌آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فریب سعی فصیحی مخور که کعبه وصل</p></div>
<div class="m2"><p>به دلنوازی پای شکسته می‌آید</p></div></div>