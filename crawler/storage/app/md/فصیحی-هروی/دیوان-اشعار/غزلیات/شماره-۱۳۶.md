---
title: >-
    شمارهٔ ۱۳۶
---
# شمارهٔ ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>متاع اشک گر آتش بود برو بفروش</p></div>
<div class="m2"><p>وگرنه از مژه بستان برو به جو بفروش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو کاروان هوس دررسد ز راه حرم</p></div>
<div class="m2"><p>بگیر ذلت و صد چشمه آبرو بفروش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برهنه شو چو گل داغ و از خزان مندیش</p></div>
<div class="m2"><p>به هر بها که ستانند رنگ و بو بفروش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شود گر از کرم عشق مو به مویت دل</p></div>
<div class="m2"><p>به نیم غمزه آن چشم فتنه‌جو بفروش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>متاع زهد کسادست سوی میکده بر</p></div>
<div class="m2"><p>به خنده قدح و گریه سبو بفروش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرو به رسته مصر ار روی زلیخاوار</p></div>
<div class="m2"><p>چو بنده‌ای بخری خویش را بدو بفروش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زبان شعله فصیحی بخر درین بازار</p></div>
<div class="m2"><p>به نرخ ناله جانسوز گفتگوی بفروش</p></div></div>