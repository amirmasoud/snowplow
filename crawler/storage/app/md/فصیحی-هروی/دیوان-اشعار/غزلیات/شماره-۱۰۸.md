---
title: >-
    شمارهٔ ۱۰۸
---
# شمارهٔ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>شب که جانم خسته از بیم وداع یار بود</p></div>
<div class="m2"><p>ناله‌های خفته در دل بر لبم بیدار بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لخت لخت دل ز مژگان سوی چشمم بازگشت</p></div>
<div class="m2"><p>بینوا گویی چو من لب تشنه دیدار بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کاروان گریه گویی از جگر آمد که دوش</p></div>
<div class="m2"><p>قطره‌های اشک ما را ناله‌ها در بار بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دامنم از پاره‌های دل گلستان گشت آه</p></div>
<div class="m2"><p>یاد روزی کز تماشا دیده‌ام گلزار بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان شب از خون جگر مرثیه ما می‌نوشت</p></div>
<div class="m2"><p>این قدر هم مرحمت ز آن بی‌وفا بسیار بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شرم عشقم می‌کشد کز دولت وصلش دو روز</p></div>
<div class="m2"><p>شعله‌های دوزخ غم در جگر بیکار بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی تو هرگز نوبر سیر سر مژگان نکرد</p></div>
<div class="m2"><p>سال‌ها گویی فصیحی را نگه بیمار بود</p></div></div>