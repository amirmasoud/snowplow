---
title: >-
    شمارهٔ ۱۳۵
---
# شمارهٔ ۱۳۵

<div class="b" id="bn1"><div class="m1"><p>تاب خورشید رخت از تب فزونتر بود دوش</p></div>
<div class="m2"><p>آتشم چون شمع جای خاک بر سر بود دوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از عرق می‌ریخت شبنم بر رخ گلهای حسن</p></div>
<div class="m2"><p>عید آب خضر با نوروز کوثر بود دوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن تنی کز ناز تاب بار رعنایی نداشت</p></div>
<div class="m2"><p>آه کز بیداد تب در آب و آذر بود دوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از تبت تا صبح بیماران غم می‌سوختند</p></div>
<div class="m2"><p>بر شهیدان تو گویی روز محشر بود دوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در فراق چشم بیمار تو خواب ناز را</p></div>
<div class="m2"><p>پهلوی آسودگی بر نیش نشتر بود دوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست غم بادا قوی بازو که اندر سینه‌ام</p></div>
<div class="m2"><p>هر نفس آزرده صد نوک خنجر بود دوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از تهی‌دستی فصیحی هیچ قربانت نکرد</p></div>
<div class="m2"><p>زآنکه چشم گوهر افشانش در اخگر بود دوش</p></div></div>