---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>دوش غم بر گریه‌های زخم ما خندید و رفت</p></div>
<div class="m2"><p>پاره‌ای بر ریش‌های ما نمک پاشید و رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عید نومیدی مبارک باد کز رویش نگاه</p></div>
<div class="m2"><p>همچو اشک حسرتم در خاک و خون غلتید و رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب در آمد غم درون از رخنه‌های سینه‌ام</p></div>
<div class="m2"><p>دامنی داغ جگر از هر گیاهم چید و رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرهم الماس می‌پنداشت زخمم چاره‌جوست</p></div>
<div class="m2"><p>آمد و روی تظلم بر زمین مالید و رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سر نعش فصیحی این همه فریاد چیست</p></div>
<div class="m2"><p>بر در غم بینوایی پاره‌ای نالید و رفت</p></div></div>