---
title: >-
    شمارهٔ ۱۰۵
---
# شمارهٔ ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>چون صبا جلوه آن زلف گره گیر دهد</p></div>
<div class="m2"><p>عقل را ذوق جنون مژده زنجیر دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناقه را پا همه بر دیده خونبار آید</p></div>
<div class="m2"><p>هجر چون قافله را رخصت شبگیر دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که در عهد جمال تو ز مادر زاید</p></div>
<div class="m2"><p>دایه فطرتش از خون جگر شیر دهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هان فصیحی کم جان گوی که این بیشه ما</p></div>
<div class="m2"><p>همه از زهر گیا طعمه نخجیر دهد</p></div></div>