---
title: >-
    شمارهٔ ۹۶
---
# شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>تلخکامان مزه شهد هوس نشناسند</p></div>
<div class="m2"><p>سایه پرورد همایند مگس نشناسند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داد ازین شعله مزاجان که چو مرهم گردند</p></div>
<div class="m2"><p>سینه‌ای ریش‌تر از سینه خس نشناسند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نفس سوختگان شعله طورست ولیک</p></div>
<div class="m2"><p>شعله طور ندانند و نفس نشناسند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یاد آن قافله کز غم چو حدی ساز کنند</p></div>
<div class="m2"><p>ناله خویش ز فریاد جرس نشناسند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکرستان نیازند جگر سوختگان</p></div>
<div class="m2"><p>وای اگر لذت پابوس مگس نشناسند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مصرعصمت چه دیاریست که خوبان آنجا</p></div>
<div class="m2"><p>صورت خویش در آیینه کس نشناسند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طرفه رمزی‌ست فصیحی که به گلزار جهان</p></div>
<div class="m2"><p>بلبلان جمله قفس‌زاد و قفس نشناسند</p></div></div>