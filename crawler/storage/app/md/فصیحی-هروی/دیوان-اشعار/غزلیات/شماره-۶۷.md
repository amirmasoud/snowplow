---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>چنان که باغ در آغوش گل نمی‌گنجد</p></div>
<div class="m2"><p>شکیب در دل مدهوش گل نمی گنجد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نخست نام دهانت شنید غنچه مگر</p></div>
<div class="m2"><p>که هیچ زمزمه در گوش گل نمی‌گنجد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهید تیغ ستم شو که زیر نعش هزار</p></div>
<div class="m2"><p>ز بس هجوم ملک دوش گل نمی‌گنجد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به عندلیب بگویید کاین فغان تا چند</p></div>
<div class="m2"><p>درین چمن لب خاموش گل نمی‌گنجد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نصیب این دل ریش است نیش خار ارنه</p></div>
<div class="m2"><p>صبا درین چمن از جوش گل نمی‌گنجد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به محملی که خموشی هزار دستانست</p></div>
<div class="m2"><p>لب سرود فراموش گل نمی‌گنجد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرو به باغ فصیحی که این دلی که تراست</p></div>
<div class="m2"><p>ز نیش خار درو جوش گل نمی‌گنجد</p></div></div>