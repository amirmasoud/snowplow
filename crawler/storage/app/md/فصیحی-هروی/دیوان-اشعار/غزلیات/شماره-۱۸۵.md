---
title: >-
    شمارهٔ ۱۸۵
---
# شمارهٔ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>لب زخمیم و افغان را پرستیم</p></div>
<div class="m2"><p>دم شمشیر عریان را پرستیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل از بلبل همین بس دولت ما</p></div>
<div class="m2"><p>که دیوار گلستان را پرستیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز ما یوسف پرستیدن ادب نیست</p></div>
<div class="m2"><p>بیا تا خاک کنعان را پرستیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پریشان‌زاده چون زلفیم و از شوق</p></div>
<div class="m2"><p>سر زلف پریشان را پرستیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به بوی عشق اندر کعبه و دیر</p></div>
<div class="m2"><p>دل گبر و مسلمان را پرستیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دورنگی در عبادتگاه ما نیست</p></div>
<div class="m2"><p>به یک دل کفر و ایمان را پرستیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فصیحی دست ناگه گیر عشقیم</p></div>
<div class="m2"><p>همه چاک گریبان را پرستیم</p></div></div>