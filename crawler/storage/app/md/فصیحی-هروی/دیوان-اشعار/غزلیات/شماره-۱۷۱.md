---
title: >-
    شمارهٔ ۱۷۱
---
# شمارهٔ ۱۷۱

<div class="b" id="bn1"><div class="m1"><p>کو هجوم گریه کز یک قطره صد جیحون کنم</p></div>
<div class="m2"><p>چشم مفلس را ز فیض رشحه‌ای قارون کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دل از جوش ملالم جای داغی هم نماند</p></div>
<div class="m2"><p>هم مگر آرایش این خانه از بیرون کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان اگر از ناتوانی برنیاید باک نیست</p></div>
<div class="m2"><p>ناله‌ام از ضعف اگر بر لب نیاید چون کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طرز این فکر نفس بافان پسند طبع نیست</p></div>
<div class="m2"><p>پاره‌ای از خون دل بر دارم و موزون کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر لبم حرفی اگر آید ز خون دل تهی</p></div>
<div class="m2"><p>بازش آرم تا از آن سرچشمه‌اش پرخون کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون صبا بر خار و گل پیچم به گلزار سخن</p></div>
<div class="m2"><p>هر چه خون آلوده نبود ز آن چمن بیرون کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر گزد دلهای محزون را فصیحی ناله‌ام</p></div>
<div class="m2"><p>از لب افسردگانش در نفس افسون کنم</p></div></div>