---
title: >-
    شمارهٔ ۱۸۴
---
# شمارهٔ ۱۸۴

<div class="b" id="bn1"><div class="m1"><p>از عمر دمی را به غمی باز نبستیم</p></div>
<div class="m2"><p>یک پرده آهنگ برین ساز نبستیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما ساده‌نوایان بهشتیم چو بلبل</p></div>
<div class="m2"><p>مرغوله بیهوده بر آواز نبستیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دام فتادیم صد افسوس کز آن پس</p></div>
<div class="m2"><p>بر بال و پر خود پر پرواز نبستیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تعویذ نکوییست وفا حیف که آن را</p></div>
<div class="m2"><p>بر بازوی آن زلف فسون‌ساز نبستیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کردیم فصیحی به فسون شکر که خود را</p></div>
<div class="m2"><p>بر دامن آن غمزه غماز نبستیم</p></div></div>