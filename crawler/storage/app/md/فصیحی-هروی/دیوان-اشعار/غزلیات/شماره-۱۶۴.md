---
title: >-
    شمارهٔ ۱۶۴
---
# شمارهٔ ۱۶۴

<div class="b" id="bn1"><div class="m1"><p>کتاب عشقم و آیات زلف دوست عنوانم</p></div>
<div class="m2"><p>ز سر تا پا گرم بندند شیرازه پریشانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگاه هرزه گردم شد سبک پرواز بستانی</p></div>
<div class="m2"><p>که باز از اشک نومیدی گرانبارست مژگانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی موج غریبم ای سراب عافیت رحمی</p></div>
<div class="m2"><p>که عمری در کنار تربیت پرورده طوفانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نماند از ترکتاز گریه هیچم خون و می‌ترسم</p></div>
<div class="m2"><p>که سازد تیغ نازی شرمسار خاک میدانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان شد تنگ عیش من که در گوش تنک ظرفان</p></div>
<div class="m2"><p>گران آید نوای خنده چاک گریبانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه بت نه ایزدم مانا فصیحی جلوه عشقم</p></div>
<div class="m2"><p>که در زلف و رخ خوبان پرستد کفر و ایمانم</p></div></div>