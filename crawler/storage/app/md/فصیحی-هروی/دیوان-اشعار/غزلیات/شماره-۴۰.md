---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>وفای عهد ز خوبان عهد ما غلط‌ست</p></div>
<div class="m2"><p>نسیم عافیت از گلشن بلا غلط‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کتاب حسن بر استاد عشق خواندم گفت</p></div>
<div class="m2"><p>درین میانه همین آیت وفا غلط‌ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حدیث طور شنیدی به کوی عشق گذر</p></div>
<div class="m2"><p>ز ما مپرس که این قصه راست یا غلط ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حیا به گوش ادب دوش در چمن می گفت</p></div>
<div class="m2"><p>که خنده گل و بی‌تابی صبا غلط‌ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زبان عشق خموشی‌ست لب ز ناله ببند</p></div>
<div class="m2"><p>که در طریق ادب عرض مدعا غلط‌ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پرست غنچه خاموش را ز گل آغوش</p></div>
<div class="m2"><p>به عندلیب بگویید کاین نوا غلط‌ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دوست قصه جان دادن فصیحی را</p></div>
<div class="m2"><p>چو هجر گفته دگر گفتگوی ما غلط‌ست</p></div></div>