---
title: >-
    شمارهٔ  ۵
---
# شمارهٔ  ۵

<div class="b" id="bn1"><div class="m1"><p>چو آفتاب کند تاب شرم ماه ترا</p></div>
<div class="m2"><p>به موج فتنه درآرد خط سیاه ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به چشم زخم بگریند کشتگان که مباد</p></div>
<div class="m2"><p>ز دیده اشک برد لذت نگاه ترا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه گلشنی تو که مشاطگان کشور حسن</p></div>
<div class="m2"><p>به نو‌بهار بروبند خوابگاه ترا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو مست جور و من از رشک غرق خون که مباد</p></div>
<div class="m2"><p>به نام خویش نویسد ملک گناه ترا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز باغ شعله فصیحی گل مراد بچین</p></div>
<div class="m2"><p>که نوبهار فکند از نظر گیاه ترا</p></div></div>