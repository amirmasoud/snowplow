---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>گر نقاب از رخ بگیری آفتابم می‌کشد</p></div>
<div class="m2"><p>ور گذاری این چنین رشک نقابم می‌کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شعله‌ام وز تشنگی بی‌تاب ای پیر مغان</p></div>
<div class="m2"><p>آتشی داری کرم فرما که آبم می‌کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس که گلزار دلم از تشنگی شد شعله‌زار</p></div>
<div class="m2"><p>غرقه دریایم و شوق سرابم می‌کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آرزوی دوزخم در آبش عصیان فکند</p></div>
<div class="m2"><p>ای خدا رحمی که هجران عذابم می‌کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سر بازار رعنایی فصیحی روز و شب</p></div>
<div class="m2"><p>خود فروشیهای ماه و آفتابم می‌کشد</p></div></div>