---
title: >-
    شمارهٔ  ۹
---
# شمارهٔ  ۹

<div class="b" id="bn1"><div class="m1"><p>جنونی کو که سرگردان کنم در دیده طوفان را</p></div>
<div class="m2"><p>ز برق کبریای اشک سوزم طور مژگان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرفتم سرمه از خاک ره نازی که می‌بینم</p></div>
<div class="m2"><p>عیان در گردن بیداد او خون شهیدان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرستار سر زلفی شدم وز شرم می‌سوزم</p></div>
<div class="m2"><p>که نشتر زار کردم از حسد رگهای ایمان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پریشانی مرا افزاید از جمعیت خاطر</p></div>
<div class="m2"><p>چو از شیرازه بند شانه آن زلف پریشان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هم دیری‌ست تا کردند دیر و کعبه صلح کل</p></div>
<div class="m2"><p>ندانم چیست باعث کینه گبر و مسلمان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخیلی نیست آیین محبت لیکن از غیرت</p></div>
<div class="m2"><p>درون جان کنم از سینه پنهان داغ حرمان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمی‌دانم فصیحی چون به هوش خویش باز آید</p></div>
<div class="m2"><p>کسی کز دست حسن آراست آن صف‌های مژگان را</p></div></div>