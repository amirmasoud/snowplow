---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>ماییم جدا از تو به غم ساخته‌ای چند</p></div>
<div class="m2"><p>با یاد تو دل از همه پرداخته‌ای چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماییم ز سودای بتان سود ندیده</p></div>
<div class="m2"><p>بی‌فایده نقد دل و دین‌باخته‌ای چند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیدی که چسان راز مرا پرده دریدند</p></div>
<div class="m2"><p>از روی نکو پرده برانداخته‌ای چند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رخسار تو کردند به آیینه برابر</p></div>
<div class="m2"><p>از بی‌بصری قدر تو نشناخته‌ای چند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگشای خدنگ مژه کز ذوق بمیرند</p></div>
<div class="m2"><p>جانها سپر تیر بلا ساخته‌ای چند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ارباب محبت چه کسانند فصیحی</p></div>
<div class="m2"><p>در کوچه محنت علم افراخته‌ای چند</p></div></div>