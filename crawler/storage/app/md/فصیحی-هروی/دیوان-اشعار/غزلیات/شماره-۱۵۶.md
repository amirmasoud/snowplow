---
title: >-
    شمارهٔ ۱۵۶
---
# شمارهٔ ۱۵۶

<div class="b" id="bn1"><div class="m1"><p>بترس از آن که دمی دامن سحر گیرم</p></div>
<div class="m2"><p>چو شعله دم به دم از سوز سینه درگیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به کوی زخم فروشان روم به سنه چاک</p></div>
<div class="m2"><p>هزار زخم در آغوش یک جگر گیرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیا شکفته و شیرین که گر مرا باشد</p></div>
<div class="m2"><p>رخ و لب تو جهان در گل و شکر گیرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به فتوی جگر از شمع خویشتن هر شب</p></div>
<div class="m2"><p>هزار شعله به تاوان بال و پر گیرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شدم مسافر اقلیم دل فصیحی‌وار</p></div>
<div class="m2"><p>بود که دامن دردی درین سفرگیرم</p></div></div>