---
title: >-
    شمارهٔ ۱۱۷
---
# شمارهٔ ۱۱۷

<div class="b" id="bn1"><div class="m1"><p>به باده صوفی ما صاف از ریا نشود</p></div>
<div class="m2"><p>که تار سبحه به مضراب خوش نوا نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گل نگویم اما شهید نام گلم</p></div>
<div class="m2"><p>که از فسون نیاز بهار وا نشود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترا چه جرم که حکم غرور حسن اینست</p></div>
<div class="m2"><p>که وعده‌های تو از صد یکی وفا نشود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر به خلد روم سوز دل همان باقی‌ست</p></div>
<div class="m2"><p>سموم بادیه در گلستان صبا نشود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سحاب ابر فصیحی به جرم ما چه کند</p></div>
<div class="m2"><p>به قطره‌ای گل عیش بهار وانشود</p></div></div>