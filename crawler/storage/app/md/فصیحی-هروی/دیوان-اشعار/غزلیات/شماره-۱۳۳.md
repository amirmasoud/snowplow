---
title: >-
    شمارهٔ ۱۳۳
---
# شمارهٔ ۱۳۳

<div class="b" id="bn1"><div class="m1"><p>من که افسرده‌ترم از نفس مرغ خموش</p></div>
<div class="m2"><p>کی رسد در چمن حسن توام لاف خروش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان گر از بهر نثارت نفرستم چه کنم</p></div>
<div class="m2"><p>گل مفلس چه کند گر نشود عطرفروش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه بهشت‌ست محبت که درو می‌گردند</p></div>
<div class="m2"><p>گریه زخم من و خنده گل دوش به دوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شوق بنگر که چو نامش به زبانم آید</p></div>
<div class="m2"><p>نگه از دیده سراسیمه دود تا در گوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تیغ نازی مگر احرام دلم بسته که باز</p></div>
<div class="m2"><p>زخمها هر نفس از شوق گشایند آغوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیش مژگان تو چون چشم تو شوخست مباد</p></div>
<div class="m2"><p>چشم زخمی رسد و بشکند اندر رگ هوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همت دیده بلندست فصیحی تن زن</p></div>
<div class="m2"><p>نظری را به تماشای دو عالم مفروش</p></div></div>