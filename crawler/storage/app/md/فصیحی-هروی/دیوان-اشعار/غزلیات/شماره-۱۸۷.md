---
title: >-
    شمارهٔ ۱۸۷
---
# شمارهٔ ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>ما بت نه زاندیشه معبود شکستیم</p></div>
<div class="m2"><p>آرایش بتخانه ما بود شکستیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ماتم گوش شنوا تار بریدیم</p></div>
<div class="m2"><p>صد زمزمه در زیر لب عود شکستیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاکستر ما شعله‌فروش است درین دیر</p></div>
<div class="m2"><p>ما نیش زیان در جگر سود شکستیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشقیم که چون پای خموشی بفشردیم</p></div>
<div class="m2"><p>گلبانگ نوا بر لب داود شکستیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حسن تو گواهست که ما خانه خرابان</p></div>
<div class="m2"><p>بر سنگ ازل شیشه بهبود شکستیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر لخت جگر طاقت صد داغ دگر داشت</p></div>
<div class="m2"><p>قفل در رسوایی خود زود شکستیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفتیم در آتشکده عشق فصیحی</p></div>
<div class="m2"><p>رنگ رخ صد شعله بی‌دود شکستیم</p></div></div>