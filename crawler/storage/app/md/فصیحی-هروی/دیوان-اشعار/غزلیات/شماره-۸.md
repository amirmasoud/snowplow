---
title: >-
    شمارهٔ  ۸
---
# شمارهٔ  ۸

<div class="b" id="bn1"><div class="m1"><p>باز عشق آمد که آراید گلستان مرا</p></div>
<div class="m2"><p>سازد از خون جگر شاداب بستان مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز عشق آمد که از فیض نشاط گریه‌ای</p></div>
<div class="m2"><p>چون کنار گل کند پرخنده دامان مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باز عشق آمد که دربستان کفر زلف دوست</p></div>
<div class="m2"><p>شبنم رخساره گل سازد ایمان مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باز عشق آمد که از هر دورباش غمزه‌ای</p></div>
<div class="m2"><p>بشکند در دیده من نوک مژگان مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باز عشق آمد که ناخن بررگ جانم زند</p></div>
<div class="m2"><p>زیور گوش ملایک سازد افغان مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باز عشق آمد که اندازد به رغم عافیت</p></div>
<div class="m2"><p>در کف صد چاک هر تار گریبان مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باز عشق آمد که از ذوق سرشک افشانیی</p></div>
<div class="m2"><p>دیده یعقوب سازد داغ حرمان مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باز عشق آمد که مهمان خدنگ ناز دوست</p></div>
<div class="m2"><p>پرسد از هر دل که بیند خانه جان مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باز عشق آمد که در بازار حسن خودفروش</p></div>
<div class="m2"><p>بر سر مژگان گشاید بار سامان مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باز عشق آمد که خنداند ز فیض جلوه‌ای</p></div>
<div class="m2"><p>چون لب زخم شهیدان چشم گریان مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باز عشق آمد که از زیر گلیم عافیت</p></div>
<div class="m2"><p>آورد بیرون فصیحی طبل پنهان مرا</p></div></div>