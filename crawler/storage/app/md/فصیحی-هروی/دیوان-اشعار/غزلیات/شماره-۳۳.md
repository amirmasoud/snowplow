---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>هزار گل ز جگر در کنار خنده ماست</p></div>
<div class="m2"><p>خزان عافیت ما بهار خنده ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برون بهشت و درون دوزخست پرهیزید</p></div>
<div class="m2"><p>از آن گیاه که در مرغزار خنده ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار شکر که حساد ما نمی‌دانند</p></div>
<div class="m2"><p>که گریه تازه گل نوبهار خنده ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز زخم بس که شکفتیم نوبهاران را</p></div>
<div class="m2"><p>هزار قافله در زیر بار خنده ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرت ز زهر فصیحی لبالب است ایاغ</p></div>
<div class="m2"><p>نگاه دار که آن یادگار خنده ماست</p></div></div>