---
title: >-
    شمارهٔ ۱۴۷
---
# شمارهٔ ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>دیده را گم داشتم عمری و اکنون یافتم</p></div>
<div class="m2"><p>گر نیامد نکهت پیراهنی چون یافتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از غرور عافیت می‌کردمش از گل سراغ</p></div>
<div class="m2"><p>عاقبت اندر میان دجله خون یافتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تشنگی می‌داشتم بستند دریا و سراب</p></div>
<div class="m2"><p>آخرالامر این گهر در جیب جیحون یافتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناله دیرآشنای گوش لیلایم از آن</p></div>
<div class="m2"><p>خویش را گم کردم و در جان مجنون یافتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل نماندم تا غم دل جلوه‌گر شد بی‌نقاب</p></div>
<div class="m2"><p>قطره‌ای گم کردم و صد بحر افزون یافتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سجده‌ای بر هر در افشاندیم اما عاقبت</p></div>
<div class="m2"><p>کعبه را از تنگنای کعبه بیرون یافتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قصه درد فصیحی می‌زدم امشب رقم</p></div>
<div class="m2"><p>لفظ را گم گشته در خوناب مضمون یافتم</p></div></div>