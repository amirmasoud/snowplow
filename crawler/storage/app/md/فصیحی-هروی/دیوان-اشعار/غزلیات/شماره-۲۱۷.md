---
title: >-
    شمارهٔ ۲۱۷
---
# شمارهٔ ۲۱۷

<div class="b" id="bn1"><div class="m1"><p>کوش تا سیراب از بحر تماشا نگذری</p></div>
<div class="m2"><p>همچو موج هرزه گرد از روی دریا نگذری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر ز گمراهی رهت بر شهر درمان اوفتد</p></div>
<div class="m2"><p>تا توانی بر شبستان مسیحا نگذری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لخت لختم تشنه دیدار تست ای برق غم</p></div>
<div class="m2"><p>چون به کوی ما رسی بر جان تنها نگذری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نکهتی‌ام بار کنعان بسته از مصر ای صبا</p></div>
<div class="m2"><p>عصمت من پاس داری بر زلیخا نگذری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دیار زخم ما ای مرهم آسودگی</p></div>
<div class="m2"><p>گر همه الماس باشی بی‌محابا نگذری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای کبوتر هر کجا بر خاک افتد نامه‌ام</p></div>
<div class="m2"><p>کوی یار ماست آن زنهار از آنجا نگذری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر فصیحی بال خواهش بی‌محابا می‌زنی</p></div>
<div class="m2"><p>بی‌ادب پروانه‌ای بر محفل ما نگذری</p></div></div>