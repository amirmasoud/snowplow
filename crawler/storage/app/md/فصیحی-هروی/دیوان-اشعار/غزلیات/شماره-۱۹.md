---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>باز دل بر در غم طرح محبت انداخت</p></div>
<div class="m2"><p>بر سر شعله چو خس رخت اقامت انداخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای سگ دوست به جانت که چو غم جانم خورد</p></div>
<div class="m2"><p>استخوانم را در دوزخ حسرت انداخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ریسمان نفسم را غم ایام گسیخت</p></div>
<div class="m2"><p>دلو عمرم به ته چاه قیامت انداخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل به صد حیله شد آبستن طفل طربی</p></div>
<div class="m2"><p>دید بخت سیهم را و بساعت انداخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه زین پیش مصیبت به دل آتش می‌زد</p></div>
<div class="m2"><p>ماتمم آتش در جان مصیبت انداخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فطرت پست فصیحی‌ست که در روز ازل</p></div>
<div class="m2"><p>خلعت همت بر دوش قناعت انداخت</p></div></div>