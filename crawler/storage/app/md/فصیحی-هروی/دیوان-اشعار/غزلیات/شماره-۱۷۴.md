---
title: >-
    شمارهٔ ۱۷۴
---
# شمارهٔ ۱۷۴

<div class="b" id="bn1"><div class="m1"><p>ما ز مادر داغ بر دل خاک بر سر زاده‌ایم</p></div>
<div class="m2"><p>همچو طفل غنچه در دامان نشتر زاده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نام ما مردود عالم روزی ما خون دل</p></div>
<div class="m2"><p>ما وغم گویا به یک طالع ز مادر زاده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شسته‌ایم از دل به خون عافیت نقش مراد</p></div>
<div class="m2"><p>ما همان دم کاندرین نه طاق ششدر زاده‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اشک یعقوبیم و از خون جگر جوشیده‌ایم</p></div>
<div class="m2"><p>آه مجنونیم و از دریای آذر زاده‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از سراب تلخکامی تشنه لب خواهیم مرد</p></div>
<div class="m2"><p>ما که همچون موج در دامان کوثر زاده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در گلستان که هر خارش پیمبر زاده‌ست</p></div>
<div class="m2"><p>میوه هیچیم ما وز نخل بی‌بر زاده‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شعله شوقیم و از دلهای خونین سر زده</p></div>
<div class="m2"><p>غیر پندارد فصیحی ما ز اخگر زاده‌ایم</p></div></div>