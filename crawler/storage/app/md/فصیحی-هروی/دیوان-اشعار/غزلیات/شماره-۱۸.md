---
title: >-
    شمارهٔ  ۱۸
---
# شمارهٔ  ۱۸

<div class="b" id="bn1"><div class="m1"><p>باز دل در موج تبخال از تب حرمان کیست</p></div>
<div class="m2"><p>لخت لختش در خروش از شعله هجران کیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان ما خود بال افشان از پی محمل برفت</p></div>
<div class="m2"><p>یا‌رب این مسکین که می‌سوزد فراقش جان کیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرگران گر بر مزار کشتگان بگذشت دوست</p></div>
<div class="m2"><p>بر لب زخم شهیدان نوحه پس قربان کیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست ما از دیده در هجران فزون بگریستی</p></div>
<div class="m2"><p>گر بدانستی که مسکین دور از دامان کیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سال‌ها شد کز فراق ما فصیحی پاک سوخت</p></div>
<div class="m2"><p>این نگاه خونچکان از دیده گریان کیست</p></div></div>