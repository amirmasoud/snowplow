---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>غمش به تازه ندانم چه مدعا دارد</p></div>
<div class="m2"><p>که فکر مرهم بهبود زخم ما دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چکیده دل دردست آسمان در کین</p></div>
<div class="m2"><p>به زخم ما چو رسد شربت دوا دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کدام صورت زشت اندرین بهشت آمد</p></div>
<div class="m2"><p>که باز آینه‌ام شکوه از صفا دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسوخت بلبل و خاکسترش ز شوق هنوز</p></div>
<div class="m2"><p>به بوی گل سرآمیزش صبا دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گسسته تار فصیحی ز هجر ناخن غم</p></div>
<div class="m2"><p>نهفته زیر لب شوق صد نوا دارد</p></div></div>