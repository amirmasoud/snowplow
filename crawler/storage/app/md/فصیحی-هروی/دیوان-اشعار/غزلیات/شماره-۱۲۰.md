---
title: >-
    شمارهٔ ۱۲۰
---
# شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>مراد ما ز مادر بر سر خشت عدم آید</p></div>
<div class="m2"><p>وجود عشرت ما با عدم از یک شکم آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر تسبیح سازد زاهد از خاک شهیدانش</p></div>
<div class="m2"><p>به جای نام ایزد بر زبانش یاصنم آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه عمرم به مردن صرف شد کاین جان غم روزی</p></div>
<div class="m2"><p>دلی صد ره برون از تن به استقبال غم آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فصیحی تشنه مرگ است چندانی که عیدستش</p></div>
<div class="m2"><p>چو ماه روزه گر از عمر او یک روز کم آید</p></div></div>