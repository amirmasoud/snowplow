---
title: >-
    شمارهٔ  ۱۱
---
# شمارهٔ  ۱۱

<div class="b" id="bn1"><div class="m1"><p>به صبوری بفریبم دل شیدایی را</p></div>
<div class="m2"><p>در جگر بند کنم ناله صحرایی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دم ز انکار محبت زدم و بد کردم</p></div>
<div class="m2"><p>نه که این باد بریزد گل رسوایی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باغبان خواست که حیران گل و خار شویم</p></div>
<div class="m2"><p>بست بر دیده ما راه شناسایی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آسمان ماتم خس خانه‌ خود‌گیر که سوخت</p></div>
<div class="m2"><p>دامن ناله ما دست شکیبایی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر نفس داغ دگر در جگرم زنده کند</p></div>
<div class="m2"><p>از که آموخته هجر تو مسیحایی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مست یکرنگی دردیم که از صحبت او</p></div>
<div class="m2"><p>نرسد هیچ خلل عصمت تنهایی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاک آن کوی فصیحی ز جبین رنجه مکن</p></div>
<div class="m2"><p>از مه و مهر بیاموز جبین‌سایی را</p></div></div>