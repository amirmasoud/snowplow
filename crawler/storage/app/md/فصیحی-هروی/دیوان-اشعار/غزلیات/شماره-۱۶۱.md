---
title: >-
    شمارهٔ ۱۶۱
---
# شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>دیریست که از گریه بهار دل ریشم</p></div>
<div class="m2"><p>شاداب‌تر از غنچه خون بر سر نیشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک قطره خونیم و ز فیض نظر عشق</p></div>
<div class="m2"><p>از حوصله دیده هر غمزه بیشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا سبحه و زنار نسوزیم چه دانند</p></div>
<div class="m2"><p>کآیین جنون چیست و مجنون چه کیشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم محمل عشقیم ولی در کشش شوق</p></div>
<div class="m2"><p>یک بانگ دراوار ازین قافله پیشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیگانه مباش این همه از ما که گواهست</p></div>
<div class="m2"><p>زخم جگر ریش که با تیغ تو خویشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر سونش الماس وگر مرهم عیسی</p></div>
<div class="m2"><p>گردیم همان از نظر افتاده ریشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاصیت معشوق گرفتیم فصیحی</p></div>
<div class="m2"><p>از عیب وفا ورنه چرا دشمن خویشم</p></div></div>