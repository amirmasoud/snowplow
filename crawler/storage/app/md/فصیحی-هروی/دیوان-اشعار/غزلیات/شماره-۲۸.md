---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>بر گوش دلم زمزمه توبه حرام است</p></div>
<div class="m2"><p>این گوش پرستار نوای لب جام است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صید تو به منقار وفا برکند از بال</p></div>
<div class="m2"><p>هر پرکه نه آن شیفته طره دام است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیهوده میفروز چنین دوزخ کین را</p></div>
<div class="m2"><p>کار جگر خسته به یک شعله تمام است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با قافله مصر ز یوسف اثری نیست</p></div>
<div class="m2"><p>ور هست شمیمی گل خودروی مشام است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن خسته شهیدست فصیحی که نداند</p></div>
<div class="m2"><p>الماس کدام و جگر ریش کدام‌ست</p></div></div>