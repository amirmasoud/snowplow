---
title: >-
    شمارهٔ  ۶
---
# شمارهٔ  ۶

<div class="b" id="bn1"><div class="m1"><p>سرمه کوری کشیدم دیده تحقیق را</p></div>
<div class="m2"><p>تا نبینم در لباس صدق هر زندیق را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاقبت از راه گمراهی برد سوی خودم</p></div>
<div class="m2"><p>آن که نپسندید بر من منت توفیق را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تشنه‌تر گشتم ز خون دل همانا شوق دوست</p></div>
<div class="m2"><p>چون سراب از تشنگی پر کرد این ابریق را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دیار عقل بیرون رو که در بازار او</p></div>
<div class="m2"><p>صیقلی زنگی شکست آیینه تصدیق را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق را نازم که چون با بی‌نیازی می‌کشد</p></div>
<div class="m2"><p>باده زندیق سازد خون صد صدیق را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آبروی این ریاکاران فصیحی وه که کرد</p></div>
<div class="m2"><p>غرق دریای خجالت کشتی توفیق را</p></div></div>