---
title: >-
    شمارهٔ ۱۴۳
---
# شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>ناله دردم و خوش بر سر کار آمده‌ام</p></div>
<div class="m2"><p>از دل چنگ کنون بر لب تار آمده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آهم از شعله من باغ دلی آب دهید</p></div>
<div class="m2"><p>که ز دریوزه جانهای فگار آمده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گریه‌ام کز جگر سوخته در دیده ابر</p></div>
<div class="m2"><p>بهر آرایش رخسار بهار آمده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نخل نومیدیم و میوه من سوختن‌ست</p></div>
<div class="m2"><p>اینک اندر جگر شعله به بار آمده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نی دل بلبلم و نی لب گل حیرانم</p></div>
<div class="m2"><p>که درین باغ فصیحی به چه کار‌ آ‌‌‌‌مده‌ام</p></div></div>