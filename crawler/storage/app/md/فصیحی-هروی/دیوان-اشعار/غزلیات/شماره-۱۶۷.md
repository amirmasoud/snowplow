---
title: >-
    شمارهٔ ۱۶۷
---
# شمارهٔ ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>عشق کو تا آتشی در خرمن اخگر زنم</p></div>
<div class="m2"><p>چتر شاهی بر سر اورنگ خاکستر زنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لعل شاهنشاهیم بر خاک عجزم افکنید</p></div>
<div class="m2"><p>تا دو روزی خنده بر رسوایی افسر زنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از لب زخم شهیدان طالعی گیرم به وام</p></div>
<div class="m2"><p>تا مگر بوس مرادی بر لب خنجر زنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سالها بودم سرشک و دیده‌ای نگداختم</p></div>
<div class="m2"><p>ناله‌ای گردم مگر آتش به گوشی در زنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آبروی عفو را بر خاک نتوان ریخت لیک</p></div>
<div class="m2"><p>حله‌ای از شعله‌پوشم غوطه در کوثر زنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در زدم یک عمر و زین نه دیر نامد یک جواب</p></div>
<div class="m2"><p>حلقه یک چندی فصیحی بر در دیگر زنم</p></div></div>