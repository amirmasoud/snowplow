---
title: >-
    شمارهٔ ۹۱
---
# شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>رفتند همدمان و مرا دل فگار ماند</p></div>
<div class="m2"><p>خون جگر ز صحبتشان یادگار ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل مضطرب ز روزن چشمم برون دوید</p></div>
<div class="m2"><p>اما ز ناتوانی هم در کنار ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دشمنم چه شکوه که این زخم دوست زد</p></div>
<div class="m2"><p>بیگانه را چه جرم که این داغ یار ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نی‌نی ز یار و دوست شکایت نه درخورست</p></div>
<div class="m2"><p>کاین داغ روزگار سیه‌روزگار ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر جا بود دو چشمه خون تربت من‌ست</p></div>
<div class="m2"><p>کاینم به جای لوح نشان مزار ماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پا تا سرم چو زلف بتان بیقرار شد</p></div>
<div class="m2"><p>الا غم فراق که بر یک قرار ماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جانم تمام سوخت فصیحی غم فراق</p></div>
<div class="m2"><p>وز روی مرگ تا ابدم شرمسار ماند</p></div></div>