---
title: >-
    شمارهٔ ۹۸
---
# شمارهٔ ۹۸

<div class="b" id="bn1"><div class="m1"><p>یک ناوک ارنه در دل صد پاره بشکند</p></div>
<div class="m2"><p>رنگ نفاق بر رخ سیاره بشکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر لحظه بشکند نفس از بار بی‌بری</p></div>
<div class="m2"><p>کاش این نهال بیهده یکباره بشکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز آنجا که فتنه‌جویی حسن ستیزه خوست</p></div>
<div class="m2"><p>مژگان ز باد دامن نظاره بشکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشقش کند به داروی بیچارگی درست</p></div>
<div class="m2"><p>گر شیشه‌ات ز کشمکش خاره بشکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر شب کنم ز درد نفس صاف و تن زنم</p></div>
<div class="m2"><p>ترسم ز ناله‌ام دل سیاره بشکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گویم شهید کیست فصیحی ولی مباد</p></div>
<div class="m2"><p>رنگ حیا در آن گل رخساره بشکند</p></div></div>