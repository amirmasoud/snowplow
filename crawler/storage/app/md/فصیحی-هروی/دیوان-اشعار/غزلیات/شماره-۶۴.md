---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>شب نخل تجلی به گلستان تو بر داد</p></div>
<div class="m2"><p>هر موی مرا ذوق تماشای دگر داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌خواست حیا بند نهد بر نگهم لیک</p></div>
<div class="m2"><p>شوق آمد و مژگان مرا بال نظر داد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در حسن چه شایسته رسولی که لبت را</p></div>
<div class="m2"><p>صد معجزه یزدان بجز از شق قمر داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب دیده به نظاره رخسار تو می‌رفت</p></div>
<div class="m2"><p>شمع نظری در کف هر لخت جگر داد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از همت عشقست که ما هیچکسان را</p></div>
<div class="m2"><p>صد کشور اندوه به یک آه سحر داد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی منت این ابر تنک مایه فصیحی</p></div>
<div class="m2"><p>بید چمن ما گل خورشید ثمر داد</p></div></div>