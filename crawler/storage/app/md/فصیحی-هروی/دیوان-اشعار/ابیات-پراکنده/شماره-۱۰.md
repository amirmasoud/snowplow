---
title: >-
    شمارهٔ  ۱۰
---
# شمارهٔ  ۱۰

<div class="b" id="bn1"><div class="m1"><p>خاکستر منصور تو را باز توان سوخت</p></div>
<div class="m2"><p>صد مرتبه زیباتر از آغاز توان سوخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نامرد حریفی‌ست شکیب ارنه در این بزم</p></div>
<div class="m2"><p>از شوق نیازی جگر ناز توان سوخت</p></div></div>