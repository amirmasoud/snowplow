---
title: >-
    شمارهٔ  ۷۷
---
# شمارهٔ  ۷۷

<div class="b" id="bn1"><div class="m1"><p>شهید عشق تو را راه کعبه مقصود</p></div>
<div class="m2"><p>کسی نشان ندهد جز سر بریده او</p></div></div>