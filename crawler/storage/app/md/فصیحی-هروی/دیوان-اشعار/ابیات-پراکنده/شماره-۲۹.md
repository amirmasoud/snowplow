---
title: >-
    شمارهٔ  ۲۹
---
# شمارهٔ  ۲۹

<div class="b" id="bn1"><div class="m1"><p>دهن زخم چو خندان شود افزون گردد</p></div>
<div class="m2"><p>یک دم ار شاد نشینم جگرم خون گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قامتی دیده‌ام امروز که بی‌منت فکر</p></div>
<div class="m2"><p>هر چه آید به زبانم همه موزون گردد</p></div></div>