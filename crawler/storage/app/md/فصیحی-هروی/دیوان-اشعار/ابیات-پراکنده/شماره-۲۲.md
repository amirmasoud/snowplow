---
title: >-
    شمارهٔ  ۲۲
---
# شمارهٔ  ۲۲

<div class="b" id="bn1"><div class="m1"><p>گر لذت داغ جگر اینست فصیحی</p></div>
<div class="m2"><p>افسوس که در هر سر مویم جگری نیست</p></div></div>