---
title: >-
    شمارهٔ  ۲
---
# شمارهٔ  ۲

<div class="b" id="bn1"><div class="m1"><p>ایزد جزای مستی من کی دهد مگر</p></div>
<div class="m2"><p>لب تشنه در شراب شعور افکند مرا</p></div></div>