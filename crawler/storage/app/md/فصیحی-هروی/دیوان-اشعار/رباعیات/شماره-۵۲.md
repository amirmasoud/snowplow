---
title: >-
    شمارهٔ  ۵۲
---
# شمارهٔ  ۵۲

<div class="b" id="bn1"><div class="m1"><p>ز آن پیش دلا که هجر زارت بکشد</p></div>
<div class="m2"><p>زنهار چنان کنی که یارت بکشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر وعده او ز سادگی دل ننهی</p></div>
<div class="m2"><p>کاری نکنی که انتظارت بکشد</p></div></div>