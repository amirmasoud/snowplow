---
title: >-
    شمارهٔ  ۲۰
---
# شمارهٔ  ۲۰

<div class="b" id="bn1"><div class="m1"><p>هر چند دلم ز درد خونریزتر است</p></div>
<div class="m2"><p>بر من دل تیغ آسمان تیزتر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کین دلم دلیر باشید که زنگ</p></div>
<div class="m2"><p>زآیینه‌ام از عکس سبک خیزتر است</p></div></div>