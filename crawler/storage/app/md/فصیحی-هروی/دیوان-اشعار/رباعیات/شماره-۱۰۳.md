---
title: >-
    شمارهٔ  ۱۰۳
---
# شمارهٔ  ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>ای طور ز شوق جلوه‌ات خانه به دوش</p></div>
<div class="m2"><p>پروانه پر سوخته حسن تو هوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی دیده ز رشک سویت آیم وز شوق</p></div>
<div class="m2"><p>چون مردمک دیده شوم آینه‌پوش</p></div></div>