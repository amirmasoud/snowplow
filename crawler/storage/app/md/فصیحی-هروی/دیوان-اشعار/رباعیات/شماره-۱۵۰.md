---
title: >-
    شمارهٔ  ۱۵۰
---
# شمارهٔ  ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>ماییم و دل شکسته از خود رسته</p></div>
<div class="m2"><p>پیمان وفا به هر دو عالم بسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مصر وفای ما دو چیزست که نیست</p></div>
<div class="m2"><p>پیمان شکسته و دل نشکسته</p></div></div>