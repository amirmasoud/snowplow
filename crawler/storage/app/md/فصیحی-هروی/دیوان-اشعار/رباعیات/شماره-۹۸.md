---
title: >-
    شمارهٔ  ۹۸
---
# شمارهٔ  ۹۸

<div class="b" id="bn1"><div class="m1"><p>ای کشته ز اشک شوق مژگان پرواز</p></div>
<div class="m2"><p>ایمن بادا شمع تو از سوز و گداز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای نرگس مست گریه مشکن دل ناز</p></div>
<div class="m2"><p>خون ریز ولی ز چشم مستان نیاز</p></div></div>