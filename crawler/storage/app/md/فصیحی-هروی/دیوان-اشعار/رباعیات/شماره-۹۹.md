---
title: >-
    شمارهٔ  ۹۹
---
# شمارهٔ  ۹۹

<div class="b" id="bn1"><div class="m1"><p>ای خلق تو فیض بخش بستان نیاز</p></div>
<div class="m2"><p>وز روی تو بشکفد گلستان نیاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا حسن چو خورشید جهان افروزد</p></div>
<div class="m2"><p>دوران تو باد و باد دوران نیاز</p></div></div>