---
title: >-
    شمارهٔ  ۱۲۴
---
# شمارهٔ  ۱۲۴

<div class="b" id="bn1"><div class="m1"><p>تا کی ز تو وعده ارمغانی گیرم</p></div>
<div class="m2"><p>وز وعده برات دو زبانی گیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفتم که برای انتظار از عقبی</p></div>
<div class="m2"><p>سرمایه عمر جاودانی گیرم</p></div></div>