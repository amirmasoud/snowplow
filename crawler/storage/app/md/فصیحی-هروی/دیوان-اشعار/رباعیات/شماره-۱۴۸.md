---
title: >-
    شمارهٔ  ۱۴۸
---
# شمارهٔ  ۱۴۸

<div class="b" id="bn1"><div class="m1"><p>آن شوخ که ماه راست زو نور جبین</p></div>
<div class="m2"><p>شد خانه آفتاب ازو خانه زین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا هست بنای خانه زین هرگز</p></div>
<div class="m2"><p>مهمان نشدستش آفتابی به ازین</p></div></div>