---
title: >-
    شمارهٔ  ۱۲۹
---
# شمارهٔ  ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>ما نقش هوس ز لوح هستی شستیم</p></div>
<div class="m2"><p>وز خون رخ آرزوپرستی شستیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما روز ازل نشان آسوده دلی</p></div>
<div class="m2"><p>از چهره به گریه‌های مستی شستیم</p></div></div>