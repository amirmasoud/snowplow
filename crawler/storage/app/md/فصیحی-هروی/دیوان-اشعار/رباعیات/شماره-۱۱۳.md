---
title: >-
    شمارهٔ  ۱۱۳
---
# شمارهٔ  ۱۱۳

<div class="b" id="bn1"><div class="m1"><p>گفتم رمزی با تو صریح و مبهم</p></div>
<div class="m2"><p>بشنو که کند گوش ترا رشک ارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر زنده شود دلت بگو شکر نعم</p></div>
<div class="m2"><p>ورنه تو اصم باش و مرا گیر ابکم</p></div></div>