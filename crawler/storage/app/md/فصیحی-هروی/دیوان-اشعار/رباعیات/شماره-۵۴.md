---
title: >-
    شمارهٔ  ۵۴
---
# شمارهٔ  ۵۴

<div class="b" id="bn1"><div class="m1"><p>آن غنچه که از لباس خود بیرون شد</p></div>
<div class="m2"><p>در کسوت دیگر شد و گویم چون شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان شیرین در تن خسرو آمد</p></div>
<div class="m2"><p>روح لیلی به قالب مجنون شد</p></div></div>