---
title: >-
    شمارهٔ  ۷۶
---
# شمارهٔ  ۷۶

<div class="b" id="bn1"><div class="m1"><p>شوخی که گلش بهار امید بود</p></div>
<div class="m2"><p>با تنباکوش الفت جاوید بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم عارض خورشید بپوشد از ناز</p></div>
<div class="m2"><p>ز آن ابر که خانه‌زاد خورشید بود</p></div></div>