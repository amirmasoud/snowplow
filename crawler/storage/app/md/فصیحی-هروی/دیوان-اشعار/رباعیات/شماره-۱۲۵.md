---
title: >-
    شمارهٔ  ۱۲۵
---
# شمارهٔ  ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>رفتم که ز زلفت خردی وام کنم</p></div>
<div class="m2"><p>دل را به فسون عافیت رام کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زآتشکده واسوزم و خلدش خوانم</p></div>
<div class="m2"><p>وز شعله برنجم و گلشن وام کنم</p></div></div>