---
title: >-
    شمارهٔ  ۴۲
---
# شمارهٔ  ۴۲

<div class="b" id="bn1"><div class="m1"><p>امشب که طرب از دل مهجور برفت</p></div>
<div class="m2"><p>نومید طبیب از سر رنجور برفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که تماشای رخ درد کنم</p></div>
<div class="m2"><p>ناگه ز چراغ زندگی نور برفت</p></div></div>