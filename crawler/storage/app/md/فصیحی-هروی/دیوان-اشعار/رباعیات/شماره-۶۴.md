---
title: >-
    شمارهٔ  ۶۴
---
# شمارهٔ  ۶۴

<div class="b" id="bn1"><div class="m1"><p>روزم ز فراق دود گلخن سازند</p></div>
<div class="m2"><p>در وصل شبم چراغ ایمن سازند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمعم که درین انجمن راز مرا</p></div>
<div class="m2"><p>هر صبح کشند و باز روشن سازند</p></div></div>