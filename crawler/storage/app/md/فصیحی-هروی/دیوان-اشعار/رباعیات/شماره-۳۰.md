---
title: >-
    شمارهٔ  ۳۰
---
# شمارهٔ  ۳۰

<div class="b" id="bn1"><div class="m1"><p>این نسخه که بر شیشه نادان سنگست</p></div>
<div class="m2"><p>بر جلوه او ساحت دانش تنگست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلدیست که صد رنگ بود هر برگش</p></div>
<div class="m2"><p>وین طرفه که چون درنگری یک رنگست</p></div></div>