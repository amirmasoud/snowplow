---
title: >-
    شمارهٔ  ۹۰
---
# شمارهٔ  ۹۰

<div class="b" id="bn1"><div class="m1"><p>دیریست که از سینه‌ام آهی ندمید</p></div>
<div class="m2"><p>زین مزرع غم خشک گیاهی ندمید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند که بی تو دیده را دادم آب</p></div>
<div class="m2"><p>زین شور زمین گل نگاهی ندمید</p></div></div>