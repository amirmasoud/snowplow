---
title: >-
    شمارهٔ  ۲۲
---
# شمارهٔ  ۲۲

<div class="b" id="bn1"><div class="m1"><p>ای شمع گرت ذوق غم آموختن است</p></div>
<div class="m2"><p>این سوختن تو عمر اندوختن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور ز آن که برای مجلس افروختن است</p></div>
<div class="m2"><p>صد بار نسوختن به از سوختن است</p></div></div>