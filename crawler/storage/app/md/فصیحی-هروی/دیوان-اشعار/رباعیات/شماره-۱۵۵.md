---
title: >-
    شمارهٔ  ۱۵۵
---
# شمارهٔ  ۱۵۵

<div class="b" id="bn1"><div class="m1"><p>دلاکی گشت از سرم موی زدای</p></div>
<div class="m2"><p>گردید ز اعجاز هنر سحر نمای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با استره ای که باد را نشکافد</p></div>
<div class="m2"><p>هر موی مرا شکافت تا ناخن پای</p></div></div>