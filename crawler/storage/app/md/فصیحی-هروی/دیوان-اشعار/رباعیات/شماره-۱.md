---
title: >-
    شمارهٔ  ۱
---
# شمارهٔ  ۱

<div class="b" id="bn1"><div class="m1"><p>در دیده مور اگر روم چون عنقا</p></div>
<div class="m2"><p>گم سازم از فراخی جا خود را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با این تن بالیده و این نشو نما</p></div>
<div class="m2"><p>زین سفله سحاب می‌کشم منتها</p></div></div>