---
title: >-
    شمارهٔ  ۱۰۶
---
# شمارهٔ  ۱۰۶

<div class="b" id="bn1"><div class="m1"><p>ای کرده به خونریز اسیران آهنگ</p></div>
<div class="m2"><p>بر خرمن صلح من مزن آتش جنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مپسند که از خون شهیدان غمت</p></div>
<div class="m2"><p>بر تیغ جفای تو شود گوهر رنگ</p></div></div>