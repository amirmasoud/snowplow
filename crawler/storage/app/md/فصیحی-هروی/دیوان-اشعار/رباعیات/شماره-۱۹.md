---
title: >-
    شمارهٔ  ۱۹
---
# شمارهٔ  ۱۹

<div class="b" id="bn1"><div class="m1"><p>دیشب ز دلم شعله آهی برخاست</p></div>
<div class="m2"><p>وز دود دلم روز سیاهی برخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو ابری و ما گیاه تشنه جگریم</p></div>
<div class="m2"><p>کی ابر به کینه گیاهی برخاست</p></div></div>