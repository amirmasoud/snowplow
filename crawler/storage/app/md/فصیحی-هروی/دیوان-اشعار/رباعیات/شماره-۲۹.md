---
title: >-
    شمارهٔ  ۲۹
---
# شمارهٔ  ۲۹

<div class="b" id="bn1"><div class="m1"><p>از مرگ گل حیات بی‌رنگترست</p></div>
<div class="m2"><p>این نغمه از آن نغمه کج آهنگترست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر من که چو مردمک به هیچم خرسند</p></div>
<div class="m2"><p>از چشم جهانیان جهان تنگترست</p></div></div>