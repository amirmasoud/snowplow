---
title: >-
    شمارهٔ  ۳
---
# شمارهٔ  ۳

<div class="b" id="bn1"><div class="m1"><p>دردم که بهار نیست گلزار مرا</p></div>
<div class="m2"><p>هجرم که علاج نیست بیمار مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیراهن صبرم که ز دست غم دوست</p></div>
<div class="m2"><p>چاکی شده سر نوشت هر تار مرا</p></div></div>