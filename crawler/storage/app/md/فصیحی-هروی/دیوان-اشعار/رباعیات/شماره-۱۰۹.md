---
title: >-
    شمارهٔ  ۱۰۹
---
# شمارهٔ  ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>گردون که نباشدش به بیداد بدل</p></div>
<div class="m2"><p>عالم شود ار لبالب از غم به مثل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز من دگری در خور غم کی یابد</p></div>
<div class="m2"><p>گردد مگرش دیده بینش احول</p></div></div>