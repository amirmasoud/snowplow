---
title: >-
    شمارهٔ  ۱۵۶
---
# شمارهٔ  ۱۵۶

<div class="b" id="bn1"><div class="m1"><p>ای از گل اشک گشته مژگان آرای</p></div>
<div class="m2"><p>رو گریه به من گذار و تو خنده سرای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور ذوق تماشای گرستن داری</p></div>
<div class="m2"><p>یک شب چو نگه به سیر مژگان من آی</p></div></div>