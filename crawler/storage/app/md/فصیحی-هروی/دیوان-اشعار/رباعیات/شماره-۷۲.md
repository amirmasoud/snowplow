---
title: >-
    شمارهٔ  ۷۲
---
# شمارهٔ  ۷۲

<div class="b" id="bn1"><div class="m1"><p>عشقت صنما دل مشوش خواهد</p></div>
<div class="m2"><p>از دیده به جای آب آتش خواهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گو غم شبخون آر بر آن بوالهوسی</p></div>
<div class="m2"><p>کو روی تو بیند و دل خوش خواهد</p></div></div>