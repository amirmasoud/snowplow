---
title: >-
    شمارهٔ  ۱۱۹
---
# شمارهٔ  ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>جانا به غم تو زندگانی کردم</p></div>
<div class="m2"><p>غمهای ترا همدم جانی کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا گرد من از کوی تو نتواند رفت</p></div>
<div class="m2"><p>جان در سر کار ناتوانی کردم</p></div></div>