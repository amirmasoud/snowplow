---
title: >-
    شمارهٔ  ۲۴
---
# شمارهٔ  ۲۴

<div class="b" id="bn1"><div class="m1"><p>امشب چمنت به آب و تابی شده است</p></div>
<div class="m2"><p>کز عکس رخت باده گلابی شده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زینگونه که یافت از جمال تو فروغ</p></div>
<div class="m2"><p>فرد است که ماه آفتابی شده است</p></div></div>