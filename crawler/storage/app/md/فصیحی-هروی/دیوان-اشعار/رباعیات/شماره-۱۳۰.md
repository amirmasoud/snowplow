---
title: >-
    شمارهٔ  ۱۳۰
---
# شمارهٔ  ۱۳۰

<div class="b" id="bn1"><div class="m1"><p>یک چند درین رسته پریشان گشتیم</p></div>
<div class="m2"><p>گفتیم گران شویم ارزان گشتیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در طالع ما کساد بازاری بود</p></div>
<div class="m2"><p>آیینه‌فروش شهر کوران گشتیم</p></div></div>