---
title: >-
    شمارهٔ  ۹۷
---
# شمارهٔ  ۹۷

<div class="b" id="bn1"><div class="m1"><p>ای کلک تو مشکبار چون طره حور</p></div>
<div class="m2"><p>چون چشم خرد دوات تو چشمه نور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بهر مرکبت فلک دوده گرفت</p></div>
<div class="m2"><p>ز آن شمع که افروخت قضا در شب طور</p></div></div>