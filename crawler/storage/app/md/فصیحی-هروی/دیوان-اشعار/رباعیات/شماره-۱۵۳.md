---
title: >-
    شمارهٔ  ۱۵۳
---
# شمارهٔ  ۱۵۳

<div class="b" id="bn1"><div class="m1"><p>بنشین و به طوف قفس از دام برو</p></div>
<div class="m2"><p>منزل منزل سوی دلارام برو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین عرصه که نقش قدم گمراهی‌ست</p></div>
<div class="m2"><p>یک گام فرا برو و بی‌گام برو</p></div></div>