---
title: >-
    شمارهٔ  ۸۹
---
# شمارهٔ  ۸۹

<div class="b" id="bn1"><div class="m1"><p>گر در تو چمن طراز کنعان می‌دید</p></div>
<div class="m2"><p>رنگ گل حسن را به سامان می‌دید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیدار تو آفرید در دیده نگاه</p></div>
<div class="m2"><p>گویی همه زین پیش به مژگان می‌دید</p></div></div>