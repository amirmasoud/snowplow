---
title: >-
    شمارهٔ  ۳۱
---
# شمارهٔ  ۳۱

<div class="b" id="bn1"><div class="m1"><p>این روح که شمع مجلس افروز منست</p></div>
<div class="m2"><p>فرداست که نه زآن تو نه زآن منست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طاسی‌ست به گرمابه عالم این عمر</p></div>
<div class="m2"><p>پر ساختنش بهر تهی ساختنست</p></div></div>