---
title: >-
    شمارهٔ  ۵۸
---
# شمارهٔ  ۵۸

<div class="b" id="bn1"><div class="m1"><p>ای آنکه دمت راز مسیحا داند</p></div>
<div class="m2"><p>کی رنجه شدن از تو دل ما داند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دیده تو نوری و نرنجد از نور</p></div>
<div class="m2"><p>آن دیده که لذت تماشا داند</p></div></div>