---
title: >-
    شمارهٔ  ۱۲۲
---
# شمارهٔ  ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>در دیده ز اشک نوبهاری دارم</p></div>
<div class="m2"><p>در سینه ز داغ لاله‌زاری دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لب پر شکر از ناله زاری دارم</p></div>
<div class="m2"><p>در آینه با خود سروکاری دارم</p></div></div>