---
title: >-
    شمارهٔ  ۱۰۷
---
# شمارهٔ  ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>با آن که ز جوش حسن آن دلبر شنگ</p></div>
<div class="m2"><p>بر شاهد آفتاب شد میدان تنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنشسته فسردگان این معرکه را</p></div>
<div class="m2"><p>در آینه دیده نگه همچون رنگ</p></div></div>