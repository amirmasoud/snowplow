---
title: >-
    شمارهٔ  ۱۴۷
---
# شمارهٔ  ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>ای واله طور جلوه نور ببین</p></div>
<div class="m2"><p>عالم عالم ز نور معمور ببین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آشوب صدای «لن ترانی» است ز طور</p></div>
<div class="m2"><p>بگذر از طور و نور بی طور ببین</p></div></div>