---
title: >-
    شمارهٔ  ۴۸
---
# شمارهٔ  ۴۸

<div class="b" id="bn1"><div class="m1"><p>آهم چو بر آسمان شبیخون آرد</p></div>
<div class="m2"><p>بر هر مویم فغان شبیخون آرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر ناوک ناله‌ای که لب بگشاید</p></div>
<div class="m2"><p>بر گردد و بر کمان شبیخون آرد</p></div></div>