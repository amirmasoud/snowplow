---
title: >-
    شمارهٔ  ۱۰۴
---
# شمارهٔ  ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>ای کرده سپهر در حریم تو رکوع</p></div>
<div class="m2"><p>غم نیست اگر کوکب تو کرد رجوع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید سپهر دولتی و خورشید</p></div>
<div class="m2"><p>گر شام فرو رود کند صبح طلوع</p></div></div>