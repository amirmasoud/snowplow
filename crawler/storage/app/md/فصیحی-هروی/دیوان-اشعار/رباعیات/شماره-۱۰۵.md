---
title: >-
    شمارهٔ  ۱۰۵
---
# شمارهٔ  ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>ای روی ترا ترجمه در دین مصحف</p></div>
<div class="m2"><p>وز خال و خطت یافته تزیین مصحف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک نقطه سهو در همه روی تو نیست</p></div>
<div class="m2"><p>گویی به خط مصنف است این مصحف</p></div></div>