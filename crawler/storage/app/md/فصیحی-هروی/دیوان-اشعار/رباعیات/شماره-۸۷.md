---
title: >-
    شمارهٔ  ۸۷
---
# شمارهٔ  ۸۷

<div class="b" id="bn1"><div class="m1"><p>دل کشته خنجر ملامت گردید</p></div>
<div class="m2"><p>آواره کشور سلامت گردید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر غنچه که در گلبن امید دمید</p></div>
<div class="m2"><p>نشکفته هنوز داغ حسرت گردید</p></div></div>