---
title: >-
    شمارهٔ  ۶۶
---
# شمارهٔ  ۶۶

<div class="b" id="bn1"><div class="m1"><p>در ساغر عیش باده خامان ریزند</p></div>
<div class="m2"><p>عشاق ز دیده خون به دامان ریزند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌درد کجا ذوق محبت ز کجا</p></div>
<div class="m2"><p>این شهد به کام تلخکامان ریزند</p></div></div>