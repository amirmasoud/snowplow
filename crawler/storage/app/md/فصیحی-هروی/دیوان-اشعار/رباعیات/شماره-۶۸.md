---
title: >-
    شمارهٔ  ۶۸
---
# شمارهٔ  ۶۸

<div class="b" id="bn1"><div class="m1"><p>نوروز چو خان به بخت فیروز کند</p></div>
<div class="m2"><p>دولت عید مراد آن روز کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر روز که در دولت او کهنه شود</p></div>
<div class="m2"><p>آن را به شگون زمانه نوروز کند</p></div></div>