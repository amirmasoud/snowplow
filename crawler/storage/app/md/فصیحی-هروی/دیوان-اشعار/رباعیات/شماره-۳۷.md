---
title: >-
    شمارهٔ  ۳۷
---
# شمارهٔ  ۳۷

<div class="b" id="bn1"><div class="m1"><p>دور از تو دلم چو سینه آهستانی‌ست</p></div>
<div class="m2"><p>آتشکده‌ای کنون گیاهستانی‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر ذره ز کوی تو چو ماهستانی‌ست</p></div>
<div class="m2"><p>هر لختم چون دیده نگاهستانی‌ست</p></div></div>