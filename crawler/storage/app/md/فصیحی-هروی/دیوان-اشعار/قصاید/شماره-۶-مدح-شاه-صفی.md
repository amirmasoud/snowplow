---
title: >-
    شمارهٔ  ۶ - مدح شاه صفی
---
# شمارهٔ  ۶ - مدح شاه صفی

<div class="b" id="bn1"><div class="m1"><p>ز پرده ساز جهان‌ نوا برخاست</p></div>
<div class="m2"><p>که فر شاه صفی تخت و تاج را آراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دهان سکه چو نامش گرفت پرزر شد</p></div>
<div class="m2"><p>زبان خطبه ثنایش سرود کامرواست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به نیم موجه دلش آز را کریم کند</p></div>
<div class="m2"><p>سپهر الحق دریادلی چنین می‌خواست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرض ز ملک طبیعت گریخت از بیمش</p></div>
<div class="m2"><p>همین عقیم به دورش سپهر حادثه‌زاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهار حلقش عطری فشاند بر عالم</p></div>
<div class="m2"><p>که هر کجا خس و خاریست همچو گل بویاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکفت در جگر لاله نیز غنچه داغ</p></div>
<div class="m2"><p>اگر سیاه‌دلی نشکفد گل سوداست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو اوست قبله شاهان زبان ثنایش گفت</p></div>
<div class="m2"><p>به راستی که زبانم به کام قبله‌نماست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ستم به عهدش همچون دفینه در خاکست</p></div>
<div class="m2"><p>جهان به دستش همچون سفینه بر دریاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شهنشها تو جوان بخت و دولت تو جوان</p></div>
<div class="m2"><p>به عیش کوش که وقت جوانی دنیاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تراست غره سال شباب و می‌دانم</p></div>
<div class="m2"><p>که ملک دولت و دین را بهار نشو و نماست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به ذوق عهد تو عمر گذشته برگردد</p></div>
<div class="m2"><p>به کشوری که تویی در حساب دی فرداست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سپهر را تو به غایت عزیز مهمانی</p></div>
<div class="m2"><p>که بیشتر ز چهل سال خانه می‌آراست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز گونه گونه نعم هر چه بود در عالم</p></div>
<div class="m2"><p>برای مصلحت دولت تو می‌پیراست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کنون که آمده‌ای میزبان عالم باش</p></div>
<div class="m2"><p>که نعمت خوش خوان سپهر عدل و سخاست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شها چو جد تو آن شاه آسمان خرگاه</p></div>
<div class="m2"><p>به عزم سیر بهشت از سر جهان برخاست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جهان چو مردم چشم بتان سیه پوشید</p></div>
<div class="m2"><p>سپهر چون مژه صفهای فتنه می‌آراست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به نیم لحظه تو گفتی که آفتاب منیر</p></div>
<div class="m2"><p>خمیر مایه شام و شب غم و یلداست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز باد حادثه بنشست آسمان به زمین</p></div>
<div class="m2"><p>زموج اشک زمین همچو آسمان برخاست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز بس رمیدی از سایه شخص می‌دیدند</p></div>
<div class="m2"><p>که سایه کسی از کس هزار گام جداست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جهانیان را شد سر این سخن روشن</p></div>
<div class="m2"><p>که سایه در ره ناامن دشمنی ز قفاست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که ناگه از افق غیب صبح دولت تو</p></div>
<div class="m2"><p>طلوع (کرد و) جهان را به عدل و داد آراست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هنوز فتنه خوابیده چشم می‌مالید</p></div>
<div class="m2"><p>که تیغ فتنه نشان تو کرد قامت راست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کنون ز عدل تو عالم چنان شد امن آباد</p></div>
<div class="m2"><p>که سرخ رویی شمع از طپانچه‌های صباست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همیشه تا گل نوروز بر سر سالست</p></div>
<div class="m2"><p>مدام تا ز سحاب آبروی نشو و نماست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ترا به سیر گل اقبال سایه‌گستر باد</p></div>
<div class="m2"><p>کز آن شمیم نسیم وجود غالیه‌ساست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از آن زمان که دعای تو گشت ما را ورد</p></div>
<div class="m2"><p>کلیدداری درهای آسمان با ماست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ترا حیات طبیعی دهد خدای جهان</p></div>
<div class="m2"><p>سخا و جود تو بر طول مدت تو گواست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر حیات ابد نیز آرزو داری</p></div>
<div class="m2"><p>به عدل کوش که آب خضر درین صحراست</p></div></div>