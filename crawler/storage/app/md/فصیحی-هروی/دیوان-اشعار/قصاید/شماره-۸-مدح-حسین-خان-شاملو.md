---
title: >-
    شمارهٔ  ۸ - مدح حسین خان شاملو
---
# شمارهٔ  ۸ - مدح حسین خان شاملو

<div class="b" id="bn1"><div class="m1"><p>ساقی بیار باده که نوروز اکبرست</p></div>
<div class="m2"><p>رنگ بهار تازه‌تر از روی دلبرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند کیمیای چمن خنده گل‌ست</p></div>
<div class="m2"><p>مگذار داغ لاله که کبریت احمرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر قطره خون که از نفس بلبلان چکد</p></div>
<div class="m2"><p>بر خاک نا‌رسیده گلی تازه و ترست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حسن بهار در همه جا هست دلگشا</p></div>
<div class="m2"><p>در گلشن هرات ولی دلگشاترست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>الحق چه کشورست که از شرم ساحتش</p></div>
<div class="m2"><p>پنهان بهشت در پس دیوار محشرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون در نظر در آید روح مجسم‌ست</p></div>
<div class="m2"><p>چون در ضمیر آید عقل مصورست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاکش چنان لطیف که نقش قدم بر آن</p></div>
<div class="m2"><p>گویی چو موجه است که بر روی کوثرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حسنش مگر ز چاه ذقن داده است آب</p></div>
<div class="m2"><p>کش نیش خار غیرت مژگان دلبرست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از بس ز فیض عصمت حسنش سر شته‌اند</p></div>
<div class="m2"><p>ز آمیزش نسیم غبارش مکدرست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آیینه‌وار بر در و دیوارش از صفا</p></div>
<div class="m2"><p>چون بنگری معاینه عکست مصورست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نخل همیشه خشک فغان کش ثمر نبود</p></div>
<div class="m2"><p>آنجا ز میوه اثرش شاخ پربرست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سرو از لطافتی که در آب و هوای اوست</p></div>
<div class="m2"><p>بی سایه گشت در چمن اکنون پیمبرست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرنه پیمبرست چرا در حریم او</p></div>
<div class="m2"><p>قمری زبان وحی گشاده نو اگرست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صد نکته گفته بود جهان در ثنای او</p></div>
<div class="m2"><p>اکنون ز لطف خان همه چون سکه بر زرست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مهر سپهر مجد و معالی حسین خان</p></div>
<div class="m2"><p>انکو چو مهر مایه‌ده هفت کشورست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنجا که رای اوست خرد طفل مکتب است</p></div>
<div class="m2"><p>و آنجا که قدر اوست فلک حلقه بر درست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>و آنجا که جز بر آن خردم خواندی ثنا</p></div>
<div class="m2"><p>نخل نفس چو بید تهی شاخ از برست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>و آنجا که در مدایح او گویدی سخن</p></div>
<div class="m2"><p>هر لفظ گنج‌نامه صد گنج دیگرست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر از سبک عنانی عزمش رقم کند</p></div>
<div class="m2"><p>هر حرف چون دعای سحر آتشین پرست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ور از گران رکابی حلمش سخن کنم</p></div>
<div class="m2"><p>باد نفس به کشتی الفاظ لنگرست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اقبال بین که خطبه او دین مختلف</p></div>
<div class="m2"><p>کاندر میانشان سخن صلح خنجرست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر یک فراز منبر خواندند هر دو قدم</p></div>
<div class="m2"><p>در یک زمان ولی به میان شور بی شرست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>روزی که از دو سو س۱ه کینه صف کشد</p></div>
<div class="m2"><p>گویی که رستخیز جهان را دو محشرست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حال زمین ز زمزمه حمله آن زمان</p></div>
<div class="m2"><p>چون برگ کاه[و] سیلی بیداد صر صرست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در لجه عروق دلیران ز جوش کین</p></div>
<div class="m2"><p>هر موج در شکنجه صد موج دیگرست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مردان رکاب عزم تو بوسند فتح‌وار</p></div>
<div class="m2"><p>آری همیشه فتح ازین در مظفرست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون رخصت نبرد دهی حمله آورند</p></div>
<div class="m2"><p>گرم آن چنان که شیوه طوفان آذرست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اقبال حضرتش پی انجام کار فتح</p></div>
<div class="m2"><p>در لجه‌های خون چو نهنگی شناورست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تو چون عنان عزم به جنبش در آوری</p></div>
<div class="m2"><p>تکبیر فتحت از لب جبریل در خورست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دریای کین به ناله رود از نهیب تو</p></div>
<div class="m2"><p>وان ناله هم به خاصیت الله اکبرست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گیری چو ملک را ز عدو هم بدو دهی</p></div>
<div class="m2"><p>کاین مکرمت به ملت تو فتح دیگرست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>با آنکه در حسام تو در شرع انتقام</p></div>
<div class="m2"><p>خون عو حلال‌تر از شیر مادرست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خانا سپهر مکرمتا بحر مشربا</p></div>
<div class="m2"><p>ای آنکه مدحت تو ز اندیشه برترست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>من طفل برنخورده ز شیر مروتم</p></div>
<div class="m2"><p>با آنکه دایه‌ام همه دم چار مادرست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گردون غبار فتنه فرو ریخت بر سرم</p></div>
<div class="m2"><p>اکنون همان غبار مرا زیب افسرست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ریش‌ست از سپهر مرا بر دل فگار</p></div>
<div class="m2"><p>کش در علاج اگر همه عیب‌ست مضطرست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نیشی بیازمای برین ریش از کرم</p></div>
<div class="m2"><p>کاین کهنه ریش قابل احسان نشترست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>افسانه‌ام ملال فزاید ز جوش حزن</p></div>
<div class="m2"><p>مشنو که هم به گوش من این قصه درخورست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شکرانه عطیه نوروز عفو کن</p></div>
<div class="m2"><p>جرم مرا که توشه یک دوزخ آذرست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گیرم عظیم‌تر بود از کوه جرم من</p></div>
<div class="m2"><p>لیکن به نزد عفو تو کاهی محقرست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تو آفتاب اوج سپهر مروتی</p></div>
<div class="m2"><p>بی‌اختیار تربیتت ذره پرورست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تا هست رسم شادی نوروز هر بهار</p></div>
<div class="m2"><p>رو شاد زی که حضرت یزدانت یاورست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نوروز و نو‌بهار پرستار این درند</p></div>
<div class="m2"><p>آری پناه مردم بیچاره این درست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بادا ریاض عمر تو هر روز تازه‌تر</p></div>
<div class="m2"><p>تا رسم روز درشکن چرخ اخضرست</p></div></div>