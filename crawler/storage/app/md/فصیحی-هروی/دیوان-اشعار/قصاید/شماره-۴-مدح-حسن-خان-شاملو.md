---
title: >-
    شمارهٔ  ۴ - مدح حسن خان شاملو
---
# شمارهٔ  ۴ - مدح حسن خان شاملو

<div class="b" id="bn1"><div class="m1"><p>ابر آمد و گلزار ارم ساخت جهان را</p></div>
<div class="m2"><p>چون روی گل آراست زمین را و زمان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد سبز در و دشت بدان‌سان که نسیمی</p></div>
<div class="m2"><p>چون سبز روان سبز کند چوب شبان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر جا که نهی پا به زمین ریشه دواند</p></div>
<div class="m2"><p>زینست که پا مانده به گل سرو روان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بس هوس سیر قرار از همه کس برد</p></div>
<div class="m2"><p>در دل نتوان داشت نگه راز نهان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد ریشه غم سست به نوعی که برد باد</p></div>
<div class="m2"><p>چون نکهت گل داغ دل لاله‌ستان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از حسن فریبندگی باغ عجب نیست</p></div>
<div class="m2"><p>کز عکس گلش بند نهد آب روان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون عکس که در آب نماید ز لطافت</p></div>
<div class="m2"><p>در شخص توان دید عیان صورت جان را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آینه یوسف شد و هر برگ جلا داد</p></div>
<div class="m2"><p>چون دیده یعقوب زمین را و زمان را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از عین لطافت در و دیوار گلستان</p></div>
<div class="m2"><p>عینک شده نظارگی دل نگران را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از روشنی عارض گل هیچ عجب نیست</p></div>
<div class="m2"><p>گر در دل بلبل بشمارند فغان را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من هم به نوایی دل حیرت بگشایم</p></div>
<div class="m2"><p>مرغان چو گشودند لب نغمه‌فشان را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر نغمه ندارم قدری ناله‌فشانم</p></div>
<div class="m2"><p>داغی به سر داغ نهم لاله‌ستان را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از مطلع دیگر چمنی سازم و آرم</p></div>
<div class="m2"><p>آنجا بدل آب روان اشک روان را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کو بخت که بر سنگ زنم شیشه جان را</p></div>
<div class="m2"><p>بر دوش اجل افکنم این بار گران را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در تیرگی بخت سیه‌روز گریزم</p></div>
<div class="m2"><p>بی‌سایه کنم این تن بی‌تاب و توان را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون شمع دهم جان به شبیخون نسیمی</p></div>
<div class="m2"><p>بر من شب غم کرد ز بس تیره جهان را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز آسیب نفس پیکرم از ضعف بپژمرد</p></div>
<div class="m2"><p>مهتاب چه حاجت بود این کهنه کتان را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ریزد ز گلم رنگ به تحریک نسیمی</p></div>
<div class="m2"><p>زحمت ندهد گلشن من باد خزان را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>داغ جگرم شوخ‌مزاجست درین فصل</p></div>
<div class="m2"><p>چون لاله برون افکنم این راز نهان را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پاس دل خود دارکه کس را غم کس نیست</p></div>
<div class="m2"><p>صلح‌ست درین بادیه با گرگ شبان را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفتم که بهار آمد و از فیض تماشا</p></div>
<div class="m2"><p>گلزار کنم دیده خونابه‌فشان را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>غافل که به رغمم کند این چرخ مشعبد</p></div>
<div class="m2"><p>در طبع هوا تعبیه آسیب خزان را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هشدار فصیحی که سر رشته شد از دست</p></div>
<div class="m2"><p>این پرده مخالف بود آهنگ بتان را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو بلبل قدسی نفس مدحت گل گوی</p></div>
<div class="m2"><p>از زهد مکن رنجه لب فاتحه‌خوان را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نوروز و چمن خرم و گل مست می ناز</p></div>
<div class="m2"><p>آراسته از جلوه کران تا به کران را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از لطف هوا در تتق شاخ توان دید</p></div>
<div class="m2"><p>چون عکس در آیینه گل لعل‌فشان را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گل روی نگارست که جان داده چمن را</p></div>
<div class="m2"><p>یا دولت خانست که نو کرده جهان را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شاداب گل گلشن اقبال حسن خان</p></div>
<div class="m2"><p>ای قدر تو آراسته اورنگ کیان را</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از تازگی گلشن خلقت گل سوری</p></div>
<div class="m2"><p>خونابه‌فشان کرد لب غنچه نشان را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تب کرد شب از رشک تو چون شمع سحر مرد</p></div>
<div class="m2"><p>مرغان بگشودند لب مرثیه‌خوان را</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ظالم شده از بس که به دوران تو مظلوم</p></div>
<div class="m2"><p>آهوبره غمخواره بود شیر ژیان را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شمشیر عدو مرده تابوت نیامست</p></div>
<div class="m2"><p>بگرفت مگر خاصیت تیغ زبان را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دستان زن قهر تو به یک مالش گوشی</p></div>
<div class="m2"><p>آهنگ کند ساز کج آهنگ جهان را</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ور زانکه به دلخواه وی آهنگ نگردد</p></div>
<div class="m2"><p>بیرون کند از ساختش اوتار زیان را</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خورشید ثنای تو ز بس شوخ مزاجست</p></div>
<div class="m2"><p>منزل نکند نیم‌نفس هیچ مکان را</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هر لحظه شود جلوه‌گر از مطلع دیگر</p></div>
<div class="m2"><p>وز نور دهد غوطه زمین را و زمان را</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شاداب کند ابر ثنای تو بیان را</p></div>
<div class="m2"><p>صد برگ کند غنچه بی برگ دهان را</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از بس که دهنهاست پر از مدح تو یابند</p></div>
<div class="m2"><p>چون برگ‌شکن یافته در غنچه زبان را</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کلکم قدری شهد ثنایت به کف آورد</p></div>
<div class="m2"><p>انباشت از آن مغز بیان را و بنان را</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اکنون شکرستان شده هر صفحه شعرم</p></div>
<div class="m2"><p>از بس که مکیده‌ست گه این را و گه آن را</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اعدای نگون‌بخت ترا با تو بسنجد</p></div>
<div class="m2"><p>آن کس که یکی دید نگون را و ستان را</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شایستگی عفو تو مجرم ز گنه یافت</p></div>
<div class="m2"><p>آری ز کجی راست شود کار کمان را</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دزد اجل از بیم سیاستگر قهرت</p></div>
<div class="m2"><p>واداد به اموات جهان جوهر جان را</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تا فیض سبکروحی عزم تو برون داد</p></div>
<div class="m2"><p>از دیده مستان عدم خواب گران را</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چون شاهد طناز زبان تو به خوبی</p></div>
<div class="m2"><p>شیدایی خود ساخت جهان گذران را</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>از جوهر علم تو جهان ساخت طلسمی</p></div>
<div class="m2"><p>چون نقش قدم کرد زمین‌گیر زمان را</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مرغ سخنم از هوس باغ ثنایت</p></div>
<div class="m2"><p>در بیضه پر و بال گشاید طیران را</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گردون منشا جم روشا عرش جنابا!</p></div>
<div class="m2"><p>ای فرض ثنای تو لب‌کون و مکان را</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>در ظرف ثنا رای شگرف تو نگنجد</p></div>
<div class="m2"><p>زیبد تن خورشید مراین جوهر جان را</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>در عقل نگنجی که مدیح تو سرایم</p></div>
<div class="m2"><p>این مختصر الفاظ ثنایی‌ست گمان را</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>شخص خردم گرچه حرم‌زاده قدسی‌ست</p></div>
<div class="m2"><p>آراست درین رسته به مدح تو دکان را</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>نشنوده خوش‌آوازه‌تر از نغمه مدحت</p></div>
<div class="m2"><p>بر تار نفس تا زده مضراب زبان را</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گفتم که پس از مدح ثنای تو طرازم</p></div>
<div class="m2"><p>ز آن سو که بود رسم لب قاعده‌دان را</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ناگه خردم از در اندیشه درآمد</p></div>
<div class="m2"><p>زد بانگ که در پیچ ازین راه عنان را</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>با این نفس سرد دعای تو چنانست</p></div>
<div class="m2"><p>کز چشمه مهتاب بشویند کتان را</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ورد ملک آیات دعایش شده تا ساخت</p></div>
<div class="m2"><p>خمیازه‌کش خاک هری باغ جنان را</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>اقبال برد از ره او گرد حوادث</p></div>
<div class="m2"><p>زآن‌سان که برد باد یقین خاک گمان را</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ور پند منت نیست پسندیده دعا کن</p></div>
<div class="m2"><p>اما نه بدان‌گونه که بهمان و فلان را</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>او فیض رسانست همی گوی و دگر هیچ</p></div>
<div class="m2"><p>یارب تو نگه‌دار مر این فیض رسان را</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>تا نام و نشانست به نوروز ز گیتی</p></div>
<div class="m2"><p>هر روز تو نوروز طرب باد جهان را</p></div></div>