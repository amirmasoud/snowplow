---
title: >-
    شمارهٔ  ۱ - در منقبت مولای منقیان حضرت علی علیه‌السلام
---
# شمارهٔ  ۱ - در منقبت مولای منقیان حضرت علی علیه‌السلام

<div class="b" id="bn1"><div class="m1"><p>رخش سفر بر جهان زین در دار فنا</p></div>
<div class="m2"><p>خیمه عزلت بزن بر در ملک بقا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخت بکش زین دیار هین که ازین تنگنا</p></div>
<div class="m2"><p>یوسف جان رفتنی است جانب مصر بقا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خنده بران از لب و نوحه دل گوش کن</p></div>
<div class="m2"><p>خیمه عشرت مزن بر در ماتم‌سرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تخم تعلق مپاش در گل این خاکدان</p></div>
<div class="m2"><p>تا نکنندت دواب طعمه خود چون گیا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سگ‌صفتان جهان قصد شکارت کنند</p></div>
<div class="m2"><p>گر همه از استخوان طعمه کنی چون هما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگسل ازین آب وگل چند کنی پایمال</p></div>
<div class="m2"><p>گوهر دل را که هست هر دو جهانش بها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جرعه آب حیات در گلوی خر مریز</p></div>
<div class="m2"><p>مروحه سگ مکن شهپر جبریل را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گوهر مقصود عشق در کف تو کی نهند</p></div>
<div class="m2"><p>تا نکنی همچو دل در شط خون آشنا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زآینه هستیت زنگ محبت زدود</p></div>
<div class="m2"><p>ورنه گل و روشنی جوهر خاک و صفا!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی سر و پا کن چو چرخ طی بیابان عشق</p></div>
<div class="m2"><p>تا شودت جیب دل مطلع صبح صفا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می‌رودم همچون خون شعله همی در عروق</p></div>
<div class="m2"><p>دایه عشقم مگر داده ز آتش غذا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>درد تو چون پا نهد بر سر بالین دلم</p></div>
<div class="m2"><p>از در دل تا به لب جوش زند مرحبا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>می‌روم از کوی دوست با جگری لخت‌لخت</p></div>
<div class="m2"><p>دیده چو دل غرق خون دل همه رنج و عنا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رحمی رحمی اجل بر دل این ناامید</p></div>
<div class="m2"><p>کز سر عنفش طبیب رانده ز دارالشفا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شربت دردت حلال باد بر آن کو خورد</p></div>
<div class="m2"><p>همچو عدوی امام از جگر خود غذا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدر امامت علی نور سمی کلیم</p></div>
<div class="m2"><p>آنکه بر ایمن فشاند از ید بیضا ضیا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دهر نگستردی از خوان وجود ترا</p></div>
<div class="m2"><p>معده هستی تهی ماندی همی از غذا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خوان کرم چون نهند مطبخیانت به بزم</p></div>
<div class="m2"><p>حرص سراپا شکم میرد از امتلا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کوس ظفر چون زنند نوبتیانت به رزم</p></div>
<div class="m2"><p>گیرد از تیغ تو خصم دعای وبا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کشتی تن بشکند موج محیط نبرد</p></div>
<div class="m2"><p>جان به کنار افکند موجه بحر دغا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نعره‌زنان چون سمند جلوه دهی هیبتت</p></div>
<div class="m2"><p>جوشن هستی درد بر تن شخص بقا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وه چه سمندی که گر زان سوی ملک ازل</p></div>
<div class="m2"><p>همره پیک گمان پای گشاید ز پا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آن نشکسته هنوز بیعت خاطر که این</p></div>
<div class="m2"><p>ملک ابد را کشد زیر سم انتها</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ور ز دیار فنا شاهسوار عدم</p></div>
<div class="m2"><p>آوردش زیر ران روی سوی ملک بقا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون بگشاید ز هم گام نخست افکند</p></div>
<div class="m2"><p>بر سر دوش قدم غاشیه ابتدا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تیغ چو پر گل کند دامن گردان شود</p></div>
<div class="m2"><p>روح به باغ عدم بلبل دستان‌سرا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون ز تگاور شوند همچو صراحی نگون</p></div>
<div class="m2"><p>پر شود از خون رزم جام امل مرگ را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شخص حسامت که هست از غم دشمن خراب</p></div>
<div class="m2"><p>چون دم هیجا دهیش از دل اعدا غذا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چندان خون قی کند کاندر میدان رزم</p></div>
<div class="m2"><p>خیل نهنگان کنند در شط خون آشنا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گرم کند بزم رزم زمزمه گیر و دار</p></div>
<div class="m2"><p>دور نخستین برد هوش ز مغز بقا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ملک چو آیینه‌ایست در وسط خصم و تو</p></div>
<div class="m2"><p>نزد عدو تیره‌روی پیش تو گیتی نما</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تیغ ترا گر به فرض آینه سازد عدو</p></div>
<div class="m2"><p>بیند تمثال خویش گشته سر از تن جدا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پیکر تمثال را روح طبیعی دهد</p></div>
<div class="m2"><p>از نفس پاکت ار آینه گیرد جلا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>طفل نبات ار به فرض شیر ولایت خورد</p></div>
<div class="m2"><p>گیرد طبع نسیم خاصیت کهربا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خلق تو گر چون خلیل پای بر آتش نهد</p></div>
<div class="m2"><p>شعله گلستان شود در دهن اژدها</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بس که به عهد تو شد دهر سراپا نشاط</p></div>
<div class="m2"><p>عیش تصور کند آینه‌بین عکس را</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>یا سندالاتقیا رشحه فیضی که سوخت</p></div>
<div class="m2"><p>مزرعه هستیم برق سموم جفا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ابر کرم را بگوی سوی فصیحی خرام</p></div>
<div class="m2"><p>مزرع اخلاص اوست تشنه‌لب از مدعا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کوری چشم حسود گو به قضا تا کند</p></div>
<div class="m2"><p>خاک درت را چو نور در نظرم توتیا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تا بود این امهات از مدد نه پدر</p></div>
<div class="m2"><p>بر سر خشت وجود شام و سحر فتنه‌زا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از شر این هفت و چار باد اسیر ترا</p></div>
<div class="m2"><p>کعبه جاهت پناه درگه تو ملتجا</p></div></div>