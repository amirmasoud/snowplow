---
title: >-
    بخش ۲ - در ذات و صفات و توحید حضرت باری تعالی فرماید
---
# بخش ۲ - در ذات و صفات و توحید حضرت باری تعالی فرماید

<div class="b" id="bn1"><div class="m1"><p>تعالی اللّه زهی ذات و صفاتت</p></div>
<div class="m2"><p>که کی باشد صفاتت غیر ذاتت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تعالی اللّه زهی دیدار رویت</p></div>
<div class="m2"><p>نموده خود همه درگفتگویت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>توئی صنع نهان و آشکارا</p></div>
<div class="m2"><p>که بنمودی بیکباره تو ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>توئی صانع توئی جان و توئی حق</p></div>
<div class="m2"><p>توئی در هر دو عالم نور مطلق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حقیقت نیست جز ذات تو اینجا</p></div>
<div class="m2"><p>شدستم عقل در ذات تو شیدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو خواهی بود تا باشی سراسر</p></div>
<div class="m2"><p>حقیقت جز تو چیزی نیست دیگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وصالت را همه جویا تو در جان</p></div>
<div class="m2"><p>جهانی بر رخ تو مانده حیران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو بودی آدم و آدم تو بودی</p></div>
<div class="m2"><p>خودی خود تو در آدم نمودی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو بودی نوح در دریای معنی</p></div>
<div class="m2"><p>فکندی شورش و غوغای معنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو ابراهیمی و در نار هستی</p></div>
<div class="m2"><p>بت نمرود را صورت شکستی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو اسماعیلی و قربان خویشی</p></div>
<div class="m2"><p>تو هم دردی و هم درمان خویشی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>توئی اسحق و خود را سر بریدی</p></div>
<div class="m2"><p>چو جز دیدار خود چیزی ندیدی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>توئی یعقوب نابینا چرائی</p></div>
<div class="m2"><p>از آن کز یوسف جانت جدائی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>توئی یوسف درون چاه مانده</p></div>
<div class="m2"><p>ز سِرِّ خویشتن آگاه مانده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>توئی جرجیس گشته پاره پاره</p></div>
<div class="m2"><p>جهانی مر ترا اینجانظاره</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>توئی موسی و بر طور الستی</p></div>
<div class="m2"><p>ز اسرار نمود خویش مستی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سلیمانی و ملکت داده بر باد</p></div>
<div class="m2"><p>اگر خواهی کنی تو دیگر آباد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو ایوبی ودیده رنج و زحمت</p></div>
<div class="m2"><p>ولی در عاقبت دیدی تو رحمت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زکریائی و مانده در درختی</p></div>
<div class="m2"><p>به تیغ عشق بیشک لخت لختی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>توئی یحیی در اینجا سر بریده</p></div>
<div class="m2"><p>وصال اینجا ز بیچونی ندیده</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو خضری چشمهٔ حیوان تو داری</p></div>
<div class="m2"><p>چرا ازتشنگی جان بر لب آری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>توئی عیسی و اندر پای داری</p></div>
<div class="m2"><p>نمود عشق خود را پایداری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>توئی مر مصطفا ونور عالم</p></div>
<div class="m2"><p>نموده اندر اینجا سرّ خاتم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو در شهر علومت حیدری تو</p></div>
<div class="m2"><p>نمود راز هر معنی دری تو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>توئی مرانبیاء و اولیاء را</p></div>
<div class="m2"><p>تو هستی ابتدا و انتها را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جلالت انبیا راسخ نمودست</p></div>
<div class="m2"><p>در اسرار کلی برگشودست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>توئی آتش ولیکن درحجابی</p></div>
<div class="m2"><p>از آن پیوسته دائم در عتابی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>توئی باد و روان در جسم وجانی</p></div>
<div class="m2"><p>از آن از دیدهها اینجا نهانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>توئی آب و روانی در همه جای</p></div>
<div class="m2"><p>بهر کسوت که میخواهی تو بنمای</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>توئی خاک و نموده کلّ اسرار</p></div>
<div class="m2"><p>توئی پیدا شده اعیان دیدار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>توئی کان و پر از گوهر نمائی</p></div>
<div class="m2"><p>سزد گر این زمان گوهر فزائی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>توئی عین نبات و سرّ معدن</p></div>
<div class="m2"><p>بتو شد جملهٔ اسرار روشن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>توئی بنموده رخ در کایناتی</p></div>
<div class="m2"><p>در این ظلمات تن آب حیاتی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>توئی جانان و جان اینجا چه گویم</p></div>
<div class="m2"><p>که جز ذاتت درون جان نجویم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زبانی در دهان گویا شده تو</p></div>
<div class="m2"><p>درونِ جانها جویا شده تو</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>توئی و تو شده پیدا مرا یار</p></div>
<div class="m2"><p>که اینجا مینبینم هیچ اغیار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>توئی ای دیدنت در پرده دل</p></div>
<div class="m2"><p>تو هستی در نهان گمکردهٔ دل</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>توئی اللّه در توحیدِ مطلق</p></div>
<div class="m2"><p>نمودِ تست اشیا جمله الحق</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>توئی اللّه اینجا در دل و جان</p></div>
<div class="m2"><p>ز خود برگوی و هم از خود تو برخوان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>توئی اللّه جوهر در میانم</p></div>
<div class="m2"><p>بجز از جوهر ذاتت ندانم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>توئی اللّه اینجا در دل و جان</p></div>
<div class="m2"><p>ز چشم آفرینش نیز پنهان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>توئی اللّه گویائی زبانی</p></div>
<div class="m2"><p>ز چشم آفرینش مر نهانی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دلا چون راز دیدی جمله سرباز</p></div>
<div class="m2"><p>حجاب از پیش چشم خود برانداز</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>حقیقت فاش کردی خویشتن تو</p></div>
<div class="m2"><p>نمودی مر حجاب جان و تن تو</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>جهان چون اوّل و آخر تو باشی</p></div>
<div class="m2"><p>چه گویم این زمان اسرار فاشی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بدیدار تو پنهان گشت پیدا</p></div>
<div class="m2"><p>تعالی اللّه زهی نورِ هویدا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو یکی من چرا پیوند جویم</p></div>
<div class="m2"><p>توئی مطلوبِ طالب چند گویم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>دلم خون گشت ای ساقی اسرار</p></div>
<div class="m2"><p>مرا در عین خود کن ناپدیدار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مرا جامی بده زان جام باقی</p></div>
<div class="m2"><p>که تو هم جام و هم جانی و ساقی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو من توحید اسرار تو بافم</p></div>
<div class="m2"><p>چنان خواهم که جان را برشکافم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>خوشا آن دم که جان بی جسم باشد</p></div>
<div class="m2"><p>بجز ذات تو دیگر اسم باشد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>یقینم شد که نی مُردی نمیری</p></div>
<div class="m2"><p>همه ذرّات عالم دستگیری</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>حیات باقی و عین بهشتی</p></div>
<div class="m2"><p>که طینت در یداللهت سرشتی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>زهی اسرار جان اسراردان کو</p></div>
<div class="m2"><p>یکی دانندهٔ بیننده جان کو</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>هزاران جان پاک پاکبازان</p></div>
<div class="m2"><p>فدای آنکه یابد سرّ جانان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>توئی جانان به جز تو من ندیدم</p></div>
<div class="m2"><p>ز تست این جملهٔ گفت و شنیدم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ز خود آورد ودر خود او نمودست</p></div>
<div class="m2"><p>گره در عاقبت خود برگشودست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چودیدت عشق عقل آمد ملامت</p></div>
<div class="m2"><p>از آن بنمود این رازو پیامت</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>حقیقت جمله دیدار تو آمد</p></div>
<div class="m2"><p>جمال جان خریدارِ تو آمد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>حقیقت هم تو دیده دید دیدار</p></div>
<div class="m2"><p>ز جمله او ترا آمد خریدار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ترا بشناخت اینجا در معانی</p></div>
<div class="m2"><p>که کلّی رهنمای جانِ جانی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>تو جانانی برونِ تو چو اسمست</p></div>
<div class="m2"><p>توئی گنج و همه عالم طلسمست</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>زهی گنج تو جوهر فاش کرده</p></div>
<div class="m2"><p>توئی نقش و توئی نقاش کرده</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ز یکی در یکی خود باز دیده</p></div>
<div class="m2"><p>خود این انجام و خود آغاز دیده</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>زهی از عشق خود مجروح گشته</p></div>
<div class="m2"><p>توئی صورت عیان روح گشته</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>تو دانستی که چون بستی صور را</p></div>
<div class="m2"><p>توئی انداخته عین گهر را</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ترا بر ذرّه ذرّه راه بینم</p></div>
<div class="m2"><p>ترا در جزو و کل آگاه بینم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>تو آگاهی و صورت بیخبر ماند</p></div>
<div class="m2"><p>درون پرده حیران در نظر ماند</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>نبینم جز ترا یک چیز دیگر</p></div>
<div class="m2"><p>چو تو باشی نباشد نیز دیگر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چنین گفتست اینجا راه بینی</p></div>
<div class="m2"><p>ز وصف تو مرا عین الیقینی</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>که گردیدم بسی درجان عالم</p></div>
<div class="m2"><p>نظر کردم باین و آن عالم</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ندیدم هیچ جز جانان حقیقت</p></div>
<div class="m2"><p>چو بسپردم بدو راه طریقت</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ندیدم هیچ جز دیدار رویش</p></div>
<div class="m2"><p>همه دارند سرّ لا و هویش</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ندیدم هیچ جز دیدار اللّه</p></div>
<div class="m2"><p>زدم دم در عیان قل هو اللّه</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ندیدم جز یکی در جوهر ذات</p></div>
<div class="m2"><p>نمود یار دیدم جمله ذرّات</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ندیدم جز یکی در کارگاهش</p></div>
<div class="m2"><p>شدم در سایهٔ عشق و پناهش</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ندیدم جز یکی پیدا و پنهان</p></div>
<div class="m2"><p>نمود خویش دیدم جمله جانان</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ندیدم جز یکی و در یکی بود</p></div>
<div class="m2"><p>نمود یار حق حق بیشکی بود</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ندیدم جز یکی تا راه بردم</p></div>
<div class="m2"><p>ز دید عشق راه جان سپردم</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>ندیدم جز یکی در گنج جانان</p></div>
<div class="m2"><p>اگرچه پر کشیدم رنج جانان</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>ندیدم جز یکی در دل عیانست</p></div>
<div class="m2"><p>ز یکی یار بی نام و نشانست</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>ندیدم جز یکی در لانموده</p></div>
<div class="m2"><p>نمودم یار در لا، لا ربوده</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>ندیدم جز یکی اندر نمودار</p></div>
<div class="m2"><p>عیان دوست دیدم لیس فی الدّار</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>یکی دیدم همه انجام و آغاز</p></div>
<div class="m2"><p>از آن اسرار کردم جمله سرباز</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>یکی دیدم تمامت بی نهایت</p></div>
<div class="m2"><p>همه در دوست در دیدار غایت</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>یکی دیدم مکان و لامکان هم</p></div>
<div class="m2"><p>بهم پیوسته دیدم جسم و جان هم</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>یکی دیدم ز یکی کل نموده</p></div>
<div class="m2"><p>ز یکی دیده و دیدم گشوده</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>یکی دیدم عیان و در یکی هست</p></div>
<div class="m2"><p>یکی اندر دوئی یار پیوست</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>یکی دیدم ز خود پیدا نکرده</p></div>
<div class="m2"><p>ولی اندر حجاب هفت پرده</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>یکی دیدم درون را با برونش</p></div>
<div class="m2"><p>همه ره گم بکرده رهنمونش</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>یکی دیدم درون جان سراسر</p></div>
<div class="m2"><p>از آن اسرار ربّانی تو مگذر</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>چو درتوحید جانان در یکیام</p></div>
<div class="m2"><p>ز یکی جوهر کل بیشکیام</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>چو در توحید جز یکی ندیدم</p></div>
<div class="m2"><p>یکی را در یکی یکی گزیدم</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>منم توحید یار و سرّ اسرار</p></div>
<div class="m2"><p>منم در جسم و جان بنموده گفتار</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>منم توحید اسرار الهی</p></div>
<div class="m2"><p>نموده سر ز ماهم تا بماهی</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>منم توحید جانان آشکاره</p></div>
<div class="m2"><p>خودی خود زخود کرده نظاره</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>منم توحید در لا مانده پنهان</p></div>
<div class="m2"><p>حقیقت مینمایم سرّ اعیان</p></div></div>