---
title: >-
    بخش ۴۰ - در عیان پسر و اسرار منصور و اجازت از پدر خواستن فرماید
---
# بخش ۴۰ - در عیان پسر و اسرار منصور و اجازت از پدر خواستن فرماید

<div class="b" id="bn1"><div class="m1"><p>چو منصور این حقیقت راست برگفت</p></div>
<div class="m2"><p>نمود جوهر اسرار بر سُفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آن دریا بصورت برخمید او</p></div>
<div class="m2"><p>ز چشم جمله گشتش ناپدید او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مثال برق آنجاگاه بشتافت</p></div>
<div class="m2"><p>چه کس باشد کز این معنی خبر یافت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پدر چون دید آن سرّ عجائب</p></div>
<div class="m2"><p>عجائب ماند آنجا زین غرائب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بزد یک نعره و خاموش شد او</p></div>
<div class="m2"><p>در آن عین خودی بیهوش شد او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو پیر آن دید اندر حالت افتاد</p></div>
<div class="m2"><p>برآورد آن زمانش پیر فریاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که ای جان جهان آخر کجائی</p></div>
<div class="m2"><p>ندانم تا برم دیگر کی آئی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شدی غایب ز پیشم ناگهانی</p></div>
<div class="m2"><p>نمیدانم من این سرّ را تو دانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شدی غائب ولی در جان و جسمی</p></div>
<div class="m2"><p>توئی گنج و درونم در طلسمی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کجا بینم ترا دیگر در این جای</p></div>
<div class="m2"><p>برآورده خروش و بانگ و غوغای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تمامت خلق کشتی در تحیّر</p></div>
<div class="m2"><p>بمانده بیخبر چون در صدف دُر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همه حیران در آن اسرار مانده</p></div>
<div class="m2"><p>مثال نقطه در پرگار مانده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو با خود آمد آنگه باب منصور</p></div>
<div class="m2"><p>ز جان افتاده بود و از جهان دور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر آوردش دم و یک نعره دربست</p></div>
<div class="m2"><p>نمود خویشتن در ذات او بست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برون انداخت از کشتی وجودش</p></div>
<div class="m2"><p>درون بحر شد دریای بودش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو آن در عین آن دریا فتاد او</p></div>
<div class="m2"><p>درون بحر بیخود جان بداد او</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همه بحر جهان دید و جهان دید</p></div>
<div class="m2"><p>ز خویشش دید آن راز نهان دید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بزد یک اللّه و وز جان برآمد</p></div>
<div class="m2"><p>جهان جانستان بروی سرآمد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جهانا هرچه میخواهی کنون تو</p></div>
<div class="m2"><p>که مر عهد یکایک بشکنی تو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وفاداری مجو زین کندهٔ پیر</p></div>
<div class="m2"><p>که هر لحظه کند صد رأی و تدبیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وفا هرگز مجو از وی بپرهیز</p></div>
<div class="m2"><p>تو با او دیگر اینجاگاه مستیز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که او را هیچ اینجاگه وفا نیست</p></div>
<div class="m2"><p>که کار او به جز جور و جفا نیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جهانا چند خواهی گشت آخر</p></div>
<div class="m2"><p>کرا خواهی مرا این هست آخر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جهانا طبع مردم خوار داری</p></div>
<div class="m2"><p>که رسم تست مردم خوار داری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جهانا مهلتم ده تا زمانی</p></div>
<div class="m2"><p>فرو گرییم ازدستت جهانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کما بیشی من پیداست آخر</p></div>
<div class="m2"><p>ز خون من چه خواهد خواست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جهان از مرگ من ماتم نگیرد</p></div>
<div class="m2"><p>ز مشتی استخوان عالم نگیرد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جهانا مهلتم ده تا ببینی</p></div>
<div class="m2"><p>نمود من اگر صاحب یقینی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کجا دانی تو اسرارم در اینجا</p></div>
<div class="m2"><p>ترا بنمایم این اسرار اینجا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دلا خون خور در این بحر معانی</p></div>
<div class="m2"><p>که قدر خویش هم اینجا ندانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دلا خون خور که از خون آمدی تو</p></div>
<div class="m2"><p>چگویم تا که خود چون آمدی تو</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بخور خون که ترا خاکت خوُرَد باز</p></div>
<div class="m2"><p>نمود تو بجای خود برد باز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>میان آب می رو پاک جان شو</p></div>
<div class="m2"><p>به عین عشق دیدار جهان شو</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>درون آب دریای جهانی</p></div>
<div class="m2"><p>بگو تا چند از این کشتی دوانی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دلت شد با خبر زین سرّ دریا</p></div>
<div class="m2"><p>حکایت کوش کردستی و بینا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نهٔ آگه که چون بودست رازت</p></div>
<div class="m2"><p>رها کردی در اینجا شاهبازت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چونشناسی نمود سرّ مردان</p></div>
<div class="m2"><p>از آنی چون فلک پیوسته گردان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سوی بحر فناشو همچو منصور</p></div>
<div class="m2"><p>ز کشتی و ز باب خویش شو دور</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سوی بحر فنا شو سوی یارت</p></div>
<div class="m2"><p>میان آب دریا می چه کارت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>خبر داری زجوهر زود بشتاب</p></div>
<div class="m2"><p>نمود خویش در بغداد جان یاب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز بغدادت اگر واقف شدستی</p></div>
<div class="m2"><p>ز دید چشم و جان واقف شدستی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چرا چندین تو اندر بند خلقی</p></div>
<div class="m2"><p>بدان ماند که حاجتمند خلقی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز دنیا بگذر و از عین دریا</p></div>
<div class="m2"><p>که در دریا نبینی جز که سودا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>درون بحر جان انداز خود را</p></div>
<div class="m2"><p>مگر کآگاه گردانی تو خود را</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>درون بحر شو تا راز بینی</p></div>
<div class="m2"><p>حقیقت جوهر جان باز بینی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>درون بحرمعنی هرکه ره برد</p></div>
<div class="m2"><p>چو غوّاصا ره دلدار بسپرد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به جوهر در رسیدم چند گویم</p></div>
<div class="m2"><p>بهر وصفی که میگویم چه گویم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چه گویم اندر این میدان فتاده</p></div>
<div class="m2"><p>میان خاک بی جولان فتاده</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>منم بیچاره و حیران بمانده</p></div>
<div class="m2"><p>چگوئی خوار و سرگردان بمانده</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>منم بیچاره اندر کوی دلدار</p></div>
<div class="m2"><p>اگرچه راه بردم سوی دلدار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نه در دینم نه اندر کیش مانده</p></div>
<div class="m2"><p>بسان کافری درویش مانده</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>در این دریای بی پایان فتاده</p></div>
<div class="m2"><p>سراندر قعر این عمان نهاده</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چو غوّاصی کنم در بحر اعظم</p></div>
<div class="m2"><p>قدم می دارم اندر عشق محکم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چو غوّاصی کنم دُرها بیابم</p></div>
<div class="m2"><p>پس آن گاهی سوی بالاشتابم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>درون جان من بحریست در دید</p></div>
<div class="m2"><p>که اینجا مینبینم جز که آن دید</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>درون جان من بحریست معنی</p></div>
<div class="m2"><p>ندارم با کسی اینجای دعوی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>منم عطّار کز بحر معانی</p></div>
<div class="m2"><p>کنم هر ساعتی گوهر فشانی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>منم دریا و کشتی رانده بی حد</p></div>
<div class="m2"><p>شده فارغ ز بود نیک یا بد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>منم عطّار و اسرار جهانم</p></div>
<div class="m2"><p>حقیقت درّ معنی میفشانم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چو نقش من دگر عالم نبیند</p></div>
<div class="m2"><p>کسی داند که او جانان گزیند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>عیان این جهان و آن جهانم</p></div>
<div class="m2"><p>ورای این زمان و آسمانم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>حقیقت من نمودم جوهر دوست</p></div>
<div class="m2"><p>برون آوردم اینجا روغن از پوست</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>سلاطینان عالم گرچه شاهند</p></div>
<div class="m2"><p>بحمداللّه بر من خاک راهند</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چو سلطانم به معنی و بصورت</p></div>
<div class="m2"><p>بیفکنده ز دل خود کدروت</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چو سلطانم اباخیل و سپاهم</p></div>
<div class="m2"><p>که اندر سلطنت دیدار شاهم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>منم شاه جهان در سرّ معنی</p></div>
<div class="m2"><p>که دارم در حقیقت عین تقوی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بسی شادی و غم خوردم بعالم</p></div>
<div class="m2"><p>که سلطانم ابی شک من در این دم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>منم سلطان جمله سالکان من</p></div>
<div class="m2"><p>که دیدستم حقیقت جان جان من</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>کجا اهل دلی درگوشهٔ فرد</p></div>
<div class="m2"><p>که بنشیند دمی با من در این درد</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>که بنمایم ورا سرّ الهی</p></div>
<div class="m2"><p>بماهش افکنم او را ز ماهی</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بسی اسرار گویانند و بسیار</p></div>
<div class="m2"><p>ولی هرگز نباشد همچو عطّار</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>که من بگشودهام این راز مشکل</p></div>
<div class="m2"><p>بسی حسرت که در جاندارم و دل</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>کجا گویم چو همرازی ندیدم</p></div>
<div class="m2"><p>نخوانم چون هم آوزی ندیدم</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>از این ایوان پردود و ستاره</p></div>
<div class="m2"><p>بسی کردم بهر جانب نظاره</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>دمی غافل نبودم زین نمودار</p></div>
<div class="m2"><p>که تادریافتم اعیان اسرار</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>نمود عشق جمله عاشقانم</p></div>
<div class="m2"><p>عیان راه جمله سالکانم</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>حقیقت یافتم جانان و جان من</p></div>
<div class="m2"><p>بکردم فاش این راز نهان من</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>حقیقت هرکه شد اینجا خبردار</p></div>
<div class="m2"><p>نمود خویشتن آویخت بردار</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>حقیقت هر که این دیدار دریافت</p></div>
<div class="m2"><p>هر آن چیزی که میبیند نکو یافت</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>بجز حق بین نداند گفتهٔ من</p></div>
<div class="m2"><p>که بنهادستم این اسرار روشن</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>دلا خون خوردهٔ تا راز گفتی</p></div>
<div class="m2"><p>هر آن رازی که دیدی بازگفتی</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>دلا خون خوردهٔ در پردهٔ خود</p></div>
<div class="m2"><p>که تا دیدی عیان گم کردهٔ خود</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>دلا خون خوردهٔ و غرق خونی</p></div>
<div class="m2"><p>ولیکن این زمان دیدار چونی</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>دلا خون خوردهٔ تا در صفاتی</p></div>
<div class="m2"><p>ولیکن این زمان دیدار ذاتی</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>دلا خون خوردهٔ و خون بخورهم</p></div>
<div class="m2"><p>که خون خوردست هم بسیار آدم</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>دلا خون خور که خون بودی ز اول</p></div>
<div class="m2"><p>ولی اینجا شدی در خود معطّل</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>ز خونی آمدی اوّل پدیدار</p></div>
<div class="m2"><p>بآخرهم بخون مانی گرفتار</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>ز خونی لیک اندر خاکماندی</p></div>
<div class="m2"><p>ز سرّصنع عین پاک ماندی</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>ز راه چشم خون دل بریزان</p></div>
<div class="m2"><p>که خواهی گشت خاک خاکبیزان</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>که بعد از ما وفاداران هشیار</p></div>
<div class="m2"><p>بخاک ما فرو گریند بسیار</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>نباشد فایده زیرا که خاکیم</p></div>
<div class="m2"><p>به عین عاقبت اندر هلاکیم</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>چه حاجت بود چندان گفتن ای دوست</p></div>
<div class="m2"><p>که میبایست در طین خفت ای دوست</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>نمود خاک اصل پاک دارد</p></div>
<div class="m2"><p>که آدم دید حق درخاک دارد</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>اگرنه خاک اصل پاک بودی</p></div>
<div class="m2"><p>گلِ آدم کجادرخاک بودی</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>نمود خاک از آن حاصل نموداست</p></div>
<div class="m2"><p>که خود را بیشکی واصل نمودست</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>ز خاکست اصل و در خاکی شدی تو</p></div>
<div class="m2"><p>چگویم تا در اول چون بُدی تو</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>حقیقت خاک واصل شد در این راه</p></div>
<div class="m2"><p>که او اینجا ریاضت یافت از شاه</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>حقیقت خاک چندینی ریاضت</p></div>
<div class="m2"><p>کشید و یافت او بیشک سعادت</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>حقیقت خاک میداند که جان چیست</p></div>
<div class="m2"><p>درون او همه راز نهان چیست</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>شنیدم من که پیری پر ز اسرار</p></div>
<div class="m2"><p>بگِردِ خاک مردان گشت بسیار</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>شبی میگفت خوش کرد خاکی</p></div>
<div class="m2"><p>بگوش او رسید آواز پاکی</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>که ای مسکین چرا چندین بگردی</p></div>
<div class="m2"><p>بگو تا اندر این دنیا چه کردی</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>چرا این گور مردم میپرستی</p></div>
<div class="m2"><p>بگرد کارمردم گرد و رستی</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>که ما خاکیم و هستی هم تو از خاک</p></div>
<div class="m2"><p>ولی با تست بیشک صانع پاک</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>اگرچه خاک گشتیم اندر این راه</p></div>
<div class="m2"><p>ولی ما بهتریم از جمله آگاه</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>که خاکیم این زمان در عین هستی</p></div>
<div class="m2"><p>نه مانند شما در بت پرستی</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>چو زیر خاک ما را یار باشد</p></div>
<div class="m2"><p>در این معنی بسی اسرار باشد</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>درونیم و برون بگرفته از دوست</p></div>
<div class="m2"><p>حقیقت مغز باشد جملگی پوست</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>خدا با ما است هم دیدار اوئیم</p></div>
<div class="m2"><p>که اندر خاک برخوردار اوئیم</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>نمود خاک ما را کرد واصل</p></div>
<div class="m2"><p>همه مقصود ما اینجاست حاصل</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>همه مقصود اینجاگه بدیدیم</p></div>
<div class="m2"><p>که از چشم جهان ما ناپدیدیم</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>جهانیم و نه اندر روی خاکیم</p></div>
<div class="m2"><p>که این دم نور قدس و نور پاکیم</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>نمایم این زمان دیدار بیخود</p></div>
<div class="m2"><p>که فانیّم و گشته فارغ از بد</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>خدا با ماست ماهم با خدائیم</p></div>
<div class="m2"><p>که این دم یافته عین بقائیم</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>فنائیم این زمان از دید صورت</p></div>
<div class="m2"><p>بسر کرده همه عین کدورت</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>فنائیم این زمان در جزو و در کل</p></div>
<div class="m2"><p>برسته از غم وز رنج وز ذل</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>فنائیم این زمان از عالم دون</p></div>
<div class="m2"><p>درون افتادهایم از عین گردون</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>فنائیم این زمان اندر جلالیم</p></div>
<div class="m2"><p>ز حیرت پیش جانان گنگ و لالیم</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>فنائیم این زمان در عین هستی</p></div>
<div class="m2"><p>رها کردیم اینجا بت پرستی</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>فنائیم و بقا دریافته ما</p></div>
<div class="m2"><p>بسوی جزو و کل بشتافته ما</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>ز بود خویشتن نابود بودیم</p></div>
<div class="m2"><p>که این دم بودِ بودِ بود بودیم</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>ز بود حق چو صورت برفکندیم</p></div>
<div class="m2"><p>خود اندر ذات آن حق درفکندیم</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>جمال اندر جلال کل بدیدیم</p></div>
<div class="m2"><p>حقیقت با خدای خود رسیدیم</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>نمود حیرتست اینجای در عشق</p></div>
<div class="m2"><p>نمیدانیم این غوغای در عشق</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>درونست و برون ما یکی هم</p></div>
<div class="m2"><p>که حق گشتیم بیشک حق یکی هم</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>شما مانند ما خواهید بودن</p></div>
<div class="m2"><p>نماند دائم این گفت و شنودن</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>شما مانند ما در خون بر آئید</p></div>
<div class="m2"><p>چرا در بند ایوان و سرائید</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>دل از بند جهان آزاد دارید</p></div>
<div class="m2"><p>بجز تخم نکونامی مکارید</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>که ما همچون شما بودیم چندان</p></div>
<div class="m2"><p>بنشنیدیم پند هوشمندان</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>ز کار آخرت بودیم غافل</p></div>
<div class="m2"><p>نکردیم آنچهمان فرمود عاقل</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>کنون هستیم از کرده پشیمان</p></div>
<div class="m2"><p>که کرم و مور باشدمان ندیمان</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>چه سود از روزگار برفشانده</p></div>
<div class="m2"><p>بدل در حسرت جاوید مانده</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>کنون ای دوستان زنهار زنهار</p></div>
<div class="m2"><p>بترسید از بد این دهر مکّار</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>بجز فرمان یزدان نیست کاری</p></div>
<div class="m2"><p>بورزید و مدارید هیچ عاری</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>که راهی سخت دشوارست در پیش</p></div>
<div class="m2"><p>اگر تو مؤمنی زین دم بیندیش</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>بجز حق هیچکس واقف نبود است</p></div>
<div class="m2"><p>که این اسرار از دیدار بودست</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>نمود خاک جمله جان پاکست</p></div>
<div class="m2"><p>دراو رفتن تو میگوئی چه باکست</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>بهرگامی که اینجا مینهی در</p></div>
<div class="m2"><p>سرشاهیست چون فغفور و قیصر</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>بسی بادام چشمانند در خاک</p></div>
<div class="m2"><p>که جان دادن نزد صانع پاک</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>تونیز ار عاقلی آهسته میرو</p></div>
<div class="m2"><p>نمود عشق از عطّار بشنو</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>مَخُسب ای دل سخن بپذیر آخر</p></div>
<div class="m2"><p>ز چندین رفته نفرت گیر آخر</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>مخسب ای دل که تا بیدار گردی</p></div>
<div class="m2"><p>مگر شایستهٔ اسرار گردی</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>در این اسرار تو اندیشهٔ کن</p></div>
<div class="m2"><p>نمود عشق خود را پیشهٔ کن</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>مخسب اندر شب مهتاب آخر</p></div>
<div class="m2"><p>چه خواهی دیدن از این خواب آخر</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>شب مهتاب خوابت چون پرد بین</p></div>
<div class="m2"><p>شب مهتاب نور عشق حق بین</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>شب مهتاب چون میآمدت خواب</p></div>
<div class="m2"><p>که عاشق خواب کی ماند ز مهتاب</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>شب مهتاب واصل شو ز اسرار</p></div>
<div class="m2"><p>در آن ساعت که باشد لیس فی الدّار</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>شب مهتاب اگر معشوق بینی</p></div>
<div class="m2"><p>دمی با او در آن خلوت نشینی</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>شب مهتاب اندر نور باشی</p></div>
<div class="m2"><p>میان جزو و کل مشهور باشی</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>شب مهتاب بنماید رخت یار</p></div>
<div class="m2"><p>که در شب مینگنجد هیچ اغیار</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>شب مهتاب اگر واصل شوی تو</p></div>
<div class="m2"><p>نباید زین سخن غافل شوی تو</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>شب مهتاب کآنشب بدر باشد</p></div>
<div class="m2"><p>در آن شب عاشقان را قدر باشد</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>شب مهتاب حق بی شک بیابی</p></div>
<div class="m2"><p>چه گویم کاین زمان در عین خوابی</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>چرا خفتی شب مهتاب ای دوست</p></div>
<div class="m2"><p>که تا با مغز گردانی همه پوست</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>نیندیشی که چون عمرت سرآید</p></div>
<div class="m2"><p>بسی مهتاب در گورت درآید</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>ترا زیر کفن بگرفته خوابی</p></div>
<div class="m2"><p>فرو افتد بگورت ماهتابی</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>محنسب و سرّ این سرار دریاب</p></div>
<div class="m2"><p>مشو ای دوست چندینی تو در خواب</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>نکو نبود چگوید مرد هشیار</p></div>
<div class="m2"><p>بخفته عاشق و معشوق بیدار</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>تو در خوابی و بیداران برفتند</p></div>
<div class="m2"><p>عزیزان و وفاداران برفتند</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>تو در دنیا و اندر دیر خود رای</p></div>
<div class="m2"><p>بماندی همچو سیم قلب در جای</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>تو در این دار دنیا باز مانده</p></div>
<div class="m2"><p>ز بهر شهوت پر آز مانده</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>توئی غافل در این دنیای مکّار</p></div>
<div class="m2"><p>که ناگاهت برون آرد ز پرگار</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>تو این دم خفته آگاهی نداری</p></div>
<div class="m2"><p>که اینجا عین اللهی نداری</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>همه مردان سوی درگاه رفتند</p></div>
<div class="m2"><p>ز بود خویشتن آگاه رفتند</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>همه مردان عالم راز دیدند</p></div>
<div class="m2"><p>ز جان گم کردهٔ خود بازدیدند</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>همه مردان دراین میدان چو گویند</p></div>
<div class="m2"><p>بجز توحید او چیزی نگویند</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>چو مردان عالمی پر درد دارند</p></div>
<div class="m2"><p>ز درد عشق خود را فرد دارند</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>اگر مردی تو اندر دار دنیا</p></div>
<div class="m2"><p>خبر یابی تو از اعیان عقبی</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>اگر مردی به جز مردان مبین تو</p></div>
<div class="m2"><p>همیشه خدمت مردان گزین تو</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>که مردانند دائم سالک راه</p></div>
<div class="m2"><p>طلبکاراند ز دائم دیدن شاه</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>طلب کن اینچنین مردان حق تو</p></div>
<div class="m2"><p>که بردی از همه در ره سبق تو</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>خدا زیشان طلب تا راز یابی</p></div>
<div class="m2"><p>حقیقت جان جانت بازیابی</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>خدا ز آن سان طلب در عین اسرار</p></div>
<div class="m2"><p>که از انسان شود این سر پدیدار</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>خدا ز آن سان طلب چون میندانی</p></div>
<div class="m2"><p>چو گویندت عجب حیران بمانی</p></div></div>