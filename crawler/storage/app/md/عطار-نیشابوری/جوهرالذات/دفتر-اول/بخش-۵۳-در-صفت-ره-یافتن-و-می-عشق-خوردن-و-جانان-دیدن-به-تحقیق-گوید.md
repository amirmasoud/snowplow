---
title: >-
    بخش ۵۳ - در صفت ره یافتن و می عشق خوردن و جانان دیدن به تحقیق گوید
---
# بخش ۵۳ - در صفت ره یافتن و می عشق خوردن و جانان دیدن به تحقیق گوید

<div class="b" id="bn1"><div class="m1"><p>دریغا ره نمیدانی چه گوئی</p></div>
<div class="m2"><p>که سرگردان شده مانند گوئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در این میدان چو گوئی در تک و تاز</p></div>
<div class="m2"><p>شدی اکنون و مر چوگان بینداز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در این میدان چو گوئی مینبردی</p></div>
<div class="m2"><p>کجا صافی خوری مانند دُردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خرابات فنا کن اختیارت</p></div>
<div class="m2"><p>تو با میدان و گوی اکنون چه کارت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خرابات فنا از بهر مردانست</p></div>
<div class="m2"><p>که این چرخ فلک زانروی گردانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خراباتی شو و اندر خرابات</p></div>
<div class="m2"><p>بوقت صبحدم میکن مناجات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خراباتی شو و رطل گران کش</p></div>
<div class="m2"><p>دمادم جام وحدت رایگان کش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خراباتی شو این جام کن نوش</p></div>
<div class="m2"><p>اگر مردی در اینجا باش خاموش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خراباتی شو و اندر پیش دلدار</p></div>
<div class="m2"><p>ز نام و ننگ خود بگذر به یکبار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شود زان می ترا فانی وجودت</p></div>
<div class="m2"><p>نظر کن آنگهی مر بود بودت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خدا دریاب و از خود شو تو فانی</p></div>
<div class="m2"><p>اگر این سرّ معنی باز دانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مئی کان عاشقان خوردند اینجا</p></div>
<div class="m2"><p>وزان میگوی کل بردند اینجا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مئی کان عاشقان صادق بنوشند</p></div>
<div class="m2"><p>همه باید کز آن سرّ کم خروشند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مئی کان عاشقان خوردند و رفتند</p></div>
<div class="m2"><p>حقیقت راز معنی فاش گفتند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مئی کان عاشقان لاابالش</p></div>
<div class="m2"><p>کشیدند آنگهی عین وصالش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در آن می وصل اینجا باز دیدند</p></div>
<div class="m2"><p>ز بود او بکام دل رسیدند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در آن می جملهٔ ذرّات مستند</p></div>
<div class="m2"><p>از آن در جود حق با نیست هستند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در آن می هر که او پائی بدارد</p></div>
<div class="m2"><p>یقین دانم که او تحقیق دارد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در آن می راز بیند همچو مردان</p></div>
<div class="m2"><p>حقیقت نزد اولاشیء شود جان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در آن می جان کجا گنجد زمانی</p></div>
<div class="m2"><p>که آن دم نیست اینجا کل مکانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در آن می هستی جاوید باشد</p></div>
<div class="m2"><p>ترا از ذرّهٔ خورشید باشد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در آن می در یکی بینی تو خود را</p></div>
<div class="m2"><p>نگنجد هیچگونه نیک و بد را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در آن می گر یکی بینی حقیقت</p></div>
<div class="m2"><p>طریقت با حقیقت در شریعت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در آن می در خدا بینی حقیقت</p></div>
<div class="m2"><p>نمیگنجد دگر اینجا دقیقت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در آن می جمله مردان فاش گشتند</p></div>
<div class="m2"><p>ز نقش اندر جهان نقاش گشتند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در آن می واصلی شان منکشف شد</p></div>
<div class="m2"><p>نمود اوّل آخر متّصف شد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در آن می شان حقیقت گشت واصل</p></div>
<div class="m2"><p>شدند اندر خرابی جمله حاصل</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در آن می شان عیان عشق بایار</p></div>
<div class="m2"><p>حجاب از پیششان برخواست یکبار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در آن می شان تمامت گشت روشن</p></div>
<div class="m2"><p>رها کردند آنگه عین گلخن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در آن می شان فنا آمد پدیدار</p></div>
<div class="m2"><p>نگه کردند و دیدیند جمله عطّار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در آن می گر نبودی هستی من</p></div>
<div class="m2"><p>کجا پیدا شدی این هستی من</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در آن می یافتم هستی دو جهان</p></div>
<div class="m2"><p>ز جامی یافتم مستی جانان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نمیدانستم و غافل بمانده</p></div>
<div class="m2"><p>در این عین جهان بیدل بمانده</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نمیدانستم وهم باز دیدم</p></div>
<div class="m2"><p>نمود عشق کلّی راز دیدم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نمیدانستم ودانستم اکنون</p></div>
<div class="m2"><p>که یارم در درون ماندست بیرون</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بدو پیدا شدم از هستی او</p></div>
<div class="m2"><p>نمود سرّ کل از هستی او</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بدو پیدا شدم و زوی بگفتم</p></div>
<div class="m2"><p>در اسرار را نیکو بسفتم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بدو پیدا شدم وز اوست پنهان</p></div>
<div class="m2"><p>از او دارم نمود و اوست جانان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بدو پیدا شدم در جوهر راز</p></div>
<div class="m2"><p>حجاب هفت پرده کردهام باز</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بدو پیدا شدم از بود و نابود</p></div>
<div class="m2"><p>مرا اندر میان مقصود او بود</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بدو پیدا شدم او واصلم کرد</p></div>
<div class="m2"><p>عیان خویش اینجا حاصلم کرد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بدو پیدا شدم بنمود ما را</p></div>
<div class="m2"><p>عیان ابتدا با انتها را</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بدو پیدا شدم دیدار او بین</p></div>
<div class="m2"><p>نمود عشقم و گفتار او بین</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بدو پیدا شدم بنمود باقی</p></div>
<div class="m2"><p>مرا درجام کل او بود ساقی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بدو دیدم هم او را بی حجابی</p></div>
<div class="m2"><p>بجز یکی نمیبینم حسابی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بدو دیدم جمال طلعت او</p></div>
<div class="m2"><p>هم او بد نور قدس و خلعت او</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مرا بخشید در گفتار اسرار</p></div>
<div class="m2"><p>دمادم گفت در جانم که عطّار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>یقین در پیش دارد جز مراتو</p></div>
<div class="m2"><p>مبین در ابتدا و انتها تو</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مرا بین در دل و جان تا توانی</p></div>
<div class="m2"><p>منت دادم همه سرّ معانی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>منت دادم همه اسرار عشاق</p></div>
<div class="m2"><p>بتو ختمست کل انوار عشاق</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>منت دادم چنین تشریف در پوش</p></div>
<div class="m2"><p>زمانی هم مشو ز اینجای خاموش</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو بلبل باش اندر گلستانم</p></div>
<div class="m2"><p>نواها میزن اندر بوستانم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بصد دستان همی زن مر نوا تو</p></div>
<div class="m2"><p>چو داری این زمان عشق لقا تو</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نوای پردهٔ عشاق می ساز</p></div>
<div class="m2"><p>درون پردهام می سوز و می ساز</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بهر دستان که میخواهی تو دستان</p></div>
<div class="m2"><p>بجز می از یداللّهات تو مستان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>که مست شوق مائی از ازل تو</p></div>
<div class="m2"><p>وجودت را بجان کردم بدل تو</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>نظیرت نیست لیکن در مقامات</p></div>
<div class="m2"><p>عیان تست در اسرار طامات</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>منم گویا در این عین زبانت</p></div>
<div class="m2"><p>منم اینجا همه شرح و بیانت</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ترا دادم عیان سّر معانی</p></div>
<div class="m2"><p>که تا اینجاتو قدر من بدانی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ترا دادم عیان واصلانت</p></div>
<div class="m2"><p>کنم اینجای بی نام ونشانت</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>اگر ما را بکل آنجای خواهی</p></div>
<div class="m2"><p>سزد گر هیچ جز از ما نخواهی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>نشان واصلی این است دریاب</p></div>
<div class="m2"><p>دمادم سوی من بیجان تو بشتاب</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>سراسر هر چه بینی ما همی بین</p></div>
<div class="m2"><p>بجز من هیچ در دیدار مگزین</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چو تو جویای ما بودی در اول</p></div>
<div class="m2"><p>در آخر میشوی چندین معطّل</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>رضای ما بدست آور ز مائی</p></div>
<div class="m2"><p>که ما را هست عین کلّ خدائی</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بکل قربان ما شو اندر این راه</p></div>
<div class="m2"><p>که هستی این زمان از سرّم آگاه</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>نثار روی ما کن جان و دل تو</p></div>
<div class="m2"><p>گذر کن از نقوش آب و گل تو</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>کلاه عشق دادیمت چو بر سر</p></div>
<div class="m2"><p>که در پیشت نهم آفاق یکسر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ببُر سر تا مرا بینی عیان تو</p></div>
<div class="m2"><p>که این سرّ درنمیگنجد بدان تو</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ز خود چون بگذری مارا بدانی</p></div>
<div class="m2"><p>در آخر چون بدانی کل توانی</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>منم مخفی ز جمله ناپدیدار</p></div>
<div class="m2"><p>که آوردم ز خود کلّی بدیدار</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ز خود پیدا نمودم خویش پنهان</p></div>
<div class="m2"><p>شده اینجا به جز دیدن به نتوان</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>در آندم کین دم صورت نماند</p></div>
<div class="m2"><p>بجز من عین مقصودت نماند</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>نماند اسم و جسم و عقل و ادراک</p></div>
<div class="m2"><p>سراسر محو گردانم ترا پاک</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>حجاب صورتت بردارم از پیش</p></div>
<div class="m2"><p>کنم بود وجودت جملگی خویش</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>نماند هیچ گفتار تو اینجا</p></div>
<div class="m2"><p>نماند هیچ اسرار تو اینجا</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بجز من هر چه در دیدار آری</p></div>
<div class="m2"><p>یقین میدان که خود سرّی نداری</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>نماند هیچ جز من مر ترا هیچ</p></div>
<div class="m2"><p>نبینی این طلسم پیچ در پیچ</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>مرادم کشتن تست اندر اینجا</p></div>
<div class="m2"><p>که تا اینجا ببینی دیدن ما</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>مرادم کشتن تست از طریقت</p></div>
<div class="m2"><p>که ما را بنگری اندر حقیقت</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>مرادم کشتن تست آخر کار</p></div>
<div class="m2"><p>که تا یابی مرا در جمله اظهار</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>مرادم کشتن تست ار بدانی</p></div>
<div class="m2"><p>که مقصودم توئی سرّ نهانی</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>مرادم کشتن تست و فنا شو</p></div>
<div class="m2"><p>مرا در جزو و کل عین بقا شو</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>مرادم کشتن تست و تو بگذر</p></div>
<div class="m2"><p>تو خود جز من دمی در هیچ منگر</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>مرادم کشتن تست و فنایت</p></div>
<div class="m2"><p>نمایم جزو و کل عین بقایت</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>چو تو جز من یقین غیری ندیدی</p></div>
<div class="m2"><p>ز من گفتی و هم از من شنیدی</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>ز من گفتی همه اسرار ما را</p></div>
<div class="m2"><p>تو کردی فاش مر سرّ بقا را</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>تو با من گردی و من با تو بودم</p></div>
<div class="m2"><p>یکی بد با توام گفت و شنودم</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>بجز من هیچ تکراری نکردی</p></div>
<div class="m2"><p>میان واصلان امروز فردی</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>بمن فردی بمن گشتی منزّه</p></div>
<div class="m2"><p>بمن دیدی سراسر دید این ره</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>منت ره بودم و من نیز منزل</p></div>
<div class="m2"><p>منت بگشادهام اینجای مشکل</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>منت گویایم و من نیز گویا</p></div>
<div class="m2"><p>در این عین جهان منگر به جز ما</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>یقین اینجا به جز من هیچکس نیست</p></div>
<div class="m2"><p>بجز من هیچکس فریادرس نیست</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>منت جان دادم و منجان ستانم</p></div>
<div class="m2"><p>منت بنمایم اینجا و من آنم</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>که در خون خاک جسمت در کنم من</p></div>
<div class="m2"><p>کنم اسرار کلّی از تو روشن</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>چو پیش از خویش اینجاگه بمردی</p></div>
<div class="m2"><p>از آن گوی سعادت را تو بردی</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>تو مردستی و هستی حّی زنده</p></div>
<div class="m2"><p>برون تو رفتهٔ اکنون زبنده</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>هر آن کو پیش از مرگم نمیرد</p></div>
<div class="m2"><p>میان حلقهٔ این در نگیرد</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>منت میبینم و در راه من تو</p></div>
<div class="m2"><p>شدی در واصلی آگاه من تو</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>منت بینم ز من خود درگذشتی</p></div>
<div class="m2"><p>حقیقت راه اعیان در نوشتی</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>بسی اندر جهانم سالکانند</p></div>
<div class="m2"><p>که مرکب سوی ما بسیار رانند</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>طلبکار آمدند اندر سوی ما</p></div>
<div class="m2"><p>ولی در عاقبت گشتند شیدا</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>طلبکار آمدند وبازگشتند</p></div>
<div class="m2"><p>نمود سفل و علوی در نوشتند</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>کرا باشد نمود عشق طاقت</p></div>
<div class="m2"><p>که درآخر بیابد این سعادت</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>کسی باید که او از جان نترسد</p></div>
<div class="m2"><p>بجز ما هیچ چیزی او نپرسد</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>بجز ما ننگرد در هر دو عالم</p></div>
<div class="m2"><p>یکی بیند مرا در عین آدم</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>بجز من هیچ در پیشش نگنجد</p></div>
<div class="m2"><p>دو عالم نزد او موئی نسنجد</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>بجز ما هیچ اینجا ننگرد او</p></div>
<div class="m2"><p>بمردی این ره ما بسپرد او</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>چو ره بسپارد اندر سوی درگاه</p></div>
<div class="m2"><p>یکی بیند مرا در جمله آنگاه</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>مرا دیدن در این صورت به نتوان</p></div>
<div class="m2"><p>بوقتی کو ببیند راز پنهان</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>که جان بسپارد و ما را به بیند</p></div>
<div class="m2"><p>ابا ما او در این خلوت نشیند</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>تو ای عطّار جانت برفشاندی</p></div>
<div class="m2"><p>بسی در بحر ما کشتی براندی</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>در این دریای ما دیدی تو جوهر</p></div>
<div class="m2"><p>ترا دیدم در اینجا هفت اختر</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>تمامت در تو اینجا درج کردیم</p></div>
<div class="m2"><p>درون دل تو ما را عین دردیم</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>تو داری در رهم از درد شو فرد</p></div>
<div class="m2"><p>که مردی می نیابی جز که در درد</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>ز درد عشق ما آگاه میباش</p></div>
<div class="m2"><p>بصورت همچنان در راه میباش</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>که من دیدم ترا از جمله مردان</p></div>
<div class="m2"><p>دل و جانت بدیدم شاد و گردان</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>توئی و نزد من جمله عزیزی</p></div>
<div class="m2"><p>که جز با من نباشی و چه چیزی</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>چو جز من در نمیگنجد بر تو</p></div>
<div class="m2"><p>منم در هر دو عالم رهبر تو</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>چو جز من در نمیگنجد بجانت</p></div>
<div class="m2"><p>دمادم مینماید رخ عیانت</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>چو جز من درنمیگنجد درونت</p></div>
<div class="m2"><p>منم اندر درون و در برونت</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>کس کو شرع محبوبم سپارد</p></div>
<div class="m2"><p>بشرع دوستم او پای دارد</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>مراو را اینچنین واصل کنم هان</p></div>
<div class="m2"><p>نمودش جملگی حاصل کنم هان</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>نمایم ذات خود او را تمامت</p></div>
<div class="m2"><p>بفردوسش برم یوم القیامت</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>ببخشم من گناه او سراسر</p></div>
<div class="m2"><p>ندارد اینکه مومن دان تو باور</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>کند این را قبول از جان و دل او</p></div>
<div class="m2"><p>نگردد عاقبت اینجا خجل او</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>مرا ز آن دم که آدم دردمنداست</p></div>
<div class="m2"><p>از آن دم این تمیز اندر پسنداست</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>از آن دم این دم تو هست پیدا</p></div>
<div class="m2"><p>از آن دم یافتی این دم هویدا</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>اگر آن دم در این آدم نبودی</p></div>
<div class="m2"><p>وجود تو در این عالم نبودی</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>از آن دم یافتی این جوهر یار</p></div>
<div class="m2"><p>از آن اینجا همی بینی تو اغیار</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>از آن دم یافتی انوار عالم</p></div>
<div class="m2"><p>نفخت فیه میآید دمادم</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>از آن دم هر دمی اندر دم تست</p></div>
<div class="m2"><p>که دم اندر دم تو آدم تست</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>از آن دم دم زن و زیندم میندیش</p></div>
<div class="m2"><p>رها کن جمله از عالم میندیش</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>از آن دم تو دمادم هر سخن گوی</p></div>
<div class="m2"><p>که بردی درحقیقت در سخن گوی</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>از آن دم دمدمه افکن در آفاق</p></div>
<div class="m2"><p>دمادم که ازآندم جمله عشاق</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>از آن دم در عیان اسرار کل بین</p></div>
<div class="m2"><p>وجود خویشتن انوار کل بین</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>از آن دم این دم تو در جهان است</p></div>
<div class="m2"><p>که بگرفته زمین اندر زمانست</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>از آن دم آدم اینجا خویشتن یافت</p></div>
<div class="m2"><p>عیان بود و ز دید جان و تن یافت</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>از آن دم این دم تو میزند دم</p></div>
<div class="m2"><p>عیان بین تو مر این کلّ دمادم</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>ز دمهائی که اینجاگه زدی تو</p></div>
<div class="m2"><p>دمادم کان معنی بستدی تو</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>ز دمهائی که از دلدار دیدی</p></div>
<div class="m2"><p>حقیقت جملهٔ اسرار دیدی</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>دم آدم از آن دم یافت بودش</p></div>
<div class="m2"><p>از آن دم عین آدم مینمودش</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>چو آدم در بهشت این مرتبت یافت</p></div>
<div class="m2"><p>از آن دم سوی جانان زود بشتافت</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>چو آدم در بهشت جان زد آندم</p></div>
<div class="m2"><p>نظر میکرد و خود میدید آدم</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>عجب درمانده بد در کائنات او</p></div>
<div class="m2"><p>که چون آمد نهان در سوی ذات او</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>نهان با خود دمادم زار میگفت</p></div>
<div class="m2"><p>غم دل با خدا او باز میگفت</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>چو حق درخویشتن میدید تحقیق</p></div>
<div class="m2"><p>بخود میگفت و خود میکرد تصدیق</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>که ای جان جهان و جوهر من</p></div>
<div class="m2"><p>توئی در هژده عالم رهبر من</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>مرا آورده و بنمودهٔ تو</p></div>
<div class="m2"><p>خودی خود بمن بخشودهٔ تو</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>درونم هم توئی بگرفته بیرون</p></div>
<div class="m2"><p>ترا دانم در اینجا سرّ بیچون</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>حجاب تو بود این صورت تو</p></div>
<div class="m2"><p>که عین شوق عشقست صورت تو</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>حجاب از پیش رو بردار و بنمای</p></div>
<div class="m2"><p>که هستی در درون جان تو یکتای</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>چنین تنها مرا اینجا بمگذار</p></div>
<div class="m2"><p>که دانائی مرا کرده پدیدار</p></div></div>