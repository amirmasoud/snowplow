---
title: >-
    بخش ۶۲ - رفتن ابلیس به تلبیس در بهشت در دهان مار از جهت مکر کردن با آدم علیه افضل الصّلوات و اکمل التحیات
---
# بخش ۶۲ - رفتن ابلیس به تلبیس در بهشت در دهان مار از جهت مکر کردن با آدم علیه افضل الصّلوات و اکمل التحیات

<div class="b" id="bn1"><div class="m1"><p>چو شد شیطان سوی جنّت ابا مار</p></div>
<div class="m2"><p>درون آن دهن او ماند بیمار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تفرّج کرد همچون اوّلین او</p></div>
<div class="m2"><p>ز بهر جان آدم در کمین او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بُد از ملعونی و ناپاکی خویش</p></div>
<div class="m2"><p>نظر انداخته اندر پس و پیش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان پیدا شده با عقل و با هوش</p></div>
<div class="m2"><p>زبان دربسته و او گشته خاموش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز خاموشی نظر میکرد آدم</p></div>
<div class="m2"><p>دگر با خویش میآمد دمادم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنان میخواست آن ملعون غدّار</p></div>
<div class="m2"><p>که آدم را کند ز آنجاه آوار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر چاره که او هر ساعت آنجا</p></div>
<div class="m2"><p>عجائب مهرههائی باخت آنجا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که تا فرصت بآدم او بیابد</p></div>
<div class="m2"><p>پس آنگاهی سوی آدم شتابد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنان گردان شده باوی عجب یار</p></div>
<div class="m2"><p>که بُد ابلیس اندر رنج و تیمار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدش آدم چو شاهی خوش نشسته</p></div>
<div class="m2"><p>نظر میکرد مر ابلیس خسته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که آدم عزّ و قرب لامکان داشت</p></div>
<div class="m2"><p>سراز رفعت باوج آسمان داشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز رفعت نور محض و جان جان بود</p></div>
<div class="m2"><p>که جنّات اندرو کلّی نهان بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بصورت بود آدم نور عالم</p></div>
<div class="m2"><p>بدو ریزان شده فیض دمادم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنان از بود او جنّت پرانوار</p></div>
<div class="m2"><p>بد اینجا از نمود فعل جبّار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که حوران و قصوران نور او بود</p></div>
<div class="m2"><p>تو گوئی سر بسر منشور او بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو ابلیس آن همه رفعت عیان دید</p></div>
<div class="m2"><p>ز خشم خویشتن آتش روان دید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چنانش آتش از غیرت فنا کرد</p></div>
<div class="m2"><p>که جانش گشت اینجاگاه پردرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زبان بگشاد آنجاگه بزاری</p></div>
<div class="m2"><p>بگفت ابلیس اگر تو هوشیاری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بخود چیزی تو نتوانی بکردن</p></div>
<div class="m2"><p>بجز اندوه و رنج و غصّه خوردن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نمودی هست اینجا دیدهٔ تو</p></div>
<div class="m2"><p>که اندر عشق صاحب دیدهٔ تو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بزاری پیش حق آنجا بزارید</p></div>
<div class="m2"><p>بس آب حسرت ازدیده ببارید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که یارب می تو دانی راز آدم</p></div>
<div class="m2"><p>بدزدی آمدم اینجا در این دم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>که یارب می تو دانی راز جانم</p></div>
<div class="m2"><p>بدزدی آمدم اینجا نهانم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو دانی و کسی اینجا نداند</p></div>
<div class="m2"><p>که همچون تو نمود توبداند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز احوال منی آگاه یارب</p></div>
<div class="m2"><p>که در اندوه و رنج و محنت و تب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شب و روزم ز دردت دور مانده</p></div>
<div class="m2"><p>میان لعنتم مهجور مانده</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو راندی مر مرا اینجا که آورد</p></div>
<div class="m2"><p>که من هستم ترا من صاحب درد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز درد من هم آگاهی نداری</p></div>
<div class="m2"><p>چو دائم عزّت و شاهی نداری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز درد من تو داری آگهی بس</p></div>
<div class="m2"><p>در این محنت مرا فریادی رس</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زمانی مر مرا مگذار اینجا</p></div>
<div class="m2"><p>که آدم یافتم اینجای تنها</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بتو یک حاجتی دارم نهانی</p></div>
<div class="m2"><p>که راز و حاجتم ای جان تو دانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مرا حاجت بدرگاهت چنانست</p></div>
<div class="m2"><p>که در عالم نمود من عیانست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مرا این حاجتست اینجا و بگذار</p></div>
<div class="m2"><p>که تا آدم کنی زینجای آواز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو طوق تست اندر گردن من</p></div>
<div class="m2"><p>نظر کن اندر این غم خوردن من</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مرا رسوا مکن چون بار دیگر</p></div>
<div class="m2"><p>بعجز من تو ای ستاربنگر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>رها کن تا برم آدم من از راه</p></div>
<div class="m2"><p>دراندازم ورا زین عزّت و جاه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>رها کن تا ز راهش افکنم من</p></div>
<div class="m2"><p>نمود قول او را بشکنم من</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>رها کن تا قضای تو ببیند</p></div>
<div class="m2"><p>در این شادی بلای تو ببیند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>رها کن تا برون آرم ز جنّات</p></div>
<div class="m2"><p>ببیند نیستی جمله ذرّات</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تو میدانی که من راز تو دانم</p></div>
<div class="m2"><p>که اسرار تو و شان تو دانم</p></div></div>