---
title: >-
    بخش ۳۵ - در جواب گفتن پسر پیر دانا را و اسرار گفتن فرماید
---
# بخش ۳۵ - در جواب گفتن پسر پیر دانا را و اسرار گفتن فرماید

<div class="b" id="bn1"><div class="m1"><p>جوابش داد کای پیر پراسرار</p></div>
<div class="m2"><p>چه جوهرها فشاندستی ز گفتار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترا زیبد که گوهر میفشانی</p></div>
<div class="m2"><p>که راز من در اینجا می تو دانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گواه من توئی اینجا حقیقت</p></div>
<div class="m2"><p>سپردستی یقین راه شریعت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میان این همه تو بینظیری</p></div>
<div class="m2"><p>که همدانائی و با عشق پیری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زپیری راه دانستی در اسرار</p></div>
<div class="m2"><p>ترا پیدابود اعیان ز گفتار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>توئی ره برده در اسرار معنی</p></div>
<div class="m2"><p>توئی هم نقطه و پرگار معنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>توئی دریافته اسرار یارا</p></div>
<div class="m2"><p>توئی بشناخته سرّ خدا را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>توئی این دم زده در دید کشتی</p></div>
<div class="m2"><p>درین اسرار ما واقف تو گشتی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>توئی دریافته معنای باطن</p></div>
<div class="m2"><p>ز دید شرع و در تقوای باطن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو داری و تو گفتی آنچه دیدست</p></div>
<div class="m2"><p>یقین جان تو این معنی شنیدست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا تو دید جانی در هدایت</p></div>
<div class="m2"><p>که داری ره عیان سوی سعادت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زهی دریافته اسرار معنی</p></div>
<div class="m2"><p>تو کردستی عیان اسرار معنی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عیانست این بیان و مگذر از او</p></div>
<div class="m2"><p>که جز جانان نباشد هیچ نیکو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز جانان هرچه جوئی آن بیابی</p></div>
<div class="m2"><p>که این دم در میان غرق آبی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز دید واصلان ما را نظر کن</p></div>
<div class="m2"><p>همه جان مرا سمع و بصر کن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مرا اندر میانه با تو کارست</p></div>
<div class="m2"><p>که گر معنی بیابم بیشمارست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>منم منصور با من راست گفتی</p></div>
<div class="m2"><p>دُرِّ اسرار ربّانی تو سُفتی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>منم منصور اینجا رخ نموده</p></div>
<div class="m2"><p>گره از کار عالم برگشوده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرا این ابتدای واصلانست</p></div>
<div class="m2"><p>که ذاتم با همه ذرات پیوست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نمود عشق دارم این زمان من</p></div>
<div class="m2"><p>شده مخفی بر خلق جهان من</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سفر کرده منم در ابتدایش</p></div>
<div class="m2"><p>عیان دیدم جمال انتهایش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز بود خود سفر در خویش کردم</p></div>
<div class="m2"><p>نمود عشق را در پیش کردم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ولی در معنوی و هم بصورت</p></div>
<div class="m2"><p>کنون افتاد کارم را ضرورت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از این پس در سفر چالاک خواهم</p></div>
<div class="m2"><p>ز جمله من نمود پاک خواهم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پدر آوردِ امروزم چنین بین</p></div>
<div class="m2"><p>در این دریا مرا عین الیقین بین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مرا اینست اوّل راه صورت</p></div>
<div class="m2"><p>که بگرفتم من از کلّ تو نورت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مرا بنمودهاند این راز اینجا</p></div>
<div class="m2"><p>که دیدم غایت آغاز اینجا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کنون در عین دریایی چنینم</p></div>
<div class="m2"><p>که درحق اوّلین و آخرینم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دراین دریا بسی نایاب گفتم</p></div>
<div class="m2"><p>دُرِ اسرار حق را من بسُفتم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بسر مردی بزرگست از نمودار</p></div>
<div class="m2"><p>ولی ما را نداند یمن اسرار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ترا این بکر معنی دست دادست</p></div>
<div class="m2"><p>که حق در دیدهٔ جانت نهادست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تو داری زین میان معنی تو داری</p></div>
<div class="m2"><p>حقیقت دید این تقوی تو داری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مرا در عین دریا هست اسرار</p></div>
<div class="m2"><p>اگر اینجا درافزایم به گفتار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کجا حاصل کنم من دیدن دوست</p></div>
<div class="m2"><p>که مغز آمد مرا این صورت و پوست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ایا سالک بیان راز بشنو</p></div>
<div class="m2"><p>نمود شاه از شهباز بشنو</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مرا بنمودهاند اسرار باقی</p></div>
<div class="m2"><p>مئی در دادم اینجا باده ساقی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مئی خوردم من از آن جام اسرار</p></div>
<div class="m2"><p>که ناپیداست جمله پیش دلدار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مئی خوردم که هشیارم نه سرمست</p></div>
<div class="m2"><p>ولی در نیستی دانستهام هست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مئی خوردم که جان محوست در یار</p></div>
<div class="m2"><p>نمیگنجد به جز دلدار دیار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نمیگنجد به جز جانان درونم</p></div>
<div class="m2"><p>که جانان شد درون وهم برونم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نمیگنجد به جز جانان در این دل</p></div>
<div class="m2"><p>که اونگشاد ما را راز مشکل</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>حقیقت دیدهٔدیدار دیدم</p></div>
<div class="m2"><p>ز پیش این جسم را بردار دیدم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نمیدانم که احوالم چه باشد</p></div>
<div class="m2"><p>عیان من در این عالم چه باشد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ولی شرح و بیانم بی شمارست</p></div>
<div class="m2"><p>که دایم پای داری پایدارست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>حقیقت چون نمایم صورت تو</p></div>
<div class="m2"><p>ندانم در جهان من صورت تو</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>حقیقت دم زنم اندر هواللّه</p></div>
<div class="m2"><p>یکی پیدا کنم در دید اللّه</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ولی از حال مستقبل چگویم</p></div>
<div class="m2"><p>که این دم در جهان مانند گویم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>مرا گوئی فلک گرداند در ذات</p></div>
<div class="m2"><p>که میگردد از او دیدار ذرات</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مراگوئی فلک در دید پیداست</p></div>
<div class="m2"><p>که از دیدار من گردان و شیداست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>مراگوئی فلک چون ارزنی است</p></div>
<div class="m2"><p>که خورشید اندرو چون روزنی است</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>مرا گوئی فلک سرگشته باشد</p></div>
<div class="m2"><p>که در کویم حقیقت گشته باشد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بسی سالست از دوران افلاک</p></div>
<div class="m2"><p>که گردانست بر ما دور و یا خاک</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بسی سالست تا بسیار گشتست</p></div>
<div class="m2"><p>که مردم زادگان بسیار کشتست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>مرا شوریست در این بحر اعظم</p></div>
<div class="m2"><p>که یک شب بود در پیشش دو عالم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>مرا شوریست در سر بی نهایت</p></div>
<div class="m2"><p>که گفتم راست ناید در حکایت</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>مرا شوریست اندر عالم جان</p></div>
<div class="m2"><p>که از هر ذره پیدا است طوفان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ز درد عشق جانم جان جان شد</p></div>
<div class="m2"><p>نمود صورتم هر دوجهان شد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ز درد عشق ناپیدا بماندم</p></div>
<div class="m2"><p>تمامت رخت بر دریا فشاندم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ز عین جوهر لا دراِلهم</p></div>
<div class="m2"><p>که بر ذرات عالم پادشاهم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>مرا دیدار باید نه خریدار</p></div>
<div class="m2"><p>که بی شک جان نباشد جز که دیدار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>مرا دردیست هم از دیرگاهی</p></div>
<div class="m2"><p>که درمانست او را مر الهی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>مرا دردیست درمان دوست باشد</p></div>
<div class="m2"><p>ز مغز جان نه بی شک پوست باشد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>مرا دردیست درمانش تو باشی</p></div>
<div class="m2"><p>مرا جانیست جانانش تو باشی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>حقیقت در دمی هستش تو درمان</p></div>
<div class="m2"><p>عیان جان تویی ای جان جانان</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چو درد من دوایی میندانم</p></div>
<div class="m2"><p>حقیقت تو خدایی میندانم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چو دردم دادی و اینجاست درمان</p></div>
<div class="m2"><p>مرا اکنون دوا آمد ز جانان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>توئی جانان و جانها در بر توست</p></div>
<div class="m2"><p>دل و جانها عجایب غمخور توست</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>تویی جانان و اندر جان نهانی</p></div>
<div class="m2"><p>حقیقت راز من پنهان تو دانی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>مرا در سوی این دریا چه کارست</p></div>
<div class="m2"><p>که اندر وی عجایب بی شمارست</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>مرا میباید اینجا عین ذاتت</p></div>
<div class="m2"><p>که لالست این زبان اندر صفاتت</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>صفات و ذات تو هم جانست و هم دل</p></div>
<div class="m2"><p>مرا کردی در این دریا تو واصل</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>حقیقت پیر ره خواهم شدن من</p></div>
<div class="m2"><p>بگو تا کی در این خواهم بُدَن من</p></div></div>