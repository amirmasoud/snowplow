---
title: >-
    بخش ۴۶ - در اسرار قربت شیطان فرماید
---
# بخش ۴۶ - در اسرار قربت شیطان فرماید

<div class="b" id="bn1"><div class="m1"><p>حقیقت بشنو از اسرار او تو</p></div>
<div class="m2"><p>چه اندیشی هم از کردار او تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حقیقت بود او دانسته اسرار</p></div>
<div class="m2"><p>که دائم دیده بُد اعیان اسرار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه اشیاء بر او بود روشن</p></div>
<div class="m2"><p>قدمگاهی بدش از هفت گلشن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه در زیر فرمانش روان بود</p></div>
<div class="m2"><p>چو آب اندر همه اشیا روان بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کنون در فعل کلّی او روانست</p></div>
<div class="m2"><p>ز اشیا دو رو مردود جهان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز وصل ابتدا دارد نشانی</p></div>
<div class="m2"><p>کنون هر لحظهٔ اینجا بیانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بزاری زار چون مینشنود کس</p></div>
<div class="m2"><p>که او بیند یقین اللّه را بس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همی نالد خوشی اندر خرابات</p></div>
<div class="m2"><p>سرش اینجاست یکسان در مناجات</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز وصل اوّلین چون شاد آمد</p></div>
<div class="m2"><p>ز بود لعنتی آزاد آمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که میداند یقین سّر خدا او</p></div>
<div class="m2"><p>که خارج نیست اینجا از خدا او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یقین تحقیق میداند که رحمت</p></div>
<div class="m2"><p>که رحمت از خداوندست و لعنت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شریعت کرد او را لعنت اینجا</p></div>
<div class="m2"><p>که بهتر داند او از رحمت اینجا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چنان او عاشق است ازگفتن دوست</p></div>
<div class="m2"><p>که پنداری که در کلّی همه اوست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنان در عشق جانان در عتابست</p></div>
<div class="m2"><p>ز ذوق عشق جانان در خطابست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خطابش در درون جان بماندست</p></div>
<div class="m2"><p>از آن از بود خود حیران بماندست</p></div></div>