---
title: >-
    بخش ۶۹ - در مکر کردن شیطان آدم را در خوردن گندم و ناپدید شدن شیطان و گندم خوردن حضرت آدم علیه السّلام و الصلاة فرماید
---
# بخش ۶۹ - در مکر کردن شیطان آدم را در خوردن گندم و ناپدید شدن شیطان و گندم خوردن حضرت آدم علیه السّلام و الصلاة فرماید

<div class="b" id="bn1"><div class="m1"><p>از آن تلبیس چون شیطان نهان شد</p></div>
<div class="m2"><p>عجب آدم بماند و ناتوان شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمیدانست اینجا سرّ این کار</p></div>
<div class="m2"><p>که چون شد در بر او ناپدیدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بخود میگفت آیا او کجا رفت</p></div>
<div class="m2"><p>نهان شد از برم آخر چه جا رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندانم این چه کس بود او نمودار</p></div>
<div class="m2"><p>که گفتم راز و او شد ناپدیدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخود اندیشه این میکرد آدم</p></div>
<div class="m2"><p>بمانده در تعجب او دمادم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخود میگفت برمیکرد پنهان</p></div>
<div class="m2"><p>که تا او را چه پیش آید از آنسان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عجائب مانده بُد حیران و غمخوار</p></div>
<div class="m2"><p>نظر میکرد گندم در برابر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بخود میگفت کاینجا مزد حق بود</p></div>
<div class="m2"><p>که ما را ناگهانی روی بنمود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بمن گفت آنچه بُد مر راست اینجا</p></div>
<div class="m2"><p>ندارم من دروغی قول او را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خورم من گندم اینجادر نهانی</p></div>
<div class="m2"><p>اگر باشد قضای آسمانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه آید بر سرم از صانع پاک</p></div>
<div class="m2"><p>تو خواهی زهر باش و خواه تریاک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنان کآید چنان باید یقین دان</p></div>
<div class="m2"><p>نداند جز خدا این راز پنهان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چنان بُد آدم از وسواس غافل</p></div>
<div class="m2"><p>که مانده بود اینجاگاه بیدل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شده ابلیس او را در رگ و پوست</p></div>
<div class="m2"><p>بدو میگفت بین چون جمله از اوست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بخور گندم چرا حیران شدستی</p></div>
<div class="m2"><p>دَرِ اندیشه سرگردان شدستی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بخور گندم مخور یک لحظه تو غم</p></div>
<div class="m2"><p>چرا حیران بماندستی تو آدم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بخور گندم مترس از بود بودت</p></div>
<div class="m2"><p>چنین بیدل شدی چنین چه بودت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بخور گندم که حق گفتست این خَور</p></div>
<div class="m2"><p>یقین اینجایگه فرمان حق بر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بخور گندم ز قول حق بیندیش</p></div>
<div class="m2"><p>حجاب خوف را بردار از پیش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بخور گندم که اسراریست آدم</p></div>
<div class="m2"><p>که حق بنماید از تو در بعالم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بسی اسرار اینجاگه نهانست</p></div>
<div class="m2"><p>ولی آدم عیان آنجاندانست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که ابلیس است او را برده از راه</p></div>
<div class="m2"><p>قضای حق ببین آدم که ناگاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بزد دست و یکی خوشه از آن چید</p></div>
<div class="m2"><p>نهاد اندر دهان و خوش بخائید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یکی طعم لذیذ اندردهانش</p></div>
<div class="m2"><p>پدید آمد عجب راز نهانش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فرو برد آنگهی آن گندم آدم</p></div>
<div class="m2"><p>سه خوشه دیگرش بر کند آدم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بحوّا داد گفتا هان بخور این</p></div>
<div class="m2"><p>که خوش چیزیست نغز و خوب و شیرین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بخور زیرا که این راز نهانست</p></div>
<div class="m2"><p>که حق ما را ندیم جاودانست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همو گفتا مخور هم اودگر گفت</p></div>
<div class="m2"><p>بخورد حوّا چو این اسرار بشنفت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>درون هر دو بُد شیطان مکّار</p></div>
<div class="m2"><p>ز چشم هم دو گشته ناپدیدار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بحوّا گفت بستان و بخور زود</p></div>
<div class="m2"><p>که تا آدم کنی از خویش خشنود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نه او خورد و نبُد رنجی وِراهم</p></div>
<div class="m2"><p>ببر تو این زمان فرمان آدم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تو را نیکو بود اما چو خوردی</p></div>
<div class="m2"><p>چو آدم نیز تو هم گوی بردی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ستد حوّا از آدم گندم خوب</p></div>
<div class="m2"><p>ببردش عاقبت فرمان محبوب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نهاد اندر دهان و خورد حوّا</p></div>
<div class="m2"><p>مر او را گشت مر لرزی هویدا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو حوّا گندم ازحیرت بخائید</p></div>
<div class="m2"><p>ز سر تا پای چون بیدی بلرزید</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بحای حلّه از هر دو جدا شد</p></div>
<div class="m2"><p>نمود هر دومنثور و هبا شد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بپرّید از برهر دو چو دو طیر</p></div>
<div class="m2"><p>همی کردند ایشان هر دو آن سیر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در آن اسرار چون حیران بماندند</p></div>
<div class="m2"><p>عجائب خوار و سرگردان بماندند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چنان حیران شدند ایشان و خاموش</p></div>
<div class="m2"><p>ز حیرانی عجب گشتند مدهوش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>برهنه هر دو تن شیدا بمانده</p></div>
<div class="m2"><p>میان حوریان رسوا بمانده</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز رسوائی و شرم اهل جنّت</p></div>
<div class="m2"><p>فتاده هر دو در اندوه و محنت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز رسوائی که آنجا یافت آدم</p></div>
<div class="m2"><p>بتر از مرگ او را بُد دمادم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تمامت اهل حوران و قصوران</p></div>
<div class="m2"><p>فرومانده عجب در حال ایشان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>عجب در حال ایشان مانده بودند</p></div>
<div class="m2"><p>نه چون ابلیسایشان رانده بودند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>از آن ابلیس بُد شادان و خندان</p></div>
<div class="m2"><p>که بر آدم شده جنت چو زندان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بشادی هر زمان خوش خوش بخندید</p></div>
<div class="m2"><p>چو آدم سرّ خود آن دم چنان دید</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سر افکنده به پیش از شرمساری</p></div>
<div class="m2"><p>ز دیده همچو باران بهاری</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بزاری زار و گریان گشت آدم</p></div>
<div class="m2"><p>جگر از سوز بریان گشت آدم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ستاده قائم و دستش پس و پیش</p></div>
<div class="m2"><p>ز بهر ستر خود او مانده دلریش</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نمیدانست تا او را چه آید</p></div>
<div class="m2"><p>که کامش جملگی از پیش بستد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نمیدانست چه چاره کند او</p></div>
<div class="m2"><p>شکسته مر سبو اندرلب جو</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چه چاره چون بشد از دست تدبیر</p></div>
<div class="m2"><p>بباید کرد در هر کار تأخیر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چوکاری میکنی اینجا یقین تو</p></div>
<div class="m2"><p>سزد کاینجای باشی پیش بین تو</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>همیشه پیش بین کار خود باش</p></div>
<div class="m2"><p>که تا گردد ترا اسرار آن فاش</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>نکردی پیش بینی دل در آن کار</p></div>
<div class="m2"><p>فرومانی تو اندر رنج و تیمار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>نکردی پیش بینی جان بدادی</p></div>
<div class="m2"><p>بهر زه در بلای دل فتادی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>نکردی پیش بینی همچو او تو</p></div>
<div class="m2"><p>زدی بر سنگ و بشکستی سبو تو</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نکردی پیش بینی و بماندی</p></div>
<div class="m2"><p>خود از درگاه حق هرزه براندی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نکردی پیش بینی و شدی خوار</p></div>
<div class="m2"><p>بسرگردان شدی مانند پرگار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>نکردی پیش بینی همچو مردان</p></div>
<div class="m2"><p>بلای عشق را بیحد و مرز دان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>نکردی پیش بینی در بلا تو</p></div>
<div class="m2"><p>شدی مانند آدم مبتلا تو</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>نکردی پیش بینی بر سر چاه</p></div>
<div class="m2"><p>بچاه انداختی خود را بناگاه</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>نکردی پیش بینی اوّل کار</p></div>
<div class="m2"><p>که تا آخر شدی در غم گرفتار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>نکردی پیش بینی از پس راز</p></div>
<div class="m2"><p>بماندی همچو مرغان در تک و تاز</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>نکردی پیش بینی در نظر تو</p></div>
<div class="m2"><p>از آن ماندی چنین زیر و زبر تو</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>نکردی پیش بینی همچو حوّا</p></div>
<div class="m2"><p>فتادی همچنین مجروح و رسوا</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>نکردی پیش بینی چند گویم</p></div>
<div class="m2"><p>که تا مردرد جانان چاره جویم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>دریغا نیست سودی جز زیانت</p></div>
<div class="m2"><p>که شد فاش اندر اینجا داستانت</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بدست خود زدی بر پای تیشه</p></div>
<div class="m2"><p>درخت شوق برکندی ز ریشه</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>بدست خود زدی خود بر سر خود</p></div>
<div class="m2"><p>که خود بودی در اینجا رهبر خود</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بدست خود تبه کردی تو سودت</p></div>
<div class="m2"><p>چه چاره چونکه دزدی فاش بودت</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بدست خود بچاه انداختی خود</p></div>
<div class="m2"><p>وجود خویشتن درباختی خود</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بدست خود تو گندم خوردهٔ خود</p></div>
<div class="m2"><p>در اینجا خویش رسواکردهٔ خود</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>تو رسوای جهانی ای دل آزار</p></div>
<div class="m2"><p>فرومانده عجائب سخت افگار</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>تو رسوای جهانی در نظاره</p></div>
<div class="m2"><p>چو آدم می نداری هیچ چاره</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>تو رسوائی و اندر خون فتادی</p></div>
<div class="m2"><p>ز عزّ پردهات بیرون فتادی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>تو رسوائی و اکنون چارهٔ نیست</p></div>
<div class="m2"><p>بجز حق مر ترا خود چارهٔ نیست</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>توئی رسوا و شیدا خون شده دل</p></div>
<div class="m2"><p>که بگشاید ترا این راز مشکل</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بتن عریان و بی ستری وغمخوار</p></div>
<div class="m2"><p>فرومانده عجائب سخت افگار</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>در این دنیای غدّاری فتاده</p></div>
<div class="m2"><p>بدست خود تو سر بر باد داده</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>در این دنیای غدّاری چو مردار</p></div>
<div class="m2"><p>فتاده در نهاد خود گرفتار</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>چو خود کردی کرا تاوان کنی تو</p></div>
<div class="m2"><p>که تا مر درد خود درمان کنی تو</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>چودر دردی فتادی نیست درمان</p></div>
<div class="m2"><p>مدان این درد خود ای دوست آسان</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>چو در دردی چنین تو مبتلائی</p></div>
<div class="m2"><p>چو آدم این زمان عین بلائی</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>چو آدم آنچنان بد ایستاده</p></div>
<div class="m2"><p>تن اندر حکم ایزد باز داده</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>تن اندر حکم و جان اندر کف دست</p></div>
<div class="m2"><p>ستاده در بلای نیستی هست</p></div></div>