---
title: >-
    بخش ۹۱ - حکایت در ادب و عزّت نگاهداشتن در حضرت باری فرماید
---
# بخش ۹۱ - حکایت در ادب و عزّت نگاهداشتن در حضرت باری فرماید

<div class="b" id="bn1"><div class="m1"><p>حقیقت چون ز عزت دم نهانی</p></div>
<div class="m2"><p>ز دید اینجا کمال جاودانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مر ایشان گشت حاصل در یکی باز</p></div>
<div class="m2"><p>یقین دیدند هم انجام و آغاز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در این دم چو تو در عزت درآئی</p></div>
<div class="m2"><p>حقیقت این گره را برگشائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یقین از عزت اینجا راز بین تو</p></div>
<div class="m2"><p>یقین هم جان جان را بازبین تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترا جانان نموده روی بنگر</p></div>
<div class="m2"><p>بجز نور حق از هر سوی منگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عیان ذرّات اینجا هست اظهار</p></div>
<div class="m2"><p>ز چشم تو عیانی ناپدیدار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عیان ذات اینجا آشکارست</p></div>
<div class="m2"><p>اگر دانی همه دیدار یارست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عیان ذات بنگر در دل و جان</p></div>
<div class="m2"><p>که از ذرّات آمد جمله پنهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طلبکارند و تو این سر ببینی</p></div>
<div class="m2"><p>ز من دریاب اگر صاحب یقینی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از این صورت گذر میکن دمادم</p></div>
<div class="m2"><p>حقیقت صبر میکن بر دو عالم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درون خویش بنگر بین تو هم او</p></div>
<div class="m2"><p>اگر بینی چنین دانَمت نیکو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دو عالم در تو و تو دردو عالم</p></div>
<div class="m2"><p>ترا مخفی است اسرار اندر این دم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دو عالم در تو و تو از دو بینی</p></div>
<div class="m2"><p>ز من دریاب اگر صاحب یقینی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دو عالم آن زمان بینی تو در خَود</p></div>
<div class="m2"><p>که یکسانت نماید نیک با بد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دو عالم آن زمان یکسان به بینی</p></div>
<div class="m2"><p>که خود پیدا و هم پنهان به بینی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بهر اندیشه کاینجاگه تو کردی</p></div>
<div class="m2"><p>رهی در سوی آن حضرت ببردی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ره آسانست لیکن بی تو دشوار</p></div>
<div class="m2"><p>بکردستی در اینجاگه بیکبار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ره آسانست دشواری هم از تست</p></div>
<div class="m2"><p>رها کن صورت و در راه او جست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو مردان باش و دم از لامکان زن</p></div>
<div class="m2"><p>که این دم میزنند اینجای مر زن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زنان راه تو مردان بیابند</p></div>
<div class="m2"><p>چنان قربی که مر ایشان نیابند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زنان راه او را خاک شو تو</p></div>
<div class="m2"><p>حقیقت از تمامت پاک شو تو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز آلایش برون آی و دم یار</p></div>
<div class="m2"><p>بپاکی زن اگر هستی خبردار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بپاکی راه حق بیشک توان یافت</p></div>
<div class="m2"><p>بپاکی در مقام جان جان یافت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بپاکی میتوانی یافت حق تو</p></div>
<div class="m2"><p>ز مردان اندر اینجاگه سبق تو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بپاکی میتوانی گر بری راه</p></div>
<div class="m2"><p>یقین اینجایگه تاحضرت شاه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بپاکی روی او دانی توان یافت</p></div>
<div class="m2"><p>که نتوان روی او هر ناتوان یافت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بتقوی روی جانان میتوان یافت</p></div>
<div class="m2"><p>مر این دشوار آسان میتوان یافت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بوقتی کین نهادت پاک گردد</p></div>
<div class="m2"><p>یقینت در نهاد خاک گردد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تن و دل پاک دار اندر بر خاک</p></div>
<div class="m2"><p>حقیقت محو گردد تا شود پاک</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>وگرنه در عیان و زندگانی</p></div>
<div class="m2"><p>یقین میدان که در پاکی بمانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دو تقوی هست در معنی بگویم</p></div>
<div class="m2"><p>وگر چاره در اینجا من بجویم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ترا این سرّ معنی در نمودار</p></div>
<div class="m2"><p>بس است این گر شوی از جان خبردار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یکی تقوی و ظاهر امر فرمان</p></div>
<div class="m2"><p>که بسپاری حقیقت راه جانان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بتقوی میتوانی یافت این سر</p></div>
<div class="m2"><p>که بیشک انبیا تقوای ظاهر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در اینجاگه نمودند وشدند دوست</p></div>
<div class="m2"><p>بیابد مغز آنگاهی یقین پوست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دم تقوی بباطن گر توانی</p></div>
<div class="m2"><p>بری ره سوی جانان ناگهانی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>حقیقت تقوی باطن همی جوی</p></div>
<div class="m2"><p>که ناگاهی بری از وصل او گوی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تو باطن پاک دار و کمترک خور</p></div>
<div class="m2"><p>که ذرّه ناگهان آید سوی خور</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تو باطن پاک دارد از هر خیانت</p></div>
<div class="m2"><p>که عرضاً عرضه کردم در امانت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تو باطن پاک دار و دوست دریاب</p></div>
<div class="m2"><p>بمعنی تو توئی دوست دریاب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تو باطن پاک میدار از طبیعت</p></div>
<div class="m2"><p>بتقوی باش درعین شریعت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تو باطن پاک دار و خواب کم کن</p></div>
<div class="m2"><p>حقیقت خورد و خواب خویش کم کن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز خورد و خواب اینجا تاتوانی</p></div>
<div class="m2"><p>گریزان باش تاگردی معانی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز خورد و خواب بگذر همچو مردان</p></div>
<div class="m2"><p>ز بیداری نظر کن جان جانان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز خورد و خواب تو چیزی ندیدی</p></div>
<div class="m2"><p>بجز رنج و غم و گند و پلیدی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ز خورد و خواب تا بودی غمت بود</p></div>
<div class="m2"><p>دریغا خورد و خوابت زودتر بود</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز خورد و خواب دیدی رنج بسیار</p></div>
<div class="m2"><p>غم و اندوه ماندستی گرفتار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز خورد و خواب مردان در گذشتند</p></div>
<div class="m2"><p>ره جانان بیک ره در نوشتند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز بی خوابی و کم خواری در اینجا</p></div>
<div class="m2"><p>کشیدند رنج و هم خواری در اینجا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز بیخوابی جمال یار دیدند</p></div>
<div class="m2"><p>ز کم خواری بکام دل رسیدند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ز بی خوابی نهانشان درگشودند</p></div>
<div class="m2"><p>نگه کردند بیشک دوست دیدند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ز بیخوابی در اینجا قربت دوست</p></div>
<div class="m2"><p>گزیدند و برون رفتند از پوست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ز بی خوابی عیان ذات بیچون</p></div>
<div class="m2"><p>شدند اینجایگه خوش بیچه و چن</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ز بیخوابی در اینجا راز مطلق</p></div>
<div class="m2"><p>شدند وآنگهی گفتند اناالحق</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>اناالحق آن زمان گفتند در ذات</p></div>
<div class="m2"><p>که بیشک جان جان شد جمله ذرّات</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>اناالحق آن زمان گفتند بیخود</p></div>
<div class="m2"><p>که یکسان گشت جمله نیک با بد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>اناالحق آن زمان گفتند در راز</p></div>
<div class="m2"><p>که یکی گشتشان انجام و آغاز</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>اناالحق آن زمان گفتند با خلق</p></div>
<div class="m2"><p>که فارغ آمدند از دام وز دلق</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بدانستند چندی و بگفتند</p></div>
<div class="m2"><p>دَرِ این راز خود با کس نگفتند</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بدانستند چندی راز تقلید</p></div>
<div class="m2"><p>ولی شان مینمود این سر توحید</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بدانستند چندی سرّ این راز</p></div>
<div class="m2"><p>حجاب انداختند از پیش خود باز</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بدانستند چندی در نهانی</p></div>
<div class="m2"><p>غلط کردند بی تو تا ندانی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بدانستند چندی از شنفته</p></div>
<div class="m2"><p>ولی بیدار کی باشد چو خفته</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بدانستند چندی در نمودار</p></div>
<div class="m2"><p>شده آگه بکل از سرّ این کار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>نهان گفتند با یکدیگر اینجا</p></div>
<div class="m2"><p>که راز پادشه نارند پیدا</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>نهان گفتند با یکدیگر اینجا</p></div>
<div class="m2"><p>که راز دوست را نارند پیدا</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>نه هر کس صاحب اسرار باشد</p></div>
<div class="m2"><p>نه هر سر لایق دیدار باشد</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بقدر جمله دارم هم بگفتار</p></div>
<div class="m2"><p>بیانها مینمایم اندر اسرار</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>کسانی کین طلب دارند در دل</p></div>
<div class="m2"><p>که ناگه پی برند این راز مشکل</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ببازی نیست بخشایش حقیقت</p></div>
<div class="m2"><p>سپردن بایدت راه شریعت</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ره اینجاگه سپاری دوست گردد</p></div>
<div class="m2"><p>حقیقت مغز هم بی پوست گردد</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ره جان کن که درجانست جانان</p></div>
<div class="m2"><p>در اینجا جملگی اسرار جویان</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>وصالش جمله در جان باز دیدند</p></div>
<div class="m2"><p>چو فی الجمله بکام خود رسیدند</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>در اینجاهیچ و جملهاند سرمست</p></div>
<div class="m2"><p>فتاده از بلندی در سوی پست</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>نداند اوّل و آخر تمامت</p></div>
<div class="m2"><p>نمودند بازمانده در ملامت</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>اگرچه بازگوید همچنانست</p></div>
<div class="m2"><p>ولیکن در جنون راز نهان است</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بهر نوعی یقین بسیار گویند</p></div>
<div class="m2"><p>همه از دیدن دلدارگویند</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ولیکن کس نمیداند یقین راز</p></div>
<div class="m2"><p>که شرحی گویم از انجام و آغاز</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>همه حیران و گویا در خموشند</p></div>
<div class="m2"><p>چو دیگی اندر این سودا بجوشند</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>در این سودا فتادستند بسیار</p></div>
<div class="m2"><p>نیامد هیچ اینجایگه پدیدار</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>که تا برگوید اینجا راز جانان</p></div>
<div class="m2"><p>ببازد اندر اینجا گوهر جان</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>سر و جان هر دو دربازد بپایش</p></div>
<div class="m2"><p>بیابد ابتدا و انتهایش</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>سر و جان گر ببازی این بود راز</p></div>
<div class="m2"><p>حجاب افتد ز رویش بیشکی باز</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>بدانی و بگوئی بعد از این تو</p></div>
<div class="m2"><p>چو بردی ره سوی عین الیقین تو</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>رهی نابردهٔ در پردهٔ راز</p></div>
<div class="m2"><p>که تا بینی حقیقت ناگهی باز</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>رخ معشوق تا چندی از این درد</p></div>
<div class="m2"><p>شوی از بود اشیا جملگی فرد</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>چراگفتار بیهوده درآئی</p></div>
<div class="m2"><p>که در گفتارمانند درائی</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>چرا گفتار بنمودی تو چندین</p></div>
<div class="m2"><p>از آن داری تو در اسرار حق بین</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>از آن کین جاتو گفتی پیش هر کس</p></div>
<div class="m2"><p>نمود دوست آخر چند از این بس</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>بگوی و خوف جان از پیش بردار</p></div>
<div class="m2"><p>بیک ره دل ز جان خویش بردار</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>به یک ره دل ببر از عالم دون</p></div>
<div class="m2"><p>که تا آخر نماید دور گردون</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>نخواهد مانددور آفرینش</p></div>
<div class="m2"><p>تو بگشا این زمان اسرار بینش</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>بیک ره کن مبدّل صورت جان</p></div>
<div class="m2"><p>فنا کن هر دو اندر ذات جانان</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>نداری هیچ باید اندر این راه</p></div>
<div class="m2"><p>بماندستی چو موری در بن چاه</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>قدم در راه نه میرو یقین تو</p></div>
<div class="m2"><p>چو مردان باش اینجا پیش بین تو</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>قدم اینجا نه و این سر تو دریاب</p></div>
<div class="m2"><p>که مقصودت شود حاصل از این باب</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>از آن درهر چه جوئی میدهندت</p></div>
<div class="m2"><p>یقین میدان که منّت مینهندت</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>نباشد منّت اینجا هرچه دارند</p></div>
<div class="m2"><p>همه از بهر تو اینجا بکارند</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>در اینجا هر چه دادند کی ستانند</p></div>
<div class="m2"><p>ازآن می هیچ کس این سر ندانند</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>که دل باید که با جان همدم راز</p></div>
<div class="m2"><p>بود تا این بیابد بیشکی باز</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>چو جان و دل ابا هم یار باشند</p></div>
<div class="m2"><p>یقین توحید هم اینجا بباشند</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>نمود هر دو با رفعت شود باز</p></div>
<div class="m2"><p>بود تا این بیابد بی شکی باز</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>بوقتی جان شود تن اندر اینجا</p></div>
<div class="m2"><p>که گردد باطنت کلّی مصفّا</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>بوقتی جان شود جان حقیقت</p></div>
<div class="m2"><p>که بسپارد بکل سرّ شریعت</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>بوقتی جان شود جانان در این راز</p></div>
<div class="m2"><p>که محو آید ورا انجام و آغاز</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>بوقتی جان شود جانان در اینجا</p></div>
<div class="m2"><p>که پنهانی شود ناگاه پیدا</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>بوقتی جان شود جانان یقین دان</p></div>
<div class="m2"><p>که گردد جسم از دیدار پنهان</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>بوقتی جان شود جانان که بینی</p></div>
<div class="m2"><p>یقین این سر اگر صاحب یقینی</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>که پنهان گردد این صورت عیانت</p></div>
<div class="m2"><p>شود بیشک حقیقت جان جانت</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>چو صورت از میان برخواست جانست</p></div>
<div class="m2"><p>پس آنگه دیدن جان جهانست</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>چوصورت از میان برخواست بیشک</p></div>
<div class="m2"><p>سراسر بینی اینجا در یکی یک</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>بود یکی شده صورت عیان گم</p></div>
<div class="m2"><p>جهان اندر وی و وی در جهان گم</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>بسی راهست در گم بودن اینجا</p></div>
<div class="m2"><p>رهی نیکو طلب ای مرد دانا</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>رهت آخر فنای جاودانست</p></div>
<div class="m2"><p>در آخر کار بی نام ونشانست</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>در آخر کار بیکاریست تحقیق</p></div>
<div class="m2"><p>نهان گشتن پس آنگه راز توفیق</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>نهان خواهی شدن ای دل نهانی</p></div>
<div class="m2"><p>نمیدانم ولی دانم که دانی</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>در آخر رازت اینجاگه نهان است</p></div>
<div class="m2"><p>ترا پیدانمودن جان جانست</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>در آخر میشوی اینجا فنا تو</p></div>
<div class="m2"><p>که تا یابی یقین دید بقا تو</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>در آخر میشوی مر ناپدیدار</p></div>
<div class="m2"><p>در آنسو میشوی کلّی پدیدار</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>در آخر اصل او گر باز جوئی</p></div>
<div class="m2"><p>سزد این سر که با هر کس نگوئی</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>ولیکن چون کنی در عین گفتار</p></div>
<div class="m2"><p>که حق گویاست اندر کلّ اسرار</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>چو حق گویاست حق میگوید این راز</p></div>
<div class="m2"><p>بگو تاکه در اینجا بشنود باز</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>یقین دریاب این اسرار بنگر</p></div>
<div class="m2"><p>نظر کن زود این گفتار بنگر</p></div></div>