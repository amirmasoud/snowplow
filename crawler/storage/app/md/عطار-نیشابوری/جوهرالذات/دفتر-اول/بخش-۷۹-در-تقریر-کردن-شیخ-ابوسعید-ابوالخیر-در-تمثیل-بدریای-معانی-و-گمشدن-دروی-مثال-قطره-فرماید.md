---
title: >-
    بخش ۷۹ - در تقریر کردن شیخ ابوسعید ابوالخیر در تمثیل بدریای معانی و گمشدن دروی مثال قطره فرماید
---
# بخش ۷۹ - در تقریر کردن شیخ ابوسعید ابوالخیر در تمثیل بدریای معانی و گمشدن دروی مثال قطره فرماید

<div class="b" id="bn1"><div class="m1"><p>چنین گفت آن بزرگ پیر اعظم</p></div>
<div class="m2"><p>پناه دین و سلطان معظم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بحق محبوب حق عین شریعت</p></div>
<div class="m2"><p>سپهسالار دین شاه حقیقت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سلیمان سخن در منطق الطّیر</p></div>
<div class="m2"><p>که آنکس بوسعید است و ابوالخیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ابوالخیر است دائم خیر حق بود</p></div>
<div class="m2"><p>که بُد اینجایگه دیدار معبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یقین دریافت اسرار معانی</p></div>
<div class="m2"><p>که او را بود کل صاحبقرانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یقین ازنور حق بود او نمودار</p></div>
<div class="m2"><p>ز شاهی داشت اینجا زینت و کار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همش دنیا همش عقبی فزون بود</p></div>
<div class="m2"><p>حقیقت رهبران را رهنمون بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همش گنج صور هم گنج معنی</p></div>
<div class="m2"><p>ورا بود ای پسر از گنج تقوی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنان رفعت که او اندر جهان دید</p></div>
<div class="m2"><p>کسی دیگر بخواب آن کی توان دید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنین گفت او که اندر کلّ احوال</p></div>
<div class="m2"><p>نشان بی نشان جستم به سی سال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به سی سال اندر اینجا خوندل من</p></div>
<div class="m2"><p>بخوردم بی نمود آب و گل من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حجابم یک شبی برخواست ازپیش</p></div>
<div class="m2"><p>نگه کردم من اندر جوهر خویش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو دیدم بحر جستم گم شدم من</p></div>
<div class="m2"><p>چو یک قطره که در قلزم شدم من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نظر کردم درون و هم برونم</p></div>
<div class="m2"><p>حقیقت حق بد اینجا رهنمونم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صفات خویشتن در ذات دیدم</p></div>
<div class="m2"><p>نمود جسم ودل در ذات دیدم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>درون کل نظر کردم من ازجان</p></div>
<div class="m2"><p>چو دیدم در حقیقت راز پنهان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یکی دریای بی پایان بدم من</p></div>
<div class="m2"><p>در آندریا عجب غرقه شدم من</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نمود ذات دیدم در دل خود</p></div>
<div class="m2"><p>فروماندم میان مشکل خود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عجائب حیرتم در دل فزون شد</p></div>
<div class="m2"><p>ولیکن عشق با من رهنمون شد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دلا دربحر لارفتم ابی خود</p></div>
<div class="m2"><p>ندیدم هیچ آنجا نیک هم بد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نظر کردم همه یکسان نمودم</p></div>
<div class="m2"><p>که من خود در میان واقف نبودم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ندیدم غیر در دریای جانان</p></div>
<div class="m2"><p>شدم از عشق ناپروای جانان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یکی دیدم در آنجاجمله اشیا</p></div>
<div class="m2"><p>ز پنهانی شده در بحر پیدا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همه در بحر موجود ونبُد هیچ</p></div>
<div class="m2"><p>ولیکن در نظر بُد نقش پرپیچ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همه گمگشته بود و حق شده فاش</p></div>
<div class="m2"><p>بهر کسوت نموده روی نقّاش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز آب بحربنگر هر چه بینی</p></div>
<div class="m2"><p>بکن فهمی اگر صاحب یقینی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همه از آب دریاگشته پیدا</p></div>
<div class="m2"><p>همه در آب حیرانند و شیدا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همه در آب بنگر رخ نموده</p></div>
<div class="m2"><p>ببسته بُد گره خود برگشوده</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همه در آب و هم اندر همه جان</p></div>
<div class="m2"><p>نمود سرّخود در بحر جانان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بهر نوعی که میدیدم ز اسرار</p></div>
<div class="m2"><p>نمود شاه بُد آنجا پدیدار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نمود شاه بُد نی غیر دیدم</p></div>
<div class="m2"><p>همه در آب دریا سیر دیدم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همه در آب و فارغ گشته ازآب</p></div>
<div class="m2"><p>فتاده جملگی اندر تب و تاب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همه در آب دریا وصل جویان</p></div>
<div class="m2"><p>بسر در عشق او هر لحظه پویان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همه در بحر آب و گشته غرقاب</p></div>
<div class="m2"><p>همه در آب و گشته طالب آب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>صدف را جوهر او درنهادش</p></div>
<div class="m2"><p>در این بحر عیانی داد دادش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>صدف دُر داشت جوهر نیز بر سر</p></div>
<div class="m2"><p>شده در راه او بی پا و بی سر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همه گویا دل و خامش دهانان</p></div>
<div class="m2"><p>طلبکار آمده در نزد جانان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همه در آب هم او را طلبکار</p></div>
<div class="m2"><p>زهی قدرت زهی صنع جهاندار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>طلبکارند چون جمله بدانی</p></div>
<div class="m2"><p>تو نیز اینجایگه راز نهانی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>طلبکارند و آب و جمله جانست</p></div>
<div class="m2"><p>که همچون دوست بی نقش و نشانست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نشان دارند کل در بینشانی</p></div>
<div class="m2"><p>همی جویند حیات جاودانی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تو چون ایشان میان آب غرقی</p></div>
<div class="m2"><p>ولی اینجایگه در دید فرقی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بسی فرقست ازمه تا بماهی</p></div>
<div class="m2"><p>ولی یکسانست نزدیک الهی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بسی فرقست اینجا چون ببینی</p></div>
<div class="m2"><p>ولیکن حق زجمله برگزینی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>همه یکرنگ دان در عین دریا</p></div>
<div class="m2"><p>که در دریا شدند این جمله پیدا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ز هر دواصل دارند و وطن آب</p></div>
<div class="m2"><p>ولیکن این معانی زود دریاب</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تفاوت از سلوک و خلوت افتاد</p></div>
<div class="m2"><p>که جوهر در صدف سر داد بر باد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بخلوت صبر کرد و شاد بنشست</p></div>
<div class="m2"><p>ز جمله فارغ و آزاد بنشست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سکون کرد و ز ذات کل عیان شد</p></div>
<div class="m2"><p>پس آنگه لایق هر شایگان شد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ببین تا لاجرم اینجا بهایش</p></div>
<div class="m2"><p>چگونه هست مر نور لقایش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>فروغش روشنی دل فزاید</p></div>
<div class="m2"><p>شب تاریک ظلمت در رُباید</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ز نورست و حقیقت نور باشد</p></div>
<div class="m2"><p>به پیش سالکان مشهور باشد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>دُر ارچه اصل آبست هم بغایت</p></div>
<div class="m2"><p>بود از این و آن فرقی تفاوت</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تفاوت آمدست اینجای از اصل</p></div>
<div class="m2"><p>ز اصل اینجا بیابی ناگهی وصل</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>در این دریا توئی اینجا صدف وار</p></div>
<div class="m2"><p>دهان بر بسته و بنشسته ناچار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>در این بحر فنا بر بن نشسته</p></div>
<div class="m2"><p>دهان خویش از حسرت ببسته</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>یکی جوهر درون سینه داری</p></div>
<div class="m2"><p>چو ایشان نیز تو گنجینه داری</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>عجایب جوهری داری تو نادان</p></div>
<div class="m2"><p>نمیدانی ترا از این چه تاوان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>عجائب جوهری داری شب افروز</p></div>
<div class="m2"><p>که شب گردد ز تاب نور آن سوز</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>عجائب جوهری شاهانه داری</p></div>
<div class="m2"><p>ولیکن در صدف دُردانه داری</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ترا این جوهر از هر دو جهان است</p></div>
<div class="m2"><p>درونش جوهری دیگر نهان است</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>اگر این جوهر اینجا یافتی باز</p></div>
<div class="m2"><p>ترا باشد از آن تمکین و اعزاز</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>اگر بی جوهر اینجاگه بمانی</p></div>
<div class="m2"><p>چو ماهیدر بُن این چه بمانی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>طلب کن جوهر این ذات اینجا</p></div>
<div class="m2"><p>مشو غرقه چنین در عین دریا</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>طلب کن جوهر بیچون ببین ذات</p></div>
<div class="m2"><p>نمود عین گردون بین در ذات</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>از این دُرهای معنی زود بگزین</p></div>
<div class="m2"><p>درون هر یکی یک جوهری بین</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چنین مستغرق دریا شدستی</p></div>
<div class="m2"><p>که از بودت تو ناپروا شدستی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>دمی زین بحر کن آخر نظاره</p></div>
<div class="m2"><p>که اینجا جوهری اندر کناره</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>شده پیدا صدف با اوست مرده</p></div>
<div class="m2"><p>به پیشِ عینِ جوهر جان سپرده</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>سپرده جان ودیده روی جوهر</p></div>
<div class="m2"><p>نمود عشق گشته ناگهی دَر</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>هلاکیّت فتاده مر صدف وار</p></div>
<div class="m2"><p>تو از آن جوهر اینجاگه بکف آر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>چو جوهر یافتی بنگر شعاعش</p></div>
<div class="m2"><p>نمود جسم و جان کن بس وداعش</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>کجا یابی تو اینجا جوهر ذات</p></div>
<div class="m2"><p>که سرگردانی اندر بحر ذرّات</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>تو در بحریّ و جوهر مینجوئی</p></div>
<div class="m2"><p>بیان چند زر تا چند گوئی</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>جواهر جوی از دریایِ معنی</p></div>
<div class="m2"><p>اگر هستی ز دل دارای معنی</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>جواهر جوی همچون پادشاهان</p></div>
<div class="m2"><p>چو جوهر یافتی برگو چو شاهان</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>خوشا آندم که جوهر باز بینی</p></div>
<div class="m2"><p>وزان هم عزت اندر ناز بینی</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ترا جوهر درون جسم و جانست</p></div>
<div class="m2"><p>کنون از چشم صورتگر نهانست</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>نهانست این همه درجوهر ذات</p></div>
<div class="m2"><p>خروشانند اینجا جمله ذرّات</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>طلبکارند او را جمله اینجا</p></div>
<div class="m2"><p>بماند جملگی سرگشته اینجا</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>همه جویای جوهر در همه درج</p></div>
<div class="m2"><p>نمیدانند این جوهر ورا ارج</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>نداند ارج این جوهر مگر آن</p></div>
<div class="m2"><p>که دریابد حقیقت جان جانان</p></div></div>