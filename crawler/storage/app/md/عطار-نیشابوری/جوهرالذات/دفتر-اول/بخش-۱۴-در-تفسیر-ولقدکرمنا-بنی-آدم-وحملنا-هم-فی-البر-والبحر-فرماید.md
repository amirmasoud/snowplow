---
title: >-
    بخش ۱۴ - در تفسیر وَلَقَدکَرَّمنا بَنی آدَمَ وَحَمَّلنا هُم فی البَرِّ وَالبَحر فرماید
---
# بخش ۱۴ - در تفسیر وَلَقَدکَرَّمنا بَنی آدَمَ وَحَمَّلنا هُم فی البَرِّ وَالبَحر فرماید

<div class="b" id="bn1"><div class="m1"><p>الا ای جان و دل را درد و دارو</p></div>
<div class="m2"><p>تو آن نوری که لَم تَمسَسهُ نارو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو درمشکات تن مصباح نوری</p></div>
<div class="m2"><p>ز نزدیکی که هستی دور دوری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز روزنهای مِشکات مشبک</p></div>
<div class="m2"><p>نشیمن کردهٔ خاک مبارکت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زجاجه بشکن و زَیتَت برون ریز</p></div>
<div class="m2"><p>بنور کوکب درّی درآویز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترا با مشرق و مغرب چه کار است</p></div>
<div class="m2"><p>که نور آسمان گردن حصار است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بینائی مدان این فرّ و فرهنگ</p></div>
<div class="m2"><p>که گنجشکی بپّرد بیست فرسنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو آن نوری که اندر بام افلاک</p></div>
<div class="m2"><p>همی گشتی بگرد کعبهٔ خاک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو آن نوری که منشوری بعالم</p></div>
<div class="m2"><p>عیان عین منصوری بعالم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو نوری لیکن در ظلمت فتادی</p></div>
<div class="m2"><p>ولی در عین آن قربت فتادی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تونور مخزن اسرار جانی</p></div>
<div class="m2"><p>که اینجا رهنمای لامکانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو نوری این زمان در عین مشکات</p></div>
<div class="m2"><p>ز مصباحت نموداری تو ذرّات</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حقیقت لامکان گلشن تو داری</p></div>
<div class="m2"><p>که جسم و جان و عقل غمگساری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سفر کردی ز دریا سوی عُنصُر</p></div>
<div class="m2"><p>سفر ناکرده قطره کی شود دُر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سفر کردی بمنزل در رسیدی</p></div>
<div class="m2"><p>حقیقت روی جان اینجا بدیدی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سفر کردی ز دریا در صدف باز</p></div>
<div class="m2"><p>شدی جوهر کنون از عزّت و ناز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سفر کردی تو در انجام این تن</p></div>
<div class="m2"><p>بتوست این جملهٔ آفاق روشن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سفر کردی ز کل فارغ شدی تو</p></div>
<div class="m2"><p>در اینجا در صدف بالغ شدی تو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو ای جوهر چو از دریا برآئی</p></div>
<div class="m2"><p>ز زیر طشت زرّین بر سر آئی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>توئی جوهر که قدر خویش دانی</p></div>
<div class="m2"><p>نباید کاین چنین اینجا بمانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو در قعری کجا باشد بهایت</p></div>
<div class="m2"><p>بهای تست جان بی حدّ و غایت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>توئی آن جوهر هر دو جهان هم</p></div>
<div class="m2"><p>که بخشیدی تو این معنی دمادم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کنون درهرچه هستی روی بنمای</p></div>
<div class="m2"><p>یکی شو بی عدد هر سوی بنمای</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>که جوهر روشنیّ او یکی است</p></div>
<div class="m2"><p>نمود گردش او بیشکی است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>الا ای جوهر بالا گزیده</p></div>
<div class="m2"><p>ولیکن در گمانی نارسیده</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>الا ای جوهر بحر معانی</p></div>
<div class="m2"><p>کنون اندر صدف بیشک نهانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>توئی دریا ولی جوهر نمودی</p></div>
<div class="m2"><p>که دایم در صدف گوهر نبودی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سفر کردی و دیدی روی دلدار</p></div>
<div class="m2"><p>حجاب این صدف از پیش بردار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>صدف بشکن که اندر نور قدسی</p></div>
<div class="m2"><p>جدا مانی ز حیوان نقش سُدسی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>برافکن شش جهاتت از صدف باز</p></div>
<div class="m2"><p>که با نورت بود پر دُر صدف باز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زند عکس و تو مه را باز بینی</p></div>
<div class="m2"><p>همت انجام و هم آغاز بینی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تو نور قدس داری در درونت</p></div>
<div class="m2"><p>یکی نور درون و هم برونت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تو نور قدس داری در نمودار</p></div>
<div class="m2"><p>عیان روح داری جسم بردار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تو نور قدسی افتادی در اینجا</p></div>
<div class="m2"><p>شعاعت در گرفته عین دریا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تونور قدسی و در این صفاتی</p></div>
<div class="m2"><p>حقیقت ترجمان عین ذاتی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تو نور قدسی و دیدی تو خود را</p></div>
<div class="m2"><p>عیان دریاب خود عین اَحَد را</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خطابم باتو و با هیچکس نیست</p></div>
<div class="m2"><p>که جز تو هیچکس فریادرس نیست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>الا ای نور قدسی روی بنمای</p></div>
<div class="m2"><p>ز زنگ آینه دل پاک بزدای</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زمین و اسمان از پیش بردار</p></div>
<div class="m2"><p>نمود جسم و جان از خویش بردار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در این آیینهٔ دل کن نظر باز</p></div>
<div class="m2"><p>حجاب صورت و معنی برانداز</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز اشترنامهٔ سرکاردیدی</p></div>
<div class="m2"><p>حقیقت دیدهٔ دیدار دیدی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>برافکن چار طبع و شش جهت تو</p></div>
<div class="m2"><p>که تا ز اعداد گردی یک صفت تو</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>صفات وذات خود هر دو یکی بین</p></div>
<div class="m2"><p>درون را با برون حق بیشکی بین</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>توئی ذات و صفات و فعل در حق</p></div>
<div class="m2"><p>جهانِ جان توئی ای یار مطلق</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>جواهر ذات بر گو آشکاره</p></div>
<div class="m2"><p>چو خواهد کرد یارت پاره پاره</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو چیزی دیگرت اینجا نماندست</p></div>
<div class="m2"><p>بجز نامیت اینجا که نشاندست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بگو اسرار فاش و فاش گردان</p></div>
<div class="m2"><p>برافکن نقش خود نقّاش گردان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>توئی عین العیان جوهر ذات</p></div>
<div class="m2"><p>نمودِ تست بیشک جمله ذرّات</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو خواهد کُشت محبوبت بزاری</p></div>
<div class="m2"><p>برافکن جوهر و کن پایداری</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو عیسی زنده میر از جوهر پاک</p></div>
<div class="m2"><p>که تا چون خر نمانی در گوِ خاک</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو زان کانی که جانهاگوهر اوست</p></div>
<div class="m2"><p>فلک از دیرگه خاک در اوست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تو داری ملکت معنی سراسر</p></div>
<div class="m2"><p>دمی تو از نمود دوست مگذر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>درونِ کعبهٔ دل باز دیدی</p></div>
<div class="m2"><p>دمی در صحبت جان آرمیدی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>شترها را رها کن در چراگاه</p></div>
<div class="m2"><p>درونِ کعبه می زن صنعةاللّه</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>درون کعبهٔ جانان تو داری</p></div>
<div class="m2"><p>سزد گر اشتران اینجاگذاری</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>درون کعبه خلوتگاه جانست</p></div>
<div class="m2"><p>که مرد دیدار جانان کل عیانست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>درونِ کعبهٔ و راز داری</p></div>
<div class="m2"><p>سزد گردل ز شهوت باز داری</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>درون کعبه این سرّ درنگنجد</p></div>
<div class="m2"><p>فلک اینجا به یک کنجد نسنجد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>درون کعبه زیبا باید و پاک</p></div>
<div class="m2"><p>درون کعبه نی گرد است و نی خاک</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>درون کعبه گر یک شب درآئی</p></div>
<div class="m2"><p>به بینی جان جانان جانفزائی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>درون کعبه جانان میزند سیر</p></div>
<div class="m2"><p>اگرچه مینگنجد بت در این دیر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>درون کعبه غیری درنگنجد</p></div>
<div class="m2"><p>بجز یک دید سیری در نگنجد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>حرمگاه دلت را کن نظر زود</p></div>
<div class="m2"><p>که تا بینی درو دیدار معبود</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>حرمگاه دلت جانان مقیم است</p></div>
<div class="m2"><p>ترا هم پرده دار و هم ندیم است</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>حرمگاه دلت چون جانست دریاب</p></div>
<div class="m2"><p>ز پیدایی عجب پنهانست دریاب</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>حرمگاه دلت جانست دردید</p></div>
<div class="m2"><p>ز دید او یکی بین گفت واشنید</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چو در خلوت نشیند یار با یار</p></div>
<div class="m2"><p>اگر موئی بود کی گنجد اغیار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>نگنجد در نمود دیدن دوست</p></div>
<div class="m2"><p>ترا از گوش جان بشنیدن دوست</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چو در خلوت مقیم است او عیانت</p></div>
<div class="m2"><p>زمانی تازه گردان عین جانت</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>توئی در کعبه و بت میپرستی</p></div>
<div class="m2"><p>چرا تو بت بیکباره شکستی</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>توئی در کعبه و بت کرده حاصل</p></div>
<div class="m2"><p>کجا گردی تو اندر عین واصل</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>توئی در کعبه اینجا بت شکن باش</p></div>
<div class="m2"><p>چو حق دیدی بحق بنمای دین فاش</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>توئی در کعبه و پستی بیفکن</p></div>
<div class="m2"><p>بتِ صورت چوابراهیم بشکن</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>توئی در کعبه و خلوت گزیده</p></div>
<div class="m2"><p>نمودی دیده و بُت برگزیده</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>اگر با دیدهٔ با دیده میباش</p></div>
<div class="m2"><p>ز خلقان خویشتن دزدیده میباش</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>اگر با دیدهٔ نادیده مشنو</p></div>
<div class="m2"><p>حقیقت جوی و بر تقلید مگرو</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>اگر بادیدهٔ رازت نهان گوی</p></div>
<div class="m2"><p>وگر گوئی ابا خلق جهان گوی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>اگر با دیدهٔ حاصل چه داری</p></div>
<div class="m2"><p>در این اعیان دلت واصل چه داری</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ندیدی وصل یار ای بی وفا تو</p></div>
<div class="m2"><p>از آن هستی در این راه جفا تو</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>جفاکردی وفا میداری امّید</p></div>
<div class="m2"><p>بسوزد ذرّه اندر عین خورشید</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>جفا کردی وفا هرگز نبینی</p></div>
<div class="m2"><p>بجز وقتی که خود عاجز ببینی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بعجز اقرار ده ای تو ستمکار</p></div>
<div class="m2"><p>که تاعذرت پذیرد روی دلدار</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>ز عجز خویش دایم باش مسکین</p></div>
<div class="m2"><p>که تا چون گل شوی خوشبوی و مشگین</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>ز عجز خویش دایم ربّنا گوی</p></div>
<div class="m2"><p>بروز و شب یقین فاغفرلنا گوی</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>زعجز خویش خود گم نه بهرحال</p></div>
<div class="m2"><p>که تا رسته شوی از قیل وزقال</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>ز عجز خویش کن دائم تو طاعت</p></div>
<div class="m2"><p>که بیرون آید از رنج تو راحت</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>ببینی چون دمی انصاف از خود</p></div>
<div class="m2"><p>ز نور شرع بینی نیک از بد</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>اگر انصاف دادی رستی ازنار</p></div>
<div class="m2"><p>ببخشد مر ترا پس عاقبت یار</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>اگر انصاف دادی پاک باشی</p></div>
<div class="m2"><p>ولی باید که همچون خاک باشی</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>اگر انصاف دادی راست بینی</p></div>
<div class="m2"><p>درون کعبه با جانان نشینی</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>اگر انصاف دادی یار رستی</p></div>
<div class="m2"><p>به کنج عافیت شادان نشستی</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>اگر انصاف دادی گنج یابی</p></div>
<div class="m2"><p>درون جان و دل بی رنج یابی</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>اگر انصاف دادی جانِ جانی</p></div>
<div class="m2"><p>که هستی قاف سیمرغ معانی</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>اگر انصاف دادی نور گردی</p></div>
<div class="m2"><p>درونِ جزو و کل مشهور گردی</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>اگر انصاف دادی در صفائی</p></div>
<div class="m2"><p>نمود عشق کل اندر صف آئی</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>بده انصاف تا این راز یابی</p></div>
<div class="m2"><p>که خود بی شک حق از خود بازیابی</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>چو انصافست اینجا پرده راز</p></div>
<div class="m2"><p>تو نیز انصاف ده پرده برانداز</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>ز طاعت مگذر و عین قناعت</p></div>
<div class="m2"><p>قناعت برتر است از عین طاعت</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>قناعت بهتر است از هر دو عالم</p></div>
<div class="m2"><p>قناعت کرد و توبه یافت آدم</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>قناعت سلطنت دارد بتحقیق</p></div>
<div class="m2"><p>ز هر کس ناید از پندار توفیق</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>قناعت کردهاند اینجای مردان</p></div>
<div class="m2"><p>تو از عین قناعت رخ مگردان</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>قناعت از صفاکردست اینجا</p></div>
<div class="m2"><p>مصفّا شد از آن آمد هویدا</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>قناعت مرد را در حق رساند</p></div>
<div class="m2"><p>کسی کو راز فقر کل بداند</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>قناعت بهتر از هر دو جهانست</p></div>
<div class="m2"><p>بدان این سر که بیشک کار جانست</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>قناعت روی جانان باز دیدست</p></div>
<div class="m2"><p>قناعت زینت و اعزاز دیدست</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>قناعت انبیا کردند پیشه</p></div>
<div class="m2"><p>از آن در وصل بودندی همیشه</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>قناعت اندرون صافی نماید</p></div>
<div class="m2"><p>همه زنگ طبیعت بر زداید</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>قناعت گوهری بس بی بها بین</p></div>
<div class="m2"><p>قناعت جوی پس عین لقا بین</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>قناعت دل کند صافی و روشن</p></div>
<div class="m2"><p>نماید دید گلخن همچو گلشن</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>قناعت کردهاند اینجای پیدا</p></div>
<div class="m2"><p>که تا جان در عیان گردد هویدا</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>قناعت چون کنی اینجا یقینی</p></div>
<div class="m2"><p>رخ معشوق خود اینجا ببینی</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>درونت صاف و پاکی گردد از کل</p></div>
<div class="m2"><p>شوی فارغ نیابی رنج و هم ذل</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>دلت آئینهٔ صافی کند زود</p></div>
<div class="m2"><p>نماید اندر او دیدار معبود</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>در آئینه ببینی هرچه باشد</p></div>
<div class="m2"><p>به جز رخسار جان چیزی نباشد</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>همه جانان بود گر بازدانی</p></div>
<div class="m2"><p>ولی باید که آن هم راز دانی</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>که بی فقرت نباشد این مسلم</p></div>
<div class="m2"><p>ز قعر افتادم این عین دمادم</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>قناعت کردهام اینجا بسی من</p></div>
<div class="m2"><p>یقین دانستهام خود را کسی من</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>ندیدم خویش را در عین صورت</p></div>
<div class="m2"><p>از آن ذوقم نمود آن بی کدورت</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>قناعت کردم و دیدار دیدم</p></div>
<div class="m2"><p>نمود جان ودل را یار دیدم</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>قناعت جوهریست ازعالم عشق</p></div>
<div class="m2"><p>که میخوانند او را آدم عشق</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>قناعت لامکان دارد ز اللّه</p></div>
<div class="m2"><p>عیان دارد نمود قل هواللّه</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>قناعت جز یکی هرگز ندیداست</p></div>
<div class="m2"><p>اگرچه زو بسی گفت و شنیداست</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>ز فقر است ای برادر این قناعت</p></div>
<div class="m2"><p>قناعت کن تو تا بینی سعادت</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>قناعت کرد اینجا عنکبوتی</p></div>
<div class="m2"><p>درون خلوتی اندر بیوتی</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>حقیقت کرده اینجا پردهٔ باز</p></div>
<div class="m2"><p>درونش آنِ تست ای محرم راز</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>در اینجا او قناعت میگذارد</p></div>
<div class="m2"><p>وطن پیوسته اندر پرده دارد</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>تو تا چون عنکبوت اینجا نباشی</p></div>
<div class="m2"><p>چو او لاغر صفت اعضا نباشی</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>درون پرده کی بینی تو اسرار</p></div>
<div class="m2"><p>که میگویم ترا اینجا به تکرار</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>تو این صورت در اینجا پرده بستی</p></div>
<div class="m2"><p>درون پرده بس فارغ نشستی</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>بیکباره چنین میبایدت راست</p></div>
<div class="m2"><p>که این پرده به پیوسته که آراست</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>چو خواهد گشت پرده پاره پاره</p></div>
<div class="m2"><p>قناعت کن تو و کم کن نظاره</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>بهر چیزی تو بنگر تا توانی</p></div>
<div class="m2"><p>خدا را بین تو از روی معانی</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>چو جز حق نیست چیزی دیگر ای دوست</p></div>
<div class="m2"><p>اگر جز حق دگر بینی نه نیکو است</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>ریاضت اختیار کاملان است</p></div>
<div class="m2"><p>کسی را کاندر این راه او نشان است</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>ریاضت مرد را واصل کند زود</p></div>
<div class="m2"><p>عیان دیده را حاصل کند زود</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>ریاضت واصلان دیدند اینجا</p></div>
<div class="m2"><p>از آن در قرب حق گشتند یکتا</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>ریاضت کش که جانا رخ نماید</p></div>
<div class="m2"><p>درون چشمهٔ کل بحر زاید</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>ریاضت میکشد اینجای ذرات</p></div>
<div class="m2"><p>بخوان ازجاهدوادر عین آیات</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>ریاضت مصطفی اینجا کشیدست</p></div>
<div class="m2"><p>از آن جانان درون خود بدیدست</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>ریاضت او کشید و گشت سرور</p></div>
<div class="m2"><p>ز جمله انبیا او گشت برتر</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>ریاضت او کشید از دیدن شاه</p></div>
<div class="m2"><p>چو بیخود شد بگفت اولی مع اللّه</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>ریاضت او کشید و جان جان شد</p></div>
<div class="m2"><p>درون جزو و کلّ کلّی نهان شد</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>ریاضت او کشید و ذات آمد</p></div>
<div class="m2"><p>ز عین ذات در آیات آمد</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>بگفت اسرار فاش اینجا به حیدر</p></div>
<div class="m2"><p>که بر شهر علومش بود او در</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>بدو اسرار گفت اندر قناعت</p></div>
<div class="m2"><p>محمد صاحب حوض و شفاعت</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>بدو اسرار گفت و راز بنمود</p></div>
<div class="m2"><p>حقیقت مرتضی نفس نبی بود</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>محمّد با علی هر دو یکیاند</p></div>
<div class="m2"><p>ز نورحق حقیقت بیشکیاند</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>محمّد با علی هر دو دو رازند</p></div>
<div class="m2"><p>که بهر آفرینش کار سازند</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>محمّد با علی هر دو همامند</p></div>
<div class="m2"><p>که ایشان در میان کل تمامند</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>محمّد با علی از نور ذاتند</p></div>
<div class="m2"><p>که این دم همدم عین صفاتند</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>محمّد با علی هر دو جهانند</p></div>
<div class="m2"><p>که ایشان برتر از کون و مکانند</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>محمّد با علی دو سرفرازند</p></div>
<div class="m2"><p>که جان مومنان زیشان بنازند</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>محمّد با علی دو شمع دینند</p></div>
<div class="m2"><p>که ایشان رهنمای کفر و دینند</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>محمّد با علی دارند بیشک</p></div>
<div class="m2"><p>وجود لحمک لحمی ابریک</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>یکی باشند ایشان گر بدانی</p></div>
<div class="m2"><p>اگر اسرار ایشان باز دانی</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>یکی باشند ایشان عین اسرار</p></div>
<div class="m2"><p>از ایشان شد حقیقت کل پدیدار</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>یکی باشند ایشان و دو جوهر</p></div>
<div class="m2"><p>اگر تو مؤمنی زیشان تو بگذر</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>از ایشان راه جو تا ره نمایند</p></div>
<div class="m2"><p>که ایشانت در این سر برگشایند</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>از ایشان بازدانی جوهر خویش</p></div>
<div class="m2"><p>نهندت مرهمی اندر دل ریش</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>از ایشان بازدانی هر دوعالم</p></div>
<div class="m2"><p>که ایشانند نفخ جان در این دم</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>از ایشان بازدانی تا چه بودی</p></div>
<div class="m2"><p>که با ایشان تو در گفت و شنودی</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>از ایشان بازدانی سرّ اسرار</p></div>
<div class="m2"><p>کز ایشانت دید تو پدیدار</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>از ایشان جوی اینجا مرهم دل</p></div>
<div class="m2"><p>که ایشانند اینجا محرم دل</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>از ایشان جوی در عین شریعت</p></div>
<div class="m2"><p>که بنمایند رازت از حقیقت</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>از ایشان جوی اینجا نور ایمان</p></div>
<div class="m2"><p>که ایشانند اینجا ذات سبحان</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>از ایشان جوی بیشک نور بینش</p></div>
<div class="m2"><p>که ایشان زندهاند از آفرینش</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>از ایشان جوی عین کل تمامت</p></div>
<div class="m2"><p>که ایشانند شاهان قیامت</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>از ایشان جوی تا بینی عیان یار</p></div>
<div class="m2"><p>وز ایشانت شود اعیان پدیدار</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>از ایشان جوی راه لامکانی</p></div>
<div class="m2"><p>کز ایشان سرّ سبحانی بدانی</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>از ایشان بود بود آمد پدیدار</p></div>
<div class="m2"><p>نداند این سخن جز مرد دیندار</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>که ایشان سالکان و واصلانند</p></div>
<div class="m2"><p>حقیقت بیشکی هر دو جهانند</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>هم ایشان رازدار آفرینند</p></div>
<div class="m2"><p>هم ایشان درگشای آخرینند</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>از ایشانست بود کل در اینجا</p></div>
<div class="m2"><p>که ایشانند پنهانی و پیدا</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>از ایشان جوی اسرار دو عالم</p></div>
<div class="m2"><p>که ایشانند نور چشم آدم</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>میان دیدهها بینا نمایند</p></div>
<div class="m2"><p>درون جسم و جان یکتا نمایند</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>درون دل نظر کن روی ایشان</p></div>
<div class="m2"><p>که تو بنشستهٔ در کوی ایشان</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>درون دل نظر کن راز تحقیق</p></div>
<div class="m2"><p>که ایشانند بود تو ز توفیق</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>حقیقت سرّ ایشان گر بدانی</p></div>
<div class="m2"><p>از ایشان واصل هر دو جهانی</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>چو زیشان یک نفس خارج نباشی</p></div>
<div class="m2"><p>که جانند و در او جمله تو باشی</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>چو ایشانند و تو هستی از ایشان</p></div>
<div class="m2"><p>برادر خواندت هستی چو خویشان</p></div></div>
<div class="b" id="bn180"><div class="m1"><p>از ایشان مگذر و زیشان همی گوی</p></div>
<div class="m2"><p>درون دل تو ایشان را همی جوی</p></div></div>
<div class="b" id="bn181"><div class="m1"><p>از ایشان مگذر و ایشان همی بین</p></div>
<div class="m2"><p>درون جان و دل ای مرد با دین</p></div></div>
<div class="b" id="bn182"><div class="m1"><p>از ایشان واصلی آید ترا هم</p></div>
<div class="m2"><p>اگر داری قدم در کارمحکم</p></div></div>
<div class="b" id="bn183"><div class="m1"><p>درون دل تراگشتند پیدا</p></div>
<div class="m2"><p>نمیبینی تو ایشان را هویدا</p></div></div>
<div class="b" id="bn184"><div class="m1"><p>درون دل ترا بنموده اسرار</p></div>
<div class="m2"><p>کنون بشنو تو این سرّ و نگهدار</p></div></div>
<div class="b" id="bn185"><div class="m1"><p>درون جان تو ایشان بدیدند</p></div>
<div class="m2"><p>ولی از چشم صورت ناپدیدند</p></div></div>
<div class="b" id="bn186"><div class="m1"><p>درون جان تو رویت نمودند</p></div>
<div class="m2"><p>به نیکی هر دو در گفت و شنودند</p></div></div>
<div class="b" id="bn187"><div class="m1"><p>درون جان ودل اسرار گفتند</p></div>
<div class="m2"><p>ابا تو جمله از دادار گفتند</p></div></div>
<div class="b" id="bn188"><div class="m1"><p>درون جان تو عین عیانند</p></div>
<div class="m2"><p>که ایشان در تو چون جان جهانند</p></div></div>
<div class="b" id="bn189"><div class="m1"><p>ترا گفتند اسرار دمادم</p></div>
<div class="m2"><p>چگویم خفتهٔ اینجا تو بی غم</p></div></div>
<div class="b" id="bn190"><div class="m1"><p>کجا دانی تو مر اسرار ایشان</p></div>
<div class="m2"><p>که این دم خفتهٔ بیشک پریشان</p></div></div>
<div class="b" id="bn191"><div class="m1"><p>کجا هرگز بیابد خفته این راز</p></div>
<div class="m2"><p>مگر وقتی که با خویش آید او باز</p></div></div>
<div class="b" id="bn192"><div class="m1"><p>کجاهرگز بداند خفته اسرار</p></div>
<div class="m2"><p>مگر آنگه که گردد زود بیدار</p></div></div>
<div class="b" id="bn193"><div class="m1"><p>مخفت ای دوست یارت در درونست</p></div>
<div class="m2"><p>ولی بیچاره خفته در برونست</p></div></div>
<div class="b" id="bn194"><div class="m1"><p>مخفت ای دوست تا بیدار کردی</p></div>
<div class="m2"><p>مگر شایستهٔ اسرار کردی</p></div></div>
<div class="b" id="bn195"><div class="m1"><p>مخفت ای جان سخن بپذیر آخر</p></div>
<div class="m2"><p>که میگویم ترا اسرار ظاهر</p></div></div>
<div class="b" id="bn196"><div class="m1"><p>چرا خفتی که یارت هست بیدار</p></div>
<div class="m2"><p>ز مستی با خود آی و باش هشیار</p></div></div>
<div class="b" id="bn197"><div class="m1"><p>محمّد(ص) با علی درخود نظر کن</p></div>
<div class="m2"><p>برِ ایشان تو آهنگ اَدَب کن</p></div></div>
<div class="b" id="bn198"><div class="m1"><p>اگر ایشان در این معنی ببینی</p></div>
<div class="m2"><p>گمان بردار هان صاحب یقینی</p></div></div>
<div class="b" id="bn199"><div class="m1"><p>دل و جان کن نثار روی ایشان</p></div>
<div class="m2"><p>چوخاکی باش اندر کوی ایشان</p></div></div>