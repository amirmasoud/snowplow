---
title: >-
    بخش ۳۱ - پسر در اعیان حقیقت کل گوید
---
# بخش ۳۱ - پسر در اعیان حقیقت کل گوید

<div class="b" id="bn1"><div class="m1"><p>منم دریای لاهوتی اسرّار</p></div>
<div class="m2"><p>که در دریا شوم من ناپدیدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منم دریای علم وحکمت حق</p></div>
<div class="m2"><p>که خواهم گفت اینجا راز مطلق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منم دریای دید جمله مردان</p></div>
<div class="m2"><p>که از بهر من است این چرخ گردان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منم دریای بیچون و چگونه</p></div>
<div class="m2"><p>که کردم جمله کشتی باژگونه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منم دریای علم و بحر تنزیل</p></div>
<div class="m2"><p>که صورت را کنم اینجای تبدیل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در این دریا منم بابا الهی</p></div>
<div class="m2"><p>گواهی میدهندم مرغ و ماهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در ین دریا منم اللّه بنگر</p></div>
<div class="m2"><p>نمود دید الا اللّه بنگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منم بابا نمود دید اللّه</p></div>
<div class="m2"><p>در این دریا منم عین هواللّه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منم منصور وبنمایم ترا دید</p></div>
<div class="m2"><p>که میگوئی ابا من عین تقلید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پدر در بحر افکندیم خود را</p></div>
<div class="m2"><p>کنون بنگر مگو تو نیک و بد را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>منم اینجا خدای هر دو عالم</p></div>
<div class="m2"><p>درون بحر من سرّ دمادم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نمایم ای پدر در عین هستی</p></div>
<div class="m2"><p>نخواهم همچو من در بت پرستی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>منم بابا در این بحر هدایت</p></div>
<div class="m2"><p>ولیکن این زمان عین عنایت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>منم این دم ز وصل خود عنایت</p></div>
<div class="m2"><p>کنم تحقیق بابا در پناهت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همه در من، من اندر جملگی گم</p></div>
<div class="m2"><p>شدستم همچو قطره بحر قلزم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>منم بابا در اینجا عین توحید</p></div>
<div class="m2"><p>مگو با من دگر از راه تقلید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همه خلقان کشتی مانده در وی</p></div>
<div class="m2"><p>در آن دریا و آن مستی و آن می</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که بُودِ او از آن کشتی برون بود</p></div>
<div class="m2"><p>حقیقت آفرینش رهنمون بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همه دریا شده مستغرق او</p></div>
<div class="m2"><p>اگر تو واقفی این راز برگو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مدان این راحکایت جز معانی</p></div>
<div class="m2"><p>سزد گر این حکایت خود بخوانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بپا برخواست آن قطب سرافراز</p></div>
<div class="m2"><p>که او را بود کلّی عزّت و ناز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نمود واصلان بودست منصور</p></div>
<div class="m2"><p>کجا همچون که او باشی تو مشهور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چنین گفت ای پدر اکنون وداعست</p></div>
<div class="m2"><p>مرا زین دیدن دریا صداعست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وداعت کردم و خواهم شدن زود</p></div>
<div class="m2"><p>ز بهر شرع از من باش خشنود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که ما را سرّ اسرارست اینجا</p></div>
<div class="m2"><p>نمود من بسی کارست اینجا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اگرچه در کتابم می تو کردی</p></div>
<div class="m2"><p>ز حد شرع بر من سعی بردی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شدم من حافظ قرآن و اسرار</p></div>
<div class="m2"><p>نمود خود از آن دیدم سزاوار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بسی اسرار دانستم پدر من</p></div>
<div class="m2"><p>کجا یابم ز ذات خود خبر من</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دهم با تو نشانی این زمان شاد</p></div>
<div class="m2"><p>که اسرار من اندر ملک بغداد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شود پیدا زبعد شصت و یکسال</p></div>
<div class="m2"><p>مرا اینست اینجاگاه احوال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کنون بر قدر سرّی میگشایم</p></div>
<div class="m2"><p>ولی دیدار با تو مینمایم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خدا بخشیده ما را هر دو عالم</p></div>
<div class="m2"><p>که من دارم عیان عین آدم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خدا بخشید وهم از حق شود راست</p></div>
<div class="m2"><p>بحکمت باز دید من بیاراست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ولیکن جرعهٔ خوردیم اینجا</p></div>
<div class="m2"><p>که از مستی من حیرانست دریا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همه هستی دریاهای عالم</p></div>
<div class="m2"><p>کجا گنجد که پیش ماست شبنم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مرا زان خمّ می جامی بدادند</p></div>
<div class="m2"><p>مرا از کان کُل کامی بدادند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز جام عشق دل رفت وشدم جان</p></div>
<div class="m2"><p>ز پیدائی خود هستیم پنهان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خدا را عین جزو و کل بدیدم</p></div>
<div class="m2"><p>در این دریا بدید حق رسیدم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در این دریا ببردم عین تحقیق</p></div>
<div class="m2"><p>که جوهر یافتم از عین توفیق</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>پدر دریای وحدت جز یکی نیست</p></div>
<div class="m2"><p>محقق را در این معنی شکی نیست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نخواندی سورة طلاه سراسر</p></div>
<div class="m2"><p>ز موسی دار این معنی تو باور</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>درختی دید آن شب موسی از دور</p></div>
<div class="m2"><p>ز صد ساله ره آنجا کُه پُر از نور</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بیک جذبه بشد آن نیکبخت او</p></div>
<div class="m2"><p>ز قربت تا سوی نور درخت او</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>همی زد آن درخت انّی انااللّه</p></div>
<div class="m2"><p>که واصل بود ای بابا در این راه</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>از آنی در انااللّه بود پر نور</p></div>
<div class="m2"><p>که حق کردست این آیات مشهور</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>درختی این چنین گوید انااللّه</p></div>
<div class="m2"><p>که گردد از نمود شاه آگاه</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>درختی این چنین واصل ببودست</p></div>
<div class="m2"><p>که او را این شرف حاصل ببودست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>درختی این چنین قربت بیابد</p></div>
<div class="m2"><p>که در دیدار این وحدت بیابد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>درختی این چنین گفتست این راز</p></div>
<div class="m2"><p>بکرده پرده از اسرار کل باز</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>درختی این چنین مشهور بنمود</p></div>
<div class="m2"><p>که موسی را عیان نور بنمود</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>درختی این چنین در منزلاتست</p></div>
<div class="m2"><p>که گفتارش گشود مشکلاتست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>درختی این چنین اسرار گفتست</p></div>
<div class="m2"><p>روا باشد اگرچه در نهفتست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>درختی یافتست این قربت دوست</p></div>
<div class="m2"><p>که میداند که بود بودش از اوست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>درختی یافتست اینجانمودار</p></div>
<div class="m2"><p>که میگوید نمود سرّ اسرار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>رواست انّی انااللّه گفتن او</p></div>
<div class="m2"><p>که پنهان نیست گوهر سفتن او</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>رواست انّی انااللّه از درختی</p></div>
<div class="m2"><p>ز وصل اینجا نگوید نیکبختی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>رواست انّی انااللّه گر بگوئی</p></div>
<div class="m2"><p>بوقتی کز خودی خود نکوئی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو حق دیدم پدر در عین تحقیق</p></div>
<div class="m2"><p>حقیقت حق شدم از سر توفیق</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چو حق بودم من و واصل ببودم</p></div>
<div class="m2"><p>نمود ذات او حاصل نمودم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چو حق دیدم فنای خود گزیدم</p></div>
<div class="m2"><p>که در عین بقای کل رسیدم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چو حق دیدم شدم با حق در آنجاست</p></div>
<div class="m2"><p>گواه مننمود حق ز دریاست</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>منم حق ای پدر بنموده رویم</p></div>
<div class="m2"><p>ز شوق خویشتن در گفتگویم</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>منم حق هیچ باطل نیست ذاتم</p></div>
<div class="m2"><p>ببین اکنون تو اعیان صفاتم</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>منم حق لیک تا وقتم درآید</p></div>
<div class="m2"><p>نمودم سوی وصل کل درآید</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ز حق در حق حقیقت من بگویم</p></div>
<div class="m2"><p>اناالحق در میان مطلق بگویم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>اناالحق گویم اندر ملک بغداد</p></div>
<div class="m2"><p>ز عین عالم و معنی وهم داد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>اناالحق گویم اینجا نیز من هم</p></div>
<div class="m2"><p>نهم بر ریش و بر درد تو مرهم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>اناالحق گویم و در حق شوم گم</p></div>
<div class="m2"><p>مثال قطرهٔ در عین قلزم</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>اناالحق گویم و خواهم شدن من</p></div>
<div class="m2"><p>حقیقت کل خدا خواهم بُدن من</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>اناالحق گویم و در حق نمودم</p></div>
<div class="m2"><p>تو حق بین ای پدر گفت و شنودم</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>اناالحق گویم از دریای وحدت</p></div>
<div class="m2"><p>فرو نوشم کنون در عین قربت</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>خدا با ماست با ما هیچکس نیست</p></div>
<div class="m2"><p>نمود عشق جز اللّه بس نیست</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>خدا با ماست کن در ما نظر باز</p></div>
<div class="m2"><p>حجاب از پیش خود بابا برانداز</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>خدا با ماست بابا این زمان بین</p></div>
<div class="m2"><p>مرا در عین حق در آسمان بین</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>خدا با ماست جز من کس نبیند</p></div>
<div class="m2"><p>کسی باید که همچون من ببیند</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>خدا با ماست در دریا و کشتی</p></div>
<div class="m2"><p>پدر اکنون نظر کن تا چه کشتی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>خدا با ماست و اندر گفتگویست</p></div>
<div class="m2"><p>هزاران سر در این دریا چو گویست</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>در این دریا منم اللّه مطلق</p></div>
<div class="m2"><p>زده دم همچو مردان از اناالحق</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>اناالحق میزنم بابا و گفتم</p></div>
<div class="m2"><p>جواب خود زحق کلّی شنفتم</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>اناالحق میزنم در عین دریا</p></div>
<div class="m2"><p>نخواهیدم دگر دیدن در اینجا</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>اناالحق میزنم بر جوهر بحر</p></div>
<div class="m2"><p>که کردستم حقیقت لطف را قهر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>اناالحق میزنم و اندر بیانم</p></div>
<div class="m2"><p>چو دریا من ابا نام و نشانم</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>اناالحق میزنم چون جمله دیدم</p></div>
<div class="m2"><p>اگر بینی مرا هم ناپدیدم</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>اناالحق حق ز دست ای باب دریاب</p></div>
<div class="m2"><p>در این دریا چو کشتی عین غرقاب</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>در این کشتیِ تن دریا نظر کن</p></div>
<div class="m2"><p>پس آنگه این تن شیدا نظر کن</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>در این کشتی تویی جان و دل من</p></div>
<div class="m2"><p>که بنمودستی این آب و گل من</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>در این کشتی بماندی و بمانی</p></div>
<div class="m2"><p>که از رمزم پدر موئی ندانی</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>حقیقت گر تو خواهی آمدن بین</p></div>
<div class="m2"><p>مرا بنگر کنون و کل مرا بین</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>بیا تا همسفر باشیم با هم</p></div>
<div class="m2"><p>چو من شو تا شوی در عشق محرم</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>بیا تا بگذریم از عین دریا</p></div>
<div class="m2"><p>ز دریای دگر گردیم یکتا</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>در آن دریا که این دریا از آنست</p></div>
<div class="m2"><p>که این یک قطره زان عین العیانست</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>در آن دریا قدم زن با من ای باب</p></div>
<div class="m2"><p>نمود عشق من اینجا تو دریاب</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>در آن دریا قدم زن تا شوی گل</p></div>
<div class="m2"><p>رهی یکبارگی از رنج وز ذل</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>در آن دریا قدم زن تا الهی</p></div>
<div class="m2"><p>شوی بیشک یکی در ماه و ماهی</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>در آن دریا قدم زن در قدم تو</p></div>
<div class="m2"><p>اگر داری نمود دم بدم تو</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>در آن دریا قدم زن تا شوی یار</p></div>
<div class="m2"><p>پدر یکی شهر اینجای بسیار</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>در آن دریا ترا یکی نمایند</p></div>
<div class="m2"><p>ترا عین نمود کل فزایند</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>در آن دریا نبینی دید کشتی</p></div>
<div class="m2"><p>بوقتی کز صور اندر گذشتی</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>در آن دریا منم حق الیقینم</p></div>
<div class="m2"><p>نمود اوّلین و آخرینم</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>در آن دریا یکی دیدم سراسر</p></div>
<div class="m2"><p>نهاده جان و دل او را برابر</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>در آن دریا شدم بیخود ابا خود</p></div>
<div class="m2"><p>نمیگنجد در آن دریا چرا بد</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>در آن دریا همی یکسان نمودم</p></div>
<div class="m2"><p>از آن دریا من این برهان نمودم</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>در آن دریا نمودندم همه راز</p></div>
<div class="m2"><p>بدیدم اندر او انجام و آغاز</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>در آن دریا همه جانست و جانان</p></div>
<div class="m2"><p>نمودش عین پیدایست و پنهان</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>در آن دریا حقیقت نور دیدم</p></div>
<div class="m2"><p>نظر کردم به کل معبود دیدم</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>در آن دریا نمیگنجد سر و پای</p></div>
<div class="m2"><p>کجا باشد در آنجا بود دنیای</p></div></div>