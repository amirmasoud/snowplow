---
title: >-
    بخش ۵۶ - در خطاب کردن با دل در اعیان کل و گذرکردن از تقلید فرماید
---
# بخش ۵۶ - در خطاب کردن با دل در اعیان کل و گذرکردن از تقلید فرماید

<div class="b" id="bn1"><div class="m1"><p>دلا بگذر ز خود وندر فنا شو</p></div>
<div class="m2"><p>عیان انبیا و اولیا شو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دیده گوی وز تقلید مگرو</p></div>
<div class="m2"><p>دگر اسرار آدم نیز بشنو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>توئی آدم بحوّا بازماندی</p></div>
<div class="m2"><p>عجب در عزّت و در ناز ماندی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بحوّا گر بمانی باز اینجا</p></div>
<div class="m2"><p>یقین چون او تو جان درباز اینجا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو میدانی که خواهی رفت آخر</p></div>
<div class="m2"><p>دمی مگذر تو از معنی ظاهر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز حق یک دم مشو دور ای دل و جان</p></div>
<div class="m2"><p>وگرنه از بهشتت زود جانان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کند بیرون بیک ره همچو ابلیس</p></div>
<div class="m2"><p>بگو تا چند خواهی کرد تلبیس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو بیرون گرکند اینجا بزاری</p></div>
<div class="m2"><p>سزد گر این زمان شرمی بداری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدان میگویمت تا گوش دل تو</p></div>
<div class="m2"><p>گشائی این حجاب آب و گل تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نمودِ آدم و حوّا بخوانی</p></div>
<div class="m2"><p>ز تفسیر عیان اسرار خوانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چوآدم آن چنان صورت عیان دید</p></div>
<div class="m2"><p>ز شادی در میان یک دم بنازید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو حوّا دید پیش خود نشسته</p></div>
<div class="m2"><p>در غمها بروی او ببسته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که پیش و پس همه خیل فرشته</p></div>
<div class="m2"><p>همه از فیض ربّانی سرشته</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ستاده جبرئیل و جمله حوران</p></div>
<div class="m2"><p>همه در پیش آدم با قصوران</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خطابی کرد حق آنگه ابا او</p></div>
<div class="m2"><p>که چون میبینی آدم گفت نیکو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>توئی دانا و رحمانی چگویم</p></div>
<div class="m2"><p>در این میدان که سرگردان چو گویم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صفات تست اینجا آشکاره</p></div>
<div class="m2"><p>مرا اینجا رسد عین نظاره</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو بینائی و راز جمله دانی</p></div>
<div class="m2"><p>تو پیدا کردهٔ راز نهانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همه مانده عجایب اندر این حال</p></div>
<div class="m2"><p>زبانم گشته اندر صنع تو لال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو آوردی در اینجا سرّ بیچون</p></div>
<div class="m2"><p>نمیدانم که این احوال مر چون</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نه خوابست اینکه میبینم عیانی</p></div>
<div class="m2"><p>و یا پندار این سرّ نهانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>منم اندر بهشت لایزالت</p></div>
<div class="m2"><p>شده اندر تجلّی جمالت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز وصفت واله و شیدا شدستم</p></div>
<div class="m2"><p>ز جام عشق تو حیران و مستم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز صنعت عقل من حیران بماندست</p></div>
<div class="m2"><p>خرد انگشت در دندان بماندست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز صنعت ماندهام در عین خوابی</p></div>
<div class="m2"><p>که در پهلوی من یک آفتابی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نشاندستی کنون این از کجا بود</p></div>
<div class="m2"><p>که ما را اندر این پیدا لقا بود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کجا بد اول و این از که آمد</p></div>
<div class="m2"><p>که عقل و هوش آدم جمله بستد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مرا برگوی تا خود این چه بود است</p></div>
<div class="m2"><p>که صنع تو مرا پیدا نمودست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تعالی اللّه زهی دیدار یکتا</p></div>
<div class="m2"><p>که گردی اندر این جنّت هویدا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تعالی اللّه زهی قدرت نمودی</p></div>
<div class="m2"><p>در این صنعت چنین نمودی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تعالی اللّه زهی انوار بیچون</p></div>
<div class="m2"><p>که پیدا کردهٔ از کاف وز نون</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تعالی اللّه زهی نقاش مطلق</p></div>
<div class="m2"><p>ترا باشد چنین راز اناالحق</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تعالی اللّه که آدم گشت حیران</p></div>
<div class="m2"><p>جلالش را در اینجا وصف نتوان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تعالی اللّه که آدم آفریدی</p></div>
<div class="m2"><p>ورا در عین جنّت آوریدی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نمودی این زمانش جوهر خویش</p></div>
<div class="m2"><p>حجابم بر گرفتی جمله از پیش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>حجابم این زمان برداشتی باز</p></div>
<div class="m2"><p>که دیدم من در این انجام و آغاز</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>حجابم این زمان رفته بیکبار</p></div>
<div class="m2"><p>که آمد راز جانانم پدیدار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>حجابم این زمان شد جملگی دور</p></div>
<div class="m2"><p>که میبینم ورا نور علی نور</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>حجابم دور شد از روی دلدار</p></div>
<div class="m2"><p>چو دیدم گشتهام از جنگ بیزار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>حجابم دور شد تا راز دیدم</p></div>
<div class="m2"><p>نمودش اندر اینجا باز دیدم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>حجابم دور شد تا روی یارم</p></div>
<div class="m2"><p>حقیقت گشت اینجاگه شکارم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>حجابم دور شد میبینمش روی</p></div>
<div class="m2"><p>نشسته این دمم جانان بپهلوی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>حجابم دور شد از عین جنّات</p></div>
<div class="m2"><p>که میبینم کنون مستور ذرّات</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>جمال یار رویاروی دیدم</p></div>
<div class="m2"><p>نمود دوست در پهلوی دیدم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>جمال یار آنگاهی چنانم</p></div>
<div class="m2"><p>ندانم تا که وصفش من چه خوانم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>جمال یار این حوران که باشند</p></div>
<div class="m2"><p>به پیش رویت اینان خود که باشند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>جمال روی ما حور و قصورست</p></div>
<div class="m2"><p>جمال جان آدم پر ز نورست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>جمال یار عین جاودانست</p></div>
<div class="m2"><p>که این از پیش آدم رایگان است</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>جمال یار و دیدار نکوئی</p></div>
<div class="m2"><p>که وصفش مینگنجد از نکوئی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>جمال یار آنگه پرلقایست</p></div>
<div class="m2"><p>که درد جان آدم را دوایست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>جمال یار میبینم کنونم</p></div>
<div class="m2"><p>در این جنّات اندر رهنمونم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>جمال یار میبینم بشادی</p></div>
<div class="m2"><p>مرا این عین جاویدان تو دادی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>جمال یار میبینم عیانی</p></div>
<div class="m2"><p>توآوردی تو کردی و تو دانی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>جمال یار بی برقع پدیدست</p></div>
<div class="m2"><p>ولی اندر لطافت ناپدیدست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>جمال یار روح جان فروزست</p></div>
<div class="m2"><p>که در جنّات جانم رخ نمودست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>جمال یار دیدم رایگانی</p></div>
<div class="m2"><p>تو گویائی در این شرح و معانی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>جمال یار بس زیبا و خوبست</p></div>
<div class="m2"><p>ولی در ذات ستّار العیوبست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نمودی این زمان از پردهٔ راز</p></div>
<div class="m2"><p>مرا پیدا شده انجام و آغاز</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نمودی این زمان دیدار خویشت</p></div>
<div class="m2"><p>عجب برداشتی ای جان زپیشت</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چو برقع برگرفتی ای دل و جان</p></div>
<div class="m2"><p>ندارم طاقت خورشید رخشان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ندارم طاقت خورشید رویت</p></div>
<div class="m2"><p>از آن چون ذرّهام حیران ببویت</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ندارم طاقت عکس جمالت</p></div>
<div class="m2"><p>اگرچه دیدهام عین وصالت</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>وصالت رخ نمود اینجامرا بین</p></div>
<div class="m2"><p>دل شیدای ما از جان ما بین</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>وصالت میرباید جان آدم</p></div>
<div class="m2"><p>که بنمودی چنین اعیان آدم</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>وصالت میرباید جوهر جان</p></div>
<div class="m2"><p>نمییارم که بینم رویت ای جان</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>شدستآدم در این جنّات بیهوش</p></div>
<div class="m2"><p>زبان اینجا نیارم کرد خاموش</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>دل و جان واله و حیران چه گویم</p></div>
<div class="m2"><p>در اینجا گاه ای جانان چه گویم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>توئی صانع درون جان آدم</p></div>
<div class="m2"><p>تو هم حوّائی و جانان آدم</p></div></div>