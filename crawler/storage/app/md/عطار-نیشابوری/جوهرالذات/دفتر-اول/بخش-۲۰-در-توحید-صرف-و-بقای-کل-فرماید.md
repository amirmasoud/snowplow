---
title: >-
    بخش ۲۰ - در توحید صرف و بقای کل فرماید
---
# بخش ۲۰ - در توحید صرف و بقای کل فرماید

<div class="b" id="bn1"><div class="m1"><p>خدا شد بیجهت درحق مبرّا</p></div>
<div class="m2"><p>دم اللّه زد وز دید یکتا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خدا شد بود او نابود آمد</p></div>
<div class="m2"><p>ز دید دید حق معبود آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خدا شد در خدا دانی یقین دید</p></div>
<div class="m2"><p>در اینجا اوّلین و آخرین دید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خدا شد در خدا ز اللّه دم زد</p></div>
<div class="m2"><p>در اعیان خدائی او قدم زد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خدا شد بود خود در بود حق باخت</p></div>
<div class="m2"><p>عیان شد عشق و صورت را برانداخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خدا شد تا خدا در جان نباشد</p></div>
<div class="m2"><p>نمود بود او اعیان نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خدا شد از خدا گفت آشکاره</p></div>
<div class="m2"><p>بکردندش در اینجا پاره پاره</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر آن کو راز بین باشد در این کار</p></div>
<div class="m2"><p>شود در عاقبت اندر سرِ دار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر آن کو سِرّ بداند سرفشاند</p></div>
<div class="m2"><p>ولی در عاقبت حیران نماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر آن کو سر بداند جان جانست</p></div>
<div class="m2"><p>ولیکن این سخن از من نهانست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو منصور از مرید دوست خود دید</p></div>
<div class="m2"><p>یقین در کشتن خود او سبق دید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که میبینم که چون منصور عطّار</p></div>
<div class="m2"><p>بخواهد سر بریدن زود ناچار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جدا خواهد شدن از بود خود زود</p></div>
<div class="m2"><p>شود در عاقبت دیدارمعبود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جواهر ذات بعد از این که خواند</p></div>
<div class="m2"><p>ز هر یک چشم جوی خون فشاند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چه دیدست اندر اینجاکشتن خویش</p></div>
<div class="m2"><p>حجاب خویشتن برداشت از پیش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شترنامه عیان یار خود گفت</p></div>
<div class="m2"><p>ز منصور حقیقی راز بشنفت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عیان منصور عطّارست دریاب</p></div>
<div class="m2"><p>کنون از عشق بر دارست دریاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عیان منصور دید و بود منصور</p></div>
<div class="m2"><p>که اشترنامه زو گشتست مشهور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عیان منصور بود و کل لقا دید</p></div>
<div class="m2"><p>چو اندر کشتن خود او بقا دید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عیان منصور بود و راز گفت او</p></div>
<div class="m2"><p>همه راز نهانی باز گفت او</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عیان منصور بود و زد اناالحق</p></div>
<div class="m2"><p>بگفت اندر میانه راز مطلق</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عیان منصور بود و گشت عاشق</p></div>
<div class="m2"><p>فنای خویشتن میدید لایق</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عیان منصور بود و در جلالش</p></div>
<div class="m2"><p>بدید آنجا نمودار کمالش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عیان منصور بود و جان بداد او</p></div>
<div class="m2"><p>ز عین عشق بیشک داد داد او</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عیان منصور بود و کشتن خویش</p></div>
<div class="m2"><p>ز دید حق بدید اینجای از پیش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عیان منصور بود و جوهر ذات</p></div>
<div class="m2"><p>نمود اینجا همه د رعین ذرّات</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عیان منصور بود و بس کتب ساخت</p></div>
<div class="m2"><p>همه در دیدن جانان بپرداخت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>عیان منصور بود و در بقا شد</p></div>
<div class="m2"><p>ز دید حق نهان انبیا شد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>میان جزو و کل بد پیش بین او</p></div>
<div class="m2"><p>که بد در واصلی صاحب یقین او</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز عین وصل حق چون اصل دریافت</p></div>
<div class="m2"><p>طمع از خود برید و وصل دریافت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز عین وصل او در لامکان شد</p></div>
<div class="m2"><p>بر جانان بکلّی او عیان شد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هر آنکو گاهگاهی عشق بشناخت</p></div>
<div class="m2"><p>چو من چون موم درخورشید بگداخت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو خورشید حقیقی دیدهام من</p></div>
<div class="m2"><p>ز جانان راز خود بشنیدهام من</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو ذرّه در فنا گشتم چو خورشید</p></div>
<div class="m2"><p>از آن ماندم من اندر عشق جاوید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>عیان جاودان اینجا بدیدم</p></div>
<div class="m2"><p>ز حق بینی به کام دل رسیدم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز جان بگذشتم و جانان شدم کلّ</p></div>
<div class="m2"><p>ز بود خویشتن پنهان شدم کل</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>حقیقت چیست بیش اندیش بودن</p></div>
<div class="m2"><p>ز خود بگذشتن و با خویش بودن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>حقیقت چیست جانان باز دیدن</p></div>
<div class="m2"><p>نمود خویش عین راز دیدن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>حقیقت چیست محو جاودانی</p></div>
<div class="m2"><p>که گردی از نمود خویش فانی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سر خود دور نه اندر سرِدار</p></div>
<div class="m2"><p>که تا زان سر تو باشی نیز سردار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>سر خود دور نه جان را برافشان</p></div>
<div class="m2"><p>غبار هستی ازدامن بیفشان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سر خود دور نه مانند عطّار</p></div>
<div class="m2"><p>که تا چون او شوی واصل در اسرار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>سر خود دور نه بگذر ز هستی</p></div>
<div class="m2"><p>چو گبران میمکن این بت پرستی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سر خود دور نه تا خود ببینی</p></div>
<div class="m2"><p>حقیقت نیز نیک و بد ببینی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سر خود دور نه تا راز یابی</p></div>
<div class="m2"><p>نمود ذات اعیان بازیابی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سر خود دور نه و رخ مگردان</p></div>
<div class="m2"><p>که یابی بیشکی دیدار جانان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سر خود دور نه مانند منصور</p></div>
<div class="m2"><p>کز او شد عالم تحقیق مشهور</p></div></div>