---
title: >-
    بخش ۶۶ - در پردههای اسرار نی فرماید
---
# بخش ۶۶ - در پردههای اسرار نی فرماید

<div class="b" id="bn1"><div class="m1"><p>چه میگوئی همی گوید که بشتاب</p></div>
<div class="m2"><p>برون از نه فلک اسرار دریاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو من نُه زخم دارم در حقیقت</p></div>
<div class="m2"><p>گذشتستم ز نه پرده حقیقت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ده و دو پرده اینجا مینوازم</p></div>
<div class="m2"><p>دل عشّاق در پرده نوازم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ده و دو پرده دارم در درون من</p></div>
<div class="m2"><p>شدم عشاق کل را رهنمون من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ده و دو پرده دارم بر دریده</p></div>
<div class="m2"><p>عیان اینجا منم خود راز دیده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ده و دو پرده در یک پرده دارم</p></div>
<div class="m2"><p>از آن من پردهها گم کرده دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگاهی کاندر آیم من بآواز</p></div>
<div class="m2"><p>کنم من پردهها اینجایگه باز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو اندر پرده سازم پرده سازی</p></div>
<div class="m2"><p>نمایم در درون پرده رازی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو آیم در خروش اینجا نهانی</p></div>
<div class="m2"><p>کنم من پردهها پاره عیانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل عشّاق از پرده برآرم</p></div>
<div class="m2"><p>درون را با برونش شاد دارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل عشّاق را اندر نوایم</p></div>
<div class="m2"><p>حقیقت سرّ ربانی نمایم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل عشّاق از من ناز بیند</p></div>
<div class="m2"><p>عیانِ رازِ من او باز بیند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دل عشّاق از من یافت اسرار</p></div>
<div class="m2"><p>که میگوئیم اینجا قصّهٔ یار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زبان بیزبانی یافتم من</p></div>
<div class="m2"><p>نشان بی نشان یافتم من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زبانم بیزبان اسرار گوید</p></div>
<div class="m2"><p>همه اینجایگه از یار گوید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کسی گوید که ساز من شناسد</p></div>
<div class="m2"><p>پس آنگه دید را از من شناسد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کسی باید که دریابد در آن دم</p></div>
<div class="m2"><p>که من زاری کنم اینجادمادم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز درد من خبر یابد زمانی</p></div>
<div class="m2"><p>ز من او گوش دارد داستانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز درد خود بداند درد خود او</p></div>
<div class="m2"><p>اگر این سرّ بدانی هست نیکو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز درد من خبر دریاب از جان</p></div>
<div class="m2"><p>که بنمایم ترا اسرار پنهان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز درد من خبر داری در اینجا</p></div>
<div class="m2"><p>که از بهرچه دارم شور و غوغا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دمی ز آندم عیانی یافتم من</p></div>
<div class="m2"><p>وز آندم کُل معانی یافتم من</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دمی ز آندم مرا دردم نمودند</p></div>
<div class="m2"><p>از آندم مرهم دردم نمودند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دمی ز آندم مرا اندر دم آمد</p></div>
<div class="m2"><p>تو گوئی زخم ما را مرهم آمد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دمی دارم از آندم درخروشم</p></div>
<div class="m2"><p>وز آندم اینچنین در عین جوشم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دمی دارم از آندم یافته من</p></div>
<div class="m2"><p>که درد عشقِ آدم یافته من</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دمی دارم از آندم یافته راز</p></div>
<div class="m2"><p>همی نالم که هستم سخت افگار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دمی دارم از آندم در نمودم</p></div>
<div class="m2"><p>از آن زاری در آنجاگه نمودم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دمی دارم من اندر دم شده جان</p></div>
<div class="m2"><p>از آن میگویمت اسرار پنهان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از آن دم یافتم این دمدمه من</p></div>
<div class="m2"><p>کنم اندر دم تو زمزمه من</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو من بگشایم آندم ازدم تو</p></div>
<div class="m2"><p>شوم در جان و در دل همدم تو</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو من بگشایم اندر زار زاری</p></div>
<div class="m2"><p>کنم فریادها در بیقراری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اگر مردی چو من پیوسته می زار</p></div>
<div class="m2"><p>که تو هم زخمها داری ز دلدار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو من گر ناله و فریاد داری</p></div>
<div class="m2"><p>وز آن دم اندر این دم یار داری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو من اینجا بدانی تو دمادم</p></div>
<div class="m2"><p>که مر چون اوفتاد اسرار آدم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در آندم آدم آمد قصّهٔ او</p></div>
<div class="m2"><p>که آمد اندر اینجا غصّه او</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در آندم چون درون جنّت افتاد</p></div>
<div class="m2"><p>ز شیطان ناگهی در محنت افتاد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دریغا این همه اعزاز و رفعت</p></div>
<div class="m2"><p>دریغا آن همه اعیان و قربت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>که از ابلیس دون افتاد بر باد</p></div>
<div class="m2"><p>از آن میآیدم اندر نفس یاد</p></div></div>