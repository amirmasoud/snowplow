---
title: >-
    بخش ۴۸ - در جواب دادن ابلیس در اعیان فرماید
---
# بخش ۴۸ - در جواب دادن ابلیس در اعیان فرماید

<div class="b" id="bn1"><div class="m1"><p>جوابش داد کای پیر گزیده</p></div>
<div class="m2"><p>چرا گوی سخنهای شنیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو میپرسی مرا از عین اسرار</p></div>
<div class="m2"><p>شنیده کم بگو اینجا بگفتار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز دیده گوی تا من دیده گویم</p></div>
<div class="m2"><p>ترا من نکتهٔ بگزیده گویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گزیده گوی چندانی که دانی</p></div>
<div class="m2"><p>گزیده هست اصل زندگانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز اصل ذات من اینجا نپرسی</p></div>
<div class="m2"><p>که مخفی ماند اینجانور قدسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز اصل ذات من گوئی چه دانی</p></div>
<div class="m2"><p>که ازمعنی تو جز نامی ندانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر پُر تو ز عشق کوی گشتی</p></div>
<div class="m2"><p>در این میدان مثال گوی گشتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز زور بازویت اینجا نماندست</p></div>
<div class="m2"><p>فتاده گوی حیران در بماندست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منم آن جوهر ذات عیانی</p></div>
<div class="m2"><p>که دارم طوق لعنت رایگانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>منم آن جوهر اسرار جانان</p></div>
<div class="m2"><p>که فعلم ظاهر است اسرار پنهان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>منم آن جوهر جان داده بر باد</p></div>
<div class="m2"><p>به لعنت کرده اینجا جمله آباد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>منم آن جوهر ذاتی که آیات</p></div>
<div class="m2"><p>خدا از بهر من گفتست در ذات</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بقرآن چندجانانم نموده است</p></div>
<div class="m2"><p>زیانم نزد جانان جمله سودست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زیانم سود باشد از خطابش</p></div>
<div class="m2"><p>نمییارم ز هیبت من جوابش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بدادن زانکه جانان راز گفتست</p></div>
<div class="m2"><p>عیان عشق با من باز گفتست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خطاب دوست اندر اندرونم</p></div>
<div class="m2"><p>که میداند که من در شوق چونم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خطاب دوست ما را در نهادست</p></div>
<div class="m2"><p>نهادم اندر اینجا داد دادست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خطاب دوست کردش نام باشد</p></div>
<div class="m2"><p>همه ننگ جهان در نام باشد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خطاب دوست درجانم رقم زد</p></div>
<div class="m2"><p>نمود من دمادم در عدم زد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بقا بودم شدم نقش فنا من</p></div>
<div class="m2"><p>ولی خواهم شدن عین بقا من</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بقا بودم ولی اندر خطابش</p></div>
<div class="m2"><p>خوشی کردم همی عین عذابش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عذاب اینجا و آنجادر خطابست</p></div>
<div class="m2"><p>بر آن عاشق که مر او را خطابست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو من گرد و نگردد یک دم ازدوست</p></div>
<div class="m2"><p>حقیقت مغز گرداند عیان پوست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو من در عشق کی آید پدیدار</p></div>
<div class="m2"><p>که لعنت راکند رحمت خریدار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو خود من عاشقی اینجا ندیدم</p></div>
<div class="m2"><p>منالم و اصلی بینا ندیدم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به پنهان در میان انبیا من</p></div>
<div class="m2"><p>بسی بشنیدهام اسرارها من</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>میان انبیا من راز گفتم</p></div>
<div class="m2"><p>حقیقت هم بد ایشان باز گفتم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نمود من در اوّل بود بودش</p></div>
<div class="m2"><p>نمود خویش دیدم در نمودش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>حضورم نزد جانان بود دائم</p></div>
<div class="m2"><p>بذات خود بدم پیوسته قائم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>عیان راز دیدم در ملایک</p></div>
<div class="m2"><p>نمود کل شیء نیز هالک</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همه در لوح محفوظم نمودند</p></div>
<div class="m2"><p>نمود عشق در بودم نمودند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همه دیدم بچشم سر نهانی</p></div>
<div class="m2"><p>جمال بار در عین العیانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نمود یار دیدم در همه چیز</p></div>
<div class="m2"><p>نمود جمله بود و زان من نیز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>قلم بدرفته در هرراز او بود</p></div>
<div class="m2"><p>ولیکن راز من عین نکو بود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ندانستم که چون بُد سرّ اسرار</p></div>
<div class="m2"><p>که اعیانم بد اینجا دیدن بار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جمال یار بود آنجا عیانم</p></div>
<div class="m2"><p>نمود عشق در هر دو جهانم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو خواندم راز دیدم آنچه بُد آن</p></div>
<div class="m2"><p>مر آن را چارهٔ ما را نَبُد آن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>قلم چون رفت اندر عین کاغذ</p></div>
<div class="m2"><p>ولی نتوان نوشتن نیک مربد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>قلم چون رفت کاغذ شد نوشته</p></div>
<div class="m2"><p>چوخاکی شد به آبی آن نوشته</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چه سود از رفته دارم آنچه خواند</p></div>
<div class="m2"><p>کند چیزی نیفزاید کآید</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همه رفتست اندر بود او نیز </p></div>
<div class="m2"><p>که او داند یقین راز هر چیز</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو خطی یار بنویسد بخونم</p></div>
<div class="m2"><p>حقیقت حاکمست و من زبونم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>منم مفلس در این دنیا بمانده</p></div>
<div class="m2"><p>نمودم جمله در عقبی بمانده</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>میان هر دو من اینجا اسیرم</p></div>
<div class="m2"><p>همو باشد در اینجا دستگیرم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>من بیچاره چتوانم بکردن</p></div>
<div class="m2"><p>بجز غم اینجاگاه خوردن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ندارم هیچ چیزی خبر نمودش</p></div>
<div class="m2"><p>طلبکارم در اینجا بود بودش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>طلبکارم که تا ذاتش بیابم</p></div>
<div class="m2"><p>نمود عشق جز ذاتش ندانم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز اصل خویش در من غم گرفتار</p></div>
<div class="m2"><p>عیان در نزد شرعم من گرفتار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>عجائب راز دارم در جهان من</p></div>
<div class="m2"><p>که دارم در جهان راز نهان من</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>طلب کردم بسی تا خود بدانم</p></div>
<div class="m2"><p>که چون بد اصلُ فرع داستانم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بجز من هر کسی من چون شناسد</p></div>
<div class="m2"><p>کسی باید که او چون من شناسد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بجز من کس نداند حال من هم</p></div>
<div class="m2"><p>بریش خویشتن بنهاده مرهم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بجز من هیچکس رازم نداند</p></div>
<div class="m2"><p>وگر داند بخود حیران بماند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>منم استاد جمله پیش بینان</p></div>
<div class="m2"><p>منم اینجا نموده عشق جانان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>مرا طوقیست در گردن فتاده</p></div>
<div class="m2"><p>از او در عین ما و من فتاده</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ز طوق لعنتم خود پاک نبود</p></div>
<div class="m2"><p>که اینجا آتش اندر خاک نبود</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ولی چون خاک اصل پاک دارد</p></div>
<div class="m2"><p>نمود زهر من تریاک دارد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>همی با یکدگر پیوند داریم</p></div>
<div class="m2"><p>ز قول و عهد او سر بر نداریم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>هر آن یک چشم باشد کفر و دینم</p></div>
<div class="m2"><p>بجز یکی در این دیده نبینم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بجز یکی نباشد در وصالم</p></div>
<div class="m2"><p>بجز یکی نیاید در خیالم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بجز یکی در اینجا من ندارم</p></div>
<div class="m2"><p>که راز جان جان پنهان ندارم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بگویم راز با تو گر بدانی</p></div>
<div class="m2"><p>که هستی صاحب عشق و معانی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>مرا افتاد کاری تا قیامت</p></div>
<div class="m2"><p>ندارم جز خود اینجا من ندامت</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>مرا افتاد کاری اندر اینجا</p></div>
<div class="m2"><p>نگردانم رخ از دیدار یکتا</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>مرا افتاد اینجاگاه کاری</p></div>
<div class="m2"><p>گرفتست این زمان ذرّه غباری</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ملامت میکشم در عشق دلدار</p></div>
<div class="m2"><p>نیندیشم دمی از لعنت یار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ملامت میکشم در غرق خونم</p></div>
<div class="m2"><p>ز بیهوشی فتاده در جنونم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ملامت میکشم از طوق لعنت</p></div>
<div class="m2"><p>چو جانم هست اینجا عین رحمت</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ملامت میکشم در هر دو عالم</p></div>
<div class="m2"><p>منم در عشق جانان شاد و خرّم</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>زیارم گر جفا آید پدیدار</p></div>
<div class="m2"><p>ولیک از من وفا آید پدیدار</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>زیارم گر جفا دیدم بسی من</p></div>
<div class="m2"><p>همان خواهم که باشم با کسی من</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>که او اینجا کس هر ناکسانست</p></div>
<div class="m2"><p>هر آن کو این بداند خود کسانست</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>اگر ناکس شوی در کوی دلدار</p></div>
<div class="m2"><p>کسانت گر شوندت من خریدار</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>اگر تو ناکسی از ناکسانش</p></div>
<div class="m2"><p>ز من بشنو همه شرح و بیانش</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چو دیدم خود بدیدم نار بودم</p></div>
<div class="m2"><p>ز بود کفر در زنّار بودم</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>همه کفر جهان دارم بیکبار</p></div>
<div class="m2"><p>شدم کافر چینن در روی دلدار</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>اگر درکنه یکدم دم زنی تو</p></div>
<div class="m2"><p>ببام هفتمین خرگه زنی تو</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>اگر در کفر آئی عشق بینی</p></div>
<div class="m2"><p>نمود عشق هم در عشق بینی</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>اگر در کافری بوئی بری تو</p></div>
<div class="m2"><p>ز بود چرخ و انجم بگذری تو</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>اگر در کافری یابی تو دلدار</p></div>
<div class="m2"><p>نمودبت شکن در کفر بسیار</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>شو اندر آخر کارت نظر کن</p></div>
<div class="m2"><p>دلت از کفر روحانی خبر کن</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>اگر کافر شوی باشی مسلمان</p></div>
<div class="m2"><p>ولی گفتن چنین بر جای نتوان</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>اگر از کافری دم میزنی تو</p></div>
<div class="m2"><p>نه چون مردان بمعنی چون زنی تو</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>اگر از کافری خواهی نشانی</p></div>
<div class="m2"><p>ز من بشنو کنون شرح و بیانی</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>من اندر کافری دلدار دیدم</p></div>
<div class="m2"><p>در اینجاگه نمود یار دیدم</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>من اندر کافری زنّار بستم</p></div>
<div class="m2"><p>وز آنجا در نمود یار بستم</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>من اندر عاشقی کافر نبودم</p></div>
<div class="m2"><p>هم اندر کافری صادق ببودم</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>من اندر کافری اسرار دارم</p></div>
<div class="m2"><p>نمود جزو و کل دلدار دارم</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>من اندر کافری بگزیدهام یار</p></div>
<div class="m2"><p>هم اندر کافری هم دیدهام یار</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>نشان عشق دارم من بگردن</p></div>
<div class="m2"><p>چگویم تا چه بتوانم بکردن</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>نشان عشق اینجا برنهادم</p></div>
<div class="m2"><p>که درد عشق باشد در نهادم</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>نشان عشق رویم زرد کردست</p></div>
<div class="m2"><p>نهاد جان و دل پر درد کردست</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>نشان عشق ما را در میان کرد</p></div>
<div class="m2"><p>ولی بودم ابی نام و نشان کرد</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>نشان عشق از من بنگر ای دل</p></div>
<div class="m2"><p>چرا درماندهٔ در آب و در گل</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>نشان عشق من دارم بزاری</p></div>
<div class="m2"><p>که کردستم در اینجا پایداری</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>نشان عشق در جانم نهانست</p></div>
<div class="m2"><p>ولیکن یار اینجا در میان است</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>چو درد دوست دارد جان من هان</p></div>
<div class="m2"><p>کجا باشد مرا هرگز دل و جان</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>چو جانان رخ نمودم رایگانی</p></div>
<div class="m2"><p>من این لعنت گزیدم در نهانی</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>ز جانان چون خطابی هست ما را</p></div>
<div class="m2"><p>دمادم چون جوابی هست ما را</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>خطاب او کجا دارد جوابی</p></div>
<div class="m2"><p>ولی در عشق مسکینی خطابی</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>کنم هر لحظه در عشق تو تکرار</p></div>
<div class="m2"><p>چو او دارم ابا او گویم اسرار</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>که ای جان جهان و جوهر کل</p></div>
<div class="m2"><p>مرا گر راحت آری و اگر ذل</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>منت ذل کل شمارم راحت جان</p></div>
<div class="m2"><p>که دیدم مر ترا مر راحت جان</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>همه جانها بتو قائم بدیدم</p></div>
<div class="m2"><p>همه دلها بتو دائم بدیدم</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>نمود جان و دلها چون تو داری</p></div>
<div class="m2"><p>مرا اندر میان ضایع گذاری</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>مکن ضایع مر او و شاد دل کن</p></div>
<div class="m2"><p>ببخشد یارِ ما ما را بحل کن</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>ببخش اندر میان و دست گیرم</p></div>
<div class="m2"><p>در این لعنت که دارم دستگیرم</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>نمود لعنتم اینجا تو کردی</p></div>
<div class="m2"><p>در اینجاگه مرا رسوا تو کردی</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>منم خوار و توئی غمخوار مانده</p></div>
<div class="m2"><p>میان آفرینش خوار مانده</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>منم رسوا شده در کویت ای جان</p></div>
<div class="m2"><p>نظر بنهاده اندر سویت ای جان</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>بسی در دل جفای تو کشیدم</p></div>
<div class="m2"><p>به امّیدی بکوی تو دویدم</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>بسی دیدم ملامت اندر اینجا</p></div>
<div class="m2"><p>بسی کردم زبودت نیز غوغا</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>من اندر کوی تو غمخوار و مسکین</p></div>
<div class="m2"><p>نموده کافری و رفته از دین</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>منم در راز تو ثابت قدم من</p></div>
<div class="m2"><p>که دیدستم همه راز قدم من</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>مرا شاید که گویم وصفت ای جان</p></div>
<div class="m2"><p>تو لعنت میکنی ما را چه تاوان</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>تو لعنت کردی و رحمت گزیدم</p></div>
<div class="m2"><p>که در رحمت منش لعنت بدیدم</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>مرا لعنت بکن چندانکه خواهی</p></div>
<div class="m2"><p>که بر اجزای این کل پادشاهی</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>مرا لعنت کن اینجاگه دمادم</p></div>
<div class="m2"><p>بهانه می منه اُسجُدلِآدَم</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>مرا لعنت کن و از خود مرانم</p></div>
<div class="m2"><p>که هر چیزی که گوئی من همانم</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>منم ملعون ترا اینجا طلبکار</p></div>
<div class="m2"><p>که دارم از عنایت راز بسیار</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>منم لعنت گزیده چند گویم</p></div>
<div class="m2"><p>که از بهر تو اینجا گفتگویم</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>مرا این لعنت عالم چه باشد</p></div>
<div class="m2"><p>نموده سجدهٔ آدم چه باشد</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>که مارا با تو افتادست کاری</p></div>
<div class="m2"><p>که با ما کردهٔ تو یادگاری</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>مرا شد یادگاری دانم اینجا</p></div>
<div class="m2"><p>که دیدستم عیان عین الیقین را</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>مرا این بس که دارم بودت ای جان</p></div>
<div class="m2"><p>بروز محشرم هم شاد گردان</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>بروز محشرم بخشی بیکبار</p></div>
<div class="m2"><p>ز دوش آنجای برداری مرا بار</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>بروز محشرم تو کل ببخشی</p></div>
<div class="m2"><p>گناه جزوی و کلّی ببخشی</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>کنم پیوسته زاری من بدرگاه</p></div>
<div class="m2"><p>که من دارم نمود قل هو اللّه</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>اگر منسوخ گشتم بر در او</p></div>
<div class="m2"><p>فتادستم بکلّی بر در او</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>چو نسخم کردی اندر این میان کم</p></div>
<div class="m2"><p>بفضل خود ببخشم رایگان هم</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>مرا از رایگان کردی تو پیدا</p></div>
<div class="m2"><p>شدم در کوی تو مسکین و رسوا</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>چو رسوائی ببخشی کم نباشد</p></div>
<div class="m2"><p>ز بحر خود یکی شبنم نباشد</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>مرا جز تو دگر اینجا کجا بود</p></div>
<div class="m2"><p>که بود من ز بودت انتها بود</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>بفضل خود ببخشم در جهانت</p></div>
<div class="m2"><p>که رحمت یافتم هر دو جهانت</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>مرا چیزی که کردی حاکمی تو</p></div>
<div class="m2"><p>خداوندی نمودی عالمی تو</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>همه رحمت ترا و لعنت من</p></div>
<div class="m2"><p>بفضل خویش کن تاریک روشن</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>عیان رحمت تو بیشمارست</p></div>
<div class="m2"><p>مرا امّید در روز شمارست</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>عیان رحمت تو جاودانست</p></div>
<div class="m2"><p>همه امیّدشان تا جاودانست</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>چو تو شاهی، شاهانت گدایند</p></div>
<div class="m2"><p>نموت انبیا و اولیایند</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>چو توشاهی تمامت ملکت تست</p></div>
<div class="m2"><p>همه امّید ما بر رحمت تست</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>چو تو شاهی تمامت بندگانند</p></div>
<div class="m2"><p>نهاده جمله سر بر آستانند</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>تو شاهی و ترا از جان غلامند</p></div>
<div class="m2"><p>اگر اتمام اگر نه ناتمامند</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>تو شاهی و کنی جمله که خواهی</p></div>
<div class="m2"><p>که بر ملک دو عالم پادشاهی</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>تو شاهی و همه در تو اسیرند</p></div>
<div class="m2"><p>نمیری و همه پیش تو میرند</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>تو شاهی و ترا زیبد که مانی</p></div>
<div class="m2"><p>که شاه آشکارا و نهانی</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>همه شاهان بتو باشند زنده</p></div>
<div class="m2"><p>شده ازجان ترا محکوم و بنده</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>تو شاهی و توئی شاه و توئی شاه</p></div>
<div class="m2"><p>توئی سالک توئی اصل و تو آگاه</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>ز کار راز جمله می تو دانی</p></div>
<div class="m2"><p>که بیرون از جهان و هم جهانی</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>ترا در دیدهها بینا بدیدم</p></div>
<div class="m2"><p>ترا در لفظها گویا بدیدم</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>از اوّل تا به آخر راز داری</p></div>
<div class="m2"><p>سزد گر لعنت از من بازداری</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>بمن رحمت کنی یوم القیامت</p></div>
<div class="m2"><p>ببخشی هم گناهم با ندامت</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>هم آمرزی تمامت بیشکی تو</p></div>
<div class="m2"><p>که دیدستم عیانت مر یکی تو</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>چو ذات تو قدیم و لایزالست</p></div>
<div class="m2"><p>زبانم اندر اینجا گنگ و لال است</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>تمامت انبیا و صفت بگفتند</p></div>
<div class="m2"><p>بجز جوهر ز انوارت نسفتند</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>همه حیران تو بهر خطابی</p></div>
<div class="m2"><p>که تو بنمایی ایشان را عتابی</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>ز بهر تو چنین حیران بماندند</p></div>
<div class="m2"><p>حزین و خوار و سرگردان بماندند</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>ز دید سالکان واصلانت</p></div>
<div class="m2"><p>بچشم من زجمله رهروانت</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>ترا دیدم همه تصدیق و رحمت</p></div>
<div class="m2"><p>نمیگنجد در ذات تو لعنت</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>ترا دانم که جانی و دلی تو</p></div>
<div class="m2"><p>گشاده رازهای مشکلی تو</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>حقیقت نیست جز ذاتت در اسرار</p></div>
<div class="m2"><p>چه باشد لعنت اینجا مرد ستّار</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>خداوند نهان و آشکاری</p></div>
<div class="m2"><p>مرا باید ببین در پرده داری</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>چنان در لعنت تو دیدم اینجا</p></div>
<div class="m2"><p>بسی در کوی تو گردیدم اینجا</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>همه در من بُد و من در همه گم</p></div>
<div class="m2"><p>چو دیده قطرهٔ در عین قلزم</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>صفاتت لامکان و من مکانم</p></div>
<div class="m2"><p>که راز تو در آن باشد عیانم</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>صفاتت برتر است از عقل و افعال</p></div>
<div class="m2"><p>کجا گنجد ز علم عالمان قال</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>تمامت وصف گفتندت بهرحال</p></div>
<div class="m2"><p>زبان جمله از حیرت شده لال</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>بجز تو هیچ چیزی درنگنجد</p></div>
<div class="m2"><p>همه پیشم به جو سنگی نسنجد</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>بجز تو من ندیدم هیچ غیری</p></div>
<div class="m2"><p>ز دورت درتو ما را بود سیری</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>یقینت عین بالا بود پیوست</p></div>
<div class="m2"><p>که یدّ صنع حکمت نقش توبست</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>تو بستی نقش آدم در نمودت</p></div>
<div class="m2"><p>شده بیدار اینجا بود بودت</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>نظر کردم صفاتت ذات دیدم</p></div>
<div class="m2"><p>وجود آدم و ذرّات دیدم</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>بهم پیوسته بودی جز و کل تو</p></div>
<div class="m2"><p>که تا محرم شوی زان عز و ذل تو</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>بهم پیوسته شد تا فاش دیدی</p></div>
<div class="m2"><p>حقیقت در عیان نقاش دیدی</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>ز ذاتت در صفات فعل مطلق</p></div>
<div class="m2"><p>نمودی بودی آدم را تو الحق</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>چو من بی من به تو اسرار دیدم</p></div>
<div class="m2"><p>وجود آدم و انوار دیدم</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>همه علم من و حکمت تو بودی</p></div>
<div class="m2"><p>مرا اندر بهانه در ربودی</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>نبد آدم که دیدم ذات پاکت</p></div>
<div class="m2"><p>تجلّی فعل گشته در صفاتت</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>جهان جان جان کل شده باز</p></div>
<div class="m2"><p>بهم پیوسته بُد انجام و آغاز</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>یقین دانستم اسرارت در اینجا</p></div>
<div class="m2"><p>چو دیدم آدم از ذاتت هویدا</p></div></div>
<div class="b" id="bn180"><div class="m1"><p>یقین شد زانکه غیری نیست جز تو</p></div>
<div class="m2"><p>نمود جمله سیری نیست جز تو</p></div></div>
<div class="b" id="bn181"><div class="m1"><p>به علم اندر ملائک گشتم</p></div>
<div class="m2"><p>تمامت نام و ننگم در نوشتم</p></div></div>
<div class="b" id="bn182"><div class="m1"><p>ز معدن روشنم شد در معانی</p></div>
<div class="m2"><p>که پیدا گشته از راز نهانی</p></div></div>
<div class="b" id="bn183"><div class="m1"><p>ندیدم غیر آن میدانم اینجا</p></div>
<div class="m2"><p>که بودتست هم پنهان و پیدا</p></div></div>
<div class="b" id="bn184"><div class="m1"><p>مراگرچه تو فرمودی بسجده</p></div>
<div class="m2"><p>که آدم هست ما را عین زبده</p></div></div>
<div class="b" id="bn185"><div class="m1"><p>بهانه خاک بود و من بدم نار</p></div>
<div class="m2"><p>شدم کافر ببستم عین زنّار</p></div></div>
<div class="b" id="bn186"><div class="m1"><p>چو کافر گشتم و کفران گزیدم</p></div>
<div class="m2"><p>ز کفران روی خوبت باز دیدم</p></div></div>
<div class="b" id="bn187"><div class="m1"><p>نبد غیری تو دانم جمله هستی</p></div>
<div class="m2"><p>برم اینجا نگنجد بُت پرستی</p></div></div>
<div class="b" id="bn188"><div class="m1"><p>من اینجا کافر عشق تو هستم</p></div>
<div class="m2"><p>بت صورت بمعنی کی پرستم</p></div></div>
<div class="b" id="bn189"><div class="m1"><p>جمال بی نشانی یافتی تو</p></div>
<div class="m2"><p>بسوی بی نشانی تافتی تو</p></div></div>
<div class="b" id="bn190"><div class="m1"><p>جمال بی نشان صورت شده باز</p></div>
<div class="m2"><p>حجاب بت برم آخر برانداز</p></div></div>
<div class="b" id="bn191"><div class="m1"><p>بصورت آدم آمد حق ولی هان</p></div>
<div class="m2"><p>بسی افتاد اینجا عین برهان</p></div></div>
<div class="b" id="bn192"><div class="m1"><p>تو گفتستی که آدم صورت ما است</p></div>
<div class="m2"><p>ز دید من عیانم جمله پیداست</p></div></div>
<div class="b" id="bn193"><div class="m1"><p>اگر سجده کنم هر پیشهٔ من</p></div>
<div class="m2"><p>بود پیش تو این اندیشهٔ من</p></div></div>
<div class="b" id="bn194"><div class="m1"><p>غلط این بُد که خودبینی نمودم</p></div>
<div class="m2"><p>عیان عشق تو کلّی ربودم</p></div></div>
<div class="b" id="bn195"><div class="m1"><p>ز خودبینی شدم در عین لعنت</p></div>
<div class="m2"><p>ولی آخر کنی بر جمله رحمت</p></div></div>
<div class="b" id="bn196"><div class="m1"><p>چو عین بحر رحمت خاص و عامست</p></div>
<div class="m2"><p>از آنجا قطرهٔ ما را تمامست</p></div></div>
<div class="b" id="bn197"><div class="m1"><p>اگر خواهی ببخشی مر مرا بخش</p></div>
<div class="m2"><p>که ذات پاک تو نیکست در نقش</p></div></div>
<div class="b" id="bn198"><div class="m1"><p>کجا وصف تو داند کرد ادراک</p></div>
<div class="m2"><p>که عاجز اوفتاد اندر کف خاک</p></div></div>
<div class="b" id="bn199"><div class="m1"><p>عقول عاقلان گم شد ز حیرت</p></div>
<div class="m2"><p>فرو ماندند اندر عین قربت</p></div></div>
<div class="b" id="bn200"><div class="m1"><p>صفات ذات پاک تو منزّه</p></div>
<div class="m2"><p>عقول افتاد بیخود اندر این ره</p></div></div>
<div class="b" id="bn201"><div class="m1"><p>که باشد عقل کز ذاتت زند دم</p></div>
<div class="m2"><p>که سرگردانست اندر عین عالم</p></div></div>
<div class="b" id="bn202"><div class="m1"><p>که باشد عقل طفلِ شیرخواره</p></div>
<div class="m2"><p>نداند کرد اینجا هیچ چاره</p></div></div>
<div class="b" id="bn203"><div class="m1"><p>که باشد عقل افتاده برابر</p></div>
<div class="m2"><p>فرومانده میان آب و آذر</p></div></div>
<div class="b" id="bn204"><div class="m1"><p>که باشد عقل اینجا باز مانده</p></div>
<div class="m2"><p>میان آرزو و آز مانده</p></div></div>
<div class="b" id="bn205"><div class="m1"><p>که باشد عقل گردان گرد کویت</p></div>
<div class="m2"><p>دونده تا برد ره نیز سویت</p></div></div>
<div class="b" id="bn206"><div class="m1"><p>بسی گشت و ندیدست جز که افعال</p></div>
<div class="m2"><p>نهادش در صفاتت در بیان قال</p></div></div>
<div class="b" id="bn207"><div class="m1"><p>بسی زو عاقبت درمانده عاجز</p></div>
<div class="m2"><p>نمود عشق تو نایافت هرگز</p></div></div>