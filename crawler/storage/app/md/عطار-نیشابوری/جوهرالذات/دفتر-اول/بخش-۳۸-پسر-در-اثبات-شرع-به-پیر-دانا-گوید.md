---
title: >-
    بخش ۳۸ - پسر در اثبات شرع به پیر دانا گوید
---
# بخش ۳۸ - پسر در اثبات شرع به پیر دانا گوید

<div class="b" id="bn1"><div class="m1"><p>کنون ای پیر خواهم رفت دریاب</p></div>
<div class="m2"><p>توئی صورت به نزد ما تو بشتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کنون ای باب خواهم رفت دریاب</p></div>
<div class="m2"><p>که گشتم من زحیرت عین غرقاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کنون ای باب خواهم شد حقیقت</p></div>
<div class="m2"><p>ز دست خود مهل اینجا شریعت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شریعت گوش دار ای عالم دل</p></div>
<div class="m2"><p>که تا مقصود خود بینی تو حاصل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شریعت گوش دار و راز کل بین</p></div>
<div class="m2"><p>تو در عین شریعت نار کل بین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز نور شرع یابی جان جانان</p></div>
<div class="m2"><p>که بعد هر غمی شادیست آسان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز نور شرع ره بینی تو روشن</p></div>
<div class="m2"><p>جهان صورتست اینجا چو گلشن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز نور شرع نور شمس بنگر</p></div>
<div class="m2"><p>که پنهان میشود هر شب بزیور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز نور شرع مه بگداخت هر ماه</p></div>
<div class="m2"><p>که تا بوئی برد در شرع زین راه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شریعت کوش و آنگه کن طریقت</p></div>
<div class="m2"><p>سوم ره دمزن از عین حقیقت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حقیقت در شریعت میتوان یافت</p></div>
<div class="m2"><p>طریقت در حقیقت نیز بشتافت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که تا دریافت اسرار حقیقت</p></div>
<div class="m2"><p>مسلم نیست بی نور شریعت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کسی کو شرع بشناسد ز اللّه</p></div>
<div class="m2"><p>شریعت هست عین قل هواللّه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مقام ایمنی باشد شریعت</p></div>
<div class="m2"><p>که محو آرد نمودار طبیعت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مقام ایمنی در شرع یابی</p></div>
<div class="m2"><p>که اندر وی تواصل و فرع یابی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مقام ایمنی شرعست دریاب</p></div>
<div class="m2"><p>درِ این خانه بگشادست دریاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شریعت ایمنی و ساکنی است</p></div>
<div class="m2"><p>که با نور خدائی هست پیوست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مقام ایمنی زو شد پدیدار</p></div>
<div class="m2"><p>کسی کاین را بجان آمد خریدار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مقام ایمنی دارند در ذات</p></div>
<div class="m2"><p>که اندر شرع مییابند اثبات</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز شرع مصطفی حق بین و حق شو</p></div>
<div class="m2"><p>حقیقت اوّلین و آخرین شو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز شرع مصطفی مگذر زمانی</p></div>
<div class="m2"><p>دمادم تا کنی از وی بیانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شریعت جوی و جان آزاد گردان</p></div>
<div class="m2"><p>ز حق جان و دلت را شاد گردان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شریعت سرّ عالم برگشود است</p></div>
<div class="m2"><p>ره تحقیق حق اینجا نمود است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شریعت راز دارست از حقیقت</p></div>
<div class="m2"><p>که مخفی میکند عین طبیعت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شریعت حق شناس و راه باطل</p></div>
<div class="m2"><p>رها کن تا شود مقصود حاصل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شریعت حق شناس و حق یقین شو</p></div>
<div class="m2"><p>حقیقت اوّلین و آخرین شو</p></div></div>