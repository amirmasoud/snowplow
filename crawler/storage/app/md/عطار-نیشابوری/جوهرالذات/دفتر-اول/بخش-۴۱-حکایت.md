---
title: >-
    بخش ۴۱ - حکایت
---
# بخش ۴۱ - حکایت

<div class="b" id="bn1"><div class="m1"><p>مگر پیری ز پیران رسیده</p></div>
<div class="m2"><p>طلب میکرد مردی راز دیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی پیری بزرگی با ادب بود</p></div>
<div class="m2"><p>ز شوق دوست دائم در طلب بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسی در عین طاعت کرده کوشش</p></div>
<div class="m2"><p>چو دریا بود او در عین جوشش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همیشه صاحب درد و الم بود</p></div>
<div class="m2"><p>ولی در عشق او صاحب قدم بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخوانده علم صورت بود بسیار</p></div>
<div class="m2"><p>نشسته دائما با یار بسیار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طلب میکرد اینجا واصلی او</p></div>
<div class="m2"><p>که تا یابد ز اعیان حاصلی او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طلب میکرد اینجا پیش بینی</p></div>
<div class="m2"><p>در اعیان خدا صاحب یقینی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نشان دادن او را صاحب راز</p></div>
<div class="m2"><p>بشد تا می خبر یابد از او باز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو نزدیک وی آمد زود آن پیر</p></div>
<div class="m2"><p>سلامی کرد و بنشست آن زمان پیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برش بنشست و در وی مانده بد او</p></div>
<div class="m2"><p>که پیری بود او هم نیک و خوشخو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نظر میکرد او را دید خاموش</p></div>
<div class="m2"><p>نمود عشق را میدید با هوش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دمی بنشست با ما او به خلوت</p></div>
<div class="m2"><p>که بود آن پیر صادق مست حضرت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سؤالی کرد آنگاه از حقیقت</p></div>
<div class="m2"><p>که ما را گوی ای پیر طریقت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سؤالی دارم و برگوی ما را</p></div>
<div class="m2"><p>مرنجان مر مرا اینجا خدا را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بگفت ای دوست برگوچه سؤالست</p></div>
<div class="m2"><p>که را ما آشتی نه قیل و قال است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بگو تا من بگویم مر جوابت</p></div>
<div class="m2"><p>که تا چونست این عین صوابت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بگفت ای پیر من جان جهانم</p></div>
<div class="m2"><p>خدا اندر کجایست تا من بدانم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرا بنمای حق گر رهبری تو</p></div>
<div class="m2"><p>که میدانم که نیکو اختری تو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جوابش داد کین نیکو سؤالست</p></div>
<div class="m2"><p>بگویم این من اکنون بی مجال است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>طلبکاری و هم از خود بیابی</p></div>
<div class="m2"><p>اگرنه غرقه اندر بحر آبی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خدا باتست اگر او را بجوئی</p></div>
<div class="m2"><p>حجاب از پیش برداری تو اوئی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حجاب از پیش خود بردار ای پیر</p></div>
<div class="m2"><p>مکن دیگر تو مر این رای و تدبیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خدا با تست و در جانت نهانست</p></div>
<div class="m2"><p>ولی در دید جان عین العیانست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خدا با تست چون تو بنگری تو</p></div>
<div class="m2"><p>چگونه راه او را بسپری تو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خدا با تست در دیدار بنگر</p></div>
<div class="m2"><p>درون جان و دل دیدار بنگر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خدا با تست هرگز او ندیدی</p></div>
<div class="m2"><p>در این دم او ببین چون در رسیدی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خدا با تست این دم زود دریاب</p></div>
<div class="m2"><p>درون جان و دل معبود دریاب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خدا با تست بنموده جمالش</p></div>
<div class="m2"><p>ولیکن چون بیابی تو وصالش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خدا با تست اندر دیده میبین</p></div>
<div class="m2"><p>ولیکن مر ورا در دیده میبین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خدا با تست و در بینائی تست</p></div>
<div class="m2"><p>عیان بنگر که در دانائی تست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خدا با تست در گفتار بنگر</p></div>
<div class="m2"><p>ز من دریاب وین اسرار بنگر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خدا با تست اینجا رخ نموده</p></div>
<div class="m2"><p>ولیکن در دلست و دل ربوده</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خدا با تست اگر دانی بیندیش</p></div>
<div class="m2"><p>حجاب صورتت بردار از پیش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خدا با تست صورت محو گردان</p></div>
<div class="m2"><p>چنین کردند اینجاگاه مردان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خدا با تست جز او کس مبین تو</p></div>
<div class="m2"><p>اگر اینجا شوی راز و یقین تو</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خدا با تست او را میشناسی</p></div>
<div class="m2"><p>نکو کردی که با شکر و سپاسی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خدا با تست و اندر گفتگویست</p></div>
<div class="m2"><p>جز این پیرا بگو چیت آرزویست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خدا با تست ای پیر طریقت</p></div>
<div class="m2"><p>که بسپردی بحق راه شریعت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خدا با تست میدانم که دانی</p></div>
<div class="m2"><p>که هستی پیر و بس صاحب معانی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>درون خود نظر کن یار خود را</p></div>
<div class="m2"><p>که یکّی بینی اندر خود احد را</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>درون جان تو دیدار بنمود</p></div>
<div class="m2"><p>مرا این لحظه کل اسرار بنمود</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تو چندینی که در آفاق گشتی</p></div>
<div class="m2"><p>ندیدی وین زمان کل طاق گشتی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>درون جان نظر کن حق ببین تو</p></div>
<div class="m2"><p>که داری اوّلین و آخرین تو</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>درون جان نظر کن روی دلدار</p></div>
<div class="m2"><p>که از مستی شدی ای پیر هشیار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>درون جان همه اسرار او بین</p></div>
<div class="m2"><p>وجود نقطه در پرگار او بین</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>درون جان نظر کن تا بیابی</p></div>
<div class="m2"><p>اگر هر جای از خود میشتابی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نبینی مرد را جز دیدن خویش</p></div>
<div class="m2"><p>نظر کن این زمان بشنیدن خویش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>زبانت نیز خود گویا به او است</p></div>
<div class="m2"><p>دل و جانت بکل جویای او است</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>دو چشمت هست بینائی از او دان</p></div>
<div class="m2"><p>زمانی رخ از این معنی بگردان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو بود تست او را می چه جوئی</p></div>
<div class="m2"><p>چو او اینجاست با تو، تو چه گوئی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نهان تست و در صورت هویداست</p></div>
<div class="m2"><p>نمودتست او پنهان و پیداست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ترا گفتم اگر دانی تو ای دوست</p></div>
<div class="m2"><p>که دیدار همه در دید تو اوست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تو خود بشناس و حق شو در حقیقت</p></div>
<div class="m2"><p>برون آ از هوا و از طبیعت</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تو خود بشناس تا او را بدانی</p></div>
<div class="m2"><p>اگر هستی تو مر صاحب معانی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تو خود بشناس کاینجا یار باتست</p></div>
<div class="m2"><p>حقیقت بیشکی دلدار با تست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>تو خود بشناس اگر حق میشناسی</p></div>
<div class="m2"><p>چرا در علم حق تو ناسپاسی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تو خود بشناس آنگاهی خدا بین</p></div>
<div class="m2"><p>نمود خود از او در ابتدا بین</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>تو خود بشناس تا واقف شوی تو</p></div>
<div class="m2"><p>ز دید دید حق واصف شوی تو</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>تو خود بشناس تا واقف شوی هان</p></div>
<div class="m2"><p>دل خود از بلای نفس برهان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>تو خود بشناس و خود دیدار او بین</p></div>
<div class="m2"><p>نمود جان و تن اسرار او بین</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>تو خود بشناس کاین جاگه قبولی</p></div>
<div class="m2"><p>چو حق دیدی عیان صاحب وصولی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>تو خود بشناس چون حقی تو در حق</p></div>
<div class="m2"><p>خبر دادم ترا از راز مطلق</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>تو خود بشناس و همچون خود فنا باش</p></div>
<div class="m2"><p>در آن دید فنا سّر خدا باش</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>تو خود بشناس تا جانان شوی کل</p></div>
<div class="m2"><p>ز دید خویشتن پنهان شوی کل</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>تو خود بشناس و جز حق هیچ منگر</p></div>
<div class="m2"><p>صور هیچست اندر هیچ منگر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>تو خود بشناس و اندر حق نظر کن</p></div>
<div class="m2"><p>دل خود را از این معنی خبر کن</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>تو خود بشناس و آنگه پادشا شو</p></div>
<div class="m2"><p>ز اسم صورت و معنی خدا شو</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>تو خود بشناس تا جانان شوی تو</p></div>
<div class="m2"><p>اگر معنی خدا الحق شوی تو</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>خدا شو گر تو جانان دوست داری</p></div>
<div class="m2"><p>تو مغزی چون نظر با پوست داری</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>تو جانانی و آگاهی نداری</p></div>
<div class="m2"><p>که این دم ملکت و شاهی نداری</p></div></div>