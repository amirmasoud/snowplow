---
title: >-
    بخش ۳۰ - پسر در راز معنی و در نوع حقیقت کل گوید
---
# بخش ۳۰ - پسر در راز معنی و در نوع حقیقت کل گوید

<div class="b" id="bn1"><div class="m1"><p>پسر گفت ای پدر قول حدیثت</p></div>
<div class="m2"><p>ترا بر من چنین دامی خبیثست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه در عالم جان عین جانان</p></div>
<div class="m2"><p>توئی در بود من اسرار پنهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منم در عین کشتی بحر اعظم</p></div>
<div class="m2"><p>توئی از دید من اسرار عالم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منم با تو درون بحر هستی</p></div>
<div class="m2"><p>پدر در عقل ماندستی ومستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منم از آن صدف درّ یگانه</p></div>
<div class="m2"><p>که خواهم بُد ترا من جاودانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من ای بابا سخن زینسان بگویم</p></div>
<div class="m2"><p>که حق میآرد اندر گفتگویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرا بابا سخن بیهوده گفتی</p></div>
<div class="m2"><p>که در دیدار خود نادیده سُفتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا میسوی دریا گفتی اینجا</p></div>
<div class="m2"><p>که اندازم نکو گفتی در اینجا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسوی بحرم انداز و نظر کن</p></div>
<div class="m2"><p>ز سرّ خود نمود جان خبر کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا در سوی بحر انداز و جان بین</p></div>
<div class="m2"><p>حقیقت در دلم عین العیان بین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا در سوی بحر آخر درانداز</p></div>
<div class="m2"><p>مرا از دید خود این لحظه بنواز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا در سوی بحر خود زمانی</p></div>
<div class="m2"><p>فکن تا من بخوانم داستانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرا در سوی بحر انداز و بگذر</p></div>
<div class="m2"><p>ولی اکنون نهٔ زین ره تو رهبر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پدر من دارم اسرار حقیقت</p></div>
<div class="m2"><p>پدر من دیدم انوار طریقت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پدر جانا منم در روی عالم</p></div>
<div class="m2"><p>که آوردم ز دل سرّ دمادم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>منم آن جوهر ذات و صفاتت</p></div>
<div class="m2"><p>پدر تا دانی و مگذر ز ذاتت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز ذات خود بدان اسرار ما باز</p></div>
<div class="m2"><p>حجاب از پیش دیدارم برانداز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پدر داری سر آن کین زمان تو</p></div>
<div class="m2"><p>بیابی این جهان و آن جهان تو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرا انداز سوی بحر هستی</p></div>
<div class="m2"><p>مکن چندین تو بر من پیش دستی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نمیگیرد مرا تقلید اینجا</p></div>
<div class="m2"><p>که دیدستم نمود دید اینجا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نخواهم رفت از این دریا برون من</p></div>
<div class="m2"><p>که تا باشم شما را رهنمون من</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>قضای حق کسی هرگز نداند</p></div>
<div class="m2"><p>وگر داند در اینجا خیره ماند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مرا الهام میگوید که ای باب</p></div>
<div class="m2"><p>که خواهم شد در اینجاگاه غرقاب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مر الهام میگوید که جان شو</p></div>
<div class="m2"><p>برون از عالم کون و مکان شو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مر الهام میگوید که باز آی</p></div>
<div class="m2"><p>گره یکبارگی ازخویش بگشای</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مر الهام میگوید در اینجا</p></div>
<div class="m2"><p>که ناپیدا شو اندر عین دریا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مر الهام میگوید که دانی</p></div>
<div class="m2"><p>چرا در کشتی عین صفاتی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مر الهام میگوید که بگذر</p></div>
<div class="m2"><p>درون بحر تا یابی تو جوهر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مر الهام میآید که دیدی</p></div>
<div class="m2"><p>نمود ماجرا تو آرمیدی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مر الهام جانانست امروز</p></div>
<div class="m2"><p>که پیدائیم پنهانست امروز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مر الهام میآید ز دلدار</p></div>
<div class="m2"><p>که کم گردان نمود خود به یک بار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مر الهام میآید که نوری</p></div>
<div class="m2"><p>در این دریای کل صاحب حضوری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مر الهام جانانست در دل</p></div>
<div class="m2"><p>شدم بی عین این گفتار واصل</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>منم واصل پدر بی عقل در جان</p></div>
<div class="m2"><p>مرا بنموده رخ چون جان جانان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>من ار کشتی شکستم صورت خود</p></div>
<div class="m2"><p>که نیکو گویم و دورم من از بد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در این کشتی کجا بینی تو اسرار</p></div>
<div class="m2"><p>که چیزی نیست جز دریا پدیدار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در این دریا منم منصور بنگر</p></div>
<div class="m2"><p>در این عالم منم مشهور بنگر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>پدر اسرار من هر دو جهانست</p></div>
<div class="m2"><p>ولی از چشم نامحرم نهانست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>پدر اسرار من کلّی صفاتست</p></div>
<div class="m2"><p>مرا دریا نموداری ز ذاتست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تو در دریای ذات من قدم نه</p></div>
<div class="m2"><p>وجود خویش در عین عدم نه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تو در دریای ذات من چنانی</p></div>
<div class="m2"><p>مثال قطره در بحر نهانی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تو در دریای ذات من فتادی</p></div>
<div class="m2"><p>دریغا ای پدر دادی ندادی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تو در دریای ذات من نهانی</p></div>
<div class="m2"><p>نمود گفت من بابا ندانی</p></div></div>