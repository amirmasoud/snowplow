---
title: >-
    بخش ۱۹ - در اظهار کردن قوّت و قدرت و استغناء کل فرماید
---
# بخش ۱۹ - در اظهار کردن قوّت و قدرت و استغناء کل فرماید

<div class="b" id="bn1"><div class="m1"><p>منم یکتا که جمله دستگیرم</p></div>
<div class="m2"><p>بمیرانم تمامت من نمیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منم حییّ که دایم زنده باشم</p></div>
<div class="m2"><p>که بیخ بدکنش برکنده باشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ستانم داد مظلومان ز ظالم</p></div>
<div class="m2"><p>بذات خویش من پیوسته قائم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهان و هرچه در هر دو جهان است</p></div>
<div class="m2"><p>بر من جمله بی نام و نشانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خدایم من خدایم من خدایم</p></div>
<div class="m2"><p>که از هر عیب و سهوی من جدایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من آوردم تمامت اندرین جای</p></div>
<div class="m2"><p>برم بار دگر در غیر ماوای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی بردم در اوّل هم در آخر</p></div>
<div class="m2"><p>نمودم خویشتن در عین ظاهر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه در خویشتن پیدا نمودم</p></div>
<div class="m2"><p>ز دید خود چنین غوغا نمودم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز ذاتم عقل و جان آگه نباشد</p></div>
<div class="m2"><p>بجز من هیچکس اللّه نباشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز ذاتم عقل و جان اینجا خبر نیست</p></div>
<div class="m2"><p>که من در بود خود هستم دگر نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من آوردم شما را هم بدنیا</p></div>
<div class="m2"><p>برم من جمله اندر سوی عقبی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کجا و همت تواند کرد ادراک</p></div>
<div class="m2"><p>که ادراکست و عقل افتاده در خاک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>منزّه آمدم از جمله خلقان</p></div>
<div class="m2"><p>منم بیشک نمود جمله میدان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خدائی مر مرا باشد سزاوار</p></div>
<div class="m2"><p>که گر خواهم بیامرزم به یک بار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خدائی مر مرا باشد بتحقیق</p></div>
<div class="m2"><p>که هر کس را ببخشم عین توفیق</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دهم توفیق دیدارم ببیند</p></div>
<div class="m2"><p>ابا من در میان جان نشیند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>منم یکتای بی همتا که بودم</p></div>
<div class="m2"><p>نمود جملگی در بود بودم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز وصف ذات پاکم عقل ماندست</p></div>
<div class="m2"><p>از آن پیوسته اندر نقل ماندست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز وصف ذات پاکم جان چه گوید</p></div>
<div class="m2"><p>اگر جز دید من چیزی بجوید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز وصف من تمامت گنگ و لالند</p></div>
<div class="m2"><p>ز من اندر تجلّی جلالند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز وصف من تمامت گشته حیران</p></div>
<div class="m2"><p>که ما هستیم اندر پرده پنهان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز وصف من همه در بحر مانند</p></div>
<div class="m2"><p>اگرچه بود هم خود نمایند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کجاوصفم تواند کرد هر کس</p></div>
<div class="m2"><p>که من در دید خود اللّهم و بس</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کجا وصفم تواند کرد هر جان</p></div>
<div class="m2"><p>منم جسم و منم جان اندر اعیان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>حقیقت من منم یکتا و دلدار</p></div>
<div class="m2"><p>ز ذات خویشتن مائیم جبّار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عیانم در همه چیزی تو بنگر</p></div>
<div class="m2"><p>بجز دیدارما تو هیچ منگر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>فلک گردان ز من وز شوق مدهوش</p></div>
<div class="m2"><p>کواکب جمله حیرانند و خاموش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مه از شوقم گدازانست هر ماه</p></div>
<div class="m2"><p>سپر انداخته از بیم من شاه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کنم من شمس هر شام رخ زرد</p></div>
<div class="m2"><p>که از دیدار من باشد پر از درد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز دردم جبرئیل اینجای مدهوش</p></div>
<div class="m2"><p>بمانده در درون پرده خاموش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ملایک جمله در من راز بینند</p></div>
<div class="m2"><p>که در دیدار ما خلوت گزینند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>که عرش از دید من بر قطرهٔ آب</p></div>
<div class="m2"><p>بماندست اندر اینجا عین غرقاب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز بودم فرش گوناگون پدیدار</p></div>
<div class="m2"><p>نموده رخ در این دیدار پرگار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز عینم در بهشت افتاده دائم</p></div>
<div class="m2"><p>نموده روح و ریحان گشته قائم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز دوزخ کس امان من ندارد</p></div>
<div class="m2"><p>که اینجا جز عیان من ندارد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>منم بیچون و دانم راز جمله</p></div>
<div class="m2"><p>منم انجام و هم آغاز جمله</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>منم دانا و بینا در دل و چشم</p></div>
<div class="m2"><p>که بر بنده نگیرم زود من خشم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>منم پیدا و پنهان جهانم</p></div>
<div class="m2"><p>که در نطق همه شرح و بیانم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو من هرگز نباشد پادشاهی</p></div>
<div class="m2"><p>چو من هرگز نبینی نیکخواهی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو من هرگز کجا همراز ببینی</p></div>
<div class="m2"><p>نمودستم اگر خود باز بینی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چون من دیگر کجا در جان بیابی</p></div>
<div class="m2"><p>سزد گر مر مرا اعیان نیابی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز وصف خویش دائم در حضورم</p></div>
<div class="m2"><p>که در ظلمات تنهائیت نورم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز وصف خویش خود را رازگویم</p></div>
<div class="m2"><p>نمود خویش با خود بازگویم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز دید خویش دائم در جلالم</p></div>
<div class="m2"><p>ز نورخوش قائم در وصالم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز نور خود نمودم جمله اشیاء</p></div>
<div class="m2"><p>ز بود خویش کردم جمله پیدا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>زخون مشک و ز نِی شِکّر نمایم</p></div>
<div class="m2"><p>ز باران در زکان گوهر نمایم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز کفّ خود برآرم آدمی را</p></div>
<div class="m2"><p>ز کاف ونون فلک را و زمین را</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز دودی گنبد خضرا کنم من</p></div>
<div class="m2"><p>ز پیهی نرگسی بینا کنم من</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مه و خورشید دائم در سجودم</p></div>
<div class="m2"><p>که ایشانند در نور نمودم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نهان از خلق و پنهان از خیالم</p></div>
<div class="m2"><p>که نور در تجلّی جمالم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ز وصفم عقل در پرده نهان شد</p></div>
<div class="m2"><p>ز دیدم عشق هرجائی عیان شد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>منم اوّل منم آخر در اشیاء</p></div>
<div class="m2"><p>مرا باشد همه صنعی مهّیا</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>که بنمایم وجود و پی کنم من</p></div>
<div class="m2"><p>نمایم ظلمت اندر نور روشن</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>همه دروصف من حیران و خاموش</p></div>
<div class="m2"><p>زبان ناطقانم لال و خاموش</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ز دید خویش جمله آفریدم</p></div>
<div class="m2"><p>در این روی زمین شان آوریدم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ز ذات خود محمد(ص) راز دادم</p></div>
<div class="m2"><p>نمودم تا ز خود اعزاز دادم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>حبیب من زجمله مصطفایست</p></div>
<div class="m2"><p>شما را پیشوا و رهنمایست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نمودم شرع در دیدار احمد(ص)</p></div>
<div class="m2"><p>که هر کو شه بجان دیندار احمد(ص)</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>هر آنکس کو رسول خود شناسد</p></div>
<div class="m2"><p>مرا در دید خود احمد شناسد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>نمایم مر ورا دیدار خویشم</p></div>
<div class="m2"><p>که من در عشق برخوردار خویشم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>هر آن کو راه پیغامبر گزیند</p></div>
<div class="m2"><p>یقین اندر جهان او بد نبیند</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>حبیب من زجان مردوست دارند</p></div>
<div class="m2"><p>نمود عشق ما را یاد دارند</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>کنون ای پیر توحیدم شنیدی</p></div>
<div class="m2"><p>درون ذاتم اعیان باز دیدی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>برو با مسکن خود زودبین باش</p></div>
<div class="m2"><p>وز این گفتار با عین الیقین باش</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>خوشا آنکس که ما را دید در ذات</p></div>
<div class="m2"><p>گذشت از جسم و جان جمله ذرات</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>خوشا آنکس که جز ما کس نبیند</p></div>
<div class="m2"><p>یقین ذات ما را برگزیند</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چو باهوش آئی و بینی یقینم</p></div>
<div class="m2"><p>نظر کن اوّلین و آخرینم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>همه اندر درون خویشتن بین</p></div>
<div class="m2"><p>نمود جسم را در جان جان بین</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بر هر کس مگو اسرار ما فاش</p></div>
<div class="m2"><p>ز دیدارم تو برخوردار میباش</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>حریم وصل ما میدان و میرو</p></div>
<div class="m2"><p>بجز ما را مبین و هیچ مشنو</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>که ذات پاک ما هرگز نیابند</p></div>
<div class="m2"><p>اگرچه سالکان نزدم شتابند</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>نبینید هیچکس ما را به تحقیق</p></div>
<div class="m2"><p>مگر آنکس که یابد چشم توفیق</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>نبینید هیچکس ما را چنان باز</p></div>
<div class="m2"><p>که تا اینجا نگردد جسم و جان باز</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>کسی کو بی سر آید اندر این راه</p></div>
<div class="m2"><p>بیابد مر مرا بی خویش ناگاه</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>اگر بی سر شوی این سر بدانی</p></div>
<div class="m2"><p>وگرنه گربه چند از جاه خوانی</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>اگر بی سر شوی اسرار یابی</p></div>
<div class="m2"><p>ابی دیدار خود دلدار یابی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>اگر بی سر شوی فانی نباشی</p></div>
<div class="m2"><p>نمود جزو و کل را جان تو باشی</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>سر خود دورنه تا دید دیدار</p></div>
<div class="m2"><p>ببینی در حقیقت جان دلدار</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>سر خود دورنه مانند حلّاج</p></div>
<div class="m2"><p>که تا بر فرق معنایت نهد تاج</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>سر خود دور نه گر کاردانی</p></div>
<div class="m2"><p>که مردن بهتر از این زندگانی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>سر خود دورنه تا یار گردی</p></div>
<div class="m2"><p>ز نقطه بگذری پرگار گردی</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>سر خود دورنه مانند مردان</p></div>
<div class="m2"><p>که بهر تست خدمتکار دو جهان</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>سر خود دورنه اندر بلا تو</p></div>
<div class="m2"><p>بمانند شهید کربلا تو</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>سر خود دورنه مانند جرجیس</p></div>
<div class="m2"><p>چرا چندین شوی در مکر و تلبیس</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>سر خود دورنه مانند یحیی</p></div>
<div class="m2"><p>که تا گردی ز پنهانی تو پیدا</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>سر خود دورنه تا سر تو باشی</p></div>
<div class="m2"><p>نمود عالم اکبر تو باشی</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>سر خود دورنه تا سر تو گردی</p></div>
<div class="m2"><p>بیکباره ز ما و من تو گردی</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>سر خود دورنه تا دوست گردی</p></div>
<div class="m2"><p>حقیقت مغز جان در پوست گردی</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>سر خود دورنه همچون شهیدان</p></div>
<div class="m2"><p>که تا یابی وصالان حبیبان</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>سر خود دورنه مانند گوئی</p></div>
<div class="m2"><p>بزن چون عاشقانه تو های و هوئی</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>سر خود دورنه تا بر سر دار</p></div>
<div class="m2"><p>ببین خویشتن را عین جبار</p></div></div>
<div class="b" id="bn92"><div class="m1"><p> سر خود دورنه در خاک و خون شو</p></div>
<div class="m2"><p>ز عین این جهان دون برون شو</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>اناالحق گوی تا واصل بباشی</p></div>
<div class="m2"><p>فنای عشق را لایق تو باشی</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>اناالحق گوی تا مانند منصور</p></div>
<div class="m2"><p>برافشان اندر اینجا جوهر نور</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>اناالحق گوی و سر بردار و سر بر</p></div>
<div class="m2"><p>که جوهر مینباشد کمتر از زر</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>اناالحق گوی و درجمله قدم زن</p></div>
<div class="m2"><p>وجود خویشتن را بر عدم زن</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>اناالحق گوی و محو آور وجودت</p></div>
<div class="m2"><p>نظر کن آنگهی مر بود بودت</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>اناالحق گوی اگر حق الیقینی</p></div>
<div class="m2"><p>چرا مانده تو اندر کفر و دینی</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>اناالحق گوی و بگذر کلّی از دین</p></div>
<div class="m2"><p>هم اندر حق حقیقت عین خودبین</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>اناالحق گوی اینجا آشکاره</p></div>
<div class="m2"><p>ز عشق دوست شو تو پاره پاره</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>اناالحق گوی تا یکتا بباشی</p></div>
<div class="m2"><p>میان جزو و کل رسوا تو باشی</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>اناالحق گوی و بگذر از دل و جان</p></div>
<div class="m2"><p>دل وجان بر نثار حق بر افشان</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>اناالحق گوی چون گوئی همی گرد</p></div>
<div class="m2"><p>اگر در عشق مردی مردهٔ مرد</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>اناالحق گوی بر مانند عطّار</p></div>
<div class="m2"><p>که آویزندت اینجا بر سر دار</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>اناالحق گوی چون جوئی حقیقت</p></div>
<div class="m2"><p>ببردی هم طریقت هم شریعت</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>اناالحق گوی چون حق رخ نمودست</p></div>
<div class="m2"><p>که حق اینجا ترا گفت و شنود است</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>اناالحق گوی و عین لامکان شو</p></div>
<div class="m2"><p>چو مردان بی زمین و بی زمان شو</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>اناالحق گوی تا خونت بباشی</p></div>
<div class="m2"><p>که حق حق حقیقت هم تو باشی</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>اناالحق گوی تو اینجا اناالحق</p></div>
<div class="m2"><p>که نه بر باطلی الاّ که بر حق</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>اناالحق گوی تا چون او شوی باز</p></div>
<div class="m2"><p>نمو عشق گردی اندرین راز</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>اناالحق گوی چون حق دیدهٔ تو</p></div>
<div class="m2"><p>حقیقت نور مطلق دیدهٔ تو</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>اناالحق گوی کاشترنامه خواندی</p></div>
<div class="m2"><p>همه اندر قطار اشتر تو راندی</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>اناالحق گوی کاشتر آشکارست</p></div>
<div class="m2"><p>که این معنی چو اشتر بر قطارست</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>اناالحق گوی و ز دیرت برون آی</p></div>
<div class="m2"><p>نمود دیر و کعبه هر دو بنمای</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>اناالحق گوی این کعبه برانداز</p></div>
<div class="m2"><p>تو چون شمعی وجود خویش بگداز</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>اناالحق گوی کان دیرت خرابست</p></div>
<div class="m2"><p>درون دیر بیشک آفتابست</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>اناالحق گوی اینجا بت شکن باش</p></div>
<div class="m2"><p>وگرنه اندرین نی مرد و زن باش</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>اناالحق زن چو مردان تا توانی</p></div>
<div class="m2"><p>که بهر تُست اسرار معانی</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>اناالحق زن چو مردان در جهان تو</p></div>
<div class="m2"><p>گذر کن از زمین و از زمان تو</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>اناالحق زن چو مردان بر سر دار</p></div>
<div class="m2"><p>اگر تو خود زنی این سر نگهدار</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>اناالحق گفت و پس بردار آمد</p></div>
<div class="m2"><p>ز دید دوست برخوردار آمد</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>اناالحق گفت و شد قربان در اینراه</p></div>
<div class="m2"><p>یکی دیدار جان باشد در این راه</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>اناالحق گفت و قربان گشت از دوست</p></div>
<div class="m2"><p>در اینجا مغز گشتش جملگی پوست</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>اناالحق گفت و گفتارش یکی بود</p></div>
<div class="m2"><p>خدا را دید واصل بیشکی بود</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>اناالحق گفت و در حق حق نظر کرد</p></div>
<div class="m2"><p>همه ذرات عالم را خبر کرد</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>اناالحق گفت و جانان دید از جان</p></div>
<div class="m2"><p>دُر افشاندند و او آمد سر افشان</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>اناالحق گفت او چون راست اینجا</p></div>
<div class="m2"><p>بگفتِ عشق او پیداست اینجا</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>اناالحق گفت و عشقش یار بنمود</p></div>
<div class="m2"><p>گره از کار عالم جمله بگشود</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>اناالحق گفت و حق حق دید اینجا</p></div>
<div class="m2"><p>که دیداریست پنهانی و پیدا</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>اناالحق گفت تو گر باز بینی</p></div>
<div class="m2"><p>سزد گر حق در اینجا باز بینی</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>اناالحق آنکسی داند که از خود</p></div>
<div class="m2"><p>رود بیرون نبیند نیک هم بد</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>اناالحق زن یقین اللّه باشد</p></div>
<div class="m2"><p>کسی کو از عیان آگاه باشد</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>چو منصوراز حقیقت مست حق شد</p></div>
<div class="m2"><p>حقیقت نیست گشت و هست حق شد</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>چو منصوراز حقیقت یافت جانان</p></div>
<div class="m2"><p>ز پیدائی شد اینجاگاه پنهان</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>چو منصوراز حقیقت راست بین بود</p></div>
<div class="m2"><p>حقیقت جان او عین الیقین بود</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>چو منصوراز حقیقت دید حق باز</p></div>
<div class="m2"><p>حقیقت گفت و شد با حق سوی یار</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>چو منصوراز حقیقت لاف کل زد</p></div>
<div class="m2"><p>چو سیمرغی خود اندر قاف کل زد</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>چو منصوراز حقیقت لامکان بود</p></div>
<div class="m2"><p>از آن او فتنهٔ کلّ جهان بود</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>چو منصوراز حقیقت بیجهت شد</p></div>
<div class="m2"><p>ز ذات کل بحق او یک صفت شد</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>چو منصوراز حقیقت دل رها کرد</p></div>
<div class="m2"><p>ز جان آهنگ دیدار خدا کرد</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>چو منصوراز حقیقت جان برانداخت</p></div>
<div class="m2"><p>چو شمعی در عیان عشق بگداخت</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>چو منصوراز حقیقت کل فنا شد</p></div>
<div class="m2"><p>حقیقت جاودان عین بقا شد</p></div></div>