---
title: >-
    بخش ۳۴ - در صفت پیر دانا و حکایت اسرار کردن کل با او فرماید
---
# بخش ۳۴ - در صفت پیر دانا و حکایت اسرار کردن کل با او فرماید

<div class="b" id="bn1"><div class="m1"><p>میان کشتی آنجا بود پیری</p></div>
<div class="m2"><p>بمعنی و بصورت بی نظیری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بقدر خویشتن واصل بدش او</p></div>
<div class="m2"><p>همه اسبابها حاصل بدش او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میان جمله مردان بود او مرد</p></div>
<div class="m2"><p>در آن کشتی که بودش صاحب درد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سفر کرده بسی دانسته اسرار</p></div>
<div class="m2"><p>گرفته سالها او انس دلدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بمعنی برتر از هر دو جهان بود</p></div>
<div class="m2"><p>ز عشق و عقل او صاحب بیان بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز درد عشق جانان باخبر بود</p></div>
<div class="m2"><p>ز دید جزو و کل صاحب نظر بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همی در عین اعیان بود با یار</p></div>
<div class="m2"><p>که کرده بد سفرها نیز بسیار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>علوم علم جان حاصل بکرده</p></div>
<div class="m2"><p>وز آنجا گاه خود واصل بکرده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ره جانان سپرده بود آن پیر</p></div>
<div class="m2"><p>در آن شرح پسر میکرد تأخیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زمانی صبر کرد و گشت خاموش</p></div>
<div class="m2"><p>دلش از شوق چون دریا زنان جوش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خوشش میآمد آن اسرار جانان</p></div>
<div class="m2"><p>ز پیدایی نمودی خویش پنهان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همی دید و گمانش در یقین بود</p></div>
<div class="m2"><p>که در عشق ازل او راه بین بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همی دانست سرّی هست او را</p></div>
<div class="m2"><p>که میگفت از حقیقت آن نکو را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همی دانست و میدیدش نمودار</p></div>
<div class="m2"><p>که میگفت او همی در عین اسرار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دل آن پیر معنی موج جان زد</p></div>
<div class="m2"><p>به یک دم او دم شرح و بیان زد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نظر کردش بسوی آن پسر گفت</p></div>
<div class="m2"><p>که این معنی که گفتست و که اشنفت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نکو میگویی ار هستی خبردار</p></div>
<div class="m2"><p>مشو بیهوش وز ما تو خبردار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ترا شد این مسلّم تا بدانی</p></div>
<div class="m2"><p>که جوهر سوی دریا میفشانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ترا شد این مسلّم در حقیقت</p></div>
<div class="m2"><p>که می جوئی ره عین طریقت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ترا شد این مسلّم راز و گفتار</p></div>
<div class="m2"><p>که داری در حقیقت حق پدیدار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ترا شد این مسلّم سرّ عالم</p></div>
<div class="m2"><p>که دم از حق زدی اینجا دمادم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ترا شد این مسلّم در نهانی</p></div>
<div class="m2"><p>که گفتی این همه شرح و معانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دم وحدت ز دستی بیشکی تو</p></div>
<div class="m2"><p>که دیدستی مر این دریا یکی تو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دم وحدت زدی از راه مستی</p></div>
<div class="m2"><p>در این کشتی مرا انباز گشتی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دم وحدت زدی و جان جانی</p></div>
<div class="m2"><p>توئی در جان من صاحب معانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دم وحدت زدی و گوش کردم</p></div>
<div class="m2"><p>دل و جان در برت بیهوش کردم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دم وحدت زدی و کائناتی</p></div>
<div class="m2"><p>ولیکن این زمان عین صفاتی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دم وحدت زدی و جمله هستی</p></div>
<div class="m2"><p>که پنداری بت صورت شکستی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دم وحدت زدی و یار مائی</p></div>
<div class="m2"><p>گره از کار من این دم گشائی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دم وحدت زدی ودیدمت کل</p></div>
<div class="m2"><p>دمی فارغ شدم از رنج و از ذل</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دم وحدت زدی و بی نشانی</p></div>
<div class="m2"><p>همه اسرار معنی میفشانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دم وحدت زدی ازنقش دریا</p></div>
<div class="m2"><p>توئی در هر دو دریا دوست یکتا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دم وحدت زدیّ و جان ببردی</p></div>
<div class="m2"><p>به معنی بس بزرگی گر چه خردی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دم وحدت زدی و دل ربودی</p></div>
<div class="m2"><p>یقین دانم که ما را بود بودی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دم وحدت زدی در عقل رفتم</p></div>
<div class="m2"><p>ز تو اشنفتم و هم با تو گفتم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دم وحدت تو داری که خدائی</p></div>
<div class="m2"><p>چرا از دید ماتو میجدائی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چوداری جزو و کل در دید دلدار</p></div>
<div class="m2"><p>منم از جان ترا اینجا خبردار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ترا میدانم و آنجات دیدم</p></div>
<div class="m2"><p>در این دریا در آن دریات دیدم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تو دریائی و دریا قطرهٔ تست</p></div>
<div class="m2"><p>تو خورشیدی و عالم ذرّهٔ تست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تو دریائی و جان جوهر نمودی</p></div>
<div class="m2"><p>چرا جوهر ز چنگ خود ربودی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تو دریائی و هستی عین کشتی</p></div>
<div class="m2"><p>نبد جائی که آنجاگه نگشتی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>همه ذرّات عالم مست ذاتت</p></div>
<div class="m2"><p>نمودار آمده اندر صفاتت</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همه ذرّات جویان تو هستند</p></div>
<div class="m2"><p>از این خمخانه دیرتو مستند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>همه ذرّات عالم گشته جویان</p></div>
<div class="m2"><p>ترا در وحدت کل جمله گویان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>همه ذرّات اندر گفتگویند</p></div>
<div class="m2"><p>توئی در جمله و جمله تو جویند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>همه ذرّات میدانند بتحقیق</p></div>
<div class="m2"><p>که از تو یافتند این عین توفیق</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>همه ذرّات میبینند دیدت</p></div>
<div class="m2"><p>شدند از جان بکلی ناپدیدت</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>همه ذرّات مستند و سر از پای</p></div>
<div class="m2"><p>نمیدانند رفته جمله از جای</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>کجا کانجا نباشد دیدن تست</p></div>
<div class="m2"><p>همه گفت تو و بشنیدن تست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>کجا اینجا نه هستی و ندیدند</p></div>
<div class="m2"><p>چرا کاندر نمودت ناپدیدند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>کجائی این زمان اندر دل و جان</p></div>
<div class="m2"><p>در این کشتی نمودی راز پنهان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو پیدائی چرا پنهان شوی تو</p></div>
<div class="m2"><p>چو با من هستی جانان شوی تو</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چگونه یافتم بر گوی با من</p></div>
<div class="m2"><p>بیانی گوی با من سخت روشن</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بسی کردم سفر زان سوی دریا</p></div>
<div class="m2"><p>ز بهر دیدنت ای جان جانها</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بسی کردم سفر در چین و ماچین</p></div>
<div class="m2"><p>ز بهر رویت ای خورشید ره بین</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بسی گردیدم و دریافتم هان</p></div>
<div class="m2"><p>مرا این دم از این صورت تو برهان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بسی با سالکان این ره سپردم</p></div>
<div class="m2"><p>که تا موئی ز وصلت راه بردم</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بسی با سالکان گردیدم ای جان</p></div>
<div class="m2"><p>نمود عشق اینجا دیدم ای جان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بسی گشتم بسی دیدم کسانت</p></div>
<div class="m2"><p>شدم خاک قدوم رهروانت</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بسی سودای تو اینجای پختم</p></div>
<div class="m2"><p>هنوز از خام کاری نیم پختم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بسی در دیدن رویت بگشتم</p></div>
<div class="m2"><p>بسی دریا بسی صحرا بگشتم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بسی با واصلان تقریر گفتم</p></div>
<div class="m2"><p>همه از آیت و تفسیر گفتم</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بسی سر بر سر زانو نهادم</p></div>
<div class="m2"><p>ز پای خود به زانو درفتادم</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بسی اندر چله سی پاره خواندم</p></div>
<div class="m2"><p>ز خان و مان کنون آواره ماندم</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بسی با رِند در میخانهٔ تو</p></div>
<div class="m2"><p>نشستم این زمان دیوانهٔ تو</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بسی گفتم و بسیاری شنودم</p></div>
<div class="m2"><p>دمی از جستجو فارغ نبودم</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بسی کردم اینجاگه طلب باز</p></div>
<div class="m2"><p>که تادیدم ترا این جایگاه باز</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>کنون وقتست اگر ما رو نمائی</p></div>
<div class="m2"><p>جهان جان توئی و هم خدائی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>کنون سی و سه سالست ازنمودار</p></div>
<div class="m2"><p>که یک شب دیدمت در خواب بیدار</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>نمود خود نمودی این چنینم</p></div>
<div class="m2"><p>که امروزی ترا عین الیقینم</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>شده دید جمالت آشکاره</p></div>
<div class="m2"><p>برویت جزو و کل گشته نظاره</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>در این دریا نمودت باز اوّل</p></div>
<div class="m2"><p>کجا باشد صفات تو مبدّل</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>تو داری و تو دانیّ و تو گوئی</p></div>
<div class="m2"><p>توئی شاه و تو سلطان نکوئی</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>نمیداند پدر ذاتت تمامی</p></div>
<div class="m2"><p>که از تو یافتست او نیکنامی</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>نمیداند پدر اسرارت ای جان</p></div>
<div class="m2"><p>که پیدائی بصورت لیک پنهان</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بمعنی برتر از جانی و صورت</p></div>
<div class="m2"><p>ترا دادند دیدار حضورت</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>توئی معنی و صورت دیدن تست</p></div>
<div class="m2"><p>عیان گفتار من بشنیدن تست</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>توئی جان و جهان عالم دل</p></div>
<div class="m2"><p>که بگشائی تمامت راز مشکل</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>توئی منصور تا دانی که دانم</p></div>
<div class="m2"><p>که جز دیدار تو چیزی ندانم</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>توئی منصور صوری در همه دم</p></div>
<div class="m2"><p>تو هستی دادهٔ در عین عالم</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>توئی منصور کز حدّ جلالت</p></div>
<div class="m2"><p>نداند هیچکس جز خود کمالت</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>توئی منصور در عین حضوری</p></div>
<div class="m2"><p>که نزدیکی بجمله لیک دوری</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>توئی منصور و در عین لقائی</p></div>
<div class="m2"><p>سپر گشته تو در عین بلائی</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>ترا بسیار برهانست اینجا</p></div>
<div class="m2"><p>که دیدت دید جانانست اینجا</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>حقیقت برتر از کون و مکانی</p></div>
<div class="m2"><p>که هم جسمی و بیشک جان جانی</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>ترا بیشک حقیقت حق شناسم</p></div>
<div class="m2"><p>که از دید تو با شکر و سپاسم</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>ترا بیشک حقیقت شد مسلّم</p></div>
<div class="m2"><p>توئی نور جهان و جسم آدم</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>خدا داری درون دل بتحقیق</p></div>
<div class="m2"><p>تو بردی گوی از میدان توفیق</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>خدا داری حقیقت در درونت</p></div>
<div class="m2"><p>خدا باشد حقیقت رهنمونت</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>تو بنمودی رخ اندر عالم جان</p></div>
<div class="m2"><p>تو هستی در بهشت آدم جان</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>تو بنمودی حقیقت روی ما را</p></div>
<div class="m2"><p>تو آوردی همه در کون ما را</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>تو جانی و جهان هم سایهٔ تست</p></div>
<div class="m2"><p>تو نوری شمس همچون سایهٔ تست</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>تو روحی و دل و جان رهبر آمد</p></div>
<div class="m2"><p>که بودت جَست از خود بر درآمد</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>کنون چون دیدمت بنمای رخسار</p></div>
<div class="m2"><p>که تا کلّی شوی بر من پدیدار</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>از این دریا که افتادم یقین من</p></div>
<div class="m2"><p>ترا دیدم کنون عین الیقین من</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>از این دریا تو داری جوهر نور</p></div>
<div class="m2"><p>ترا دانسته است اینجای منصور</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>از این دریا حقیقت کل تو داری</p></div>
<div class="m2"><p>نمود عالم و هم دل تو داری</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>از این دریا مرا دل گشت بیهوش</p></div>
<div class="m2"><p>چو کردم عین تحقیق ترا گوش</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>بدانستم یقین کان خواب دیدم</p></div>
<div class="m2"><p>ترا در کشتی اندر آب دیدم</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>تو ما را رهنمائی این زمان زود</p></div>
<div class="m2"><p>که دیدارت مرا دیدار بنمود</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>مرا کن واصل و صورت برانداز</p></div>
<div class="m2"><p>مرا مانند شمعی تو بمگداز</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>مرا کن واصل اندر عین دریا</p></div>
<div class="m2"><p>سر تختم رسان اندر ثرّیا</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>مرا واصل کن و جانم توئی بس</p></div>
<div class="m2"><p>در این غرقاب جان فریاد من رس</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>مرا واصل کن و پرده برافکن</p></div>
<div class="m2"><p>که نور تست در آفاق روشن</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>مرا واصل کن اندر دید دیدار</p></div>
<div class="m2"><p>که دارم از تو کلّی عین اسرار</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>مرا واصل کن و جانم رها کن</p></div>
<div class="m2"><p>مرا کل ابتدا و انتها کن</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>مرا واصل کن و کل وارهانم</p></div>
<div class="m2"><p>که میبینم توئی جان و جهانم</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>مرا از وصل خود یک ذرّه بنمای</p></div>
<div class="m2"><p>چرا اندازیم از جای بر جای</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>مرا از وصل جانان شاد گردان</p></div>
<div class="m2"><p>دل و جانم بکل آبادگردان</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>مرا از وصل جانان رخ نمودی</p></div>
<div class="m2"><p>گره این لحظه از کارم گشودی</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>مرا از وصل خود گردان فنا تو</p></div>
<div class="m2"><p>که تا بینم ز تو عین بقا تو</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>چو بنمودی جمال اندر جمالت</p></div>
<div class="m2"><p>برون آور مرا هان ازوبالت</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>جلالت یافتم طاقت ندارم</p></div>
<div class="m2"><p>تو گوئی این زمان من پایدارم</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>کنون من پایدارم گر بگوئی</p></div>
<div class="m2"><p>ندانم کاین زمان با من چگوئی</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>رهی بگذاشته و استاده اینجا</p></div>
<div class="m2"><p>نمود من در اینجا داده غوغا</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>عیانی در دل و در جان گرفته</p></div>
<div class="m2"><p>حقیقت کفر با ایمان گرفته</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>ز ایمانم ملال آمد بیکبار</p></div>
<div class="m2"><p>شدم کافر حجاب از پیش بردار</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>ز وصلت کافری دارم چگویم</p></div>
<div class="m2"><p>در این میدانِ تو مانند گویم</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>عنان عقل از دستم برون شد</p></div>
<div class="m2"><p>چو دریا این دلم پر موج خون شد</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>عنان عقل از دستم شد ای جان</p></div>
<div class="m2"><p>کنون از دیدن تو مستم ای جان</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>عنان عقل رفت و عشق آمد</p></div>
<div class="m2"><p>مرا کل ازنهاد خویش بستد</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>عیان عشق دیدم از نمودت</p></div>
<div class="m2"><p>یقین من خویش دیدم دید دیدت</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>عیان عشقی و دریای نوری</p></div>
<div class="m2"><p>عجب در عشق اینجاگه صبوری</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>خدایا بیش از این چیزی ندانم</p></div>
<div class="m2"><p>ز بعد صورت و معنی بیانم</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>ندانم جز خدایت آشکاره</p></div>
<div class="m2"><p>گر این مردم کنندم پاره پاره</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>ندانم جز خدایت در همه من</p></div>
<div class="m2"><p>توئی قلب و توئی جان و توئی تن</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>توئی افلاک و انجم در نمودار</p></div>
<div class="m2"><p>توئی بنموده رخ از چرخ دوّار</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>توئی ماه و توئی خورشید جانها</p></div>
<div class="m2"><p>که پیدا میکنی سرّ نهانها</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>توئی عرش و توئی فرش و توئی لوح</p></div>
<div class="m2"><p>که جانها رادهی در عین تن روح</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>توئی عین قلم چون کل نوشتی</p></div>
<div class="m2"><p>نمود جسم را از طین سرشتی</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>توئی کرسی و دائم در خروجی</p></div>
<div class="m2"><p>که در عین همه ذات البروجی</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>توئی عین بهشت و عین ناری</p></div>
<div class="m2"><p>چرا با ما دمی در دم نیاری</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>توئی آتش توئی در جملگی باد</p></div>
<div class="m2"><p>که از تو شد جهانِ عشق آباد</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>توئی آب و توئی دیدار در خاک</p></div>
<div class="m2"><p>نمود صنع خود در عالم پاک</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>توئی هستی در این دریای جوهر</p></div>
<div class="m2"><p>نمودی از نمود هفت اختر</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>توئی کوه و زکان گوهر نمائی</p></div>
<div class="m2"><p>که جان را اندرو رهبر نمائی</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>توئی اصل و نمودِتست دیدار</p></div>
<div class="m2"><p>کنون اسرار کل ما را پدیدار</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>نمودخود نما اینجا بتحقیق</p></div>
<div class="m2"><p>که گفتم از تو بیشک راز توفیق</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>توئی دید بهشت و عین یاری</p></div>
<div class="m2"><p>چرا بابا دمی دردم نیاری</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>جوابم ده که گفتار از تو دارم</p></div>
<div class="m2"><p>نهانم کن که انوار از تو دارم</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>جوابم ده چرا خاموش هستی</p></div>
<div class="m2"><p>توئی دریا منم در عین مستی</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>بیانم کن که اصل واصلانی</p></div>
<div class="m2"><p>مرا برگوی این راز نهانی</p></div></div>