---
title: >-
    بخش ۷۱ - در عتاب کردن حضرت آفریدگار عزّ شأنه با آدم در گندم خوردن و عاجز شدن آدم در گناه و مقرّ شدن و توبه کردن فرماید
---
# بخش ۷۱ - در عتاب کردن حضرت آفریدگار عزّ شأنه با آدم در گندم خوردن و عاجز شدن آدم در گناه و مقرّ شدن و توبه کردن فرماید

<div class="b" id="bn1"><div class="m1"><p>ندا آمد زحضرت ناگهانی</p></div>
<div class="m2"><p>ز من بشنو تو این سرّ معانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که ای آدم نگفتم مر تراهان</p></div>
<div class="m2"><p>بخوردی گندمت آخر بخورهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگو تا گندم از بهر چه خوردی؟</p></div>
<div class="m2"><p>تو فرمان من اینجاگه نبردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز نافرمانیت اکنون چسازم</p></div>
<div class="m2"><p>ترا در آتش غیرت گدازم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسوزانم کنونت در تف نار</p></div>
<div class="m2"><p>ایا آدم همی روزی بصد بار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو هستی بیخبر اکنون ز ذاتم</p></div>
<div class="m2"><p>تو افکندی عیان اینجا صفاتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگو با من کنون و ده جوابم</p></div>
<div class="m2"><p>وگرنه ز آتش غیرت بتابم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسوزانم بیک لحظه دل وجانت</p></div>
<div class="m2"><p>بگو با من کنون اسرارو برهانت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز شرم و خجلت آنجاگاه آدم</p></div>
<div class="m2"><p>بعجزی برگشاد آن لحظه او دم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زبان بگشاد کای دانای اسرار</p></div>
<div class="m2"><p>تو میدانی چگویم من بگفتار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو دانائی من اینجاگه چگویم</p></div>
<div class="m2"><p>ستاده مبتلا و زرد رویم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو دانائی و میدانی ز حالم</p></div>
<div class="m2"><p>که این دم اوفتاده در وبالم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو دانائی و آگاهی ز اسرار</p></div>
<div class="m2"><p>نمییارم زدن دم را بگفتار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گنه کارم فتاده در چَه و گِل</p></div>
<div class="m2"><p>که از قولت نبودم آگه دل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گنهکارم فتاده در بُن چاه</p></div>
<div class="m2"><p>مرا ابلیس گردانید گمراه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مرا ابلیس اینجا وسوسه کرد</p></div>
<div class="m2"><p>بدادم گندم اینجا را ابر خورد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرا ابلیس اینجا رهنمون شد</p></div>
<div class="m2"><p>دلم از خویشتن کلّی برون شد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرا ابلیس کرد اینجا بخواری</p></div>
<div class="m2"><p>نکردم من ز رازت پایداری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرا از ره ببرد و داد گندم</p></div>
<div class="m2"><p>مرا کرد ازوصالت ناگهان گم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مرا از ره ببرد و کرد خوارم</p></div>
<div class="m2"><p>تبه کرد او بهرزه روزگارم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مرا از ره ببرد و داوری ساخت</p></div>
<div class="m2"><p>چو مومم ناگهان در نار بگداخت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مرا از ره ببرد و کرد رسوا</p></div>
<div class="m2"><p>تو میدانی که هستی ذات یکتا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو میدانی کس اسرارت نداند</p></div>
<div class="m2"><p>که آدم اینچنین مسکین بماند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو میدانی و دانائی ترا است</p></div>
<div class="m2"><p>که ذات تو ببود جان بپیوست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کنون تو حاکمی بد کرد آدم</p></div>
<div class="m2"><p>بر این ریش دلش هم نه تومرهم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اگرچه من بدی کردم در اینجا</p></div>
<div class="m2"><p>شدم اندر نمود خویش رسوا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اگرچه من بدی کردم در آخر</p></div>
<div class="m2"><p>توئی دانا توئی اوّل تو آخر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بدی کردم ببخشم رایگان تو</p></div>
<div class="m2"><p>که هستی مر خدای غیب دان تو</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدی کردم در اینجا بد مگیرم</p></div>
<div class="m2"><p>میان این بلا تو دستگیرم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بدی کردم بنفس خویشتن من</p></div>
<div class="m2"><p>شده تاریک چون شب روز روشن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بدی کردم بنفس خود نهانی</p></div>
<div class="m2"><p>فتادم در بلا اکنون تودانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بدی کردم بنفس خود یقین من</p></div>
<div class="m2"><p>نبودم اندر اول پیش بین من</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بدی کردم ندانستم گنهکار</p></div>
<div class="m2"><p>منم، تو عالِمِ سرّی و ستّار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بدی کردم بپوشان سرّم اینجا</p></div>
<div class="m2"><p>که در درگاه تو هستیم رسوا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز رسوائی کنون طاقت ندارم</p></div>
<div class="m2"><p>که سر در حضرت جانان برآرم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز رسوائی مرا طاقت شده طاق</p></div>
<div class="m2"><p>که در درگاه تو ماندم چنین عاق</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز رسوائی که آمد بر سر من</p></div>
<div class="m2"><p>توخواهی بود اینجا رهبر من</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>برسوائی چنین مگذار آدم</p></div>
<div class="m2"><p>که عاجز مانده است او اندر این دم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چنین مگذار آدم را تو حیران</p></div>
<div class="m2"><p>بفضل خود تو او را شاد گردان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چنین مگذار آدم را چنین خوار</p></div>
<div class="m2"><p>بپوشان سرّ او دانای ستّار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چنین مگذار آدم را دلش خون</p></div>
<div class="m2"><p>بمانده در بهشتت زار و محزون</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چنین مگذارم و تو دستگیرم</p></div>
<div class="m2"><p>که کس نبود به جز تو دستگیرم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چنین مگذار اینجا مبتلا باز</p></div>
<div class="m2"><p>چنینم در بلای عشق مگداز</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دلم خون شد در این رسوائی خود</p></div>
<div class="m2"><p>که میدانم که بد کردم همین بد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بدیّ من ببخش و درگذارم</p></div>
<div class="m2"><p>که هستی در دو عالم کردگارم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو شیطانم بدی کرد و بدی ساخت</p></div>
<div class="m2"><p>چنین بازی مرا اینجا بپرداخت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چنین بازیچه دادم در بهشتم</p></div>
<div class="m2"><p>چو یاد تو من از خاطر بهشتم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چنین بازیچه دادم بیخود اینجا</p></div>
<div class="m2"><p>ابا من کرد شیطان مر بد اینجا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو شیطان بود اینجا همچو دشمن</p></div>
<div class="m2"><p>چنین کرد اودر اینجایم ابامن</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ولیکن من زخود دیدم ز شیطان</p></div>
<div class="m2"><p>توئی ستّار و هم غفار و رحمان</p></div></div>