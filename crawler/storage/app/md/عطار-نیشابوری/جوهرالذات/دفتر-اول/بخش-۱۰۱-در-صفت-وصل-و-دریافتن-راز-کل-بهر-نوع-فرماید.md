---
title: >-
    بخش ۱۰۱ - در صفت وصل و دریافتن راز کل بهر نوع فرماید
---
# بخش ۱۰۱ - در صفت وصل و دریافتن راز کل بهر نوع فرماید

<div class="b" id="bn1"><div class="m1"><p>تو او باشی و او تو من چگویم</p></div>
<div class="m2"><p>بجز درمان دردت می چه جویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوشا آن دم که پرده بفکند یار</p></div>
<div class="m2"><p>ز پنهانی نماید عین دیدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوشا آن دم که جان و تن نماند</p></div>
<div class="m2"><p>بجز حق هیچ ما و من نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوشا آن دم که بینی روی جانان</p></div>
<div class="m2"><p>تو باشی در یکی هم سوی جانان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خطاب آمد درآن دم خود بخود او</p></div>
<div class="m2"><p>شده فارغ ز گفت و نیک و بد او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که بنده این زمان شاهی تو بنگر</p></div>
<div class="m2"><p>نمودم در همه ماهی تو بنگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بمن قائم شدی میباش قائم</p></div>
<div class="m2"><p>که من هم با تو خواهم بود دائم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من از آنِ توام تو آنِ مائی</p></div>
<div class="m2"><p>زهی عین خطاب رب خدائی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نداند نفس این سرّ پی ببردن</p></div>
<div class="m2"><p>بجز حسرت در اینجاگاه خوردن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نداند این بیان جز حق شناسی</p></div>
<div class="m2"><p>خطا داند بیانم ناسپاسی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیانم از شریعت باز دان تو</p></div>
<div class="m2"><p>هواللّه قُل و آنگه رازدان تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکی خواهی بُدن در آخر کار</p></div>
<div class="m2"><p>بماند نقطه اندر عین پرگار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه این راز میگویند و جویند</p></div>
<div class="m2"><p>کسانی کاندر این دم راز جویند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر آن کو پی برد در سرّ عطّار</p></div>
<div class="m2"><p>ببیند همچو او اینجا رخ یار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زمانی گر نه صاحب درد باشد</p></div>
<div class="m2"><p>زنی باشد نه مرد مرد باشد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدرد این راز بتوانی تو دیدن</p></div>
<div class="m2"><p>ز خود بگذشتن اینجاگه رسیدن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدرد این شرح اینجا راست آید</p></div>
<div class="m2"><p>درت اینجا بکلّی برگشاید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بدرد این یاب و سوی درد بشتاب</p></div>
<div class="m2"><p>نمود دوست هم از دوست دریاب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بدرد این راست آید چند جوئی</p></div>
<div class="m2"><p>بیفکن صورت و بنگر تو اوئی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بدرد این درد واکن هان و مِی نوش</p></div>
<div class="m2"><p>ولی مانندهٔ منصور مخروش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بدرد این درد مردان را در آشام</p></div>
<div class="m2"><p>غلط گفتم بر افکن ننگ با نام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که صاحب درد راز دوست دیدست</p></div>
<div class="m2"><p>حقیقت مغز اندر پوست دیدست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ولیکن مغز کی چون پوست باشد</p></div>
<div class="m2"><p>اگرچه پوست هم از دوست باشد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو اینجا پوست بگذار و یقین پوست</p></div>
<div class="m2"><p>که چون شد پوست محو اندر یقین اوست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تو مغزی و طلب کن مغز جانت</p></div>
<div class="m2"><p>که ازجان بنگری راز نهانت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو مغزی پوست همراه تو آمد</p></div>
<div class="m2"><p>چو دامی بند این راه تو آمد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چرا در بند دام اینجا بماندی</p></div>
<div class="m2"><p>دلِ سرگشتهٔ مانندهٔ گوی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سخن تا چند گوئی ای دل مست</p></div>
<div class="m2"><p>کنون چون دیده با دیدار پیوست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>رها کن ترک نام و ننگ برگوی</p></div>
<div class="m2"><p>چرا سرگشتهٔ مانندهٔ گوی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>رها کن نام و ننگ و زهد و طامات</p></div>
<div class="m2"><p>دو روزی روی نِه سوی خرابات</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خراباتی شو و منصور واری</p></div>
<div class="m2"><p>اناالحق زن در این خمخانه باری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گرو کن طیلسان درکوی خمّار</p></div>
<div class="m2"><p>زمانی سر نه اندر کوی خمّار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نظر کن اندر اینجا دُرد نوشان</p></div>
<div class="m2"><p>که از دُردی شده مست و خموشان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از آن دُردی که مردان نوش کردند</p></div>
<div class="m2"><p>ولی چون حلقهٔ درگوش کردند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از آن دُردی که بوئی یافت منصور</p></div>
<div class="m2"><p>بگفتا کل منم نور علی نور</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از آن دُردی در آشامید حق گفت</p></div>
<div class="m2"><p>چو خود حق دیدهم حق بود حق گفت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از آن دُردی که قوت عاشقانست</p></div>
<div class="m2"><p>بده ساقی که این شرح و بیانست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از آن دردی مرا ده زود یک جام</p></div>
<div class="m2"><p>که بگذشتم هم از آغاز و انجام</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مرا ده دردی زان خمّ وحدت</p></div>
<div class="m2"><p>که تا بگذارم اینجا عین کثرت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مرا ده دردئی ز آن خم زمانی</p></div>
<div class="m2"><p>مرا ازخویشتن کن گُم نشانی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مرا ده دردی و بستان و در جان</p></div>
<div class="m2"><p>از این بیشم دگر جانا مرنجان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مرا جامی بده هان زود ساقی</p></div>
<div class="m2"><p>زنام و ننگ برهان زود ساقی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مرا جامی بده تا جانفشانم</p></div>
<div class="m2"><p>غباری بر سر میدان فشانم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چه جای دل که جان سیصد هزاران</p></div>
<div class="m2"><p>بود جانم فدای رویت ای جان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چه باشد جان که در خورد تو باشد</p></div>
<div class="m2"><p>بود درمان که در درد تو باشد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مرا دردیست جامی کن دوایش</p></div>
<div class="m2"><p>ز جامی کن مرا مست لقایش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دوا کن دردم ای درمان جانها</p></div>
<div class="m2"><p>که از دردست این شرح و بیانها</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>دوا کن دردمند خود دوا کن</p></div>
<div class="m2"><p>بجامی حاجت جانم روا کن</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>دوا کن ای دوای دردمندان</p></div>
<div class="m2"><p>مرا زین سجن غم آزاد گردان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دوا کن ای بتو روشن دل من</p></div>
<div class="m2"><p>توئی اندر زمانه حاصل من</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>دوا کن ای تو بود اولیّنم</p></div>
<div class="m2"><p>دوا کن بی نهان آخرینم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دوا کن دل که دل داغ تو دارد</p></div>
<div class="m2"><p>بهر زه روزگاری میگذارد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>دوا کن دل که دل محبوس ماندست</p></div>
<div class="m2"><p>درش اینجایگه مدروس ماندست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>دوا کن این دل بیچاره مانده</p></div>
<div class="m2"><p>بسان ناکسی آواره مانده</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>دوا کن این دل مجروح افگار</p></div>
<div class="m2"><p>که در دام غمت ماندست گرفتار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>دوا کن این دل حیران شده مست</p></div>
<div class="m2"><p>که تا یک دم وصال او را دهد دست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>دوا کن این دل افتاده در دام</p></div>
<div class="m2"><p>مگر بیند رخ خوبت سرانجام</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>دوا کن این دل آتش رسیده</p></div>
<div class="m2"><p>که شد در آتش عشقت کفیده</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>دوا کن این دل از غم کبابم</p></div>
<div class="m2"><p>تو دستم گیر کز سر رفت آبم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>شفائی بخش اینجا عاشقانت</p></div>
<div class="m2"><p>بکن پیدا بکل راز نهانت</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چو دردم از تو و درمانم ازتست</p></div>
<div class="m2"><p>چو جسمم از تو و هم جانم از تست</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>حقیقت جسم و جان هر دو تو داری</p></div>
<div class="m2"><p>چه باشد گر سوی من رحمت آری</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ندارم عقل و هوشم شد بیکبار</p></div>
<div class="m2"><p>حجاب من منم از پیش بردار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چنان در قید صورت شد گرفتار</p></div>
<div class="m2"><p>که اینجا باز ماند از دیدن یار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>از آن دم میزنی بر جمله ذرّات</p></div>
<div class="m2"><p>که دام داری عیان از نفخهٔ ذات</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>از آن دم میزنی ای راز دیده</p></div>
<div class="m2"><p>که این دم زان دم کل باز دیده</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>دمی زن حق درون خود نظر کن</p></div>
<div class="m2"><p>دگر ذرّات از این دمها خبر کن</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>خبر کن جملهٔ ذرّات از این دم</p></div>
<div class="m2"><p>که میگوید بیانت حق دمادم</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>خبر کن جملهٔ ذرّات از این راز</p></div>
<div class="m2"><p>که سوی آن دم اینجاگه شوند باز</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>خبر کن جملهٔ ذرات بس حق</p></div>
<div class="m2"><p>اناالحق زن چوهستی نور مطلق</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>دم عطّار بیرون ازمکانست</p></div>
<div class="m2"><p>حقیقت دید عین لامکانست</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>دم عطّار زد اینجا اناالحق</p></div>
<div class="m2"><p>بگفت او در حقیقت راز مطلق</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>دم عطار بیشک دید دیدست</p></div>
<div class="m2"><p>خدا دان تو که در گفت وشنیدست</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>دم عطّار زد اینجای سر باز</p></div>
<div class="m2"><p>از آن شد آخر او هم جان و سرباز</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>دم عطّار منصورست بردار</p></div>
<div class="m2"><p>اناالحق میزند بهر نمودار</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بیک ره پرده از رو برگرفتست</p></div>
<div class="m2"><p>از آن از دوست پاسخ در گرفتست</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>یقین دارد از آن او بی گمان شد</p></div>
<div class="m2"><p>صور بگذاشت تا کل جان جان شد</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>همه معنی یکی گفت و یکی شد</p></div>
<div class="m2"><p>حققت ذات معنی بیشکی شد</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>نداند مبتدی اسرار عطّار</p></div>
<div class="m2"><p>مگر صاحبدلی هم صاحب اسرار</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>که بردارد گمان از پیش خود او</p></div>
<div class="m2"><p>یکی بیند چه هم نیک و چه بد او</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>جمال یارش اینجا آشکاره</p></div>
<div class="m2"><p>همه سوی جمال او نظاره</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>همه دیدار او دیدند یکسر</p></div>
<div class="m2"><p>ولیکن عقل کی دارد میّسر</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>که جانانست جمله عشق داند</p></div>
<div class="m2"><p>که این دُرهای پُر معنی فشاند</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>بیان من نه از عقلست اینجا</p></div>
<div class="m2"><p>ز عشق آمد نه از عقلست اینجا</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>کسی کو عقل را بشناخت جانست</p></div>
<div class="m2"><p>مر او را عشق کل عین العیانست</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>نگوید راز تقلیدی ابر گوی</p></div>
<div class="m2"><p>که سرگردان شدست از گفت و ز گوی</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>حقیقت زو که ازتقلید گوید</p></div>
<div class="m2"><p>سخن کی از عیان دید گوید</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>حقیقت زو که خود رادوست دارد</p></div>
<div class="m2"><p>نه مغز است او که کلّی پوست دارد</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>حققت زو که جانان بیند اینجا</p></div>
<div class="m2"><p>مر آن خورشید رخشان بیند اینجا</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>یکی بیند دوئی را محو کرده</p></div>
<div class="m2"><p>بگوید او سخن از هفت پرده</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>یکی را در یکی گوید بیانش</p></div>
<div class="m2"><p>نماید راز ذات جان جانش</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>چو اصل و فرع بیند در یکی گُم</p></div>
<div class="m2"><p>شده او در یکی، یک در یکی گُم</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>بود واصل در اینجا بی طبیعت</p></div>
<div class="m2"><p>یکی را دیده در عین شریعت</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>اگرچه آخر از اوّل خبردار</p></div>
<div class="m2"><p>شود اینجایگه در دیدن یار</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>مر او را این بیان گردد میسّر</p></div>
<div class="m2"><p>اگر آخر ببازد همچو من سر</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>فنا را در بقا بنموده باشد</p></div>
<div class="m2"><p>گره ازکار خود بگشوده باشد</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>مشایخ جمله خود را دوست دارند</p></div>
<div class="m2"><p>حقیقت مغز جان هم پوست دارند</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>همه دم میزنند از سرّ اسرار</p></div>
<div class="m2"><p>شده چندی از آن حضرت خبردار</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>دم حق میزنند وحق پرستند</p></div>
<div class="m2"><p>اگرچه در معانی نیست هستند</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>ولیکن فرق این بسیار باشد</p></div>
<div class="m2"><p>که چون منصور دیگریار باشد</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>مشایخ گرچه اوّل بود بسیار</p></div>
<div class="m2"><p>دلی چون بایزید آمد پدیدار</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>جنید و شبلی معروف آمد</p></div>
<div class="m2"><p>ولی منصور از این معروف آمد</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>همه این دم زدند امّا نهانی</p></div>
<div class="m2"><p>ولی منصور آمد در عیانی</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>همه این دم زدند این راز گفتند</p></div>
<div class="m2"><p>درون خلوت ایشان راز گفتند</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>عوام النّاس چندی واصلانند</p></div>
<div class="m2"><p>اگرچه صورت بیحاصلانند</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>همی گویند چندی آشکارا</p></div>
<div class="m2"><p>ولیکن جز خموشی نیست ما را</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>چو از تقلید گویند این سخن باز</p></div>
<div class="m2"><p>ولی کی باشد اینجا صاحب راز</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>که بیشک جسم و جان اینجا ببازند</p></div>
<div class="m2"><p>در آن حضرت پس آنگه سرفرازند</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>در آن حضرت چه خاص است و چه مر عام</p></div>
<div class="m2"><p>در آن قربت چه قهرست و چه انعام</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>ولکین این بیان مر صاحب راز</p></div>
<div class="m2"><p>سزد اینجا که گوید نی جز آغاز</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>نمودی کان ز جمله خلق پنهانست</p></div>
<div class="m2"><p>کسی شاید که گوید از دل و جانست</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>کسی شاید که این اسرار گوید</p></div>
<div class="m2"><p>که او را دیده و دیدار گوید</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>از آن حضرت بود کلّی خبردار</p></div>
<div class="m2"><p>نبیند هیچ غیری جز رخ یار</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>از آن حضرت کسی کو آگهی یافت</p></div>
<div class="m2"><p>چو ذرّه سوی آن خورشید بشتافت</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>از آن حضرت کسی کو دید چون من</p></div>
<div class="m2"><p>یکی شد در درون و در برون من</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>از آنی بی خبر ای دل ندانی</p></div>
<div class="m2"><p>که در عین بقا اندر گمانی</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>از آنی بیخبر ای دل بمانده</p></div>
<div class="m2"><p>که هستی دست از خود برفشانده</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>دمی خاموشی و دیگر سخن گوی</p></div>
<div class="m2"><p>اگر تو بردهٔ اندر سخن گوی</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>دمی در عین دیدار خدائی</p></div>
<div class="m2"><p>دمی از جسم و جان کلّی جدائی</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>همه با هم یکی دان همچو اوّل</p></div>
<div class="m2"><p>که تا آخر نگردی تو معطّل</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>چو اصلت هست فرع تو هم اصلست</p></div>
<div class="m2"><p>گذشته فرقت دیدار وصلست</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>گمان رفتست و کل عین الیقین است</p></div>
<div class="m2"><p>ترا جانان نموده رخ چنین است</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>گمان رفتست و دیدارت نموده</p></div>
<div class="m2"><p>ترا هر لحظه صد معنی فزوده</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>گمان رفتست و دیدارست اینجا</p></div>
<div class="m2"><p>حقیقت جان تو یارست اینجا</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>گمان رفتست و گفتارت یقین شد</p></div>
<div class="m2"><p>نمودت اوّلین و آخرین شد</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>گمان رفتست و دل بر جای هم نه</p></div>
<div class="m2"><p>در این معنی ترا شادی و غم نه</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>گمان رفتست اکنون در یقین باش</p></div>
<div class="m2"><p>چو منصور از اناالحق جمله این باش</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>چو منصور از اناالحق رازها گوی</p></div>
<div class="m2"><p>یکی آواز در آوازها گوی</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>چو منصور از اناالحق گرد نقاش</p></div>
<div class="m2"><p>بگو با جملهٔ ذرّاتها فاش</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>چو منصور از حقیقت گو اناالحق</p></div>
<div class="m2"><p>بهر هستی بنه این راز مطلق</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>که بد عطّار بیشک راز اللّه</p></div>
<div class="m2"><p>اناالحق زد ز سرّ قل هو اللّه</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>نبُد عطّار بیشک بود او حق</p></div>
<div class="m2"><p>بدو برگفت اینجا راز مطلق</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>همه گفتار عطّارست بیچون</p></div>
<div class="m2"><p>که میگوید اناالحق بیچه و چون</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>همه گفتار عطّارست از آن دید</p></div>
<div class="m2"><p>از آن بگذاشت گفت و دید تقلید</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>گذشت او بیشک ازتقلید اینجا</p></div>
<div class="m2"><p>چویار خویشتن را دید اینجا</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>چویار خویشتن اینجایگه یافت</p></div>
<div class="m2"><p>میان عاشقان این پایگه یافت</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>چو یار خویشتن اینجا بدید او</p></div>
<div class="m2"><p>ز دید خویش گشتش ناپدید او</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>چو یار خویشتن دید و فنا شد</p></div>
<div class="m2"><p>چو اوّل زانکه اوّل در فنا شد</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>فنا شد اوّل و آخر فنایست</p></div>
<div class="m2"><p>فنا نزدیک در عین بقایست</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>چو اوّل شد فنا از بود خود او</p></div>
<div class="m2"><p>که دیدستش یقین معبود خود او</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>چو اوّل شد فنا در دید فطرت</p></div>
<div class="m2"><p>از اینجاگه ورا بخشید قربت</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>چو اوّل شد فنا آخر بقا دید</p></div>
<div class="m2"><p>عیان انبیاء و اولیا دید</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>چو اوّل شد فنای بود جمله</p></div>
<div class="m2"><p>بود در آخر او معبود جمله</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>چو اوّل شد فنا و گفت او راز</p></div>
<div class="m2"><p>چو او گر میتوانی خود برانداز</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>فنا عین بقای جاودانی است</p></div>
<div class="m2"><p>فنا بنگر که آن راز نهانی است</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>همه اینجا فنا بُد اوّل کار</p></div>
<div class="m2"><p>نمودار نمود و عین پرگار</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>پدیدار آمد و دیگر فنا شد</p></div>
<div class="m2"><p>نمیگویم که از اوّل فنا شد</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>فنا لا دان و الّااللّه بنگر</p></div>
<div class="m2"><p>دو عالم بود الّا اللّه بنگر</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>فنا دانم که الّا هست باقی</p></div>
<div class="m2"><p>بخور جام فنا از دست ساقی</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>چو جانت هست شد از بود آن ذات</p></div>
<div class="m2"><p>فنا گردان نمود جمله ذرات</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>اگر سوی یقین آری گمان تو</p></div>
<div class="m2"><p>نیابی هرگز اینجا جان جان تو</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>یقین را سوی خود ده راه بنگر</p></div>
<div class="m2"><p>برافکن پردهٔ آن ماه و بنگر</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>یقین بنمایدت دیدار جانان</p></div>
<div class="m2"><p>بگوید با تو کل اسرار جانان</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>هر آن کو با یقین همراز باشد</p></div>
<div class="m2"><p>دوعالم بر دلش در باز باشد</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>هر آن کو با یقین باشد زمانی</p></div>
<div class="m2"><p>جمال یار خود بیند عیانی</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>یقین بشناس اگر تو راز بینی</p></div>
<div class="m2"><p>که بیشک تو عیان کل بازبینی</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>حقیقت بودتست از بود اللّه</p></div>
<div class="m2"><p>تو داری در عیانت قل هواللّه</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>تو داری رفعت لولاک اینجا</p></div>
<div class="m2"><p>چرامانی بآب و خاک اینجا</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>بزن کوس معانی همچو عطّار</p></div>
<div class="m2"><p>برافکن آب و خاک و باز بین نار</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>زهی عطّار کز بحر حقیقت</p></div>
<div class="m2"><p>فشاندستی تو درهای شریعت</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>محمّد ﷺهست در جانت یداللّه</p></div>
<div class="m2"><p>از آن پیدا بیانت قل هواللّه</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>ز دید حق بسی اسرار داری</p></div>
<div class="m2"><p>هزاران نافهٔ تاتاری داری</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>پر از عطرست عالم ازدم تو</p></div>
<div class="m2"><p>چو حق آمد حقیقت همدم تو</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>از این درها که هر دم برفشاندی</p></div>
<div class="m2"><p>حقیقت بر سر رهبر فشاندی</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>تمامت سالکانت دوست دارند</p></div>
<div class="m2"><p>تمامت واصلان ازجانت یارند</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>توئی واصل دهد این دور زمانه</p></div>
<div class="m2"><p>زدی تیر مرادت بر نشانه</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>کمال معنی و بازوی تقوی</p></div>
<div class="m2"><p>تو داری میزنی این تیر معنی</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>چنانی گرم رو اندر ره یار</p></div>
<div class="m2"><p>که در ره میفشانی درّ اسرار</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>حقیقت وصل جانان یافتی باز</p></div>
<div class="m2"><p>بسوی قرب او بشتافتی باز</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>چنان دید حقیقی روی بنمود</p></div>
<div class="m2"><p>رخ دلدار از هر سوی بنمود</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>که شک بُد اوّل کارت یقین است</p></div>
<div class="m2"><p>ترا چشم دل اینجا دوست بین است</p></div></div>