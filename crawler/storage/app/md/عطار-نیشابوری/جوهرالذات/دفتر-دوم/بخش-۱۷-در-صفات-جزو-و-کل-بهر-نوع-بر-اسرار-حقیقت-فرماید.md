---
title: >-
    بخش ۱۷ - در صفات جزو و کل بهر نوع بر اسرار حقیقت فرماید
---
# بخش ۱۷ - در صفات جزو و کل بهر نوع بر اسرار حقیقت فرماید

<div class="b" id="bn1"><div class="m1"><p>وجودت در صفات نور گردانست</p></div>
<div class="m2"><p>اگر یابی حقیقت جان جانانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صفات هرچه یابی اندر اینجا</p></div>
<div class="m2"><p>ز تقوی جمله را بینی مصفّا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صفات جسم یابی قوّت دل</p></div>
<div class="m2"><p>مراد دل شده از وی بحاصل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صفات جان نظر کن معنی ذات</p></div>
<div class="m2"><p>فتاده نور او بر جمله ذرّات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صفات جمله اشیا بر تو پیداست</p></div>
<div class="m2"><p>حقیقت ذات در جانت هویداست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صفات آفتاب روح میبین</p></div>
<div class="m2"><p>که آغازت از این بُد در نخستین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صفات ماه بنگر در درونت</p></div>
<div class="m2"><p>که نور اوست در جان رهنمونت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صفات مشتری بنگر ز باطن</p></div>
<div class="m2"><p>اگر مرد رهی بگذر ز باطن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صفات زهره در شمس و قمر بین</p></div>
<div class="m2"><p>حقیقت کوکبان را سر بسر بین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صفات جملگی اندر تو موجود</p></div>
<div class="m2"><p>بود بیشک توئی دیدار معبود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صفات هرچه یابی سوی افلاک</p></div>
<div class="m2"><p>همه پیداست در تو خفته برخاک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صفات روح را در دل نظر کن</p></div>
<div class="m2"><p>در او پیدا حقیقت سر بسر کن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صفات خویش را اندر قلم بین</p></div>
<div class="m2"><p>وجودت بیشکی عین عدم بین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صفات عرش در جانست و در دل</p></div>
<div class="m2"><p>شده نورش یقین در جمله حاصل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صفات عرش جسمست تا بدانی</p></div>
<div class="m2"><p>در او پیدا همه راز معانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صفات جنّت و حوران یقین بین</p></div>
<div class="m2"><p>صفات دوزخ از بین القرین بین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صفات آتش از نورست بنگر</p></div>
<div class="m2"><p>مر این سر جمله مشهورست بنگر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صفات باد موجود است در تو</p></div>
<div class="m2"><p>یقینِ روحِ معبود است در تو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صفات آب بنگر در رگ و پوست</p></div>
<div class="m2"><p>گرفته در درونت توی بر توست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>صفات خاک چون پیداست در تن</p></div>
<div class="m2"><p>که خود در خاک داری عین مسکن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>صفات کوه بنگر جسم خود بین</p></div>
<div class="m2"><p>مشو ای دوست اندر راز خود بین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>صفات بحر بنگر در درونت</p></div>
<div class="m2"><p>صدف در جوهر اینجا رهنمونت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صفات این همه چون یافتی باز</p></div>
<div class="m2"><p>حجابست این همه از خود برانداز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو در خلوت ندانست او که چونست</p></div>
<div class="m2"><p>حقیقت عشق بین کو رهنمونست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو در خلوت ندیدی راز جانان</p></div>
<div class="m2"><p>توئی انجام با آغاز جانان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در این خلوتسرای جان و دل خود</p></div>
<div class="m2"><p>فنا شو تا بیابی حاصل خود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در این خلوتگه جانان که هستی</p></div>
<div class="m2"><p>بت نفس و هوا بشکن که رستی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مصفّی کن دل و جان همچو وی نیز</p></div>
<div class="m2"><p>نظر که جمله را ارواح و حَی نیز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نظر کن در دل و دریاب بیچون</p></div>
<div class="m2"><p>درون را با برون بگرفته در خون</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در آن خونست بیشک جوهر دوست</p></div>
<div class="m2"><p>نموده نور خود در مغز و در پوست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>صفای دل بهست از نور خورشید</p></div>
<div class="m2"><p>که خواهد بود مر این نور جاوید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>صفای دل طلب کن از معانی</p></div>
<div class="m2"><p>اگرچه قدر این جوهر ندانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو حِصن قلب دیدی سوی جان شو</p></div>
<div class="m2"><p>ندای سرّ ربانی تو بشنو</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وصال دل طلب کن در درون تو</p></div>
<div class="m2"><p>که جان خواهد شدن این رهنمون تو</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>حقیقت جان شناس و یار بنگر</p></div>
<div class="m2"><p>که از جان باز یابی سرّ اکبر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>توئی در خلوت دل بازمانده</p></div>
<div class="m2"><p>ندیده راز در دل باز مانده</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در این خلوت اگر کژ بین شوی تو</p></div>
<div class="m2"><p>نیابی مر چنین سرّ قویتو</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ایا سالک که داری خلوت دل</p></div>
<div class="m2"><p>چه کردی عاقبت اینجای حاصل</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ایا سالک که داری خلوت جان</p></div>
<div class="m2"><p>وجود خویشتن دیگر مرنجان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>فنا شو تا بقا آید به پیشت</p></div>
<div class="m2"><p>چرا تو ماندهٔ در کفر و کیشت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>فنا شو تا بقا یابی سراسر</p></div>
<div class="m2"><p>اگر مرد رهی از خویش برخَور</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز خود آسوده شو بی رنج مر کس</p></div>
<div class="m2"><p>در اینجا یک زمان فریاد خود رس</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز خود آسوده شو ای مرد درویش</p></div>
<div class="m2"><p>حقیقت مرهمی نه بر دل ریش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز خود آسوده شو در کل احوال</p></div>
<div class="m2"><p>رها کن زهد با سالوس افعال</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز خود آسوده شو تا کام یابی</p></div>
<div class="m2"><p>رها کن ننگ تا مر نام یابی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ز خود آسوده شو اندر فنا کوش</p></div>
<div class="m2"><p>مگو بسیار در جان باش خاموش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز خود آسوده شو بیرنج اینجا</p></div>
<div class="m2"><p>نظر کن بیشکی مر گنج اینجا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز خود آسوده شو وز نار برخَور</p></div>
<div class="m2"><p>که داری هم تو ماه و هم تو اختر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز خود آسوده شو مانند مردان</p></div>
<div class="m2"><p>خود از این رنج تن آزاد گردان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تو خود آسوده شو ای راز دیده</p></div>
<div class="m2"><p>که گم کردی دل اندر باز دیده</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بجز جانان مبین در پردهٔ دل</p></div>
<div class="m2"><p>که او بُد بیشکی گم کردهٔ دل</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>در این منزل چو تو با جسم گردی</p></div>
<div class="m2"><p>دوئی برداری آنگاهی تو فردی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>در این منزل یکی بین و یکی شو</p></div>
<div class="m2"><p>مر این گفتار از یکّی تو بشنو</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>همه مردانِ ره اندر برِ تست</p></div>
<div class="m2"><p>ندانی اینکه عشقت رهبرِ تست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>رموز این بیان نز عقل و تقلید</p></div>
<div class="m2"><p>که این معنی بچشم سِر توان دید</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>توانی دید این معنی تو از جان</p></div>
<div class="m2"><p>بصورت مینیاید راست این دان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بمعنی هر که اینجا رازِ ره برد</p></div>
<div class="m2"><p>رهِ معنی ز عشق دوست بسپرد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بخلوتگاهِ جان بنشست اوّل</p></div>
<div class="m2"><p>که تا شد جسم با جانت مبدّل</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چو جسمت جان شد و جان راهبرشد</p></div>
<div class="m2"><p>حقیقت جسم بی خوف و خطر شد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ترا تا خوف در جانست پیدا</p></div>
<div class="m2"><p>توئی همچون یکی دیوانه شیدا</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>برسوائی در افتی آخر کار</p></div>
<div class="m2"><p>مر این گفته که من گفتم نگهدار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بخود این سر ندانی تا بدانی</p></div>
<div class="m2"><p>یقین بشناس در سرّ معانی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>یقین از پیش دار ای دل در اینجا</p></div>
<div class="m2"><p>بکن مقصود خود حاصل در اینجا</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>یقین را پیش دار و راه او کن</p></div>
<div class="m2"><p>همه ذرّات او درگاه او کن</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بگو با جملهٔ ذرّات این راز</p></div>
<div class="m2"><p>نماشان جملگی انجام و آغاز</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>صفات خویشتن کن ذات اوّل</p></div>
<div class="m2"><p>مر این صورت در اینجا کن مبدّل</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>تو باشی لیکن اینجا تو نباشی</p></div>
<div class="m2"><p>چو تو بی‌تو شدی کلّی تو باشی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>تو باشی اوّل و آخر نگهدار</p></div>
<div class="m2"><p>مر این معنی تو از ظاهر نگهدار</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>تو باشی هرچه بینی در صفاتت</p></div>
<div class="m2"><p>بیان میگویم از اسرار ذاتت</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>تو باشی آن زمان در عین خلوت</p></div>
<div class="m2"><p>حقیقت هرچه آید عین قربت</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>تو باشی جملهٔ پنهان و پیدا</p></div>
<div class="m2"><p>همه در تو تو اندر کل هویدا</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>تو باشی آفتاب و ماه بیشک</p></div>
<div class="m2"><p>نموده نور تو در جملگی یک</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>تو باشی مشتری و زهره ای پیر</p></div>
<div class="m2"><p>بیان را گوش کن ای سالک پیر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>تو باشی عرش و فرش و لوح و کرسی</p></div>
<div class="m2"><p>حقیقت دان و دیگر می چه پرسی</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>از این معنی اگر اسرار جوئی</p></div>
<div class="m2"><p>نکردستی تو گم دلدار جوئی</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>شنو دلدار دلدارست دل‌دار</p></div>
<div class="m2"><p>ازین بیهوشی و مستی خبردار</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>زهی نادان زخود بیرون فتادی</p></div>
<div class="m2"><p>از آن افتاده چون مجنون فتادی</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>تو مجنونی و لیلی در بر تست</p></div>
<div class="m2"><p>حقیقت اندر اینجا رهبر تست</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>تو مجنونی و لیلی رخ نمودست</p></div>
<div class="m2"><p>ترا آدم بدم پاسخ نمودست</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>تو مجنونی کنون از شوق لیلی</p></div>
<div class="m2"><p>همی یابی در اینجا ذوق لیلی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>تو مجنونی و لیلی در درونت</p></div>
<div class="m2"><p>ویت در سوی خود چون رهنمونت</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>تو مجنونی و لیلی باز دیده</p></div>
<div class="m2"><p>یقین بگشای ای شهباز دیده</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>تو مجنونی و لیلی حاضر تست</p></div>
<div class="m2"><p>گمان بر بیشکی او ناظر تست</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>تو مجنونی و لیلی باز بین هان</p></div>
<div class="m2"><p>درون چسم و جان می راز بین هان</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>تو مجنونی و غم بسیار دیده</p></div>
<div class="m2"><p>وصالی جز غم جانان ندیده</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>ترا لیلی است پیدا و تو پنهان</p></div>
<div class="m2"><p>بهرزه میدهی از عشق او جان</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>ترا لیلی درون جان شیرین</p></div>
<div class="m2"><p>تو داده از فراقش جان شیرین</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>ترا لیلی درون و بنگر اسرار</p></div>
<div class="m2"><p>تو را بیرون شده لیلی طلبکار</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>جمال خوبِ لیلی در درونست</p></div>
<div class="m2"><p>چگویم من که این معنی چگونست</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>جمال خوبِ لیلی آفتابست</p></div>
<div class="m2"><p>دل مجنون از آن در تک و تابست</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>جمال خوب لیلی هست بیچون</p></div>
<div class="m2"><p>فکنده نور خود بر هفت گردون</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>تو لیلی را ندیدستی دل مست</p></div>
<div class="m2"><p>از آن مجنون‌صفت رفتی تو از دست</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>یقین دیوانه‌ای از عشق لیلی</p></div>
<div class="m2"><p>زیادت میکنی هر لحظه میلی</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>یقین دیوانه‌ای و تو ندانی</p></div>
<div class="m2"><p>نشاید کاینچنین دیوانه مانی</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>در این دیوانگی ای دل چه دیدی</p></div>
<div class="m2"><p>دمی در صحبت لیلی رسیدی؟</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>ندیدی روی لیلی ای دل ریش</p></div>
<div class="m2"><p>حجاب آورده اینجاگه تو در پیش</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>نمیداند مگر لیلی در اینجا</p></div>
<div class="m2"><p>که مجنون میکند هر لحظه غوغا</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>نمیداند مگر لیلی در این راز</p></div>
<div class="m2"><p>که خواهد گشت مجنون جان و سر باز</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>نمیداند مگر لیلی که مجنون</p></div>
<div class="m2"><p>فتادست این زمان اندر گوِ خون</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>چنان مجنون اسیر و مستمندست</p></div>
<div class="m2"><p>بدست خود سر خود را فکندست</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>چنان مجنون فتاده در دل خونست</p></div>
<div class="m2"><p>که اندر وی نظاره هفت گردونست</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>عجب لیلی درون جان بمانده</p></div>
<div class="m2"><p>میان خاک ره در خون بمانده</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>ابا لیلی و لیلی گشته مجنون</p></div>
<div class="m2"><p>که میداند که این اسرار خود چون</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>چنان عطّار مجنون وصالست</p></div>
<div class="m2"><p>که گوئی در غم و رنج و وبالست</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>دمادم میشود دیوانهٔ عشق</p></div>
<div class="m2"><p>همی گوید یقین افسانهٔ عشق</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>چو لیلی دارد اندر بر چگوید</p></div>
<div class="m2"><p>نکرده هیچ گم دیگر چه جوید</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>چو لیلی با منست و راز دیدم</p></div>
<div class="m2"><p>حقیقت روی لیلی باز دیدم</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>مر این دیوانگی از عشق محبوب</p></div>
<div class="m2"><p>مرا باشد که پیدا گشت مطلوب</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>حقیقت طالبم مطلوب آمد</p></div>
<div class="m2"><p>وز اینجا یوسفم یعقوب آمد</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>منم مجنون منم لیلی در اینجا</p></div>
<div class="m2"><p>منم یعقوب و یوسف در هویدا</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>وصال لیلی‌ام حاصل شده کل</p></div>
<div class="m2"><p>چو یوسف جان من واصل شده کل</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>چنان دیدم جمال روی جانان</p></div>
<div class="m2"><p>که چون مجنون شدم در عشق پنهان</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>چنان لیلی صفت مجنون شدستم</p></div>
<div class="m2"><p>که گویی از خودی بیرون شدستم</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>چنان بیخود شدم اندر خودی من</p></div>
<div class="m2"><p>که یکسان شد برم نیک و بدی من</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>چنان مستم که با خود مینمایم</p></div>
<div class="m2"><p>که در هستی بکل عین بقایم</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>چنان در جان من بنشسته محبوب</p></div>
<div class="m2"><p>که طالب را بیک ره دید مطلوب</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>دلم چون یار دید و با خود آمد</p></div>
<div class="m2"><p>در آخر فارغ از نیک و بد آمد</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>در آخر فارغست و عین گفتار</p></div>
<div class="m2"><p>دمادم مینماید دیدن یار</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>منم امروز اعجوب زمانه</p></div>
<div class="m2"><p>که معشوقست حاصل بی بهانه</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>منم امروز بنموده در این راز</p></div>
<div class="m2"><p>ابا عشّاق خود انجام و آغاز</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>جمال طلعت لیلی بدیدم</p></div>
<div class="m2"><p>از آن مجنونی اکنون آرمیدم</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>جمال روی لیلی بس عیانست</p></div>
<div class="m2"><p>از او شوری فتاده در جهانست</p></div></div>