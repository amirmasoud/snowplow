---
title: >-
    بخش ۱۳ - در نشانی دادن جوهر حقیقت فرماید
---
# بخش ۱۳ - در نشانی دادن جوهر حقیقت فرماید

<div class="b" id="bn1"><div class="m1"><p>حقیقت جوهری اندر تو پیداست</p></div>
<div class="m2"><p>کز او در جمله عشق و شور و غوغاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حقیقت در تو و تو در حقیقت</p></div>
<div class="m2"><p>فرومانده تو در عین طبیعت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حقیقت در تو و تو در گمانی</p></div>
<div class="m2"><p>از آن یک رمز اینجاگه ندانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حقیقت در تو بنمودست دیدار</p></div>
<div class="m2"><p>اگر مردی یقین خود را پدید آر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حقیقت در تو است و تو در اوئی</p></div>
<div class="m2"><p>ولیکن گر براندازی دوروئی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حقیقت را یقین یابی در اینجا</p></div>
<div class="m2"><p>ابی صورت تو بشتابی در اینجا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حقیقت باز دان و راه بگذار</p></div>
<div class="m2"><p>سوی مه بازگرد و جاه بگذار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حقیقت بازدان از پیر رهبر</p></div>
<div class="m2"><p>درون تست پیر عشق رهبر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حقیقت باز دان ای کار دیده</p></div>
<div class="m2"><p>ز پیر عشق او دان یار دیده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز پیر عشق پرس احوال رازت</p></div>
<div class="m2"><p>که بنماید حقیقت راز بازت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز پیر عشق پرس و باز بنگر</p></div>
<div class="m2"><p>از او دیدار سرّ کار بنگر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز پیر عشق اگر آگاه گردی</p></div>
<div class="m2"><p>بکلّی اندر اینجا شاه گردی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز پیر عشق بستان جام اسرار</p></div>
<div class="m2"><p>فروکش جام و آنگه رو سوی دار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز پیر عشق بشنو آنچه گوید</p></div>
<div class="m2"><p>که او درمان دردت را بجوید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز پیر عشق بشنو راز و حق شو</p></div>
<div class="m2"><p>از او بین و از او دان و بدر رو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو پیری در درون داری حقیقت</p></div>
<div class="m2"><p>ندیده پیر خود گوید شفیقت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز پیر عشق بستان جام و کن نوش</p></div>
<div class="m2"><p>چو احمد جامهٔ تحقیق در پوش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز پیر ار جام بستانی دمادم</p></div>
<div class="m2"><p>به یارت در رساند او به یک دم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز پیرت نوش کن جام و بمخروش</p></div>
<div class="m2"><p>وجود خود بکلّی کن فراموش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو این جام از کف آن پیر خوردی</p></div>
<div class="m2"><p>فروکش بعد از آن هر جمله دردی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در آن دردی و مستی گر زنی دم</p></div>
<div class="m2"><p>برون باید شد از جنّت چو آدم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مرا این راز از آن شد آشکاره</p></div>
<div class="m2"><p>که کل در پیر خود کردم نظاره</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر آنچه پیر گفت اینجا نوشتم</p></div>
<div class="m2"><p>برون کرد آخر کار از بهشتم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو آدم از بهشت خود برون کرد</p></div>
<div class="m2"><p>سرشته خاک من در عین خون کرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ترا اینجا اگر رازت بهشتست</p></div>
<div class="m2"><p>بنزد عاشقان دانم که زشتست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چه باشد جنّت و رضوان و کوثر</p></div>
<div class="m2"><p>حجابی دان بر عاشق سراسر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بر عاشق بهشت اینجا عذابست</p></div>
<div class="m2"><p>اگرچه اندر او عین عتابست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بر عاشق به جز جانان نگنجد</p></div>
<div class="m2"><p>که در تحقیق جسم و جان نگنجد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بر عاشق همه دیدار جانانست</p></div>
<div class="m2"><p>بهشتش قطره‌ای از بحر پنهانست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بر عاشق به جز جانان مگو تو</p></div>
<div class="m2"><p>بجز این درد او درمان مجو تو</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کسانی کاندر این رازند مانده</p></div>
<div class="m2"><p>از آن پیوسته زین بازند مانده</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بهشت و حور و غلمان و در و دوست</p></div>
<div class="m2"><p>حقیقت مغز، یارست و دگر پوست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو آدم راز دید از وی برون شد</p></div>
<div class="m2"><p>به آخر یار او را رهنمون شد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چنانت سرّ با آدم بگویم</p></div>
<div class="m2"><p>در اینجا درد و درمانت بجویم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نه آدم را برون کردند کآدم</p></div>
<div class="m2"><p>نمیگنجید آنجاگه در آن دم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مثال بوستانی بُد بهشتش</p></div>
<div class="m2"><p>از آن مر آخر کار او بهشتش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز بعد قربت آمد هجر آخر</p></div>
<div class="m2"><p>رها کرد او بهشت از دید ظاهر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بر جانان بهشتش گشت زندان</p></div>
<div class="m2"><p>تمامت کرد ترک او همچو رندان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>منزّه شد چو ذات پاک بیچون</p></div>
<div class="m2"><p>حقیقت عاشق آسا رفت بیرون</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بهشتش عقل بود و عقل بگذاشت</p></div>
<div class="m2"><p>نمود جبرئیل ونقل بگذاشت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چنان شد آدم از ظلمت سوی نور</p></div>
<div class="m2"><p>که از جنّات و حوّا گشت او دور</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو پیر عشق او را روی بنمود</p></div>
<div class="m2"><p>مر او را کل در توفیق بگشود</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مرا او را گفت کای آدم نظر کن</p></div>
<div class="m2"><p>نمود من ببین جانت خبر کن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>اگر بیرون فتادستی ز جنّات</p></div>
<div class="m2"><p>ترا آخر رسانم سوی کلّ ذات</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>منم با تو ترا بیرون فکنده</p></div>
<div class="m2"><p>به شاهی میرسانم هان تو بنده</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مرا بشناس و با من باش ساکن</p></div>
<div class="m2"><p>که در آخر کنم بود تو ایمن</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو من دیدی منت بنمایم این راز</p></div>
<div class="m2"><p>حجاب اندازم این دم آخرت باز</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>منم بیرون فکنده تا بدانی</p></div>
<div class="m2"><p>تو ای آدم به راز کلّ نهانی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>کنون جز من مدان در سینهٔ خویش</p></div>
<div class="m2"><p>منم پیر تو ای دیرینهٔ خویش</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>از آنت کردم از جنات بیرون</p></div>
<div class="m2"><p>که تا بنمایمت کل ذات بیچون</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به غیر ما نظر اینجا مکن تو</p></div>
<div class="m2"><p>ز من بشنو حقیقت این سخن تو</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بهشت و حور و غلمان جمله دیدی</p></div>
<div class="m2"><p>که یک نقش و سراسر پیچ دیدی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ندیدی هیچ آدم بازدان تو</p></div>
<div class="m2"><p>ز من بشنو هم از من راز دان تو</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>همه مانند نقشی بود پیشت</p></div>
<div class="m2"><p>اگرچه در برونت هست خویشت</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>همه اندر نمود آدم اینجا</p></div>
<div class="m2"><p>منت کردم در اینجاگه مصفّا</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به من پیدا شد و در من نهان شد</p></div>
<div class="m2"><p>به من آدم در اینجاگه عیان شد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ترا آن رازها کاندر سر تخت</p></div>
<div class="m2"><p>نمودم بازگفتم با تو بدبخت</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ندیدستی ندیدی زان شدی دور</p></div>
<div class="m2"><p>بخود گشتی در این جنات مغرور</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نمودی من نمودم با تو آدم</p></div>
<div class="m2"><p>بگفتم با تو من سرّ دمادم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>فرستادم بتو جبریل و گفتم</p></div>
<div class="m2"><p>دو گوشم راز ما اینجا شنفتم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چنان غافل شدست از عشق حوا</p></div>
<div class="m2"><p>که یک دم می نیفتادی تو با ما</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>دمی با ما اگر چه راز گفتی</p></div>
<div class="m2"><p>ز من با من حقیقت باز گفتی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>منت حاضر بُدم در جان و در دل</p></div>
<div class="m2"><p>منت مقصود کردم جمله حاصل</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ولی در عاقبت آدم ندانی</p></div>
<div class="m2"><p>که سرّ دوست رازست و نهانی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>منت سرّ تو آدم راز گویم</p></div>
<div class="m2"><p>ز تو در تو حقیقت راز جویم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ز بهر آن برون کردست از آنجا</p></div>
<div class="m2"><p>که غیری را نبینی جز من اینجا</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>در این هجران وصال من تو دریاب</p></div>
<div class="m2"><p>در این قربت جمال من تو دریاب</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>که هجران من و وصلست هردو</p></div>
<div class="m2"><p>یکی آدم حقیقت چه من و تو</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>از آن وصل و از این هجران مرا بین</p></div>
<div class="m2"><p>درون بنگر مرا عین لقا بین</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>منم بر تو ید قدرت نموده</p></div>
<div class="m2"><p>در اینجاگه مه بدرت نموده</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>منت جنّت نمودم باز حوّا</p></div>
<div class="m2"><p>منت کردم ز دید خویش پیدا</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>منم درآخر کارت فراقی</p></div>
<div class="m2"><p>نمودم اندر اینجا اشتیاقی</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>حقیقت نوش با ما نیش باشد</p></div>
<div class="m2"><p>ترا این راه ما در پیش باشد</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>رهی در پیش داری آدم پیر</p></div>
<div class="m2"><p>بباید رفتن اکنون می چه تدبیر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ره ما راه تست و راه کن تو</p></div>
<div class="m2"><p>مگو دیگر بگستاخی سخن تو</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>رهت در ما کن و رس بر در ما</p></div>
<div class="m2"><p>که با تست این زمان مر رهبر ما</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ره عشقم ره دور و درازست</p></div>
<div class="m2"><p>در او گاهی نشیب و گه فرازست</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بمن کن راه و منزل بین تو در من</p></div>
<div class="m2"><p>بآخر آن بت صورت تو بشکن</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بمن کن راه و منزل بین و خوش باش</p></div>
<div class="m2"><p>حقیقت تن دَرِ دل بین و خوش باش</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>تو پنداری مگر کین عشقبازیست</p></div>
<div class="m2"><p>بیانی دیگرست این سر نه بازیست</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>توئی آدم ز جنّت رفته بیرون</p></div>
<div class="m2"><p>فتاده این زمان در سیر گردون</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>رهی دور و عجب در پیش داری</p></div>
<div class="m2"><p>ابا خود پیر پیش اندیش داری</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>ترا خود میکند در خود خطابی</p></div>
<div class="m2"><p>نداری زهره تاگوی جوابی</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>ندانی ره از آنی باز مانده</p></div>
<div class="m2"><p>چو گنجشکی اسیر باز مانده</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>ترا بیرون فکند از عین جنّات</p></div>
<div class="m2"><p>هزاران نکته میگوید ز آیات</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>تو چون در شکّی او را کی شناسی</p></div>
<div class="m2"><p>چو طفل از عین وحشت میهراسی</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>ترا میگوید اینجا گه دمادم</p></div>
<div class="m2"><p>در این دم چون توئی مر عین آدم</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>تو بیرونی از آن در ره فتادم</p></div>
<div class="m2"><p>ز بالا در سوی این چه فتادم</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>خطابت میکند هر لحظه زینسان</p></div>
<div class="m2"><p>تو هستی هر نفس در خود هراسان</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>نمیدانی جوابی دادن او را</p></div>
<div class="m2"><p>که باشد در خور جانان نکو را</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>چو میترسی از آنی باز در راه</p></div>
<div class="m2"><p>فتاده عاشق و بیچاره در چاه</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>رها کردی تو جنّت را بصد ناز</p></div>
<div class="m2"><p>برون پس آمدی ای صاحب راز</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>کنون چون آمدی مانند آدم</p></div>
<div class="m2"><p>خطاب خوف میآید دمادم</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>تو درخوف و بمانده در رجائی</p></div>
<div class="m2"><p>فتاده اندر این دام بلائی</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>نمیدانی که راهت از کجایست</p></div>
<div class="m2"><p>از آن جان تودرخوف و رجایست</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>رجا و خوف کی راهت نماید</p></div>
<div class="m2"><p>که جز پیرت یقین راهت نماید</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>چو پیرت در ره افکندست در خود</p></div>
<div class="m2"><p>از آنی میروی با او تو بیخود</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>دمی گوید که منزل اندر اینجاست</p></div>
<div class="m2"><p>دمی گوید که مر منزل نه پیداست</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>دمی گوید منت بیرون فکندم</p></div>
<div class="m2"><p>دمی گوید منت در خون فکندم</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>دمی گوید منت دیدار دارم</p></div>
<div class="m2"><p>ابا تو اندر این سر کار دارم</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>دمی گوید مترس و خوش همی باش</p></div>
<div class="m2"><p>گهی در آب و گه آتش همی باش</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>دمی بر کسوت آدم برآید</p></div>
<div class="m2"><p>گهی حوّا ز آدم مینماید</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>دمی تاجت نهد بر سر ز شاهی</p></div>
<div class="m2"><p>دمیت از مه در اندازد بماهی</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>دمی در خاکت اندازد بخواری</p></div>
<div class="m2"><p>نباشد زهره تا سر را بخاری</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>دمی بر عرشت افرازد یقین سر</p></div>
<div class="m2"><p>دمی از قربتت بر فرق افسر</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>دمی عزت دمی نخوت نماید</p></div>
<div class="m2"><p>دمی بُعد و دمی قربت نماید</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>بجز آنکو در این ره درد یابد</p></div>
<div class="m2"><p>چو مردان خویشتن او فرد یابد</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>غم جانان خورد در خون نشیند</p></div>
<div class="m2"><p>بجز او در همه غیری نبیند</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>بلای قرب جانان همچو آدم</p></div>
<div class="m2"><p>کشید اینجا ز عشق او دمادم</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>بلا بیند نیارد دم زدن او</p></div>
<div class="m2"><p>گهی در گفت باشد گاه در گو</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>گهی چون آدم از جنّت شود دور</p></div>
<div class="m2"><p>فتد چون سالکان اندر ره دور</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>گهی چون آتش اینجا خود بسوزد</p></div>
<div class="m2"><p>گهی چون باد آتش بر فروزد</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>گهی در بار غم مانند منصور</p></div>
<div class="m2"><p>بسوزد تاشود کلّی علی نور</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>گهی مانند او گوید اناالحق</p></div>
<div class="m2"><p>در آخر منزلت اینست الحق</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>مثالی بود این سر تا بدانی</p></div>
<div class="m2"><p>که راز دیگر است این از معانی</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>نمودی گفتم اینجا آشکاره</p></div>
<div class="m2"><p>نمیدانی نمیبینی چه چاره</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>نمیدانی که یارت با تو چونست</p></div>
<div class="m2"><p>گهی در راستی گه با سکونست</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>گهی بنمایدت دیدار بیچون</p></div>
<div class="m2"><p>گهی بیرون کند از هفت گردون</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>گهی چون سالکانت در ره خویش</p></div>
<div class="m2"><p>در اندازد بسوی درگه خویش</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>گهی چون پیر دین منصور حلّاج</p></div>
<div class="m2"><p>ترا بر فرق معنی بر نهد تاج</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>گهی بنمایدت اسرار وانگه</p></div>
<div class="m2"><p>گهی مانند او بر دار آنگه</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>کند بودت که تا رازش بگوئی</p></div>
<div class="m2"><p>ندانم تا در این معنی چگوئی</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>نمییاری بترک جان خود کرد</p></div>
<div class="m2"><p>که چون منصور گردی در همه فرد</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>نمییاری چو آدم در ره او</p></div>
<div class="m2"><p>فتادت تا رهی بر درگه او</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>نمییاری دمی تا راز بینی</p></div>
<div class="m2"><p>وصال شه در اینجا باز بینی</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>نمییاری وصال شاه دیدن</p></div>
<div class="m2"><p>گذشتن از خود و در وی رسیدن</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>نمییاری گذشت از خود حقیقت</p></div>
<div class="m2"><p>حقیقت دوست میداری طبیعت</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>طبیعت آنچنانت بند کردست</p></div>
<div class="m2"><p>که جانت مانده در دیدار فردست</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>طبیعت دوستداری زو جدائی</p></div>
<div class="m2"><p>از آن محروم از دید خدائی</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>طبیعت همچو شیطانست در تو</p></div>
<div class="m2"><p>حقیقت عین رحمانست در تو</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>طبیعت کردت ازدلدار خود دور</p></div>
<div class="m2"><p>از آنی مانده اندر خویش مغرور</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>طبیعت مر ترا در دوزخ انداخت</p></div>
<div class="m2"><p>وجودت از تف این نار بگداخت</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>طبیعت بند بندت را فروبست</p></div>
<div class="m2"><p>تو اندر گردن او کرده‌ای دست</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>چنانش دوست میداری که جانست</p></div>
<div class="m2"><p>نمیدانی که خونت رایگانست</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>بخواهد کشتنت در عین این نار</p></div>
<div class="m2"><p>تو همچون کافری دادست زنّار</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>حقیقت کافری زنّار داری</p></div>
<div class="m2"><p>که از جان مر بُتِ خود دوست داری</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>تو چون بت میپرستی کافری تو</p></div>
<div class="m2"><p>ز ناگاهی شوی از جان بری تو</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>بت نفس تو کافر مرد خواهد</p></div>
<div class="m2"><p>شدن در آتش اینجاگه نکاهد</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>ندیده‌دین، و کافر مُرد خواهی</p></div>
<div class="m2"><p>ندانم تا چه چیزی بُرد خواهی</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>شکست این و یقین را باز جو تو</p></div>
<div class="m2"><p>ابا جانت در اینجا راز جو تو</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>در آن سر جز پشیمانی و حسرت</p></div>
<div class="m2"><p>بمانی در تف نار ندامت</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>بصورت مبتلا تا چند باشی</p></div>
<div class="m2"><p>در این عین بلا تا چند باشی</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>ترا چون نیست دردی کی شود دوست</p></div>
<div class="m2"><p>ترا چون نیست مغزی باش در پوست</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>بصورت مبتلائی چون عزازیل</p></div>
<div class="m2"><p>از آنی رخ سینه مانندهٔ فیل</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>دمادم مینماید راز، جانت</p></div>
<div class="m2"><p>حقیقت میکند آگاه جانت</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>ترا هم این بباید سوخت بیشک</p></div>
<div class="m2"><p>وگرنه نکته‌ای آموخت بیشک</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>از آنی مانده در زندان به ماتم</p></div>
<div class="m2"><p>که خودبینی چو او بیشک دمادم</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>بود کز سرّ معنی بازیابی</p></div>
<div class="m2"><p>رسی در منزل آنگه شاه یابی</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>بود کین شک شود عین الیقینی</p></div>
<div class="m2"><p>ترا چون نیست اینجا پیش بینی</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>ترا چون نیست رهبر بر سر راه</p></div>
<div class="m2"><p>بماندستی چو روبه در بُن چاه</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>تو رهبر را طلب کن در دلِ ریش</p></div>
<div class="m2"><p>وز او بگشای کلّی مشکل خویش</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>تو رهبرداری اندر جان حقیقت</p></div>
<div class="m2"><p>که او بیند یقین عین طبیعت</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>ولیکن چون تو بشناسی نمودش</p></div>
<div class="m2"><p>که آخر باز دانی بود بودش</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>مر او را آدم اینجا رهنمون شد</p></div>
<div class="m2"><p>که آدم زو یقین عین سکون شد</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>ره جمله نمود و خویش گم کرد</p></div>
<div class="m2"><p>همه اندر دوئی افکند خود فرد</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>حجاب از پیش بردارد در آخر</p></div>
<div class="m2"><p>شود مخفی و بود او بظاهر</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>ز عشق این سرّ تواند شد میسّر</p></div>
<div class="m2"><p>ولیکن گرز آید عاقبت سر</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>ترا تا سَر بود این سرّ نبینی</p></div>
<div class="m2"><p>نبینی تا تو این ظاهر نبینی</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>به ظاهر شرع بین و باطن آن یاب</p></div>
<div class="m2"><p>بسوی عشق چون منصور بشتاب</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>حقیقت هر که دید او سرفشان شد</p></div>
<div class="m2"><p>چو جان داد او حقیقت جان جان شد</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>ترا تا جان بود در قالب ای دوست</p></div>
<div class="m2"><p>حقیقت مغز باشی لیک در پوست</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>دوئی چون از میان برخاست جان شد</p></div>
<div class="m2"><p>حقیقت جان ابر جانان نهان شد</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>چو جان جانان شود جز حق نباشد</p></div>
<div class="m2"><p>توئی باطل کز این جز حق نباشد</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>بجان جان توانی یافت خود را</p></div>
<div class="m2"><p>که هر کس مینگردد کل اَحَد را</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>خدا بیند خدا صورت نداند</p></div>
<div class="m2"><p>وگر داند در او حیران بماند</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>چون جان برخاست جانان رخ نماید</p></div>
<div class="m2"><p>ترا هر لحظه صد پاسخ نماید</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>چو جان شد جسم آمد در سوی خاک</p></div>
<div class="m2"><p>نهان گردید زیر چرخ افلاک</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>نهان گردد در آن خلوتگه یار</p></div>
<div class="m2"><p>در اینجاگه شود او آگه یار</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>در اینجا آگهی صورت ندارد</p></div>
<div class="m2"><p>در اینجا آگهی ار خویش دارد</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>حجابی نیست صورت اندر این خاک</p></div>
<div class="m2"><p>که اینجا میشود هم محو در پاک</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>در اینجا عین خونست و پلیدی</p></div>
<div class="m2"><p>در اینجاگه یقین بر چون رسیدی</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>در اینجا صورتت مانند خونست</p></div>
<div class="m2"><p>ولی این قصّه با مُل رهنمونست</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>چو صورت محو گردد جان بزاید</p></div>
<div class="m2"><p>بجز جان هیچ مر او را نشاید</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>چو جان گردد صور در عالمِ گِل</p></div>
<div class="m2"><p>تنی باشد که گردد در مکان دل</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>ز بعد دل شود اینجایگه جان</p></div>
<div class="m2"><p>پس آنگاهی شود دیدار جانان</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>اگرچه شرح بسیارست این را</p></div>
<div class="m2"><p>ولیکن راز میجوید یقین را</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>یقین این است اندر آخر کار</p></div>
<div class="m2"><p>که میگردد صور کل ناپدیدار</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>حقیقت همچو جان اینجا شود گم</p></div>
<div class="m2"><p>مثال قطره در دریای قلزم</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>حقیقت قطره چون در بحر پیوست</p></div>
<div class="m2"><p>یقین هم نیست گردد کاندر او هست</p></div></div>
<div class="b" id="bn180"><div class="m1"><p>چو قطره عین دریا شد در اینجا</p></div>
<div class="m2"><p>حقیقت بود یکتا شد در اینجا</p></div></div>
<div class="b" id="bn181"><div class="m1"><p>چو جسمت محو شد کل بود گردد</p></div>
<div class="m2"><p>بگویم عاقبت معبود گردد</p></div></div>
<div class="b" id="bn182"><div class="m1"><p>وصال صورتست اندر دل خاک</p></div>
<div class="m2"><p>در اینجاگه رسد در صانع پاک</p></div></div>
<div class="b" id="bn183"><div class="m1"><p>در اینجا مخزن خود باز بیند</p></div>
<div class="m2"><p>در اینجا او حقیقت راز بیند</p></div></div>
<div class="b" id="bn184"><div class="m1"><p>در اینجا آتشت چون نار گردد</p></div>
<div class="m2"><p>نمود خاک کلّی در نوردد</p></div></div>
<div class="b" id="bn185"><div class="m1"><p>سوی معدن شود با مسکن خود</p></div>
<div class="m2"><p>بیابد بار دیگر مأمن خود</p></div></div>
<div class="b" id="bn186"><div class="m1"><p>دگر چون هم از اینجا او شود باز</p></div>
<div class="m2"><p>بسوی باد یابد همچنین راز</p></div></div>
<div class="b" id="bn187"><div class="m1"><p>دگر هم آب شد اینجا روانه</p></div>
<div class="m2"><p>رسد در آب اینجا بی بهانه</p></div></div>
<div class="b" id="bn188"><div class="m1"><p>یقین چون خاک باشد در سوی خاک</p></div>
<div class="m2"><p>یکی باشد همه در عین کل پاک</p></div></div>
<div class="b" id="bn189"><div class="m1"><p>پلیدی پاک گردد بد نماند</p></div>
<div class="m2"><p>بجز عطّار این سِرّ کس نداند</p></div></div>
<div class="b" id="bn190"><div class="m1"><p>که عطّار است اینجا راز دیده</p></div>
<div class="m2"><p>ز خود مرده در اینجا باز دیده</p></div></div>
<div class="b" id="bn191"><div class="m1"><p>بمرد از خویش اندر گور صورت</p></div>
<div class="m2"><p>فتادت این همه دید ضرورت</p></div></div>
<div class="b" id="bn192"><div class="m1"><p>چنان این سر در اینجا باز دیدست</p></div>
<div class="m2"><p>که خود مُردست وین کل راز دیدست</p></div></div>
<div class="b" id="bn193"><div class="m1"><p>چو مر این جسم و جانش اینچنین است</p></div>
<div class="m2"><p>کسی کاین یافت اینجا راز بینست</p></div></div>
<div class="b" id="bn194"><div class="m1"><p>بباید رفت زینجا آخر کار</p></div>
<div class="m2"><p>بزیر خاک تاریکت بیکبار</p></div></div>
<div class="b" id="bn195"><div class="m1"><p>حجاب اینجا برافتد تا بدانی</p></div>
<div class="m2"><p>ز من دریاب این راز نهانی</p></div></div>
<div class="b" id="bn196"><div class="m1"><p>هر آنکو مُرد آخر زنده گردد</p></div>
<div class="m2"><p>چو خورشید از فلک تابنده گردد</p></div></div>
<div class="b" id="bn197"><div class="m1"><p>در اینجا زندگانی مرگ باشد</p></div>
<div class="m2"><p>ولی چون عاقبت کل ترک باشد</p></div></div>
<div class="b" id="bn198"><div class="m1"><p>در آخر ترک خواهد بُد ز صورت</p></div>
<div class="m2"><p>بباید شد از این معنی ضرورت</p></div></div>
<div class="b" id="bn199"><div class="m1"><p>بباید شد از این دنیای غدّار</p></div>
<div class="m2"><p>نباید بست دل در دهر خونخوار</p></div></div>
<div class="b" id="bn200"><div class="m1"><p>در این دنیا که بر عین بلایست</p></div>
<div class="m2"><p>دهان بگشاده همچون اژدهایست</p></div></div>
<div class="b" id="bn201"><div class="m1"><p>دمادم میکشد هر کس سوی خویش</p></div>
<div class="m2"><p>زند بر جان هر کس هر زمان نیش</p></div></div>
<div class="b" id="bn202"><div class="m1"><p>در اینجائی فنا اندر بلائی</p></div>
<div class="m2"><p>بآخر زهر کام اژدهائی</p></div></div>
<div class="b" id="bn203"><div class="m1"><p>چو مردان از دم او کن کناره</p></div>
<div class="m2"><p>مکن در سوی آن ملعون نظاره</p></div></div>
<div class="b" id="bn204"><div class="m1"><p>ببین او را که کامی زشت دارد</p></div>
<div class="m2"><p>ابا کسی هیچ اُنسی میندارد</p></div></div>
<div class="b" id="bn205"><div class="m1"><p>ندارد هیچ اُنسی با کس این شوم</p></div>
<div class="m2"><p>از این معنی شود جان تو معلوم</p></div></div>
<div class="b" id="bn206"><div class="m1"><p>چو بوقلمونست دنیا تو نظر کن</p></div>
<div class="m2"><p>دل خود را از این معنی خبر کن</p></div></div>
<div class="b" id="bn207"><div class="m1"><p>برآرد رنگ بر مانندهٔ تو</p></div>
<div class="m2"><p>نیوش این پند از دانندهٔ تو</p></div></div>
<div class="b" id="bn208"><div class="m1"><p>چو شکلی ساخت این ملعون مکّار</p></div>
<div class="m2"><p>چو نقش تو شود اینجا پدیدار</p></div></div>
<div class="b" id="bn209"><div class="m1"><p>نماید خویشتن را با تو اینجا</p></div>
<div class="m2"><p>که بفریبد ترا ای مرد دانا</p></div></div>
<div class="b" id="bn210"><div class="m1"><p>تو پنداری که او را دوست گیری</p></div>
<div class="m2"><p>نمیدانی که اندر پوست میری</p></div></div>
<div class="b" id="bn211"><div class="m1"><p>چنانت درکشد چون اژدهائی</p></div>
<div class="m2"><p>که دیگر مینیابد زورهائی</p></div></div>
<div class="b" id="bn212"><div class="m1"><p>چنین است آخرت آنگه بدیدی</p></div>
<div class="m2"><p>چرا در سوی دنیا آرمیدی</p></div></div>
<div class="b" id="bn213"><div class="m1"><p>ترا دنیا خوش آمد ای برادر</p></div>
<div class="m2"><p>چو ققنوس این زمان در سوی آذر</p></div></div>
<div class="b" id="bn214"><div class="m1"><p>فتادستی و هم در وی بسوزی</p></div>
<div class="m2"><p>هم ازخود آتشی در خود فروزی</p></div></div>
<div class="b" id="bn215"><div class="m1"><p>بخواهی سوخت اندر آخر کار</p></div>
<div class="m2"><p>بخواهی مرد اندر وی به پندار</p></div></div>
<div class="b" id="bn216"><div class="m1"><p>تو تا کی ماندهٔ دنیا بمانی</p></div>
<div class="m2"><p>ز سرّ آخرت رمزی ندانی</p></div></div>
<div class="b" id="bn217"><div class="m1"><p>ز سرّ آخرت این سر شنفتی</p></div>
<div class="m2"><p>نکردی گوش و اندر خواب خفتی</p></div></div>
<div class="b" id="bn218"><div class="m1"><p>ترا دنیا چنان در قید کردست</p></div>
<div class="m2"><p>که مرغ جانت اینجا صید کردست</p></div></div>
<div class="b" id="bn219"><div class="m1"><p>چو صیّاد ازل مر مرغ جانت</p></div>
<div class="m2"><p>گرفت و صید کرد آخر نهانت</p></div></div>
<div class="b" id="bn220"><div class="m1"><p>نخواهد گشت اندر خاک ره خوار</p></div>
<div class="m2"><p>تو خواهی ماند اندر عاقبت زار</p></div></div>
<div class="b" id="bn221"><div class="m1"><p>نخواهی یافت آخر می رهائی</p></div>
<div class="m2"><p>چرا بیچاره در قید و بلائی</p></div></div>
<div class="b" id="bn222"><div class="m1"><p>ز جان مر جان خود بگذار دنیا</p></div>
<div class="m2"><p>ره حق گیر و رسوائی مولی</p></div></div>
<div class="b" id="bn223"><div class="m1"><p>ز دنیا هیچ ناید مر ترا سود</p></div>
<div class="m2"><p>بجز آن کاندر این آتش شوی زود</p></div></div>
<div class="b" id="bn224"><div class="m1"><p>جهان نزدیک حق قدری ندارد</p></div>
<div class="m2"><p>هِلالست این مَهَت بدری ندارد</p></div></div>
<div class="b" id="bn225"><div class="m1"><p>جهان و هرچه در روی جهان است</p></div>
<div class="m2"><p>چو یک ذاتست چون یابد جهان است</p></div></div>
<div class="b" id="bn226"><div class="m1"><p>جهان بگذار و بگذر زو یقین تو</p></div>
<div class="m2"><p>چو مردان باش در خود پیش بین تو</p></div></div>
<div class="b" id="bn227"><div class="m1"><p>جهان بگذار کین مردار هیچست</p></div>
<div class="m2"><p>که چون نقش عجائب پیچ پیچست</p></div></div>
<div class="b" id="bn228"><div class="m1"><p>جهان بگذار تا یابی رهائی</p></div>
<div class="m2"><p>خدا بشناس وز وی کن خدائی</p></div></div>
<div class="b" id="bn229"><div class="m1"><p>جهان بگذار چون مردان و دیندار</p></div>
<div class="m2"><p>نمود خویش در عین الیقین دار</p></div></div>
<div class="b" id="bn230"><div class="m1"><p>جهان بگذار و بگذر زو چو مردان</p></div>
<div class="m2"><p>خود از بند بلا آزاد گردان</p></div></div>
<div class="b" id="bn231"><div class="m1"><p>جهان بگذار چون آدم ز جنّت</p></div>
<div class="m2"><p>در افکن خویشتن در سرّ قربت</p></div></div>
<div class="b" id="bn232"><div class="m1"><p>جهان بگذار همچون او ره دوست</p></div>
<div class="m2"><p>که تا آخر شوی مر آگه دوست</p></div></div>
<div class="b" id="bn233"><div class="m1"><p>جهان بگذار و همچون او فنا شو</p></div>
<div class="m2"><p>در آن دید جهان عین بقا شو</p></div></div>
<div class="b" id="bn234"><div class="m1"><p>جهان بگذار تا یابی سرانجام</p></div>
<div class="m2"><p>بنوشی از کف معشوق خود جام</p></div></div>
<div class="b" id="bn235"><div class="m1"><p>جهان جاودان بنگر در اینجا</p></div>
<div class="m2"><p>حقیقت جان جان بنگر در اینجا</p></div></div>
<div class="b" id="bn236"><div class="m1"><p>چه دیدی آخر از دنیا چگوئی</p></div>
<div class="m2"><p>که سرگردان در او مانند گوئی</p></div></div>
<div class="b" id="bn237"><div class="m1"><p>چه دیدی آخر از دنیا به جز رنج</p></div>
<div class="m2"><p>کشیدی رنج و نادیده رخ گنج</p></div></div>
<div class="b" id="bn238"><div class="m1"><p>چه دیدی آخر از دنیا به جز غم</p></div>
<div class="m2"><p>نمودت درد و غم اینجا دمادم</p></div></div>
<div class="b" id="bn239"><div class="m1"><p>چه دیدی آخر از دنیای غدّار</p></div>
<div class="m2"><p>بجز درد و بلا و عین آزار</p></div></div>
<div class="b" id="bn240"><div class="m1"><p>ز دنیا هیچ دل شادان نباشد</p></div>
<div class="m2"><p>که جان و دل بکلّی میخراشد</p></div></div>
<div class="b" id="bn241"><div class="m1"><p>ز دنیا هیچ دل را نیست شادی</p></div>
<div class="m2"><p>عجب در غرق این دریا فتادی</p></div></div>
<div class="b" id="bn242"><div class="m1"><p>چو دریائیست دنیا موج پر خون</p></div>
<div class="m2"><p>دمادم میزند بر هفت گردون</p></div></div>
<div class="b" id="bn243"><div class="m1"><p>چو دریائیست دنیا پر نهنگست</p></div>
<div class="m2"><p>درونش جای عیش و هوش و هنگست</p></div></div>
<div class="b" id="bn244"><div class="m1"><p>در این دریا بسی کشتی نظر کن</p></div>
<div class="m2"><p>دل خود را از این دریا خبر کن</p></div></div>
<div class="b" id="bn245"><div class="m1"><p>که پر موجست از خون عزیزان</p></div>
<div class="m2"><p>از او شو گر تو مردی هان گریزان</p></div></div>
<div class="b" id="bn246"><div class="m1"><p>نهنگ جانستان اینجاست دائم</p></div>
<div class="m2"><p>کز او هر لحظه صد غوغاست دائم</p></div></div>
<div class="b" id="bn247"><div class="m1"><p>در این دریا هر آن کشتی که یابد</p></div>
<div class="m2"><p>شتابان سوی آن کشتی شتابد</p></div></div>
<div class="b" id="bn248"><div class="m1"><p>بیک دم در کشد کشتی بیکبار</p></div>
<div class="m2"><p>شود در عین دریا ناپدیدار</p></div></div>
<div class="b" id="bn249"><div class="m1"><p>ز دنیا بگذر ای سالک حقیقت</p></div>
<div class="m2"><p>که کس جز جان نخواهد بُد رفیقت</p></div></div>
<div class="b" id="bn250"><div class="m1"><p>ز دنیا بگذر ای دل یک زمان تو</p></div>
<div class="m2"><p>مبند اینجای خود در جسم وجان تو</p></div></div>