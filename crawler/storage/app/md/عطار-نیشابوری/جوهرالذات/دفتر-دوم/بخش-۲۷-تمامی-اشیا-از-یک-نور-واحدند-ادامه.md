---
title: >-
    بخش ۲۷ - تمامی اشیا از یک نور واحدند (ادامه)
---
# بخش ۲۷ - تمامی اشیا از یک نور واحدند (ادامه)

<div class="b" id="bn1"><div class="m1"><p>اگر سجده کنی مانند عطّار</p></div>
<div class="m2"><p>ببینی بت پرست خویش دیدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ببینی بُت پرستی خویش اینجا</p></div>
<div class="m2"><p>تو برداری حجاب از پیش اینجا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ببینی بُت پرست خویش در خویش</p></div>
<div class="m2"><p>حجاب این جات او بردارد از پیش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ببینی بت پرست مر وجودی</p></div>
<div class="m2"><p>کنی او را دمادم تو سجودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سجود بت پرست خویشتن کن</p></div>
<div class="m2"><p>نمی گویم تو سجده جان و تن کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سجود بت پرست لامکانی</p></div>
<div class="m2"><p>بکن اینجا که تو او را بدانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سجود بت پرست اینجاست دریاب</p></div>
<div class="m2"><p>حقیقت دید او پیداست دریاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سجود او کن و دریاب اصلش</p></div>
<div class="m2"><p>در اینجاگاه کل دریاب وصلش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سجود او کن و دیدار او بین</p></div>
<div class="m2"><p>وجود خویشتن اسرار او بین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>توئی بت او حقیقت بُت پرستست</p></div>
<div class="m2"><p>چو امروز اودرون خویش مستست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>توئی بت سجده کن او را دمادم</p></div>
<div class="m2"><p>که بنمودست رخ در عین عالم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سجود او کن از راه شریعت</p></div>
<div class="m2"><p>که بنمودست اینجاگاه دیدت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سجود او کن اویت ره نماید</p></div>
<div class="m2"><p>جمال خویش کل ناگه نماید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سجود او کن اندر زندگانی</p></div>
<div class="m2"><p>که اندر زندگی او را بدانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چه به باشد ترا دیدن از این راز</p></div>
<div class="m2"><p>که بنماید دراو اینجا نظر باز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر بنمایدت ناگه جمالش</p></div>
<div class="m2"><p>تو خود یابی حقیقت اتصالش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو و او هر دو یکی در یک آمد</p></div>
<div class="m2"><p>حقیقت در یکی کل بیشک آمد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو و او در جلال لایزالی</p></div>
<div class="m2"><p>حقیقت تو از او اندر جلالی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرا او را سجده کن در شرع تحقیق</p></div>
<div class="m2"><p>که از وی بازیابی عزّ و توفیق</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مر او را سجده کن در شرع بیشک</p></div>
<div class="m2"><p>که او اصلست و تو خود فرع بیشک</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو این دم صورتی او جان جان است</p></div>
<div class="m2"><p>که در تو راز پیدا و نهانست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چنان کن سجده اندر پیش رویش</p></div>
<div class="m2"><p>که یکی بینی اینجا جمله سویش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چنان کن سجده در دیدار جانان</p></div>
<div class="m2"><p>که او را بیشکی یابی تو اعیان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چنان کن سجده پیش روی دلدار</p></div>
<div class="m2"><p>که در یکی شوی با او نمودار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چنان کن سجده پیش روی آن ماه</p></div>
<div class="m2"><p>که بینی در یکی مر جملگی شاه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چنان کن سجده پیش روی خورشید</p></div>
<div class="m2"><p>که دریکی توئی تا عین جاوید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ببازی نیست اینجا دیدن یار</p></div>
<div class="m2"><p>کسی کاینجا بود از وی خبردار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نبیند غیر او در هیچ بابی</p></div>
<div class="m2"><p>حقیقت نشنود جزوی خطابی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نبیند غیر او در هیچ دیدار</p></div>
<div class="m2"><p>ببازی نیست اینجا دیدن یار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نبیند غیر او در هیچ دیدار</p></div>
<div class="m2"><p>که جمله اوست بیشک در نمودار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نبیند غیر او در کلّ دنیا</p></div>
<div class="m2"><p>یکی بیند همه دیدارمولی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همه دیدار جانانست دریاب</p></div>
<div class="m2"><p>درون خویش خورشید جهانتاب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جهان از نور رویش پر ضیایست</p></div>
<div class="m2"><p>حقیقت نور با نور آشنایست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جهان از نور رویش روشن آمد</p></div>
<div class="m2"><p>حقیقت سرّ او در گلشن آمد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در این صورت نمودارست جانان</p></div>
<div class="m2"><p>حقیقت چون مه و خورشید تابان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در این صورت نمودارست دریافت</p></div>
<div class="m2"><p>که خود در خود حقیقت خود خبر یافت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در این صورت همه مردان آفاق</p></div>
<div class="m2"><p>از او در وی یقین گشتند مشتاق</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در این صورت همه رویش بدیدند</p></div>
<div class="m2"><p>اگرچه جمله در وی ناپدیدند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در این صورت چو بنماید جمالت</p></div>
<div class="m2"><p>حقیقت در جهان نور جلالست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>در این صورت تو دیدارش بیابی</p></div>
<div class="m2"><p>مگو ور نه تو بردارش بیابی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نمود صورتِ او بیشکی راز</p></div>
<div class="m2"><p>کند از خویشتن در خویش پرواز</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نمیشاید بگفتن راز جانان</p></div>
<div class="m2"><p>بجز با صاحب دردی به پنهان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اگر تو صاحب درد و بقائی</p></div>
<div class="m2"><p>در اینجاگاه باید آشنائی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>در اینجاگاه سرّ کار بنگر</p></div>
<div class="m2"><p>نظر کن دیدِ دیدِ یار بنگر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>مگو با هیچکس تو راز پنهان</p></div>
<div class="m2"><p>وگرنه بر سرت چون گوی و چوگان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کند گردان در این میدان افلاک</p></div>
<div class="m2"><p>بزاری آنگهی در زیر اینخاک</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>کند پنهان چون عطّار خدا بین</p></div>
<div class="m2"><p>مگو ورنه سرت از تن جدا بین</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>حقیقت من در اینجا صاحب اسرار</p></div>
<div class="m2"><p>شدم اینجا ز دید او خبردار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو دانستم که اینجا آشنائی است</p></div>
<div class="m2"><p>مرا این روشنی دید خدائی است</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دم سرّ اناالحق را زدم من</p></div>
<div class="m2"><p>از او مر کام جانم بستدم من</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>مرا گفتست رازم فاش کردی</p></div>
<div class="m2"><p>تو نقشی دید خود نقاش کردی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تو از دیدارمائی صورتی فاش</p></div>
<div class="m2"><p>کجا هرگز توانی گشت نقّاش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>مرا از عقل اینجا کرد افگار</p></div>
<div class="m2"><p>که تاگشتم ابر او عاشق زار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بسی در عقل اوّل راز دیدم</p></div>
<div class="m2"><p>که با او عاقبت را باز دیدم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چو عشق آمد بشد عقل از تنم باز</p></div>
<div class="m2"><p>بدیدم راز کلّی روشنم باز</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>حقیقت راز جانان فاش دیدم</p></div>
<div class="m2"><p>یقین نقش خود نقّاش دیدم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>حقیقت نقش خود دیدم عیان فاش</p></div>
<div class="m2"><p>درون خویشتن مر دید نقاش </p></div></div>
<div class="b" id="bn58"><div class="m1"><p>حقیقت فاش کردم روی جانان</p></div>
<div class="m2"><p>همه ذرّات را در کوی جانان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>حقیقت سرّ بیچون هر که گفتت</p></div>
<div class="m2"><p>چو من او بیشک از جانان شنفتت</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>دمی کآندم بگوید با همه راز</p></div>
<div class="m2"><p>چو من گردد یقین در دوست سرباز</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>حقیقت فاش کردم دید دیدار</p></div>
<div class="m2"><p>همه ذرّات را کردم خبردار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>حقیقت فاش کردم تا بدانند</p></div>
<div class="m2"><p>نوشتم تا همه عالم بخوانند</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چنین نیکو بود امّا بتقلید</p></div>
<div class="m2"><p>حقیقت از حقیقت کی توان دید</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>کسی بیند که این سرّ فاش گفتست</p></div>
<div class="m2"><p>حقیقت خویشتن نقّاش گفتست</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>کسانی کاندر این راهند زحمت</p></div>
<div class="m2"><p>کنندش از نمود خویش لعنت</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>کنندش لعنت اینجاگاه از یار</p></div>
<div class="m2"><p>چو منصورش در اینجاگاه بردار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>کنندش لعنت اندر زندگانی</p></div>
<div class="m2"><p>دهندش زحمت اندر زندگانی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>کنندش لعنت اینجاگه دمادم</p></div>
<div class="m2"><p>یکی بیند نه بنید هیچ محرم</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چو بشناسد حقیقت سرّ این راه</p></div>
<div class="m2"><p>که لعنت میکند او را عیان شاه</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>حقیقت لعنت دلدار نیکوست</p></div>
<div class="m2"><p>اگر باشد در این پندار نیکوست</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>حقیقت چون کند دلدار لعنت</p></div>
<div class="m2"><p>حقیقت به بود بیشک ز زحمت</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>اگر مر لعنت جانان کنی نوش</p></div>
<div class="m2"><p>کنی رحمت بیکباره فراموش</p></div></div>