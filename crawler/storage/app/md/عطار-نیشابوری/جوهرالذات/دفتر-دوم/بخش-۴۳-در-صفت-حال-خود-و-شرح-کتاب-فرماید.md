---
title: >-
    بخش ۴۳ - در صفت حال خود و شرح کتاب فرماید
---
# بخش ۴۳ - در صفت حال خود و شرح کتاب فرماید

<div class="b" id="bn1"><div class="m1"><p>نمودی واصل کون و مکانی</p></div>
<div class="m2"><p>حقیقت شد کنون تو جان جانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمودی آنچه هرگز کس نگفتست</p></div>
<div class="m2"><p>بسُفتی دُر که هرگز کس نسفتست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دُرِ معنی تو بگشادی حقیقت</p></div>
<div class="m2"><p>که واصل کردهٔ عین طبیعت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دُرِ معنی تو بگشادی بتحقیق</p></div>
<div class="m2"><p>ترا دادند اینجا وصل توفیق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دُرِ معنی تو بگشادی تو بگشاد</p></div>
<div class="m2"><p>کسی دیگر چو تو در اصل بنیاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دُرِ معنی تو بگشادی یقین باز</p></div>
<div class="m2"><p>نمودی با همه انجام وآغاز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دُرِ معنی گشادستی یقین است</p></div>
<div class="m2"><p>که پیدا اوّلین و آخرین است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دُر معنی کنون چون برگشادی</p></div>
<div class="m2"><p>برمردان عالم داد دادی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جواهرنامه کردی آشکاره</p></div>
<div class="m2"><p>ترا مر جزو و کل اینجا نظاره</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو ظاهر شد کنون اسرار جانت</p></div>
<div class="m2"><p>بدیدار آمده راز نهانت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنان خواهی کنون تو مست دلدار</p></div>
<div class="m2"><p>ترا خواهد نمودن آخر کار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حقیقت ذات کلّی بی وجودت</p></div>
<div class="m2"><p>ابی صورت عیان بود بودت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نهادستی کنون گردن بَرِ شاه</p></div>
<div class="m2"><p>که از شاهی کنون از عیش آگاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ترا شه عزّت اینجاگه فزودست</p></div>
<div class="m2"><p>که اسرارت ز دید خود نمودست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در این عالم شدی ازوی سرافراز</p></div>
<div class="m2"><p>کنون هیلاج گوی و سر برافراز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مگردان رخ ز گفتن یک زمان تو</p></div>
<div class="m2"><p>که خواهی گشت ناگه بی نشان تو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نشان داری کنون از عشق دلدار</p></div>
<div class="m2"><p>ترا از ذات خود کرده پدیدار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نشان داری کنون در بی نشانی</p></div>
<div class="m2"><p>بصورت لیک پنهان از معانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نشان داری کنون در پاکبازی</p></div>
<div class="m2"><p>ولی باید در آخر پاکبازی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نشان داری کنون از سرّ مردان</p></div>
<div class="m2"><p>زمانی از بلا تو رخ مگردان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نشان داری و اکنون بی نشان شو</p></div>
<div class="m2"><p>بگو هیلاج وانگه جان جان شو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بگو هیلاج وانگه جان برافشان</p></div>
<div class="m2"><p>دل و جان بر رخ جانان برافشان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بگو هیلاج و بنما آنگهی راز</p></div>
<div class="m2"><p>پس آنگه قطره در دریا درانداز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بگو هیلاج را تا بیشکی تو</p></div>
<div class="m2"><p>شوی آنگه ز بود خود یکی تو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بگو هیلاج و محو انبیا شو</p></div>
<div class="m2"><p>حقیقت عین الاّ اللّه لا شو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بگو هیلاج نزد کار دیده</p></div>
<div class="m2"><p>حقیقت نقطه و پرگار دیده</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بگو هیلاج تا پیدا کنی یار</p></div>
<div class="m2"><p>حجاب از پیش برگیری بیکبار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بگو هیلاج تا جانان نمائی</p></div>
<div class="m2"><p>دگر سرّ ازل با جان نمائی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بگو هیلاج تا آگاه گردند</p></div>
<div class="m2"><p>که بیشک هرگدائی شاه کردند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بگو هیلاج با مردان اسرار</p></div>
<div class="m2"><p>که وصل کل از آنجا شد پدیدار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>حقیقت را در آنجا فاش کن تو</p></div>
<div class="m2"><p>در آنجا نقش خود نقّاش کن تو</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در اینجا صورت و معنی برانداز</p></div>
<div class="m2"><p>حقیقت دنیی و عقبی برانداز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>برانداز آن زمان پرده ز دیدار</p></div>
<div class="m2"><p>درون جزو و کل خود را پدیدآر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در اینجا خود پدید آور که آنی</p></div>
<div class="m2"><p>که هم ذاتی و هم جسمی و جانی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دراینجا بود خود اظهار گردان</p></div>
<div class="m2"><p>برافکن بود خود را یارگردان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>حقیقت آنچه تو بنمودهٔ باز</p></div>
<div class="m2"><p>دَرِ اسرار کل بگشودهٔ باز</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نچندان وصف بیچون و چگونست</p></div>
<div class="m2"><p>نمیدانند کو در اندرونست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تو دانستی کنون اسرار جانان</p></div>
<div class="m2"><p>ترا بنموده است دیدار جانان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تو دیداری کنون ای پیر جمله</p></div>
<div class="m2"><p>که میدانی کنون تدبیر جمله</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>یقینشان واصلی بنمودهٔ تو</p></div>
<div class="m2"><p>حقیقت جزء و کل بگشودهٔ تو</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اگر هیلاج خواهی گفت این بار</p></div>
<div class="m2"><p>حجاب کفر از ایمان تو بردار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چنان گو سرّ کل در آخر ای دوست</p></div>
<div class="m2"><p>که کلّی یک نماید مغز با پوست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چنان گو سرّ کل در آخر کار</p></div>
<div class="m2"><p>که می هیچی نباشد جز که دیدار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چنان گو سرّ کل و صاحب راز</p></div>
<div class="m2"><p>که یابد آخر این گم کرده را باز</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چنان گو سرّ کل اندر شریعت</p></div>
<div class="m2"><p>که بینی در شریعت دید دیدت</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>منه بیرون تو پای از شرع زنهار</p></div>
<div class="m2"><p>که از عین شریعت شد خبردار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دل و جانت حقیقت در یکیاند</p></div>
<div class="m2"><p>ز نور شرع جانان بیشکیاند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بنور شرع هر دو راه دیدند</p></div>
<div class="m2"><p>در آخر هر دو روی شاه دیدند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تو اکنون واصل هر دو جهانی</p></div>
<div class="m2"><p>بهر دم درجهان گوئی معانی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ببردی گوی معنی عاقبت باز</p></div>
<div class="m2"><p>دل و جان اندر آخر عاقبت باز</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>دل و جان عاقبت در باز در یار</p></div>
<div class="m2"><p>مرا جای دگر هان از بَرِ یار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ترا این درگشاد اینجایگه یار</p></div>
<div class="m2"><p>ترا بخشیده است این پایگه یار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ندارد هیچکس امروز دلدار</p></div>
<div class="m2"><p>چنان در بر بکام دل که عطّار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>دل عطّار امروز است کل جان</p></div>
<div class="m2"><p>حقیقت جان شده هم محو جانان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>دل عطّار امروز اوفتاده است</p></div>
<div class="m2"><p>چو خورشیدی که در روز اوفتادست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چنان در آسمان جانست گردان</p></div>
<div class="m2"><p>بسان جوهر ذاتست رخشان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ترا این جوهر دل کن نظر باز</p></div>
<div class="m2"><p>که کل در دل ویست و جان خبرباز</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>دگر با دل یقین دادست اینجا</p></div>
<div class="m2"><p>در دل جان چو بگشادست اینجا</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>از آن این گنج دل پرگوهر آمد</p></div>
<div class="m2"><p>که از صورت بیک ره بر درآمد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>برون آمد یقین از جایگه او</p></div>
<div class="m2"><p>فکندش هرچه بودش سوی ره او</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>دلم چون ترکِ بودِ خویش کرد است</p></div>
<div class="m2"><p>از آن ذرّات رادر پیش کرد است</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>حقیقت دل نهادم بر کف دست</p></div>
<div class="m2"><p>که جان دیدم که کل بادوست پیوست</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>دلم چون جان فنا را کرد آخر</p></div>
<div class="m2"><p>تقاضا تا که باشد فرد آخر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>کنون فردست جان در دل بمانده</p></div>
<div class="m2"><p>بروی جان جان حیران بمانده</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>کنون فردست دل در عین جانان</p></div>
<div class="m2"><p>که در جانست اینجاگاه پنهان</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>کنون فردست دل در دید دلدار</p></div>
<div class="m2"><p>از آن یکی است در توحید دلدار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>که جان در اوست او درجان نمودار</p></div>
<div class="m2"><p>از آن هر دو بجانان ناپدیدار</p></div></div>