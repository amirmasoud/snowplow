---
title: >-
    بخش ۵ - در صفات پرده در افتادن فرماید
---
# بخش ۵ - در صفات پرده در افتادن فرماید

<div class="b" id="bn1"><div class="m1"><p>در آن ساعت که این پرده برافتد</p></div>
<div class="m2"><p>ترا آن دم نظر بر جوهر افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آن ساعت که این پرده نماند</p></div>
<div class="m2"><p>ترا جوهر بنزد خویش خواند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آن دم باز بینی یار خود گم</p></div>
<div class="m2"><p>که بودی کرده در دیدار قلزم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حجاب آن دم که برگیرندت از پیش</p></div>
<div class="m2"><p>بجز یکی مبین چه پس چه از پیش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حجاب آن دم که برگیرند پیدا</p></div>
<div class="m2"><p>شود اندر یقین ذرّات شیدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حجاب آن دم که برگیرد نظر کن</p></div>
<div class="m2"><p>دلت از اوّل و آخر خبر کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حجاب آن دم که برگیرد به بینی</p></div>
<div class="m2"><p>یکی اندر یکی گر در یقینی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حجاب آن دم که برگیرد از آن ذات</p></div>
<div class="m2"><p>یکی گردد در آنجا و یکی ذات</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی بنگر دوئی بگذار ای جان</p></div>
<div class="m2"><p>که جانانت نبود غیر جانان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حجابی نیست مقصود من اینست</p></div>
<div class="m2"><p>کسی داند که در عین الیقین است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حجابی نیست کل دیدار یارست</p></div>
<div class="m2"><p>عدد بنموده یکی بیشمارست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حجابی نیست یکی بین زمانی</p></div>
<div class="m2"><p>نخواهی یافت بهتر زین زمانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حجابی نیست در عین شریعت</p></div>
<div class="m2"><p>که یکسانست آخر در حقیقت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حجابی نیست امّا تو حجابی</p></div>
<div class="m2"><p>که پیوسته تو درعین حسابی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حجابی نیست جز برگفتن ای دوست</p></div>
<div class="m2"><p>وگرنه بیشکی میدان که کل اوست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حجابی نیست کاین سر بی حجابست</p></div>
<div class="m2"><p>دل ذرّات با خود در حسابست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حجابی نیست ای دل چند گوئی</p></div>
<div class="m2"><p>یکی داری یکی پیوند جوئی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو پند تو با دیدار بایست</p></div>
<div class="m2"><p>دل و جانت پر از اسرار بایست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ترا این راز بنمودست سرباز</p></div>
<div class="m2"><p>مگو بسیار هان برخیز و سرباز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چووقت آمد که برداری حجابت</p></div>
<div class="m2"><p>نماند هیچ اعداد و حسابت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چووقت آمد حسابت رفت خواهد</p></div>
<div class="m2"><p>حقیقت بود تن اینجا بکاهد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چووقت آمد که با آن سر شوی باز</p></div>
<div class="m2"><p>سزد کین سرّ ز جانان بشنوی باز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دمادم اقتلونی یا ثقاتی</p></div>
<div class="m2"><p>پس آنگه اِنَّ فی قتلی حیاتی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حیات تست اندر کشتن تو</p></div>
<div class="m2"><p>یقین تو بخون آغشتن تو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بخواهد کشت جانانت در آخِر</p></div>
<div class="m2"><p>که آن مخفی ببینی دوست ظاهر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بخواهد کشت جانانت چنان زار</p></div>
<div class="m2"><p>که آن دم باز بینی عین دیدار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چه باشد جان چو جانان رخ نماید</p></div>
<div class="m2"><p>خوش آن ساعت که او پاسخ نماید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چه باشد تن ز بهر کشتن یار</p></div>
<div class="m2"><p>هزاران جان چه باشد پیشش ایثار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چه باشد آنقدر گویم چه باشد</p></div>
<div class="m2"><p>که عاشق پیش اقدام تو باشد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ترا چون من هزارانست اینجا</p></div>
<div class="m2"><p>ز من سرگشته تو مجروح و شیدا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بکش جانا و کلّی وارهانم</p></div>
<div class="m2"><p>مرا چه غم توئی جان و جهانم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بکُش جانا مرا تا چند سوزی</p></div>
<div class="m2"><p>نماند مر مرا در بند سوزی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بکش جانا مرا در قرب قربت</p></div>
<div class="m2"><p>که دیدستم بسی اندوه و محنت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بکش جانا مرا مراتا من نمانم</p></div>
<div class="m2"><p>کتاب هجر بر تو چند خوانم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بکش جانا مرا تا کل تو مانی</p></div>
<div class="m2"><p>که سرگردانم از دست معانی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مرا بی بود معنی کن که صورت</p></div>
<div class="m2"><p>یقین دانم که خواهد شد ضرورت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چنان از دست معنی ماندهام من</p></div>
<div class="m2"><p>اگرچه جوهرش افشاندهام من</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چنان ازدست معنی من اسیرم</p></div>
<div class="m2"><p>حقیقت زین اسیری دستگیرم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چنان ازدست معنی پای بندم</p></div>
<div class="m2"><p>که مانده ناامید و مستمندم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چنان از دست معنی باز ماندم</p></div>
<div class="m2"><p>که بی روی تو دل از راز ماندم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چنانم کرد معنی واله و مست</p></div>
<div class="m2"><p>که صورت با نمود دوست پیوست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ولیکن عشق دید هرزه گویست</p></div>
<div class="m2"><p>در این میدان بسرگردان چو گویست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>در این میدان معنی تاختم پر</p></div>
<div class="m2"><p>فشاندستم در این میدان بسی دُر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>در این میدان ز دستم گوی وحدت</p></div>
<div class="m2"><p>بهر معنی که بُد دلجوی حضرت</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>حقیقت معنی اینجا ره ندارد</p></div>
<div class="m2"><p>که عشقش جز دل آگه ندارد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو معنی نزد عشقش کاردان شد</p></div>
<div class="m2"><p>ز پیدائی در او کلّی نهان شد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ندارد راه معنی سوی دلدار</p></div>
<div class="m2"><p>بگفت و گو شده در کوی دلدار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>حقیقت عشق و درد عشق دریاب</p></div>
<div class="m2"><p>ز بود عشق خود یک دم خبر یاب</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>حقیقت عشق دریاب از معانی</p></div>
<div class="m2"><p>که بنماید نشان بی نشانی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>اگر عشقت نماید رخ در اینجا</p></div>
<div class="m2"><p>دهد بیشک ترا پاسخ در اینجا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>اگر عشقت نماید دوست یابی</p></div>
<div class="m2"><p>نمود او درون پوست یابی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>اگر عشق کند بیرنگ صورت</p></div>
<div class="m2"><p>به بینی روی جانان در حضورت</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>حضور عشق اگر آری پدیدار</p></div>
<div class="m2"><p>شود اشیا بدستت ناپدیدار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>حضور عشق سالک را نداند</p></div>
<div class="m2"><p>وگرداند بجای خود نماند</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>حضور عشق واصل یافت اینجا</p></div>
<div class="m2"><p>مراد خویش حاصل یافت اینجا</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>حضور عشق آدم زاندم اوست</p></div>
<div class="m2"><p>مسمّا کرد و گفت این دم دم اوست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>حضور عشق جنّات نعیمست</p></div>
<div class="m2"><p>در اینجاگه چه جاس ترس و بیمَست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>حضور عشق اینجا رخ نمودست</p></div>
<div class="m2"><p>که این دم در همه گفت و شنوداست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>حضور عشق بیشک عین نورست</p></div>
<div class="m2"><p>کسی داند کز آن دم با حضور است</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>حضور عشق بشناس ای دل ریش</p></div>
<div class="m2"><p>بجز جانان تو منگر از پس و پیش</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بجز جانان مبین در عشقبازی</p></div>
<div class="m2"><p>حرامست از چنین جز عشقبازی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بجز جانان مبین در هیچ احوال</p></div>
<div class="m2"><p>چو دیدی این زمان دیگر مزن قال</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بجز جانان مبین تا راز دانی</p></div>
<div class="m2"><p>نمود عشقبازی باز دانی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بجز جانان مبین وین پرده بردار</p></div>
<div class="m2"><p>وگر یارت کند با پرده بردار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بجز جانان مبین مانند مردان</p></div>
<div class="m2"><p>که مرادن باز دیدند روی جانان</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بجز جانان مبین تو در نمودش</p></div>
<div class="m2"><p>بکن چون جملهٔ مردان سجودش</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بجز جانان مبین ای کاردان تو</p></div>
<div class="m2"><p>همه جانان نگر در دید جان تو</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بجز جانان مبین و در فنا باش</p></div>
<div class="m2"><p>چو گشتی تو فنا در حق بقاباش</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بجز جانان مبین ای جمله بودت</p></div>
<div class="m2"><p>که حق کلّی توکّل در سجودت</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ایا نادیده اینجا وصل جانان</p></div>
<div class="m2"><p>بمانده در نمود خویش حیران</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>تو گر اینجا بیابی اصل آن بود</p></div>
<div class="m2"><p>تو باشی بیشکی دیدار معبود</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ایا نادیده وصل جان جانت</p></div>
<div class="m2"><p>در این ظاهر گرفتار عیانت</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ابا تست آنچه گم کردی چه جوئی</p></div>
<div class="m2"><p>چو گم چیزی نکردی می چه جوئی</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ابا تست آنچه جویانند جمله</p></div>
<div class="m2"><p>ز آتش نیز گویانند جمله</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ابا تست و ندیدی ای دل ریش</p></div>
<div class="m2"><p>جمالش تا حجب برداری از پیش</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ابا تست آنچه میجویند هر کس</p></div>
<div class="m2"><p>ابا تست این بیان اوّلت بس</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ابا تست و تو با اوئی همیشه</p></div>
<div class="m2"><p>چرا در جستن و جوئی همیشه</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ابا تست و ترا دیدار باشد</p></div>
<div class="m2"><p>ترا او صاحب اسرار باشد</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ابا تست ای سلوکت وصل گشته</p></div>
<div class="m2"><p>نشاط جزو و کل در تو نوشته</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چنان رخ را نمود است از نمودار</p></div>
<div class="m2"><p>که در یکّی است کلّی لیس فی الدّار</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>همه رخ را نمود و گشت دیگر</p></div>
<div class="m2"><p>همه اینجا فکنده اندر آذر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>چو خود میگوید و خود روی بنمود</p></div>
<div class="m2"><p>همو اینجاگره از کار بگشود</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>درون جمله و بیرون گرفتست</p></div>
<div class="m2"><p>حقیقت جمله گردون گرفتست</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>فنا را در بقا پیوسته با خویش</p></div>
<div class="m2"><p>همی بیخود دراو پیوسته با خویش</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>که هر کو بود من اینجای بشناخت</p></div>
<div class="m2"><p>ز بود من در اینجا سر برافراخت</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>چو جز من نیست چیزی آشکاره</p></div>
<div class="m2"><p>کنم اندر جمال خود نظاره</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>نظاره خود بخود اینجا کنم من</p></div>
<div class="m2"><p>نمود جمله اینجا بشکنم من</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>حقیقت ذات بیچونی است اینجا</p></div>
<div class="m2"><p>در اینجا بین که بیرون نیست اینجا</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>چو ناپیدا شود این جسم تحقیق</p></div>
<div class="m2"><p>یقین برخیزد اینجا اسم تحقیق</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>چو جسم و اسم گردد ناپدیدار</p></div>
<div class="m2"><p>حقیقت جان جان آید پدیدار</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>جمالش آفتاب عالم افروز</p></div>
<div class="m2"><p>بود کاینجا از او جانست پیروز</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>جمالش آفتاب جان نموداست</p></div>
<div class="m2"><p>که اندر جانها تابان نمود است</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>جمالش هست خورشید منوّر</p></div>
<div class="m2"><p>کز او روشن شده اینجا سراسر</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>جمالش هست بر اشیا همه نور</p></div>
<div class="m2"><p>از این خورشید ذرّاتند مشهور</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>از این خورشید جانها شد دلم مست</p></div>
<div class="m2"><p>که عکس او درون این دلم هست</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>از این خورشید شهرآرای جانم</p></div>
<div class="m2"><p>چنان روشن شدم کاندر فغانم</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>اگرچه محو شد سایه ز خورشید</p></div>
<div class="m2"><p>چنان کام محو بنموداست جاوید</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>بشد سایه بیکبار از میانه</p></div>
<div class="m2"><p>که خورشید است بیشک جاودانه</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>بیکباره چو خورشید حقیقی</p></div>
<div class="m2"><p>ابا او کرد مر سایه رفیقی</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>حقیقت سایه در بود فنا شد</p></div>
<div class="m2"><p>در آن خورشید کل عین بقا شد</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>درآن خورشید شد دیدار خورشید</p></div>
<div class="m2"><p>چنان کز دید شد در نور جاوید</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>در آن خورشید دید او از سر ناز</p></div>
<div class="m2"><p>اگر تو مرد رازی زود سرباز</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>درآن خورشید هر کو در فنا شد</p></div>
<div class="m2"><p>بگویم با تو کل بیشک خدا شد</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>خدا شد هر که این اسراردریافت</p></div>
<div class="m2"><p>بدان خورشید همچون ذرّه بشتافت</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>خدا شد هر که این سر باز دید او</p></div>
<div class="m2"><p>چو منصور از حقیقت راز دید او</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>خدا شد هر که او دیدار دیدست</p></div>
<div class="m2"><p>عجب گر بود اینجا او پدیدست</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>خدا شد آنکه این سر پی برد او</p></div>
<div class="m2"><p>بجز یکی حقیقت ننگرد او</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>حقیقت جز خدا غیرست دریاب</p></div>
<div class="m2"><p>همه ذرّات در سیرست دریاب</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>حقیقت در بر این چار عنصر</p></div>
<div class="m2"><p>همی گردند در این بحر پُر در</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>ظهورش عنصر آمد در نمودار</p></div>
<div class="m2"><p>دگر خواهد شدن کل لیس فی الدار</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>ظهورش عنصرآمد راز دیده</p></div>
<div class="m2"><p>که خود در عنصرست او بازدیده</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>در این عنصر شده پیداست رویش</p></div>
<div class="m2"><p>فتاده ذرّهها در گفتگویش</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>در این عنصر شناسان گرد و بشناس</p></div>
<div class="m2"><p>جمال دوست را بیرنج وسواس</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>در این عنصر هر آنکو دید دلدار</p></div>
<div class="m2"><p>بمانندت کسی از خواب بیدار</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>شود ناگاه باشد خواب دیده</p></div>
<div class="m2"><p>نمود خویش در غرقاب دیده</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>دگر چون گشت بیدار او از آن خواب</p></div>
<div class="m2"><p>رهائی یافت او از بحر و غرقاب</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>مثالت همچو خوابی دان و بنگر</p></div>
<div class="m2"><p>که هستی از وجود خویش بر در</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>دگر ره بازگشته سوی صورت</p></div>
<div class="m2"><p>خیال بود نزد تو نفورت</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>دگر چون بازهوش آئی دگر تو</p></div>
<div class="m2"><p>بیابی اندر اینجاگه خبر تو</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>خبر یابی از آن بیهوشی خود</p></div>
<div class="m2"><p>نمودی بینی ازمدهوشی خود</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>خیالت این جهان و آن جهان بین</p></div>
<div class="m2"><p>خیالی در خیالی در عیان بین</p></div></div>