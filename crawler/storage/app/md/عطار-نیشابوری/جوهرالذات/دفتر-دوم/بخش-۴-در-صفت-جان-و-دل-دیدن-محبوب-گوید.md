---
title: >-
    بخش ۴ - در صفت جان و دل دیدن محبوب گوید
---
# بخش ۴ - در صفت جان و دل دیدن محبوب گوید

<div class="b" id="bn1"><div class="m1"><p>بسوز ای دل اگر تو سوختستی</p></div>
<div class="m2"><p>درونت آتشی افروختستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسوز ای دل همه راز نهانی</p></div>
<div class="m2"><p>که خود گفتی و هم خود میندانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسوز ای دل که همدردی ندیدی</p></div>
<div class="m2"><p>در این ره همچو خود فردی ندیدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسوز ای دل که همراهانت رفتند</p></div>
<div class="m2"><p>در این ره خفته تو ایشان نهفتند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسوز ای دل که ماندستی تو غمناک</p></div>
<div class="m2"><p>در این ماتم سرای کرهٔ خاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسوز ای دل چو تومستی در این راز</p></div>
<div class="m2"><p>بد آخر تا نمانی ذرّهٔ باز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسوز ای دل تو چون ذرّات اینجا</p></div>
<div class="m2"><p>که تا گردی حقیقت ذات اینجا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسوز ای دل که دید یار دیدی</p></div>
<div class="m2"><p>در اینجا غصهٔ بسیار دیدی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسوز ای دل که مردان چون چراغی</p></div>
<div class="m2"><p>تمامت سوختند اندر فراقی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسوز ای دل که چون منصور مستی</p></div>
<div class="m2"><p>مکن چون دیگران اینجای هستی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بسوز و نیست شو در نفخه ذات</p></div>
<div class="m2"><p>که آنگه باز یابی عین ذرّات</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو گر خود را بسوزانی خدائی</p></div>
<div class="m2"><p>یکی گردی نه چون این دم جدائی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جدائی این زمان از دید دلدار</p></div>
<div class="m2"><p>بمانده اندر این صورت در آزار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ترا اصل است بر بادی و بنگر</p></div>
<div class="m2"><p>ندادی نفس را دادی و بنگر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ترا اصل است چون بادی روانه</p></div>
<div class="m2"><p>بگردی سوی خاکی آشیانه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ترا اصل است بادی عمر بر باد</p></div>
<div class="m2"><p>کجا در باد ماند خاک آباد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ترا چون اصل خاک و باد و آبست</p></div>
<div class="m2"><p>فتاده آتشی در دل چه تابست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو این بنهادهٔ در پیش خود تو</p></div>
<div class="m2"><p>شده قانع بسوی نیک و بد تو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چنانت آتش اینجا برفروزند</p></div>
<div class="m2"><p>که خشک و تر در اینجاگه بسوزند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنات باد در پندارت آورد</p></div>
<div class="m2"><p>که ناگاهت بزیر دارت آورد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنانت آب کرد اینجا روانه</p></div>
<div class="m2"><p>که پنداری که مانی جاودانه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر این آتش بزن آبی و خوش باش</p></div>
<div class="m2"><p>وگرنه بستهٔ این پنج و شش باش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در این باد هوس تا چند باشی</p></div>
<div class="m2"><p>از آن مانده چنین در بند باشی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگر آبی زنی بر آتش و باد</p></div>
<div class="m2"><p>شود خاک وجودت جمله آباد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نماند آتش و آبی نماند</p></div>
<div class="m2"><p>در این خاکت دگر تابی نماند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دهد آن آب و خاک آنگاه بر باد</p></div>
<div class="m2"><p>شوی آنگه ندانی ذات آباد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نماند هیچ از دیدار عنصر</p></div>
<div class="m2"><p>وگر پرسی دگر گویم مگو پر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چنین کن این چنین در آخر کار</p></div>
<div class="m2"><p>که تا پرده بسوزانی بیکبار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بسوز این پرده اندر آتش عشق</p></div>
<div class="m2"><p>که تا گردد حقیقت سرکش عشق</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بسوز این پرده تا پیدانمائی</p></div>
<div class="m2"><p>تو اندر خویشتن یکتا نمائی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بسوز این پرده ای غافل بمانده</p></div>
<div class="m2"><p>چو بیکاران تو بیحاصل بمانده</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بسوز این پرده در دیدار جانان</p></div>
<div class="m2"><p>چو خواهی باشی برخوردار جانان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بسوز این پرده و دیدار او بین</p></div>
<div class="m2"><p>چنین بد نیست او جمله نکو بین</p></div></div>