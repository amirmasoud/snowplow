---
title: >-
    بخش ۷ - قصّه منصور و عیان او بهر نوع فرماید
---
# بخش ۷ - قصّه منصور و عیان او بهر نوع فرماید

<div class="b" id="bn1"><div class="m1"><p>چنان گم دید خود در دید اول</p></div>
<div class="m2"><p>که جسم و جان شد اندر هم معطّل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو او خود دید خود را دید جانان</p></div>
<div class="m2"><p>درون جزو و کل خورشید رخشان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان گم دید خود اندر صفات او</p></div>
<div class="m2"><p>که پیدا شد حقیقت عین ذات او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان خود دید در آخر در اول</p></div>
<div class="m2"><p>کسی اندر یکی صورت مبدّل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان اندر یکی بنمود جانش</p></div>
<div class="m2"><p>که صورت گشت اندر جان نهانش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنان خود دید اندر آفرینش</p></div>
<div class="m2"><p>که او بد بیشکی در جمله بینش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنان خود دید ذات لایزالی</p></div>
<div class="m2"><p>که بنموده رخش در جمله حالی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنان خود دید و خود را جمله خود دید</p></div>
<div class="m2"><p>که صورت محو دید کلّی احد دید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>احددید اندر اینجا آشکار او</p></div>
<div class="m2"><p>نموده روی خود در پنج و چار او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>احد دید و عدد برداشت از پیش</p></div>
<div class="m2"><p>نظاره کرد اینجاگه رخ خویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>احد دید و گمانش با یقین شد</p></div>
<div class="m2"><p>حقیقت کفر او اسرار بین شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اناالحق راز دان خورشید تابان</p></div>
<div class="m2"><p>که پیشش نیک و بد کل بود یکسان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اناالحق زان زد آن سلطان جمله</p></div>
<div class="m2"><p>که جان بُد جمله او جانان جمله</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اناالحق راز دان ماه دلارام</p></div>
<div class="m2"><p>که بیشک یافت اینجا بی دلارام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو درعین خدائی او یکی دید</p></div>
<div class="m2"><p>خود اندر جزو و کل حق بیشکی دید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یقین بر جزو و کل او گشت دانا</p></div>
<div class="m2"><p>یکی شد بروی اینجا عین اشیا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از این معنی دل واصل خبر یافت</p></div>
<div class="m2"><p>که او مانندهٔ او یک نظر یافت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نظر کن یک و بگذر از دوبینی</p></div>
<div class="m2"><p>که تا بی خویشتن حق کل ببینی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نظر کن این دوئی بردار اینجا</p></div>
<div class="m2"><p>چو او شو بیشکی بردار اینجا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنان در حالتست این چرخ گردان</p></div>
<div class="m2"><p>که میخواهد که یابد راز جانان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در این دیر فلک بنگر زمانی</p></div>
<div class="m2"><p>که تا یای یقین عین العیانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همه سرگشتگیّ تست از او</p></div>
<div class="m2"><p>که دارد پرده بیشک توی بر تو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چنان در پردهها نور است تابان</p></div>
<div class="m2"><p>که بگرفتست در ذرّات اعیان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در او نور است تابنده چو شمعی</p></div>
<div class="m2"><p>کشیده شیب و بالا عین و معنی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همه ذرّات در وی رخ نهاده</p></div>
<div class="m2"><p>که ازجمه یقین مشکل گشاده</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همه سوزنده چون پروانه از شمع</p></div>
<div class="m2"><p>در اینجا میشنو از جزو و کل جمع</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>فلک زین نور اندر تک و تابست</p></div>
<div class="m2"><p>از این حالت فتاده در شتابست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همی گردد بگرد خویش گردان</p></div>
<div class="m2"><p>که تا راهی برد در سوی جانان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همی گردد ز عشق دوست زارست</p></div>
<div class="m2"><p>از آن پیوسته بیدل بیقرار است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>قراری چون ندارد سوی بودش</p></div>
<div class="m2"><p>کجا باشد ز ذات کل نمودش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>قراری چون ندارد در نمودار</p></div>
<div class="m2"><p>از آن میکرد اینجا عاشق زار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چنان عاشق شد است از گردش خود</p></div>
<div class="m2"><p>که او را نیست اینجا تابش خود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چنان در حالت اوّل فتاد است</p></div>
<div class="m2"><p>که کُشته در خرابات اوفتادست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زمین از سیر او شد جمله ذرّات</p></div>
<div class="m2"><p>نهاده روی خود در فیض آن ذات</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شد است از عشق خود او ریزه ریزه</p></div>
<div class="m2"><p>چو دریا نیز در شور و ستیزه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چنان مست وصالش آمده باز</p></div>
<div class="m2"><p>که بشتابد از او دردی باعزاز</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>زمین و آسمان و چرخ و افلاک</p></div>
<div class="m2"><p>شود اینجا نهان در بوتهٔ خاک</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هر آن فیضی که میریزد ز گردون</p></div>
<div class="m2"><p>در اینجا خاک دارد بیچه و چون</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هر آن فیضی که میریزد از آن نور</p></div>
<div class="m2"><p>همه میآید اندر خاک مشهور</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هر آن فیضی کز آن حضرت درآید</p></div>
<div class="m2"><p>حقیقت خاک قدرت مینماید</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هر آن فیضی که میبارند از ذات</p></div>
<div class="m2"><p>خبردارست اینجا جمله ذرّات</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از آن بحری که گردون شبنم اوست</p></div>
<div class="m2"><p>فلک زان عشق اندر ماتمِ اوست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>زمین از عشق جانان پست افتاد</p></div>
<div class="m2"><p>فلک شد بیقرار و مست افتاد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو حق در خاک رخ بنمود اینجا</p></div>
<div class="m2"><p>از آن آدم شده صفات و مصفّا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هر آن فیضی که میریزد ز افلاک</p></div>
<div class="m2"><p>شود اینجا نهان در بوتهٔ خاک</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>حقیقت خاک اینجا برقرارست</p></div>
<div class="m2"><p>که صنعش دمبدم زان برقرارست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>شود اینجا حقیقت جوهر ذات</p></div>
<div class="m2"><p>نماید از همه در عین ذرّات</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>در اینجا هر که این جوهر ندید است</p></div>
<div class="m2"><p>ولی آینده اینجا ناپدید است</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>در اینجا جوهر ذات و صفاتست</p></div>
<div class="m2"><p>نمود او صفاتش نور ذاتست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>از آن مقصود بُد در آفرینش</p></div>
<div class="m2"><p>که تا پیدا نماید هردو بینش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>از آن از جوهر آمد سوی عالم</p></div>
<div class="m2"><p>که جزو و کل شود دیدار آدم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو جزو و کل بآن پیوسته باشد</p></div>
<div class="m2"><p>نمود ذات از آن پیوسته باشد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>زوالی نیست در ذات و صفاتش</p></div>
<div class="m2"><p>که بی نقصانست اثباتِ حیاتش</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نمیمیرد کسی از قرب این ذات</p></div>
<div class="m2"><p>ولیکن میشود مر محو ذرّات</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>نمیمیرد کسی بشنو بیانم</p></div>
<div class="m2"><p>که زین بحر فنا دُر میچکانم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>نمیمیرد کسی چون باشد این راز</p></div>
<div class="m2"><p>بگویم با تو این معنی دگر باز</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>نمیمیرد کسی ای کار دیده</p></div>
<div class="m2"><p>زمانی کن سوی پرگار دیده</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نمیمیرد کسی کز نور ذاتست</p></div>
<div class="m2"><p>که ذات کل حیات اندر حیاتست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نمیمیرد ولیکن عشقش اینجا</p></div>
<div class="m2"><p>نمود یار گرداند در اینجا</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>صفات اینجایگه در اسم آمد</p></div>
<div class="m2"><p>از آن نورش به پیشت جسم آمد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>تن از خاکست جان از جوهر پاک</p></div>
<div class="m2"><p>مرکّب گشته نور و اسم در خاک</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>تن از خاک است جان از ذات آمد</p></div>
<div class="m2"><p>مرکب شد چو از ذرّات آمد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>یکی دان اصل ذات اینجا بمعنی</p></div>
<div class="m2"><p>که در این سر بدانی ذات مولی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>همه ذاتست و بهر نقش پیداست</p></div>
<div class="m2"><p>ولیکن عقل در شین است و شیداست</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>نمود عشق بنگر سوی جانت</p></div>
<div class="m2"><p>که تا گوید یقین راز نهانت</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>غلام عشق شو تا ره نماید</p></div>
<div class="m2"><p>در بسته برویت برگشاید</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>غلام عشق شو تا راز دانی</p></div>
<div class="m2"><p>نمود اوّل اینجاباز دانی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>غلام عشق شو تا شاه گردی</p></div>
<div class="m2"><p>ز ذات کل بکل آگاه گردی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>غلام عشق شو تا جان شوی تو</p></div>
<div class="m2"><p>ز عشق آنگه سوی جانان شوی تو</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>غلام عشقم و شاهی است ما را</p></div>
<div class="m2"><p>غلامانم مه و ماهی است ما را</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چنان بنمایدت روشن در اینجا</p></div>
<div class="m2"><p>که باشد از نی گلشن در اینجا</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>تو زان گلشن در اینجا آمدستی</p></div>
<div class="m2"><p>هم از این گلخنی روشن شدستی</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ترا چون گلشنت گلخن نموداست</p></div>
<div class="m2"><p>ولی بر آتشت افتاده دود است</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>در این گلخن بمانده گلشنی باش</p></div>
<div class="m2"><p>بقدر خویش بی کبرو منی باش</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>در این تاب و تبِ آتش همی سوز</p></div>
<div class="m2"><p>بریز این ریزه و گلشن همی سوز</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چنان در گلخنی فارغ نشسته</p></div>
<div class="m2"><p>دَرِ گلشن بروی خود ببسته</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>فکنده آتش و دودی بگلخن</p></div>
<div class="m2"><p>شده گلشن ز نور نار روشن</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>تو در آن روشنی کرده نظاره</p></div>
<div class="m2"><p>نماند ریزه وانگاهی چه چاره</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بیمرد آتش و آنگه بماند</p></div>
<div class="m2"><p>ز بعدِ اَنْت خاکستر بماند</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>تو چون در گلخن هستی بمانده</p></div>
<div class="m2"><p>خود اندر عین خاکستر نشانده</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>ندیدستی دمی مر گلشن یار</p></div>
<div class="m2"><p>که تا روحی ترا آید پدیدار</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>در این گلخن بماندستی تو مجروح</p></div>
<div class="m2"><p>نداری جز تپش در قوّت روح</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>در این گلخن بماندستی بناچار</p></div>
<div class="m2"><p>شدی از دود گلخن خسته و زار</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>در این گلخن گذر کن همچو مردان</p></div>
<div class="m2"><p>در آن گلشن نگاهی کن چو مردان</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>تو تادر گلخن صورت اسیری</p></div>
<div class="m2"><p>نه بینی آنچه جوئی پس چه چیزی</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>تو در گلخن فتاده زار و رنجور</p></div>
<div class="m2"><p>در آن گلشن نظر کن سر بسر نور</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>ببین تا بازیابی گلشن جان</p></div>
<div class="m2"><p>وز این گلخن برو خود را مرنجان</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>از آن گلشن که گلهایش ستاره</p></div>
<div class="m2"><p>ترا اینجا دمی نامد نظاره</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>نظاره کن سوی گلشن که جانست</p></div>
<div class="m2"><p>همه ارواح و اشباح عیانست</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>که میگوید که ماه و آفتابست</p></div>
<div class="m2"><p>جمال یار جانش پر ز تابست</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>جمال یار از این معنی برونست</p></div>
<div class="m2"><p>نظر کن روی او ای دل که چونست</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>از آن عالم در این عالم فتادست</p></div>
<div class="m2"><p>جمال یار اسمی برگشادست</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>در این گلخن سرای عالم چشم</p></div>
<div class="m2"><p>شود پیدا ز بود و مینهد اسم</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>چو اسم تو در این عالم مسّما</p></div>
<div class="m2"><p>شود بار دگر در بود یکتا</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>از آن عالم در این عالم در آید</p></div>
<div class="m2"><p>بخود چون منع خود را مینماید</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>چو جسم از خاک و آب آمد پدیدار</p></div>
<div class="m2"><p>دگر در باد و آتش ناپدیدار</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>شود گل چون در این گلخن بسوزد</p></div>
<div class="m2"><p>پس آنگه آتشی دیگر فروزد</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>شود جان چون بسوزد سوی گلشن</p></div>
<div class="m2"><p>رود بار دگر در سوی گلشن</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>ز دید مرد کی یابد حیاتی</p></div>
<div class="m2"><p>در آخر باشد او دیدار ذاتی</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>که موجود است آن بر کل اشیا</p></div>
<div class="m2"><p>از او گشتند سر تاسر هویدا</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>در آخر چون شود فانی ضرورت</p></div>
<div class="m2"><p>بر جانان شود جان عین صورت</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>حیات طیبّه آید پدیدار</p></div>
<div class="m2"><p>در آن دم چون شود گل ناپدیدار</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>صور جان گردد و جان سرّ جانان</p></div>
<div class="m2"><p>شود چون قطرهٔ در بحر پنهان</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>حیاتی یابد از دیدار دیگر</p></div>
<div class="m2"><p>بیابد بیشکی اسرار دیگر</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>بمیرد بار دیگر سوی جسم او</p></div>
<div class="m2"><p>حقیقت ذات باشد خود نه اسم او</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>حقیقت باشد او اندر حیات او</p></div>
<div class="m2"><p>که یابد بار دیگر خود حیات او</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>دلا عین حیات جان بدیدی</p></div>
<div class="m2"><p>ولی جان دیدی و جانان ندیدی</p></div></div>