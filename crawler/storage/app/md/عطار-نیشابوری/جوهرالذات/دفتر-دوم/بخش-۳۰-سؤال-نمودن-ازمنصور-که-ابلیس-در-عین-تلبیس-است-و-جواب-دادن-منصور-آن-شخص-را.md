---
title: >-
    بخش ۳۰ - سؤال نمودن ازمنصور که ابلیس در عین تلبیس است و جواب دادن منصور آن شخص را
---
# بخش ۳۰ - سؤال نمودن ازمنصور که ابلیس در عین تلبیس است و جواب دادن منصور آن شخص را

<div class="b" id="bn1"><div class="m1"><p>یکی پرسید از منصور کابلیس</p></div>
<div class="m2"><p>که دائم هست اندر عین تلبیس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدی جُمله از او آمد پدیدار</p></div>
<div class="m2"><p>از این سر باشد اینجا او خبردار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه زشتی عالم زو عیانست</p></div>
<div class="m2"><p>که اینجا بیشکی دون جهانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چگوئی بهر او ای راز دیده</p></div>
<div class="m2"><p>چنان کاینجا توئی او باز دیده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حقیقت میشناسد مر خدا او</p></div>
<div class="m2"><p>مر این مشکل ز بهر حق مراگو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جوابش داد منصور گزیده</p></div>
<div class="m2"><p>که چون ابلیس نبود راز دیده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنان کابلیس اینجا دوست دیدست</p></div>
<div class="m2"><p>حقیقت همچو او دیگر که دیدست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنان کو یافت در حق آشنائی</p></div>
<div class="m2"><p>نداندیافت مطلق روشنائی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو او دیگر کسی در دور عالم</p></div>
<div class="m2"><p>که او دیدست حق اینجا دمادم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یقین او دید حق اینجا دمادم</p></div>
<div class="m2"><p>که از وی هست چندین غم دمادم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حقیقت اوست اینجا راز محبوب</p></div>
<div class="m2"><p>اگرچه طالبست او دیده مطلوب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حقیقت عاشق چابک سوارست</p></div>
<div class="m2"><p>که او را در جهان این یادگار است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تمامت انبیا دیدست اینجا</p></div>
<div class="m2"><p>وز ایشان راز بشنیدست اینجا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تمامت انبیا کرده سؤال او</p></div>
<div class="m2"><p>بهر وجهی در اینجا حسب حال او</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بپرسیدست از صاحب کمالان</p></div>
<div class="m2"><p>حقیقت راز خود در سرّ جانان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همه با او در اینجا راز گفتند</p></div>
<div class="m2"><p>ز دید خویش با اوباز گفتند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چنان کو دید خودداند در اسرار</p></div>
<div class="m2"><p>کسی زونیست اینجاگه خبردار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چنان کو دیدخود دیدست در خلق</p></div>
<div class="m2"><p>گهی در عین زنّار است و گه دلق</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گهی اندر خراباتست ساکن</p></div>
<div class="m2"><p>گهی اندر مناجاتست ایمن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گهی در کعبه بر درگه ستادست</p></div>
<div class="m2"><p>گهی بر خاک پای شه فتادست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گهی در میکده او دُرد نوشست</p></div>
<div class="m2"><p>گهی گویا و گه گاهی خموشست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گهی از بیم جانانست حیران</p></div>
<div class="m2"><p>گهی در پرده جانانست پنهان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گهی در کافری بستست زنّار</p></div>
<div class="m2"><p>گهی در عین اسلامست در کار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گهی درخلوت تن در مقیم است</p></div>
<div class="m2"><p>گهی با هر کس اینجاگه سلیم است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گه یاندازد او ذرّات از راه</p></div>
<div class="m2"><p>گهی گم کرده آرد بر سر راه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گهی راهت نماید گه کند گم</p></div>
<div class="m2"><p>ترا چون قطرهٔ در عین قلزم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گهی اندر حقیقت ره نماید</p></div>
<div class="m2"><p>گهی اندر طریقت ره گشاید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گهی در عالم تحقیق باشد</p></div>
<div class="m2"><p>گهی کافر گهی زندیق باشد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از آن خوانندش اینجاگاه ابلیس</p></div>
<div class="m2"><p>که باشد دائماً در عین تلبیس</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نه دائم اندر اینجا برقرار است</p></div>
<div class="m2"><p>بهر سیرت عجب ناپایدار است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نه یک رنگست اینجا در دو رنگست</p></div>
<div class="m2"><p>از آن نزدیک هرکس خوار وننگست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نه یک رنگست دائم در دوئی اوست</p></div>
<div class="m2"><p>چنین بودن بر عاقل نه نیکو است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>حقیقت اصل او از عین نارست</p></div>
<div class="m2"><p>از آن پیوسته نزد عقل خوارست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ولیکن من از او یک چیز دانم</p></div>
<div class="m2"><p>از او اینجایگه رمزی برانم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>حقیقت دیدمش در عشقبازی</p></div>
<div class="m2"><p>که این رازم نماید پیشبازی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>که در لعنت چنان او استوار است</p></div>
<div class="m2"><p>که دائم اندر این سر پایدار است</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چنان در عشق محبوس و اسیرست</p></div>
<div class="m2"><p>که اندر طوق لعنت بی نظیر است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چنان کاندر جهان او خوار آمد</p></div>
<div class="m2"><p>یقین در عشق برخوردار آمد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تمامت انبیا از لعنت حق</p></div>
<div class="m2"><p>گذرکردند و جستند رحمت حق</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تمامت انبیا از سرّ لعنت</p></div>
<div class="m2"><p>شدند ترسان همی در عین حضرت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تمامتانبیا ترسان از ایناند</p></div>
<div class="m2"><p>که ایشان اندر این ره راز بینند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>همه ترسان شدند از لعنت یار</p></div>
<div class="m2"><p>تبه بردند اندر حضرت یار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همه ترسان شدند اینجایگه کُل</p></div>
<div class="m2"><p>نیارستند یک ذرّه مر این ذل</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>حقیقت بار شه اینجا کشیدن</p></div>
<div class="m2"><p>زهی ابلیس این تاوان کشیدن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ترا زیبد که بار آن کشی تو</p></div>
<div class="m2"><p>در اینجاگه رقم برجان کشی تو</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ترا زیبد که اینجا طوق لعنت</p></div>
<div class="m2"><p>نهی بر گردن اندر شور حضرت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ترا زیبد که بار آن کشیدی</p></div>
<div class="m2"><p>حقیقت زهر جان جان چشیدی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>دگر گفتاکه ابلیس است ملعون</p></div>
<div class="m2"><p>که اینجا دید راز سرّ بیچون</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بیک سجده که اینجاگه نکرداست</p></div>
<div class="m2"><p>نظر کن تا که چندین زخم خورداست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بیک سجده که در اوّل نکرد او</p></div>
<div class="m2"><p>حقیقت تا قیامت زخم خورد او</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>حقیقت من زنسل آدممم باز</p></div>
<div class="m2"><p>که منصورم در اینجا صاحب راز</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>در آن لحظه که آدم گشت پیدا</p></div>
<div class="m2"><p>ز دید جزو و کل در خود هویدا</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>حقیقت دزد را هم بود ابلیس</p></div>
<div class="m2"><p>نظر کردم در اینجاگه بتلبیس</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چو گنج آدم اینجا مینهادم</p></div>
<div class="m2"><p>حقیقت این لعین را در گشادم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>درون جسم آمد او نهانی</p></div>
<div class="m2"><p>نظر میکرد سرّ کن فکانی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>یقین گنج آدم یافت اینجا</p></div>
<div class="m2"><p>حقیقت در نهان دم یافت اینجا</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بدید او سر بیچون و چگونم</p></div>
<div class="m2"><p>حقیقت راه برد از اندرونم</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>حقیقت جان بدید ودل عیان یافت</p></div>
<div class="m2"><p>درون ما همه راز نهان یافت</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>حقیقت دزد گنج من شد ازدید</p></div>
<div class="m2"><p>نظر میکرد اینجا سرّ توحید</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>عیان گنج من کرد او نظاره</p></div>
<div class="m2"><p>حقیقت دید دیدم من چه چاره</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>مرا چون گنج بنمودم در اینجا</p></div>
<div class="m2"><p>حقیقت سجده فرمودم در اینجا</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ملایک سجدهٔ آدم نمودند</p></div>
<div class="m2"><p>ز ذات ما یقین واقف نبودند</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>همه سجده بما کرده ملایک</p></div>
<div class="m2"><p>که آدم زبدهٔ کل ممالک</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چو سرّ دیدند اینجا سجده کردند</p></div>
<div class="m2"><p>ملایک جملگی این گوی بردند</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>حقیقت سجده اینجاگه نکرد او</p></div>
<div class="m2"><p>ز من اینجایگه او زخم خورد او</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>تمامت انبیا اینجای ظاهر</p></div>
<div class="m2"><p>همه در قالب آن پاک ناظر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>نکرد این سجده بیرون شد ز رحمت</p></div>
<div class="m2"><p>حقیقت یافت آن دم طوق لعنت</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چو نافرمانی ما کرد گردون</p></div>
<div class="m2"><p>یقین از ذات ماافتاد بیرون</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>اگراو این زمان مردود راهست</p></div>
<div class="m2"><p>حقیقت آخر اینجا عذرخواهست</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>حقیقت مانده اینجا راه بین است</p></div>
<div class="m2"><p>مر او را لعنتش تا یوم دین است</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>حقیقت لعنتش از ما است رحمت</p></div>
<div class="m2"><p>ولیکن رحمت آمد به ز لعنت</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>یقین میدان که رحمت آخر کار</p></div>
<div class="m2"><p>بخواهد کرد بر جمله بیکبار</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>یقین میدان که لعنت هم از او بود</p></div>
<div class="m2"><p>که ابلیسست ز آتش نه نکو بود</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ز خود بینی همی در لعنت افتاد</p></div>
<div class="m2"><p>ز قربت دور شد بی رحمت افتاد</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>یقین هر کس که دور ازدوست باشد</p></div>
<div class="m2"><p>نه مغزی باشد او کل پوست باشد</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>حقیقت قصّهٔ ابلیس این است</p></div>
<div class="m2"><p>که اینجاگاه بیشک راز بین است</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>اگرچه سالک واصل نموده</p></div>
<div class="m2"><p>درِ بسته بیک ره برگشوده</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ز ابلیسی کنون بیرون شدستی</p></div>
<div class="m2"><p>حقیقت ذات کل بیچون شدستی</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>حقیقت اینهمه از بهر آن بود</p></div>
<div class="m2"><p>که تا ابلیس آخر در عیان بود</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>که او در عین نافرمانی دوست</p></div>
<div class="m2"><p>حقیقت بازماند اندر سوی پوست</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>تو گر مرد رهی مانند ابلیس</p></div>
<div class="m2"><p>مباش اینجایگه درمکرو تلبیس</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>برون کن دیو ابلیس ازدماغت</p></div>
<div class="m2"><p>منوّر کن بنور کل چراغت</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>یقین دنیا است ابلیس این بدان تو</p></div>
<div class="m2"><p>گذر کن بیشکی زین خاکدان تو</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>هر آنچه فکر بد باشد طبیعت</p></div>
<div class="m2"><p>تو مر ابلیس دان بیشک حقیقت</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>برون کن فکر بد از خاطر خویش</p></div>
<div class="m2"><p>دو روزی باش اینجاناظر خویش</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>دو روزی کاندر این دنیای دونی</p></div>
<div class="m2"><p>حقیقت ذرّهها را رهنمونی</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>نه اندیشد که رحمت کن نه لعنت</p></div>
<div class="m2"><p>حقیقت باش اندر عین قربت</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>تو اندیشه ز خود کن تا که چونی</p></div>
<div class="m2"><p>که تو از هر دو عالم مر فزونی</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>ببین آنکس که اینجا سجدهٔ دوست</p></div>
<div class="m2"><p>نکردست این در اینجا لعنت دوست</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>ترا سجده نکرد ابلیس نادان</p></div>
<div class="m2"><p>ز لعنت گشت پرتلبیس نادان</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>ترا سجده نکرد و طوق لعنت</p></div>
<div class="m2"><p>بگردان یافت بیرون شد زرحمت</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>ترا سجده نکرد ای جوهر کل</p></div>
<div class="m2"><p>چنین افتاده شد در عین این ذل</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>به بین تا تو چه چیزی ای دل و جان</p></div>
<div class="m2"><p>که بنمودست رویت جان جانان</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>به بین تا تو چگونه آشنائی</p></div>
<div class="m2"><p>که در اعیان تو دیدارِ خدائی</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>ببین ذات خدا در خویش موجود</p></div>
<div class="m2"><p>ترا اسرار کل در پیش معبود</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>ببین ذات خدا در خود نمودار</p></div>
<div class="m2"><p>برون شو این زمان ز ابلیس و پندار</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>ببین ذات خدا در خود نه ابلیس</p></div>
<div class="m2"><p>میندیش از ریا و مکر و تلبیس</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>ببین ذات خدا ابلیس بگذار</p></div>
<div class="m2"><p>اگرمرد رهی تلبیس بگذار</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>ببین ذات خدا ای صادق کل</p></div>
<div class="m2"><p>اگر هستی حقیقت عاشق کل</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>ببین ذات خدا اینجا و بشناس</p></div>
<div class="m2"><p>رها کن مکر و زرق اینجا و وسواس</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>که تو برتر ز عقل و عکس ناری</p></div>
<div class="m2"><p>اگر اینجا حقیقت پایداری</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>ترا از ذات خود موجود کردست</p></div>
<div class="m2"><p>حقیقت نقش خود معبود کردست</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>ترا از ذات خود کردست پیدا</p></div>
<div class="m2"><p>در این عالم ز عقل کل هویدا</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>ترا از ذات خود پیدا نمودست</p></div>
<div class="m2"><p>ابا تو گفته و ازتو شنودست</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>ترا ازذات خودبنمود بینش</p></div>
<div class="m2"><p>بتو پیداست بودآفرینش</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>تو مغزی این زمان در صورت پوست</p></div>
<div class="m2"><p>حقیقت چون بیابی صورت اوست</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>تو مغزی ز آفرینش رخ نموده</p></div>
<div class="m2"><p>حقیقت خود بخود پاسخ نموده</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>تو مغزی این زمان در عین دنیا</p></div>
<div class="m2"><p>ترا پیدا شده دیدارمولا</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>تو مغزی این زمان اعیان نموده</p></div>
<div class="m2"><p>حقیقت خویش را پنهان نموده</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>تو مغزی این زمان اعیان بدیده</p></div>
<div class="m2"><p>از آن منزل بدین منزل رسیده</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>ز ذاتِ پاکِ او پیدا نمودی</p></div>
<div class="m2"><p>دو روزی نقش در دنیا نمودی</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>ز ذاتِ پاکِ او اعیانِ رازی</p></div>
<div class="m2"><p>تو سیمرغی بصورت شاهبازی</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>اگر شاهت نماید روی اینجا</p></div>
<div class="m2"><p>ورا بینی تو از هر سوی اینجا</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>ترا شاهست اینجا صورت جان</p></div>
<div class="m2"><p>حقیقت چون نمیدانی تو ایشان</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>باسمی لیک جان جانت اینجاست</p></div>
<div class="m2"><p>حقیقت سرّ مر پنهانت اینجاست</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>باسم آدمی تو لیک معنی</p></div>
<div class="m2"><p>چو آخر بنگری دیدار مولی</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>حقیقت در تو دیدارست بنگر</p></div>
<div class="m2"><p>که سر تا پای تو یارست بنگر</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>حقیقت درتودیدار الهست</p></div>
<div class="m2"><p>ز سر تا پای تو دیدار شاهست</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>تو شاهی اندر این عالم فتاده</p></div>
<div class="m2"><p>بصورت سیرت آدم فتاده</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>تو شاهی اندر این عالم یقین شاه</p></div>
<div class="m2"><p>درونِ جانت هم خورشید و هم ماه</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>تو شاهی اندر این دنیا حقیقت</p></div>
<div class="m2"><p>نشسته بر سر تخت شریعت</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>وزیر تست عقل کل ندانی</p></div>
<div class="m2"><p>که بر تخت شریعت کامرانی</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>وزیر تست عقل و پادشاهی</p></div>
<div class="m2"><p>که مشتق گشته از خورشید و ماهی</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>وزیر تست عقل کل درونت</p></div>
<div class="m2"><p>بسوی ذات اینجا رهنمونت</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>وزیر تست عقل اندر ممالک</p></div>
<div class="m2"><p>تو عشقی لیک اندر عشق هالک</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>مقام سالکی درتو بدیدست</p></div>
<div class="m2"><p>حقیقت واصلیات ناپدیدست</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>تو هستی سالک کون و مکانی</p></div>
<div class="m2"><p>که ره در سوی آن کل بازدانی</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>حقیقت عقل کل با تست دریاب</p></div>
<div class="m2"><p>درون خانهٔ از عشق درتاب</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>حقیقت عقل کل همسایهٔ تست</p></div>
<div class="m2"><p>تو خورشیدی و او چون سایهٔ تست</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>تو خورشیدی حقیقت سایه بگذار</p></div>
<div class="m2"><p>تمامت بین و پایه پایه بگذار</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>تو خورشیدی ز ذات کل نموده</p></div>
<div class="m2"><p>حقیقت عقل کل را در ربوده</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>بمعنی و بصورت جان جانها</p></div>
<div class="m2"><p>درون تست اینجاگه نهانها</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>غرض چون پردهٔ در پیش رویست</p></div>
<div class="m2"><p>دردن پرده او در گفتگویست</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>غرض چون پرده بر رویت فتاده</p></div>
<div class="m2"><p>چنین دیدار هر سویت فتاده</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>اگر این پرده بر خیزد ز دیدار</p></div>
<div class="m2"><p>معاینه به بینی خود پدیدار</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>اگر این پرده برخیزد ز رویت</p></div>
<div class="m2"><p>نماند این نفس خود گفتگویت</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>اگر این پرده برخیزد ز اسرار</p></div>
<div class="m2"><p>چه بینی در حقیقت لیس فی الدّار</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>در آن عین فنا در خویش منگر</p></div>
<div class="m2"><p>که یکّی در یکی است پیش منگر</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>یکی داری و در عین دوبینی</p></div>
<div class="m2"><p>مر این اسرار اینجاگه نه بینی</p></div></div>