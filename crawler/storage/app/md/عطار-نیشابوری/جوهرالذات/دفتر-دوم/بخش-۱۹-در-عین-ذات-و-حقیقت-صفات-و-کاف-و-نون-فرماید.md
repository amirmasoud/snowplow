---
title: >-
    بخش ۱۹ - در عین ذات و حقیقت صفات و کاف و نون فرماید
---
# بخش ۱۹ - در عین ذات و حقیقت صفات و کاف و نون فرماید

<div class="b" id="bn1"><div class="m1"><p>ز اصل کاف و نون گشتی تو پیدا</p></div>
<div class="m2"><p>در این روی زمین گشتی هویدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آنجا آمدی بی جسم و بی جان</p></div>
<div class="m2"><p>در اینجا فاش گشتی، راز خود دان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو نور جوهر ذات و صفاتی</p></div>
<div class="m2"><p>که از حضرت کنون عین حیاتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر خورشید لاهوتی بیابی</p></div>
<div class="m2"><p>حقیقت مرغ ناسوتی بیابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمین و آسمانها در تو پیداست</p></div>
<div class="m2"><p>همه اشیا درون تو هویداست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نباشد هیچ تا آن مر ترا هست</p></div>
<div class="m2"><p>ولی در یافتن آن کی دهد دست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که هر چیزی که بینی خویش یابی</p></div>
<div class="m2"><p>حقیقت جزو و کل در پیش یابی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر خورشیدی بینی هم تو آنی</p></div>
<div class="m2"><p>که در صورت ترا بنمود جانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو خورشیدی ز برج ذات گردان</p></div>
<div class="m2"><p>حقیقت دروجود خویش گردان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بتو پیداست اینجا نور خورشید</p></div>
<div class="m2"><p>وگرنه ذات خواهی بود جاوید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بتو پیداست اینجا صورت ماه</p></div>
<div class="m2"><p>زده از بهر تو این هفت خرگاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بتو پیداست اینجا مشتری بین</p></div>
<div class="m2"><p>ترا صد ماه زهره مشتری بین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بتو پیداست اینجا نور زهره</p></div>
<div class="m2"><p>توئی در عین اشیا مانده شهره</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بتو پیداست اینجا جمله انجم</p></div>
<div class="m2"><p>حقیقت جملگی در نور تو گم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بتو پیداست اینجا چرخ و افلاک</p></div>
<div class="m2"><p>ز بهر تست گردان جوهر پاک</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بتو پیداست اینجا نور آتش</p></div>
<div class="m2"><p>توئی آتش ولیکن گشته سرکش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بتو پیداست اینجا مخزن باد</p></div>
<div class="m2"><p>ز تو پیداست اینجا نور اضداد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بتو پیداست آب اینجا روانه</p></div>
<div class="m2"><p>زند آتش بسوی او زبانه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بتو پیداست اینجا خاک بنگر</p></div>
<div class="m2"><p>تو خود را سر صنع پاک بنگر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بتو پیداست اینجا کوه و دریا</p></div>
<div class="m2"><p>کشیده جوهرت اندر ثریّا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بتو پیداست فرش و عرش و کرسی</p></div>
<div class="m2"><p>قلم با لوح و جنّت می چه پرسی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بتو پیداست نور دل حقیقت</p></div>
<div class="m2"><p>فکنده پرتوی سوی طبیعت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بتو پیداست نور نور جانان</p></div>
<div class="m2"><p>درون جان و دل او مانده پنهان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همه او بین که جز او کس ندید است</p></div>
<div class="m2"><p>از او پیدا همه او ناپدید است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همه او بین و ذات اوست جمله</p></div>
<div class="m2"><p>یقین اشیا صفات اوست جمله</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دوئی بگذار و همچون او یکی باش</p></div>
<div class="m2"><p>توئی نقش و درونت اوست نقاش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یکی بنگر که این نقاش بیچون</p></div>
<div class="m2"><p>ز خود کردست پیدا بی چه و چون</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یکی بنگر که این نقاش کردست</p></div>
<div class="m2"><p>خودی خود یقینِ هفت پرده است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یکی بین و دوئی اینجا رها کن</p></div>
<div class="m2"><p>چو منصورت وجود خود رها کن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>یکی بین و چو دیدی راز اوّل</p></div>
<div class="m2"><p>مشو بر هر صفت دیگر مبدّل</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>یکی بین و یقین را دار در پیش</p></div>
<div class="m2"><p>دگر اینجایگه کافر میندیش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>یکی بین گبر و ترسا و مسلمان</p></div>
<div class="m2"><p>که بنمودست از خود جمله جانان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یکی بین بت پرست و اهل زنّار</p></div>
<div class="m2"><p>همه از او و او را کل طلبکار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>یکی بین کین همه جمله یقین اوست</p></div>
<div class="m2"><p>اگر نه این چنین بینی نه نیکوست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>یکی دریاب و در یکی احد شو</p></div>
<div class="m2"><p>حقیقت فارغت از نیک و بد شو</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یکی بین و دم اینجا از یکی زن</p></div>
<div class="m2"><p>اگر نه این چنین بینی توئی زن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>یکی بین و وجود انبیا باش</p></div>
<div class="m2"><p>حقیقت هم لقای اولیا باش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مسلمانی رها کن گرد کافر</p></div>
<div class="m2"><p>بگو تا چند باشی در پی شر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>حقیقت کافر فقر و فنا شو</p></div>
<div class="m2"><p>تو در یکی بکل عین بقا شو</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تو در یکی قدم زن همچو مردان</p></div>
<div class="m2"><p>رخت را از دوئی اینجا بگردان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تو در یکی قدم زن اصل آسا</p></div>
<div class="m2"><p>کلش رنج و بلا و خوش بیاسا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تو در یکی قدم زن در تولّا</p></div>
<div class="m2"><p>همه لابین و در لا گرد الا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تو در یکی قدم زن همچو منصور</p></div>
<div class="m2"><p>یکی نزدیک بین و بس دوئی دور</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تو در یکی قدم زن در اناالحق</p></div>
<div class="m2"><p>انالحق گوی کاین است سرّ مطلق</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تو در یکی قدم زن سالک پیر</p></div>
<div class="m2"><p>مکن دیگر تو در هر راز تدبیر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>تو در یکی قدم زن بی نمودار</p></div>
<div class="m2"><p>حجابت جسم دان آن نیز بردار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تو چون منصور اینجا راز کل گوی</p></div>
<div class="m2"><p>وگر نادان شدستی راز کل جوی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>در این صورت نظر کن نفخهٔ ذات</p></div>
<div class="m2"><p>یقین اللّه بین در جمله ذرّات</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تو بیچون آمدی، چون این بدانی؟</p></div>
<div class="m2"><p>مگر آن دم که باشی در معانی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تو بیچون آمدی از پرده بیرون</p></div>
<div class="m2"><p>نمودی روی خود را بی چه و چون</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تو بیچون آمدی از هفت پرده</p></div>
<div class="m2"><p>حقیقت راز خود را پی نبرده</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تو بیچون آمدی در جمله ذرّات</p></div>
<div class="m2"><p>حقیقت هست اینجا عین آن ذات</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تو بیچون آمدی در روی عالم</p></div>
<div class="m2"><p>شدی فارغ عجب در کوی عالم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تو بیچون آمدی ای سرّ بیچون</p></div>
<div class="m2"><p>تو لیلی هستی و خود گشته مجنون</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تو بیچون آمدی در هر چه دیدی</p></div>
<div class="m2"><p>ولی اینجا کمال خود ندیدی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>تو بیچون آمدی در عین عالم</p></div>
<div class="m2"><p>نمودی سر خود در نقش آدم</p></div></div>