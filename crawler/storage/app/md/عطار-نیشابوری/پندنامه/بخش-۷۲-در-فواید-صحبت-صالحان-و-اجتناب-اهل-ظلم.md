---
title: >-
    بخش ۷۲ - در فواید صحبت صالحان و اجتناب اهل ظلم
---
# بخش ۷۲ - در فواید صحبت صالحان و اجتناب اهل ظلم

<div class="b" id="bn1"><div class="m1"><p>همنشین صالحان باش ای پسر</p></div>
<div class="m2"><p>هم جدا از فاسقان باش ای پسر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانب ظالم مکن میل از عزیز</p></div>
<div class="m2"><p>ورکنی گردی از آن خیل ای عزیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رو ز اهل ظلم بگریز ای فقیر</p></div>
<div class="m2"><p>تا نسوزی ز آتش تیز ای فقیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صحبت ظالم بسان آتشست</p></div>
<div class="m2"><p>زانکه خلق آزار و تند و سرکشست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از حضور صالحان صالح شوی</p></div>
<div class="m2"><p>ور نشینی با بدان طالح شوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که او با صالحان همدم شود</p></div>
<div class="m2"><p>در حریم خاص حق محرم شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای پسر مگذار راه شرع را</p></div>
<div class="m2"><p>اصل یابی گر بگیری فرع را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از شریعت گر نهی بیرون قدم</p></div>
<div class="m2"><p>در ضلالت افتی و رنج و الم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که در راه ضلالت می‌رود</p></div>
<div class="m2"><p>از جهالت با بطالت می‌رود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حق طلب و زکار باطل دورباش</p></div>
<div class="m2"><p>در سخا و مردمی مشهور باش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر که نگزیند صراط مستقیم</p></div>
<div class="m2"><p>در عذاب آخرت ماند مقیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در ره شیطان منه گام ای اخی</p></div>
<div class="m2"><p>تا نگردی خوار و بدنام ای اخی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر که در راه حقیقت سالک است</p></div>
<div class="m2"><p>روز و شب خایف ز قهر مالک است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر خلاف نفس کن کار ای پسر</p></div>
<div class="m2"><p>تا نیفتی خوار در نار سقر</p></div></div>