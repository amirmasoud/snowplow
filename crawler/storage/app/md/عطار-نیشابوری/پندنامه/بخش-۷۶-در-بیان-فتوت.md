---
title: >-
    بخش ۷۶ - در بیان فتوت
---
# بخش ۷۶ - در بیان فتوت

<div class="b" id="bn1"><div class="m1"><p>چیست مردی ای پسر نیکو بدان</p></div>
<div class="m2"><p>اولا ترسیدن از حق در نهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عذر خواهان مرد پیش از معصیت</p></div>
<div class="m2"><p>باشدش طاعات بیش از معصیت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه کار نیک مردان می‌کند</p></div>
<div class="m2"><p>با ضعیفان لطف و احسان می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که او باشد ز مردان خدا</p></div>
<div class="m2"><p>باشد اندر تنگ دستی با سخا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای پسر در صحبت مردان درآی</p></div>
<div class="m2"><p>تا نظرها یابی از فضل خدای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که از مردان حق دارد نشان</p></div>
<div class="m2"><p>نگذراند عیب دشمن بر زبان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون نخواهد مرد حق خصمان هلاک</p></div>
<div class="m2"><p>از غم مردم شود اندوه ناک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می‌نجوید مرد انصاف از کسی</p></div>
<div class="m2"><p>گر رسد ظلم و جفا با وی بسی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که پا اندر ره مردان نهاد</p></div>
<div class="m2"><p>کی رود هرگز بدنبال مراد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای پسر ترک مراد خویش گیر</p></div>
<div class="m2"><p>وانگهی راه سلامت پیش گیر</p></div></div>