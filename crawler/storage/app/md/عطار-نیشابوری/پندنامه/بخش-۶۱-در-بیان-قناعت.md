---
title: >-
    بخش ۶۱ - در بیان قناعت
---
# بخش ۶۱ - در بیان قناعت

<div class="b" id="bn1"><div class="m1"><p>با قناعت ساز دایم ای پسر</p></div>
<div class="m2"><p>گرچه هیچ از فقر نبود تلخ‌تر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر سحر برخیز و استغفار کن</p></div>
<div class="m2"><p>فرصتی اکنون که داری کار کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همنشین خویش را غیبت مکن</p></div>
<div class="m2"><p>غیر شیطان بر کسی لعنت مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون شود هر روز در عالم جدید</p></div>
<div class="m2"><p>از گناهان تو به می‌باید گزید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کرا ترسی نباشد از خدا</p></div>
<div class="m2"><p>حق بترساند زهر چیزی ورا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا توانی حاجت مسکین برآر</p></div>
<div class="m2"><p>تا برآرد حاجتت را کردگار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هست مالت جمله در کف عاریت</p></div>
<div class="m2"><p>گر بماند از تو باشد زاریت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاریت را باز می‌باید سپرد</p></div>
<div class="m2"><p>هیچ کس دیدی که زر با خود ببرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حاصل از دنیا چه باشد ای امین</p></div>
<div class="m2"><p>نه گزی کرباس و دو سه گز زمین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرچه دادی در ره حق آن تست</p></div>
<div class="m2"><p>هر که با اندک ز حق راضی شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر که با اندک ز حق راضی شود</p></div>
<div class="m2"><p>حاجت او را خدا قاضی شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هست دنیا بر مثال جیفهٔ</p></div>
<div class="m2"><p>بگذر از وی گر تو خو مردانهٔ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هست دنیا بر مثال قطرهٔ</p></div>
<div class="m2"><p>بگذر از وی زانکه داری بهرهٔ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر که سازد بر سر پل خانهٔ</p></div>
<div class="m2"><p>نیست عاقل او بود دیوانهٔ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از خدا نبود روا جستن غنا</p></div>
<div class="m2"><p>هست مؤمن را غنا رنج و عنا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فقر و درویشی شفای مؤمن است</p></div>
<div class="m2"><p>زانکه اندر وی صفای مؤمن است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مال و اولادت بمعنی دشمنند</p></div>
<div class="m2"><p>گرچه نزدیک تو چشم روشنند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>انما اولادکم را یاد گیر</p></div>
<div class="m2"><p>مال و ملک این جهان را یادگیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرد ره را بود دنیا سود نیست</p></div>
<div class="m2"><p>هرگزش اندیشهٔ نابود نیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر کرا از صدق دل صافی بود</p></div>
<div class="m2"><p>خرقهٔ و لقمهٔ کافی بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آنکه در بند زیادت می‌شود</p></div>
<div class="m2"><p>دور از اهل سعادت می‌شود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بندگان حق چو جان را باختند</p></div>
<div class="m2"><p>اسب همت تا ثریا تاختند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا نبازی در ره حق آنچه هست</p></div>
<div class="m2"><p>آنچه می‌باید کجا آید بدست</p></div></div>