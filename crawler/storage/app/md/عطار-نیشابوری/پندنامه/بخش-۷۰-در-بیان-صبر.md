---
title: >-
    بخش ۷۰ - در بیان صبر
---
# بخش ۷۰ - در بیان صبر

<div class="b" id="bn1"><div class="m1"><p>تا شوی در روزگار از صابران</p></div>
<div class="m2"><p>رو مکن از دیدن سختی گران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی خود گر ترش سازی از بلا</p></div>
<div class="m2"><p>خویش را از صابران مشمار هلا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در بلا وقتی که صابر نیستی</p></div>
<div class="m2"><p>نزد اهل صدق شاکر نیستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی شکایت صبر تو باشد جمیل</p></div>
<div class="m2"><p>با کسی کم کن شکایت ای خلیل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نباشد فخر از درویشیت</p></div>
<div class="m2"><p>کی باهل فقر باشد خویشیت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر همه جنبش بفرمان باشدت</p></div>
<div class="m2"><p>حرمت از خدمت فراوان باشدت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنده از خدمت بعقبی می‌رسد</p></div>
<div class="m2"><p>لیکن از حرمت بمولی می‌رسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حرمتت در خدمت آرام دلست</p></div>
<div class="m2"><p>هر که خدمت کرد مرد مقبلست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر نگردی ای پسر گرد خلاف</p></div>
<div class="m2"><p>آنگهی زیبد ترا در صبر لاف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر همی داری فرح را انتظار</p></div>
<div class="m2"><p>در بلا نبود بصیرت هیچ کار</p></div></div>