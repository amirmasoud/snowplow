---
title: >-
    بخش ۳۰ - در چارچیز که کم بقا دارد
---
# بخش ۳۰ - در چارچیز که کم بقا دارد

<div class="b" id="bn1"><div class="m1"><p>چارچیز ای خواجه کم دارد بقا</p></div>
<div class="m2"><p>گوش دار ای مومن نیکو لقا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جور سلطان را بقا کمتر بود</p></div>
<div class="m2"><p>پس عتاب دوستان خوشتر بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیگر آن مهری که باشد از زنان</p></div>
<div class="m2"><p>بی بقا چون صحبت ناجنس دان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با رعیت چون کند سلطان ستم</p></div>
<div class="m2"><p>مرورا باشد بقا در ملک کم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر ترا از دوستان آید عتاب</p></div>
<div class="m2"><p>کم بقا دارد چو خط بر روی آب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرچه باشد زن زمانی مهربان</p></div>
<div class="m2"><p>چون کم آید بهرهٔ بگشاید زبان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون بناجنسان نشیند آدمی</p></div>
<div class="m2"><p>کمترک بیند از ایشان همدمی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زاغ چون فارغ زبوی گل شود</p></div>
<div class="m2"><p>نفرتش از صحبت بلبل شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صحبت ناجنس جانگاهی بود</p></div>
<div class="m2"><p>جمله را زین حال آگاهی بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون ترا ناجنس آید در نظر</p></div>
<div class="m2"><p>ای پسر چون باد از وی درگذر</p></div></div>