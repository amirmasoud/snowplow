---
title: >-
    بخش ۵۸ - در بیان علامات شقی
---
# بخش ۵۸ - در بیان علامات شقی

<div class="b" id="bn1"><div class="m1"><p>هست ظاهر سه علامت در شقی</p></div>
<div class="m2"><p>می‌خورد دایم حرام از احمقی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌طهارت باشد و بی گاه خیز</p></div>
<div class="m2"><p>هم از اهل علم باشد درگریز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای پسر مگریز از اهل علوم</p></div>
<div class="m2"><p>تا نسوزد مر ترا نار سموم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا توانی هیچ کس را بدمگوی</p></div>
<div class="m2"><p>پیش مردم هم ز باب خود مگوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>معرفت داری کره بر زر مبند</p></div>
<div class="m2"><p>چون رسد مهمان برویش درمبند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با طهارت باش و پاکی پیشه کن</p></div>
<div class="m2"><p>وز عذاب کور نیز اندیشه کن</p></div></div>