---
title: >-
    بخش ۳۶ - در بیان چارچیز که از خطاهاست
---
# بخش ۳۶ - در بیان چارچیز که از خطاهاست

<div class="b" id="bn1"><div class="m1"><p>چارچیز است از خطاها ای پسر</p></div>
<div class="m2"><p>گوش دارش با تو گویم سر بسر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اول از زن داشتن چشم وفا</p></div>
<div class="m2"><p>ساده دل را بس خطا باشد خطا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ایمنی از بد خطای دیگرست</p></div>
<div class="m2"><p>صحبت صبیان ازینها بدترست</p></div></div>