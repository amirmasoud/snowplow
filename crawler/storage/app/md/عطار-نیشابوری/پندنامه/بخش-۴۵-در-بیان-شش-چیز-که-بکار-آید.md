---
title: >-
    بخش ۴۵ - در بیان شش چیز که بکار آید
---
# بخش ۴۵ - در بیان شش چیز که بکار آید

<div class="b" id="bn1"><div class="m1"><p>در جهان شش چیز می‌آید بکار</p></div>
<div class="m2"><p>اولا باری طعام خوشگوار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش بود یار موافق در جهان</p></div>
<div class="m2"><p>باز مخدومی که باشد مهربان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر سخن کان راست گویی و درست</p></div>
<div class="m2"><p>به ز دنیا زانکه در وی نفع تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه ارزانست عالم در بهاش</p></div>
<div class="m2"><p>عقل کامل دان وزان خرسند باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دشمن حق را نباید داشت دوست</p></div>
<div class="m2"><p>بازگشت جمله چون آخر بدوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عیب کس با او نمی‌باید نمود</p></div>
<div class="m2"><p>زانکه نبود هیچ لحمی بی غدود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از خدا خواه هرچه خواهی ای پسر</p></div>
<div class="m2"><p>نیست در دست خلایق نفع و ضرر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بندگان را نیست ناصر جز اله</p></div>
<div class="m2"><p>یاری از حق خواه و از غیرش مخواه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنکه از قهر خدا ترسد بسی</p></div>
<div class="m2"><p>بی‌گمان می‌ترسد از وی هر کسی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از بدی گفتن زبان را هر که بست</p></div>
<div class="m2"><p>کرد شیطان لعین را زیر دست</p></div></div>