---
title: >-
    بخش ۱۶ - در صفت ریاضت نفس و ترک دنیا
---
# بخش ۱۶ - در صفت ریاضت نفس و ترک دنیا

<div class="b" id="bn1"><div class="m1"><p>گر همی خواهی که گردی سر بلند</p></div>
<div class="m2"><p>ای پسر بر خود در راحت ببند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که بربست او در راحت تمام</p></div>
<div class="m2"><p>باز شد بر وی در دار السلام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غیر حق را هر که خواهد ای پسر</p></div>
<div class="m2"><p>کیست در عالم ازو گمراه تر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای برادر ترک عز و جاه کن</p></div>
<div class="m2"><p>خویش را شایسته درگاه کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوار گردد هر که گردد جته جوی</p></div>
<div class="m2"><p>ای برادر قرب این درگاه جوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عز و جاهت سوی پستی می‌کشد</p></div>
<div class="m2"><p>مرا ترا بر تن پرستی می‌کشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نفس در ترک هوا مسکین بود</p></div>
<div class="m2"><p>گوشمال نفس نادان این بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون دلت بر یاد حق ایمن بود</p></div>
<div class="m2"><p>نفسک اماره هم ساکن بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که او را تکیه بر صانع بود</p></div>
<div class="m2"><p>در جهان با لقمهٔ قانع بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اکتفا بر روزی هر روزه کن</p></div>
<div class="m2"><p>گر نداری از خدا دریوزه کن</p></div></div>