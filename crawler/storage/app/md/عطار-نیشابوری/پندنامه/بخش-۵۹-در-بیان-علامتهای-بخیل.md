---
title: >-
    بخش ۵۹ - در بیان علامتهای بخیل
---
# بخش ۵۹ - در بیان علامتهای بخیل

<div class="b" id="bn1"><div class="m1"><p>سه علامت ظاهر آمد در بخیل</p></div>
<div class="m2"><p>با تو گویم یاد گیرش ای خلیل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اولا از سایلان ترسان بود</p></div>
<div class="m2"><p>وز بلای جوع هم لرزان بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون رسد در ره بخویش و آشنا</p></div>
<div class="m2"><p>بگذرد چون باد و گوید مرحبا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نبود از مالش کسی را فایده</p></div>
<div class="m2"><p>کم رسد با کس ز خوانش مایده</p></div></div>