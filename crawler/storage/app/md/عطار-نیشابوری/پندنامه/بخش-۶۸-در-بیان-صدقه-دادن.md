---
title: >-
    بخش ۶۸ - در بیان صدقه دادن
---
# بخش ۶۸ - در بیان صدقه دادن

<div class="b" id="bn1"><div class="m1"><p>گر کنی خیری بدست خویش کن</p></div>
<div class="m2"><p>خیر خود را وقف هر درویش کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک درم کان را بدست خود دهند</p></div>
<div class="m2"><p>به بود زان کز پی او صددهند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر ببخشی خود یکی خرمای تر</p></div>
<div class="m2"><p>بهتر از بعد تو صد مثقال زر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر چه بخشیدی مکن با او رجوع</p></div>
<div class="m2"><p>گر ز پا افتادهٔ از دست رجوع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این بدان ماند که شخصی قی کند</p></div>
<div class="m2"><p>باز میل خوردن آن کی کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با پسر گر چیزکی بخشد پدر</p></div>
<div class="m2"><p>می‌رسد گر باز گیرد از پسر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای پسر با مال و زر شادی مجوی</p></div>
<div class="m2"><p>آنچه کس را دادهٔ دیگر مگوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شادی دنیا سراسر غم بود</p></div>
<div class="m2"><p>سور او را در عقب ماتم بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>امر لا تفرح ز دنیا گوش دار</p></div>
<div class="m2"><p>جای شادی نیست دنیا هوش دار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شادمان را ندارد دوست حق</p></div>
<div class="m2"><p>این سخن دارم ز استادان سبق</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کر فرح داری ز فضل حق رواست</p></div>
<div class="m2"><p>لیک از دنیا فرح جستن خطاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای پسر با محنت و غم خوی کن</p></div>
<div class="m2"><p>روی دل را جانب دلجوی کن</p></div></div>