---
title: >-
    بخش ۳۳ - در صفت چار چیز که از چار چیز دیگر می‌‬آید
---
# بخش ۳۳ - در صفت چار چیز که از چار چیز دیگر می‌‬آید

<div class="b" id="bn1"><div class="m1"><p>حاصل آید چارچیز از چارچیز</p></div>
<div class="m2"><p>یاد دار از این نکته از من ای عزیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خامشی را هر که سازد پیشهٔ</p></div>
<div class="m2"><p>در جهان نبود ز کس اندیشهٔ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از سخاوت مرد یابد سروری</p></div>
<div class="m2"><p>شکر نعمت را دهد افزون‌تری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر سلامت بایدت خاموش باش</p></div>
<div class="m2"><p>گشت ایمن هر که نیکی کرد فاش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که او ساکت شد و خاموش کرد</p></div>
<div class="m2"><p>از سلامت کسوتی بر دوش کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر همی خواهی که باشی در امان</p></div>
<div class="m2"><p>رو نکویی کن تو با خلق جهان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر کرا عادت شود جود و کرم</p></div>
<div class="m2"><p>در میان خلق گردد محترم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که کار نیک یا بد می‌کند</p></div>
<div class="m2"><p>آن همه می‌دان که با خود می‌کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای برادر بندهٔ معبود باش</p></div>
<div class="m2"><p>تا توانی با سخا و جود باش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باش از بخل بخیلان بر حذر</p></div>
<div class="m2"><p>تا نسوزد مرا ترا نار سقر</p></div></div>