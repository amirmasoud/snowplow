---
title: >-
    بخش ۱۵ - در صفت بدبختی
---
# بخش ۱۵ - در صفت بدبختی

<div class="b" id="bn1"><div class="m1"><p>چارچیز آثار بدبختی بود</p></div>
<div class="m2"><p>جاهلی و کاهلی سختی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی کسی و ناکسی هرچار شد</p></div>
<div class="m2"><p>بخت بد را این همه آثار شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که در بند عبادت می‌شود</p></div>
<div class="m2"><p>بی شک از اهل سعادت می‌شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه در بند عبارت می‌شود</p></div>
<div class="m2"><p>بی شک از اهل خسارت می‌شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر هوای خود قدم هر کو نهاد</p></div>
<div class="m2"><p>می‌تواند کرد با نفسک جهاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که سازد در جهان با خواب و خور</p></div>
<div class="m2"><p>در قیامت نبودش ز آتش گذر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روی گردان از مراد و آرزو</p></div>
<div class="m2"><p>پس بدرگاه خدا آور تو رو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کامرانی سر بناکامی کشد</p></div>
<div class="m2"><p>مرد ره خط در نکونامی کشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>امر ونهی و حق چوداری ای وحید</p></div>
<div class="m2"><p>پس مرو بروایهٔ نفس پلید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>امر و نهی حق ز قرآن گوش دار</p></div>
<div class="m2"><p>جای شادی نیست دنیا هوش دار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر که ترک کامرانی می‌کند</p></div>
<div class="m2"><p>بر خلافش زندگانی می‌کند</p></div></div>