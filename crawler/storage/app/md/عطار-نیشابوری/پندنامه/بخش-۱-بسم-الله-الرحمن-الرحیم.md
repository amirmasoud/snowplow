---
title: >-
    بخش ۱ - بسم الله الرحمن الرحیم
---
# بخش ۱ - بسم الله الرحمن الرحیم

<div class="b" id="bn1"><div class="m1"><p>حمد بی حد آن خدای پاک را</p></div>
<div class="m2"><p>آنکه ایمان داد مشتی خاک را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکه در آدم دمید او روح را</p></div>
<div class="m2"><p>داد از طوفان نجات او نوح را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه فرمان کرد قهرش باد را</p></div>
<div class="m2"><p>تا سزای داد قوم عاد را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه لطف خویش را اظهار کرد</p></div>
<div class="m2"><p>بر خلیلش نار را گلزار کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن خداوندی که هنگام سحر</p></div>
<div class="m2"><p>کرد قوم لوط را زیر و زبر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سوی او خصمی که تیرانداخته</p></div>
<div class="m2"><p>پشه کارش کفایت ساخته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنکه اعدا را بدریا درکشید</p></div>
<div class="m2"><p>ناقه را از سنگ خارا برکشید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون عنایت قادر و قیوم کرد</p></div>
<div class="m2"><p>در کف داود آهن موم کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با سلیمان داد ملک و سروری</p></div>
<div class="m2"><p>شد مطیع خاتمش دیو و پری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از تن صابر بکژمان قوت داد</p></div>
<div class="m2"><p>هم ز یونس لقمهٔ با حوث داد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بنده را اره بر سر می‌نهد</p></div>
<div class="m2"><p>دیگری را تاج بر سر می‌نهد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اوست سلطان هرچه خواهد آن کند</p></div>
<div class="m2"><p>عالمی را در دمی ویران کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هست سلطانی مسلم مرورا</p></div>
<div class="m2"><p>نیست کس را زهرهٔ چون و چرا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن یکی را گنج و نعمت می‌دهد</p></div>
<div class="m2"><p>وان دگر را رنج و زحمت می‌دهد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن یکی را زر دو صد همیان دهد</p></div>
<div class="m2"><p>دیگری در حسرت نان جان دهد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن یکی بر تخت با صد عز و ناز</p></div>
<div class="m2"><p>و آن دگر کرده دهان از فاقه باز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن یکی پوشیده سنجاب و سمور</p></div>
<div class="m2"><p>دیگری خفته برهنه در تنور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن یکی بر بستر کمخا و نخ</p></div>
<div class="m2"><p>وان دگر بر خاک خواری بسته یخ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>طرفه العین جهان بر هم زند</p></div>
<div class="m2"><p>کس نمی‌یارد که آنجا دم زند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آنکه با مرغ هوا ماهی دهد</p></div>
<div class="m2"><p>بندگان را دولت شاهی دهد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بی پدر فرزند پیدا اوکند</p></div>
<div class="m2"><p>طفل را در مهد گویا او کند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مردهٔ صد ساله را حی می‌کند</p></div>
<div class="m2"><p>این به جز حق دیگری کی می‌کند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صانعی کز طین سلاطین می‌کند</p></div>
<div class="m2"><p>نجم را رجم شیاطین می‌کند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از زمین خشک رویاند گیاه</p></div>
<div class="m2"><p>آسمان را نیز اودارد نگاه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هیچ کس در ملک او انبازنی</p></div>
<div class="m2"><p>قول او را لحن نی آواز نی</p></div></div>