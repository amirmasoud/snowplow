---
title: >-
    بخش ۵۰ - در چار خصلت که ترک کردن می‬باید
---
# بخش ۵۰ - در چار خصلت که ترک کردن می‬باید

<div class="b" id="bn1"><div class="m1"><p>درگذر از چار خصلت زینهار</p></div>
<div class="m2"><p>تا نسوزد مر ترا بسیار نار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لذت عمرت اگر باید بدهر</p></div>
<div class="m2"><p>باش دایم برحذر از خشم و قهر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون نگردد خلق با خوی توراست</p></div>
<div class="m2"><p>گر بخوی مردمان سازی رواست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای برادر تکیه بر دولت مکن</p></div>
<div class="m2"><p>یاد دار از ناصح خود این سخن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سود نکند گر گریزی از قضا</p></div>
<div class="m2"><p>هر چه می‌آید بدان می ده رضا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زانکه حاصل نیست دل خرسند دار</p></div>
<div class="m2"><p>گوش دل را جانب این پند دار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که با دوستان یک دل بود</p></div>
<div class="m2"><p>جمله مقصود دلش حاصل بود</p></div></div>