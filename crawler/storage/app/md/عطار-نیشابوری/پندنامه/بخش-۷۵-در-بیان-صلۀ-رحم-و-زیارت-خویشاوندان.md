---
title: >-
    بخش ۷۵ - در بیان صلۀ رحم و زیارت خویشاوندان
---
# بخش ۷۵ - در بیان صلۀ رحم و زیارت خویشاوندان

<div class="b" id="bn1"><div class="m1"><p>رو بپرسیدن بر خویشان خویش</p></div>
<div class="m2"><p>تا که گردد مدت عمر تو بیش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که گرداند ز خویشاوند رو</p></div>
<div class="m2"><p>بی گمان نقصان پذیرد عمر او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که او ترک اقارب می‌کند</p></div>
<div class="m2"><p>جسم خود قوت عقارب می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه خویشان تو باشند از بدان</p></div>
<div class="m2"><p>بدتر از قطع رحم چیزی مدان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که او از خویش خود بیگانه شد</p></div>
<div class="m2"><p>نامش از روی بدی فسانه شد</p></div></div>