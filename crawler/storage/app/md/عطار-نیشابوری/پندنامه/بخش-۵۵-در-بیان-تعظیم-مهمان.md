---
title: >-
    بخش ۵۵ - در بیان تعظیم مهمان
---
# بخش ۵۵ - در بیان تعظیم مهمان

<div class="b" id="bn1"><div class="m1"><p>ای برادر دار مهمان را عزیز</p></div>
<div class="m2"><p>تا بیابی رحمت از رحمن تو نیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مؤمنی کو داشت مهمان را نکو</p></div>
<div class="m2"><p>حق گشاید باب رحمت را برو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کرا شد طبع از مهمان ملول</p></div>
<div class="m2"><p>از وی آزارد خدا و هم رسول</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بندهٔ کو خدمت مهمان کند</p></div>
<div class="m2"><p>خویش را شایستهٔ رحمن کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که مهمان را بر وی تازه دید</p></div>
<div class="m2"><p>از خدا الطاف بی اندازه دید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از تکلف دور باش ای میزبان</p></div>
<div class="m2"><p>تا گرانی نبودت از میهمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میهمان هست از عطاهای کریم</p></div>
<div class="m2"><p>هر که زو پنهان شود باشد لییم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خیره بر خوان کسی مهمان مشو</p></div>
<div class="m2"><p>چون رسد مهمان ازو پنهان مشو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که مهمانت شود از خاص و عام</p></div>
<div class="m2"><p>پیش او می‌باید آوردن طعام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زانکه داری اندک و بیش ای پسر</p></div>
<div class="m2"><p>برد باید پیش درویش ای پسر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نان بده با جایعان بهر خدای</p></div>
<div class="m2"><p>تا دهندت در بهشت عدن جای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر که ثوبی بر تن عاری دهد</p></div>
<div class="m2"><p>در دو عالم ایزدش یاری دهد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر بر آری حاجت محتاج را</p></div>
<div class="m2"><p>بر سر از اقبال یابی تاج را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر که باشد او ز دولت بخت یار</p></div>
<div class="m2"><p>خیر ورزد در نهان و آشکار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای پسر هرگز مخور نان بخیل</p></div>
<div class="m2"><p>کم نشین در عمر بر خوان بخیل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نان ممسک جمله رنجست و عنا</p></div>
<div class="m2"><p>می‌شود نان سخی جمله ضیا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا نخوانندت بخوان کس مرو</p></div>
<div class="m2"><p>وز پی مردار چون کرکس مرو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چشم نیکی از خسیس دون مدار</p></div>
<div class="m2"><p>سقف او را هم تو بی استون شمار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر کنی خیری تو آن از خود مبین</p></div>
<div class="m2"><p>هرچه بینی نیک ببین و بد مبین</p></div></div>