---
title: >-
    بخش ۷۱ - در بیان تجرید و تفرید
---
# بخش ۷۱ - در بیان تجرید و تفرید

<div class="b" id="bn1"><div class="m1"><p>گر صفا می‌بایدت تجرید شو</p></div>
<div class="m2"><p>گر خرد داری ز اهل دید شو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترک دعوی هست تجرید ای پسر</p></div>
<div class="m2"><p>فهم کن معنی تفرید ای پسر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اصل تجریدت وداع شهوتست</p></div>
<div class="m2"><p>بلکه کلی انقطاع لذتست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر دهی یکبار شهوت را طلاق</p></div>
<div class="m2"><p>آن زمان گردی تو در تفرید طلاق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر تو ببریدی ز موجودات امید</p></div>
<div class="m2"><p>آنکه از تجرید گردی مستفید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اعتمادت چون همه بر حق بود</p></div>
<div class="m2"><p>آن دمت تفرید جان مطلق بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترک دنیا کن برای آخرت</p></div>
<div class="m2"><p>وز بدن برکش لباس فاخرت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر بیابی از سعادت این مقام</p></div>
<div class="m2"><p>صاحب تجرید باشی والسلام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر ز عقبی دست شویی بهر حق</p></div>
<div class="m2"><p>دان که از تفرید گیرندت سبق</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رو مجرد باش دایم فرد باش</p></div>
<div class="m2"><p>تا بهر فرقی نشینی گرد باش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرد کبر و عجب و خودرایی مگرد</p></div>
<div class="m2"><p>قدر خود بشناس و هر جایی مگرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر که گرد کورهٔ انگشت گشت</p></div>
<div class="m2"><p>جامه از دودش سیاه و زشت گشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وانکه باعطار می‌گردد قریب</p></div>
<div class="m2"><p>او همی یابد ز بوی خوش نصیب</p></div></div>