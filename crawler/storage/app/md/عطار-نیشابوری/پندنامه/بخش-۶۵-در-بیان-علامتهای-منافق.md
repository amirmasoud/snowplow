---
title: >-
    بخش ۶۵ - در بیان علامتهای منافق
---
# بخش ۶۵ - در بیان علامتهای منافق

<div class="b" id="bn1"><div class="m1"><p>دور باش ای خواجه از اهل نفاق</p></div>
<div class="m2"><p>در جهنم دان منافق را وثاق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سه علامت در منافق ظاهرست</p></div>
<div class="m2"><p>زان سبب مقهور قهر قاهرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وعده‌های او همه باشد خلاف</p></div>
<div class="m2"><p>قول او نبود بغیر از کذب ولاف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مؤمنان را کم رعایت می‌کند</p></div>
<div class="m2"><p>هم امانت راخیانت می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست در وعده منافق را وفا</p></div>
<div class="m2"><p>زان نباشد در رخش نور و صفا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا نپنداری منافق را امین</p></div>
<div class="m2"><p>نیست باداتخمش از روی زمین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از منافق ای پسر پرهیز کن</p></div>
<div class="m2"><p>تیغ را از بهر قتلش تیز کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با منافق هر که همره می‌شود</p></div>
<div class="m2"><p>منزل او در تک چه می‌شود</p></div></div>