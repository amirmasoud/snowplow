---
title: >-
    بخش ۲۲ - در بیان چارچیز که صاحب عقل و دانش را ازینها دور باید بود
---
# بخش ۲۲ - در بیان چارچیز که صاحب عقل و دانش را ازینها دور باید بود

<div class="b" id="bn1"><div class="m1"><p>هر کرا عقلست ودانش ای عزیز</p></div>
<div class="m2"><p>دور باید بودنش از چارچیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کار خود با ناسزا نکند رها</p></div>
<div class="m2"><p>مردمی نکند بجای ناسزا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل داری میل بدکاری مکن</p></div>
<div class="m2"><p>زین چو بگذشتی سبکساری مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا شوی پیش از همه در روزگار</p></div>
<div class="m2"><p>دست بر نان و نمک بگشاده دار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا تو باشی در زمانه دادگر</p></div>
<div class="m2"><p>زیر دستان را نکو دار ای پسر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که در پند خود آمد استوار</p></div>
<div class="m2"><p>پند او را دیگران بندند کار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که از گفتار خود باشد ملول</p></div>
<div class="m2"><p>قول او را دیگران نکنند قبول</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرچه باشد در شریعت ناپسند</p></div>
<div class="m2"><p>گرد او هرگز مگرد ای هوشمند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا صواب کار بینی سر بسر</p></div>
<div class="m2"><p>بر مراد خود مکن کار ای پسر</p></div></div>