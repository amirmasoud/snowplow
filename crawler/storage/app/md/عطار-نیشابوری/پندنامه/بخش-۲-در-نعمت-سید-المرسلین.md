---
title: >-
    بخش ۲ - در نعمت سید المرسلین
---
# بخش ۲ - در نعمت سید المرسلین

<div class="b" id="bn1"><div class="m1"><p>سید الکونین ختم المرسلین</p></div>
<div class="m2"><p>آخر آمد بود فخر الاولین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکه آمد نه فلک معراج او</p></div>
<div class="m2"><p>انبیاء و اولیاء محتاج او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد وجودش رحمة للعالمین</p></div>
<div class="m2"><p>مسجد اوشد همه روی زمین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه یارش بد ابوبکر و عمر</p></div>
<div class="m2"><p>از سر انگشت او شق شد قمر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن یکی را او رفیق غار بود</p></div>
<div class="m2"><p>و آن دگر لشکرزش ابرار بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صاحبش بودند عثمان و علی</p></div>
<div class="m2"><p>بهر آن گشتند در عالم ولی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن یکی کان حیا و حلم بود</p></div>
<div class="m2"><p>وان دگر باب مدینه علم بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن رسول حق که خیر الناس بود</p></div>
<div class="m2"><p>عم پاکش حمزه و عباس بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر دم از ما صد درود و صد سلام</p></div>
<div class="m2"><p>بر رسول و آل و اصحابش تمام</p></div></div>