---
title: >-
    بخش ۲۶
---
# بخش ۲۶

<div class="b" id="bn1"><div class="m1"><p>بگویم بهر تو ای مرد دانا</p></div>
<div class="m2"><p>کنم با تو بیان این معما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بمعنی زندگی دنیا محال است</p></div>
<div class="m2"><p>که این عالم همه خواب و خیال است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حقیقت زندگانی هست ایمان</p></div>
<div class="m2"><p>تو ایمان را کمال زندگانی دان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برو ای سالک ره راه یزدان</p></div>
<div class="m2"><p>تو همچون خضر مینوش آب حیوان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که تا یابی حیات زندگانی</p></div>
<div class="m2"><p>بمانی تا ابد در جاودانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حقیقت آب حیوان راه یزدان</p></div>
<div class="m2"><p>مراد از راه یزدان تو علی دان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باو ایمان و دین تو تمام است</p></div>
<div class="m2"><p>بمعنی هر دو عالم را امام است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به نور او بمعنی راه میجو</p></div>
<div class="m2"><p>تو سرش از دل آگاه میجو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز اسرارش شوی آنگاه آگاه</p></div>
<div class="m2"><p>که برداری حجاب خویش از راه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه ره بردی بنورش زنده مانی</p></div>
<div class="m2"><p>وزو یابی بقای جاودانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو آن آب حیات اسرار میدان</p></div>
<div class="m2"><p>همه مقصود خود آن یار میدان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بود تاریکی این آب ای یار</p></div>
<div class="m2"><p>مثل پنهانیش از چشم اغیار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه ره یابی بسویش در معانی</p></div>
<div class="m2"><p>بیابی در حقیقت کامرانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر او را نیابی مردهٔ تو</p></div>
<div class="m2"><p>میان زندگان افسردهٔ تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر او را بیابی زنده باشی</p></div>
<div class="m2"><p>میان مؤمنان فرخنده باشی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چه ره یابی شوی مانند خورشید</p></div>
<div class="m2"><p>بمانی در بقایش زنده جاوید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حجاب خویشتن از راه کن دور</p></div>
<div class="m2"><p>که تا گردی بمعنی همچو منصور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ولی اسرار مستوری همین دان</p></div>
<div class="m2"><p>ز جاهل این سخنها کن تو پنهان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شنیدی تو که با منصور حق گو</p></div>
<div class="m2"><p>ز نادانی چها کردند با او</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شده بود از دو عالم بر کرانه</p></div>
<div class="m2"><p>نمی‌دانسته جز حق آن بیگانه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>برآورد از وجود خویشتن گرد</p></div>
<div class="m2"><p>سجود درگه حق را چنان کرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سجود اهل دید از دل باشد</p></div>
<div class="m2"><p>سجود دیگران تقلید باشد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو سجده آنچنان کن آن دلی را</p></div>
<div class="m2"><p>کهٔ سجده بود آخر دم علی را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بگویم با تو اسرار سجودش</p></div>
<div class="m2"><p>که چون با حق تعالی راز بودش</p></div></div>