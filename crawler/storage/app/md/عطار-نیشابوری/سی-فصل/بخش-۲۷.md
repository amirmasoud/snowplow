---
title: >-
    بخش ۲۷
---
# بخش ۲۷

<div class="b" id="bn1"><div class="m1"><p>شنیدستم ز دانایان اسرار</p></div>
<div class="m2"><p>که در جنگ احد سلطان کرار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی تیری چه تیر نوک پیکان</p></div>
<div class="m2"><p>به پای مرتضی گردید پنهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میان استخوان پنهان همی بود</p></div>
<div class="m2"><p>علی از درد آن نالان همی بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بیرون کردنش بودند عاجز</p></div>
<div class="m2"><p>ز دردش مرتضی می‌کرد پرهیز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به پیش مصطفی جراح برگفت</p></div>
<div class="m2"><p>که شد پیکان او با استخوان جفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بباید پای او بشکافت اکنون</p></div>
<div class="m2"><p>که تا آید ز پایش تیر بیرون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمی‌شاید مرا این کار کردن</p></div>
<div class="m2"><p>چنان دردی بپای او نهادن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نبی گفتا بدست ماست درمان</p></div>
<div class="m2"><p>بسازم بر تو این دشوار آسان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به هنگامی که حیدر در نماز است</p></div>
<div class="m2"><p>چنان مستغرق دریای راز است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که او را از کس و از خود خبر نیست</p></div>
<div class="m2"><p>غم پیکان و هم درد دگر نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بزن چاک و بکش پیکان ز پایش</p></div>
<div class="m2"><p>که گشته غرق دریای رضایش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو بشنید این سخن را از پیمبر</p></div>
<div class="m2"><p>بشد جراح تا نزدیک حیدر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ستاده دید شه را در نماز او</p></div>
<div class="m2"><p>بحق برداشته روی نیاز او</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بپای شه در افتاد و ثنا گفت</p></div>
<div class="m2"><p>هزاران شاه دین را مرحبا گفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شکافی زد بپای شاه مردان</p></div>
<div class="m2"><p>ز خود بیخود برون آورد پیکان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جراحت را بزد دارو و بر بست</p></div>
<div class="m2"><p>برفت آنگاه جراح سبکدست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به نزد مصطفی آمدکه این راز</p></div>
<div class="m2"><p>بلطف و مرحمت با من بگو باز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بگفتا او بحق چون وصل دارد</p></div>
<div class="m2"><p>چه پروائی ز فرع و اصل دارد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چنان مستغرقست در ذات یزدان</p></div>
<div class="m2"><p>که اورا نه خبر از جسم و از جان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نه پروای زمین و آسمانش</p></div>
<div class="m2"><p>نه فکر این جهان و آن جهانش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چه رو آرد بدرگاه خداوند</p></div>
<div class="m2"><p>ببرد از وجود خویشتن پیوند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اگر زیر و زبر گردد دو عالم</p></div>
<div class="m2"><p>نگرداند سر از درگاه آن دم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همه با حق بود گفت و شنودش</p></div>
<div class="m2"><p>برای حق بود جود و سجودش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدین معنی خوش و خورسند باشد</p></div>
<div class="m2"><p>مر او را با خدا پیوند باشد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چنین باید عبادت مر خدا را</p></div>
<div class="m2"><p>چنین میر و طریق مرتضی را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کسی را کین عبادت یار باشد</p></div>
<div class="m2"><p>دلش منزلگه دلدار باشد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چنین میکن عبادت ای برادر</p></div>
<div class="m2"><p>ولی میدار در دل حب حیدر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر صد سال باشی در عبادت</p></div>
<div class="m2"><p>نیابی تا بشاه دین ارادت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عبادت آن زمان حق را قبول است</p></div>
<div class="m2"><p>که در دل حب اولاد رسول است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>امیرالمؤمنین را گربدانی</p></div>
<div class="m2"><p>بیابی در حقیقت کامرانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بنورش راهبر شو در معانی</p></div>
<div class="m2"><p>که تا اسرار یزدانی بدانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بدو واصل شوی چون بحر و قطره</p></div>
<div class="m2"><p>بیابی از وجود خویش بهره</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بنورش زندهٔ جاوید باشی</p></div>
<div class="m2"><p>بمعنی بهتر از خورشید باشی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دگر پرسی که علم دین کدامست</p></div>
<div class="m2"><p>معلم در ره و آیین کدامست</p></div></div>