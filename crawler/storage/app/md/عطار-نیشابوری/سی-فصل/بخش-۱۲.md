---
title: >-
    بخش ۱۲
---
# بخش ۱۲

<div class="b" id="bn1"><div class="m1"><p>تو ناجی را نمی‌دانی ز هالک</p></div>
<div class="m2"><p>نمی‌دانی درین ره کیست مالک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حدیثی مصطفی گفته در این باب</p></div>
<div class="m2"><p>بگویم با تو این اسرار در یاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنین فرمود کز بعد من امت</p></div>
<div class="m2"><p>شوند در دین هفتاد و سه ملت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی ناجی بود در دین الله</p></div>
<div class="m2"><p>بود هفتاد و دو مردود درگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگویم با تو آن ناجی کدام است</p></div>
<div class="m2"><p>کسی کو واقف از سر امام است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود مأمور امر مصطفی را</p></div>
<div class="m2"><p>امام خویش نامد مرتضی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شناسد از ره معنی وصی را</p></div>
<div class="m2"><p>نباشد منکر او قول نبی را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شناسای امامان سالکانند</p></div>
<div class="m2"><p>ولیکن ناشناسان هالکانند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بود ناجی کسی بیشک درین راه</p></div>
<div class="m2"><p>که او باشد ز اصل خویش آگاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو با حق دان کسی کو راه دانست</p></div>
<div class="m2"><p>بعالم مظهر الله دانست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو ناجی دان کسی کو یار باشد</p></div>
<div class="m2"><p>بمعنی واقف اسرار باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو ناجی دان کسی کو راه شاهست</p></div>
<div class="m2"><p>امیرالمؤمنین او را پناهست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر آنکس کز علی گردید مأمور</p></div>
<div class="m2"><p>شود بیشک سرا پایش همه نور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ازو باشد نجات و رستگاری</p></div>
<div class="m2"><p>تو دست از دامن او برنداری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خدا اورا به هر جاه راه دادست</p></div>
<div class="m2"><p>بهر چیزی دل آگاه دادست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو حاضر دان مر او را در همه جا</p></div>
<div class="m2"><p>گهی پنهان بود او گاه پیدا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گهی حاضر بود او گاه غایب</p></div>
<div class="m2"><p>مر او را گفته‌اند مظهر عجایب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بگویم اول و آخر همه اوست</p></div>
<div class="m2"><p>بمعنی باطن و ظاهر همه اوست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یقین میدان که او از نور ذاتست</p></div>
<div class="m2"><p>میان جان و دل آب حیات است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در این اسرار مرد نیک صادق</p></div>
<div class="m2"><p>بود آن هالک بی‌دین منافق</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو هالک دان هر آن کو ره ندانست</p></div>
<div class="m2"><p>طریق ملت آن شه ندانست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو هالک دان کسی کو غیر حیدر</p></div>
<div class="m2"><p>گزیند در ره دین پیردیگر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو هالک دان که نشناسد علی را</p></div>
<div class="m2"><p>نداند او امام حق ولی را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو هالک دان کسی مأمور نبود</p></div>
<div class="m2"><p>نهاده جان بکف منصور نبود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تو هالک دان کسی کو نیست درویش</p></div>
<div class="m2"><p>نمی‌داند امام و رهبر خویش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اگر خواهی که باشی ناجی راه</p></div>
<div class="m2"><p>نتابی سر ز امر حضرت شاه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اگر بندی کمر در راه فرمان</p></div>
<div class="m2"><p>وجود خود کنی همچون گلستان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به جان آزاد شو از هر دو عالم</p></div>
<div class="m2"><p>چگویم به ازین والله اعلم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دگر پرسی که علم دین کدامست</p></div>
<div class="m2"><p>که آن ما را ز امر حق پیامست</p></div></div>