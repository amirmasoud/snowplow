---
title: >-
    بخش ۹
---
# بخش ۹

<div class="b" id="bn1"><div class="m1"><p>بدان کانسان کامل انبیا بود</p></div>
<div class="m2"><p>ولی بهتر ز جمله مصطفی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به عالم انبیا بسیار بودند</p></div>
<div class="m2"><p>نه جمله واقف اسرار بودند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ولیکن شش پیمبر در طریقت</p></div>
<div class="m2"><p>شدند مأمور اسرار شریعت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نخستین این ندا در داد آدم</p></div>
<div class="m2"><p>بگسترد او شریعت را به عالم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس ابراهیم بد صاحب توکل</p></div>
<div class="m2"><p>که بر وی آتش نمرود شد گل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بعد او کلیم الله را دان</p></div>
<div class="m2"><p>عصا شد در کفش مانند ثعبان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیامد بعد از آن عیسی مریم</p></div>
<div class="m2"><p>که مرده زنده گردانید از دم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز بعدش خاتم خیرالبشر بود</p></div>
<div class="m2"><p>که او پیغمبران را جمله سر بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برو شد ختم اسرار شریعت</p></div>
<div class="m2"><p>طریق اوست اکمال طریقت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که حال جمله پیغمبران اوست</p></div>
<div class="m2"><p>اگر دانی تو این اسرار نیکوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ازو میپرس اسرار شریعت</p></div>
<div class="m2"><p>به پیش حیدر آمد دین و ملت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بقرآن این چنین فرمود داور</p></div>
<div class="m2"><p>تو تا دینش بدانی ای برادر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که عالم را به شش روز آفریدم</p></div>
<div class="m2"><p>محمد را به عالم برگزیدم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بود عالم حقیقت عالم دین</p></div>
<div class="m2"><p>چنین دارم ز پیر راه تلقین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بود شش روز دور شش پیمبر</p></div>
<div class="m2"><p>مرا تعلیم قرآن گشت یاور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ولیکن روز دین سالی هزار است</p></div>
<div class="m2"><p>بدان ترتیب عالم را مدار است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چه گردد شش هزار از سال آخر</p></div>
<div class="m2"><p>شود قایم مقام خلق ظاهر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بسر آید همه دور شریعت</p></div>
<div class="m2"><p>بامر حق شود پیدا قیامت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو اسرار قیامت را ندانی</p></div>
<div class="m2"><p>ره دین و قیامت را چه دانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نبد فرمان که سازند انبیا را</p></div>
<div class="m2"><p>رموز این قیامت آشکارا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حدیثی مصطفی گفته درین باب</p></div>
<div class="m2"><p>روایت این چنین کردند اصحاب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که جن و انس چندانی که باشند</p></div>
<div class="m2"><p>همه اندر قیامت جمع باشند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>که بردارند علم از پیش خلقان</p></div>
<div class="m2"><p>نباشد قوت برداشتن شان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به تنهائی علی بردارد آن را</p></div>
<div class="m2"><p>کند اسرار پنهان آشکارا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بگوید جمله علم اولین را</p></div>
<div class="m2"><p>نماید سر علم آخرین را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خدا را هم به خلقان او نماید</p></div>
<div class="m2"><p>در بسته به خلقان او گشاید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جهان گردد ازو پر امن و ایمان</p></div>
<div class="m2"><p>جماد و جانور یابد ازو جان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کسی کو مرده باشد در جهالت</p></div>
<div class="m2"><p>برفته راه حق را از ضلالت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نماند در جهان ترسا و کافر</p></div>
<div class="m2"><p>کند علم حقیقت جمله ظاهر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>قیامت دور دین مرتضی دان</p></div>
<div class="m2"><p>به معنیش تو باب مصطفی دان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تو باب الله میدان مرتضی را</p></div>
<div class="m2"><p>ز خودآگاه میدان مرتضی را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ازین در رو که تا بینی خدا را</p></div>
<div class="m2"><p>ازین درگاه بینی مصطفی را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ازین در گر روی باشی تو برحق</p></div>
<div class="m2"><p>درو بینی حقیقت سر مطلق</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>که باب حق هم او باشد بمعنی</p></div>
<div class="m2"><p>امیرالمؤمنین میدان تو یعنی:</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>امیرالمؤمنین است جان آدم</p></div>
<div class="m2"><p>امیرالمؤمنین با نوح همدم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>امیرالمؤمنین عیسی و مریم</p></div>
<div class="m2"><p>امیرالمؤمنین با روح همدم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>امیرالمؤمنین باب نبوت</p></div>
<div class="m2"><p>امیرالمؤمنین اصل فتوت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>امیرالمؤمنین شرح بیان است</p></div>
<div class="m2"><p>امیرالمؤمنین نطق زبان است</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>امیرالمؤمنین سلطان عادل</p></div>
<div class="m2"><p>امیرالمؤمنین انسان کامل</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>امیرالمؤمنین باب ولایت</p></div>
<div class="m2"><p>امیرالمؤمنین ختم رسالت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اگر از بحث برخوردار گردی</p></div>
<div class="m2"><p>مطیع حیدر کرار گردی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مراتب گر نماید راه تحقیق</p></div>
<div class="m2"><p>تو باب الله را دانی به تحقیق</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>در این درباش و دولتمند میباش</p></div>
<div class="m2"><p>بدین دولت خوش و خورسند میباش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دگر پرسی که دارد زهد و تقوی</p></div>
<div class="m2"><p>درین معنی مرا چه هست دعوی؟</p></div></div>