---
title: >-
    بخش ۱۳
---
# بخش ۱۳

<div class="b" id="bn1"><div class="m1"><p>علوم دین بگویم با تو ای یار</p></div>
<div class="m2"><p>تو این اسرار از من گوش میدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>علوم باطنی را گوش میدار</p></div>
<div class="m2"><p>علوم ظاهری فرموش میدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز علم باطنی ای یار انور</p></div>
<div class="m2"><p>چنین گفتند دانایان رهبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که علم دین بود دانستن راه</p></div>
<div class="m2"><p>شود در راه دین از خویش آگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شناسی خویشتن را گر کجائی</p></div>
<div class="m2"><p>درین محنت سرا بهر چرائی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باول از کجا داری تو آغاز</p></div>
<div class="m2"><p>بآخر هم کجا خواهی شدن باز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>امام خویشتن را هم بدانی</p></div>
<div class="m2"><p>طلب داری حیات جاودانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ولیکن کس بخود این ره نداند</p></div>
<div class="m2"><p>که پیر رهبر این ره بداند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طلب کن پیر رهبر اندرین راه</p></div>
<div class="m2"><p>که گرداند ترا از کار آگاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ترا راه حقیقت او نماید</p></div>
<div class="m2"><p>در اسرار برویت گشاید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از آن در علم دین آگاه گردی</p></div>
<div class="m2"><p>تو واقف از کلام الله گردی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو او را گر شناسی علم دانی</p></div>
<div class="m2"><p>علوم اول وآخر بخوانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو او را گر شناسی محو مانی</p></div>
<div class="m2"><p>بغیر او دگر چیزی ندانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو او را گر شناسی جان بیابی</p></div>
<div class="m2"><p>طریق بوذر و سلمان بیابی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همین است علم دین ای مرد دانا</p></div>
<div class="m2"><p>که دانا در ره وحدت خدا را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بفر شاه مردان ره بری تو</p></div>
<div class="m2"><p>شوی واقف ز سر حیدری تو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مقام علم دین در فر شاهی است</p></div>
<div class="m2"><p>مرا در معنی این علم راهی است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بمعنایش نمایم من ترا راه</p></div>
<div class="m2"><p>که تا گردی ز سر وحدت آگاه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مبین خود را اگر تو مرد دینی</p></div>
<div class="m2"><p>خدا بینی اگر خود را نه بینی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو خود را محو کن در شیر یزدان</p></div>
<div class="m2"><p>خدا بین و خداخوان و خدا دان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>درآئی در مقام خودپرستی</p></div>
<div class="m2"><p>تو خود باشی بت و خود را پرستی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بجز حق هرچه مقصود تو باشد</p></div>
<div class="m2"><p>همان مقصود و معبود تو باشد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو خود را نیست میکن هست اوباش</p></div>
<div class="m2"><p>زجام وحدت حق مست او باش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو خود اول شناسی پس خدا را</p></div>
<div class="m2"><p>ز بعد مصطفی خود مرتضی را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به اسرار علی گر راه یابی</p></div>
<div class="m2"><p>ز علم مصطفی آگاه یابی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو او را گر شناسی نور گردی</p></div>
<div class="m2"><p>بپاکی خوبتر از هور گردی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو او را گر شناسی مرد راهی</p></div>
<div class="m2"><p>بیابی در دو عالم پادشاهی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>باسرارش اگر باشی تو محرم</p></div>
<div class="m2"><p>روی چون قطره اندر بحر اعظم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بنور او ولی او را شناسی</p></div>
<div class="m2"><p>مکن با نعمت او ناسپاسی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بهر عصری ظهوری کرد در دهر</p></div>
<div class="m2"><p>گهی باشد به صحرا گاه در شهر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>محمد نور و حیدر نور نور است</p></div>
<div class="m2"><p>بهرجائی که خوانی در حضور است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ترا رهبر بود او ره نماید</p></div>
<div class="m2"><p>نشان راه آن درگه نماید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ترا دانش بدان در کار آرد</p></div>
<div class="m2"><p>ز بی راهی ترا در راه آرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>برو عطار این سر را نگهدار</p></div>
<div class="m2"><p>میان عاشقان میگو تو اسرار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>من اسراری که در دل می‌نهفتم</p></div>
<div class="m2"><p>بتو ای مرد سالک بازگفتم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در معنی برویت برگشادم</p></div>
<div class="m2"><p>کلید علم بر دست تو دادم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بگو با مرد دانا سر حق را</p></div>
<div class="m2"><p>ز نادانان بگردان این ورق را</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دگر پرسی ز من این چرخ فیروز</p></div>
<div class="m2"><p>ز بهر چیست گردان در شب روز</p></div></div>