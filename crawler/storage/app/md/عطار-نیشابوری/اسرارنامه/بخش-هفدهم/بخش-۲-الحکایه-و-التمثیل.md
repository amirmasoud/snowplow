---
title: >-
    بخش ۲ - الحکایه و التمثیل
---
# بخش ۲ - الحکایه و التمثیل

<div class="b" id="bn1"><div class="m1"><p>یکی چندانک در ره ژنده دیدی</p></div>
<div class="m2"><p>جز آن کارش نبودی ژنده چیدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شبی چون پرشدش از ژنده خانه</p></div>
<div class="m2"><p>فتادش اخگری اندر میانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه ژنده بسوخت او در میان هم</p></div>
<div class="m2"><p>کرا در هر دو عالم بود از آن غم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>الا یا ژنده چین ژنده چه چینی</p></div>
<div class="m2"><p>میان ژنده تا چندی نشستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو بهر ژنده داری چشم بر راه</p></div>
<div class="m2"><p>بسوزی هم تو و هم ژنده ناگاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو پنداری که چون مردی برستی</p></div>
<div class="m2"><p>کجا رستی که در سختی نشستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یقین می‌دان که چون جانت برآید</p></div>
<div class="m2"><p>بیک یک ذره طوفانت برآید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نباشد از تو یک یک ذره بی کار</p></div>
<div class="m2"><p>بود در رنج جان کندن گرفتار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو ازگورت برانگیزند مضطر</p></div>
<div class="m2"><p>برهنه پا و سر در دشت محشر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو خوش آتش زدی در خرمن خویش</p></div>
<div class="m2"><p>ندانی آنچ کردی با تن خویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تر این پس روی غول تا کی</p></div>
<div class="m2"><p>بدنیا دوستی مشغول تا کی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدادی رایگانی عمر از دست</p></div>
<div class="m2"><p>اگر بر خود بگریی جان آن هست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دمی کان را بها آید جهانی</p></div>
<div class="m2"><p>پی آن دم نمی‌گیری زمانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گرفتی از سر غفلت کم خویش</p></div>
<div class="m2"><p>نمی‌دانی بهای یک دم خویش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گهی معجز گهی برهان نمودند</p></div>
<div class="m2"><p>گهی توریت و گه قرآن نمودند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ترا از نیک و بد آگاه کردند</p></div>
<div class="m2"><p>بسوی حق رهت کوتاه کردند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بگفتندت چه کن چون کن چرا کن</p></div>
<div class="m2"><p>هوارا امیل کش کار خدا کن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نه زان بود این همه سختی و درخواست</p></div>
<div class="m2"><p>که تا دستار رعنایی کنی راست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ببازار تکبر می‌خرامی</p></div>
<div class="m2"><p>نیارد گفت کس با تو چه نامی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بپوشی جامهٔ با صد شکن تو</p></div>
<div class="m2"><p>نیندیشی ز کرباس و کفن تو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ترا تا نشکند در هم سر و پای</p></div>
<div class="m2"><p>نگردی سیرنان و جامه وجای</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو تا سر داری و تا پای داری</p></div>
<div class="m2"><p>رگ سود و زیان بر جای داری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو خاکی طبع چندین باد پندار</p></div>
<div class="m2"><p>چو سر بنهی ز سر بنهی بیک بار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خوشی خود را غروری می‌دهی تو</p></div>
<div class="m2"><p>سبد از آب زود آری تهی تو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو در خوابی سخن هیچی ندانی</p></div>
<div class="m2"><p>چو سر اندر کفن پیچی ندانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>برو جهدی کن ار پیغمبری تو</p></div>
<div class="m2"><p>که تا توشه ازین عالم بری تو</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو پنداری بیک طاعت برستی</p></div>
<div class="m2"><p>که از غفلت چنین فارغ نشستی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ترا این سخته نیست این کار ای دوست</p></div>
<div class="m2"><p>برون می‌باید آمد پاک از پوست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>فغان و خامشی سودی ندارد</p></div>
<div class="m2"><p>که هستی تو به بودی ندارد</p></div></div>