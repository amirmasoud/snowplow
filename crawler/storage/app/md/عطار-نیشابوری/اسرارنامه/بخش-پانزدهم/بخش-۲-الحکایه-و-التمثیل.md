---
title: >-
    بخش ۲ - الحکایه و التمثیل
---
# بخش ۲ - الحکایه و التمثیل

<div class="b" id="bn1"><div class="m1"><p>مگر بیمار شد آن تنگ دستی</p></div>
<div class="m2"><p>که دایم کونهٔ هیزم شکستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بپرسش رفت غزالی بر او</p></div>
<div class="m2"><p>نشست از پای اما بر سر او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدو گفتا که بهتر گردی این بار</p></div>
<div class="m2"><p>مخور غم زین جوابش داد بیمار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که بهتر گشته گیرم ای خردمند</p></div>
<div class="m2"><p>شکسته بار دیگر کونه‌ای چند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه برهم می‌نهی چون آخر کار</p></div>
<div class="m2"><p>فور خواهد فتاد از هم بیک بار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز سود خود مشود خشنود دنیا</p></div>
<div class="m2"><p>اگر مردی زیان کن سود دنیا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یقین می‌دان که مرد راه آنست</p></div>
<div class="m2"><p>که سود این جهان او را زیانست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز بی هیچی خود پیچش نباشد</p></div>
<div class="m2"><p>نباشد هیچش از هیچش نباشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بزرگانی که دین مقصود ایشانست</p></div>
<div class="m2"><p>زیان کار دنیا سود ایشانست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدنیا ملک عقبی زان خردیدند</p></div>
<div class="m2"><p>که این صد ساله سختی سود دیدند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو نیز ای مانده در دنیای فانی</p></div>
<div class="m2"><p>چنین بیع و شری کن گر توانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زیان آمد همه سود من و تو</p></div>
<div class="m2"><p>فغان از زاد وز بود من و تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بزادن جمله در شوریم و آشوب</p></div>
<div class="m2"><p>بمردن جمله در زیر لگدکوب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جهان تا بود ازو جان می برآمد</p></div>
<div class="m2"><p>یکی می‌رفت و دیگر می‌درآمد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جهان را ماه شادی زیر میغ است</p></div>
<div class="m2"><p>همه کار جهان درد و دریغ است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جهان با سینهٔ پر درد ما را</p></div>
<div class="m2"><p>خوشی درخواب خواهد کرد ما را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز بیدادی جهان داند جهان سوخت</p></div>
<div class="m2"><p>نباید گرگ را دریدن آموخت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چنان می جادوی سازد زمانه</p></div>
<div class="m2"><p>که کس دستش نبیند در میانه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بدست چپ نماید این شگفتی</p></div>
<div class="m2"><p>تو پای راست نه در پیش و رفتی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ترا با جادویی او چه کارست</p></div>
<div class="m2"><p>مقامت نیست دنیا ره گذارست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جهان بر ره گذر هنگامه کردست</p></div>
<div class="m2"><p>تو بگذر زانک این هنگامه سردست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اگر کودک نیی بنگر پس و پیش</p></div>
<div class="m2"><p>بهنگامه مه ایست ای دوست زین پیش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چه می‌خواهی ز خود بیرون بمانده</p></div>
<div class="m2"><p>میان خاک دل پرخون بمانده</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>برو جان گیر و ترک این جهان کن</p></div>
<div class="m2"><p>که او گیر و داوش در میان کن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چه خواهی داو زین گردنده پرگار</p></div>
<div class="m2"><p>که خواهی شد بد او او گرفتار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چه بخشد چرخ مردم را در آغاز</p></div>
<div class="m2"><p>که در انجام نستاند از او باز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو طاوسیست گردون پرگشاده</p></div>
<div class="m2"><p>جهانی خلق را بر پر نهاده</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بروز این آسمان دود کبودست</p></div>
<div class="m2"><p>بشب آب سیاه آخر چه بودست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بماندی در کبودی و سیاهی</p></div>
<div class="m2"><p>بمردی در میان آخر چه خواهی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>برو زین گرد نای آبنوسی</p></div>
<div class="m2"><p>چه زین درنده درزی می‌بیوسی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سخن تا چند گویی آسمان را</p></div>
<div class="m2"><p>که بی شک بر زمین اندازد آنرا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زدست آسمان هر دل که جان داشت</p></div>
<div class="m2"><p>گرش دستست هم بر آسمان داشت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>فلک طشتیست پر اخگر ز اختر</p></div>
<div class="m2"><p>تو دل پر تفت زیر طشت و اخگر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سزد گر پای بر آتش بماندی</p></div>
<div class="m2"><p>که زیر آتشین مفرش بماندی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر از خورشید فرق تو کله داشت</p></div>
<div class="m2"><p>کله نتوانی از گردون نگه داشت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مرا باری دل از گردون فرومرد</p></div>
<div class="m2"><p>ز بس کس کو برآورد و فرو برد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کرا این گنبد گردان بر آرد</p></div>
<div class="m2"><p>که نه در عاقبت از جان برآرد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>جهان خون بی حد و بی باک کردست</p></div>
<div class="m2"><p>بسی زین تیغ زیر خاک کردست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>فلک هر لحظه دیگر چیزت آرد</p></div>
<div class="m2"><p>بهر ساعت بلایی نیزت آورد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>عجب درمانده‌ام چون مبتلایی</p></div>
<div class="m2"><p>که دل چون می‌چخد با هر بلایی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بگو تا چند گاه اندوه و گه غم</p></div>
<div class="m2"><p>فغان از روز و شب وز سال و مه هم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نگردد هیچ صبحی روز نزدیک</p></div>
<div class="m2"><p>که تا بر ما نگردد روز تاریک</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نگردد هیچ شامی شب پدیدار</p></div>
<div class="m2"><p>که نه شب خوش کند شادی بیک بار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نگردد هیچ ماهی نو درین باب</p></div>
<div class="m2"><p>که تا بر ما نپیمایند مهتاب</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نگردد هیچ سالی نو ز ایام</p></div>
<div class="m2"><p>که نه ده ساله از ماغم کند وام</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>حدیث ماه و سال و روز و شب بین</p></div>
<div class="m2"><p>عجب بازی چرخ بوالعجب بین</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو شب انگشت ریزندش ببردر</p></div>
<div class="m2"><p>بهر روزی ببایندش ز سر در</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تنوری تافتست این دیر ناساز</p></div>
<div class="m2"><p>کزو بی سوز ناید گردهٔ باز</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بتر زین در زمانه فتنه‌ای نیست</p></div>
<div class="m2"><p>کزین چنبر رسن را رخنه‌ای نیست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>اگر خواهی که تو بیرون گریزی</p></div>
<div class="m2"><p>نه پایست و نه چنبر چون گریزی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>که گفتت گرد چرخ چنبری گرد</p></div>
<div class="m2"><p>که قد همچو سروت چنبری کرد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>سپهری را که دریاییست پرجوش</p></div>
<div class="m2"><p>شدی چون چنبر دف حلقه در گوش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ترا چون چنبر گردون فرو بست</p></div>
<div class="m2"><p>چرا در گردنش چنبر کنی دست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>سپهر چنبری چنبر بسی زد</p></div>
<div class="m2"><p>چو حلقه بر در حق سربسی زد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بسی چنبر بزد چون خاک بیزی</p></div>
<div class="m2"><p>نیامد بر سر غربال چیزی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>درین اندوه پشتش چنبری شد</p></div>
<div class="m2"><p>لباس او ز غم نیلوفری شد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تو می‌خواهی که برخیزی ببازی</p></div>
<div class="m2"><p>ازین چنبر جهی بیرون چو غازی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>تو نشناسی الف از چنبری باز</p></div>
<div class="m2"><p>مکن سوی سپهر چنبری ساز</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گذر زین چنبر آن ساعت توانی</p></div>
<div class="m2"><p>که جان برچنبر حلقت رسانی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>اگر صد گز رسن باشد بناکام</p></div>
<div class="m2"><p>گذر بر چنبرش باشد سرانجام</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>زهی افسوس و حیلت سازی ما</p></div>
<div class="m2"><p>زهی دوران چنبر بازی ما</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>جهانا طبع مردم خوار داری</p></div>
<div class="m2"><p>که چندین خلق در پروار داری</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>یکایک را میان نعمت و ناز</p></div>
<div class="m2"><p>بپروردی و خوردی عاقبت باز</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>جهانا کیست کز دور تو شادست</p></div>
<div class="m2"><p>همه دور تو با جور تو بادست</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>جهانا غولی و مردم نمایی</p></div>
<div class="m2"><p>که جو بفروشی و گندم نمایی</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>جهانا با که خواهی ساخت آخر</p></div>
<div class="m2"><p>بکوری چند خواهی باخت آخر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>دلا ترک جهان گیر از جهان چند</p></div>
<div class="m2"><p>ترا هر دم ز دور او زیان چند</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ز دست نه خم پرپیچ ایام</p></div>
<div class="m2"><p>چه می‌پیچی بخواهی مرد ناکام</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>جهان چون نیست از کار تو غم ناک</p></div>
<div class="m2"><p>چرا بر سر کنی از دست او خاک</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چه سود ار خاک بر افلاک ریزی</p></div>
<div class="m2"><p>که گر سنگی میان خاک ریزی</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>جهان را بر کسی غم خوارگی نیست</p></div>
<div class="m2"><p>کسی را چاره جز بیچارگی نیست</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>جهان چو تو بسی داماد دارد</p></div>
<div class="m2"><p>بسی عید و عروسی یاد دارد</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>نه بتواند زمانی شاد دیدت</p></div>
<div class="m2"><p>نه یک دم از غمی آزاد دیدت</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>بعمری می‌دهد رنج مدامت</p></div>
<div class="m2"><p>که تا کار جهان گیرد نظامت</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بعمری جز بلا حاصل نبینی</p></div>
<div class="m2"><p>که تا روزی بکام دل نشینی</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چو بنشستی برانگیزد بزورت</p></div>
<div class="m2"><p>بزاری می‌دواند تا بگورت</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>تو تا بنشستهٔ در دار فانی</p></div>
<div class="m2"><p>نشسته رفتهٔ و می ندانی</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>مثالت راست چون گردست پیوست</p></div>
<div class="m2"><p>که گرد آنگه رود بی شک که بنشست</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ز دور نه سپهر یک ده آیت</p></div>
<div class="m2"><p>چه باید کرد چندینی شکاست</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>فلک سرگشته تر از تست بسیار</p></div>
<div class="m2"><p>چه باید خواست زو یاری بهر کار</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>فلک عمری دوید اندر تک و تاز</p></div>
<div class="m2"><p>که تا سرگشتگی دارد ز خود باز</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>چو نتواند که از خود باز دارد</p></div>
<div class="m2"><p>ترا چون در میان ناز دارد</p></div></div>