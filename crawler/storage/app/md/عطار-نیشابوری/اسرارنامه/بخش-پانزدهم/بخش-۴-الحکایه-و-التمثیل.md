---
title: >-
    بخش ۴ - الحکایه و التمثیل
---
# بخش ۴ - الحکایه و التمثیل

<div class="b" id="bn1"><div class="m1"><p>مگر آن روستایی بود دلتنگ</p></div>
<div class="m2"><p>بشهر آمد همی زد مطربی چنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوشش آمد که مطرب چنگ بنواخت</p></div>
<div class="m2"><p>کشید او لالکا در مطرب انداخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر مطرب شکست او چنگ بفکند</p></div>
<div class="m2"><p>بروت روستایی پاک برکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو سوی ده شد آن بیچاره از قهر</p></div>
<div class="m2"><p>ز نادانی بروتی زد فرا شهر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که نزد من ندارد شهر مقدار</p></div>
<div class="m2"><p>ولیکن بر بروتش بد پدار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهان پر شیشه بر هم نهادست</p></div>
<div class="m2"><p>اگر سنگی زنی بر تو فتادست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو در معنی نه اهل راز باشی</p></div>
<div class="m2"><p>بتاریکی چو مشت انداز باشی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر اینجای یک دم می‌زنی تو</p></div>
<div class="m2"><p>هم اینجا بیخ عالم می‌زنی تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو هفت اندام تو افتاد در دام</p></div>
<div class="m2"><p>چه گویی فارغم از هفت اندام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر سر کژ کند یک موی بر تو</p></div>
<div class="m2"><p>هزاران درد آرد روی بر تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر گردد یک انگشتت بریده</p></div>
<div class="m2"><p>ز عجز خود شوی پرده دریده </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>درین نه طشت خوان در گفت و گویی</p></div>
<div class="m2"><p>بماندی همچو منجی در سبویی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو خود در چه حسابی وز کجایی</p></div>
<div class="m2"><p>که تو چون شیشه زیر آسیایی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نمی‌دانی که در بازار فطرت</p></div>
<div class="m2"><p>بجز حق نیست بازرگان قدرت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو پنداری که می‌آیی ز جایی</p></div>
<div class="m2"><p>زهی پندار تو ناخوش بلایی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو خفاشی که از روزن برآید</p></div>
<div class="m2"><p>ز کنج آستان بیشش درآید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بگردد گرد باغ و راغ لختی</p></div>
<div class="m2"><p>نشیند بر سر هر سر درختی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اگرموری سری یابد ز جایی</p></div>
<div class="m2"><p>چنان داند که گشت او پادشایی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بجز خود را نبیند در میانه</p></div>
<div class="m2"><p>بمویی شاد گردد از زمانه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ولی چون آفتاب آتشین روی</p></div>
<div class="m2"><p>نهد از آسمان سوی زمین روی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نماید در دل خفاش دستان</p></div>
<div class="m2"><p>گریزان شیر می‌ریزد ز پستان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>الا ای روز و شب مانند خفاش</p></div>
<div class="m2"><p>شده هم رغم این یک مشت اوباش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بمویی چند چون خفاش قانع</p></div>
<div class="m2"><p>ز کوری عمر شیرین کرده ضایع</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو شب پر روز کوری بازمانده</p></div>
<div class="m2"><p>شبان روزی اسیر آز مانده</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نه روی آفتاب از دور دیده</p></div>
<div class="m2"><p>نه چشمت رشته تای نور دیده</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نیندیشی که چون خورشید جبار</p></div>
<div class="m2"><p>ز برج وحدتی آید پدیدار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دلت شایستگی ناداده جان را</p></div>
<div class="m2"><p>چگونه تاب آرد نور آن را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>برو شایستگی خویش کن ساز</p></div>
<div class="m2"><p>چو ذره پیش آن خورشید شو باز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>برا ای ذره زین روزن که داری</p></div>
<div class="m2"><p>که نیست این خانه بس روشن که داری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ترا رفتن ازین روزن صوابست</p></div>
<div class="m2"><p>که صحرای جهان پر آفتابست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تو می‌گویی که نور من چنانست</p></div>
<div class="m2"><p>که کس از نور من قدرم ندانست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سخن از قدر خود تا چند رانی</p></div>
<div class="m2"><p>اگر خواهی که قدر خود بدانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کفی خاک سیه بر گیر از راه</p></div>
<div class="m2"><p>نقش کن پس ببادش ده هم آنگاه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بدان کاغاز و انجام تو در کار</p></div>
<div class="m2"><p>کفی خاکست اگر هستی خبردار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تو مشتی خاک و چندینی تغیر</p></div>
<div class="m2"><p>تفکر کن مکن چندین تکبر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تکبر می‌کنی ای پارهٔ خون</p></div>
<div class="m2"><p>ز چندین ره گذر افتاده بیرون</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>برو از سر بنه کبر و بر اندیش</p></div>
<div class="m2"><p>که تا تو کیستی و چیست در پیش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خوشی دل بر جهان بنهاده ای تو</p></div>
<div class="m2"><p>ببین تا خود کجا افتاده‌ای تو</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چنین چرخی که گردتست گردان</p></div>
<div class="m2"><p>چنین گویی که زیرتست میدان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اگر تو رفع و خفض آن نبینی</p></div>
<div class="m2"><p>میان هر دو ساکن چون نشینی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>رهی جویی بفکرت همچو مردان</p></div>
<div class="m2"><p>بگردی در مضیق چرخ گردان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بسوی آشیان خود کنی ساز</p></div>
<div class="m2"><p>درین عالم بجای خود رسی باز</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بگردی گرد این مردار خانه</p></div>
<div class="m2"><p>نترسی از طلسمات زمانه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چه گر، دریا همی بینی تو خاموش</p></div>
<div class="m2"><p>ولی می‌ترس کاید زود در جوش</p></div></div>