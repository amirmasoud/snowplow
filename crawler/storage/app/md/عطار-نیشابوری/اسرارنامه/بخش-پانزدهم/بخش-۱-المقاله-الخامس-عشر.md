---
title: >-
    بخش ۱ - المقاله الخامس عشر
---
# بخش ۱ - المقاله الخامس عشر

<div class="b" id="bn1"><div class="m1"><p>نکو باریست در دنیا و برگی</p></div>
<div class="m2"><p>که درخوردست سر باریش مرگی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نکو جاییست گور تنگ و تاریک</p></div>
<div class="m2"><p>که در باید صراطی نیز باریک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پلی نیکوست چون موی صراطی</p></div>
<div class="m2"><p>که دوزخ باید آن پل را رباطی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو گویی نیست چندین غم تمامت</p></div>
<div class="m2"><p>که در باید غم روز قیامت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درین معنی مجال دم زدن نیست</p></div>
<div class="m2"><p>همه رفتند و کس را آمدن نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه کس از رفتگان دارد نشانی</p></div>
<div class="m2"><p>نه کس دیدست زین وادی کرانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهانی جان درین محنت دو نیم است</p></div>
<div class="m2"><p>که داند کین چه گردابی عظیم است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهانی سر درین ره گوی راهست</p></div>
<div class="m2"><p>که داند کین چه وادی سیاه است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جهانی خلق درغرقاب خونند</p></div>
<div class="m2"><p>که می‌داند که زیر خاک چونند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جهان را کرده ناکرده ست جمله</p></div>
<div class="m2"><p>که بازییی پس پرده‌ست جمله</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه مقصودست چندین رنج بردن</p></div>
<div class="m2"><p>که چون شمعی فرو خواهیم مردن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جهان بی هیچ باقی خوش سراییست</p></div>
<div class="m2"><p>ولی چون نیست باقی این بلاییست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جهان بگذار و بگذر زین سخن زود</p></div>
<div class="m2"><p>چو باقی نیست در باقیش کن زود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو تا بودی ز دنیا خسته بودی</p></div>
<div class="m2"><p>بهر ره جان کنی پیوسته بودی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه هرگز لقمه‌ای بی قهر خوردی</p></div>
<div class="m2"><p>نه هرگز شربتی بی زهر خوردی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هزاران سیل خونین بر دلت بست</p></div>
<div class="m2"><p>که تا بادی ز عالم بر دلت جست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو خود اندیشه کن گر کاردانی</p></div>
<div class="m2"><p>که تا خود مرگ به یا زندگانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هزاران غم فرو آمد برویت</p></div>
<div class="m2"><p>که تا یک آب آمد در گلویت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همه دنیا بیک جو غم نیرزد</p></div>
<div class="m2"><p>چه یک جو نیم ارزن هم نیرزد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>غم دنیا مخور ای دوست بسیار</p></div>
<div class="m2"><p>که در دنیا نخواهد ماند دیار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چه می‌نازی بدین دنیای غدار</p></div>
<div class="m2"><p>که تو گرکس نیی گر اوست مردار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همه تخم جهان برداشته گیر</p></div>
<div class="m2"><p>بدست آورده و بگذاشته گیر</p></div></div>