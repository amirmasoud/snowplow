---
title: >-
    بخش ۶ - الحکایه و التمثیل
---
# بخش ۶ - الحکایه و التمثیل

<div class="b" id="bn1"><div class="m1"><p>عزیزی گفت من عمری درین کار</p></div>
<div class="m2"><p>بعقد و جد در بودم گرفتار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو پنهان می‌شدم من خود نبودم</p></div>
<div class="m2"><p>چو پیدا می‌شدم بودم چه سودم</p></div></div>