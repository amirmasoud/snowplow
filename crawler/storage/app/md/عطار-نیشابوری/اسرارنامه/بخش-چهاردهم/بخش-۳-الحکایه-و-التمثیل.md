---
title: >-
    بخش ۳ - الحکایه و التمثیل
---
# بخش ۳ - الحکایه و التمثیل

<div class="b" id="bn1"><div class="m1"><p>شنودم کز سلف درویش حالی</p></div>
<div class="m2"><p>هوای قلیه‌ای بودش بسالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو سیمی دست داد آن مرد درویش</p></div>
<div class="m2"><p>سوی قصاب راه آورد در پیش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر قصاب ناخوش زندگانی</p></div>
<div class="m2"><p>بدادش گوشتی چو نان که دانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو پیر آن گوشت الحق نه چنان دید</p></div>
<div class="m2"><p>سراسر یا جگر یا استخوان دید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جگر خود بود یکباره دگر خواست</p></div>
<div class="m2"><p>که کار ما نیاید بی جگر راست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل ما غرقهٔ خون شد بیک بار</p></div>
<div class="m2"><p>چه می‌خواهند زین مشتی جگر خوار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه ما را طاقت بارگران است</p></div>
<div class="m2"><p>نه ما را برگ بی برگی جانست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنان غم یار ما شد در غم یار</p></div>
<div class="m2"><p>که نیست از کار غم ما را غم کار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر گردون بمرگ ما کند ساز</p></div>
<div class="m2"><p>غم عشقش کفن ازما کند باز</p></div></div>