---
title: >-
    بخش ۴ - الحکایه و التمثیل
---
# بخش ۴ - الحکایه و التمثیل

<div class="b" id="bn1"><div class="m1"><p>شنودم من که جایی بی دلی بود</p></div>
<div class="m2"><p>نه از دل هم چو مابی حاصلی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زدندش کودکان سنگی زهر راه</p></div>
<div class="m2"><p>تگرگی نیز پیدا گشت ناگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسوی آسمان برداست سر را</p></div>
<div class="m2"><p>که چون بردی دل این بی خبر را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تگرگ و سنگ کردی بر تنم بار</p></div>
<div class="m2"><p>شدی تو نیز با این کودکان یار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه می‌گویم برو ای غافل مست</p></div>
<div class="m2"><p>که یار تو نیالاید بتو دست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیی تو اهل یار و یار دورست</p></div>
<div class="m2"><p>تو دور از کار وز تو کار دورست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یقین می‌دان که خورشید سرافراز</p></div>
<div class="m2"><p>نخواهد شد بسوی کس سرانداز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بپیش آفتاب نام بردار</p></div>
<div class="m2"><p>چه سارخک و چه پیل آید پدیدار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فراغت بین که در بنیاد کارست</p></div>
<div class="m2"><p>مچخ کین کار ساز استادکارست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سخن در پرده گوی از پرده سازی</p></div>
<div class="m2"><p>رها کن این خیال و پرده بازی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو شادی نیست دل در غم فروبند</p></div>
<div class="m2"><p>چوهم دم نیست بر لب دم فروبند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جوامردا سخن در پرده می‌دار</p></div>
<div class="m2"><p>که با هر دون نشاید گفت اسرار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرا عمریست تادر بند آنم</p></div>
<div class="m2"><p>که تا با هم دمی رمزی برانم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نمی‌یابم یکی هم دم موافق</p></div>
<div class="m2"><p>فغان زین هم نشینان منافق</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر این کار ما از هم نشین است</p></div>
<div class="m2"><p>عذاب دوزخ از بئس القرینست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دلا خاموش چون محرم نیابی</p></div>
<div class="m2"><p>مزن دم زانک یک هم دم نیابی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو مردان خوی کن دایم سه طاعت</p></div>
<div class="m2"><p>خموشی و صبوری و قناعت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>طریق مرد عزلت جوی کن ساز</p></div>
<div class="m2"><p>اگر مردی ز مردم خوی کن باز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ترا مردان دنیا ره زنانند</p></div>
<div class="m2"><p>مگر مردان نیند ایشان زنانند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز یک سو باده و زیک سوی شاهد</p></div>
<div class="m2"><p>جیان خلق چون مانی توزاهد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یکی در سور دیگر در مصیبت</p></div>
<div class="m2"><p>زفان و دل پر از تزویر و غیبت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جهان از گفت بیهوده برآمد</p></div>
<div class="m2"><p>همه عالم درای استر آمد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>درین ره صد هزاران سر چو گوییست</p></div>
<div class="m2"><p>چه جای کار و بار و گفت و گوییست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگر جان گویم اندر خون بماندست</p></div>
<div class="m2"><p>وگر تن او ز در بیرون بماندست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو جان سر باز نشناسید از پای</p></div>
<div class="m2"><p>چه آید زین تن افتاده بر جای</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو در خونابه می‌گردند جانها</p></div>
<div class="m2"><p>چه برخیزد ز بوده استخوانها</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بزرگان را رخی پر اشک خونیست</p></div>
<div class="m2"><p>چه جای خرده گیران کنونیست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کسی کز عقل صد کل را کلاه است</p></div>
<div class="m2"><p>ز کوری همچو می مغزان راهست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو موسی هرک کوران را عصا شد</p></div>
<div class="m2"><p>ز فرعونان ره پیرش خطا شد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نه چندانست در ره ره زن تو</p></div>
<div class="m2"><p>که گر گویم بگرید دشمن تو</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ضرورت می‌بباید شد چه پیچی</p></div>
<div class="m2"><p>توکل کن که او داند که هیچی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>براه عاشقان بر زن قدم تو</p></div>
<div class="m2"><p>چه باشی از سگی در راه کم تو</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>که آن سگ چون ازین ره شمهٔ یافت </p></div>
<div class="m2"><p>بسنگ و چوب زین ره سر نمی‌تافت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نمی‌خورد و نه یک دم خواب می‌کرد</p></div>
<div class="m2"><p>نگه بانی آن اصحاب می‌کرد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تو گرد مرد رهی در ره فرو شو</p></div>
<div class="m2"><p>قدم در نه فدای راه او شد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گرت گویند سر در راه ما باز</p></div>
<div class="m2"><p>بدین شادی تو دستاراند انداز</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بصد حمله سپر گر بفکنی تو</p></div>
<div class="m2"><p>چو آن دیوانه بس تر دامنی تو</p></div></div>