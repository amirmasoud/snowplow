---
title: >-
    بخش ۱۲ - الحکایه و التمثیل
---
# بخش ۱۲ - الحکایه و التمثیل

<div class="b" id="bn1"><div class="m1"><p>حکایت کرد ما را نیک خواهی</p></div>
<div class="m2"><p> که در راه بیابان بود چاهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن چه آب می‌جستم که ناگاه</p></div>
<div class="m2"><p>فتاد انگشتری از دست در چاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرستادم یکی را زیر چه سار</p></div>
<div class="m2"><p>که چندانی که بینی زیر چه بار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه در دلو کن تا برکشم من</p></div>
<div class="m2"><p>بود کانگشتری بر سر، کشم من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشیدم چند دلو بار از چاه</p></div>
<div class="m2"><p>فراوان بار جستم بر سر راه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی سنگ سیه دیدم در آن خاک</p></div>
<div class="m2"><p>چو گویی شکل او بس روشن و پاک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برافکندم که تا سنگی گران هست</p></div>
<div class="m2"><p>ز دستم بر زمین افتاد وبشکست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دو نیمه گشت و کرمی از میانش</p></div>
<div class="m2"><p>برآمد سبز برگی در دهانش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زهی منعم که در پروردگاری</p></div>
<div class="m2"><p>میان سنگ کرمی را بداری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بچاه تیره در راه بیابان</p></div>
<div class="m2"><p>میان سنگ کرمی را نگه بان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حریصا لطف رزاقی او بین</p></div>
<div class="m2"><p>عطا و نعمت باقی او بین</p></div></div>