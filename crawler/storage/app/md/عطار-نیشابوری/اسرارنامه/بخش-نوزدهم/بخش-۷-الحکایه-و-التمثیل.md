---
title: >-
    بخش ۷ - الحکایه و التمثیل
---
# بخش ۷ - الحکایه و التمثیل

<div class="b" id="bn1"><div class="m1"><p>شبی خفت آن گدایی در تنوری</p></div>
<div class="m2"><p>شهی را دید می‌شد در سموری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمستان بود و سرما بود بسیار</p></div>
<div class="m2"><p>گدا با شاه گفت ای شاه هشیار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو گرچه بی‌خبر بودی ز سرما</p></div>
<div class="m2"><p>فرا سرآمد این شب نیز بر ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عزیزا در بن این دیر گردان</p></div>
<div class="m2"><p>صبوری و قناعت کن چو مردان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بمردی صبر کن بر جای بنشین</p></div>
<div class="m2"><p>بسر می در مدو وز پای بنشین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حکیمی در مثل رمزی نمودست</p></div>
<div class="m2"><p>که صبر اندر همه کاری ستودست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه خذلان مردم از شتابست</p></div>
<div class="m2"><p>خرد را این سخن چون آفتابست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شتاب ازحرص دارد جان مردم</p></div>
<div class="m2"><p>نگه کن حرص آدم بین و گندم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر نه حرص در دل راه داری</p></div>
<div class="m2"><p>کجا از جنت الماوی فتادی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز آدم حرص میراثست ما را</p></div>
<div class="m2"><p>درازا محنتا آشفته کارا</p></div></div>