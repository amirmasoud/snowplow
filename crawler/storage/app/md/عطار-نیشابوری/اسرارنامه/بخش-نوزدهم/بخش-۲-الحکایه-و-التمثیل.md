---
title: >-
    بخش ۲ - الحکایه و التمثیل
---
# بخش ۲ - الحکایه و التمثیل

<div class="b" id="bn1"><div class="m1"><p>درآمد آن فقیر از خانقاهی</p></div>
<div class="m2"><p>نهاده بر سر از ژنده کلاهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی گفتش بطیبت ای خردمند</p></div>
<div class="m2"><p>کلاه ار می‌فروشی قیمتش چند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جواب این بود آن درویش دین را</p></div>
<div class="m2"><p>بکل کون نفروشم من این را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسی خلقم خریدار کلاه‌اند</p></div>
<div class="m2"><p>بکل کون از من می بخواهند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنفروشم که دانم بهتر ارزد</p></div>
<div class="m2"><p>که یک نخ زو دو گیتی گوهر ارزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه دانی تو که من در سر چه دارم</p></div>
<div class="m2"><p>چو من خود بی سرم افسر چه دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلا بیدار شو گر هست دردیت</p></div>
<div class="m2"><p>که ناوردند بهر خواب و خوردیت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرفتم جملهٔ عالم بخوردی</p></div>
<div class="m2"><p>ندانی جستن از مردن بمردی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ترا تا کی ز تو ای آفت خویش</p></div>
<div class="m2"><p>تویی آفت تو هم برخیز از پیش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگو تا کی ز بی شرمی و شوخی</p></div>
<div class="m2"><p>چه سنگین دل کسی، کویی کلوخی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بکن هرچت همی باید کژ و راست</p></div>
<div class="m2"><p>اگر این را نخواهد بود واخواست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر چون خاک ره زر خواهدت بود</p></div>
<div class="m2"><p>ز خاک راه بستر خواهد بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ترا چرخ فلک در چرخه انداخت</p></div>
<div class="m2"><p>که بر یک جو زرت صد نرخه انداخت</p></div></div>