---
title: >-
    بخش ۲ - الحکایه و التمثیل
---
# بخش ۲ - الحکایه و التمثیل

<div class="b" id="bn1"><div class="m1"><p>شنودم من که پیری بود کامل</p></div>
<div class="m2"><p>نه چون پیران دیگر مانده غافل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه شب خفتی و نه روز آرمیدی</p></div>
<div class="m2"><p>بروز و شب کسش خفته ندیدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی پرسید کای پیر دل افروز</p></div>
<div class="m2"><p>چرا هرگز نه شب خفتی و نه روز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدو گفتا نخسبد مرد دانا</p></div>
<div class="m2"><p>بهشت و دوزخش در شیب و بالا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی پیوسته می‌تابند در شیب</p></div>
<div class="m2"><p>دگر را می‌دهند آرایش و زیب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میان خلد و دوزخ در زمانه</p></div>
<div class="m2"><p>چگونه خوابم آید در میانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیاوردست کس خطی بنامم</p></div>
<div class="m2"><p>که تا من زین دو جا اهل کدامم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلی پر تفت و جانی پر تب و تاب</p></div>
<div class="m2"><p>چگونه یابد آخر چشم من خواب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو دل پر تفت و جان پرتاب باشد</p></div>
<div class="m2"><p>نگوساری من درخواب باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هزاران جان پاک نامداران</p></div>
<div class="m2"><p>فدای خلوت بیدارداران</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عزیزا چند خسبی چشم کن باز</p></div>
<div class="m2"><p>پس زانوی خود خلوت کن آغاز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مباش آخر از آن مستی پریشان</p></div>
<div class="m2"><p>که شب مهتاب بنماید بدیشان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چرا خفتی شب مهتاب آخر</p></div>
<div class="m2"><p>چه خواهد آمدن زین خواب آخر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیندیشی که چون عمرت سرآید</p></div>
<div class="m2"><p>بسی مهتاب در گورت درآید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ترا زیر کفن بگرفته خوابی</p></div>
<div class="m2"><p>فرو آید بگورت ماهتابی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>براندیشد کسی چون خواب یابد</p></div>
<div class="m2"><p>که در گورش بسی مهتاب تابد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شب مهتاب چون می‌آیدت خواب</p></div>
<div class="m2"><p>که عاشق خواب کم یابد بمهتاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نکو نبود چه گوید مرد هشیار</p></div>
<div class="m2"><p>بخفته عاشق و معشوق بیدار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چه معشوق و چه عاشق این چه لافست</p></div>
<div class="m2"><p>بخاکی کی رسد پاکی گزافست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو مرد گلخن نفس و هوایی</p></div>
<div class="m2"><p>کجا مردان عشق پادشایی</p></div></div>