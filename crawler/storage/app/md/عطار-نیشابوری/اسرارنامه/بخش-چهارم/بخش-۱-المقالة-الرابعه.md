---
title: >-
    بخش ۱ - المقالة الرابعه
---
# بخش ۱ - المقالة الرابعه

<div class="b" id="bn1"><div class="m1"><p>الا ای جان و دل را درد و دارو</p></div>
<div class="m2"><p>تو آن نوری که کم تمسه نارو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز روزنهای مشکاتی مشبک</p></div>
<div class="m2"><p>نشیمن کرده بر شاخی مبارک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو در مصباح تن مشکوة نوری</p></div>
<div class="m2"><p>ز نزدیکی که هستی دور دوری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زجاجه بشکن و زیتت فروریز</p></div>
<div class="m2"><p>بنور کوکب دری درآویز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترا با مشرق و مغرب چه کارست</p></div>
<div class="m2"><p>که نور آسمان گردت حصارست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>الا ای بلبل گویای اسرار</p></div>
<div class="m2"><p>ز صندوق جواهر بند بردار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو عیسی در سخن شیرین زفان شو</p></div>
<div class="m2"><p>صدف را بشکن و گوهر فشان شو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بآواز خوش خود سر میفراز</p></div>
<div class="m2"><p>که در ابریشم ونی هست آواز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوش آوازی بلبل از تو بیش است</p></div>
<div class="m2"><p>که سرمست خوش آوازی خویش است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز شنوائی خود چندین بمخروش</p></div>
<div class="m2"><p>که بانگی بشنود ده میل خرگوش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز بینائی مدان این فر و فرهنگ</p></div>
<div class="m2"><p>که گنجشکی ببیند بیست فرسنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز بویایی ناقص نیز کم گوی</p></div>
<div class="m2"><p>که از یک میل موشی بشنوی بوی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زوهم خود مدان خود را تزید</p></div>
<div class="m2"><p>که آب از وهم خود بنمود هدهد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو گر بیشی از آن جمله از آنی</p></div>
<div class="m2"><p>که بس گویا و بس پاکیزه دانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>الا ای قطره بالا گزیده</p></div>
<div class="m2"><p>ز دریای قدم بویی شنیده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز دریا گرچه بالایی گزیدی</p></div>
<div class="m2"><p>ولیکن در کمال خود رسیدی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو از دریا سوی بالا شدی تو</p></div>
<div class="m2"><p>صدف را لولوی لالا شدی تو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو ناکرده سفر گوهر نگردی</p></div>
<div class="m2"><p>چو خاکستر شدی اخگر نگردی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سفر کردی ز دریا سوی عنصر</p></div>
<div class="m2"><p>سفر ناکرده قطره کی شود در</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نخستین قطره باران سفر کرد</p></div>
<div class="m2"><p>و از آن پس قعر دریا پر گهر کرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بدریا گر گهر پنهان بماند</p></div>
<div class="m2"><p>گهر با خاک ره یکسان بماند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ولی چون گوهر از دریا برآید</p></div>
<div class="m2"><p>ززیر طشت پر زر با سرآید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو برگ تود از موضع سفر کرد</p></div>
<div class="m2"><p>ز دیبا و ز اطلس سر بدر کرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سفر را گرنه این انجام بودی</p></div>
<div class="m2"><p>فلک را یک نفس آرام بودی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سفر را گر چنین قدری نبودی</p></div>
<div class="m2"><p>مه نو از سفر بدری نبودی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>الا ای نیک یار تند مستیز</p></div>
<div class="m2"><p>دمی زین چارچوب طبع برخیز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بپرواز جهان لامکان شو</p></div>
<div class="m2"><p>زمانی بی زمین و بی زمان شو</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که اندر لازمان صد سال و یک دم</p></div>
<div class="m2"><p>بپیشت هر دو یکسانند با هم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دمی آنجایگه صد سال باشد</p></div>
<div class="m2"><p>ز استقبال و ماضی حال باشد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ولیکن حال نبود در زمانی</p></div>
<div class="m2"><p>از آن معنی که نبود آسمانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نیابی انقضای دور دوران</p></div>
<div class="m2"><p>نبینی انقلاب چرخ گردان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو نور دیده باشد آسمانها</p></div>
<div class="m2"><p>نباشد چون چنینها آنچنانها</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نه نقصان باشد آنجا نه کمالی</p></div>
<div class="m2"><p>نه ماضی و نه مستقبل نه حالی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چوهست آن حضرت از هر دو جهان دور</p></div>
<div class="m2"><p>ازآنست از زمان و از مکان دور</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بود در یک نفس مهدی و آدم</p></div>
<div class="m2"><p>نه آن یک بیش ازین نه این از آن کم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو حالی این زمین کردی بدل تو</p></div>
<div class="m2"><p>یکی بینی ابد را با ازل تو</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو آنجا نه چه ونه چند باشد</p></div>
<div class="m2"><p>ازل را با ابد پیوند باشد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یقین دانم که هر دو جز یکی نیست</p></div>
<div class="m2"><p>محقق را درین معنی شکی نیست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>الا یا مهره باز حقه پرداز</p></div>
<div class="m2"><p>نقاب از لعبت معنی برانداز</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مشعبدوار چابک دستی کن</p></div>
<div class="m2"><p>شرابی درکش و بدمستی کن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بخاک آینه جان پاک بزدای</p></div>
<div class="m2"><p>تهی کن حقه را و پاک بنمای</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز بند پیچ بر پیچ زمانه</p></div>
<div class="m2"><p>گرفتار آمدی در کنج خانه</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اگر تو روی بنمائی ز پرده</p></div>
<div class="m2"><p>بسوزی هفت چرخ سال خورده</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تو گنجی نه سپهرت درمیانه</p></div>
<div class="m2"><p>برآی از چار دیوار زمانه</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>طلسم و بند نیز نجات بشکن</p></div>
<div class="m2"><p>در و دهلیز موجودات بشکن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>تو گنجی لیک در بند طلسمی</p></div>
<div class="m2"><p>تو جانی لیک در زندان جسمی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ازین زندان دنیا رخت برگیر</p></div>
<div class="m2"><p>بکلی دل ز بند سخت برگیر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>میان پارگین و آز ماندی</p></div>
<div class="m2"><p>نمی‌دانی که ازچه باز ماندی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تو معذوری که آگاهی نداری</p></div>
<div class="m2"><p>که اینجا آنچ می‌خواهی نداری</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو از حق برگ رندان می‌نیابی</p></div>
<div class="m2"><p>عجب نبود اگر آن می‌نیابی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>الا یا مرغ حکمت دان زمانی</p></div>
<div class="m2"><p>چه خواهی یافت زین به آشیانی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بپرواز معانی باز کن پر</p></div>
<div class="m2"><p>سرای هفت در را باز کن در</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چو بگذشتی ز چار ونه بپرواز</p></div>
<div class="m2"><p>ز خود بگذر بحق کن چشم خود باز</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چرا مغرور جای دیو گشتی</p></div>
<div class="m2"><p>تو دیوانه شدی کالیو کشتی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چو میدانی که می‌باید شدن زود</p></div>
<div class="m2"><p>نه خواهد نیز روی آمدن بود</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چه خواهی کرد جای مکر و تلبیس</p></div>
<div class="m2"><p>ز دنیا بگذر و بگذار ابلیس</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بدان کاقطاع ابلیس است دنیا</p></div>
<div class="m2"><p>سرای مکر و تلبیس است دنیا</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>سرای او بدو ده باز رفتی</p></div>
<div class="m2"><p>نظر بر پیشگاه انداز و رفتی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چو نست ابلیس را با جای تو کار</p></div>
<div class="m2"><p>تو نیز از جای او بگذر بهنجار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چو زین گلخن بدان گلشن رسیدی</p></div>
<div class="m2"><p>همان انگار کین گلخن ندیدی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>نخستین در جهان قدس بخرام</p></div>
<div class="m2"><p>وزان پس در جهان انس نه گام</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چو بر استبرق خضرا نشینی</p></div>
<div class="m2"><p>تو باشی جمله و خود را نه بینی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چو بگذشتی ز چندان پرده و دام</p></div>
<div class="m2"><p>بیک چندی شوی هادی بر آن بام</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>شود چشمت بخورشید جهان باز</p></div>
<div class="m2"><p>شود بر تو در دریای جان باز</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چو تو هادی شدی در خود نگه کن</p></div>
<div class="m2"><p>بدان خود را و قصد بارگه کن</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>که چون خود دان شوی حق دان شوی تو</p></div>
<div class="m2"><p>از آن پس زود در پیشان شوی تو</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>اگر هستی حجابی پیشت آرد</p></div>
<div class="m2"><p>از آن حالت دمی با خویشت آرد</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چو هستی تو ننماید بر او</p></div>
<div class="m2"><p>ز خود بی خود بمانی بر در او</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>دگرره پرده‌ای در پیش آید</p></div>
<div class="m2"><p>خودی در بی خودی با خویش آید</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چو آگه شد شود لذت پدیدار</p></div>
<div class="m2"><p>ز شادی در خروش آید دگر بار</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چو پروانه بر آتش می‌زند خویش</p></div>
<div class="m2"><p>که تا هستی او برخیزد از پیش</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>چو برخیزد حجاب هستی او</p></div>
<div class="m2"><p>دگر ره قوت آرد مستی او</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>گهی افتان گهی خیزان بماند</p></div>
<div class="m2"><p>گهی بیجان گهی با جان بماند</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>گهی در لذتی گه در فنایی</p></div>
<div class="m2"><p>گهی در فرقتی گه در بقایی</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بگویم این سخن سرباز با تو </p></div>
<div class="m2"><p>که گه غم چیست گاهی ناز با تو</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>قدم را با حدوث آویزشی نیست</p></div>
<div class="m2"><p>وگر آویز شست آمیزشی نیست</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>کنون ای آفتاب سایه پرورد</p></div>
<div class="m2"><p>که گفتت کز کنار دایه برگرد</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چو تودر عالم حادث شتابی</p></div>
<div class="m2"><p>ز نور عالم ثالث چه یابی</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>الا ای مرغ بیرون آی ازین دام</p></div>
<div class="m2"><p>دمی در مرغزار خلد بخرام</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چو هستی بر دل اسرارگشته</p></div>
<div class="m2"><p>ز شاخ عشق برخوردار گشته</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بگردان روی از دیوار آخر</p></div>
<div class="m2"><p>فرو شو در پی اسرار آخر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>همی هر ذره از عالم که بینی</p></div>
<div class="m2"><p>اگر تو در پی آن می‌نشینی</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>چنان پیدا شود آن ذره در راه</p></div>
<div class="m2"><p>که نوری گردد از انوار درگاه</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>شود هر ذرهٔ چون آفتابی</p></div>
<div class="m2"><p>پدید آید حجابی از حجابی</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>برون می‌آید از استار اسرار</p></div>
<div class="m2"><p>رهی دور و نهایت ناپایدار</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>نه هرگز هیچ کس پیشانش یابد</p></div>
<div class="m2"><p>نه هرگز غایت و پایانش یابد</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>چنین گفتست طاهر پاک بازی</p></div>
<div class="m2"><p>که من چل سال ماندم در نیازی</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>ز یک یک ذره سوی دوست راهست</p></div>
<div class="m2"><p>ولی برچشم تو عالم سیاه است</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>نهادت پرده و دادت بسی هیل</p></div>
<div class="m2"><p>که تا نا اهل پیدا آید از اهل</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>تو گر اهلیتی داری درین راه</p></div>
<div class="m2"><p>ز یک یک ذره می شو تا بدرگاه</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>ز پیشان گر نظر بر تو نبودی</p></div>
<div class="m2"><p>ز سوی تو سفر بر تو نبودی</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>ولی چون نور پیشان رهبر تست</p></div>
<div class="m2"><p>چرا این کاهلی در جوهر تست</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>ببین آخر اگر داری حضوری</p></div>
<div class="m2"><p>که هر دم می‌رسد از یار نوری</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>ز تو گر یار گیرد یک نظر باز</p></div>
<div class="m2"><p>به دیناری نبابی هیچ زنار</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>اگر روشن کنی آیینه دل</p></div>
<div class="m2"><p>دری بگشایدت در سینه دل</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>دری کان در چو بر دلبر گشاید</p></div>
<div class="m2"><p>فلک را پرده داری برنشاید</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>تو را سه چیز می‌باید ز کونین</p></div>
<div class="m2"><p>بدانستن عمل کردن شدن عین</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>چو علمت از عبادت بین گردد</p></div>
<div class="m2"><p>دلت آیینه کونین گردد</p></div></div>