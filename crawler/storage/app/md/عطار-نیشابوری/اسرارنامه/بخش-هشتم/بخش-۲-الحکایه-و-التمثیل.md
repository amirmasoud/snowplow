---
title: >-
    بخش ۲ - الحکایه و التمثیل
---
# بخش ۲ - الحکایه و التمثیل

<div class="b" id="bn1"><div class="m1"><p>سیاهی کرد در آبی نگاهی</p></div>
<div class="m2"><p>بدید از آب رویی پر سیاهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو رویی دید نامعلوم و ناخوش</p></div>
<div class="m2"><p>از آن زشتی دویدش بر سر آتش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان اندیشه کرد آن مرد دل تنگ</p></div>
<div class="m2"><p>که هست آن مردم آب سیه رنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زفان بگشاد گفت ای صورت زشت</p></div>
<div class="m2"><p>کدامین دیو در عالم ترا کشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برآی از آب ای زشت سیه تاب</p></div>
<div class="m2"><p>که در آتش همی پایی نه در آب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو بر بیهوده بسیاری سخن گفت</p></div>
<div class="m2"><p>ندانست و همه با خویشتن گفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو هم در آب رویت کن نگاهی</p></div>
<div class="m2"><p>ببین تا خود سپیدی یا سیاهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو مرغ جان فرو ریزد پر و بال</p></div>
<div class="m2"><p>ببینی روی خود در آب اعمال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیه رویی سیاهی پیشت آرد</p></div>
<div class="m2"><p>سپیدی در فروغ خویشت آرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو جان پاک در یک دم بدادی</p></div>
<div class="m2"><p>قدم حالی در آن عالم نهادی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز دنیا تا بعقبی نیست بسیار</p></div>
<div class="m2"><p>ولی در ره وجود تست دیوار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ترا بانگ و خروش و گریه چندانست</p></div>
<div class="m2"><p>که این نفس دبی هم صحبت جانست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر با نفس میری وای بر تو</p></div>
<div class="m2"><p>بسی گرید ز سر تا پای بر تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وگر بی نفس میری پاک باشی</p></div>
<div class="m2"><p>چه اندر آتش و در خاک باشی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ترا چو جان پاکت رفت و تن مرد</p></div>
<div class="m2"><p>نباید خویش را با خویشتن برد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که هر گاهی که تو از پیش مردی</p></div>
<div class="m2"><p>بسا کس را که گوی از پیش بردی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زبانت هرچ بر خود می‌شمرد آن</p></div>
<div class="m2"><p>چو زیر خاک رفتی باد برد آن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از آن پس عالم خاموشی آید</p></div>
<div class="m2"><p>مقامات ره مدهوشی آید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>برون پرده آید شور ایام</p></div>
<div class="m2"><p>درون پرده خاموشیست و آرام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو اینجایی ز خود آگاه از خویش</p></div>
<div class="m2"><p>که آنجا اگهی برخیزد از پیش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنان مستغرق آن نور گردی</p></div>
<div class="m2"><p>که زان لذت ز هستی دور گردی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>و گر داری ازین برتر مقامی</p></div>
<div class="m2"><p>توداری اندرین قربت نظامی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مقرب آن بود کامروز بی خویش</p></div>
<div class="m2"><p>بود آن حضرتش در پیش بی پیش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همه حق بیند و بی خویش گردد</p></div>
<div class="m2"><p>بجوهر از دو گیتی بیش گردد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>درین معنی که من گفتم شکی نیست</p></div>
<div class="m2"><p>تو بی‌چشمی و عالم جز یکی نیست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مثالی باز گویم با تو از راه</p></div>
<div class="m2"><p>مگر جانت شود زین راز آگاه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چه گر عمری بخون گردیدهٔ تو</p></div>
<div class="m2"><p>مثالی مثل این نشنیدهٔ تو</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بچشمت کی درآید چرخ گردون</p></div>
<div class="m2"><p>که قدر او ز چشم تست افزون</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همی هر ذرهٔ کان دیدهٔ تو</p></div>
<div class="m2"><p>نیاید عین آن در دیدهٔ تو</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>که می‌گوید که گردون آن چنانست</p></div>
<div class="m2"><p>که چشمت دید یا عقل تو دانست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پس آن چیزی که شد در چشم حاصل</p></div>
<div class="m2"><p>مثالی بیش نیست ای مرد غافل</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گرفتار آمدی در بند تمییز</p></div>
<div class="m2"><p>مثالست این چه می‌بینی نه آن چیز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بصنع حق نگر تا راز بینی</p></div>
<div class="m2"><p>حقیقتهای اشیا باز بینی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>اگر اشیا چنین بودی که پیداست</p></div>
<div class="m2"><p>سئوال مصطفی کی آمدی راست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>که با حق مهتر دین گفت الهی</p></div>
<div class="m2"><p>بمن بنمای اشیا را کماهی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اگر پاره کنی دل را بصد بار</p></div>
<div class="m2"><p>نیاید آنچ دل باشد پدیدار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همین چشم و همین دست و همین گوش</p></div>
<div class="m2"><p>همین جان و همین عقل و همین هوش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اگر زین می نیاری گشت آگاه</p></div>
<div class="m2"><p>مهر زینجا سوی فسطانیان راه</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خدا داند که خود اشیا چگونست</p></div>
<div class="m2"><p>که در چشم تو باری با شکونست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بماند از مغز معنی پوست با تو</p></div>
<div class="m2"><p>مثالی بیش نیست ای دوست با تو</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تو پنداری که چیزی دیدهٔ تو</p></div>
<div class="m2"><p>ندیدستی تو و نشنیدهٔ تو</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مثال آن همی بینی وگرنه</p></div>
<div class="m2"><p>یکیست این جمله در اصل و دگر نه</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>یکی کان یک برون باشد ز آحاد</p></div>
<div class="m2"><p>نه آن یک را نشان باشد نه اعداد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>همه باقی بیک چیزند جاوید</p></div>
<div class="m2"><p>ز یک یک ذره می شو تا بخورشید</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دو عالم غرق این دریای نور است</p></div>
<div class="m2"><p>ولیکن نقش عالم ها غرورست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>هر آن نقشی که در عالم پدیدست</p></div>
<div class="m2"><p>دری بستست و حس آنرا کلیدست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>کلید و در از آن پیدا نماند</p></div>
<div class="m2"><p>که هرگز نقش بر دریا نماند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>کسی کو نقش بی نقشی پذیرفت</p></div>
<div class="m2"><p>چو مردان ترک این صورت گری گفت</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>اگر بی صورتی و بی نشانی</p></div>
<div class="m2"><p>پذیرفتی تو داری زندگانی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>وگرنه مردهٔ مغرور می باش</p></div>
<div class="m2"><p>نداری زندگی از دور می‌باش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>اگر گویی که چیست این هرچ پیداست</p></div>
<div class="m2"><p>بگویم راست گر تو بشنوی راست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>همه ناچیز و فانی و همه هیچ</p></div>
<div class="m2"><p>همه همچون طلسمی پیچ بر پیچ</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>خیالست آنچ دانستی و دیدی</p></div>
<div class="m2"><p>صدایست آنچ در عالم شنیدی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>خیال و وهم و عقل و حس مقامست</p></div>
<div class="m2"><p>که هر یک در مقام خود تمامست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ولی چون زان مقام آبی برون تو</p></div>
<div class="m2"><p>خیالی بینی آن را هم کنون تو</p></div></div>