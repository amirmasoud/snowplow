---
title: >-
    بخش ۳ - الحکایه و التمثیل
---
# بخش ۳ - الحکایه و التمثیل

<div class="b" id="bn1"><div class="m1"><p>یکی پرسید از آن دیوانه مجنون</p></div>
<div class="m2"><p>که عالم چیست گفتا کفک صابون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بما سوره بگیر آن کفک و در دم</p></div>
<div class="m2"><p>برون آور از آن ماسوره عالم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ببین این شکل رنگارنگ زیبا</p></div>
<div class="m2"><p>کز آن ماسوره می‌گردد هویدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگرچه صورتی بس دلستانست</p></div>
<div class="m2"><p>دوم صورت که احول بیند آنست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فنا ملک و زوالش مالک آمد</p></div>
<div class="m2"><p>اساسش کل شئی هالک آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میانش باد و او خود هیچ هیچی</p></div>
<div class="m2"><p>ز هیچی هیچ ناید چند پیچی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شود فانی نماید ناگهان کم</p></div>
<div class="m2"><p>جهان در هیچ و هیچ اندر جهان گم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر نور دلت گردد پدیدار</p></div>
<div class="m2"><p>نه درچشم تو درماند نه دیوار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه در دل شود چون ذرهٔ گم</p></div>
<div class="m2"><p>بلی در بحر گردد قطرهٔ گم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عصا در دست موسی اژدها شد</p></div>
<div class="m2"><p>همه باطل فرو برد و عصا شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگفتم جملهٔ اسرار سر باز</p></div>
<div class="m2"><p>حجاب آخر ز پیش خود برانداز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر این پرده از هم بر درانی</p></div>
<div class="m2"><p>همه جز یک نبینی و ندانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زهی عطار خوش گفتار بادی</p></div>
<div class="m2"><p>وزین گفتار برخوردار بادی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر بر نیستی از شاخ معنیت</p></div>
<div class="m2"><p>نکردندی چنین گستاخ معنیت</p></div></div>