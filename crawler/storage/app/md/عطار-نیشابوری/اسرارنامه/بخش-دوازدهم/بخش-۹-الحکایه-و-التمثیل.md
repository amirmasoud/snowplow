---
title: >-
    بخش ۹ - الحکایه و التمثیل
---
# بخش ۹ - الحکایه و التمثیل

<div class="b" id="bn1"><div class="m1"><p>بمنبر بر امامی نغز گفتار</p></div>
<div class="m2"><p>ز هر نوعی سخن می‌گفت بسیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی دیوانه گفتش چه می‌گویی</p></div>
<div class="m2"><p>ز چندین گفت آخر می چه جویی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جوابش داد حالی مرد هشیار</p></div>
<div class="m2"><p>که چل سالست تا می‌گویم اسرار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر مجلس یکی غسلی بیارم</p></div>
<div class="m2"><p>چنین مجلس چرا آخر ندارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جوابش داد آن مجنون مفلس</p></div>
<div class="m2"><p>که چل سال دگرمی گوی مجلس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همی کن غسل و این اسرار می‌گوی</p></div>
<div class="m2"><p>گهی قرآن و گه اخبار می‌گوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو سال تو رسد از چل به هشتاد</p></div>
<div class="m2"><p>بنزدیک من آی آنگه چون باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کواره با خود آر ای دوغ خواره</p></div>
<div class="m2"><p>که با دوغت کنم اندر کواره</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بعمری این کواره بافتی تو</p></div>
<div class="m2"><p>ولیکن دوغ در وی یافتی تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سبد در آب داری می ندانی</p></div>
<div class="m2"><p>سر اندر خواب داری می ندانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بسی خورشید اندر دشت تابد</p></div>
<div class="m2"><p>ولیکن دشت او را درنیابد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا صبرست تا این طبل پر باد</p></div>
<div class="m2"><p>دریده گردد و بی بانگ و فریاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر بینا شود چشمت باسرار</p></div>
<div class="m2"><p>نماند عالم ودیار و آثار</p></div></div>