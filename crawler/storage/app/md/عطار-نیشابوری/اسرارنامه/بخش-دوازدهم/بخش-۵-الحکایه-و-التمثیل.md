---
title: >-
    بخش ۵ - الحکایه و التمثیل
---
# بخش ۵ - الحکایه و التمثیل

<div class="b" id="bn1"><div class="m1"><p>حکیمی را یکی زر در بدل زد</p></div>
<div class="m2"><p>حکیم اندر حق او این مثل زد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که در دامت چنان آرم بمردی</p></div>
<div class="m2"><p>که بر یک جست ده گردم بگردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زهی هیبت که گردون یک اثر دید</p></div>
<div class="m2"><p>که بر یک جست چندینی بگردید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر صد قرن دیگر زود گردد</p></div>
<div class="m2"><p>چو از دودیست هم در دودگردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهان را گر فراز و گر فرو دست</p></div>
<div class="m2"><p>گل تیره‌ست یا دود کبودست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فلک گر دیر گر زودست گردان</p></div>
<div class="m2"><p>میان این گل ودودست گردان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدین پرقوتی که افلاک گردد</p></div>
<div class="m2"><p>کجا از بهر مشتی خاک گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنین جرمی عظیم القداری دوست</p></div>
<div class="m2"><p>نگردد از پی مشتی رگ و پوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنین دریا بما عاجز نگردد</p></div>
<div class="m2"><p>ز بهر شب نمی هرگز نگردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مگس پنداشت کان قصاب دمساز</p></div>
<div class="m2"><p>برای او در دکان کند باز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه می‌گویم عجب نیست از خدایی</p></div>
<div class="m2"><p>که بهر دانه راند آسیابی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فلک گردان ز بهر جان پاکست</p></div>
<div class="m2"><p>نه از بهر کفی آبست و خاکست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قدم در نه درین ره همچو مردان</p></div>
<div class="m2"><p>که خدمت کار تست این چرخ گردان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ولیکن روز کی چندی جهاندار</p></div>
<div class="m2"><p>درین حبس زمین کردت گرفتار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که تا چون بگذری زین حبس فانی</p></div>
<div class="m2"><p>تمامت قدر آن گلشن بدانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از آن کانی که جانها گوهر اوست</p></div>
<div class="m2"><p>فلک از دیر گه خاک دراوست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فلک در جنب آن کان اصل گردیست</p></div>
<div class="m2"><p>که آن کانرافلک چون لاژوردیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو در فهم گهر جان می‌کنی تو</p></div>
<div class="m2"><p>چگونه فهم آن کان می‌کنی تو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بسی کوکب که بر چرخ برین است</p></div>
<div class="m2"><p>صد و ده بار مهتر از زمینست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بباید سی هزاران سال از آغاز</p></div>
<div class="m2"><p>که تا هر یک بجای خود رسد باز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اگر سنگی بیندازی از افلاک</p></div>
<div class="m2"><p>بپانصد سال افتد بر سر خاک</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زمین در جنب این نه سقف مینا</p></div>
<div class="m2"><p>چو خشخاشی بود بر روی دریا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ببین تا توازین خشخاش چندی</p></div>
<div class="m2"><p>سزد گر بر بروت خود بخندی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو خشخاشی همی پوشی توازناز</p></div>
<div class="m2"><p>کجا یابی تو این خشخاش را باز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>توزین خشخاش کی آگاه کردی</p></div>
<div class="m2"><p>که سی سوراخ در خشخاش کردی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ازین نه چار طاق پر ستاره</p></div>
<div class="m2"><p>بتو نرسد مگر لختی نظاره</p></div></div>