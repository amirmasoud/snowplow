---
title: >-
    بخش ۲ - الحکایه و التمثیل
---
# بخش ۲ - الحکایه و التمثیل

<div class="b" id="bn1"><div class="m1"><p>یکی دیوانهٔ استاد در کوی</p></div>
<div class="m2"><p>جهانی خلق می‌رفتند هر سوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فغان برداشت این دیوانه ناگاه</p></div>
<div class="m2"><p>که از یک سوی باید رفت و یک راه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهر سویی چرا باید دویدن</p></div>
<div class="m2"><p>بصد سو هیچ جا نتوان رسیدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تویی با یک دل ای مسکین و صد یار</p></div>
<div class="m2"><p>بیک دل چون توانی کرد صد کار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو در یک دل بود صد گونه کارت</p></div>
<div class="m2"><p>تو صد دل باش اندر عشق یارت</p></div></div>