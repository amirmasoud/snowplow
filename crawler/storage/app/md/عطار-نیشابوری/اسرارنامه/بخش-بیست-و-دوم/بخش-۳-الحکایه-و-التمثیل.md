---
title: >-
    بخش ۳ - الحکایه و التمثیل
---
# بخش ۳ - الحکایه و التمثیل

<div class="b" id="bn1"><div class="m1"><p>شنودم من که فردوسی طوسی</p></div>
<div class="m2"><p>که کرد او درحکایت بی فسوسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بیست و پنج سال از نو ک خامه</p></div>
<div class="m2"><p>بسر می‌برد نقش شاهنامه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بآخر چون شد آن عمرش بآخر</p></div>
<div class="m2"><p>ابوالقاسم که بد شیخ اکابر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگرچه بود پیری پر نیاز او</p></div>
<div class="m2"><p>نکرد از راه دین بروی نماز او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنین گفت او که فردوسی بسی گفت</p></div>
<div class="m2"><p>همه در مدح گبری ناکسی گفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بمدح گبر کان عمری بسر برد</p></div>
<div class="m2"><p>چو وقت رفتن آمد بی خبر مرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرادر کار او برگ ریا نیست</p></div>
<div class="m2"><p>نمازم بر چنین شاعر روا نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو فردوسی مسکین را ببردند</p></div>
<div class="m2"><p>بزیر خاک تاریکش سپردند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در آن شب شیخ او را دید خواب</p></div>
<div class="m2"><p>که پیش شیخ آمد دیده پر آب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز مرد رنگ تاجی سبز بر سر</p></div>
<div class="m2"><p>لباسی سبزتر از سبزه در بر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بپیش شیخ بنشست و چنین گفت</p></div>
<div class="m2"><p>که ای جان تو با نور یقین جفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نکردی آن نماز از بی نیازی</p></div>
<div class="m2"><p>که می ننگ آمدت زین نانمازی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خدای تو جهانی پر فرشته</p></div>
<div class="m2"><p>همه از فیض روحانی سرشته</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فرستاد اینت لطف کار سازی</p></div>
<div class="m2"><p>که تا کردند بر خاکم نمازی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خطم دادند بر فردوس اعلی</p></div>
<div class="m2"><p>که فردوسی بفردوس است اولی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خطاب آمد که ای فردوسی پیر</p></div>
<div class="m2"><p>اگر راندت ز پیش آن طوسی پیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پذیرفتم منت تا خوش بخفتی</p></div>
<div class="m2"><p>بدان یک بیت توحیدم که گفتی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مشو نومید از فضل الهی</p></div>
<div class="m2"><p>مده بر فضل ما بخل گواهی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یقین می‌دان چوهستی مرداسرار</p></div>
<div class="m2"><p>که عاصی اندکست و فضل بسیار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر آمرزم بیک ره خلق را پاک</p></div>
<div class="m2"><p>نیامرزیده باشم جز کفی خاک</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خداوندا تو می‌دانی که عطار</p></div>
<div class="m2"><p>همه توحید تو گوید در اشعار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز نور تو شعاعی می‌نماید</p></div>
<div class="m2"><p>چو فردوسی فقاعی می‌گشاید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو فردوسی ببخشش رایگان تو</p></div>
<div class="m2"><p>بفضل خود بفردوسش رسان تو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بفردوسی که علیینش خوانند</p></div>
<div class="m2"><p>مقام صدق و قصر دینش خوانند</p></div></div>