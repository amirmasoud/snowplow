---
title: >-
    بخش ۴ - الحکایه و التمثیل
---
# بخش ۴ - الحکایه و التمثیل

<div class="b" id="bn1"><div class="m1"><p>شنیدم من که شبلی با گروهی</p></div>
<div class="m2"><p>همی شد در بیابان تا بکوهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بره در کاسه سر دید پر باد</p></div>
<div class="m2"><p>که از باد وزان می‌کرد فریاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرفت آن کاسه سرگشته گشته</p></div>
<div class="m2"><p>برو دید ای عجب خطی نبشته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که بنگر کین سر مردیست پر غم</p></div>
<div class="m2"><p>که او دنیا زیان کرد آخرت هم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو شبلی آن خط آشفته برخواند</p></div>
<div class="m2"><p>بزد یک نعره و آشفته درماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیاران گفت این سر در چنین راه</p></div>
<div class="m2"><p>سر مردیست از مردان درگاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که هر کو در نبازد هر دو عالم</p></div>
<div class="m2"><p>نگردد در حریم وصل محرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو هم گر هر دو عالم ترک گویی</p></div>
<div class="m2"><p>چنان کان مرد از مردان اویی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بپیمایی بسختی چند فرسنگ</p></div>
<div class="m2"><p>که تا یک جو زر آید بوک در چنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>براه حق چنین تا شب بخفتی</p></div>
<div class="m2"><p>براه راستی گامی نرفتی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو بی صد رنج یک جو زر نیابی</p></div>
<div class="m2"><p>سوی حق رنج نابرده شتابی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو می‌گیرد عسس روز سپیدت</p></div>
<div class="m2"><p>شب تاریک چون باشد امیدت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو می‌گویی که جز حق می‌نخواهم</p></div>
<div class="m2"><p>بهشت و حور الحق می نخواهم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو آبی گندهٔ در ژندهٔ تنگ</p></div>
<div class="m2"><p>نمی‌باید بهشتت ای همه ننگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز شیری زهره تو می‌شود آب</p></div>
<div class="m2"><p>در آن هیبت چگونه آوری تاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بیک دردی درآید عقل از پای</p></div>
<div class="m2"><p>چگونه ماند آنجا عقل بر جای</p></div></div>