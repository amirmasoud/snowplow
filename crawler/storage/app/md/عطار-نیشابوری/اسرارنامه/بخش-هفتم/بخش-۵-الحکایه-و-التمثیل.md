---
title: >-
    بخش ۵ - الحکایه و التمثیل
---
# بخش ۵ - الحکایه و التمثیل

<div class="b" id="bn1"><div class="m1"><p>یکی پشه شکایت کرد از باد</p></div>
<div class="m2"><p>بنزدیک سلیمان شد بفریاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که ناگه باد تندم در زمانی</p></div>
<div class="m2"><p>بیندازد جهانی تا جهانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بعدلت باز خر این نیم جان را</p></div>
<div class="m2"><p>وگرنه بر تو بفروشم جهان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سلیمان پشه را نزدیک بنشاند</p></div>
<div class="m2"><p>پس آنگه باد را نزدیک خود خواند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو آمد باد از دوری بتعجیل</p></div>
<div class="m2"><p>گریزان شد ازو پشه بصد میل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلیمان گفت نیست از باد بیداد</p></div>
<div class="m2"><p>ولیکن پشه می‌نتواند استاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو بادی می‌رسد او می‌گریزد</p></div>
<div class="m2"><p>چگونه پشه با صرصر ستیزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر امروز دادی نیم خرما</p></div>
<div class="m2"><p>برستی هم ز دوزخ هم ز گرما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وگر یکبار آوری شهادت</p></div>
<div class="m2"><p>حلالت شد بهشت با سعادت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وگر چیزی ورای این دو جویی</p></div>
<div class="m2"><p>شبت خوش باد بیهوده چو گویی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طلب مردود آمد راه مسدود</p></div>
<div class="m2"><p>چو مقصودی نمی‌بینم چه مقصود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وگر تو گرم رو مردی درین کار</p></div>
<div class="m2"><p>برو تا پینه بر کفشت زند یار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر صد قرن می‌گردی چو گویی</p></div>
<div class="m2"><p>نمی‌دانم که خواهی یافت بویی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بپنداری ببردی روزگارت</p></div>
<div class="m2"><p>تو این را کیستی با این چه کارت</p></div></div>