---
title: >-
    بخش ۶ - الحکایه و التمثیل
---
# بخش ۶ - الحکایه و التمثیل

<div class="b" id="bn1"><div class="m1"><p>چنین گفت آن جوامرد پگه خیز</p></div>
<div class="m2"><p>که پیش از صبح دم در طاعت آویز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر طاعت که فرمودند پای آر</p></div>
<div class="m2"><p>نماز چاشت آنگاهی بجای آر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو این کردی ز فرمان بیش کردی</p></div>
<div class="m2"><p>نکو کردی تو آن خویش کردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنون گر در رسد بازیت از راه</p></div>
<div class="m2"><p>نشیند بر سر دست تو ناگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو پایش گیر کاینجا جمله سودست</p></div>
<div class="m2"><p>وگرنه باز گیر تو که بودست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر آویزشی داری بمویی</p></div>
<div class="m2"><p>نیابی بوی او از هیچ سویی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگر پالوده گردی روزگاری</p></div>
<div class="m2"><p>که تا بویی بیابی از کناری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز تو تا هست مویی مانده بر جای</p></div>
<div class="m2"><p>بدان یک موی مانی بند بر پای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جنت را بر تن ارخشکست یک موی</p></div>
<div class="m2"><p>هنوزش نانمازی دان بصد روی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو مویی تا بکوهی در حسابست</p></div>
<div class="m2"><p>چه مویی و چه کوهی چون حجابست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو تا یک بارگی جان درنبازی</p></div>
<div class="m2"><p>جنب دانم ترا و نانمازی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مکاتب را اگر یک جو بماندست</p></div>
<div class="m2"><p>بدان جو جاودان در گو بماندست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تویی تو ترا نامحرم آمد</p></div>
<div class="m2"><p>تو بی تو شو که آدم آن دم آمد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر آیینه تو هم دم تست</p></div>
<div class="m2"><p>چو از دم تیره شد نامحرم تست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دو هم دم را که با هم شان حسابست</p></div>
<div class="m2"><p>اگر مویی میان باشد حجابست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو بنشیند بخلوت یار با یار</p></div>
<div class="m2"><p>نفس نامحرم افتد همچو اغیار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ندانی کرد هرگز خلوت آغاز</p></div>
<div class="m2"><p>مگر از هر چه داری خو کنی باز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نه زان شیر مردان سر راه</p></div>
<div class="m2"><p>که گردد جان تو زین راز آگاه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>علی الجمله یقین بشناس مطلق</p></div>
<div class="m2"><p>که ازحق نیست برخوردار جز حق</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بگو تا در خور حق یار که بود</p></div>
<div class="m2"><p>چو جز حق نیست برخوردار که بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو در دریای قدرت قطرهٔ تو</p></div>
<div class="m2"><p>چو با خورشید حضرت ذرهٔ تو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چگونه وصل او داری تو امید</p></div>
<div class="m2"><p>چگونه بر توانی شد بخورشید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو می‌خواهی بزاری و بزوری</p></div>
<div class="m2"><p>که آید پیل در سوراخ موری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>برو بنشین که جان از دست برخاست</p></div>
<div class="m2"><p>درآمد هوشیار و مست برخاست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اگر جانست دایم غرقه اوست</p></div>
<div class="m2"><p>وگر عقل او برون از حلقه اوست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هزاران ذره سرگردان بماندست</p></div>
<div class="m2"><p>ولی خورشید در ایوان بماندست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>درین دریا هزاران قطره پنهانست</p></div>
<div class="m2"><p>ولی گوهر درون قعر پنهانست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بسی در وصف او تصنیف کردند</p></div>
<div class="m2"><p>بسی با یک دگر تعریف کردند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هزاران قرن می‌کردند فکرت</p></div>
<div class="m2"><p>بآخر با سرآمد عجز و حیرت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زهی دریای پر در الهی</p></div>
<div class="m2"><p>که ننشیند برو گرد تباهی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سخنها می‌رود چون آب زر پاک</p></div>
<div class="m2"><p>ولیکن دیده داری تو پر خاک</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دلت با نفس شهوت خوی کرده</p></div>
<div class="m2"><p>کجا بیند معانی زیر پرده</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو تو عالم ندانی جز خیالی</p></div>
<div class="m2"><p>کجا یابی ازین معنی کمالی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ترا با این چه کار ای خفته باری</p></div>
<div class="m2"><p>ندارد مشک با کناس کاری</p></div></div>