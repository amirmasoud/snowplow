---
title: >-
    بخش ۲ - الحکایه و التمثیل
---
# بخش ۲ - الحکایه و التمثیل

<div class="b" id="bn1"><div class="m1"><p>سرای خود بغارت داد شاهی</p></div>
<div class="m2"><p>در افتادند غارت را سپاهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غلامی پیش شاه ایستادبر پای</p></div>
<div class="m2"><p>دران غارت نمی‌جنبید از جای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی گفتش که غارت کن زمانی</p></div>
<div class="m2"><p>که گر سودی بود نبود زیانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخندید او که این بر من حرامست</p></div>
<div class="m2"><p>که روی شاه سود من تمامست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا در روی شه کردن نگاهی</p></div>
<div class="m2"><p>بسی خوشتر که از مه تا بماهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل شه گشت خرم زان یگانه</p></div>
<div class="m2"><p>جواهر خواست خالی از خزانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسی جوهر باعزاز و نکو داشت</p></div>
<div class="m2"><p>بدست خویشتن در پیش او داشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که برگیر آنچ می‌خواهی ترا باد</p></div>
<div class="m2"><p>که کردی ای گرامی جان من شاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غلامش دست خود بگشاد از هم</p></div>
<div class="m2"><p>سرانگشت شه بگرفت محکم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که ما را کار با این اوفتادست</p></div>
<div class="m2"><p>چه جوهر چه خزانه جمله یادست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو تو هستی مرا دیگر همه هست</p></div>
<div class="m2"><p>همه دستم دهد چون تو دهی دست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همی هرگز مباد آن روز را نور</p></div>
<div class="m2"><p>که من از تو بدون تو شوم دور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو جانان آمد از جان کم نیاید</p></div>
<div class="m2"><p>همه این جوی تو کان کم نیاید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دو گیتی را نجوید هر که مردست</p></div>
<div class="m2"><p>یکی را جوید او کین هر دو گردست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو هر لذت که در هر دوجهان هست</p></div>
<div class="m2"><p>ترا در حضرت او بیش از آن هست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چرا پس ترک دو جهان می‌نگیری</p></div>
<div class="m2"><p>چو مشتاقان پی آن می‌نگیری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یکی را خواه تا در ره نمانی</p></div>
<div class="m2"><p>فلک رو باش تا در چه نمانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شواغل دور کن مشغول او شو</p></div>
<div class="m2"><p>چو خود را گم کنی در حق فروشو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر از دیدهٔ خود دور افتی</p></div>
<div class="m2"><p>همی در عالم پر نور افتی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بهشت آدم بدو گندم بدادست</p></div>
<div class="m2"><p>تو هم بفروش اگر کارت فتادست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نه سید گفت بعضی را بتدبیر</p></div>
<div class="m2"><p>سوی جنت کشند آنگه بزنجیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اگر جان را بخواهد بود دیدار</p></div>
<div class="m2"><p>چه باشی هشت جنت را خریدار</p></div></div>