---
title: >-
    بخش ۱ - المقاله التاسعه
---
# بخش ۱ - المقاله التاسعه

<div class="b" id="bn1"><div class="m1"><p>بدان ای پاک دین گر پاک آبی</p></div>
<div class="m2"><p>که آن ساعت که زیر خاک آیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قدم بیرون نهی از کوی دنیا</p></div>
<div class="m2"><p>نبینی نیز هرگز روی دنیا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو رفتی رفتی از دنیا و رفتی</p></div>
<div class="m2"><p>دگر هرگز بدنیا در نیفتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بعقبی بارگاهی یابی از نور</p></div>
<div class="m2"><p>بپوشی حله و در بر کشی حور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وگر آلایشی داری ز کاری</p></div>
<div class="m2"><p>در آلایش بمانی روزگاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه شرکت حواس تست در راه</p></div>
<div class="m2"><p>همه ابلیس و همت دیو بدخواه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه مرگ تو خوی ناخوش تست</p></div>
<div class="m2"><p>همه خشمت بدوزخ آتش تست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر آنگه کز جهان رفتی تو بیرون</p></div>
<div class="m2"><p>نخواهد بود حالت از دو بیرون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر آلودهٔ پالوده گردی</p></div>
<div class="m2"><p>وگر پالوده آسوده گردی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو تو آلوده باشی و گنه کار</p></div>
<div class="m2"><p>کنندت در نهاد خود گرفتار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وگر پالوده دل باشی تو در راه</p></div>
<div class="m2"><p>فشانان دست بخرامی بدرگاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فراز عیش و شیب وجاه باتست</p></div>
<div class="m2"><p>بهشت و دوزخت هم راه با تست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همی تا تو چگونه رفت خواهی</p></div>
<div class="m2"><p>درین ره بر چه پهلو خفت خواهی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر در پردهٔ در پرده باشی</p></div>
<div class="m2"><p>در آن چیزی که در وی مرده باشی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نمیرد هیچ بینا دل سفیهی</p></div>
<div class="m2"><p>نخیزد هیچ کناسی فقیهی </p></div></div>