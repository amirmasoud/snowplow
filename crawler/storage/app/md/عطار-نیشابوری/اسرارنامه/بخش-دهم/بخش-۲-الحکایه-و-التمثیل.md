---
title: >-
    بخش ۲ - الحکایه و التمثیل
---
# بخش ۲ - الحکایه و التمثیل

<div class="b" id="bn1"><div class="m1"><p>سبویی می‌ستد رندی زخمار</p></div>
<div class="m2"><p>که این ساعت گرو بستان و بردار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو خورد آن باده گفتندش گرو کو</p></div>
<div class="m2"><p>گرو گفتا منم گفتند نیکو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زهی نیکو گرو برخیز و رو تو</p></div>
<div class="m2"><p>نیرزی نیم جو وقت گرو تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر ارزندهٔ داری تو با خویش</p></div>
<div class="m2"><p>نیرزی تو بنزد کس از آن بیش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترا قیمت بعلمست و بکردار</p></div>
<div class="m2"><p>تو همچون من در افزودی بگفتار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بقدر آن که علم و کار داری</p></div>
<div class="m2"><p>بدان ارزی بدان مقدار داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فشاندم در معنی بر تو بسیار</p></div>
<div class="m2"><p>ولی کی کور بیند در شهوار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو چون نرگس همه چشمی نه بینا</p></div>
<div class="m2"><p>چو سیسنبر همه گوشی نه شنوا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو این ساعت که عقل و هوش داری</p></div>
<div class="m2"><p>نه بنیوشی سخن نه گوش داری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در آن ساعت که عقل و هوش شد پاک</p></div>
<div class="m2"><p>مگر خواهی شنودن مرده در خاک</p></div></div>