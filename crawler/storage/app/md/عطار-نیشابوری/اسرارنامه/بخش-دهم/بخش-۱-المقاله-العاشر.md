---
title: >-
    بخش ۱ - المقاله العاشر
---
# بخش ۱ - المقاله العاشر

<div class="b" id="bn1"><div class="m1"><p>یکی دریای بی پایان نهادند</p></div>
<div class="m2"><p>وزان دریا رهی با جان گشادند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی بر روی آن دریا برون شد</p></div>
<div class="m2"><p>گهی مؤمن گهی ترسا برون شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درین دریا که بی قعر و کنارست</p></div>
<div class="m2"><p>عجایب در عجایب بی‌شمارست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهی دریای بی‌پایان اسرار</p></div>
<div class="m2"><p>که نه سر دارد و نه بن پدیدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر آن دریا نه زیر پرده بودی</p></div>
<div class="m2"><p>بکلی کردها ناکرده بودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهانی کرده چون پر شد بدان نور</p></div>
<div class="m2"><p>نماند هست تا نبود از آن دور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر گویی چرا ماندست پرده</p></div>
<div class="m2"><p>چو آنجا می‌نماید هیچ کرده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سخن اینجا زبان را می‌نشاید</p></div>
<div class="m2"><p>که این جز عقل و جان را می‌نشاید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سخن را در پس سرپوش میدار</p></div>
<div class="m2"><p>زبان را از سخن چین گوش می‌دار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کسی را نیست فهم این سخنها</p></div>
<div class="m2"><p>تو با خود روی در روی آر تنها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مشو رنجه ز گفت هر زبانی</p></div>
<div class="m2"><p>یقین داری مرنج از هر گمانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو دریا در تغیر باش دایم</p></div>
<div class="m2"><p>چو مردان در تفکر باش دایم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کمال خود بدان کز بس تعظم</p></div>
<div class="m2"><p>غلامان تواند افلاک و انجم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر آن چیزی که دی اندر ازل رفت</p></div>
<div class="m2"><p>فلک امروزانرا در عمل رفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هزاران دور می‌بایست در کار</p></div>
<div class="m2"><p>که تا هم چون تویی آید پدیدار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بهر دم کز تو برمی‌آید ای دوست</p></div>
<div class="m2"><p>چنان باید که پنداری یکی توست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همه عمرت اگر بیش است اگر کم</p></div>
<div class="m2"><p>کمال جانت را شرطست دم دم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همی هر لحظه جان معنی اندیش</p></div>
<div class="m2"><p>تواند کرد خود را رونقی بیش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو اینجا لذتی فانی براندی</p></div>
<div class="m2"><p>ز صد لذات باقی باز ماندی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دمی کاینجا خوش آمد خورد و خفتت</p></div>
<div class="m2"><p>دو صد چندان خوشی از دست رفتت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو دنیا کشت زار آن جهانست</p></div>
<div class="m2"><p>بکاراین تخم کاکنون وقت آنست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زمین و آب داری دانه در پاش</p></div>
<div class="m2"><p>بکن دهقانی و این کار را باش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نکو کن کشت خویش از وعده من</p></div>
<div class="m2"><p>اگر بد افتدت در عهده من</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگر این کشت و زری را نورزی</p></div>
<div class="m2"><p>در آن خرمن بنیم ارزن نیرزی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>برو گر روز بازاری نداری</p></div>
<div class="m2"><p>بکار این دانه چون کاری نداری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>برای آن فرستادند اینجات</p></div>
<div class="m2"><p>که تا امروز سازی برگ فدات</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اگر بیرون شوی ناکشته دانه</p></div>
<div class="m2"><p>تو خواهی بود رسوای زمانه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دو کس را در ره دین تخم دادند</p></div>
<div class="m2"><p>ره دنیا بهر کس برگشادند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یکی ضایع گذاشت آن تخم در راه</p></div>
<div class="m2"><p>یکی می‌پروریدش گاه و بیگاه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همی چون وقت برخوردن درآمد</p></div>
<div class="m2"><p>یکی بر سر دگر یک در سرآمد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بکاری بر درو کاید پدیدت</p></div>
<div class="m2"><p>درو وقت گرو اید پدیدت</p></div></div>