---
title: >-
    بخش ۲ - فی فضیلت امیرالمؤمنین عمر رضی الله عنه
---
# بخش ۲ - فی فضیلت امیرالمؤمنین عمر رضی الله عنه

<div class="b" id="bn1"><div class="m1"><p>سپهر دین عمر خورشید خطاب</p></div>
<div class="m2"><p>چراغ هشت جنت شمع اصحاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه شمعی کافتاب نامبردار</p></div>
<div class="m2"><p>طواف او کند پروانه کردار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازین پرتو که بود آن شمع دین را</p></div>
<div class="m2"><p>نمی‌شایست جز خلد برین را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر او قطب دین حق نبودی</p></div>
<div class="m2"><p>کمال شرع را رونق نبودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بهر سر بریدن سر بداد او</p></div>
<div class="m2"><p>بدان شد تا سرآرد سر نهاد او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو آهنگ سر شمع هدی کرد</p></div>
<div class="m2"><p>به پیش طای طاها سر فدا کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو چشم جان او اسرار بین شد</p></div>
<div class="m2"><p>شکش برخاست مشکلها یقین شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شریعت را کمال افزود اول</p></div>
<div class="m2"><p>ز چل مردان یکی او بود اول</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رسولش گفت گر بودی دگر کس</p></div>
<div class="m2"><p>نبی جز من نبودی جز عمر کس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خداوند جهان از نور جانش</p></div>
<div class="m2"><p>سخنها گفته بی او بر زفانش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو حق را حلقهٔ در گوش کرد او</p></div>
<div class="m2"><p>بنامش زهر قاتل نوش کرد او</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از آن برخویشتن زهر آزمودی</p></div>
<div class="m2"><p>که صد تریاق فاروقیش بودی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چنان شد ظلم در ایام او گم</p></div>
<div class="m2"><p>که اشکی در میان بحر قلزم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جهان از عدل او آسوده گشته</p></div>
<div class="m2"><p>ستم از بیم او نابوده گشته</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عجم را تا قیامت درگشاده</p></div>
<div class="m2"><p>هزار و شصت وشش منبر نهاده</p></div></div>