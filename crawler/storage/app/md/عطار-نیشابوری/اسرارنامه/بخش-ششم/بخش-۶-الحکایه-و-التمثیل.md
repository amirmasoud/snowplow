---
title: >-
    بخش ۶ - الحکایه و التمثیل
---
# بخش ۶ - الحکایه و التمثیل

<div class="b" id="bn1"><div class="m1"><p>چنین گفت آن بزرگ برگزیده</p></div>
<div class="m2"><p>که جنت این زمان هست آفریده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ولی آنگه شود جنت تمامت</p></div>
<div class="m2"><p>که در جنت شوند اهل قیامت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر پیدا شود حوری بدنیا</p></div>
<div class="m2"><p>شوند این خلق بیهش تا بعقبی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نداری تاب آن امروز اینجا</p></div>
<div class="m2"><p>که بینی حور روح افروز اینجا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زهی قوت که اندر جانت باشد</p></div>
<div class="m2"><p>که فرداتاب صد چندانت باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تویی آن نقطهٔ افتادهٔ فارغ</p></div>
<div class="m2"><p>که اندر خلد خواهی گشت بالغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلوغ اینجاست در عقبی طهورش</p></div>
<div class="m2"><p>دلت اینجاست در فردوس نورش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در و دیوار جنت از حیاتست</p></div>
<div class="m2"><p>زمین و آسمان او نجاتست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درختش صدق و اخلاص است و تقوی</p></div>
<div class="m2"><p>همه بار درخت اسرار معنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درخت طیبه آنجا بروید</p></div>
<div class="m2"><p>که دست و پا سخن آنجا بگوید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه سید گفت کاینجانیک بختی</p></div>
<div class="m2"><p>بیک نیکی نشاند آنجا درختی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه آنجا اقربا ماند نه اسباب</p></div>
<div class="m2"><p>که فرزند عمل باشند انساب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بسا مردا که او اب الصلاتست</p></div>
<div class="m2"><p>بسا زن کان زمان اخت الزکاتست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نه در دل بگذرد کان خود چه سانست</p></div>
<div class="m2"><p>نه درجان آیدت کین از جهانست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همه عالم ز حوران می‌زند جوش</p></div>
<div class="m2"><p>چو ناخن زنده‌اند ایشان و خاموش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در و دیوار ایشانند جمله</p></div>
<div class="m2"><p>ولی در پرده پنهانند جمله</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زمینها و آسمانها پر فرشته‌ست</p></div>
<div class="m2"><p>تو کی بینی که چشم تو سرشته‌ست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر آنگه کز سرشت آیی برون تو</p></div>
<div class="m2"><p>ببینی هر دو عالم را کنون تو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شود معنی هر چیزی ترا فاش</p></div>
<div class="m2"><p>چه می‌گویم یکی می‌دانیی کاش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>حیات لعب و لهوست اینچ دیدی</p></div>
<div class="m2"><p>حیوه طیبه نامی شنیدی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حیات ای دوست تو بر تو فتادست</p></div>
<div class="m2"><p>بهر تویی درون نوعی نهادست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>الست آنگه که بشنودی که بودی</p></div>
<div class="m2"><p>نبودی بود بودن کان شنودی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>حیاتی داشتی آنگه کنون هم</p></div>
<div class="m2"><p>ببین کین دو حیاتت هست چون هم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ترا چون از یکی گفتن خبر نیست</p></div>
<div class="m2"><p>وزان نوع حیاتت هیچ اثر نیست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو از نطق و حیاتت بی نشانی</p></div>
<div class="m2"><p>حیوه و نطق ذره چون بدانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>میامرزاد یزدانش بعقبی</p></div>
<div class="m2"><p>که گوید فلسفه‌ست این گونه معنی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز جامی دیگرست این گونه اسرار</p></div>
<div class="m2"><p>ندارد فلسفی با این سخن کار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>محقق این بچشم تیز بیند</p></div>
<div class="m2"><p>دو عالم را بکل یک چیز بیند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همه عالم ببیند بند بوده</p></div>
<div class="m2"><p>کند آن بند بوده جمله سوده</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دهد بر باد تا پیچش نماند</p></div>
<div class="m2"><p>چو هیچی باشد او هیچش نماند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کسی کین دید و چشمش این صفا یافت</p></div>
<div class="m2"><p>بنور صدر عالم مصطفی یافت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز کونین ارشوی پاک و مجرد</p></div>
<div class="m2"><p>نیاید راست بی نور محمد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اگر راه محمد را چو خاکی</p></div>
<div class="m2"><p>دو عالم خاک تو گردد ز پاکی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز قول فلسفی گو دور می‌باش</p></div>
<div class="m2"><p>ز عقل و زیرکی مهجور می‌باش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بعقل ار نقش این اسرار بندی</p></div>
<div class="m2"><p>میان گبر کان زنار بندی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ورای عقل چندان طول بیش است</p></div>
<div class="m2"><p>که بعد و هم را در غور بیش است</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو جز در زیرکی نبود ترا دست</p></div>
<div class="m2"><p>ز کوزه آن تراود کاندرو هست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بگویم اعتقاد خویش با تو</p></div>
<div class="m2"><p>اگرچه کی شود این بیش باتو</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همان مذهب که مشتی پیرزن داشت</p></div>
<div class="m2"><p>مرا آن مذهبست اینک سخن راست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بسی بشناس و چون من کرد عاجز</p></div>
<div class="m2"><p>علی الحق این بود دین عجایز</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بکل آن پیرزن دادست اقرار</p></div>
<div class="m2"><p>ترا در ره بهر جزویست انکار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>جو تو بی علت چون و چرایی</p></div>
<div class="m2"><p>اگر آیی تو بی علت نیایی</p></div></div>