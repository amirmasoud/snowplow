---
title: >-
    بخش ۷ - الحکایه و التمثیل
---
# بخش ۷ - الحکایه و التمثیل

<div class="b" id="bn1"><div class="m1"><p>سئوالی کرد زین شیوه یکی خام</p></div>
<div class="m2"><p>از آن سلطان بر حق پیر بسطام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که از بهر چرا عالم چنین است</p></div>
<div class="m2"><p>که آن یک آسمان این یک زمین است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو آن پیوسته در جنبش فتادست</p></div>
<div class="m2"><p>چرا این ساکن اینجا ایستادست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چرا این هفت گردد بر هم اینجا</p></div>
<div class="m2"><p>چرا جاییست خاص این عالم اینجا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جوابش داد آن سلطان مطلق</p></div>
<div class="m2"><p>که بشنو این جواب از ما علی الحق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخن بشنو نه دل تاب و نه سر پیچ</p></div>
<div class="m2"><p>برای این که می‌بینی دگر هیچ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو ما در اصل کل علت نگوییم</p></div>
<div class="m2"><p>بلی در فرع هم علت نجوییم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو عقل فلسفی در علت افتاد</p></div>
<div class="m2"><p>ز دین مصطفا بی دولت افتاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه اشکالست در دین و نه علت</p></div>
<div class="m2"><p>بجز تسلیم نیست این دین و ملت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ورای عقل ما را بارگاهیست</p></div>
<div class="m2"><p>ولیکن فلسفی یک چشم راهیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همی هر کو چرا گفت او خطا گفت</p></div>
<div class="m2"><p>بگو تا خود چرا باید چرا گفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چرا و چون نبات و خاک و همست</p></div>
<div class="m2"><p>کسی دریابد این کو پاک فهمست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عزیزا سر جان و تن شنیدی</p></div>
<div class="m2"><p>ز مغز هر سخن روغن کشیدی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تن و جان را منور کن باسرار</p></div>
<div class="m2"><p>وگرنه جان و تن گردد گرفتار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو می‌بینی بهم یاری هر دو</p></div>
<div class="m2"><p>بهم باشد گرفتاری هر دو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مثال جان و تن خواهی ز من خواه</p></div>
<div class="m2"><p>مثال کور و مفلوج است در راه</p></div></div>