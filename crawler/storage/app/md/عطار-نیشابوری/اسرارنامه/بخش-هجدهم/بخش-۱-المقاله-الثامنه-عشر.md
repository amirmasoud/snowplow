---
title: >-
    بخش ۱ - المقاله الثامنه عشر
---
# بخش ۱ - المقاله الثامنه عشر

<div class="b" id="bn1"><div class="m1"><p>دریغا دیدهٔ ره بین نداری</p></div>
<div class="m2"><p>بغفلت عمر شیرین می‌گذاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسر بردی بغفلت روزگاری</p></div>
<div class="m2"><p>مگر در گور خواهی کرد کاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>الا ای حرص در کارت کشیده</p></div>
<div class="m2"><p>چو شد قد الف وارت خمیده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر طاعت کنی اکنون نه زانست</p></div>
<div class="m2"><p>که می‌ترسی که مرگت ناگهانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسی شادی بکردی کام راندی</p></div>
<div class="m2"><p>کنون چون پیر گشتی بازماندی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز دارو کردنت ای پیر تا کی</p></div>
<div class="m2"><p>بمی باید شدن تدبیر تاکی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشد یک ذره کم ای پیر آزت</p></div>
<div class="m2"><p>نکردستند گوی از شیر بازت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کنون زشتست حرص از مردم پیر</p></div>
<div class="m2"><p>گنه خود چون بود با موی چوی شیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو مویت شیر شد ای پیرخیره</p></div>
<div class="m2"><p>مکن آلوده شیرت را بشیره</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بکف در آتشین داری نواله</p></div>
<div class="m2"><p>که در پیری بکف داری پیاله</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو می‌شویی بآب تلخ تن را</p></div>
<div class="m2"><p>بشوی از اشگ شور خود کفن را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مکن روباه بازی و بیارام</p></div>
<div class="m2"><p>که پیه گرگ در مالیدت ایام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نمی‌ترسی که از کوی جهانت</p></div>
<div class="m2"><p>تو غافل در ربایند از میانت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو خوش بنشسته و گردون دونده</p></div>
<div class="m2"><p>تو مرغ دانه کش عمرت پرنده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو خفته عمر بر پنجاه آمد</p></div>
<div class="m2"><p>کنون بیدار شو که گاه آمد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو گر عمری بدنیا خون گرستی</p></div>
<div class="m2"><p>نه بس کاریست این کاکنون کرستی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چه کارست این که در دنیاء فانیست</p></div>
<div class="m2"><p>جهانی کار کار آن جهانیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>غم خود خور که کس را از تو غم نیست</p></div>
<div class="m2"><p>چه می‌گویم ترا حقا که هم نیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ترا افتاد اگر افتاد کاری</p></div>
<div class="m2"><p>که کس را نیست بر دل از تو باری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز مرگت گر کسی دل ریش دارد</p></div>
<div class="m2"><p>ز خود ترسد که آن در پیش دارد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کسی کز مرگ تو بسیار گرید</p></div>
<div class="m2"><p>ز مرگ خود بترسد زار گرید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زمانی لب زخندیدن بندد</p></div>
<div class="m2"><p>بصد لب یک زمان دیگر بخندد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ترا افتاد کار ای پیرخون خور</p></div>
<div class="m2"><p>بایمان گر توانی جان برون بر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نخواهی بود با کس در میانه</p></div>
<div class="m2"><p>تو خواهی بود با تو جاودانه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نترسی زانک فرداهم درین سوز</p></div>
<div class="m2"><p>همه با خود گذارندت چو امروز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کنون من گفتم و رفتم بزودی</p></div>
<div class="m2"><p>بکشتم می ندانم تا درودی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کنون با گفت افتادست کارم</p></div>
<div class="m2"><p>که گر طاعت کنم طاقت ندارم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کنون آن بادها از سر برون شد</p></div>
<div class="m2"><p>که زیر خاک می‌باید درون شد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کنون چون زندگانی رخت دربست</p></div>
<div class="m2"><p>بسوی خاک رفتم باد در دست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کنون گر شاد و گر غمناک رفتم</p></div>
<div class="m2"><p>دلی پر آرزو با خاک رفتم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جهان پر غمم بسیار دم داد</p></div>
<div class="m2"><p>سپهر گوژ پشتم پشت خم داد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>غم من چند خواهد کرد بردار</p></div>
<div class="m2"><p>ندارم جز ز فانی هیچ بر کار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بسی در دین و دنیا راز راندم</p></div>
<div class="m2"><p>بدین نرسیدم و زان باز ماندم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دمم شد سرد و دل برخاست از دست</p></div>
<div class="m2"><p>که بر فرقم زپیری برف بنشست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو شد کافور موی مشگ بارم</p></div>
<div class="m2"><p>کفن باید که من کافور دارم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همه مویم تا سپیدی جایگه کرد</p></div>
<div class="m2"><p>جهان بر من سر پستان سیه کرد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چنان افتاده‌ام از پای پیری</p></div>
<div class="m2"><p>که از کس می‌نیابم دست گیری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>جوانان طعنه خوش می‌زنندم</p></div>
<div class="m2"><p>بطعنه در دل آتش می‌زنندم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ولیکم هست صبپر آنک ایشان</p></div>
<div class="m2"><p>چو من بیچاره گردند و پریشان</p></div></div>