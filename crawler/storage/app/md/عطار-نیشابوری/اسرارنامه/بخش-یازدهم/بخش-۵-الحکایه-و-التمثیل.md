---
title: >-
    بخش ۵ - الحکایه و التمثیل
---
# بخش ۵ - الحکایه و التمثیل

<div class="b" id="bn1"><div class="m1"><p>چنین گفت آن بزرگ کار دیده</p></div>
<div class="m2"><p>که بود او نیک و بد بسیار دیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که خالق هرچ را دادست هستی</p></div>
<div class="m2"><p>چه پیش و پس چه بالا و چه پستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه انجم چه فلک چه مهر و چه ماه</p></div>
<div class="m2"><p>چه دریا چه زمین چه کوه و چه کاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه لوح و چه قلم چه عرش و کرسی</p></div>
<div class="m2"><p>چه روحانی چه کروبی چه انسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه می چه انگبین چه خلد و چه حور</p></div>
<div class="m2"><p>چه ماهی و چه مه چه نار و چه نور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه شرق و چه غرب چه از قاف تا قاف</p></div>
<div class="m2"><p>چه هرچ آمد برون از نون و زکاف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه اسراری که در هر دو جهان هست</p></div>
<div class="m2"><p>چه لذاتی که پیدا و نهان هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه اندر هر دو عالم ذره ذره</p></div>
<div class="m2"><p>چه اندر هفت دریا قطره قطره</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه بنمایدت روشن چو خورشید</p></div>
<div class="m2"><p>حنانک آن جمله می‌بینی تو جاوید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ولی مویی بتو ننماید از تو</p></div>
<div class="m2"><p>تویی تو نهان می‌باید از تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر چشم تو بر روی تو افتاد</p></div>
<div class="m2"><p>ز عشق تو براید از تو فریاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر می‌بایدت بویی هم از تو</p></div>
<div class="m2"><p>ریاضت کن که پر شد عالم از تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چرا اندر غلط افتادی آخر</p></div>
<div class="m2"><p>چرا از بندگی آزادی آخر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عدم دیدی نظر بگماشتی تو</p></div>
<div class="m2"><p>وجود خود عدم پنداشتی تو</p></div></div>