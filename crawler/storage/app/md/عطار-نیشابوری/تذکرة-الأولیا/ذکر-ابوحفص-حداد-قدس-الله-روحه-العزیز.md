---
title: >-
    ذکر ابوحفص حداد قدس الله روحه العزیز
---
# ذکر ابوحفص حداد قدس الله روحه العزیز

<div class="n" id="bn1"><p>آن قدوه رجال، آن نقطه کمال، آن عابد صادق، آن زاهد عاشق، آن سلطان اوتاد، قطب عالم: ابوحفص حداد، رحمةالله علیه، پادشاه مشایخ بود علی الاطلاق، خلیفه حق بود به استحقاق، و او از محتشمان این طایفه بود، و کسی به بزرگی او نبود در وقت وی، ور در ریاضت و کرامت و مروت و فتوت بی نظیر بود و در کشف و بیان یگانه و معلم و ملقن او بی واسطه خدای بود، عزوجل. و پیر بوعثمان حیری بود و شاه شجاع از کرمان به زیارت او آمدو در صحبت او به بغداد به زیارت مشایخ، و ابتدای او آن بود که بر کنیزکی عاشق بود، چنانکه قرار نداشت، او را گفتند: در شارستان نشابور جهودی جادوگر است، تدبیر کار تو او کند. </p></div>
<div class="n" id="bn2"><p>ابوحفص پیش او رفت و حال بگفت. او گفت: تو را چهل روز نماز نباید کرد و هیچ طاعت و عمل نیکو نباید کرد و نام خدای بر زبان نشاید راند و نیت نیکو نباید کرد، تا من حیلت کنم و تو را به سحر به مقصود رسانم. </p></div>
<div class="n" id="bn3"><p>بوحفص چهل روز چنان کرد. بعد از آن جهود آن طلسم بکرد و مراد حاصل نشد. جهود گفت: بی شک از تو خیری در وجود آمده است و اگر نه مرا یقین است که این مقصود حاصل شدی. </p></div>
<div class="n" id="bn4"><p>بوحفص گفت: من هیچ چیزی نکردم الا در راه که می‌آمدم سنگی از راه به پای باز کناره افگندم تا کسی بر او نیفتد. </p></div>
<div class="n" id="bn5"><p>جهود گفت: میازار خداوندی را که تو چهل روز فرمان او ضایع کنی و او از کرم این مقدار رنج تو ضایع نکرد. </p></div>
<div class="n" id="bn6"><p>آتشی از این سخن در دل ابوحفص پدید آمد و چندان قوت کرد که بو حفص به دست جهود توبه کرد و همان آهنگری می‌کرد و واقعه خود نهان می‌داشت و هر روز یک دینار کسب می‌کرد و شب به درویشان دادی و در کلیددان بیوه زنان انداختی - چنانکه ندانستندی - و نماز خفتن دریوزه کردی و روزه بدان گشادی. وقت بودی که در حوضی که تره شستندی بقایای آن برچیدی و نان خورش ساختی مدتی بدین روزگار گذاشتی یک روز نابینائی در بازا ر می‌گذشت. این آیت می‌خواند: اعوذ بالله من الشیطان الرجیم * بسم الله الرحمن الرحیم * و بدالهم من الله ما لم یکونوا یحتسبون* دلش بدین آیت مشغول شد و چیزی بر وی در آمد و بیخود گذشت. به جای انبر، دست در کوره کرد و آهن تفسیده بیرون کرد و بر سندان نهاد. شاگردان پتک بزدند، نگاه کردند، آهن در دست او دیدند - که می‌گردانید. گفتند: ای استاد! این چه حال است؟ </p></div>
<div class="n" id="bn7"><p>او بانگ بر شاگردان زد که بزنید! </p></div>
<div class="n" id="bn8"><p>گفتند: ای استاد! برکجا بزنیم؟ چون آهن پاک شد؟ </p></div>
<div class="n" id="bn9"><p>پس بوحفص به خود بازآمد. آهن تافته در دست خود دید و این سخن بشنید که: چون پاک شد برکجا زنیم؟</p></div>
<div class="n" id="bn10"><p>نعره بزد و آهن بیفگند و دکان را به غارت داد و گفت: ما چندین گاه خواستیم به تکلف که این کار رها کنیم و نکردیم تا آنگاه که این حدیث حمله آورد و ما را از ما بستد و اگر چه من دست از کار می‌داشتم تا کار دست از من نداشت فایده نبود. </p></div>
<div class="n" id="bn11"><p>پس روی به ریاضت سخت نهاد و عزلت و مراقبت پیش گرفت. </p></div>
<div class="n" id="bn12"><p>چنانکه نقل است که در همسایگی او احادیث استماع می‌کردند. گفتند: آخر چرا نیایی تاا ستماع احادیث کنی؟ </p></div>
<div class="n" id="bn13"><p>گفت: من سی سال است تا می‌خواهم داد یک حدیث بدهم، نمی‌توانم داد. سماع دیگر حدیث چون کنم؟ </p></div>
<div class="n" id="bn14"><p>گفتند: آن حدیث کدام است؟ </p></div>
<div class="n" id="bn15"><p>گفت: آنکه می‌فرماید: رسول صلی الله علیه و آله وسلم من حسن اسلام المرء ترکه ما لا یعنیه. از نیکویی اسلام مرد آن است که ترک کند چیزی که به کارش نیاید. </p></div>
<div class="n" id="bn16"><p>نقل است که با یاران به صحرا رفته بود و سخن گفت. وقت ایشان خوش گشت. آهویی از کوه بیامد و سر برکنار نهاد. ابوحفص تپانچه بر روی خود می‌زد و فریاد می‌کرد. آهو برفت. شیخ به حال خود بازآمد. اصحاب پرسیدندکه: این چه بود؟ </p></div>
<div class="n" id="bn17"><p>گفت: چون وقت ما خوش شد در خاطرم آمد که کاشکی گوسفندی بودی تا بریان کردمانی و یاران امشب پراکنده نشدندی. چون در خاطرم بگذشت آهویی بیامد. </p></div>
<div class="n" id="bn18"><p>مریدان گفتند: یا شیخ! کسی را با حق چنین حالی بود فریاد کردن و تپانچه زدن چه معنی دارد؟ </p></div>
<div class="n" id="bn19"><p>شیخ گفت: نمی‌دانید که مراد در کنار نهادن از در بیرون کردن است. اگر خدای تعالی به فرعون نیکی خواستی بر مراد او نیل را روان نکردی. </p></div>
<div class="n" id="bn20"><p>نقل است که هر وقت در خشم شدی سخن در خلق نیکو گفتی تا خشم او ساکن شدی، آنگه به سخن دیگر شدی. </p></div>
<div class="n" id="bn21"><p>نقل است که یک روز می‌گذشت. یکی را دید متحیر و گریان. گفت: تو را چه بوده است؟ </p></div>
<div class="n" id="bn22"><p>گفت: خری داشتم، گم شده است و جز آن هیچ نداشتم. </p></div>
<div class="n" id="bn23"><p>شیخ توقف کرد و گفت: به عزت تو که گام برندارم تا خر بدو باز نرسد. در حال خر پدید آمد. </p></div>
<div class="n" id="bn24"><p>ابوعثمان حیری گوید: روزی در پیش ابوحفص می‌رفتم. مویزی چند دیدم پیش او نهاده. یکی برداشتم و در دهان نهادم. حلق مرا بگرفت و گفت: ای خائن!مویز من بخوردی از چه وجه؟</p></div>
<div class="n" id="bn25"><p>گفتم: من از دل تو دانم و بر تو اعتماد دارم و نیز دانستم که هرچه داری ایثار کنی. </p></div>
<div class="n" id="bn26"><p>گفت: ای جاهل!من بر دل خویش اعتماد ندارم، تو بر دل من چون اعتماد داری. به پاکی حق - که عمری است تا برهراس او می‌زیم و نمی‌دانم که از من چه خواهد آمد - کسی درون خویش نداند، دیگری درون او چه داند. </p></div>
<div class="n" id="bn27"><p>و هم ابوعثمان گوید که با ابوحفص به خانه ابوبکر حلیفه بودم و جمعی اصحاب آنجا بودند، از درویشی یاد می‌کردند. گفتم: کاشکی حاضر بودی. </p></div>
<div class="n" id="bn28"><p>شیخ گفت: اگر کاغذی بودی رقعه ای نوشتمی تا بیامدی. </p></div>
<div class="n" id="bn29"><p>گفتم: اینجا کاغذ هست. </p></div>
<div class="n" id="bn30"><p>گفت: خداوند خانه به بازار رفته است. اگر مرده باشد و کاغذ وارث را شده باشد نشاید بر این کاغذ چیزی نوشتن. </p></div>
<div class="n" id="bn31"><p>بوعثمان گفت: بوحفص را گفتم: مرا چنان روشن شده است که مجلس علم گویم. گفت: تو را چه بدین آورده است؟ گفتم: شفقت تو بر خلق تا چه حد است؟ گفت: تا بدان حد که اگرحق تعالی مرا به عوض همه عاصیان در دوزخ کند و عذاب کند روا دارم. گفت: اگر چنین است بسم الله. اما چون مجلس گویی اول دل خود را پند ده و تن خود را؛ و دیگر آن که جمع آمدن مردم تو را غره نکند که ایشان ظاهر تو را مراقبت کنند و حق تعالی باطن تو را. </p></div>
<div class="n" id="bn32"><p>پس من بر تخت برآمدم. بوحفص پنهان در گوشه ای بنشست. چون مجلس به آخر آمد سایلی برخاست و پیراهنی خواست. در حال پیراهن خود بیرون آوردم و به وی دادم. ابوحفص گفت: یا کذاب انزل من المنبر. فرود آی ای دروغ زن از منبر! گفتم: چه دروغ گفتم؟ گفت: دعوی کردی که شفقت من بر خلق بیش از آن است که برخود؛ و به صدقه دادن سبقت کردی تا فضل سابقان تو را باشد؛ خود را بهتر خواستی. اگر دعوی تو راست بودی زمانی درنگ کردی تا فضل سابقان دیگری را باشد. پس تو کذابی ونه منبر جای کذابان است. </p></div>
<div class="n" id="bn33"><p>نقل است که یک روز در بازار می‌رفت. جهودی پیش آمد. او در حال بیفتاد و بیهوش شد. چون بهوش آمد از او پرسیدند. گفت: مردی را دیدم لباس عدل پوشیده و خود را دیدم لباس فضل پوشیده. ترسیدم که نباید که لباس فضل از سر من برکشند و در آن جهود پوشند، و لباس عدل از وی برکشند و در من پوشند. </p></div>
<div class="n" id="bn34"><p>و گفت: سی سال چنان بودم که حق را خشمگین می‌دیدم که در من نگریست. سبحان الله آن چه سوز و بیم بوده باشد او را در آن حال. </p></div>
<div class="n" id="bn35"><p>نقل است که ابوحفص را عزم حج افتاد و او عامی بود و تازی نمی‌دانست. چون به بغداد رسید مریدان با هم گفتندکه: شیئی عظیم باشد که شیخ الشیوخ خراسان راترجمانی باید تا زبان ایشان را بداند. پس جنید مریدان را به استقبال فرستاد و شیخ بدانست که اصحابنا چه می‌اندیشند. در حال تازی گفتن آغاز کرد - چنانکه اهل بغداد در فصاحت او عجب ماندند و جماعتی از اکابر پیش او جمع آمدند و از فتوت پرسیدند. بوحفص گفت: عبارت شما را است. شما گویید. </p></div>
<div class="n" id="bn36"><p>جنید گفت: فتوت نزدیک من آن است که فتوت از خود نبینی و آنچه کرده باشی آن را به خود نسبت ندهی که این من کرده ام. </p></div>
<div class="n" id="bn37"><p>بوحفص گفت: نیکوست آنچه گفتی. اما فتوت نزدیک من انصاف دادن و انصاف ناطلبیدن است. </p></div>
<div class="n" id="bn38"><p>جنید گفت:افضل در عمل آرید اصحابنا. </p></div>
<div class="n" id="bn39"><p>بوحفص گفت: این به سخن راست نیاید. </p></div>
<div class="n" id="bn40"><p>جنید چون این بشنید گفت: برخیزید ای اصحابنا که زیادت آورد بوحفص برآدم و ذریت او در جوانمردی. یعنی خطی گرد اولاد آدم بکشید در جوانمردی، اگر جوانمردی این است که او می‌گوید. </p></div>
<div class="n" id="bn41"><p>و بوحفص اصحاب خویش را عظیم به هیبت و ادب داشتی و هیچ مرید را زهره نبودی که در پیش او بنشستی و چشم بر روی او نیارستی انداخت و پیش او همه برپای بودندی و بی امر او ننشستندی. بوحفص سلطان وار نشسته بودی. </p></div>
<div class="n" id="bn42"><p>جنید گفت: اصحاب را ادب سلاطین آموخته ای. </p></div>
<div class="n" id="bn43"><p>بوحفص گفت: تو عنوان نامه بیش نمی‌بینی اما از عنوان دلیل توان ساخت که در نامه چیست. </p></div>
<div class="n" id="bn44"><p>پس ابوحفص گفت: دیگی زیره با و حلوا فرمای تا بسازند. </p></div>
<div class="n" id="bn45"><p>جنید اشارت کرد به مریدی تا بسازد. چون بیاورد ابوحفص گفت: بر سر حمالی نهید تا می‌برد. چندانکه خسته گردد آنجا بر در هر خانه ای که رسیده باشد آواز دهد، و هرکه بیرون آید به وی دهد. </p></div>
<div class="n" id="bn46"><p>حمال چنان کرد و می‌رفت تا خسته شدو طاقت نماند. بنهاد بر در خانه ای و آواز داد. پیری خداوند خانه بود. گفت: اگر زیره و با حلوا آورده ای، تا دربگشاییم. </p></div>
<div class="n" id="bn47"><p>گفت: آری. دربگشاد و گفت: درآر. </p></div>
<div class="n" id="bn48"><p>حمال گفت: عجب داشتم. از پیر پرسیدم که این چه حال است و تو چه دانستی که ما زیره با و حلوا آورده ایم؟</p></div>
<div class="n" id="bn49"><p>گفت: دوش در مناجات این بر خاطرم بگذشت که مدتی است فرزندان من از من این می‌طلبند. دانم که بر زمین نیفتاده باشد. </p></div>
<div class="n" id="bn50"><p>نقل است که مریدی بود در خدمت بوحفص - سخت با ادب - جنید چند بار در وی نگرست. از آنکه او خوش آمدش پرسید که : چند سال است تا در خدمت شماست؟ </p></div>
<div class="n" id="bn51"><p>ابوحفص گفت: ده سال است. </p></div>
<div class="n" id="bn52"><p>گفت: ادبی تمام دارد و فری عجب و شایسته جوانی است. </p></div>
<div class="n" id="bn53"><p>ابوحفص گفت: آری، هفده هزار دینار در راه ما باخته است و هفده هزار دیگر وام کرده ام و در باخته‌ام، هنوز زهره آن ندارد که از ما سخنی پرسد. </p></div>
<div class="n" id="bn54"><p>پس ابوحفص روی به بادیه نهاد. گفت: ابوتراب را دیدم در بادیه و من شانزده روز هیچ نخورده بودم. بر کنار حوضی رفتم تا آب خورم. به فکری فرورفتم. ابوتراب گفت: تو را چه نشانده است اینجا؟ گفتم: میان علم و یقین انتظار می‌کنم تا غلبه کدام را بود تا یار آن دیگر باشم که غالب باشد. یعنی اگر غلبه علم را بود آب خورم و اگر یقین را بود بروم. </p></div>
<div class="n" id="bn55"><p>بوتراب گفت: کار تو بزرگ شود. </p></div>
<div class="n" id="bn56"><p>پس چون به مکه رسید جماعتی مسکین را دید مضطر و فرومانده. خواست که در حق ایشان انعامی کند. گرم گشت. حالتی بر وی ظاهر شد، دست فرو کرد و سنگی برداشت و گفت: به عزت او که اگر چیزی به من ندهی جمله قنادیل مسجد بشکنم. </p></div>
<div class="n" id="bn57"><p>این بگفت و در طواف آمد. در حال یکی بیامد و صره ای زر بیاورد و بدو داد تا بر درویشان خرج کرد. چون حج بگزارد و به بغداد آمد اصحاب جنید از او استقبال کردند. جنید گفت: ای شیخ! راه آورد ما را چه آورده ای؟ </p></div>
<div class="n" id="bn58"><p>بوحفص گفت: مگر یکی از اصحاب ما چنانکه می‌بایست زندگانی نمی‌توانست کرد؟ اینم فتوح بود که گفتم اگر از برادری ترک ادبی بینید آن را عذری از خود برانگیزید و بی او آن عذر را از خود بخواهید. اگر بدان عذر غبار برنخیزد و حق به دست تو بود عذر بهتر برانگیزد و بی او عذری دیگر از خود بخواه. اگر بدین همه غبار برنخیزد عذری دیگر انگیز تا چهل بار. اگر بعد از آن غبار برنخیزد و حق به جانب تو باشد و آن چهل عذر در مقابله آن جرم نیفتد بنشین و با خود بگوی که زهی گاو نفس! زهی گران و تاریک! زهی خودرای بی ادب! زهی ناجوانمرد جافی که تویی!برادری برای جرمی چهل عذر از تو خواست و تو یکی نپذیرفتی و همچنان بر سر کار خودی. من دستم از تو شستم. تو دانی. چنانکه خواهی می‌کن. </p></div>
<div class="n" id="bn59"><p>جنید چون این بشنید تعجب کرد. یعنی این قوت که را تواند بود. </p></div>
<div class="n" id="bn60"><p>نقل است که شبلی چهار ماه بوحفص را مهمان کرد و هرروز چند لون طعام و چند گونه حلوا آوردی. آخر چون به وداع او رفت گفت: یا شبلی!اگر وقتی به نشابور آیی میزبانی و جوانمردی به تو آموزم. </p></div>
<div class="n" id="bn61"><p>گفت: یا اباحفص! چه کردمی؟ </p></div>
<div class="n" id="bn62"><p>گفت: تکلف کردی و متکلف جوانمرد نبود. مهمان را چنان باید داشت که خود را به آمدن مهمانی گرانی نیایدت و به رفتن شادی نبودت و چون تکلف کنی آمدن او بر تو گران بود و رفتن آسان و هرکه را با مهمان حال این بود ناجوانمردی بود. </p></div>
<div class="n" id="bn63"><p>پس چون شبلی به نشابور آمد پیش ابوحفص فرود آمد و چهل تن بودند. بو حفص شبانه چهل و یک چراغ برگرفت. شبلی گفت: نه، گفته بودی که تکلف نباید کرد. </p></div>
<div class="n" id="bn64"><p>بوحفص گفت: برخیز و بنشان. </p></div>
<div class="n" id="bn65"><p>شبلی برخاست و هرچند جهد کرد یک چراغ بیش نتوانست نشاند. پس گفت: یا شیخ!این چه حال است؟ </p></div>
<div class="n" id="bn66"><p>گفت: شما چهل تن بودیت فرستاده حق - که مهمان فرستاده حق بود - لاجرم به نام هریکی چراغی گرفتم برای خدای و یکی برای خود. آن چهل که برای خدای بود نتوانستی نشاند اما آن یکی که از برای من بود نشاندی. تو هرچه در بغداد کردی برای من کردی و من اینچه کردم برای خدای کردم. لاجرم آن تکلف باشد و این نه. </p></div>
<div class="n" id="bn67"><p>بوعلی ثقفی گویدکه: بوحفص گفت: هرکه افعال و احوال خود به هروقتی نسنجد به میزان کتاب و سنت و خواطر خود را متهم ندارد او را از جمله مردان مشمر. </p></div>
<div class="n" id="bn68"><p>پرسیدندکه : ولی را خاموشی به یا سخن؟ </p></div>
<div class="n" id="bn69"><p>گفت: اگر سخنگوی آفت سخن داند هرچند تواند خاموش باشد، اگر چه به عمر نوح بود. و خاموش اگر راحت خاموشی بداند از خدای در خواهد تا دو چند عمر نوح دهدش تا سخن نگوید. </p></div>
<div class="n" id="bn70"><p>گفتند: چرا دنیا را دشمن داری؟ </p></div>
<div class="n" id="bn71"><p>گفت: از آنکه سرایی است که هر ساعت بنده را در گناهی دیگر می‌اندازد. </p></div>
<div class="n" id="bn72"><p>گفتند: اگر دنیا بد است تو به نیک است و توبه هم در دنیا حاصل شود. </p></div>
<div class="n" id="bn73"><p>گفت: چنین است. اما به گناهی که در دنیا کرده می‌آید. یقینم و در یقین تو نه به شک و بر خطریم. </p></div>
<div class="n" id="bn74"><p>گفتند: عبودیت چیست؟ </p></div>
<div class="n" id="bn75"><p>گفت: آنکه ترک هرچه توراست بگویی، و ملازم باشی چیزی را که تو را بدان فرموده اند. </p></div>
<div class="n" id="bn76"><p>گفتند: درویشی چیست؟ </p></div>
<div class="n" id="bn77"><p>گفت: به حضرت خدای شکستگی عرضه کردن. </p></div>
<div class="n" id="bn78"><p>گفتند: نشان دوستان چیست؟ </p></div>
<div class="n" id="bn79"><p>گفت: آنکه روزی که بمیرد دوستان شاد شوند. یعنی چنان مجرد از دنیا بیرون رود که از وی چیزی نماند که آن خلاف دعوی او بود در تجرید. </p></div>
<div class="n" id="bn80"><p>گفتند: ولی کیست؟ </p></div>
<div class="n" id="bn81"><p>گفت: آنکه او را قوت کرامات داده باشند و او را ازآن غایب گردانیده. </p></div>
<div class="n" id="bn82"><p>گفتند: عاقل کیست؟</p></div>
<div class="n" id="bn83"><p>گفت: آنکه از نفس خویش اخلاص طلبد. </p></div>
<div class="n" id="bn84"><p>گفتند: بخل چیست؟ </p></div>
<div class="n" id="bn85"><p>گفت: آنکه ایثار ترک کند د روقتیکه بدان محتاج بود. </p></div>
<div class="n" id="bn86"><p>و گفت: ایثار آن است که مقدم داری نصیب برادران بر نصیب خود در کارهای دنیا و آخرت. </p></div>
<div class="n" id="bn87"><p>و گفت: کرم انداختن دنیا است برای کسی که بدان محتاج است و روی آوردن است بر خدای به سبب نیازی که تو را است به حق. </p></div>
<div class="n" id="bn88"><p>و گفت: نیکوترین وسیلتی که بنده بدو تقرب کند به خدای دوام فقر است به همه حالها و ملازم گرفتن سنت در همه فعلها و طلب قوت حلال. </p></div>
<div class="n" id="bn89"><p>و گفت: هرکه خود را متهم ندارد در همه وقتها و همه حالتها و مخالفت خود نکند مغرور بود و هرکه به عین رضا بخود نگرست هلاک شد. </p></div>
<div class="n" id="bn90"><p>و گفت: خوف چراغ دل بود و آنچه در دل بود از خیر و شر بدان چراغ توان دید. </p></div>
<div class="n" id="bn91"><p>و گفت: کسی را نرسد که دعوی فراست کند ولکن از فراست دیگران بباید ترسید. </p></div>
<div class="n" id="bn92"><p>و گفت: هرکه بدهد و بستاند او مردی است، و هرکه بدهد و نستاند او نیم مردی است؛ و هرکه ندهد و بستاند او مگسی است، نه کسی است در وی هیچ خیر نیست. </p></div>
<div class="n" id="bn93"><p>بوعثمان حیری گفت: معنی این سخن از او پرسیدند. گفت: هرکه از خدای بستاند وبدهد به خدای، او مردی است، زیرا که او دراین حال خود را نمی‌بیند در آنچه کند؛ و هرکه بدهد و نستاند او نیم مردی است زیرا که خود را می‌بیند در آنچه کند که ناستدن فضلی است؛ و هرکه ندهد و بستاند او هیچ کسی است، زیرا که گمان او چنان است که دهنده و ستاننده اوست نه خدای. </p></div>
<div class="n" id="bn94"><p>و گفت: هرکه در همه حال فضل خدای می‌بیند بر خویشتن امید می‌دارد که از هالکان نباشد. </p></div>
<div class="n" id="bn95"><p>و گفت: مبادا که عبادت خدای تو را پشتی بود تا معبود معبود بود. </p></div>
<div class="n" id="bn96"><p>و گفت: فاضلترین چیزی اهل اعمال را مراقبت خویش است با خدای. </p></div>
<div class="n" id="bn97"><p>و گفت: چه نیکوست استغنا به خدای و چه زشت است استغنا با نام. </p></div>
<div class="n" id="bn98"><p>و گفت: هرکه جرعه ای از شراب ذوق چشید بیهوش شد به صفتی که بهوش نتواند آمد مگر در وقت لقا و مشاهده. </p></div>
<div class="n" id="bn99"><p>و گفت: حال مفارقت نکند از عالم و مفارقت نکند با قبول. </p></div>
<div class="n" id="bn100"><p>و گفت: خلق خبر می‌دهند از وصول و از قرب مقامات عالی و مرا همه آرزوی آن است که دلالت کنند مرا به راهی که آن ره حق بود و اگرهمه یک لحظه بود. </p></div>
<div class="n" id="bn101"><p>و گفت: عبادات در ظاهر سرور است و در حقیقت غرور از آنکه مقدور سبقت گرفته است و اصل آن است که کس به فعل خود شاد نشود مگر مغروری. </p></div>
<div class="n" id="bn102"><p>و گفت: معاصی برید کفر است چنانکه زهر بر ید مرگ است. </p></div>
<div class="n" id="bn103"><p>و گفت: هرکه داند که او را برخواهند انگیخت و حسابش خواهند کرد و از معاصی اجتناب ننماید و از مخالفات روی نگرداند یقین است که از سر خود خبر می‌دهد که من ایمان ندارم به بعث و حساب. </p></div>
<div class="n" id="bn104"><p>و گفت: هرکه دوست دارد که دل او متواضع شود گو در صحبت صالحان باش و خدمت ایشان را ملازم. </p></div>
<div class="n" id="bn105"><p>و گفت: روشنی تنها به خدمت او است و روشنی جانها به استقامت. </p></div>
<div class="n" id="bn106"><p>و گفت: تقوی در حلال محض است و بس. </p></div>
<div class="n" id="bn107"><p>و گفت: تصوف همه ادب است. </p></div>
<div class="n" id="bn108"><p>و گفت: بنده در توبه بر هیچ کار نیست زیرا که توبه آن است که بدو آید نه آنکه از او آید. </p></div>
<div class="n" id="bn109"><p>و گفت: هر عمل که شایسته بود آن را برند و بر تو فراموش کنند. </p></div>
<div class="n" id="bn110"><p>و گفت: نابینا آن است که خدای را به اشیاء بیند و نبیند اشیاء را به خدا و بینا آن است که از خدای بود نظر او به مکونات. </p></div>
<div class="n" id="bn111"><p>نقل است که یکی از او وصیت خواست. گفت: یا اخی! لازم یک در باش تا همه درها برتو گشایند و لازم یک سید باش تا همه سادات تو را گردن نهند. </p></div>
<div class="n" id="bn112"><p>محمش گفت: بیست و دو سال با ابوحفص صحبت داشتم. ندیدم که هرگز با غفلت و انبساط خدای را یاد کرد که چون خدای را یاد کردی برسبیل حضور و تعظیم و حرمت یاد کردی و در آن حال متغیر شدی. چنانکه حاضران آن را بدیدندی. </p></div>
<div class="n" id="bn113"><p>و سخن اوست که گفت: در وقت نزع که شکسته دل باید بود به همه حال در تقصیرهای خویش. </p></div>
<div class="n" id="bn114"><p>از او پرسیدندکه : بر چه روی بخدا آورده گفت فقیر که روی به غنی آرد به چه آرد الا به فقر و فروماندگی. </p></div>
<div class="n" id="bn115"><p>و وصیت عبدالله سلمی آن بود که چون وفات کنم سر من بر پای ابوحفص نهید. رحمةالله علیه. </p></div>