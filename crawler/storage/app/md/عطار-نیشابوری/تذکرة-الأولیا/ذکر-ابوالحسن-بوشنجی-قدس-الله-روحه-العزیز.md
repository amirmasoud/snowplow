---
title: >-
    ذکر ابوالحسن بوشنجی قدس الله روحه العزیز
---
# ذکر ابوالحسن بوشنجی قدس الله روحه العزیز

<div class="n" id="bn1"><p>آن صادق کار دیده آن مخلص بارکشیده آن موحد یک رنگی شیخ ابوالحسن بوشنجی رحمة الله علیه از جوانمردان خراسان بود و محتشم‌ترین اهل زمانه و عالم‌ترین در علم طریقت و در تجرید قدمی ثابت داشت و ابن عطا و بوعثمان و جریدی و ابن عمرو رادیده و سالها از بوشنج برفت و بعراق می‌بود چون بازآمد بزندقه منسوب کردندش از آنجا بنشابور آمد و عمر را آنجا گذاشت چنانکه مشهور شد تا به حدی که روستائی را دراز گوشی گم شده بود پرسید که در نشابور پارساتر کیست گفتند ابوالحسن بوشنجی بیامد و در دامنش آویخت که خرمن تو بردهٔ درماند و گفت: ای جوانمرد غلط کرده‌ای من ترا اکنون می‌بینم گفت: نی خرمن تو بردهٔ درماند و دست برداشت و گفت: الهی مرا از وی باز خر در حال یکی آواز داد که او را رها کن که خر یافتیم بعد از آن روستائی گفت: ای شیخ من دانستم که تو ندیدهٔ لکن من خود را هیچ آبروی ندیدم برین درگاه گفتم تاتو نفسی بزنی تا مقصود من برآید. </p></div>
<div class="n" id="bn2"><p>نقلست که یک روز در راهی می‌رفت ناگاه ترکی درآمد و قفائی بر شیخ زد و برفت مردمان گفتند چرا کردی که او شیخ ابوالحسن است مردی بزرگ پشیمان شد و بازآمد و از شیخ عذر می‌خواست شیخ گفت: ای دوست فارغ باش که ما این نه از تو دیدیم از آنجا که رفت غلط نرود. </p></div>
<div class="n" id="bn3"><p>نقلست که در متوضا بود در خاطرش آمد که این پیرهن به فلان درویش می‌باید داد خادم را آواز داد وگفت: این پیراهن از سر من برکش و به فلان درویش ده خادم گفت: ای خواجه چندان صبر کن که بیرون آئی گفت: می‌ترسم که شیطان راه بزند و این اندیشه بر دلم سرد گرداند. </p></div>
<div class="n" id="bn4"><p>نقلست که یکی ازو پرسید که چگونهٔ گفت: دندانم فرسوده شد از نعمت حق خوردن و زبانم از کار شد از بس شکایت کردن. </p></div>
<div class="n" id="bn5"><p>پرسیدند که مروت چیست گفت: دست داشتن از آنچه بر تو حرام است تا مروتی باشد که باکرام الکاتبین کرده باشی و پرسیدند تصوف چیست گفت: تصوف اسمی و حقیقت پدیدنه و بیش از این حقیقت بود بی‌اسم. </p></div>
<div class="n" id="bn6"><p>پرسیدند از تصوف گفت: کوتاهی امل است و مداومت بر عمل. </p></div>
<div class="n" id="bn7"><p>پرسیدند از فتوت گفت: مراعات نیکو کردن و بر موافقت دایم بودن و از نفس خویش به ظاهر چیزی نادیدن که مخالف آن بود باطن تو. </p></div>
<div class="n" id="bn8"><p>و گفت: توحید آن بود که بدانی او آن مانند هیچ ذاتی نیست. </p></div>
<div class="n" id="bn9"><p>و گفت: اخلاص آنست که کرام الکاتبین نتواند نوشت و شیطان آنرا تباه نتواند کرد و آدمی بر وی مطلع نتواند شد. </p></div>
<div class="n" id="bn10"><p>و گفت: اول ایمان به آخر آن پیوسته است. </p></div>
<div class="n" id="bn11"><p>وگفتند ایمان و توکل چیست گفت: آنکه نان از پیش خود خوری و لقمه خرد خائی به آرام دل و بدانی که آنچه تراست از تو فوت نشود. </p></div>
<div class="n" id="bn12"><p>و گفت: هرکه خود را خوار داشت خدای تعالی او را رفیع القدر گردانید و هر که خود را عزیز داشت خدای تعالی او را خوار گردانید. </p></div>
<div class="n" id="bn13"><p>نقلست که یکی ازو دعا خواست گفت: حق تعالی تو را از فتنه تو نگاهدارد. </p></div>
<div class="n" id="bn14"><p>نقلست که بعد ازوفات اودرویشی بسر خاک او می‌رفت و از حق تعالی دنیا می‌خواست شبی ابوالحسن را بخواب دید که گفت: ای درویش چون بسر خاک ما آئی نعمت دنیا مخواه و اگر نعمت دنیا خواهی بسر خاک خواجگان دنیا رو و چون اینجا آئی همت از دو کون بریده کردن خواه رحمةالله علیه. </p></div>