---
title: >-
    بخش ۳۱ - در بیان سماع و کیفیت آن
---
# بخش ۳۱ - در بیان سماع و کیفیت آن

<div class="b" id="bn1"><div class="m1"><p>سماع اصلی بزرگست اندرین ره</p></div>
<div class="m2"><p>چو یابی سمع دل گردی تو آگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر سمع دلت نبود ندانی</p></div>
<div class="m2"><p>بپوشند بر تو یکسر این معانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی را کز سماعش ذوق نبود</p></div>
<div class="m2"><p>حقیقت دان که او را شوق نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنای عشقبازی شوق باشد</p></div>
<div class="m2"><p>کسی داند که صاحب ذوق باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی کو را نباشد سمع معنی</p></div>
<div class="m2"><p>نباشد از سماعش جمع معنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود معزول از سمع حقیقت</p></div>
<div class="m2"><p>نباشد در صف جمع طریقت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بود جان و دلش از ذوق محجوب</p></div>
<div class="m2"><p>نه طالب باشد او هرگز نه مطلوب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شود اوصاف او یکسر فسرده</p></div>
<div class="m2"><p>تو او را زنده دانی هست مرده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ازو هرگز نیاید هیچ کاری</p></div>
<div class="m2"><p>مگر ضایع گذارد روزگاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسر پرد درین ره مرد آگاه</p></div>
<div class="m2"><p>نثار از جان و دل سازد در این راه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زمان باید پس آنگه خوش مکانی</p></div>
<div class="m2"><p>پس اخوان تا شود آسوده جانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز منهیات شرعی دور باید</p></div>
<div class="m2"><p>ز ناجنسان بسی مستور باید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ازین جمله اگر یک چیز کم شد</p></div>
<div class="m2"><p>همه شادی دل اندوه و غم شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ولی برمبتدی زهر است دائم</p></div>
<div class="m2"><p>که نفس او بهستی گشت قائم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو مرتاض و مجاهد گشت شد پاک</p></div>
<div class="m2"><p>نماند از هستیش در راه خاشاک</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز گفت و خواب و خور بیزار گردد</p></div>
<div class="m2"><p>گهی مست و گهی هشیار گردد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زبانش دائماً گویای این راه</p></div>
<div class="m2"><p>بجان ودل بود پویای این راه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تمامی از کدورت پاک گردد</p></div>
<div class="m2"><p>برش هر زهر چون تریاک گردد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نباشد طالب جاه و متاعی</p></div>
<div class="m2"><p>بود پیوسته جویای سماعی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز آواز خوشی کاید بگوشش</p></div>
<div class="m2"><p>رود از شوق جانان عقل و هوشش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ببوی وصل جانان زنده باشد</p></div>
<div class="m2"><p>بوقت و فهم او گوینده باشد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو زین عالم ترقی کرد درحال</p></div>
<div class="m2"><p>در او ظاهر نگردد قول قوال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مگر گویندهٔ خوب و موافق</p></div>
<div class="m2"><p>قرین حال او معشوق و عاشق</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در آن پرده که رهرو را مقام است</p></div>
<div class="m2"><p>بدان کین سالکان را زآن مقام است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از آن صورت بود گر هست دلکش</p></div>
<div class="m2"><p>شود وقت عزیزان یک زمان خوش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خورد روحش بمعراج معانی</p></div>
<div class="m2"><p>ز جوی قرب آب زندگانی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اگر حاضر بود صاحب نیازی</p></div>
<div class="m2"><p>بروز آن وقت آن برگی و سازی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کند زان توشهٔ راه قیامت</p></div>
<div class="m2"><p>در آن ره یابد از آفت سلامت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو زین عالم ترقی کرد رهرو</p></div>
<div class="m2"><p>سماعش را تو شرح و وصف بشنو</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بوقت استماع قول قوّال</p></div>
<div class="m2"><p>که هر یک را دگرگون گردد احوال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تو گوی شفقت از روی فتوت</p></div>
<div class="m2"><p>بباید کردن او را صد مروت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ترا جمع بایدش کردن ز احوال</p></div>
<div class="m2"><p>که تا حاضر شود با تو در آن حال</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بصورت با تو در جنبد زمانی</p></div>
<div class="m2"><p>دهد حالات خود را زان نشانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شود بیمار حالان را طبیبی</p></div>
<div class="m2"><p>دهد صاحب نصیبان را نصیبی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بود چون کیمیا آن وقت و آن حال</p></div>
<div class="m2"><p>بگردد جمله رازان جمله احوال</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تمامت را برنگ خود برآرد</p></div>
<div class="m2"><p>بر ایشان روز بدبختی سرآرد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سزای وقت و استعداد هر یک</p></div>
<div class="m2"><p>ببخشد خلعتی زانجمله بیشک</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بود نادر چنین صاحب سماعی</p></div>
<div class="m2"><p>که بر هر کس بتابد زان شعاعی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بجان آن چنان وقت و چنان حال</p></div>
<div class="m2"><p>که تا یکسر بگردد بر تو احوال</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>در آن جمع ار شوی حاضر بیکبار</p></div>
<div class="m2"><p>نماند از گنه برگردنت بار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اگر یک دم در آن محفل نشینی</p></div>
<div class="m2"><p>بسا تخم سعادت را که چینی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خوری زان مجمع آب زندگانی</p></div>
<div class="m2"><p>بدل حاضر شو ای جان گر توانی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شنیده باشی ای جان حال شاهد</p></div>
<div class="m2"><p>مشو منکر تو بر احوال شاهد</p></div></div>