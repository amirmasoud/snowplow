---
title: >-
    بخش ۳ - آغاز سخن
---
# بخش ۳ - آغاز سخن

<div class="b" id="bn1"><div class="m1"><p>تمامت طول و عرض آفرینش</p></div>
<div class="m2"><p>ز بهر تست اگر داری تو بینش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهشت و دوزخ و رضوان و مالک</p></div>
<div class="m2"><p>فروع واصلش از منها و ذلک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برای تست جمله آفریده</p></div>
<div class="m2"><p>ترا از بهر حضرت برگزیده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگرچه بس شریفت آفریدند</p></div>
<div class="m2"><p>پی شغل بزرگت پروریدند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببازی در میاور کار خود را</p></div>
<div class="m2"><p>شناسا شو تو از خود نیک و بد را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بجان ودل شنو ا زمن سخن را</p></div>
<div class="m2"><p>بجو از اصل اصل خویشتن را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگر تادر چه شغل و در چه کاری</p></div>
<div class="m2"><p>مکن با جان خود زنهار خواری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ببین تا خود چه چیزی وز کجائی</p></div>
<div class="m2"><p>بجو از خویش اصل آشنائی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بچشم باطن خود خویش را بین</p></div>
<div class="m2"><p>به ریش وسبلتی ای مرد مسکین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه چشمی نه سری نه دست ونه پا</p></div>
<div class="m2"><p>بمعنی زین همه هستی مبرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بصورت آنی از چه غیر اینی</p></div>
<div class="m2"><p>تو معنی بین اگر مرد یقینی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>توئی تو گر تو خود را بازیابی</p></div>
<div class="m2"><p>مقام فخر و عز و ناز یابی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه چشم و صورتی ای مرد ره رو</p></div>
<div class="m2"><p>تو صورت بین مشو زنهار بشنو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>توئی اعجوبهٔ صنع الهی</p></div>
<div class="m2"><p>توئی مقصود صنع پادشاهی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>توئی و تونهٔ جانا تو بشنو</p></div>
<div class="m2"><p>نداند این سخن جز مرد رهرو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>طلسم بند وزندانست صورت</p></div>
<div class="m2"><p>از آن زندان برون شو بی ضرورت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو جسم و صورت خود را قفس دان</p></div>
<div class="m2"><p>چو بشکستی شدی فی الحال پران</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اگر هستی کبوتر ور خودی باز</p></div>
<div class="m2"><p>قفس بشکن بجای خویش شو باز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو این آلات را از بهر صورت</p></div>
<div class="m2"><p>بتو دادند ترا شد این ضرورت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که تا تخم سعادت را نشانی</p></div>
<div class="m2"><p>که چون آنجا رسی بی پر نمانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نباید در شقاوت خرج کردن</p></div>
<div class="m2"><p>از آن دوزخ نباید درج کردن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کمال خویش اینجا کسب کن هان</p></div>
<div class="m2"><p>تو خود را از طلسم جسم برهان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مزین کن بحکمت جان خود را</p></div>
<div class="m2"><p>که تا عارف شوی هر نیک و بد را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وجود خود بحکمت کن تو گلشن</p></div>
<div class="m2"><p>که تا احوال گردد بر تو روشن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بچشم باطن خود گوش میدار</p></div>
<div class="m2"><p>که تا کج بین نگردی آخر کار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>حقیقت راه خود را باز بینی</p></div>
<div class="m2"><p>مبادا باطل از حق برگزینی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو راه شرع را ره دان حقیقت</p></div>
<div class="m2"><p>که تا باشی تو از اهل طریقت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خلاف شرع جمله باطل آمد</p></div>
<div class="m2"><p>وزان بی‌حاصلیها حاصل آمد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اگر خواهی که یابی نزد حق بار</p></div>
<div class="m2"><p>سرکوی شریعت را نگهدار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سر موئی مگر دان از شریعت</p></div>
<div class="m2"><p>که تا یابی تو ذوقی از حقیقت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز خواب و خوردو خفت و گفت زنهار</p></div>
<div class="m2"><p>بتدریج اندک اندک کم کن ای یار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>که تا صافی شوی خود را بدانی</p></div>
<div class="m2"><p>کزان دانش فزائی زندگانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بچشم خود جمال خویش بنگر</p></div>
<div class="m2"><p>که هستی تو در این ویرانه درخور</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>غریبی اندرین ویرانه گلخن</p></div>
<div class="m2"><p>فراموشت شد آن آباد گلشن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بخود بازآی و عزم آن سفر کن</p></div>
<div class="m2"><p>بمحسوسات بر یکسر گذر کن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>وطنگاه نخستین تو آنست</p></div>
<div class="m2"><p>که ازچشم سرت دایم نهانست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اگر تو دوست داری آن وطنگاه</p></div>
<div class="m2"><p>شوی از خاصگان حضرت شاه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو تو با معدن اصلی روی زود</p></div>
<div class="m2"><p>خدا گردد در این حال از تو خوشنود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مشو زنهار گرد آلود این خاک</p></div>
<div class="m2"><p>که تا راهت بود بالای افلاک</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز لذات بهیمی روی برتاب</p></div>
<div class="m2"><p>که تا خوشرو شوی چون تیر پرتاب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ملک را خدمت دیوان مفرمای</p></div>
<div class="m2"><p>ملک را کار در دیوان مفرمای</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>که تا مستوجب هر بدنگردی</p></div>
<div class="m2"><p>سزای جای دیو و دد نگردی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>رفیقان بد و نیکند با تو</p></div>
<div class="m2"><p>همه چون دانه و ریگند با تو</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو کبر و بخل و حرص و شهوت و آز</p></div>
<div class="m2"><p>همان مکر و حسد پس کبر و پس ناز</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز نیکان چون تواضع پس قناعت</p></div>
<div class="m2"><p>پس آنگاهی سخا و جود و طاعت</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو علم و حکمت و پرهیزکاری</p></div>
<div class="m2"><p>پس آنگه پیشه کن در بردباری</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مبدل کن تو آنها را باینها</p></div>
<div class="m2"><p>که تا سودت شود جمله زیانها</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو شد تبدیل اخلافت میسر</p></div>
<div class="m2"><p>شوی صافی و روحانی و انور</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بفکرت چشم معنی را کنی باز</p></div>
<div class="m2"><p>شود معلومت آنگه سر هر راز</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>هر آن چیزی که در کون و مکانست</p></div>
<div class="m2"><p>نشان هر یک اندر تو عیانست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>درونت جوهری بر جمله افزون</p></div>
<div class="m2"><p>بود اصلش ورای هفت گردون</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تو تادانای آن جوهر نگردی</p></div>
<div class="m2"><p>ز توظاهر نگردد هیچ مردی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>شناسش چون یکی را حاصل آمد</p></div>
<div class="m2"><p>حقیقت دان که آنکس واصل آمد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بود مقصود ره دانستن اوی</p></div>
<div class="m2"><p>چو دانستی بری از این مکان روی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>همه سختی اعمال و عبادت</p></div>
<div class="m2"><p>شدن مرتاض و کردن ترک عادت</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>منازل قطع کردن ره بریدن</p></div>
<div class="m2"><p>شب وروز اندر آن وادی دویدن</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>مراد آنست کان جوهر بدانی</p></div>
<div class="m2"><p>خوری زان دانش آب زندگانی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو علمت با خبر انباز گردد</p></div>
<div class="m2"><p>عمل با هر دو آن دمساز گردد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>مدد بخشد خدایت از هدایت</p></div>
<div class="m2"><p>شوی صاحب قدم اندر هدایت</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ز هستیهای خود درویش گردی</p></div>
<div class="m2"><p>شناسای وجود خویش گردی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چو زان دانش کنی حاصل ضیا را</p></div>
<div class="m2"><p>بقدر خویش بشناسی خدا را</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>اگرچه هست آن جوهر گزیده</p></div>
<div class="m2"><p>حقیقت دان که هست آن آفریده</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>زبان عاجز شود از شرح ذاتش</p></div>
<div class="m2"><p>ولی بعضی توان گفت از صفاتش</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ورا بخشید معبود یگانه</p></div>
<div class="m2"><p>ز لطف خود صفات بیگانه</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بود یک رویش اندر حضرت پاک</p></div>
<div class="m2"><p>شود زان روی دیگر او طربناک</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ز روی دیگر او کار تو سازد</p></div>
<div class="m2"><p>بنور خویش جسمت را نوازد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>نه خارج از بدن باشد نه داخل</p></div>
<div class="m2"><p>نداند این سخن جز مرد کامل</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>شناسائی گهر کار عزیز است</p></div>
<div class="m2"><p>نداند هر کسی کان خود چه چیز است</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بتازی آن گهر را روح خوانند</p></div>
<div class="m2"><p>ازو مردم به جز نامی ندانند</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ورای روح سرّی هست دائم</p></div>
<div class="m2"><p>که روح از سرّ آن نور است قائم</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>نظام سرّ و روح از سرّ سر دان</p></div>
<div class="m2"><p>کزان نورند دائم هر دو گردان</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>تو سرّ سر بخوانش یا خفی دان</p></div>
<div class="m2"><p>ز هر کس این حکایت مختفی دان</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>تمامت انبیا زنده بدانند</p></div>
<div class="m2"><p>که آن سر خفی را می‌بدانند</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>مزین اولیا زان نور باشند</p></div>
<div class="m2"><p>از آن پیوسته زان مسرور باشند</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>نداده هیچکس را دیگر آن نور</p></div>
<div class="m2"><p>تمامت گشته‌اند زان نور مهجور</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بنور قلب و عقل و روح عامی</p></div>
<div class="m2"><p>شود پیدا چو دارد نیکنامی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بدان هر کس که شد زنده نمیرد</p></div>
<div class="m2"><p>فنا دیگر گریبانش نگیرد</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>سخن چون منغلق خواهید ای یار</p></div>
<div class="m2"><p>نباید گفت منکر گردد اغیار</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بلی سرّ خفی را جز که ابرار</p></div>
<div class="m2"><p>نداند دیگری از جمع احرار</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>نیابد هیچکس زان جمله بنیاد</p></div>
<div class="m2"><p>سرای آن گهر جز آدمی زاد</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>سزای روح قدسی آدم آمد</p></div>
<div class="m2"><p>که فخر ملّک و تاج عالم آمد</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>بصورت قبلهٔ روحانیان شد</p></div>
<div class="m2"><p>بمعنی پیشوای انس و جان شد</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>مزیّن چون بدان گوهر شد آدم</p></div>
<div class="m2"><p>امانت داشتن گشتش مسلم</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>بدان گوهر کشیدن شاید آن یار</p></div>
<div class="m2"><p>که بود آدم بدان جوهر سزاوار</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>چون دارد نسبتی با حضرت پاک</p></div>
<div class="m2"><p>بدان نسبت کشید آن یار چالاک</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>چوآدم گشت از آن جوهر مُزّین </p></div>
<div class="m2"><p>امانت داشتن را شد مبیّن</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>یقین گشتش که در باب فتّوت</p></div>
<div class="m2"><p>امانت داشتن هست از مروت</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>چو آدم شد بدان خلعت مکّرم</p></div>
<div class="m2"><p>از آن گنج مروّت گشت خرّم</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>بجان میداشت آدم پاس آن گنج</p></div>
<div class="m2"><p>که تا از دشمنش ناید بدو رنج</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>امین آن امانت آدم آمد</p></div>
<div class="m2"><p>که ثابت در دیانت آدم آمد</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>امانت داشتن کاری عظیمست</p></div>
<div class="m2"><p>دل سنگین کوه از وی دو نیم است</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>زمین و آسمان را نیست یارا</p></div>
<div class="m2"><p>پذیرفتن نهان و آشکارا</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>بجان و دل کند آدم قبولش</p></div>
<div class="m2"><p>گهی خواند ظلوم و گه جهولش</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>گهی عاصی و گه عادیش خوانند</p></div>
<div class="m2"><p>بصورت دورش از جنت برانند</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>چو بیند کو شکسته شد ز عصیان</p></div>
<div class="m2"><p>بخواهد عذر او کش عذر نسیان</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>عتابی ظاهرا بر وی براند</p></div>
<div class="m2"><p>براه باطنش با خویش خواند</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>خراب آباد گردد او بصورت</p></div>
<div class="m2"><p>باشگ چشم شوید آن کدورت</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>شود گنج امانت را سزاوار</p></div>
<div class="m2"><p>که تا پوشیده میدارد ز اغیار</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>خرابی جای گنج پادشاه است</p></div>
<div class="m2"><p>چه دانی تا خرابی خود چه جاه است</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>عتاب دوستان خورشید جانست</p></div>
<div class="m2"><p>بسا صلحی که اندر وی نهانست</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>بنای دوستی خود بر عتابست</p></div>
<div class="m2"><p>عتاب اندر محبت فتح بابست</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>محبت چون که بر آدم اثر کرد</p></div>
<div class="m2"><p>بکلی خویش را از خود بدر کرد</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>قبول منصب علم اسامی</p></div>
<div class="m2"><p>همان حور و قصور شاد کامی</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>خوشیهای بهشت هشتگانه</p></div>
<div class="m2"><p>همان عیش و حیات جاودانه</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>نعیم هشت خلد از کارسازی</p></div>
<div class="m2"><p>بیک گندم بداد از پاکبازی</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>تمامت طوق و تاج و تخت جنت</p></div>
<div class="m2"><p>نهاد اندر ره عشق و محبت</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>یقین بودش که با آن برگ و آن ساز</p></div>
<div class="m2"><p>نشاید عشقبازی کردن آغاز</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>فداکردی همه اندر ره عشق</p></div>
<div class="m2"><p>که تا بارش بود بر درگه عشق</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>مجرد شد از آن جمله علایق</p></div>
<div class="m2"><p>برو مکشوف شد جمله حقایق</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>نظر افتادش اندر گوهر فقر</p></div>
<div class="m2"><p>گمان برد او که باشد رهبر فقر</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>زبان حال خواجه گفتش ای باب</p></div>
<div class="m2"><p>بدست من شود مفتوح این باب</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>بمعنی زان سبق بردم ز اخوان</p></div>
<div class="m2"><p>که من برداشتم این گوهر از کان</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>چوخاص ماست این گوهر توئی باب</p></div>
<div class="m2"><p>بیاری زن قدم این نکته دریاب</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>تمامت انبیا جویای آنند</p></div>
<div class="m2"><p>در این ره جمله از ما باز مانند</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>چو آمد اختصاص ماش مانع</p></div>
<div class="m2"><p>ببویش هر یکی گشتند قانع</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>دل خود را برون آور از آن بند</p></div>
<div class="m2"><p>ببوی فقر قانع باش و خرسند</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>نیارد کرد کس آن را تمنا</p></div>
<div class="m2"><p>که اقطابند و خاص حضرت ما</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>بود در امتم هر جا غریبی</p></div>
<div class="m2"><p>ازین گوهر بود او را نصیبی</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>بهین امتان از بهر اینند</p></div>
<div class="m2"><p>که اندر حفظ این گوهر امینند</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>بسمع دل چو بشنید این ندا را</p></div>
<div class="m2"><p>گزید از بهر خود راه مدارا</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>بدانست آدم از راه نبوت</p></div>
<div class="m2"><p>که خاص او نیامد این فتوت</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>طریق عشقبازی کرد آغاز</p></div>
<div class="m2"><p>گهی با راز می‌بد گاه با ناز</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>امانت را بجان میداشت پاسی</p></div>
<div class="m2"><p>ز حوطی قرب می‌نوشید کاسی</p></div></div>