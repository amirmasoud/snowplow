---
title: >-
    بخش ۶ - دربیان و شرح عقل فرماید
---
# بخش ۶ - دربیان و شرح عقل فرماید

<div class="b" id="bn1"><div class="m1"><p>خرد شد کاشف سرّ الهی</p></div>
<div class="m2"><p>بنور او شود روشن سیاهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خرد شد پیشوای اهل ایمان</p></div>
<div class="m2"><p>هم او شد رهنمای جمله نیکان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خرد شد قهرمان خانهٔ تن</p></div>
<div class="m2"><p>اگرچه هست او بیگانهٔ تن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ازو گر نور نبود در دماغت</p></div>
<div class="m2"><p>ز نادانی خلل گیرد چراغت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندانی خالق خود را نه خود را</p></div>
<div class="m2"><p>شناسا می‌نگردی نیک و بد را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلیل و رهبر آمد مرد ره را</p></div>
<div class="m2"><p>بنور او توانی دیده ره را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگردد هیچ چیزش مانع نور</p></div>
<div class="m2"><p>بود روشن برو نزدیک و هم دور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گهی شعله زند بالای افلاک</p></div>
<div class="m2"><p>گهی گردد بگرد تودهٔ خاک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نهایتها بنور خود ببیند</p></div>
<div class="m2"><p>سعادتهای هر یک برگزیند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بپای خود بپوید گرد عالم</p></div>
<div class="m2"><p>گشاید مشکلاتش را بیک دم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کند معلوم اسرار معانی</p></div>
<div class="m2"><p>شود روشن برو راز نهانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بود محکوم احکام شریعت</p></div>
<div class="m2"><p>شود منعم بانعام شریعت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بنور علم عقل آگاه باشی</p></div>
<div class="m2"><p>اگر نه تا ابد گمراه باشی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو با روحانیان همره بعقلی</p></div>
<div class="m2"><p>مرایشان را تو اندر خور بعقلی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بدان جوهر هرانکو نیست قایم</p></div>
<div class="m2"><p>بود اندر صف جمع بهایم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو محکوم شریعت بهر آنی</p></div>
<div class="m2"><p>که داری در دماغ از در کانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جدا گر مانی از وی روزگاری</p></div>
<div class="m2"><p>شریعت را نباشد با تو کاری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زهی گوهر که او محکوم شرع است</p></div>
<div class="m2"><p>اساس بندگی زان اصل و فرع است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سزای معرفت از بهر آنی</p></div>
<div class="m2"><p>که آن جوهر تو داری در نهانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همان جوهر اگر یادت نبودی</p></div>
<div class="m2"><p>بدرگاه خدا یادت نبودی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عجب نوریست نور عقل و ای جان</p></div>
<div class="m2"><p>شود پیدا ز نورش جمله پنهان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همه چیزی بنور خود بداند</p></div>
<div class="m2"><p>مگر در راه عشق او خیره ماند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خوشا مرغی که اصل کیمیا شد</p></div>
<div class="m2"><p>بصورت درد و در معنی دوا شد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نشاید زندگی بی عشق کردن</p></div>
<div class="m2"><p>نه هرگز بندگی بی عشق کردن</p></div></div>