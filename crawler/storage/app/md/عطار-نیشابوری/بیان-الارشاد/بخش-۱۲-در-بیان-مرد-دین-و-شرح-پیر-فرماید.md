---
title: >-
    بخش ۱۲ - در بیان مرد دین و شرح پیر فرماید
---
# بخش ۱۲ - در بیان مرد دین و شرح پیر فرماید

<div class="b" id="bn1"><div class="m1"><p>چو دولت همنشین مرد باشد</p></div>
<div class="m2"><p>همیشه او قرین درد باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو درد دین نماید وی ترا راه</p></div>
<div class="m2"><p>شوی از خواب غفلت زود آگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پدید آید ترا در سینه شوقی</p></div>
<div class="m2"><p>که یابد نفس تو زان شوق ذوقی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس آنگه شوق و ذوقت سوز گردد</p></div>
<div class="m2"><p>شبت زان سوز همچون روز گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شوی طالب که تا خود کیستی تو</p></div>
<div class="m2"><p>درین دنیا ز بهر چیستی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شب و روزت بود این درد دایم</p></div>
<div class="m2"><p>وجود تو بود زین درد قایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشایخ درد دین دانند این درد</p></div>
<div class="m2"><p>کنند از جمله آلایش ترا فرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عجب دردیست این درد مبارک</p></div>
<div class="m2"><p>بود در خورد هر مرد مبارک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مبادا هیچکس زین درد خالی</p></div>
<div class="m2"><p>که ماند از سعادت فرد و خالی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دوای جملهٔ انسی و جنی</p></div>
<div class="m2"><p>همین درد است می‌باید که دانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خوشا دردا که آخر او دوا شد</p></div>
<div class="m2"><p>تمامت رنجها را او شفا شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همان دل کو ز دین بی درد باشد</p></div>
<div class="m2"><p>یقین دان کو ز معنی فرد باشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>درونت گر دمی از وی جدا شد</p></div>
<div class="m2"><p>بصد گونه بلاها مبتلا شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگرچه نفست از وی در عذابست</p></div>
<div class="m2"><p>ولیکن جان و دل را فتح بابست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو درد دین ترا در دل اثر کرد</p></div>
<div class="m2"><p>ز خویشت خواجگی باید بدر کرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بباید یک نظر کردن در آفاق</p></div>
<div class="m2"><p>تفکر کردن اندر عهد و میثاق</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پس آنگه زان نظر باید بریدن</p></div>
<div class="m2"><p>تمامت پرده هستی دریدن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بفکرت باید اندر خود نظر کرد</p></div>
<div class="m2"><p>پس آنگه بیخود اندر خود سفر کرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو آن چیزی که تو جویای آنی</p></div>
<div class="m2"><p>برون از تو نباشد تا تودانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در آفاقش نیابی گرچه جوئی</p></div>
<div class="m2"><p>ولی در خود بیابی گر بجوئی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ولی تنها ندانی کار کردن</p></div>
<div class="m2"><p>بباید این سفر ناچار کردن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بخود گر برنشینی گم کنی راه</p></div>
<div class="m2"><p>بدان این تا نیفتی در بن چاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بسا طالب که بر خود برنشستند</p></div>
<div class="m2"><p>تمامت راه را بر خود ببستند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نشاید بیدلیلی رفتن این راه</p></div>
<div class="m2"><p>که درهومنزلش باشد دو صد چاه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در آن هر یک بود غولی خطرناک</p></div>
<div class="m2"><p>شده در رهزدن گستاخ و چالاک</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بآواز خوشت خواند فراچاه</p></div>
<div class="m2"><p>نهد بر دست و پایت بند از آن چاه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در آنچاه طبیعت ار بمانی</p></div>
<div class="m2"><p>شود یکباره تلخت زندگانی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بباید رهبر چابک طلب کرد</p></div>
<div class="m2"><p>که او داند ترا در ره ادب کرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>برایش خویشتن تسلیم میکن</p></div>
<div class="m2"><p>رسوم و راه از آن تعلیم میکن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شریعت ورز باشد مرد هشیار</p></div>
<div class="m2"><p>شده مکشوف بروی جمله اسرار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>قدم اندر شریعت داشته او</p></div>
<div class="m2"><p>نه هرگز سنتی بگذاشته او</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نکرده یک نفس با او مدارا</p></div>
<div class="m2"><p>همه آفاق بر وی آشکارا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شده او مطمئن اندر همه حال</p></div>
<div class="m2"><p>بکرده ترک نفس و جاه با مال</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نه هرگز ره زده بر وی هوائی</p></div>
<div class="m2"><p>نه صادر گشته زاعمالش ریائی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گذشته از مقامات و ز تلوین</p></div>
<div class="m2"><p>شده قایم بحالات و بتمکین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>منازل قطع کرده ره بریده</p></div>
<div class="m2"><p>تمامت پردهٔ هستی دریده</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اجازت یافته در کارها او</p></div>
<div class="m2"><p>بجان ودل کشیده بارها او</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>علوم ظاهر و باطن برش جمع</p></div>
<div class="m2"><p>گدازان گشته اندر راه چون شمع</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>که تا با او دراین ره در بدایت</p></div>
<div class="m2"><p>بنور شمع او یابی هدایت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>صلاح کار او یکسر بجوید</p></div>
<div class="m2"><p>ز نفست زنگ خود بینی بشوید</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ترا در ره بهمّت پاس دارد</p></div>
<div class="m2"><p>منازل یک بیک بر تو شمارد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بگوید آفت هر منزلی چیست</p></div>
<div class="m2"><p>همان همره ترا در هر قدم کیست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نشان قرب و بعدو وصل هجران</p></div>
<div class="m2"><p>از آن یکسر بیاموزی او ای جان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو دولت پایمرد کار باشد</p></div>
<div class="m2"><p>همان بخت تو هر دم یار باشد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بدست آور چنین صاحب دلی را</p></div>
<div class="m2"><p>که بگشائی ازو هر مشکلی را</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>همان چیزی که فرماید تو زنهار</p></div>
<div class="m2"><p>بجان و دل کن استقبال آن کار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز دست ظاهر او خرقه در پوش</p></div>
<div class="m2"><p>بباطن رو بجان و دل همی کوش</p></div></div>