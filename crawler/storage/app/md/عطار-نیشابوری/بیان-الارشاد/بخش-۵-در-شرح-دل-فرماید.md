---
title: >-
    بخش ۵ - در شرح دل فرماید
---
# بخش ۵ - در شرح دل فرماید

<div class="b" id="bn1"><div class="m1"><p>بجدّ و سعی خود آن را طلب کن</p></div>
<div class="m2"><p>اگر یابی دل آنگاهی طرب کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همی جو دل اگر دل باز یابی</p></div>
<div class="m2"><p>که خود را محرم هر راز یابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو روی دل ببینی شادگردی</p></div>
<div class="m2"><p>به یک ره از خودی آزاد گردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برآید جمله ی کار تو از دل</p></div>
<div class="m2"><p>مراد تو شود زو جمله حاصل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو جان از دل به جز نامی ندانی</p></div>
<div class="m2"><p>که در قالب همیشه قلب خوانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مدان جانا تو دل آن گوشت پاره</p></div>
<div class="m2"><p>که کافر را بود چون سنگ خاره</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بود هر خوک و سگ را آنچنان دل</p></div>
<div class="m2"><p>از آن دل هیچ نتوان کرد حاصل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بود دل نور الطاف الهی</p></div>
<div class="m2"><p>نماید از سپیدی تا سیاهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بود منزلگهش آن گوشت بی‌ شک</p></div>
<div class="m2"><p>نگیرد نور او از پوست تا رگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همان نور لطیف روشن پاک</p></div>
<div class="m2"><p>بدین منزل فرود آمد بدین خاک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جمالش چون که بنماید ز بالا</p></div>
<div class="m2"><p>درین منزل شود نورش هویدا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بود چون قالبی آن قلب روحش</p></div>
<div class="m2"><p>بود زان روح هر دم صد فتوحش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>منور گردد اعضا ها از آن نور</p></div>
<div class="m2"><p>وجود تو شود زان نور مسرور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نماید نورش اول پاره پاره</p></div>
<div class="m2"><p>پس آنگه جمع گردد چون ستاره</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پس آن گه همچو مهتابی نماید</p></div>
<div class="m2"><p>درو هر لحظه نوری می‌ فزاید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ببینی آن گهی چون آفتابش</p></div>
<div class="m2"><p>شود روشن وجود از نور تابش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بگیرد نور او نزدیک و هم دور</p></div>
<div class="m2"><p>شود کار تو زان نور علی نور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فرو گیرد تمامی سینه ی تو</p></div>
<div class="m2"><p>شود شادی غم دیرینه ی تو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بود آیینه وجه الهی</p></div>
<div class="m2"><p>درو بینی هر آن چیزی که خواهی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نزول لطف حق را منزل اوست</p></div>
<div class="m2"><p>اگر تو طالبی دل را دل اوست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو وسعت یابد از نور الهی</p></div>
<div class="m2"><p>بود منظور لطف پادشاهی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گهی ارضی بود گاهی سمائی</p></div>
<div class="m2"><p>گهی صدقی بود گاهی صفایی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از آن خوانند قلب او را که هر دم</p></div>
<div class="m2"><p>بگردد صد ره اندر گرد عالم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز وجهی قلب انوار آمد آن نور</p></div>
<div class="m2"><p>بدین اسم او شد اندر جمع مشهور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هم او شد ملک خاص حضرت شاه</p></div>
<div class="m2"><p>نباشد دیو را هرگز در او راه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بود آیینه کل ممالک</p></div>
<div class="m2"><p>نماید اندرو رضوان و مالک</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز روح او روح می‌یابد پیاپی</p></div>
<div class="m2"><p>پس آنگه عقل راحت یابد از وی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هر آنکس را که بخشیدند آن دل</p></div>
<div class="m2"><p>مراد او شود یک سر بحاصل</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اگر داری خبر از دل تو مردی</p></div>
<div class="m2"><p>وگرنه از معانی جمله فردی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>وجودی را که از خود آگهی نیست</p></div>
<div class="m2"><p>سزای حضرت شاهنشهی نیست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بدل یابی خبر از سرّ هر کار</p></div>
<div class="m2"><p>بدل گردی قرین جمله احرار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تو صاحب دل شو ای مرد معانی</p></div>
<div class="m2"><p>که تا اسرار هر کاری بدانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به گوش دل شنیدن جمله اسرار</p></div>
<div class="m2"><p>به چشم عقل دیدن سر هر کار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>اگر آن چشم و آن گوشت نباشد</p></div>
<div class="m2"><p>بجز شیطان در آغوشت نباشد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اگر از اهل دل آگه نباشی</p></div>
<div class="m2"><p>یقین می دان که جز گمره نباشی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تو غافل دان هر آنک س را که پیوست</p></div>
<div class="m2"><p>بود از حب مال و جاه سرمست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به جمع مال دنیا هر که کوشد</p></div>
<div class="m2"><p>چینن کس چشم عقل خویش پوشد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تو عاقل آن کسی را دان که عقبی</p></div>
<div class="m2"><p>گزیند بر نعیم و ملک دنیا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به دنیا دار اگر معلول باشد</p></div>
<div class="m2"><p>بکار آخرت مشغول باشد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تو آن کس را که او آساید از کبر</p></div>
<div class="m2"><p>همیشه خویش را بزداید از کبر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به جان و دل شود جویای دنیا</p></div>
<div class="m2"><p>زبانش دائماً گویای دنیا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چنین کس را نشاید خواند عاقل</p></div>
<div class="m2"><p>بود دیوانه و مجنون و غافل</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از آن عالی تر آمد جوهر عقل</p></div>
<div class="m2"><p>که باشد هر سری اندر خور عقل</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نخستین گوهر پاک گزیده</p></div>
<div class="m2"><p>که هست ایزد تعالی آفریده</p></div></div>