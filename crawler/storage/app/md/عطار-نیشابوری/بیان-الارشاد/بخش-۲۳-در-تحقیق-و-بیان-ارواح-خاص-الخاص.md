---
title: >-
    بخش ۲۳ - در تحقیق و بیان ارواح خاص الخاص
---
# بخش ۲۳ - در تحقیق و بیان ارواح خاص الخاص

<div class="b" id="bn1"><div class="m1"><p>در آن شب خواجهٔ ما شد بمعراج</p></div>
<div class="m2"><p>نهاد او بر سرش از بندگی تاج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درون پرده دید ارواح جمعی</p></div>
<div class="m2"><p>شده ازنور تابان همچو شمعی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جمال معنیش منظور ایشان</p></div>
<div class="m2"><p>شده از نیستی در خاک راهش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه گشته ز جمعیت چو یک جان</p></div>
<div class="m2"><p>بفقر و مسکنت درگشته اخوان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه از روی معنی گشته یک رنگ</p></div>
<div class="m2"><p>همه فارغ شده از نام و از ننگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه حیران وقت لی مع الله</p></div>
<div class="m2"><p>درون پردهٔ اسرارشان راه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه در عشق صاحب درد گشته</p></div>
<div class="m2"><p>محبت را بجان در خورد گشته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه محبوب درگاه الهی</p></div>
<div class="m2"><p>همه مقصود صنع پادشاهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه اندر کشیده میل ما زاغ</p></div>
<div class="m2"><p>محبت برکشیده جمله را داغ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه در نیستی فقر مسکین</p></div>
<div class="m2"><p>شده آزاد از تلوین و تمکین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بداده جمله را پوشیده ز آغاز</p></div>
<div class="m2"><p>بخلوتخانه اسرار خود باز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شده فانی ز خود باقی بمحبوب</p></div>
<div class="m2"><p>همه هم طالب و هم گشته مطلوب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز غیرت یافته هر یک نصیبی</p></div>
<div class="m2"><p>بقرب اندر شده هر یک قریبی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز دل تابع شده او را هم ازجان</p></div>
<div class="m2"><p>نه معجز خواسته هرگز نه برهان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ندا آمد ز درگاه الهی</p></div>
<div class="m2"><p>که ای مقصود صنع پادشاهی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همین جمعند خاص صحبت تو</p></div>
<div class="m2"><p>عطاها یافته از حرمت تو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همه از نور خود موجود گشته</p></div>
<div class="m2"><p>از آن نورند خود مسعود گشته</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بصورت جمله مسکینندو درویش</p></div>
<div class="m2"><p>بمعنی جمله بی پیوند و بی خویش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خوش آمد خواجه را زان جمع پرنور</p></div>
<div class="m2"><p>شده اندر محبت مست و مخمور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بفقر و مسکنت چون دیدشان جمع</p></div>
<div class="m2"><p>همه گشته بمعنی چون یکی شمع</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو دید آن عهد و آن میثاق ایشان</p></div>
<div class="m2"><p>بصورت نیز شد مشتاق ایشان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در آن مجمع نمود از ذوق شوقی</p></div>
<div class="m2"><p>که شد در جان هر یک همچو طوقی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بکرد از لطف خود سردار اکرم</p></div>
<div class="m2"><p>با خوانیت ایشان رامکرم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تشرف یافتند ایشان بدین نام</p></div>
<div class="m2"><p>از آن نسبت برآمد جمله را کام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شراب فقر بی ایشان نخورد او</p></div>
<div class="m2"><p>بایشان و همه کس بخش کرد او</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بمسکینی چو ایشان را لقب دید</p></div>
<div class="m2"><p>همه افعالشان عین ادب دید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بحاجت صحبت ایشان زحق خواست</p></div>
<div class="m2"><p>که تا گردد تمامت کارشان راست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بصورت چونکه باز آمد ز معراج</p></div>
<div class="m2"><p>بجودش هر دو عالم گشته محتاج</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز ذوق صحبت ارواح ایشان</p></div>
<div class="m2"><p>نمیشد نزد نزدیکان و خویشان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ندا آمد که ای شهباز حضرت</p></div>
<div class="m2"><p>بگوش سرشنیده راز حضرت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>وجود تو ز بهر خاص و عام است</p></div>
<div class="m2"><p>ز جودت کار جمله با نظام است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بصورت اهل صورت را نگهدار</p></div>
<div class="m2"><p>که ما تا خود ترا آریم در کار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بمعنی یار غار اهل دل باش</p></div>
<div class="m2"><p>بهمت پاسدار اهل دل باش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو خواهی صحبت ارواح ایشان</p></div>
<div class="m2"><p>که گردی مستفیض ز اشباح ایشان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همان صحبت حوالت با نماز است</p></div>
<div class="m2"><p>در آن حالت که ما را با تو راز است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو معراج نماز آغاز کردی</p></div>
<div class="m2"><p>در آن ساعت هزاران ناز کردی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز جان چون راز حضرت می‌شنیدی</p></div>
<div class="m2"><p>همه ارواح ایشان جمع دیدی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شدی چشم دلش روشن بدان جمع</p></div>
<div class="m2"><p>که بودندی ز نورش گشته چون شمع</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بدی معلومش از نور نبوت</p></div>
<div class="m2"><p>که هستند جملگی اهل فتوت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز جاهش جمله صاحب جاه گشته</p></div>
<div class="m2"><p>تمامت خاص آن درگاه گشته</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پناه امت بیچاره باشند</p></div>
<div class="m2"><p>تمامت را بجان غمخواره باشند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شود از جاه ایشان فتنه‌ها دفع</p></div>
<div class="m2"><p>بیابد امتان از جودشان جمع</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو معراج نماز او ضرورت</p></div>
<div class="m2"><p>بدی عالیتر از معراج صورت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز حد و حصر بیرون بد معارج</p></div>
<div class="m2"><p>ندانی تو که تا چون بد معارج</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تو جز معراج ظاهر را ندانی</p></div>
<div class="m2"><p>بباطن چون رسی بیچاره مانی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>شبانروزی بدش هفتاد معراج</p></div>
<div class="m2"><p>بهر معراج قومی گشته محتاج</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بهر معراج قومی را زحق خواست</p></div>
<div class="m2"><p>تمامت کار امت زو شده راست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تو قدر امت احمد ندانی</p></div>
<div class="m2"><p>که پوشیدند از تو این معانی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چه دانی قدر این امت که چونست</p></div>
<div class="m2"><p>که آن از حد وهم تو برون است</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بجهد خویش میکن روز و شب شکر</p></div>
<div class="m2"><p>ترا برهاند ای جان از تعب شکر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تو آن شکرانه کردن کی توانی</p></div>
<div class="m2"><p>مگر در عجز خود را باز دانی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بدین شکرانه جان را در میان نه</p></div>
<div class="m2"><p>بدین نعمت بود جان در میان نه</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>که تو زین امتی پاک و گزیده</p></div>
<div class="m2"><p>همی از بهر رحمت آفریده</p></div></div>