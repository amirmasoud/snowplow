---
title: >-
    بخش ۲۵ - در بیان قوتهای معنی
---
# بخش ۲۵ - در بیان قوتهای معنی

<div class="b" id="bn1"><div class="m1"><p>کند تقریر اهل این معانی</p></div>
<div class="m2"><p>بجز این قوّت و این زندگانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دگرگون قوّت و دیگر حیاتی</p></div>
<div class="m2"><p>که دارد هر وجودی زان ثباتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حیات قوتی از روی معنی</p></div>
<div class="m2"><p>مدد باشد ورا از روی معنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر آن قوت دمی پژمرده گردد</p></div>
<div class="m2"><p>حیات آن وجود افسرده گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درین عالم که خواهد گشت فانی</p></div>
<div class="m2"><p>سه قوت آمد اصل زندگانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدان قوت قوام هر گروهی</p></div>
<div class="m2"><p>بود پاینده زان قوت شکوهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی نفسانی ای جان تا که دانی</p></div>
<div class="m2"><p>دوم روحانی اصل زندگانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سیم ربانی آن کو شد حیقت</p></div>
<div class="m2"><p>بدان زنده شوند اهل طریقت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حیات بیشتر اهل زمانه</p></div>
<div class="m2"><p>بدنیا باشد ای یار یگانه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بجان و دل شوند جویندهٔ او</p></div>
<div class="m2"><p>تو پنداری که هستند بندهٔ او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر آن یک را که شد دنیا ز دستش</p></div>
<div class="m2"><p>تو گوئی پا و سر درهم شکستش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بود از معنی و صورت چو مرده</p></div>
<div class="m2"><p>تمامت خون او گردد فسرده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بود این قوّت نفسانی ای جان</p></div>
<div class="m2"><p>که میدارد ترا پیوسته حیران</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حیات و قوت بعضی از اصحاب</p></div>
<div class="m2"><p>بود ازذکر و طاعت نیک دریاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر یک ورد از ایشان فوت گردد</p></div>
<div class="m2"><p>همان صورت برایشان موت گردد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز خوف دوزخ و ترس جهنم</p></div>
<div class="m2"><p>جگر پرتاب دارد دیده پرنم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همیشه با غم و اندوه باشد</p></div>
<div class="m2"><p>یکایک خالی از اندوه باشد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نعیم خویش را جوینده دایم</p></div>
<div class="m2"><p>درین اندیشه می‌باشند نایم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شود ظاهر ازیشان در مقامات</p></div>
<div class="m2"><p>سخنها در فراسات و کرامات</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگرچه از دل و جان بنده باشند</p></div>
<div class="m2"><p>ببوی عیش عقل زنده باشند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بود روحانی این قوت در ایشان</p></div>
<div class="m2"><p>نباشند هرگز از چیزی پریشان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حیات و قوت اهل طریقت</p></div>
<div class="m2"><p>که آگاهند یک سر از حقیقت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بود دایم همیشه از محبت</p></div>
<div class="m2"><p>نه دوزخ یادشان آید نه جنت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز امید بهشت و خوف آتش</p></div>
<div class="m2"><p>شوند یکباره از اندیشه‌ها خوش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بترک جمله نسبتها بگویند</p></div>
<div class="m2"><p>کرامات وفراست را بجویند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نخواهند از کسی ملکی و مالی</p></div>
<div class="m2"><p>ندارند انتظار کشف حالی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زیارت آنکه ایشان گوش دارند</p></div>
<div class="m2"><p>مراد خویش در آغوش دارند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ببوی وصل جانان زنده باشند</p></div>
<div class="m2"><p>محبت را بجان جوینده باشند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بود ربانی این قوت یکی دان</p></div>
<div class="m2"><p>تو آن قوت حیاتی را متین خوان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بدان قوّت هر آنکو زندگی یافت</p></div>
<div class="m2"><p>فنا یکباره از وی روی برتافت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ببازی برنیاید این چنین کار</p></div>
<div class="m2"><p>ریاضتها کشیدن باید ای یار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بسی تکلیفها بر وی نهادن</p></div>
<div class="m2"><p>عنان خود بدست پیر دادن</p></div></div>