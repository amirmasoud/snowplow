---
title: >-
    بخش ۲۴ - در توحید و در بیان آنکه باب توبه نبندد که موجب ختم ولایت نباشد
---
# بخش ۲۴ - در توحید و در بیان آنکه باب توبه نبندد که موجب ختم ولایت نباشد

<div class="b" id="bn1"><div class="m1"><p>خداوند جهان دانای اکبر</p></div>
<div class="m2"><p>برافزایندهٔ این شمع خاور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بقدرت چون پدید آورد عالم</p></div>
<div class="m2"><p>ز بهر مسکن اولاد آدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بعلم و حکمت خود کرد داور</p></div>
<div class="m2"><p>بقای این جهان چندان مقدر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که ماند انبیا و اولیایش</p></div>
<div class="m2"><p>نیاید بعد از آن دیگر بقایش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یقین میدان که تا باشند ایشان</p></div>
<div class="m2"><p>نخواهد شد کس از محشر پریشان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بهر آدمیزادست گیتی</p></div>
<div class="m2"><p>بایشان دان که آباد است گیتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دو فرقت آدمی را باشد ای جان</p></div>
<div class="m2"><p>بمعنی و بصورت گشته انسان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گروه اولی زان انبیایند</p></div>
<div class="m2"><p>که خاص بارگاه کبریایند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دوم فرقه از ایشان اولیاء دان</p></div>
<div class="m2"><p>بود داخل در ایشان اهل ایمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جز اینها جمله چون انعام باشند</p></div>
<div class="m2"><p>ز معنی غافل و بیکام باشند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه از خود گفته شد این نکته ای جان</p></div>
<div class="m2"><p>ز من گر نشنوی بشنو ز قرآن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بصورت آدمی بسیار باشند</p></div>
<div class="m2"><p>که در محشر سزای نار باشند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بمعنی آدمی می‌بایدت بود</p></div>
<div class="m2"><p>که تا برناورد دوزخ ز تو دود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بود امت نبی را همچو فرزند</p></div>
<div class="m2"><p>بمعنی باشد او را یار و پیوند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کسی باید که او این حال داند</p></div>
<div class="m2"><p>که خواجه امتان را آل خواند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بمعنی هر که از آدم دهد بو</p></div>
<div class="m2"><p>بود فرزند او دلخواه و دلجو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شد از معنی بصورت راه بسیار</p></div>
<div class="m2"><p>بحشر اندر ز معنیها کند کار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز من بشنو تو از روی ارادت</p></div>
<div class="m2"><p>یقین میدان که تا یابی سعادت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که تا مفتوح باشد باب توبه</p></div>
<div class="m2"><p>ولایت را نباشد قطع نوبه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بهر وقتی و هر دور و زمانی</p></div>
<div class="m2"><p>بود صاحب دلی در هر مکانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که باشد آن زمان از وی مشرف</p></div>
<div class="m2"><p>همان جا و مکان ازوی مشرف</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وجود او بلاها می‌کند دفع</p></div>
<div class="m2"><p>بجمله مردمان از وی رسد نفع</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نباشد ختمشان تا روز محشر</p></div>
<div class="m2"><p>که گردد این جهان یکسر مکدر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بصورت تا یکی گردد ز ایشان</p></div>
<div class="m2"><p>نگردد گیتی از محشر پریشان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو ایشان رخت بربندند یکسر</p></div>
<div class="m2"><p>شود پیدا علامت‌های محشر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو بردارند تمامت اولیا را</p></div>
<div class="m2"><p>قیامت کشف گردد آشکارا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کسی کو غیر ازین بیند خیال است</p></div>
<div class="m2"><p>وگر گویند ایشان را وبال است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بدین قول اتفاق اهل دین است</p></div>
<div class="m2"><p>یقین میدان که این گفته چنین است</p></div></div>