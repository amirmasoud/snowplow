---
title: >-
    بخش ۲۸ - در بیان اربعین ثانی
---
# بخش ۲۸ - در بیان اربعین ثانی

<div class="b" id="bn1"><div class="m1"><p>پس آنگه ساز و ترتیب سفر کن</p></div>
<div class="m2"><p>بکلی خویش را از خود بدر کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو اصل کار خود را نیستی دان</p></div>
<div class="m2"><p>که از هستی نیابی ذوق ایمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بساز از جان تو ساز اربعینت</p></div>
<div class="m2"><p>که تا ایزد بود یار و معینت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برآور اربعین ثانی ای یار</p></div>
<div class="m2"><p>تهی از خود شو و فارغ از اغیار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بفکر اندر شده مستغرق وقت</p></div>
<div class="m2"><p>بری گشته ز شکر و کبر و ازمقت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بذکر اندر زبان با دل موافق</p></div>
<div class="m2"><p>بدار ای جان که تا باشی توصادق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مکن ذکری به جز تهلیل جانا</p></div>
<div class="m2"><p>که تهلیست بهتر ذکر دانا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل خود را بجد و جهد میجوی</p></div>
<div class="m2"><p>که تا گاهیت بنماید ترا روی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر روی دل خود بازیابی</p></div>
<div class="m2"><p>تمامت برگ خود را ساز یابی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مگردان قوت خود کمتر ز پنجاه</p></div>
<div class="m2"><p>مباش ایمن ز نقش خویش در راه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بقدر طاقت خود خواب کن دور</p></div>
<div class="m2"><p>ز بیخوابی مشو یکباره رنجور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شب هر جمعهٔ بیدار میباش</p></div>
<div class="m2"><p>بجان و دل تو اندر کار میباش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چنان میکوب این در را بحرمت</p></div>
<div class="m2"><p>که بگشایند و بخشایند جرمت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدین سان اربعینی چون برآری</p></div>
<div class="m2"><p>بدان در ره ز معنی برقراری</p></div></div>