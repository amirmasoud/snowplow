---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>تا چند مرا سوخته خرمن نگری</p></div>
<div class="m2"><p>وز دوستیت به کام دشمن نگری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو ناقد عاشقانی و رویم زر</p></div>
<div class="m2"><p>آخر به زکات چشم در من نگری</p></div></div>