---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>تا چند مرا خوار و خجل خواهی داشت</p></div>
<div class="m2"><p>دیوانه و زنجیر گسل خواهی داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلدار منی بیا ودل با من دار</p></div>
<div class="m2"><p>گر با منِ دلسوخته دل خواهی داشت</p></div></div>