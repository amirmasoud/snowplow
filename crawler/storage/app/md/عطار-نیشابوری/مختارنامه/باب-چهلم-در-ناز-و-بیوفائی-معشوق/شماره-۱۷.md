---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>سهل است اگر کار مرا ساز دهی</p></div>
<div class="m2"><p>گاهم بنوازی و گه آواز دهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون عاشق دل شکسته را دل بردی</p></div>
<div class="m2"><p>چه کم شود ازتو گر دلش باز دهی</p></div></div>