---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>بر خاک چو بادم ای دل افزای هنوز</p></div>
<div class="m2"><p>بر آتش و چشمم آب پالای هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر خاک نشسته بادپیمای هنوز</p></div>
<div class="m2"><p>آبم شد و آتش تو بر جای هنوز</p></div></div>