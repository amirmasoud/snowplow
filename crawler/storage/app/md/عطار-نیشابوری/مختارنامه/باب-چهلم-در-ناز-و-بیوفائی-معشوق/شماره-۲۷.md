---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>تا از غم تب دلش به صد درد افتاد</p></div>
<div class="m2"><p>شد زرد رخ و بر رخ او گرد افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که چه بود کافتابت شد زرد</p></div>
<div class="m2"><p>گفتا مگر آفتاب بر زرد افتاد</p></div></div>