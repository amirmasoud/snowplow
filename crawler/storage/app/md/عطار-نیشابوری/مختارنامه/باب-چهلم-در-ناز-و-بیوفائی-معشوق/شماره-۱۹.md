---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>گفتم که اگر دلِ تو یک رنگ آید</p></div>
<div class="m2"><p>در بر کشیم گرچه ترا ننگ آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی تو که در قبای من کی گنجی</p></div>
<div class="m2"><p>در برکشمت قبای من تنگ آید!</p></div></div>