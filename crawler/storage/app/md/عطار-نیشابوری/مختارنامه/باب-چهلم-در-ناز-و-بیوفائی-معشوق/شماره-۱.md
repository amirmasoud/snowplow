---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>گر خورشیدی چرخ برینت نرسد</p></div>
<div class="m2"><p>ور جمشیدی روی زمینت نرسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی که مرا ناز رسد بر همه کس</p></div>
<div class="m2"><p>تا چند کنی ناز که اینت نرسد</p></div></div>