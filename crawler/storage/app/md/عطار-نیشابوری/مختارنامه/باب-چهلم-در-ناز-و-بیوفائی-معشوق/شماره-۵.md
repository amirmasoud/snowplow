---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>پیوسته به آرزو ترا باید خواست</p></div>
<div class="m2"><p>تا از تو یک آرزو مرا ناید راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کینهٔ من نشستهای پیوسته</p></div>
<div class="m2"><p>زین کینه به جز دلم چه بر خواهد خواست</p></div></div>