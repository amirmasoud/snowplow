---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>تو بیخبری و تا خبر خواهد بود</p></div>
<div class="m2"><p>از جملهٔ عالمت گذر خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برخیز که اینجا که فرو آمدهای</p></div>
<div class="m2"><p>آرامگهِ کسی دگر خواهد بود</p></div></div>