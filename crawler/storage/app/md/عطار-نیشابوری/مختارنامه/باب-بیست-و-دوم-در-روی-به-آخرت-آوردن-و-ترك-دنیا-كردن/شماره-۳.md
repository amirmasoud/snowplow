---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>چون مرگ در افکند به غرقاب ترا</p></div>
<div class="m2"><p>با خاک برد با دل پرتاب ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون گور ز پیش داری ومرگ از پس</p></div>
<div class="m2"><p>چون میآید درین میان خواب ترا</p></div></div>