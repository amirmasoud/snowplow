---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>آن چیست مرا از غم و تیمار که نیست</p></div>
<div class="m2"><p>وز ناکامی اندک و بسیار که نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از جملهٔدخل و خرج این عالم خاک</p></div>
<div class="m2"><p>بادی است مرا در سر و انگار که نیست</p></div></div>