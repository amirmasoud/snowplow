---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>گر عقل تو کامل است کم خور غم خویش</p></div>
<div class="m2"><p>هرکس را عالمی و تو عالم خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کس ماتم تو، چنانکه باید، نکند</p></div>
<div class="m2"><p>بر خود بگری و خود بکن ماتم خویش</p></div></div>