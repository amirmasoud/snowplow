---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>تا چند درِ فتوح جان دربندی</p></div>
<div class="m2"><p>در پیش بُتِ نفس میان دربندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر میخواهی که بر تو بگشاید کار</p></div>
<div class="m2"><p>از نیک و بدِ خلق زبان دربندی</p></div></div>