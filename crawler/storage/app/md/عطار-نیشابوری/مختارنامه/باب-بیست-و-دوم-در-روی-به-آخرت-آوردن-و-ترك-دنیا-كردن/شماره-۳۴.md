---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>گر دیدهوری جمله نکو باید دید</p></div>
<div class="m2"><p>بر باید رفت و پس فرو باید دید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنگر به درختِ سرنگونسار که چیست</p></div>
<div class="m2"><p>یعنی همه شاخِ صنعِّ او باید دید</p></div></div>