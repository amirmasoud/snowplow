---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>در عالم محنت به طرب آمدهیی</p></div>
<div class="m2"><p>در دریائی و خشک لب آمدهیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسوده و آرمیده بودی به عدم</p></div>
<div class="m2"><p>آخر به وجود از چه سبب آمدهیی</p></div></div>