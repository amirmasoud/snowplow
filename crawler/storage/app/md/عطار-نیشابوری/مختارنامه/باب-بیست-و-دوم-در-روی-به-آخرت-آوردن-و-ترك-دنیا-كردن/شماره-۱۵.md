---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>ای دل صفت نفس بد اندیش مگیر</p></div>
<div class="m2"><p>بر جهل، پی صورت ازین بیش مگیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کوتاهی عمر مینگر غرّه مباش</p></div>
<div class="m2"><p>چندین امل دراز در پیش مگیر</p></div></div>