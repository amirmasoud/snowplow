---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>گر مرد رهی، حدیث عالم چه کنی</p></div>
<div class="m2"><p>از جان بگذر زحمت جان هم چه کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای بی معنی! اگر چنان جان بخشی</p></div>
<div class="m2"><p>جان خواست ز تو، این همه ماتم چه کنی</p></div></div>