---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>بر هر وجهی که بستهٔ اسبابی</p></div>
<div class="m2"><p>مرگت کند آگه که کنون در خوابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دستت که ز پیوستن او بیخبری</p></div>
<div class="m2"><p>تا از تو نبرند، خبر کی یابی</p></div></div>