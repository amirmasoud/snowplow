---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>هر دیده که روی در معانی آورد</p></div>
<div class="m2"><p>بیشک ز کمال زندگانی آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر باد مده عمر که هر لحظه ز عمر</p></div>
<div class="m2"><p>صد مُلک به دست میتوانی آورد</p></div></div>