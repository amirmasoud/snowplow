---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>دیدی تو که محنت زده و شاد بمرد</p></div>
<div class="m2"><p>شاگرد به خاک رفت و استاد بمرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن دَم مُردی که زادهای از مادر</p></div>
<div class="m2"><p>این مایه بدان که هر که او زاد بمرد</p></div></div>