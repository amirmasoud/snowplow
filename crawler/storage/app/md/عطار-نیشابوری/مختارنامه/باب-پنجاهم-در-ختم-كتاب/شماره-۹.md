---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>اینک جانم به پیشِ جانان شده‌ام</p></div>
<div class="m2"><p>در پرتوِ او سایهٔ پنهان شده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لب بر لبِ لعلش سخنی می‌گفتم</p></div>
<div class="m2"><p>زانست که در سخن دُرافشان شده‌ام</p></div></div>