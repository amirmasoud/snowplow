---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>از دفترِ عشقم ورقی بنهادم</p></div>
<div class="m2"><p>وز درس وجودم سبقی بنهادم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند که آفتاب در دل دارم</p></div>
<div class="m2"><p>همچون گردون بر طبقی بنهادم</p></div></div>