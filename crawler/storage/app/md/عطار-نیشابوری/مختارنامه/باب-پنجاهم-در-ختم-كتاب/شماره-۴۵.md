---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>ماییم به صد هزار غم رفته به خاک</p></div>
<div class="m2"><p>پیدا شده در جهان و بنهفته به خاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای بس که به خاک من مسکین آیند</p></div>
<div class="m2"><p>گویند که این تویی چنین خفته به خاک</p></div></div>