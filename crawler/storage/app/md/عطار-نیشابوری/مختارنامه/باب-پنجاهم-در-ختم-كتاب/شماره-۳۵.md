---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>بر دل ز هوا اگر چه بند است تُرا</p></div>
<div class="m2"><p>بنیوش سخن که سودمند است تُرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود یک کلمه است جمله پند است تُرا</p></div>
<div class="m2"><p>گر کار کنی یکی، پسند است تُرا</p></div></div>