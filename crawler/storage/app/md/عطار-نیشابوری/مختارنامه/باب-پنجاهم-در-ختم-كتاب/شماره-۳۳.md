---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>تا بود مجال گفت، جان، دُرها سفت</p></div>
<div class="m2"><p>وز گلبن اسرار یقین، گلها رُفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانا! جانم میزند از معنی موج</p></div>
<div class="m2"><p>لیکن چه کنم چو مینیاید در گفت</p></div></div>