---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>یا رب ز خور و خفت چه میباید دید</p></div>
<div class="m2"><p>وز تهمت پذرفت چه میباید دید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسیار بگفتم و نمیداند کس</p></div>
<div class="m2"><p>تا خود پس ازین گفت چه میباید دید</p></div></div>