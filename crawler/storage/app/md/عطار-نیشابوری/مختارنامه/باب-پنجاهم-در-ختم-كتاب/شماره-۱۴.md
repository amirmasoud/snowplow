---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>در فقر دلم عزم سیاهی دارد</p></div>
<div class="m2"><p>قصد صفتی نامتناهی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ظلمت ازان گریخت چون مردم چشم</p></div>
<div class="m2"><p>یعنی که بسی نور الاهی دارد</p></div></div>