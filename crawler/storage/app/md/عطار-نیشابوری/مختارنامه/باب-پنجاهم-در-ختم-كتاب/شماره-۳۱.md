---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>دل میبینم عاشق وآشفته ازو</p></div>
<div class="m2"><p>جان هر نفسی گلی دگر رُفته ازو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکر ایزد را که آنچه در جان من است</p></div>
<div class="m2"><p>در گفت نیاید این همه گفته ازو</p></div></div>