---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>جمشید یقین شدم ز پیدایی خویش</p></div>
<div class="m2"><p>خورشید منور از نکورایی خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گوشهٔ غم با دل سودایی خویش</p></div>
<div class="m2"><p>بردم سبق از جهان به تنهایی خویش</p></div></div>