---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>تا روی چو آفتاب دلدار بتافت</p></div>
<div class="m2"><p>در یک تابش جملهٔ اسرار بتافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم‌:همه کار در عبارت آرم</p></div>
<div class="m2"><p>خود گنگ شدم چو ذرهای کار بتافت</p></div></div>