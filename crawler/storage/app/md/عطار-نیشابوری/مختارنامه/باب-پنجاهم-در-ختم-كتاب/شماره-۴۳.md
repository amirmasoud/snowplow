---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>عالم که امان نداد کس را نفسی</p></div>
<div class="m2"><p>خوابیم نمود در هوا و هوسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای بیخبرانِ خفته! گفتیم بسی</p></div>
<div class="m2"><p>رفتیم که قدر ما ندانست کسی</p></div></div>