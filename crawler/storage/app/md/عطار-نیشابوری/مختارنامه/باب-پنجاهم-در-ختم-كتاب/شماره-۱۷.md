---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>عیسی چو شرابِ لطف در کامم ریخت</p></div>
<div class="m2"><p>بارانِ کمال بر در و بامم ریخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون جان و جهان زخویش کردم خالی</p></div>
<div class="m2"><p>خضر آبِ حیات خواست در جامم ریخت</p></div></div>