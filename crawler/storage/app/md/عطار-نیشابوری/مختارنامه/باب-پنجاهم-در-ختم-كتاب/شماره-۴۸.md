---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>گاهی سخنم به صد جنون بنویسند</p></div>
<div class="m2"><p>گاه از سر عقل ذوفنون بنویسند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر از فضلایند به زر نقش کنند</p></div>
<div class="m2"><p>ور عاشق زارند به خون بنویسند</p></div></div>