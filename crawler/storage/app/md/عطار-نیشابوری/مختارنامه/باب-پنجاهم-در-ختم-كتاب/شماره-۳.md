---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>بحر کرم و گنج وفا در دل ماست</p></div>
<div class="m2"><p>گنجینهٔ تسلیم و رضا در دل ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چرخ فلک چو آسیا میگردد</p></div>
<div class="m2"><p>غم نیست که میخ آسیا در دل ماست</p></div></div>