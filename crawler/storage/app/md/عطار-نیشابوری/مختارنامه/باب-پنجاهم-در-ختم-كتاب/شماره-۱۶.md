---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>که کرد چو بازی مگسی را هرگز</p></div>
<div class="m2"><p>وین عز نبودست خسی را هرگز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن لطف که با ناکس خود میکند او</p></div>
<div class="m2"><p>میبرنتوان گفت کسی را هرگز</p></div></div>