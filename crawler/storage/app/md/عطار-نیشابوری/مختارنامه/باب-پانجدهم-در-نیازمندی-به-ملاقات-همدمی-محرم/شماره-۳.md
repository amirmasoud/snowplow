---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>سرمایهٔ عالم درمی بیش نبود</p></div>
<div class="m2"><p>سر دفترِ هستی عدمی بیش نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با همنفسی گر نفسی دستم داد</p></div>
<div class="m2"><p>زان نیز چه گویم که دمی بیش نبود</p></div></div>