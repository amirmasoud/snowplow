---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>ای آن که به کلّی دل و جان داده نه‌ای</p></div>
<div class="m2"><p>در ره، چو قلم، به فرق استاده نه‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندان که ملامتم کنی باکی نیست</p></div>
<div class="m2"><p>تو معذوری که کار افتاده نه‌ای</p></div></div>