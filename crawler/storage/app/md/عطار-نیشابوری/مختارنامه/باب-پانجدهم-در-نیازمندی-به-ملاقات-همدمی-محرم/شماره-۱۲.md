---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>در پای بلا فتادهام، چتوان کرد</p></div>
<div class="m2"><p>سر رشته ز دست دادهام، چتوان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان روز که زادهام ز مادر بیکس</p></div>
<div class="m2"><p>در گشته به خون بزادهام، چتوان کرد</p></div></div>