---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>چشم من دلخسته به هر انجمنی</p></div>
<div class="m2"><p>چون خویشتنی ندید بیخویشتنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون همنفسی نیافتم در همه عمر</p></div>
<div class="m2"><p>در غصّه بسوختم دریغا چو منی!</p></div></div>