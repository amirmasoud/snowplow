---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>من، توبهٔ عامی، به گناهی نخرم</p></div>
<div class="m2"><p>صد باغ چو خلدش، به گیاهی نخرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این ردّ و قبول خلق و این رسم و رسوم</p></div>
<div class="m2"><p>تاجان دارم، به برگ کاهی نخرم</p></div></div>