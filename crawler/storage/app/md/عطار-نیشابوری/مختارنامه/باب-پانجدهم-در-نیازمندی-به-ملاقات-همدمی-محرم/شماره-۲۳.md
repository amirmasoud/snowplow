---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>هرکو سخنی شنود، یکبار، از من</p></div>
<div class="m2"><p>بنشست به صد هزار تیمار از من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کو مستمعی که بشنود یک ساعت</p></div>
<div class="m2"><p>صد درد دلم بزاری زار از من</p></div></div>