---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>کو مستمعی تا سخنش برگویم</p></div>
<div class="m2"><p>واحوالِ دلِ ممتحنش برگویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیخویشتنی ندیدهام در همه عمر</p></div>
<div class="m2"><p>تا واقعهٔ خویشتنش برگویم</p></div></div>