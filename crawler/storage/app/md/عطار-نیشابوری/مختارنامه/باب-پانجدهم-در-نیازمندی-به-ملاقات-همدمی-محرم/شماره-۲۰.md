---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>با قوّت پیل، مور میباید بود</p></div>
<div class="m2"><p>با ملک دو کون،عور میباید بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وین طرفه نگر که حدّ هر آدمی یی</p></div>
<div class="m2"><p>میباید دید و کور میباید بود</p></div></div>