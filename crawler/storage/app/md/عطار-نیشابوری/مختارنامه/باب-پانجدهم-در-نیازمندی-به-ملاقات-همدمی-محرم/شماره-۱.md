---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>دل خون شد و کس محرم این راز نیافت</p></div>
<div class="m2"><p>در روی زمین هم نفسی باز نیافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پر درد به خاک رفت و در عالم خاک</p></div>
<div class="m2"><p>هم صحبت و هم درد و هم آواز نیافت</p></div></div>