---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>هر دل که نه در زمانه روز افزون شد</p></div>
<div class="m2"><p>نتوان گفتن که حال آن دل چون شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس عقل، که بی پرورش دایهٔ فکر</p></div>
<div class="m2"><p>طفل آمد و طفل از جهان بیرون شد</p></div></div>