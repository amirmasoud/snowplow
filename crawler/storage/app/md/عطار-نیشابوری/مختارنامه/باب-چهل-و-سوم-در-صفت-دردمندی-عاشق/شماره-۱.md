---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>خواهی که ز شغل دو جهان فرد شوی</p></div>
<div class="m2"><p>با اهل صفا همدم و همدرد شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غایب مشو از دردِ دلِ خویش دمی</p></div>
<div class="m2"><p>مستحضرِ درد باش تا مرد شوی</p></div></div>