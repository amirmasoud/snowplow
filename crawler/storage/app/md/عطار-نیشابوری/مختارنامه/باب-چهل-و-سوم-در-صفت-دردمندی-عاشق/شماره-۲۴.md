---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>چون هست غمت غمی دگر حاجت نیست</p></div>
<div class="m2"><p>با خون دلم خون جگر حاجت نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که هزار نوحه گر بنشانم</p></div>
<div class="m2"><p>ماتم زده را به نوحه گر حاجت نیست</p></div></div>