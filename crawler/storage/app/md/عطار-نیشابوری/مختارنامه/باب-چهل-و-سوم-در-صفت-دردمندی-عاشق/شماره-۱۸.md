---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>برخاست دلم چنانکه در غم بنشست</p></div>
<div class="m2"><p>وز شیوهٔ جست و جوی عالم بنشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از درد دلم یکی بگفتم به جهان</p></div>
<div class="m2"><p>ذرات جهان جمله به ماتم بنشست</p></div></div>