---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>دل چون دل من غم زده نتواند بود</p></div>
<div class="m2"><p>صد واقعه بر هم زده نتواند بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا شربت عالم نشود خونابه</p></div>
<div class="m2"><p>قوت من ماتم زده نتواند بود</p></div></div>