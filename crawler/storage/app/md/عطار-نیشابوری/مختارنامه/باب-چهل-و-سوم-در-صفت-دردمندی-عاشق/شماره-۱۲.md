---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>کس را چه خبر ز آهِ دلسوزِ دلم</p></div>
<div class="m2"><p>وز واقعهٔ قیامت افروزِ دلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز چنانم که به فردا نرسم</p></div>
<div class="m2"><p>فردای قیامت است امروزِ دلم</p></div></div>