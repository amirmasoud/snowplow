---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>اندیشهٔ عالمی مرا افتادست</p></div>
<div class="m2"><p>هر جاکه فتد غمی مرا افتادست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون خوش دارم دلت که تا جان دارم</p></div>
<div class="m2"><p>تنها همه ماتمی مرا افتادست</p></div></div>