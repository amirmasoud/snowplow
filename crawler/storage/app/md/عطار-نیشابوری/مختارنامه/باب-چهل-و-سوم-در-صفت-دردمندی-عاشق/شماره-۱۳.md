---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>در عشق، خلاصهٔ جنون از من خواه</p></div>
<div class="m2"><p>جان رفته و عقل سرنگون از من خواه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صدواقعهٔ روزفزون از من خواه</p></div>
<div class="m2"><p>صد بادیه پر آتش و خون از من خواه</p></div></div>