---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>وقتست که بیزحمت جان بنشینم</p></div>
<div class="m2"><p>برخیزم و بی هر دو جهان بنشینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از عالم هست و نیست آزاد آیم</p></div>
<div class="m2"><p>وانگاه برون این و آن بنشینم</p></div></div>