---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>از پس منشین یک دم و در پیش مباش</p></div>
<div class="m2"><p>در بند رضای نفس بد کیش مباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کی گویی که من چه خواهم کردن</p></div>
<div class="m2"><p>تو هرچه کنی به رایت خویش مباش</p></div></div>