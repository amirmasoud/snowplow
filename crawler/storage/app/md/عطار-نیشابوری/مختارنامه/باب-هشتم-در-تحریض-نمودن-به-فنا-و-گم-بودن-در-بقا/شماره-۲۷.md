---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>آنرا که نظر در آن جهان باید کرد</p></div>
<div class="m2"><p>پرواز ورای آسمان باید کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگاه که دولتی بدو آرد روی</p></div>
<div class="m2"><p>در حال ز خویشتن نهان باید کرد</p></div></div>