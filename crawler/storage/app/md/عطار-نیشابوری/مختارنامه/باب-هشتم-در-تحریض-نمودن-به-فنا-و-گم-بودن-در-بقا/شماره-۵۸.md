---
title: >-
    شمارهٔ ۵۸
---
# شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>گفتم: ز فناء خود چنانم که مپرس</p></div>
<div class="m2"><p>گفتا:‌به بقائیت رسانم که مپرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یعنی چو به نیستی بدیدی خود را</p></div>
<div class="m2"><p>چندان هستی بر تو فشانم که مپرس</p></div></div>