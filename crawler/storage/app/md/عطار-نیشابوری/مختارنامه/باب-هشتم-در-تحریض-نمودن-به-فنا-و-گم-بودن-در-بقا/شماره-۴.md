---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>تا نفس کم و کاست نخواهد آمد</p></div>
<div class="m2"><p>یار تو به درخواست نخواهد آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن میباید که تو نباشی اصلاً</p></div>
<div class="m2"><p>کاین کار به تو راست نخواهد آمد</p></div></div>