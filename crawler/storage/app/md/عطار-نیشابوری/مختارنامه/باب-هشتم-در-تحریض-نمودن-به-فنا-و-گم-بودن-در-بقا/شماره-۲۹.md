---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>یا شادی دو کون غم انگار همه</p></div>
<div class="m2"><p>یا ملکِ جهان مسلّم انگار همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهی که وجودِ اصل تابد بر تو</p></div>
<div class="m2"><p>کونین بکلّی عدم انگار همه</p></div></div>