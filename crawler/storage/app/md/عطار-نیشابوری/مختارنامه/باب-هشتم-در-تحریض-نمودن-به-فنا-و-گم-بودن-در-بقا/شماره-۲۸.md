---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>چون نیستی تو محض اقرار بود</p></div>
<div class="m2"><p>هستیت ز سرمایهٔ انکار بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کس که ز نیستی ندارد بوئی</p></div>
<div class="m2"><p>کافر میرد اگرچه دیندار بود</p></div></div>