---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>گر میخواهی که بازیابی این راز</p></div>
<div class="m2"><p>بیخود شو و با بیخودی خویش بساز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بیخودیست اصل هر چیز که هست</p></div>
<div class="m2"><p>تو کی یابی چو در خودی جوئی باز</p></div></div>