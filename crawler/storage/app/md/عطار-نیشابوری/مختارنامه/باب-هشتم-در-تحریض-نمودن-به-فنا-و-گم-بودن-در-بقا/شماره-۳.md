---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>تا هستی تو نصیب میخواهد جست</p></div>
<div class="m2"><p>دل روی به خونِ دیده میخواهد شست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا یک سرِ موی از تو میخواهد ماند</p></div>
<div class="m2"><p>زان یک سرِ موی، کوه میخواهد رست</p></div></div>