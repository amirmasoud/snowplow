---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>خوش خواهدبود، اگر فنا خواهد بود</p></div>
<div class="m2"><p>زیرا که فنا عین بقا خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این میدانم که بس شگرف است فنا</p></div>
<div class="m2"><p>لیکن بندانم که کرا خواهد بود</p></div></div>