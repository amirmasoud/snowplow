---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>دل از طمع خام چنان بریان شد</p></div>
<div class="m2"><p>از آتش شوقی که چنان نتوان شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانی که ز قدر فخر موجوداتست</p></div>
<div class="m2"><p>در راه غم تو با عدم یکسان شد</p></div></div>