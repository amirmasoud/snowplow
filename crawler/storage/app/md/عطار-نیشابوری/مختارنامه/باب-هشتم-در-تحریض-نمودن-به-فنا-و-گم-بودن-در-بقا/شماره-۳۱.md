---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>راهی که درو پای ز سر باید کرد</p></div>
<div class="m2"><p>ره توشه درو خون جگر باید کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهی که ازین راه خبردار شوی</p></div>
<div class="m2"><p>خود را ز دو کون بیخبر باید کرد</p></div></div>