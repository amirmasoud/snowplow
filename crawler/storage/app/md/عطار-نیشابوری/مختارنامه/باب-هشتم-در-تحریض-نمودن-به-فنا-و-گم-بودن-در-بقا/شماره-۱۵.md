---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>دلشاد مشو ز وصل اگر در طربی</p></div>
<div class="m2"><p>دل تنگ مکن ز هجر اگر در تعبی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از شادی وصل و غم هجران بگذر</p></div>
<div class="m2"><p>با هیچ بساز اگر همه میطلبی</p></div></div>