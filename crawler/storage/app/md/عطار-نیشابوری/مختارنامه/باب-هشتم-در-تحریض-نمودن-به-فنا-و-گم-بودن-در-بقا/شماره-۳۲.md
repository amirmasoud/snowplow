---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>آن جوهر پوشیده به هر جان نرسد</p></div>
<div class="m2"><p>دشوار به دست آید و‌آسان نرسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر در ره باز و دست از پای بدار</p></div>
<div class="m2"><p>کاین راه به پای تو به پایان نرسد</p></div></div>