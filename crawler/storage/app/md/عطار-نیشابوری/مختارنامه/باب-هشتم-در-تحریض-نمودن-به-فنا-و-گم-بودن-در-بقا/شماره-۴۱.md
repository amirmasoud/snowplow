---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>تا هیچ وجود و عدمت میماند</p></div>
<div class="m2"><p>نیک و بد و شادی و غمت میماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرده شو و دم مزن که در پردهٔ عشق</p></div>
<div class="m2"><p>همدم نشوی تا که دمت میماند</p></div></div>