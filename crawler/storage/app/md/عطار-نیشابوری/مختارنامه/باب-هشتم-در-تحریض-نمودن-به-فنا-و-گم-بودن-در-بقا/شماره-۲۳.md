---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>آنجا که روی به پا و سر نتوان رفت</p></div>
<div class="m2"><p>ور مرغ شوی به بال و پر نتوان رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از عقل برون آی اگر جان داری</p></div>
<div class="m2"><p>کاین راه به عقل مختصر نتوان رفت</p></div></div>