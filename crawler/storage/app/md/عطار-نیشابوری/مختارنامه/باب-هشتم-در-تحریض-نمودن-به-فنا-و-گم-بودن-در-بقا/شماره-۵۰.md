---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>تا شد دلم از بوی می عشق تو مست</p></div>
<div class="m2"><p>هم پرده دریده گشت و هم توبه شکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز منم هر نفسی دست به دست</p></div>
<div class="m2"><p>از هست به نیست رفته از نیست به هست</p></div></div>