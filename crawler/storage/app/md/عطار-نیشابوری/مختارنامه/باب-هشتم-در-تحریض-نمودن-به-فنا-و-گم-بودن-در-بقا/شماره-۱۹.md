---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>تا چند به خود درنگری چندینی</p></div>
<div class="m2"><p>در هستی خود رنج بری چندینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک ذرّه چو وادید نخواهی آمد</p></div>
<div class="m2"><p>خود را چه دهی جلوهگری چندینی</p></div></div>