---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>ای دل غم جان محنت اندیش ببین</p></div>
<div class="m2"><p>سرگشتگی خواجه و درویش ببین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک ذره چو استغناء او نتوان دید</p></div>
<div class="m2"><p>بی قدری و کم کاستی خویش ببین</p></div></div>