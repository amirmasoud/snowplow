---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>چون هر نفسی ز درد مهجورتری</p></div>
<div class="m2"><p>هر روز درین واقعه معذورتری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نزدیک مشو بدو و زو دور مباش</p></div>
<div class="m2"><p>کانگاه که نزدیکتری دورتری</p></div></div>