---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>گاهی به کمال برتر از خورشیدم</p></div>
<div class="m2"><p>گه در نقصان چو ذرّهای جاویدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگه که به استغناء او مینگرم</p></div>
<div class="m2"><p>بیم است که منقطع شود امیدم</p></div></div>