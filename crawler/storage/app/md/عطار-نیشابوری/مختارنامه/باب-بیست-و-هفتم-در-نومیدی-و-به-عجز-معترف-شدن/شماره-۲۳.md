---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>که گفت ترا که راه اندوهش گیر</p></div>
<div class="m2"><p>یا شیوهٔ عاشقان انبوهش گیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنجا که درو هزار عالم هیچ است</p></div>
<div class="m2"><p>یک ذره کجا رسد تو صد کوهش گیر</p></div></div>