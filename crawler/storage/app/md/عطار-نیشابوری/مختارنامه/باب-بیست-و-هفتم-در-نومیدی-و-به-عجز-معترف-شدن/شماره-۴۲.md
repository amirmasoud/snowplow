---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>در عالم خوف روزگاری دارم</p></div>
<div class="m2"><p>زیرا که امید چون تو یاری دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون من هر دم فرو ترم تو برتر</p></div>
<div class="m2"><p>تادر تو رسم درازکاری دارم</p></div></div>