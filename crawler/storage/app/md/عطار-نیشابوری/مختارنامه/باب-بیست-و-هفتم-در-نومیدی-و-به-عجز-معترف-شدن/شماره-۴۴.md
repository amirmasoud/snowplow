---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>تا زلف تو چون کمند میبینم من</p></div>
<div class="m2"><p>افتاده دلم به بند میبینم من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز نرسد دست به فتراک توام</p></div>
<div class="m2"><p>فتراک تو بس بلند میبینم من</p></div></div>