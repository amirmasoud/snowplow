---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>عمری به هوس نخل معانی بستم</p></div>
<div class="m2"><p>گفتم که مگر ز هر حسابی رستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اکنون لوحی که لوح محفوظم بود</p></div>
<div class="m2"><p>از اشک بشستم و قلم بشکستم</p></div></div>