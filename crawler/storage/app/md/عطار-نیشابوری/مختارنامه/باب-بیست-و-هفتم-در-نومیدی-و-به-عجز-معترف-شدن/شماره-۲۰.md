---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>چندان که دل من به سفر بیش دَرَست</p></div>
<div class="m2"><p>ره نیست، چو او به جوهر خویش دَرَست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس وادی سخت و بس ره صعب که ما</p></div>
<div class="m2"><p>کردیم ز پس هنوز و ره پیش دَرَست</p></div></div>