---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>ای گم شده از جای به صد جای پدید</p></div>
<div class="m2"><p>پیش تو نه جان نه عقل خود رای پدید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی صد ره ز پای رفتم تا سر</p></div>
<div class="m2"><p>لیکن تو نه در سری نه در پای پدید</p></div></div>