---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>ای دل بندی بس استوارت افتاد</p></div>
<div class="m2"><p>ناخورده می عشق، خمارت افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندیشه نمیکنی و درکار شدی</p></div>
<div class="m2"><p>باری بنگر که با که کارت افتاد!</p></div></div>