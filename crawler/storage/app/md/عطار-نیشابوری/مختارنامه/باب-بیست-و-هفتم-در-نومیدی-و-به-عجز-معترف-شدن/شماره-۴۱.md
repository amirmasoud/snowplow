---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>اینجا که منم، پردهٔ پندار بسی است</p></div>
<div class="m2"><p>وانجا که تویی، پردهٔ اسرار بسی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا زین همه پردهها که اندر راه است</p></div>
<div class="m2"><p>یا در تو رسم یا نرسم، کار بسی است</p></div></div>