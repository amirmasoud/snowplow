---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>جانی که به راه رهنمون دارد رای</p></div>
<div class="m2"><p>وز حسرت خود میان خون دارد جای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقلی که شود به جرعهای درد از دست</p></div>
<div class="m2"><p>در معرفت خدای چون دارد پای</p></div></div>