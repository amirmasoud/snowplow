---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>چون دیده سپید شد نظر چند کنیم</p></div>
<div class="m2"><p>چون راه سیه گشت سفر چند کنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زانجا که نشان نیست نشان چند دهیم</p></div>
<div class="m2"><p>وان را که خبر نیست خبر چند کنیم</p></div></div>