---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>آن دل که سراسیمهٔ عالم بودی</p></div>
<div class="m2"><p>یک ذرّه ندید از همه عالم سودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر سودایی که بود بسیار بپخت</p></div>
<div class="m2"><p>حاصل نامد زان همه سودا دودی</p></div></div>