---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>گر در همه عمر در سفر خواهی بود</p></div>
<div class="m2"><p>همچون فلکی زیر و زبر خواهی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند سلوک بیشتر خواهی کرد</p></div>
<div class="m2"><p>هر لحظه ز پس ماندهتر خواهی بود</p></div></div>