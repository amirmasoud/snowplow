---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>تا عشق نشست ناگهی در سر من</p></div>
<div class="m2"><p>برخاست ازین غم دل غم پرور من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز به چه باز آید مرغِ دل من</p></div>
<div class="m2"><p>تا بازآید برین که رفت از بر من</p></div></div>