---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>ماهی که به حسن، عالم آرای افتاد</p></div>
<div class="m2"><p>جان در طلبش شیفته هر جای افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیچاره دلم که دست و پایی میزد</p></div>
<div class="m2"><p>از دست بشد از آن که در پای افتاد</p></div></div>