---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>عمری به هوس در تک و تاز آمد دل</p></div>
<div class="m2"><p>تا محرم راز دلنواز آمد دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس رفت به پیش باز و جان پاک بباخت</p></div>
<div class="m2"><p>انصاف بده که پاکباز آمد دل</p></div></div>