---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>جان سوخته پای بست آمد بی تو</p></div>
<div class="m2"><p>وز دست شده به دست آمد بی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا خیلِ خیال تو شبیخون آورد</p></div>
<div class="m2"><p>بر قلبِ بسی شکست آمد بی تو</p></div></div>