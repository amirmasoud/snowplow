---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>جان گِردِ تو از میان جان میگردد</p></div>
<div class="m2"><p>تن در هوست نعره زنان میگردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وان دل که ز زنجیر سر زلف تو جست</p></div>
<div class="m2"><p>زنجیر گسسته درجهان میگردد</p></div></div>