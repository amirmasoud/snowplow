---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>تاکی بی تو زاری پیوست کنم</p></div>
<div class="m2"><p>جان را ز شراب عشق تو مست کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گاهی خود را نیست و گهی هست کنم</p></div>
<div class="m2"><p>وقت است که در گردن تودست کنم</p></div></div>