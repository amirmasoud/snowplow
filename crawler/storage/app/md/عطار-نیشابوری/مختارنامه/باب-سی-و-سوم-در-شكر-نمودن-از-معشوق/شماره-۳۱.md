---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>در عشق تو اسبِ جان بسر خواهم تاخت</p></div>
<div class="m2"><p>پروانه صفت پای ز پر خواهم ساخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان و تن ودین و دل و ملک دوجهان</p></div>
<div class="m2"><p>در باختم و چیز دگر خواهم باخت</p></div></div>