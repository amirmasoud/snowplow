---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>چندان که دلم سوی تو بشتابد باز</p></div>
<div class="m2"><p>هر دم کاری دگر بر او تابد باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من گم شدهام، تو گم نیی زانکه دلم</p></div>
<div class="m2"><p>در هر چه نگه کند ترا یابد باز</p></div></div>