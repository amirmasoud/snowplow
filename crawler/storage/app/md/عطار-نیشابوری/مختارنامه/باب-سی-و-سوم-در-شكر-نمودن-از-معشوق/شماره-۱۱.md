---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>با جان چه کنم که عشق تو جانم بس</p></div>
<div class="m2"><p>درمان چکنم درد تو درمانم بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عشق تو، صد هزار دردست مرا</p></div>
<div class="m2"><p>یک ذرّه گر افزون کنیم آنم بس</p></div></div>