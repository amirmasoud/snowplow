---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>هر روز مرا با تو حسابی دگرست</p></div>
<div class="m2"><p>هر لحظه ترا تازه عتابی دگرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی یاد تو از خلق دل پُر خونم</p></div>
<div class="m2"><p>هر دم که برآورد حجابی دگرست</p></div></div>