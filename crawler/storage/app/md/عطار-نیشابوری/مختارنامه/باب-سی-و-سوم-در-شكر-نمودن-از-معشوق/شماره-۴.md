---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>گاهی به سخن قوت روانم بخشی</p></div>
<div class="m2"><p>گاهی به سحر راز نهانم بخشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر دل ببری هزار دل باز دهی</p></div>
<div class="m2"><p>ور جان ببری هزار جانم بخشی</p></div></div>