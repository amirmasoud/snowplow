---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>از بس که شدم ز عشق تو دور اندیش</p></div>
<div class="m2"><p>اندیشه ندارم از دو عالم کم و بیش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هر چیزی که بنگرد این دل ریش</p></div>
<div class="m2"><p>آن چیز ز پس بیند و روی تو ز پیش</p></div></div>