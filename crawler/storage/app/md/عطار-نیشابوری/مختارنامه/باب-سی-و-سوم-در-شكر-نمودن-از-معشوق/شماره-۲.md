---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>ای هر نفسی جلوهگری افزونت</p></div>
<div class="m2"><p>گه رد خاکست جلوه، گه در خونت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچون متحیری فرو ماندهام</p></div>
<div class="m2"><p>از لطف حجابهای گوناگونت</p></div></div>