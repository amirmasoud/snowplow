---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>تاکی باشم بستهٔ هستی بی تو</p></div>
<div class="m2"><p>افتادهٔ هشیاری ومستی بی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نالیدم ز تنگدستی بی تو</p></div>
<div class="m2"><p>قارون شدهام به زر پرستی بی تو</p></div></div>