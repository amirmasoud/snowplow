---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>دل را ز غمت بی سر و پا میدارم</p></div>
<div class="m2"><p>وز خلق جهان چشم ترا میدارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در شادی و غم چون به غمم شادی تو</p></div>
<div class="m2"><p>هر غم که به من رسد روا میدارم</p></div></div>