---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>خورشید رخت ملک جهان می‌بخشد</p></div>
<div class="m2"><p>دُرّ سخنت گنج نهان می‌بخشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد جان یابم از غم عشقت هر روز</p></div>
<div class="m2"><p>گویی که غم عشق تو جان می‌بخشد</p></div></div>