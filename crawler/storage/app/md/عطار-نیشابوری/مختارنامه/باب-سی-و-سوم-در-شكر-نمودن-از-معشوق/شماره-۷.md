---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>یک ذرّه ز عشق تو به صحرا آمد</p></div>
<div class="m2"><p>تا این همه گفت و گوی پیدا آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان نعره زنان در بن دریا افتاد</p></div>
<div class="m2"><p>دل رقص کنان با سر غوغا آمد</p></div></div>