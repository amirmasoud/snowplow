---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>جانم، ز میان جان، وفای تو کند</p></div>
<div class="m2"><p>دل ترک دو عالم از برای تو کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر تارک خورشید نهد پای از قدر</p></div>
<div class="m2"><p>هرذره که لحظهای هوای تو کند</p></div></div>