---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>گر ماه نه زیر میغ میداشتیی</p></div>
<div class="m2"><p>بس سر که بر تو تیغ میداشتیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در درد و دریغ جاودان ماندی دل</p></div>
<div class="m2"><p>گر درد ز دل دریغ میداشتیی</p></div></div>