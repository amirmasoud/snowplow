---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>سرگشتهٔ تست، نُه فلک، میدانی</p></div>
<div class="m2"><p>گرد در تو گشته به سرگردانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو خورشیدی ولی میان جانی</p></div>
<div class="m2"><p>خورشید که دیدهست بدین پنهانی</p></div></div>