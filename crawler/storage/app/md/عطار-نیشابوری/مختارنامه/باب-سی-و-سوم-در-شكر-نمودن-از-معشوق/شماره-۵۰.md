---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>جانا! همه راه، بر زبانم بودی</p></div>
<div class="m2"><p>در هر منزل مژده رسانم بودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای جان و دلم! گر ز تو غایب گشتم</p></div>
<div class="m2"><p>هر جا که بُدم در دل و جانم بودی</p></div></div>