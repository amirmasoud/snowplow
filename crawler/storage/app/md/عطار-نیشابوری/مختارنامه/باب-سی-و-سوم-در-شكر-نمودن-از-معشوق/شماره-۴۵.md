---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>سر در سر سودای تو خواهم کردن</p></div>
<div class="m2"><p>در حجرهٔ دل جای تو خواهم کردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برگیر ز رخ پرده که در عالم جان</p></div>
<div class="m2"><p>دل غرق تماشای تو خواهم کردن</p></div></div>