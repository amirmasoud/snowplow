---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>چون نیست ره هجرِ ترا پایان باز</p></div>
<div class="m2"><p>پس چون بگشایم گرهِ هجران باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کی باشم فتاده از جانان باز</p></div>
<div class="m2"><p>چون کودک شیرخواره از پستان باز</p></div></div>