---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>گر بشتابم نه روی بشتافتن است</p></div>
<div class="m2"><p>ور سر یابم نه گنج سر یافتن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز حسرت و خون دل چه بر خواهد خاست</p></div>
<div class="m2"><p>زین یافتنی که عین نایافتن است</p></div></div>