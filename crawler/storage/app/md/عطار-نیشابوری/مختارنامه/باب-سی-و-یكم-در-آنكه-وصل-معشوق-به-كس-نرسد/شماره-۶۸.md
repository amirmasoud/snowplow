---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>ای جمله اشارات و رموزم از تو</p></div>
<div class="m2"><p>پیوسته یجوز و لایجوزم از تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگداخته چون برف تموزم از تو</p></div>
<div class="m2"><p>صد گونه حجاب است هنوزم از تو</p></div></div>