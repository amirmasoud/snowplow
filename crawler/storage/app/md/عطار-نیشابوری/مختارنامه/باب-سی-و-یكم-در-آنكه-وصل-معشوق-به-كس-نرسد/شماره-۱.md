---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>چندین در بسته بی کلیدست چه سود</p></div>
<div class="m2"><p>کس نام گشادن نشنیدست چه سود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیراهن یوسف است یک یک ذرّه</p></div>
<div class="m2"><p>یوسف ز میانه ناپدیدست چه سود</p></div></div>