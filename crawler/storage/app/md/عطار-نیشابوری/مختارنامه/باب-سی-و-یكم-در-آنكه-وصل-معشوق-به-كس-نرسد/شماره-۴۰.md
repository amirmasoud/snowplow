---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>درداکه قرار از دل سرمستم رفت</p></div>
<div class="m2"><p>خون شد دلم و امید پیوستم رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر بوی وصال او نشستم عمری</p></div>
<div class="m2"><p>او دست نداد و جمله از دستم رفت</p></div></div>