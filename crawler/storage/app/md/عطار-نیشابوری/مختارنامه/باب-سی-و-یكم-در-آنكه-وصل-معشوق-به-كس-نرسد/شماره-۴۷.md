---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>جانا رخ چون تویی به حس نتوان دید</p></div>
<div class="m2"><p>زر چون بینم به حس که مس نتوان دید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وصل تو به دو دست تهی نتوان یافت</p></div>
<div class="m2"><p>روی تو به دو چشم نجس نتوان دید</p></div></div>