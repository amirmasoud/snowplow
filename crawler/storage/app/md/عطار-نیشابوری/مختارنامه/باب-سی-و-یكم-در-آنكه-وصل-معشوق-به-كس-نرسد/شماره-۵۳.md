---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>هرگه که من از وصل تو بابی شنوم</p></div>
<div class="m2"><p>شب خوش بادم که یاد خوابی شنوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو گنگ شوم با تو حدیثی گویم</p></div>
<div class="m2"><p>چون کر گردم از تو جوابی شنوم</p></div></div>