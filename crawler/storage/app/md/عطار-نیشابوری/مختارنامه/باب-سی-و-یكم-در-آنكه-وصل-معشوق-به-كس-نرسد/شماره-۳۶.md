---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>چون نیست دلم را جز ازو دلجویی</p></div>
<div class="m2"><p>سرگشته شدم گردِ جهان چون گویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه غصّه بدین رسد که از ملکِ دو کون</p></div>
<div class="m2"><p>او رادارم وزو ندارم بویی</p></div></div>