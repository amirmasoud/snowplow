---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>نادیده ترا شرح سروپات خوش است</p></div>
<div class="m2"><p>گر سود کنیم و گرنه، سودات خوش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را همه وقت خوشی تست مراد</p></div>
<div class="m2"><p>پس بی تو بمیریم چو بی مات خوش است</p></div></div>