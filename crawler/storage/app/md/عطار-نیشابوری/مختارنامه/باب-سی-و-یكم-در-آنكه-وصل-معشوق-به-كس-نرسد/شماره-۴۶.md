---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>هم عقل طلسم جسم و جان باز نیافت</p></div>
<div class="m2"><p>هم گنج زمین و آسمان باز نیافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید هزار قرن بر پهلو گشت</p></div>
<div class="m2"><p>یک ذرّه سراپای جهان باز نیافت</p></div></div>