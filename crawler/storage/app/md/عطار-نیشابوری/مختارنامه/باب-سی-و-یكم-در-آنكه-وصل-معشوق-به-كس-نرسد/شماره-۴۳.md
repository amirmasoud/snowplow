---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>چون وصل نیامد به کسی اولیتر</p></div>
<div class="m2"><p>بی همنفسی هر نفسی اولیتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نیست به وصل او رسیدن ممکن</p></div>
<div class="m2"><p>در هجر گریختن بسی اولیتر</p></div></div>