---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>گنجت باید به رنج خو باید کرد</p></div>
<div class="m2"><p>جان وقف بلای عشق او باید کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در پنجهٔ شیر اوفتادن به ازانک</p></div>
<div class="m2"><p>با او نفسی پنجه فرو باید کرد</p></div></div>