---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>عقلی که کمال در جنون میبیند</p></div>
<div class="m2"><p>بنیاد وجود خاک و خون میبیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمی که دو کون در درون میبیند</p></div>
<div class="m2"><p>مشتی رگ و استخوان برون میبیند</p></div></div>