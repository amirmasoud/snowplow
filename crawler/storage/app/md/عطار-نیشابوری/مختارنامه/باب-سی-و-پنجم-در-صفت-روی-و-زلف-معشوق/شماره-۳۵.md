---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>شرطِ رَهِ عشق چیست، درخون گشتن</p></div>
<div class="m2"><p>همچون شمعی به فرق بیرون گشتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از مشعلهٔ روی تو دلگرم شدن</p></div>
<div class="m2"><p>وز سلسلهٔ زلفِ تو مجنون گشتن</p></div></div>