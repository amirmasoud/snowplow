---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>شب نیست که جان بی تو به لب مینرسد</p></div>
<div class="m2"><p>روزی نه که در غصّه به شب مینرسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلفِ تو چنین دراز و من در عجبم</p></div>
<div class="m2"><p>تا دست بدو از چه سبب مینرسد</p></div></div>