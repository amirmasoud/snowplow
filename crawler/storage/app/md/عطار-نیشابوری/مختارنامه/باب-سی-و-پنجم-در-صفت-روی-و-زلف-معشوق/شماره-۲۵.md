---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>تا روی چو آفتاب جانان بفروخت</p></div>
<div class="m2"><p>ازحسن جهان بر مه تابان بفروخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از رشک رخت کمال بسیار خرید</p></div>
<div class="m2"><p>تا بفروزد جمله به نقصان بفروخت</p></div></div>