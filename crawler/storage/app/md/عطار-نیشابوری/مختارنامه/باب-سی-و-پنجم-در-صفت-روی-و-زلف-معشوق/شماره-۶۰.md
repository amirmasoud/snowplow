---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>چون چشمِ تو تیرِ غمزه محکم انداخت</p></div>
<div class="m2"><p>هر لحظه هزار صید بر هم انداخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون زلف تو سر بستگی آغاز نهاد</p></div>
<div class="m2"><p>سرگشتگیی در همه عالم انداخت</p></div></div>