---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>بگشاده رخ و بسته قبا میآید</p></div>
<div class="m2"><p>سرمست به بازار چرا میآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میآید و در پوست چو گل میخندد</p></div>
<div class="m2"><p>آری چه توان کرد مرا میآید</p></div></div>