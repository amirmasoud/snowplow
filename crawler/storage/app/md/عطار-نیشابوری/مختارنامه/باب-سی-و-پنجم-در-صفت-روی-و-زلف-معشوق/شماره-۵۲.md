---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>بوئی که ز زلف مشکبوی تو رسد</p></div>
<div class="m2"><p>دل در طلبش بر سر کوی تو رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن زلف سیاهِ تو بلایی سیه است</p></div>
<div class="m2"><p>ترسم که نیاید که به روی تو رسد</p></div></div>