---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>گر لعل لب تو آب حیوانم داد</p></div>
<div class="m2"><p>ور چشم خوش تو قوت جانم داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلف تو به دست سخت میخواهم داشت</p></div>
<div class="m2"><p>من این شیوه ز دست نتوانم داد</p></div></div>