---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>ای بیخبر از رنج و گرفتاری من</p></div>
<div class="m2"><p>شادم که تو خوشدلی به غمخواری من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا غمزه به خونِ دلِ من بگشادی</p></div>
<div class="m2"><p>در زلف تو بسته است نگونساری من</p></div></div>