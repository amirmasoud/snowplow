---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>تا روی ز زیرِ پرده بنمودی تو</p></div>
<div class="m2"><p>صد پرده دریدی و ببخشودی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز همه جهان ز تو پُر شور است</p></div>
<div class="m2"><p>زین پیش که داند که کجا بودی تو</p></div></div>