---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>ای تُرک! دلم غاشیه بر دوش تو شد</p></div>
<div class="m2"><p>جانم ز جهان واله و مدهوش تو شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سیمِ بناگوش تو چون جملهٔ خلق،</p></div>
<div class="m2"><p>در مینگرند، حلقه در گوش تو شد</p></div></div>