---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>ای پیش تو سرو و ماه پیوسته رهی</p></div>
<div class="m2"><p>با قد چو سرو و با رخ همچو مهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مه چهره و سرو قد بسی هست ولیک</p></div>
<div class="m2"><p>تابندهتر است ماه بر سروِ سهی</p></div></div>