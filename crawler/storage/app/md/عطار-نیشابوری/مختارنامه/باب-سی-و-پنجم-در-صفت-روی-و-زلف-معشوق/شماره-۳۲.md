---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>بی لعلِ لبش شکرستان میچکنم</p></div>
<div class="m2"><p>بی ماهِ رخش زحمتِ جان میچکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویند: «جهان بر رخِ او باید دید»</p></div>
<div class="m2"><p>گر پیش آید رخش جهان میچکنم</p></div></div>