---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>ای باد به سوی زلفِ آن یار بتاز</p></div>
<div class="m2"><p>کوتاه مکن دست از آن زلف دراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آهم به سرِ زلفِ درازش برسان</p></div>
<div class="m2"><p>بوی جگر سوخته در مشک انداز</p></div></div>