---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>زلف تو که بود آرزوئی همه را</p></div>
<div class="m2"><p>جز دیدن او نبود روئی همه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>موئی ز سرِ یک شکنش برکندم</p></div>
<div class="m2"><p>کآویخته بود دل به موئی همه را</p></div></div>