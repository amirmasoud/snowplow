---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>گفتی که اگر میطلبی تدبیری</p></div>
<div class="m2"><p>هرچت باید بخواه بیتأخیری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلفت خواهم ازانکه در میباید</p></div>
<div class="m2"><p>دیوانگی مرا چنان زنجیری</p></div></div>