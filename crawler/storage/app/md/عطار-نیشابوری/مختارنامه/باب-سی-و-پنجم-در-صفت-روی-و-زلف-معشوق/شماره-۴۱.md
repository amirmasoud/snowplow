---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>زلف تو برفت از نظرم چه توان کرد</p></div>
<div class="m2"><p>برد این دل زیر و زبرم چه توان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر من کمری ز زلف تو بربندم</p></div>
<div class="m2"><p>زنّار بود آن کمرم چه توان کرد</p></div></div>