---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>دل دادم و ترکِ کفر ودینش کردم</p></div>
<div class="m2"><p>گمراهی و مفلسی یقینش کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نامِ تو نقشِ دلِ من بود مدام</p></div>
<div class="m2"><p>در حلقهٔ زلفِ تو نگینش کردم</p></div></div>