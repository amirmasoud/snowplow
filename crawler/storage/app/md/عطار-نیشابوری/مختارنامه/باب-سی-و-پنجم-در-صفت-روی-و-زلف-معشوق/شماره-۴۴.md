---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>دل در خم آن زلف چو زنجیر بماند</p></div>
<div class="m2"><p>سر بر خط تو دو پای در قیر بماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشکِ سرِ زلفِ تو دلِ ما بربود</p></div>
<div class="m2"><p>ما را، جگرِ سوخته، توفیر بماند</p></div></div>