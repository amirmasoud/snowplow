---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>تا در سر زلفت خم و چین افکندی</p></div>
<div class="m2"><p>بر ماه نقاب عنبرین افکندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با تو سخنی ز زلف تو میگفتم</p></div>
<div class="m2"><p>در خشم شدی و بر زمین افکندی</p></div></div>