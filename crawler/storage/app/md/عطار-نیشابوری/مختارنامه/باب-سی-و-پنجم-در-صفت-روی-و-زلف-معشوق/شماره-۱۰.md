---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>ای روی چو آفتابِ تو پشت سیاه</p></div>
<div class="m2"><p>بی پشتی تو مه ننهد روی به راه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از روی توآفتاب را پشت شکست</p></div>
<div class="m2"><p>وز روی تو پشتِ دست میخاید ماه</p></div></div>