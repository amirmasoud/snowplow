---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>چون گشت دل من از سر زلف تو مست</p></div>
<div class="m2"><p>هرگز بندادم ز سر زلف تو دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی سر زلف من کرا خواهد بود</p></div>
<div class="m2"><p>دانی که سر زلف تو دارم پیوست</p></div></div>