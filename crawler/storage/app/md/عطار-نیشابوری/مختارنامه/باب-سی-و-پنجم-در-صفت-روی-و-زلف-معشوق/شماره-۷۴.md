---
title: >-
    شمارهٔ ۷۴
---
# شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>بیچاره دلِ من که غمِ جانش نیست</p></div>
<div class="m2"><p>در درد بسوخت و هیچ درمانش نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که سرِ زلفِ تو دستش گیرد</p></div>
<div class="m2"><p>در پای فکندی که سر آنش نیست</p></div></div>