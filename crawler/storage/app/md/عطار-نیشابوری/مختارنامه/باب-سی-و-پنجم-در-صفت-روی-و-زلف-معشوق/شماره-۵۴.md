---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>در عشق رخت چون رخ تو بیشم نیست</p></div>
<div class="m2"><p>قربان تو گردم که جز این کیشم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بردی دل من به زلف و بندش کردی</p></div>
<div class="m2"><p>زانست که یک لحظه دل خویشم نیست</p></div></div>