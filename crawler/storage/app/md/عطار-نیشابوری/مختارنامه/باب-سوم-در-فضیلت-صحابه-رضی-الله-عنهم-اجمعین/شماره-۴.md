---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>صدری که گلِ طارمِ معنی او رُفت</p></div>
<div class="m2"><p>دُرِّ صدفِ قُلزمِ تقوی او سفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بودند دو کون سائلان درِ او</p></div>
<div class="m2"><p>و او بود که از جمله سَلُونی او گفت</p></div></div>