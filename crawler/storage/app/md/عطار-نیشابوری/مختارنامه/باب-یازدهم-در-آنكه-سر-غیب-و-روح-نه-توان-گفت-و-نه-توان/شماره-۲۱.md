---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>رازی که دل من است سرگشتهٔ آن</p></div>
<div class="m2"><p>وز خون دو دیده گشتم آغشتهٔ آن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کی به سر سوزن فکرت کاوم</p></div>
<div class="m2"><p>سِرّی که کسی نیافت سَرْ رشتهٔ آن</p></div></div>