---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>این درد جگرسوز که در سینه مراست</p></div>
<div class="m2"><p>میگرداند گِرِد جهانم چپ و راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمریست که میروم به تاریکی در</p></div>
<div class="m2"><p>و آگاه نیم که چشمهٔ خضر کجاست</p></div></div>