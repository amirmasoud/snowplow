---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>میپنداری که جان توانی دیدن</p></div>
<div class="m2"><p>اسرار همه جهان توانی دیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگاه که بینشِ تو گردد به کمال</p></div>
<div class="m2"><p>کوری خود آن زمان توانی دیدن</p></div></div>