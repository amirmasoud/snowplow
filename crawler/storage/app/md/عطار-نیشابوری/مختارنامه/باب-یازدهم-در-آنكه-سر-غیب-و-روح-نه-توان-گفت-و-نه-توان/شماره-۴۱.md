---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>جز درد تو درمان دل ریشم نیست</p></div>
<div class="m2"><p>جز آینهٔ شوق تو در پیشم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کس چیزی میطلبد، از تو مرا،</p></div>
<div class="m2"><p>چون از تو خبر شد، خبر از خویشم نیست</p></div></div>