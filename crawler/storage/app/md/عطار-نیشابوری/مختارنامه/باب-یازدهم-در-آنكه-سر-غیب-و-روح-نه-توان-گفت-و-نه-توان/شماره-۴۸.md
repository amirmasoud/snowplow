---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>عمری دل این سوخته تن در خون داد</p></div>
<div class="m2"><p>و او هر نفسم وعدهٔ دیگرگون داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون پرده برانداخت نمود آنچه نمود</p></div>
<div class="m2"><p>ببرید زبانم و سرم بیرون داد</p></div></div>