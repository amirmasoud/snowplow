---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>دل رفت و نگفت دلستانم که چه بود</p></div>
<div class="m2"><p>جان شد که خبر نداد جانم که چه بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سِرِّ دل و جان من مرا برگفتند</p></div>
<div class="m2"><p>نه خفته نه بیدار ندانم که چه بود</p></div></div>