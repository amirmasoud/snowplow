---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>آنها که درین درد مرا میبینند</p></div>
<div class="m2"><p>در درد و دریغای منِ مسکینند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون یک سر موی از تو خبر نیست رواست</p></div>
<div class="m2"><p>گر هر موئی به ماتمی بنشینند</p></div></div>