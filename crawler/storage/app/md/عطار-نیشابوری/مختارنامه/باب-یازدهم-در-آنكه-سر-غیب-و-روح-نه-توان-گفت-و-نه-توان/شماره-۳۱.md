---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>در حیرانی بنده وآزاد هنوز</p></div>
<div class="m2"><p>با خاک همی شوند ناشاد هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنگر تو که چرخ صد هزاران سال است</p></div>
<div class="m2"><p>کاین حلقه زد و دَرَشْ بنگشاد هنوز</p></div></div>