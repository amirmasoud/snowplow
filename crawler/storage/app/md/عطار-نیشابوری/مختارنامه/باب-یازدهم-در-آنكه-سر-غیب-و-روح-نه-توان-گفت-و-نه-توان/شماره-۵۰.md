---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>جانی که به رمز، قصّهٔ جانان گفت</p></div>
<div class="m2"><p>ببرید زبان و بیزبان پنهان گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کی گویی: «واقعهٔ عشق بگوی!»</p></div>
<div class="m2"><p>چیزی که چشیدنی بود نتوان گفت</p></div></div>