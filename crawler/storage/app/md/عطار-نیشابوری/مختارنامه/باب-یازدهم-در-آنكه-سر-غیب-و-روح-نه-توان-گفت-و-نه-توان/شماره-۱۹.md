---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>این دل که بسوخت روز و شب در تک و تاز</p></div>
<div class="m2"><p>میجوشد و میجوید و میگوید راز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندان که بدین پرده فرو داد آواز</p></div>
<div class="m2"><p>دردا که کسش جواب مینَدْهَد باز</p></div></div>