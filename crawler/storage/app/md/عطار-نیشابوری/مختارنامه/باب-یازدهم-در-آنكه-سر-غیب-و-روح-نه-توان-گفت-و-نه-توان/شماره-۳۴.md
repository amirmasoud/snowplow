---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>جانا! ز غم عشق تو فریاد مرا</p></div>
<div class="m2"><p>کز عشق تو جز دریغ نگشاد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر ذرّه اگر گره گشایی گردد</p></div>
<div class="m2"><p>حل کی شود این واقعه کافتاد مرا</p></div></div>