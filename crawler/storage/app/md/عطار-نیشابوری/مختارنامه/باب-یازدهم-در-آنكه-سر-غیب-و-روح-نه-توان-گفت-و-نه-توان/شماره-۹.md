---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>تا عالِمِ جهل خود نگردی به نخست</p></div>
<div class="m2"><p>هر اصل که در علم نهی نیست درست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای بس که دلم دست به خونابه بشست</p></div>
<div class="m2"><p>در حسرت نایافت و نیافت آنچه بجست</p></div></div>