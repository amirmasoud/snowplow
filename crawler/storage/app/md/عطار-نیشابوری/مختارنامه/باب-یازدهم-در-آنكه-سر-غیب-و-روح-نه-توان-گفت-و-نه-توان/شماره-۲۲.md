---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>شد رنجِ دلم فَرِهْ چه تدبیر کنم</p></div>
<div class="m2"><p>بگسست مرا زِرِهْ چه تدبیر کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دردا که به صد هزار انگشت حیل</p></div>
<div class="m2"><p>مینگشاید گِرِهْ چه تدبیر کنم</p></div></div>