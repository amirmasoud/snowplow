---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>دل خون شد و سررشتهٔ این راز نیافت</p></div>
<div class="m2"><p>جز غصه ز انجام وز آغاز نیافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغ دل من ز آشیان دور افتاد</p></div>
<div class="m2"><p>ای بس که طپید و آشیان باز نیافت</p></div></div>