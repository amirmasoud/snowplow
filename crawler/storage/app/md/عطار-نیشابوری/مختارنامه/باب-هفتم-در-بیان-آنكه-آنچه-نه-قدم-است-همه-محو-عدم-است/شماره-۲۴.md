---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>چون هستی را نیست کسی اولیتر</p></div>
<div class="m2"><p>بازی که توداری مگسی اولیتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان نیست همی شوند هستان، که همه</p></div>
<div class="m2"><p>هستند به نیستی بسی اولیتر</p></div></div>