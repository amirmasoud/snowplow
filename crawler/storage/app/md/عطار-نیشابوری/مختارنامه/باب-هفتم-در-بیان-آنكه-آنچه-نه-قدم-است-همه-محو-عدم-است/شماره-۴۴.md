---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>هیچم من و در گفت و شنید آمدهام</p></div>
<div class="m2"><p>در نیست پدید و بیکلید آمدهام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این نیست عجب که گم بخواهم بودن</p></div>
<div class="m2"><p>اینست عجب که چون پدید آمدهام</p></div></div>