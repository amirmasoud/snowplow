---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>چون محرم هم نفس نهای، تو چه کنی</p></div>
<div class="m2"><p>شایستهٔ این هوس نهای، تو چه کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیوسته به جنگ خویش برخاستهای</p></div>
<div class="m2"><p>خود را،‌چو تو هیچ کس نهای، تو چه کنی</p></div></div>