---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>شایستهٔ این هوس نهای، تو چه کنی</p></div>
<div class="m2"><p>در بیقدری چون مگسی باشی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود را،‌چو تو هیچ کس نهای، تو چه کنی</p></div>
<div class="m2"><p>آخر تو که باشی که کسی باشی تو</p></div></div>