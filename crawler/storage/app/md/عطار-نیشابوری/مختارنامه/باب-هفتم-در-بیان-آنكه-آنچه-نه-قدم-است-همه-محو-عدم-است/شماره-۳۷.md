---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>از بس که در آثار نمیبینم من</p></div>
<div class="m2"><p>جز پردهٔ پندار، نمیبینم من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بس که به قعر نیستی در رفتم</p></div>
<div class="m2"><p>گم گشتم و دیار نمیبینم من</p></div></div>