---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>جان شیفتهٔ الست می‌پنداری</p></div>
<div class="m2"><p>و اندیشهٔ ما بهانه‌ای بیش نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنست که خویش، هست می‌پنداری</p></div>
<div class="m2"><p>قصّه چه کنم، نشانه‌ای بیش نبود</p></div></div>