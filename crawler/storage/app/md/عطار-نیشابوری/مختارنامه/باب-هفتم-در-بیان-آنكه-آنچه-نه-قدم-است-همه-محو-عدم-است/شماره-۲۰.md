---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>جمشید به گلخنی در افتاد و برفت</p></div>
<div class="m2"><p>در فرع کجا مشبّهی افتاده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید به روزنی در افتاد و برفت</p></div>
<div class="m2"><p>چون در نگری حقّه تهی افتاده است</p></div></div>