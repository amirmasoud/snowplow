---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>در فرع کجا مشبّهی افتاده است</p></div>
<div class="m2"><p>افلاک ز یکدگر فرو آسایند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون در نگری حقّه تهی افتاده است</p></div>
<div class="m2"><p>یک ره همه از سفر فروآسایند</p></div></div>