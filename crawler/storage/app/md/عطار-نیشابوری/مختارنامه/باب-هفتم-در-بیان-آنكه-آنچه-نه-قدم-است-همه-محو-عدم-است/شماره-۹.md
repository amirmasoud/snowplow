---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>بگذر ز حس و خیال،‌ای طالب حال</p></div>
<div class="m2"><p>تا هر دو جهان جلال بینی و جمال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیرا که تو هرچه در جهان میبینی</p></div>
<div class="m2"><p>جز وجه بقا همه سرابست و خیال</p></div></div>