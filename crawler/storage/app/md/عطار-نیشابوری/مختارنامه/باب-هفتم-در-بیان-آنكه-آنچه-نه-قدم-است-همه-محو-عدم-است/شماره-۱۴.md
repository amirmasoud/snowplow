---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>هیچ است همه، وسوسهٔ خاطر چند</p></div>
<div class="m2"><p>از هیچ بلا، چند شود ظاهر چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو هیچ بُدی و هیچ خواهی گشتن</p></div>
<div class="m2"><p>بر هیچ میانِ این دو هیچ آخر چند</p></div></div>