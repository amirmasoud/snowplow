---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>ای پردهٔ پندار پسندیدهٔ تو</p></div>
<div class="m2"><p>وی وهم خودی در دل شوریدهٔ تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچی تو و هیچ را چنین میگویی</p></div>
<div class="m2"><p>به زین نتوان نهاد در دیدهٔ تو</p></div></div>