---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>درویشی چیست مست و مفلس بودن</p></div>
<div class="m2"><p>بیخود خود را ز خویش مونس بودن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انگشت به لب باز نهادن جاوید</p></div>
<div class="m2"><p>همچون ناخن زنده و بیحس بودن</p></div></div>