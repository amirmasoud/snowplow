---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>درها به فنا گشادهاند، اینت عجب!</p></div>
<div class="m2"><p>بر هیچ قرار دادهاند، اینت عجب!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پنداشت که مانهایم و پندار وجود</p></div>
<div class="m2"><p>در دیدهٔ ما نهادهاند، اینت عجب!</p></div></div>