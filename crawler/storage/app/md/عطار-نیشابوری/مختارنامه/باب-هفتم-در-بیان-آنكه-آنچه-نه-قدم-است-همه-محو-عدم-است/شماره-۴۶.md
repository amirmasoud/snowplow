---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>ای دل! دیدی که هرچه دیدی هیچ است</p></div>
<div class="m2"><p>هر قصّهٔ دوران که شنیدی هیچ است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندین که ز هر سوی دویدی هیچ است</p></div>
<div class="m2"><p>و امروز که گوشهای گزیدی هیچ است</p></div></div>