---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>چشمت که سبق به دلربائی او راست</p></div>
<div class="m2"><p>در خون ریزی کام روائی او راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر جان خواهد رواست زیرا که لبت</p></div>
<div class="m2"><p>صد جان دهدم که جان فزائی او راست</p></div></div>