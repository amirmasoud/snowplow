---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>بر شاخِ دل شکسته یک برگم نیست</p></div>
<div class="m2"><p>کز بی برگی بتر ز صد مرگم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی دانه چگونه برگ باشد آخر</p></div>
<div class="m2"><p>بی دانهٔ نارِ لبِ تو برگم نیست</p></div></div>