---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>گفتم: «شکریم ده مسلمانی نیست»</p></div>
<div class="m2"><p>گفتا: «جان ده که نرخ پنهانی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک بوسه به جانیست مرا، گو بمخر</p></div>
<div class="m2"><p>آن را که بدین گرانی ارزانی نیست»</p></div></div>