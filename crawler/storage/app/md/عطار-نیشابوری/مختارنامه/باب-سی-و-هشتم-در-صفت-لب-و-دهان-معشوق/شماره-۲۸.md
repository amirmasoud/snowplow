---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>دل، مست بتی عهدشکن دارم من</p></div>
<div class="m2"><p>با او به یکی بوسه سخن دارم من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم: «شکری» گفت که تعجیل مکن</p></div>
<div class="m2"><p>بشنو سخنی که در دهن دارم من</p></div></div>