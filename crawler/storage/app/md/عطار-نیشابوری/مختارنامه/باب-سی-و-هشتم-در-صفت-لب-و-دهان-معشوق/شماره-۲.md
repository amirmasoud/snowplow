---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>چون دیده به روی تو نظر بگشاید</p></div>
<div class="m2"><p>از هر مژهای خون جگر بگشاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در صد گره‌ام ز زلف خم در خمِ تو</p></div>
<div class="m2"><p>تا پسته به یک تنگ شکر بگشاید</p></div></div>