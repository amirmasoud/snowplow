---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>زلفِ تو سرِ درازدستی دارد</p></div>
<div class="m2"><p>چشمِ تو همه میل به مستی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امّا دهنت که ذرّهای را ماند</p></div>
<div class="m2"><p>یک ذرّه نه نیستی نه هستی دارد</p></div></div>