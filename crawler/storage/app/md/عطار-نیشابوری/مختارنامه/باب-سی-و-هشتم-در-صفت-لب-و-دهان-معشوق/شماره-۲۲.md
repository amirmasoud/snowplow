---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>کس مثل تو در جهانِ جان ماه نیافت</p></div>
<div class="m2"><p>همتای تو یک دلبر دلخواه نیافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانا! سخن از دهانِ تنگت گفتن</p></div>
<div class="m2"><p>کاری است که اندیشه در او راه نیافت</p></div></div>