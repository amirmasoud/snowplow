---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>چون توبهٔ تو گناه خواهد افتاد</p></div>
<div class="m2"><p>بس کس که به تو ز راه خواهد افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای ماه! به صَدْقَه یک شکربخش مرا</p></div>
<div class="m2"><p>کاین صَدْقه به جایگاه خواهد افتاد</p></div></div>