---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>ای کرده پسند از دو جهان چاره منت</p></div>
<div class="m2"><p>حقّا که دریغ دارم از خویشتنت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بیخورشید ذرّه را نتوان دید</p></div>
<div class="m2"><p>بیروی تو در چشم کی آید دهنت</p></div></div>