---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>چون گشت لبت به یک شکر ارزانی</p></div>
<div class="m2"><p>از لعلِ لبت شکر چه میافشانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من در عوض یک شکر از پستهٔ تو</p></div>
<div class="m2"><p>دل دادم نقد و قلب مینستانی</p></div></div>