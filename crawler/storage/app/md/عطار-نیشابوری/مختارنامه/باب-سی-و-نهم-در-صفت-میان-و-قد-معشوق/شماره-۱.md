---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>گفتم که «ترا عقل مه تابان گفت»</p></div>
<div class="m2"><p>گفتا که «ز دیوانگی و نقصان گفت»</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که «میان تست این یا مویی»</p></div>
<div class="m2"><p>گفتا که «درین میان سخن نتوان گفت!»</p></div></div>