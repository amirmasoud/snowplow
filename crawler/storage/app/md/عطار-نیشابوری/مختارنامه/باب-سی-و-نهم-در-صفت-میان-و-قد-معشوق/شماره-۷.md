---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>جائی که چنان خطّ سیه رنگ آید</p></div>
<div class="m2"><p>شک نیست که پای حسن در سنگ آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و آن را که میان! بود بدین باریکی</p></div>
<div class="m2"><p>نادر نبود اگر قبا تنگ آید</p></div></div>