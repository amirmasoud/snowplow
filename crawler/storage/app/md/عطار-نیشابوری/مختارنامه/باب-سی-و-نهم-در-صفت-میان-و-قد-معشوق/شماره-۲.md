---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>ای ماه! گشاده کن به وصلت گرهم</p></div>
<div class="m2"><p>تا من ز فرو بستگی غم برهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از جانب من میانِ ما مویی نیست</p></div>
<div class="m2"><p>آن موی میان تست، من بی‌گنهم</p></div></div>