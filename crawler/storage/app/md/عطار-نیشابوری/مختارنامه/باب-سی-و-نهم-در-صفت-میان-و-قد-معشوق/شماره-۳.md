---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>ای عقل ز شوق تو فغان در بسته</p></div>
<div class="m2"><p>در وصف تو دل از دل و جان در بسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وی پیش میان تو – که گوئی عدم است</p></div>
<div class="m2"><p>هر جا که وجودی است میان در بسته</p></div></div>