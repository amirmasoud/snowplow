---
title: >-
    شمارهٔ ۱۰۸
---
# شمارهٔ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>چون نیست امید غمگسارم نفسی</p></div>
<div class="m2"><p>پس من چکنم با که برآرم نفسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا دور فتادهام ازان شمع چو گل</p></div>
<div class="m2"><p>چون شمع سرِ خویش ندارم نفسی</p></div></div>