---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>چون عین بریدگی بود دوختنم</p></div>
<div class="m2"><p>پس بی خبریم به ز آموختنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه سود چو شمع اول افروختنم</p></div>
<div class="m2"><p>چون خواهدْ بود آخرش سوختنم</p></div></div>