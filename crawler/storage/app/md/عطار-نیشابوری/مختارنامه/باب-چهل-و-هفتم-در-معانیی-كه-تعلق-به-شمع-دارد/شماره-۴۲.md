---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>امروز منم عهد مصیبت بسته</p></div>
<div class="m2"><p>برخاسته دل میان خون بنشسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون شمع تنی سوخته جانی خسته</p></div>
<div class="m2"><p>امید گسسته اشک در پیوسته</p></div></div>