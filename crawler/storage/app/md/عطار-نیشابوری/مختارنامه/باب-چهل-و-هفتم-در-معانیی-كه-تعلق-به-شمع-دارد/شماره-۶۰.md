---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>چون شمع به یک نفس فرو مرده مباش</p></div>
<div class="m2"><p>در کوی هوس عمر بسر برده مباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون شمع فسرده آمد اندر ره عشق</p></div>
<div class="m2"><p>میسوزندش که نیز افسرده مباش</p></div></div>