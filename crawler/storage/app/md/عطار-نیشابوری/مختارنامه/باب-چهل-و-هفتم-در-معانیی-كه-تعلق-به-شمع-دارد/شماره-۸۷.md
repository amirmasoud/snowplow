---
title: >-
    شمارهٔ ۸۷
---
# شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>ای شمع! مگر چنان گمانْت افتادست</p></div>
<div class="m2"><p>کاتش ز زبان در دل و جانْت افتادست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دم گویی در دلم آتش افتاد</p></div>
<div class="m2"><p>این چه سخنی است کز زبانْت افتادست</p></div></div>