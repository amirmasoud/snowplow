---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>ای آن که دل زندهٔ تو مُرد از تو</p></div>
<div class="m2"><p>ناخورده ز صافِ عشق یک دُرد از تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمری است که علمِ شمع میآموزی</p></div>
<div class="m2"><p>چه سود که پروانه سبق بُرد از تو</p></div></div>