---
title: >-
    شمارهٔ ۱۰۶
---
# شمارهٔ ۱۰۶

<div class="b" id="bn1"><div class="m1"><p>دی میگفتم دستِ من و دامنِ او</p></div>
<div class="m2"><p>چون خونِ من او بریخت در گردنِ او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پروانه به پای شمع از آن افتادست</p></div>
<div class="m2"><p>تا شمع به اشکِ خود بشوید تنِ او</p></div></div>