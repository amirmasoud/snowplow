---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>جان بر گرهِ زلفِ تو آموخته گیر</p></div>
<div class="m2"><p>بی روی تو چشم از دو جهان دوخته گیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل را که چو پروانه به پای افتادست</p></div>
<div class="m2"><p>چون شمع اگر بسر برم سوخته گیر</p></div></div>