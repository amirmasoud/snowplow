---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>داری سرِ عشق کار از سردرگیر</p></div>
<div class="m2"><p>گر مست نیی خمار از سر درگیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور نرم نشد چو موم این رمز تُرا</p></div>
<div class="m2"><p>چون شمع هزار بار از سر درگیر</p></div></div>