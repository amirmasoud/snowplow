---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>در عشق چو شمع من به سوزم زنده</p></div>
<div class="m2"><p>در سوز بروی دلفروزم زنده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امشب همه گردِ من درآیند به جمع</p></div>
<div class="m2"><p>زیرا که چو شمع تا به روزم زنده</p></div></div>