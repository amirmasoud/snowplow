---
title: >-
    شمارهٔ ۱۰۵
---
# شمارهٔ ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>دل بی غم دلفروز نتوان آورد</p></div>
<div class="m2"><p>جان در طلبش به سوز نتوان آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچون شمعم هزار شب بنشانند</p></div>
<div class="m2"><p>بی سوز تو شب به روز نتوان آورد</p></div></div>