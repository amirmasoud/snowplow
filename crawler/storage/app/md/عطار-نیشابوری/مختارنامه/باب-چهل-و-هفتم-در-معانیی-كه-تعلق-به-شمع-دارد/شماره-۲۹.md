---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>با دل گفتم که راه دلبر گیرم</p></div>
<div class="m2"><p>چون راه به پای شد ز سر درگیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>واکنون که چو شمع ره به پای آوردم</p></div>
<div class="m2"><p>در سوز بمُردم چه ره از سر گیرم</p></div></div>