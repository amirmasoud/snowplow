---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>ای آتش شمع سوز ناساز مشو</p></div>
<div class="m2"><p>در شمع سرافروز و سرافراز مشو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر شمع شهد دور شد آن همه رفت</p></div>
<div class="m2"><p>چه بر سر او زنی پیش باز مشو</p></div></div>