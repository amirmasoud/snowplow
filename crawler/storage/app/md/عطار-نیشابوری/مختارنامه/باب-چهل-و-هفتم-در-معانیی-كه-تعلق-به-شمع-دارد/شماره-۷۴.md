---
title: >-
    شمارهٔ ۷۴
---
# شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>ای شمع! فروختی و لاف آوردی</p></div>
<div class="m2"><p>آتش به سر خود به گزاف آوردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سینه چو من نهفته در آتش عشق</p></div>
<div class="m2"><p>از بهر چه سر را به طواف آوردی</p></div></div>