---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>آن دل که چو موم نرمم آمدبی تو</p></div>
<div class="m2"><p>از بس که بسوخت شرمم آمد بی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تادیدهام از دور تُرا شمع توام</p></div>
<div class="m2"><p>زان در دهن آب گرمم آمد بی تو</p></div></div>