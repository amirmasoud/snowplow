---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>از بس که ز غم سوختم ای شمع طراز</p></div>
<div class="m2"><p>چون شمع ز تو سوخته میمانم باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کوتاه کنم سخن که مینتوان گفت</p></div>
<div class="m2"><p>غمهای دلم مگر به شبهای دراز</p></div></div>