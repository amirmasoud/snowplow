---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>پیوسته ز عشق جان و تن میسوزم</p></div>
<div class="m2"><p>در درد فراق خویشتن میسوزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من خام طمع به صد هزاران زاری</p></div>
<div class="m2"><p>چون شمع میان پیرهن میسوزم</p></div></div>