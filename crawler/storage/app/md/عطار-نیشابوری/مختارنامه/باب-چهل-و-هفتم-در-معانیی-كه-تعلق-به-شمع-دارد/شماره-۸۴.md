---
title: >-
    شمارهٔ ۸۴
---
# شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>ای شمع! چو از آتش افسر کردی</p></div>
<div class="m2"><p>تا دست به گردن بلا در کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سر مکن از خویش و غمِ خود خور ازانک</p></div>
<div class="m2"><p>بی سرگشتی از آنچه در سر کردی</p></div></div>