---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>سر رفت به باد و من کله میدارم</p></div>
<div class="m2"><p>چشمم بشد و گوش به ره میدارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گریه و در گداز مانندهٔ شمع</p></div>
<div class="m2"><p>میسوزم و خویش را نگه میدارم</p></div></div>