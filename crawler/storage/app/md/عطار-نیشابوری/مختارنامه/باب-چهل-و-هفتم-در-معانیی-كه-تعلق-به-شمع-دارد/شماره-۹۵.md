---
title: >-
    شمارهٔ ۹۵
---
# شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>در عشقِ تو عقل و سمع میباید باخت</p></div>
<div class="m2"><p>مردانه میان جمع میباید باخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من غرقهٔ خون چو لالهٔ سیر آبی</p></div>
<div class="m2"><p>سر در آتش چو شمع میباید باخت</p></div></div>