---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>شمعم که حریف آتشم می‌آید</p></div>
<div class="m2"><p>وز اشک همه پیشکشم می‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سوز مصیبت فراقِ تو چو شمع</p></div>
<div class="m2"><p>بر خویش گریستن خوشم می‌آید</p></div></div>