---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>چون نیست نصیبِ من به جز غمخواری</p></div>
<div class="m2"><p>موجود برای غم شدم پنداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون شمع اگر تنم بسوزد صد بار</p></div>
<div class="m2"><p>یک ذرّه ز پروانه نجویم یاری</p></div></div>