---
title: >-
    شمارهٔ ۷۳
---
# شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>ای شمع سرافراز چه پنداشته‌ای</p></div>
<div class="m2"><p>کز سرکشی خویش سر افراشته‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سوختن و بریدن افکندی سر</p></div>
<div class="m2"><p>با خویش همانا که سری داشته‌ای</p></div></div>