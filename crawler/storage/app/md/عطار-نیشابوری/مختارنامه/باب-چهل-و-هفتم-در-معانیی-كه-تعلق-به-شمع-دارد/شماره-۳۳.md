---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>زین کار که در گردنِ من خواهد بود</p></div>
<div class="m2"><p>آتش همه در خرمنِ من خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با سر نتوانم که زیم زانکه چو شمع</p></div>
<div class="m2"><p>سر بر تنِ من دشمن من خواهد بود</p></div></div>