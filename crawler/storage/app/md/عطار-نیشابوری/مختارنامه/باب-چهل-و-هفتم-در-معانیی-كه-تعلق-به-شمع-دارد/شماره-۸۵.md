---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>ای شمع! اگرچه مجلس افروخته‌ای</p></div>
<div class="m2"><p>اما تن نرم و نازکت سوخته‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو سر زده در دهان گرفتی آتش</p></div>
<div class="m2"><p>نفط اندازی از که در آموخته‌ای</p></div></div>