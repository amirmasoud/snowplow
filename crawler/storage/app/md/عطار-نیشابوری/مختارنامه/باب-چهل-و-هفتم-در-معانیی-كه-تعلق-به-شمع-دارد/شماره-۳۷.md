---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>شمعم که چنین زار و نزار آمده‌ام</p></div>
<div class="m2"><p>در سوختن و گریهٔ زار آمده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از اشک نمیرد آتشِ من همه شب</p></div>
<div class="m2"><p>چون شمع ز آتش اشکبار آمده‌ام</p></div></div>