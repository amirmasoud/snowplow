---
title: >-
    شمارهٔ ۱۰۳
---
# شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>در اشک خود از فرقت آن یار که بود</p></div>
<div class="m2"><p>غرقه شدم از گریهٔ بسیار که بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون کار من سوخته دل سوختن است</p></div>
<div class="m2"><p>با سر بردم چو شمع هر کار که بود</p></div></div>