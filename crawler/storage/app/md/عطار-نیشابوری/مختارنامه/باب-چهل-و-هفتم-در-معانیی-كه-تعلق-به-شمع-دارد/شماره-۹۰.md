---
title: >-
    شمارهٔ ۹۰
---
# شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>در شمع نگاه کن که جان میسوزد</p></div>
<div class="m2"><p>وز آتش دل همه جهان میسوزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آتش دل اوست برگرفته است از خویش</p></div>
<div class="m2"><p>بر خود دل گرم او از آن میسوزد!</p></div></div>