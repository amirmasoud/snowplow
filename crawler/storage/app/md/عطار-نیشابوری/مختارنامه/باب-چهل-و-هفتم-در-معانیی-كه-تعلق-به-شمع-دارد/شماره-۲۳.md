---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>تادور فتادهام از آن نادره کار</p></div>
<div class="m2"><p>دل گشت به صد پاره و صد شد به هزار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من چون شمعم که در فراقِ رخِ یار</p></div>
<div class="m2"><p>شب میسوزم به روز میمیرم زار</p></div></div>