---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>امشب به صفت شمع دلفروزم من</p></div>
<div class="m2"><p>میگریم و میخندم و میسوزم من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای صبح بدم که عمر شب خوش کندم</p></div>
<div class="m2"><p>زیرا که چو شمع زنده تا روزم من</p></div></div>