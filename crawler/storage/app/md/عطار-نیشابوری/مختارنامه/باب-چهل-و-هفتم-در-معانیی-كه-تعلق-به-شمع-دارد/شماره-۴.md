---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>تا چند ز سودای تو در سوز و گداز</p></div>
<div class="m2"><p>چون شمع آرم به روز شبهای دراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کی ز تو بازمانم ای شمع طراز</p></div>
<div class="m2"><p>مانندهٔ طفل تشنه از پستان باز</p></div></div>