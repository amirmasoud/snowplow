---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>در شمع نگر فتاده در سوز و گداز</p></div>
<div class="m2"><p>برّیده ز انگبین به صد تلخی باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاید که زبانْش در دهان گیرد گاز</p></div>
<div class="m2"><p>تا در آتش زبان چرا کرد دراز</p></div></div>