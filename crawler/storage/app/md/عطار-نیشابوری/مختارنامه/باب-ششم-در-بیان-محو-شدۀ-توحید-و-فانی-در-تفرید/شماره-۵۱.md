---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>چون بحر وجود روی بنمود مرا</p></div>
<div class="m2"><p>موج آمد و باکنار زد زود مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در چاه حدوث کار کردم عمری</p></div>
<div class="m2"><p>چون آب برآمد همه بربود مرا</p></div></div>