---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>در وادی عشق بیقراری است مرا</p></div>
<div class="m2"><p>سرمایهٔ این سلوک خواری است مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جاییست مرا مقام کانجا در سیر</p></div>
<div class="m2"><p>هر لحظه هزار ساله زاری است مرا</p></div></div>