---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>تا عقل من از عقیله آزادی یافت</p></div>
<div class="m2"><p>دل غمگین شد ولیک جان شادی یافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دانایی هزار جهلش بفزود</p></div>
<div class="m2"><p>در نادانی هزار استادی یافت</p></div></div>