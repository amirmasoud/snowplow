---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>صعب است به ذرّهای نگاهی کردن</p></div>
<div class="m2"><p>زان ذرّه رهی نامتناهی کردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانان چو گشاده کرد بر جان آن راه</p></div>
<div class="m2"><p>گفتم: چه کنم گفت: چه خواهی کردن</p></div></div>