---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>بستیم میان و خون دل بگشادیم</p></div>
<div class="m2"><p>پندار وجود خود ز سر بنهادیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را چه کنی ملامت، ای دوست که ما</p></div>
<div class="m2"><p>در وادی بینهایتی افتادیم</p></div></div>