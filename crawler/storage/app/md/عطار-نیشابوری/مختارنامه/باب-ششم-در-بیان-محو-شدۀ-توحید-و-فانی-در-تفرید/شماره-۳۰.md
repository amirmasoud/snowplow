---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>ما جوهر پاک خویش بشناخته‌ایم</p></div>
<div class="m2"><p>پیش از اجل این خانه بپرداخته‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از پوست برون رفتن و مرگ آزادیم</p></div>
<div class="m2"><p>کاین پوست به زندگانی انداخته‌ایم</p></div></div>