---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>اینجا شکرم مگس فرو میگیرد</p></div>
<div class="m2"><p>صد واقعه پیش و پس فرو میگیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنگر که به صحرا طلبد آنک او را</p></div>
<div class="m2"><p>در هر دو جهان نفس فرو میگیرد</p></div></div>