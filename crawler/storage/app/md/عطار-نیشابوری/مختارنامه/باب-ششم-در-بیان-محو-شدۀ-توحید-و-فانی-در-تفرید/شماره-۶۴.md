---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>آن دم که چو بحر کل شود ذات مرا</p></div>
<div class="m2"><p>روزن گردد جملهٔ ذرات مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان میسوزم، چو شمع، تا در ره عشق</p></div>
<div class="m2"><p>یک وقت شود جملهٔ اوقات مرا</p></div></div>