---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>از فوق، ورای آسمان بودم من</p></div>
<div class="m2"><p>وز تحت، زمین بیکران بودم من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمریم جهان باز همی خواند به خویش</p></div>
<div class="m2"><p>چون در نگریستم جهان بودم من</p></div></div>