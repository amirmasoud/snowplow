---
title: >-
    شمارهٔ ۷۳
---
# شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>آن وقت که گفتمی که ناشاد منم</p></div>
<div class="m2"><p>چون دانستم که بر چه بنیاد منم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حلقهٔ نیست هست چون زنجیری</p></div>
<div class="m2"><p>در هم افتاده وانچه افتاده منم</p></div></div>