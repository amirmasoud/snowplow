---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>آرام ز جانِ حاضرم میبینم</p></div>
<div class="m2"><p>جنبش ز دلِ مسافرم میبینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندان که سلوک میکنم در دل خویش</p></div>
<div class="m2"><p>نه اولِ خود نه آخرم میبینم</p></div></div>