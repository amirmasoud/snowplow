---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>هر روز حجاب بیقراران بیش است</p></div>
<div class="m2"><p>زان، درد من از قطرهٔ باران بیش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زینجا که منم تا که بدانجا که منم</p></div>
<div class="m2"><p>دو کون چه باشد که هزاران بیش است</p></div></div>