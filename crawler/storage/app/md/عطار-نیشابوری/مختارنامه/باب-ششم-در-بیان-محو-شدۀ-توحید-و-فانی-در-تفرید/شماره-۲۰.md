---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>روزی دو سه خانه در عدم باید داشت</p></div>
<div class="m2"><p>روزی دو سه دروجود هم باید داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اکنون ز وجود و از عدم آزادیم</p></div>
<div class="m2"><p>ما ما گشتیم از که غم باید داشت</p></div></div>