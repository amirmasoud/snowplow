---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>زان روز که ما به زندگانی مُردیم</p></div>
<div class="m2"><p>گوی طلب از هزار عالم بُردیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راهی که در او هزار هشیار بسوخت</p></div>
<div class="m2"><p>در مستی خویش و بیخودی بسپُردیم</p></div></div>