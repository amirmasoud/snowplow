---
title: >-
    شمارهٔ ۷۶
---
# شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>آن راز که پیوسته از آن میپرسم</p></div>
<div class="m2"><p>در جان من است و از جهان میپرسم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا هیچ کسی برون نیاید بر من</p></div>
<div class="m2"><p>او در دل و از برون نشان میپرسم</p></div></div>