---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>عمری دل من غرقهٔ خون آمده بود</p></div>
<div class="m2"><p>بر درگه عشق سرنگون آمده بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بس که زد این در وکسش درنگشاد</p></div>
<div class="m2"><p>او بود که از برون درون آمده بود</p></div></div>