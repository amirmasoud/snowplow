---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>امروز چو من شفیته و مجنون کیست</p></div>
<div class="m2"><p>بر خاک فتاده، با دلی پرخون، کیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این خود نه منم، خدای میداند و بس</p></div>
<div class="m2"><p>تا آنگاهی که بودم و اکنون کیست</p></div></div>