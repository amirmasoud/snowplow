---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>چون مرغِ دلم زین قفسِ تنگ برفت</p></div>
<div class="m2"><p>بینقش شد و چو نقش از سنگ برفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هر قدمی هزار عالم طی کرد</p></div>
<div class="m2"><p>در هر نفسی هزار فرسنگ برفت</p></div></div>