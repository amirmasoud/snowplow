---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>چندین که روی و نیک یا بد بینی</p></div>
<div class="m2"><p>گر دیدهوری آن همه از خود بینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>احول که یکی دو دید اگر آن بد دید</p></div>
<div class="m2"><p>تو زو بتری زانک یکی صد بینی</p></div></div>