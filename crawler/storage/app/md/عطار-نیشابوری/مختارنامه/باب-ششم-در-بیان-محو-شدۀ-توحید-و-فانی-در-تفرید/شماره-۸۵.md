---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>گه عشق تو در میان جان دارم من</p></div>
<div class="m2"><p>گه جان ز غم تو بر میان دارم من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن چیز که از عشق تو آن دارم من</p></div>
<div class="m2"><p>حقا که ز جان خود نهان دارم من</p></div></div>