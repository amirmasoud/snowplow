---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>زین پیش دم از سر جنون می‌زده‌ام</p></div>
<div class="m2"><p>وانگه قدم از چرا و چون می‌زده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمری بزدم این در و چون بگشادند</p></div>
<div class="m2"><p>من خود ز درون، در برون می‌زده‌ام</p></div></div>