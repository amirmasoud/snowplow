---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>آن مرغ عجب در آشیان کی گنجد</p></div>
<div class="m2"><p>وان ماه زمین در آسمان کی گنجد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن دانه که در دل زمین افکندند</p></div>
<div class="m2"><p>گر شاخ زند در دو جهان کی گنجد</p></div></div>