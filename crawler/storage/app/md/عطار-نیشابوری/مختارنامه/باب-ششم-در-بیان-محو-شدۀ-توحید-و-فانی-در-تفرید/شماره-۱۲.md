---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>از بس که دلم در بُنِ این قلزم گشت</p></div>
<div class="m2"><p>یک یک مویش زِ شور چون انجم گشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دی داشتم از جهان زبانی و دلی</p></div>
<div class="m2"><p>امروز زبان گنگ شد و دل گم گشت</p></div></div>