---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>این سودایی که میدواند ما را</p></div>
<div class="m2"><p>هرگز نتوان نشاند این سودا را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویند که خویش را فرود آر آخر</p></div>
<div class="m2"><p>دربند چگونه آورم دریا را</p></div></div>