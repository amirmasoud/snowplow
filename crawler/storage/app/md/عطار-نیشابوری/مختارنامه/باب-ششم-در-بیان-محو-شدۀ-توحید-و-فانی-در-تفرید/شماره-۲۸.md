---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>تا شاگردم به قطع استادترم</p></div>
<div class="m2"><p>تا بندهتر ز جمله آزادترم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاری است عجب کار من بی سر و بُن</p></div>
<div class="m2"><p>غمگین ترم آن زمان که دلشادترم</p></div></div>