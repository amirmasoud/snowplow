---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>بر درگه حق کراست این عز که تراست</p></div>
<div class="m2"><p>وز عالم قدس این مجاهز که تراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حقّا که نیافت هیچ پیغامبرِ حق</p></div>
<div class="m2"><p>این منزلت و مقام و معجز که تراست</p></div></div>