---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>تا هست ز انگشت تو مه را راهی</p></div>
<div class="m2"><p>میبشکافد ماه فلک، هر ماهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا روز قیامت که درآید از پای</p></div>
<div class="m2"><p>دستش گیرد چون تو شفاعت خواهی</p></div></div>