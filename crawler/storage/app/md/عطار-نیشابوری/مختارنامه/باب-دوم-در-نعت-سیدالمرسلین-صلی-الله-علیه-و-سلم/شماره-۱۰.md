---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>در امت تو اگر مطیعی نبود،</p></div>
<div class="m2"><p>بر پشتی چون توئی بدیعی نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاید که ز بیم معصیت خون گرید</p></div>
<div class="m2"><p>آن را که بحق چون تو شفیعی نبود</p></div></div>