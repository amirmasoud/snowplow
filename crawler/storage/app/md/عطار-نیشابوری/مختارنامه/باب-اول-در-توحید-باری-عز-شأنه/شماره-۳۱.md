---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>یا رب! همه اسرار، تو میدانی تو</p></div>
<div class="m2"><p>اندازهٔ هر کار، تو میدانی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین سِرّ که در نهاد ما میگردد</p></div>
<div class="m2"><p>کس نیست خبردار،‌تو میدانی تو</p></div></div>