---
title: >-
    شمارهٔ ۸۴
---
# شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>روئی که به روز پنج ره میشوئیم</p></div>
<div class="m2"><p>وز خون دو دیده گه به گه میشوئیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاتش بمسوز،‌تا به آبِ حسرت</p></div>
<div class="m2"><p>بر روی تو نامهٔ گنه میشوئیم</p></div></div>