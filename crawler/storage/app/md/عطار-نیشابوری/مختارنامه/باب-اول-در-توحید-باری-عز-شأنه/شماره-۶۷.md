---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>دل خون کن اگر سَرِ بلای تو نداشت</p></div>
<div class="m2"><p>جان بر هم سوز اگر وفای تو نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه دل و جان هیچ سزای تو نداشت</p></div>
<div class="m2"><p>کفرست همی هرچه برای تو نداشت</p></div></div>