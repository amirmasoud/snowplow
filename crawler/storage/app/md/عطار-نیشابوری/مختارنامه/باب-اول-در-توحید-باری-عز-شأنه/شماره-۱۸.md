---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>آنجا که تویی هیچ مبارز نرسد</p></div>
<div class="m2"><p>پیک نظر و عقل مُجاهِز نرسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فی الجمله، به کنه تو که کس را ره نیست</p></div>
<div class="m2"><p>نه هیچ کسی رسید و هرگز نرسد</p></div></div>