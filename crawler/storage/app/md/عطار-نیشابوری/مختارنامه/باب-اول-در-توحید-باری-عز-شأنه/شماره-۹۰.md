---
title: >-
    شمارهٔ ۹۰
---
# شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>ای جانِ من سوخته دل زندهٔ تو</p></div>
<div class="m2"><p>وز خجلت فعل خود سرافکندهٔ تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بپذیر مرا که جزتو کس نیست مرا</p></div>
<div class="m2"><p>گر نپذیری کجا رود بندهٔ تو</p></div></div>