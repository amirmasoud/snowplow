---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>ای هشت بهشت، یک نثارِ درِ تو</p></div>
<div class="m2"><p>وی هفت سپهر، پرده دارِ درِ تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخ زرد و کبود جامه، خورشیدِ منیر</p></div>
<div class="m2"><p>سرگشتهٔ ذرهٔ غبارِ درِتو</p></div></div>