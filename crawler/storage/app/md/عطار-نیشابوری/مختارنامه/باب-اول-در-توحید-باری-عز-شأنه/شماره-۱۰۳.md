---
title: >-
    شمارهٔ ۱۰۳
---
# شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>تا چند تنم پردهٔ بیچارگیم</p></div>
<div class="m2"><p>تا کی نوشم شربت غمخوارگیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وقت است که دست گیریم تا برهم</p></div>
<div class="m2"><p>کز پای درافتادهٔ یکبارگیم</p></div></div>