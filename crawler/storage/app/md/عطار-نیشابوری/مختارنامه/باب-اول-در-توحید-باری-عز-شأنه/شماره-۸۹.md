---
title: >-
    شمارهٔ ۸۹
---
# شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>سیر این دل خسته کی شود ازتو مرا</p></div>
<div class="m2"><p>ره سوی تو بسته کی شود از تو مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر زانکه کشی به قهر بندم از بند</p></div>
<div class="m2"><p>امید گسسته کی شود ازتو مرا</p></div></div>