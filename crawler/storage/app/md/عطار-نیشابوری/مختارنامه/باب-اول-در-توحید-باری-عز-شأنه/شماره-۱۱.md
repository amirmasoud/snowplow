---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>خورشید، که او زیر و زبر می‌گردد</p></div>
<div class="m2"><p>از تو،‌ به امید یک نظر می‌گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ذوقِ شکَرِ شُکْرِ تو طوطی فلک</p></div>
<div class="m2"><p>تا یافت، از آن وقت، به سر می‌گردد</p></div></div>