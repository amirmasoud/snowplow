---
title: >-
    شمارهٔ ۹۲
---
# شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>از هیبت تو این دل غم خواره بسوخت</p></div>
<div class="m2"><p>دل خود که بود که جان بیچاره بسوخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا رب بمسوز این تن سرگردان را</p></div>
<div class="m2"><p>کز آتش تشویر تو صد باره بسوخت</p></div></div>