---
title: >-
    شمارهٔ ۱۰۹
---
# شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>ای هفت زمین و آسمانها ز تو پُر</p></div>
<div class="m2"><p>چون مینشود کام و زبانها ز تو پُر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای زندگی دلم روا میداری</p></div>
<div class="m2"><p>من دست تهی، هر دو جهانها ز تو پُر</p></div></div>