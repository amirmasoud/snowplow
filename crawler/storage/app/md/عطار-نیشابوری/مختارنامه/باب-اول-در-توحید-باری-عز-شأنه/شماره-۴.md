---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>وصفت نه به اندازهٔ عقلِ کَهُن است</p></div>
<div class="m2"><p>کز وصفِ تو هر چه گفته آمد، سَخُن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هر دو جهان هر گلِ وصفت که شکفت</p></div>
<div class="m2"><p>در وادی توحیدِ تو یک خاربُن است</p></div></div>