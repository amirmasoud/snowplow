---
title: >-
    شمارهٔ ۸۱
---
# شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>چون بیخبرم که چیست تقدیر مرا</p></div>
<div class="m2"><p>دیوانگی آورد به زنجیر مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون کار به علّت نکنی با بد و نیک</p></div>
<div class="m2"><p>ترکِ بد و نیک گیر و بپذیر مرا</p></div></div>