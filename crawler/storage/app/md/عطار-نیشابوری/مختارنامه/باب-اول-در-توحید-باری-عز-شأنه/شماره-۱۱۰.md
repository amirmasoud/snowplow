---
title: >-
    شمارهٔ ۱۱۰
---
# شمارهٔ ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>گر من زگنه توبه کنم بسیاری</p></div>
<div class="m2"><p>تا تو ندهی توبه، نیم بر کاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نیکم و گر بدم مسلمان توام</p></div>
<div class="m2"><p>از کافرِ نفسم برهان یکباری</p></div></div>