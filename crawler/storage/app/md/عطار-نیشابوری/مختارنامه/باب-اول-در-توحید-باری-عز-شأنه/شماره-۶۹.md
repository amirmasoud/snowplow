---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>هرجان که طریق پردهٔ راز نیافت</p></div>
<div class="m2"><p>از پرده اگر یافت، جز آواز نیافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کور است کسی که نسخهٔ یک یک چیز</p></div>
<div class="m2"><p>در آینهٔ جمالِ تو باز نیافت</p></div></div>