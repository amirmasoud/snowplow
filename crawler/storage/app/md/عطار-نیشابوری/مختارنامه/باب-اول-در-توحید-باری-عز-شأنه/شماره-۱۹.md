---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>نه لایق کوی تست سیری که بود</p></div>
<div class="m2"><p>نه نیز موافقست خیری که بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک ذرّه خیال غیر، هرگز مگذار</p></div>
<div class="m2"><p>کافسوس بود خیال غیری که بود</p></div></div>