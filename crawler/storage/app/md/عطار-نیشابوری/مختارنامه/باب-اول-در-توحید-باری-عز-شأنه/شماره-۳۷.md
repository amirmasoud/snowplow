---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>کو چشم که ذرّهای جمالت بیند</p></div>
<div class="m2"><p>کو عقل که سُدَّهٔ کمالت بیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر جملهٔ ذرّات جهان دیده شود</p></div>
<div class="m2"><p>ممکن نبود که در وصالت بیند</p></div></div>