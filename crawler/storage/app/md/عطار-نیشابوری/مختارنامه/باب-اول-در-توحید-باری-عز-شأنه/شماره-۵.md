---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>جان، محو شد و به هیچ رویت نشناخت</p></div>
<div class="m2"><p>دل خون شد و قدرِ خاکِ کویت نشناخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای از سرّ موئی دو جهان کرده پدید!</p></div>
<div class="m2"><p>کس در دو جهان یک سرّ‌مویت نشناخت</p></div></div>