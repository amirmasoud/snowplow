---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>زان روز که از عدم پدید آمده‌ایم</p></div>
<div class="m2"><p>بر بیهده در گفت و شنید آمده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی: «جمع آی!» بس پریشان شده‌ایم</p></div>
<div class="m2"><p>گفتی: «پاک آی!» بس پلید آمده‌ایم</p></div></div>