---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>یا رب جان را بیم گنه کاران هست</p></div>
<div class="m2"><p>دل را شب و روز ماتم یاران هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی که به بیچارگی و عجز درآی</p></div>
<div class="m2"><p>بیچارگی و عجز به خرواران هست</p></div></div>