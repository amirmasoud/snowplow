---
title: >-
    شمارهٔ ۵۸
---
# شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>ای در دلِ من نشسته جانی یا نه</p></div>
<div class="m2"><p>از پیدایی چنین نهانی یا نه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن چیز که هرگز بنخواهم دانست</p></div>
<div class="m2"><p>با بنده بگو که تا تو آنی یا نه</p></div></div>