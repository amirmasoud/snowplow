---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>چون دردِ تو چاره ساز آمد جان را</p></div>
<div class="m2"><p>دردِ تو بس است این دلِ بیدرمان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون از سرِ فضل، ره نمایی همه را</p></div>
<div class="m2"><p>راهی بنما اینهمه سرگردان را</p></div></div>