---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>چون ذُلِّ من از من است و چون عزّ از تو</p></div>
<div class="m2"><p>عزْ چون طلبد این دل عاجز از تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون هر چه که داری تو سرش پیدا نیست</p></div>
<div class="m2"><p>قانع نشوم به هیچ هرگز از تو</p></div></div>