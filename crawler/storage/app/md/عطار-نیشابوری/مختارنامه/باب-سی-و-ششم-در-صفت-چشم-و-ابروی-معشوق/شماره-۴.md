---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>هر دم به حیل زخمِ دگر سانم زن</p></div>
<div class="m2"><p>وز نرگسِ مست تیرِ مژگانم زن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیرِ مژه چون کشیدهای در رویم</p></div>
<div class="m2"><p>دل خود بردی بیا و بر جانم زن</p></div></div>