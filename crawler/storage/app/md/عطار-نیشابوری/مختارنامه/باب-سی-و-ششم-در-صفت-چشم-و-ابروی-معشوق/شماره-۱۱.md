---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>زلفِ تو به هم در اوفتاده عجب است</p></div>
<div class="m2"><p>گه سرکش و گاه سر نهاده عجب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانا! مژهٔ من است در آب مدام</p></div>
<div class="m2"><p>تیرِ مژهٔ تو آب داده عجب است!</p></div></div>