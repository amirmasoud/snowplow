---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>چندان که تو این بحر گهر خواهی دید</p></div>
<div class="m2"><p>بر دیده و دیده دیده ور خواهی دید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بحری است که هر باطن هر قطره از او</p></div>
<div class="m2"><p>آرامگهِ کسی دگر خواهی دید</p></div></div>