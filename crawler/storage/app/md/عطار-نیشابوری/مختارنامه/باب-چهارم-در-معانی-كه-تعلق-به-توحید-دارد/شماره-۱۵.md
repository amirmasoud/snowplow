---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>چیزی که ورای دانش و تمییز است</p></div>
<div class="m2"><p>چون هر چیزش مدان که چیزی نیز است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بودیست که بودها در او نابود است</p></div>
<div class="m2"><p>چیزی است که چیزها در او ناچیز است</p></div></div>