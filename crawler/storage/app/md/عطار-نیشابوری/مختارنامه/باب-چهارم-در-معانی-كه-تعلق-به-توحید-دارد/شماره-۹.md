---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>پیوسته دلی گرفته از غیرت باد!</p></div>
<div class="m2"><p>در بادیهٔ یگانگی سیرت باد!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر نقش که از پرده برون میبینی</p></div>
<div class="m2"><p>چون پرده براوفتد، همه،‌خیرت باد!</p></div></div>