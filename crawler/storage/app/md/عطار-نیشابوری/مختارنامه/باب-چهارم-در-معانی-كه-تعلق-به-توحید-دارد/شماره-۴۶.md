---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>آن سر عجب نه توبدانی ونه من</p></div>
<div class="m2"><p>حل کردن آن نه تو توانی و نه من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک ذرّه گر آشکار گردد آن سر</p></div>
<div class="m2"><p>یک ذرّه نه تو نیز بخوانی و نه من</p></div></div>