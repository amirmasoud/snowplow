---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>هان ای دل بیخبر! کجاییم بیا</p></div>
<div class="m2"><p>از یکدیگر چرا جداییم بیا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنگر تو که هر ذرّه که در عالم هست</p></div>
<div class="m2"><p>فریاد همی زند که ماییم بیا</p></div></div>