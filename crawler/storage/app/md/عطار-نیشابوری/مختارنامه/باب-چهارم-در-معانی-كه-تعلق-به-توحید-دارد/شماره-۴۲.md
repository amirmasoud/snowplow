---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>گاهی ز نو و گه ز کهن میگویند</p></div>
<div class="m2"><p>گاهی ز کن و گه ز مکن میگویند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند فراغتیست لیک از سرِ لطف</p></div>
<div class="m2"><p>با ما به زبان ما سخن میگویند</p></div></div>