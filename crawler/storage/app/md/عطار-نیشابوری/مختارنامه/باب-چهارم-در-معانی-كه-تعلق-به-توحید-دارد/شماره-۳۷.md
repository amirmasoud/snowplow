---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>سریست برون زین همه اسرار که هست</p></div>
<div class="m2"><p>نوریست جدا زین همه انوار که هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خرسند مشو به هیچ کاری و بدانک</p></div>
<div class="m2"><p>کاریست ورای این همه کار که هست</p></div></div>