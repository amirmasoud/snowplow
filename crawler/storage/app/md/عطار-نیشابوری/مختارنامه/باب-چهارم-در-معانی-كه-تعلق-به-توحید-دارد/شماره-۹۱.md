---
title: >-
    شمارهٔ ۹۱
---
# شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>یارب چه نهان چه آشکارا که تویی</p></div>
<div class="m2"><p>نه عقل رسد نه علم آنجا که تویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر بگشای بر دل بسته دری</p></div>
<div class="m2"><p>تا غرقه شوم در آن تماشا که تویی</p></div></div>