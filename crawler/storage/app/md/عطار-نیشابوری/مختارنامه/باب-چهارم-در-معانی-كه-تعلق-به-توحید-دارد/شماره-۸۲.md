---
title: >-
    شمارهٔ ۸۲
---
# شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>آنجا که زمین را فلکی بینی تو</p></div>
<div class="m2"><p>بسیار زمان چو اندکی بینی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگاه که این دایره از دور استاد</p></div>
<div class="m2"><p>حالی ازل و ابد یکی بینی تو</p></div></div>