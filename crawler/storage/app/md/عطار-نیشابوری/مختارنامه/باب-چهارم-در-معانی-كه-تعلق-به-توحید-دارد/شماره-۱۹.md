---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>جایی که درو نه شیب ونه بالا بود</p></div>
<div class="m2"><p>نه جسم و جهت نه جنبشِ اجزا بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چیز که جُست مردِ جوینده بسی</p></div>
<div class="m2"><p>چون آنجا شد همه تمام آنجا بود</p></div></div>