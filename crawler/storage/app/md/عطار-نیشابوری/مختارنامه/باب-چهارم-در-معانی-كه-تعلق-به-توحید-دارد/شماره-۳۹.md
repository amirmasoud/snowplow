---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>کس نیست که دریا همه او را افتاد</p></div>
<div class="m2"><p>یا جنگ و مدارا همه او را افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با این همه هر ذرّه همی پندارد</p></div>
<div class="m2"><p>کاین کار به تنها همه او را افتاد</p></div></div>