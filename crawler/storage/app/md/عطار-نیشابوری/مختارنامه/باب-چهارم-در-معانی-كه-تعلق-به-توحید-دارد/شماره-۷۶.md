---
title: >-
    شمارهٔ ۷۶
---
# شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>گر برخیزد ز پیش چشم تو منی</p></div>
<div class="m2"><p>بینی تو که بر محض فنا مفتتنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حق مستغنیست لیک چون درنگری</p></div>
<div class="m2"><p>چون نیست جز او، از که بود مستغنی</p></div></div>