---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>دل را نه ز آدم و نه حواست نسب</p></div>
<div class="m2"><p>جان را نه زمین نه آسمان است طلب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه زَهره که باد بگذرانم بر لب</p></div>
<div class="m2"><p>نه صبر که تن زنم، زهی کار عجب!</p></div></div>