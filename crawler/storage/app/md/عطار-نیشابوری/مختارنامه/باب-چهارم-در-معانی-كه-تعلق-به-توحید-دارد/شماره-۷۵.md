---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>ور راه ز پس قطع کنی پایانت</p></div>
<div class="m2"><p>آن ذرّه بر آفتاب بگزینی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا خود به کدام ره درافتد جانت</p></div>
<div class="m2"><p>پس ظاهر اوست هر چه میبینی تو</p></div></div>