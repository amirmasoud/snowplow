---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>برخیز و به بحرِ عشقِ دلدار درای</p></div>
<div class="m2"><p>مردی کن و مردانه بدین کاردرای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از هر دو جهان چو سوزنی برهنه گرد</p></div>
<div class="m2"><p>وانگاه به بحر، سرنگونسار، درای</p></div></div>