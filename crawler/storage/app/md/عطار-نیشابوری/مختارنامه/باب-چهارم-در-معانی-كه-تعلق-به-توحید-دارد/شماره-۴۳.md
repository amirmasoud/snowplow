---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>در عالم جان نه مرد پیداست نه زن</p></div>
<div class="m2"><p>چه عالم جان نه جان هویداست نه تن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کی گویی ز ما و من شرمت باد</p></div>
<div class="m2"><p>تا چند ز ما و من که نه ماست نه من</p></div></div>