---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>هر جان که به نور قدس پیش اندیش است</p></div>
<div class="m2"><p>از خویش برون نیست همه در خویش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک ذرّه خیالِ غیر در باطن تو</p></div>
<div class="m2"><p>تخم دو هزارکوه آتش بیش است</p></div></div>