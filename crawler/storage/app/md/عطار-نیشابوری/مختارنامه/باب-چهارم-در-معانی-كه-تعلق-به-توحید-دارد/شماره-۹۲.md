---
title: >-
    شمارهٔ ۹۲
---
# شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>هر روز به حسن بیشتر خواهی بود</p></div>
<div class="m2"><p>هر لحظه به جلوهای دگر خواهی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز رخ خویشتن به کس ننمایی</p></div>
<div class="m2"><p>تا خواهی بود جلوهگر خواهی بود</p></div></div>