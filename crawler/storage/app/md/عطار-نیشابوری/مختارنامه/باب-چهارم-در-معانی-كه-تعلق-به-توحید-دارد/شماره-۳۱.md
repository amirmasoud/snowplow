---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>آن بحر که موجش گهر انداز آید</p></div>
<div class="m2"><p>در سینهٔ عاشقان به صد ناز آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک بار درآمد و مرا بیخود کرد</p></div>
<div class="m2"><p>این بار گمم کند اگر باز آید</p></div></div>