---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>هم دیده بر آن روی چو مه باید داشت</p></div>
<div class="m2"><p>هم توبه ازان روی گنه باید داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم: «جانا چشم من ازدست بشد»</p></div>
<div class="m2"><p>گفتا: «چکنم چشم نگه باید داشت»</p></div></div>