---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>جانا ره بدخویی ناساز مگیر</p></div>
<div class="m2"><p>خشمی که مبادت از سر ناز مگیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من خاک توام که باد دارم در دست</p></div>
<div class="m2"><p>چون خاک توام پای ز من باز مگیر</p></div></div>