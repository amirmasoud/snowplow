---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>گاهی به خودم بار دهد مستی مست</p></div>
<div class="m2"><p>گاهی ز خودم دور کند پستی پست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گاهیم چنان کند که حیران گردم</p></div>
<div class="m2"><p>تا هست جهان و در جهان هستی هست</p></div></div>