---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>در راه فکندهای مرا در تک و تاز</p></div>
<div class="m2"><p>گه شیب نهی پیش من و گاه فراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر لحظه مرا به شیوهای میانداز</p></div>
<div class="m2"><p>مگذار که یک نفس به خویش آیم باز</p></div></div>