---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>تا کی دل و جان دردمندم سوزی</p></div>
<div class="m2"><p>وز آتش عشق بندبندم سوزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون سوخته و فکندهٔ راه توام</p></div>
<div class="m2"><p>چندم فکنی ز چشم و چندم سوزی</p></div></div>