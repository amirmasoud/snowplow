---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>جانا بگذر به کوی ما یک باری</p></div>
<div class="m2"><p>برگیر قدم به سوی ما یک باری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خاک نظر چه میکنی بیهوده</p></div>
<div class="m2"><p>آخر بنگر به روی ما یک باری</p></div></div>