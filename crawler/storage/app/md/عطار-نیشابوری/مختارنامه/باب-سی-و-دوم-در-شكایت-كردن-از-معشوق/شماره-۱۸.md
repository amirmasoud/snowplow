---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>دیوانه شدم زلف تو زنجیر کنم</p></div>
<div class="m2"><p>به زان که هوای عقل دلگیر کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عشق تو هر حیله که میاندیشم</p></div>
<div class="m2"><p>از پیش نمیرود چه تدبیر کنم</p></div></div>