---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>ای عشقِ تو کیمیای سرگردانی</p></div>
<div class="m2"><p>وی کوی تو در بادیهٔ حیرانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون میدانم که حالِ من میدانی</p></div>
<div class="m2"><p>تا چند به خونِ جگرم گردانی</p></div></div>