---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>تا جان دارم سر وفا دارم من</p></div>
<div class="m2"><p>ور جان ببری روان روا دارم من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کی پرسی که هان چه داری در دل</p></div>
<div class="m2"><p>چون در همه آفاق ترا دارم من</p></div></div>