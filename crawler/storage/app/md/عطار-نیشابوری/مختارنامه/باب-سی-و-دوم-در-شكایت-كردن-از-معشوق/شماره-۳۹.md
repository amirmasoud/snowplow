---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>هر لحظه به سوی من شبیخون آری</p></div>
<div class="m2"><p>دست از دو جهان در دل مجنون آری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر ناله کنم که پرده برگیر آخر</p></div>
<div class="m2"><p>چیزی دگرم ز پرده بیرون آری</p></div></div>