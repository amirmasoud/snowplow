---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>آن را که ز دریای تو گوهر بایست</p></div>
<div class="m2"><p>همچون گویش نه پا و نه سر بایست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من خود بودم چنانک بودم دلتنگ</p></div>
<div class="m2"><p>دیوانگی عشق تو می در بایست</p></div></div>