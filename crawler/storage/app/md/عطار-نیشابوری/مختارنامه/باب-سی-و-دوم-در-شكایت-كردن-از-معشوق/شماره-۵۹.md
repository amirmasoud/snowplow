---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>جان میسوزد هر نفسم تا کی ازین</p></div>
<div class="m2"><p>دل میندهد هیچ کسم تا کی ازین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگرفت بلا پیش و پسم تاکی ازین</p></div>
<div class="m2"><p>فریاد ز فریاد رسم تاکی ازین</p></div></div>