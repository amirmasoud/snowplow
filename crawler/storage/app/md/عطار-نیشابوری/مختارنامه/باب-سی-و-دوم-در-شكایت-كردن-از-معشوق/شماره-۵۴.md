---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>عشق تو به هر دمم هزار افسون کرد</p></div>
<div class="m2"><p>تا عقل ز من برد و مرا مجنون کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من هرچه که داشتم ندادم از دست</p></div>
<div class="m2"><p>اما همه او ز دست من بیرون کرد</p></div></div>