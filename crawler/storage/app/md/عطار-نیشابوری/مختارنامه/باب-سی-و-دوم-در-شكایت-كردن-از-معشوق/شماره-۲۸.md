---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>از آهِ درون کام و زبانم بمسوز</p></div>
<div class="m2"><p>وز فرقت خود به یک زمانم بمسوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فعل بدمن بپوش و خونم بمریز</p></div>
<div class="m2"><p>بر دردِ دلم ببخش و جانم بمسوز</p></div></div>