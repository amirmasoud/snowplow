---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>گه عشق تو چون حلقهٔ در میبَرَدم</p></div>
<div class="m2"><p>گاه از بد و نیک بیخبر میبَرَدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دم به غرامتی دگر میکشدم</p></div>
<div class="m2"><p>هر لحظه به عالمی دگر میبَرَدم</p></div></div>