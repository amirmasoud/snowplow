---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>گر بی تو دمی خون جگر مینخورم</p></div>
<div class="m2"><p>آغشته همی شوم ز خون جگرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کار تو به هیچ گونه پی مینبرم</p></div>
<div class="m2"><p>سر گردانا که من به کارتو درم!</p></div></div>