---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>صد بار کشیدم و به سرباری بار</p></div>
<div class="m2"><p>خوارم کردی چه خیزد از خواری خوار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشقت چو مرا کشت به صد زاری زار</p></div>
<div class="m2"><p>آنگاه مرا چه سود از یاری یار</p></div></div>