---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>گر هیچ نظر کنی به روی ما کن</p></div>
<div class="m2"><p>ور هیچ گذر کنی به کوی ما کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای ترک چو کار تو همه تاختن است</p></div>
<div class="m2"><p>گر تاختنی کنی به سوی ما کن</p></div></div>