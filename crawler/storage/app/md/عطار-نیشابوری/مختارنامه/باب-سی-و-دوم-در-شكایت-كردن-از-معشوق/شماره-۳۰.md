---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>هر لحظه همی بیشترم میسوزی</p></div>
<div class="m2"><p>هر روز به نوعی دگرم میسوزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون با من بی دل بنمی سازی تو</p></div>
<div class="m2"><p>از بهر چه چندین جگرم میسوزی</p></div></div>