---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>تا کی رانی از در خود دربدرم</p></div>
<div class="m2"><p>تا کی سوزی ز آتش هجران جگرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر نظری کن که اگر بعد از این</p></div>
<div class="m2"><p>خواهی که نظر کنی نیابی اثرم</p></div></div>