---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>خون ناخوردن به از وبالست ترا</p></div>
<div class="m2"><p>زان است کزین میوه وبالست ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنست که تو حرام خوار افتادی</p></div>
<div class="m2"><p>ورنه همه خونها حلالست ترا</p></div></div>