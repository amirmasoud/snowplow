---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>جانا! دل و جانم آتش افروز از تست</p></div>
<div class="m2"><p>ناسازی این بخت جگرسوز از تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب نیست که روز دل فرومینشود</p></div>
<div class="m2"><p>خوش باد شبت که دل بدین روز از تست!</p></div></div>