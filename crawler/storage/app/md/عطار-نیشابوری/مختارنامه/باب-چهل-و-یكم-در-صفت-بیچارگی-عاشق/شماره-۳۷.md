---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>در عشق تو خوف و خطرم بسیارست</p></div>
<div class="m2"><p>خون دل وآه سحرم بسیارست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان روز که در عشق تو شور آوردم</p></div>
<div class="m2"><p>زان شور نمک بر جگرم بسیارست</p></div></div>