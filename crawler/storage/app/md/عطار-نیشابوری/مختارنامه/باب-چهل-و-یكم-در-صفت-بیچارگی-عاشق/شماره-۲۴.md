---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>جانا! ز غمت این دل دیوانه بسوخت</p></div>
<div class="m2"><p>در دام بر امّید یکی دانه بسوخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بس که دل خام طمع سودا پخت</p></div>
<div class="m2"><p>در خامی و سوز همچو پروانه بسوخت</p></div></div>