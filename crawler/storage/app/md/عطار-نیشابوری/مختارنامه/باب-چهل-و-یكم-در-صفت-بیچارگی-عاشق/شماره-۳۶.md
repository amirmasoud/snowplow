---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>دوشم غم تو وداع جان میفرمود</p></div>
<div class="m2"><p>برکندن دل ازین جهان میفرمود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پا بر زبر جهان و جان بنهادم</p></div>
<div class="m2"><p>یعنی که غم توام چنان میفرمود</p></div></div>