---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>در راه تو دانش و خرد مینرسد</p></div>
<div class="m2"><p>با عشق تو نام نیک و بد مینرسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هستی ترا نهایتی نیست از آنک</p></div>
<div class="m2"><p>هر هست که در تو میرسد مینرسد</p></div></div>