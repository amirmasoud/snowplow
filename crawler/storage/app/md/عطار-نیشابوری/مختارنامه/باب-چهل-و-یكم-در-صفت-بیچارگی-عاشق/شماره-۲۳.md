---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>گر دل گویم ز غایت مشتاقی</p></div>
<div class="m2"><p>از دست بشد باده بیار ای ساقی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور جان گویم در ره تو فانی شد</p></div>
<div class="m2"><p>جان فانی شد کنون تو دانی باقی</p></div></div>