---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>خوش خوش بربود نیکوئی تو مرا</p></div>
<div class="m2"><p>در کار کشید بدخوئی تو مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تلخی تو نیست شوربختی من است</p></div>
<div class="m2"><p>شیرینی آن ترش روئی تو مرا</p></div></div>