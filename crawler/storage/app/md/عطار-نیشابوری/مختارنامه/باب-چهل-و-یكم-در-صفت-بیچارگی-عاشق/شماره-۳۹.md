---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>در دست جفای تو زبون است دلم</p></div>
<div class="m2"><p>در پای غم تو سرنگون است دلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچند که خون دل حلال است ترا</p></div>
<div class="m2"><p>در خونِ دلم مشو که خون است دلم</p></div></div>