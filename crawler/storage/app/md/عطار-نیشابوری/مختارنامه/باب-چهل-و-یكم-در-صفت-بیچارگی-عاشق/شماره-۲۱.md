---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>تا دل به غمت فرو شد و برنامد</p></div>
<div class="m2"><p>زان روز ز دل نشان دیگر نامد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در پای تو افشاند همی هرچه که داشت</p></div>
<div class="m2"><p>دردا که به جز دریغ با سر نامد</p></div></div>