---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>گاهم ز سگ نفس مشوش بودن</p></div>
<div class="m2"><p>گاهم ز سر خشم بر آتش بودن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی: «خوش باش» چون مرا دست دهد</p></div>
<div class="m2"><p>با این همه سگ در اندرون خوش بودن</p></div></div>