---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>اول میلم چو از همه سویی بود</p></div>
<div class="m2"><p>و آورده به روی هر کسم رویی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر گفتم بمردم از هستی خویش</p></div>
<div class="m2"><p>خود فرعونی در بُنِ هر مویی بود</p></div></div>