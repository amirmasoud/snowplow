---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>مائیم که نه سوخته و نه خامیم</p></div>
<div class="m2"><p>نه صاف چشیده و نه دُرد آشامیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه چو فلک ز عشق بیآرامیم</p></div>
<div class="m2"><p>صد سال به تک دویده در یک گامیم</p></div></div>