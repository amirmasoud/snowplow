---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>این نفس کم انگاشته آید آخر</p></div>
<div class="m2"><p>تا چند سرافراشته آید آخر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای بس که فرو داشتهام این سگ را</p></div>
<div class="m2"><p>تا بوکه فرو داشته آید آخر</p></div></div>