---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>گاهی به هوس حرف فنا میخوانیم</p></div>
<div class="m2"><p>گاهی ز هوس نزد بقا میمانیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تر دامنی وجود خود میدانیم</p></div>
<div class="m2"><p>بر خشک بمانده چند کشتی رانیم</p></div></div>