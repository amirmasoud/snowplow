---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>مائیم به امر، پای ناآورده</p></div>
<div class="m2"><p>یک عذر گره گشای ناآورده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر روز هزار عهد محکم بسته</p></div>
<div class="m2"><p>وآنگاه یکی بجای ناآورده</p></div></div>