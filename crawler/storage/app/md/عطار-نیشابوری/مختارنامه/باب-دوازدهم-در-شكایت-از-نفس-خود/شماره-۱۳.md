---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>دل را که نه دنیا و نه دین میبینم</p></div>
<div class="m2"><p>با نفس پلید همنشین میبینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون شیری شد مویم و در هر بن موی</p></div>
<div class="m2"><p>صد شیر و پلنگ در کمین میبینم</p></div></div>