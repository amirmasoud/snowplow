---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>چون نفس سگیست بدگمان چتوان کرد</p></div>
<div class="m2"><p>گلخن دارد پر استخوان چتوان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر در پیشش هزارتن مُرده شوند</p></div>
<div class="m2"><p>او زندهتر است هر زمان چتوان کرد</p></div></div>