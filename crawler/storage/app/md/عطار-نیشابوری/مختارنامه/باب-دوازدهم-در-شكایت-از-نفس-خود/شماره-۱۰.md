---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>نفسی دارم که هر نفس مِه گردد</p></div>
<div class="m2"><p>گفتم که ریاضت دهمش بِهْ گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندان که به جهد لاغرش گردانم</p></div>
<div class="m2"><p>از یک سخن دروغ فربه گردد</p></div></div>