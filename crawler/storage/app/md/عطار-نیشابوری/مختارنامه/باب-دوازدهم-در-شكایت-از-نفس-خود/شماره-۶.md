---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>هرچند دریغ صدهزار است هنوز</p></div>
<div class="m2"><p>زین بیش دریغ بر شمار است هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر روز هزار بار خود را کشتم</p></div>
<div class="m2"><p>وین کافر نفس برقرار است هنوز</p></div></div>