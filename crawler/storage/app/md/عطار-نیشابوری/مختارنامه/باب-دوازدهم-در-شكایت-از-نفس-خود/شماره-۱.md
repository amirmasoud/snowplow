---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>چندان که تو اسرار حقیقت خواهی</p></div>
<div class="m2"><p>ز آنجا سخنی نیست به از کوتاهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آگاه ز سِرْ اوست ز مه تا ماهی</p></div>
<div class="m2"><p>کس را سر مویی نرسد آگاهی</p></div></div>