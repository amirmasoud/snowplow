---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>ناکرده وجودم بدل اینجا چه کنم</p></div>
<div class="m2"><p>چون نیست مرا خود محل اینجا چه کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویند بیا کآتش موسی بینی</p></div>
<div class="m2"><p>با فرعونی در بغل اینجا چه کنم</p></div></div>