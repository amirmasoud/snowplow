---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>از کس چو سخن نمیپذیری آخر</p></div>
<div class="m2"><p>آگه نشوی تا بنمیری‌ آخر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندان بدوی از پی شهوت که مپرس</p></div>
<div class="m2"><p>یک گام به صدق برنگیری آخر</p></div></div>