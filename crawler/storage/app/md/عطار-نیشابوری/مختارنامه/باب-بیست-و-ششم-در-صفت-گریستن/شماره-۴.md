---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>خون دل من که هر دم افزون گردد</p></div>
<div class="m2"><p>دریا دریا ز دیده بیرون گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وانگه که ز خاکِ تنِ من کوزه کنند</p></div>
<div class="m2"><p>گر آب در آن کوزه کنی خون گردد</p></div></div>