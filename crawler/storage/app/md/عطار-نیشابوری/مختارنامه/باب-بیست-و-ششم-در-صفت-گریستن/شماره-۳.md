---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>دریای دلم گرچه بسی میآشفت</p></div>
<div class="m2"><p>از غیرت خلق گوهر راز نسفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رازی که دلم ز خلق میداشت نهفت</p></div>
<div class="m2"><p>اشکم به سر جمع به رویم در گفت</p></div></div>