---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>تا کی ز تو روی بر زمین باید داشت</p></div>
<div class="m2"><p>سوز دل وآه آتشین باید داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس سیل که خاست هر نفس چشمم را</p></div>
<div class="m2"><p>آخر ز تو چشم این چنین باید داشت</p></div></div>