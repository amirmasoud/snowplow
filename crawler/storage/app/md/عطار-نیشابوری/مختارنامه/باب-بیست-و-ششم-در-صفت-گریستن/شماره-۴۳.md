---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>گر دل نه چنین عاشق شیدا بودی</p></div>
<div class="m2"><p>از عشق تو یک لحظه شکیبا بودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای کاش هر آن اشک که در فرقت تو،</p></div>
<div class="m2"><p>من میریزم، هزار دریا بودی</p></div></div>