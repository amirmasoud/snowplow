---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>ای عشق توأم در تک و تاب افکنده</p></div>
<div class="m2"><p>سودای توأم بی خور و خواب افکنده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی روی تودر مردمک دیدهٔ من</p></div>
<div class="m2"><p>خون ریزش را سپر بر آب افکنده</p></div></div>