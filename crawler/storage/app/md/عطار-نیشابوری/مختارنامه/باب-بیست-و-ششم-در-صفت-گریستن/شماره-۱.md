---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>چون جان دلم ز سیر،‌چون برق شدند</p></div>
<div class="m2"><p>مستغرق او، ز پای تا فرق شدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این فرعونان که در درونم بودند</p></div>
<div class="m2"><p>از بس که گریستم همه غرق شدند</p></div></div>