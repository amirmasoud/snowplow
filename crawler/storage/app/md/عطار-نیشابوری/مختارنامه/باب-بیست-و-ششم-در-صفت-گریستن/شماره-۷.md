---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>گر دل بشناختی که من کیستمی</p></div>
<div class="m2"><p>سبحان اللّه چگونه خوش زیستمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای کاش که گر تشنگی دل ننشست</p></div>
<div class="m2"><p>چشمی بودی که سیر بگریستمی</p></div></div>