---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>چون هر مویم نوحه گر آید بی تو</p></div>
<div class="m2"><p>وز هر سویم ناله برآید بی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلگون سرشکم که همی تازد تیز</p></div>
<div class="m2"><p>ای بس که به روی می درآید بی تو</p></div></div>