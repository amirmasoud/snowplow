---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>در عشق مرا چه کار با پردهٔ راز</p></div>
<div class="m2"><p>کار من دل سوخته اشک است و نیاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند که جهد میکنم در تک و تاز</p></div>
<div class="m2"><p>از دیدهٔ من اشک نمیاستد باز</p></div></div>