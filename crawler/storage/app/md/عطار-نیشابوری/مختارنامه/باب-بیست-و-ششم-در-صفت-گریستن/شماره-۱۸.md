---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>یک همنفسی کو که برو گریم من</p></div>
<div class="m2"><p>گر هم نفسی بود نکو گریم من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در روی همه زمین نمییابم باز</p></div>
<div class="m2"><p>خاکی که برو سیر فرو گریم من</p></div></div>