---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>خونی که مرا در دل و جان اکنون هست</p></div>
<div class="m2"><p>صد چندانم ز چشم چون جیحون هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر قصد کنی به خون من کشته شوی</p></div>
<div class="m2"><p>کاینجا که منم هزاردریاخون هست</p></div></div>