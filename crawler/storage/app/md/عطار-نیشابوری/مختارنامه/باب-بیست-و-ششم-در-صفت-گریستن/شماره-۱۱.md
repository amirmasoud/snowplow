---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>هر چند که پشت و روی دارم کاری</p></div>
<div class="m2"><p>از دیدهٔ خویش تازه رویم باری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رویم که ز آب دیده دارد ادرار</p></div>
<div class="m2"><p>هر لحظه مراتازه کند ادراری</p></div></div>