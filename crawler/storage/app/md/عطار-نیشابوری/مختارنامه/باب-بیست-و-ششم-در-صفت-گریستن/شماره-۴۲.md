---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>اول دل من، عشق رخت در جان داشت</p></div>
<div class="m2"><p>چون پیدا شد مینتوان پنهان داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن رفت که در دیده همی گشتم اشک</p></div>
<div class="m2"><p>کامروز به زور باز مینتوان داشت</p></div></div>