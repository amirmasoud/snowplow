---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>هر لحظه ز چرخ بیش میباید رفت</p></div>
<div class="m2"><p>گاه از بس و گه زپیش میباید رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گردِ جهان دویدنت فایده نیست</p></div>
<div class="m2"><p>گردِ سر و پای خویش میباید رفت</p></div></div>