---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>از دورِ فلک زیر و زبر خواهی شد</p></div>
<div class="m2"><p>رسوای جهانِ پرده در خواهی شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خواب درآی ای دلِ سرگشته که زود</p></div>
<div class="m2"><p>تا چشم زنی به خواب درخواهی شد</p></div></div>