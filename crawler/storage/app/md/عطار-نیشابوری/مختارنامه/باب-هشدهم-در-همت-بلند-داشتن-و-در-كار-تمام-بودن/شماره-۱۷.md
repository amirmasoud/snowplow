---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>جانی دگرست و جانفزایی دگرست</p></div>
<div class="m2"><p>شهری دگرست و پادشایی دگرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما بستهٔ دام هر گدایی نشویم</p></div>
<div class="m2"><p>ما را نظر دوست به جایی دگرست</p></div></div>