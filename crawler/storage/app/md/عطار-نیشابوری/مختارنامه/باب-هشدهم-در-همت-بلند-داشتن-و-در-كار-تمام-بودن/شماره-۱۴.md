---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>کو راه روی که ره نوردش گویم</p></div>
<div class="m2"><p>یا سوختهای که اهل دردش گویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردی که میان شغل دنیا نَفَسی</p></div>
<div class="m2"><p>با او افتد هزار مردش گویم</p></div></div>