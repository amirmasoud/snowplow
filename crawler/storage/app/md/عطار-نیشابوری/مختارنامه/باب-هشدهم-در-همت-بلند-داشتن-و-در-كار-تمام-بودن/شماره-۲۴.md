---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>درعشق گمان خود عیان باید کرد</p></div>
<div class="m2"><p>ترک بد و نیک این جهان باید کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر گوید: «ترکِ دو جهان باید داد.»</p></div>
<div class="m2"><p>بیآنکه چرا کنی چنان باید کرد</p></div></div>