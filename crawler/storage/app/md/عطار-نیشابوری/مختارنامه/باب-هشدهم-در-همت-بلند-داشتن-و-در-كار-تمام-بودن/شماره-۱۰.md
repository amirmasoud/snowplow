---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>از غیب گرت هست نشان آوردن</p></div>
<div class="m2"><p>از عیب نشاید به زبان آوردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کان چیز که ازدست بشد گر خواهی</p></div>
<div class="m2"><p>دشوار به دست میتوان آوردن</p></div></div>