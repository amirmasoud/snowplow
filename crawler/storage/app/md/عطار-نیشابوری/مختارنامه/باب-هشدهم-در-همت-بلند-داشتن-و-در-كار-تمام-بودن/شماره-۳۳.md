---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>هر چند که دریای پر آب آمد پیش</p></div>
<div class="m2"><p>بشتاب که کار با شتاب آمد پیش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر غرقه شدی چه سود کاندر همه عمر</p></div>
<div class="m2"><p>بیدار کنون شدی که خواب آمد پیش</p></div></div>