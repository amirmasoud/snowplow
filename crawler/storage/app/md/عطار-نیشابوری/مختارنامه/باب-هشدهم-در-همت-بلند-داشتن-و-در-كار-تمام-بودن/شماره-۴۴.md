---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>گر باز نماید سَرِ یک موی به تو</p></div>
<div class="m2"><p>صد گونه مدد رسد ز هر سوی به تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای بیخبر، آن چه بیوفاییست آخر</p></div>
<div class="m2"><p>تو پشت بدو کردهای او روی به تو</p></div></div>