---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>آن گنج که من در طلب آن گنجم</p></div>
<div class="m2"><p>در دیر طلسمات از آن میرنجم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن بحر کزو دو کون یک قطره نیافت</p></div>
<div class="m2"><p>آن میخواهم که جمله بر خود سنجم</p></div></div>