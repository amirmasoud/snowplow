---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>نردِ هوسِ وصال میباید باخت</p></div>
<div class="m2"><p>اسبِ طمعِ محال میباید تاخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک لحظه سپر همی نباید انداخت</p></div>
<div class="m2"><p>میباید سوخت و کار میباید ساخت</p></div></div>