---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>دل بستهٔ روی چون نگار او کن</p></div>
<div class="m2"><p>جان بر کف دست نه، نثار او کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنگر سَرِ کار و زود کار از سر گیر</p></div>
<div class="m2"><p>پس کار و سر اندر سَرِ کار او کن</p></div></div>