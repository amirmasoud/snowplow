---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>از بس که غم دنیی مردار خوری</p></div>
<div class="m2"><p>نه کار کنی ونه غم کار خوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرمایهٔ تو در همه عالم عمریست</p></div>
<div class="m2"><p>بر باد مده که غصّه بسیار خوری</p></div></div>