---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>گر مرد رهی راه نهان باید رفت</p></div>
<div class="m2"><p>صد بادیه را به یک زمان باید رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر میخواهی که راهت انجام دهد</p></div>
<div class="m2"><p>منزل همه در درون جان باید رفت</p></div></div>