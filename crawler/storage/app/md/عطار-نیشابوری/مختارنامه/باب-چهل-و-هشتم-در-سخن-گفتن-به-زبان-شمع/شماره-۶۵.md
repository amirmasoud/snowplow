---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>شمع آمد و گفت: هر که مردی بودست</p></div>
<div class="m2"><p>سوزش چو من از غایتِ دردی بودست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر گریم تلخ هم روا میدارم</p></div>
<div class="m2"><p>کز شیرینیم پیش خوردی بودست</p></div></div>