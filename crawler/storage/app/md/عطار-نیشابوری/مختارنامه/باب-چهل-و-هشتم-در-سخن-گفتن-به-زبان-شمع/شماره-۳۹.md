---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>شمع آمد و گفت: مانده در سوز و گداز</p></div>
<div class="m2"><p>کار من غم کشته کی آید با ساز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه همه جمع را زِ من روشنی است</p></div>
<div class="m2"><p>در چشم همه به هیچ میآیم باز</p></div></div>