---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>شمع آمد و گفت: یارِ من خواهد بود</p></div>
<div class="m2"><p>پروانه که جان سپارِ من خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اول چو بشویمش به اشکی که مراست</p></div>
<div class="m2"><p>آخر لحدش کنارِ من خواهد بود</p></div></div>