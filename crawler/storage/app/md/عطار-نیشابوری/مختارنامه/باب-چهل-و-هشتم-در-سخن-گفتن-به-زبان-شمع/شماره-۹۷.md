---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>شمع آمد و گفت: جور عالم برسد</p></div>
<div class="m2"><p>وین سوختن و اشک دمادم برسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من در آتش میروم آتش در من</p></div>
<div class="m2"><p>چون من برسم آتش من هم برسد</p></div></div>