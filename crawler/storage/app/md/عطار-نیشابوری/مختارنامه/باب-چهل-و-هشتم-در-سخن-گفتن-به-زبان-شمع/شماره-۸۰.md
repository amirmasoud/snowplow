---
title: >-
    شمارهٔ ۸۰
---
# شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>شمع آمد و گفت: کشته بنشینم نیز</p></div>
<div class="m2"><p>تا کشته به سوزد تن مسکینم نیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آتش تیز میزیم جان من اوست</p></div>
<div class="m2"><p>وان عمر به سر آمده میبینم نیز</p></div></div>