---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>شمع آمد وگفت: آمدهام شب پیمای</p></div>
<div class="m2"><p>تا بو که از آتش برهم در یکجای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آتش چو به پای رفت شد عمر به سر</p></div>
<div class="m2"><p>برگفتمت این حدیث از سر تا پای</p></div></div>