---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>شمع آمد و گفت: شهر پر خندهٔ ماست</p></div>
<div class="m2"><p>ابر از سر درد نیز گریندهٔ ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون من ز سر راستیی بر پایم</p></div>
<div class="m2"><p>سر میفکنندم که سرافکندهٔ ماست</p></div></div>