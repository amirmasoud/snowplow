---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>شمع آمد و گفت: نی غمم میبرسد</p></div>
<div class="m2"><p>نه سوختن دمادمم میبرسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب میسوزم که صبح را دریابم</p></div>
<div class="m2"><p>چون میبدمد صبح دمم میبرسد</p></div></div>