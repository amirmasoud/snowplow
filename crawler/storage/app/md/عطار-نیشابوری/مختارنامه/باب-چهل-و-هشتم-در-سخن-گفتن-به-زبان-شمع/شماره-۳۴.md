---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>شمع آمد و در آتش سرکش پیوست</p></div>
<div class="m2"><p>در آتش سوزان که چنان خوش پیوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیوند عجب نگر که او را افتاد</p></div>
<div class="m2"><p>بُبرید از انگبینْ به آتش پیوست</p></div></div>