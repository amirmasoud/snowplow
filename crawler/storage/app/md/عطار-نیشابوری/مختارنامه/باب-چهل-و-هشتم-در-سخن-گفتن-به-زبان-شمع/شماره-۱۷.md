---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>شمع آمد و گفت: چون منم دشمنِ من</p></div>
<div class="m2"><p>کوکس که به گازی ببُرد گردنِ من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بُکْشَنْدم تنم بماند زنده</p></div>
<div class="m2"><p>ور زنده بمانم بنماند تنِ من</p></div></div>