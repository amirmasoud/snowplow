---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>شمع آمد و گفت: آمدهام رنگ آمیز</p></div>
<div class="m2"><p>بر چهره ز ابر آتشین طوفان ریز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من از سر عشق میزنم لاف و تو هم</p></div>
<div class="m2"><p>تا خود که برد زین دو به سر آتش تیز</p></div></div>