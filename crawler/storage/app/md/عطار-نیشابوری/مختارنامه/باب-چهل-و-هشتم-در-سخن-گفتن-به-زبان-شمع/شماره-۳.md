---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>شمع آمد و گفت: جان فشان آمدهام</p></div>
<div class="m2"><p>کز کشتن و سوختن به جان آمدهام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آتش به زبان از آن برآرم هر شب</p></div>
<div class="m2"><p>کز آتشِ تیزتر زبان آمدهام</p></div></div>