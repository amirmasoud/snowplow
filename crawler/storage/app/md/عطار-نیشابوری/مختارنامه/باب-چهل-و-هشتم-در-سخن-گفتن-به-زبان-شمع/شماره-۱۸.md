---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>شمع آمد و گفتا: منِ مجنون باری</p></div>
<div class="m2"><p>ننهم قدمی ز سوز بیرون باری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بر سرمّ آتشِ جهان افروز است</p></div>
<div class="m2"><p>بالا دارد کارِ من اکنون باری</p></div></div>