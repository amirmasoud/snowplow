---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>شمع آمد زار زار و میگفت به راز</p></div>
<div class="m2"><p>حال من و آتش است با سوز و گداز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من کرده به درد گریهٔ تلخ آغاز</p></div>
<div class="m2"><p>برّیده ز من یار به شیرینی باز</p></div></div>