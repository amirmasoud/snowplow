---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>شمع آمد و گفت: جان نگر بر لبِ من</p></div>
<div class="m2"><p>گردون به خروش آمد از یاربِ من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وین طرفه که روز شادیم شب خوش کرد</p></div>
<div class="m2"><p>در آتش و سوز چون بُود خود شبِ من</p></div></div>