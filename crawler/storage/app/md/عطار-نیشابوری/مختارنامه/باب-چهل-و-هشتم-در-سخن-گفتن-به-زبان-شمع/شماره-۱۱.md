---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>شمع آمد وگفت: در دلم خون افتاد</p></div>
<div class="m2"><p>کز پرده ز بیم سوز بیرون افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من در هوس آتش و کس آگه نیست</p></div>
<div class="m2"><p>تا در سرِ من چنین هوس چون افتاد</p></div></div>