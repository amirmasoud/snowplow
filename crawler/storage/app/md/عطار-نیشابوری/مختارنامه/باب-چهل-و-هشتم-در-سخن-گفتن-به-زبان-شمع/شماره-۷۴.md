---
title: >-
    شمارهٔ ۷۴
---
# شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>شمع آمد و گفت: تا مرا یافته‌اند</p></div>
<div class="m2"><p>در تافتنم به جمع بشتافته‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کمتر باشد ز ریسمانی که مراست</p></div>
<div class="m2"><p>آن نیز در اندرون من بافته‌اند</p></div></div>