---
title: >-
    شمارهٔ ۸۳
---
# شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>شمع آمد و گفت: دولتم دوری بود</p></div>
<div class="m2"><p>کان شد که مرا پردهٔ زنبوری بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نوری که از او کار جهان نور گرفت</p></div>
<div class="m2"><p>زان نور نصیب من همه نوری بود</p></div></div>