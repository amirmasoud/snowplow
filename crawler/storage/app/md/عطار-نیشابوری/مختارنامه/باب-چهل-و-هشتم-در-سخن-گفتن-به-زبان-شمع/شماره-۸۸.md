---
title: >-
    شمارهٔ ۸۸
---
# شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>شمع آمد و گفت: اگر بمی باید رفت</p></div>
<div class="m2"><p>شک نیست که زودتر بمی بایدرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون در بند است پایم و ره در پیش</p></div>
<div class="m2"><p>ناکام مرا به سر بمی باید رفت</p></div></div>