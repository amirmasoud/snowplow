---
title: >-
    شمارهٔ ۵۸
---
# شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>شمع‌ آمد و گفت: میروم حیران من</p></div>
<div class="m2"><p>گه کشته و گه مرده و گه گریان من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخریدهام این فروختن از جان من</p></div>
<div class="m2"><p>مینفروشم تا نکنم تاوان من</p></div></div>