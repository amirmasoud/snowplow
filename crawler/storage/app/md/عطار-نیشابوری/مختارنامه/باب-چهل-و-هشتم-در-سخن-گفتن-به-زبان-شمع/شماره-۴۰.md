---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>شمع آمد و گفت: ماندهام بی سر و پا</p></div>
<div class="m2"><p>پای اندر بند و سر در آتش همه جا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گاهم بکشند و گه بسوزند به درد</p></div>
<div class="m2"><p>یک سوخته سرگشتهتر از من بنما</p></div></div>