---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>شمع آمد و گفت: جان من پُردرد است</p></div>
<div class="m2"><p>زین اشک که آتشم به روی آورده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دی شهد همی خوردم و امروز آتش</p></div>
<div class="m2"><p>تا درد همو خورد که صافی خورده است</p></div></div>