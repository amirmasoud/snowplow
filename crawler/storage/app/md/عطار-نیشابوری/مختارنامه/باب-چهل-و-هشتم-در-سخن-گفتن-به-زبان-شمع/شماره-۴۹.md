---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>شمع آمد و گفت: رختِ رفتن بستم</p></div>
<div class="m2"><p>در آتشِ سوزنده به جان پیوستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون هر نفسم به گاز سر میفکنند</p></div>
<div class="m2"><p>بر پای که سر نهم که گیرد دستم</p></div></div>