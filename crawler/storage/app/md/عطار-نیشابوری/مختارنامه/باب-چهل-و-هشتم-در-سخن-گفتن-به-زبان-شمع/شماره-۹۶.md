---
title: >-
    شمارهٔ ۹۶
---
# شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>شمع آمد و گفت: گه دلم مُرده شود</p></div>
<div class="m2"><p>گه در سوزم عمر به سر بُرده شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون در دهن آب گرمم آید بیدوست</p></div>
<div class="m2"><p>بر روی ز باد سردم افسُرده شود</p></div></div>