---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>شمع آمد و گفت: دل گرفت از خلقم</p></div>
<div class="m2"><p>کافتاد ز خلق آتشی در فرقم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون زار نسوزم و نگریم بر خویش</p></div>
<div class="m2"><p>آتش بر فرق و ریسمان در حلقم</p></div></div>