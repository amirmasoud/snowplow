---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>شمع آمد و گفت: این تن لاغر همه سوخت</p></div>
<div class="m2"><p>رفتم که مرا ز پای تا سر همه سوخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خشکم همه از دست شد و تر همه سوخت</p></div>
<div class="m2"><p>اشکی دو سه نم بماند و دیگر همه سوخت</p></div></div>