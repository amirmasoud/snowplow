---
title: >-
    شمارهٔ ۱۰۰
---
# شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>شمع آمد و گفت: سخت کوشم امشب</p></div>
<div class="m2"><p>وز آتش دل هزار جوشم امشب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دی شیر ز پستان عسل نوشیدم</p></div>
<div class="m2"><p>شیر از آتش چگونه نوشم امشب</p></div></div>