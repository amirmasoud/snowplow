---
title: >-
    شمارهٔ ۹۲
---
# شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>شمع آمد وگفت: چون درآمد آتش</p></div>
<div class="m2"><p>سر در آتش چگونه باشم سرکش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانم به لب آورد به زاری آتش</p></div>
<div class="m2"><p>کس نیست که بر لبم زند آبی خوش</p></div></div>