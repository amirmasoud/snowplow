---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>چون خط تو باعث گنه خواهد شد</p></div>
<div class="m2"><p>هر روز هزار دل ز ره خواهد شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین شیوه که خط تو محقق افتاد</p></div>
<div class="m2"><p>دیوان من از خطت سیه خواهد شد</p></div></div>