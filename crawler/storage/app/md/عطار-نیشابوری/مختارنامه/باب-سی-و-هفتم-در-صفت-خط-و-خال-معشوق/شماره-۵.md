---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>گفتم: ز خط تو بوی خون می‌آید</p></div>
<div class="m2"><p>وز خطّ تو عقل در جنون می‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتا که خط از برای زر می‌آرم</p></div>
<div class="m2"><p>گفتم که زر از سنگ برون می‌آید</p></div></div>