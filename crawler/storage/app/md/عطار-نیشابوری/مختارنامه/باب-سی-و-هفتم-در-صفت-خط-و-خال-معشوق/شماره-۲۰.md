---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>از عشقِ خط تو سرنگون میگردم</p></div>
<div class="m2"><p>وز خال تو در میان خون میگردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا روی نمود نقطهٔ خال توام</p></div>
<div class="m2"><p>چون پرگاری به سر برون میگردم</p></div></div>