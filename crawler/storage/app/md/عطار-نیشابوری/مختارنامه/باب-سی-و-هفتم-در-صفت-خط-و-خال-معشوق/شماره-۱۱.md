---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>یا رب چه خط است این که درآوردی تو</p></div>
<div class="m2"><p>تادست به بیداد برآوردی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دی خطّ به خون من همی آوردی</p></div>
<div class="m2"><p>و امروز خطی پر شکر آوردی تو</p></div></div>