---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>بندیش که بر زمین نهای آن که تویی</p></div>
<div class="m2"><p>واجرام فلک نشین نهای آن که تویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون جوهر تو، به چشم سر نتوان دید</p></div>
<div class="m2"><p>در خود منگر که این نهای آن که تویی</p></div></div>