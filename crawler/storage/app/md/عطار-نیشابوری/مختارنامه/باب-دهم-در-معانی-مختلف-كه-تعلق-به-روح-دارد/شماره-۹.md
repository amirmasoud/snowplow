---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>ای بلبل روح چند باشی مگسی</p></div>
<div class="m2"><p>پَر باز کُن و به عرش رو در نَفَسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کی بستهٔ پالان آخر</p></div>
<div class="m2"><p>پالان نتوان نهاد بر مرغ کسی</p></div></div>