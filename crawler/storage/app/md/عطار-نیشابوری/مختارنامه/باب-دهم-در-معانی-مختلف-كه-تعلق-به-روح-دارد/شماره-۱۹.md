---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>گر مرغِ دلت کارِ روش ساز کند</p></div>
<div class="m2"><p>دُرج دل تو خزینهٔ راز کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور پر ندهی ز نور معنی او را</p></div>
<div class="m2"><p>چون بشکند این قفس، چه پرواز کند</p></div></div>