---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>آنجا که فروغ عالم جان بینی</p></div>
<div class="m2"><p>خورشید و قمر را اثری زان بینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عالم جان چو قدسیان خوان بنهند</p></div>
<div class="m2"><p>طاووس فلک را مگس خوان بینی</p></div></div>