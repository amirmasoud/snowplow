---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>جانی که به نورِ حق ندارد امّید</p></div>
<div class="m2"><p>در عالم اوهام بماند جاوید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون ذرّهٔ ناچیز بوَد در سایه</p></div>
<div class="m2"><p>چون کودکِ یک روزه بوَد در خورشید</p></div></div>