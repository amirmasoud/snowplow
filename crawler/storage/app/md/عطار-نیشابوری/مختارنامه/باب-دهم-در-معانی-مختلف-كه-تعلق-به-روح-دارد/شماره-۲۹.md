---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>هر چند که کارهای تو بسیاریست</p></div>
<div class="m2"><p>از جزو به سوی کل شوی، آن کاریست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر خاصیت که در دو عالم نقد است</p></div>
<div class="m2"><p>در جوهر تو زان همه انموداریست</p></div></div>