---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>هر دیده که راه بینشانی نشناخت</p></div>
<div class="m2"><p>در پرده بماند و زندگانی نشناخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند که جاوید بقائیش دهند</p></div>
<div class="m2"><p>میدان که بقاءِ جاودانی نشناخت</p></div></div>