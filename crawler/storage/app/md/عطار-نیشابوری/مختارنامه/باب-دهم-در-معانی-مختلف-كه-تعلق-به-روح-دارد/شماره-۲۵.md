---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>سِرّی که به تو رسد ز خود پنهان دار</p></div>
<div class="m2"><p>امّید همه به درد بیدرمان دار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وانگاه ز جان آینهای ساز مدام</p></div>
<div class="m2"><p>و آن آینه در برابر جانان دار</p></div></div>