---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>هر جان که ز حق حمایتی افتادهست</p></div>
<div class="m2"><p>در هر دو جهان عنایتی افتادهست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر روح که هم ولایتی افتادهست</p></div>
<div class="m2"><p>در عالم بینهایتی افتادهست</p></div></div>