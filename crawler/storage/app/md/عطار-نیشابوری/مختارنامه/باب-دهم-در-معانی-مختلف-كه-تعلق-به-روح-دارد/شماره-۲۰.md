---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>ای بس که فلک در صف انجم گردد</p></div>
<div class="m2"><p>تا یک مردم تمام مردم گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان تو کبوتریست پرّیده ز عرش</p></div>
<div class="m2"><p>هرگاه که هادی نشود گم گردد</p></div></div>