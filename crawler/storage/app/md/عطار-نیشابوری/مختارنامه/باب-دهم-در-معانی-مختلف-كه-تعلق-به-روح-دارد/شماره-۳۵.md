---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>کردم ورقِ وجودِّ تو با تو بیان</p></div>
<div class="m2"><p>تا کی باشی در ورق سود و زیان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چیز که در هر دو جهان است عیان</p></div>
<div class="m2"><p>در جان تو هست و تو برون شو ز میان</p></div></div>