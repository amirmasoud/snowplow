---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>ای بلبلِ روح مبتلا مانده‌ای</p></div>
<div class="m2"><p>کاندر پی این دام بلا مانده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خو کرده‌ای اندر قفس خانهٔ تنگ</p></div>
<div class="m2"><p>وآگاه نه‌ای کز که جدا مانده‌ای</p></div></div>