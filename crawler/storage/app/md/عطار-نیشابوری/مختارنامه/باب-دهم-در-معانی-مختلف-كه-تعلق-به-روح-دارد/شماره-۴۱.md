---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>مرگ است خلاص عالم فانی را</p></div>
<div class="m2"><p>درهم چه کشی ز مرگ پیشانی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر مزبلهٔ تن تو را مرگ رسید</p></div>
<div class="m2"><p>با مرگ چکار جان تو با جانی را</p></div></div>