---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>گاه از غم اودست ز جان میشویی</p></div>
<div class="m2"><p>گه قصهٔ او به دردِ دل میگویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرگشته چرا گِردِ جهان میپویی</p></div>
<div class="m2"><p>کار از تو برون نیست کرا میجویی</p></div></div>