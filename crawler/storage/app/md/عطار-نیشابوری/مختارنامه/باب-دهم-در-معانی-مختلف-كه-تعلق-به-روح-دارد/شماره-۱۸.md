---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>آن ذات که جسم و جوهرش اسم بود</p></div>
<div class="m2"><p>در جسم مدان که قابل قسم بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فی الجمله یقین بدان که بیهیچ شکی</p></div>
<div class="m2"><p>گر جان تو در جسم بود جسم بود</p></div></div>