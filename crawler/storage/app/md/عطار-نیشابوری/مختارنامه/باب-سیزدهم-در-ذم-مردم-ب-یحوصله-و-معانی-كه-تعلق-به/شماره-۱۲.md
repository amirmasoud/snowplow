---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>چون مرغ دلم حوصلهٔ راز نیافت</p></div>
<div class="m2"><p>چون چرخ، طریق، جز تک و تاز نیافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویند چرا میننشیند دل تو</p></div>
<div class="m2"><p>چون بنشیند چو جای خود باز نیافت</p></div></div>