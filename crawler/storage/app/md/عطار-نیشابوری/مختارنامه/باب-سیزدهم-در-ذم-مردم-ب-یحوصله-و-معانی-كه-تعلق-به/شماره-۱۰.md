---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>ای جان تو در ذُلِّ جدائی قانع</p></div>
<div class="m2"><p>گشته دل تو به بی وفائی قانع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این سخت نیایدت که میباید بود</p></div>
<div class="m2"><p>سلطان بچه‌ای را به گدائی قانع</p></div></div>