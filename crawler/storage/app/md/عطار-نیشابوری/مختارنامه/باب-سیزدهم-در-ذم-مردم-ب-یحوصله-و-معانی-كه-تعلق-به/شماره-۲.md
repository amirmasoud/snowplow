---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>هر دل که بجان طریق دمساز نیافت</p></div>
<div class="m2"><p>در ذُلّ بماند و هیچ اعزاز نیافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اقبال دو کون، ره بدو یافتن است</p></div>
<div class="m2"><p>بیچاره کسی که ره بدو باز نیافت</p></div></div>