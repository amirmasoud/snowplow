---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>چون می بتوان به پادشاهی مردن</p></div>
<div class="m2"><p>افسوس بود بدین تباهی مردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عالم همه پرمایدهٔ انعام است</p></div>
<div class="m2"><p>تو گرسنه و تشنه بخواهی مردن</p></div></div>