---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>در عشق رخت علم و خرد باختهام</p></div>
<div class="m2"><p>چه علم و خرد که جان خود باختهام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در راه تو هرچه داشتم حاصل عمر</p></div>
<div class="m2"><p>در باختم و هنوز بد باختهام</p></div></div>