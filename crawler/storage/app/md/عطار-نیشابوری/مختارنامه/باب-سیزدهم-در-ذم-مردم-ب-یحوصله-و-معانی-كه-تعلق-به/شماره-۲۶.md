---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>در قلزم عشق تو که دیار نماند</p></div>
<div class="m2"><p>تا غرقه شوم ز خود بسی کار نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس زیر و زبر که آمدم تا آخر</p></div>
<div class="m2"><p>ناچیز چنان شدم که آثار نماند</p></div></div>