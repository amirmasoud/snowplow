---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>در بادیهٔ تو منزلی میباید</p></div>
<div class="m2"><p>وز واقعهٔ تو حاصلی میباید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون میگردد دلم به هر دم صد بار</p></div>
<div class="m2"><p>در راهِ تو از سنگ، دلی میباید</p></div></div>