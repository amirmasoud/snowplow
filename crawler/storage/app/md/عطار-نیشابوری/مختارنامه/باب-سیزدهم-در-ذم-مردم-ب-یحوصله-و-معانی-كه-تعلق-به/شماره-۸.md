---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>ای در طلب گره گشائی مرده</p></div>
<div class="m2"><p>در وصل بزاده در جدائی مرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای بر لب بحر، تشنه، با خاک شده</p></div>
<div class="m2"><p>وی بر سر گنج در گدائی مرده</p></div></div>