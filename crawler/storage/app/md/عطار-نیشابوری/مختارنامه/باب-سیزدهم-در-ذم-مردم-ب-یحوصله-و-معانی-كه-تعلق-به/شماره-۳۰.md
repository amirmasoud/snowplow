---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>هر کس که ز زلف تو ندارد تابی</p></div>
<div class="m2"><p>از چشمهٔ خضر تو نیابد آبی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر خود همه بیدارترین کس باشد</p></div>
<div class="m2"><p>حقا که ز بیداری او به خوابی</p></div></div>