---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>تا کی باشی چو آسمان در تک و تاز</p></div>
<div class="m2"><p>در زیر قدم شو چو زمین پستِ نیاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر صبر کنی، صبر، کند کار تو راست</p></div>
<div class="m2"><p>ور نه پس و پیش میدو و کژ میباز</p></div></div>