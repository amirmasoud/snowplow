---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>تا کی هنر خویش پدیدار کنی</p></div>
<div class="m2"><p>بنشینی و پوستین اغیار کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون در قدمی هزار انکار کنی</p></div>
<div class="m2"><p>تنها بنشین که سود بسیار کنی</p></div></div>