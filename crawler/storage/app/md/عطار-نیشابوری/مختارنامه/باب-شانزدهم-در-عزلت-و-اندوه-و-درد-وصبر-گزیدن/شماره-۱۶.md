---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>از جزو به سوی کل سفر باید کرد</p></div>
<div class="m2"><p>وز کل به کل نیز گذر باید کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون هر کل و هر جزو بدیدی و شدی</p></div>
<div class="m2"><p>آنگاه به کلِّ کل نظر باید کرد</p></div></div>