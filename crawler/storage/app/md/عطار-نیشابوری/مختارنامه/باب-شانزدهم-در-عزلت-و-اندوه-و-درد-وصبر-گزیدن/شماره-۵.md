---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>بد چند کنی کار نکو کن، بنشین</p></div>
<div class="m2"><p>سجادهٔ تسلیم فرو کن، بنشین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون شیوهٔ خلق دیدی و دانستی</p></div>
<div class="m2"><p>خط بر همه کش روی بدو کن، بنشین</p></div></div>