---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>دیرست که دور آسمان میگردد</p></div>
<div class="m2"><p>میترسد و زان ترس بجان میگردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دید که قبله گاه دنیا چونست</p></div>
<div class="m2"><p>صد قرن گذشت و همچنان میگردد</p></div></div>