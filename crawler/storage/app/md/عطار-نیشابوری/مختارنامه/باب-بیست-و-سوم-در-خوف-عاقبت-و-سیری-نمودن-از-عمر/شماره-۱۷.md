---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>تا کی باشم گرد جهان در تک و تاز</p></div>
<div class="m2"><p>سیر آمدم از جهان و از آز و نیاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرگی که مرا رهاند از عمر دراز</p></div>
<div class="m2"><p>حقا که به آرزوش میجویم باز</p></div></div>