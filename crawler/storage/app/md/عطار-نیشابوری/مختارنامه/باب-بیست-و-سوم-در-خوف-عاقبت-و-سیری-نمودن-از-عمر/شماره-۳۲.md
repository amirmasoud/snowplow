---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>چون دل ز طلب در ره جانان استاد</p></div>
<div class="m2"><p>نه با تن خود گفت ونه با جان استاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آری چو شتاب و خوف بسیار شود</p></div>
<div class="m2"><p>با یکدیگر به قطع نتوان استاد</p></div></div>