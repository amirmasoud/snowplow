---
title: >-
    شمارهٔ ۵۸
---
# شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>هم کار ز دست رفت در بی کاری</p></div>
<div class="m2"><p>هم عمر عزیز میرود در خواری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چون بود این باقی عمرم که نبود</p></div>
<div class="m2"><p>از عمر گذشته هیچ برخورداری</p></div></div>