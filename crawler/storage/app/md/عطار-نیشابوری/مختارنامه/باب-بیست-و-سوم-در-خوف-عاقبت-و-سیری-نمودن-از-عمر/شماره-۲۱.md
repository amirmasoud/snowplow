---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>اشکم پس و پیش منزلم بگرفتهست</p></div>
<div class="m2"><p>سیلاب بلا آب و گلم بگرفتهست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر لحظه هزار مشکلم بگرفتهست</p></div>
<div class="m2"><p>دیرست که از خویش دلم بگرفتهست</p></div></div>