---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>دردا که ز خواب بس دل غافل ما</p></div>
<div class="m2"><p>تا موی سپید شد سیه شد دل ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دردا و دریغا که به جز درد و دریغ</p></div>
<div class="m2"><p>حاصل نامد ز عمر بیحاصل ما</p></div></div>