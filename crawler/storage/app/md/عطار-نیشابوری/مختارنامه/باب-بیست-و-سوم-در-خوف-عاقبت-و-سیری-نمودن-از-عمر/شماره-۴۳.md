---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>تا کی به هوس چارهٔ بهبود کنیم</p></div>
<div class="m2"><p>کان به که خوشی عزمِ سفر زود کنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون عمرِ عزیز بود سرمایهٔ ما</p></div>
<div class="m2"><p>سرمایه ز دست رفت چه سود کنیم</p></div></div>