---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>امروز منم خسته ازین بحر فضول</p></div>
<div class="m2"><p>سیر آمده یکبارگی از جان ملول</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کردند ز کار هر دو کونم معزول</p></div>
<div class="m2"><p>خود را بدروغ چند دارم مشغول</p></div></div>