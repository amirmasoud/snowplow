---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>دیرست که جان خویشتن میسوزم</p></div>
<div class="m2"><p>وز آتش جان، چو شمع، تن میسوزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای کاش، شد آمدم نبودی که مدام</p></div>
<div class="m2"><p>تا آمدم از بیم شدن میسوزم</p></div></div>