---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>چون روی به پنجاه و به شصت آوردیم</p></div>
<div class="m2"><p>چیزی که نشایست به دست آوردیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز درین جهان دارم جز عجز</p></div>
<div class="m2"><p>در نزد خدائیت شکست آوردیم</p></div></div>