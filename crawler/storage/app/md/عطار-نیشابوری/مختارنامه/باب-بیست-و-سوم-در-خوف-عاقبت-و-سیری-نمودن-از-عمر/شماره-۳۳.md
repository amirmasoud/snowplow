---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>یک ذره چو آن حکم دگرگون نشود</p></div>
<div class="m2"><p>بیمرگ کسی به راه بیرون نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون گشت دلمْ ز خوف این وادی صعب</p></div>
<div class="m2"><p>سنگی بود آن دل که ازین خون نشود</p></div></div>