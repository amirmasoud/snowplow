---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>چون خواهد بود در کمین افتادن</p></div>
<div class="m2"><p>بر خاستنت زیرترین افتادن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انصاف بده دلا که کاری است عظیم</p></div>
<div class="m2"><p>در ششدرهٔ روی زمین افتادن</p></div></div>