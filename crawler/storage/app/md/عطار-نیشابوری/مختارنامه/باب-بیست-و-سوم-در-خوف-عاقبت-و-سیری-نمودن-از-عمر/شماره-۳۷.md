---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>چون پنجه سال خویشتن را کُشتم</p></div>
<div class="m2"><p>بر عمرِ نهاد سالِ شصت انگشتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شک نیست که شست را کمانی باید</p></div>
<div class="m2"><p>چون شصت تمام شد کمان شد پشتم</p></div></div>