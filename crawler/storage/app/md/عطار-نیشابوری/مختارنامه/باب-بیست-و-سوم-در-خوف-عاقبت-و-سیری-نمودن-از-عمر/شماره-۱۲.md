---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>گاهی ز غم نفس وخرد میگریم</p></div>
<div class="m2"><p>گاهی ز برای نیک و بد میگریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر آخر عمر گوشهای دست دهد</p></div>
<div class="m2"><p>بنشینم و بر گناه خود میگریم</p></div></div>