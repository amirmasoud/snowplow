---
title: >-
    شمارهٔ ۷۶
---
# شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>با این دلِ چون قیر چه خواهی کردن</p></div>
<div class="m2"><p>با نَفْسِ زبون گیر چه خواهی کردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در روز جوانی بنکردی کاری</p></div>
<div class="m2"><p>امروز چنین پیر چه خواهی کردن</p></div></div>