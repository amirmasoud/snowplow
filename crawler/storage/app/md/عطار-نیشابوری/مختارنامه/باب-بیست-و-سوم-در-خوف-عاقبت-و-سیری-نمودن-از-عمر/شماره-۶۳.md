---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>امروز منم نشسته نه نیست نه هست</p></div>
<div class="m2"><p>در پردهٔ نیستْ هست شوریده و مست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه چاره کنم چو شیشه افتاد و شکست</p></div>
<div class="m2"><p>هم دست ز کار رفت و هم کار از دست</p></div></div>