---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>بی روی تو در ماه سیاهی آمد</p></div>
<div class="m2"><p>مرگت به جوانی و پگاهی آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خفتی نه چنان نیز که برخواهی خاست</p></div>
<div class="m2"><p>رفتی نه چنان که باز خواهی آمد</p></div></div>