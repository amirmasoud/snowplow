---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>دی بر سر خاک دلبری با دل ریش</p></div>
<div class="m2"><p>میباریدم خون جگر بر رخ خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آواز آمد که چند گریی بر ما</p></div>
<div class="m2"><p>بر خویش گری که کار داری در پیش</p></div></div>