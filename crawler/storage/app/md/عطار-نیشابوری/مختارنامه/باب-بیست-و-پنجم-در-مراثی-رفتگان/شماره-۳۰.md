---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>از کفر بتر بی تو غنودن ما را</p></div>
<div class="m2"><p>آخر ز تو گفتن و شنودن ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای روی چو ماه کرده در خاک سیاه</p></div>
<div class="m2"><p>بی روی تو نیست روی بودن ما را</p></div></div>