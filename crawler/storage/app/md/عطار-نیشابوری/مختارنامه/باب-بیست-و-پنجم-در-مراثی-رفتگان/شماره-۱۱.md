---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>ای ماه زمین به برج افلاک شدی</p></div>
<div class="m2"><p>یا رب که چه پاک آمدی و پاک شدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناخورده در آتش جوانی آبی</p></div>
<div class="m2"><p>چون باد درآمدی و برخاک شدی</p></div></div>