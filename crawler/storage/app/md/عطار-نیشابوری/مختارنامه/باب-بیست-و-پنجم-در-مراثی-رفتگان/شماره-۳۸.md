---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>از گریهٔ زار ابر، گل تازه و پاک</p></div>
<div class="m2"><p>خندان بدمید دامن خود زده چاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان میگریم چو ابر بر خاک تو زار</p></div>
<div class="m2"><p>تا بو که چو گل شکفته گردی از خاک</p></div></div>