---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>چون گریهٔ من ابر بهاری نبود</p></div>
<div class="m2"><p>چون نالهٔ من ناله بزاری نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون من زغم مرگ تو ای یار عزیز</p></div>
<div class="m2"><p>در شهر به صد هزار خواری نبود</p></div></div>