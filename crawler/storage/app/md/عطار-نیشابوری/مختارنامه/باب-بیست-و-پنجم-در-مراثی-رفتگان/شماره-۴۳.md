---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>گل خندان شد ز گریهٔ ابر بهار</p></div>
<div class="m2"><p>با ما بنشین یک نفس ای سیم عذار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بندیش که چون بسر شود ما را کار</p></div>
<div class="m2"><p>بسیار به خاک ما فرو گریی زار</p></div></div>