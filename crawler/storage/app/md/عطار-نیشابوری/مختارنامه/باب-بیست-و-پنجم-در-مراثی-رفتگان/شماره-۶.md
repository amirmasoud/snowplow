---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>ماهی که چو مهر عالم آرای افتاد</p></div>
<div class="m2"><p>تا هر کس را به مهر او رای افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دی میشد و میکشید موی اندر پای</p></div>
<div class="m2"><p>و امروز چو موی گشت و از پای افتاد</p></div></div>