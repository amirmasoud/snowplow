---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>آن ماه که از کنار شد بیرونم</p></div>
<div class="m2"><p>در ماتم او کنار شد پر خونم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوشش دیدم به خواب در،‌خفته به خاک</p></div>
<div class="m2"><p>گفتم: چونی گفت: چه گویم چونم</p></div></div>