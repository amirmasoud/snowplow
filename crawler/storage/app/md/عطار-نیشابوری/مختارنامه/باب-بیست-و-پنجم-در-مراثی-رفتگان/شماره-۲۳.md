---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>گفتم همه عمر نازنینت بینم</p></div>
<div class="m2"><p>امروز چه گونه در زمینت بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای در دل خاک خفته خون کرده دلم</p></div>
<div class="m2"><p>کی دانستم که این چنینت بینم</p></div></div>