---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>میگریم ازان مهوشم و میگریم</p></div>
<div class="m2"><p>شکّر چو لبش میچشم و میگریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاکی که بدو رسید روزی قدمش</p></div>
<div class="m2"><p>در دیدهٔ خود میکشم و میگریم</p></div></div>