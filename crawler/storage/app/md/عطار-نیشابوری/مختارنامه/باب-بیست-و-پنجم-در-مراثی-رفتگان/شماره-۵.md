---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>دردا که گلم میان گلزار بریخت</p></div>
<div class="m2"><p>وز باد اجل بزاری زار بریخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این درد دلم با که بگویم که بهار</p></div>
<div class="m2"><p>بشکفت گل و گل من از بار بریخت</p></div></div>