---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>بس زود به مرگ کردی آهنگ آخر</p></div>
<div class="m2"><p>گویی رفتی هزار فرسنگ آخر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ناز چو درجهان نمیگنجیدی</p></div>
<div class="m2"><p>چون گنجیدی در لحد تنگ آخر</p></div></div>