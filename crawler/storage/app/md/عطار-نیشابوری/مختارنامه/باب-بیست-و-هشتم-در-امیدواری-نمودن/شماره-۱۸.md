---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>تا کی بی تو زاری پیوست کنم</p></div>
<div class="m2"><p>جان را ز شرابِ عشق تو مست کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گاهی خود را نیست و گه هست کنم</p></div>
<div class="m2"><p>وقت است که در گردن تو دست کنم</p></div></div>