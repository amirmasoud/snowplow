---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>تا دولت برگشته چه خواهد کردن</p></div>
<div class="m2"><p>وین چاک دگر گشته چه خواهد کردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وین قطرهٔ خون که زیر صد اندوه است</p></div>
<div class="m2"><p>یعنی دل سرگشته چه خواهد کردن</p></div></div>