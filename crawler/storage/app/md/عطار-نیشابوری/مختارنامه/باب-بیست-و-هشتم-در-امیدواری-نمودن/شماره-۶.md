---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>در اصل چو مقبول ونه مهمل بودم</p></div>
<div class="m2"><p>نه بوالعجب احوال و نه احول بودم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در فرع به صد هزار بند افتادم</p></div>
<div class="m2"><p>آخر برسم بر آنچه اوّل بودم</p></div></div>