---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>نی از سر زلفت خبری میرسدم</p></div>
<div class="m2"><p>نی از لبِ لعلت شکری میرسدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از روی توام گر نظری مینرسد</p></div>
<div class="m2"><p>در کوی تو باری گذری میرسدم</p></div></div>