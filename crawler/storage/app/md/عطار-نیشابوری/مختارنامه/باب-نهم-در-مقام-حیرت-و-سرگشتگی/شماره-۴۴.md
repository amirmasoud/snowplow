---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>چندان که ز هر شیوه سخن میگویم</p></div>
<div class="m2"><p>میننماید کنه معانی رویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و امروز اگر چه عمر در علم گذشت</p></div>
<div class="m2"><p>تقلید نخست روزه وا میجویم</p></div></div>