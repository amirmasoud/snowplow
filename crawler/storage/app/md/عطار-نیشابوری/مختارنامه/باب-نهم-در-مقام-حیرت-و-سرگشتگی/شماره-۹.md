---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>دانی که چهایم نه بزرگیم نه خُرد</p></div>
<div class="m2"><p>دانی که چه میخوریم نه صاف نه دُرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه میبتوان ماند نه میبتوان بُرد</p></div>
<div class="m2"><p>نه میبتوان زیست نه میبتوان مُرد</p></div></div>