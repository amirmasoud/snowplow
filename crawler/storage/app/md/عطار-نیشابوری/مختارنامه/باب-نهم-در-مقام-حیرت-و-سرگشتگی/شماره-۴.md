---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>بنشین که اگر بسی گذر خواهی کرد</p></div>
<div class="m2"><p>هم بر سر خویش خاک بر خواهی کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندان که درین پرده سفر خواهی کرد</p></div>
<div class="m2"><p>حیرانی خویش بیشتر خواهی کرد</p></div></div>