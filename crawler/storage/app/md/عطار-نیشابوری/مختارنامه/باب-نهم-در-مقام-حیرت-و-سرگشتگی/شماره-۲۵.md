---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>چون چارهٔ خویش میندانم چه کنم</p></div>
<div class="m2"><p>مویی کم و بیش میندانم چه کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بادیهای فتادهام بی سر و پای</p></div>
<div class="m2"><p>راه از پس و پیش میندانم چه کنم</p></div></div>