---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>یک بیدل و بیرأی چو من بنمایید</p></div>
<div class="m2"><p>نه جامه و نه جای چو من بنمایید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گردش این دایرهٔ بی سر و پای</p></div>
<div class="m2"><p>یک بی سر و بی پای چو من بنمایید</p></div></div>