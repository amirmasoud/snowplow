---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>امروز منم ذوقِ خرد نادیده</p></div>
<div class="m2"><p>انسی ز وجودِ نیک و بد نادیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در واقعهای که شرح مینتوان داد</p></div>
<div class="m2"><p>هرگز متحیری چو خود نادیده</p></div></div>