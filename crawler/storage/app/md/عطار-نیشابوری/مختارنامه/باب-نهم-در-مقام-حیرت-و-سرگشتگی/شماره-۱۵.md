---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>دل از همه عالم به کنار آمد باز</p></div>
<div class="m2"><p>بگریخت زلشکر به حصار آمد باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با این همه درد و رنج آگاه نیم</p></div>
<div class="m2"><p>تا آمدن من به چه کار آمد باز</p></div></div>