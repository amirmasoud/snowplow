---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>گر برکشم از سینهٔ پرخون آهی</p></div>
<div class="m2"><p>آتش گیرد جملهٔ عالم ماهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین حیرت اگر ز دل برآرم نفسی</p></div>
<div class="m2"><p>بر هم سوزم همه جهان ناگاهی</p></div></div>