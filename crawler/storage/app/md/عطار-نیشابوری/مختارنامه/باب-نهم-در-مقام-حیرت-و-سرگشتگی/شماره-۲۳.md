---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>چون عمر بشد زادِ رهم از «چه کنم»</p></div>
<div class="m2"><p>تدبیر گشادِ گرهم از «چه کنم»</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون از «چه کنم» هیچ نخواهد آمد</p></div>
<div class="m2"><p>آخر چه کنم تا برهم از «چه کنم»</p></div></div>