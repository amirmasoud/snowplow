---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>من زین دل بی‌خبر به جان آمده‌ام</p></div>
<div class="m2"><p>وز جان ستمکش به فغان آمده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون کار جهان با من و بی‌من یکسانسْت</p></div>
<div class="m2"><p>پس من به چه کار در جهان آمده‌ام</p></div></div>