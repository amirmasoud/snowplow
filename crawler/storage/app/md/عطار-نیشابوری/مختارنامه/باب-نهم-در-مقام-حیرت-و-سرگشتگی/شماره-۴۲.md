---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>نی کس خبری میدهد از پیشانم</p></div>
<div class="m2"><p>نه یک نفس آگهی است از پایانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون زیستنی به جهل مینتوانم</p></div>
<div class="m2"><p>روزی صد بار میبسوزد جانم</p></div></div>