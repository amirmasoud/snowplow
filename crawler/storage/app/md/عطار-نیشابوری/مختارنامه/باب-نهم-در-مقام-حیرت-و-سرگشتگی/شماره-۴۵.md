---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>در بادیهٔ جهان دری بنمایید</p></div>
<div class="m2"><p>وین بادیه را پا و سری بنمایید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای خلق! درین دایرهٔ سرگردان</p></div>
<div class="m2"><p>سرگشتهتر از من دگری بنمایید</p></div></div>