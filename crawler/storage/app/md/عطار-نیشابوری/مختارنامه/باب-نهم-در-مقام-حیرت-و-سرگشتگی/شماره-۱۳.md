---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>این درد چه دردیست که درمانش نیست</p></div>
<div class="m2"><p>وین راه چه راهیست که پایانش نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هان ای دل سرگشته بدین وادی صعب</p></div>
<div class="m2"><p>تا چند فرو روی که پایانش نیست</p></div></div>