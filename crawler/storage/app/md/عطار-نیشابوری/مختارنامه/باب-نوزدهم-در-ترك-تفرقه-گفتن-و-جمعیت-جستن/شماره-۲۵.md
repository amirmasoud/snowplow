---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>گر یک سرِ موی سرِّ جانان بینی</p></div>
<div class="m2"><p>هر درد که هست عینِ درمان بینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک قطره بگیر، خواه بد خواهی نیک</p></div>
<div class="m2"><p>پس لازمِ آن باش، همه آن بینی</p></div></div>