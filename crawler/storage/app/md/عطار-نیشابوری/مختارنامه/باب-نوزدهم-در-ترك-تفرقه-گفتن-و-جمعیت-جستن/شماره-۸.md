---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>هر چند که بیرون و درون خواهی دید</p></div>
<div class="m2"><p>مشتی رگ و استخوان و خون خواهی دید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر روز،‌هزار پرده بر خویش تنی</p></div>
<div class="m2"><p>با این همه پرده، راه چون خواهی دید</p></div></div>