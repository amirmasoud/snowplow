---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>شد از تو جهان بیرخ آن ماه سیاه</p></div>
<div class="m2"><p>گو شو که جهان سیاه گردد بیماه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او را تو برای خویشتن میطلبی</p></div>
<div class="m2"><p>پس عاشق خویش بودهیی چندین گاه</p></div></div>