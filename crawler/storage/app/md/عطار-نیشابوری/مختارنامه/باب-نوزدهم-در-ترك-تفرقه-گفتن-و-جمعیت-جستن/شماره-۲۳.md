---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>بی فکر دلی که هست خرّم دارش</p></div>
<div class="m2"><p>نقد دو جهان جمله مسلّم دارش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هر که نماند هیچ اندیشه و درد</p></div>
<div class="m2"><p>دریای حقیقت است محکم دارش</p></div></div>