---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>تا با تو، تویی بود، کجا گیری تو</p></div>
<div class="m2"><p>از کس سخنی به صدق نپذیری تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر لحظه که بیحضور او خواهی بود</p></div>
<div class="m2"><p>کافر میری آن دم اگر میری تو</p></div></div>