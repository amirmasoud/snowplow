---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>پیوستن تو به یک به یک بسیاریست</p></div>
<div class="m2"><p>بگسل که قبول خلق مشکل کاریست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میدان به یقین که در میان جانت</p></div>
<div class="m2"><p>هرجا که خوش آمدی بود زناریست</p></div></div>