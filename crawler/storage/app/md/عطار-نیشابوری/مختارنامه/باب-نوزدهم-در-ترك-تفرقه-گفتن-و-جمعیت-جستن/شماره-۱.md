---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>تا هیچ پراکنده توانی بودن</p></div>
<div class="m2"><p>حقّا که اگر بنده توانی بودن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از یک یک چیز میبباید مردن</p></div>
<div class="m2"><p>تا بوک بدو زنده توانی بودن</p></div></div>