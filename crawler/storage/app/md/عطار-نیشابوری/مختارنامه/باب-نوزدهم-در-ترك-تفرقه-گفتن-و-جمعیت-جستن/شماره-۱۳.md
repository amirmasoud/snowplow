---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>دیوانه اگر مقید زنجیرست</p></div>
<div class="m2"><p>سر تا سر کار او همه تقصیرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا شیوهٔ تو تصرّف و تدبیرست</p></div>
<div class="m2"><p>یک یک چیزت که هست دامنگیرست</p></div></div>