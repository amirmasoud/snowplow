---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>تا چند ترا ز پرده بیش آوردن</p></div>
<div class="m2"><p>در هر نفسی تفرقه پیش آوردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانی که عذاب سختتر چیست ترا</p></div>
<div class="m2"><p>تنها بودن روی به خویش آوردن</p></div></div>