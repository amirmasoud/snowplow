---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>اول بنگر به جانِ چون برقِ همه</p></div>
<div class="m2"><p>و آخر به میان خاک و خون غرقِ همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میمیراند به زاری و میگوید:</p></div>
<div class="m2"><p>چون ما هستیم خاک بر فرقِ همه!</p></div></div>