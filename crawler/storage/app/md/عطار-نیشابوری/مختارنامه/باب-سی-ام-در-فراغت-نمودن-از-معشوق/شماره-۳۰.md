---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>اندهگن توییم از دیری گاه</p></div>
<div class="m2"><p>در ما نگر، ای مرا ز اندوه پناه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کانها که به حسن گوی بردند زماه</p></div>
<div class="m2"><p>کردند در اندوهگن خویش نگاه</p></div></div>