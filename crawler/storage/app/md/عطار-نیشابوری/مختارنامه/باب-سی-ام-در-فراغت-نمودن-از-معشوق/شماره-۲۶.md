---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>هر روز ز نو پردهٔ دیگر سازی</p></div>
<div class="m2"><p>تادر پس پرده عشق با خود بازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون تو نفسی به سر نیائی از خویش</p></div>
<div class="m2"><p>هرگز به کسی دگر کجا پردازی</p></div></div>