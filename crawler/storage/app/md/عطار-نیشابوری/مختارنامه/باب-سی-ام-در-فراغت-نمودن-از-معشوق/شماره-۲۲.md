---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>چون باد ز من می‌گذری چه تْوان کرد</p></div>
<div class="m2"><p>چون خاک رهم می‌سپری چه تْوان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچند که با تو آشنا می‌گردم</p></div>
<div class="m2"><p>هرروز تو بیگانه‌تری چه تْوان کرد</p></div></div>