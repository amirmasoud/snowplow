---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>بی پیش و پسی تو و پس و پیش تراست</p></div>
<div class="m2"><p>دوری ز کم و بیش و کم و بیش تراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خاطر هیچ کسی نیاید هرگز</p></div>
<div class="m2"><p>یک ذرّه از آن خوی که از خویش تراست</p></div></div>