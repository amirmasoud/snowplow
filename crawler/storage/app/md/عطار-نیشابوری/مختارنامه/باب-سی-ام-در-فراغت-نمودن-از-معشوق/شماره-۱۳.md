---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>سرگشتهٔ روز و شبم آنجا که منم</p></div>
<div class="m2"><p>دلسوخته، جان بر لبم آنجا که منم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو فارغی آنجا که تویی از من و من</p></div>
<div class="m2"><p>تا آمدهام میطپم آنجا که منم</p></div></div>