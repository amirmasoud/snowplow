---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>گفتم: دل و جان در سر کارت کردم</p></div>
<div class="m2"><p>هر چیز که داشتم نثارت کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتا: تو که باشی که کنی یا نکنی</p></div>
<div class="m2"><p>کان من بودم که بیقرارت کردم</p></div></div>