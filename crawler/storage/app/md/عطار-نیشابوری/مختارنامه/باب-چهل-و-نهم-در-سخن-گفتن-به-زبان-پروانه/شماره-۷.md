---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>پروانه به شمع گفت: چون خوش افتاد</p></div>
<div class="m2"><p>حالی که مرا با چو تو سرکش افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویند که در سوخته افتد آتش</p></div>
<div class="m2"><p>این سوختهٔ تو چون در آتش افتاد</p></div></div>