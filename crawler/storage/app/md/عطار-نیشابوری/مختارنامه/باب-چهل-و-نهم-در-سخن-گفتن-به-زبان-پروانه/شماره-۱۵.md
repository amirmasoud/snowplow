---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>پروانه به شمع گفت: دمسازی من</p></div>
<div class="m2"><p>میبینی و میکنی سراندازی من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با این همه گر چه نیست با جان بازی</p></div>
<div class="m2"><p>در عشق تو کس نیست به جانبازی من</p></div></div>