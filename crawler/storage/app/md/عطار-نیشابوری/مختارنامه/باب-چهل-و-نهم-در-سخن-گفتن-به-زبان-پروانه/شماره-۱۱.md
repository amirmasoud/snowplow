---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>پروانه به شمع گفت: میسوزم زار</p></div>
<div class="m2"><p>شمعش گفتا که سوختن بادت کار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان میسوزی که میپرستی آتش</p></div>
<div class="m2"><p>آتش مپرست و کافری دست بدار</p></div></div>