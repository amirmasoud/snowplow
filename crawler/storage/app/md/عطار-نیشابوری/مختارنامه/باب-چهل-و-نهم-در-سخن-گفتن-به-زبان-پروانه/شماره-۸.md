---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>پروانه به شمع گفت: کیفر بردیم</p></div>
<div class="m2"><p>وز دستِ تو جان یک ره دیگر بردیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمعش گفتا: کنون مترس از آتش</p></div>
<div class="m2"><p>کان آتشِ سینه سوز با سر بردیم</p></div></div>