---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>پروانه به شمع گفت: غم بیشستی</p></div>
<div class="m2"><p>گر سوز من و تو را نه در پیشستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچند سرِ منت نبودست دمی</p></div>
<div class="m2"><p>ای کاش که یک دمت سرِ خویشستی</p></div></div>