---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>پروانه که شمع دلگشایش افتاد</p></div>
<div class="m2"><p>دلبستگی گره گشایش افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گردِ سرِ شمع پایکوبان میگشت</p></div>
<div class="m2"><p>جان بر سرش افشاند و به پایش افتاد</p></div></div>