---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>پروانه به شمع گفت: عیدِ تو خوش است</p></div>
<div class="m2"><p>قربانم کن که من یزیدِ تو خوش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم وعدهٔ تو خوش و وعید تو خوش است</p></div>
<div class="m2"><p>تو شاهدِ ما و ما شهیدِ تو خوش است</p></div></div>