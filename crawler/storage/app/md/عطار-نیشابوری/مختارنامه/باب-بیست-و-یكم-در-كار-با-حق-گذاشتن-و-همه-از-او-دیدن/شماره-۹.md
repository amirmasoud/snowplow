---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>خواهی که ز اضطرار و خواری برهی</p></div>
<div class="m2"><p>وز بیادبی و بیقراری برهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چند به خود کنی تصرّف در خویش</p></div>
<div class="m2"><p>گر کار بدو بازگذاری برهی</p></div></div>