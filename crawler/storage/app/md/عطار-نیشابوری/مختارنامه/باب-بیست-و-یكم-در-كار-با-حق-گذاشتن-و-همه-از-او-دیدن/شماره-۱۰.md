---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>جان محرم درگاه همی باید برد</p></div>
<div class="m2"><p>دل پر غم و پر آه همی باید برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خویش بدو راه نیابی هرگز</p></div>
<div class="m2"><p>هم زو سوی او راه همی باید برد</p></div></div>