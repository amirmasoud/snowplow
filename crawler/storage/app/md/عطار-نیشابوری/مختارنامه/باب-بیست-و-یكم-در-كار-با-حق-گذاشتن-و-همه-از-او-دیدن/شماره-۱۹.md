---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>گر دوزخی و اگر بهشتی امروز</p></div>
<div class="m2"><p>پیدا نشودخوبی و زشتی امروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دی رفت قلم آنچه نوشتی امروز</p></div>
<div class="m2"><p>فردا ببر آید آنچه کشتی امروز</p></div></div>