---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>دی حکم حیات با اجل رانده‌اند</p></div>
<div class="m2"><p>کس را چه خبر تا چه عمل رانده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلقان نروند تا بر ایشان نرود</p></div>
<div class="m2"><p>هر نیک و بدی که در ازل رانده‌اند</p></div></div>