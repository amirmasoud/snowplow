---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>عالم چو زکاف و نون توان آوردن</p></div>
<div class="m2"><p>پس شخص ز خاک و خون توان آوردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این نقش که هست چون برون آوردند</p></div>
<div class="m2"><p>صد نقش دگر برون توان آوردن</p></div></div>