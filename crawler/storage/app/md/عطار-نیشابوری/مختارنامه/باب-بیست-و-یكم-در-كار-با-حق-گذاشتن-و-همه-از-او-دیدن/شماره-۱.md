---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>آنجا که نه جان رسید ونه تن آنجا</p></div>
<div class="m2"><p>نه مرد رسد هرگز ونه زن آنجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر هر دو جهان زیر و زبر گردانم</p></div>
<div class="m2"><p>تا تو نرسانی نرسم من آنجا</p></div></div>