---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>گر تن گویم به خویشتن مینرود</p></div>
<div class="m2"><p>ور جان گویم به حکم تن مینرود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چند به اختیار خود خواهم رفت</p></div>
<div class="m2"><p>چون کار به اختیار من مینرود</p></div></div>