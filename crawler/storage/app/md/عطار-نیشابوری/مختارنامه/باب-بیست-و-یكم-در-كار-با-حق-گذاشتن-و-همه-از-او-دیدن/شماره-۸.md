---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>از خود نتوان راه معانی کردن</p></div>
<div class="m2"><p>آهنگ به ملک جاوانی کردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک قطرهای و هزار بحرت در پیش</p></div>
<div class="m2"><p>آخر چه کنی یا چه توانی کردن</p></div></div>