---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>گر در سفر یگانگی خواهی بود</p></div>
<div class="m2"><p>از جمع چرا کرانگی خواهی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور تو پر و بال خویش خواهی برّید</p></div>
<div class="m2"><p>ای بس که چو مرغ خانگی خواهی بود</p></div></div>