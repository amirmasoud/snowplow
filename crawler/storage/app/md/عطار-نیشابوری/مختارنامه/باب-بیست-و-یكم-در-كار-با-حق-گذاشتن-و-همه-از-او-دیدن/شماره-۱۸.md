---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>از کارِ قضا در تب و در تفت چه سود</p></div>
<div class="m2"><p>وز حکم ازل بیخور و بیخفت چه سود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کی به هزار لوح خوانم بر تو</p></div>
<div class="m2"><p>کز هرچه همی رود قلم رفت چه سود</p></div></div>