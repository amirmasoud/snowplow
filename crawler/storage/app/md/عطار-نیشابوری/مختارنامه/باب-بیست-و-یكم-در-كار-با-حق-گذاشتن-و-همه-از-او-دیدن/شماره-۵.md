---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>راهی به خودم که مینماید آخر</p></div>
<div class="m2"><p>بندی ز دلم که میگشاید آخر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون کار ز دست جمله کردند برون</p></div>
<div class="m2"><p>چه کار زدست ما برآید آخر</p></div></div>