---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>گل بر سر پای غرقهٔ خون زانست</p></div>
<div class="m2"><p>کاو روز دویی درین جهان مهمانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیکان در خون عجب نباشد دیدن</p></div>
<div class="m2"><p>در غنچه نگر که خونِ در پیکان است</p></div></div>