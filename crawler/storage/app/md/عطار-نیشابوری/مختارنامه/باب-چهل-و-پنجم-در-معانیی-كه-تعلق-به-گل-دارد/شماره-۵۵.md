---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>گل بین که به صد غنج و به صدناز رسید</p></div>
<div class="m2"><p>وز غنچهٔ سرکش به صد اعزاز رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رازی که صبا به گوش گل درمیگفت</p></div>
<div class="m2"><p>امروز به بلبل آن همه باز رسید</p></div></div>