---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>گل گفت: کسم هیچ فسون مینکند</p></div>
<div class="m2"><p>درمان من غرقه به خون مینکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین پای که من بر سر آتش دارم</p></div>
<div class="m2"><p>کس خار گلابگر برون مینکند</p></div></div>