---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>بلبل به سحر نعره‌زنان می‌آشفت</p></div>
<div class="m2"><p>وز غنچهٔ سر تیز حدیثی می‌گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون غنچه درون پوست زر داشت نهفت</p></div>
<div class="m2"><p>در پوست نگنجید و ز شادی بشکفت</p></div></div>