---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>یارب صفت رایحهٔ نسرین چیست</p></div>
<div class="m2"><p>این روح ریاحین چمن چندین چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر مصحف حسن نیست گلبرگ لطیف</p></div>
<div class="m2"><p>پس بر ورقش دَه آیهٔ زرّین چیست</p></div></div>