---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>تا پرده ز روی گلِ تر باز افتاد</p></div>
<div class="m2"><p>بلبل با گل همدم و همراز افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناآمده گویی به سرانجام رسید</p></div>
<div class="m2"><p>زین شیوه که کار غنچه آغاز افتاد</p></div></div>