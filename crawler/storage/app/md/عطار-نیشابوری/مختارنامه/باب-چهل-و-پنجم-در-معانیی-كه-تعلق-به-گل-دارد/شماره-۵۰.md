---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>با گل گفتم: چو چشم آن میدارم</p></div>
<div class="m2"><p>کز خندهٔ تو گشاده گردد کارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل گفت: چو ابر گرید آید زارم</p></div>
<div class="m2"><p>کز خندیدن ریختن آرد بارم</p></div></div>