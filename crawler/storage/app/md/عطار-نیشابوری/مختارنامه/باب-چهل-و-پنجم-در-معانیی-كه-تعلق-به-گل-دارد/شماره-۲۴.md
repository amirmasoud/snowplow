---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>گل گفت: نقاب برگشادیم و شدیم</p></div>
<div class="m2"><p>از دست به دسته اوفتادیم و شدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون عمر وفا نکرد هم بر سرِ پای</p></div>
<div class="m2"><p>ما دستهٔ خویش باز دادیم و شدیم</p></div></div>