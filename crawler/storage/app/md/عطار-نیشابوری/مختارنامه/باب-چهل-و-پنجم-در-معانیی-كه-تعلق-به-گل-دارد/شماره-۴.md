---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>از دست گلابگر گل عشوه پرست</p></div>
<div class="m2"><p>در پای آمد چنانکه بر خاک نشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل خون شد و از درد به بلبل میگفت:</p></div>
<div class="m2"><p>«آخر به چنین خون که بیالاید دست»</p></div></div>