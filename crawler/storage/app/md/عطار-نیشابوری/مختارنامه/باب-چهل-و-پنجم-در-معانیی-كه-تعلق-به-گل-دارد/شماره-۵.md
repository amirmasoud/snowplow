---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>با گل گفتم چو یوسفِ کنعانی</p></div>
<div class="m2"><p>در مصرِ چمن تُرا سزد سلطانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل گفت که من صد وَرَقم در هر باب</p></div>
<div class="m2"><p>خود یک وَرَقست این که تو بر میخوانی</p></div></div>