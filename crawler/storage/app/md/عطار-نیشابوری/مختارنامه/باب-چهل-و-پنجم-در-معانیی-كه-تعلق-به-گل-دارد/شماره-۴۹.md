---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>گل قصّهٔ بی خویشتنی خواهد گفت</p></div>
<div class="m2"><p>و افسانهٔ شیرین سخنی خواهد گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل کیست به طفلی دهنی پُرآتش</p></div>
<div class="m2"><p>موسی است مگر او«اَرِنِی» خواهد گفت</p></div></div>