---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>گرچه گل تر درآمدن سر تیز است</p></div>
<div class="m2"><p>چه سود که در وقت شدن خونریز است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تاروی نمود گل همی پشت بداد</p></div>
<div class="m2"><p>دردا که وصال گل فراق آمیز است</p></div></div>