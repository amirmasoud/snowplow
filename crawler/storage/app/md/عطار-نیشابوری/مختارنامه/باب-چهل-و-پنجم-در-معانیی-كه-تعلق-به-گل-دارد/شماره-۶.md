---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>بلبل که به عشق یک هم آواز نیافت</p></div>
<div class="m2"><p>همچون تو گلی شکفته در ناز نیافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل گرچه به حسن صد وَرَق داشت ولیک</p></div>
<div class="m2"><p>در هیچ وَرَق شرحِ رخت باز نیافت</p></div></div>