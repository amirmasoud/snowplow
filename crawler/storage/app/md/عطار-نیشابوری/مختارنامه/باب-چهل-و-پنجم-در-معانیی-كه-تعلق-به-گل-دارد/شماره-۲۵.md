---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>گل گفت: چنین که من کنون می‌آیم</p></div>
<div class="m2"><p>حقا که خلاصهٔ جنون می‌آیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاید اگر آغشتهٔ خون می‌آیم</p></div>
<div class="m2"><p>چون از رحمِ غنچه برون می‌آیم</p></div></div>