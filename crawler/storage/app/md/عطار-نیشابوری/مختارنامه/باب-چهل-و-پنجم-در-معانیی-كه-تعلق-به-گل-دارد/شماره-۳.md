---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>ابری که رخ باغ کنون خواهد شست</p></div>
<div class="m2"><p>گل را به گلاب بین که چون خواهد شست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل میآید با قدحی خون در دست</p></div>
<div class="m2"><p>از عمر مگر دست به خون خواهد شست</p></div></div>