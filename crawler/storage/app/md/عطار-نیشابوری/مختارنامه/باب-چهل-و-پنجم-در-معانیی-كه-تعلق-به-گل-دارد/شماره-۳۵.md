---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>بلبل به سحرگه غزلی تر میخواند</p></div>
<div class="m2"><p>تا ظن نبری کان غزل از بر میخواند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دفتر گل باز همی کرد ورق</p></div>
<div class="m2"><p>وز هر ورقش قصّهٔ دیگر میخواند</p></div></div>