---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>گل گفت که تا چشم گشادند مرا</p></div>
<div class="m2"><p>دیدم که برای مرگ زادند مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند که صد برگ نهادند مرا</p></div>
<div class="m2"><p>بی برگ به راه سر بدادند مرا</p></div></div>