---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>ساقی به صبوحی می ناب اندر ده</p></div>
<div class="m2"><p>مستان شبانه را شراب اندر ده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مستیم و خراب در خرابات فنا</p></div>
<div class="m2"><p>آوازه به عالم خراب اندر ده</p></div></div>