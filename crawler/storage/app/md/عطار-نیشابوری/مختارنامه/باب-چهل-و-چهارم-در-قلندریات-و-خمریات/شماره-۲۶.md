---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>تاکی شوم از زمانه پست ای ساقی</p></div>
<div class="m2"><p>زین پس من و آن زلف خوش است ای ساقی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلف تو به دست باتو دستی بزنیم</p></div>
<div class="m2"><p>زان پیش که بگذرد ز دست ای ساقی</p></div></div>