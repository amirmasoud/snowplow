---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>در عشق تو دین خویش نو خواهم کرد</p></div>
<div class="m2"><p>در ترسایی گفت و شنو خواهم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنّارِ چهارْ کرد برخواهم بست</p></div>
<div class="m2"><p>دستار به میخانه گرو خواهم کرد</p></div></div>