---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>از تفِّ دلم می به صباح ای ساقی</p></div>
<div class="m2"><p>جوشیده چو گشت شد مباح ای ساقی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مستی و مُقامری بسی بهتر از آنک</p></div>
<div class="m2"><p>بر روی و ریا کنی صلاح ای ساقی</p></div></div>