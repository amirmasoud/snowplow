---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>وقت است که در بر آشنائی بزنیم</p></div>
<div class="m2"><p>تا بر گل و سبزه تکیه جایی بزنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان پیش که دست و پا فرو بندد مرگ</p></div>
<div class="m2"><p>آخر کم ازانکه دست و پائی بزنیم</p></div></div>