---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>سلطان، تو، به می دهندگی ای ساقی</p></div>
<div class="m2"><p>ما بسته میان به بندگی ای ساقی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما مردهٔ محنتیم و امروز به تست</p></div>
<div class="m2"><p>جان را ز شراب،‌زندگی ای ساقی</p></div></div>