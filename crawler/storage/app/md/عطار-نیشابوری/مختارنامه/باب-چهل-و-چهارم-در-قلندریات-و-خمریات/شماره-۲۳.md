---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>دل گشت ز معصیت سیاه ای ساقی</p></div>
<div class="m2"><p>فریاد زشومی گناه ای ساقی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برگیر به سوی توبه راه ای ساقی</p></div>
<div class="m2"><p>کز عمر بسی نماند آه ای ساقی</p></div></div>