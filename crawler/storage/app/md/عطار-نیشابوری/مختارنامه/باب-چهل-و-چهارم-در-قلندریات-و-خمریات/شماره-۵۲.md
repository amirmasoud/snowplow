---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>گل جلوه همی کند به بستان ای دوست</p></div>
<div class="m2"><p>دریاب چنین وقت گلستان ای دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنشین چو ز هر چه هست برخواهی خاست</p></div>
<div class="m2"><p>روزی دو ز عیشْ داد بستان ای دوست</p></div></div>