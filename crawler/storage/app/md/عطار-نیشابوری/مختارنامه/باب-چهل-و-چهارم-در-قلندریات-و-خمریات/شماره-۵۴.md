---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>آن لحظه که از اجل گریزان گردیم</p></div>
<div class="m2"><p>چون برگ ز شاخ عمر ریزان گردیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عالم ز نشاط دل به غربال کنید</p></div>
<div class="m2"><p>زان پیش که خاکِ خاک بیزان گردیم</p></div></div>