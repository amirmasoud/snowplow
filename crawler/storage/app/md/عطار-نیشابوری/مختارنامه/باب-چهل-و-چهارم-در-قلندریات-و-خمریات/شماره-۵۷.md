---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>صبح از پس کوه روی بنمود ای دوست</p></div>
<div class="m2"><p>خوش باش و بدان که بودنی بود ای دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر سیم که داری به زیان آر که عمر</p></div>
<div class="m2"><p>چون درگذرد نداردت سود ای دوست</p></div></div>