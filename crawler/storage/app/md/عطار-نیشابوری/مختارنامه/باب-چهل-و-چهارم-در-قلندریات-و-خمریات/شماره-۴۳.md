---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>مهتاب به نور دامن شب بشکافت</p></div>
<div class="m2"><p>میخور که دمی خوشتر ازین نتوان یافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش باش و بیندیش که مهتاب بسی</p></div>
<div class="m2"><p>خوش بر سر خاک یک به یک خواهد تافت</p></div></div>