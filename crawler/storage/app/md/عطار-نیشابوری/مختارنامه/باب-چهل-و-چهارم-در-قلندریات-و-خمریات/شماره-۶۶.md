---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>مائیم و میی و مطربی مشکین خال</p></div>
<div class="m2"><p>بی هجر میسَّر شده ایام وصال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با سیمبری نشسته در باد شمال</p></div>
<div class="m2"><p>زین آب حرام خون خود کرده حلال</p></div></div>