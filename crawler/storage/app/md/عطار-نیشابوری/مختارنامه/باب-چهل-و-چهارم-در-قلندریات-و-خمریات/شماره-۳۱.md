---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>تا چند ازین بی خبران ای ساقی</p></div>
<div class="m2"><p>دل کرده سبک، کیسه گران ای ساقی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کی ز خصومت خران ای ساقی</p></div>
<div class="m2"><p>بگذر ز جهانِ گذران ای ساقی</p></div></div>