---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>بر روی گل از ابر گلاب است هنوز</p></div>
<div class="m2"><p>در طبع دلم میل شراب است هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خواب مشو چه جای خواب است هنوز</p></div>
<div class="m2"><p>جانا! می ده که ماهتاب است هنوز</p></div></div>