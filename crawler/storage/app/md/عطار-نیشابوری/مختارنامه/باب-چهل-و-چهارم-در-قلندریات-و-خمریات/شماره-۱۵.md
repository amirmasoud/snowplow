---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>از بس که دلم بسوخت زین کاردرشت</p></div>
<div class="m2"><p>روزی صد ره به دست خود خود را کشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جامی دو، می مغانه خواه از زردشت</p></div>
<div class="m2"><p>تا باز کنم قبای آدم از پشت</p></div></div>