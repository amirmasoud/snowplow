---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>خواهی که غم از دل تو یک دم بشود</p></div>
<div class="m2"><p>میخور که چو می به دل رسد غم بشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگشای سر زلف بتان، بند ز بند،</p></div>
<div class="m2"><p>زان پیش که بند بندت از هم بشود</p></div></div>