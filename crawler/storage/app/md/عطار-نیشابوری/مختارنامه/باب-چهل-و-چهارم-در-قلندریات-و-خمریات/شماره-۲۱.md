---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>شمع است و شراب و ماهتاب ای ساقی</p></div>
<div class="m2"><p>شاهد و شرابْ نیم خواب ای ساقی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خام مگو وین دل پر آتش نیز</p></div>
<div class="m2"><p>بر باد مده بیار آب ای ساقی</p></div></div>