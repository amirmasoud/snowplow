---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>سودای توام بیدل و دین میخواهد</p></div>
<div class="m2"><p>خمّار و خرابات نشین میخواهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من میخواهم که عاقلی باشم چُست</p></div>
<div class="m2"><p>دیوانگی توام چنین میخواهد</p></div></div>