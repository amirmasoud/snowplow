---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>ای ترک قلندری شرابی در ده</p></div>
<div class="m2"><p>جامی دو، می، از بهر خرابی در ده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وین بستهٔ حرص عالم فانی را</p></div>
<div class="m2"><p>زان پیش که خاک گردد آبی در ده</p></div></div>