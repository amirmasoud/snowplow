---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>ای هم نفسان فعل اجل میدانید</p></div>
<div class="m2"><p>روزی دو سه داد خود ز خود بستانید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیزید و نشینید که خود بعد از این</p></div>
<div class="m2"><p>خواهید به هم نشستن ونتوانید</p></div></div>