---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>چون گل بشکفت ساعتی برخیزیم</p></div>
<div class="m2"><p>بر شادی می، ز دست غم بگریزیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باشد که بهار دیگر ای هم نفسان</p></div>
<div class="m2"><p>گل میریزد ز بار و ما میریزیم</p></div></div>