---
title: >-
    شمارهٔ ۷۴
---
# شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>چون جلوهٔ گل ز گلستان پیدا شد</p></div>
<div class="m2"><p>بلبل به سخن درآمد و شیدا شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در جام بلور کن می لعل که باغ</p></div>
<div class="m2"><p>از مروارید ابر چون مینا شد</p></div></div>