---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>ما خرقهٔ رسم، از سرانداخته‌ایم</p></div>
<div class="m2"><p>سر را، بَدَلِ خرقه، درانداخته‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چیز که سدِّ راه ما خواهد بود</p></div>
<div class="m2"><p>گر خود همه جان است برانداخته‌ایم</p></div></div>