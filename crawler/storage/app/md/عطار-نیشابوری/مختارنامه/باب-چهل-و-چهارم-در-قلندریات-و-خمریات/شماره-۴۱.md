---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>گر سبز خطی است، گوشهای خالی گیر</p></div>
<div class="m2"><p>بر مفرش سبزه، رو، کَمِ قالی گیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندیشهٔ حال زیر خاکت تا کی</p></div>
<div class="m2"><p>عمر تو چو باد میرود حالی گیر</p></div></div>