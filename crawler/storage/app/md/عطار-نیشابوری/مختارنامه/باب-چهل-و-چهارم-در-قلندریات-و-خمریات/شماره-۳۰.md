---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>پر کن شکمی به اشتها ای ساقی</p></div>
<div class="m2"><p>از قافِ قرابه تابهها ای ساقی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون شد دل من به ابتدا باده بیار</p></div>
<div class="m2"><p>تاتوبه کنم به انتها ای ساقی</p></div></div>