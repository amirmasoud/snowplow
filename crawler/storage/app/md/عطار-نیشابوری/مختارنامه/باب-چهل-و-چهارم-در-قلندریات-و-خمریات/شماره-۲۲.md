---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>همچون من و تو علی الیقین ای ساقی</p></div>
<div class="m2"><p>بسیار فرو خورد زمین ای ساقی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تاکی کنی اندیشه ازین ای ساقی</p></div>
<div class="m2"><p>العیش! که عمر رفت هین ای ساقی</p></div></div>