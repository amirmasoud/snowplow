---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>می خور که فلک بهر هلاک من و تو</p></div>
<div class="m2"><p>قصدی دارد به جان پاک من و تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سبزه نشین که عمر بسیار نماند</p></div>
<div class="m2"><p>تا سبزه برون دمد ز خاک من و تو</p></div></div>