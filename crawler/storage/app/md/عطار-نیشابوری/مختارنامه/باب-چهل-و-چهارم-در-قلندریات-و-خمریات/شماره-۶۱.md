---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>مهتاب افتاد در گلستان امشب</p></div>
<div class="m2"><p>گل روی نمود سوی بستان امشب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ده می گلرنگ که مینتوان خفت</p></div>
<div class="m2"><p>از مشغلهٔ هزار دستان امشب</p></div></div>