---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>جان پیش تو بر میان کمر خواهم داشت</p></div>
<div class="m2"><p>هر دم به تو شوق بیشتر خواهم داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من خاک توام دایم و خاکم بر سر</p></div>
<div class="m2"><p>گر سر ز سر خاک تو بر خواهم داشت</p></div></div>