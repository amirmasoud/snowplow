---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>گه پیش در تو در سجود آمدهام</p></div>
<div class="m2"><p>گه بر سر آتشت چو عود آمدهام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مستی مرا امید هشیاری نیست</p></div>
<div class="m2"><p>کز عشق تو مست در وجود آمدهام</p></div></div>