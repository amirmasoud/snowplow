---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>تا جاندارم گردِ تو میخواهم تاخت</p></div>
<div class="m2"><p>میخواهم سوخت و نیز میخواهم ساخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو شاد بزی که نرد عشقت شب و روز</p></div>
<div class="m2"><p>تا من باشم با تو همی خواهم باخت</p></div></div>