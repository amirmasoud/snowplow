---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>چون مرغ دلم به دام هستی در شد</p></div>
<div class="m2"><p>چندانکه طپید بند محکم تر شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وز بی صبری و بی قراری جانم</p></div>
<div class="m2"><p>از بس که بسوخت جمله خاکستر شد</p></div></div>