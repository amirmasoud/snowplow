---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>دیرست که در کوی تو دارم گذری</p></div>
<div class="m2"><p>گر وقت آمد به سوی من کُن نظری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور در خورِ کشتنم مکش دردِ سری</p></div>
<div class="m2"><p>در پای خودم کُش نه به دستِ دگری</p></div></div>