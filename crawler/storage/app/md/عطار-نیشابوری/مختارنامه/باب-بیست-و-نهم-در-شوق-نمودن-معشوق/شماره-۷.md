---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>من این دل بسته را کجا خواهم برد</p></div>
<div class="m2"><p>ور صاف مرا نیست کجا خواهم دُرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نوش کنم هزار دریا هر روز</p></div>
<div class="m2"><p>حقا که ز دَردِ تشنگی خواهم مرد</p></div></div>