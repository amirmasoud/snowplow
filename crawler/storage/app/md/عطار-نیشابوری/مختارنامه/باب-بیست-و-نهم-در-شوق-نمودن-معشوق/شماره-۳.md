---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>نه دل دارم نه چشم ره بین چکنم</p></div>
<div class="m2"><p>درمانده نه دنیی و نه دین چکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه سوی تو راهست و نه سوی دگران</p></div>
<div class="m2"><p>سیلی است بر آتش من مسکین چکنم</p></div></div>