---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>قومی که به هم میبنشینند ترا</p></div>
<div class="m2"><p>بر هر دو جهان میبگزینند ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نادیده ترا جان و دل از دست بشد</p></div>
<div class="m2"><p>چون پای آرند اگر ببینند ترا</p></div></div>