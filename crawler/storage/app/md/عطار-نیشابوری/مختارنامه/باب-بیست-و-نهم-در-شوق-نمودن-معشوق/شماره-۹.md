---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>نه بستهٔ پیوند توانم بودن</p></div>
<div class="m2"><p>نه رنج کش بند توانم بودن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمری است که بی قرارتر از فلکم</p></div>
<div class="m2"><p>ساکن چو زمین چند توانم بودن</p></div></div>