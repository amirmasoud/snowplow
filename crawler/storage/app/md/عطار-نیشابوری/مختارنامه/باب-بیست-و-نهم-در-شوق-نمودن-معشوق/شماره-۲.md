---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>جز تشنگی تو هوسم مینکند</p></div>
<div class="m2"><p>میمیرم و سیرآب کسم مینکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه حیله کنم که هرنفس صد دریا</p></div>
<div class="m2"><p>مینوشم و میخورم بسم مینکند</p></div></div>