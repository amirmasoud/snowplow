---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>چون من به تو در همه جهانم زنده</p></div>
<div class="m2"><p>یک لحظه مباد بی تو جانم زنده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی زحمت تن با تو دلم را نفسی است</p></div>
<div class="m2"><p>گر زندهام امروز بدانم زنده</p></div></div>