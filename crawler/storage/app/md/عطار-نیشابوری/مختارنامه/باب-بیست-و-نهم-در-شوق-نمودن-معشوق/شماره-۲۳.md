---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>کو پای که از دست تو بگریختمی</p></div>
<div class="m2"><p>کو دست که در پای تو آویختمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای کاش هزار جانمی تا هر دم</p></div>
<div class="m2"><p>در خاک قدمهای تو میریختمی</p></div></div>