---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>از عشقِ تو در جهان عَلَم خواهم شد</p></div>
<div class="m2"><p>وز شوق به فرق چون قلم خواهم شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از عشقِ تو مست در وجود آمدهام</p></div>
<div class="m2"><p>وز شوق تو مست با عَدَم خواهم شد</p></div></div>