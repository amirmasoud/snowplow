---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>ای لعل توام به حکم ایمان داده</p></div>
<div class="m2"><p>کفرم به سر زلف پریشان داده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو در پس پرده با من و من بی تو</p></div>
<div class="m2"><p>از پرده برون زشوق تو جان داده</p></div></div>