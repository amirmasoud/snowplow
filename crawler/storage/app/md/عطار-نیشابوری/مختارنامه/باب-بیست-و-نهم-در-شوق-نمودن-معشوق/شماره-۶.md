---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>چون کس بنداند آنچه من دانم ازو</p></div>
<div class="m2"><p>خواهم که کنم حیله و نتوانم ازو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد گونه بلا اگر به رویم بارد</p></div>
<div class="m2"><p>آن روی ندارم که بگردانم ازو</p></div></div>