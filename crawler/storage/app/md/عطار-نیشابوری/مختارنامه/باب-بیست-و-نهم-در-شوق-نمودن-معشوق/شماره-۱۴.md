---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>در پرده درونِ دل ریشت بینم</p></div>
<div class="m2"><p>از پرده برون نشسته بیشت بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر روز هزار بار بیشت بینم</p></div>
<div class="m2"><p>تا کی بُود آن نَفَس که خویشت بینم</p></div></div>