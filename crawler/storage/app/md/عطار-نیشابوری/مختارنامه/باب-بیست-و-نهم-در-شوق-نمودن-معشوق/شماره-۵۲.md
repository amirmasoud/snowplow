---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>در عشق تو نیم ذرّه سرگردانی</p></div>
<div class="m2"><p>خوشتر ز هزار منصب سلطانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان میآیم زیر و زبر میدانی</p></div>
<div class="m2"><p>تا بیشترم زیر و زبر گردانی</p></div></div>