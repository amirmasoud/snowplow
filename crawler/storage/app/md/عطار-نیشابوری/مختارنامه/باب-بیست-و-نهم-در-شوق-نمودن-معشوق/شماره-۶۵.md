---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>در کوی تو چون میگذرم، اینت عجب!</p></div>
<div class="m2"><p>وز سوی تو چون مینگرم، اینت عجب!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر زهرهٔ آن بود که یاد تو کنم</p></div>
<div class="m2"><p>گر بر نپرد دل از برم، اینت عجب!</p></div></div>