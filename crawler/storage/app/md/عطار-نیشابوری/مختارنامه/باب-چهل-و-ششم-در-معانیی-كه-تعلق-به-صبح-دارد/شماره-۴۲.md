---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>دوش آمد و گفت: چند جانت سوزم</p></div>
<div class="m2"><p>وقت است که امشبیت جان افروزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دردا که هنوز در دهن داشت سخن</p></div>
<div class="m2"><p>خود صبح برآمد و فرو شد روزم</p></div></div>