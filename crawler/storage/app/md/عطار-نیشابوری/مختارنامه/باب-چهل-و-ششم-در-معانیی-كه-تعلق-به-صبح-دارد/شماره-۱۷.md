---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>ای صبح هنوز ماهتاب است، مخند</p></div>
<div class="m2"><p>در شیشهٔ ما یقین شراب است، مخند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در تیغ نهاده‌ای قلم میخندی</p></div>
<div class="m2"><p>چون جای تو تیغ آفتاب است، مخند</p></div></div>