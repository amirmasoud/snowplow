---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>ای صبح! اگر طلوع خواهی کردن</p></div>
<div class="m2"><p>در کشتن من شروع خواهی کردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حقا که اگر رنجه شوی ز آه دلم</p></div>
<div class="m2"><p>از نیمهٔ ره رجوع خواهی کردن</p></div></div>