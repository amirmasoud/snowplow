---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>گر صبح شبی واقعهٔ من دیدی</p></div>
<div class="m2"><p>در پرده شدی پردهٔ من نَدْریدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور دم نزدی یک سخنم نشنیدی</p></div>
<div class="m2"><p>تا حشر دمش فرو شدی نَدْمیدی</p></div></div>