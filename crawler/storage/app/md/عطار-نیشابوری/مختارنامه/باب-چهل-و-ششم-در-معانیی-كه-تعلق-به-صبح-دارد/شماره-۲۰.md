---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>تا کی ز شبِ دراز گریان گردم</p></div>
<div class="m2"><p>در تاریکی چو زلفِ جانان گردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر زنگی شب، چو صبح، خندان گردد</p></div>
<div class="m2"><p>من چون زنگی سپید دندان گردم</p></div></div>