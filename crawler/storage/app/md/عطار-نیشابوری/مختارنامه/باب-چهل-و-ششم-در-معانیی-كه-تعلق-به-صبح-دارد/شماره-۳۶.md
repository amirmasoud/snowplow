---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>امشب که مرا نه تاب و نه تب بودست</p></div>
<div class="m2"><p>با یار به هم جامِ لبالب بودست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای صبح! در آن کوش که امشب ندمی</p></div>
<div class="m2"><p>زیرا که مرا روز خود امشب بودست</p></div></div>