---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>جانم به مرادِ دل رسیدست امشب</p></div>
<div class="m2"><p>بر سیم بری سری کشیدست امشب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای صبح! مکن مرا مگریان و مخند</p></div>
<div class="m2"><p>کآرام دل من آرمیدست امشب</p></div></div>