---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>امشب ز دمیدن تو ترسم ای صبح</p></div>
<div class="m2"><p>وز تیغ کشیدن تو ترسم ای صبح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون در پس پرده یار با ما بنشست</p></div>
<div class="m2"><p>از پرده دریدن تو ترسم ای صبح</p></div></div>