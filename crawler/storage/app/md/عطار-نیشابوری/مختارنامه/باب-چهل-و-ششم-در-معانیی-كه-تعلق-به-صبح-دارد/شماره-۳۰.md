---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>ای صبح!‌اگر تو یاریی خواهی کرد</p></div>
<div class="m2"><p>آنست که پرده داریی خواهی کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من خود ز سیه گری شب میترسم</p></div>
<div class="m2"><p>تو نیز سفیدکاریی خواهی کرد</p></div></div>