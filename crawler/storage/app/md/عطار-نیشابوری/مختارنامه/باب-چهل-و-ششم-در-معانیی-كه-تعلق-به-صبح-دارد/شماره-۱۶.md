---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>ای صبح اگر عزیمتِ خنده کنی</p></div>
<div class="m2"><p>حالم چو جمالِ خویش فرخنده کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دایم چو تویی همدم عیسی در دم</p></div>
<div class="m2"><p>تا بوکه دل مردهٔ من زنده کنی</p></div></div>