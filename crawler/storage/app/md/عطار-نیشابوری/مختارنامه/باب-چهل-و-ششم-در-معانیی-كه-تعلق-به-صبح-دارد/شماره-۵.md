---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>ای شب مزن از ستاره چندینی جوش</p></div>
<div class="m2"><p>خفاش بسیست نور و ظلمت در پوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وی صبح گر آفتاب داری در دل</p></div>
<div class="m2"><p>چون همنفسِ تو کاذب افتاد خموش</p></div></div>