---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>ای صبح چو امشب تو ز اهلِ حَرَمی</p></div>
<div class="m2"><p>هندوی توام، مباش ترکی عجمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنهار مَدَم که در دل آتش دارم</p></div>
<div class="m2"><p>آتش گردم گر بدمد صبح دمی</p></div></div>