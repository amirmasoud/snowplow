---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>چون لوح دل از دو کون بستردم من</p></div>
<div class="m2"><p>دو کون به زیر پای بسپردم من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای بس سخنی را که سرم کردی گوی</p></div>
<div class="m2"><p>آمد به گلویم و فرو بردم من</p></div></div>