---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>تا چند زنی منادی، ای سر که فروش!</p></div>
<div class="m2"><p>بیزحمت لب شراب تحقیق بنوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چند زنی ای زن برخاسته جوش</p></div>
<div class="m2"><p>در ماتم این حدیث بنشین و خموش!</p></div></div>