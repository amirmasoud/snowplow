---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>خود را به طریق چاره میباید کرد</p></div>
<div class="m2"><p>وز خلق جهان کناره میباید کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم دل پر خون خموش میباید بود</p></div>
<div class="m2"><p>هم لب بر هم نظاره میباید کرد</p></div></div>