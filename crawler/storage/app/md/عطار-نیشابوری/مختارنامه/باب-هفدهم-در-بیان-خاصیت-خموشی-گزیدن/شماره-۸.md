---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>ای دل شب و روز چند جوشی، بنشین</p></div>
<div class="m2"><p>تا چند چخی و چند کوشی، بنشین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون راز تو در گفت نخواهد آمد</p></div>
<div class="m2"><p>در قعر دلت بِهّ ار بپوشی، بنشین</p></div></div>