---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>تا چشم ز دیدارِ جهان در بستیم</p></div>
<div class="m2"><p>وز بهرِ گریختن میان دربستیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوردیم غمِ عشق و فغان دربستیم</p></div>
<div class="m2"><p>چون اهل ندیدیم زبان دربستیم</p></div></div>