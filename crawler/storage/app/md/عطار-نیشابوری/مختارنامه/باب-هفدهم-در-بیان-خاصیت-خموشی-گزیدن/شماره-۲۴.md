---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>گر خواهی تو که وقت خود داری گوش</p></div>
<div class="m2"><p>دم در کشی و به خویش بازآری هوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر هر دو جهان چو بحر آید در جوش</p></div>
<div class="m2"><p>تو یافه مگو ز دور بنشین و خموش!</p></div></div>