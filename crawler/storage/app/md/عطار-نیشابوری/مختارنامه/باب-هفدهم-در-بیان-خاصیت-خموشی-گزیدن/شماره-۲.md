---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>فرّخ دل آن که مُرد حیران و نگفت</p></div>
<div class="m2"><p>صد واقعه داشت، کرد پنهان و نگفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دردِ تو نگاه داشت در جان و نگفت</p></div>
<div class="m2"><p>اندوه تو کرد ورد پایان و نگفت</p></div></div>