---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>گر نام ونشان من توانستی بود</p></div>
<div class="m2"><p>کس را غم جان من توانستی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای کاش که اسرار دل پر خونم</p></div>
<div class="m2"><p>مسمار زبان من توانستی بود</p></div></div>