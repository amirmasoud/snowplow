---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>دل در پی راز عشق، پویان میدار</p></div>
<div class="m2"><p>جان میکن و راز عشق، در جان میدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سِرّی که سر اندر سرِ آن باختهای</p></div>
<div class="m2"><p>چون پیدا شد ز خویش پنهان میدار</p></div></div>