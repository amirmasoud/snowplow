---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>پیش از من و تو پیر و جوانی بودست</p></div>
<div class="m2"><p>اندوهگنی و شادمانی بودست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جرعه مفکن بر دهن خاک که خاک</p></div>
<div class="m2"><p>خاک دهنی چو نقل دانی بودست</p></div></div>