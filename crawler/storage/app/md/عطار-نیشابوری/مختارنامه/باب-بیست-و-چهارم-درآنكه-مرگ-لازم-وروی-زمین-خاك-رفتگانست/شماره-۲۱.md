---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>چون رفت ز جسم جوهر روشن ما</p></div>
<div class="m2"><p>از خار دریغ پر شود گلشن ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر ما بروند و هیچ کس نشناسد</p></div>
<div class="m2"><p>تا زیر زمین چه میرود بر تن ما</p></div></div>