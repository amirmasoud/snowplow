---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>بر فرق تو هر حادثه تیغی دگرست</p></div>
<div class="m2"><p>در پیش تو هرواقعه میغی دگرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر برگ و گیاهی که برون رُست ز خاک</p></div>
<div class="m2"><p>از هر دل غم گشته دریغی دگرست</p></div></div>