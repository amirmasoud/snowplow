---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>دل کز سرِ عمر سرنگون بر میخاست</p></div>
<div class="m2"><p>از هر مویش چشمه خون بر میخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این بلبل روح بر سرِ گلبنِ جسم</p></div>
<div class="m2"><p>از بهرِ چه مینشست چون بر میخاست</p></div></div>