---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>خلقی که درین جهان پدیدار شدند</p></div>
<div class="m2"><p>در خانه به عاقبت گرفتار شدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندین غم خود مخور که همچون من و تو</p></div>
<div class="m2"><p>بسیار درآمدند و بسیار شدند</p></div></div>