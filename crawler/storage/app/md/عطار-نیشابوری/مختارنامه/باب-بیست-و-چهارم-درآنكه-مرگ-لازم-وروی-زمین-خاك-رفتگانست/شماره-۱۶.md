---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>دو چشم ز اشک خیره میباید کرد</p></div>
<div class="m2"><p>از بس که غمم ذخیره میباید کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چند به آب پاک روشن داریم</p></div>
<div class="m2"><p>روئی که به خاک تیره میباید کرد</p></div></div>