---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>خلقند به خاک بیعدد آورده</p></div>
<div class="m2"><p>از حکم ازل رای ابد آورده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای بس که بگردد در و دیوار فلک</p></div>
<div class="m2"><p>ما روی به دیوار لحد آورده</p></div></div>