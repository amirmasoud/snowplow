---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>دی خاک همی نمود با من تندی</p></div>
<div class="m2"><p>میگفت که زیر قدمم افکندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من همچو تو بودهام، تو خوش بیخبری</p></div>
<div class="m2"><p>زودا که تو نیز این کمر بربندی</p></div></div>