---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>تا کی به نظارهٔ جهان خواهی زیست</p></div>
<div class="m2"><p>فارغ ز طلسم جسم و جان خواهی زیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک ذرّه به مرگِ خویشتن برگت نیست</p></div>
<div class="m2"><p>پنداشتهای که جاودان خواهی زیست</p></div></div>