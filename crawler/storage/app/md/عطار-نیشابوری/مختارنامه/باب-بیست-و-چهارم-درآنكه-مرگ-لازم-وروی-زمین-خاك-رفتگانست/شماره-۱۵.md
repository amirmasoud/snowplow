---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>قومی که به خاک مرگ سر بازنهند</p></div>
<div class="m2"><p>تا حشر ز قال و قیل خود باز رهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کی گوئی کسی خبر باز نداد</p></div>
<div class="m2"><p>چون بیخبرند از چه خبر باز دهند</p></div></div>