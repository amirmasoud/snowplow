---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>هر رنگ که ممکن است آمیخته گیر</p></div>
<div class="m2"><p>هر فتنه که ساکن است انگیخته گیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وین روی چو ماه آسمانت بدریغ</p></div>
<div class="m2"><p>از صرصر مرگ در زمین ریخته گیر</p></div></div>