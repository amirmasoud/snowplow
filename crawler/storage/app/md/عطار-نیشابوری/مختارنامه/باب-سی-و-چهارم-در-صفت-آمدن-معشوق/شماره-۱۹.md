---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>دوش آمد و گفت: اگر وفا خواهی کرد</p></div>
<div class="m2"><p>دردِ همه ساله را دوا خواهی کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه سود طلب نه مایه با هیچ بساز</p></div>
<div class="m2"><p>گر کار به سرمایهٔ ما خواهی کرد</p></div></div>