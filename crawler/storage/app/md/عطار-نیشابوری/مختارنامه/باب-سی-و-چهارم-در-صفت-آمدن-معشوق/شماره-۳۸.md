---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>دی گفت: حجب ز پیش برنگرفتم</p></div>
<div class="m2"><p>دو کون ز راهِ خویش برنگرفتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد عالمِ تشویر پدیدار آمد</p></div>
<div class="m2"><p>یک پرده کنون ز پیش برنگرفتم</p></div></div>