---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>دی گفت: کجا شدی،‌چنین میباید</p></div>
<div class="m2"><p>از دوست جدا شدی، چنین میباید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی دو ز بهرِ آنکه دور افتادی</p></div>
<div class="m2"><p>بیگانه زما شدی، چنین میباید</p></div></div>