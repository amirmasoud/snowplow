---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>دوش آمد و گفت: گِردِ اِعزاز مگرد</p></div>
<div class="m2"><p>خواری طلب و دگر سرافراز مگرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میدان که تو سایهٔ منی خوش میباش</p></div>
<div class="m2"><p>هرجا که روم از پی من باز مگرد</p></div></div>