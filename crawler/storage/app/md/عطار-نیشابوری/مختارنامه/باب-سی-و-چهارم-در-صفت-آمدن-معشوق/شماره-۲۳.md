---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>آن بت که دلم عاشقِ جانبازش بود</p></div>
<div class="m2"><p>جان شیفتهٔ زلفِ سرافرازش بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که چو آید برود صد نازش</p></div>
<div class="m2"><p>دوش آمد و آنچه رفت هم نازش بود</p></div></div>