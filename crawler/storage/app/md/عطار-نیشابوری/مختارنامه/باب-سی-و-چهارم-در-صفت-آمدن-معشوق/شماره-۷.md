---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>دوش آمد و گفت: خانهٔ ما آخر</p></div>
<div class="m2"><p>روشن بکن ای یگانهٔ ما آخر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وقت است که دست درکش آری با ما</p></div>
<div class="m2"><p>تا کِی گوئی فسانهٔ ما آخر</p></div></div>