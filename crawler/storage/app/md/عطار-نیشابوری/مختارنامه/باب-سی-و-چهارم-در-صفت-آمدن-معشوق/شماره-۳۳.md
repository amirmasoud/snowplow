---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>امشب ز پگاهی به خروش آمده‌ای</p></div>
<div class="m2"><p>چونست که مست‌تر ز دوش آمده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بازارت نمی‌رود کار مگر</p></div>
<div class="m2"><p>زانست که در خانه فروش آمده‌ای</p></div></div>