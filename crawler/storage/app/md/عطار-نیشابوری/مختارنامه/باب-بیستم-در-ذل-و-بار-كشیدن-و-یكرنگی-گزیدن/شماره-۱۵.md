---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>خود را به محال خود دچار آیی تو</p></div>
<div class="m2"><p>چون خاک رهی چه باد پیمایی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کم کاستی تو باشد ای بی حاصل</p></div>
<div class="m2"><p>هرچیز که از خویش درافزایی تو</p></div></div>