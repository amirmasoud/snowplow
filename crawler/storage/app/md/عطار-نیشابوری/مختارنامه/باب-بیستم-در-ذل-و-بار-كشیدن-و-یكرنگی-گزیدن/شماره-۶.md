---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>هر دل که طلب کند چنین یاری را</p></div>
<div class="m2"><p>مردانه به جان کشد چنین باری را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردی باید شگرف تا همچو فلک</p></div>
<div class="m2"><p>بر طاق نهد جامه چنین کاری را</p></div></div>