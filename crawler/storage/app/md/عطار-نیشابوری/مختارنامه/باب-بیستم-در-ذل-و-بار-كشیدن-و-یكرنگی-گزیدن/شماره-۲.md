---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>گر جان ببرد عشق توام جان آنست</p></div>
<div class="m2"><p>ور درد دهد جملهٔدرمان آنست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر ناکامی که باشد این طایفه را</p></div>
<div class="m2"><p>میدان به یقین که کام ایشان آنست</p></div></div>