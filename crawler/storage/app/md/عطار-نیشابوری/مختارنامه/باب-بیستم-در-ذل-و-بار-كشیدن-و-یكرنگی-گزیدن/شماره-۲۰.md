---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>هر دل که تمام از سردردی برخاست</p></div>
<div class="m2"><p>هستیش ز پیش همچو گردی برخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنگاه اگر مخنثّی در همه عمر</p></div>
<div class="m2"><p>در سایهٔ او نشست مردی برخاست</p></div></div>