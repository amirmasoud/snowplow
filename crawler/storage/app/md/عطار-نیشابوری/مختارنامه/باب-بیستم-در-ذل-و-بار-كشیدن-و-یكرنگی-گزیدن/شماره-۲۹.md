---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>چون بحر،‌دلی هزار جوش است مرا</p></div>
<div class="m2"><p>تن در غم عشق، سخت کوش است مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر زهد کنم زبان خموش است مرا</p></div>
<div class="m2"><p>کاین زهد نه از بهر فروش است مرا</p></div></div>