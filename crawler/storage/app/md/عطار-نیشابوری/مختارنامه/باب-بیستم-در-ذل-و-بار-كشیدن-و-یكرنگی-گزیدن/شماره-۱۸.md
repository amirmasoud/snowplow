---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>چون کرد شراب شرک و غفلت مستت</p></div>
<div class="m2"><p>عالم عالم، غرور در پیوستت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندان که مپرس سرفرازی هستت</p></div>
<div class="m2"><p>تاتن بنیوفتی که گیرد دستت</p></div></div>