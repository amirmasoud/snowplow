---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>ای در رهِ دین و کارِ کفر آمده سُست</p></div>
<div class="m2"><p>نه مؤمن اصلی و نه کافر بدرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر روی و ریا طاعت تو معصیت است</p></div>
<div class="m2"><p>با مفسدِ فاش باش یا زاهدِ رُست</p></div></div>