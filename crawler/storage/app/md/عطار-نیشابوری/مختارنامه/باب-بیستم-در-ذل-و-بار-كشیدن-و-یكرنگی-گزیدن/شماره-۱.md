---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>جان سوخته سرفکنده میباید بود</p></div>
<div class="m2"><p>چون شمع، به سوز، زنده میبایدبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کارت به مراد این خدائی باشد</p></div>
<div class="m2"><p>ناکامی کش که بنده میباید بود</p></div></div>