---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>خود را چو زخواب و خور نمیداری باز</p></div>
<div class="m2"><p>پس چه تو، چه آن ستور، در پردهٔ راز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر ز وجود خویشتن شرمت نیست</p></div>
<div class="m2"><p>معشوق تو بیدارو تو خوش خفته به ناز</p></div></div>