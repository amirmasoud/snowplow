---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>ای تن دل ناموافقت میداند</p></div>
<div class="m2"><p>وز روی و ریا منافقت میداند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر فعل که میکنی، بد و نیک، مپوش</p></div>
<div class="m2"><p>گو خلق بدان، چو خالقت میداند</p></div></div>