---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>امروز منم نه کفر و نه ایمانی</p></div>
<div class="m2"><p>نه دانائی تمام و نه نادانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوریده دلی، شیفته ای، حیرانی</p></div>
<div class="m2"><p>بر سر گردن فتاده سرگردانی</p></div></div>