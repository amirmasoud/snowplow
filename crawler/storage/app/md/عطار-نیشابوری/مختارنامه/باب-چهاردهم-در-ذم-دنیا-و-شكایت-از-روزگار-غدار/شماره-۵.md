---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>چون هست جهان جایگه رسوایی</p></div>
<div class="m2"><p>در جایگهی چنین چرا میپایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون میگویی که من نیم اینجایی</p></div>
<div class="m2"><p>پس این همه از چه رو فرو میآیی</p></div></div>