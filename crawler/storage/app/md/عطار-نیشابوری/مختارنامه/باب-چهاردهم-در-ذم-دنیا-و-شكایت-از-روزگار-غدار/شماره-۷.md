---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>این دنیای غدار چه خواهی کردن</p></div>
<div class="m2"><p>وین شوکهٔ پرخار چه خواهی کردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر نه پلنگی تو نه خوکی نه سگی</p></div>
<div class="m2"><p>این گلخن مردار چه خواهی کردن</p></div></div>