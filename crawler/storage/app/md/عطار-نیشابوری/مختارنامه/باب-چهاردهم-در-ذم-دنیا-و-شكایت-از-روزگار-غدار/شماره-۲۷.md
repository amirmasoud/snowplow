---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>بر دل ز غم زمانه باری دارم</p></div>
<div class="m2"><p>در دیدهٔ هر مراد خاری دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه هم نفسی نه غمگساری دارم</p></div>
<div class="m2"><p>شوریده دلی و روزگاری دارم</p></div></div>