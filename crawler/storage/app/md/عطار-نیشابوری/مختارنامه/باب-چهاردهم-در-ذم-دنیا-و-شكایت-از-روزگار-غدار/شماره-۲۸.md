---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>جز بیخبری هیچ خبر نیست مرا</p></div>
<div class="m2"><p>وز اهل نظر هیچ نظر نیست مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند که صد نوحه گرم میباید</p></div>
<div class="m2"><p>جز نوحه گری کار دگر نیست مرا</p></div></div>