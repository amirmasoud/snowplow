---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>ای دل ای دل غم جهان چند خوری</p></div>
<div class="m2"><p>و اندوه به لب آمده جان چند خوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گوشهٔ گلخنی که پرخوک و سگند</p></div>
<div class="m2"><p>این لقمه که آتش به از آن چند خوری</p></div></div>