---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>بویی که به جان ممتحن می‌آید</p></div>
<div class="m2"><p>از بهر هلاک جان و تن می‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چند کمان کشم که هر تیر که من</p></div>
<div class="m2"><p>می‌اندازم بر دل من می‌آید</p></div></div>