---
title: >-
    بخش ۷ - حكایت آدم علیه السلام
---
# بخش ۷ - حكایت آدم علیه السلام

<div class="b" id="bn1"><div class="m1"><p>جزو و کل با یکدگر جمع آمدند</p></div>
<div class="m2"><p>پای تا سر دیدهٔ شمع آمدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از یقین نور تجلّی چون بتافت</p></div>
<div class="m2"><p>خاک مرده روح روحانی بیافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد نفخت فیه من روحی، نثار</p></div>
<div class="m2"><p>سرّ جانان گشت بر خاک آشکار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چل صباح آن جهانی بر گذشت</p></div>
<div class="m2"><p>تا وجود آدم از گل زنده گشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جزو و کل شد چون فرو شد جان بجسم</p></div>
<div class="m2"><p>کس نسازد زین عجائب تر طلسم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جسم آدم صورت جان گشت کل</p></div>
<div class="m2"><p>گشت پیدا راه عزّ و راه ذل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق و عقل و فهم و ادراک و یقین</p></div>
<div class="m2"><p>حزن و شوق و ذوق ازو شد کفر و دین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آدم آنگه چشم معنی باز کرد</p></div>
<div class="m2"><p>روی جانان دید و آنگه ناز کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دید آنگه جنّت و حور و قصور</p></div>
<div class="m2"><p>گشت از نور تجلّی پر زنور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آسمان دید وزمین و چرخ و ماه</p></div>
<div class="m2"><p>کرد در اشیا یکایک او نگاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بود تا بابود کلّی سیر کرد</p></div>
<div class="m2"><p>آنگهی آهنگ کنج دیر کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دید خود را روشن و آراسته</p></div>
<div class="m2"><p>جمله از نور تجلّی خاسته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عطسه آمد ورا، سوی دماغ</p></div>
<div class="m2"><p>گفت او الحمدللّه از فراغ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در تماشای بهشت او باز ماند</p></div>
<div class="m2"><p>حق تعالی از میان جان بخواند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت ای روشن بتو بود وجود</p></div>
<div class="m2"><p>این بگفت و اوفتاد اندر سجود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سجده کرد و گفت ای دانای راز</p></div>
<div class="m2"><p>کار این بیچاره بر کلّی بساز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>این چه اسرارست و من خود کیستم</p></div>
<div class="m2"><p>اندرین جا گاه بهر چیستم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از کدامین ره بدینجا آمدم</p></div>
<div class="m2"><p>عاجز و بی دست و بی پا آمدم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خالقا بیچارهٔ را هم ترا</p></div>
<div class="m2"><p>همچو موری لنگ در راهم ترا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عشق آمد پرده از رخ برگرفت</p></div>
<div class="m2"><p>آدم بیچاره را در بر گرفت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در خطاب آمد که دریاب آدمی</p></div>
<div class="m2"><p>تا ابد چشم و چراغ عالمی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از دم حق آمدی آدم تویی</p></div>
<div class="m2"><p>اصل کرّمنا بنی آدم تویی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خویش را بشناس در زیر و زبر</p></div>
<div class="m2"><p>سجده کردندت ملایک سر بسر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در زمین و آسمان لشکر تراست</p></div>
<div class="m2"><p>جسم و جان و جزو کل یکسر تراست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>قبله گاه آفرینش آمدی</p></div>
<div class="m2"><p>پای تا سر عین بینش آمدی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>باز چون در راه حق بالغ شدی</p></div>
<div class="m2"><p>تا ابد از جسم و جان فارغ شدی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آنچه دیدی و آنچه بینی آن تویی</p></div>
<div class="m2"><p>خویش را بشناس صد چندان تویی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اولش اسما همه تعلیم داد</p></div>
<div class="m2"><p>آنگه او را سلّموا تسلیم داد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گفت آدم ای کریم لایزال</p></div>
<div class="m2"><p>حی و قیوم و رحیم و ذوالجلال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ای دل آدم بتو گشته سرور</p></div>
<div class="m2"><p>غرقه گشته در میان نار و نور</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>من به تو پیدا شدم پیدا بُدی</p></div>
<div class="m2"><p>تو بگو تا تو به که پیدا شدی؟</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>حق تعالی گفت گستاخی مکن</p></div>
<div class="m2"><p>میندانی تا چه میگوئی سخن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>راز ما هم ما بدانیم از نخست</p></div>
<div class="m2"><p>هم بگویم با تو کاین هم حقّ تست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تو بمن پیدا شدی و من بتو</p></div>
<div class="m2"><p>گشته پیدا صنعهای من بتو</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>لیک مقصود من از تو یک کس است</p></div>
<div class="m2"><p>از همه نسل تو ما را او بس است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نام او سلطان محمد آمدست</p></div>
<div class="m2"><p>طاهر و محمود و احمد آمدست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گرنه او بودی نبودی کاینات</p></div>
<div class="m2"><p>گر نه او بودی نبودی این صفات</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گرنه او بودی نبودی ماه و خور</p></div>
<div class="m2"><p>گرنه او بودی نبودی بحر و بر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گرنه او بودی نبودی آسمان</p></div>
<div class="m2"><p>گرنه او بودی نبودی جسم و جان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گرنه او بودی نبودی اسم تو</p></div>
<div class="m2"><p>کی بدی هرگز نشان جسم تو</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گرنه او بودی نبودی انبیا</p></div>
<div class="m2"><p>گرنه او بودی نبودی اولیا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گرنه او بودی نبودی این زمین</p></div>
<div class="m2"><p>عرش و کرسی با مکان و با مکین</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گرنه او بودی نبودی این بهشت</p></div>
<div class="m2"><p>نور او خاک وجود تو سرشت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گرنه او بودی نبودی این جهان</p></div>
<div class="m2"><p>خوب و زشت و آشکارا و نهان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گرنه او بودی شفاعت خواه تو</p></div>
<div class="m2"><p>کی شدی پیدا در این جا راه تو</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>آفرینش را جز او مقصود نیست</p></div>
<div class="m2"><p>پاک دامن تر ازو موجود نیست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>وصف پیغمبر چو آدم گوش کرد</p></div>
<div class="m2"><p>یک زمان از شوق، جان بیهوش کرد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گفت میخواهم که بینم روی او</p></div>
<div class="m2"><p>تا شوم من گرد خاک کوی او</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>حق تعالی گفت نور او ببین</p></div>
<div class="m2"><p>کان نهادم من ترا اندر جبین</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>لیک بنگر این زمان از دست راست</p></div>
<div class="m2"><p>تا ببینی آنچه مقصود تراست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چون نظر آدم بدست راست کرد</p></div>
<div class="m2"><p>آنچه او از حق تعالی خواست کرد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دید آدم عرش با لوح و قلم</p></div>
<div class="m2"><p>بعد از آن، آن نور عالم زد علم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>شعلهٔ زد نور پاک مصطفی</p></div>
<div class="m2"><p>کرد روشن هم زمین و هم سما</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نور عالی هر دو عالم در گرفت</p></div>
<div class="m2"><p>آدم از آن نور مانده در شگفت</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چشم آدم شورشی آغاز کرد</p></div>
<div class="m2"><p>بعد از آن بر هم نهاد وباز کرد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ناخن آدم از آن روشن ببود</p></div>
<div class="m2"><p>روی آدم همچو آئینه نمود</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بر سر هر ناخنی دیدآدمی</p></div>
<div class="m2"><p>بود پیدا هر یکی را عالمی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>آدم از آن شوق بیهوش اوفتاد</p></div>
<div class="m2"><p>در نبود وبود، خاموش اوفتاد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گفت این فرزند خاص الخاص تست</p></div>
<div class="m2"><p>ره نمای توبهٔ و اخلاص تست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>آدم آنگه پس دعاآغاز کرد</p></div>
<div class="m2"><p>هر دو کف بر روی خود او باز کرد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>گفت آدم یا اله العالمین</p></div>
<div class="m2"><p>راحم و رحمن و خیر النّاصرین</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>آدم بیچاره را توفیق بخش</p></div>
<div class="m2"><p>دیدهٔ جانش ره تحقیق بخش</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>رحمتی کن بر تمامت جزو و کل</p></div>
<div class="m2"><p>آدم مسکین مگردان عین ذل</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>در موافق جملهٔ کروّبیان</p></div>
<div class="m2"><p>جمله آمین گوی بودند آن زمان</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چون دو دست خویش بر روی آورید</p></div>
<div class="m2"><p>آدم آنگه سوی جنّت بنگرید</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>آنچنانش سکر عشق آورده بود</p></div>
<div class="m2"><p>کز میان پرده فوقش پرده بود</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>یک زمان در خواب شد بیهوش او</p></div>
<div class="m2"><p>بود از آن مستی عجب مدهوش او</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ناگهان در خواب اندر خواب دید</p></div>
<div class="m2"><p>صورتی چون ماه و خور گشتی پدید</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>صورتی کاندر جهان مثلش نبود</p></div>
<div class="m2"><p>روی خود را سوی آدم مینمود</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>صورتی مانندهٔ خورشید و ماه</p></div>
<div class="m2"><p>آدم اندر وی همی کردی نگاه</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چشم عالم همچنان دیگر ندید</p></div>
<div class="m2"><p>کرد پیدا حق تعالی دید دید</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>رغبت او کرد آدم آن زمان</p></div>
<div class="m2"><p>گشت عاشق بر جمالش آنچنان</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>خواست تا او را بگیرد در کنار</p></div>
<div class="m2"><p>کرد ازوی ناگهان حوّا کنار</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>غیرت حق در وجودش کار کرد</p></div>
<div class="m2"><p>بعد از آن اندیشهٔ بسیار کرد</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>خالق آفاق من فوق الحجاب</p></div>
<div class="m2"><p>بر ید قدرت چنین کرد انقلاب</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>آنچه آدم را بخوابش مینمود</p></div>
<div class="m2"><p>در زمان از صنع او پیدا ببود</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چون چراغی از چراغی برفروخت</p></div>
<div class="m2"><p>آن چراغ نور آدم بر فروخت</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>آدم از آن خواب خوش ناگه بجست</p></div>
<div class="m2"><p>در دو چشم خویش مالید او دو دست</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>دید آن محبوب و آن روح قلوب</p></div>
<div class="m2"><p>گفت ای رحمن و ستّار العیوب</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>این چه سرّست این دگر با من بگوی</p></div>
<div class="m2"><p>کز کجا پیدا شدست این ماهروی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>حق تعالی گفت این هم جفت تست</p></div>
<div class="m2"><p>هم سروهم راز تو،‌هم گفت تست</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>از تو و او خلق عالم سر بسر</p></div>
<div class="m2"><p>میکنم پیدا، بدان ای بی خبر</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>از ره شرع رسول هاشمی</p></div>
<div class="m2"><p>کرد ایشان را بباید همدمی</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>گفت ای آدم کنون گندم مخور</p></div>
<div class="m2"><p>کز بهشت جاودان افتی بدر</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>این شجر زنهار تا تو ننگری</p></div>
<div class="m2"><p>چون ببینی این شجر زو بگذری</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>جبرئیلش هر زمان رخ مینمود</p></div>
<div class="m2"><p>قدر آدم لحظه لحظه میفزود</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>زان شجر میداد مر وی را خبر</p></div>
<div class="m2"><p>زینهار ای آدم این گندم مخور</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>آدم از عزّت چنان در عز فتاد</p></div>
<div class="m2"><p>تاج بر فرق جهاندارش نهاد</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>هر زمان در منزلی و گوشهٔ </p></div>
<div class="m2"><p>پیش او میرست هر جا خوشهٔ </p></div></div>
<div class="b" id="bn90"><div class="m1"><p>بود با حوا چنان در عین راز</p></div>
<div class="m2"><p>گاه در شیب و گهی اندر فراز</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>صورت حوا چنانش دوست بود</p></div>
<div class="m2"><p>پای تا سر مغز بُد نه پوست بود</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>مانده حیران در رخ آن دلنواز</p></div>
<div class="m2"><p>در حقیقت بود اندر عین راز</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>لیک ابلیس لعین در جست و جوی</p></div>
<div class="m2"><p>خوار و ملعون گشته رفته آبروی</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>جان او در تفّ نار افتاده بود</p></div>
<div class="m2"><p>کار او بس خوار و زار افتاده بود</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>مکر بر تلبیس میکرد از حسد</p></div>
<div class="m2"><p>تا کند آدم مثال خویش رد</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>سالها در ناله و در درد بود</p></div>
<div class="m2"><p>در میان زندگان او فرد بود</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>بر در جنّت نشست او، سالها</p></div>
<div class="m2"><p>تا همه معلوم کرد احوالها</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>مار با طاوس، دربان بهشت</p></div>
<div class="m2"><p>رفت و با ایشان گل انسی بکشت</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>در دهان مار بنشست آن لعین</p></div>
<div class="m2"><p>رفت در سوی بهشت او پر ز کین</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>تا بر آدم میرسید آن نابکار</p></div>
<div class="m2"><p>دید آدم را نشسته شاهوار</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>بود در پهلوی حوا شادمان</p></div>
<div class="m2"><p>بد نشسته بر سر تخت روان</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>تخت هرجائی روان مانند آب</p></div>
<div class="m2"><p>انگبین ناب با شیر و شراب</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>زیر آن تخت روان هر جای جوی</p></div>
<div class="m2"><p>بود سرگردان عزازیلش چو گوی</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>هر کجا ابلیس میشد از نخست</p></div>
<div class="m2"><p>یک دوسه خوشه در آنجا میشکفت</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>پیش آدم رفت و گفتا این بخور</p></div>
<div class="m2"><p>کاین همه از بهر تست ای نامور</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>چون شب تاریک لیل عسعسه</p></div>
<div class="m2"><p>هر زمان میکرد بروی وسوسه</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>عاقبت اندر تن او راه یافت</p></div>
<div class="m2"><p>هر دم از لونی دگر بیرون شتافت</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>خواست تا او را برد از ره برون</p></div>
<div class="m2"><p>فعل مس الجن، باشد در جنون</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>آنچه تقدیر خدا بود از ازل</p></div>
<div class="m2"><p>کارگاهش، حکم رفته بی خلل</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>هرچه هست و بود و آید در وجود</p></div>
<div class="m2"><p>شبنمی دان آن هم از دریای جود</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>یفعل اللّه ما یشا حق گفته است</p></div>
<div class="m2"><p>درّ این اسرار معنی،‌سفته است</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>هر که او اسرار اول باز دید</p></div>
<div class="m2"><p>خویشتن را برتر از اعزاز دید</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>هرچه حق خواهد بباشد در زمان</p></div>
<div class="m2"><p>بی خبر زین راز آمد جسم و جان</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>هرچه حق خواهد بباشد بی شکی</p></div>
<div class="m2"><p>این کسی داند که او بیند یکی</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>هرچه حق خواهد کند در قادری</p></div>
<div class="m2"><p>این بدان ای بیخبر گر ناظری</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>هرچه حق خواهد کند بی گفتگوی</p></div>
<div class="m2"><p>چند گویی هیچ باشد گفت و گوی</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>در ازل او سر یکایک دیده است</p></div>
<div class="m2"><p>این کسی داند که صاحب دیده است</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>در ازل بنوشت هم خود باز خواند</p></div>
<div class="m2"><p>خود براند از پیش و هم خود باز خواند</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>در ازل حکمی که رفته آن بود</p></div>
<div class="m2"><p>مرگ را آخر درین تاوان بود</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>گر شوی در راه او، تسلیم او</p></div>
<div class="m2"><p>درگذر زین شیوه و این گفتگو</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>هر که خواهد برگزیند از میان</p></div>
<div class="m2"><p>جهد کن تا خود نه بینی در میان</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>بر سر هر کس قضائی میرود</p></div>
<div class="m2"><p>برتن هر کس بلائی میرود</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>خون صدّیقان ازین حسرت بریخت</p></div>
<div class="m2"><p>آسمان بر فرق ایشان خاک بیخت</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>عشقبازی میکند با خویشتن</p></div>
<div class="m2"><p>تو ندانی ای که دیدی خویشتن</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>نه قلم دارد ازین سر آگهی</p></div>
<div class="m2"><p>لوح خود را شسته، بر دست تهی</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>عرش سرگردان این اسرار شد</p></div>
<div class="m2"><p>از نمود خویشتن بیزار شد</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>در قضای خود رضا ده از یقین</p></div>
<div class="m2"><p>خویشتن رادر میان یکجو مبین</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>از قضای رفته گر واقف شوی</p></div>
<div class="m2"><p>در زمان و در مکان عارف شوی</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>از قضای رفته کس آگاه نیست</p></div>
<div class="m2"><p>هیچ عاقل واقف این راه نیست</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>بر قضای رفته ای دل تن بنه</p></div>
<div class="m2"><p>پیش حق هر لحظهٔ گردن بنه</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>از قضای رفته، بسیاری مگوی</p></div>
<div class="m2"><p>درد این اسرار را، درمان مجوی</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>از قضای رفته، آدم بی خبر</p></div>
<div class="m2"><p>بود خوش بنشسته بی خوف و خطر</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>از قضای رفته، زد سیمرغ لاف</p></div>
<div class="m2"><p>لاجرم از شرم شد بر کوه قاف</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>عاقبت چون حکم ایزد کار کرد</p></div>
<div class="m2"><p>آدم اندیشه در آن بسیار کرد</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>چون قضای رفته بُد گندم بخورد</p></div>
<div class="m2"><p>ناگهان افتاد در اندوه و درد</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>رفت حوا نیز اکلی کرد از آن</p></div>
<div class="m2"><p>خوشهٔ بستد ز آدم خورد از آن</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>حُلّهاشان محو شد اندر وجود</p></div>
<div class="m2"><p>آنچه اول رفته بود آخر ببود</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>خوار و سرگردان شدند و مستمند</p></div>
<div class="m2"><p>ناگهان نزدیکشان آمد گزند</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>جبرئیل آمد که حق فرموده است</p></div>
<div class="m2"><p>کاین همه رنج شما بیهوده است</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>از بهشت عدن ما بیرون شوید</p></div>
<div class="m2"><p>همچنان در نزد خاک و خون شوید</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>این همه گفتم شما را پیش ازین</p></div>
<div class="m2"><p>عاقبت خود کار خود کرد آن لعین</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>از بهشت عدنشان بیرون فکند</p></div>
<div class="m2"><p>بار دیگرشان میان خون فکند</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>حق تعالی گفت با آدم براز</p></div>
<div class="m2"><p>کای خطا کرده، بمانده در نیاز</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>این چراکردی؟ که گفتت این بخور؟</p></div>
<div class="m2"><p>کو فتادی از بهشت ما بدر</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>آدم بیچاره زان خاموش شد</p></div>
<div class="m2"><p>از خجالت یک زمان مدهوش شد</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>بار دیگر کرد حق، با او خطاب</p></div>
<div class="m2"><p>تا چراخاموش کشتی در جواب</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>گفت آدم ربّنا بد کردهام</p></div>
<div class="m2"><p>هرچه کردم با تن خود کردهام</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>لیک شیطان لعین از ره ببرد</p></div>
<div class="m2"><p>بر من مسکین بکرد این دست برد</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>ربّنا یا ربّنا یا ربّنا</p></div>
<div class="m2"><p>ظلم کردم بعد ازینم ره نما</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>بعد از آن بادی برآمد پر خطر</p></div>
<div class="m2"><p>کردشان هر دو جدا از یکدگر</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>درنگر ای راه بین تاشان چه بود</p></div>
<div class="m2"><p>حق تعالی بود با ایشان نمود</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>هست این تمثیل جسم و جان تو</p></div>
<div class="m2"><p>یک دو روزی آمده مهمان تو</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>از بهشت عدن بیرون رفتهٔ</p></div>
<div class="m2"><p>در میان خاک و در خون خفتهٔ</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>هست ابلیس لعینت رهنمای</p></div>
<div class="m2"><p>باز میافتی دمادم از خدای</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>چون بلای قرب حق آدم بدید</p></div>
<div class="m2"><p>خویشتن را در میان دردم بدید</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>سرّ گندم بود کورا خوار کرد</p></div>
<div class="m2"><p>سرّ گندم هفت و پنج و چار کرد</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>سرّ گندم در درون نطفه بین</p></div>
<div class="m2"><p>تا شود جمله گمان تو یقین</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>گر نبودی جسم را، جانی بکار</p></div>
<div class="m2"><p>کی شدی آدم خود آنجا آشکار</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>بند راه آدم آمد این شجر</p></div>
<div class="m2"><p>بر سلوک آن فتادش این خطر</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>او بدو گندم،‌ بهشت عدن داد</p></div>
<div class="m2"><p>این بلا و رنج را بر خود نهاد</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>گر بیک جو میتوانی، داد ده</p></div>
<div class="m2"><p>نفس خود را یک زمانی،‌ داده ده</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>برگذر زین خاکدان خلق خوار</p></div>
<div class="m2"><p>درگذر زین صورت ناپایدار</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>ورنه دنیا زود مردارت کند</p></div>
<div class="m2"><p>گنده تر از خویش صد بارت کند</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>هست دنیا بر مثال آتشی</p></div>
<div class="m2"><p>هر زمان خلقی بسوزاند خوشی</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>هست دنیا بر مثال کژدمی</p></div>
<div class="m2"><p>میزند او نیشها در هر دمی</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>هست دنیا چون پلی بگذر ز وی</p></div>
<div class="m2"><p>ورنه لرزان گرددت هم پا و پی</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>هست دنیا آشیان حرص و آز</p></div>
<div class="m2"><p>مانده از فرعون و از نمرود باز</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>هست دنیا گنده پیری گوژ پشت</p></div>
<div class="m2"><p>صد هزاران شوی هر روزی بکشت</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>هست دنیا بی وفا و پر جفا</p></div>
<div class="m2"><p>تو ازو امید میداری وفا</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>هست دنیا جای مرگ و دردو داغ</p></div>
<div class="m2"><p>گر تو مردی زود گیری زو فراغ</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>هست دنیا کشتزار آن جهان</p></div>
<div class="m2"><p>تو در اینجا نیز تخمی برفشان</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>هست دنیا همچو مرداری خسیس</p></div>
<div class="m2"><p>کم مگردان اندرو جان نفیس</p></div></div>