---
title: >-
    بخش ۹ - در صفت كتاب گوید
---
# بخش ۹ - در صفت كتاب گوید

<div class="b" id="bn1"><div class="m1"><p>گوش کن ای هوشمند راز بین</p></div>
<div class="m2"><p>در حقیقت خویشتن را باز بین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتهٔ بیهوده را چندین مخوان</p></div>
<div class="m2"><p>مرکب بیهودگی چندین مران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم دل را بر یقینت باز کن</p></div>
<div class="m2"><p>مجلسی دیگر ز نو آغاز کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیر و درد دل عطّار شو</p></div>
<div class="m2"><p>از هزاران گنج بر خوردار شو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در جهان قدس هر دم میخرام</p></div>
<div class="m2"><p>تا شود کارت همه یکسر تمام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برگذر از ننگ خاص و عامه را</p></div>
<div class="m2"><p>گوش کن مررمز اشترنامه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این کتابی دیگرست از سرّ راز</p></div>
<div class="m2"><p>دیدهٔ اسرار بین را، کن تو باز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر رموز این، میسّر آیدت</p></div>
<div class="m2"><p>کاینات اینجایگه، بنمایدت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طرز و طرحی دیگرست این پر رموز</p></div>
<div class="m2"><p>نارسیده دست کس بروی هنوز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بوی این گر هیچ بتوانی شنود</p></div>
<div class="m2"><p>گویی از کونین بتوانی ربود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرچه تصنیف کسانی دیگرست</p></div>
<div class="m2"><p>همچو طفل شیرخوار مادرست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر کسی را فهم این حاصل شود</p></div>
<div class="m2"><p>دیر نبود کو درین واصل شود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>منطق الطیرم زبانی دیگرست</p></div>
<div class="m2"><p>مرغ آن از آشیانی دیگرست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من مصیبت نامه را بگذاشتم</p></div>
<div class="m2"><p>مغز آنست این کزان برداشتم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خسرو و گل فاش کردم در صور</p></div>
<div class="m2"><p>معنی آن باز دان ای بیخبر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هست اسرارم در آنجا سرّ جان</p></div>
<div class="m2"><p>دیدهام معشوق خود عین عیان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر الهی نامه، در چنگت فتد</p></div>
<div class="m2"><p>از وجود خویشتن ننگت فتد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هست اشترنامه اسرار عیان</p></div>
<div class="m2"><p>لیک پنهانست این اندر جهان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>فاش کردم آن سخنهای دگر</p></div>
<div class="m2"><p>نا نداند سر این هر بیخبر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>این کتاب عاشقانست ای پسر</p></div>
<div class="m2"><p>اندرو سر عیانست ای پسر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>این کتاب از لامکان آوردهام</p></div>
<div class="m2"><p>جان صورت اندر آن گم کردهام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در نهان سر نهان دانستهام</p></div>
<div class="m2"><p>این جهان وآن جهان دانستهام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دید دید از غیب آوردم برون</p></div>
<div class="m2"><p>تا نمایم اندر آنجا چند و چون</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عاشقان در تحفهٔ این سر شوید</p></div>
<div class="m2"><p>بر رموز معنویم بنگرید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دیدهٔ اسرار بین را باز کن</p></div>
<div class="m2"><p>آنچه میدانی ز سر آغاز کن</p></div></div>