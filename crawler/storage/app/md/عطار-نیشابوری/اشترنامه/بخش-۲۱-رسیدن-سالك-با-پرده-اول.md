---
title: >-
    بخش ۲۱ - رسیدن سالك با پرده اول
---
# بخش ۲۱ - رسیدن سالك با پرده اول

<div class="b" id="bn1"><div class="m1"><p>میبرید او راه خود در پرده باز</p></div>
<div class="m2"><p>تا مگر پیدا شود در پرده راز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اولین پرده ز نور تاب دید</p></div>
<div class="m2"><p>چون نظر کرد اندران پرتاب دید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود نوری شعله زن در پرده در</p></div>
<div class="m2"><p>گر نبینی آن تو باشی پرده در</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود نوری سبز با او تاب دار</p></div>
<div class="m2"><p>در صفت مانند حوضی آب دار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت ای استاد ای تو پرده در</p></div>
<div class="m2"><p>چیست با من تو بیان کن این خبر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت ای مسکین مترس و اندر آی</p></div>
<div class="m2"><p>تا ترا بر فرق افتد نور ورای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چند ترسان باشی و بیخود شوی</p></div>
<div class="m2"><p>نیک میبین تا نباشی در بدی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خود مبین تا این همه کم گرددت</p></div>
<div class="m2"><p>قطره دریا بهم کم گرددت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در سلوک آتش طبعی ممان</p></div>
<div class="m2"><p>سرّ ما را هم ز ما تو بازدان</p></div></div>