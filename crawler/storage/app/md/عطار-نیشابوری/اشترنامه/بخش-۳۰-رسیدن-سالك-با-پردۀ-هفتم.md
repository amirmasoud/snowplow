---
title: >-
    بخش ۳۰ - رسیدن سالك با پردۀ هفتم
---
# بخش ۳۰ - رسیدن سالك با پردۀ هفتم

<div class="b" id="bn1"><div class="m1"><p>سالک ره کرده چون ره کرد و رفت</p></div>
<div class="m2"><p>همچنان چون برق تازان می برفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در رسید او پردهٔ هفتم تمام</p></div>
<div class="m2"><p>دید آنجا گه مقام بی مقام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خویشتن بالای اشیا یافت او</p></div>
<div class="m2"><p>وان نهانی راز پیدا یافت او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرده در آنجا عجایب مینمود</p></div>
<div class="m2"><p>بود آنجا لیک دربود و نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پرده رفته ذات بی وصف و صفت</p></div>
<div class="m2"><p>در کمال قل هواللّه معرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جایگاهی دید او برتر ز جسم</p></div>
<div class="m2"><p>هرکه آن جا رفت بیرون شد ز اسم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جایگاهی دید راز راز دار</p></div>
<div class="m2"><p>بود آن جا ایستاده پرده دار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جایگاهی دید مرد معنوی</p></div>
<div class="m2"><p>در نهاد کل عجایب او قوی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جایگاهی بود چون بحری لذیذ</p></div>
<div class="m2"><p>لازمان و لامکان و لاجدید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جایگاهی یافت بیرون از خیال</p></div>
<div class="m2"><p>رفعت او برتر از ذات کمال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون در آن کون پر از عزّت رسید</p></div>
<div class="m2"><p>یک تنی از ذات پاک او بدید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صورتی اما نه صورت بود آن</p></div>
<div class="m2"><p>نور تحقیق و عیان اندر عیان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پای تا سر جمله از نور اله</p></div>
<div class="m2"><p>ایستاده بود اندر پیشگاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر صفت مانندهٔ نوری بد او</p></div>
<div class="m2"><p>گوییا خود نیز چون حوری بداو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر صفت او را نه سر بود ونه پای</p></div>
<div class="m2"><p>ایستاده بود لیکن نه بجای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جای او از حد گذشته بی صفت</p></div>
<div class="m2"><p>چون کنم این را کمالی بر صفت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بود پیری لیک بدهم جفت نور</p></div>
<div class="m2"><p>با کمال معرفت اندر حضور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دفتری در دست و معنی پر عدد</p></div>
<div class="m2"><p>اندر آنجا قل هواللّه احد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جوهری در دست چپ با دفتری</p></div>
<div class="m2"><p>جوهری چه جوهری چه گوهری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چوهری کان از دو عالم بیش بود</p></div>
<div class="m2"><p>سر ز هیبت درفکنده پیش بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عکس آن جوهر به از نور یقین</p></div>
<div class="m2"><p>سالکان را پیش رو او راه بین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عکس آن جوهر فروغ ذات داشت</p></div>
<div class="m2"><p>روی با آن دفتر و آیات داشت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یک ستونی پیش او استاده بود</p></div>
<div class="m2"><p>نورها ازعکس آن بگشاده بود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عکس جوهر بر ستون افتاده بود</p></div>
<div class="m2"><p>بر ستون کون و مکان بگشاده بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عکس جوهر ماورای عقل تافت</p></div>
<div class="m2"><p>آنکه این دریافت نه از نقل یافت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عکس جوهر لامکان بگرفته کل</p></div>
<div class="m2"><p>نور آن کون و مکان بگرفته کل</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عکس را بر ذات ذات اندر یقین</p></div>
<div class="m2"><p>کس ندیدش جز که مرد راه بین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سالک ره کردهٔ بی خویشتن</p></div>
<div class="m2"><p>اندر آنجا بود نه جان و نه تن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سالک ره کرده اندر نیستی</p></div>
<div class="m2"><p>دیده خود رادر مقام نیستی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سالک ره کردهٔ واصل شده</p></div>
<div class="m2"><p>اندر آنجا دید کلی دل شده</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سالک ره کردهٔ راه یقین</p></div>
<div class="m2"><p>دید آنجا راستی در آستین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سالک ره کردهٔ بی جسم و جان</p></div>
<div class="m2"><p>خود بدیده برتر از کون و مکان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سالک آنگه سوی آن تن باز شد</p></div>
<div class="m2"><p>با رموز راز او دمساز شد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کرد بروی از ارادت یک سلام</p></div>
<div class="m2"><p>داد وی در هر سلامی بی کلام</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>روی سوی سالک بیهوش کرد</p></div>
<div class="m2"><p>بعد از آن گفتار سالک گوش کرد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گفت سالک ای کمال نور قدس</p></div>
<div class="m2"><p>یک نفس بامن زمانی گیر انس</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ای سلاطینان عالم پیش تو</p></div>
<div class="m2"><p>سرفکنده همچو گوئی پیش تو</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ای نمود تو نمود بی نمود</p></div>
<div class="m2"><p>عکس نور تو مرا اینجا نمود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>پرتو نور تو نور آسمان</p></div>
<div class="m2"><p>صد جهان اندر زمان اندر مکان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>پرتو نور تو اینجا راه یافت</p></div>
<div class="m2"><p>تا دو چشم جان من ناگاه یافت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پرتو نور تو آمد کارگر</p></div>
<div class="m2"><p>تا مرا از زیر افکندش ببر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>پرتو نور تو زد ناگه علم</p></div>
<div class="m2"><p>پیش خود هم با وجودت در عدم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>این چه جای است این رموز لم یزل</p></div>
<div class="m2"><p>راز بر گو تا کنم جان بر بدل</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>پرتو تو آسمانست و زمین</p></div>
<div class="m2"><p>پرتو تو هم مکان و هم مکین</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>راست برگوی و مرا راهی نمای</p></div>
<div class="m2"><p>راه ما را تو برمزی برگشای</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کز کجا اینجا فتادم ناگهان</p></div>
<div class="m2"><p>نور دایم کن پیاپی در بیان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>از کجا اینجا فتادم بی قرار</p></div>
<div class="m2"><p>کین قراراین جا ندارد خود کنار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>از کجا اینجا دگر راهی برم</p></div>
<div class="m2"><p>تادگر راز دگر من بنگرم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نیست چیزی عکس نورست این زمان</p></div>
<div class="m2"><p>با من این راز حقیقت کن عیان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نیست پیدا هیچکس هیچی نمود</p></div>
<div class="m2"><p>این همه بر هیچ باشد بی وجود</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نیست پیدا آسمان و هم زمین</p></div>
<div class="m2"><p>نیست پیدا هم مکان و هم مکین</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>نیست پیدا آتش بادست و باد</p></div>
<div class="m2"><p>آب و خاک آنجا کجا آید بباد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نیست تحتی فوق آخر در که رفت</p></div>
<div class="m2"><p>نیست پیدا هست باری در چه رفت</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نیست پیدا نیست اشیا نقشها</p></div>
<div class="m2"><p>نیست پیدا چون کنم این نقشها</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>با من سرگشته اکنون راز گوی</p></div>
<div class="m2"><p>آنچه تو دانی ابا من باز گوی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>راه کردم بی حد و بی منتها</p></div>
<div class="m2"><p>تا رسیدم اندرین جای صفا</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>دهشتی دارد ولی ذوقی رسید</p></div>
<div class="m2"><p>این زمانم دم بدم شوقی رسید</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>راه بی حد کردم اندر پرده من</p></div>
<div class="m2"><p>تا برون بردم ازآنجا خویشتن</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>دیدم و دیدم ز دیده شد نهان</p></div>
<div class="m2"><p>بس که جائی نی مکانست و زمان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>دیدم آنجا محو محو اندر یکی</p></div>
<div class="m2"><p>کی رسد آن جای هرگز آدمی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>این چنین جائی که اینجا آمدم</p></div>
<div class="m2"><p>چون در این جا گاه پیدا آمدم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>من عجایب در عجب دیدم کنون</p></div>
<div class="m2"><p>مر مرا راهی نما ای رهنمون</p></div></div>