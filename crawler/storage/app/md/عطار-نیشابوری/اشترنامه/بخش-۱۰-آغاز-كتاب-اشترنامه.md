---
title: >-
    بخش ۱۰ - آغاز كتاب اشترنامه
---
# بخش ۱۰ - آغاز كتاب اشترنامه

<div class="b" id="bn1"><div class="m1"><p>یک دمی ای ساربان عاشقان</p></div>
<div class="m2"><p>در چرا آور زمانی اشتران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندرین صحرای بی پایان درآی</p></div>
<div class="m2"><p>تا درین ره بشنوی بانگ درای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا در آنجا جمع گردد قافله</p></div>
<div class="m2"><p>سوی حج رانیم ما بی مشغله</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کعبهٔ مقصود را حاصل کنیم</p></div>
<div class="m2"><p>در تجلّی خویش را واصل کنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عقبی بر شیمهٔ این ره زنیم</p></div>
<div class="m2"><p>حلقه بر سندان داراللّه زنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی در صحرای عشّاق آوریم</p></div>
<div class="m2"><p>یاد از گفتار مشتاق آوریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باز سرگردان این صحرا شویم</p></div>
<div class="m2"><p>در درون کعبه ناپروا شویم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این چنین ره راه مشتاقان بود</p></div>
<div class="m2"><p>تا ابد این راه بی پایان بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای دل آخر جان خود ایثار کن</p></div>
<div class="m2"><p>یک دمی آهنگ کوی یار کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ره روان رفتند و تو درماندهٔ</p></div>
<div class="m2"><p>حلقهٔ در زن،‌که بر درماندهٔ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اشتران جسم در صحرای عشق</p></div>
<div class="m2"><p>مست میآیند در سودای عشق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همچو ایشان گرم رو باش و مترس</p></div>
<div class="m2"><p>اندرین ره سیر کن فاش و مترس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر کجا آیی فرود آنجا مباش</p></div>
<div class="m2"><p>این دویی بگذار و جز یکتا مباش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بعد از آن در گرد این صورت مگرد</p></div>
<div class="m2"><p>همچو درّی باش در دریاش فرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا که از ملک معانی برخوری</p></div>
<div class="m2"><p>از سر سررشتهٔ خود بگذری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پس مهار عشق در بینی کنی</p></div>
<div class="m2"><p>بعد از آن آهنگ یک بینی کنی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر قطار اشتران عاشق شوی</p></div>
<div class="m2"><p>در درون کعبه صادق شوی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دیر صورت زود گردانی خراب</p></div>
<div class="m2"><p>تا بتابد از درونت آفتاب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در محبّت تاکه غیری باشدت</p></div>
<div class="m2"><p>در درون کعبه دیری باشدت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بحث بارهبان دیری بر فکن</p></div>
<div class="m2"><p>طیلسان لم یکن در سر فکن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا ترا معلوم گردد حالها</p></div>
<div class="m2"><p>باز دانی زو همه احوالها</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در بن این بحر بی پایان بسی</p></div>
<div class="m2"><p>غرق کشتند و نیامد خود کسی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کس چه داند تا درین دیر کهن</p></div>
<div class="m2"><p>چند تقدیر آوریدند از سخن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جایگاهی خوفناکست این مکان</p></div>
<div class="m2"><p>وامایست و اشتران زینجا بران</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تامگر در کعبهٔ جانان روی</p></div>
<div class="m2"><p>در مقام ایمنی خوش بغنوی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کعبهٔ جانها مکانی دیگرست</p></div>
<div class="m2"><p>این زمان آنجا زمانی دیگرست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>این رهی بس بینهایت آمدست</p></div>
<div class="m2"><p>تا ابد بیحدّ وغایت آمدست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پای دل در پیچ و بس مردانه باش</p></div>
<div class="m2"><p>در جنون عشق کل دیوانه باش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>راه اینست و دگر خود راه نیست</p></div>
<div class="m2"><p>هیچ دل زین راه ما آگاه نیست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ره روان دانند راه عشق را</p></div>
<div class="m2"><p>کز دو عالم هست این ره برترا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>راه عشقست این وراه کوی دوست</p></div>
<div class="m2"><p>راه رو تا در رسی در کوی دوست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کعبهٔ عشّاق را دریاب زود</p></div>
<div class="m2"><p>جملهٔ ذرّات شان این راه بود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>این ره اندر نیستی پیدا شدست</p></div>
<div class="m2"><p>این چنین ره راه پرغوغا شدست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>راه دورست و دمی منشین ورو</p></div>
<div class="m2"><p>ره یکی است و تو ره میبین و رو</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جهد کن تا اندرین راه دراز</p></div>
<div class="m2"><p>گرم رو باشی نمانی مانده باز</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جهد کن تا هیچ غیری ننگری</p></div>
<div class="m2"><p>تا ز فضل جاودانی برخوری</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>جهد کن تا تو بمانی برقرار</p></div>
<div class="m2"><p>زانکه بسیارست گل بازخم خار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سالکان پخته و مردان دین</p></div>
<div class="m2"><p>عاشقان بردبار راه بین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اندرین ره رازها برگفتهاند</p></div>
<div class="m2"><p>درّ معنی را بمعنی سفتهاند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گم شدند اول ز خود پس از وجود</p></div>
<div class="m2"><p>لاجرم دیگر ره رفتن نبود</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>باز بعضی در صور یک بین شدند</p></div>
<div class="m2"><p>باز بعضی مانده و خودبین شدند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>باز بعضی در گمان و در یقین</p></div>
<div class="m2"><p>همچو بلعم مانده نه کفر و نه دین</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>باز بعضی خوار مانده در جنون</p></div>
<div class="m2"><p>باز بعضی گفتشان آمد فنون</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>باز بعضی در زمین، خوار از تعب</p></div>
<div class="m2"><p>باز ماندند از صورها در عجب</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>باز بعضی از کتبها دم زدند</p></div>
<div class="m2"><p>داد خود از صورت حس بستدند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>باز بعضی در شراب و در قمار</p></div>
<div class="m2"><p>بازماندند اندرین پنج و چهار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>باز بعضی خوارو ناپروا شدند</p></div>
<div class="m2"><p>درمیان خلق، تن رسوا شدند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>باز بعضی تن ضعیف و جان نزار</p></div>
<div class="m2"><p>خرقهٔ در فقر کردند اختیار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>باز بعضی در عجایبهای راه</p></div>
<div class="m2"><p>باز استادند در جنب گناه</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>باز بعضی کنج بگزیدندو درد</p></div>
<div class="m2"><p>تا فنای شوق آید در نبرد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>باز بعضی غرقهٔ آتش شدند</p></div>
<div class="m2"><p>باز افتادند و دل ناخوش شدند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>باز بعضی باد کبر از سر برون</p></div>
<div class="m2"><p>آوریدند وشدند ایشان زبون</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>باز بعضی آبروی خویشتن</p></div>
<div class="m2"><p>عرضه دادند از میان انجمن</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>باز بعضی زرق وسالوس آمدند</p></div>
<div class="m2"><p>اندرین ره بس بناموس آمدند</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>باز بعضی عالم منطق شدند</p></div>
<div class="m2"><p>از مقام الکنی ناطق شدند</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>باز بعضی ناطق وپر گو شدند</p></div>
<div class="m2"><p>در بر چوگان تن،‌چون گوشدند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>باز بعضی در جدل جهّال وار</p></div>
<div class="m2"><p>آمدند و قیل کردند اختیار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>باز بعضی حاکم دنیا شدند</p></div>
<div class="m2"><p>فارغ ازدانستن عقبی شدند</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>باز بعضی عدل کردند از یقین</p></div>
<div class="m2"><p>آنچه حق فرموده است از راه دین</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>باز بعضی دزد و هم رهزن شدند</p></div>
<div class="m2"><p>عاقبت اندر سر این فن شدند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>باز بعضی کنج کردند اختیار</p></div>
<div class="m2"><p>تن فرو دادند در صبر و قرار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>باز بعضی عقل را عاشق شدند</p></div>
<div class="m2"><p>در مقام عقل خود صادق شدند</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>باز بعضی در مقام بیخودی</p></div>
<div class="m2"><p>عشق آوردند در الّا الذی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>باز بعضی عاشق و حیران شدند</p></div>
<div class="m2"><p>در ره توحید، بی برهان شدند</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>این ره کل دان و باقی هیچ دان</p></div>
<div class="m2"><p>این ره جدّست و باقی پیچ دان</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>گردرین راه اندر آیی یکدمی</p></div>
<div class="m2"><p>حیرت جان سوز بینی عالمی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چند خفسی خیز و ره را ساز کن</p></div>
<div class="m2"><p>یک زمان اندر یقین پرواز کن</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>برگذر زین چار طبع و شش جهت</p></div>
<div class="m2"><p>تا به بینی کل جان، در عافیت</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>کعبهٔ عشاق یزدانست آن</p></div>
<div class="m2"><p>ره نداند برد جسم، الا بجان</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>گر درین رهها، نهٔ تو راه بین</p></div>
<div class="m2"><p>بازمانی دور افتی از یقین</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>هرکسی در مذهب و راهی دگر</p></div>
<div class="m2"><p>هر کسی چون دلو در چاهی دگر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>آن کسان که دیگ سودا میپزند</p></div>
<div class="m2"><p>هر یکی اندر هوای دیگرند</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>آنکه او در قید دنیا مبتلاست</p></div>
<div class="m2"><p>او کجا مستوجب راه بقاست</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>آنکه در تحصیل دنیا باز ماند</p></div>
<div class="m2"><p>در میان چار طبع آز ماند</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>آنکه او مستغرق عرفان بود</p></div>
<div class="m2"><p>بر سر خلق جهان سلطان بود</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>آنکه او شایسته آید پیش شاه</p></div>
<div class="m2"><p>کی تواند کرد در غیری نگاه</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>آنکه او در صحبت سلطان بود</p></div>
<div class="m2"><p>هرچه گوید پادشه را آن بود</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>آرزویی نکندت تا جان شوی</p></div>
<div class="m2"><p>بعد از آن شایستهٔ جانان شوی</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>راز سلطان گوش داری در دلست</p></div>
<div class="m2"><p>حل شود اینجایگه هر مشکلست</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>هرکه او در پیش شه شد راز دار</p></div>
<div class="m2"><p>زان توان دانست هر ترتیب کار</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>راه کن تا بر در سلطان شوی</p></div>
<div class="m2"><p>ورنه تو چون چرخ سرگردان شوی</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>راه کن در گفت و گوی تن ممان</p></div>
<div class="m2"><p>سرّ اسرار حرم را باز دان</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>چارهٔ در رفتن این ره بجوی</p></div>
<div class="m2"><p>قافله رفتند و ما در گفت و گوی</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>قافله رفتند و تو در خوابگاه</p></div>
<div class="m2"><p>کی تواند کرد،‌مرد خفته راه</p></div></div>