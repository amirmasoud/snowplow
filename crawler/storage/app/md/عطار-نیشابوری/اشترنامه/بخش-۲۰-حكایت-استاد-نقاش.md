---
title: >-
    بخش ۲۰ - حكایت استاد نقاش
---
# بخش ۲۰ - حكایت استاد نقاش

<div class="b" id="bn1"><div class="m1"><p>بود استادی عجایب ماه وسال</p></div>
<div class="m2"><p>هردم ازنوعی ببازیدی خیال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پردیی در پیش رویش بسته بود</p></div>
<div class="m2"><p>در پس آن پرده او بنشسته بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از صورها مختلف او بی شمار</p></div>
<div class="m2"><p>کرده اندر هر خیالی او نگار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ریسمانی بسته بد بر روی نطع</p></div>
<div class="m2"><p>از صورها جمع کردی پیش نطع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جمله اندر ریسمان دانی فنون</p></div>
<div class="m2"><p>بود نقّاشی عجایب ذوفنون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرچه در عالم بدی از خیر و شر</p></div>
<div class="m2"><p>جملگی کردند آنجا سر بسر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نقش انسانات هم بر کرده بود</p></div>
<div class="m2"><p>نقش حیوانات بی مرکرده بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از وحوش و از طیور و هرچه هست</p></div>
<div class="m2"><p>کرده بود از نیست آنجا گاه هست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از برون پرده آن میباختی</p></div>
<div class="m2"><p>در درون آن کار را میساختی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر سر آن نطع چابک دست بود</p></div>
<div class="m2"><p>هرچه بود او را همه در دست بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرچه در فهم آید و عقل و خیال</p></div>
<div class="m2"><p>کرده بود از نقشها خود بی محال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جمله از یک رنگ امّا مختلف</p></div>
<div class="m2"><p>در عبارت گشته کلی متّصف</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جمله یکسان بود اما اوستاد</p></div>
<div class="m2"><p>هریکی بر گونهٔ دیگر نهاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>داشت صندوقی درون پرده او</p></div>
<div class="m2"><p>جملگی پردخته آنجا کرده او</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون برون کردی صورها را از آن</p></div>
<div class="m2"><p>اوفکندی اندران بند روان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر یک از شکلی مر آنرا جملهٔ</p></div>
<div class="m2"><p>شاد کردی بی محابا جلوهٔ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر یک از نوعی دگر میباختی</p></div>
<div class="m2"><p>هر صور از گونهٔ میساختی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گاه صورت گاه حیوان گاه خود</p></div>
<div class="m2"><p>ساختی او صورتی از نیک و بد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نقش رنگارنگ او بر لون لون</p></div>
<div class="m2"><p>آوریدی او برون بی عون عون</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون ببازیدی بهر کسوت بران</p></div>
<div class="m2"><p>درکشیدی بند آن در خود روان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بگسلانیدی صورها اوستاد</p></div>
<div class="m2"><p>پس بدادی هم در آن ساعت بباد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پس نهادی آن بصندوق اندرون</p></div>
<div class="m2"><p>او فکندی آن بزرگ رهنمون</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اندران صندوق افکندی ورا</p></div>
<div class="m2"><p>کس نمیپرسید ازو این ماجرا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هرکه کردی این سئوال از اوستاد</p></div>
<div class="m2"><p>کز برای چه چنین دادی بباد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از برای چه تو این ها ساختی</p></div>
<div class="m2"><p>خرد کردی عاقبت در باختی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از برای چه تو بر بستی ورا</p></div>
<div class="m2"><p>وز برای چه تو بشکستی ورا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از برای چیست این با ما بگوی</p></div>
<div class="m2"><p>تا چرا کردی و افکندی بگوی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هیچکس او سعی خود باطل کند؟</p></div>
<div class="m2"><p>هیچکس او رنج خود عاطل کند؟</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هیچکس هرگز کند انصاف ده</p></div>
<div class="m2"><p>راست برگو آنگهی بنیاد نه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هرکه میکردی سؤال از اوستاد</p></div>
<div class="m2"><p>او جواب هیچکس را مینداد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چون جواب کس ندادی اندر آن</p></div>
<div class="m2"><p>آن همه راز نهانی بد عیان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خلق را از روی دل دیوانه گشت</p></div>
<div class="m2"><p>آشنا بودند اگر بیگانه گشت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زان صورها لون لون بی عدد</p></div>
<div class="m2"><p>او برون کردی عجایب بی مدد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دیگران مردم شدندی پیش او</p></div>
<div class="m2"><p>گرچه دل خونی بدی از نیش او</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آن همه نقش عجایب در بساط</p></div>
<div class="m2"><p>اوفکندی اندران عین نشاط</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دیگران یکسر همه کردی تباه</p></div>
<div class="m2"><p>صورت و صندوق میکردی نگاه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هم تباهی آوریده اندرو</p></div>
<div class="m2"><p>دیگر آن قوم آمده در گفت و گو</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هیچکس را مر جواب اونبود</p></div>
<div class="m2"><p>هرچه گفتندی صواب او نبود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>عاقبت چون کس نیامد مرد او</p></div>
<div class="m2"><p>جمله میبودند دل پر درد او</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اندران مردم همه میسوختند</p></div>
<div class="m2"><p>هر زمانی آتشی افروختند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بود مردی کامل و بسیار دان</p></div>
<div class="m2"><p>در حقیقت گشته بود او راز دان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بود مردی با کمال و فرّ و هوش</p></div>
<div class="m2"><p>کرده بود او از شراب شوق نوش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>صاحب اسراردانش بود او</p></div>
<div class="m2"><p>صاحب عقل و توانش بود او</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>کار این استاد آنکس فهم داشت</p></div>
<div class="m2"><p>نه چو عقل دیگران او وهم داشت</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>او رموز و راز اودانسته بود</p></div>
<div class="m2"><p>هرچه بد اسرار اودانسته بود</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>یک شبی رفت او بنزد اوستاد</p></div>
<div class="m2"><p>کرد اکرامی و پیشش ایستاد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تاکمال خویشتن حاصل کند</p></div>
<div class="m2"><p>خویش را در نزد او واصل کند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نزد آن صاحب رموز راز شد</p></div>
<div class="m2"><p>یک دمی با او بخلوت ساز شد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>از طریق عزّت او کردش سلام</p></div>
<div class="m2"><p>تا بماند دولت کل احترام</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>پیش استاد جهان او راز گفت</p></div>
<div class="m2"><p>هرچه یکسر بود یکره باز گفت</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>این سؤال از اوستاد آنگاه کرد</p></div>
<div class="m2"><p>تادل خود او از آن آگاه کرد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گفت ای استاد راز کاردان</p></div>
<div class="m2"><p>از حقیقت جمله تو بسیار دان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>این رموز تو کسی نایافته</p></div>
<div class="m2"><p>هریک از نوعی دگر بشتافته</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چشم عالم همچو تو دیگر نیافت</p></div>
<div class="m2"><p>هردم از نوعی دگر گرچه شتافت</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>راز صورت را بمعنی جان شدی</p></div>
<div class="m2"><p>با رموز کلّ خود شادان شدی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>خلق اندر گفت و گوی تو روند</p></div>
<div class="m2"><p>گرچه اندر جست و جوی تو روند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>جملگی در ماجرای خویشتن</p></div>
<div class="m2"><p>جملگی اندر بلای خویشتن</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>جز خیال تو نمیبینند آن</p></div>
<div class="m2"><p>لیک راز تو نمیدانند آن</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>در مقالات تو گفتار هوس</p></div>
<div class="m2"><p>میپزند و مینداند هیچکس</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>کین چنین راز تو از یدّ تو است</p></div>
<div class="m2"><p>این همه نقش از قلم مدّ تواست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>می نداند هیچکس اسرار تو</p></div>
<div class="m2"><p>می نهبیند هیچکس هنجار تو</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>می چه داند هر کسی رمز و رموز</p></div>
<div class="m2"><p>کین نه اسراریست پیدایی هنوز</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>جمله در کار تو حیران آمدند</p></div>
<div class="m2"><p>جمله همچون چرخ سرگردان شدند</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>واقف راز تو چون هرگز نبود</p></div>
<div class="m2"><p>زانک دانائی ترا دیده نبود</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>این زمان بر من رموز تو ز تو</p></div>
<div class="m2"><p>گشت پیدا هر زمانی تو بتو</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>نی من از تو باز خواهم گفت راز</p></div>
<div class="m2"><p>این حدیث از تو نخواهم گفت باز</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>آنچه من دیدم ز تو از دید تو</p></div>
<div class="m2"><p>هم ز دید تو بگویم دید تو</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>از تو دیدم آنچه میبایست دید</p></div>
<div class="m2"><p>از تو خواهم گفت دیدم آنچه دید</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>این همه ازتو بکلی با تواند</p></div>
<div class="m2"><p>باتو گویااند و بی تو با تواند</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>هم کمال تو تو دانی بی شکی</p></div>
<div class="m2"><p>هم ز تو خواهم بگفتن اندکی</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>آنچه بینی راز تو باشد بکل</p></div>
<div class="m2"><p>پس مرا بیرون فکن زین نقش ذل</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>من بدانستم ز بازی های تو</p></div>
<div class="m2"><p>از مقام عشق بازیهای تو</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>هرچه کردی هم ز تو دیدم ترا</p></div>
<div class="m2"><p>نزد دید خویشتن دیدم ترا</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>هر چه کردی آوریدی در بساط</p></div>
<div class="m2"><p>جملهٔ آن نقش کردم احتیاط</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>احتیاط نوع نوعت کردهام</p></div>
<div class="m2"><p>همچو پرده مانده اندر پردهام</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>جمله دیدم هرچه کردی بی خلاف</p></div>
<div class="m2"><p>من یقین دانم نباشد این گزاف</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>جملهٔ صورت ز یکسان کردهای</p></div>
<div class="m2"><p>جمله را یک رنگ همسان کردهای</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>جملهٔ ترکیب هر انواع را</p></div>
<div class="m2"><p>کردهای بر هر صفت اصناع را</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>چون که تو کردی برآوردی برون</p></div>
<div class="m2"><p>از برای دید این نقش فنون</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>بر بساط مملکت کردی روان</p></div>
<div class="m2"><p>از صفت هر جایگه آن را روان</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>عاقبت چون از تمامت باختی</p></div>
<div class="m2"><p>از برای چه تو آن را ساختی</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>چون کنی در عاقبت آن خرد تو</p></div>
<div class="m2"><p>از چه باشد عاقبت دست برد تو</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>سعی چندینی تو بردی اندران</p></div>
<div class="m2"><p>از چه کردی خرد آنرادر جهان</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>اول کردن چه بودت ساختن</p></div>
<div class="m2"><p>عاقبت هم خویش آن را باختن</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>کردن از چه بود و بشکستن ز چه</p></div>
<div class="m2"><p>آوریدن چه و بر بستن ز چه</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>از چه سعی خود کنی باطل چنین</p></div>
<div class="m2"><p>بازگو این راز با این راز بین</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>تا بگویم من بدین خلق جهان</p></div>
<div class="m2"><p>وارهند از گفت و گویش این زمان</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>عاقبت استاد از اسرار حال</p></div>
<div class="m2"><p>مر جوابی گفت از کشف سؤال</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>گفت ای پرسندهٔ زیبا سخن</p></div>
<div class="m2"><p>کار عالم نیست پیدا سر زبن</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>نیک کردی این سؤال لامحال</p></div>
<div class="m2"><p>من بگویم درجواب این سؤال</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>این سؤال تو نکو کردی ز من</p></div>
<div class="m2"><p>من جواب تو بگویم بی سخن</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>گوش هوشت باز کن سوی سؤال</p></div>
<div class="m2"><p>تا جوابت بشنوی در کل حال</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>این سؤال از من که کردی زین همه</p></div>
<div class="m2"><p>راز من یک جزو بودی زین همه</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>نیک فهمی داری و خوش گفت تو</p></div>
<div class="m2"><p>وین در اسرار کردی سفت تو</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>اول اصل من ز من تو گوش کن</p></div>
<div class="m2"><p>گر توانائی ازین می نوش کن</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>اول کار خود از من بازدان</p></div>
<div class="m2"><p>آنگهی تو از حقیقت راز دان</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>اول از پندار عقل آیی برون</p></div>
<div class="m2"><p>تابدانی سرّ اسرارم کنون</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>اول این اصل باید کرد حل</p></div>
<div class="m2"><p>تا نباشد کار کلی برحیل</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>اول این ترتیب اگر حاصل کنی</p></div>
<div class="m2"><p>تا چو آنها خویشتن بیدل کنی</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>همچو ایشان تو مشو در گفتگو</p></div>
<div class="m2"><p>لیک مر این سر شنو باجستجو</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>این چنین اسرار مشکل حل بکن</p></div>
<div class="m2"><p>چون شکر در آب خود را حل بکن</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>سرّ اسرارت ز من گردد یقین</p></div>
<div class="m2"><p>هم ز من بشنو ز من ای راز بین</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>این همه نقش مخالف از صور</p></div>
<div class="m2"><p>من بکردم هر یک از لونی دیگر</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>هر یک از لونی دگر برساختم</p></div>
<div class="m2"><p>هر یک از نقشی دگر پرداختم</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>هریک از شانی دگر آوردهام</p></div>
<div class="m2"><p>هر یک از نوعی دگر من کردهام</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>هر یکی برکسوتی کردم روان</p></div>
<div class="m2"><p>هر یکی بر یک صفت کردم عیان</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>هرچه رنگ آنجا مخالف آمدست</p></div>
<div class="m2"><p>در همه جمله موالف آمدست</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>رنگ آنجا مختلف بر مختلف</p></div>
<div class="m2"><p>صورت و معنی بیاید متّصف</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>من همه ترکیب کردم از قیاس</p></div>
<div class="m2"><p>جمله بر ترتیب کن آن را قیاس</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>من همه پرداختم از بهر کار</p></div>
<div class="m2"><p>تا تماشایی بود در روزگار</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>چون تماشا بود هم آمد به علم</p></div>
<div class="m2"><p>از تماشا گشت کلی راه علم</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>هرچه علمست آن و جهلست از یقین</p></div>
<div class="m2"><p>علم و جهل از یکدگر آمد به بین</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>جهل و علم از یکدگر آمد پدید</p></div>
<div class="m2"><p>این بدان و آن بدین آمد پدید</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>تا نباشد جهل علم آنگه نبود</p></div>
<div class="m2"><p>علم از جهل آمدت اندر نمود</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>گرچه علم و جهل حاضر آمدند</p></div>
<div class="m2"><p>هر یکی در کار ناظر آمدند</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>علم باید گرچه مرد اهل آمدست</p></div>
<div class="m2"><p>تابدانی کاخرش جهل آمدست</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>علم صورت هیچ باشد بی خلاف</p></div>
<div class="m2"><p>علم معنی هست معنی بی گزاف</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>علم معنی آن نگردد مختلف</p></div>
<div class="m2"><p>علم معنی میشود زین متّصف</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>این همه صورت که من آراستم</p></div>
<div class="m2"><p>این همه از دید خود پیراستم</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>این همه صورت که اعیان کردهام</p></div>
<div class="m2"><p>هر یکی رنگی دگرسان کردهام</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>این همه صورت ز معنی فاش شد</p></div>
<div class="m2"><p>بود معنی نقش صورتهاش شد</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>سالها ترتیب کردم جمله را</p></div>
<div class="m2"><p>تا بدانستم اساس جمله را</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>سالها بنیاد اینها کردهام</p></div>
<div class="m2"><p>سعی بی حد اندرین ها بردهام</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>چون منم نقّاش هم صورت گرم</p></div>
<div class="m2"><p>هرچه سازم آن به بینم بنگرم</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>چون منم نقّاش از روی حساب</p></div>
<div class="m2"><p>آورم شان میبرم اندر حجاب</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>چون منم نقّاش هم استاد هم</p></div>
<div class="m2"><p>من کنم این جمله را بنیاد هم</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>چون همه من میکنم من باشم او</p></div>
<div class="m2"><p>این همه پیدا ز من شد گفتگو</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>من چه غم دارم از اینهای دگر</p></div>
<div class="m2"><p>بهتر از لونی کنم لونی دگر</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>چون منم سازندهٔ کار نخست</p></div>
<div class="m2"><p>بشکنم آنگه کنم کلی درست</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>چون منم دانندهٔ این کار را</p></div>
<div class="m2"><p>کی بود ترسی ز هر گفتار را</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>چون منم بر جزو و کل این صور</p></div>
<div class="m2"><p>حاضر و پیدا کننده سر بسر</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>پرده من دارم درون پرده هم</p></div>
<div class="m2"><p>پا و سر در پردهام گم کردهام</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>من برون آرم بهر نوعی که هست</p></div>
<div class="m2"><p>هم بیارم هم کنم آن جمله پست</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>هم بگویم راز و هم گویم بتو</p></div>
<div class="m2"><p>سرّ خود را باز گویم هم بتو</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>هم منم هم خود مرا معلوم گشت</p></div>
<div class="m2"><p>راز من هم مر مرا مفهوم گشت</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>هیچکس رازم نمیداند یقین</p></div>
<div class="m2"><p>در گمان افتاده کی یابد یقین</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>درگمان این راز هرگز پی نبرد</p></div>
<div class="m2"><p>آنکه یابد عاقبت او پی ببرد</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>در زمان این راز گردد فاش تو</p></div>
<div class="m2"><p>آنگهی پیدا شود نقّاش تو</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>در یقین آنگه به بینی روی وی</p></div>
<div class="m2"><p>چون بری این راز را کلی بوی</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>راز ما را کژ مبین ره گم مکن</p></div>
<div class="m2"><p>خویشتن را در صف مردم بکن</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>هرکه رازم یافت او دیوانه گشت</p></div>
<div class="m2"><p>از خرد یکبارگی بیگانه گشت</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>کار من از راز من پیدا بشد</p></div>
<div class="m2"><p>این زمان جانها ازین شیدا بشد</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>من همی دانم چه کردم چون شدم</p></div>
<div class="m2"><p>من ازین پرده همه بیرون شدم</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>بشکنم آن را به آخر من همان</p></div>
<div class="m2"><p>بار دیگر من برون آرم از آن</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>این نه اینست و نه آنست آن بدان</p></div>
<div class="m2"><p>رمز من کس را نباشد ترجمان</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>رمز من اینجا ز اسرار قدم</p></div>
<div class="m2"><p>آمده تا تو بدانی زد قدم</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>رمز من ز اسرار من گردد عیان</p></div>
<div class="m2"><p>از شکستن هم مرا آید بیان</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>این عیان صورت تو بشکنم</p></div>
<div class="m2"><p>بردن و آوردن آن روشنم</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>روشنم آمد نباشد روشنت</p></div>
<div class="m2"><p>تا نه پنداری بکلی جوشنت</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>تو سفر داری کنون در گفت و گو</p></div>
<div class="m2"><p>حالیا میباش اندر جست و جو</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>روشن آنگه میشود کو بشکند</p></div>
<div class="m2"><p>زین همه گشتن از آنجا وارهد</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>روشن آنگه میشود کو خرده گشت</p></div>
<div class="m2"><p>هم بصورت هم بمعنی مرده گشت</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>روشن آنگه میشود کو خود نبود</p></div>
<div class="m2"><p>آن زمان پیدا شود نابود و بود</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>اوستاد آبگینه گر ببین</p></div>
<div class="m2"><p>زو ببین اسرار و آنگه زو ببین</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>چون کند یک شیشه آنگه بشکند</p></div>
<div class="m2"><p>آنگهی بازش بپرده در برد</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>شیشه دیگر برون آرد لطیف</p></div>
<div class="m2"><p>جوهری شفّاف بس نغز و شریف</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>جوهر دیگر برون آرد دگر</p></div>
<div class="m2"><p>ور دگر خواهی دگر آرد دگر</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>جملگی یک آبگینه بود آن</p></div>
<div class="m2"><p>هر یک از نوعی دگر بنمود آن</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>هر یک از لونی دگر آرد برون</p></div>
<div class="m2"><p>اوستاد جلد سازد پر فنون</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>جوهرش یکیست اما بیشها</p></div>
<div class="m2"><p>میکند هر نوع نوعی شیشه ها</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>چون همه یکیست اندر اصل کار</p></div>
<div class="m2"><p>شیشهها آرد تفاوت بی شمار</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>شیشههای بی تفاوت آورد</p></div>
<div class="m2"><p>ور بخواهد او بکلی بشکند</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>ور بخواهد همچنان بگذاردش</p></div>
<div class="m2"><p>باز از نوعی دگر باز آردش</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>هرچه زینسان میکند او کرده است</p></div>
<div class="m2"><p>رنج بی حد اندر آن او برده است</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>چونکه خود سازد یقین داند که اوست</p></div>
<div class="m2"><p>از چه این کلی زبانها گفتگوست</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>چون همه من کردم و کردم خراب</p></div>
<div class="m2"><p>هم من از من مر مرا گویم جواب</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>من همی دانم که این اسرار چیست</p></div>
<div class="m2"><p>نقش این پرده درین پرگار چیست</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>خرد گردانم تمامت نقش ها</p></div>
<div class="m2"><p>هیچ انجا باز مینکنم رها</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>راز خود با تو بگویم زین همه</p></div>
<div class="m2"><p>تاترا مقصود جویم زین همه</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>تا جهان بر گفتگوی من شود</p></div>
<div class="m2"><p>جملگی در جستجوی من شود</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>تا مرا بشناسد این عقل فضول</p></div>
<div class="m2"><p>گرچه آن جا میکند ردّ و قبول</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>تا مراد خود ز خود باقی کنم</p></div>
<div class="m2"><p>عاقبت آن جمله در باقی کنم</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>رازهای دیگرم در پرده است</p></div>
<div class="m2"><p>هیچکس آن را زحل ناکرده است</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>آنچه من بنمودم آن جا اندکی</p></div>
<div class="m2"><p>بیش ازین دیگر نباشد اندکی</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>آنچه ما را در نهان پرده است</p></div>
<div class="m2"><p>پرده اندر پرده اندر پرده است</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>از پس پرده اگر یابی همه</p></div>
<div class="m2"><p>راز ما را کل تو دریابی همه</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>لیک این معنی مکن بر کس تو فاش</p></div>
<div class="m2"><p>معنیم بنگر تو صورت بین مباش</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>صورتت بشکن که تا تو بنگری</p></div>
<div class="m2"><p>آنگهی از راز ما تو برخوری</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>گر تو از راز درون آگه شوی</p></div>
<div class="m2"><p>گرچه بی راهی ولی یاره شوی</p></div></div>
<div class="b" id="bn180"><div class="m1"><p>راز من چون بر تو گردد جمله فاش</p></div>
<div class="m2"><p>از عذاب جان و دل ایمن مباش</p></div></div>
<div class="b" id="bn181"><div class="m1"><p>دست من بر دست خود نه استوار</p></div>
<div class="m2"><p>بعد از آن تو سرّ ما کن آشکار</p></div></div>
<div class="b" id="bn182"><div class="m1"><p>یک زمان در پردهٔ ما در خرام</p></div>
<div class="m2"><p>یک زمانی بگذر از این ننگ و نام</p></div></div>
<div class="b" id="bn183"><div class="m1"><p>نام و ننگ خود بکلی در فکن</p></div>
<div class="m2"><p>صورت خود خرد اندر هم شکن</p></div></div>
<div class="b" id="bn184"><div class="m1"><p>این صورت را کن بکلی خرد تو</p></div>
<div class="m2"><p>تا که بر شیطان نماند عضو تو</p></div></div>
<div class="b" id="bn185"><div class="m1"><p>از خیال خویشتن آیی برون</p></div>
<div class="m2"><p>در درون پرده آیی از برون</p></div></div>
<div class="b" id="bn186"><div class="m1"><p>آن خیال آنجا که تودیدی همی</p></div>
<div class="m2"><p>آن خیال از نقل آمد یک دمی</p></div></div>
<div class="b" id="bn187"><div class="m1"><p>در درون آیی همه آهنگ کن</p></div>
<div class="m2"><p>نام خود بردار و خود بی ننگ کن</p></div></div>
<div class="b" id="bn188"><div class="m1"><p>در درون پرده شو واقف ز ما</p></div>
<div class="m2"><p>تات بنمائیم هر دم جایها</p></div></div>
<div class="b" id="bn189"><div class="m1"><p>در درون پرده عزّت خرام</p></div>
<div class="m2"><p>در درون پرده وحدت خرام</p></div></div>
<div class="b" id="bn190"><div class="m1"><p>خاص آنجا شو اگر خواهی خلاص</p></div>
<div class="m2"><p>تا شوی اندر درون پرده خاص</p></div></div>
<div class="b" id="bn191"><div class="m1"><p>پرده بردار و بیا اسرار بین</p></div>
<div class="m2"><p>هر دم از نوعی دگر گفتار بین</p></div></div>
<div class="b" id="bn192"><div class="m1"><p>پرده بردار و ببین راز مرا</p></div>
<div class="m2"><p>این همه تمکین و اعزاز مرا</p></div></div>
<div class="b" id="bn193"><div class="m1"><p>در درون پرده صاحب راز شو</p></div>
<div class="m2"><p>آنگهی در سوی ایشان باز شو</p></div></div>
<div class="b" id="bn194"><div class="m1"><p>آن همه صورت که دید آن زمان</p></div>
<div class="m2"><p>دیگر از نوعی دگر بینی عیان</p></div></div>
<div class="b" id="bn195"><div class="m1"><p>آن دگر از صورت دیگر ببین</p></div>
<div class="m2"><p>کل طلب کل جوی کل شو کل ببین</p></div></div>
<div class="b" id="bn196"><div class="m1"><p>صورت خود از میان برداریی</p></div>
<div class="m2"><p>راز خود آنگه بکل دریابی</p></div></div>
<div class="b" id="bn197"><div class="m1"><p>راز ما در پرده دل باز بین</p></div>
<div class="m2"><p>آنگهی تو معنی و اعزاز بین</p></div></div>
<div class="b" id="bn198"><div class="m1"><p>در زمان و در مکان آی و برو</p></div>
<div class="m2"><p>در مکان اندر زمان آی و برو</p></div></div>
<div class="b" id="bn199"><div class="m1"><p>از مکان و از زمان شو تو برون</p></div>
<div class="m2"><p>تا بیابی راز ما بی چه و چون</p></div></div>
<div class="b" id="bn200"><div class="m1"><p>راز ما دریاب آنگه کل بباش</p></div>
<div class="m2"><p>چون شوی تو کل بکل بی دل بباش</p></div></div>
<div class="b" id="bn201"><div class="m1"><p>هر که کل شد جزو را با او چه کار</p></div>
<div class="m2"><p>و آنکه جان شد عضو را با او چه کار</p></div></div>
<div class="b" id="bn202"><div class="m1"><p>کل شوی آنگاه چون بینی تو راز</p></div>
<div class="m2"><p>اولین یابی بآخر هم تو باز</p></div></div>
<div class="b" id="bn203"><div class="m1"><p>هرکه ساز کوی ما سازد بکل</p></div>
<div class="m2"><p>اول از پندار افتد او بذل</p></div></div>
<div class="b" id="bn204"><div class="m1"><p>هر که خواهد از وصال ما دمی</p></div>
<div class="m2"><p>حیرت جان سوز بیند عالمی</p></div></div>
<div class="b" id="bn205"><div class="m1"><p>یک زمان اندر درون پرده آی</p></div>
<div class="m2"><p>پردهٔ راز خود از پرده گشای</p></div></div>
<div class="b" id="bn206"><div class="m1"><p>مرد ره بین چون زاستاد این شنید</p></div>
<div class="m2"><p>روی استاد حقیقی باز دید</p></div></div>
<div class="b" id="bn207"><div class="m1"><p>روی او میدید و او پنهان شده</p></div>
<div class="m2"><p>در پس آن پرده او حیران شده</p></div></div>
<div class="b" id="bn208"><div class="m1"><p>گفت ای استاد دور از انقلاب</p></div>
<div class="m2"><p>از چه افکندی مرا در اضطراب</p></div></div>
<div class="b" id="bn209"><div class="m1"><p>راه ده اندر درون پردهام</p></div>
<div class="m2"><p>زانکه بی توراه را گم کردهام</p></div></div>
<div class="b" id="bn210"><div class="m1"><p>راه ده تا من درآیم سوی تو</p></div>
<div class="m2"><p>چون درآیم من ببینم روی تو</p></div></div>
<div class="b" id="bn211"><div class="m1"><p>گر دهی راهم بیابم دور چرخ</p></div>
<div class="m2"><p>چون دهی را هم رسم در غور چرخ</p></div></div>
<div class="b" id="bn212"><div class="m1"><p>پرده عشق ترا دوری کنم</p></div>
<div class="m2"><p>بیخ غم از جان و ازدل برکنم</p></div></div>
<div class="b" id="bn213"><div class="m1"><p>حاجبی آمد برون از پرده او</p></div>
<div class="m2"><p>ایستاد ودست او بگرفت او</p></div></div>
<div class="b" id="bn214"><div class="m1"><p>گفت بسم اللّه که استاد جهان</p></div>
<div class="m2"><p>میبرد اینجا ترادر میهمان</p></div></div>
<div class="b" id="bn215"><div class="m1"><p>یک زمان در اندرون آی از برون</p></div>
<div class="m2"><p>تاترا باشم در آنجا رهنمون</p></div></div>
<div class="b" id="bn216"><div class="m1"><p>دست او بگرفت وشد در پرده باز</p></div>
<div class="m2"><p>اوفکند آن لحظه از هم پرده باز</p></div></div>
<div class="b" id="bn217"><div class="m1"><p>چون درون پرده شد بیخویشتن</p></div>
<div class="m2"><p>درگذشته ازوجود و جان و تن</p></div></div>
<div class="b" id="bn218"><div class="m1"><p>عالم صغری چو در کبری فتاد</p></div>
<div class="m2"><p>راز او کلّی در آن عالم گشاد</p></div></div>
<div class="b" id="bn219"><div class="m1"><p>راه کلی پرده اندر پرده بود</p></div>
<div class="m2"><p>لیک آن راه از صفت گم کرده بود</p></div></div>
<div class="b" id="bn220"><div class="m1"><p>حاجب از چشمش نهان شد در زمان</p></div>
<div class="m2"><p>مرد را لرزی درآمد در نهان</p></div></div>
<div class="b" id="bn221"><div class="m1"><p>ناگهان الحاح استاد او شنید</p></div>
<div class="m2"><p>لیک مر استاد را آنجا ندید</p></div></div>