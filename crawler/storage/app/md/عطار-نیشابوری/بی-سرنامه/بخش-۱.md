---
title: >-
    بخش ۱
---
# بخش ۱

<div class="b" id="bn1"><div class="m1"><p>من بغیر از تو نه‌بینم درجهان</p></div>
<div class="m2"><p>قادرا پروردگارا جاودان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من ترا دانم ترا دانم ترا</p></div>
<div class="m2"><p>حق ترا کی غیر باشد ای خدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون به جز تو نیست در هر دو جهان</p></div>
<div class="m2"><p>لاجرم غیری نباشد در میان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اولین و آخرین وای احد</p></div>
<div class="m2"><p>ظاهرین و باطنین و بی عدد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این جهان و آن جهان و در نهان</p></div>
<div class="m2"><p>آشکارا در نهان و در عیان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم عیان و هم نهان پیدا توئی</p></div>
<div class="m2"><p>هم درون گنبد خضرا توئی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در ازل بودی و باشی همچنان</p></div>
<div class="m2"><p>تا ابد هستی و باشی جاودان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای ز تو پیدا شده کون و مکان</p></div>
<div class="m2"><p>ای ز تو پیدا شده جان و جهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای ز تو عالم پر از غوغا شده</p></div>
<div class="m2"><p>جان پاکان در رهت یغما شده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای ز تو چرخ فلک گردان شده</p></div>
<div class="m2"><p>صدهزاران دل ز تو حیران شده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای ز وصلت عاشقان دلسوخته</p></div>
<div class="m2"><p>جامهٔ وصل تو هر دم دوخته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای ز وصلت کار بازار آمده</p></div>
<div class="m2"><p>همچو ابراهیم در نار آمده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای ز وصلت جانها اندر فغان</p></div>
<div class="m2"><p>همچو موسی درجواب لن تران</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای ز وصلت جانها بریان شده</p></div>
<div class="m2"><p>همچو اسمعیل صید قربان شده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای ز وصلت زاهدان در تهنیت</p></div>
<div class="m2"><p>همچو داود نبی در تعزیت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای ز وصلت عالمان در گیر و دار</p></div>
<div class="m2"><p>چون سلیمان پادشاهی ملک دار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای ز وصلت جان ما تاراج یافت</p></div>
<div class="m2"><p>چون محمد یک شب معراج یافت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای ز وصلت عاشقان آشفته کار</p></div>
<div class="m2"><p>همچو عیسی آمده از پای دار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای ز وصلت آسمان گردان شده</p></div>
<div class="m2"><p>اندرین ره راه بی‌پایان شده</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای ز وصلت کوکبان اندر طلب</p></div>
<div class="m2"><p>می‌نیاسایند هرگز از تعب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای ز وصلت آفتاب اندر سما</p></div>
<div class="m2"><p>غلط غلطان می‌رود بی سر و پا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای ز وصلت خاک را خون در جگر</p></div>
<div class="m2"><p>هر زمان سردگر کرده بدر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای ز وصلت آب در کار آمده</p></div>
<div class="m2"><p>هر زمان هر سو پدیدار آمده</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای ز وصلت شد فریدت غرق خون</p></div>
<div class="m2"><p>هر زمان در خاک افتد سرنگون</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای ز وصلت آتش از غم سوخته</p></div>
<div class="m2"><p>اندر آن دم سنگ بر سر کوفته</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای ز وصلت هر زمان حیران شدم</p></div>
<div class="m2"><p>در تحیر سر بسر گردانشدم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای ز وصلت غرق توحید آمدم</p></div>
<div class="m2"><p>لاجرم در عین تجرید آمدم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>من توام تو من نه من جمله توئی</p></div>
<div class="m2"><p>محو کردم در تو مائی و توئی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خود یکی بود و نبود او را دوئی</p></div>
<div class="m2"><p>از منی هر چیز هم اینجا توئی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>من بوصلت عارفی مطلق شدم</p></div>
<div class="m2"><p>عارفی رفته تمامی حق شدم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>من خدایم من خدایم من خدا</p></div>
<div class="m2"><p>فارغم از کبر و کینه وز هوا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سر بی سرنامه را پیدا کنم</p></div>
<div class="m2"><p>عاشقان را در جهان شیدا کنم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>صد هزاران خلق حیران مانده‌اند</p></div>
<div class="m2"><p>اندرین ره نوح گریان مانده‌اند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>صد هزاران عارفان در گفتگو</p></div>
<div class="m2"><p>اندرین ره لوح دل در شست و شو</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>عاشقان آتش زنند در هر دو کون</p></div>
<div class="m2"><p>تا رهی زین نقشهای لون لون</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نقشها را جمله در آتش بسوز</p></div>
<div class="m2"><p>بعد از آن شمع وصالش برفرور</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چون نماند نقشها اندر نهان</p></div>
<div class="m2"><p>آن زمان نقاش را بینی عیان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>با تو گویم سر اسرار نهان</p></div>
<div class="m2"><p>ای برادر نقش را نقاش دان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چون ترا باشد کمال دین به حق</p></div>
<div class="m2"><p>خویش را هرگز نبینی جز به حق</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>جملگی اعضای تو ای بی خبر</p></div>
<div class="m2"><p>ذات کلی این جهان را سر به سر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>عرش و فرش و لوح کرسی و قلم</p></div>
<div class="m2"><p>از توشان شد اسم در عالم علم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گوهری جان در هوس تو کردهٔ</p></div>
<div class="m2"><p>با سگی و جاهلی خوکردهٔ</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دادهٔ بر باد عمر جاودان</p></div>
<div class="m2"><p>یک زمان آگه نهٔ از سرجان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چون شوی آگه ز سر خویشتن</p></div>
<div class="m2"><p>ترک گیری ازحدیث ما و من</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>جمله را یک بینی ای مرد خدای</p></div>
<div class="m2"><p>تا نه‌بینی ای پسر رشته دوتای</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گر تو راه عشق را مایل شوی</p></div>
<div class="m2"><p>یک ره و یک کعبه و یک دل شوی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ننگری در هیچ سوای مردکار</p></div>
<div class="m2"><p>دایما در عشق باشی بی‌قرار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>عشق جانان جوهر جان آمده است</p></div>
<div class="m2"><p>لاجرم از خلق پنهان آمده است</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>هست پیدا نیک تنها از شما</p></div>
<div class="m2"><p>کی بود خفاش را تاب ضیا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>این جهان و آن جهان با هم ببین</p></div>
<div class="m2"><p>بگذر از راه گمان و از یقین</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>عشق با انسان و آن آمیخته</p></div>
<div class="m2"><p>روح اندر خاک دان آویخته</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گفتم ای آرام جان عاشقان</p></div>
<div class="m2"><p>هم شوی درمان درون جسم و جان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ای جمالت عاشقان نشناخته</p></div>
<div class="m2"><p>مرکب معنی درین ره تاخته</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ای وصالت سالکان را رهروان</p></div>
<div class="m2"><p>جمله در آیند از ره بی نشان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ای وصالت صادقان صادق شده</p></div>
<div class="m2"><p>در طریق عشق خود لائق شده</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ای وصالت عالمان درهای و هوی</p></div>
<div class="m2"><p>در ره تقلید بشکافند موی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ای وصالت اولیا را داد حال</p></div>
<div class="m2"><p>دأب ایشان ماورای قیل و قال</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ای وصالت آسمان و هم زمین</p></div>
<div class="m2"><p>هست در تسبیح رب العالمین</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ای وصالت شمس را دریافته</p></div>
<div class="m2"><p>نور او در جمله عالم یافته</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ای وصالت ماه را هاله زده</p></div>
<div class="m2"><p>گاه بدروگه هلالی بر زده</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ای وصالت باد و آتش را به هم</p></div>
<div class="m2"><p>داد وصلت از ره لطف و کرم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ای وصالت بحر را بگداخته</p></div>
<div class="m2"><p>هر زمان درد دگر پرداخته</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ای وصالت کرد آب و خاک را</p></div>
<div class="m2"><p>داد قدسی روح قدس پاک را</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ای وصالت کوه را در گل زده</p></div>
<div class="m2"><p>صد هزاران عاربش بر دل زده</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ای وصالت سر دریای قدم</p></div>
<div class="m2"><p>صد هزاران درّ آرد از عدم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ای وصالت آشکارا و نهان</p></div>
<div class="m2"><p>ای وصالت بی بیان و بی عیان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ای وصالت انبیا و اولیا</p></div>
<div class="m2"><p>ای وصالت عاشقان و اصفیا</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ای وصالت زاهدان و مخلصان</p></div>
<div class="m2"><p>ای وصالت نیستی و هستیان</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ای وصالت هست گشته در جهان</p></div>
<div class="m2"><p>ای وصالت هست پیدا ونهان</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ای وصالت از جهان بیرون شده</p></div>
<div class="m2"><p>ای وصالت عالم بیچون شده</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ای وصالت هر دو عالم سوخته</p></div>
<div class="m2"><p>ای وصالت خان و مانم سوخته</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>عالمان در علم اودرمانده‌اند</p></div>
<div class="m2"><p>عارفان از عرف او وامانده‌اند</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>عاشقان از عشق او حیران شدند</p></div>
<div class="m2"><p>هر دم از نوعی دگر بی جان شدند</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>زاهدان از زهد او رسوا شدند</p></div>
<div class="m2"><p>در خیال زهد او شیدا شدند</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بعد پنجه سال او اسرار یافت</p></div>
<div class="m2"><p>از فریدالدین لقب عطار یافت</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>سر بیسرنامه را پیدا کنم</p></div>
<div class="m2"><p>عاشقان رادرجهان شیدا کنم</p></div></div>