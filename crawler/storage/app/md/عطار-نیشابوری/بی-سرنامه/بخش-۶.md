---
title: >-
    بخش ۶
---
# بخش ۶

<div class="b" id="bn1"><div class="m1"><p>این سخن را از ره مردی شنو</p></div>
<div class="m2"><p>تا نمانی در قیامت در گرو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوهر عشق از تو پیدا می‌شود</p></div>
<div class="m2"><p>هر دوعالم در دلت یکتا شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی تو در شک نامده درّ یقین</p></div>
<div class="m2"><p>بگذری ازکفر و از اسلام و دین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن زمان تو عشق را لائق شوی</p></div>
<div class="m2"><p>عشق حق را عاشق صادق شوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر مرا از عشق تو باشد خبر</p></div>
<div class="m2"><p>مرتدی باشیم و در ره بی خبر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن چنان خواهم که کلی گم شوی</p></div>
<div class="m2"><p>تا ز پستی آدم مردم شوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ورنه همچون زاهدان کور و کر</p></div>
<div class="m2"><p>چون ز هستی خودت باشد خبر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کی توانم کرد پنهان دود را</p></div>
<div class="m2"><p>من نه زهر کاشته نمرود را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بحر معنی بی‌نهایت آمده</p></div>
<div class="m2"><p>لاشکی بی حد و غایت آمده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یافتم یک قطره از بحر صفا</p></div>
<div class="m2"><p>ز آن بر آمد هر زمانی موج‌ها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>راه توحید عیانی داشتم</p></div>
<div class="m2"><p>گنج اسرار نهانی داشتم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>راه حق را صادق عشق آمدم</p></div>
<div class="m2"><p>حق حق است حق مطلق آمدم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من خدایم من خدایم من خدا</p></div>
<div class="m2"><p>فارغم از کبر و کینه وز هوا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سر بیسر نامه را پیدا کنم</p></div>
<div class="m2"><p>عاشقان رادرجهان شیدا کنم</p></div></div>