---
title: >-
    بخش ۲ - الحكایة و التمثیل
---
# بخش ۲ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>کاملی گفتست از اهل یقین</p></div>
<div class="m2"><p>گر جهودان جمله بگزینند دین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان مرا چندان نیاید دلخوشی</p></div>
<div class="m2"><p>کز سر دردی کسی بی سرکشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ره این درد آید دردناک</p></div>
<div class="m2"><p>هم درین دردش بود رفتن بخاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زیسته در درد و رفته هم بدرد</p></div>
<div class="m2"><p>رفته زین عالم بدان عالم بدرد</p></div></div>