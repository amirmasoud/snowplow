---
title: >-
    بخش ۴ - الحكایة و التمثیل
---
# بخش ۴ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>آن یکی درخواند مجنون را ز راه</p></div>
<div class="m2"><p>گفت اگر خواهی تو لیلی را بخواه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت هرگز مینباید زن مرا</p></div>
<div class="m2"><p>بس بود این زاری و شیون مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت او را چون نمیخواهی برت</p></div>
<div class="m2"><p>این همه سودا برون کن از سرت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یاد خوشتر گفت از لیلی مرا</p></div>
<div class="m2"><p>سرکشی او را و واویلی مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مغز عشق عاشقان یادی بود</p></div>
<div class="m2"><p>هرچه بگذشتی ازین بادی بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من نیم زان عاشقی شهوت پرست</p></div>
<div class="m2"><p>تا کنم خالی زیاد دوست دست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تاکه باشد یاد غیری در حساب</p></div>
<div class="m2"><p>ذکر مولی باشد از تو در حجاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون همه یاد تو از مولی بود</p></div>
<div class="m2"><p>همچو مجنونت همه لیلی بود</p></div></div>