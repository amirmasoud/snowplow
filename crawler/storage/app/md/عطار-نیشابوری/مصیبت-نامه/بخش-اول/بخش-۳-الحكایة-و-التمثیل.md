---
title: >-
    بخش ۳ - الحكایة و التمثیل
---
# بخش ۳ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>مار افسایی یکی حربه بدست</p></div>
<div class="m2"><p>کرده بد بر مار سوراخی نشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر زمان میساخت معجونی دگر</p></div>
<div class="m2"><p>هر نفس میخواند افسونی دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناگهی عیسی برانجا درگذشت</p></div>
<div class="m2"><p>مار آمد پیش او در سر گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت ای روح اللّه ای شمع انام</p></div>
<div class="m2"><p>هست سیصد سال عمر من تمام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرد سی ساله مرا افسون کند</p></div>
<div class="m2"><p>تا زسوراخم مگر بیرون کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رفت عیسی عاقبت زانجایگاه</p></div>
<div class="m2"><p>چوندگر باره فرود آمد براه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرد را گفتا چه کردی کار را</p></div>
<div class="m2"><p>گفت اندر سله کردم مار را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد سر آن سله عیسی برگرفت</p></div>
<div class="m2"><p>چون بدید او را سخن از سرگرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت ای مار از چه طاعت داشتی</p></div>
<div class="m2"><p>خاصه چندانی شجاعت داشتی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن همه دعوی که کردی از نخست</p></div>
<div class="m2"><p>از چه افتادی چنین در دام سست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت من نفریفتم ز افسون او</p></div>
<div class="m2"><p>میتوانستم که ریزم خون او</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لیک چون بسیار حق را نام برد</p></div>
<div class="m2"><p>نام حق خوش خوش مرا در دام برد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون بنام حق شدم در دام او</p></div>
<div class="m2"><p>صد چو جان من فدای نام او</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وصل همچون آتشی جان سوزدت</p></div>
<div class="m2"><p>یاد باید تا جهان افروزدت</p></div></div>