---
title: >-
    بخش ۶ - الحكایة و التمثیل
---
# بخش ۶ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>پیره زالی برد پیش بوسعید</p></div>
<div class="m2"><p>کودکی را تا بود او را مرید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن جوان در کار مرد آمد ولیک</p></div>
<div class="m2"><p>زرد گشت و ناتوان از ضعف نیک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برگ بی برگی و بی خویشی نداشت</p></div>
<div class="m2"><p>طاقت خواری و درویشی نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاست مرد و شیخ را گفت این زمان</p></div>
<div class="m2"><p>صوفیم ناکرده کردی ناتوان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواستم تا صوفئی گردانیم</p></div>
<div class="m2"><p>همچو خویش از خویشتن برهانیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو مرا در دام مرگ انداختی</p></div>
<div class="m2"><p>کار من جمله ز برگ انداختی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت چون صوفی نشاند بوسعید</p></div>
<div class="m2"><p>صوفی او چون تو باشد ای مرید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لیک چون صوفی نشاند کردگار</p></div>
<div class="m2"><p>لاجرم چون بوسعید آید بکار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرچه آن از من رود چون من بود</p></div>
<div class="m2"><p>دوست از من گر رود دشمن بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>راست ناید صوفئی هرگز بکسب</p></div>
<div class="m2"><p>خر کجاگردد بجد و جهد اسب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لیک اگر دولت رسد ازجای خویش</p></div>
<div class="m2"><p>یک خر عیسی بود صد اسب بیش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جد وجهدت را چو رای دیگرست</p></div>
<div class="m2"><p>صوفئی کردن ز جای دیگرست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جد وجهدت بی ثوابی کی بود</p></div>
<div class="m2"><p>لیک گنجشگی عقابی کی بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر همه عالم ثواب تو بود</p></div>
<div class="m2"><p>تا تو باشی تو عذاب تو بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صوفئی سنگیست مبهوت آمده</p></div>
<div class="m2"><p>سنگ رفته لعل و یاقوت آمده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا بذات اندر تبدل نبودت</p></div>
<div class="m2"><p>جزو باشی ذات تو کل نبودت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در حقیقت گرچه توکل آمدی</p></div>
<div class="m2"><p>لیک این ساعت همه ذل آمدی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر شود ذل تودر کل ناپدید</p></div>
<div class="m2"><p>تو بکلی کل شوی ذل ناپدید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ور بماند ذرهٔ از ذل تو</p></div>
<div class="m2"><p>پس بود آن ذره ذلت غل تو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هست صوفی ذل در کل باخته</p></div>
<div class="m2"><p>ذل و کل در کل کل انداخته</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کل کل در کل کلات آمده</p></div>
<div class="m2"><p>بی صفت بی فعل و بی ذات آمده</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پای ناگاهی فرو رفتن بگنج</p></div>
<div class="m2"><p>پیشهٔ نبود که آموزی برنج</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هست صوفی مرد بی رنج آمده</p></div>
<div class="m2"><p>پای او ناگاه در گنج آمده</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>صوفئی باید ترا اندیشه کن</p></div>
<div class="m2"><p>تا که دانی گنج یابی پیشه کن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>لیک جد و جهد میباید ترا</p></div>
<div class="m2"><p>تادر این گنج بگشاید ترا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زانکه در راهی که سلطانان گنج</p></div>
<div class="m2"><p>گنجها دیدند بی رنج و برنج</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>صد نشان دادند ازان ره پیش تو</p></div>
<div class="m2"><p>تا بجنبد نفس کافر کیش تو</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سر بدان راه آور و مردانه وار</p></div>
<div class="m2"><p>گنج میجو با دلی پرانتظار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زانکه در راهی که گنج آنجا نهند</p></div>
<div class="m2"><p>هیچ نبود شک که رنج آنجا نهند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر تو در راهی دگر پویندهٔ</p></div>
<div class="m2"><p>گنج نیست آنجا که تو جویندهٔ</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در رهی رو کان نشانت دادهاند</p></div>
<div class="m2"><p>جهد کن تا سر بدانت دادهاند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>جهد میکن روز و شب در کوی رنج</p></div>
<div class="m2"><p>بو که ناگاهی ببینی روی گنج</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هان و هان گر گنج دین بینی تو مست</p></div>
<div class="m2"><p>ظن مبر کز جهد تو آمد بدست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زانکه آنجا جهد را مقدار نیست</p></div>
<div class="m2"><p>گنج را جز گنج کس بر کار نیست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هرکرا بنمود آن محض عطاست</p></div>
<div class="m2"><p>وانکه را ننمود از حکم قضاست</p></div></div>