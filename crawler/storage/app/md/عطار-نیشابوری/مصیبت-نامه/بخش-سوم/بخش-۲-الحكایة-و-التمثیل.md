---
title: >-
    بخش ۲ - الحكایة و التمثیل
---
# بخش ۲ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>کرد حاتم را سؤال آن مرد خام</p></div>
<div class="m2"><p>کز کجا آری تو هر روزی طعام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت حاتم تا که جان دارم بجای</p></div>
<div class="m2"><p>هست قوت من ز انبان خدای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرد گفتش تو بسالوس و برنگ</p></div>
<div class="m2"><p>میکنی مال مسلمانان بچنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روز و شب مال مسلمانان بری</p></div>
<div class="m2"><p>چون بخوردی عاقبت را ننگری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حاتمش گفتا که ای مرد عزیز</p></div>
<div class="m2"><p>خوردهام زان تو هرگز هیچ چیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت نه گفتا مسلمان پس نهٔ</p></div>
<div class="m2"><p>تن بزن چون این سخن را کس نهٔ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سایلش گفتا که حجت می میار</p></div>
<div class="m2"><p>گفت حجت خواهد از ما کردگار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت میخواهی که چون کارت خطاست</p></div>
<div class="m2"><p>کاین خطاها را سخن بنهی تو راست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت از هفت آسمان آمد سخن</p></div>
<div class="m2"><p>از خدا هم با میان آمد سخن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مادرت چون شوهری کرد اختیار</p></div>
<div class="m2"><p>شدحلال از یک سخن آغاز کار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سایلش گفتا تو کرده خوش نشست</p></div>
<div class="m2"><p>زاسمان ناید ترا روزی بدست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفت روزی همه خلق جهان</p></div>
<div class="m2"><p>همچو روزی من آید زاسمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کانکه او دارندهٔ جان و جهانست</p></div>
<div class="m2"><p>گفت روزی همه در آسمانست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت دایم پای در دامن ترا</p></div>
<div class="m2"><p>روزئی میناید از روزن ترا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت بودم درشکم نه ماه من</p></div>
<div class="m2"><p>بردم از روزن بروزی راه من</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سائلش گفتا بخسب اکنون ستان</p></div>
<div class="m2"><p>تا درآید روزی تو در دهان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفت من قرب دو سال ای کوربین</p></div>
<div class="m2"><p>بودهام در گاهواره همچنین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>من ستان خفته در آن مهد بزر</p></div>
<div class="m2"><p>در دهانم شیر میریخت از زبر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سایلش گفتا که باید کشت زود</p></div>
<div class="m2"><p>هیچکس ناکشته هرگز چون درود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>حاتمش گفتا که ای سرگشته من</p></div>
<div class="m2"><p>موی سر میبدروم ناکشته من</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفت ناپخته بخور تا بنگرم</p></div>
<div class="m2"><p>گفت ناپخته چو مرغان هم خورم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گفت زیر آب شو روزی طلب</p></div>
<div class="m2"><p>گفت چون ماهی شوم نبود عجب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مرد عاجز گشت ازو حیران بماند</p></div>
<div class="m2"><p>زان سخن انگشت در دندان بماند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عاقبت بر دست حاتم بازگشت</p></div>
<div class="m2"><p>توبه کرد و همدم و همراز گشت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>لطف و رزق حق درین منزل طلب</p></div>
<div class="m2"><p>حل این مشکل درون دل طلب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون همه زانجایگه بینی مدام</p></div>
<div class="m2"><p>کار تو زانجایگه گردد تمام</p></div></div>