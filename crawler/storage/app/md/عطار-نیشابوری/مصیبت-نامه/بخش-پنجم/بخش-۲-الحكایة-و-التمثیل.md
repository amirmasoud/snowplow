---
title: >-
    بخش ۲ - الحكایة و التمثیل
---
# بخش ۲ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>این سخن نقلست در قوت القلوب</p></div>
<div class="m2"><p>زان بزرگ پای دین پاک از عیوب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت هر روز ازملایک عالمی</p></div>
<div class="m2"><p>سوخته گردد ز نور حق همی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز ابتداتا انتهای روزگار</p></div>
<div class="m2"><p>چند دانی نسل آدم را شمار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راست هم چندان بهر روزی ملک</p></div>
<div class="m2"><p>از سماک انگشت گردد تا سمک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میبسوزند این همه روحانیان</p></div>
<div class="m2"><p>پس دگر میآید آنگه در میان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای عجب هر روز چندین سوخته</p></div>
<div class="m2"><p>خیل دیگر خویشتن بر دوخته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون ملایک حاضر و جمع آمدند</p></div>
<div class="m2"><p>سر بسر پروانهٔ شمع آمدند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این همه هر روز میسوزند پاک</p></div>
<div class="m2"><p>دیگران در آرزوی آن هلاک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تاملک کردند آدم را سجود</p></div>
<div class="m2"><p>عشقشان یک ذره آمد در وجود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ره بحق چون جان آدم یافتند</p></div>
<div class="m2"><p>تا ابد در خدمتش بشتافتند</p></div></div>