---
title: >-
    بخش ۲ - الحكایة و التمثیل
---
# بخش ۲ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>آن سگی مرده براه افتاده بود</p></div>
<div class="m2"><p>مرگ دندانش ز هم بگشاده بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوی ناخوش زان سگ الحق میدمید</p></div>
<div class="m2"><p>عیسی مریم چو پیش او رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همرهی را گفت این سگ آن اوست</p></div>
<div class="m2"><p>و آن سپیدی بین که در دندان اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه بدی نه زشت بوئی دید او</p></div>
<div class="m2"><p>و آن همه زشتی نکوئی دید او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پاک بینی پیشه کن گر بندهٔ</p></div>
<div class="m2"><p>پاک بین گر بندهٔ بینندهٔ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جمله را یک رنگ و یک مقدار بین</p></div>
<div class="m2"><p>مار مهره بین نه مهره ماربین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم نکوئی هم نکوکاری گزین</p></div>
<div class="m2"><p>مهربانی و وفاداری گزین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر خدا را میشناسی بنده باش</p></div>
<div class="m2"><p>حق گزار نعمت دارنده باش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نعمت او میخوری در سال و ماه</p></div>
<div class="m2"><p>حق آن نعمت نمیداری نگاه</p></div></div>