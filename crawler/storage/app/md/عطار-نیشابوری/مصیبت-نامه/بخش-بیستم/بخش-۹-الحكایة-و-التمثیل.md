---
title: >-
    بخش ۹ - الحكایة و التمثیل
---
# بخش ۹ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>در رهی میرفت شبلی دردناک</p></div>
<div class="m2"><p>دید دو کودک در افتاده بخاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زانکه جوزی در میان افتاده بود</p></div>
<div class="m2"><p>هر دو را دعوی آن افتاده بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر دو از یک جوز میکردند جنگ</p></div>
<div class="m2"><p>شیخ گفتا کرد میباید درنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا من این جوز محقر بشکنم</p></div>
<div class="m2"><p>پس میان هر دو تن قسمت کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جوز بشکست و تهی آمد میانش</p></div>
<div class="m2"><p>برگسست آنجایگه آهی ز جانش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گشت بیمغزی خویشش آشکار</p></div>
<div class="m2"><p>اشک میبارید و میشد بیقرار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هاتفی گفتش که ای شوریده جان</p></div>
<div class="m2"><p>گر تو قَسّامی هلا قسمت کن آن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو نهٔ صاحب نظر خامی مکن</p></div>
<div class="m2"><p>بعد از این دعوی قَسّامی مکن</p></div></div>