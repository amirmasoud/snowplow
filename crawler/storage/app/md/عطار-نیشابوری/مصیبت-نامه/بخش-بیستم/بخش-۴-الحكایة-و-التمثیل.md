---
title: >-
    بخش ۴ - الحكایة و التمثیل
---
# بخش ۴ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>شبلی آن کز مغز معنی راز گفت</p></div>
<div class="m2"><p>این حکایت از برادر باز گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت بود اندر دبیرستان شهر</p></div>
<div class="m2"><p>میرزادی یوسف کنعان شهر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر دوعالم بر نکوئی نقد او</p></div>
<div class="m2"><p>در نکوئی هرچه گوئی نقد او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حسن او فهرست دیوان جمال</p></div>
<div class="m2"><p>وصف او بالای ایوان کمال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>او بمکتب پیش استاد آمده</p></div>
<div class="m2"><p>جمله شاگردان بفریاد آمده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود آنجا کودکی درویش حال</p></div>
<div class="m2"><p>کفشگر بودش در پدر بی ملک و مال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل ز عشق آن پسر مستش بماند</p></div>
<div class="m2"><p>شد ز دست او و بر دستش بماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک زمان نشکفت از دیدار او</p></div>
<div class="m2"><p>گرم تر شد هر نفس در کار او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در هوای آن چراغ روزگار</p></div>
<div class="m2"><p>میگداخت از عشق همچون شمع زار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کودکی ناخورده یک اندوه عشق</p></div>
<div class="m2"><p>چون کشد چون کاه گشته کوه عشق</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رفت یک روزی بمکتب میر داد</p></div>
<div class="m2"><p>کودکی را دید پیش میرزاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفت این کودک بگو تا آن کیست</p></div>
<div class="m2"><p>گفت آن کشفگر مقصود چیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفت آخر شرم دار ای اوستاد</p></div>
<div class="m2"><p>او بهم با میرزادی چون فتاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>میر زاده چون کند با او نشست</p></div>
<div class="m2"><p>طبع او گیرد دهد همت ز دست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کودک دلداده را مرد ادیب</p></div>
<div class="m2"><p>کرد از مکتب نشستن بی نصیب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دور کردش از دبیرستان خویش</p></div>
<div class="m2"><p>تا شد آن بیچاره سرگردان خویش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شد ز عشق آن پسر چون اخگری</p></div>
<div class="m2"><p>پس چو اخگر رفت در خاکستری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چشم همچون ابر نوروز آمدش</p></div>
<div class="m2"><p>آه همچون برق جانسوز آمدش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عاقبت از خویشتن دل برگرفت</p></div>
<div class="m2"><p>از برای مرگ منزل برگرفت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>میرزاد از حال او شد با خبر</p></div>
<div class="m2"><p>کس فرستادش که ای زیر و زبر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از چه مینالی بگو با من چنین</p></div>
<div class="m2"><p>گفت دل در کار تو کردم یقین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>این زمان دوران جان دادن رسید</p></div>
<div class="m2"><p>نوبت در خاک افتادن رسید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اشک چون گوگرد سرخ ای یار من</p></div>
<div class="m2"><p>کردهمچون زر مس رخسار من</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مدتی در انتظارم داشتی</p></div>
<div class="m2"><p>همچو آتش بی قرارم داشتی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>رفت پیش میرزاد آن مرد باز</p></div>
<div class="m2"><p>گفت میگوید که مردم در نیاز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زانکه در کار تو کردم دل ز عشق</p></div>
<div class="m2"><p>مرگ آمد بیتوام حاصل ز عشق</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>میرزادش داد پیغام دگر</p></div>
<div class="m2"><p>گفت اگر کردی تو دل زیر و زبر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در سر کارم بنزد من فرست</p></div>
<div class="m2"><p>دانهٔ دل را بدین خرمن فرست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بازآمد مرد چون گفت این سخن</p></div>
<div class="m2"><p>کودکش گفتا زمانی صبر کن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چون دلم خواهد ز من دلخواه من</p></div>
<div class="m2"><p>تا فرستادن نباشد راه من</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>رفت کودک خانه را در خون گرفت</p></div>
<div class="m2"><p>سینه را بشکافت دل بیرون گرفت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پس نهاد آن بر طبق پوشیده سر</p></div>
<div class="m2"><p>گفت گیر این پیش او پوشیده بر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چون دل خود بر طبق حالی نهاد</p></div>
<div class="m2"><p>بودش از جان یک رمق حالی بداد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>میرزاد القصه چون دید آن طبق</p></div>
<div class="m2"><p>او نخوانده بود هرگز آن سبق</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آن دل پرخون او بیرون گرفت</p></div>
<div class="m2"><p>جملهٔ‌مکتب ز چشمش خون گرفت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شد قیامت آشکارا در دلش</p></div>
<div class="m2"><p>رستخیزی نقد امد حاصلش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>عاقبت خود کشت و خود ماتم گرفت</p></div>
<div class="m2"><p>هم بنتوانست کردن هم گرفت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خاک او را قبله جای خویش کرد</p></div>
<div class="m2"><p>هر زمانی ماتم او بیش کرد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گرچه پنداری که پیر عالمی</p></div>
<div class="m2"><p>در ره عشق از چنین طفلی کمی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گر تو مرد راه عشقی دل شکاف</p></div>
<div class="m2"><p>ورنه تن زن جان مکن چندین ملاف</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تا که جان داری بلای جان تست</p></div>
<div class="m2"><p>جان بده در درد کاین درمان تست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>منت تریاک تا چندی کشی</p></div>
<div class="m2"><p>زانکه جان از زهر افتد در خوشی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تو همی محجوب از خود ماندهٔ</p></div>
<div class="m2"><p>تا ابد معیوب ازخود ماندهٔ</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چون توئی تو برافتد از میان</p></div>
<div class="m2"><p>تو بمانی بی حجاب جاودان</p></div></div>