---
title: >-
    بخش ۱۳ - الحكایة و التمثیل
---
# بخش ۱۳ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>میگریست آن بیدل دیوانه زار</p></div>
<div class="m2"><p>آن یکی گفتش چرائی اشکبار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت گیرم میشکیبم برهنه</p></div>
<div class="m2"><p>چون نگریم زانکه هستم گرسنه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت اگرچه میکند نانت هوس</p></div>
<div class="m2"><p>چون زگرسنگی بگرید چون تو کس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت آخر چون نگریم ده تنه</p></div>
<div class="m2"><p>کاو ازان دارد چنینم گرسنه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا بگریم همچو ابر نوبهار</p></div>
<div class="m2"><p>لاجرم میگریم اکنون زار زار</p></div></div>