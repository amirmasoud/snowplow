---
title: >-
    بخش ۸ - الحكایة و التمثیل
---
# بخش ۸ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>رفت با بهلول هارون الرشید</p></div>
<div class="m2"><p>سوی گورستان بر خاکی رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کلهٔ دیدند خشک آن کسی</p></div>
<div class="m2"><p>مرغ در وی خانه بنهاده بسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کرد هارونش ازان کله سؤال</p></div>
<div class="m2"><p>گفت بهلولش که پنهان نیست حال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوده است این مرد سر انداخته</p></div>
<div class="m2"><p>در کبوتر باختن جان باخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرد چون در دوستی این بمرد</p></div>
<div class="m2"><p>چون بشد با خویشتن هم این ببرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون نرفتست این هوس از سر برونش</p></div>
<div class="m2"><p>بیضهٔ مرغست در کله کنونش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم دماغش بر کبوتر بازیست</p></div>
<div class="m2"><p>خاک گشته همچنان در بازیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از هوس گر کله خاکستر شود</p></div>
<div class="m2"><p>می ندانم تا هنوز از سر شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرچه در دنیا خیالت آن بود</p></div>
<div class="m2"><p>تا ابد راه وصالت آن بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کار بر خود از امل کردی دراز</p></div>
<div class="m2"><p>بند کن پیش از اجل از خویش باز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ورنه در مردن نه آسان باشدت</p></div>
<div class="m2"><p>هر نفس مرگی دگر سان باشدت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جمله در باز و فرو کن پای راست</p></div>
<div class="m2"><p>گر کفن را هیچ نگذاری رواست</p></div></div>