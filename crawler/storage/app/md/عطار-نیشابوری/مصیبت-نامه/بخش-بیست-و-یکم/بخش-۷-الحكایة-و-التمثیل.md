---
title: >-
    بخش ۷ - الحكایة و التمثیل
---
# بخش ۷ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>بود بهلول از شراب عشق مست</p></div>
<div class="m2"><p>بر سر راهی مگر بر پل نشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میگذشت آنجایگه هارون مگر</p></div>
<div class="m2"><p>او خوشی میبود پیش افکنده سر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت هارونش که ای بهلول مست</p></div>
<div class="m2"><p>خیز از اینجا چون توان بر پل نشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت این با خویشتن گو ای امیر</p></div>
<div class="m2"><p>تا چرا بر پل بماندی جای گیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جملهٔ دنیا پلست و قنطرهست</p></div>
<div class="m2"><p>بر پُلت بنگر که چندین منظرهست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر بسی بر پل کنی ایوان و در</p></div>
<div class="m2"><p>هست آبی زان سوی پل سر بسر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گردنت را خانه بر پل چیست غل</p></div>
<div class="m2"><p>کی شود با مگر این بیرون بپل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا توانی زیر پل ساکن مباش</p></div>
<div class="m2"><p>چون شکست آورد پل ایمن مباش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از مجره آسمان دارد شکست</p></div>
<div class="m2"><p>زودبگذر تا نگردی پست پست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گنبدی بشکسته تو بنشسته زیر</p></div>
<div class="m2"><p>آمدستی گوئیا از جانت سیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گنبد بشکسته چون زیر اوفتد</p></div>
<div class="m2"><p>کی جهد کس گر خود او شیر اوفتد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرگ از پیش و تو از پس میروی</p></div>
<div class="m2"><p>بهر مرداری چو کرکس میروی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پاک شو از جیفهٔ دنیا تمام</p></div>
<div class="m2"><p>ورنه چون مردار میمانی بدام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زانکه هر چیزی که سودای تو است</p></div>
<div class="m2"><p>چون بمردی نقد فردای تو است</p></div></div>