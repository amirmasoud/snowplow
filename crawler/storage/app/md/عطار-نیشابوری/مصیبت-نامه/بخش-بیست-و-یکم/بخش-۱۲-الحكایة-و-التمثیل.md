---
title: >-
    بخش ۱۲ - الحكایة و التمثیل
---
# بخش ۱۲ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>نان پزی دیوانه و بیچاره شد</p></div>
<div class="m2"><p>وز میان نان پزان آواره شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شهر میگشتی چو پی گم کردهٔ</p></div>
<div class="m2"><p>گردهٔ میخواستی بی کردهٔ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سایلی پرسید ازو کای حیله جوی</p></div>
<div class="m2"><p>گردهٔ بی کرده چون باشد بگوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت تا من پختمی یک گرده نان</p></div>
<div class="m2"><p>گردهٔ نو در رسیدی همچنان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا بپختی گردهٔ ای بیخبر</p></div>
<div class="m2"><p>در بر ریشم نهادندی دگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون سری پیدا نبد این گرده را</p></div>
<div class="m2"><p>سر بگردید از جنون این مرده را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر دلم چیزی درآمد از اله</p></div>
<div class="m2"><p>گفت صد گرده مپز یک گرده خواه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روز تا شب گردهٔ نان میبست</p></div>
<div class="m2"><p>گردهٔ آخر رسد از صد کست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوش خوشی میرو میانراه تو</p></div>
<div class="m2"><p>گردهٔ بی کردهٔ میخواه تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چارهٔ صد گرده میبایست کرد</p></div>
<div class="m2"><p>تا مرا یک گرده میبایست خورد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این زمان هر روز شکر میخورم</p></div>
<div class="m2"><p>به زنان صد چیز دیگر میخورم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گرترا نان نرسد از حق زان بود</p></div>
<div class="m2"><p>تادلت پیوسته سرگردان بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زانکه گر سرگشتهٔ نان خواهدش</p></div>
<div class="m2"><p>ندهدش نان زانکه گریان خواهدش</p></div></div>