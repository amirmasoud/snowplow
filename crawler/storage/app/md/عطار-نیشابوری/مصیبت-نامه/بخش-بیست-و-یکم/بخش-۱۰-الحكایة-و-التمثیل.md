---
title: >-
    بخش ۱۰ - الحكایة و التمثیل
---
# بخش ۱۰ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>پیش حیدر آمد آن درویش حال</p></div>
<div class="m2"><p>کرد ازان دریای دانش سه سؤال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت از هفتاد فرسنگ آمدم</p></div>
<div class="m2"><p>هین جوابم ده که دلتنگ آمدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چیست درویشی و بیماری و مرگ</p></div>
<div class="m2"><p>داد حیدر سه جواب او ببرگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت درویشی تو جهل آمدست</p></div>
<div class="m2"><p>فقر تو گر عالمی سهل آمدست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هست بیماری حسد بردن همه</p></div>
<div class="m2"><p>هست بد خوئی تو مردن همه</p></div></div>