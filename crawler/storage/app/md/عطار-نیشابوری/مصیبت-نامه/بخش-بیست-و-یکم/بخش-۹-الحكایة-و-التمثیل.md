---
title: >-
    بخش ۹ - الحكایة و التمثیل
---
# بخش ۹ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>بود مردی در سخاوت بی بدل</p></div>
<div class="m2"><p>هرچه بودی خرج کردی بی خلل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مینداشت البته یک جو زر نگاه</p></div>
<div class="m2"><p>گفت یک روزیش مردی نیک خواه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کای فلان آخر نترسی از هلاک</p></div>
<div class="m2"><p>کان زمان کز تو برآید جان پاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون نمیداری نگه یک پیرهن</p></div>
<div class="m2"><p>پس فراهم بایدت کردن کفن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت چون جانم برآید در پسی</p></div>
<div class="m2"><p>وان کفن کدیه کنند از هر کسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ز دروازه درآیم نیز من</p></div>
<div class="m2"><p>پس شما بر سر زنیدم آن کفن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حرص مینگذاردت پاک ای پسر</p></div>
<div class="m2"><p>تا پلید آئی تو درخاک ای پسر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دایماً در خوی ناخوش ماندهٔ</p></div>
<div class="m2"><p>وز صفات بد در آتش ماندهٔ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا صفاتت باتو خواهد بود جمع</p></div>
<div class="m2"><p>تو نخواهی بود بی سوزی چو شمع</p></div></div>