---
title: >-
    بخش ۳ - الحكایة و التمثیل
---
# بخش ۳ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>خواجهٔ در نزع جمعی را بخواست</p></div>
<div class="m2"><p>گفت کار من کنید ای جمع راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر یکی را کار دیگر راست کرد</p></div>
<div class="m2"><p>حاجتی از هر کسی درخواست کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون ز عمر خود نمیدید او امان</p></div>
<div class="m2"><p>زود زود آن حرف میگفت آن زمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود بر بالین او شوریدهٔ</p></div>
<div class="m2"><p>گفت تو کوری نداری دیدهٔ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن ثریدی را که تو در کل حال</p></div>
<div class="m2"><p>در شکستی مدت هفتاد سال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون براری آنهمه در یک زمان</p></div>
<div class="m2"><p>هین فرو کن پای و جان ده زود جان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در چنین عمری دراز ای بی هنر</p></div>
<div class="m2"><p>تو کجا بودی کنونت شد خبر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جملهٔ عمرت چنین بودست کار</p></div>
<div class="m2"><p>وین زمان هم درحسابی و شمار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می بمیری خنده زن چون شمع میر</p></div>
<div class="m2"><p>زین بشولش تا کی آخر جمع میر</p></div></div>