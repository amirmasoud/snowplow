---
title: >-
    بخش ۷ - الحكایة و التمثیل
---
# بخش ۷ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>خواجهٔ را طوطی چالاک بود</p></div>
<div class="m2"><p>زهر با سر سبزیش تریاک بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مدت یکسال میدادش شکر</p></div>
<div class="m2"><p>تا بنطق آید شکر ریزد مگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روز و شب در کار او دل بسته بود</p></div>
<div class="m2"><p>ز اشتیاق نطق اودل خسته بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه میدادش شکر سالی تمام</p></div>
<div class="m2"><p>او نگفت از هیچ وجهی یک کلام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاقبت کاری قوی ناخوش فتاد</p></div>
<div class="m2"><p>در سرای آن خواجه را آتش فتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون بگرد آن قفس آتش رسید</p></div>
<div class="m2"><p>تفت آن در طوطی دلکش رسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت هین ای خواجه زنهار الامان</p></div>
<div class="m2"><p>ورنه در آتش بسوزم این زمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواجه گفتش چون چنین کاری فتاد</p></div>
<div class="m2"><p>آمدت از من چنین در وقت یاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درکشیدی دم شبان روزی تمام</p></div>
<div class="m2"><p>از کجا آوردی اکنون این کلام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون ز بیم جان خود درماندی</p></div>
<div class="m2"><p>از قصور عجز خویشم خواندی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از برای خویش پیشم خواندهٔ</p></div>
<div class="m2"><p>دفع آتش را بخویشم خواندهٔ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گرنکردی آتشت جان بی قرار</p></div>
<div class="m2"><p>با منت هرگز نبودی هیچ کار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یاد من پیوسته چون باد آمدت</p></div>
<div class="m2"><p>این چنین وقتی ز من یاد آمدت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون بکردی یاد من بیگانه وار</p></div>
<div class="m2"><p>تن کنون در سوز ده پروانه وار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هرکه در آتش چو ابراهیم نیست</p></div>
<div class="m2"><p>گر بسوزد همچو طوطی بیم نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تانیفتد کار در کار ای پسر</p></div>
<div class="m2"><p>کی ز کار افتادگی یابی خبر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هست خلت عین کار افتادگی</p></div>
<div class="m2"><p>گر خلیلی کم طلب آزادگی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>راه تو زیر و زبر افتادنست</p></div>
<div class="m2"><p>زانکه بهبودت بتر افتادنست</p></div></div>