---
title: >-
    بخش ۵ - الحكایة و التمثیل
---
# بخش ۵ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>گفت روزی پادشاه عصر خویش</p></div>
<div class="m2"><p>بر کنار بام شد بر قصر خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کودکی را دید زیبا و لطیف</p></div>
<div class="m2"><p>مشت میزد سخت پیری را ضعیف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زیر قصر آمد وزو پرسید حال</p></div>
<div class="m2"><p>کز چه او را میدهی این این گوشمال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت او را میبباید زد بسی</p></div>
<div class="m2"><p>تا نیارد کرد این دعوی کسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دعوی عشق منش میبوده است</p></div>
<div class="m2"><p>پس سه روز و شب فرو آسوده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه طلب کرده مرا نه جسته باز</p></div>
<div class="m2"><p>مانده در عشق این چنین آهسته باز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از همه عالم گزیدست او مرا</p></div>
<div class="m2"><p>شد سه روز اکنون که دیدست او مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کرده اودعوی من از دیرگاه</p></div>
<div class="m2"><p>زین بتر در عشق کی باشد گناه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاه گفتا زین بتر باید زدن</p></div>
<div class="m2"><p>هر دم از نوعی دگر باید زدن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صبر از معشوق عاشق چون کند</p></div>
<div class="m2"><p>کی تواند کرد تا اکنون کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرکه بی معشوق میگیرد قرار</p></div>
<div class="m2"><p>کی توان بر ضرب کردن اختصار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زان که هر کو نان این دیوان خورد</p></div>
<div class="m2"><p>بس قفا کو در قفای آن خورد</p></div></div>