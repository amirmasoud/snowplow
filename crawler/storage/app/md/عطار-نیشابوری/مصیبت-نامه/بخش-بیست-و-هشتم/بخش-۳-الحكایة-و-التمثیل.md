---
title: >-
    بخش ۳ - الحكایة و التمثیل
---
# بخش ۳ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>گشت محمود و ایاز دلنواز</p></div>
<div class="m2"><p>هردو در میدان غزنین گوی باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دو با هم گوی تنها باختند</p></div>
<div class="m2"><p>گوی همچون عشق زیبا باختند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گاه این یک اسب تاخت و گاه آن</p></div>
<div class="m2"><p>گاه این یک گوی باخت و گاه آن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز ارزوی آن غلام و پادشاه</p></div>
<div class="m2"><p>گشت چوگان آسمان و گوی ماه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرد میدان عالمی نظارگی</p></div>
<div class="m2"><p>فتنهٔ هر دو شده یکبارگی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون بماندند آن دو مرغ دلنواز</p></div>
<div class="m2"><p>در بر یکدیگر استادند باز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاه گفتش ای جهان روشن ز تو</p></div>
<div class="m2"><p>به تو میبازی ز من یا من ز تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت شه فتوی کند از رای خویش</p></div>
<div class="m2"><p>شه یکی نظارگی را خواند پیش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت گوی از ما که به بازد بگوی</p></div>
<div class="m2"><p>اسب در میدان که به تازد بگوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بود آن نظارگی صاحب نظر</p></div>
<div class="m2"><p>گفت چشمم کور باد ای دادگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر شما را من دو تن میدیدهام</p></div>
<div class="m2"><p>جز یکی نیست اینچه من میدیدهام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون نگه کردم بشاه حق شناس</p></div>
<div class="m2"><p>بود از سر تا قدم جمله ایاس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون ایاست را نگه کردم نهان</p></div>
<div class="m2"><p>بود هفت اعضای او شاه جهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر دوتن را در نظر آوردمی</p></div>
<div class="m2"><p>در میان هر دوحکمی کردمی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لیک چون هر دو یکی دیدم عیان</p></div>
<div class="m2"><p>حکم نتوان کرد هرگز درمیان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون سخن شایسته گفت آن مرد راه</p></div>
<div class="m2"><p>گوهر بازو درو انداخت شاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا بود معشوق را در خود نظر</p></div>
<div class="m2"><p>عاشق از وی کی تواند خورد بر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا نظر معشوق را بر عاشق است</p></div>
<div class="m2"><p>جان عاشق عشق او را لایق است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر دو را بر یکدگر باید نظر</p></div>
<div class="m2"><p>تاخورد آن برازین این زان دگر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر دو میباینده یک ذات آمده</p></div>
<div class="m2"><p>بی دو بودن در ملاقات آمده</p></div></div>