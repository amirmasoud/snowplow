---
title: >-
    بخش ۶ - الحكایة و التمثیل
---
# بخش ۶ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>صوفئی میرفت و جانی پرغمش</p></div>
<div class="m2"><p>پای بازی زد قفائی محکمش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون قفای سخت خورد آنجایگاه</p></div>
<div class="m2"><p>کرد آن صوفی مگر از پس نگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرد گفت از چه ز پس نگرندهٔ</p></div>
<div class="m2"><p>کاینت باید خورد تا تو زندهٔ</p></div></div>