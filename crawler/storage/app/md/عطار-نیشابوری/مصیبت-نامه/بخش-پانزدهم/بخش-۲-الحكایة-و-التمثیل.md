---
title: >-
    بخش ۲ - الحكایة و التمثیل
---
# بخش ۲ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>در رهی میرفت عیسی غرق نور</p></div>
<div class="m2"><p>همرهیش افتاد نیک از راه دور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود عیسی را سه گرده نان مگر</p></div>
<div class="m2"><p>خورد یک گرده بدو داد آن دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس ازان سه گرده یک گرده بماند</p></div>
<div class="m2"><p>در میان هر دو ناخورده بماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد ز بهر آب عیسی سوی راه</p></div>
<div class="m2"><p>همرهش گرده بخورد آنجایگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عسی مریم چو آمد سوی او</p></div>
<div class="m2"><p>میندید آن گرده در پهلوی او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت آن گرده کجا شد ای پسر</p></div>
<div class="m2"><p>گفت من هرگز ندارم زان خبر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میشدند آن هر دو تن زانجانگاه</p></div>
<div class="m2"><p>تا یکی دریا پدید آمد براه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دست او بگرفت عیسی آن زمان</p></div>
<div class="m2"><p>گشت با اوبر سر دریا روان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون بران دریاش داد آخر گذر</p></div>
<div class="m2"><p>گفت ای همره بحق دادگر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پادشاهی کاین چنین برهان نمود</p></div>
<div class="m2"><p>کاین چنین برهان بخود نتوان نمود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کاین زمان با من بگو ای مرد راه</p></div>
<div class="m2"><p>تا که خورد آن گرده را آنجایگاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرد گفتا نیست آگاهی مرا</p></div>
<div class="m2"><p>چون نمیدانم چه میخواهی مرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همچنان میرفت عیسی زو نفور</p></div>
<div class="m2"><p>تا پدید آمد یکی آهو ز دور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خواند عیسی آهوی چالاک را</p></div>
<div class="m2"><p>سرخ کرد از خون آهو خاک را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کرد بریانش اندکی هم خورد نیز</p></div>
<div class="m2"><p>تا بگردن سیر شد آن مرد نیز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بعد ازان عیسی مریم استخوانش</p></div>
<div class="m2"><p>جمع کرد و در دمید اندر میانش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آهو آن دم زندگی از سرگرفت</p></div>
<div class="m2"><p>کرد خدمت راه صحرا برگرفت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هم دران ساعت مسیح رهنمای</p></div>
<div class="m2"><p>گفت ای همره بحق آن خدای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کاین چنین حجت نمودت آن زمان</p></div>
<div class="m2"><p>کاگهم کن تو ازان یک گرده نان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گفت سودا دارد ای همره ترا</p></div>
<div class="m2"><p>چون ندانم چون کنم آگه ترا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همچنان آن مرد را با خویش برد</p></div>
<div class="m2"><p>تا پدید آمد سه کوه خاک خرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کرد آن ساعت دعا عیسی پاک</p></div>
<div class="m2"><p>تا زر صامت شد آن سه پاره خاک</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گفت یک پاره ترا ای مرد راست</p></div>
<div class="m2"><p>وان دگر پاره که میبینی مراست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وان سیم پاره مرآنراست آن زمان</p></div>
<div class="m2"><p>کو نهان خوردست آن یک گرده نان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مرد را چون نام زر آمد پدید</p></div>
<div class="m2"><p>ای عجب حالی دگر آمد پدید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گفت پس آن گرده نان من خوردهام</p></div>
<div class="m2"><p>گرسنه بودم نهان من خوردهام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون ازو عیسی سخن بشنود راست</p></div>
<div class="m2"><p>گفت من بیزارم این هر سه تراست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو نمیشائی بهمراهی مرا</p></div>
<div class="m2"><p>خود نخواهم من اگر خواهی مرا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>این بگفت و زین سبب رنجور شد</p></div>
<div class="m2"><p>مرد را بگذاشت وز وی دور شد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>یک زمان بگذشت دو تن آمدند</p></div>
<div class="m2"><p>هر دو زر دیدند دشمن آمدند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آن نخستین گفت جمله زرمراست</p></div>
<div class="m2"><p>وین دو تن گفتند این زر آن ماست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گفت و گوی و جنگشان بسیار شد</p></div>
<div class="m2"><p>هم زفان هم دستشان از کار شد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>عاقبت راضی شدند آن هر سه خام</p></div>
<div class="m2"><p>تا بسه حصه کنند آن زر تمام</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گرسنه بودند آنجا هر سه کس</p></div>
<div class="m2"><p>بر نیامدشان ز گرسنگی نفس</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آن یکی گفتا که جان به از زرم</p></div>
<div class="m2"><p>رفتم اینک سوی شهر ونان خرم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هر دو تن گفتند اگر نان آوری</p></div>
<div class="m2"><p>در تن رنجور ما جان آوری</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تو بنان رو چون رسی از ره فراز</p></div>
<div class="m2"><p>زر کنیم آن وقت از سه حصه باز</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مرد حالی زر بیار خود سپرد</p></div>
<div class="m2"><p>ره گرفت و دل بکار خود سپرد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شد بشهر و نان خرید و خورد نیز</p></div>
<div class="m2"><p>پس بحیلت زهر در نان کرد نیز</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تا بمیرند آن دو تن از نان او</p></div>
<div class="m2"><p>واو بماند و آن همه زر زان او</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>وین دو تن کردند عهد اینجایگاه</p></div>
<div class="m2"><p>کاین دو برگیرند آن یک را ز راه</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>پس کنند آن هر سه حصه از دوباز</p></div>
<div class="m2"><p>چون قرار افتاد مرد آمد فراز</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هر دو تن کشتند او را در زمان</p></div>
<div class="m2"><p>بعد ازان مردند چون خوردند نان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>عیسی مریم چو باز آنجا رسید</p></div>
<div class="m2"><p>کشته و آن مرده را آنجا بدید</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گفت اگر این زر بماند بر قرار</p></div>
<div class="m2"><p>خلق ازین زر کشته گردد بیشمار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>پس دعا کرد آن زمان از جان پاک</p></div>
<div class="m2"><p>تا شد آن زر همچو اول باز خاک</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گفت ای زرگر تو یابی روزگار</p></div>
<div class="m2"><p>کشته گردانی بروزی صد هزار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چه اگر از خاک زر نیکوترست</p></div>
<div class="m2"><p>آن نکوتر زر که خاکش برسرست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>زر اگر چه سرخ رو و دلکشست</p></div>
<div class="m2"><p>لیک تادر دست داری آتشست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چون ندارد نرگس تو چشم راه</p></div>
<div class="m2"><p>سیم و زر میدارد ازکوری نگاه</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>زر که چندین خلق در سودای اوست</p></div>
<div class="m2"><p>فرج استر یاسم خر جای اوست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چون چنین زر میبیندازد ز راه</p></div>
<div class="m2"><p>این دو جا اولیتر او را جایگاه</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گر ترا صد گنج زر متواریست</p></div>
<div class="m2"><p>از همه مقصود برخورداریست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>گه ببر گاهی بخور گاهی بدار</p></div>
<div class="m2"><p>اینت برخورداریت از روزگار</p></div></div>