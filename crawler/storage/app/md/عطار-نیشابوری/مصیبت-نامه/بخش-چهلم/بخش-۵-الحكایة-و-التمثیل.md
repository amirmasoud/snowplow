---
title: >-
    بخش ۵ - الحكایة و التمثیل
---
# بخش ۵ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>برفتاد از جان خرقانی نقاب</p></div>
<div class="m2"><p>دید آن شب حق تعالی را بخواب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت الهی روز و شب در کل حال</p></div>
<div class="m2"><p>جستمت پیدا و پنهان شصت سال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر امیدت ره بسی پیمودهام</p></div>
<div class="m2"><p>طالب تو بودهام تا بودهام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از وجود من رهائی ده مرا</p></div>
<div class="m2"><p>نور صبح‌ آشنائی ده مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حق تعالی گفت ای خرقانیم</p></div>
<div class="m2"><p>گر بسالی شصت تو میدانیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا بسالی شصت چه روز و چه شب</p></div>
<div class="m2"><p>کردهٔ بر جهد خود ما را طلب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من در آزال الازال بی علتیت</p></div>
<div class="m2"><p>کردهام تقدیر صاحب دولتیت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم در آزال الازل هم در قدم</p></div>
<div class="m2"><p>در طلب بودم ترا تو در عدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بودهام خواهان تو بیش از تو من</p></div>
<div class="m2"><p>در طلب بودم ترا پیش از تو من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این طلب کامروز از جان توخاست</p></div>
<div class="m2"><p>نیست هیچ آن تو جمله آن ماست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر طلب ازما نبودی از نخست</p></div>
<div class="m2"><p>کی ز تو هرگز طلب گشتی درست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون کشنده هم نهنده یافتی</p></div>
<div class="m2"><p>خویش را بیخویش زنده یافتی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لاجرم جاوید شمع دین شدی</p></div>
<div class="m2"><p>در امانت مرد عالم بین شدی</p></div></div>