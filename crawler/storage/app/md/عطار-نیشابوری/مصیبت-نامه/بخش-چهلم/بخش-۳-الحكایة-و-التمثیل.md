---
title: >-
    بخش ۳ - الحكایة و التمثیل
---
# بخش ۳ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>با پسر میگفت یک روزی عمر</p></div>
<div class="m2"><p>طعم دین تو کی شناسی ای پسر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طعم دین من دانم و من دیده‌ام</p></div>
<div class="m2"><p>زانکه طعم کفر هم بچشیده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان چو در خود دید چندان کار و بار</p></div>
<div class="m2"><p>در خروش آمد چو ابر نوبهار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت اگر من نیک اگر بد بوده‌ام</p></div>
<div class="m2"><p>در حقیقت طالب خود بوده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از طلب یک دم فرو ننشسته‌ام</p></div>
<div class="m2"><p>روز تا شب خویش را میجسته‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکجا رفتم به بالا و نشیب</p></div>
<div class="m2"><p>جمله را ازجان من نورست وزیب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در حقیقت چون همه من بوده‌ام</p></div>
<div class="m2"><p>نور بخش هفت گلشن بوده‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پس چرا بیرون سفر میکرده‌ام</p></div>
<div class="m2"><p>سوی این و آن نظر میکرده‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای دریغا ره سپردم عالمی</p></div>
<div class="m2"><p>لیک قدر خود ندانستم دمی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر همه در جان خود میگشتمی</p></div>
<div class="m2"><p>من به هر یک ذره صد میگشتمی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سالک سرگشته آمد پیش پیر</p></div>
<div class="m2"><p>شرح روحش داد از لوح ضمیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفت هر چیزی که پیدا و نهانست</p></div>
<div class="m2"><p>جملهٔ آثار جان افروز جانست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در جهان آثار جان بینم همه</p></div>
<div class="m2"><p>پرتو جان و جهان بینم همه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پرتوی از قدس ظاهرشد بزور</p></div>
<div class="m2"><p>در جهان افکند و درجان نیز شور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پرتوی بس بی نهایت اوفتاد</p></div>
<div class="m2"><p>تاابد بیحد و غایت اوفتاد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هرچه بود و هست خواهد بود نیز</p></div>
<div class="m2"><p>جمله زان پرتو گرفتست اسم چیز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نام آن پرتو بحق جان اوفتاد</p></div>
<div class="m2"><p>هر دو عالم را مدد آن اوفتاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>قدس ظاهر شد بیک چیزی قوی</p></div>
<div class="m2"><p>وی عجب آنبود جان معنوی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>لیک چون جان رانبود آن روزگار</p></div>
<div class="m2"><p>در هزاران صورت آمد آشکار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بود جان را هم صفت هم ذات نیز</p></div>
<div class="m2"><p>هر دو چون جان هم گرامی و عزیز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اصل جان نور مجرد بود و بس</p></div>
<div class="m2"><p>یعنی آن نور محمد بود و بس</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ذات چون در تافت شد عرش مجید</p></div>
<div class="m2"><p>عرش چون در تافت شد کرسی پدید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بازچون کرسی بتافت از سرکار</p></div>
<div class="m2"><p>آسمان گشت و کواکب آشکار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>باز چون اختر بتافت و آسمان</p></div>
<div class="m2"><p>چار ارکان نقد شد در یک زمان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بعد ازان چون قوت تاوش نماند</p></div>
<div class="m2"><p>چار ارکان را در آمیزش نشاند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا وحوش و طیر وحیوان ونبات</p></div>
<div class="m2"><p>با مرکبهای دیگر یافت ذات</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ذات جان را هم صفاتی بود نیز</p></div>
<div class="m2"><p>لاجرم از علم و قدرت شد عزیز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شد ز علمش لوح محفوظ آشکار</p></div>
<div class="m2"><p>شد قلم از قدرتش مشغول کار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون ارادت را بسی سر جمله بود</p></div>
<div class="m2"><p>هم ملایک بی عدد هم حمله بود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از رضای جان بهشت عدن خاست</p></div>
<div class="m2"><p>وز غضب کوداشت دوزخ گشت راست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>روح چون در اصل امر محض بود</p></div>
<div class="m2"><p>جبرئیل از امر ظاهر گشت زود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>باز روح از لطف وز بخشش که داشت</p></div>
<div class="m2"><p>زود میکائیل را سر برفراشت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>باز قهرش اصل عزرائیل گشت</p></div>
<div class="m2"><p>دو صفت ماندش که اسرافیل گشت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>یک صفت ایجاد و اعدام آن دگر</p></div>
<div class="m2"><p>وز وجود و از عدم جان بر زبر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر صفات روح بی اندازه خاست</p></div>
<div class="m2"><p>هر یکی را یک ملک گیری رواست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>پیر چون از شرح او آگاه شد</p></div>
<div class="m2"><p>گفت اکنون جانت مرد راه شد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>لاجرم یک ذره پندارت نماند</p></div>
<div class="m2"><p>جز فنای در فناکارت نماند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا که میدیدی تو خود را در میان</p></div>
<div class="m2"><p>برکناری بودی از سر عیان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چون طلب از دوست دیدی سوی دوست</p></div>
<div class="m2"><p>این نظر را گر نگه داری نکوست</p></div></div>