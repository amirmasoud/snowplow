---
title: >-
    بخش ۲۲ - الحكایة و التمثیل
---
# بخش ۲۲ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>در مناجات آن بزرگ دین شبی</p></div>
<div class="m2"><p>پیش حق میکرد آه و یاربی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت الهی چون شود حشر آشکار</p></div>
<div class="m2"><p>بر لب دوزخ خوشی گیرم قرار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس بدست آرم یکی خنجر ز نور</p></div>
<div class="m2"><p>خلق را میرانم از دوزخ ز دور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا ز دوزخ سر بسر ایمن شوند</p></div>
<div class="m2"><p>در بهشت جاودان ساکن شوند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هاتفی آواز دادش آن زمان</p></div>
<div class="m2"><p>گفت تو خاموش بنشین هان و هان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ورنه عیب تو بگویم آشکار</p></div>
<div class="m2"><p>تا کنندت خلق عالم سنگسار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بعدازان داد آن بزرگ دین جواب</p></div>
<div class="m2"><p>گفت هان و هان چه گفتم ناصواب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو بدان میآریم تااین زمان</p></div>
<div class="m2"><p>برگشایم بر سر خلقان زفان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از تو چندان بازگویم فضل وجود</p></div>
<div class="m2"><p>کز همه عالم کست نکند سجود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پادشاها با دمی سرد آمدم</p></div>
<div class="m2"><p>با دلی پرغصه و درد آمدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون نیم من هیچ و آگاهی ز من</p></div>
<div class="m2"><p>ای همه تو پس چه میخواهی ز من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گرعذاب تو ز صد رویم بود</p></div>
<div class="m2"><p>در خور یک تارهٔ مویم بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لیک یک فضلت چو صد عالم فتاد</p></div>
<div class="m2"><p>جرم جمله کم ز یک شبنم فتاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آمد از من آنچه آید از لئیم</p></div>
<div class="m2"><p>تو بکن نیز آنچه آید از کریم</p></div></div>