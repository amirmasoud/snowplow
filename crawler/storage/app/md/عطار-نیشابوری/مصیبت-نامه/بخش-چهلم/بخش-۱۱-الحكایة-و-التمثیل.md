---
title: >-
    بخش ۱۱ - الحكایة و التمثیل
---
# بخش ۱۱ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>خطبهٔ در نعت و توحید خدای</p></div>
<div class="m2"><p>کرده بود انشا بزرگی رهنمای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سجع بود آن خطبه رنجی برده بود</p></div>
<div class="m2"><p>پیش شیخ کرکان آورده بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون بخواند آن خطبه را در پیش او</p></div>
<div class="m2"><p>خواست تحسین طبع دوراندیش او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شیخ گفتا بر دلم صد غم نهاد</p></div>
<div class="m2"><p>آن دل بیکارکاین برهم نهاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که دل زندهست در سودای دین</p></div>
<div class="m2"><p>نبودش بی هیچ شک پروای این</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک نشان مرد بیکار این بود</p></div>
<div class="m2"><p>شغل مشغولان پندار این بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرد را آن خطبه بر دل سرد شد</p></div>
<div class="m2"><p>خجلتش آورد و رویش زرد شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حال من با این کتاب اینست و بس</p></div>
<div class="m2"><p>حجت بیکاری دینست و بس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چند گوی آخر ای دل تن بزن</p></div>
<div class="m2"><p>نفس را خاموش کن گردن بزن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چند شعر چون شکر گوئی تو خوش</p></div>
<div class="m2"><p>همچو بادامی زفان در کام کش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پنبه را یکبارگی برکش ز گوش</p></div>
<div class="m2"><p>در دهن نه محکم و بنشین خموش</p></div></div>