---
title: >-
    بخش ۱۵ - الحكایة و التمثیل
---
# بخش ۱۵ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>از ارسطالیس پرسیدند راز</p></div>
<div class="m2"><p>کان چه میدانی که در عمر دراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی گنه در خورد زندان آمدست</p></div>
<div class="m2"><p>گفت آنچش حبس دندان آمدست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنچه او محبوس میباید مدام</p></div>
<div class="m2"><p>آن زفان تست در زندان کام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو در از دندان و دو در از لبش</p></div>
<div class="m2"><p>بسته میدارند هر روز و شبش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا مگر یک لحظهٔ گیرد قرار</p></div>
<div class="m2"><p>وانگهش جز بیقراری نیست کار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکه خاموشست ثابت آمدست</p></div>
<div class="m2"><p>عزت زر بین که صامت آمدست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با که گویم درد دل چون کس نماند</p></div>
<div class="m2"><p>تن زنم کز عمر من هم بس نماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون خموشی این همه مقدار داشت</p></div>
<div class="m2"><p>لیک دو داعیم بر گفتارداشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جان من چون بودمست و بیقرار</p></div>
<div class="m2"><p>بر نمیزد یک نفس از درد کار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر دمی تن میزدم از جان پاک</p></div>
<div class="m2"><p>می برآمد از خموشی صد هلاک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از ازل چون عشق با جان خوی کرد</p></div>
<div class="m2"><p>شور عشقم این چنین پرگوی کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از شراب عشق چون لایعقلم</p></div>
<div class="m2"><p>کی تواند شد خموشی حاصلم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کاشکی جان مرا بودی قرار</p></div>
<div class="m2"><p>تا همیشه تن زدن بودیم کار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آنچه در جان من آگاه هست</p></div>
<div class="m2"><p>می ندانم تا بدانجا راه هست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون نمیبینم بعالم مرد خویش</p></div>
<div class="m2"><p>می فرو گویم بدانجا دردخویش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>داعی دیگر مرا آن بود و بس</p></div>
<div class="m2"><p>کاین حدیثم شد بحجت هر نفس</p></div></div>