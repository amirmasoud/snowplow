---
title: >-
    بخش ۶ - الحكایة و التمثیل
---
# بخش ۶ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>حق تعالی عرش را چون بر فراخت</p></div>
<div class="m2"><p>صد جهان پر فرشته سر فراخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حق بدیشان گفت بردارید عرش</p></div>
<div class="m2"><p>زانکه این را بر نتابد اهل فرش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد هزاران باره بیش اند از شمار</p></div>
<div class="m2"><p>در روید از قوت و شوکت بکار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جمله در رفتند چست و سرفراز</p></div>
<div class="m2"><p>عاقبت گشتند عاجز جمله باز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون مضاعف کرد اعداد همه</p></div>
<div class="m2"><p>عین عجز افتاد میعاد همه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عرش را چندان ملک می بر نتافت</p></div>
<div class="m2"><p>گفتئی موری فلک می بر نتافت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هشت قدسی را ز حق فرمان رسید</p></div>
<div class="m2"><p>در ربودند ای عجب عرش مجید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عرش را بر دوش خود برداشتند</p></div>
<div class="m2"><p>سرازان تعظیم می افراشتند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کای عجب عرشی که چندانی ملک</p></div>
<div class="m2"><p>پر بیفکندند از وی یک بیک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما بتنهائی خود برداشتیم</p></div>
<div class="m2"><p>خردهٔ الحق فرو نگذاشتیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اندکی عجبی پدید آمد مگر</p></div>
<div class="m2"><p>تا رسید امر از خدای دادگر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کای ملایک بنگردید از جای خویش</p></div>
<div class="m2"><p>تا چه میبینید زیر پای خویش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن ملایک چون نگه کردند زیر</p></div>
<div class="m2"><p>آمدند از جان خود از خوف سیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زیر پای خود هوا دیدند و بس</p></div>
<div class="m2"><p>در هوا چون پای دارد هیچکس</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حق بدیشان کرد آن ساعت خطاب</p></div>
<div class="m2"><p>کای ز عجب خود خطا کرده صواب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عرش اعظم گر شما برداشتید</p></div>
<div class="m2"><p>حامل آن خویش را پنداشتید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کیست بردارندهٔ بار شما</p></div>
<div class="m2"><p>بنگرید ای پر خلل کار شما</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون ملایک را فتاد آنجا نظر</p></div>
<div class="m2"><p>آن همه پندار بیرون شد زسر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هرکه پندارد که جان بیقرار</p></div>
<div class="m2"><p>بر تواند داشت سر کردگار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یا چنان انوار را حامل شود</p></div>
<div class="m2"><p>یا چنان اسرار را قابل شود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آن ازو عجبی و پنداری بود</p></div>
<div class="m2"><p>وین چنین در راه بسیاری بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آن امانت سر او هم میکشد</p></div>
<div class="m2"><p>قشر عالم مغز عالم میکشد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر نبودی در میان آن سر پاک</p></div>
<div class="m2"><p>کی کشیدی آن امانت آب و خاک</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>روستم را را رخش رستم میکشد</p></div>
<div class="m2"><p>تا نه پنداری که مردم میکشد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر حملناهم نیفتادی ز پیش</p></div>
<div class="m2"><p>حامل آن سر نبودی کس بخویش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون رسیدی وانچه دیدی دیده شد</p></div>
<div class="m2"><p>مرد را اینجا زفان ببریده شد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا ابد اکنون سفر در خویش کن</p></div>
<div class="m2"><p>هر زمانی رونق خود بیش کن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>لیک اگر از خویشتن خواهی خلاص</p></div>
<div class="m2"><p>تا شوی در پردهٔ توحید خاص</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از وجود جان برون باید شدن</p></div>
<div class="m2"><p>محرم جانان کنون باید شدن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>حوصله باید اگر آن بایدت</p></div>
<div class="m2"><p>کی بود جانانت گر جان بایدت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عقل و جانت را دو کفه ساز خوش</p></div>
<div class="m2"><p>عقل و جانت را در آنجا نه بکش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>عقل اگر افزون بود نقصان تراست</p></div>
<div class="m2"><p>جان اگر راجح شود جانان تراست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در فقیری چون زفانه باش راست</p></div>
<div class="m2"><p>سوی عقل و سوی جان منگر بخواست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تو زفانه گر نباشی بی شکی</p></div>
<div class="m2"><p>با ازل بینی ابد گشته یکی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کفر و دین و عقل و جان و خاک و آب</p></div>
<div class="m2"><p>جمله یک رنگت شود چون آفتاب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چون همه یک رنگت آمد در احد</p></div>
<div class="m2"><p>از همه درویش مانی تا ابد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ور بود در فقر جان یک ذره چیز</p></div>
<div class="m2"><p>حال کادالفقر باشد کفر نیز</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فقر چه بود سایه جاوید آمده</p></div>
<div class="m2"><p>در میان قرص خورشید آمده</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>پس بقرصی گشته قانع تاابد</p></div>
<div class="m2"><p>قرص و قانع محو احد مانده احد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>جز احد آنجا اگر چیزی بود</p></div>
<div class="m2"><p>هم احد باشد چو تمییزی بود</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>زانکه اینجا این همه هم اوست و بس</p></div>
<div class="m2"><p>بدمبین کاین جمله بس نیکوست و بس</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آن و این و این و آن اینجا بود</p></div>
<div class="m2"><p>لیکن آنجا اینهمه سودا بود</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گر مثالی بایدت کاسان شود</p></div>
<div class="m2"><p>همچو دریا دان که او باران شود</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هرچه از قرب احد آید پدید</p></div>
<div class="m2"><p>چون شود نازل عدد آید پدید</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هست قرآن در حقیقت یک کلام</p></div>
<div class="m2"><p>بی عدد آمد چو منزل شد تمام</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>صد هزاران قطره یک عمان بود</p></div>
<div class="m2"><p>چون زعمان بگذرد باران بود</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>هرچه اسمی یافت آمد در وجود</p></div>
<div class="m2"><p>آن همه یک شبنمست از بحر جود</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>حق عرفانت آن زمان حاصل شود</p></div>
<div class="m2"><p>کاینچه عقلش خواندهٔ باطل شود</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>عقل باید تا عبودیت کشد</p></div>
<div class="m2"><p>جانت باید تا ربوبیت کشد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>عقل با جان کی تواند ساختن</p></div>
<div class="m2"><p>با براقی لاشه نتوان تاختن</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>دردت اول از تفکر میرسد</p></div>
<div class="m2"><p>آخر الامرت تحیر میرسد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>علم باید گر چه مرد اهل آمدست</p></div>
<div class="m2"><p>تابداند کاخرش جهل آمدست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>هرکه او یک ذره از عز پی برد</p></div>
<div class="m2"><p>هیچ گردد هیچ هرگز کی برد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>عاریت باشد همه کردار او</p></div>
<div class="m2"><p>آن او نبود همه گفتار او</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گر بیان نیکو بود در شرع و راه</p></div>
<div class="m2"><p>آن بیان در حق بود برف سیاه</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>در بیان شرع صاحب حال شو</p></div>
<div class="m2"><p>لیک در حق کور گرد و لال شو</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چون شنیدی سر کار اکنون تمام</p></div>
<div class="m2"><p>نیز حاجت نیست دیگر والسلام</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>سالک از آیات آفاق ای عجب</p></div>
<div class="m2"><p>رفت با آیات انفس روز و شب</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گرچه بسیاری ز پس وز پیش دید</p></div>
<div class="m2"><p>هر دو عالم در درون خویش دید</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>هر دو عالم عکس جان خویش یافت</p></div>
<div class="m2"><p>وز دو عالم جان خود را بیش یافت</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چون بسر جان خود بیننده شد</p></div>
<div class="m2"><p>زندهٔگشت و خدا را بنده شد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بعد ازین اکنون اساس بندگیست</p></div>
<div class="m2"><p>هر نفس صد زندگی در زندگیست</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>سالک سرگشته را زیر و زبر</p></div>
<div class="m2"><p>تا بحق بودست چندینی سفر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بعد ازین در حق سفر پیش آیدش</p></div>
<div class="m2"><p>هرچه گویم بیش از پیش آیدش</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چون سفر آنست کار آنست و بس</p></div>
<div class="m2"><p>گیر و دارو کار و بار آنست و بس</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>زان سفر گر با تو انیجا دم زنم</p></div>
<div class="m2"><p>هر دو عالم بیشکی بر هم زنم</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>گر بدست آید مرا عمری دگر</p></div>
<div class="m2"><p>باز گویم با تو شرح آن سفر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>آن سفر را گر کتابی نو کنم</p></div>
<div class="m2"><p>تاابد دو کون پر پرتو کنم</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>گر بود از پیشگه دستورئی</p></div>
<div class="m2"><p>نیست جانم را ز شرحش دورئی</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>لیک شرح آن بخود دادن خطاست</p></div>
<div class="m2"><p>گر بود اذنی از آن حضرت رواست</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>شرح دادم این سفر باری تمام</p></div>
<div class="m2"><p>تادگر فرمان چه آید والسلام</p></div></div>