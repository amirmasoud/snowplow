---
title: >-
    بخش ۸ - الحكایة و التمثیل
---
# بخش ۸ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>بوسعید مهنه شیخ محترم</p></div>
<div class="m2"><p>بود در حمام با پیری بهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخت حمامی خوش ودمساز بود</p></div>
<div class="m2"><p>زانکه آب و آتشش هم ساز بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیر گفت ای شیخ حمامی خوشست</p></div>
<div class="m2"><p>وز خوشی هم دلگشا هم دلکشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شیخ گفتش هیچدانی خوش چراست</p></div>
<div class="m2"><p>گفت میدانم بگویم با تو راست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون درین حمام شیخی چون تو هست</p></div>
<div class="m2"><p>خوش شد و خوش گشت و خوش آمد نشست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شیخ گفتش زین بهت خواهم بیان</p></div>
<div class="m2"><p>پای من چون آوریدی در میان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیر گفتش تو بگو شیخا جواب</p></div>
<div class="m2"><p>کانچه تو گوئی جز آن نبود صواب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت حمامیست خوش از حد برون</p></div>
<div class="m2"><p>کز متاع جملهٔ دنیای دون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست جز سطل و ازاری با تو چیز</p></div>
<div class="m2"><p>وانگهی آن هر دو نیست آن تو نیز</p></div></div>