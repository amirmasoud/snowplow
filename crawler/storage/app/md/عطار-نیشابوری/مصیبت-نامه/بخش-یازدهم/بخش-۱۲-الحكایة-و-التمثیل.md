---
title: >-
    بخش ۱۲ - الحكایة و التمثیل
---
# بخش ۱۲ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>عیسی مریم بغاری رفته بود</p></div>
<div class="m2"><p>در میان غار مردی خفته بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت برخیز ای ز عالم بی خبر</p></div>
<div class="m2"><p>کار کن تا توشهٔ یابی مگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت من کار دو عالم کردهام</p></div>
<div class="m2"><p>تا ابد ملکی مسلم کردهام </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت هین کار تو چیست ای مرد راه</p></div>
<div class="m2"><p>گفت دنیا شد مرا یکبرگ کاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جملهٔ دنیا بنانی میدهم</p></div>
<div class="m2"><p>نان بسگ چون استخوانی میدهم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مدتی شد تا ز دنیا فارغم</p></div>
<div class="m2"><p>نیستم من طفل بازی بالغم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بالغم با لعب و با لهوم چکار</p></div>
<div class="m2"><p>فارغم با غفلت و سهوم چکار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عیسی مریم چو بشنود این سخن</p></div>
<div class="m2"><p>گفت اکنون هرچه میخواهی بکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون ز دنیا فارغی آزاد خفت</p></div>
<div class="m2"><p>خواب خوش بادت بخفت و شاد خفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون ز دنیا نیستت غمخوارگی</p></div>
<div class="m2"><p>کرده داری کارها یکبارگی</p></div></div>