---
title: >-
    بخش ۵ - الحكایة و التمثیل
---
# بخش ۵ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>میدوید آن عامی زیر و زبر</p></div>
<div class="m2"><p>تا نماز مرده در یابد مگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن یکی دیوانه چون او را بدید</p></div>
<div class="m2"><p>کو در آن تعجیل بیخود میدوید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت چیزی سرد میگردد براه</p></div>
<div class="m2"><p>هین بدو تا در رسی آنجایگاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هستی از مردار دنیا ناصبور</p></div>
<div class="m2"><p>میروی چون مرده میبینی ز دور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میخوری مردار دنیا ماه و سال</p></div>
<div class="m2"><p>وین خود از جوعست برمردان حلال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا که یک عاقل برآرد یک دمی</p></div>
<div class="m2"><p>جاهلان خوردند در هم عالمی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا بحکمت لقمهٔ لقمان خورد</p></div>
<div class="m2"><p>در خیانت خائنی صد جان خورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اهل دنیا چون سگ دیوانهاند</p></div>
<div class="m2"><p>در گزندت زانکه بس بیگانهاند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میخورند از جهل مرداری بناز</p></div>
<div class="m2"><p>میکنند آنگه کفن از مرده باز</p></div></div>