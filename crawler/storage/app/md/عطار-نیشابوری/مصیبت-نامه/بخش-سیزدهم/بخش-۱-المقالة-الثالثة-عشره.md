---
title: >-
    بخش ۱ - المقالة الثالثة عشره
---
# بخش ۱ - المقالة الثالثة عشره

<div class="b" id="bn1"><div class="m1"><p>سالک سرگشته چون مستی خراب</p></div>
<div class="m2"><p>شد دلی پرتاب پیش آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت ای سلطانسرگیتی نورد</p></div>
<div class="m2"><p>در جهان بسیار دیده گرم و سرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای بفیض و روشنی برده سبق</p></div>
<div class="m2"><p>بوده برچارم سما زرین طبق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرم کردی ذات ذریات را</p></div>
<div class="m2"><p>عاشقی آموختی ذرات را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرنهٔ سلطان علم چون میزنی</p></div>
<div class="m2"><p>کوس زرین صبحدم چون میزنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هست انگشتیت در هر روزنی</p></div>
<div class="m2"><p>ذره ذره دیدهٔ چون روشنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو بحق چشم و چراغ عالمی</p></div>
<div class="m2"><p>این جهان را وان جهان را محرمی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گاه سنگ از فیض گوهر میکنی</p></div>
<div class="m2"><p>گاه مس بی کیمیا زر میکنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رخش گردون زیر ران داری مدام</p></div>
<div class="m2"><p>ملکت هر دوجهان داری مدام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پختگی جملهٔ خامان ز تست</p></div>
<div class="m2"><p>زینت و زیب نکو نامان ز تست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من ز مقصودم جدا افتادهام</p></div>
<div class="m2"><p>سرنگون در صد بلا افتادهام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر ز مقصودم نشانی میدهی</p></div>
<div class="m2"><p>مرده را انگار جانی میدهی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آفتاب این قصه را چون کرد گوش</p></div>
<div class="m2"><p>بر رخش پروین اشک آمد بجوش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت من هم نیز غمگینم چو تو</p></div>
<div class="m2"><p>دم بدم سرگشتهٔ اینم چو تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>روی زردم زین غم و جامه کبود</p></div>
<div class="m2"><p>میزنم تک در فراز ودر فرود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روز و شب زین عشق افروزندهام</p></div>
<div class="m2"><p>سال و ماه از شوق این سوزندهام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پای از سر می ندانم سر ز پای</p></div>
<div class="m2"><p>میدوم هر ساعت از جائی بجای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چشمهٔ بی آب از این غم ماندهام</p></div>
<div class="m2"><p>دایماً در تاب ازین غم ماندهام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گه سپر بر آب اندازم ز میغ</p></div>
<div class="m2"><p>گه بقتل خویش دست آرم بتیغ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گاه بر خاک اوفتم زین درد من</p></div>
<div class="m2"><p>گه برایم سرخ و گاهی زرد من</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>صد هزاران رنگ در کار آورم</p></div>
<div class="m2"><p>تا مگر بوئی پدیدار آورم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بی سر و بن گرچه میگردم چو گوی</p></div>
<div class="m2"><p>کار می برنایدم از رنگ و بوی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>من که چشمم وین همه گردیده‌ام</p></div>
<div class="m2"><p>کافرم گر هیچ بویی دیده‌ام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گردهٔ هر شب برم در کوی او</p></div>
<div class="m2"><p>تا مگر چیزی کند بر روی او</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>من ز تو حیران ترم بگذر ز من</p></div>
<div class="m2"><p>زانکه نگشاید ترا این در ز من</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سالک آمد پیش پیر دیده ور</p></div>
<div class="m2"><p>کرد از حال خودش حالی خبر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پیر گفتش آفتاب اندر صفت</p></div>
<div class="m2"><p>بارگاه همتست و معرفت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هرکه صاحب همت آمد مرد شد</p></div>
<div class="m2"><p>همچو خورشید از بلندی فرد شد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر چو گوهر همت عالی بود</p></div>
<div class="m2"><p>بر سر زر جای تو خالی بود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر بهر چیزی فرود آئی براه</p></div>
<div class="m2"><p>کی توانی خورد جام از دست شاه</p></div></div>