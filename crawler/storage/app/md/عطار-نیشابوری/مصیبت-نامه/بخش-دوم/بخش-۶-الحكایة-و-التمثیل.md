---
title: >-
    بخش ۶ - الحكایة ‌و التمثیل
---
# بخش ۶ - الحكایة ‌و التمثیل

<div class="b" id="bn1"><div class="m1"><p>گفت روزی شبلی افتاده کار</p></div>
<div class="m2"><p>در بردیوانگان شد سوکوار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دید آنجا پس جوان دیوانهٔ</p></div>
<div class="m2"><p>آشنا با حق نه چون بیگانهٔ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت شبلی را که مردی روشنی</p></div>
<div class="m2"><p>گر سحرگاهان مناجاتی کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از زفان من بگو با کردگار</p></div>
<div class="m2"><p>کوفکندی در جهانم بی قرار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دور کردی از پدر وز مادرم</p></div>
<div class="m2"><p>ژندهٔ بگذاشتی اندر برم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پردهٔ عصمت ز من برداشتی</p></div>
<div class="m2"><p>در غریبی بی دلم بگذاشتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کردی آواره ز خان و مان مرا</p></div>
<div class="m2"><p>آتشی انداختی در جان مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آتش تو گرچه در جانم خوشست</p></div>
<div class="m2"><p>برجگر بیآییم زان آتشست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بستی از زنجیر سر تا پای من</p></div>
<div class="m2"><p>تا رهائی یابم ازتو وای من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر ترا گویم چه میسازی مرا</p></div>
<div class="m2"><p>در بلای دیگر اندازی مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه مرا جامه نه نانی میدهی</p></div>
<div class="m2"><p>نان چرا ندهی چو جانی میدهی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چند باشم گرسنه این جایگاه</p></div>
<div class="m2"><p>گر نداری نان زجائی وام خواه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این بگفت وپارهٔ شد هوشیار</p></div>
<div class="m2"><p>بعد از آن بگریست لختی زار زار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت ای شیخ آنچه گفتم بیشکی</p></div>
<div class="m2"><p>گر بگوئی بو که درگیرد یکی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رفت شبلی از برش گریان شده</p></div>
<div class="m2"><p>در تحیر مانده سرگردان شده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون برون رفت از در آن خانه زود</p></div>
<div class="m2"><p>دادش آواز از پس آن دیوانه زود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفت زنهار ای امام رهنمای</p></div>
<div class="m2"><p>تانگوئی آنچه گفتم با خدای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زانکه گر با او بگوئی اینقدر</p></div>
<div class="m2"><p>زآنچه میکرد او کند صد ره بتر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>من نخواهم خواست از حق هیچ چیز</p></div>
<div class="m2"><p>زآنکه با او درنگیرد هیچ نیز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>او همه با خویش میسازد مدام</p></div>
<div class="m2"><p>هرچه گوئی هیچ باشد والسلام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دوستان را هر نفس جانی دهد</p></div>
<div class="m2"><p>لیک جان سوزد اگر نانی دهد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر بلا کین قوم راحق داده است</p></div>
<div class="m2"><p>زیر آن گنج کرم بنهاده است</p></div></div>