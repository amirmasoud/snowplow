---
title: >-
    بخش ۴ - الحكایة ‌و التمثیل
---
# بخش ۴ - الحكایة ‌و التمثیل

<div class="b" id="bn1"><div class="m1"><p>بود شاهی را غلامی سیمبر</p></div>
<div class="m2"><p>هم ادب از پای تا سر هم هنر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بخندیدی لب گلرنگ او</p></div>
<div class="m2"><p>گلشکر گشتی فراخ از تنگ او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماه را خورشید رویش مایه داد</p></div>
<div class="m2"><p>مهر را زلف سیاهش سایه داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دام مشکینش چوشست انداختی</p></div>
<div class="m2"><p>جان بهای و دل ز دست انداختی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راستی از بس کژی کان شست بود</p></div>
<div class="m2"><p>صیدش از هفتاد فرقت شست بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ابروی او در کژی طاق آمده</p></div>
<div class="m2"><p>راستی محراب عشاق آمده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مردمی چشم او در جادوئی</p></div>
<div class="m2"><p>ترک تازش در میان هندوئی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از میانش بود دل در هیچ و بس</p></div>
<div class="m2"><p>وز دهانش روح در ضیق النفس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لعل او را وصف کردن راه نیست</p></div>
<div class="m2"><p>زانکه کس از آب خضر آگاه نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این غلام دلربای جان فزای</p></div>
<div class="m2"><p>پیش شاه خویش استادی بپای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از قضا روزی مگر در پیش شاه</p></div>
<div class="m2"><p>کرد بسیاری همی در خود نگاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شاه حالی دشنهٔ زد بر دلش</p></div>
<div class="m2"><p>جان بداد و آن جهان شد منزلش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پس زفان در خشم او بگشاد شاه</p></div>
<div class="m2"><p>گفت تا چندی کنی بر خود نگاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گه علم میبینی و بازوی خویش</p></div>
<div class="m2"><p>گه نظاره میکنی بر موی خویش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گه کنی در پا و در موزه نگاه</p></div>
<div class="m2"><p>گه نهی از پیش و گاه از پس کلاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گه شوی مشغول در انگشتری</p></div>
<div class="m2"><p>خودپرستی تو و یا خدمت گری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون چنین تو عاشق خویش آمدی</p></div>
<div class="m2"><p>بهر خدمت از چه در پیش آمدی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ترک خدمت گیر و خود را میپرست</p></div>
<div class="m2"><p>بعدازین برخیز و با خود کن نشست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دعوی خدمت کنی با شهریار</p></div>
<div class="m2"><p>خود ز عشق خویش باشی بی قرار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گرچه خود را سخت بخرد میکنی</p></div>
<div class="m2"><p>در حقیقت خدمت خود میکنی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>من ز تو بر مینگیرم یک نظر</p></div>
<div class="m2"><p>تو زخود دیدن نمیآئی بسر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مردم دیده چو خود بینی نکرد</p></div>
<div class="m2"><p>جای خود جز دیده میبینی نکرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کار نزدیکان خطر دارد بسی</p></div>
<div class="m2"><p>چون تواند جست نزدیکی کسی</p></div></div>