---
title: >-
    بخش ۷ - الحكایة ‌و التمثیل
---
# بخش ۷ - الحكایة ‌و التمثیل

<div class="b" id="bn1"><div class="m1"><p>سوی آن دیوانه شد مردی عزیز</p></div>
<div class="m2"><p>گفت هستت آرزوی هیچ چیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت ده روزست تا من گرسنه</p></div>
<div class="m2"><p>ماندهام لوتیم باید ده تنه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت دل خوش کن که رفتم این زمانت</p></div>
<div class="m2"><p>از پی حلوا و بریانی و نانت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت غلبه میمکن ای ژاژ خای</p></div>
<div class="m2"><p>نرم گو تا نشنود یعنی خدای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نیم آهسته کن آواز را</p></div>
<div class="m2"><p>زانکه گر حق بشنود این راز را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچ نگذارد که نانم آوری</p></div>
<div class="m2"><p>لیک گوید تا بجانم آوری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوست را زان گرسنه دارد مدام</p></div>
<div class="m2"><p>تا ز جان خویش سیر آید تمام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون زجان سیرآید او در درد کار</p></div>
<div class="m2"><p>گرسنه گردد بجانان بی قرار</p></div></div>