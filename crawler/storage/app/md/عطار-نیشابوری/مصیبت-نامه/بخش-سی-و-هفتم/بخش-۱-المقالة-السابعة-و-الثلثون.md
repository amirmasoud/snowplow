---
title: >-
    بخش ۱ - المقالة السابعة و الثلثون
---
# بخش ۱ - المقالة السابعة و الثلثون

<div class="b" id="bn1"><div class="m1"><p>سالک آتش دل شوریده حال</p></div>
<div class="m2"><p>شد ز خیل حس برون پیش خیال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت ای در اصل یک ذات آمده</p></div>
<div class="m2"><p>پنج محسوست مقامات آمده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو یکی و جملهٔ پاک و نجس</p></div>
<div class="m2"><p>میکنی ادراک همچون پنج حس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شم و ذوق و لمس با سمع و بصر</p></div>
<div class="m2"><p>کرده یک لوح ترا ذات الصور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنچه حاجت بود پنج آلت برونش</p></div>
<div class="m2"><p>تو بیک آلت گرفتی در درونش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پارهٔ چون دور بودی از عدد</p></div>
<div class="m2"><p>پنج مدرک نقدت آمد از احد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون زمانی و مکانی آمدی</p></div>
<div class="m2"><p>پنج ره در خرده دانی آمدی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرچه بودت پنج محسوس آشکار</p></div>
<div class="m2"><p>مدرکت هر پنج شد در پنج یار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون نیارستی بیک ره پنج دید</p></div>
<div class="m2"><p>از زمان ذات تو چندین رنج دید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وی عجب از پنج ادراک قوی</p></div>
<div class="m2"><p>صورتی ماند از زمانه معنوی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون بوحدت آمدی نزدیک تر</p></div>
<div class="m2"><p>بود راه تو ز حس باریک تر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پس بوحدت از عدد درکش مرا</p></div>
<div class="m2"><p>ره بمن بنمای و کن دلخوش مرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا برون آیم ز چندین تفرقه</p></div>
<div class="m2"><p>خرقه بر آتش نهم ازمخرقه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سر بوادی محبت آورم</p></div>
<div class="m2"><p>ره درین غربت بقربت آورم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زین سخن همچون خیالی شد خیال</p></div>
<div class="m2"><p>حال بر وی گشت حالی زین محال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفت من زین نقد بس دور آمدم</p></div>
<div class="m2"><p>زینچه میجوئی تو مهجور آمدم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون بمن در خواب میآید خطاب</p></div>
<div class="m2"><p>کی توانم دید بیداری بخواب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هیچ صورت هیچ معنی هیچ کار</p></div>
<div class="m2"><p>نیست جز در پرده بر من آشکار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آنکه در پرده بود فریاد خواه</p></div>
<div class="m2"><p>دیگری را چون دهد در پرده راه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هیچ نگشاید ز من در هیچ حال</p></div>
<div class="m2"><p>من خیالم چند پیمائی خیال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر طلبکاری ازینجا نقل کن</p></div>
<div class="m2"><p>پای نه بر حس و ره بر عقل کن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سالک آمد پیش پیر مهربان</p></div>
<div class="m2"><p>حال خود با او نهاد اندر میان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پیر گفتش هست دیوان خیال</p></div>
<div class="m2"><p>از حس و از عقل پر خیل مثال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هرکجا صورت جمال آرد پدید</p></div>
<div class="m2"><p>زو مثالی در خیال آرد پدید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>قسم حس آمد فراق اما خیال</p></div>
<div class="m2"><p>نقددارد از همه عالم وصال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هرچه خواهد جمله در پیشش بود</p></div>
<div class="m2"><p>وینچنین وصلی هم از خویشش بود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>حس چنان در بعد افتادست طاق</p></div>
<div class="m2"><p>کز وصال نقد بیند صد فراق</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نانهاده یک قدم در وصل خویش</p></div>
<div class="m2"><p>صد فراقش آید از هر سوی پیش</p></div></div>