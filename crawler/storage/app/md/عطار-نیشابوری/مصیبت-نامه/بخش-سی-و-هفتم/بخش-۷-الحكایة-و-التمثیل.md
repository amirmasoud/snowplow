---
title: >-
    بخش ۷ - الحكایة ‌و التمثیل
---
# بخش ۷ - الحكایة ‌و التمثیل

<div class="b" id="bn1"><div class="m1"><p>در مناجات آن بزرگ کاردان</p></div>
<div class="m2"><p>گفت ای دانندهٔ‌اسرار دان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کور گردان خلق را در رستخیز</p></div>
<div class="m2"><p>پس مرا جاوید چشمی بخش نیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نبیند هیچکس جز من ترا</p></div>
<div class="m2"><p>تا توانم دید بی دشمن ترا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بعد از آن چون مدتی بگذشت ازین</p></div>
<div class="m2"><p>زینچه میخواست او ز حق برگشت ازین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت ای یاری ده هر دم مرا</p></div>
<div class="m2"><p>در قیامت کور گردان هم مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا نبینم آن جمال پر فروغ</p></div>
<div class="m2"><p>کان بخویش آید دریغم بی دروغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه غیرت بردن از عاشق نکوست</p></div>
<div class="m2"><p>غیرت معشوق دایم بیش ازوست</p></div></div>