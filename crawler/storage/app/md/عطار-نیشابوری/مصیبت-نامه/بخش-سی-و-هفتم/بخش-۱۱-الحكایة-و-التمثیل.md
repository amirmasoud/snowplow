---
title: >-
    بخش ۱۱ - الحكایة ‌و التمثیل
---
# بخش ۱۱ - الحكایة ‌و التمثیل

<div class="b" id="bn1"><div class="m1"><p>رفت دزدی در سرای رابعه</p></div>
<div class="m2"><p>خفته بود آن مرغ صاحب واقعه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چادرش برداشت راه در نیافت</p></div>
<div class="m2"><p>باز بنهاد و بسوی در شتافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بازبرداشت و بیامد ره ندید</p></div>
<div class="m2"><p>باز چون بنهاد شد درگه پدید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گشت عاجز هاتفیش آواز داد</p></div>
<div class="m2"><p>گفت چادر باید این دم باز داد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زانکه گر شد دوستی درخواب مست</p></div>
<div class="m2"><p>دوستی دیگر چنین بیدار هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چادرش بنهی اگر در بایدت</p></div>
<div class="m2"><p>ورنه بنشینی چو چادر بایدت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرچه هستت چون برای او بود</p></div>
<div class="m2"><p>دوستی تو سزای او بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ور تو خود را دوستر داری ازو</p></div>
<div class="m2"><p>دشمنی تو گر خبر داری ازو</p></div></div>