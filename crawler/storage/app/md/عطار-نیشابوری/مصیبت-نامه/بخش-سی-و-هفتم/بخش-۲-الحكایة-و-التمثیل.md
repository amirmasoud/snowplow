---
title: >-
    بخش ۲ - الحكایة ‌و التمثیل
---
# بخش ۲ - الحكایة ‌و التمثیل

<div class="b" id="bn1"><div class="m1"><p>بوعلی دقاق آن شیخ جهان</p></div>
<div class="m2"><p>شد بنزدیک مریدی میهمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن مرید از عشق او میسوخت زار</p></div>
<div class="m2"><p>کرده بودش روزگاری انتظار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شیخ بنشست آن مرید نونیاز</p></div>
<div class="m2"><p>گفت شیخا کی بخواهی رفت باز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت ناافتاده وصلی اتفاق</p></div>
<div class="m2"><p>پیش باز آوردی آواز فراق</p></div></div>