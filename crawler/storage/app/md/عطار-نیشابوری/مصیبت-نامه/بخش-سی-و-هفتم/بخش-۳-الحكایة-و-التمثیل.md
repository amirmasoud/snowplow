---
title: >-
    بخش ۳ - الحكایة ‌و التمثیل
---
# بخش ۳ - الحكایة ‌و التمثیل

<div class="b" id="bn1"><div class="m1"><p>کاملی گفتست کز بیم گناه</p></div>
<div class="m2"><p>گر نبودی پیش حاصل رنج راه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا زجان کندن بلا بودی و بس</p></div>
<div class="m2"><p>یا عذاب گور بودی پیش و پس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا صراطستی و یا میزانستی</p></div>
<div class="m2"><p>هرچه هستی آن همه آسانستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این همه سهل است اگر نبود فراق</p></div>
<div class="m2"><p>چون بود فرقت دلی پر اشتیاق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر عذابی کان همی داند یکی</p></div>
<div class="m2"><p>جمله در جنب فراقست اندکی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو چه دانی ای پسر سوز فراق</p></div>
<div class="m2"><p>عاشقی داند دلی پراشتیاق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو چو عاشق نیستی دل مردهٔ</p></div>
<div class="m2"><p>دعوی عشق از چه در سرکردهٔ</p></div></div>