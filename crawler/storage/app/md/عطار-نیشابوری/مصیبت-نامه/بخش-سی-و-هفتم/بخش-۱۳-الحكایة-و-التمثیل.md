---
title: >-
    بخش ۱۳ - الحكایة ‌و التمثیل
---
# بخش ۱۳ - الحكایة ‌و التمثیل

<div class="b" id="bn1"><div class="m1"><p>بود محمود و حسن در بارگاه</p></div>
<div class="m2"><p>گشته هم خلوت وزیر و پادشاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه یکی آمد نه یک تن راه خواست</p></div>
<div class="m2"><p>نه گدائی قرب شاهنشاه خواست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیچکس در دادخواهی ره نجست</p></div>
<div class="m2"><p>هم رعیت هم سپاهی ره نجست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود بر درگاه آرامی عظیم</p></div>
<div class="m2"><p>نه امیدی هیچکس را و نه بیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با وزیر خویش گفت آن شهریار</p></div>
<div class="m2"><p>بر در ما کو نشان کار و بار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه کسی فریاد میخواهد زما</p></div>
<div class="m2"><p>نه گدائی داد میخواهد ز ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر کرا زینسان در عالی بود</p></div>
<div class="m2"><p>کی روا باشد اگر خالی بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این چنین درگاه عالی ای وزیر</p></div>
<div class="m2"><p>نیست خوش از شور خالی ای وزیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن وزیرش گفت عدلی این چنین</p></div>
<div class="m2"><p>کز تو ظاهر گشت درروی زمین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون جهان پر عدل دارد پادشاه</p></div>
<div class="m2"><p>کی تواند بود هرگز دادخواه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاه گفتا راست گفتی این زمان</p></div>
<div class="m2"><p>شور اندازم جهانی در جهان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این بگفت و لشکری را راست کرد</p></div>
<div class="m2"><p>پس ز هر شهر و دهی درخواست کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جوش و شوری در همه عالم فتاد</p></div>
<div class="m2"><p>درگه محمود خالی کم فتاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شد در او موج زن از کار و بار</p></div>
<div class="m2"><p>آنچه آن میخواست آن گشت آشکار</p></div></div>