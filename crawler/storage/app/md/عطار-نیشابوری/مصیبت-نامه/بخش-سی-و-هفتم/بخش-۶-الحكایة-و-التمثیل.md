---
title: >-
    بخش ۶ - الحكایة ‌و التمثیل
---
# بخش ۶ - الحكایة ‌و التمثیل

<div class="b" id="bn1"><div class="m1"><p>در رهی میشد سلیمان با سپاه</p></div>
<div class="m2"><p>دید جفتی صعوه را یک جایگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دو عشق یکدگر میباختند</p></div>
<div class="m2"><p>هر دو با دل سوختن میساختند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گاه این یک ناز کرد و گاه آن</p></div>
<div class="m2"><p>گاه این آغاز کرد و گاه آن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صعوهٔ عاشق زفان بگشاد و گفت</p></div>
<div class="m2"><p>تو به نیکوئی مرا طاقی و جفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرچه فرمودی چنان کردم همه</p></div>
<div class="m2"><p>کارهای تو بجان کردم همه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ور دگر فرمائیم فرمان کنم</p></div>
<div class="m2"><p>هرچه تو حکمم کنی از جان کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر توام گوئی فرو آرم بخود</p></div>
<div class="m2"><p>قبهٔ ملک سلیمان از لگد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون سلیمان رفت با ایوان خویش</p></div>
<div class="m2"><p>گفت تا آن سعوه را خواندند پیش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صعوه چون آمد بدید آن کار و بار</p></div>
<div class="m2"><p>شد ز لرزیدن چو برقی بیقرار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پس سلیمان گفت چندینی ملاف</p></div>
<div class="m2"><p>صعوهٔ را لاف مه از کوه قاف</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو که قادر نیستی یک حبه را</p></div>
<div class="m2"><p>از لگد چون بشکنی این قبه را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از سلیمان صعوه چون بشنود راز</p></div>
<div class="m2"><p>گفت ای در دین و دنیا سرفراز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نامهٔ ناموس عاشق را مدام</p></div>
<div class="m2"><p>مهری از یطوی و لایحکی تمام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عاشقان از بس که غیرت داشتند</p></div>
<div class="m2"><p>جان خود را غرق حیرت داشتند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از سر جان پاک بر میخاستند</p></div>
<div class="m2"><p>هرچه شان بایست در میخواستند</p></div></div>