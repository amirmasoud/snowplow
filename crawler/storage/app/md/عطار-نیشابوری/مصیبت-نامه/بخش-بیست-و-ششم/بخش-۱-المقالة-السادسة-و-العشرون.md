---
title: >-
    بخش ۱ - المقالة السادسة و العشرون
---
# بخش ۱ - المقالة السادسة و العشرون

<div class="b" id="bn1"><div class="m1"><p>سالک آمد پیش شیطان رجیم</p></div>
<div class="m2"><p>گفت ای مردود رحمن و رحیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای در اول مقتدای خواندگان</p></div>
<div class="m2"><p>وی در آخر پیشوای راندگان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای بیک بیحرمتی مفتون شده</p></div>
<div class="m2"><p>وی بیک ترک ادب ملعون شده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هفتصد باره هزاران سال تو</p></div>
<div class="m2"><p>جمع کردی سر حال و قال تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قال تو اغلال شد حالت محال</p></div>
<div class="m2"><p>مسخ گشتی تا نه پرماندی نه بال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر جرس جنبان دولت بودهٔ</p></div>
<div class="m2"><p>چون جرس اکنون همه بیهودهٔ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست کس از تو مصیبت دیده تر</p></div>
<div class="m2"><p>خشک لب بنشین مدام ودیده تر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در بهشت عدن بودی اوستاد</p></div>
<div class="m2"><p>کار تو با قعر دوزخ اوفتاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنچنان بوده چنین چون آمدی</p></div>
<div class="m2"><p>دی ملک امروز ملعون آمدی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آتش کفر تو در دین اوفتاد</p></div>
<div class="m2"><p>در همه عالم کرا این اوفتاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون فرشته خویش را دانی دلیر</p></div>
<div class="m2"><p>دیوی تو آشکار آمد نه دیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای فرشته دیو مردم آمدی</p></div>
<div class="m2"><p>در پری جفتی چو کژدم آمدی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر بسی بر دیگران فرقت نهند</p></div>
<div class="m2"><p>هم کلاه دیو برفرقت نهند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هم دل مؤمن تو داری در دهان</p></div>
<div class="m2"><p>هم توئی با خون دل در رگ روان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هم ز ماهی جایگه تا مه تراست</p></div>
<div class="m2"><p>هم ز مشرق تا بمغرب ره تراست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون جهانی درگرفتی پیش تو</p></div>
<div class="m2"><p>آگهم گردان ز کار خویش تو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر رهی دزدیده داری سوی گنج</p></div>
<div class="m2"><p>شرح ده تا من برون آیم ز رنج</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زین سخن ابلیس در خون اوفتاد</p></div>
<div class="m2"><p>آتشش از سینه بیرون اوفتاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گفت اول صد هزاران سال من</p></div>
<div class="m2"><p>خوردهام این جام مالامال من</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا بآخر جام کردم سرنگون</p></div>
<div class="m2"><p>درد لعنت آمد از زیرش برون</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در دو عالم نیست از سر تا بپای</p></div>
<div class="m2"><p>هیچ جائی تا نکردم سجده جای</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بس که بر ابلیس لعنت کردمی</p></div>
<div class="m2"><p>خویشتن را شکر نعمت کردمی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>من چه دانستم که این بد میکنم</p></div>
<div class="m2"><p>روز تا شب لعنت خود میکنم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ناگهی سیلاب محنت در رسید</p></div>
<div class="m2"><p>پس شبیخونی ز لعنت در رسید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>صد هزاران ساله اعمالم که بود</p></div>
<div class="m2"><p>در عزازیلی پر وبالم که بود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جمله را سیلاب لعنت پیش کرد</p></div>
<div class="m2"><p>تا مرا هم مسخ و هم بی خویش کرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>لاجرم ملعون و نافرمان شدم</p></div>
<div class="m2"><p>گر فرشته بودهام شیطان شدم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آنکه اول حور را همخوابه کرد</p></div>
<div class="m2"><p>این زمانش دیو در گرمابه کرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پای تا سر عین حسرت گشتهام</p></div>
<div class="m2"><p>در همه آفاق عبرت گشتهام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر تو از من عبرتی گیری رواست</p></div>
<div class="m2"><p>ور بکاری نیز میآئی خطاست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>صد جهان رحمت چرا بگذاشتی</p></div>
<div class="m2"><p>راه لعنت بی خبر برداشتی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>من ز لعنت دارم الحق دور باش</p></div>
<div class="m2"><p>تو نداری تاب لعنت دور باش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سالک آمد پیش پیر رهبران</p></div>
<div class="m2"><p>قصهٔ برگفت صد عبرت در آن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پیر گفتش هست ابلیس دژم</p></div>
<div class="m2"><p>عالم رشک و منی سر تا قدم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زانکه گفتندش که ای افتاده دور</p></div>
<div class="m2"><p>چون شدی در غایت دوری صبور</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گفت دور استادهام تیغی بدست</p></div>
<div class="m2"><p>باز میرانم از آن درهر که هست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تا نگردد گرد آن در هیچکس</p></div>
<div class="m2"><p>در همه عالم مرا این کار بس</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دور استادم دودیده همچو میغ</p></div>
<div class="m2"><p>زانکه آن رویم بخویش آید دریغ</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دور استادم که نتوانم که کس</p></div>
<div class="m2"><p>روی او بیند به جز من یک نفس</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دور استادم که من در راه او</p></div>
<div class="m2"><p>نیستم شایستهٔ درگاه او</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دور استادم نه پا نه سر ازو</p></div>
<div class="m2"><p>چون بسوزم دور اولیتر ازو</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دور استادم ز هجران تیره حال</p></div>
<div class="m2"><p>چون ندارم تاب قرب آن وصال</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گرچه هستم راندهٔ در گاه او</p></div>
<div class="m2"><p>سر نه پیچم ذرهٔ از راه او</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تانهادستم قدم در کوی یار</p></div>
<div class="m2"><p>ننگرستم هیچ سو جز سوی یار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چونشدم با سر معنی هم نفس</p></div>
<div class="m2"><p>ننگرم هرگز سر موئی بکس</p></div></div>