---
title: >-
    بخش ۴ - الحكایة و التمثیل
---
# بخش ۴ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>صاحب اطفالی ز غم میسوختی</p></div>
<div class="m2"><p>خار کندی تا شب و بفروختی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود بس درویش و پیر و ناتوان</p></div>
<div class="m2"><p>مانده در اطفال و در جفتی جوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا که نشکستی تنش صد ره نخست</p></div>
<div class="m2"><p>دست کی دادیش یک نان درست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خانهٔ او در میان دشت بود</p></div>
<div class="m2"><p>ناگهی موسی برو بگذشت زود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دید موسی را که میشد سوی طور</p></div>
<div class="m2"><p>گفت از بهر خداوند غفور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خدا در خواه تا هر روزیم</p></div>
<div class="m2"><p>میفرستد بی زحیری روزیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زانکه تا یک گرده دستم میدهد</p></div>
<div class="m2"><p>دور گردون صد شکستم میدهد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خار باید کند هر روزی مرا</p></div>
<div class="m2"><p>تا بدست آید مگر روزی مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از خدای خویش آن میبایدم</p></div>
<div class="m2"><p>کز سر فضلی دری بگشایدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون بشد موسی و با حق راز گفت</p></div>
<div class="m2"><p>قصهٔ آن پیر عاجز باز گفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حق تعالی گفت هرچه آن پیر خواست</p></div>
<div class="m2"><p>هیچدر دنیا نخواهد گشت راست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لیک دو حاجت که من میدانمش</p></div>
<div class="m2"><p>گر بخواهد آن روا گردانمش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>باز آمد موسی و گفت از خدا</p></div>
<div class="m2"><p>نیست جز دو حاجتت اینجا روا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون دوحاجت امرت آمد از اله</p></div>
<div class="m2"><p>غیر دنیا هرچه میخواهی بخواه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مرد شد در دشت تا خار آورد</p></div>
<div class="m2"><p>وآن دو حاجت نیز درکار آورد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پادشاهی از قضا در دشت بود</p></div>
<div class="m2"><p>بر زن آن خارکش بگذشت زود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صورتی میدید بس صاحب جمال</p></div>
<div class="m2"><p>در صفت ناید که چون شد در جوال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شاه گفتا کیست او را بارکش</p></div>
<div class="m2"><p>آن یکی گفتا که پیری خارکش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شاه گفتا نیست او در خورد او</p></div>
<div class="m2"><p>کس نمیدانم به جز خود مرد او</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در زمان فرمود زنرا شاه دهر</p></div>
<div class="m2"><p>تا که در صندوق بردندش بشهر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون نماز دیگری آن خارکش</p></div>
<div class="m2"><p>سوی کنج خویش آمد بارکش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دید طفلان را جگر بریان شده</p></div>
<div class="m2"><p>در غم مادر همه گریان شده</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>باز پرسید او که مادرتان کجاست</p></div>
<div class="m2"><p>قصه پیش پیر برگفتند راست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پیر سرگردان شد و خون میگریست</p></div>
<div class="m2"><p>زانکه بی زن هیچ نتوانست زیست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گفت یا رب بر دلم بخشودهٔ</p></div>
<div class="m2"><p>وین دوحاجت هم توام فرمودهٔ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یا رب آن زن را تو میدانی همی</p></div>
<div class="m2"><p>این زمان خرسیش گردانی همی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گفت این و رفت با عیشی چو زهر</p></div>
<div class="m2"><p>از برای نان طفلان سوی شهر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شاه چون با شهر آمد از شکار</p></div>
<div class="m2"><p>گفت آن صندوق ای خادم بیار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون در صندوق بگشادند باز</p></div>
<div class="m2"><p>روی خرسی دید شاه سرفراز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گفت گوئی او پری دارد مگر</p></div>
<div class="m2"><p>هر زمانش صورتی باشد دگر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هین برید او را بجای خویش باز</p></div>
<div class="m2"><p>تا مگر بر ما پری ناید فراز</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خارکش در شهر چون بفروخت خار</p></div>
<div class="m2"><p>نان خرید و سوی طفلان رفت زار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دید خرسی را میان کودکان</p></div>
<div class="m2"><p>در گریز از بیم او آن طفلکان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خارکش چون خرس را آنجا بدید</p></div>
<div class="m2"><p>گفتیی صد تشنه یک دریا بدید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گفت یا رب حاجتی ماندست و بس</p></div>
<div class="m2"><p>همچنانش کن که بود او این نفس</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خرس شد حالی چنان کز پیش بود</p></div>
<div class="m2"><p>در نکوئی گوئیا زان بیش بود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چون شد آن اطفال را مادر پدید</p></div>
<div class="m2"><p>هر یکی را دل ز شادی بر پرید</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مرد را چون آن دو حاجت شد روا</p></div>
<div class="m2"><p>آمد آن فرتوت غافل در دعا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ناسپاسی ترک گفت آن ناسپاس</p></div>
<div class="m2"><p>کرد حق را شکرهای بی قیاس</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گفت یارب تا نکو میداریم</p></div>
<div class="m2"><p>قانعم گر همچنین بگذاریم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پیش ازین از ناسپاسی میگداخت</p></div>
<div class="m2"><p>قدر آن کز پیش بود اکنون شناخت</p></div></div>