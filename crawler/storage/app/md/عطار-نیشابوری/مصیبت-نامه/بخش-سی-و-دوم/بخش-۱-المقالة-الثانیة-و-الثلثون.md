---
title: >-
    بخش ۱ - المقالة الثانیة و الثلثون
---
# بخش ۱ - المقالة الثانیة و الثلثون

<div class="b" id="bn1"><div class="m1"><p>سالک آمد پیش موسی ناصبور</p></div>
<div class="m2"><p>موسم موسی بدید از کوه طور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت ای نور دو عالم ذات تو</p></div>
<div class="m2"><p>نه فلک ده یک زنه آیات تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای بشب گنج الهی یافته</p></div>
<div class="m2"><p>از شبانی پادشاهی یافته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در شبانی گر رمه کردی بدست</p></div>
<div class="m2"><p>بلکه در یک شب همه کردی بدست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو چه دانستی که با چندین رمه</p></div>
<div class="m2"><p>آن همه حاصل کنی با این همه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از گلیمی آمدی بیرون کلیم</p></div>
<div class="m2"><p>در شبانی پادشا گشتی مقیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در همه آفاق روزانو شبان</p></div>
<div class="m2"><p>این چنین روزی نیابد یک شبان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روزیت چون در شبانی شد قوی</p></div>
<div class="m2"><p>در شبانی ختم کردی شبروی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون شنود انی انا اللّه گوش تو</p></div>
<div class="m2"><p>هفت دریا خاست از یک جوش تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آتش حضرت ز راهت در ربود</p></div>
<div class="m2"><p>کهربای حق چو کاشت در ربود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بود تا آتش ز تو صد ساله راه</p></div>
<div class="m2"><p>تو بیک جذبه شدی آنجایگاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کرد آن آتش جهان بر تو فراخ</p></div>
<div class="m2"><p>ای همه سر سبزی آن سبز شاخ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون شدی بیخود ز کاس اصطناع</p></div>
<div class="m2"><p>کرد جان تو کلام حق سماع</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از حجب چون آن کلام آمد بدر</p></div>
<div class="m2"><p>گشت یک یک ذره داودی دگر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صد جهان پرعقل بایستی و هوش</p></div>
<div class="m2"><p>تا شدی آنجایگه جاوید گوش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>این چنین دولت که جاویدان تراست</p></div>
<div class="m2"><p>خاص سلطانی و خود سلطان تراست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر کنی یک ذره دولت قسم من</p></div>
<div class="m2"><p>در دو عالم با سر آید اسم من</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>موسی عمرانش گفت ای سوخته</p></div>
<div class="m2"><p>تانگردی آتشی افروخته</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جان نسوزی تن نفرسائی تمام</p></div>
<div class="m2"><p>ره نیابی سوی جانان والسلام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اول از هستی خود بیزار شو</p></div>
<div class="m2"><p>پس بعشق نیستی در کار شو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر شوی در نیستی صاحب نظر</p></div>
<div class="m2"><p>در جهان فقر گردی دیده ور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فقر کلی نقد خاص مصطفاست</p></div>
<div class="m2"><p>بی قبول او نیاید کار راست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون بدیدم فقر و صاحب همتیش</p></div>
<div class="m2"><p>خواستم از حق تعالی امتیش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون تو هستی امت او شاد باش</p></div>
<div class="m2"><p>بندگی او کن و آزاد باش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>راه او گیر و هوای او طلب</p></div>
<div class="m2"><p>در رضای حق رضای او طلب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مرده دل مردی تو و راهیست دور</p></div>
<div class="m2"><p>زنده کن جان از دم صاحب زبور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سالک آمد پیش پیر پاک ذات</p></div>
<div class="m2"><p>شرح دادش آنچه بود از مشکلات</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پیر گفتش جان موسی کلیم</p></div>
<div class="m2"><p>عالم عشق است و دریای عظیم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در جهان عشق او دارد سبق</p></div>
<div class="m2"><p>عشق را او میسزد الحق بحق</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>عشق دولت خانهٔ هر دو جهانست</p></div>
<div class="m2"><p>هرکه عاشق نیست داوش در میانست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>روی میباید بخون خویش شست</p></div>
<div class="m2"><p>تا بود در عشق مرغ جانت چست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>عاشقی در عشق اگر نیکو بود</p></div>
<div class="m2"><p>خویشتن کشتن طریق او بود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هر کرا با عشق دمسازی فتاد</p></div>
<div class="m2"><p>کمترین چیزیش جانبازی فتاد</p></div></div>