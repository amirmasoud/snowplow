---
title: >-
    بخش ۱۰ - الحكایة و التمثیل
---
# بخش ۱۰ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>نوح پیغامبر چو از کفار رست</p></div>
<div class="m2"><p>با چهل تن کرد بر کوهی نشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برد یک تن زان چهل کس کوزه گر</p></div>
<div class="m2"><p>برگشاد او یک دکان پر کوزه در</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جبرئیل آمد که میگوید خدای</p></div>
<div class="m2"><p>بشکنش این کوزهها ای رهنمای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نوح گفتش آن همه نتوان شکست</p></div>
<div class="m2"><p>کین بصد خون دلش آمد بدست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه کوزه بشکنی گل بشکند</p></div>
<div class="m2"><p>در حقیقت مرد رادل بشکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باز جبریل آمد و دادش پیام</p></div>
<div class="m2"><p>گفت میگوید خداوندت سلام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پس چنین میگوید او کای نیکبخت</p></div>
<div class="m2"><p>گر شکست کوزهٔ چندست سخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این بسی زان سخت تر در کل یاب</p></div>
<div class="m2"><p>کز دعائی خلق را دادی بآب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همتی را بر همه بگماشتی</p></div>
<div class="m2"><p>لاتذر گفتی و کس نگذاشتی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یک دکان کوزه بشکستن خطاست</p></div>
<div class="m2"><p>یک جهان پر آدمی کشتن رواست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خود دلت میداد ای شیخ کبار</p></div>
<div class="m2"><p>زان همه مردم برآوردن دمار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کز پی آن بندگان بی قرار</p></div>
<div class="m2"><p>لطف ما چندان همی بگریست زار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کاین زمانش در گرفت از گریه چشم</p></div>
<div class="m2"><p>تو مرو از کوزهٔ چندین بخشم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یا رب این خود چه عنایت کردنست</p></div>
<div class="m2"><p>این چه شکر اندر شکایت کردنست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که بجانها میکند چندین عتاب</p></div>
<div class="m2"><p>گاه جانها میکند خون بی حساب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صد هزاران بی سر و بن را بخواند</p></div>
<div class="m2"><p>جمله رادر کشتی حیرت نشاند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بعد ازان کشتی بدریا درفکند</p></div>
<div class="m2"><p>صد جهان جان را بغوغا درفکند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بعد ازان باد مخالف روز و شب</p></div>
<div class="m2"><p>گرد کشتی میفرستاد ای عجب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تادران دریای بی پایان همه</p></div>
<div class="m2"><p>سر بسر برخاستند از جان همه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جمله را بگسست در دریا نفس</p></div>
<div class="m2"><p>از همه با سر نیامد هیچ کس</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گرچه فرض افتاد مردن پیشه کرد</p></div>
<div class="m2"><p>میندارم زهره این اندیشه کرد</p></div></div>