---
title: >-
    بخش ۱ - المقالة الرابعه
---
# بخش ۱ - المقالة الرابعه

<div class="b" id="bn1"><div class="m1"><p>سالک سرکش سر گردن کشان</p></div>
<div class="m2"><p>پیش عزرائیل آمد جان فشان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت ای جان تشنهٔ دیدار تو</p></div>
<div class="m2"><p>نفس گو سر میزن اندر کار تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طاقت هجران نداری اینت خوش</p></div>
<div class="m2"><p>جان بجانان میسپاری اینت خوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فالق الاصباح فی الاشباح تو</p></div>
<div class="m2"><p>باسط الید قابض الارواح تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اول نام تو از نام عزیز</p></div>
<div class="m2"><p>یافته عزت چه خواهد بود نیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون جمالت ذرهٔدید آفتاب</p></div>
<div class="m2"><p>گشت سرگردان نمیآورد تاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلق عالم چون ببینند آن جمال</p></div>
<div class="m2"><p>جان برافشانند جمله کرده حال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرکه رویت دید جان افشاند و رفت</p></div>
<div class="m2"><p>دامن از هر دو جهان افتشاند و رفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خلق گوید مرد زو گم شد نشان</p></div>
<div class="m2"><p>زنده است او بر تو کرده جان فشان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میسزد گر جان بر افشانیش تو</p></div>
<div class="m2"><p>تا بجانان زنده گردانیش تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زندگی کردن بجان زیبنده نیست</p></div>
<div class="m2"><p>جز بجانان زنده بودن زنده نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون بدست تست جان را زندگی</p></div>
<div class="m2"><p>ماندهام دل مرده در افکندگی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جان بگیر و زنده دل گردان مرا</p></div>
<div class="m2"><p>زانکه بی جانان نباید جان مرا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا که عزرائیل این پاسخ شنید</p></div>
<div class="m2"><p>راست گفتی روی عزرائیل دید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت اگر از درد من آگاهئی</p></div>
<div class="m2"><p>این چنین چیزی ز من کی خواهئی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صد هزاران قرن شد تا روز و شب</p></div>
<div class="m2"><p>جان یک یک میستانم در تعب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>من بهر جانی که بستانم ز تن</p></div>
<div class="m2"><p>می بریزم خون جان خویشتن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دم بدم از بسکه جان برداشتم</p></div>
<div class="m2"><p>دل بکلی از جهان برداشتم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>با که کردند آنچه با من کردهاند</p></div>
<div class="m2"><p>صد جهان خونم بگردن کردهاند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر بگویم خوف خود از صد یکی</p></div>
<div class="m2"><p>ذره ذره گردی اینجا بیشکی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون نمیآیم ز خوف خود بسر</p></div>
<div class="m2"><p>کی توان کردن طلب چیزی دگر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو برو کز خوف کار آگه نهٔ</p></div>
<div class="m2"><p>در عزا بنشین که مرد ره نهٔ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سالک آمد پیش پیر کاردان</p></div>
<div class="m2"><p>داد شرح حال با بسیار دان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پیر گفتش هست عزرائیل پاک</p></div>
<div class="m2"><p>راه قهر و معدن مرگ و هلاک</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مرگ نه احمق نه بخرد را گذاشت</p></div>
<div class="m2"><p>نه یکی نیک و یکی بد را گذاشت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر تو زین قومی و گر زان دیگری</p></div>
<div class="m2"><p>همچو ایشان بگذری تا بنگری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هرکه مرد و گشت زیر خاک پست</p></div>
<div class="m2"><p>هر کسش گوید بیاسود و برست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مرگ را زرین نهنبن مینهند</p></div>
<div class="m2"><p>مردنت آسایش تن مینهند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>الحقت دنیا چه پر برگ اوفتاد</p></div>
<div class="m2"><p>کاولین آسایشش مرگ اوفتاد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چون ترازرین نهنبن هست مرگ</p></div>
<div class="m2"><p>دیگ را سر برگرفتن نیست برگ</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خیز تا گامی بگردون بر نهیم</p></div>
<div class="m2"><p>پس سر این دیگ پرخون برنهیم</p></div></div>