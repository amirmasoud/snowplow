---
title: >-
    بخش ۴ - الحكایة و التمثیل
---
# بخش ۴ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>آن یکی دیوانه برگوری بخفت</p></div>
<div class="m2"><p>از سر آن گور یک دم مینرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سائلی گفتش که تو آشفتهٔ</p></div>
<div class="m2"><p>جملهٔ عمر از چه اینجا خفتهٔ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیز سوی شهر آی ای بیقرار</p></div>
<div class="m2"><p>تا جهانی خلق بینی بیشمار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت این مرده رهم ندهد براه</p></div>
<div class="m2"><p>هیچ میگوید مرو زین جایگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زانکه از رفتن رهت گردد دراز</p></div>
<div class="m2"><p>عاقبت اینجات باید گشت باز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شهریان را چون بگورستانست راه</p></div>
<div class="m2"><p>من چه خواهم کرد شهری پرگناه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میروم گریان چو میغ از آمدن</p></div>
<div class="m2"><p>آه از رفتن دریغ از آمدن</p></div></div>