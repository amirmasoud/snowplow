---
title: >-
    بخش ۶ - الحكایة و التمثیل
---
# بخش ۶ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>در زمستان یک شبی بهلول مست</p></div>
<div class="m2"><p>پای در گل میشد و کفشی بدست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سائلی گفتش که سر داری براه</p></div>
<div class="m2"><p>تو کجا خواهی شدن زین جایگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت دارم سوی گورستان شتاب</p></div>
<div class="m2"><p>زانکه آنجا ظالمیست اندر عذاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میروم چون گور او پر آتش است</p></div>
<div class="m2"><p>گرم گردم زانکه سر ما ناخوش است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن یکی را این چنین مرگی بود</p></div>
<div class="m2"><p>وان دگر را مرگ او برگی بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ظلم آتش در درونت افکند</p></div>
<div class="m2"><p>در میان خاک و خونت افکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه راه ظلم از پیشان رود</p></div>
<div class="m2"><p>هرکه آن ره رفت سرگردان رود</p></div></div>