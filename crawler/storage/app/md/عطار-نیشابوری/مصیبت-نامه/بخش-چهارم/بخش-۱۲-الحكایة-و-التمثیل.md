---
title: >-
    بخش ۱۲ - الحكایة و التمثیل
---
# بخش ۱۲ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>چون برآمد جان باقی از خلیل</p></div>
<div class="m2"><p>باز پرسیدش خداوند جلیل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کای ز کل خلق نیکو بخت تر</p></div>
<div class="m2"><p>در جهان چه چیز دیدی سخت تر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت اگر کشتن پسر را سخت بود</p></div>
<div class="m2"><p>در سقر دیدن پدر را سخت بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در میان آتشم انداختی</p></div>
<div class="m2"><p>روزگاری با بلا درساختی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بسی سختی و پیچاپیچ بود</p></div>
<div class="m2"><p>در بر جان دادن آنها هیچ بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حق تعالی کرد سوی او خطاب</p></div>
<div class="m2"><p>گفت اگر جان دادنت آمد عذاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از پس جان دادن و مردن ز خویش</p></div>
<div class="m2"><p>هست چندان سختی زاندازه بیش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کانکه را شد نقد افتادن درو</p></div>
<div class="m2"><p>راحت روحست جان دادن درو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون چنین در کار مشکل ماندهٔ</p></div>
<div class="m2"><p>روز و شب بهر چه غافل ماندهٔ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چارهٔ این کار مشکل پیش گیر</p></div>
<div class="m2"><p>راه بر مرگست منزل پیش گیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ترک دنیا گیر و کار مرگ ساز</p></div>
<div class="m2"><p>راه بس دورست ره را برگ ساز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زانکه دنیا گر همه بر هم نهی</p></div>
<div class="m2"><p>باز مانی عاقبت دستی تهی</p></div></div>