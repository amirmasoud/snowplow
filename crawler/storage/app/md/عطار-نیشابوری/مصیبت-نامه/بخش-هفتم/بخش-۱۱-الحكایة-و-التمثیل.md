---
title: >-
    بخش ۱۱ - الحكایة و التمثیل
---
# بخش ۱۱ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>رفت یک روزی مگر بهلول مست</p></div>
<div class="m2"><p>در بر هارون و بر تختش نشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیل او چندان زدندش چوب و سنگ</p></div>
<div class="m2"><p>کز تن او خون روان شد بیدرنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون بخورد آن چوب بگشاد او زفان</p></div>
<div class="m2"><p>گفت هارون را که ای شاه جهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک زمان کاین جایگه بنشستهام</p></div>
<div class="m2"><p>از قفا خوردن ببین چون خستهام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو که اینجا کردهٔ عمری نشست</p></div>
<div class="m2"><p>بس که یک یک بند خواهندت شکست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک نفس را من بخوردم آن خویش</p></div>
<div class="m2"><p>وای بر تو زانچه خواهی داشت پیش</p></div></div>