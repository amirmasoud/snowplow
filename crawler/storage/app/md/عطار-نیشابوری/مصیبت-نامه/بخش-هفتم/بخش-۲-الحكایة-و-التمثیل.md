---
title: >-
    بخش ۲ - الحكایة و التمثیل
---
# بخش ۲ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>در رهی میرفت هارون الرشید</p></div>
<div class="m2"><p>بود تابستان و آبی ناپدید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تشنگی غالب شد و در تف و تاب</p></div>
<div class="m2"><p>چشم را بود ای عجب گربود آب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عابدی گفتش که ای شاه جهان</p></div>
<div class="m2"><p>تشنگی چون برتو افتاد این زمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر دلت از تشنگی گردد خراب</p></div>
<div class="m2"><p>ور نیابی فی المثل ده روز آب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر کسی یک نیمه خواهد ملک شاه</p></div>
<div class="m2"><p>تاترا یک شربت آب ارد براه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سر آن بر توانی خاست تو</p></div>
<div class="m2"><p>کژنشین با من بگو این راست تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت ملک خود کنم نیمی نثار</p></div>
<div class="m2"><p>تا رسد جانم بآب خوشگوار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت اگر آن شربت آبت در درون</p></div>
<div class="m2"><p>ره نیابد تا بزیر آید برون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر طبیبی خواهد آن نیمی دگر</p></div>
<div class="m2"><p>تا دهد آن آب را در تو گذر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن دگر نیمه توانی داد خوش</p></div>
<div class="m2"><p>برتوانی خاست زان آزاد خوش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت چون در من بود صد پیچ پیچ</p></div>
<div class="m2"><p>ملک با آن درد نبود هیچ هیچ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من بگویم ترک ملک و مرد خویش</p></div>
<div class="m2"><p>تا خلاصی باشدم از درد خویش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفت آن ملکت که در دفع عذاب</p></div>
<div class="m2"><p>میتوان کردن عوض با یک من آب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دل درو بیهوده چندینی مبند</p></div>
<div class="m2"><p>وز کفی دو آب چندینی مخند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ملکتی کان یک من آب ارزد ترا</p></div>
<div class="m2"><p>دل برو چندین چرا لرزد ترا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ملک عقبی خواه تا خرم بود</p></div>
<div class="m2"><p>ذرهٔ زان ملک صد عالم بود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عدل کن تادر میان این نشست</p></div>
<div class="m2"><p>ذرهٔ زان مملکت آری بدست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عدل نبود این که بنشینی خوشی</p></div>
<div class="m2"><p>میزنی در هر سرائی آتشی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر چو خود خواهی رعیت را مدام</p></div>
<div class="m2"><p>مملکت را عادلی باشی تمام</p></div></div>