---
title: >-
    بخش ۴ - الحكایة و التمثیل
---
# بخش ۴ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>خسروی قصری معظم ساز کرد</p></div>
<div class="m2"><p>اوستاد کار کار آغاز کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بر آن قصر زالی خانه داشت</p></div>
<div class="m2"><p>از همه عالم همان ویرانه داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاه را گفتند ای صاحب کمال</p></div>
<div class="m2"><p>گر نباشد کلبهٔ این پیر زال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قصر نبود چار سو آن را بخر</p></div>
<div class="m2"><p>تا شود قصرت مربع در نظر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیرزن را خواند شاه سخت کوش</p></div>
<div class="m2"><p>گفت گشت این کلبه را واجب فروش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا مربع گردد این قصر بلند</p></div>
<div class="m2"><p>این زمانت رخت میباید فکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیرزن گفتا که لا واللّه مگوی</p></div>
<div class="m2"><p>از فروش این بنا ای شه مگوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر ترا ملک جهان گردد تمام</p></div>
<div class="m2"><p>کار حرص تو کجا گیرد نظام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر کرا حرص جهان ازجان نخاست</p></div>
<div class="m2"><p>کی شود کارش بدین یک کلبه راست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ترک این گیر و مرا مپشول هیچ</p></div>
<div class="m2"><p>تا زآه من نگردی پیچ پیچ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صبر کرد القصه روزی پادشاه</p></div>
<div class="m2"><p>تا برفت آن پیره زن زان جایگاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شاه گفت آن خانه راویران کنید</p></div>
<div class="m2"><p>چارسویش با زمین یکسان کنید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هرچه دارد رخت او بر ره نهید</p></div>
<div class="m2"><p>پس بنای قصر من آنگه نهید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پیره زن آخر چو باز آمد ز راه</p></div>
<div class="m2"><p>کلبهٔ خود دید قصر پادشاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رخت خود بر راه دید انداخته</p></div>
<div class="m2"><p>کلبه را دیوار ایوان ساخته</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آتشی در جان آن غمگین فتاد</p></div>
<div class="m2"><p>چشم چون سیلاب ازان آتش گشاد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با دلی پرخون زدست شهریار</p></div>
<div class="m2"><p>روی رادر خاک ره مالید زار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفت اگر اینجا نبودم ای اله</p></div>
<div class="m2"><p>تو نبودی نیز هم این جایگاه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تن زدی تا کلبهٔ احزان من</p></div>
<div class="m2"><p>در هم افکندند بی فرمان من</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>این بگفت و با رخی تر خشک لب</p></div>
<div class="m2"><p>برکشید از حلق جان آهی عجب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>غلغلی در آسمان افتاد ازو</p></div>
<div class="m2"><p>سرنگون شد حالی آن بنیاد ازو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حق تعالی کرد آن شه را هلاک</p></div>
<div class="m2"><p>در سرای خود فرو بردش بخاک</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عدل کن در ملک چون فرزانگان</p></div>
<div class="m2"><p>تا نگردی سخرهٔ دیوانگان</p></div></div>