---
title: >-
    بخش ۹ - الحكایة و التمثیل
---
# بخش ۹ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>خواجهٔ اکافی آن برهان دین</p></div>
<div class="m2"><p>گفت سنجر را که ای سلطان دین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>واجبم آید بتو دادن زکات</p></div>
<div class="m2"><p>زانکه تو درویش حالی در حیات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر ترا ملک وزری هست این زمان</p></div>
<div class="m2"><p>هست آن جمله ازان مردمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کردهٔ از خلق حاصل آن همه</p></div>
<div class="m2"><p>بر تو واجب میشود تا وان همه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون از آن خود نبودت هیچ چیز</p></div>
<div class="m2"><p>زین همه منصب چه سودت هیچ نیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از همه کس گر چه داری بیشتر</p></div>
<div class="m2"><p>میندانم کس ز تو درویشتر</p></div></div>