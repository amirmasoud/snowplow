---
title: >-
    بخش ۲۴ - الحكایة و التمثیل
---
# بخش ۲۴ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>آن امام دین چنین گفتست راست</p></div>
<div class="m2"><p>کان چنان قربی که نزدیک خداست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اهل لطف و طبع را کس در جهان</p></div>
<div class="m2"><p>آن نیابد آشکارا و نهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از زفانها هر سخن بیرون رود</p></div>
<div class="m2"><p>از زفان شاعران موزون رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه بود او سرور پیغامبران</p></div>
<div class="m2"><p>گفت در زیر زفان شاعران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هست حق را گنجهای بیشمار</p></div>
<div class="m2"><p>سر آن یک میندانند از هزار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم قوافی کان خوش و یکسان بود</p></div>
<div class="m2"><p>زان سخن بسیار در قرآن بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر قوافی را رواجی نیستی</p></div>
<div class="m2"><p>بر سر هر خطبه تاجی نیستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نظم ونثری کان میان امتست</p></div>
<div class="m2"><p>از قوافی آن سخن را حرمتست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر پیمبرمی نخواندی شعر راست</p></div>
<div class="m2"><p>پادشا جولاهه گر نبود رواست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون جهودان ساحرش میخواندند</p></div>
<div class="m2"><p>بت پرستان شاعرش میخواندند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حق تعالی گفت این بس ظاهرست</p></div>
<div class="m2"><p>کو بحق نه ساحر ونه شاعرست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شاعری در منصب پیغامبری</p></div>
<div class="m2"><p>همچو حجامیست در اسکندری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آنکه باشد هر دو کونش ارزنی</p></div>
<div class="m2"><p>خوشه چینی چون کند در خرمنی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حق چو گفتش نیست شاعر زان نبود</p></div>
<div class="m2"><p>ورنه او را در سخن تاوان نبود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بود او هم در عرب هم در عجم</p></div>
<div class="m2"><p>افصح الفصحاء فی کل الامم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شاعران را نطق او خاموش کرد</p></div>
<div class="m2"><p>در لفظش حلقهشان در گوش کرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هم فصیحان پیش او الکن شدند</p></div>
<div class="m2"><p>هم ظریفان جهان کودن شدند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باز با جبریل گفت ای محترم</p></div>
<div class="m2"><p>من نیم قاری نبود او لاجرم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر دو عالم زیر پایش بود خاک</p></div>
<div class="m2"><p>گر نبود او قاری و شاعر چه باک</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شعر از طبع آید و پیغامبران</p></div>
<div class="m2"><p>طبع کی دارند همچون دیگران</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>روح قدسی را طبیعت کی بود</p></div>
<div class="m2"><p>انبیا را جز شریعت کی بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در سخن آمد بسی و اندکی</p></div>
<div class="m2"><p>گر بسنجی وزن گیرد بیشکی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شعر گفتن همچو زر پختن بود</p></div>
<div class="m2"><p>در عروض آوردنش سختن بود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>لیکن آن کس را که زر باشد بسی</p></div>
<div class="m2"><p>کی تواند سختن آن هرگز هر کسی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر بسنجی زر زر موزون بود</p></div>
<div class="m2"><p>ور بسی باشد ز وزن افزون بود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زر چو در میزان نمیگنجد بسی</p></div>
<div class="m2"><p>پس زر بسیار چون سنجد کسی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زر بسی سختن نه بس کاری بود</p></div>
<div class="m2"><p>چون توان سختن چو بسیاری بود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چون پیمبر خواجه اسرار بود</p></div>
<div class="m2"><p>در خور سرش سخن بسیار بود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون بسختن در نمیآید زرش</p></div>
<div class="m2"><p>همچنان ناسخته میشد از برش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر زر سخته دهد مرد کریم</p></div>
<div class="m2"><p>گرچه موزون باشد آن باشد سلیم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چون زر ناسخته او پخته بود</p></div>
<div class="m2"><p>سخت اگر گفتی سخن هم سخته بود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>حاتم طی را ترازو کی نکوست</p></div>
<div class="m2"><p>لیک فرش بخل را زوطی نکوست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پادشا را زور بازو بس بود</p></div>
<div class="m2"><p>دست زر پاشش ترازو بس بود</p></div></div>