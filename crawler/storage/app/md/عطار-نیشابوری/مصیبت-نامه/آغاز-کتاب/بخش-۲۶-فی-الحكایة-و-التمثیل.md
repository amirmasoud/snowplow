---
title: >-
    بخش ۲۶ - فی الحكایة و التمثیل
---
# بخش ۲۶ - فی الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>مصطفی کو بود دل جان را ز قدر</p></div>
<div class="m2"><p>منبری بنهاد حسان را ز قدر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر منبر فرستادش پگاه</p></div>
<div class="m2"><p>تا ادا میکرد شعر آنجایگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گه ثنا گفتیش گه آراستی</p></div>
<div class="m2"><p>گاه از وی قطعهٔ درخواستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنگرید ای منکران بیوفا</p></div>
<div class="m2"><p>تا کرا بنهاد منبر مصطفی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت حسان را ز احسان و کرم</p></div>
<div class="m2"><p>هست جبریل امین با تو بهم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواجهٔ دنیا و دین شمع کرام</p></div>
<div class="m2"><p>خواند ایشان را امیران کلام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شعر را جاوید چون نبود مزید</p></div>
<div class="m2"><p>اصدق قول عرب قول لبید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مصطفی گفتست شعر نامدار</p></div>
<div class="m2"><p>چون سخنهای دگر دارد شمار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زشت او زشت و نکوی اونکوست</p></div>
<div class="m2"><p>زشت دشمن دار نیکو دار دوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از ابوبکر وعمر هم شعر خواست</p></div>
<div class="m2"><p>اشعر از هر دو علی مرتضا است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نظم حسانی و اشعار حسن</p></div>
<div class="m2"><p>هست منقول از حسین و از حسن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شافعی را شعر هم بسیار هست</p></div>
<div class="m2"><p>وز امامان دگر اشعار هست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شعر اگر حکمت بود طاعت بود</p></div>
<div class="m2"><p>قیمتش هر روز و هر ساعت بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شعر بر حکمت پناهی یافتست</p></div>
<div class="m2"><p>کوبه یؤتی الحکمه راهی یافتست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شعر مدح و هزل گفتن هیچ نیست</p></div>
<div class="m2"><p>شعر حکمت به که در وی پیچ نیست</p></div></div>