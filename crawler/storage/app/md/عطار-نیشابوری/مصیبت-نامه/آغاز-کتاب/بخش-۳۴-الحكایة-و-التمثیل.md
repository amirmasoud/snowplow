---
title: >-
    بخش ۳۴ - الحكایة ‌و التمثیل
---
# بخش ۳۴ - الحكایة ‌و التمثیل

<div class="b" id="bn1"><div class="m1"><p>پیش ذوالقرنین شد مردی دژم</p></div>
<div class="m2"><p>سیم خواست از شاه عالم یک درم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاه کان بشنود گفت ای بی خبر</p></div>
<div class="m2"><p>از چو من شاهی که خواهد این قدر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت پس شهری ده و گنجی مرا</p></div>
<div class="m2"><p>تا براید کار بی رنجی مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت چندینی بشاه چین دهند</p></div>
<div class="m2"><p>تو که باشی تا ترا چندین دهند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سالک سرگشته چون اینجا رسید</p></div>
<div class="m2"><p>رخت همت هرچه بد اینجا کشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی از پس رفتنش ممکن نبود</p></div>
<div class="m2"><p>ور ز پس میرفت دل ساکن نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ره بپیشان بردنش امکان نداشت</p></div>
<div class="m2"><p>زانکه هیچ این ره سروپایان نداشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دید عالم عالم از خون موج زن</p></div>
<div class="m2"><p>گاه عرش و گاه گردون موج زن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صد هزاران عالم پر شور بود</p></div>
<div class="m2"><p>جای زاری بد نه جای زور بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لاجرم انبان زاری برگرفت</p></div>
<div class="m2"><p>در بدر بی زور زاری درگرفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خویشتن را رسته دید اول ز بند</p></div>
<div class="m2"><p>لاجرم بر شد برین دود بلند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پر شد از پندار وسودا در گرفت</p></div>
<div class="m2"><p>زان بدین زودی ز بالا درگرفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اول آغازی نهاد از جبرئیل</p></div>
<div class="m2"><p>صدقه میجست او چو ابناء السبیل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در بدر میرفت تا پایان کار</p></div>
<div class="m2"><p>بشنو اکنون قصه از پیشان کار</p></div></div>