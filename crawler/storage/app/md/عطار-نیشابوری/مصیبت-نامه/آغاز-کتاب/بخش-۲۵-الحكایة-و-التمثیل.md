---
title: >-
    بخش ۲۵ - الحكایة و التمثیل
---
# بخش ۲۵ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>گفت شهزادی مگر پیش پدر</p></div>
<div class="m2"><p>خواند یک روزی غلامی را بدر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت برخیز ای غلام چست کار</p></div>
<div class="m2"><p>نیم جو زر تره خر پیش من آر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاه گفت ای مدبر و ای هیچکس</p></div>
<div class="m2"><p>تو خسیسی هیچ ناید از تو خس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاه را کز نیم جو اندیشه است</p></div>
<div class="m2"><p>گو ترا تره فروشی پیشه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زین قدر آنرا که آگاهی بود</p></div>
<div class="m2"><p>کی سزاوار شهنشاهی بود</p></div></div>