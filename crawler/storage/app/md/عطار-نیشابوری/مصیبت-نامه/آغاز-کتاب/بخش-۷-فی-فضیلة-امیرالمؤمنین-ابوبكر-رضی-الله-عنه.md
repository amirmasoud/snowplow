---
title: >-
    بخش ۷ - فی فضیلة امیرالمؤمنین ابوبكر رضی الله عنه
---
# بخش ۷ - فی فضیلة امیرالمؤمنین ابوبكر رضی الله عنه

<div class="b" id="bn1"><div class="m1"><p>تا نبی صدیق را محرم گرفت</p></div>
<div class="m2"><p>صبح صادق عرصه عالم گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبح صدق از مشرق عزت بتافت</p></div>
<div class="m2"><p>قاف تا قاف جهان عزت بیافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جملهٔ عالم ازو پر نور گشت</p></div>
<div class="m2"><p>چشم بد یا کور شد یا دور گشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صدق میبارد ز یک یک کار او</p></div>
<div class="m2"><p>گر ندانی بحث کن اسرار او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون نبی از خوان حی لایموت</p></div>
<div class="m2"><p>در محیط صدر او میریخت قوت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسته بودش هفت سقف دلفروز</p></div>
<div class="m2"><p>کو نخوردی قوت جز تا هفت روز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گاه مال و گاه جان میباخت او</p></div>
<div class="m2"><p>با رسول و با خدا میساخت او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مصطفا گفتا خداوند جلیل</p></div>
<div class="m2"><p>بود و خواهد بود جاویدم خلیل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر مرا بودی خلیلی جز احد</p></div>
<div class="m2"><p>آن ابوبکر منستی تا ابد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یک تجلی خلق را عام آمدست</p></div>
<div class="m2"><p>خاص آن او را ز انعام آمدست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرده گر میرود بر روی خاک</p></div>
<div class="m2"><p>هست از قول نبی صدیق پاک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون صفات نفس در وی مرده بود</p></div>
<div class="m2"><p>سر بصدق زندگی آورده بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>او بدین عالم نیفتاده ز خویش</p></div>
<div class="m2"><p>جان بدان عالم فرستاده ز پیش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جان او چون آن جهانی گشته بود</p></div>
<div class="m2"><p>غرق دریای معانی گشته بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن جهانی داشت جان تا بود او</p></div>
<div class="m2"><p>برد هم جان همچنان تا بود او</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون در آن عالم بود جان یکی</p></div>
<div class="m2"><p>هرچه گوید صدق گوید بی شکی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>لاجرم پیوسته در تحقیق بود</p></div>
<div class="m2"><p>هم خلیفه بود و هم صدیق بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جان او چون زان جهان میگفت راز</p></div>
<div class="m2"><p>صدق او در در خلافت کرد باز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>فتنه کز خواب نبی بیدار شد</p></div>
<div class="m2"><p>او بتنهایی خود در کار شد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تانشاند از راه خویش آن فتنه را</p></div>
<div class="m2"><p>دست بگشاد و ببست آن رخنه را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر نبودی صدق ورای آن امام</p></div>
<div class="m2"><p>از مسلمانی نماندی بیش نام</p></div></div>