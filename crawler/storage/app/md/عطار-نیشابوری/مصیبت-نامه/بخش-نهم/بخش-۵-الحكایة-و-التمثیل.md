---
title: >-
    بخش ۵ - الحكایة و التمثیل
---
# بخش ۵ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>بود در غزنی امامی از کرام</p></div>
<div class="m2"><p>نام بودش میرهٔ عبدالسلام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون سخن گفتی امام نامدار</p></div>
<div class="m2"><p>خلق آنجا جمع گشتی بی شمار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کرا در شهر چیزی گم شدی</p></div>
<div class="m2"><p>روز مجلس پیش آن مردم شدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بانگ کردی آنچه گم کردی براه</p></div>
<div class="m2"><p>پس نشان جستی ز خلق آنجایگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز مجلس بود مردی سوگوار</p></div>
<div class="m2"><p>زانکه خر گم کرده بود آن بیقرار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر سر آن مردم مجلس نیوش</p></div>
<div class="m2"><p>مرد خر گم کرده آمد در خروش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کای مسلمانان خری باجل که یافت</p></div>
<div class="m2"><p>چه خر و چه اسب آن دلدل که یافت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون نداد آنجا کسی از خر نشان</p></div>
<div class="m2"><p>مرد شد بر خاک از آن غم خون فشان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن امام القصه حرف آغاز کرد</p></div>
<div class="m2"><p>دفتر عشاق از هم باز کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وصف عشق و عاشقان گفتن گرفت</p></div>
<div class="m2"><p>وز کمال عشق آشفتن گرفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پس چنین گفت او که ذرات جهان</p></div>
<div class="m2"><p>جمله در عشقند پیدا ونهان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در جهان کس بود کو عاشق نبود</p></div>
<div class="m2"><p>یا کمال عشق را لایق نبود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هست در مجلس کسی اینجایگاه</p></div>
<div class="m2"><p>کو بسر عشق کم بردست راه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>غافلی برخاست پنداشت آن سلیم</p></div>
<div class="m2"><p>کانکه عاشق نیست کاریست آن عظیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت اگر چه یافتم عمری تمام</p></div>
<div class="m2"><p>هرگزم عشقی نبودست ای امام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>میره گفت آن مرد خرگم کرده را</p></div>
<div class="m2"><p>روفساری آر و گیر این مرده را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کانچه تو در جستنش بشتافتی</p></div>
<div class="m2"><p>منت ایزد را که اینجا یافتی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرد را بی عشق کاری چون بود</p></div>
<div class="m2"><p>این چنین خر بی فساری چون بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر که عاشق نیست او را خر شمر</p></div>
<div class="m2"><p>خر بسی باشد ز خر کمتر شمر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عاشقی در چستی و چالاکیست</p></div>
<div class="m2"><p>هرکه عاشق نیست کرمی خاکیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عشق را گاهی نوازش باشدت</p></div>
<div class="m2"><p>گاه چون شمعی گدازش باشدت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا نخواهی دید در اول گداز</p></div>
<div class="m2"><p>نیست درآخر ترا ممکن نواز</p></div></div>