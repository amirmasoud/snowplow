---
title: >-
    بخش ۷ - الحكایة و التمثیل
---
# بخش ۷ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>بر پلی میشد نظام الملک شاد</p></div>
<div class="m2"><p>چشم او ناگه بزیر پل فتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیدلی در سایهٔ پل رفته بود</p></div>
<div class="m2"><p>فارغ از هر دو جهان خوش خفته بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت اگر عاقل اگر آشفتهٔ</p></div>
<div class="m2"><p>هرچه هستی فارغ و خوش خفتهٔ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیدل دیوانه گفتش ای نظام</p></div>
<div class="m2"><p>کی دو تیغ آید بهم در یک نیام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ملک دنیا هست دین میبایدت</p></div>
<div class="m2"><p>آن همه داری و این میبایدت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ترا دین باید از دنیا مناز</p></div>
<div class="m2"><p>هردو با هم راست ناید کژ مباز</p></div></div>