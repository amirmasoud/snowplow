---
title: >-
    بخش ۱۲ - الحكایة ‌و التمثیل
---
# بخش ۱۲ - الحكایة ‌و التمثیل

<div class="b" id="bn1"><div class="m1"><p>بر شره میخورد مجنونی طعام</p></div>
<div class="m2"><p>شکر حق میگفت شکری بردوام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کای خداوندی که جان و تن ز تست</p></div>
<div class="m2"><p>شکر تو از من طعام من ز تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو طعامم میفرستی ز اسمان</p></div>
<div class="m2"><p>شکر من بر میفرستم هر زمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میفرست اینجا فرو هر دم طعام</p></div>
<div class="m2"><p>تا منت بر میفرستم بر دوام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>واسطه این قوم را برخاستست</p></div>
<div class="m2"><p>قول ایشان لاجرم بس راستست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون نمیبینند غیری جز مجاز</p></div>
<div class="m2"><p>جمله زو شنوند و زو گویند باز</p></div></div>