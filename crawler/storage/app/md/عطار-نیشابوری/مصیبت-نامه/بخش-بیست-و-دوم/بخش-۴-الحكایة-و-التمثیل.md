---
title: >-
    بخش ۴ - الحكایة ‌و التمثیل
---
# بخش ۴ - الحكایة ‌و التمثیل

<div class="b" id="bn1"><div class="m1"><p>بود آن دیوانهٔ در اضطرار</p></div>
<div class="m2"><p>در مناجاتی شبی میگفت زار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کای خدا از تو نخواهم هیچ من</p></div>
<div class="m2"><p>یا دهی یا ندهیم بشنو سخن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سخت در خود ماندهام جان در خطر</p></div>
<div class="m2"><p>تا که از من این چه دادی واببر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این وجودم را که داری در زحیر</p></div>
<div class="m2"><p>مینخواهم هیچ میگویم بگیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرچه از دیوانه آید در وجود</p></div>
<div class="m2"><p>عفو فرمایند از دیوان جود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرچه نبود نیک بپذیرند ازو</p></div>
<div class="m2"><p>پس بچیزی نیک بر گیرند ازو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر بد او را مراعاتی کنند</p></div>
<div class="m2"><p>از نکو وجهی مکافاتی کنند</p></div></div>