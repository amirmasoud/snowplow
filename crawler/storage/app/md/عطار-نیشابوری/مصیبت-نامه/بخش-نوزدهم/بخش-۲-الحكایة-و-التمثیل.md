---
title: >-
    بخش ۲ - الحكایة و التمثیل
---
# بخش ۲ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>طالبی مطلوب را گم کرده بود</p></div>
<div class="m2"><p>روز و شب سر در جهان آورده بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از غم جان وجهان بفریفته</p></div>
<div class="m2"><p>در جهان میرفت جانی شیفته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پای از سر در طلب نشناخت او</p></div>
<div class="m2"><p>خویش را نعلین آهن ساخت او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس جهان صدباره چون پیموده کرد</p></div>
<div class="m2"><p>ای عجب نعلین آهن سوده کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ذره ذره گشت در راهی دراز</p></div>
<div class="m2"><p>آهن نعلین او بی دلنواز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر چه بسیاری بگشت از درد او</p></div>
<div class="m2"><p>هم نیافت از هیچ راهی گرد او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاقبت در پیش او آمد سه راه</p></div>
<div class="m2"><p>بر سر هر راه او خطی سیاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر سر یک ره نوشته کای غلام</p></div>
<div class="m2"><p>گر فرو آئی بدین ره تو تمام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرچه این راهیست دشوار و دراز</p></div>
<div class="m2"><p>هم برآئی عاقبت زین راه باز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر ره دیگر نبشته کای سلیم</p></div>
<div class="m2"><p>گر فرو آئی بدین راه عظیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یا برآئی زین ره آخر ناگهان</p></div>
<div class="m2"><p>یا ازین جابرنیائی جاودان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر سیم بنبشته بدکای مرد پاک</p></div>
<div class="m2"><p>گر فرود آئی بدین راه هلاک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برنیائی تا ابد هرگز دگر</p></div>
<div class="m2"><p>نه نشان از تو بماند نه خبر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>محو گردی گم شوی ناچیز هم</p></div>
<div class="m2"><p>زین چه فانی تر بود آننیز هم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت چون در وصال اومید نیست</p></div>
<div class="m2"><p>کار جز نومیدی جاوید نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>این سیم راهست راه من مدام</p></div>
<div class="m2"><p>این بگفت و شد در آن ره والسلام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>راه اول در شریعت رفتن است</p></div>
<div class="m2"><p>در عبادت بی طبیعت رفتن است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پس دوم راهت طریقت آمدست</p></div>
<div class="m2"><p>ور سیم خواهی حقیقت آمدست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در حقیقت گر قدم خواهی زدن</p></div>
<div class="m2"><p>محو گردی تا که دم خواهی زدن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر که در راه حقیقت زد دو گام</p></div>
<div class="m2"><p>تا ابد نابود گردد والسلام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گام اول را زخود مطلق شود</p></div>
<div class="m2"><p>پس بدیگر گام محو حق شود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر کرا زانجایگه بوئی بود</p></div>
<div class="m2"><p>در نگنجد گر همه موئی بود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p> </p></div>
<div class="m2"><p></p></div></div>