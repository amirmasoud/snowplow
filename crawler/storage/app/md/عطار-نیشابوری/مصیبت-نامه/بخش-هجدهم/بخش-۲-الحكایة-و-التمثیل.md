---
title: >-
    بخش ۲ - الحكایة و التمثیل
---
# بخش ۲ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>بود عبداللّه طاهر در شکار</p></div>
<div class="m2"><p>باز میآمد بشهر آن نامدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود در راهش پلی جای نشست</p></div>
<div class="m2"><p>پیر زالی از پس آن پل بجست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اسب عبداللّه سر بر زد ز راه</p></div>
<div class="m2"><p>بر زمین افکند از فرقش کلاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خشمگین شد سخت عبداللّه ازو</p></div>
<div class="m2"><p>خواست تا خود را کند آگاه ازو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت ای نادان چکارت اوفتاد</p></div>
<div class="m2"><p>کاین چنین جای اختیارت اوفتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قصهٔ دادش بدست آن پیرزن</p></div>
<div class="m2"><p>گفت فرزندیست بی جرم آن من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مانده در زندان تو خوار و اسیر</p></div>
<div class="m2"><p>لطف کن او را برون آر ای امیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میبسوزد جان من از درد او</p></div>
<div class="m2"><p>شد سیه روزم ز روی زرد او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیرم و رفته بآخر روز من</p></div>
<div class="m2"><p>رحمتی کن بر دل پر سوز من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خورد سوگند از سر خشم آن امیر</p></div>
<div class="m2"><p>کان پسر در حبس خواهد مرد اسیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برنیارم من ز زندان هرگزش</p></div>
<div class="m2"><p>همچنان میدارم آنجا عاجزش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پیره زن گفت ای امیر کاردان</p></div>
<div class="m2"><p>نیست بر کاری خداوند جهان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر تو بر کاری خدا هم نیز هست</p></div>
<div class="m2"><p>قادر و دانندهٔ هر چیز هست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من کنون با او گذارم کار خویش</p></div>
<div class="m2"><p>توبرو من نیز بردم بار خویش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا درچون حق جهانداری بود</p></div>
<div class="m2"><p>بر در تو آمدن عاری بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تن زدم جان سوخته رفتم ز جای</p></div>
<div class="m2"><p>تا تو بهتر آئی اکنون با خدای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>این سخن بر جان عبداللّه زد</p></div>
<div class="m2"><p>اشک خونین بر غبار راه زد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خورد سوگندی دگر آن مهربان</p></div>
<div class="m2"><p>کز سر پل نگذرم من این زمان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا نیارند آن پسر را سوی من</p></div>
<div class="m2"><p>تا ببینم روی او او روی من</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شد بزندان مرد و آوردش سوار</p></div>
<div class="m2"><p>چون جمال او بدید آن نامدار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خلعتش بخشید وگفت آن سرفراز</p></div>
<div class="m2"><p>تا بگردانند در شهرش بناز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پس منادی میکنند از چپ و راست</p></div>
<div class="m2"><p>کاین طلیق اللّه آزاد خداست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>این چنین کاری که کوهی کاه کرد</p></div>
<div class="m2"><p>رغم عبداللّه را اللّه کرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر تحمل هست نیکو از یکی</p></div>
<div class="m2"><p>هست نیکوتر ز شاهان بیشکی</p></div></div>