---
title: >-
    بخش ۶ - الحكایة و التمثیل
---
# بخش ۶ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>بود کشتی گیر برنائی چو ماه</p></div>
<div class="m2"><p>سرکشان را سرنگون کردی براه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاقبت از گردش لیل و نهار</p></div>
<div class="m2"><p>شد ز یک مویش سپیدی آشکار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>موی را بر کند و بر دستش نهاد</p></div>
<div class="m2"><p>پس سرشک از چشم خون افشان گشاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت در کشتی چو سرافراختم</p></div>
<div class="m2"><p>سرکشان را سرنگون انداختم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای عجب این موی سرافراختست</p></div>
<div class="m2"><p>سرنگونم بر زمین انداختست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با همه مردان بکوشم وقت کار</p></div>
<div class="m2"><p>نیستم در پیش موئی پایدار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساختستم با بتر در صبح و شام</p></div>
<div class="m2"><p>وز بتر بهتر همی جویم مدام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با بتر تا چند خواهی ساخت تو</p></div>
<div class="m2"><p>در بتر بهتر چه خواهی باخت تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهترین چیزی که عمرست آن دراز</p></div>
<div class="m2"><p>در بتر چیزی که دنیاست آن مباز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای بیک جو بهر دنیا جان فروش</p></div>
<div class="m2"><p>بوده یوسف را چنین ارزان فروش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون تو یوسف را بجان نخریدهٔ</p></div>
<div class="m2"><p>لاجرم او را بجان نگزیدهٔ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یوسف جان را کسی سلطان کند</p></div>
<div class="m2"><p>کو خریداری او از جان کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یوسف جانت عزیزست ای پسر</p></div>
<div class="m2"><p>بهترت از وی چه چیزست ای پسر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قدر یوسف کور نتواند شناخت</p></div>
<div class="m2"><p>جز دلی پر شور نتواند شناخت</p></div></div>