---
title: >-
    بخش ۱۰ - الحكایة و التمثیل
---
# بخش ۱۰ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>کرد مجنونی بگورستان نشست</p></div>
<div class="m2"><p>مردهٔ را سردرآورده بدست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>موی از آن سر پاک برمیکند زود</p></div>
<div class="m2"><p>در میان خاک میافکند زود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سایلی گفتش چه میجوئی ازین</p></div>
<div class="m2"><p>گفت ای غافل چرا گوئی چنین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مینگنجیدست این سر در جهان</p></div>
<div class="m2"><p>لیک موئی در نگنجد این زمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو گوئی کردهای گم پا و سر</p></div>
<div class="m2"><p>این چه سرگردانی است ای بیخبر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر کنار آی از همه کار جهان</p></div>
<div class="m2"><p>پیش از آن کت در ربایند از میان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هیچ را چون پایداری روی نیست</p></div>
<div class="m2"><p>دشمنی و دوستداری روی نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گوئیا آس فلک سود و نسود</p></div>
<div class="m2"><p>هرچه هست ای جان من بود و نبود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روی را چون نیست روی اینجا بدن</p></div>
<div class="m2"><p>فرق نبود زشت یا زیبا بدن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>موی را چون نیست در بودن امید</p></div>
<div class="m2"><p>پس کنون خواهی سیه خواهی سپید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر کسی آمد ببالا بازگشت</p></div>
<div class="m2"><p>قطرهٔ دان کو بدریا بازگشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غم مخور گر خنده زد برقی و مرد</p></div>
<div class="m2"><p>شبنمی افتاد در غرق و بمرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کار و بار عالم حس هیچ نیست</p></div>
<div class="m2"><p>تاتوان کوشید زر مس هیچ نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زندگی عالم حس عالمی</p></div>
<div class="m2"><p>هست در جنب حقیقت یک دمی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هرچه آن یک لحظه باشد خوب و زشت</p></div>
<div class="m2"><p>من نخواهم گر همه باشد بهشت</p></div></div>