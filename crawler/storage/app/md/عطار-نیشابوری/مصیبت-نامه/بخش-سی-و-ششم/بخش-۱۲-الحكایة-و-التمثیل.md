---
title: >-
    بخش ۱۲ - الحكایة و التمثیل
---
# بخش ۱۲ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>خسروی میرفت در صحرا و شخ</p></div>
<div class="m2"><p>با سپاهی در عدد مور و ملخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جملهٔ صحرا غبار و گرد بود</p></div>
<div class="m2"><p>بانگ پیل و کوس و بردا برد بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود بر ره شاه را ویرانهٔ</p></div>
<div class="m2"><p>خفته بر دیوار آن دیوانهٔ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاه چون پیش آمدش او برنخاست</p></div>
<div class="m2"><p>همچنان میبود کرده پای راست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاه گفتش ای گدای خاک راه</p></div>
<div class="m2"><p>تو چرا حرمت نمیداری نگاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاه میبینی و لشکر پیش و پس</p></div>
<div class="m2"><p>برنخیزد چون منی را چون تو کس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیش شه دیوانهٔ آزادوش</p></div>
<div class="m2"><p>همچنان خفته زبان بگشاد خوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت آخر از چه دارم حرمتت</p></div>
<div class="m2"><p>یا کجا در چشمم آید نعمتت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر بقارونی برون خواهی شدن</p></div>
<div class="m2"><p>همچو قارون سرنگون خواهی شدن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ور چو نمرودی تو از ملک و سپاه</p></div>
<div class="m2"><p>همچو او گردی بیک پشه تباه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ور نکو روئیست در غایت ترا</p></div>
<div class="m2"><p>کافری باشی ز ترکان ختا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ور ترا علمست و با آن کار نیست</p></div>
<div class="m2"><p>از تو تا ابلیس ره بسیار نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ور تو همچون صاحب عادی بزور</p></div>
<div class="m2"><p>سردهد چون عوج یک سنگت بگور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ور بهشت آمد سرایت خشت خشت</p></div>
<div class="m2"><p>همچو شدادت کشند اندر بهشت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ور نداری این همه عیب و بدی</p></div>
<div class="m2"><p>پس چو هم باشیم هر دو در خودی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر دو از یک آب در خون آمدیم</p></div>
<div class="m2"><p>هر دو از یک راه بیرون آمدیم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر دو از یک زاد بر پائیم ما</p></div>
<div class="m2"><p>هر دو از یک باد برجائیم ما</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر دو در یک گز زمین افتادهایم</p></div>
<div class="m2"><p>هر دو اندر یک کمین افتادهایم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر دو از یک مرگ خیره میشویم</p></div>
<div class="m2"><p>هر دو با یک خاک تیره میشویم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در همه نوعی چو باتو همدمم</p></div>
<div class="m2"><p>من چرا برخیزم ازتو چه کمم</p></div></div>