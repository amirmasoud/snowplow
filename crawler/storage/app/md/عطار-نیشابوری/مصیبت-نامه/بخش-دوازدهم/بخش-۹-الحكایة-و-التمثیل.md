---
title: >-
    بخش ۹ - الحكایة و التمثیل
---
# بخش ۹ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>داشت اندر خانه اسحق ندیم</p></div>
<div class="m2"><p>بندهٔ در خدمت او مستقیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دایماً هر روز پیش از آفتاب</p></div>
<div class="m2"><p>میکشیدی تا بشب از دجله آب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون نمیشد تشنگی و آب کم</p></div>
<div class="m2"><p>مینزد یک دم غلام از کار دم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دید روزی خواجه او را بیقرار</p></div>
<div class="m2"><p>فارغ از خلق و شده مشغول کار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواجه گفتش کیف عیشک ای غلام</p></div>
<div class="m2"><p>گفت کاری سخت دارم بر دوام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در میان دو بلا افتادهام</p></div>
<div class="m2"><p>سرنگون در زیر پا افتادهام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هست از یک سویم آبی بی قیاس</p></div>
<div class="m2"><p>وز دگر سو تشنگان ناسپاس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دجله را خالی بکردن روی نیست</p></div>
<div class="m2"><p>تشنه را سیری سر یک موی نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در میان دجله و تشنه مدام</p></div>
<div class="m2"><p>ماندهام درآمد و شد والسلام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در میان دین و دنیا ماندهام</p></div>
<div class="m2"><p>گه بمعنی گه بدعوا ماندهام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه ز دینم میرسد بوئی تمام</p></div>
<div class="m2"><p>نه دمی دنیام میگیرد نظام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من نه این نه آن ز راه افتاده باز</p></div>
<div class="m2"><p>خردغل باری گران راهی دراز</p></div></div>