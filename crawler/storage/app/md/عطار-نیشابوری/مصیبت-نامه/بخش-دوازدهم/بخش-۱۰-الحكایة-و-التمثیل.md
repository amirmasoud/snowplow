---
title: >-
    بخش ۱۰ - الحكایة و التمثیل
---
# بخش ۱۰ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>یک کلیچه یافت آن سگ در رهی</p></div>
<div class="m2"><p>ماه دید از سوی دیگر ناگهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن کلیچه بر زمین افکند سگ</p></div>
<div class="m2"><p>تا بگیرد ماه بر گردون بتک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون بسی تک زد ندادش دست ماه</p></div>
<div class="m2"><p>باز پس گردید و با زآمد براه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن کلیچه جست بسیاری نیافت</p></div>
<div class="m2"><p>بار دیگر رفت و سوی مه شتافت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه کلیچه دست میدادش نه ماه</p></div>
<div class="m2"><p>از سر ره میشد او تا پای راه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در میان راه حیران مانده</p></div>
<div class="m2"><p>گم شده نه این ونه آن مانده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا چنین دردی نیاید در دلت</p></div>
<div class="m2"><p>زندگی هرگز نگردد حاصلت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درد میباید ترادر هر دمی</p></div>
<div class="m2"><p>اندکی نه عالمی در عالمی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا مگر این درد ره پیشت برد</p></div>
<div class="m2"><p>از وجود خویش بی خویشتن برد</p></div></div>