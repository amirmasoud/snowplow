---
title: >-
    بخش ۸ - الحكایة و التمثیل
---
# بخش ۸ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>صوفئی را گفت مردی از رجال</p></div>
<div class="m2"><p>کای جهان گردیده چون داری تو حال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت سی سال ای اخی بشتافتم</p></div>
<div class="m2"><p>نه جوی زر دیدم و نه یافتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وی عجب کردم من این ساعت نشست</p></div>
<div class="m2"><p>تا مرا صد گنج زر آید بدست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه در عمری جوی هرگز نیافت</p></div>
<div class="m2"><p>دور نبود گر ز گنجی عز نیافت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست رویت یک جو زر یافتن</p></div>
<div class="m2"><p>چون توانی گنج گوهر یافتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکه او را هیچ در ده راه نیست</p></div>
<div class="m2"><p>مه دهی گر جوید او آگاه نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر همه شب روز میباید ترا</p></div>
<div class="m2"><p>درد درمان سوز میباید ترا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من که درد عشق در جان منست</p></div>
<div class="m2"><p>وی عجب این درد درمان منست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مینیابم آنچه میجویم همی</p></div>
<div class="m2"><p>وین طلب ساکن نمیگردد دمی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در میان این و آن درماندهام</p></div>
<div class="m2"><p>تا که جان دارم بجان درماندهام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هست دریای محبت بی کنار</p></div>
<div class="m2"><p>لاجرم یک تشنگی شد صد هزار</p></div></div>