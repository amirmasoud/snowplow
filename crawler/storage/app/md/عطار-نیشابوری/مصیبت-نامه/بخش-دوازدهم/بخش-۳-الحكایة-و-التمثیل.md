---
title: >-
    بخش ۳ - الحكایة و التمثیل
---
# بخش ۳ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>شد بر دیوانهٔ آن مرد پاک</p></div>
<div class="m2"><p>دید او را در میان خون و خاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو مستی واله و حیرانش دید</p></div>
<div class="m2"><p>سرنگونش یافت و سرگردانش دید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت ای دیوانهٔ بی روی و راه</p></div>
<div class="m2"><p>در چه کاری روز و شب اینجایگاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت هستم حق طلب در روز وشب</p></div>
<div class="m2"><p>مرد گفتش من همین دارم طلب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرد مجنون گفت پس پنجاه سال</p></div>
<div class="m2"><p>همچو من در خون نشین در کل حال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کاسهٔ پرخون تو میخور ای عزیز</p></div>
<div class="m2"><p>بعد از آن می ده بمن یک کاسه نیز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تاکه این دریا شود پرداخته</p></div>
<div class="m2"><p>یا نه کار ما شود برساخته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این گره را چون گشادن روی نیست</p></div>
<div class="m2"><p>هم بمردن هم بزادن روی نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این قدر دانم که با این پیچ پیچ</p></div>
<div class="m2"><p>می ندانم می ندانم هیچ هیچ</p></div></div>