---
title: >-
    بخش ۱۱ - الحكایة و التمثیل
---
# بخش ۱۱ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>طالبی را کو طلب می‌کرد راز</p></div>
<div class="m2"><p>گفت یک روزی اویس پاکباز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی آن دارد که تو در راه بیم</p></div>
<div class="m2"><p>تا که جان داری چنان باشی مقیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کاین همه خلق جهان را آشکار</p></div>
<div class="m2"><p>گوئیا تو کشتهٔ از درد کار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا نباشد این چنین دردی ترا</p></div>
<div class="m2"><p>ننگ باشد خواندن مردی ترا</p></div></div>