---
title: >-
    بخش ۲ - الحكایة و التمثیل
---
# بخش ۲ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>مصطفی چون آمد از معراج در</p></div>
<div class="m2"><p>وام میخواست از جهودی جومگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از برای قوت جو میخواستش</p></div>
<div class="m2"><p>وان جهود سگ گرو میخواستش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر دو عالم دید آن شب ارزنی</p></div>
<div class="m2"><p>روز دیگر جو نبودش یک منی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لاجرم چون این و آن یکسانش بود</p></div>
<div class="m2"><p>هر دو عالم زیر یک فرمانش بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ضعف ایمان باشدت ای ناتوان</p></div>
<div class="m2"><p>تو چه دانی سر فقر شب روان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان آدم نیز سر فقر سوخت</p></div>
<div class="m2"><p>هشت جنت را بیک گندم فروخت</p></div></div>