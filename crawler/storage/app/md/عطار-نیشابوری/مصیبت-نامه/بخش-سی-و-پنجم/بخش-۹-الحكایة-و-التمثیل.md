---
title: >-
    بخش ۹ - الحكایة و التمثیل
---
# بخش ۹ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>در میان جمع یک صاحب کمال</p></div>
<div class="m2"><p>کرد محی الدین یحیی را سؤال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کان همه منصب که پیدا ونهان</p></div>
<div class="m2"><p>مصطفی را بود در هر دوجهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از چه گفت او کاشکی از بحر جود</p></div>
<div class="m2"><p>حق نیاوردی مرا اندر وجود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه جمله از برای او بود</p></div>
<div class="m2"><p>هر دوعالم خاک پای او بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این چرا گوید چه حکمت دانی این</p></div>
<div class="m2"><p>شرح ده چندان که می بتوانی این</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت دو لوری بچه مرد و زنی</p></div>
<div class="m2"><p>کرده در خرگه بصحرا مسکنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بود دو خرگه برابر هر دو را</p></div>
<div class="m2"><p>وصل یکدیگر میسر هر دو را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر دو در خوبی کمالی داشتند</p></div>
<div class="m2"><p>هم ملاحت هم جمالی داشتند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر دو مست روی یکدیگر شدند</p></div>
<div class="m2"><p>صید شست موی یکدیگر شدند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روز و شب در عشق هم میسوختند</p></div>
<div class="m2"><p>سال و مه سر تا قدم میسوختند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر جمال یکدیگر میزیستند</p></div>
<div class="m2"><p>دایماً در هم همی نگریستند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یک دم از همشان شکیبائی نبود</p></div>
<div class="m2"><p>زانکه عشق هر دو هر جائی نبود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عاقبت آن هر دو را از روزگار</p></div>
<div class="m2"><p>گوسفند و گاو شد بیش از شمار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کار و بار هر دو تن بسیار شد</p></div>
<div class="m2"><p>هر دو خرگه جای گیر و دار شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون زیادت گشت هر ساعت مقام</p></div>
<div class="m2"><p>بیشتر شد هر زمان خیل وغلام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آمدند از دشت سوی شهر باز</p></div>
<div class="m2"><p>شد میسرشان دو قصر سرفراز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پرده دار و حاجبان بنشاندند</p></div>
<div class="m2"><p>پادشاهی جهان میراندند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کار هر دو در گذشت از آسمان</p></div>
<div class="m2"><p>زانکه بود آن در ترقی هر زمان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زین سبب آن هر دو مرغ دلنواز</p></div>
<div class="m2"><p>اوفتادند از بر هم دور باز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر دو را از کار و بار و گیر دار</p></div>
<div class="m2"><p>وصل رفت و هجر آمد آشکار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در میان هر دو راهی دور ماند</p></div>
<div class="m2"><p>این ازان و آن ازین مهجور ماند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در فراق یکدگر میسوختند</p></div>
<div class="m2"><p>هر دم از نوع دگر میسوختند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هیچکس از دردشان آگه نبود</p></div>
<div class="m2"><p>هیچ سوی یکدگرشان ره نبود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر دو مشتاق گدائی آمدند</p></div>
<div class="m2"><p>دشمن آن پادشائی آمدند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>درگدائی هر دوچون شیر و شکر</p></div>
<div class="m2"><p>تازه و خوش میشدند از یکدگر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>لیک چون منشور شاهی خواندند</p></div>
<div class="m2"><p>از سپیدی در سیاهی ماندند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در گدائیشان بسی به بود کار</p></div>
<div class="m2"><p>پادشاهیشان نیامد سازگار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در گدائی عشق با هم باختند</p></div>
<div class="m2"><p>در شهی با هم نمیپرداختند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عاقبت از گردش لیل ونهار</p></div>
<div class="m2"><p>هر دو تن را کرد مفلس روزگار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پادشاهی رفت وآن بیشی نماند</p></div>
<div class="m2"><p>حاصلی جز نقد درویشی نماند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شهر را بیخویشتن بگذاشتند</p></div>
<div class="m2"><p>راه صحرا هر دو تن برداشتند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هر دوچون محروم و مسکین آمدند</p></div>
<div class="m2"><p>با سر جای نخستین آمدند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همچو اول بار دو خرگه تمام</p></div>
<div class="m2"><p>برکشیدند آن دو تن در یک مقام</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بار دیگر هر دو دلبر بی تعب</p></div>
<div class="m2"><p>در برابر اوفتادند ای عجب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هر دو از سر باز در هم گم شدند</p></div>
<div class="m2"><p>وز همه عالم بیک دم گم شدند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نقد وصل وگنج جان برداشتند</p></div>
<div class="m2"><p>زحمت هجر از میان برداشتند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هر زمان ذوقی دگرگون یافتند</p></div>
<div class="m2"><p>هر نفس صد لذت افزون یافتند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>برگشادند آن دو مرغ آنجا زفان</p></div>
<div class="m2"><p>شکرها گفتند حق را هر زمان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کز شهی با این گدائی آمدیم</p></div>
<div class="m2"><p>با سر این آشنائی آمدیم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>پادشاهی دام ما افتاده بود</p></div>
<div class="m2"><p>تا دو مرغ از هم جدا افتاده بود</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خاک درویشی شدیم از جان پاک</p></div>
<div class="m2"><p>بر سر آن پادشاهی باد خاک</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کاش آن شاهی نبودی وان کمال</p></div>
<div class="m2"><p>تانبردی روزگار این وصال</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کاش بی کوس و علم می بودمی</p></div>
<div class="m2"><p>تا چنین دایم بهم میبودمی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گر همه عالم مسلم بودن است</p></div>
<div class="m2"><p>از همه مقصود با هم بودن است</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هر دو چون باهم رسیدیم این نفس</p></div>
<div class="m2"><p>فارغیم از جمله کار اینست و بس</p></div></div>