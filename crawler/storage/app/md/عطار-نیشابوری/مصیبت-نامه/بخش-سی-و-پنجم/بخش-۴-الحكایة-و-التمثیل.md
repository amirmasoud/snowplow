---
title: >-
    بخش ۴ - الحكایة و التمثیل
---
# بخش ۴ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>بایزید از خانه میآمد پگاه</p></div>
<div class="m2"><p>اوفتاد آنجا سگی با او براه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیخ حالی جامه را در هم گرفت</p></div>
<div class="m2"><p>زانکه سگ را سخت نامحرم گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سگ زفان حال بگشاد آن زمان</p></div>
<div class="m2"><p>گفت اگر خشکم مکش از من عنان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ورترم هفت آب و یک خاک ای سلیم</p></div>
<div class="m2"><p>صلح اندازد میان ما مقیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر تو را سهل است با من زان چه باک</p></div>
<div class="m2"><p>کار تو با تست کاری خوفناک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر بخود دامن زنی یک ذره باز</p></div>
<div class="m2"><p>پس ز صد دریا کنی غسل نماز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زان جنابت هم نگردی هیچ پاک</p></div>
<div class="m2"><p>پاک میگردی ز من از آب و خاک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اینکه تو دامن ز من داری نگاه</p></div>
<div class="m2"><p>جهد کن کز خویشتن داری نگاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شیخ گفتش ظاهری داری پلید</p></div>
<div class="m2"><p>هست آن در باطن من ناپدید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عزم کن تا هر دو یک منزل کنیم</p></div>
<div class="m2"><p>بو کز آنجا پاکئی حاصل کنیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر دوجا آب نجس بر هم شود</p></div>
<div class="m2"><p>چون بدو قله رسد محرم شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همرهی کن ای بظاهر باطنم</p></div>
<div class="m2"><p>تا شود از پاکی دل ایمنم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سگ بدو گفت ای امام راهبر</p></div>
<div class="m2"><p>من نشایم همرهی را در گذر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زانکه من رد جهانم این زمان</p></div>
<div class="m2"><p>وانگهی هستی تو مقبول جهان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هرکرا بینم مرا کوبی رسد</p></div>
<div class="m2"><p>با لگد یا سنگ یا چوبی رسد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هرکرا بینی تو گردد خاک تو</p></div>
<div class="m2"><p>شکر گوید ز اعتقاد پاک تو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از پی فردای خود تا زادهام</p></div>
<div class="m2"><p>استخوانی خویش را ننهادهام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو مگر شکاک راه افتادهٔ</p></div>
<div class="m2"><p>لاجرم گندم دو خم بنهادهٔ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا بود گندم مگر فردات را</p></div>
<div class="m2"><p>سر نمیگردد چنین سودات را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شیخ کاین بشنود مشتی آه کرد</p></div>
<div class="m2"><p>روی و ره نه روی سوی راه کرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفت چون من مینشایم زابلهی</p></div>
<div class="m2"><p>تا کنم با یک سگ او همرهی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همرهی لایزال و لم یزل</p></div>
<div class="m2"><p>چون توانم کرد با چندین خلل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا که میماند من ومائی ترا</p></div>
<div class="m2"><p>روی نبود ایمنی جائی ترا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون ز ما و من برون آئی تمام</p></div>
<div class="m2"><p>هر دو عالم کل تو باشی والسلام</p></div></div>