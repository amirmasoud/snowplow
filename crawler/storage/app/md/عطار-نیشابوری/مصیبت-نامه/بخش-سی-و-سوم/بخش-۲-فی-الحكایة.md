---
title: >-
    بخش ۲ - فی الحكایة
---
# بخش ۲ - فی الحكایة

<div class="b" id="bn1"><div class="m1"><p>خواند داود پیامبر شست سال</p></div>
<div class="m2"><p>بر سر خلقان زبور ذوالجلال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای عجب آواز چون برداشتی</p></div>
<div class="m2"><p>عقل رابر جای خود نگذاشتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باد از رفتن باستادی خموش</p></div>
<div class="m2"><p>برگهای شاخ گشتی جمله گوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آب فارغ از دویدن آمدی</p></div>
<div class="m2"><p>مرغ معزول از پریدند آمدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه خوش آوازیش بسیار بود</p></div>
<div class="m2"><p>لیک از ماتم نبود از کار بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لاجرم یک آدمی نگریستی</p></div>
<div class="m2"><p>میشنودی خلق و خوش میزیستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاقبت چون ضربتی خورد از قدر</p></div>
<div class="m2"><p>شد دل و جانش همه زیر و زبر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نوحهٔ خود را بصحرا شد برون</p></div>
<div class="m2"><p>شد روان ازنوحهٔ او جوی خون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون شد آواز خوش او دردناک</p></div>
<div class="m2"><p>ای عجب شد چل هزار آنجا هلاک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرکه آن آواز بشنیدی ز دور</p></div>
<div class="m2"><p>گشتی اندر جان فشاندن ناصبور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا خطاب آمد که ای داود پاک</p></div>
<div class="m2"><p>آدمی شد چل هزار از تو هلاک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پیش ازین کس رانمیشد دیده تر</p></div>
<div class="m2"><p>این زمان بنگر که چون شد کارگر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لاجرم اکنون چو کارت اوفتاد</p></div>
<div class="m2"><p>آتشی در روزگارت اوفتاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نوحهٔ تو چون برفت ازدرد کار</p></div>
<div class="m2"><p>بر سر تو جان فشاندم چل هزار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بود آواز خوشت زین بیشتر</p></div>
<div class="m2"><p>نوحهٔ ماتم دگر باشد دگر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هرچه از دردی هویدا آید آن</p></div>
<div class="m2"><p>خلق کشتن را بصحرا آید آن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ما ز آدم درد دین میخواستیم</p></div>
<div class="m2"><p>تا جهانی را بدو آراستیم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>او چو مرد درد آمد در سرشت</p></div>
<div class="m2"><p>پاک شد از رنگ و از بوی بهشت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زن کند رنگی و بوئی اختیار</p></div>
<div class="m2"><p>مرد را با رنگ و با بوئی چکار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>لاجرم چون اهبطوش آمد خطاب</p></div>
<div class="m2"><p>پای تا سر درد آمد و اضطراب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هرکرا دل در مودت زنده شد</p></div>
<div class="m2"><p>در خصوصیت خدا را بنده شد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سر نه پیچید از ادب تا زنده بود</p></div>
<div class="m2"><p>لاجرم پیوسته سر افکنده بود</p></div></div>