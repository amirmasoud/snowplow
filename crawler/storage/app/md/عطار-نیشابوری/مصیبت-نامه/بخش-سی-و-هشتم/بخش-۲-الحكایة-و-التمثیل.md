---
title: >-
    بخش ۲ - الحكایة و التمثیل
---
# بخش ۲ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>چون سکندر با حکیم و با خفیر</p></div>
<div class="m2"><p>ماند اندر غار تاریکی اسیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچکس البته ره نشناخت باز</p></div>
<div class="m2"><p>جمله درماندند و شد کاری دراز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>متفق گشتند آخر سر بسر</p></div>
<div class="m2"><p>تاخری در پیش باشد راهبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش در کردند خر تا راه برد</p></div>
<div class="m2"><p>جمله را زانجا بلشگرگاه برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای عجب ایشان حیکمان جهان</p></div>
<div class="m2"><p>با خبر از سر پیدا و نهان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در چنان ره راهبرشان شد خری</p></div>
<div class="m2"><p>تا به حکمت لاف نزند دیگری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون نمود آن قوم را اسرار خویش</p></div>
<div class="m2"><p>گفت ای بی حاصلان کار خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرچه هر یک مرد پیش اندیش بود</p></div>
<div class="m2"><p>از شما باری خری در پیش بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون خری از عاقلان افزون بود</p></div>
<div class="m2"><p>دیگران را کاردانی چون بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عقل اگر جاهل بود جانت برد</p></div>
<div class="m2"><p>ور تکبر آرد ایمانت برد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عقل آن بهتر که فرمان بر شود</p></div>
<div class="m2"><p>ورنه گرکامل شود کافر شود</p></div></div>