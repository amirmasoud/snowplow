---
title: >-
    بخش ۱ - المقالة ‌الثامنة و الثلثون
---
# بخش ۱ - المقالة ‌الثامنة و الثلثون

<div class="b" id="bn1"><div class="m1"><p>سالک بگذشته از خیل خیال</p></div>
<div class="m2"><p>پیش عقل آمد بجسته از عقال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت ای دستور حل و عقد ملک</p></div>
<div class="m2"><p>نیست رایج بی تو هرگز نقد ملک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خرقهٔ‌تکلیف دین بر قد تست</p></div>
<div class="m2"><p>تا بحد نیستی سر حد تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ذرهٔ‌گر نیستی بگرفتئی</p></div>
<div class="m2"><p>ذرهٔ تکلیف نپذیرفتئی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اقبل و ادبر خطاب تست خاص</p></div>
<div class="m2"><p>گاه در قیدی و گاهی در خلاص</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون شود در نیستی چشم تو باز</p></div>
<div class="m2"><p>اقبلت گرداند از خود پاک باز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون شوی در عین هستی دیده ور</p></div>
<div class="m2"><p>ادبرت هر دم کند قیدی دگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرچه توداری ز نقصان و کمال</p></div>
<div class="m2"><p>حس ترا بخشیده از راه خیال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حس عدد آمد بصورت در عدد</p></div>
<div class="m2"><p>پس خیال آمد عدد اندر احد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو احد بودی عدد را معنوی</p></div>
<div class="m2"><p>کز زمان و از مکان دوری قوی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پنج مدرک را خیال از پنج بار</p></div>
<div class="m2"><p>کرد ادراک تو یکدم صد هزار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو همه در یک نفس دانندهٔ</p></div>
<div class="m2"><p>گرچه شاگردی ز خود خوانندهٔ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر چه حسن افتادت اول اوستاد</p></div>
<div class="m2"><p>زاوستادت کار برتر اوفتاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حس بمعنی در حقیقت از تو خاست</p></div>
<div class="m2"><p>لیک کارصورتت او کرد راست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون تو او را زنده کردی در صفت</p></div>
<div class="m2"><p>داد او در صورتت صد معرفت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون ترا در زنده کردن دست هست</p></div>
<div class="m2"><p>در دلم این مردگی پیوست هست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زندگی بخش و بمقصودم رسان</p></div>
<div class="m2"><p>در عبودیت بمعبودم رسان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عقل گفتش تو نداری عقل هیچ</p></div>
<div class="m2"><p>می نبینی این همه در عقل پیچ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کیش و دین از عقل آمد مختلف</p></div>
<div class="m2"><p>بر دراو چون توان شد معتکف</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>صد هزاران حجت آرد بی مجاز</p></div>
<div class="m2"><p>عالمی شبهت فرستد پیش باز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در تزلزل دایماً سرگشتهٔ</p></div>
<div class="m2"><p>در تردد طالب سر رشتهٔ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از وجود عقل خاست انکارها</p></div>
<div class="m2"><p>وز نمود عقل بود اقرارها</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عقل را گر هیچ بودی اتفاق</p></div>
<div class="m2"><p>چون دلستی پای تا سر اشتیاق</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عقل اندر حق شناسی کاملست</p></div>
<div class="m2"><p>لیک کاملتر ازو جان و دلست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر کمال عشق میباید ترا</p></div>
<div class="m2"><p>جز ز دل این پرده نگشاد ترا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سالک آمد پیش پیر نامور</p></div>
<div class="m2"><p>نامهٔ از کشف برخواندش زبر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پیر گفتش عقل از حق ترجمانست</p></div>
<div class="m2"><p>قاضی عدل زمین و آسمانست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نافذ آمد حکم او در کائنات</p></div>
<div class="m2"><p>هست حکم او کلید مشکلات</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بر درخت عقل هر شاخی که هست</p></div>
<div class="m2"><p>آفتاب آنجا نیارد برد دست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هرکه او از عقل لافی میزند</p></div>
<div class="m2"><p>از سر کذب و گزافی میزند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زانکه هر کس را که گردد عقل صاف</p></div>
<div class="m2"><p>در سرش نه کذب ماند نه گزاف</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کی تواند گشت مرد از قیل و قال</p></div>
<div class="m2"><p>در مقام عقل خود صاحب کمال</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سالها باید که تا یک نیکنام</p></div>
<div class="m2"><p>عقل را بی عقده گرداند تمام</p></div></div>