---
title: >-
    بخش ۵ - الحكایة و التمثیل
---
# بخش ۵ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>در بر دیوانهٔ شد عاقلی</p></div>
<div class="m2"><p>دید آن دیوانه را غمگین دلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت غمگین از کهٔ گفت از خدای</p></div>
<div class="m2"><p>کز غم او می ندانم سر ز پای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میبترسم زو و گر دیدن بود</p></div>
<div class="m2"><p>جمله را زو روی ترسیدن بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون نترسند از کسی خلقان همه</p></div>
<div class="m2"><p>کو چو گرگان را دهد سر در رمه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا شبان بنشیند و ماتم کند</p></div>
<div class="m2"><p>چه عجب گر از چنین کس غم کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کرد امروزم چنین شوریده دین</p></div>
<div class="m2"><p>تا چه خواهد کرد با من بعد ازین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای عجب دیوانه نیز از بیم او</p></div>
<div class="m2"><p>میکند چون عاقلان تسلیم او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیم او چون دل شکافی میکند</p></div>
<div class="m2"><p>عقل را از عقل صافی میکند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا زهیبت عقل مجنون میرود</p></div>
<div class="m2"><p>وز جنون خویش در خون میرود</p></div></div>