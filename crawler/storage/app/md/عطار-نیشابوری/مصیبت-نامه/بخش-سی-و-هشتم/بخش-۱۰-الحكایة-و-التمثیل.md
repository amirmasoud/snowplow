---
title: >-
    بخش ۱۰ - الحكایة و التمثیل
---
# بخش ۱۰ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>بامدادی شد بر سلطان ایاس</p></div>
<div class="m2"><p>خوبیش بیحد و ملحش بی قیاس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد شکن در گرد ماه افکنده بود</p></div>
<div class="m2"><p>هر شکن صد پادشاه افکنده بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاه را پیوسته رو با روی او</p></div>
<div class="m2"><p>حاجبی نزدیکتر ابروی او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاه درچشم سیاهش خیره بود</p></div>
<div class="m2"><p>ماه درجنب جمالش تیره بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر دو لعل او کلید مشکلات</p></div>
<div class="m2"><p>این چو آب کوثر آن آب حیات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آفتاب روی او از نیکوی</p></div>
<div class="m2"><p>شاهرا الحق بچشم آمد قوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت هان ای چشم من روشن ز تو</p></div>
<div class="m2"><p>تو ز من نیکوتری یا من ز تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت من نیکوترم ای شهریار</p></div>
<div class="m2"><p>پادشاهش گفت رو آئینه آر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت آئینه کژ آید بیشتر</p></div>
<div class="m2"><p>حکم کژ هرگز نباشد معتبر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت چون سازیم حکم این جمال</p></div>
<div class="m2"><p>گفت از آئینهٔ دل پرس حال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حکم دل بینندگان را جان فزود</p></div>
<div class="m2"><p>هرچه دل گوید بران نتوان فزود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شاه گفتش کز دل خود کن سؤال</p></div>
<div class="m2"><p>تامنم پیش ازتو یا تو در جمال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون برآمد ساعتی آنگه ایاس</p></div>
<div class="m2"><p>گفت من نیکوترم ای حق شناس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شاه گفت ای حاجت هر بیقرار</p></div>
<div class="m2"><p>این چه میگوید دلت حجت بیار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت چندانی که من در پیش شاه</p></div>
<div class="m2"><p>میکنم در بند بند خود نگاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>من نبینم هیچ جز سلطان مدام</p></div>
<div class="m2"><p>ذرهٔ از خود نمیبینم تمام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون همه شاه مظفر آمدم</p></div>
<div class="m2"><p>لاجرم بی شک نکوتر آمدم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در نکوئی کار تو دیگر بود</p></div>
<div class="m2"><p>عاقبت محمود نیکوتر بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر شود عالم سراسر پر غلام</p></div>
<div class="m2"><p>عاقبت محمود باید والسلام</p></div></div>