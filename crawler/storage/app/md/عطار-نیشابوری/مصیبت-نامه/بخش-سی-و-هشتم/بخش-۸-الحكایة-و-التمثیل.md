---
title: >-
    بخش ۸ - الحكایة و التمثیل
---
# بخش ۸ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>برزفان میراند یحیی بن المعاد</p></div>
<div class="m2"><p>کای خداوندان علم و اعتقاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قصرهاتان هست یکسر قیصری</p></div>
<div class="m2"><p>خانهاتان کسروی نه حیدری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جامهاتان جمله خاتونی شده</p></div>
<div class="m2"><p>مرکبانتان جمله قارونی شده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رویهاتان گشته ظلمانی همه</p></div>
<div class="m2"><p>خویهاتان جمله شیطانی همه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم عروسیهای فرعونی کنید</p></div>
<div class="m2"><p>ماتم گبرانه صد لونی کنید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم بعادتهای شدادی درید</p></div>
<div class="m2"><p>هم بکبر و نخوت عادی درید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این همه دارید و هم زین بیش نیز</p></div>
<div class="m2"><p>احمدی تان نیست آخر هیچ چیز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روز و شب مشغول رسم و کار و بار</p></div>
<div class="m2"><p>نیستتان با دین احمد هیچ کار</p></div></div>