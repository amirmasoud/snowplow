---
title: >-
    بخش ۴ - الحكایة و التمثیل
---
# بخش ۴ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>بود پیری عاجز و حیران شده</p></div>
<div class="m2"><p>سخت کوش چرخ سرگردان شده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست تنگی پایمالش کرده بود</p></div>
<div class="m2"><p>گرگ پیری در جوالش کرده بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود نالان همچو چنگی ز اضطراب</p></div>
<div class="m2"><p>پیشهٔ او از همه نقلی رباب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه یکی بانگ ربابش میخرید</p></div>
<div class="m2"><p>نه کسی نان ثوابش میخرید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرسنه مانده نه خوردی و نه خواب</p></div>
<div class="m2"><p>برهنه مانده نه نانی و نه آب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون نبودش هیچ روی از هیچ سوی</p></div>
<div class="m2"><p>برگرفت آخر رباب و شد بکوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مسجدی بود از همه نوعی خراب</p></div>
<div class="m2"><p>برفت آنجا و بزد لختی رباب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رخ بقبله زخمه را بر کار کرد</p></div>
<div class="m2"><p>پس سرودی نیز با آن یار کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون بزد لختی رباب آن بیقرار</p></div>
<div class="m2"><p>گفت یا رب من ندانم هیچ کار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این چه میدانستم آن آوردمت</p></div>
<div class="m2"><p>خوش سماعی با میان آوردمت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عاجزم پیرم ضعیفم بیکسم</p></div>
<div class="m2"><p>چون ندارم هیچ نان جان میبسم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه کسم میخواند از بهر رباب</p></div>
<div class="m2"><p>نه کسم نان میدهد بهر ثواب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من چو کردم آن خود بر تو نثار</p></div>
<div class="m2"><p>تو کریمی نیز آن خود بیار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در همه دنیا ندارم هیچ چیز</p></div>
<div class="m2"><p>رایگان مشنو سماع من تو نیز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کار من آماده کن یکبارگی</p></div>
<div class="m2"><p>تا رهائی یابم از غمخوارگی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون ز بس گفتن دلش در تاب شد</p></div>
<div class="m2"><p>هم دران مسجد خوشی درخواب شد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صوفیان بوسعید آن پیر راه</p></div>
<div class="m2"><p>گرسنه بودند جمله چند گاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چشم در ره تا فتوحی دررسد</p></div>
<div class="m2"><p>قوت تن قوت روحی در رسد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عاقبت مردی درآمد با خبر</p></div>
<div class="m2"><p>پیش شیخ آورد صد دینار زر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بوسه داد و گفت اصحاب تراست</p></div>
<div class="m2"><p>تا کنندامروز وجه سفره راست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شد دل اصحاب الحق خوش ازان</p></div>
<div class="m2"><p>رویشان بفروخت چون آتش ازان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شیخ آن زر داد خادم را و گفت</p></div>
<div class="m2"><p>در فلان مسجد یکی پیری بخفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>با ربابی زیر سر پیری نکوست</p></div>
<div class="m2"><p>این زر او را ده که این زر آن اوست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>رفت خادم برد زر درویش را</p></div>
<div class="m2"><p>گرسنه بگذاشت قوم خویش را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آن همه زر چون بدید آن پیر زار</p></div>
<div class="m2"><p>سر بخاک آورد و گفت ای کردگار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از کرم نیکو غنیمی میکنی</p></div>
<div class="m2"><p>با چو من خاکی کریمی میکنی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بعد از اینم گر نیاردمرگ خواب</p></div>
<div class="m2"><p>جمله از بهر تو خواهم زد رباب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>میشناسی قدر استادان تو نیک</p></div>
<div class="m2"><p>هیچکس مثل تو نشناسد ولیک</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون تو خود بستودهٔ چه ستایمت</p></div>
<div class="m2"><p>لیک چون زر برسدم بازآیمت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هرکرا در عقل نقصان اوفتد</p></div>
<div class="m2"><p>کار او فی الجمله آسان اوفتد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>لاجرم دیوانه را گرچه خطاست</p></div>
<div class="m2"><p>هرچه میگوید بگستاخی رواست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خیر و شر چون جمله زینجا میرود</p></div>
<div class="m2"><p>نوحهٔ دیوانه زیبا میرود</p></div></div>