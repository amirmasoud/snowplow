---
title: >-
    بخش ۱ - المقالة الخامسة و العشرون
---
# بخش ۱ - المقالة الخامسة و العشرون

<div class="b" id="bn1"><div class="m1"><p>سالک آمد پیش حیوان دردناک</p></div>
<div class="m2"><p>نه امید امن ونه بیم هلاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طالب اوحی شده دل پر شعاع</p></div>
<div class="m2"><p>سبع هشتم باز میجست از سباع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت ای جویندگان راهبر</p></div>
<div class="m2"><p>در رکوع استاده جمله کارگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از چرا و چند معزول آمده</p></div>
<div class="m2"><p>در چرای خویش مشغول آمده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در زمین گاو سیاهی از شماست</p></div>
<div class="m2"><p>زیر بار گاو ماهی از شماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر فلکتان گاو و ماهی نیز هست</p></div>
<div class="m2"><p>دب و شیر و هرچه خواهی نیز هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زیر و بالا سر بسر بگرفتهاید</p></div>
<div class="m2"><p>کوه و صحرا خشک و تر بگرفتهاید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گه شما را نیز ظاهر یک خلف</p></div>
<div class="m2"><p>ناقة اللّه بس بود در پیش صف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خود مپرسید از سگ اصحاب کهف</p></div>
<div class="m2"><p>زانکه جانی دارد او بر عشق وقف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از شما پیغامبری را اینت نام</p></div>
<div class="m2"><p>گوسفندی میشود قایم مقام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وز شما یک ماهی پاکیزه جای</p></div>
<div class="m2"><p>یونسی را میشود خلوت سرای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وز شما بزغالهٔ بریان بزهر</p></div>
<div class="m2"><p>میکند آگاه احمد را ز قهر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شاخ دولت از شما بر میدهد</p></div>
<div class="m2"><p>مشک آهو گاو عنبر میدهد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون کسی در راه دولت یار گشت</p></div>
<div class="m2"><p>دیگری را هم تواند یار گشت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هست روی دولت از سوی شما</p></div>
<div class="m2"><p>دولتی میخواهم از کوی شما</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون شنید این حال مشکل جانور</p></div>
<div class="m2"><p>شد زخود زین حال حالی بیخبر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفت ای هم بی خبر هم بی ادب</p></div>
<div class="m2"><p>کس ز گاو و خر گهر دارد طلب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ما همه دزد ره یکدیگریم</p></div>
<div class="m2"><p>یکدگر را میکشیم و میخوریم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سر بعالم در نهاده بیقرار</p></div>
<div class="m2"><p>نیست ما را جز خور و جز خفت کار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آنچه میجوئی تو اینجا آن مجوی</p></div>
<div class="m2"><p>گوهر دریائی از صحرا مجوی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>صد هزار از ما بمیرد زار بار</p></div>
<div class="m2"><p>تا شود یک ره براقی آشکار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر بلندی یافتست از ما کسی</p></div>
<div class="m2"><p>حکم نتوان کرد بر نادر بسی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از خر و از گاو نتوان یافت راز</p></div>
<div class="m2"><p>پس سر خود گیر زود ای سرفراز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سالک آمد پیش پیر بخردان</p></div>
<div class="m2"><p>قصهٔ بر گفتش از خیل ددان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پیر گفتش هست حیوان و سباع</p></div>
<div class="m2"><p>ز آتش نفس مجوسی یک شعاع</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نفس کافر سرکشی دارد مدام</p></div>
<div class="m2"><p>گر سر اندازیش سر بنهد تمام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گه مسلمانی دهی گه زر دهی</p></div>
<div class="m2"><p>تا که یک لقمه بدین کافر دهی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر طعام نفس خوش گر ناخوشست</p></div>
<div class="m2"><p>چون گذر بر نفس دارد آتشست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خوش مده نفس مجوسی را طعام</p></div>
<div class="m2"><p>تا نبینی ناخوشی او تمام</p></div></div>