---
title: >-
    بخش ۷ - الحكایة و التمثیل
---
# بخش ۷ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>مرگ را مردی بجان مشتاق شد</p></div>
<div class="m2"><p>پیش خواجه بوعلی دقاق شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت من از دست شیطان رجیم</p></div>
<div class="m2"><p>می ندارم ذرهٔ از مرگ بیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر دمم جان گوئیا شیطان برد</p></div>
<div class="m2"><p>مرگ نیکوتر بود گر جان برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواجه گفت ای چاره خواه نیکبخت</p></div>
<div class="m2"><p>در سرایت از میان بر کن درخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا برو گنجشگ ننشیند دگر</p></div>
<div class="m2"><p>بی درختت دیو کی بیند دگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا درونت آشیان دیو هست</p></div>
<div class="m2"><p>دایمت ازدیو سر کالیو هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون بسوزی آشیان دیو پاک</p></div>
<div class="m2"><p>دیو را با تو چه کار ای دردناک</p></div></div>