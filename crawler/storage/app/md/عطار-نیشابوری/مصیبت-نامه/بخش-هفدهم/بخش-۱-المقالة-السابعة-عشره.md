---
title: >-
    بخش ۱ - المقالة السابعة عشره
---
# بخش ۱ - المقالة السابعة عشره

<div class="b" id="bn1"><div class="m1"><p>سالک آمد پیش آب پاک رو</p></div>
<div class="m2"><p>گفت ای پاکیزهٔ چالاک رو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در جهان از تست یک یک هر چه هست</p></div>
<div class="m2"><p>وز تو بگشاید بلاشک هرچه هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکجا سر سبزئی آثار تست</p></div>
<div class="m2"><p>تازگی کردن طریق کار تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سلسبیل وکوثر و رضوان تراست</p></div>
<div class="m2"><p>زندگی چشمهٔ حیوان تراست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ره جانان خوش و تر میروی</p></div>
<div class="m2"><p>لاجرم هر لحظه خوشتر میروی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از کمال عشق جانان چون قلم</p></div>
<div class="m2"><p>سر نهی اول براه آنکه قدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم طهور دایم و هم طاهری</p></div>
<div class="m2"><p>جسم و جانی باطنی و ظاهری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در همه چیزی روانی همچو روح</p></div>
<div class="m2"><p>در دو عالم با سرافتاد از تو نوح</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر کرا آبیست آنکس پست تست</p></div>
<div class="m2"><p>کآبروی هرکه هست از دست تست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سخت تر زاهن نباشد تشنهٔ</p></div>
<div class="m2"><p>از تو گردد آب داده دشنهٔ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنکه آهن را چنین سیراب کرد</p></div>
<div class="m2"><p>هم تواند جان من بیتاب کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از در او آگهی ده یکدمم</p></div>
<div class="m2"><p>تا بود آن یکدمم صد عالمم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آب ازین چون آتشی در تاب شد</p></div>
<div class="m2"><p>آتشی برخاست زو وز آب شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت آخر من کیم تر دامنی</p></div>
<div class="m2"><p>از تر اندامی نه مردی نه زنی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دست شسته جملهٔ عالم ز من</p></div>
<div class="m2"><p>تر مزاجی بنی آدم ز من</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>میروم سر پا برهنه روز و شب</p></div>
<div class="m2"><p>میکنم پیوسته این معنی طلب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گه ز نومیدی چو نرمی میروم</p></div>
<div class="m2"><p>گاه از پندار گرمی میروم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گاه در صد گونه جوشم زین سبب</p></div>
<div class="m2"><p>گاه در بانگ و خروشم زین سبب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>من که سر تا بن همه اشکم ازین</p></div>
<div class="m2"><p>بی سر و بن زاتش رشکم ازین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مدتی رفتم بر امید بهی</p></div>
<div class="m2"><p>برنیامد کارم از آبی تهی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گوئیا دیدست مقصودم مرا</p></div>
<div class="m2"><p>لیک یکباری براه آسیا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر چو آتش گرم آیم در طلب</p></div>
<div class="m2"><p>گویدم بر ریگ رو ای بی ادب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>با چنین دردی ندیدم بوی او</p></div>
<div class="m2"><p>دیگری را چون برم ره سوی او</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سالک آمد پیش پیر دستگیر</p></div>
<div class="m2"><p>عرضه دادش گوهر درج ضمیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پیر گفتش آب پاک افتاده است</p></div>
<div class="m2"><p>کار او دایم طهارت دادنست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آب چون از اصل پاکی زاد بود</p></div>
<div class="m2"><p>عرش را بر آب ازان بنیاد بود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هرکه او در پاکی این ره بود</p></div>
<div class="m2"><p>جانش از پاکی حق آگه بود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو زنفس سگ پلید افتادهٔ</p></div>
<div class="m2"><p>در نجاست ناپدید افتادهٔ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نیست یک ساعت چو فرعونت شکست</p></div>
<div class="m2"><p>گر نداری مصر فرعونیت هست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو بفرعونی چو مصر جامعی</p></div>
<div class="m2"><p>یار فرعونی که هامان طالعی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عبد بطن و فرجیای مردار خوار</p></div>
<div class="m2"><p>جیفة اللیلی و بطال النهار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آن سگ دوزخ که تو بشنودهٔ</p></div>
<div class="m2"><p>در تو خفتست و تو خوش آسودهٔ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>این سگ دوزخ که آتش میخورد</p></div>
<div class="m2"><p>هرچه او را میدهی خوش میخورد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>باش تا فردا سگ نفس و منیت</p></div>
<div class="m2"><p>سر ز دوزخ برکند در دشمنیت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دشمن تست این سگ و از سگ بتر</p></div>
<div class="m2"><p>چند سگ را پروری ای بیخبر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نفس را قوت از پی دل ده مدام</p></div>
<div class="m2"><p>تا نگردد قوت تو بر تو حرام</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>قوت کی باشد حرامی گر خوری</p></div>
<div class="m2"><p>همچو مردان خور طعامی گر خوری</p></div></div>