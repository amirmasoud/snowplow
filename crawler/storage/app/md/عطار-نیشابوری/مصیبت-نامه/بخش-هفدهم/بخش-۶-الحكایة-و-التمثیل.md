---
title: >-
    بخش ۶ - الحكایة و التمثیل
---
# بخش ۶ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>شد بر فرعون ابلیس لعین</p></div>
<div class="m2"><p>یک کف پر ریگ برداشت از زمین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس نمود آن ریگ مروارید باز</p></div>
<div class="m2"><p>بعد از آنش ریگ گردانید باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت گیر این ریگ و گوهر کن تو نیز</p></div>
<div class="m2"><p>گفت ازین من میندانم هیچ چیز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس زفان بگشاد ابلیس لعین</p></div>
<div class="m2"><p>گفت تو با این سرو ریشی چنین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زشتم آید گر گدائی میکنی</p></div>
<div class="m2"><p>از چه دعوی خدائی میکنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر زمان ریشی مرصع بر نهی</p></div>
<div class="m2"><p>تخت خواهی تاج اقرع بر نهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با چنین ریشی چو گردی گرم تو</p></div>
<div class="m2"><p>اینت ریش آخر نداری شرم تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با چنین قدرت درین افکندگی</p></div>
<div class="m2"><p>می فرا نپذیردم در بندگی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون تو هم پیسی و هم کل تا بگوش</p></div>
<div class="m2"><p>در خدائی کی پذیرندت خموش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نفس کافر را که در هر ساعتش</p></div>
<div class="m2"><p>آزمایش میکنم در طاعتش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غرقهٔ بهر خطر میبینمش</p></div>
<div class="m2"><p>هر نفس از بد بتر میبینمش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنچه با من این سگ شوم آن کند</p></div>
<div class="m2"><p>کافرم گر کافر روم آن کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نیست چون من خویش دشمن هیچکس</p></div>
<div class="m2"><p>بیخبر تر کیست از من هیچکس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آنچه بر من میرود بر کس نرفت</p></div>
<div class="m2"><p>این سر افرازی هنوز از پس نرفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دولتم چون خشک میغی بود و بس</p></div>
<div class="m2"><p>حاصل از عمرم دریغی بود و بس</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تن که یک درد مرا مرهم نکرد</p></div>
<div class="m2"><p>همچو موئی گشت و موئی کم نکرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای دریغا جان بتن در باختیم</p></div>
<div class="m2"><p>قیمت جان ذرهٔ نشناختیم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تشنه میمیریم در طوفان همه</p></div>
<div class="m2"><p>وانک آب از چشمهٔ حیوان همه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هم زمان عیش را سوری نماند</p></div>
<div class="m2"><p>هم چراغ عمر را نوری نماند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>درد را مرهم کجا خواهیم کرد</p></div>
<div class="m2"><p>عمر شد ماتم کجاخواهیم کرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خون شد آهن زانکه این دردش بخاست</p></div>
<div class="m2"><p>دل که از خونست چون آهن چراست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا نگردی نقطهٔ درد ای پسر</p></div>
<div class="m2"><p>کی توان گفتن ترا مرد ای پسر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هرکه او در دیدهٔ خود خار نیست</p></div>
<div class="m2"><p>با گل غیب خدایش کار نیست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>میروی چون کافر درویش او</p></div>
<div class="m2"><p>کی توان شد این چنین در پیش او</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون زدین و دل تهی داری سرای</p></div>
<div class="m2"><p>چون روی بی دین و دل پیش خدای</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون ترا در خانه جای ماتمست</p></div>
<div class="m2"><p>در چنین جائی دلت چون خرمست</p></div></div>