---
title: >-
    بخش ۱ - المقالة ‌الثامنه
---
# بخش ۱ - المقالة ‌الثامنه

<div class="b" id="bn1"><div class="m1"><p>سالک آمد لوح را رهبر گرفت</p></div>
<div class="m2"><p>چون قلم سرگشته لوح از سرگرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لوح را گفت ای همه ریحان و روح</p></div>
<div class="m2"><p>نیست هم تلویح تو در هیچ لوح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قابلی آیات پر اسرار را</p></div>
<div class="m2"><p>حاملی الفاظ معنی دار را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقش بند حکم دیوان ازل</p></div>
<div class="m2"><p>جملهٔ نقاشی علم و عمل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا ابد پیرایهٔ ذات تو ساخت</p></div>
<div class="m2"><p>جملهٔ اسرار آیات تو ساخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرچه رفت و میرود در هر دو کون</p></div>
<div class="m2"><p>یک بیک پیداست بر تولون لون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جملهٔ احکام خوش میخوان توراست</p></div>
<div class="m2"><p>چون نخوانی چون خط خوشخوان تر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون محیطی جملهٔ‌اسرار را</p></div>
<div class="m2"><p>چارهٔ کاری کن این بیکار را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زانکه گر از لوح نگشاید درم</p></div>
<div class="m2"><p>چون قلم از غصه دربازم سرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زین سخن در گشت لوح و گفت خیز</p></div>
<div class="m2"><p>آبروی خویش و آن ما مریز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من چو اطفالم نشسته بی قرار</p></div>
<div class="m2"><p>بی خبر لوحی نهاده بر کنار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از قلم هر خط که بیرون اوفتاد</p></div>
<div class="m2"><p>من فرو خوانم ز بیم اوستاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر زمانی با دلی پررشک من</p></div>
<div class="m2"><p>میبشویم نقش لوح از اشک من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر کسی از لوح دیدی زندگی</p></div>
<div class="m2"><p>مرده را لوحیست در افکندگی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حکم سابق صد جهان درهم سرشت</p></div>
<div class="m2"><p>هر دمم زان نقش لوحی در نبشت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>لاجرم آن لوح میخوانم زبر</p></div>
<div class="m2"><p>هر زمانی لوح میگیرم ز سر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر دمم سوی دگر دامن کشند</p></div>
<div class="m2"><p>درخطم از بسکه خط در من کشند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>می فرو گیرند در حرفم تمام</p></div>
<div class="m2"><p>مینهند انگشت بر حرفم مدام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ماندهام حیران نه جان نه تن پدید</p></div>
<div class="m2"><p>تا چه نقش آید مرا از من پدید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>لوح بفکن ای چو کرسی سر فراز</p></div>
<div class="m2"><p>با دبیرستان نخواهی رفت باز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گرچه بسیاریست خط درشان من</p></div>
<div class="m2"><p>نیست خط عشق در دیوان من</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>درد من بین برفشان دامن برو</p></div>
<div class="m2"><p>خط بیزاری ستان از من برو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سالک آمد پیش پیر دردناک</p></div>
<div class="m2"><p>شرح دادش حال خود از جان پاک</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پیر گفتش لوح محفوظ اله</p></div>
<div class="m2"><p>عالم علمست و نقش پیشگاه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هرکجادر علم اسراری نهانست</p></div>
<div class="m2"><p>لوح رادر عکس او نقشی چنانست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نقش محنت هست و نقش دولتست</p></div>
<div class="m2"><p>هرچه هست آنجایگه بی علتست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کار بی علت از آنجا میرود</p></div>
<div class="m2"><p>محنت و دولت از آنجا میرود</p></div></div>