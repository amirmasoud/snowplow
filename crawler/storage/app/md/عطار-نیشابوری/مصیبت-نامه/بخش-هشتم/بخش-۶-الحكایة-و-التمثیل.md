---
title: >-
    بخش ۶ - الحكایة و التمثیل
---
# بخش ۶ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>ناگهی معشوق طوسی را مگر</p></div>
<div class="m2"><p>بود بر بازار عطاران گذر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن یکی عطار خوشتر از بهشت</p></div>
<div class="m2"><p>غالیه از مشک و عنبر می سرشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غالیه بستد ازو معشوق چست</p></div>
<div class="m2"><p>بود در پیشش خری ادبار و سست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زیر دنبال خر آلوده بکرد</p></div>
<div class="m2"><p>بر پلیدی غالیه سوده بکرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر این پرسید ازو مردی ز راه</p></div>
<div class="m2"><p>گفت این خلقی که هست اینجایگاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خدادارند چندانی خبر</p></div>
<div class="m2"><p>کز دم این غالیه این لاشه خر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از درخت ذات تو یک شاخ تر</p></div>
<div class="m2"><p>تا نپیوندد باصل کار در</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو بریده مانی از اصل همه</p></div>
<div class="m2"><p>فصل باشد قسمت از وصل همه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این زمان کن شاخ را پیوسته تو</p></div>
<div class="m2"><p>زانکه چون مردی بمانی بسته تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسته نتواند بلاشک کار کرد</p></div>
<div class="m2"><p>کار اینجا بایدت نهمار کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ور بدو پیوسته خواهی مرد تو</p></div>
<div class="m2"><p>زندگی پیوسته خواهی برد تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زندهٔ بی مرگ بسیاری بود</p></div>
<div class="m2"><p>گر بمیری زنده این کاری بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر در او چون توانی یافت بار</p></div>
<div class="m2"><p>چون ز بیکاری نه پردازی بکار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیستت پروای ریش خود دمی</p></div>
<div class="m2"><p>همچنین مردار خواهی شد همی</p></div></div>