---
title: >-
    بخش ۹ - الحكایة و التمثیل
---
# بخش ۹ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>گفت یک روزی سلیمان کای اله</p></div>
<div class="m2"><p>بهر من ابلیس را آور براه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چو هر دیوی شود فرمانبرم</p></div>
<div class="m2"><p>بی پری جفتی نهد سر در برم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حق بدو گفتا مشو او را شفیع</p></div>
<div class="m2"><p>تاکنم در حکم تو او را مطیع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاقبت ابلیس شد فرمان برش</p></div>
<div class="m2"><p>گشت چون باد ای عجب خاک درش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه چندانی سلیمان کار داشت</p></div>
<div class="m2"><p>کز زمین تا عرش گیر و دار داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مسکنت را قدر چون بشناخت او</p></div>
<div class="m2"><p>قوت از زنبیل بافی ساخت او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خادمش یک روز در بازار شد</p></div>
<div class="m2"><p>از پی زنبیل او در کار شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرچه بسیاری بگشت از پیش و پس</p></div>
<div class="m2"><p>عاقبت نخرید آن زنبیل کس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بازگشت و سوی او آورد باز</p></div>
<div class="m2"><p>شد گرسنگی سلیمان را دراز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روز دیگر دیگری بهتر ببافت</p></div>
<div class="m2"><p>تا خریداری تواند بو که یافت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برد خادم هر دو بازاری نبود</p></div>
<div class="m2"><p>تا بشب گشت و خریداری نبود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون نمیآمد خریداری پدید</p></div>
<div class="m2"><p>ضعف شد القصه بسیاری پدید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شد ز بی قوتی سلیمان دردناک</p></div>
<div class="m2"><p>آمدش بی قوتی در جان پاک</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حق تعالی گفتش آخر حال چیست</p></div>
<div class="m2"><p>کز ضعیفی بر تو دشوارست زیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت نان میبایدم ای کردگار</p></div>
<div class="m2"><p>گفت نان خور چند باشی بیقرار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفت یا رب نان ندارم در نگر</p></div>
<div class="m2"><p>گفت بفروش آن متاعت نان بخر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفت زنبیلم فرستادم بسی</p></div>
<div class="m2"><p>نیست این ساعت خریدارم کسی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفت کی زنبیل باید کار را</p></div>
<div class="m2"><p>بنده کرده مهتر بازار را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بی شکی شیطان چو محبوس آیدت</p></div>
<div class="m2"><p>کار دنیا جمله مدروس آیدت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چونبود در بند ابلیس پلید</p></div>
<div class="m2"><p>کی توان کردن فروشی یا خرید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کار دنیا جمله موقوف ویست</p></div>
<div class="m2"><p>نهی منکر امر معروف ویست</p></div></div>