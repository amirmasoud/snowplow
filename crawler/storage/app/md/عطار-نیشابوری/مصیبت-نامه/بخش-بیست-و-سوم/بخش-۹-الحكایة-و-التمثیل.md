---
title: >-
    بخش ۹ - الحكایة و التمثیل
---
# بخش ۹ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>دیر میآمد یکی از آب باز</p></div>
<div class="m2"><p>صوفیان کرده زفان در وی دراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوسعید مهنه گفت ای مردمان</p></div>
<div class="m2"><p>آب چون آرد فلانی این زمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زانکه آب خوش که آن روزی ماست</p></div>
<div class="m2"><p>درنیامد تا شدی این کار راست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون درآید برکشد آن آب مرد</p></div>
<div class="m2"><p>چون توان بیوقت هرگز آب خورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حکم او راست و نگهدار اوست بس</p></div>
<div class="m2"><p>در نگهداری نکوکار اوست بس</p></div></div>