---
title: >-
    بخش ۱۱ - الحكایة و التمثیل
---
# بخش ۱۱ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>گفت رکن الدین اکافی مگر</p></div>
<div class="m2"><p>می فشاند اندر سخن روزی گهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجلس او پارهٔ شوریده شد</p></div>
<div class="m2"><p>خواجه را آن از کسی پرسیده شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کاین چه افتادست وین شورش چراست</p></div>
<div class="m2"><p>ما نمیدانیم بر گوئید راست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن یکی گفتش فلان مرد نه خرد</p></div>
<div class="m2"><p>در نهان کفشی بدزدید و ببرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کفش ازو میبستدیم اینجایگاه</p></div>
<div class="m2"><p>شورشی برخاست زان گم کرده راه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواجه میگفتش مکن قصه دراز</p></div>
<div class="m2"><p>زانکه گر روزی خدای بی نیاز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برفکندی پردهٔ عصمت ز ما</p></div>
<div class="m2"><p>کفش دزد اولستی این گدا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کس چه داند تا چه حکمت میرود</p></div>
<div class="m2"><p>هر وجودی را چه قسمت میرود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خون صدیقان ازین حسرت بریخت</p></div>
<div class="m2"><p>واسمان بر فرق ایشان خاک بیخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرچه ره جستند هر سوئی ازین</p></div>
<div class="m2"><p>پی نبردند ای عجب موئی ازین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صد جهان حسرت بجان پاک در</p></div>
<div class="m2"><p>میتوان دیدن بزیر خاک در</p></div></div>