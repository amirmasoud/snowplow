---
title: >-
    بخش ۴ - الحكایة و التمثیل
---
# بخش ۴ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>باغبانی سه خیار آورد خرد</p></div>
<div class="m2"><p>تحفه را پیش نظام الملک برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورد یک نوباوه را حالی نظام</p></div>
<div class="m2"><p>پس دوم خورد و سوم هم شد تمام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بودش از هر سوی بسیار از کبار</p></div>
<div class="m2"><p>او نداد البته کس را زان خیار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باغبان راداد سی دینار زر</p></div>
<div class="m2"><p>مرد خدمت کرد و بیرون شد بدر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس زفان بگشاد در مجمع نظام</p></div>
<div class="m2"><p>گفت خوردم این سه نوباوه تمام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پس ندادم هیچکس را از کبار</p></div>
<div class="m2"><p>زانکه هر سه تلخ افتاد آن خیار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میبترسیدم که گر گوید کسی</p></div>
<div class="m2"><p>آن جگر خسته برنجد زان بسی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوردم آن تنها و برخویش آمدم</p></div>
<div class="m2"><p>یک زمان من نیز درویش آمدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیشوایانی که سر افراشتند</p></div>
<div class="m2"><p>پیش ازین یارب چه رحمت داشتند</p></div></div>