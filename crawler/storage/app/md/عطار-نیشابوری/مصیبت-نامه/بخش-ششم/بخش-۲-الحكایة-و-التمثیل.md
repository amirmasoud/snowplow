---
title: >-
    بخش ۲ - الحكایة و التمثیل
---
# بخش ۲ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>سوی اسپاهان براه مرغزار</p></div>
<div class="m2"><p>باز میآمد ملکشاه از شکار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغزاری ودهی بد پیش راه</p></div>
<div class="m2"><p>کرد منزل وقت شام آنجایگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از غلامان چند تن بشتافتند</p></div>
<div class="m2"><p>بر کنار راه گاوی یافتند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ذبح کردند و بخوردندش بناز</p></div>
<div class="m2"><p>آمدند آنگه بلشگرگاه باز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود گاو پیر زالی دل دو نیم</p></div>
<div class="m2"><p>روز و شب درمانده با مشتی یتیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قوت او و آن یتیمان اسیر</p></div>
<div class="m2"><p>آن زمان بودی که دادی گاو شیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چند تن درگاو مینگریستند</p></div>
<div class="m2"><p>جمله بر پشتی او میزیستند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیرزن را چون خبر آمد ازان</p></div>
<div class="m2"><p>بی خبر گشت و بسر آمد ازان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جملهٔ شب در نفیر و آه بود</p></div>
<div class="m2"><p>پیش آن پل شد که پیش راه بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون ملکشه بامداد آنجا رسید</p></div>
<div class="m2"><p>پیرزن پشت دوتا آنجا بدید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>موی همچون پنبه روئی چون زریر</p></div>
<div class="m2"><p>با یتیمان آمده آنجا اسیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با عصا در دست پشتی چون کمان</p></div>
<div class="m2"><p>گفت ای شهزادهٔ الب ارسلان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر برین سر پل بدادی داد من</p></div>
<div class="m2"><p>رستی از درد دل و فریاد من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ورنه پیش آن سر پل و ان صراط</p></div>
<div class="m2"><p>داد خواهم این زمان کن احتیاط</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر ز ظلم تو زبون گردم ز تو</p></div>
<div class="m2"><p>پیش حق فردا بخون گردم ز تو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>من ز ظلمت میندانم سر ز پای</p></div>
<div class="m2"><p>گرچه شاهی بر نیائی با خدای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هان و هان دادم برین پل ده تمام</p></div>
<div class="m2"><p>تا بران پل بر نمانی بر دوام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از همه سود و زیان در پیش و پس</p></div>
<div class="m2"><p>مر یتیمان مرا این بود بس</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گرسنه بگذاشتی اطفال را</p></div>
<div class="m2"><p>پیش خلق انداختی این زال را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در سحر یک نالهٔ این پیر زال</p></div>
<div class="m2"><p>مردی صد رستم آرد در زوال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>این نه از شاه جهانم میرسد</p></div>
<div class="m2"><p>کاین ز دور آسمانم میرسد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سخت کندم کرد چرخ تیز گرد</p></div>
<div class="m2"><p>چون توان با سرکشی آویز کرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>این بگفت وهمچو باران بهار</p></div>
<div class="m2"><p>با یتیمان شد بزاری اشکبار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هیبتی در جان شاه افتاد ازو</p></div>
<div class="m2"><p>سخت شوری در سپاه افتاد ازو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گفت ای مادر مگردان دل ز شاه</p></div>
<div class="m2"><p>هرچه میخواهی برین سر پل بخواه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تابراین پل بر تو برگویم جواب</p></div>
<div class="m2"><p>کان سر پل را ندارم هیچ تاب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>حال چیست ای زال گفت او حال خویش</p></div>
<div class="m2"><p>دادش او هفتاد گاو از مال خویش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفت این هفتاد گاو ای پیر زال</p></div>
<div class="m2"><p>در عوض بستان که هست این از حلال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>این بگفت وآن غلامان را بخواند</p></div>
<div class="m2"><p>زجر کرد و سبز خنگ از پل براند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پیرزن را وقت چون شبگیر شد</p></div>
<div class="m2"><p>حق آن انعام دامن گیر شد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>غسل آورد و نماز آغاز کرد</p></div>
<div class="m2"><p>روی بر خاک ودر دل باز کرد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گفت ای پروردگار دادگر</p></div>
<div class="m2"><p>چون ملکشه بادنیمی از بشر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از کرم نگذاشت بر من مابقی</p></div>
<div class="m2"><p>تو که جاویدان کریم مطلقی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>فضل کن با او و در بندش مدار</p></div>
<div class="m2"><p>وانچه نپسندیدهٔ زو در گذار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چون ملکشه رفت از آن جای خراب</p></div>
<div class="m2"><p>دیدش از عباد دین مردی بخواب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گفت هان چون رفت حال ای پادشاه</p></div>
<div class="m2"><p>گفت اگر آن بیوه زال دادخواه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از برای من نکردی آن دعا</p></div>
<div class="m2"><p>جز شقاوت نیستی دایم مرا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نیک بختی گشت آن بدبختیم</p></div>
<div class="m2"><p>از دعای اونماند آن سختیم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>عالمی بار اوفتاد از گردنم</p></div>
<div class="m2"><p>تا ابد آزاد کرد آن زنم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گرچه مرد ملک و مالی آمدم</p></div>
<div class="m2"><p>در پناه پیر زالی آمدم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کس چه داند تا دعای پیر زن</p></div>
<div class="m2"><p>چون بود وقت سحرگه تیر زن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آنچه زالی در سحر گاهی کند</p></div>
<div class="m2"><p>میندانم رستیم ماهی کند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گر نبودی رحمت آن پادشاه</p></div>
<div class="m2"><p>باز ماندی تا ابد در قعر چاه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ور نبودی آن دعای پیر زال</p></div>
<div class="m2"><p>دولت دین آمدی بروی زوال</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بود اول رحمت آن شهریار</p></div>
<div class="m2"><p>این دعا با او در آخر گشت یار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>لاجرم شه رستگار آمد مدام</p></div>
<div class="m2"><p>از رحیمی نیست برتر یک مقام</p></div></div>