---
title: >-
    بخش ۱ - المقالة السادسه
---
# بخش ۱ - المقالة السادسه

<div class="b" id="bn1"><div class="m1"><p>سالک آمد پیش عرش صعبناک</p></div>
<div class="m2"><p>گفت ای سر حد جسم و جان پاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هفت گلشن نقطهٔ پرگار تو</p></div>
<div class="m2"><p>هشت جنت غرقهٔ انوار تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اولین بنیاد در عالم توئی</p></div>
<div class="m2"><p>واپسین جسمی که ماند هم توئی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جسم و جان را کار پرداز آمدی</p></div>
<div class="m2"><p>جزو و کل را قبهٔ راز آمدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جملهٔ ارواح را مرجع توئی</p></div>
<div class="m2"><p>جملهٔ اشباح را مقطع توئی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای بقیومی حق قایم شده</p></div>
<div class="m2"><p>قدسیان را کعبهٔدایم شده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صد هزاران جوهر کروبیند</p></div>
<div class="m2"><p>در محبی اند و در محبوبیند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جمله ذاتت کعبهٔ خود ساخته</p></div>
<div class="m2"><p>تاابد دل در طواف انداخته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صد هزاران عنصر روحانیند</p></div>
<div class="m2"><p>در طواف تو بسر گردانیند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رحمت از هر دو جهان قسمت تراست</p></div>
<div class="m2"><p>زانکه از رحمن همه رحمت تراست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنکه با چندین جلالت آید او</p></div>
<div class="m2"><p>میتواند گر رهی بنماید او</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عرش اعظم زین سخن از جای شد</p></div>
<div class="m2"><p>چون شفق از دیده خون پالای شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفت بر من زین سخن جز نام نیست</p></div>
<div class="m2"><p>لاجرم یک ساعتم آرام نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیست از رحمن به جز نامی مرا</p></div>
<div class="m2"><p>چند الرحمن علی العرش استوی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همچو گرگی گرسنه فرسودهام</p></div>
<div class="m2"><p>در شکم هیچ ودهان آلودهام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون زموت سعد لرزیدم ز جای</p></div>
<div class="m2"><p>چون توانم داشت طاقت با خدای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر ز پیشان آب روشن میرود</p></div>
<div class="m2"><p>تیره میگردد چو بر من میرود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر دمم دولت رسد صد قافله</p></div>
<div class="m2"><p>من نمیبینم یکی را حوصله</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>لست ساقی روز میثاق آن مراست</p></div>
<div class="m2"><p>قصهٔ والتفت الساق آن مراست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هست اساس و اصل من برروی آب</p></div>
<div class="m2"><p>من برآبی مضطرب همچون حباب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گرچه محراب ملایک گشتهام</p></div>
<div class="m2"><p>منصبی نیست این نه مالک گشتهام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>من از آن کرسی نهادم زیر پای</p></div>
<div class="m2"><p>تا رسد خود دست من بر هیچ جای</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خود ز زیر پای من کرسی برفت</p></div>
<div class="m2"><p>وز دماغم آنچه میپرسی برفت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حال خود برگفتمت ای پاک مرد</p></div>
<div class="m2"><p>همچو من در خون نشین بر خاک درد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سالک آمد پیش پیر خرده دان</p></div>
<div class="m2"><p>برگشاد از حال او با او زفان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پیر گفتش هست عرش صعبناک</p></div>
<div class="m2"><p>عالم رحمت جهان نور پاک</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هرکجا در هر دو عالم رحمتست</p></div>
<div class="m2"><p>جمله را از عرش رحمن قسمتست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>منزل رحمت ز حق عرش آمدست</p></div>
<div class="m2"><p>پس زراه عرش در فرش آمدست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هر که او امروز رحمت میکند</p></div>
<div class="m2"><p>حق ز عرشش نور قسمت میکند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هرکه او بر زیردستان شد رحیم</p></div>
<div class="m2"><p>گشت دایم ایمن از خوف جحیم</p></div></div>