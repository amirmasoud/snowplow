---
title: >-
    بخش ۲ - الحكایة و التمثیل
---
# بخش ۲ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>عاشقی را بود معشوقی چو ماه</p></div>
<div class="m2"><p>مهر کرده ترک پیش او کلاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مدتی در انتظارش بوده بود</p></div>
<div class="m2"><p>جان بلب پرخون دل پالوده بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داد آخر وعدهٔ وصلیش یار</p></div>
<div class="m2"><p>گفت خواهد بودت امشب روز بار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرد آمد تا در دلخواه خویش</p></div>
<div class="m2"><p>اوفتادش مشکلی در راه پیش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت اگر این حلقه را بر در زنم</p></div>
<div class="m2"><p>گویدم آن کیست من گویم منم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گویدم پس چون توئی با خویش ساز</p></div>
<div class="m2"><p>عشق اگر بازی همه با خویش باز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ور بدو گویم نیم من این توی</p></div>
<div class="m2"><p>گویدم پس تو برو گر میروی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در میان این دو مشکل چون کنم</p></div>
<div class="m2"><p>خویش را بیخویش حاصل چون کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از شبانگه بر در آن دلفروز</p></div>
<div class="m2"><p>هم درین اندیشه بود او تا بروز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این سخن گفتند پیش صادقی</p></div>
<div class="m2"><p>گفت عاقل بود او نه عاشقی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زانکه همچون عاقلان صد گونه حال</p></div>
<div class="m2"><p>گشت بروی در جواب و در سؤال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لیک اگر بودیش عشقی کارگر</p></div>
<div class="m2"><p>درشکستی زود و در رفتی بدر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا براندیشی تو کار از بد دلی</p></div>
<div class="m2"><p>حاصلت گردد همه بی حاصلی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عاشقان را نیست با اندیشه کار</p></div>
<div class="m2"><p>مصلحت اندیش باشد پیشه کار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عاشق جانسوز خواهد سوز عشق</p></div>
<div class="m2"><p>روز محشر شب شود در روز عشق</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عشق بر معشوق چشم افتادنست</p></div>
<div class="m2"><p>بعد از آن از بیدلی جان دادنست</p></div></div>