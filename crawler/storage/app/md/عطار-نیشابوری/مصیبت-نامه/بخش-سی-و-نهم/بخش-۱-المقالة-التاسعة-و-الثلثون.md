---
title: >-
    بخش ۱ - المقالة التاسعة و الثلثون
---
# بخش ۱ - المقالة التاسعة و الثلثون

<div class="b" id="bn1"><div class="m1"><p>سالک بیدل فغان برداشته</p></div>
<div class="m2"><p>پیش دل شد دل ز جان برداشته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت ای حایل میان جسم و جان</p></div>
<div class="m2"><p>عکس اسرار تو ذرات جهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جملهٔ اسرار هست و نیست راست</p></div>
<div class="m2"><p>تا ابد از ذات تو حاصل تراست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هست آن ذرات جمله معنوی</p></div>
<div class="m2"><p>دایماً پاک از یکی و از دوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وی عجب آنجا یک و دو نیز هست</p></div>
<div class="m2"><p>نیست تمییز و همه تمییز هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نبودی هست و نیست آیات تو</p></div>
<div class="m2"><p>جزو بودی کل نبودی ذات تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جمله داری و نداری هیچ چیز</p></div>
<div class="m2"><p>تا چو هر بودت بود نابود نیز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با احد دور از عدد چون شنیدی</p></div>
<div class="m2"><p>همچو جمعه نی خودی نه بیخودی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون یسار تو یمین آمد همه</p></div>
<div class="m2"><p>هرچه آن را گوئی این آمدهمه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این و آنت نقد آن و این بس است</p></div>
<div class="m2"><p>حجت کلت ایدیه این بس است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در میان اصبعین افتادهٔ</p></div>
<div class="m2"><p>لاجرم غیری و عین افتادهٔ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اصبعینت را یمین سلطان بسست</p></div>
<div class="m2"><p>این دو حجت دایمت برهان بسست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون چنین قربی مسلم آمدت</p></div>
<div class="m2"><p>کمترین بعدی دو عالم آمدت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قربتی ده این بعید افتاده را</p></div>
<div class="m2"><p>بیدلی در من یزید افتاده را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دل ز بیدل چون شنود اسرار او</p></div>
<div class="m2"><p>همچو دل سرگشته شد در کار او</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفت من عکسیام از خورشید جان</p></div>
<div class="m2"><p>مست جاوید از می جاوید جان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دل ز اصبع جان ز نفخ خاص خاست</p></div>
<div class="m2"><p>کی کند ظاهر چو باطن کار راست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>قلب از آنم من که میگردم مقیم</p></div>
<div class="m2"><p>تا رسد از نفخ روحم یک نسیم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>قلب از آنم من که میگردم مدام</p></div>
<div class="m2"><p>تا رسد از قرب جانم یک سلام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>قلب از آنم من که میگردم چو گوی</p></div>
<div class="m2"><p>تا رسد ازجان مرا یک ذره بوی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دایماً بی باده مست افتادهام</p></div>
<div class="m2"><p>کز چنان باطن بدست افتادهام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>باطنی کانرا نهایت روی نیست</p></div>
<div class="m2"><p>اهل ظاهر را ازو یک موی نیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جان ز باطن میرسد من چون کنم</p></div>
<div class="m2"><p>لاجرم زین غصه خود را خون کنم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یک نفس گر قرب من میبایدت</p></div>
<div class="m2"><p>در میانخون وطن میبایدت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ورنه ترک خون و ترک خاک گیر</p></div>
<div class="m2"><p>پاک گرد و راه جان پاک گیر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سالک آمد پیش پیر هوشیار</p></div>
<div class="m2"><p>حال خود برگفت دل پر اضطرار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پیر گفتش هست دل دریای عشق</p></div>
<div class="m2"><p>موج او پرگوهر سودای عشق</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>درد عشق آمد دوای هر دلی</p></div>
<div class="m2"><p>حل نشدبی عشق هرگز مشکلی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عشق در دل بین و دل در جان نهان</p></div>
<div class="m2"><p>صد جهان در صد جهان درصد جهان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در کلیدانی چه میباشی همی</p></div>
<div class="m2"><p>این جهانها راتماشا کن دمی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چند اندیشی بدین میدان درای</p></div>
<div class="m2"><p>همچو گوئی گرد و سرگردان درای</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مصلحت اندیش نبود مرد عشق</p></div>
<div class="m2"><p>بیقراری خواهد از تو درد عشق</p></div></div>