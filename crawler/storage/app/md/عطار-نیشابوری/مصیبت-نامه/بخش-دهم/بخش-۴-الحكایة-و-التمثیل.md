---
title: >-
    بخش ۴ - الحكایة و التمثیل
---
# بخش ۴ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>چون زلیخا شد ز یوسف بی قرار</p></div>
<div class="m2"><p>با میان آورد عشقش از کنار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر زلیخا شد همه عالم سیاه</p></div>
<div class="m2"><p>تا کند یوسف بسوی او نگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذرهٔ یوسف بدو می ننگریست</p></div>
<div class="m2"><p>تا زلیخا بر سر او میگریست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر زمان از پیش او برخاستی</p></div>
<div class="m2"><p>خویش از نوع دگر آراستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جلوه میکردی بپیش روی او</p></div>
<div class="m2"><p>ننگرستی هیچ یوسف سوی او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون زلیخا شد بجان درمانده</p></div>
<div class="m2"><p>حیلتی بر ساخت آن درمانده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خانهٔ فرمود بر هر سوی او</p></div>
<div class="m2"><p>کرده صورت جمله نقش روی او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چار دیوارش چو سقف از هر کنار</p></div>
<div class="m2"><p>بود از نقش زلیخا پرنگار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لایق آن خانه مفرش ساخت او</p></div>
<div class="m2"><p>هم ز نقش خود منقش ساخت او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت یوسف قبلهٔ روی عزیز</p></div>
<div class="m2"><p>چون نمیبیند چه خواهد بود نیز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون رخم نقد عزیز عالمست</p></div>
<div class="m2"><p>نیل مصرجامعم را شبنمست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون عزیزم من چنین در چشم خود</p></div>
<div class="m2"><p>برکشم چون مصر نیل از چشم بد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شش جهت در صورت خویش آورم</p></div>
<div class="m2"><p>یوسف صدیق را پیش آورم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا چو بیند نقش من از خانه او</p></div>
<div class="m2"><p>همچو من از من شود دیوانه او</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عاقبت چون حیله ساخت آن دلربای</p></div>
<div class="m2"><p>کرد یوسف رادرون خانه جای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یوسف از هر سوی کافکندی نظر</p></div>
<div class="m2"><p>نقش آن دلداده دیدی پیش در</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شش جهاتش صورت آن روی بود</p></div>
<div class="m2"><p>ای عجب یک صورت از شش سوی بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یوسف صدیق جان پاک تو</p></div>
<div class="m2"><p>در درون خانهٔ پر خاک تو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون نگه میکرد از هر سوی او</p></div>
<div class="m2"><p>می ندید از شش جهت جز روی او</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دید در هر ذرهٔ انوار حق</p></div>
<div class="m2"><p>موج میزد جزو جزو اسرار حق</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>لاجرم گر ماهی و گر ماه دید</p></div>
<div class="m2"><p>هر دو عالم نور وجه اللّه دید</p></div></div>