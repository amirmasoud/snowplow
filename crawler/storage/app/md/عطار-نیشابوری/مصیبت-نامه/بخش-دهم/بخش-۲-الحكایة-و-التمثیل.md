---
title: >-
    بخش ۲ - الحكایة و التمثیل
---
# بخش ۲ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>گرم شد یک روز شیخ با یزید</p></div>
<div class="m2"><p>گفت اگر خواهد خداوند مجید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مدت هفتاد سالم را شمار</p></div>
<div class="m2"><p>من ازو خواهم شمار ده هزار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زانکه سالی ده هزارست از عدد</p></div>
<div class="m2"><p>تا الست ربکم گفتست احد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جمله را در شور آورد از الست</p></div>
<div class="m2"><p>وز بلی شان جز بلا نامد بدست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر بلا کان در زمین وآسمانست</p></div>
<div class="m2"><p>از بلی گفتن نشان دوستانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بعد از آن گفتا که میآید خطاب</p></div>
<div class="m2"><p>کاین سخن چون گفته شد بشنو جواب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هفت اندامت کنم روز شمار</p></div>
<div class="m2"><p>جزو جزو و ذره ذره چون غبار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پس بهر یک ذره دیدارت دهم</p></div>
<div class="m2"><p>در خور هر دیدهٔ بارت دهم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ده هزاران ساله را نقد شمار</p></div>
<div class="m2"><p>گویمت اینک نهادم در کنار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا بهر یک ذره کاری میکنی</p></div>
<div class="m2"><p>این چنین کن گر شماری میکنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرکرا آن آفتاب اینجا بتافت</p></div>
<div class="m2"><p>آنچه آنجا وعده بود اینجا بیافت</p></div></div>