---
title: >-
    بخش ۹ - الحكایة و التمثیل
---
# بخش ۹ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>کرد پیغامبر مگر روزی گذر</p></div>
<div class="m2"><p>ناودانی گل همی در زد عمر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گذشت ازوی نکرد او را سلام</p></div>
<div class="m2"><p>از پسش حالی عمر برداشت گام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت آخر یا رسول اللّه چه بود</p></div>
<div class="m2"><p>کز عمر می بر شکستی زود زود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت گشتی از عمارت غرهٔ</p></div>
<div class="m2"><p>تو بمرگ ایماننداری ذرهٔ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو بلاشک بیخ جانت میزنی</p></div>
<div class="m2"><p>گر گلی بر ناودانت میزنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکرا در گور باید گشت خاک</p></div>
<div class="m2"><p>گل کند آخر نترسد از هلاک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از جهان بیرون همی باید شدن</p></div>
<div class="m2"><p>زیر خاک و خون همی باید شدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تانگردی پایمال خاک و خون</p></div>
<div class="m2"><p>کی رود سرگشتگیت از سر برون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر درختی گردد این هر ذره خاک</p></div>
<div class="m2"><p>بر دهد هر ذرهٔ صد جان پاک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کس چه داند تا چه جانهای شگرف</p></div>
<div class="m2"><p>غوطه خوردست اندرین دریای ژرف</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کس چه داند تا چه دلهای عزیز</p></div>
<div class="m2"><p>خون شدست و خون شود آن تو نیز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کس چه داند تا چه قالبهای پاک</p></div>
<div class="m2"><p>در میان خون فرو شد زیر خاک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در دوعالم نیست حاصل جز دریغ</p></div>
<div class="m2"><p>هیچکس را نیست در دل جز دریغ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در سرائی چون توان بنشست راست</p></div>
<div class="m2"><p>کز سر آن زود برخواهیم خاست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کار عالم جز طلسم و پیچ نیست</p></div>
<div class="m2"><p>جز خرابی در خرابی هیچ نیست</p></div></div>