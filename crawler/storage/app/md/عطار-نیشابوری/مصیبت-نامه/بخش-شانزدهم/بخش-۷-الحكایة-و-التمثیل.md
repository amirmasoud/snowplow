---
title: >-
    بخش ۷ - الحكایة و التمثیل
---
# بخش ۷ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>کاملی گفتست دانی مرد کیست</p></div>
<div class="m2"><p>نیست مرد انک او تواند شاد زیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرد آن باشد که جانی شادمان</p></div>
<div class="m2"><p>خوش تواند برد آزاد از جهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای درین چنبر همه تاب آمده</p></div>
<div class="m2"><p>همچو شاگرد رسن تاب آمده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون گذر بر چنبر آمد جاودان</p></div>
<div class="m2"><p>چند در گیری رسن گرد جهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند خواهی بیش ازین بر هم نهاد</p></div>
<div class="m2"><p>چون همه از هم فرو خواهد فتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نخواهی کرد قارونی مدام</p></div>
<div class="m2"><p>خورد و پوشی تا لب گورت تمام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>انبیا چون این چنین کردند کار</p></div>
<div class="m2"><p>تو دکان بالای استادان مدار</p></div></div>