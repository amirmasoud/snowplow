---
title: >-
    بخش ۲ - الحكایة و التمثیل
---
# بخش ۲ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>گفت یک روزی همایی میپرید</p></div>
<div class="m2"><p>لشکر محمود هرکورا بدید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر بسر در سایهٔ او تاختند</p></div>
<div class="m2"><p>خویش را بر یکدیگر انداختند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا ایاز آمد بر مقصود شد</p></div>
<div class="m2"><p>در پناه سایهٔ محمود شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس دران سایه میان خاک راه</p></div>
<div class="m2"><p>هر زمان در سر بگشتی پیش شاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن یکی گفتش که ای شوریده رای</p></div>
<div class="m2"><p>نیست آنجا سایهٔ پرهمای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت سلطانم همای من بسست</p></div>
<div class="m2"><p>سایهٔ او رهنمای من بسست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون بدانستم که کار اینست و بس</p></div>
<div class="m2"><p>در دو عالم روزگار اینست و بس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سر نپیچم هرگز از درگاه او</p></div>
<div class="m2"><p>میروم بی پاو سر در راه او</p></div></div>