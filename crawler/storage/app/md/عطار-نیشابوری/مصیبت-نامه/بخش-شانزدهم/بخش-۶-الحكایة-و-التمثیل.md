---
title: >-
    بخش ۶ - الحكایة و التمثیل
---
# بخش ۶ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>از نیاز بندگی آن پادشاه</p></div>
<div class="m2"><p>پیش مردی رفت از مردان راه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت پندی ده که رهبر باشدم</p></div>
<div class="m2"><p>زین چنین صد ملک بهتر باشدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت بنگر تا ترا ای شهریار</p></div>
<div class="m2"><p>کار دنیا چند میآید بکار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کار دنیا آنچه باشد ناگزیر</p></div>
<div class="m2"><p>آن قدر چون کرده شد آرام گیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کار عقبی نیز بنگر این زمان</p></div>
<div class="m2"><p>تا بعقبی چند محتاجی بدان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنچه در عقبی ترا آن درخورست</p></div>
<div class="m2"><p>کار آن کردن ترا لایق ترست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کار دین و کار دنیا روز وشب</p></div>
<div class="m2"><p>تو بقدر احتیاج خود طلب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنچت اینجا احتیاجست آن بکن</p></div>
<div class="m2"><p>وانچه آنجا بایدت درمان بکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر بموئی بستگی باشد ترا</p></div>
<div class="m2"><p>هم بموئی خستگی باشد ترا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ور بکوهی بستگی پیش آیدت</p></div>
<div class="m2"><p>هم بکوهی خستگی بیش آیدت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر تو هر پیوند تو بندی بود</p></div>
<div class="m2"><p>تا ترا پیوند خود چندی بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باز بر پیوند سر تا پای تو</p></div>
<div class="m2"><p>تا توانی مرد ور نه وای تو</p></div></div>