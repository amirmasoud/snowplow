---
title: >-
    بخش ۱ - المقالة السادسة عشره
---
# بخش ۱ - المقالة السادسة عشره

<div class="b" id="bn1"><div class="m1"><p>سالک سلطان دل درویش زاد</p></div>
<div class="m2"><p>با سری پر خاک آمد پیش باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت ای جان پرور خلق آمده</p></div>
<div class="m2"><p>همدم و پیوستهٔ حلق آمده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکه عمری کامران دارد زتست</p></div>
<div class="m2"><p>زندگی هرکه جان دارد ز تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ره بسوی جان بحرمت میبری</p></div>
<div class="m2"><p>نور میآری و ظلمت میبری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفت و روب صحن جانها هم ز تست</p></div>
<div class="m2"><p>گفت و گوی در زبانها هم ز تست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آتش افروز جوانی هم توئی</p></div>
<div class="m2"><p>مایه بخش زندگانی هم توئی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو سلیمان را ببالا بردهٔ</p></div>
<div class="m2"><p>تخت او شرقاً و غرباً بردهٔ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عادیان را تو ز بن برکندهٔ</p></div>
<div class="m2"><p>سرنگون کرده بخاک افکندهٔ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هم ترا لطفست و هم قوت بسی</p></div>
<div class="m2"><p>قوتم ده پس بلطفم کن کسی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو بسی گردیدهٔ گرد جهان</p></div>
<div class="m2"><p>بوی جانانم بجان من رسان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون ز سالک باد این پاسخ شنید</p></div>
<div class="m2"><p>زین دریغش باد سرد آمد پدید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفت من خود بر سر پایم مدام</p></div>
<div class="m2"><p>زین مصیبت باد پیمایم مدام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خاک بر سر دارم و بادی بدست</p></div>
<div class="m2"><p>از غم این نیست یک جایم نشست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در بدر میگردم و میجویمش</p></div>
<div class="m2"><p>روز تا شب این سخن میگویمش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>من درین ره سخت حیران آمدم</p></div>
<div class="m2"><p>همچو بادی سست پیمان آمدم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>این زمان بر باد دادم خوب و زشت</p></div>
<div class="m2"><p>من نه دوزخ خواهم اکنون نه بهشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر ازین مقصود یابم بوی من</p></div>
<div class="m2"><p>از دو عالم در ربایم گوی من</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ور نخواهم یافت بوئی یک نفس</p></div>
<div class="m2"><p>باد سردم کار خواهد بود و بس</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آتشم در دل فتاده زین غمست</p></div>
<div class="m2"><p>خرمنم بر باد داده زین غمست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر جهان صد باره پیمایم بسر</p></div>
<div class="m2"><p>هم نخواهد بود ازین سرم خبر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو بیفشان باری از من دامنت</p></div>
<div class="m2"><p>زانکهکاری راست ناید از منت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سالک آمد پیش پیر مقتدا</p></div>
<div class="m2"><p>کرد حال خویش پیش او ادا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پیر گفتش باد خدمتگار جانست</p></div>
<div class="m2"><p>ریح از روحست روح او از آنست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>راحت او انس و جان را شاملست</p></div>
<div class="m2"><p>در دوعالم انس و جان زو کاملست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>طیب افتادست و طیبی دارد او</p></div>
<div class="m2"><p>وز دم رحمن نصیبی دارد او</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هرکه اورا یوسفی گم کرده نیست</p></div>
<div class="m2"><p>گرچه ایمان آورد آورده نیست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یوسفی در مصر جان داری مقیم</p></div>
<div class="m2"><p>هر زمانت میرسد از وی نسیم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر نسیم او نیابی یک نفس</p></div>
<div class="m2"><p>آن نفس دانی که باشی هیچکس</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر دو عالم خصم تو افتد مقیم</p></div>
<div class="m2"><p>بس بود از یوسف خویشت نسیم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر همه عالم شود زیر و زبر</p></div>
<div class="m2"><p>تو مکن از سایهٔ‌ یوسف گذر</p></div></div>