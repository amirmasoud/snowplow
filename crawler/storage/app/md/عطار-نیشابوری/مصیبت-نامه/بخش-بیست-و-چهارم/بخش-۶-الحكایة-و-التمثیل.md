---
title: >-
    بخش ۶ - الحكایة و التمثیل
---
# بخش ۶ - الحكایة و التمثیل

<div class="b" id="bn1"><div class="m1"><p>پیش آن دیوانهٔ شد پادشا</p></div>
<div class="m2"><p>گفت از من حاجتی خواه ای گدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت دارم من دو حاجت در جهان</p></div>
<div class="m2"><p>بو که از شاهم برآید این زمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اول از دوزخ چو خوش برهانیم</p></div>
<div class="m2"><p>در بهشتم آری و بنشانیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پادشاهش گفت ای حیران راه</p></div>
<div class="m2"><p>هست این کار خدا از من مخواه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود مجنون را یکی خم پیش در</p></div>
<div class="m2"><p>شاه آن خم را ستاده بر زبر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت دور از پیش خم تا نرم نرم</p></div>
<div class="m2"><p>خم شود از تابش خورشید گرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زانکه شب تا روز در خم میشوم</p></div>
<div class="m2"><p>گرم و خوش میخسبم و گم میشوم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جامهٔ خوابم خمست ای نامور</p></div>
<div class="m2"><p>دور ازان سر تا نگردد سردتر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه مرا شد از تو یک حاجت روا</p></div>
<div class="m2"><p>نه زتودرد مرا آمد دوا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون نکردی داروی این درد تو</p></div>
<div class="m2"><p>جامه خوابم را مگردان سرد تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنکه صد تیمار دارش نیست بس</p></div>
<div class="m2"><p>چون تواند داشتن تیمار کس</p></div></div>