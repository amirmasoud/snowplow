---
title: >-
    بخش ۱ - المقالة الرابعة و العشرون
---
# بخش ۱ - المقالة الرابعة و العشرون

<div class="b" id="bn1"><div class="m1"><p>سالک طیار شد پیش طیور</p></div>
<div class="m2"><p>گفت ای پرندگان نار و نور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای برون جسته ز دام پر بلا</p></div>
<div class="m2"><p>صف کشیده جمله فی جوالسما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم زفان مرغ در شهر شماست</p></div>
<div class="m2"><p>هم نوا و نور از بهر شماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاشیان بی صفت پریدهاید</p></div>
<div class="m2"><p>در جهان معرفت گردیدهاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم ز بال و پر قفس بشکستهاید</p></div>
<div class="m2"><p>هم ز دام و بند بیرون جستهاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از شما شد هدهد دلاله کار</p></div>
<div class="m2"><p>صاحب انگشتری را راز دار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این شما را بس که هدهد یافتست</p></div>
<div class="m2"><p>وز چنان شاهی تفقد یافتست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شب هوای طشت پروین میکنید</p></div>
<div class="m2"><p>تا سحرگه خایه زرین میکنید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای همه بیواسطه بشتافته</p></div>
<div class="m2"><p>چینه از تغدوا خماصاً یافته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زیر سایه غرب تا شرق شما</p></div>
<div class="m2"><p>سایهٔ سیمرغ بر فرق شما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون شما را صحبت سیمرغ هست</p></div>
<div class="m2"><p>هرچه خواهم تا بشیر مرغ هست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>طفل راهم چارهٔ شیری کنید</p></div>
<div class="m2"><p>می بمیرم تشنه تدبیری کنید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون شنیدند این سخن مرغان باغ</p></div>
<div class="m2"><p>شد جهان بر چشمشان چون پر زاغ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرغ گفت ای بیخبر از حال من</p></div>
<div class="m2"><p>زین مصیبت سوخت پر وبال من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زین غمم در خون و درگل مانده</p></div>
<div class="m2"><p>همچو مرغی نیم بسمل مانده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جملهٔ عالم به پر پیمودهام</p></div>
<div class="m2"><p>پر و منقارم بخون آلودهام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>روز تا شب این طلب میکردهام</p></div>
<div class="m2"><p>خواب را شب خوش بشب میکردهام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عاقبت همچون تو حیران ماندهام</p></div>
<div class="m2"><p>بال و پر زین جست و جو افشاندهام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هست مرغ عاشق ما عندلیب</p></div>
<div class="m2"><p>واو ندارد هیچ جز دستان نصیب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر همایست استخوانی میخورد</p></div>
<div class="m2"><p>تا ازو شاهی جهانی میخورد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جلوهٔ طاوس منگر این نگر</p></div>
<div class="m2"><p>کو فرو آرد بیک میویز سر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هدهد از خود نیز در سر میکند</p></div>
<div class="m2"><p>در سرش چیزیست سر بر میکند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون شتر مرغی ما سیمرغ دید</p></div>
<div class="m2"><p>لاجرم از ننگ ما عزلت گزید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر تو پریدن بپر ما کنی</p></div>
<div class="m2"><p>پر بریزی خویش را رسوا کنی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سالک آمد پیش پیر بی نظیر</p></div>
<div class="m2"><p>داد حالی شرح از زاری چو زیر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پیر گفتش هست مرغ از بس کمال</p></div>
<div class="m2"><p>جملهٔ معنی علوی را مثال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>معنئی کان از سر خیری بود</p></div>
<div class="m2"><p>صورتش را آخرت طیری بود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ذات جان را معنی بسیار هست</p></div>
<div class="m2"><p>لیک تا نقد تو گردد کار هست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هر معانی کان ترا درجان بود</p></div>
<div class="m2"><p>تا نپیوندد بتن پنهان بود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چون بتن پیوست آن خاص آن تست</p></div>
<div class="m2"><p>نیست خاص آن تو گردر جان تست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دولت دین گر میسر گرددت</p></div>
<div class="m2"><p>نقد جان با تن برابر گرددت</p></div></div>