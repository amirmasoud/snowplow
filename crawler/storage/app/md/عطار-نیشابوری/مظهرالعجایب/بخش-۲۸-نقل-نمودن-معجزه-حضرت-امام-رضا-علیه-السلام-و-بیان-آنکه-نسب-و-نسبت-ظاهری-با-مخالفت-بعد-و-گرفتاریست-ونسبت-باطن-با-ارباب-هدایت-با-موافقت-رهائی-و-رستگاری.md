---
title: >-
    بخش ۲۸ - نقل نمودن معجزه حضرت امام رضا علیه السلام و بیان آنکه نسب و نسبت ظاهری با مخالفت، بعد و گرفتاریست ونسبت باطن با ارباب هدایت با موافقت رهائی و رستگاری
---
# بخش ۲۸ - نقل نمودن معجزه حضرت امام رضا علیه السلام و بیان آنکه نسب و نسبت ظاهری با مخالفت، بعد و گرفتاریست ونسبت باطن با ارباب هدایت با موافقت رهائی و رستگاری

<div class="b" id="bn1"><div class="m1"><p>بود در بغداد نیکو مقبلی</p></div>
<div class="m2"><p>سیّد پاکیزه خلقی پر دلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاهد و عابد بُد و پرهیزکار</p></div>
<div class="m2"><p>نیک روی و نیک خلق و با وقار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود نام او ابوالقاسم تمام</p></div>
<div class="m2"><p>سید و هم صالح و هم نیکنام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کرد عزم کوفه او با کاروان</p></div>
<div class="m2"><p>تا که حاصل گرددش مقصود جان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود در ره بیشهٔ بس هولناک</p></div>
<div class="m2"><p>صد هزاران تن در او رفته بخاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناگهی از کاروان پیشی گرفت</p></div>
<div class="m2"><p>راه درویشی و دلریشی گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک حماری داشت میرباوقار</p></div>
<div class="m2"><p>می شدی گه بر حمار خود سوار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون بشد یک پاره آن درویش راه</p></div>
<div class="m2"><p>دید یک شیری ستاده پیش راه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیلتن پر زور و مردم خوار و تند</p></div>
<div class="m2"><p>گشته از هوش هزاران فهم کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حمله کرد آنشیر و پیش او دوید</p></div>
<div class="m2"><p>از چنان هیبت خر سیّد رمید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جست سیّد بر زمین گفت ای اله</p></div>
<div class="m2"><p>جمله مسکینان عالم را پناه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از چنین محنت جدائی ده مرا</p></div>
<div class="m2"><p>وز بلای بدرهائی ده مرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زین سخن چون فارغ و آزاد گشت</p></div>
<div class="m2"><p>ناگهان اندر ضمیر او گذشت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آنکه روزی عارفی با او بگفت</p></div>
<div class="m2"><p>شیر را باشد حیا در چشم جفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هرکه چشم خود بچشم شیر بست</p></div>
<div class="m2"><p>شیر را با او نباشد هیچ دست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هرکه برچشمش بدوزد چشم گرم</p></div>
<div class="m2"><p>هیچکس را می نرنجاند ز شرم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خود چنان نزدیک با آن شیر بود</p></div>
<div class="m2"><p>کز دم آن شیر جانش سیر بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چشم سید چون بچشم شیر دوخت</p></div>
<div class="m2"><p>سر بزیر افکند شیر و بر فروخت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سر به پیش افکند آن شیر از حیا</p></div>
<div class="m2"><p>چشم بروی بود سیّد زاده را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پس غلام سیّد از پی در رسید</p></div>
<div class="m2"><p>خواجهٔ خود را به پیش شیر دید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نعرهٔ زد گفت ای مخدوم من</p></div>
<div class="m2"><p>میکُشد این شیرت آخر بی‌سخن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>رو بسوی کاروان فریاد کرد</p></div>
<div class="m2"><p>شیر برجست و ورا بر باد کرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شیر بر دریّد از یکدیگرش</p></div>
<div class="m2"><p>پاره پاره کرد از پا تا سرش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پس فدای جان سیّد شد غلام</p></div>
<div class="m2"><p>این معانی هست در جامع تمام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون خلاصی یافت از شیر آن زمان</p></div>
<div class="m2"><p>رفت سوی کوفه آن سید روان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون بکوفه کرد آن سید مقام</p></div>
<div class="m2"><p>جمع گردیدند خویشانش تمام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گشته بودند آگه آن مردم تمام</p></div>
<div class="m2"><p>از حدیث شیر و قتل آن غلام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زین الم گفتند ما بیدل شدیم</p></div>
<div class="m2"><p>چون کبوتر در غمت بسمل شدیم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شکرها کردیم اکنون این زمان</p></div>
<div class="m2"><p>کز بلای شیر ماندی در امان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در میان شان بود پیری خویش او</p></div>
<div class="m2"><p>مرهمی بهر درون ریش او</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بود نام نیک او سید علی</p></div>
<div class="m2"><p>عم یحیی بود آن نقد ولی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گفت قول مصطفی نشنیده‌اید</p></div>
<div class="m2"><p>این چنین حالت مگر کم دیده‌اید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هرکه باشد بیشک از نسل بتول</p></div>
<div class="m2"><p>کی کند زخم سباع او را ملول</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز آنکه بر آل نبی ای دین پرست</p></div>
<div class="m2"><p>هیچ درّنده نخواهد یافت دست</p></div></div>