---
title: >-
    بخش ۱ - بسم الله الرحمن الرحیم
---
# بخش ۱ - بسم الله الرحمن الرحیم

<div class="b" id="bn1"><div class="m1"><p>آفرین جان آفرین و جان جان</p></div>
<div class="m2"><p>آنکه هست او آشکارا و نهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مقام لایزالی آشکار</p></div>
<div class="m2"><p>در درون عاشقان بیقرار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آسمان یک پرده از اسرار او</p></div>
<div class="m2"><p>وین زمین یک نقطه از پرگار او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای منزّه از همه بود ونبود</p></div>
<div class="m2"><p>وی مبرّا از همه گفت و شنود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آسمان چون چرخ سرگردان او</p></div>
<div class="m2"><p>قل هو الله آیتی در شان او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاک را از قدرت خود آفرید</p></div>
<div class="m2"><p>عقل و جان آورد از صنعت پدید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آفتاب از صنع او گردان شده</p></div>
<div class="m2"><p>ماه و زهره در رهش حیران شده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جسم را از خاک قدرت نقش داد</p></div>
<div class="m2"><p>روح را از آتش و از باد زاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روح را چون جان در این تن او نهاد</p></div>
<div class="m2"><p>سرّ اسرارش در این جان زوفتاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این زمین یک خشت از ایوان او</p></div>
<div class="m2"><p>نحن اقرب گفتهٔ دیوان او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هیچکس آسان به کنهش پی نبرد</p></div>
<div class="m2"><p>تا بظاهر او یقین از خود نمرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای بخود مغرور در ملک جهان</p></div>
<div class="m2"><p>کی بیایی تو ز کنه او نشان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای ز تو غافل همه عالم تمام</p></div>
<div class="m2"><p>سرّ اسرارت میان خاص و عام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دانهٔ لطف معانی داشتی</p></div>
<div class="m2"><p>در میان جان آدم کاشتی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون حق آمد در درون تو نهان</p></div>
<div class="m2"><p>این زمان عطّار درها میفشان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بعد از این گویم همه نعت رسول</p></div>
<div class="m2"><p>حضرت حق کرده عرفانش قبول</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از محّمد گویم و اطوار او</p></div>
<div class="m2"><p>من یقین دانسته‌ام کردار او</p></div></div>