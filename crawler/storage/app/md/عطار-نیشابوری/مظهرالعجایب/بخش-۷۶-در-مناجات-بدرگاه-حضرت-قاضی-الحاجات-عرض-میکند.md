---
title: >-
    بخش ۷۶ - در مناجات بدرگاه حضرت قاضی الحاجات عرض میکند
---
# بخش ۷۶ - در مناجات بدرگاه حضرت قاضی الحاجات عرض میکند

<div class="b" id="bn1"><div class="m1"><p>ای تو سلطان و کریم لایزال</p></div>
<div class="m2"><p>بی‌مثال و ذات پاکت بی‌زوال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای ترا آدم شده جویا مدام</p></div>
<div class="m2"><p>بوده اوبا نور تو بینا مدام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای ترا خود شیث بوده راز دان</p></div>
<div class="m2"><p>وی ترا ادریس بوده درس خوان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای تو داده نوح را کشتی حلم</p></div>
<div class="m2"><p>وی ز تو هم یافته جرجیس علم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای ز تو دیده‌ست ابراهیم آن</p></div>
<div class="m2"><p>حکم قربانی و بشنیده بجان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای تو با اسحق داده رحمتی</p></div>
<div class="m2"><p>وی تو داده هود را خوش نعمتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای درون نغمهٔ داود تو</p></div>
<div class="m2"><p>ای درون آتش نمرود تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای نشانده خود سلیمان را به تخت</p></div>
<div class="m2"><p>هم زکریا گشته زارّه لخت لخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای به یوسف همره و یعقوب زار</p></div>
<div class="m2"><p>کو زهجرش کرده خود راسوگوار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای درون کلبهٔ احزان شده</p></div>
<div class="m2"><p>هم به یوسف در چه زندان شده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای شده یعقوب را چون جان عزیز</p></div>
<div class="m2"><p>ای به به یوسف داده خود ملک عزیز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای به یوسف بر سر تخت آمده</p></div>
<div class="m2"><p>وی بکنعان طالع و بخت آمده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای زلیخا را فکنده خوار تو</p></div>
<div class="m2"><p>کرده از عشق جوانی زار تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای شعیبی راز تو علم و ضیا</p></div>
<div class="m2"><p>داده موسی را بمعنی تو عصا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای بداده صالح خود را صلاح</p></div>
<div class="m2"><p>ای نبوّت داده با او در صباح</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای ز ذوالکفل آب رحمت خواسته</p></div>
<div class="m2"><p>جنّتی از بهر او آراسته</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای بداده درد و صبر ایّوب را</p></div>
<div class="m2"><p>عاقبت داده است دردش را دوا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای که او را درد تو درمان بود</p></div>
<div class="m2"><p>مهر حب تو در ایمان بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای بداده ارمیارا جام عشق</p></div>
<div class="m2"><p>یافت از یوشع بلندی نام عشق</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای تو با الیاس و خضر راهبر</p></div>
<div class="m2"><p>وی ز روح الله جان داده خبر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای باحمد گفته خود اسرارها</p></div>
<div class="m2"><p>وی باحمد بوده در عین صفا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای تو با حیدر بمعنی آمده</p></div>
<div class="m2"><p>وی بهر دو کون بیناآمده</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای باحمد هم سر و هم تاج تو</p></div>
<div class="m2"><p>ای باحمد در شب معراج تو</p></div></div>