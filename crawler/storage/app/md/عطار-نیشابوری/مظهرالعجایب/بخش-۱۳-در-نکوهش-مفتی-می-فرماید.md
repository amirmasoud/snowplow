---
title: >-
    بخش ۱۳ - در نکوهش مفتی می‬فرماید
---
# بخش ۱۳ - در نکوهش مفتی می‬فرماید

<div class="b" id="bn1"><div class="m1"><p>من بگویم حالت قاضی تمام</p></div>
<div class="m2"><p>زانکه رشوت گیرد او از خلق عام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او همی بیند که دارد نام وننگ</p></div>
<div class="m2"><p>در شود در نار دوزخ بی‌درنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رشوت بسیار و زرهای یتیم</p></div>
<div class="m2"><p>در نهانی گیرد او از روی بیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس یتیم بیکسی پیدا کند</p></div>
<div class="m2"><p>مال او در دفتر خود جاکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قاضیی را یک ملازم بود فرد</p></div>
<div class="m2"><p>ضبط کردی مال ایتام از نبرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کرد پیدا او یتیمی بی سخن</p></div>
<div class="m2"><p>قاضیش گفتا که مالش ضبط کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شش هزاری داشت نقره آن یتیم</p></div>
<div class="m2"><p>گفت حق داده مرا خوان نعیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پس گرفت آن زر بسوی خانه کرد</p></div>
<div class="m2"><p>وز یتیم بی پدر بیگانه کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برگرفت او یک هزار از بهر خود</p></div>
<div class="m2"><p>پنج دیگر را بقاضی کرد رد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی‌تکلّف بهر خود قاضی گرفت</p></div>
<div class="m2"><p>وین حکایت را ز مردم می نهفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت قاضی تو چه کردی وجه را</p></div>
<div class="m2"><p>گفت کردم خرج او بی ماجرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کرد قاضی یک هزاری قرض از او</p></div>
<div class="m2"><p>گفت دیگر را نگه دار ای نکو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون برآمد چند روزی زین سخن</p></div>
<div class="m2"><p>گفت با قاضی که با ما رحم کن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وجه آن مسکین یتیم مستمند</p></div>
<div class="m2"><p>برد دزد و اوفتادش در کمند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جمله را دزدان بدزدیدند و رفت</p></div>
<div class="m2"><p>جان از این آتش بوددر تاب و تفت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفت رو چون برتو این دعوی کنند</p></div>
<div class="m2"><p>با تو این دعویّ بی‌معنی کنند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گوی زر را دزد از من برده است</p></div>
<div class="m2"><p>خاطر من زین سبب افسرده است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>من بحفظ آن بکردم جهد نیک</p></div>
<div class="m2"><p>جمله را محکم نهادم زیر ریگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>من زرت را چون امینی بوده‌ام</p></div>
<div class="m2"><p>کی بدان من دست خود آلوده‌ام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هیچ بر تو می‌نیاید مرد باش</p></div>
<div class="m2"><p>وز غم واندوه عالم فرد باش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون یتیم آن زر طلب کرد از امین</p></div>
<div class="m2"><p>پیش قاضی رفت نالان و غمین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ماجری گفتند با قاضی بهم</p></div>
<div class="m2"><p>کرد قاضی ناتوان را متّهم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گفت قاضی با یتیم ای بوالعجیب</p></div>
<div class="m2"><p>این چنین در شرع ما نبود غریب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اویکی مرد امین عادل است</p></div>
<div class="m2"><p>سالها در محکمه دارد نشست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زو خیانت کی روا باشد روا</p></div>
<div class="m2"><p>بر تو باشد زین حکایت حدّ روا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دیگر آنکه هیچ می‌ناید بشرع</p></div>
<div class="m2"><p>برامین تو برای اصل و فرع</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون یتیم از قاضی اعظم شنید</p></div>
<div class="m2"><p>این سخن را گفت از شرع این بعید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کار قاضی این و کار مفتی آن</p></div>
<div class="m2"><p>کار ملاّی مدرّس را بمان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>راه شرع اینست کایشان می‌روند</p></div>
<div class="m2"><p>این همه دنبال شیطان می‌روند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>راه راه مصطفی و آل اوست</p></div>
<div class="m2"><p>چون بدانستی برو کاین ره نکوست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>من بتو صد بار گفتم صد هزار</p></div>
<div class="m2"><p>دست از دامان حیدر بر مدار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>راه حیدر رو که اندر راه او</p></div>
<div class="m2"><p>نور حق بد از دل آگاه او</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>راه راه اوست دیگر راه نیست</p></div>
<div class="m2"><p>گر روی جای دگر جز چاه نیست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خویش را مفکن تو اندر چاه تن</p></div>
<div class="m2"><p>جهد کن تا تو برون آئی چو من</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>این همه درها که این عطّار سفت</p></div>
<div class="m2"><p>در درون گوش او کرّار گفت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گفت بشنو گیر درگوش این همه</p></div>
<div class="m2"><p>تا شود روشن شب تو زین همه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز آنکه شب تاریک و ظلمانی بود</p></div>
<div class="m2"><p>در درونش آب حیوانی بود</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>من بسی شبها بکنجی بوده‌ام</p></div>
<div class="m2"><p>راه عرفان را بسی فرسوده‌ام</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گنج جانست و جواهر معرفت</p></div>
<div class="m2"><p>من از اینها می‌کنم باتو صفت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ای تو مغرور جهان و مال خود</p></div>
<div class="m2"><p>رحم می‌ناید ترا بر حال خود</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گر هزاران سال تو زحمت بری</p></div>
<div class="m2"><p>مال دنیا را همه جمع آوری</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>عاقبت بگذاری و بیرون روی</p></div>
<div class="m2"><p>خود یقین میدان که تو ملعون روی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هست دنیا پر ز آتش بهر کس</p></div>
<div class="m2"><p>اوفتاده اندرو چون خار و خس</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ای گرفتار عیال و زن شده</p></div>
<div class="m2"><p>همچو حیوان در پی خوردن شده</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>باتو کردم بارها این ماجرا</p></div>
<div class="m2"><p>تا به کی تو پروری این نفس را</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>رو تو از دنیای دون بگذر چو من</p></div>
<div class="m2"><p>گر تو انسانی گذر زین انجمن</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ای تودر بازار دنیا بس خراب</p></div>
<div class="m2"><p>می نداری هیچ در عقبی ثواب</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بهر یک نان بیسر و سامان شده</p></div>
<div class="m2"><p>در میان مردمان حیران شده</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گر تو صد اشتر پر از دیبا کنی</p></div>
<div class="m2"><p>وین جهان را جمله پرغوغا کنی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>سقف و ایوان سازی و سلطان شوی</p></div>
<div class="m2"><p>تاجدار ملک هندستان شوی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ور چو اسکندر شوی با تاج و تخت</p></div>
<div class="m2"><p>یا فریدونی شوی با حظّ و بخت</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>عاقبت راه فنا گیری به پیش</p></div>
<div class="m2"><p>می عدم بینی همه اعضای خویش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بعد از آن در خاک پنهانت کنند</p></div>
<div class="m2"><p>پس عزیزان ختم قرآنت کنند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>این چنین ها بین و فکر خویش کن</p></div>
<div class="m2"><p>زاد راهت مظهر درویش کن</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>رو تو درویشی گزین و پاک باش</p></div>
<div class="m2"><p>در میان عاشقان چالاک باش</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>رو تو با حق باس و راز حق شنو</p></div>
<div class="m2"><p>تا بیابی سرّ عرفان نو بنو</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تو نیابی بی ولی راه خدا</p></div>
<div class="m2"><p>گر هزاران سال باشی رهنما</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بی دلیلی راه گم گردد ترا</p></div>
<div class="m2"><p>خوش دلیلی هست شاه اولیا</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>راه او راه محمّد دان و باش</p></div>
<div class="m2"><p>راه احمد دان ره یزدان و باش</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>همچو عطّار اندر این ره زن قدم</p></div>
<div class="m2"><p>گر همی خواهی که یابی سرّ جم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>رو تو گردی باش اندر پای او</p></div>
<div class="m2"><p>تادری یابی تو از دریای او</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>گر بمعنیّم رسی انسان شوی</p></div>
<div class="m2"><p>ورنه میرو تا که چون حیوان شوی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>من به صنعت سحر دارم در سخن</p></div>
<div class="m2"><p>من هم از حق دارم این سرّکهن</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>من ز دریاها جواهر آرمت</p></div>
<div class="m2"><p>وندر او سرّها بظاهر آرمت</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>اهل دل آگه شوند از رمز من</p></div>
<div class="m2"><p>عارفان کردند فهم این سخن</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>فهم من در جان عاشق نور شد</p></div>
<div class="m2"><p>زین سخن دانای مامستور شد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>هر که او مستور شد در راه عشق</p></div>
<div class="m2"><p>هست او از جان و دل آگاه عشق</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>عشق سرگردان او در کلّ حال</p></div>
<div class="m2"><p>حال او معشوق داند چون زلال</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>هر که او همرنگ یار خویش بود</p></div>
<div class="m2"><p>از جهان گوی معانی را ربود</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ای تو در راه خدا یکرنگ نه</p></div>
<div class="m2"><p>وز درون و وز برون جز رنگ نه</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>زنگ دل را برتراش و پاک شو</p></div>
<div class="m2"><p>و آنگهی در راه حق چون خاک شو</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>هر که چون دانه بیفتد سرکشد</p></div>
<div class="m2"><p>خم معنی را به یک دم درکشد</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>سرفرازی حقّ درویشان بود</p></div>
<div class="m2"><p>آه و سوز و درد هم ز ایشان بود</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>گر تو می‌خواهی که یابی دولتی</p></div>
<div class="m2"><p>وارهی بی شبهه از هر ذلّتی</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>رو طریق و راه درویشان بگیر</p></div>
<div class="m2"><p>همچو ایشان باش و با ایشان بگیر</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>هست شرع احمدی راه درست</p></div>
<div class="m2"><p>هر که جز این راه رفت او رنج جست</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>هر که در الطاف سرمد باشد او</p></div>
<div class="m2"><p>پیرو شرع محمد باشد او</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>گر تو یک دم همنشین جان شوی</p></div>
<div class="m2"><p>همچو ناصر سرور ایمان شوی</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ناصر خسرو بحق چون راه یافت</p></div>
<div class="m2"><p>همچو منصور او نظر در شاه یافت</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>تو یقین میدان که شه بیراه نیست</p></div>
<div class="m2"><p>گر روی راه دگر شه راه نیست</p></div></div>