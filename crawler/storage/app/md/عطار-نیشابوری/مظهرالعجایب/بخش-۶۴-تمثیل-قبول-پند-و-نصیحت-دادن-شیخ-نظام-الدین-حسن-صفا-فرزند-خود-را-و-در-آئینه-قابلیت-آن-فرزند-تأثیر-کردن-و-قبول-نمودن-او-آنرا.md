---
title: >-
    بخش ۶۴ - تمثیل قبول پند و نصیحت دادن شیخ نظام الدین حسن صفا فرزند خود را و در آئینه قابلیت آن فرزند تأثیر کردن و قبول نمودن او آنرا
---
# بخش ۶۴ - تمثیل قبول پند و نصیحت دادن شیخ نظام الدین حسن صفا فرزند خود را و در آئینه قابلیت آن فرزند تأثیر کردن و قبول نمودن او آنرا

<div class="b" id="bn1"><div class="m1"><p>بود شیخی همچو شبلی پارسا</p></div>
<div class="m2"><p>حق تعالی داده بود او را صفا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در معانی رهنمای اهل دین</p></div>
<div class="m2"><p>او بشهر علم حقّ بودی امین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نام او بودی نظام الدّین حسن</p></div>
<div class="m2"><p>شد ملقّب با صفا آن مؤتمن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلق را از لطف خود بنواختی</p></div>
<div class="m2"><p>مال پیش مردمان انداختی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جمله خلقان را بدی راحت رسان</p></div>
<div class="m2"><p>اندر این معنی نکرده او زیان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکه بر خلق خدا شفقت کند</p></div>
<div class="m2"><p>حق تعالی خود برو رحمت کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکه او یک بنده را دل شاد کرد</p></div>
<div class="m2"><p>حق مر او را در زمان آزاد کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خطّ آزادی بسالک می‌دهند</p></div>
<div class="m2"><p>زآنکه بر خلق خدا خوش مشفق‌اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شفقت آن مرد حق حق آمده</p></div>
<div class="m2"><p>سینهٔ بی نور را صیقل زده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>داشت فرزندی عجب او پر غرور</p></div>
<div class="m2"><p>بود از اطوار او بس بی‌حضور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرچه دم زد در حقیقت او مدام</p></div>
<div class="m2"><p>میل خاطر بود او را سوی عام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دایماً با اهل دنیا کار داشت</p></div>
<div class="m2"><p>او چو ایشان جامه و دستار داشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شیخ را خاطر از او غمگین شده</p></div>
<div class="m2"><p>زآنکه او بامردم بی‌دین شده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صبح و شام و گاه و بی گه همرهش</p></div>
<div class="m2"><p>بوده این جمع ددان بر درگهش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اوطعام نیک دادی جمله را</p></div>
<div class="m2"><p>داشتی صحبت بآنها بر ملا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>طعمه‌اش خوردند و نیشش می‌زدند</p></div>
<div class="m2"><p>خبث از یاران و خویشش می‌زدند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چند بارش گفت شیخ ای بوالحسن</p></div>
<div class="m2"><p>بابدان منشین که داری نور من</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عاقبت از صحبت اهل جدل</p></div>
<div class="m2"><p>می‌شود نور تو با ظلمت بدل</p></div></div>