---
title: >-
    بخش ۵۶ - تمثیل در فواید خاموشی و گوش کردن پند استاد و یک جهت بودن در آن و بیان استعداد جبلی مستعدان
---
# بخش ۵۶ - تمثیل در فواید خاموشی و گوش کردن پند استاد و یک جهت بودن در آن و بیان استعداد جبلی مستعدان

<div class="b" id="bn1"><div class="m1"><p>پادشاهی بود احمد نام او</p></div>
<div class="m2"><p>رونق اسلام در ایّام او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پادشاهی عادل و با فضل و داد</p></div>
<div class="m2"><p>خاص و عام دهر پیشش سر نهاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در زمان او همه اهل علوم</p></div>
<div class="m2"><p>شادمان بودند در هر مرز وبوم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچکس در ملک او غمگین نبود</p></div>
<div class="m2"><p>زانکه لطفش عام گشته دروجود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرهم درد دل درویش بود</p></div>
<div class="m2"><p>بوددایم پیش حق اندر سجود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دایماً می‌خواست از حق یک پسر</p></div>
<div class="m2"><p>داد وی را حق تعالی یک گهر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوهری از صلب او موجود ساخت</p></div>
<div class="m2"><p>وآنگه او را عابد و معبود ساخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قابل و بس عاقل و بسیار دان</p></div>
<div class="m2"><p>داشت استعداد و شد اسراردان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون بحدّ چارده اندر رسید</p></div>
<div class="m2"><p>خود پدر او را بجان می‌پرورید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یوسف اندر حسن وداود از نفس</p></div>
<div class="m2"><p>دیدن او خلق را می‌شد هوس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صدهزاران دل اسیر غمزه‌اش</p></div>
<div class="m2"><p>بود سلطان جهان خود بنده‌اش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دانهٔ خالش هزاران دام داشت</p></div>
<div class="m2"><p>جان آدم را درو آرام داشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مردم چشمش دل عشّاق برد</p></div>
<div class="m2"><p>طاق ابرویش ز عالم طاق برد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صدهزاران دل ازو بُد بیقرار</p></div>
<div class="m2"><p>عشق او می‌کرد جانها را شکار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چشم خویش فتنهٔ عالم شده</p></div>
<div class="m2"><p>قدّ خویش سایهٔ آدم شده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صد هزاران دل ازو در موج خون</p></div>
<div class="m2"><p>غوطه خوردند و یکی نامد برون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آفتاب از رشک عکسش تاب زد</p></div>
<div class="m2"><p>خود سهیل طلعتش بر آب زد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صد هزاران بنده‌اش زرّین کمر</p></div>
<div class="m2"><p>تاج سلطانی بدش خود زیر سر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صد هزاران اسب تازیّ و شتر</p></div>
<div class="m2"><p>بیش بودش با دو صد صندوق درّ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عشق اودر جمله دلها نقش بست</p></div>
<div class="m2"><p>توبهٔ ارباب تقوی را شکست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خلق از عشقش ز قید عقل رست</p></div>
<div class="m2"><p>در هوایش گشت خلقی بت پرست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>لیک گنجی داشت در دل از علوم</p></div>
<div class="m2"><p>گنج دنیا پیش آن سیمرغ بوم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عرض می‌کردند بر وی گنج و مال</p></div>
<div class="m2"><p>او از آن میبود دایم در ملال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گفت یابم رنج من از عرض گنج</p></div>
<div class="m2"><p>گنج معنی بایدم نی گنج رنج</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بود او را عقل لقمان حکیم</p></div>
<div class="m2"><p>در میان اهل عرفان بُد سلیم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>روی او بود آیت صنع الاه</p></div>
<div class="m2"><p>بود بر رویش دو چشم او گواه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عقل انسان لال گشته پیش او</p></div>
<div class="m2"><p>جمله شاهان جهان درکیش او</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چون بدید آن شاه کیخسرو نشان</p></div>
<div class="m2"><p>آن پسر را مستعدیّ آن چنان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گفت یا رب این نعیم خلد را</p></div>
<div class="m2"><p>رهنما شو سوی مردی مقتدا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مقتدائی کودلیل حق بود</p></div>
<div class="m2"><p>در ره حق رهبر مطلق بود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>باشد او واقف ز گفتار نبی</p></div>
<div class="m2"><p>در طریقت راه او راه ولی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>او بدین مصطفی محکم بود</p></div>
<div class="m2"><p>در طریق مرتضی محرم بود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پس طلب کرد این چنین شیخی بدل</p></div>
<div class="m2"><p>باشد آنکس در معانی جان ودل</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مدّتی در این هوس افسرده بود</p></div>
<div class="m2"><p>کی زمانی زین طلب آسوده بود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>عاقبت گفتش یکی مقبول راه</p></div>
<div class="m2"><p>هست مردی عارف اندر ملک شاه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هست روشن ملک تو ازنطق او</p></div>
<div class="m2"><p>از همه دنیا بحق آورده رو</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>وانماید زو همه اسرار حق</p></div>
<div class="m2"><p>لی مع الله آمده درآن ورق</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فاضل و عرفان شعاری واقفی</p></div>
<div class="m2"><p>بر همه سرّ معانی عارفی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شاه مردی را بنزد خویش خواند</p></div>
<div class="m2"><p>گفت عرض بندگی باید رساند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>از من بیدل بر صاحبدلی</p></div>
<div class="m2"><p>گو که دارد ذوق تو یک مقبلی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>رفت آن مرد و سخن از راه گفت</p></div>
<div class="m2"><p>پیش عارف شد سخن از شاه گفت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گفت شه را چون شنید آمد ز دور</p></div>
<div class="m2"><p>اهل حق را کی بود در سر غرور</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>پس یکی آمد بنزد شاه گفت</p></div>
<div class="m2"><p>می‌رسد سلطان معنی در نهفت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شه باستقبال او بیرون دوید</p></div>
<div class="m2"><p>در جبین او زمعنی نور دید</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>شاه چون درویش را در برگرفت</p></div>
<div class="m2"><p>خدمت مردان حق از سر گرفت</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>برد شاهش سوی خلوتگاه خویش</p></div>
<div class="m2"><p>تا بجوید سوی معنی راه خویش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>شه بجای آورد شکر مقدمش</p></div>
<div class="m2"><p>ساخت در راز معانی محرمش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گفت فرزندی مرا حق داده است</p></div>
<div class="m2"><p>در دلش گنج حیا بنهاده است</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>در دل او میل دنیا هیچ نیست</p></div>
<div class="m2"><p>در سرش از آرزوها هیچ نیست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>آرزو دارم که باشد پیش تو</p></div>
<div class="m2"><p>بهره یابد از طریق و کیش تو</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>کشف اسرار یقین پیدا کند</p></div>
<div class="m2"><p>رو بعقبی پشت بر دنیا کند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>کرد آخر چون سخن شه با حکیم</p></div>
<div class="m2"><p>خواند آن فرزند را پیشش سلیم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>دید چون درویش آن خلق و وفا</p></div>
<div class="m2"><p>گفت در باطن بود او را صفا</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>داده‌اند او را بسی معنی ز غیب</p></div>
<div class="m2"><p>چونکه در ذاتش نبوده هیچ عیب</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گشته او واقف بسی ز اسرار من</p></div>
<div class="m2"><p>در معارف می‌شود او مؤتمن</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>حق عطا داده است او را علم وحلم</p></div>
<div class="m2"><p>صافی از درد جهالت شد بعلم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>شاه چون بشنید از پیر این سخن</p></div>
<div class="m2"><p>گفت با درویش کی پیر کهن</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>هم تو عالم هم تو عارف هم حکیم</p></div>
<div class="m2"><p>هم تو درویش و تو دیندار و سلیم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>لطف فرما از ره مهرو وداد</p></div>
<div class="m2"><p>این پسر را از کرم باش اوستاد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>علم دین و معرفت تعلیم کن</p></div>
<div class="m2"><p>گوش او پرگوهر تعظیم کن</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>تا شود در خدمتت ای ارجمند</p></div>
<div class="m2"><p>از معارف وز حقایق بهره‌مند</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>از پدر کردش قبول آن پیر راه</p></div>
<div class="m2"><p>گفت ادب باشد ورا خود عذر خواه</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>آنچه هست از دانش حق پیش من</p></div>
<div class="m2"><p>من باو خواهم رسانم بی سخن</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>لیک باید از سر خود دور شد</p></div>
<div class="m2"><p>ز آرزوهای جهان معذور شد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>سرّ حق گفتن ورا آسان بود</p></div>
<div class="m2"><p>در دل او گر مکان آن بود</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>سرّ حق گفتن باو نیکو بود</p></div>
<div class="m2"><p>گر بر از حق دلش را خو بود</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>سرّ حق گفتن بسی مشکل بود</p></div>
<div class="m2"><p>این معانی پیش اهل دل بود</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>سرّ حق گفتن بهر کس بد بود</p></div>
<div class="m2"><p>ز آنکه او را این معانی رد بود</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>سرّ حق را من بگویم پایدار</p></div>
<div class="m2"><p>زاو همی ترسم که گردد آشکار</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>گفت سلطان ای برحمت همنشین</p></div>
<div class="m2"><p>کس نباشد با تو در معنی بکین</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>گرچه باشد علم معنی خود نهان</p></div>
<div class="m2"><p>تو مکن سرّ خدا را زو عیان</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>زانکه ما سرّ خدا ظاهر کنیم</p></div>
<div class="m2"><p>پیشت اهل فضل را حاضر کنیم</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>آنچه حق گفته است تو با او بگو</p></div>
<div class="m2"><p>غیر حق را تو مکن خود جستجو</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>عارف آن شهزاده را با خویش برد</p></div>
<div class="m2"><p>مدّتی شهزاده پیشش جان سپرد</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>علم دین و علم معنی خواند و دید</p></div>
<div class="m2"><p>جمله گلهای حقایق را بچید</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>گشت حاضر بر تمام علم قال</p></div>
<div class="m2"><p>گشت آگاه از طریق اهل حال</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>گفت با استاد کی گنج علوم</p></div>
<div class="m2"><p>نیست مثلت عارفی در مرز و بوم</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چیست کار من که گردم غیب دان</p></div>
<div class="m2"><p>تا که من ثابت قدم گردم در آن</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>گفت دو چیز است کارت ای مرید</p></div>
<div class="m2"><p>تا شوی تو گنج معنی را کلید</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>گر بدانی بیشکی واصل شوی</p></div>
<div class="m2"><p>در میان عاشقان مقبل شوی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>گر بدانی همره قرآن شوی</p></div>
<div class="m2"><p>در معانی مغز نغز آن شوی</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>گر بدانی اوّلین و آخرین</p></div>
<div class="m2"><p>پیش تو باشد بمعنی در یقین</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>گر بدانی این دو معنی رادرست</p></div>
<div class="m2"><p>ملکت اسرار شاهی آن تست</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>گر بدانی این معانی را درست</p></div>
<div class="m2"><p>کوس سلطانی همه بر بام تست</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>گر بدانی محرم دلها شوی</p></div>
<div class="m2"><p>در وجود خویشتن یکتا شوی</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>گفت برگو نکتهٔ سر بسته را</p></div>
<div class="m2"><p>شربتی فرمای این دلخسته را</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>پیر گفت ای نکته‌دان تیزهوش</p></div>
<div class="m2"><p>دو سخن گویم بگیر آن را بگوش</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>غیر ازین خود نیست در عالم درود</p></div>
<div class="m2"><p>رو تو عود و چنگ را بربند زود</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>تا نگوید حال مشتاقان بکس</p></div>
<div class="m2"><p>این معمّا گفته‌ام من هر نفس</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>لیک گوش کس نیارد این شنید</p></div>
<div class="m2"><p>هست این اسرار من در جان دوعید</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>غیر ازین دو جمله غوغایست و شور</p></div>
<div class="m2"><p>این معانی من نگویم خود بزور</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>غیر ازین غیر است درمعنی بدان</p></div>
<div class="m2"><p>زآنکه مقصود تو آمد این بیان</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>آنچه مقصود است در علم آن بدان</p></div>
<div class="m2"><p>بعد از آن خاموش باش و بی‌زبان</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>غیر را محرم بدان اندر سخن</p></div>
<div class="m2"><p>یاد گیر این نکته را این دم ز من</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>غیر ازین پیوند جان خود مساز</p></div>
<div class="m2"><p>ورنه آرندت ببوته در گداز</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>غیر ازین چیزی نمی‌دانم یقین</p></div>
<div class="m2"><p>هست این معنی حقیقت راه دین</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>غیر ازین درگوش خود نشنیده‌ام</p></div>
<div class="m2"><p>من بچشم خویشتن این دیده‌ام</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>غیر ازین چیزی نمی‌باید شنید</p></div>
<div class="m2"><p>زآنکه این معنی رهی دارد بعید</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>غیر ازین کفر است و بی راهیّ مرد</p></div>
<div class="m2"><p>بعد ازین دفتر بکلّی درنورد</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>شاهزاده چون کلام او شنید</p></div>
<div class="m2"><p>مدّتی در علم می‌جستی مزید</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>مدتی خود را باین معنی ندید</p></div>
<div class="m2"><p>عاقبت او گفت پیر خود شنید</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>چونکه او را وقت خاموشی رسید</p></div>
<div class="m2"><p>گشت خاموش و دگر دم در کشید</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>دم فرو بست و درین محکم ستاد</p></div>
<div class="m2"><p>مهر اسرار خدا بر لب نهاد</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>شاه چون دریافت خاموشی آن</p></div>
<div class="m2"><p>گشت آشفته ز بیهوشی آن</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>هرچه گفت او را جواب او نداد</p></div>
<div class="m2"><p>شه از این حالت بسی شد نامراد</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>اهل ساز و اهل جشن اهل علوم</p></div>
<div class="m2"><p>جملگی کردند پیش او هجوم</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>تا شود از صحبت این جمله شاد</p></div>
<div class="m2"><p>وز پریشانی شود او را گشاد</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>این همه حاضر شد وسودی نداشت</p></div>
<div class="m2"><p>درد او زین هیچ بهبودی نداشت</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>بعد از آن شخصی عزایم خوان رسید</p></div>
<div class="m2"><p>گفت او را سایهٔ دیوان رسید</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>من عزایم خوانم و در وی دمم</p></div>
<div class="m2"><p>نیک گردد نقد شاه عالمم</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>چون امیران جمله خودترسان شدند</p></div>
<div class="m2"><p>جمله پیش آن عزایم خوان شدند</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>زان عزایم کم نشد هم درد عشق</p></div>
<div class="m2"><p>خود عزایم خوان نباشد مرد عشق</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>شاه عاجز گشت در احوال او</p></div>
<div class="m2"><p>ماند سرگردان عجب در حال او</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>شاه گنج بیکران کردش نثار</p></div>
<div class="m2"><p>هیچ درویشی نماندش در دیار</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>جمله را زر داد و منعم کرد و گفت</p></div>
<div class="m2"><p>خود دعا گوئید بر جانش نهفت</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>این همه از برکت اسرار اوست</p></div>
<div class="m2"><p>دید او ازمعنی دیدار اوست</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>رفت پیش پیر او آن شاه و گفت</p></div>
<div class="m2"><p>نقد من خاک درت ازدیده رفت</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>هرچه می‌گوئی ز تو می‌بشنود</p></div>
<div class="m2"><p>غیر روی تو بکس می ننگرد</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>رحم کن بر جان من ای پیر راه</p></div>
<div class="m2"><p>گوی با او تا کند در من نگاه</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>خود بفرما تا سخن گوید بمن</p></div>
<div class="m2"><p>بعد از آن در جان من گیرد وطن</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>گفت پیر راه با شاه جهان</p></div>
<div class="m2"><p>صبر کن تا حال او گردد عیان</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>سیر فرمایش بهر سوئی ز دهر</p></div>
<div class="m2"><p>تا ببیند جملگی آثار شهر</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>چون عجایب بیند او گوید سخن</p></div>
<div class="m2"><p>سرّ این معنی بدان و فهم کن</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>کرد آن شهزاده را آن شه سوار</p></div>
<div class="m2"><p>سیر می‌کردند در هر مرغزار</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>پس روان گشتند شاه و شهریار</p></div>
<div class="m2"><p>سیر می‌کردند اندر لاله زار</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>پیشتر میراند آن شاه وحید</p></div>
<div class="m2"><p>ناگهان درّاج بانکی در کشید</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>سوی آن جنگل روان بشتافتند</p></div>
<div class="m2"><p>چون طلب کردند او را یافتند</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>چون گرفتندش فتاد انرد بلا</p></div>
<div class="m2"><p>دید چون شهزاده آن درّاج را</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>گفت ای گشته مقیم بیشه تو</p></div>
<div class="m2"><p>چونکه خاموشی نکردی پیشه تو</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>گر تو خود خاموش میبودی چنین</p></div>
<div class="m2"><p>دشت و بیشه بُد ترا زیر نگین</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>خود نبود این ذوق خاموشی ترا</p></div>
<div class="m2"><p>اوفتادی لاجرم اندر بلا</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>این زمان از گفت خود داری فراق</p></div>
<div class="m2"><p>خود ندانستی تو ذوق اشتیاق</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>از زبان کردی تو سر رادر زیان</p></div>
<div class="m2"><p>این معانی را ندانستی بیان</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>این زمان کردی تو خود را سوگوار</p></div>
<div class="m2"><p>می‌برندت پای بسته زیردار</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>این زمان کردی دل خود را کباب</p></div>
<div class="m2"><p>چون بدادی تو جواب ناصواب</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>تو زگفت خود شدی در دام و بند</p></div>
<div class="m2"><p>از سخن گفتن فتادی در کمند</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>از زبان خودفتادی در رسن</p></div>
<div class="m2"><p>خود زبان تو بود سردار تن</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>هر زیان بینی تو هست او از زبان</p></div>
<div class="m2"><p>باشد اندر پیش من این سرعیان</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>شه شنید این نکتهٔ آزاده را</p></div>
<div class="m2"><p>قصّهٔ درّاج و آن شهزاده را</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>چون سخن گفتن شنید او از ولد</p></div>
<div class="m2"><p>کرد شکر خالق فرد صمد</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>گفت دادی هر چه جستم ای الاه</p></div>
<div class="m2"><p>هست لطفت جمله اشیا را پناه</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>پس بگفت آن شه بفرزند عزیز</p></div>
<div class="m2"><p>کای تمامی گشته خود عقل و تمیز</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>چون در این مدت چنین صامت بدی</p></div>
<div class="m2"><p>شکر کاین ساعت چنین گویا شدی</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>حال خود را گوی با من ای پسر</p></div>
<div class="m2"><p>تا که گردد کشف بر من این خبر</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>شاهزاده چون نداد او را جواب</p></div>
<div class="m2"><p>باز شاه اندر تعجّب اوفتاد</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>خلق می‌کردند با نطق و بیان</p></div>
<div class="m2"><p>قصهٔ درّاج را با شه عیان</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>شاه گفتا گوی با من یک سخن</p></div>
<div class="m2"><p>این دل آشفته‌ام را شاد کن</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>گر نگوئی تو سخن با من بلند</p></div>
<div class="m2"><p>خویش رادر خاک و خون خواهم فکند</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>هرچه شه گفت او جواب شه نداد</p></div>
<div class="m2"><p>همچو طفلانی که مادر نوبزاد</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>شاه ازین معنی برآشفته نگشت</p></div>
<div class="m2"><p>هم بچوبش کوفتند و هم بمشت</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>پس گشاد از شرم درج با گهر</p></div>
<div class="m2"><p>گفت کز گفتار کم یابم ثمر</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>گفته استادم بمن کاندر جهان</p></div>
<div class="m2"><p>کس زناگفتن ندید آخر زیان</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>هست خاموشی رهائی از همه</p></div>
<div class="m2"><p>عاقبت بینی جدائی از همه</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>هست خاموشی همه فرزانگی</p></div>
<div class="m2"><p>گرچه باشد ظاهرش دیوانگی</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>شد خموشی ملکت جم در نگین</p></div>
<div class="m2"><p>باشد اسرار خدا با وی یقین</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>هست خامش آیهٔ صنع خدا</p></div>
<div class="m2"><p>در همه معنی بود او مقتدا</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>هست خامش از همه غوغا خلاص</p></div>
<div class="m2"><p>فارغ است ازگفتگوی عام و خاص</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>هست خاموشی ره مردان راه</p></div>
<div class="m2"><p>این معانی کس نداند غیر شاه</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>هست خاموشی طریق اولیا</p></div>
<div class="m2"><p>شاهد این قول باشند انبیا</p></div></div>