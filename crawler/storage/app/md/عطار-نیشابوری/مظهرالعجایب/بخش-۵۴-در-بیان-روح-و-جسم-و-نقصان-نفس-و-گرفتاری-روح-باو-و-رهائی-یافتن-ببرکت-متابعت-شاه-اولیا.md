---
title: >-
    بخش ۵۴ - در بیان روح و جسم و نقصان نفس، و گرفتاری روح باو و رهائی یافتن ببرکت متابعت شاه اولیا
---
# بخش ۵۴ - در بیان روح و جسم و نقصان نفس، و گرفتاری روح باو و رهائی یافتن ببرکت متابعت شاه اولیا

<div class="b" id="bn1"><div class="m1"><p>روح تو شهباز علوی آمده است</p></div>
<div class="m2"><p>او شهنشاه سماوی آمده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خرمگس نفس است و بس دنیاپرست</p></div>
<div class="m2"><p>او کند دایم بمرداری نشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو بروح خویشتن بنگر که چیست</p></div>
<div class="m2"><p>بعد از آن در معنیش بنگر که کیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دشمن بسیار دارد روح تو</p></div>
<div class="m2"><p>لیک از او دایم بود مفتوح تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نفس دشمن کور سازد روح شاد</p></div>
<div class="m2"><p>تا روی در عالم معنی چو باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دشمن روحت همی بخل است و آز</p></div>
<div class="m2"><p>ز آن کی از کاهلی دایم نماز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دایما نفس تو باشد همچو سگ</p></div>
<div class="m2"><p>آهوان حرص را گیرد بتک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خود بکذّابی برآوردی تو نام</p></div>
<div class="m2"><p>تا بزیر جبّه بنهادی تو دام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیگر آنکه غافلی از یاد حق</p></div>
<div class="m2"><p>بهر یک دینار کردی صد عرق</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از عرق در مرگ رو اندیشه کن</p></div>
<div class="m2"><p>در معانیها عبادت پیشه کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نفس تو چون غافلت سازد ز حق</p></div>
<div class="m2"><p>دایما باشی تو اندر واق ووق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بهر مال و گنج داری رنج و درد</p></div>
<div class="m2"><p>خیز و افشان دامن خود را ز گرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کرد شیطان یک گنه توصد کنی</p></div>
<div class="m2"><p>هرچه گوید او تو آن را رد کنی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دیگر آنکه علم باطل ورد تست</p></div>
<div class="m2"><p>صد چو شیطان هر طرف شاگرد تست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سینه‌ات از حیله و شر پر بود</p></div>
<div class="m2"><p>در خیالت آنکه آن پر در بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رو صدف پر درّ معنی کن چو من</p></div>
<div class="m2"><p>تا شوی فارغ ز شرّ اهرمن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دیگرت وسواس ونخوت در سراست</p></div>
<div class="m2"><p>این چنین شیوه ترا کی درخور است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دید تو باشد جدا اندر جهان</p></div>
<div class="m2"><p>ز آنکه منصب داری و خرج گران</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گردبدعت گرد برگردت گرفت</p></div>
<div class="m2"><p>ورد باطل کردی و دردت گرفت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هست جسمت حقّهٔ پرریم و خون</p></div>
<div class="m2"><p>وقت خوردن میشوی تو سرنگون</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هست اجسامت پر از اخلاط و درد</p></div>
<div class="m2"><p>اندر این آلودگی خفتی و مرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اندرین دنیا مکن زنهار خواب</p></div>
<div class="m2"><p>زآنکه در معنی نباشد این صواب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر چو حیوان تو بخوردن راضیی</p></div>
<div class="m2"><p>من زتو بیزارم ار خود قاضیی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>قاضی شرع محمّد نیستی</p></div>
<div class="m2"><p>زآنکه اندر شرع احمد نیستی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در شریعت رد نه بینی همچو او</p></div>
<div class="m2"><p>گر ز معنی تو خبرداری و بو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>رو ازینها بگذر و مقصود بین</p></div>
<div class="m2"><p>در میان جان خود معبود بین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>رو تو این قطره بدریا وصل ساز</p></div>
<div class="m2"><p>زانکه این معنی بدانند اهل راز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جسم خود را پاک گردان همچو روح</p></div>
<div class="m2"><p>خویش را انداز در کشتی نوح</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>رو تو غیر حق ز جسمت دور کن</p></div>
<div class="m2"><p>بعد از آنی خانه‌ات پر نور کن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>غیر بیرون کن که حق آید درون</p></div>
<div class="m2"><p>گشت نفست در سوی الله رهنمون</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در درون خانه دارم راز حق</p></div>
<div class="m2"><p>برده‌ام از جملهٔ خلقان سبق</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>من سبق از پیش خود کی خوانده‌ام</p></div>
<div class="m2"><p>بر زبان من غیر او کی رانده‌ام</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>من سبق از مرتضی دارم بگوش</p></div>
<div class="m2"><p>لیک دارد آن سبق صد پرده پوش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هست از آن یک پرده پیش جبرئیل</p></div>
<div class="m2"><p>در درون پرده اسرار جلیل</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آسمان شد پردهٔ انوار او</p></div>
<div class="m2"><p>این زمین یک گردی از اسرار او</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>غیر او خود نیست با عطّار هیچ</p></div>
<div class="m2"><p>تو بیا بر نام من این نامه پیچ</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>غیر او در دل ندارم مهر کس</p></div>
<div class="m2"><p>مصطفی باشد گواهم این نفس</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>غیر مدح او نگویم مدح کس</p></div>
<div class="m2"><p>زآنکه مدح او خدا گفتست بس</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هست عطّار این زمان بس مستمند</p></div>
<div class="m2"><p>سینه مجروح و فقیر و دردمند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ای تو را ملک معانی در نگین</p></div>
<div class="m2"><p>جملهٔ کرُوبیانت خوشه چین</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ای تو را معبود محرم داشته</p></div>
<div class="m2"><p>در میان جان آدم داشته</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هست اسرارم بمعنی بود بود</p></div>
<div class="m2"><p>در همه جا مظهر انسان نمود</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>جوهر ذاتم ترا انسان کند</p></div>
<div class="m2"><p>در معانی همچو در غلطان کند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>جوهرذاتم عیان اندر عیان</p></div>
<div class="m2"><p>مظهر من هم نهان اندر نهان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>مظهر و جوهر براهت آورد</p></div>
<div class="m2"><p>بلکه خود نزدیک شاهت آورد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>خواهم از فضلت خدایا رحمتی</p></div>
<div class="m2"><p>کن بلطف خویش بر ما رحمتی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>خوان انعام تو باشد در خورم</p></div>
<div class="m2"><p>سرّ سودای تو باشد در سرم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ای خداوندا بحقّ انبیاء</p></div>
<div class="m2"><p>حقّ قرب و حقّ قدر اولیاء</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>کاین سخن را کن زنا محرم نهان</p></div>
<div class="m2"><p>زآنکه می‌بینم در حق را عیان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>حق عیانشد پیش ارباب کمال</p></div>
<div class="m2"><p>حق نهان شد پیش جمع قیل و قال</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>حق عیان دان پیش ره بینان عشق</p></div>
<div class="m2"><p>هست ظاهر نزد محبوبان عشق</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>حق عیان دان پیش جمعی اهل درد</p></div>
<div class="m2"><p>خیز و راه غیر حق را در نورد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>حق عیاندان پیش درویشان دین</p></div>
<div class="m2"><p>گه شده پیدا بآن و گه باین</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>حق عیان دان در وجود اهل دل</p></div>
<div class="m2"><p>تو شدی از فعل نفس خود خجل</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>فعل نفس تو ترا تیره کند</p></div>
<div class="m2"><p>بر جمیع فعلها خیره کند</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>دارم از علم لدّنی نقطه‌ای</p></div>
<div class="m2"><p>هر دو عالم پیش او خود ذرّه‌ای</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>هان که مقصودت ازان حاصل بکن</p></div>
<div class="m2"><p>تا بیابی جان معنی در سخن</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>من سخن گویم چو درّ شاهوار</p></div>
<div class="m2"><p>بهر تو آوردم و کردم نثار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>من سخن در ذات یزدان رانده‌ام</p></div>
<div class="m2"><p>بعد ازآن مظهر به انسان خوانده‌ام</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>من سخن دارم نگویم پیش کس</p></div>
<div class="m2"><p>ز آنکه تو واقف نه‌ای از کیش کس</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>کیش ترسائی به است از دین تو</p></div>
<div class="m2"><p>زآنکه ازنفس است کفر آئین تو</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>کردهٔ هفتاد فرقه دینت را</p></div>
<div class="m2"><p>از نبی شرمی بدار ای بی‌حیا</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>رو طریق آل احمد دار دوست</p></div>
<div class="m2"><p>راه ایشان تو یقین دان راه اوست</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>راه را دان از نبی و ازولی</p></div>
<div class="m2"><p>تو باین ره رو گریز از کاهلی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>کاهلی از جاهلی باشد ترا</p></div>
<div class="m2"><p>جاهلی کفر جلی باشد ترا</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>من ترا راهی نمایم راه راست</p></div>
<div class="m2"><p>وآنگهی گویم که راه حق کجاست</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>من ترادر راه حق رهبر کنم</p></div>
<div class="m2"><p>آگهت از دین پیغمبر کنم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>من ترا در راه حق خندان کنم</p></div>
<div class="m2"><p>خارجی را همچو سگ گریان کنم</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>من ترا راهی نمایم همچو نور</p></div>
<div class="m2"><p>تا کنی در عالم معنی ظهور</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>من ترا راهی نمایم از کلام</p></div>
<div class="m2"><p>انّما برخوان اگر داری نظام</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>من ترا راهی نمایم از یقین</p></div>
<div class="m2"><p>گر تو هستی مؤمن و بس پاک دین</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>من ترا راهی نمایم از رسول</p></div>
<div class="m2"><p>تو هم از عطّار کن این ره قبول</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>من ترا راهی نمایم همچو روح</p></div>
<div class="m2"><p>تا روی در کشتی احمد چو نوح</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>من ترا راهی نمایم گر روی</p></div>
<div class="m2"><p>واندرین ره کن بمعنی دل قوی</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>من ترا راهی نمایم در علوم</p></div>
<div class="m2"><p>بعد من هم عارفی گوید بروم</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>من ترا راهی نمایم از الست</p></div>
<div class="m2"><p>گر نباشد اعتقادات تو پست</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>من ترا راهی نمایم از ولی</p></div>
<div class="m2"><p>سر بنه در راه او گر مقبلی</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>من ترا راهی نمایم در ظهور</p></div>
<div class="m2"><p>تا تو بینی عکس رخسارش چو نور</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>من ترا راهی نمایم در علن</p></div>
<div class="m2"><p>تا تو بینی شاه خود در خویشتن</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>من ترا راهی نمایم عشق گفت</p></div>
<div class="m2"><p>آشکارا هین بکن سرّنهفت</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>من ترا راهی نمایم از کرم</p></div>
<div class="m2"><p>غیر این ره خود بعالم نسپرم</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>من ترا راهی نمایم از کمال</p></div>
<div class="m2"><p>گر تو باشی فاضل و عابد بحال</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>من ترا راهی نمایم همچو روز</p></div>
<div class="m2"><p>اوّلش عشقست و آخر دردوسوز</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>من ترا راهی نمایم فکر کن</p></div>
<div class="m2"><p>رو تو یاری گیر و با اوذکر کن</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>من ترا راهی نمایم از خدا</p></div>
<div class="m2"><p>لیک باید که تو باشی پارسا</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>من ترا راهی نمایم از علیم</p></div>
<div class="m2"><p>تو بر آن ره رو بجنّات النعیم</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>من ترا راهی نمایم خود بدهر</p></div>
<div class="m2"><p>کاندرو بینی هزاران شهر شهر</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>این چنین ره سالکان سر کرده‌اند</p></div>
<div class="m2"><p>پی از آن در ملک معنی برده‌اند</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>این چنین ره را نبیّ الله دید</p></div>
<div class="m2"><p>این حقیقت را همه از حق شنید</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>مصطفی ره را بفرزندان نمود</p></div>
<div class="m2"><p>راه ایشان گیر و حق را کن سجود</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>راه ایشان گیر تا حق بین شوی</p></div>
<div class="m2"><p>ورنه اندر گور بی تلقین شوی</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>راه ایشان گیر و دینت ترک کن</p></div>
<div class="m2"><p>از محبّان باش و کینت ترک کن</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>راه ایشان گیر تا ایمن شوی</p></div>
<div class="m2"><p>در ره معنی همه باطن شوی</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>راه ایشان گیر و درجنّت درای</p></div>
<div class="m2"><p>زانکه جنّت باشد ایشان را سرای</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>راه ایشان گیر و با سلمان نشین</p></div>
<div class="m2"><p>زانکه سلمان بوده اندر عین دین</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>گر روی این ره بمنزلها رسی</p></div>
<div class="m2"><p>ورنه کی در معنی دلها رسی</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>گر روی این ره مسلمان گویمت</p></div>
<div class="m2"><p>فیض بار از نور ایمان گویمت</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>گر روی این راه تو نامی شوی</p></div>
<div class="m2"><p>ورنه چون شیطان ببدنامی شوی</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>گر روی این ره تو دنیائی مبین</p></div>
<div class="m2"><p>ورنه رو بنشین و رسوائی مبین</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>گر روی این ره نبی همراه تست</p></div>
<div class="m2"><p>مصطفی و آل او آگاه تست</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>گر روی این ره شوی واقف ز خویش</p></div>
<div class="m2"><p>در درون خویش می‌بینی تو کیش</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>گر روی این ره مثال جان شوی</p></div>
<div class="m2"><p>ورنه در ملک جهان بیجان شوی</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>گر روی این ره معانی دان شوی</p></div>
<div class="m2"><p>خود درون جوهرت انسان شوی</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>گر روی این ره خدا راضی ز تو</p></div>
<div class="m2"><p>مصطفی و مرتضی راضی ز تو</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>گر روی این ره نظام الدین شوی</p></div>
<div class="m2"><p>یا شوی حلاج و هم حق بین شوی</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>گر روی این ره شود روشن دلت</p></div>
<div class="m2"><p>نعرهٔ مستان برآید از کلت</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>گر روی این ره عبادت پیشه کن</p></div>
<div class="m2"><p>رو زنااهلان دین اندیشه کن</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>گر روی این ره یکی همراه گیر</p></div>
<div class="m2"><p>بعد از آن رو دامن آن شاه گیر</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>گر روی این ره چو ابراهیم رو</p></div>
<div class="m2"><p>یا چو اسمعیل کن جانت گرو</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>گر روی این ره چو من دانی همه</p></div>
<div class="m2"><p>هم تو علم معرفت خوانی همه</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>گر روی این ره به اسراری رسی</p></div>
<div class="m2"><p>خود بکنج خانهٔ یاری رسی</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>گر روی این ره سفر باید سفر</p></div>
<div class="m2"><p>تا تو بینی در جهان هر خیر و شر</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>گر روی این ره تو همّت دار پیش</p></div>
<div class="m2"><p>رو طلب کن جوهر ذاتم ز خویش</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>گر روی این ره بمظهر کن نظر</p></div>
<div class="m2"><p>تا نیفتد در وجود تو خطر</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>گر روی این ره خطر ناید زبد</p></div>
<div class="m2"><p>خود همه افعال بد آید ز بد</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>گر روی این ره ببُر از خلق هم</p></div>
<div class="m2"><p>تا نگردی پیش ایشان متّهم</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>گر روی این ره بحق واصل شوی</p></div>
<div class="m2"><p>ورنه چون دیوار شوره گل شوی</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>گر روی این ره دلت روشن شود</p></div>
<div class="m2"><p>بعد از آن چشم توهم گلشن شود</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>گر روی این ره محبّت بایدت</p></div>
<div class="m2"><p>وز دل عطّار همّت بایدت</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>گر روی این ره دامن آن شاه گیر</p></div>
<div class="m2"><p>بعد از آن دست یکی همراه گیر</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>گر روی این ره ز سر باید گذشت</p></div>
<div class="m2"><p>از مراد خویش برباید گذشت</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>گر روی این ره تو ما را یادکن</p></div>
<div class="m2"><p>روح ما را از دعائی شاد کن</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>گر روی این ره باو باید نشست</p></div>
<div class="m2"><p>وآنگهی از غیر او باید گسست</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>گر روی این ره دلت غمگین مدار</p></div>
<div class="m2"><p>زانکه گیرندت بمعنی در کنار</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>گر روی این ره بخلقان کن کرم</p></div>
<div class="m2"><p>وآنگهی بیرون کن از ذاتت ستم</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>گر روی این ره مجو آزار خلق</p></div>
<div class="m2"><p>رو بمعنی کن نظر در زیر دلق</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>گر روی این ره نهان کن سرّ من</p></div>
<div class="m2"><p>تا نگویندت بدیها در سخن</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>گر روی این ره برو آسوده شو</p></div>
<div class="m2"><p>وآنگهی بر از ملایک تو گرو</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>گر روی این ره تو فرد فرد شو</p></div>
<div class="m2"><p>در میان اهل دل یک مرد شو</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>گر روی این ره ز زن باید گذشت</p></div>
<div class="m2"><p>بلکه از صندوق تن باید گذشت</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>گر روی این ره تو دنیائی بریز</p></div>
<div class="m2"><p>بعد از آن از أهل دنیا کن گریز</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>گر روی این ره ز راه خود گذر</p></div>
<div class="m2"><p>راه حق را خدادان بی خطر</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>گر روی این ره تو خورده دان شوی</p></div>
<div class="m2"><p>در معانی موسی عمران شوی</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>گر روی این راه منزل گویمت</p></div>
<div class="m2"><p>اوّلا از کعبهٔ دل گویمت</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>گر روی این راه شفقت کن بخلق</p></div>
<div class="m2"><p>تا دهندت جامهٔ شاهی نه دلق</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>گر روی این راه باید همرهمی</p></div>
<div class="m2"><p>تا نماید اندرین راهت رهی</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>گر روی این راه با یاران بهم</p></div>
<div class="m2"><p>من ترا خرگاه در کرسی زنم</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>گر روی این راه بی یاران مرو</p></div>
<div class="m2"><p>تانیفتی در درون چاه و کو</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>گر روی این راه ویاری نبودت</p></div>
<div class="m2"><p>مظهر و جوهر بکن تو همرهت</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>گر روی این راه بی رهبر مرو</p></div>
<div class="m2"><p>ور روی میبایدت صد جان نو</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>گر روی این راه با عطّار باش</p></div>
<div class="m2"><p>بحر لطف و مظهر انوار باش</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>رو تو این راه و مرو دنبال کس</p></div>
<div class="m2"><p>زانکه هفتادند در معنی و بس</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>رو تو این راه و رضا ده برقضا</p></div>
<div class="m2"><p>تا دهندت در معانیها عطا</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>رو تو این راه و بپا در کوی او</p></div>
<div class="m2"><p>تا ببینی در معانی روی او</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>رو تو این راه و مرو با گمرهان</p></div>
<div class="m2"><p>زانکه هستند همچو حیوان بی‌زبان</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>رو تو این راه و محمّد (ص)را بدان</p></div>
<div class="m2"><p>زانکه او هست رهنمای انس و جان</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>رو تو این راه و درین ره شاه بین</p></div>
<div class="m2"><p>بعد از آنی نور إلاّ الله بین</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>رو تو این راه و بدانش اصل خویش</p></div>
<div class="m2"><p>زانکه بر حق باشی و باوصل خویش</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>رو تو این راه و علی را دان امام</p></div>
<div class="m2"><p>تا که گردد دین و اسلامت تمام</p></div></div>