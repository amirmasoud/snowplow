---
title: >-
    بخش ۶۶ - ***
---
# بخش ۶۶ - ***

<div class="b" id="bn1"><div class="m1"><p>تو خدا را از یقین خود شناس</p></div>
<div class="m2"><p>باش از قهرش همیشه در هراس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بعد از آن خود را شناس و اصل خویش</p></div>
<div class="m2"><p>گرچه پیداگشتهٔ ای پاک کیش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرچه گوئی و کنی تو در جهان</p></div>
<div class="m2"><p>عاقبت گردد به پیش تو عیان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرچه تو از دید آن نقصان کنی</p></div>
<div class="m2"><p>عاقبت بین شو نباید آن کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرچه گوئی در نصیحت ای پسر</p></div>
<div class="m2"><p>اولّا تو در درون خود نگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رو توقدر مردمان نیک دان</p></div>
<div class="m2"><p>دوست را کن تو بسودا امتحان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو بکن دانای نیکو اختیار</p></div>
<div class="m2"><p>یک چله در پیش آن دانا برآر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا مسیح روح تو دانا شود</p></div>
<div class="m2"><p>چون کلیم دل بجان بینا شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون سخن گوئی تو نیکو گو سخن</p></div>
<div class="m2"><p>این معانی نکو را ورد کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو زبخل و از تکبّر دور باش</p></div>
<div class="m2"><p>از صفای علم همچون نور باش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جهد کن علم معانی را شناس</p></div>
<div class="m2"><p>تا بکی باشی ز شیطان در هراس</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای پسر در گوش گیر این پندها</p></div>
<div class="m2"><p>تو مده سر رشته را از کف رها</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از صفای علم لطف محض باش</p></div>
<div class="m2"><p>داردایم حضرت حق را سپاس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>روز بهر حق تو جان خویش باز</p></div>
<div class="m2"><p>باش منصور و بحق میدار راز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رو تو اهل دل طلب نه اهل کل</p></div>
<div class="m2"><p>زانکه اهل دل نباشد منفعل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اهل دل آنست عشق یار داشت</p></div>
<div class="m2"><p>در الم نشرح بسی اذکار داشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در الم نشرح چه گفته رو بدان</p></div>
<div class="m2"><p>زانکه با او سرّها بوده عیان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هرکه از قرآن حق بیدار شد</p></div>
<div class="m2"><p>والضحی وهل اتایش یار شد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هرکه با قرآن رود قرآن شود</p></div>
<div class="m2"><p>همنشین رحمت رحمن شود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر که او در مغز قرآن راز یافت</p></div>
<div class="m2"><p>دیدهٔ خورشید را چون ماه یافت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هرکه او با فقر باشد همنشین</p></div>
<div class="m2"><p>می‌نهم بر خاک پایش من جبین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر که دارد این مراتب یار ماست</p></div>
<div class="m2"><p>در خور سودای این بازار ماست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای پسر در گوش گیر این پند من</p></div>
<div class="m2"><p>تا که باشی در جهان پیوند من</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هرکه پیوندی بود اصلی بود</p></div>
<div class="m2"><p>زانکه او را با خدا وصلی بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای پسر میدان که غیر دوست نیست</p></div>
<div class="m2"><p>در نهان و آشکارا ظاهریست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای پسر گر بشنوی پند پدر</p></div>
<div class="m2"><p>عاقبت سلطان شوی بی سیم و زر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون پسر بشنید این پند از پدر</p></div>
<div class="m2"><p>بر درون صومعه بنهادسر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفت بد کردم ز لطف ای رهنما</p></div>
<div class="m2"><p>عفو فرما جرم این بیچاره را</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>من بدم چون طفل نادان درجهان</p></div>
<div class="m2"><p>حال من بد همچو حال آن ددان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>من از این کیش بدان برخاستم</p></div>
<div class="m2"><p>دل بشرع مصطفی آراستم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بعد از این حکم شما بر جان ماست</p></div>
<div class="m2"><p>راه شرع احمدی ایمان ماست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هرچه فرمائی تو ای پیر طریق</p></div>
<div class="m2"><p>من بجان گردم ورا یار و رفیق</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پند پیران بهتر از عمر دراز</p></div>
<div class="m2"><p>زآنکه ایشانند خود در عین راز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پند پیران بهتر از بخت جوان</p></div>
<div class="m2"><p>بشنو این معنی ز پیر غیب دان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پند پیران همچو اسم اعظم است</p></div>
<div class="m2"><p>بر جراحتها مثال مرهم است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>پند پیران مرهم جانی بود</p></div>
<div class="m2"><p>پند پیران راز پنهانی بود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>پند پیران باشدت چون پیشوا</p></div>
<div class="m2"><p>پند پیران باشدت خود مقتدا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>پند پیران آفتاب بی زوال</p></div>
<div class="m2"><p>پند پیرانست ماه عمر وسال</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>پند پیرانست فتح الباب دین</p></div>
<div class="m2"><p>پند پیرانت کند با دین قرین</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>پند پیرانست بحر موج زن</p></div>
<div class="m2"><p>پند پیرانست چون درّ عدن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پند پیرانست خود اسرار فاش</p></div>
<div class="m2"><p>در معانی واقف عطّار باش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گفت عطّارت که بیخوای گزین</p></div>
<div class="m2"><p>باش دایم با دل شب همنشین</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هر که با شب همنشین شد نور شد</p></div>
<div class="m2"><p>او بپاکی بهتر از صد حور شد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هر که با شب همنشین شد روز شد</p></div>
<div class="m2"><p>هست چون شمعی که او پر سوز شد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هر که با شب همنشین شد یار دید</p></div>
<div class="m2"><p>او چو منصور زمان دیدار دید</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>هر که با شب همنشین شد او ولی است</p></div>
<div class="m2"><p>در میان مؤمنان نور علی است</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>رو تو روز و شب توکّل آر پیش</p></div>
<div class="m2"><p>تا بیابی در حقیقت کام خویش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تو توکّل کن بدرگاه الاه</p></div>
<div class="m2"><p>تا بمانی تو ز شیطان در پناه</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نفس شوم تو بود شیطان تو</p></div>
<div class="m2"><p>هست این خود آیتی در شأن تو</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>رو ز نفس شوم بگذر ای پسر</p></div>
<div class="m2"><p>تا بیابی از همه معنی خبر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>جملهٔ مردان که دین دار آمدند</p></div>
<div class="m2"><p>از هوای نفس بیزار آمدند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>از سر نفس و هوا برخاستند</p></div>
<div class="m2"><p>خانهٔ ایمان خود آراستند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>هرکه از نفس و هوا بیزار شد</p></div>
<div class="m2"><p>او به اولادعلی خود یار شد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>هرکه او از دین بدین محکم ستاد</p></div>
<div class="m2"><p>مهر او در جان انسان اوفتاد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>هرکه رفت او راه ایشان راه یافت</p></div>
<div class="m2"><p>این حقیقت از دل آگاه یافت</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>از وفا گردی تو از اهل صفا</p></div>
<div class="m2"><p>راه ایشان رو اگر داری وفا</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>این وفا خود خاص خاصان خداست</p></div>
<div class="m2"><p>در وفاداری چو عطّاری کجاست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>این وفا جبریل و احمد را بود</p></div>
<div class="m2"><p>یا معانی دان ابجد را بود</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بوی این معنی ز خاک من شنو</p></div>
<div class="m2"><p>از درون چاک چاک من شنو</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>یا شنو از مظهر معجز نما</p></div>
<div class="m2"><p>تا شوی واقف ز اسرار خدا</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>در وفا حبّ علی دارم بدل</p></div>
<div class="m2"><p>گشته حبّ او بجان من سجلّ</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>جوهر ذاتم ز مشکلهای اوست</p></div>
<div class="m2"><p>در درون مظهرم خود جای اوست</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>گر بدانی شد وفای تو درست</p></div>
<div class="m2"><p>ورنه هستی دروفای ما تو سست</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>این وفا هرکس ندارد خارجی است</p></div>
<div class="m2"><p>او و پیرش را بدوزخ ملتجی است</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ناصبی چون خارجی بی‌دین شده</p></div>
<div class="m2"><p>او زسر تا پای خود سرگین شده</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>این جماعت دشمنان حیدرند</p></div>
<div class="m2"><p>پیش ما لایق به تیغ و خنجرند</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>تیغ لعنت بر سر دشمن بزن</p></div>
<div class="m2"><p>تا نباشی پیش مردان کم ز زن</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چون تو در راه وفا ارزنده‌ای</p></div>
<div class="m2"><p>پند ما را یاد گیر ارزنده‌ای</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>پند ما را یاد گیر ای پور تو</p></div>
<div class="m2"><p>دین و دنیا را بکن معمور تو</p></div></div>