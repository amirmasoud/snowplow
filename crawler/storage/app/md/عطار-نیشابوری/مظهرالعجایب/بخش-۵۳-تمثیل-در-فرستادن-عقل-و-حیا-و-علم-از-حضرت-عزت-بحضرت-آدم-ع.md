---
title: >-
    بخش ۵۳ - تمثیل در فرستادن عقل و حیا و علم از حضرت عزت بحضرت آدم (ع)
---
# بخش ۵۳ - تمثیل در فرستادن عقل و حیا و علم از حضرت عزت بحضرت آدم (ع)

<div class="b" id="bn1"><div class="m1"><p>چون ز عزّت خلعت آدم بداد</p></div>
<div class="m2"><p>تاج اسرارش روان بر سر نهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت ای جبریل این سه تحفه را</p></div>
<div class="m2"><p>بر بنزد آدم خاکیّ ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوی کاین سه تحفه از حق آمده</p></div>
<div class="m2"><p>از برای دید مطلق آمده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا بتو باشند خود یار وندیم</p></div>
<div class="m2"><p>هم بتو باشند در معنی مقیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو بایشان باش و با ایشان نشین</p></div>
<div class="m2"><p>تا که حاصل گرددت اسرار دین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پس نظر کرد آدم معنی در آن</p></div>
<div class="m2"><p>دید نور عالم معنی در آن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت آدم با ملایک در ملا</p></div>
<div class="m2"><p>کاین سه جوهر را که آمد از خدا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عقل خواهم تا جدار من شود</p></div>
<div class="m2"><p>علم خواهم در دلم محکم شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خود حیا را جابچشم خویش کرد</p></div>
<div class="m2"><p>او نظر در حرمت او بیش کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>منزلی کردند خود هر یک قبول</p></div>
<div class="m2"><p>شرح این معنی همی داند رسول</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت هر کس علم دارد جان بود</p></div>
<div class="m2"><p>خود حیا یک شعبه از ایمان بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر که او با عقل باشد متّقی است</p></div>
<div class="m2"><p>این شقاوت بیشکی از احمقی است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر کرا با عقل همراهی بود</p></div>
<div class="m2"><p>دیدنش از ماه تا ماهی بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر کرا علم از معانی بوده است</p></div>
<div class="m2"><p>عالم و اسرار دانی بوده است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر کرا عقل و حیا همراه اوست</p></div>
<div class="m2"><p>آدم معنی دل همراه اوست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر که دارد عقل و دین همراه اوست</p></div>
<div class="m2"><p>خود مقام فضل منزلگاه اوست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عقل با علم و حیا چون جمع شد</p></div>
<div class="m2"><p>سینه‌ها روشن از او چون شمع شد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر که دارد عقل این دو پیروند</p></div>
<div class="m2"><p>پس حیا و علم باوی بگروند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر که دارد عقل راه شه رود</p></div>
<div class="m2"><p>نی چو بی‌عقلان درون چه رود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رو تو از بی‌عقل و نادان کن کنار</p></div>
<div class="m2"><p>تا چو حیوان می نباشی در قطار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خود حیا اهل معانی را بود</p></div>
<div class="m2"><p>علم و عقلت از حیا ظاهر شود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جان من دان پرتو انوار اوست</p></div>
<div class="m2"><p>زانکه او را علم معنی یار اوست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عقل با علم و حیا همخانه شد</p></div>
<div class="m2"><p>هم می و میخانه و جانانه شد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از من و میخانه عشق آمد برون</p></div>
<div class="m2"><p>گشت او درملک معنی رهنمون</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گفت با جان که بیا تا برپریم</p></div>
<div class="m2"><p>خرقهٔ تن را سراسر بردریم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خیز تا با هم می معنی خوریم</p></div>
<div class="m2"><p>پس بسوی ملک معنی ره بریم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دل ز باطل پاک کن آنگه درون</p></div>
<div class="m2"><p>تا نیندازندت از خانه برون</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ظاهر و باطن بمعنی پاک ساز</p></div>
<div class="m2"><p>بعد از آن اندر مساجد کن نماز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر نماز پاک خواهی پاک شو</p></div>
<div class="m2"><p>ورنه اندر بند جسمت خاک شو</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گلشن جان را بعشقش پاک دار</p></div>
<div class="m2"><p>تا بروید گل بمعنی صدهزار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شوق ما از حالت مستان بود</p></div>
<div class="m2"><p>جان ما از شوق او نالان بود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سرّ اسرارش نهانی آمده</p></div>
<div class="m2"><p>جوهر ذاتش عیانی آمده</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>صدهزاران راز دارم در دورن</p></div>
<div class="m2"><p>لیک بستم باب معنی از برون</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ای همه مشغول صورت آمده</p></div>
<div class="m2"><p>جملگی محض کدورت آمده</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در نظر غیر خدا را پست کن</p></div>
<div class="m2"><p>دفتر معنی ما را دست کن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تو بخوان و گوش کن اسرار من</p></div>
<div class="m2"><p>تا بیابی کلبهٔ عطّار من</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کلبهٔ عطّار جای عاشقانست</p></div>
<div class="m2"><p>واند او ظاهر سرور عارفانست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اهل صوت نیست اندر منزلم</p></div>
<div class="m2"><p>گفتهٔ ایشان نباشد حاصلم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>من از این صورت برون رفتم تمام</p></div>
<div class="m2"><p>صورت و معنی او دارم مدام</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>من کتاب صورت خود شسته‌ام</p></div>
<div class="m2"><p>چشم صورت بین خود را بسته‌ام</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>علم حال من همه عالم گرفت</p></div>
<div class="m2"><p>بلکه تار و رشته آدم گرفت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>علم من در عرش حوران خوانده‌اند</p></div>
<div class="m2"><p>نی گرفتاران دوران خوانده‌اند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>علم صورت از رهت بیرون برد</p></div>
<div class="m2"><p>علم معنی بر سر گردون برد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>علم صورت معنیت ویران کند</p></div>
<div class="m2"><p>صد هزاران رخنه در ایمان کند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>علم صورت اهل صورت را نکوست</p></div>
<div class="m2"><p>لیک در معنی بغایت نانکوست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>علم معنی در دل خود جای کن</p></div>
<div class="m2"><p>علم صورت را بزیر پای کن</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>علم معنی را بخود همراه بین</p></div>
<div class="m2"><p>علم صورت را ز بهر جاه بین</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>علم معنی عشق رادارد عیان</p></div>
<div class="m2"><p>علم صورت عقل را دارد زیان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>علم معنی عالم جانها گرفت</p></div>
<div class="m2"><p>علم صورت در زمین مأوی گرفت</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>علم معنی خود حیا در چشم داشت</p></div>
<div class="m2"><p>علم صورت تخم جهل و عجب کاشت</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>علم معنی کرد جانم را شکار</p></div>
<div class="m2"><p>تا دهد او را بباز شهریار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>علم معنی آمد و شیطان گریخت</p></div>
<div class="m2"><p>در درون من همه ایمان بریخت</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>علم معنی آمد و عالم گرفت</p></div>
<div class="m2"><p>در حقیقت کشور آدم گرفت</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>علم معنی آمد و جانیم داد</p></div>
<div class="m2"><p>مهر سلطان در درون من نهاد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>علم معنی آمد و گفتار شد</p></div>
<div class="m2"><p>پیش احمد آمد و کردار شد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>علم معنی سرفراز دین ماست</p></div>
<div class="m2"><p>در دو عالم آیهٔ تلقین ماست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>علم معنی با دل من راز گفت</p></div>
<div class="m2"><p>قصّهٔ آدم بیک دم باز گفت</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>علم معنی عشق را در برگرفت</p></div>
<div class="m2"><p>رفت و کیش ساقی کوثر گرفت</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>علم معنی کفر ودین از من ربود</p></div>
<div class="m2"><p>گفت رو این دم بکن حق را سجود</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>علم معنی آمد و احمد شنید</p></div>
<div class="m2"><p>گفت با حیدر نبی در عین دید</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>علم معنی آمد و شرعش گرفت</p></div>
<div class="m2"><p>بعد از آن از اصل و از فرعش گرفت</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>علم معنی با محمّد راز گفت</p></div>
<div class="m2"><p>بعد از آن با شاه مردان بازگفت</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>علم معنی کرد درعالم ظهور</p></div>
<div class="m2"><p>بعداز آن او برد موسی را بطور</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>علم معنی بود اسرار خدا</p></div>
<div class="m2"><p>علم معنی بود انوار هدی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>علم معنی مصطفی را شرع داد</p></div>
<div class="m2"><p>بعد از آن با مفتی ما فرع داد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>علم معنی با علی همراه بود</p></div>
<div class="m2"><p>خود نبیّ الله از آن آگاه بود</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>علم معنی را رسول الله دید</p></div>
<div class="m2"><p>او علی را اندر آن همراه دید</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>علم معنی را که این عطّار گفت</p></div>
<div class="m2"><p>جملگی از گفتهٔ کرّار گفت</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>علم معنی کاف و ها یا عین و صاد</p></div>
<div class="m2"><p>هست در معنی بقرآنت گشاد</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>علم معنی در طریقت راست بود</p></div>
<div class="m2"><p>در نهان سرّ حقیقت را نمود</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>علم معنی در دلم معنی شکافت</p></div>
<div class="m2"><p>بعد از آن در مظهر انسان بتافت</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>علم معنی را زمعنیها بپرس</p></div>
<div class="m2"><p>در حقیقت روز از نادا بپرس</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>علم معنی خود کلام الله خواند</p></div>
<div class="m2"><p>بر زبان ذکر ولیّ الله خواند</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>علم معنی گشت در بازار عشق</p></div>
<div class="m2"><p>یافت اوسر رشتهٔ اسرار عشق</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>علم معنی راه هدایت کار کن</p></div>
<div class="m2"><p>خیز ورو خود را باو تو یار کن</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>علم معنی پادشاه علم بود</p></div>
<div class="m2"><p>ز آن فقیر بینوا را حلم بود</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>علم معنی کاروان سرّ غیب</p></div>
<div class="m2"><p>دامن من چاک کرده تا به جیب</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>علم معنی را بدل عطّار یافت</p></div>
<div class="m2"><p>زان معانی گوهر اسرار یافت</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>علم معنی را شریعت خانه‌ای</p></div>
<div class="m2"><p>غیر آن خانه همه ویرانه‌ای</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>علم معنی خانهٔ دلها گرفت</p></div>
<div class="m2"><p>واندر آنجا منزل و مأوی گرفت</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>علم معنی گوش کرده جبرئیل</p></div>
<div class="m2"><p>هست گفتار نبیّ الله دلیل</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>علم معنی قاف تا قاف آمده</p></div>
<div class="m2"><p>ز آن همه معنی می صاف آمده</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>علم معنی در درونم زد علم</p></div>
<div class="m2"><p>ز آن سبب برهم زنم لوح و قلم</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>علم معنی بود اسرار نهفت</p></div>
<div class="m2"><p>شاه مردانش درون چاه گفت</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>علم معنی نی شد و آواز کرد</p></div>
<div class="m2"><p>اهل معنی را بخود همراز کرد</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>علم معنی گشت بانی همنشین</p></div>
<div class="m2"><p>این زمان گفتار او در من ببین</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>علم معنی را ندانستی چه بود</p></div>
<div class="m2"><p>خویش را بر باد دادی همچو دود</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>علم معنی با علی گفتا نبی</p></div>
<div class="m2"><p>این چنین اسرار کی داند ولی</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>علم معنی مرتضا(ع) را جام داد</p></div>
<div class="m2"><p>بعد از آن در راه او آرام داد</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>علم معنی با علی اسرار گفت</p></div>
<div class="m2"><p>بعد از آنش حیدر کرّار گفت</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>علم معنی با علی (ع) همدم شده</p></div>
<div class="m2"><p>در میان جان و دل محرم شده</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>علم معنی پیش او خود روشن است</p></div>
<div class="m2"><p>بعد از آن در جان عاشق روزن است</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>علم معنی را ز مظهر پرس و رو</p></div>
<div class="m2"><p>و آنگهی اسرار ربّانی شنو</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>علم معنی را ز مظهر گوش کن</p></div>
<div class="m2"><p>بعد از آن چون جوهری در گوش کن</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>علم معنی را عبادتخانه‌ایست</p></div>
<div class="m2"><p>واندر آن خانه خدا را دانه‌ایست</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>علم معنی را محبّت دانه شد</p></div>
<div class="m2"><p>بعد از آنش آدمی همخانه شد</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>علم معنی گفتگو دارد بسی</p></div>
<div class="m2"><p>مثل این مظهر ندارد خود کسی</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>علم معنی رو بخود همراه کن</p></div>
<div class="m2"><p>بعد از آنی جان ودل آگاه کن</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>علم معنی گفتگو دارد بسی</p></div>
<div class="m2"><p>خود نخوانده مثل این مظهر کسی</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>علم معنی مهدیم دارد بغیب</p></div>
<div class="m2"><p>زآنکه آن شه سرّها دارد بجیب</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>علم معنی داشت حیدر در یقین</p></div>
<div class="m2"><p>زآنکه او بد مظهر اسرار دین</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>علم معنی دان تو علم اوّلین</p></div>
<div class="m2"><p>هم باو ختم است علم آخرین</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>علم معنی دان تو باب اولیا</p></div>
<div class="m2"><p>زآنکه او بوده است نفس مصطفی</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>علم معنی دان که معنی روح تست</p></div>
<div class="m2"><p>شهسوار لو کشف خود نوح تست</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>علم معنی مهدی من شد بعلم</p></div>
<div class="m2"><p>هست از مهدی مرا خود علم و حلم</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>علم معنی مهدیم دارد ز غیب</p></div>
<div class="m2"><p>زانکه آن شه سرّها دارد به جیب</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>علم معنی دان و ترک جاه کن</p></div>
<div class="m2"><p>خیز و فکر توشهٔ این راه کن</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>علم معنی دان و سرگردان مشو</p></div>
<div class="m2"><p>همچو کوران جهان ترسان مشو</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>علم معنی دان و راه حق برو</p></div>
<div class="m2"><p>و از ولیّ الله کلام حق شنو</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>علم معنی دان چو شاه لو کشف</p></div>
<div class="m2"><p>تا شود بر تو حقایق منکشف</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>علم معنی دان و از صوری گذر</p></div>
<div class="m2"><p>تا خلاصی یابی از نار سقر</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>علم معنی دان و از بد کن حذر</p></div>
<div class="m2"><p>زآنکه ایندنیا ندارد ره بدر</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>علم معنی دان وخارج را مبین</p></div>
<div class="m2"><p>گر همی خواهی که بایش پاک دین</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>علم معنی دان و از صورت گذر</p></div>
<div class="m2"><p>زانکه صورت بین شده خود در بدر</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>علم معنی دان و معنی فاش کن</p></div>
<div class="m2"><p>همچو منصوری که گفته است این سخن</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>علم معنی دان و از خود کن حذر</p></div>
<div class="m2"><p>زآنکه خود بین را نباشد ثمر</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>علم معنی دان که معنی سهل نیست</p></div>
<div class="m2"><p>همچو صوری او همه بر جهل نیست</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>علم معنی دان و راه شرع رو</p></div>
<div class="m2"><p>تابری از جمله اهل دین گرو</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>علم معنی دان بحکم مرتضی</p></div>
<div class="m2"><p>گر همیخواهی که باشی باصفا</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>علم معنی دان زجعفر در جهان</p></div>
<div class="m2"><p>زآنکه با او بوده علم حق عیان</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>علم معنی دان وصادق را شناس</p></div>
<div class="m2"><p>پس ز علم او بنه در دین اساس</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>علم معنی دان و خاک راه باش</p></div>
<div class="m2"><p>تو محبّ و دوستدار شاه باش</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>علم معنی دان و خود را تو مدان</p></div>
<div class="m2"><p>زانکه این دانش ترا دارد زیان</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>علم معنی دان و حق در خویش بین</p></div>
<div class="m2"><p>زآنکه این معنی ندارد خویش بین</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>علم معنی دان و عقل از حال گیر</p></div>
<div class="m2"><p>وانگهی گفت مرا زو فال گیر</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>علم معنی دان و چون عطار باش</p></div>
<div class="m2"><p>در میان چشم دل دیدار باش</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>علم معنی دان و چون خورشید شو</p></div>
<div class="m2"><p>پس برو در ملک او جمشید شو</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>علم معنی دان و رفض او مبین</p></div>
<div class="m2"><p>زآنکه دارد دُرّ حق را در نگین</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>علم معنی دان به نورم در سخن</p></div>
<div class="m2"><p>این معانی خود زجوهر فهم کن</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>علم معنی دان وفتوی گوش کن</p></div>
<div class="m2"><p>حبّ او باشد مرا خود بیخ دین</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>هر که دارد حبّ او ایمان برد</p></div>
<div class="m2"><p>ورنه ایمانش همه شیطان برد</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>رو تو حبّش در درون دل بکار</p></div>
<div class="m2"><p>تا درخت نور بینی بی شمار</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>رو تو حبّش دار و صیقل زن دلت</p></div>
<div class="m2"><p>تا نروید خار غفلت ازگلت</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>راه او را جو اگر مرتد نه‌ای</p></div>
<div class="m2"><p>همچو مفتی زمان تو رد نه‌ای</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>ازمنافق دور باش او را مبین</p></div>
<div class="m2"><p>گر همی خواهی که باشی پاکدین</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>رو تو شهبازی بمعنی پر برآر</p></div>
<div class="m2"><p>ورنه باشی در دو عالم خوار و زار</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>ای پسر تو روح را شهباز کن</p></div>
<div class="m2"><p>نه مثال خرمگس پرواز کن</p></div></div>