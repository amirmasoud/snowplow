---
title: >-
    بخش ۸۰ - تنبیه در آنکه از غیرببری  و بخود روی آوری تا درحجاب نمیری
---
# بخش ۸۰ - تنبیه در آنکه از غیرببری  و بخود روی آوری تا درحجاب نمیری

<div class="b" id="bn1"><div class="m1"><p>بُد به نیشابور مرد منعمی</p></div>
<div class="m2"><p>بود او را خانهٔ پردرهمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تاجران بسیار در ملک جهان</p></div>
<div class="m2"><p>بهر نفع مال میکردی روان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مزرعه در ملک ما بسیار داشت</p></div>
<div class="m2"><p>تخم بی‌صبری در او بسیار کاشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خانه‌ها و جایها بسیار ساخت</p></div>
<div class="m2"><p>خود چه حاصل چون کسی را کم نواخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز و شب فکرش خیال جاه بود</p></div>
<div class="m2"><p>دستش ازنیکی ولی کوتاه بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناگهم افتاد در کویش گذر</p></div>
<div class="m2"><p>بود چون فرزند او بیرون در</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم او افتاده بر من گفت آه</p></div>
<div class="m2"><p>آمدی خوش ورنه می‌گشتم تباه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از جفای دورو ازدرد پدر</p></div>
<div class="m2"><p>این زمان افتاده از خود بیخبر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این توقع دارم از لطف تو من</p></div>
<div class="m2"><p>پیش او آیی و گوئی یک سخن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مدت ده روز شد تاخسته است</p></div>
<div class="m2"><p>او زاکل و شرب لب بربسته است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر که آید در عیادت پیش او</p></div>
<div class="m2"><p>غیر فحش از وی نیامد گفتگو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در نصیحت نکته‌ای با او بگوی</p></div>
<div class="m2"><p>تا نریزد او ازین فحش آب روی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون برفتم پیش او بی‌گفتگو</p></div>
<div class="m2"><p>در مقام کندن جان بود او</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون نظر افتاد او را بر فقیر</p></div>
<div class="m2"><p>گفت ای عطّار ما را دستگیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفتمش دم با خدا باید زدن</p></div>
<div class="m2"><p>خود از این دنیا بدر باید شدن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفت ای عطّار رفتن مشکل است</p></div>
<div class="m2"><p>زآنکه حبّ این جهانم در دل است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>این چنین در روی من بسیار گفت</p></div>
<div class="m2"><p>وآن همه از هستی و پندار گفت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>من زبالینش روان برخاستم</p></div>
<div class="m2"><p>هر زمان از بیم آن میکاستم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون باو گفتم بسی گوی از خدا</p></div>
<div class="m2"><p>یا ببر پیشم تو نام مصطفی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>او ز مال و جاه خود میگفت قال</p></div>
<div class="m2"><p>خود نبود از یاد حقش ذوق و حال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جان همی کند و همی گفت این سخن</p></div>
<div class="m2"><p>غیر این معنی نبودش هیچ فن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ناگهی درویشی آمد پیش من</p></div>
<div class="m2"><p>گفت از حال غنی برگو سخن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گفتمش ای دوست او جان می‌کند</p></div>
<div class="m2"><p>خویشتن را او بزندان میکند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون شنید این قصه از من پیر راه</p></div>
<div class="m2"><p>خندهٔ او کرد از شکر الاه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گفت او هفتاد سال ای اهل دل</p></div>
<div class="m2"><p>درجهان کنده است جانی متصل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>او بعمر خویشتن جان کنده است</p></div>
<div class="m2"><p>این زمان در پیش شیطان مانده است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای برادر حال دنیا دار بین</p></div>
<div class="m2"><p>چون درون نار گشته زار بین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای برادر از جهان بیزار باش</p></div>
<div class="m2"><p>دایماً با ذکر حق درکارباش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هرکه دنیا دار شد مردود شد</p></div>
<div class="m2"><p>همچو هیمه در میان دود شد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هر که دنیا دار شد بی ما بود</p></div>
<div class="m2"><p>در دو عالم بیشک او رسوا بود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هر که دنیا دار شد غمخوار شد</p></div>
<div class="m2"><p>او ز دنیائی خود بیمار شد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هر که دنیا دار شد او مرده‌ایست</p></div>
<div class="m2"><p>او به خواری در جهان افسرده ایست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هر که دنیا دار شد او یار نیست</p></div>
<div class="m2"><p>در دو عالم خود ازو آثار نیست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هر که دنیا دار شد او را مبین</p></div>
<div class="m2"><p>تا نیندازد ترا او بر زمین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هر که دنیا دار شد ترسان بود</p></div>
<div class="m2"><p>پیشوای او همه شیطان بود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هر که دنیا دار شد آلوده شد</p></div>
<div class="m2"><p>او بکفگر جهان پالوده شد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هر که دنیا دار شد لذت نیافت</p></div>
<div class="m2"><p>او به پیش عارفان همّت نیافت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هر که دنیا دار شد عقبی ندید</p></div>
<div class="m2"><p>او ثمر از خوشهٔ طوبا نچید</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هر که دنیا دار شد از ما گذشت</p></div>
<div class="m2"><p>دارد او با اهل دنیا خود نشست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هر که دنیا دار شد ای وای او</p></div>
<div class="m2"><p>خود مرا رحم است بر فردای او</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هر که دنیا دار شد خود را بسوخت</p></div>
<div class="m2"><p>یا به تیری از بلا خود را بدوخت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هر که دنیا دار شد بیمار شد</p></div>
<div class="m2"><p>او برون از کلبهٔ عطّار شد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هر که دنیا دار شد زیر زمین</p></div>
<div class="m2"><p>خود ورا شیطان ملعون درکمین</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هر که دنیا دار شد او گیج شد</p></div>
<div class="m2"><p>همچو مال خویشتن او هیچ شد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هر که دنیا دار شد از ما برید</p></div>
<div class="m2"><p>در معانی مظهر ما را ندید</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>هر که دنیا دار شد سودا پزد</p></div>
<div class="m2"><p>مار دنیا دایمش بر پا گزد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>هر که دنیا دار شد آخر چه کرد</p></div>
<div class="m2"><p>او ز دنیا رفت با صد آه و درد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>هر که دنیا دار شد مرگش گرفت</p></div>
<div class="m2"><p>هرکه عقبا دار شد ترکش گرفت</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>هر که دنیا دار شد اهل گل است</p></div>
<div class="m2"><p>هرکه عقبی دار شد اهل دلست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>هر که دنیا دار شد کی راه دید</p></div>
<div class="m2"><p>خویشتن را عاقبت درچاه دید</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هر که دنیا دار شد کفتار شد</p></div>
<div class="m2"><p>او درون غار بسته خوار شد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>هر که دنیا دار شد او کور شد</p></div>
<div class="m2"><p>در میان مفلسان عور شد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>هر که دنیا دار شد کی آدمی است</p></div>
<div class="m2"><p>کی ورا در علم معنی خرّمیست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>هر که دنیا دار شد کی عشق دید</p></div>
<div class="m2"><p>مظهر عطّار را او کی شنید</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>هر که دنیا دار شد مظهر نیافت</p></div>
<div class="m2"><p>او ز جوهر ذات من جوهر نیافت</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>هر که دنیا دار شد عطّار نیست</p></div>
<div class="m2"><p>در معانی واقف اسرار نیست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>هر که دنیا دار شد در زحمت است</p></div>
<div class="m2"><p>هرکه از پیشش رود در رحمت است</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>هر که دنیا دار شد ویران شود</p></div>
<div class="m2"><p>یامثال خواجهٔ دیوان شود</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>هر که دنیا دار شد او منصبی است</p></div>
<div class="m2"><p>او در آن صورت بمعنی عقربیست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>هر که دنیا دار شد دکّان گرفت</p></div>
<div class="m2"><p>نه برفت و علّم القرآن گرفت</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>هر که دنیا دار شد دنیا گرفت</p></div>
<div class="m2"><p>خویش را در پیش شیطان جا گرفت</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>هر که دنیا دار شد فاسق بود</p></div>
<div class="m2"><p>کی چودرویشان دین عاشق بود</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>هر که دنیا دار شد در نار سوخت</p></div>
<div class="m2"><p>او زبهر جیفهٔ دینار سوخت</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>هر که دنیا دار شد او جان کند</p></div>
<div class="m2"><p>از تن خود جامهٔ ایمان کند</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>هر که دنیا دار شد خود بین شده</p></div>
<div class="m2"><p>پای تا سر جملگی سرگین شده</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>هر که دنیا دار شد سنگین دلست</p></div>
<div class="m2"><p>همچو خر دایم فتاده در گل است</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>هر که دنیا دار شد در راه ماند</p></div>
<div class="m2"><p>پای بسته دردرون چاه ماند</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>هر که دنیا دار شد دانی چه کرد</p></div>
<div class="m2"><p>او ز دنیا رفت با صد آه و درد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>هر که دنیا دار شد دیندار نیست</p></div>
<div class="m2"><p>او درون کلبهٔ عطّار نیست</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>در گذر از جیفهٔدنیای دون</p></div>
<div class="m2"><p>تا برآری از صدف گوهر برون</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>گوهر معنی بیان انبیاست</p></div>
<div class="m2"><p>جوهر معنی زبان اولیاست</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>در معانی کوش نی در جاه و مال</p></div>
<div class="m2"><p>زآنکه جاه و مال را باشد زوال</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>مال دنیا از حقت دوری دهد</p></div>
<div class="m2"><p>پس ترا از کفر رنجوری دهد</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>درگذر از منصب دنیای دون</p></div>
<div class="m2"><p>زانکه خلقی را دراندازد بخون</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بگذر از دنیا و جام عشق نوش</p></div>
<div class="m2"><p>همچو مستان خدامیکن خروش</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>گر تو خواهی پیش آن دلجو شوی</p></div>
<div class="m2"><p>بایدت اولّ که همچون او شوی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>یعنی از هستی خود از دل گذر</p></div>
<div class="m2"><p>وآنگهی بیخود بسویش راه بر</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چون درآئی خویش را گم کن دراو</p></div>
<div class="m2"><p>تا بیابی خویش را پهلوی او</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>هر که دارد این ادب مقبل بود</p></div>
<div class="m2"><p>او بمقبولان حق واصل بود</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>هرکه دارد این ادب مظهر گرفت</p></div>
<div class="m2"><p>جام راحت از کف حیدر گرفت</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>خویش رادر زندگانی فوت بین</p></div>
<div class="m2"><p>این معانی را تو پیش از موت بین</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>تا بمانی زنده درملک الاه</p></div>
<div class="m2"><p>خود بعلّیّینت باشد تکیه گاه</p></div></div>