---
title: >-
    بخش ۲۲ - تنبیه حال گرفتاران دنیا و شیرین نمودن ایشان بطلب مولی
---
# بخش ۲۲ - تنبیه حال گرفتاران دنیا و شیرین نمودن ایشان بطلب مولی

<div class="b" id="bn1"><div class="m1"><p>ای تو در زندان دنیا همچو سگ</p></div>
<div class="m2"><p>میدوی تا آهوئی گیری به تک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جست آهو و تو افتادی بچاه</p></div>
<div class="m2"><p>چارهٔ چَه را چه خواهی کرد آه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای تو گم کرده چه سگ آن راه را</p></div>
<div class="m2"><p>چشم بر آهو ندیده چاه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترک صید آهوی دنیا بکن</p></div>
<div class="m2"><p>خویشتن را همچو سگ رسوا مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تانیفتی همچو سگ در چاه تن</p></div>
<div class="m2"><p>نفس شومت را برون کن از بدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هست این عالم مثال گلخنی</p></div>
<div class="m2"><p>اندر او عارف بسان گلشنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای به گلخن میل کرده از خری</p></div>
<div class="m2"><p>ره به گلزار معانی کی بری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جمله خلقان را بدان چون گلخنی</p></div>
<div class="m2"><p>خورده از حمّامی تن گردنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وی تو در قید عیال و تن شده</p></div>
<div class="m2"><p>بهر نان وابستهٔگلخن شده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رو تو ترک این همه کن همچو من</p></div>
<div class="m2"><p>خود مشو محبوس اندر چاه تن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همچو مردان از خودی آزاد شو</p></div>
<div class="m2"><p>در طریق اهل معنی شاد شو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از برای تو بیارم مظهری</p></div>
<div class="m2"><p>وز دل دریا برآرم گوهری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مظهرم میدان تو گوهر گوش دار</p></div>
<div class="m2"><p>تا بیابی در معنی بی شمار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از یقین درها بگوش خویش کن</p></div>
<div class="m2"><p>وآنگهی یاد من درویش کن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خود دعائی کن مرا ای مرد خاص</p></div>
<div class="m2"><p>تاکه گردد روح من از غم خلاص</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>قدر مظهر را چه دانند ابلهان</p></div>
<div class="m2"><p>قیمت گوهرمجوی از گمرهان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قیمت گوهر به پیش گوهریست</p></div>
<div class="m2"><p>صاحب مظهر عجایب گوهریست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خواجه گوید سرّ مظهر گوش کن</p></div>
<div class="m2"><p>جامی از مظهر بگیر و نوش کن</p></div></div>