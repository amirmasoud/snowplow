---
title: >-
    بخش ۲۰ - در بیان حال و منع آنهائی که اهل شرند واز خود بیخبرند و دیگران را احتساب فرمایند
---
# بخش ۲۰ - در بیان حال و منع آنهائی که اهل شرند واز خود بیخبرند و دیگران را احتساب فرمایند

<div class="b" id="bn1"><div class="m1"><p>بوی سرگین در دماغت هست چست</p></div>
<div class="m2"><p>محتسب گشتی که دینم شد درست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز مانی احتساب خویش کن</p></div>
<div class="m2"><p>ترک کردار و کتاب خویش کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرکنی تو همچو بهمان احتساب</p></div>
<div class="m2"><p>بر سرت آید عذاب بی‌حساب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برگذر زین کار و از آزار خلق</p></div>
<div class="m2"><p>ورنه چون دزدان بیاویزی بحلق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دزد دنیا خود متاعی برده بود</p></div>
<div class="m2"><p>یا زمال اهل دنیا خورده بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو کنی رخنه بدین مصطفی</p></div>
<div class="m2"><p>هیچ شرمی می نداری از خدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو کنی دلهای مردم را ملول</p></div>
<div class="m2"><p>می نداری شرم از روح رسول</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر نباشد جمله کار تو ریا</p></div>
<div class="m2"><p>در ره این فش از کجا و تو کجا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای ترا افعال زشت و خلق هم</p></div>
<div class="m2"><p>از تو حق گشته ملول و خلق هم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای تو با این فسق و دستار بلند</p></div>
<div class="m2"><p>در میان خلق گشته خود پسند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای گرفته سبحه از بهر ریا</p></div>
<div class="m2"><p>از ریا بگذر تو وبا راه آ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چند گردی بهر آزار کسان</p></div>
<div class="m2"><p>شرم دار از خالق هر دو جهان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دل بدست آر و مجو آزار دل</p></div>
<div class="m2"><p>ز آنکه باشدمخزن اسرار دل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خود نکوتر باشد از صد کعبه دل</p></div>
<div class="m2"><p>ساز دل نیکوتر است از ساز گل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دل بود منزلگه اسرار غیب</p></div>
<div class="m2"><p>گر نمی‌دانی تو راخود نیست عیب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عیب من آنست که گفتم راست را</p></div>
<div class="m2"><p>بشنو ازمن خود یکی درخواست را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ترک آزار دل دانا بکن</p></div>
<div class="m2"><p>تا نیفتی چون درخت از بیخ وبن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر که آزار دل دانا کند</p></div>
<div class="m2"><p>در دو عالم خویش را رسوا کند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رو مجو آزار دلها بی‌گناه</p></div>
<div class="m2"><p>ورنه باشی در دو عالم رو سیاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جهد کن دلهای ایشان شاد ساز</p></div>
<div class="m2"><p>تا شود درهای جنّت بر تو باز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>رو تو بی‌منّت بدستت آر دل</p></div>
<div class="m2"><p>ز آنکه از منّت بسی باشد خجل</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر که یک دل را بیازارد چو جان</p></div>
<div class="m2"><p>جمله دلها را بیازارد عیان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>این چنین کس از بدیها بدتر است</p></div>
<div class="m2"><p>بلکه او خود در جهان چون کافر است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چند گویم من بتو ای هیچکس</p></div>
<div class="m2"><p>هیچ کردی خویش را همچو مگس</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ترک کن افعال بد را نیک شو</p></div>
<div class="m2"><p>بر طریق صالحان نیک رو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>من چگویم باتو تو خود هیچ کس</p></div>
<div class="m2"><p>در میان خلق گشتی خرمگس</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای که آزردی دل عطار را</p></div>
<div class="m2"><p>من بتوکی گویم این اسرار را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>این همه اسرار از دل آمده</p></div>
<div class="m2"><p>باتو گفتن راز مشکل آمده</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بعد من گر خوانی این مظهر تمام</p></div>
<div class="m2"><p>زینهارش تو نگهدار از عوام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بود این مظهر چو جوهر ذات بود</p></div>
<div class="m2"><p>وین معانی از صفات ذات بود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>رو تو جوهر خوان شو و جوهرشناس</p></div>
<div class="m2"><p>تا بیابی علم معنی بی قیاس</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>رو تو جوهر دان و مظهر نیز هم</p></div>
<div class="m2"><p>تا نگردی در معانی متّهم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مظهر وجوهر هم از گنج وی است</p></div>
<div class="m2"><p>خود بدست ابلهان رنج وی است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از برای روح احمد جوهرم</p></div>
<div class="m2"><p>و از برای نور حیدر مظهرم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نیک دان و نیکخوان و گوش کن</p></div>
<div class="m2"><p>تا که روشن گرددت سرّ کهن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در جهان بسیار معنا گفته‌اند</p></div>
<div class="m2"><p>درّ اسرار معانی سفته‌اند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از زمان مصطفا تا این زمان</p></div>
<div class="m2"><p>واز زمان آدم آخر زمان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از ولی و شیخ و شاعر تا نجوم</p></div>
<div class="m2"><p>کس ندانسته چو عطّار این علوم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هست او شاگرد حیدر بی‌شکی</p></div>
<div class="m2"><p>تو چه میدانی از اینها خود یکی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نیست چون عطّار مرغی در جهان</p></div>
<div class="m2"><p>زانکه هست او بلبل این بوستان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هیچ میدانی که این دادم ز کیست</p></div>
<div class="m2"><p>وین همه افغان و فریادم ز کیست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بهر آن است تا بدانی خویش را</p></div>
<div class="m2"><p>چند برخود میزنی تو نیش را</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خویش را و نیش را بشناس تو</p></div>
<div class="m2"><p>تا شود کارت چو حال من نکو</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خویش تو پیراست باراه آردت</p></div>
<div class="m2"><p>نیش تو کفر است گمراه آردت</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گر نیابی پیر جوهر پیش آر</p></div>
<div class="m2"><p>وانگهی مظهر چو جان خویش دار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چند گوئی تو به نااهلان سخن</p></div>
<div class="m2"><p>دم نگهدار ومعانی ختم کن</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تانگویندت توئی اهل حلول</p></div>
<div class="m2"><p>یا توئی همچون روافض بوالفضول</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>یا نه دین ناصبی بربوده‌ای</p></div>
<div class="m2"><p>یا نه تو همچون خوارج بوده‌ای</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>یا بگویند اتّحادی بوده‌ای</p></div>
<div class="m2"><p>یا تو کیش ملحدان بربوده‌ای</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گر نگویم راست اینها نشنوم</p></div>
<div class="m2"><p>من بدین مصطفی آسوده‌ام</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هرچه گویندم کنمشان منبحل</p></div>
<div class="m2"><p>ز آنکه دارم مهر شاهی را بدل</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>آنچه او گفتا بگو من گفته‌ام</p></div>
<div class="m2"><p>من بگفت دیگران کی رفته‌ام</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گفت دیگر ابلهان قیل است وقال</p></div>
<div class="m2"><p>گفت شاه اولیا حالست حال</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>قال را در درس مان و حال گیر</p></div>
<div class="m2"><p>تا شوی واصل تودرعرفان پیر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>پیر تو شاهست دیگر پیر نیست</p></div>
<div class="m2"><p>در دو عالم همچو او یک میر نیست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>نور او از نور احمد تافته</p></div>
<div class="m2"><p>حق بدست قدرتش بشکافته</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>سرّ ایشان کس نداند جز الاه</p></div>
<div class="m2"><p>این سخن روشن شد از ماهی بماه</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>قصد من بسیار مردم کرده‌اند</p></div>
<div class="m2"><p>خاطر مسکین من آزرده‌اند</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>جور بسیار از جهان بر من رسید</p></div>
<div class="m2"><p>جور دنیا راه همی باید کشید</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ناصرخسرو ز سرّ آگاه بود</p></div>
<div class="m2"><p>نه چو تو او مرتد و گمراه بود</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ناصر خسرو که اندوهی گرفت</p></div>
<div class="m2"><p>رفت و منزل در سر کوهی گرفت</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ناصر خسرو بحق پی برده بود</p></div>
<div class="m2"><p>از میان خلق بیرون رفته بود</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>یار او یک غار بود و تار بود</p></div>
<div class="m2"><p>او بنور و نار حق در کار بود</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>رو تو در کار خدامردانه باش</p></div>
<div class="m2"><p>وز وجود خویشتن بیگانه باش</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>تا ببینی مظهر سلطان عشق</p></div>
<div class="m2"><p>وانمائی در جهان برهان عشق</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>عشق چبود قبلهٔ سلطان دل</p></div>
<div class="m2"><p>عشق چبود کعبهٔ میدان دل</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>عشق چبود مقصد ومقصود تو</p></div>
<div class="m2"><p>عشق باشد عابد و معبود تو</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>عشق دارد درجهان دیوانه‌ها</p></div>
<div class="m2"><p>عشق کرده خانمان ویرانه‌ها</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>عشق باشد تاج جمله اولیا</p></div>
<div class="m2"><p>عشق گفته بامحمّد انّما</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>عشق گفته با محمّد در شهود</p></div>
<div class="m2"><p>در نهان و آشکارا هرچه بود</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>عشق گفته با محمّد راز خود</p></div>
<div class="m2"><p>هم از او بشنیده خود و آواز خود</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>عشق گفته آنچه پنهانی بود</p></div>
<div class="m2"><p>عشق گفته آنچه سبحانی بود</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>عشق گفته راز پنهانی بما</p></div>
<div class="m2"><p>رو بگو عطّار آن را برملا</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>عشق گفته رو بگو اسرار من</p></div>
<div class="m2"><p>خود مترسان خویش را ازدار من</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>عشق گفتا من شدم همراه تو</p></div>
<div class="m2"><p>عشق گفتا من شدم خود شاه تو</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>عشق گفتا من بتو ایمان دهم</p></div>
<div class="m2"><p>بعد از آنی در معانی جان دهم</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>عشق گفتا شرع تعلیمت کنم</p></div>
<div class="m2"><p>در طریق عشق تعظیمت کنم</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>عشق گفتا خود حقیقت آن ماست</p></div>
<div class="m2"><p>وین معانی و بیان در شأن ماست</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>عشق گوید جملهٔ عالم منم</p></div>
<div class="m2"><p>در میان جان و تن محرم منم</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>عشق گوید من بجمله انبیا</p></div>
<div class="m2"><p>گفته‌ام راز نهانی بر ملا</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>عشق گوید اولیا شاگرد من</p></div>
<div class="m2"><p>خواندن درس معانی ورد من</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>عشق گوید همنشین تو شدم</p></div>
<div class="m2"><p>درس و تکرار و معین تو شدم</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>عشق گوید غافلی از حال من</p></div>
<div class="m2"><p>از بد ونیک و ازین افعال من</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>عشق گوید فعل من نیکست و نیک</p></div>
<div class="m2"><p>واندر این دریا نهانم همچو ریگ</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>عشق گوید تو برو بیهوش شو</p></div>
<div class="m2"><p>پیش عشق او چو من پرجوش شو</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>عشق گوید غافلی از یار من</p></div>
<div class="m2"><p>گوش کن یک لحظه از اسرار من</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>عشق گوید گر ز من غافل شدی</p></div>
<div class="m2"><p>خود یقین میدان که بی‌حاصل شدی</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>عشق می‌گوید منم دریای راز</p></div>
<div class="m2"><p>با توحاضر بوده‌ام من در نماز</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>عشق گوید که مراخود یاد کن</p></div>
<div class="m2"><p>وین دل غمگین من تو شاد کن</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>عشق گوید رو ز شیطان دور شو</p></div>
<div class="m2"><p>وانگهی چون جان جانان نور شو</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>عشق گوید رو بدین شه گرو</p></div>
<div class="m2"><p>وانگهی اسرار حق از شه شنو</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>عشق گوید که همو مقصود بود</p></div>
<div class="m2"><p>با محمّد حامد ومحمود بود</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>عشق گوید گر بدانی شاه را</p></div>
<div class="m2"><p>همچو خورشیدی ببینی ماه را</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>عشق گوید راه او راه من است</p></div>
<div class="m2"><p>همچو عطّاری که آگاه من است</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>عشق گوید من بعالم آمدم</p></div>
<div class="m2"><p>از برای دید آدم آمدم</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>عشق گوید گه نهانم گه عیان</p></div>
<div class="m2"><p>من بجسم تو درآیم همچو جان</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>عشق گوید گر تو می‌خواهی مرا</p></div>
<div class="m2"><p>رو بپوشان جامهٔ شاهی مرا</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>عشق گوید که لسان غیب من</p></div>
<div class="m2"><p>این کتب را گفته‌ام بی عیب من</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>عشق گوید که بسی اسرارها</p></div>
<div class="m2"><p>من دراین مظهر بگفتم بارها</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>عشق می‌گوید که این راز من است</p></div>
<div class="m2"><p>بر سردست شهان باز من است</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>عشق می‌گوید که با حق راز من</p></div>
<div class="m2"><p>از برون و از درون آواز من</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>عشق می‌گوید همه حیوان بدند</p></div>
<div class="m2"><p>یک یکی در راه او انسان شدند</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>عشق می‌گوید که سلطانی کنم</p></div>
<div class="m2"><p>باشه خود سرّ پنهانی کنم</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>عشق می‌گوید که دیدم رازها</p></div>
<div class="m2"><p>مرغ معنی کرده است پروازها</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>عشق می‌گوید مدار حق منم</p></div>
<div class="m2"><p>در معانی پود و تار حق منم</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>عشق می‌گوید نبی بر حق شتافت</p></div>
<div class="m2"><p>زان بقرب حضرت اوراه یافت</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>عشق می‌گوید ولی بر من گذشت</p></div>
<div class="m2"><p>تیر مهر او ز جان و تن گذشت</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>عشق می‌گوید علیٌ بابها</p></div>
<div class="m2"><p>روزها گویم بتو زین بابها</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>عشق می‌گوید که بابم را شناس</p></div>
<div class="m2"><p>وین معانی را بمظهر کن قیاس</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>عشق گوید چند می‌گویم بتو</p></div>
<div class="m2"><p>سرّ اسرار نهانی تو بتو</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>عشق می‌گوید علی را می‌شناس</p></div>
<div class="m2"><p>این معانی بشنو و میدار پاس</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>عشق می‌گوید علی چون روح بود</p></div>
<div class="m2"><p>خود بدریای معانی نوح بود</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>عشق می‌گوید علی با حق چه گفت</p></div>
<div class="m2"><p>هرچه گفته بود او آخر شنفت</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>عشق می‌گوید که ای گم کرده راه</p></div>
<div class="m2"><p>می‌طلب از شاه مردان تو پناه</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>عشق می‌گوید که ایمان نیستت</p></div>
<div class="m2"><p>ز آنکه مهر شاه مردان نیستت</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>عشق می‌گوید که شاهم اولیاست</p></div>
<div class="m2"><p>با محمّد نور او در انّماست</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>عشق می‌گوید که علم اوّلین</p></div>
<div class="m2"><p>پیش سلطان جهان باشد یقین</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>عشق می‌گوید که حق بیزار شد</p></div>
<div class="m2"><p>از کسی کو از یکی با چار شد</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>عشق می‌گوید که ایمان چار نیست</p></div>
<div class="m2"><p>در درون خود یکی دان چار نیست</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>عشق می‌گوید که جز یک یار نیست</p></div>
<div class="m2"><p>جز یکی اندر جهان دیّار نیست</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>یار را یک دان نه یک را چار دان</p></div>
<div class="m2"><p>تا شوی در ملک جان اسراردان</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>گفتگو بگذار مذهب خود یکی است</p></div>
<div class="m2"><p>گر ندانی یک در ایمانت شکی است</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>تو براه شرع احمد رو چو من</p></div>
<div class="m2"><p>تا شوی در ملک معنی بی سخن</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>من لسان الغیب دارم در زبان</p></div>
<div class="m2"><p>زان لسان الغیب خوانندم عیان</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>تو لسان الغیب را نشنیده‌ای</p></div>
<div class="m2"><p>ز آن طریق جاهلان بگزیده‌ای</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>رو براه مظهر و مظهر بخوان</p></div>
<div class="m2"><p>تاشوی درمظهر من راز دان</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>مظهر و جوهر از این دریا بود</p></div>
<div class="m2"><p>گه نهان گشته گهی پیدا بود</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>ای نهان و آشکارا جمله تو</p></div>
<div class="m2"><p>در عیان مرد دانا جمله تو</p></div></div>