---
title: >-
    بخش ۱۶ - ***
---
# بخش ۱۶ - ***

<div class="b" id="bn1"><div class="m1"><p>این سخن نقلست از سلطان دین</p></div>
<div class="m2"><p>از امام متقیین ایمان دین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن امامی کو حقیقت یاب بود</p></div>
<div class="m2"><p>در میان بحر دین گرداب بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اسم او خواهی که دانی ز اولیا</p></div>
<div class="m2"><p>هست نام او علی موسی الرضا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن امامی کو طریق دید حق</p></div>
<div class="m2"><p>جمله اهل الله را داده سبق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن امامی کو بغیر از حق ندید</p></div>
<div class="m2"><p>عالمی انوار از او آمد پدید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت تو خواهی که ایمانت بود</p></div>
<div class="m2"><p>انس و جنّ جمله بفرمانت بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو ز دین مصطفی جاهل مباش</p></div>
<div class="m2"><p>در طریق مرتضی غافل مباش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در ره دین ذکر حق را کن نثار</p></div>
<div class="m2"><p>تخم حبّ مرتضی در دل بکار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هست ذکر حق حصار و شرط آن</p></div>
<div class="m2"><p>حبّ آل مصطفی باشد بدان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت پیغمبر حدیثی بر ملا</p></div>
<div class="m2"><p>هست این معنی خود از پیش خدا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رو تو از عطّار پرس اسرار او</p></div>
<div class="m2"><p>ز آنکه دارد مظهر انوار او</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من بتو اسرارگویم پایدار</p></div>
<div class="m2"><p>گر تو منصوری سخن را پاسدار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای زانوارت جهان روشن شده</p></div>
<div class="m2"><p>قرص خور، شمعی از آن روزن شده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چند گویم من بتو اسرار را</p></div>
<div class="m2"><p>خود ز کل نشناختی انوار را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هست از نور خدا روشن دلم</p></div>
<div class="m2"><p>حلّ شده از نور حیدر مشکلم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گشته روشن این ضمیر پاک من</p></div>
<div class="m2"><p>شد زیارت گاه مردان خاک من</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز آنکه من عطّار ثانی آمدم</p></div>
<div class="m2"><p>وز وجود خویش فانی آمدم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خود مرا مولد به نیشابور بود</p></div>
<div class="m2"><p>لیک اصل من بکوه طور بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>طور چبود مظهر اسرار او</p></div>
<div class="m2"><p>نور چبود واصل انوار او</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نور طور خود در اودیدم عیان</p></div>
<div class="m2"><p>گر تو می‌بینی بیا نزدیک مان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز آنکه چون منصور واصل آمدیم</p></div>
<div class="m2"><p>نی چو زرّاقان جاهل آمدیم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بیعت ما بیعتی باشد نخست</p></div>
<div class="m2"><p>گشته این بیعت بدین ما درست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دین خود را می‌کنم من آشکار</p></div>
<div class="m2"><p>گر برندم این زمان در پای دار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دین من دین امیرالمؤمنین</p></div>
<div class="m2"><p>راه من راه امام المتقین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ما بدین حیدری داریم رو</p></div>
<div class="m2"><p>یک جهت باشیم ما در دین او</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو زدین لفظی برآری برزبان</p></div>
<div class="m2"><p>خودنمی‌دانی معانی را عیان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>رو، ز قرآن مغز گیر و پوست مان</p></div>
<div class="m2"><p>پوست را انداز پیش کرکسان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>روغن این مغز جان اولیاست</p></div>
<div class="m2"><p>این چنین معنی بیان اولیاست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>رو، ز قرآن صورت و معنی ببین</p></div>
<div class="m2"><p>تا شود روشن ترا دنیا و دین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خود نمی‌دانی که قرآن نطق راست</p></div>
<div class="m2"><p>ناطق او را نمی‌دانی کجاست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ناطق او خود امیرمؤمنان</p></div>
<div class="m2"><p>در کلام الله نطق او بیان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>او بود قرآن ناطق در یقین</p></div>
<div class="m2"><p>زانکه او گفته است نطقم را ببین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ناطق من خود محمّد بود شاه</p></div>
<div class="m2"><p>رو تو واقف شو ز اسرار اله</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جمله اسرار خدا آموختم</p></div>
<div class="m2"><p>جامه از انّا عطینا دوختم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر هزاران سال باشی در طلب</p></div>
<div class="m2"><p>ور هزاران جام گیری تا به لب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ور بهر روزی گزاری صد نماز</p></div>
<div class="m2"><p>ور شوی با روزه در عمری دراز</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گر شوی غزّالی طوسی به دهر</p></div>
<div class="m2"><p>ور برون آری بسی درها ز بحر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گر اویس خاص باشی مصطفا</p></div>
<div class="m2"><p>ور حسن گردی به سیرت با صفا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ور چو مالک تو نهٔ دینار جو</p></div>
<div class="m2"><p>چون محمد واسعی تو یار جو</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گر تو باشی همچو ایشان درروش</p></div>
<div class="m2"><p>ور بیابی در طریقت پرورش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ور حبیب اعجمی باشی بحال</p></div>
<div class="m2"><p>ور چو بوخالد شوی در عمروسال</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ور شوی تو همچو عتبه ذکر گوی</p></div>
<div class="m2"><p>ور بیابی تو در آن سیر آبروی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ور تو همچون رابعه باشی خموش</p></div>
<div class="m2"><p>ور فضیلی خود بعالم در خروش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گر چو ابراهیم ادهم در جهان</p></div>
<div class="m2"><p>ور چو بشر حافی آیی راز دان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گر شوی ذوالنون مصری پرمحن</p></div>
<div class="m2"><p>بایزیدی گر شوی بسطام فنّ</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ور چو عبدالله مبارک آمدی</p></div>
<div class="m2"><p>ور چو لقمان نور تارک آمدی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گر شوی داود طائی با وفا</p></div>
<div class="m2"><p>ور چو حارث شد جنابت باصفا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ور سلیمانی و دارائی بدرد</p></div>
<div class="m2"><p>ور محمد این سمّاکی تو فرد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گر محمد اسلم و اعلم شوی</p></div>
<div class="m2"><p>احمد حرب اندرین عالم شوی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گر چو حاتم کو اصم بدعالمی</p></div>
<div class="m2"><p>ور ابوسهلی و در دین مکرمی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گر شوی معروف کرخی در کرم</p></div>
<div class="m2"><p>ور چو سرّی سقطی گردی تو هم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گر شوی تو همچو فتح موصلی</p></div>
<div class="m2"><p>ور شوی چون احمد حواری ولی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گر چو سلطان احمد خضرویه راه</p></div>
<div class="m2"><p>یابی و گردی بملک فقر شاه</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>یا بگردی بوتراب نخشبی</p></div>
<div class="m2"><p>یا شوی تو همچو شیخ مغربی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>یا چو یحیی معاذو شه شجاع</p></div>
<div class="m2"><p>کین دوشه کردند عالم را وداع</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گر چو یوسف بن حسین راز دان</p></div>
<div class="m2"><p>باشی و عبدالله حیری روان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>یا تو چون بوحفص حدادی شوی</p></div>
<div class="m2"><p>از علوم دین دل آبادی شوی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>یا تو چون حمدون قصاری شوی</p></div>
<div class="m2"><p>یا تو چون منصور عمّاری شوی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گر شوی چو احمد عاصم به علم</p></div>
<div class="m2"><p>ور شوی همچون جنید محترم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>عمر و عبدالله مکّی گر شوی</p></div>
<div class="m2"><p>بر همه مردان عالم سرشوی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>گر تو چون خراز باشی سرّ پوش</p></div>
<div class="m2"><p>چون حسین نوری آیی در خروش</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>یا ابوعثمان حیری در حرم</p></div>
<div class="m2"><p>در طریق عشق باشی محترم</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چون محمد گر بود اسمش رویم</p></div>
<div class="m2"><p>بر سر ارباب عرفان بود غیم</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>گر شوی ابن عطا در کار حق</p></div>
<div class="m2"><p>ور چو ابراهیم رقی یار حق</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>یوسف اسباط یا یعقوب پیر</p></div>
<div class="m2"><p>نهر جوری آنکه بود او بی‌نظیر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چون محمد کو حکیم سرمدی است</p></div>
<div class="m2"><p>آنکه او سرور بملک بیخودیست</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بوالحسن آن شیخ بوشنجی شوی</p></div>
<div class="m2"><p>یا تو چون ورّاق راه دین روی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>گر چو بوحمزه خراسانی شوی</p></div>
<div class="m2"><p>ور براه حق بآسانی شوی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ور شوی عبدالله ابن الجلا</p></div>
<div class="m2"><p>ور تو باشی چون علیّ مرحبا</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>جملگی کردند کار راه حق</p></div>
<div class="m2"><p>تو بری در معرفت ز آنها سبق</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>احمد مسروق اگر باشی بدهر</p></div>
<div class="m2"><p>ور شوی سمنون مجنون نورشهر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ور شوی در رتبه چون شیخ کبیر</p></div>
<div class="m2"><p>در میان اهل عرفان بی نظیر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ور چو بواسحق گردی کاردان</p></div>
<div class="m2"><p>بو محمد مرتعش را همزبان</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ور تو منصوری و حلاج اسم تست</p></div>
<div class="m2"><p>جمله انوار خدا در جسم تست</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>همچو فضل ار صاحب سیری شوی</p></div>
<div class="m2"><p>بوسعید بن ابوالخیری شوی</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ور چو شیخ مغربی گردی عیان</p></div>
<div class="m2"><p>چون ابوالقاسم شوی شیخ کلان</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>گر شوی تو همچو نجم الدین ما</p></div>
<div class="m2"><p>از تو گیرد عالمی نور و صفا</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ور چو سیف الدین و مجدالدین شوی</p></div>
<div class="m2"><p>چون علی لالا توهم ره بین شوی</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ور هزاران سال تو شیخی کنی</p></div>
<div class="m2"><p>ور شوی در ملک عرفان تو غنی</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>گر کتبهای سماوی بشنوی</p></div>
<div class="m2"><p>ور تو عمری در ره عرفان شوی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>راه یک دان نه دو باشد راه حق</p></div>
<div class="m2"><p>این سخن را گوش کن از شاه حق</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>این جماعت جمله از خورد و کلان</p></div>
<div class="m2"><p>راه بین باشند و جمله راه دان</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>راه این جمله یقین میدان یکیست</p></div>
<div class="m2"><p>کور باشد آنکه رادر این شکیست</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>بود اینها را مسلّم راه شرع</p></div>
<div class="m2"><p>باخبر بودند جمله اصل و فرع</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>همچو ایشان باش در دین پایدار</p></div>
<div class="m2"><p>تخم ایمان در زمین دل بکار</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>تخم ایمان را بعالم زرع دان</p></div>
<div class="m2"><p>تا که گردد سیر ایمانت عیان</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>چونکه گردد سبز باز آرد ثمر</p></div>
<div class="m2"><p>رو تو این بررا چو جان خود شمر</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>بعد از آن جان را بجانان وصل کن</p></div>
<div class="m2"><p>دست و رو از جمله دینها غسل کن</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>گرچه مردم دین بسی دارند لیک</p></div>
<div class="m2"><p>تو نمی‌دانی که این دین نیست نیک</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>راه دانانی که بر حقّ رفته‌اند</p></div>
<div class="m2"><p>راه حقّ را راست مطلق رفته‌اند</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>جمله یک دینند پیش شاه خود</p></div>
<div class="m2"><p>چون بدانستند ایشان راه خود</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>ای تو گم کرده ز ایمان راه را</p></div>
<div class="m2"><p>روشناس آخر چو ایشان شاه را</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>جمله دانند این جماعت شاه را</p></div>
<div class="m2"><p>گم نکردند از حقیقت راه را</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>هر که در راه ولایت انور است</p></div>
<div class="m2"><p>او بشهر دین احمد چون دراست</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>هر که در راه علی ره دان شده</p></div>
<div class="m2"><p>در میان جان ما ایمان شده</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>هر که در راه علی از جان گذشت</p></div>
<div class="m2"><p>تیر او از هفتمین ایمان گذشت</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>هر که در راه علی دارد قدم</p></div>
<div class="m2"><p>هست در دار بهشت او محترم</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>گر تو مردی سرّ شاه از من شنو</p></div>
<div class="m2"><p>مظهر حقّ را بدان با او گرو</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>هست عطّار این زمان خود حیدری</p></div>
<div class="m2"><p>یافته در دین حیدر سروری</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>هست عطار این زمان با شه درست</p></div>
<div class="m2"><p>دامن او گیر ای طالب تو چست</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>ز آنکه همچون او نداری رهبری</p></div>
<div class="m2"><p>رهبر عطّار آمد سروری</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>سرور مردان عالم شاه ماست</p></div>
<div class="m2"><p>در حقیقت دید او همراه ماست</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>من بدیدم دید او در خویشتن</p></div>
<div class="m2"><p>ز آن بنالم همچو بلبل در چمن</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>بلیل طبعم از او گویا شد</p></div>
<div class="m2"><p>چشم دید من از او بینا شده</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>عالمی روشن شده از نور او</p></div>
<div class="m2"><p> و آنکه هست انسان کامل پور او</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>هر که راه او رود فرزند اوست</p></div>
<div class="m2"><p>رشتهٔ جانهای ما پیوند اوست</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>گمره است آنکس که غیر او بود</p></div>
<div class="m2"><p>وز خدا دور است آنکو بشنود</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>بشنود هر کس بجان این را ز ما</p></div>
<div class="m2"><p>در جهان جان شود انبازما</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>زو شنیدم نطق و نطقم او بداد</p></div>
<div class="m2"><p>این همه اسرار در جانم گشاد</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>این چنین مظهر همه از غیب دان</p></div>
<div class="m2"><p>بعد از این عطّار گشته غیب دان</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>در میان جان من او بوده است</p></div>
<div class="m2"><p>خود همو گفته همو بشنو ده است</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>من چه گویم من چه دانم من که‌ام</p></div>
<div class="m2"><p>در شنیدن در سخن گفتن که‌ام</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>هست او گویا چو نور اندر تنم</p></div>
<div class="m2"><p>کز زبان او حکایت می‌کنم</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>این سخنها را روایت می‌کنم</p></div>
<div class="m2"><p>خلق عالم را هدایت می‌کنم</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>من ازو گویم ازو دانم از او</p></div>
<div class="m2"><p>می‌کنم دایم ز مظهر گفتگو</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>بعد از این گویم حقایق بیشمار</p></div>
<div class="m2"><p>گر تو ره دانی بسویم گوشدار</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>من معانی با تو گویم بیشمار</p></div>
<div class="m2"><p>شمّه‌ای را ز آن معانی گوشدار</p></div></div>