---
title: >-
    بخش ۴۵ - قال النبی صلی الله علیه و آله و سلم: «التعظیم لامر الله والشفقة علی خلق الله»
---
# بخش ۴۵ - قال النبی صلی الله علیه و آله و سلم: «التعظیم لامر الله والشفقة علی خلق الله»

<div class="b" id="bn1"><div class="m1"><p>تا که ایمانت شود محکم از او</p></div>
<div class="m2"><p>مهر او میدار در جانت نکو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان خود آمیز با مهرش نکو</p></div>
<div class="m2"><p>تا درآید در میان جانت او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیگری آن کز میان خلق رو</p></div>
<div class="m2"><p>گوشه‌ای گیر ودرون دلق رو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون بیابی سرّ ما سرپوش باش</p></div>
<div class="m2"><p>در میان عاشقان می نوش باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رو چو عطّار و قناعت پیشه کن</p></div>
<div class="m2"><p>در میان مظهرم اندیشه کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زاد راهت هرچه باشد غیر ازین</p></div>
<div class="m2"><p>دشمنان باشند و دارندت کمین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای برادر اهل دنیا را مبین</p></div>
<div class="m2"><p>ز آنکه ایشانند گمراهان دین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو بهردرویش عارف باش یار</p></div>
<div class="m2"><p>گاه گاهی جوهر را پیش آر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون ببینی جوهر ذاتم چنان</p></div>
<div class="m2"><p>اندر آیی در میان سالکان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درّ ز بحر دل در آرم بیشمار</p></div>
<div class="m2"><p>گر تو می‌جوئیش رو جوهر بیار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر دری زین گوشوار عالمی است</p></div>
<div class="m2"><p>هر که این راگوش دارد آدمی است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فکر وذکر خویش را صافی بساز</p></div>
<div class="m2"><p>جهد فرما آنگهی اندر نماز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بعد از آنی روزه‌دار از کلّ نفس</p></div>
<div class="m2"><p>ز آنکه باشد روزهٔ تو غلّ نفس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از طعام بد بپرهیز ای پسر</p></div>
<div class="m2"><p>همچو دد کم باش خونریز ای پسر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نفس را از روزه اندر بنددار</p></div>
<div class="m2"><p>مر ورا نز لقمهٔ خورسند دار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روزه‌ای میدار چون مردان مرد</p></div>
<div class="m2"><p>نفس خود را از همه میدار فرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نی همین از اکل او را باز دار</p></div>
<div class="m2"><p>بلکه نگذارش بفکر هیچ کار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رو تو کُش نفس و مگردانش تو سیر</p></div>
<div class="m2"><p>وانگهی بر خود مگردانش دلیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ذکر حقّ باشد تمامی کار او</p></div>
<div class="m2"><p>ورنه از خوردن نباشد عار او</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در میان اهل معنی کن حضور</p></div>
<div class="m2"><p>ز آنکه ایشانند چون دریای نور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>رو تو از مردان دین غافل مباش</p></div>
<div class="m2"><p>ز آنکه ایشانند ما را خواجه تاش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر تو اهل فضل را نشناختی</p></div>
<div class="m2"><p>دین و دنیا را به یک جو باختی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>رومعانی دان شو و اسرار خوان</p></div>
<div class="m2"><p>تا شوی در ملک معنی جان جان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نقطهٔ باب ولایت را طلب</p></div>
<div class="m2"><p>و آنگهی از وی هدایت را طلب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر نبایستی بعالم راهبر</p></div>
<div class="m2"><p>کی فرستادی رسول با خبر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>انبیا را اولیا باشد وصی</p></div>
<div class="m2"><p>اولیا را اصفیا باشد صفی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اولیا و انبیا لطف حقّ‌اند</p></div>
<div class="m2"><p>در حقیقت جمله حقّ مطلق‌اند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>انبیا را خود ولی باید مبین</p></div>
<div class="m2"><p>تا بگوید علم معنی را یقین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر تو بی ایشان روی راه ای پسر</p></div>
<div class="m2"><p>از حقیقت خود کجا یابی خبر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر نیابی تو ولی را در جهان</p></div>
<div class="m2"><p>رو ز مظهر جوی تا گوید عیان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هر چه ایشان گفته‌اند آن را شنو</p></div>
<div class="m2"><p>هرچه کردند ای پسر با آن گرو</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا شوی در ملک معنی مقتدا</p></div>
<div class="m2"><p>خیز و برخوان ربّ انصرنی علی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا رسی بر آنچه مقصودت بود</p></div>
<div class="m2"><p>خود بیابی آنچه مطلوبت بود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بهره کی یابی ولی زین کار تو</p></div>
<div class="m2"><p>زآنکه میجوئی بسی اسرار تو</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هرکه آزار کسی دارد بدل</p></div>
<div class="m2"><p>پیش مردان باشد اودایم خجل</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جانت از مهر علی آباد کن</p></div>
<div class="m2"><p>خاطرت از بار غم آزاد کن</p></div></div>