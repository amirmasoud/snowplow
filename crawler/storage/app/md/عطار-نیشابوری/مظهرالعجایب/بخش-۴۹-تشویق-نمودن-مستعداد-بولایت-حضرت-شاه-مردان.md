---
title: >-
    بخش ۴۹ - تشویق نمودن مستعداد بولایت حضرت شاه مردان
---
# بخش ۴۹ - تشویق نمودن مستعداد بولایت حضرت شاه مردان

<div class="b" id="bn1"><div class="m1"><p>چون بدین مصطفی همره شوی</p></div>
<div class="m2"><p>از طریق مرتضی آگه شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچه گفتار کلام است و حدیث</p></div>
<div class="m2"><p>گوش کن مشنو سخن از هر خبیث</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رو توبیعت کن باولاد رسول</p></div>
<div class="m2"><p>تا کند الله ایمانت قبول</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرچه فرمایند میکن تو بجان</p></div>
<div class="m2"><p>تو بجان کن آنچه گویندت عیان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خود ورای رأی ایشان راه نیست</p></div>
<div class="m2"><p>گر روی ره غیر آن جز چاه نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چاه چبود چاه خسران چاه ویل</p></div>
<div class="m2"><p>گفتمت حرفی ببینش چون سهیل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن سهیلی کز یمن بر هر که تافت</p></div>
<div class="m2"><p>از شعاعش بوی دید ور نگ یافت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بوی و رنگ از حبّ آل مصطفی است</p></div>
<div class="m2"><p>هر که دید او سرخ روی دو سراست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرکرا چیزی بخاطر خوش بود</p></div>
<div class="m2"><p>دفتری سازد که ظاهر خوش بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رو تو غیر از راستی چیزی مگو</p></div>
<div class="m2"><p>زآنکه باشد این بعالم خود نکو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر که او دفتر بقلاّبی کشید</p></div>
<div class="m2"><p>عاقبت خود را بخلاّبی کشید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای برادر خط بقلاّبی بکش</p></div>
<div class="m2"><p>پیش قلّابان فکن این غلّ و غش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عالمی از دست خط گمره شدند</p></div>
<div class="m2"><p>جمع دیگر بر خطوط شه شدند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خطّ شه با خطّ احمد جمع کن</p></div>
<div class="m2"><p>بعد از آن چون نور ایمان شمع کن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هست قلّابی خلاف دین همه</p></div>
<div class="m2"><p>چیست چندین رسم و این آئیین همه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رسم و آئین را گذار و راست باش</p></div>
<div class="m2"><p>باش یک روی و مکن این راز فاش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>راست قول مصطفی و مرتضی است</p></div>
<div class="m2"><p>غیر این هر کس که کرد او برنخاست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>راه احمد راه حق دان بی‌گزاف</p></div>
<div class="m2"><p>راه دوزخ دان ره اهل خلاف</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خط کلام است و حدیث است و ورع</p></div>
<div class="m2"><p>نیست حاصل دیگران را جز جزع</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هیچ میدانی که در عالم چه شد</p></div>
<div class="m2"><p>این همه بدعت بعالم از که شد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از کسی کو راه حق پوشید و رفت</p></div>
<div class="m2"><p>آستان دوزخ او بوسید و رفت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>رو تو بی‌حکم خدا کاری مکن</p></div>
<div class="m2"><p>خویش را درضدّ چو مرداری مکن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر که از دین نبی بیزار شد</p></div>
<div class="m2"><p>او نجس گردید چون مردار شد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هرکه او در راه دین تقصیر کرد</p></div>
<div class="m2"><p>خویشتن را درجوانی پیر کرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هرکه او آید بهمراهی ما</p></div>
<div class="m2"><p>جای او باشد بهشت باصفا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر که با او یار شد او یار دید</p></div>
<div class="m2"><p>کور شد آنکه ورا اغیار دید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هرچه در عالم بظاهر حاضر است</p></div>
<div class="m2"><p>تو یقین میدان که فانی آخر است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو بظاهر نیک باش و نیک رو</p></div>
<div class="m2"><p>تا بباطن تو شوی معنی شنو</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>رو مقام بیخودی راگوشه کن</p></div>
<div class="m2"><p>وآنگهی گفتار ما را توشه کن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>غیر ایشان نیست هادی ای عزیز</p></div>
<div class="m2"><p>گر تو بینی ناکسی و بی تمیز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تو کناره گیر از شهر بدان</p></div>
<div class="m2"><p>رو بصحرا آر و خود را وارهان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هست صحرا و ادئی بس با حضور</p></div>
<div class="m2"><p>دیدهٔ اغیار از آنجا مانده دور</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هست صحرا جای امن و باصفا</p></div>
<div class="m2"><p>هر که آمد رفت از اهل وفا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هست صحرا آنکه گل روید از او</p></div>
<div class="m2"><p>اهل معنی نکته‌ها گوید از او</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>غیر را آنجانباشد هیچ راه</p></div>
<div class="m2"><p>خار نبوددر میان آن گیاه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چون برون آیی تو از شهر بدن</p></div>
<div class="m2"><p>اندر آن صحرا روی بی‌خویشتن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بوی حبّ مرتضی مستت کند</p></div>
<div class="m2"><p>در بهشت عدن پابستت کند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خانه و شهر بدن ویران کنی</p></div>
<div class="m2"><p>همچو گل جا در میان جان کنی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>رو تو نیکو باش و هرجا باش باش</p></div>
<div class="m2"><p>زآنکه این معنی نباشد بی‌بلاش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>رو بلای آسمانی را بخر</p></div>
<div class="m2"><p>تا که یابی از بلای او ثمر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گر بلا ازوی بود نیکو بود</p></div>
<div class="m2"><p>خود بلای تو همه از تو بود</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هر بلا کز وی بیاید خوش بود</p></div>
<div class="m2"><p>بر سر تو خود بلای تو بود</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>رو مکن با اهل حق جنگ و نزاع</p></div>
<div class="m2"><p>گر کنی حشر تو باشد باسباع</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>حیف باشد خود که شیطان در جهان</p></div>
<div class="m2"><p>خود تووسواسی شوی با این و آن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>رو تو از وسواس شیطان دور باش</p></div>
<div class="m2"><p>تا ببینی با نبی در یک قباش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گر بصورت دردومظهر جلوه کرد</p></div>
<div class="m2"><p>مظهر ما در دو عالم هست فرد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>هست بینا آنکه راه حق رود</p></div>
<div class="m2"><p>کور گردد آنکه اوبردق رود</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>رو بحجّت کار کن با شاه حق</p></div>
<div class="m2"><p>زآنکه اعمی را نباشد راه حق</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>راه حق از معصیت گردد خراب</p></div>
<div class="m2"><p>من زجور تو دلی دارم کباب</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ساختی یک خانه را هفتاد در</p></div>
<div class="m2"><p>سر بسر ازدین احمد بیخبر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>باب یک دانم بگفت مصطفی</p></div>
<div class="m2"><p>مصطفا گفتا علیٌ بابها</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>خود برآ از باب او درعلم حق</p></div>
<div class="m2"><p>تا بری از جمله صدّیقان سبق</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>رو ازین در تو بشهر مصطفا</p></div>
<div class="m2"><p>تا ببینی جنّت و فردوس را</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چونکه جنّت خواستی با حق گرو</p></div>
<div class="m2"><p>رو تو فتّاح علیم از حق شنو</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ز آنکه حق دانا ز سرّ خلق شد</p></div>
<div class="m2"><p>در درون جبّه و هر دلق شد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>من درون جبّه دیدم شاه را</p></div>
<div class="m2"><p>از درون یابم بسویش راه را</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>گر بدین و مذهبش تونگروی</p></div>
<div class="m2"><p>در حقیقت مرتد و ملعون شوی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>مذهب غیر از دلت بیرون دوان</p></div>
<div class="m2"><p>در دلت نهری ز ایمان کن روان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>مذهب شه را بدان و راه جو</p></div>
<div class="m2"><p>تا که باشد علم شرع تو نکو</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>من براه اهل ملّت رفته‌ام</p></div>
<div class="m2"><p>بر همه اطوار سنّت رفته‌ام</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>سنّت پیغمبر و ملّت یکیست</p></div>
<div class="m2"><p>راه حیدر را در این خود کی شکیست</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>هست مهر شاه مردانب ر دلم</p></div>
<div class="m2"><p>قرنها این بُد سرشته در گلم</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>تا که گفت آنشاه من با من سخن</p></div>
<div class="m2"><p>عیب من در این سخنها تو مکن</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>آنچه او گفته است من خود آن کنم</p></div>
<div class="m2"><p>غیر دینش را همه ویران کنم</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>تو زدین او بکن یک خانه‌ای</p></div>
<div class="m2"><p>واندر آنجا جای ده جانانه‌ای</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>مایهٔ تو گنج حبّ او بود</p></div>
<div class="m2"><p>در دو عالم مایهٔ نیکو بود</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>گنج و مایهٔ حبّ او باشد ترا</p></div>
<div class="m2"><p>چون نداری گنج گردی بینوا</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>خانهٔ تو خانهٔ شیطان بود</p></div>
<div class="m2"><p>پیش تو دیو لعین رحمان بود</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>جامده در خانه بغض و کینه را</p></div>
<div class="m2"><p>تیره از ظلمت مساز آئینه را</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>هرکه بر دین شه مردان برفت</p></div>
<div class="m2"><p>از جهان میدان که با ایمان برفت</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>خانهٔ دل را ز غیرت پاک کن</p></div>
<div class="m2"><p>وآنگهی رو جان دشمن چاک کن</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>جان دشمن چاک کردم زین سخن</p></div>
<div class="m2"><p>زآنکه دشمن را نباشد بیخ و بن</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>من سخن ازدانش او گفته‌ام</p></div>
<div class="m2"><p>وز عطایش درّ معنی سفته‌ام</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ور نه از عطّار کی آید سخن</p></div>
<div class="m2"><p>این معانی را بدان و فهم کن</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>در درون خود آتش شوقش بود</p></div>
<div class="m2"><p>در میان جان من ذوقش بود</p></div></div>