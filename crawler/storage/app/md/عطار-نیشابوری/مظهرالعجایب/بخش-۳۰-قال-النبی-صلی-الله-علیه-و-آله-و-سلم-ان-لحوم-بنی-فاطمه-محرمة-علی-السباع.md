---
title: >-
    بخش ۳۰ - قال النبی صلی الله علیه و آله و سلم: «ان لحوم بنی فاطمه محرمة علی السباع»
---
# بخش ۳۰ - قال النبی صلی الله علیه و آله و سلم: «ان لحوم بنی فاطمه محرمة علی السباع»

<div class="b" id="bn1"><div class="m1"><p>هر که در اصل از نبی دارد مقام</p></div>
<div class="m2"><p>بر درنده گوشتش آمد حرام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آل پیغمبر بقول آن امام</p></div>
<div class="m2"><p>باشد ایمن از سباع و از هوام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس خلیفه گفت این دم می‌رویم</p></div>
<div class="m2"><p>تا حدیث مصطفی را بکرویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اندر این جا خانهٔ پر شیر هست</p></div>
<div class="m2"><p>پیش شیران می‌کنیم این دم نشست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خلق بغداد از یکی تا صدهزار</p></div>
<div class="m2"><p>جمله رفتند از عقبشان بیشمار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن امام دین ابا خلق آن زمان</p></div>
<div class="m2"><p>شد بسوی خانهٔ شیران روان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت کذّابه که تو خود پیش رو</p></div>
<div class="m2"><p>زانکه هستی پیشوا و پیش رو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رفت شاه و پیش آن شیران رسید</p></div>
<div class="m2"><p>گفت امروز است ما را روز عید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چونکه بوی آدمی بشنید شیر</p></div>
<div class="m2"><p>جمله برجستند از جاشان دلیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چونکه چشم شیر چشم شاه دید</p></div>
<div class="m2"><p>گفت چشمم این زمان الله دید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بود او نور خدا و مصطفی</p></div>
<div class="m2"><p>خلق عالم بر ولای او گوا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همچو گربه پیش شه غلطان شدند</p></div>
<div class="m2"><p>خلق بغداد اندر آن حیران شدند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پیش شیران رفت شاه دلنواز</p></div>
<div class="m2"><p>در میانشان کرد دو رکعت نماز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شیری آمد با دو چشم آبناک</p></div>
<div class="m2"><p>روی خود مالید نزد شه بخاک</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ناله‌ها می‌کرد و عرض حال گفت</p></div>
<div class="m2"><p>گفت نبود راز من از تو نهفت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پیرم و دندان ندارم این زمان</p></div>
<div class="m2"><p>طعمه‌ام را می‌خورند این دیگران</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>امر فرما که مرا این دم جدا</p></div>
<div class="m2"><p>طعمه بخشند این گروه باجفا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>با خلیفه گفت سیّد حال را</p></div>
<div class="m2"><p>مردمان کردند فرمانش روا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زینب ملعونه را در پیش خواند</p></div>
<div class="m2"><p>او ز بیم زخم شیر از دور ماند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون بدیدند آن چنانش مردمان</p></div>
<div class="m2"><p>پیش شیرانش کشیدند آن زمان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زن چو اندر چنگ شیران افتاد</p></div>
<div class="m2"><p>پیش شیران دور از جان افتاد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پاره‌اش کردند و بیجان ساختند</p></div>
<div class="m2"><p>پس بخاکش زود یکسان ساختند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خوش ز هم کندند شیران بلا</p></div>
<div class="m2"><p>زینب ملعونهٔ کذّابه را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بعد از آن شیران همه پیش امام</p></div>
<div class="m2"><p>روی مالیدند برره ز احترام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>با زبان حال می‌گفتند ما</p></div>
<div class="m2"><p>گربه‌ها باشیم از شیر خدا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نقد شیر حق و شاه ذوالفقار</p></div>
<div class="m2"><p>تو زما بی حرمتیها درگذار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مدح جدّ و مادر و باب شما</p></div>
<div class="m2"><p>کرده نقش الله بر ارض و سما</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شد شما را ای همه فرخندگان</p></div>
<div class="m2"><p>جنّ و انس از کمترین بندگان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای که دایم لاف ایمان می‌زنی</p></div>
<div class="m2"><p>با ولای او دم از جان میزنی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در اطاعت روز و شب بیدار باش</p></div>
<div class="m2"><p>با ولای حیدر کرار باش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>حب ایشان را بجان خویش دار</p></div>
<div class="m2"><p>تا بیابی علم معنی بیشمار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>رو تو حبّ شاه مردان کن بدل</p></div>
<div class="m2"><p>تا نگردی همچو مردودان خجل</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>رو تو مدح شاه را میکن نهان</p></div>
<div class="m2"><p>تا شوی از جملهٔ انسانیان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر تو حبش را خریدار آمدی</p></div>
<div class="m2"><p>از همه خوابی تو بیدار آمدی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>رو تو مهرش دار و با ایشان نشین</p></div>
<div class="m2"><p>تا شوی ایمن ز شیران عرین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>رو تو حبّش دار چون من در جهان</p></div>
<div class="m2"><p>تا خلاصی یابی از شرّ این زمان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>رو تو حبّش ورز چون سلمان فارس</p></div>
<div class="m2"><p>تا نیابی بیم از شیران فارس</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>رو تو حبّش دار چون محبوب اوست</p></div>
<div class="m2"><p>در جهان جان همه مطلوب اوست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>رو تومهرش دار و با او یار باش</p></div>
<div class="m2"><p>وز همه خلق جهان بیزار باش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>خلق چون دور از ره ایشان روند</p></div>
<div class="m2"><p>جملگی پی بر پی غولان روند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هرکه حبش چون رضا در جان نهاد</p></div>
<div class="m2"><p>حق تعلای سرّ اعیانش بداد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>رو تو حبش را یقین در جان بنه</p></div>
<div class="m2"><p>تا شوی مقبول خاص و عام و که</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هر که بر حبّ رضا داده رضا</p></div>
<div class="m2"><p>جنت و فردوس را گشت اوسزا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>در درون سینه‌ای یار عزیز</p></div>
<div class="m2"><p>غیر حبّ او ندارم هیچ چیز</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ای ز نادانی همه خود بین شده</p></div>
<div class="m2"><p>راه حق گم کرده و بی‌دین شده</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>حبّ ایشان نور حق باشد ترا</p></div>
<div class="m2"><p>نور حق را در دل خود ده تو جا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تا خلاصی یابی از شیران بغض</p></div>
<div class="m2"><p>ورنه باشی تیره و حیران بغض</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بغض حیدر دین و ایمانت برد</p></div>
<div class="m2"><p>سوی قعر دوزخ آسانت برد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بغض در عالم ترا ویران کند</p></div>
<div class="m2"><p>همچو روبه طعمهٔ شیران کند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>هر کرا بغض علی در جان بود</p></div>
<div class="m2"><p>هرگزش کی بهره از ایمان بود</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>خلق عالم جمله گمراه آمدند</p></div>
<div class="m2"><p>ز آنکه بغضش را هواخواه آمدند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تو تولّادار با حبّش درست</p></div>
<div class="m2"><p>کن تبرّا تو زبغضش از نخست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>هر که خود رادشمن آن یار دید</p></div>
<div class="m2"><p>چشم نابینای او خود چار دید</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چار دیدن عکس شیطانی بود</p></div>
<div class="m2"><p>دیدن حق راه رحمانی بود</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>هر که او غیر از یکی در کار دید</p></div>
<div class="m2"><p>هرکجا دید او همه اغیار دید</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گر تو نقل از مصطفی داری بیا</p></div>
<div class="m2"><p>غیر یک مذهب کجا باشد روا</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>مصطفی گفتا که راه راست رو</p></div>
<div class="m2"><p>از دوئی بگذر بیکتائی گرو</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>هست ذات حق تعالی خود یکی</p></div>
<div class="m2"><p>دو ندانم من خدا را بی‌شکی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>زود باشد تا تو ای روباه نام</p></div>
<div class="m2"><p>خود بچنگ شیر افتی چون غلام</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>زود باشد تا تو چون زینب شوی</p></div>
<div class="m2"><p>چون نداری رشتهٔ ایمان قوی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>زینب کذابه هم دین باشدت</p></div>
<div class="m2"><p>با رضا آن شاه دین کین باشدت</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>نقش کینه ازدرون خود تراش</p></div>
<div class="m2"><p>ورنه هستی تو بمعنی بت تراش</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>گر ندارد قلب تو پاکی ز آز</p></div>
<div class="m2"><p>بیشک آرندت بدوزخ در گداز</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>قلب خود را از کدورت پاک ساز</p></div>
<div class="m2"><p>تاترا گردد نمازی هر نماز</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>هر که حبّ مصطفی دارد بدل</p></div>
<div class="m2"><p>پیش ذات حق نباشد او خجل</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>مهراحمد آنکه بر دل زد سجل</p></div>
<div class="m2"><p>حبّ فرزندانش هم دارد بدل</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>هیچ راضی نیست خود کرّار از او</p></div>
<div class="m2"><p>ورنه باشد مصطفی بیزار از او</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>در ره دین نبی مردانه باش</p></div>
<div class="m2"><p>وز همه یاران بدبیگانه باش</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>رو تو ارباب معانی را ببین</p></div>
<div class="m2"><p>دور باش از مفتی محفل نشین</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>رو تو با درویش دین صحبت بدار</p></div>
<div class="m2"><p>تا نهندت لوح عرفان بر کنار</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>رو تو واصل شو بدریای یقین</p></div>
<div class="m2"><p>ز آنکه هستت نور معنی در جبین</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>رو تو علم معنی از قرآن بگیر</p></div>
<div class="m2"><p>زانکه باشد علم قرآن دستگیر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>رو تو ازتفسیر این مشتی حمار</p></div>
<div class="m2"><p>دور باش و معنی قرآن بیار</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>رو تودوری کن ازین مشتی پلید</p></div>
<div class="m2"><p>شد کلام حق از ایشان ناپدید</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بین کلید حیله‌شان اندر بغل</p></div>
<div class="m2"><p>بر حذر میباش از این مشتی دغل</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>راه شرع مصطفی ویران کنند</p></div>
<div class="m2"><p>کفر را گیرند و نام ایمان کنند</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>شرع می‌گویند فرماید چنین</p></div>
<div class="m2"><p>رای خود را شرع پندارند و دین</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>رو تو کار خود بیزدان راست کن</p></div>
<div class="m2"><p>راه خود در طور مردان راست کن</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>رو تو با حق راست گوی و راست باش</p></div>
<div class="m2"><p>دور باش از خودپسند و خودتراش</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>رو تو از قاضی بددوری گزین</p></div>
<div class="m2"><p>ز آنکه می‌گیرد برشوت از تودین</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>گویدت بامن اگر داری تو کار</p></div>
<div class="m2"><p>دین ما را گیرودین خود گذار</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>گر تو این ره از رضای حق روی</p></div>
<div class="m2"><p>شرع در ظاهر شود بر تو قوی</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>شرع باطن مصطفی دارد نه تو</p></div>
<div class="m2"><p>شرع ظاهر را بگردان تا مگو</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>راه باطل بهر دنیائی روی</p></div>
<div class="m2"><p>نیست اسلام تو درمعنی قوی</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>هست دنیائی پلید و راهزن</p></div>
<div class="m2"><p>حبّ دنیائیت سازد کم ز زن</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>مفتی آورده کتاب حیله را</p></div>
<div class="m2"><p>پر ز رشوت کرده قاضی کیله را</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>خود مدرّس زحمت شبها کشید</p></div>
<div class="m2"><p>روزها هم علّت سودا کشید</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>تا رسد وجهش زوقفی بر مدام</p></div>
<div class="m2"><p>هست اندر مذهب این احترام</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>خود نه آخر این حدیث مصطفی است</p></div>
<div class="m2"><p>طالب دنیا چو سگ باشد رواست</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>ای برادر حیلهٔ شرعی میار</p></div>
<div class="m2"><p>دست از این جیفهٔ دنیا بدار</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>ورنه از قول رسول هاشمی</p></div>
<div class="m2"><p>دور گردی از طریق مردمی</p></div></div>