---
title: >-
    بخش ۵۵ - قصه شیخ شقیق بلخی و هارون الرشید، و بیان نمودن ربقه امام معصوم موسی کاظم علیه السلام و منصور حلاج و تمثیل آنکه آشنا بیگانه را راه به آشنائی می‬نماید و بیگانه آشنا نمی‬شود
---
# بخش ۵۵ - قصه شیخ شقیق بلخی و هارون الرشید، و بیان نمودن ربقه امام معصوم موسی کاظم علیه السلام و منصور حلاج و تمثیل آنکه آشنا بیگانه را راه به آشنائی می‬نماید و بیگانه آشنا نمی‬شود

<div class="b" id="bn1"><div class="m1"><p>بود شیخی عابد و بس پارسا</p></div>
<div class="m2"><p>بود او مشهور از اهل صفا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داده اور ا معرفت یزدان پاک</p></div>
<div class="m2"><p>غیر حق را رفته بود از جان پاک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نام او را با تو گویم ای رفیق</p></div>
<div class="m2"><p>خوانده اندش اولیای حق شقیق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود او در عصر هارون الرّشید</p></div>
<div class="m2"><p>شرع احمد را نهان از خلق دید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفت روزی نزد هارون در خلا</p></div>
<div class="m2"><p>تا بگوید سرّ اسرار خدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خلاف و آشکارا و نهان</p></div>
<div class="m2"><p>آنچه دیده بود خود گوید عیان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون بدید او را خلیفه عذر خواست</p></div>
<div class="m2"><p>گفت هستی در زمانه مرد راست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زاهدی مثلت ندانم در جهان</p></div>
<div class="m2"><p>نیست زهدتو به پیش من نهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شیخ با او گفت زاهد نیستم</p></div>
<div class="m2"><p>من بزهد خویش عابد نیستم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زاهد است آنکو قناعت باشدش</p></div>
<div class="m2"><p>زهد هم از دید طاعت باشدش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من بترک دید دنیا کرده‌ام</p></div>
<div class="m2"><p>آخرت را جسته پیدا کرده‌ام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زاهد دنیا توئی ای ملک بین</p></div>
<div class="m2"><p>زآنکه داری ملک دنیا در نگین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خود باین دنیا قناعت کرده‌ای</p></div>
<div class="m2"><p>آبروی آخرت را برده‌ای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من بهردو کی قناعت میکنم</p></div>
<div class="m2"><p>وصل او خواهم که طاعت می‌کنم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چونکه هارون این سخن بشنید از او</p></div>
<div class="m2"><p>آه سردی خوش برآورد از گلو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفت پس ای شیخ پندی ده مرا</p></div>
<div class="m2"><p>تا شوم دل سرد از این محنت سرا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شیخ گفتا حق ترا با خویش خواند</p></div>
<div class="m2"><p>بعد از آن برجای صدّیقان نشاند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا بیاری صدق بر گفتار حق</p></div>
<div class="m2"><p>از کلام مصطفی خوانی ورق</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هرچه حق فرموده باشد آن کنی</p></div>
<div class="m2"><p>غیر حق را در جهان ویران کنی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دیگر آنکه عدل کن تو در جهان</p></div>
<div class="m2"><p>گو تو داری از علوم دین نشان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ورنه عمر خویش ضایع میکنی</p></div>
<div class="m2"><p>خویش را از خلد مانع میکنی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دیگر آنکه جای حیدر جای تست</p></div>
<div class="m2"><p>مسند عزّت بزیر پای تست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بود علم و فضل در ذات علی</p></div>
<div class="m2"><p>خود حیا و جود ظاهر ز آن ولی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>او دل از شرع نبی پر نور کرد</p></div>
<div class="m2"><p>وز شجاعت کفر را مقهور کرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>حق تعالی ذوالفقارش چون بداد</p></div>
<div class="m2"><p>وهم او در جان بی دینان فتاد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شرع احمد را رواج از تیغ داد</p></div>
<div class="m2"><p>پیش تیغ او خوارج سر نهاد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو مخالف را چو آن شه منع کن</p></div>
<div class="m2"><p>اصل ایشان را بکن از بیخ و بن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>داد مظلومان ز ظالم واستان</p></div>
<div class="m2"><p>غیر را محرم مکن در این و آن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خلق عالم شادمان از عدل تو</p></div>
<div class="m2"><p>بر جمیع پادشاهان فضل تو</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ورنه باشی حاکمی غافل بدهر</p></div>
<div class="m2"><p>عاقبت ظلمت بگیرد شهر شهر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>حق تعالی سرنگون اندازدت</p></div>
<div class="m2"><p>خود چه میدانی که چون اندازدت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تو طریق عدل را بنیاد کن</p></div>
<div class="m2"><p>عالمی از عدل خود آباد کن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>رو تو بنیادی بمان از عدل خویش</p></div>
<div class="m2"><p>رو فرست اسباب عقبایت ز پیش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>رو تو راهی ساز همچون راه حج</p></div>
<div class="m2"><p>زآنکه بنیادی ندارد خشت و کج</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>رو تو راهی ساز از علم طریق</p></div>
<div class="m2"><p>گر تو هستی با من مسکین رفیق</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>رو تو راهی ساز از شرع نبی</p></div>
<div class="m2"><p>تا ببینی روز روشن در شبی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>رو تو با ارباب دین همّت بدار</p></div>
<div class="m2"><p>زآنکه این دنیا نباشد پایدار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>رو به پیش موسی کاظم بحلم</p></div>
<div class="m2"><p>زآنکه او باشد بمعنی کان علم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>رو به پیش موسی کاظم بحرف</p></div>
<div class="m2"><p>جان خود را در ره او ساز صرف</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>رو به پیش موسی کاظم که او</p></div>
<div class="m2"><p>هست نقد احمد و حیدر نکو</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>رو به پیش موسی کاظم ببین</p></div>
<div class="m2"><p>در جمالش نوری از حق الیقین</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>رو بر موسیّ کاظم عذر خواه</p></div>
<div class="m2"><p>زآنکه تو منصور را کردی تباه</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تو به پیش کاظم از منصور پرس</p></div>
<div class="m2"><p>حالت مستان حق از طور پرس</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>رو تو از آل نبی همّت طلب</p></div>
<div class="m2"><p>زآنکه ایشانند در دنیا سبب</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>رو تو کفر خویش ار خود دور کن</p></div>
<div class="m2"><p>در محبّت جان خود پرنور کن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>رو بفریاد دل درویش رس</p></div>
<div class="m2"><p>تا شود راضی خداوند از تو بس</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>رو حذر از آه مسکینان حذر</p></div>
<div class="m2"><p>ورنه افتی تو بدنیا در بدر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>رو حذر از آه خلقان خدای</p></div>
<div class="m2"><p>ورنه آویزند در نارت ز پای</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>رو حذر از سوز مسکین الحذر</p></div>
<div class="m2"><p>تا نیاویزندت از دنیا بسر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>رو مکن ظلم و ز خود ظلمت مران</p></div>
<div class="m2"><p>زآنکه ظالم نیست گردد در جهان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تو بدرویشان تکبّر کفر دان</p></div>
<div class="m2"><p>زآنکه ایشانند شاه و شه نشان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>رو تو پند من بجان خود نشان</p></div>
<div class="m2"><p>تا دهندت خود بمعنیها نشان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تو کناره گیر از راه بدان</p></div>
<div class="m2"><p>ریز در آتش علوم جاهلان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>رو کناره کن از این مشتی حمار</p></div>
<div class="m2"><p>زآنکه فضل و علم ایشان شد فشار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>رو ببین شان در قطار نحو صرف</p></div>
<div class="m2"><p>خود ندانند علم معنی نیم حرف</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>رو تو پندم را میان جان نشان</p></div>
<div class="m2"><p>این همه معنی کلام حق بدان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>پندهای من بمعنی گوش کن</p></div>
<div class="m2"><p>لب ز ذکر غیر حق خاموش کن</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چونکه هارون این سخنها را شنید</p></div>
<div class="m2"><p>نعره‌ای زد گفت باخود کای رشید</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>در جهان این تخم را کی کاشتی</p></div>
<div class="m2"><p>حیف اوقاتی که ضایع داشتی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>حیف اوقات تو و حالات تو</p></div>
<div class="m2"><p>بر بساط نرد حق شهمات تو</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>حیف رفتی از جهان نادیده سیر</p></div>
<div class="m2"><p>حکمها راندی نکردی هیچ خیر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>حیف کردی کُشتی این منصور را</p></div>
<div class="m2"><p>گوش کردی حرف اهل زور را</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ظلم کردی بر چنان سلطان دین</p></div>
<div class="m2"><p>گوش کردی گفت این مشتی لعین</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>از چنین حالت بسی بیدل شد او</p></div>
<div class="m2"><p>از سرشک دیده اندر گل شد او</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بعد از آن نزدیک کاظم شد بشب</p></div>
<div class="m2"><p>گفت از من هرچه می‌خواهی طلب</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>من در این مدّت ز تو غافل بُدم</p></div>
<div class="m2"><p>بلکه خود در علم دین جاهل بُدم</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>من ترا دانم خلیفه از یقین</p></div>
<div class="m2"><p>زآنکه هستی نقد خیر المرسلین</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>من ترا دانم امام هر انام</p></div>
<div class="m2"><p>زآنکه داری شربت کوثر بجام</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>من ترا دانم ولیّ حق یقین</p></div>
<div class="m2"><p>زآنکه با تو همرهست اسرار دین</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>من ترا دانم بمعنی پیشوا</p></div>
<div class="m2"><p>زآنکه هستی در هدایت مقتدا</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>مردمان جمله بقصد تو بدند</p></div>
<div class="m2"><p>دشمن منصور بهر تو شدند</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>زآنکه منصور از محبّان تو بود</p></div>
<div class="m2"><p>بود او را پیش درگاهت سجود</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>پنج سال است اینکه غیبت می‌کنند</p></div>
<div class="m2"><p>بر سر منصور خودبدعت زنند</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>پیش من گویند هر شب تا سحر</p></div>
<div class="m2"><p>پیش کاظم می‌نهد حلاج سر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>دیگر آنکه چون برون آید به پیش</p></div>
<div class="m2"><p>سر نهد بر آستان صد بار بیش</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>روی و موی خود بمالد بر زمین</p></div>
<div class="m2"><p>سجده باید کرد حق را این چنین</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>من بایشان گفتم این خود باک نیست</p></div>
<div class="m2"><p>این خلاف شرع و از ادراک نیست</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>من شنیدم یک سخن از باب خویش</p></div>
<div class="m2"><p>گفته‌ام صد بار با اصحاب خویش</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>گفت در ایّام صادق روز عید</p></div>
<div class="m2"><p>شیخ بسطامی به پیش او دوید</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چند جا برآستانش سرنهاد</p></div>
<div class="m2"><p>این حکایت از پدر دارم بیاد</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>من چگویم خود بحلاّج این زمان</p></div>
<div class="m2"><p>زآنکه این کردند مردان در جهان</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>صدق او از آستان او بجو</p></div>
<div class="m2"><p>زآنکه بوده آستانش آبرو</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>من ندارم کار با حلاج هیچ</p></div>
<div class="m2"><p>گر توداری مردکی پوچی و گیج</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>بود این معنی میان ما و خلق</p></div>
<div class="m2"><p>بعد از آن می‌زد اناالحق زیر دلق</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>از فقیهان مجمعی حاضر بُدند</p></div>
<div class="m2"><p>بر حدیث و قول او ناظر بُدند</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>جمله فتواها بخونش داشتند</p></div>
<div class="m2"><p>خود ز خون او گلستان کاشتند</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>اندر این معنی گناه من نبود</p></div>
<div class="m2"><p>از چنین کشتن نیامد هیچ سود</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>من بعذر استاده‌ام در پیش تو</p></div>
<div class="m2"><p>خود نکردم من بمعنی این نکو</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>از سر این جرم شاها در گذار</p></div>
<div class="m2"><p>عفو فرما بر من مسکین زار</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>پس زبان بگشاد آن سلطان دین</p></div>
<div class="m2"><p>گفت در باطن توئی با من بکین</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>لیک این دم عفو کردم جرم تو</p></div>
<div class="m2"><p>ز آنکه این اقرار می‌باشد نکو</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>بعد از این با اهل دین دمساز باش</p></div>
<div class="m2"><p>اهل دل را همچو من همراز باش</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>گفت با هارون که بین منصور را</p></div>
<div class="m2"><p>گشته او در پیش حقّ محو لقا</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>دید هارونش بکنجی دم زده</p></div>
<div class="m2"><p>او به پیش شاه خود محرم شده</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>نعره زد هارون و رفت از خویشتن</p></div>
<div class="m2"><p>گفت موسااش بیا بنگر بمن</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>پیش ما درویش باشد پادشاه</p></div>
<div class="m2"><p>پیش ما دلریش باشد در پناه</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>پیش ما مرهم بود دلریش را</p></div>
<div class="m2"><p>پیش ما خود کس بود بیخویش را</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>پیش ما نبود عذاب و کینه‌ای</p></div>
<div class="m2"><p>پیش ما کینه مدان در سینه‌ای</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>پیش ما باشد معانی در بیان</p></div>
<div class="m2"><p>پیش ما باشد نهانی در عیان</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>پیش ما انعام باشد صد هزار</p></div>
<div class="m2"><p>پیش ما اکرام باشد بیشمار</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>پیش ما باشد ملایک صبح و شام</p></div>
<div class="m2"><p>پیش ما باشد معانی کلام</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>پیش ما باشد همه اسرار غیب</p></div>
<div class="m2"><p>پیش ما باشد همه انوار غیب</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>پیش ما باشد کتاب انبیا</p></div>
<div class="m2"><p>پیش ما باشد مقام اولیا</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>پیش ما شد تاج شاهان سرنگون</p></div>
<div class="m2"><p>پیش ما باشد همه شیران زبون</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>پیش ما باشد همه اسرار حقّ</p></div>
<div class="m2"><p>پیش ما باشد همه دیدار حقّ</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>پیش ما باشد زمین و آسمان</p></div>
<div class="m2"><p>پیش ما باشد همه رفتار جان</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>پیش ما باشد دلی پر خون بسی</p></div>
<div class="m2"><p>پیش ما باشد ز کاف و نون بسی</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>پیش ما باشد همه اسرار عشق</p></div>
<div class="m2"><p>پیش ما باشد همه گفتار عشق</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>پیش ما باشد بمعنی شیخ و شاب</p></div>
<div class="m2"><p>پیش ما باشد عذاب و هم عقاب</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>پیش ما باشد صلاح پارسا</p></div>
<div class="m2"><p>پیش ما باشد مقام التجا</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>پیش ما باشد جحیم و خلد هم</p></div>
<div class="m2"><p>پیش ما باشد معانی جام و جم</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>پیش ما جوهر چه باشد در جهان</p></div>
<div class="m2"><p>پیش ما آن آشکار او نهان</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>پیش ما باشد شراب کوثری</p></div>
<div class="m2"><p>پیش ما باشد طریق رهبری</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>پیش ما باشد مقامات ولی</p></div>
<div class="m2"><p>پیش ما باشد کرامات ولی</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>پیش ما باشد ریاضتهای عشق</p></div>
<div class="m2"><p>پیش ما باشد فراغتهای عشق</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>پیش ما باشد ملایک صف زده</p></div>
<div class="m2"><p>پیش ما باشند حوران کف زده</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>پیش ما باشد کرام الکاتبین</p></div>
<div class="m2"><p>پیش ما جا کرده جبریل امین</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>چونکه هارون این معانی را شنید</p></div>
<div class="m2"><p>پیش آن شه خویش را بی‌خویش دید</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>چشم خود را بر زمین او دوخته</p></div>
<div class="m2"><p>هستی خودرا به پیشش سوخته</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>خود ز چشم او همه خون می‌چکید</p></div>
<div class="m2"><p>زآنکه او منصور را کرده شهید</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>او به پیش شاه از خود رفته بود</p></div>
<div class="m2"><p>زآنکه با منصور او بدکرده بود</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>بعد از آن گفتا که یا خیرالامم</p></div>
<div class="m2"><p>در دو عالم بوده‌ای تو محترم</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>یک توقّع دارم از تو یا امام</p></div>
<div class="m2"><p>آنکه از این بنده مستان انتقام</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>دیگری آنکه بگو منصور را</p></div>
<div class="m2"><p>تا کند روحش دگر با من صفا</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>من همی ترسم که ویرانم کند</p></div>
<div class="m2"><p>بی نجاح و نسل و بیجانم کند</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>من همی ترسم که ازتختم کشند</p></div>
<div class="m2"><p>بر سر دار بلا سختم کشند</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>یا امام دین بده امّید من</p></div>
<div class="m2"><p>رحم کن بر محنت جاوید من</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>پس امام آنگه نظر بروی فکند</p></div>
<div class="m2"><p>گفت او افکنده بودت در کمند</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>این زمان گشتی خلاص از بند او</p></div>
<div class="m2"><p>عاقبت خواهی شدن خرسند او</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>گر باخلاص آوری روئی بما</p></div>
<div class="m2"><p>در عذاب آخر نگردی مبتلا</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>ور همیشه تو بکین باشی چنین</p></div>
<div class="m2"><p>مرتد روی زمینی در یقین</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>گر شوی پیوند ما در رشته‌ای</p></div>
<div class="m2"><p>بعد از این پیدا کنی سررشته‌ای</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>رشتهٔ ما از معانی تافته است</p></div>
<div class="m2"><p>زانجهت سرّ لدنّی یافته است</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>رشتهٔ ما کارگاه انس و جان</p></div>
<div class="m2"><p>بافته دان پیش بازار جهان</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>رشتهٔ ما سلسله در سلسله است</p></div>
<div class="m2"><p>رشتهٔ ما قافله در قافله است</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>رشتهٔ ما این جهان و آن جهان</p></div>
<div class="m2"><p>رشتهٔ ما کار گاه لامکان</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>رشتهٔ ما آدم و نوح است و هود</p></div>
<div class="m2"><p>رشتهٔ ما نسل ابراهیم بود</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>رشتهٔ ما رشتهٔ جانها شده</p></div>
<div class="m2"><p>بعد از آندر قرب او ادنا شده</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>رشتهٔ ما بارگاه اولیاست</p></div>
<div class="m2"><p>رشتهٔ ما در مقام قل کفی است</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>رشتهٔ ما از نبیّ الله بود</p></div>
<div class="m2"><p>از ولایش جان و دل آگاه بود</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>رشتهٔ ما رشته‌ای ز الله بود</p></div>
<div class="m2"><p>زآن درون ما ز حق آگاه بود</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>رشتهٔ ما گیر و آنگه خوش برو</p></div>
<div class="m2"><p>تا بیابی خود حیات خویش نو</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>رشتهٔ ما را محبّان داشتند</p></div>
<div class="m2"><p>در میان جان جانان کاشتند</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>رشتهٔ ما دان صراط مستقیم</p></div>
<div class="m2"><p>پیش ما آن رشته می‌باشد مقیم</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>رشتهٔ ما دان ردای صالحان</p></div>
<div class="m2"><p>رشتهٔ ما خرقهٔ کروّبین</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>رشتهٔ ما جامهٔ آدم شده</p></div>
<div class="m2"><p>رشتهٔ ما تار و پود دم شده</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>رشتهٔ ما با علی پیوند شد</p></div>
<div class="m2"><p>رشتهٔ ما با ولی در بند شد</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>رشتهٔ ما دان حسن آنگه حسین</p></div>
<div class="m2"><p>رشتهٔ ما دان علی آن نور عین</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>رشتهٔ ما باقی و صادق بود</p></div>
<div class="m2"><p>آنکه او در ملک دین حاذق بود</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>رشتهٔ ما داده عالم را نظام</p></div>
<div class="m2"><p>ختم این رشته بمهدی شد تمام</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>گر تو میخواهی که گردی رستگار</p></div>
<div class="m2"><p>در ولایتهای ما تو شک میار</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>زآنکه ما هستیم بی روی و ریا</p></div>
<div class="m2"><p>نخل باغ مصطفی و مرتضی</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>هرکه با ما نیک شد نیکو شود</p></div>
<div class="m2"><p>در میان حور عین دلجو شود</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>وآنکه با ما از حسد گردید بد</p></div>
<div class="m2"><p>مالک دوزخ سوی خویشش کشد</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>گفت هارون یا امام المتّقین</p></div>
<div class="m2"><p>چند جانم را بسوزی این چنین</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>بستم آخر با شما ز آنگونه عهد</p></div>
<div class="m2"><p>که کنم در دوستی بسیار جهد</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>در حق تو قول دشمن نشنوم</p></div>
<div class="m2"><p>خصم را از بیخ و از بن برکنم</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>گفت امامش گر چنین باشی مقیم</p></div>
<div class="m2"><p>ایمنی از محنت قعر جحیم</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>به بقول خصم خواهی کرد بیم</p></div>
<div class="m2"><p>کی دهندت جا بجنات النعیم</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>من گرفتم بر تو حجت این زمان</p></div>
<div class="m2"><p>گر شنودی هستی آخر در امان</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>ور بقول دیگران کردی تو کار</p></div>
<div class="m2"><p>در سقر باشد مقامت پایدار</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>از می دنیا نگردی مست تو</p></div>
<div class="m2"><p>دین و دنیا را مده از دست تو</p></div></div>