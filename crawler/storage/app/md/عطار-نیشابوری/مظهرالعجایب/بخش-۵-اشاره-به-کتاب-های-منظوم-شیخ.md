---
title: >-
    بخش ۵ - اشاره به کتاب‌های منظوم شیخ
---
# بخش ۵ - اشاره به کتاب‌های منظوم شیخ

<div class="b" id="bn1"><div class="m1"><p>گر ازین مرهم نیابی کام خویش</p></div>
<div class="m2"><p>جوهر الذاّتم بیاور تو به پیش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنچه از وی بشنوی در خویش بین</p></div>
<div class="m2"><p>تاشود سرّ عجایب پیش بین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جوهر الذّاتم سخن بی پرده است</p></div>
<div class="m2"><p>همچو اشتر نامه مستی کرده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر تو از مرغ حقایق پر بری</p></div>
<div class="m2"><p>منطق الطّیرم بخوان تا بربری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرغ عطّار از زبان حق شنید</p></div>
<div class="m2"><p>لاجرم از آشیان حق پرید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چونکه حق بشناختی شیرین به بین</p></div>
<div class="m2"><p>تا شود این دید تو حقّ الیقین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رو تو اسرار ولایت گوش کن</p></div>
<div class="m2"><p>و آنگهی جام هدایت نوش کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر تو از جام محبّت می خوری</p></div>
<div class="m2"><p>جانب شهر ولایت پی بری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رو مصیبت نامه را از سر بخوان</p></div>
<div class="m2"><p>تا شود حاصل تو رامقصود جان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر الهی نامه را گیری بگوش</p></div>
<div class="m2"><p>جام وحدت را کنی بی شبهه نوش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پندنامه گر بیابی در جهان</p></div>
<div class="m2"><p>تو عزیزش دار همچون جان جان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا بیابی عزّت دنیا و دین</p></div>
<div class="m2"><p>آنگهی بر تخت سلطانی نشین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رو بذکر اولیا مشغول شو</p></div>
<div class="m2"><p>و آنگهی چون تذکره مقبول شو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همچو ایشان ترک کن تجرید شو</p></div>
<div class="m2"><p>دور روزی چند ازتقلید شو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>من کتب بسیاردارم در جهان</p></div>
<div class="m2"><p>لیک مظهر را عجایب نیک دان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مظهر کلّ عجایب حیدر است</p></div>
<div class="m2"><p>در میان سالکان او رهبر است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ختم کردم این کتب بر نام او</p></div>
<div class="m2"><p>زآنکه دارم مستیی از جام او</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر که او از جام تو یک قطره خورد</p></div>
<div class="m2"><p>گوی دولت از میانه او ببرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای تو درمقصود یکتا آمده</p></div>
<div class="m2"><p>مظهر سرّ هویدا آمده</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>احمد مرسل چو رویت را بدید</p></div>
<div class="m2"><p>گفت اینک نور حق از حق رسید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حق بسی گفته ثنا در شأن او</p></div>
<div class="m2"><p>گر نمی‌دانی بخوان قرآن او</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر تو از قرآن حق منکر شوی</p></div>
<div class="m2"><p>بیشکی میدان که تو کافر شوی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای ز بینش مقصد و مقصود حق</p></div>
<div class="m2"><p>وی بدانش برده تو از کلّ سبق</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای تو درعالم محقّق آمده</p></div>
<div class="m2"><p>نور تو با ذات ملحق آمده</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پرتو ذات الهی بود تو</p></div>
<div class="m2"><p>بحرها چون شبنمی از جود تو</p></div></div>