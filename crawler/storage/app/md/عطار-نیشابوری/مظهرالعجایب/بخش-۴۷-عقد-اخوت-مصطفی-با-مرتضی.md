---
title: >-
    بخش ۴۷ - عقد اخوت مصطفی با مرتضی
---
# بخش ۴۷ - عقد اخوت مصطفی با مرتضی

<div class="b" id="bn1"><div class="m1"><p>گفت روزی مصطفی اصحاب را</p></div>
<div class="m2"><p>عقد میفرمود با هم در اخا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت او با یکدگر یاری کنید</p></div>
<div class="m2"><p>خود بهم عهد و وفاداری کنید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون شوم من یارتان حق یار شد</p></div>
<div class="m2"><p>از بدیهای شما بیزار شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت ای صدّیق هستی یار من</p></div>
<div class="m2"><p>در مغاره بوده یار غار من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت با فاروق کی چست آمده</p></div>
<div class="m2"><p>در طریق شرع من رست آمده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر دو را با یکدگر بیعت بداد</p></div>
<div class="m2"><p>پس برادر کردشان و عهد داد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پس بذوالنورین گفت ای یار ما</p></div>
<div class="m2"><p>کاتب وحی منی پیشم بیا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پس بعبدالله او را عقد داد</p></div>
<div class="m2"><p>جمله را با یکدگر دادی وداد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دو بدو با یکدگرشان عقد داد</p></div>
<div class="m2"><p>می‌شدند از صحبت هم جمله شاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جملهٔ اصحاب کردندی خروش</p></div>
<div class="m2"><p>بود اندر گوشه‌ای حیدر خموش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت با او مصطفی گو حال گو</p></div>
<div class="m2"><p>خود چنین ساکت چرائی ای نکو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفت ما را یا نبیّ المرسلین</p></div>
<div class="m2"><p>تا بکی تنها گذاری این چنین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جملگی گشتند با هم همنشین</p></div>
<div class="m2"><p>من شده در گوشه‌ای تنها چنین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت ای نورولایت درنهان</p></div>
<div class="m2"><p>جبرئیل آمد بگفتا کن چنان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بعد از آن گفت ای تو محبوب الاه</p></div>
<div class="m2"><p>بند خود عقد اخوّت را بشاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز آنکه حق این عقد را در عرش بست</p></div>
<div class="m2"><p>ای سر هر سروری پیش تو پست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جملهٔ کرّوبیان حاضر بدند</p></div>
<div class="m2"><p>ماه و خورشید اندر آن ناظر بدند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>حوریان خود جمله جان افشان شدند</p></div>
<div class="m2"><p>در رخ این هر دو شه حیران شدند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>حق تعالی بیعت ما بسته است</p></div>
<div class="m2"><p>تو نپنداری که این خود رسته است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پس نبی دست علی را چون گرفت</p></div>
<div class="m2"><p>صیغهٔ عقد اخوت را بگفت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بعد از آن گفتا که شو فارغ ز غم</p></div>
<div class="m2"><p>ما چو موسائیم و چون هارون بهم</p></div></div>