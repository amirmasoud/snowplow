---
title: >-
    بخش ۳۳ - قوله تعالی: «انما ولیکم الله و رسوله والذین آمنوا الذین یقیمون الصلوة و یؤتون الزکوة و هم راکعون»
---
# بخش ۳۳ - قوله تعالی: «انما ولیکم الله و رسوله والذین آمنوا الذین یقیمون الصلوة و یؤتون الزکوة و هم راکعون»

<div class="b" id="bn1"><div class="m1"><p>گفت پیغمبر به یاران این سخن</p></div>
<div class="m2"><p>پیک ربّ العالمین آمد بمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت حیدر را خدا این تحفه داد</p></div>
<div class="m2"><p>بر همه خلق جهان فضلش نهاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت او والی بود در ملک من</p></div>
<div class="m2"><p>خود یقین میدان و رادر سلک من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گشت داخل از یقین زوج بتول</p></div>
<div class="m2"><p>درولایت با خداوند و رسول</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حاکم و میر و ولی خلق شد</p></div>
<div class="m2"><p>در ولا با مصطفی هم دلق شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که باشد مصطفی او راولیّ</p></div>
<div class="m2"><p>پس ولیّ او بود بیشک علی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مصطفی چون هست هادیّ خدا</p></div>
<div class="m2"><p>شد علی هادی شرع مصطفی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غیر حقّ خود نیست با حیدر کسی</p></div>
<div class="m2"><p>او بده در عالم معنی بسی</p></div></div>