---
title: >-
    بخش ۳ - در اشاره به کتب و تألیفات خود فرماید
---
# بخش ۳ - در اشاره به کتب و تألیفات خود فرماید

<div class="b" id="bn1"><div class="m1"><p>به اوّل سه کتب تقریر کردم</p></div>
<div class="m2"><p>به آخر یک از آن تحریر کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جواهر نامه با مختار نامه</p></div>
<div class="m2"><p>بشرح القلب من رهبر بخانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترا معراج نامه پیش حق خواند</p></div>
<div class="m2"><p>جواهر نامه‌ات خود این سبق خواند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترا مختار نامه چون بهشت است</p></div>
<div class="m2"><p>بشرح القلب معنا چون کنشت است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بعد این کتب خوان سه کتب را</p></div>
<div class="m2"><p>که تا گردد وجودت خود مصفّا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بوصلت نامه دان وصل معانی</p></div>
<div class="m2"><p>ز بلبل نامهٔ ما وا نمانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زهیلاجم جهان در لرزش آمد</p></div>
<div class="m2"><p>فلک از قدرتش در گردش آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کتب بسیار دارم گر بخوانی</p></div>
<div class="m2"><p>ازو دنیا و عقبی را بدانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ازو ناجی شوی و سالک آیی</p></div>
<div class="m2"><p>براه دیگران خودهالک آیی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدان کین مظهرم جان کتبها است</p></div>
<div class="m2"><p>درو اسرار دین حق هویداست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیا در جان من مقصود جان بین</p></div>
<div class="m2"><p>بعین عین خود عین العیان بین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیا بین آنچه مقصود اله است</p></div>
<div class="m2"><p>که او ملک وملایک را پناه است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بیا بین نور حق رادر معانی</p></div>
<div class="m2"><p>که نور اوست نور جاودانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیا بین نور او را در وجودت</p></div>
<div class="m2"><p>بشکرانه بکن او را سجودت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو آدم نور حق را پیش خود دید</p></div>
<div class="m2"><p>ورا بود آن چنان روزی دو صد عید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به عدل او را اشارت خود همو کرد</p></div>
<div class="m2"><p>که ای باب همه مردان توئی فرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بکن عدل ارز ما خواهی دگر بار</p></div>
<div class="m2"><p>وگرنه پیش ما نبود ترا بار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بکن عدل ار محبّ مصطفائی</p></div>
<div class="m2"><p>غلام و چاکر آل عبائی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بکن عدل ارز حکمت با نصیبی</p></div>
<div class="m2"><p>که علم و عدل باشد خود حسیبی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بکن عدل و امین شو در جهان تو</p></div>
<div class="m2"><p>که تا باشی سعادت جاودان تو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بکن عدل و کرم با خلق آفاق</p></div>
<div class="m2"><p>که تا باشی میان صالحان طاق</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بکن عدل و کرم گر میتوانی</p></div>
<div class="m2"><p>که این ماند بدنیا جاودانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بکن عدل و کرم ای نقد آدم</p></div>
<div class="m2"><p>که تا باشی میان حاتمان یم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بکن عدل و کرم تا نام یابی</p></div>
<div class="m2"><p>میان عاشقان آرام یابی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بکن عدل و کرم گر تاج خواهی</p></div>
<div class="m2"><p>ز شاهان جهان اخراج خواهی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بکن عدل و کرم در ملک دنیا</p></div>
<div class="m2"><p>که تا باشد ترا عقبی مهیا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بکن عدل و کرم تا راه یابی</p></div>
<div class="m2"><p>بزیر جبّه‌ات صد ماه یابی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بکن عدل و کرم تا جان دهندت</p></div>
<div class="m2"><p>بوقت مرگ خود ایمان دهندت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بکن عدل و کرم ای فخر ایّام</p></div>
<div class="m2"><p>اگر داری تو بر این قصر ما کام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بکن عدل وکرم گر ملک خواهی</p></div>
<div class="m2"><p>که این باشد نشان پادشاهی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بکن عدل و کرم گر میتوانی</p></div>
<div class="m2"><p>کتاب ظلم را دیگر نخوانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بکن عدل و کرم کین فخر دین است</p></div>
<div class="m2"><p>نشان اولیآء ملک دین است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بکن عدل و کرم تا شاد گردی</p></div>
<div class="m2"><p>ز دوزخ بیشکی آزاد گردی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بکن عدل و کرم تا زنده باشی</p></div>
<div class="m2"><p>میان اولیآ فرخنده باشی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بکن عدل و کرم ای جان درویش</p></div>
<div class="m2"><p>که خورشید است قرص خوان درویش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بکن عدل و کرم ورنه زبون شو</p></div>
<div class="m2"><p>درون دوزخ تابان نگون شو</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بکن عدل و کرم ورنه خرابی</p></div>
<div class="m2"><p>درون آتش سوزان کبابی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بکن عدل و کرم ورنه بمردی</p></div>
<div class="m2"><p>ز دنیا حسرت واندوه بردی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بکن عدل و کرم ورنه اسیری</p></div>
<div class="m2"><p>بغلّ و بند در زندان بمیری</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بکن عدل و کرم ورنه فتادی</p></div>
<div class="m2"><p>تو برخود این در محنت گشادی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بتو هرچند گویم از معانی</p></div>
<div class="m2"><p>تو این را بشنوی افسانه خوانی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>معانیهای عالم جمع کردم</p></div>
<div class="m2"><p>ز دستش بادهٔ عرفان بخوردم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شدم مست و ببحرش راه بردم</p></div>
<div class="m2"><p>ز جسم هستی خود جمله مردم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز علم دوست گشتم حیّ موجود</p></div>
<div class="m2"><p>هم او بوده مرا از علم مقصود</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز بحر علم دُر آرم بخروار</p></div>
<div class="m2"><p>کنم در راه جانان جمله ایثار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ز بحر علم دارم صد کتب من</p></div>
<div class="m2"><p>در آن بنهاده‌ام اسرار لب من</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز بحر علم دارم جامه‌ها پر</p></div>
<div class="m2"><p>برو بستان تو از الفاظ من در</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تو آن در را نگهدار و رهی شو</p></div>
<div class="m2"><p>بکوی راستان همچون شهی شو</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز بحر علم دارد جان من جوش</p></div>
<div class="m2"><p>ولی علم صور کردم فراموش</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز علم انبیا خواندم سبقها</p></div>
<div class="m2"><p>ز شرح اولیا دارم ورقها</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>کتابی را که از ایمان نویسم</p></div>
<div class="m2"><p>ز علم معنی قرآن نویسم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>کتابی را که با جانان قرین است</p></div>
<div class="m2"><p>ز گفتار نبی المرسلین است</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>کتابی را که من از آن نویسم</p></div>
<div class="m2"><p>بود بحر و دگر را چون نویسم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>کمال علم او دانستن جان</p></div>
<div class="m2"><p>ولی در ذات انسانست پنهان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چو انسان نیستی علمت نباشد</p></div>
<div class="m2"><p>میان مردمان حلمت نباشد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چو آن سان نیستی تو سر ندانی</p></div>
<div class="m2"><p>توسرّ خویش را از برندانی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>هر آنکس را که دنیا خویش باشد</p></div>
<div class="m2"><p>ورا زقوّم دوزخ پیش باشد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>هر آنکس را که دنیا همنشین است</p></div>
<div class="m2"><p>ورا شیطان ملعون در کمین است</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>هر آنکس را که دنیا یار دانست</p></div>
<div class="m2"><p>ز خود عقبی همه بیزار دانست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>هر آنکس را که دنیا رهنمونست</p></div>
<div class="m2"><p>بتحقیق و یقین خود بس زبونست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>هر آنکس را که دنیا برده از راه</p></div>
<div class="m2"><p>نباشد از خدای خویش آگاه</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>هر آنکس کو زدنیا کام ور شد</p></div>
<div class="m2"><p>به آخر او ز دین حق بدر شد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>هر آنکس کو ز دنیا شاد کام است</p></div>
<div class="m2"><p>مقام آخرت بروی حرام است</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>هر آنکس را که دنیا برقع افکند</p></div>
<div class="m2"><p>ورا کرد او بزیر پرده در بند</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>هر آنکس را که دنیا خود مقامست</p></div>
<div class="m2"><p>ورا در عالم قدسی نه کام است</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>هر آنکس را که دنیا برگزیده است</p></div>
<div class="m2"><p>فلک را زیر گردش خود خمیده است</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>هر آنکس را که دنیا برکشیده است</p></div>
<div class="m2"><p>فلک او را بزیر پنجه دیده است</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>هر آنکس را که دنیا پیشوا شد</p></div>
<div class="m2"><p>محمّد با علی از وی جدا شد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>هر آنکس را که دنیا دام باشد</p></div>
<div class="m2"><p>شیاطین جملگی بر بام باشد</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>هر آنکس را که دنیا ذکر باشد</p></div>
<div class="m2"><p>ز ذکر جنّتش کی فکر باشد</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>هر آنکس را که دنیا درنگین است</p></div>
<div class="m2"><p>ورا صد دشمن بد در کمین است</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>هر آنکس را که دنیا چون شکر شد</p></div>
<div class="m2"><p>ورا تیغ چو زهرش در جگر شد</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>هر آنکس را که دنیا خود حیاتست</p></div>
<div class="m2"><p>به آخر اصل حال او ممات است</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>هر آنکس را که دنیا آرزو شد</p></div>
<div class="m2"><p>سیه رو گشت و حال او چو مو شد</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>هر آنکس را که دنیا شد زبون شد</p></div>
<div class="m2"><p>چو عیسی بر فلک بر گو که چون شد</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>برو تو حبّ دنیا را چو مردان</p></div>
<div class="m2"><p>برون کن از دل و خود را مرنجان</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>برو تو حبّ دنیا بی ثمر دان</p></div>
<div class="m2"><p>تو اصل دانش و دین چون قمر دان</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>برو با یار گو اسرار رازم</p></div>
<div class="m2"><p>برویش باب معنی کن تو بازم</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>هر آنکو دین ندارد مرد ما نیست</p></div>
<div class="m2"><p>میان عاشقان و با صفا نیست</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>برو ای یار دینم را وطن کن</p></div>
<div class="m2"><p>پس آنکه با کتبهایم سخن کن</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>برو ای یار با عطّار بنشین</p></div>
<div class="m2"><p>که تا یابی بوقت مرگ تلقین</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>چو تلقین یافتی اندر بهشتی</p></div>
<div class="m2"><p>وگرنه دین و ایمانت بهشتی</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>ترا عطّار از اسرارگوید</p></div>
<div class="m2"><p>نه با نفس و هوایت یار گوید</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>ترا ازمعنی قرآن دهد پند</p></div>
<div class="m2"><p>برو خود را بقرآن کن تو پیوند</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>که تا محکم شود ایمان و دینت</p></div>
<div class="m2"><p>شود جمله نهانیها یقینت</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>تو دانستی یقین تو یار ما باش</p></div>
<div class="m2"><p>درون جبّهٔ اسرار ما باش</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>برو با اهل معنی خلوتی کن</p></div>
<div class="m2"><p>ز جام اهل معنی شربتی کن</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>برو ای یار پیش یار درویش</p></div>
<div class="m2"><p>که او باشد ترا پیوند و هم خویش</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>برو ای یار سالک را دعا کن</p></div>
<div class="m2"><p>تو این دنیای دون را خود رها کن</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>برو ای یار خاک آن قدم شو</p></div>
<div class="m2"><p>پش آنکه سرفراز و محترم شو</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>برو ای یار با او همنشین باش</p></div>
<div class="m2"><p>بجور بردباری چون زمین باش</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>برو ای یار با او همقرین شو</p></div>
<div class="m2"><p>پس آنگه باملایک همنشین شو</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>اگر تا نی بیائی اندرین راه</p></div>
<div class="m2"><p>ترا مظهر کند از حال آگاه</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>اگر در منزل او راه یابی</p></div>
<div class="m2"><p>بهر دو کون بیشک جاه یابی</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>اگردانا دهد جاهت بشاهی</p></div>
<div class="m2"><p>بگیری این فلک با ماه و ماهی</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>اگر دانا ترا افکند از پای</p></div>
<div class="m2"><p>سرت رفت و نیابی هیچ جا جای</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>برو تو دانش دانا زبر کن</p></div>
<div class="m2"><p>ز دانشهای نادان تو حذر کن</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>ز دانشهای نادان در چه افتی</p></div>
<div class="m2"><p>چه خوک تیر خورده در ره افتی</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>ز دانشهای نادان کرده ره گم</p></div>
<div class="m2"><p>نخوردی یک دمی از آب زمزم</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>ترا چون آب زمزم نیست در جان</p></div>
<div class="m2"><p>وصال کعبه کی یابی چو مردان</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>ز کعبه یافتم مقصود کعبه</p></div>
<div class="m2"><p>از آنم مشتری گشته چو زهره </p></div></div>
<div class="b" id="bn102"><div class="m1"><p>مرا با شاه کعبه حالها شد</p></div>
<div class="m2"><p>که نی از درد من در ناله‌ها شد</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>زهرجا نعره‌ها آمد زصخره</p></div>
<div class="m2"><p>که رو چون بیت مقدس گیر بهره</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>در آن بهره تو مقصودی طلب کن</p></div>
<div class="m2"><p>ز مقصودم تو محبوبی طلب کن</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>در آن مطلوب محبوبم هویداست</p></div>
<div class="m2"><p>ز سر تا پای او انوار پیداست</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>مرا با اوست بیعت در معانی</p></div>
<div class="m2"><p>تو این اسرار معنی را چه دانی</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>مرا با اوست این دنیا و دینم</p></div>
<div class="m2"><p>ظهور او شده عین الیقینم</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>مرا ازاوست این جانی که بینی</p></div>
<div class="m2"><p>ترا کفر است با او همنشینی</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>اگر شخصی بگوید دین من اوست</p></div>
<div class="m2"><p>به خونش میدهی فتوی که نیکوست</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>ترا از بهر کشتن نافریدند</p></div>
<div class="m2"><p>ز بهر وصل کردن آفریدند</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>تو بشناس آنکه او باب الجنانست</p></div>
<div class="m2"><p>بشهرستان احمد چون جنان است</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>تو بشناس آنکه او ما را یقین گفت</p></div>
<div class="m2"><p>یقین از گفت شاه المرسلین گفت</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>تو بشناس آنکه او سرّ معالیست</p></div>
<div class="m2"><p>درون نی ز غیر او چه خالیست</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>که بود آنکه محمّد گفت جانش</p></div>
<div class="m2"><p>بحال نزع بوسید اودهانش</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>به آن بوسه باو اسرارها گفت</p></div>
<div class="m2"><p>دگر او را سر و سردارها گفت</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>هم او سردار باشد اولیآ را</p></div>
<div class="m2"><p>هم او دیدار باشد انبیآ را</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>اگر خواهی بدانی پیشوایت</p></div>
<div class="m2"><p>بگویم تا بدانی مقتدایت</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>امیرالمؤمنین حیدر ولیّم</p></div>
<div class="m2"><p>محمّد فخر آدم شد نبیّم</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>امیرالمؤمنین اسم وی آمد</p></div>
<div class="m2"><p>ز بهر دیگران این خود کی آمد</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>امیرالمؤمنین باشد امامم</p></div>
<div class="m2"><p>که مهر اوست وابسته بجانم</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>امیرالمؤمنین نور خدایست</p></div>
<div class="m2"><p>دگر او نطق و نفس مصطفایست</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>امیرالمؤمنین روح روانم</p></div>
<div class="m2"><p>بمعنی نطق گشته در زبانم</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>امیرالمؤمنین میدان که شاه است</p></div>
<div class="m2"><p>مرا در کلّ آفتها پناه است</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>امیرالمؤمنین درویش آمد</p></div>
<div class="m2"><p>درین عالم ز جمله پیش آمد</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>امیرالمؤمنین دانای سرها</p></div>
<div class="m2"><p>امیرالمؤمنین از جان هویدا</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>امیرالمؤمنین شد اسم اعظم</p></div>
<div class="m2"><p>امیرالمؤمنین باشد مکرّم</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>امیرالمؤمنین در هر زمانی</p></div>
<div class="m2"><p>امیرالمؤمنین در هر مکانی</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>امیرالمؤمنین شاه ولایت</p></div>
<div class="m2"><p>امیرالمؤمنین جاه ولایت</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>امیرالمؤمنین راه و طریقست</p></div>
<div class="m2"><p>امیرالمؤمنین بحر عمیقست</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>امیرالمؤمنین شمشیر برّان</p></div>
<div class="m2"><p>امیرالمؤمنین خود شیر غرّان</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>امیرالمؤمنین چون ماه تابان</p></div>
<div class="m2"><p>امیرالمؤمنین آن اصل قرآن</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>امیرالمؤمنین قهّار آمد</p></div>
<div class="m2"><p>امیرالمؤمنین جبّار آمد</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>امیرالمؤمنین در حکم محکم</p></div>
<div class="m2"><p>امیرالمؤمنین با روح همدم</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>امیرالمؤمنین را تو چه دانی</p></div>
<div class="m2"><p>که بغضش رامیان جان نشانی</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>ز بغضش راه دوزخ پیش گیری</p></div>
<div class="m2"><p>ز حبّش درولای او نمیری</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>تراگر دین و ایمان پابجای است</p></div>
<div class="m2"><p>ترا حبّش ز حق در دین عطایست</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>در این عالم بسی من راه دیدم</p></div>
<div class="m2"><p>همه این راهرا در چاه دیدم</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>بغیر راه او کآن راه حق است</p></div>
<div class="m2"><p>دگرها جمله مکروهات فسق است</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>تو اندر وقف راهی ساختستی</p></div>
<div class="m2"><p>که ازدرس معانی باز رستی</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>برو در مدرسه تو علم حق خوان</p></div>
<div class="m2"><p>مده تغییر در معنیّ قرآن</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>بقرآن وقف ترکان کی حلالست</p></div>
<div class="m2"><p>ترا این خدمت و منصب وبالست</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>به پیشم حیلهٔ شرعی میاور</p></div>
<div class="m2"><p>به پیش من نباشد حیله باور</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>ترا از بهر دانش آوریدند</p></div>
<div class="m2"><p>ز بهر بینشت خود پروریدند</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>ترا انسان کامل نام کردند</p></div>
<div class="m2"><p>میان سالکانت جام کردند</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>پس آنگه ریختند در وی شرابی</p></div>
<div class="m2"><p>که انسان و ملک خوردند آبی</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>همه از جرعه‌اش مدهوش و مستند</p></div>
<div class="m2"><p>همه از جوی بیراهی بجستند</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>همه هستند و سر مستند و هشیار</p></div>
<div class="m2"><p>در این دنیای دون و دون گرفتار</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>برون آ از گرفتاری این چرخ</p></div>
<div class="m2"><p>که تا گردی چو معروفی در آن کرخ</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>ز کرخ دل برون آی و تو جان بین</p></div>
<div class="m2"><p>تو معروف حقیقی بیگمان بین</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>مرا خود آرزوی لامکانست</p></div>
<div class="m2"><p>که آجا سرّ ما اوحی عیانست</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>جهان خود پر ز انوارتجلّی است</p></div>
<div class="m2"><p>ولیکن دیدهٔ تو مثل اعمی است</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>ترا انوار جانان نیست روشن</p></div>
<div class="m2"><p>از آن افتادی اندر چاه بیژن</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>چو افتادی بدان چه کی برآئی</p></div>
<div class="m2"><p>درون آتش هجران درآئی</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>برون آ خانه را روشن کن از نور</p></div>
<div class="m2"><p>رفیقی اندرو بنشان به از حور</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>که تا از راه بد آرد براهت</p></div>
<div class="m2"><p>بمعنی باشد او پشت و پناهت</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>ترا باشد رفیق نیک ایمان</p></div>
<div class="m2"><p>باین عالم تو باشی چون سلیمان</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>بیا تا ما و تو اسرار گوئیم</p></div>
<div class="m2"><p>میان خانه و بازار گوئیم</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>به اسرارت نمایم راه توفیق</p></div>
<div class="m2"><p>بکن این قول حقانی تو تصدیق</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>اگر این قول را خوانی بتکرار</p></div>
<div class="m2"><p>به او واصل شوی درعین دیدار</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>بیا و علم حقانی زبر کن</p></div>
<div class="m2"><p>تو انسان را ز علم حق خبر کن</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>برو تو علم عاشق گیر در دین</p></div>
<div class="m2"><p>که تا گردی چو منصور خدابین</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>برو تو واقف اسرار من باش</p></div>
<div class="m2"><p>درون کلبهٔ عطّار من باش</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>که تا بینی که سرمستان کیانند</p></div>
<div class="m2"><p>میان دیدهٔ بینا عیانند</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>هرآنکس کو از این جرعه چشیده است</p></div>
<div class="m2"><p>دو عالم را مثال ذرّه دیده است</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>ملایک با همه انسان عالم</p></div>
<div class="m2"><p>طفیل مصطفا اند بلکه آدم</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>محمّد هست محبوب خداوند</p></div>
<div class="m2"><p>هم او بوده است مطلوب خداوند</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>هم او باشد به این اسرار محرم</p></div>
<div class="m2"><p>هم او باشد به یاران یار همدم</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>تو یار یار را نشناختستی</p></div>
<div class="m2"><p>از آن ایمان و دین در باختستی</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>تو یار یار محبوب محمّد</p></div>
<div class="m2"><p>بدان تا گردی از معنی مؤیّد</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>تو بشناس آنکه او اسرار دیده است</p></div>
<div class="m2"><p>میان اولیآ دیدار دیده است</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>تو بشناس آنکه او را حق ولی خواند</p></div>
<div class="m2"><p>محمّد بعد خویشش خود وصی خواند</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>تو بشناس آنکه مقصود جنان است</p></div>
<div class="m2"><p>معین و رهبر این کاروان شد</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>تو بشناس آنکه او دانای راز است</p></div>
<div class="m2"><p>تو بشناس آنکه او بینای راز است</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>تو بشناس آنکه او در عین دید است</p></div>
<div class="m2"><p>همه گلهای معنی او بچیده است</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>توبشناس آنکه او دید الهست</p></div>
<div class="m2"><p>هم او مولای خود را عذر خواه است</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>ترا حیله است ورد جان و تلقین</p></div>
<div class="m2"><p>از آن گندیده گشتی همچو سرگین</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>مرا با حال پاکان کار باشد</p></div>
<div class="m2"><p>که در پاکی همه انوار باشد</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>مرا با اهل معنی ذوق باشد</p></div>
<div class="m2"><p>که از عشقش درونم شوق باشد</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>مرا با اهل عرفان رازهایست</p></div>
<div class="m2"><p>که از دردش درونم ناله‌هایست</p></div></div>
<div class="b" id="bn180"><div class="m1"><p>مرا جز اهل وحدت گفتگونیست</p></div>
<div class="m2"><p>که گفت دیگرانم همچو بونیست</p></div></div>
<div class="b" id="bn181"><div class="m1"><p>مرا از بحر عشقش یکدوجو نیست</p></div>
<div class="m2"><p>که پیشم بحر نادان چون سبو نیست</p></div></div>
<div class="b" id="bn182"><div class="m1"><p>مرا هر دو جهان بر مثل موئیست</p></div>
<div class="m2"><p>به آتش سوزمش این دم که هوئیست</p></div></div>
<div class="b" id="bn183"><div class="m1"><p>مرا از دست نادان خون شده دل</p></div>
<div class="m2"><p>بنادان گفتن اسرار مشکل</p></div></div>
<div class="b" id="bn184"><div class="m1"><p>مرا کاری دگر در پیش راه است</p></div>
<div class="m2"><p>که عالم بر دو چشم من سیاه است</p></div></div>
<div class="b" id="bn185"><div class="m1"><p>مقیّد مانده‌ام در دست اطفال</p></div>
<div class="m2"><p>یکان وقتی بدرد آید مرا حال</p></div></div>
<div class="b" id="bn186"><div class="m1"><p>مرا از درد ایشان درد زاید</p></div>
<div class="m2"><p>زمانه دایمم انگشت خاید</p></div></div>
<div class="b" id="bn187"><div class="m1"><p>خداوندا بحق جود و فضلت</p></div>
<div class="m2"><p>بحقّ رحمت و احسان و بذلت</p></div></div>
<div class="b" id="bn188"><div class="m1"><p>بحقّ جمله محبوبان درگاه</p></div>
<div class="m2"><p>بحقّ جمله مطلوبان درگاه</p></div></div>
<div class="b" id="bn189"><div class="m1"><p>بحقّ اولیا و انبیایت</p></div>
<div class="m2"><p>بحقّ اصفیا و اتقیایت</p></div></div>
<div class="b" id="bn190"><div class="m1"><p>بحقّ جمله قرآن و کلامت</p></div>
<div class="m2"><p>به بیداری که داری در قیامت</p></div></div>
<div class="b" id="bn191"><div class="m1"><p>بحقّ جملهٔ کروّبیانت</p></div>
<div class="m2"><p>به فضل جملهٔ روحانیانت</p></div></div>
<div class="b" id="bn192"><div class="m1"><p>بحقّ آتش شوق محبّان</p></div>
<div class="m2"><p>بحقّ حالت ذوق محبّان</p></div></div>
<div class="b" id="bn193"><div class="m1"><p>بحقّ آن یتیم زار و بیمار</p></div>
<div class="m2"><p>بحقّ آن اسیران نگونسار</p></div></div>
<div class="b" id="bn194"><div class="m1"><p>بحقّ عاشقان مست اسرار</p></div>
<div class="m2"><p>بحقّ عارفان سینه افکار</p></div></div>
<div class="b" id="bn195"><div class="m1"><p>بحقّ جام وصل واصلانت</p></div>
<div class="m2"><p>بحقّ ذکر و اوراد مهانت</p></div></div>
<div class="b" id="bn196"><div class="m1"><p>بحقّ آن شهیدان کفن تر</p></div>
<div class="m2"><p>بحقّ آن یتیم دیده بردر</p></div></div>
<div class="b" id="bn197"><div class="m1"><p>بحقّ آن شجاع سر فدایت</p></div>
<div class="m2"><p>بحقّ آنکه دادیش از عطایت</p></div></div>
<div class="b" id="bn198"><div class="m1"><p>بحقّ آنکه چون منصور مست است</p></div>
<div class="m2"><p>بحقّ آنکه او مست الست است</p></div></div>
<div class="b" id="bn199"><div class="m1"><p>بحقّ آدم و نوح و سلیمان</p></div>
<div class="m2"><p>بحقّ شیث با موسیّ عمران</p></div></div>
<div class="b" id="bn200"><div class="m1"><p>بحقّ خضر و با الیاس و یعقوب</p></div>
<div class="m2"><p>بحقّ ارمیا با هود وایّوب</p></div></div>
<div class="b" id="bn201"><div class="m1"><p>بحقّ دانیال ادریس و یحیی</p></div>
<div class="m2"><p>به اسمعیل و اسحق و به عیسی</p></div></div>
<div class="b" id="bn202"><div class="m1"><p>بحقّ یونس ابراهیم امجد</p></div>
<div class="m2"><p>بصدق آن شعیب پاک و اسعد</p></div></div>
<div class="b" id="bn203"><div class="m1"><p>بحقّ اولیاء ما تقدم</p></div>
<div class="m2"><p>بحقّ انبیاء دیده پرنم</p></div></div>
<div class="b" id="bn204"><div class="m1"><p>بحقّ مصطفی و آل یسین</p></div>
<div class="m2"><p>بحقّ مرتضی آن نور تلقین</p></div></div>
<div class="b" id="bn205"><div class="m1"><p>بحقّ جمله فرزندان پاکش</p></div>
<div class="m2"><p>بحقّ عابدان خاک راهش</p></div></div>
<div class="b" id="bn206"><div class="m1"><p>بحقّ پیروان آل حیدر</p></div>
<div class="m2"><p>بحقّ جانشینان مطهّر</p></div></div>
<div class="b" id="bn207"><div class="m1"><p>بحقّ شیعهٔ شبّیر و شبّر</p></div>
<div class="m2"><p>بآب دیدهٔ عابد بشب تر</p></div></div>
<div class="b" id="bn208"><div class="m1"><p>بحقّ باقر آن دریای رحمت</p></div>
<div class="m2"><p>بحقّ صادق آن نور حقیقت</p></div></div>
<div class="b" id="bn209"><div class="m1"><p>بحقّ کاظم آن بحر تحمّل</p></div>
<div class="m2"><p>بحقّ آن رضا کان توکّل</p></div></div>
<div class="b" id="bn210"><div class="m1"><p>بحقّ آن تقی چون باب معصوم</p></div>
<div class="m2"><p>بحقّ آن نقیّ کشته مظلوم</p></div></div>
<div class="b" id="bn211"><div class="m1"><p>بحقّ عسکری آن تاج ایمان</p></div>
<div class="m2"><p>بحقّ مهدی آن هادی ایمان</p></div></div>
<div class="b" id="bn212"><div class="m1"><p>بحقّ بوذر و سلمان و قنبر</p></div>
<div class="m2"><p>بحقّ یاسر و عمّار و اشتر</p></div></div>
<div class="b" id="bn213"><div class="m1"><p>بحقّ بصری ومالک به دینار</p></div>
<div class="m2"><p>بحقّ آن محمّد واسع کار</p></div></div>
<div class="b" id="bn214"><div class="m1"><p>بحقّ آن حبیب اعجمیم</p></div>
<div class="m2"><p>بحقّ خالد مکّی ولیّم</p></div></div>
<div class="b" id="bn215"><div class="m1"><p>بحقّ عتبه با شیخ فضیلم</p></div>
<div class="m2"><p>بحقّ رابع سلطان کمیلم</p></div></div>
<div class="b" id="bn216"><div class="m1"><p>بحقّ شاه ابراهیم ادهم</p></div>
<div class="m2"><p>به بشر حافی آن شیخ مکرّم</p></div></div>
<div class="b" id="bn217"><div class="m1"><p>بحقّ شیخ آن ذوالنون مصری</p></div>
<div class="m2"><p>به بازید و شقیق آن شیخ بلخی</p></div></div>
<div class="b" id="bn218"><div class="m1"><p>بحقّ عبد آن شیخ مبارک</p></div>
<div class="m2"><p>بحقّ آنکه بگرفت او سه تارک</p></div></div>
<div class="b" id="bn219"><div class="m1"><p>بحقّ داود طائی و حارث</p></div>
<div class="m2"><p>بحقّ احمد حرب و بوارث</p></div></div>
<div class="b" id="bn220"><div class="m1"><p>بحقّ عبدسهل معروف و اعلم</p></div>
<div class="m2"><p>به سمّاک و بدارا و به اسلم</p></div></div>
<div class="b" id="bn221"><div class="m1"><p>بحقّ پیر رضی الدین لالا</p></div>
<div class="m2"><p>به حاتمّ اصم آن نور والا</p></div></div>
<div class="b" id="bn222"><div class="m1"><p>بحقّ سرّی و آن فتح موصل</p></div>
<div class="m2"><p>به شیخ احمد آن عبّاد فاضل</p></div></div>
<div class="b" id="bn223"><div class="m1"><p>بحقّ بوتراب و خضرویّه</p></div>
<div class="m2"><p>به یحیی معاذ آن پیر خرقه</p></div></div>
<div class="b" id="bn224"><div class="m1"><p>بحقّ شه شجاع و مجد بغداد</p></div>
<div class="m2"><p>به یوسف بن حسن با شیخ حدّاد</p></div></div>
<div class="b" id="bn225"><div class="m1"><p>بحقّ شیخ دین منصور عماد</p></div>
<div class="m2"><p>بحمدون قصار آن بحر اسرار</p></div></div>
<div class="b" id="bn226"><div class="m1"><p>بحقّ مرد حق احمد عاصم</p></div>
<div class="m2"><p>به شیخ ما جنید آن مست قائم</p></div></div>
<div class="b" id="bn227"><div class="m1"><p>بحقّ عمرو و آن عثمان مکّی</p></div>
<div class="m2"><p>به خرّاز و ابوسفیان ثوری</p></div></div>
<div class="b" id="bn228"><div class="m1"><p>بحقّ آن محمّد بحر رویم</p></div>
<div class="m2"><p>به ابراهیم رقی با عطایم</p></div></div>
<div class="b" id="bn229"><div class="m1"><p>بحقّ یوسف و اسباط و یعقوب</p></div>
<div class="m2"><p>بسمنون محبّ و شیخ ایوب</p></div></div>
<div class="b" id="bn230"><div class="m1"><p>بحقّ شیخ بوشنجی و ورّاق</p></div>
<div class="m2"><p>بحقّ مرتعش آن شیخ دقاق</p></div></div>
<div class="b" id="bn231"><div class="m1"><p>بحقّ فضل دین با شیخ مغرب</p></div>
<div class="m2"><p>بحقّ حمزهٔ طوسی و مهلب</p></div></div>
<div class="b" id="bn232"><div class="m1"><p>بحقّ شیخ علی مرحبانی</p></div>
<div class="m2"><p>بحقّ احمد مسروق فانی</p></div></div>
<div class="b" id="bn233"><div class="m1"><p>بحقّ شیخ عبدالله روعد</p></div>
<div class="m2"><p>بحقّ شیخ مرشد کوست سرمد</p></div></div>
<div class="b" id="bn234"><div class="m1"><p>بحقّ پیر ذخّار کبیرم</p></div>
<div class="m2"><p>که او بوده بدین عالم منیرم</p></div></div>
<div class="b" id="bn235"><div class="m1"><p>بحقّ شاه سرمستان آفاق</p></div>
<div class="m2"><p> که نامش مستطر بوده به نه طاق</p></div></div>
<div class="b" id="bn236"><div class="m1"><p>بحقّ شیخ محمد حریری</p></div>
<div class="m2"><p>که او را بوده انفاس کبیری</p></div></div>
<div class="b" id="bn237"><div class="m1"><p>بحقّ شیخ دشت خاورانی</p></div>
<div class="m2"><p>که او را بوده حکم کامرانی</p></div></div>
<div class="b" id="bn238"><div class="m1"><p>بحقّ نالش عطار مسکین</p></div>
<div class="m2"><p>بحقّ رهروان راه این دین</p></div></div>
<div class="b" id="bn239"><div class="m1"><p>بحقّ کعبه و بطحا و زمزم</p></div>
<div class="m2"><p>بحقّ سجده گاه باب آدم</p></div></div>
<div class="b" id="bn240"><div class="m1"><p>که اهل علم را ده تو صفائی</p></div>
<div class="m2"><p>و یا بر سرنهش تاج وفائی</p></div></div>
<div class="b" id="bn241"><div class="m1"><p>ویا رحمی بده یارب ورا تو</p></div>
<div class="m2"><p>که تا سازد دل درویش نیکو</p></div></div>
<div class="b" id="bn242"><div class="m1"><p>دگر اهل معانی را حضوری</p></div>
<div class="m2"><p>بده تا طاعتش باشد چو نوری</p></div></div>
<div class="b" id="bn243"><div class="m1"><p>دگر دست عدو کوتاه گردان</p></div>
<div class="m2"><p>بدرویشی و فقرم شاه گردان</p></div></div>
<div class="b" id="bn244"><div class="m1"><p>چو درویشی و فقرم شد مسلّم</p></div>
<div class="m2"><p>زنم در کاینات الله اعلم</p></div></div>
<div class="b" id="bn245"><div class="m1"><p>دگر اهل و عیال و خیل وخالم</p></div>
<div class="m2"><p>تو شان جمعیّتی ده در وصالم</p></div></div>
<div class="b" id="bn246"><div class="m1"><p>دگر این بنده را کنج حضوری</p></div>
<div class="m2"><p>خداوندا بده یا خود صبوری</p></div></div>
<div class="b" id="bn247"><div class="m1"><p>دگر از خلق دوری ذوق دارم</p></div>
<div class="m2"><p>ازین دوری بخود بس شوق دارم</p></div></div>
<div class="b" id="bn248"><div class="m1"><p>وگر از خلق دارم من نفوری</p></div>
<div class="m2"><p>ندارم من بایشان دست زوری</p></div></div>
<div class="b" id="bn249"><div class="m1"><p>وگر من ازگنه بسیاردارم</p></div>
<div class="m2"><p>ولیکن عفو تو من یار دارم</p></div></div>