---
title: >-
    بخش ۲ - در نعت سلطان سریر ارتضا علی بن موسی الرضا علیه السلام و کسب فیوضات از آستان آن حضرت
---
# بخش ۲ - در نعت سلطان سریر ارتضا علی بن موسی الرضا علیه السلام و کسب فیوضات از آستان آن حضرت

<div class="b" id="bn1"><div class="m1"><p>شه من در خراسان چون دفین شد</p></div>
<div class="m2"><p>همه ملک خراسان را نگین شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امام هشتم و نقد محمّد</p></div>
<div class="m2"><p>رضای حق بد او در دین احمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم او بد قرة العین ولایت</p></div>
<div class="m2"><p>به او همراه بد کلّ عبادت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدان تو کعبه بر حق مرقدش را</p></div>
<div class="m2"><p>از آنکه هست محبوب حق آنجا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بقول مصطفا حج شد طوافش</p></div>
<div class="m2"><p>چرا کردی تو ای ملعون خلافش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز کعبه بس مراتب دان بلندش</p></div>
<div class="m2"><p>بگویم لیک نتوانی فکندش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درون کعبهٔ ما نقد شاه است</p></div>
<div class="m2"><p>که او محبوب و مطلوب اله است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بحال کودکی در آستانش</p></div>
<div class="m2"><p>به شبها خوانده‌ام ورد زبانش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا از روح او آمد مددها</p></div>
<div class="m2"><p>دگر گفتا که شابورت بود جا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بوقت کودکی من هیجده سال</p></div>
<div class="m2"><p>بمشهد بوده‌ام خوشوقت و خوشحال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دگر رفتم بنیشابور و تون هم</p></div>
<div class="m2"><p>به آخر گشت شاپورم چو همدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به شاپورم بدندی سالکان جمع</p></div>
<div class="m2"><p>از ایشان داشتم اسرارها سمع</p></div></div>