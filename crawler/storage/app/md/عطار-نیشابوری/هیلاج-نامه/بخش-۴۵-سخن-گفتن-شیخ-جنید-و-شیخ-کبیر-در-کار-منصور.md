---
title: >-
    بخش ۴۵ - سخن گفتن شیخ جنید و شیخ کبیر در کار منصور
---
# بخش ۴۵ - سخن گفتن شیخ جنید و شیخ کبیر در کار منصور

<div class="b" id="bn1"><div class="m1"><p>جنید راهبر سلطان عشاق</p></div>
<div class="m2"><p>که آمد در حقیقت بیشکی طاق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر شیخ کبیر استاده بُد او</p></div>
<div class="m2"><p>همه دیده بر اوبنهاده بود او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان در ذوق بود از سر جانان</p></div>
<div class="m2"><p>ولی استاده بد در عشق پنهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از اول تا بآخر بر سردار</p></div>
<div class="m2"><p>جنید پاک از بودش خبردار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خبر بودش ازو در حضرت شاه</p></div>
<div class="m2"><p>وی استاده پیش خورشید درگاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یقین چون شیخ معنی دید اینجا</p></div>
<div class="m2"><p>که کل میگفت از توحید اینجا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بپرسیدش ز سرّ و راز منصور</p></div>
<div class="m2"><p>سوی شیخ کبیر آن شاه مشهور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنین گفتا که ای شیخ جهان بین</p></div>
<div class="m2"><p>درین حالت کنون صاحبقران بین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عجب مردی که چون او من ندیدم</p></div>
<div class="m2"><p>چنین مردی و نه ازکس شنیدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عجب رازیست امروز آشکارا</p></div>
<div class="m2"><p>بگو تا چیست با من سر تو یارا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چگونه بینی اور ا بر سر دار</p></div>
<div class="m2"><p>اناالحق میزند هر دم ز گفتار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز زندان تا بدینجا آوردیم</p></div>
<div class="m2"><p>بسوی دار او را برکشیدم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قصاص شرع راندیمش حقیقت</p></div>
<div class="m2"><p>که تا یک دم زند اندر شریعت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنان آویخته اینجای مطلق</p></div>
<div class="m2"><p>دم کل میزند اندر اناالحق</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دم کل میزند اینجا چو ما او</p></div>
<div class="m2"><p>حقیقت بس بلند این گفت دین گو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اناالحق میزند با پیر معنی</p></div>
<div class="m2"><p>چگونه این زمان تدبیر معنی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حقیقت آنچه گوئی آن کنم من</p></div>
<div class="m2"><p>مرا ای شیخ دین بی گوی روشن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شریعت عالیست اینجا حقیقت</p></div>
<div class="m2"><p>نگنجد هیچ در عین شریعت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شریعت غالب آمد نزد عشاق</p></div>
<div class="m2"><p>فکنده دمدمه در کل آفاق</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>قدم از شرع این بیرون نهاده است</p></div>
<div class="m2"><p>ندانم سر این تا چون فتاده است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>برون از شرع میگوید سخن باز</p></div>
<div class="m2"><p>بچشم جان نموده است این یقین باز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سخن اینجا بلند آورد دمدم</p></div>
<div class="m2"><p>که من دمدم یقین هستم زآدم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سخن کین گفت این دم در ره شرع</p></div>
<div class="m2"><p>حقیقت دانم اینجا ز آن سخن فرع</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حقیقت کافر است این مرد اینجا</p></div>
<div class="m2"><p>که پیدا شد حقیقت شور و غوغا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دگر کردیم اینجا گاه بردار</p></div>
<div class="m2"><p>مگر باشد ز سرّ ما خبردار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دو دست او در اینجا گه ببریم</p></div>
<div class="m2"><p>که او خر مهره است و ما چو درّیم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بباید دست او اینجا بریدن</p></div>
<div class="m2"><p>نباید این سخن از وی شنیدن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زبانش هم بباید کرد بیرون</p></div>
<div class="m2"><p>که تا خامش شود چون مانده درخون</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چه میگوئی حقیقت شیخ عالم</p></div>
<div class="m2"><p>بگو تا چون کنیم از شرع این دم</p></div></div>