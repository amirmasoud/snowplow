---
title: >-
    بخش ۳۶ - سؤال دیگر شبلی از منصور
---
# بخش ۳۶ - سؤال دیگر شبلی از منصور

<div class="b" id="bn1"><div class="m1"><p>محمددان وصال حق در اینجا</p></div>
<div class="m2"><p>محمد اوست خلق مطلق اینجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو شبلی این بیان بشنید از او</p></div>
<div class="m2"><p>تعجب کرد از وی گفت نیکو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حقیقت این چنین است اندر اینجا</p></div>
<div class="m2"><p>که را همچو تو بگشاید در اینجا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در تو در حقیقت باز کرده است</p></div>
<div class="m2"><p>دو چشم جانت اینجا باز کرده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در این سر هیچ شک اینجایگه نیست</p></div>
<div class="m2"><p>که جمله اوست جزدیدار شه نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنین است و ولیکن کس نداند</p></div>
<div class="m2"><p>بجز تو هیچکس این سر نداند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترا دادست این سر در ازل یار</p></div>
<div class="m2"><p>نباید گفت این پاسخ به اغیار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نباید گفت این با هر کس ای دوست</p></div>
<div class="m2"><p>که تو مغزی و خلقانند در پوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو مغزی وهمه چون پوست باشند</p></div>
<div class="m2"><p>کجامانند او ای دوست باشند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو اصلی این همه فرعست دانی</p></div>
<div class="m2"><p>برون کونی و عین مکانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ترا زیبد که دیدستی رخ شاه</p></div>
<div class="m2"><p>کزین اسرارها هستی تو آگاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دم از این میزنم گرمیزنم من</p></div>
<div class="m2"><p>تو مرد راهی و بیشک منم زن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرا این زهره کی باشد که باعام</p></div>
<div class="m2"><p>بگویم ز آنکه این عامند انعام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ندانند و مرا همچون تو ای حق</p></div>
<div class="m2"><p>بیاویزند پهلوی تو مطلق</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حقیقت کفر دانند این خلایق</p></div>
<div class="m2"><p>مرا این گفتن اینجا نیست لایق</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کرا برگویم این راز آشکاره</p></div>
<div class="m2"><p>بیک ساعت کنندم پاره پاره</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ترا گفتست جانان تا بدانی</p></div>
<div class="m2"><p>تو بیشک خود یقین دانم که دانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چوتو یاری ترا باید نمودن</p></div>
<div class="m2"><p>حقیقت گفتن و از حق شنیدن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو صاحب دولتی در کل آفاق</p></div>
<div class="m2"><p>توئی اندر میان واصلان طاق</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو صاحب دولت و کون و مکانی</p></div>
<div class="m2"><p>که برگفتی یقین راز نهانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نهان بد راز تا این دم بعالم</p></div>
<div class="m2"><p>تو کردی فاش نزد خلق این دم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خلایق جملگی در گفت و گویند</p></div>
<div class="m2"><p>تمامت سالکان در جست و جویند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گمانی میبرند از سالکانت</p></div>
<div class="m2"><p>درین گفتار وین راز نهانت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گمانشان هم یقین شد آخر کار</p></div>
<div class="m2"><p>مرایشان را وصال آور پدیدار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پدیدارست رویت چو خورشید</p></div>
<div class="m2"><p>بتو دارند مر ذرات امید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گمان بردار ای شاه جهان بین</p></div>
<div class="m2"><p>که تا بینندت اینجا در یقین بین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>حقیقت گفتگوی خلق بسیار</p></div>
<div class="m2"><p>در این بازار عشقت شد پدیدار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>امید جملگی در حضرت تست</p></div>
<div class="m2"><p>وصال سالکان در قربت تست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چه باشد گر کنی اینجا نظر باز</p></div>
<div class="m2"><p>درون جانها بیشک سرافراز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همه در انتظار وصل بودند</p></div>
<div class="m2"><p>همه در دیدن این اصل بودند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عجب روزیست امروز همایون</p></div>
<div class="m2"><p>که رخ بنمودهٔ از کاف و از نون</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>منت میدانم اینجا اول کار</p></div>
<div class="m2"><p>ببازی برنگفتم عین گفتار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تو بیش از جملهٔ از جملگی گم</p></div>
<div class="m2"><p>همانا قطرهٔ تو بود قلزم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تو دریائی و ایشان جمله قطره</p></div>
<div class="m2"><p>تو خورشیدی و ایشان جمله ذره</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تو بحر جوهری و ایشان صدف وار</p></div>
<div class="m2"><p>توئی جوهر یقین در جمله اسرار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نداند جوهر تو جز تو جانا</p></div>
<div class="m2"><p>که هستی جوهر اندر بود الّا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بدانستم ز اول ذات پاکت</p></div>
<div class="m2"><p>بدانستم در آخر زین چه باکت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ازین شیوه که بنمودی تو امروز</p></div>
<div class="m2"><p>منم در واصلی یک ذره پیروز</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تمامت وصل میخواهم ز دیدار</p></div>
<div class="m2"><p>که بنمائی مرا آری پدیدار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>وصال امروز از تو یافتستم</p></div>
<div class="m2"><p>ز جان نزد تو من بشتافتسم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مرا امروز از تو بر وصال است</p></div>
<div class="m2"><p>که امروز یقین روز وصالست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>وصالم آشکارا گشت چندی</p></div>
<div class="m2"><p>مرا بنهادهٔ در عشق بندی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>منم در بند تو ای ماه دیدار</p></div>
<div class="m2"><p>بجانم وصلت اینجاگه خریدار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تو وصل خود نما تا جان فشانم</p></div>
<div class="m2"><p>بجان و سر ترا بیشک بخوانم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تو وصل خود نمایم یک زمانی</p></div>
<div class="m2"><p>که درغوغای عشق تو جهانی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بهر زه گفت و گوئی گشت پیدا</p></div>
<div class="m2"><p>منم در وصل تو باشور و غوغا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>در این غوغا مرا با تو وصالست</p></div>
<div class="m2"><p>دلم در ذات تو عین جلال است</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>مرا بر گوی جانا عشق بازی</p></div>
<div class="m2"><p>توداری عشق وعشقت نیست بازی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چه باشد عشق بی منصور جمله</p></div>
<div class="m2"><p>که ایشانند از تو نور جمله</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تو نور جملهٔ ای ماه تابان</p></div>
<div class="m2"><p>حقیقت در دل و جانی تو جانان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ز راز عشق آگاهم کن ای دوست</p></div>
<div class="m2"><p>مرا بیرون کن اینجا گاه از پوست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>که عشقت چیست اصل عشق گویم</p></div>
<div class="m2"><p>در این احوال وصف عشق گویم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بسی گفتم من از عشق نهانی</p></div>
<div class="m2"><p>تو سر عشق ای دل نیک دانی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>حقیقت عشق تو بالای دین است</p></div>
<div class="m2"><p>که سر کل ترا عین الیقین است</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ترا عین الیقین از کشف راز است</p></div>
<div class="m2"><p>که آن بر تو ز نور عشق باز است</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ترا از خویش شد در باز اینجا</p></div>
<div class="m2"><p>توئی در عشق کل در را ز اینجا</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>مرا راز نهان میباید اینجا</p></div>
<div class="m2"><p>توئی درعشق کل در راز اینجا</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>مرا راز نهان میباید از دل</p></div>
<div class="m2"><p>که تا چون درم بگشاید از دل</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>درم بگشای و ره ده در درونم</p></div>
<div class="m2"><p>که هم تو درگشا و رهنمونم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>شدی اینجایگه در قربت خود</p></div>
<div class="m2"><p>مرا گرهم دهی نی قوت خود</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>به بخشم تا بگویم راز خود باز</p></div>
<div class="m2"><p>شوم چون تو دگر ای دوست سرباز</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ببخشا راز تا جانی است اینجا</p></div>
<div class="m2"><p>یقین دان راز ربّانی است اینجا</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ببخشا راز تا جانی است ای جان</p></div>
<div class="m2"><p>کنم در راه تو امروز قربان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ببخشم راز از عشق الستت</p></div>
<div class="m2"><p>مرا آگاه کن زین سر که هستت</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>مرا اینجا ببخشی عین دیدار</p></div>
<div class="m2"><p>نبودی این عیانم جمله بریار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>حجابم از میان بردار اینجا</p></div>
<div class="m2"><p>مرا چون خویش کن بردار اینجا</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>حقیقت سالکان راست ای دل</p></div>
<div class="m2"><p>بگو امروز اینجا راز مشکل</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>دلم چون شد بسی در انتظارت</p></div>
<div class="m2"><p>کنون درخاک آمد پایدارت</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>دلم چون گشت چون رویت ندیدم</p></div>
<div class="m2"><p>هلالی شد ز شمست ناپدیدم</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>تو خورشیدی و من ذره درین راه</p></div>
<div class="m2"><p>منم بنده توئی بر جان ودل شاه</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>تو در جانی و راز جمله دانی</p></div>
<div class="m2"><p>ولی میگویم اینجا تا بدانی</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>که میدانم ولی چون تو حقیقت</p></div>
<div class="m2"><p>کجادانم یقین از دید دیدت</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>سخن میرانم اندر قدر خود باز</p></div>
<div class="m2"><p>منم گنجشک تو باز سرافراز</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>چو میدانم که میدانی تو رازم</p></div>
<div class="m2"><p>ز شیب انداز در سوی فرازم</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ز شیب اندازم اکنون سوی بالا</p></div>
<div class="m2"><p>که با تو باز گویم عین الا</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>تو ای اینجا فنا آخر بقایم</p></div>
<div class="m2"><p>ز عین لای خود اینجا نمایم</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بماندم این چنین حیران دلدار</p></div>
<div class="m2"><p>که گشتم از خود و از خویش بیزار</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بسی گفتم ولی سودی ندارد</p></div>
<div class="m2"><p>که دردم هیچ بهبودی ندارد</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>چو توفیق تو میخواهم در اینجا</p></div>
<div class="m2"><p>که گردانی مرا این دم مصفا</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>ز دست خلق مانده در ز حیرم</p></div>
<div class="m2"><p>زمانی باش اینجا دستگیرم</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>تو دستم گیر چون تو دستگیری</p></div>
<div class="m2"><p>ازین افتاده از پا دست گیری</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>تو دستم گیر تا من پایدارم</p></div>
<div class="m2"><p>بسر استاده اندر پای دارم</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>کجا همچون تودارم پایداری</p></div>
<div class="m2"><p>مرا این بس که جان و دل تو داری</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>ببخشا این زمان بر جسم و جانم</p></div>
<div class="m2"><p>تو میگوئی کنون راز نهانم</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>یقین در نطق من از گفتن تست</p></div>
<div class="m2"><p>دل و جانم در اینجا دیدن تست</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>بمقصودی رسیدی این زمان باز</p></div>
<div class="m2"><p>که خودشان بگذرانی زین نشان باز</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>مکانی صعبناکی پر بلا هست</p></div>
<div class="m2"><p>مرا رفتن حقیقت سوی لا هست</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>در آخر باز گویم شرح این راز</p></div>
<div class="m2"><p>که چون خواهم شدن تا حضرتش باز</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>چگونه باز گشتم سوی ذاتت</p></div>
<div class="m2"><p>بود در آخر کار از صفاتت</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>درین اندیشهام ای جان جمله</p></div>
<div class="m2"><p>منم در عشق تو حیران جمله</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>در اینجا دیدمت بازار دنیا</p></div>
<div class="m2"><p>حقیقت باز گشتم سوی عقبی</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>چگونه است این فنا آنکه بقایم</p></div>
<div class="m2"><p>بگو از سر عشق آن لقایم</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>لقایم دید اندر عین صورت</p></div>
<div class="m2"><p>مرا باید که دانم این ضرورت</p></div></div>