---
title: >-
    بخش ۵۴ - تحسین کردن جنید منصور را در اسرار عشق
---
# بخش ۵۴ - تحسین کردن جنید منصور را در اسرار عشق

<div class="b" id="bn1"><div class="m1"><p>جنیدا راهبر چون راز بشنید</p></div>
<div class="m2"><p>تبسّم کرد و گفت آن صاحب دید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترا زیبد که این گوئی چنین است</p></div>
<div class="m2"><p>چنین خواهد بدن عین الیقین است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ولیکن این زمان معنی توداری</p></div>
<div class="m2"><p>ابا این نفس کل دعوی توداری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یقین شیخ معظم ایستاده است</p></div>
<div class="m2"><p>دو چشم اندر سوی حضرت نهادست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حقیقت آنچه او داند تودانی</p></div>
<div class="m2"><p>که او را یار غاری و تودانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که او اینجا چه است و در چه چیز است</p></div>
<div class="m2"><p>که در آفاق همچون جان عزیز است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عزیز است این زمان در آفرینش</p></div>
<div class="m2"><p>وزو روشن شده اسرار بینش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من اینجا گرچه شیخ و پیشوایم</p></div>
<div class="m2"><p>تو سلطانی و من همچو گدایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گدایم من تو سلطان کباری</p></div>
<div class="m2"><p>تو تاج سلطنت بر فرق داری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو تاج سلطنت داری ابر سر</p></div>
<div class="m2"><p>حقیقت میدهی هم تاج و افسر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نداند هیچکس قدر حیاتت</p></div>
<div class="m2"><p>مگر آنکس که او بشناخت ذاتت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر آنکو ذات خود بشناخت اینجا</p></div>
<div class="m2"><p>ز شادی جان و دل در باخت اینجا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عجایب جوهری تو باز دیدم</p></div>
<div class="m2"><p>هم از جان و دل همراز دیدم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سرافرازی و سربازی درین راه</p></div>
<div class="m2"><p>که از راز خودی ای شیخ آگاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو آگاه خودی در آفرینش</p></div>
<div class="m2"><p>تو همراز خودی در کل بینش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ندیده چشم عالم چون تو شاهی</p></div>
<div class="m2"><p>همه اینجا بکش چون پادشاهی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو شاه آفرینش آمدی کل</p></div>
<div class="m2"><p>چرا افکندهٔ خود را درین ذل</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نهٔ این ذلّ و دیگر بس چه خواهی</p></div>
<div class="m2"><p>نخواهم یافت چون تو جان پناهی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>حقیقت جملگی را قهر گردان</p></div>
<div class="m2"><p>برافکن از میان چرخ گردان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بماتا چند اینجا میکشی تو</p></div>
<div class="m2"><p>گهی اندر خوشی گه ناخوشی تو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو بود تو یکی بوده است اول</p></div>
<div class="m2"><p>همی کن بود را اینجامعطل</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>برون انداز خود را از سردار</p></div>
<div class="m2"><p>که تا چیزی نباشد لیس فی الدار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو میدانم که کلی جوهری تو</p></div>
<div class="m2"><p>حقیقت نور ذات و سروری تو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو اصلی این همه فرع تو آمد</p></div>
<div class="m2"><p>مصاحب نیز از شرع تو آمد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از آن اینجا کمال خویش بردار</p></div>
<div class="m2"><p>نمودستی تو از بهر نمودار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ازل را با ابد پیوند داری</p></div>
<div class="m2"><p>چراخود را تو اندر بندداری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو خواهی رفت عالم را بسوزان</p></div>
<div class="m2"><p>که هستت بخت و تاج نیک روزان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو خواهی رفت ازین صورت تو بیرون</p></div>
<div class="m2"><p>بگردان جمله را در خاک ودرخون</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چوخواهی رفت چیزی را بمگذار</p></div>
<div class="m2"><p>بجز ذات خود ای دانای اسرار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بشرع اقوال پاکت یافتستم</p></div>
<div class="m2"><p>نمود خود ز خاکت یافتستم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>من اندر اصل جوهر از تو بودم</p></div>
<div class="m2"><p>بجز ذات تودرکلی نبودم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تماشا کردمت سری که گفتی</p></div>
<div class="m2"><p>تو خود گفتی و هم از خود شنفتی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>رهی بردم سوی اسرار ذاتت</p></div>
<div class="m2"><p>نماندستم کنون اندر صفاتت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو ذاتت در صفاتت هست موجود</p></div>
<div class="m2"><p>همه ذات تو هست ونیست جز بود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جنیدا ذات تست اینجا حقیقت</p></div>
<div class="m2"><p>ولیکن از صفات اینجا پدیدت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جنیدا ذات تست و خود تودانی</p></div>
<div class="m2"><p>که میدانی که اندر جان نهانی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>جنید امروز می چیزی نداند</p></div>
<div class="m2"><p>بجز تو در همه حیران بماند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چه میدانم که چیزی می ندانم</p></div>
<div class="m2"><p>صفاتی چند اینجا آگاه خوانم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>صفاتت کی جنید اینجا بیابد</p></div>
<div class="m2"><p>چو تو کی صید کی اینجا بیابد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو تو مرغی که سیمرغ مکانی</p></div>
<div class="m2"><p>نموده روی خود در لامکانی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>که داند تا چه تو دانی در اینجا</p></div>
<div class="m2"><p>که بگشادی صفات خوددر اینجا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>وصالت اندرین فقر است اینجا</p></div>
<div class="m2"><p>که در فقری همیشه بود تنها</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اگر شیخم تو شیخی داریم دوست</p></div>
<div class="m2"><p>وگر مغزم تو اینجا کردهٔ پوست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز شیخی این زمان من فارغم یار</p></div>
<div class="m2"><p>بجز تو نقش خود میبینم اغیار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز شیخی فارغم و از زهد و سالوس</p></div>
<div class="m2"><p>حقیقت جملگی میدارم افسوس</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ز شیخی فارغم از عین فتوی</p></div>
<div class="m2"><p>ترا میدانم ای دنیا و عقبی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چه خواهم کرد شیخی زین سپس من</p></div>
<div class="m2"><p>ترا میبینم اندر جمله بس من</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز شیخی جانم آمد بر زبانم</p></div>
<div class="m2"><p>بطاقت آمد از این کار جانم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>کنون بودم درین سر عین پندار</p></div>
<div class="m2"><p>ازین پندار جان من برون آر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تو گفتی آنچه اینجا گفتنی است</p></div>
<div class="m2"><p>دلم زان تو از جمله مکین است</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>همه فعلند و تو عین صفاتی</p></div>
<div class="m2"><p>صفاتند این همه تو بود ذاتی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>من اینها را ندانم چون تو دیگر</p></div>
<div class="m2"><p>کجا باشد صدف مانند گوهر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>صدف را گرچه گوهردار باشد</p></div>
<div class="m2"><p>کجا همچون در شهوار باشد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>درین دریا که اینجا جوهر تست</p></div>
<div class="m2"><p>حقیقت عقل اینجا رهبر تست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>اگرچه عاشقی معشوقه گردی</p></div>
<div class="m2"><p>ز عشق خود کنی این ره نوردی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>دگر میبشکنی بت از وجودت</p></div>
<div class="m2"><p>که ناپیدا نماید بود بودت</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چه بود تو یقین هم پایدار است</p></div>
<div class="m2"><p>جنیدت عاشق اندر پای دار است</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ز چندین راز کاینجا گفتهٔ باز</p></div>
<div class="m2"><p>در اسرارها هم سفتهٔ باز</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>توقع دارم از شیرین زبانت</p></div>
<div class="m2"><p>که برگوئی بسی شرح و بیانت</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بیانت دمبدم ذات خود آمد</p></div>
<div class="m2"><p>از آنت نحن و آیات خود آمد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>تو نزدیکی چرا دوری گزینی</p></div>
<div class="m2"><p>ز ما امروز معذوری نه بینی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>همه معذور راهیم ای سرافراز</p></div>
<div class="m2"><p>تو از بهر چه میآئی سرانداز</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چرا دست و زبانت دور داری</p></div>
<div class="m2"><p>ازین گفتن مرا معذور داری</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چرا خود را بسوزانی درآتش</p></div>
<div class="m2"><p>چرا بیرون شوی از پنج و از شش</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>در این ترکیب رخسارت پدید است</p></div>
<div class="m2"><p>درین صورت ترا گفت و شنید است</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>همه معنی ازین صورت عیانست</p></div>
<div class="m2"><p>وزین صورت همه شرح و بیانست</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>در این صورت تو میبینند و آفاق</p></div>
<div class="m2"><p>وزین صورت همه شرحست و مشتاق</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ازین صورت ترا بردار بینند</p></div>
<div class="m2"><p>حقیقت نقطهٔ پرگار بینند</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چو ما ز این صورت اینجا آشنائیم</p></div>
<div class="m2"><p>نمود تو در این صورت نمائیم</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چرائی محو خواهی کرد صورت</p></div>
<div class="m2"><p>چه افتاده است بر گوئی ضرورت</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چو در اصل تو صورت هست پیدا</p></div>
<div class="m2"><p>وجود جمله اندر لاوالّا</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>تو ذاتی از تو ظاهر هست ذاتم</p></div>
<div class="m2"><p>وز آن سر نکته دیگر برانم</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>من این فتوی نخواهم داد اینجا</p></div>
<div class="m2"><p>که بُرندت زبان با دست و با پا</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>اگر سر میرود ما را حقیقت</p></div>
<div class="m2"><p>نخواهم ترک کردن دید دیدت</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>مرا این سرفرازی از سر تست</p></div>
<div class="m2"><p>مرا بر سر حقیقت افسر تست</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>مرا بر دست دستان تو باشد</p></div>
<div class="m2"><p>بخاصه چون قدم زان تو باشد</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>حقیقت خود بسوزانم درین راه</p></div>
<div class="m2"><p>کجا هرگز توانم سوخت ای شاه</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ترا اینجا اگر جمله بسوزم</p></div>
<div class="m2"><p>از آن به کین وجودت برفروزم</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>حقیقت این چنین است ای یگانه</p></div>
<div class="m2"><p>مرا زهره نباشد در زمانه</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>که کاری این چنین آرم پدیدار</p></div>
<div class="m2"><p>تو باقی هرچه میخواهی پدید آر</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>تو اینجا حاکم بود و وجودی</p></div>
<div class="m2"><p>کنی هر چیز اینجاگه که بودی</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>ولکین راز بسیار است دانم</p></div>
<div class="m2"><p>ترازین کار بس بار است دانم</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>حقیقت شیخ دین شیخ کبیر است</p></div>
<div class="m2"><p>که در معنی و صورت بی نظیر است</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>ببینم تا چه میگوید درین راز</p></div>
<div class="m2"><p>پس آنگه این همی کن ای سرافراز</p></div></div>