---
title: >-
    بخش ۵۸ - راز گفتن شیخ کبیر با شیخ جنید(قس) از هواداری منصور
---
# بخش ۵۸ - راز گفتن شیخ کبیر با شیخ جنید(قس) از هواداری منصور

<div class="b" id="bn1"><div class="m1"><p>چو شیخ این راز بشنید از خداباز</p></div>
<div class="m2"><p>جُنید پیر را گفت ای سرافراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین افتاد اینجا آنچه بینی</p></div>
<div class="m2"><p>شنیدی جمله و صاحب یقینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عجب حالیست در عین زمانه</p></div>
<div class="m2"><p>که این شهباز آمد نشانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه آن مرغست این کز دانه گردد</p></div>
<div class="m2"><p>همی خواهد که دامش در نوردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندیدم مثل این و کس نه بیند</p></div>
<div class="m2"><p>بجز عین زمانه گر نه بیند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وصالش اندر اینجا دست دادست</p></div>
<div class="m2"><p>از آن در کشتن اینجاگاه شاد است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وصالش از تجلی جلالست</p></div>
<div class="m2"><p>فراقش در میانه دید وصالست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنان مستغرق اسرار آمد</p></div>
<div class="m2"><p>که بیخود جملگی بردار آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سراپایش همه دیداردارد</p></div>
<div class="m2"><p>چنین شرح و بیان گفتار دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تودیدی هر صفاتی عین گفتار</p></div>
<div class="m2"><p>که میگوید حقیقت او ز دلدار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه گفتار او از جان جانست</p></div>
<div class="m2"><p>در این سرش حیات جاودانست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همه گفتار او از دید دید است</p></div>
<div class="m2"><p>ابا جانان درین گفت و شنید است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه گفتار او از بهر مرگ است</p></div>
<div class="m2"><p>که این شاه جهان خود کرده ترکست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همه گفتار او در کشتن آمد</p></div>
<div class="m2"><p>از آنش در جهان برگشتن آمد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه صورت لیک جان جان جانست</p></div>
<div class="m2"><p>حقیقت ذات او کل جاودانست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز عهد آدم ای شیخا تودیدی</p></div>
<div class="m2"><p>چنین شخصی بگو از که شنیدی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چنین شخصی کجا آوازه دارد</p></div>
<div class="m2"><p>که جان عاشقان او تازه دارد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>حقیقت فتنهٔ روی زمین است</p></div>
<div class="m2"><p>که از عشاق این کس پیش بین است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ندیدم فتنهٔ چون او بعالم</p></div>
<div class="m2"><p>که میگوید یقین این سردمادم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دمادم راز میگوید عیان باز</p></div>
<div class="m2"><p>که خواهم کشتن اندر عین سرباز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو چوندیدی مر او را باز گو تو</p></div>
<div class="m2"><p>به پیش من حققت راز گو تو</p></div></div>