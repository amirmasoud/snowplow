---
title: >-
    بخش ۸ - در سؤال کردن از هیلاج و جواب دادن او را
---
# بخش ۸ - در سؤال کردن از هیلاج و جواب دادن او را

<div class="b" id="bn1"><div class="m1"><p>بدو گفتم که ای جان چیست نامت؟</p></div>
<div class="m2"><p>که حق داده است اینجا گاه نامت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه نامی بازگو تا بشنوم باز</p></div>
<div class="m2"><p>چه میگوئی بگویم ای سرافراز؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جوابم داد من منصور حلاج</p></div>
<div class="m2"><p>مرانام است در آفاق هیلاج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدو گفتم که ای معنی خدائی</p></div>
<div class="m2"><p>بدانستم که از عین خدائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جوابم داد کای عطار برگوی</p></div>
<div class="m2"><p>مرا بگذار هین اسرار برگوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منم هیلاج ودیگر کدخدایم</p></div>
<div class="m2"><p>تو منصوری و من در تو خدایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کنون بنویس مر اسرار ما را</p></div>
<div class="m2"><p>نگهدارش تو این گفتار ما را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درون جان تو مائیم گویا</p></div>
<div class="m2"><p>توی از من شده در عشق جویا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگفت این آنگهی نزدیکم آمد</p></div>
<div class="m2"><p>چراغی در دل تاریکم آمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدادم بوسهٔ بر دست وبر سر</p></div>
<div class="m2"><p>نهادم بر سر از اسرار افسر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نظر کردم پس آنگه سوی بالا</p></div>
<div class="m2"><p>که تا بینم مبارک سوی هیلا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ندیدم هیچ صورت در میانه</p></div>
<div class="m2"><p>مرا بخشیدش آنگه یک نشانه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کلاهی بد نشانه بر سر ما</p></div>
<div class="m2"><p>که آن باشد بعالم افسر ما</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نشان بود آن کلاه از رب دادار</p></div>
<div class="m2"><p>که سرافرازی از حق شد پدیدار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نشانست آن کلاه از جان جانم</p></div>
<div class="m2"><p>رموز آشکار او نهانم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تأمل کردم از دم در تأمّل</p></div>
<div class="m2"><p>فتادم جان و دل در شور و غلغل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بخود گفتم که هان برخیز و خوشباش</p></div>
<div class="m2"><p>که بنمودست اینک دید نقاش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نمودی بود کاینجاگه نمود او</p></div>
<div class="m2"><p>زهر معنی دری دیگر گشود او</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دری بگشاد از معنی برویت</p></div>
<div class="m2"><p>که آرد دیگر اندر گفتگویت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>حقیقت گفت وهم زو گفت نرگس</p></div>
<div class="m2"><p>که او باشد ترا فریاد رس بس</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کلاه از عیب آمد سر فرازیست</p></div>
<div class="m2"><p>ترا اینجایگه عشقی نه بازیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ترا فهمی دگر دادست هیلاج</p></div>
<div class="m2"><p>حقیقت رخ نمود اینجای هیلاج</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نمودم روی سوی آن دو عالم</p></div>
<div class="m2"><p>چرا خاموش اینجا در کشی دم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دمت بگشای و دمدم جوهر افشان</p></div>
<div class="m2"><p>دل و جان جست برخاک در افشان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از ایمعنی که بخشیدند از نو</p></div>
<div class="m2"><p>از آن حضرت خطاب عشق بشنو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ترا وقتی است چون منصور حلاج</p></div>
<div class="m2"><p>دگر بنمود رخ در عشق هیلاج</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همه دیدار جانانست عطار</p></div>
<div class="m2"><p>حقیقت درد و درمانست عطار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو دردت این زمان درمانست دریاب</p></div>
<div class="m2"><p>چو جانت این زمان جانانست دریاب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو این دم یافتی کام دل خود</p></div>
<div class="m2"><p>تو خواهی بود این دم واصل خود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کنون وصل است دید شادمانی</p></div>
<div class="m2"><p>که میگوئی زهر راز و معانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کنون بگشای دل در عشق و مستی</p></div>
<div class="m2"><p>حقیقت دان تو این یک دم که هستی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مشو بیرون دمی از سیرهیلاج</p></div>
<div class="m2"><p>دمادم یاد میآور ز حلاج</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>فنا خواهی شدن در پایداری</p></div>
<div class="m2"><p>چو او این لحظه اندر پایداری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کلاه عشق دادندت بسر بر</p></div>
<div class="m2"><p>که بینی در خدا این دم سراسر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سرافرازی کن ای بیسر در آخر</p></div>
<div class="m2"><p>که اینجا نیستت همسر در آخر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دمادم مانده از اینجا تو بیرون</p></div>
<div class="m2"><p>حقیقت جوهرت باشد دگرگون</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اساس راه را عطار دارد</p></div>
<div class="m2"><p>که اسرارش همه گفتار دارد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کتابی دیگر است از سر حلاج</p></div>
<div class="m2"><p>که باشد ز آن نهد بر فرق خود تاج</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کتاب آخر است این تا بدانی</p></div>
<div class="m2"><p>اگر تو امزه داری این بخوانی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بخوان با خویش و از خود رنج بردار</p></div>
<div class="m2"><p>تو داری گنج از خود گنج بردار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>توی گنج و چنین محروم مانده</p></div>
<div class="m2"><p>میان کافری مظلوم مانده</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>در اینجا گنج معنی بیشمار است</p></div>
<div class="m2"><p>در آخر دوستان را یادگاراست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بخوان تا آخر و بگشای دیده</p></div>
<div class="m2"><p>مکن باور سخنهای شنیده</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>همه از دیده خوان و از دیده بشنو</p></div>
<div class="m2"><p>اگر مرد رهی از دیده بشنو</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>اباتست آنچه جوئی تا به بینی</p></div>
<div class="m2"><p>در این آخر اگر صاحب یقینی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو در عشقی تو عاشق وار میخوان</p></div>
<div class="m2"><p>اگر بادردآئی رهبر است هان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سخن بادرد خوان و آشنا شو</p></div>
<div class="m2"><p>چه خواندی این کتب کلی فدا شو</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>اساس شرع در اینجاست بنگر</p></div>
<div class="m2"><p>همه اسرارها پیداست بنگر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>جواهرنامه گر خوانی در آخر</p></div>
<div class="m2"><p>وزو گردد یقین منصور ظاهر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>جوابم ده در این معنی که این چون</p></div>
<div class="m2"><p>چگونه دم زد اینجا بیچه و چون</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چگونه گشت واصل در تن تو</p></div>
<div class="m2"><p>چگونه دید ذات روشن تو</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ز وصل او بگو تا ما بدانیم</p></div>
<div class="m2"><p>درین پنهانیش پیدا بدانیم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>جنید اینجا چنین از کار رفته</p></div>
<div class="m2"><p>که همچون نقطه در پرگار رفته</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>اگر این سرّ بگوئی در زمانم</p></div>
<div class="m2"><p>شود مکشوف ای جان و جهانم</p></div></div>