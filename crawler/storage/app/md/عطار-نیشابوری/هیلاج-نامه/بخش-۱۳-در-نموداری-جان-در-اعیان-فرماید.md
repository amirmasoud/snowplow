---
title: >-
    بخش ۱۳ - در نموداری جان در اعیان فرماید
---
# بخش ۱۳ - در نموداری جان در اعیان فرماید

<div class="b" id="bn1"><div class="m1"><p>بتو پیداست جان ای غافل اینجا</p></div>
<div class="m2"><p>گشاده او ترا از خود دل اینجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بتو پیداست جانان مینبینی</p></div>
<div class="m2"><p>از آن مرد درد را درمان نبینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بتو پیداست جانان اندر اینجا</p></div>
<div class="m2"><p>گشاده او ترا در از خود اینجا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترا کی عاشقی خوانم که جانت</p></div>
<div class="m2"><p>بیابد همچو من راز نهانت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترا کی عاشقی خوانم که جان را</p></div>
<div class="m2"><p>ببازی در بر جان و جهان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر مردی دمی از خود برون آی</p></div>
<div class="m2"><p>در این معنی که گفتم در تو بگشای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بمعنی این در جان بازکن تو</p></div>
<div class="m2"><p>همه ذرات را دمساز کن تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درون گنج شو تا گنج یابی</p></div>
<div class="m2"><p>حقیقت گنج خود بیرنج یابی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درون گنج شو بشکن طلسمت</p></div>
<div class="m2"><p>در افکن پردهٔ صورت ز اسمت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درون از گنج شو بیشک حقیقت</p></div>
<div class="m2"><p>یقین مر اژدهای این طبیعت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو تا این اژدهای نفس مردار</p></div>
<div class="m2"><p>نگردانی در اینجا ناپدیدار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کجا یابی خبر از گنج معنی</p></div>
<div class="m2"><p>اگرچه برکشیدی رنج معنی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در این گنجت اگر راهست بنگر</p></div>
<div class="m2"><p>درون گنج شو و از گنج برخور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>درون گنج شو چون سالکان تو</p></div>
<div class="m2"><p>حقیقت گنج بستان رایگان تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از این گنج بقاکان واصلان راست</p></div>
<div class="m2"><p>ز هر صورت پرست بیدل آن راست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بخور برگر توانی خورد ای شیخ</p></div>
<div class="m2"><p>نه بتوانخورد این بیدرد ای شیخ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر این گنج من خوردم حقیقت</p></div>
<div class="m2"><p>که بیشک صاحب دردم حقیقت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر این گنج من خوردم در اینجا</p></div>
<div class="m2"><p>که بیشک صاحب دردم در اینجا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر این گنج من خوردم دگربار</p></div>
<div class="m2"><p>که اینجا میکنم مر عشق تکرار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بر این گنج من خوردم که یارم</p></div>
<div class="m2"><p>از آن گنجست اینجا آشکارم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بر این گنج من خوردم در این سود</p></div>
<div class="m2"><p>ک دیدستم حقیقت دید معبود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر این گنج من خوردم که اویم</p></div>
<div class="m2"><p>درون گنج باشد گفتگویم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر این گنج من خوردم در این راز</p></div>
<div class="m2"><p>که کردستم در این گنج را باز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بر این گنج خوردستم یقین من</p></div>
<div class="m2"><p>که از من شد همه اسرار روشن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بر این گنج من خوردم دمادم</p></div>
<div class="m2"><p>از آنم میزند الله این دم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>منم گنج و طلسم از هم شکسته</p></div>
<div class="m2"><p>حقیقت اژدها از هم گسسته</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>منم گنج و گشاده مر در گنج</p></div>
<div class="m2"><p>منم بیشک حقیقت رهبر گنج</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>منم گنج پر از گوهر ز اسرار</p></div>
<div class="m2"><p>ترا این گنجها آید پدیدار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اگر بیسر شوی گنج تو پیداست</p></div>
<div class="m2"><p>بیابی آن زمان بیشک معماست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو با گنجی ولیکن کی دهد دست</p></div>
<div class="m2"><p>که بیسر گردی زین سر آنگهی هست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اگر مردرهی از خود برون آی</p></div>
<div class="m2"><p>درون جان و دل دیدار بگشای</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تو با گنجی بمانده در میان گم</p></div>
<div class="m2"><p>از آن بی بهرهٔ اندر جهان کم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تو با گنجی و آگاهی نداری</p></div>
<div class="m2"><p>از آن این گوهر شاهی نداری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تو گنجی و بمانده خوار اینجا</p></div>
<div class="m2"><p>کجا گردی تو برخوردار اینجا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تو با گنجی و واصل یافته گنج</p></div>
<div class="m2"><p>ولیکن بر کشیده زحمت و رنج</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تو با گنجی و گنج خود ندیده</p></div>
<div class="m2"><p>کنون اینست میبگشای دیده</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بگنج خود نظر کن تا بیابی</p></div>
<div class="m2"><p>حقیقت گنج را پیدا بیابی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تو گنج خود نظر کن هان و بنگر</p></div>
<div class="m2"><p>که گنجی داری اینجا پر ز گوهر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ترا گنجست پر اسرار معنی</p></div>
<div class="m2"><p>از آن شد دوست برخوردار معنی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ترا گنجست پیدا در بن چاه</p></div>
<div class="m2"><p>چه گویم چون نهٔ از گنج آگاه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اگر آگاه گنجی در جهان تو</p></div>
<div class="m2"><p>به هر جانب مباش اینجا جهان تو</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>اگر آگاه گنجی در بر دوست</p></div>
<div class="m2"><p>حقیقت دان که گنجی اوست از دوست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ترا گنجست داده شاه و بنگر</p></div>
<div class="m2"><p>ولیکن در دل آگاه بنگر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بصد نوعت بگفتم شرح این گنج</p></div>
<div class="m2"><p>نظر کن از سر عین الیقین گنج</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کسی داند که در کل پیش بین است</p></div>
<div class="m2"><p>که این گنج الیقین عین الیقین است</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ترا تا در حقیقت اول کار</p></div>
<div class="m2"><p>نباشد در یکی آئی پدیدار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دم عشق است کاینجا میدهد دوست</p></div>
<div class="m2"><p>عیان جملگی این دم همه اوست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>دم عشقست ای شیخ گزین تو</p></div>
<div class="m2"><p>درین دم آن دمت درخود ببین تو</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>زهی اسرار ما اسراردان کو</p></div>
<div class="m2"><p>حقیقت واصلی اندر جهان کو</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>کزیندم او خبردارست اینجا</p></div>
<div class="m2"><p>مگر عاشق که بردار است اینجا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>خبردارست از آن دم این دم الحق</p></div>
<div class="m2"><p>یقین منصور میگوید اناالحق</p></div></div>