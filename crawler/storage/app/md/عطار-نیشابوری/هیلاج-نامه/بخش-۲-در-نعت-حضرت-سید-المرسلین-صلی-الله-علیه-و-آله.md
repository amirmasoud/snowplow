---
title: >-
    بخش ۲ - در نعت حضرت سید المرسلین صلی الله علیه و آله
---
# بخش ۲ - در نعت حضرت سید المرسلین صلی الله علیه و آله

<div class="b" id="bn1"><div class="m1"><p>ز بعد خالق کون و مکان را</p></div>
<div class="m2"><p>ثنا بر خاتم پیغمبران را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حقیقت صدر بود از آفرینش</p></div>
<div class="m2"><p>که او آمد حقیقت نور بینش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>محمد آنکه نور شرع بنمود</p></div>
<div class="m2"><p>در اینجا عین اصل و فرع بنمود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز ایزد جزء و کل را پیشوا اوست</p></div>
<div class="m2"><p>حقیقت نور چشم انبیا اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کمال شرع او در عالم آمد</p></div>
<div class="m2"><p>دل مجروح جمله مرهم آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چراغ آسمانها و زمین است</p></div>
<div class="m2"><p>ازیرا رحمة للعالمین است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طلبکاریست گردون در بر او</p></div>
<div class="m2"><p>بسر گردنده بر خاک در او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ندیده چشم عالم همچو او نور</p></div>
<div class="m2"><p>درون جزو و کل اویست مشهور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طفیل خندهٔ او آفتابست</p></div>
<div class="m2"><p>غم او کارفرمای سحاب است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جلال و رفعت او بیش از آنست</p></div>
<div class="m2"><p>که گویم از زمین تا آسمانست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرفته نور شرعش قاف تا قاف</p></div>
<div class="m2"><p>فکنده غلغلی در نون ودر کاف</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اساس شرع او آفاق بگرفت</p></div>
<div class="m2"><p>در اینجا گه دل مشتاق بگرفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ندیده انبیا این عزت و ناز</p></div>
<div class="m2"><p>که ازحق یافت اینجا آن سرافراز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سرافراز دو عالم اوست اینجا</p></div>
<div class="m2"><p>حقیقت مغز بد با پوست اینجا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به هر اندیشه او نغز آمد</p></div>
<div class="m2"><p>از آن در آفرینش مغز آمد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شب معراج با حق گفته او راز</p></div>
<div class="m2"><p>برفته سوی او کل آمده باز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بسوی ذات خویشش راه بخشید</p></div>
<div class="m2"><p>مر او را عز و قرب و جاه بخشید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نیابد هیچکس چون او دگر عز</p></div>
<div class="m2"><p>نباشد مثل او در دهر هرگز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زهی بگرفته تیغت ملک عالم</p></div>
<div class="m2"><p>ز تو دیده شرف ابنای آدم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بتو آدم مشرف در زمانه</p></div>
<div class="m2"><p>ز ذات او تو اصلی در میانه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حقیقت آدم آمد طفل راهت</p></div>
<div class="m2"><p>از آن پیوسته باشد در پناهت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خلیل از شوق تو شد سوی آتش</p></div>
<div class="m2"><p>از آن شد گلستان آتش بر و خوش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>توی شاه و همه آفاق خیلاند</p></div>
<div class="m2"><p>توی شاه و همه عالم طفیلاند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دو عالم بهر تو کر دست پیدا</p></div>
<div class="m2"><p>چه نور و ظلمت و چه زشت و زیبا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>طفیلت آفرید ای شاه جمله</p></div>
<div class="m2"><p>که تاگشتی یقین آگاه جمله</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو آگاهی میان جمله ای دوست</p></div>
<div class="m2"><p>جدا کردی حقیقت مغز از پوست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نیابد چشم دنیا چون توسرور</p></div>
<div class="m2"><p>نه بیند نیز کس همچون تو مهتر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>وصال دوست دیدستی حقیقت</p></div>
<div class="m2"><p>ازو آمد یقین عین شریعت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ره وصل تو دیگر هر که بیند</p></div>
<div class="m2"><p>دگر مانند تو رهبر که بیند؟</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>یقین حق را بچشم سر بدیدی</p></div>
<div class="m2"><p>ابا او گفتی و بااو شنیدی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>غم تو بهر امت بود اینجا</p></div>
<div class="m2"><p>طلب شان کردی از معبود اینجا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زهی سرور که چرخ مهر و افلاک</p></div>
<div class="m2"><p>بنزد همتت آمد کف خاک</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کف خاکست نزدت آفرینش</p></div>
<div class="m2"><p>توی اندر میان اسرار بینش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تمامت انبیا را پیشوائی</p></div>
<div class="m2"><p>تو بیشک در میان نور خدائی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زهی معراج تو اسرار بیچون</p></div>
<div class="m2"><p>که دیدی حق تو بی مثیل و چه چون</p></div></div>