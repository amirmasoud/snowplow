---
title: >-
    بخش ۵۷ - در سر صفات بعیان عین الیقین فرماید
---
# بخش ۵۷ - در سر صفات بعیان عین الیقین فرماید

<div class="b" id="bn1"><div class="m1"><p>لقای خالق الخلق قدیمم</p></div>
<div class="m2"><p>که بسم الله الرحمن الرحیمم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا اینجا نباید خویش و پیوند</p></div>
<div class="m2"><p>حقیقت نه زن و نی یار و فرزند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمانم هیچکس را من به تحقیق</p></div>
<div class="m2"><p>دهم هر کس که خواهم عین توفیق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صفاتم بین منزه از همه کس</p></div>
<div class="m2"><p>مرا بنگر تو هم از پیش و از پس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نداند وصف من کردن به جز من</p></div>
<div class="m2"><p>ز اسرارم حقیقت هست روشن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منم منصور شاه آفرینش</p></div>
<div class="m2"><p>حققت عذر خواه آفرینش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیان میگویم این اسرار سرباز</p></div>
<div class="m2"><p>که خواهم باخت اینجا نیک و بد باز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وصالم آفرینش پایدار است</p></div>
<div class="m2"><p>دلم با جان در اینجا بردبار است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سزای خود دهم اینجای با خود</p></div>
<div class="m2"><p>برم یکسانست اینجا نیک یابد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کنون شیخا منم سلطان عالم</p></div>
<div class="m2"><p>یقین هم جان و هم جانان عالم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>منم جان در تن هر کس حقیقت</p></div>
<div class="m2"><p>که باشدجز من اینجاگه حقیقت؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>منم جان در تن این جمله اینجا</p></div>
<div class="m2"><p>همه نادان ومن درخویش دانا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>منم جان در تن ونور دودیده</p></div>
<div class="m2"><p>کسی وصلم در اینجا کل ندیده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که یابد وصل من گر جان شود باز</p></div>
<div class="m2"><p>حقیقت بود ما باشد یقین باز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو شیخا این چنین دان سرّ توحید</p></div>
<div class="m2"><p>که در توحید موجود است تقلید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شنیدستی قیامت را که گویند</p></div>
<div class="m2"><p>قیامت روز امروز است جویند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قیامت روز امروز است اینجا</p></div>
<div class="m2"><p>از آنم بخت پیروز است اینجا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>قیامت روز امروز است بنگر</p></div>
<div class="m2"><p>همه ذرات من نزدیک آور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>قیامت خویشتن داده است کل باز</p></div>
<div class="m2"><p>اگرچه مانده اندر عین ذل باز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>قیامت دیدهٔ امروز او بین</p></div>
<div class="m2"><p>ز من بشنو حقیقت صاحب دین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مبین منصور جز دیدار بیچون</p></div>
<div class="m2"><p>که بنموده است دانا بیچه وچون</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ابی مثلست در آفاق میدان</p></div>
<div class="m2"><p>فتاده اندرین سر طاق میدان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ندارد مثل در آفاق منصور</p></div>
<div class="m2"><p>که بیشک اوفتاده طاق منصور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>درین نه طاق روی او پدید است</p></div>
<div class="m2"><p>ابا تو این زمان گفت و شنید است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مرا ای شیخ دین دیندار اینجا</p></div>
<div class="m2"><p>که میگویم ترا در دار اینجا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جهان میبین تو شادان از رخ من</p></div>
<div class="m2"><p>حقیقت گوش کن این پاسخ من</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بود منصور ذات لایزالی</p></div>
<div class="m2"><p>درین منزل تجلی جلالی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مرا زیبد که اینجا مینماید</p></div>
<div class="m2"><p>در وصلت اینجا میگشاید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در وصلت گشادم می نه بینی</p></div>
<div class="m2"><p>ترا منداد دادم می نه بینی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هنوز اندر کمال شیخ اینجا</p></div>
<div class="m2"><p>نمیدانی یقین گفتار ما را</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کمان بگذار و بنگر دید دیدم</p></div>
<div class="m2"><p>که گویم درحقیقت ناپدیدم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کمان بردار و ما را پیشوا بین</p></div>
<div class="m2"><p>چو منصور اندر اینجا گه خدابین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>منم الله جز من نیست ای خلق</p></div>
<div class="m2"><p>وجودذات من یکیست ای خلق</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خلایق این زمان ما را پرستند</p></div>
<div class="m2"><p>در اینجاهرکه استاداست هستند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خدای خویشتن منصور باشد</p></div>
<div class="m2"><p>درونش بین همه پرنور باشد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خدای جمله منصور است حلاج</p></div>
<div class="m2"><p>نهاده برسر شیخ جهان تاج</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خدای جملگی منصور شیخ است</p></div>
<div class="m2"><p>ولکین در میان منصور شیخ است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کجادانند این سرّ می ندانند</p></div>
<div class="m2"><p>همه منصور را بینند وخوانند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همه منصور دانند از حقیقت</p></div>
<div class="m2"><p>پرستندش همه اندر شریعت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بجز منصور اینجا نیست الله</p></div>
<div class="m2"><p>که از اسرار رحمن است آگاه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خبر تا میدهد ز اسرار اینجا</p></div>
<div class="m2"><p>نمودار است او بردار اینجا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نمودار است رویش باز بیند</p></div>
<div class="m2"><p>پرستیدن اگر صاحب یقیناند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خدا منصور و منصوراست خالق</p></div>
<div class="m2"><p>وصال اینست اینجا ای خلایق</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خلایق جمله درگفتار ماندند</p></div>
<div class="m2"><p>همه در پردهٔ پندار ماندند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>همه در پردهاند و مانده کل باز</p></div>
<div class="m2"><p>در اینجا گاه اندر عین ذل باز</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>منم در پردهٔ جانها حقیقت</p></div>
<div class="m2"><p>پدیدارند جانهای حقیقت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تعالی این چه شور است و چه افغان</p></div>
<div class="m2"><p>که تا افکندهام اندر دل و جان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>خلایق من خدایم تا به بینند</p></div>
<div class="m2"><p>نمودم مینمایم تا به بینند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خلایق من خدایم در نمودار</p></div>
<div class="m2"><p>ز عشق خویش امروزم بر این دار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>خلایق من خدایم چند گویم</p></div>
<div class="m2"><p>همه خواهند تا پیوند جویم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>منم پیوندتان اکنون خلایق</p></div>
<div class="m2"><p>منم جان می ندانند این خلایق</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>صفات ذات من در جمله پیداست</p></div>
<div class="m2"><p>درون جملگی دیدم هویداست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>دگر با شیخ گفت ای پیر رهبر</p></div>
<div class="m2"><p>زمانی باش و ما را باش غمخور</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>زمانی شیخ ما را بیوفا باش</p></div>
<div class="m2"><p>تو بر ما این زمان تو پیشوا باش</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بفرما این زمان کاینجا جُنید است</p></div>
<div class="m2"><p>که سیمرغست اندر خویش صید است</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بسی گفتم نخواهد بُرد فرمان</p></div>
<div class="m2"><p>مرا امروز ای شیخ جهان بان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ز هر گونه ورا میگویمش باز</p></div>
<div class="m2"><p>همی سوزد دلش بر من سرافراز</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نمیدانم ورا معذور دارم</p></div>
<div class="m2"><p>نمیخواهم که وی را دور دارم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>اگر او عاشق کل پاکباز است</p></div>
<div class="m2"><p>حقیقت بیشکی در پایدار است</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بفرماید مرا اینجا قصاص او</p></div>
<div class="m2"><p>که عام الناس را باشد مناص او</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>فتادستند و نادانان راهند</p></div>
<div class="m2"><p>چوامروزی که در دیدار شاهند</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>نمیدانند شاه خود یقین باز</p></div>
<div class="m2"><p>بماندستم درون جان و تن باز</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>مرا دانندصورت راز داند</p></div>
<div class="m2"><p>ازین فکرت از ایشان باز ماند</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چنان در فکر ماندستند اینجا</p></div>
<div class="m2"><p>فتاده از خروش بانگ وغوغا</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بخواهم کرد اکنون یادگارم</p></div>
<div class="m2"><p>برای شیخ هان برروی دارم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>خلایق را بپرس و عالمان باز</p></div>
<div class="m2"><p>یقین از ما گمان از جاهلان باز</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>که منصور است اکنون راز گفته</p></div>
<div class="m2"><p>حقیقت سر جانان بازگفته</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چنان بنموده است امروز او باز</p></div>
<div class="m2"><p>که خواهد گشت اندر عشق جانباز</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>نخواهد باخت جانان روی جانان</p></div>
<div class="m2"><p>فکنده دمدمه درکوی جانان</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>بخواهد باخت جان و سرحقیقت</p></div>
<div class="m2"><p>ندارد هیچ او سر بر حقیقت</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چنین میگوید اینجا پیر حلاج</p></div>
<div class="m2"><p>که امروزم کنید از عشق آماج</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>چو آیم این زمان اندر دل و جان</p></div>
<div class="m2"><p>حقیقت میزنم من دم ز جانان</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>اگر از عاشقان راه مائید</p></div>
<div class="m2"><p>همی امروز کل آگاه مائید</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>نخواهم جان و تن نی عز و نی ذل</p></div>
<div class="m2"><p>بخواهم این زمان انداختن کل</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>دل وجان چون حجاب راه ما بود</p></div>
<div class="m2"><p>کنون هم جان و دل آگاه ما بود</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چو دل آگاه شد هم جان آگاه</p></div>
<div class="m2"><p>شوید آگاه از ما خلق گمراه</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>کنون سر را بگفتم در قصاصم</p></div>
<div class="m2"><p>نظر میکن تو درعین سپاسم</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بگو ای شیخ اکنون چون کبیر است</p></div>
<div class="m2"><p>در اینجا کردهام من بینظیر است</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>نظیرت نیست اندر روی آفاق</p></div>
<div class="m2"><p>مرا این قطب در روی جهان طاق</p></div></div>