---
title: >-
    بخش ۴۱ - در نموداری سر توحید به هر نوع
---
# بخش ۴۱ - در نموداری سر توحید به هر نوع

<div class="b" id="bn1"><div class="m1"><p>تعالی الله منم منصور حلّاج</p></div>
<div class="m2"><p>همه بر رحمت من گشته محتاج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تعالی الله منم خورشید و اختر</p></div>
<div class="m2"><p>مرا گویند کل الله اکبر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تعالی الله منم اینجا خداوند</p></div>
<div class="m2"><p>وجود خویش ازمن جمله پیوند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تعالی الله منم سرّ عیانی</p></div>
<div class="m2"><p>ز من گویند هر شرح و بیانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تعالی الله منم هم نفخ و هم ذات</p></div>
<div class="m2"><p>همی آیم درون جمله ذرات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تعالی الله منم اسرار لائی</p></div>
<div class="m2"><p>نموده درنمود خود خدائی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تعالی الله روح از ماست پیدا</p></div>
<div class="m2"><p>بما پیوسته و یکتاست پیدا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زهی دیدارما با جان و دل حق</p></div>
<div class="m2"><p>منم اینجا حقیقت واصل حق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نداند ذات ما جز ما کسی باز</p></div>
<div class="m2"><p>صفات ماست هم انجام و آغاز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هم انجامم بآغازم سلامت</p></div>
<div class="m2"><p>الست بربکم ما را پیامت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>الست بربّکم گفتم بذرّات</p></div>
<div class="m2"><p>دمیدم در تمامت نفخهٔ ذات</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>الست اندر ازل گفتم ابد را</p></div>
<div class="m2"><p>نمایم چون نمودم نیک و بد را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر آنکس را که خواهم من برانم</p></div>
<div class="m2"><p>هر آنکس را که میخواهم بخوانم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نداند هیچ کس چون خواندهام من</p></div>
<div class="m2"><p>حدیث عشق کلی راندهام من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خداوندی مرا زیبد که دانم</p></div>
<div class="m2"><p>تمامت در یقین راز نهانم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خداوندی مرا زیبد به اسرار</p></div>
<div class="m2"><p>که هستم آفرینش رانگهدار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز صنعم آفرینش جمله پیداست</p></div>
<div class="m2"><p>ز نور ذاتم اینجاگه هویداست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مه و خورشید و چرخم با ستاره</p></div>
<div class="m2"><p>صفاتم جمله ذراتم نظاره</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یکی ذانم منزه در همه من</p></div>
<div class="m2"><p>فکنده در تمامت دمدمه من</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بمن آمد تمامت آفرینش</p></div>
<div class="m2"><p>منم در جملگی آثار بینش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز کنه ذرات من اینجا نشان نیست</p></div>
<div class="m2"><p>بجز از جان جان بر من نشان نیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نشان دارم صور گر باز دانند</p></div>
<div class="m2"><p>مرا بینند و از من راز دانند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دوئی نبود مرا کاینجا یکیام</p></div>
<div class="m2"><p>حقیقت جزو با کل بیشکیام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>صفاتم کس ندیده کس نه بیند</p></div>
<div class="m2"><p>اگرچه عقل بسیاری نشیند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در اینجا بهر دیدن بر سر راه</p></div>
<div class="m2"><p>کجا گردد دوی ز اسرار آگاه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>منم اسرار خود اینجا نموده</p></div>
<div class="m2"><p>درون جانها پیدا نموده</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>منم اسرار خود بنموده اینجا</p></div>
<div class="m2"><p>ابا خود گفته و بشنوده اینجا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>منم ذرات در خورشید عالم</p></div>
<div class="m2"><p>دمیده از دم خود در همه دم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زهی فرد حضور نورذاتم</p></div>
<div class="m2"><p>که آدم بود در عین صفاتم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>حقیقت آدم آمد ذات ماراست</p></div>
<div class="m2"><p>دراینجا علم الاسماء ما راست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>حقیقت بایزید اینجا خبردار</p></div>
<div class="m2"><p>تو بردار من و از من خبردار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اناالحق میزنم اینجای دیگر</p></div>
<div class="m2"><p>مرا در مأمن و مأوای بنگر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اناالحق میزنم از جان گذشته</p></div>
<div class="m2"><p>بساط جزو و کل را در نوشته</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>اناالحق میزنم در کایناتم</p></div>
<div class="m2"><p>حقیقت ذاتم و عین صفاتم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اناالحق میزنم بیچون منم هان</p></div>
<div class="m2"><p>که بنمودم حقیقت نص و برهان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو حق در جان من گوید اناالحق</p></div>
<div class="m2"><p>ترا میگوید اینجاراز مطلق</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو درجانست جانان بنگر اکنون</p></div>
<div class="m2"><p>فکنده نور خود در هفت گردون</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>درون تو چو جانانست بنگر</p></div>
<div class="m2"><p>وجوداوست آسانست بنگر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چه آسان تر ازین که جمله جانان</p></div>
<div class="m2"><p>که تو اوئی که چه اسرار پنهان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>در آن دم روی دریا باز بینی</p></div>
<div class="m2"><p>که پرده از رخ جان باز بینی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو پرده برگرفت از رخ بیکبار</p></div>
<div class="m2"><p>جمال بینشان آید پدیدار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>جمال بینشان اینست بنگر</p></div>
<div class="m2"><p>درون جان هویدا است بنگر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از اول تا بآخر لا گرفته است</p></div>
<div class="m2"><p>حقیقت لا همه الّا گرفته است</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز اول تا بآخر ذات بیچون</p></div>
<div class="m2"><p>نمودی از صفاتش هفت گردون</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>از اول تا بآخر در یکی باز</p></div>
<div class="m2"><p>نظر میکن بیاب انجام و آغاز</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>از اول تا بآخر در یکی بین</p></div>
<div class="m2"><p>همه جانست اینجا بیشکی بین</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز اول تا بآخر یک دم آمد</p></div>
<div class="m2"><p>کمال این حقیقت آدم آمد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>از آن دم یافت آدم روشنائی</p></div>
<div class="m2"><p>از اینجا دید زاندم آشنائی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>از آن دم یافت آدم لام اینجا</p></div>
<div class="m2"><p>از آن دم آدم آمد جام اینجا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو جام معرفت را داد دادم</p></div>
<div class="m2"><p>حقیقت بازدید اینجای آن دم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>حقیقت باز بین اینجای ذاتم</p></div>
<div class="m2"><p>که من مجموعهٔ ذات و صفاتم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>حقیقت بایزید آن دم مرا بین</p></div>
<div class="m2"><p>تو بیشک آن زمان آدم مرا بین</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>دمادم بازگشتم سوی آدم</p></div>
<div class="m2"><p>دم من بد دراینجا نام آن دم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>هزاران طور گشتم در زمانی</p></div>
<div class="m2"><p>بمردم یافتم عین مکانی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>از اول تا بآخر باز گشتم</p></div>
<div class="m2"><p>در اینجا گاه صاحب راز گشتم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چودیدم باز آن دم در یقین من</p></div>
<div class="m2"><p>شدم جمله در اشیا پیش بین من</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چو اینجا پیش بین گشتم در اسرار</p></div>
<div class="m2"><p>ز سر خود شدم اینجا خبردار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>فراقم در وصال اینجا عیان بود</p></div>
<div class="m2"><p>اگرچه نقشم اندر بی نشان بود</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نشان را محو کردم بینشانی</p></div>
<div class="m2"><p>حقیقت ماند جانم در نهانی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چوذات خویشتن کردم تماشا</p></div>
<div class="m2"><p>حقیقت جزؤم و کلی هویدا</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>زجز واینجایگه اکنون شدم کل</p></div>
<div class="m2"><p>بکردم اختیار خویشتن ذُلّ</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چو ذاتم اختیار افتاد اینجا</p></div>
<div class="m2"><p>از آن ای دوست یار افتاد اینجا</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>هر آنکو اختیار آمد درین راه</p></div>
<div class="m2"><p>حقیقت دید یار آمد درین راه</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چه به زین تا ترا جانان بود دوست</p></div>
<div class="m2"><p>توئی تو درین ره بیشکی اوست</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چو کل کردم دراینجا اختیارم</p></div>
<div class="m2"><p>نه بیند هیچ جز دیدار یارم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>همه مائیم اینجابا یزیدم</p></div>
<div class="m2"><p>درونم با برون گفت وشنیدم</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>تو اکنون قطره شو در دید جانم </p></div>
<div class="m2"><p>که من در ظاهر و باطن عیانم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>تو اکنون قطره شو در دید دریا</p></div>
<div class="m2"><p>تو جزوی کل شو از من هان هویدا</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>تو کل شو بایزید و جزو بگذار</p></div>
<div class="m2"><p>تو جان بایزید وعضو بگذار</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چو کل گردی چو من میگوی مطلق</p></div>
<div class="m2"><p>در درون جان ما با ما اناالحق</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>اناالحق چون زدی بر راستی تو</p></div>
<div class="m2"><p>همه بازار ما آراستی تو</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>درین بازار اگر زاری تو ما را</p></div>
<div class="m2"><p>برون خویش بازاری تو ما را</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>اناالحق کردی و بیجان شو چو ماتو</p></div>
<div class="m2"><p>یکی میبین در این عین فنا تو</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>چو اینجا گه بگفتی کل اناالحق</p></div>
<div class="m2"><p>همین باشد حقیقت راژ مطلق</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>فنا باش و بقا میجوی اینجا</p></div>
<div class="m2"><p>همی سرّ لقا میگوی اینجا</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چو شد بر تو حقیقت راز ما فاش</p></div>
<div class="m2"><p>تو در نقشی و ما باشیم نقاش</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چو نقش خویش اینجا در فکندی</p></div>
<div class="m2"><p>شوی آزاد از این مستمندی</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>تو حق باشی و من درحق یکی باز</p></div>
<div class="m2"><p>ز من دریاب این عین الیقین باز</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>سرافرازی کن و سر را ببر تو</p></div>
<div class="m2"><p>که هستی جوهر و هم بحرُ در تو</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چو جانست این زمان جوهر درین راز</p></div>
<div class="m2"><p>ز من دریاب این حق الیقین باز</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>چو جانت جوهر است و بحرمائیم</p></div>
<div class="m2"><p>گه این جوهر درونت مینمائیم</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>درین بحری تو اکنون بازمانده</p></div>
<div class="m2"><p>چو جوهر در صدفها باز مانده</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>صدف بشکن اگر جوهر تو خواهی</p></div>
<div class="m2"><p>که بیشک بهره زو یابند و شاهی</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>چو بشکستی صدف جوهر ببینی</p></div>
<div class="m2"><p>چنین کن هان اگر صاحب یقینی</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>بسی مردند وین جوهر ندیدند</p></div>
<div class="m2"><p>چنین کن هان اگر صاحب یقینی</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>بسی مردند وین جوهر ندیدند</p></div>
<div class="m2"><p>درون بحر مرده آرمیدند</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>هر آنکو یافت جوهر همچو ماشد</p></div>
<div class="m2"><p>حقیقت جوهر اسرار لا شد</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>بصد قرن این چنین جوهر نیابند</p></div>
<div class="m2"><p>بسی جویند خشک و تر نیابند</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>نه آنست این بیان که کس بداند</p></div>
<div class="m2"><p>یقین منصور دیگر کس نداند</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>اگرچه من کنون منصور عشقم</p></div>
<div class="m2"><p>حقیقت غرقه اندر نو رعشقم</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>حقیقت جوهر خودباز دیدم</p></div>
<div class="m2"><p>چو جوهر بود خود را باز دیدم</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>چوجانت جوهر است اینجا حقیقت</p></div>
<div class="m2"><p>نگر این بحر درغوغا حقیقت</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>چو جوهر جان بود اینجا به تحقیق</p></div>
<div class="m2"><p>ز جان جان بدیده سر توفیق</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>رسیده سوی یار و او شده فاش</p></div>
<div class="m2"><p>ز جسم و جان حقیقت دید نقاش</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>حقیقت دید جان دیدار یار است</p></div>
<div class="m2"><p>در اینجا دیدن جانان بکار است</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>حقیقت دید جان دیدار جانست</p></div>
<div class="m2"><p>در اینجا دید جانان باز دانست</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>در اینجا بازدید و یار شد او</p></div>
<div class="m2"><p>ز بود خویشتن بیزار شد او</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>در اینجا یار دید و آشنا شد</p></div>
<div class="m2"><p>عیانی محو کرد و کل خدا شد</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>خدا شد جان ابا منصور اینجا</p></div>
<div class="m2"><p>خدا منصور را مهجور اینجا</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>خدا شد کرد او اسرار آفاق</p></div>
<div class="m2"><p>که تا افتاد همچون بود او طاق</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>خدا شد این زمان منصور در عشق</p></div>
<div class="m2"><p>درون جزو و کل مشهور در عشق</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>خدا شد این زمان تا بار دیده است</p></div>
<div class="m2"><p>حقیقت خویش برخوردار دیده است</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>خدا شد در خدائی زد اناالحق</p></div>
<div class="m2"><p>ابا ذرات گفت او راز مطلق</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>خدا شد تا مکان را بیمکان دید</p></div>
<div class="m2"><p>همه جان بود و خود از جان جان دید</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>خدا شد تا یکی آمد پدیدار</p></div>
<div class="m2"><p>خدای بیشکی آمد پدیدار</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>چودرعین خدائی پاکبازیم</p></div>
<div class="m2"><p>حقیقت ما در اینجا پاک بازیم</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>ز عشق خویشتن خود آفریدیم</p></div>
<div class="m2"><p>جمال خود هر آیینه بدیدیم</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>بعشق خود زهر آیینه دم دم</p></div>
<div class="m2"><p>نمودم سر عشق خود بآدم</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>بعشق خویش اینجا درنمودم</p></div>
<div class="m2"><p>نمودم سر عشق خود بآدم</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>بعشق خویش اینجا در نمودم</p></div>
<div class="m2"><p>درون جمله خود گفت و شنودم</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>چو در صنعم کنون پیدا در اینجا</p></div>
<div class="m2"><p>یقین کردم چنین غوغا در اینجا</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>ره عشقم چنین است ار به بینی</p></div>
<div class="m2"><p>همه تلخست اگر صاحب یقینی</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>فراقم دروصال آمد پدیدار</p></div>
<div class="m2"><p>وصالم عاشق اینجا شد خبردار</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>حقیقت شرح جان گفتم ترا من</p></div>
<div class="m2"><p>که تا شد سر جان ز اسرار روشن</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>ندارد نقش جان نقاش بشناس</p></div>
<div class="m2"><p>جمال ماست اینجا فاش بشناس</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>از این ظلمت که تن خوانند بگریز</p></div>
<div class="m2"><p>بنور ذات حق خود را در آویز</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>ازین ظلمت که تن خوانند برون آی</p></div>
<div class="m2"><p>همه ذرات ما را رهنمون آی</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>از این ظلمت اگر آئی برون تو</p></div>
<div class="m2"><p>ابا ما گردی اینجا خاک و خون تو</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>چو تن دیدی وجان بشناختی باز</p></div>
<div class="m2"><p>تنت در سوی جان انداختی باز</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>تن اینجا ظلمت و جانت ز نوراست</p></div>
<div class="m2"><p>نفور است این تن وجان کل حضور است</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>حضور جان طلب نی ظلمت تن</p></div>
<div class="m2"><p>که جان آمد حقیقت نور روشن</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>چو نور افروزد اینجا صبحگاهان</p></div>
<div class="m2"><p>نظر میکن تو در خورشید تابان</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>نه چندانی که چونخور میبرآید</p></div>
<div class="m2"><p>کجا ظلمت در اینجاگه نماید</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>نماید هیچ ظلمت نزد خورشید</p></div>
<div class="m2"><p>حقیقت محو گردد سایه جاوید</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>چو خورشید عیان آید پدیدار</p></div>
<div class="m2"><p>حقیقت سایه گردد ناپدیدار</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>حقیقت سایهٔ صورت برافتد</p></div>
<div class="m2"><p>نقاب از روی منصورت برافتد</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>تو از جانان بیابی راز منصور</p></div>
<div class="m2"><p>یکی گردی بکل نور علی نور</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>اگر این سر بدانی بایزیدی</p></div>
<div class="m2"><p>از این اسرارها هل من مزیدی</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>حقیقت در خدائی رهبری تو</p></div>
<div class="m2"><p>هم از کون و مکانت بگذری تو</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>مرا پایت یکی گردد باسرار</p></div>
<div class="m2"><p>ترا اسرار ما آید پدیدار</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>سراپایت یکی گردد چو فرموک</p></div>
<div class="m2"><p>چو مردان ترک گیری پنبهٔ دوک</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>سراپایت یکی گردد چو خورشید</p></div>
<div class="m2"><p>بمانی تو ز ذات اینجا تو جاوید</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>سراپایت یکی گردد چو ماهی</p></div>
<div class="m2"><p>زنی بر هفت گردون پایگاهی</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>سراپایت یکی گردد ز بینش</p></div>
<div class="m2"><p>تو باشی مغز کل آفرینش</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>سراپایت یکی گردد چو من پاک</p></div>
<div class="m2"><p>نماند هیچ نار و آب با خاک</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>سراپایت یکی باشد به هر چار</p></div>
<div class="m2"><p>بوصل خود بوند ایشان گرفتار</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>سراپایت یکی باشد نهانی</p></div>
<div class="m2"><p>تو باشی بود خود اما چه دانی</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>چو در یکی جمال خود بدیدی</p></div>
<div class="m2"><p>چو ما اینجا وصال خود بدیدی</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>چو در یکی تو باشی خود یقین دان</p></div>
<div class="m2"><p>تو بود خویش از ما بیشکی دان</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>یکی دانست بود ما همه را</p></div>
<div class="m2"><p>نهاده در درونه دمدمه را</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>چو شور است آنکه خود را راست کردم</p></div>
<div class="m2"><p>بدار عشق خود را راست کردم</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>چه شور است اینکه درجانها فکندیم</p></div>
<div class="m2"><p>که در هر قطره طوفانها فکندیم</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>چه شور است آن که این فانیست بنگر</p></div>
<div class="m2"><p>بجز ما جسم و جانت نیست بنگر</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>بعشق خویش شور انگیز خویشم</p></div>
<div class="m2"><p>حیققت نیک و بد یکیست پیشم</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>چو یکسانست پیشم نیک یا بد</p></div>
<div class="m2"><p>هر آنچیزی که کردم کردهام خود</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>یکی جانم گهی جسم و گهی دل</p></div>
<div class="m2"><p>مرا مقصود هر چیز است حاصل</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>چو مقصود من اینجا ذات آمد</p></div>
<div class="m2"><p>یکی ذاتم که این آیات آمد</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>بیان این معانی کرد آگاه</p></div>
<div class="m2"><p>صفات ذات پاکم قل هوالله</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>منم در قل هو الله راز دیده</p></div>
<div class="m2"><p>در اینجا گه هوالله باز دیده</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>منم در قل هوالله راز گفته</p></div>
<div class="m2"><p>اناالحق در عیانم باز گفته</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>چو ذاتم قل هوالله است بنگر</p></div>
<div class="m2"><p>نمود من هوالله است بنگر</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>نموم از هوالله است پیدا</p></div>
<div class="m2"><p>عیانم قل هوالله است پیدا</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>یکی ذاتست کاین راز است بیچون</p></div>
<div class="m2"><p>که من گفتم ابا تو بیچه و چون</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>چو جان از نور من در روشنائی است</p></div>
<div class="m2"><p>درآ در عاقبت دیدخدائی است</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>چو جان از نور من در قربت آمد</p></div>
<div class="m2"><p>از آن درحضرت و در غربت آمد</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>گهی گردد فلک گه مهر و گه ماه</p></div>
<div class="m2"><p>گهی باشد زمین گه کوه و گه کاه</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>گهی نور است و گاهی عین ظلمت</p></div>
<div class="m2"><p>گهی دریاست گاهی عز و قربت</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>گهی جان و دل آید گه بود جان</p></div>
<div class="m2"><p>دل وجان شد یقین امروز جانان</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>منم جانان یقین اینست رازم</p></div>
<div class="m2"><p>ز هر نوعی یقینت گفته بازم</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>منم جانان تو کاینجا بدیدم</p></div>
<div class="m2"><p>ترا اسمای اعظم بایزیدم</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>منم جانان تو از جان آگاه</p></div>
<div class="m2"><p>بکردستم ز جان و دل مرا خواه</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>دمی زد بعد از آن خاموش گشته</p></div>
<div class="m2"><p>ز عشق ذات خود بیهوش گشته</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>چنان بیهوش و باهوشی از آن داشت</p></div>
<div class="m2"><p>که بیشک در صور کون و مکان داشت</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>چنان در قربت او راه دیده</p></div>
<div class="m2"><p>دراینجاگه جمال شاه دیده</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>در اینجا در برون و دردرون راز</p></div>
<div class="m2"><p>که اینجا آمده در عشق شهباز</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>دمی دیگر بزد پس گفت الله</p></div>
<div class="m2"><p>اناالحق گفت و دیگر قل هوالله</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>بخواند و کرد خوداندر دمیدش</p></div>
<div class="m2"><p>جوابی داد بیشک بایزیدش</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>بدو گفتا چرا خاموش گشتی</p></div>
<div class="m2"><p>چو من اینجا عجب مدهوش گشتی</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>چنان خواهم که با من راز گوئی</p></div>
<div class="m2"><p>سؤالم در شریعت بازگوئی</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>بپرس آنچه ندانی تا بگویم</p></div>
<div class="m2"><p>دوای دردت اینجاگه بجویم</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>دمی کین جایگه از عمر مانده است</p></div>
<div class="m2"><p>بصورت لیک دایم جان بمانده است</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>سؤالی کن ز وحدت گر توانی</p></div>
<div class="m2"><p>تو منگر سوی کثرت گر توانی</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>همه ذرات خود را دان تو کثرت</p></div>
<div class="m2"><p>ز کثرت در گذر شو سوی وحدت</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>که در حضرت بیابی آنچه خواهی</p></div>
<div class="m2"><p>ترا بخشد کمال پادشاهی</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>هر آنکو سوی دنیا باز ماند</p></div>
<div class="m2"><p>ز کثرت هر کجا اوراز داند</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>همه دنیا پر از کثرت نمودم</p></div>
<div class="m2"><p>درین کثرت یقین وحدت نمودم</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>کسانی چند کثرت راز وحدت</p></div>
<div class="m2"><p>یکی دانند در اسرار قربت</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>ولیکن صاحب شرع اندر اینجا</p></div>
<div class="m2"><p>توئی گفتست اصل و فرع اینجا</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>حقیقت اصل اینجا بهتر آمد</p></div>
<div class="m2"><p>حقیقت شرع اینجا برتر آمد</p></div></div>
<div class="b" id="bn180"><div class="m1"><p>از آن گفتم که فرع صورت خود</p></div>
<div class="m2"><p>چو مردان دیدهام در راه جان بد</p></div></div>
<div class="b" id="bn181"><div class="m1"><p>بد از خود دور کردم تا بدانند</p></div>
<div class="m2"><p>کنون در عشق فردم تا بدانند</p></div></div>
<div class="b" id="bn182"><div class="m1"><p>بد و نیکم کنون یکسانست در عشق</p></div>
<div class="m2"><p>کنون اسرار مادرجانست در عشق</p></div></div>
<div class="b" id="bn183"><div class="m1"><p>ز کثرت درگذر وحدت نظر کن</p></div>
<div class="m2"><p>نظر اینجا سوی صاحب خبر کن</p></div></div>
<div class="b" id="bn184"><div class="m1"><p>همه دنیا بیک جو زر نیرزد</p></div>
<div class="m2"><p>چو یک جوزر که خاکستر نیرزد</p></div></div>
<div class="b" id="bn185"><div class="m1"><p>چو دنیا نزد من چون برگ کاهست</p></div>
<div class="m2"><p>مرا دنیا حقیقت عذر خواه است</p></div></div>
<div class="b" id="bn186"><div class="m1"><p>درین دنیا نمانم تا بدانی</p></div>
<div class="m2"><p>که من بودم همه راز نهانی</p></div></div>
<div class="b" id="bn187"><div class="m1"><p>درین دنیاست بیشک عاشقان را</p></div>
<div class="m2"><p>که بیشک صورتی بیند آن را</p></div></div>
<div class="b" id="bn188"><div class="m1"><p>در این دنیاست دیدار خدائی</p></div>
<div class="m2"><p>اگر نبود چو منصورت جدائی</p></div></div>
<div class="b" id="bn189"><div class="m1"><p>در این دنیاست بیشک شور و غوغا</p></div>
<div class="m2"><p>حقیقت گفتن بیهوده پیدا</p></div></div>
<div class="b" id="bn190"><div class="m1"><p>ز پر گفتست اندر دار دنیا</p></div>
<div class="m2"><p>نیرزد نزد عاشق یار دنیا</p></div></div>
<div class="b" id="bn191"><div class="m1"><p>بیک ارزن که دنیا ارزنی هست</p></div>
<div class="m2"><p>بنزد عقل کین دنیا زنی هست</p></div></div>
<div class="b" id="bn192"><div class="m1"><p>تو این دنیا زنی دان ای برادر</p></div>
<div class="m2"><p>یقین چون ارزنی دان ای برادر</p></div></div>
<div class="b" id="bn193"><div class="m1"><p>همه دنیا کف خاکست بنگر</p></div>
<div class="m2"><p>چه غم چون حضرت پاکست بنگر</p></div></div>
<div class="b" id="bn194"><div class="m1"><p>حقیقت درگه پروردگار است</p></div>
<div class="m2"><p>مرین دنیا اگرچه رهگذار است</p></div></div>
<div class="b" id="bn195"><div class="m1"><p>چو مردان زن قدم در آشنائی</p></div>
<div class="m2"><p>که باشد آشنائی روشنائی</p></div></div>
<div class="b" id="bn196"><div class="m1"><p>چو گشتی آشنای یار اینجا</p></div>
<div class="m2"><p>تو منگر برجفای یار اینجا</p></div></div>
<div class="b" id="bn197"><div class="m1"><p>حقیقت برجفای او وفایست</p></div>
<div class="m2"><p>وفای تو یقین عین لقایست</p></div></div>
<div class="b" id="bn198"><div class="m1"><p>اگر می واصلی خواهی در اینجا</p></div>
<div class="m2"><p>که بگشاید ترا بیشک در اینجا</p></div></div>
<div class="b" id="bn199"><div class="m1"><p>دمی اینجا قدم بی او مزن تو</p></div>
<div class="m2"><p>وگر بی او زنی باشی چو زن تو</p></div></div>
<div class="b" id="bn200"><div class="m1"><p>زنی باشد که او خود دم زند باز</p></div>
<div class="m2"><p>کجا گردد چو مردان او سرافراز</p></div></div>
<div class="b" id="bn201"><div class="m1"><p>سرافرازی عالم مرد دارد</p></div>
<div class="m2"><p>عیان عشق صاحب درد دارد</p></div></div>
<div class="b" id="bn202"><div class="m1"><p>هر آنکو درد دارد اندرین دار</p></div>
<div class="m2"><p>درونت درد او گیرد بیکبار</p></div></div>
<div class="b" id="bn203"><div class="m1"><p>چو در دردت یقین در ما نماید</p></div>
<div class="m2"><p>از اول جان و دل شیدا نماید</p></div></div>
<div class="b" id="bn204"><div class="m1"><p>ز درد عشق اگر جانت خبر یافت</p></div>
<div class="m2"><p>همه در یک حقیقت در نظر یافت</p></div></div>
<div class="b" id="bn205"><div class="m1"><p>همه مردان ز درد اوست دایم</p></div>
<div class="m2"><p>برون جسته چو مغز از پوست دایم</p></div></div>
<div class="b" id="bn206"><div class="m1"><p>ز درد اینجا شوند از خویش بیزار</p></div>
<div class="m2"><p>نماند تن بماند جان و دیدار</p></div></div>
<div class="b" id="bn207"><div class="m1"><p>حقیقت بایزیدا دردداری</p></div>
<div class="m2"><p>ترا گویم که جان خرد داری</p></div></div>
<div class="b" id="bn208"><div class="m1"><p>نکو بشنو تو و باطن سخنگوی</p></div>
<div class="m2"><p>که بودم بیشکی اندر سخن گوی</p></div></div>