---
title: >-
    بخش ۲۸ - در خلوت و عزلت ودیدار الوهیت گوید
---
# بخش ۲۸ - در خلوت و عزلت ودیدار الوهیت گوید

<div class="b" id="bn1"><div class="m1"><p>بکنج خلوت دل باش ساکن</p></div>
<div class="m2"><p>که تا باشی ز هر آفات ایمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بکنج خلوت دل گرد واصل</p></div>
<div class="m2"><p>تو در خلوت بکن مقصود حاصل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بکنج خلوت دل راز میجوی</p></div>
<div class="m2"><p>همان گم کردهٔ خود باز میجوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بکنج خلوت دل یار میبین</p></div>
<div class="m2"><p>یقین بیزحمت اغیار میبین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بکنج خلوت خود در یکی باش</p></div>
<div class="m2"><p>تو ذات صرف اینجا بیشکی باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بکنج خلوت دل جوی جانان</p></div>
<div class="m2"><p>که بنماید رخت ناگاه سلطان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو درخلوت سرای جان درآئی</p></div>
<div class="m2"><p>حقیقت بنگری دید خدائی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به از خلوت مدان اینجا حقیقت</p></div>
<div class="m2"><p>حضوری جوی بی عین طبیعت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به از خلوت مدان گر راز دانی</p></div>
<div class="m2"><p>که درخلوت رسد سر معانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به از خلوت چه باشد نزد عشاق</p></div>
<div class="m2"><p>که در خلوت شدند ایشان یقین طاق</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حضور خلوت از بازار خوشتر</p></div>
<div class="m2"><p>حقیقت زندگی با یار خوشتر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حضور خلوت اینجاگه طلب کن</p></div>
<div class="m2"><p>دلت با جان حقیقت با ادب کن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حضور خلوت عشاق در یاب</p></div>
<div class="m2"><p>ازین عین دوئی خود طاق دریاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دمی با یار به اندر خلوت دل</p></div>
<div class="m2"><p>به از بغداد و مصر و چین و موصل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دمی با یار اندر خلوت عشق</p></div>
<div class="m2"><p>کزویابی حقیقت قربت عشق</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دمی با یار به ازملک عالم</p></div>
<div class="m2"><p>چه میگوئی چه میجوئی در این دم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بخلوت جوی یار خویشتن باز</p></div>
<div class="m2"><p>گذر کن هان ز جسم و جان و تن باز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بخلوت یک زمان بنشین تو فارغ</p></div>
<div class="m2"><p>که در خلوت شوی ای شیخ بالغ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>حضور خلوتست اینجای بنگر</p></div>
<div class="m2"><p>توی در خلوت یکتای بنگر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نموددوست درخلوت عیانست</p></div>
<div class="m2"><p>که درخلوت یقین دیدار جانست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بسی در خلوت اینجا چله دارند</p></div>
<div class="m2"><p>هوای صورتی در کله دارند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نیرزد خلوت ایشان پشیزی</p></div>
<div class="m2"><p>چنین گفتست با من آن عزیزی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>که در خلوت نشستن آن نشاید</p></div>
<div class="m2"><p>که جز جانان نه بیند دید باید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هوای غیر نبود در درونش</p></div>
<div class="m2"><p>بجز یک سیر نبود در درونش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بجانان ذات او قائم نماید</p></div>
<div class="m2"><p>نمود او یقین دایم نماید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چنان دست از همه عالم بشوید</p></div>
<div class="m2"><p>که جز اسرار با جانان نگوید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اباجانان دمادم گوید او راز</p></div>
<div class="m2"><p>که تا جانان کند اورا سرافراز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ابا جانان چنان باشد یگانه</p></div>
<div class="m2"><p>که با جانان بماند جاودانه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ابا جانان چنان مشتاق باشد</p></div>
<div class="m2"><p>که در جانان حقیقت طاق باشد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ابا جانان شودیکتای جانان</p></div>
<div class="m2"><p>ز پنهانی بود پیدای جانان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ابا جانان بود یکتای این جا</p></div>
<div class="m2"><p>یکی بیند همه ذرات اینجا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>حضور جان و دل دارد چنان گم</p></div>
<div class="m2"><p>که باشد بیگمان مانند قلزم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>حضورش از یکی آید پدیدار</p></div>
<div class="m2"><p>شود در هر دو عالم صاحب اسرار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>حضورش بیشکی در یک نماید</p></div>
<div class="m2"><p>ز دید عشق ما پیدا نماید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همه چیزی ازو یکتا بود کل</p></div>
<div class="m2"><p>ز دید عشق ناپیدا بود کل</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از اول تا به آخر یار بیند</p></div>
<div class="m2"><p>یکی اندر یکی دیدار بیند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بجز یکی نداند در حقیقت</p></div>
<div class="m2"><p>بجای آرد همه شرط شریعت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اگر بیشرع آید فرع دانش</p></div>
<div class="m2"><p>بجز زندیق در این سر مخوانش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اگر بسپارد اینجا گه ره شرع</p></div>
<div class="m2"><p>بخلوت در بیابد مرشه شرع</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چنین کردند اینجا پاکبازان</p></div>
<div class="m2"><p>ره تحقیق جسته کارسازان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>حقیقت چون در خلوت نشینی</p></div>
<div class="m2"><p>یقین باید که جز یکی نه بینی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بشرع احمد اینجا پاکدل باش</p></div>
<div class="m2"><p>تو اندر پاکبازی یاب نقاش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>حضور خلوت از روی زمین به</p></div>
<div class="m2"><p>ز ذات کل یقین عین الیقین به</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو در خلوت نشینی پیشه سازی</p></div>
<div class="m2"><p>ز ذات کل حقیقت سرفرازی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نه اندر بند آن باشی که آن دست</p></div>
<div class="m2"><p>ترا بوسند درخلوت جهان دست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بت ره باشی آن دم نزد جانان</p></div>
<div class="m2"><p>کجا بستانی آنگه مرد جانان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بت خود بشکن از دیدار بیشک</p></div>
<div class="m2"><p>ز جانان باش برخوردار بیشک</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>حقیقت بت ترا مر دوست آمد</p></div>
<div class="m2"><p>از آن مغزت حقیقت پوست آمد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>که خود را دوست داری در برخلق</p></div>
<div class="m2"><p>همی ترسی تو از خیر و شر خلق</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>اگر از عین دنیا این تمامت</p></div>
<div class="m2"><p>که خواهی تا بماندنیک نامت</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بنام وننگ اینجا در نمازی</p></div>
<div class="m2"><p>تو پنداری که بیشک کارسازی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بنام وننگ جانت رفت بر باد</p></div>
<div class="m2"><p>کجا بینی تو ذات خویش آباد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بنام و ننگ در مکری بمانده</p></div>
<div class="m2"><p>ز سرّ عشق یک نکته نخوانده</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بنام وننگ میخواهی بسربرد</p></div>
<div class="m2"><p>بنام و ننگ خواهی بیخبر مرد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ز ننگت چیست چون نامی نداری</p></div>
<div class="m2"><p>بجز حسرت دگر کامی نداری</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>تو از بهر ریای خلق تحقیق</p></div>
<div class="m2"><p>بپوشیدی حقیقت دلق تحقیق</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>یقین دلق تو زنار است اینجا</p></div>
<div class="m2"><p>ابا تو لایق نار است اینجا</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بسوزان دلق آنگه خود بسوزان</p></div>
<div class="m2"><p>تو نام نیک را و بد بسوزان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>اگر رویت حقیقت در خدایست</p></div>
<div class="m2"><p>ابا اوباش کو خود رهنمایست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چرا در بند خلقی بازمانده</p></div>
<div class="m2"><p>در آن خلوتسرای راز مانده</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>طمع یکبارگی باید بریدن</p></div>
<div class="m2"><p>ز خلق آنگه جمال شاه دیدن</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>طمع زین ناگهان آخر ببر تو</p></div>
<div class="m2"><p>شنو این نکتهای همچو در تو</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>طمع زینها ببر اینجا به تحقیق</p></div>
<div class="m2"><p>که به زینت ندیدم هیچ توفیق</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بکار تو کجا آیند اینان</p></div>
<div class="m2"><p>کجاکار تو بگشایند اینان</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>همه درمکر و زرق ونام و ناموس</p></div>
<div class="m2"><p>بمانده درنهاد خود بافسوس</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>همه مردار و هم مردار خوارند</p></div>
<div class="m2"><p>یقین میدان که چون مردار خوارند</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>دلم بگرفت شیخ از دید دو نان</p></div>
<div class="m2"><p>از آن میگویمت اینجا ببرهان</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>طمع زینها بیکباره بریدم</p></div>
<div class="m2"><p>که تا اینجا یقین جانان بدیدم</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>طمع زینها بریدم در خدائی</p></div>
<div class="m2"><p>که تادیدم وصال خود نمائی</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>طمع زینها بریدم در حقیقت</p></div>
<div class="m2"><p>سپردم آنگهی راه شریعت</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>طمع ببریدهام از هر دو عالم</p></div>
<div class="m2"><p>که تا میگویم این سر دمادم</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بجز حق این همه باطل شناسم</p></div>
<div class="m2"><p>از اینان کی در این معنی هراسم</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بجز حق این همه خار جهانند</p></div>
<div class="m2"><p>که چون سگ هر نفس هر سو جهانند</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>اوائل این چنین دیدم حقیقت</p></div>
<div class="m2"><p>از اینان ذات بگزیدم حقیقت</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چوذات حق در ایشانست موجود</p></div>
<div class="m2"><p>مرا آن ذات بد از جمله مقصود</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>همه دارند لیکن چون ندارند</p></div>
<div class="m2"><p>حقیقت آمده بیچون ندارند</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>اگر دارند اما این حقیقت</p></div>
<div class="m2"><p>حقیقت شیخ حق است ای رفیقت</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ابا دارند اما این حکایت</p></div>
<div class="m2"><p>حقیقت شیخ دور است از شکایت</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>مرا مقصود ازین گفتار آنست</p></div>
<div class="m2"><p>که شرع اندر میان ذات جانست</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>شریعت گفتمت تا راز دانی</p></div>
<div class="m2"><p>حقیقت ذات ایشان باز دانی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>که چندی در میانه این چنیناند</p></div>
<div class="m2"><p>گمان در پیش کرده بییقیناند</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>بسی دیدم ملامت من از اینان</p></div>
<div class="m2"><p>ولیکن خاطر اسرار بینان</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>درین ره مر مرا داده است تحقیق</p></div>
<div class="m2"><p>همی بینم دراینجا اهل توفیق</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>مرا کار است با ایشان حقیقت</p></div>
<div class="m2"><p>چه کارم شیخ با اهل طبیعت</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>همه در ذات یکی مینماید</p></div>
<div class="m2"><p>ولیکن گفتن ایشان را نشاید</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>مر این اسرار ای شیخ جهان تو</p></div>
<div class="m2"><p>همی گویم که هستی در میان تو</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>نمیدانند هر چندی سر از پای</p></div>
<div class="m2"><p>رموز ما در اینجا گاه بگشای</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>سراپای حقیقت دیدهام من</p></div>
<div class="m2"><p>همه کون و مکان گردیدهام من</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>حقیقت دیدهام هم مغز و هم پوست</p></div>
<div class="m2"><p>اگرچه در حقیقت این همه اوست</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>بنور حق مزین شرع بشناس</p></div>
<div class="m2"><p>ز حق مراصل را با فرع بشناس</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>همه زین کار هانه رخ نمودند</p></div>
<div class="m2"><p>به هر صورت یقین ما را نمودند</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>نظام کار عالم ار بدانست</p></div>
<div class="m2"><p>که نیکان راعیانی در عیانست</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>چنین افتادهٔ از شرع در فرع</p></div>
<div class="m2"><p>که تا تو بازدانی اصل با فرع</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>کمال شرع از آن تحقیق دارد</p></div>
<div class="m2"><p>که ذات مصطفی توفیق دارد</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>ابوجهل لعین باشد چو احمد؟</p></div>
<div class="m2"><p>حقیقت مصطفی نیکست و او بد؟</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>کجا فرعون باشد همچو موسی</p></div>
<div class="m2"><p>که او حق بدفراز طور سینا</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>کجا نمرود ابراهیم باشد</p></div>
<div class="m2"><p>کزین معنی حقیقت بیم باشد</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>تو و شیخ کبیر اینجا یکی اید</p></div>
<div class="m2"><p>حقیقت ذات بیچون بیشکیاید</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>که ره بسبودهاید اندر خدائی</p></div>
<div class="m2"><p>شما را می نهبینم در خدائی</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>چنین افتاد سرّ عشقبازی</p></div>
<div class="m2"><p>مدان اسرار ما شیخا ببازی</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>از اول عزلتی خوش داشتم من</p></div>
<div class="m2"><p>ز عزلت بهرهها برداشتم من</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>ز عزلت یافتم سر کماهی</p></div>
<div class="m2"><p>ز خلوت یافتم دید الهی</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>ز عزلت یافتم اسرار بیچون</p></div>
<div class="m2"><p>مرا بخشیده او اسرار بیچون</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>ز عزلت یافتم اسرارها کل</p></div>
<div class="m2"><p>از آنم در همه دیدارها کل</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>ز عزلت در درون خلوت دل</p></div>
<div class="m2"><p>شدم ای شیخ در دیدار واصل</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>ز عزلت جوی شیخ و یار خودبین</p></div>
<div class="m2"><p>بخلوت جملهٔ اسرار خود بین</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>تو عزلت جوی و در عین الیقین شو</p></div>
<div class="m2"><p>در اینجا در حقیقت پیش بین شو</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>اگر عزلت گزینی همچو عنقا</p></div>
<div class="m2"><p>تو در خلوت شوی ای شیخ یکتا</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>اگر عزلت گزیدی درخودی تو</p></div>
<div class="m2"><p>برون آیی ز نیکی و بدی تو</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>اگر عزلت گزینی در عیانت</p></div>
<div class="m2"><p>نماید دید بیشک دید جانت</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>اگر عزلت گزینی صاحب درد</p></div>
<div class="m2"><p>شوی درخلوت ای شیخ جهان فرد</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>اگر عزلت گزینی همچو عشاق</p></div>
<div class="m2"><p>شوی ای شیخ عالم همچو من طاق</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>اگز عزلت گزینی همچو مردان</p></div>
<div class="m2"><p>حقیقت ذات خود را فرد گردان</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>به از عزلت گزینی از سر درد</p></div>
<div class="m2"><p>نمانی جاودان از جان جان فرد</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>اگر عزلت گزینی در لقایت</p></div>
<div class="m2"><p>نماید رخ حقیقت جانفزایت</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>حقیقت جوی عزلت تا توانی</p></div>
<div class="m2"><p>که چون عزلت کنی این خود بدانی</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>حقیقت جوی عزلت همچو مردان</p></div>
<div class="m2"><p>ازینان خویشتن آزاد گردان</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>حقیقت دردسر میدان تو دنیا</p></div>
<div class="m2"><p>ز دنیا عزلت دیدار مولا</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>ز دنیا حظ روح خویش بردار</p></div>
<div class="m2"><p>عیان فتح و فتوح خویش بردار</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>تمامت انبیاء در عزلت خویش</p></div>
<div class="m2"><p>حقیقت یافته از قربت خویش</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>چو میدیدند کین دنیای ناساز</p></div>
<div class="m2"><p>نخواهد بود با کس نیز دمساز</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>کناره زین جهان کردند ایشان</p></div>
<div class="m2"><p>که سودی نیست زینجای پریشان</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>حقیقت سوددنیا چیست طاعت</p></div>
<div class="m2"><p>به از این نیست این عین سعادت</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>چو دنیا کنده پیر گوژپشتست</p></div>
<div class="m2"><p>بسا پرورده و آنگه بکشتست</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>تو ازدنیا چه خواهی برد آنجا</p></div>
<div class="m2"><p>که بیشک تو نخواهی مرد آنجا</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>جهان و هرچه در روی جهان است</p></div>
<div class="m2"><p>همه از ذات حق عکسی عیان است</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>جهان بیوفا نوری ندارد</p></div>
<div class="m2"><p>دمی بیماتم او سوری ندارد</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>بلا و محنت است این دار دنیا</p></div>
<div class="m2"><p>که شد از عشق برخوردار دنیا</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>جهان بیگانهٔ دان در ره عشق</p></div>
<div class="m2"><p>اگر هستی حقیقت آگه عشق</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>جهان بیگانهٔ چون آشنایست</p></div>
<div class="m2"><p>وفا از وی مجو که بیوفایست</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>جهان بیگانهٔ مردار خواراست</p></div>
<div class="m2"><p>بنزدعاشقان مردار، خوار است</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>جهان بیگانهٔ پر درد و رنج است</p></div>
<div class="m2"><p>بنزد عاشقان خوان سپنج است</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>جهان بیگانه دان ای شیخ اینجا</p></div>
<div class="m2"><p>در آن دیوانهٔ دان شیخ اینجا</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>جهان بگذار شیخ و در نهان شو</p></div>
<div class="m2"><p>چو کردی پشت بر وی جان جانشو</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>جهان بگذار شیخ و راستی کن</p></div>
<div class="m2"><p>ز دید او نظر در کاستی کن</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>جهان بگذار تا جاوید گردی</p></div>
<div class="m2"><p>تو در عین عیان خورشید گردی</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>جهان بگذار همچون عاشقان تو</p></div>
<div class="m2"><p>چه میجوئی به آخر زین جهان تو</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>جهان بگذار تا رویت نماید</p></div>
<div class="m2"><p>مکن گوشت بوی کویت نماید</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>جهان بگذار ای شیخ جهان بین</p></div>
<div class="m2"><p>جهان چبود خداوند جهان بین</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>جهان بگذار و در حق پیش بین گرد</p></div>
<div class="m2"><p>که تا مانی تو در عین الیقن فرد</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>جهان بگذار و در یکی قدم زن</p></div>
<div class="m2"><p>وگرنه در ره مردان قدم زن</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>ره مردان طلب مانند مردان</p></div>
<div class="m2"><p>طلب کن علم و بگذر زینجهان هان</p></div></div>