---
title: >-
    بخش ۱۹ - هم در این معنی بنوع دیگر فرماید
---
# بخش ۱۹ - هم در این معنی بنوع دیگر فرماید

<div class="b" id="bn1"><div class="m1"><p>چنان مستم کنون در روی ساقی</p></div>
<div class="m2"><p>که درمستی نخواهم ماند باقی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان مستم که پای از سر ندانم</p></div>
<div class="m2"><p>بجز ساقی در این رهبر ندانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان مستم که ساقی پیش بینم</p></div>
<div class="m2"><p>ولیکن دید ساقی خویش بینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان مستم درینجان فنا من</p></div>
<div class="m2"><p>که میبینم همه عین بقا من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز مستی در همه کون و مکانم</p></div>
<div class="m2"><p>اناالحق میزند عین العیانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حقیقت شیخ ازین می باز خور تو</p></div>
<div class="m2"><p>گذر کن بعد از این از ماه و خور تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حقیقت شیخ ازین یک جرعه کن نوش</p></div>
<div class="m2"><p>بجز او جملگی گردد فراموش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حقیقت شیخ از این جرعه خبردار</p></div>
<div class="m2"><p>که در مستی به بینی روی دلدار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منم مست و شده ازدست اینجا</p></div>
<div class="m2"><p>از آنم جام بشکسته است اینجا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بده جامی دگر ساقی به از این</p></div>
<div class="m2"><p>نه جای تلخ جای خوب و شیرین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر من جام بشکستم تو جامی</p></div>
<div class="m2"><p>دگر ده تا بیابم زود نامی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر من جام بشکستم دراینجا</p></div>
<div class="m2"><p>تو جامی ده در اینجا گه مصفا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر من جام بشکستم حقیقت</p></div>
<div class="m2"><p>درون جام دیدم دید دیدت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>درون جام میبینم ترا من</p></div>
<div class="m2"><p>کشیدم از تو پر جور و جفا من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>درون جام میبینم رخ تو</p></div>
<div class="m2"><p>همی بینم ز جام فرخ تو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>توی جانا اناالحق گوی ما را</p></div>
<div class="m2"><p>که گردان کردهٔ چونگوی ما را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>توی جانا درون جان نهانی</p></div>
<div class="m2"><p>اناالحق میزنی باقی تو دانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز مستی شیخ ما را دار معذور</p></div>
<div class="m2"><p>که طاقت طاق شد درجان منصور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز مستی شیخ هستی یافتستم</p></div>
<div class="m2"><p>یقین جانان ز مستی یافتستم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز مستی شیخ من عین عیانم</p></div>
<div class="m2"><p>از آن اندر نشان بینشانم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زمستی در صفاتم بیشکی ذات</p></div>
<div class="m2"><p>ز ذاتم مست کرده جمله ذرات</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همه ذرات من از مستی عشق</p></div>
<div class="m2"><p>اناالحق میزنند از هستی عشق</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همه ذرات من از روی جانان</p></div>
<div class="m2"><p>بماندستند مست روی جانان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همه ذرات من اینجا عیانند</p></div>
<div class="m2"><p>ازین مستی حقیقت جان جانند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همه ذرات من درتست اعیان</p></div>
<div class="m2"><p>نخواهد ماند یاد دوست پنهان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از آن جرعه که ساقی داد بشکست</p></div>
<div class="m2"><p>حقیقت نیست شد دیگر شده هست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دمادم جام خواهم خورد اینجا</p></div>
<div class="m2"><p>که ازمستی بمانده فرد اینجا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دمادم جام خواهم من از این خورد</p></div>
<div class="m2"><p>که خواهم بود دائم در جهان فرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دمادم جام خواهم خورد معنی</p></div>
<div class="m2"><p>کزین جامم بکل دیدار مولا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دمادم نوش خواهم کرد این جام</p></div>
<div class="m2"><p>که میبینم در او آغاز و انجام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نظرکن هان و جام آخر به بینم</p></div>
<div class="m2"><p>که به آمد ز جام اولینم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز ساقی مر مرا جام است اینجا</p></div>
<div class="m2"><p>ز ساقی مر مرا کامست در کام</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو کام دل ز ساقی یافتستم</p></div>
<div class="m2"><p>در اینجا خویش باقی یافتستم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بخواهد خواند آخر تا ابد من</p></div>
<div class="m2"><p>حقیقت فارغ از هر نیک و بد من</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نخواهم ماند اینجا گاه باقی</p></div>
<div class="m2"><p>ولکین مینخواهد ریخت ساقی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>درین حیرت که منصور است سرمست</p></div>
<div class="m2"><p>نگاهی میکند اندر سرمست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بدست یار دست خویش بیند</p></div>
<div class="m2"><p>حقیقت جام می در پیش بیند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چنان درپاکی او مست آمد</p></div>
<div class="m2"><p>که دست یارش اندر دست آمد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو من از روی جانان زار و مستم</p></div>
<div class="m2"><p>بت خود در بر جانان شکستم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بت من لاجرم بشکست و جان شد</p></div>
<div class="m2"><p>حقیقت بت پرست اینجا عیان شد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بت ما لاجرم بشکست دلدار</p></div>
<div class="m2"><p>پس آنگه بت پرست آمد پدیدار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چنان مستم که بت بشکسته بینم</p></div>
<div class="m2"><p>حقیقت خویش را پیوسته بینم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>منم شیخا حقیقت بت شکسته</p></div>
<div class="m2"><p>ز ننگ و نام دنیا باز رسته</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>درین معنی منم هشیار معنی</p></div>
<div class="m2"><p>قلندروار اندر دار دنیا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>قلندر در جهان منصور آمد</p></div>
<div class="m2"><p>که از جان و جهان او دور آمد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو رخت افکندهام این لحظه بر در</p></div>
<div class="m2"><p>از آنم در ره معنی قلندر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>قلندر وار اینجا پاکبازم</p></div>
<div class="m2"><p>که در پاکی حقیقت پاکبازم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>میان پاکبازان در خرابات</p></div>
<div class="m2"><p>گذشتم من ز تقلید خرافات</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>میان پاکبازان رند و مستم</p></div>
<div class="m2"><p>کزو گردم حقیقت هر چه هستم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>خرابات فنادان و درو رو</p></div>
<div class="m2"><p>ز من این نکتههای بکر بشنو</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>اگر خواهی شدن سوی خرابات</p></div>
<div class="m2"><p>نمیگنجد در اینجا عین طامات</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>اگر خواهی شدن جان بر کف دست</p></div>
<div class="m2"><p>نهٔ دلدار چون گردی تو سرمست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بجانی جرعهٔ اینجا به جز تو</p></div>
<div class="m2"><p>اگر میشایدت کلی بخور تو</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بصد جان جرعهٔ اینجا فروشند</p></div>
<div class="m2"><p>همه تقلید اینجا چون بپوشند</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>در آن خمخانه کان منصور دیده است</p></div>
<div class="m2"><p>که غمهاراسرار نور دیده است</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>اگر راهت دهند آنجا حقیقت</p></div>
<div class="m2"><p>نگنجد اندر آنجا گه طبیعت</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>در آن خمخانه چون رفتی فنا شو</p></div>
<div class="m2"><p>ز بود خویش آنگه آشنا شو</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو ساقی اندر آن خمخانه بینی</p></div>
<div class="m2"><p>تو عقل و دین و دل دیوانه بینی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بجز از دست ساقی می مخور باز</p></div>
<div class="m2"><p>که گرداند ترا ساقی سرافراز</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ز دست ساقی ار جامی بنوشی</p></div>
<div class="m2"><p>زمانی تن زن آنجا درخموشی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>خموشی کن مرو بیرون ز خود تو</p></div>
<div class="m2"><p>وگر نه میبریزی خون خود تو</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>در آن خمخانه بنگر جمله عشاق</p></div>
<div class="m2"><p>که ایشان گشته ازمستی می طاق</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>در آن خمخانه بنگر سالکان را</p></div>
<div class="m2"><p>فداکردی بکلی جسم و جان را</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>در آن خمخانه بنگر واصل ای یار</p></div>
<div class="m2"><p>یقین منصور آنجا واصل یار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چو منصور است ساقی بسکه باشد</p></div>
<div class="m2"><p>بجز او اندر اینجاگه چه باشد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>میی دارد در آن خمخانهٔ عشق</p></div>
<div class="m2"><p>که بیشک آن خورد دیوانهٔ عشق</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>میی دارد که گر خوردی نمیری</p></div>
<div class="m2"><p>اگر تو خود گدا یا شاه و میری</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>میی دارد که جان بخش حیاتست</p></div>
<div class="m2"><p>در آن می بیشکی دیدار ذاتست</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>میی کان هر که خورد از خود برون شد</p></div>
<div class="m2"><p>اگر عاقل بود عین جنون شد</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>میی کان هر که خورد از دید معنی</p></div>
<div class="m2"><p>برون تا زد ز جان دردید معنی</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>میی کان هر که خورد از عین دیدار</p></div>
<div class="m2"><p>شود از هر دوعالم ناپدیدار</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>میی کان هر که خورد از لاعیان شد</p></div>
<div class="m2"><p>ولی در صورت اینجاگه عیان شد</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>میی کان هر که خورد از گفت و گو رست</p></div>
<div class="m2"><p>بماند تا ابد اینجایگه مست</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>میی کان هر که خورد از خود فناشد</p></div>
<div class="m2"><p>پس آنگه در فنا دید خدا شد</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>میی کان هر که خورد اینجای الحق</p></div>
<div class="m2"><p>زند مانند من هم او اناالحق</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>حقیقت هر که را این آرزویست</p></div>
<div class="m2"><p>درین معنی چه جای گفت و گویست</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>در اینجا گفتگو گر میکنی باز</p></div>
<div class="m2"><p>درون شو تا به بینی ای سرافراز</p></div></div>