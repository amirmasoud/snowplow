---
title: >-
    بخش ۹ - جواب دادن منصور شیخ جنید را از حالات
---
# بخش ۹ - جواب دادن منصور شیخ جنید را از حالات

<div class="b" id="bn1"><div class="m1"><p>بدو منصور گفت ای ذات بیچون</p></div>
<div class="m2"><p>اناالحق میزند از ذات بیچون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اناالحق میزند درخون او باز</p></div>
<div class="m2"><p>وگرنه خون کجا این دم زند باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اناالحق چون نیارد زو تو دریاب</p></div>
<div class="m2"><p>بگویم نکتهٔ دیگر تو دریاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اناالحق خون کجا آورد ای دوست</p></div>
<div class="m2"><p>اناالحق گفتن اندر دم دم اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اناالحق حق تواند زد حقیقت</p></div>
<div class="m2"><p>وگرنه خود بود بیشک حقیقت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اناالحق حق زند اینجای بنگر</p></div>
<div class="m2"><p>اناالحق گفتنش ای شاه بنگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>موافق تا نباشد در رگ و پی</p></div>
<div class="m2"><p>کجا یارد زدن هر دم وی از وی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دمم حق زنده گردانید در خون</p></div>
<div class="m2"><p>نمود اینجای رازش بیچه و چون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز سرّ جان جان چون یافت بوئی</p></div>
<div class="m2"><p>اناالحق زدوی اندر های و هوئی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دم حق هرکجا کاید نمودار</p></div>
<div class="m2"><p>اناالحق خیزدش از سنگ و دیوار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درخت سبز با موسی در آن شب</p></div>
<div class="m2"><p>اناالحق گفت با موسی در آن شب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>درختی دید موسی صاحب راز</p></div>
<div class="m2"><p>اناالله گفت با موسی در آن باز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>درختی واصل این راه باشد</p></div>
<div class="m2"><p>عجب گر خون ما آگاه باشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>درختی وصل جانان یافت آن دم</p></div>
<div class="m2"><p>اناالحق گفت او اینجا در آن دم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عجب باشد اگر در خون چو منصور</p></div>
<div class="m2"><p>شود در عشق او القصه مشهور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نه چون آید حقیقت کردگارت</p></div>
<div class="m2"><p>که خون گشته نهان در زیردارت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اناالحق میزند بیدست مانده</p></div>
<div class="m2"><p>حقیقت خون ز دست خود فشانده</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از آن اینجا اناالحق میزند باز</p></div>
<div class="m2"><p>که اینجا گشت خواهد ناپدیدار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نه دست من که دست خود بریده است</p></div>
<div class="m2"><p>طمع اینجا زنیک و بد بریده است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>طمع ببریده است است ازدست آفاق</p></div>
<div class="m2"><p>از آن افتاده جان اندر جهان طاق</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>طمع ببریده است از دست و از پای</p></div>
<div class="m2"><p>یکی میبینم اینجا مسکن و جای</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>درین مسکن زخلوت صاف بوده</p></div>
<div class="m2"><p>درین معنی بخون رگ را گشوده</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>حیات طیّبه در خون بدیده</p></div>
<div class="m2"><p>که تا دانی تو کان را چون بدیده</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حیات طیّبه آمد پدیدار</p></div>
<div class="m2"><p>از آن خون اناالحق زد ابایار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>حیات طیّبه منصور دارد</p></div>
<div class="m2"><p>که سرتا پای خود او نور دارد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>وجودش جمله جان گشته در اینجا</p></div>
<div class="m2"><p>نه همچون دیگران سرگشته اینجا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>حیاتی یافت جانم اندر این دم</p></div>
<div class="m2"><p>که ریزان گشت از دست و دلم دم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>حیاتی یافت جان اینجا نمانی</p></div>
<div class="m2"><p>نمود اسرار صورت در معانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دو دستم بایدالله است بنگر</p></div>
<div class="m2"><p>دو دستم دست دلخواهست بنگر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دو دستم برد اینجاگه بدستان</p></div>
<div class="m2"><p>درون جان و دل صد گونه دستان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>یقین خواهد نمودن بر سردار</p></div>
<div class="m2"><p>دمادم میکند دلها خبردار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>حقیقت حق بدینجا شیخ اعظم</p></div>
<div class="m2"><p>اناالحق باش اندر عشق هر دم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دگر بنگر قدم تا می چه گوید</p></div>
<div class="m2"><p>چه بیند راز دردستم چه گوید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زبان بیزبان چون گویدم راز</p></div>
<div class="m2"><p>دگر چون بنگری در عین آواز</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تو حال دست چون دیدی چه باشد</p></div>
<div class="m2"><p>از این معنی که پرسیدی چه باشد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تو حال دست را پرسیدی اینست</p></div>
<div class="m2"><p>که با ذرات در عین یقین است</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مرا اینجایگه چه دست و چه سر</p></div>
<div class="m2"><p>همه عین الیقین بوده است بنگر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز سر تا پای منصور است واصل</p></div>
<div class="m2"><p>همه ذرات در عشقند کامل</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز سر تا پای منصور است جانان</p></div>
<div class="m2"><p>اناالحق گوی اینجا در یقین دان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز سر تا پای دلدار است منصور</p></div>
<div class="m2"><p>اناالحق گوی اینجا بر سر طور</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز سر تا پای منصور است دلدار</p></div>
<div class="m2"><p>اناالحق گوی اینجا برسردار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز سر تا پای منصور است بیشک</p></div>
<div class="m2"><p>گرفتار آمده دربند کل یک</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>یکی ذاتست منصور از حقیقت</p></div>
<div class="m2"><p>خداگشته چه جای و چه طبیعت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز سر تا پای منصور است اشیا</p></div>
<div class="m2"><p>نمود دوست دروی جمله پیدا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز سر تا پای منصور است خورشید</p></div>
<div class="m2"><p>همه ذرات دروی کرده امید</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ز سر تا پای منصور است کل ذات</p></div>
<div class="m2"><p>اناالحق گوی در وی جمله ذرات</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چنانم این زمان در سر بیچون</p></div>
<div class="m2"><p>چه ذاتم چه رگ و چه پوست و چه خون</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چنانم این زمان در ذات مانده</p></div>
<div class="m2"><p>کنون در عین هر ذرات مانده</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چنانم ده ریئی و در یکی کم</p></div>
<div class="m2"><p>منم چون قطره در دریای قلزم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چنانم از یدالله آشکار است</p></div>
<div class="m2"><p>مرا بادست این صورت چکار است</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>یدالله است راز ما در این بس</p></div>
<div class="m2"><p>نمیداند به جز من سیر این کس</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ندیدم واصلی تا راز گویم</p></div>
<div class="m2"><p>ورا اسرار کلی بازگویم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تو گرچه واصلی در عشق مانده</p></div>
<div class="m2"><p>کجاباشی تو دست از جان فشانده</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>اگر مردی تودست ازجان فشانی</p></div>
<div class="m2"><p>مر این اسرار اینجا بازدانی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>اگر تو ترک کردی صورت خویش</p></div>
<div class="m2"><p>حجاب بیشکی برخیزد از خویش</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>حقیقت ای جنید پاک دین تو</p></div>
<div class="m2"><p>مرو بیرون ازین پس بی یقین تو</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>من ازعین یقین و اصل شد ستم</p></div>
<div class="m2"><p>چنین اسرارها حاصل شدستم</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>من از عین الیقین اعیان ذاتم</p></div>
<div class="m2"><p>اناالحق گوی اینجا در صفاتم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>حقم اندر حق و اینجا تو بنگر</p></div>
<div class="m2"><p>که میگویم کنون الله اکبر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>صفاتم سر بسر دیدار یار است</p></div>
<div class="m2"><p>چه غم دارد که جانان آشکار است</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>صفاتم در حقیقت حق شد اینجا</p></div>
<div class="m2"><p>نمود جسم و جان مطلق شد اینجا</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>صفاتم حق بود چون راز دیدی</p></div>
<div class="m2"><p>اناالحق تو ز خونم باز دیدی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>صفاتم این زمان حقست بنگر</p></div>
<div class="m2"><p>بجان ودل از این معنی تو برخور</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>صفاتم این زمان حقست مطلق</p></div>
<div class="m2"><p>اناالحق گویم اینجا من اناالحق</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>مزه چون درین میدان فتادست</p></div>
<div class="m2"><p>اناالحق مرو را در جان فتاده است</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>منزه چون درین راز است اینجا</p></div>
<div class="m2"><p>از آن بیشک در آواز است اینجا</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>منزه چون من عین صفات است</p></div>
<div class="m2"><p>از آن اینجایگه دیدار ذاتست</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>صفاتم ذات بیچونست اینجا</p></div>
<div class="m2"><p>ویم درخاک و درخونست اینجا</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بجز او نیست اکنون در درونم</p></div>
<div class="m2"><p>اناالحق زن به بین در خاک و خونم</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ایا اینجا ندیده سر اسرار</p></div>
<div class="m2"><p>اناالحق چند خواهی گفت با یار</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>سخن اینست اکنون سالک پیر</p></div>
<div class="m2"><p>که باید شست دست از جان چه تدبیر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>دو دست از جان بباید شست ناچار</p></div>
<div class="m2"><p>که تا بنمایدت این جای اسرار</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>دو دست از جان بیابد شست ای دل</p></div>
<div class="m2"><p>که تا روزی مگر گردی تو واصل</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>دو دست از جان بدار و آشنا شو</p></div>
<div class="m2"><p>اناالحق گوی و آنگاهی جداشو</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>تو دستان فلک اینجا چه دانی</p></div>
<div class="m2"><p>که پنهان نیست این راز نهانی</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>تو دستان فلک بنگر یقین باز</p></div>
<div class="m2"><p>که میبنمایدت مردم چنین راز</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>از آن ماندی که دست از خود بداری</p></div>
<div class="m2"><p>کجا زیبد تو امر پای داری</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>تودست ازخود کجا داری بتحقیق</p></div>
<div class="m2"><p>که تا یارت دهد در عشق توفیق</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>تو دست از جان بدار و جان جانشو</p></div>
<div class="m2"><p>ز دید خویشتن کلی نهان شو</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چو دست از خویش شستی یارگشتی</p></div>
<div class="m2"><p>حقیقت بیشکی دلدار گشتی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>تو دست از جان بدار ارکاردانی</p></div>
<div class="m2"><p>که بگشاید درت باز از معانی</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>تودست از خود بدار و او شو اینجا</p></div>
<div class="m2"><p>حقیقت کرد اینجا گاه یکتا</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>تو دست از خود بیکباره فرو شوی</p></div>
<div class="m2"><p>هر آن چیزی که او گوید تو میگوی</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>دریغا شیخ دین کاینجا بماندیم</p></div>
<div class="m2"><p>حقیقت ما کنون بیما بماندیم</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>قلم راندیم اندر اصل او</p></div>
<div class="m2"><p>نمود دست خود کردم معطل</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>هر آنکو شد فنا از بود اینجا</p></div>
<div class="m2"><p>بدید اندر فنا معبود اینجا</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>هر آنکو شد فنا اندر دل و جان</p></div>
<div class="m2"><p>نموداری جانان در دل و جان</p></div></div>