---
title: >-
    بخش ۳۳ - در حقیقت سرّ منصورو دریافتن اعیان گوید
---
# بخش ۳۳ - در حقیقت سرّ منصورو دریافتن اعیان گوید

<div class="b" id="bn1"><div class="m1"><p>چنین فرمود سلطان حقیقت</p></div>
<div class="m2"><p>سپهر جان و دل قطب شریعت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمود ذات و سر لامکانی</p></div>
<div class="m2"><p>بگویم کیست تا کلی بدانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نهاده بر سر معنی خود تاج</p></div>
<div class="m2"><p>نهان و آشکارا نیز حلاج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که من در خواب دیدم حق تعالی</p></div>
<div class="m2"><p>مرا بنمود اینجاگاه دنیا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه دنیا من اندر خواب دیدم</p></div>
<div class="m2"><p>همه ذرات درغرقاب دیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدیدم هر دو عالم در درونم</p></div>
<div class="m2"><p>نمودم روی در جان رهنمونم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حقیقت جان جان را باز دیدم</p></div>
<div class="m2"><p>بخواب از وی تمامت راز دیدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه من بودم و من بیخبر ز آن</p></div>
<div class="m2"><p>حقیقت بود من جز جان جانان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من و او هر دو یکی گشت درخواب</p></div>
<div class="m2"><p>مثال قطره اندر عین غرقاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو دیدم راز بنمودم حقیقت</p></div>
<div class="m2"><p>یکی دیدم در این عین طبیعت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکی بُد چونشدم بیدار و آن بود</p></div>
<div class="m2"><p>نهانم در نهان کلی عیان بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عیانی چون بدیدم جمله در خویش</p></div>
<div class="m2"><p>حجابم آن زمان برخاست از پیش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بشد ظلمت چو نور آمد پدیدار</p></div>
<div class="m2"><p>بجان و سر شدم سرش خریدار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو در خوابی کنون درعین صورت</p></div>
<div class="m2"><p>نمیدانی تو این یعنی ضرورت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر بنمایدت چون او به بینی</p></div>
<div class="m2"><p>بیابی صورت از صاحب یقینی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حقیقت مینماید یار اینجا</p></div>
<div class="m2"><p>دمادم میفزاید یار اینجا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو میبینی و در خوابی بمانده</p></div>
<div class="m2"><p>ز بیآبی و در آبی بمانده</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چنان مغرور در دنیا بماندی</p></div>
<div class="m2"><p>که در صورت ابی معنا بماندی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زمانی کاندرین خواب جهانی</p></div>
<div class="m2"><p>چنین در حرص و غرقاب جهانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نگاهی کن به بیداری و بنگر</p></div>
<div class="m2"><p>که تا اینجا که میبینی تو ره بر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حقیقت مرگ هم مانند خوابست</p></div>
<div class="m2"><p>چو برقی عمر تو اندر شتابست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو مردی خواب مرگت میبرد باز</p></div>
<div class="m2"><p>پس آنگاهیت با خویش آورد باز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو با خویش آیی و بینی رخ او</p></div>
<div class="m2"><p>دگر میبشنوی زو پاسخ او</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ترا چون وقت مرگ آید بدانی</p></div>
<div class="m2"><p>نمود سرّخود گر کاردانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یقین خواب طبیعت خود بود خواب</p></div>
<div class="m2"><p>ز من خواب حقیقت خود تو دریاب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>حقیقت مرگ خواب آمد حقیقت</p></div>
<div class="m2"><p>ولی خوش خفته در خواب طبیعت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو مرگ آید شوی از خواب بیدار</p></div>
<div class="m2"><p>برون آیی ز بیهوشی پندار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو این دم شو ز خواب نفس بیدار</p></div>
<div class="m2"><p>که دلدار است با تو بین رخ یار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زهی نادان گرش اینجا نیابی</p></div>
<div class="m2"><p>کجا آنجاش بی آنجاش یابی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در اینجا راز آنجا دان و بنگر</p></div>
<div class="m2"><p>نظر کن دل کتب برخوان و بنگر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تو با اوئی و او با تودر اینجا</p></div>
<div class="m2"><p>ابا تو راز بگشاده در اینجا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>درت بسته است و تو در بسته در خود</p></div>
<div class="m2"><p>از آنی دایماً در بسته در خود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تو تا خودبینی او راکی به بینی</p></div>
<div class="m2"><p>همه اویست وگر تو او به بینی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تو او میبین درون خویش زنهار</p></div>
<div class="m2"><p>دمی بی او تو ضایع هیچ مگذار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تو با اویی چنین غافل بمانده</p></div>
<div class="m2"><p>چو ملحد گفته لاواصل بمانده</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چه دانی راه نابرده بمنزل</p></div>
<div class="m2"><p>کجا باشی بآخر عین واصل</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در این منزل که دنیا نام دارد</p></div>
<div class="m2"><p>که را دیدی که اینجا کام دارد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نه بیند هیچکس کامی ز اسرار</p></div>
<div class="m2"><p>که نی آخر فرو ماند گرفتار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همه دنیا چو شور و فتنه آمد</p></div>
<div class="m2"><p>حقیقت راهروزو کام بستد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گذر کرد و به آنجا رفت او باز</p></div>
<div class="m2"><p>برفت از فتنه دید او عز و اعزاز</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خوشا آنکس که پیش از مرگ مرده است</p></div>
<div class="m2"><p>حقیقت گوی او از پیش برده است</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بمیر از خویش تا زنده بمانی</p></div>
<div class="m2"><p>که چون مردی حقیقت جان جانی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>زوالت نیست اما در زوالی</p></div>
<div class="m2"><p>وصالت نیست اما در وصالی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ترا خورشید جان تا بنده اینجاست</p></div>
<div class="m2"><p>هزاران مهر و مه تابنده اینجاست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تو میدانی که اینجا کیستی تو</p></div>
<div class="m2"><p>در این پرگار بهر چیستی تو</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ترا در آفرینش هست بینش</p></div>
<div class="m2"><p>تو هستی برتر از این آفرینش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>کمالت برتر از حد کمال است</p></div>
<div class="m2"><p>ترا اینجا بجانانت وصال است</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بخواهد آفتابت هم فرو شد</p></div>
<div class="m2"><p>نهان بیشک حقیقت نور او شد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>دگر از برج غیبت سر برآرد</p></div>
<div class="m2"><p>زوال آخر حقیقت می ندارد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>زوالی نیست مر خورشید بنگر</p></div>
<div class="m2"><p>که چون رفت او دگر باز آید از در</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ترا خورشید جان چون رفت اینجا</p></div>
<div class="m2"><p>بشیب مغرب اندر عین الّا</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مقام تو بالّا میشود باز</p></div>
<div class="m2"><p>برآید صبح کل الا شود باز</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>شود اندر وصال حال بیچون</p></div>
<div class="m2"><p>یکی کرده همی از شست بیرون</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>حقیقت آفتاب این جهانی</p></div>
<div class="m2"><p>تو در اینجا کجا خود را بدانی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>توئی خورشید اندر عالم جان</p></div>
<div class="m2"><p>شدستی در حقیقت جان جانان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>توئی خورشید اما گردعالم</p></div>
<div class="m2"><p>همی گردی برای کل دمادم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>همه ذرات عالم از تو نورند</p></div>
<div class="m2"><p>سراسر جمله در ذوق حضورند</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>دل عطار با تو آشنایست</p></div>
<div class="m2"><p>ز نور تو پر از نور و ضیایست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ضیا ونور تو کون و مکانست</p></div>
<div class="m2"><p>کسی نور تو کلی میندانست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>کجا اعمی بیابد نورت اینجا</p></div>
<div class="m2"><p>که پیدائی گهی در عشق دردا</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>مزین از توذرات دوعالم</p></div>
<div class="m2"><p>زتو اینجایگه پر نور و خرم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>هر آن وصفی که خواهم کرد از جان</p></div>
<div class="m2"><p>حقیقت برتر از آنست میدان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>مرا انباز عشقم رهنمونست</p></div>
<div class="m2"><p>دلم اینجای بیصبر و سکونست</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ندارم طاقت اسرار گفتن</p></div>
<div class="m2"><p>نه سری نیز از کس میشنفتن</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ندارم طاقت درد فراقش</p></div>
<div class="m2"><p>همی سوزم دمادم ز اشتیاقش</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ندارم طاقتی در پایداری</p></div>
<div class="m2"><p>دمی در صبر یک دم بیقراری</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>مرا اینجا همان پیداست اسرار</p></div>
<div class="m2"><p>که آن حلاج را آمد پدیدار</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بجز حلاج چیزی می ندانم</p></div>
<div class="m2"><p>که باوی گفتم و ازوی بخوانم</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>مرا چیزی به جز او ای دل اینجا</p></div>
<div class="m2"><p>کزو گشتی بکل تو واصل اینجا</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ز منصورم کنون واصل بمانده</p></div>
<div class="m2"><p>چو او دست از دو عالم برفشانده</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>همان آتش که در حلاج افتاد</p></div>
<div class="m2"><p>مرا در جان ودل آنست فریاد</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>زنم هر لحظه دم ازعشق منصور</p></div>
<div class="m2"><p>اگرچه مینماید در دلم شور</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>چه سریّ بود این در آخر کار</p></div>
<div class="m2"><p>که آمد در دل و در جان عطار</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>چه سری بود ای جان باز گویم</p></div>
<div class="m2"><p>ز هر نوعی که خواهم راز گویم</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چو میدانم که میدانم که اینست</p></div>
<div class="m2"><p>دگر تقلید دین عین الیقین است</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>یقین اینست و دیگر نیست تقلید</p></div>
<div class="m2"><p>مرا این راز میآید ز توحید</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ز اول تا بآخر ختم این راز</p></div>
<div class="m2"><p>که تا آخر بدیدم راز سرباز</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>مرا تا جان بود در دیر فانی</p></div>
<div class="m2"><p>همه گویم ازو سر معانی</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>مرا تا جان بود زو راز گویم</p></div>
<div class="m2"><p>ازو هر قصه هر دم بازگویم</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>مرا تا جان بود جز او نه بینم</p></div>
<div class="m2"><p>کزو پیوسته در عین الیقینم</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>همه منصور میبیند درونم</p></div>
<div class="m2"><p>همه خواهد بد آخر رهنمونم</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>همه منصور مییابم در آفاق</p></div>
<div class="m2"><p>که منصور است اندر جزو و کل طاق</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>بجز او کیست تا من بنگرم کس</p></div>
<div class="m2"><p>همه او دانم و میبنگرم کس</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>حقیقت اوست این دم سر گفتار</p></div>
<div class="m2"><p>که میگوید درون جان عطار</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>ز دست عقل هر دم درشکیبم</p></div>
<div class="m2"><p>که اینجا میدهد هر دم شکیبم</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>ز دست عقل من درماندهام من</p></div>
<div class="m2"><p>مثال حلقه بردر ماندهام من</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>ولیکن عقل اینجا هم بکار است</p></div>
<div class="m2"><p>که او را سر معنی بیشمار است</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>ز نور عشق در نور و ضیایم</p></div>
<div class="m2"><p>که میبخشد همه نور و صفایم</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>مرا تا عشق گوید دمبدم راز</p></div>
<div class="m2"><p>نخواهم ماند اندر عقل ممتاز</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>که باشد عقل پیری بر فضولی</p></div>
<div class="m2"><p>ولی در عشق کی باشد اصولی</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>حقیقت عشق به از عقل میدان</p></div>
<div class="m2"><p>ازو چیزی که میبینی تو میخوان</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>مرا تا عقل اول بود در کار</p></div>
<div class="m2"><p>حکایتها بسی گفتم ز اسرار</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>ز عقلم بود اول گفت تقلید</p></div>
<div class="m2"><p>ولیکن عشق دارم سر توحید</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>بنور عشق جانان یافتم باز</p></div>
<div class="m2"><p>ز جان در سوی او بشتافتم باز</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>چه راز از عشق جویم تا بیابم</p></div>
<div class="m2"><p>که از عشق است چندین فتح بابم</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>کمال عشق اگر در جان نماید</p></div>
<div class="m2"><p>بیک ره جسم با جان در رباید</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>کمال عشق هر کس را نشاید</p></div>
<div class="m2"><p>شگرفی چابک و پاکیزه باید</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>حقیقت عشق مشتق دان تو از ذات</p></div>
<div class="m2"><p>که میگویم دمادم سر آیات</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>مرا چون عشق درجانست و در دل</p></div>
<div class="m2"><p>نخواهم ماند من یک لحظه غافل</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>دمادم سرّدیگر مینماید</p></div>
<div class="m2"><p>مرا ازجان و ازدل میرباید</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>سخنهای مرا میدان و میخوان</p></div>
<div class="m2"><p>که گفتارم به کل عشق است و جانان</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>گذشتم من ز عقل آنگه ز تقلید</p></div>
<div class="m2"><p>چودیدم عشق رازم گشت توحید</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>دل و جان واقفند اینجا زتقلید</p></div>
<div class="m2"><p>ولیکن بیشمار آید بتوحید</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>یقین شد حاصلم کل بیگمانم</p></div>
<div class="m2"><p>که از سرّ یقین شد کل عیانم</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>یقین در پیش دار ای مرد سالک</p></div>
<div class="m2"><p>که در عین یقین گیری ممالک</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>یقین گر باشدت اینجانمودار</p></div>
<div class="m2"><p>مرا جان بیگمان آید پدیدار</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>یقین چون جان پیامی در همه تو</p></div>
<div class="m2"><p>در اندازی بعالم دیدهٔ تو</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>یقین وصل است و باقی بیگمانت</p></div>
<div class="m2"><p>مراین معنی که اینجا کس ندانست</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>بجز منصور کاینجابی گمان شد</p></div>
<div class="m2"><p>گمان برداشت تاکل جان جاشد</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>گمان برداشت تا عین الیقین دید</p></div>
<div class="m2"><p>در اینجا ذات کل آن پیش بین دید</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>گمان برداشت اینجا کل مطلق</p></div>
<div class="m2"><p>جمال دوست دید وزد اناالحق</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>جمال دوست در خود جاودان یافت</p></div>
<div class="m2"><p>نمودار حقیقت جسم و جان یافت</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>وصالش گشت اینجاگاه حاصل</p></div>
<div class="m2"><p>که تا شد در جمال عشق واصل</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>چو واصل شد فغان از جان پردرد</p></div>
<div class="m2"><p>که او بُد درمیانه صاحب درد</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>چودرد عشق اینجا دید اول</p></div>
<div class="m2"><p>از آن شد عقل و جان اینجا مبدل</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>همان صورت که اول داشت اینجا</p></div>
<div class="m2"><p>بکلی جسم را بگماشت اینجا</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>رها کرد آن زمان هم جسم و هم جان</p></div>
<div class="m2"><p>یقین پیوسته شد تا دید جانان</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>چنان از خود برون آمد که خود دید</p></div>
<div class="m2"><p>خودی خود زخود الانکودید</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>ز خود بیرون شد و در اندرون یار</p></div>
<div class="m2"><p>اناالحق زد شد آنگه سوی دیدار</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>چو در چشمش کمی شد آفرینش</p></div>
<div class="m2"><p>یکی بد در یکی عین الیقینش</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>از آنسرداشت وز آن سر باز گفت او</p></div>
<div class="m2"><p>ز خود بگذشت و کلی راز گفت او</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>چو بخشایش کسی را داد بیچون</p></div>
<div class="m2"><p>بگوید راز بیچون بیچه و چون</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>اگر محرم شوی مانند منصور</p></div>
<div class="m2"><p>سراپایت شود نور علی نور</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>اگر محرم شوی در جسم و جانت</p></div>
<div class="m2"><p>گشاید سر بسر راز نهانت</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>اگر محرم شوی در دار دنیا</p></div>
<div class="m2"><p>درون دل بیابی سر مولا</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>اگرمحرم شوی مانند مردان</p></div>
<div class="m2"><p>یکی بینی درون خود جانان</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>اگر محرم شوی مانند عطار</p></div>
<div class="m2"><p>نمائی سر کل آنگه بگفتار</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>اگر محرم شوی این راز یابی</p></div>
<div class="m2"><p>در اینجا راز ما را باز یابی</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>چو مردان ره درون راز جستند</p></div>
<div class="m2"><p>در آن گم کرده کی خود باز جستند</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>چواول راه گم شد اندرین راه</p></div>
<div class="m2"><p>در آخر راه بردند سوی درگاه</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>در معنی گشاده است ار بدانی</p></div>
<div class="m2"><p>بود در آخرت صاحبقرانی</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>چو راه اینجایگه بردند سویش</p></div>
<div class="m2"><p>بمستی دم زدند در گفت و گویش</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>بگفتن راست ناید راست این راز</p></div>
<div class="m2"><p>اگر از عاشقانی جان و سرباز</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>دگرگوئی ابا اهل دلان گوی</p></div>
<div class="m2"><p>نه با کون خوان وابلهان گوی</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>کسی راگوی کوره برده باشد</p></div>
<div class="m2"><p>بسوی دوست ره بسپرده باشد</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>ابا او گوی راز ار میتوانی</p></div>
<div class="m2"><p>که او گوید ترا درد نهانی</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>مگو این درد جز با صاحب درد</p></div>
<div class="m2"><p>که او باشد چو تو در عشق کل فرد</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>مرا این درد دل گفتن از آنست</p></div>
<div class="m2"><p>که درمان من از صاحبدلانست</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>مرا با عشق راز است و نیاز است</p></div>
<div class="m2"><p>که عشق از هر دو عالم بی نیاز است</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>مرا با عشق خواهد بودن این راز</p></div>
<div class="m2"><p>که هم در عشق خواهم گشت جان باز</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>یقین صاحبدلان دانند در دم</p></div>
<div class="m2"><p>که چون ایشان من اندر عشق فردم</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>منم امروز اندر درد جانان</p></div>
<div class="m2"><p>بمانده در جهان من فرد جانان</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>از این دردم دوا آمد پدیدار</p></div>
<div class="m2"><p>چنین در دردماندستم گرفتار</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>حقیقت دردم اینجا بی دوایست</p></div>
<div class="m2"><p>که نور عشقم اینجا رهنمایست</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>تمامت اهل دل با من درونند</p></div>
<div class="m2"><p>مرا هر لحظه اینجا رهنمونند</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>ندیدم هیچ جز ایشان در این دل</p></div>
<div class="m2"><p>کز ایشانست هر مقصود حاصل</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>تمامت انبیا و اولیایم</p></div>
<div class="m2"><p>همی بینم درون پر صفایم</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>همه با من منم در ذات ایشان</p></div>
<div class="m2"><p>همی گویم به کل آیات ایشان</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>منم آدم منم نوح ستوده</p></div>
<div class="m2"><p>منم دریار کل جولان نموده</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>منم موسی صفت کل رفته در طور</p></div>
<div class="m2"><p>دم ارنی زده پس غرقه در نور</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>منم ماننداسمعیل جانباز</p></div>
<div class="m2"><p>که تا دید ستم اینجا جان جانباز</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>منم اسحق اینجا سر ببازم</p></div>
<div class="m2"><p>چو شمعی دیگر اینجا سرفرازم</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>منم یعقوب دیده یوسف خویش</p></div>
<div class="m2"><p>مرا یوسف کنون بیش است در پیش</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>منم جرجیس اینجا پاره پاره</p></div>
<div class="m2"><p>حقیقت جزو و کل در من نظاره</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>منم بر تخت معنی همچو داود</p></div>
<div class="m2"><p>سلیمانم رسیده سوی مقصود</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>منم اینجا زکریا پاره گشته</p></div>
<div class="m2"><p>بساط جزو و کلم در نوشته</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>منم یحیی و سوزان در فراقش</p></div>
<div class="m2"><p>همی نالم ز سوز اشتیاقش</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>منم عیسی که اندر پای دارم</p></div>
<div class="m2"><p>حقیقت عشق جانان پایدارم</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>منم احمد زده دم از رآنی</p></div>
<div class="m2"><p>کزو دارم همه سر معانی</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>منم حیدر که دیدارم یقینست</p></div>
<div class="m2"><p>دل و جانم برازش پیش بین است</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>چو در من جمله دیدارند کرده</p></div>
<div class="m2"><p>ازینم صاحب اسرار کرده</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>همه در من پدیدارند اینجا</p></div>
<div class="m2"><p>درون من بگفتارند اینجا</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>چو من در این یقین باشم سرافراز</p></div>
<div class="m2"><p>از آن خواهم شدن در عشق سرباز</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>محمد جان جان تست بنگر</p></div>
<div class="m2"><p>ز نور جان تو اینجاگاه برخور</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>علی در دل به بین ولُو کَشَفْ یاب</p></div>
<div class="m2"><p>اگر مرد رهی مگذر ازین باب</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>علی نفس محمد دان حقیقت</p></div>
<div class="m2"><p>علی بیرونست از دار طبیعت</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>علی بنمایدت راز نهانی</p></div>
<div class="m2"><p>گشاید بر تو درهای معانی</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>دو دست خود زد امانش تو مگذار</p></div>
<div class="m2"><p>که تا بنمایدت اینجایگه باز</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>از آن خوانندش اینجاگاه حیدر</p></div>
<div class="m2"><p>که او بگشاد در اینجای صد در</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>در معنی علی بگشاد اینجا</p></div>
<div class="m2"><p>مرا این گنج کل او داد اینجا</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>شبی دیدم جمال جانفزایش</p></div>
<div class="m2"><p>شده افتاده اندر خاکپایش</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>ازو پرسیدم احوالم سراسر</p></div>
<div class="m2"><p>مرا برگفت اندر خواب حیدر</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>بگفتم رازها در خواب آن شاه</p></div>
<div class="m2"><p>مرا از کشتن او کردست آگاه</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>نمودم آنچه پنهان بود بر من</p></div>
<div class="m2"><p>که تا اسرارهایم گشت روشن</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>مراگفتا که ای عطار مانده</p></div>
<div class="m2"><p>ز سر عشق برخوردار مانده</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>بسی گفتی ز ما اینجا حقیقت</p></div>
<div class="m2"><p>ببردی نزد ما راه شریعت</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>بسی اینجا ریاضت یافتستی</p></div>
<div class="m2"><p>که تا عین سعادت یافتستی</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>بسی کردی تو تحصیل معانی</p></div>
<div class="m2"><p>که تا دادیمت این صاحبقرانی</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>کنون از عشق برخوردار میباش</p></div>
<div class="m2"><p>که کردی سر ما اینجایگه فاش</p></div></div>
<div class="b" id="bn180"><div class="m1"><p>بگفتی آنچه ما اینجا بگفتیم</p></div>
<div class="m2"><p>ز تو دیدیم اسرار و شنفتیم</p></div></div>
<div class="b" id="bn181"><div class="m1"><p>حقیقت آنچه اینجا گه تو گفتی</p></div>
<div class="m2"><p>در اسرار ما اینجا تو سفتی</p></div></div>
<div class="b" id="bn182"><div class="m1"><p>حقیقت بر تو این در برگشادیم</p></div>
<div class="m2"><p>ترا گنج یقین در دل نهادیم</p></div></div>
<div class="b" id="bn183"><div class="m1"><p>ترا این لحظه چون دادیم این گنج</p></div>
<div class="m2"><p>ز ما اینجا بکش از جان جان رنج</p></div></div>
<div class="b" id="bn184"><div class="m1"><p>بکش رنج این زمان چون گنج داری</p></div>
<div class="m2"><p>ز ما در عشق هان کن پایداری</p></div></div>
<div class="b" id="bn185"><div class="m1"><p>ترا خواهند کشتن آخر کار</p></div>
<div class="m2"><p>که کردی فاش اینجا گاه اسرار</p></div></div>
<div class="b" id="bn186"><div class="m1"><p>کسی کوراز ماگوید حقیقت</p></div>
<div class="m2"><p>نه بگذاریم اور ا در طبیعت</p></div></div>
<div class="b" id="bn187"><div class="m1"><p>حقیقت گفت منصور آن خود دید</p></div>
<div class="m2"><p>در اینجا گه جفای نیک و بد دید</p></div></div>
<div class="b" id="bn188"><div class="m1"><p>تو آن گفتی که آن منصور گفتست</p></div>
<div class="m2"><p>که دیگر چون تو این مشهور گفته است</p></div></div>
<div class="b" id="bn189"><div class="m1"><p>تو گفتی سرّ ما اکنون ببر سر</p></div>
<div class="m2"><p>که تا آیی و بینی بیشک آن سر</p></div></div>
<div class="b" id="bn190"><div class="m1"><p>هر آنکو کرد گستاخی درین راز</p></div>
<div class="m2"><p>نه بگذاریم او را در جهان باز</p></div></div>
<div class="b" id="bn191"><div class="m1"><p>کنونت وقت کشتن آمد ای دوست</p></div>
<div class="m2"><p>که مغزی و برون آریمت از پوست</p></div></div>
<div class="b" id="bn192"><div class="m1"><p>همه خواهیم کشتن همچو تو باز</p></div>
<div class="m2"><p>که تا اینجا نگوید این سخن باز</p></div></div>
<div class="b" id="bn193"><div class="m1"><p>کنون این گفته عطار بینوش</p></div>
<div class="m2"><p>بشو یک ذره از اسرار خاموش</p></div></div>
<div class="b" id="bn194"><div class="m1"><p>بگوی و جان خود را باز اینجا</p></div>
<div class="m2"><p>که بگشادستمت در باز اینجا</p></div></div>
<div class="b" id="bn195"><div class="m1"><p>کنون وقتست ای دل تا بدانی</p></div>
<div class="m2"><p>که حیدر گفت این راز نهانی</p></div></div>
<div class="b" id="bn196"><div class="m1"><p>مشو خاموش و خوش بنویس و برخوان</p></div>
<div class="m2"><p>دمادم لقمهٔ میخور ازین خوان</p></div></div>
<div class="b" id="bn197"><div class="m1"><p>تو برخوان ای محمد با علی راز</p></div>
<div class="m2"><p>چو بشنودی بگفتی عاقبت باز</p></div></div>
<div class="b" id="bn198"><div class="m1"><p>بخور این لقمهٔ اسرار معنی</p></div>
<div class="m2"><p>دمادم کن ز جان تکرار معنی</p></div></div>
<div class="b" id="bn199"><div class="m1"><p>حقیقت آنکه با ایشانست در کار</p></div>
<div class="m2"><p>چو من آید حقیقت صاحب اسرار</p></div></div>
<div class="b" id="bn200"><div class="m1"><p>محمد با علی تو باز بینی</p></div>
<div class="m2"><p>چو بینی سر بریدی راز بینی</p></div></div>
<div class="b" id="bn201"><div class="m1"><p>محمد اول اندر خواب دیدم</p></div>
<div class="m2"><p>ازو بسیار فتح الباب دیدم</p></div></div>
<div class="b" id="bn202"><div class="m1"><p>شترنامه ازو گفتم حقیقت</p></div>
<div class="m2"><p>سپردم مرد را راز شریعت</p></div></div>
<div class="b" id="bn203"><div class="m1"><p>حقیقت صاحب دعوت مرا اوست</p></div>
<div class="m2"><p>که بیرون آوریدم مغز از پوست</p></div></div>
<div class="b" id="bn204"><div class="m1"><p>بجز او صاحب دعوت که بینم</p></div>
<div class="m2"><p>که او اینجاست کل عین الیقینم</p></div></div>
<div class="b" id="bn205"><div class="m1"><p>که او بنمود راهم تا بر شاه</p></div>
<div class="m2"><p>از آن هستی ز سر شاه آگاه</p></div></div>
<div class="b" id="bn206"><div class="m1"><p>کسی کو احمدش کل رهنماید</p></div>
<div class="m2"><p>درش اینجا بکلی برگشاید</p></div></div>
<div class="b" id="bn207"><div class="m1"><p>بجز احمد هر آنکو جست حیدر</p></div>
<div class="m2"><p>کجا بگشایدش اینجایگه در</p></div></div>
<div class="b" id="bn208"><div class="m1"><p>در علم محمد حیدر آمد</p></div>
<div class="m2"><p>که جمله اولیا را سرور آمد</p></div></div>
<div class="b" id="bn209"><div class="m1"><p>سرو سالا جمله اولیا اوست</p></div>
<div class="m2"><p>مشایخ را تمامت پیشوا اوست</p></div></div>
<div class="b" id="bn210"><div class="m1"><p>هر آن سالک که راه مرتضی یافت</p></div>
<div class="m2"><p>سوی احمد شد و آنگه خدا یافت</p></div></div>
<div class="b" id="bn211"><div class="m1"><p>هر آن سالک که اینجا رهنماید</p></div>
<div class="m2"><p>در آخر در برویش برگشاید</p></div></div>
<div class="b" id="bn212"><div class="m1"><p>بجز حیدر مبین بشنو تو این راز</p></div>
<div class="m2"><p>تو حیدر مصطفی دان ای سرافراز</p></div></div>
<div class="b" id="bn213"><div class="m1"><p>محمد با علی هر دو یکی است</p></div>
<div class="m2"><p>ندانم تا ترا اینجا شکی است</p></div></div>
<div class="b" id="bn214"><div class="m1"><p>محمد با علی سالار دیناند</p></div>
<div class="m2"><p>که ایشان درحقیقت پیش بیناند</p></div></div>
<div class="b" id="bn215"><div class="m1"><p>محمد با علی بشناس ای دل</p></div>
<div class="m2"><p>که تا باشی درون کون واصل</p></div></div>
<div class="b" id="bn216"><div class="m1"><p>محمد با علی ذات خدایند</p></div>
<div class="m2"><p>که دمدم راز در جان مینمایند</p></div></div>
<div class="b" id="bn217"><div class="m1"><p>محمد با علی فتحو فتوحست</p></div>
<div class="m2"><p>محمد آدم و حیدر چو نوح است</p></div></div>
<div class="b" id="bn218"><div class="m1"><p>حقیقت احمد و حیدر ز ذاتند</p></div>
<div class="m2"><p>که بیشک رهنمای این صفاتند</p></div></div>
<div class="b" id="bn219"><div class="m1"><p>چو از احمد بحیدر در رسیدی</p></div>
<div class="m2"><p>حقیقت هر دو یکی ذات دیدی</p></div></div>
<div class="b" id="bn220"><div class="m1"><p>یکی دانند ایشان از نمودار</p></div>
<div class="m2"><p>ز سر حق حقیقت صاحب اسرار</p></div></div>
<div class="b" id="bn221"><div class="m1"><p>یکی ذاتند ایشان همچو خورشید</p></div>
<div class="m2"><p>به ایشانست مر ذرات امید</p></div></div>
<div class="b" id="bn222"><div class="m1"><p>محمد رحمة للعالمین است</p></div>
<div class="m2"><p>علی بیشک صفات اندر یقین است</p></div></div>
<div class="b" id="bn223"><div class="m1"><p>ایاسالک اگر تو مرد راهی</p></div>
<div class="m2"><p>حقیقت واصلی در عشق خواهی</p></div></div>
<div class="b" id="bn224"><div class="m1"><p>ره احمد گزین و سرّ حیدر</p></div>
<div class="m2"><p>که بگشاید ز وصلت ناگهی در</p></div></div>
<div class="b" id="bn225"><div class="m1"><p>مبین چیزی مجو جز ذات ایشان</p></div>
<div class="m2"><p>نظر میکن تو در آیات ایشان</p></div></div>
<div class="b" id="bn226"><div class="m1"><p>ره ایشان گزین و گرد واصل</p></div>
<div class="m2"><p>کز ایشانست خود مقصود حاصل</p></div></div>
<div class="b" id="bn227"><div class="m1"><p>از ایشان هر که خود را اصل کل یافت</p></div>
<div class="m2"><p>چو منصور از حقیقت وصل کل یافت</p></div></div>