---
title: >-
    بخش ۱۸ - در بیخودی و مستی و کشف ذات فرماید
---
# بخش ۱۸ - در بیخودی و مستی و کشف ذات فرماید

<div class="b" id="bn1"><div class="m1"><p>چو ساقی ازل جامی مرا داد</p></div>
<div class="m2"><p>درم از بود خود اینجام بگشاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو ساقی ازل عین عیان است</p></div>
<div class="m2"><p>نشانش در نشان بینشانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو ساقی دمبدم در جان نمودار</p></div>
<div class="m2"><p>کند کردم بسر عشق دیدار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا ساقی درون جانست بنگر</p></div>
<div class="m2"><p>دمادم میدهد نقلم ز ساغر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آن ساغر که دل طاقت ندارد</p></div>
<div class="m2"><p>بجز منصور این طاقت نیارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه جامی آن کزین نه کاسهٔ چرخ</p></div>
<div class="m2"><p>در اینجاگاه آورده است در چرخ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فلک بوئی از آن مییافت اینجا</p></div>
<div class="m2"><p>بسر پیوسته گردیده است اینجا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از آن میگردمی شیخا بنوشی</p></div>
<div class="m2"><p>تو این نه خرقه ازرق بپوشی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بساقی بخش اندر آخر کار</p></div>
<div class="m2"><p>چو گردی از رخ ساقی خبردار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میی عشق اندر اینجانوش کن شیخ</p></div>
<div class="m2"><p>ز عشقش جان و دل بیهوش کن شیخ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>میی در کش که منصور آن کشیدست</p></div>
<div class="m2"><p>جمال یار درآنمی بدیداست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>میی درکش که آنجا گه حلال است</p></div>
<div class="m2"><p>از آن منصور در عین وصال است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>میی درکش که تا جانان به بینی</p></div>
<div class="m2"><p>نگار خویشتن آسان به بینی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>میی درکش که جانت زنده گردد</p></div>
<div class="m2"><p>بساط هستی اینجا در نوردد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>میی در کش که در مستی درآئی</p></div>
<div class="m2"><p>در آنمستی زنی دم از خدائی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>میی درکش که بینی عین دیدار</p></div>
<div class="m2"><p>حقیقت جسم آید ناپدیدار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از آنمی خور که من خوردستم ای شیخ</p></div>
<div class="m2"><p>بسوی یار ره بردستم ای شیخ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از آنمی خور که بودت بود گردد</p></div>
<div class="m2"><p>سراپایت بکل معبود گردد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از آن می خور که گردی در زمان ذات</p></div>
<div class="m2"><p>اناالحق میزنی بر جمله ذرات</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در آن می زن اناالحق همچو من تو</p></div>
<div class="m2"><p>عیان خویش را در تن بتن تو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در آن می زن اناالحق بردریار</p></div>
<div class="m2"><p>که کل بینی عنایت لیس فی الدار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در آن می زن اناالحق همچو حلاج</p></div>
<div class="m2"><p>تو بر فرق سپهر آئی بر آن تاج</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از آن می خوردهام شیخی گزین من</p></div>
<div class="m2"><p>حقیقت دوست دیدستم یقین من</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از آن می خوردهام از دست جانان</p></div>
<div class="m2"><p>از آنم این چنین من مست جانان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از آن می خوردهام در عز و در ناز</p></div>
<div class="m2"><p>که دیدستم رخ دلدار خود باز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از آن می خوردهام بیخویشتن من</p></div>
<div class="m2"><p>که خورشید ستم اندر ذات روشن</p></div></div>