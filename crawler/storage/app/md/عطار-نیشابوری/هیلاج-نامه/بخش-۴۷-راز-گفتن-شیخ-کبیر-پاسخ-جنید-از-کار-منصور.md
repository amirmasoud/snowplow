---
title: >-
    بخش ۴۷ - راز گفتن شیخ کبیر پاسخ جنید از کار منصور
---
# بخش ۴۷ - راز گفتن شیخ کبیر پاسخ جنید از کار منصور

<div class="b" id="bn1"><div class="m1"><p>حقیقت از شب اندر کشتی این راز</p></div>
<div class="m2"><p>مرا میگفت نزد آن کسان باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من ای شیخا حقیقت چون خدایم</p></div>
<div class="m2"><p>یقین گشتی جهان را رهنمایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خدایم من که در کون و مکانم</p></div>
<div class="m2"><p>بدان شیخا که برتر ز آسمانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خدایم من نشسته سوی کشتی</p></div>
<div class="m2"><p>در این اسرار شیخا چون گذشتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فلک را با ملک در سوی دریا</p></div>
<div class="m2"><p>من امشب دیدهام در خویش پیدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه از من پدیدارند امشب</p></div>
<div class="m2"><p>ز من بیشک خریدارند امشب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه در من من اندر خود نشسته</p></div>
<div class="m2"><p>مهار عشق را بر جمله بسته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدستم آسمانها و زمین است</p></div>
<div class="m2"><p>مرا خورشید در مهر نگین است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا پیوسته اینجا آشنائی است</p></div>
<div class="m2"><p>ابا ذراتم اینجاگه خدائی است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به بین شیخا درون بحر هستی</p></div>
<div class="m2"><p>مرا اینجا خدایم در پرستی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حقیقت کافرم هم بت پرستم</p></div>
<div class="m2"><p>حقیقت جوهری در بحر هستم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من اینجا بر سر کشتی اسرار</p></div>
<div class="m2"><p>درون بحر هستم درّ شهوار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تودانی شیخ بنگر در بُن بحر</p></div>
<div class="m2"><p>نظر کن درّ مابین اندر این قعر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نظر کن در من و بنگر در اینجا</p></div>
<div class="m2"><p>حققت در ما را جوهر اینجا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همه ذراتاند از من پدیدار</p></div>
<div class="m2"><p>منم در وصل خود خود را خریدار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خدای برّ و بحر جمله گانم</p></div>
<div class="m2"><p>محیط جملهٔ کون و مکانم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چه باشد نزد من دیدار عقبی</p></div>
<div class="m2"><p>منم اسرار وآن بر جمله مولا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>درون جمله اینجا راز بینم</p></div>
<div class="m2"><p>کجا در دار خود را باز بینم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کجا یابم حقیقت دار خود باز</p></div>
<div class="m2"><p>که تا بردار خود گردم سرافراز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عجب ماندم در آن شب کو چنین گفت</p></div>
<div class="m2"><p>در اسرار آن شب این چنین سفت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که بنگر هان چو تو شیخ کبیری</p></div>
<div class="m2"><p>بمانده در کف صورت اسیری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مرا هم صورتست وذات و معنی</p></div>
<div class="m2"><p>منم پیوسته در آیات و معنی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جلالم در جلالم بحر مانده</p></div>
<div class="m2"><p>بسی دربحر خود کشتی برانده</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کنون از بحر خواهم رفت بیرون</p></div>
<div class="m2"><p>نمایم راز اینجا بیچه و چون</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>درین بحر فنا بودم گرفتار</p></div>
<div class="m2"><p>دو روزی با تو در دریای اسرار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کنون شیخا چو وقت رفتن آمد</p></div>
<div class="m2"><p>مرا اسرار با تو گفتن آمد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>برون خواهم شدن دراندرونم</p></div>
<div class="m2"><p>تمات بحریان راه نمونم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو شیخا دل ابا ماراست میدار</p></div>
<div class="m2"><p>که ما را باز بینی بر سر دار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در آن روزی که در بغداد آیی</p></div>
<div class="m2"><p>ابا ما یک دمی دل شاد آیی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مرا آن روز بینی خوار و خسته</p></div>
<div class="m2"><p>طناب ذُل اندر دست بسته</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مرا آن روز یابی شاه عالم</p></div>
<div class="m2"><p>مرا آن لحظه یاب آگاه عالم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چنین خواهد بدن گر باز بینی</p></div>
<div class="m2"><p>مرادر عشق صاحب راز بینی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تو شیخا این زمان از ما نهانی</p></div>
<div class="m2"><p>که مادانیم راز و تو ندانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>درین ره گرچه بیشک واصلانند</p></div>
<div class="m2"><p>نه هر کس اصل کل اینجا بدانند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همانکس وصل یابد همچو من باز</p></div>
<div class="m2"><p>که گردد سوی دریای فنا باز</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بدریای فنا خواهم شدن من</p></div>
<div class="m2"><p>بجوهر کل خدا خواهم بدن من</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بدریای فنا خواهم شدن کل</p></div>
<div class="m2"><p>که تا یابی چو ما در عین آن ذل</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در این دریا کنون رفتیم و گفتیم</p></div>
<div class="m2"><p>حقیقت را ز رخ را در نهفتیم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خدایم چند گویم من خدایم</p></div>
<div class="m2"><p>حقیقت بیزوال و در بقایم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مگو ای ابله دیوانه این راز</p></div>
<div class="m2"><p>وگرنه لایقی بر نفت و براز</p></div></div>