---
title: >-
    بخش ۶ - در اسرار عشق الهی فرماید
---
# بخش ۶ - در اسرار عشق الهی فرماید

<div class="b" id="bn1"><div class="m1"><p>دلا اکنون شدی از خواب بیدار</p></div>
<div class="m2"><p>رهائی یافتی از خواب بیدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز غفلت آمدی بیرون حقیقت</p></div>
<div class="m2"><p>بسی خوردی در اینجا چون حقیقت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بغفلت روزگاری بسپریدی</p></div>
<div class="m2"><p>درین گام بلا کامی ندیدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندیدی هیچ کامی سوی دنیا</p></div>
<div class="m2"><p>بماندی غافل اندر کوی دنیا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگرچه دادهای جان اندرین راه</p></div>
<div class="m2"><p>که از رازی کنون درمانده درچاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بمنزل در رسیدی مانده درچاه</p></div>
<div class="m2"><p>اگرچه دادهٔ جان اندرین راه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بمنزل در رسیدی باز مانده</p></div>
<div class="m2"><p>هنوز از شوق صاحب راز مانده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دم عرفان زدی اینجا بیکبار</p></div>
<div class="m2"><p>ترا جانان نموده عین دیدار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز اصل دوست برخورد ار عشقی</p></div>
<div class="m2"><p>چو منصور این زمان بردار عشقی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ترا از عشق بد چندین ملامت</p></div>
<div class="m2"><p>که خوردی حسرت و رنج و ندامت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قدم چون سوی این گلشن نهادی</p></div>
<div class="m2"><p>ندانستی و در گلخن فتادی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>درین گلخن بماندی مدتی باز</p></div>
<div class="m2"><p>گهی درسوز بودی گاه در ساز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه خواهی کرد گلخن جای تو نیست</p></div>
<div class="m2"><p>قبای خاک بر بالای تو نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>توی از جوهر بالا گزیده</p></div>
<div class="m2"><p>مقام عالم بالا ندیده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سفر کردی ندیدستی ره خود</p></div>
<div class="m2"><p>بکلی مینگشتی آگه خود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سفر کردی سوی منزل رسیدی</p></div>
<div class="m2"><p>دمی وصلت ز نور خود ندیدی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سفر کردی تو با اینسان در اینجا</p></div>
<div class="m2"><p>ندیدی هیچ همراهان در اینجا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سفر کردی ز دریا سوی عنصر</p></div>
<div class="m2"><p>سفر ناکرده گوهر کی شود در</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سفر را گرچنین قدری نبودی</p></div>
<div class="m2"><p>مه نو بر فلک بدری نبودی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نخستین قطرهٔ باران سفر کرد</p></div>
<div class="m2"><p>وز آن پس قعر دریا پرگهر کرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>توی کرده سفر در عین دریا</p></div>
<div class="m2"><p>چرا میمانی اندر قعر دریا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو در دریای عشقی پروریده</p></div>
<div class="m2"><p>کمال خود در این دریا ندیده</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کمال خود ندیدی در جواهر</p></div>
<div class="m2"><p>که اسرارت شود اینجای ظاهر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>طلب کن جوهرخودسوی دریا</p></div>
<div class="m2"><p>چرا ماندستی اندر قعر دریا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>طلب کن جوهر ای دانای اسرار</p></div>
<div class="m2"><p>صدف را بشکن و گوهر برون آر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>توئی دریا و جوهر در نشان نه</p></div>
<div class="m2"><p>ترانامی ولی نام ونشان نه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>توی اندر صدف ساکن بمانده</p></div>
<div class="m2"><p>ز دامن پاک خود ایمن بمانده</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو دست شاه لایقتر نمائی</p></div>
<div class="m2"><p>تو بیشک رازدار پادشائی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چرا تو اندرین دریای خونخوار</p></div>
<div class="m2"><p>بجنگ این صدف ماندی گرفتار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>صدف را بشکن و بنمای هم رخ</p></div>
<div class="m2"><p>تو از دریا شنو پیوسته پاسخ</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نظر کن در خود اکنون چون شکستی</p></div>
<div class="m2"><p>صدف بشکن که کلی خود تو هستی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تو داری نور پاک هفت گلشن</p></div>
<div class="m2"><p>تو در دریا شده پیوسته روشن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کنار بحر روشن از تو باشد</p></div>
<div class="m2"><p>حقیقت هفت گلشن از تو باشد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>الا ای جوهر بی منتها تو</p></div>
<div class="m2"><p>حقیقت بیشکی نور خدا تو</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>الا ای خانهٔ راز الهی</p></div>
<div class="m2"><p>عجایب جوهری جوهر نمائی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نه در کونین و نی در عالمینی</p></div>
<div class="m2"><p>که سرگردان بین اصبعینی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>الا ای جوهر قدسی کجائی</p></div>
<div class="m2"><p>نه در عرشی نه در فرشی کجائی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>درین دریا اگر دریا به بینی</p></div>
<div class="m2"><p>تو خود را محو و ناپیدا به بینی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نه جای تست این دریا و بگذر</p></div>
<div class="m2"><p>درین دریای بیپایان تو بنگر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اگرچه ماندهٔ این دم بغرقاب</p></div>
<div class="m2"><p>کمال خویش هم اینجا تو دریاب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کمال خویش بشناس اندر اینجا</p></div>
<div class="m2"><p>که تا زینجا رسی در عین اینجا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چه میدانی در اینجا تا تو چونی</p></div>
<div class="m2"><p>توئی آن جوهری که ذوفنونی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تو را خواهند بردن تا برشاه</p></div>
<div class="m2"><p>که تا شه گردد از راز تو آگاه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>حقیقت پیش شه خواهی شدن باز</p></div>
<div class="m2"><p>تو باشی در کف سلطان باعزاز</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تو خواهی بود باز و بند سلطان</p></div>
<div class="m2"><p>چوداری حکم بازوبند سلطان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کمالت آنگهی افزاید از یار</p></div>
<div class="m2"><p>که سلطانت بود از جان خریدار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>خریدار تو سلطانست از عشق</p></div>
<div class="m2"><p>در اینجا راز پنهانست ار عشق</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>دریغا چون ندانستی چه گویم</p></div>
<div class="m2"><p>دوای درد بیدرمان چه جویم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>دوای درد خود هستم حقیقت</p></div>
<div class="m2"><p>وزین زندان برون جستم حقیقت</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>برون جستم ازین زندان ظلمات</p></div>
<div class="m2"><p>شدم آزاد اندر حضرت ذات</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>مرا در سوی آن حضرت برد باز</p></div>
<div class="m2"><p>که تا از راز او گردم سرافراز</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بیابم حضرت بیچونش ای دل</p></div>
<div class="m2"><p>که مقصود منست اینجای حاصل</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>مرا اینجاست عز و قدر و قیمت</p></div>
<div class="m2"><p>در اینجا دیدن جانان حقیقت</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>غنیمت دان که در اینجا دو روزی</p></div>
<div class="m2"><p>مثال عاشقان سازی بسوزی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چو با عشاق صاحب درد باشم</p></div>
<div class="m2"><p>نه چون زن همچو مردان مرد باشم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>مرا با درد جانان آشنائیست</p></div>
<div class="m2"><p>دوای دردم از صورت جدائیست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>دریغا درد مادرمان ندارد</p></div>
<div class="m2"><p>حقیقت راه ما پایان ندارد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ندارد درد من درمان دریغا</p></div>
<div class="m2"><p>بمانم بیسر و سامان دریغا</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>سر و سامان ندارم در ره جان</p></div>
<div class="m2"><p>بماندم خوار در بازار جانان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>مرا تا درد باشد جان ندارم</p></div>
<div class="m2"><p>در اینجا جز رخ جانان ندارم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>مرا مقصود جانانست دیدن</p></div>
<div class="m2"><p>پس آنگه درکمال جان رسیدن</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>سر من بهر این راز است سرباز</p></div>
<div class="m2"><p>که یابد عاقبت اسرار ما باز</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ازین معنی نگردم یک زمان من</p></div>
<div class="m2"><p>که تا اینجا رسم در جان جان من</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>نخواهد بود اینجا نطق خاموش</p></div>
<div class="m2"><p>که دل چون دیگ در آتش زند جوش</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>دلم در دیگ سودای معانی</p></div>
<div class="m2"><p>چنان پخته که آن پیر نهانی</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>در آنچه گفت خواهم آنچه او گفت</p></div>
<div class="m2"><p>که حق دید و وزو دید و نکو گفت</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>هر آن چیزی که از حق گفت خواهی</p></div>
<div class="m2"><p>دری باشد که بیشک سفت خواهی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ز حق چندانکه گوئی بیش از آنست</p></div>
<div class="m2"><p>کسی اسرار او کلی ندانست</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ز حق گوی و ز حق بشنو بتحقیق</p></div>
<div class="m2"><p>که از حق میرسد پیوسته توفیق</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ز توفیق وی اینجا جوی طاعت</p></div>
<div class="m2"><p>که در طاعت بیابی استطاعت</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ترا آنجاکمال عشق شاه است</p></div>
<div class="m2"><p>چه غم داری چو شه در بارگاهست</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>مدد از شاه جوی و خرمی کن</p></div>
<div class="m2"><p>مگردان روی از شه همدمی کن</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>چو فرمودت ترا در عین فرمان</p></div>
<div class="m2"><p>ببر فرمان او خود را مرنجان</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>چنان میدان که شاه آفرینش</p></div>
<div class="m2"><p>ترا پیداست اندر آفرینش</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>کمال شاه و فرّ شاه با تست</p></div>
<div class="m2"><p>حقیقت هم دل آگاه با تست</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>همه در دل شناس و دل عیان بین</p></div>
<div class="m2"><p>درون جان جمال بی نشان بین</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ترا در دل جمال ماهروئیست</p></div>
<div class="m2"><p>بلای عشق در هر لحظه سوئی است</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>تو از اوئی و با اوباش اینجا</p></div>
<div class="m2"><p>توی نقش رویت نقاش اینجا</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ترا او نقش بسته آخر کار</p></div>
<div class="m2"><p>کند خود این همه نقشت بیکبار</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>تو چندینی چرا خود دوست داری</p></div>
<div class="m2"><p>به مغزی در حقیقت پوست داری</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>ترا مغز است و در خود ماندی ای دوست</p></div>
<div class="m2"><p>از آن مغزی ندیدستی به جز پوست</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>ترا چون مغز اینجا گه نباشد</p></div>
<div class="m2"><p>چو مردانت دل آگه نباشد</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>دل آگه باید در میانه</p></div>
<div class="m2"><p>که تا یابد کمال جاودانه</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>هر آن غم کاندرین منزل نهادند</p></div>
<div class="m2"><p>حقیقت بار آن بر دل نهادند</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>ز بحر وصل جانها بیقرار است</p></div>
<div class="m2"><p>مکان وصل در دارالقرار است</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>اگر دارالقرار اینجا بدانی</p></div>
<div class="m2"><p>بیابی وصل و اسرار نهانی</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>حقیقت باید اینجا گه قرارت</p></div>
<div class="m2"><p>که پنهان نیست خود دیدار یارت</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>ترا دیدار جانانست اینجا</p></div>
<div class="m2"><p>ولی در پرده پنهانست اینجا</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>وصال او اگر میبایدت دوست</p></div>
<div class="m2"><p>برون میباید آمد پاک از پوست</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>همه گفتارها از بهر این است</p></div>
<div class="m2"><p>که در مردن یقین عین الیقین است</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>اگر مردی برستی از جهان تو</p></div>
<div class="m2"><p>یقین یابی بهشت جاودان تو</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>در آنجا دایماً عین وصالست</p></div>
<div class="m2"><p>که اینجا خانهٔ رنج و وبال است</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>در این محنت سرای عالم کل</p></div>
<div class="m2"><p>کجا آید مراد کل بحاصل</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>خوشستی زندگانی و کشستی</p></div>
<div class="m2"><p>اگر نه مرگ ناخوش در پی استی</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>فراق آخر کار است ما را</p></div>
<div class="m2"><p>وصالش دیدن یار است ما را</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>فراق سخت در راهست آخر</p></div>
<div class="m2"><p>کسی یابد که آگاهست آخر</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>ز بعد آن وصال جاودانست</p></div>
<div class="m2"><p>همه دیدار با آن جان جانست</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>ولی اینجا فراق اندر فراقست</p></div>
<div class="m2"><p>همه دوری ز درد اشتیاق است</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>مراد اینجا تمنا دان حقیقت</p></div>
<div class="m2"><p>در او پنهان و پیدا دان حقیقت</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>دم آخر همه اسرار یابند</p></div>
<div class="m2"><p>کسانی کاندر این دم یار یابند</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>جهانی پر زاندو هست و ماتم</p></div>
<div class="m2"><p>که ما را مینماید غم دمادم</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>بلا و رنج بیحد یافتستم</p></div>
<div class="m2"><p>اگرچه مویها بشکافتستم</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>دل و جان در بلای قرب جانانست</p></div>
<div class="m2"><p>چنین اسرار گفتن کی چنانست</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>دل و جان رازدار پادشاهند</p></div>
<div class="m2"><p>حقیقت دایماً نور الهند</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>چه حاجت بود چندینی ز گفتن</p></div>
<div class="m2"><p>چو میبایست اندر خاک خفتن</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>چه میجوئی ز چندین سر اسرار</p></div>
<div class="m2"><p>که ما گفتیم و هم آمد پدیدار</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>وصال جان جان از جان بگویم</p></div>
<div class="m2"><p>به هر اسرار صد برهان بگویم</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>از اول درد مییابد حقیقت</p></div>
<div class="m2"><p>دوم تقوی در اسرار شریعت</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>سوم جز آنگهی معشوق دیدن</p></div>
<div class="m2"><p>چهارم وصل آنگه سر بریدن</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>نظر در کار این کردم بیکبار</p></div>
<div class="m2"><p>نداند این سخن جز صاحب اسرار</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>جهان و هرچه در هر دو جهانست</p></div>
<div class="m2"><p>نیرزد پرّ کاهی گرچه جانست</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>بجز جانان در این عالم ندانی</p></div>
<div class="m2"><p>به بینی گر تو هم صاحب یقینی</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>بجز جانان مجو ای جان و دل تو</p></div>
<div class="m2"><p>وگرنه عاقبت گردی خجل تو</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>جز او آخر چه باشد هیچ باشد</p></div>
<div class="m2"><p>جهان نقش و طلسم و پیچ باشد</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>حقیقت جملهٔ مردان که بودند</p></div>
<div class="m2"><p>کزو گفتند وهم از وی شنیدند</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>همه گفتار ایشان بود از یار</p></div>
<div class="m2"><p>یکی دیدند اینجاگه نگهدار</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>چنان دیدند در این جایگه باز</p></div>
<div class="m2"><p>که گوئی جان ایشان بد یکی راز</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>طلب کردند تا آخر رسیدند</p></div>
<div class="m2"><p>بوصل اصل جانان باز دیدند</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>رهی دور است این راه خطرناک</p></div>
<div class="m2"><p>چه داند کرد اندر ره کف خاک</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>رهی دور است و بس راهیست مشکل</p></div>
<div class="m2"><p>که یارد رفت آنجا سوی منزل</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>رهی دور است باید رفت ناچار</p></div>
<div class="m2"><p>ترا میگویمت اکنون خبردار</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>خبردار از سوال دوست ای دل</p></div>
<div class="m2"><p>جواب او یقین با اوست ای دل</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>ترا باید شدن واقف ز اسرار</p></div>
<div class="m2"><p>شوی و وارهی از گیر و از دار</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>ترا تا صورت اینجا باز باشد</p></div>
<div class="m2"><p>دلت پر غصه و پر راز باشد</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>چه خواهی یافت از دیدار صورت</p></div>
<div class="m2"><p>که باید زو گذشت آخر ضرورت</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>دو روزی کاندرین صورت اسیری</p></div>
<div class="m2"><p>مجو چیزی به جز عشق و فقیری</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>فقیری کن طلب در قعر جان کوش</p></div>
<div class="m2"><p>لباس نیستی در فقر درپوش</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>فقیر اینجا ملامت شوق داند</p></div>
<div class="m2"><p>هزاران دوزخ آمد ذوق داند</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>چه سرما و چه گرما در فقیری</p></div>
<div class="m2"><p>بر عاشق یکی باشد اسیری</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>ز صورت دان و گرنه فقر یا راست</p></div>
<div class="m2"><p>در او اسرارهای بیشمار است</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>اگر فقر و فنا خواهی در این راز</p></div>
<div class="m2"><p>تکبر از نهاد خود بینداز</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>تکبر پاک کن از جان و از دل</p></div>
<div class="m2"><p>که تا مقصود خود آری بحاصل</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>ترا اینجا برای عجز آورد</p></div>
<div class="m2"><p>که تا باشی در اینجا صاحب درد</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>چو ما را داد ماهم جان فشانیم</p></div>
<div class="m2"><p>بر معشوق دایم بی نشانیم</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>حقیقت حق شناسی چیست تسلیم</p></div>
<div class="m2"><p>شدن فارغ ز هر اندوه و هر بیم</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>اگر مردی حقیقت او شوی تو</p></div>
<div class="m2"><p>ببین خود تا حقیقت خودشوی تو</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>همه در خود خداوند جهان بین</p></div>
<div class="m2"><p>به هرچه اندر به بینی جان جان بین</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>ره او بسپر اینجا همچو مردان</p></div>
<div class="m2"><p>که خدمتکارت آید چرخ گردان</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>ترا چون چرخ گردون بنده باشد</p></div>
<div class="m2"><p>مه و مهرت بجان تابنده باشد</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>فلک گردان تست و می ندانی</p></div>
<div class="m2"><p>همه ملک آن تست و می ندانی</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>قدم زن بهتر از دوران افلاک</p></div>
<div class="m2"><p>که سرگردان تست این کرهٔ خاک</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>ترا سرّی ورای اوست بنگر</p></div>
<div class="m2"><p>اگر رویت نماید دوست بنگر</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>توانی یافت وصل اینجا حقیقت</p></div>
<div class="m2"><p>اگر میبسپری راه شریعت</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>شریعت بسپر آنگه از نمودار</p></div>
<div class="m2"><p>بگویم رازها آنکه خبردار</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>عمل میبایدت کردن در اینجا</p></div>
<div class="m2"><p>پس آنگه گوی خود بردن در اینجا</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>عمل کن تا ستانی مرد کارت</p></div>
<div class="m2"><p>عمل باشد در اینجا یادگارت</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>عمل کردند مردان اندرین راه</p></div>
<div class="m2"><p>بترس از آه موری در بن چاه</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>عمل چون هست در علمت عمل کن</p></div>
<div class="m2"><p>پس از علم و عمل اسرار حل کن</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>اگر علمت بود در اول کار</p></div>
<div class="m2"><p>عمل آید ترا اینجا خریدار</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>ترا دو چیز میباید ز کونین</p></div>
<div class="m2"><p>بدانستن عمل کردن شدن عین</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>طلب باید که تا در برگشاید</p></div>
<div class="m2"><p>پس آگاهی بمطلوبت نماید</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>دریغا کین طلب در دست کس نیست</p></div>
<div class="m2"><p>درین وادی کسی فریادرس نیست</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>نه فریادت رسد جز جان در اینجا</p></div>
<div class="m2"><p>که جان دیده است مر جانان در اینجا</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>کمال عشق اگر آید پدیدار</p></div>
<div class="m2"><p>بچشم تو نه درماند نه دیوار</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>دلی باید ز عشق یار در جوش</p></div>
<div class="m2"><p>بماند تا ابد او مست و مدهوش</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>نشاید عشق را هر ناتوانی</p></div>
<div class="m2"><p>بباید کاملی و کاردانی</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>الا تا در مقام عشق بازی</p></div>
<div class="m2"><p>تو پنداری مگر این عشق بازی</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>که داند بردره در معدن عشق</p></div>
<div class="m2"><p>چنان برگشتهٔ از مامن عشق</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>حقیقت عقل چون طفلی به پیشش</p></div>
<div class="m2"><p>همیشه میخورد از شوق پیشش</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>کجا دارد ابا او پایداری</p></div>
<div class="m2"><p>سزد گر عشق با جان پایداری</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>به پیش کار گه چون رخ نمودند</p></div>
<div class="m2"><p>در آخر این چنین پاسخ شنودند</p></div></div>