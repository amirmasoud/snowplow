---
title: >-
    بخش ۳۱ - رجوع به مطلب
---
# بخش ۳۱ - رجوع به مطلب

<div class="b" id="bn1"><div class="m1"><p>هیبت حق جمله را یکسان کند</p></div>
<div class="m2"><p>جسم‌ها را جملگی چون جان کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیبت حق جمله را زیبا کند</p></div>
<div class="m2"><p>این عددها را همه یکتا کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیبت حق جمله را فاضل کند</p></div>
<div class="m2"><p>بیشکی آندم ترا واصل کند</p></div></div>