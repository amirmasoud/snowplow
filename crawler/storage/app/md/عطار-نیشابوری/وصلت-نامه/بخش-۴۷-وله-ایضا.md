---
title: >-
    بخش ۴۷ - وله ایضاً
---
# بخش ۴۷ - وله ایضاً

<div class="b" id="bn1"><div class="m1"><p>بی یاد تو دل چو سایه در خورشید است</p></div>
<div class="m2"><p>با یاد تو بی‌نهایت امید است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر تخم که در زمین دل کاشته‌ایم</p></div>
<div class="m2"><p>جز یاد تو تخم حسرت جاوید است</p></div></div>