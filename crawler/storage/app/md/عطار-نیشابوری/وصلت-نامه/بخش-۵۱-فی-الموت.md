---
title: >-
    بخش ۵۱ - فی الموت
---
# بخش ۵۱ - فی الموت

<div class="b" id="bn1"><div class="m1"><p>زان پیش که در عین هلاکت فکنند</p></div>
<div class="m2"><p>بفکن همه پاک بو که پاکت فکنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیرا که ز روزگار روزی چندی</p></div>
<div class="m2"><p>بر تو شمرند و بس به خاکت فکنند</p></div></div>