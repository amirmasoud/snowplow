---
title: >-
    بخش ۵۸ - و له ایضاً
---
# بخش ۵۸ - و له ایضاً

<div class="b" id="bn1"><div class="m1"><p>ره بس دور است توشه بردار و برو</p></div>
<div class="m2"><p>فارغ منشین تمام کن کار برو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چند کنی جمع که تا چشم زنی</p></div>
<div class="m2"><p>فرمان آید که جمله بگذار برو</p></div></div>