---
title: >-
    بخش ۳۰ - غزل در بیان مقام انس با حق تعالی
---
# بخش ۳۰ - غزل در بیان مقام انس با حق تعالی

<div class="b" id="bn1"><div class="m1"><p>انس چون بادوست باشد باد و آتش هم توئی</p></div>
<div class="m2"><p>انس چون با دوست باشد آدم عالم توئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انس چون با دوست باشد ذره‌ها دریا شود</p></div>
<div class="m2"><p>انس چون با دوست باشد رازها پیدا شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>انس چون بادوست باشد طالبان مطلوب شد</p></div>
<div class="m2"><p>انس چون با دوست باشد هر بلا محبوب شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>انس چون بادوست باشدخاکدان شدآسمان</p></div>
<div class="m2"><p>انس چون با دوست باشد خودمکان شدلامکان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>انس چون بادوست باشددوزخت جنت شود</p></div>
<div class="m2"><p>انس چون بادوست باشد لعنتت رحمت شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>انس چون بادوست باشد این جهان شدآن جهان</p></div>
<div class="m2"><p>انس چون با دوست باشد سر پنهان شد عیان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>انس چون با نور باشد نار را تونور دان</p></div>
<div class="m2"><p>انس چون بادوست باشد دیو را تو حور دان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>انس چون بادوست باشدظلمت تو روشنست</p></div>
<div class="m2"><p>انس چون بادوست باشد گلخن تو گلشن است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>انس چون بادوست باشد راه تو منزل شود</p></div>
<div class="m2"><p>انس چون با دوست باشد کام تو حاصل شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p> </p></div>
<div class="m2"><p></p></div></div>