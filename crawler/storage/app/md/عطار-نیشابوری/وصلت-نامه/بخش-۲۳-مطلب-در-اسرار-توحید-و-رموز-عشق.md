---
title: >-
    بخش ۲۳ - مطلب در اسرار توحید و رموز عشق
---
# بخش ۲۳ - مطلب در اسرار توحید و رموز عشق

<div class="b" id="bn1"><div class="m1"><p>ای برادر غیر حق خود نیست کس</p></div>
<div class="m2"><p>اهل معنی را همین یک حرف بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر تو غیر حق به بینی درجهان</p></div>
<div class="m2"><p>بر تو روشن گردد اسرار نهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر تو اندر راه یک بینی شوی</p></div>
<div class="m2"><p>از وجود خویشتن فانی شوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن رزمان ز اسرار حق یا بی‌خبر</p></div>
<div class="m2"><p>خودشوی از جسم وجان کلی بدر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عقل از این گفتن چه سودا می‌کند</p></div>
<div class="m2"><p>عشق هر دم خانه یغما می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیراین راهت یقین تو عشق دان</p></div>
<div class="m2"><p>تا رسی اندر مقام لامکان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقل را بگذار در راه ای پسر</p></div>
<div class="m2"><p>تا نمانی اندر این ره کور و کر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عقل شیطان را براه آوردنست</p></div>
<div class="m2"><p>زان سبب در راه او سر تافتن است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عقل و شیطان گفت ما زادیم بهم</p></div>
<div class="m2"><p>عقل و شیطان فکر روحانی بهم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حق تعالی گفت ای ملعون راه</p></div>
<div class="m2"><p>از طریق عشق بیرونی ز راه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آدم معنی ندیدی ای لعین</p></div>
<div class="m2"><p>روح پاکست رحمة للعالمین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>او من است و من ویم ای بی‌خبر</p></div>
<div class="m2"><p>لاجرم نادیده گشتی کور و کر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر ترا دیده بدی در راه ما</p></div>
<div class="m2"><p>آدم ما را بدیدی همچو ما</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون ندیدی آدم ما را یقین</p></div>
<div class="m2"><p>نام تو کردند ابلیس لعین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای برادر در کمال خویش باش</p></div>
<div class="m2"><p>در ره توحید حق بی‌کیش باش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بگذر از کیش ونفاق وکفر و دین</p></div>
<div class="m2"><p>تا رسی در قرب رب العالمین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>این نه راه تست ای طفل نژند</p></div>
<div class="m2"><p>راه شیرانست و مرد هوشمند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زاد این ره نیستی می‌دان یقین</p></div>
<div class="m2"><p>شک بسوزان و ببر از کفر و دین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خودپرستان اندر این ره گمرهند</p></div>
<div class="m2"><p>از طریق نیستی آگه نیند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نفس ایشان رد راه عشق شد</p></div>
<div class="m2"><p>عارفان را راه پیش از عشق شد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عقل را بگزین ونفسک را بسوز</p></div>
<div class="m2"><p>نفس تاریکت بگردد همچو روز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نفس را بت دان و بت را برشکن</p></div>
<div class="m2"><p>تا رسی در بارگاه ذوالمنن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نفس را اینجا حجاب راه دان</p></div>
<div class="m2"><p>این سخن را از دل آگاه دان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر که اندر بند نفس خویش ماند</p></div>
<div class="m2"><p>از ره حق همچو کافر کیش ماند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>این نه تقلید است نه راه هوا</p></div>
<div class="m2"><p>راه تحقیقست و راه مصطفی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>راه احمد بود توحید ای پسر</p></div>
<div class="m2"><p>از ره توحید حق شد باخبر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در ره توحید جان ایثار کرد</p></div>
<div class="m2"><p>دیده با دیدار حق دیدار کرد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>د رجلال حق جمال حق بدید</p></div>
<div class="m2"><p>در صفاتش ذات خود را حق بدید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اندر این ره کاملی باید شگرف</p></div>
<div class="m2"><p>تا کند غواصی این بحر ژرف</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>صدهزاران طالب اینجا سرنهاد</p></div>
<div class="m2"><p>تا که یک کس اندر این ره پا نهاد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>صدهزاران خلق حیران مانده‌اند</p></div>
<div class="m2"><p>اندر این ره زار و گریان مانده‌اند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>صدهزاران عارفان درگفتگو</p></div>
<div class="m2"><p>اندر این ره لوح دل در شستشو</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>عاشقانه آتشی زن در دو کون</p></div>
<div class="m2"><p>تا رهی از نقشهای لون لون</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نقشها را جمله در آتش بسوز</p></div>
<div class="m2"><p>بعد از آن شمع وصالت برفروز</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چون نماند نقشها اندر میان</p></div>
<div class="m2"><p>آن زمان نقاش را بینی عیان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بازگویم سر اسرار نهان</p></div>
<div class="m2"><p>ای برادر نقش را نقاش دان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چون ترا باشد کمال دین حق</p></div>
<div class="m2"><p>خویش را هرگز نبینی جز بحق</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چون ترا معلوم گردد آن عیان</p></div>
<div class="m2"><p>غیر حق هرگز نبینی در میان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هرچه بینی آن تو باشی بیشکی</p></div>
<div class="m2"><p>چه صداست و چه هزار و چه یکی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>جمله اجزای تواند ای بی‌خبر</p></div>
<div class="m2"><p>ذات کلی این جهان را سر بسر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>عرش و فرش و لوح و کرسی و قلم</p></div>
<div class="m2"><p>از توشان شد علم در عالم عَلَم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نور تو از هر دو عالم برتر است</p></div>
<div class="m2"><p>این جهان و آن جهان را برتر است</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گر شود چشمت بنور خویش باز</p></div>
<div class="m2"><p>قدسیان پایت ببوسند از نیاز</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>جوهر تو جملهٔ کروبیان</p></div>
<div class="m2"><p>چون بدیدند سجده کردند آن زمان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>جهد کن تا جوهرت آید به چنگ</p></div>
<div class="m2"><p>تا رهی از گیر ودار و صلح و جنگ</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>جوهر جان در هوس گم کرده‌ای</p></div>
<div class="m2"><p>با هوای نفس خود خو کرده‌ای</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>داده‌ای بر باد عمر جاودان</p></div>
<div class="m2"><p>یک زمان آگه نه‌ای از سرّ جان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گر شوی آگه ز جان خویشتن</p></div>
<div class="m2"><p>ترک گیری این حدیث ما و من</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>جمله را یک رنگ بین مرد خدا</p></div>
<div class="m2"><p>تا نبینی غیر او راتو جدا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دو مبین ذاتش تو ای مرد ولی</p></div>
<div class="m2"><p>تا نباشی در مقام احولی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گر تو راه عشق را مایل شوی</p></div>
<div class="m2"><p>یک ره و یک قبله و یک دل شوی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ننگری از هیچ سوای مرد کار</p></div>
<div class="m2"><p>دائماً از عشق باشی بی‌قرار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>عشق جانان جوهر جان آمده است</p></div>
<div class="m2"><p>زان سبب از خلق پنهان آمده است</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>هست پیدا لیک پنهان از شما</p></div>
<div class="m2"><p>کی شود خفاش را تاب ضیاء</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>این جهان و آن جهان با هم ببین</p></div>
<div class="m2"><p>بگذر از راه گمان می‌بین یقین</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>عشق باعشاق بین آمیخته</p></div>
<div class="m2"><p>روح اندر خاک دان آویخته</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چندگویم ای پسر در من نگر</p></div>
<div class="m2"><p>تا ببینی خویش را در خود مگر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>گفت پیغمبر که ما اخوان شدیم</p></div>
<div class="m2"><p>همدگر را آینه از جان شدیم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>جسم واحد خواند ما را آن زمان</p></div>
<div class="m2"><p>انبیا و اولیا را این زمان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>وانمود او سر اسرار عدم</p></div>
<div class="m2"><p>آوریده درمعنی از قدم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>سر حق را وانمود از لطف حق</p></div>
<div class="m2"><p>آورید از بحر معنی این سبق</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>راه را بنموده آن بحر صفا</p></div>
<div class="m2"><p>تا شود عارف به حق خیرالورا</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>عارفان زین معرفت دریافتند</p></div>
<div class="m2"><p>سالکان مرکب در این ره تاختند</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>طالبان در جستجوی او بدند</p></div>
<div class="m2"><p>عالمان در گفتگوی او شدند</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>زاهدان یک شمه از او یافتند</p></div>
<div class="m2"><p>سالها با سوختن در ساختند</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>عاشقان دیدند روی او عیان</p></div>
<div class="m2"><p>دست‌ها شستند در ساعت ز جان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>راه بر علم محمد(ص) آمد است</p></div>
<div class="m2"><p>اسم او محمود و احمد آمد است</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>راه از وی جو اگر تو رهروی</p></div>
<div class="m2"><p>تا نمانی در بلای کجروی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>گر به فقرت نیست فخری چون رسول</p></div>
<div class="m2"><p>هست راهت کفر و دینت بی‌اصول</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>گر ز دنیا ور ز عقبی نگذری</p></div>
<div class="m2"><p>در ره احمد تو هم کور و کری</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>راه راه اوست هم دنیا ودین</p></div>
<div class="m2"><p>سر حقست رحمة للعالمین</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>هر که از راه محمد راه یافت</p></div>
<div class="m2"><p>سر حق را از دل آگاه یافت</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>احمدآنجا بد احد ای مرد کار</p></div>
<div class="m2"><p>سر حق را با تو کردم آشکار</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>میم را بر دار احمد شد احد</p></div>
<div class="m2"><p>فهم کن تحقیق الله الصمد</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>هست این اسرار از جای دگر</p></div>
<div class="m2"><p>سر این را کی شناسد کور و کر</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>کور را خود از رخ زیبا چه سود</p></div>
<div class="m2"><p>گرچه داند لذت آواز عود</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>کور و کر از راه عقبی مانده‌اند</p></div>
<div class="m2"><p>روز و شب در بند دنیا مانده‌اند</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>راه بینا عین توحید آمد است</p></div>
<div class="m2"><p>منزلش تجرید و تفرید آمد است</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بگذر از هستی خود یکبارگی</p></div>
<div class="m2"><p>تا زوصلش برخوری یکبارگی</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>خودپرستی راه شیطان آمد است</p></div>
<div class="m2"><p>بت شکستن راه یزدان آمد است</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بت شکن در راه حق ای مرد کار</p></div>
<div class="m2"><p>تا نباشی در قیامت شرمسار</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>گر ز خود نتوانی این بت را شکست</p></div>
<div class="m2"><p>کی بیاری راه وصلت را به دست</p></div></div>