---
title: >-
    بخش ۹ - رجوع به قصه
---
# بخش ۹ - رجوع به قصه

<div class="b" id="bn1"><div class="m1"><p>آن حکیم پر خرد در آینه</p></div>
<div class="m2"><p>جمله یکتا دید او معاینه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن حکیم پر هنر را روح دان</p></div>
<div class="m2"><p>نفس شومت احول آمد در میان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روح اندر عالم وحدت بود</p></div>
<div class="m2"><p>نفس شومت عالم کثرت بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل بدان آیینه از روی کمال</p></div>
<div class="m2"><p>اندر او می بین جمال ذوالجلال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندر این ره گر تو صاحب دل شوی</p></div>
<div class="m2"><p>بی‌گمان و بی‌یقین واصل شوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روح و نفس و عقل و دل هر دو یکیست</p></div>
<div class="m2"><p>مرد معنی را در اینجا کی شکیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چونکه ره بین شد تو آنرا روح دان</p></div>
<div class="m2"><p>تا که کژبین است نفس شوم دان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عقل صورت می‌گذار این دم بتاب</p></div>
<div class="m2"><p>عشق صورتها کند مات و خراب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عقل اندازد ترا اندر فراق</p></div>
<div class="m2"><p>عشق بدهد غیر حق را سه طلاق</p></div></div>