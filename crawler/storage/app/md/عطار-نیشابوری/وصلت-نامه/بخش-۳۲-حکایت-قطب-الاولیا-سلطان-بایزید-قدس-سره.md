---
title: >-
    بخش ۳۲ - حکایت قطب الاولیاء سلطان بایزید قدس سره
---
# بخش ۳۲ - حکایت قطب الاولیاء سلطان بایزید قدس سره

<div class="b" id="bn1"><div class="m1"><p>سائلی بنشست پیش بایزید</p></div>
<div class="m2"><p>گفت از لطف خدای برمزید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ره حق دائماً مردانه‌ای</p></div>
<div class="m2"><p>در میان عارفان فرزانه‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راه حق راتو به جان کوشیده‌ای</p></div>
<div class="m2"><p>دائماً از شوق حق جوشیده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو شراب سر حق نوشیده‌ای</p></div>
<div class="m2"><p>سر اسرار خدا پوشیده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر سبحانی ز تو شد آشکار</p></div>
<div class="m2"><p>در میان عاشقان نامدار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان و تن را در طلب بگداختی</p></div>
<div class="m2"><p>تا کمال معرفت در یافتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر دو عالم را در این ره باختی</p></div>
<div class="m2"><p>مرکب معنی در این ره تاختی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در وجود خویشتن فانی شدی</p></div>
<div class="m2"><p>در بقای حق بحق باقی شدی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیدهٔ نفس بهیمی دوختی</p></div>
<div class="m2"><p>این جهان و آن جهان را سوختی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سر تو از فکر جمله برتر است</p></div>
<div class="m2"><p>فکر تو از عرش اعلی برتر است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مظهر تحقیق و تجرید آمدی</p></div>
<div class="m2"><p>لاجرم در عین توحید آمدی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غیر حق را اندر این ره سوختی</p></div>
<div class="m2"><p>چشم خود بینی در این ره دوختی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طالبان و سالکان در راه تو</p></div>
<div class="m2"><p>جمله همچون چاکرند شاه تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عاشقان در راه تو حیران شدند</p></div>
<div class="m2"><p>عارفان از درد تو بیجان شدند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زاهدان از زهد تو وامانده‌اند</p></div>
<div class="m2"><p>عالمان از علم تو درمانده‌اند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پیر ما در ره توئی این دم یقین</p></div>
<div class="m2"><p>نام تو کردند سلطان عارفین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مشکلی افتاده اندر ره مرا</p></div>
<div class="m2"><p>مشکل ما را بکن حالی روا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اندر این ره می‌روم با پا و سر</p></div>
<div class="m2"><p>هر زمان پیش آیدم لونی دگر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گاه نورانی و گه ظلمانیم</p></div>
<div class="m2"><p>گاه روحانی و گه نفسانیم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گاه در علویم و گه در اسفلی</p></div>
<div class="m2"><p>گاه در عقلیم و گه در غافلی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گاه طالب گاه مطلوب آمدم</p></div>
<div class="m2"><p>گه محب و گاه محبوب آمدم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گاه عاشق گاه صادق آمدم</p></div>
<div class="m2"><p>گه منافق گاه فاسق آمدم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گه محقق گه موحد آمدم</p></div>
<div class="m2"><p>گاه زاهد گه مقلد آمدم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر زمان لون دگر می‌دیده‌ام</p></div>
<div class="m2"><p>اندر این ره راه را نادیده‌ام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گفت سلطانش چو انس حق رسد</p></div>
<div class="m2"><p>این خیالات از سرت بیرون کند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چونکه انس حق ترا حاصل شود</p></div>
<div class="m2"><p>راه حق در پیش تو واصل شود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اندر این ره جسم تو یکتا شود</p></div>
<div class="m2"><p>طالب و مطلوب هم یکجا شود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>علو را در سفل بینی ای پسر</p></div>
<div class="m2"><p>بشنو این اسرار شو صاحب نظر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نور در ظلمت ببینی آشکار</p></div>
<div class="m2"><p>فهم کن اسرار ای مرد کبار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>عشق و عاشق هر دو را محبوب دان</p></div>
<div class="m2"><p>سالک و طالب همه مطلوب دان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>یافتن اینجا بود نایافتن</p></div>
<div class="m2"><p>گم شدن اینجا بود پیدا شدن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هست را میدان در این ره غافلی</p></div>
<div class="m2"><p>خیز ونادان شو اگر تو عاقلی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بعد از آن بینی انیس با جلیس</p></div>
<div class="m2"><p>اندر این منزل شوی روح نفیس</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دائماً بنشسته باشی با خدا</p></div>
<div class="m2"><p>فارغ ازکبر و نفاق و از هوا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>روح اندر خلوت جانان بود</p></div>
<div class="m2"><p>در حریم وصل با جانان بود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یک زمان غایب نباشی از خدا</p></div>
<div class="m2"><p>دائماً از نور حق گیری ضیاء</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سر این اسرار را حاصل کنی</p></div>
<div class="m2"><p>جان و دل در معرفت کامل کنی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در جلیس این جسم تو چون جان شود</p></div>
<div class="m2"><p>در حریم حضرت جانان شود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در جلیسی با خدا و مصطفی</p></div>
<div class="m2"><p>هم انیست می‌شود دایم صفا</p></div></div>