---
title: >-
    بخش ۲۲ - ادامه
---
# بخش ۲۲ - ادامه

<div class="b" id="bn1"><div class="m1"><p>جملهٔ مردان ز خود فانی شدند</p></div>
<div class="m2"><p>در بقای حق به حق باقی شدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرتو مرد راه عشقی راه رو</p></div>
<div class="m2"><p>همچو مردان از دل آگاه رو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جملهٔ مردان ز خود بیرون شدند</p></div>
<div class="m2"><p>در ره عشاق غرق خون شدند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جسم و جان و تن همه در باختند</p></div>
<div class="m2"><p>تا کمال راه حق بشناختند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هستی خود را زره برداشتند</p></div>
<div class="m2"><p>نیستی را اندرین ره داشتند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مال و ملک و آب و جاه این جهان</p></div>
<div class="m2"><p>جمله را انداخته پیش خسان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زهد را و علم و قیل و قال را</p></div>
<div class="m2"><p>وسوسه بوده همه این حال را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صورت خود را به کل کردن خراب</p></div>
<div class="m2"><p>این جهان در پیش ایشان چون سراب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیده را از غیر حق بردوختند</p></div>
<div class="m2"><p>غیر حق را اندر این ره سوختند</p></div></div>