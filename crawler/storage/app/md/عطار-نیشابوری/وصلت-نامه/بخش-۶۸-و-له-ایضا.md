---
title: >-
    بخش ۶۸ - و له ایضاً
---
# بخش ۶۸ - و له ایضاً

<div class="b" id="bn1"><div class="m1"><p>لاله ز رخ چو ماه می‌بینم من</p></div>
<div class="m2"><p>سبزه ز خط سیاه می‌بینم من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وان کاسهٔ سرکه بود پر باد غرور</p></div>
<div class="m2"><p>پیمانهٔ خاک راه می‌بینم من</p></div></div>