---
title: >-
    بخش ۲۶ - الحکایت و الرموز و شرح حال آن جوان که عزم کعبه کرد
---
# بخش ۲۶ - الحکایت و الرموز و شرح حال آن جوان که عزم کعبه کرد

<div class="b" id="bn1"><div class="m1"><p>بود برنائی بغایت ماهرو</p></div>
<div class="m2"><p>پیش خلق عالمی پر آب و رو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مال و ملکی داشت بی‌حد آن غلام</p></div>
<div class="m2"><p>در نشابورش بدی او را مقام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود یک خیلی همه خویشان او</p></div>
<div class="m2"><p>دائماً خویشان دل پیشان او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روز و شب در خدمتش بودند شاد</p></div>
<div class="m2"><p>جمله همچون چاکران کیقباد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ماهرویان خطائی و سرای</p></div>
<div class="m2"><p>بود اندر خدمت آن پاکرای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روز و شب در غرق شادی و طرب</p></div>
<div class="m2"><p>بد نشسته فارغ از راه طلب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناگهان دردی درآمد در دلش</p></div>
<div class="m2"><p>در خیال کار شد بس مشکلش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عزم کعبه کرد آندم آن غلام</p></div>
<div class="m2"><p>پس وداعی کرد خویشان را تمام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زاد ره برداشت سوی قافله</p></div>
<div class="m2"><p>قافله می‌رفت هر دم مرحله</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن جوان می‌رفت در ره شادشاد</p></div>
<div class="m2"><p>تا رسید آن قافله در باغداد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون درآمد آن جوان در باغداد</p></div>
<div class="m2"><p>در تفرج گشت حج رفتش زیاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر زمان در یکطرف می‌گشت او</p></div>
<div class="m2"><p>جملهٔ خلقان بدیده گشت او</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر یکی سرگشتهٔ کردار خویش</p></div>
<div class="m2"><p>دل نهاده کار خود در کار خویش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر طرف هنگامهٔ ایستاده دید</p></div>
<div class="m2"><p>بهر نظاره زهر سو می‌دوید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بس عجایبهای گوناگون بدید</p></div>
<div class="m2"><p>خویشتن راهر زمان مجنون بدید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همچنان می‌رفت تا دجله رسید</p></div>
<div class="m2"><p>در تعجب ماند کشتی را بدید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفت یک ملاح او را ای پسر</p></div>
<div class="m2"><p>اندرآ در کشتی وزان سو گذر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اندر آن در کشتی و بغداد بین</p></div>
<div class="m2"><p>صدهزاران قامت شمشاد بین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اندرآ در کشتی ای سرو روان</p></div>
<div class="m2"><p>تا ببینی آن طرف آن سروران</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اندرآ در کشتی ای مرد حزین</p></div>
<div class="m2"><p>تا ببینی آن طرف صد نازنین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اندرآ در کشتی ای خو بروی</p></div>
<div class="m2"><p>تا ببینی آن طرف صد ماهروی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اندرآ در کشتی ای مرد لطیف</p></div>
<div class="m2"><p>تا ببینی آن طرف حسن ظریف</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اندرآ درکشتی و بنشین تو خوش</p></div>
<div class="m2"><p>تا ببینی آن طرف صد ماهوش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اندرآ در کشتی ومیکن نظار</p></div>
<div class="m2"><p>تا ببینی آن طرف صد گلعذار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اندرآ در کشتی ای مرد جوان</p></div>
<div class="m2"><p>تا ببینی آن طرف تیر وکمان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اندرآ در کشتی و شو در پناه</p></div>
<div class="m2"><p>تا ببینی آن طرف زلف سیاه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اندرآ در کشتی و میزن دو دست</p></div>
<div class="m2"><p>تا ببینی آن طرف چشمان مست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اندرآ درکشتی و بنشین بخند</p></div>
<div class="m2"><p>تا ببینی آن طرف لبها چو قند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اندرآ در کشتی این دم بیقرار</p></div>
<div class="m2"><p>تا ببینی آن طرف روی نگار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اندرآ در کشتی و بنشین خموش</p></div>
<div class="m2"><p>تا ببینی آن طرف صد باده نوش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>وسوسه کردش بسی آن بوالفضول</p></div>
<div class="m2"><p>تا فریبانید او را همچو غول</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>رفت در کشتی و شد زانسوی شط</p></div>
<div class="m2"><p>شد ز گفت آن لعین او را غلط</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هر کنار شط یکی قصری بدید</p></div>
<div class="m2"><p>چشم او هرگز چنان قصری ندید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بر سر آن قصر یک دختر چو ماه</p></div>
<div class="m2"><p>بد نشسته چشم چون خال سیاه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در زمان آمد همان آزاد مرد</p></div>
<div class="m2"><p>دل بدست او بداد و خاک و خورد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دل به دست او بداد آن بیقرار</p></div>
<div class="m2"><p>گشت عاشق بر رخ آن گلعذار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در میان آمد ز دست گلعذار</p></div>
<div class="m2"><p>جامه رادرید بر تن تار تار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خاک بر سر کرد و او در خون فتاد</p></div>
<div class="m2"><p>عشق آن دختر چو آن مجنون فتاد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>زاد خود را پیش آن معشوقه برد</p></div>
<div class="m2"><p>گفت جانم از غم عشق تو مرد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>زاد راه او بخورد آن هیچکس</p></div>
<div class="m2"><p>مفلس و بیچاره ماند از همنفس</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دخترش گفت آن زمان زرها بیار</p></div>
<div class="m2"><p>تا نمایم روی خود ای گلعذار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گفت وصل وشادئی می‌بایدت</p></div>
<div class="m2"><p>بی‌زری این حاصلت کی آیدت</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بعد از آنش گفت برخیز و برو</p></div>
<div class="m2"><p>تا نگردد مال وملکت در گرو</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>پس خجل شد آن جوان زرمی‌نداشت</p></div>
<div class="m2"><p>عشق دختر رفت و کارش سر نداشت</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چون پسر زانحال بازآمد بدید</p></div>
<div class="m2"><p>پیره زالی در برابر شد پدید</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>هر دو چشمش از رق ودندان دراز</p></div>
<div class="m2"><p>چون بدید آن را و شد اندر گداز</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>یادش آمد آن زمان از قافله</p></div>
<div class="m2"><p>در دلش افتاد آن دم ولوله</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سر برهنه پا برهنه شد برون</p></div>
<div class="m2"><p>از دلش می‌رفت آن دم موج خون</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>هر که را می‌دید او از مردمان</p></div>
<div class="m2"><p>می‌بپرسید آن زمان از کاروان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>هاتفش گفتا که ای جان پدر</p></div>
<div class="m2"><p>قافله رفت و تو بودی بیخبر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بشنو این احوال از من ای فقیر</p></div>
<div class="m2"><p>وصف حال تست گر باشی بصیر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>قافله را رهروان دین بدان</p></div>
<div class="m2"><p>راه رفتند و رسیدند در جنان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>در بهشتند آن عزیزان در وصال</p></div>
<div class="m2"><p>محو گشته در جمال ذوالجلال</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>شهر بغدادت در اینجا کون دان</p></div>
<div class="m2"><p>در تعجب ماندهٔ در لون آن</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>هست آن دجله تو را این دم خیال</p></div>
<div class="m2"><p>جسم تو کشتی و غرقی در ضلال</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ای پسر ملاح را تو دیو دان</p></div>
<div class="m2"><p>وسوسه کرده ترا اندر جهان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بحر دنیا آن شیطان آمده است</p></div>
<div class="m2"><p>بیشکی در بحرکشتیبان بداست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>در طلسم کشتی آن دیو پلید</p></div>
<div class="m2"><p>صدهزاران خلق را درخون کشید</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>در طلسم کشتی آن دیو نژند</p></div>
<div class="m2"><p>سالکان را کرد هر دم پای بند</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>در طلسم کشتی آن دیو لعین</p></div>
<div class="m2"><p>ظالمان را باز داشت از راه دین</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>در طلسم کشتی و شد هم ز سر</p></div>
<div class="m2"><p>زشت بنموده به پیشت چون قمر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>در طلسم کشتی و لاوه گری</p></div>
<div class="m2"><p>دیو را بنموده پیشت چون پری</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چون بود راه تودر کشتی جسم</p></div>
<div class="m2"><p>قصر را بنموده آن دم در طلسم</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>دختر زیبای رخ را وانمود</p></div>
<div class="m2"><p>بود دنیا و ندانستی چه سود</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>دل ز دست خود بدادی ای غلام</p></div>
<div class="m2"><p>همرهان رفتند در خوابی مدام</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>عاشق دنیای دون رفتن ز دست</p></div>
<div class="m2"><p>در بلا و رنج ماندی پای بست</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>دختری بنمود دنیا بس ظریف</p></div>
<div class="m2"><p>در یقین بد پیره زالی بس خریف</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>همرهان رفتند و حج دریافتند</p></div>
<div class="m2"><p>کام خود از راه حق دریافتند</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>تو بماندی اندرین کون وفساد</p></div>
<div class="m2"><p>هر دمی کعبه همی دادی به باد</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>می‌روی هر سوی و می‌پرسی خبر</p></div>
<div class="m2"><p>قافله رفتند و ماندی کور و کر</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>هر که اودر کون ماند همچنین</p></div>
<div class="m2"><p>کی رسد در قرب رب العالمین</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>هر که او در بند دنیا بازماند</p></div>
<div class="m2"><p>بیشکی از راه مولا باز ماند</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>هر که را روئی در این عالم بود</p></div>
<div class="m2"><p>او کالانعام است کی آدم بود</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>هر که اندر عالم فانی بماند</p></div>
<div class="m2"><p>در عذاب جاودانی باز ماند</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>هر که در دنیای دون وامانده است</p></div>
<div class="m2"><p>از لقای حی بیچون مانده است</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>هر که در گرداب دنیا اوفتاد</p></div>
<div class="m2"><p>بی‌شکی از راه عقبی اوفتاد</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>هر که از دنیای دون شادان بود</p></div>
<div class="m2"><p>بی‌شکی در آتش سوزان بود</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>هر که را محبوب اودنیا بود</p></div>
<div class="m2"><p>در جهنم دائمش ماوا بود</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>هر که در دنیا به حرصی بازماند</p></div>
<div class="m2"><p>تو یقین می‌دان کز این ره بازماند</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>هر که در دنیا کند یاوه گری</p></div>
<div class="m2"><p>بی‌شکی باشد چو قوم سامری</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>هر که در دنیا به کامدل نشست</p></div>
<div class="m2"><p>هست او در راه دنیا بت پرست</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>هر که را شد قبله دنیا ای غلام</p></div>
<div class="m2"><p>ماند اندر آتش سوزان مدام</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>هر که او دنیای دون راترک کرد</p></div>
<div class="m2"><p>گرد نعلینش شرف بر جمله مرد</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>هر که از دنیای دون ماند خلاص</p></div>
<div class="m2"><p>او بود در راه حق خاص الخواص</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>هر که بند این جهان بر هم شکست</p></div>
<div class="m2"><p>در ره تحقیق باشد حق پرست</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>هر که از دنیای دون آزاد گشت</p></div>
<div class="m2"><p>در نعیم جاودانی شاد گشت</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>هر که ازدنیا و شغل او برست</p></div>
<div class="m2"><p>بر سریر جنت المأوا نشست</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>هر که دنیا را به چشمش ننگرد</p></div>
<div class="m2"><p>از نعیم جاودانی بر خورد</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>خانهٔ نفس است دنیا سر بسر</p></div>
<div class="m2"><p>بگذر از دنیا و شو صاحب نظر</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>هر که او در راه شیطانی بود</p></div>
<div class="m2"><p>بی‌شکی در کیش نفسانی بود</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>هر که رحمانی بود اندر جهان</p></div>
<div class="m2"><p>خاک او بهتر ز خون دیگران</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>طالب راه خدا باش ای پسر</p></div>
<div class="m2"><p>از ره شیطان ملعون کن حذر</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>در ره حق دائماً مردانه باش</p></div>
<div class="m2"><p>همچو مجنون در طلب دیوانه باش</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>راه رو از جانو دل ای مرد کار</p></div>
<div class="m2"><p>تا شوی در هر دو عالم بختیار</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>بگذر از نفس بهیمی ای فقیر</p></div>
<div class="m2"><p>عاشقانه دامن مردان بگیر</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>نفس سگ را اندر این ره خوار کن</p></div>
<div class="m2"><p>جان خود در راه حق ایثار کن</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>جهد کن تا در ره معنی رسی</p></div>
<div class="m2"><p>در حریم وصل آن مولی رسی</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>در بهشت عدن دائم جاودان</p></div>
<div class="m2"><p>باش اندر صحبت آن شادمان</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>گر بمانی اندر این ره ای جوان</p></div>
<div class="m2"><p>در بلا و درد مانی جاودان</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>پند من بشنو برو این راه را</p></div>
<div class="m2"><p>تا ببینی حضرت الله را</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>پند من بشنو وجود خود بباز</p></div>
<div class="m2"><p>عشق تو آید در این ره شاهباز</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>عشق چون خواند ترا جانان شوی</p></div>
<div class="m2"><p>آن زمان شایستهٔ رحمن شوی</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>عشق آنجا ره نماید مر ترا</p></div>
<div class="m2"><p>عشق آنجا در گشاید مر ترا</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>گر تو اندر راه حق عاشق شوی</p></div>
<div class="m2"><p>راه حق را آن زمان لایق شوی</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>اندر این ره عشق باید ای پسر</p></div>
<div class="m2"><p>تا شوی در راه معنی با خبر</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>عشق را دردی بباید ای فقیر</p></div>
<div class="m2"><p>درد باشد در دو عالم دستگیر</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>رودراین ره درد خواه ای مرد کار</p></div>
<div class="m2"><p>درد باشد اندر این ره بختیار</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>درد شد درمان جان عاشقان</p></div>
<div class="m2"><p>درد شد معشوق جان بی‌دلان</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>درگذر از راه تقلید و بیان</p></div>
<div class="m2"><p>درد باید تا شود راهت عیان</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>هر که را در راه بینش درد نیست</p></div>
<div class="m2"><p>خاک بر فرقش که آنکس مرد نیست</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>درد آمد اندر این ره پیر راه</p></div>
<div class="m2"><p>هر که با درد است آگه شد ز شاه</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>درد را بگزین و ترک قال کن</p></div>
<div class="m2"><p>جان خود را باز و ره در حال کن</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>درگذر از ذکر و زهد و قیل و قال</p></div>
<div class="m2"><p>درد را بگزین ز بی‌دردی بنال</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>درد درمان دل ما آمده است</p></div>
<div class="m2"><p>درد در جان رهبر ما آمده است</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>درد ما را ره نمود از وصل یار</p></div>
<div class="m2"><p>سر پنهان کرد بر ماآشکار</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>درد ما را برد اندر سر جان</p></div>
<div class="m2"><p>درد ما را برد اندر لامکان</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>درد ما را داد هر دم صد صفا</p></div>
<div class="m2"><p>درد ما را داد هر دم صد عطا</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>درد ما را داد هر دم خلعتی</p></div>
<div class="m2"><p>درد ما را داد هردم نعمتی</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>درد ما را از خودی فانی بکرد</p></div>
<div class="m2"><p>در بقای حق به حق باقی بکرد</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>درد ما را از جهان آزاد کرد</p></div>
<div class="m2"><p>درد هر دم جان ما را شاد کرد</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>درد ما را کرد بینا در جهان</p></div>
<div class="m2"><p>تا بدیدم سر پنهان و عیان</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>درد ما را برد راه مصطفی</p></div>
<div class="m2"><p>درد ما را داد سر اولیاء</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>درد ما را داد حال صوفیان</p></div>
<div class="m2"><p>درد ما را داد شوق عارفان</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>درد ما را برد اندر پیش حق</p></div>
<div class="m2"><p>حق به درد ما همی دادی سبق</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>درد ما را از خدا آگاه کرد</p></div>
<div class="m2"><p>درد راه حق بما کوتاه کرد</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>درد ما را قربت مسند نهاد</p></div>
<div class="m2"><p>بر سریر شوق آن حضرت نشاند</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>درد ما را در صف جان بار داد</p></div>
<div class="m2"><p>وانگهی در تخت جانان بار داد</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>درد ما را کرد راه حق عیان</p></div>
<div class="m2"><p>وانکه بی‌درد است کی یابد نشان</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>درد حاصل کن که درمان درد تست</p></div>
<div class="m2"><p>مقصد و مقصود جانان درد تست</p></div></div>