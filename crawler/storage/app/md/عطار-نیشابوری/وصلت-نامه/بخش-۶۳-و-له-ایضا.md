---
title: >-
    بخش ۶۳ - و له ایضاً
---
# بخش ۶۳ - و له ایضاً

<div class="b" id="bn1"><div class="m1"><p>بس خون که دلم ز اول کار بریخت</p></div>
<div class="m2"><p>تا آخر کار چون گل از خار بریخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرسبزی خاک از چه سبب می‌بایست</p></div>
<div class="m2"><p>چون زرد شد و بزاری زار بریخت</p></div></div>