---
title: >-
    بخش ۶۵ - و له ایضاً
---
# بخش ۶۵ - و له ایضاً

<div class="b" id="bn1"><div class="m1"><p>ماتمزدگان عالم خاک هنوز</p></div>
<div class="m2"><p>می خاک شوند در غم خاک هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندانکه تهی می‌شود از پشت زمین</p></div>
<div class="m2"><p>پرمی نشود این شکم خاک هنوز</p></div></div>