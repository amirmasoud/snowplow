---
title: >-
    بخش ۵۶ - و له ایضاً
---
# بخش ۵۶ - و له ایضاً

<div class="b" id="bn1"><div class="m1"><p>بگشای نظر خلق پراکند نگر</p></div>
<div class="m2"><p>سرگردانی مرده و زنده نگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از شربت ناگوار دنیا به منال</p></div>
<div class="m2"><p>در شریت گور ناگوارنده نگر</p></div></div>