---
title: >-
    بخش ۶۱ - و له ایضاً
---
# بخش ۶۱ - و له ایضاً

<div class="b" id="bn1"><div class="m1"><p>در حبس وجود از چه افتادم من</p></div>
<div class="m2"><p>کز ننگ وجودخود بفریادم من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون می‌مردم بصد هزاران زاری</p></div>
<div class="m2"><p>از مادر خویشتن چرا زادم من</p></div></div>