---
title: >-
    بخش ۶ - الحکایت الوصال فی شرح البلال
---
# بخش ۶ - الحکایت الوصال فی شرح البلال

<div class="b" id="bn1"><div class="m1"><p>بشنو این رمز از بلال با وفا</p></div>
<div class="m2"><p>خواجهٔ ما و غلام مصطفی(ص)</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اوفتاده بود آن دُر ثمین</p></div>
<div class="m2"><p>در میان آن جهودان لعین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرد دین بودو طلبکار آمده</p></div>
<div class="m2"><p>عشق احمد را خریدار آمده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روز و شب در دین حق بیدار بود</p></div>
<div class="m2"><p>واقف سرّ بود و مرد کار بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز بهر آن جهودان کار کرد</p></div>
<div class="m2"><p>شب همه شب خدمت جبار کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن جهودان لعین گمره شدند</p></div>
<div class="m2"><p>از طریق عشق او آگه شدند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چندتن زان گمرهان جمع آمدند</p></div>
<div class="m2"><p>بر بلال پاک دین ناحق زدند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا بگردانند ز دین مصطفی</p></div>
<div class="m2"><p>ترک دارند این طریق با صفا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو چرا در راه دین او روی</p></div>
<div class="m2"><p>هم زجان تو مؤذن احمد شوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دین او را تو چرا کردی قبول</p></div>
<div class="m2"><p>گشته‌ای از راه ما تو بوالفضول</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت او راه حقست ومهتر است</p></div>
<div class="m2"><p>راهتان باطل به پیشش ابتر است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بعد از آن او را ببستند آن سگان</p></div>
<div class="m2"><p>چوبها بر وی زدند از قهر آن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پس بلال از شوق او گفتی احد</p></div>
<div class="m2"><p>قادر و فرد و خداوند و صمد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر هزاران پاره گردد جسم من</p></div>
<div class="m2"><p>بیشکی دانم ترا بی‌ما و من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ما و من برگیر و بگذار از دوئی</p></div>
<div class="m2"><p>تا در این ره مرد صاحب سر شوی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون بلال باصفا بگذر ز خود</p></div>
<div class="m2"><p>تا رهی از ننگ ونام و نیک و بد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا دم آخر به یکتائی رسی</p></div>
<div class="m2"><p>در کمال ذات یکتائی رسی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون تو یکتا گشتهٔ ای محترم</p></div>
<div class="m2"><p>بگذری از کفر و از اسلام هم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون تو یکتا باشی ای مرد یقین</p></div>
<div class="m2"><p>هم ز دنیا بگذری و هم ز دین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون تو یکتا باشی ای مرد خدا</p></div>
<div class="m2"><p>پس بقا باشد ترا بعد از فنا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون تو یکتا باشی ای مرد فقیر</p></div>
<div class="m2"><p>بر همه عالم شوی سلطان و میر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون تو یکتا باشی اندر لامکان</p></div>
<div class="m2"><p>ساقیت باشند هر دم قدسیان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون تو یکتا باشی اندر بحر نور </p></div>
<div class="m2"><p>وصل یابی و شوی اندر حضور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون تو یکتا باشی اندر بحر جان</p></div>
<div class="m2"><p>جان نماید خویشتن را در زمان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون تو یکتا باشی اندر سرّ جان</p></div>
<div class="m2"><p>سرّ دل را یابی هم از سر جان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون تو یکتا باشی اندر سرّ دل</p></div>
<div class="m2"><p>سرّ دل را یابی هم از سر دل</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون تو یکتا باشی اندر معرفت</p></div>
<div class="m2"><p>معرفت آید ترا هر دم صفت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چون تو یکتا باشی هر دم راه را</p></div>
<div class="m2"><p>مات سازی هر زمان صد شاه را</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون که تو یکتا شدی در درد عشق</p></div>
<div class="m2"><p>بیشکی گردی تو آن دم مرد عشق</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چون تو یکتا گشتی کل یکتا بدان</p></div>
<div class="m2"><p>سر معنی کرده‌ام با تو بیان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چون جهان جمله ز یک پیدا شده است</p></div>
<div class="m2"><p>عقلها جمله ز یک گویا شده است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>انبیا جمله ز یک گفتند باز</p></div>
<div class="m2"><p>از یکی گشتند ایشان سرفراز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شرع و ترتیب از یکی شد آشکار</p></div>
<div class="m2"><p>بشنو این معنی تو یکدم گوش دار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آسمانها از یکی گردان شده</p></div>
<div class="m2"><p>ماه و خورشید از یکی تابان شده</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از یکی شد این نجوم بیشمار</p></div>
<div class="m2"><p>از یکی شد هفت و نه پنج و چهار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از یکی شد این جهان پرگفتگو</p></div>
<div class="m2"><p>از یکی شد عالمی در جستجو</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از یکی شد کوه پیدا در جهان</p></div>
<div class="m2"><p>از برای ساکنی این جهان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از یکی پیدا شده اشجارها</p></div>
<div class="m2"><p>این جهان را فیض داده بارها</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از یکی پیدا شده باد و هوا</p></div>
<div class="m2"><p>این جهان را داده هر دم صدصفا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>از یکی پیدا شده آب روان</p></div>
<div class="m2"><p>این جهان را سبز کرده رایگان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از یکی پیدا شده خیل و حشم</p></div>
<div class="m2"><p>اشتر و اسب و خر وگاو و غنم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از یکی پیدا شده در و گهر</p></div>
<div class="m2"><p>سنگ و یاقوت و ز لعل معتبر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از یکی پیدا شده وحش و طیور</p></div>
<div class="m2"><p>هر یکی را صد عطا و صد سرور</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>از یکی پیدا شده صد نازنین</p></div>
<div class="m2"><p>هر یکی را در لباس خوش ببین</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>از یکی پیدا شده صد ماهروی</p></div>
<div class="m2"><p>سروقدی تنگ چشمی مشک موی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>از یکی پیدا شده صد دلفریب</p></div>
<div class="m2"><p>کرده بر عشاق هر دم صد عتیب</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>از یکی پیدا شده صد گل عذار</p></div>
<div class="m2"><p>ابروان چون حاجبی چشم خمار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>از یکی پیدا شده صد نامدار</p></div>
<div class="m2"><p>عاشقان را کرده هر دم جان نزار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>از یکی پیدا شده صد خوشه‌چین</p></div>
<div class="m2"><p>چشمها بادام و لبها شکرین</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>از یکی پیدا شده صدماهوش</p></div>
<div class="m2"><p>دستشان درگردن هر یک چه خوش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>از یکی پیدا شده صد مه لقا</p></div>
<div class="m2"><p>عاشقان را گشته هر دم از جفا</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>از یکی پیدا شده هر دو جهان</p></div>
<div class="m2"><p>از یکی شد آشکار اونهان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>از یکی پیدا شده این عقل وجان</p></div>
<div class="m2"><p>سر این معنی بدانند عارفان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>از یکی آمد علوم انبیا</p></div>
<div class="m2"><p>از یکی گشته حضور اولیا</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>از یکی آمد خلیل وذوفنون</p></div>
<div class="m2"><p>در ره حق تاجدار و رهنمون</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>از یکی آمد نبوت در جهان</p></div>
<div class="m2"><p>از یکی آمد ولایت در عیان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>از یکی احمد شده سالار و شاه</p></div>
<div class="m2"><p>عقلها را بر گرفته او ز راه</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>از یکی موسی شده صاحبقران</p></div>
<div class="m2"><p>دم نیاورده ز بیم لن تران</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>از یکی عیسی شده بر آسمان</p></div>
<div class="m2"><p>ترک کرده او مکان خاکدان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>از یکی بین هرچه بینی سر بسر</p></div>
<div class="m2"><p>چه بدو چه نیک چه خشک و چه تر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>این همه تفسیر از بهر یکی است</p></div>
<div class="m2"><p>مرد معنی را در اینجا کی شکی است</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>این یکی خود از یکی آمد مدام</p></div>
<div class="m2"><p>تو یکی اندر یکی بین والسلام</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>خود یکی اندر یکی یکی بود</p></div>
<div class="m2"><p>اندر این معنی کجا شکی بود</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>این یکی اندر یکی توحید دان</p></div>
<div class="m2"><p>بر دل و جان این سخن تحقیق دان</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>خود یک اندر یک بدان ای بیخبر</p></div>
<div class="m2"><p>تا شوی درمعرفت صاحب نظر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>این یک اندر یک تو عشق روح دان</p></div>
<div class="m2"><p>این رموز از جملگی مفتوح دان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>این یک اندر یک خدا باشد خدا</p></div>
<div class="m2"><p>بشنو این معنی پاک با صفا</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ذات حق را در صفات حق ببین</p></div>
<div class="m2"><p>بگذر از کفر و رها کن کیش و دین</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>پس جمالش در جلالش بازبین</p></div>
<div class="m2"><p>شک بسوزان و گذر کن از یقین</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>پس نهان اندر عیان میدان مدام</p></div>
<div class="m2"><p>چون عیان اندر نهان میدان مدام</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>هم عیان و هم نهان هردو بهم</p></div>
<div class="m2"><p>هم درون و هم برون لطف و کرم</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>هم زمین و هم سما و هم فلک</p></div>
<div class="m2"><p>هم بروج و هم نجوم و هم ملک</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>هم نبی و هم علی و هم ولی</p></div>
<div class="m2"><p>دو مبین تاتو نباشی احولی</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>چون یکی آمد یکی شد کل یکی</p></div>
<div class="m2"><p>حق ببین معنی کجا باشد شکی</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>خود یکی آمد یکی می‌بین همه</p></div>
<div class="m2"><p>عقل احول گشته اندر دمدمه</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>دمبدم در هر مکانی رخ نمود</p></div>
<div class="m2"><p>چون مکانش نیست هر جائی که بود</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>این سخن از ترجمانی دیگر است</p></div>
<div class="m2"><p>عارفان را خود نشانی دیگر است</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>این سخن از لامکان آورده‌ام</p></div>
<div class="m2"><p>سر مخفی رایگان آورده‌ام</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>این سخن از عقل و از جان برتر است</p></div>
<div class="m2"><p>این کسی داند که عالی گوهر است</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>این سخن از عرش اعلی آمده است</p></div>
<div class="m2"><p>از رموز حق تعالی آمده است</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>این سخن از بهر مشتاق آمده است</p></div>
<div class="m2"><p>از برای جان مشتاق آمده است</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>این سخن از بحر معنی آمده است</p></div>
<div class="m2"><p>نه بدعوی نه بفتوی آمده است</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>این سخن از سر وحدت آمده است</p></div>
<div class="m2"><p>نز ره تقلید وکثرت آمده است</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>این سخن از غایت درد آمده است</p></div>
<div class="m2"><p>در طریق عاشقی فرد آمده است</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>این سخن از سر پنهان آمده است</p></div>
<div class="m2"><p>صدهزاران گوهر جان آمده است</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>این سخن برهان معنی آمده است</p></div>
<div class="m2"><p>از طریق عشق مولی آمده است</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>این سخن از عشق جانان آمده است</p></div>
<div class="m2"><p>لاجرم از عقل پنهان آمده است</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>این سخن عارف بداند بیشکی</p></div>
<div class="m2"><p>زان بداند این رموز حق یکی </p></div></div>
<div class="b" id="bn89"><div class="m1"><p>گر ترا درد است درمان هم بود</p></div>
<div class="m2"><p>گر ترا عشقست جانان هم بود</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>درگذر از زهد و علم و قال قیل</p></div>
<div class="m2"><p>درد را بگزین و میکش بار فیل</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>درگذر از ذکر و فکر و معرفت</p></div>
<div class="m2"><p>درد را بگزین و شو در تعزیت</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>درگذر از این جهان و آن جهان</p></div>
<div class="m2"><p>چند باشی آشکارا و نهان</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>درگذر از خویشتن یکبارگی</p></div>
<div class="m2"><p>تا رسی در عالم بیچارگی</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>بگذر از خود پاک و کلی شو فنا</p></div>
<div class="m2"><p>تا شوی اندر فنا عین بقا</p></div></div>
<div class="b" id="bn95"><div class="m1"><p> گر یکی بینی تو جان ره بین شوی</p></div>
<div class="m2"><p>در دو بینی احولی کژ بین شوی</p></div></div>