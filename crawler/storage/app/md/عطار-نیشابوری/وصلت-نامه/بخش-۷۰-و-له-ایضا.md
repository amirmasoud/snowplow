---
title: >-
    بخش ۷۰ - و له ایضاً
---
# بخش ۷۰ - و له ایضاً

<div class="b" id="bn1"><div class="m1"><p>بر بستر خاک خفتگان می‌بینم</p></div>
<div class="m2"><p>در زیر زمین نهفتگان می‌بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندانکه به صحرای عدم می‌نگرم</p></div>
<div class="m2"><p>ناآمده‌گان و رفتگان می‌بینم</p></div></div>