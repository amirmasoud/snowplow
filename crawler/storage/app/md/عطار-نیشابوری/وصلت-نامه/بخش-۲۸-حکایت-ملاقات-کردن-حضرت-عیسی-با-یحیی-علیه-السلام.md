---
title: >-
    بخش ۲۸ - حکایت ملاقات کردن حضرت عیسی با یحیی علیه السلام
---
# بخش ۲۸ - حکایت ملاقات کردن حضرت عیسی با یحیی علیه السلام

<div class="b" id="bn1"><div class="m1"><p>در خبر دیدم که یحیی دائماً</p></div>
<div class="m2"><p>بود درخوف خدا او قائماً</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز و شب در گریه و زاری بد او</p></div>
<div class="m2"><p>دائماً در ساز هشیاری بد او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از میانخلق بیرون رفته بود</p></div>
<div class="m2"><p>بر سر که پارهٔ بنشسته بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دائماً در خوف بودی آن امام</p></div>
<div class="m2"><p>بر سر کوهش بدی دائم مقام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناگهی عیسی رسید آنجا ز راه</p></div>
<div class="m2"><p>دید یحیی را میان سوز و آه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آه می‌کرد و بزاری می‌گریست</p></div>
<div class="m2"><p>هر زمان از خوف حق چون مرده زیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت عیسی رحمت حق را ببین</p></div>
<div class="m2"><p>چند گرئی ای نبی راه بین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت یحیی که تو قهرش را نگر</p></div>
<div class="m2"><p>چند باشی ایمن ای صاحب نظر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عیسیش گفتا که رحمت سابق است</p></div>
<div class="m2"><p>حق تعالی گفت این خود واقف است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت یحیی گر بیاید جبرئیل</p></div>
<div class="m2"><p>این زمان گوید مرا از این دلیل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه رجادانم نه خوف از این نشان</p></div>
<div class="m2"><p>بگذر از خوف و نگردر بی‌نشان</p></div></div>