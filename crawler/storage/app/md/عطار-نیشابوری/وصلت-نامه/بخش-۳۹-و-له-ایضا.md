---
title: >-
    بخش ۳۹ - و له ایضاً
---
# بخش ۳۹ - و له ایضاً

<div class="b" id="bn1"><div class="m1"><p>در وصف تو عقل طبع دیوانه گرفت</p></div>
<div class="m2"><p>جان تن ز دو با عجز بهم خانه گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون شمع تجلی تو آمد به ظهور</p></div>
<div class="m2"><p>طاوس فلک مذهب پروانه گرفت</p></div></div>