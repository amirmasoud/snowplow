---
title: >-
    بخش ۷۱ - و له ایضاً
---
# بخش ۷۱ - و له ایضاً

<div class="b" id="bn1"><div class="m1"><p>هر سبزه و گل که از زمین بیرون رست</p></div>
<div class="m2"><p>از خاک یکی سبز خطی گلگون رست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر نرگس و لاله کز که و هامون رست</p></div>
<div class="m2"><p>از چشم و بتن وز جگر پرخون رست</p></div></div>