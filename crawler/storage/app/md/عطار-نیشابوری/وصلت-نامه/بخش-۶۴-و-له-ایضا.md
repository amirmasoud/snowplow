---
title: >-
    بخش ۶۴ - و له ایضاً
---
# بخش ۶۴ - و له ایضاً

<div class="b" id="bn1"><div class="m1"><p>تا چند زمرگ غمناک شوی</p></div>
<div class="m2"><p>آن به که ز اندیشهٔ خود پاک شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک قطرهٔ آب بودهٔ اول کار</p></div>
<div class="m2"><p>تا آخر کار یک کف خاک شوی</p></div></div>