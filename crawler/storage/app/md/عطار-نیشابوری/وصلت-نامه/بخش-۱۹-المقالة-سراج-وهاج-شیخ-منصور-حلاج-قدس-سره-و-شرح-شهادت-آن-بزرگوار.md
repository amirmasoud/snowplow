---
title: >-
    بخش ۱۹ - المقالة سراج وهاج شیخ منصور حلاج قدس سره و شرح شهادت آن بزرگوار
---
# بخش ۱۹ - المقالة سراج وهاج شیخ منصور حلاج قدس سره و شرح شهادت آن بزرگوار

<div class="b" id="bn1"><div class="m1"><p>بود منصور ای عجب شوریده حال</p></div>
<div class="m2"><p>در ره تحقیق او را صد کمال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حال او حال عجب بود ای پسر</p></div>
<div class="m2"><p>نی چو حال این خسان بی‌خبر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از رموز سر حق ره برده بود</p></div>
<div class="m2"><p>نی چو ما و تو رهی گم کرده بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از شراب وصل حق نوشیده بود</p></div>
<div class="m2"><p>دائماً از شوق حق جوشیده بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راه توحید حقیقی رفته بود</p></div>
<div class="m2"><p>لاجرم از جسم کلی مرده بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>او یقین خویش حاصل کرده بود</p></div>
<div class="m2"><p>در یقینش خویش واصل کرده بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راه در گنج معانی برده بود</p></div>
<div class="m2"><p>نه که همچون ما و تو در پرده بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاشق صادق بد آن بحر صفا</p></div>
<div class="m2"><p>عارف فارغ بد آن کان وفا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در علوم دین وقوفی داشت او</p></div>
<div class="m2"><p>هیچ علمی را فرو نگذاشت او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عالمان از علم اودرمانده بود</p></div>
<div class="m2"><p>عارفان از عشق او وامانده بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سالکان بودند شیران کریم</p></div>
<div class="m2"><p>جمله پیچیدند سر اندر گلیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عاشقان از عشق او حیران شدند</p></div>
<div class="m2"><p>هر دم از نوع دگر بریان شدند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صادقان از صدق او خون جگر</p></div>
<div class="m2"><p>سالها خوردند کس را نه خبر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زاهدان از زهد او رسوا شدند</p></div>
<div class="m2"><p>هم ز حال زهد او شیدا شدند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حال او حال عجب بود ای فقیر</p></div>
<div class="m2"><p>بد به معنی و به صورت بی‌نظیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بود پنجه سال او اسرار پوش</p></div>
<div class="m2"><p>ناگهان از وی برآمد صد خروش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفت انا الحق سرخود پیدا بکرد</p></div>
<div class="m2"><p>جمله بغدادش پر از غوغا بکرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اهل تقلید آن زمان برخواستند</p></div>
<div class="m2"><p>از برای خویش فتوا خواستند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سیصد وهفتاد کس تقلیدیان</p></div>
<div class="m2"><p>جمله بر کاغذ نوشتند آن زمان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وین زمان حلاج کافر گشته است</p></div>
<div class="m2"><p>از طریق دین ما برگشته است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا بگردد او از این کفر عیان</p></div>
<div class="m2"><p>ورنه خونش را بریزیم این زمان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جملهٔ بغداد پرغوغا شده</p></div>
<div class="m2"><p>او بگفت خویش در سودا شده</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بعد از آن نزد خلیفه آمدند</p></div>
<div class="m2"><p>کام خود را از خلیفه بستدند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وانموده حالت منصور را</p></div>
<div class="m2"><p>صاحب سر آن شه سیفور را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون خلیفه واقف اسرار شد</p></div>
<div class="m2"><p>در دل او صدهزاران خار شد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زانکه دایم او محب او بدی</p></div>
<div class="m2"><p>کام خود از گفتهٔ او بستدی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>صد کتاب از گفتهٔ اوخوانده بود</p></div>
<div class="m2"><p>سر مخفی زوبجان بخریده بود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خود از این سرش عوام قلبتان</p></div>
<div class="m2"><p>منع نتوانست کردن آن زمان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پس بفرمود او که در زندان برند</p></div>
<div class="m2"><p>بو که باز آید از این آن مستمند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>من همی دانم که او مرد خداست</p></div>
<div class="m2"><p>فارغ از کفر ونفاق و از هواست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بعد از آن منصور در زندان نشست</p></div>
<div class="m2"><p>بد در آن زندان قومی پای بست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چارصد تن بد در آن زندان ببند</p></div>
<div class="m2"><p>خود در آنجا رفت شیخ هوشمند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شب درآمد گفت ای زندانیان</p></div>
<div class="m2"><p>اندر این زندان چرائید این زمان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جمله واگفتند حال یکدگر</p></div>
<div class="m2"><p>کز چه افتادیم ما در این خطر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بعد از آن منصور گفت ای مردمان</p></div>
<div class="m2"><p>جمله را آزاد کردم این زمان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آن کسان گفتند ما در بند سخت</p></div>
<div class="m2"><p>کی توانیم رفت زینجا نیکبخت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شیخ آندم دست را افشاند زود</p></div>
<div class="m2"><p>جملگی را بندها افتاد زود</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بعد از آن گفتند درها بسته‌اند</p></div>
<div class="m2"><p>ما در اینجا خوار و زار و مستمند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چون رویم ای پیشوای سالکان</p></div>
<div class="m2"><p>زانکه در بسته است ماهم هالکان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>پس اشارت کرد آن مرد صفا</p></div>
<div class="m2"><p>رخنه‌ها شد اندر آن دیوارها</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چارصد رخنه بشد آندم پدید</p></div>
<div class="m2"><p>هر یکی از رخنهٔ بیرون دوید</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چونکه زندانبان بدید آن حال و کار</p></div>
<div class="m2"><p>پیشش آمد آنگهی بگریست زار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دست و پای شیخ را او بوسه داد</p></div>
<div class="m2"><p>وانگهی سر در کف پایش نهاد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گفت ای شیخ بزرگ خورده دان</p></div>
<div class="m2"><p>خیز و رو تو نیز همچون دیگران</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گفت من آگه شدم از سر کار</p></div>
<div class="m2"><p>من نخواهم رفت جز در پای دار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>تا که جمله سالکان آگه شوند</p></div>
<div class="m2"><p>از طریق عشق حق رهبر شوند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بعد از آنش گفت برخیز و برو</p></div>
<div class="m2"><p>تا که یک دم با خود آیم از گرو</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چونکه زندانبان برفت آن مرد دین</p></div>
<div class="m2"><p>در مناجات آمد آن مرد یقین</p></div></div>