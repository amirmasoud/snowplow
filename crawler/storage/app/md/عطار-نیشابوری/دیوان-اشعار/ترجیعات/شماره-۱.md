---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>فداک ابی و امی این تمشی</p></div>
<div class="m2"><p>براق آمد مگر بر عزم عرشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تورا چه عالم و چه عرش و چه فرش</p></div>
<div class="m2"><p>که صد عالم ورای عرش و فرشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کنون روحانیان عرش را بین</p></div>
<div class="m2"><p>چو سر بر خط نهاده انس و وحشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تویی سلطان مطلق در دوعالم</p></div>
<div class="m2"><p>که خط دادندت انس و جان به خوشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بس کامد به نزدیک تو جبریل</p></div>
<div class="m2"><p>شده چون دحیه الکلب قریشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو اندر عالم جان اوفتادی</p></div>
<div class="m2"><p>از آن بی سایه دایم می‌درفشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو دایم رحمة للعالمینی</p></div>
<div class="m2"><p>بران جرم دو عالم را ببخشی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگردد مطلع بر نقش تو کس</p></div>
<div class="m2"><p>که تو برتر ز نه طالق بنفشی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو تو برتر ز افلاکی به جز حق</p></div>
<div class="m2"><p>که داند تا چه نوری و چه نقشی</p></div></div>
<div class="b2" id="bn10"><p>فسبحان الذی اسری بعبده</p>
<p>الی الملکوت و الجبروت کله</p></div>
<div class="b" id="bn11"><div class="m1"><p>زهی از عرش اعلی بر گذشته</p></div>
<div class="m2"><p>وز آنجا عرش بالا بر گذشته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چه گویم من که از هر جا که گویم</p></div>
<div class="m2"><p>به صد عالم از آنجا بر گذشته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه روحانیان بر جای مانده</p></div>
<div class="m2"><p>تو در بی جایی از جا بر گذشته</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هم از عقل معظم پیش رفته</p></div>
<div class="m2"><p>هم از روح معلی بر گذشته</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>قیامت نقد امروزت که هاتین</p></div>
<div class="m2"><p>تو از دی و ز فردا بر گذشته</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به خاصیت تو دری عالم افروز</p></div>
<div class="m2"><p>ز قعر هفت دریا بر گذشته</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به یک دم چون گهر از طشت پر زر</p></div>
<div class="m2"><p>ازین نه طشت مینا بر گذشته</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به نور جان به ذات حق رسیده</p></div>
<div class="m2"><p>ز آلا و ز نعما بر گذشته</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شده مستغرق نور مسما</p></div>
<div class="m2"><p>ز اعداد و ز اسما بر گذشته</p></div></div>
<div class="b2" id="bn20"><p>زهی دانای اسرار معانی</p>
<p>ورای این جهان و آن جهانی</p></div>
<div class="b" id="bn21"><div class="m1"><p>زهی سلطان دارالملک افلاک</p></div>
<div class="m2"><p>زهی تخت تو عرش و تاج لولاک</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مجره زان پدید آمد که یک شب</p></div>
<div class="m2"><p>فلک از دست قدرت جامه زد چاک</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>قزح زان آشکارا شد که یک روز</p></div>
<div class="m2"><p>کشیدی از علی قوسی بر افلاک</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز اول حقه یک شب مهرهٔ ماه</p></div>
<div class="m2"><p>بدو بنموده‌ای دست تو زان پاک</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تو آن وقتی نبی‌الله بودی</p></div>
<div class="m2"><p>که آدم بود یک کف خاک نمناک</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اگر نور وجود تو نبودی</p></div>
<div class="m2"><p>بماندی در کف او آن کف خاک</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو پیش هو زنی هویی جگرسوز</p></div>
<div class="m2"><p>شود چون ناف آهو نافهٔ ناک</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>فرو ماند چو خر در گل ز مدحت</p></div>
<div class="m2"><p>دو اسبه گر بتازد عقل و ادراک</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ندارد هیچ کس با پشتی تو</p></div>
<div class="m2"><p>ز جرم جملهٔ روی زمین باک</p></div></div>
<div class="b2" id="bn30"><p>زهی دارای طول و عرض اکبر</p>
<p>شفاعت خواه مطلق روز محشر</p></div>
<div class="b" id="bn31"><div class="m1"><p>زهی روز قیامت روز بارت</p></div>
<div class="m2"><p>خلایق سر به سر در انتظارت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گنه کاران بر جان خورده زنهار</p></div>
<div class="m2"><p>همه جان بر کف اندر زینهارت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کجا پیغمبری دانی که آن روز</p></div>
<div class="m2"><p>نسوزاند سپند روزگارت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تویی مختار کل آفرینش</p></div>
<div class="m2"><p>که حق بی علتی کرد اختیارت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو تو بر باد دیدی ملک عالم</p></div>
<div class="m2"><p>به ملک فقر آمد افتخارت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به صورت چرخ از آن فوق تو افتاد</p></div>
<div class="m2"><p>که چرخ آمد طبق‌های نثارت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>فلک زان می‌دود با طشت خورشید</p></div>
<div class="m2"><p>که هست از دیرگاهی طشت دارت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به فراشی از آن می‌آیدت ابر</p></div>
<div class="m2"><p>که از خاکی تورا نبود غبارت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تورا چون حارس و چون حاجب آمد</p></div>
<div class="m2"><p>مه و خورشید در لیل و نهارت</p></div></div>
<div class="b2" id="bn40"><p>فلک با خواجگی خود غلامت</p>
<p>چو لام منحنی از دال نامت</p></div>
<div class="b" id="bn41"><div class="m1"><p>زهی خاک درت تریاک اعظم</p></div>
<div class="m2"><p>طفیلی وجودت کل عالم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زهی موسی عمران بر در تو</p></div>
<div class="m2"><p>به هارونی میان دربسته محکم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>زهی دربان تو یعنی که افلاک</p></div>
<div class="m2"><p>شده چوبک زنت عیسی مریم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تو را شیطان مسلمان گشته جاوید</p></div>
<div class="m2"><p>ولی پیچیده سر از پیش عالم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>اگر با نام حق نامت نگویند</p></div>
<div class="m2"><p>که را باشد مسلمانی مسلم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نیاید خسته‌ای کو منکرت شد</p></div>
<div class="m2"><p>بجز خاکستر خود هیچ مرهم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>عدو گر بنگرد در تو به انکار</p></div>
<div class="m2"><p>نماند مردمش در دیده محکم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نگین می‌خواست از مهر تو گردون</p></div>
<div class="m2"><p>از آن شد حلقه وش مانند خاتم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نگینش چون نشد مهر نبوت</p></div>
<div class="m2"><p>لبان خویش نیلی کرد ازین غم</p></div></div>
<div class="b2" id="bn50"><p>اگر در نطق آیم تا قیامت</p>
<p>نیارم گفت یک وصف تمامت</p></div>
<div class="b" id="bn51"><div class="m1"><p>زهی مه را رخت تنویر داده</p></div>
<div class="m2"><p>به یکسو روز را شبگیر داده</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>جمالت حسن را در بر گرفته</p></div>
<div class="m2"><p>کمالت عقل را تشویر داده</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>خرد نطق خوشت را کار بسته</p></div>
<div class="m2"><p>شکر لعل لبت را شیر داده</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>عروش هشت جنت در فراقت</p></div>
<div class="m2"><p>ازین نه بم نوای زیر داده</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چو خوشه ده زبان گشته نهم چرخ</p></div>
<div class="m2"><p>صفاتت صد یکی تقریر داده</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ازین طاق چهارم روی خورشید</p></div>
<div class="m2"><p>ز عکس رای تو تأثیر داده</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>قضا دیده قدر مایه ز قدرت</p></div>
<div class="m2"><p>ز کف سر رشتهٔ تقدیر داده</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به فرمان تو ای فرمان ده جان</p></div>
<div class="m2"><p>عذاب خلد را تأخیر داده</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>دل عطار مجنون غم تو</p></div>
<div class="m2"><p>تو از زلف خودش زنجیر داده</p></div></div>
<div class="b2" id="bn60"><p>به هم نامی حق دارم زهی قدر</p>
<p>به هم نامی نکو نامم کن ای صدر</p></div>
<div class="b" id="bn61"><div class="m1"><p>دلی کایینهٔ اسرار گردد</p></div>
<div class="m2"><p>غلام خواجهٔ احرار گردد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>تویی آن خواجه کز یک شاخ نعتت</p></div>
<div class="m2"><p>دو عالم خلق برخوردار گردد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>تویی آن مرد کز نور وجودت</p></div>
<div class="m2"><p>عدم آبستن اسرار گردد</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>تویی آن صدر کز دریای جودت</p></div>
<div class="m2"><p>کفی بحر و نمی امطار گردد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>دل من یا رسول الله خفته است</p></div>
<div class="m2"><p>دلی در بند تا بیدار گردد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چه کم گردد ز بحر بی نهایت</p></div>
<div class="m2"><p>که یک شبنم دری شهوار گردد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>دل عطار را گر بار دادی</p></div>
<div class="m2"><p>دلی بیدار معنی‌دار گردد</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>نکوکارا مگر کاری شود پیش</p></div>
<div class="m2"><p>چو کاری رفت مرد کار گردد</p></div></div>