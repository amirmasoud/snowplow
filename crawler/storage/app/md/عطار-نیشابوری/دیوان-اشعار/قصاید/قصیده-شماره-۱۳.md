---
title: >-
    قصیدهٔ شمارهٔ ۱۳
---
# قصیدهٔ شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>جانم ز سر کون به سودا در اوفتاد</p></div>
<div class="m2"><p>دل زو سبق ببرد و به غوغا در اوفتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بس که من به فکر ز پای آمدم به سر</p></div>
<div class="m2"><p>پایم زدست رفت و سر از پا در اوفتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون آب این حدیث ز بالای سرگذشت</p></div>
<div class="m2"><p>آتش همی به جان و دل ما دراوفتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون دل زهر کرده بدو هرچه گفته بود</p></div>
<div class="m2"><p>بادی به دست دید به سودا دراوفتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امروز گشت پیش دلم رستخیز نقد</p></div>
<div class="m2"><p>از بس که جان به فکرت فردا دراوفتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا رفته دید کار و ز دستش برفته کار</p></div>
<div class="m2"><p>از کار خویشتن به دریغا دراوفتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیک و بد و وجود و عدم جمله پاک برد</p></div>
<div class="m2"><p>جان را یگانه کرد که یکتا در اوفتاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فرخ کسی که در طلب و درد این حدیث</p></div>
<div class="m2"><p>بر خاره خار خورد و به صحرا دراوفتاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از ابلهیم غصه کند کز کمال جهل</p></div>
<div class="m2"><p>این جمله دید و خوش به تماشا دراوفتاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون مرگ در رسید مقامات خوف رفت</p></div>
<div class="m2"><p>وز بیم مرگ لرزه بر اعضا دراوفتاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یک حمله کرد ترک تحیر به ترک تاز</p></div>
<div class="m2"><p>پس دست برگشاد و به یغما دراوفتاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر خویشتن بلرز اگرچه ز بیم مرگ</p></div>
<div class="m2"><p>آتش به مغز صخره صما دراوفتاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تسلیم کن وجود و برو ترک خویش گیر</p></div>
<div class="m2"><p>کانکس هلاک شد که به هیجا دراوفتاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیچاره منکری که در آن موسم رضا</p></div>
<div class="m2"><p>از غایت سخط به علالا دراوفتاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بسیار قطره چون من و چون تو به یک زمان</p></div>
<div class="m2"><p>در بحر چه نهان و چه پیدا دراوفتاد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چه کم شد و چه بیش گر از تند باد مرگ</p></div>
<div class="m2"><p>یک شبنم ضعیف به دریا دراوفتاد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چندین مخور غم خود و انگار شیشه‌ای</p></div>
<div class="m2"><p>ناگه ز دست بر سر خارا دراوفتاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>این خود چه آتش است که از باطن جهان</p></div>
<div class="m2"><p>ظاهر شد و به پیر و به برنا دراوفتاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در زیر چرخ باد هوا دید موج‌زن</p></div>
<div class="m2"><p>چونان که نور دیدهٔ بینا دراوفتاد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ترسید دل که بستهٔ این دامگه شود</p></div>
<div class="m2"><p>مردانه پیش صف شد و تنها دراوفتاد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون عقل رای زن شد و چون علم حیله‌گر</p></div>
<div class="m2"><p>بی عقل و علم آمد و شیدا دراوفتاد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>احباب ره نداشت بسی رنج راه دید</p></div>
<div class="m2"><p>القصه حمله کرد و به اعدا دراوفتاد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر هم درید پردهٔ اسما و خوش برفت</p></div>
<div class="m2"><p>اسما چو محو شد به مسما دراوفتاد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>توفیق حق نگر که چه مردانه جست ازانک</p></div>
<div class="m2"><p>زو مردتر بسی دل دانا دراوفتاد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون در جهان غیب فنا گشت در بقا</p></div>
<div class="m2"><p>برخاست لا ز پیش به الا دراوفتاد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اسرار ذره ذره بر او گشت آشکار</p></div>
<div class="m2"><p>بازش نظر به عالم اسما دراوفتاد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون سر ذره نامتناهی بدید او</p></div>
<div class="m2"><p>دایم درین طلب به تقاضا دراوفتاد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چندان که سر بیش طلب کرد بیش یافت</p></div>
<div class="m2"><p>آخر ز عجز خود به مدارا دراوفتاد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گاه از حجاب تن به ثری رفت تا قدم</p></div>
<div class="m2"><p>گه سوی وجه فوق ثریا دراوفتاد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>می‌گشت در میانهٔ وجه و قدم مدام</p></div>
<div class="m2"><p>گاهی به پست و گاه به بالا دراوفتاد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چون در قدم رسید همه شوق وجه داشت</p></div>
<div class="m2"><p>چون وجه داشت زان به تمنا دراوفتاد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نی در قدم قرار و نه در وجه هم قرار</p></div>
<div class="m2"><p>نی هر دو، هر دو چیست به عمدا دراوفتاد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پنجه هزار سال سفر کن علی‌الدوام</p></div>
<div class="m2"><p>وین صید را ببین که چه زیبا دراوفتاد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>طوطی که کرد از قفس آهنین حذر</p></div>
<div class="m2"><p>تا چشم زد همی به همانجا دراوفتاد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از پیش کار پرده برافکن که زهر به</p></div>
<div class="m2"><p>زان یک شکر که طوطی گویا دراوفتاد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ما را ز بهر یک شکر از ما جدا کنند</p></div>
<div class="m2"><p>طوطی به پای دام بلازا دراوفتاد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چیزی نیافت یک دم و از دست رفت دل</p></div>
<div class="m2"><p>جان نیز نیست گشت و به سودا دراوفتاد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یوسف چو پاره پاره برون آمد از نقاب</p></div>
<div class="m2"><p>دیدی که سخت سخت زلیخا دراوفتاد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ای بس که چرخ در پی این راز شد نگون</p></div>
<div class="m2"><p>گاهی به زیر و گاه به بالا دراوفتاد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چون راه شوق عشق به پای خرد نبود</p></div>
<div class="m2"><p>از دست رفت عقلم و از جا دراوفتاد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بر اوج لامکان سفری خوش گزیده بود</p></div>
<div class="m2"><p>اینجا پدید نیست همانا دراوفتاد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>یارب درین طلب دل عطار خون گرفت</p></div>
<div class="m2"><p>زان خون شفق به گنبد خضرا دراوفتاد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>در من نگر که خاک سگ کوی تو منم</p></div>
<div class="m2"><p>وین سگ به کوی تو به تولا دراوفتاد</p></div></div>