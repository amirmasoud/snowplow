---
title: >-
    قصیدهٔ شمارهٔ ۶
---
# قصیدهٔ شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>ندارد درد من درمان دریغا</p></div>
<div class="m2"><p>بماندم بی سر و سامان دریغا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین حیرت فلک ها نیز دیر است</p></div>
<div class="m2"><p>که می‌گردند سرگردان دریغا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درین دشواری ره جان من شد</p></div>
<div class="m2"><p>که راهی نیست بس آسان دریغا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرو ماندم درین راه خطرناک</p></div>
<div class="m2"><p>چنین واله چنین حیران دریغا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رهی بس دور می‌بینم من این راه</p></div>
<div class="m2"><p>نه سر پیدا و نه پایان دریغا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز رنج تشنگی مردم به زاری</p></div>
<div class="m2"><p>جهان پر چشمهٔ حیوان دریغا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو نه جانان بخواهد ماند نه جان</p></div>
<div class="m2"><p>ز جان دردا و از جانان دریغا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر سنگی نه ای بنیوش آخر</p></div>
<div class="m2"><p>ز یک‌یک سنگ گورستان دریغا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عزیزان جهان را بین به یک راه</p></div>
<div class="m2"><p>همه با خاک ره یکسان دریغا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ببین تا بر سر خاک عزیزان</p></div>
<div class="m2"><p>چگونه ابر شد گریان دریغا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مگر جان‌های ایشان ابر بوده است</p></div>
<div class="m2"><p>که می‌بارند چون باران دریغا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیا تا در وفای دوستداران</p></div>
<div class="m2"><p>فرو باریم صد طوفان دریغا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه یاران به زیر خاک رفتند</p></div>
<div class="m2"><p>تو خواهی رفت چون ایشان دریغا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رخی کامد ز پیدایی چو خورشید</p></div>
<div class="m2"><p>کنون در خاک شد پنهان دریغا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از آن لب‌های چون عناب دردا</p></div>
<div class="m2"><p>وزان خط های چون ریحان دریغا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به یک تیغ اجل درج دهان را</p></div>
<div class="m2"><p>نه پسته ماند و نه مرجان دریغا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بتان ماه‌روی خوش‌سخن را</p></div>
<div class="m2"><p>کجا شد آن لب و دندان دریغا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زنخدان‌ها چو بر خواهند بستن</p></div>
<div class="m2"><p>زنخدان را ز نخ می‌دان دریغا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بسا شخصا که از تب ریخت در خاک</p></div>
<div class="m2"><p>شد از تبریز با کرمان دریغا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بسا ایوان که بر کیوانش بردند</p></div>
<div class="m2"><p>کجا شد آنهمه ایوان دریغا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بسا قصرا که چون فردوس کردند</p></div>
<div class="m2"><p>کنون شد کلبهٔ احزان دریغا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>درین غم‌خانه هر یوسف که دیدی</p></div>
<div class="m2"><p>لحد بر جمله شد زندان دریغا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو یکسان است آنجا ترک و تاجیک</p></div>
<div class="m2"><p>هم از ایران هم از توران دریغا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو خواه از روم باش و خواه از چین</p></div>
<div class="m2"><p>نه قیصر ماند و نه خاقان دریغا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز افریدون و از جمشید دردا</p></div>
<div class="m2"><p>ز کیخسرو ز نوشروان دریغا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هزاران گونه دستان داشت بلبل</p></div>
<div class="m2"><p>نبودش سود یک دستان دریغا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پس از وصلی که همچون باد بگذشت</p></div>
<div class="m2"><p>درآمد این غم هجران دریغا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز مال و ملک این عالم تمام است</p></div>
<div class="m2"><p>تو را یک لقمه چون لقمان دریغا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>برای نان چه ریزی آب رویت</p></div>
<div class="m2"><p>که آتش بهتر از این نان دریغا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو را تا جان بود نان کم نیاید</p></div>
<div class="m2"><p>چه باید کند چندین جان دریغا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خداوندا همه عمر عزیزم</p></div>
<div class="m2"><p>به جهل آورده‌ام به زیان دریغا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اگرچه بس سپیدم می‌شود موی</p></div>
<div class="m2"><p>سیه می‌گرددم دیوان دریغا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو دوران جوانی رفت چون باد</p></div>
<div class="m2"><p>بسی گفتم درین دوران دریغا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نشد معلوم من جز آخر عمر</p></div>
<div class="m2"><p>که کردم عمر خود تاوان دریغا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مرا گر عمر بایستی خریدن</p></div>
<div class="m2"><p>تلف کی کردمی زین‌سان دریغا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بسی عطار را درد و دریغ است</p></div>
<div class="m2"><p>که او را هست جای آن دریغا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خدایا چون گناهم کرد ناقص</p></div>
<div class="m2"><p>نهادم روی در نقصان دریغا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اگر کرد این گدا بر جهل کاری</p></div>
<div class="m2"><p>از آن غم کرد صدچندان دریغا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تو عفوش کن که گر عفوت نباشد</p></div>
<div class="m2"><p>فرو ماند به صد خذلان دریغا</p></div></div>