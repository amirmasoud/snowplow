---
title: >-
    قصیدهٔ شمارهٔ ۱۵
---
# قصیدهٔ شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>دم عیسی است که بوی گل تر می‌آرد</p></div>
<div class="m2"><p>وز بهشت است نسیمی که سحر می‌آرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا نه زان است نسیم سحر از سوی تبت</p></div>
<div class="m2"><p>کاهویی آه دل سوخته‌بر می‌آرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا صبا رفت و صف مشک ختن بر هم زد</p></div>
<div class="m2"><p>نافهٔ مشک مدد از گل تر می‌آرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا نه بادی است که از طرهٔ مشکین بتی</p></div>
<div class="m2"><p>به بر عاشق شوریده خبر می‌آرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا نه از گیسوی لیلی اثری یافت سحر</p></div>
<div class="m2"><p>که سوی مجنون زینگونه اثر می‌آرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا برآورد ز دل شیفته‌ای بادی سرد</p></div>
<div class="m2"><p>باد می‌آید و آن باد دگر می‌آرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا چو من سوخته‌ای را جگری سوخته‌اند</p></div>
<div class="m2"><p>باد از سینهٔ او بوی جگر می‌آرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یا کسی از مقر عز برون افتاده است</p></div>
<div class="m2"><p>به غریبی به سحر باد سحر می‌آرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یا مگر آه دل رستم دستان این دم</p></div>
<div class="m2"><p>نوش‌دارو به بر کشته پسر می‌آرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یا مگر باد به پیراهن یوسف بگذشت</p></div>
<div class="m2"><p>بوی پیراهن او سوی پدر می‌آرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یا نه داود زبور از سر دردی برخواند</p></div>
<div class="m2"><p>جبرئیل آن نفس پاک به پر می‌آرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یا مگر باد سحر آن دم طاها خواندن</p></div>
<div class="m2"><p>از سر واقعه‌ای سوی عمر می‌آرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یا مگر سیدسادات به امید وصال</p></div>
<div class="m2"><p>روی از مکه به هجرت به سفر می‌آرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یا نه روح‌القدس از خلد برین سوی رسول</p></div>
<div class="m2"><p>می‌خرامد خوش و قرآنش ز بر می‌آرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>این چه بادی است که طفلان چمن را هردم</p></div>
<div class="m2"><p>سرمه‌ای می‌کشد و شانه به سر می‌آرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نقش بند چمن از نافهٔ مشکین هر روز</p></div>
<div class="m2"><p>این جگرسوختگان بین که به در می‌آرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نو به نو دشت کنون زیب دگر می‌گیرد</p></div>
<div class="m2"><p>دم‌به‌دم باغ کنون گنج گهر می‌آرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نه که هر گنج که در زیر زمین بود دفین</p></div>
<div class="m2"><p>ابر خوش بار به یکبار ز بر می‌آرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کوه با لاله به هم بند کمر می‌بندد</p></div>
<div class="m2"><p>کبک از تیغ برون سر به کمر می‌آرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بلبل مست ز شاخ گل تر موسی وار</p></div>
<div class="m2"><p>ارنی گوی سوی غنچه حشر می‌آرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ابر گرینده به یک گریه گهر می‌ریزد</p></div>
<div class="m2"><p>غنچه بر شاخ ز بس خنده سپر می‌آرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سمن تازه که از لطف به بازی است گروه</p></div>
<div class="m2"><p>بر سر پای همی عمر به سر می‌آرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ارغوان هر سحری شبنم نوروزی را</p></div>
<div class="m2"><p>بهر تسکن صبا همچو شرر می‌آرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یاسمن دست‌زنان بر سر گل می‌نازد</p></div>
<div class="m2"><p>لاله دل از دل من سوخته‌تر می‌آرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نرگس سیمبر آن را که فروشد عمرش</p></div>
<div class="m2"><p>بر سر کاسهٔ سر خوانچهٔ زر می‌آرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سبزه از بهر زمین بوسی اسکندر عهد</p></div>
<div class="m2"><p>روی بر خاک سوی راه گذر می‌آرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خسرو روی زمین فخر وجود آنکه ز جود</p></div>
<div class="m2"><p>دستش از بحر کرم گوهر و زر می‌آرد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مهد خورشید که زنجیرهٔ زرین دارد</p></div>
<div class="m2"><p>هر مه از ماه نوش حلقهٔ در می‌آرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خسروا در دل خصم تو ز غصه شجری است</p></div>
<div class="m2"><p>که برش محنت و اشکوفه ضرر می‌آرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آفتابی تو و کوهی است عدو لیک ز برف</p></div>
<div class="m2"><p>بنگرش تا ز کجا تا چه قدر می‌آرد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دشمنت را که شب از شب بترش باد فلک</p></div>
<div class="m2"><p>روزش از روز همه عمر بتر می‌آرد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خسروا خاطر عطار ز دریای سخن</p></div>
<div class="m2"><p>نعت منثور تو در سلک درر می‌آرد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نیست در باب سخن در خور من یک هنری</p></div>
<div class="m2"><p>گو بیاید هلا هر که هنر می‌آرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>عیسی نظمم و هر نظم که آرد دگری</p></div>
<div class="m2"><p>در میان فضلا زحمت خر می‌آرد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ختم کردم سخن و هرکه پس از من گوید</p></div>
<div class="m2"><p>پیش دریای گهر آب شمر می‌آرد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تا که هشتم به ششم دور به هم می‌گردد</p></div>
<div class="m2"><p>تا نهم دور نه چون دور دگر می‌آرد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تو فروگیر به کام دل خود هشت بهشت</p></div>
<div class="m2"><p>که عدو رخت سوی هفت سقر می‌آرد</p></div></div>