---
title: >-
    قصیدهٔ شمارهٔ ۲۱
---
# قصیدهٔ شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>دلی پر گوهر اسرار دارم</p></div>
<div class="m2"><p>ولیکن بر زبان مسمار دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو یک همدم نمی‌دارم در آفاق</p></div>
<div class="m2"><p>سزد گر روی در دیوار دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو هیچ آزادهٔ داننده دل نیست</p></div>
<div class="m2"><p>چه سود ار جان پر از گفتار دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درین تنهایی و سرگشتگی من</p></div>
<div class="m2"><p>نه یک همدم نه یک دلدار دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا گویند کو عزلت گرفته است</p></div>
<div class="m2"><p>درین عزلت خدا را یار دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر کس می‌ندارم چون کنم من</p></div>
<div class="m2"><p>مگر من طبع بوتیمار دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرم ببریده باد از بن قلم‌وار</p></div>
<div class="m2"><p>اگر یک دم سر دستار دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا گویند او کس را ندارد</p></div>
<div class="m2"><p>اگر بینم کسی نهمار دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا از خلق ناهموار تا چند</p></div>
<div class="m2"><p>همی هموار و ناهموار دارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ندانم برد من تیمار یک کس</p></div>
<div class="m2"><p>چگونه این همه تیمار دارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز دنیایی مرا چیزی که نقد است</p></div>
<div class="m2"><p>جهانی زحمت اغیار دارم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو در عالم نمی‌بینم رفیقی</p></div>
<div class="m2"><p>میان خاره دل پر خار دارم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کجاست اندر جهان اسرارجویی</p></div>
<div class="m2"><p>که تا با او شبی بیدار دارم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر امید هم آوازی شب و روز</p></div>
<div class="m2"><p>طریق گنبد دوار دارم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چه جویم همدمی چون می‌نیابم</p></div>
<div class="m2"><p>که هم دم دم به دم اسرار دارم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به حمدالله رغما للمرائی</p></div>
<div class="m2"><p>تنی پاک و دلی هشیار دارم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>درون دل مرا گلزار عشق است</p></div>
<div class="m2"><p>که دایم سر درین گلزار دارم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برون نایم ازین گلزار هرگز</p></div>
<div class="m2"><p>چو خود را در درون غمخوار دارم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همه دنیا چو مردار است حقا</p></div>
<div class="m2"><p>نیم سگ چون سر مردار دارم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فریدم فرد بنشستم که در دل</p></div>
<div class="m2"><p>ز فردیت بسی انوار دارم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>درخت موسی از دورم نمودند</p></div>
<div class="m2"><p>سزد گر آه موسی‌وار دارم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اگر موسی نیم موسیچه هستم</p></div>
<div class="m2"><p>درون سینه موسیقار دارم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو موسیقار می‌نالم به زاری</p></div>
<div class="m2"><p>که کاری مشکل و دشوار دارم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز کار خویشتن تا چند گویم</p></div>
<div class="m2"><p>که باشم من کجا مقدار دارم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز هر چیزی که گفتم توبه کردم</p></div>
<div class="m2"><p>زبان اکنون بر استغفار دارم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>میان خلق از آن معنی عزیزم</p></div>
<div class="m2"><p>که نفس خویشتن را خوار دارم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خطا گفتم غلط کردم که در راه</p></div>
<div class="m2"><p>به نادانی خویش اقرار دارم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مگردانید سر از من به خواری</p></div>
<div class="m2"><p>که سرگردانی بسیار دارم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مرا سودای دلبندی چنان کرد</p></div>
<div class="m2"><p>که عمری رفت و عمری کار دارم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو از هستی او با خویش افتم</p></div>
<div class="m2"><p>ز ننگ هستی خود عار دارم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دلی در راه او در کفر و اسلام</p></div>
<div class="m2"><p>میان کعبه و خمار دارم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بوینیدم بسوزیدم در آتش</p></div>
<div class="m2"><p>که زیر خرقه در زنار دارم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ندارم ذره‌ای مقصود حاصل</p></div>
<div class="m2"><p>ولی اندیشه صد خروار دارم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>فغان از هستی عطار امروز</p></div>
<div class="m2"><p>من این غم جمله از عطار دارم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خداوندا تو می‌دانی که دیر است</p></div>
<div class="m2"><p>که از ایوان تو ادرار دارم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به فضل ادرار خود را تازه گردان</p></div>
<div class="m2"><p>که هم بی برگم و هم بار دارم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گر استعداد ادرار توام نیست</p></div>
<div class="m2"><p>به دست توست چون انکار دارم</p></div></div>