---
title: >-
    قصیدهٔ شمارهٔ ۷
---
# قصیدهٔ شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>وقت کوچ است الرحیل ای دل ازین جای خراب</p></div>
<div class="m2"><p>تا ز حضرت سوی جانت ارجعی آید خطاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بال و پر ده مرغ جان را تا میان این قفس</p></div>
<div class="m2"><p>بر دلت پیدا شود در یک نفس صد فتح باب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل را و نقل را همچون ترازو راست دار</p></div>
<div class="m2"><p>جهد کن تا در میان نه سیخ سوزد نه کباب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون ز عقل و نقل ذوق عشق حاصل شد تو را</p></div>
<div class="m2"><p>از دل پر عشق خود آتش زنی در جاه و آب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه عالم می‌نماید دیگران را آب خضر</p></div>
<div class="m2"><p>تو چنان گردی که گردد پیش تو همچون سراب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر چنان گردی جدا از خود که باید شد جدا</p></div>
<div class="m2"><p>ذره‌ای گردد به پیش نور جانت آفتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر صواب کار خواهی اندرین وادی صعب</p></div>
<div class="m2"><p>از خطای نفس خود تا چند بینی اضطراب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رو درین وادی چو اشتر باش و بگذر از خطا</p></div>
<div class="m2"><p>نرم می‌رو خار می‌خور بار می‌کش بر صواب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از هوای نفس شومت در حجابی مانده‌ای</p></div>
<div class="m2"><p>چون هوای نفس تو بنشست برخیزد حجاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در شراب و شاهد دنیا گرفتار آمدی</p></div>
<div class="m2"><p>ای دلت مست شراب نفس تا چند از شراب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خیز کاجزای جهان موقوف یک آه تواند</p></div>
<div class="m2"><p>از دل پر خون برآر آهی چو مستان خراب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر نفس سرمایهٔ عمر است و تو زان بی‌خبر</p></div>
<div class="m2"><p>خیز و روی از حسرت دل کن به خون دل خضاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>درد و حسرت بین که چندانی که فکرت می‌کنم</p></div>
<div class="m2"><p>هیچ کاری را نمی‌شایی تو اندر هیچ باب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون نیامد از تو کاری کان به کار آید تو را</p></div>
<div class="m2"><p>بر خود و کار خود بنشین و بگری چون سحاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو چنان دانی که هستی با بزرگان هم عنان</p></div>
<div class="m2"><p>باش تا زین جای فانی پای آری در رکاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>این زمان با توست حرصی و ندانی این نفس</p></div>
<div class="m2"><p>تا نیاری زیر خاک تیره رویت در نقاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون اجل در دامن عمرت زند ناگاه چنگ</p></div>
<div class="m2"><p>تو ز چنگ او بمانی دست بر سر چون ذباب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای دریغا می‌ندانی کز چه دور افتاده‌ای</p></div>
<div class="m2"><p>آخر ار شوقی است در تو ذوق این معنی بیاب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون چراغ عمر تو بی‌شک بخواهد مرد زود</p></div>
<div class="m2"><p>خویشتن را همچو شمعی زآتش شهوت متاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آخر ای شهوت‌پرست بی خبر گر عاقلی</p></div>
<div class="m2"><p>یک دمی لذت کجا ارزد به صد ساله عذاب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>توشهٔ این ره بساز آخر که مردان جهان</p></div>
<div class="m2"><p>در چنین راهی فرو ماندند چون خر در خلاب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>غرهٔ دنیا مباش و پشت بر عقبی مکن</p></div>
<div class="m2"><p>تا چو روی اندر لحد آری نمانی در عقاب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شب چو مردان زنده‌دار و تا توانی می‌مخسب</p></div>
<div class="m2"><p>زانکه زیر خاک بسیاریت خواهد بود خواب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بس که تو در خاک خواهی بود و زین طاق کبود</p></div>
<div class="m2"><p>بر سر خاک تو می‌تابد به زاری ماهتاب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون نمی‌دانی که روز واپسین حال تو چیست</p></div>
<div class="m2"><p>در غرور خود مکن بیهوده چندینی شتاب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کار روز واپسین دارد که روز واپسین</p></div>
<div class="m2"><p>از سیاست آب گردد زهره شیر از عتاب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تکیه بر طاعت مکن زیرا که در آخر نفس</p></div>
<div class="m2"><p>هیچکس را نیست آگاهی که چون آید زباب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چون به یک دم جمله چون شمعی فروخواهیم مرد</p></div>
<div class="m2"><p>پس چرا چون شمع باید دید چندین تف و تاب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون سر و افسر نخواهد ماند تا می‌بنگری</p></div>
<div class="m2"><p>چه کلاه ژنده و چه افسر افراسیاب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر همی‌بینی که روزی چند این مشتی گدا</p></div>
<div class="m2"><p>پادشا گشتند هان تا نبودت هیچ انقلاب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زانکه این مشتی دغل کار سیه دل تا نه دیر</p></div>
<div class="m2"><p>همچو بید پوده می‌ریزند در تحت التراب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زیر خاک از حد مشرق تا به مغرب خفته‌اند</p></div>
<div class="m2"><p>بنده و آزاد و شهری و غریب و شیخ و شاب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دل منه بر چشم و دندان بتان، کین خاک راه</p></div>
<div class="m2"><p>چشم، چون بادام و دندان است چون در خوشاب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آنکه از خشمش طناب خیمه مه می‌گسست</p></div>
<div class="m2"><p>در لحد اکنون کفن در گردن او شد طناب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>وانکه پیراهن زتاب خویشتن نگشاد باز</p></div>
<div class="m2"><p>تا کفن سازندش از وی باز کردندش ز تاب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>وانکه رویش همچو گل بشکفته بودی این زمان</p></div>
<div class="m2"><p>ابر می‌بارد به زاری بر سر خاکش گلاب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>وانکه زلفش همچو سنبل تاب در سر داشتی</p></div>
<div class="m2"><p>خاک تاریکش نه سر بگذاشت، نه سنبل، نه تاب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ما همه بی آگهیم آباد بر جان کسی</p></div>
<div class="m2"><p>کز سر با آگهی بگذشت ازین جای خراب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>یارب از فضل و کرم عطار را بیدار کن</p></div>
<div class="m2"><p>تا به بیداری شود در خواب تا یوم‌الحساب</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>توبه کردم یارب از چیزی که می‌بایست کرد</p></div>
<div class="m2"><p>روی لطف خویش را از تایب مسکین متاب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هر که این شوریده خاطر را دعا گوید به صدق</p></div>
<div class="m2"><p>یارب آن خورشید خاطر را دعا کن مستجاب</p></div></div>