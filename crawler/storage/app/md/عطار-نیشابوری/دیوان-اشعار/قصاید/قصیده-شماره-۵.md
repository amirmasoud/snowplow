---
title: >-
    قصیدهٔ شمارهٔ ۵
---
# قصیدهٔ شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>اگر ز گلبن خلقش گلی به بار رسد</p></div>
<div class="m2"><p>به حکم نیشکر آرد برون ز زهرگیا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خدایگانا امروز در سواد جهان</p></div>
<div class="m2"><p>به قطع تیغ تو را دیده‌ام ید بیضا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو اصل گوهر تیغت ز کوه می‌خیزد</p></div>
<div class="m2"><p>ازین جهت جهد آتش ز صخره صما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز سنگ لاله از آن می‌دمد که خونین شد</p></div>
<div class="m2"><p>ز بیم خار سر رمح تو دل خارا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برو در آمده زان است نیم ترک سپهر</p></div>
<div class="m2"><p>که تا کله بنهد پیش چار ترک تو را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تویی که در شب تاریک می‌کند روشن</p></div>
<div class="m2"><p>هزار چشم به روی تو این سپهر دو تا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فلک ز لؤلؤ لا لا از آن طبق پر کرد</p></div>
<div class="m2"><p>که تا نثار کند بر تو لؤلؤ لا لا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به جنب قدر تو ماه سپهر تحت افتاد</p></div>
<div class="m2"><p>ورای این چه توان گفت ماورای ورا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز فیض نقطهٔ نام تو همچو دریایی</p></div>
<div class="m2"><p>محیط گشت و چنین نامدار شد طغرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز کوه حلم تو یک ذره گر پدید آید</p></div>
<div class="m2"><p>هزار کوه به خود درکشد چو کاه‌ربا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز موج بحر کف تو چو نشو یافت نمی</p></div>
<div class="m2"><p>نبات سدره و طوبی گرفت نشو و نما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو بحر دست تو در جود گوهر افشان شد</p></div>
<div class="m2"><p>فروچکید ز هر قطره‌ای دو صد دریا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز فرق تا به قدم ابر اشک گشت از رشک</p></div>
<div class="m2"><p>ز زیر تا به زبر بحر آب شد ز حیا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به رشح جام تو دریای خشک لب تشنه است</p></div>
<div class="m2"><p>عجایبی است ز دریای آب استسقا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز خجلت کف تو بحر کف چو بر سر زد</p></div>
<div class="m2"><p>گهی ز رعشه بلرزید و گه ز استرخا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو قلزمی است کف کافیت که هر روزی</p></div>
<div class="m2"><p>چو شبنمی به همه کوه و بحر کرد هبا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به حق جود تو ای پادشاه گیتی بخش</p></div>
<div class="m2"><p>که حشو دشمنم آتش فکند در احشا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اگر مرا ز جناب چو تو سلیمانی</p></div>
<div class="m2"><p>فتاد غیبت هدهد که رفته بد به سبا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هزار حجت قاطع چو تیغ آرم پیش</p></div>
<div class="m2"><p>که جمله بر گهر صدق من بوند گوا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بدان خدای که در آفتاب معرفتش</p></div>
<div class="m2"><p>به ذره‌ای نرسد عقل جملهٔ عقلا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مقدسی که ز هر پاکیی که بتوان گفت</p></div>
<div class="m2"><p>منزه است از آن وصف و پاک و بی‌همتا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز شرح حکمت او کند مانده جان و خرد</p></div>
<div class="m2"><p>ز وصف غزت او کور گشته چون و چرا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جهان پیر چو شش روزه طفل گهواره است</p></div>
<div class="m2"><p>نگار کرد بزد هفت مهدش از میزا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به علم آنکه هزاران هزار راز شناخت</p></div>
<div class="m2"><p>ز سوز سینهٔ آن مور لیلةالظلما</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به سمع آنکه چو شد پشه در سر نمرود</p></div>
<div class="m2"><p>ز زخم راندن آن نیش می‌شنود آوا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به مبدعی که در ابداع او جهانی عقل</p></div>
<div class="m2"><p>به هر نفس ز سر عجز می‌شود شیدا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به قادری که به یکدم هزار نقش نگاشت</p></div>
<div class="m2"><p>ز اوج دایرهٔ چرخ و مرکز غبرا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به صانعی که به یک حله‌بافی صنعش</p></div>
<div class="m2"><p>هزار رنگ برآورد خاک چون دیبا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به یک خدای قدیم و به یک رسول کریم</p></div>
<div class="m2"><p>به یک حضور قیامت به یک شهود لقا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به دو سجود و دو حرف ظهور کن فیکون</p></div>
<div class="m2"><p>به دو عروج و دو معراج و دو جهان و دنا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به سه جواهر روح و به سه رطوبت چشم</p></div>
<div class="m2"><p>به سه طلاق به صدق و به سه طریق ملا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به چار پیک خدای و به چار یار رسول</p></div>
<div class="m2"><p>به چار جوی بهشت و به چار فصل بها</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به پنج فرض نماز و به پنج نزل کتاب</p></div>
<div class="m2"><p>به پنج نوبت شرع و به پنج رکن هدی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به شش سحرگه فطرت به شش جهات جهان</p></div>
<div class="m2"><p>به شش کرامت و شش روز و شش کریم عبا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به هفت اختر علو به هفت کشور سفل</p></div>
<div class="m2"><p>به هفت مفرش ارض و به هفت سقف سما</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به هشت جملهٔ عرش و به هشت خفتهٔ کهف</p></div>
<div class="m2"><p>به هشت معتدل و هشت جنةالماوا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به نه مه بچه و نه مه سراچهٔ مهد</p></div>
<div class="m2"><p>به نه مزاج و به نه طاق گلشن خضرا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به ده مبشره و ده مقولهٔ عالم</p></div>
<div class="m2"><p>به ده حس و به ده ایام ماه عاشورا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به جان آنکه نه عالم بدو نه آدم نیز</p></div>
<div class="m2"><p>که غرقه بود در انوار آیه الکبری</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بدان حضور که لااحصئی برآمد ازو</p></div>
<div class="m2"><p>که از هزار ثنا بیش بود آن یک لا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بدان شرف که ز اقبال بندگی شب قرب</p></div>
<div class="m2"><p>نسیم همنفسی یافت در حریم رضا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بدان نفس که ز خون شد محاسنش چو عقیق</p></div>
<div class="m2"><p>که سنگ گشت روان از مقابح سفها</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بدان نگار که از وی عکاشه برد سبق</p></div>
<div class="m2"><p>بدان نگارگری کان نگاشت چون دیبا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به قلب او که هزاران جناح روح‌القدس</p></div>
<div class="m2"><p>چو پر یک ملخ آمد در آن عریض فضا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به چشم او که نکرد التفات ما زاغ او</p></div>
<div class="m2"><p>به جان او که ز خود شد ز ماء مااوحی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به مجمعی که به صحرای حشر خواهد بود</p></div>
<div class="m2"><p>به جمع آدم و ذریتش به زیر لوا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به صدق صاحب غار و به عدل کسری شرع</p></div>
<div class="m2"><p>به حلم شاهد قرآن به علم شیر خدا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به دشنه خوردهٔ آن تشته به خون غرقه</p></div>
<div class="m2"><p>به نوش داروی در زهر کشتهٔ زهرا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به خون حمزه و عثمان و مرتضی و عمر</p></div>
<div class="m2"><p>به خون یحیی و سبطین و جملهٔ شهدا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به صد هزار نبی و به بیست و چار هزار</p></div>
<div class="m2"><p>بسی و اند هزار اهل صفه و اهل صفا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به داغ وجه بلال و دل چو بدر هلال</p></div>
<div class="m2"><p>به وجه زرد صهیب و به درد بودردا</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به آه سرد اویس قرن سوی یثرب</p></div>
<div class="m2"><p>به عشق گرم معاذ جبل سوی مبدا</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به شیر مردی خالد به حکم سیف‌الله</p></div>
<div class="m2"><p>به اهل بیتی سلمان و خلعت منا</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بدان چهل‌تن در ریگ رفته تشنه جگر</p></div>
<div class="m2"><p>لباس آن همه یک خرقه، قوت یک خرما</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به شبروان طواف و به ساکنان حرم</p></div>
<div class="m2"><p>به خفتگان بقیع و به کشتگان غزا</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به بو حنیفه که کرد آن حدیث و نص قیاس</p></div>
<div class="m2"><p>مثلثی که مربع نشست دین به نوا</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به شافعی که چو اخبار بی قیاسش بود</p></div>
<div class="m2"><p>سخن ز خواجهٔ دین بی قیاس کرد ادا</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به عین معرفت بایزید و خرقانی</p></div>
<div class="m2"><p>به شوق بی صفت بوسعید و ابن عطا</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بدان مقام که حلاج همچو پنبه بسوخت</p></div>
<div class="m2"><p>ز انالحقش همه حق ماند و محو گشت انا</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>به چل صباح که از نور خاص حق بسرشت</p></div>
<div class="m2"><p>خمیر این همه اعجوبه بی سواد مسا</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بدان دمی که چه گر پیر بود عالم طفل</p></div>
<div class="m2"><p>ازو بزاد زنی طفل پیر چون حوا</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>به دوست‌رویی پاکیزگان هفت رواق</p></div>
<div class="m2"><p>به شرمناکی دوشیزگان هفت‌سرا</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>به کار دیدگی آن که کم ز سی سال است</p></div>
<div class="m2"><p>که دور اوست و ز پیری همی رود به عصا</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>به قاضیئی که مر او را نیافت یک معلول</p></div>
<div class="m2"><p>ز حجتش که برو نور روی اوست گوا</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>به خونیی که بسی قلب بر جناح سفر</p></div>
<div class="m2"><p>به خون بگشته ز ضرب دو دست او به دعا</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>به تیغ میر علم کز دهان شیر سپهر</p></div>
<div class="m2"><p>به سرکشی سپر زرد می‌کند پیدا</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>به آب دست نگاری که رود نیل فلک</p></div>
<div class="m2"><p>ز بحر شعر ترش در سه پرده یافت نوا</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>به کلک و کاغذ سطان دین نظام دوم</p></div>
<div class="m2"><p>که در سه بعد محقق ازوست خط ذکا</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>به تاجری که چو سیماب داشت صرفه ندید</p></div>
<div class="m2"><p>متاع خود به منازل سپرد از سیما</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>به شب که از مه نو هندویی است زرین گوش</p></div>
<div class="m2"><p>به روز کز دم صبح است ترک مارافسا</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>به علم و حلم پریر و به حکم لازم دی</p></div>
<div class="m2"><p>به روزنامهٔ امروز و به هیبت فردا</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>به سابقان شریعت به راسخان علوم</p></div>
<div class="m2"><p>به پختگان طریقت به عادلان قضا</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>به صائمان نهار و به قایمان در لیل</p></div>
<div class="m2"><p>به ساجدان سحرگه به صابران غدا</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>به خاصگان کمال و به محرمان وصال</p></div>
<div class="m2"><p>به عاشقان جمال و به تشنگان فنا</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>به مخلصی که دهد جان به حق به تنهایی</p></div>
<div class="m2"><p>میان سجده ز سبحان ربی‌الاعلی</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>به عاشقی که بزد دست و جان فشان در رفت</p></div>
<div class="m2"><p>هنوز در ره او ناشنیده بانگ درا</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>به عارفی که به یک ضرب معرفت جانش</p></div>
<div class="m2"><p>بلا فرو شود آنگه برآید از الا</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>به عالمی که ز بیدار داشتن همه شب</p></div>
<div class="m2"><p>چو عقل کل بنخفتد میانهٔ اجزا</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>به صادقی که اگر در رهش بود گردی</p></div>
<div class="m2"><p>به هر سحر بنشاند ز چشم خون پالا</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>به قانعی که همه کون بوریا پنداشت</p></div>
<div class="m2"><p>که کس نیافت از آن بوریاش بوی ریا</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>به عاصیی که پس از توبه در شبی صد بار</p></div>
<div class="m2"><p>به نار و نور درافتد میان خوف و رجا</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>به قاف و طور و سراندیب و بوقبیس و احد</p></div>
<div class="m2"><p>به مروه و جبل‌الرحمه و منا و صفا</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>به آب زمزم و آب فرات و آب محیط</p></div>
<div class="m2"><p>به آب کوثر و آب حیات و آب رضا</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>به مجمع‌العرفات و به محشرالعرصات</p></div>
<div class="m2"><p>به منظرالدرجات و به مخرج‌المرعی</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>به عز عالم ارواح و عالم اجساد</p></div>
<div class="m2"><p>به فر عالم کبری و عالم صغری</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>به بیت معمور و بیت قدس و بیت حرام</p></div>
<div class="m2"><p>به بیت احزان و بیت قبر و بیت بقا</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>به خال طرفهٔ نون و به چشم شاهد صاد</p></div>
<div class="m2"><p>به زلف پر خم یاسین و طرهٔ طاها</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>به قاف والقرآن و به صاد والقران</p></div>
<div class="m2"><p>به علم القرآن و به علم الاسما</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>به روز عرفه و روز بدر و روز حنین</p></div>
<div class="m2"><p>به روز جمعه و عید و به روز حشر و جزا</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>به عزت شب قدر و شب حساب برات</p></div>
<div class="m2"><p>به حرمت شب آبستن و شب یلدا</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>به جانفزایی علم و به دل‌گشایی جان</p></div>
<div class="m2"><p>به پادشاهی عقل و رئیسی اعضا</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>به به‌نشینی عمر و به به‌حریفی بخت</p></div>
<div class="m2"><p>به پیر طبعی روح و به دولت برنا</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>به حاجبی دو ابرو به مردمی دو چشم</p></div>
<div class="m2"><p>به هم سری دو دست و به سرکشی دوپا</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>به عشق بلبل مست و غم کبوتر نوح</p></div>
<div class="m2"><p>به حدس هدهد بلقیس و عزت عنقا</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>به بار عام تو یعنی که غلغل ملکوت</p></div>
<div class="m2"><p>به رخش خاص تو یعنی که دلدل شهبا</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>به پای تخت تو یعنی که ساق عرش مجید</p></div>
<div class="m2"><p>به شیر فرش تو یعنی اسد برین بالا</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>به خاک پای تو کز رشح اوست آب حیات</p></div>
<div class="m2"><p>به یاد گرد تو کاتش فکند در اعدا</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>بدان بلارک خون ریز زهرپاش چو نیل</p></div>
<div class="m2"><p>که گوهری به قطع اوست خاصه در هیجا</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>به رمح مار مثالت که چون عصای کلیم</p></div>
<div class="m2"><p>فرو برد به دمی صد هزار اژدرها</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>به ناوکت که شب تیره است موی شکاف</p></div>
<div class="m2"><p>که روشن است مویی نمی‌برد ز سها</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>به فیض کف کریمت که بری و بحریش</p></div>
<div class="m2"><p>قبول کرد به صد بر و بحر در اعطا</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>به مجلس تو که جنات عدن را ماند</p></div>
<div class="m2"><p>یمینش از صف غلمان، یسارش از حورا</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>به ساقی تو که چون عزم ترکتاز کند</p></div>
<div class="m2"><p>هزار دل به سرغمزه آرد از یغما</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>به مطرب تو که از رشک زخم زخمهٔ او</p></div>
<div class="m2"><p>چو زخمه سر زده شد زهره از سر صفرا</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>به شعر من که اگر نقد نه فلک خوانیش</p></div>
<div class="m2"><p>ز هشت خلد برآید خروش صدقنا</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>بدین قصیده که گر تک زند کسی صد قرن</p></div>
<div class="m2"><p>نیابدش دومین در کراسهٔ شعرا</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>به سوز جان من از کید حاسد بد گوی</p></div>
<div class="m2"><p>به صور آه من از دست دشمن رعنا</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>که هرچه بر من افتاده افترا کردند</p></div>
<div class="m2"><p>چو افک عایشهٔ پاک دین خطاست خطا</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>خدای هست گواهم که نیست بر یادم</p></div>
<div class="m2"><p>که گفته‌ام سخن از تو برون ز مدح و ثنا</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>اگر تفحص این سر کنی دل خجلم</p></div>
<div class="m2"><p>که همچو دیدهٔ مور است می‌شود صحرا</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>ز هیبت تو اگرچه چو برگ می‌لرزم</p></div>
<div class="m2"><p>مکن ز خشم مرا پوستین درین سرما</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>وگر که من ز در کشتنم تو کش که خوش است</p></div>
<div class="m2"><p>ز دست یوسف صدیق دیدهٔ بینا</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>اگر مرا بکشی ملک را چه برخیزد</p></div>
<div class="m2"><p>ز خون من نه زخون چو من هزار گدا</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>وگر هزار عقوبت به جای من بکنی</p></div>
<div class="m2"><p>مرا سزاست بتر زان و از تو نیست سزا</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>چه گر تدارک این واقعه نمی‌دانم</p></div>
<div class="m2"><p>مرا بس این که بدین صدق هست حق دانا</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>چو شمع بر سرپایم کنون و حکم تو راست</p></div>
<div class="m2"><p>به راندن که برو یا به خواندن که بیا</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>وثیقتم همه بر عفو توست و چون نبود</p></div>
<div class="m2"><p>از آنکه حبل متین است و عروة وثقی</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>چو سایه از بر خویشم گر افکنی بر خاک</p></div>
<div class="m2"><p>چو سایه نیست مرا دور بودن از تو روا</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>وگر زنی من سرگشته را به چوگان زخم</p></div>
<div class="m2"><p>چو گوی می‌دومت در رکاب ناپروا</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>وگر چو کلک به تیغم سرافکنی از تن</p></div>
<div class="m2"><p>چو کلک بر خط حکمت به سر دوم حقا</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>به دور باش گرم پیش خود کنی بیجان</p></div>
<div class="m2"><p>چو دور باش به جان داری آیمت ز قفا</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>چو صبح خنده زنم از سر وفا هر روز</p></div>
<div class="m2"><p>وگر چو شمع کشی هر شبم به تیغ جفا</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>کز آستان تو صد شیر کی تواند کرد</p></div>
<div class="m2"><p>به سنگ چون سگ اصحاب کهف دور مرا</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>بدین قصیده توان کرد جرم ناکرده</p></div>
<div class="m2"><p>ز بندهٔ تو خود این کرده گیر بر عمدا</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>عطارد دوم آمد به مدح تو عطار</p></div>
<div class="m2"><p>عطاردی است برو ختم چون که بر تو عطا</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>اگرچه تا که مرا در تن است جان باقی</p></div>
<div class="m2"><p>دعای جان تو کار است در خلا و ملا</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>چو خلق روی زمینت همه دعا گویند</p></div>
<div class="m2"><p>به جز تو کیست که آمین کند مرا به دعا</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>ولی بس است که آمین همی کنند به جمع</p></div>
<div class="m2"><p>مقربان سماوی ز حضرت اعلی</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>مقدسا چو بدو ملک این جهان دادی</p></div>
<div class="m2"><p>در آن جهانش بده نیز ملکتی والا</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>به چار بالش ملکش در آن جهان بنشان</p></div>
<div class="m2"><p>که لایق است هم اینجا به ملک و هم آنجا</p></div></div>