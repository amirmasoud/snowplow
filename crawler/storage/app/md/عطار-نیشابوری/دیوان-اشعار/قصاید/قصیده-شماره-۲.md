---
title: >-
    قصیدهٔ شمارهٔ ۲
---
# قصیدهٔ شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>ای مرغ روح بر پر ازین دام پر بلا</p></div>
<div class="m2"><p>پرواز کن به ذروهٔ ایوان کبریا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر دل در دو کون فروبند از گمان</p></div>
<div class="m2"><p>گر چشم خویش بازگشایی از آن لقا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیمرغ وار از همگان عزلتی طلب</p></div>
<div class="m2"><p>کز هیچ کس ندید دمی هیچکس وفا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گنج وفا مجوی که در کنج روزگار</p></div>
<div class="m2"><p>گنجی نیافت هیچ کس از بیم اژدها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنگر که چند پند شنیدی ز یک به یک</p></div>
<div class="m2"><p>بنگر که با تو چند بگفتند انبیا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این جمله گفت و گوی نه زان بود تا تو خوش</p></div>
<div class="m2"><p>در ششدر غرور دغل بازی و دغا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آخر بقای عمر تو تا چند درکشد</p></div>
<div class="m2"><p>تو در محل نیستی و معرض فنا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای همچو مور خسته درین راه بیش جوی</p></div>
<div class="m2"><p>وی همچو گل ضعیف درین دور کم‌بقا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>افلاک در میان کشدت خوش‌خوش از کنار</p></div>
<div class="m2"><p>و ایام در کنار کند خوش خوشت سزا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر آنچه می‌کنی تو ز غفلت برای خویش</p></div>
<div class="m2"><p>با تو همان کند دگری کی دهی رضا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرکب ضعیف و بار گران و رهی دراز</p></div>
<div class="m2"><p>تو خوش بخفته کی رسی آخر به منتها</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو خفته‌ای ز دیرگه و عمر در گذر</p></div>
<div class="m2"><p>تو غافلی ز کار خود و مرگ در قفا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عمر تو در هوا بد و برباد رفته شد</p></div>
<div class="m2"><p>تو همچنین نشسته چنین کی بود روا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عمری که یک نفس اگرت آرزو کند</p></div>
<div class="m2"><p>نفروشدت کس ار بدهی صد گهر بها</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دربند خلق مانده‌ای و زهد از آن کنی</p></div>
<div class="m2"><p>تا گویدت کسی که فلانی است پارسا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>این زهد کی بود که تو را شرم باد ازین</p></div>
<div class="m2"><p>گویی تو را نه شرم بماندست و نه حیا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>باد غرور از سر تو کی برون شود</p></div>
<div class="m2"><p>تا ندروند از تو سر تو چو گندنا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از بس که چرخ بر سر تو آسیا براند</p></div>
<div class="m2"><p>مویت همه سپید شد از گرد آسیا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کافور گشت موی تو ساز سفر بکن</p></div>
<div class="m2"><p>کامد گه رحیل سوی عالم جزا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>منشین که عمر رفت و دریغا به دست ماند</p></div>
<div class="m2"><p>برخیز و رو که بانگ برآمد که الصلا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خو کرده‌اند جان و تن از دیرگه به هم</p></div>
<div class="m2"><p>خواهند شد هرآینه از یکدگر جدا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بگری چو ابر و زار گری و بسی گری</p></div>
<div class="m2"><p>در ماتم جدایی این هر دو آشنا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اول میان خون بده‌ای در رحم اسیر</p></div>
<div class="m2"><p>و آخر به خاک آمده‌ای عور و بی‌نوا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از خون رسیدی اول و آخر شدی به خاک</p></div>
<div class="m2"><p>بنگر که اولت ز کجا و آخرت کجا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خاک است و خون به گرد تو و در میانه تو</p></div>
<div class="m2"><p>گه باغ و حوض سازی و گه منظر و سرا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آگاه نیستی که ز چندین سرا و باغ</p></div>
<div class="m2"><p>لختی زمین است قسم تو دیگر همه هبا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر رای خویش جمله بیابی به کام خویش</p></div>
<div class="m2"><p>ور ملک کاینات مسلم شود تو را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در روز واپسین که سرانجام عمر توست</p></div>
<div class="m2"><p>از خشت باشدت کله و از کفن قبا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>رویی که ماه نو نگرفتی و نیم جو</p></div>
<div class="m2"><p>در زیر خاک زرد شود همچو کهربا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو طفل این جهانی و نادیده آن جهان</p></div>
<div class="m2"><p>گهوارهٔ تو گور و تو در رنج و در عنا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دو زنگی عظیم درآید به گور تو</p></div>
<div class="m2"><p>وز نیکی و بدیت بپرسند ماجرا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نه مادریت بر سر نه مشفقیت یار</p></div>
<div class="m2"><p>ای وای بر تو گر نرسد رحمت خدا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تو در میان خاک فرو مانده‌ای اسیر</p></div>
<div class="m2"><p>گویا زبان حال تو با حق که ربنا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آن شیشهٔ گلاب که بر خویش می‌زدی</p></div>
<div class="m2"><p>بر خاک تو زنند و بدارندت از عزا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تو چون گیاه خشک بریزی به زیر خاک</p></div>
<div class="m2"><p>تا بنگری ز خاک تو بیرون دمد گیا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تو زیر خاک و بی‌خبران را خبر نه زانک</p></div>
<div class="m2"><p>بر شخص تو چه می‌رود از خوف و از رجا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چون مدتی مدید برین حال بگذرد</p></div>
<div class="m2"><p>جای گذر شود سر خاکت به زیر پا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خاک تو خاک بیز به غربال می‌زند</p></div>
<div class="m2"><p>باد هوا همی برد آن خاک بر هوا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بسیار چون به بیزدت و باز جویدت</p></div>
<div class="m2"><p>نقدی نیابد از تو کند در دمت رها</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تو پایمال گشته و هر ذره خاک تو</p></div>
<div class="m2"><p>برداشته زبان که دریغا و حسرتا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>آن دم که طاق عمر تو از هم فرو فتد</p></div>
<div class="m2"><p>نه طمطراق ماند و نه تاج و نه لوا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بر آسمان مسای سر خود که تا نه دیر</p></div>
<div class="m2"><p>خواهی شدن به زیر زمین همچو توتیا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از شرق تا به غرب سراپای خفته‌اند</p></div>
<div class="m2"><p>خرد و بزرگ و پیر و جوان و شه و گدا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تو در هوای نفسی و آگاه نیستی</p></div>
<div class="m2"><p>کاجزای خفتگان است همه ذره در هوا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نه پیشوای وقت بماند نه پس روش</p></div>
<div class="m2"><p>نه پاسبان ملک بماند نه پادشا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بیچاره آدمی دل پر خون ز کار خویش</p></div>
<div class="m2"><p>که مبتلای آز و گه از حرص در بلا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>از دست حرص و آز بخستی به گوشه‌ای</p></div>
<div class="m2"><p>زین بیش دست می‌ندهد چون کنیم ما</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بیچاره آدمی که فرومانده‌ای است سخت</p></div>
<div class="m2"><p>در مات‌خانهٔ قدر و ششدر قضا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گاه از هوای کار جهان روی او چو زر</p></div>
<div class="m2"><p>گاه از بلای بار شکم پشت او دو تا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گه خوف آنکه پاره کند سینه را ز خشم</p></div>
<div class="m2"><p>گه بیم آنکه جامه بدرد ز تنگنا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گه مرده دل ز یک سخن طنز از کسی</p></div>
<div class="m2"><p>گه زنده دل به طال بقایی که مرحبا</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گه نیم‌جو نسنجد اگر خوانیش اسیر</p></div>
<div class="m2"><p>گه در جهان نگنجد اگر گوییش فتا</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گه بی‌خبر ز طفلی و آن در حساب نیست</p></div>
<div class="m2"><p>گه مست از جوانی و مستغرق هوا</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نه هیچ صدقه داده برای خدای خاص</p></div>
<div class="m2"><p>نه هیچ کار ساخته بی‌روی و بی‌ریا</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گر هیچ پای بر سر خاری نهد به سهو</p></div>
<div class="m2"><p>بر جایگه بداردش آن خار مبتلا</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>عمرش گرو به یک دم و او صد هزار کوه</p></div>
<div class="m2"><p>بر جان خود نهاده که این چون و این چرا</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بسیار جان بکنده و جان داده عاقبت</p></div>
<div class="m2"><p>من جملهٔ حدیث بگفتم به سر ملا</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>یارب به فضل در دل عطار کن نظر</p></div>
<div class="m2"><p>خط در کش آنچه کرد درین خطه از خطا</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>یارب هزار نور به جانش رسان به نقد</p></div>
<div class="m2"><p>آن را که گویدم به دل پاک یک دعا</p></div></div>