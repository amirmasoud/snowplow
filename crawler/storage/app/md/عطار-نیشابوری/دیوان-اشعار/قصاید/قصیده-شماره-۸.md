---
title: >-
    قصیدهٔ شمارهٔ ۸
---
# قصیدهٔ شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>بس کز جگرم خون دگرگونه چکیده است</p></div>
<div class="m2"><p>تا دست به کام دل خویشم برسیده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و امروز پشیمانی و درد است دلم را</p></div>
<div class="m2"><p>در عمر خود از هرچه بگفته است و شنیده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پایی که بسی پویه بی‌فایده کردی</p></div>
<div class="m2"><p>دیر است که در دامن اندوه کشیده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دستی که به هر دامن حاجب زدمی من</p></div>
<div class="m2"><p>از دست خود امروز همه جامه دریده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>و آن قد چو تیرم که سبک دل بد ازو سرو</p></div>
<div class="m2"><p>از بار گران همچو کمانی بخمیده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>و آن دیده که خون جگر از درد بسی ریخت</p></div>
<div class="m2"><p>زان کرد سیه جامه که همدرد ندیده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وان تن که نشستی به هوس بر سر هر صدر</p></div>
<div class="m2"><p>اکنون ز سر عاجزی از گوشه خزیده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وان دل که ز خوی خوش خود در همه پیوست</p></div>
<div class="m2"><p>امروز طمع از بد و از نیک بریده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وان جان که به انصاف به ارزد ز جهانی</p></div>
<div class="m2"><p>از ننگ من ناخلف از تن برمیده است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وان عقل که هشیارترین همه او بود</p></div>
<div class="m2"><p>از غایت حیرت سرانگشت گزیده است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هان ای دل گمراه چه خسبی که درین راه</p></div>
<div class="m2"><p>تو مانده‌ای و عمر تو از پیش دویده است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اندیشه کن از مرگ که شیران جهان را</p></div>
<div class="m2"><p>از هیبت شمشیر اجل زهره دریده است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چندین می نوشین چه چشی کانکه چشید او</p></div>
<div class="m2"><p>گر تو به حقیقت نگری زهر چشیده است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شهدی که ز سر نشتر زنبور بجسته است</p></div>
<div class="m2"><p>سرسام ز پی دارد اگر چند لذیذ است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عمر تو که یک لحظه به صد گنج به‌ارزد</p></div>
<div class="m2"><p>نفست همه بفروخته و عشق خریده است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دل از شرهٔ نفس تو در پای فتاده است</p></div>
<div class="m2"><p>هر چند درین واقعه مردانه چخیده است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هرکز نفسی پاک نیاید ز دلت بر</p></div>
<div class="m2"><p>تا جان تو فرمانبر این نفس پلید است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو خفته و همراه تو بس دور برفته است</p></div>
<div class="m2"><p>تو غافلی و صبح قیامت بدمیده است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نه بادیهٔ آز تو را هیچ کران است</p></div>
<div class="m2"><p>نه قفل غم حرص تو را هیچ کلید است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مویت همه شیر شد و از بچه طبعی</p></div>
<div class="m2"><p>گویی تو که امروز لبت شیر مکیده است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آخر تو چه مرغی که ز بس دانه که چینی</p></div>
<div class="m2"><p>از دام نجستی تو و عمرت بپریده است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یارب به کرم کن نظری در دل عطار</p></div>
<div class="m2"><p>کز دست دل خویش دل او بپزیده است</p></div></div>