---
title: >-
    قصیدهٔ شمارهٔ ۲۳
---
# قصیدهٔ شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>آتش تر می‌دمد از طبع چون آب ترم</p></div>
<div class="m2"><p>در معنی می‌چکد از لفظ معنی‌پرورم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر هفتم طبق در من یزید هشت خلد</p></div>
<div class="m2"><p>بیش می‌ارزد دو عالم پر گهر یک گوهرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دختران خاطرم بکرند چون مریم از آنک</p></div>
<div class="m2"><p>بکر می‌زایند از ایشان شعر همچون شکرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون برون آرم ز خاطر در معنی‌های بکر</p></div>
<div class="m2"><p>از درون طبع منکر ریب و شک بیرون برم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر ببازم با فلک نرد سخن از یک دو ضرب</p></div>
<div class="m2"><p>زان سخن در ششدرم افتد همی هفت اخترم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان دهان عقل همچون پسته از هم باز ماند</p></div>
<div class="m2"><p>کاب گرم اندر دهانش آمد از شعر ترم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه در باب سخن همتا ندارم در جهان</p></div>
<div class="m2"><p>زین جهان سیرم که در بند جهانی دیگرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کار آن دارد که کار این جهانش هیچ نیست</p></div>
<div class="m2"><p>یارب آنجاییم گردان تا از اینجا بگذرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کی تواند یافت جانم گوهر دریای دین</p></div>
<div class="m2"><p>تا بود این پنج حس و چار گوهر لنگرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نفس خودرایم به غفلت تا به جان در کار شد</p></div>
<div class="m2"><p>گر به جان با نفس کافر برنیایم کافرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر زمانم از رهی دیگر کشاند بوالعجب</p></div>
<div class="m2"><p>وای من گر نفس خواهد بود زین سان رهبرم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تن زنم تا همچنینم سوی دوزخ می‌برد</p></div>
<div class="m2"><p>آخر اندر قعر دوزخ دور گردد از برم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر میان دوزخ از من دور گردد نفس شوم</p></div>
<div class="m2"><p>در میان آتش دوزخ میان کوثرم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا که با نفسم فرود هفت دوزخ مانده‌ام</p></div>
<div class="m2"><p>چون نماند نفس شوم از هشت جنت برترم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نفس بر من چون جهان بفروخت دادم دین و دل</p></div>
<div class="m2"><p>تا خریدم شهوتی انصاف نیک ارزان خرم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پیکرم چون در دهان اژدهای چرخ زاد</p></div>
<div class="m2"><p>اژدها بچه است گویی در حقیقت پیکرم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>من چه سازم در میان این دو نره اژدها</p></div>
<div class="m2"><p>اژدها کرده است با این اژدها هم بسترم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>لاجرم چون جای من پیوسته کام اژدهاست</p></div>
<div class="m2"><p>زهر گردد گر می نوشین بود در ساغرم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون گل اندر غنچه‌ام هم تشنه‌دل هم بسته‌لب</p></div>
<div class="m2"><p>دل به خون می‌خندد آخر چند خون دل خورم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کی دهد با نار شهوت نور معنی خاطرم</p></div>
<div class="m2"><p>چون کند با ظلمت اجسام روح انورم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مانده‌ام در پرده‌های بوالعجب بر هیچ نه</p></div>
<div class="m2"><p>کی بود کین پرده‌های بوالعجب بر هم درم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در بیابانی که نه پا و نه سر دارد پدید</p></div>
<div class="m2"><p>هر زمان سرگشته‌تر هر ساعتی حیران‌ترم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مانده‌ام بی دانه و آبی اسیر این قفس</p></div>
<div class="m2"><p>مرغ جانم پر ندارد چون کنم چون بر پرم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مانده‌ام در چاه زندان پای در بند استوار</p></div>
<div class="m2"><p>پای در بند از چنین چاهی که آرد بر سرم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خلق عالم جمله مشغولند اندر کار خویش</p></div>
<div class="m2"><p>من ز بیکاران راهم گر بسی می‌بنگرم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر کسی خود را به پنداری غروری می‌دهد</p></div>
<div class="m2"><p>بو که خود را از میان جمله بیرون آورم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گرچه بسیاری رسن بازی فکرت کرده‌ام</p></div>
<div class="m2"><p>بیش ازین چیزی نمی‌دانم که سر در چنبرم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر بگویم آنچه از اندیشه بر جان من است</p></div>
<div class="m2"><p>یا چو من حیران بمانی یا نداری باورم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر بسی زیر و زبر آیم بنگشاید گره</p></div>
<div class="m2"><p>کی گشاید این گره تا من به دنیا اندرم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بیقراری می‌کنم اما چه سازم زانکه من</p></div>
<div class="m2"><p>در بن خاشاک دنیا بس عجایب گوهرم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خالقا عطار را یک قطره بخش از بحر قدس</p></div>
<div class="m2"><p>تا بود آن قطره در تنهایی جان یاورم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سر نپیچم از درت گر بند بندم بگسلی</p></div>
<div class="m2"><p>کز میان جان ز دیری باز خاک این درم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از عذاب من اگر کار تو خواهد گشت راست</p></div>
<div class="m2"><p>حکم حکم توست بنشان در میان اخگرم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بنده خاک توست و می‌دانم که دست اینت هست</p></div>
<div class="m2"><p>گر به باد لاابالی بر دهی خاکسترم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>لیکن از فضل تو آن زیبد که دستی بر نهی</p></div>
<div class="m2"><p>پس ازین پستی به علیین رسانی جوهرم</p></div></div>