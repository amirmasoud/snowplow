---
title: >-
    غزل شمارهٔ ۹۱
---
# غزل شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>عشق جمال جانان دریای آتشین است</p></div>
<div class="m2"><p>گر عاشقی بسوزی زیرا که راه این است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جایی که شمع رخشان ناگاه بر فروزند</p></div>
<div class="m2"><p>پروانه چون نسوزد کش سوختن یقین است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر سر عشق خواهی از کفر و دین گذر کن</p></div>
<div class="m2"><p>کانجا که عشق آمد چه جای کفر و دین است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق که در ره آید اندر مقام اول</p></div>
<div class="m2"><p>چون سایه‌ای به خواری افتاده در زمین است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون مدتی برآید سایه نماند اصلا</p></div>
<div class="m2"><p>کز دور جایگاهی خورشید در کمین است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چندین هزار رهرو دعوی عشق کردند</p></div>
<div class="m2"><p>برخاتم طریقت منصور چون نگین است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکس که در معنی زین بحر بازیابد</p></div>
<div class="m2"><p>در ملک هر دو عالم جاوید نازنین است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کاری قوی است عالی کاندر ره طریقت</p></div>
<div class="m2"><p>بر هر هزار سالی یک مرد راه‌بین است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو مرد ره چه دانی زیرا که مرد ره را</p></div>
<div class="m2"><p>اول قدم درین ره بر چرخ هفتمین است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عطار اندرین ره جایی فتاد کانجا</p></div>
<div class="m2"><p>برتر ز جسم و جان است بیرون ز مهر و کین است</p></div></div>