---
title: >-
    غزل شمارهٔ ۲۱۲
---
# غزل شمارهٔ ۲۱۲

<div class="b" id="bn1"><div class="m1"><p>خطت خورشید را در دامن آورد</p></div>
<div class="m2"><p>ز مشک ناب خرمن خرمن آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان خطت برآوردست دستی</p></div>
<div class="m2"><p>که با خورشید و مه در گردن آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کله‌دار فلک از عشق خطت</p></div>
<div class="m2"><p>چو گل کرده قبا پیراهن آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خط مشکینت جوشی در دل انداخت</p></div>
<div class="m2"><p>لب شیرینت جوشی در من آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فلک را عشق تو در گردش انداخت</p></div>
<div class="m2"><p>جهان را شوق تو در شیون آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندانم تا فلک در هیچ دوری</p></div>
<div class="m2"><p>به خوبی تو یک سیمین‌تن آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فلک چون هر شبی زلف تو می‌دید</p></div>
<div class="m2"><p>که چندین حلقهٔ مردافکن آورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز چشم بد بترسید از کواکب</p></div>
<div class="m2"><p>سر زلف تو را چوبک‌زن آورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از آن سر رشته گم کردم که رویت</p></div>
<div class="m2"><p>دهانی همچو چشم سوزن آورد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از آن سرگشته دل ماندم که لعلت</p></div>
<div class="m2"><p>گهر سی‌دانه در یک ارزن آورد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز بهر ذره‌ای وصل تو هر روز</p></div>
<div class="m2"><p>اگر خورشید وجهی روشن آورد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون آن ذره نیافت از خجلت آن</p></div>
<div class="m2"><p>فرو شد زرد و سر در دامن آورد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دل عطار در وصلت ضمیری</p></div>
<div class="m2"><p>به اسرار سخن آبستن آورد</p></div></div>