---
title: >-
    غزل شمارهٔ ۶۸۹
---
# غزل شمارهٔ ۶۸۹

<div class="b" id="bn1"><div class="m1"><p>آنچه با من می‌کند سودای تو</p></div>
<div class="m2"><p>می‌کشم چون نیست کس همتای تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با خیالی آمد از خجلت هلال</p></div>
<div class="m2"><p>پیش بدر عارض زیبای تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر گشاید کار هر دو کون را</p></div>
<div class="m2"><p>یک گره از زلف عنبرسای تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو ز خون پوشیده قوس قامتم</p></div>
<div class="m2"><p>از خدنگ نرگس رعنای تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیچ کارم نیست جز جان کاستن</p></div>
<div class="m2"><p>بر امید لعل جان‌افزای تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جای آن داری که صد صد را کشند</p></div>
<div class="m2"><p>لیک بر یک جان یک یک جای تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو چو شمعی وین جهان و آن جهان</p></div>
<div class="m2"><p>راست چون پروانه ناپروای تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کی رسم من بی سر و پا در تو زانک</p></div>
<div class="m2"><p>بی سر و پای است سر تا پای تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صد هزاران قرن باید خورد خون</p></div>
<div class="m2"><p>تا توانم کرد یکدم رای تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کی توانم پخت سودای تو من</p></div>
<div class="m2"><p>هست سودای تو بر بالای تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر شود هر ذره صد دوزخ مدام</p></div>
<div class="m2"><p>هم نگردد پخته یک سودای تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دم فرو بست از سخن اینجا فرید</p></div>
<div class="m2"><p>تا کند غواصی دریای تو</p></div></div>