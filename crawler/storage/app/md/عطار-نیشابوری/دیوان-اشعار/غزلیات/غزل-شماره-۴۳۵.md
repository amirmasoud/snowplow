---
title: >-
    غزل شمارهٔ ۴۳۵
---
# غزل شمارهٔ ۴۳۵

<div class="b" id="bn1"><div class="m1"><p>می‌شد سر زلف در زمین کش</p></div>
<div class="m2"><p>چون شرح دهم تو را که آن خوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تیزی و تازگی که او بود</p></div>
<div class="m2"><p>گویی همه آب بود و آتش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پر کرده ز چشم نرگسینش</p></div>
<div class="m2"><p>از تیر جفا هزار ترکش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زیر قدشم هزار مشتاق</p></div>
<div class="m2"><p>از مردم دیده کرده مفرش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان همه کاملان ز زلفش</p></div>
<div class="m2"><p>همچون سر زلف او مشوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی همه عاشقان ز عشقش</p></div>
<div class="m2"><p>از خون جگر شده منقش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گل چهره و گل فشان و گل بوی</p></div>
<div class="m2"><p>مه طلعت و مه جبین و مهوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صد تشنه ز خون دیده سیراب</p></div>
<div class="m2"><p>از دشنهٔ چشم آن پریوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گه دل گه جان خروش می‌کرد</p></div>
<div class="m2"><p>کای غالیه زلف زلف برکش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عطار ز زلف دلکش او</p></div>
<div class="m2"><p>تا حشر فتاده در کشاکش</p></div></div>