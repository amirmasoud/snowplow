---
title: >-
    غزل شمارهٔ ۸۱۸
---
# غزل شمارهٔ ۸۱۸

<div class="b" id="bn1"><div class="m1"><p>هر زمان لاف وفایی می زنی</p></div>
<div class="m2"><p>آتشی در مبتلایی می زنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون که جانی داری اندر مردگی</p></div>
<div class="m2"><p>لاف نیکویی ز جایی می زنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوالعجب مرغی که کس آگاه نیست</p></div>
<div class="m2"><p>تا تو پر بر چه هوایی می زنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ماهرویی و ازین رو ای پسر</p></div>
<div class="m2"><p>مهر و مه را پشت پایی می زنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفته‌ای کار تو را رایی زنم</p></div>
<div class="m2"><p>من بمردم تا تو رایی می زنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌زنم بر آتش عشق آب چشم</p></div>
<div class="m2"><p>تا چرا راه چو مایی می زنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بس‌که کردم آشنا در خون دل</p></div>
<div class="m2"><p>تا همه بر آشنایی می زنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زخمه بر ابریشم عطار زن</p></div>
<div class="m2"><p>گر به صد زاری نوایی می زنی</p></div></div>