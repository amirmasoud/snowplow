---
title: >-
    غزل شمارهٔ ۸۴۶
---
# غزل شمارهٔ ۸۴۶

<div class="b" id="bn1"><div class="m1"><p>سر برهنه کرده‌ام به سودایی</p></div>
<div class="m2"><p>برخاسته دل نه عقل و نه رایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با چشم پر آب پای در آتش</p></div>
<div class="m2"><p>بر خاک نشسته باد پیمایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون گوی بمانده در خم چوگان</p></div>
<div class="m2"><p>سرگشته شده سری و نه پایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از صحبت اختران صورت‌بین</p></div>
<div class="m2"><p>خورشید صفت بمانده تنهایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر روز ز تشنگی چو آتش</p></div>
<div class="m2"><p>بی واسطه در کشیده دریایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر سودایی که بیندم گوید</p></div>
<div class="m2"><p>زین شیوه ندیده‌ایم سودایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر بنشینم به نطق برخیزد</p></div>
<div class="m2"><p>از نکتهٔ من به شهر غوغایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون یکجایم نشسته نگذارند</p></div>
<div class="m2"><p>هر ساعت از آن دوم به هر جایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زین واقعه‌ای که کس نشان ندهد</p></div>
<div class="m2"><p>عطار نه عاقلی نه شیدایی</p></div></div>