---
title: >-
    غزل شمارهٔ ۶۸۴
---
# غزل شمارهٔ ۶۸۴

<div class="b" id="bn1"><div class="m1"><p>ای جلوه‌گر عالم، طاوس جمال تو</p></div>
<div class="m2"><p>سرسبزی و شب رنگی وصف خط و خال تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدری که فرو شد زو خورشید به تاریکی</p></div>
<div class="m2"><p>در دق و ورم مانده از رشک هلال تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد مرد چو رستم را چون بچهٔ یک روزه</p></div>
<div class="m2"><p>پرورده به زیر پر سیمرغ جمال تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان درفکند خود را خورشید به هر روزن</p></div>
<div class="m2"><p>تا بو که به دست آرد یک ذره وصال تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مه گرچه به روز و شب دواسبه همی تازد</p></div>
<div class="m2"><p>نرسد به رخ خوب خورشید مثال تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم ز خیال تو رنگی بودم یک شب</p></div>
<div class="m2"><p>خود هم تک برق آمد شبرنگ خیال تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتی که تو را از من صبر است اگر خواهی</p></div>
<div class="m2"><p>کشتن شودم واجب از گفت محال تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عطار به وصافی گرچه به کمال آمد</p></div>
<div class="m2"><p>شد گنگ زبان او در وصف کمال تو</p></div></div>