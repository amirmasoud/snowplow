---
title: >-
    غزل شمارهٔ ۵
---
# غزل شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>سوختی جانم چه می‌سازی مرا</p></div>
<div class="m2"><p>بر سر افتادم چه می‌تازی مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در رهت افتاده‌ام بر بوی آنک</p></div>
<div class="m2"><p>بوک بر گیری و بنوازی مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لیک می‌ترسم که هرگز تا ابد</p></div>
<div class="m2"><p>بر نخیزم گر بیندازی مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بندهٔ بیچاره گر می‌بایدت</p></div>
<div class="m2"><p>آمدم تا چاره‌ای سازی مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون شدم پروانهٔ شمع رخت</p></div>
<div class="m2"><p>همچو شمعی چند بگدازی مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرچه با جان نیست بازی درپذیر</p></div>
<div class="m2"><p>همچو پروانه به جانبازی مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو تمامی من نمی‌خواهم وجود</p></div>
<div class="m2"><p>وین نمی‌باید به انبازی مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سر چو شمعم بازبر یکبارگی</p></div>
<div class="m2"><p>تا کی از ننگ سرافرازی مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دوش وصلت نیم شب در خواب خوش</p></div>
<div class="m2"><p>کرد هم خلوت به دمسازی مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا که بر هم زد وصالت غمزه‌ای</p></div>
<div class="m2"><p>کرد صبح آغاز غمازی مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو ز تو آواز می‌ندهد فرید</p></div>
<div class="m2"><p>تا دهی قرب هم آوازی مرا</p></div></div>