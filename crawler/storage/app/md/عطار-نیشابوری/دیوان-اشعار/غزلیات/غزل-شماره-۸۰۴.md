---
title: >-
    غزل شمارهٔ ۸۰۴
---
# غزل شمارهٔ ۸۰۴

<div class="b" id="bn1"><div class="m1"><p>دردی است درین دلم نهانی</p></div>
<div class="m2"><p>کان درد مرا دوا تو دانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو مرهم درد بیدلانی</p></div>
<div class="m2"><p>دانم که مرا چنین نمانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من بندهٔ بی کس ضعیفم</p></div>
<div class="m2"><p>تو یار کسان بی کسانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر مورچه‌ای در تو کوبد</p></div>
<div class="m2"><p>آنی تو که ضایعش نمانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از من گنه آید و من اینم</p></div>
<div class="m2"><p>وز تو کرم آید و تو آنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یارب به در که باز گردم</p></div>
<div class="m2"><p>گر تو ز در خودم برانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از خواندن و راندنم چه باک است</p></div>
<div class="m2"><p>خواه این کن و خواه آن تو دانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گویم «ارنی» و زار گریم</p></div>
<div class="m2"><p>ترسم ز جواب «لن ترانی»</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیری بشنید و جان به حق داد</p></div>
<div class="m2"><p>عطار سخن مگو که جانی</p></div></div>