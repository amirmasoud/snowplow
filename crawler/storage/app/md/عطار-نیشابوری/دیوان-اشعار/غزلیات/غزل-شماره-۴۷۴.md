---
title: >-
    غزل شمارهٔ ۴۷۴
---
# غزل شمارهٔ ۴۷۴

<div class="b" id="bn1"><div class="m1"><p>ای برده به آب‌روی آبم</p></div>
<div class="m2"><p>وز نرگس نیم خواب خوابم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا روی چو ماه تو بدیدم</p></div>
<div class="m2"><p>افتاده چو ماهیی ز آبم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون شد خط سبز تو پدیدار</p></div>
<div class="m2"><p>بر زرده نشست آفتابم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرگه که به خون خطی نویسی</p></div>
<div class="m2"><p>من سر ز خط تو برنتابم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگه که حدیث وصل گویم</p></div>
<div class="m2"><p>دل خون گردد ز اضطرابم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از بی نمکی و بی قراری</p></div>
<div class="m2"><p>در سیخ جهد که من کبابم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وصلت نرسد به دل که از دل</p></div>
<div class="m2"><p>تا با جانم خبر نیابم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من خاک توام تو گنج حسنی</p></div>
<div class="m2"><p>بنمای رخ از دل خرابم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در پای فتاده‌ام چو زلفت</p></div>
<div class="m2"><p>زین بیش چو زلف خود متابم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عطار ز دست شد به یکبار</p></div>
<div class="m2"><p>وقت است که کم کنی عذابم</p></div></div>