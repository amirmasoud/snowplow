---
title: >-
    غزل شمارهٔ ۶۸۰
---
# غزل شمارهٔ ۶۸۰

<div class="b" id="bn1"><div class="m1"><p>هر زمان شوری دگر دارم ز تو</p></div>
<div class="m2"><p>هر نفس دل خسته‌تر دارم ز تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر بساط عشق تو هر دو جهان</p></div>
<div class="m2"><p>می ببازم تا خبر دارم ز تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک بر فرقم اگر جز خون دل</p></div>
<div class="m2"><p>هیچ آبی بر جگر دارم ز تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون ندارم هیچ آبی بر جگر</p></div>
<div class="m2"><p>پس چگونه چشم تر دارم ز تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه که چشم من تر است از خون دل</p></div>
<div class="m2"><p>زانکه دل خون تا به سر دارم ز تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این دل یکتای من شد تو به تو</p></div>
<div class="m2"><p>هر تویی عشق دگر دارم ز تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نی خطا گفتم که در دل توی نیست</p></div>
<div class="m2"><p>هم توی تویی اگر دارم ز تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفته بودی دل ز من بردار و رو</p></div>
<div class="m2"><p>دل چو خون شد من چه بردارم ز تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر شبی چون شمع بی‌صبح رخت</p></div>
<div class="m2"><p>سوز و تفی تا سحر دارم ز تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون برآید صبح همچون آفتاب</p></div>
<div class="m2"><p>زرد رویی در بدر دارم ز تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همچو چنگی هر رگی در پرده‌ای</p></div>
<div class="m2"><p>سوی دردی راه بر دارم ز تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همچو نی دل پر خروش و تن نزار</p></div>
<div class="m2"><p>جزو جزوم نوحه‌گر دارم ز تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ماه رویا کار من از دست شد</p></div>
<div class="m2"><p>تا کی آخر دست بردارم ز تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کوه غم برگیر از جانم از آنک</p></div>
<div class="m2"><p>دست با غم در کمر دارم ز تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خیز ای عطار و سر در عشق باز</p></div>
<div class="m2"><p>تا کی آخر دردسر دارم ز تو</p></div></div>