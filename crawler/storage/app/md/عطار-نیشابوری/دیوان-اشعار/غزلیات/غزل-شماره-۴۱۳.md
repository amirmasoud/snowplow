---
title: >-
    غزل شمارهٔ ۴۱۳
---
# غزل شمارهٔ ۴۱۳

<div class="b" id="bn1"><div class="m1"><p>عمر رفت و تو منی داری هنوز</p></div>
<div class="m2"><p>راه بر ناایمنی داری هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زخم کاید بر منی آید همه</p></div>
<div class="m2"><p>تا تو می‌رنجی منی داری هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد منی می‌زاید از تو هر نفس</p></div>
<div class="m2"><p>وی عجب آبستنی داری هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیر گشتی و بسی کردی سلوک</p></div>
<div class="m2"><p>طبع رند گلخنی داری هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همرهان رفتند و یاران گم شدند</p></div>
<div class="m2"><p>همچنان تو ساکنی داری هنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روز و شب در پرده با چندین ملک</p></div>
<div class="m2"><p>عادت اهریمنی داری هنوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روی گردانیده‌ای از تیرگی</p></div>
<div class="m2"><p>پشت سوی روشنی داری هنوز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلبرت در دوستی کی ره دهد</p></div>
<div class="m2"><p>چون دلی پر دشمنی داری هنوز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می‌زنی دم از پی معنی ولیک</p></div>
<div class="m2"><p>تو کجا آن چاشنی داری هنوز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در گریبان کش سر و بنشین خموش</p></div>
<div class="m2"><p>چون بسی تر دامنی داری هنوز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خویشتن را می‌کش و می‌کش بلا</p></div>
<div class="m2"><p>زانکه نفس کشتنی داری هنوز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رهبری چون آید از تو ای فرید</p></div>
<div class="m2"><p>چون تو عزم رهزنی داری هنوز</p></div></div>