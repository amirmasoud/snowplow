---
title: >-
    غزل شمارهٔ ۴۶۷
---
# غزل شمارهٔ ۴۶۷

<div class="b" id="bn1"><div class="m1"><p>از می عشق تو مست افتاده‌ام</p></div>
<div class="m2"><p>بر درت چون خاک پست افتاده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مستیم را نیست هشیاری پدید</p></div>
<div class="m2"><p>کز نخستین روز مست افتاده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خرابات خراب عاشقی</p></div>
<div class="m2"><p>عاشق و دردی‌پرست افتاده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>توبه من چون بود هرگز درست</p></div>
<div class="m2"><p>کز ملامت در شکست افتاده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیستی من ز هستی من است</p></div>
<div class="m2"><p>نیستم زیرا که هست افتاده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌تپم چون ماهیی دانی چرا</p></div>
<div class="m2"><p>زانکه از دریا به شست افتاده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی خودم کن ساقیا بگشای دست</p></div>
<div class="m2"><p>زانکه در خود پای بست افتاده‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دست دور از روی چون ماهت که من</p></div>
<div class="m2"><p>دورم از رویت ز دست افتاده‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این زمان عطار و یک نصفی شراب</p></div>
<div class="m2"><p>کز زمان در نصف شست افتاده‌ام</p></div></div>