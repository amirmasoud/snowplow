---
title: >-
    غزل شمارهٔ ۵۴۳
---
# غزل شمارهٔ ۵۴۳

<div class="b" id="bn1"><div class="m1"><p>دامن دل از تو در خون می‌کشم</p></div>
<div class="m2"><p>ننگری ای دوست تا چون می‌کشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از رگ جان هر شبی در هجر تو</p></div>
<div class="m2"><p>سوی چشم خونفشان خون می‌کشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه چون کاهی شدم از دست هجر</p></div>
<div class="m2"><p>بار غم از کوه افزون می‌کشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دور از روی تو هر دم بی تو من</p></div>
<div class="m2"><p>محنت و رنج دگرگون می‌کشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن همه خود هیچ بود و درگذشت</p></div>
<div class="m2"><p>درد و غم این است کاکنون می‌کشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من که عطارم یقین می‌باشدم</p></div>
<div class="m2"><p>کین بلا از دور گردون می‌کشم</p></div></div>