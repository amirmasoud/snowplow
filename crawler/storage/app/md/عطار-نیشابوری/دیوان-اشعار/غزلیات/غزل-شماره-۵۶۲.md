---
title: >-
    غزل شمارهٔ ۵۶۲
---
# غزل شمارهٔ ۵۶۲

<div class="b" id="bn1"><div class="m1"><p>ازین دریا که غرق اوست جانم</p></div>
<div class="m2"><p>برون جستم ولیکن در میانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسی رفتم درین دریا و گفتم</p></div>
<div class="m2"><p>گشاده شد به دریا دیدگانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون نیکو باز جستم سر دریا</p></div>
<div class="m2"><p>سر مویی ز دریا می ندانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی کو روی این دریا بدید است</p></div>
<div class="m2"><p>دهد خوش خوش نشانی هر زمانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ولیکن آنکه در دریاست غرقه</p></div>
<div class="m2"><p>ندانم تا دهد هرگز نشانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو چشمم نیست دریابین، چه مقصود</p></div>
<div class="m2"><p>اگر من غرق این دریا بمانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو نابینای مادرزاد، کشتی</p></div>
<div class="m2"><p>درین دریا همه بر خشک رانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو در دریا جنب می‌بایدم مرد</p></div>
<div class="m2"><p>چنین لب خشک و تر دامن از آنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کسی در آب حیوان تشنه میرد</p></div>
<div class="m2"><p>چه گویند آخر آن کس را من آنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دریغا کانچه می‌جستم ندیدم</p></div>
<div class="m2"><p>وزین غم پر دریغا ماند جانم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ندارم یکشبه حاصل ولیکن</p></div>
<div class="m2"><p>به انواع سخن گوهر فشانم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا از عالمی علم شکر به</p></div>
<div class="m2"><p>که باشد یک شکر اندر دهانم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دلم کلی ز علم انکار بگرفت</p></div>
<div class="m2"><p>کنون من در پی کار عیانم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر کاری عیان من نگردد</p></div>
<div class="m2"><p>چو مرداری شوم در خاکدانم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر عطار را فانی بیابم</p></div>
<div class="m2"><p>به بحر دولتش باقی رسانم</p></div></div>