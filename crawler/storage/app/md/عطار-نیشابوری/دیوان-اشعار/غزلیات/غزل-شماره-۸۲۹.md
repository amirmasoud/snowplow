---
title: >-
    غزل شمارهٔ ۸۲۹
---
# غزل شمارهٔ ۸۲۹

<div class="b" id="bn1"><div class="m1"><p>به وادییی که درو گوی راه سر بینی</p></div>
<div class="m2"><p>به هر دمی که زنی ماتمی دگر بینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز هرچه می‌دهدت روزگار عمر بهست</p></div>
<div class="m2"><p>ولی چه سود که آن نیز بر گذر بینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز دولتی به چه نازی که تا که چشم زنی</p></div>
<div class="m2"><p>اثر نبینی ازو در جهان اگر بینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مساز قبهٔ زرین که تیز شمشیر است</p></div>
<div class="m2"><p>سزای قبهٔ زرین که بر سپر بینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر سلوک کنی صد هزار قرن هنوز</p></div>
<div class="m2"><p>چو مرد رهگذری جمله رهگذر بینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو هر چه هست همه اصل خویش می‌جویند</p></div>
<div class="m2"><p>ز شوق جملهٔ ذرات در سفر بینی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو کل اصل جهان از یک اصل خاسته‌اند</p></div>
<div class="m2"><p>سزد که کل جهان را به یک نظر بینی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مکن ز نفس تکبر تو چشم باز گشای</p></div>
<div class="m2"><p>که تا همه شکم خاک سیم و زر بینی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به باد بر زبر خاک گنجه چند کنی</p></div>
<div class="m2"><p>که تا که رنجه شوی خاک بر زبر بینی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چگونه پای نهی در خزانه‌ای که درو</p></div>
<div class="m2"><p>به هر سویی که روی صد هزار سر بینی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه لحظه‌ای ز همه خفتگان خبر شنوی</p></div>
<div class="m2"><p>نه ذره‌ای ز همه رفتگان اثر بینی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز بس که خون جگر می‌فروخورد به زمین</p></div>
<div class="m2"><p>زمین ز خون جگر بسته چون جگر بینی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر جهان همه از پس کنی نمی‌دانم</p></div>
<div class="m2"><p>که در جهان ز دریغا چه بیشتر بینی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>درین مصیبت و سرگشتگی محال بود</p></div>
<div class="m2"><p>که در زمانه چو عطار نوحه‌گر بینی</p></div></div>