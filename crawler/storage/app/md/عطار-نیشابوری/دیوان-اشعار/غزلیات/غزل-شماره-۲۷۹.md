---
title: >-
    غزل شمارهٔ ۲۷۹
---
# غزل شمارهٔ ۲۷۹

<div class="b" id="bn1"><div class="m1"><p>عشق تو ز سقسین و ز بلغار برآمد</p></div>
<div class="m2"><p>فریاد ز کفار به یک بار برآمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در صومعه‌ها نیم شبان ذکر تو می‌رفت</p></div>
<div class="m2"><p>وز لات و عزی نعرهٔ اقرار برآمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم که کنم توبه در عشق ببندم</p></div>
<div class="m2"><p>تا چشم زدم عشق ز دیوار برآمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک لحظه نقاب از رخ زیبات براندند</p></div>
<div class="m2"><p>صد دلشده را زان رخ تو کار برآمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک زمزمه از عشق تو با چنگ بگفتم</p></div>
<div class="m2"><p>صد نالهٔ زار از دل هر تار برآمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آراسته حسن تو به بازار فروشد</p></div>
<div class="m2"><p>در حال هیاهوی ز بازار برآمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عیسی به مناجات به تسبیح خجل گشت</p></div>
<div class="m2"><p>ترسا ز چلیپا و ز زنار برآمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یوسف ز می وصل تو در چاه فروشد</p></div>
<div class="m2"><p>منصور ز شوقت به سر دار برآمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای جان جهان هر که درین ره قدمی زد</p></div>
<div class="m2"><p>کار دو جهانیش چو عطار برآمد</p></div></div>