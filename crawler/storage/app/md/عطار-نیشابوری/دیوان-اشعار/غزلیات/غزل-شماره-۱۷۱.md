---
title: >-
    غزل شمارهٔ ۱۷۱
---
# غزل شمارهٔ ۱۷۱

<div class="b" id="bn1"><div class="m1"><p>هر دل که ز خویشتن فنا گردد</p></div>
<div class="m2"><p>شایستهٔ قرب پادشا گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر گل که به رنگ دل نشد اینجا</p></div>
<div class="m2"><p>اندر گل خویش مبتلا گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امروز چو دل نشد جدا از گل</p></div>
<div class="m2"><p>فردا نه ز یکدگر جدا گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاک تن تو شود همه ذره</p></div>
<div class="m2"><p>هر ذره کبوتر هوا گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ور در گل خویشتن بماند دل</p></div>
<div class="m2"><p>از تنگی گور کی رها گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل آینه‌ای است پشت او تیره</p></div>
<div class="m2"><p>گر بزدایی بروی وا گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گل دل گردد چو پشت گردد رو</p></div>
<div class="m2"><p>ظلمت چو رود همه ضیا گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرگاه که پشت و روی یکسان شد</p></div>
<div class="m2"><p>آن آینه غرق کبریا گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ممکن نبود که هیچ مخلوقی</p></div>
<div class="m2"><p>گردید خدای یا خدا گردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اما سخن درست آن باشد</p></div>
<div class="m2"><p>کز ذات و صفات خود فنا گردد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرگه که فنا شود ازین هر دو</p></div>
<div class="m2"><p>در عین یگانگی بقا گردد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حضرت به زبان حال می‌گوید</p></div>
<div class="m2"><p>کس ما نشود ولی ز ما گردد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چیزی که شود چو بود کی باشد</p></div>
<div class="m2"><p>کی نادایم چو دایما گردد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر می‌خواهی که جان بیگانه</p></div>
<div class="m2"><p>با این همه کار آشنا گردد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در سایهٔ پیر شو که نابینا</p></div>
<div class="m2"><p>آن اولیتر که با عصا گردد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کاهی شو و کوه عجب بر هم زن</p></div>
<div class="m2"><p>تا پیر تو را چو کهربا گردد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ور این نکنی که گفت عطارت</p></div>
<div class="m2"><p>هر رنج که می‌بری هبا گردد</p></div></div>