---
title: >-
    غزل شمارهٔ ۸۵
---
# غزل شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>جهانی جان چو پروانه از آن است</p></div>
<div class="m2"><p>که آن ترسا بچه شمع جهان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به ترسایی درافتادم که پیوست</p></div>
<div class="m2"><p>مرا زنار زلفش بر میان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درآمد دوش آن ترسا بچه مست</p></div>
<div class="m2"><p>مرا گفتا که دین من عیان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درین دین گر بقا خواهی فنا شو</p></div>
<div class="m2"><p>که گر سودی کنی آنجا زیان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدو گفتم نشانی ده ازین راه</p></div>
<div class="m2"><p>مرا گفتا که این ره بی نشان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز پیدایی هویدا در هویداست</p></div>
<div class="m2"><p>ز پنهانی نهان اندر نهان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فنا اندر فنای است و عجب این</p></div>
<div class="m2"><p>که اندر وی بقای جاودان است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو پیدا و نهان دانستی این راه</p></div>
<div class="m2"><p>یقین می‌دان که نه این و نه آن است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به دین ما درآ گر مرد کفری</p></div>
<div class="m2"><p>که عاشق غیر این دین کفر دان است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یقین می‌دان که کفر عاشقی را</p></div>
<div class="m2"><p>بنا بر کافری جاودان است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر داری سر این پای در نه</p></div>
<div class="m2"><p>به ترک جان بگو چه جای جان است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وگرنه با سلامت رو که با تو</p></div>
<div class="m2"><p>سخن گفتن ز دلق و طیلسان است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برو عطار و تن زن زانکه این شرح</p></div>
<div class="m2"><p>نه کار توست کار رهبران است</p></div></div>