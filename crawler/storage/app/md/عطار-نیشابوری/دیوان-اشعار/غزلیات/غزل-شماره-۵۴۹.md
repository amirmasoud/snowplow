---
title: >-
    غزل شمارهٔ ۵۴۹
---
# غزل شمارهٔ ۵۴۹

<div class="b" id="bn1"><div class="m1"><p>هرگز دل پر خون را خرم نکنی دانم</p></div>
<div class="m2"><p>مجروح توام دانی مرهم نکنی دانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای شادی غمگینان چون تو به غمم شادی</p></div>
<div class="m2"><p>یکدم دل پر غم را بی غم نکنی دانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون دم دهیم دایم گر دم زنم و گرنه</p></div>
<div class="m2"><p>با خویشتنم یکدم همدم نکنی دانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر روز وفاداری من بیش کنم دانی</p></div>
<div class="m2"><p>مویی ز جفاکاری تو کم نکنی دانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون راز دل از اشکم پنهان به نمی‌ماند</p></div>
<div class="m2"><p>در پردهٔ یک رازم محرم نکنی دانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتی که اگر خواهی تا عهد کنم با تو</p></div>
<div class="m2"><p>گر عهد کنی با من، محکم نکنی دانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن روز که دل بردی گفتی ببرم جانت</p></div>
<div class="m2"><p>ای راحت جان و دل این هم نکنی دانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سهل است اگرم کشتی از جان بحلت کردم</p></div>
<div class="m2"><p>صعب است که بعد از من ماتم نکنی دانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با خیل گران‌جانان بنشسته‌ای و یکدم</p></div>
<div class="m2"><p>عطار سبک‌دل را خرم نکنی دانم</p></div></div>