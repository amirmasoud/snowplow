---
title: >-
    غزل شمارهٔ ۱۰
---
# غزل شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>در دلم بنشسته‌ای بیرون میا</p></div>
<div class="m2"><p>نی برون آی از دلم در خون میا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون ز دل بیرون نمی‌آیی دمی</p></div>
<div class="m2"><p>هر زمان در دیده دیگرگون میا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون کست یک ذره هرگز پی نبرد</p></div>
<div class="m2"><p>تو به یک یک ذره بوقلمون میا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غصه‌ای باشد که چون تو گوهری</p></div>
<div class="m2"><p>آید از دریا برون بیرون میا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرنگون غواص خود پیش آیدت</p></div>
<div class="m2"><p>تو ز فقر بحر در هامون میا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر پدید آیی دو عالم گم شود</p></div>
<div class="m2"><p>بیش از این ای لولو مکنون میا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نی برون آی و دو عالم محو کن</p></div>
<div class="m2"><p>گو برون از تو کسی اکنون، میا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون تو پیدا می‌شوی گم می‌شوم</p></div>
<div class="m2"><p>لطف کن وز وسع من افزون میا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون به یک مویت ندارم دست رس</p></div>
<div class="m2"><p>دست بر نه برتر از گردون میا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون ز هشیاری به جان آمد دلم</p></div>
<div class="m2"><p>بی‌شرابی پیش این مجنون میا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدرهٔ موزون شعرت ای فرید</p></div>
<div class="m2"><p>بستهٔ این بدرهٔ موزون میا</p></div></div>