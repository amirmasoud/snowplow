---
title: >-
    غزل شمارهٔ ۷۹۰
---
# غزل شمارهٔ ۷۹۰

<div class="b" id="bn1"><div class="m1"><p>جانا ز فراق تو این محنت جان تا کی</p></div>
<div class="m2"><p>دل در غم عشق تو رسوای جهان تا کی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون جان و دلم خون شد در درد فراق تو</p></div>
<div class="m2"><p>بر بوی وصال تو دل بر سر جان تا کی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نامد گه آن آخر کز پرده برون آیی</p></div>
<div class="m2"><p>آن روی بدان خوبی در پرده نهان تا کی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آرزوی رویت ای آرزوی جانم</p></div>
<div class="m2"><p>دل نوحه کنان تا چند، جان نعره‌زنان تا کی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بشکن به سر زلفت این بند گران از دل</p></div>
<div class="m2"><p>بر پای دل مسکین این بند گران تا کی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل بردن مشتاقان از غیرت خود تا چند</p></div>
<div class="m2"><p>خون خوردن و خاموشی زین دلشدگان تا کی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای پیر مناجاتی در میکده رو بنشین</p></div>
<div class="m2"><p>درباز دو عالم را این سود و زیان تا کی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اندر حرم معنی از کس نخرند دعوی</p></div>
<div class="m2"><p>پس خرقه بر آتش نه زین مدعیان تا کی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر طالب دلداری از کون و مکان بگذر</p></div>
<div class="m2"><p>هست او ز مکان برتر از کون و مکان تا کی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر عاشق دلداری ور سوختهٔ یاری</p></div>
<div class="m2"><p>بی نام و نشان می‌رو زین نام و نشان تا کی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتی به امید تو بارت بکشم از جان</p></div>
<div class="m2"><p>پس بارکش ار مردی این بانگ و فغان تا کی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عطار همی بیند کز بار غم عشقش</p></div>
<div class="m2"><p>عمر ابدی یابد عمر گذران تا کی</p></div></div>