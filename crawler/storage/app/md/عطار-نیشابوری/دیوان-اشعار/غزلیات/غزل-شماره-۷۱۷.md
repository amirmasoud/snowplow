---
title: >-
    غزل شمارهٔ ۷۱۷
---
# غزل شمارهٔ ۷۱۷

<div class="b" id="bn1"><div class="m1"><p>جانا منم ز مستی سر در جهان نهاده</p></div>
<div class="m2"><p>چون شمع آتش تو بر فرق جان نهاده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو همچو آفتابی تابنده از همه سو</p></div>
<div class="m2"><p>من همچو ذره پیشت جان در میان نهاده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من چون طلسم و افسون بیرون گنج مانده</p></div>
<div class="m2"><p>تو در میان جانم گنجی نهان نهاده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر یک گهر از آن گنج آید پدید بر من</p></div>
<div class="m2"><p>بینی مرا ز شادی سر در جهان نهاده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داغ غم تو دارم لیکن چگونه گویم</p></div>
<div class="m2"><p>مهری بدین عظیمی بر سر زبان نهاده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از روی همچو ماهت بر گیر آستینی</p></div>
<div class="m2"><p>سر چند دارم آخر بر آستان نهاده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عطار را چو عشقت نقد یقین عطا داد</p></div>
<div class="m2"><p>این ساعت است و جانی دل بر عیان نهاده</p></div></div>