---
title: >-
    غزل شمارهٔ ۷۹۲
---
# غزل شمارهٔ ۷۹۲

<div class="b" id="bn1"><div class="m1"><p>ماییم ز عالم معالی</p></div>
<div class="m2"><p>رندی دو سه اندرین حوالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عشق دلی و نیم جانی</p></div>
<div class="m2"><p>بر داده به باد لاابالی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگذشته ز هستی و گرفته</p></div>
<div class="m2"><p>چون صوفی ابن‌وقت حالی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در صفهٔ عاشقان حضرت</p></div>
<div class="m2"><p>از برهنگی فکنده قالی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس یافته برترین مقامی</p></div>
<div class="m2"><p>احسنت زهی مقام عالی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما را چه مرقع و چه اطلس</p></div>
<div class="m2"><p>چه نیک کنی چه بد سگالی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای زاهد کهنه درد نقد است</p></div>
<div class="m2"><p>برخیز که گوشه‌ای است خالی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا نالهٔ عاشقان نیوشی</p></div>
<div class="m2"><p>بر خلق ز زهد چند نالی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن می که تو می‌خوری حرام است</p></div>
<div class="m2"><p>ما می نخوریم جز حلالی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما بر سر آتشیم پیوست</p></div>
<div class="m2"><p>مستغرق بحر لایزالی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ما بی خوابیم و چون بود خواب</p></div>
<div class="m2"><p>در حضرت قرب ذوالجلالی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون خواب کند کسی که او را</p></div>
<div class="m2"><p>از ریگ روان بود نهالی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عطار برو که دست بردی</p></div>
<div class="m2"><p>از جملهٔ عالم معالی</p></div></div>