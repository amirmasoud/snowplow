---
title: >-
    غزل شمارهٔ ۶۹۷
---
# غزل شمارهٔ ۶۹۷

<div class="b" id="bn1"><div class="m1"><p>ای خم چرخ از خم ابروی تو</p></div>
<div class="m2"><p>آفتاب و ماه عکس روی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا به کوی عقل و جان کردی گذر</p></div>
<div class="m2"><p>معتکف شد عقل و جان در کوی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی دهد آن را که بویی داده‌ای</p></div>
<div class="m2"><p>هر دو عالم بوی یکتا موی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در میان جان و دل پنهان شدی</p></div>
<div class="m2"><p>تا نیاید هیچ‌کس ره سوی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون تویی جان و دلم را جان و دل</p></div>
<div class="m2"><p>من ز جان و دل شدم هندوی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق تو چندان که می‌سوزد دلم</p></div>
<div class="m2"><p>می نیاید از دلم جز بوی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پشت گردانید دایم از دو کون</p></div>
<div class="m2"><p>تا ابد عطار در پهلوی تو</p></div></div>