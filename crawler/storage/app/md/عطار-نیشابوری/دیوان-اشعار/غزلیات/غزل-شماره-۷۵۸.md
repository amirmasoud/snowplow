---
title: >-
    غزل شمارهٔ ۷۵۸
---
# غزل شمارهٔ ۷۵۸

<div class="b" id="bn1"><div class="m1"><p>ای آفتاب از ورق رویت آیتی</p></div>
<div class="m2"><p>در جنب جام لعل تو کوثر حکایتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز ندید هیچ کس از مصحف جمال</p></div>
<div class="m2"><p>سرسبزتر ز خط سیاه تو آیتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر نیت خطت که دلم جای وقف دید</p></div>
<div class="m2"><p>کرد از حروف زلف تو عالی روایتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از مشک خط خود جگرم سوختی ولیک</p></div>
<div class="m2"><p>دل ندهدم که در قلم آرم شکایتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آب حیات در ظلمات ظلالت است</p></div>
<div class="m2"><p>تا کی ز عکس لعل تو یابد هدایتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خورشید را که سلطنت سخت روشن است</p></div>
<div class="m2"><p>بنگر گرفت سایهٔ زلفت حمایتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر دم ز زلف تو شکنی دیگرم رسد</p></div>
<div class="m2"><p>زان پی نمی‌برم شکنش را نهایتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون زلف تو به تاب درم تا کیم رسد</p></div>
<div class="m2"><p>از زلف عنبر تو نسیم عنایتی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زلف توراست از در دربند تا ختن</p></div>
<div class="m2"><p>زان دل فرو گرفت زهی خوش ولایتی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عطار تا که بود، نبودش به هیچ روی</p></div>
<div class="m2"><p>جز دوستی روی تو هرگز جنایتی</p></div></div>