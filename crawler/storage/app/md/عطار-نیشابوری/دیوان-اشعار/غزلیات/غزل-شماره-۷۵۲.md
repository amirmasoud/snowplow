---
title: >-
    غزل شمارهٔ ۷۵۲
---
# غزل شمارهٔ ۷۵۲

<div class="b" id="bn1"><div class="m1"><p>از من بی خبر چه می‌طلبی</p></div>
<div class="m2"><p>سوختم خشک و تر چه می‌طلبی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه شهباز معرفت بودم</p></div>
<div class="m2"><p>ریختم بال و پر چه می‌طلبی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دو عالم ز هرچه بود و نبود</p></div>
<div class="m2"><p>بگسستم دگر چه می‌طلبی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مانده‌ام همچو گوی در ره تو</p></div>
<div class="m2"><p>گم شده پا و سر چه می‌طلبی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من آشفته را ز عشق رخت</p></div>
<div class="m2"><p>هر دم آشفته‌تر چه می‌طلبی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش طرف کلاه گوشهٔ تو</p></div>
<div class="m2"><p>کرده‌ام جان کمر چه می‌طلبی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفته‌ای درد تو همی طلبم</p></div>
<div class="m2"><p>درد ازین بیشتر چه می‌طلبی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با دلی پر ز درد تو شب و روز</p></div>
<div class="m2"><p>شده‌ام نوحه‌گر چه می‌طلبی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی خبر مانده‌ام ز مستی عشق</p></div>
<div class="m2"><p>هستت آخر خبر چه می‌طلبی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پرده برگیر و بیش ازین آخر</p></div>
<div class="m2"><p>پردهٔ من مدر چه می‌طلبی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چند باشم نه دل نه جان بی تو</p></div>
<div class="m2"><p>راندهٔ در بدر چه می‌طلبی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بی تو عطار را روا نبود</p></div>
<div class="m2"><p>خون گرفته جگر چه می‌طلبی</p></div></div>