---
title: >-
    غزل شمارهٔ ۷۵۱
---
# غزل شمارهٔ ۷۵۱

<div class="b" id="bn1"><div class="m1"><p>گر تو نسیمی ز زلف یار نیابی</p></div>
<div class="m2"><p>تا به ابد رد شوی و بار نیابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک دم اگر بوی زلف او به تو آید</p></div>
<div class="m2"><p>گنج حقیقت کم از هزار نیابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لیک اگر بنگری به حلقهٔ زلفش</p></div>
<div class="m2"><p>تا ابد آن حلقه را شمار نیابی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر دو جهان پرده‌ای است پیش رخ تو</p></div>
<div class="m2"><p>لیک درین پرده پود و تار نیابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حجله سرایی است پیش روی تو پرده</p></div>
<div class="m2"><p>پرده بدر گرچه پرده‌دار نیابی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرچه وجودی گرفت جمله غبار است</p></div>
<div class="m2"><p>ره به عدم بر تو تا غبار نیابی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یافتن یار چیست گم شدن تو</p></div>
<div class="m2"><p>تا نشوی گم ز خویش یار نیابی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غار غرور است در نهاد تو پنهان</p></div>
<div class="m2"><p>غور چنین غار آشکار نیابی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر نشوی آشنای او تو درین غار</p></div>
<div class="m2"><p>غرقه شوی بوی یار غار نیابی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر شودت ملک هر دو کون میسر</p></div>
<div class="m2"><p>بگذری از هر دو و قرار نیابی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ملک غمش بهتر است از دو جهان زانک</p></div>
<div class="m2"><p>جز غم او ملک پایدار نیابی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر غم او هست ذره‌ایت مخور غم</p></div>
<div class="m2"><p>زانکه ازین به تو غمگسار نیابی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هرچه که فرمود عشق رو تو به جان کن</p></div>
<div class="m2"><p>ورنه به جان هیچ زینهار نیابی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>می فکنی کار عشق جمله به فردا</p></div>
<div class="m2"><p>می به نترسی که روزگار نیابی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پای به ره در نه و ز کار مکش سر</p></div>
<div class="m2"><p>زانکه چو شد عمر وقت کار نیابی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بی‌ادب آنجا مرو وگرنه کشندت</p></div>
<div class="m2"><p>در همه عالم چو خواستگار نیابی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سر چه فرازی پیاده شو ز وجودت</p></div>
<div class="m2"><p>زانکه درین راه یک سوار نیابی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یک قدم این جایگاه بر نتوان داشت</p></div>
<div class="m2"><p>تا سر صد صد بزرگوار نیابی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو نتوانی که راه عشق کنی قطع</p></div>
<div class="m2"><p>کین ره جانسوز را کنار نیابی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چند روی ای فرید در پی آن گل</p></div>
<div class="m2"><p>خاصه تو زان سالکی که خار نیابی</p></div></div>