---
title: >-
    غزل شمارهٔ ۷۷۸
---
# غزل شمارهٔ ۷۷۸

<div class="b" id="bn1"><div class="m1"><p>درآمد دوش دلدارم به یاری</p></div>
<div class="m2"><p>مرا گفتا بگو تا در چه کاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حرامت باد اگر بی ما زمانی</p></div>
<div class="m2"><p>برآوردی دمی یا می برآری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو با ما می‌توانی بود هر شب</p></div>
<div class="m2"><p>روا نبود که بی ما شب گذاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو با ما غمگساری می‌توان کرد</p></div>
<div class="m2"><p>چرا با دیگری غم می گساری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوشی با دشمن ما در نشستی</p></div>
<div class="m2"><p>نباشد این دلیل دوستداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدان می‌داریم کز عزت خویش</p></div>
<div class="m2"><p>تو را در خاک اندازم به خواری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به تنهاییت بگذارم که تا تو</p></div>
<div class="m2"><p>بمانی تا ابد در بیقراری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو بشنیدم ز جانان این سخن‌ها</p></div>
<div class="m2"><p>بدو گفتم که دست از جمله داری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ولیکن چون تو یار غمگنانی</p></div>
<div class="m2"><p>مرا از ننگ من برهان به یاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که گر عطار در هستی بماند</p></div>
<div class="m2"><p>برو گریند عالمیان به زاری</p></div></div>