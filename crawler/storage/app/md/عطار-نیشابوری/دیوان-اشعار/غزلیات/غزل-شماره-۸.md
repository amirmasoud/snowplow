---
title: >-
    غزل شمارهٔ ۸
---
# غزل شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>چون شدستی ز من جدا صنما</p></div>
<div class="m2"><p>مُلْتَقَى لِمْ تَرَکْتَ بِیْ نَدَما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حق میان من و تو آگاه است</p></div>
<div class="m2"><p>هُوَ یَکْفی مِنَ الَّذی ظَلَما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور به دست تو آمده است اجلم</p></div>
<div class="m2"><p> قَدْ رَضیْتُ بِما جَرى قَلَما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گشت فانی ز خویش چون عطار</p></div>
<div class="m2"><p>گفت غیر از وجود حق عدما</p></div></div>