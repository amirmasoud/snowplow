---
title: >-
    غزل شمارهٔ ۱۴۸
---
# غزل شمارهٔ ۱۴۸

<div class="b" id="bn1"><div class="m1"><p>کشتی عمر ما کنار افتاد</p></div>
<div class="m2"><p>رخت در آب رفت و کار افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>موی همرنگ کفک دریا شد</p></div>
<div class="m2"><p>وز دهان در شاهوار افتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روز عمری که بیخ بر باد است</p></div>
<div class="m2"><p>با سر شاخ روزگار افتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر به ره در نهاد سیل اجل</p></div>
<div class="m2"><p>شورشی سخت در حصار افتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مستییی بود عهد برنایی</p></div>
<div class="m2"><p>این زمان کار با خمار افتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون به مقصد رسم که بر سر راه</p></div>
<div class="m2"><p>خر نگونسار گشت و بار افتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گل چگویم ز گلستان جهان</p></div>
<div class="m2"><p>که به یک گل هزار خار افتاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که در گلستان دنیا خفت</p></div>
<div class="m2"><p>پای او در دهان مار افتاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که یک دم شمرد در شادی</p></div>
<div class="m2"><p>در غم و رنج بی شمار افتاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی قراری چرا کنی چندین</p></div>
<div class="m2"><p>چه کنی چون چنین قرار افتاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه توان کرد اگر ز سکهٔ عمر</p></div>
<div class="m2"><p>نقد عمر تو کم عیار افتاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو مزن دم خموش باش خموش</p></div>
<div class="m2"><p>که نه این کار اختیار افتاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر نبودی امید، وای دلم</p></div>
<div class="m2"><p>لیک عطار امیدوار افتاد</p></div></div>