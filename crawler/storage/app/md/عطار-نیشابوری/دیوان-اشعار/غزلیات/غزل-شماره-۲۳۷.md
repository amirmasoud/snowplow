---
title: >-
    غزل شمارهٔ ۲۳۷
---
# غزل شمارهٔ ۲۳۷

<div class="b" id="bn1"><div class="m1"><p>بوی زلف یار آمد یارم اینک می‌رسد</p></div>
<div class="m2"><p>جان همی آساید و دلدارم اینک می‌رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اولین شب صبحدم با یارم اینک می‌دمد</p></div>
<div class="m2"><p>وآخرین اندیشه و تیمارم اینک می‌رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کنار جویباران قامت و رخسار او</p></div>
<div class="m2"><p>سرو سیمین آن گل بی خارم اینک می‌رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای بسا غم کو مرا خورد و غمم کس می نخورد</p></div>
<div class="m2"><p>چون نباشم شاد چون غمخوارم اینک می‌رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مدتی تا بودم اندر آرزوی یک نظر</p></div>
<div class="m2"><p>لاجرم چندین نظر در کارم اینک می‌رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دین و دنیا و دل و جان و جهان و مال و ملک</p></div>
<div class="m2"><p>آنچه هست از اندک و بسیارم اینک می‌رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روی تو ماه است و مه اندر سفر گردد مدام</p></div>
<div class="m2"><p>همچو ماه از مشرق ره یارم اینک می‌رسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بزم شادی از برای نقل سرمستان عشق</p></div>
<div class="m2"><p>پسته و عناب شکر بارم اینک می‌رسد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من به استقبال او جان بر کف از بهر نثار</p></div>
<div class="m2"><p>یار می‌گوید کنون عطارم اینک می‌رسد</p></div></div>