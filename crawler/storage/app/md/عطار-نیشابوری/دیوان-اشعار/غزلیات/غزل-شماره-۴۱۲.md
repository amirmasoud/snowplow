---
title: >-
    غزل شمارهٔ ۴۱۲
---
# غزل شمارهٔ ۴۱۲

<div class="b" id="bn1"><div class="m1"><p>جان ز مشک زلف دلم چون جگر مسوز</p></div>
<div class="m2"><p>با من بساز و جانم ازین بیشتر مسوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر روز تا به شب چو ز عشق تو سوختم</p></div>
<div class="m2"><p>هر شب چو شمع زار مرا تا سحر مسوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرغ توام به دست خودم دانه‌ای فرست</p></div>
<div class="m2"><p>زین بیش در هوای خودم بال و پر مسوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون آرزوی وصل توام خشک و تر بسوخت</p></div>
<div class="m2"><p>در آتش فراق، خودم خشک و تر مسوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون دل ببردی و جگر من بسوختی</p></div>
<div class="m2"><p>با دل بساز و بیش ازینم جگر مسوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکبارگی چو می‌بنسوزی مرا تمام</p></div>
<div class="m2"><p>هر روزم از فراق به نوعی دگر مسوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جانم که زآرزوی لبت همچو شمع سوخت</p></div>
<div class="m2"><p>چون عود بی‌مشاهدهٔ آن شکر مسوز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عطار را اگر نظری بر تو اوفتد</p></div>
<div class="m2"><p>این نیست ور بود نظرش در بصر مسوز</p></div></div>