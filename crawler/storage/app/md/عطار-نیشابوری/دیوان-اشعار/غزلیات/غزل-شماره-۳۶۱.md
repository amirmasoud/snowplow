---
title: >-
    غزل شمارهٔ ۳۶۱
---
# غزل شمارهٔ ۳۶۱

<div class="b" id="bn1"><div class="m1"><p>آن ماه برای کس نمی‌آید</p></div>
<div class="m2"><p>کو با غم خویش بس نمی‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آینه روی خویش می‌بیند</p></div>
<div class="m2"><p>در دام هوای کس نمی‌آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر تو به هوس جمال او خواهی</p></div>
<div class="m2"><p>او در طلب و هوس نمی‌آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جانا ره عشق چون تو معشوقی</p></div>
<div class="m2"><p>در زیر تک فرس نمی‌آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در وادی بی‌نهایت عشقش</p></div>
<div class="m2"><p>سیمرغ به یک مگس نمی‌آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرگز نشوی تو هم نفس کس را</p></div>
<div class="m2"><p>کانجا که تویی نفس نمی‌آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خورشید بلند را چه کم بیشی</p></div>
<div class="m2"><p>کش سایه ز پیش و پس نمی‌آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون در قعر است در وصل تو</p></div>
<div class="m2"><p>جز بر سر آب خس نمی‌آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در پای فراق تو شوم پامال</p></div>
<div class="m2"><p>چون وصل تو دسترس نمی‌آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عطار که چینهٔ تو می‌چیند</p></div>
<div class="m2"><p>مرغی است که در قفس نمی‌آید</p></div></div>