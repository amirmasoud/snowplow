---
title: >-
    غزل شمارهٔ ۱۲۹
---
# غزل شمارهٔ ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>تا گل از ابر آب حیوان یافت</p></div>
<div class="m2"><p>گرد خود صد هزار دستان یافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زره ابر گشت پیکان باز</p></div>
<div class="m2"><p>جوشن آب زخم پیکان یافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل خندان چو برفکند نقاب</p></div>
<div class="m2"><p>ابر را زار زار گریان یافت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون صبا چاک کرد دامن گل</p></div>
<div class="m2"><p>نافهٔ مشک در گریبان یافت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای نگاری که هر که دید رخت</p></div>
<div class="m2"><p>از رخ جانفزای تو جان یافت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دل و جان تو را که جان و دلی</p></div>
<div class="m2"><p>هر که فرمان ببرد فرمان یافت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می گلرنگ خور به موسم گل</p></div>
<div class="m2"><p>که گل تازه‌روی باران یافت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می‌خور و شاد زی که خوشتر ازین</p></div>
<div class="m2"><p>یک نفس در دو کون نتوان یافت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می به عطار ده به سرخی لعل</p></div>
<div class="m2"><p>که زمی جان چو در درخشان یافت</p></div></div>