---
title: >-
    غزل شمارهٔ ۸۰۹
---
# غزل شمارهٔ ۸۰۹

<div class="b" id="bn1"><div class="m1"><p>کجایی ای دل و جانم مگر که در دل و جانی</p></div>
<div class="m2"><p>که کس نمی‌دهد از تو به هیچ جای نشانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هیچ جای نشانی نداد هیچ کس از تو</p></div>
<div class="m2"><p>نشانی از تو کسی چون دهد که برتر از آنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عجب بمانده‌ام از ذات و از صفات تو دایم</p></div>
<div class="m2"><p>کز آفتاب هویداتری اگرچه نهانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه گوهری تو که در عرصهٔ دو کون نگنجی</p></div>
<div class="m2"><p>همه جهان ز تو پر گشت و تو برون ز جهانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منم که هستی من بند ره شدست درین ره</p></div>
<div class="m2"><p>تویی که از تویی خود مرا ز من برهانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من از خودی خود افتاده‌ام به چاه طبیعت</p></div>
<div class="m2"><p>مرا ز چاه به ماه ار بر آوری تو توانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آرزوی تو عمری به سر دویدم و اکنون</p></div>
<div class="m2"><p>چو در سر آمدم آخر مرا به سر چه دوانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه باشد ار ز سر لطف جان تشنه لبان را</p></div>
<div class="m2"><p>از آن شراب دل آشوب قطره‌ای بچشانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>امید ما همه آن است در ره تو که یک‌دم</p></div>
<div class="m2"><p>ز بوی خویش نسیمی به جان ما برسانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز اشتیاق تو عطار از دو کون فنا شد</p></div>
<div class="m2"><p>از آن او بود این و از آن خویش تو دانی</p></div></div>