---
title: >-
    غزل شمارهٔ ۴۸۸
---
# غزل شمارهٔ ۴۸۸

<div class="b" id="bn1"><div class="m1"><p>دوش دل را در بلایی یافتم</p></div>
<div class="m2"><p>خانه چون ماتم سرایی یافتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم ای دل چیست حال آخر بگو</p></div>
<div class="m2"><p>گفت بوی آشنایی یافتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو گویی در خم چوگان عشق</p></div>
<div class="m2"><p>خویش را نه سر نه پایی یافتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواستم تا دل نثار او کنم</p></div>
<div class="m2"><p>زانکه جانم را سزایی یافتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش از من جان بر او رفته بود</p></div>
<div class="m2"><p>گرچه من بی‌جان بقایی یافتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن بقا از جان نبود از عشق بود</p></div>
<div class="m2"><p>زانکه عشق جان فزایی یافتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مردم چشم خودش خوانم از آنک</p></div>
<div class="m2"><p>دایمش در دیده جایی یافتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرچه زلف او گره بسیار داشت</p></div>
<div class="m2"><p>هر گره مشکل‌گشایی یافتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با چنان مشکل‌گشایی حل نشد</p></div>
<div class="m2"><p>آنچه من از دلربایی یافتم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون به خون خویشتن بستم سجل</p></div>
<div class="m2"><p>هر سرشکی را گوایی یافتم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون سجل بندم به خون چون پیش ازین</p></div>
<div class="m2"><p>از لب او خون بهایی یافتم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عقل از زلفش ز بس کاندیشه کرد</p></div>
<div class="m2"><p>حاصلش تاریکنایی یافتم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با دهانش تا دوچاری خورد دل</p></div>
<div class="m2"><p>دایمش در تنگنایی یافتم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در هوای او دل عطار را</p></div>
<div class="m2"><p>ذره کردم چون هبایی یافتم</p></div></div>