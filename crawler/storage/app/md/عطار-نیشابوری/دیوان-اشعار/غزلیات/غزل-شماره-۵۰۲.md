---
title: >-
    غزل شمارهٔ ۵۰۲
---
# غزل شمارهٔ ۵۰۲

<div class="b" id="bn1"><div class="m1"><p>رفتم به زیر پرده و بیرون نیامدم</p></div>
<div class="m2"><p>تا صید پرده‌بازی گردون نیامدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون قطب ساکن آمدم اندر مقام فقر</p></div>
<div class="m2"><p>هر لحظه همچو چرخ دگرگون نیامدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنهاده‌ام قدم به حرمگاه فقر در</p></div>
<div class="m2"><p>تا هرچه بود از همه بیرون نیامدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زر همچو گل ز صره از آن ریختم به خاک</p></div>
<div class="m2"><p>تا همچو غنچه با دل پر خون نیامدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از اهل روزگار به معیار امتحان</p></div>
<div class="m2"><p>کم نیستم به هیچ، گر افزون نیامدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچون مگس به ریزهٔ کس ننگریستم</p></div>
<div class="m2"><p>هر چند چون همای همایون نیامدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منت خدای را که اگر بود و گر نبود</p></div>
<div class="m2"><p>در زیر بار منت هر دون نیامدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر بی خبر برون درست از وجود من</p></div>
<div class="m2"><p>آخر من از عدم به شبیخون نیامدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عطار پر به سوی فلک همچو جبرئیل</p></div>
<div class="m2"><p>راه زمین مرو که چو قارون نیامدم</p></div></div>