---
title: >-
    غزل شمارهٔ ۷۳۸
---
# غزل شمارهٔ ۷۳۸

<div class="b" id="bn1"><div class="m1"><p>من کیم اندر جهان سرگشته‌ای</p></div>
<div class="m2"><p>در میان خاک و خون آغشته‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ریای خود منافق پیشه‌ای</p></div>
<div class="m2"><p>در نفاق خود ز حد بگذشته‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهرگردی خودنمایی رهزنی</p></div>
<div class="m2"><p>مفلسی بی پا و سر سرگشته‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در ازل گویی قلم رندم نبشت</p></div>
<div class="m2"><p>کاشکی هرگز قلم ننبشته‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک سر سوزن ندیدم روی دوست</p></div>
<div class="m2"><p>پس چرا گم کرده‌ام سر رشته‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برهمی جوید دلم ناکشته تخم</p></div>
<div class="m2"><p>کاشکی یک تخم هرگز کشته‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کیست عطار این سخن را هیچکس</p></div>
<div class="m2"><p>با دلی خاکی به خون بسرشته‌ای</p></div></div>