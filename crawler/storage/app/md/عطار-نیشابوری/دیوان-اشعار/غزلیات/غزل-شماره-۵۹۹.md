---
title: >-
    غزل شمارهٔ ۵۹۹
---
# غزل شمارهٔ ۵۹۹

<div class="b" id="bn1"><div class="m1"><p>تا با غم عشق آشنا گشتیم</p></div>
<div class="m2"><p>از نیک و بد جهان جدا گشتیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا هست شدیم در بقای تو</p></div>
<div class="m2"><p>از هستی خویشتن فنا گشتیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا در ره نامرادی افتادیم</p></div>
<div class="m2"><p>بر کل مراد پادشا گشتیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان دست همه جهان فرو بستی</p></div>
<div class="m2"><p>تا جمله به جملگی تورا گشتیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک شمه چو زان حدیث بنمودی</p></div>
<div class="m2"><p>مستغرق سر کبریا گشتیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زانگه که به عشق اقتدا کردیم</p></div>
<div class="m2"><p>در عالم عشق مقتدا گشتیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای دل تو کجایی او کجا آخر</p></div>
<div class="m2"><p>این خود چه سخن بود کجا گشتیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عمری مس نفس را بپالودیم</p></div>
<div class="m2"><p>گفتیم مگر که کیمیا گشتیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون روی چو آفتاب بنمودی</p></div>
<div class="m2"><p>ناچیز شدیم و چون هوا گشتیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون تاب جمال تو نیاوردیم</p></div>
<div class="m2"><p>سرگشته چو چرخ آسیا گشتیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون محرم عشق تو نیفتادیم</p></div>
<div class="m2"><p>در زیر زمین چو توتیا گشتیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نومید مشو درین ره ای عطار</p></div>
<div class="m2"><p>هرچند که نا امید وا گشتیم</p></div></div>