---
title: >-
    غزل شمارهٔ ۶۶
---
# غزل شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>روی تو شمع آفتاب بس است</p></div>
<div class="m2"><p>موی تو عطر مشک ناب بس است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند پیکار آفتاب کشم</p></div>
<div class="m2"><p>قبلهٔ رویت آفتاب بس است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی چون روز در نقاب مپوش</p></div>
<div class="m2"><p>زلف شبرنگ تو نقاب بس است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خطا گر کشیدمت سر زلف</p></div>
<div class="m2"><p>چین ابروی تو جواب بس است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر همه عمر این خطا کردم</p></div>
<div class="m2"><p>در همه عمرم این صواب بس است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تاب در زلف دلستان چه دهی</p></div>
<div class="m2"><p>دل من بی تو جای تاب بس است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه قرارم بری که خواب از من</p></div>
<div class="m2"><p>برد آن چشم نیم خواب بس است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه زنی در من آتشی که مرا</p></div>
<div class="m2"><p>در گذشته ز فرق آب بس است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر ز ماهی طلب کنی سی روز</p></div>
<div class="m2"><p>از توام سی در خوشاب بس است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا ابد بیهشان روی تو را</p></div>
<div class="m2"><p>عرق روی تو گلاب بس است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مجلس انس تشنگان تو را</p></div>
<div class="m2"><p>لب میگون تو شراب بس است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رگ و پی در تنم در آن مجلس</p></div>
<div class="m2"><p>همچو زیر و بم رباب بس است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر نمکدان تو شکر ریز است</p></div>
<div class="m2"><p>دل پر شور من کباب بس است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دل عطار تا که جان دارد</p></div>
<div class="m2"><p>کنج عشق تو را خراب بس است</p></div></div>