---
title: >-
    غزل شمارهٔ ۸۰۷
---
# غزل شمارهٔ ۸۰۷

<div class="b" id="bn1"><div class="m1"><p>به هر کویی مرا تا کی دوانی</p></div>
<div class="m2"><p>ز هر زهری مرا تا کی چشانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو زهرم می‌چشاند چرخ گردون</p></div>
<div class="m2"><p>به تریاک سعادت کی رسانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گهی تابوتم اندازی به دریا</p></div>
<div class="m2"><p>گهی بر تخت فرعونم نشانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برآری برفراز طور سینا</p></div>
<div class="m2"><p>شراب الفت وصلم چشانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو بنده مست شد دیدار خود را</p></div>
<div class="m2"><p>خطاب آید که موسی لن‌ترانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ایا موسی سخن گستاخ تا چند</p></div>
<div class="m2"><p>نه آنی که شعیبم را شبانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من آنم که شعیبت را شبانم</p></div>
<div class="m2"><p>تو آنی که شبانی را بخوانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منم موسی تویی جبار عالم</p></div>
<div class="m2"><p>گرم خوانی ورم رانی تو دانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شبانی را کجا آن قدر باشد</p></div>
<div class="m2"><p>که تو بی‌واسطه وی را بخوانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سخن گویی بدو در طور سینا</p></div>
<div class="m2"><p>درو در و گهر سازی نهانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ایا موسی تو رخت خویش بربند</p></div>
<div class="m2"><p>که تا خود را به منزلگه رسانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه ایوبم که چندین صبر دارم</p></div>
<div class="m2"><p>نیم یوسف که در چاهم نشانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برون آمد گل زرد از گل سرخ</p></div>
<div class="m2"><p>مکن در باغ ویران باغبانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نشان وصل ما موی سفید است</p></div>
<div class="m2"><p>رسول آشکارا نه نهانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زهی عطار کز بحر معانی</p></div>
<div class="m2"><p>به الماس سخن در می‌چکانی</p></div></div>