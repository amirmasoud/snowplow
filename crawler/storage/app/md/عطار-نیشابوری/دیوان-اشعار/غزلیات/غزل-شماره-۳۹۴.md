---
title: >-
    غزل شمارهٔ ۳۹۴
---
# غزل شمارهٔ ۳۹۴

<div class="b" id="bn1"><div class="m1"><p>ای در درون جانم و جان از تو بی خبر</p></div>
<div class="m2"><p>وز تو جهان پر است و جهان از تو بی خبر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون پی برد به تو دل و جانم که جاودان</p></div>
<div class="m2"><p>در جان و در دلی دل و جان از تو بی خبر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای عقل پیر و بخت جوان گرد راه تو</p></div>
<div class="m2"><p>پیر از تو بی نشان و جوان از تو بی خبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقش تو در خیال و خیال از تو بی نصیب</p></div>
<div class="m2"><p>نام تو بر زبان و زبان از تو بی خبر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تو خبر به نام و نشان است خلق را</p></div>
<div class="m2"><p>وآنگه همه به نام و نشان از تو بی خبر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جویندگان جوهر دریای کنه تو</p></div>
<div class="m2"><p>در وادی یقین و گمان از تو بی خبر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون بی خبر بود مگس از پر جبرئیل</p></div>
<div class="m2"><p>از تو خبر دهند و چنان از تو بی خبر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شرح و بیان تو چه کنم زانکه تا ابد</p></div>
<div class="m2"><p>شرح از تو عاجز است و بیان از تو بی خبر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عطار اگرچه نعرهٔ عشق تو می‌زند</p></div>
<div class="m2"><p>هستند جمله نعره‌زنان از تو بی خبر</p></div></div>