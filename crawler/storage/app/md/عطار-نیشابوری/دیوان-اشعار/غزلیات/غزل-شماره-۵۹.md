---
title: >-
    غزل شمارهٔ ۵۹
---
# غزل شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>آن دهان نیست که تنگ شکر است</p></div>
<div class="m2"><p>وان میان نیست که مویی دگر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان تنم شد چو میانت باریک</p></div>
<div class="m2"><p>کز دهان تو دلم تنگ‌تر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دهان و به میانت ماند</p></div>
<div class="m2"><p>چشم سوزن که به دو رشته در است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که مویی ز میان و ز دهانت</p></div>
<div class="m2"><p>خبری باز دهد بی‌خبر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از میان تو سخن چون مویی است</p></div>
<div class="m2"><p>وز دهان تو سخن چون شکر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه کمر را ز میانت وطنی است</p></div>
<div class="m2"><p>نه سخن را ز دهانت گذر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میم دیدی که به جای دهن است</p></div>
<div class="m2"><p>موی دیدی که میان کمر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه میان چون الفی معدوم است</p></div>
<div class="m2"><p>چه دهان چون صدفی پر گوهر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون میان تو سخن گفت فرید</p></div>
<div class="m2"><p>چون دهان تو از آن نامور است</p></div></div>