---
title: >-
    غزل شمارهٔ ۷۴
---
# غزل شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>در دلم تا برق عشق او بجست</p></div>
<div class="m2"><p>رونق بازار زهد من شکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون مرا می‌دید دل برخاسته</p></div>
<div class="m2"><p>دل ز من بربود و درجانم نشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خنجر خون‌ریز او خونم بریخت</p></div>
<div class="m2"><p>ناوک سر تیز او جانم بخست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آتش عشقش ز غیرت بر دلم</p></div>
<div class="m2"><p>تاختن آورد همچون شیر مست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بانگ بر من زد که ای ناحق شناس</p></div>
<div class="m2"><p>دل به ما ده چند باشی بت‌پرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر سر هستی ما داری تمام</p></div>
<div class="m2"><p>در ره ما نیست گردان هرچه هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که او در هستی ما نیست شد</p></div>
<div class="m2"><p>دایم از ننگ وجود خویش رست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می‌ندانی کز چه ماندی در حجاب</p></div>
<div class="m2"><p>پردهٔ هستی تو ره بر تو بست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرغ دل چون واقف اسرار گشت</p></div>
<div class="m2"><p>می‌طپید از شوق چون ماهی بشست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر امید این گهر در بحر عشق</p></div>
<div class="m2"><p>غرقه شد وان گوهرش نامد به دست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آخر این نومیدی ای عطار چیست</p></div>
<div class="m2"><p>تو نه ای مردانه همتای تو هست</p></div></div>