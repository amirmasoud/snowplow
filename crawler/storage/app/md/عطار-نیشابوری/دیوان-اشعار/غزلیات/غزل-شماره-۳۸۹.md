---
title: >-
    غزل شمارهٔ ۳۸۹
---
# غزل شمارهٔ ۳۸۹

<div class="b" id="bn1"><div class="m1"><p>بردار صراحیی ز خمار</p></div>
<div class="m2"><p>بربند به روی خرقه زنار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با دردکشان دردپیشه</p></div>
<div class="m2"><p>بنشین و دمی مباش هشیار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا پیش هوا به سجده درشو</p></div>
<div class="m2"><p>یا بند هوا ز پای بردار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا چند نهان کنی به تلبیس</p></div>
<div class="m2"><p>این دین مزورت ز اغیار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کی ز مذبذبین بوی تو</p></div>
<div class="m2"><p>یک لحظه نخفته و نه بیدار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر زن صفتی به کوی سر نه</p></div>
<div class="m2"><p>ور مرد رهی درآی در کار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر در نه و هرچه بایدت کن</p></div>
<div class="m2"><p>گه کعبه مجوی و گاه خمار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون سیر شدی ز هرزه کاری</p></div>
<div class="m2"><p>آنگاه به دین درآی یکبار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گه آیی و گاه بازگردی</p></div>
<div class="m2"><p>این نیست نشان مرد دین‌دار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چیزی که صلاح تو در آن است</p></div>
<div class="m2"><p>بنیوش که با تو گفت عطار</p></div></div>