---
title: >-
    غزل شمارهٔ ۶۴۱
---
# غزل شمارهٔ ۶۴۱

<div class="b" id="bn1"><div class="m1"><p>نیست ره عشق را برگ و نوا ساختن</p></div>
<div class="m2"><p>خرقهٔ پیروز را دام ریا ساختن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلق و عصا را بسوز کین نه نکو مذهبی است</p></div>
<div class="m2"><p>از پی دیدار حق دلق و عصا ساختن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرغ دلت را که اوست مرغ هوا خواه دوست</p></div>
<div class="m2"><p>لایق عشاق نیست صید هوا ساختن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از فلک بی‌قرار هیچ نیاموختن</p></div>
<div class="m2"><p>در طلب درد عشق پشت دوتا ساختن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مفلس این راه را سلطنت فقر چیست</p></div>
<div class="m2"><p>برگ عدم داشتن راه فنا ساختن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر سر میدان عشق در خم چوگان دوست</p></div>
<div class="m2"><p>دل به صفت همچو گوی بی سر و پا ساختن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کار تو در بند توست کار بساز و بیا</p></div>
<div class="m2"><p>پیش برون کی شود کار ز ناساختن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زخم خور ار عاشقی زانکه پدیدار نیست</p></div>
<div class="m2"><p>خستگی عشق را هیچ دوا ساختن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا دل عطار را درد و دوا شد یکی</p></div>
<div class="m2"><p>نیست جز او را به عشق مدح و ثنا ساختن</p></div></div>