---
title: >-
    غزل شمارهٔ ۲۰۷
---
# غزل شمارهٔ ۲۰۷

<div class="b" id="bn1"><div class="m1"><p>هر که را عشق تو سرگردان کرد</p></div>
<div class="m2"><p>هرگزش چارهٔ آن نتوان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چارهٔ عشق تو بیچارگی است</p></div>
<div class="m2"><p>هر که بیچاره نشد تاوان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر به فرمان بنهد خورشیدش</p></div>
<div class="m2"><p>هر که یک ذره تو را فرمان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون به زیبایی آن داری تو</p></div>
<div class="m2"><p>این چنین عاشق زارم آن کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم خون‌ریز تو از غمزهٔ تیز</p></div>
<div class="m2"><p>چشم این سوخته خون‌افشان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه کنی قصد به خونم که دلم</p></div>
<div class="m2"><p>خویش را پیش رخت قربان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان عطار تو خود می‌دانی</p></div>
<div class="m2"><p>که هوایت ز میان جان کرد</p></div></div>