---
title: >-
    غزل شمارهٔ ۱۵۴
---
# غزل شمارهٔ ۱۵۴

<div class="b" id="bn1"><div class="m1"><p>پیر ما بار دگر روی به خمار نهاد</p></div>
<div class="m2"><p>خط به دین برزد و سر بر خط کفار نهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خرقه آتش زد و در حلقهٔ دین بر سر جمع</p></div>
<div class="m2"><p>خرقهٔ سوخته در حلقهٔ زنار نهاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در بن دیر مغان در بر مشتی اوباش</p></div>
<div class="m2"><p>سر فرو برد و سر اندر پی این کار نهاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درد خمار بنوشید و دل از دست بداد</p></div>
<div class="m2"><p>می‌خوران نعره‌زنان روی به بازار نهاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم ای پیر چه بود این که تو کردی آخر</p></div>
<div class="m2"><p>گفت کین داغ مرا بر دل و جان یار نهاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من چه کردم چو چنین خواست چنین باید بود</p></div>
<div class="m2"><p>گلم آن است که او در ره من خار نهاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باز گفتم که اناالحق زده‌ای سر در باز</p></div>
<div class="m2"><p>گفت آری زده‌ام روی سوی دار نهاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل چو بشناخت که عطار درین راه بسوخت</p></div>
<div class="m2"><p>از پی پیر قدم در پی عطار نهاد</p></div></div>