---
title: >-
    غزل شمارهٔ ۳۸۶
---
# غزل شمارهٔ ۳۸۶

<div class="b" id="bn1"><div class="m1"><p>اگر خورشید خواهی سایه بگذار</p></div>
<div class="m2"><p>چو مادر هست شیر دایه بگذار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو با خورشید هم‌تک می‌توان شد</p></div>
<div class="m2"><p>ز پس در تک زدن چون سایه بگذار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو همسایه است با جان تو جانان</p></div>
<div class="m2"><p>بده جان و حق همسایه بگذار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو را سرمایهٔ هستی بلایی است</p></div>
<div class="m2"><p>زیانت سود کن سرمایه بگذار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو مردان جوشن و شمشیر برگیر</p></div>
<div class="m2"><p>نه‌ای آخر چو زن پیرایه بگذار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فلک طشت است و اختر خایه در طشت</p></div>
<div class="m2"><p>خیال علم طشت و خایه بگذار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فروتر پایهٔ تو عرش اعلاست</p></div>
<div class="m2"><p>تو برتر رو فروتر پایه بگذار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فرید از مایهٔ هستی جدا شد</p></div>
<div class="m2"><p>تو هم مردی شو و این مایه بگذار</p></div></div>