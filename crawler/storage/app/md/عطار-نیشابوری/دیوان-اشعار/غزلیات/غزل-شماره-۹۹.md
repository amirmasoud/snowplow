---
title: >-
    غزل شمارهٔ ۹۹
---
# غزل شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>نور ایمان از بیاض روی اوست</p></div>
<div class="m2"><p>ظلمت کفر از سر یک موی اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ذره ذره در دو عالم هر چه هست</p></div>
<div class="m2"><p>پرده‌ای در آفتاب روی اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که را در هر دو عالم قبله‌ای است</p></div>
<div class="m2"><p>گرچه نیست آگاه آنکس سوی اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر دو عالم هیچ می‌دانی که چیست</p></div>
<div class="m2"><p>هر دو عکس طاق دو ابروی اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون کمان ابروی او درکشیم</p></div>
<div class="m2"><p>کان کمان پیوسته بر بازوی اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن همه غوغای روز رستخیز</p></div>
<div class="m2"><p>از مصاف غمزهٔ جادوی اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رستخیز آری کلمح باالبصر</p></div>
<div class="m2"><p>از خدنگ چشم چون آهوی اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم زمین از راه او گردی است بس</p></div>
<div class="m2"><p>هر فلک سرگشته‌ای در کوی اوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زان سیه گردد قیامت آفتاب</p></div>
<div class="m2"><p>تا شود روشن که او هندوی اوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آسمان را از درش بویی رسید</p></div>
<div class="m2"><p>تا قیامت سرنگون بر بوی اوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خلق هر دو کون را درد گناه</p></div>
<div class="m2"><p>بر امید ذره‌ای داروی اوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا که بویی یافت عطار از درش</p></div>
<div class="m2"><p>دل نمی‌داند که در پهلوی اوست</p></div></div>