---
title: >-
    غزل شمارهٔ ۶۴۳
---
# غزل شمارهٔ ۶۴۳

<div class="b" id="bn1"><div class="m1"><p>بندگی چیست به فرمان رفتن</p></div>
<div class="m2"><p>پیش امر از بن دندان رفتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه دشواری تو از طمع است</p></div>
<div class="m2"><p>ترک خود گفتن و آسان رفتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر فدا کردن و سامان جستن</p></div>
<div class="m2"><p>وانگهی بی سر و سامان رفتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قابل امر شدن همچون گوی</p></div>
<div class="m2"><p>پس به یک ضربه به پایان رفتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از گران‌باری خود ترسیدن</p></div>
<div class="m2"><p>پس سبکبار به پیشان رفتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در پی شمع شریعت شب و روز</p></div>
<div class="m2"><p>همچو پروانه به پیمان رفتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آبرو باش تو در جوی طریق</p></div>
<div class="m2"><p>تا توانی تو بیابان رفتن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برگ ره ساز که بی برگ رهی</p></div>
<div class="m2"><p>در چنین بادیه نتوان رفتن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر تو دنیا همه زندان دیدی</p></div>
<div class="m2"><p>فرخت باد ز زندان رفتن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ور ندانی تو به جز دنیا هیچ</p></div>
<div class="m2"><p>مرده باید به فراوان رفتن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا کی از خواب درآموز آخر</p></div>
<div class="m2"><p>یک شب از گنبد گردان رفتن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قرن‌ها شد که نمی‌آسایند</p></div>
<div class="m2"><p>از تو شب خفتن وزیشان رفتن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عاشقان راست مسلم نه تو را</p></div>
<div class="m2"><p>در ره دوست به مژگان رفتن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سر فدا کردن و چون عیاران</p></div>
<div class="m2"><p>جان به کف بر در جانان رفتن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ترک عطار به گفتن کلی</p></div>
<div class="m2"><p>پس درین بادیه ترسان رفتن</p></div></div>