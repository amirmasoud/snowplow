---
title: >-
    غزل شمارهٔ ۶۹۸
---
# غزل شمارهٔ ۶۹۸

<div class="b" id="bn1"><div class="m1"><p>ای دو عالم پرتوی از روی تو</p></div>
<div class="m2"><p>جنت الفردوس خاک کوی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد جهان پر عاشق سرگشته را</p></div>
<div class="m2"><p>هیچ وجهی نیست الا روی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد هزارن قصه دارم دردناک</p></div>
<div class="m2"><p>دور از روی تو با هر موی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کور باید گشت از دید دو کون</p></div>
<div class="m2"><p>تا توان کردن نگاهی سوی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یافت هندوخان لقب بر خوان چرخ</p></div>
<div class="m2"><p>ترک گردون تا که شد هندوی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پشت صد صد پهلوان می‌بشکند</p></div>
<div class="m2"><p>تیر یک یک غمزهٔ جادوی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دی مرا خواندی به تیر غمزه پیش</p></div>
<div class="m2"><p>تا کمان بر زه کنم ز ابروی تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خود سپر بفکندم و بگریختم</p></div>
<div class="m2"><p>کان کمان هم هست بر بازوی تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه ز تو بگریختم از بیم سنگ</p></div>
<div class="m2"><p>زانکه دیدم سنگ در پهلوی تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شد زبان در وصف تو عطار را</p></div>
<div class="m2"><p>درفشان چون حلقهٔ لؤلؤی تو</p></div></div>