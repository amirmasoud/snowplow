---
title: >-
    غزل شمارهٔ ۶۷۵
---
# غزل شمارهٔ ۶۷۵

<div class="b" id="bn1"><div class="m1"><p>ای صبا برگرد امشب گرد سر تاپای او</p></div>
<div class="m2"><p>صد هزاران سجده کن در عشق یک یک جای او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان ما را زندهٔ جاوید گردانی به قطع</p></div>
<div class="m2"><p>گر نسیمی آوری از زلف عنبرسای او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر سر انگشت بی حرمت به زلف او بری</p></div>
<div class="m2"><p>دشنهٔ خونین خوری از نرگس شهلای او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیک راهی تو به شمع روی او منگر بسی</p></div>
<div class="m2"><p>تا نگردی همچو من پروانه نا پروای او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست دستوری که آری چهرهٔ او در نظر</p></div>
<div class="m2"><p>کز نظر آزرده گردد چهرهٔ زیبای او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر تو خواهی کرد کاری صد جهان جان وام کن</p></div>
<div class="m2"><p>پس برافشان جمله بر روی جهان آرای او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جام جم پر آب خضر از دست عیسی چون خورند</p></div>
<div class="m2"><p>همچنان خور شربتی از جام جان افزای او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منتظر بنشسته‌ام تا تحفه آری زودتر</p></div>
<div class="m2"><p>سر به مهرم یک شکر از لعل گوهر زای او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جهد کن تا آن سمن را بر نیازاری به گرد</p></div>
<div class="m2"><p>خاصه آن ساعت که روی آری به خاک پای او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا نسازی چشم را از خاک پایش توتیا</p></div>
<div class="m2"><p>کی توانی شد به چشم خویشتن بینای او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غسل ناکرده مرو تر دامن آنجا زینهار</p></div>
<div class="m2"><p>زانکه نتوان کرد الا پاک دامن رای او</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غسل کن اول به آب دیدهٔ من هفت بار</p></div>
<div class="m2"><p>تا طهارت کرده گردی گرد هفت اعضای او</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر زیان کردی دل و دین در ره او ای فرید</p></div>
<div class="m2"><p>سود تو در هر دو عالم بس بود سودای او</p></div></div>