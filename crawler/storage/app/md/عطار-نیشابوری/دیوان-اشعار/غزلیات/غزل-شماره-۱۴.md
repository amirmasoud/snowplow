---
title: >-
    غزل شمارهٔ ۱۴
---
# غزل شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>چه شاهدی است که با ماست در میان امشب</p></div>
<div class="m2"><p>که روشن است ز رویش همه جهان امشب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه شمع راست شعاعی، نه ماه را تابی</p></div>
<div class="m2"><p>نه زهره راست فروغی در آسمان امشب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میان مجلس ما صورتی همی تابد</p></div>
<div class="m2"><p>که آفتاب شد از شرم او نهان امشب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسی سعادت از این شب پدید خواهد شد</p></div>
<div class="m2"><p>که هست مشتری و زهره را قران امشب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شبی خوش است و ز اغیار نیست کس بر ما</p></div>
<div class="m2"><p>غنیمت است ملاقات دوستان امشب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دمی خوش است مکن صبح دم دمی مردی</p></div>
<div class="m2"><p>که همدم است مرا یار مهربان امشب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میان ما و تو امشب کسی نمی گنجد</p></div>
<div class="m2"><p>که خلوتی است مرا با تو در نهان امشب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بساز مطرب از آن پرده‌های شور انگیز</p></div>
<div class="m2"><p>نوای تهنیت بزم عاشقان امشب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه حکایت مطبوع درد عطار است</p></div>
<div class="m2"><p>ترانهٔ خوش شیرین مطربان امشب</p></div></div>