---
title: >-
    غزل شمارهٔ ۸۴۷
---
# غزل شمارهٔ ۸۴۷

<div class="b" id="bn1"><div class="m1"><p>ترسا بچه‌ای دیشب در غایت ترسایی</p></div>
<div class="m2"><p>دیدم به در دیری چون بت که بیارایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنار کمر کرده وز دیر برون جسته</p></div>
<div class="m2"><p>طرف کله اشکسته از شوخی و رعنایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون چشم و لبش دیدم صد گونه بگردیدم</p></div>
<div class="m2"><p>ترسا بچه چون دیدم بی توش و توانایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آمد بر من سرمست زنار و می اندر دست</p></div>
<div class="m2"><p>اندر بر من بنشست گفتا اگر از مایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امشب بر ما باشی تاج سر ما باشی</p></div>
<div class="m2"><p>ما از تو بیاساییم وز ما تو بیاسایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از جان کنمت خدمت بی منت و بی علت</p></div>
<div class="m2"><p>دارم ز تو صد منت کامشب بر ما آیی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفتم به در دیرش خوردم ز می عشقش</p></div>
<div class="m2"><p>در حال دلم دریافت راهی ز هویدایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عطار ز عشق او سرگشته و حیران شد</p></div>
<div class="m2"><p>در دیر مقیمی شد دین داد به ترسایی</p></div></div>