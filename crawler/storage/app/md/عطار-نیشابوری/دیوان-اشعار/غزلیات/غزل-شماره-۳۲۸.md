---
title: >-
    غزل شمارهٔ ۳۲۸
---
# غزل شمارهٔ ۳۲۸

<div class="b" id="bn1"><div class="m1"><p>زلف تو که فتنهٔ جهان بود</p></div>
<div class="m2"><p>جانم بربود و جای آن بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دل که زعشق تو خبر یافت</p></div>
<div class="m2"><p>صد جانش به رایگان گران بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرده‌دل آن کسی که او را</p></div>
<div class="m2"><p>در عشق تو زندگی به جان بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم دل خویش خون کنم من</p></div>
<div class="m2"><p>کز دست دلم بسی زیان بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناگاه کشیده داشت دستم</p></div>
<div class="m2"><p>چون پای غم تو در میان بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر من دادم امان دلم را</p></div>
<div class="m2"><p>دل را ز غم تو کی امان بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم که دهان تو ببینم</p></div>
<div class="m2"><p>خود از دهنت که را نشان بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرگز نرسید هیچ جایی</p></div>
<div class="m2"><p>آن را که غم چنان دهان بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتی که چگونه‌ای تو بی‌من</p></div>
<div class="m2"><p>دانی تو که بی‌تو چون توان بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز آنروز که یک زمانت دیدم</p></div>
<div class="m2"><p>صد ساله غمم به یک زمان بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر خاک درت نشسته عطار</p></div>
<div class="m2"><p>تا بود ز عشق جان فشان بود</p></div></div>