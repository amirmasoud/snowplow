---
title: >-
    غزل شمارهٔ ۵۶
---
# غزل شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>ره عشاق راهی بی‌کنار است</p></div>
<div class="m2"><p>ازین ره دور اگر جانت به کار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وگر سیری ز جان در باز جان را</p></div>
<div class="m2"><p>که یک جان را عوض آنجا هزار است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو هر وقتی که جانی برفشانی</p></div>
<div class="m2"><p>هزاران جان نو بر تو نثار است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وگر در یک قدم صد جان دهندت</p></div>
<div class="m2"><p>نثارش کن که جان‌ها بی‌شمار است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه خواهی کرد خود را نیم‌جانی</p></div>
<div class="m2"><p>چو دایم زندگی تو بیاراست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی کز جان بود زنده درین راه</p></div>
<div class="m2"><p>ز جرم خود همیشه شرمسار است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درآمد دوش در دل عشق جانان</p></div>
<div class="m2"><p>خطابم کرد کامشب روز بار است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کنون بی‌خود بیا تا بار یابی</p></div>
<div class="m2"><p>که شاخ وصل بی باران به بار است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو شد فانی دلت در راه معشوق</p></div>
<div class="m2"><p>قرار عشق جانان بی‌قرار است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو را اول قدم در وادی عشق</p></div>
<div class="m2"><p>به زارش کشتن است آنگاه دار است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وزان پس سوختن تا هم بوینی</p></div>
<div class="m2"><p>که نور عاشقان در مغز نار است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو خاکستر شوی و ذره گردی</p></div>
<div class="m2"><p>به رقص آیی که خورشید آشکار است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو را از کشتن و وز سوختن هم</p></div>
<div class="m2"><p>چه غم چون آفتابت غمگسار است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کسی سازد رسن از نور خورشید</p></div>
<div class="m2"><p>که اندر هستی خود ذره‌وار است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کسی کو در وجود خویش ماندست</p></div>
<div class="m2"><p>مده پندش که بندش استوار است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>درین مجلس کسی باید که چون شمع</p></div>
<div class="m2"><p>بریده سر نهاده بر کنار است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شبانروزی درین اندیشه عطار</p></div>
<div class="m2"><p>چو گل پر خون و چون نرگس نزار است</p></div></div>