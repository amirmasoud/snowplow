---
title: >-
    غزل شمارهٔ ۴۴۰
---
# غزل شمارهٔ ۴۴۰

<div class="b" id="bn1"><div class="m1"><p>ای دل ز جفای یار مندیش</p></div>
<div class="m2"><p>در نه قدم و ز کار مندیش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جویندهٔ در ز جان نترسد</p></div>
<div class="m2"><p>گل می‌طلبی ز خار مندیش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با پنجهٔ شیر پنجه می‌زن</p></div>
<div class="m2"><p>از کام و دهان مار مندیش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مردانه به کوی یار درشو</p></div>
<div class="m2"><p>از خنجر هر عیار مندیش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نیل وصال یار باید</p></div>
<div class="m2"><p>از گفتن ننگ و عار مندیش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون با تو بود عنایت یار</p></div>
<div class="m2"><p>گر خصم بود هزار مندیش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون یافته‌ای جمال او را</p></div>
<div class="m2"><p>از گشتن سنگسار مندیش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منصور تویی بزن اناالحق</p></div>
<div class="m2"><p>تسلیم شو و ز دار مندیش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عطار تویی چو ماه و خورشید</p></div>
<div class="m2"><p>در تاب زهر غبار مندیش</p></div></div>