---
title: >-
    غزل شمارهٔ ۲۵۱
---
# غزل شمارهٔ ۲۵۱

<div class="b" id="bn1"><div class="m1"><p>برقع از خورشید رویش دور شد</p></div>
<div class="m2"><p>ای عجب هر ذره‌ای صد حور شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو خورشید از فروغ طلعتش</p></div>
<div class="m2"><p>ذره ذره پای تا سر نور شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جملهٔ روی زمین موسی گرفت</p></div>
<div class="m2"><p>جملهٔ آفاق کوه طور شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون تجلی‌اش به فرق که فتاد</p></div>
<div class="m2"><p>طور با موسی بهم مهجور شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فوت خورشید نبود سایه را</p></div>
<div class="m2"><p>لاجرم آن آمد این مقهور شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قطره‌ای آوازهٔ دریا شنید</p></div>
<div class="m2"><p>از طمع شوریده و مغرور شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مدتی می‌رفت چون دریا بدید</p></div>
<div class="m2"><p>محو گشت و تا ابد مستور شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون در آن دریا نه بد دید و نه نیک</p></div>
<div class="m2"><p>نیک و بد آنجایگه معذور شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر دوعالم انگبین صرف بود</p></div>
<div class="m2"><p>لاجرم چون خانهٔ زنبور شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زانگبین چون آن همه زنبور خاست</p></div>
<div class="m2"><p>هر یکی هم زانگبین مخمور شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قسم هر یک زانگبین چندان رسید</p></div>
<div class="m2"><p>کز خود و از هر دو عالم دور شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سایه چون از ظلمت هستی برست</p></div>
<div class="m2"><p>در بر خورشید نورالنور شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همچو این عطار بس مشهور گشت</p></div>
<div class="m2"><p>همچو آن حلاج بس منصور شد</p></div></div>