---
title: >-
    غزل شمارهٔ ۳۶۷
---
# غزل شمارهٔ ۳۶۷

<div class="b" id="bn1"><div class="m1"><p>نه یار هرکسی را رخسار می‌نماید</p></div>
<div class="m2"><p>نه هر حقیر دل را دیدار می‌نماید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آرزوی رویش در خاک خفت و خون خور</p></div>
<div class="m2"><p>کان ماه‌روی رخ را دشوار می‌نماید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر چار سوی دعوی از بی‌نیازی خود</p></div>
<div class="m2"><p>سرهای سرکشان بین کز دار می‌نماید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سلطان غیرت او خون همه عزیزان</p></div>
<div class="m2"><p>بر خاک اگر بریزد بس خوار می‌نماید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر مرد ره نه‌ای تو بر بوی گل چه پویی</p></div>
<div class="m2"><p>رو باز گرد کین ره پر خار می‌نماید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زنهار تا بپویی بی رهبری درین ره</p></div>
<div class="m2"><p>زیرا که این بیابان خون‌خوار می‌نماید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر مردیی نداری پرهیز کن که چون تو</p></div>
<div class="m2"><p>سرگشتگان گمره بسیار می‌نماید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در راه کفر و ایمان مرد آن بود که خود را</p></div>
<div class="m2"><p>دایم چنانکه باشد در کار می‌نماید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در کار اگر تمامی در نه قدم درین ره</p></div>
<div class="m2"><p>کاحوال ناتمامان بس زار می‌نماید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کو آتشی که بر وی این خرقه را بسوزم</p></div>
<div class="m2"><p>کین خرقه در بر من زنار می‌نماید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اندر میان غفلت در خواب شد دل من</p></div>
<div class="m2"><p>کو هیچ دل که یک دم بیدار می‌نماید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جمله ز خود نمایی اندر نفاق مستند</p></div>
<div class="m2"><p>کو عاشقی که در دین هشیار می‌نماید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در بند دین و دنیی لیکن نه دین و دنیی</p></div>
<div class="m2"><p>سرگشته روزگاری عطار می‌نماید</p></div></div>