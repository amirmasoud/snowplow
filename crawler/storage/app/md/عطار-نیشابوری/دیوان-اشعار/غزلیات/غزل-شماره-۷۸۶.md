---
title: >-
    غزل شمارهٔ ۷۸۶
---
# غزل شمارهٔ ۷۸۶

<div class="b" id="bn1"><div class="m1"><p>چون خط شبرنگ بر گلگون کشی</p></div>
<div class="m2"><p>حلقه در گوش مه گردون کشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر ببینی روی خود در خط شده</p></div>
<div class="m2"><p>سرکشی و هر زمان افزون کشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفته بودی در خط خویشت کشم</p></div>
<div class="m2"><p>تا لباس سرکشی بیرون کشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خط تو بر ماه و من در قعر چاه</p></div>
<div class="m2"><p>در خط خویشم ندانم چون کشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بریزی بر زمین خونم رواست</p></div>
<div class="m2"><p>بلکه آن خواهم که تیغ اکنون کشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لیک زلفت از درازی بر زمین است</p></div>
<div class="m2"><p>خون شود جانم اگر در خون کشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌کشی در خاک زلفت تا مرا</p></div>
<div class="m2"><p>هر نفس در بند دیگرگون کشی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون منم دیوانه تو زنجیر زلف</p></div>
<div class="m2"><p>می‌کشی تا بر من مجنون کشی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دام مشکین می‌نهی عطار را</p></div>
<div class="m2"><p>تا به دام مشکش از افسون کشی</p></div></div>