---
title: >-
    غزل شمارهٔ ۶
---
# غزل شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>گر سیر نشد تو را دل از ما</p></div>
<div class="m2"><p>یک لحظه مباش غافل از ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آتش دل بسر همی گرد</p></div>
<div class="m2"><p>مانندهٔ مرغ بسمل از ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تر می‌گردان به خون دیده</p></div>
<div class="m2"><p>هر روز هزار منزل از ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون ابر بهار می‌گری زار</p></div>
<div class="m2"><p>تا خاک ز خون کنی گل از ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آخر به چه میل همچو خامان</p></div>
<div class="m2"><p>گه گاه بگیردت دل از ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا در غم ما تمام پیوند</p></div>
<div class="m2"><p>یا رشتهٔ عشق بگسل از ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگریز ز ما اگرچه نامد</p></div>
<div class="m2"><p>جز رنج و بلات حاصل از ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کز هر رنجی گشاده گردد</p></div>
<div class="m2"><p>صد گنج طلسم مشکل از ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عطار در این مقام چون است</p></div>
<div class="m2"><p>دیوانهٔ عشق و عاقل از ما</p></div></div>