---
title: >-
    غزل شمارهٔ ۶۰۷
---
# غزل شمارهٔ ۶۰۷

<div class="b" id="bn1"><div class="m1"><p>دردا که درین بادیه بسیار دویدیم</p></div>
<div class="m2"><p>در خود برسیدیم و بجایی نرسیدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسیار درین بادیه شوریده برفتیم</p></div>
<div class="m2"><p>بسیار درین واقعه مردانه چخیدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گه نعره‌زنان معتکف صومعه بودیم</p></div>
<div class="m2"><p>گه رقص‌کنان گوشهٔ خمار گزیدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کردیم همه کار ولی هیچ نکردیم</p></div>
<div class="m2"><p>دیدیم همه چیز ولی هیچ ندیدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر درج دل ماست یکی قفل گران سنگ</p></div>
<div class="m2"><p>در بند ازینیم که در بند کلیدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خون رحم چون به گو خاک فتادیم</p></div>
<div class="m2"><p>از طفل مزاجی همه انگشت مزیدیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون شیر ز انگشت براهیم برآمد</p></div>
<div class="m2"><p>انگشت مزیدان چه که انگشت گزیدیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وامروز که بالغ شدگانیم به صورت</p></div>
<div class="m2"><p>یک پر بنماند ارچه به صد پر بپریدیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از دست فتادیم نه دیده نه چشیده</p></div>
<div class="m2"><p>زان باده که از جرعهٔ او بوی شنیدیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون هستی عطار درین راه حجاب است</p></div>
<div class="m2"><p>از هستی عطار به یکبار بریدیم</p></div></div>