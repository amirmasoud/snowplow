---
title: >-
    غزل شمارهٔ ۱۳۶
---
# غزل شمارهٔ ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>دوش جان دزدیده از دل راه جانان برگرفت</p></div>
<div class="m2"><p>دل خبر یافت و به تک خاست و دل از جان برگرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان چو شد نزدیک جانان دید دل را نزد او</p></div>
<div class="m2"><p>غصه‌ها کردش ز پشت دست دندان برگرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناگهی بادی برآمد مشکبار از پیش و پس</p></div>
<div class="m2"><p>برقع صورت ز پیش روی جانان برگرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان ز خود فانی شد و دل در عدم معدوم گشت</p></div>
<div class="m2"><p>عقل حیلت‌گر به کلی دست ازیشان برگرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی نشان شد جان کدامین جان که گنجی داشت او</p></div>
<div class="m2"><p>گاه پیدایش نهاد و گاه پنهان برگرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرخ آن اقبال باری کاندرین دریای ژرف</p></div>
<div class="m2"><p>ترک جان گفت و سر این نفس حیوان برگرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکر یزدان را که گنج دین درین کنج خراب</p></div>
<div class="m2"><p>بی غم و رنجی دل عطار آسان برگرفت</p></div></div>