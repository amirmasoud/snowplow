---
title: >-
    غزل شمارهٔ ۷۷۹
---
# غزل شمارهٔ ۷۷۹

<div class="b" id="bn1"><div class="m1"><p>ترسا بچه‌ای شنگی زین نادره دلداری</p></div>
<div class="m2"><p>زین خوش نمکی شوخی، زین طرفه جگرخواری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از پستهٔ خندانش هرجا که شکر ریزی</p></div>
<div class="m2"><p>در چاه زنخدانش هر جا که نگونساری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از هر سخن تلخش ره یافته بی دینی</p></div>
<div class="m2"><p>وز هر شکن زلفش گمره شده دین‌داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیوانهٔ عشق او هرجا که خردمندی</p></div>
<div class="m2"><p>دردی کش درد او هرجا که طلب کاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آمد بر پیر ما می در سر و می در بر</p></div>
<div class="m2"><p>پس در بر پیر ما بنشست چو هشیاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتش که بگیر این می، این روی و ریا تا کی</p></div>
<div class="m2"><p>گر نوش کنی یک می، از خود برهی باری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای همچو یخ افسرده یک لحظه برم بنشین</p></div>
<div class="m2"><p>تا در تو زند آتش ترسا بچه یک باری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی خویش شو از هستی تا باز نمانی تو</p></div>
<div class="m2"><p>ای چون تو به هر منزل واماندهٔ بسیاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیر از سر بی خویشی، می بستد و بیخود شد</p></div>
<div class="m2"><p>در حال پدید آمد در سینهٔ او ناری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کاریش پدید آمد کان پیر نود ساله</p></div>
<div class="m2"><p>بر جست و میان حالی بر بست به زناری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در خواب شد از مستی بیدار شد از هستی</p></div>
<div class="m2"><p>از صومعه بیرون شد بنشست چو خماری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عطار ز کار او در مانده به صد حیرت</p></div>
<div class="m2"><p>هرکس که چنین بیند حیرت بودش آری</p></div></div>