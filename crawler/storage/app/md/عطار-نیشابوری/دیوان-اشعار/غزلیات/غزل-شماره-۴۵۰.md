---
title: >-
    غزل شمارهٔ ۴۵۰
---
# غزل شمارهٔ ۴۵۰

<div class="b" id="bn1"><div class="m1"><p>ای عشق تو با وجود هم تنگ</p></div>
<div class="m2"><p>در راه تو کفر و دین به یک رنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی روی تو کعبه‌ها خرابات</p></div>
<div class="m2"><p>بی نام تو نامها همه ننگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در عشق تو هر که نیست قلاش</p></div>
<div class="m2"><p>دور است به صد هزار فرسنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قلاشان را درین ولایت</p></div>
<div class="m2"><p>از دار همی کنند آونگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشقت به ترازوی قیامت</p></div>
<div class="m2"><p>دو کون نسخت نیم جو سنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قرابهٔ ننگ و شیشهٔ نام</p></div>
<div class="m2"><p>افتاد و شکست بر سر سنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زنار مغانه بر میان بند</p></div>
<div class="m2"><p>وانگه به کلیسیا کن آهنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مردانه درآی کاندرین راه</p></div>
<div class="m2"><p>نه بوی همی خرند و نه رنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>راهی است دراز و عمر کوتاه</p></div>
<div class="m2"><p>باری است گران و مرکبی لنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کلی ز سر وجود برخیز</p></div>
<div class="m2"><p>افتاده مباش بر در تنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می‌دان به یقین که در دو عالم</p></div>
<div class="m2"><p>در راه تو نیست جز تو خرسنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برخیز ز راه خود چو عطار</p></div>
<div class="m2"><p>تا بازرهی ز صلح و از جنگ</p></div></div>