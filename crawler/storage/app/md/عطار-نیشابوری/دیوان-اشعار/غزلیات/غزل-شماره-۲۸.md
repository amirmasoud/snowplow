---
title: >-
    غزل شمارهٔ ۲۸
---
# غزل شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>چون ز مرغ سحر فغان برخاست</p></div>
<div class="m2"><p>ناله از طاق آسمان برخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبح چون دردمید از پس کوه</p></div>
<div class="m2"><p>آتشی از همه جهان برخاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عنبر شب چو سوخت زآتش صبح</p></div>
<div class="m2"><p>بوی عنبر ز گلستان برخاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سپر آفتاب تیغ کشید</p></div>
<div class="m2"><p>قلم عافیت ز جان برخاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقی از در درآمد و بنشست</p></div>
<div class="m2"><p>صد قیامت به یک زمان برخاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کس چه داند که چون شراب بخورد</p></div>
<div class="m2"><p>شور چون از شکرستان برخاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زآرزوی سماع و شاهد و می</p></div>
<div class="m2"><p>از همه عاشقان فغان برخاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باده ناخورده مست شد عطار</p></div>
<div class="m2"><p>سوی مدح خدایگان برخاست</p></div></div>