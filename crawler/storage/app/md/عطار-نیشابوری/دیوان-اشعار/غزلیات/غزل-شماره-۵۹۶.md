---
title: >-
    غزل شمارهٔ ۵۹۶
---
# غزل شمارهٔ ۵۹۶

<div class="b" id="bn1"><div class="m1"><p>بس که جان در خاک این در سوختیم</p></div>
<div class="m2"><p>دل چو خون کردیم و در بر سوختیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در رهش با نیک و بد در ساختیم</p></div>
<div class="m2"><p>در غمش هم خشک و هم تر سوختیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوز ما با عشق او قوت نداشت</p></div>
<div class="m2"><p>گرچه ما هر دم قوی‌تر سوختیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون بدو ره نی و بی او صبر نی</p></div>
<div class="m2"><p>مضطرب گشتیم و مضطر سوختیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون ز جانان آتشی در جان فتاد</p></div>
<div class="m2"><p>جان خود چون عود مجمر سوختیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون ز دلبر طعم شکر یافتیم</p></div>
<div class="m2"><p>دل چو عود از طعم شکر سوختیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون دل و جان پردهٔ این راه بود</p></div>
<div class="m2"><p>جان ز جانان دل ز دلبر سوختیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مدت سی سال سودا پخته‌ایم</p></div>
<div class="m2"><p>مدت سی سال دیگر سوختیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عاقبت چون شمع رویش شعله زد</p></div>
<div class="m2"><p>راست چون پروانه‌ای پر سوختیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پر چو سوخت آنگه درافکندیم خویش</p></div>
<div class="m2"><p>تا به‌کلی پای تا سر سوختیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خواه گو بنمای روی و خواه نه</p></div>
<div class="m2"><p>ما سپند روی او بر سوختیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون به یک جو می‌نیرزیدیم ما</p></div>
<div class="m2"><p>خرمن پندار یکسر سوختیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون شکست اینجا قلم عطار را</p></div>
<div class="m2"><p>اعجمی گشتیم و دفتر سوختیم</p></div></div>