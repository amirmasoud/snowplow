---
title: >-
    غزل شمارهٔ ۸۵۱
---
# غزل شمارهٔ ۸۵۱

<div class="b" id="bn1"><div class="m1"><p>چون روی بود بدان نکویی</p></div>
<div class="m2"><p>نازش برود به هرچه گویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رویی که ز شرم او درافتاد</p></div>
<div class="m2"><p>خورشید فلک به زرد رویی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون در خور او نمی‌توان شد</p></div>
<div class="m2"><p>بر بوی وصال او چه پویی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خون می‌خور و پشت دست می‌خای</p></div>
<div class="m2"><p>گر در ره درد مرد اویی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جانان به تو باز ننگرد راست</p></div>
<div class="m2"><p>تا دست ز جان و دل نشویی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو ره نبری تو تا تویی تو</p></div>
<div class="m2"><p>تا کی تو تویی تویی و تویی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چیزی که ازو خبر نداری</p></div>
<div class="m2"><p>گم ناشده از تو چند جویی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر گویندت چه گم شد از تو</p></div>
<div class="m2"><p>ای غره به خویشتن چه گویی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باری بنشین گزاف کم‌گوی</p></div>
<div class="m2"><p>بندیش که در چه آرزویی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عطار کجا رسی به سلطان</p></div>
<div class="m2"><p>زیرا که کم از سگان کویی</p></div></div>