---
title: >-
    غزل شمارهٔ ۱۴۴
---
# غزل شمارهٔ ۱۴۴

<div class="b" id="bn1"><div class="m1"><p>ای آفتاب سرکش یک ذره خاک پایت</p></div>
<div class="m2"><p>آب حیات رشحی از جام جانفزایت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم خواجه تاش گردون دل بر وفا غلامت</p></div>
<div class="m2"><p>هم پادشاه گیتی جان بر میان گدایت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم چرخ خرقه‌پوشی در خانقاه عشقت</p></div>
<div class="m2"><p>هم جبرئیل مرغی در دام دل ربایت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در سر گرفته عالم اندیشهٔ وصالت</p></div>
<div class="m2"><p>در چشم کرده کوثر خاک در سرایت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کوثر که آب حیوان یک شبنم است از وی</p></div>
<div class="m2"><p>دربسته تا به جان دل در لعل دلگشایت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سری که هر دو عالم یک ذره می‌نیابند</p></div>
<div class="m2"><p>جاوید کف گرفته جام جهان نمایت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوباوهٔ جمالت ماه نو است و هر مه</p></div>
<div class="m2"><p>بنهد کله ز خجلت در دامن قبایت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو ابرش نکویی می‌تازی و مه و مهر</p></div>
<div class="m2"><p>چون سایه در رکابت چون ذره در هوایت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا بوی مشک زلفت پر مشک کرد جانم</p></div>
<div class="m2"><p>عطار مشک ریزم از زلف مشک سایت</p></div></div>