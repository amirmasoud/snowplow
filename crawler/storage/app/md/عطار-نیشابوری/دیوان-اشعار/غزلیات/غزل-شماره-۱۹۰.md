---
title: >-
    غزل شمارهٔ ۱۹۰
---
# غزل شمارهٔ ۱۹۰

<div class="b" id="bn1"><div class="m1"><p>بار دگر پیر ما رخت به خمار برد</p></div>
<div class="m2"><p>خرقه بر آتش بسوخت دست به زنار برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دین به تزویر خویش کرد سیه‌رو چنانک</p></div>
<div class="m2"><p>بر سر میدان کفر گوی ز کفار برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نعرهٔ رندان شنید راه قلندر گرفت</p></div>
<div class="m2"><p>کیش مغان تازه کرد قیمت ابرار برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بر دیندار دیر چست قماری بکرد</p></div>
<div class="m2"><p>دین نود ساله را از کف دیندار برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درد خرابات خورد ذوق می عشق یافت</p></div>
<div class="m2"><p>عشق برو غلبه کرد عقل به یکبار برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون می تحقیق خورد در حرم کبریا</p></div>
<div class="m2"><p>پای طبیعت ببست دست به اسرار برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در صف عشاق شد پیشه‌وری پیشه کرد</p></div>
<div class="m2"><p>پیشه‌وری شد چنانک رونق عطار برد</p></div></div>