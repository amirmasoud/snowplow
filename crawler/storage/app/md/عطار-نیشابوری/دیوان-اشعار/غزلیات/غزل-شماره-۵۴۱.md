---
title: >-
    غزل شمارهٔ ۵۴۱
---
# غزل شمارهٔ ۵۴۱

<div class="b" id="bn1"><div class="m1"><p>بی لبت از آب حیوان می‌بسم</p></div>
<div class="m2"><p>بی رخت از ماه تابان می‌بسم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کار روی حسن تو گردان بس است</p></div>
<div class="m2"><p>ز آفتاب چرخ گردان می‌بسم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر گرانم من ز چین زلف تو</p></div>
<div class="m2"><p>از همه چین مشک ارزان می‌بسم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر ندارم آبرویی پیش تو</p></div>
<div class="m2"><p>آب روی از چشم گریان می‌بسم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا لب لعل تو در چشم من است</p></div>
<div class="m2"><p>تا ابد از بحر و از کان می‌بسم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از همه ملک دو عالم یک نفس</p></div>
<div class="m2"><p>با تو گر دستم دهد آن می‌بسم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفته‌ای زارت بخواهم سوختن</p></div>
<div class="m2"><p>آتش شوق تو در جان می‌بسم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زآتش دیگر چه می‌سوزی مرا</p></div>
<div class="m2"><p>چون یک آتش هست سوزان می‌بسم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ساقیا در ده شرابی آشکار</p></div>
<div class="m2"><p>کز دلی پر کفر پنهان می‌بسم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زین همه زنار از تشویر خلق</p></div>
<div class="m2"><p>کرده پنهان زیر خلقان می‌بسم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درد ده تا درد بفزاید مرا</p></div>
<div class="m2"><p>زانکه با دردت ز درمان می‌بسم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غرق دریا گر مرا کرده است نفس</p></div>
<div class="m2"><p>تشنه می‌میرم بیابان می‌بسم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مست لایعقل کن این ساعت مرا</p></div>
<div class="m2"><p>کز دم عقل سخن دان می‌بسم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عقل خود را مصلحت جوید مدام</p></div>
<div class="m2"><p>زین چنین عقل تن آسان می‌بسم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کارساز است او ز پیش و پس ولی</p></div>
<div class="m2"><p>هم ز پایان هم ز پیشان می‌بسم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عقل را بگذار اگر اهل دلی</p></div>
<div class="m2"><p>زانکه چون دل هست از جان می‌بسم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نقد ابن الوقت قلب است ای فرید</p></div>
<div class="m2"><p>دل طلب کز عقل حیران می‌بسم</p></div></div>