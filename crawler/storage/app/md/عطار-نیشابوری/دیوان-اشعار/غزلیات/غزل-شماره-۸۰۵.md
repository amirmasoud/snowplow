---
title: >-
    غزل شمارهٔ ۸۰۵
---
# غزل شمارهٔ ۸۰۵

<div class="b" id="bn1"><div class="m1"><p>ترسا بچهٔ لولی همچون بت روحانی</p></div>
<div class="m2"><p>سرمست برون آمد از دیر به نادانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنار و بت اندر بر ناقوس ومی اندر کف</p></div>
<div class="m2"><p>در داد صلای می از ننگ مسلمانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون نیک نگه کردم در چشم و لب و زلفش</p></div>
<div class="m2"><p>بر تخت دلم بنشست آن ماه به سلطانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگرفتم زنارش در پای وی افتادم</p></div>
<div class="m2"><p>گفتم چکنم جانا گفتا که تو می‌دانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر وصل منت باید ای پیر مرقع پوش</p></div>
<div class="m2"><p>هم خرقه بسوزانی هم قبله بگردانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با ما تو به دیر آیی محراب دگر گیری</p></div>
<div class="m2"><p>وز دفتر عشق ما سطری دو سه بر خوانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اندر بن دیر ما شرطت بود این هر سه</p></div>
<div class="m2"><p>کز خویش برون آیی وز جان و دل فانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می خور تو به دیر اندر تا مست شوی بیخود</p></div>
<div class="m2"><p>کز بی خبری یابی آن چیز که جویانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر گه که شود روشن بر تو که تویی جمله</p></div>
<div class="m2"><p>فریاد اناالحق زن در عالم انسانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عطار ز راه خود برخیز که تا بینی</p></div>
<div class="m2"><p>خود را ز خودی برهان کز خویش تو پنهانی</p></div></div>