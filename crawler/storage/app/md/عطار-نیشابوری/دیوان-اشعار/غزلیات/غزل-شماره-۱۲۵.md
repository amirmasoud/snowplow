---
title: >-
    غزل شمارهٔ ۱۲۵
---
# غزل شمارهٔ ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>درد دل من از حد و اندازه درگذشت</p></div>
<div class="m2"><p>از بس که اشک ریختم آبم ز سر گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پایم ز دست واقعه در قیر غم گرفت</p></div>
<div class="m2"><p>کارم ز جور حادثه از دست درگذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر روی من چو بر جگر من نماند آب</p></div>
<div class="m2"><p>بس سیل‌های خون که ز خون جگر گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر شب ز جور چرخ بلایی دگر رسید</p></div>
<div class="m2"><p>هر دم ز روز عمر به دردی دگر گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواب و خورم نماند و گر قصه گویمت</p></div>
<div class="m2"><p>زان غصه‌ها که بر من بی خواب و خور گذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اشکم به قعر سینهٔ ماهی فرو رسید</p></div>
<div class="m2"><p>آهم از روی آینهٔ ماه درگذشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بر گرفت جان مرا تیر غم چنانک</p></div>
<div class="m2"><p>پیکان به جان رسید وز جان تا به بر گذشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر جان من که رنج و بلایی ندیده بود</p></div>
<div class="m2"><p>چندین بلا و رنج ز دردم بدر گذشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر عمر من اجل چو سحرگاه شام خورد</p></div>
<div class="m2"><p>زان شام آفتاب من اندر سحر گذشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عطار چون که سایهٔ عزت بر او نماند</p></div>
<div class="m2"><p>چون سایه‌ای ز خواری خود در به در گذشت</p></div></div>