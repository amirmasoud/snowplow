---
title: >-
    غزل شمارهٔ ۱۵۵
---
# غزل شمارهٔ ۱۵۵

<div class="b" id="bn1"><div class="m1"><p>عشق تو پرده، صد هزار نهاد</p></div>
<div class="m2"><p>پرده در پرده بی شمار نهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس هر پرده عالمی پر درد</p></div>
<div class="m2"><p>گه نهان و گه آشکار نهاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد جهان خون و صد جهان آتش</p></div>
<div class="m2"><p>پس هر پرده استوار نهاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرده بازی چنان عجایب کرد</p></div>
<div class="m2"><p>که یکی در یکی هزار نهاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پردهٔ دل به یک زمان بگرفت</p></div>
<div class="m2"><p>پرده بر روی اختیار نهاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کرد با دل ز جور آنچه مپرس</p></div>
<div class="m2"><p>جرم بر جان بی قرار نهاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان مضطر چو خاک راهش گشت</p></div>
<div class="m2"><p>روی بر خاک اضطرار نهاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شیرمرد همه جهان بودم</p></div>
<div class="m2"><p>عشق بر دست من نگار نهاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که بداند که دور از رویت</p></div>
<div class="m2"><p>گل روی توام چه خار نهاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دوش آمد خیال تو سحری</p></div>
<div class="m2"><p>تا مرا در هزار کار نهاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همچو لاله فکند در خونم</p></div>
<div class="m2"><p>بر دلم داغ انتظار نهاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سر من همچو شمع باز برید</p></div>
<div class="m2"><p>پس بیاورد و در کنار نهاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون همی بازگشت از بر من</p></div>
<div class="m2"><p>درد هجرم به یادگار نهاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر زمان عقبه‌ای ز درد فراق</p></div>
<div class="m2"><p>پیش عطار دل فگار نهاد</p></div></div>