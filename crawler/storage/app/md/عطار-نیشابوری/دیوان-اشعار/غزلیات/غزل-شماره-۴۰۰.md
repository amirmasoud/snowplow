---
title: >-
    غزل شمارهٔ ۴۰۰
---
# غزل شمارهٔ ۴۰۰

<div class="b" id="bn1"><div class="m1"><p>گر ز سر عشق او داری خبر</p></div>
<div class="m2"><p>جان بده در عشق و در جانان نگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون کسی از عشق هرگز جان نبرد</p></div>
<div class="m2"><p>گر تو هم از عاشقانی جان مبر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر ز جان خویش سیری الصلا</p></div>
<div class="m2"><p>ور همی ترسی تو از جان الحذر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق دریایی است قعرش ناپدید</p></div>
<div class="m2"><p>آب دریا آتش و موجش گهر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوهرش اسرار و هر سری ازو</p></div>
<div class="m2"><p>سالکی را سوی معنی راهبر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرکشی از هر دو عالم همچو موی</p></div>
<div class="m2"><p>گر سر مویی درین یابی خبر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوش مست و خفته بودم نیمشب</p></div>
<div class="m2"><p>کوفتاد آن ماه را بر من گذر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دید روی زرد ما در ماهتاب</p></div>
<div class="m2"><p>کرد روی زرد ما از اشک تر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رحمش آمد شربت وصلم بداد</p></div>
<div class="m2"><p>یافت یک یک موی من جانی دگر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرچه مست افتاده بودم زان شراب</p></div>
<div class="m2"><p>گشت یک یک موی بر من دیده‌ور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در رخ آن آفتاب هر دو کون</p></div>
<div class="m2"><p>مست و لایعقل همی کردم نظر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گرچه بود از عشق جانم پر سخن</p></div>
<div class="m2"><p>یک نفس نامد زبانم کارگر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خفته و مستم گرفت آن ماه روی</p></div>
<div class="m2"><p>لاجرم ماندم چنین بی خواب و خور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گاه می‌مردم گهی می‌زیستم</p></div>
<div class="m2"><p>در میان سوز چون شمع سحر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عاقبت بانگی برآمد از دلم</p></div>
<div class="m2"><p>موج‌ها برخاست از خون جگر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون از آن حالت گشادم چشم باز</p></div>
<div class="m2"><p>نه ز جانان نام دیدم نه اثر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>من ز درد و حسرت و شوق و طلب</p></div>
<div class="m2"><p>می‌زدم چون مرغ بسمل بال و پر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هاتفی آواز داد از گوشه‌ای</p></div>
<div class="m2"><p>کای ز دستت رفته مرغی معتبر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خاک بر دنبال او بایست کرد</p></div>
<div class="m2"><p>تا نرفتی او ازین گلخن به در</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تن فرو ده آب در هاون مکوب</p></div>
<div class="m2"><p>در قفس تا کی کنی باد ای پسر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بی نیازی بین که اندر اصل هست</p></div>
<div class="m2"><p>خواه مطرب باش و خواهی نوحه‌گر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>این کمان هرگز به بازوی تو نیست</p></div>
<div class="m2"><p>جان خود می‌سوز و حیران می‌نگر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ماندی ای عطار در اول قدم</p></div>
<div class="m2"><p>کی توانی برد این وادی به سر</p></div></div>