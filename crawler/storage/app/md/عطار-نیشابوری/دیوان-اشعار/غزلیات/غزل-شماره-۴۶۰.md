---
title: >-
    غزل شمارهٔ ۴۶۰
---
# غزل شمارهٔ ۴۶۰

<div class="b" id="bn1"><div class="m1"><p>صبح برانداخت نقاب ای غلام</p></div>
<div class="m2"><p>می‌ده و برخیز ز خواب ای غلام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو گلم بر سر آتش نشاند</p></div>
<div class="m2"><p>شوق شراب چو گلاب ای غلام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی نمکی چند کنی باده نوش</p></div>
<div class="m2"><p>وز جگرم خواه کباب ای غلام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دور بگردان و شتابی بکن</p></div>
<div class="m2"><p>چند کند عمر شتاب ای غلام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان من سوخته دل را دمی</p></div>
<div class="m2"><p>زنده کن از جام شراب ای غلام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آب حیات است می و من چو شمع</p></div>
<div class="m2"><p>مرده دلم بی می ناب ای غلام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از قدح باده دلم زنده کن</p></div>
<div class="m2"><p>تا برهد جان ز عذاب ای غلام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون دل عطار ز تو تافته است</p></div>
<div class="m2"><p>تافته را نیز متاب ای غلام</p></div></div>