---
title: >-
    غزل شمارهٔ ۲۳۳
---
# غزل شمارهٔ ۲۳۳

<div class="b" id="bn1"><div class="m1"><p>گرچه ز تو هر روزم صد فتنه دگر خیزد</p></div>
<div class="m2"><p>در عشق تو هر ساعت دل شیفته‌تر خیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لعلت که شکر دارد حقا که یقینم من</p></div>
<div class="m2"><p>گر در همه خوزستان زین شیوه شکر خیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرگه که چو چوگانی زلف تو به پای افتد</p></div>
<div class="m2"><p>دل در خم زلف تو چون گوی به سر خیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتی به بر سیمین زر از تو برانگیزم</p></div>
<div class="m2"><p>آخر ز چو من مفلس دانی که چه زر خیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قلبی است مرا در بر رویی است مرا چون زر</p></div>
<div class="m2"><p>این قلب که برگیرد زان وجه چه برخیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا در تو نظر کردم رسوای جهان گشتم</p></div>
<div class="m2"><p>آری همه رسوایی اول ز نظر خیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتی چو منی بگزین تا من برهم از تو</p></div>
<div class="m2"><p>آری چو تو بگزینم، گر چون تو دگر خیزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیچاره دلم بی کس کز شوق رخت هر شب</p></div>
<div class="m2"><p>بر خاک درت افتد در خون جگر خیزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو خاک توام آخر خونم به چه می‌ریزی</p></div>
<div class="m2"><p>از خون چو من خاکی چه خیزد اگر خیزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عطار اگر روزی رخ تازه بود بی تو</p></div>
<div class="m2"><p>آن تازگی رویش از دیدهٔ‌تر خیزد</p></div></div>