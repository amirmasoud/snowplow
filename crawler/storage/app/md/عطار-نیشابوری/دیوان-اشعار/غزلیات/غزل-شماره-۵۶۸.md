---
title: >-
    غزل شمارهٔ ۵۶۸
---
# غزل شمارهٔ ۵۶۸

<div class="b" id="bn1"><div class="m1"><p>قصهٔ عشق تو از بر چون کنم</p></div>
<div class="m2"><p>وصل را از وعده باور چون کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان ندارم، بار جانان چون کشم</p></div>
<div class="m2"><p>دل ندارم، قصد دلبر چون کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حلقهٔ زلف توام چون بند کرد</p></div>
<div class="m2"><p>مانده‌ام چون حلقه بر در چون کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون تو خورشیدی و من چون سایه‌ام</p></div>
<div class="m2"><p>خویش را با تو برابر چون کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفته‌ای تو پای سر کن در رهم</p></div>
<div class="m2"><p>می ندانم پای از سر چون کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفته بودی عزم من کن مردوار</p></div>
<div class="m2"><p>برده‌ام صد بار کیفر چون کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عزم کردم وصل تو جانم بسوخت</p></div>
<div class="m2"><p>مانده‌ام بی عزم مضطر چون کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون ندارد ذره‌ای وصل تو روی</p></div>
<div class="m2"><p>وصل روی تو میسر چون کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کشتی عمرم به غرقاب اوفتاد</p></div>
<div class="m2"><p>مفلسم از صبر لنگر چون کنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چشم بگشادم که بینم روی تو</p></div>
<div class="m2"><p>گشت چشمم غرق گوهر چون کنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لب گشادم تا کنم وصف تو شرح</p></div>
<div class="m2"><p>نیست آن کار سخنور چون کنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفته‌ای بردوز چشم و لب ببند</p></div>
<div class="m2"><p>چون نه خشکم ماند و نه تر چون کنم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روح می‌خواهی برای یک شکر</p></div>
<div class="m2"><p>آن عوض با این محقر چون کنم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفته‌ام صد باره ترک روح خویش</p></div>
<div class="m2"><p>چون تو هستی روح پرور چون کنم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون به یک دستم همی داری نگاه</p></div>
<div class="m2"><p>می‌زیم از دست دیگر چون کنم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هرگز از عطار حرفی نشنوی</p></div>
<div class="m2"><p>قصه‌ای با تو مقرر چون کنم</p></div></div>