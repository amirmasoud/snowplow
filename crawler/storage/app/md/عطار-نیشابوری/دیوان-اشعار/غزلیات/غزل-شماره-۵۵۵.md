---
title: >-
    غزل شمارهٔ ۵۵۵
---
# غزل شمارهٔ ۵۵۵

<div class="b" id="bn1"><div class="m1"><p>من این دانم که مویی می ندانم</p></div>
<div class="m2"><p>بجز مرگ آرزویی می ندانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا مبشول مویی زانکه در عشق</p></div>
<div class="m2"><p>چنان غرقم که مویی می ندانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنین رنگی که بر من سایه افکند</p></div>
<div class="m2"><p>ز دو کونش رکویی می ندانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنانم در خم چوگان فگنده</p></div>
<div class="m2"><p>که پا و سر چو گویی می ندانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسی بر بوی سر عشق رفتم</p></div>
<div class="m2"><p>نبردم بوی و بویی می ندانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسی هر کار را روی است از ما</p></div>
<div class="m2"><p>به از تسلیم رویی می ندانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به از تسلیم و صبر و درد و خلوت</p></div>
<div class="m2"><p>درین ره چارسویی می ندانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شدم در کوی اهل دل چو خاکی</p></div>
<div class="m2"><p>که به زین کوی کویی می ندانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلم را راه جوی عشق کردم</p></div>
<div class="m2"><p>که به زو راه جویی می ندانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درون دل بسی خود را بجستم</p></div>
<div class="m2"><p>که به زین جست و جویی می ندانم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به خون دل بشستم دست از جان</p></div>
<div class="m2"><p>که به زین شست و شویی می ندانم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بسی این راز نادانسته گفتم</p></div>
<div class="m2"><p>که به زین گفت و گویی می ندانم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو کردم جوی چشمان همچو عطار</p></div>
<div class="m2"><p>که به زین آب جویی می ندانم</p></div></div>