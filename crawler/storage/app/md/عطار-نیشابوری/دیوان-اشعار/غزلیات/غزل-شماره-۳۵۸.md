---
title: >-
    غزل شمارهٔ ۳۵۸
---
# غزل شمارهٔ ۳۵۸

<div class="b" id="bn1"><div class="m1"><p>گر نه از خاک درت باد صبا می‌آید</p></div>
<div class="m2"><p>صبحدم مشک‌فشان پس ز کجا می‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای جگرسوختگان عهد کهن تازه کنید</p></div>
<div class="m2"><p>که گل تازه به دلداری ما می‌آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل تر را ز دم صبح به شام اندازد</p></div>
<div class="m2"><p>این چنین گرم که گلگون صبا می‌آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هواداری گل ذره صفت در رقص آی</p></div>
<div class="m2"><p>کم ز ذره نه‌ای او هم ز هوا می‌آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا گذر کرد نسیم سحری بر در دوست</p></div>
<div class="m2"><p>نوش‌دارو ز دم زهرگیا می‌آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عمر و عیش از سر صد ناز و طرب می‌گذرد</p></div>
<div class="m2"><p>بلبل و گل ز سر برگ و نوا می‌آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بوی بر مشک ختا از دم عطار هوا</p></div>
<div class="m2"><p>زانکه ناکست کزو بوی خطا می‌آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بلبل شیفته را بی گل تر عمر عزیز</p></div>
<div class="m2"><p>قدری فوت شد از بهر قضا می‌آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بلبل سوخته را در جگر آب است که نیست</p></div>
<div class="m2"><p>گل سیراب چنین تشنه چرا می‌آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گل که غنچه به بر از خون دلش پرورده است</p></div>
<div class="m2"><p>از کله‌داری او بسته قبا می‌آید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از بنفشه به عجب مانده‌ام کز چه سبب</p></div>
<div class="m2"><p>روز طفلی به چمن پشت دوتا می‌آید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نسترن کوتهی عمر مگر می‌داند</p></div>
<div class="m2"><p>زان چنین بی سر و بن بر سر پا می‌آید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر شکر خندهٔ گل درد دل کس نگذاشت</p></div>
<div class="m2"><p>دم عطار کزو بوی دوا می‌آید</p></div></div>