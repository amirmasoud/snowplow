---
title: >-
    غزل شمارهٔ ۵۳۷
---
# غزل شمارهٔ ۵۳۷

<div class="b" id="bn1"><div class="m1"><p>زیر بار ستمت می‌میرم</p></div>
<div class="m2"><p>روی در روی غمت می‌میرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شغل عشق تو چنان کرد مرا</p></div>
<div class="m2"><p>کایمن از مدح و ذمت می‌میرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زندهٔ بی سر از آنم که چو شمع</p></div>
<div class="m2"><p>سر خود بر قدمت می‌میرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حرمت گرچه مرا روی نمود</p></div>
<div class="m2"><p>روی سوی حرمت می‌میرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آستین چند فشانی بر من</p></div>
<div class="m2"><p>که میان حشمت می‌میرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آستینت چو علم کرد مرا</p></div>
<div class="m2"><p>زار زیر علمت می‌میرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا شدم زنده‌دل از خط خوشت</p></div>
<div class="m2"><p>سرنگون چون قلمت می‌میرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به ستم رزق هرگه که دهی</p></div>
<div class="m2"><p>می خورم وز ستمت می‌میرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دم عیسی است تورا وین عجب است</p></div>
<div class="m2"><p>تا چرا من ز دمت می‌میرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من بمیرم ز تو روزی صد بار</p></div>
<div class="m2"><p>تا نگویی که کمت می‌میرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لیک چون لعل توام زنده کند</p></div>
<div class="m2"><p>زین قدم دم به دمت می‌میرم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>درده از جام جمت آب حیات</p></div>
<div class="m2"><p>هین که بی جام جمت می‌میرم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بی تو گر زنده بماندم نفسی</p></div>
<div class="m2"><p>هر نفس لاجرمت می‌میرم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کرم عشق تو دیده است فرید</p></div>
<div class="m2"><p>بر امید کرمت می‌میرم</p></div></div>