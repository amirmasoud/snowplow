---
title: >-
    غزل شمارهٔ ۲۱۴
---
# غزل شمارهٔ ۲۱۴

<div class="b" id="bn1"><div class="m1"><p>چو طوطی خط او پر بر آورد</p></div>
<div class="m2"><p>جهان حسن در زیر پر آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خوش رنگی رخش عالم برافروخت</p></div>
<div class="m2"><p>ز سرسبزی خطش رنگی بر آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لب چون لعلش از چشمم گهر ریخت</p></div>
<div class="m2"><p>بر چون سیمش از رویم زر آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل از شرم رخ او خشک لب گشت</p></div>
<div class="m2"><p>ز خشکی ای عجب دامن تر آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دهان تنگ او یارب چه چشمه است</p></div>
<div class="m2"><p>که از خنده به دریا گوهر آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر زلفش شکار دلبری را</p></div>
<div class="m2"><p>هزاران حلقه در یکدیگر آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فلک زان چنبری آمد که زلفش</p></div>
<div class="m2"><p>فلک را نیز سر در چنبر آورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فلک در پای او چون گوی می‌گشت</p></div>
<div class="m2"><p>چو چوگانش به خدمت بر سر آورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو شد عطار لالای در او</p></div>
<div class="m2"><p>ز زلفش خادمی را عنبر آورد</p></div></div>