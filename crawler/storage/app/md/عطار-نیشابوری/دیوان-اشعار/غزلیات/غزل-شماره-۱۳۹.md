---
title: >-
    غزل شمارهٔ ۱۳۹
---
# غزل شمارهٔ ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>ای زلف تو دام و دانه خالت</p></div>
<div class="m2"><p>هر صید که می‌کنی حلالت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید دراوفتاده پیوست</p></div>
<div class="m2"><p>در حلقهٔ دام شب مثالت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچون نقطی سیه پدیدار</p></div>
<div class="m2"><p>بر چهرهٔ آفتاب خالت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل فتنهٔ طرهٔ سیاهت</p></div>
<div class="m2"><p>جان تشنهٔ چشمهٔ زلالت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از عالم حسن دایه لطف</p></div>
<div class="m2"><p>آورده به صد هزار سالت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رخ زرد و کبود جامه خورشید</p></div>
<div class="m2"><p>سرگشتهٔ ذرهٔ وصالت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو خفته و اختران همه شب</p></div>
<div class="m2"><p>مبهوت بمانده در جمالت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو ماه تمامی و عجب آنک</p></div>
<div class="m2"><p>انگشت نمای شد هلالت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرغی عجبی که می‌نگنجد</p></div>
<div class="m2"><p>در صحن سپهر پر و بالت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون در تو توان رسید چون کس</p></div>
<div class="m2"><p>هرگز نرسید در خیالت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پی گم کردی چنانکه هرگز</p></div>
<div class="m2"><p>کس پی نبرد به هیچ حالت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خواهد که بسی بگوید از تو</p></div>
<div class="m2"><p>عطار ولی بود ملالت</p></div></div>