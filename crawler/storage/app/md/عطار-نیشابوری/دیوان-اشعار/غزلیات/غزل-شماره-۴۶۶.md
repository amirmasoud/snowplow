---
title: >-
    غزل شمارهٔ ۴۶۶
---
# غزل شمارهٔ ۴۶۶

<div class="b" id="bn1"><div class="m1"><p>تا دیده‌ام رخ تو کم جان گرفته‌ام</p></div>
<div class="m2"><p>اما هزار جان عوض آن گرفته‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون ز لبت نبود مرا روی یک شکر</p></div>
<div class="m2"><p>ای بس که پشت دست به دندان گرفته‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا آب زندگانی تو دیده‌ام ز دور</p></div>
<div class="m2"><p>دور از رخ تو مرگ خود آسان گرفته‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون توشهٔ وصال توام دست می نداد</p></div>
<div class="m2"><p>در پا فتاده گوشهٔ هجران گرفته‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون بر کمان ابروی تو تیر دیده‌ام</p></div>
<div class="m2"><p>گر خواست وگرنه کم جان گرفته‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آوازهٔ لب تو ز خلقی شنیده‌ام</p></div>
<div class="m2"><p>زان تشنه راه چشمهٔ حیوان گرفته‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن راه چشمه در ظلمات دو زلف توست</p></div>
<div class="m2"><p>یارب رهی چه دور و پریشان گرفته‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون خشک‌سال وصل تو در کون دیده‌ام</p></div>
<div class="m2"><p>از ابر چشم عادت طوفان گرفته‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرچه ز چشم خاست مرا عشق تو چو اشک</p></div>
<div class="m2"><p>این جرم نیز بر دل بریان گرفته‌ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برهم دریده پرده ز تر دامنی چشم</p></div>
<div class="m2"><p>کو را به دست ابر گریبان گرفته‌ام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتی که من به کار تو سر تیز می‌کنم</p></div>
<div class="m2"><p>کین پر دلی ز زلف زره‌سان گرفته‌ام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خونی گشاد از همه سر تیزی توام</p></div>
<div class="m2"><p>وین تجربه ز ناوک مژگان گرفته‌ام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون تو ز ناز و کبر نگنجی به شهر در</p></div>
<div class="m2"><p>من شهر ترک گفته بیابان گرفته‌ام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عطار تا که از تو چو یوسف جدا افتاد</p></div>
<div class="m2"><p>یعقوب‌وار کلبهٔ احزان گرفته‌ام</p></div></div>