---
title: >-
    غزل شمارهٔ ۷۵۶
---
# غزل شمارهٔ ۷۵۶

<div class="b" id="bn1"><div class="m1"><p>جانا دلم ببردی در قعر جان نشستی</p></div>
<div class="m2"><p>من بر کنار رفتم تو در میان نشستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر جان ز من ربودی الحمدلله ای جان</p></div>
<div class="m2"><p>چون تو بجای جانم بر جای جان نشستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه تو را نبینم بی تو جهان نبینم</p></div>
<div class="m2"><p>یعنی تو نور چشمی در چشم از آن نشستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون خواستی که عاشق در خون دل بگردد</p></div>
<div class="m2"><p>در خون دل نشاندی در لامکان نشستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من چون به خون نگردم از شوق تو چو تنها</p></div>
<div class="m2"><p>در زیر خدر عزت چندان نهان نشستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتی مرا چو جویی در جان خویش یابی</p></div>
<div class="m2"><p>چون جویمت که در جان بس بی نشان نشستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برخاست ز امتحانت یکبارگی دل من</p></div>
<div class="m2"><p>من خود کیم که با من در امتحان نشستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا من تو را بدیدم دیگر جهان ندیدم</p></div>
<div class="m2"><p>گم شد جهان ز چشمم تا در جهان نشستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عطار عاشق از تو زین بیش صبر نکند</p></div>
<div class="m2"><p>نبود روا که چندین بی عاشقان نشستی</p></div></div>