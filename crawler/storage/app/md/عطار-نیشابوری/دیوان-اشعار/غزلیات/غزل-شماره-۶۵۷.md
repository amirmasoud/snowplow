---
title: >-
    غزل شمارهٔ ۶۵۷
---
# غزل شمارهٔ ۶۵۷

<div class="b" id="bn1"><div class="m1"><p>چو دریا شور در جانم میفکن</p></div>
<div class="m2"><p>ز سودا در بیابانم میفکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو پر پشهٔ وصلت ندیدم</p></div>
<div class="m2"><p>به پای پیل هجرانم میفکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دست خویش در پای خودم کش</p></div>
<div class="m2"><p>به دست و پای دورانم میفکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دشواری به دست آید چو من کس</p></div>
<div class="m2"><p>چنین از دست آسانم میفکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر از تشنگی چون شمع مردم</p></div>
<div class="m2"><p>به سیرابی طوفانم میفکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به چشم او کز ابروی کمان کش</p></div>
<div class="m2"><p>به دل در تیر مژگانم میفکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زره چون در نمی‌پوشیم از زلف</p></div>
<div class="m2"><p>میان تیربارانم میفکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو پیچ و تاب در زلف تو زیباست</p></div>
<div class="m2"><p>به جان تو که در جانم میفکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو پایم نیست با چوگان زلفت</p></div>
<div class="m2"><p>چو گویی پیش چوگانم میفکن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو من جمعیت از زلف تو دارم</p></div>
<div class="m2"><p>چو زلف خود پریشانم میفکن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خط آوردی و جان می‌خواهی از من</p></div>
<div class="m2"><p>ز خط خود به دیوانم میفکن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو شد خاک رهت عطار حیران</p></div>
<div class="m2"><p>به خاک راه حیرانم میفکن</p></div></div>