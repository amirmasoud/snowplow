---
title: >-
    غزل شمارهٔ ۴۳۷
---
# غزل شمارهٔ ۴۳۷

<div class="b" id="bn1"><div class="m1"><p>ترسا بچهٔ شکر لبم دوش</p></div>
<div class="m2"><p>صد حلقهٔ زلف در بناگوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد پیر قوی به حلقه می‌داشت</p></div>
<div class="m2"><p>زان حلقهٔ زلف حلقه در گوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آمد بر من شراب در دست</p></div>
<div class="m2"><p>گفتا که به یاد من کن این نوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در پرده اگر حریف مایی</p></div>
<div class="m2"><p>چون می‌نوشی خموش و مخروش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زیرا که دلی نگشت گویا</p></div>
<div class="m2"><p>تا مرد زبان نکرد خاموش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل چون بشنود این سخن زود</p></div>
<div class="m2"><p>ناخورده شراب گشت مدهوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون بستدم آن شراب و خوردم</p></div>
<div class="m2"><p>در سینهٔ من فتاد صد جوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دادم همه نام و ننگ بر باد</p></div>
<div class="m2"><p>کردم همه نیک و بد فراموش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از دست بشد مرا دل و جان</p></div>
<div class="m2"><p>وز پای درآمدم تن و توش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یک قطره از آن شراب مشکل</p></div>
<div class="m2"><p>آورد دو عالمم در آغوش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یک ذره سواد فقر در تافت</p></div>
<div class="m2"><p>شد هر دو جهان از آن سیه‌پوش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جانم ز سر دو کون برخاست</p></div>
<div class="m2"><p>در شیوهٔ فقر شد وفا کوش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر که بخرد به جان و دل فقر</p></div>
<div class="m2"><p>بر جان و دلش دو کون بفروش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ور دین تو نیست دین عطار</p></div>
<div class="m2"><p>کفر آیدت این حدیث منیوش</p></div></div>