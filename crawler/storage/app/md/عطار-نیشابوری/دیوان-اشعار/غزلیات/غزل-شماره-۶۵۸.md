---
title: >-
    غزل شمارهٔ ۶۵۸
---
# غزل شمارهٔ ۶۵۸

<div class="b" id="bn1"><div class="m1"><p>زلف به انگشت پریشان مکن</p></div>
<div class="m2"><p>روی بدان خوبی پنهان مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طرهٔ مشکین سیه رنگ را</p></div>
<div class="m2"><p>سایهٔ خورشید درافشان مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از سر بیداد سر سروران</p></div>
<div class="m2"><p>در سر آن سرو خرامان مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق دل سوخته را دست گیر</p></div>
<div class="m2"><p>جان و دلم بی سر و سامان مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون بر ما آمده‌ای یک زمان</p></div>
<div class="m2"><p>حال دل خسته پریشان مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در بر ما یک نفس آرام گیر</p></div>
<div class="m2"><p>از بر ما قصد شبستان مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی رخ خود عالم همچون بهشت</p></div>
<div class="m2"><p>بر من دل سوخته زندان مکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر تو چو عطار جفایی نکرد</p></div>
<div class="m2"><p>آنچه ز تو آن نسزد آن مکن</p></div></div>