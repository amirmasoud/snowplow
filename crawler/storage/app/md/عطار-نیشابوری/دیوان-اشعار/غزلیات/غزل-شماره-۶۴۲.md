---
title: >-
    غزل شمارهٔ ۶۴۲
---
# غزل شمارهٔ ۶۴۲

<div class="b" id="bn1"><div class="m1"><p>کافری است از عشق دل برداشتن</p></div>
<div class="m2"><p>اقتدا در دین به کافر داشتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ملا تحقیق کردن آشکار</p></div>
<div class="m2"><p>در خلا دین مزور داشتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از برون گفتن که شیطان گمره است</p></div>
<div class="m2"><p>وز درونش پیر و رهبر داشتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون درآید تیرباران بلا</p></div>
<div class="m2"><p>در هزیمت دامن تر داشتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کار مردان چیست بیکار آمدن</p></div>
<div class="m2"><p>پس به هر دم کار دیگر داشتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاک ره بر خود نمایان ریختن</p></div>
<div class="m2"><p>خویشتن را خاک این در داشتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غرقهٔ این بحر گشتن ناامید</p></div>
<div class="m2"><p>وانگهی امید گوهر داشتن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دست بر سر پای در گل آمدن</p></div>
<div class="m2"><p>خشت بالین، خاک بستر داشتن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دام تن در راه معنی سوختن</p></div>
<div class="m2"><p>مرغ جان بی‌بال و بی‌پر داشتن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر سری کان از تو سر برمی‌زند</p></div>
<div class="m2"><p>از برای تیغ و خنجر داشتن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون فلک خورشید را بر سر کشید</p></div>
<div class="m2"><p>کی تواند پای بر سر داشتن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پای بر سر نه که اینجا کافری است</p></div>
<div class="m2"><p>سر برای تاج و افسر داشتن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همچو عطار این سگ درنده را</p></div>
<div class="m2"><p>زهر دادن یا مسخر داشتن</p></div></div>