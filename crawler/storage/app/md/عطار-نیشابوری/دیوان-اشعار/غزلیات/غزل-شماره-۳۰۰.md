---
title: >-
    غزل شمارهٔ ۳۰۰
---
# غزل شمارهٔ ۳۰۰

<div class="b" id="bn1"><div class="m1"><p>عاشقانی کز نسیم دوست جان می‌پرورند</p></div>
<div class="m2"><p>جمله وقت سوختن چون عود اندر مجمرند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فارغند از عالم و از کار عالم روز و شب</p></div>
<div class="m2"><p>والهٔ راهی شگرف و غرق بحری منکرند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که در عالم دویی می‌بیند آن از احولی است</p></div>
<div class="m2"><p>زانکه ایشان در دو عالم جز یکی را ننگرند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر صفتشان برگشاید پردهٔ صورت ز روی</p></div>
<div class="m2"><p>از ثری تا عرش اندر زیر گامی بسپرند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنچه می‌جویند بیرون از دوعالم سالکان</p></div>
<div class="m2"><p>خویش را یابند چون این پرده از هم بردرند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر دو عالم تخت خود بینند از روی صفت</p></div>
<div class="m2"><p>لاجرم در یک نفس از هر دو عالم بگذرند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از ره صورت ز عالم ذره‌ای باشند و بس</p></div>
<div class="m2"><p>لیکن از راه صفت عالم به چیزی نشمرند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فوق ایشان است در صورت دو عالم در نظر</p></div>
<div class="m2"><p>لیکن ایشان در صفت از هر دو عالم برترند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عالم صغری به صورت عالم کبری به اصل</p></div>
<div class="m2"><p>اصغرند از صورت و از راه معنی اکبرند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جمله غواصند در دریای وحدت لاجرم</p></div>
<div class="m2"><p>گرچه بسیارند لیکن در صفت یک گوهرند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روز و شب عطار را از بهر شرح راه عشق</p></div>
<div class="m2"><p>هم به همت دل دهند و هم به دل جان پرورند</p></div></div>