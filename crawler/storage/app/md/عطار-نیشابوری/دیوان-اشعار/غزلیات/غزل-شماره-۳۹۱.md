---
title: >-
    غزل شمارهٔ ۳۹۱
---
# غزل شمارهٔ ۳۹۱

<div class="b" id="bn1"><div class="m1"><p>در عشق تو گم شدم به یکبار</p></div>
<div class="m2"><p>سرگشته همی دوم فلک‌وار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نقطهٔ دل به جای بودی</p></div>
<div class="m2"><p>سرگشته نبودمی چو پرگار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل رفت ز دست و جان برآن است</p></div>
<div class="m2"><p>کز پی برود زهی سر و کار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای ساقی آفتاب پیکر</p></div>
<div class="m2"><p>بر جانم ریز جام خون‌خوار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خون جگرم به جام بفروش</p></div>
<div class="m2"><p>کز جانم جام را خریدار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جامی پر کن نه بیش و نه کم</p></div>
<div class="m2"><p>زیرا که نه مستم و نه هشیار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در پای فتادم از تحیر</p></div>
<div class="m2"><p>در دست تحیرم به مگذار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جامی دارم که در حقیقت</p></div>
<div class="m2"><p>انکار نمی‌کند ز اقرار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نفسی دارم که از جهالت</p></div>
<div class="m2"><p>اقرار نمی‌دهد ز انکار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می‌نتوان بود بیش ازین نیز</p></div>
<div class="m2"><p>در صحبت نفس و جان گرفتار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا چند خورم ز نفس و جان خون</p></div>
<div class="m2"><p>تا کی باشم به زاری زار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>درماندهٔ این وجود خویشم</p></div>
<div class="m2"><p>پاکم به عدم رسان به یکبار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون با عدمم نمی‌رسانی</p></div>
<div class="m2"><p>از روی وجود پرده بردار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا کشف شود در آن وجودم</p></div>
<div class="m2"><p>اسرار دو کون و علم اسرار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>من نعره‌زنان چو مرغ در دام</p></div>
<div class="m2"><p>بیرون جهم از مضیق پندار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هرگاه که این میسرم شد</p></div>
<div class="m2"><p>پر مشک شود جهان ز عطار</p></div></div>