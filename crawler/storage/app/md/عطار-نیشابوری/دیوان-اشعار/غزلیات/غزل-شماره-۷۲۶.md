---
title: >-
    غزل شمارهٔ ۷۲۶
---
# غزل شمارهٔ ۷۲۶

<div class="b" id="bn1"><div class="m1"><p>در راه تو مردانند از خویش نهان مانده</p></div>
<div class="m2"><p>بی جسم و جهت گشته بی نام و نشان مانده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در قبهٔ متواری لایعرفهم غیری</p></div>
<div class="m2"><p>محبوب ازل بوده محجوب جهان مانده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کسوت کادالفقر از کفر زده خیمه</p></div>
<div class="m2"><p>در زیر سوادالوجه از خلق نهان مانده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قومی نه نکو نه بد نه با خود و نه بیخود</p></div>
<div class="m2"><p>نه بوده و نه نابوده نی مانده عیان مانده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در عالم ما و من نی ما شده و نی من</p></div>
<div class="m2"><p>در کون و مکان با تو بی کون و مکان مانده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جانشان به حقیقت کل تنشان به شریعت هم</p></div>
<div class="m2"><p>هم جان همه و هم تن نی این و نه آن مانده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون دایره سرگردان چون نقطه قدم محکم</p></div>
<div class="m2"><p>صد دایره عرش آسا در نقطهٔ جان مانده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون عین بقا دیده از خویش فنا گشته</p></div>
<div class="m2"><p>در بحر یقین غرقه در تیه گمان مانده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فارش از سر هر مویی صد گونه سخن گفته</p></div>
<div class="m2"><p>اما همه از گنگی بی کام و زبان مانده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جمله ز گران عقلی در سیر سبک بوده</p></div>
<div class="m2"><p>وآنگه ز سبک روحی در بار گران مانده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صد عالم بی پایان از خوف و رجا بیرون</p></div>
<div class="m2"><p>از خوف شده مویی در خط امان مانده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بشکسته دلیران را از چست سواری پشت</p></div>
<div class="m2"><p>مرکب شده ناپیدا در دست عنان مانده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بفروخته از همت دو کون به یک نان خوش</p></div>
<div class="m2"><p>وز ناخوشی عالم وقوف دو نان مانده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن کس که نزاد است او از مادر خود هرگز</p></div>
<div class="m2"><p>ایشان همه هم با تو از فقر چنان مانده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا راه چنین قومی عطار بیان کرده</p></div>
<div class="m2"><p>جانش به لب افتاده دل در خفقان مانده</p></div></div>