---
title: >-
    غزل شمارهٔ ۱۳۱
---
# غزل شمارهٔ ۱۳۱

<div class="b" id="bn1"><div class="m1"><p>دل کمال از لعل میگون تو یافت</p></div>
<div class="m2"><p>جان حیات از نطق موزون تو یافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر ز چشمت خسته‌ای آمد به تیر</p></div>
<div class="m2"><p>زنده شد چون در مکنون تو یافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا فسونت کرد چشم ساحرت</p></div>
<div class="m2"><p>جامه پر کژدم ز افسون تو یافت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سخت‌تر از سنگ نتوان آمدن</p></div>
<div class="m2"><p>لعل بین یعنی دلش خون تو یافت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا فشاندی زلف و بگشادی دهن</p></div>
<div class="m2"><p>عقل خود را مست و مجنون تو یافت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ملک کسری در سر زلف تو دید</p></div>
<div class="m2"><p>جام جم در لعل گلگون تو یافت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاف تا قاف جهان یکسر بگشت</p></div>
<div class="m2"><p>کاف کفر از زلف چون نون تو یافت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جمله را صدباره فی‌الجمله بدید</p></div>
<div class="m2"><p>هیچش آمد هرچه بیرون تو یافت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا دل عطار عالم کم گرفت</p></div>
<div class="m2"><p>رونق از حسن در افزون تو یافت</p></div></div>