---
title: >-
    غزل شمارهٔ ۱۲۷
---
# غزل شمارهٔ ۱۲۷

<div class="b" id="bn1"><div class="m1"><p>ای دلم مست چشمهٔ نوشت</p></div>
<div class="m2"><p>در خطم از خط سیه پوشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باد سرسبزی خطت که به لطف</p></div>
<div class="m2"><p>سر برون زد ز چشمهٔ نوشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حلقه در گوش کرد خلق را</p></div>
<div class="m2"><p>حلقهٔ زلف بر بناگوشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو من صد هزار سرگشته</p></div>
<div class="m2"><p>حلقه در گوش حلقهٔ گوشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گشت معلوم من که جان نبرد</p></div>
<div class="m2"><p>دلم از طرهٔ سیه پوشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو به جان و دلی جفا کوشم</p></div>
<div class="m2"><p>من به جان و دلم وفا کوشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشوه مفروش زانکه من پس ازین</p></div>
<div class="m2"><p>نخرم نیز خواب خرگوشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یاد کن از کسی که در همه عمر</p></div>
<div class="m2"><p>نکند لحظه‌ای فراموشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مست از آنم چنین که در بر خویش</p></div>
<div class="m2"><p>مست در خواب دیده‌ام دوشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بو که تعبیر خوابم آن باشد</p></div>
<div class="m2"><p>که شوم امشبی هم آغوشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل عطار باده ناخورده</p></div>
<div class="m2"><p>تا قیامت بمانده مدهوشت</p></div></div>