---
title: >-
    غزل شمارهٔ ۳۵
---
# غزل شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>طرقوا یا عاشقان کین منزل جانان ماست</p></div>
<div class="m2"><p>زانچه وصل و هجر او هم درد و هم درمان ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راه ده ما را اگر چه مفلسان حضرتیم</p></div>
<div class="m2"><p>آیت قل یا عبادی آمده در شان ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیستم اینجا مقیم ای دوستان بر رهگذر</p></div>
<div class="m2"><p>یک دو روزه روح غیبی آمده مهمان ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عزم ره داریم نتوان بیش از این کردن درنگ</p></div>
<div class="m2"><p>زانکه جلاد اجل در انتظار جان ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا غیاث المستغیث یا اله العالمین</p></div>
<div class="m2"><p>جملهٔ شب تا سحر بر درگهش افغان ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن چنان خلوت که ما از جان و دل بودیم دوش</p></div>
<div class="m2"><p>جبرئیل آید نگنجد در میان گر جان ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر شما را طاعت است و زهد و تقوی و ورع</p></div>
<div class="m2"><p>باک نیست چون دوست اندر عهد و در پیمان ماست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تحفهٔ جنت که از بهر شما آراستند</p></div>
<div class="m2"><p>با غم هجران او دوزخ سرابستان ماست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غم مخور عطار چندین از برای جسم خود</p></div>
<div class="m2"><p>زانکه بحر رحمتش در انتظار جان ماست</p></div></div>