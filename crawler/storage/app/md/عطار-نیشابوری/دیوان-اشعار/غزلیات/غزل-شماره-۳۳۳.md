---
title: >-
    غزل شمارهٔ ۳۳۳
---
# غزل شمارهٔ ۳۳۳

<div class="b" id="bn1"><div class="m1"><p>کسی کو خویش بیند بنده نبود</p></div>
<div class="m2"><p>وگر بنده بود بیننده نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خود زنده مباش ای بنده آخر</p></div>
<div class="m2"><p>چرا شبنم به دریا زنده نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو هستی شبنمی دریاب دریا</p></div>
<div class="m2"><p>که جز دریا تو را دارنده نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درین دریا چو شبنم پاک گم شو</p></div>
<div class="m2"><p>که هر کو گم نشد داننده نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر در خود بمانی ناشده گم</p></div>
<div class="m2"><p>تو را جاوید کس جوینده نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو می‌ترسی که در دنیا مدامت</p></div>
<div class="m2"><p>بسازی از بقا افکنده نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وجود جاودان خواهی، ندانی</p></div>
<div class="m2"><p>که گل چون گل بسی پاینده نبود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وجود گل به بالای گل آمد</p></div>
<div class="m2"><p>که سلطانی مقام بنده نبود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تورا در نو شدن جامه که آرد</p></div>
<div class="m2"><p>اگر بر قد تو زیبنده نبود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه می‌گویم چو تو هستی نداری</p></div>
<div class="m2"><p>تورا جز نیستی یابنده نبود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر خواهی که دایم هست گردی</p></div>
<div class="m2"><p>که در هستی تورا ماننده نبود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فرو شو در ره معشوق جاوید</p></div>
<div class="m2"><p>که هرگز رفته‌ای آینده نبود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در آتش کی رسد شمع فسرده</p></div>
<div class="m2"><p>اگر شب تا سحر سوزنده نبود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فلک هرگز نگردد محرم عشق</p></div>
<div class="m2"><p>اگر سر تا قدم گردنده نبود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر آن کبکی که قوت باز گردد</p></div>
<div class="m2"><p>ورای او کسی پرنده نبود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چه می‌گویی تو ای عطار آخر</p></div>
<div class="m2"><p>به عالم در چو تو گوینده نبود</p></div></div>