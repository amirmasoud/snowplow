---
title: >-
    غزل شمارهٔ ۴۳۴
---
# غزل شمارهٔ ۴۳۴

<div class="b" id="bn1"><div class="m1"><p>ای ز عشقت این دل دیوانه خوش</p></div>
<div class="m2"><p>جان و دردت هر دو در یک خانه خوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر وصال است از تو قسمم گر فراق</p></div>
<div class="m2"><p>هست هر دو بر من دیوانه خوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من چنان در عشق غرقم کز توام</p></div>
<div class="m2"><p>هم غرامت هست و هم شکرانه خوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل بسی افسانهٔ وصل تو گفت</p></div>
<div class="m2"><p>تا که شد در خواب ازین افسانه خوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر تو ای دل عاشقی پروانه‌وار</p></div>
<div class="m2"><p>از سر جان درگذر مردانه خوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه که جان درباختن کار تو نیست</p></div>
<div class="m2"><p>جان فشاندن هست از پروانه خوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قرب سلطان جوی و پروانه مجوی</p></div>
<div class="m2"><p>روستایی باشد از پروانه خوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر تو مرد آشنایی چون شوی</p></div>
<div class="m2"><p>از شرابی همچو آن بیگانه خوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که صد دریا ندارد حوصله</p></div>
<div class="m2"><p>تا ابد گردد به یک پیمانه خوش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرد این ره آن زمانی کز دو کون</p></div>
<div class="m2"><p>مفلسی باشی درین ویرانه خوش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو از آن مرغان مدان عطار را</p></div>
<div class="m2"><p>کز دو عالم آیدش یک دانه خوش</p></div></div>