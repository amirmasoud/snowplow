---
title: >-
    غزل شمارهٔ ۴۹۶
---
# غزل شمارهٔ ۴۹۶

<div class="b" id="bn1"><div class="m1"><p>گم شدم در خود نمی‌دانم کجا پیدا شدم</p></div>
<div class="m2"><p>شبنمی بودم ز دریا غرقه در دریا شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سایه‌ای بودم از اول بر زمین افتاده خوار</p></div>
<div class="m2"><p>راست کان خورشید پیدا گشت ناپیدا شدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زآمدن بس بی نشانم وز شدن بس بی خبر</p></div>
<div class="m2"><p>گوئیا یکدم برآمد کامدم من یا شدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌مپرس از من سخن زیرا که چون پروانه‌ای</p></div>
<div class="m2"><p>در فروغ شمع روی دوست ناپروا شدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ره عشقش چو دانش باید و بی دانشی</p></div>
<div class="m2"><p>لاجرم در عشق هم نادان و هم دانا شدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون همه تن دیده می‌بایست بود و کور گشت</p></div>
<div class="m2"><p>این عجایب بین که چون بینا و نابینا شدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاک بر فرقم اگر یک ذره دارم آگهی</p></div>
<div class="m2"><p>تا کجاست آنجا که من سرگشته‌دل آنجا شدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون دل عطار بیرون دیدم از هر دو جهان</p></div>
<div class="m2"><p>من ز تاثیر دل او بی دل و شیدا شدم</p></div></div>