---
title: >-
    غزل شمارهٔ ۸۶
---
# غزل شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>همه عالم خروش و جوش از آن است</p></div>
<div class="m2"><p>که معشوقی چنین پیدا، نهان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز هر یک ذره خورشیدی مهیاست</p></div>
<div class="m2"><p>ز هر یک قطره‌ای بحری روان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر یک ذره را دل برشکافی</p></div>
<div class="m2"><p>ببینی تا که اندر وی چه جان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آن اجسام پیوسته است درهم</p></div>
<div class="m2"><p>که هر ذره به دیگر مهربان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه توحید است اینجا و نه تشبیه</p></div>
<div class="m2"><p>نه کفر است و نه دین نه هر دوان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر جمله بدانی هیچ دانی</p></div>
<div class="m2"><p>که این جمله نشان از بی نشان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلی را کش از آنجا نیست قوتی</p></div>
<div class="m2"><p>میان اهل دل دستار خوان است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل عطار تا شد غرق این راه</p></div>
<div class="m2"><p>همه پنهانیش عین عیان است</p></div></div>