---
title: >-
    غزل شمارهٔ ۳۴۰
---
# غزل شمارهٔ ۳۴۰

<div class="b" id="bn1"><div class="m1"><p>چون تو جانان منی جان بی تو خرم کی شود</p></div>
<div class="m2"><p>چون تو در کس ننگری کس با تو همدم کی شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر جمال جانفزای خویش بنمایی به ما</p></div>
<div class="m2"><p>جان ما گر در فزاید حسن تو کم کی شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل ز من بردی و پرسیدی که دل گم کرده‌ای</p></div>
<div class="m2"><p>این چنین طراریت با من مسلم کی شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عهد کردی تا من دلخسته را مرهم کنی</p></div>
<div class="m2"><p>چون تو گویی یا کنی این عهد محکم کی شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون مرا دلخستگی از آرزوی روی توست</p></div>
<div class="m2"><p>این چنین دل خستگی زایل به مرهم کی شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غم از آن دارم که بی تو همچو حلقه بر درم</p></div>
<div class="m2"><p>تا تو از در در نیایی از دلم غم کی شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلوتی می‌بایدم با تو زهی کار کمال</p></div>
<div class="m2"><p>ذره‌ای هم‌خلوت خورشید عالم کی شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیستی عطار مرد او که هر تر دامنی</p></div>
<div class="m2"><p>گر به میدان لاشه تازد رخش رستم کی شود</p></div></div>