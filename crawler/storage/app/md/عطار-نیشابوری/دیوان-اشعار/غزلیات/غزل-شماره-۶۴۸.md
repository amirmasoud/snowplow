---
title: >-
    غزل شمارهٔ ۶۴۸
---
# غزل شمارهٔ ۶۴۸

<div class="b" id="bn1"><div class="m1"><p>عشق را بی‌خویشتن باید شدن</p></div>
<div class="m2"><p>نفس خود را راهزن باید شدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بت بود در راه او هرچه آن نه اوست</p></div>
<div class="m2"><p>در ره او بت‌شکن باید شدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلف جانان را شکن بیش از حد است</p></div>
<div class="m2"><p>کافر یک یک شکن باید شدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو بدو نزدیک نزدیکی ولیک</p></div>
<div class="m2"><p>دور دور از خویشتن باید شدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در نگنجد ما و من در راه او</p></div>
<div class="m2"><p>در رهش بی ما و من باید شدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوست چون هرگز نیاید در وطن</p></div>
<div class="m2"><p>عاشقان را بی وطن باید شدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در ره او بر امید وصل او</p></div>
<div class="m2"><p>خاک راه تن به تن باید شدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچو لاله غرقه در خون جگر</p></div>
<div class="m2"><p>زنده در زیر کفن باید شدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در ره او چون دویی را راه نیست</p></div>
<div class="m2"><p>با یکی در پیرهن باید شدن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پس چو عطار اندر آفاق جهان</p></div>
<div class="m2"><p>پاکباز انجمن باید شدن</p></div></div>