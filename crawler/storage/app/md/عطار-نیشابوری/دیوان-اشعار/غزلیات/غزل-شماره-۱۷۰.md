---
title: >-
    غزل شمارهٔ ۱۷۰
---
# غزل شمارهٔ ۱۷۰

<div class="b" id="bn1"><div class="m1"><p>تا زلف تو همچو مار می‌پیچد</p></div>
<div class="m2"><p>جان بی دل و بی قرار می‌پیچد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل بود بسی در انتظار تو</p></div>
<div class="m2"><p>در هر پیچی هزار می‌پیچد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان می‌پیچم که تاج را چندین</p></div>
<div class="m2"><p>زلف تو کمندوار می‌پیچد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس جان که ز پیچ حلقهٔ زلفت</p></div>
<div class="m2"><p>در حلقهٔ بی شمار می‌پیچد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس دل که ز زلف تابدار تو</p></div>
<div class="m2"><p>چو زلف تو تابدار می‌پیچد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس تن که ز بار عشق یک مویت</p></div>
<div class="m2"><p>بی روی تو زیر دار می‌پیچد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو می‌گذری ز ناز بس فارغ</p></div>
<div class="m2"><p>و او بر سر دار زار می‌پیچد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر دل که شکار زلف تو گردد</p></div>
<div class="m2"><p>جان می‌دهد و چو مار می‌پیچد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ترکانه و چست هندوی زلفت</p></div>
<div class="m2"><p>بس نادره در شکار می‌پیچد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر دل که ز دام زلف تو بجهد</p></div>
<div class="m2"><p>زان چهرهٔ چون نگار می‌پیچد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون می‌پیچد فرید بپذیرش</p></div>
<div class="m2"><p>زیرا که به اضطرار می‌پیچد</p></div></div>