---
title: >-
    غزل شمارهٔ ۲۶۳
---
# غزل شمارهٔ ۲۶۳

<div class="b" id="bn1"><div class="m1"><p>کسی کز حقیقت خبردار باشد</p></div>
<div class="m2"><p>جهان را بر او چه مقدار باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان وزن جایی پدیدار آرد</p></div>
<div class="m2"><p>که در دیده او را پدیدار باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلی دیده‌ای کز حقیقت گشاید</p></div>
<div class="m2"><p>جهان پیش او ذره کردار باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غلط گفتم آن ذره‌ای گر بود هم</p></div>
<div class="m2"><p>چو زان چشم بینی تو بسیار باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی را که دو کون یک قطره گردد</p></div>
<div class="m2"><p>ببین تا درونش چه بر کار باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر سایهٔ باطن او نباشد</p></div>
<div class="m2"><p>کجا گردش چرخ دوار باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نباشد خبر یک سر مویش از خود</p></div>
<div class="m2"><p>بقای ابد را سزاوار باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی را که تیمار دادش بقا شد</p></div>
<div class="m2"><p>فنا گشتن از خود چه تیمار باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غم خود مخور تا تو را ذره ذره</p></div>
<div class="m2"><p>به صد وجه پیوسته غمخوار باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به جای تو چون اصل کار است باقی</p></div>
<div class="m2"><p>اگر تو نباشی بسی کار باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درین راه اگر تا ابد فکر برود</p></div>
<div class="m2"><p>مپندار سری که پندار باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر جان عطار این بوی یابد</p></div>
<div class="m2"><p>یقین دان که آن دم نه عطار باشد</p></div></div>