---
title: >-
    غزل شمارهٔ ۶۱۲
---
# غزل شمارهٔ ۶۱۲

<div class="b" id="bn1"><div class="m1"><p>ما در غمت به شادی جان باز ننگریم</p></div>
<div class="m2"><p>در عشق تو به هر دو جهان باز ننگریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش خوش چو شمع ز آتش عشق تو فی‌المثل</p></div>
<div class="m2"><p>گر جان ما بسوخت به جان باز ننگریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر طاعتی که خلق جهان کرد و می‌کنند</p></div>
<div class="m2"><p>گر نقد ماست جمله بدان باز ننگریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سود دو کون در طلبت گر زیان کنیم</p></div>
<div class="m2"><p>ما در طلب به سود و زیان باز ننگریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر عین ما شود همه ذرات کاینات</p></div>
<div class="m2"><p>یک ذره ما به عین عیان باز ننگریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اسرار تو ز کون و مکان چون منزه است</p></div>
<div class="m2"><p>ما تا ابد به کون و مکان باز ننگریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون شد یقین ما که تویی اصل هرچه هست</p></div>
<div class="m2"><p>در پردهٔ یقین به گمان باز ننگریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در کوی تو دو اسبه بتازیم مردوار</p></div>
<div class="m2"><p>هرگز به مرکب و به عنان باز ننگریم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عطار چو کناره گرفت از میان ما</p></div>
<div class="m2"><p>ما از کنار او به میان باز ننگریم</p></div></div>