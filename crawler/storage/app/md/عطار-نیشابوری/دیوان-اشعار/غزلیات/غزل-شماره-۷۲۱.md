---
title: >-
    غزل شمارهٔ ۷۲۱
---
# غزل شمارهٔ ۷۲۱

<div class="b" id="bn1"><div class="m1"><p>ای ز سودای تو دل شیدا شده</p></div>
<div class="m2"><p>زآتش عشق تو آب ما شده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشقان در جست و جویت صد هزار</p></div>
<div class="m2"><p>تو چو دری در بن دریا شده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از میان آب و گل برخاسته</p></div>
<div class="m2"><p>در میان جان و دل پیدا شده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشقان را بر امید روی تو</p></div>
<div class="m2"><p>خون دل پالوده و جانها شده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو ز جمله فارغ و مشغول خویش</p></div>
<div class="m2"><p>خود به عشق خویش ناپروا شده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیده روی خویشتن در آینه</p></div>
<div class="m2"><p>بر جمال خویشتن شیدا شده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما همه پروانهٔ پر سوخته</p></div>
<div class="m2"><p>تو چو شمع از نور خود یکتا شده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یوسف اندر ملک مصر و سلطنت</p></div>
<div class="m2"><p>دیدهٔ یعقوب نابینا شده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گم شدم در جست و جویت روز و شب</p></div>
<div class="m2"><p>چند بازت جویم ای گم ناشده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون دل عطار در عالم دلی</p></div>
<div class="m2"><p>می‌نبینم از تو خون پالا شده</p></div></div>