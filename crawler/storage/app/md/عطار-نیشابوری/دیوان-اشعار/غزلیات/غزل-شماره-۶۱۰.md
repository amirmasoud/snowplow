---
title: >-
    غزل شمارهٔ ۶۱۰
---
# غزل شمارهٔ ۶۱۰

<div class="b" id="bn1"><div class="m1"><p>ما مرد کلیسیا و زناریم</p></div>
<div class="m2"><p>گبری کهنیم و نام برداریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دریوزه گران شهر گبرانیم</p></div>
<div class="m2"><p>شش‌پنج‌زنان کوی خماریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با جملهٔ مفسدان به تصدیقیم</p></div>
<div class="m2"><p>با جملهٔ زاهدان به انکاریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در فسق و قمار پیر و استادیم</p></div>
<div class="m2"><p>در دیر مغان مغی به هنجاریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تسبیح و ردا نمی‌خریم الحق</p></div>
<div class="m2"><p>سالوس و نفاق را خریداریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در گلخن تیره سر فرو برده</p></div>
<div class="m2"><p>گاهی مستیم و گاه هشیاریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>واندر ره تایبان نامعلوم</p></div>
<div class="m2"><p>گاهی عوریم و گاه عیاریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با وسوسه‌های نفس شیطانی</p></div>
<div class="m2"><p>در حضرت حق چه مرد اسراریم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اندر صف دین حضور چون یابیم</p></div>
<div class="m2"><p>کاندر کف نفس خود گرفتاریم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این خود همه رفت عیب ما امروز</p></div>
<div class="m2"><p>این است که دوست دوست می‌داریم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دیری است که اوست آرزوی ما</p></div>
<div class="m2"><p>بی او به بهشت سر فرو ناریم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر جملهٔ ما به دوزخ اندازد</p></div>
<div class="m2"><p>او به داند اگر سزاواریم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بی یار دمی چو زنده نتوان بود</p></div>
<div class="m2"><p>در دوزخ و در بهشت با یاریم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بی او چو نه‌ایم هرچه باداباد</p></div>
<div class="m2"><p>جز یار ز هرچه هست بیزاریم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در راه یگانگی و مشغولی</p></div>
<div class="m2"><p>فارغ ز دو کون همچو عطاریم</p></div></div>