---
title: >-
    غزل شمارهٔ ۸۱۵
---
# غزل شمارهٔ ۸۱۵

<div class="b" id="bn1"><div class="m1"><p>گفتم بخرم غمت به جانی</p></div>
<div class="m2"><p>بر من بفروختی جهانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مفروش چنان برآن که پیوست</p></div>
<div class="m2"><p>عشوه خرد از تو هر زمانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنواز مرا که بی تو برخاست</p></div>
<div class="m2"><p>چون چنگ ز هر رگم فغانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نی نی چو ربابم از غم تو</p></div>
<div class="m2"><p>یعنی که رگی و استخوانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای دوست روا مدار دل را</p></div>
<div class="m2"><p>نومید ز چون تو دلستانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دستی بر نه اگر کنم سود</p></div>
<div class="m2"><p>دانم نبود تو را زیانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا نی سبکم بکن ز هستی</p></div>
<div class="m2"><p>تا چند ز رحمت گرانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون شمع مرا ز عشق می‌سوز</p></div>
<div class="m2"><p>تا می‌ماند ز من نشانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عطار چو بی نشان شد از عشق</p></div>
<div class="m2"><p>از محو رسد سوی عیانی</p></div></div>