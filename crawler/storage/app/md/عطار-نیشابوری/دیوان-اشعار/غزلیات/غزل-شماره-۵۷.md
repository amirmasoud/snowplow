---
title: >-
    غزل شمارهٔ ۵۷
---
# غزل شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>آتش عشق تو در جان خوشتر است</p></div>
<div class="m2"><p>جان ز عشقت آتش‌افشان خوشتر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که خورد از جام عشقت قطره‌ای</p></div>
<div class="m2"><p>تا قیامت مست و حیران خوشتر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا تو پیدا آمدی پنهان شدم</p></div>
<div class="m2"><p>زانکه با معشوق پنهان خوشتر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درد عشق تو که جان می‌سوزدم</p></div>
<div class="m2"><p>گر همه زهر است از جان خوشتر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درد بر من ریز و درمانم مکن</p></div>
<div class="m2"><p>زانکه درد تو ز درمان خوشتر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌نسازی تا نمی‌سوزی مرا</p></div>
<div class="m2"><p>سوختن در عشق تو زان خوشتر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون وصالت هیچکس را روی نیست</p></div>
<div class="m2"><p>روی در دیوار هجران خوشتر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خشک سال وصل تو بینم مدام</p></div>
<div class="m2"><p>لاجرم در دیده طوفان خوشتر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همچو شمعی در فراقت هر شبی</p></div>
<div class="m2"><p>تا سحر عطار گریان خوشتر است</p></div></div>