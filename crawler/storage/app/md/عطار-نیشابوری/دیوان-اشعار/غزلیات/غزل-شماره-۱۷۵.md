---
title: >-
    غزل شمارهٔ ۱۷۵
---
# غزل شمارهٔ ۱۷۵

<div class="b" id="bn1"><div class="m1"><p>اگر دردت دوای جان نگردد</p></div>
<div class="m2"><p>غم دشوار تو آسان نگردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که دردم را تواند ساخت درمان</p></div>
<div class="m2"><p>اگر هم درد تو درمان نگردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دمی درمان یک دردم نسازی</p></div>
<div class="m2"><p>که بر من درد صد چندان نگردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که یابد از سر زلف تو مویی</p></div>
<div class="m2"><p>که دایم بی سر و سامان نگردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که یابد از سر کوی تو گردی</p></div>
<div class="m2"><p>که همچون چرخ سرگردان نگردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که یابد از می عشق تو بویی</p></div>
<div class="m2"><p>که جانش مست جاویدان نگردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ندانم تا چه خورشیدی است عشقت</p></div>
<div class="m2"><p>که جز در آسمان جان نگردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلا هرگز بقای کل نیابی</p></div>
<div class="m2"><p>که تا جان فانی جانان نگردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یقین می‌دان که جان در پیش جانان</p></div>
<div class="m2"><p>نیابد قرب تا قربان نگردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر قربان نگردد نیست ممکن</p></div>
<div class="m2"><p>که بر تو عمر تو تاوان نگردد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو خفاشی بمیری چشم بسته</p></div>
<div class="m2"><p>اگر خورشید تو رخشان نگردد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر آدم کفی گل بود گو باش</p></div>
<div class="m2"><p>به گل خورشید تو پنهان نگردد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در آن خورشید حیران گشت عطار</p></div>
<div class="m2"><p>چنان جایی کسی حیران نگردد</p></div></div>