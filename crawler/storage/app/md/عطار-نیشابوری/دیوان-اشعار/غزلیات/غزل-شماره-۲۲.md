---
title: >-
    غزل شمارهٔ ۲۲
---
# غزل شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>عشق جانان همچو شمعم از قدم تا سر بسوخت</p></div>
<div class="m2"><p>مرغ جان را نیز چون پروانه بال و پر بسوخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشقش آتش بود کردم مجمرش از دل چو عود</p></div>
<div class="m2"><p>آتش سوزنده بر هم عود و هم مجمر بسوخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زآتش رویش چو یک اخگر به صحرا اوفتاد</p></div>
<div class="m2"><p>هر دو عالم همچو خاشاکی از آن اخگر بسوخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواستم تا پیش جانان پیشکش جان آورم</p></div>
<div class="m2"><p>پیش دستی کرد عشق و جانم اندر بر بسوخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست از خشک و ترم در دست جز خاکستری</p></div>
<div class="m2"><p>کاتش غیرت درآمد خشک و تر یکسر بسوخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دادم آن خاکستر آخر بر سر کویش به باد</p></div>
<div class="m2"><p>برق استغنا بجست از غیب و خاکستر بسوخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم اکنون ذره‌ای دیگر بمانم گفت باش</p></div>
<div class="m2"><p>ذرهٔ دیگر چه باشد ذره‌ای دیگر بسوخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون رسید این جایگه عطار نه هست و نه نیست</p></div>
<div class="m2"><p>کفر و ایمانش نماند و مؤمن و کافر بسوخت</p></div></div>