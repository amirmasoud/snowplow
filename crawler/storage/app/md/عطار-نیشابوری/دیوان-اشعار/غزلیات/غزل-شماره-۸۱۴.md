---
title: >-
    غزل شمارهٔ ۸۱۴
---
# غزل شمارهٔ ۸۱۴

<div class="b" id="bn1"><div class="m1"><p>ترسا بچه‌ای به دلستانی</p></div>
<div class="m2"><p>در دست شراب ارغوانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوش آمد و تیز و تازه بنشست</p></div>
<div class="m2"><p>چون آتش و آب زندگانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دانی که خوشی او چه سان بود</p></div>
<div class="m2"><p>چون عشق به موسم جوانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بسته میان خود به زنار</p></div>
<div class="m2"><p>بگشاده دهن به دلستانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در هر خم زلف دلفریبش</p></div>
<div class="m2"><p>صد عالم کافری نهانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آمد بنشست و پیر ما را</p></div>
<div class="m2"><p>بنهاد محک به امتحانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>القصه چو پیر روی او دید</p></div>
<div class="m2"><p>از دست بشد ز ناتوانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دردی ستد و درود دین کرد</p></div>
<div class="m2"><p>یارب ز بلای ناگهانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دردا که چنان بزرگواری</p></div>
<div class="m2"><p>برخاست ز راه خرده دانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ترسا بچه را به پیش خود خواند</p></div>
<div class="m2"><p>پس گفت نشان ره چه دانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتا که نشان راه جایی است</p></div>
<div class="m2"><p>کانجا نه تویی و نه نشانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون پیر سخن شنید جان داد</p></div>
<div class="m2"><p>عطار سخن بگو که جانی</p></div></div>