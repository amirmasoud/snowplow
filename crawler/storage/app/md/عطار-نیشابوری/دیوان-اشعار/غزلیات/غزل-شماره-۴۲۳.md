---
title: >-
    غزل شمارهٔ ۴۲۳
---
# غزل شمارهٔ ۴۲۳

<div class="b" id="bn1"><div class="m1"><p>دستم نرسد به زلف چون شستش</p></div>
<div class="m2"><p>در پای از آن فتادم از دستش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر مرغ هوای او شوم شاید</p></div>
<div class="m2"><p>صد دام معنبر است در شستش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از لب ندهد میی و می‌داند</p></div>
<div class="m2"><p>مخموری من ز نرگس مستش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیچاره دلم که چشم مست او</p></div>
<div class="m2"><p>صد توبه به یک کرشمه بشکستش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بشکفت گل رخش به زیبایی</p></div>
<div class="m2"><p>غنچه ز میان جان کمر بستش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از بس که بریخت مشک از زلفش</p></div>
<div class="m2"><p>چون خاک به زیر پای شد پستش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون بود بتی چنان که در عالم</p></div>
<div class="m2"><p>بپرستندش که جای آن هستش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک یک سر موی من همی گوید</p></div>
<div class="m2"><p>رویش بنگر که گفت مپرستش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نی نی که نقاب بر نمی‌دارد</p></div>
<div class="m2"><p>تا سجده نمی‌کنند پیوستش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عطار دلی که داشت در عشقش</p></div>
<div class="m2"><p>برخاست اومید و نیست بنشستش</p></div></div>