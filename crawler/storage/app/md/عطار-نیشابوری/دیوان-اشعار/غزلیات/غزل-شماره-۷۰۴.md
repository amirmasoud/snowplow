---
title: >-
    غزل شمارهٔ ۷۰۴
---
# غزل شمارهٔ ۷۰۴

<div class="b" id="bn1"><div class="m1"><p>در کنج اعتکاف دلی بردبار کو</p></div>
<div class="m2"><p>بر گنج عشق جان کسی کامگار کو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر میان صفه‌نشینان خانقاه</p></div>
<div class="m2"><p>یک صوفی محقق پرهیزگار کو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در پیشگاه مسجد و در کنج صومعه</p></div>
<div class="m2"><p>یک پیر کار دیده و یک مرد کار کو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در حلقهٔ سماع که دریای حالت است</p></div>
<div class="m2"><p>بر آتش سماع دلی بی‌قرار کو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در رقص و در سماع ز هستی فنا شده</p></div>
<div class="m2"><p>اندر هوای دوست دلی ذره‌وار کو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خالص برای لله ازین ژنده جامگان</p></div>
<div class="m2"><p>بی زرق و بی نفاق یکی خرقه‌دار کو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مردان مرد و راه‌نمایان روزگار</p></div>
<div class="m2"><p>زین پیش بوده‌اند درین روزگار کو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در وادی محبت و صحرای معرفت</p></div>
<div class="m2"><p>مردی تمام پاک رو و اختیار کو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اندر صف مجاهده یک تن ز سروران</p></div>
<div class="m2"><p>بر مرکب توکل و تقوی سوار کو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سرگشته مانده‌ایم درین راه بی کران</p></div>
<div class="m2"><p>وز سابقان پیشرو آخر غبار کو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عطار سوی گوهر آن بحر موج زن</p></div>
<div class="m2"><p>جز در درون سینه تورا رهگذار کو</p></div></div>