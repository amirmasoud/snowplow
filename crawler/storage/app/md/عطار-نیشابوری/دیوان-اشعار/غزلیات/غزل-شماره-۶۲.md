---
title: >-
    غزل شمارهٔ ۶۲
---
# غزل شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>مرکب لنگ است و راه دور است</p></div>
<div class="m2"><p>دل را چکنم که ناصبور است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این راه پریدنم خیال است</p></div>
<div class="m2"><p>وین شیوه گرفتنم غرور است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد قرن چو باد اگر بپویم</p></div>
<div class="m2"><p>هم باد بود که یار دور است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با این همه گر دمی برآرم</p></div>
<div class="m2"><p>بی او همه فسق یا فجور است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دانی تو که سر کافری چیست</p></div>
<div class="m2"><p>آن دم که همی نه در حضور است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی او نفسی مزن که ناگاه</p></div>
<div class="m2"><p>تیغت زند او که بس غیور است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگذر ز رجا و خوف کین‌جا</p></div>
<div class="m2"><p>چه جای خیال نار و نور است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جایی است که صد جهان اگر نیست</p></div>
<div class="m2"><p>ور هست نه ماتم و نه سور است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مردی که بدین صفت رسیده است</p></div>
<div class="m2"><p>دایم هم ازین صفت نفور است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همچون دریا بود که پیوست</p></div>
<div class="m2"><p>لب خشک بماند از قصور است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این حرف ز بی نهایتی رفت</p></div>
<div class="m2"><p>چون زین بگذشت زرق و زور است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یک ذره‌گی فرید اینجا</p></div>
<div class="m2"><p>بالای هزار خلد و حور است</p></div></div>