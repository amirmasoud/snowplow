---
title: >-
    غزل شمارهٔ ۵۸۴
---
# غزل شمارهٔ ۵۸۴

<div class="b" id="bn1"><div class="m1"><p>هر شبی وقت سحر در کوی جانان می‌روم</p></div>
<div class="m2"><p>چون ز خود نامحرمم از خویش پنهان می‌روم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون حجابی مشکل آمد عقل و جان در راه او</p></div>
<div class="m2"><p>لاجرم در کوی او بی عقل و بی جان می‌روم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو لیلی مستمندم در فراقش روز و شب</p></div>
<div class="m2"><p>همچو مجنون گرد عالم دوست جویان می‌روم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر سحر عنبر فشاند زلف عنبر بار او</p></div>
<div class="m2"><p>من بدان آموختم وقت سحر زان می‌روم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا بدیدم زلف چون چوگان او بر روی ماه</p></div>
<div class="m2"><p>در خم چوگان او چون گوی گردان می‌روم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ماه رویا در من مسکین نگر کز عشق تو</p></div>
<div class="m2"><p>با دلی پر خون به زیر خاک حیران می‌روم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ذره ذره زان شدم تا پیش خورشید رخش</p></div>
<div class="m2"><p>همچو ذره بی سر و تن پای کوبان می‌روم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون بیابانی نهد هر ساعتی در پیش من</p></div>
<div class="m2"><p>من چنین شوریده دل سر در بیابان می‌روم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا کی ای عطار از ننگ وجود تو مرا</p></div>
<div class="m2"><p>کین زمان از ننگ تو با خاک یکسان می‌روم</p></div></div>