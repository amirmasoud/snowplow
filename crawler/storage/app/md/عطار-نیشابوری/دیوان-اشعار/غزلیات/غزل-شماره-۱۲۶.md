---
title: >-
    غزل شمارهٔ ۱۲۶
---
# غزل شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>در عشق تو عقل سرنگون گشت</p></div>
<div class="m2"><p>جان نیز خلاصهٔ جنون گشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود حال دلم چگونه گویم</p></div>
<div class="m2"><p>کان کار به جان رسیده چون گشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر خاک درت به زاری زار</p></div>
<div class="m2"><p>از بس که به خون بگشت خون گشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خون دل ماست یا دل ماست</p></div>
<div class="m2"><p>خونی که ز دیده‌ها برون گشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درمان چه طلب کنم که عشقت</p></div>
<div class="m2"><p>ما را سوی درد رهنمون گشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن مرغ که بود زیرکش نام</p></div>
<div class="m2"><p>در دام بلای تو زبون گشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لختی پر و بال زد به آخر</p></div>
<div class="m2"><p>از پای فتاد و سرنگون گشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا دور شدم من از در تو</p></div>
<div class="m2"><p>از ناله دلم چو ارغنون گشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا قوت عشق تو بدیدم</p></div>
<div class="m2"><p>سرگشتگیم بسی فزون گشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا درد تو را خرید عطار</p></div>
<div class="m2"><p>قد الفش بسان نون گشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عطار که بود کشتهٔ تو</p></div>
<div class="m2"><p>دریاب که کشته‌تر کنون گشت</p></div></div>