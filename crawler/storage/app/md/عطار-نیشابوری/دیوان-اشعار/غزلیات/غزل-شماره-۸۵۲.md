---
title: >-
    غزل شمارهٔ ۸۵۲
---
# غزل شمارهٔ ۸۵۲

<div class="b" id="bn1"><div class="m1"><p>ای آفتاب رویت از غایت نکویی</p></div>
<div class="m2"><p>افزون ز هرچه دانی برتر ز هرچه گویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نیکویی رویت یک ذره رخ نماید</p></div>
<div class="m2"><p>دو کون مست گردد از غایت نکویی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یارب چه آفتابی کاندر دو کون هرگز</p></div>
<div class="m2"><p>در چشم جان نیاید مثلت به خوبرویی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون از کمال غیرت بر جان کمین گشایی</p></div>
<div class="m2"><p>از خون عاشقانت روی زمین بشویی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عطار در ره او از هر دو کون بگذر</p></div>
<div class="m2"><p>وانگه ز خود فنا شو گر مرد راه اویی</p></div></div>