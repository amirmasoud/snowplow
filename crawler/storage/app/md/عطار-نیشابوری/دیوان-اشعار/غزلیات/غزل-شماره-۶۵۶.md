---
title: >-
    غزل شمارهٔ ۶۵۶
---
# غزل شمارهٔ ۶۵۶

<div class="b" id="bn1"><div class="m1"><p>ای پسر این رخ به آفتاب درافکن</p></div>
<div class="m2"><p>بادهٔ گلرنگ چون گلاب درافکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبح علم بر کشید و شمع برافروخت</p></div>
<div class="m2"><p>جام پیاپی کن و شراب درافکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاهد سرمست را ز خواب برانگیز</p></div>
<div class="m2"><p>سوختهٔ عشق را رباب درافکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه شب اندر شکست ماه بلند است</p></div>
<div class="m2"><p>بادهٔ خوش آمد به ماهتاب درافکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل بشکفت و دلم ز عشق تو برخاست</p></div>
<div class="m2"><p>چند نشینی به بند و تاب درافکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مست خرابیم جمله نعره زنانیم</p></div>
<div class="m2"><p>نعره درین عالم خراب درافکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چند ازین نام و ننگ و زهد و ز تزویر</p></div>
<div class="m2"><p>توبه کن از توبه دل بتاب درافکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر دل عطار را عذاب غم توست</p></div>
<div class="m2"><p>گو دل او غم ازین عذاب درافکن</p></div></div>