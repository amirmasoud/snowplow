---
title: >-
    غزل شمارهٔ ۳۴۸
---
# غزل شمارهٔ ۳۴۸

<div class="b" id="bn1"><div class="m1"><p>زلف را چون به قصد تاب دهد</p></div>
<div class="m2"><p>کفر را سر به مهر آب دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز چون درکشد نقاب از روی</p></div>
<div class="m2"><p>همه کفار را جواب دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون درآید به جلوه ماه رخش</p></div>
<div class="m2"><p>تاب در جان آفتاب دهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تیر چشمش که کم خطا کرده است</p></div>
<div class="m2"><p>مالش عاشقان صواب دهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه خامان بی حقیقت را</p></div>
<div class="m2"><p>سر زلفش هزار تاب دهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تشنگان را که خار هجر نهاد</p></div>
<div class="m2"><p>لب گلرنگ او شراب دهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غم او زان چنین قوی افتاد</p></div>
<div class="m2"><p>که دلم دایمش کباب دهد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گاه شعرم بدو شکر ریزد</p></div>
<div class="m2"><p>گاه چشمم بدو گلاب دهد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر دلم می‌دهد غمش را جای</p></div>
<div class="m2"><p>گنج را جایگه خراب دهد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل به جان باز می‌نهد غم او</p></div>
<div class="m2"><p>تا درین دردش انقلاب دهد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل عطار چون ز دست بشد</p></div>
<div class="m2"><p>چکند تن در اضطراب دهد</p></div></div>