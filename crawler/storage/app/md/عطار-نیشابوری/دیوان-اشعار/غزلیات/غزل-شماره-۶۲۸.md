---
title: >-
    غزل شمارهٔ ۶۲۸
---
# غزل شمارهٔ ۶۲۸

<div class="b" id="bn1"><div class="m1"><p>در عشق همی بلا همی جویم</p></div>
<div class="m2"><p>درد دل مبتلا همی جویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مان چه طلب کنم که در عشقش</p></div>
<div class="m2"><p>یک درد به صد دعا همی جویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از صوف صفای دل نمی‌یابم</p></div>
<div class="m2"><p>از درد مغان صفا همی جویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خرقه و طیلسان دلم خون شد</p></div>
<div class="m2"><p>زنار و کلیسیا همی جویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در بحر هزار موج عشق او</p></div>
<div class="m2"><p>غرقه شده و آشنا همی جویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جانا به لقا چو آفتابی تو</p></div>
<div class="m2"><p>یک ذره از آن بقا همی جویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا چند دوم به گرد عالم در</p></div>
<div class="m2"><p>تو با من و من که را همی جویم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو دست به جان من فرا کرده</p></div>
<div class="m2"><p>من گرد جهان تورا همی جویم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو در دل و من به گرد عالم در</p></div>
<div class="m2"><p>بنگر که تورا کجا همی جویم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عطار شدم ز عطر زلف تو</p></div>
<div class="m2"><p>زان عطر دگر عطا همی جویم</p></div></div>