---
title: >-
    غزل شمارهٔ ۱
---
# غزل شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>چون نیست هیچ مردی در عشق یار ما را</p></div>
<div class="m2"><p>سجاده زاهدان را درد و قمار ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جایی که جان مردان باشد چو گوی گردان</p></div>
<div class="m2"><p>آن نیست جای رندان با آن چکار ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر ساقیان معنی با زاهدان نشینند</p></div>
<div class="m2"><p>می زاهدان ره را درد و خمار ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درمانش مخلصان را دردش شکستگان را</p></div>
<div class="m2"><p>شادیش مصلحان را غم یادگار ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای مدعی کجایی تا ملک ما ببینی</p></div>
<div class="m2"><p>کز هرچه بود در ما برداشت یار ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آمد خطاب ذوقی از هاتف حقیقت</p></div>
<div class="m2"><p>کای خسته چون بیابی اندوه زار ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عطار اندرین ره اندوهگین فروشد</p></div>
<div class="m2"><p>زیرا که او تمام است انده گسار ما را</p></div></div>