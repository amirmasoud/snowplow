---
title: >-
    غزل شمارهٔ ۲۳۹
---
# غزل شمارهٔ ۲۳۹

<div class="b" id="bn1"><div class="m1"><p>جان در مقام عشق به جانان نمی‌رسد</p></div>
<div class="m2"><p>دل در بلای درد به درمان نمی‌رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درمان دل وصال و جمال است و این دو چیز</p></div>
<div class="m2"><p>دشوار می‌نماید و آسان نمی‌رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذوقی که هست جمله در آن حضرت است نقد</p></div>
<div class="m2"><p>وز صد یکی به عالم عرفان نمی‌رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وز هرچه نقد عالم عرفان است از هزار</p></div>
<div class="m2"><p>جزوی به کل گنبد گردان نمی‌رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وز صد هزار چیز که بر چرخ می‌رود</p></div>
<div class="m2"><p>صد یک به سوی جوهر انسان نمی‌رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وز هرچه یافت جوهر انسان ز شوق و ذوق</p></div>
<div class="m2"><p>بویی به جنس جملهٔ حیوان نمی‌رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مقصود آنکه از می ساقی حضرتش</p></div>
<div class="m2"><p>یک قطره درد درد به دو جهان نمی‌رسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چندین حجاب در ره تو خود عجب مدار</p></div>
<div class="m2"><p>گر جان تو به حضرت جانان نمی‌رسد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جانان چو گنج زیر طلسم جهان نهاد</p></div>
<div class="m2"><p>گنجی که هیچ کس به سر آن نمی‌رسد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زان می که می‌دهند از آن حسن قسم تو</p></div>
<div class="m2"><p>جز درد واپس آمد ایشان نمی‌رسد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو قانعی به لذت جسمی چو گاو و خر</p></div>
<div class="m2"><p>چون دست تو به معرفت جان نمی‌رسد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا کی چو کرم پیله تنی گرد خویشتن</p></div>
<div class="m2"><p>بر خود متن که خود به تو چندان نمی‌رسد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خود را قدم قدم به مقام بر پران</p></div>
<div class="m2"><p>چندان پران که رخصت امکان نمی‌رسد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زیرا که مرد راه نگیرد به هیچ روی</p></div>
<div class="m2"><p>یکدم قرار تا که به پیشان نمی‌رسد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چندین هزار حاجب و دربان که در رهند</p></div>
<div class="m2"><p>شاید اگر کسی بر سلطان نمی‌رسد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در راه او رسید قدم‌های سالکان</p></div>
<div class="m2"><p>وین راه بی‌کرانه به پایان نمی‌رسد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پایان ندید کس ز بیابان عشق از آنک</p></div>
<div class="m2"><p>هرگز دلی به پای بیابان نمی‌رسد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چندان به بوی وصل که در خود سفر کند</p></div>
<div class="m2"><p>عطار را به جز غم هجران نمی‌رسد</p></div></div>