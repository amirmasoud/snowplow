---
title: >-
    غزل شمارهٔ ۳۳۷
---
# غزل شمارهٔ ۳۳۷

<div class="b" id="bn1"><div class="m1"><p>چه سازی سرای و چه گویی سرود</p></div>
<div class="m2"><p>فروشو بدین خاک تیره فرود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یقین‌دان که همچون تو بسیار کس</p></div>
<div class="m2"><p>فکندست در چرخ چرخ کبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه برخیزد از خود و آهن تو را</p></div>
<div class="m2"><p>چو سر آهنین نیست در زیر خود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر جامهٔ عمر تو زآهن است</p></div>
<div class="m2"><p>اجل بگسلد از همش تار و پود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر سر کشی زین پل هفت طاق</p></div>
<div class="m2"><p>سر و سنگ ماننده آب رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز سرگشتگی زیر چوگان چرخ</p></div>
<div class="m2"><p>چو گویی ندانی فراز از فرود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو دور سپهرت نخواهد گذاشت</p></div>
<div class="m2"><p>ز دور سپهری چه نالی چو رود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رفیقان هم‌راز را کن وداع</p></div>
<div class="m2"><p>عزیزان همدرد را کن درود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درخت بتر بودن از بن بکن</p></div>
<div class="m2"><p>ز شاخ بهی کن کلوخ آمرود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مکن همچو عطار عمر عزیز</p></div>
<div class="m2"><p>همه ضایع اندر سرای و سرود</p></div></div>