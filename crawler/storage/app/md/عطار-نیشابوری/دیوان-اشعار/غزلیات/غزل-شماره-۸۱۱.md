---
title: >-
    غزل شمارهٔ ۸۱۱
---
# غزل شمارهٔ ۸۱۱

<div class="b" id="bn1"><div class="m1"><p>ای هجر تو وصل جاودانی</p></div>
<div class="m2"><p>واندوه تو عین شادمانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عشق تو نیم ذره حسرت</p></div>
<div class="m2"><p>خوشتر ز حیات جاودانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی یاد حضور تو زمانی</p></div>
<div class="m2"><p>کفر است حدیث زندگانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صد جان و هزار جان نثارت</p></div>
<div class="m2"><p>آن لحظه که از درم برانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کار دو جهان من برآید</p></div>
<div class="m2"><p>گر یک نفسم به خویش خوانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با خوندان و راندنم چه کار است</p></div>
<div class="m2"><p>خواه این کن و خواه آن تو دانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر قهر کنی سزای آنم</p></div>
<div class="m2"><p>ور لطف کنی برای آنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صد دل باید به هر زمانم</p></div>
<div class="m2"><p>تا تو ببری به دلستانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر بر فکنی نقاب از روی</p></div>
<div class="m2"><p>جبریل سزد به جان‌فشانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کس نتواند جمال تو دید</p></div>
<div class="m2"><p>زیرا که ز دیده بس نهانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نی نی که به جز تو کس نبیند</p></div>
<div class="m2"><p>چون جمله تویی بدین عیانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در عشق تو گر بمرد عطار</p></div>
<div class="m2"><p>شد زندهٔ دایم از معانی</p></div></div>