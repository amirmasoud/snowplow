---
title: >-
    غزل شمارهٔ ۷۴۴
---
# غزل شمارهٔ ۷۴۴

<div class="b" id="bn1"><div class="m1"><p>بحری است عشق و عقل ازو برکناره‌ای</p></div>
<div class="m2"><p>کار کنارگی نبود جز نظاره‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بحر عشق عقل اگر راهبر بدی</p></div>
<div class="m2"><p>هرگز کجا فتادی ازو برکناره‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وانجا که بحر عشق درآید به جان و دل</p></div>
<div class="m2"><p>عقل است اعجمی و خرد شیرخواره‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در پردهٔ وجود ز هستی عدم شوند</p></div>
<div class="m2"><p>آنها که ره برند درین پرده پاره‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسیار چاره می‌طلبی تا که سر عشق</p></div>
<div class="m2"><p>یک دم شود به پیش تو چون آشکاره‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر صد هزار سال درین ره قدم زنی</p></div>
<div class="m2"><p>تا تو تویی تو را نتوان کرد چاره‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو درد عشق خود چه شناسی که چون بود</p></div>
<div class="m2"><p>تا بر دلت ز عشق نیاید کتاره‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در هر هزار سال به برج دلی رسد</p></div>
<div class="m2"><p>از آسمان عشق بدین سان ستاره‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عطار اگر پیاده شوی از دو کون تو</p></div>
<div class="m2"><p>در هر دو کون چون تو نباشد سواره‌ای</p></div></div>