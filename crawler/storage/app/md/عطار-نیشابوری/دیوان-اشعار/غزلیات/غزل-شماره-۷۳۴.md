---
title: >-
    غزل شمارهٔ ۷۳۴
---
# غزل شمارهٔ ۷۳۴

<div class="b" id="bn1"><div class="m1"><p>چون کشته شدم هزار باره</p></div>
<div class="m2"><p>بر من به چه می‌کشی کناره</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از کشتن کشته‌ای چه خیزد</p></div>
<div class="m2"><p>کشته که کشد هزار باره</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حاجت نبود به تیغ کشتن</p></div>
<div class="m2"><p>در پیش رخ تو ماه‌پاره</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خود خلق دو کون کشته گردند</p></div>
<div class="m2"><p>هر گه که شوی تو آشکاره</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زیرا که ز تیغ غمزهٔ تو</p></div>
<div class="m2"><p>خونی گردد چو لعل خاره</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر بر گیری نقاب از روی</p></div>
<div class="m2"><p>مه شق شود آفتاب پاره</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ذرات دو کون دیده گردند</p></div>
<div class="m2"><p>وایند چو ذره در نظاره</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از پرتو رویت آخرالامر</p></div>
<div class="m2"><p>هر ذره شود چو صد ستاره</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از پرده چو آفتاب رویت</p></div>
<div class="m2"><p>بر مرکب حسن شد سواره</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خورشید که شاه پیشگاه است</p></div>
<div class="m2"><p>شد پیش رخ تو پیشکاره</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون شیر عنایتت درآید</p></div>
<div class="m2"><p>هر ذره شوند شیرخواره</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>طفلان زمانهٔ خرف را</p></div>
<div class="m2"><p>لطف تو بس است گاهواره</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کاجزای دو کون را تمام است</p></div>
<div class="m2"><p>لطف تو چو بحر بی کناره</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیچارهٔ خود فرید را خوان</p></div>
<div class="m2"><p>زیرا که ندارد از تو چاره</p></div></div>