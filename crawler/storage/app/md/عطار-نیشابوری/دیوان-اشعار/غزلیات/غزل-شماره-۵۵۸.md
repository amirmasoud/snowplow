---
title: >-
    غزل شمارهٔ ۵۵۸
---
# غزل شمارهٔ ۵۵۸

<div class="b" id="bn1"><div class="m1"><p>چون نام تو بر زبان برانم</p></div>
<div class="m2"><p>صد میل به یک زمان برانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر نام تو در میان خشکی</p></div>
<div class="m2"><p>کشتی روان روان برانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین دریاها که پیش دارم</p></div>
<div class="m2"><p>صد سیل ز دیدگان برانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نام تو کشتیی بسازم</p></div>
<div class="m2"><p>وآن کشتی را چنان برانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کز قوت آن روش به یک دم</p></div>
<div class="m2"><p>کام دل جاودان برانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رخش فلکی به زین درآرم</p></div>
<div class="m2"><p>بس گرد همه جهان برانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اسب از سه صف زمان بتازم</p></div>
<div class="m2"><p>وز شش جهت مکان برانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در هر قدمی ز راه سیلی</p></div>
<div class="m2"><p>از دیدهٔ خونفشان برانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وین ملک که گشت ملک عطار</p></div>
<div class="m2"><p>در عالم بی نشان برانم</p></div></div>