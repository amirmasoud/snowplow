---
title: >-
    غزل شمارهٔ ۱۱۸
---
# غزل شمارهٔ ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>کیست که از عشق تو پردهٔ او پاره نیست</p></div>
<div class="m2"><p>وز قفس قالبش مرغ دل آواره نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وزن کجا آورد خاصه به میزان عشق</p></div>
<div class="m2"><p>گر زر عشاق را سکهٔ رخساره نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر نفسم همچو شمع زاربکش پیش خویش</p></div>
<div class="m2"><p>گر دل پر خون من کشتهٔ صد پاره نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر تو ز من فارغی من ز تو فارغ نیم</p></div>
<div class="m2"><p>چارهٔ کارم بکن کز تو مرا چاره نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که درین راه یافت بوی می عشق تو</p></div>
<div class="m2"><p>مست شود تا ابد گر دلش از خاره نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هست همه گفتگو با می عشقش چه کار</p></div>
<div class="m2"><p>هرکه درین میکده مفلس و این کاره نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درد ره و درد دیر هست محک مرد را</p></div>
<div class="m2"><p>دلق بیفکن که زرق لایق میخواره نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در بن این دیر اگر هست میت آرزو</p></div>
<div class="m2"><p>درد خور اینجا که دیر موضع نظاره نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گشت هویدا چو روز بر دل عطار از آنک</p></div>
<div class="m2"><p>عهد ندارد درست هر که درین پاره نیست</p></div></div>