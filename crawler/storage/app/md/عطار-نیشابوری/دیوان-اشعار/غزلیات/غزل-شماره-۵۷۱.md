---
title: >-
    غزل شمارهٔ ۵۷۱
---
# غزل شمارهٔ ۵۷۱

<div class="b" id="bn1"><div class="m1"><p>دل ز دستم رفت و جان هم، بی دل و جان چون کنم</p></div>
<div class="m2"><p>سر عشقم آشکارا گشت پنهان چون کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکسم گوید که درمانی کن آخر درد را</p></div>
<div class="m2"><p>چون به دردم دایما مشغول درمان چون کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون خروشم بشنود هر بی خبر گوید خموش</p></div>
<div class="m2"><p>می‌تپد دل در برم می‌سوزدم جان چون کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عالمی در دست من، من همچو مویی در برش</p></div>
<div class="m2"><p>قطره‌ای خون است دل، در زیر طوفان چون کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در تموزم مانده جان خسته و تن تب زده</p></div>
<div class="m2"><p>وآنگهم گویند براین ره به پایان چون کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون ندارم یک نفس اهلیت صف النعال</p></div>
<div class="m2"><p>پیشگه چون جویم و آهنگ پیشان چون کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بن هر موی صد بت بیش می‌بینم عیان</p></div>
<div class="m2"><p>در میان این همه بت عزم ایمان چون کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه ز ایمانم نشانی نه ز کفرم رونقی</p></div>
<div class="m2"><p>در میان این و آن درمانده حیران چون کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون نیامد از وجودم هیچ جمعیت پدید</p></div>
<div class="m2"><p>بیش ازین عطار را از خود پریشان چون کنم</p></div></div>