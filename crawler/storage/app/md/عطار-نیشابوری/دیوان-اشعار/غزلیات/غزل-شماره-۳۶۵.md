---
title: >-
    غزل شمارهٔ ۳۶۵
---
# غزل شمارهٔ ۳۶۵

<div class="b" id="bn1"><div class="m1"><p>گر رخ او ذره‌ای جمال نماید</p></div>
<div class="m2"><p>طلعت خورشید را زوال نماید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور ز رخش لحظه‌ای نقاب برافتد</p></div>
<div class="m2"><p>هر دو جهان بازی خیال نماید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذرهٔ سرگشته در برابر خورشید</p></div>
<div class="m2"><p>نیست عجب گر ضعیف حال نماید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرد مسلمان اگر ز زلف سیاهش</p></div>
<div class="m2"><p>کفر نیارد مرا محال نماید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که به عشقش فروخت عقل به نقصان</p></div>
<div class="m2"><p>جمله نقصان او کمال نماید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوش غمش خون من بریخت و مرا گفت</p></div>
<div class="m2"><p>خون توام چشمه زلال نماید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق حرامت بود اگر تو ندانی</p></div>
<div class="m2"><p>کین همه خون‌ها مرا حلال نماید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در دهن مار نفس در بن چاه است</p></div>
<div class="m2"><p>هر که درین راه جاه و مال نماید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر تو درین راه خاک راه نگردی</p></div>
<div class="m2"><p>خاک تو را زود گوشمال نماید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چند چو طاوس در مقابل خورشید</p></div>
<div class="m2"><p>مرغ وجود تو پر و بال نماید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درنگر ای خودنمای تا سر مویی</p></div>
<div class="m2"><p>هر دو جهان پیش آن جمال نماید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر که درین دیرخانه دردکش افتاد</p></div>
<div class="m2"><p>کور شود از دو کون و لال نماید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دیر که دولت سرای عالم عشق است</p></div>
<div class="m2"><p>دردکشی در هزار سال نماید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مثل و مثالم طلب مکن تو درین دیر</p></div>
<div class="m2"><p>کاینه عطار را مثال نماید</p></div></div>