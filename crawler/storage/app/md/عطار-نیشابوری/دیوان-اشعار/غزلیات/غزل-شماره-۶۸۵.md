---
title: >-
    غزل شمارهٔ ۶۸۵
---
# غزل شمارهٔ ۶۸۵

<div class="b" id="bn1"><div class="m1"><p>ای دل و جان کاملان، گم شده در کمال تو</p></div>
<div class="m2"><p>عقل همه مقربان، بی خبر از وصال تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جمله تویی به خود نگر جمله ببین که دایما</p></div>
<div class="m2"><p>هجده هزار عالم است آینهٔ جمال تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا دل طالبانت را از تو دلالتی بود</p></div>
<div class="m2"><p>هرچه که هست در جهان هست همه مثال تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جملهٔ اهل دیده را از تو زبان ز کار شد</p></div>
<div class="m2"><p>نیست مجال نکته‌ای در صفت کمال تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرخ رونده قرن‌ها بی سر و پای در رهت</p></div>
<div class="m2"><p>پشت خمیده می‌رود در غم گوشمال تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا ابدش نشان و نام از دو جهان بریده شد</p></div>
<div class="m2"><p>هر که دمی جلاب خورد از قدح جلال تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مانده‌اند دور دور اهل دو کون از رهت</p></div>
<div class="m2"><p>زانکه وجود گم کند خلق در اتصال تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خشک شدیم بر زمین پرده ز روی برفکن</p></div>
<div class="m2"><p>تا لب خشک عاشقان تر شود از زلال تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرچه فرید در جهان هست فصیح‌تر کسی</p></div>
<div class="m2"><p>رد مکنش که در سخن هست زبانش لال تو</p></div></div>