---
title: >-
    غزل شمارهٔ ۳۳۹
---
# غزل شمارهٔ ۳۳۹

<div class="b" id="bn1"><div class="m1"><p>هر گدایی مرد سلطان کی شود</p></div>
<div class="m2"><p>پشه‌ای آخر سلیمان کی شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی عجب آن است کین مرد گدا</p></div>
<div class="m2"><p>چون که سلطان نیست سلطان کی شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس عجب کاری است بس نادر رهی</p></div>
<div class="m2"><p>این چو عین آن بود آن کی شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بدین برهان کنی از من طلب</p></div>
<div class="m2"><p>این سخن روشن به برهان کی شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا نگردی از وجود خود فنا</p></div>
<div class="m2"><p>بر تو این دشوار آسان کی شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتمش فانی شو و باقی تویی</p></div>
<div class="m2"><p>هر دو یکسان نیست یکسان کی شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه هم دریای عمان قطره‌ای است</p></div>
<div class="m2"><p>قطره‌ای دریای عمان کی شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر کسی را دیده دریابین نشد</p></div>
<div class="m2"><p>قطره‌بین باشد مسلمان کی شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا نگردد قطره و دریا یکی</p></div>
<div class="m2"><p>سنگ کفرت لعل ایمان کی شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جمله یک خورشید می‌بینم ولیک</p></div>
<div class="m2"><p>می‌ندانم بر تو رخشان کی شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر که خورشید جمال او ندید</p></div>
<div class="m2"><p>جان‌فشان بر روی جانان کی شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صد هزاران مرد می‌بینم ز عشق</p></div>
<div class="m2"><p>منتظر بنشسته تا جان کی شود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چند اندایی به گل خورشید را</p></div>
<div class="m2"><p>گل بدین درگه نگهبان کی شود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از کفی گل کان وجود آدم است</p></div>
<div class="m2"><p>آن چنان خورشید پنهان کی شود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر به کلی برنگیری گل ز راه</p></div>
<div class="m2"><p>پای در گل ره به پایان کی شود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نه چه می‌گویم تو مرد این نه‌ای</p></div>
<div class="m2"><p>هر صبی رستم به دستان کی شود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کی توانی شد تو مرد این حدیث</p></div>
<div class="m2"><p>هر مخنث مرد میدان کی شود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا نباشد همچو موسی عاشقی</p></div>
<div class="m2"><p>هر عصا در دست ثعبان کی شود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عمرت ای عطار تاوان کرده‌ای</p></div>
<div class="m2"><p>بر تو آن خورشید تابان کی شود</p></div></div>