---
title: >-
    غزل شمارهٔ ۳۱۲
---
# غزل شمارهٔ ۳۱۲

<div class="b" id="bn1"><div class="m1"><p>دل نظر بر روی آن شمع جهان می‌افکند</p></div>
<div class="m2"><p>تن به جای خرقه چون پروانه جان می‌افکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بود غوغای عشقش بر کنار عالمی</p></div>
<div class="m2"><p>دل ز شوقش خویشتن را در میان می‌افکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلف او صد توبه را در یک نفس می‌بشکند</p></div>
<div class="m2"><p>چشم او صد صید را در یک زمان می‌افکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طرهٔ مشکینش تابی در فلک می‌آورد</p></div>
<div class="m2"><p>پستهٔ شیرینش شوری در جهان می‌افکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سبز پوشان فلک ماه زمینش خوانده‌اند</p></div>
<div class="m2"><p>زانکه رویش غلغلی در آسمان می‌افکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا ابد کامش ز شیرینی نگردد تلخ و تیز</p></div>
<div class="m2"><p>هر که نام آن شکر لب بر زبان می‌افکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترکم آن دارد سر آن چون ندارد چون کنم</p></div>
<div class="m2"><p>هندوی خود را چنین در پا از آن می‌افکند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچو دف حلقه به گوش او شدم با این همه</p></div>
<div class="m2"><p>بر تنم چون چنگ هر رگ در فغان می‌افکند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گاهگاهی گویدم هستم یقین من زان تو</p></div>
<div class="m2"><p>لاجرم عطار را اندر گمان می‌افکند</p></div></div>