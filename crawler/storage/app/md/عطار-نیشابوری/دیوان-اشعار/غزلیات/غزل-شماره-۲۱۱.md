---
title: >-
    غزل شمارهٔ ۲۱۱
---
# غزل شمارهٔ ۲۱۱

<div class="b" id="bn1"><div class="m1"><p>چون باد صبا سوی چمن تاختن آورد</p></div>
<div class="m2"><p>گویی به غنیمت همه مشک ختن آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان تاختنش یوسف دل گر نشد افگار</p></div>
<div class="m2"><p>پس از چه سبب غرقه به خون پیرهن آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اشکال بدایع همه در پردهٔ رشکند</p></div>
<div class="m2"><p>زین شکل که از پرده برون یاسمن آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرگز ز گل و مشک نیفتاد به صحرا</p></div>
<div class="m2"><p>زین بوی که از نافه به صحرا سمن آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صد بیضهٔ عنبر نخرد کس به جوی نیز</p></div>
<div class="m2"><p>زین رسم که در باغ کنون نسترن آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر لحظه صبا از پی صد راز نهانی</p></div>
<div class="m2"><p>از مشک برافکند و به گوش چمن آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن راز به طفلی همه عیسی صفتان را</p></div>
<div class="m2"><p>در مهد چو عیسی به شکر در سخن آورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون کرد گل سرخ عرق از رخ یارم</p></div>
<div class="m2"><p>آبی چو گلابش ز صفا در دهن آورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لاله چو شهیدان همه آغشته به خون شد</p></div>
<div class="m2"><p>سر از غم کم عمری خود در کفن آورد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اول نفس از مشک چو عطار همی زد</p></div>
<div class="m2"><p>آخر جگری سوخته دل‌تر ز من آورد</p></div></div>