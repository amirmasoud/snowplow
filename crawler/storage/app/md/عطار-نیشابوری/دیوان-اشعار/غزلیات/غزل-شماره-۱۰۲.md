---
title: >-
    غزل شمارهٔ ۱۰۲
---
# غزل شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>ای دلشده دلربای من کیست</p></div>
<div class="m2"><p>از جای شدم به جای من کیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیگانه شدم ز هر دو عالم</p></div>
<div class="m2"><p>واگه نه که آشنای من کیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ره گم کردم درین بیابان</p></div>
<div class="m2"><p>کو رهبر و رهنمای من کیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان می‌کاهم درین ره دور</p></div>
<div class="m2"><p>پیک ره جان‌فزای من کیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صد بار بریختند خونم</p></div>
<div class="m2"><p>در عهدهٔ خون‌بهای من کیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر دم گرهی عظیم افتد</p></div>
<div class="m2"><p>در پرده گره‌گشای من کیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صد کار فتاده هر کسی را</p></div>
<div class="m2"><p>غمخوارهٔ من برای من کیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>محرومم ازین طلب که دارم</p></div>
<div class="m2"><p>مطلوب حرم‌سرای من کیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر من سجلی کنم درین کار</p></div>
<div class="m2"><p>جز زردی رخ گوای من کیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر گفت فرید ماجرایی</p></div>
<div class="m2"><p>اشنودهٔ ماجرای من کیست</p></div></div>