---
title: >-
    غزل شمارهٔ ۶۷۲
---
# غزل شمارهٔ ۶۷۲

<div class="b" id="bn1"><div class="m1"><p>هر که جان درباخت بر دیدار او</p></div>
<div class="m2"><p>صد هزاران جان شود ایثار او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا توانی در فنای خویش کوش</p></div>
<div class="m2"><p>تا شوی از خویش برخودار او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم مشتاقان روی دوست را</p></div>
<div class="m2"><p>نسیه نبود پرتو رخسار او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقد باشد اهل دل را روز و شب</p></div>
<div class="m2"><p>در مقام معرفت دیدار او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوست یک دم نیست خاموش از سخن</p></div>
<div class="m2"><p>گوش کو تا بشنود گفتار او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پنبه را از گوش بر باید کشید</p></div>
<div class="m2"><p>بو که یکدم بشنوی اسرار او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نور و نار او بهشت و دوزخ است</p></div>
<div class="m2"><p>پای برتر نه ز نور و نار او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوزخ مردان بهشت دیگران است</p></div>
<div class="m2"><p>درگذر زین هر دو در زنهار او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کز امید وصل و از بیم فراق</p></div>
<div class="m2"><p>جان مردان خون شد اندر کار او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عاشقان خسته دل بین صد هزار</p></div>
<div class="m2"><p>سرنگون آویخته از دار او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همچو مرغ نیم بسمل مانده‌اند</p></div>
<div class="m2"><p>بیخود و سرگشته از تیمار او</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صد هزاران رفته‌اند و کس ندید</p></div>
<div class="m2"><p>تا که دید از رفتگان آثار او</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زاد عطار اندرین ره هیچ نیست</p></div>
<div class="m2"><p>جز امید رحمت بسیار او</p></div></div>