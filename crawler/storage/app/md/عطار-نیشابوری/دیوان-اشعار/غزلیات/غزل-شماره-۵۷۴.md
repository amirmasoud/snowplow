---
title: >-
    غزل شمارهٔ ۵۷۴
---
# غزل شمارهٔ ۵۷۴

<div class="b" id="bn1"><div class="m1"><p>چاره نیست از توام چه چاره کنم</p></div>
<div class="m2"><p>تا به تو از همه کناره کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چکنم تا همه یکی بینم</p></div>
<div class="m2"><p>به یکی در همه نظاره کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنچه زو هیچ ذره پنهان نیست</p></div>
<div class="m2"><p>همچو خورشید آشکاره کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ذره‌آی چون هزار عالم هست</p></div>
<div class="m2"><p>پرده بر ذره ذره پاره کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا که هر ذره را چو خورشیدی</p></div>
<div class="m2"><p>بر براق فلک سواره کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صد هزاران هزار عالم را</p></div>
<div class="m2"><p>پیش روی تو پیشکاره کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پس به یک یک نفس هزار جهان</p></div>
<div class="m2"><p>تحفهٔ چون تو ماه پاره کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون کنم قصد این سلوک شگرف</p></div>
<div class="m2"><p>کوکب کفش از ستاره کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شیر دوشم هزار دریا بیش</p></div>
<div class="m2"><p>لیک پستان ز سنگ خاره کنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ذره‌های دو کون را زان شیر</p></div>
<div class="m2"><p>همچو اطفال شیرخواره کنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون کمال بلوغ ممکن نیست</p></div>
<div class="m2"><p>چکنم گور گاهواره کنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای عجب چون بسازم این همه کار</p></div>
<div class="m2"><p>هیچ باشد همه چه چاره کنم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عاقبت چون فلک فرو ریزم</p></div>
<div class="m2"><p>این روش گر هزار باره کنم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همه چون چرخ گرد خود گردم</p></div>
<div class="m2"><p>گرچه خورشید پشتواره کنم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نرهم از دو کون یک سر موی</p></div>
<div class="m2"><p>مگر از خویشتن گذاره کنم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون ز معشوق محو گشت فرید</p></div>
<div class="m2"><p>تا کیش مرغ عشق باره کنم</p></div></div>