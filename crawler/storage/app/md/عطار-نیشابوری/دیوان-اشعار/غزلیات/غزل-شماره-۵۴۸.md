---
title: >-
    غزل شمارهٔ ۵۴۸
---
# غزل شمارهٔ ۵۴۸

<div class="b" id="bn1"><div class="m1"><p>ای جان و جهان رویت پیدا نکنی دانم</p></div>
<div class="m2"><p>تا جان و جهانی را شیدا نکنی دانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پشت من یکتا دل از زلف دوتا کردی</p></div>
<div class="m2"><p>و آن زلف دوتا هرگز یکتا نکنی دانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر جور کنی ور نی تا کار تو می‌ماند</p></div>
<div class="m2"><p>زین شیوه بسی افتد عمدا نکنی دانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در غارت جان و دل در زلف و لبت بازی</p></div>
<div class="m2"><p>زیرا که چنین کاری تنها نکنی دانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون عاشق غم‌کش را در خاک کنی پنهان</p></div>
<div class="m2"><p>بر خویش نظر آری پیدا نکنی دانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتی کنم از بوسی روزی دهنت شیرین</p></div>
<div class="m2"><p>این خود به زبان گویی اما نکنی دانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اندر عوض بوسی گر جان و تنم بردی</p></div>
<div class="m2"><p>تا عاشق سودایی رسوا نکنی دانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتی که شبی با تو دستی کنم اندر کش</p></div>
<div class="m2"><p>یارب چه دروغ است این با ما نکنی دانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتی که جفا کردم در حق تو ای عطار</p></div>
<div class="m2"><p>آخر همه کس داند کانها نکنی دانم</p></div></div>