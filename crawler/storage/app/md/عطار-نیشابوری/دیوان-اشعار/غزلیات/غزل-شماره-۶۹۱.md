---
title: >-
    غزل شمارهٔ ۶۹۱
---
# غزل شمارهٔ ۶۹۱

<div class="b" id="bn1"><div class="m1"><p>ای دل مبتلای من شیفتهٔ هوای تو</p></div>
<div class="m2"><p>دیده دلم بسی بلا آن همه از برای تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رای مرا به یک زمان جمله برای خود مران</p></div>
<div class="m2"><p>چون ز برای خود کنم چند کشم بلای تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نی ز برای تو به جان بار بلای تو کشم</p></div>
<div class="m2"><p>عشق تو و بلای جان، جان من و وفای تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باد جهان بی وفا دشمن من ز جان و دل</p></div>
<div class="m2"><p>گر نکنم ز دوستی از دل و جان هوای تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پرده ز روی برفکن زانکه بماند تا ابد</p></div>
<div class="m2"><p>جملهٔ جان عاشقان مست می لقای تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان و دلی است بنده را بر تو فشانم اینکه هست</p></div>
<div class="m2"><p>نی که محقری است خود کی بود این سزای تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم من از گریستن تیره شدی اگر مرا</p></div>
<div class="m2"><p>گاه و به‌گاه نیستی سرمه ز خاک پای تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر ببری به دلبری از سر زلف جان من</p></div>
<div class="m2"><p>زنده شوم به یک نفس از لب جانفزای تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هست ز مال این جهان نقد فرید نیم جان</p></div>
<div class="m2"><p>می نپذیری این ازو پس چه کند برای تو</p></div></div>