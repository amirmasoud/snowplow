---
title: >-
    غزل شمارهٔ ۶۳۰
---
# غزل شمارهٔ ۶۳۰

<div class="b" id="bn1"><div class="m1"><p>چون نیاید سر عشقت در بیان</p></div>
<div class="m2"><p>همچو طفلان مهر دارم بر زبان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون عبارت محرم عشق تو نیست</p></div>
<div class="m2"><p>چون دهد نامحرم از پیشان نشان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنک ازو سگ می‌کند پهلو تهی</p></div>
<div class="m2"><p>دوستکانی چون خورد با پهلوان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون زبان در عشق تو بر هیچ نیست</p></div>
<div class="m2"><p>لب فرو بستم قلم کردم زبان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو مرغ نیم بسمل در رهت</p></div>
<div class="m2"><p>در میان خاک و خون گشتم نهان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دور از تو جان ز من گیرد کنار</p></div>
<div class="m2"><p>گر مرا بیرون نیاری زین میان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوش عشق تو درآمد نیم شب</p></div>
<div class="m2"><p>از رهی دزدیده یعنی راه جان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت صد دریا ز خون دل بیار</p></div>
<div class="m2"><p>تا در آشامم که مستم این زمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرغ دل آوارهٔ دیرینه بود</p></div>
<div class="m2"><p>باز یافت از عشق حالی آشیان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در پرید و عشق را در بر گرفت</p></div>
<div class="m2"><p>عقل و جان را کارد آمد به استخوان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عقل فانی گشت و جان معدوم شد</p></div>
<div class="m2"><p>عشق و دل ماندند با هم جاودان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عشق با دل گشت و دل با عشق شد</p></div>
<div class="m2"><p>زین عجب‌تر قصه نبود در جهان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دیدن و دانستن اینجا باطل است</p></div>
<div class="m2"><p>بودن آن کار نه علم و بیان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون بباشی فانی مطلق ز خویش</p></div>
<div class="m2"><p>هست مطلق گردی اندر لامکان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جان و جانان هر دو نتوان یافتن</p></div>
<div class="m2"><p>گر همی جانانت باید جان‌فشان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا کی ای عطار گویی راز عشق</p></div>
<div class="m2"><p>راز می‌گویی طلب کن رازدان</p></div></div>