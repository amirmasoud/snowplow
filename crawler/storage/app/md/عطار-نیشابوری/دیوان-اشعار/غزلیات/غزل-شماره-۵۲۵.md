---
title: >-
    غزل شمارهٔ ۵۲۵
---
# غزل شمارهٔ ۵۲۵

<div class="b" id="bn1"><div class="m1"><p>اگر برشمارم غم بیشمارم</p></div>
<div class="m2"><p>ندارند باور یکی از هزارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیاید در انگشت این غم شمردن</p></div>
<div class="m2"><p>مگر اشک می‌ریزم و می‌شمارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر انگشت نتواند این غم به سر برد</p></div>
<div class="m2"><p>به سر می‌برد دیدهٔ اشکبارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگرچه فشاندم بسی اشک خونین</p></div>
<div class="m2"><p>مبر ظن که من اشک دیگر نبارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرفتم ز خلق زمانه کناری</p></div>
<div class="m2"><p>فشاندم بسی اشک خون در کنارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو روی نگارم ز چشمم برون شد</p></div>
<div class="m2"><p>ز شوقش به خون روی خود می‌نگارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه کاری بر آید ز دست من اکنون</p></div>
<div class="m2"><p>که شد کارم از دست و از دست کارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا هست در دل بسی سر پنهان</p></div>
<div class="m2"><p>ندانم که هرگز شود آشکارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو صاحب دلی اهل این سر ندیدم</p></div>
<div class="m2"><p>همه سر به مهرش به دل می‌سپارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه گویی که عطار عیسی دمم من</p></div>
<div class="m2"><p>چو زهره ندارم که یکدم برآرم</p></div></div>