---
title: >-
    غزل شمارهٔ ۳۶۲
---
# غزل شمارهٔ ۳۶۲

<div class="b" id="bn1"><div class="m1"><p>آن روی به جز قمر که آراید</p></div>
<div class="m2"><p>وان لعل به جز شکر که فرساید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس جان که ز پرده در جهان افتد</p></div>
<div class="m2"><p>چون روی ز زیر پرده بنماید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در زیبایی و عالم افروزی</p></div>
<div class="m2"><p>رویی دارد چنان که می‌باید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورشید چو روی او همی بیند</p></div>
<div class="m2"><p>می‌گردد و پشت دست می‌خاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امروز قیامتی است از خطش</p></div>
<div class="m2"><p>خطی که هزار فتنه می‌زاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گویی ز بنفشه گلستانش را</p></div>
<div class="m2"><p>مشاطهٔ حسن می‌بیاراید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آورد خطی و دل ببرد از من</p></div>
<div class="m2"><p>جان منتظر است تا چه فرماید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زین بیع و شری که خط او دارد</p></div>
<div class="m2"><p>جز خون جگر مرا چه بگشاید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>الحق ز معاملان خط او</p></div>
<div class="m2"><p>دیری است که بوی مشک می‌آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زین گونه که خط او درآبم زد</p></div>
<div class="m2"><p>شک نیست که دوستی بیفزاید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عطار اگر چنین کند سودا</p></div>
<div class="m2"><p>چه سود چو جان او نیاساید</p></div></div>