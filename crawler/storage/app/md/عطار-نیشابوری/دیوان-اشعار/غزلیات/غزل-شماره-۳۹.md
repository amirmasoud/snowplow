---
title: >-
    غزل شمارهٔ ۳۹
---
# غزل شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>عزیزا هر دو عالم سایهٔ توست</p></div>
<div class="m2"><p>بهشت و دوزخ از پیرایهٔ توست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تویی از روی ذات آئینهٔ شاه</p></div>
<div class="m2"><p>شه از روی صفاتی آیهٔ توست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که داند تا تو اندر پردهٔ غیب</p></div>
<div class="m2"><p>چه چیزی و چه اصلی مایهٔ توست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو طفلی وانکه در گهوارهٔ تو</p></div>
<div class="m2"><p>تو را کج می‌کند هم دایهٔ توست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر بالغ شوی ظاهر ببینی</p></div>
<div class="m2"><p>که صد عالم فزون‌تر پایهٔ توست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو اندر پردهٔ غیبی و آن چیز</p></div>
<div class="m2"><p>که می‌بینی تو آن خود سایهٔ توست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برآی از پرده و بیع و شرا کن</p></div>
<div class="m2"><p>که هر دو کون یک سرمایهٔ توست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو از عطار بشنو کانچه اصل است</p></div>
<div class="m2"><p>برون نی از تو و همسایهٔ توست</p></div></div>