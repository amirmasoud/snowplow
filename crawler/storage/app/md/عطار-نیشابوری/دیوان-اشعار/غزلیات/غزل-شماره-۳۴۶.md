---
title: >-
    غزل شمارهٔ ۳۴۶
---
# غزل شمارهٔ ۳۴۶

<div class="b" id="bn1"><div class="m1"><p>گر دلبرم به یک شکر از لب زبان دهد</p></div>
<div class="m2"><p>مرغ دلم ز شوق به شکرانه جان دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌ندهد او به جان گرانمایه بوسه‌ای</p></div>
<div class="m2"><p>پنداشتی که بوسه چنین رایگان دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون کس نیافت از دهن تنگ او خبر</p></div>
<div class="m2"><p>هر بی خبر چگونه خبر زان دهان دهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>معدوم شیء گوید اگر نقطهٔ دلم</p></div>
<div class="m2"><p>جز نام از خیال دهانش نشان دهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مردی محال گوی بود آنکه بی خبر</p></div>
<div class="m2"><p>یک موی فی‌المثل خبر از آن میان دهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون دید آفتاب که آن ماه هشت خلد</p></div>
<div class="m2"><p>از روی خود زکات به هفت آسمان دهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>افتاد در غروب و فروشد خجل زده</p></div>
<div class="m2"><p>تا نوبت طلوع بدان دلستان دهد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در آفتاب صد شکن آرم چو زلف او</p></div>
<div class="m2"><p>گر زلف او مرا سر مویی امان دهد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ابروی چون کمانش که آن غمزه تیر اوست</p></div>
<div class="m2"><p>هر ساعتی چو تیر سرم در جهان دهد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گویی که جور هندوی زلفش تمام نیست</p></div>
<div class="m2"><p>آخر به ترک مست که تیر و کمان دهد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از عشق او چگونه کنم توبه چون دلم</p></div>
<div class="m2"><p>صد توبهٔ درست به یک پاره نان دهد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن دارد آن نگار ز عطار چون گذشت</p></div>
<div class="m2"><p>امکان ندارد آنکه کسی شرح آن دهد</p></div></div>