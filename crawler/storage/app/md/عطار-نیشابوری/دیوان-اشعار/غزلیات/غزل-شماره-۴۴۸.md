---
title: >-
    غزل شمارهٔ ۴۴۸
---
# غزل شمارهٔ ۴۴۸

<div class="b" id="bn1"><div class="m1"><p>هر که دایم نیست ناپروای عشق</p></div>
<div class="m2"><p>او چه داند قیمت سودای عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق را جانی بباید بیقرار</p></div>
<div class="m2"><p>در میان فتنه سر غوغای عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جمله چون امروز در خود مانده‌اند</p></div>
<div class="m2"><p>کس چه داند قیمت فردای عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده‌ای کو تا ببیند صد هزار</p></div>
<div class="m2"><p>واله و سرگشته در صحرای عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس سر گردنکشان کاندر جهان</p></div>
<div class="m2"><p>پست شد چون خاک زیرپای عشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در جهان شوریدگان هستند و نیست</p></div>
<div class="m2"><p>هر که او شوریده شد شیدای عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون که نیست از عشق جانت را خبر</p></div>
<div class="m2"><p>کی بود هرگز تو را پروای عشق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاشقان دانند قدر عشق دوست</p></div>
<div class="m2"><p>تو چه دانی چون نه‌ای دانای عشق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشم دل آخر زمانی باز کن</p></div>
<div class="m2"><p>تا عجایب بینی از دریا عشق</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در نشیب نیستی آرام گیر</p></div>
<div class="m2"><p>تا برآرندت به سر بالای عشق</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خیز ای عطار و جان ایثار کن</p></div>
<div class="m2"><p>زانکه در عالم تویی مولای عشق</p></div></div>