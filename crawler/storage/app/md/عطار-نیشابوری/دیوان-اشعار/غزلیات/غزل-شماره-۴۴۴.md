---
title: >-
    غزل شمارهٔ ۴۴۴
---
# غزل شمارهٔ ۴۴۴

<div class="b" id="bn1"><div class="m1"><p>هر روز که جلوه می‌کند رویش</p></div>
<div class="m2"><p>بر می‌خیزد قیامت ز کویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌نتوان دید روی او لیکن</p></div>
<div class="m2"><p>می‌بتوان دید روی در رویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌نتوان یافت سوی او راهی</p></div>
<div class="m2"><p>ای بس که برآمدم ز هر سویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا فال گرفته‌ام جمال او</p></div>
<div class="m2"><p>چون قرعه بگشته‌ام به پهلویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در هر نفسم هزار جان باید</p></div>
<div class="m2"><p>تا صید کنند کمند گیسویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر روز به نو خراج می‌آرند</p></div>
<div class="m2"><p>از هندستان به هندوی مویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان بر کف دست می‌رسد هر شب</p></div>
<div class="m2"><p>از ترکستان هزار هندویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد حلقه به گوش لؤلؤ لالا</p></div>
<div class="m2"><p>در لالایی درج لولویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خورشید که تیغ می‌زند در میغ</p></div>
<div class="m2"><p>افکند سپر ز جزع جادویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل را به دهان شیر می‌خواند</p></div>
<div class="m2"><p>رو به بازی چشم آهویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خواهم که ببیند ابرویش رستم</p></div>
<div class="m2"><p>تا هست خود این کمان به بازویش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رستم به هزار سال چون زالی</p></div>
<div class="m2"><p>بر زه نکند کمان ابرویش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عطار که طاق از ابروی او شد</p></div>
<div class="m2"><p>دردی دارد که نیست دارویش</p></div></div>