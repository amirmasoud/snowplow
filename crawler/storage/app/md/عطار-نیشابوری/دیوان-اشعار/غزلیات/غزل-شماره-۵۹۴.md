---
title: >-
    غزل شمارهٔ ۵۹۴
---
# غزل شمارهٔ ۵۹۴

<div class="b" id="bn1"><div class="m1"><p>گرچه در عشق تو جان درباختیم</p></div>
<div class="m2"><p>قیمت سودای تو نشناختیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سالها بر مرکب فکرت مدام</p></div>
<div class="m2"><p>در ره سودای تو می‌باختیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خود تو در دل بودی و ما از غرور</p></div>
<div class="m2"><p>یک نفس با تو نمی‌پرداختیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون بگستردی بساط داوری</p></div>
<div class="m2"><p>پیش عشقت جان و دل درباختیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر دوعالم سرفرازی یافتیم</p></div>
<div class="m2"><p>تا به سودای تو سر بفراختیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آتش عشقت درآمد گرد دل</p></div>
<div class="m2"><p>ما چو شمع از تف آن بگداختیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر امید وصل تو پروانه‌وار</p></div>
<div class="m2"><p>خویشتن در آتشت انداختیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گاه چون پروانه‌ای می‌سوختیم</p></div>
<div class="m2"><p>گاه با آن سوختن می‌ساختیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همچو عطار از جهان بردیم دست</p></div>
<div class="m2"><p>تا نوای درد تو بنواختیم</p></div></div>