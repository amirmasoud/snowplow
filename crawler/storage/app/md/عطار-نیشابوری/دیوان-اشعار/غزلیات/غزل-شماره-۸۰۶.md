---
title: >-
    غزل شمارهٔ ۸۰۶
---
# غزل شمارهٔ ۸۰۶

<div class="b" id="bn1"><div class="m1"><p>ای ساقی از آن قدح که دانی</p></div>
<div class="m2"><p>پیش آر سبک مکن گرانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک قطره شراب در صبوحی</p></div>
<div class="m2"><p>باشد که به حلق ما چکانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان پیش خمار در سر آید</p></div>
<div class="m2"><p>یک باده به دست ما رسانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگذر تو ز خویش و از قرابات</p></div>
<div class="m2"><p>پیش آر قرابهٔ مغانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در عقل مغیش تا نبینی</p></div>
<div class="m2"><p>وز علم مجوس تا نخوانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کین جای نه جای قیل و قال است</p></div>
<div class="m2"><p>کافسانه کنی و قصه خوانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این جای مقام کم زنان است</p></div>
<div class="m2"><p>تو مرد ردا و طیلسانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ساقی تو بیا و بر کفم نه</p></div>
<div class="m2"><p>یک کوزهٔ آب زندگانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یک قطرهٔ درد اگر بنوشی</p></div>
<div class="m2"><p>یابی تو حیات جاودانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ساقی شو و راوقی در انداز</p></div>
<div class="m2"><p>زان لعل چو در که می‌چکانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عطار بیا ز پرده بیرون</p></div>
<div class="m2"><p>تا چند سخن ز پرده رانی</p></div></div>