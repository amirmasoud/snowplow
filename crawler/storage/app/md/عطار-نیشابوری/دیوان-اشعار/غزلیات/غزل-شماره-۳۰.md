---
title: >-
    غزل شمارهٔ ۳۰
---
# غزل شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>اینت گم گشته دهانی که توراست</p></div>
<div class="m2"><p>وینت نابوده میانی که توراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دو چشم تو جهان پرشور است</p></div>
<div class="m2"><p>اینت شوریده جهانی که توراست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جادوان را به سخن خشک کنی</p></div>
<div class="m2"><p>خه زهی چرب‌زبانی که توراست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آخر این ناز تو هم در گذرد</p></div>
<div class="m2"><p>چند مانده است زمانی که توراست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی از من شکری باید خواست</p></div>
<div class="m2"><p>اینت آشفته دهانی که توراست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون بهای شکرت صد جان است</p></div>
<div class="m2"><p>چه کنم نیمهٔ جانی که توراست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مده ای ماه کسی را شکری</p></div>
<div class="m2"><p>که شکر هست زبانی که توراست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خط معزولی حسن تو دمید</p></div>
<div class="m2"><p>سست ازآن گشت عنانی که توراست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قیر شد گرد رخت غالیه گون</p></div>
<div class="m2"><p>خطت از غالیه دانی که توراست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون خط او بدمد ای عطار</p></div>
<div class="m2"><p>کم شود آه و فغانی که توراست</p></div></div>