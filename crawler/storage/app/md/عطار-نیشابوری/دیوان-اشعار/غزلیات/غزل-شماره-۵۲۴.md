---
title: >-
    غزل شمارهٔ ۵۲۴
---
# غزل شمارهٔ ۵۲۴

<div class="b" id="bn1"><div class="m1"><p>نظری به کار من کن که ز دست رفت کارم</p></div>
<div class="m2"><p>به کسم مکن حواله که به جز تو کس ندارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منم و هزار حسرت که در آرزوی رویت</p></div>
<div class="m2"><p>همه عمر من برفت و بنرفت هیچ کارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر به دستگیری بپذیری اینت منت</p></div>
<div class="m2"><p>واگر نه رستخیزی ز همه جهان برآرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه کمی درآید آخر به شرابخانهٔ تو</p></div>
<div class="m2"><p>اگر از شراب وصلت ببری ز سر خمارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو نیم سزای شادی ز خودم مدار بی غم</p></div>
<div class="m2"><p>که درین چنین مقامی غم توست غمگسارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز غم تو همچو شمعم که چو شمع در غم تو</p></div>
<div class="m2"><p>چو نفس زنم بسوزم چو بخندم اشکبارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو زکار شد زبانم بروم به پیش خلقی</p></div>
<div class="m2"><p>غم تو به خون دیده همه بر رخم نگارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز توام من آنچه هستم که تو گرنه‌ای نیم من</p></div>
<div class="m2"><p>که تویی که آفتابی و منم که ذره‌وارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر از تو جان عطار اثر کمال یابد</p></div>
<div class="m2"><p>منم آنکه از دو عالم به کمال اختیارم</p></div></div>