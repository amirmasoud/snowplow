---
title: >-
    غزل شمارهٔ ۶۱
---
# غزل شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>ذره‌ای اندوه تو از هر دو عالم خوشتر است</p></div>
<div class="m2"><p>هر که گوید نیست دانی کیست آن کس کافر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کافری شادی است و آن شادی نه از اندوه تو</p></div>
<div class="m2"><p>نی که کار او ز اندوه و ز شادی برتر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن کزو غافل بود دیوانه‌ای نامحرم است</p></div>
<div class="m2"><p>وانکه زو فهمی کند دیوانه‌ای صورتگر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کس سر مویی ندارد از مسما آگهی</p></div>
<div class="m2"><p>اسم می‌گویند و چندان کاسم گویی دیگر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرچه در فهم تو آید آن بود مفهوم تو</p></div>
<div class="m2"><p>کی بود مفهوم تو او کو از آن عالی‌تر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای عجب بحری است پنهان لیک چندان آشکار</p></div>
<div class="m2"><p>کز نم او ذره ذره تا ابد موج‌آور است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صورتی کان در درون آینه از عکس توست</p></div>
<div class="m2"><p>در درون آینه هر جا که گویی مضمر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر تو آن صورت در آئینه ببینی عمرها</p></div>
<div class="m2"><p>زو نیابی ذره‌ای کان در محلی انور است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای عجب با جملهٔ آهن به هم آن صورت است</p></div>
<div class="m2"><p>گرچه بیرون است ازآن آهن بدان آهن در است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صورتی چون هست با چیزی و بی چیزی به هم</p></div>
<div class="m2"><p>در صفت رهبر چنین گر جان پاکت رهبر است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ور مثالی دیگرت باید به حکم او نگر</p></div>
<div class="m2"><p>صورتش خاک است و برتر سنگ و برتر زان زر است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا که در دریای دل عطار کلی غرق شد</p></div>
<div class="m2"><p>گوییا تیغ زبانش ابر باران گوهر است</p></div></div>