---
title: >-
    غزل شمارهٔ ۲۷۰
---
# غزل شمارهٔ ۲۷۰

<div class="b" id="bn1"><div class="m1"><p>نور روی تو را نظر نکشد</p></div>
<div class="m2"><p>سوز عشق تو را جگر نکشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باد خاک سیاه بر سر آنک</p></div>
<div class="m2"><p>خاک کوی تو در بصر نکشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آتش عشق بیدلان تو را</p></div>
<div class="m2"><p>هفت آتش گه سقر نکشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از درازی و دوری راهت</p></div>
<div class="m2"><p>هیچ کس راه تو به سر نکشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که رهت جز به قدر و قوت ما</p></div>
<div class="m2"><p>قدر یک گام بیشتر نکشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درد هر کس به قدر طاقت اوست</p></div>
<div class="m2"><p>کانچه عیسی کشید خر نکشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کوه اندوه و بار محنت تو</p></div>
<div class="m2"><p>چون کشد دل که بحر و بر نکشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خود عجب نبود آنکه از ره عجز</p></div>
<div class="m2"><p>پشه‌ای پیل را به بر نکشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با کمان فلک به هیچ سبیل</p></div>
<div class="m2"><p>بازوی هیچ پشه در نکشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هیچکس عشق چون تو معشوقی</p></div>
<div class="m2"><p>به ترازوی عقل بر نکشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون کشد کوه بی نهایت را</p></div>
<div class="m2"><p>آن ترازو که بیش زر نکشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وزن عشق تو عقل کی داند</p></div>
<div class="m2"><p>عشق تو عقل مختصر نکشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عشقت از دیرها نگردد باز</p></div>
<div class="m2"><p>تا که ابدال را بدر نکشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دل عطار در غم تو چنان است</p></div>
<div class="m2"><p>که غم دیگران دگر نکشد</p></div></div>