---
title: >-
    غزل شمارهٔ ۸۳۳
---
# غزل شمارهٔ ۸۳۳

<div class="b" id="bn1"><div class="m1"><p>ای لب گلگونت جام خسروی</p></div>
<div class="m2"><p>پیشهٔ شبرنگ زلفت شبروی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پهلوی خورشید مشک‌آلود کرد</p></div>
<div class="m2"><p>خط تو یعنی که هستم پهلوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردم چشمت بدان خردی که هست</p></div>
<div class="m2"><p>می‌ببندد دست چرخ از جادوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کی توان گفت از دهان تو سخن</p></div>
<div class="m2"><p>زانکه صورت نیست آن جز معنوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاه همچون آفتابی از جمال</p></div>
<div class="m2"><p>گاه همچون ماه از بس نیکوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من ندانم کافتابی یا مهی</p></div>
<div class="m2"><p>کژ چه گویم راست به از هر دوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشقان را جامه می‌گردد قبا</p></div>
<div class="m2"><p>تو کله بنهاده کژ خوش می‌روی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفته بودی آنکه دل برد از تو کیست</p></div>
<div class="m2"><p>من ندارم زهره تا گویم توی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ور بگویم من که تو بردی دلم</p></div>
<div class="m2"><p>دل به من ندهی و هرگز نشنوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل ندارم زان ضعیفم همچو موی</p></div>
<div class="m2"><p>تو دلم ده تا شود کارم قوی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من که تخم نیکوی کشتم مدام</p></div>
<div class="m2"><p>بر نخوردم از تو الا بدخوی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو که با من تخم کین کاری همه</p></div>
<div class="m2"><p>درو نبود کانچه کاری بدروی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در سخن عطار اگر معجز نمود</p></div>
<div class="m2"><p>تو به اعجاز سخن می‌نگروی</p></div></div>