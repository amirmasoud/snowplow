---
title: >-
    غزل شمارهٔ ۱۳
---
# غزل شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>برقع از ماه برانداز امشب</p></div>
<div class="m2"><p>ابرش حسن برون تاز امشب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده بر راه نهادم همه روز</p></div>
<div class="m2"><p>تا درآیی تو به اعزاز امشب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من و تو هر دو تمامیم بهم</p></div>
<div class="m2"><p>هیچکس را مده آواز امشب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کارم انجام نگیرد که چو دوش</p></div>
<div class="m2"><p>سرکشی می‌کنی آغاز امشب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه کار تو همه پرده‌دری است</p></div>
<div class="m2"><p>پرده زین کار مکن باز امشب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو چو شمعی و جهان از تو چو روز</p></div>
<div class="m2"><p>من چو پروانهٔ جانباز امشب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو پروانه به پای افتادم</p></div>
<div class="m2"><p>سر ازین بیش میفراز امشب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عمر من بیش شبی نیست چو شمع</p></div>
<div class="m2"><p>عمر شد، چند کنی ناز امشب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نفسی در رخ من خند چو صبح</p></div>
<div class="m2"><p>همچو شمعم چه نهی گاز امشب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بوده‌ام بی تو به‌صد سوز امروز</p></div>
<div class="m2"><p>چکنی کشتن من ساز امشب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرغ دل در قفس سینه ز شوق</p></div>
<div class="m2"><p>می‌کند قصد به پرواز امشب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دانه از مرغ دلم باز مگیر</p></div>
<div class="m2"><p>که شد از بانگ تو دمساز امشب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رازم از دم مفکن فاش چو صبح</p></div>
<div class="m2"><p>که تو بی همدم و همراز امشب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دل عطار نگر شیشه صفت</p></div>
<div class="m2"><p>سنگ بر شیشه مینداز امشب</p></div></div>