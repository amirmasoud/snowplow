---
title: >-
    غزل شمارهٔ ۸۰۲
---
# غزل شمارهٔ ۸۰۲

<div class="b" id="bn1"><div class="m1"><p>ای یک کرشمه تو غارتگر جهانی</p></div>
<div class="m2"><p>دشنام تو خریده ارزان خران به جانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آشفتهٔ رخ تو هرجا که ماهرویی</p></div>
<div class="m2"><p>دلدادهٔ لب تو هر جا که دلستانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر از دهان تنگت بوسی به من فرستی</p></div>
<div class="m2"><p>جان‌های تنگ بسته برهم نهم جهانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو خود دهان نداری چون بوسه خواهم از تو</p></div>
<div class="m2"><p>هرگز برون نگنجد بوس از چنین دهانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون تو میان نداری من با کنار رفتم</p></div>
<div class="m2"><p>چون دست درکش آرد کس با چنان میانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو یوسفی و هر دم زلف تو از نسیمی</p></div>
<div class="m2"><p>کرده روان به کنعان از مشک کاروانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیری است تا دل من از درد توست سوزان</p></div>
<div class="m2"><p>آخر دلت نسوزد بر درد من زمانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتی بخواه چیزی کان سودمندت آید</p></div>
<div class="m2"><p>کز سود کردن تو نبود مرا زیانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وقت بهار خواهم در نور شمع رویت</p></div>
<div class="m2"><p>من کرده در رخ تو هر لحظه گلفشانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عطار اگرت بیند یک شب چنان که گفتم</p></div>
<div class="m2"><p>صد جان تازه یابد آنگاه هر زمانی</p></div></div>