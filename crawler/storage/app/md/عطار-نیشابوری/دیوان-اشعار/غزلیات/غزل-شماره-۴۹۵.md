---
title: >-
    غزل شمارهٔ ۴۹۵
---
# غزل شمارهٔ ۴۹۵

<div class="b" id="bn1"><div class="m1"><p>هر شبی عشقت جگر می‌سوزدم</p></div>
<div class="m2"><p>همچو شمعی تا سحر می‌سوزدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی پر و بال توام تا عشق تو</p></div>
<div class="m2"><p>گاه بال و گاه پر می‌سوزدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون کنم در روی چون ماهت نظر</p></div>
<div class="m2"><p>کز فروغ تو نظر می‌سوزدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند دارم دیده بر راه امید</p></div>
<div class="m2"><p>کز نظر کردن بصر می‌سوزدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی جگر خوردن دمی در من نگر</p></div>
<div class="m2"><p>کز جگر خوردن جگر می‌سوزدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت با من ساز تا کم سوزمت</p></div>
<div class="m2"><p>گر نمی‌سازم بتر می‌سوزدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرد و گرمم می‌نسازد بی تو زانک</p></div>
<div class="m2"><p>سوز عشقت خشک و تر می‌سوزدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا بخواهم سوختن یکبارگی</p></div>
<div class="m2"><p>هر دم از نوعی دگر می‌سوزدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا قدم از سر گرفتم در رهش</p></div>
<div class="m2"><p>از قدم تا فرق سر می‌سوزدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تن زن ای عطار و عود عشق سوز</p></div>
<div class="m2"><p>تا به خلوتگاه بر می‌سوزدم</p></div></div>