---
title: >-
    غزل شمارهٔ ۷۲۳
---
# غزل شمارهٔ ۷۲۳

<div class="b" id="bn1"><div class="m1"><p>ای درس عشقت هر شبم تا روز تکرار آمده</p></div>
<div class="m2"><p>وی روز من بی روی تو همچون شب تار آمده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای مه غلام روی تو گشته زحل هندوی تو</p></div>
<div class="m2"><p>وی خور ز عکس روی تو چون ذره در کار آمده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای در سرم سودای تو جان و دلم شیدای تو</p></div>
<div class="m2"><p>گردون به زیر پای تو چون خاک ره خوار آمده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان بنده شد رای تورا روی دل‌آرای تو را</p></div>
<div class="m2"><p>خاک کف پای تو را چشمم به دیدار آمده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون بر بساط دلبری شطرنج عشقم می‌بری</p></div>
<div class="m2"><p>گشتم ز جان و دل بری ای یار عیار آمده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا نرد عشقت باختم شش را ز یک نشناختم</p></div>
<div class="m2"><p>چون جان و دل درباختم هستم به زنهار آمده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای جزع تو شکر فروش ای لعل تو گوهر فروش</p></div>
<div class="m2"><p>ای زلف تو عنبر فروش از پیش عطار آمده</p></div></div>