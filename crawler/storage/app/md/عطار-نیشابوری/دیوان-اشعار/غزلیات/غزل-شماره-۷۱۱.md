---
title: >-
    غزل شمارهٔ ۷۱۱
---
# غزل شمارهٔ ۷۱۱

<div class="b" id="bn1"><div class="m1"><p>ای ذره‌ای از نور تو بر عرش اعظم تافته</p></div>
<div class="m2"><p>از عرش اعظم در گذر بر هر دو عالم تافته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن ذره ذریت شده خورشید خاصیت شده</p></div>
<div class="m2"><p>سر تا قدم نیت شده بر جان آدم تافته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اولاد پیدا آمده خلقی به صحرا آمده</p></div>
<div class="m2"><p>پس بی‌محابا آمده بر بیش و بر کم تافته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک موی تو در صبحدم بر گاو و آهو زد رقم</p></div>
<div class="m2"><p>مشک است یا عنبر بهم موی تو بر هم تافته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر عاشقان روی تو بر ساکنان کوی تو</p></div>
<div class="m2"><p>در پرتو یک موی تو کاری است معظم تافته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عکس رخت از نه فلک بگذشته تا پشت سمک</p></div>
<div class="m2"><p>بی واسطه بر یک به یک نوری مسلم تافته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گه جان پر اسرار را کرده فدا دیدار را</p></div>
<div class="m2"><p>گاهی دل عطار را عشقت به یک دم تافته</p></div></div>