---
title: >-
    غزل شمارهٔ ۵۱۴
---
# غزل شمارهٔ ۵۱۴

<div class="b" id="bn1"><div class="m1"><p>تا عشق تو در میان جان دارم</p></div>
<div class="m2"><p>جان پیش در تو بر میان دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اشکم چو به صد زبان سخن گوید</p></div>
<div class="m2"><p>راز دل خویش چون نهان دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در عشق تو بس سبکدل افتادم</p></div>
<div class="m2"><p>کز بادهٔ عشق سر گران دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم چو به تو نمی‌رسم باری</p></div>
<div class="m2"><p>نامت همه روز بر زبان دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون کرد فراق تو زبان بندم</p></div>
<div class="m2"><p>چه روز و چه روزگار آن دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون کار نمی‌کند فغان بی تو</p></div>
<div class="m2"><p>از دست غم تو چون فغان دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در خاطر هیچکس نمی‌آید</p></div>
<div class="m2"><p>شوری که از آن شکرستان دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم شکریم ده به جان تو</p></div>
<div class="m2"><p>کاخر من دلشکسته جان دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتی که تو را شکر زیان دارد</p></div>
<div class="m2"><p>گو دار که من بسی زیان دارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا چند رخت به آستین پوشی</p></div>
<div class="m2"><p>تا کی ز تو سر بر آستان دارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتی که جهان به کام عطار است</p></div>
<div class="m2"><p>من بی تو کجا سر جهان دارم</p></div></div>