---
title: >-
    غزل شمارهٔ ۴۱۶
---
# غزل شمارهٔ ۴۱۶

<div class="b" id="bn1"><div class="m1"><p>در عشق روی او ز حدوث و قدم مپرس</p></div>
<div class="m2"><p>گر مرد عاشقی ز وجود و عدم مپرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردانه بگذر از ازل و از ابد تمام</p></div>
<div class="m2"><p>کم گوی از ازل ز ابد نیز هم مپرس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین چار رکن چون بگذشتی حرم ببین</p></div>
<div class="m2"><p>وانگاه دیده برکن و نیز از حرم مپرس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنجا که نیست هستی توحید، هیچ نیست</p></div>
<div class="m2"><p>زانجای درگذر به دمی و ز دم مپرس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لوح و قلم به قطع دماغ و زبان توست</p></div>
<div class="m2"><p>لوح و قلم بدان و ز لوح و قلم مپرس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کرسی است سینهٔ تو و عرش است دل درو</p></div>
<div class="m2"><p>وین هر دو نیست جز رقمی وز رقم مپرس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون تو بدین مقام رسیدی دگر مباش</p></div>
<div class="m2"><p>گم گرد در فنا و دگر بیش و کم مپرس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک ذره سایه باش تو اینجا در آفتاب</p></div>
<div class="m2"><p>اینجا چو تو نه‌ای تو ز شادی و غم مپرس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر چیز کان تو فهم کنی آن همه تویی</p></div>
<div class="m2"><p>پس تا که تو تویی ز حدوث و قدم مپرس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عطار اگر رسیدی اینجایگاه تو</p></div>
<div class="m2"><p>در لذت حقیقت خود از الم مپرس</p></div></div>