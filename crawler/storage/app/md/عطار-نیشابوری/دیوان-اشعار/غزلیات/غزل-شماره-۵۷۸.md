---
title: >-
    غزل شمارهٔ ۵۷۸
---
# غزل شمارهٔ ۵۷۸

<div class="b" id="bn1"><div class="m1"><p>چشم از پی آن دارم تا روی تو می‌بینم</p></div>
<div class="m2"><p>دل را همه میل جان با سوی تو می‌بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا جان بودم در تن رو از تو نگردانم</p></div>
<div class="m2"><p>زیرا که حیات جان باروی تو می‌بینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس عاشق سرگردان از عشق تو لب برجان</p></div>
<div class="m2"><p>آواره ز خان و مان بر بوی تو می‌بینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از عشق تو نشکیبم گر خوانی و گر رانی</p></div>
<div class="m2"><p>زیرا که دل افتاده در کوی تو می‌بینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر جا که یکی بیدل از عشق تو بی حاصل</p></div>
<div class="m2"><p>سرگشته و بی منزل سر کوی تو می‌بینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن دل که بود سرکش گشته است اسیر عشق</p></div>
<div class="m2"><p>اندر خم چوگانت چون گوی تو می‌بینم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم که مگر کلی وصل تو بدانستم</p></div>
<div class="m2"><p>صد جان و دل خود را یک موی تو می‌بینم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عطار مگر روزی ترکیش بود درسر</p></div>
<div class="m2"><p>کامروز به عشق اندر هندوی تو می‌بینم</p></div></div>