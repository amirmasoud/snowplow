---
title: >-
    غزل شمارهٔ ۵۸۵
---
# غزل شمارهٔ ۵۸۵

<div class="b" id="bn1"><div class="m1"><p>ما هر چه آن ماست ز ره بر گرفته‌ایم</p></div>
<div class="m2"><p>با پیر خویش راه قلندر گرفته‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در راه حق چو محرم ایمان نبوده‌ایم</p></div>
<div class="m2"><p>ایمان خود به تازگی از سر گرفته‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون اصل کار ما همه روی و ریا نمود</p></div>
<div class="m2"><p>یکباره ترک کار مزور گرفته‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از هر دو کون گوشهٔ دیری گزیده‌ایم</p></div>
<div class="m2"><p>زنار چار کرده به‌بر در گرفته‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندر قمارخانه چو رندان نشسته‌ایم</p></div>
<div class="m2"><p>وز طیلسان و خرقه قلم برگرفته‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان چشمهٔ حیات که در کوی دوست بود</p></div>
<div class="m2"><p>تا روز حشر ملک سکندر گرفته‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برتر ز هست و نیست قدم در نهاده‌ایم</p></div>
<div class="m2"><p>بیرون ز کفر و دین ره دیگر گرفته‌ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر روی دوست ساغر و دست از میان برون</p></div>
<div class="m2"><p>از دست دوست باده به ساغر گرفته‌ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عطار تا بیان مقامات عشق کرد</p></div>
<div class="m2"><p>از لفظ او دو کون به گوهر گرفته‌ایم</p></div></div>