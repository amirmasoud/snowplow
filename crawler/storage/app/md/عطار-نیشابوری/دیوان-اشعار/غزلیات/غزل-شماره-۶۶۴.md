---
title: >-
    غزل شمارهٔ ۶۶۴
---
# غزل شمارهٔ ۶۶۴

<div class="b" id="bn1"><div class="m1"><p>عشق تو در جان من ای جان من</p></div>
<div class="m2"><p>آتشی زد در دل بریان من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دل بریان من آتش مزن</p></div>
<div class="m2"><p>رحم کن بر دیدهٔ گریان من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیدهٔ گریان من پرخون مدار</p></div>
<div class="m2"><p>در نگر آخر به‌سوز جان من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوز جانم بیش ازین ظاهر مکن</p></div>
<div class="m2"><p>گوش می‌دار این غم پنهان من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درد این بیچاره از حد درگذشت</p></div>
<div class="m2"><p>چاره‌ای ساز و بکن درمان من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خود مرا فرمان کجا باشد ولیک</p></div>
<div class="m2"><p>کج مکن چون زلف خود پیمان من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرچه خواهی کن تو به دانی از آنک</p></div>
<div class="m2"><p>زاریی باشد نه فرمان زان من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان عطار از تو در آتش فتاد</p></div>
<div class="m2"><p>آب زن در آتش سوزان من</p></div></div>