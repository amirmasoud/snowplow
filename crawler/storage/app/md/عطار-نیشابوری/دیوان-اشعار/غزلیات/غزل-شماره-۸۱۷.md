---
title: >-
    غزل شمارهٔ ۸۱۷
---
# غزل شمارهٔ ۸۱۷

<div class="b" id="bn1"><div class="m1"><p>خال مشکین بر گلستان می زنی</p></div>
<div class="m2"><p>دل همی سوزی و بر جان می زنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر بیاض برگ گل عمر مرا</p></div>
<div class="m2"><p>هر زمان فال دگرسان می زنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صید خواهی کرد دلها را به زلف</p></div>
<div class="m2"><p>زلف را بر یکدگر زان می زنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان دو لعل آتشین آبدار</p></div>
<div class="m2"><p>آتش اندر آب حیوان می زنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از لبت یک بوسه نتوان زد به تیر</p></div>
<div class="m2"><p>کز سر کین تیر مژگان می زنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفته‌ای ایمانت را راهی زنم</p></div>
<div class="m2"><p>چون بکشتی الحق آسان می زنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در تو پیمان نیست صد عاشق بمرد</p></div>
<div class="m2"><p>تا تو رای عهد و پیمان می زنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دامن اندر خون زند عطار زانک</p></div>
<div class="m2"><p>تو نفس با او ز هجران می زنی</p></div></div>