---
title: >-
    غزل شمارهٔ ۴۲
---
# غزل شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>آنکه چندین نقش ازو برخاسته است</p></div>
<div class="m2"><p>یارب او در پرده چون آراسته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون ز پرده دم به دم می تافته است</p></div>
<div class="m2"><p>هر دو عالم دم به دم می‌کاسته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون شود یک ره ز پرده آشکار</p></div>
<div class="m2"><p>تو یقین دان کان قیامت خاسته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محو گردد در قیامت زان جمال</p></div>
<div class="m2"><p>هر که نقشی در جهان پیراسته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ذره‌ای معشوق کی آید پدید</p></div>
<div class="m2"><p>چون دو عالم پر زر و پر خواسته است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در قیامت سوی خود کس ننگرد</p></div>
<div class="m2"><p>چون جمال آن چنان آراسته است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ذره‌ای گشت است ظاهر زان جمال</p></div>
<div class="m2"><p>شور از هر دو جهان برخاسته است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای فرید اینجا چه خواهی کار و بار</p></div>
<div class="m2"><p>راه تو نادانی و ناخواسته است</p></div></div>