---
title: >-
    غزل شمارهٔ ۴۲۵
---
# غزل شمارهٔ ۴۲۵

<div class="b" id="bn1"><div class="m1"><p>اگر دلم ببرد یار دلبری رسدش</p></div>
<div class="m2"><p>وگر بپروردم بنده‌پروری رسدش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بس که من سر او دارم از قدم تا فرق</p></div>
<div class="m2"><p>گرم چو شمع بسوزد به سرسری رسدش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سفید کاری صبح رخش جهان بگرفت</p></div>
<div class="m2"><p>چو شب به طره طلسم سیه‌گری رسدش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو آفتاب رخش نور بخش اسلام است</p></div>
<div class="m2"><p>اگر ز زلف نهد رسم کافری رسدش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو پشت لشکر حسن است روی صف شکنش</p></div>
<div class="m2"><p>اگر به عمد کند قصد لشکری رسدش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدید بیخبری روی او و گفت امروز</p></div>
<div class="m2"><p>به حکم با مه گردون برابری رسدش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صد آفتاب مرا روشن است کین ساعت</p></div>
<div class="m2"><p>نطاق بسته چو جوزا به چاکری رسدش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو هست چشمهٔ حیوان زکات‌خواه لبش</p></div>
<div class="m2"><p>اگر قیام کند در سکندری رسدش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سکندری چه بود با لب چو آب حیات</p></div>
<div class="m2"><p>که گر چو خضر رود در پیمبری رسدش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فرید چون ز لب لعل او سخن گوید</p></div>
<div class="m2"><p>نثار در و گهر در سخن‌وری رسدش</p></div></div>