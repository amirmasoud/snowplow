---
title: >-
    غزل شمارهٔ ۲۹۹
---
# غزل شمارهٔ ۲۹۹

<div class="b" id="bn1"><div class="m1"><p>چون سیمبران روی به گلزار نهادند</p></div>
<div class="m2"><p>گل را ز رخ چون گل خود خار نهادند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا با رخ چون گل بگذشتند به گلزار</p></div>
<div class="m2"><p>نار از رخ گل در دل گلنار نهادند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کار شدند و می چون زنگ کشیدند</p></div>
<div class="m2"><p>پس عاشق دلسوخته را کار نهادند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تلخی ز می لعل ببردند که می را</p></div>
<div class="m2"><p>تنگی ز لب لعل شکربار نهادند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای ساقی گلرنگ درافکن می گلبوی</p></div>
<div class="m2"><p>کز گل کلهی بر سر گلزار نهادند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌نوش چو شنگرف به سرخی که گل تر</p></div>
<div class="m2"><p>طفلی است که در مهد چو زنگار نهادند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بوی جگر سوخته بشنو که چمن را</p></div>
<div class="m2"><p>گلهای جگر سوخته در بار نهادند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان غرقهٔ خون گشت تن لاله که او را</p></div>
<div class="m2"><p>آن داغ سیه بر دل خون خوار نهادند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سوسن چو زبان داشت فروشد به خموشی</p></div>
<div class="m2"><p>در سینهٔ او گوهر اسرار نهادند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از بر بنیارد کس و از بحر نزاید</p></div>
<div class="m2"><p>آن در که درین خاطر عطار نهادند</p></div></div>