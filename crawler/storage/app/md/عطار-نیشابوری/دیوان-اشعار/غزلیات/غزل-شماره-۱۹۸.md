---
title: >-
    غزل شمارهٔ ۱۹۸
---
# غزل شمارهٔ ۱۹۸

<div class="b" id="bn1"><div class="m1"><p>هر دل که وصال تو طلب کرد</p></div>
<div class="m2"><p>شب خوش بادش که روز شب کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در تاریکی میان خون مرد</p></div>
<div class="m2"><p>هر که آب حیات تو طلب کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وآنکس که بنا در این گهر یافت</p></div>
<div class="m2"><p>بی خود شد و مدتی طرب کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن چیز که یافت بس عجب یافت</p></div>
<div class="m2"><p>وآن حال که کرد بس عجب کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون حوصله پر برآمد او را</p></div>
<div class="m2"><p>بانگی نه به وقت ازین سبب کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق تو میان خون و آتش</p></div>
<div class="m2"><p>بردار کشیدش و ادب کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق تو هزار طیلسان را</p></div>
<div class="m2"><p>در گردن عاشقان کنب کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بس مرد شگرف را که این بحر</p></div>
<div class="m2"><p>لب برهم دوخت و خشک لب کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بس جان عظیم را که این درد</p></div>
<div class="m2"><p>گه تاب بسوخت گاه تب کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون خار رطب بد و رطب خار</p></div>
<div class="m2"><p>عقل از چه عزیمت رطب کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صد حقه و مهره هست و هیچ است</p></div>
<div class="m2"><p>این کار کدام بلعجب کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون نتوانی محمدی یافت</p></div>
<div class="m2"><p>باری مکن آنچه بولهب کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عطار سزد که پشت گرم است</p></div>
<div class="m2"><p>چون روی به قبلهٔ عرب کرد</p></div></div>