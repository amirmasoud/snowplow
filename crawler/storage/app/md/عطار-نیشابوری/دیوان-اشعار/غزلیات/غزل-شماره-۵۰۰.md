---
title: >-
    غزل شمارهٔ ۵۰۰
---
# غزل شمارهٔ ۵۰۰

<div class="b" id="bn1"><div class="m1"><p>تا جمال تو بدیدم مست و مدهوش آمدم</p></div>
<div class="m2"><p>عاشق لعل شکربارش گهر پوش آمدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نامهٔ عشقت بخواندم عاشق دردت شدم</p></div>
<div class="m2"><p>حلقهٔ زلفت بدیدم حلقه در گوش آمدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرخ رو از چشم بودم پیش ازین از خون دل</p></div>
<div class="m2"><p>زردرو از سبزهٔ آن چشمهٔ نوش آمدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شغبهٔ آن شکرستان شکربار ار شدم</p></div>
<div class="m2"><p>فتنهٔ آن سنبلستان بناگوش آمدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواب خرگوشم بسی دادی ندانستم ولیک</p></div>
<div class="m2"><p>هم به آخر در جوال خواب خرگوش آمدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی بگردانم ز تو از هر جفایی روی از آنک</p></div>
<div class="m2"><p>تو جفا کیش آمدی و من وفا کوش آمدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق تو کاندر میان جان من شد معتکف</p></div>
<div class="m2"><p>کی فراموشش کنم گر من فراموش آمدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وصف می‌کرد از تو عطار اندر آفاق جهان</p></div>
<div class="m2"><p>نک سخن ناگفته حالی گنگ و مدهوش آمدم</p></div></div>