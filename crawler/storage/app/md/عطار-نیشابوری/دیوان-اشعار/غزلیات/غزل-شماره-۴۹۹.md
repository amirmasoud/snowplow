---
title: >-
    غزل شمارهٔ ۴۹۹
---
# غزل شمارهٔ ۴۹۹

<div class="b" id="bn1"><div class="m1"><p>تا ز سر عشق سرگردان شدم</p></div>
<div class="m2"><p>غرقهٔ دریای بی پایان شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دلم در آتش عشق اوفتاد</p></div>
<div class="m2"><p>مبتلای درد بی درمان شدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون سر و کار مرا سامان نماند</p></div>
<div class="m2"><p>من ز حیرت بی سر و سامان شدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق صاحب جمالی شد دلم</p></div>
<div class="m2"><p>کز کمال حسن او حیران شدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا بدیدم آفتاب روی او</p></div>
<div class="m2"><p>بر مثال ذره سرگردان شدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون نبودم مرد وصلش لاجرم</p></div>
<div class="m2"><p>مدتی غمخوارهٔ هجران شدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مدتی رنجی کشیدم در جهان</p></div>
<div class="m2"><p>جان و دل درباختم سلطان شدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچو مرغی نیم بسمل در فراق</p></div>
<div class="m2"><p>پر زدم بسیار تا بی جان شدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون به جان فانی شدم در راه او</p></div>
<div class="m2"><p>در فتا شایستهٔ جانان شدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون بقای خود بدیدم در فنا</p></div>
<div class="m2"><p>آنچه می‌جستم به کلی آن شدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رستم از عار خود و با یار خود</p></div>
<div class="m2"><p>بی خود اندر پیرهن پنهان شدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا که عطار این سخن آزاد گفت</p></div>
<div class="m2"><p>بندهٔ او از میان جان شدم</p></div></div>