---
title: >-
    غزل شمارهٔ ۶۳۱
---
# غزل شمارهٔ ۶۳۱

<div class="b" id="bn1"><div class="m1"><p>ای روی تو شمع بت‌پرستان</p></div>
<div class="m2"><p>یاقوت تو قوت تنگدستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلف تو و صد هزار حلقه</p></div>
<div class="m2"><p>چشم تو و صد هزار دستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خورشید نهاده چشم بر در</p></div>
<div class="m2"><p>تا تو به درآیی از شبستان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گردون به هزار چشم هر شب</p></div>
<div class="m2"><p>واله شده در تو همچو مستان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنچ از رخ تو رود در اسلام</p></div>
<div class="m2"><p>هرگز نرود به کافرستان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیران ره حروف زلفت</p></div>
<div class="m2"><p>ابجد خوانان این دبستان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در عشق تو نیستان که هستند</p></div>
<div class="m2"><p>هستند نه نیستان نه هستان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ممکن نبود به لطف تو خلق</p></div>
<div class="m2"><p>از دینداران و بت‌پرستان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوی تو که آب خضر بوده است</p></div>
<div class="m2"><p>هر شیر که خورده‌ای ز پستان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای بر شده بس بلند آخر</p></div>
<div class="m2"><p>به زین نگرید سوی بستان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گلگون جمال در جهان تاز</p></div>
<div class="m2"><p>وز عمر رونده داد بستان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کین گلبن نوبهار عمرت</p></div>
<div class="m2"><p>درهم ریزد به یک زمستان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مشغول مشو به گل که ماراست</p></div>
<div class="m2"><p>پنهان ز تو خفته در گلستان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زخمی زندت به چشم زخمی</p></div>
<div class="m2"><p>گورستانت کند ز بستان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو گلبن گلستان حسنی</p></div>
<div class="m2"><p>عطار تورا هزاردستان</p></div></div>