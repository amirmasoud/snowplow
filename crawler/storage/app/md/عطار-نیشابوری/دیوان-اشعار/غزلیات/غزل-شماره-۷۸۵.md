---
title: >-
    غزل شمارهٔ ۷۸۵
---
# غزل شمارهٔ ۷۸۵

<div class="b" id="bn1"><div class="m1"><p>هر دمم مست به بازار کشی</p></div>
<div class="m2"><p>راستی چست و به هنجار کشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می عشقم بچشانی و مرا</p></div>
<div class="m2"><p>مست گردانی و در کار کشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گاهم از کفر به دین باز آری</p></div>
<div class="m2"><p>گاهم از کعبه به خمار کشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گاهم از راه یقین دور کنی</p></div>
<div class="m2"><p>گاهم اندر ره اسرار کشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گه ز مسجد به خرابات بری</p></div>
<div class="m2"><p>گاهم از میکده در غار کشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون ز اسلام منت ننگ آید</p></div>
<div class="m2"><p>از مصلام به زنار کشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون مرا ننگ ره دین بینی</p></div>
<div class="m2"><p>هر دمم در ره کفار کشی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بس که پیران حقیقت‌بین را</p></div>
<div class="m2"><p>اندرین واقعه بر دار کشی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای دل سوخته گر مرد رهی</p></div>
<div class="m2"><p>خون خوری تن زنی و بار کشی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر امید گل وصلش شب و روز</p></div>
<div class="m2"><p>همچو گلبن ستم خار کشی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آتش اندر دل ایام زنی</p></div>
<div class="m2"><p>خاک در دیدهٔ اغیار کشی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بویی از مجمرهٔ عشق بری</p></div>
<div class="m2"><p>باده بر چهرهٔ دلدار کشی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>غم معشوق که شادی دل است</p></div>
<div class="m2"><p>در ره عشق چو عطار کشی</p></div></div>