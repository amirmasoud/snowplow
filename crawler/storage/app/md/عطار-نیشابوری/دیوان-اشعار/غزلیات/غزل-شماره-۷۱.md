---
title: >-
    غزل شمارهٔ ۷۱
---
# غزل شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>در سرم از عشقت این سودا خوش است</p></div>
<div class="m2"><p>در دلم از شوقت این غوغا خوش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من درون پرده جان می‌پرورم</p></div>
<div class="m2"><p>گر برون جان می کند اعدا خوش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون جمالت برنتابد هیچ چشم</p></div>
<div class="m2"><p>جملهٔ آفاق نابینا خوش است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو چرخ از شوق تو در هر دو کون</p></div>
<div class="m2"><p>هر که در خون می‌نگردد ناخوش است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بندگی را پیش یک بند قبات</p></div>
<div class="m2"><p>صد کمر بر بسته بر جوزا خوش است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان فشان از خندهٔ جان‌پرورت</p></div>
<div class="m2"><p>زاهد خلوت نشین رسوا خوش است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر زبانم گنگ شد در وصف تو</p></div>
<div class="m2"><p>اشک خون آلود من گویا خوش است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون تو خونین می‌کنی دل در برم</p></div>
<div class="m2"><p>گرچه دل می‌سوزدم اما خوش است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این جهان فانی است گر آن هم بود</p></div>
<div class="m2"><p>تو بسی، مه این مه آن یکتا خوش است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر نباشد هر دو عالم گو مباش</p></div>
<div class="m2"><p>تو تمامی با توام تنها خوش است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ماه‌رویا سیرم اینجا از وجود</p></div>
<div class="m2"><p>بی وجودم گر بری آنجا خوش است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پرده از رخ برفکن تا گم شوم</p></div>
<div class="m2"><p>کان تماشا بی وجود ما خوش است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>الحق آنجا کآفتاب روی توست</p></div>
<div class="m2"><p>صد هزاران بی سر و بی پا خوش است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صد جهان بر جان و بر دل تا ابد</p></div>
<div class="m2"><p>والهٔ آن طلعت زیبا خوش است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پرتو خورشید چون صحرا شود</p></div>
<div class="m2"><p>ذرهٔ سرگشته ناپروا خوش است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون تو پیدا آمدی چون آفتاب</p></div>
<div class="m2"><p>گر شدم چون سایه ناپیدا خوش است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از درون چاه جسمم دل گرفت</p></div>
<div class="m2"><p>قصد صحرا می‌کنم صحرا خوش است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دی اگر چون قطره‌ای بودم ضعیف</p></div>
<div class="m2"><p>این زمان دریا شدم دریا خوش است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وای عجب تا غرق این دریا شدم</p></div>
<div class="m2"><p>بانگ می‌دارم که استسقا خوش است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>غرق دریا تشنه می‌میرم مدام</p></div>
<div class="m2"><p>این چه سودایی است این سودا خوش است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز اشتیاقت روز و شب عطار را</p></div>
<div class="m2"><p>دیده پر خون و دلی شیدا خوش است</p></div></div>