---
title: >-
    غزل شمارهٔ ۳۸۴
---
# غزل شمارهٔ ۳۸۴

<div class="b" id="bn1"><div class="m1"><p>قدم درنه اگر مردی درین کار</p></div>
<div class="m2"><p>حجاب تو تویی از پیش بردار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر خواهی که مرد کار گردی</p></div>
<div class="m2"><p>مکن بی حکم مردی عزم این کار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یقین دان کز دم این شیرمردان</p></div>
<div class="m2"><p>شود چون شیر بیشه شیر دیوار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو بازان جای خود کن ساعد شاه</p></div>
<div class="m2"><p>مشو خرسند چون کرکس به مردار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلیری شیرمردی باید این جا</p></div>
<div class="m2"><p>که صد دریا درآشامد به یکبار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز رعنایان نازک‌دل چه خیزد</p></div>
<div class="m2"><p>که این جا پردلی باید جگرخوار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه او را کفر دامن‌گیر و نه دین</p></div>
<div class="m2"><p>نه او را نور دامن‌سوز و نه نار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلا تا کی روی بر سر چو گردون</p></div>
<div class="m2"><p>قراری گیر و دم درکش زمین‌وار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر خواهی که دریایی شوی تو</p></div>
<div class="m2"><p>چو کوهی خویش را برجای می دار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کنون چون نقطه ساکن باش یکچند</p></div>
<div class="m2"><p>که سرگردان بسی گشتی چو پرگار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر خواهی که در پیش افتی از خویش</p></div>
<div class="m2"><p>سه کارت می‌بباید کرد ناچار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکی آرام و دیگر صبر کردن</p></div>
<div class="m2"><p>سیم دایم زبان بستن ز گفتار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر دستت دهد این هر سه حالت</p></div>
<div class="m2"><p>علم بر هر دو عالم زن چو عطار</p></div></div>