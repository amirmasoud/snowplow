---
title: >-
    غزل شمارهٔ ۲۹۴
---
# غزل شمارهٔ ۲۹۴

<div class="b" id="bn1"><div class="m1"><p>عقل در عشق تو سرگردان بماند</p></div>
<div class="m2"><p>چشم جان در روی تو حیران بماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ذره‌ای سرگشتگی عشق تو</p></div>
<div class="m2"><p>روز و شب در چرخ سرگردان بماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون ندید اندر دو عالم محرمی</p></div>
<div class="m2"><p>آفتاب روی تو پنهان بماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که چوگان سر زلف تو دید</p></div>
<div class="m2"><p>همچو گویی در خم چوگان بماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پای و سر گم کرد دل تا کار او</p></div>
<div class="m2"><p>چون سر زلف تو بی‌پایان بماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که یکدم آن لب و دندان بدید</p></div>
<div class="m2"><p>تا ابد انگشت در دندان بماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که جست آب حیات وصل تو</p></div>
<div class="m2"><p>جاودان در ظلمت هجران بماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ور کسی را وصل دادی بی طلب</p></div>
<div class="m2"><p>دایما در درد بی درمان بماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ور کسی را با تو یک دم دست داد</p></div>
<div class="m2"><p>عمر او در هر دو عالم آن بماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حاصل عطار در سودای تو</p></div>
<div class="m2"><p>دیده‌ای گریان دلی بریان بماند</p></div></div>