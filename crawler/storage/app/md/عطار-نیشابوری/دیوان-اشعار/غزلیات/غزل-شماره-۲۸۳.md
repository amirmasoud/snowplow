---
title: >-
    غزل شمارهٔ ۲۸۳
---
# غزل شمارهٔ ۲۸۳

<div class="b" id="bn1"><div class="m1"><p>مستغرقی که از خود هرگز به سر نیامد</p></div>
<div class="m2"><p>صد ره بسوخت هر دم دودی به در نیامد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که روی او را روزی سپند سوزم</p></div>
<div class="m2"><p>زیرا که از چو من کس کاری دگر نیامد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون نیک بنگرستم آن روی بود جمله</p></div>
<div class="m2"><p>از روی او سپندی کس را به سر نیامد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جانان چو رخ نمودی هرجا که بود جانی</p></div>
<div class="m2"><p>فانی شدند جمله وز کس خبر نیامد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آخر سپند باید بهر چنان جمالی</p></div>
<div class="m2"><p>دردا که هیچ کس را این کار برنیامد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش تو محو گشتند اول قدم همه کس</p></div>
<div class="m2"><p>هرگز دوم قدم را یک راهبر نیامد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون گام اول از خود جمله شدند فانی</p></div>
<div class="m2"><p>کس را به گام دیگر رنج گذر نیامد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما سایه و تو خورشید آری شگفت نبود</p></div>
<div class="m2"><p>خورشید سایه‌ای را گر در نظر نیامد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که سر نهاد روزی بر پای درد عشقت</p></div>
<div class="m2"><p>تا در رهت چو گویی بی پا و سر نیامد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که گوشهٔ جگر خواند او از میان جانت</p></div>
<div class="m2"><p>تا از میان جانش بوی جگر نیامد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چندان که برگشادم بر دل در معانی</p></div>
<div class="m2"><p>عطار را از آن در جز دردسر نیامد</p></div></div>