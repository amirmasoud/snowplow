---
title: >-
    غزل شمارهٔ ۴۹۸
---
# غزل شمارهٔ ۴۹۸

<div class="b" id="bn1"><div class="m1"><p>ای عشق بی نشان ز تو من بی نشان شدم</p></div>
<div class="m2"><p>خون دلم بخوردی و در خورد جان شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون کرم‌پیله، عشق تنیدم به خویش بر</p></div>
<div class="m2"><p>چون پرده راست گشت من اندر میان شدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیگر که داندم چو من از خود برآمدم</p></div>
<div class="m2"><p>دیگر که بیندم چو من از خود نهان شدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون در دل آمدم آنچه زبان لال گشت از آن</p></div>
<div class="m2"><p>در خامشی و صبر چنین بی زبان شدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرده چگونه بر سر دریا فتد ز قعر</p></div>
<div class="m2"><p>من در میان آتش عشقت چنان شدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرغی بدم ز عالم غیبی برآمده</p></div>
<div class="m2"><p>عمری به سر بگشتم و با آشیان شدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون بر نتافت هر دو جهان بار جان من</p></div>
<div class="m2"><p>بیرون ز هر دو در حرم جاودان شدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عطار چند گویی ازین گفت توبه کن</p></div>
<div class="m2"><p>نه توبه چون کنم که کنون کامران شدم</p></div></div>