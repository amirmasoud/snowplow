---
title: >-
    غزل شمارهٔ ۶۰۸
---
# غزل شمارهٔ ۶۰۸

<div class="b" id="bn1"><div class="m1"><p>تا ما سر ننگ و نام داریم</p></div>
<div class="m2"><p>بر دل غم تو حرام داریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو فارغ و ما در اشتیاقت</p></div>
<div class="m2"><p>بیچارگیی تمام داریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز اندیشهٔ آنکه فارغی تو</p></div>
<div class="m2"><p>اندیشهٔ بر دوام داریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گه دست ز جان خود بشوییم</p></div>
<div class="m2"><p>گه دست به سوی جام داریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گه زهد و نماز پیش گیریم</p></div>
<div class="m2"><p>گه میکده را مقام داریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گه بر سر درد درد ریزیم</p></div>
<div class="m2"><p>گه بر سر کام کام داریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما با تو کدام نوع ورزیم</p></div>
<div class="m2"><p>وز هر نوعی کدام داریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از تو به گزاف وصل جوییم</p></div>
<div class="m2"><p>یارب طمعی چه خام داریم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عطار چو فارغ است از نام</p></div>
<div class="m2"><p>ما گفتهٔ او به نام داریم</p></div></div>