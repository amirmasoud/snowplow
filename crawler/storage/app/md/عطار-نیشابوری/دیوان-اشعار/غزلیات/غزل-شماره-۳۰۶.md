---
title: >-
    غزل شمارهٔ ۳۰۶
---
# غزل شمارهٔ ۳۰۶

<div class="b" id="bn1"><div class="m1"><p>دل ز میان جان و دل قصد هوات می‌کند</p></div>
<div class="m2"><p>جان به امید وصل تو عزم وفات می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه ندید جان و دل از تو وفا به هیچ روی</p></div>
<div class="m2"><p>بر سر صد هزار غم یاد جفات می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌نکند به صد قران ترک کلاه‌دار چرخ</p></div>
<div class="m2"><p>آنچه میان عاشقان بند قبات می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خسرو یک سواره را بر رخ نطع نیلگون</p></div>
<div class="m2"><p>لعل تو طرح می‌نهد روی تو مات می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان و دلم به دلبری زیر و زبر همی کنی</p></div>
<div class="m2"><p>وین تو نمی‌کنی بتا زلف دوتات می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خود تو چه آفتی که چرخ از پی گوشمال من</p></div>
<div class="m2"><p>هر نفسی به داوری بر سر مات می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه فرید، از جفا می‌نکند سزای تو</p></div>
<div class="m2"><p>خط تو خود به دست خود با تو سزات می‌کند</p></div></div>