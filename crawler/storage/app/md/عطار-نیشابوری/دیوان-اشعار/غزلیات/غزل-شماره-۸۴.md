---
title: >-
    غزل شمارهٔ ۸۴
---
# غزل شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>تا عشق تودر میان جان است</p></div>
<div class="m2"><p>جان بر همه چیز کامران است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یارب چه کسی که در دو عالم</p></div>
<div class="m2"><p>کس قیمت عشق تو ندانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشقت به همه جهان دریغ است</p></div>
<div class="m2"><p>زان است که از جهان نهان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اندوه تو کوه بی‌قرار است</p></div>
<div class="m2"><p>سودای تو بحر بی کران است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شادی دل کسی که دایم</p></div>
<div class="m2"><p>با درد غم تو شادمان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با تو نفسی نشسته بودم</p></div>
<div class="m2"><p>دیری است کم آرزوی آن است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر دست دهد دمی وصالت</p></div>
<div class="m2"><p>پیش از اجل آرزوی آن است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جانا چو تو از جهان فزونی</p></div>
<div class="m2"><p>خود جان ز چه بستهٔ جهان است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی صبر و قرار جان عطار</p></div>
<div class="m2"><p>بر بوی وصال جاودان است</p></div></div>