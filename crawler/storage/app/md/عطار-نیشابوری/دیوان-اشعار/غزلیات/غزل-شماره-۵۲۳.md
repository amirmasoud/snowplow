---
title: >-
    غزل شمارهٔ ۵۲۳
---
# غزل شمارهٔ ۵۲۳

<div class="b" id="bn1"><div class="m1"><p>تا نرگست به دشنه چون شمع کشت زارم</p></div>
<div class="m2"><p>چون لاله دور از تو جز خون کفن ندارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در پای اوفتادم زیرا که سر ندارد</p></div>
<div class="m2"><p>چون حلقه‌های زلفت غمهای بی شمارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بسکه هست حلقه در زلف سرفرازت</p></div>
<div class="m2"><p>هرگز سری ندارد چندان که برشمارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بادم نبردی آخر چون ذره‌ای ز سستی</p></div>
<div class="m2"><p>گر داشتی دل تو یک ذره استوارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگز ستاره دیدی در آفتاب بنگر</p></div>
<div class="m2"><p>در آفتاب رویت چشم ستاره بارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیوسته پیش حکمت چون سرفکنده‌ام من</p></div>
<div class="m2"><p>زین بیش سر میفکن چون شمع در کنارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر نه به لطف دستی کز حد گذشت دانی</p></div>
<div class="m2"><p>بی لاله‌زار رویت این ناله‌های زارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون دم نمی‌توان زد با هیچکس ز عشقت</p></div>
<div class="m2"><p>پس من ز درد عشقت با که نفس برآرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عطار کی تواند شرح غم تو دادن</p></div>
<div class="m2"><p>کز کار شد زبانم وز دست رفت کارم</p></div></div>