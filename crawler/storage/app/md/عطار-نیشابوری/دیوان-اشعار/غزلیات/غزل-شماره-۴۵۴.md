---
title: >-
    غزل شمارهٔ ۴۵۴
---
# غزل شمارهٔ ۴۵۴

<div class="b" id="bn1"><div class="m1"><p>ای زلف تو شبی خوش وانگه به روز حاصل</p></div>
<div class="m2"><p>خورشید را ز رشکت صد گونه سوز حاصل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر تابش مهت را مهری هزار در سر</p></div>
<div class="m2"><p>هر تیر ترکشت را صد کینه توز حاصل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماهی در درجت هر یک چو روز روشن</p></div>
<div class="m2"><p>ماهی که دید او را سی و دو روز حاصل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی تو بود روزی خطت گرفت نیمی</p></div>
<div class="m2"><p>ملکی ز خطت آمد در نیمروز حاصل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ملکی که هیچ سلطان حاصل ندید خود را</p></div>
<div class="m2"><p>کردی به چشم زخمی تو دلفروز حاصل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وان راستی که کس را هرگز نشد مسلم</p></div>
<div class="m2"><p>زلف تو کرده آن را پیوسته کو ز حاصل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرده دریدن تو پیوند کی پذیرد</p></div>
<div class="m2"><p>عطار را گر آید صد پرده دوز حاصل</p></div></div>