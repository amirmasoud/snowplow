---
title: >-
    غزل شمارهٔ ۲۳۴
---
# غزل شمارهٔ ۲۳۴

<div class="b" id="bn1"><div class="m1"><p>هر روز غم عشقت بر ما حشر انگیزد</p></div>
<div class="m2"><p>صد واقعه پیش آرد صد فتنه برانگیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشقت که ازو دل را پر خون جگر دیدم</p></div>
<div class="m2"><p>اندوه دل‌افزایت تف جگر انگیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرگه که برون آید از چشم تو اخباری</p></div>
<div class="m2"><p>تا چشم زنی بر هم از سنگ برانگیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرخی لب لعلت سرسبزی جان دارد</p></div>
<div class="m2"><p>سودای سر زلفت صفرای سر انگیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون پستهٔ شیرینت شوری چو شکر دارد</p></div>
<div class="m2"><p>هر لحظه به شیرینی شوری دگر انگیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عطار به وصف تو چون بحر دلی دارد</p></div>
<div class="m2"><p>کان بحر چو موج آرد سیل گهر انگیزد</p></div></div>