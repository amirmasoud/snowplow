---
title: >-
    غزل شمارهٔ ۸۳۹
---
# غزل شمارهٔ ۸۳۹

<div class="b" id="bn1"><div class="m1"><p>زلف تیره بر رخ روشن نهی</p></div>
<div class="m2"><p>سرکشان را بار بر گردن نهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی بنمایی چو ماه آسمان</p></div>
<div class="m2"><p>منت روی زمین بر من نهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا کی از زنجیر زلف تافته</p></div>
<div class="m2"><p>داغ گه بر جان و گه بر تن نهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وقت نامد کز نمکدان لبت</p></div>
<div class="m2"><p>دام من زان نرگس رهزن نهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا سر یک رشته یابم از تو باز</p></div>
<div class="m2"><p>خارم از مژگان چون سوزن نهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر مرا در دوستی تو ز چشم</p></div>
<div class="m2"><p>اشک ریزد نام من دشمن نهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفته بودی خون گری تا مهر عشق</p></div>
<div class="m2"><p>بی‌رخم بر دیدهٔ روشن نهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر بگریم تر شود دامن مرا</p></div>
<div class="m2"><p>تو مرا در عشق تر دامن نهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بار ندهی لیک قسم عاشقان</p></div>
<div class="m2"><p>همچو یوسف بوی پیراهن نهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ور دهی در عمر خود بار جمال</p></div>
<div class="m2"><p>بار غم بر جان مرد و زن نهی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وصف تو چون از فرید آید که تو</p></div>
<div class="m2"><p>افصح آفاق را الکن نهی</p></div></div>