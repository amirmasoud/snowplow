---
title: >-
    غزل شمارهٔ ۱۶
---
# غزل شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>تا درین زندان فانی زندگانی باشدت</p></div>
<div class="m2"><p>کنج عزلت گیر تا گنج معانی باشدت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این جهان را ترک کن تا چون گذشتی زین جهان</p></div>
<div class="m2"><p>این جهانت گر نباشد آن جهانی باشدت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کام و ناکام این زمان در کام خود درهم شکن</p></div>
<div class="m2"><p>تا به کام خویش فردا کامرانی باشدت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روزکی چندی چو مردان صبر کن در رنج و غم</p></div>
<div class="m2"><p>تا که بعداز رنج گنج شایگانی باشدت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی خود را زعفرانی کن به بیداری شب</p></div>
<div class="m2"><p>تا به روز حشر روی ارغوانی باشدت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر به ترک عالم فانی بگویی مردوار</p></div>
<div class="m2"><p>عالم باقی و ذوق جاودانی باشدت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صبحدم درهای دولتخانه‌ها بگشاده‌اند</p></div>
<div class="m2"><p>عرضه کن گر آن زمان راز نهانی باشدت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا کی از بی حاصلی ای پیرمرد بچه طبع</p></div>
<div class="m2"><p>در هوای نفس مستی و گرانی باشدت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از تن تو کی شود این نفس سگ سیرت برون</p></div>
<div class="m2"><p>تا به صورت خانهٔ تن استخوانی باشدت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر توانی کشت این سگ را به شمشیر ادب</p></div>
<div class="m2"><p>زان پس ار تو دولتی جویی نشانی باشدت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر بمیری در میان زندگی عطاروار</p></div>
<div class="m2"><p>چون درآید مرگ عین زندگانی باشدت</p></div></div>