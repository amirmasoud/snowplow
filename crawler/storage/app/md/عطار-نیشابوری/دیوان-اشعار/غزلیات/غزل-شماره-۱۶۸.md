---
title: >-
    غزل شمارهٔ ۱۶۸
---
# غزل شمارهٔ ۱۶۸

<div class="b" id="bn1"><div class="m1"><p>جانا شعاع رویت در جسم و جان نگنجد</p></div>
<div class="m2"><p>وآوازهٔ جمالت اندر جهان نگنجد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وصلت چگونه جویم کاندر طلب نیاید</p></div>
<div class="m2"><p>وصفت چگونه گویم کاندر زبان نگنجد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرگز نشان ندادند از کوی تو کسی را</p></div>
<div class="m2"><p>زیرا که راه کویت اندر نشان نگنجد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آهی که عاشقانت از حلق جان برآرند</p></div>
<div class="m2"><p>هم در زمان نیاید هم در مکان نگنجد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنجا که عاشقانت یک دم حضور یابند</p></div>
<div class="m2"><p>دل در حساب ناید جان در میان نگنجد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندر ضمیر دلها گنجی نهان نهادی</p></div>
<div class="m2"><p>از دل اگر برآید در آسمان نگنجد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عطار وصف عشقت چون در عبارت آرد</p></div>
<div class="m2"><p>زیرا که وصف عشقت اندر بیان نگنجد</p></div></div>