---
title: >-
    غزل شمارهٔ ۳۰۱
---
# غزل شمارهٔ ۳۰۱

<div class="b" id="bn1"><div class="m1"><p>از می عشق نیستی هر که خروش می‌زند</p></div>
<div class="m2"><p>عشق تو عقل و جانش را خانه فروش می‌زند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشق عشق تو شدم از دل و جان که عشق تو</p></div>
<div class="m2"><p>پرده نهفته می‌درد زخم خموش می‌زند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل چو ز درد درد تو مست خراب می‌شود</p></div>
<div class="m2"><p>عمر وداع می‌کند عقل خروش می‌زند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه دل خراب من از می عشق مست شد</p></div>
<div class="m2"><p>لیک صبوح وصل را نعره به هوش می‌زند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل چو حریف درد شد ساقی اوست جان ما</p></div>
<div class="m2"><p>دل می عشق می‌خورد جام دم نوش می‌زند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا دل من به مفلسی از همه کون درگذشت</p></div>
<div class="m2"><p>از همه کینه می‌کشد بر همه دوش می‌زند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا ز شراب شوق تو دل بچشید جرعه‌ای</p></div>
<div class="m2"><p>جملهٔ پند زاهدان از پس گوش می‌زند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای دل خسته نیستی مرد مقام عاشقی</p></div>
<div class="m2"><p>سیر شدی ز خود مگر خون تو جوش می‌زند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جان فرید از بلی مست می الست شد</p></div>
<div class="m2"><p>شاید اگر به بوی او لاف سروش می‌زند</p></div></div>