---
title: >-
    غزل شمارهٔ ۵۲۸
---
# غزل شمارهٔ ۵۲۸

<div class="b" id="bn1"><div class="m1"><p>چون من ز همه عالم ترسا بچه‌ای دارم</p></div>
<div class="m2"><p>دانم که ز ترسایی هرگز نبود عارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا زلف چو زنارش دیدم به کنار مه</p></div>
<div class="m2"><p>پیوسته میان خود بربسته به زنارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا از شکن زلفش شد کشف مرا صد سر</p></div>
<div class="m2"><p>برخاست ز پیش دل اقرارم و انکارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر لحظه به رغم من در زلف دهد تابی</p></div>
<div class="m2"><p>با تاب چنان زلفی من تاب نمی‌آرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون از سر هر مویش صد فتنه فرو بارد</p></div>
<div class="m2"><p>از هر مژه طوفانی چون ابر فروبارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن رفت که می‌آمد از دست مرا کاری</p></div>
<div class="m2"><p>اکنون چو سر زلفش، از دست بشد کارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر شب ز فراق او چون شمع همی سوزم</p></div>
<div class="m2"><p>واو بر صفت شمعی هر روز کشد زارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم به جز از عشوه چیزی نفروشی تو</p></div>
<div class="m2"><p>بفروخت جهان بر من زیرا که خریدارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه در صف درویشی شایستهٔ آن ماهم</p></div>
<div class="m2"><p>نه در ره ترسایی اهلیت او دارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه مرد مناجاتم نه رند خراباتم</p></div>
<div class="m2"><p>نه محرم محرابم نه در خور خمارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه مؤمن توحیدم نه مشرک تقلیدم</p></div>
<div class="m2"><p>نه منکر تحقیقم نه واقف اسرارم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از بس که چو کرم قز بر خویش تنم پرده</p></div>
<div class="m2"><p>پیوسته چو کردم قز در پردهٔ پندارم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از زحمت عطارم بندی است قوی در ره</p></div>
<div class="m2"><p>کو کس که کند فارغ از زحمت عطارم</p></div></div>