---
title: >-
    غزل شمارهٔ ۴۶۳
---
# غزل شمارهٔ ۴۶۳

<div class="b" id="bn1"><div class="m1"><p>خط مکش در وفا کزآن توام</p></div>
<div class="m2"><p>فتنهٔ خط دلستان توام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی تو با چشم خون فشان همه شب</p></div>
<div class="m2"><p>در غم لعل درفشان توام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دهانت چو گوش را خبر است</p></div>
<div class="m2"><p>من چرا چشم بر دهان توام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از تو تا برکنار ماند دلم</p></div>
<div class="m2"><p>بی تو چون موی از میان توام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیم جان داشتم غم تو بسوخت</p></div>
<div class="m2"><p>گر کنون زنده‌ام به جان توام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی خود ز آستین مپوش که من</p></div>
<div class="m2"><p>روی بر خاک آستان توام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می ندانم من سبکدل هیچ</p></div>
<div class="m2"><p>تا چرا رایگان گران توام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کینه‌گیری ز من نکو نبود</p></div>
<div class="m2"><p>چون تو دانی که مهربان توام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون زنم در هوای تو پر و بال</p></div>
<div class="m2"><p>که نه من مرغ آشیان توام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همچو عطار مانده باده به دست</p></div>
<div class="m2"><p>کمترین سگ ز چاکران توام</p></div></div>