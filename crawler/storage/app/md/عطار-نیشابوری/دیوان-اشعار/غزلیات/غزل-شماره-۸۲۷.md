---
title: >-
    غزل شمارهٔ ۸۲۷
---
# غزل شمارهٔ ۸۲۷

<div class="b" id="bn1"><div class="m1"><p>نگر تا ای دل بیچاره چونی</p></div>
<div class="m2"><p>چگونه می‌روی سر در نگونی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چگونه می‌کشی صد بحر آتش</p></div>
<div class="m2"><p>چو اندر نفس خود یک قطره خونی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زمانی در تماشای خیالی</p></div>
<div class="m2"><p>زمانی در تمنای جنونی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر خواهی که باشی از بزرگان</p></div>
<div class="m2"><p>مباش از خرده‌گیران کنونی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرا باشی نه کافر نه مسلمان</p></div>
<div class="m2"><p>که تو نه رهروی نه رهنمونی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز یک یک ذره سوی دوست راه است</p></div>
<div class="m2"><p>ولی ره نیست بهتر از زبونی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زبون عشق شو تا بر کشندت</p></div>
<div class="m2"><p>که هرگاهی که کم گشتی فزونی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خود از رفعت ورای هر دو کونی</p></div>
<div class="m2"><p>چرا هم‌صحبت این نفس دونی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلا تو چیستی هستی تو یا نه</p></div>
<div class="m2"><p>وگر نه نیستی نه هست چونی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>منی یا نه منی عینی تو یا غیر</p></div>
<div class="m2"><p>و یا از هرچه اندیشم برونی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه می‌گویم تو خود از خود نهانی</p></div>
<div class="m2"><p>که دو انگشت حق را در درونی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو ای عطار اگر چه دل نداری</p></div>
<div class="m2"><p>ولیکن اهل دل را ذوفنونی</p></div></div>