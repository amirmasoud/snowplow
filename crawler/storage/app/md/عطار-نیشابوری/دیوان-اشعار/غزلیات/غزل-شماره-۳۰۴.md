---
title: >-
    غزل شمارهٔ ۳۰۴
---
# غزل شمارهٔ ۳۰۴

<div class="b" id="bn1"><div class="m1"><p>آفتاب رخ آشکاره کند</p></div>
<div class="m2"><p>جگرم ز اشتیاق پاره کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از پس پرده روی بنماید</p></div>
<div class="m2"><p>مهر و مه را دو پیشکاره کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شوق رویش چو روی پر از اشک</p></div>
<div class="m2"><p>روی خورشید پر ستاره کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لعل دانی که چیست رخش لبش</p></div>
<div class="m2"><p>خون خارا ز سنگ خاره کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که او روی چو گلش خواهد</p></div>
<div class="m2"><p>مدتی خار پشتواره کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در میان با کسی همی آید</p></div>
<div class="m2"><p>کان کس اول ز جان کناره کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشقانی که وصل او طلبند</p></div>
<div class="m2"><p>همه را دوع در کواره کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بالغان در رهش چو طفل رهند</p></div>
<div class="m2"><p>جمله را گور گاهواره کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا کسی روی او نداند باز</p></div>
<div class="m2"><p>چهرهٔ مردم آشکاره کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نور رویش ز هر دریچهٔ چشم</p></div>
<div class="m2"><p>چون سیه پوش شد نظاره کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عشق او در غلط بسی فکند</p></div>
<div class="m2"><p>چون نداند کسی چه چاره کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نتوانیم توبه کرد ز عشق</p></div>
<div class="m2"><p>توبه را صد هزار باره کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شیر عشقش چو پنجه بگشاید</p></div>
<div class="m2"><p>عقل را طفل شیرخواره کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زور یک ذره عشق چندان است</p></div>
<div class="m2"><p>که ز هر سو جهان گذاره کند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ضربت عشق با فرید آن کرد</p></div>
<div class="m2"><p>که ندانم که صد کتاره کند</p></div></div>