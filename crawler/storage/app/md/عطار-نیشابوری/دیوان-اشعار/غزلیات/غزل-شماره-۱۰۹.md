---
title: >-
    غزل شمارهٔ ۱۰۹
---
# غزل شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>از تو کارم همچو زر بایست نیست</p></div>
<div class="m2"><p>وز وصال تو خبر بایست نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کی آخر از فراقت کار من</p></div>
<div class="m2"><p>با وصالت به بتر بایست نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا بگریم در فراقت زار زار</p></div>
<div class="m2"><p>عالمی خون جگر بایست نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون بدادم دل به تو بر یک نظر</p></div>
<div class="m2"><p>در منت به زین نظر بایست نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون شکر داری بسی با عاشقان</p></div>
<div class="m2"><p>یک سخن همچون شکر بایست نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من ز سر تا پای فقر و فاقه‌ام</p></div>
<div class="m2"><p>من تو را خود هیچ در بایست نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون درآیی از درم بهر نثار</p></div>
<div class="m2"><p>عالمی پر گنج زر بایست نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون بدیدم دلشده عطار را</p></div>
<div class="m2"><p>بر کف پای تو سر بایست نیست</p></div></div>