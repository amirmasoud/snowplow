---
title: >-
    غزل شمارهٔ ۳۲۲
---
# غزل شمارهٔ ۳۲۲

<div class="b" id="bn1"><div class="m1"><p>عشق بی درد ناتمام بود</p></div>
<div class="m2"><p>کز نمک دیگ را طعام بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمک این حدیث درد دل است</p></div>
<div class="m2"><p>عشق بی درد دل حرام بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کشته عشق گرد و سوخته شو</p></div>
<div class="m2"><p>زانکه بی این دو کار خام بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشتهٔ عشق را به خون شویند</p></div>
<div class="m2"><p>آب اگر نیست خون تمام بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کفن عاشقان ز خون سازند</p></div>
<div class="m2"><p>کفنی به ز خون کدام بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از ازل تا ابد ز مستی عشق</p></div>
<div class="m2"><p>بی قراری علی‌الدوام بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در ره عاشقان دلی باید</p></div>
<div class="m2"><p>که منزه ز دال و لام بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه خریدار نیک و بد باشد</p></div>
<div class="m2"><p>نه گرفتار ننگ و نام بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سرفرازی و خواجگی نخرد</p></div>
<div class="m2"><p>جملهٔ خلق را غلام بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نبود تیغش و اگر باشد</p></div>
<div class="m2"><p>با همه خلق در نیام بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همچو خود بی قرار و مست کند</p></div>
<div class="m2"><p>هر که را پیش او مقام بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گاه‌گاهی چنین شود عطار</p></div>
<div class="m2"><p>بو که این دولتش مدام بود</p></div></div>