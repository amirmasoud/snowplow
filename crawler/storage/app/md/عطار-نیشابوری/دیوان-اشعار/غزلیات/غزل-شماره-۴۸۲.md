---
title: >-
    غزل شمارهٔ ۴۸۲
---
# غزل شمارهٔ ۴۸۲

<div class="b" id="bn1"><div class="m1"><p>از می عشق تو چنان مستم</p></div>
<div class="m2"><p>که ندانم که نیست یا هستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آتش عشق چون درآمد تنگ</p></div>
<div class="m2"><p>من ز خود رستم و درو جستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لاجرم هست نیستم، هیچم</p></div>
<div class="m2"><p>لاجرم عاقلی نیم، مستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند گویم ز خود که در ره عشق</p></div>
<div class="m2"><p>جرعه‌ای خوردم و ز خود رستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ننگ من از من است بی من من</p></div>
<div class="m2"><p>بر پریدم به دوست پیوستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساقیا درد درد در ده زود</p></div>
<div class="m2"><p>که به یک درد توبه بشکستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باز، خمخانه برگشادم در</p></div>
<div class="m2"><p>باز، زنار بر میان بستم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرچه کردم به عمرهای دراز</p></div>
<div class="m2"><p>زان همه حسرت است در دستم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ترک عطار گفتم و بی او</p></div>
<div class="m2"><p>دیده پر خون به گوشه بنشستم</p></div></div>