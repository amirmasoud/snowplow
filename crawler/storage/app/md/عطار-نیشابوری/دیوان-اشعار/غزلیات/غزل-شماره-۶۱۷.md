---
title: >-
    غزل شمارهٔ ۶۱۷
---
# غزل شمارهٔ ۶۱۷

<div class="b" id="bn1"><div class="m1"><p>ساقیا خیز که تا رخت به خمار کشیم</p></div>
<div class="m2"><p>تائبان را به شرابی دو سه در کار کشیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاهد خانه‌نشین را به یکی کوزه درد</p></div>
<div class="m2"><p>اوفتان خیزان از خانه به بازار کشیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هوست هست که صافی دل و صوفی گردی</p></div>
<div class="m2"><p>خیز تا پیش مغان دردی خمار کشیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که را در ره اسلام قدم ثابت نیست</p></div>
<div class="m2"><p>به یکی جرعه میش در صف کفار کشیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که دعوی اناالحق کند و حق گوید</p></div>
<div class="m2"><p>انا گویان خودی را به سر دار کشیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند داریم نهان زیر مرقع زنار</p></div>
<div class="m2"><p>وقت نامد که خط اندر خط زنار کشیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هیچکس را ندهد دنیی و دین دست بهم</p></div>
<div class="m2"><p>هرکه گوید که دهد، خنجر انکار کشیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر تو دین می‌طلبی از سر دنیی برخیز</p></div>
<div class="m2"><p>که ز دین بار نیابیم مگر بار کشیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر ازین شاخ گل وصل طمع می‌داریم</p></div>
<div class="m2"><p>اندرین راه غم عشق چو عطار کشیم</p></div></div>