---
title: >-
    غزل شمارهٔ ۶۸۸
---
# غزل شمارهٔ ۶۸۸

<div class="b" id="bn1"><div class="m1"><p>ای غنچه غلام خندهٔ تو</p></div>
<div class="m2"><p>سرو آزاد بندهٔ تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افتاد سر هزار سرکش</p></div>
<div class="m2"><p>از طرهٔ سر فکندهٔ تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گلهای بهار نیم مرده</p></div>
<div class="m2"><p>از نرگس نیم زندهٔ تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورشید گرفته لوح از سر</p></div>
<div class="m2"><p>بر سر چو قلم دوندهٔ تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من کشته و غم کشندهٔ من</p></div>
<div class="m2"><p>تو دلکش و دل کشنده تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان است شفق که طوطی چرخ</p></div>
<div class="m2"><p>در خون گردد ز خندهٔ تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون سایه در آفتاب نرسد</p></div>
<div class="m2"><p>کی در تو رسد روندهٔ تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عطار به هر پری که پرد</p></div>
<div class="m2"><p>دانی که بود پرندهٔ تو</p></div></div>