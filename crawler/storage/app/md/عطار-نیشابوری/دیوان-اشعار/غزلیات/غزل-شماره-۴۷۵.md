---
title: >-
    غزل شمارهٔ ۴۷۵
---
# غزل شمارهٔ ۴۷۵

<div class="b" id="bn1"><div class="m1"><p>نه ز وصل تو نشان می‌یابم</p></div>
<div class="m2"><p>نه ز هجر تو امان می‌یابم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دشنهٔ هجر توام کشت از آنک</p></div>
<div class="m2"><p>تشنهٔ وصل تو جان می‌یابم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از میان تو چو مویی شده‌ام</p></div>
<div class="m2"><p>که تورا موی میان می‌یابم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به یقین از دهن پرشکرت</p></div>
<div class="m2"><p>اثری هم به گمان می‌یابم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر رخت تا به نگویی سخنی</p></div>
<div class="m2"><p>می‌ندانم که دهان می‌یابم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در صفات لبت از غایت عجز</p></div>
<div class="m2"><p>عقل را کند زبان می‌یابم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل و جان بر چو لبت آن دارد</p></div>
<div class="m2"><p>کین همه لایق آن می‌یابم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان به روی تو جهان روشن شد</p></div>
<div class="m2"><p>که تورا شمع جهان می‌یابم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنچه از خلق نهان می‌جستم</p></div>
<div class="m2"><p>در جمال تو عیان می‌یابم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی تو عطار جگر سوخته را</p></div>
<div class="m2"><p>نتوان گفت چه سان می‌یابم</p></div></div>