---
title: >-
    غزل شمارهٔ ۵۶۱
---
# غزل شمارهٔ ۵۶۱

<div class="b" id="bn1"><div class="m1"><p>ز تو گر یک نظر آید به جانم</p></div>
<div class="m2"><p>نباید این جهان و آن جهانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا آن یک نفس جاوید نه بس</p></div>
<div class="m2"><p>تو دانی دیگر و من می ندانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر گویی سرت خواهم بریدن</p></div>
<div class="m2"><p>ز شادی چون قلم بر سر دوانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وگر گویی به لب جان خواهمت داد</p></div>
<div class="m2"><p>به لب آید بدین امید جانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر خاکی شد و گردیت آورد</p></div>
<div class="m2"><p>ز تو یک روز می‌باید امانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که تا از اشک بنشانم من آن گرد</p></div>
<div class="m2"><p>همه بر خاک راهت خون فشانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کلاه چرخ بربایم اگر تو</p></div>
<div class="m2"><p>کمر سازی ز دلق و طیلسانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو بی روی تو عالم می نبینم</p></div>
<div class="m2"><p>در آن عزمم که در چشمت نشانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ولی ترسم که در خون سرشکم</p></div>
<div class="m2"><p>شوی غرقه من از تو دور مانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو هستی در میان جانم و من</p></div>
<div class="m2"><p>ز شوق روی تو جان بر میانم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر من باشم و گرنه غمی نیست</p></div>
<div class="m2"><p>تو می‌باید که باشی جاودانم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که گر صد سود خواهم کرد بی تو</p></div>
<div class="m2"><p>نخواهد بود جز حاصل زیانم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>و گر در بند خویش آری مرا تو</p></div>
<div class="m2"><p>نخواهم کفر و دین در بند آنم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در ایمان گر نیابم از تو بویی</p></div>
<div class="m2"><p>یقین دانم که در کافرستانم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وگر در کفر بویی یابم از تو</p></div>
<div class="m2"><p>ز ایمان نور بر گردون رسانم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو تا دل برده‌ای جانا ز عطار</p></div>
<div class="m2"><p>به مهر توست جان مهربانم</p></div></div>