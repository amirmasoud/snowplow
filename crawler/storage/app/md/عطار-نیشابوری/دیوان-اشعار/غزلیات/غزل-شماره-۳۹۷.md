---
title: >-
    غزل شمارهٔ ۳۹۷
---
# غزل شمارهٔ ۳۹۷

<div class="b" id="bn1"><div class="m1"><p>آتش عشق تو دلم، کرد کباب ای پسر</p></div>
<div class="m2"><p>زیر و زبر شدم ز تو، چیست صواب ای پسر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون من خسته دل ز تو، زیر و زبر بمانده‌ام</p></div>
<div class="m2"><p>زیر و زبر چه می‌کنی، زلف بتاب ای پسر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا که بدید چشم من، چهرهٔ جانفزای تو</p></div>
<div class="m2"><p>ساخته‌ام ز خون دل، چره خضاب ای پسر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان من از جهان غم، سوخته شد به جان تو</p></div>
<div class="m2"><p>جام بیا و درفکن، بادهٔ ناب ای پسر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آب حیات جان من، جام شراب می‌دهد</p></div>
<div class="m2"><p>زانکه به جان همی رسد، جام شراب ای پسر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند غم جهان خوری، چیست جهان، خرابه‌ای</p></div>
<div class="m2"><p>ما همه در خرابه‌ای، مست و خراب ای پسر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هین که نشست آسمان، در پی گوشمال تو</p></div>
<div class="m2"><p>خیز و بمال اندکی، گوش رباب ای پسر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نقل چه می‌کنیم ما، قند لب تو نقل بس</p></div>
<div class="m2"><p>زان دو لب شکرفشان، هین بشتاب ای پسر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شمع چه می‌کنیم ما، نور رخ تو شمع بس</p></div>
<div class="m2"><p>برفکن از رخ چو مه، خیز نقاب ای پسر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نرگس نیم خواب را، باز کن و شراب خور</p></div>
<div class="m2"><p>غفلت ماست خواب ما، چند ز خواب ای پسر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زان دو لب تو یک شکر، بنده سال می‌کند</p></div>
<div class="m2"><p>مفتی این سخن تویی، چیست جواب ای پسر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گرچه تو آفتاب را، رخ بنهاده‌ای به رخ</p></div>
<div class="m2"><p>با من دلشده مرا، خر به خلاب ای پسر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وصف تو گر فرید را، ورد زبان همی شود</p></div>
<div class="m2"><p>آب شود ز رشک او، در خوشاب ای پسر</p></div></div>