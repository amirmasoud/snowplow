---
title: >-
    غزل شمارهٔ ۴۵۷
---
# غزل شمارهٔ ۴۵۷

<div class="b" id="bn1"><div class="m1"><p>گشت جهان همچو نگار ای غلام</p></div>
<div class="m2"><p>بادهٔ گلرنگ بیار ای غلام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با گل و با بلبل و با مل بهم</p></div>
<div class="m2"><p>وصل‌طلب فصل بهار ای غلام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلبل عاشق به صبوحی درست</p></div>
<div class="m2"><p>می‌شنوی نالهٔ زار ای غلام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نرگس سرمست نگر کاو فکند</p></div>
<div class="m2"><p>سر ز گرانی به کنار ای غلام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش نشین تازه بکن کار آب</p></div>
<div class="m2"><p>بیش مبر آب ز کار ای غلام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آب بده زانکه جهان هر نفس</p></div>
<div class="m2"><p>خاک کند چون تو هزار ای غلام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زخم خمارم چو به زاری بکشت</p></div>
<div class="m2"><p>نوش خمارم ز خم آر ای غلام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روز چو شد باز نیاید دگر</p></div>
<div class="m2"><p>چند کنی روز گذار ای غلام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چند شمار زر و زینت کنی</p></div>
<div class="m2"><p>فکر کن از روز شمار ای غلام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیستی آگه که دم واپسین</p></div>
<div class="m2"><p>از تو برآرند دمار ای غلام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قصهٔ مرگم جگر و دل بسوخت</p></div>
<div class="m2"><p>دست ازین قصه بدار ای غلام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>واقعهٔ مشکل دارالغرور</p></div>
<div class="m2"><p>برد ز عطار قرار ای غلام</p></div></div>