---
title: >-
    غزل شمارهٔ ۵۸۳
---
# غزل شمارهٔ ۵۸۳

<div class="b" id="bn1"><div class="m1"><p>در ره او بی سر و پا می‌روم</p></div>
<div class="m2"><p>بی تبرا و تولا می‌روم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایمن از توحید و از شرک آمدم</p></div>
<div class="m2"><p>فارغ از امروز و فردا می‌روم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه من و نه ما شناسم ذره‌ای</p></div>
<div class="m2"><p>زانکه دایم بی من و ما می‌روم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سالک مطلق شدم چون آفتاب</p></div>
<div class="m2"><p>لاجرم از سایه تنها می‌روم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرغ عشقم هر زمانی صد جهان</p></div>
<div class="m2"><p>بی پر و بی بال زیبا می‌روم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون همه دانم ولیکن هیچ دان</p></div>
<div class="m2"><p>لاجرم نادان و دانا می‌روم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قطره‌ای بودم ز دریا آمده</p></div>
<div class="m2"><p>این زمان با قعر دریا می‌روم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در دلم تا عشق قدس آرام یافت</p></div>
<div class="m2"><p>من ز دل با جان شیدا می‌روم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شرح عشق او بگویم با تو راست</p></div>
<div class="m2"><p>گرچه من گنگم که گویا می‌روم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بارگاهی زد ز آدم عشق او</p></div>
<div class="m2"><p>گفت بر یک جا به صد جا می‌روم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زو بپرسیدند کاخر تا کجا</p></div>
<div class="m2"><p>گفت روزی در به صحرا می‌روم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون هویت از بطون در پرده بود</p></div>
<div class="m2"><p>در هویت بس هویدا می‌روم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرچه نه پنهانم و نه آشکار</p></div>
<div class="m2"><p>هم نهان هم آشکارا می‌روم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر هویدا خواهیم پنهان شوم</p></div>
<div class="m2"><p>ور نهان جوییم پیدا می‌روم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه چنینم نه چنان نه هردوم</p></div>
<div class="m2"><p>بل کزین هر دو مبرا می‌روم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون فرید از خویش یکتا می‌رود</p></div>
<div class="m2"><p>هم به سر من فرد و یکتا می‌روم</p></div></div>