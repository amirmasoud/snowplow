---
title: >-
    غزل شمارهٔ ۴۳۰
---
# غزل شمارهٔ ۴۳۰

<div class="b" id="bn1"><div class="m1"><p>بنمود رخ از پرده، دل گشت گرفتارش</p></div>
<div class="m2"><p>دانی که کجا شد دل در زلف نگونسارش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بس که سر زلفش در خون دل من شد</p></div>
<div class="m2"><p>در نافهٔ زلف او دل گشت جگرخوارش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون مشک و جگر دید او در ناک دهی آمد</p></div>
<div class="m2"><p>ناک از چه دهد آخر خاکی شده عطارش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای کاش چو دل برد او بارش دهدی باری</p></div>
<div class="m2"><p>چون بار دهد دل را چون دل ندهد بارش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جانا چو دلم دارد درد از سر زلف تو</p></div>
<div class="m2"><p>بگذار در آن دردش وز دست بمگذارش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بردی دلم و پایش بستی به سر زلفت</p></div>
<div class="m2"><p>دل باز نمی‌خواهم اما تو نکو دارش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا بو که به دست آرم یک ذره وصال تو</p></div>
<div class="m2"><p>جان می‌بفروشم من کس نیست خریدارش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون نیست وصالت را در کون خریداری</p></div>
<div class="m2"><p>عطار کجا افتد یک ذره سزاوارش</p></div></div>