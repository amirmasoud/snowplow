---
title: >-
    غزل شمارهٔ ۲۵۲
---
# غزل شمارهٔ ۲۵۲

<div class="b" id="bn1"><div class="m1"><p>بار دگر پیر ما مفلس و قلاش شد</p></div>
<div class="m2"><p>در بن دیر مغان ره زن اوباش شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میکدهٔ فقر یافت خرقهٔ دعوی بسوخت</p></div>
<div class="m2"><p>در ره ایمان به کفر در دو جهان فاش شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زآتش دل پاک سوخت مدعیان را به دم</p></div>
<div class="m2"><p>دردی اندوه خورد عاشق و قلاش شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پاک بری چست بود در ندب لامکان</p></div>
<div class="m2"><p>کم زن و استاد گشت حیله گر و طاش شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لاشهٔ دل را ز عشق بار گران برنهاد</p></div>
<div class="m2"><p>فانی و لاشییء گشت یار هویداش شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راست که بنمود روی آن مه خورشید چهر</p></div>
<div class="m2"><p>عقل چو طاوس گشت وهم چو خفاش شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وهم ز تدبیر او آزر بت‌ساز گشت</p></div>
<div class="m2"><p>عقل ز تشویر او مانی نقاش شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون دل عطار را بحر گهربخش دید</p></div>
<div class="m2"><p>در سخن آمد به حرف ابر گهرپاش شد</p></div></div>