---
title: >-
    غزل شمارهٔ ۲۴۷
---
# غزل شمارهٔ ۲۴۷

<div class="b" id="bn1"><div class="m1"><p>قصهٔ عشق تو چون بسیار شد</p></div>
<div class="m2"><p>قصه‌گویان را زبان از کار شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قصهٔ هرکس چو نوعی نیز بود</p></div>
<div class="m2"><p>ره فراوان گشت و دین بسیار شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر یکی چون مذهبی دیگر گرفت</p></div>
<div class="m2"><p>زین سبب ره سوی تو دشوار شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ره به خورشید است یک یک ذره را</p></div>
<div class="m2"><p>لاجرم هر ذره دعوی‌دار شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیر و شر چون عکس روی و موی توست</p></div>
<div class="m2"><p>گشت نور افشان و ظلمت‌بار شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ظلمت مویت بیافت انکار کرد</p></div>
<div class="m2"><p>پرتو رویت بتافت اقرار شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که باطل بود در ظلمت فتاد</p></div>
<div class="m2"><p>وانکه بر حق بود پر انوار شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مغز نور از ذوق نورالنور گشت</p></div>
<div class="m2"><p>مغز ظلمت از تحسر نار شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مدتی در سیر آمد نور و نار</p></div>
<div class="m2"><p>تا زوال آمد ره و رفتار شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پس روش برخاست پیدا شد کشش</p></div>
<div class="m2"><p>رهروان را لاجرم پندار شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون کشش از حد و غایت درگذشت</p></div>
<div class="m2"><p>هم وسایط رفت و هم اغیار شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نار چون از موی خاست آنجا گریخت</p></div>
<div class="m2"><p>نور نیز از پرده با رخسار شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>موی از عین عدد آمد پدید</p></div>
<div class="m2"><p>روی از توحید بنمودار شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ناگهی توحید از پیشان بتافت</p></div>
<div class="m2"><p>تا عدد هم‌رنگ روی یار شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر غضب چون داشت رحمت سبقتی</p></div>
<div class="m2"><p>گر عدد بود از احد هموار شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کل شیء هالک الا وجهه</p></div>
<div class="m2"><p>سلطنت بنمود و برخوردار شد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چیست حاصل عالمی پر سایه بود</p></div>
<div class="m2"><p>هر یکی را هستییی مسمار شد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صد حجب اندر حجب پیوسته گشت</p></div>
<div class="m2"><p>تا رونده در پس دیوار شد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرتفع چو شد به توحید آن حجب</p></div>
<div class="m2"><p>خفته از خواب هوس بیدار شد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گرچه در خون گشت دل عمری دراز</p></div>
<div class="m2"><p>این زمان کودک همه دلدار شد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هرکه او زین زندگی بویی نیافت</p></div>
<div class="m2"><p>مرده زاد از مادر و مردار شد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وان کزین طوبی مشک‌افشان دمی</p></div>
<div class="m2"><p>برد بویی تا ابد عطار شد</p></div></div>