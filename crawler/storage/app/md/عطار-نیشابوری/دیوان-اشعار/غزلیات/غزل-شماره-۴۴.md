---
title: >-
    غزل شمارهٔ ۴۴
---
# غزل شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>ندای غیب به جان تو می‌رسد پیوست</p></div>
<div class="m2"><p>که پای در نه و کوتاه کن ز دنیی دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار بادیه در پیش بیش داری تو</p></div>
<div class="m2"><p>تو این چنین ز شراب غرور ماندی مست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان پلی است بدان سوی جه که هر ساعت</p></div>
<div class="m2"><p>پدید آید ازین پل هزار جای شکست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پل برون نشود با چنین پلی کارت</p></div>
<div class="m2"><p>برو بجه ز چنین پل که نیست جای نشست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو سیل پل‌شکن از کوه سر فرود آرد</p></div>
<div class="m2"><p>بیوفتد پل و در زیر پل بمانی پست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو غافلی و به هفتاد پشت شد چو کمان</p></div>
<div class="m2"><p>تو خوش بخفته‌ای و تیر عمر رفت از شست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر تو زار بگریی به صد هزاران چشم</p></div>
<div class="m2"><p>ز کار بیهدهٔ خویش جای آنت هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فرشته‌ای تو و دیوی سرشته در تو به هم</p></div>
<div class="m2"><p>گهی فرشته طلب، گه بمانده دیو پرست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هزار بار به نامرده طوطی جانت</p></div>
<div class="m2"><p>چگونه زین قفس آهنین تواند جست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو گرچه زنده‌ای امروز لیک در گوری</p></div>
<div class="m2"><p>چو تن به گور فرو رفت جان ز گور برست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو جان بمرد از این زندگانی ناخوش</p></div>
<div class="m2"><p>ز خود برید و میان خوشی به حق پیوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>میان جشن بقا کرد نوش نوشش باد</p></div>
<div class="m2"><p>ز دست ساقی جان ساغر شراب الست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دل آن دل است که چون از نهاد خویش گسست</p></div>
<div class="m2"><p>ز کبریای حق اندیشه می‌کند پیوست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به حکم بند قبای فلک ز هم بگشاد</p></div>
<div class="m2"><p>دلی که از کمر معرفت میان در بست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به زیر خاک بسی خواب داری ای عطار</p></div>
<div class="m2"><p>مخسب خیز چو عمر آمدت به نیمهٔ شصت</p></div></div>