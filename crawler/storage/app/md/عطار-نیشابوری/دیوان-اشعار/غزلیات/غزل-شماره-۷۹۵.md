---
title: >-
    غزل شمارهٔ ۷۹۵
---
# غزل شمارهٔ ۷۹۵

<div class="b" id="bn1"><div class="m1"><p>گر من اندر عشق مرد کارمی</p></div>
<div class="m2"><p>از بد و نیک و جهان بیزارمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کفر و دین درباختم در بیخودی</p></div>
<div class="m2"><p>چیستی گر بیخود از دلدارمی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کاشکی گر محرم مسجد نیم</p></div>
<div class="m2"><p>محرم دردی‌کش خمارمی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کاشکی گر در خور مصحف نیم</p></div>
<div class="m2"><p>یک نفس اندر خور زنارمی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دلم گر هیچ هشیاریستی</p></div>
<div class="m2"><p>از می غفلت دمی هشیارمی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون نمی‌بینم جمال روی دوست</p></div>
<div class="m2"><p>زین مصیبت روی در دیوارمی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر ندارم از وصال او نشان</p></div>
<div class="m2"><p>باری از کویش نشانی دارمی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر مرا در پرده راهستی دمی</p></div>
<div class="m2"><p>محرم او زحمت اغیارمی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر نبودی راه از من در حجاب</p></div>
<div class="m2"><p>من درین ره رهبر عطارمی</p></div></div>