---
title: >-
    غزل شمارهٔ ۵۱۱
---
# غزل شمارهٔ ۵۱۱

<div class="b" id="bn1"><div class="m1"><p>آن در که بسته باید تا چند باز دارم</p></div>
<div class="m2"><p>کامروز وقتش آمد کان در فراز دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با هر که از حقیقت رمزی دمی بگویم</p></div>
<div class="m2"><p>گوید مگوی یعنی برگ مجاز دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا لاجرم به مردی با پاره پاره جانی</p></div>
<div class="m2"><p>در جان خویش گفتم چندان که راز دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون این جهان و آن یک با صد جهان دیگر</p></div>
<div class="m2"><p>در چشم من فروشد چون چشم باز دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چیزی برفت از من و اینجا نماند چیزی</p></div>
<div class="m2"><p>تا این شود چون آن یک کاری دراز دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جانی که داشتم من، شد محو عشق جانان</p></div>
<div class="m2"><p>جان من است جانان، جان دلنواز دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نی نی اگر چو شمعی این دم زدم ز گرمی</p></div>
<div class="m2"><p>اکنون چو شمع از آن دم سر زیر گاز دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون عز و ناز ختم است بر تو همیشه دایم</p></div>
<div class="m2"><p>تا چند خویشتن را در عز و ناز دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کارم فتاد و از من تو فارغی به غایت</p></div>
<div class="m2"><p>نه صبر می‌توانم نه کارساز دارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از بس که بی نیازی است آنجا که حضرت توست</p></div>
<div class="m2"><p>من زاد این بیابان عجز و نیاز دارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شوریدهٔ جهانم چون قربت تو جویم</p></div>
<div class="m2"><p>محمود نیستم من، خو با ایاز دارم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بازی اگر نشیند بر دوش من نگیرم</p></div>
<div class="m2"><p>ورنه کسی نبوده است البته باز دارم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من شمع جمع عشقم نه جان به تن بمانده</p></div>
<div class="m2"><p>جان در میان آتش تن در گداز دارم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>لاف ای فرید کم زن زیرا که در ره او</p></div>
<div class="m2"><p>چون سرنگون نه‌ای تو صد سرفراز دارم</p></div></div>