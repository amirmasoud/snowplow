---
title: >-
    حکایت سلیمان و نگین انگشتری او
---
# حکایت سلیمان و نگین انگشتری او

<div class="b" id="bn1"><div class="m1"><p>هیچ گوهر را نبود آن سروری</p></div>
<div class="m2"><p>کان سلیمان داشت در انگشتری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان نگینش بود چندان نام و بانگ</p></div>
<div class="m2"><p>و آن نگین خود بود سنگی نیم دانگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون سلیمان کرد آن گوهر نگین</p></div>
<div class="m2"><p>زیر حکمش شد همه روی زمین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون سلیمان ملک خود چندان بدید</p></div>
<div class="m2"><p>جملهٔ آفاق در فرمان بدید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه شادروان چل فرسنگ داشت</p></div>
<div class="m2"><p>هم بنا بر نیم دانگ سنگ داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت چون این مملکت وین کار و بار</p></div>
<div class="m2"><p>زین قدر سنگ است دایم پای دار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من نمی‌خواهم که در دنیا و دین</p></div>
<div class="m2"><p>بازماند کس به ملکی هم چنین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پادشاها من به چشم اعتبار</p></div>
<div class="m2"><p>آفت این ملک دیدم آشکار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هست آن در جنب عقبی مختصر</p></div>
<div class="m2"><p>بعد از این کس را مده هرگز دگر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من ندارم با سپاه و ملک کار</p></div>
<div class="m2"><p>می‌کنم زنبیل بافی اختیار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرچه زان گوهر سلیمان شاه شد</p></div>
<div class="m2"><p>آن گهر بودش که بند راه شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زان به پانصد سال بعد از انبیا</p></div>
<div class="m2"><p>با بهشت عدن گردد آشنا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن گهر چون با سلیمان این کند</p></div>
<div class="m2"><p>کی چو تو سرگشته را تمکین کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون گهر سنگیست چندین کان مکن</p></div>
<div class="m2"><p>جز برای روی جانان جان مکن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دل ز گوهر برکن ای گوهر طلب</p></div>
<div class="m2"><p>جوهری را باش دایم در طلب</p></div></div>