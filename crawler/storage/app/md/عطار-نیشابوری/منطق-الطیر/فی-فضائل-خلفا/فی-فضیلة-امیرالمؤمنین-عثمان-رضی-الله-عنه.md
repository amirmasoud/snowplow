---
title: >-
    فی فضیلة امیرالمؤمنین عثمان رضی الله عنه
---
# فی فضیلة امیرالمؤمنین عثمان رضی الله عنه

<div class="b" id="bn1"><div class="m1"><p>خواجهٔ سنت که نور مطلق است</p></div>
<div class="m2"><p>بل خداوند دو نور پر حق است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنک غرق قدس و عرفان آمدست</p></div>
<div class="m2"><p>صدر دین عثمن عفان آمدست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رفعتی کان رایت ایمان گرفت</p></div>
<div class="m2"><p>از امیرالمؤمنین عثمن گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رونقی کان عرصهٔ کونین یافت</p></div>
<div class="m2"><p>از دل پر نور ذی النورین یافت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یوسف ثانی به قول مصطفا</p></div>
<div class="m2"><p>بحر تقوی و حیا کان وفا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کار ذی القربی به جان پرداخته</p></div>
<div class="m2"><p>جان خود در کار ایشان باخته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر بریدندش که تا بنشسته‌ای</p></div>
<div class="m2"><p>ازچه پیوسته رحم پیوسته‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم هدایت در جهان و هم هنر</p></div>
<div class="m2"><p>امتش در عهد او شد بیشتر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هم به عهد او شد ایمان منتشر</p></div>
<div class="m2"><p>هم ز حکمش گشت قرآن منتشر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سید سادات گفتی بر فلک</p></div>
<div class="m2"><p>شرم دارد دایم از عثمن ملک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هم پیامبر گفت در کشف و حجاب</p></div>
<div class="m2"><p>حق نخواهد کرد با عثمن عتاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون نبود او تا کند بیعت قبول</p></div>
<div class="m2"><p>بد به جای دست او دست رسول</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حاضران گفتند ما برسودمی</p></div>
<div class="m2"><p>گر چو ذوالنورین غایب بودمی</p></div></div>