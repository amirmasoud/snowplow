---
title: >-
    حکایت شیخ نصر آباد که پس از چهل حج طواف آتشگاه گبران می‌کرد
---
# حکایت شیخ نصر آباد که پس از چهل حج طواف آتشگاه گبران می‌کرد

<div class="b" id="bn1"><div class="m1"><p>شیخ نصرآباد را بگرفت درد</p></div>
<div class="m2"><p>کرد چل حج بر توکل اینت مرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بعد از آن موی سپید و تن نزار</p></div>
<div class="m2"><p>برهنه دیدش کسی با یک از ار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل دلش تابی و در جانش تفی</p></div>
<div class="m2"><p>بسته زناری و بگشاده کفی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آمده نه از سر دعوی و لاف</p></div>
<div class="m2"><p>گرد آتش گاه گبری در طواف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت گفتم ای بزرگ روزگار</p></div>
<div class="m2"><p>این چه کار تست آخر شرم دار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کرده‌ای چندین حج و بس سروری</p></div>
<div class="m2"><p>حاصل آن جمله آمد کافری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این چنین کار از سر خامی بود</p></div>
<div class="m2"><p>اهل دل را از تو بدنامی بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وین کدامین شیخ کرد، این راه کیست</p></div>
<div class="m2"><p>می‌ندانی این که آتش گاه کیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شیخ گفتا کار من سخت اوفتاد</p></div>
<div class="m2"><p>آتشم در خانه و رخت اوفتاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شد ازین آتش مرا خرمن بباد</p></div>
<div class="m2"><p>داد کلی نام و ننگ من بباد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گشته‌ای کالیو کار خویش من</p></div>
<div class="m2"><p>من ندانم حیله‌ای زین بیش من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون درآید این چنین آتش به جان</p></div>
<div class="m2"><p>کی گذارد نام و ننگم یک زمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا گرفتار چنین کار آمدم</p></div>
<div class="m2"><p>ازکنشت و کعبه بی‌زار آمدم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ذره‌ای گر حیرتت آید پدید</p></div>
<div class="m2"><p>همچو من صد حسرتت آید پدید</p></div></div>