---
title: >-
    گفتار یک صوفی با مردی که کلیدش را گم کرده بود
---
# گفتار یک صوفی با مردی که کلیدش را گم کرده بود

<div class="b" id="bn1"><div class="m1"><p>صوفیی می‌رفت، آوازی شنید</p></div>
<div class="m2"><p>کان یکی می‌گفت گم کردم کلید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که کلیدی یافتست این جایگاه</p></div>
<div class="m2"><p>زانک دربستست این بر خاک راه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر در من بسته ماند، چون کنم</p></div>
<div class="m2"><p>غصهٔ پیوسته ماند، چون کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صوفیش گفتا؛که گفتت خسته باش</p></div>
<div class="m2"><p>در چو می‌دانی برو، گو بسته باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر در بسته چو بنشینی بسی</p></div>
<div class="m2"><p>هیچ شک نبود که بگشاید کسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کار تو سهل است و دشوار آن من</p></div>
<div class="m2"><p>کز تحیر می‌بسوزد جان من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست کارم را نه پایی نه سری</p></div>
<div class="m2"><p>نه کلیدم بود هرگز نه دری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کاش این صوفی بسی بشتافتی</p></div>
<div class="m2"><p>بسته یا بگشاده‌ای در، یافتی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست مردم را نصیبی جز خیال</p></div>
<div class="m2"><p>می نداند هیچ کس تا چیست حال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر که گوید چون کنم، گو چون مکن</p></div>
<div class="m2"><p>تا کنون چون کرده‌ای اکنون مکن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر که او در وادی حیرت فتاد</p></div>
<div class="m2"><p>هر نفس در بی‌عدد حسرت فتاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حیرت و سرگشتگی تا کی برم</p></div>
<div class="m2"><p>پی چو گم کردند من چون پی برم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>می‌ندانم کاشکی می‌دانمی</p></div>
<div class="m2"><p>که اگر می‌دانمی حیرانمی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مر مرا اینجا شکایت شکر شد</p></div>
<div class="m2"><p>کفر ایمان گشت و ایمان کفر شد</p></div></div>