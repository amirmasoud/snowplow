---
title: >-
    حکایت اسکندر که خود به رسولی می‌رفت
---
# حکایت اسکندر که خود به رسولی می‌رفت

<div class="b" id="bn1"><div class="m1"><p>گفت چون اسکندر آن صاحب قبول</p></div>
<div class="m2"><p>خواستی جایی فرستادن رسول</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون رسد آخر خود آن شاه جهان</p></div>
<div class="m2"><p>جامه پوشیدی و خود رفتی نهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس بگفتی آنچ کس نشنوده است</p></div>
<div class="m2"><p>گفتی اسکندر چنین فرموده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در همه عالم نمی‌دانست کس</p></div>
<div class="m2"><p>کین رسول اسکندر است آنجا و بس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیچ کس چون چشم اسکندر نداشت</p></div>
<div class="m2"><p>گرچه گفت اسکندر و باور نداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هست راهی سوی هر دل شاه را</p></div>
<div class="m2"><p>لیک ره نبود دل گم راه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر برون حجره شد بیگانه بود</p></div>
<div class="m2"><p>غم مخور خوردی درون هم خانه بود</p></div></div>