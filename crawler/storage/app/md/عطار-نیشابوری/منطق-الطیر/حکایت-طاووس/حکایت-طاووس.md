---
title: >-
    حکایت طاووس
---
# حکایت طاووس

<div class="b" id="bn1"><div class="m1"><p>بعد از آن طاوس آمد زرنگار</p></div>
<div class="m2"><p>نقش پرش صد چه بل که صد هزار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون عروسی جلوه کردن ساز کرد</p></div>
<div class="m2"><p>هر پر او جلوه‌ای آغاز کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت تا نقاش غیبم نقش بست</p></div>
<div class="m2"><p>چینیان را شد قلم انگشت دست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه من جبریل مرغانم ولیک</p></div>
<div class="m2"><p>رفت بر من از قضا کاری نه نیک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یار شد با من به یک جا مار زشت</p></div>
<div class="m2"><p>تا بیفتادم به خواری از بهشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون بدل کردند خلوت جای من</p></div>
<div class="m2"><p>تخت بند پای من شد پای من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عزم آن دارم کزین تاریک جای</p></div>
<div class="m2"><p>رهبری باشد به خلدم رهنمای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من نه آن مردم که در سلطان رسم</p></div>
<div class="m2"><p>بس بود اینم که در دروان رسم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کی بود سیمرغ را پروای من</p></div>
<div class="m2"><p>بس بود فردوس عالی جای من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من ندارم در جهان کاری دگر</p></div>
<div class="m2"><p>تا بهشتم ره دهد باری دگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هدهدش گفت ای ز خود گم کرده راه</p></div>
<div class="m2"><p>هر که خواهد خانه‌ای از پادشاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گوی نزدیکی او این زان به است</p></div>
<div class="m2"><p>خانه‌ای از حضرت سلطان به است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خانهٔ نفس است خلد پر هوس</p></div>
<div class="m2"><p>خانهٔ دل مقصد صدق است و بس</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حضرت حق هست دریای عظیم</p></div>
<div class="m2"><p>قطرهٔ خرد است جنات النعیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>قطره باشد هرکه را دریا بود</p></div>
<div class="m2"><p>هرچ جز دریا بود سودا بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون به دریا می‌توانی راه یافت</p></div>
<div class="m2"><p>سوی یک شبنم چرا باید شتافت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هرک داند گفت با خورشید راز</p></div>
<div class="m2"><p>کی تواند ماند از یک ذره باز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هرک کل شد جزو را با او چه کار</p></div>
<div class="m2"><p>وانک جان شد عضو را با او چه کار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر تو هستی مرد کلی، کل ببین</p></div>
<div class="m2"><p>کل طلب، کل باش، کل شو، کل گزین</p></div></div>