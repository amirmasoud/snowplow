---
title: >-
    قصه رانده شدن آدم از بهشت
---
# قصه رانده شدن آدم از بهشت

<div class="b" id="bn1"><div class="m1"><p>کرد شاگردی سؤال از اوستاد</p></div>
<div class="m2"><p>کز بهشت آدم چرا بیرون فتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت بود آدم همی عالی گهر</p></div>
<div class="m2"><p>چون به فردوسی فرو آورد سر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هاتفی برداشت آوازی بلند</p></div>
<div class="m2"><p>کای بهشتت کرده از صد گونه بند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرک در هر دو جهان بیرون ما</p></div>
<div class="m2"><p>سر فرو آرد به چیزی دون ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما زوال آریم بر وی هرچ هست</p></div>
<div class="m2"><p>زانک نتوان زد به غیر دوست دست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جای باشد پیش جانان صد هزار</p></div>
<div class="m2"><p>جای بی‌جانان کجا آید به کار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرک جز جانان به چیزی زنده شد</p></div>
<div class="m2"><p>گر همه آدم بود افکنده شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اهل جنت را چنین آمد خبر</p></div>
<div class="m2"><p>کاولین چیزی دهند آنجا جگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اهل جنت چون نباشد اهل راز</p></div>
<div class="m2"><p>زان جگر خوردن ز سرگیرند باز</p></div></div>