---
title: >-
    بیان وادی استغنا
---
# بیان وادی استغنا

<div class="b" id="bn1"><div class="m1"><p>بعد ازین وادی استغنا بود</p></div>
<div class="m2"><p>نه درو دعوی و نه معنی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌جهد از بی‌نیازی صرصری</p></div>
<div class="m2"><p>می‌زند بر هم به یک دم کشوری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هفت دریا یک شمر اینجا بود</p></div>
<div class="m2"><p>هفت اخگر یک شرر اینجا بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هشت جنت نیز اینجا مرده‌ایست</p></div>
<div class="m2"><p>هفت دوزخ همچو یخ افسرده ایست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هست موری را هم اینجا ای عجب</p></div>
<div class="m2"><p>هر نفس صد پیل اجری بی سبب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا کلاغی را شود پر، حوصله</p></div>
<div class="m2"><p>کس نماند زنده در صد قافله</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صد هزاران سبز پوش از غم بسوخت</p></div>
<div class="m2"><p>تا که آدم را چراغی برفروخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صد هزاران جسم خالی شد ز روح</p></div>
<div class="m2"><p>تا درین حضرت دروگر گشت نوح</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صد هزاران پشه در لشگر فتاد</p></div>
<div class="m2"><p>تا براهیم از میان با سرفتاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صد هزاران طفل سر ببریده گشت</p></div>
<div class="m2"><p>تا کلیم الله صاحب دیده گشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صد هزاران خلق در زنار شد</p></div>
<div class="m2"><p>تا که عیسی محرم اسرار شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صد هزاران جان و دل تاراج یافت</p></div>
<div class="m2"><p>تا محمد یک شبی معراج یافت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قدر نه نو دارد اینجا نه کهن</p></div>
<div class="m2"><p>خواه اینجا هیچ کن خواهی مکن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر جهانی دل کبابی دیده‌ای</p></div>
<div class="m2"><p>همچنان دانم که خوابی دیده‌ای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر درین دریا هزاران جان فتاد</p></div>
<div class="m2"><p>شبنمی در بحر بی‌پایان فتاد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر فرو شد صد هزاران سر بخواب</p></div>
<div class="m2"><p>ذره‌ای با سایه‌ای شد ز آفتاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر بریخت افلاک و انجم لخت لخت</p></div>
<div class="m2"><p>در جهان کم گیر برگی از درخت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر ز ماهی در عدم شد تا به ماه</p></div>
<div class="m2"><p>پای مور لنگ شد در قعر چاه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر دو عالم شد همه یک بارنیست</p></div>
<div class="m2"><p>در زمین ریگی همان انگار نیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر نماند از دیو وز مردم اثر</p></div>
<div class="m2"><p>از سر یک قطره باران در گذر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر بریخت این جملهٔ تن‌ها به خاک</p></div>
<div class="m2"><p>موی حیوانی اگر نبود چه باک</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر شد اینجا جزو و کل کلی تباه</p></div>
<div class="m2"><p>کم شد از روی زمین یک برگ کاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر به یک ره گشت این نه طشت گم</p></div>
<div class="m2"><p>قطره‌ای در هشت دریا گشت گم</p></div></div>