---
title: >-
    حکایت مگسی که به کندو رفت و دست و پایش در عسل ماند
---
# حکایت مگسی که به کندو رفت و دست و پایش در عسل ماند

<div class="b" id="bn1"><div class="m1"><p>آن مگس می‌شد ز بهر توشه‌ای</p></div>
<div class="m2"><p>دید کندوی عسل در گوشه‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد ز شوق آن عسل دل داده‌ای</p></div>
<div class="m2"><p>در خروش آمد که کو آزاده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کز من مسکین جوی بستاند او</p></div>
<div class="m2"><p>در درون کندوم بنشاند او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاخ وصلم گر ببرآید چنین</p></div>
<div class="m2"><p>منج نیکوتر بود در انگبین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرد کارش را کسی، بیرون شوی</p></div>
<div class="m2"><p>در درون ره دادش و بستد جوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون مگس را با عسل افتاد کار</p></div>
<div class="m2"><p>پای و دستش در عسل شد استوار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در طپیدن سست شد پیوند او</p></div>
<div class="m2"><p>وز چخیدن سخت‌تر شد بند او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در خروش آمد که ما را قهر کشت</p></div>
<div class="m2"><p>وانگبینم سخت‌تر از زهر کشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر جوی دادم، دو جو اکنون دهم</p></div>
<div class="m2"><p>بوک ازین درماندگی بیرون جهم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کس درین وادی دمی فارغ مباد</p></div>
<div class="m2"><p>مرد این وادی به جز بالغ مباد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روزگاریست ای دل آشفته کار</p></div>
<div class="m2"><p>تا به غفلت می‌گذاری روزگار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عمر در بی‌حاصلی بردی به سر</p></div>
<div class="m2"><p>کو کنون تحصیل را عمری دگر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خیز و این وادی مشکل قطع کن</p></div>
<div class="m2"><p>بازپر، وز جان وز دل قطع کن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زانک تا با جان و بادل هم بری</p></div>
<div class="m2"><p>مشرکی وز مشرکان غافل‌تری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جان برافشان در ره و دل کن نثار</p></div>
<div class="m2"><p>ورنه ز استغنی بگردانند کار</p></div></div>