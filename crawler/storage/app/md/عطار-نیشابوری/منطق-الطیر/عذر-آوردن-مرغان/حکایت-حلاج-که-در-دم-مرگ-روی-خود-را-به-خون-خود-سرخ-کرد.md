---
title: >-
    حکایت حلاج که در دم مرگ روی خود را به خون خود سرخ کرد
---
# حکایت حلاج که در دم مرگ روی خود را به خون خود سرخ کرد

<div class="b" id="bn1"><div class="m1"><p>چون شد آن حلاج بر دار آن زمان</p></div>
<div class="m2"><p>جز انا الحق می‌نرفتش بر زبان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون زبان او همی‌نشناختند</p></div>
<div class="m2"><p>چار دست و پای او انداختند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زرد شد خون بریخت از وی بسی</p></div>
<div class="m2"><p>سرخ کی ماند درین حالت کسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زود درمالید آن خورشید و ماه</p></div>
<div class="m2"><p>دست بریده به روی هم چو ماه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت چون گلگونهٔ مردست خون</p></div>
<div class="m2"><p>روی خود گلگونه بر کردم کنون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا نباشم زرد در چشم کسی</p></div>
<div class="m2"><p>سرخ رویی باشدم اینجا بسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکه را من زرد آیم در نظر</p></div>
<div class="m2"><p>ظن برد کاینجا بترسیدم مگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون مرا از ترس یک سر موی نیست</p></div>
<div class="m2"><p>جز چنین گلگونه اینجا روی نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرد خونی چون نهد سر سوی دار</p></div>
<div class="m2"><p>شیرمردیش آن زمان آید به کار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون جهانم حلقهٔ میمی بود</p></div>
<div class="m2"><p>کی چنین جایی مرا بیمی بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر که را با اژدهای هفت سر</p></div>
<div class="m2"><p>در تموز افتاده دایم خورد و خور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زین چنین بازیش بسیار اوفتد</p></div>
<div class="m2"><p>کمترین چیزیش سر دار اوفتد</p></div></div>