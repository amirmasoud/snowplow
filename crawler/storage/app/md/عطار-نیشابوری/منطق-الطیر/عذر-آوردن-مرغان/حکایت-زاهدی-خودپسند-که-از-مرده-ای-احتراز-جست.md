---
title: >-
    حکایت زاهدی خودپسند که از مرده‌ای احتراز جست
---
# حکایت زاهدی خودپسند که از مرده‌ای احتراز جست

<div class="b" id="bn1"><div class="m1"><p>چون بمرد آن مرد مفسد در گناه</p></div>
<div class="m2"><p>گفت می‌بردند تابوتش به راه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بدید آن زاهدی، کرد احتراز</p></div>
<div class="m2"><p>تا نباید کرد بر مفسد نماز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در شب آن زاهد مگر دیدش به خواب</p></div>
<div class="m2"><p>در بهشت و روی همچون آفتاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرد زاهد گفتش آخر ای غلام</p></div>
<div class="m2"><p>از کجا آوردی این عالی مقام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در گنه بودی تو تا بودی همه</p></div>
<div class="m2"><p>پای تا فرقت بیالودی همه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت از بی‌رحمی تو کردگار</p></div>
<div class="m2"><p>کرد رحمت بر من آشفته‌کار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق بازی بین که حکمت می‌کند</p></div>
<div class="m2"><p>می‌کند این کار و رحمت می‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حکمت او در شبی چون پر زاغ</p></div>
<div class="m2"><p>کودکی را می‌فرستد با چراغ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بعد از آن بادی فرستد تیزرو</p></div>
<div class="m2"><p>کان چراغ او بکش، برخیز و رو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پس بگیرد طفل را در ره گذر</p></div>
<div class="m2"><p>کز چه کشتی آن چراغ ای بی‌خبر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زان بگیرد طفل را تا در حساب</p></div>
<div class="m2"><p>می‌کند با او به صد شفقت عتاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر همه کس جز نمازی نیستی</p></div>
<div class="m2"><p>حکمتش را عشق بازی نیستی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کار حکمت جز چنین نبود تمام</p></div>
<div class="m2"><p>لاجرم خوداین چنین آمد مدام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در ره او صد هزاران حکمتست</p></div>
<div class="m2"><p>قطرهٔ راحصه بحری رحمتست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>روز و شب این هفت پرگار ای پسر</p></div>
<div class="m2"><p>از برای تست در کار ای پسر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>طاعت روحانیون از بهر تست</p></div>
<div class="m2"><p>خلد و دوزخ عکس لطف و قهرتست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قدسیان جمله سجودت کرده‌اند</p></div>
<div class="m2"><p>جزو و کل غرق وجودت کرده‌اند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از حقارت سوی خود منگر بسی</p></div>
<div class="m2"><p>زانک ممکن نیست بیش از تو کسی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جسم تو جزوست و جانت کل کل</p></div>
<div class="m2"><p>خویش را عاجز مکن در عین ذل</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کل تو درتافت جزوت شد پدید</p></div>
<div class="m2"><p>جان تو بشتافت عضوت شد پدید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نیست تن از جان جدا ، جزوی ازوست</p></div>
<div class="m2"><p>نیست جان از کل جدا، عضوی ازوست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون عدد نبود درین راه واحد</p></div>
<div class="m2"><p>جزو و کل گفتی نابشد تاابد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صد هزاران ابر رحمت فوق تو</p></div>
<div class="m2"><p>می‌ببارد تافزاید شوق تو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون درآید وقت رفعتهای کل</p></div>
<div class="m2"><p>از برای تست خلعتهای کل</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هرچ چندانی ملایک کرده‌اند</p></div>
<div class="m2"><p>از پی تو بر فذلک کرده‌اند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جملهٔ طاعات ایشان، کردگار</p></div>
<div class="m2"><p>بر تو خواهد کرد جاویدان نثار</p></div></div>