---
title: >-
    حکایت چوب خوردن یوسف به دستور زلیخا
---
# حکایت چوب خوردن یوسف به دستور زلیخا

<div class="b" id="bn1"><div class="m1"><p>چون زلیخا حشمت واعزاز داشت</p></div>
<div class="m2"><p>رفت یوسف را به زندان بازداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با غلامی گفت بنشان این دمش</p></div>
<div class="m2"><p>پس بزن پنجاه چوب محکمش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر تن یوسف چنان بازو گشای</p></div>
<div class="m2"><p>کین دم آهش بشنوم از دور جای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن غلام آمد بسی کارش نداد</p></div>
<div class="m2"><p>روی یوسف دید دل بارش نداد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پوستینی دید مرد نیک بخت</p></div>
<div class="m2"><p>دست خود بر پوستین بگشاد سخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرد هر چوبی که می‌زد استوار</p></div>
<div class="m2"><p>ناله‌ای می‌کرد یوسف زار زار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون زلیخا بانگ بشنودی ز دور</p></div>
<div class="m2"><p>گفتی آخر سخت‌تر زن ای صبور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرد گفت ای یوسف خورشید فر</p></div>
<div class="m2"><p>گر زلیخا بر تو اندازد نظر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون نبیند بر تو زخم چوب هیچ</p></div>
<div class="m2"><p>بی شک اندازد مرا در پیچ پیچ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برهنه کن دوش، دل برجای دار</p></div>
<div class="m2"><p>بعد از آن چوبی قوی را پای دار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرچه این ضربت زیانی باشدت</p></div>
<div class="m2"><p>چون ترا بیند نشانی باشدت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تن برهنه کرد یوسف آن زمان</p></div>
<div class="m2"><p>غلغلی افتاد در هفت آسمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرد حالی کرد دست خود بلند</p></div>
<div class="m2"><p>سخت چوبی زد که در خاکش فکند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون زلیخا زو شنود آن بار آه</p></div>
<div class="m2"><p>گفت بس، کین آه بود از جایگاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پیش ازین آن آهها ناچیزبود</p></div>
<div class="m2"><p>آه آن باد این ز جایی نیز بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر بود در ماتمی صد نوحه‌گر</p></div>
<div class="m2"><p>آه صاحب درد آید کارگر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر بود در حلقه‌ای صد غم زده</p></div>
<div class="m2"><p>حلقه را باشد نگین ماتم زده</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا نگردی مرد صاحب درد تو</p></div>
<div class="m2"><p>در صف مردان نباشی مرد تو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر که درد عشق دارد، سوز هم</p></div>
<div class="m2"><p>شب کجا یابد قرار و روز هم</p></div></div>