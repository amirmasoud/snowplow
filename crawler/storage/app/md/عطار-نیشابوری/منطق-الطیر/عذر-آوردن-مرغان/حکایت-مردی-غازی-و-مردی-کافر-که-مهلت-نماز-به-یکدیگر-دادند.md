---
title: >-
    حکایت مردی غازی و مردی کافر که مهلت نماز به یکدیگر دادند
---
# حکایت مردی غازی و مردی کافر که مهلت نماز به یکدیگر دادند

<div class="b" id="bn1"><div class="m1"><p>غازیی از کافری بس سرفراز</p></div>
<div class="m2"><p>خواست مهلت تا که بگزارد نماز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بشد غازی نماز خویش کرد</p></div>
<div class="m2"><p>بازآمد جنگ هر دم بیش کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود کافر را نمازی زان خویش</p></div>
<div class="m2"><p>مهل خواست او نیز بیرون شد ز پیش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوشه‌ای بگزید کافر پاک‌تر</p></div>
<div class="m2"><p>پس نهاد او سوی بت بر خاک سر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غازیش چون دید سر بر خاک راه</p></div>
<div class="m2"><p>گفت نصرت یافتم این جایگاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواست تا تیغی زند بر وی نهان</p></div>
<div class="m2"><p>هاتفیش آواز داد از آسمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کای همه بد عهدی از سر تا بپای</p></div>
<div class="m2"><p>خوش وفا و عهد می‌آری بجای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>او نزد تیغت چو اول داد مهل</p></div>
<div class="m2"><p>تو اگر تیغش زنی جهل است جهل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای و او فو العهد برنا خوانده</p></div>
<div class="m2"><p>گشته کژ، بر عهد خودنا مانده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون نکویی کرد کافر پیش ازین</p></div>
<div class="m2"><p>ناجوامردی مکن تو بیش ازین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>او نکویی کرد و تو بد می‌کنی</p></div>
<div class="m2"><p>با کسان آن کن که با خود می کنی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بودت از کافر وفا و ایمنی</p></div>
<div class="m2"><p>کو وفاداری ترا، گرمؤمنی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای مسلمان، نامسلم آمدی</p></div>
<div class="m2"><p>در وفا از کافری کم آمدی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رتف غازی زین سخن از جای خویش</p></div>
<div class="m2"><p>در عرق گم دید سر تا پای خویش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کافرش چون دید گریان مانده</p></div>
<div class="m2"><p>تیغش اندر دست، حیران مانده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفت گریان از چه‌ای بر گفت راست</p></div>
<div class="m2"><p>کین زمان کردند از من بازخواست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بی‌وفا گفتند از بهر توم</p></div>
<div class="m2"><p>این چنین گریان من از قهر توم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون شنید این قصه کافر آشکار</p></div>
<div class="m2"><p>نعره‌ای زد بعد از آن بگریست زار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گفت جباری که با محبوب خویش</p></div>
<div class="m2"><p>از برای دشمن معیوب خویش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از وفاداری کند چندین عتاب</p></div>
<div class="m2"><p>چون کنم من بی‌وفایی بی‌حساب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عرضه کن اسلام تا دین آورم</p></div>
<div class="m2"><p>شرک سوزم، شرع آیین آورم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای دریغا بر دلم بندی چنین</p></div>
<div class="m2"><p>بی‌خبر من از خداوندی چنین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بس که با مطلوب خود ای بی‌طلب</p></div>
<div class="m2"><p>بی‌وفایی کرده‌ای تو بی‌ادب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>لیک صبرم هست تا طاس فلک</p></div>
<div class="m2"><p>جمله در رویت بگوید یک به یک</p></div></div>