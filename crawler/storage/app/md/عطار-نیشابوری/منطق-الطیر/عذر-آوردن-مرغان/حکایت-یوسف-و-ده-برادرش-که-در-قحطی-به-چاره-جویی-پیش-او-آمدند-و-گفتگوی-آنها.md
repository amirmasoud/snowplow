---
title: >-
    حکایت یوسف و ده برادرش که در قحطی به چاره جویی پیش او آمدند و گفتگوی آنها
---
# حکایت یوسف و ده برادرش که در قحطی به چاره جویی پیش او آمدند و گفتگوی آنها

<div class="b" id="bn1"><div class="m1"><p>ده برادر قحطشان کرده نفور</p></div>
<div class="m2"><p>پیش یوسف آمدند از راه دور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سر بی‌چارگی گفتند حال</p></div>
<div class="m2"><p>چاره‌ای می‌خواستند از تنگ حال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی یوسف بود در برقع نهان</p></div>
<div class="m2"><p>پیش یوسف بود طاسی آن زمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست زد بر طاس یوسف آشکار</p></div>
<div class="m2"><p>طاسش اندر ناله آمد زار زار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت حالی یوسف حکمت شناس</p></div>
<div class="m2"><p>هیچ می‌دانید کین آواز طاس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ده برادر برگشادند آن زمان</p></div>
<div class="m2"><p>پیش یوسف از سر عجزی زفان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جمله گفتند ای عزیر حق شناس</p></div>
<div class="m2"><p>کس چه داند بانگ آید ز طاس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یوسف آنگه گفت من دانم درست</p></div>
<div class="m2"><p>کو چه گوید با شما ای جمله سست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت می‌گوید شما را پیش ازین</p></div>
<div class="m2"><p>یک برادر بود حسنش بیش ازین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نام یوسف داشت، که بود از شما</p></div>
<div class="m2"><p>در نکویی گوی بر بود از شما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دست زد بر طاس از سر باز در</p></div>
<div class="m2"><p>گفت برگوید بدین آواز در</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جمله افکندید یوسف را به چاه</p></div>
<div class="m2"><p>پس بیاوردید گرگی بی‌گناه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پیرهن در خون کشیدید از فسون</p></div>
<div class="m2"><p>تا دل یعقوب از آن خون گشت خون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دست زد بر طاس یک باری دگر</p></div>
<div class="m2"><p>طاس را آورد در کاری دگر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت می‌گوید پدر را سوختید</p></div>
<div class="m2"><p>یوسف مه روی را بفروختید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>با برادر کی کنند این ، کافران</p></div>
<div class="m2"><p>شرم تان باد از خدا ای حاضران</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زان سخن آن قوم حیران آمده</p></div>
<div class="m2"><p>آب گشتند، از پی نان آمده</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گرچه یوسف را چنان بفروختند</p></div>
<div class="m2"><p>برخود آن ساعت جهان بفروختند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون به چاه افکندنش کردند ساز</p></div>
<div class="m2"><p>جمله در چاه بلا ماندند باز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کور چشمی باشد آن کین قصه او</p></div>
<div class="m2"><p>بشنود زین برنگیرد حصه او</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو مکن چندین در آن قصه نظر</p></div>
<div class="m2"><p>قصهٔ تست این همه، ای بی خبر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آنچ تو از بی‌وفایی کرده‌ای</p></div>
<div class="m2"><p>نی به نور آشنایی کرده‌ای</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر کسی عمری زند بر طاس دست</p></div>
<div class="m2"><p>کار ناشایست تو زان بیش هست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>باش تا از خواب بیدارت کنند</p></div>
<div class="m2"><p>در نهاد خود گرفتارت کنند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>باش تا فردا جفاهای ترا</p></div>
<div class="m2"><p>کافریهای و خطاهای ترا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پیش رویت عرضه دارند آن همه</p></div>
<div class="m2"><p>یک به یک برتو شمارند آن همه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون بسی آواز طاس آید به گوش</p></div>
<div class="m2"><p>می‌ندانم تا بماند عقل و هوش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای چو موری لنگ در کار آمده</p></div>
<div class="m2"><p>در بن طاسی گرفتارآمده</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چند گرد طاس گردی سرنگون</p></div>
<div class="m2"><p>در گذر کین هست طشت غرق خون</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در میان طاس مانی مبتلا</p></div>
<div class="m2"><p>هر دم آوازی دگر آید ترا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پر برآر و درگذرای حق شناس</p></div>
<div class="m2"><p>ورنه رسوا گردی از آوازطاس</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دیگری پرسید ازو کای پیشوا</p></div>
<div class="m2"><p>هست گستاخی در آن حضرت روا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر کسی گستاخیی یابد عظیم</p></div>
<div class="m2"><p>بعد از آنش از پی درآید هیچ بیم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چون بود گستاخی آنجا، بازگوی</p></div>
<div class="m2"><p>در معنی برفشان و رازگوی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گفت هر کس را که اهلیت بود</p></div>
<div class="m2"><p>محرم سر الوهیت بود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گر کند گستاخیی او را رواست</p></div>
<div class="m2"><p>زانک دایم رازدار پادشاست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>لیک مردی رازدان و رازدار</p></div>
<div class="m2"><p>کی کند گستاخیی گستاخ‌وار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چون ز چپ باشد ادب حرمت زراست</p></div>
<div class="m2"><p>یک نفس گستاخیی از وی رواست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مرد اشتروان که باشد برکنار</p></div>
<div class="m2"><p>کی تواند بود شه را رازدار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گر کند گستاخیی چون اهل راز</p></div>
<div class="m2"><p>ماند از ایمان وز جان نیز باز</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کی تواند داشت رندی در سپاه</p></div>
<div class="m2"><p>زهرهٔ گستاخیی در پیش شاه</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گر به راه آید وشاق اعجمی</p></div>
<div class="m2"><p>هست گستاخی او از خرمی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>جمله رب داند نه رب داند نه رب</p></div>
<div class="m2"><p>گر کند گستاخیی از فرط حب</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>او چه دیوانه بود از شور عشق</p></div>
<div class="m2"><p>می‌رود بر روی آب از زور عشق</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>خوش بود گستاخی او، خوش بود</p></div>
<div class="m2"><p>زانک آن دیوانه چون آتش بود</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>در ره آتش سلامت کی بود</p></div>
<div class="m2"><p>مرد مجنون را ملامت کی بود</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چون ترا دیوانگی آید پدید</p></div>
<div class="m2"><p>هرچ تو گویی ز تو بتوان شنید</p></div></div>