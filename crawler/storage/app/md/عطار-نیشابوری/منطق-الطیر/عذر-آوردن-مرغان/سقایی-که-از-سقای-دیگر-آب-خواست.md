---
title: >-
    سقایی که از سقای دیگر آب خواست
---
# سقایی که از سقای دیگر آب خواست

<div class="b" id="bn1"><div class="m1"><p>می‌شد آن سقا مگر آبی به کف</p></div>
<div class="m2"><p>دید سقایی دگر در پیش صف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حالی این یک آب در کف آن زمان</p></div>
<div class="m2"><p>پیش آن یک رفت و آبی خواست از آن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرد گفتش ای ز معنی بی‌خبر</p></div>
<div class="m2"><p>چون تو هم این آب داری خوش بخور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت هین آبی ده‌ای بخرد مرا</p></div>
<div class="m2"><p>زانکه دل بگرفت از آن خود مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود آدم را دلی از کهنه سیر</p></div>
<div class="m2"><p>از برای نو به گندم شد دلیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کهنها جمله به یک گندم فروخت</p></div>
<div class="m2"><p>هرچ بودش جمله در گندم بسوخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عور شد، دردی ز دل سر بر زدش</p></div>
<div class="m2"><p>عشق آمد حلقه‌ای بر در زدش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در فروغ عشق چون ناچیز شد</p></div>
<div class="m2"><p>کهنه و نو رفت واو هم نیزشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون نماندش هیچ، با هیچی بساخت</p></div>
<div class="m2"><p>هرچ دستش داد در هیچی به باخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل ز خود بگرفتن و مردن بسی</p></div>
<div class="m2"><p>نیست کار ما و کار هر کسی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دیگری گفتش که پندارم که من</p></div>
<div class="m2"><p>کرده‌ام حاصل کمال خویشتن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هم کمال خویش حاصل کرده‌ام</p></div>
<div class="m2"><p>هم ریاضتهای مشکل کرده‌ام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون هم اینجا کار من حاصل ببود</p></div>
<div class="m2"><p>رفتنم زین جایگه مشکل ببود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دیدهٔ کس را که برخیزد ز گنج</p></div>
<div class="m2"><p>می‌دود در کوه و در صحرا به رنج</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت ای ابلیس طبع پر غرور</p></div>
<div class="m2"><p>در منی گم وز مراد من نفور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در خیال خویش مغرور آمده</p></div>
<div class="m2"><p>از فضای معرفت دورآمده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نفس بر جان تو دستی یافته</p></div>
<div class="m2"><p>دیو در مغزت نشستی یافته</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر ترا نوریست در ره یارتست</p></div>
<div class="m2"><p>ور ترا ذوقیست آن پندار تست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وجد و فقر تو خیالی بیش نیست</p></div>
<div class="m2"><p>هرچ می‌گویی محالی بیش نیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>غره این روشنی ره مباش</p></div>
<div class="m2"><p>نفس تو باتست، جز آگه مباش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>با چنین خصمی ز بی تیغی به دست</p></div>
<div class="m2"><p>کی تواند هیچ کس ایمن نشست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر ترا نوری ز نفس آمد پدید</p></div>
<div class="m2"><p>زخم کژدم از کرفس آمد پدید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو بدان نور نجس غره مباش</p></div>
<div class="m2"><p>چون نه‌ای خورشید جز ذره مباش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نه ز تاریکی ره نومید شو</p></div>
<div class="m2"><p>نه ز نورش هم بر خورشید شو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا تو پندار خویشی ای عزیز</p></div>
<div class="m2"><p>خواندن و راندن نه ارزد یک پشیز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون برون آیی ز پندار وجود</p></div>
<div class="m2"><p>بر تو گردد دور پرگار وجود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ور ترا پندار هستی هست هیچ</p></div>
<div class="m2"><p>نبودت از نیستی در دست هیچ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ذره‌ای گر طعم هستی با شدت</p></div>
<div class="m2"><p>کافری و بت پرستی با شدت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر پدید آیی به هستی یک نفس</p></div>
<div class="m2"><p>تیر باران آیدت از پیش و پس</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا تو هستی، رنج جان را تن بنه</p></div>
<div class="m2"><p>صد قفا را هر زمان گردن بنه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر تو آیی خود به هستی آشکار</p></div>
<div class="m2"><p>صد قفات از پی در آرد روزگار</p></div></div>