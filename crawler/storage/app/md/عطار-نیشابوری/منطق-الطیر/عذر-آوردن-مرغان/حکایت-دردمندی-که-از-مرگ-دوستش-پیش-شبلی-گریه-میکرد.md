---
title: >-
    حکایت دردمندی که از مرگ دوستش پیش شبلی گریه میکرد
---
# حکایت دردمندی که از مرگ دوستش پیش شبلی گریه میکرد

<div class="b" id="bn1"><div class="m1"><p>دردمندی پیش شبلی می‌گریست</p></div>
<div class="m2"><p>شیخ پرسیدش که این گریه ز چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت شیخا دوستی بود آن من</p></div>
<div class="m2"><p>از جمالش تازه بودی جان من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دی بمرد و من بمردم از غمش</p></div>
<div class="m2"><p>شد جهان بر من سیاه از ماتمش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شیخ گفتا چون دلت بی‌خویش ازینست</p></div>
<div class="m2"><p>این چه غم باشد، سزایت بیش از ینست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوستی دیگر گزین ای یار تو</p></div>
<div class="m2"><p>کو نمیرد تا نمیری زار تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوستی کز مرگ نقصان آورد</p></div>
<div class="m2"><p>دوستی او غم جان آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرک شد در عشق صورت مبتلا</p></div>
<div class="m2"><p>هم از آن صورت فتد در صد بلا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زودش آن صورت شود بیرون ز دست</p></div>
<div class="m2"><p>و او از آن حیرت کند در خون نشست</p></div></div>