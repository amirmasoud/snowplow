---
title: >-
    حکایت گور کنی که عمر دراز یافت
---
# حکایت گور کنی که عمر دراز یافت

<div class="b" id="bn1"><div class="m1"><p>یافت مردی گورکن عمری دراز</p></div>
<div class="m2"><p>سایلی گفتش که چیزی گوی باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چو عمری گور کندی در مغاک</p></div>
<div class="m2"><p>چه عجایب دیده‌ای در زیر خاک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت این دیدم عجایب حسب حال</p></div>
<div class="m2"><p>کین سگ نفسم همی هفتاد سال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گور کندن دید و یک ساعت نمرد</p></div>
<div class="m2"><p>یک دمم فرمان یک طاعت نبرد</p></div></div>