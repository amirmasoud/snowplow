---
title: >-
    گفتهٔ بوعلی رودبار در وقت مرگ
---
# گفتهٔ بوعلی رودبار در وقت مرگ

<div class="b" id="bn1"><div class="m1"><p>وقت مردن بوعلی رودبار</p></div>
<div class="m2"><p>گفت جانم بر لب آمد ز انتظار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسمان را در همه بگشاده‌اند</p></div>
<div class="m2"><p>در بهشتم مسندی بنهاده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو بلبل قدسیان خوش سرای</p></div>
<div class="m2"><p>بانگ می‌دارند کای عاشق درآی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکر می‌کن پس به شادی می‌خرام</p></div>
<div class="m2"><p>زانک هرگز کس ندیدست این مقام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه این انعام و این توفیق هست</p></div>
<div class="m2"><p>می‌ندارد جانم از تحقیق دست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زانک می‌گوید ترا با این چه کار</p></div>
<div class="m2"><p>داده‌ای عمری درازم انتظار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست برگم تا چو اهل شهوتی</p></div>
<div class="m2"><p>سر فرو آرم به اندک رشوتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق تو با جان من در هم سرشت</p></div>
<div class="m2"><p>من نه دوزخ دانم اینجا نه بهشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر بسوزی همچو خاکستر مرا</p></div>
<div class="m2"><p>در نیابد جز تو کس دیگر مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من ترادانم، نه دین، نه کافری</p></div>
<div class="m2"><p>نگذرم من زین، اگر تو بگذری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من ترا خواهم، ترا دانم، ترا</p></div>
<div class="m2"><p>هم تو جانم را و هم جانم ترا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حاجت من در همه عالم تویی</p></div>
<div class="m2"><p>این جهانم و آن جهانم هم تویی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حاجت این دل شده، مویی برآر</p></div>
<div class="m2"><p>یک نفس با من به هم هویی برآر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جان من گر سرکشد مویی ز تو</p></div>
<div class="m2"><p>جان ببر، هایی ز من هویی ز تو</p></div></div>