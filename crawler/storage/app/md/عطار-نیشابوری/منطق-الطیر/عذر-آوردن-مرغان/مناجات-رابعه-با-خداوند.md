---
title: >-
    مناجات رابعه با خداوند
---
# مناجات رابعه با خداوند

<div class="b" id="bn1"><div class="m1"><p>رابعه گفتی که ای دانای راز</p></div>
<div class="m2"><p>دشمنان را کار دنیا می‌بساز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوستان را آخرت ده بردوام</p></div>
<div class="m2"><p>زانک من زین کار آزادم مدام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر ز دنیا و آخرت مفلس شوم</p></div>
<div class="m2"><p>کم غمم گر یک دمت مونس شوم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس بود این مفلسی از تو مرا</p></div>
<div class="m2"><p>زانک دایم تو بسی از تو مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بسوی هر دو عالم بنگرم</p></div>
<div class="m2"><p>یا به جز تو هیچ خواهم، کافرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکرا او هست، کل او را بود</p></div>
<div class="m2"><p>هفت دریا زیر پل او را بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرچ بود و هست و خواهد بود نیز</p></div>
<div class="m2"><p>مثل دارد، جز خداوند عزیز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرچ را جویی جزو یابی نظیر</p></div>
<div class="m2"><p>اوست دایم بی‌نظیر وناگزیر</p></div></div>