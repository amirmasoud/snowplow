---
title: >-
    حکایت ابلهی که در آب افتاد و ریش بزرگش وبال او بود
---
# حکایت ابلهی که در آب افتاد و ریش بزرگش وبال او بود

<div class="b" id="bn1"><div class="m1"><p>داشت ریشی بس بزرگ آن ابلهی</p></div>
<div class="m2"><p>غرقه شد در آب دریا ناگهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیدش از خشکی مگر مردی سره</p></div>
<div class="m2"><p>گفت از سر برفکن آن تو بره</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت نیست آن تو به ره، ریش منست</p></div>
<div class="m2"><p>نیست خود این ریش، تشویش منست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت احسنت اینت ریش و اینت کار</p></div>
<div class="m2"><p>تو فروده اینت خواهد کشت زار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای چو بز از ریش خود شرمیت نه</p></div>
<div class="m2"><p>برگرفته ریش و آزرمیت نه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا ترا نفسی و شیطانی بود</p></div>
<div class="m2"><p>در تو فرعونی و هامانی بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پشم درکش همچو موسی کون را</p></div>
<div class="m2"><p>ریش گیر آنگاه این فرعون را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ریش این فرعون گیر و سخت دار</p></div>
<div class="m2"><p>جنگ ریشاریش کن مردانه‌وار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پای درنه، ترک ریش خویش گیر</p></div>
<div class="m2"><p>تا کیت زین ریش، ره در پیش گیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرچه از ریشت به جز تشویش نیست</p></div>
<div class="m2"><p>یک دمت پروای ریش خویش نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در ره دین آن بود فرزانه‌ای</p></div>
<div class="m2"><p>کو ندارد ریش خود را شانه‌ای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خویش را از ریش خود آگه کند</p></div>
<div class="m2"><p>ریش را دستار خوان ره کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه به جز خونابه آبی یابد او</p></div>
<div class="m2"><p>نه به جز از دل کبابی یابد او</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر بود گازر، نبیند آفتاب</p></div>
<div class="m2"><p>ور بود دهقان، نیارد میغ آب</p></div></div>