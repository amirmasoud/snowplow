---
title: >-
    عقیدهٔ مردی پاک‌دین دربارهٔ مبتدی
---
# عقیدهٔ مردی پاک‌دین دربارهٔ مبتدی

<div class="b" id="bn1"><div class="m1"><p>پاک دینی گفت آن نیکوترست</p></div>
<div class="m2"><p>مبتدی را کو به تاریکی درست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا به کلی گم شود در بحر جود</p></div>
<div class="m2"><p>پس نماند هیچ رشدش در وجود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زانک چیزی گر برو ظاهر شود</p></div>
<div class="m2"><p>غره گردد وان زمان کافر شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنچ در تست از حسد و از خشم تو</p></div>
<div class="m2"><p>چشم مردان بیند اونه چشم تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هست در تو گلخنی پر اژدها</p></div>
<div class="m2"><p>تو ز غفلت کرده ایشان را رها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روز و شب در پرورش‌شان مانده</p></div>
<div class="m2"><p>فتنهٔ خفت و خورش‌شان مانده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اصل تو از خاک وز خون شد تمام</p></div>
<div class="m2"><p>وی عجب هر دو ز بی‌قدری حرام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خون که او نزدیک‌تر آمد به تو</p></div>
<div class="m2"><p>هم نجس هم مختصر آمد به تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرچ در بعد دلست از قرب حس</p></div>
<div class="m2"><p>هم حرام افتد بلا شک هم نجس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر پلیدیی درون می‌بینیی</p></div>
<div class="m2"><p>این چنین فارغ کجا بنشینیی</p></div></div>