---
title: >-
    حکایت عاشقی که عیب چشم یار را پس از نقصان عشق دید
---
# حکایت عاشقی که عیب چشم یار را پس از نقصان عشق دید

<div class="b" id="bn1"><div class="m1"><p>بود مردی شیردل خصم افکنی</p></div>
<div class="m2"><p>گشت عاشق پنج سال او بر زنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داشت بر چشم آن زن همچون نگار</p></div>
<div class="m2"><p>یک سر ناخن سپیدی آشکار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان سپیدی مرد بودش بی‌خبر</p></div>
<div class="m2"><p>گرچه بسیاری برافکندی نظر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرد عاشق چون بود در عشق زار</p></div>
<div class="m2"><p>کی خبر یابد ز عیب چشم یار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بعد از آن کم گشت عشق آن مرا را</p></div>
<div class="m2"><p>دارویی آمد پدید آن درد را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق آن زن در دلش نقصان گرفت</p></div>
<div class="m2"><p>کار او برخویشتن آسان گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پس بدید آن مرد عیب چشم یار</p></div>
<div class="m2"><p>این سپیدی گفت کی شد آشکار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت آن ساعت که شد عشق تو کم</p></div>
<div class="m2"><p>چشم من عیب آن زمان آورد هم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون ترا در عشق نقصان شد پدید</p></div>
<div class="m2"><p>عیب در چشمم چنین زان شد پدید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کرده‌ای از وسوسه پر شور دل</p></div>
<div class="m2"><p>هم ببین یک عیب خود ای کور دل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چند جویی دیگران را عیب باز</p></div>
<div class="m2"><p>آن خود یک ره بجوی از جیب باز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا چو بر تو عیب تو آید گران</p></div>
<div class="m2"><p>نبودت پروای عیب دیگران</p></div></div>