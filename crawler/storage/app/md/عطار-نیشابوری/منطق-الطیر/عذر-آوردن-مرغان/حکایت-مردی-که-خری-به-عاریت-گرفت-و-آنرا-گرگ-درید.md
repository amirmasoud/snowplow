---
title: >-
    حکایت مردی که خری به عاریت گرفت و آنرا گرگ درید
---
# حکایت مردی که خری به عاریت گرفت و آنرا گرگ درید

<div class="b" id="bn1"><div class="m1"><p>بود در کاریز بی‌سرمایه‌ای</p></div>
<div class="m2"><p>عاریت بستد خر از همسایه‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفت سوی آسیا و خوش بخفت</p></div>
<div class="m2"><p>چون بخفت آن مرد حالی خر برفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرگ آن خر را بدرید و بخورد</p></div>
<div class="m2"><p>روز دیگر بود تاوان خواست مرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر دو تن می‌آمدند از ره دوان</p></div>
<div class="m2"><p>تا بنزد میر کاریز آن زمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قصه پیش میر برگفتند راست</p></div>
<div class="m2"><p>زو بپرسیدند کین تاوان کراست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میر گفتا هرک گرگ یک تنه</p></div>
<div class="m2"><p>سردهد در دشت صحرا گرسنه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی شک این تاوان برو باشد درست</p></div>
<div class="m2"><p>هردو را تاوان ازو بایست جست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با رب این تاوان چه نیکو می‌کند</p></div>
<div class="m2"><p>هیچ تاوان نیست هرچ او می‌کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر زنان مصر چون حالت بگشت</p></div>
<div class="m2"><p>زانک مخلوقی به دیشان برگذشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه عجب باشد که بر دیوانه‌ای</p></div>
<div class="m2"><p>حالتی تابد ز دولت خانه‌ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا در آن حالت شود بی‌خویش او</p></div>
<div class="m2"><p>ننگرد هیچ از پس و از پیش او</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جمله زو گوید، بدو گوید همه</p></div>
<div class="m2"><p>جمله زو جوید، بدو جوید همه</p></div></div>