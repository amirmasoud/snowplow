---
title: >-
    حکایت مفلسی که عاشق شاه مصر شد
---
# حکایت مفلسی که عاشق شاه مصر شد

<div class="b" id="bn1"><div class="m1"><p>بود اندر مصر شاهی نامدار</p></div>
<div class="m2"><p>مفلسی بر شاه عاشق گشت زار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون خبر آمد ز عشقش شاه را</p></div>
<div class="m2"><p>خواند حالی عاشق گم‌راه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت چون عاشق شدی بر شهریار</p></div>
<div class="m2"><p>از دو کار اکنون یکی کن اختیار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا به ترک شهر، وین کشور بگوی</p></div>
<div class="m2"><p>یا نه، در عشقم به ترک سر بگوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با تو گفتم کار تو یک بارگی</p></div>
<div class="m2"><p>سر بریدن خواهی یا آوارگی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون نبود آن مرد عاشق مرد کار</p></div>
<div class="m2"><p>کرد او را شهر رفتن اختیار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون برفت آن مفلس بی‌خویشتن</p></div>
<div class="m2"><p>شاه گفتا سر ببریدش ز تن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حاجبی گفتا که هست او بی‌گناه</p></div>
<div class="m2"><p>ازچه سربریدنش فرمود شاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاه گفتا زانک او عاشق نبود</p></div>
<div class="m2"><p>در طریق عشق من صادق نبود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر چنان بودی که بودی مرد کار</p></div>
<div class="m2"><p>سربریدن کردی اینجا اختیار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرک سر بر وی به از جانان بود</p></div>
<div class="m2"><p>عشق ورزیدن برو تاوان بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر ز من او سربریدن خواستی</p></div>
<div class="m2"><p>شهریار از مملکت برخاستی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر میان بستی کمر در پیش او</p></div>
<div class="m2"><p>خسرو عالم شدی درویش او</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>لیک چون در عشق دعوی دار بود</p></div>
<div class="m2"><p>سربریدن سازدش نهمار زود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هرکه در هجرم سر سر دارد او</p></div>
<div class="m2"><p>مدعیست دامن‌تر دارد او</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>این بدان گفتم که تا هر بی‌فروغ</p></div>
<div class="m2"><p>کم زند در عشق ما لاف دروغ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دیگری گفتش که نفسم دشمن است</p></div>
<div class="m2"><p>چون روم ره زانک هم ره رهزنست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نفس سگ هرگز نشد فرمان برم</p></div>
<div class="m2"><p>من ندانم تا ز دستش جان برم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آشنا شد گرگ در صحرا مرا</p></div>
<div class="m2"><p>و آشنا نیست این سگ رعنا مرا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در عجایب مانده‌ام زین بی‌وفا</p></div>
<div class="m2"><p>تا چرا می‌اوفتد در آشنا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفت ای سگ در جوالت کرده خوش</p></div>
<div class="m2"><p>هم چو خاکی پای مالت کرده خوش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نفس تو هم احول و هم اعورست</p></div>
<div class="m2"><p>هم سگ و هم کاهل و هم کافرست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر کسی بستایدت اما دروغ</p></div>
<div class="m2"><p>از دروغی نفس تو گیرد فروغ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نیست روی آن که این سگ به شود</p></div>
<div class="m2"><p>کز دروغی این چنین فربه شود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بود در اول همه بی‌حاصلی</p></div>
<div class="m2"><p>کودکی و بی‌دلی و غافلی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بود در اوسط همه بیگانگی</p></div>
<div class="m2"><p>وز جوانی شعبهٔ دیوانگی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بود در آخر که پیری بود کار</p></div>
<div class="m2"><p>جان خرف درمانده تن گشته نزار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>با چنین عمری به جهل آراسته</p></div>
<div class="m2"><p>کی شود این نفس سگ پیراسته</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون ز اول تا به آخر غافلیست</p></div>
<div class="m2"><p>حاصل ما لاجرم بی‌حاصلیست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بنده دارد در جهان این سگ بسی</p></div>
<div class="m2"><p>بندگی سگ کند آخر کسی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>با وجود نفس بودن ناخوش است</p></div>
<div class="m2"><p>زانک نفست دوزخی پر آتش است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گه به دوزخ در سعیر شهوتست</p></div>
<div class="m2"><p>گاه در وی زمهریر نخوتست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دوزخ الحق زان خوش است و دل پذیر</p></div>
<div class="m2"><p>کو دو مغزست آتش است و زمهریر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>صد هزاران دل بمرد از غم همی</p></div>
<div class="m2"><p>وین سگ کافر نمی‌میرد دمی</p></div></div>