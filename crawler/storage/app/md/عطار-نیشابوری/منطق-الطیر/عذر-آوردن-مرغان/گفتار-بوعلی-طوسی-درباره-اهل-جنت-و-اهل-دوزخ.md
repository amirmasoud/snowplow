---
title: >-
    گفتار بوعلی طوسی دربارهٔ اهل جنت و اهل دوزخ
---
# گفتار بوعلی طوسی دربارهٔ اهل جنت و اهل دوزخ

<div class="b" id="bn1"><div class="m1"><p>بوعلی طوسی که پیر عهد بود</p></div>
<div class="m2"><p>سالک وادی جد و جهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن چنان جا کو به ناز و عز رسید</p></div>
<div class="m2"><p>من ندانم هیچکس هرگز رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت فردا اهل دوزخ زار زار</p></div>
<div class="m2"><p>اهل جنت را بپرسند آشکار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کز خوشی جنت و ذوق وصال</p></div>
<div class="m2"><p>حال خود گویید با ما حسب حال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهل جنت جمله گویند این زمان</p></div>
<div class="m2"><p>خوشی فردوس برخاست از میان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زانک ما را در بهشت پر کمال</p></div>
<div class="m2"><p>روی بنمود آفتاب آن جمال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون جمال او به ما نزدیک شد</p></div>
<div class="m2"><p>هشت خلد از شرم آن تاریک شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در فروغ آن جمال جان فشان</p></div>
<div class="m2"><p>خلد را نه نام باشد نه نشان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون بگویند اهل جنت حال خویش</p></div>
<div class="m2"><p>اهل دوزخ در جواب آیند پیش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کای همه فارغ ز فردوس و جنان</p></div>
<div class="m2"><p>هرچ گفتید آنچنانست، آنچنان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زانک ما کاصحاب جای ناخوشیم</p></div>
<div class="m2"><p>از قدم تا فرق غرق آتشیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>روی چون بنمود ما را آشکار</p></div>
<div class="m2"><p>حسرت واماندگی از روی یار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون شدیم اگه که ما افتاده‌ایم</p></div>
<div class="m2"><p>وز چنان رویی جدا افتاده‌ایم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز آتش حسرت دل ناشاد ما</p></div>
<div class="m2"><p>آتش دوزخ ببرد از یاد ما</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر کجا کین آتش آید کارگر</p></div>
<div class="m2"><p>ز آتش دوزخ کجا ماند خبر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هرک را شد در رهش حسرت پدید</p></div>
<div class="m2"><p>کم تواند کرد از غیرت پدید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حسرت و آه و جراحت بایدت</p></div>
<div class="m2"><p>در جراحت ذوق و راحت بایدت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر درین منزل تو مجروح آمدی</p></div>
<div class="m2"><p>محرم خلوت گه روح آمدی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر تو مجروحی دم از عالم مزن</p></div>
<div class="m2"><p>داغ می‌نه بر جراحت، دم مزن</p></div></div>