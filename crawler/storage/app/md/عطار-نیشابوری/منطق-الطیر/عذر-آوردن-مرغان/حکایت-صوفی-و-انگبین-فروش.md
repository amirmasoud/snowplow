---
title: >-
    حکایت صوفی و انگبین فروش
---
# حکایت صوفی و انگبین فروش

<div class="b" id="bn1"><div class="m1"><p>صوفیی می‌رفت در بغداد زود</p></div>
<div class="m2"><p>در میان راه آوازی شنود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کان یکی گفت انگبین دارم بسی</p></div>
<div class="m2"><p>می‌فروشم سخت ارزان، کو کسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شیخ صوفی گفت ای مرد صبود</p></div>
<div class="m2"><p>می‌دهی هیچی به هیچی، گفت دور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو مگر دیوانه‌ای ای بوالهوس</p></div>
<div class="m2"><p>کس به هیچی کی دهد چیزی به کس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هاتفی گفتش که‌ای صوفی درآی</p></div>
<div class="m2"><p>یک دکان زینجا که هستی برترآی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا به هیچی ما همه چیزت دهیم</p></div>
<div class="m2"><p>ور دگر خواهی بسی نیزت دهیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هست رحمت آفتابی تافته</p></div>
<div class="m2"><p>جملهٔ ذرات را دریافته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رحمت او بین که با پیغامبری</p></div>
<div class="m2"><p>در عتاب آمد برای کافری</p></div></div>