---
title: >-
    حکایت خواجه‌ای که از غلامش خواست او را برای نماز بیدار کند
---
# حکایت خواجه‌ای که از غلامش خواست او را برای نماز بیدار کند

<div class="b" id="bn1"><div class="m1"><p>خواجه زنگی را غلامی چست بود</p></div>
<div class="m2"><p>دست پاک از کار دنیا شست بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جملهٔ شب آن غلام پاک باز</p></div>
<div class="m2"><p>تا به وقت صبح می‌کردی نماز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواجه گفتش ای غلام کارکن</p></div>
<div class="m2"><p>شب چو برخیزی مرا بیدار کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا وضو سازم کنم با تو نماز</p></div>
<div class="m2"><p>آن غلام او را جوابی داد باز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت آن زن را که درد زه بخاست</p></div>
<div class="m2"><p>گر کسش بیدارگر نبود رواست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ترا دردیستی بیداریی</p></div>
<div class="m2"><p>روز و شب در کار نه بی‌کاریی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون کسی باید که بیدارت کند</p></div>
<div class="m2"><p>دیگری باید که او کارت کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که را این حسرت و این درد نیست</p></div>
<div class="m2"><p>خاک بر فرقش که این کس مرد نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که را این درد دل در هم سرشت</p></div>
<div class="m2"><p>محو شد هم دوزخ او را هم بهشت</p></div></div>