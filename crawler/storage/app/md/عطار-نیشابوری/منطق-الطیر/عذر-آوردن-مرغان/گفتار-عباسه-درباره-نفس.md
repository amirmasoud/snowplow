---
title: >-
    گفتار عباسه دربارهٔ نفس
---
# گفتار عباسه دربارهٔ نفس

<div class="b" id="bn1"><div class="m1"><p>یک شبی عباسه گفت ای حاضران</p></div>
<div class="m2"><p>این همه گر پر شوند از کافران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس همه از ترکمانی پر فضول</p></div>
<div class="m2"><p>از سر صدقی کنند ایمان قبول</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این تواند بود، اما آمدند</p></div>
<div class="m2"><p>انبیا این صد هزار و بیست و اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا شود این نفس کافر یک زمان</p></div>
<div class="m2"><p>یا مسلمان یا بمیرد در میان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این نیارستند کرد و آن رواست</p></div>
<div class="m2"><p>در میان چندین تفاوت از چه خاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما همه در حکم نفس کافریم</p></div>
<div class="m2"><p>در درون خویش کافر پروریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کافریست این نفس نافرمان چنین</p></div>
<div class="m2"><p>کشتن او کی بود آسان چنین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون مدد می‌گیرد این نفس از دو راه</p></div>
<div class="m2"><p>بس عجب باشد اگر گردد تباه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل سوار مملکت آمد مقیم</p></div>
<div class="m2"><p>روز و شب این نفس سگ او را ندیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اسب چندانی که می‌تازد سوار</p></div>
<div class="m2"><p>بر بر او می‌دود سگ در شکار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرک دل از حضرت جانان گرفت</p></div>
<div class="m2"><p>نفس از دل نیز هم چندان گرفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هرک این سگ را به مردی کرد بند</p></div>
<div class="m2"><p>در دو عالم شیرآرد در کمند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هرک این سگ را زبون خویش کرد</p></div>
<div class="m2"><p>گرد کفشش را نیابد هیچ مرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هرک این سگ را نهد بندی گران</p></div>
<div class="m2"><p>خاک او بهتر ز خون دیگران</p></div></div>