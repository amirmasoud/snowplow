---
title: >-
    حکایت پیرزنی که از شیخ مهنه دعای خوشدلی خواست
---
# حکایت پیرزنی که از شیخ مهنه دعای خوشدلی خواست

<div class="b" id="bn1"><div class="m1"><p>گفت شیخ مهنه را آن پیرزن</p></div>
<div class="m2"><p>دلخوشی را هین دعایی ده به من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌کشیدم بی‌مرادی پیش ازین</p></div>
<div class="m2"><p>می‌نیارم تاب اکنون بیش ازین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر دعای خوش دلی آموزیم</p></div>
<div class="m2"><p>بی‌شک آن وردی بود هر روزیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شیخ گفتش مدتی شد روزگار</p></div>
<div class="m2"><p>تا گرفتم من پس زانو حصار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اینچ می‌خواهی، بسی بشتافتم</p></div>
<div class="m2"><p>ذره‌ای نه دیدم و نه یافتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا دوا ناید پدید این درد را</p></div>
<div class="m2"><p>خوش دلی کی روی باشد مرد را</p></div></div>