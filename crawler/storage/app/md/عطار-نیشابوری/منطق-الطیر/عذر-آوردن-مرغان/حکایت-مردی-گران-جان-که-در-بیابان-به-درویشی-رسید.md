---
title: >-
    حکایت مردی گران جان که در بیابان به درویشی رسید
---
# حکایت مردی گران جان که در بیابان به درویشی رسید

<div class="b" id="bn1"><div class="m1"><p>بس سبک مردی گران جان می‌دوید</p></div>
<div class="m2"><p>در بیابانی به درویشی رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت چون داری تو ای درویش کار</p></div>
<div class="m2"><p>گفت آخر می‌بپرسی شرم دار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مانده‌ام در تنگنای این جهان</p></div>
<div class="m2"><p>تنگ تنگ است این جهانم در زمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرد گفتش اینچ گفتی نیست راست</p></div>
<div class="m2"><p>در بیابان فراخت تنگناست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت اگر اینجا نبودی تنگنا</p></div>
<div class="m2"><p>تو کجا افتادیی هرگز به ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ترا صد وعدهٔ خوش می‌دهند</p></div>
<div class="m2"><p>آن نشان زان سوی آتش می‌دهند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آتش تو چیست دنیا درگذر</p></div>
<div class="m2"><p>هم چو شیران کن ازین آتش حذر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون گذر کردی دل خویش آیدت</p></div>
<div class="m2"><p>پس سرای خوش شدن پیش آیدت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آتشی در پیش و راهی سخت دور</p></div>
<div class="m2"><p>تن ضعیف و دل اسیر و جان نفور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو ز جمله فارغ و پرداخته</p></div>
<div class="m2"><p>در میان کاری چنین برساخته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر بسی دیدی جهان، جان برفشان</p></div>
<div class="m2"><p>کز جهان نه نام داری نه نشان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر بسی بینی نه بینی هیچ تو</p></div>
<div class="m2"><p>چند گویم بیش ازین کم پیچ تو</p></div></div>